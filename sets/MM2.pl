% Modern Masters 2015 Edition

set('MM2').
set_name('MM2', 'Modern Masters 2015 Edition').
set_release_date('MM2', '2015-05-22').
set_border('MM2', 'black').
set_type('MM2', 'reprint').

card_in_set('æthersnipe', 'MM2').
card_original_type('æthersnipe'/'MM2', 'Creature — Elemental').
card_original_text('æthersnipe'/'MM2', 'When Æthersnipe enters the battlefield, return target nonland permanent to its owner\'s hand.\nEvoke {1}{U}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('æthersnipe'/'MM2', 'aethersnipe').
card_uid('æthersnipe'/'MM2', 'MM2:Æthersnipe:aethersnipe').
card_rarity('æthersnipe'/'MM2', 'Common').
card_artist('æthersnipe'/'MM2', 'Zoltan Boros & Gabor Szikszai').
card_number('æthersnipe'/'MM2', '39').
card_multiverse_id('æthersnipe'/'MM2', '397746').

card_in_set('agony warp', 'MM2').
card_original_type('agony warp'/'MM2', 'Instant').
card_original_text('agony warp'/'MM2', 'Target creature gets -3/-0 until end of turn.\nTarget creature gets -0/-3 until end of turn.').
card_image_name('agony warp'/'MM2', 'agony warp').
card_uid('agony warp'/'MM2', 'MM2:Agony Warp:agony warp').
card_rarity('agony warp'/'MM2', 'Uncommon').
card_artist('agony warp'/'MM2', 'Dave Allsop').
card_number('agony warp'/'MM2', '170').
card_flavor_text('agony warp'/'MM2', 'Life\'s circle has become inverted in Grixis. The same energy is endlessly recycled and becomes more stagnant with each pass.').
card_multiverse_id('agony warp'/'MM2', '397784').

card_in_set('air servant', 'MM2').
card_original_type('air servant'/'MM2', 'Creature — Elemental').
card_original_text('air servant'/'MM2', 'Flying\n{2}{U}: Tap target creature with flying.').
card_image_name('air servant'/'MM2', 'air servant').
card_uid('air servant'/'MM2', 'MM2:Air Servant:air servant').
card_rarity('air servant'/'MM2', 'Uncommon').
card_artist('air servant'/'MM2', 'Lars Grant-West').
card_number('air servant'/'MM2', '40').
card_flavor_text('air servant'/'MM2', '\"Wind is forceful, yet ephemeral. It can knock a dragon out of the sky, yet pass through the smallest crack unhindered.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('air servant'/'MM2', '397847').

card_in_set('algae gharial', 'MM2').
card_original_type('algae gharial'/'MM2', 'Creature — Crocodile').
card_original_text('algae gharial'/'MM2', 'Shroud (This creature can\'t be the target of spells or abilities.)\nWhenever another creature dies, you may put a +1/+1 counter on Algae Gharial.').
card_image_name('algae gharial'/'MM2', 'algae gharial').
card_uid('algae gharial'/'MM2', 'MM2:Algae Gharial:algae gharial').
card_rarity('algae gharial'/'MM2', 'Uncommon').
card_artist('algae gharial'/'MM2', 'Michael Ryan').
card_number('algae gharial'/'MM2', '137').
card_flavor_text('algae gharial'/'MM2', 'It lurks just under the surface, using the algae-choked tar pits of Jund as both home and hunting blind.').
card_multiverse_id('algae gharial'/'MM2', '397854').

card_in_set('all is dust', 'MM2').
card_original_type('all is dust'/'MM2', 'Tribal Sorcery — Eldrazi').
card_original_text('all is dust'/'MM2', 'Each player sacrifices all colored permanents he or she controls.').
card_image_name('all is dust'/'MM2', 'all is dust').
card_uid('all is dust'/'MM2', 'MM2:All Is Dust:all is dust').
card_rarity('all is dust'/'MM2', 'Rare').
card_artist('all is dust'/'MM2', 'Jason Felix').
card_number('all is dust'/'MM2', '1').
card_flavor_text('all is dust'/'MM2', '\"The emergence of the Eldrazi isn\'t necessarily a bad thing, as long as you\'ve already lived a fulfilling and complete life without regrets.\"\n—Javad Nasrin, Ondu relic hunter').
card_multiverse_id('all is dust'/'MM2', '397750').

card_in_set('all suns\' dawn', 'MM2').
card_original_type('all suns\' dawn'/'MM2', 'Sorcery').
card_original_text('all suns\' dawn'/'MM2', 'For each color, return up to one target card of that color from your graveyard to your hand. Exile All Suns\' Dawn.').
card_image_name('all suns\' dawn'/'MM2', 'all suns\' dawn').
card_uid('all suns\' dawn'/'MM2', 'MM2:All Suns\' Dawn:all suns\' dawn').
card_rarity('all suns\' dawn'/'MM2', 'Rare').
card_artist('all suns\' dawn'/'MM2', 'Glen Angus').
card_number('all suns\' dawn'/'MM2', '138').
card_flavor_text('all suns\' dawn'/'MM2', '\"When the fifth sun rose, Mirrodin saw its first true dawn.\"\n—Inscribed on the skin of Thrun, the last troll').
card_multiverse_id('all suns\' dawn'/'MM2', '397741').

card_in_set('alloy myr', 'MM2').
card_original_type('alloy myr'/'MM2', 'Artifact Creature — Myr').
card_original_text('alloy myr'/'MM2', '{T}: Add one mana of any color to your mana pool.').
card_image_name('alloy myr'/'MM2', 'alloy myr').
card_uid('alloy myr'/'MM2', 'MM2:Alloy Myr:alloy myr').
card_rarity('alloy myr'/'MM2', 'Common').
card_artist('alloy myr'/'MM2', 'Matt Cavotta').
card_number('alloy myr'/'MM2', '201').
card_flavor_text('alloy myr'/'MM2', 'With or without witnesses, the suns continued their prismatic dance.').
card_multiverse_id('alloy myr'/'MM2', '397796').

card_in_set('ant queen', 'MM2').
card_original_type('ant queen'/'MM2', 'Creature — Insect').
card_original_text('ant queen'/'MM2', '{1}{G}: Put a 1/1 green Insect creature token onto the battlefield.').
card_image_name('ant queen'/'MM2', 'ant queen').
card_uid('ant queen'/'MM2', 'MM2:Ant Queen:ant queen').
card_rarity('ant queen'/'MM2', 'Rare').
card_artist('ant queen'/'MM2', 'Trevor Claxton').
card_number('ant queen'/'MM2', '139').
card_flavor_text('ant queen'/'MM2', '\"Kill the queen first, or we\'ll be fighting her drones forever. It is not in a queen\'s nature to have enough servants.\"\n—Borzard, exterminator captain').
card_multiverse_id('ant queen'/'MM2', '397821').

card_in_set('apocalypse hydra', 'MM2').
card_original_type('apocalypse hydra'/'MM2', 'Creature — Hydra').
card_original_text('apocalypse hydra'/'MM2', 'Apocalypse Hydra enters the battlefield with X +1/+1 counters on it. If X is 5 or more, it enters the battlefield with an additional X +1/+1 counters on it.\n{1}{R}, Remove a +1/+1 counter from Apocalypse Hydra: Apocalypse Hydra deals 1 damage to target creature or player.').
card_image_name('apocalypse hydra'/'MM2', 'apocalypse hydra').
card_uid('apocalypse hydra'/'MM2', 'MM2:Apocalypse Hydra:apocalypse hydra').
card_rarity('apocalypse hydra'/'MM2', 'Rare').
card_artist('apocalypse hydra'/'MM2', 'Jason Chan').
card_number('apocalypse hydra'/'MM2', '171').
card_multiverse_id('apocalypse hydra'/'MM2', '397885').

card_in_set('apostle\'s blessing', 'MM2').
card_original_type('apostle\'s blessing'/'MM2', 'Instant').
card_original_text('apostle\'s blessing'/'MM2', '({W/P} can be paid with either {W} or 2 life.)\nTarget artifact or creature you control gains protection from artifacts or from the color of your choice until end of turn.').
card_image_name('apostle\'s blessing'/'MM2', 'apostle\'s blessing').
card_uid('apostle\'s blessing'/'MM2', 'MM2:Apostle\'s Blessing:apostle\'s blessing').
card_rarity('apostle\'s blessing'/'MM2', 'Common').
card_artist('apostle\'s blessing'/'MM2', 'Brad Rigney').
card_number('apostle\'s blessing'/'MM2', '8').
card_multiverse_id('apostle\'s blessing'/'MM2', '397768').

card_in_set('aquastrand spider', 'MM2').
card_original_type('aquastrand spider'/'MM2', 'Creature — Spider Mutant').
card_original_text('aquastrand spider'/'MM2', 'Graft 2 (This creature enters the battlefield with two +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{G}: Target creature with a +1/+1 counter on it gains reach until end of turn. (It can block creatures with flying.)').
card_image_name('aquastrand spider'/'MM2', 'aquastrand spider').
card_uid('aquastrand spider'/'MM2', 'MM2:Aquastrand Spider:aquastrand spider').
card_rarity('aquastrand spider'/'MM2', 'Common').
card_artist('aquastrand spider'/'MM2', 'Dany Orizio').
card_number('aquastrand spider'/'MM2', '140').
card_multiverse_id('aquastrand spider'/'MM2', '397697').

card_in_set('argent sphinx', 'MM2').
card_original_type('argent sphinx'/'MM2', 'Creature — Sphinx').
card_original_text('argent sphinx'/'MM2', 'Flying\nMetalcraft — {U}: Exile Argent Sphinx. Return it to the battlefield under your control at the beginning of the next end step. Activate this ability only if you control three or more artifacts.').
card_image_name('argent sphinx'/'MM2', 'argent sphinx').
card_uid('argent sphinx'/'MM2', 'MM2:Argent Sphinx:argent sphinx').
card_rarity('argent sphinx'/'MM2', 'Rare').
card_artist('argent sphinx'/'MM2', 'Chris Rahn').
card_number('argent sphinx'/'MM2', '41').
card_flavor_text('argent sphinx'/'MM2', 'A great mirage, a dream of wings and silver.').
card_multiverse_id('argent sphinx'/'MM2', '397887').

card_in_set('arrest', 'MM2').
card_original_type('arrest'/'MM2', 'Enchantment — Aura').
card_original_text('arrest'/'MM2', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.').
card_image_name('arrest'/'MM2', 'arrest').
card_uid('arrest'/'MM2', 'MM2:Arrest:arrest').
card_rarity('arrest'/'MM2', 'Common').
card_artist('arrest'/'MM2', 'Daarken').
card_number('arrest'/'MM2', '9').
card_flavor_text('arrest'/'MM2', 'Bladehold houses a rogues\' gallery filled with the living \"statues\" of the worst enemies of the Auriok.').
card_multiverse_id('arrest'/'MM2', '397723').

card_in_set('artisan of kozilek', 'MM2').
card_original_type('artisan of kozilek'/'MM2', 'Creature — Eldrazi').
card_original_text('artisan of kozilek'/'MM2', 'When you cast Artisan of Kozilek, you may return target creature card from your graveyard to the battlefield.\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)').
card_image_name('artisan of kozilek'/'MM2', 'artisan of kozilek').
card_uid('artisan of kozilek'/'MM2', 'MM2:Artisan of Kozilek:artisan of kozilek').
card_rarity('artisan of kozilek'/'MM2', 'Uncommon').
card_artist('artisan of kozilek'/'MM2', 'Jason Felix').
card_number('artisan of kozilek'/'MM2', '2').
card_multiverse_id('artisan of kozilek'/'MM2', '397777').

card_in_set('ashenmoor gouger', 'MM2').
card_original_type('ashenmoor gouger'/'MM2', 'Creature — Elemental Warrior').
card_original_text('ashenmoor gouger'/'MM2', 'Ashenmoor Gouger can\'t block.').
card_image_name('ashenmoor gouger'/'MM2', 'ashenmoor gouger').
card_uid('ashenmoor gouger'/'MM2', 'MM2:Ashenmoor Gouger:ashenmoor gouger').
card_rarity('ashenmoor gouger'/'MM2', 'Uncommon').
card_artist('ashenmoor gouger'/'MM2', 'Matt Cavotta').
card_number('ashenmoor gouger'/'MM2', '190').
card_flavor_text('ashenmoor gouger'/'MM2', 'After his hands had crumbled away, leaving only wickedly sharp points, he decided his only purpose was war.').
card_multiverse_id('ashenmoor gouger'/'MM2', '397691').

card_in_set('azorius chancery', 'MM2').
card_original_type('azorius chancery'/'MM2', 'Land').
card_original_text('azorius chancery'/'MM2', 'Azorius Chancery enters the battlefield tapped.\nWhen Azorius Chancery enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {W}{U} to your mana pool.').
card_image_name('azorius chancery'/'MM2', 'azorius chancery').
card_uid('azorius chancery'/'MM2', 'MM2:Azorius Chancery:azorius chancery').
card_rarity('azorius chancery'/'MM2', 'Uncommon').
card_artist('azorius chancery'/'MM2', 'John Avon').
card_number('azorius chancery'/'MM2', '235').
card_multiverse_id('azorius chancery'/'MM2', '397866').

card_in_set('banefire', 'MM2').
card_original_type('banefire'/'MM2', 'Sorcery').
card_original_text('banefire'/'MM2', 'Banefire deals X damage to target creature or player.\nIf X is 5 or more, Banefire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_image_name('banefire'/'MM2', 'banefire').
card_uid('banefire'/'MM2', 'MM2:Banefire:banefire').
card_rarity('banefire'/'MM2', 'Rare').
card_artist('banefire'/'MM2', 'Raymond Swanland').
card_number('banefire'/'MM2', '104').
card_flavor_text('banefire'/'MM2', 'For Sarkhan Vol, the dragon is the purest expression of life\'s savage splendor.').
card_multiverse_id('banefire'/'MM2', '397676').

card_in_set('battlegrace angel', 'MM2').
card_original_type('battlegrace angel'/'MM2', 'Creature — Angel').
card_original_text('battlegrace angel'/'MM2', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, it gains lifelink until end of turn.').
card_image_name('battlegrace angel'/'MM2', 'battlegrace angel').
card_uid('battlegrace angel'/'MM2', 'MM2:Battlegrace Angel:battlegrace angel').
card_rarity('battlegrace angel'/'MM2', 'Rare').
card_artist('battlegrace angel'/'MM2', 'Matt Stewart').
card_number('battlegrace angel'/'MM2', '10').
card_multiverse_id('battlegrace angel'/'MM2', '397766').

card_in_set('bestial menace', 'MM2').
card_original_type('bestial menace'/'MM2', 'Sorcery').
card_original_text('bestial menace'/'MM2', 'Put a 1/1 green Snake creature token, a 2/2 green Wolf creature token, and a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('bestial menace'/'MM2', 'bestial menace').
card_uid('bestial menace'/'MM2', 'MM2:Bestial Menace:bestial menace').
card_rarity('bestial menace'/'MM2', 'Uncommon').
card_artist('bestial menace'/'MM2', 'Andrew Robinson').
card_number('bestial menace'/'MM2', '141').
card_flavor_text('bestial menace'/'MM2', '\"My battle cry reaches ears far keener than yours.\"\n—Saidah, Joraga hunter').
card_multiverse_id('bestial menace'/'MM2', '397840').

card_in_set('bitterblossom', 'MM2').
card_original_type('bitterblossom'/'MM2', 'Tribal Enchantment — Faerie').
card_original_text('bitterblossom'/'MM2', 'At the beginning of your upkeep, you lose 1 life and put a 1/1 black Faerie Rogue creature token with flying onto the battlefield.').
card_image_name('bitterblossom'/'MM2', 'bitterblossom').
card_uid('bitterblossom'/'MM2', 'MM2:Bitterblossom:bitterblossom').
card_rarity('bitterblossom'/'MM2', 'Mythic Rare').
card_artist('bitterblossom'/'MM2', 'Rebecca Guay').
card_number('bitterblossom'/'MM2', '71').
card_flavor_text('bitterblossom'/'MM2', 'In Lorwyn\'s brief evenings, the sun pauses at the horizon long enough for a certain species of violet to bloom with the fragrance of mischief.').
card_multiverse_id('bitterblossom'/'MM2', '397701').

card_in_set('blades of velis vel', 'MM2').
card_original_type('blades of velis vel'/'MM2', 'Tribal Instant — Shapeshifter').
card_original_text('blades of velis vel'/'MM2', 'Changeling (This card is every creature type at all times.)\nUp to two target creatures each get +2/+0 and gain all creature types until end of turn.').
card_image_name('blades of velis vel'/'MM2', 'blades of velis vel').
card_uid('blades of velis vel'/'MM2', 'MM2:Blades of Velis Vel:blades of velis vel').
card_rarity('blades of velis vel'/'MM2', 'Common').
card_artist('blades of velis vel'/'MM2', 'Ron Spencer').
card_number('blades of velis vel'/'MM2', '105').
card_flavor_text('blades of velis vel'/'MM2', '\"The changing kind suffers as we do. We must join as one to quench our tyrants!\"').
card_multiverse_id('blades of velis vel'/'MM2', '397754').

card_in_set('blinding souleater', 'MM2').
card_original_type('blinding souleater'/'MM2', 'Artifact Creature — Cleric').
card_original_text('blinding souleater'/'MM2', '{W/P}, {T}: Tap target creature. ({W/P} can be paid with either {W} or 2 life.)').
card_image_name('blinding souleater'/'MM2', 'blinding souleater').
card_uid('blinding souleater'/'MM2', 'MM2:Blinding Souleater:blinding souleater').
card_rarity('blinding souleater'/'MM2', 'Common').
card_artist('blinding souleater'/'MM2', 'Igor Kieryluk').
card_number('blinding souleater'/'MM2', '202').
card_flavor_text('blinding souleater'/'MM2', '\"We thank the souleaters for inscribing our souls with subservience, to reinforce the sacred order.\"\n—Drones\' hymn of gratitude').
card_multiverse_id('blinding souleater'/'MM2', '397810').

card_in_set('blinkmoth nexus', 'MM2').
card_original_type('blinkmoth nexus'/'MM2', 'Land').
card_original_text('blinkmoth nexus'/'MM2', '{T}: Add {1} to your mana pool.\n{1}: Blinkmoth Nexus becomes a 1/1 Blinkmoth artifact creature with flying until end of turn. It\'s still a land.\n{1}, {T}: Target Blinkmoth creature gets +1/+1 until end of turn.').
card_image_name('blinkmoth nexus'/'MM2', 'blinkmoth nexus').
card_uid('blinkmoth nexus'/'MM2', 'MM2:Blinkmoth Nexus:blinkmoth nexus').
card_rarity('blinkmoth nexus'/'MM2', 'Rare').
card_artist('blinkmoth nexus'/'MM2', 'Sam Burley').
card_number('blinkmoth nexus'/'MM2', '236').
card_multiverse_id('blinkmoth nexus'/'MM2', '397660').

card_in_set('blood ogre', 'MM2').
card_original_type('blood ogre'/'MM2', 'Creature — Ogre Warrior').
card_original_text('blood ogre'/'MM2', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFirst strike').
card_image_name('blood ogre'/'MM2', 'blood ogre').
card_uid('blood ogre'/'MM2', 'MM2:Blood Ogre:blood ogre').
card_rarity('blood ogre'/'MM2', 'Common').
card_artist('blood ogre'/'MM2', 'Christopher Moeller').
card_number('blood ogre'/'MM2', '106').
card_flavor_text('blood ogre'/'MM2', '\"Ogres taunt the dragons by moving into their territory, but even the hellkites find them too much trouble to hunt.\"\n—Marza, mountain scout').
card_multiverse_id('blood ogre'/'MM2', '397759').

card_in_set('bloodshot trainee', 'MM2').
card_original_type('bloodshot trainee'/'MM2', 'Creature — Goblin Warrior').
card_original_text('bloodshot trainee'/'MM2', '{T}: Bloodshot Trainee deals 4 damage to target creature. Activate this ability only if Bloodshot Trainee\'s power is 4 or greater.').
card_image_name('bloodshot trainee'/'MM2', 'bloodshot trainee').
card_uid('bloodshot trainee'/'MM2', 'MM2:Bloodshot Trainee:bloodshot trainee').
card_rarity('bloodshot trainee'/'MM2', 'Uncommon').
card_artist('bloodshot trainee'/'MM2', 'Matt Stewart').
card_number('bloodshot trainee'/'MM2', '107').
card_flavor_text('bloodshot trainee'/'MM2', '\"Just think how strong I\'ll be if my arms don\'t tear off!\"').
card_multiverse_id('bloodshot trainee'/'MM2', '397671').

card_in_set('bloodthrone vampire', 'MM2').
card_original_type('bloodthrone vampire'/'MM2', 'Creature — Vampire').
card_original_text('bloodthrone vampire'/'MM2', 'Sacrifice a creature: Bloodthrone Vampire gets +2/+2 until end of turn.').
card_image_name('bloodthrone vampire'/'MM2', 'bloodthrone vampire').
card_uid('bloodthrone vampire'/'MM2', 'MM2:Bloodthrone Vampire:bloodthrone vampire').
card_rarity('bloodthrone vampire'/'MM2', 'Common').
card_artist('bloodthrone vampire'/'MM2', 'Steve Argyle').
card_number('bloodthrone vampire'/'MM2', '72').
card_flavor_text('bloodthrone vampire'/'MM2', 'Some humans willingly offered up their blood, hoping it would grant the vampire families the strength to stave off the Eldrazi.').
card_multiverse_id('bloodthrone vampire'/'MM2', '397715').

card_in_set('bone splinters', 'MM2').
card_original_type('bone splinters'/'MM2', 'Sorcery').
card_original_text('bone splinters'/'MM2', 'As an additional cost to cast Bone Splinters, sacrifice a creature.\nDestroy target creature.').
card_image_name('bone splinters'/'MM2', 'bone splinters').
card_uid('bone splinters'/'MM2', 'MM2:Bone Splinters:bone splinters').
card_rarity('bone splinters'/'MM2', 'Common').
card_artist('bone splinters'/'MM2', 'Cole Eastburn').
card_number('bone splinters'/'MM2', '73').
card_flavor_text('bone splinters'/'MM2', 'Witches of the Split-Eye Coven speak of a future when Grixis will overflow with life energy. For now, they must harvest vis from the living to fuel their dark magics.').
card_multiverse_id('bone splinters'/'MM2', '397770').

card_in_set('boros garrison', 'MM2').
card_original_type('boros garrison'/'MM2', 'Land').
card_original_text('boros garrison'/'MM2', 'Boros Garrison enters the battlefield tapped.\nWhen Boros Garrison enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{W} to your mana pool.').
card_image_name('boros garrison'/'MM2', 'boros garrison').
card_uid('boros garrison'/'MM2', 'MM2:Boros Garrison:boros garrison').
card_rarity('boros garrison'/'MM2', 'Uncommon').
card_artist('boros garrison'/'MM2', 'John Avon').
card_number('boros garrison'/'MM2', '237').
card_multiverse_id('boros garrison'/'MM2', '397817').

card_in_set('boros swiftblade', 'MM2').
card_original_type('boros swiftblade'/'MM2', 'Creature — Human Soldier').
card_original_text('boros swiftblade'/'MM2', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_image_name('boros swiftblade'/'MM2', 'boros swiftblade').
card_uid('boros swiftblade'/'MM2', 'MM2:Boros Swiftblade:boros swiftblade').
card_rarity('boros swiftblade'/'MM2', 'Uncommon').
card_artist('boros swiftblade'/'MM2', 'Doug Chaffee').
card_number('boros swiftblade'/'MM2', '172').
card_flavor_text('boros swiftblade'/'MM2', 'When the Boros Legion attacks, swiftblades enter the fray first. They pick off the archers and mages, softening the enemy front before the flame-kin and giants go in.').
card_multiverse_id('boros swiftblade'/'MM2', '397870').

card_in_set('brute force', 'MM2').
card_original_type('brute force'/'MM2', 'Instant').
card_original_text('brute force'/'MM2', 'Target creature gets +3/+3 until end of turn.').
card_image_name('brute force'/'MM2', 'brute force').
card_uid('brute force'/'MM2', 'MM2:Brute Force:brute force').
card_rarity('brute force'/'MM2', 'Common').
card_artist('brute force'/'MM2', 'Wayne Reynolds').
card_number('brute force'/'MM2', '108').
card_flavor_text('brute force'/'MM2', 'Blood, bone, and sinew are magnified, as is the rage that drives them. The brain, however, remains unchanged—a little bean, swinging by a strand in a cavernous, raving head.').
card_multiverse_id('brute force'/'MM2', '397729').

card_in_set('burst lightning', 'MM2').
card_original_type('burst lightning'/'MM2', 'Instant').
card_original_text('burst lightning'/'MM2', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nBurst Lightning deals 2 damage to target creature or player. If Burst Lightning was kicked, it deals 4 damage to that creature or player instead.').
card_image_name('burst lightning'/'MM2', 'burst lightning').
card_uid('burst lightning'/'MM2', 'MM2:Burst Lightning:burst lightning').
card_rarity('burst lightning'/'MM2', 'Common').
card_artist('burst lightning'/'MM2', 'Vance Kovacs').
card_number('burst lightning'/'MM2', '109').
card_multiverse_id('burst lightning'/'MM2', '397662').

card_in_set('cathodion', 'MM2').
card_original_type('cathodion'/'MM2', 'Artifact Creature — Construct').
card_original_text('cathodion'/'MM2', 'When Cathodion dies, add {3} to your mana pool.').
card_image_name('cathodion'/'MM2', 'cathodion').
card_uid('cathodion'/'MM2', 'MM2:Cathodion:cathodion').
card_rarity('cathodion'/'MM2', 'Common').
card_artist('cathodion'/'MM2', 'Izzy').
card_number('cathodion'/'MM2', '203').
card_flavor_text('cathodion'/'MM2', 'Instead of creating a tool that would be damaged by heat, the Thran built one that was charged by it.').
card_multiverse_id('cathodion'/'MM2', '397827').

card_in_set('celestial purge', 'MM2').
card_original_type('celestial purge'/'MM2', 'Instant').
card_original_text('celestial purge'/'MM2', 'Exile target black or red permanent.').
card_image_name('celestial purge'/'MM2', 'celestial purge').
card_uid('celestial purge'/'MM2', 'MM2:Celestial Purge:celestial purge').
card_rarity('celestial purge'/'MM2', 'Uncommon').
card_artist('celestial purge'/'MM2', 'David Palumbo').
card_number('celestial purge'/'MM2', '11').
card_flavor_text('celestial purge'/'MM2', '\"They say only the good die young. Obviously, you are one of the exceptions.\"\n—Delrobah, cleric of Ivora Gate').
card_multiverse_id('celestial purge'/'MM2', '397699').

card_in_set('chimeric mass', 'MM2').
card_original_type('chimeric mass'/'MM2', 'Artifact').
card_original_text('chimeric mass'/'MM2', 'Chimeric Mass enters the battlefield with X charge counters on it.\n{1}: Until end of turn, Chimeric Mass becomes a Construct artifact creature with \"This creature\'s power and toughness are each equal to the number of charge counters on it.\"').
card_image_name('chimeric mass'/'MM2', 'chimeric mass').
card_uid('chimeric mass'/'MM2', 'MM2:Chimeric Mass:chimeric mass').
card_rarity('chimeric mass'/'MM2', 'Rare').
card_artist('chimeric mass'/'MM2', 'David Palumbo').
card_number('chimeric mass'/'MM2', '204').
card_multiverse_id('chimeric mass'/'MM2', '397782').

card_in_set('cloud elemental', 'MM2').
card_original_type('cloud elemental'/'MM2', 'Creature — Elemental').
card_original_text('cloud elemental'/'MM2', 'Flying\nCloud Elemental can block only creatures with flying.').
card_image_name('cloud elemental'/'MM2', 'cloud elemental').
card_uid('cloud elemental'/'MM2', 'MM2:Cloud Elemental:cloud elemental').
card_rarity('cloud elemental'/'MM2', 'Common').
card_artist('cloud elemental'/'MM2', 'Michael Sutfin').
card_number('cloud elemental'/'MM2', '42').
card_flavor_text('cloud elemental'/'MM2', '\"The sky teems with just as much life as the forest or the deep seas.\"\n—Hadi Kasten, Calla Dale naturalist').
card_multiverse_id('cloud elemental'/'MM2', '397744').

card_in_set('combust', 'MM2').
card_original_type('combust'/'MM2', 'Instant').
card_original_text('combust'/'MM2', 'Combust can\'t be countered by spells or abilities.\nCombust deals 5 damage to target white or blue creature. The damage can\'t be prevented.').
card_image_name('combust'/'MM2', 'combust').
card_uid('combust'/'MM2', 'MM2:Combust:combust').
card_rarity('combust'/'MM2', 'Uncommon').
card_artist('combust'/'MM2', 'Jaime Jones').
card_number('combust'/'MM2', '110').
card_multiverse_id('combust'/'MM2', '397783').

card_in_set('comet storm', 'MM2').
card_original_type('comet storm'/'MM2', 'Instant').
card_original_text('comet storm'/'MM2', 'Multikicker {1} (You may pay an additional {1} any number of times as you cast this spell.)\nChoose target creature or player, then choose another target creature or player for each time Comet Storm was kicked. Comet Storm deals X damage to each of them.').
card_image_name('comet storm'/'MM2', 'comet storm').
card_uid('comet storm'/'MM2', 'MM2:Comet Storm:comet storm').
card_rarity('comet storm'/'MM2', 'Mythic Rare').
card_artist('comet storm'/'MM2', 'Jung Park').
card_number('comet storm'/'MM2', '111').
card_multiverse_id('comet storm'/'MM2', '397758').

card_in_set('commune with nature', 'MM2').
card_original_type('commune with nature'/'MM2', 'Sorcery').
card_original_text('commune with nature'/'MM2', 'Look at the top five cards of your library. You may reveal a creature card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('commune with nature'/'MM2', 'commune with nature').
card_uid('commune with nature'/'MM2', 'MM2:Commune with Nature:commune with nature').
card_rarity('commune with nature'/'MM2', 'Common').
card_artist('commune with nature'/'MM2', 'Lars Grant-West').
card_number('commune with nature'/'MM2', '142').
card_multiverse_id('commune with nature'/'MM2', '397767').

card_in_set('conclave phalanx', 'MM2').
card_original_type('conclave phalanx'/'MM2', 'Creature — Human Soldier').
card_original_text('conclave phalanx'/'MM2', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nWhen Conclave Phalanx enters the battlefield, you gain 1 life for each creature you control.').
card_image_name('conclave phalanx'/'MM2', 'conclave phalanx').
card_uid('conclave phalanx'/'MM2', 'MM2:Conclave Phalanx:conclave phalanx').
card_rarity('conclave phalanx'/'MM2', 'Common').
card_artist('conclave phalanx'/'MM2', 'Wayne Reynolds').
card_number('conclave phalanx'/'MM2', '12').
card_multiverse_id('conclave phalanx'/'MM2', '397812').

card_in_set('copper carapace', 'MM2').
card_original_type('copper carapace'/'MM2', 'Artifact — Equipment').
card_original_text('copper carapace'/'MM2', 'Equipped creature gets +2/+2 and can\'t block.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('copper carapace'/'MM2', 'copper carapace').
card_uid('copper carapace'/'MM2', 'MM2:Copper Carapace:copper carapace').
card_rarity('copper carapace'/'MM2', 'Common').
card_artist('copper carapace'/'MM2', 'Franz Vohwinkel').
card_number('copper carapace'/'MM2', '205').
card_flavor_text('copper carapace'/'MM2', '\"We will fight as they do: our flesh protected behind metal.\"\n—Tae Aquil, Viridian weaponsmith').
card_multiverse_id('copper carapace'/'MM2', '397849').

card_in_set('court homunculus', 'MM2').
card_original_type('court homunculus'/'MM2', 'Artifact Creature — Homunculus').
card_original_text('court homunculus'/'MM2', 'Court Homunculus gets +1/+1 as long as you control another artifact.').
card_image_name('court homunculus'/'MM2', 'court homunculus').
card_uid('court homunculus'/'MM2', 'MM2:Court Homunculus:court homunculus').
card_rarity('court homunculus'/'MM2', 'Common').
card_artist('court homunculus'/'MM2', 'Matt Cavotta').
card_number('court homunculus'/'MM2', '13').
card_flavor_text('court homunculus'/'MM2', 'Mages of Esper measure their wealth and status by the number of servants in their retinues.').
card_multiverse_id('court homunculus'/'MM2', '397776').

card_in_set('cranial plating', 'MM2').
card_original_type('cranial plating'/'MM2', 'Artifact — Equipment').
card_original_text('cranial plating'/'MM2', 'Equipped creature gets +1/+0 for each artifact you control.\n{B}{B}: Attach Cranial Plating to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('cranial plating'/'MM2', 'cranial plating').
card_uid('cranial plating'/'MM2', 'MM2:Cranial Plating:cranial plating').
card_rarity('cranial plating'/'MM2', 'Uncommon').
card_artist('cranial plating'/'MM2', 'Adam Rex').
card_number('cranial plating'/'MM2', '206').
card_multiverse_id('cranial plating'/'MM2', '397869').

card_in_set('creakwood liege', 'MM2').
card_original_type('creakwood liege'/'MM2', 'Creature — Horror').
card_original_text('creakwood liege'/'MM2', 'Other black creatures you control get +1/+1.\nOther green creatures you control get +1/+1.\nAt the beginning of your upkeep, you may put a 1/1 black and green Worm creature token onto the battlefield.').
card_image_name('creakwood liege'/'MM2', 'creakwood liege').
card_uid('creakwood liege'/'MM2', 'MM2:Creakwood Liege:creakwood liege').
card_rarity('creakwood liege'/'MM2', 'Rare').
card_artist('creakwood liege'/'MM2', 'Cole Eastburn').
card_number('creakwood liege'/'MM2', '191').
card_multiverse_id('creakwood liege'/'MM2', '397875').

card_in_set('cryptic command', 'MM2').
card_original_type('cryptic command'/'MM2', 'Instant').
card_original_text('cryptic command'/'MM2', 'Choose two —\n• Counter target spell.\n• Return target permanent to its owner\'s hand.\n• Tap all creatures your opponents control.\n• Draw a card.').
card_image_name('cryptic command'/'MM2', 'cryptic command').
card_uid('cryptic command'/'MM2', 'MM2:Cryptic Command:cryptic command').
card_rarity('cryptic command'/'MM2', 'Rare').
card_artist('cryptic command'/'MM2', 'Wayne England').
card_number('cryptic command'/'MM2', '43').
card_multiverse_id('cryptic command'/'MM2', '397793').

card_in_set('culling dais', 'MM2').
card_original_type('culling dais'/'MM2', 'Artifact').
card_original_text('culling dais'/'MM2', '{T}, Sacrifice a creature: Put a charge counter on Culling Dais.\n{1}, Sacrifice Culling Dais: Draw a card for each charge counter on Culling Dais.').
card_image_name('culling dais'/'MM2', 'culling dais').
card_uid('culling dais'/'MM2', 'MM2:Culling Dais:culling dais').
card_rarity('culling dais'/'MM2', 'Uncommon').
card_artist('culling dais'/'MM2', 'Anthony Palumbo').
card_number('culling dais'/'MM2', '207').
card_flavor_text('culling dais'/'MM2', '\"Forswear the flesh and you will truly see.\"\n—Jin-Gitaxias, Core Augur').
card_multiverse_id('culling dais'/'MM2', '397780').

card_in_set('cytoplast root-kin', 'MM2').
card_original_type('cytoplast root-kin'/'MM2', 'Creature — Elemental Mutant').
card_original_text('cytoplast root-kin'/'MM2', 'Graft 4 (This creature enters the battlefield with four +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\nWhen Cytoplast Root-Kin enters the battlefield, put a +1/+1 counter on each other creature you control with a +1/+1 counter on it.\n{2}: Move a +1/+1 counter from target creature you control onto Cytoplast Root-Kin.').
card_image_name('cytoplast root-kin'/'MM2', 'cytoplast root-kin').
card_uid('cytoplast root-kin'/'MM2', 'MM2:Cytoplast Root-Kin:cytoplast root-kin').
card_rarity('cytoplast root-kin'/'MM2', 'Uncommon').
card_artist('cytoplast root-kin'/'MM2', 'Thomas M. Baxa').
card_number('cytoplast root-kin'/'MM2', '143').
card_multiverse_id('cytoplast root-kin'/'MM2', '397684').

card_in_set('daggerclaw imp', 'MM2').
card_original_type('daggerclaw imp'/'MM2', 'Creature — Imp').
card_original_text('daggerclaw imp'/'MM2', 'Flying\nDaggerclaw Imp can\'t block.').
card_image_name('daggerclaw imp'/'MM2', 'daggerclaw imp').
card_uid('daggerclaw imp'/'MM2', 'MM2:Daggerclaw Imp:daggerclaw imp').
card_rarity('daggerclaw imp'/'MM2', 'Uncommon').
card_artist('daggerclaw imp'/'MM2', 'Pete Venters').
card_number('daggerclaw imp'/'MM2', '74').
card_flavor_text('daggerclaw imp'/'MM2', 'The Simic use the claws as scalpels, while the Rakdos use them for tattooing and torture. The Gruul use them to pick their teeth after lunching on the rest of the carcass.').
card_multiverse_id('daggerclaw imp'/'MM2', '397813').

card_in_set('dark confidant', 'MM2').
card_original_type('dark confidant'/'MM2', 'Creature — Human Wizard').
card_original_text('dark confidant'/'MM2', 'At the beginning of your upkeep, reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost.').
card_image_name('dark confidant'/'MM2', 'dark confidant').
card_uid('dark confidant'/'MM2', 'MM2:Dark Confidant:dark confidant').
card_rarity('dark confidant'/'MM2', 'Mythic Rare').
card_artist('dark confidant'/'MM2', 'Scott M. Fischer').
card_number('dark confidant'/'MM2', '75').
card_flavor_text('dark confidant'/'MM2', 'Greatness, at any cost.').
card_multiverse_id('dark confidant'/'MM2', '397731').

card_in_set('darksteel axe', 'MM2').
card_original_type('darksteel axe'/'MM2', 'Artifact — Equipment').
card_original_text('darksteel axe'/'MM2', 'Indestructible (Effects that say \"destroy\" don\'t destroy this artifact.)\nEquipped creature gets +2/+0.\nEquip {2}').
card_image_name('darksteel axe'/'MM2', 'darksteel axe').
card_uid('darksteel axe'/'MM2', 'MM2:Darksteel Axe:darksteel axe').
card_rarity('darksteel axe'/'MM2', 'Uncommon').
card_artist('darksteel axe'/'MM2', 'Daniel Ljunggren').
card_number('darksteel axe'/'MM2', '208').
card_flavor_text('darksteel axe'/'MM2', 'Heavier than it looks, tricky to wield, guaranteed to last.').
card_multiverse_id('darksteel axe'/'MM2', '397685').

card_in_set('darksteel citadel', 'MM2').
card_original_type('darksteel citadel'/'MM2', 'Artifact Land').
card_original_text('darksteel citadel'/'MM2', 'Indestructible (Effects that say \"destroy\" don\'t destroy this land.)\n{T}: Add {1} to your mana pool.').
card_image_name('darksteel citadel'/'MM2', 'darksteel citadel').
card_uid('darksteel citadel'/'MM2', 'MM2:Darksteel Citadel:darksteel citadel').
card_rarity('darksteel citadel'/'MM2', 'Common').
card_artist('darksteel citadel'/'MM2', 'John Avon').
card_number('darksteel citadel'/'MM2', '238').
card_flavor_text('darksteel citadel'/'MM2', 'Structures built from darksteel yield to neither assault nor age.').
card_multiverse_id('darksteel citadel'/'MM2', '397853').

card_in_set('daybreak coronet', 'MM2').
card_original_type('daybreak coronet'/'MM2', 'Enchantment — Aura').
card_original_text('daybreak coronet'/'MM2', 'Enchant creature with another Aura attached to it\nEnchanted creature gets +3/+3 and has first strike, vigilance, and lifelink.').
card_image_name('daybreak coronet'/'MM2', 'daybreak coronet').
card_uid('daybreak coronet'/'MM2', 'MM2:Daybreak Coronet:daybreak coronet').
card_rarity('daybreak coronet'/'MM2', 'Rare').
card_artist('daybreak coronet'/'MM2', 'Johannes Voss').
card_number('daybreak coronet'/'MM2', '14').
card_flavor_text('daybreak coronet'/'MM2', '\"Let the deception of the gods yield to the pure light of day.\"\n—Brimaz, king of Oreskos').
card_multiverse_id('daybreak coronet'/'MM2', '397798').

card_in_set('death denied', 'MM2').
card_original_type('death denied'/'MM2', 'Instant — Arcane').
card_original_text('death denied'/'MM2', 'Return X target creature cards from your graveyard to your hand.').
card_image_name('death denied'/'MM2', 'death denied').
card_uid('death denied'/'MM2', 'MM2:Death Denied:death denied').
card_rarity('death denied'/'MM2', 'Common').
card_artist('death denied'/'MM2', 'James Paick').
card_number('death denied'/'MM2', '76').
card_flavor_text('death denied'/'MM2', 'Takenuma was filled with a chorus of moans, shrieks, and wails. Some came from the living, some from the dying, and some, most horribly, from the dead.').
card_multiverse_id('death denied'/'MM2', '397718').

card_in_set('deathmark', 'MM2').
card_original_type('deathmark'/'MM2', 'Sorcery').
card_original_text('deathmark'/'MM2', 'Destroy target green or white creature.').
card_image_name('deathmark'/'MM2', 'deathmark').
card_uid('deathmark'/'MM2', 'MM2:Deathmark:deathmark').
card_rarity('deathmark'/'MM2', 'Uncommon').
card_artist('deathmark'/'MM2', 'Steven Belledin').
card_number('deathmark'/'MM2', '77').
card_flavor_text('deathmark'/'MM2', 'There are few ways to escape the deathmark: bargaining with a demon, washing in the fabled waters of youth, and of course, death.').
card_multiverse_id('deathmark'/'MM2', '397787').

card_in_set('devouring greed', 'MM2').
card_original_type('devouring greed'/'MM2', 'Sorcery — Arcane').
card_original_text('devouring greed'/'MM2', 'As an additional cost to cast Devouring Greed, you may sacrifice any number of Spirits.\nTarget player loses 2 life plus 2 life for each Spirit sacrificed this way. You gain that much life.').
card_image_name('devouring greed'/'MM2', 'devouring greed').
card_uid('devouring greed'/'MM2', 'MM2:Devouring Greed:devouring greed').
card_rarity('devouring greed'/'MM2', 'Uncommon').
card_artist('devouring greed'/'MM2', 'Vance Kovacs').
card_number('devouring greed'/'MM2', '78').
card_multiverse_id('devouring greed'/'MM2', '397661').

card_in_set('dimir aqueduct', 'MM2').
card_original_type('dimir aqueduct'/'MM2', 'Land').
card_original_text('dimir aqueduct'/'MM2', 'Dimir Aqueduct enters the battlefield tapped.\nWhen Dimir Aqueduct enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{B} to your mana pool.').
card_image_name('dimir aqueduct'/'MM2', 'dimir aqueduct').
card_uid('dimir aqueduct'/'MM2', 'MM2:Dimir Aqueduct:dimir aqueduct').
card_rarity('dimir aqueduct'/'MM2', 'Uncommon').
card_artist('dimir aqueduct'/'MM2', 'John Avon').
card_number('dimir aqueduct'/'MM2', '239').
card_multiverse_id('dimir aqueduct'/'MM2', '397672').

card_in_set('dimir guildmage', 'MM2').
card_original_type('dimir guildmage'/'MM2', 'Creature — Human Wizard').
card_original_text('dimir guildmage'/'MM2', '{3}{U}: Target player draws a card. Activate this ability only any time you could cast a sorcery.\n{3}{B}: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_image_name('dimir guildmage'/'MM2', 'dimir guildmage').
card_uid('dimir guildmage'/'MM2', 'MM2:Dimir Guildmage:dimir guildmage').
card_rarity('dimir guildmage'/'MM2', 'Uncommon').
card_artist('dimir guildmage'/'MM2', 'Adam Rex').
card_number('dimir guildmage'/'MM2', '192').
card_multiverse_id('dimir guildmage'/'MM2', '397704').

card_in_set('dismember', 'MM2').
card_original_type('dismember'/'MM2', 'Instant').
card_original_text('dismember'/'MM2', '({B/P} can be paid with either {B} or 2 life.)\nTarget creature gets -5/-5 until end of turn.').
card_image_name('dismember'/'MM2', 'dismember').
card_uid('dismember'/'MM2', 'MM2:Dismember:dismember').
card_rarity('dismember'/'MM2', 'Uncommon').
card_artist('dismember'/'MM2', 'Terese Nielsen').
card_number('dismember'/'MM2', '79').
card_flavor_text('dismember'/'MM2', '\"You serve Phyrexia. Your pieces would better serve Phyrexia elsewhere.\"\n—Azax-Azog, the Demon Thane').
card_multiverse_id('dismember'/'MM2', '397830').

card_in_set('dispatch', 'MM2').
card_original_type('dispatch'/'MM2', 'Instant').
card_original_text('dispatch'/'MM2', 'Tap target creature.\nMetalcraft — If you control three or more artifacts, exile that creature.').
card_image_name('dispatch'/'MM2', 'dispatch').
card_uid('dispatch'/'MM2', 'MM2:Dispatch:dispatch').
card_rarity('dispatch'/'MM2', 'Uncommon').
card_artist('dispatch'/'MM2', 'Erica Yang').
card_number('dispatch'/'MM2', '15').
card_flavor_text('dispatch'/'MM2', 'Venser wondered if it could still be called a teleportation spell if the destination is oblivion.').
card_multiverse_id('dispatch'/'MM2', '397781').

card_in_set('dragonsoul knight', 'MM2').
card_original_type('dragonsoul knight'/'MM2', 'Creature — Human Knight').
card_original_text('dragonsoul knight'/'MM2', 'First strike\n{W}{U}{B}{R}{G}: Until end of turn, Dragonsoul Knight becomes a Dragon, gets +5/+3, and gains flying and trample.').
card_image_name('dragonsoul knight'/'MM2', 'dragonsoul knight').
card_uid('dragonsoul knight'/'MM2', 'MM2:Dragonsoul Knight:dragonsoul knight').
card_rarity('dragonsoul knight'/'MM2', 'Common').
card_artist('dragonsoul knight'/'MM2', 'Justin Sweet').
card_number('dragonsoul knight'/'MM2', '112').
card_flavor_text('dragonsoul knight'/'MM2', 'The farther he roamed from Jund and its dragons, the more he felt their essence in himself.').
card_multiverse_id('dragonsoul knight'/'MM2', '397859').

card_in_set('dread drone', 'MM2').
card_original_type('dread drone'/'MM2', 'Creature — Eldrazi Drone').
card_original_text('dread drone'/'MM2', 'When Dread Drone enters the battlefield, put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('dread drone'/'MM2', 'dread drone').
card_uid('dread drone'/'MM2', 'MM2:Dread Drone:dread drone').
card_rarity('dread drone'/'MM2', 'Common').
card_artist('dread drone'/'MM2', 'Raymond Swanland').
card_number('dread drone'/'MM2', '80').
card_flavor_text('dread drone'/'MM2', 'Each Eldrazi ancient spawned a lineage of horrors in unfathomable shapes.').
card_multiverse_id('dread drone'/'MM2', '397833').

card_in_set('drooling groodion', 'MM2').
card_original_type('drooling groodion'/'MM2', 'Creature — Beast').
card_original_text('drooling groodion'/'MM2', '{2}{B}{G}, Sacrifice a creature: Target creature gets +2/+2 until end of turn. Another target creature gets -2/-2 until end of turn.').
card_image_name('drooling groodion'/'MM2', 'drooling groodion').
card_uid('drooling groodion'/'MM2', 'MM2:Drooling Groodion:drooling groodion').
card_rarity('drooling groodion'/'MM2', 'Uncommon').
card_artist('drooling groodion'/'MM2', 'Kev Walker').
card_number('drooling groodion'/'MM2', '173').
card_flavor_text('drooling groodion'/'MM2', '\"The Golgari expand, yes, but I refuse to call their tainted creations ‘growth.\'\"\n—Veszka, Selesnya evangel').
card_multiverse_id('drooling groodion'/'MM2', '397818').

card_in_set('duskhunter bat', 'MM2').
card_original_type('duskhunter bat'/'MM2', 'Creature — Bat').
card_original_text('duskhunter bat'/'MM2', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFlying').
card_image_name('duskhunter bat'/'MM2', 'duskhunter bat').
card_uid('duskhunter bat'/'MM2', 'MM2:Duskhunter Bat:duskhunter bat').
card_rarity('duskhunter bat'/'MM2', 'Common').
card_artist('duskhunter bat'/'MM2', 'Jesper Ejsing').
card_number('duskhunter bat'/'MM2', '81').
card_flavor_text('duskhunter bat'/'MM2', 'The swamps go silent at its approach, but it still hears the heartbeat of the prey within.').
card_multiverse_id('duskhunter bat'/'MM2', '397791').

card_in_set('eldrazi temple', 'MM2').
card_original_type('eldrazi temple'/'MM2', 'Land').
card_original_text('eldrazi temple'/'MM2', '{T}: Add {1} to your mana pool.\n{T}: Add {2} to your mana pool. Spend this mana only to cast colorless Eldrazi spells or activate abilities of colorless Eldrazi.').
card_image_name('eldrazi temple'/'MM2', 'eldrazi temple').
card_uid('eldrazi temple'/'MM2', 'MM2:Eldrazi Temple:eldrazi temple').
card_rarity('eldrazi temple'/'MM2', 'Uncommon').
card_artist('eldrazi temple'/'MM2', 'James Paick').
card_number('eldrazi temple'/'MM2', '240').
card_flavor_text('eldrazi temple'/'MM2', 'Each temple is a door to a horrible future.').
card_multiverse_id('eldrazi temple'/'MM2', '397690').

card_in_set('electrolyze', 'MM2').
card_original_type('electrolyze'/'MM2', 'Instant').
card_original_text('electrolyze'/'MM2', 'Electrolyze deals 2 damage divided as you choose among one or two target creatures and/or players.\nDraw a card.').
card_image_name('electrolyze'/'MM2', 'electrolyze').
card_uid('electrolyze'/'MM2', 'MM2:Electrolyze:electrolyze').
card_rarity('electrolyze'/'MM2', 'Uncommon').
card_artist('electrolyze'/'MM2', 'Zoltan Boros & Gabor Szikszai').
card_number('electrolyze'/'MM2', '174').
card_flavor_text('electrolyze'/'MM2', 'The Izzet learn something from every lesson they teach.').
card_multiverse_id('electrolyze'/'MM2', '397832').

card_in_set('elesh norn, grand cenobite', 'MM2').
card_original_type('elesh norn, grand cenobite'/'MM2', 'Legendary Creature — Praetor').
card_original_text('elesh norn, grand cenobite'/'MM2', 'Vigilance\nOther creatures you control get +2/+2.\nCreatures your opponents control get -2/-2.').
card_image_name('elesh norn, grand cenobite'/'MM2', 'elesh norn, grand cenobite').
card_uid('elesh norn, grand cenobite'/'MM2', 'MM2:Elesh Norn, Grand Cenobite:elesh norn, grand cenobite').
card_rarity('elesh norn, grand cenobite'/'MM2', 'Mythic Rare').
card_artist('elesh norn, grand cenobite'/'MM2', 'Igor Kieryluk').
card_number('elesh norn, grand cenobite'/'MM2', '16').
card_flavor_text('elesh norn, grand cenobite'/'MM2', '\"The Gitaxians whisper among themselves of other worlds. If they exist, we must bring Phyrexia\'s magnificence to them.\"').
card_multiverse_id('elesh norn, grand cenobite'/'MM2', '397880').

card_in_set('emrakul, the aeons torn', 'MM2').
card_original_type('emrakul, the aeons torn'/'MM2', 'Legendary Creature — Eldrazi').
card_original_text('emrakul, the aeons torn'/'MM2', 'Emrakul, the Aeons Torn can\'t be countered.\nWhen you cast Emrakul, take an extra turn after this one.\nFlying, protection from colored spells, annihilator 6\nWhen Emrakul is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_image_name('emrakul, the aeons torn'/'MM2', 'emrakul, the aeons torn').
card_uid('emrakul, the aeons torn'/'MM2', 'MM2:Emrakul, the Aeons Torn:emrakul, the aeons torn').
card_rarity('emrakul, the aeons torn'/'MM2', 'Mythic Rare').
card_artist('emrakul, the aeons torn'/'MM2', 'Mark Tedin').
card_number('emrakul, the aeons torn'/'MM2', '3').
card_multiverse_id('emrakul, the aeons torn'/'MM2', '397905').

card_in_set('endrek sahr, master breeder', 'MM2').
card_original_type('endrek sahr, master breeder'/'MM2', 'Legendary Creature — Human Wizard').
card_original_text('endrek sahr, master breeder'/'MM2', 'Whenever you cast a creature spell, put X 1/1 black Thrull creature tokens onto the battlefield, where X is that spell\'s converted mana cost.\nWhen you control seven or more Thrulls, sacrifice Endrek Sahr, Master Breeder.').
card_image_name('endrek sahr, master breeder'/'MM2', 'endrek sahr, master breeder').
card_uid('endrek sahr, master breeder'/'MM2', 'MM2:Endrek Sahr, Master Breeder:endrek sahr, master breeder').
card_rarity('endrek sahr, master breeder'/'MM2', 'Rare').
card_artist('endrek sahr, master breeder'/'MM2', 'Mark Tedin').
card_number('endrek sahr, master breeder'/'MM2', '82').
card_multiverse_id('endrek sahr, master breeder'/'MM2', '397748').

card_in_set('etched champion', 'MM2').
card_original_type('etched champion'/'MM2', 'Artifact Creature — Soldier').
card_original_text('etched champion'/'MM2', 'Metalcraft — Etched Champion has protection from all colors as long as you control three or more artifacts.').
card_image_name('etched champion'/'MM2', 'etched champion').
card_uid('etched champion'/'MM2', 'MM2:Etched Champion:etched champion').
card_rarity('etched champion'/'MM2', 'Rare').
card_artist('etched champion'/'MM2', 'Igor Kieryluk').
card_number('etched champion'/'MM2', '209').
card_flavor_text('etched champion'/'MM2', 'Its predecessors were etched with the wisdom of ancients; its own etchings bear warnings of a future fraught with war.').
card_multiverse_id('etched champion'/'MM2', '397710').

card_in_set('etched monstrosity', 'MM2').
card_original_type('etched monstrosity'/'MM2', 'Artifact Creature — Golem').
card_original_text('etched monstrosity'/'MM2', 'Etched Monstrosity enters the battlefield with five -1/-1 counters on it.\n{W}{U}{B}{R}{G}, Remove five -1/-1 counters from Etched Monstrosity: Target player draws three cards.').
card_image_name('etched monstrosity'/'MM2', 'etched monstrosity').
card_uid('etched monstrosity'/'MM2', 'MM2:Etched Monstrosity:etched monstrosity').
card_rarity('etched monstrosity'/'MM2', 'Rare').
card_artist('etched monstrosity'/'MM2', 'Steven Belledin').
card_number('etched monstrosity'/'MM2', '210').
card_flavor_text('etched monstrosity'/'MM2', 'Now etched only with the scars of phyresis.').
card_multiverse_id('etched monstrosity'/'MM2', '397872').

card_in_set('etched oracle', 'MM2').
card_original_type('etched oracle'/'MM2', 'Artifact Creature — Wizard').
card_original_text('etched oracle'/'MM2', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)\n{1}, Remove four +1/+1 counters from Etched Oracle: Target player draws three cards.').
card_image_name('etched oracle'/'MM2', 'etched oracle').
card_uid('etched oracle'/'MM2', 'MM2:Etched Oracle:etched oracle').
card_rarity('etched oracle'/'MM2', 'Uncommon').
card_artist('etched oracle'/'MM2', 'Matt Cavotta').
card_number('etched oracle'/'MM2', '211').
card_multiverse_id('etched oracle'/'MM2', '397838').

card_in_set('ethercaste knight', 'MM2').
card_original_type('ethercaste knight'/'MM2', 'Artifact Creature — Human Knight').
card_original_text('ethercaste knight'/'MM2', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_image_name('ethercaste knight'/'MM2', 'ethercaste knight').
card_uid('ethercaste knight'/'MM2', 'MM2:Ethercaste Knight:ethercaste knight').
card_rarity('ethercaste knight'/'MM2', 'Uncommon').
card_artist('ethercaste knight'/'MM2', 'Steven Belledin').
card_number('ethercaste knight'/'MM2', '175').
card_flavor_text('ethercaste knight'/'MM2', '\"We coat ourselves in steel every day. What is etherium but the next logical step?\"').
card_multiverse_id('ethercaste knight'/'MM2', '397889').

card_in_set('everflowing chalice', 'MM2').
card_original_type('everflowing chalice'/'MM2', 'Artifact').
card_original_text('everflowing chalice'/'MM2', 'Multikicker {2} (You may pay an additional {2} any number of times as you cast this spell.)\nEverflowing Chalice enters the battlefield with a charge counter on it for each time it was kicked.\n{T}: Add {1} to your mana pool for each charge counter on Everflowing Chalice.').
card_image_name('everflowing chalice'/'MM2', 'everflowing chalice').
card_uid('everflowing chalice'/'MM2', 'MM2:Everflowing Chalice:everflowing chalice').
card_rarity('everflowing chalice'/'MM2', 'Uncommon').
card_artist('everflowing chalice'/'MM2', 'Steve Argyle').
card_number('everflowing chalice'/'MM2', '212').
card_multiverse_id('everflowing chalice'/'MM2', '397679').

card_in_set('evolving wilds', 'MM2').
card_original_type('evolving wilds'/'MM2', 'Land').
card_original_text('evolving wilds'/'MM2', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'MM2', 'evolving wilds').
card_uid('evolving wilds'/'MM2', 'MM2:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'MM2', 'Common').
card_artist('evolving wilds'/'MM2', 'Steven Belledin').
card_number('evolving wilds'/'MM2', '241').
card_flavor_text('evolving wilds'/'MM2', 'Every world is an organism, able to grow new lands. Some just do it faster than others.').
card_multiverse_id('evolving wilds'/'MM2', '397871').

card_in_set('expedition map', 'MM2').
card_original_type('expedition map'/'MM2', 'Artifact').
card_original_text('expedition map'/'MM2', '{2}, {T}, Sacrifice Expedition Map: Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('expedition map'/'MM2', 'expedition map').
card_uid('expedition map'/'MM2', 'MM2:Expedition Map:expedition map').
card_rarity('expedition map'/'MM2', 'Uncommon').
card_artist('expedition map'/'MM2', 'Franz Vohwinkel').
card_number('expedition map'/'MM2', '213').
card_flavor_text('expedition map'/'MM2', '\"I use maps to find out where explorers have already been. Then I go the other way.\"\n—Javad Nasrin, Ondu relic hunter').
card_multiverse_id('expedition map'/'MM2', '397742').

card_in_set('eye of ugin', 'MM2').
card_original_type('eye of ugin'/'MM2', 'Legendary Land').
card_original_text('eye of ugin'/'MM2', 'Colorless Eldrazi spells you cast cost {2} less to cast.\n{7}, {T}: Search your library for a colorless creature card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('eye of ugin'/'MM2', 'eye of ugin').
card_uid('eye of ugin'/'MM2', 'MM2:Eye of Ugin:eye of ugin').
card_rarity('eye of ugin'/'MM2', 'Rare').
card_artist('eye of ugin'/'MM2', 'James Paick').
card_number('eye of ugin'/'MM2', '242').
card_flavor_text('eye of ugin'/'MM2', 'An eye closes. A race awakens.').
card_multiverse_id('eye of ugin'/'MM2', '397726').

card_in_set('faerie mechanist', 'MM2').
card_original_type('faerie mechanist'/'MM2', 'Artifact Creature — Faerie Artificer').
card_original_text('faerie mechanist'/'MM2', 'Flying\nWhen Faerie Mechanist enters the battlefield, look at the top three cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('faerie mechanist'/'MM2', 'faerie mechanist').
card_uid('faerie mechanist'/'MM2', 'MM2:Faerie Mechanist:faerie mechanist').
card_rarity('faerie mechanist'/'MM2', 'Common').
card_artist('faerie mechanist'/'MM2', 'Matt Cavotta').
card_number('faerie mechanist'/'MM2', '44').
card_multiverse_id('faerie mechanist'/'MM2', '397888').

card_in_set('fiery fall', 'MM2').
card_original_type('fiery fall'/'MM2', 'Instant').
card_original_text('fiery fall'/'MM2', 'Fiery Fall deals 5 damage to target creature.\nBasic landcycling {1}{R} ({1}{R}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('fiery fall'/'MM2', 'fiery fall').
card_uid('fiery fall'/'MM2', 'MM2:Fiery Fall:fiery fall').
card_rarity('fiery fall'/'MM2', 'Common').
card_artist('fiery fall'/'MM2', 'Daarken').
card_number('fiery fall'/'MM2', '113').
card_flavor_text('fiery fall'/'MM2', 'Jund feasts on the unprepared.').
card_multiverse_id('fiery fall'/'MM2', '397692').

card_in_set('flashfreeze', 'MM2').
card_original_type('flashfreeze'/'MM2', 'Instant').
card_original_text('flashfreeze'/'MM2', 'Counter target red or green spell.').
card_image_name('flashfreeze'/'MM2', 'flashfreeze').
card_uid('flashfreeze'/'MM2', 'MM2:Flashfreeze:flashfreeze').
card_rarity('flashfreeze'/'MM2', 'Uncommon').
card_artist('flashfreeze'/'MM2', 'Brian Despain').
card_number('flashfreeze'/'MM2', '45').
card_flavor_text('flashfreeze'/'MM2', '\"Your downfall was not your ignorance, your weakness, or your hubris, but your warm blood.\"\n—Heidar, Rimewind master').
card_multiverse_id('flashfreeze'/'MM2', '397775').

card_in_set('flayer husk', 'MM2').
card_original_type('flayer husk'/'MM2', 'Artifact — Equipment').
card_original_text('flayer husk'/'MM2', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +1/+1.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('flayer husk'/'MM2', 'flayer husk').
card_uid('flayer husk'/'MM2', 'MM2:Flayer Husk:flayer husk').
card_rarity('flayer husk'/'MM2', 'Common').
card_artist('flayer husk'/'MM2', 'Igor Kieryluk').
card_number('flayer husk'/'MM2', '214').
card_multiverse_id('flayer husk'/'MM2', '397826').

card_in_set('fortify', 'MM2').
card_original_type('fortify'/'MM2', 'Instant').
card_original_text('fortify'/'MM2', 'Choose one —\n• Creatures you control get +2/+0 until end of turn.\n• Creatures you control get +0/+2 until end of turn.').
card_image_name('fortify'/'MM2', 'fortify').
card_uid('fortify'/'MM2', 'MM2:Fortify:fortify').
card_rarity('fortify'/'MM2', 'Common').
card_artist('fortify'/'MM2', 'Christopher Moeller').
card_number('fortify'/'MM2', '17').
card_flavor_text('fortify'/'MM2', '\"Where metal is tainted and wood is scarce, we are best armed by faith.\"\n—Tavalus, acolyte of Korlis').
card_multiverse_id('fortify'/'MM2', '397874').

card_in_set('frogmite', 'MM2').
card_original_type('frogmite'/'MM2', 'Artifact Creature — Frog').
card_original_text('frogmite'/'MM2', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_image_name('frogmite'/'MM2', 'frogmite').
card_uid('frogmite'/'MM2', 'MM2:Frogmite:frogmite').
card_rarity('frogmite'/'MM2', 'Common').
card_artist('frogmite'/'MM2', 'Terese Nielsen').
card_number('frogmite'/'MM2', '215').
card_flavor_text('frogmite'/'MM2', 'At first, vedalken observers thought blinkmoths naturally avoided certain places. Then they realized those places were frogmite feeding grounds.').
card_multiverse_id('frogmite'/'MM2', '397893').

card_in_set('fulminator mage', 'MM2').
card_original_type('fulminator mage'/'MM2', 'Creature — Elemental Shaman').
card_original_text('fulminator mage'/'MM2', 'Sacrifice Fulminator Mage: Destroy target nonbasic land.').
card_image_name('fulminator mage'/'MM2', 'fulminator mage').
card_uid('fulminator mage'/'MM2', 'MM2:Fulminator Mage:fulminator mage').
card_rarity('fulminator mage'/'MM2', 'Rare').
card_artist('fulminator mage'/'MM2', 'rk post').
card_number('fulminator mage'/'MM2', '193').
card_flavor_text('fulminator mage'/'MM2', '\"Unsafe Terrain Ahead—Turn Back\"\n—Sign near the former location of Pyrtagh Cairn').
card_multiverse_id('fulminator mage'/'MM2', '397686').

card_in_set('ghost council of orzhova', 'MM2').
card_original_type('ghost council of orzhova'/'MM2', 'Legendary Creature — Spirit').
card_original_text('ghost council of orzhova'/'MM2', 'When Ghost Council of Orzhova enters the battlefield, target opponent loses 1 life and you gain 1 life.\n{1}, Sacrifice a creature: Exile Ghost Council of Orzhova. Return it to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('ghost council of orzhova'/'MM2', 'ghost council of orzhova').
card_uid('ghost council of orzhova'/'MM2', 'MM2:Ghost Council of Orzhova:ghost council of orzhova').
card_rarity('ghost council of orzhova'/'MM2', 'Rare').
card_artist('ghost council of orzhova'/'MM2', 'Greg Staples').
card_number('ghost council of orzhova'/'MM2', '176').
card_multiverse_id('ghost council of orzhova'/'MM2', '397844').

card_in_set('ghostly changeling', 'MM2').
card_original_type('ghostly changeling'/'MM2', 'Creature — Shapeshifter').
card_original_text('ghostly changeling'/'MM2', 'Changeling (This card is every creature type at all times.)\n{1}{B}: Ghostly Changeling gets +1/+1 until end of turn.').
card_image_name('ghostly changeling'/'MM2', 'ghostly changeling').
card_uid('ghostly changeling'/'MM2', 'MM2:Ghostly Changeling:ghostly changeling').
card_rarity('ghostly changeling'/'MM2', 'Common').
card_artist('ghostly changeling'/'MM2', 'Chuck Lukacs').
card_number('ghostly changeling'/'MM2', '83').
card_flavor_text('ghostly changeling'/'MM2', 'In desolate places, changelings may take the shape of fancies, or memories, or fears.').
card_multiverse_id('ghostly changeling'/'MM2', '397705').

card_in_set('glassdust hulk', 'MM2').
card_original_type('glassdust hulk'/'MM2', 'Artifact Creature — Golem').
card_original_text('glassdust hulk'/'MM2', 'Whenever another artifact enters the battlefield under your control, Glassdust Hulk gets +1/+1 until end of turn and can\'t be blocked this turn.\nCycling {W/U} ({W/U}, Discard this card: Draw a card.)').
card_image_name('glassdust hulk'/'MM2', 'glassdust hulk').
card_uid('glassdust hulk'/'MM2', 'MM2:Glassdust Hulk:glassdust hulk').
card_rarity('glassdust hulk'/'MM2', 'Uncommon').
card_artist('glassdust hulk'/'MM2', 'Franz Vohwinkel').
card_number('glassdust hulk'/'MM2', '177').
card_multiverse_id('glassdust hulk'/'MM2', '397807').

card_in_set('glint hawk idol', 'MM2').
card_original_type('glint hawk idol'/'MM2', 'Artifact').
card_original_text('glint hawk idol'/'MM2', 'Whenever another artifact enters the battlefield under your control, you may have Glint Hawk Idol become a 2/2 Bird artifact creature with flying until end of turn.\n{W}: Glint Hawk Idol becomes a 2/2 Bird artifact creature with flying until end of turn.').
card_image_name('glint hawk idol'/'MM2', 'glint hawk idol').
card_uid('glint hawk idol'/'MM2', 'MM2:Glint Hawk Idol:glint hawk idol').
card_rarity('glint hawk idol'/'MM2', 'Common').
card_artist('glint hawk idol'/'MM2', 'Dave Allsop').
card_number('glint hawk idol'/'MM2', '216').
card_multiverse_id('glint hawk idol'/'MM2', '397876').

card_in_set('gnarlid pack', 'MM2').
card_original_type('gnarlid pack'/'MM2', 'Creature — Beast').
card_original_text('gnarlid pack'/'MM2', 'Multikicker {1}{G} (You may pay an additional {1}{G} any number of times as you cast this spell.)\nGnarlid Pack enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_image_name('gnarlid pack'/'MM2', 'gnarlid pack').
card_uid('gnarlid pack'/'MM2', 'MM2:Gnarlid Pack:gnarlid pack').
card_rarity('gnarlid pack'/'MM2', 'Common').
card_artist('gnarlid pack'/'MM2', 'Johann Bodin').
card_number('gnarlid pack'/'MM2', '144').
card_multiverse_id('gnarlid pack'/'MM2', '397863').

card_in_set('goblin fireslinger', 'MM2').
card_original_type('goblin fireslinger'/'MM2', 'Creature — Goblin Warrior').
card_original_text('goblin fireslinger'/'MM2', '{T}: Goblin Fireslinger deals 1 damage to target player.').
card_image_name('goblin fireslinger'/'MM2', 'goblin fireslinger').
card_uid('goblin fireslinger'/'MM2', 'MM2:Goblin Fireslinger:goblin fireslinger').
card_rarity('goblin fireslinger'/'MM2', 'Common').
card_artist('goblin fireslinger'/'MM2', 'Pete Venters').
card_number('goblin fireslinger'/'MM2', '114').
card_flavor_text('goblin fireslinger'/'MM2', 'A rock between your eyes hurts. A burning rock between your eyes ruins your whole day.').
card_multiverse_id('goblin fireslinger'/'MM2', '397850').

card_in_set('goblin war paint', 'MM2').
card_original_type('goblin war paint'/'MM2', 'Enchantment — Aura').
card_original_text('goblin war paint'/'MM2', 'Enchant creature\nEnchanted creature gets +2/+2 and has haste.').
card_image_name('goblin war paint'/'MM2', 'goblin war paint').
card_uid('goblin war paint'/'MM2', 'MM2:Goblin War Paint:goblin war paint').
card_rarity('goblin war paint'/'MM2', 'Common').
card_artist('goblin war paint'/'MM2', 'Austin Hsu').
card_number('goblin war paint'/'MM2', '115').
card_flavor_text('goblin war paint'/'MM2', 'War paint made from kolya fruit heightens senses and lessens fear. Unfortunately, fear is usually what keeps you alive.').
card_multiverse_id('goblin war paint'/'MM2', '397864').

card_in_set('golgari rot farm', 'MM2').
card_original_type('golgari rot farm'/'MM2', 'Land').
card_original_text('golgari rot farm'/'MM2', 'Golgari Rot Farm enters the battlefield tapped.\nWhen Golgari Rot Farm enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{G} to your mana pool.').
card_image_name('golgari rot farm'/'MM2', 'golgari rot farm').
card_uid('golgari rot farm'/'MM2', 'MM2:Golgari Rot Farm:golgari rot farm').
card_rarity('golgari rot farm'/'MM2', 'Uncommon').
card_artist('golgari rot farm'/'MM2', 'John Avon').
card_number('golgari rot farm'/'MM2', '243').
card_multiverse_id('golgari rot farm'/'MM2', '397879').

card_in_set('gorehorn minotaurs', 'MM2').
card_original_type('gorehorn minotaurs'/'MM2', 'Creature — Minotaur Warrior').
card_original_text('gorehorn minotaurs'/'MM2', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)').
card_image_name('gorehorn minotaurs'/'MM2', 'gorehorn minotaurs').
card_uid('gorehorn minotaurs'/'MM2', 'MM2:Gorehorn Minotaurs:gorehorn minotaurs').
card_rarity('gorehorn minotaurs'/'MM2', 'Common').
card_artist('gorehorn minotaurs'/'MM2', 'Wayne Reynolds').
card_number('gorehorn minotaurs'/'MM2', '116').
card_flavor_text('gorehorn minotaurs'/'MM2', 'Some of the eleven minotaur clans of Mirtiin are expert crafters and learned philosophers. Others just like to hit stuff.').
card_multiverse_id('gorehorn minotaurs'/'MM2', '397809').

card_in_set('grim affliction', 'MM2').
card_original_type('grim affliction'/'MM2', 'Instant').
card_original_text('grim affliction'/'MM2', 'Put a -1/-1 counter on target creature, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('grim affliction'/'MM2', 'grim affliction').
card_uid('grim affliction'/'MM2', 'MM2:Grim Affliction:grim affliction').
card_rarity('grim affliction'/'MM2', 'Common').
card_artist('grim affliction'/'MM2', 'Erica Yang').
card_number('grim affliction'/'MM2', '84').
card_flavor_text('grim affliction'/'MM2', 'Even the small wounds let hope bleed out.').
card_multiverse_id('grim affliction'/'MM2', '397899').

card_in_set('gruul turf', 'MM2').
card_original_type('gruul turf'/'MM2', 'Land').
card_original_text('gruul turf'/'MM2', 'Gruul Turf enters the battlefield tapped.\nWhen Gruul Turf enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{G} to your mana pool.').
card_image_name('gruul turf'/'MM2', 'gruul turf').
card_uid('gruul turf'/'MM2', 'MM2:Gruul Turf:gruul turf').
card_rarity('gruul turf'/'MM2', 'Uncommon').
card_artist('gruul turf'/'MM2', 'John Avon').
card_number('gruul turf'/'MM2', '244').
card_multiverse_id('gruul turf'/'MM2', '397689').

card_in_set('guile', 'MM2').
card_original_type('guile'/'MM2', 'Creature — Elemental Incarnation').
card_original_text('guile'/'MM2', 'Guile can\'t be blocked except by three or more creatures.\nIf a spell or ability you control would counter a spell, instead exile that spell and you may play that card without paying its mana cost.\nWhen Guile is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('guile'/'MM2', 'guile').
card_uid('guile'/'MM2', 'MM2:Guile:guile').
card_rarity('guile'/'MM2', 'Rare').
card_artist('guile'/'MM2', 'Zoltan Boros & Gabor Szikszai').
card_number('guile'/'MM2', '46').
card_multiverse_id('guile'/'MM2', '397696').

card_in_set('gust-skimmer', 'MM2').
card_original_type('gust-skimmer'/'MM2', 'Artifact Creature — Insect').
card_original_text('gust-skimmer'/'MM2', '{U}: Gust-Skimmer gains flying until end of turn.').
card_image_name('gust-skimmer'/'MM2', 'gust-skimmer').
card_uid('gust-skimmer'/'MM2', 'MM2:Gust-Skimmer:gust-skimmer').
card_rarity('gust-skimmer'/'MM2', 'Common').
card_artist('gust-skimmer'/'MM2', 'Dan Scott').
card_number('gust-skimmer'/'MM2', '217').
card_flavor_text('gust-skimmer'/'MM2', 'Phyrexian smog clouds choked the skies, threatening creatures who couldn\'t comprehend the menace below.').
card_multiverse_id('gust-skimmer'/'MM2', '397669').

card_in_set('gut shot', 'MM2').
card_original_type('gut shot'/'MM2', 'Instant').
card_original_text('gut shot'/'MM2', '({R/P} can be paid with either {R} or 2 life.)\nGut Shot deals 1 damage to target creature or player.').
card_image_name('gut shot'/'MM2', 'gut shot').
card_uid('gut shot'/'MM2', 'MM2:Gut Shot:gut shot').
card_rarity('gut shot'/'MM2', 'Common').
card_artist('gut shot'/'MM2', 'Greg Staples').
card_number('gut shot'/'MM2', '117').
card_flavor_text('gut shot'/'MM2', '\"Down here, we have a more pointed version of the scriptures.\"\n—Urabrask\'s enforcer').
card_multiverse_id('gut shot'/'MM2', '397673').

card_in_set('hearthfire hobgoblin', 'MM2').
card_original_type('hearthfire hobgoblin'/'MM2', 'Creature — Goblin Soldier').
card_original_text('hearthfire hobgoblin'/'MM2', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_image_name('hearthfire hobgoblin'/'MM2', 'hearthfire hobgoblin').
card_uid('hearthfire hobgoblin'/'MM2', 'MM2:Hearthfire Hobgoblin:hearthfire hobgoblin').
card_rarity('hearthfire hobgoblin'/'MM2', 'Uncommon').
card_artist('hearthfire hobgoblin'/'MM2', 'Steven Belledin').
card_number('hearthfire hobgoblin'/'MM2', '194').
card_flavor_text('hearthfire hobgoblin'/'MM2', 'Hobgoblins are best left alone. They sharpen their farm implements far more than is necessary for their work in the fields.').
card_multiverse_id('hearthfire hobgoblin'/'MM2', '397856').

card_in_set('helium squirter', 'MM2').
card_original_type('helium squirter'/'MM2', 'Creature — Beast Mutant').
card_original_text('helium squirter'/'MM2', 'Graft 3 (This creature enters the battlefield with three +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{1}: Target creature with a +1/+1 counter on it gains flying until end of turn.').
card_image_name('helium squirter'/'MM2', 'helium squirter').
card_uid('helium squirter'/'MM2', 'MM2:Helium Squirter:helium squirter').
card_rarity('helium squirter'/'MM2', 'Common').
card_artist('helium squirter'/'MM2', 'Hideaki Takamura').
card_number('helium squirter'/'MM2', '47').
card_multiverse_id('helium squirter'/'MM2', '397903').

card_in_set('hellkite charger', 'MM2').
card_original_type('hellkite charger'/'MM2', 'Creature — Dragon').
card_original_text('hellkite charger'/'MM2', 'Flying, haste\nWhenever Hellkite Charger attacks, you may pay {5}{R}{R}. If you do, untap all attacking creatures and after this phase, there is an additional combat phase.').
card_image_name('hellkite charger'/'MM2', 'hellkite charger').
card_uid('hellkite charger'/'MM2', 'MM2:Hellkite Charger:hellkite charger').
card_rarity('hellkite charger'/'MM2', 'Rare').
card_artist('hellkite charger'/'MM2', 'Jaime Jones').
card_number('hellkite charger'/'MM2', '118').
card_multiverse_id('hellkite charger'/'MM2', '397761').

card_in_set('hikari, twilight guardian', 'MM2').
card_original_type('hikari, twilight guardian'/'MM2', 'Legendary Creature — Spirit').
card_original_text('hikari, twilight guardian'/'MM2', 'Flying\nWhenever you cast a Spirit or Arcane spell, you may exile Hikari, Twilight Guardian. If you do, return it to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('hikari, twilight guardian'/'MM2', 'hikari, twilight guardian').
card_uid('hikari, twilight guardian'/'MM2', 'MM2:Hikari, Twilight Guardian:hikari, twilight guardian').
card_rarity('hikari, twilight guardian'/'MM2', 'Uncommon').
card_artist('hikari, twilight guardian'/'MM2', 'Glen Angus').
card_number('hikari, twilight guardian'/'MM2', '18').
card_multiverse_id('hikari, twilight guardian'/'MM2', '397790').

card_in_set('horde of notions', 'MM2').
card_original_type('horde of notions'/'MM2', 'Legendary Creature — Elemental').
card_original_text('horde of notions'/'MM2', 'Vigilance, trample, haste\n{W}{U}{B}{R}{G}: You may play target Elemental card from your graveyard without paying its mana cost.').
card_image_name('horde of notions'/'MM2', 'horde of notions').
card_uid('horde of notions'/'MM2', 'MM2:Horde of Notions:horde of notions').
card_rarity('horde of notions'/'MM2', 'Rare').
card_artist('horde of notions'/'MM2', 'Adam Rex').
card_number('horde of notions'/'MM2', '178').
card_flavor_text('horde of notions'/'MM2', 'Even the oldest treefolk was but an acorn when Lorwyn\'s first mysteries were born.').
card_multiverse_id('horde of notions'/'MM2', '397680').

card_in_set('hurkyl\'s recall', 'MM2').
card_original_type('hurkyl\'s recall'/'MM2', 'Instant').
card_original_text('hurkyl\'s recall'/'MM2', 'Return all artifacts target player owns to his or her hand.').
card_image_name('hurkyl\'s recall'/'MM2', 'hurkyl\'s recall').
card_uid('hurkyl\'s recall'/'MM2', 'MM2:Hurkyl\'s Recall:hurkyl\'s recall').
card_rarity('hurkyl\'s recall'/'MM2', 'Rare').
card_artist('hurkyl\'s recall'/'MM2', 'Ralph Horsley').
card_number('hurkyl\'s recall'/'MM2', '48').
card_flavor_text('hurkyl\'s recall'/'MM2', 'Hurkyl\'s research at the College of Lat-Nam wasn\'t enough to stop the two brothers, but for centuries thereafter her spellcraft taught artificers restraint.').
card_multiverse_id('hurkyl\'s recall'/'MM2', '397868').

card_in_set('incandescent soulstoke', 'MM2').
card_original_type('incandescent soulstoke'/'MM2', 'Creature — Elemental Shaman').
card_original_text('incandescent soulstoke'/'MM2', 'Other Elemental creatures you control get +1/+1.\n{1}{R}, {T}: You may put an Elemental creature card from your hand onto the battlefield. That creature gains haste until end of turn. Sacrifice it at the beginning of the next end step.').
card_image_name('incandescent soulstoke'/'MM2', 'incandescent soulstoke').
card_uid('incandescent soulstoke'/'MM2', 'MM2:Incandescent Soulstoke:incandescent soulstoke').
card_rarity('incandescent soulstoke'/'MM2', 'Uncommon').
card_artist('incandescent soulstoke'/'MM2', 'Todd Lockwood').
card_number('incandescent soulstoke'/'MM2', '119').
card_multiverse_id('incandescent soulstoke'/'MM2', '397752').

card_in_set('indomitable archangel', 'MM2').
card_original_type('indomitable archangel'/'MM2', 'Creature — Angel').
card_original_text('indomitable archangel'/'MM2', 'Flying\nMetalcraft — Artifacts you control have shroud as long as you control three or more artifacts. (An artifact with shroud can\'t be the target of spells or abilities.)').
card_image_name('indomitable archangel'/'MM2', 'indomitable archangel').
card_uid('indomitable archangel'/'MM2', 'MM2:Indomitable Archangel:indomitable archangel').
card_rarity('indomitable archangel'/'MM2', 'Rare').
card_artist('indomitable archangel'/'MM2', 'Allen Williams').
card_number('indomitable archangel'/'MM2', '19').
card_flavor_text('indomitable archangel'/'MM2', '\"Every sword drawn to defend is an angelic blade, guided to justice by an angelic hand.\"\n—Auriok proverb').
card_multiverse_id('indomitable archangel'/'MM2', '397727').

card_in_set('inexorable tide', 'MM2').
card_original_type('inexorable tide'/'MM2', 'Enchantment').
card_original_text('inexorable tide'/'MM2', 'Whenever you cast a spell, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('inexorable tide'/'MM2', 'inexorable tide').
card_uid('inexorable tide'/'MM2', 'MM2:Inexorable Tide:inexorable tide').
card_rarity('inexorable tide'/'MM2', 'Rare').
card_artist('inexorable tide'/'MM2', 'Dave Kendall').
card_number('inexorable tide'/'MM2', '49').
card_flavor_text('inexorable tide'/'MM2', '\"See how gratefully this world accepts our blessings.\"\n—Jin-Gitaxias, Core Augur').
card_multiverse_id('inexorable tide'/'MM2', '397762').

card_in_set('inner-flame igniter', 'MM2').
card_original_type('inner-flame igniter'/'MM2', 'Creature — Elemental Warrior').
card_original_text('inner-flame igniter'/'MM2', '{2}{R}: Creatures you control get +1/+0 until end of turn. If this is the third time this ability has resolved this turn, creatures you control gain first strike until end of turn.').
card_image_name('inner-flame igniter'/'MM2', 'inner-flame igniter').
card_uid('inner-flame igniter'/'MM2', 'MM2:Inner-Flame Igniter:inner-flame igniter').
card_rarity('inner-flame igniter'/'MM2', 'Common').
card_artist('inner-flame igniter'/'MM2', 'Scott Hampton').
card_number('inner-flame igniter'/'MM2', '120').
card_flavor_text('inner-flame igniter'/'MM2', 'A light an army can follow.').
card_multiverse_id('inner-flame igniter'/'MM2', '397884').

card_in_set('instill infection', 'MM2').
card_original_type('instill infection'/'MM2', 'Instant').
card_original_text('instill infection'/'MM2', 'Put a -1/-1 counter on target creature.\nDraw a card.').
card_image_name('instill infection'/'MM2', 'instill infection').
card_uid('instill infection'/'MM2', 'MM2:Instill Infection:instill infection').
card_rarity('instill infection'/'MM2', 'Common').
card_artist('instill infection'/'MM2', 'Chris Rahn').
card_number('instill infection'/'MM2', '85').
card_flavor_text('instill infection'/'MM2', '\"The results of our ninth inoculation were the same. The specimen was, of course, euthanized before the contagion could spread.\"\n—Vedalken research notes').
card_multiverse_id('instill infection'/'MM2', '397820').

card_in_set('iona, shield of emeria', 'MM2').
card_original_type('iona, shield of emeria'/'MM2', 'Legendary Creature — Angel').
card_original_text('iona, shield of emeria'/'MM2', 'Flying\nAs Iona, Shield of Emeria enters the battlefield, choose a color.\nYour opponents can\'t cast spells of the chosen color.').
card_image_name('iona, shield of emeria'/'MM2', 'iona, shield of emeria').
card_uid('iona, shield of emeria'/'MM2', 'MM2:Iona, Shield of Emeria:iona, shield of emeria').
card_rarity('iona, shield of emeria'/'MM2', 'Mythic Rare').
card_artist('iona, shield of emeria'/'MM2', 'Jason Chan').
card_number('iona, shield of emeria'/'MM2', '20').
card_flavor_text('iona, shield of emeria'/'MM2', 'No more shall the righteous cower before evil.').
card_multiverse_id('iona, shield of emeria'/'MM2', '397800').

card_in_set('izzet boilerworks', 'MM2').
card_original_type('izzet boilerworks'/'MM2', 'Land').
card_original_text('izzet boilerworks'/'MM2', 'Izzet Boilerworks enters the battlefield tapped.\nWhen Izzet Boilerworks enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{R} to your mana pool.').
card_image_name('izzet boilerworks'/'MM2', 'izzet boilerworks').
card_uid('izzet boilerworks'/'MM2', 'MM2:Izzet Boilerworks:izzet boilerworks').
card_rarity('izzet boilerworks'/'MM2', 'Uncommon').
card_artist('izzet boilerworks'/'MM2', 'John Avon').
card_number('izzet boilerworks'/'MM2', '245').
card_multiverse_id('izzet boilerworks'/'MM2', '397707').

card_in_set('kami of ancient law', 'MM2').
card_original_type('kami of ancient law'/'MM2', 'Creature — Spirit').
card_original_text('kami of ancient law'/'MM2', 'Sacrifice Kami of Ancient Law: Destroy target enchantment.').
card_image_name('kami of ancient law'/'MM2', 'kami of ancient law').
card_uid('kami of ancient law'/'MM2', 'MM2:Kami of Ancient Law:kami of ancient law').
card_rarity('kami of ancient law'/'MM2', 'Common').
card_artist('kami of ancient law'/'MM2', 'Mark Tedin').
card_number('kami of ancient law'/'MM2', '21').
card_flavor_text('kami of ancient law'/'MM2', '\"Duty and law are the foundation on which civilization stands. They must not fall, for when they do, they take everything with them.\"\n—Lord Konda').
card_multiverse_id('kami of ancient law'/'MM2', '397892').

card_in_set('karn liberated', 'MM2').
card_original_type('karn liberated'/'MM2', 'Planeswalker — Karn').
card_original_text('karn liberated'/'MM2', '+4: Target player exiles a card from his or her hand.\n−3: Exile target permanent.\n−14: Restart the game, leaving in exile all non-Aura permanent cards exiled with Karn Liberated. Then put those cards onto the battlefield under your control.').
card_image_name('karn liberated'/'MM2', 'karn liberated').
card_uid('karn liberated'/'MM2', 'MM2:Karn Liberated:karn liberated').
card_rarity('karn liberated'/'MM2', 'Mythic Rare').
card_artist('karn liberated'/'MM2', 'Jason Chan').
card_number('karn liberated'/'MM2', '4').
card_multiverse_id('karn liberated'/'MM2', '397828').

card_in_set('karplusan strider', 'MM2').
card_original_type('karplusan strider'/'MM2', 'Creature — Yeti').
card_original_text('karplusan strider'/'MM2', 'Karplusan Strider can\'t be the target of blue or black spells.').
card_image_name('karplusan strider'/'MM2', 'karplusan strider').
card_uid('karplusan strider'/'MM2', 'MM2:Karplusan Strider:karplusan strider').
card_rarity('karplusan strider'/'MM2', 'Uncommon').
card_artist('karplusan strider'/'MM2', 'Dan Scott').
card_number('karplusan strider'/'MM2', '145').
card_flavor_text('karplusan strider'/'MM2', 'The strider\'s long, loping gait is an adaptation that allows it to move quickly in deep snow.').
card_multiverse_id('karplusan strider'/'MM2', '397846').

card_in_set('kavu primarch', 'MM2').
card_original_type('kavu primarch'/'MM2', 'Creature — Kavu').
card_original_text('kavu primarch'/'MM2', 'Kicker {4} (You may pay an additional {4} as you cast this spell.)\nConvoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nIf Kavu Primarch was kicked, it enters the battlefield with four +1/+1 counters on it.').
card_image_name('kavu primarch'/'MM2', 'kavu primarch').
card_uid('kavu primarch'/'MM2', 'MM2:Kavu Primarch:kavu primarch').
card_rarity('kavu primarch'/'MM2', 'Common').
card_artist('kavu primarch'/'MM2', 'Kev Walker').
card_number('kavu primarch'/'MM2', '146').
card_multiverse_id('kavu primarch'/'MM2', '397843').

card_in_set('kiki-jiki, mirror breaker', 'MM2').
card_original_type('kiki-jiki, mirror breaker'/'MM2', 'Legendary Creature — Goblin Shaman').
card_original_text('kiki-jiki, mirror breaker'/'MM2', 'Haste\n{T}: Put a token that\'s a copy of target nonlegendary creature you control onto the battlefield. That token has haste. Sacrifice it at the beginning of the next end step.').
card_image_name('kiki-jiki, mirror breaker'/'MM2', 'kiki-jiki, mirror breaker').
card_uid('kiki-jiki, mirror breaker'/'MM2', 'MM2:Kiki-Jiki, Mirror Breaker:kiki-jiki, mirror breaker').
card_rarity('kiki-jiki, mirror breaker'/'MM2', 'Mythic Rare').
card_artist('kiki-jiki, mirror breaker'/'MM2', 'Steven Belledin').
card_number('kiki-jiki, mirror breaker'/'MM2', '121').
card_multiverse_id('kiki-jiki, mirror breaker'/'MM2', '397698').

card_in_set('kitesail', 'MM2').
card_original_type('kitesail'/'MM2', 'Artifact — Equipment').
card_original_text('kitesail'/'MM2', 'Equipped creature gets +1/+0 and has flying.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('kitesail'/'MM2', 'kitesail').
card_uid('kitesail'/'MM2', 'MM2:Kitesail:kitesail').
card_rarity('kitesail'/'MM2', 'Common').
card_artist('kitesail'/'MM2', 'Cyril Van Der Haegen').
card_number('kitesail'/'MM2', '218').
card_flavor_text('kitesail'/'MM2', 'Kitesailing is a way of life—and without practice, the end of it.').
card_multiverse_id('kitesail'/'MM2', '397896').

card_in_set('kor duelist', 'MM2').
card_original_type('kor duelist'/'MM2', 'Creature — Kor Soldier').
card_original_text('kor duelist'/'MM2', 'As long as Kor Duelist is equipped, it has double strike. (It deals both first-strike and regular combat damage.)').
card_image_name('kor duelist'/'MM2', 'kor duelist').
card_uid('kor duelist'/'MM2', 'MM2:Kor Duelist:kor duelist').
card_rarity('kor duelist'/'MM2', 'Uncommon').
card_artist('kor duelist'/'MM2', 'Izzy').
card_number('kor duelist'/'MM2', '22').
card_flavor_text('kor duelist'/'MM2', '\"Swords cannot reach far enough. Chains cannot strike hard enough. An eternal dilemma, but a simple one.\"').
card_multiverse_id('kor duelist'/'MM2', '397675').

card_in_set('kozilek\'s predator', 'MM2').
card_original_type('kozilek\'s predator'/'MM2', 'Creature — Eldrazi Drone').
card_original_text('kozilek\'s predator'/'MM2', 'When Kozilek\'s Predator enters the battlefield, put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('kozilek\'s predator'/'MM2', 'kozilek\'s predator').
card_uid('kozilek\'s predator'/'MM2', 'MM2:Kozilek\'s Predator:kozilek\'s predator').
card_rarity('kozilek\'s predator'/'MM2', 'Common').
card_artist('kozilek\'s predator'/'MM2', 'Steve Argyle').
card_number('kozilek\'s predator'/'MM2', '147').
card_flavor_text('kozilek\'s predator'/'MM2', 'It\'s difficult to outwit something that doesn\'t speak, strategize, or even think.').
card_multiverse_id('kozilek\'s predator'/'MM2', '397716').

card_in_set('kozilek, butcher of truth', 'MM2').
card_original_type('kozilek, butcher of truth'/'MM2', 'Legendary Creature — Eldrazi').
card_original_text('kozilek, butcher of truth'/'MM2', 'When you cast Kozilek, Butcher of Truth, draw four cards.\nAnnihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.)\nWhen Kozilek is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_image_name('kozilek, butcher of truth'/'MM2', 'kozilek, butcher of truth').
card_uid('kozilek, butcher of truth'/'MM2', 'MM2:Kozilek, Butcher of Truth:kozilek, butcher of truth').
card_rarity('kozilek, butcher of truth'/'MM2', 'Mythic Rare').
card_artist('kozilek, butcher of truth'/'MM2', 'Michael Komarck').
card_number('kozilek, butcher of truth'/'MM2', '5').
card_multiverse_id('kozilek, butcher of truth'/'MM2', '397668').

card_in_set('leyline of sanctity', 'MM2').
card_original_type('leyline of sanctity'/'MM2', 'Enchantment').
card_original_text('leyline of sanctity'/'MM2', 'If Leyline of Sanctity is in your opening hand, you may begin the game with it on the battlefield.\nYou have hexproof. (You can\'t be the target of spells or abilities your opponents control.)').
card_image_name('leyline of sanctity'/'MM2', 'leyline of sanctity').
card_uid('leyline of sanctity'/'MM2', 'MM2:Leyline of Sanctity:leyline of sanctity').
card_rarity('leyline of sanctity'/'MM2', 'Rare').
card_artist('leyline of sanctity'/'MM2', 'Ryan Pancoast').
card_number('leyline of sanctity'/'MM2', '23').
card_multiverse_id('leyline of sanctity'/'MM2', '397677').

card_in_set('lightning bolt', 'MM2').
card_original_type('lightning bolt'/'MM2', 'Instant').
card_original_text('lightning bolt'/'MM2', 'Lightning Bolt deals 3 damage to target creature or player.').
card_image_name('lightning bolt'/'MM2', 'lightning bolt').
card_uid('lightning bolt'/'MM2', 'MM2:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'MM2', 'Uncommon').
card_artist('lightning bolt'/'MM2', 'Christopher Moeller').
card_number('lightning bolt'/'MM2', '122').
card_flavor_text('lightning bolt'/'MM2', 'The sparkmage shrieked, calling on the rage of the storms of his youth. To his surprise, the sky responded with a fierce energy he\'d never thought to see again.').
card_multiverse_id('lightning bolt'/'MM2', '397722').

card_in_set('lodestone golem', 'MM2').
card_original_type('lodestone golem'/'MM2', 'Artifact Creature — Golem').
card_original_text('lodestone golem'/'MM2', 'Nonartifact spells cost {1} more to cast.').
card_image_name('lodestone golem'/'MM2', 'lodestone golem').
card_uid('lodestone golem'/'MM2', 'MM2:Lodestone Golem:lodestone golem').
card_rarity('lodestone golem'/'MM2', 'Rare').
card_artist('lodestone golem'/'MM2', 'Chris Rahn').
card_number('lodestone golem'/'MM2', '219').
card_flavor_text('lodestone golem'/'MM2', '\"Somehow it warps the Æther. It brings a strange weight, a blockade in the flow of spellcraft.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('lodestone golem'/'MM2', '397736').

card_in_set('lodestone myr', 'MM2').
card_original_type('lodestone myr'/'MM2', 'Artifact Creature — Myr').
card_original_text('lodestone myr'/'MM2', 'Trample\nTap an untapped artifact you control: Lodestone Myr gets +1/+1 until end of turn.').
card_image_name('lodestone myr'/'MM2', 'lodestone myr').
card_uid('lodestone myr'/'MM2', 'MM2:Lodestone Myr:lodestone myr').
card_rarity('lodestone myr'/'MM2', 'Rare').
card_artist('lodestone myr'/'MM2', 'Greg Staples').
card_number('lodestone myr'/'MM2', '220').
card_flavor_text('lodestone myr'/'MM2', 'When necessary, myr can override and control any artificial object, as can their creator.').
card_multiverse_id('lodestone myr'/'MM2', '397769').

card_in_set('long-forgotten gohei', 'MM2').
card_original_type('long-forgotten gohei'/'MM2', 'Artifact').
card_original_text('long-forgotten gohei'/'MM2', 'Arcane spells you cast cost {1} less to cast.\nSpirit creatures you control get +1/+1.').
card_image_name('long-forgotten gohei'/'MM2', 'long-forgotten gohei').
card_uid('long-forgotten gohei'/'MM2', 'MM2:Long-Forgotten Gohei:long-forgotten gohei').
card_rarity('long-forgotten gohei'/'MM2', 'Rare').
card_artist('long-forgotten gohei'/'MM2', 'Alan Pollack').
card_number('long-forgotten gohei'/'MM2', '221').
card_flavor_text('long-forgotten gohei'/'MM2', 'Long ago, the priests would wave the gohei to call down the gods. Now it lies forgotten, but the spirits still feel its pull.').
card_multiverse_id('long-forgotten gohei'/'MM2', '397711').

card_in_set('lorescale coatl', 'MM2').
card_original_type('lorescale coatl'/'MM2', 'Creature — Snake').
card_original_text('lorescale coatl'/'MM2', 'Whenever you draw a card, you may put a +1/+1 counter on Lorescale Coatl.').
card_image_name('lorescale coatl'/'MM2', 'lorescale coatl').
card_uid('lorescale coatl'/'MM2', 'MM2:Lorescale Coatl:lorescale coatl').
card_rarity('lorescale coatl'/'MM2', 'Uncommon').
card_artist('lorescale coatl'/'MM2', 'Greg Staples').
card_number('lorescale coatl'/'MM2', '179').
card_flavor_text('lorescale coatl'/'MM2', '\"The enlightenment I never found in etherium I have found traced in the coatl\'s scales.\"\n—Ranalus, vedalken heretic').
card_multiverse_id('lorescale coatl'/'MM2', '397836').

card_in_set('mana leak', 'MM2').
card_original_type('mana leak'/'MM2', 'Instant').
card_original_text('mana leak'/'MM2', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'MM2', 'mana leak').
card_uid('mana leak'/'MM2', 'MM2:Mana Leak:mana leak').
card_rarity('mana leak'/'MM2', 'Common').
card_artist('mana leak'/'MM2', 'Howard Lyon').
card_number('mana leak'/'MM2', '50').
card_flavor_text('mana leak'/'MM2', 'The fatal flaw in every plan is the assumption that you know more than your enemy.').
card_multiverse_id('mana leak'/'MM2', '397773').

card_in_set('matca rioters', 'MM2').
card_original_type('matca rioters'/'MM2', 'Creature — Human Warrior').
card_original_text('matca rioters'/'MM2', 'Domain — Matca Rioters\'s power and toughness are each equal to the number of basic land types among lands you control.').
card_image_name('matca rioters'/'MM2', 'matca rioters').
card_uid('matca rioters'/'MM2', 'MM2:Matca Rioters:matca rioters').
card_rarity('matca rioters'/'MM2', 'Common').
card_artist('matca rioters'/'MM2', 'Steve Argyle').
card_number('matca rioters'/'MM2', '148').
card_flavor_text('matca rioters'/'MM2', 'When outsiders interrupted the matca championship, things got ugly.').
card_multiverse_id('matca rioters'/'MM2', '397886').

card_in_set('midnight banshee', 'MM2').
card_original_type('midnight banshee'/'MM2', 'Creature — Spirit').
card_original_text('midnight banshee'/'MM2', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nAt the beginning of your upkeep, put a -1/-1 counter on each nonblack creature.').
card_image_name('midnight banshee'/'MM2', 'midnight banshee').
card_uid('midnight banshee'/'MM2', 'MM2:Midnight Banshee:midnight banshee').
card_rarity('midnight banshee'/'MM2', 'Rare').
card_artist('midnight banshee'/'MM2', 'Daarken').
card_number('midnight banshee'/'MM2', '86').
card_flavor_text('midnight banshee'/'MM2', 'Many have heard the beginning of its low, sustained shriek but few the end.').
card_multiverse_id('midnight banshee'/'MM2', '397799').

card_in_set('mighty leap', 'MM2').
card_original_type('mighty leap'/'MM2', 'Instant').
card_original_text('mighty leap'/'MM2', 'Target creature gets +2/+2 and gains flying until end of turn.').
card_image_name('mighty leap'/'MM2', 'mighty leap').
card_uid('mighty leap'/'MM2', 'MM2:Mighty Leap:mighty leap').
card_rarity('mighty leap'/'MM2', 'Common').
card_artist('mighty leap'/'MM2', 'rk post').
card_number('mighty leap'/'MM2', '24').
card_flavor_text('mighty leap'/'MM2', '\"The southern fortress taken by invaders? Heh, sure . . . when elephants fly.\"\n—Brezard Skeinbow, captain of the guard').
card_multiverse_id('mighty leap'/'MM2', '397681').

card_in_set('mirran crusader', 'MM2').
card_original_type('mirran crusader'/'MM2', 'Creature — Human Knight').
card_original_text('mirran crusader'/'MM2', 'Double strike, protection from black and from green').
card_image_name('mirran crusader'/'MM2', 'mirran crusader').
card_uid('mirran crusader'/'MM2', 'MM2:Mirran Crusader:mirran crusader').
card_rarity('mirran crusader'/'MM2', 'Rare').
card_artist('mirran crusader'/'MM2', 'Eric Deschamps').
card_number('mirran crusader'/'MM2', '25').
card_flavor_text('mirran crusader'/'MM2', 'A symbol of what Mirrodin once was and hope for what it will be again.').
card_multiverse_id('mirran crusader'/'MM2', '397737').

card_in_set('mirror entity', 'MM2').
card_original_type('mirror entity'/'MM2', 'Creature — Shapeshifter').
card_original_text('mirror entity'/'MM2', 'Changeling (This card is every creature type at all times.)\n{X}: Until end of turn, creatures you control have base power and toughness X/X and gain all creature types.').
card_image_name('mirror entity'/'MM2', 'mirror entity').
card_uid('mirror entity'/'MM2', 'MM2:Mirror Entity:mirror entity').
card_rarity('mirror entity'/'MM2', 'Rare').
card_artist('mirror entity'/'MM2', 'Zoltan Boros & Gabor Szikszai').
card_number('mirror entity'/'MM2', '26').
card_flavor_text('mirror entity'/'MM2', 'Unaware of Lorwyn\'s diversity, it sees only itself, reflected a thousand times over.').
card_multiverse_id('mirror entity'/'MM2', '397806').

card_in_set('moonlit strider', 'MM2').
card_original_type('moonlit strider'/'MM2', 'Creature — Spirit').
card_original_text('moonlit strider'/'MM2', 'Sacrifice Moonlit Strider: Target creature you control gains protection from the color of your choice until end of turn.\nSoulshift 3 (When this creature dies, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_image_name('moonlit strider'/'MM2', 'moonlit strider').
card_uid('moonlit strider'/'MM2', 'MM2:Moonlit Strider:moonlit strider').
card_rarity('moonlit strider'/'MM2', 'Common').
card_artist('moonlit strider'/'MM2', 'John Avon').
card_number('moonlit strider'/'MM2', '27').
card_multiverse_id('moonlit strider'/'MM2', '397803').

card_in_set('mortarpod', 'MM2').
card_original_type('mortarpod'/'MM2', 'Artifact — Equipment').
card_original_text('mortarpod'/'MM2', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +0/+1 and has \"Sacrifice this creature: This creature deals 1 damage to target creature or player.\"\nEquip {2}').
card_image_name('mortarpod'/'MM2', 'mortarpod').
card_uid('mortarpod'/'MM2', 'MM2:Mortarpod:mortarpod').
card_rarity('mortarpod'/'MM2', 'Uncommon').
card_artist('mortarpod'/'MM2', 'Eric Deschamps').
card_number('mortarpod'/'MM2', '222').
card_multiverse_id('mortarpod'/'MM2', '397797').

card_in_set('mox opal', 'MM2').
card_original_type('mox opal'/'MM2', 'Legendary Artifact').
card_original_text('mox opal'/'MM2', 'Metalcraft — {T}: Add one mana of any color to your mana pool. Activate this ability only if you control three or more artifacts.').
card_image_name('mox opal'/'MM2', 'mox opal').
card_uid('mox opal'/'MM2', 'MM2:Mox Opal:mox opal').
card_rarity('mox opal'/'MM2', 'Mythic Rare').
card_artist('mox opal'/'MM2', 'Volkan Baga').
card_number('mox opal'/'MM2', '223').
card_flavor_text('mox opal'/'MM2', 'The suns of Mirrodin have shone upon perfection only once.').
card_multiverse_id('mox opal'/'MM2', '397719').

card_in_set('mulldrifter', 'MM2').
card_original_type('mulldrifter'/'MM2', 'Creature — Elemental').
card_original_text('mulldrifter'/'MM2', 'Flying\nWhen Mulldrifter enters the battlefield, draw two cards.\nEvoke {2}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('mulldrifter'/'MM2', 'mulldrifter').
card_uid('mulldrifter'/'MM2', 'MM2:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'MM2', 'Uncommon').
card_artist('mulldrifter'/'MM2', 'Eric Fortune').
card_number('mulldrifter'/'MM2', '51').
card_multiverse_id('mulldrifter'/'MM2', '397764').

card_in_set('mutagenic growth', 'MM2').
card_original_type('mutagenic growth'/'MM2', 'Instant').
card_original_text('mutagenic growth'/'MM2', '({G/P} can be paid with either {G} or 2 life.)\nTarget creature gets +2/+2 until end of turn.').
card_image_name('mutagenic growth'/'MM2', 'mutagenic growth').
card_uid('mutagenic growth'/'MM2', 'MM2:Mutagenic Growth:mutagenic growth').
card_rarity('mutagenic growth'/'MM2', 'Uncommon').
card_artist('mutagenic growth'/'MM2', 'Dave Kendall').
card_number('mutagenic growth'/'MM2', '149').
card_flavor_text('mutagenic growth'/'MM2', '\"Sympathy is for weaklings. Whoever survives, wins.\"\n—Benzir, archdruid of Temple Might').
card_multiverse_id('mutagenic growth'/'MM2', '397717').

card_in_set('myr enforcer', 'MM2').
card_original_type('myr enforcer'/'MM2', 'Artifact Creature — Myr').
card_original_text('myr enforcer'/'MM2', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_image_name('myr enforcer'/'MM2', 'myr enforcer').
card_uid('myr enforcer'/'MM2', 'MM2:Myr Enforcer:myr enforcer').
card_rarity('myr enforcer'/'MM2', 'Common').
card_artist('myr enforcer'/'MM2', 'Jim Murray').
card_number('myr enforcer'/'MM2', '224').
card_flavor_text('myr enforcer'/'MM2', 'Most myr monitor other species. Some myr monitor other myr.').
card_multiverse_id('myr enforcer'/'MM2', '397865').

card_in_set('myrsmith', 'MM2').
card_original_type('myrsmith'/'MM2', 'Creature — Human Artificer').
card_original_text('myrsmith'/'MM2', 'Whenever you cast an artifact spell, you may pay {1}. If you do, put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_image_name('myrsmith'/'MM2', 'myrsmith').
card_uid('myrsmith'/'MM2', 'MM2:Myrsmith:myrsmith').
card_rarity('myrsmith'/'MM2', 'Uncommon').
card_artist('myrsmith'/'MM2', 'Eric Deschamps').
card_number('myrsmith'/'MM2', '28').
card_flavor_text('myrsmith'/'MM2', 'The Auriok see the artificer as a conduit, beckoning new creations into the world.').
card_multiverse_id('myrsmith'/'MM2', '397779').

card_in_set('mystic snake', 'MM2').
card_original_type('mystic snake'/'MM2', 'Creature — Snake').
card_original_text('mystic snake'/'MM2', 'Flash\nWhen Mystic Snake enters the battlefield, counter target spell.').
card_image_name('mystic snake'/'MM2', 'mystic snake').
card_uid('mystic snake'/'MM2', 'MM2:Mystic Snake:mystic snake').
card_rarity('mystic snake'/'MM2', 'Rare').
card_artist('mystic snake'/'MM2', 'Daren Bader').
card_number('mystic snake'/'MM2', '180').
card_flavor_text('mystic snake'/'MM2', 'Its fangs are in your flesh before its hiss leaves your ears.').
card_multiverse_id('mystic snake'/'MM2', '397714').

card_in_set('nameless inversion', 'MM2').
card_original_type('nameless inversion'/'MM2', 'Tribal Instant — Shapeshifter').
card_original_text('nameless inversion'/'MM2', 'Changeling (This card is every creature type at all times.)\nTarget creature gets +3/-3 and loses all creature types until end of turn.').
card_image_name('nameless inversion'/'MM2', 'nameless inversion').
card_uid('nameless inversion'/'MM2', 'MM2:Nameless Inversion:nameless inversion').
card_rarity('nameless inversion'/'MM2', 'Common').
card_artist('nameless inversion'/'MM2', 'Jeff Miracola').
card_number('nameless inversion'/'MM2', '87').
card_flavor_text('nameless inversion'/'MM2', 'Just as a changeling\'s influence can have dramatic effects, so too can its sudden withdrawal.').
card_multiverse_id('nameless inversion'/'MM2', '397861').

card_in_set('narcolepsy', 'MM2').
card_original_type('narcolepsy'/'MM2', 'Enchantment — Aura').
card_original_text('narcolepsy'/'MM2', 'Enchant creature\nAt the beginning of each upkeep, if enchanted creature is untapped, tap it.').
card_image_name('narcolepsy'/'MM2', 'narcolepsy').
card_uid('narcolepsy'/'MM2', 'MM2:Narcolepsy:narcolepsy').
card_rarity('narcolepsy'/'MM2', 'Common').
card_artist('narcolepsy'/'MM2', 'Johann Bodin').
card_number('narcolepsy'/'MM2', '52').
card_flavor_text('narcolepsy'/'MM2', '\"It\'s so cute when it\'s sleeping, isn\'t it? Actually, it\'s still as abhorrent as ever, but at least it\'s not trying to kill us.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('narcolepsy'/'MM2', '397802').

card_in_set('necrogenesis', 'MM2').
card_original_type('necrogenesis'/'MM2', 'Enchantment').
card_original_text('necrogenesis'/'MM2', '{2}: Exile target creature card from a graveyard. Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('necrogenesis'/'MM2', 'necrogenesis').
card_uid('necrogenesis'/'MM2', 'MM2:Necrogenesis:necrogenesis').
card_rarity('necrogenesis'/'MM2', 'Uncommon').
card_artist('necrogenesis'/'MM2', 'Trevor Claxton').
card_number('necrogenesis'/'MM2', '181').
card_flavor_text('necrogenesis'/'MM2', '\"Those may be the squirms of one life ending or of another beginning. Either way, I\'d leave it alone.\"\n—Rakka Mar').
card_multiverse_id('necrogenesis'/'MM2', '397745').

card_in_set('necroskitter', 'MM2').
card_original_type('necroskitter'/'MM2', 'Creature — Elemental').
card_original_text('necroskitter'/'MM2', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever a creature an opponent controls with a -1/-1 counter on it dies, you may return that card to the battlefield under your control.').
card_image_name('necroskitter'/'MM2', 'necroskitter').
card_uid('necroskitter'/'MM2', 'MM2:Necroskitter:necroskitter').
card_rarity('necroskitter'/'MM2', 'Rare').
card_artist('necroskitter'/'MM2', 'Jaime Jones').
card_number('necroskitter'/'MM2', '88').
card_multiverse_id('necroskitter'/'MM2', '397683').

card_in_set('nest invader', 'MM2').
card_original_type('nest invader'/'MM2', 'Creature — Eldrazi Drone').
card_original_text('nest invader'/'MM2', 'When Nest Invader enters the battlefield, put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('nest invader'/'MM2', 'nest invader').
card_uid('nest invader'/'MM2', 'MM2:Nest Invader:nest invader').
card_rarity('nest invader'/'MM2', 'Common').
card_artist('nest invader'/'MM2', 'Trevor Claxton').
card_number('nest invader'/'MM2', '150').
card_flavor_text('nest invader'/'MM2', 'It nurtures its masters\' glorious future.').
card_multiverse_id('nest invader'/'MM2', '397901').

card_in_set('niv-mizzet, the firemind', 'MM2').
card_original_type('niv-mizzet, the firemind'/'MM2', 'Legendary Creature — Dragon Wizard').
card_original_text('niv-mizzet, the firemind'/'MM2', 'Flying\nWhenever you draw a card, Niv-Mizzet, the Firemind deals 1 damage to target creature or player.\n{T}: Draw a card.').
card_image_name('niv-mizzet, the firemind'/'MM2', 'niv-mizzet, the firemind').
card_uid('niv-mizzet, the firemind'/'MM2', 'MM2:Niv-Mizzet, the Firemind:niv-mizzet, the firemind').
card_rarity('niv-mizzet, the firemind'/'MM2', 'Rare').
card_artist('niv-mizzet, the firemind'/'MM2', 'Todd Lockwood').
card_number('niv-mizzet, the firemind'/'MM2', '182').
card_multiverse_id('niv-mizzet, the firemind'/'MM2', '397851').

card_in_set('nobilis of war', 'MM2').
card_original_type('nobilis of war'/'MM2', 'Creature — Spirit Avatar').
card_original_text('nobilis of war'/'MM2', 'Flying\nAttacking creatures you control get +2/+0.').
card_image_name('nobilis of war'/'MM2', 'nobilis of war').
card_uid('nobilis of war'/'MM2', 'MM2:Nobilis of War:nobilis of war').
card_rarity('nobilis of war'/'MM2', 'Rare').
card_artist('nobilis of war'/'MM2', 'Christopher Moeller').
card_number('nobilis of war'/'MM2', '195').
card_flavor_text('nobilis of war'/'MM2', '\"A great siege is a banquet to him; a long and terrible battle, the most exquisite delicacy.\"\n—The Seer\'s Parables').
card_multiverse_id('nobilis of war'/'MM2', '397733').

card_in_set('noble hierarch', 'MM2').
card_original_type('noble hierarch'/'MM2', 'Creature — Human Druid').
card_original_text('noble hierarch'/'MM2', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{T}: Add {G}, {W}, or {U} to your mana pool.').
card_image_name('noble hierarch'/'MM2', 'noble hierarch').
card_uid('noble hierarch'/'MM2', 'MM2:Noble Hierarch:noble hierarch').
card_rarity('noble hierarch'/'MM2', 'Rare').
card_artist('noble hierarch'/'MM2', 'Mark Zug').
card_number('noble hierarch'/'MM2', '151').
card_flavor_text('noble hierarch'/'MM2', 'She protects the sacred groves from blight, drought, and the Unbeholden.').
card_multiverse_id('noble hierarch'/'MM2', '397709').

card_in_set('novijen sages', 'MM2').
card_original_type('novijen sages'/'MM2', 'Creature — Human Advisor Mutant').
card_original_text('novijen sages'/'MM2', 'Graft 4 (This creature enters the battlefield with four +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{1}, Remove two +1/+1 counters from among creatures you control: Draw a card.').
card_image_name('novijen sages'/'MM2', 'novijen sages').
card_uid('novijen sages'/'MM2', 'MM2:Novijen Sages:novijen sages').
card_rarity('novijen sages'/'MM2', 'Uncommon').
card_artist('novijen sages'/'MM2', 'Luca Zontini').
card_number('novijen sages'/'MM2', '53').
card_multiverse_id('novijen sages'/'MM2', '397663').

card_in_set('oblivion ring', 'MM2').
card_original_type('oblivion ring'/'MM2', 'Enchantment').
card_original_text('oblivion ring'/'MM2', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'MM2', 'oblivion ring').
card_uid('oblivion ring'/'MM2', 'MM2:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'MM2', 'Uncommon').
card_artist('oblivion ring'/'MM2', 'Franz Vohwinkel').
card_number('oblivion ring'/'MM2', '29').
card_multiverse_id('oblivion ring'/'MM2', '397760').

card_in_set('orzhov basilica', 'MM2').
card_original_type('orzhov basilica'/'MM2', 'Land').
card_original_text('orzhov basilica'/'MM2', 'Orzhov Basilica enters the battlefield tapped.\nWhen Orzhov Basilica enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {W}{B} to your mana pool.').
card_image_name('orzhov basilica'/'MM2', 'orzhov basilica').
card_uid('orzhov basilica'/'MM2', 'MM2:Orzhov Basilica:orzhov basilica').
card_rarity('orzhov basilica'/'MM2', 'Uncommon').
card_artist('orzhov basilica'/'MM2', 'John Avon').
card_number('orzhov basilica'/'MM2', '246').
card_multiverse_id('orzhov basilica'/'MM2', '397902').

card_in_set('otherworldly journey', 'MM2').
card_original_type('otherworldly journey'/'MM2', 'Instant — Arcane').
card_original_text('otherworldly journey'/'MM2', 'Exile target creature. At the beginning of the next end step, return that card to the battlefield under its owner\'s control with a +1/+1 counter on it.').
card_image_name('otherworldly journey'/'MM2', 'otherworldly journey').
card_uid('otherworldly journey'/'MM2', 'MM2:Otherworldly Journey:otherworldly journey').
card_rarity('otherworldly journey'/'MM2', 'Common').
card_artist('otherworldly journey'/'MM2', 'Vance Kovacs').
card_number('otherworldly journey'/'MM2', '30').
card_flavor_text('otherworldly journey'/'MM2', '\"The landscape shimmered and I felt a chill breeze. When my vision cleared, I found myself alone among the corpses of my fallen friends.\"\n—Journal found in Numai').
card_multiverse_id('otherworldly journey'/'MM2', '397855').

card_in_set('overwhelm', 'MM2').
card_original_type('overwhelm'/'MM2', 'Sorcery').
card_original_text('overwhelm'/'MM2', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nCreatures you control get +3/+3 until end of turn.').
card_image_name('overwhelm'/'MM2', 'overwhelm').
card_uid('overwhelm'/'MM2', 'MM2:Overwhelm:overwhelm').
card_rarity('overwhelm'/'MM2', 'Uncommon').
card_artist('overwhelm'/'MM2', 'Wayne Reynolds').
card_number('overwhelm'/'MM2', '152').
card_flavor_text('overwhelm'/'MM2', '\"Let the song of Selesnya be heard above the rhythm of our thundering hordes!\"').
card_multiverse_id('overwhelm'/'MM2', '397693').

card_in_set('overwhelming stampede', 'MM2').
card_original_type('overwhelming stampede'/'MM2', 'Sorcery').
card_original_text('overwhelming stampede'/'MM2', 'Until end of turn, creatures you control gain trample and get +X/+X, where X is the greatest power among creatures you control.').
card_image_name('overwhelming stampede'/'MM2', 'overwhelming stampede').
card_uid('overwhelming stampede'/'MM2', 'MM2:Overwhelming Stampede:overwhelming stampede').
card_rarity('overwhelming stampede'/'MM2', 'Rare').
card_artist('overwhelming stampede'/'MM2', 'Steven Belledin').
card_number('overwhelming stampede'/'MM2', '153').
card_flavor_text('overwhelming stampede'/'MM2', '\"I trust the instincts of any beast over the advice of any human who presumes to be an expert in the art of war.\"\n—Rinshel, elf insurgent').
card_multiverse_id('overwhelming stampede'/'MM2', '397788').

card_in_set('pelakka wurm', 'MM2').
card_original_type('pelakka wurm'/'MM2', 'Creature — Wurm').
card_original_text('pelakka wurm'/'MM2', 'Trample\nWhen Pelakka Wurm enters the battlefield, you gain 7 life.\nWhen Pelakka Wurm dies, draw a card.').
card_image_name('pelakka wurm'/'MM2', 'pelakka wurm').
card_uid('pelakka wurm'/'MM2', 'MM2:Pelakka Wurm:pelakka wurm').
card_rarity('pelakka wurm'/'MM2', 'Uncommon').
card_artist('pelakka wurm'/'MM2', 'Daniel Ljunggren').
card_number('pelakka wurm'/'MM2', '154').
card_flavor_text('pelakka wurm'/'MM2', 'It eats what it wants to eat—which is anything that moves.').
card_multiverse_id('pelakka wurm'/'MM2', '397763').

card_in_set('pillory of the sleepless', 'MM2').
card_original_type('pillory of the sleepless'/'MM2', 'Enchantment — Aura').
card_original_text('pillory of the sleepless'/'MM2', 'Enchant creature\nEnchanted creature can\'t attack or block.\nEnchanted creature has \"At the beginning of your upkeep, you lose 1 life.\"').
card_image_name('pillory of the sleepless'/'MM2', 'pillory of the sleepless').
card_uid('pillory of the sleepless'/'MM2', 'MM2:Pillory of the Sleepless:pillory of the sleepless').
card_rarity('pillory of the sleepless'/'MM2', 'Uncommon').
card_artist('pillory of the sleepless'/'MM2', 'Marc Simonetti').
card_number('pillory of the sleepless'/'MM2', '183').
card_flavor_text('pillory of the sleepless'/'MM2', 'Which is worse—the sleep which never ends or that which never comes?').
card_multiverse_id('pillory of the sleepless'/'MM2', '397890').

card_in_set('plagued rusalka', 'MM2').
card_original_type('plagued rusalka'/'MM2', 'Creature — Spirit').
card_original_text('plagued rusalka'/'MM2', '{B}, Sacrifice a creature: Target creature gets -1/-1 until end of turn.').
card_image_name('plagued rusalka'/'MM2', 'plagued rusalka').
card_uid('plagued rusalka'/'MM2', 'MM2:Plagued Rusalka:plagued rusalka').
card_rarity('plagued rusalka'/'MM2', 'Common').
card_artist('plagued rusalka'/'MM2', 'Alex Horley-Orlandelli').
card_number('plagued rusalka'/'MM2', '89').
card_flavor_text('plagued rusalka'/'MM2', '\"Look at her, once filled with innocence. Death has a way of wringing away such . . . deficiencies.\"\n—Savra').
card_multiverse_id('plagued rusalka'/'MM2', '397906').

card_in_set('plaxcaster frogling', 'MM2').
card_original_type('plaxcaster frogling'/'MM2', 'Creature — Frog Mutant').
card_original_text('plaxcaster frogling'/'MM2', 'Graft 3 (This creature enters the battlefield with three +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{2}: Target creature with a +1/+1 counter on it gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_image_name('plaxcaster frogling'/'MM2', 'plaxcaster frogling').
card_uid('plaxcaster frogling'/'MM2', 'MM2:Plaxcaster Frogling:plaxcaster frogling').
card_rarity('plaxcaster frogling'/'MM2', 'Uncommon').
card_artist('plaxcaster frogling'/'MM2', 'Greg Staples').
card_number('plaxcaster frogling'/'MM2', '184').
card_multiverse_id('plaxcaster frogling'/'MM2', '397822').

card_in_set('plummet', 'MM2').
card_original_type('plummet'/'MM2', 'Instant').
card_original_text('plummet'/'MM2', 'Destroy target creature with flying.').
card_image_name('plummet'/'MM2', 'plummet').
card_uid('plummet'/'MM2', 'MM2:Plummet:plummet').
card_rarity('plummet'/'MM2', 'Common').
card_artist('plummet'/'MM2', 'Pete Venters').
card_number('plummet'/'MM2', '155').
card_flavor_text('plummet'/'MM2', '\"Let nothing own the skies but the wind.\"\n—Dejara, Giltwood druid').
card_multiverse_id('plummet'/'MM2', '397786').

card_in_set('precursor golem', 'MM2').
card_original_type('precursor golem'/'MM2', 'Artifact Creature — Golem').
card_original_text('precursor golem'/'MM2', 'When Precursor Golem enters the battlefield, put two 3/3 colorless Golem artifact creature tokens onto the battlefield.\nWhenever a player casts an instant or sorcery spell that targets only a single Golem, that player copies that spell for each other Golem that spell could target. Each copy targets a different one of those Golems.').
card_image_name('precursor golem'/'MM2', 'precursor golem').
card_uid('precursor golem'/'MM2', 'MM2:Precursor Golem:precursor golem').
card_rarity('precursor golem'/'MM2', 'Rare').
card_artist('precursor golem'/'MM2', 'Chippy').
card_number('precursor golem'/'MM2', '225').
card_multiverse_id('precursor golem'/'MM2', '397708').

card_in_set('primeval titan', 'MM2').
card_original_type('primeval titan'/'MM2', 'Creature — Giant').
card_original_text('primeval titan'/'MM2', 'Trample\nWhenever Primeval Titan enters the battlefield or attacks, you may search your library for up to two land cards, put them onto the battlefield tapped, then shuffle your library.').
card_image_name('primeval titan'/'MM2', 'primeval titan').
card_uid('primeval titan'/'MM2', 'MM2:Primeval Titan:primeval titan').
card_rarity('primeval titan'/'MM2', 'Mythic Rare').
card_artist('primeval titan'/'MM2', 'Aleksi Briclot').
card_number('primeval titan'/'MM2', '156').
card_flavor_text('primeval titan'/'MM2', 'When nature calls, run.').
card_multiverse_id('primeval titan'/'MM2', '397688').

card_in_set('profane command', 'MM2').
card_original_type('profane command'/'MM2', 'Sorcery').
card_original_text('profane command'/'MM2', 'Choose two —\n• Target player loses X life.\n• Return target creature card with converted mana cost X or less from your graveyard to the battlefield.\n• Target creature gets -X/-X until end of turn.\n• Up to X target creatures gain fear until end of turn.').
card_image_name('profane command'/'MM2', 'profane command').
card_uid('profane command'/'MM2', 'MM2:Profane Command:profane command').
card_rarity('profane command'/'MM2', 'Rare').
card_artist('profane command'/'MM2', 'Wayne England').
card_number('profane command'/'MM2', '90').
card_multiverse_id('profane command'/'MM2', '397713').

card_in_set('puppeteer clique', 'MM2').
card_original_type('puppeteer clique'/'MM2', 'Creature — Faerie Wizard').
card_original_text('puppeteer clique'/'MM2', 'Flying\nWhen Puppeteer Clique enters the battlefield, put target creature card from an opponent\'s graveyard onto the battlefield under your control. It gains haste. At the beginning of your next end step, exile it.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('puppeteer clique'/'MM2', 'puppeteer clique').
card_uid('puppeteer clique'/'MM2', 'MM2:Puppeteer Clique:puppeteer clique').
card_rarity('puppeteer clique'/'MM2', 'Rare').
card_artist('puppeteer clique'/'MM2', 'Daren Bader').
card_number('puppeteer clique'/'MM2', '91').
card_multiverse_id('puppeteer clique'/'MM2', '397839').

card_in_set('qumulox', 'MM2').
card_original_type('qumulox'/'MM2', 'Creature — Beast').
card_original_text('qumulox'/'MM2', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying').
card_image_name('qumulox'/'MM2', 'qumulox').
card_uid('qumulox'/'MM2', 'MM2:Qumulox:qumulox').
card_rarity('qumulox'/'MM2', 'Uncommon').
card_artist('qumulox'/'MM2', 'Carl Critchlow').
card_number('qumulox'/'MM2', '54').
card_flavor_text('qumulox'/'MM2', 'Even the clouds bend themselves to Memnarch\'s will, eager to swallow those who oppose him.').
card_multiverse_id('qumulox'/'MM2', '397877').

card_in_set('raise the alarm', 'MM2').
card_original_type('raise the alarm'/'MM2', 'Instant').
card_original_text('raise the alarm'/'MM2', 'Put two 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('raise the alarm'/'MM2', 'raise the alarm').
card_uid('raise the alarm'/'MM2', 'MM2:Raise the Alarm:raise the alarm').
card_rarity('raise the alarm'/'MM2', 'Common').
card_artist('raise the alarm'/'MM2', 'Zoltan Boros').
card_number('raise the alarm'/'MM2', '31').
card_flavor_text('raise the alarm'/'MM2', 'Like blinking or breathing, responding to an alarm is an involuntary reflex.').
card_multiverse_id('raise the alarm'/'MM2', '397666').

card_in_set('rakdos carnarium', 'MM2').
card_original_type('rakdos carnarium'/'MM2', 'Land').
card_original_text('rakdos carnarium'/'MM2', 'Rakdos Carnarium enters the battlefield tapped.\nWhen Rakdos Carnarium enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{R} to your mana pool.').
card_image_name('rakdos carnarium'/'MM2', 'rakdos carnarium').
card_uid('rakdos carnarium'/'MM2', 'MM2:Rakdos Carnarium:rakdos carnarium').
card_rarity('rakdos carnarium'/'MM2', 'Uncommon').
card_artist('rakdos carnarium'/'MM2', 'John Avon').
card_number('rakdos carnarium'/'MM2', '247').
card_multiverse_id('rakdos carnarium'/'MM2', '397753').

card_in_set('rampant growth', 'MM2').
card_original_type('rampant growth'/'MM2', 'Sorcery').
card_original_text('rampant growth'/'MM2', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('rampant growth'/'MM2', 'rampant growth').
card_uid('rampant growth'/'MM2', 'MM2:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'MM2', 'Common').
card_artist('rampant growth'/'MM2', 'Steven Belledin').
card_number('rampant growth'/'MM2', '157').
card_flavor_text('rampant growth'/'MM2', 'Nature grows solutions to her problems.').
card_multiverse_id('rampant growth'/'MM2', '397794').

card_in_set('reassembling skeleton', 'MM2').
card_original_type('reassembling skeleton'/'MM2', 'Creature — Skeleton Warrior').
card_original_text('reassembling skeleton'/'MM2', '{1}{B}: Return Reassembling Skeleton from your graveyard to the battlefield tapped.').
card_image_name('reassembling skeleton'/'MM2', 'reassembling skeleton').
card_uid('reassembling skeleton'/'MM2', 'MM2:Reassembling Skeleton:reassembling skeleton').
card_rarity('reassembling skeleton'/'MM2', 'Uncommon').
card_artist('reassembling skeleton'/'MM2', 'Austin Hsu').
card_number('reassembling skeleton'/'MM2', '92').
card_flavor_text('reassembling skeleton'/'MM2', 'Though you may see the same bones, you\'ll never see the same skeleton twice.').
card_multiverse_id('reassembling skeleton'/'MM2', '397819').

card_in_set('remand', 'MM2').
card_original_type('remand'/'MM2', 'Instant').
card_original_text('remand'/'MM2', 'Counter target spell. If that spell is countered this way, put it into its owner\'s hand instead of into that player\'s graveyard.\nDraw a card.').
card_image_name('remand'/'MM2', 'remand').
card_uid('remand'/'MM2', 'MM2:Remand:remand').
card_rarity('remand'/'MM2', 'Uncommon').
card_artist('remand'/'MM2', 'Zoltan Boros').
card_number('remand'/'MM2', '55').
card_flavor_text('remand'/'MM2', 'For the Azorius, the law can be a physical shield against chaos and anarchy.').
card_multiverse_id('remand'/'MM2', '397881').

card_in_set('repeal', 'MM2').
card_original_type('repeal'/'MM2', 'Instant').
card_original_text('repeal'/'MM2', 'Return target nonland permanent with converted mana cost X to its owner\'s hand.\nDraw a card.').
card_image_name('repeal'/'MM2', 'repeal').
card_uid('repeal'/'MM2', 'MM2:Repeal:repeal').
card_rarity('repeal'/'MM2', 'Common').
card_artist('repeal'/'MM2', 'Anthony Palumbo').
card_number('repeal'/'MM2', '56').
card_flavor_text('repeal'/'MM2', '\"Your deed cannot be undone. You, however, can be.\"\n—Agosto, Azorius imperator').
card_multiverse_id('repeal'/'MM2', '397667').

card_in_set('restless apparition', 'MM2').
card_original_type('restless apparition'/'MM2', 'Creature — Spirit').
card_original_text('restless apparition'/'MM2', '{W/B}{W/B}{W/B}: Restless Apparition gets +3/+3 until end of turn.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('restless apparition'/'MM2', 'restless apparition').
card_uid('restless apparition'/'MM2', 'MM2:Restless Apparition:restless apparition').
card_rarity('restless apparition'/'MM2', 'Uncommon').
card_artist('restless apparition'/'MM2', 'Jeff Easley').
card_number('restless apparition'/'MM2', '196').
card_multiverse_id('restless apparition'/'MM2', '397774').

card_in_set('root-kin ally', 'MM2').
card_original_type('root-kin ally'/'MM2', 'Creature — Elemental Warrior').
card_original_text('root-kin ally'/'MM2', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTap two untapped creatures you control: Root-Kin Ally gets +2/+2 until end of turn.').
card_image_name('root-kin ally'/'MM2', 'root-kin ally').
card_uid('root-kin ally'/'MM2', 'MM2:Root-Kin Ally:root-kin ally').
card_rarity('root-kin ally'/'MM2', 'Uncommon').
card_artist('root-kin ally'/'MM2', 'Arnie Swekel').
card_number('root-kin ally'/'MM2', '158').
card_multiverse_id('root-kin ally'/'MM2', '397659').

card_in_set('runed servitor', 'MM2').
card_original_type('runed servitor'/'MM2', 'Artifact Creature — Construct').
card_original_text('runed servitor'/'MM2', 'When Runed Servitor dies, each player draws a card.').
card_image_name('runed servitor'/'MM2', 'runed servitor').
card_uid('runed servitor'/'MM2', 'MM2:Runed Servitor:runed servitor').
card_rarity('runed servitor'/'MM2', 'Common').
card_artist('runed servitor'/'MM2', 'Mike Bierek').
card_number('runed servitor'/'MM2', '226').
card_flavor_text('runed servitor'/'MM2', 'Scholars had puzzled for centuries over the ruins at Tal Terig. Its secrets had always lived within one rune-carved head.').
card_multiverse_id('runed servitor'/'MM2', '397772').

card_in_set('rusted relic', 'MM2').
card_original_type('rusted relic'/'MM2', 'Artifact').
card_original_text('rusted relic'/'MM2', 'Metalcraft — Rusted Relic is a 5/5 Golem artifact creature as long as you control three or more artifacts.').
card_image_name('rusted relic'/'MM2', 'rusted relic').
card_uid('rusted relic'/'MM2', 'MM2:Rusted Relic:rusted relic').
card_rarity('rusted relic'/'MM2', 'Common').
card_artist('rusted relic'/'MM2', 'Igor Kieryluk').
card_number('rusted relic'/'MM2', '227').
card_flavor_text('rusted relic'/'MM2', '\"We consider rust a curable disease.\"\n—Tarrin, Hammer-Tribe shaman').
card_multiverse_id('rusted relic'/'MM2', '397837').

card_in_set('savage twister', 'MM2').
card_original_type('savage twister'/'MM2', 'Sorcery').
card_original_text('savage twister'/'MM2', 'Savage Twister deals X damage to each creature.').
card_image_name('savage twister'/'MM2', 'savage twister').
card_uid('savage twister'/'MM2', 'MM2:Savage Twister:savage twister').
card_rarity('savage twister'/'MM2', 'Uncommon').
card_artist('savage twister'/'MM2', 'John Avon').
card_number('savage twister'/'MM2', '185').
card_flavor_text('savage twister'/'MM2', '\"Nature is the ultimate mindless destroyer, capable of power and ferocity no army can match, and the Gruul follow its example.\"\n—Trigori, Azorius senator').
card_multiverse_id('savage twister'/'MM2', '397814').

card_in_set('scatter the seeds', 'MM2').
card_original_type('scatter the seeds'/'MM2', 'Instant').
card_original_text('scatter the seeds'/'MM2', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPut three 1/1 green Saproling creature tokens onto the battlefield.').
card_image_name('scatter the seeds'/'MM2', 'scatter the seeds').
card_uid('scatter the seeds'/'MM2', 'MM2:Scatter the Seeds:scatter the seeds').
card_rarity('scatter the seeds'/'MM2', 'Common').
card_artist('scatter the seeds'/'MM2', 'Rob Alexander').
card_number('scatter the seeds'/'MM2', '159').
card_flavor_text('scatter the seeds'/'MM2', 'From the seeds of faith, great forests grow.').
card_multiverse_id('scatter the seeds'/'MM2', '397665').

card_in_set('scavenger drake', 'MM2').
card_original_type('scavenger drake'/'MM2', 'Creature — Drake').
card_original_text('scavenger drake'/'MM2', 'Flying\nWhenever another creature dies, you may put a +1/+1 counter on Scavenger Drake.').
card_image_name('scavenger drake'/'MM2', 'scavenger drake').
card_uid('scavenger drake'/'MM2', 'MM2:Scavenger Drake:scavenger drake').
card_rarity('scavenger drake'/'MM2', 'Uncommon').
card_artist('scavenger drake'/'MM2', 'Trevor Claxton').
card_number('scavenger drake'/'MM2', '93').
card_flavor_text('scavenger drake'/'MM2', 'Dragons consider drakes to be mockeries of their perfection, destroying them on sight.').
card_multiverse_id('scavenger drake'/'MM2', '397811').

card_in_set('scion of the wild', 'MM2').
card_original_type('scion of the wild'/'MM2', 'Creature — Avatar').
card_original_text('scion of the wild'/'MM2', 'Scion of the Wild\'s power and toughness are each equal to the number of creatures you control.').
card_image_name('scion of the wild'/'MM2', 'scion of the wild').
card_uid('scion of the wild'/'MM2', 'MM2:Scion of the Wild:scion of the wild').
card_rarity('scion of the wild'/'MM2', 'Common').
card_artist('scion of the wild'/'MM2', 'Kev Walker').
card_number('scion of the wild'/'MM2', '160').
card_flavor_text('scion of the wild'/'MM2', 'It has a hundred thousand extinctions to avenge.').
card_multiverse_id('scion of the wild'/'MM2', '397894').

card_in_set('scute mob', 'MM2').
card_original_type('scute mob'/'MM2', 'Creature — Insect').
card_original_text('scute mob'/'MM2', 'At the beginning of your upkeep, if you control five or more lands, put four +1/+1 counters on Scute Mob.').
card_image_name('scute mob'/'MM2', 'scute mob').
card_uid('scute mob'/'MM2', 'MM2:Scute Mob:scute mob').
card_rarity('scute mob'/'MM2', 'Rare').
card_artist('scute mob'/'MM2', 'Zoltan Boros & Gabor Szikszai').
card_number('scute mob'/'MM2', '161').
card_flavor_text('scute mob'/'MM2', '\"Survival rule 781: There are always more scute bugs.\"\n—Zurdi, goblin shortcutter').
card_multiverse_id('scute mob'/'MM2', '397751').

card_in_set('scuttling death', 'MM2').
card_original_type('scuttling death'/'MM2', 'Creature — Spirit').
card_original_text('scuttling death'/'MM2', 'Sacrifice Scuttling Death: Target creature gets -1/-1 until end of turn.\nSoulshift 4 (When this creature dies, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_image_name('scuttling death'/'MM2', 'scuttling death').
card_uid('scuttling death'/'MM2', 'MM2:Scuttling Death:scuttling death').
card_rarity('scuttling death'/'MM2', 'Common').
card_artist('scuttling death'/'MM2', 'Thomas M. Baxa').
card_number('scuttling death'/'MM2', '94').
card_multiverse_id('scuttling death'/'MM2', '397740').

card_in_set('selesnya guildmage', 'MM2').
card_original_type('selesnya guildmage'/'MM2', 'Creature — Elf Wizard').
card_original_text('selesnya guildmage'/'MM2', '{3}{G}: Put a 1/1 green Saproling creature token onto the battlefield.\n{3}{W}: Creatures you control get +1/+1 until end of turn.').
card_image_name('selesnya guildmage'/'MM2', 'selesnya guildmage').
card_uid('selesnya guildmage'/'MM2', 'MM2:Selesnya Guildmage:selesnya guildmage').
card_rarity('selesnya guildmage'/'MM2', 'Uncommon').
card_artist('selesnya guildmage'/'MM2', 'Mark Zug').
card_number('selesnya guildmage'/'MM2', '197').
card_multiverse_id('selesnya guildmage'/'MM2', '397808').

card_in_set('selesnya sanctuary', 'MM2').
card_original_type('selesnya sanctuary'/'MM2', 'Land').
card_original_text('selesnya sanctuary'/'MM2', 'Selesnya Sanctuary enters the battlefield tapped.\nWhen Selesnya Sanctuary enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {G}{W} to your mana pool.').
card_image_name('selesnya sanctuary'/'MM2', 'selesnya sanctuary').
card_uid('selesnya sanctuary'/'MM2', 'MM2:Selesnya Sanctuary:selesnya sanctuary').
card_rarity('selesnya sanctuary'/'MM2', 'Uncommon').
card_artist('selesnya sanctuary'/'MM2', 'John Avon').
card_number('selesnya sanctuary'/'MM2', '248').
card_multiverse_id('selesnya sanctuary'/'MM2', '397734').

card_in_set('shadowmage infiltrator', 'MM2').
card_original_type('shadowmage infiltrator'/'MM2', 'Creature — Human Wizard').
card_original_text('shadowmage infiltrator'/'MM2', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever Shadowmage Infiltrator deals combat damage to a player, you may draw a card.').
card_image_name('shadowmage infiltrator'/'MM2', 'shadowmage infiltrator').
card_uid('shadowmage infiltrator'/'MM2', 'MM2:Shadowmage Infiltrator:shadowmage infiltrator').
card_rarity('shadowmage infiltrator'/'MM2', 'Rare').
card_artist('shadowmage infiltrator'/'MM2', 'Tomasz Jedruszek').
card_number('shadowmage infiltrator'/'MM2', '186').
card_multiverse_id('shadowmage infiltrator'/'MM2', '397858').

card_in_set('shrewd hatchling', 'MM2').
card_original_type('shrewd hatchling'/'MM2', 'Creature — Elemental').
card_original_text('shrewd hatchling'/'MM2', 'Shrewd Hatchling enters the battlefield with four -1/-1 counters on it.\n{U/R}: Target creature can\'t block Shrewd Hatchling this turn.\nWhenever you cast a blue spell, remove a -1/-1 counter from Shrewd Hatchling.\nWhenever you cast a red spell, remove a -1/-1 counter from Shrewd Hatchling.').
card_image_name('shrewd hatchling'/'MM2', 'shrewd hatchling').
card_uid('shrewd hatchling'/'MM2', 'MM2:Shrewd Hatchling:shrewd hatchling').
card_rarity('shrewd hatchling'/'MM2', 'Uncommon').
card_artist('shrewd hatchling'/'MM2', 'Carl Frank').
card_number('shrewd hatchling'/'MM2', '198').
card_multiverse_id('shrewd hatchling'/'MM2', '397898').

card_in_set('shrivel', 'MM2').
card_original_type('shrivel'/'MM2', 'Sorcery').
card_original_text('shrivel'/'MM2', 'All creatures get -1/-1 until end of turn.').
card_image_name('shrivel'/'MM2', 'shrivel').
card_uid('shrivel'/'MM2', 'MM2:Shrivel:shrivel').
card_rarity('shrivel'/'MM2', 'Common').
card_artist('shrivel'/'MM2', 'Jung Park').
card_number('shrivel'/'MM2', '95').
card_flavor_text('shrivel'/'MM2', '\"Have you ever killed insects nibbling at your crops? I think that\'s what the Eldrazi believe they\'re doing to us.\"\n—Sheyda, Ondu gamekeeper').
card_multiverse_id('shrivel'/'MM2', '397857').

card_in_set('sickle ripper', 'MM2').
card_original_type('sickle ripper'/'MM2', 'Creature — Elemental Warrior').
card_original_text('sickle ripper'/'MM2', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_image_name('sickle ripper'/'MM2', 'sickle ripper').
card_uid('sickle ripper'/'MM2', 'MM2:Sickle Ripper:sickle ripper').
card_rarity('sickle ripper'/'MM2', 'Common').
card_artist('sickle ripper'/'MM2', 'Dan Scott').
card_number('sickle ripper'/'MM2', '96').
card_flavor_text('sickle ripper'/'MM2', 'His sickle was forged in the heat of another cinder\'s funeral pyre.').
card_multiverse_id('sickle ripper'/'MM2', '397834').

card_in_set('sickleslicer', 'MM2').
card_original_type('sickleslicer'/'MM2', 'Artifact — Equipment').
card_original_text('sickleslicer'/'MM2', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +2/+2.\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('sickleslicer'/'MM2', 'sickleslicer').
card_uid('sickleslicer'/'MM2', 'MM2:Sickleslicer:sickleslicer').
card_rarity('sickleslicer'/'MM2', 'Common').
card_artist('sickleslicer'/'MM2', 'Jason Felix').
card_number('sickleslicer'/'MM2', '228').
card_multiverse_id('sickleslicer'/'MM2', '397771').

card_in_set('sigil blessing', 'MM2').
card_original_type('sigil blessing'/'MM2', 'Instant').
card_original_text('sigil blessing'/'MM2', 'Until end of turn, target creature you control gets +3/+3 and other creatures you control get +1/+1.').
card_image_name('sigil blessing'/'MM2', 'sigil blessing').
card_uid('sigil blessing'/'MM2', 'MM2:Sigil Blessing:sigil blessing').
card_rarity('sigil blessing'/'MM2', 'Uncommon').
card_artist('sigil blessing'/'MM2', 'Matt Stewart').
card_number('sigil blessing'/'MM2', '187').
card_flavor_text('sigil blessing'/'MM2', '\"For unwavering commitment and unflinching strength, the Order of the White Orchid confers its sigil. Rise, knight-captain, and do your duty.\"').
card_multiverse_id('sigil blessing'/'MM2', '397756').

card_in_set('sign in blood', 'MM2').
card_original_type('sign in blood'/'MM2', 'Sorcery').
card_original_text('sign in blood'/'MM2', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'MM2', 'sign in blood').
card_uid('sign in blood'/'MM2', 'MM2:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'MM2', 'Common').
card_artist('sign in blood'/'MM2', 'Howard Lyon').
card_number('sign in blood'/'MM2', '97').
card_flavor_text('sign in blood'/'MM2', 'Little agonies pave the way to greater power.').
card_multiverse_id('sign in blood'/'MM2', '397739').

card_in_set('simic growth chamber', 'MM2').
card_original_type('simic growth chamber'/'MM2', 'Land').
card_original_text('simic growth chamber'/'MM2', 'Simic Growth Chamber enters the battlefield tapped.\nWhen Simic Growth Chamber enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {G}{U} to your mana pool.').
card_image_name('simic growth chamber'/'MM2', 'simic growth chamber').
card_uid('simic growth chamber'/'MM2', 'MM2:Simic Growth Chamber:simic growth chamber').
card_rarity('simic growth chamber'/'MM2', 'Uncommon').
card_artist('simic growth chamber'/'MM2', 'John Avon').
card_number('simic growth chamber'/'MM2', '249').
card_multiverse_id('simic growth chamber'/'MM2', '397757').

card_in_set('simic initiate', 'MM2').
card_original_type('simic initiate'/'MM2', 'Creature — Human Mutant').
card_original_text('simic initiate'/'MM2', 'Graft 1 (This creature enters the battlefield with a +1/+1 counter on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)').
card_image_name('simic initiate'/'MM2', 'simic initiate').
card_uid('simic initiate'/'MM2', 'MM2:Simic Initiate:simic initiate').
card_rarity('simic initiate'/'MM2', 'Common').
card_artist('simic initiate'/'MM2', 'Dany Orizio').
card_number('simic initiate'/'MM2', '162').
card_flavor_text('simic initiate'/'MM2', 'Simic initiates begin their training as experimental subjects. Failures are flushed to the undersewers.').
card_multiverse_id('simic initiate'/'MM2', '397895').

card_in_set('skarrgan firebird', 'MM2').
card_original_type('skarrgan firebird'/'MM2', 'Creature — Phoenix').
card_original_text('skarrgan firebird'/'MM2', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)\nFlying\n{R}{R}{R}: Return Skarrgan Firebird from your graveyard to your hand. Activate this ability only if an opponent was dealt damage this turn.').
card_image_name('skarrgan firebird'/'MM2', 'skarrgan firebird').
card_uid('skarrgan firebird'/'MM2', 'MM2:Skarrgan Firebird:skarrgan firebird').
card_rarity('skarrgan firebird'/'MM2', 'Uncommon').
card_artist('skarrgan firebird'/'MM2', 'Kev Walker').
card_number('skarrgan firebird'/'MM2', '123').
card_multiverse_id('skarrgan firebird'/'MM2', '397867').

card_in_set('skyhunter skirmisher', 'MM2').
card_original_type('skyhunter skirmisher'/'MM2', 'Creature — Cat Knight').
card_original_text('skyhunter skirmisher'/'MM2', 'Flying\nDouble strike (This creature deals both first-strike and regular combat damage.)').
card_image_name('skyhunter skirmisher'/'MM2', 'skyhunter skirmisher').
card_uid('skyhunter skirmisher'/'MM2', 'MM2:Skyhunter Skirmisher:skyhunter skirmisher').
card_rarity('skyhunter skirmisher'/'MM2', 'Common').
card_artist('skyhunter skirmisher'/'MM2', 'Greg Staples').
card_number('skyhunter skirmisher'/'MM2', '32').
card_flavor_text('skyhunter skirmisher'/'MM2', '\"Like dawn\'s first light, blind the unprepared and banish the shadows.\"\n—Skyhunter creed').
card_multiverse_id('skyhunter skirmisher'/'MM2', '397835').

card_in_set('skyreach manta', 'MM2').
card_original_type('skyreach manta'/'MM2', 'Artifact Creature — Fish').
card_original_text('skyreach manta'/'MM2', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)\nFlying').
card_image_name('skyreach manta'/'MM2', 'skyreach manta').
card_uid('skyreach manta'/'MM2', 'MM2:Skyreach Manta:skyreach manta').
card_rarity('skyreach manta'/'MM2', 'Common').
card_artist('skyreach manta'/'MM2', 'Christopher Moeller').
card_number('skyreach manta'/'MM2', '229').
card_flavor_text('skyreach manta'/'MM2', 'As the dawns break, the manta soars.').
card_multiverse_id('skyreach manta'/'MM2', '397702').

card_in_set('smash to smithereens', 'MM2').
card_original_type('smash to smithereens'/'MM2', 'Instant').
card_original_text('smash to smithereens'/'MM2', 'Destroy target artifact. Smash to Smithereens deals 3 damage to that artifact\'s controller.').
card_image_name('smash to smithereens'/'MM2', 'smash to smithereens').
card_uid('smash to smithereens'/'MM2', 'MM2:Smash to Smithereens:smash to smithereens').
card_rarity('smash to smithereens'/'MM2', 'Common').
card_artist('smash to smithereens'/'MM2', 'Pete Venters').
card_number('smash to smithereens'/'MM2', '124').
card_flavor_text('smash to smithereens'/'MM2', 'The giant Tarvik dreamed that trinkets and machines caused all the world\'s woe. When he awoke from his troubled sleep, he took the name Tarvik Relicsmasher.').
card_multiverse_id('smash to smithereens'/'MM2', '397795').

card_in_set('smokebraider', 'MM2').
card_original_type('smokebraider'/'MM2', 'Creature — Elemental Shaman').
card_original_text('smokebraider'/'MM2', '{T}: Add two mana in any combination of colors to your mana pool. Spend this mana only to cast Elemental spells or activate abilities of Elementals.').
card_image_name('smokebraider'/'MM2', 'smokebraider').
card_uid('smokebraider'/'MM2', 'MM2:Smokebraider:smokebraider').
card_rarity('smokebraider'/'MM2', 'Common').
card_artist('smokebraider'/'MM2', 'Anthony S. Waters').
card_number('smokebraider'/'MM2', '125').
card_flavor_text('smokebraider'/'MM2', '\"Be silent and listen to your inner fire. Only then can you walk the Path of Flame.\"').
card_multiverse_id('smokebraider'/'MM2', '397725').

card_in_set('somber hoverguard', 'MM2').
card_original_type('somber hoverguard'/'MM2', 'Creature — Drone').
card_original_text('somber hoverguard'/'MM2', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying').
card_image_name('somber hoverguard'/'MM2', 'somber hoverguard').
card_uid('somber hoverguard'/'MM2', 'MM2:Somber Hoverguard:somber hoverguard').
card_rarity('somber hoverguard'/'MM2', 'Common').
card_artist('somber hoverguard'/'MM2', 'Adam Rex').
card_number('somber hoverguard'/'MM2', '57').
card_flavor_text('somber hoverguard'/'MM2', 'The vedalken interrogate all intruders—once the hoverguards are done with them.').
card_multiverse_id('somber hoverguard'/'MM2', '397831').

card_in_set('soulbright flamekin', 'MM2').
card_original_type('soulbright flamekin'/'MM2', 'Creature — Elemental Shaman').
card_original_text('soulbright flamekin'/'MM2', '{2}: Target creature gains trample until end of turn. If this is the third time this ability has resolved this turn, you may add {R}{R}{R}{R}{R}{R}{R}{R} to your mana pool.').
card_image_name('soulbright flamekin'/'MM2', 'soulbright flamekin').
card_uid('soulbright flamekin'/'MM2', 'MM2:Soulbright Flamekin:soulbright flamekin').
card_rarity('soulbright flamekin'/'MM2', 'Common').
card_artist('soulbright flamekin'/'MM2', 'Kev Walker').
card_number('soulbright flamekin'/'MM2', '126').
card_flavor_text('soulbright flamekin'/'MM2', 'When provoked, a flamekin\'s inner fire burns far hotter than any giant\'s forge.').
card_multiverse_id('soulbright flamekin'/'MM2', '397730').

card_in_set('spectral procession', 'MM2').
card_original_type('spectral procession'/'MM2', 'Sorcery').
card_original_text('spectral procession'/'MM2', '({2/W} can be paid with any two mana or with {W}. This card\'s converted mana cost is 6.)\nPut three 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('spectral procession'/'MM2', 'spectral procession').
card_uid('spectral procession'/'MM2', 'MM2:Spectral Procession:spectral procession').
card_rarity('spectral procession'/'MM2', 'Uncommon').
card_artist('spectral procession'/'MM2', 'Tomasz Jedruszek').
card_number('spectral procession'/'MM2', '33').
card_flavor_text('spectral procession'/'MM2', '\"I need never remember my past associates, my victims, my mistakes. Innistrad preserves them all for me.\"\n—Sorin Markov').
card_multiverse_id('spectral procession'/'MM2', '397841').

card_in_set('spellskite', 'MM2').
card_original_type('spellskite'/'MM2', 'Artifact Creature — Horror').
card_original_text('spellskite'/'MM2', '{U/P}: Change a target of target spell or ability to Spellskite. ({U/P} can be paid with either {U} or 2 life.)').
card_image_name('spellskite'/'MM2', 'spellskite').
card_uid('spellskite'/'MM2', 'MM2:Spellskite:spellskite').
card_rarity('spellskite'/'MM2', 'Rare').
card_artist('spellskite'/'MM2', 'Chippy').
card_number('spellskite'/'MM2', '230').
card_flavor_text('spellskite'/'MM2', '\"Let\'s show Vorinclex that progress doesn\'t always need teeth or claws.\"\n—Malcator, Executor of Synthesis').
card_multiverse_id('spellskite'/'MM2', '397743').

card_in_set('sphere of the suns', 'MM2').
card_original_type('sphere of the suns'/'MM2', 'Artifact').
card_original_text('sphere of the suns'/'MM2', 'Sphere of the Suns enters the battlefield tapped and with three charge counters on it.\n{T}, Remove a charge counter from Sphere of the Suns: Add one mana of any color to your mana pool.').
card_image_name('sphere of the suns'/'MM2', 'sphere of the suns').
card_uid('sphere of the suns'/'MM2', 'MM2:Sphere of the Suns:sphere of the suns').
card_rarity('sphere of the suns'/'MM2', 'Common').
card_artist('sphere of the suns'/'MM2', 'Jana Schirmer & Johannes Voss').
card_number('sphere of the suns'/'MM2', '231').
card_multiverse_id('sphere of the suns'/'MM2', '397904').

card_in_set('spikeshot elder', 'MM2').
card_original_type('spikeshot elder'/'MM2', 'Creature — Goblin Shaman').
card_original_text('spikeshot elder'/'MM2', '{1}{R}{R}: Spikeshot Elder deals damage equal to its power to target creature or player.').
card_image_name('spikeshot elder'/'MM2', 'spikeshot elder').
card_uid('spikeshot elder'/'MM2', 'MM2:Spikeshot Elder:spikeshot elder').
card_rarity('spikeshot elder'/'MM2', 'Rare').
card_artist('spikeshot elder'/'MM2', 'Izzy').
card_number('spikeshot elder'/'MM2', '127').
card_flavor_text('spikeshot elder'/'MM2', 'Now that he knows a thousand ways of hurting people, his next shamanic quest is to discover a thousand more.').
card_multiverse_id('spikeshot elder'/'MM2', '397883').

card_in_set('spitebellows', 'MM2').
card_original_type('spitebellows'/'MM2', 'Creature — Elemental').
card_original_text('spitebellows'/'MM2', 'When Spitebellows leaves the battlefield, it deals 6 damage to target creature.\nEvoke {1}{R}{R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('spitebellows'/'MM2', 'spitebellows').
card_uid('spitebellows'/'MM2', 'MM2:Spitebellows:spitebellows').
card_rarity('spitebellows'/'MM2', 'Uncommon').
card_artist('spitebellows'/'MM2', 'Larry MacDougall').
card_number('spitebellows'/'MM2', '128').
card_flavor_text('spitebellows'/'MM2', 'Disaster stalks with gaping jaws across unready lands.').
card_multiverse_id('spitebellows'/'MM2', '397664').

card_in_set('splinter twin', 'MM2').
card_original_type('splinter twin'/'MM2', 'Enchantment — Aura').
card_original_text('splinter twin'/'MM2', 'Enchant creature\nEnchanted creature has \"{T}: Put a token that\'s a copy of this creature onto the battlefield. That token has haste. Exile it at the beginning of the next end step.\"').
card_image_name('splinter twin'/'MM2', 'splinter twin').
card_uid('splinter twin'/'MM2', 'MM2:Splinter Twin:splinter twin').
card_rarity('splinter twin'/'MM2', 'Rare').
card_artist('splinter twin'/'MM2', 'Goran Josic').
card_number('splinter twin'/'MM2', '129').
card_flavor_text('splinter twin'/'MM2', '\"I know just the person for that job.\"').
card_multiverse_id('splinter twin'/'MM2', '397816').

card_in_set('spread the sickness', 'MM2').
card_original_type('spread the sickness'/'MM2', 'Sorcery').
card_original_text('spread the sickness'/'MM2', 'Destroy target creature, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('spread the sickness'/'MM2', 'spread the sickness').
card_uid('spread the sickness'/'MM2', 'MM2:Spread the Sickness:spread the sickness').
card_rarity('spread the sickness'/'MM2', 'Uncommon').
card_artist('spread the sickness'/'MM2', 'Jaime Jones').
card_number('spread the sickness'/'MM2', '98').
card_flavor_text('spread the sickness'/'MM2', 'Life is ephemeral. Phyrexia is eternal.').
card_multiverse_id('spread the sickness'/'MM2', '397823').

card_in_set('steady progress', 'MM2').
card_original_type('steady progress'/'MM2', 'Instant').
card_original_text('steady progress'/'MM2', 'Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)\nDraw a card.').
card_image_name('steady progress'/'MM2', 'steady progress').
card_uid('steady progress'/'MM2', 'MM2:Steady Progress:steady progress').
card_rarity('steady progress'/'MM2', 'Common').
card_artist('steady progress'/'MM2', 'Efrem Palacios').
card_number('steady progress'/'MM2', '58').
card_flavor_text('steady progress'/'MM2', '\"More of that strange oil . . . It\'s probably nothing.\"').
card_multiverse_id('steady progress'/'MM2', '397805').

card_in_set('stoic rebuttal', 'MM2').
card_original_type('stoic rebuttal'/'MM2', 'Instant').
card_original_text('stoic rebuttal'/'MM2', 'Metalcraft — Stoic Rebuttal costs {1} less to cast if you control three or more artifacts.\nCounter target spell.').
card_image_name('stoic rebuttal'/'MM2', 'stoic rebuttal').
card_uid('stoic rebuttal'/'MM2', 'MM2:Stoic Rebuttal:stoic rebuttal').
card_rarity('stoic rebuttal'/'MM2', 'Common').
card_artist('stoic rebuttal'/'MM2', 'Chris Rahn').
card_number('stoic rebuttal'/'MM2', '59').
card_flavor_text('stoic rebuttal'/'MM2', 'Obsessed with the pursuit of knowledge above all else, vedalken can appear to be cold and emotionless.').
card_multiverse_id('stoic rebuttal'/'MM2', '397765').

card_in_set('stormblood berserker', 'MM2').
card_original_type('stormblood berserker'/'MM2', 'Creature — Human Berserker').
card_original_text('stormblood berserker'/'MM2', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)\nStormblood Berserker can\'t be blocked except by two or more creatures.').
card_image_name('stormblood berserker'/'MM2', 'stormblood berserker').
card_uid('stormblood berserker'/'MM2', 'MM2:Stormblood Berserker:stormblood berserker').
card_rarity('stormblood berserker'/'MM2', 'Uncommon').
card_artist('stormblood berserker'/'MM2', 'Min Yum').
card_number('stormblood berserker'/'MM2', '130').
card_flavor_text('stormblood berserker'/'MM2', 'Blood is holy to those whose god is War.').
card_multiverse_id('stormblood berserker'/'MM2', '397712').

card_in_set('sundering vitae', 'MM2').
card_original_type('sundering vitae'/'MM2', 'Instant').
card_original_text('sundering vitae'/'MM2', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nDestroy target artifact or enchantment.').
card_image_name('sundering vitae'/'MM2', 'sundering vitae').
card_uid('sundering vitae'/'MM2', 'MM2:Sundering Vitae:sundering vitae').
card_rarity('sundering vitae'/'MM2', 'Common').
card_artist('sundering vitae'/'MM2', 'Shishizaru').
card_number('sundering vitae'/'MM2', '163').
card_flavor_text('sundering vitae'/'MM2', 'Centuries of wind, rain, and roots compressed into an instant of destruction: such is the power of Selesnya.').
card_multiverse_id('sundering vitae'/'MM2', '397792').

card_in_set('sunforger', 'MM2').
card_original_type('sunforger'/'MM2', 'Artifact — Equipment').
card_original_text('sunforger'/'MM2', 'Equipped creature gets +4/+0.\n{R}{W}, Unattach Sunforger: Search your library for a red or white instant card with converted mana cost 4 or less and cast that card without paying its mana cost. Then shuffle your library.\nEquip {3}').
card_image_name('sunforger'/'MM2', 'sunforger').
card_uid('sunforger'/'MM2', 'MM2:Sunforger:sunforger').
card_rarity('sunforger'/'MM2', 'Rare').
card_artist('sunforger'/'MM2', 'Darrell Riche').
card_number('sunforger'/'MM2', '232').
card_multiverse_id('sunforger'/'MM2', '397900').

card_in_set('sunlance', 'MM2').
card_original_type('sunlance'/'MM2', 'Sorcery').
card_original_text('sunlance'/'MM2', 'Sunlance deals 3 damage to target nonwhite creature.').
card_image_name('sunlance'/'MM2', 'sunlance').
card_uid('sunlance'/'MM2', 'MM2:Sunlance:sunlance').
card_rarity('sunlance'/'MM2', 'Common').
card_artist('sunlance'/'MM2', 'Volkan Baga').
card_number('sunlance'/'MM2', '34').
card_flavor_text('sunlance'/'MM2', '\"It\'s easy for the innocent to speak of justice. They seldom feel its terrible power.\"\n—Orim, Samite inquisitor').
card_multiverse_id('sunlance'/'MM2', '397862').

card_in_set('sunspear shikari', 'MM2').
card_original_type('sunspear shikari'/'MM2', 'Creature — Cat Soldier').
card_original_text('sunspear shikari'/'MM2', 'As long as Sunspear Shikari is equipped, it has first strike and lifelink.').
card_image_name('sunspear shikari'/'MM2', 'sunspear shikari').
card_uid('sunspear shikari'/'MM2', 'MM2:Sunspear Shikari:sunspear shikari').
card_rarity('sunspear shikari'/'MM2', 'Common').
card_artist('sunspear shikari'/'MM2', 'Allen Williams').
card_number('sunspear shikari'/'MM2', '35').
card_flavor_text('sunspear shikari'/'MM2', 'Left without their leader Raksha, the leonin split into two prides: one side supported the regent kha, while the other rebelled in fury.').
card_multiverse_id('sunspear shikari'/'MM2', '397801').

card_in_set('surgical extraction', 'MM2').
card_original_type('surgical extraction'/'MM2', 'Instant').
card_original_text('surgical extraction'/'MM2', '({B/P} can be paid with either {B} or 2 life.)\nChoose target card in a graveyard other than a basic land card. Search its owner\'s graveyard, hand, and library for any number of cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_image_name('surgical extraction'/'MM2', 'surgical extraction').
card_uid('surgical extraction'/'MM2', 'MM2:Surgical Extraction:surgical extraction').
card_rarity('surgical extraction'/'MM2', 'Rare').
card_artist('surgical extraction'/'MM2', 'Steven Belledin').
card_number('surgical extraction'/'MM2', '99').
card_multiverse_id('surgical extraction'/'MM2', '397706').

card_in_set('surrakar spellblade', 'MM2').
card_original_type('surrakar spellblade'/'MM2', 'Creature — Surrakar').
card_original_text('surrakar spellblade'/'MM2', 'Whenever you cast an instant or sorcery spell, you may put a charge counter on Surrakar Spellblade.\nWhenever Surrakar Spellblade deals combat damage to a player, you may draw X cards, where X is the number of charge counters on it.').
card_image_name('surrakar spellblade'/'MM2', 'surrakar spellblade').
card_uid('surrakar spellblade'/'MM2', 'MM2:Surrakar Spellblade:surrakar spellblade').
card_rarity('surrakar spellblade'/'MM2', 'Rare').
card_artist('surrakar spellblade'/'MM2', 'Dave Kendall').
card_number('surrakar spellblade'/'MM2', '60').
card_multiverse_id('surrakar spellblade'/'MM2', '397735').

card_in_set('swans of bryn argoll', 'MM2').
card_original_type('swans of bryn argoll'/'MM2', 'Creature — Bird Spirit').
card_original_text('swans of bryn argoll'/'MM2', 'Flying\nIf a source would deal damage to Swans of Bryn Argoll, prevent that damage. The source\'s controller draws cards equal to the damage prevented this way.').
card_image_name('swans of bryn argoll'/'MM2', 'swans of bryn argoll').
card_uid('swans of bryn argoll'/'MM2', 'MM2:Swans of Bryn Argoll:swans of bryn argoll').
card_rarity('swans of bryn argoll'/'MM2', 'Rare').
card_artist('swans of bryn argoll'/'MM2', 'Eric Fortune').
card_number('swans of bryn argoll'/'MM2', '199').
card_flavor_text('swans of bryn argoll'/'MM2', 'Any being that harms them quickly learns its lesson.').
card_multiverse_id('swans of bryn argoll'/'MM2', '397778').

card_in_set('sylvan bounty', 'MM2').
card_original_type('sylvan bounty'/'MM2', 'Instant').
card_original_text('sylvan bounty'/'MM2', 'Target player gains 8 life.\nBasic landcycling {1}{G} ({1}{G}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('sylvan bounty'/'MM2', 'sylvan bounty').
card_uid('sylvan bounty'/'MM2', 'MM2:Sylvan Bounty:sylvan bounty').
card_rarity('sylvan bounty'/'MM2', 'Common').
card_artist('sylvan bounty'/'MM2', 'Chris Rahn').
card_number('sylvan bounty'/'MM2', '164').
card_flavor_text('sylvan bounty'/'MM2', 'Some who scouted new lands chose to stay.').
card_multiverse_id('sylvan bounty'/'MM2', '397658').

card_in_set('taj-nar swordsmith', 'MM2').
card_original_type('taj-nar swordsmith'/'MM2', 'Creature — Cat Soldier').
card_original_text('taj-nar swordsmith'/'MM2', 'When Taj-Nar Swordsmith enters the battlefield, you may pay {X}. If you do, search your library for an Equipment card with converted mana cost X or less and put that card onto the battlefield. Then shuffle your library.').
card_image_name('taj-nar swordsmith'/'MM2', 'taj-nar swordsmith').
card_uid('taj-nar swordsmith'/'MM2', 'MM2:Taj-Nar Swordsmith:taj-nar swordsmith').
card_rarity('taj-nar swordsmith'/'MM2', 'Uncommon').
card_artist('taj-nar swordsmith'/'MM2', 'Todd Lockwood').
card_number('taj-nar swordsmith'/'MM2', '36').
card_multiverse_id('taj-nar swordsmith'/'MM2', '397873').

card_in_set('tarmogoyf', 'MM2').
card_original_type('tarmogoyf'/'MM2', 'Creature — Lhurgoyf').
card_original_text('tarmogoyf'/'MM2', 'Tarmogoyf\'s power is equal to the number of card types among cards in all graveyards and its toughness is equal to that number plus 1.').
card_image_name('tarmogoyf'/'MM2', 'tarmogoyf').
card_uid('tarmogoyf'/'MM2', 'MM2:Tarmogoyf:tarmogoyf').
card_rarity('tarmogoyf'/'MM2', 'Mythic Rare').
card_artist('tarmogoyf'/'MM2', 'Ryan Barger').
card_number('tarmogoyf'/'MM2', '165').
card_flavor_text('tarmogoyf'/'MM2', 'What doesn\'t grow, dies. And what dies grows the tarmogoyf.').
card_multiverse_id('tarmogoyf'/'MM2', '397682').

card_in_set('telling time', 'MM2').
card_original_type('telling time'/'MM2', 'Instant').
card_original_text('telling time'/'MM2', 'Look at the top three cards of your library. Put one of those cards into your hand, one on top of your library, and one on the bottom of your library.').
card_image_name('telling time'/'MM2', 'telling time').
card_uid('telling time'/'MM2', 'MM2:Telling Time:telling time').
card_rarity('telling time'/'MM2', 'Common').
card_artist('telling time'/'MM2', 'Scott M. Fischer').
card_number('telling time'/'MM2', '61').
card_flavor_text('telling time'/'MM2', 'Mastery is achieved when \"telling time\" becomes \"telling time what to do.\"').
card_multiverse_id('telling time'/'MM2', '397728').

card_in_set('terashi\'s grasp', 'MM2').
card_original_type('terashi\'s grasp'/'MM2', 'Sorcery — Arcane').
card_original_text('terashi\'s grasp'/'MM2', 'Destroy target artifact or enchantment. You gain life equal to its converted mana cost.').
card_image_name('terashi\'s grasp'/'MM2', 'terashi\'s grasp').
card_uid('terashi\'s grasp'/'MM2', 'MM2:Terashi\'s Grasp:terashi\'s grasp').
card_rarity('terashi\'s grasp'/'MM2', 'Common').
card_artist('terashi\'s grasp'/'MM2', 'Mark Tedin').
card_number('terashi\'s grasp'/'MM2', '37').
card_flavor_text('terashi\'s grasp'/'MM2', '\"The jeweler, the potter, the smith . . . They all imbue a bit of their souls into their creations. The kami destroy that crafted mortal shell and absorb the soul within.\"\n—Noboru, master kitemaker').
card_multiverse_id('terashi\'s grasp'/'MM2', '397891').

card_in_set('tezzeret the seeker', 'MM2').
card_original_type('tezzeret the seeker'/'MM2', 'Planeswalker — Tezzeret').
card_original_text('tezzeret the seeker'/'MM2', '+1: Untap up to two target artifacts.\n−X: Search your library for an artifact card with converted mana cost X or less and put it onto the battlefield. Then shuffle your library.\n−5: Artifacts you control become artifact creatures with base power and toughness 5/5 until end of turn.').
card_image_name('tezzeret the seeker'/'MM2', 'tezzeret the seeker').
card_uid('tezzeret the seeker'/'MM2', 'MM2:Tezzeret the Seeker:tezzeret the seeker').
card_rarity('tezzeret the seeker'/'MM2', 'Mythic Rare').
card_artist('tezzeret the seeker'/'MM2', 'Anthony Francisco').
card_number('tezzeret the seeker'/'MM2', '62').
card_multiverse_id('tezzeret the seeker'/'MM2', '397700').

card_in_set('tezzeret\'s gambit', 'MM2').
card_original_type('tezzeret\'s gambit'/'MM2', 'Sorcery').
card_original_text('tezzeret\'s gambit'/'MM2', '({U/P} can be paid with either {U} or 2 life.)\nDraw two cards, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('tezzeret\'s gambit'/'MM2', 'tezzeret\'s gambit').
card_uid('tezzeret\'s gambit'/'MM2', 'MM2:Tezzeret\'s Gambit:tezzeret\'s gambit').
card_rarity('tezzeret\'s gambit'/'MM2', 'Uncommon').
card_artist('tezzeret\'s gambit'/'MM2', 'Karl Kopinski').
card_number('tezzeret\'s gambit'/'MM2', '63').
card_multiverse_id('tezzeret\'s gambit'/'MM2', '397670').

card_in_set('thief of hope', 'MM2').
card_original_type('thief of hope'/'MM2', 'Creature — Spirit').
card_original_text('thief of hope'/'MM2', 'Whenever you cast a Spirit or Arcane spell, target opponent loses 1 life and you gain 1 life.\nSoulshift 2 (When this creature dies, you may return target Spirit card with converted mana cost 2 or less from your graveyard to your hand.)').
card_image_name('thief of hope'/'MM2', 'thief of hope').
card_uid('thief of hope'/'MM2', 'MM2:Thief of Hope:thief of hope').
card_rarity('thief of hope'/'MM2', 'Common').
card_artist('thief of hope'/'MM2', 'Greg Hildebrandt').
card_number('thief of hope'/'MM2', '100').
card_multiverse_id('thief of hope'/'MM2', '397785').

card_in_set('thoughtcast', 'MM2').
card_original_type('thoughtcast'/'MM2', 'Sorcery').
card_original_text('thoughtcast'/'MM2', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nDraw two cards.').
card_image_name('thoughtcast'/'MM2', 'thoughtcast').
card_uid('thoughtcast'/'MM2', 'MM2:Thoughtcast:thoughtcast').
card_rarity('thoughtcast'/'MM2', 'Common').
card_artist('thoughtcast'/'MM2', 'Greg Hildebrandt').
card_number('thoughtcast'/'MM2', '64').
card_flavor_text('thoughtcast'/'MM2', 'Vedalken eyes don\'t see the beauty in things. They see only what those things can teach.').
card_multiverse_id('thoughtcast'/'MM2', '397804').

card_in_set('thrive', 'MM2').
card_original_type('thrive'/'MM2', 'Sorcery').
card_original_text('thrive'/'MM2', 'Put a +1/+1 counter on each of X target creatures.').
card_image_name('thrive'/'MM2', 'thrive').
card_uid('thrive'/'MM2', 'MM2:Thrive:thrive').
card_rarity('thrive'/'MM2', 'Common').
card_artist('thrive'/'MM2', 'Daren Bader').
card_number('thrive'/'MM2', '166').
card_flavor_text('thrive'/'MM2', 'The most successful of Simic creations is cytoplasm, a living, symbiotic substance that feeds off genetic rhythms and strengthens its host in return.').
card_multiverse_id('thrive'/'MM2', '397695').

card_in_set('thrummingbird', 'MM2').
card_original_type('thrummingbird'/'MM2', 'Creature — Bird Horror').
card_original_text('thrummingbird'/'MM2', 'Flying\nWhenever Thrummingbird deals combat damage to a player, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('thrummingbird'/'MM2', 'thrummingbird').
card_uid('thrummingbird'/'MM2', 'MM2:Thrummingbird:thrummingbird').
card_rarity('thrummingbird'/'MM2', 'Common').
card_artist('thrummingbird'/'MM2', 'Efrem Palacios').
card_number('thrummingbird'/'MM2', '65').
card_multiverse_id('thrummingbird'/'MM2', '397860').

card_in_set('thunderblust', 'MM2').
card_original_type('thunderblust'/'MM2', 'Creature — Elemental').
card_original_text('thunderblust'/'MM2', 'Haste\nThunderblust has trample as long as it has a -1/-1 counter on it.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('thunderblust'/'MM2', 'thunderblust').
card_uid('thunderblust'/'MM2', 'MM2:Thunderblust:thunderblust').
card_rarity('thunderblust'/'MM2', 'Rare').
card_artist('thunderblust'/'MM2', 'Dan Scott').
card_number('thunderblust'/'MM2', '131').
card_multiverse_id('thunderblust'/'MM2', '397848').

card_in_set('tribal flames', 'MM2').
card_original_type('tribal flames'/'MM2', 'Sorcery').
card_original_text('tribal flames'/'MM2', 'Domain — Tribal Flames deals X damage to target creature or player, where X is the number of basic land types among lands you control.').
card_image_name('tribal flames'/'MM2', 'tribal flames').
card_uid('tribal flames'/'MM2', 'MM2:Tribal Flames:tribal flames').
card_rarity('tribal flames'/'MM2', 'Common').
card_artist('tribal flames'/'MM2', 'Zack Stella').
card_number('tribal flames'/'MM2', '132').
card_flavor_text('tribal flames'/'MM2', '\"Fire is the universal language.\"\n—Jhoira, master artificer').
card_multiverse_id('tribal flames'/'MM2', '397842').

card_in_set('tukatongue thallid', 'MM2').
card_original_type('tukatongue thallid'/'MM2', 'Creature — Fungus').
card_original_text('tukatongue thallid'/'MM2', 'When Tukatongue Thallid dies, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('tukatongue thallid'/'MM2', 'tukatongue thallid').
card_uid('tukatongue thallid'/'MM2', 'MM2:Tukatongue Thallid:tukatongue thallid').
card_rarity('tukatongue thallid'/'MM2', 'Common').
card_artist('tukatongue thallid'/'MM2', 'Vance Kovacs').
card_number('tukatongue thallid'/'MM2', '167').
card_flavor_text('tukatongue thallid'/'MM2', 'Jund\'s thallids tried to disguise their deliciousness by covering themselves in spines harvested from the tukatongue tree.').
card_multiverse_id('tukatongue thallid'/'MM2', '397749').

card_in_set('tumble magnet', 'MM2').
card_original_type('tumble magnet'/'MM2', 'Artifact').
card_original_text('tumble magnet'/'MM2', 'Tumble Magnet enters the battlefield with three charge counters on it.\n{T}, Remove a charge counter from Tumble Magnet: Tap target artifact or creature.').
card_image_name('tumble magnet'/'MM2', 'tumble magnet').
card_uid('tumble magnet'/'MM2', 'MM2:Tumble Magnet:tumble magnet').
card_rarity('tumble magnet'/'MM2', 'Uncommon').
card_artist('tumble magnet'/'MM2', 'Drew Baker').
card_number('tumble magnet'/'MM2', '233').
card_flavor_text('tumble magnet'/'MM2', 'Magnetic devices that keep massive golems and structures standing can also be used for the opposite purpose.').
card_multiverse_id('tumble magnet'/'MM2', '397755').

card_in_set('ulamog\'s crusher', 'MM2').
card_original_type('ulamog\'s crusher'/'MM2', 'Creature — Eldrazi').
card_original_text('ulamog\'s crusher'/'MM2', 'Annihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)\nUlamog\'s Crusher attacks each turn if able.').
card_image_name('ulamog\'s crusher'/'MM2', 'ulamog\'s crusher').
card_uid('ulamog\'s crusher'/'MM2', 'MM2:Ulamog\'s Crusher:ulamog\'s crusher').
card_rarity('ulamog\'s crusher'/'MM2', 'Common').
card_artist('ulamog\'s crusher'/'MM2', 'Todd Lockwood').
card_number('ulamog\'s crusher'/'MM2', '7').
card_flavor_text('ulamog\'s crusher'/'MM2', '\"Whatever the Eldrazi\'s purpose is, it has nothing to do with something so insignificant as us.\"\n—Nirthu, lone missionary').
card_multiverse_id('ulamog\'s crusher'/'MM2', '397678').

card_in_set('ulamog, the infinite gyre', 'MM2').
card_original_type('ulamog, the infinite gyre'/'MM2', 'Legendary Creature — Eldrazi').
card_original_text('ulamog, the infinite gyre'/'MM2', 'When you cast Ulamog, the Infinite Gyre, destroy target permanent.\nIndestructible\nAnnihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.)\nWhen Ulamog is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_image_name('ulamog, the infinite gyre'/'MM2', 'ulamog, the infinite gyre').
card_uid('ulamog, the infinite gyre'/'MM2', 'MM2:Ulamog, the Infinite Gyre:ulamog, the infinite gyre').
card_rarity('ulamog, the infinite gyre'/'MM2', 'Mythic Rare').
card_artist('ulamog, the infinite gyre'/'MM2', 'Aleksi Briclot').
card_number('ulamog, the infinite gyre'/'MM2', '6').
card_multiverse_id('ulamog, the infinite gyre'/'MM2', '397815').

card_in_set('vampire lacerator', 'MM2').
card_original_type('vampire lacerator'/'MM2', 'Creature — Vampire Warrior').
card_original_text('vampire lacerator'/'MM2', 'At the beginning of your upkeep, you lose 1 life unless an opponent has 10 or less life.').
card_image_name('vampire lacerator'/'MM2', 'vampire lacerator').
card_uid('vampire lacerator'/'MM2', 'MM2:Vampire Lacerator:vampire lacerator').
card_rarity('vampire lacerator'/'MM2', 'Common').
card_artist('vampire lacerator'/'MM2', 'Steve Argyle').
card_number('vampire lacerator'/'MM2', '101').
card_flavor_text('vampire lacerator'/'MM2', 'The lacerators cut themselves before each hunt. They must feed before the sun rises or bleed to death.').
card_multiverse_id('vampire lacerator'/'MM2', '397845').

card_in_set('vampire outcasts', 'MM2').
card_original_type('vampire outcasts'/'MM2', 'Creature — Vampire').
card_original_text('vampire outcasts'/'MM2', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)\nLifelink').
card_image_name('vampire outcasts'/'MM2', 'vampire outcasts').
card_uid('vampire outcasts'/'MM2', 'MM2:Vampire Outcasts:vampire outcasts').
card_rarity('vampire outcasts'/'MM2', 'Uncommon').
card_artist('vampire outcasts'/'MM2', 'Clint Cearley').
card_number('vampire outcasts'/'MM2', '102').
card_flavor_text('vampire outcasts'/'MM2', 'Some vampires reject the decadent trappings of their kind. Once they turn feral, no amount of blood will satisfy their hunger.').
card_multiverse_id('vampire outcasts'/'MM2', '397674').

card_in_set('vapor snag', 'MM2').
card_original_type('vapor snag'/'MM2', 'Instant').
card_original_text('vapor snag'/'MM2', 'Return target creature to its owner\'s hand. Its controller loses 1 life.').
card_image_name('vapor snag'/'MM2', 'vapor snag').
card_uid('vapor snag'/'MM2', 'MM2:Vapor Snag:vapor snag').
card_rarity('vapor snag'/'MM2', 'Common').
card_artist('vapor snag'/'MM2', 'Raymond Swanland').
card_number('vapor snag'/'MM2', '66').
card_flavor_text('vapor snag'/'MM2', '\"This creature is inadequate. Send it to the splicers for innovation.\"\n—Malcator, Executor of Synthesis').
card_multiverse_id('vapor snag'/'MM2', '397738').

card_in_set('vendilion clique', 'MM2').
card_original_type('vendilion clique'/'MM2', 'Legendary Creature — Faerie Wizard').
card_original_text('vendilion clique'/'MM2', 'Flash\nFlying\nWhen Vendilion Clique enters the battlefield, look at target player\'s hand. You may choose a nonland card from it. If you do, that player reveals the chosen card, puts it on the bottom of his or her library, then draws a card.').
card_image_name('vendilion clique'/'MM2', 'vendilion clique').
card_uid('vendilion clique'/'MM2', 'MM2:Vendilion Clique:vendilion clique').
card_rarity('vendilion clique'/'MM2', 'Mythic Rare').
card_artist('vendilion clique'/'MM2', 'Willian Murai').
card_number('vendilion clique'/'MM2', '67').
card_multiverse_id('vendilion clique'/'MM2', '397824').

card_in_set('vengeful rebirth', 'MM2').
card_original_type('vengeful rebirth'/'MM2', 'Sorcery').
card_original_text('vengeful rebirth'/'MM2', 'Return target card from your graveyard to your hand. If you return a nonland card to your hand this way, Vengeful Rebirth deals damage equal to that card\'s converted mana cost to target creature or player.\nExile Vengeful Rebirth.').
card_image_name('vengeful rebirth'/'MM2', 'vengeful rebirth').
card_uid('vengeful rebirth'/'MM2', 'MM2:Vengeful Rebirth:vengeful rebirth').
card_rarity('vengeful rebirth'/'MM2', 'Uncommon').
card_artist('vengeful rebirth'/'MM2', 'Vance Kovacs').
card_number('vengeful rebirth'/'MM2', '188').
card_multiverse_id('vengeful rebirth'/'MM2', '397721').

card_in_set('viashino slaughtermaster', 'MM2').
card_original_type('viashino slaughtermaster'/'MM2', 'Creature — Viashino Warrior').
card_original_text('viashino slaughtermaster'/'MM2', 'Double strike (This creature deals both first-strike and regular combat damage.)\n{B}{G}: Viashino Slaughtermaster gets +1/+1 until end of turn. Activate this ability only once each turn.').
card_image_name('viashino slaughtermaster'/'MM2', 'viashino slaughtermaster').
card_uid('viashino slaughtermaster'/'MM2', 'MM2:Viashino Slaughtermaster:viashino slaughtermaster').
card_rarity('viashino slaughtermaster'/'MM2', 'Common').
card_artist('viashino slaughtermaster'/'MM2', 'Raymond Swanland').
card_number('viashino slaughtermaster'/'MM2', '133').
card_flavor_text('viashino slaughtermaster'/'MM2', '\"I\'ll fight two at once, and then lick their guts from my blades.\"').
card_multiverse_id('viashino slaughtermaster'/'MM2', '397882').

card_in_set('vigean graftmage', 'MM2').
card_original_type('vigean graftmage'/'MM2', 'Creature — Vedalken Wizard Mutant').
card_original_text('vigean graftmage'/'MM2', 'Graft 2 (This creature enters the battlefield with two +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{1}{U}: Untap target creature with a +1/+1 counter on it.').
card_image_name('vigean graftmage'/'MM2', 'vigean graftmage').
card_uid('vigean graftmage'/'MM2', 'MM2:Vigean Graftmage:vigean graftmage').
card_rarity('vigean graftmage'/'MM2', 'Common').
card_artist('vigean graftmage'/'MM2', 'Alan Pollack').
card_number('vigean graftmage'/'MM2', '68').
card_multiverse_id('vigean graftmage'/'MM2', '397829').

card_in_set('vines of vastwood', 'MM2').
card_original_type('vines of vastwood'/'MM2', 'Instant').
card_original_text('vines of vastwood'/'MM2', 'Kicker {G} (You may pay an additional {G} as you cast this spell.)\nTarget creature can\'t be the target of spells or abilities your opponents control this turn. If Vines of Vastwood was kicked, that creature gets +4/+4 until end of turn.').
card_image_name('vines of vastwood'/'MM2', 'vines of vastwood').
card_uid('vines of vastwood'/'MM2', 'MM2:Vines of Vastwood:vines of vastwood').
card_rarity('vines of vastwood'/'MM2', 'Common').
card_artist('vines of vastwood'/'MM2', 'Christopher Moeller').
card_number('vines of vastwood'/'MM2', '168').
card_multiverse_id('vines of vastwood'/'MM2', '397747').

card_in_set('waking nightmare', 'MM2').
card_original_type('waking nightmare'/'MM2', 'Sorcery — Arcane').
card_original_text('waking nightmare'/'MM2', 'Target player discards two cards.').
card_image_name('waking nightmare'/'MM2', 'waking nightmare').
card_uid('waking nightmare'/'MM2', 'MM2:Waking Nightmare:waking nightmare').
card_rarity('waking nightmare'/'MM2', 'Common').
card_artist('waking nightmare'/'MM2', 'Mitch Cotie').
card_number('waking nightmare'/'MM2', '103').
card_flavor_text('waking nightmare'/'MM2', '\"Once each year, the oni and other evil spirits paraded through villages to disturb mortals\' sleep. During the war, this parade became a nightly event.\"\n—Observations of the Kami War').
card_multiverse_id('waking nightmare'/'MM2', '397687').

card_in_set('water servant', 'MM2').
card_original_type('water servant'/'MM2', 'Creature — Elemental').
card_original_text('water servant'/'MM2', '{U}: Water Servant gets +1/-1 until end of turn.\n{U}: Water Servant gets -1/+1 until end of turn.').
card_image_name('water servant'/'MM2', 'water servant').
card_uid('water servant'/'MM2', 'MM2:Water Servant:water servant').
card_rarity('water servant'/'MM2', 'Uncommon').
card_artist('water servant'/'MM2', 'Igor Kieryluk').
card_number('water servant'/'MM2', '69').
card_flavor_text('water servant'/'MM2', '\"This creature has innate perceptiveness. It knows when to rise and when to vanish into the tides.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('water servant'/'MM2', '397694').

card_in_set('waxmane baku', 'MM2').
card_original_type('waxmane baku'/'MM2', 'Creature — Spirit').
card_original_text('waxmane baku'/'MM2', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Waxmane Baku.\n{1}, Remove X ki counters from Waxmane Baku: Tap X target creatures.').
card_image_name('waxmane baku'/'MM2', 'waxmane baku').
card_uid('waxmane baku'/'MM2', 'MM2:Waxmane Baku:waxmane baku').
card_rarity('waxmane baku'/'MM2', 'Common').
card_artist('waxmane baku'/'MM2', 'Greg Hildebrandt').
card_number('waxmane baku'/'MM2', '38').
card_multiverse_id('waxmane baku'/'MM2', '397732').

card_in_set('wayfarer\'s bauble', 'MM2').
card_original_type('wayfarer\'s bauble'/'MM2', 'Artifact').
card_original_text('wayfarer\'s bauble'/'MM2', '{2}, {T}, Sacrifice Wayfarer\'s Bauble: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('wayfarer\'s bauble'/'MM2', 'wayfarer\'s bauble').
card_uid('wayfarer\'s bauble'/'MM2', 'MM2:Wayfarer\'s Bauble:wayfarer\'s bauble').
card_rarity('wayfarer\'s bauble'/'MM2', 'Common').
card_artist('wayfarer\'s bauble'/'MM2', 'Darrell Riche').
card_number('wayfarer\'s bauble'/'MM2', '234').
card_flavor_text('wayfarer\'s bauble'/'MM2', 'It is the forest beyond the horizon, the mountain waiting to be climbed, the new land across the endless sea.').
card_multiverse_id('wayfarer\'s bauble'/'MM2', '397703').

card_in_set('wildfire', 'MM2').
card_original_type('wildfire'/'MM2', 'Sorcery').
card_original_text('wildfire'/'MM2', 'Each player sacrifices four lands. Wildfire deals 4 damage to each creature.').
card_image_name('wildfire'/'MM2', 'wildfire').
card_uid('wildfire'/'MM2', 'MM2:Wildfire:wildfire').
card_rarity('wildfire'/'MM2', 'Rare').
card_artist('wildfire'/'MM2', 'Rob Alexander').
card_number('wildfire'/'MM2', '134').
card_flavor_text('wildfire'/'MM2', 'Fire is always at the top of the food chain, and it has a big appetite.').
card_multiverse_id('wildfire'/'MM2', '397825').

card_in_set('wilt-leaf liege', 'MM2').
card_original_type('wilt-leaf liege'/'MM2', 'Creature — Elf Knight').
card_original_text('wilt-leaf liege'/'MM2', 'Other green creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\nIf a spell or ability an opponent controls causes you to discard Wilt-Leaf Liege, put it onto the battlefield instead of putting it into your graveyard.').
card_image_name('wilt-leaf liege'/'MM2', 'wilt-leaf liege').
card_uid('wilt-leaf liege'/'MM2', 'MM2:Wilt-Leaf Liege:wilt-leaf liege').
card_rarity('wilt-leaf liege'/'MM2', 'Rare').
card_artist('wilt-leaf liege'/'MM2', 'Jason Chan').
card_number('wilt-leaf liege'/'MM2', '200').
card_multiverse_id('wilt-leaf liege'/'MM2', '397852').

card_in_set('wings of velis vel', 'MM2').
card_original_type('wings of velis vel'/'MM2', 'Tribal Instant — Shapeshifter').
card_original_text('wings of velis vel'/'MM2', 'Changeling (This card is every creature type at all times.)\nUntil end of turn, target creature has base power and toughness 4/4, gains all creature types, and gains flying.').
card_image_name('wings of velis vel'/'MM2', 'wings of velis vel').
card_uid('wings of velis vel'/'MM2', 'MM2:Wings of Velis Vel:wings of velis vel').
card_rarity('wings of velis vel'/'MM2', 'Common').
card_artist('wings of velis vel'/'MM2', 'Jim Pavelec').
card_number('wings of velis vel'/'MM2', '70').
card_flavor_text('wings of velis vel'/'MM2', 'Changeling magic grants unusual wishes.').
card_multiverse_id('wings of velis vel'/'MM2', '397897').

card_in_set('wolfbriar elemental', 'MM2').
card_original_type('wolfbriar elemental'/'MM2', 'Creature — Elemental').
card_original_text('wolfbriar elemental'/'MM2', 'Multikicker {G} (You may pay an additional {G} any number of times as you cast this spell.)\nWhen Wolfbriar Elemental enters the battlefield, put a 2/2 green Wolf creature token onto the battlefield for each time it was kicked.').
card_image_name('wolfbriar elemental'/'MM2', 'wolfbriar elemental').
card_uid('wolfbriar elemental'/'MM2', 'MM2:Wolfbriar Elemental:wolfbriar elemental').
card_rarity('wolfbriar elemental'/'MM2', 'Rare').
card_artist('wolfbriar elemental'/'MM2', 'Chippy').
card_number('wolfbriar elemental'/'MM2', '169').
card_multiverse_id('wolfbriar elemental'/'MM2', '397878').

card_in_set('worldheart phoenix', 'MM2').
card_original_type('worldheart phoenix'/'MM2', 'Creature — Phoenix').
card_original_text('worldheart phoenix'/'MM2', 'Flying\nYou may cast Worldheart Phoenix from your graveyard by paying {W}{U}{B}{R}{G} rather than paying its mana cost. If you do, it enters the battlefield with two +1/+1 counters on it.').
card_image_name('worldheart phoenix'/'MM2', 'worldheart phoenix').
card_uid('worldheart phoenix'/'MM2', 'MM2:Worldheart Phoenix:worldheart phoenix').
card_rarity('worldheart phoenix'/'MM2', 'Uncommon').
card_artist('worldheart phoenix'/'MM2', 'Aleksi Briclot').
card_number('worldheart phoenix'/'MM2', '135').
card_multiverse_id('worldheart phoenix'/'MM2', '397724').

card_in_set('wrap in flames', 'MM2').
card_original_type('wrap in flames'/'MM2', 'Sorcery').
card_original_text('wrap in flames'/'MM2', 'Wrap in Flames deals 1 damage to each of up to three target creatures. Those creatures can\'t block this turn.').
card_image_name('wrap in flames'/'MM2', 'wrap in flames').
card_uid('wrap in flames'/'MM2', 'MM2:Wrap in Flames:wrap in flames').
card_rarity('wrap in flames'/'MM2', 'Common').
card_artist('wrap in flames'/'MM2', 'Véronique Meignaud').
card_number('wrap in flames'/'MM2', '136').
card_flavor_text('wrap in flames'/'MM2', '\"Our pyromancer\'s flames may not have killed them, but they bought us the time we needed.\"\n—The War Diaries').
card_multiverse_id('wrap in flames'/'MM2', '397720').

card_in_set('wrecking ball', 'MM2').
card_original_type('wrecking ball'/'MM2', 'Instant').
card_original_text('wrecking ball'/'MM2', 'Destroy target creature or land.').
card_image_name('wrecking ball'/'MM2', 'wrecking ball').
card_uid('wrecking ball'/'MM2', 'MM2:Wrecking Ball:wrecking ball').
card_rarity('wrecking ball'/'MM2', 'Uncommon').
card_artist('wrecking ball'/'MM2', 'Ron Spears').
card_number('wrecking ball'/'MM2', '189').
card_flavor_text('wrecking ball'/'MM2', 'Rakdos festivals almost leave enough rubble in their wake to hide the bodies.').
card_multiverse_id('wrecking ball'/'MM2', '397789').
