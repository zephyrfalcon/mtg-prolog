% Clash Pack

set('CPK').
set_name('CPK', 'Clash Pack').
set_release_date('CPK', '2014-07-18').
set_border('CPK', 'black').
set_type('CPK', 'starter').

card_in_set('courser of kruphix', 'CPK').
card_original_type('courser of kruphix'/'CPK', 'Enchantment Creature — Centaur').
card_original_text('courser of kruphix'/'CPK', '').
card_image_name('courser of kruphix'/'CPK', 'courser of kruphix').
card_uid('courser of kruphix'/'CPK', 'CPK:Courser of Kruphix:courser of kruphix').
card_rarity('courser of kruphix'/'CPK', 'Special').
card_artist('courser of kruphix'/'CPK', 'Slawomir Maniak').
card_number('courser of kruphix'/'CPK', '6').

card_in_set('fated intervention', 'CPK').
card_original_type('fated intervention'/'CPK', 'Instant').
card_original_text('fated intervention'/'CPK', '').
card_image_name('fated intervention'/'CPK', 'fated intervention').
card_uid('fated intervention'/'CPK', 'CPK:Fated Intervention:fated intervention').
card_rarity('fated intervention'/'CPK', 'Special').
card_artist('fated intervention'/'CPK', 'Eric Deschamps').
card_number('fated intervention'/'CPK', '2').

card_in_set('font of fertility', 'CPK').
card_original_type('font of fertility'/'CPK', 'Enchantment').
card_original_text('font of fertility'/'CPK', '').
card_image_name('font of fertility'/'CPK', 'font of fertility').
card_uid('font of fertility'/'CPK', 'CPK:Font of Fertility:font of fertility').
card_rarity('font of fertility'/'CPK', 'Special').
card_artist('font of fertility'/'CPK', 'Jung Park').
card_number('font of fertility'/'CPK', '3').
card_flavor_text('font of fertility'/'CPK', 'Drink deep, and know the path to the heart of the wilds.').

card_in_set('hero\'s downfall', 'CPK').
card_original_type('hero\'s downfall'/'CPK', 'Instant').
card_original_text('hero\'s downfall'/'CPK', '').
card_image_name('hero\'s downfall'/'CPK', 'hero\'s downfall').
card_uid('hero\'s downfall'/'CPK', 'CPK:Hero\'s Downfall:hero\'s downfall').
card_rarity('hero\'s downfall'/'CPK', 'Special').
card_artist('hero\'s downfall'/'CPK', 'Johannes Voss').
card_number('hero\'s downfall'/'CPK', '2').
card_flavor_text('hero\'s downfall'/'CPK', 'Destiny exalts a chosen few, but even heroes break.').

card_in_set('hydra broodmaster', 'CPK').
card_original_type('hydra broodmaster'/'CPK', 'Creature — Hydra').
card_original_text('hydra broodmaster'/'CPK', '').
card_image_name('hydra broodmaster'/'CPK', 'hydra broodmaster').
card_uid('hydra broodmaster'/'CPK', 'CPK:Hydra Broodmaster:hydra broodmaster').
card_rarity('hydra broodmaster'/'CPK', 'Special').
card_artist('hydra broodmaster'/'CPK', 'Vincent Proce').
card_number('hydra broodmaster'/'CPK', '4').

card_in_set('necropolis fiend', 'CPK').
card_original_type('necropolis fiend'/'CPK', 'Creature — Demon').
card_original_text('necropolis fiend'/'CPK', '').
card_image_name('necropolis fiend'/'CPK', 'necropolis fiend').
card_uid('necropolis fiend'/'CPK', 'CPK:Necropolis Fiend:necropolis fiend').
card_rarity('necropolis fiend'/'CPK', 'Special').
card_artist('necropolis fiend'/'CPK', 'Svetlin Velinov').
card_number('necropolis fiend'/'CPK', '1').

card_in_set('prognostic sphinx', 'CPK').
card_original_type('prognostic sphinx'/'CPK', 'Creature — Sphinx').
card_original_text('prognostic sphinx'/'CPK', '').
card_image_name('prognostic sphinx'/'CPK', 'prognostic sphinx').
card_uid('prognostic sphinx'/'CPK', 'CPK:Prognostic Sphinx:prognostic sphinx').
card_rarity('prognostic sphinx'/'CPK', 'Special').
card_artist('prognostic sphinx'/'CPK', 'Jesper Ejsing').
card_number('prognostic sphinx'/'CPK', '1').

card_in_set('prophet of kruphix', 'CPK').
card_original_type('prophet of kruphix'/'CPK', 'Creature — Human Wizard').
card_original_text('prophet of kruphix'/'CPK', '').
card_image_name('prophet of kruphix'/'CPK', 'prophet of kruphix').
card_uid('prophet of kruphix'/'CPK', 'CPK:Prophet of Kruphix:prophet of kruphix').
card_rarity('prophet of kruphix'/'CPK', 'Special').
card_artist('prophet of kruphix'/'CPK', 'Anastasia Ovchinnikova').
card_number('prophet of kruphix'/'CPK', '5').
card_flavor_text('prophet of kruphix'/'CPK', '\"Time is fluid as a dance, and truth as fleeting.\"').

card_in_set('reaper of the wilds', 'CPK').
card_original_type('reaper of the wilds'/'CPK', 'Creature — Gorgon').
card_original_text('reaper of the wilds'/'CPK', '').
card_image_name('reaper of the wilds'/'CPK', 'reaper of the wilds').
card_uid('reaper of the wilds'/'CPK', 'CPK:Reaper of the Wilds:reaper of the wilds').
card_rarity('reaper of the wilds'/'CPK', 'Special').
card_artist('reaper of the wilds'/'CPK', 'Jeff Simpson').
card_number('reaper of the wilds'/'CPK', '4').

card_in_set('sultai ascendancy', 'CPK').
card_original_type('sultai ascendancy'/'CPK', 'Enchantment').
card_original_text('sultai ascendancy'/'CPK', '').
card_image_name('sultai ascendancy'/'CPK', 'sultai ascendancy').
card_uid('sultai ascendancy'/'CPK', 'CPK:Sultai Ascendancy:sultai ascendancy').
card_rarity('sultai ascendancy'/'CPK', 'Special').
card_artist('sultai ascendancy'/'CPK', 'Karl Kopinski').
card_number('sultai ascendancy'/'CPK', '3').

card_in_set('temple of mystery', 'CPK').
card_original_type('temple of mystery'/'CPK', 'Land').
card_original_text('temple of mystery'/'CPK', '').
card_image_name('temple of mystery'/'CPK', 'temple of mystery').
card_uid('temple of mystery'/'CPK', 'CPK:Temple of Mystery:temple of mystery').
card_rarity('temple of mystery'/'CPK', 'Special').
card_artist('temple of mystery'/'CPK', 'Adam Paquette').
card_number('temple of mystery'/'CPK', '6').

card_in_set('whip of erebos', 'CPK').
card_original_type('whip of erebos'/'CPK', 'Legendary Enchantment Artifact').
card_original_text('whip of erebos'/'CPK', '').
card_image_name('whip of erebos'/'CPK', 'whip of erebos').
card_uid('whip of erebos'/'CPK', 'CPK:Whip of Erebos:whip of erebos').
card_rarity('whip of erebos'/'CPK', 'Special').
card_artist('whip of erebos'/'CPK', 'Marco Nelor').
card_number('whip of erebos'/'CPK', '5').
