% Return to Ravnica

set('RTR').
set_name('RTR', 'Return to Ravnica').
set_release_date('RTR', '2012-10-05').
set_border('RTR', 'black').
set_type('RTR', 'expansion').
set_block('RTR', 'Return to Ravnica').

card_in_set('abrupt decay', 'RTR').
card_original_type('abrupt decay'/'RTR', 'Instant').
card_original_text('abrupt decay'/'RTR', 'Abrupt Decay can\'t be countered by spells or abilities.\nDestroy target nonland permanent with converted mana cost 3 or less.').
card_first_print('abrupt decay', 'RTR').
card_image_name('abrupt decay'/'RTR', 'abrupt decay').
card_uid('abrupt decay'/'RTR', 'RTR:Abrupt Decay:abrupt decay').
card_rarity('abrupt decay'/'RTR', 'Rare').
card_artist('abrupt decay'/'RTR', 'Svetlin Velinov').
card_number('abrupt decay'/'RTR', '141').
card_flavor_text('abrupt decay'/'RTR', 'The Izzet quickly suspended their policy of lifetime guarantees.').
card_multiverse_id('abrupt decay'/'RTR', '253561').
card_watermark('abrupt decay'/'RTR', 'Golgari').

card_in_set('aerial predation', 'RTR').
card_original_type('aerial predation'/'RTR', 'Instant').
card_original_text('aerial predation'/'RTR', 'Destroy target creature with flying. You gain 2 life.').
card_first_print('aerial predation', 'RTR').
card_image_name('aerial predation'/'RTR', 'aerial predation').
card_uid('aerial predation'/'RTR', 'RTR:Aerial Predation:aerial predation').
card_rarity('aerial predation'/'RTR', 'Common').
card_artist('aerial predation'/'RTR', 'BD').
card_number('aerial predation'/'RTR', '113').
card_flavor_text('aerial predation'/'RTR', 'In the towering trees of the Samok Stand and the predators that guard them, the might of the Ravnican wild has returned.').
card_multiverse_id('aerial predation'/'RTR', '289227').

card_in_set('angel of serenity', 'RTR').
card_original_type('angel of serenity'/'RTR', 'Creature — Angel').
card_original_text('angel of serenity'/'RTR', 'Flying\nWhen Angel of Serenity enters the battlefield, you may exile up to three other target creatures from the battlefield and/or creature cards from graveyards.\nWhen Angel of Serenity leaves the battlefield, return the exiled cards to their owners\' hands.').
card_first_print('angel of serenity', 'RTR').
card_image_name('angel of serenity'/'RTR', 'angel of serenity').
card_uid('angel of serenity'/'RTR', 'RTR:Angel of Serenity:angel of serenity').
card_rarity('angel of serenity'/'RTR', 'Mythic Rare').
card_artist('angel of serenity'/'RTR', 'Aleksi Briclot').
card_number('angel of serenity'/'RTR', '1').
card_multiverse_id('angel of serenity'/'RTR', '253627').

card_in_set('annihilating fire', 'RTR').
card_original_type('annihilating fire'/'RTR', 'Instant').
card_original_text('annihilating fire'/'RTR', 'Annihilating Fire deals 3 damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.').
card_first_print('annihilating fire', 'RTR').
card_image_name('annihilating fire'/'RTR', 'annihilating fire').
card_uid('annihilating fire'/'RTR', 'RTR:Annihilating Fire:annihilating fire').
card_rarity('annihilating fire'/'RTR', 'Common').
card_artist('annihilating fire'/'RTR', 'Clint Cearley').
card_number('annihilating fire'/'RTR', '85').
card_flavor_text('annihilating fire'/'RTR', '\"The most impressive performances can be done only once.\"\n—Mote, Rakdos madcap').
card_multiverse_id('annihilating fire'/'RTR', '270801').

card_in_set('aquus steed', 'RTR').
card_original_type('aquus steed'/'RTR', 'Creature — Beast').
card_original_text('aquus steed'/'RTR', '{2}{U}, {T}: Target creature gets -2/-0 until end of turn.').
card_first_print('aquus steed', 'RTR').
card_image_name('aquus steed'/'RTR', 'aquus steed').
card_uid('aquus steed'/'RTR', 'RTR:Aquus Steed:aquus steed').
card_rarity('aquus steed'/'RTR', 'Uncommon').
card_artist('aquus steed'/'RTR', 'Warren Mahy').
card_number('aquus steed'/'RTR', '29').
card_flavor_text('aquus steed'/'RTR', 'In water, it\'s as graceful as a dolphin. On land, it darts and jerks so unpredictably that few can ride it for long.').
card_multiverse_id('aquus steed'/'RTR', '270361').

card_in_set('archon of the triumvirate', 'RTR').
card_original_type('archon of the triumvirate'/'RTR', 'Creature — Archon').
card_original_text('archon of the triumvirate'/'RTR', 'Flying\nWhenever Archon of the Triumvirate attacks, detain up to two target nonland permanents your opponents control. (Until your next turn, those permanents can\'t attack or block and their activated abilities can\'t be activated.)').
card_image_name('archon of the triumvirate'/'RTR', 'archon of the triumvirate').
card_uid('archon of the triumvirate'/'RTR', 'RTR:Archon of the Triumvirate:archon of the triumvirate').
card_rarity('archon of the triumvirate'/'RTR', 'Rare').
card_artist('archon of the triumvirate'/'RTR', 'David Rapoza').
card_number('archon of the triumvirate'/'RTR', '142').
card_multiverse_id('archon of the triumvirate'/'RTR', '253603').
card_watermark('archon of the triumvirate'/'RTR', 'Azorius').

card_in_set('archweaver', 'RTR').
card_original_type('archweaver'/'RTR', 'Creature — Spider').
card_original_text('archweaver'/'RTR', 'Reach, trample').
card_first_print('archweaver', 'RTR').
card_image_name('archweaver'/'RTR', 'archweaver').
card_uid('archweaver'/'RTR', 'RTR:Archweaver:archweaver').
card_rarity('archweaver'/'RTR', 'Uncommon').
card_artist('archweaver'/'RTR', 'Jason Felix').
card_number('archweaver'/'RTR', '114').
card_flavor_text('archweaver'/'RTR', 'The silk of the archweavers adds structural integrity to otherwise unstable Izzet building sites.').
card_multiverse_id('archweaver'/'RTR', '253601').

card_in_set('armada wurm', 'RTR').
card_original_type('armada wurm'/'RTR', 'Creature — Wurm').
card_original_text('armada wurm'/'RTR', 'Trample\nWhen Armada Wurm enters the battlefield, put a 5/5 green Wurm creature token with trample onto the battlefield.').
card_first_print('armada wurm', 'RTR').
card_image_name('armada wurm'/'RTR', 'armada wurm').
card_uid('armada wurm'/'RTR', 'RTR:Armada Wurm:armada wurm').
card_rarity('armada wurm'/'RTR', 'Mythic Rare').
card_artist('armada wurm'/'RTR', 'Volkan Baga').
card_number('armada wurm'/'RTR', '143').
card_flavor_text('armada wurm'/'RTR', 'No one in the Conclave acts alone.').
card_multiverse_id('armada wurm'/'RTR', '253587').
card_watermark('armada wurm'/'RTR', 'Selesnya').

card_in_set('armory guard', 'RTR').
card_original_type('armory guard'/'RTR', 'Creature — Giant Soldier').
card_original_text('armory guard'/'RTR', 'Armory Guard has vigilance as long as you control a Gate.').
card_first_print('armory guard', 'RTR').
card_image_name('armory guard'/'RTR', 'armory guard').
card_uid('armory guard'/'RTR', 'RTR:Armory Guard:armory guard').
card_rarity('armory guard'/'RTR', 'Common').
card_artist('armory guard'/'RTR', 'Karl Kopinski').
card_number('armory guard'/'RTR', '2').
card_flavor_text('armory guard'/'RTR', 'The Dimir agents watched from the shadows. \"Eight hours, and I\'ve yet to see him blink,\" Nefara hissed. \"I suggest we find another way in.\"').
card_multiverse_id('armory guard'/'RTR', '253570').

card_in_set('arrest', 'RTR').
card_original_type('arrest'/'RTR', 'Enchantment — Aura').
card_original_text('arrest'/'RTR', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.').
card_image_name('arrest'/'RTR', 'arrest').
card_uid('arrest'/'RTR', 'RTR:Arrest:arrest').
card_rarity('arrest'/'RTR', 'Uncommon').
card_artist('arrest'/'RTR', 'Greg Staples').
card_number('arrest'/'RTR', '3').
card_flavor_text('arrest'/'RTR', '\"We will prove your guilt. We don\'t arrest the innocent, you know.\"\n—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('arrest'/'RTR', '253573').

card_in_set('ash zealot', 'RTR').
card_original_type('ash zealot'/'RTR', 'Creature — Human Warrior').
card_original_text('ash zealot'/'RTR', 'First strike, haste\nWhenever a player casts a spell from a graveyard, Ash Zealot deals 3 damage to that player.').
card_first_print('ash zealot', 'RTR').
card_image_name('ash zealot'/'RTR', 'ash zealot').
card_uid('ash zealot'/'RTR', 'RTR:Ash Zealot:ash zealot').
card_rarity('ash zealot'/'RTR', 'Rare').
card_artist('ash zealot'/'RTR', 'Eric Deschamps').
card_number('ash zealot'/'RTR', '86').
card_flavor_text('ash zealot'/'RTR', '\"Smarter than death? Let\'s see you outsmart my mace, necromancer!\"').
card_multiverse_id('ash zealot'/'RTR', '253623').

card_in_set('assassin\'s strike', 'RTR').
card_original_type('assassin\'s strike'/'RTR', 'Sorcery').
card_original_text('assassin\'s strike'/'RTR', 'Destroy target creature. Its controller discards a card.').
card_first_print('assassin\'s strike', 'RTR').
card_image_name('assassin\'s strike'/'RTR', 'assassin\'s strike').
card_uid('assassin\'s strike'/'RTR', 'RTR:Assassin\'s Strike:assassin\'s strike').
card_rarity('assassin\'s strike'/'RTR', 'Uncommon').
card_artist('assassin\'s strike'/'RTR', 'Chase Stone').
card_number('assassin\'s strike'/'RTR', '57').
card_flavor_text('assassin\'s strike'/'RTR', 'When Selesnya missionaries moved into the Shanav Quarter, they faced scorn, ridicule, and ultimately martyrdom.').
card_multiverse_id('assassin\'s strike'/'RTR', '289216').

card_in_set('auger spree', 'RTR').
card_original_type('auger spree'/'RTR', 'Instant').
card_original_text('auger spree'/'RTR', 'Target creature gets +4/-4 until end of turn.').
card_first_print('auger spree', 'RTR').
card_image_name('auger spree'/'RTR', 'auger spree').
card_uid('auger spree'/'RTR', 'RTR:Auger Spree:auger spree').
card_rarity('auger spree'/'RTR', 'Common').
card_artist('auger spree'/'RTR', 'Raymond Swanland').
card_number('auger spree'/'RTR', '144').
card_flavor_text('auger spree'/'RTR', '\"Finally, a weapon the Boros can\'t confiscate!\"\n—Juri, proprietor of the Juri Revue').
card_multiverse_id('auger spree'/'RTR', '253579').
card_watermark('auger spree'/'RTR', 'Rakdos').

card_in_set('avenging arrow', 'RTR').
card_original_type('avenging arrow'/'RTR', 'Instant').
card_original_text('avenging arrow'/'RTR', 'Destroy target creature that dealt damage this turn.').
card_first_print('avenging arrow', 'RTR').
card_image_name('avenging arrow'/'RTR', 'avenging arrow').
card_uid('avenging arrow'/'RTR', 'RTR:Avenging Arrow:avenging arrow').
card_rarity('avenging arrow'/'RTR', 'Common').
card_artist('avenging arrow'/'RTR', 'James Ryman').
card_number('avenging arrow'/'RTR', '4').
card_flavor_text('avenging arrow'/'RTR', '\"Forgive the theft. Punish the deception.\"\n—Alcarus, Selesnya archer').
card_multiverse_id('avenging arrow'/'RTR', '265410').

card_in_set('axebane guardian', 'RTR').
card_original_type('axebane guardian'/'RTR', 'Creature — Human Druid').
card_original_text('axebane guardian'/'RTR', 'Defender\n{T}: Add X mana in any combination of colors to your mana pool, where X is the number of creatures with defender you control.').
card_first_print('axebane guardian', 'RTR').
card_image_name('axebane guardian'/'RTR', 'axebane guardian').
card_uid('axebane guardian'/'RTR', 'RTR:Axebane Guardian:axebane guardian').
card_rarity('axebane guardian'/'RTR', 'Common').
card_artist('axebane guardian'/'RTR', 'Slawomir Maniak').
card_number('axebane guardian'/'RTR', '115').
card_flavor_text('axebane guardian'/'RTR', 'Only a worthy few are permitted within the sentient forest known as Axebane.').
card_multiverse_id('axebane guardian'/'RTR', '253556').

card_in_set('axebane stag', 'RTR').
card_original_type('axebane stag'/'RTR', 'Creature — Elk').
card_original_text('axebane stag'/'RTR', '').
card_first_print('axebane stag', 'RTR').
card_image_name('axebane stag'/'RTR', 'axebane stag').
card_uid('axebane stag'/'RTR', 'RTR:Axebane Stag:axebane stag').
card_rarity('axebane stag'/'RTR', 'Common').
card_artist('axebane stag'/'RTR', 'Martina Pilcerova').
card_number('axebane stag'/'RTR', '116').
card_flavor_text('axebane stag'/'RTR', '\"When the spires have burned and the cobblestones are dust, he will take his rightful place as king of the wilds.\"\n—Kirce, Axebane guardian').
card_multiverse_id('axebane stag'/'RTR', '265383').

card_in_set('azor\'s elocutors', 'RTR').
card_original_type('azor\'s elocutors'/'RTR', 'Creature — Human Advisor').
card_original_text('azor\'s elocutors'/'RTR', 'At the beginning of your upkeep, put a filibuster counter on Azor\'s Elocutors. Then if Azor\'s Elocutors has five or more filibuster counters on it, you win the game.\nWhenever a source deals damage to you, remove a filibuster counter from Azor\'s Elocutors.').
card_first_print('azor\'s elocutors', 'RTR').
card_image_name('azor\'s elocutors'/'RTR', 'azor\'s elocutors').
card_uid('azor\'s elocutors'/'RTR', 'RTR:Azor\'s Elocutors:azor\'s elocutors').
card_rarity('azor\'s elocutors'/'RTR', 'Rare').
card_artist('azor\'s elocutors'/'RTR', 'Johannes Voss').
card_number('azor\'s elocutors'/'RTR', '210').
card_multiverse_id('azor\'s elocutors'/'RTR', '265418').
card_watermark('azor\'s elocutors'/'RTR', 'Azorius').

card_in_set('azorius arrester', 'RTR').
card_original_type('azorius arrester'/'RTR', 'Creature — Human Soldier').
card_original_text('azorius arrester'/'RTR', 'When Azorius Arrester enters the battlefield, detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)').
card_first_print('azorius arrester', 'RTR').
card_image_name('azorius arrester'/'RTR', 'azorius arrester').
card_uid('azorius arrester'/'RTR', 'RTR:Azorius Arrester:azorius arrester').
card_rarity('azorius arrester'/'RTR', 'Common').
card_artist('azorius arrester'/'RTR', 'Wayne Reynolds').
card_number('azorius arrester'/'RTR', '5').
card_flavor_text('azorius arrester'/'RTR', '\"You have the right to remain silent. Mostly because I tire of your excuses.\"').
card_multiverse_id('azorius arrester'/'RTR', '270972').
card_watermark('azorius arrester'/'RTR', 'Azorius').

card_in_set('azorius charm', 'RTR').
card_original_type('azorius charm'/'RTR', 'Instant').
card_original_text('azorius charm'/'RTR', 'Choose one — Creatures you control gain lifelink until end of turn; or draw a card; or put target attacking or blocking creature on top of its owner\'s library.').
card_first_print('azorius charm', 'RTR').
card_image_name('azorius charm'/'RTR', 'azorius charm').
card_uid('azorius charm'/'RTR', 'RTR:Azorius Charm:azorius charm').
card_rarity('azorius charm'/'RTR', 'Uncommon').
card_artist('azorius charm'/'RTR', 'Zoltan Boros').
card_number('azorius charm'/'RTR', '145').
card_flavor_text('azorius charm'/'RTR', '\"The rules of logic and order have already made the choice for you.\"\n—Isperia').
card_multiverse_id('azorius charm'/'RTR', '270962').
card_watermark('azorius charm'/'RTR', 'Azorius').

card_in_set('azorius guildgate', 'RTR').
card_original_type('azorius guildgate'/'RTR', 'Land — Gate').
card_original_text('azorius guildgate'/'RTR', 'Azorius Guildgate enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('azorius guildgate', 'RTR').
card_image_name('azorius guildgate'/'RTR', 'azorius guildgate').
card_uid('azorius guildgate'/'RTR', 'RTR:Azorius Guildgate:azorius guildgate').
card_rarity('azorius guildgate'/'RTR', 'Common').
card_artist('azorius guildgate'/'RTR', 'Drew Baker').
card_number('azorius guildgate'/'RTR', '237').
card_flavor_text('azorius guildgate'/'RTR', 'Enter the Senate, the seat of justice and the foundation of Ravnican society.').
card_multiverse_id('azorius guildgate'/'RTR', '270966').
card_watermark('azorius guildgate'/'RTR', 'Azorius').

card_in_set('azorius justiciar', 'RTR').
card_original_type('azorius justiciar'/'RTR', 'Creature — Human Wizard').
card_original_text('azorius justiciar'/'RTR', 'When Azorius Justiciar enters the battlefield, detain up to two target creatures your opponents control. (Until your next turn, those creatures can\'t attack or block and their activated abilities can\'t be activated.)').
card_first_print('azorius justiciar', 'RTR').
card_image_name('azorius justiciar'/'RTR', 'azorius justiciar').
card_uid('azorius justiciar'/'RTR', 'RTR:Azorius Justiciar:azorius justiciar').
card_rarity('azorius justiciar'/'RTR', 'Uncommon').
card_artist('azorius justiciar'/'RTR', 'Chris Rahn').
card_number('azorius justiciar'/'RTR', '6').
card_flavor_text('azorius justiciar'/'RTR', '\"Your potential to commit a crime warrants further investigation.\"').
card_multiverse_id('azorius justiciar'/'RTR', '270795').
card_watermark('azorius justiciar'/'RTR', 'Azorius').

card_in_set('azorius keyrune', 'RTR').
card_original_type('azorius keyrune'/'RTR', 'Artifact').
card_original_text('azorius keyrune'/'RTR', '{T}: Add {W} or {U} to your mana pool.\n{W}{U}: Azorius Keyrune becomes a 2/2 white and blue Bird artifact creature with flying until end of turn.').
card_first_print('azorius keyrune', 'RTR').
card_image_name('azorius keyrune'/'RTR', 'azorius keyrune').
card_uid('azorius keyrune'/'RTR', 'RTR:Azorius Keyrune:azorius keyrune').
card_rarity('azorius keyrune'/'RTR', 'Uncommon').
card_artist('azorius keyrune'/'RTR', 'Daniel Ljunggren').
card_number('azorius keyrune'/'RTR', '225').
card_flavor_text('azorius keyrune'/'RTR', '\"The higher the mind soars, the greater its understanding of the law.\"\n—Isperia').
card_multiverse_id('azorius keyrune'/'RTR', '253519').
card_watermark('azorius keyrune'/'RTR', 'Azorius').

card_in_set('batterhorn', 'RTR').
card_original_type('batterhorn'/'RTR', 'Creature — Beast').
card_original_text('batterhorn'/'RTR', 'When Batterhorn enters the battlefield, you may destroy target artifact.').
card_first_print('batterhorn', 'RTR').
card_image_name('batterhorn'/'RTR', 'batterhorn').
card_uid('batterhorn'/'RTR', 'RTR:Batterhorn:batterhorn').
card_rarity('batterhorn'/'RTR', 'Common').
card_artist('batterhorn'/'RTR', 'Dave Kendall').
card_number('batterhorn'/'RTR', '87').
card_flavor_text('batterhorn'/'RTR', 'Novice shopkeeps spend hours deciding how best to display their wares. Veterans focus on portability.').
card_multiverse_id('batterhorn'/'RTR', '265399').

card_in_set('bazaar krovod', 'RTR').
card_original_type('bazaar krovod'/'RTR', 'Creature — Beast').
card_original_text('bazaar krovod'/'RTR', 'Whenever Bazaar Krovod attacks, another target attacking creature gets +0/+2 until end of turn. Untap that creature.').
card_first_print('bazaar krovod', 'RTR').
card_image_name('bazaar krovod'/'RTR', 'bazaar krovod').
card_uid('bazaar krovod'/'RTR', 'RTR:Bazaar Krovod:bazaar krovod').
card_rarity('bazaar krovod'/'RTR', 'Uncommon').
card_artist('bazaar krovod'/'RTR', 'Lars Grant-West').
card_number('bazaar krovod'/'RTR', '7').
card_flavor_text('bazaar krovod'/'RTR', 'The Hauler\'s Collective wields great influence over the Ravnican merchant class. Without it, little moves in the city.').
card_multiverse_id('bazaar krovod'/'RTR', '253589').

card_in_set('bellows lizard', 'RTR').
card_original_type('bellows lizard'/'RTR', 'Creature — Lizard').
card_original_text('bellows lizard'/'RTR', '{1}{R}: Bellows Lizard gets +1/+0 until end of turn.').
card_first_print('bellows lizard', 'RTR').
card_image_name('bellows lizard'/'RTR', 'bellows lizard').
card_uid('bellows lizard'/'RTR', 'RTR:Bellows Lizard:bellows lizard').
card_rarity('bellows lizard'/'RTR', 'Common').
card_artist('bellows lizard'/'RTR', 'Jack Wang').
card_number('bellows lizard'/'RTR', '88').
card_flavor_text('bellows lizard'/'RTR', 'As the price of wood and coal rose, smiths found creative ways to keep their forges burning.').
card_multiverse_id('bellows lizard'/'RTR', '289214').

card_in_set('blistercoil weird', 'RTR').
card_original_type('blistercoil weird'/'RTR', 'Creature — Weird').
card_original_text('blistercoil weird'/'RTR', 'Whenever you cast an instant or sorcery spell, Blistercoil Weird gets +1/+1 until end of turn. Untap it.').
card_first_print('blistercoil weird', 'RTR').
card_image_name('blistercoil weird'/'RTR', 'blistercoil weird').
card_uid('blistercoil weird'/'RTR', 'RTR:Blistercoil Weird:blistercoil weird').
card_rarity('blistercoil weird'/'RTR', 'Uncommon').
card_artist('blistercoil weird'/'RTR', 'Dan Scott').
card_number('blistercoil weird'/'RTR', '211').
card_flavor_text('blistercoil weird'/'RTR', 'Azorius lawmages would like to outlaw the creation of weirds, but first they\'d have to settle their long-standing debate on how to classify them.').
card_multiverse_id('blistercoil weird'/'RTR', '289222').
card_watermark('blistercoil weird'/'RTR', 'Izzet').

card_in_set('blood crypt', 'RTR').
card_original_type('blood crypt'/'RTR', 'Land — Swamp Mountain').
card_original_text('blood crypt'/'RTR', '({T}: Add {B} or {R} to your mana pool.)\nAs Blood Crypt enters the battlefield, you may pay 2 life. If you don\'t, Blood Crypt enters the battlefield tapped.').
card_image_name('blood crypt'/'RTR', 'blood crypt').
card_uid('blood crypt'/'RTR', 'RTR:Blood Crypt:blood crypt').
card_rarity('blood crypt'/'RTR', 'Rare').
card_artist('blood crypt'/'RTR', 'Vincent Proce').
card_number('blood crypt'/'RTR', '238').
card_flavor_text('blood crypt'/'RTR', 'Where the dead serve as diversion, decor, and dessert.').
card_multiverse_id('blood crypt'/'RTR', '253683').
card_watermark('blood crypt'/'RTR', 'Rakdos').

card_in_set('bloodfray giant', 'RTR').
card_original_type('bloodfray giant'/'RTR', 'Creature — Giant').
card_original_text('bloodfray giant'/'RTR', 'Trample\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_first_print('bloodfray giant', 'RTR').
card_image_name('bloodfray giant'/'RTR', 'bloodfray giant').
card_uid('bloodfray giant'/'RTR', 'RTR:Bloodfray Giant:bloodfray giant').
card_rarity('bloodfray giant'/'RTR', 'Uncommon').
card_artist('bloodfray giant'/'RTR', 'Steve Argyle').
card_number('bloodfray giant'/'RTR', '89').
card_flavor_text('bloodfray giant'/'RTR', 'The star performer of the Juri Revue, his shows have fans and blood spilling into the street.').
card_multiverse_id('bloodfray giant'/'RTR', '270785').
card_watermark('bloodfray giant'/'RTR', 'Rakdos').

card_in_set('blustersquall', 'RTR').
card_original_type('blustersquall'/'RTR', 'Instant').
card_original_text('blustersquall'/'RTR', 'Tap target creature you don\'t control.\nOverload {3}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('blustersquall', 'RTR').
card_image_name('blustersquall'/'RTR', 'blustersquall').
card_uid('blustersquall'/'RTR', 'RTR:Blustersquall:blustersquall').
card_rarity('blustersquall'/'RTR', 'Uncommon').
card_artist('blustersquall'/'RTR', 'Willian Murai').
card_number('blustersquall'/'RTR', '30').
card_flavor_text('blustersquall'/'RTR', 'Weather is more predictable than the Izzet.').
card_multiverse_id('blustersquall'/'RTR', '253582').
card_watermark('blustersquall'/'RTR', 'Izzet').

card_in_set('brushstrider', 'RTR').
card_original_type('brushstrider'/'RTR', 'Creature — Beast').
card_original_text('brushstrider'/'RTR', 'Vigilance').
card_first_print('brushstrider', 'RTR').
card_image_name('brushstrider'/'RTR', 'brushstrider').
card_uid('brushstrider'/'RTR', 'RTR:Brushstrider:brushstrider').
card_rarity('brushstrider'/'RTR', 'Uncommon').
card_artist('brushstrider'/'RTR', 'Raoul Vitale').
card_number('brushstrider'/'RTR', '117').
card_flavor_text('brushstrider'/'RTR', 'Magistrate Ludy agreed to designate land for the brushstriders only after several broken windows and dozens of missing blini-cakes.').
card_multiverse_id('brushstrider'/'RTR', '265377').

card_in_set('call of the conclave', 'RTR').
card_original_type('call of the conclave'/'RTR', 'Sorcery').
card_original_text('call of the conclave'/'RTR', 'Put a 3/3 green Centaur creature token onto the battlefield.').
card_image_name('call of the conclave'/'RTR', 'call of the conclave').
card_uid('call of the conclave'/'RTR', 'RTR:Call of the Conclave:call of the conclave').
card_rarity('call of the conclave'/'RTR', 'Uncommon').
card_artist('call of the conclave'/'RTR', 'Terese Nielsen').
card_number('call of the conclave'/'RTR', '146').
card_flavor_text('call of the conclave'/'RTR', 'Centaurs are sent to evangelize in Gruul territories where words of war speak louder than prayers of peace.').
card_multiverse_id('call of the conclave'/'RTR', '261322').
card_watermark('call of the conclave'/'RTR', 'Selesnya').

card_in_set('cancel', 'RTR').
card_original_type('cancel'/'RTR', 'Instant').
card_original_text('cancel'/'RTR', 'Counter target spell.').
card_image_name('cancel'/'RTR', 'cancel').
card_uid('cancel'/'RTR', 'RTR:Cancel:cancel').
card_rarity('cancel'/'RTR', 'Common').
card_artist('cancel'/'RTR', 'Karl Kopinski').
card_number('cancel'/'RTR', '31').
card_flavor_text('cancel'/'RTR', '\"It is forbidden. Asking why is irrelevant.\"\n—Sergiu, Opal Lake magistrate').
card_multiverse_id('cancel'/'RTR', '265403').

card_in_set('carnival hellsteed', 'RTR').
card_original_type('carnival hellsteed'/'RTR', 'Creature — Nightmare Horse').
card_original_text('carnival hellsteed'/'RTR', 'First strike, haste\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_image_name('carnival hellsteed'/'RTR', 'carnival hellsteed').
card_uid('carnival hellsteed'/'RTR', 'RTR:Carnival Hellsteed:carnival hellsteed').
card_rarity('carnival hellsteed'/'RTR', 'Rare').
card_artist('carnival hellsteed'/'RTR', 'Chase Stone').
card_number('carnival hellsteed'/'RTR', '147').
card_flavor_text('carnival hellsteed'/'RTR', 'Its favorite treats are candied hands and sweet hearts.').
card_multiverse_id('carnival hellsteed'/'RTR', '270963').
card_watermark('carnival hellsteed'/'RTR', 'Rakdos').

card_in_set('catacomb slug', 'RTR').
card_original_type('catacomb slug'/'RTR', 'Creature — Slug').
card_original_text('catacomb slug'/'RTR', '').
card_first_print('catacomb slug', 'RTR').
card_image_name('catacomb slug'/'RTR', 'catacomb slug').
card_uid('catacomb slug'/'RTR', 'RTR:Catacomb Slug:catacomb slug').
card_rarity('catacomb slug'/'RTR', 'Common').
card_artist('catacomb slug'/'RTR', 'Nils Hamm').
card_number('catacomb slug'/'RTR', '58').
card_flavor_text('catacomb slug'/'RTR', '\"The entire murder scene was covered in dripping, oozing slime. No need for a soothsayer to solve that one.\"\n—Pel Javya, Wojek investigator').
card_multiverse_id('catacomb slug'/'RTR', '253525').

card_in_set('centaur healer', 'RTR').
card_original_type('centaur healer'/'RTR', 'Creature — Centaur Cleric').
card_original_text('centaur healer'/'RTR', 'When Centaur Healer enters the battlefield, you gain 3 life.').
card_first_print('centaur healer', 'RTR').
card_image_name('centaur healer'/'RTR', 'centaur healer').
card_uid('centaur healer'/'RTR', 'RTR:Centaur Healer:centaur healer').
card_rarity('centaur healer'/'RTR', 'Common').
card_artist('centaur healer'/'RTR', 'Mark Zug').
card_number('centaur healer'/'RTR', '148').
card_flavor_text('centaur healer'/'RTR', 'Instructors at the Kasarna training grounds are capable healers in case their students fail to grasp the subtleties of combat.').
card_multiverse_id('centaur healer'/'RTR', '253654').
card_watermark('centaur healer'/'RTR', 'Selesnya').

card_in_set('centaur\'s herald', 'RTR').
card_original_type('centaur\'s herald'/'RTR', 'Creature — Elf Scout').
card_original_text('centaur\'s herald'/'RTR', '{2}{G}, Sacrifice Centaur\'s Herald: Put a 3/3 green Centaur creature token onto the battlefield.').
card_first_print('centaur\'s herald', 'RTR').
card_image_name('centaur\'s herald'/'RTR', 'centaur\'s herald').
card_uid('centaur\'s herald'/'RTR', 'RTR:Centaur\'s Herald:centaur\'s herald').
card_rarity('centaur\'s herald'/'RTR', 'Common').
card_artist('centaur\'s herald'/'RTR', 'Howard Lyon').
card_number('centaur\'s herald'/'RTR', '118').
card_flavor_text('centaur\'s herald'/'RTR', 'The farther they go from Vitu-Ghazi, the less willing the crowd is to part for them.').
card_multiverse_id('centaur\'s herald'/'RTR', '265387').

card_in_set('chaos imps', 'RTR').
card_original_type('chaos imps'/'RTR', 'Creature — Imp').
card_original_text('chaos imps'/'RTR', 'Flying\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\nChaos Imps has trample as long as it has a +1/+1 counter on it.').
card_first_print('chaos imps', 'RTR').
card_image_name('chaos imps'/'RTR', 'chaos imps').
card_uid('chaos imps'/'RTR', 'RTR:Chaos Imps:chaos imps').
card_rarity('chaos imps'/'RTR', 'Rare').
card_artist('chaos imps'/'RTR', 'Tyler Jacobson').
card_number('chaos imps'/'RTR', '90').
card_multiverse_id('chaos imps'/'RTR', '270954').
card_watermark('chaos imps'/'RTR', 'Rakdos').

card_in_set('chemister\'s trick', 'RTR').
card_original_type('chemister\'s trick'/'RTR', 'Instant').
card_original_text('chemister\'s trick'/'RTR', 'Target creature you don\'t control gets -2/-0 until end of turn and attacks this turn if able.\nOverload {3}{U}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('chemister\'s trick', 'RTR').
card_image_name('chemister\'s trick'/'RTR', 'chemister\'s trick').
card_uid('chemister\'s trick'/'RTR', 'RTR:Chemister\'s Trick:chemister\'s trick').
card_rarity('chemister\'s trick'/'RTR', 'Common').
card_artist('chemister\'s trick'/'RTR', 'Christopher Moeller').
card_number('chemister\'s trick'/'RTR', '149').
card_multiverse_id('chemister\'s trick'/'RTR', '290524').
card_watermark('chemister\'s trick'/'RTR', 'Izzet').

card_in_set('chorus of might', 'RTR').
card_original_type('chorus of might'/'RTR', 'Instant').
card_original_text('chorus of might'/'RTR', 'Until end of turn, target creature gets +1/+1 for each creature you control and gains trample.').
card_first_print('chorus of might', 'RTR').
card_image_name('chorus of might'/'RTR', 'chorus of might').
card_uid('chorus of might'/'RTR', 'RTR:Chorus of Might:chorus of might').
card_rarity('chorus of might'/'RTR', 'Common').
card_artist('chorus of might'/'RTR', 'Christopher Moeller').
card_number('chorus of might'/'RTR', '119').
card_flavor_text('chorus of might'/'RTR', '\"In each of us is the strength of all of us.\"\n—Trostani').
card_multiverse_id('chorus of might'/'RTR', '265373').

card_in_set('chromatic lantern', 'RTR').
card_original_type('chromatic lantern'/'RTR', 'Artifact').
card_original_text('chromatic lantern'/'RTR', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"\n{T}: Add one mana of any color to your mana pool.').
card_first_print('chromatic lantern', 'RTR').
card_image_name('chromatic lantern'/'RTR', 'chromatic lantern').
card_uid('chromatic lantern'/'RTR', 'RTR:Chromatic Lantern:chromatic lantern').
card_rarity('chromatic lantern'/'RTR', 'Rare').
card_artist('chromatic lantern'/'RTR', 'Jung Park').
card_number('chromatic lantern'/'RTR', '226').
card_flavor_text('chromatic lantern'/'RTR', 'Dimir mages put the lanterns to good use, creating shapeshifters and sleeper agents from mana foreign to them.').
card_multiverse_id('chromatic lantern'/'RTR', '290542').

card_in_set('chronic flooding', 'RTR').
card_original_type('chronic flooding'/'RTR', 'Enchantment — Aura').
card_original_text('chronic flooding'/'RTR', 'Enchant land\nWhenever enchanted land becomes tapped, its controller puts the top three cards of his or her library into his or her graveyard.').
card_first_print('chronic flooding', 'RTR').
card_image_name('chronic flooding'/'RTR', 'chronic flooding').
card_uid('chronic flooding'/'RTR', 'RTR:Chronic Flooding:chronic flooding').
card_rarity('chronic flooding'/'RTR', 'Common').
card_artist('chronic flooding'/'RTR', 'Scott Chou').
card_number('chronic flooding'/'RTR', '32').
card_flavor_text('chronic flooding'/'RTR', 'With the Izzet occupied elsewhere, many of their public works fell into disrepair.').
card_multiverse_id('chronic flooding'/'RTR', '270786').

card_in_set('civic saber', 'RTR').
card_original_type('civic saber'/'RTR', 'Artifact — Equipment').
card_original_text('civic saber'/'RTR', 'Equipped creature gets +1/+0 for each of its colors.\nEquip {1}').
card_first_print('civic saber', 'RTR').
card_image_name('civic saber'/'RTR', 'civic saber').
card_uid('civic saber'/'RTR', 'RTR:Civic Saber:civic saber').
card_rarity('civic saber'/'RTR', 'Uncommon').
card_artist('civic saber'/'RTR', 'Jung Park').
card_number('civic saber'/'RTR', '227').
card_flavor_text('civic saber'/'RTR', 'Those without a guild signet often display a different form of protection.').
card_multiverse_id('civic saber'/'RTR', '253619').

card_in_set('cobblebrute', 'RTR').
card_original_type('cobblebrute'/'RTR', 'Creature — Elemental').
card_original_text('cobblebrute'/'RTR', '').
card_first_print('cobblebrute', 'RTR').
card_image_name('cobblebrute'/'RTR', 'cobblebrute').
card_uid('cobblebrute'/'RTR', 'RTR:Cobblebrute:cobblebrute').
card_rarity('cobblebrute'/'RTR', 'Common').
card_artist('cobblebrute'/'RTR', 'Eytan Zana').
card_number('cobblebrute'/'RTR', '91').
card_flavor_text('cobblebrute'/'RTR', 'The most ancient streets take on a life of their own. A few have decided to move to nicer neighborhoods.').
card_multiverse_id('cobblebrute'/'RTR', '265369').

card_in_set('codex shredder', 'RTR').
card_original_type('codex shredder'/'RTR', 'Artifact').
card_original_text('codex shredder'/'RTR', '{T}: Target player puts the top card of his or her library into his or her graveyard.\n{5}, {T}, Sacrifice Codex Shredder: Return target card from your graveyard to your hand.').
card_first_print('codex shredder', 'RTR').
card_image_name('codex shredder'/'RTR', 'codex shredder').
card_uid('codex shredder'/'RTR', 'RTR:Codex Shredder:codex shredder').
card_rarity('codex shredder'/'RTR', 'Uncommon').
card_artist('codex shredder'/'RTR', 'Jason Felix').
card_number('codex shredder'/'RTR', '228').
card_multiverse_id('codex shredder'/'RTR', '253635').

card_in_set('collective blessing', 'RTR').
card_original_type('collective blessing'/'RTR', 'Enchantment').
card_original_text('collective blessing'/'RTR', 'Creatures you control get +3/+3.').
card_first_print('collective blessing', 'RTR').
card_image_name('collective blessing'/'RTR', 'collective blessing').
card_uid('collective blessing'/'RTR', 'RTR:Collective Blessing:collective blessing').
card_rarity('collective blessing'/'RTR', 'Rare').
card_artist('collective blessing'/'RTR', 'Svetlin Velinov').
card_number('collective blessing'/'RTR', '150').
card_flavor_text('collective blessing'/'RTR', 'Senators of Azorius often hired agents to spy on the Selesnya. They were told to record every spore and root they saw, as each could become a deadly foe.').
card_multiverse_id('collective blessing'/'RTR', '290528').
card_watermark('collective blessing'/'RTR', 'Selesnya').

card_in_set('common bond', 'RTR').
card_original_type('common bond'/'RTR', 'Instant').
card_original_text('common bond'/'RTR', 'Put a +1/+1 counter on target creature.\nPut a +1/+1 counter on target creature.').
card_first_print('common bond', 'RTR').
card_image_name('common bond'/'RTR', 'common bond').
card_uid('common bond'/'RTR', 'RTR:Common Bond:common bond').
card_rarity('common bond'/'RTR', 'Common').
card_artist('common bond'/'RTR', 'Raymond Swanland').
card_number('common bond'/'RTR', '151').
card_flavor_text('common bond'/'RTR', 'Wolf riders hone their skills traversing the perilous rooftops, each dizzying step bringing elf and wolf closer together.').
card_multiverse_id('common bond'/'RTR', '253565').
card_watermark('common bond'/'RTR', 'Selesnya').

card_in_set('concordia pegasus', 'RTR').
card_original_type('concordia pegasus'/'RTR', 'Creature — Pegasus').
card_original_text('concordia pegasus'/'RTR', 'Flying').
card_first_print('concordia pegasus', 'RTR').
card_image_name('concordia pegasus'/'RTR', 'concordia pegasus').
card_uid('concordia pegasus'/'RTR', 'RTR:Concordia Pegasus:concordia pegasus').
card_rarity('concordia pegasus'/'RTR', 'Common').
card_artist('concordia pegasus'/'RTR', 'Winona Nelson').
card_number('concordia pegasus'/'RTR', '8').
card_flavor_text('concordia pegasus'/'RTR', '\"A kick from its hooves is like a bolt of lightning. I\'d know. I\'ve been hit by both.\"\n—Rencz, Izzet chemister\'s aide').
card_multiverse_id('concordia pegasus'/'RTR', '253558').

card_in_set('conjured currency', 'RTR').
card_original_type('conjured currency'/'RTR', 'Enchantment').
card_original_text('conjured currency'/'RTR', 'At the beginning of your upkeep, you may exchange control of Conjured Currency and target permanent you neither own nor control.').
card_first_print('conjured currency', 'RTR').
card_image_name('conjured currency'/'RTR', 'conjured currency').
card_uid('conjured currency'/'RTR', 'RTR:Conjured Currency:conjured currency').
card_rarity('conjured currency'/'RTR', 'Rare').
card_artist('conjured currency'/'RTR', 'Steve Argyle').
card_number('conjured currency'/'RTR', '33').
card_flavor_text('conjured currency'/'RTR', 'A bargain in Keyhole Downs is always too good to be true.').
card_multiverse_id('conjured currency'/'RTR', '253606').

card_in_set('corpsejack menace', 'RTR').
card_original_type('corpsejack menace'/'RTR', 'Creature — Fungus').
card_original_text('corpsejack menace'/'RTR', 'If one or more +1/+1 counters would be placed on a creature you control, twice that many +1/+1 counters are placed on it instead.').
card_image_name('corpsejack menace'/'RTR', 'corpsejack menace').
card_uid('corpsejack menace'/'RTR', 'RTR:Corpsejack Menace:corpsejack menace').
card_rarity('corpsejack menace'/'RTR', 'Rare').
card_artist('corpsejack menace'/'RTR', 'Chris Rahn').
card_number('corpsejack menace'/'RTR', '152').
card_flavor_text('corpsejack menace'/'RTR', 'Weakness is not in the nature of the Swarm.').
card_multiverse_id('corpsejack menace'/'RTR', '253533').
card_watermark('corpsejack menace'/'RTR', 'Golgari').

card_in_set('counterflux', 'RTR').
card_original_type('counterflux'/'RTR', 'Instant').
card_original_text('counterflux'/'RTR', 'Counterflux can\'t be countered by spells or abilities.\nCounter target spell you don\'t control.\nOverload {1}{U}{U}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('counterflux', 'RTR').
card_image_name('counterflux'/'RTR', 'counterflux').
card_uid('counterflux'/'RTR', 'RTR:Counterflux:counterflux').
card_rarity('counterflux'/'RTR', 'Rare').
card_artist('counterflux'/'RTR', 'Scott M. Fischer').
card_number('counterflux'/'RTR', '153').
card_multiverse_id('counterflux'/'RTR', '253524').
card_watermark('counterflux'/'RTR', 'Izzet').

card_in_set('coursers\' accord', 'RTR').
card_original_type('coursers\' accord'/'RTR', 'Sorcery').
card_original_text('coursers\' accord'/'RTR', 'Put a 3/3 green Centaur creature token onto the battlefield, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('coursers\' accord', 'RTR').
card_image_name('coursers\' accord'/'RTR', 'coursers\' accord').
card_uid('coursers\' accord'/'RTR', 'RTR:Coursers\' Accord:coursers\' accord').
card_rarity('coursers\' accord'/'RTR', 'Common').
card_artist('coursers\' accord'/'RTR', 'Nils Hamm').
card_number('coursers\' accord'/'RTR', '154').
card_flavor_text('coursers\' accord'/'RTR', 'Bleak rumors reached Trostani\'s ears. She decreed that no one should travel alone.').
card_multiverse_id('coursers\' accord'/'RTR', '253639').
card_watermark('coursers\' accord'/'RTR', 'Selesnya').

card_in_set('cremate', 'RTR').
card_original_type('cremate'/'RTR', 'Instant').
card_original_text('cremate'/'RTR', 'Exile target card from a graveyard.\nDraw a card.').
card_image_name('cremate'/'RTR', 'cremate').
card_uid('cremate'/'RTR', 'RTR:Cremate:cremate').
card_rarity('cremate'/'RTR', 'Common').
card_artist('cremate'/'RTR', 'Cynthia Sheppard').
card_number('cremate'/'RTR', '59').
card_flavor_text('cremate'/'RTR', '\"Without a body, the soul is helpless to fight the terms of our contracts.\"\n—Somhaur, Orzhov pontiff').
card_multiverse_id('cremate'/'RTR', '277990').

card_in_set('crosstown courier', 'RTR').
card_original_type('crosstown courier'/'RTR', 'Creature — Vedalken').
card_original_text('crosstown courier'/'RTR', 'Whenever Crosstown Courier deals combat damage to a player, that player puts that many cards from the top of his or her library into his or her graveyard.').
card_first_print('crosstown courier', 'RTR').
card_image_name('crosstown courier'/'RTR', 'crosstown courier').
card_uid('crosstown courier'/'RTR', 'RTR:Crosstown Courier:crosstown courier').
card_rarity('crosstown courier'/'RTR', 'Common').
card_artist('crosstown courier'/'RTR', 'Chase Stone').
card_number('crosstown courier'/'RTR', '34').
card_flavor_text('crosstown courier'/'RTR', 'Information travels quickly through Ravnica\'s network of messengers and thought agents.').
card_multiverse_id('crosstown courier'/'RTR', '277991').

card_in_set('cryptborn horror', 'RTR').
card_original_type('cryptborn horror'/'RTR', 'Creature — Horror').
card_original_text('cryptborn horror'/'RTR', 'Trample\nCryptborn Horror enters the battlefield with X +1/+1 counters on it, where X is the total life lost by your opponents this turn.').
card_image_name('cryptborn horror'/'RTR', 'cryptborn horror').
card_uid('cryptborn horror'/'RTR', 'RTR:Cryptborn Horror:cryptborn horror').
card_rarity('cryptborn horror'/'RTR', 'Rare').
card_artist('cryptborn horror'/'RTR', 'Richard Wright').
card_number('cryptborn horror'/'RTR', '212').
card_flavor_text('cryptborn horror'/'RTR', 'Although Cryptwitch Merree vanished after a spat with Rakdos, her playthings lingered.').
card_multiverse_id('cryptborn horror'/'RTR', '265411').
card_watermark('cryptborn horror'/'RTR', 'Rakdos').

card_in_set('cyclonic rift', 'RTR').
card_original_type('cyclonic rift'/'RTR', 'Instant').
card_original_text('cyclonic rift'/'RTR', 'Return target nonland permanent you don\'t control to its owner\'s hand.\nOverload {6}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('cyclonic rift', 'RTR').
card_image_name('cyclonic rift'/'RTR', 'cyclonic rift').
card_uid('cyclonic rift'/'RTR', 'RTR:Cyclonic Rift:cyclonic rift').
card_rarity('cyclonic rift'/'RTR', 'Rare').
card_artist('cyclonic rift'/'RTR', 'Chris Rahn').
card_number('cyclonic rift'/'RTR', '35').
card_flavor_text('cyclonic rift'/'RTR', 'The Izzet specialize in unnatural disaster.').
card_multiverse_id('cyclonic rift'/'RTR', '270798').
card_watermark('cyclonic rift'/'RTR', 'Izzet').

card_in_set('daggerdrome imp', 'RTR').
card_original_type('daggerdrome imp'/'RTR', 'Creature — Imp').
card_original_text('daggerdrome imp'/'RTR', 'Flying\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('daggerdrome imp', 'RTR').
card_image_name('daggerdrome imp'/'RTR', 'daggerdrome imp').
card_uid('daggerdrome imp'/'RTR', 'RTR:Daggerdrome Imp:daggerdrome imp').
card_rarity('daggerdrome imp'/'RTR', 'Common').
card_artist('daggerdrome imp'/'RTR', 'Jack Wang').
card_number('daggerdrome imp'/'RTR', '60').
card_flavor_text('daggerdrome imp'/'RTR', 'One of the many reasons why open-air markets close at dusk.').
card_multiverse_id('daggerdrome imp'/'RTR', '270349').

card_in_set('dark revenant', 'RTR').
card_original_type('dark revenant'/'RTR', 'Creature — Spirit').
card_original_text('dark revenant'/'RTR', 'Flying\nWhen Dark Revenant dies, put it on top of its owner\'s library.').
card_first_print('dark revenant', 'RTR').
card_image_name('dark revenant'/'RTR', 'dark revenant').
card_uid('dark revenant'/'RTR', 'RTR:Dark Revenant:dark revenant').
card_rarity('dark revenant'/'RTR', 'Uncommon').
card_artist('dark revenant'/'RTR', 'Daarken').
card_number('dark revenant'/'RTR', '61').
card_flavor_text('dark revenant'/'RTR', 'The murderer faced justice in the Azorius courts, but his soul was set free on a technicality.').
card_multiverse_id('dark revenant'/'RTR', '270368').

card_in_set('dead reveler', 'RTR').
card_original_type('dead reveler'/'RTR', 'Creature — Zombie').
card_original_text('dead reveler'/'RTR', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_first_print('dead reveler', 'RTR').
card_image_name('dead reveler'/'RTR', 'dead reveler').
card_uid('dead reveler'/'RTR', 'RTR:Dead Reveler:dead reveler').
card_rarity('dead reveler'/'RTR', 'Common').
card_artist('dead reveler'/'RTR', 'David Palumbo').
card_number('dead reveler'/'RTR', '62').
card_flavor_text('dead reveler'/'RTR', 'He\'s the unlife of the party.').
card_multiverse_id('dead reveler'/'RTR', '253640').
card_watermark('dead reveler'/'RTR', 'Rakdos').

card_in_set('deadbridge goliath', 'RTR').
card_original_type('deadbridge goliath'/'RTR', 'Creature — Insect').
card_original_text('deadbridge goliath'/'RTR', 'Scavenge {4}{G}{G} ({4}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_image_name('deadbridge goliath'/'RTR', 'deadbridge goliath').
card_uid('deadbridge goliath'/'RTR', 'RTR:Deadbridge Goliath:deadbridge goliath').
card_rarity('deadbridge goliath'/'RTR', 'Rare').
card_artist('deadbridge goliath'/'RTR', 'Chase Stone').
card_number('deadbridge goliath'/'RTR', '120').
card_flavor_text('deadbridge goliath'/'RTR', 'Some Golgari insects live for centuries—and they never stop growing.').
card_multiverse_id('deadbridge goliath'/'RTR', '270970').
card_watermark('deadbridge goliath'/'RTR', 'Golgari').

card_in_set('death\'s presence', 'RTR').
card_original_type('death\'s presence'/'RTR', 'Enchantment').
card_original_text('death\'s presence'/'RTR', 'Whenever a creature you control dies, put X +1/+1 counters on target creature you control, where X is the power of the creature that died.').
card_first_print('death\'s presence', 'RTR').
card_image_name('death\'s presence'/'RTR', 'death\'s presence').
card_uid('death\'s presence'/'RTR', 'RTR:Death\'s Presence:death\'s presence').
card_rarity('death\'s presence'/'RTR', 'Rare').
card_artist('death\'s presence'/'RTR', 'Ryan Barger').
card_number('death\'s presence'/'RTR', '121').
card_flavor_text('death\'s presence'/'RTR', '\"We traffic in flesh, not souls. Still, it\'s a shame to let anything go to waste.\"\n—Cevraya, Golgari shaman').
card_multiverse_id('death\'s presence'/'RTR', '270793').

card_in_set('deathrite shaman', 'RTR').
card_original_type('deathrite shaman'/'RTR', 'Creature — Elf Shaman').
card_original_text('deathrite shaman'/'RTR', '{T}: Exile target land card from a graveyard. Add one mana of any color to your mana pool.\n{B}, {T}: Exile target instant or sorcery card from a graveyard. Each opponent loses 2 life.\n{G}, {T}: Exile target creature card from a graveyard. You gain 2 life.').
card_first_print('deathrite shaman', 'RTR').
card_image_name('deathrite shaman'/'RTR', 'deathrite shaman').
card_uid('deathrite shaman'/'RTR', 'RTR:Deathrite Shaman:deathrite shaman').
card_rarity('deathrite shaman'/'RTR', 'Rare').
card_artist('deathrite shaman'/'RTR', 'Steve Argyle').
card_number('deathrite shaman'/'RTR', '213').
card_multiverse_id('deathrite shaman'/'RTR', '290529').
card_watermark('deathrite shaman'/'RTR', 'Golgari').

card_in_set('desecration demon', 'RTR').
card_original_type('desecration demon'/'RTR', 'Creature — Demon').
card_original_text('desecration demon'/'RTR', 'Flying\nAt the beginning of each combat, any opponent may sacrifice a creature. If a player does, tap Desecration Demon and put a +1/+1 counter on it.').
card_first_print('desecration demon', 'RTR').
card_image_name('desecration demon'/'RTR', 'desecration demon').
card_uid('desecration demon'/'RTR', 'RTR:Desecration Demon:desecration demon').
card_rarity('desecration demon'/'RTR', 'Rare').
card_artist('desecration demon'/'RTR', 'Jason Chan').
card_number('desecration demon'/'RTR', '63').
card_multiverse_id('desecration demon'/'RTR', '270959').

card_in_set('destroy the evidence', 'RTR').
card_original_type('destroy the evidence'/'RTR', 'Sorcery').
card_original_text('destroy the evidence'/'RTR', 'Destroy target land. Its controller reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_first_print('destroy the evidence', 'RTR').
card_image_name('destroy the evidence'/'RTR', 'destroy the evidence').
card_uid('destroy the evidence'/'RTR', 'RTR:Destroy the Evidence:destroy the evidence').
card_rarity('destroy the evidence'/'RTR', 'Common').
card_artist('destroy the evidence'/'RTR', 'Clint Cearley').
card_number('destroy the evidence'/'RTR', '64').
card_flavor_text('destroy the evidence'/'RTR', 'Everyone knew the Dimir had done the damage. Everyone suspected the Izzet had hired them.').
card_multiverse_id('destroy the evidence'/'RTR', '253571').

card_in_set('detention sphere', 'RTR').
card_original_type('detention sphere'/'RTR', 'Enchantment').
card_original_text('detention sphere'/'RTR', 'When Detention Sphere enters the battlefield, you may exile target nonland permanent not named Detention Sphere and all other permanents with the same name as that permanent.\nWhen Detention Sphere leaves the battlefield, return the exiled cards to the battlefield under their owner\'s control.').
card_first_print('detention sphere', 'RTR').
card_image_name('detention sphere'/'RTR', 'detention sphere').
card_uid('detention sphere'/'RTR', 'RTR:Detention Sphere:detention sphere').
card_rarity('detention sphere'/'RTR', 'Rare').
card_artist('detention sphere'/'RTR', 'Kev Walker').
card_number('detention sphere'/'RTR', '155').
card_multiverse_id('detention sphere'/'RTR', '270356').
card_watermark('detention sphere'/'RTR', 'Azorius').

card_in_set('deviant glee', 'RTR').
card_original_type('deviant glee'/'RTR', 'Enchantment — Aura').
card_original_text('deviant glee'/'RTR', 'Enchant creature\nEnchanted creature gets +2/+1 and has \"{R}: This creature gains trample until end of turn.\"').
card_first_print('deviant glee', 'RTR').
card_image_name('deviant glee'/'RTR', 'deviant glee').
card_uid('deviant glee'/'RTR', 'RTR:Deviant Glee:deviant glee').
card_rarity('deviant glee'/'RTR', 'Common').
card_artist('deviant glee'/'RTR', 'Michael C. Hayes').
card_number('deviant glee'/'RTR', '65').
card_flavor_text('deviant glee'/'RTR', '\"You just need the right incentive to fulfill my dreams.\"').
card_multiverse_id('deviant glee'/'RTR', '265389').
card_watermark('deviant glee'/'RTR', 'Rakdos').

card_in_set('dispel', 'RTR').
card_original_type('dispel'/'RTR', 'Instant').
card_original_text('dispel'/'RTR', 'Counter target instant spell.').
card_image_name('dispel'/'RTR', 'dispel').
card_uid('dispel'/'RTR', 'RTR:Dispel:dispel').
card_rarity('dispel'/'RTR', 'Common').
card_artist('dispel'/'RTR', 'Chase Stone').
card_number('dispel'/'RTR', '36').
card_flavor_text('dispel'/'RTR', '\"Civilization will subsume us unless we overwhelm it instead.\"\n—Speaker Zegana').
card_multiverse_id('dispel'/'RTR', '253566').

card_in_set('doorkeeper', 'RTR').
card_original_type('doorkeeper'/'RTR', 'Creature — Homunculus').
card_original_text('doorkeeper'/'RTR', 'Defender\n{2}{U}, {T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of creatures with defender you control.').
card_first_print('doorkeeper', 'RTR').
card_image_name('doorkeeper'/'RTR', 'doorkeeper').
card_uid('doorkeeper'/'RTR', 'RTR:Doorkeeper:doorkeeper').
card_rarity('doorkeeper'/'RTR', 'Common').
card_artist('doorkeeper'/'RTR', 'Kev Walker').
card_number('doorkeeper'/'RTR', '37').
card_flavor_text('doorkeeper'/'RTR', 'One piercing stare usually convinces thieves to go elsewhere.').
card_multiverse_id('doorkeeper'/'RTR', '265417').

card_in_set('downsize', 'RTR').
card_original_type('downsize'/'RTR', 'Instant').
card_original_text('downsize'/'RTR', 'Target creature you don\'t control gets -4/-0 until end of turn.\nOverload {2}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('downsize', 'RTR').
card_image_name('downsize'/'RTR', 'downsize').
card_uid('downsize'/'RTR', 'RTR:Downsize:downsize').
card_rarity('downsize'/'RTR', 'Common').
card_artist('downsize'/'RTR', 'Ryan Pancoast').
card_number('downsize'/'RTR', '38').
card_multiverse_id('downsize'/'RTR', '265374').
card_watermark('downsize'/'RTR', 'Izzet').

card_in_set('drainpipe vermin', 'RTR').
card_original_type('drainpipe vermin'/'RTR', 'Creature — Rat').
card_original_text('drainpipe vermin'/'RTR', 'When Drainpipe Vermin dies, you may pay {B}. If you do, target player discards a card.').
card_first_print('drainpipe vermin', 'RTR').
card_image_name('drainpipe vermin'/'RTR', 'drainpipe vermin').
card_uid('drainpipe vermin'/'RTR', 'RTR:Drainpipe Vermin:drainpipe vermin').
card_rarity('drainpipe vermin'/'RTR', 'Common').
card_artist('drainpipe vermin'/'RTR', 'Trevor Claxton').
card_number('drainpipe vermin'/'RTR', '66').
card_flavor_text('drainpipe vermin'/'RTR', 'When times are tough, the poor eat the rats. When times are tougher, the rats eat the poor.').
card_multiverse_id('drainpipe vermin'/'RTR', '265401').

card_in_set('dramatic rescue', 'RTR').
card_original_type('dramatic rescue'/'RTR', 'Instant').
card_original_text('dramatic rescue'/'RTR', 'Return target creature to its owner\'s hand. You gain 2 life.').
card_first_print('dramatic rescue', 'RTR').
card_image_name('dramatic rescue'/'RTR', 'dramatic rescue').
card_uid('dramatic rescue'/'RTR', 'RTR:Dramatic Rescue:dramatic rescue').
card_rarity('dramatic rescue'/'RTR', 'Common').
card_artist('dramatic rescue'/'RTR', 'Ryan Pancoast').
card_number('dramatic rescue'/'RTR', '156').
card_flavor_text('dramatic rescue'/'RTR', '\"I never thought I\'d be so glad to see two tons of beak and claws bearing down on me at the speed of an arrow.\"\n—Mirela, Azorius hussar').
card_multiverse_id('dramatic rescue'/'RTR', '253544').
card_watermark('dramatic rescue'/'RTR', 'Azorius').

card_in_set('dreadbore', 'RTR').
card_original_type('dreadbore'/'RTR', 'Sorcery').
card_original_text('dreadbore'/'RTR', 'Destroy target creature or planeswalker.').
card_first_print('dreadbore', 'RTR').
card_image_name('dreadbore'/'RTR', 'dreadbore').
card_uid('dreadbore'/'RTR', 'RTR:Dreadbore:dreadbore').
card_rarity('dreadbore'/'RTR', 'Rare').
card_artist('dreadbore'/'RTR', 'Wayne Reynolds').
card_number('dreadbore'/'RTR', '157').
card_flavor_text('dreadbore'/'RTR', 'In Rakdos-controlled neighborhoods, everyone is part of the show.').
card_multiverse_id('dreadbore'/'RTR', '270354').
card_watermark('dreadbore'/'RTR', 'Rakdos').

card_in_set('dreg mangler', 'RTR').
card_original_type('dreg mangler'/'RTR', 'Creature — Plant Zombie').
card_original_text('dreg mangler'/'RTR', 'Haste\nScavenge {3}{B}{G} ({3}{B}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_image_name('dreg mangler'/'RTR', 'dreg mangler').
card_uid('dreg mangler'/'RTR', 'RTR:Dreg Mangler:dreg mangler').
card_rarity('dreg mangler'/'RTR', 'Uncommon').
card_artist('dreg mangler'/'RTR', 'Peter Mohrbacher').
card_number('dreg mangler'/'RTR', '158').
card_multiverse_id('dreg mangler'/'RTR', '253509').
card_watermark('dreg mangler'/'RTR', 'Golgari').

card_in_set('drudge beetle', 'RTR').
card_original_type('drudge beetle'/'RTR', 'Creature — Insect').
card_original_text('drudge beetle'/'RTR', 'Scavenge {5}{G} ({5}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('drudge beetle', 'RTR').
card_image_name('drudge beetle'/'RTR', 'drudge beetle').
card_uid('drudge beetle'/'RTR', 'RTR:Drudge Beetle:drudge beetle').
card_rarity('drudge beetle'/'RTR', 'Common').
card_artist('drudge beetle'/'RTR', 'Slawomir Maniak').
card_number('drudge beetle'/'RTR', '122').
card_flavor_text('drudge beetle'/'RTR', 'The Street Swarm is the labor class that drives the Golgari\'s endless cycle of life and death.').
card_multiverse_id('drudge beetle'/'RTR', '265407').
card_watermark('drudge beetle'/'RTR', 'Golgari').

card_in_set('druid\'s deliverance', 'RTR').
card_original_type('druid\'s deliverance'/'RTR', 'Instant').
card_original_text('druid\'s deliverance'/'RTR', 'Prevent all combat damage that would be dealt to you this turn. Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('druid\'s deliverance', 'RTR').
card_image_name('druid\'s deliverance'/'RTR', 'druid\'s deliverance').
card_uid('druid\'s deliverance'/'RTR', 'RTR:Druid\'s Deliverance:druid\'s deliverance').
card_rarity('druid\'s deliverance'/'RTR', 'Common').
card_artist('druid\'s deliverance'/'RTR', 'Dan Scott').
card_number('druid\'s deliverance'/'RTR', '123').
card_flavor_text('druid\'s deliverance'/'RTR', '\"So many oppose us, but we are the reed that bends without breaking.\"').
card_multiverse_id('druid\'s deliverance'/'RTR', '289215').
card_watermark('druid\'s deliverance'/'RTR', 'Selesnya').

card_in_set('dryad militant', 'RTR').
card_original_type('dryad militant'/'RTR', 'Creature — Dryad Soldier').
card_original_text('dryad militant'/'RTR', 'If an instant or sorcery card would be put into a graveyard from anywhere, exile it instead.').
card_image_name('dryad militant'/'RTR', 'dryad militant').
card_uid('dryad militant'/'RTR', 'RTR:Dryad Militant:dryad militant').
card_rarity('dryad militant'/'RTR', 'Uncommon').
card_artist('dryad militant'/'RTR', 'Terese Nielsen').
card_number('dryad militant'/'RTR', '214').
card_flavor_text('dryad militant'/'RTR', '\"We will defend the Worldsoul from Izzet ‘progress\' at any cost.\"').
card_multiverse_id('dryad militant'/'RTR', '289226').
card_watermark('dryad militant'/'RTR', 'Selesnya').

card_in_set('dynacharge', 'RTR').
card_original_type('dynacharge'/'RTR', 'Instant').
card_original_text('dynacharge'/'RTR', 'Target creature you control gets +2/+0 until end of turn.\nOverload {2}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('dynacharge', 'RTR').
card_image_name('dynacharge'/'RTR', 'dynacharge').
card_uid('dynacharge'/'RTR', 'RTR:Dynacharge:dynacharge').
card_rarity('dynacharge'/'RTR', 'Common').
card_artist('dynacharge'/'RTR', 'Matt Stewart').
card_number('dynacharge'/'RTR', '92').
card_flavor_text('dynacharge'/'RTR', 'Nothing evens the odds like a little lightning.').
card_multiverse_id('dynacharge'/'RTR', '265397').
card_watermark('dynacharge'/'RTR', 'Izzet').

card_in_set('electrickery', 'RTR').
card_original_type('electrickery'/'RTR', 'Instant').
card_original_text('electrickery'/'RTR', 'Electrickery deals 1 damage to target creature you don\'t control.\nOverload {1}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('electrickery', 'RTR').
card_image_name('electrickery'/'RTR', 'electrickery').
card_uid('electrickery'/'RTR', 'RTR:Electrickery:electrickery').
card_rarity('electrickery'/'RTR', 'Common').
card_artist('electrickery'/'RTR', 'Greg Staples').
card_number('electrickery'/'RTR', '93').
card_multiverse_id('electrickery'/'RTR', '253545').
card_watermark('electrickery'/'RTR', 'Izzet').

card_in_set('epic experiment', 'RTR').
card_original_type('epic experiment'/'RTR', 'Sorcery').
card_original_text('epic experiment'/'RTR', 'Exile the top X cards of your library. For each instant and sorcery card with converted mana cost X or less among them, you may cast that card without paying its mana cost. Then put all cards exiled this way that weren\'t cast into your graveyard.').
card_first_print('epic experiment', 'RTR').
card_image_name('epic experiment'/'RTR', 'epic experiment').
card_uid('epic experiment'/'RTR', 'RTR:Epic Experiment:epic experiment').
card_rarity('epic experiment'/'RTR', 'Mythic Rare').
card_artist('epic experiment'/'RTR', 'Dan Scott').
card_number('epic experiment'/'RTR', '159').
card_multiverse_id('epic experiment'/'RTR', '253552').
card_watermark('epic experiment'/'RTR', 'Izzet').

card_in_set('essence backlash', 'RTR').
card_original_type('essence backlash'/'RTR', 'Instant').
card_original_text('essence backlash'/'RTR', 'Counter target creature spell. Essence Backlash deals damage equal to that spell\'s power to its controller.').
card_first_print('essence backlash', 'RTR').
card_image_name('essence backlash'/'RTR', 'essence backlash').
card_uid('essence backlash'/'RTR', 'RTR:Essence Backlash:essence backlash').
card_rarity('essence backlash'/'RTR', 'Common').
card_artist('essence backlash'/'RTR', 'Jung Park').
card_number('essence backlash'/'RTR', '160').
card_flavor_text('essence backlash'/'RTR', '\"The other guilds will learn the hard way not to interfere with our investigations.\"\n—Bori Andon, Izzet blastseeker').
card_multiverse_id('essence backlash'/'RTR', '290527').
card_watermark('essence backlash'/'RTR', 'Izzet').

card_in_set('ethereal armor', 'RTR').
card_original_type('ethereal armor'/'RTR', 'Enchantment — Aura').
card_original_text('ethereal armor'/'RTR', 'Enchant creature\nEnchanted creature gets +1/+1 for each enchantment you control and has first strike.').
card_first_print('ethereal armor', 'RTR').
card_image_name('ethereal armor'/'RTR', 'ethereal armor').
card_uid('ethereal armor'/'RTR', 'RTR:Ethereal Armor:ethereal armor').
card_rarity('ethereal armor'/'RTR', 'Common').
card_artist('ethereal armor'/'RTR', 'Daarken').
card_number('ethereal armor'/'RTR', '9').
card_flavor_text('ethereal armor'/'RTR', 'Metal is scarce, smiths are pricey, and plate mail is heavy.').
card_multiverse_id('ethereal armor'/'RTR', '265414').

card_in_set('explosive impact', 'RTR').
card_original_type('explosive impact'/'RTR', 'Instant').
card_original_text('explosive impact'/'RTR', 'Explosive Impact deals 5 damage to target creature or player.').
card_first_print('explosive impact', 'RTR').
card_image_name('explosive impact'/'RTR', 'explosive impact').
card_uid('explosive impact'/'RTR', 'RTR:Explosive Impact:explosive impact').
card_rarity('explosive impact'/'RTR', 'Common').
card_artist('explosive impact'/'RTR', 'Steve Argyle').
card_number('explosive impact'/'RTR', '94').
card_flavor_text('explosive impact'/'RTR', '\"Such boorish noise is what passes for subtlety among the Boros.\"\n—Vraska').
card_multiverse_id('explosive impact'/'RTR', '265393').

card_in_set('eyes in the skies', 'RTR').
card_original_type('eyes in the skies'/'RTR', 'Instant').
card_original_text('eyes in the skies'/'RTR', 'Put a 1/1 white Bird creature token with flying onto the battlefield, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('eyes in the skies', 'RTR').
card_image_name('eyes in the skies'/'RTR', 'eyes in the skies').
card_uid('eyes in the skies'/'RTR', 'RTR:Eyes in the Skies:eyes in the skies').
card_rarity('eyes in the skies'/'RTR', 'Common').
card_artist('eyes in the skies'/'RTR', 'James Ryman').
card_number('eyes in the skies'/'RTR', '10').
card_flavor_text('eyes in the skies'/'RTR', '\"What root and branch cannot reach, our winged friends will attend to.\"\n—Zunak, Selesnya strategist').
card_multiverse_id('eyes in the skies'/'RTR', '253553').
card_watermark('eyes in the skies'/'RTR', 'Selesnya').

card_in_set('faerie impostor', 'RTR').
card_original_type('faerie impostor'/'RTR', 'Creature — Faerie Rogue').
card_original_text('faerie impostor'/'RTR', 'Flying\nWhen Faerie Impostor enters the battlefield, sacrifice it unless you return another creature you control to its owner\'s hand.').
card_first_print('faerie impostor', 'RTR').
card_image_name('faerie impostor'/'RTR', 'faerie impostor').
card_uid('faerie impostor'/'RTR', 'RTR:Faerie Impostor:faerie impostor').
card_rarity('faerie impostor'/'RTR', 'Uncommon').
card_artist('faerie impostor'/'RTR', 'Johann Bodin').
card_number('faerie impostor'/'RTR', '39').
card_flavor_text('faerie impostor'/'RTR', 'Many Tin Street shops display a sign on the door: \"No cloaks allowed.\"').
card_multiverse_id('faerie impostor'/'RTR', '322459').

card_in_set('fall of the gavel', 'RTR').
card_original_type('fall of the gavel'/'RTR', 'Instant').
card_original_text('fall of the gavel'/'RTR', 'Counter target spell. You gain 5 life.').
card_first_print('fall of the gavel', 'RTR').
card_image_name('fall of the gavel'/'RTR', 'fall of the gavel').
card_uid('fall of the gavel'/'RTR', 'RTR:Fall of the Gavel:fall of the gavel').
card_rarity('fall of the gavel'/'RTR', 'Uncommon').
card_artist('fall of the gavel'/'RTR', 'Matt Stewart').
card_number('fall of the gavel'/'RTR', '161').
card_flavor_text('fall of the gavel'/'RTR', '\"My ruling is final. Order is upheld. Justice is done.\"').
card_multiverse_id('fall of the gavel'/'RTR', '290536').
card_watermark('fall of the gavel'/'RTR', 'Azorius').

card_in_set('fencing ace', 'RTR').
card_original_type('fencing ace'/'RTR', 'Creature — Human Soldier').
card_original_text('fencing ace'/'RTR', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_first_print('fencing ace', 'RTR').
card_image_name('fencing ace'/'RTR', 'fencing ace').
card_uid('fencing ace'/'RTR', 'RTR:Fencing Ace:fencing ace').
card_rarity('fencing ace'/'RTR', 'Uncommon').
card_artist('fencing ace'/'RTR', 'David Rapoza').
card_number('fencing ace'/'RTR', '11').
card_flavor_text('fencing ace'/'RTR', 'His prowess gives the guildless hope that they can hold out against tyranny.').
card_multiverse_id('fencing ace'/'RTR', '253588').

card_in_set('firemind\'s foresight', 'RTR').
card_original_type('firemind\'s foresight'/'RTR', 'Instant').
card_original_text('firemind\'s foresight'/'RTR', 'Search your library for an instant card with converted mana cost 3, reveal it, and put it into your hand. Then repeat this process for instant cards with converted mana costs 2 and 1. Then shuffle your library.').
card_first_print('firemind\'s foresight', 'RTR').
card_image_name('firemind\'s foresight'/'RTR', 'firemind\'s foresight').
card_uid('firemind\'s foresight'/'RTR', 'RTR:Firemind\'s Foresight:firemind\'s foresight').
card_rarity('firemind\'s foresight'/'RTR', 'Rare').
card_artist('firemind\'s foresight'/'RTR', 'Dan Scott').
card_number('firemind\'s foresight'/'RTR', '162').
card_multiverse_id('firemind\'s foresight'/'RTR', '290541').
card_watermark('firemind\'s foresight'/'RTR', 'Izzet').

card_in_set('forest', 'RTR').
card_original_type('forest'/'RTR', 'Basic Land — Forest').
card_original_text('forest'/'RTR', 'G').
card_image_name('forest'/'RTR', 'forest1').
card_uid('forest'/'RTR', 'RTR:Forest:forest1').
card_rarity('forest'/'RTR', 'Basic Land').
card_artist('forest'/'RTR', 'John Avon').
card_number('forest'/'RTR', '270').
card_multiverse_id('forest'/'RTR', '289325').

card_in_set('forest', 'RTR').
card_original_type('forest'/'RTR', 'Basic Land — Forest').
card_original_text('forest'/'RTR', 'G').
card_image_name('forest'/'RTR', 'forest2').
card_uid('forest'/'RTR', 'RTR:Forest:forest2').
card_rarity('forest'/'RTR', 'Basic Land').
card_artist('forest'/'RTR', 'Yeong-Hao Han').
card_number('forest'/'RTR', '271').
card_multiverse_id('forest'/'RTR', '289326').

card_in_set('forest', 'RTR').
card_original_type('forest'/'RTR', 'Basic Land — Forest').
card_original_text('forest'/'RTR', 'G').
card_image_name('forest'/'RTR', 'forest3').
card_uid('forest'/'RTR', 'RTR:Forest:forest3').
card_rarity('forest'/'RTR', 'Basic Land').
card_artist('forest'/'RTR', 'Adam Paquette').
card_number('forest'/'RTR', '272').
card_multiverse_id('forest'/'RTR', '289328').

card_in_set('forest', 'RTR').
card_original_type('forest'/'RTR', 'Basic Land — Forest').
card_original_text('forest'/'RTR', 'G').
card_image_name('forest'/'RTR', 'forest4').
card_uid('forest'/'RTR', 'RTR:Forest:forest4').
card_rarity('forest'/'RTR', 'Basic Land').
card_artist('forest'/'RTR', 'Richard Wright').
card_number('forest'/'RTR', '273').
card_multiverse_id('forest'/'RTR', '289327').

card_in_set('forest', 'RTR').
card_original_type('forest'/'RTR', 'Basic Land — Forest').
card_original_text('forest'/'RTR', 'G').
card_image_name('forest'/'RTR', 'forest5').
card_uid('forest'/'RTR', 'RTR:Forest:forest5').
card_rarity('forest'/'RTR', 'Basic Land').
card_artist('forest'/'RTR', 'Richard Wright').
card_number('forest'/'RTR', '274').
card_multiverse_id('forest'/'RTR', '333718').

card_in_set('frostburn weird', 'RTR').
card_original_type('frostburn weird'/'RTR', 'Creature — Weird').
card_original_text('frostburn weird'/'RTR', '{U/R}: Frostburn Weird gets +1/-1 until end of turn.').
card_first_print('frostburn weird', 'RTR').
card_image_name('frostburn weird'/'RTR', 'frostburn weird').
card_uid('frostburn weird'/'RTR', 'RTR:Frostburn Weird:frostburn weird').
card_rarity('frostburn weird'/'RTR', 'Common').
card_artist('frostburn weird'/'RTR', 'Mike Bierek').
card_number('frostburn weird'/'RTR', '215').
card_flavor_text('frostburn weird'/'RTR', 'Many chemisters are oblivious to the innumerable machinations of their guild, instead focusing obsessively on creating the perfect weird.').
card_multiverse_id('frostburn weird'/'RTR', '289230').
card_watermark('frostburn weird'/'RTR', 'Izzet').

card_in_set('gatecreeper vine', 'RTR').
card_original_type('gatecreeper vine'/'RTR', 'Creature — Plant').
card_original_text('gatecreeper vine'/'RTR', 'Defender\nWhen Gatecreeper Vine enters the battlefield, you may search your library for a basic land card or a Gate card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('gatecreeper vine', 'RTR').
card_image_name('gatecreeper vine'/'RTR', 'gatecreeper vine').
card_uid('gatecreeper vine'/'RTR', 'RTR:Gatecreeper Vine:gatecreeper vine').
card_rarity('gatecreeper vine'/'RTR', 'Common').
card_artist('gatecreeper vine'/'RTR', 'Trevor Claxton').
card_number('gatecreeper vine'/'RTR', '124').
card_flavor_text('gatecreeper vine'/'RTR', 'Every inch of Ravnica is home to something.').
card_multiverse_id('gatecreeper vine'/'RTR', '289217').

card_in_set('giant growth', 'RTR').
card_original_type('giant growth'/'RTR', 'Instant').
card_original_text('giant growth'/'RTR', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'RTR', 'giant growth').
card_uid('giant growth'/'RTR', 'RTR:Giant Growth:giant growth').
card_rarity('giant growth'/'RTR', 'Common').
card_artist('giant growth'/'RTR', 'Noah Bradley').
card_number('giant growth'/'RTR', '125').
card_flavor_text('giant growth'/'RTR', '\"The prohibition of unnecessary tossing of the citizenry by giants shall also extend to spontaneous giants.\"\n—Isperia\'s Edict, provision VII.789.2').
card_multiverse_id('giant growth'/'RTR', '289213').

card_in_set('gobbling ooze', 'RTR').
card_original_type('gobbling ooze'/'RTR', 'Creature — Ooze').
card_original_text('gobbling ooze'/'RTR', '{G}, Sacrifice another creature: Put a +1/+1 counter on Gobbling Ooze.').
card_first_print('gobbling ooze', 'RTR').
card_image_name('gobbling ooze'/'RTR', 'gobbling ooze').
card_uid('gobbling ooze'/'RTR', 'RTR:Gobbling Ooze:gobbling ooze').
card_rarity('gobbling ooze'/'RTR', 'Uncommon').
card_artist('gobbling ooze'/'RTR', 'Johann Bodin').
card_number('gobbling ooze'/'RTR', '126').
card_flavor_text('gobbling ooze'/'RTR', 'The furious citizens blamed the Simic for releasing it in their district. The Simic pointed out that rats were no longer a problem.').
card_multiverse_id('gobbling ooze'/'RTR', '253643').

card_in_set('goblin electromancer', 'RTR').
card_original_type('goblin electromancer'/'RTR', 'Creature — Goblin Wizard').
card_original_text('goblin electromancer'/'RTR', 'Instant and sorcery spells you cast cost {1} less to cast.').
card_image_name('goblin electromancer'/'RTR', 'goblin electromancer').
card_uid('goblin electromancer'/'RTR', 'RTR:Goblin Electromancer:goblin electromancer').
card_rarity('goblin electromancer'/'RTR', 'Common').
card_artist('goblin electromancer'/'RTR', 'Svetlin Velinov').
card_number('goblin electromancer'/'RTR', '163').
card_flavor_text('goblin electromancer'/'RTR', 'When asked how much power is required, Izzet mages always answer \"more.\"').
card_multiverse_id('goblin electromancer'/'RTR', '253548').
card_watermark('goblin electromancer'/'RTR', 'Izzet').

card_in_set('goblin rally', 'RTR').
card_original_type('goblin rally'/'RTR', 'Sorcery').
card_original_text('goblin rally'/'RTR', 'Put four 1/1 red Goblin creature tokens onto the battlefield.').
card_first_print('goblin rally', 'RTR').
card_image_name('goblin rally'/'RTR', 'goblin rally').
card_uid('goblin rally'/'RTR', 'RTR:Goblin Rally:goblin rally').
card_rarity('goblin rally'/'RTR', 'Uncommon').
card_artist('goblin rally'/'RTR', 'Nic Klein').
card_number('goblin rally'/'RTR', '95').
card_flavor_text('goblin rally'/'RTR', 'You don\'t so much hire goblins as put ideas in their heads.').
card_multiverse_id('goblin rally'/'RTR', '270788').

card_in_set('golgari charm', 'RTR').
card_original_type('golgari charm'/'RTR', 'Instant').
card_original_text('golgari charm'/'RTR', 'Choose one — All creatures get -1/-1 until end of turn; or destroy target enchantment; or regenerate each creature you control.').
card_first_print('golgari charm', 'RTR').
card_image_name('golgari charm'/'RTR', 'golgari charm').
card_uid('golgari charm'/'RTR', 'RTR:Golgari Charm:golgari charm').
card_rarity('golgari charm'/'RTR', 'Uncommon').
card_artist('golgari charm'/'RTR', 'Zoltan Boros').
card_number('golgari charm'/'RTR', '164').
card_flavor_text('golgari charm'/'RTR', '\"Let the rest of Ravnica sneer. One way or another, they all end up in the undercity.\"\n—Jarad').
card_multiverse_id('golgari charm'/'RTR', '265385').
card_watermark('golgari charm'/'RTR', 'Golgari').

card_in_set('golgari decoy', 'RTR').
card_original_type('golgari decoy'/'RTR', 'Creature — Elf Rogue').
card_original_text('golgari decoy'/'RTR', 'All creatures able to block Golgari Decoy do so.\nScavenge {3}{G}{G} ({3}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('golgari decoy', 'RTR').
card_image_name('golgari decoy'/'RTR', 'golgari decoy').
card_uid('golgari decoy'/'RTR', 'RTR:Golgari Decoy:golgari decoy').
card_rarity('golgari decoy'/'RTR', 'Uncommon').
card_artist('golgari decoy'/'RTR', 'Marco Nelor').
card_number('golgari decoy'/'RTR', '127').
card_multiverse_id('golgari decoy'/'RTR', '253508').
card_watermark('golgari decoy'/'RTR', 'Golgari').

card_in_set('golgari guildgate', 'RTR').
card_original_type('golgari guildgate'/'RTR', 'Land — Gate').
card_original_text('golgari guildgate'/'RTR', 'Golgari Guildgate enters the battlefield tapped.\n{T}: Add {B} or {G} to your mana pool.').
card_first_print('golgari guildgate', 'RTR').
card_image_name('golgari guildgate'/'RTR', 'golgari guildgate').
card_uid('golgari guildgate'/'RTR', 'RTR:Golgari Guildgate:golgari guildgate').
card_rarity('golgari guildgate'/'RTR', 'Common').
card_artist('golgari guildgate'/'RTR', 'Eytan Zana').
card_number('golgari guildgate'/'RTR', '239').
card_flavor_text('golgari guildgate'/'RTR', 'Enter those who are starving and sick. You are welcome among the Swarm when the rest of Ravnica rejects you.').
card_multiverse_id('golgari guildgate'/'RTR', '270964').
card_watermark('golgari guildgate'/'RTR', 'Golgari').

card_in_set('golgari keyrune', 'RTR').
card_original_type('golgari keyrune'/'RTR', 'Artifact').
card_original_text('golgari keyrune'/'RTR', '{T}: Add {B} or {G} to your mana pool.\n{B}{G}: Golgari Keyrune becomes a 2/2 black and green Insect artifact creature with deathtouch until end of turn.').
card_first_print('golgari keyrune', 'RTR').
card_image_name('golgari keyrune'/'RTR', 'golgari keyrune').
card_uid('golgari keyrune'/'RTR', 'RTR:Golgari Keyrune:golgari keyrune').
card_rarity('golgari keyrune'/'RTR', 'Uncommon').
card_artist('golgari keyrune'/'RTR', 'Daniel Ljunggren').
card_number('golgari keyrune'/'RTR', '229').
card_flavor_text('golgari keyrune'/'RTR', 'Like a single ant, it only hints at the colony below.').
card_multiverse_id('golgari keyrune'/'RTR', '253645').
card_watermark('golgari keyrune'/'RTR', 'Golgari').

card_in_set('golgari longlegs', 'RTR').
card_original_type('golgari longlegs'/'RTR', 'Creature — Insect').
card_original_text('golgari longlegs'/'RTR', '').
card_first_print('golgari longlegs', 'RTR').
card_image_name('golgari longlegs'/'RTR', 'golgari longlegs').
card_uid('golgari longlegs'/'RTR', 'RTR:Golgari Longlegs:golgari longlegs').
card_rarity('golgari longlegs'/'RTR', 'Common').
card_artist('golgari longlegs'/'RTR', 'Volkan Baga').
card_number('golgari longlegs'/'RTR', '216').
card_flavor_text('golgari longlegs'/'RTR', 'Despite its enormous stature, it can fold itself into a tunnel with startling quickness, vanishing back into the undercity.').
card_multiverse_id('golgari longlegs'/'RTR', '290535').
card_watermark('golgari longlegs'/'RTR', 'Golgari').

card_in_set('gore-house chainwalker', 'RTR').
card_original_type('gore-house chainwalker'/'RTR', 'Creature — Human Warrior').
card_original_text('gore-house chainwalker'/'RTR', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_first_print('gore-house chainwalker', 'RTR').
card_image_name('gore-house chainwalker'/'RTR', 'gore-house chainwalker').
card_uid('gore-house chainwalker'/'RTR', 'RTR:Gore-House Chainwalker:gore-house chainwalker').
card_rarity('gore-house chainwalker'/'RTR', 'Common').
card_artist('gore-house chainwalker'/'RTR', 'Dan Scott').
card_number('gore-house chainwalker'/'RTR', '96').
card_flavor_text('gore-house chainwalker'/'RTR', 'If the pig\'s blood drips on you, you\'re next on the chain.').
card_multiverse_id('gore-house chainwalker'/'RTR', '270796').
card_watermark('gore-house chainwalker'/'RTR', 'Rakdos').

card_in_set('grave betrayal', 'RTR').
card_original_type('grave betrayal'/'RTR', 'Enchantment').
card_original_text('grave betrayal'/'RTR', 'Whenever a creature you don\'t control dies, return it to the battlefield under your control with an additional +1/+1 counter on it at the beginning of the next end step. That creature is a black Zombie in addition to its other colors and types.').
card_first_print('grave betrayal', 'RTR').
card_image_name('grave betrayal'/'RTR', 'grave betrayal').
card_uid('grave betrayal'/'RTR', 'RTR:Grave Betrayal:grave betrayal').
card_rarity('grave betrayal'/'RTR', 'Rare').
card_artist('grave betrayal'/'RTR', 'Lucas Graciano').
card_number('grave betrayal'/'RTR', '67').
card_multiverse_id('grave betrayal'/'RTR', '270960').

card_in_set('grim roustabout', 'RTR').
card_original_type('grim roustabout'/'RTR', 'Creature — Skeleton Warrior').
card_original_text('grim roustabout'/'RTR', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\n{1}{B}: Regenerate Grim Roustabout.').
card_first_print('grim roustabout', 'RTR').
card_image_name('grim roustabout'/'RTR', 'grim roustabout').
card_uid('grim roustabout'/'RTR', 'RTR:Grim Roustabout:grim roustabout').
card_rarity('grim roustabout'/'RTR', 'Common').
card_artist('grim roustabout'/'RTR', 'Steven Belledin').
card_number('grim roustabout'/'RTR', '68').
card_flavor_text('grim roustabout'/'RTR', 'He\'ll point you to your death row seats.').
card_multiverse_id('grim roustabout'/'RTR', '265386').
card_watermark('grim roustabout'/'RTR', 'Rakdos').

card_in_set('grisly salvage', 'RTR').
card_original_type('grisly salvage'/'RTR', 'Instant').
card_original_text('grisly salvage'/'RTR', 'Reveal the top five cards of your library. You may put a creature or land card from among them into your hand. Put the rest into your graveyard.').
card_image_name('grisly salvage'/'RTR', 'grisly salvage').
card_uid('grisly salvage'/'RTR', 'RTR:Grisly Salvage:grisly salvage').
card_rarity('grisly salvage'/'RTR', 'Common').
card_artist('grisly salvage'/'RTR', 'Dave Kendall').
card_number('grisly salvage'/'RTR', '165').
card_flavor_text('grisly salvage'/'RTR', 'To the Golgari, anything buried is treasure.').
card_multiverse_id('grisly salvage'/'RTR', '277996').
card_watermark('grisly salvage'/'RTR', 'Golgari').

card_in_set('grove of the guardian', 'RTR').
card_original_type('grove of the guardian'/'RTR', 'Land').
card_original_text('grove of the guardian'/'RTR', '{T}: Add {1} to your mana pool.\n{3}{G}{W}, {T}, Tap two untapped creatures you control, Sacrifice Grove of the Guardian: Put an 8/8 green and white Elemental creature token with vigilance onto the battlefield.').
card_image_name('grove of the guardian'/'RTR', 'grove of the guardian').
card_uid('grove of the guardian'/'RTR', 'RTR:Grove of the Guardian:grove of the guardian').
card_rarity('grove of the guardian'/'RTR', 'Rare').
card_artist('grove of the guardian'/'RTR', 'Christine Choi').
card_number('grove of the guardian'/'RTR', '240').
card_multiverse_id('grove of the guardian'/'RTR', '270968').
card_watermark('grove of the guardian'/'RTR', 'Selesnya').

card_in_set('growing ranks', 'RTR').
card_original_type('growing ranks'/'RTR', 'Enchantment').
card_original_text('growing ranks'/'RTR', 'At the beginning of your upkeep, populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('growing ranks', 'RTR').
card_image_name('growing ranks'/'RTR', 'growing ranks').
card_uid('growing ranks'/'RTR', 'RTR:Growing Ranks:growing ranks').
card_rarity('growing ranks'/'RTR', 'Rare').
card_artist('growing ranks'/'RTR', 'Seb McKinnon').
card_number('growing ranks'/'RTR', '217').
card_flavor_text('growing ranks'/'RTR', '\"We will grow an army large enough to withstand the Izzet\'s madness.\"\n—Trostani').
card_multiverse_id('growing ranks'/'RTR', '270957').
card_watermark('growing ranks'/'RTR', 'Selesnya').

card_in_set('guild feud', 'RTR').
card_original_type('guild feud'/'RTR', 'Enchantment').
card_original_text('guild feud'/'RTR', 'At the beginning of your upkeep, target opponent reveals the top three cards of his or her library, may put a creature card from among them onto the battlefield, then puts the rest into his or her graveyard. You do the same with the top three cards of your library. If two creatures are put onto the battlefield this way, those creatures fight each other.').
card_first_print('guild feud', 'RTR').
card_image_name('guild feud'/'RTR', 'guild feud').
card_uid('guild feud'/'RTR', 'RTR:Guild Feud:guild feud').
card_rarity('guild feud'/'RTR', 'Rare').
card_artist('guild feud'/'RTR', 'Karl Kopinski').
card_number('guild feud'/'RTR', '97').
card_multiverse_id('guild feud'/'RTR', '265406').

card_in_set('guttersnipe', 'RTR').
card_original_type('guttersnipe'/'RTR', 'Creature — Goblin Shaman').
card_original_text('guttersnipe'/'RTR', 'Whenever you cast an instant or sorcery spell, Guttersnipe deals 2 damage to each opponent.').
card_first_print('guttersnipe', 'RTR').
card_image_name('guttersnipe'/'RTR', 'guttersnipe').
card_uid('guttersnipe'/'RTR', 'RTR:Guttersnipe:guttersnipe').
card_rarity('guttersnipe'/'RTR', 'Uncommon').
card_artist('guttersnipe'/'RTR', 'Steve Prescott').
card_number('guttersnipe'/'RTR', '98').
card_flavor_text('guttersnipe'/'RTR', 'He wants to watch Ravnica burn, one soul at a time.').
card_multiverse_id('guttersnipe'/'RTR', '265392').

card_in_set('hallowed fountain', 'RTR').
card_original_type('hallowed fountain'/'RTR', 'Land — Plains Island').
card_original_text('hallowed fountain'/'RTR', '({T}: Add {W} or {U} to your mana pool.)\nAs Hallowed Fountain enters the battlefield, you may pay 2 life. If you don\'t, Hallowed Fountain enters the battlefield tapped.').
card_image_name('hallowed fountain'/'RTR', 'hallowed fountain').
card_uid('hallowed fountain'/'RTR', 'RTR:Hallowed Fountain:hallowed fountain').
card_rarity('hallowed fountain'/'RTR', 'Rare').
card_artist('hallowed fountain'/'RTR', 'Jung Park').
card_number('hallowed fountain'/'RTR', '241').
card_flavor_text('hallowed fountain'/'RTR', 'A place to relax, if you have the proper permit.').
card_multiverse_id('hallowed fountain'/'RTR', '253684').
card_watermark('hallowed fountain'/'RTR', 'Azorius').

card_in_set('havoc festival', 'RTR').
card_original_type('havoc festival'/'RTR', 'Enchantment').
card_original_text('havoc festival'/'RTR', 'Players can\'t gain life.\nAt the beginning of each player\'s upkeep, that player loses half his or her life, rounded up.').
card_first_print('havoc festival', 'RTR').
card_image_name('havoc festival'/'RTR', 'havoc festival').
card_uid('havoc festival'/'RTR', 'RTR:Havoc Festival:havoc festival').
card_rarity('havoc festival'/'RTR', 'Rare').
card_artist('havoc festival'/'RTR', 'Johannes Voss').
card_number('havoc festival'/'RTR', '166').
card_flavor_text('havoc festival'/'RTR', '\"We can\'t control enemies who have no regard for their own survival.\"\n—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('havoc festival'/'RTR', '253648').
card_watermark('havoc festival'/'RTR', 'Rakdos').

card_in_set('hellhole flailer', 'RTR').
card_original_type('hellhole flailer'/'RTR', 'Creature — Ogre Warrior').
card_original_text('hellhole flailer'/'RTR', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\n{2}{B}{R}, Sacrifice Hellhole Flailer: Hellhole Flailer deals damage equal to its power to target player.').
card_first_print('hellhole flailer', 'RTR').
card_image_name('hellhole flailer'/'RTR', 'hellhole flailer').
card_uid('hellhole flailer'/'RTR', 'RTR:Hellhole Flailer:hellhole flailer').
card_rarity('hellhole flailer'/'RTR', 'Uncommon').
card_artist('hellhole flailer'/'RTR', 'Steve Prescott').
card_number('hellhole flailer'/'RTR', '167').
card_multiverse_id('hellhole flailer'/'RTR', '289211').
card_watermark('hellhole flailer'/'RTR', 'Rakdos').

card_in_set('heroes\' reunion', 'RTR').
card_original_type('heroes\' reunion'/'RTR', 'Instant').
card_original_text('heroes\' reunion'/'RTR', 'Target player gains 7 life.').
card_image_name('heroes\' reunion'/'RTR', 'heroes\' reunion').
card_uid('heroes\' reunion'/'RTR', 'RTR:Heroes\' Reunion:heroes\' reunion').
card_rarity('heroes\' reunion'/'RTR', 'Uncommon').
card_artist('heroes\' reunion'/'RTR', 'Howard Lyon').
card_number('heroes\' reunion'/'RTR', '168').
card_flavor_text('heroes\' reunion'/'RTR', 'They share no bloodline, but a bond greater than family unites them. Such are the gifts of the Worldsoul.').
card_multiverse_id('heroes\' reunion'/'RTR', '290537').
card_watermark('heroes\' reunion'/'RTR', 'Selesnya').

card_in_set('horncaller\'s chant', 'RTR').
card_original_type('horncaller\'s chant'/'RTR', 'Sorcery').
card_original_text('horncaller\'s chant'/'RTR', 'Put a 4/4 green Rhino creature token with trample onto the battlefield, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('horncaller\'s chant', 'RTR').
card_image_name('horncaller\'s chant'/'RTR', 'horncaller\'s chant').
card_uid('horncaller\'s chant'/'RTR', 'RTR:Horncaller\'s Chant:horncaller\'s chant').
card_rarity('horncaller\'s chant'/'RTR', 'Common').
card_artist('horncaller\'s chant'/'RTR', 'Eric Velhagen').
card_number('horncaller\'s chant'/'RTR', '128').
card_flavor_text('horncaller\'s chant'/'RTR', '\"The Selesnya are gathering armies. Do you still believe they aren\'t preparing for war?\"\n—Teysa, to Isperia').
card_multiverse_id('horncaller\'s chant'/'RTR', '253536').
card_watermark('horncaller\'s chant'/'RTR', 'Selesnya').

card_in_set('hover barrier', 'RTR').
card_original_type('hover barrier'/'RTR', 'Creature — Illusion Wall').
card_original_text('hover barrier'/'RTR', 'Defender, flying').
card_first_print('hover barrier', 'RTR').
card_image_name('hover barrier'/'RTR', 'hover barrier').
card_uid('hover barrier'/'RTR', 'RTR:Hover Barrier:hover barrier').
card_rarity('hover barrier'/'RTR', 'Uncommon').
card_artist('hover barrier'/'RTR', 'Mathias Kollros').
card_number('hover barrier'/'RTR', '40').
card_flavor_text('hover barrier'/'RTR', 'As whispers and rumors increased, so did the demand for fail-safe barriers.').
card_multiverse_id('hover barrier'/'RTR', '253541').

card_in_set('hussar patrol', 'RTR').
card_original_type('hussar patrol'/'RTR', 'Creature — Human Knight').
card_original_text('hussar patrol'/'RTR', 'Flash (You may cast this spell any time you could cast an instant.)\nVigilance').
card_first_print('hussar patrol', 'RTR').
card_image_name('hussar patrol'/'RTR', 'hussar patrol').
card_uid('hussar patrol'/'RTR', 'RTR:Hussar Patrol:hussar patrol').
card_rarity('hussar patrol'/'RTR', 'Common').
card_artist('hussar patrol'/'RTR', 'Seb McKinnon').
card_number('hussar patrol'/'RTR', '169').
card_flavor_text('hussar patrol'/'RTR', '\"You think no one is watching, you think you\'re smart enough to escape, and most foolish of all, you think no one cares.\"\n—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('hussar patrol'/'RTR', '253520').
card_watermark('hussar patrol'/'RTR', 'Azorius').

card_in_set('hypersonic dragon', 'RTR').
card_original_type('hypersonic dragon'/'RTR', 'Creature — Dragon').
card_original_text('hypersonic dragon'/'RTR', 'Flying, haste\nYou may cast sorcery spells as though they had flash. (You may cast them any time you could cast an instant.)').
card_image_name('hypersonic dragon'/'RTR', 'hypersonic dragon').
card_uid('hypersonic dragon'/'RTR', 'RTR:Hypersonic Dragon:hypersonic dragon').
card_rarity('hypersonic dragon'/'RTR', 'Rare').
card_artist('hypersonic dragon'/'RTR', 'Dan Scott').
card_number('hypersonic dragon'/'RTR', '170').
card_flavor_text('hypersonic dragon'/'RTR', '\"Even this primitive specimen of my kind is impressive.\"\n—Niv-Mizzet').
card_multiverse_id('hypersonic dragon'/'RTR', '253655').
card_watermark('hypersonic dragon'/'RTR', 'Izzet').

card_in_set('inaction injunction', 'RTR').
card_original_type('inaction injunction'/'RTR', 'Sorcery').
card_original_text('inaction injunction'/'RTR', 'Detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)\nDraw a card.').
card_first_print('inaction injunction', 'RTR').
card_image_name('inaction injunction'/'RTR', 'inaction injunction').
card_uid('inaction injunction'/'RTR', 'RTR:Inaction Injunction:inaction injunction').
card_rarity('inaction injunction'/'RTR', 'Common').
card_artist('inaction injunction'/'RTR', 'Wayne Reynolds').
card_number('inaction injunction'/'RTR', '41').
card_flavor_text('inaction injunction'/'RTR', '\"To prevent action is to prevent transgression.\"\n—Azorius Arrester motto').
card_multiverse_id('inaction injunction'/'RTR', '265376').
card_watermark('inaction injunction'/'RTR', 'Azorius').

card_in_set('inspiration', 'RTR').
card_original_type('inspiration'/'RTR', 'Instant').
card_original_text('inspiration'/'RTR', 'Target player draws two cards.').
card_image_name('inspiration'/'RTR', 'inspiration').
card_uid('inspiration'/'RTR', 'RTR:Inspiration:inspiration').
card_rarity('inspiration'/'RTR', 'Common').
card_artist('inspiration'/'RTR', 'Izzy').
card_number('inspiration'/'RTR', '42').
card_flavor_text('inspiration'/'RTR', '\"Day 31: I finally succeeded in my time reversal experiment!\n\"Day 30: I might have a problem here.\"\n—Journal of the Prime Izmagnus').
card_multiverse_id('inspiration'/'RTR', '265380').

card_in_set('island', 'RTR').
card_original_type('island'/'RTR', 'Basic Land — Island').
card_original_text('island'/'RTR', 'U').
card_image_name('island'/'RTR', 'island1').
card_uid('island'/'RTR', 'RTR:Island:island1').
card_rarity('island'/'RTR', 'Basic Land').
card_artist('island'/'RTR', 'John Avon').
card_number('island'/'RTR', '255').
card_multiverse_id('island'/'RTR', '289316').

card_in_set('island', 'RTR').
card_original_type('island'/'RTR', 'Basic Land — Island').
card_original_text('island'/'RTR', 'U').
card_image_name('island'/'RTR', 'island2').
card_uid('island'/'RTR', 'RTR:Island:island2').
card_rarity('island'/'RTR', 'Basic Land').
card_artist('island'/'RTR', 'Yeong-Hao Han').
card_number('island'/'RTR', '256').
card_multiverse_id('island'/'RTR', '289315').

card_in_set('island', 'RTR').
card_original_type('island'/'RTR', 'Basic Land — Island').
card_original_text('island'/'RTR', 'U').
card_image_name('island'/'RTR', 'island3').
card_uid('island'/'RTR', 'RTR:Island:island3').
card_rarity('island'/'RTR', 'Basic Land').
card_artist('island'/'RTR', 'Adam Paquette').
card_number('island'/'RTR', '257').
card_multiverse_id('island'/'RTR', '289313').

card_in_set('island', 'RTR').
card_original_type('island'/'RTR', 'Basic Land — Island').
card_original_text('island'/'RTR', 'U').
card_image_name('island'/'RTR', 'island4').
card_uid('island'/'RTR', 'RTR:Island:island4').
card_rarity('island'/'RTR', 'Basic Land').
card_artist('island'/'RTR', 'Richard Wright').
card_number('island'/'RTR', '258').
card_multiverse_id('island'/'RTR', '289314').

card_in_set('island', 'RTR').
card_original_type('island'/'RTR', 'Basic Land — Island').
card_original_text('island'/'RTR', 'U').
card_image_name('island'/'RTR', 'island5').
card_uid('island'/'RTR', 'RTR:Island:island5').
card_rarity('island'/'RTR', 'Basic Land').
card_artist('island'/'RTR', 'Richard Wright').
card_number('island'/'RTR', '259').
card_multiverse_id('island'/'RTR', '333719').

card_in_set('isperia\'s skywatch', 'RTR').
card_original_type('isperia\'s skywatch'/'RTR', 'Creature — Vedalken Knight').
card_original_text('isperia\'s skywatch'/'RTR', 'Flying\nWhen Isperia\'s Skywatch enters the battlefield, detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)').
card_first_print('isperia\'s skywatch', 'RTR').
card_image_name('isperia\'s skywatch'/'RTR', 'isperia\'s skywatch').
card_uid('isperia\'s skywatch'/'RTR', 'RTR:Isperia\'s Skywatch:isperia\'s skywatch').
card_rarity('isperia\'s skywatch'/'RTR', 'Common').
card_artist('isperia\'s skywatch'/'RTR', 'Chris Rahn').
card_number('isperia\'s skywatch'/'RTR', '43').
card_multiverse_id('isperia\'s skywatch'/'RTR', '270958').
card_watermark('isperia\'s skywatch'/'RTR', 'Azorius').

card_in_set('isperia, supreme judge', 'RTR').
card_original_type('isperia, supreme judge'/'RTR', 'Legendary Creature — Sphinx').
card_original_text('isperia, supreme judge'/'RTR', 'Flying\nWhenever a creature attacks you or a planeswalker you control, you may draw a card.').
card_first_print('isperia, supreme judge', 'RTR').
card_image_name('isperia, supreme judge'/'RTR', 'isperia, supreme judge').
card_uid('isperia, supreme judge'/'RTR', 'RTR:Isperia, Supreme Judge:isperia, supreme judge').
card_rarity('isperia, supreme judge'/'RTR', 'Mythic Rare').
card_artist('isperia, supreme judge'/'RTR', 'Scott M. Fischer').
card_number('isperia, supreme judge'/'RTR', '171').
card_flavor_text('isperia, supreme judge'/'RTR', '\"I serve only justice. But through that duty, I serve all of Ravnica.\"').
card_multiverse_id('isperia, supreme judge'/'RTR', '290531').
card_watermark('isperia, supreme judge'/'RTR', 'Azorius').

card_in_set('izzet charm', 'RTR').
card_original_type('izzet charm'/'RTR', 'Instant').
card_original_text('izzet charm'/'RTR', 'Choose one — Counter target noncreature spell unless its controller pays {2}; or Izzet Charm deals 2 damage to target creature; or draw two cards, then discard two cards.').
card_image_name('izzet charm'/'RTR', 'izzet charm').
card_uid('izzet charm'/'RTR', 'RTR:Izzet Charm:izzet charm').
card_rarity('izzet charm'/'RTR', 'Uncommon').
card_artist('izzet charm'/'RTR', 'Zoltan Boros').
card_number('izzet charm'/'RTR', '172').
card_multiverse_id('izzet charm'/'RTR', '253528').
card_watermark('izzet charm'/'RTR', 'Izzet').

card_in_set('izzet guildgate', 'RTR').
card_original_type('izzet guildgate'/'RTR', 'Land — Gate').
card_original_text('izzet guildgate'/'RTR', 'Izzet Guildgate enters the battlefield tapped.\n{T}: Add {U} or {R} to your mana pool.').
card_first_print('izzet guildgate', 'RTR').
card_image_name('izzet guildgate'/'RTR', 'izzet guildgate').
card_uid('izzet guildgate'/'RTR', 'RTR:Izzet Guildgate:izzet guildgate').
card_rarity('izzet guildgate'/'RTR', 'Common').
card_artist('izzet guildgate'/'RTR', 'Noah Bradley').
card_number('izzet guildgate'/'RTR', '242').
card_flavor_text('izzet guildgate'/'RTR', 'Enter those with the vision to create and the daring to release their creations.').
card_multiverse_id('izzet guildgate'/'RTR', '270961').
card_watermark('izzet guildgate'/'RTR', 'Izzet').

card_in_set('izzet keyrune', 'RTR').
card_original_type('izzet keyrune'/'RTR', 'Artifact').
card_original_text('izzet keyrune'/'RTR', '{T}: Add {U} or {R} to your mana pool.\n{U}{R}: Until end of turn, Izzet Keyrune becomes a 2/1 blue and red Elemental artifact creature.\nWhenever Izzet Keyrune deals combat damage to a player, you may draw a card. If you do, discard a card.').
card_first_print('izzet keyrune', 'RTR').
card_image_name('izzet keyrune'/'RTR', 'izzet keyrune').
card_uid('izzet keyrune'/'RTR', 'RTR:Izzet Keyrune:izzet keyrune').
card_rarity('izzet keyrune'/'RTR', 'Uncommon').
card_artist('izzet keyrune'/'RTR', 'Daniel Ljunggren').
card_number('izzet keyrune'/'RTR', '230').
card_multiverse_id('izzet keyrune'/'RTR', '253514').
card_watermark('izzet keyrune'/'RTR', 'Izzet').

card_in_set('izzet staticaster', 'RTR').
card_original_type('izzet staticaster'/'RTR', 'Creature — Human Wizard').
card_original_text('izzet staticaster'/'RTR', 'Flash (You may cast this spell any time you could cast an instant.)\nHaste\n{T}: Izzet Staticaster deals 1 damage to target creature and each other creature with the same name as that creature.').
card_first_print('izzet staticaster', 'RTR').
card_image_name('izzet staticaster'/'RTR', 'izzet staticaster').
card_uid('izzet staticaster'/'RTR', 'RTR:Izzet Staticaster:izzet staticaster').
card_rarity('izzet staticaster'/'RTR', 'Uncommon').
card_artist('izzet staticaster'/'RTR', 'Scott M. Fischer').
card_number('izzet staticaster'/'RTR', '173').
card_multiverse_id('izzet staticaster'/'RTR', '253638').
card_watermark('izzet staticaster'/'RTR', 'Izzet').

card_in_set('jace, architect of thought', 'RTR').
card_original_type('jace, architect of thought'/'RTR', 'Planeswalker — Jace').
card_original_text('jace, architect of thought'/'RTR', '+1: Until your next turn, whenever a creature an opponent controls attacks, it gets -1/-0 until end of turn.\n-2: Reveal the top three cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other on the bottom of your library in any order.\n-8: For each player, search that player\'s library for a nonland card and exile it, then that player shuffles his or her library. You may cast those cards without paying their mana costs.').
card_first_print('jace, architect of thought', 'RTR').
card_image_name('jace, architect of thought'/'RTR', 'jace, architect of thought').
card_uid('jace, architect of thought'/'RTR', 'RTR:Jace, Architect of Thought:jace, architect of thought').
card_rarity('jace, architect of thought'/'RTR', 'Mythic Rare').
card_artist('jace, architect of thought'/'RTR', 'Jaime Jones').
card_number('jace, architect of thought'/'RTR', '44').
card_multiverse_id('jace, architect of thought'/'RTR', '253653').

card_in_set('jarad\'s orders', 'RTR').
card_original_type('jarad\'s orders'/'RTR', 'Sorcery').
card_original_text('jarad\'s orders'/'RTR', 'Search your library for up to two creature cards and reveal them. Put one into your hand and the other into your graveyard. Then shuffle your library.').
card_first_print('jarad\'s orders', 'RTR').
card_image_name('jarad\'s orders'/'RTR', 'jarad\'s orders').
card_uid('jarad\'s orders'/'RTR', 'RTR:Jarad\'s Orders:jarad\'s orders').
card_rarity('jarad\'s orders'/'RTR', 'Rare').
card_artist('jarad\'s orders'/'RTR', 'Svetlin Velinov').
card_number('jarad\'s orders'/'RTR', '175').
card_flavor_text('jarad\'s orders'/'RTR', '\"The Izzet are searching for something. Discern what or die trying.\"').
card_multiverse_id('jarad\'s orders'/'RTR', '253633').
card_watermark('jarad\'s orders'/'RTR', 'Golgari').

card_in_set('jarad, golgari lich lord', 'RTR').
card_original_type('jarad, golgari lich lord'/'RTR', 'Legendary Creature — Zombie Elf').
card_original_text('jarad, golgari lich lord'/'RTR', 'Jarad, Golgari Lich Lord gets +1/+1 for each creature card in your graveyard.\n{1}{B}{G}, Sacrifice another creature: Each opponent loses life equal to the sacrificed creature\'s power.\nSacrifice a Swamp and a Forest: Return Jarad from your graveyard to your hand.').
card_image_name('jarad, golgari lich lord'/'RTR', 'jarad, golgari lich lord').
card_uid('jarad, golgari lich lord'/'RTR', 'RTR:Jarad, Golgari Lich Lord:jarad, golgari lich lord').
card_rarity('jarad, golgari lich lord'/'RTR', 'Mythic Rare').
card_artist('jarad, golgari lich lord'/'RTR', 'Eric Deschamps').
card_number('jarad, golgari lich lord'/'RTR', '174').
card_multiverse_id('jarad, golgari lich lord'/'RTR', '290534').
card_watermark('jarad, golgari lich lord'/'RTR', 'Golgari').

card_in_set('judge\'s familiar', 'RTR').
card_original_type('judge\'s familiar'/'RTR', 'Creature — Bird').
card_original_text('judge\'s familiar'/'RTR', 'Flying\nSacrifice Judge\'s Familiar: Counter target instant or sorcery spell unless its controller pays {1}.').
card_image_name('judge\'s familiar'/'RTR', 'judge\'s familiar').
card_uid('judge\'s familiar'/'RTR', 'RTR:Judge\'s Familiar:judge\'s familiar').
card_rarity('judge\'s familiar'/'RTR', 'Uncommon').
card_artist('judge\'s familiar'/'RTR', 'Jack Wang').
card_number('judge\'s familiar'/'RTR', '218').
card_flavor_text('judge\'s familiar'/'RTR', '\"It misses nothing and it has no sense of humor.\"\n—Barvisa, courtroom scribe').
card_multiverse_id('judge\'s familiar'/'RTR', '289221').
card_watermark('judge\'s familiar'/'RTR', 'Azorius').

card_in_set('keening apparition', 'RTR').
card_original_type('keening apparition'/'RTR', 'Creature — Spirit').
card_original_text('keening apparition'/'RTR', 'Sacrifice Keening Apparition: Destroy target enchantment.').
card_first_print('keening apparition', 'RTR').
card_image_name('keening apparition'/'RTR', 'keening apparition').
card_uid('keening apparition'/'RTR', 'RTR:Keening Apparition:keening apparition').
card_rarity('keening apparition'/'RTR', 'Common').
card_artist('keening apparition'/'RTR', 'Terese Nielsen').
card_number('keening apparition'/'RTR', '12').
card_flavor_text('keening apparition'/'RTR', 'Some souls are too damaged to be of use to the Orzhov.').
card_multiverse_id('keening apparition'/'RTR', '271100').

card_in_set('knightly valor', 'RTR').
card_original_type('knightly valor'/'RTR', 'Enchantment — Aura').
card_original_text('knightly valor'/'RTR', 'Enchant creature\nWhen Knightly Valor enters the battlefield, put a 2/2 white Knight creature token with vigilance onto the battlefield.\nEnchanted creature gets +2/+2 and has vigilance.').
card_first_print('knightly valor', 'RTR').
card_image_name('knightly valor'/'RTR', 'knightly valor').
card_uid('knightly valor'/'RTR', 'RTR:Knightly Valor:knightly valor').
card_rarity('knightly valor'/'RTR', 'Common').
card_artist('knightly valor'/'RTR', 'Matt Stewart').
card_number('knightly valor'/'RTR', '13').
card_multiverse_id('knightly valor'/'RTR', '253646').

card_in_set('korozda guildmage', 'RTR').
card_original_type('korozda guildmage'/'RTR', 'Creature — Elf Shaman').
card_original_text('korozda guildmage'/'RTR', '{1}{B}{G}: Target creature gets +1/+1 and gains intimidate until end of turn.\n{2}{B}{G}, Sacrifice a nontoken creature: Put X 1/1 green Saproling creature tokens onto the battlefield, where X is the sacrificed creature\'s toughness.').
card_image_name('korozda guildmage'/'RTR', 'korozda guildmage').
card_uid('korozda guildmage'/'RTR', 'RTR:Korozda Guildmage:korozda guildmage').
card_rarity('korozda guildmage'/'RTR', 'Uncommon').
card_artist('korozda guildmage'/'RTR', 'Ryan Pancoast').
card_number('korozda guildmage'/'RTR', '176').
card_multiverse_id('korozda guildmage'/'RTR', '270364').
card_watermark('korozda guildmage'/'RTR', 'Golgari').

card_in_set('korozda monitor', 'RTR').
card_original_type('korozda monitor'/'RTR', 'Creature — Lizard').
card_original_text('korozda monitor'/'RTR', 'Trample\nScavenge {5}{G}{G} ({5}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('korozda monitor', 'RTR').
card_image_name('korozda monitor'/'RTR', 'korozda monitor').
card_uid('korozda monitor'/'RTR', 'RTR:Korozda Monitor:korozda monitor').
card_rarity('korozda monitor'/'RTR', 'Common').
card_artist('korozda monitor'/'RTR', 'Lars Grant-West').
card_number('korozda monitor'/'RTR', '129').
card_multiverse_id('korozda monitor'/'RTR', '270797').
card_watermark('korozda monitor'/'RTR', 'Golgari').

card_in_set('launch party', 'RTR').
card_original_type('launch party'/'RTR', 'Instant').
card_original_text('launch party'/'RTR', 'As an additional cost to cast Launch Party, sacrifice a creature.\nDestroy target creature. Its controller loses 2 life.').
card_first_print('launch party', 'RTR').
card_image_name('launch party'/'RTR', 'launch party').
card_uid('launch party'/'RTR', 'RTR:Launch Party:launch party').
card_rarity('launch party'/'RTR', 'Common').
card_artist('launch party'/'RTR', 'Lucas Graciano').
card_number('launch party'/'RTR', '69').
card_flavor_text('launch party'/'RTR', 'Life\'s too short to not do the things you love.').
card_multiverse_id('launch party'/'RTR', '270781').

card_in_set('lobber crew', 'RTR').
card_original_type('lobber crew'/'RTR', 'Creature — Goblin Warrior').
card_original_text('lobber crew'/'RTR', 'Defender\n{T}: Lobber Crew deals 1 damage to each opponent.\nWhenever you cast a multicolored spell, untap Lobber Crew.').
card_first_print('lobber crew', 'RTR').
card_image_name('lobber crew'/'RTR', 'lobber crew').
card_uid('lobber crew'/'RTR', 'RTR:Lobber Crew:lobber crew').
card_rarity('lobber crew'/'RTR', 'Common').
card_artist('lobber crew'/'RTR', 'Greg Staples').
card_number('lobber crew'/'RTR', '99').
card_flavor_text('lobber crew'/'RTR', 'It\'s easier to just aim at everything.').
card_multiverse_id('lobber crew'/'RTR', '289218').

card_in_set('lotleth troll', 'RTR').
card_original_type('lotleth troll'/'RTR', 'Creature — Zombie Troll').
card_original_text('lotleth troll'/'RTR', 'Trample\nDiscard a creature card: Put a +1/+1 counter on Lotleth Troll.\n{B}: Regenerate Lotleth Troll.').
card_first_print('lotleth troll', 'RTR').
card_image_name('lotleth troll'/'RTR', 'lotleth troll').
card_uid('lotleth troll'/'RTR', 'RTR:Lotleth Troll:lotleth troll').
card_rarity('lotleth troll'/'RTR', 'Rare').
card_artist('lotleth troll'/'RTR', 'Vincent Proce').
card_number('lotleth troll'/'RTR', '177').
card_flavor_text('lotleth troll'/'RTR', 'He lurks in the undercity, eager for the corpse haulers to unload their rotting cargo.').
card_multiverse_id('lotleth troll'/'RTR', '253518').
card_watermark('lotleth troll'/'RTR', 'Golgari').

card_in_set('loxodon smiter', 'RTR').
card_original_type('loxodon smiter'/'RTR', 'Creature — Elephant Soldier').
card_original_text('loxodon smiter'/'RTR', 'Loxodon Smiter can\'t be countered.\nIf a spell or ability an opponent controls causes you to discard Loxodon Smiter, put it onto the battlefield instead of putting it into your graveyard.').
card_first_print('loxodon smiter', 'RTR').
card_image_name('loxodon smiter'/'RTR', 'loxodon smiter').
card_uid('loxodon smiter'/'RTR', 'RTR:Loxodon Smiter:loxodon smiter').
card_rarity('loxodon smiter'/'RTR', 'Rare').
card_artist('loxodon smiter'/'RTR', 'Ryan Barger').
card_number('loxodon smiter'/'RTR', '178').
card_multiverse_id('loxodon smiter'/'RTR', '290543').
card_watermark('loxodon smiter'/'RTR', 'Selesnya').

card_in_set('lyev skyknight', 'RTR').
card_original_type('lyev skyknight'/'RTR', 'Creature — Human Knight').
card_original_text('lyev skyknight'/'RTR', 'Flying\nWhen Lyev Skyknight enters the battlefield, detain target nonland permanent an opponent controls. (Until your next turn, that permanent can\'t attack or block and its activated abilities can\'t be activated.)').
card_first_print('lyev skyknight', 'RTR').
card_image_name('lyev skyknight'/'RTR', 'lyev skyknight').
card_uid('lyev skyknight'/'RTR', 'RTR:Lyev Skyknight:lyev skyknight').
card_rarity('lyev skyknight'/'RTR', 'Uncommon').
card_artist('lyev skyknight'/'RTR', 'Johannes Voss').
card_number('lyev skyknight'/'RTR', '179').
card_multiverse_id('lyev skyknight'/'RTR', '265391').
card_watermark('lyev skyknight'/'RTR', 'Azorius').

card_in_set('mana bloom', 'RTR').
card_original_type('mana bloom'/'RTR', 'Enchantment').
card_original_text('mana bloom'/'RTR', 'Mana Bloom enters the battlefield with X charge counters on it.\nRemove a charge counter from Mana Bloom: Add one mana of any color to your mana pool. Activate this ability only once each turn.\nAt the beginning of your upkeep, if Mana Bloom has no charge counters on it, return it to its owner\'s hand.').
card_first_print('mana bloom', 'RTR').
card_image_name('mana bloom'/'RTR', 'mana bloom').
card_uid('mana bloom'/'RTR', 'RTR:Mana Bloom:mana bloom').
card_rarity('mana bloom'/'RTR', 'Rare').
card_artist('mana bloom'/'RTR', 'Mike Bierek').
card_number('mana bloom'/'RTR', '130').
card_multiverse_id('mana bloom'/'RTR', '253592').

card_in_set('martial law', 'RTR').
card_original_type('martial law'/'RTR', 'Enchantment').
card_original_text('martial law'/'RTR', 'At the beginning of your upkeep, detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)').
card_first_print('martial law', 'RTR').
card_image_name('martial law'/'RTR', 'martial law').
card_uid('martial law'/'RTR', 'RTR:Martial Law:martial law').
card_rarity('martial law'/'RTR', 'Rare').
card_artist('martial law'/'RTR', 'Tyler Jacobson').
card_number('martial law'/'RTR', '14').
card_flavor_text('martial law'/'RTR', '\"The good of society matters much more than the inconvenience of a few.\"\n—Agmand Sarv, Azorius hussar').
card_multiverse_id('martial law'/'RTR', '253598').
card_watermark('martial law'/'RTR', 'Azorius').

card_in_set('mercurial chemister', 'RTR').
card_original_type('mercurial chemister'/'RTR', 'Creature — Human Wizard').
card_original_text('mercurial chemister'/'RTR', '{U}, {T}: Draw two cards.\n{R}, {T}, Discard a card: Mercurial Chemister deals damage to target creature equal to the discarded card\'s converted mana cost.').
card_first_print('mercurial chemister', 'RTR').
card_image_name('mercurial chemister'/'RTR', 'mercurial chemister').
card_uid('mercurial chemister'/'RTR', 'RTR:Mercurial Chemister:mercurial chemister').
card_rarity('mercurial chemister'/'RTR', 'Rare').
card_artist('mercurial chemister'/'RTR', 'Wesley Burt').
card_number('mercurial chemister'/'RTR', '180').
card_flavor_text('mercurial chemister'/'RTR', 'What some see as \"distracted\" is really \"fathoming the unfathomable.\"').
card_multiverse_id('mercurial chemister'/'RTR', '253510').
card_watermark('mercurial chemister'/'RTR', 'Izzet').

card_in_set('mind rot', 'RTR').
card_original_type('mind rot'/'RTR', 'Sorcery').
card_original_text('mind rot'/'RTR', 'Target player discards two cards.').
card_image_name('mind rot'/'RTR', 'mind rot').
card_uid('mind rot'/'RTR', 'RTR:Mind Rot:mind rot').
card_rarity('mind rot'/'RTR', 'Common').
card_artist('mind rot'/'RTR', 'Yeong-Hao Han').
card_number('mind rot'/'RTR', '70').
card_flavor_text('mind rot'/'RTR', 'With no memories left to compare to his present situation, Andalum was finally content.').
card_multiverse_id('mind rot'/'RTR', '265408').

card_in_set('minotaur aggressor', 'RTR').
card_original_type('minotaur aggressor'/'RTR', 'Creature — Minotaur Berserker').
card_original_text('minotaur aggressor'/'RTR', 'First strike, haste').
card_first_print('minotaur aggressor', 'RTR').
card_image_name('minotaur aggressor'/'RTR', 'minotaur aggressor').
card_uid('minotaur aggressor'/'RTR', 'RTR:Minotaur Aggressor:minotaur aggressor').
card_rarity('minotaur aggressor'/'RTR', 'Uncommon').
card_artist('minotaur aggressor'/'RTR', 'Lucas Graciano').
card_number('minotaur aggressor'/'RTR', '100').
card_flavor_text('minotaur aggressor'/'RTR', 'The smelting district is home to many who see the guilds as not for them.').
card_multiverse_id('minotaur aggressor'/'RTR', '270800').

card_in_set('mizzium mortars', 'RTR').
card_original_type('mizzium mortars'/'RTR', 'Sorcery').
card_original_text('mizzium mortars'/'RTR', 'Mizzium Mortars deals 4 damage to target creature you don\'t control.\nOverload {3}{R}{R}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('mizzium mortars', 'RTR').
card_image_name('mizzium mortars'/'RTR', 'mizzium mortars').
card_uid('mizzium mortars'/'RTR', 'RTR:Mizzium Mortars:mizzium mortars').
card_rarity('mizzium mortars'/'RTR', 'Rare').
card_artist('mizzium mortars'/'RTR', 'Noah Bradley').
card_number('mizzium mortars'/'RTR', '101').
card_multiverse_id('mizzium mortars'/'RTR', '253632').
card_watermark('mizzium mortars'/'RTR', 'Izzet').

card_in_set('mizzium skin', 'RTR').
card_original_type('mizzium skin'/'RTR', 'Instant').
card_original_text('mizzium skin'/'RTR', 'Target creature you control gets +0/+1 and gains hexproof until end of turn.\nOverload {1}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('mizzium skin', 'RTR').
card_image_name('mizzium skin'/'RTR', 'mizzium skin').
card_uid('mizzium skin'/'RTR', 'RTR:Mizzium Skin:mizzium skin').
card_rarity('mizzium skin'/'RTR', 'Common').
card_artist('mizzium skin'/'RTR', 'Scott Chou').
card_number('mizzium skin'/'RTR', '45').
card_multiverse_id('mizzium skin'/'RTR', '253636').
card_watermark('mizzium skin'/'RTR', 'Izzet').

card_in_set('mountain', 'RTR').
card_original_type('mountain'/'RTR', 'Basic Land — Mountain').
card_original_text('mountain'/'RTR', 'R').
card_image_name('mountain'/'RTR', 'mountain1').
card_uid('mountain'/'RTR', 'RTR:Mountain:mountain1').
card_rarity('mountain'/'RTR', 'Basic Land').
card_artist('mountain'/'RTR', 'John Avon').
card_number('mountain'/'RTR', '265').
card_multiverse_id('mountain'/'RTR', '289321').

card_in_set('mountain', 'RTR').
card_original_type('mountain'/'RTR', 'Basic Land — Mountain').
card_original_text('mountain'/'RTR', 'R').
card_image_name('mountain'/'RTR', 'mountain2').
card_uid('mountain'/'RTR', 'RTR:Mountain:mountain2').
card_rarity('mountain'/'RTR', 'Basic Land').
card_artist('mountain'/'RTR', 'Yeong-Hao Han').
card_number('mountain'/'RTR', '266').
card_multiverse_id('mountain'/'RTR', '289324').

card_in_set('mountain', 'RTR').
card_original_type('mountain'/'RTR', 'Basic Land — Mountain').
card_original_text('mountain'/'RTR', 'R').
card_image_name('mountain'/'RTR', 'mountain3').
card_uid('mountain'/'RTR', 'RTR:Mountain:mountain3').
card_rarity('mountain'/'RTR', 'Basic Land').
card_artist('mountain'/'RTR', 'Adam Paquette').
card_number('mountain'/'RTR', '267').
card_multiverse_id('mountain'/'RTR', '289322').

card_in_set('mountain', 'RTR').
card_original_type('mountain'/'RTR', 'Basic Land — Mountain').
card_original_text('mountain'/'RTR', 'R').
card_image_name('mountain'/'RTR', 'mountain4').
card_uid('mountain'/'RTR', 'RTR:Mountain:mountain4').
card_rarity('mountain'/'RTR', 'Basic Land').
card_artist('mountain'/'RTR', 'Richard Wright').
card_number('mountain'/'RTR', '268').
card_multiverse_id('mountain'/'RTR', '289323').

card_in_set('mountain', 'RTR').
card_original_type('mountain'/'RTR', 'Basic Land — Mountain').
card_original_text('mountain'/'RTR', 'R').
card_image_name('mountain'/'RTR', 'mountain5').
card_uid('mountain'/'RTR', 'RTR:Mountain:mountain5').
card_rarity('mountain'/'RTR', 'Basic Land').
card_artist('mountain'/'RTR', 'Richard Wright').
card_number('mountain'/'RTR', '269').
card_multiverse_id('mountain'/'RTR', '333720').

card_in_set('necropolis regent', 'RTR').
card_original_type('necropolis regent'/'RTR', 'Creature — Vampire').
card_original_text('necropolis regent'/'RTR', 'Flying\nWhenever a creature you control deals combat damage to a player, put that many +1/+1 counters on it.').
card_first_print('necropolis regent', 'RTR').
card_image_name('necropolis regent'/'RTR', 'necropolis regent').
card_uid('necropolis regent'/'RTR', 'RTR:Necropolis Regent:necropolis regent').
card_rarity('necropolis regent'/'RTR', 'Mythic Rare').
card_artist('necropolis regent'/'RTR', 'Winona Nelson').
card_number('necropolis regent'/'RTR', '71').
card_flavor_text('necropolis regent'/'RTR', '\"Jarad fancies himself king of the undercity, but he\'s merely king of rot.\"').
card_multiverse_id('necropolis regent'/'RTR', '253569').

card_in_set('new prahv guildmage', 'RTR').
card_original_type('new prahv guildmage'/'RTR', 'Creature — Human Wizard').
card_original_text('new prahv guildmage'/'RTR', '{W}{U}: Target creature gains flying until end of turn.\n{3}{W}{U}: Detain target nonland permanent an opponent controls. (Until your next turn, that permanent can\'t attack or block and its activated abilities can\'t be activated.)').
card_first_print('new prahv guildmage', 'RTR').
card_image_name('new prahv guildmage'/'RTR', 'new prahv guildmage').
card_uid('new prahv guildmage'/'RTR', 'RTR:New Prahv Guildmage:new prahv guildmage').
card_rarity('new prahv guildmage'/'RTR', 'Uncommon').
card_artist('new prahv guildmage'/'RTR', 'Karl Kopinski').
card_number('new prahv guildmage'/'RTR', '181').
card_multiverse_id('new prahv guildmage'/'RTR', '270367').
card_watermark('new prahv guildmage'/'RTR', 'Azorius').

card_in_set('niv-mizzet, dracogenius', 'RTR').
card_original_type('niv-mizzet, dracogenius'/'RTR', 'Legendary Creature — Dragon Wizard').
card_original_text('niv-mizzet, dracogenius'/'RTR', 'Flying\nWhenever Niv-Mizzet, Dracogenius deals damage to a player, you may draw a card.\n{U}{R}: Niv-Mizzet, Dracogenius deals 1 damage to target creature or player.').
card_first_print('niv-mizzet, dracogenius', 'RTR').
card_image_name('niv-mizzet, dracogenius'/'RTR', 'niv-mizzet, dracogenius').
card_uid('niv-mizzet, dracogenius'/'RTR', 'RTR:Niv-Mizzet, Dracogenius:niv-mizzet, dracogenius').
card_rarity('niv-mizzet, dracogenius'/'RTR', 'Mythic Rare').
card_artist('niv-mizzet, dracogenius'/'RTR', 'Todd Lockwood').
card_number('niv-mizzet, dracogenius'/'RTR', '183').
card_flavor_text('niv-mizzet, dracogenius'/'RTR', 'He has no patience for minds that do not inspire him or explode by trying.').
card_multiverse_id('niv-mizzet, dracogenius'/'RTR', '253626').
card_watermark('niv-mizzet, dracogenius'/'RTR', 'Izzet').

card_in_set('nivix guildmage', 'RTR').
card_original_type('nivix guildmage'/'RTR', 'Creature — Human Wizard').
card_original_text('nivix guildmage'/'RTR', '{1}{U}{R}: Draw a card, then discard a card.\n{2}{U}{R}: Copy target instant or sorcery spell you control. You may choose new targets for the copy.').
card_first_print('nivix guildmage', 'RTR').
card_image_name('nivix guildmage'/'RTR', 'nivix guildmage').
card_uid('nivix guildmage'/'RTR', 'RTR:Nivix Guildmage:nivix guildmage').
card_rarity('nivix guildmage'/'RTR', 'Uncommon').
card_artist('nivix guildmage'/'RTR', 'Scott M. Fischer').
card_number('nivix guildmage'/'RTR', '182').
card_flavor_text('nivix guildmage'/'RTR', '\"The only action worth taking is one with an unknown outcome.\"').
card_multiverse_id('nivix guildmage'/'RTR', '270366').
card_watermark('nivix guildmage'/'RTR', 'Izzet').

card_in_set('nivmagus elemental', 'RTR').
card_original_type('nivmagus elemental'/'RTR', 'Creature — Elemental').
card_original_text('nivmagus elemental'/'RTR', 'Exile an instant or sorcery spell you control: Put two +1/+1 counters on Nivmagus Elemental. (That spell won\'t resolve.)').
card_first_print('nivmagus elemental', 'RTR').
card_image_name('nivmagus elemental'/'RTR', 'nivmagus elemental').
card_uid('nivmagus elemental'/'RTR', 'RTR:Nivmagus Elemental:nivmagus elemental').
card_rarity('nivmagus elemental'/'RTR', 'Rare').
card_artist('nivmagus elemental'/'RTR', 'Mike Bierek').
card_number('nivmagus elemental'/'RTR', '219').
card_flavor_text('nivmagus elemental'/'RTR', 'When it escaped, the experimenters hesitated. It would cause untold havoc, yet they wished to see it in action.').
card_multiverse_id('nivmagus elemental'/'RTR', '290526').
card_watermark('nivmagus elemental'/'RTR', 'Izzet').

card_in_set('oak street innkeeper', 'RTR').
card_original_type('oak street innkeeper'/'RTR', 'Creature — Elf').
card_original_text('oak street innkeeper'/'RTR', 'As long as it\'s not your turn, tapped creatures you control have hexproof.').
card_first_print('oak street innkeeper', 'RTR').
card_image_name('oak street innkeeper'/'RTR', 'oak street innkeeper').
card_uid('oak street innkeeper'/'RTR', 'RTR:Oak Street Innkeeper:oak street innkeeper').
card_rarity('oak street innkeeper'/'RTR', 'Uncommon').
card_artist('oak street innkeeper'/'RTR', 'Svetlin Velinov').
card_number('oak street innkeeper'/'RTR', '131').
card_flavor_text('oak street innkeeper'/'RTR', 'The city is vast and perilous, but there are places where a weary traveler can feel very much at home.').
card_multiverse_id('oak street innkeeper'/'RTR', '270362').

card_in_set('ogre jailbreaker', 'RTR').
card_original_type('ogre jailbreaker'/'RTR', 'Creature — Ogre Rogue').
card_original_text('ogre jailbreaker'/'RTR', 'Defender\nOgre Jailbreaker can attack as though it didn\'t have defender as long as you control a Gate.').
card_first_print('ogre jailbreaker', 'RTR').
card_image_name('ogre jailbreaker'/'RTR', 'ogre jailbreaker').
card_uid('ogre jailbreaker'/'RTR', 'RTR:Ogre Jailbreaker:ogre jailbreaker').
card_rarity('ogre jailbreaker'/'RTR', 'Common').
card_artist('ogre jailbreaker'/'RTR', 'Karl Kopinski').
card_number('ogre jailbreaker'/'RTR', '72').
card_flavor_text('ogre jailbreaker'/'RTR', 'It took three years for it to occur to him to use the chain as a weapon, but once it did, he made up for lost time.').
card_multiverse_id('ogre jailbreaker'/'RTR', '270371').

card_in_set('overgrown tomb', 'RTR').
card_original_type('overgrown tomb'/'RTR', 'Land — Swamp Forest').
card_original_text('overgrown tomb'/'RTR', '({T}: Add {B} or {G} to your mana pool.)\nAs Overgrown Tomb enters the battlefield, you may pay 2 life. If you don\'t, Overgrown Tomb enters the battlefield tapped.').
card_image_name('overgrown tomb'/'RTR', 'overgrown tomb').
card_uid('overgrown tomb'/'RTR', 'RTR:Overgrown Tomb:overgrown tomb').
card_rarity('overgrown tomb'/'RTR', 'Rare').
card_artist('overgrown tomb'/'RTR', 'Steven Belledin').
card_number('overgrown tomb'/'RTR', '243').
card_flavor_text('overgrown tomb'/'RTR', 'The best funeral shroud is the fabric of new life.').
card_multiverse_id('overgrown tomb'/'RTR', '253680').
card_watermark('overgrown tomb'/'RTR', 'Golgari').

card_in_set('pack rat', 'RTR').
card_original_type('pack rat'/'RTR', 'Creature — Rat').
card_original_text('pack rat'/'RTR', 'Pack Rat\'s power and toughness are each equal to the number of Rats you control.\n{2}{B}, Discard a card: Put a token onto the battlefield that\'s a copy of Pack Rat.').
card_first_print('pack rat', 'RTR').
card_image_name('pack rat'/'RTR', 'pack rat').
card_uid('pack rat'/'RTR', 'RTR:Pack Rat:pack rat').
card_rarity('pack rat'/'RTR', 'Rare').
card_artist('pack rat'/'RTR', 'Kev Walker').
card_number('pack rat'/'RTR', '73').
card_multiverse_id('pack rat'/'RTR', '253624').

card_in_set('palisade giant', 'RTR').
card_original_type('palisade giant'/'RTR', 'Creature — Giant Soldier').
card_original_text('palisade giant'/'RTR', 'All damage that would be dealt to you or another permanent you control is dealt to Palisade Giant instead.').
card_first_print('palisade giant', 'RTR').
card_image_name('palisade giant'/'RTR', 'palisade giant').
card_uid('palisade giant'/'RTR', 'RTR:Palisade Giant:palisade giant').
card_rarity('palisade giant'/'RTR', 'Rare').
card_artist('palisade giant'/'RTR', 'Greg Staples').
card_number('palisade giant'/'RTR', '15').
card_flavor_text('palisade giant'/'RTR', 'Refusing guild allegiances, she fights for those who can\'t protect themselves.').
card_multiverse_id('palisade giant'/'RTR', '253567').

card_in_set('paralyzing grasp', 'RTR').
card_original_type('paralyzing grasp'/'RTR', 'Enchantment — Aura').
card_original_text('paralyzing grasp'/'RTR', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('paralyzing grasp'/'RTR', 'paralyzing grasp').
card_uid('paralyzing grasp'/'RTR', 'RTR:Paralyzing Grasp:paralyzing grasp').
card_rarity('paralyzing grasp'/'RTR', 'Common').
card_artist('paralyzing grasp'/'RTR', 'Scott Chou').
card_number('paralyzing grasp'/'RTR', '46').
card_flavor_text('paralyzing grasp'/'RTR', '\"Obviously, the Firemind\'s mission is worth more than a few goblins.\"\n—Pelener, chamberlain of Niv-Mizzet').
card_multiverse_id('paralyzing grasp'/'RTR', '270955').

card_in_set('perilous shadow', 'RTR').
card_original_type('perilous shadow'/'RTR', 'Creature — Insect Shade').
card_original_text('perilous shadow'/'RTR', '{1}{B}: Perilous Shadow gets +2/+2 until end of turn.').
card_first_print('perilous shadow', 'RTR').
card_image_name('perilous shadow'/'RTR', 'perilous shadow').
card_uid('perilous shadow'/'RTR', 'RTR:Perilous Shadow:perilous shadow').
card_rarity('perilous shadow'/'RTR', 'Common').
card_artist('perilous shadow'/'RTR', 'Clint Cearley').
card_number('perilous shadow'/'RTR', '74').
card_flavor_text('perilous shadow'/'RTR', 'There are some shadows that even the Dimir fear.').
card_multiverse_id('perilous shadow'/'RTR', '265405').

card_in_set('phantom general', 'RTR').
card_original_type('phantom general'/'RTR', 'Creature — Spirit Soldier').
card_original_text('phantom general'/'RTR', 'Creature tokens you control get +1/+1.').
card_first_print('phantom general', 'RTR').
card_image_name('phantom general'/'RTR', 'phantom general').
card_uid('phantom general'/'RTR', 'RTR:Phantom General:phantom general').
card_rarity('phantom general'/'RTR', 'Uncommon').
card_artist('phantom general'/'RTR', 'Christopher Moeller').
card_number('phantom general'/'RTR', '16').
card_flavor_text('phantom general'/'RTR', 'After his death, the general was reunited with fallen soldiers from over thirty of his campaigns.').
card_multiverse_id('phantom general'/'RTR', '270374').

card_in_set('pithing needle', 'RTR').
card_original_type('pithing needle'/'RTR', 'Artifact').
card_original_text('pithing needle'/'RTR', 'As Pithing Needle enters the battlefield, name a card.\nActivated abilities of sources with the chosen name can\'t be activated unless they\'re mana abilities.').
card_image_name('pithing needle'/'RTR', 'pithing needle').
card_uid('pithing needle'/'RTR', 'RTR:Pithing Needle:pithing needle').
card_rarity('pithing needle'/'RTR', 'Rare').
card_artist('pithing needle'/'RTR', 'Anthony Palumbo').
card_number('pithing needle'/'RTR', '231').
card_flavor_text('pithing needle'/'RTR', 'The fearful want the procedure before a blood festival. The guilty seek it afterward.').
card_multiverse_id('pithing needle'/'RTR', '253581').

card_in_set('plains', 'RTR').
card_original_type('plains'/'RTR', 'Basic Land — Plains').
card_original_text('plains'/'RTR', 'W').
card_image_name('plains'/'RTR', 'plains1').
card_uid('plains'/'RTR', 'RTR:Plains:plains1').
card_rarity('plains'/'RTR', 'Basic Land').
card_artist('plains'/'RTR', 'John Avon').
card_number('plains'/'RTR', '250').
card_multiverse_id('plains'/'RTR', '289310').

card_in_set('plains', 'RTR').
card_original_type('plains'/'RTR', 'Basic Land — Plains').
card_original_text('plains'/'RTR', 'W').
card_image_name('plains'/'RTR', 'plains2').
card_uid('plains'/'RTR', 'RTR:Plains:plains2').
card_rarity('plains'/'RTR', 'Basic Land').
card_artist('plains'/'RTR', 'Yeong-Hao Han').
card_number('plains'/'RTR', '251').
card_multiverse_id('plains'/'RTR', '289311').

card_in_set('plains', 'RTR').
card_original_type('plains'/'RTR', 'Basic Land — Plains').
card_original_text('plains'/'RTR', 'W').
card_image_name('plains'/'RTR', 'plains3').
card_uid('plains'/'RTR', 'RTR:Plains:plains3').
card_rarity('plains'/'RTR', 'Basic Land').
card_artist('plains'/'RTR', 'Adam Paquette').
card_number('plains'/'RTR', '252').
card_multiverse_id('plains'/'RTR', '289309').

card_in_set('plains', 'RTR').
card_original_type('plains'/'RTR', 'Basic Land — Plains').
card_original_text('plains'/'RTR', 'W').
card_image_name('plains'/'RTR', 'plains4').
card_uid('plains'/'RTR', 'RTR:Plains:plains4').
card_rarity('plains'/'RTR', 'Basic Land').
card_artist('plains'/'RTR', 'Richard Wright').
card_number('plains'/'RTR', '253').
card_multiverse_id('plains'/'RTR', '289312').

card_in_set('plains', 'RTR').
card_original_type('plains'/'RTR', 'Basic Land — Plains').
card_original_text('plains'/'RTR', 'W').
card_image_name('plains'/'RTR', 'plains5').
card_uid('plains'/'RTR', 'RTR:Plains:plains5').
card_rarity('plains'/'RTR', 'Basic Land').
card_artist('plains'/'RTR', 'Richard Wright').
card_number('plains'/'RTR', '254').
card_multiverse_id('plains'/'RTR', '333721').

card_in_set('precinct captain', 'RTR').
card_original_type('precinct captain'/'RTR', 'Creature — Human Soldier').
card_original_text('precinct captain'/'RTR', 'First strike\nWhenever Precinct Captain deals combat damage to a player, put a 1/1 white Soldier creature token onto the battlefield.').
card_first_print('precinct captain', 'RTR').
card_image_name('precinct captain'/'RTR', 'precinct captain').
card_uid('precinct captain'/'RTR', 'RTR:Precinct Captain:precinct captain').
card_rarity('precinct captain'/'RTR', 'Rare').
card_artist('precinct captain'/'RTR', 'Steve Prescott').
card_number('precinct captain'/'RTR', '17').
card_flavor_text('precinct captain'/'RTR', '\"In troubled times, we all need someone to watch our back.\"').
card_multiverse_id('precinct captain'/'RTR', '270792').

card_in_set('psychic spiral', 'RTR').
card_original_type('psychic spiral'/'RTR', 'Instant').
card_original_text('psychic spiral'/'RTR', 'Shuffle all cards from your graveyard into your library. Target player puts that many cards from the top of his or her library into his or her graveyard.').
card_first_print('psychic spiral', 'RTR').
card_image_name('psychic spiral'/'RTR', 'psychic spiral').
card_uid('psychic spiral'/'RTR', 'RTR:Psychic Spiral:psychic spiral').
card_rarity('psychic spiral'/'RTR', 'Uncommon').
card_artist('psychic spiral'/'RTR', 'Ryan Pancoast').
card_number('psychic spiral'/'RTR', '47').
card_flavor_text('psychic spiral'/'RTR', 'An elocutor forces criminals to confess to the laws they merely intended to break.').
card_multiverse_id('psychic spiral'/'RTR', '270794').

card_in_set('pursuit of flight', 'RTR').
card_original_type('pursuit of flight'/'RTR', 'Enchantment — Aura').
card_original_text('pursuit of flight'/'RTR', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"{U}: This creature gains flying until end of turn.\"').
card_first_print('pursuit of flight', 'RTR').
card_image_name('pursuit of flight'/'RTR', 'pursuit of flight').
card_uid('pursuit of flight'/'RTR', 'RTR:Pursuit of Flight:pursuit of flight').
card_rarity('pursuit of flight'/'RTR', 'Common').
card_artist('pursuit of flight'/'RTR', 'Christopher Moeller').
card_number('pursuit of flight'/'RTR', '102').
card_flavor_text('pursuit of flight'/'RTR', '\"Watch the voltage. We don\'t need another charred, crashing viashino.\"\n—Bori Andon, Izzet blastseeker').
card_multiverse_id('pursuit of flight'/'RTR', '265390').
card_watermark('pursuit of flight'/'RTR', 'Izzet').

card_in_set('pyroconvergence', 'RTR').
card_original_type('pyroconvergence'/'RTR', 'Enchantment').
card_original_text('pyroconvergence'/'RTR', 'Whenever you cast a multicolored spell, Pyroconvergence deals 2 damage to target creature or player.').
card_first_print('pyroconvergence', 'RTR').
card_image_name('pyroconvergence'/'RTR', 'pyroconvergence').
card_uid('pyroconvergence'/'RTR', 'RTR:Pyroconvergence:pyroconvergence').
card_rarity('pyroconvergence'/'RTR', 'Uncommon').
card_artist('pyroconvergence'/'RTR', 'Jack Wang').
card_number('pyroconvergence'/'RTR', '103').
card_flavor_text('pyroconvergence'/'RTR', '\"The Izzet are an equation that turns lunacy into explosions.\"\n—Leonos, Azorius arbiter').
card_multiverse_id('pyroconvergence'/'RTR', '253568').

card_in_set('racecourse fury', 'RTR').
card_original_type('racecourse fury'/'RTR', 'Enchantment — Aura').
card_original_text('racecourse fury'/'RTR', 'Enchant land\nEnchanted land has \"{T}: Target creature gains haste until end of turn.\"').
card_first_print('racecourse fury', 'RTR').
card_image_name('racecourse fury'/'RTR', 'racecourse fury').
card_uid('racecourse fury'/'RTR', 'RTR:Racecourse Fury:racecourse fury').
card_rarity('racecourse fury'/'RTR', 'Uncommon').
card_artist('racecourse fury'/'RTR', 'Sam Burley').
card_number('racecourse fury'/'RTR', '104').
card_flavor_text('racecourse fury'/'RTR', 'Close finishes are settled by who can dismount and get to weapons first.').
card_multiverse_id('racecourse fury'/'RTR', '253599').

card_in_set('rakdos cackler', 'RTR').
card_original_type('rakdos cackler'/'RTR', 'Creature — Devil').
card_original_text('rakdos cackler'/'RTR', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_image_name('rakdos cackler'/'RTR', 'rakdos cackler').
card_uid('rakdos cackler'/'RTR', 'RTR:Rakdos Cackler:rakdos cackler').
card_rarity('rakdos cackler'/'RTR', 'Uncommon').
card_artist('rakdos cackler'/'RTR', 'Ryan Barger').
card_number('rakdos cackler'/'RTR', '220').
card_flavor_text('rakdos cackler'/'RTR', 'Devil-blades and dripping blood make music Rakdos never tires of hearing.').
card_multiverse_id('rakdos cackler'/'RTR', '253596').
card_watermark('rakdos cackler'/'RTR', 'Rakdos').

card_in_set('rakdos charm', 'RTR').
card_original_type('rakdos charm'/'RTR', 'Instant').
card_original_text('rakdos charm'/'RTR', 'Choose one — Exile all cards from target player\'s graveyard; or destroy target artifact; or each creature deals 1 damage to its controller.').
card_first_print('rakdos charm', 'RTR').
card_image_name('rakdos charm'/'RTR', 'rakdos charm').
card_uid('rakdos charm'/'RTR', 'RTR:Rakdos Charm:rakdos charm').
card_rarity('rakdos charm'/'RTR', 'Uncommon').
card_artist('rakdos charm'/'RTR', 'Zoltan Boros').
card_number('rakdos charm'/'RTR', '184').
card_flavor_text('rakdos charm'/'RTR', '\"Let all feel joy in pain.\"\n—Rakdos').
card_multiverse_id('rakdos charm'/'RTR', '253590').
card_watermark('rakdos charm'/'RTR', 'Rakdos').

card_in_set('rakdos guildgate', 'RTR').
card_original_type('rakdos guildgate'/'RTR', 'Land — Gate').
card_original_text('rakdos guildgate'/'RTR', 'Rakdos Guildgate enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('rakdos guildgate', 'RTR').
card_image_name('rakdos guildgate'/'RTR', 'rakdos guildgate').
card_uid('rakdos guildgate'/'RTR', 'RTR:Rakdos Guildgate:rakdos guildgate').
card_rarity('rakdos guildgate'/'RTR', 'Common').
card_artist('rakdos guildgate'/'RTR', 'Eytan Zana').
card_number('rakdos guildgate'/'RTR', '244').
card_flavor_text('rakdos guildgate'/'RTR', 'Enter and indulge your darkest fantasies, for you may never pass this way again.').
card_multiverse_id('rakdos guildgate'/'RTR', '270953').
card_watermark('rakdos guildgate'/'RTR', 'Rakdos').

card_in_set('rakdos keyrune', 'RTR').
card_original_type('rakdos keyrune'/'RTR', 'Artifact').
card_original_text('rakdos keyrune'/'RTR', '{T}: Add {B} or {R} to your mana pool.\n{B}{R}: Rakdos Keyrune becomes a 3/1 black and red Devil artifact creature with first strike until end of turn.').
card_first_print('rakdos keyrune', 'RTR').
card_image_name('rakdos keyrune'/'RTR', 'rakdos keyrune').
card_uid('rakdos keyrune'/'RTR', 'RTR:Rakdos Keyrune:rakdos keyrune').
card_rarity('rakdos keyrune'/'RTR', 'Uncommon').
card_artist('rakdos keyrune'/'RTR', 'Daniel Ljunggren').
card_number('rakdos keyrune'/'RTR', '232').
card_flavor_text('rakdos keyrune'/'RTR', 'The Rakdos use it for everything you can think of and plenty more than that.').
card_multiverse_id('rakdos keyrune'/'RTR', '253617').
card_watermark('rakdos keyrune'/'RTR', 'Rakdos').

card_in_set('rakdos ragemutt', 'RTR').
card_original_type('rakdos ragemutt'/'RTR', 'Creature — Elemental Hound').
card_original_text('rakdos ragemutt'/'RTR', 'Lifelink, haste').
card_first_print('rakdos ragemutt', 'RTR').
card_image_name('rakdos ragemutt'/'RTR', 'rakdos ragemutt').
card_uid('rakdos ragemutt'/'RTR', 'RTR:Rakdos Ragemutt:rakdos ragemutt').
card_rarity('rakdos ragemutt'/'RTR', 'Uncommon').
card_artist('rakdos ragemutt'/'RTR', 'Ryan Barger').
card_number('rakdos ragemutt'/'RTR', '185').
card_flavor_text('rakdos ragemutt'/'RTR', 'Ragemutts pull the chariots of the Butcher Clowns, a trio of wingless, zombified faeries formerly of the Izzet.').
card_multiverse_id('rakdos ragemutt'/'RTR', '290525').
card_watermark('rakdos ragemutt'/'RTR', 'Rakdos').

card_in_set('rakdos ringleader', 'RTR').
card_original_type('rakdos ringleader'/'RTR', 'Creature — Skeleton Warrior').
card_original_text('rakdos ringleader'/'RTR', 'First strike\nWhenever Rakdos Ringleader deals combat damage to a player, that player discards a card at random.\n{B}: Regenerate Rakdos Ringleader.').
card_first_print('rakdos ringleader', 'RTR').
card_image_name('rakdos ringleader'/'RTR', 'rakdos ringleader').
card_uid('rakdos ringleader'/'RTR', 'RTR:Rakdos Ringleader:rakdos ringleader').
card_rarity('rakdos ringleader'/'RTR', 'Uncommon').
card_artist('rakdos ringleader'/'RTR', 'Jason Felix').
card_number('rakdos ringleader'/'RTR', '186').
card_multiverse_id('rakdos ringleader'/'RTR', '253585').
card_watermark('rakdos ringleader'/'RTR', 'Rakdos').

card_in_set('rakdos shred-freak', 'RTR').
card_original_type('rakdos shred-freak'/'RTR', 'Creature — Human Berserker').
card_original_text('rakdos shred-freak'/'RTR', 'Haste').
card_first_print('rakdos shred-freak', 'RTR').
card_image_name('rakdos shred-freak'/'RTR', 'rakdos shred-freak').
card_uid('rakdos shred-freak'/'RTR', 'RTR:Rakdos Shred-Freak:rakdos shred-freak').
card_rarity('rakdos shred-freak'/'RTR', 'Common').
card_artist('rakdos shred-freak'/'RTR', 'Wayne Reynolds').
card_number('rakdos shred-freak'/'RTR', '221').
card_flavor_text('rakdos shred-freak'/'RTR', '\"If there were such a thing as a soul, I think it would be behind the gallbladder but above the kidneys.\"').
card_multiverse_id('rakdos shred-freak'/'RTR', '253591').
card_watermark('rakdos shred-freak'/'RTR', 'Rakdos').

card_in_set('rakdos\'s return', 'RTR').
card_original_type('rakdos\'s return'/'RTR', 'Sorcery').
card_original_text('rakdos\'s return'/'RTR', 'Rakdos\'s Return deals X damage to target opponent. That player discards X cards.').
card_first_print('rakdos\'s return', 'RTR').
card_image_name('rakdos\'s return'/'RTR', 'rakdos\'s return').
card_uid('rakdos\'s return'/'RTR', 'RTR:Rakdos\'s Return:rakdos\'s return').
card_rarity('rakdos\'s return'/'RTR', 'Mythic Rare').
card_artist('rakdos\'s return'/'RTR', 'Daarken').
card_number('rakdos\'s return'/'RTR', '188').
card_flavor_text('rakdos\'s return'/'RTR', 'When Lord Rakdos stirred from his slumber, everyone else\'s nightmare began.').
card_multiverse_id('rakdos\'s return'/'RTR', '253574').
card_watermark('rakdos\'s return'/'RTR', 'Rakdos').

card_in_set('rakdos, lord of riots', 'RTR').
card_original_type('rakdos, lord of riots'/'RTR', 'Legendary Creature — Demon').
card_original_text('rakdos, lord of riots'/'RTR', 'You can\'t cast Rakdos, Lord of Riots unless an opponent lost life this turn.\nFlying, trample\nCreature spells you cast cost {1} less to cast for each 1 life your opponents have lost this turn.').
card_first_print('rakdos, lord of riots', 'RTR').
card_image_name('rakdos, lord of riots'/'RTR', 'rakdos, lord of riots').
card_uid('rakdos, lord of riots'/'RTR', 'RTR:Rakdos, Lord of Riots:rakdos, lord of riots').
card_rarity('rakdos, lord of riots'/'RTR', 'Mythic Rare').
card_artist('rakdos, lord of riots'/'RTR', 'Vincent Proce').
card_number('rakdos, lord of riots'/'RTR', '187').
card_multiverse_id('rakdos, lord of riots'/'RTR', '253532').
card_watermark('rakdos, lord of riots'/'RTR', 'Rakdos').

card_in_set('rest in peace', 'RTR').
card_original_type('rest in peace'/'RTR', 'Enchantment').
card_original_text('rest in peace'/'RTR', 'When Rest in Peace enters the battlefield, exile all cards from all graveyards.\nIf a card or token would be put into a graveyard from anywhere, exile it instead.').
card_first_print('rest in peace', 'RTR').
card_image_name('rest in peace'/'RTR', 'rest in peace').
card_uid('rest in peace'/'RTR', 'RTR:Rest in Peace:rest in peace').
card_rarity('rest in peace'/'RTR', 'Rare').
card_artist('rest in peace'/'RTR', 'Terese Nielsen').
card_number('rest in peace'/'RTR', '18').
card_flavor_text('rest in peace'/'RTR', 'Some corpses the Golgari cannot claim. Some souls the Orzhov cannot shackle.').
card_multiverse_id('rest in peace'/'RTR', '277995').

card_in_set('righteous authority', 'RTR').
card_original_type('righteous authority'/'RTR', 'Enchantment — Aura').
card_original_text('righteous authority'/'RTR', 'Enchant creature\nEnchanted creature gets +1/+1 for each card in its controller\'s hand.\nAt the beginning of the draw step of enchanted creature\'s controller, that player draws an additional card.').
card_first_print('righteous authority', 'RTR').
card_image_name('righteous authority'/'RTR', 'righteous authority').
card_uid('righteous authority'/'RTR', 'RTR:Righteous Authority:righteous authority').
card_rarity('righteous authority'/'RTR', 'Rare').
card_artist('righteous authority'/'RTR', 'Scott Chou').
card_number('righteous authority'/'RTR', '189').
card_multiverse_id('righteous authority'/'RTR', '253546').
card_watermark('righteous authority'/'RTR', 'Azorius').

card_in_set('risen sanctuary', 'RTR').
card_original_type('risen sanctuary'/'RTR', 'Creature — Elemental').
card_original_text('risen sanctuary'/'RTR', 'Vigilance').
card_first_print('risen sanctuary', 'RTR').
card_image_name('risen sanctuary'/'RTR', 'risen sanctuary').
card_uid('risen sanctuary'/'RTR', 'RTR:Risen Sanctuary:risen sanctuary').
card_rarity('risen sanctuary'/'RTR', 'Uncommon').
card_artist('risen sanctuary'/'RTR', 'Chase Stone').
card_number('risen sanctuary'/'RTR', '190').
card_flavor_text('risen sanctuary'/'RTR', 'When one of the great guardians arises, it sweeps enemies aside like chaff yet takes care not to crush a single insect underfoot.').
card_multiverse_id('risen sanctuary'/'RTR', '290530').
card_watermark('risen sanctuary'/'RTR', 'Selesnya').

card_in_set('rites of reaping', 'RTR').
card_original_type('rites of reaping'/'RTR', 'Sorcery').
card_original_text('rites of reaping'/'RTR', 'Target creature gets +3/+3 until end of turn. Another target creature gets -3/-3 until end of turn.').
card_first_print('rites of reaping', 'RTR').
card_image_name('rites of reaping'/'RTR', 'rites of reaping').
card_uid('rites of reaping'/'RTR', 'RTR:Rites of Reaping:rites of reaping').
card_rarity('rites of reaping'/'RTR', 'Uncommon').
card_artist('rites of reaping'/'RTR', 'David Rapoza').
card_number('rites of reaping'/'RTR', '191').
card_flavor_text('rites of reaping'/'RTR', 'Don\'t cross a Devkarin elf unless you long for an early grave.').
card_multiverse_id('rites of reaping'/'RTR', '289219').
card_watermark('rites of reaping'/'RTR', 'Golgari').

card_in_set('rix maadi guildmage', 'RTR').
card_original_type('rix maadi guildmage'/'RTR', 'Creature — Human Shaman').
card_original_text('rix maadi guildmage'/'RTR', '{B}{R}: Target blocking creature gets -1/-1 until end of turn.\n{B}{R}: Target player who lost life this turn loses 1 life.').
card_first_print('rix maadi guildmage', 'RTR').
card_image_name('rix maadi guildmage'/'RTR', 'rix maadi guildmage').
card_uid('rix maadi guildmage'/'RTR', 'RTR:Rix Maadi Guildmage:rix maadi guildmage').
card_rarity('rix maadi guildmage'/'RTR', 'Uncommon').
card_artist('rix maadi guildmage'/'RTR', 'Karl Kopinski').
card_number('rix maadi guildmage'/'RTR', '192').
card_flavor_text('rix maadi guildmage'/'RTR', 'Insatiable, irresistible, and insane.').
card_multiverse_id('rix maadi guildmage'/'RTR', '270351').
card_watermark('rix maadi guildmage'/'RTR', 'Rakdos').

card_in_set('rogue\'s passage', 'RTR').
card_original_type('rogue\'s passage'/'RTR', 'Land').
card_original_text('rogue\'s passage'/'RTR', '{T}: Add {1} to your mana pool.\n{4}, {T}: Target creature is unblockable this turn.').
card_first_print('rogue\'s passage', 'RTR').
card_image_name('rogue\'s passage'/'RTR', 'rogue\'s passage').
card_uid('rogue\'s passage'/'RTR', 'RTR:Rogue\'s Passage:rogue\'s passage').
card_rarity('rogue\'s passage'/'RTR', 'Uncommon').
card_artist('rogue\'s passage'/'RTR', 'Christine Choi').
card_number('rogue\'s passage'/'RTR', '245').
card_flavor_text('rogue\'s passage'/'RTR', 'Rumors quickly spread among thieves about a labyrinth without walls and a prize beyond all measures of worth.').
card_multiverse_id('rogue\'s passage'/'RTR', '277992').

card_in_set('rootborn defenses', 'RTR').
card_original_type('rootborn defenses'/'RTR', 'Instant').
card_original_text('rootborn defenses'/'RTR', 'Populate. Creatures you control are indestructible this turn. (To populate, put a token onto the battlefield that\'s a copy of a creature token you control. Damage and effects that say \"destroy\" don\'t destroy indestructible creatures.)').
card_first_print('rootborn defenses', 'RTR').
card_image_name('rootborn defenses'/'RTR', 'rootborn defenses').
card_uid('rootborn defenses'/'RTR', 'RTR:Rootborn Defenses:rootborn defenses').
card_rarity('rootborn defenses'/'RTR', 'Common').
card_artist('rootborn defenses'/'RTR', 'Mark Zug').
card_number('rootborn defenses'/'RTR', '19').
card_multiverse_id('rootborn defenses'/'RTR', '253529').
card_watermark('rootborn defenses'/'RTR', 'Selesnya').

card_in_set('rubbleback rhino', 'RTR').
card_original_type('rubbleback rhino'/'RTR', 'Creature — Rhino').
card_original_text('rubbleback rhino'/'RTR', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_first_print('rubbleback rhino', 'RTR').
card_image_name('rubbleback rhino'/'RTR', 'rubbleback rhino').
card_uid('rubbleback rhino'/'RTR', 'RTR:Rubbleback Rhino:rubbleback rhino').
card_rarity('rubbleback rhino'/'RTR', 'Common').
card_artist('rubbleback rhino'/'RTR', 'Johann Bodin').
card_number('rubbleback rhino'/'RTR', '132').
card_flavor_text('rubbleback rhino'/'RTR', 'The trouble started when a street urchin bet a goblin he could ride one until the clock on Shilbo\'s Tower struck thirteen.').
card_multiverse_id('rubbleback rhino'/'RTR', '265396').

card_in_set('runewing', 'RTR').
card_original_type('runewing'/'RTR', 'Creature — Bird').
card_original_text('runewing'/'RTR', 'Flying\nWhen Runewing dies, draw a card.').
card_first_print('runewing', 'RTR').
card_image_name('runewing'/'RTR', 'runewing').
card_uid('runewing'/'RTR', 'RTR:Runewing:runewing').
card_rarity('runewing'/'RTR', 'Common').
card_artist('runewing'/'RTR', 'Martina Pilcerova').
card_number('runewing'/'RTR', '48').
card_flavor_text('runewing'/'RTR', 'In the hands of the open-minded, a runewing quill writes wisdom of its own.').
card_multiverse_id('runewing'/'RTR', '265394').

card_in_set('savage surge', 'RTR').
card_original_type('savage surge'/'RTR', 'Instant').
card_original_text('savage surge'/'RTR', 'Target creature gets +2/+2 until end of turn. Untap that creature.').
card_first_print('savage surge', 'RTR').
card_image_name('savage surge'/'RTR', 'savage surge').
card_uid('savage surge'/'RTR', 'RTR:Savage Surge:savage surge').
card_rarity('savage surge'/'RTR', 'Uncommon').
card_artist('savage surge'/'RTR', 'Svetlin Velinov').
card_number('savage surge'/'RTR', '133').
card_flavor_text('savage surge'/'RTR', 'Gruul warriors never need to be stirred to battle. They need only to be shown where the battle is.').
card_multiverse_id('savage surge'/'RTR', '265388').

card_in_set('search the city', 'RTR').
card_original_type('search the city'/'RTR', 'Enchantment').
card_original_text('search the city'/'RTR', 'When Search the City enters the battlefield, exile the top five cards of your library.\nWhenever you play a card with the same name as one of the exiled cards, you may put one of those cards with that name into its owner\'s hand. Then if there are no cards exiled with Search the City, sacrifice it. If you do, take an extra turn after this one.').
card_first_print('search the city', 'RTR').
card_image_name('search the city'/'RTR', 'search the city').
card_uid('search the city'/'RTR', 'RTR:Search the City:search the city').
card_rarity('search the city'/'RTR', 'Rare').
card_artist('search the city'/'RTR', 'Jack Wang').
card_number('search the city'/'RTR', '49').
card_multiverse_id('search the city'/'RTR', '265404').

card_in_set('search warrant', 'RTR').
card_original_type('search warrant'/'RTR', 'Sorcery').
card_original_text('search warrant'/'RTR', 'Target player reveals his or her hand. You gain life equal to the number of cards in that player\'s hand.').
card_first_print('search warrant', 'RTR').
card_image_name('search warrant'/'RTR', 'search warrant').
card_uid('search warrant'/'RTR', 'RTR:Search Warrant:search warrant').
card_rarity('search warrant'/'RTR', 'Common').
card_artist('search warrant'/'RTR', 'Steven Belledin').
card_number('search warrant'/'RTR', '193').
card_flavor_text('search warrant'/'RTR', '\"And would you care to explain why your satchel is radiating an aura of unstable energy?\"').
card_multiverse_id('search warrant'/'RTR', '253614').
card_watermark('search warrant'/'RTR', 'Azorius').

card_in_set('security blockade', 'RTR').
card_original_type('security blockade'/'RTR', 'Enchantment — Aura').
card_original_text('security blockade'/'RTR', 'Enchant land\nWhen Security Blockade enters the battlefield, put a 2/2 white Knight creature token with vigilance onto the battlefield.\nEnchanted land has \"{T}: Prevent the next 1 damage that would be dealt to you this turn.\"').
card_first_print('security blockade', 'RTR').
card_image_name('security blockade'/'RTR', 'security blockade').
card_uid('security blockade'/'RTR', 'RTR:Security Blockade:security blockade').
card_rarity('security blockade'/'RTR', 'Uncommon').
card_artist('security blockade'/'RTR', 'James Ryman').
card_number('security blockade'/'RTR', '20').
card_multiverse_id('security blockade'/'RTR', '270790').

card_in_set('seek the horizon', 'RTR').
card_original_type('seek the horizon'/'RTR', 'Sorcery').
card_original_text('seek the horizon'/'RTR', 'Search your library for up to three basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('seek the horizon'/'RTR', 'seek the horizon').
card_uid('seek the horizon'/'RTR', 'RTR:Seek the Horizon:seek the horizon').
card_rarity('seek the horizon'/'RTR', 'Uncommon').
card_artist('seek the horizon'/'RTR', 'Howard Lyon').
card_number('seek the horizon'/'RTR', '134').
card_flavor_text('seek the horizon'/'RTR', 'Raked by vertical spikes of glass and stone, Ravnica\'s vistas offer only a rare glimpse of the horizon.').
card_multiverse_id('seek the horizon'/'RTR', '289224').

card_in_set('selesnya charm', 'RTR').
card_original_type('selesnya charm'/'RTR', 'Instant').
card_original_text('selesnya charm'/'RTR', 'Choose one — Target creature gets +2/+2 and gains trample until end of turn; or exile target creature with power 5 or greater; or put a 2/2 white Knight creature token with vigilance onto the battlefield.').
card_first_print('selesnya charm', 'RTR').
card_image_name('selesnya charm'/'RTR', 'selesnya charm').
card_uid('selesnya charm'/'RTR', 'RTR:Selesnya Charm:selesnya charm').
card_rarity('selesnya charm'/'RTR', 'Uncommon').
card_artist('selesnya charm'/'RTR', 'Zoltan Boros').
card_number('selesnya charm'/'RTR', '194').
card_multiverse_id('selesnya charm'/'RTR', '253651').
card_watermark('selesnya charm'/'RTR', 'Selesnya').

card_in_set('selesnya guildgate', 'RTR').
card_original_type('selesnya guildgate'/'RTR', 'Land — Gate').
card_original_text('selesnya guildgate'/'RTR', 'Selesnya Guildgate enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('selesnya guildgate', 'RTR').
card_image_name('selesnya guildgate'/'RTR', 'selesnya guildgate').
card_uid('selesnya guildgate'/'RTR', 'RTR:Selesnya Guildgate:selesnya guildgate').
card_rarity('selesnya guildgate'/'RTR', 'Common').
card_artist('selesnya guildgate'/'RTR', 'Howard Lyon').
card_number('selesnya guildgate'/'RTR', '246').
card_flavor_text('selesnya guildgate'/'RTR', 'Enter and rejoice! The Conclave stands united, open to one and all.').
card_multiverse_id('selesnya guildgate'/'RTR', '271099').
card_watermark('selesnya guildgate'/'RTR', 'Selesnya').

card_in_set('selesnya keyrune', 'RTR').
card_original_type('selesnya keyrune'/'RTR', 'Artifact').
card_original_text('selesnya keyrune'/'RTR', '{T}: Add {G} or {W} to your mana pool.\n{G}{W}: Selesnya Keyrune becomes a 3/3 green and white Wolf artifact creature until end of turn.').
card_first_print('selesnya keyrune', 'RTR').
card_image_name('selesnya keyrune'/'RTR', 'selesnya keyrune').
card_uid('selesnya keyrune'/'RTR', 'RTR:Selesnya Keyrune:selesnya keyrune').
card_rarity('selesnya keyrune'/'RTR', 'Uncommon').
card_artist('selesnya keyrune'/'RTR', 'Daniel Ljunggren').
card_number('selesnya keyrune'/'RTR', '233').
card_flavor_text('selesnya keyrune'/'RTR', 'A mark of unity when you want one; a fierce guardian when you need one.').
card_multiverse_id('selesnya keyrune'/'RTR', '253631').
card_watermark('selesnya keyrune'/'RTR', 'Selesnya').

card_in_set('selesnya sentry', 'RTR').
card_original_type('selesnya sentry'/'RTR', 'Creature — Elephant Soldier').
card_original_text('selesnya sentry'/'RTR', '{5}{G}: Regenerate Selesnya Sentry.').
card_first_print('selesnya sentry', 'RTR').
card_image_name('selesnya sentry'/'RTR', 'selesnya sentry').
card_uid('selesnya sentry'/'RTR', 'RTR:Selesnya Sentry:selesnya sentry').
card_rarity('selesnya sentry'/'RTR', 'Common').
card_artist('selesnya sentry'/'RTR', 'Wesley Burt').
card_number('selesnya sentry'/'RTR', '21').
card_flavor_text('selesnya sentry'/'RTR', 'Ravnicans still tell tales about the Battle of Sumala where four Selesnya sentries held off an entire clan of Gruul warriors.').
card_multiverse_id('selesnya sentry'/'RTR', '265382').
card_watermark('selesnya sentry'/'RTR', 'Selesnya').

card_in_set('seller of songbirds', 'RTR').
card_original_type('seller of songbirds'/'RTR', 'Creature — Human').
card_original_text('seller of songbirds'/'RTR', 'When Seller of Songbirds enters the battlefield, put a 1/1 white Bird creature token with flying onto the battlefield.').
card_first_print('seller of songbirds', 'RTR').
card_image_name('seller of songbirds'/'RTR', 'seller of songbirds').
card_uid('seller of songbirds'/'RTR', 'RTR:Seller of Songbirds:seller of songbirds').
card_rarity('seller of songbirds'/'RTR', 'Common').
card_artist('seller of songbirds'/'RTR', 'Christopher Moeller').
card_number('seller of songbirds'/'RTR', '22').
card_flavor_text('seller of songbirds'/'RTR', '\"Lady Wren is the one merchant in Keyhole Downs who isn\'t running a scam.\"\n—Mirela, Azorius hussar').
card_multiverse_id('seller of songbirds'/'RTR', '253656').

card_in_set('sewer shambler', 'RTR').
card_original_type('sewer shambler'/'RTR', 'Creature — Zombie').
card_original_text('sewer shambler'/'RTR', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)\nScavenge {2}{B} ({2}{B}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('sewer shambler', 'RTR').
card_image_name('sewer shambler'/'RTR', 'sewer shambler').
card_uid('sewer shambler'/'RTR', 'RTR:Sewer Shambler:sewer shambler').
card_rarity('sewer shambler'/'RTR', 'Common').
card_artist('sewer shambler'/'RTR', 'Nils Hamm').
card_number('sewer shambler'/'RTR', '75').
card_multiverse_id('sewer shambler'/'RTR', '253576').
card_watermark('sewer shambler'/'RTR', 'Golgari').

card_in_set('shrieking affliction', 'RTR').
card_original_type('shrieking affliction'/'RTR', 'Enchantment').
card_original_text('shrieking affliction'/'RTR', 'At the beginning of each opponent\'s upkeep, if that player has one or fewer cards in hand, he or she loses 3 life.').
card_first_print('shrieking affliction', 'RTR').
card_image_name('shrieking affliction'/'RTR', 'shrieking affliction').
card_uid('shrieking affliction'/'RTR', 'RTR:Shrieking Affliction:shrieking affliction').
card_rarity('shrieking affliction'/'RTR', 'Uncommon').
card_artist('shrieking affliction'/'RTR', 'Johann Bodin').
card_number('shrieking affliction'/'RTR', '76').
card_flavor_text('shrieking affliction'/'RTR', 'A gargoyle\'s voice reaches pitches that will shatter glass and warp bone.').
card_multiverse_id('shrieking affliction'/'RTR', '265409').

card_in_set('skull rend', 'RTR').
card_original_type('skull rend'/'RTR', 'Sorcery').
card_original_text('skull rend'/'RTR', 'Skull Rend deals 2 damage to each opponent. Those players each discard two cards at random.').
card_first_print('skull rend', 'RTR').
card_image_name('skull rend'/'RTR', 'skull rend').
card_uid('skull rend'/'RTR', 'RTR:Skull Rend:skull rend').
card_rarity('skull rend'/'RTR', 'Common').
card_artist('skull rend'/'RTR', 'Greg Staples').
card_number('skull rend'/'RTR', '195').
card_flavor_text('skull rend'/'RTR', '\"We could never accept anyone with a face that pretty or a mind that sane.\"\n—Nyoser, Gore-House ringleader').
card_multiverse_id('skull rend'/'RTR', '253622').
card_watermark('skull rend'/'RTR', 'Rakdos').

card_in_set('skyline predator', 'RTR').
card_original_type('skyline predator'/'RTR', 'Creature — Drake').
card_original_text('skyline predator'/'RTR', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_first_print('skyline predator', 'RTR').
card_image_name('skyline predator'/'RTR', 'skyline predator').
card_uid('skyline predator'/'RTR', 'RTR:Skyline Predator:skyline predator').
card_rarity('skyline predator'/'RTR', 'Uncommon').
card_artist('skyline predator'/'RTR', 'Wesley Burt').
card_number('skyline predator'/'RTR', '50').
card_flavor_text('skyline predator'/'RTR', '\"It will dodge your first arrow and flatten you before there\'s a second.\"\n—Alcarus, Selesnya archer').
card_multiverse_id('skyline predator'/'RTR', '289212').

card_in_set('skymark roc', 'RTR').
card_original_type('skymark roc'/'RTR', 'Creature — Bird').
card_original_text('skymark roc'/'RTR', 'Flying\nWhenever Skymark Roc attacks, you may return target creature defending player controls with toughness 2 or less to its owner\'s hand.').
card_first_print('skymark roc', 'RTR').
card_image_name('skymark roc'/'RTR', 'skymark roc').
card_uid('skymark roc'/'RTR', 'RTR:Skymark Roc:skymark roc').
card_rarity('skymark roc'/'RTR', 'Uncommon').
card_artist('skymark roc'/'RTR', 'Christopher Moeller').
card_number('skymark roc'/'RTR', '196').
card_flavor_text('skymark roc'/'RTR', '\"Try not to look guilty. Or delicious.\"\n—Thorpe, street urchin').
card_multiverse_id('skymark roc'/'RTR', '253555').
card_watermark('skymark roc'/'RTR', 'Azorius').

card_in_set('slaughter games', 'RTR').
card_original_type('slaughter games'/'RTR', 'Sorcery').
card_original_text('slaughter games'/'RTR', 'Slaughter Games can\'t be countered by spells or abilities.\nName a nonland card. Search target opponent\'s graveyard, hand, and library for any number of cards with that name and exile them. Then that player shuffles his or her library.').
card_first_print('slaughter games', 'RTR').
card_image_name('slaughter games'/'RTR', 'slaughter games').
card_uid('slaughter games'/'RTR', 'RTR:Slaughter Games:slaughter games').
card_rarity('slaughter games'/'RTR', 'Rare').
card_artist('slaughter games'/'RTR', 'Steve Prescott').
card_number('slaughter games'/'RTR', '197').
card_multiverse_id('slaughter games'/'RTR', '290532').
card_watermark('slaughter games'/'RTR', 'Rakdos').

card_in_set('slime molding', 'RTR').
card_original_type('slime molding'/'RTR', 'Sorcery').
card_original_text('slime molding'/'RTR', 'Put an X/X green Ooze creature token onto the battlefield.').
card_first_print('slime molding', 'RTR').
card_image_name('slime molding'/'RTR', 'slime molding').
card_uid('slime molding'/'RTR', 'RTR:Slime Molding:slime molding').
card_rarity('slime molding'/'RTR', 'Uncommon').
card_artist('slime molding'/'RTR', 'Marco Nelor').
card_number('slime molding'/'RTR', '135').
card_flavor_text('slime molding'/'RTR', '\"Give me enough refuse and mana and I will summon an ooze that can engulf all of Ravnica.\"\n—Cevraya, Golgari shaman').
card_multiverse_id('slime molding'/'RTR', '290533').

card_in_set('slitherhead', 'RTR').
card_original_type('slitherhead'/'RTR', 'Creature — Plant Zombie').
card_original_text('slitherhead'/'RTR', 'Scavenge {0} ({0}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('slitherhead', 'RTR').
card_image_name('slitherhead'/'RTR', 'slitherhead').
card_uid('slitherhead'/'RTR', 'RTR:Slitherhead:slitherhead').
card_rarity('slitherhead'/'RTR', 'Uncommon').
card_artist('slitherhead'/'RTR', 'Greg Staples').
card_number('slitherhead'/'RTR', '222').
card_flavor_text('slitherhead'/'RTR', '\"It makes better use of Uncle Dragi\'s head than he ever did.\"\n—Pesha, retired blacksmith').
card_multiverse_id('slitherhead'/'RTR', '253586').
card_watermark('slitherhead'/'RTR', 'Golgari').

card_in_set('sluiceway scorpion', 'RTR').
card_original_type('sluiceway scorpion'/'RTR', 'Creature — Scorpion').
card_original_text('sluiceway scorpion'/'RTR', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nScavenge {1}{B}{G} ({1}{B}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('sluiceway scorpion', 'RTR').
card_image_name('sluiceway scorpion'/'RTR', 'sluiceway scorpion').
card_uid('sluiceway scorpion'/'RTR', 'RTR:Sluiceway Scorpion:sluiceway scorpion').
card_rarity('sluiceway scorpion'/'RTR', 'Common').
card_artist('sluiceway scorpion'/'RTR', 'Slawomir Maniak').
card_number('sluiceway scorpion'/'RTR', '198').
card_multiverse_id('sluiceway scorpion'/'RTR', '253607').
card_watermark('sluiceway scorpion'/'RTR', 'Golgari').

card_in_set('slum reaper', 'RTR').
card_original_type('slum reaper'/'RTR', 'Creature — Horror').
card_original_text('slum reaper'/'RTR', 'When Slum Reaper enters the battlefield, each player sacrifices a creature.').
card_first_print('slum reaper', 'RTR').
card_image_name('slum reaper'/'RTR', 'slum reaper').
card_uid('slum reaper'/'RTR', 'RTR:Slum Reaper:slum reaper').
card_rarity('slum reaper'/'RTR', 'Uncommon').
card_artist('slum reaper'/'RTR', 'Karl Kopinski').
card_number('slum reaper'/'RTR', '77').
card_flavor_text('slum reaper'/'RTR', 'It\'s sent into unguilded districts by the Orzhov to collect the souls of those no one will miss.').
card_multiverse_id('slum reaper'/'RTR', '270373').

card_in_set('soul tithe', 'RTR').
card_original_type('soul tithe'/'RTR', 'Enchantment — Aura').
card_original_text('soul tithe'/'RTR', 'Enchant nonland permanent\nAt the beginning of the upkeep of enchanted permanent\'s controller, that player sacrifices it unless he or she pays {X}, where X is its converted mana cost.').
card_first_print('soul tithe', 'RTR').
card_image_name('soul tithe'/'RTR', 'soul tithe').
card_uid('soul tithe'/'RTR', 'RTR:Soul Tithe:soul tithe').
card_rarity('soul tithe'/'RTR', 'Uncommon').
card_artist('soul tithe'/'RTR', 'Dave Kendall').
card_number('soul tithe'/'RTR', '23').
card_multiverse_id('soul tithe'/'RTR', '265372').

card_in_set('soulsworn spirit', 'RTR').
card_original_type('soulsworn spirit'/'RTR', 'Creature — Spirit').
card_original_text('soulsworn spirit'/'RTR', 'Soulsworn Spirit is unblockable.\nWhen Soulsworn Spirit enters the battlefield, detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)').
card_first_print('soulsworn spirit', 'RTR').
card_image_name('soulsworn spirit'/'RTR', 'soulsworn spirit').
card_uid('soulsworn spirit'/'RTR', 'RTR:Soulsworn Spirit:soulsworn spirit').
card_rarity('soulsworn spirit'/'RTR', 'Uncommon').
card_artist('soulsworn spirit'/'RTR', 'James Ryman').
card_number('soulsworn spirit'/'RTR', '51').
card_multiverse_id('soulsworn spirit'/'RTR', '270357').
card_watermark('soulsworn spirit'/'RTR', 'Azorius').

card_in_set('spawn of rix maadi', 'RTR').
card_original_type('spawn of rix maadi'/'RTR', 'Creature — Horror').
card_original_text('spawn of rix maadi'/'RTR', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_first_print('spawn of rix maadi', 'RTR').
card_image_name('spawn of rix maadi'/'RTR', 'spawn of rix maadi').
card_uid('spawn of rix maadi'/'RTR', 'RTR:Spawn of Rix Maadi:spawn of rix maadi').
card_rarity('spawn of rix maadi'/'RTR', 'Common').
card_artist('spawn of rix maadi'/'RTR', 'Min Yum').
card_number('spawn of rix maadi'/'RTR', '199').
card_flavor_text('spawn of rix maadi'/'RTR', '\"Straight from the blood-drenched imagination of our illustrious parun!\"\n—Nyoser, Gore-House ringleader').
card_multiverse_id('spawn of rix maadi'/'RTR', '253549').
card_watermark('spawn of rix maadi'/'RTR', 'Rakdos').

card_in_set('sphere of safety', 'RTR').
card_original_type('sphere of safety'/'RTR', 'Enchantment').
card_original_text('sphere of safety'/'RTR', 'Creatures can\'t attack you or a planeswalker you control unless their controller pays {X} for each of those creatures, where X is the number of enchantments you control.').
card_first_print('sphere of safety', 'RTR').
card_image_name('sphere of safety'/'RTR', 'sphere of safety').
card_uid('sphere of safety'/'RTR', 'RTR:Sphere of Safety:sphere of safety').
card_rarity('sphere of safety'/'RTR', 'Uncommon').
card_artist('sphere of safety'/'RTR', 'Slawomir Maniak').
card_number('sphere of safety'/'RTR', '24').
card_flavor_text('sphere of safety'/'RTR', 'The Rakdos believe that all are eager to join their revels, even when told otherwise.').
card_multiverse_id('sphere of safety'/'RTR', '270969').

card_in_set('sphinx of the chimes', 'RTR').
card_original_type('sphinx of the chimes'/'RTR', 'Creature — Sphinx').
card_original_text('sphinx of the chimes'/'RTR', 'Flying\nDiscard two nonland cards with the same name: Draw four cards.').
card_first_print('sphinx of the chimes', 'RTR').
card_image_name('sphinx of the chimes'/'RTR', 'sphinx of the chimes').
card_uid('sphinx of the chimes'/'RTR', 'RTR:Sphinx of the Chimes:sphinx of the chimes').
card_rarity('sphinx of the chimes'/'RTR', 'Rare').
card_artist('sphinx of the chimes'/'RTR', 'Greg Staples').
card_number('sphinx of the chimes'/'RTR', '52').
card_flavor_text('sphinx of the chimes'/'RTR', '\"I\'ve dreamt of the future, Isperia. Danger is on the horizon. You must heed my warning.\"').
card_multiverse_id('sphinx of the chimes'/'RTR', '253594').

card_in_set('sphinx\'s revelation', 'RTR').
card_original_type('sphinx\'s revelation'/'RTR', 'Instant').
card_original_text('sphinx\'s revelation'/'RTR', 'You gain X life and draw X cards.').
card_first_print('sphinx\'s revelation', 'RTR').
card_image_name('sphinx\'s revelation'/'RTR', 'sphinx\'s revelation').
card_uid('sphinx\'s revelation'/'RTR', 'RTR:Sphinx\'s Revelation:sphinx\'s revelation').
card_rarity('sphinx\'s revelation'/'RTR', 'Mythic Rare').
card_artist('sphinx\'s revelation'/'RTR', 'Slawomir Maniak').
card_number('sphinx\'s revelation'/'RTR', '200').
card_flavor_text('sphinx\'s revelation'/'RTR', '\"Let the knowledge of absolute law inspire you to live a life of absolute order.\"').
card_multiverse_id('sphinx\'s revelation'/'RTR', '253534').
card_watermark('sphinx\'s revelation'/'RTR', 'Azorius').

card_in_set('splatter thug', 'RTR').
card_original_type('splatter thug'/'RTR', 'Creature — Human Warrior').
card_original_text('splatter thug'/'RTR', 'First strike\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_first_print('splatter thug', 'RTR').
card_image_name('splatter thug'/'RTR', 'splatter thug').
card_uid('splatter thug'/'RTR', 'RTR:Splatter Thug:splatter thug').
card_rarity('splatter thug'/'RTR', 'Common').
card_artist('splatter thug'/'RTR', 'Kev Walker').
card_number('splatter thug'/'RTR', '105').
card_flavor_text('splatter thug'/'RTR', 'The mask isn\'t meant to hide his identity. It\'s meant to keep the viscera out of his eyes.').
card_multiverse_id('splatter thug'/'RTR', '265381').
card_watermark('splatter thug'/'RTR', 'Rakdos').

card_in_set('stab wound', 'RTR').
card_original_type('stab wound'/'RTR', 'Enchantment — Aura').
card_original_text('stab wound'/'RTR', 'Enchant creature\nEnchanted creature gets -2/-2.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player loses 2 life.').
card_first_print('stab wound', 'RTR').
card_image_name('stab wound'/'RTR', 'stab wound').
card_uid('stab wound'/'RTR', 'RTR:Stab Wound:stab wound').
card_rarity('stab wound'/'RTR', 'Common').
card_artist('stab wound'/'RTR', 'Scott Chou').
card_number('stab wound'/'RTR', '78').
card_multiverse_id('stab wound'/'RTR', '270791').

card_in_set('stealer of secrets', 'RTR').
card_original_type('stealer of secrets'/'RTR', 'Creature — Human Rogue').
card_original_text('stealer of secrets'/'RTR', 'Whenever Stealer of Secrets deals combat damage to a player, draw a card.').
card_image_name('stealer of secrets'/'RTR', 'stealer of secrets').
card_uid('stealer of secrets'/'RTR', 'RTR:Stealer of Secrets:stealer of secrets').
card_rarity('stealer of secrets'/'RTR', 'Common').
card_artist('stealer of secrets'/'RTR', 'Michael C. Hayes').
card_number('stealer of secrets'/'RTR', '53').
card_flavor_text('stealer of secrets'/'RTR', 'The Dimir would hire her, if only they knew where she lived. The Azorius would condemn her, if only they knew her name.').
card_multiverse_id('stealer of secrets'/'RTR', '265413').

card_in_set('steam vents', 'RTR').
card_original_type('steam vents'/'RTR', 'Land — Island Mountain').
card_original_text('steam vents'/'RTR', '({T}: Add {U} or {R} to your mana pool.)\nAs Steam Vents enters the battlefield, you may pay 2 life. If you don\'t, Steam Vents enters the battlefield tapped.').
card_image_name('steam vents'/'RTR', 'steam vents').
card_uid('steam vents'/'RTR', 'RTR:Steam Vents:steam vents').
card_rarity('steam vents'/'RTR', 'Rare').
card_artist('steam vents'/'RTR', 'Yeong-Hao Han').
card_number('steam vents'/'RTR', '247').
card_flavor_text('steam vents'/'RTR', 'Crafted with genius, energized with madness.').
card_multiverse_id('steam vents'/'RTR', '253682').
card_watermark('steam vents'/'RTR', 'Izzet').

card_in_set('stonefare crocodile', 'RTR').
card_original_type('stonefare crocodile'/'RTR', 'Creature — Crocodile').
card_original_text('stonefare crocodile'/'RTR', '{2}{B}: Stonefare Crocodile gains lifelink until end of turn. (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('stonefare crocodile', 'RTR').
card_image_name('stonefare crocodile'/'RTR', 'stonefare crocodile').
card_uid('stonefare crocodile'/'RTR', 'RTR:Stonefare Crocodile:stonefare crocodile').
card_rarity('stonefare crocodile'/'RTR', 'Common').
card_artist('stonefare crocodile'/'RTR', 'Tomasz Jedruszek').
card_number('stonefare crocodile'/'RTR', '136').
card_flavor_text('stonefare crocodile'/'RTR', 'The Izzet\'s plans to exploit the undercity ran into a few stubborn obstacles.').
card_multiverse_id('stonefare crocodile'/'RTR', '265398').
card_watermark('stonefare crocodile'/'RTR', 'Golgari').

card_in_set('street spasm', 'RTR').
card_original_type('street spasm'/'RTR', 'Instant').
card_original_text('street spasm'/'RTR', 'Street Spasm deals X damage to target creature without flying you don\'t control.\nOverload {X}{X}{R}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_image_name('street spasm'/'RTR', 'street spasm').
card_uid('street spasm'/'RTR', 'RTR:Street Spasm:street spasm').
card_rarity('street spasm'/'RTR', 'Uncommon').
card_artist('street spasm'/'RTR', 'Raymond Swanland').
card_number('street spasm'/'RTR', '106').
card_multiverse_id('street spasm'/'RTR', '265384').
card_watermark('street spasm'/'RTR', 'Izzet').

card_in_set('street sweeper', 'RTR').
card_original_type('street sweeper'/'RTR', 'Artifact Creature — Construct').
card_original_text('street sweeper'/'RTR', 'Whenever Street Sweeper attacks, destroy all Auras attached to target land.').
card_first_print('street sweeper', 'RTR').
card_image_name('street sweeper'/'RTR', 'street sweeper').
card_uid('street sweeper'/'RTR', 'RTR:Street Sweeper:street sweeper').
card_rarity('street sweeper'/'RTR', 'Uncommon').
card_artist('street sweeper'/'RTR', 'Izzy').
card_number('street sweeper'/'RTR', '234').
card_flavor_text('street sweeper'/'RTR', 'The sweepers roam endlessly, scrubbing away the refuse—and anything else too slow or too clumsy to get out of their way.').
card_multiverse_id('street sweeper'/'RTR', '270967').

card_in_set('sundering growth', 'RTR').
card_original_type('sundering growth'/'RTR', 'Instant').
card_original_text('sundering growth'/'RTR', 'Destroy target artifact or enchantment, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('sundering growth', 'RTR').
card_image_name('sundering growth'/'RTR', 'sundering growth').
card_uid('sundering growth'/'RTR', 'RTR:Sundering Growth:sundering growth').
card_rarity('sundering growth'/'RTR', 'Common').
card_artist('sundering growth'/'RTR', 'David Palumbo').
card_number('sundering growth'/'RTR', '223').
card_flavor_text('sundering growth'/'RTR', '\"One day every pillar will be a tree and every hall a glade.\"\n—Trostani').
card_multiverse_id('sundering growth'/'RTR', '265415').
card_watermark('sundering growth'/'RTR', 'Selesnya').

card_in_set('sunspire griffin', 'RTR').
card_original_type('sunspire griffin'/'RTR', 'Creature — Griffin').
card_original_text('sunspire griffin'/'RTR', 'Flying').
card_first_print('sunspire griffin', 'RTR').
card_image_name('sunspire griffin'/'RTR', 'sunspire griffin').
card_uid('sunspire griffin'/'RTR', 'RTR:Sunspire Griffin:sunspire griffin').
card_rarity('sunspire griffin'/'RTR', 'Common').
card_artist('sunspire griffin'/'RTR', 'Johannes Voss').
card_number('sunspire griffin'/'RTR', '25').
card_flavor_text('sunspire griffin'/'RTR', '\"For each griffin wounded by an arrow, there\'s a corpse with a bow nearby.\"\n—Pel Javya, Wojek investigator').
card_multiverse_id('sunspire griffin'/'RTR', '253554').

card_in_set('supreme verdict', 'RTR').
card_original_type('supreme verdict'/'RTR', 'Sorcery').
card_original_text('supreme verdict'/'RTR', 'Supreme Verdict can\'t be countered.\nDestroy all creatures.').
card_image_name('supreme verdict'/'RTR', 'supreme verdict').
card_uid('supreme verdict'/'RTR', 'RTR:Supreme Verdict:supreme verdict').
card_rarity('supreme verdict'/'RTR', 'Rare').
card_artist('supreme verdict'/'RTR', 'Sam Burley').
card_number('supreme verdict'/'RTR', '201').
card_flavor_text('supreme verdict'/'RTR', 'Leonos had no second thoughts about the abolishment edict. He\'d left skyrunes warning of the eviction, even though it was cloudy.').
card_multiverse_id('supreme verdict'/'RTR', '253512').
card_watermark('supreme verdict'/'RTR', 'Azorius').

card_in_set('survey the wreckage', 'RTR').
card_original_type('survey the wreckage'/'RTR', 'Sorcery').
card_original_text('survey the wreckage'/'RTR', 'Destroy target land. Put a 1/1 red Goblin creature token onto the battlefield.').
card_first_print('survey the wreckage', 'RTR').
card_image_name('survey the wreckage'/'RTR', 'survey the wreckage').
card_uid('survey the wreckage'/'RTR', 'RTR:Survey the Wreckage:survey the wreckage').
card_rarity('survey the wreckage'/'RTR', 'Common').
card_artist('survey the wreckage'/'RTR', 'Warren Mahy').
card_number('survey the wreckage'/'RTR', '107').
card_flavor_text('survey the wreckage'/'RTR', 'Goblins and architects seldom get along.').
card_multiverse_id('survey the wreckage'/'RTR', '270804').

card_in_set('swamp', 'RTR').
card_original_type('swamp'/'RTR', 'Basic Land — Swamp').
card_original_text('swamp'/'RTR', 'B').
card_image_name('swamp'/'RTR', 'swamp1').
card_uid('swamp'/'RTR', 'RTR:Swamp:swamp1').
card_rarity('swamp'/'RTR', 'Basic Land').
card_artist('swamp'/'RTR', 'John Avon').
card_number('swamp'/'RTR', '260').
card_multiverse_id('swamp'/'RTR', '289317').

card_in_set('swamp', 'RTR').
card_original_type('swamp'/'RTR', 'Basic Land — Swamp').
card_original_text('swamp'/'RTR', 'B').
card_image_name('swamp'/'RTR', 'swamp2').
card_uid('swamp'/'RTR', 'RTR:Swamp:swamp2').
card_rarity('swamp'/'RTR', 'Basic Land').
card_artist('swamp'/'RTR', 'Yeong-Hao Han').
card_number('swamp'/'RTR', '261').
card_multiverse_id('swamp'/'RTR', '289320').

card_in_set('swamp', 'RTR').
card_original_type('swamp'/'RTR', 'Basic Land — Swamp').
card_original_text('swamp'/'RTR', 'B').
card_image_name('swamp'/'RTR', 'swamp3').
card_uid('swamp'/'RTR', 'RTR:Swamp:swamp3').
card_rarity('swamp'/'RTR', 'Basic Land').
card_artist('swamp'/'RTR', 'Adam Paquette').
card_number('swamp'/'RTR', '262').
card_multiverse_id('swamp'/'RTR', '289318').

card_in_set('swamp', 'RTR').
card_original_type('swamp'/'RTR', 'Basic Land — Swamp').
card_original_text('swamp'/'RTR', 'B').
card_image_name('swamp'/'RTR', 'swamp4').
card_uid('swamp'/'RTR', 'RTR:Swamp:swamp4').
card_rarity('swamp'/'RTR', 'Basic Land').
card_artist('swamp'/'RTR', 'Richard Wright').
card_number('swamp'/'RTR', '263').
card_multiverse_id('swamp'/'RTR', '289319').

card_in_set('swamp', 'RTR').
card_original_type('swamp'/'RTR', 'Basic Land — Swamp').
card_original_text('swamp'/'RTR', 'B').
card_image_name('swamp'/'RTR', 'swamp5').
card_uid('swamp'/'RTR', 'RTR:Swamp:swamp5').
card_rarity('swamp'/'RTR', 'Basic Land').
card_artist('swamp'/'RTR', 'Richard Wright').
card_number('swamp'/'RTR', '264').
card_multiverse_id('swamp'/'RTR', '333722').

card_in_set('swift justice', 'RTR').
card_original_type('swift justice'/'RTR', 'Instant').
card_original_text('swift justice'/'RTR', 'Until end of turn, target creature gets +1/+0 and gains first strike and lifelink.').
card_first_print('swift justice', 'RTR').
card_image_name('swift justice'/'RTR', 'swift justice').
card_uid('swift justice'/'RTR', 'RTR:Swift Justice:swift justice').
card_rarity('swift justice'/'RTR', 'Common').
card_artist('swift justice'/'RTR', 'Karl Kopinski').
card_number('swift justice'/'RTR', '26').
card_flavor_text('swift justice'/'RTR', '\"Having conviction is more important than being righteous.\"\n—Aurelia').
card_multiverse_id('swift justice'/'RTR', '265416').

card_in_set('syncopate', 'RTR').
card_original_type('syncopate'/'RTR', 'Instant').
card_original_text('syncopate'/'RTR', 'Counter target spell unless its controller pays {X}. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_image_name('syncopate'/'RTR', 'syncopate').
card_uid('syncopate'/'RTR', 'RTR:Syncopate:syncopate').
card_rarity('syncopate'/'RTR', 'Uncommon').
card_artist('syncopate'/'RTR', 'Clint Cearley').
card_number('syncopate'/'RTR', '54').
card_flavor_text('syncopate'/'RTR', '\"Patience, mage. Killing you now would be too easy.\"').
card_multiverse_id('syncopate'/'RTR', '270369').

card_in_set('tablet of the guilds', 'RTR').
card_original_type('tablet of the guilds'/'RTR', 'Artifact').
card_original_text('tablet of the guilds'/'RTR', 'As Tablet of the Guilds enters the battlefield, choose two colors.\nWhenever you cast a spell, if it\'s at least one of the chosen colors, you gain 1 life for each of the chosen colors it is.').
card_first_print('tablet of the guilds', 'RTR').
card_image_name('tablet of the guilds'/'RTR', 'tablet of the guilds').
card_uid('tablet of the guilds'/'RTR', 'RTR:Tablet of the Guilds:tablet of the guilds').
card_rarity('tablet of the guilds'/'RTR', 'Uncommon').
card_artist('tablet of the guilds'/'RTR', 'Nic Klein').
card_number('tablet of the guilds'/'RTR', '235').
card_flavor_text('tablet of the guilds'/'RTR', 'It is rumored the Azorius paid an exorbitant sum to ensure its symbol would appear at the top.').
card_multiverse_id('tablet of the guilds'/'RTR', '270355').

card_in_set('tavern swindler', 'RTR').
card_original_type('tavern swindler'/'RTR', 'Creature — Human Rogue').
card_original_text('tavern swindler'/'RTR', '{T}, Pay 3 life: Flip a coin. If you win the flip, you gain 6 life.').
card_first_print('tavern swindler', 'RTR').
card_image_name('tavern swindler'/'RTR', 'tavern swindler').
card_uid('tavern swindler'/'RTR', 'RTR:Tavern Swindler:tavern swindler').
card_rarity('tavern swindler'/'RTR', 'Uncommon').
card_artist('tavern swindler'/'RTR', 'Cynthia Sheppard').
card_number('tavern swindler'/'RTR', '79').
card_flavor_text('tavern swindler'/'RTR', 'Rakdos cultists are her best customers. They never flinch at pain and are seldom good at math.').
card_multiverse_id('tavern swindler'/'RTR', '270353').

card_in_set('teleportal', 'RTR').
card_original_type('teleportal'/'RTR', 'Sorcery').
card_original_text('teleportal'/'RTR', 'Target creature you control gets +1/+0 until end of turn and is unblockable this turn.\nOverload {3}{U}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('teleportal', 'RTR').
card_image_name('teleportal'/'RTR', 'teleportal').
card_uid('teleportal'/'RTR', 'RTR:Teleportal:teleportal').
card_rarity('teleportal'/'RTR', 'Uncommon').
card_artist('teleportal'/'RTR', 'Scott M. Fischer').
card_number('teleportal'/'RTR', '202').
card_multiverse_id('teleportal'/'RTR', '253562').
card_watermark('teleportal'/'RTR', 'Izzet').

card_in_set('temple garden', 'RTR').
card_original_type('temple garden'/'RTR', 'Land — Forest Plains').
card_original_text('temple garden'/'RTR', '({T}: Add {G} or {W} to your mana pool.)\nAs Temple Garden enters the battlefield, you may pay 2 life. If you don\'t, Temple Garden enters the battlefield tapped.').
card_image_name('temple garden'/'RTR', 'temple garden').
card_uid('temple garden'/'RTR', 'RTR:Temple Garden:temple garden').
card_rarity('temple garden'/'RTR', 'Rare').
card_artist('temple garden'/'RTR', 'Volkan Baga').
card_number('temple garden'/'RTR', '248').
card_flavor_text('temple garden'/'RTR', 'In the gardens of the Conclave, order and beauty are the roots of power.').
card_multiverse_id('temple garden'/'RTR', '253681').
card_watermark('temple garden'/'RTR', 'Selesnya').

card_in_set('tenement crasher', 'RTR').
card_original_type('tenement crasher'/'RTR', 'Creature — Beast').
card_original_text('tenement crasher'/'RTR', 'Haste').
card_first_print('tenement crasher', 'RTR').
card_image_name('tenement crasher'/'RTR', 'tenement crasher').
card_uid('tenement crasher'/'RTR', 'RTR:Tenement Crasher:tenement crasher').
card_rarity('tenement crasher'/'RTR', 'Common').
card_artist('tenement crasher'/'RTR', 'Warren Mahy').
card_number('tenement crasher'/'RTR', '108').
card_flavor_text('tenement crasher'/'RTR', 'Nothing was going to stop it—not the narrow alleys, not the Boros garrison, and certainly not the four-story Orzhov cathedral.').
card_multiverse_id('tenement crasher'/'RTR', '253649').

card_in_set('terrus wurm', 'RTR').
card_original_type('terrus wurm'/'RTR', 'Creature — Zombie Wurm').
card_original_text('terrus wurm'/'RTR', 'Scavenge {6}{B} ({6}{B}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('terrus wurm', 'RTR').
card_image_name('terrus wurm'/'RTR', 'terrus wurm').
card_uid('terrus wurm'/'RTR', 'RTR:Terrus Wurm:terrus wurm').
card_rarity('terrus wurm'/'RTR', 'Common').
card_artist('terrus wurm'/'RTR', 'Cliff Childs').
card_number('terrus wurm'/'RTR', '80').
card_flavor_text('terrus wurm'/'RTR', '\"We follow it through its tunnels, and where it dies, we farm.\"\n—Mihas, spore druid').
card_multiverse_id('terrus wurm'/'RTR', '289223').
card_watermark('terrus wurm'/'RTR', 'Golgari').

card_in_set('thoughtflare', 'RTR').
card_original_type('thoughtflare'/'RTR', 'Instant').
card_original_text('thoughtflare'/'RTR', 'Draw four cards, then discard two cards.').
card_first_print('thoughtflare', 'RTR').
card_image_name('thoughtflare'/'RTR', 'thoughtflare').
card_uid('thoughtflare'/'RTR', 'RTR:Thoughtflare:thoughtflare').
card_rarity('thoughtflare'/'RTR', 'Uncommon').
card_artist('thoughtflare'/'RTR', 'David Rapoza').
card_number('thoughtflare'/'RTR', '203').
card_flavor_text('thoughtflare'/'RTR', '\"If this is thinking, I don\'t know what I was doing before.\"').
card_multiverse_id('thoughtflare'/'RTR', '253551').
card_watermark('thoughtflare'/'RTR', 'Izzet').

card_in_set('thrill-kill assassin', 'RTR').
card_original_type('thrill-kill assassin'/'RTR', 'Creature — Human Assassin').
card_original_text('thrill-kill assassin'/'RTR', 'Deathtouch\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_first_print('thrill-kill assassin', 'RTR').
card_image_name('thrill-kill assassin'/'RTR', 'thrill-kill assassin').
card_uid('thrill-kill assassin'/'RTR', 'RTR:Thrill-Kill Assassin:thrill-kill assassin').
card_rarity('thrill-kill assassin'/'RTR', 'Uncommon').
card_artist('thrill-kill assassin'/'RTR', 'Tyler Jacobson').
card_number('thrill-kill assassin'/'RTR', '81').
card_flavor_text('thrill-kill assassin'/'RTR', 'As the bounty on Massacre Girl rose, so did the number of imitators.').
card_multiverse_id('thrill-kill assassin'/'RTR', '265371').
card_watermark('thrill-kill assassin'/'RTR', 'Rakdos').

card_in_set('tower drake', 'RTR').
card_original_type('tower drake'/'RTR', 'Creature — Drake').
card_original_text('tower drake'/'RTR', 'Flying\n{W}: Tower Drake gets +0/+1 until end of turn.').
card_image_name('tower drake'/'RTR', 'tower drake').
card_uid('tower drake'/'RTR', 'RTR:Tower Drake:tower drake').
card_rarity('tower drake'/'RTR', 'Common').
card_artist('tower drake'/'RTR', 'Ryan Barger').
card_number('tower drake'/'RTR', '55').
card_flavor_text('tower drake'/'RTR', 'The first motion put forth in the pristine Jelenn Column was for severe restrictions on the airspace around New Prahv.').
card_multiverse_id('tower drake'/'RTR', '265400').
card_watermark('tower drake'/'RTR', 'Azorius').

card_in_set('towering indrik', 'RTR').
card_original_type('towering indrik'/'RTR', 'Creature — Beast').
card_original_text('towering indrik'/'RTR', 'Reach (This creature can block creatures with flying.)').
card_first_print('towering indrik', 'RTR').
card_image_name('towering indrik'/'RTR', 'towering indrik').
card_uid('towering indrik'/'RTR', 'RTR:Towering Indrik:towering indrik').
card_rarity('towering indrik'/'RTR', 'Common').
card_artist('towering indrik'/'RTR', 'Lars Grant-West').
card_number('towering indrik'/'RTR', '137').
card_flavor_text('towering indrik'/'RTR', 'It chases its airborne prey relentlessly, heedless to what it pulverizes beneath its hooves.').
card_multiverse_id('towering indrik'/'RTR', '253608').

card_in_set('trained caracal', 'RTR').
card_original_type('trained caracal'/'RTR', 'Creature — Cat').
card_original_text('trained caracal'/'RTR', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('trained caracal', 'RTR').
card_image_name('trained caracal'/'RTR', 'trained caracal').
card_uid('trained caracal'/'RTR', 'RTR:Trained Caracal:trained caracal').
card_rarity('trained caracal'/'RTR', 'Common').
card_artist('trained caracal'/'RTR', 'James Ryman').
card_number('trained caracal'/'RTR', '27').
card_flavor_text('trained caracal'/'RTR', 'Some Ravnicans consider carrying a sword to be beneath them, preferring instead a tooth-and-claw escort.').
card_multiverse_id('trained caracal'/'RTR', '253615').

card_in_set('traitorous instinct', 'RTR').
card_original_type('traitorous instinct'/'RTR', 'Sorcery').
card_original_text('traitorous instinct'/'RTR', 'Gain control of target creature until end of turn. Untap that creature. Until end of turn, it gets +2/+0 and gains haste.').
card_image_name('traitorous instinct'/'RTR', 'traitorous instinct').
card_uid('traitorous instinct'/'RTR', 'RTR:Traitorous Instinct:traitorous instinct').
card_rarity('traitorous instinct'/'RTR', 'Common').
card_artist('traitorous instinct'/'RTR', 'Daarken').
card_number('traitorous instinct'/'RTR', '109').
card_flavor_text('traitorous instinct'/'RTR', 'Joining the Worldsoul took years of study and meditation. Severing that connection took the snap of Rakdos\'s fingers.').
card_multiverse_id('traitorous instinct'/'RTR', '289228').

card_in_set('transguild promenade', 'RTR').
card_original_type('transguild promenade'/'RTR', 'Land').
card_original_text('transguild promenade'/'RTR', 'Transguild Promenade enters the battlefield tapped.\nWhen Transguild Promenade enters the battlefield, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('transguild promenade', 'RTR').
card_image_name('transguild promenade'/'RTR', 'transguild promenade').
card_uid('transguild promenade'/'RTR', 'RTR:Transguild Promenade:transguild promenade').
card_rarity('transguild promenade'/'RTR', 'Common').
card_artist('transguild promenade'/'RTR', 'Noah Bradley').
card_number('transguild promenade'/'RTR', '249').
card_multiverse_id('transguild promenade'/'RTR', '253550').

card_in_set('treasured find', 'RTR').
card_original_type('treasured find'/'RTR', 'Sorcery').
card_original_text('treasured find'/'RTR', 'Return target card from your graveyard to your hand. Exile Treasured Find.').
card_first_print('treasured find', 'RTR').
card_image_name('treasured find'/'RTR', 'treasured find').
card_uid('treasured find'/'RTR', 'RTR:Treasured Find:treasured find').
card_rarity('treasured find'/'RTR', 'Uncommon').
card_artist('treasured find'/'RTR', 'Jason Chan').
card_number('treasured find'/'RTR', '204').
card_flavor_text('treasured find'/'RTR', 'Gorgons crave beautiful things: gems, exquisite amulets, the alabaster corpses of the petrified dead . . .').
card_multiverse_id('treasured find'/'RTR', '270375').
card_watermark('treasured find'/'RTR', 'Golgari').

card_in_set('trestle troll', 'RTR').
card_original_type('trestle troll'/'RTR', 'Creature — Troll').
card_original_text('trestle troll'/'RTR', 'Defender\nReach (This creature can block creatures with flying.)\n{1}{B}{G}: Regenerate Trestle Troll.').
card_first_print('trestle troll', 'RTR').
card_image_name('trestle troll'/'RTR', 'trestle troll').
card_uid('trestle troll'/'RTR', 'RTR:Trestle Troll:trestle troll').
card_rarity('trestle troll'/'RTR', 'Common').
card_artist('trestle troll'/'RTR', 'Peter Mohrbacher').
card_number('trestle troll'/'RTR', '205').
card_flavor_text('trestle troll'/'RTR', 'Unwelcome in Golgari colonies, he found his own dark place from which to represent the Swarm.').
card_multiverse_id('trestle troll'/'RTR', '289220').
card_watermark('trestle troll'/'RTR', 'Golgari').

card_in_set('trostani\'s judgment', 'RTR').
card_original_type('trostani\'s judgment'/'RTR', 'Instant').
card_original_text('trostani\'s judgment'/'RTR', 'Exile target creature, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('trostani\'s judgment', 'RTR').
card_image_name('trostani\'s judgment'/'RTR', 'trostani\'s judgment').
card_uid('trostani\'s judgment'/'RTR', 'RTR:Trostani\'s Judgment:trostani\'s judgment').
card_rarity('trostani\'s judgment'/'RTR', 'Common').
card_artist('trostani\'s judgment'/'RTR', 'Christopher Moeller').
card_number('trostani\'s judgment'/'RTR', '28').
card_flavor_text('trostani\'s judgment'/'RTR', '\"Some cannot be saved as they are, but all can serve the Worldsoul.\"\n—Trostani').
card_multiverse_id('trostani\'s judgment'/'RTR', '289225').
card_watermark('trostani\'s judgment'/'RTR', 'Selesnya').

card_in_set('trostani, selesnya\'s voice', 'RTR').
card_original_type('trostani, selesnya\'s voice'/'RTR', 'Legendary Creature — Dryad').
card_original_text('trostani, selesnya\'s voice'/'RTR', 'Whenever another creature enters the battlefield under your control, you gain life equal to that creature\'s toughness.\n{1}{G}{W}, {T}: Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('trostani, selesnya\'s voice', 'RTR').
card_image_name('trostani, selesnya\'s voice'/'RTR', 'trostani, selesnya\'s voice').
card_uid('trostani, selesnya\'s voice'/'RTR', 'RTR:Trostani, Selesnya\'s Voice:trostani, selesnya\'s voice').
card_rarity('trostani, selesnya\'s voice'/'RTR', 'Mythic Rare').
card_artist('trostani, selesnya\'s voice'/'RTR', 'Chippy').
card_number('trostani, selesnya\'s voice'/'RTR', '206').
card_multiverse_id('trostani, selesnya\'s voice'/'RTR', '253641').
card_watermark('trostani, selesnya\'s voice'/'RTR', 'Selesnya').

card_in_set('ultimate price', 'RTR').
card_original_type('ultimate price'/'RTR', 'Instant').
card_original_text('ultimate price'/'RTR', 'Destroy target monocolored creature.').
card_first_print('ultimate price', 'RTR').
card_image_name('ultimate price'/'RTR', 'ultimate price').
card_uid('ultimate price'/'RTR', 'RTR:Ultimate Price:ultimate price').
card_rarity('ultimate price'/'RTR', 'Uncommon').
card_artist('ultimate price'/'RTR', 'Karl Kopinski').
card_number('ultimate price'/'RTR', '82').
card_flavor_text('ultimate price'/'RTR', '\"Let him be an example to others who would default on their debts to us.\"\n—\"The Cozen,\" Orzhov assassin').
card_multiverse_id('ultimate price'/'RTR', '253538').

card_in_set('underworld connections', 'RTR').
card_original_type('underworld connections'/'RTR', 'Enchantment — Aura').
card_original_text('underworld connections'/'RTR', 'Enchant land\nEnchanted land has \"{T}, Pay 1 life: Draw a card.\"').
card_first_print('underworld connections', 'RTR').
card_image_name('underworld connections'/'RTR', 'underworld connections').
card_uid('underworld connections'/'RTR', 'RTR:Underworld Connections:underworld connections').
card_rarity('underworld connections'/'RTR', 'Rare').
card_artist('underworld connections'/'RTR', 'Yeong-Hao Han').
card_number('underworld connections'/'RTR', '83').
card_flavor_text('underworld connections'/'RTR', 'If you\'re looking for it, it\'s available. The question is how much you\'re willing to pay.').
card_multiverse_id('underworld connections'/'RTR', '253507').

card_in_set('urban burgeoning', 'RTR').
card_original_type('urban burgeoning'/'RTR', 'Enchantment — Aura').
card_original_text('urban burgeoning'/'RTR', 'Enchant land\nEnchanted land has \"Untap this land during each other player\'s untap step.\"').
card_first_print('urban burgeoning', 'RTR').
card_image_name('urban burgeoning'/'RTR', 'urban burgeoning').
card_uid('urban burgeoning'/'RTR', 'RTR:Urban Burgeoning:urban burgeoning').
card_rarity('urban burgeoning'/'RTR', 'Common').
card_artist('urban burgeoning'/'RTR', 'Nic Klein').
card_number('urban burgeoning'/'RTR', '138').
card_flavor_text('urban burgeoning'/'RTR', 'The ruins of Old Prahv became a wildspace, although all birdsong was silenced by lingering Azorius magic.').
card_multiverse_id('urban burgeoning'/'RTR', '270370').

card_in_set('utvara hellkite', 'RTR').
card_original_type('utvara hellkite'/'RTR', 'Creature — Dragon').
card_original_text('utvara hellkite'/'RTR', 'Flying\nWhenever a Dragon you control attacks, put a 6/6 red Dragon creature token with flying onto the battlefield.').
card_first_print('utvara hellkite', 'RTR').
card_image_name('utvara hellkite'/'RTR', 'utvara hellkite').
card_uid('utvara hellkite'/'RTR', 'RTR:Utvara Hellkite:utvara hellkite').
card_rarity('utvara hellkite'/'RTR', 'Mythic Rare').
card_artist('utvara hellkite'/'RTR', 'Mark Zug').
card_number('utvara hellkite'/'RTR', '110').
card_flavor_text('utvara hellkite'/'RTR', 'The fear of dragons is as old and as powerful as the fear of death itself.').
card_multiverse_id('utvara hellkite'/'RTR', '270372').

card_in_set('vandalblast', 'RTR').
card_original_type('vandalblast'/'RTR', 'Sorcery').
card_original_text('vandalblast'/'RTR', 'Destroy target artifact you don\'t control.\nOverload {4}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_first_print('vandalblast', 'RTR').
card_image_name('vandalblast'/'RTR', 'vandalblast').
card_uid('vandalblast'/'RTR', 'RTR:Vandalblast:vandalblast').
card_rarity('vandalblast'/'RTR', 'Uncommon').
card_artist('vandalblast'/'RTR', 'Seb McKinnon').
card_number('vandalblast'/'RTR', '111').
card_flavor_text('vandalblast'/'RTR', 'Beauty is in the eye of the exploder.').
card_multiverse_id('vandalblast'/'RTR', '253560').
card_watermark('vandalblast'/'RTR', 'Izzet').

card_in_set('vassal soul', 'RTR').
card_original_type('vassal soul'/'RTR', 'Creature — Spirit').
card_original_text('vassal soul'/'RTR', 'Flying').
card_first_print('vassal soul', 'RTR').
card_image_name('vassal soul'/'RTR', 'vassal soul').
card_uid('vassal soul'/'RTR', 'RTR:Vassal Soul:vassal soul').
card_rarity('vassal soul'/'RTR', 'Common').
card_artist('vassal soul'/'RTR', 'Dan Scott').
card_number('vassal soul'/'RTR', '224').
card_flavor_text('vassal soul'/'RTR', 'For the Azorius, the opportunity to serve the law is too great an honor for death to interrupt.').
card_multiverse_id('vassal soul'/'RTR', '253521').
card_watermark('vassal soul'/'RTR', 'Azorius').

card_in_set('viashino racketeer', 'RTR').
card_original_type('viashino racketeer'/'RTR', 'Creature — Viashino Rogue').
card_original_text('viashino racketeer'/'RTR', 'When Viashino Racketeer enters the battlefield, you may discard a card. If you do, draw a card.').
card_first_print('viashino racketeer', 'RTR').
card_image_name('viashino racketeer'/'RTR', 'viashino racketeer').
card_uid('viashino racketeer'/'RTR', 'RTR:Viashino Racketeer:viashino racketeer').
card_rarity('viashino racketeer'/'RTR', 'Common').
card_artist('viashino racketeer'/'RTR', 'Slawomir Maniak').
card_number('viashino racketeer'/'RTR', '112').
card_flavor_text('viashino racketeer'/'RTR', '\"You may call me a fool, selling this stuff on Orzhov turf. Well, you need to be a little reckless to survive.\"').
card_multiverse_id('viashino racketeer'/'RTR', '265379').

card_in_set('vitu-ghazi guildmage', 'RTR').
card_original_type('vitu-ghazi guildmage'/'RTR', 'Creature — Dryad Shaman').
card_original_text('vitu-ghazi guildmage'/'RTR', '{4}{G}{W}: Put a 3/3 green Centaur creature token onto the battlefield.\n{2}{G}{W}: Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('vitu-ghazi guildmage', 'RTR').
card_image_name('vitu-ghazi guildmage'/'RTR', 'vitu-ghazi guildmage').
card_uid('vitu-ghazi guildmage'/'RTR', 'RTR:Vitu-Ghazi Guildmage:vitu-ghazi guildmage').
card_rarity('vitu-ghazi guildmage'/'RTR', 'Uncommon').
card_artist('vitu-ghazi guildmage'/'RTR', 'Jason Chan').
card_number('vitu-ghazi guildmage'/'RTR', '207').
card_flavor_text('vitu-ghazi guildmage'/'RTR', '\"Preachers of freedom forget that conformity can be beautiful.\"').
card_multiverse_id('vitu-ghazi guildmage'/'RTR', '270346').
card_watermark('vitu-ghazi guildmage'/'RTR', 'Selesnya').

card_in_set('voidwielder', 'RTR').
card_original_type('voidwielder'/'RTR', 'Creature — Human Wizard').
card_original_text('voidwielder'/'RTR', 'When Voidwielder enters the battlefield, you may return target creature to its owner\'s hand.').
card_first_print('voidwielder', 'RTR').
card_image_name('voidwielder'/'RTR', 'voidwielder').
card_uid('voidwielder'/'RTR', 'RTR:Voidwielder:voidwielder').
card_rarity('voidwielder'/'RTR', 'Common').
card_artist('voidwielder'/'RTR', 'Chase Stone').
card_number('voidwielder'/'RTR', '56').
card_flavor_text('voidwielder'/'RTR', '\"He makes up his own laws, and that\'s dangerous to all who love peace and prosperity. Kill him on sight.\"\n—Mirela, Azorius hussar').
card_multiverse_id('voidwielder'/'RTR', '265412').

card_in_set('volatile rig', 'RTR').
card_original_type('volatile rig'/'RTR', 'Artifact Creature — Construct').
card_original_text('volatile rig'/'RTR', 'Trample\nVolatile Rig attacks each turn if able.\nWhenever Volatile Rig is dealt damage, flip a coin. If you lose the flip, sacrifice Volatile Rig.\nWhen Volatile Rig dies, flip a coin. If you lose the flip, it deals 4 damage to each creature and each player.').
card_first_print('volatile rig', 'RTR').
card_image_name('volatile rig'/'RTR', 'volatile rig').
card_uid('volatile rig'/'RTR', 'RTR:Volatile Rig:volatile rig').
card_rarity('volatile rig'/'RTR', 'Rare').
card_artist('volatile rig'/'RTR', 'Mathias Kollros').
card_number('volatile rig'/'RTR', '236').
card_multiverse_id('volatile rig'/'RTR', '270359').

card_in_set('vraska the unseen', 'RTR').
card_original_type('vraska the unseen'/'RTR', 'Planeswalker — Vraska').
card_original_text('vraska the unseen'/'RTR', '+1: Until your next turn, whenever a creature deals combat damage to Vraska the Unseen, destroy that creature.\n-3: Destroy target nonland permanent.\n-7: Put three 1/1 black Assassin creature tokens onto the battlefield with \"Whenever this creature deals combat damage to a player, that player loses the game.\"').
card_first_print('vraska the unseen', 'RTR').
card_image_name('vraska the unseen'/'RTR', 'vraska the unseen').
card_uid('vraska the unseen'/'RTR', 'RTR:Vraska the Unseen:vraska the unseen').
card_rarity('vraska the unseen'/'RTR', 'Mythic Rare').
card_artist('vraska the unseen'/'RTR', 'Aleksi Briclot').
card_number('vraska the unseen'/'RTR', '208').
card_multiverse_id('vraska the unseen'/'RTR', '290538').

card_in_set('wayfaring temple', 'RTR').
card_original_type('wayfaring temple'/'RTR', 'Creature — Elemental').
card_original_text('wayfaring temple'/'RTR', 'Wayfaring Temple\'s power and toughness are each equal to the number of creatures you control.\nWhenever Wayfaring Temple deals combat damage to a player, populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_first_print('wayfaring temple', 'RTR').
card_image_name('wayfaring temple'/'RTR', 'wayfaring temple').
card_uid('wayfaring temple'/'RTR', 'RTR:Wayfaring Temple:wayfaring temple').
card_rarity('wayfaring temple'/'RTR', 'Rare').
card_artist('wayfaring temple'/'RTR', 'Peter Mohrbacher').
card_number('wayfaring temple'/'RTR', '209').
card_multiverse_id('wayfaring temple'/'RTR', '253539').
card_watermark('wayfaring temple'/'RTR', 'Selesnya').

card_in_set('wild beastmaster', 'RTR').
card_original_type('wild beastmaster'/'RTR', 'Creature — Human Shaman').
card_original_text('wild beastmaster'/'RTR', 'Whenever Wild Beastmaster attacks, each other creature you control gets +X/+X until end of turn, where X is Wild Beastmaster\'s power.').
card_first_print('wild beastmaster', 'RTR').
card_image_name('wild beastmaster'/'RTR', 'wild beastmaster').
card_uid('wild beastmaster'/'RTR', 'RTR:Wild Beastmaster:wild beastmaster').
card_rarity('wild beastmaster'/'RTR', 'Rare').
card_artist('wild beastmaster'/'RTR', 'Kev Walker').
card_number('wild beastmaster'/'RTR', '139').
card_flavor_text('wild beastmaster'/'RTR', '\"When they covered the world in city, all they did was give themselves nowhere to run.\"').
card_multiverse_id('wild beastmaster'/'RTR', '270784').

card_in_set('worldspine wurm', 'RTR').
card_original_type('worldspine wurm'/'RTR', 'Creature — Wurm').
card_original_text('worldspine wurm'/'RTR', 'Trample\nWhen Worldspine Wurm dies, put three 5/5 green Wurm creature tokens with trample onto the battlefield.\nWhen Worldspine Wurm is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_first_print('worldspine wurm', 'RTR').
card_image_name('worldspine wurm'/'RTR', 'worldspine wurm').
card_uid('worldspine wurm'/'RTR', 'RTR:Worldspine Wurm:worldspine wurm').
card_rarity('worldspine wurm'/'RTR', 'Mythic Rare').
card_artist('worldspine wurm'/'RTR', 'Richard Wright').
card_number('worldspine wurm'/'RTR', '140').
card_multiverse_id('worldspine wurm'/'RTR', '253575').

card_in_set('zanikev locust', 'RTR').
card_original_type('zanikev locust'/'RTR', 'Creature — Insect').
card_original_text('zanikev locust'/'RTR', 'Flying\nScavenge {2}{B}{B} ({2}{B}{B}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_first_print('zanikev locust', 'RTR').
card_image_name('zanikev locust'/'RTR', 'zanikev locust').
card_uid('zanikev locust'/'RTR', 'RTR:Zanikev Locust:zanikev locust').
card_rarity('zanikev locust'/'RTR', 'Uncommon').
card_artist('zanikev locust'/'RTR', 'Cliff Childs').
card_number('zanikev locust'/'RTR', '84').
card_multiverse_id('zanikev locust'/'RTR', '289229').
card_watermark('zanikev locust'/'RTR', 'Golgari').
