% Tempest Remastered

set('TPR').
set_name('TPR', 'Tempest Remastered').
set_release_date('TPR', '2015-05-06').
set_border('TPR', 'black').
set_type('TPR', 'reprint').

card_in_set('acidic sliver', 'TPR').
card_original_type('acidic sliver'/'TPR', 'Creature — Sliver').
card_original_text('acidic sliver'/'TPR', 'All Slivers have \"{2}, Sacrifice this permanent: This permanent deals 2 damage to target creature or player.\"').
card_image_name('acidic sliver'/'TPR', 'acidic sliver').
card_uid('acidic sliver'/'TPR', 'TPR:Acidic Sliver:acidic sliver').
card_rarity('acidic sliver'/'TPR', 'Uncommon').
card_artist('acidic sliver'/'TPR', 'Jeff Miracola').
card_number('acidic sliver'/'TPR', '206').
card_flavor_text('acidic sliver'/'TPR', 'The first sliver burst against the cave wall, and others piled in behind to deepen the new tunnel.').
card_multiverse_id('acidic sliver'/'TPR', '397564').

card_in_set('aftershock', 'TPR').
card_original_type('aftershock'/'TPR', 'Sorcery').
card_original_text('aftershock'/'TPR', 'Destroy target artifact, creature, or land. Aftershock deals 3 damage to you.').
card_image_name('aftershock'/'TPR', 'aftershock').
card_uid('aftershock'/'TPR', 'TPR:Aftershock:aftershock').
card_rarity('aftershock'/'TPR', 'Common').
card_artist('aftershock'/'TPR', 'Hannibal King').
card_number('aftershock'/'TPR', '124').
card_flavor_text('aftershock'/'TPR', '\"Every act of destruction has a repercussion.\"\n—Karn, silver golem').
card_multiverse_id('aftershock'/'TPR', '397641').

card_in_set('aluren', 'TPR').
card_original_type('aluren'/'TPR', 'Enchantment').
card_original_text('aluren'/'TPR', 'Any player may play creature cards with converted mana cost 3 or less without paying their mana cost and as though they had flash.').
card_image_name('aluren'/'TPR', 'aluren').
card_uid('aluren'/'TPR', 'TPR:Aluren:aluren').
card_rarity('aluren'/'TPR', 'Rare').
card_artist('aluren'/'TPR', 'April Lee').
card_number('aluren'/'TPR', '165').
card_flavor_text('aluren'/'TPR', 'Squee bounced up and down. \"I sees a horsey, an\' a piggy, an\' a—\"\n\"If you don\'t shut up,\" hissed Mirri, \"you\'ll see a kidney and a spleeny.\"').
card_multiverse_id('aluren'/'TPR', '397389').

card_in_set('anarchist', 'TPR').
card_original_type('anarchist'/'TPR', 'Creature — Human Wizard').
card_original_text('anarchist'/'TPR', 'When Anarchist enters the battlefield, you may return target sorcery card from your graveyard to your hand.').
card_image_name('anarchist'/'TPR', 'anarchist').
card_uid('anarchist'/'TPR', 'TPR:Anarchist:anarchist').
card_rarity('anarchist'/'TPR', 'Common').
card_artist('anarchist'/'TPR', 'Brom').
card_number('anarchist'/'TPR', '125').
card_flavor_text('anarchist'/'TPR', '\"I\'ll take that. You won\'t need it when the revolution comes.\"').
card_multiverse_id('anarchist'/'TPR', '397533').

card_in_set('angelic blessing', 'TPR').
card_original_type('angelic blessing'/'TPR', 'Sorcery').
card_original_text('angelic blessing'/'TPR', 'Target creature gets +3/+3 and gains flying until end of turn.').
card_image_name('angelic blessing'/'TPR', 'angelic blessing').
card_uid('angelic blessing'/'TPR', 'TPR:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'TPR', 'Common').
card_artist('angelic blessing'/'TPR', 'Mark Zug').
card_number('angelic blessing'/'TPR', '1').
card_flavor_text('angelic blessing'/'TPR', 'Only the warrior who can admit mortal weakness will be bolstered by immortal strength.').
card_multiverse_id('angelic blessing'/'TPR', '397647').

card_in_set('angelic protector', 'TPR').
card_original_type('angelic protector'/'TPR', 'Creature — Angel').
card_original_text('angelic protector'/'TPR', 'Flying\nWhenever Angelic Protector becomes the target of a spell or ability, Angelic Protector gets +0/+3 until end of turn.').
card_image_name('angelic protector'/'TPR', 'angelic protector').
card_uid('angelic protector'/'TPR', 'TPR:Angelic Protector:angelic protector').
card_rarity('angelic protector'/'TPR', 'Uncommon').
card_artist('angelic protector'/'TPR', 'DiTerlizzi').
card_number('angelic protector'/'TPR', '2').
card_flavor_text('angelic protector'/'TPR', '\"My family sheltered in her light, the dark was content to wait.\"\n—Crovax').
card_multiverse_id('angelic protector'/'TPR', '397598').

card_in_set('anoint', 'TPR').
card_original_type('anoint'/'TPR', 'Instant').
card_original_text('anoint'/'TPR', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nPrevent the next 3 damage that would be dealt to target creature this turn.').
card_image_name('anoint'/'TPR', 'anoint').
card_uid('anoint'/'TPR', 'TPR:Anoint:anoint').
card_rarity('anoint'/'TPR', 'Common').
card_artist('anoint'/'TPR', 'Eric David Anderson').
card_number('anoint'/'TPR', '3').
card_multiverse_id('anoint'/'TPR', '397397').

card_in_set('armor sliver', 'TPR').
card_original_type('armor sliver'/'TPR', 'Creature — Sliver').
card_original_text('armor sliver'/'TPR', 'All Sliver creatures have \"{2}: This creature gets +0/+1 until end of turn.\"').
card_image_name('armor sliver'/'TPR', 'armor sliver').
card_uid('armor sliver'/'TPR', 'TPR:Armor Sliver:armor sliver').
card_rarity('armor sliver'/'TPR', 'Common').
card_artist('armor sliver'/'TPR', 'Scott Kirschner').
card_number('armor sliver'/'TPR', '4').
card_flavor_text('armor sliver'/'TPR', 'Hanna: \"We must learn how they protect each other.\"\nMirri: \"After they\'re done trying to kill us, all right?\"').
card_multiverse_id('armor sliver'/'TPR', '397515').

card_in_set('armored pegasus', 'TPR').
card_original_type('armored pegasus'/'TPR', 'Creature — Pegasus').
card_original_text('armored pegasus'/'TPR', 'Flying').
card_image_name('armored pegasus'/'TPR', 'armored pegasus').
card_uid('armored pegasus'/'TPR', 'TPR:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'TPR', 'Common').
card_artist('armored pegasus'/'TPR', 'Una Fricker').
card_number('armored pegasus'/'TPR', '5').
card_flavor_text('armored pegasus'/'TPR', '\"I always charge a little extra to take on a pegasus. They fly like eagles, kick like mules, and hide like hermits.\"\n—Tahli en-Dal, bounty hunter').
card_multiverse_id('armored pegasus'/'TPR', '397411').

card_in_set('avenging angel', 'TPR').
card_original_type('avenging angel'/'TPR', 'Creature — Angel').
card_original_text('avenging angel'/'TPR', 'Flying\nWhen Avenging Angel dies, you may put it on top of its owner\'s library.').
card_image_name('avenging angel'/'TPR', 'avenging angel').
card_uid('avenging angel'/'TPR', 'TPR:Avenging Angel:avenging angel').
card_rarity('avenging angel'/'TPR', 'Uncommon').
card_artist('avenging angel'/'TPR', 'Matthew D. Wilson').
card_number('avenging angel'/'TPR', '6').
card_multiverse_id('avenging angel'/'TPR', '397621').

card_in_set('bandage', 'TPR').
card_original_type('bandage'/'TPR', 'Instant').
card_original_text('bandage'/'TPR', 'Prevent the next 1 damage that would be dealt to target creature or player this turn.\nDraw a card.').
card_image_name('bandage'/'TPR', 'bandage').
card_uid('bandage'/'TPR', 'TPR:Bandage:bandage').
card_rarity('bandage'/'TPR', 'Common').
card_artist('bandage'/'TPR', 'Rebecca Guay').
card_number('bandage'/'TPR', '7').
card_flavor_text('bandage'/'TPR', 'Takara shook with guilt. She knew the last image her father had seen was her sword flashing toward him.').
card_multiverse_id('bandage'/'TPR', '397620').

card_in_set('barbed sliver', 'TPR').
card_original_type('barbed sliver'/'TPR', 'Creature — Sliver').
card_original_text('barbed sliver'/'TPR', 'All Sliver creatures have \"{2}: This creature gets +1/+0 until end of turn.\"').
card_image_name('barbed sliver'/'TPR', 'barbed sliver').
card_uid('barbed sliver'/'TPR', 'TPR:Barbed Sliver:barbed sliver').
card_rarity('barbed sliver'/'TPR', 'Common').
card_artist('barbed sliver'/'TPR', 'Scott Kirschner').
card_number('barbed sliver'/'TPR', '126').
card_flavor_text('barbed sliver'/'TPR', 'Spans of spines leapt from one sliver to the next, forming a deadly hedge around the Weatherlight.').
card_multiverse_id('barbed sliver'/'TPR', '397524').

card_in_set('bottle gnomes', 'TPR').
card_original_type('bottle gnomes'/'TPR', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'TPR', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_image_name('bottle gnomes'/'TPR', 'bottle gnomes').
card_uid('bottle gnomes'/'TPR', 'TPR:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'TPR', 'Common').
card_artist('bottle gnomes'/'TPR', 'Kaja Foglio').
card_number('bottle gnomes'/'TPR', '217').
card_flavor_text('bottle gnomes'/'TPR', '\"I am reminded of the fable of the silver egg. Its owner cracks it open to see what jewels it holds, only to discover a simple yellow yolk.\"\n—Karn, silver golem').
card_multiverse_id('bottle gnomes'/'TPR', '397557').

card_in_set('caldera lake', 'TPR').
card_original_type('caldera lake'/'TPR', 'Land').
card_original_text('caldera lake'/'TPR', 'Caldera Lake enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Caldera Lake deals 1 damage to you.').
card_image_name('caldera lake'/'TPR', 'caldera lake').
card_uid('caldera lake'/'TPR', 'TPR:Caldera Lake:caldera lake').
card_rarity('caldera lake'/'TPR', 'Uncommon').
card_artist('caldera lake'/'TPR', 'Allen Williams').
card_number('caldera lake'/'TPR', '235').
card_multiverse_id('caldera lake'/'TPR', '397490').

card_in_set('cannibalize', 'TPR').
card_original_type('cannibalize'/'TPR', 'Sorcery').
card_original_text('cannibalize'/'TPR', 'Choose two target creatures controlled by the same player. Exile one of those creatures and put two +1/+1 counters on the other.').
card_image_name('cannibalize'/'TPR', 'cannibalize').
card_uid('cannibalize'/'TPR', 'TPR:Cannibalize:cannibalize').
card_rarity('cannibalize'/'TPR', 'Uncommon').
card_artist('cannibalize'/'TPR', 'Robert Bliss').
card_number('cannibalize'/'TPR', '83').
card_flavor_text('cannibalize'/'TPR', '\"Mine.\"').
card_multiverse_id('cannibalize'/'TPR', '397631').

card_in_set('canopy spider', 'TPR').
card_original_type('canopy spider'/'TPR', 'Creature — Spider').
card_original_text('canopy spider'/'TPR', 'Reach (This creature can block creatures with flying.)').
card_image_name('canopy spider'/'TPR', 'canopy spider').
card_uid('canopy spider'/'TPR', 'TPR:Canopy Spider:canopy spider').
card_rarity('canopy spider'/'TPR', 'Common').
card_artist('canopy spider'/'TPR', 'Christopher Rush').
card_number('canopy spider'/'TPR', '166').
card_flavor_text('canopy spider'/'TPR', '\"We know our place in the cycle of life—and it is not as flies.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('canopy spider'/'TPR', '397437').

card_in_set('canyon wildcat', 'TPR').
card_original_type('canyon wildcat'/'TPR', 'Creature — Cat').
card_original_text('canyon wildcat'/'TPR', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_image_name('canyon wildcat'/'TPR', 'canyon wildcat').
card_uid('canyon wildcat'/'TPR', 'TPR:Canyon Wildcat:canyon wildcat').
card_rarity('canyon wildcat'/'TPR', 'Common').
card_artist('canyon wildcat'/'TPR', 'Gary Leach').
card_number('canyon wildcat'/'TPR', '127').
card_flavor_text('canyon wildcat'/'TPR', '\"Relative of yours?\" Ertai teased. Mirri simply sneered.').
card_multiverse_id('canyon wildcat'/'TPR', '397566').

card_in_set('capsize', 'TPR').
card_original_type('capsize'/'TPR', 'Instant').
card_original_text('capsize'/'TPR', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nReturn target permanent to its owner\'s hand.').
card_image_name('capsize'/'TPR', 'capsize').
card_uid('capsize'/'TPR', 'TPR:Capsize:capsize').
card_rarity('capsize'/'TPR', 'Uncommon').
card_artist('capsize'/'TPR', 'Tom Wänerstrand').
card_number('capsize'/'TPR', '42').
card_multiverse_id('capsize'/'TPR', '397492').

card_in_set('carnassid', 'TPR').
card_original_type('carnassid'/'TPR', 'Creature — Beast').
card_original_text('carnassid'/'TPR', 'Trample\n{1}{G}: Regenerate Carnassid.').
card_image_name('carnassid'/'TPR', 'carnassid').
card_uid('carnassid'/'TPR', 'TPR:Carnassid:carnassid').
card_rarity('carnassid'/'TPR', 'Uncommon').
card_artist('carnassid'/'TPR', 'Brom').
card_number('carnassid'/'TPR', '167').
card_flavor_text('carnassid'/'TPR', 'The hunter would never forget the time or place where he first met the carnassid, for they buried him there.').
card_multiverse_id('carnassid'/'TPR', '397506').

card_in_set('carnophage', 'TPR').
card_original_type('carnophage'/'TPR', 'Creature — Zombie').
card_original_text('carnophage'/'TPR', 'At the beginning of your upkeep, tap Carnophage unless you pay 1 life.').
card_image_name('carnophage'/'TPR', 'carnophage').
card_uid('carnophage'/'TPR', 'TPR:Carnophage:carnophage').
card_rarity('carnophage'/'TPR', 'Common').
card_artist('carnophage'/'TPR', 'Pete Venters').
card_number('carnophage'/'TPR', '84').
card_flavor_text('carnophage'/'TPR', 'Eating is all it knows.').
card_multiverse_id('carnophage'/'TPR', '397570').

card_in_set('cataclysm', 'TPR').
card_original_type('cataclysm'/'TPR', 'Sorcery').
card_original_text('cataclysm'/'TPR', 'Each player chooses from among the permanents he or she controls an artifact, a creature, an enchantment, and a land, then sacrifices the rest.').
card_image_name('cataclysm'/'TPR', 'cataclysm').
card_uid('cataclysm'/'TPR', 'TPR:Cataclysm:cataclysm').
card_rarity('cataclysm'/'TPR', 'Mythic Rare').
card_artist('cataclysm'/'TPR', 'Jim Nelson').
card_number('cataclysm'/'TPR', '8').
card_flavor_text('cataclysm'/'TPR', 'The Weatherlight dragged the Predator behind it, the cradle hauling the casket.').
card_multiverse_id('cataclysm'/'TPR', '397399').

card_in_set('charging paladin', 'TPR').
card_original_type('charging paladin'/'TPR', 'Creature — Human Knight').
card_original_text('charging paladin'/'TPR', 'Whenever Charging Paladin attacks, it gets +0/+3 until end of turn.').
card_image_name('charging paladin'/'TPR', 'charging paladin').
card_uid('charging paladin'/'TPR', 'TPR:Charging Paladin:charging paladin').
card_rarity('charging paladin'/'TPR', 'Common').
card_artist('charging paladin'/'TPR', 'Ciruelo').
card_number('charging paladin'/'TPR', '9').
card_flavor_text('charging paladin'/'TPR', '\"Hope shall be the blade that severs our bonds.\"').
card_multiverse_id('charging paladin'/'TPR', '397634').

card_in_set('cinder marsh', 'TPR').
card_original_type('cinder marsh'/'TPR', 'Land').
card_original_text('cinder marsh'/'TPR', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Cinder Marsh doesn\'t untap during your next untap step.').
card_image_name('cinder marsh'/'TPR', 'cinder marsh').
card_uid('cinder marsh'/'TPR', 'TPR:Cinder Marsh:cinder marsh').
card_rarity('cinder marsh'/'TPR', 'Uncommon').
card_artist('cinder marsh'/'TPR', 'John Matson').
card_number('cinder marsh'/'TPR', '236').
card_multiverse_id('cinder marsh'/'TPR', '397547').

card_in_set('city of traitors', 'TPR').
card_original_type('city of traitors'/'TPR', 'Land').
card_original_text('city of traitors'/'TPR', 'When you play another land, sacrifice City of Traitors.\n{T}: Add {2} to your mana pool.').
card_image_name('city of traitors'/'TPR', 'city of traitors').
card_uid('city of traitors'/'TPR', 'TPR:City of Traitors:city of traitors').
card_rarity('city of traitors'/'TPR', 'Rare').
card_artist('city of traitors'/'TPR', 'Kev Walker').
card_number('city of traitors'/'TPR', '237').
card_flavor_text('city of traitors'/'TPR', '\"While we fought, the il surrendered.\"\n—Oracle en-Vec').
card_multiverse_id('city of traitors'/'TPR', '397543').

card_in_set('clot sliver', 'TPR').
card_original_type('clot sliver'/'TPR', 'Creature — Sliver').
card_original_text('clot sliver'/'TPR', 'All Slivers have \"{2}: Regenerate this permanent.\"').
card_image_name('clot sliver'/'TPR', 'clot sliver').
card_uid('clot sliver'/'TPR', 'TPR:Clot Sliver:clot sliver').
card_rarity('clot sliver'/'TPR', 'Common').
card_artist('clot sliver'/'TPR', 'Jeff Laubenstein').
card_number('clot sliver'/'TPR', '85').
card_flavor_text('clot sliver'/'TPR', '\"One would think I would be accustomed to unexpected returns.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('clot sliver'/'TPR', '397636').

card_in_set('coat of arms', 'TPR').
card_original_type('coat of arms'/'TPR', 'Artifact').
card_original_text('coat of arms'/'TPR', 'Each creature gets +1/+1 for each other creature on the battlefield that shares at least one creature type with it. (For example, if two Goblin Warriors and a Goblin Shaman are on the battlefield, each gets +2/+2.)').
card_image_name('coat of arms'/'TPR', 'coat of arms').
card_uid('coat of arms'/'TPR', 'TPR:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'TPR', 'Rare').
card_artist('coat of arms'/'TPR', 'Scott M. Fischer').
card_number('coat of arms'/'TPR', '218').
card_flavor_text('coat of arms'/'TPR', '\"Hup, two, three, four,\nDunno how to count no more.\"\n—Mogg march').
card_multiverse_id('coat of arms'/'TPR', '397422').

card_in_set('coercion', 'TPR').
card_original_type('coercion'/'TPR', 'Sorcery').
card_original_text('coercion'/'TPR', 'Target opponent reveals his or her hand. You choose a card from it. That player discards that card.').
card_image_name('coercion'/'TPR', 'coercion').
card_uid('coercion'/'TPR', 'TPR:Coercion:coercion').
card_rarity('coercion'/'TPR', 'Common').
card_artist('coercion'/'TPR', 'Pete Venters').
card_number('coercion'/'TPR', '86').
card_flavor_text('coercion'/'TPR', '\"There\'s very little that escapes me, Greven. And you will escape very little if you fail.\"\n—Volrath').
card_multiverse_id('coercion'/'TPR', '397509').

card_in_set('coffin queen', 'TPR').
card_original_type('coffin queen'/'TPR', 'Creature — Zombie Wizard').
card_original_text('coffin queen'/'TPR', 'You may choose not to untap Coffin Queen during your untap step.\n{2}{B}, {T}: Put target creature card from a graveyard onto the battlefield under your control. When Coffin Queen becomes untapped or you lose control of Coffin Queen, exile that creature.').
card_image_name('coffin queen'/'TPR', 'coffin queen').
card_uid('coffin queen'/'TPR', 'TPR:Coffin Queen:coffin queen').
card_rarity('coffin queen'/'TPR', 'Rare').
card_artist('coffin queen'/'TPR', 'Kaja Foglio').
card_number('coffin queen'/'TPR', '87').
card_multiverse_id('coffin queen'/'TPR', '397562').

card_in_set('coiled tinviper', 'TPR').
card_original_type('coiled tinviper'/'TPR', 'Artifact Creature — Snake').
card_original_text('coiled tinviper'/'TPR', 'First strike').
card_image_name('coiled tinviper'/'TPR', 'coiled tinviper').
card_uid('coiled tinviper'/'TPR', 'TPR:Coiled Tinviper:coiled tinviper').
card_rarity('coiled tinviper'/'TPR', 'Common').
card_artist('coiled tinviper'/'TPR', 'John Matson').
card_number('coiled tinviper'/'TPR', '219').
card_flavor_text('coiled tinviper'/'TPR', 'The bite of the tinviper feels most like a razor drawn across the tongue.').
card_multiverse_id('coiled tinviper'/'TPR', '397396').

card_in_set('commander greven il-vec', 'TPR').
card_original_type('commander greven il-vec'/'TPR', 'Legendary Creature — Human Warrior').
card_original_text('commander greven il-vec'/'TPR', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen Commander Greven il-Vec enters the battlefield, sacrifice a creature.').
card_image_name('commander greven il-vec'/'TPR', 'commander greven il-vec').
card_uid('commander greven il-vec'/'TPR', 'TPR:Commander Greven il-Vec:commander greven il-vec').
card_rarity('commander greven il-vec'/'TPR', 'Rare').
card_artist('commander greven il-vec'/'TPR', 'Kev Walker').
card_number('commander greven il-vec'/'TPR', '88').
card_flavor_text('commander greven il-vec'/'TPR', '\"Rage is the only freedom left to me.\"').
card_multiverse_id('commander greven il-vec'/'TPR', '397539').

card_in_set('conviction', 'TPR').
card_original_type('conviction'/'TPR', 'Enchantment — Aura').
card_original_text('conviction'/'TPR', 'Enchant creature\nEnchanted creature gets +1/+3.\n{W}: Return Conviction to its owner\'s hand.').
card_image_name('conviction'/'TPR', 'conviction').
card_uid('conviction'/'TPR', 'TPR:Conviction:conviction').
card_rarity('conviction'/'TPR', 'Common').
card_artist('conviction'/'TPR', 'Paolo Parente').
card_number('conviction'/'TPR', '10').
card_flavor_text('conviction'/'TPR', 'It was not the minotaur\'s shoulders but his soul that bore the heaviest weight.').
card_multiverse_id('conviction'/'TPR', '397603').

card_in_set('corpse dance', 'TPR').
card_original_type('corpse dance'/'TPR', 'Instant').
card_original_text('corpse dance'/'TPR', 'Buyback {2} (You may pay an additional {2} as you cast this spell. If you do, put this card into your hand as it resolves.)\nReturn the top creature card of your graveyard to the battlefield. That creature gains haste until end of turn. Exile it at the beginning of the next end step.').
card_image_name('corpse dance'/'TPR', 'corpse dance').
card_uid('corpse dance'/'TPR', 'TPR:Corpse Dance:corpse dance').
card_rarity('corpse dance'/'TPR', 'Rare').
card_artist('corpse dance'/'TPR', 'Brian Snõddy').
card_number('corpse dance'/'TPR', '89').
card_multiverse_id('corpse dance'/'TPR', '397625').

card_in_set('counterspell', 'TPR').
card_original_type('counterspell'/'TPR', 'Instant').
card_original_text('counterspell'/'TPR', 'Counter target spell.').
card_image_name('counterspell'/'TPR', 'counterspell').
card_uid('counterspell'/'TPR', 'TPR:Counterspell:counterspell').
card_rarity('counterspell'/'TPR', 'Uncommon').
card_artist('counterspell'/'TPR', 'Stephen Daniele').
card_number('counterspell'/'TPR', '43').
card_flavor_text('counterspell'/'TPR', '\"It was probably a lousy spell in the first place.\"\n—Ertai, wizard adept').
card_multiverse_id('counterspell'/'TPR', '397612').

card_in_set('crashing boars', 'TPR').
card_original_type('crashing boars'/'TPR', 'Creature — Boar').
card_original_text('crashing boars'/'TPR', 'Whenever Crashing Boars attacks, defending player chooses an untapped creature he or she controls. That creature blocks Crashing Boars this turn if able.').
card_image_name('crashing boars'/'TPR', 'crashing boars').
card_uid('crashing boars'/'TPR', 'TPR:Crashing Boars:crashing boars').
card_rarity('crashing boars'/'TPR', 'Uncommon').
card_artist('crashing boars'/'TPR', 'Ron Spencer').
card_number('crashing boars'/'TPR', '168').
card_flavor_text('crashing boars'/'TPR', 'Bores ruin a party. Boars are party to ruin.').
card_multiverse_id('crashing boars'/'TPR', '397512').

card_in_set('craven giant', 'TPR').
card_original_type('craven giant'/'TPR', 'Creature — Giant').
card_original_text('craven giant'/'TPR', 'Craven Giant can\'t block.').
card_image_name('craven giant'/'TPR', 'craven giant').
card_uid('craven giant'/'TPR', 'TPR:Craven Giant:craven giant').
card_rarity('craven giant'/'TPR', 'Common').
card_artist('craven giant'/'TPR', 'Brian Snõddy').
card_number('craven giant'/'TPR', '128').
card_flavor_text('craven giant'/'TPR', 'What is high as a mountain and low as a snake?\n—Dal riddle').
card_multiverse_id('craven giant'/'TPR', '397583').

card_in_set('crovax the cursed', 'TPR').
card_original_type('crovax the cursed'/'TPR', 'Legendary Creature — Vampire').
card_original_text('crovax the cursed'/'TPR', 'Crovax the Cursed enters the battlefield with four +1/+1 counters on it.\nAt the beginning of your upkeep, you may sacrifice a creature. If you do, put a +1/+1 counter on Crovax. If you don\'t, remove a +1/+1 counter from Crovax.\n{B}: Crovax gains flying until end of turn.').
card_image_name('crovax the cursed'/'TPR', 'crovax the cursed').
card_uid('crovax the cursed'/'TPR', 'TPR:Crovax the Cursed:crovax the cursed').
card_rarity('crovax the cursed'/'TPR', 'Rare').
card_artist('crovax the cursed'/'TPR', 'Pete Venters').
card_number('crovax the cursed'/'TPR', '90').
card_multiverse_id('crovax the cursed'/'TPR', '397613').

card_in_set('crystalline sliver', 'TPR').
card_original_type('crystalline sliver'/'TPR', 'Creature — Sliver').
card_original_text('crystalline sliver'/'TPR', 'All Slivers have shroud. (They can\'t be the targets of spells or abilities.)').
card_image_name('crystalline sliver'/'TPR', 'crystalline sliver').
card_uid('crystalline sliver'/'TPR', 'TPR:Crystalline Sliver:crystalline sliver').
card_rarity('crystalline sliver'/'TPR', 'Uncommon').
card_artist('crystalline sliver'/'TPR', 'Allen Williams').
card_number('crystalline sliver'/'TPR', '207').
card_flavor_text('crystalline sliver'/'TPR', 'Bred as living shields, these slivers have proven unruly—they know they cannot be caught.').
card_multiverse_id('crystalline sliver'/'TPR', '397513').

card_in_set('curiosity', 'TPR').
card_original_type('curiosity'/'TPR', 'Enchantment — Aura').
card_original_text('curiosity'/'TPR', 'Enchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.').
card_image_name('curiosity'/'TPR', 'curiosity').
card_uid('curiosity'/'TPR', 'TPR:Curiosity:curiosity').
card_rarity('curiosity'/'TPR', 'Uncommon').
card_artist('curiosity'/'TPR', 'Val Mayerik').
card_number('curiosity'/'TPR', '44').
card_flavor_text('curiosity'/'TPR', 'All Mirri wanted to do was rest, but she couldn\'t ignore a nagging suspicion as she followed Crovax\'s skulking form.').
card_multiverse_id('curiosity'/'TPR', '397447').

card_in_set('cursed flesh', 'TPR').
card_original_type('cursed flesh'/'TPR', 'Enchantment — Aura').
card_original_text('cursed flesh'/'TPR', 'Enchant creature\nEnchanted creature gets -1/-1 and has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('cursed flesh'/'TPR', 'cursed flesh').
card_uid('cursed flesh'/'TPR', 'TPR:Cursed Flesh:cursed flesh').
card_rarity('cursed flesh'/'TPR', 'Common').
card_artist('cursed flesh'/'TPR', 'Ron Spencer').
card_number('cursed flesh'/'TPR', '91').
card_flavor_text('cursed flesh'/'TPR', 'A farewell to arms . . . and feet . . . and legs . . .').
card_multiverse_id('cursed flesh'/'TPR', '397412').

card_in_set('cursed scroll', 'TPR').
card_original_type('cursed scroll'/'TPR', 'Artifact').
card_original_text('cursed scroll'/'TPR', '{3}, {T}: Name a card. Reveal a card at random from your hand. If it\'s the named card, Cursed Scroll deals 2 damage to target creature or player.').
card_image_name('cursed scroll'/'TPR', 'cursed scroll').
card_uid('cursed scroll'/'TPR', 'TPR:Cursed Scroll:cursed scroll').
card_rarity('cursed scroll'/'TPR', 'Mythic Rare').
card_artist('cursed scroll'/'TPR', 'D. Alexander Gregory').
card_number('cursed scroll'/'TPR', '220').
card_multiverse_id('cursed scroll'/'TPR', '397653').

card_in_set('dark banishing', 'TPR').
card_original_type('dark banishing'/'TPR', 'Instant').
card_original_text('dark banishing'/'TPR', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'TPR', 'dark banishing').
card_uid('dark banishing'/'TPR', 'TPR:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'TPR', 'Common').
card_artist('dark banishing'/'TPR', 'John Matson').
card_number('dark banishing'/'TPR', '92').
card_flavor_text('dark banishing'/'TPR', '\"It is the way of most wizards to begin by exiling themselves and to end by exiling everyone else.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('dark banishing'/'TPR', '397458').

card_in_set('dark ritual', 'TPR').
card_original_type('dark ritual'/'TPR', 'Instant').
card_original_text('dark ritual'/'TPR', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'TPR', 'dark ritual').
card_uid('dark ritual'/'TPR', 'TPR:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'TPR', 'Common').
card_artist('dark ritual'/'TPR', 'Ken Meyer, Jr.').
card_number('dark ritual'/'TPR', '93').
card_flavor_text('dark ritual'/'TPR', '\"If there is such a thing as too much power, I have not discovered it.\"\n—Volrath').
card_multiverse_id('dark ritual'/'TPR', '397401').

card_in_set('dauthi horror', 'TPR').
card_original_type('dauthi horror'/'TPR', 'Creature — Dauthi Horror').
card_original_text('dauthi horror'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Horror can\'t be blocked by white creatures.').
card_image_name('dauthi horror'/'TPR', 'dauthi horror').
card_uid('dauthi horror'/'TPR', 'TPR:Dauthi Horror:dauthi horror').
card_rarity('dauthi horror'/'TPR', 'Common').
card_artist('dauthi horror'/'TPR', 'Jeff Laubenstein').
card_number('dauthi horror'/'TPR', '94').
card_multiverse_id('dauthi horror'/'TPR', '397594').

card_in_set('dauthi jackal', 'TPR').
card_original_type('dauthi jackal'/'TPR', 'Creature — Dauthi Hound').
card_original_text('dauthi jackal'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{B}{B}, Sacrifice Dauthi Jackal: Destroy target blocking creature.').
card_image_name('dauthi jackal'/'TPR', 'dauthi jackal').
card_uid('dauthi jackal'/'TPR', 'TPR:Dauthi Jackal:dauthi jackal').
card_rarity('dauthi jackal'/'TPR', 'Common').
card_artist('dauthi jackal'/'TPR', 'Adam Rex').
card_number('dauthi jackal'/'TPR', '95').
card_flavor_text('dauthi jackal'/'TPR', '\"All dauthi are jackals, regardless of breed.\"\n—Lyna, soltari emissary').
card_multiverse_id('dauthi jackal'/'TPR', '397473').

card_in_set('dauthi marauder', 'TPR').
card_original_type('dauthi marauder'/'TPR', 'Creature — Dauthi Minion').
card_original_text('dauthi marauder'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)').
card_image_name('dauthi marauder'/'TPR', 'dauthi marauder').
card_uid('dauthi marauder'/'TPR', 'TPR:Dauthi Marauder:dauthi marauder').
card_rarity('dauthi marauder'/'TPR', 'Uncommon').
card_artist('dauthi marauder'/'TPR', 'Andrew Robinson').
card_number('dauthi marauder'/'TPR', '96').
card_flavor_text('dauthi marauder'/'TPR', '\"The dauthi came from beneath the ruins one night, and the darkness cast them in the best possible light.\"\n—Soltari Tales of Life').
card_multiverse_id('dauthi marauder'/'TPR', '397395').

card_in_set('dauthi slayer', 'TPR').
card_original_type('dauthi slayer'/'TPR', 'Creature — Dauthi Soldier').
card_original_text('dauthi slayer'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Slayer attacks each turn if able.').
card_image_name('dauthi slayer'/'TPR', 'dauthi slayer').
card_uid('dauthi slayer'/'TPR', 'TPR:Dauthi Slayer:dauthi slayer').
card_rarity('dauthi slayer'/'TPR', 'Uncommon').
card_artist('dauthi slayer'/'TPR', 'Dermot Power').
card_number('dauthi slayer'/'TPR', '97').
card_flavor_text('dauthi slayer'/'TPR', '\"They have knives for every soul.\"\n—Lyna, soltari emissary').
card_multiverse_id('dauthi slayer'/'TPR', '397417').

card_in_set('dauthi warlord', 'TPR').
card_original_type('dauthi warlord'/'TPR', 'Creature — Dauthi Soldier').
card_original_text('dauthi warlord'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDauthi Warlord\'s power is equal to the number of creatures with shadow on the battlefield.').
card_image_name('dauthi warlord'/'TPR', 'dauthi warlord').
card_uid('dauthi warlord'/'TPR', 'TPR:Dauthi Warlord:dauthi warlord').
card_rarity('dauthi warlord'/'TPR', 'Uncommon').
card_artist('dauthi warlord'/'TPR', 'Kev Walker').
card_number('dauthi warlord'/'TPR', '98').
card_multiverse_id('dauthi warlord'/'TPR', '397508').

card_in_set('deadshot', 'TPR').
card_original_type('deadshot'/'TPR', 'Sorcery').
card_original_text('deadshot'/'TPR', 'Tap target creature. It deals damage equal to its power to another target creature.').
card_image_name('deadshot'/'TPR', 'deadshot').
card_uid('deadshot'/'TPR', 'TPR:Deadshot:deadshot').
card_rarity('deadshot'/'TPR', 'Uncommon').
card_artist('deadshot'/'TPR', 'Heather Hudson').
card_number('deadshot'/'TPR', '129').
card_flavor_text('deadshot'/'TPR', '\"Carrion! Keep your distance. My blade will come to you!\"\n—Crovax').
card_multiverse_id('deadshot'/'TPR', '397549').

card_in_set('death pits of rath', 'TPR').
card_original_type('death pits of rath'/'TPR', 'Enchantment').
card_original_text('death pits of rath'/'TPR', 'Whenever a creature is dealt damage, destroy it. It can\'t be regenerated.').
card_image_name('death pits of rath'/'TPR', 'death pits of rath').
card_uid('death pits of rath'/'TPR', 'TPR:Death Pits of Rath:death pits of rath').
card_rarity('death pits of rath'/'TPR', 'Rare').
card_artist('death pits of rath'/'TPR', 'Joel Biske').
card_number('death pits of rath'/'TPR', '99').
card_flavor_text('death pits of rath'/'TPR', 'As the sludge below began to shift and take shapes, Gerrard turned from the railing to Orim. \"I suppose,\" he said, \"it\'s a little too late for prayer, isn\'t it?\"').
card_multiverse_id('death pits of rath'/'TPR', '397493').

card_in_set('death stroke', 'TPR').
card_original_type('death stroke'/'TPR', 'Sorcery').
card_original_text('death stroke'/'TPR', 'Destroy target tapped creature.').
card_image_name('death stroke'/'TPR', 'death stroke').
card_uid('death stroke'/'TPR', 'TPR:Death Stroke:death stroke').
card_rarity('death stroke'/'TPR', 'Common').
card_artist('death stroke'/'TPR', 'Colin MacNeil').
card_number('death stroke'/'TPR', '100').
card_flavor_text('death stroke'/'TPR', 'For a sharp second, Selenia froze, and Crovax\'s blade found home. As the angel shattered like glass, Crovax felt his mind collapse—the curse had been fulfilled.').
card_multiverse_id('death stroke'/'TPR', '397559').

card_in_set('death\'s duet', 'TPR').
card_original_type('death\'s duet'/'TPR', 'Sorcery').
card_original_text('death\'s duet'/'TPR', 'Return two target creature cards from your graveyard to your hand.').
card_image_name('death\'s duet'/'TPR', 'death\'s duet').
card_uid('death\'s duet'/'TPR', 'TPR:Death\'s Duet:death\'s duet').
card_rarity('death\'s duet'/'TPR', 'Common').
card_artist('death\'s duet'/'TPR', 'Keith Parkinson').
card_number('death\'s duet'/'TPR', '101').
card_flavor_text('death\'s duet'/'TPR', 'Life has always been a dance. It is only fitting that death sing the tune.').
card_multiverse_id('death\'s duet'/'TPR', '397632').

card_in_set('diabolic edict', 'TPR').
card_original_type('diabolic edict'/'TPR', 'Instant').
card_original_text('diabolic edict'/'TPR', 'Target player sacrifices a creature.').
card_image_name('diabolic edict'/'TPR', 'diabolic edict').
card_uid('diabolic edict'/'TPR', 'TPR:Diabolic Edict:diabolic edict').
card_rarity('diabolic edict'/'TPR', 'Common').
card_artist('diabolic edict'/'TPR', 'Ron Spencer').
card_number('diabolic edict'/'TPR', '102').
card_flavor_text('diabolic edict'/'TPR', 'Greven il-Vec lifted Vhati off his feet. \"The fall will give you time to think on your failure.\"').
card_multiverse_id('diabolic edict'/'TPR', '397522').

card_in_set('disenchant', 'TPR').
card_original_type('disenchant'/'TPR', 'Instant').
card_original_text('disenchant'/'TPR', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'TPR', 'disenchant').
card_uid('disenchant'/'TPR', 'TPR:Disenchant:disenchant').
card_rarity('disenchant'/'TPR', 'Common').
card_artist('disenchant'/'TPR', 'Allen Williams').
card_number('disenchant'/'TPR', '11').
card_flavor_text('disenchant'/'TPR', 'The swarm seemed unmoved by the artificial slivers\' destruction. \"They clearly were not the leaders,\" Hanna cursed. \"I guess we\'ll have to do this the hard way.\"').
card_multiverse_id('disenchant'/'TPR', '397544').

card_in_set('dismiss', 'TPR').
card_original_type('dismiss'/'TPR', 'Instant').
card_original_text('dismiss'/'TPR', 'Counter target spell.\nDraw a card.').
card_image_name('dismiss'/'TPR', 'dismiss').
card_uid('dismiss'/'TPR', 'TPR:Dismiss:dismiss').
card_rarity('dismiss'/'TPR', 'Uncommon').
card_artist('dismiss'/'TPR', 'Donato Giancola').
card_number('dismiss'/'TPR', '45').
card_flavor_text('dismiss'/'TPR', '\"There is nothing you can do that I cannot simply deny.\"\n—Ertai, wizard adept').
card_multiverse_id('dismiss'/'TPR', '397545').

card_in_set('dracoplasm', 'TPR').
card_original_type('dracoplasm'/'TPR', 'Creature — Shapeshifter').
card_original_text('dracoplasm'/'TPR', 'Flying\nAs Dracoplasm enters the battlefield, sacrifice any number of creatures. Dracoplasm\'s power becomes the total power of those creatures and its toughness becomes their total toughness.\n{R}: Dracoplasm gets +1/+0 until end of turn.').
card_image_name('dracoplasm'/'TPR', 'dracoplasm').
card_uid('dracoplasm'/'TPR', 'TPR:Dracoplasm:dracoplasm').
card_rarity('dracoplasm'/'TPR', 'Rare').
card_artist('dracoplasm'/'TPR', 'Andrew Robinson').
card_number('dracoplasm'/'TPR', '208').
card_multiverse_id('dracoplasm'/'TPR', '397485').

card_in_set('dream halls', 'TPR').
card_original_type('dream halls'/'TPR', 'Enchantment').
card_original_text('dream halls'/'TPR', 'Rather than pay the mana cost for a spell, its controller may discard a card that shares a color with that spell.').
card_image_name('dream halls'/'TPR', 'dream halls').
card_uid('dream halls'/'TPR', 'TPR:Dream Halls:dream halls').
card_rarity('dream halls'/'TPR', 'Mythic Rare').
card_artist('dream halls'/'TPR', 'Matthew D. Wilson').
card_number('dream halls'/'TPR', '46').
card_flavor_text('dream halls'/'TPR', 'Within without.').
card_multiverse_id('dream halls'/'TPR', '397553').

card_in_set('dream prowler', 'TPR').
card_original_type('dream prowler'/'TPR', 'Creature — Illusion').
card_original_text('dream prowler'/'TPR', 'Dream Prowler can\'t be blocked as long as it\'s attacking alone.').
card_image_name('dream prowler'/'TPR', 'dream prowler').
card_uid('dream prowler'/'TPR', 'TPR:Dream Prowler:dream prowler').
card_rarity('dream prowler'/'TPR', 'Uncommon').
card_artist('dream prowler'/'TPR', 'Richard Kane Ferguson').
card_number('dream prowler'/'TPR', '47').
card_flavor_text('dream prowler'/'TPR', '\"To think that some find sleep a restful state.\"\n—Volrath').
card_multiverse_id('dream prowler'/'TPR', '397558').

card_in_set('dungeon shade', 'TPR').
card_original_type('dungeon shade'/'TPR', 'Creature — Shade Spirit').
card_original_text('dungeon shade'/'TPR', 'Flying\n{B}: Dungeon Shade gets +1/+1 until end of turn.').
card_image_name('dungeon shade'/'TPR', 'dungeon shade').
card_uid('dungeon shade'/'TPR', 'TPR:Dungeon Shade:dungeon shade').
card_rarity('dungeon shade'/'TPR', 'Common').
card_artist('dungeon shade'/'TPR', 'Jason Alexander Behnke').
card_number('dungeon shade'/'TPR', '103').
card_flavor_text('dungeon shade'/'TPR', 'A sickness stirs in its eyes, a nightmare born in darkened wails.').
card_multiverse_id('dungeon shade'/'TPR', '397500').

card_in_set('elven rite', 'TPR').
card_original_type('elven rite'/'TPR', 'Sorcery').
card_original_text('elven rite'/'TPR', 'Distribute two +1/+1 counters among one or two target creatures.').
card_image_name('elven rite'/'TPR', 'elven rite').
card_uid('elven rite'/'TPR', 'TPR:Elven Rite:elven rite').
card_rarity('elven rite'/'TPR', 'Common').
card_artist('elven rite'/'TPR', 'Jeff Miracola').
card_number('elven rite'/'TPR', '169').
card_flavor_text('elven rite'/'TPR', 'As the bough stretches, so shall you grow. As the roots spread, so shall you thrive.').
card_multiverse_id('elven rite'/'TPR', '397428').

card_in_set('elvish fury', 'TPR').
card_original_type('elvish fury'/'TPR', 'Instant').
card_original_text('elvish fury'/'TPR', 'Buyback {4} (You may pay an additional {4} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget creature gets +2/+2 until end of turn.').
card_image_name('elvish fury'/'TPR', 'elvish fury').
card_uid('elvish fury'/'TPR', 'TPR:Elvish Fury:elvish fury').
card_rarity('elvish fury'/'TPR', 'Common').
card_artist('elvish fury'/'TPR', 'Quinton Hoover').
card_number('elvish fury'/'TPR', '170').
card_multiverse_id('elvish fury'/'TPR', '397622').

card_in_set('emmessi tome', 'TPR').
card_original_type('emmessi tome'/'TPR', 'Artifact').
card_original_text('emmessi tome'/'TPR', '{5}, {T}: Draw two cards, then discard a card.').
card_image_name('emmessi tome'/'TPR', 'emmessi tome').
card_uid('emmessi tome'/'TPR', 'TPR:Emmessi Tome:emmessi tome').
card_rarity('emmessi tome'/'TPR', 'Uncommon').
card_artist('emmessi tome'/'TPR', 'Tom Wänerstrand').
card_number('emmessi tome'/'TPR', '221').
card_flavor_text('emmessi tome'/'TPR', 'It is like life and destiny: we think we know the story, but we have read only half the tale.').
card_multiverse_id('emmessi tome'/'TPR', '397542').

card_in_set('endangered armodon', 'TPR').
card_original_type('endangered armodon'/'TPR', 'Creature — Elephant').
card_original_text('endangered armodon'/'TPR', 'When you control a creature with toughness 2 or less, sacrifice Endangered Armodon.').
card_image_name('endangered armodon'/'TPR', 'endangered armodon').
card_uid('endangered armodon'/'TPR', 'TPR:Endangered Armodon:endangered armodon').
card_rarity('endangered armodon'/'TPR', 'Common').
card_artist('endangered armodon'/'TPR', 'Kev Walker').
card_number('endangered armodon'/'TPR', '171').
card_flavor_text('endangered armodon'/'TPR', 'These are its last days. It is doomed to be remembered only until the hunters hunger again.').
card_multiverse_id('endangered armodon'/'TPR', '397407').

card_in_set('ephemeron', 'TPR').
card_original_type('ephemeron'/'TPR', 'Creature — Illusion').
card_original_text('ephemeron'/'TPR', 'Flying\nDiscard a card: Return Ephemeron to its owner\'s hand.').
card_image_name('ephemeron'/'TPR', 'ephemeron').
card_uid('ephemeron'/'TPR', 'TPR:Ephemeron:ephemeron').
card_rarity('ephemeron'/'TPR', 'Rare').
card_artist('ephemeron'/'TPR', 'Keith Parkinson').
card_number('ephemeron'/'TPR', '48').
card_flavor_text('ephemeron'/'TPR', 'From nothing came teeth.').
card_multiverse_id('ephemeron'/'TPR', '397565').

card_in_set('erratic portal', 'TPR').
card_original_type('erratic portal'/'TPR', 'Artifact').
card_original_text('erratic portal'/'TPR', '{1}, {T}: Return target creature to its owner\'s hand unless its controller pays {1}.').
card_image_name('erratic portal'/'TPR', 'erratic portal').
card_uid('erratic portal'/'TPR', 'TPR:Erratic Portal:erratic portal').
card_rarity('erratic portal'/'TPR', 'Rare').
card_artist('erratic portal'/'TPR', 'John Matson').
card_number('erratic portal'/'TPR', '222').
card_flavor_text('erratic portal'/'TPR', '\"In Barrin\'s name!\" cried Lyna as Hanna\'s sword passed through her, \"Ertai sends word that the portal is open—but not for long!\"').
card_multiverse_id('erratic portal'/'TPR', '397649').

card_in_set('evincar\'s justice', 'TPR').
card_original_type('evincar\'s justice'/'TPR', 'Sorcery').
card_original_text('evincar\'s justice'/'TPR', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nEvincar\'s Justice deals 2 damage to each creature and each player.').
card_image_name('evincar\'s justice'/'TPR', 'evincar\'s justice').
card_uid('evincar\'s justice'/'TPR', 'TPR:Evincar\'s Justice:evincar\'s justice').
card_rarity('evincar\'s justice'/'TPR', 'Uncommon').
card_artist('evincar\'s justice'/'TPR', 'Hannibal King').
card_number('evincar\'s justice'/'TPR', '104').
card_multiverse_id('evincar\'s justice'/'TPR', '397615').

card_in_set('exalted dragon', 'TPR').
card_original_type('exalted dragon'/'TPR', 'Creature — Dragon').
card_original_text('exalted dragon'/'TPR', 'Flying\nExalted Dragon can\'t attack unless you sacrifice a land. (This cost is paid as attackers are declared.)').
card_image_name('exalted dragon'/'TPR', 'exalted dragon').
card_uid('exalted dragon'/'TPR', 'TPR:Exalted Dragon:exalted dragon').
card_rarity('exalted dragon'/'TPR', 'Rare').
card_artist('exalted dragon'/'TPR', 'Matthew D. Wilson').
card_number('exalted dragon'/'TPR', '12').
card_flavor_text('exalted dragon'/'TPR', 'If dragons excel at anything more than vanity, it is greed.').
card_multiverse_id('exalted dragon'/'TPR', '397601').

card_in_set('fanning the flames', 'TPR').
card_original_type('fanning the flames'/'TPR', 'Sorcery').
card_original_text('fanning the flames'/'TPR', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nFanning the Flames deals X damage to target creature or player.').
card_image_name('fanning the flames'/'TPR', 'fanning the flames').
card_uid('fanning the flames'/'TPR', 'TPR:Fanning the Flames:fanning the flames').
card_rarity('fanning the flames'/'TPR', 'Rare').
card_artist('fanning the flames'/'TPR', 'Ron Spencer').
card_number('fanning the flames'/'TPR', '130').
card_multiverse_id('fanning the flames'/'TPR', '397555').

card_in_set('field of souls', 'TPR').
card_original_type('field of souls'/'TPR', 'Enchantment').
card_original_text('field of souls'/'TPR', 'Whenever a nontoken creature is put into your graveyard from the battlefield, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('field of souls'/'TPR', 'field of souls').
card_uid('field of souls'/'TPR', 'TPR:Field of Souls:field of souls').
card_rarity('field of souls'/'TPR', 'Rare').
card_artist('field of souls'/'TPR', 'Richard Kane Ferguson').
card_number('field of souls'/'TPR', '13').
card_multiverse_id('field of souls'/'TPR', '397645').

card_in_set('fighting drake', 'TPR').
card_original_type('fighting drake'/'TPR', 'Creature — Drake').
card_original_text('fighting drake'/'TPR', 'Flying').
card_image_name('fighting drake'/'TPR', 'fighting drake').
card_uid('fighting drake'/'TPR', 'TPR:Fighting Drake:fighting drake').
card_rarity('fighting drake'/'TPR', 'Uncommon').
card_artist('fighting drake'/'TPR', 'DiTerlizzi').
card_number('fighting drake'/'TPR', '49').
card_flavor_text('fighting drake'/'TPR', 'Two things are needed to survive the harsh skies of Rath: a thick hide and a thicker skull.').
card_multiverse_id('fighting drake'/'TPR', '397491').

card_in_set('flame wave', 'TPR').
card_original_type('flame wave'/'TPR', 'Sorcery').
card_original_text('flame wave'/'TPR', 'Flame Wave deals 4 damage to target player and each creature he or she controls.').
card_image_name('flame wave'/'TPR', 'flame wave').
card_uid('flame wave'/'TPR', 'TPR:Flame Wave:flame wave').
card_rarity('flame wave'/'TPR', 'Rare').
card_artist('flame wave'/'TPR', 'Donato Giancola').
card_number('flame wave'/'TPR', '131').
card_flavor_text('flame wave'/'TPR', '\"I hear the roaring of a wave whose waters are red and whose mists are black.\"\n—Oracle en-Vec').
card_multiverse_id('flame wave'/'TPR', '397629').

card_in_set('flowstone blade', 'TPR').
card_original_type('flowstone blade'/'TPR', 'Enchantment — Aura').
card_original_text('flowstone blade'/'TPR', 'Enchant creature\n{R}: Enchanted creature gets +1/-1 until end of turn.').
card_image_name('flowstone blade'/'TPR', 'flowstone blade').
card_uid('flowstone blade'/'TPR', 'TPR:Flowstone Blade:flowstone blade').
card_rarity('flowstone blade'/'TPR', 'Common').
card_artist('flowstone blade'/'TPR', 'Allen Williams').
card_number('flowstone blade'/'TPR', '132').
card_flavor_text('flowstone blade'/'TPR', '\"Remember the fable of the elf who came upon a cave of gold. In trying to free the largest piece, she was crushed by its weight.\"\n—Karn, silver golem').
card_multiverse_id('flowstone blade'/'TPR', '397494').

card_in_set('flowstone mauler', 'TPR').
card_original_type('flowstone mauler'/'TPR', 'Creature — Beast').
card_original_text('flowstone mauler'/'TPR', 'Trample\n{R}: Flowstone Mauler gets +1/-1 until end of turn.').
card_image_name('flowstone mauler'/'TPR', 'flowstone mauler').
card_uid('flowstone mauler'/'TPR', 'TPR:Flowstone Mauler:flowstone mauler').
card_rarity('flowstone mauler'/'TPR', 'Uncommon').
card_artist('flowstone mauler'/'TPR', 'Paolo Parente').
card_number('flowstone mauler'/'TPR', '133').
card_flavor_text('flowstone mauler'/'TPR', 'Once these horrifying creatures were perfected, there was no need for armodons.').
card_multiverse_id('flowstone mauler'/'TPR', '397617').

card_in_set('flowstone wyvern', 'TPR').
card_original_type('flowstone wyvern'/'TPR', 'Creature — Drake').
card_original_text('flowstone wyvern'/'TPR', 'Flying\n{R}: Flowstone Wyvern gets +2/-2 until end of turn.').
card_image_name('flowstone wyvern'/'TPR', 'flowstone wyvern').
card_uid('flowstone wyvern'/'TPR', 'TPR:Flowstone Wyvern:flowstone wyvern').
card_rarity('flowstone wyvern'/'TPR', 'Uncommon').
card_artist('flowstone wyvern'/'TPR', 'Stephen Daniele').
card_number('flowstone wyvern'/'TPR', '134').
card_flavor_text('flowstone wyvern'/'TPR', '\"Where I come from, stone stays on the ground.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('flowstone wyvern'/'TPR', '397581').

card_in_set('forbid', 'TPR').
card_original_type('forbid'/'TPR', 'Instant').
card_original_text('forbid'/'TPR', 'Buyback—Discard two cards. (You may discard two cards in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nCounter target spell.').
card_image_name('forbid'/'TPR', 'forbid').
card_uid('forbid'/'TPR', 'TPR:Forbid:forbid').
card_rarity('forbid'/'TPR', 'Rare').
card_artist('forbid'/'TPR', 'Scott Kirschner').
card_number('forbid'/'TPR', '50').
card_multiverse_id('forbid'/'TPR', '397532').

card_in_set('forest', 'TPR').
card_original_type('forest'/'TPR', 'Basic Land — Forest').
card_original_text('forest'/'TPR', 'G').
card_image_name('forest'/'TPR', 'forest1').
card_uid('forest'/'TPR', 'TPR:Forest:forest1').
card_rarity('forest'/'TPR', 'Basic Land').
card_artist('forest'/'TPR', 'Douglas Shuler').
card_number('forest'/'TPR', '266').
card_multiverse_id('forest'/'TPR', '397540').

card_in_set('forest', 'TPR').
card_original_type('forest'/'TPR', 'Basic Land — Forest').
card_original_text('forest'/'TPR', 'G').
card_image_name('forest'/'TPR', 'forest2').
card_uid('forest'/'TPR', 'TPR:Forest:forest2').
card_rarity('forest'/'TPR', 'Basic Land').
card_artist('forest'/'TPR', 'Douglas Shuler').
card_number('forest'/'TPR', '267').
card_multiverse_id('forest'/'TPR', '397453').

card_in_set('forest', 'TPR').
card_original_type('forest'/'TPR', 'Basic Land — Forest').
card_original_text('forest'/'TPR', 'G').
card_image_name('forest'/'TPR', 'forest3').
card_uid('forest'/'TPR', 'TPR:Forest:forest3').
card_rarity('forest'/'TPR', 'Basic Land').
card_artist('forest'/'TPR', 'Douglas Shuler').
card_number('forest'/'TPR', '268').
card_multiverse_id('forest'/'TPR', '397582').

card_in_set('forest', 'TPR').
card_original_type('forest'/'TPR', 'Basic Land — Forest').
card_original_text('forest'/'TPR', 'G').
card_image_name('forest'/'TPR', 'forest4').
card_uid('forest'/'TPR', 'TPR:Forest:forest4').
card_rarity('forest'/'TPR', 'Basic Land').
card_artist('forest'/'TPR', 'Douglas Shuler').
card_number('forest'/'TPR', '269').
card_multiverse_id('forest'/'TPR', '397517').

card_in_set('fugue', 'TPR').
card_original_type('fugue'/'TPR', 'Sorcery').
card_original_text('fugue'/'TPR', 'Target player discards three cards.').
card_image_name('fugue'/'TPR', 'fugue').
card_uid('fugue'/'TPR', 'TPR:Fugue:fugue').
card_rarity('fugue'/'TPR', 'Uncommon').
card_artist('fugue'/'TPR', 'Randy Gallegos').
card_number('fugue'/'TPR', '105').
card_flavor_text('fugue'/'TPR', '\"My ship . . . ,\" Sisay mumbled. \"Intact,\" Gerrard reassured her, \"which is more than I can say for you, my friend.\" Sisay laughed despite the pain.').
card_multiverse_id('fugue'/'TPR', '397502').

card_in_set('furnace brood', 'TPR').
card_original_type('furnace brood'/'TPR', 'Creature — Elemental').
card_original_text('furnace brood'/'TPR', '{R}: Target creature can\'t be regenerated this turn.').
card_image_name('furnace brood'/'TPR', 'furnace brood').
card_uid('furnace brood'/'TPR', 'TPR:Furnace Brood:furnace brood').
card_rarity('furnace brood'/'TPR', 'Common').
card_artist('furnace brood'/'TPR', 'Jeff Miracola').
card_number('furnace brood'/'TPR', '135').
card_flavor_text('furnace brood'/'TPR', 'Furnace air crackles with magmatic cackles.').
card_multiverse_id('furnace brood'/'TPR', '397436').

card_in_set('gallantry', 'TPR').
card_original_type('gallantry'/'TPR', 'Instant').
card_original_text('gallantry'/'TPR', 'Target blocking creature gets +4/+4 until end of turn.\nDraw a card.').
card_image_name('gallantry'/'TPR', 'gallantry').
card_uid('gallantry'/'TPR', 'TPR:Gallantry:gallantry').
card_rarity('gallantry'/'TPR', 'Uncommon').
card_artist('gallantry'/'TPR', 'Douglas Shuler').
card_number('gallantry'/'TPR', '14').
card_flavor_text('gallantry'/'TPR', '\"Forgive me,\" Mirri whispered. \"I thought you a burden, and you saved my life.\" Hanna quietly accepted the apology.').
card_multiverse_id('gallantry'/'TPR', '397554').

card_in_set('gaseous form', 'TPR').
card_original_type('gaseous form'/'TPR', 'Enchantment — Aura').
card_original_text('gaseous form'/'TPR', 'Enchant creature\nPrevent all combat damage that would be dealt to and dealt by enchanted creature.').
card_image_name('gaseous form'/'TPR', 'gaseous form').
card_uid('gaseous form'/'TPR', 'TPR:Gaseous Form:gaseous form').
card_rarity('gaseous form'/'TPR', 'Common').
card_artist('gaseous form'/'TPR', 'Roger Raupp').
card_number('gaseous form'/'TPR', '51').
card_flavor_text('gaseous form'/'TPR', '\"It\'s hard to hurt anyone when you\'re as transparent as your motives.\"\n—Ertai, wizard adept').
card_multiverse_id('gaseous form'/'TPR', '397497').

card_in_set('gerrard\'s battle cry', 'TPR').
card_original_type('gerrard\'s battle cry'/'TPR', 'Enchantment').
card_original_text('gerrard\'s battle cry'/'TPR', '{2}{W}: Creatures you control get +1/+1 until end of turn.').
card_image_name('gerrard\'s battle cry'/'TPR', 'gerrard\'s battle cry').
card_uid('gerrard\'s battle cry'/'TPR', 'TPR:Gerrard\'s Battle Cry:gerrard\'s battle cry').
card_rarity('gerrard\'s battle cry'/'TPR', 'Rare').
card_artist('gerrard\'s battle cry'/'TPR', 'Val Mayerik').
card_number('gerrard\'s battle cry'/'TPR', '15').
card_flavor_text('gerrard\'s battle cry'/'TPR', 'Gerrard grinned and drew his sword. \"This won\'t be a fair fight,\" he called to his crew. \"They should have brought a second ship!\"').
card_multiverse_id('gerrard\'s battle cry'/'TPR', '397481').

card_in_set('goblin bombardment', 'TPR').
card_original_type('goblin bombardment'/'TPR', 'Enchantment').
card_original_text('goblin bombardment'/'TPR', 'Sacrifice a creature: Goblin Bombardment deals 1 damage to target creature or player.').
card_image_name('goblin bombardment'/'TPR', 'goblin bombardment').
card_uid('goblin bombardment'/'TPR', 'TPR:Goblin Bombardment:goblin bombardment').
card_rarity('goblin bombardment'/'TPR', 'Uncommon').
card_artist('goblin bombardment'/'TPR', 'Brian Snõddy').
card_number('goblin bombardment'/'TPR', '136').
card_flavor_text('goblin bombardment'/'TPR', 'One mogg to aim the catapult, one mogg to steer the rock.').
card_multiverse_id('goblin bombardment'/'TPR', '397584').

card_in_set('gravedigger', 'TPR').
card_original_type('gravedigger'/'TPR', 'Creature — Zombie').
card_original_text('gravedigger'/'TPR', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'TPR', 'gravedigger').
card_uid('gravedigger'/'TPR', 'TPR:Gravedigger:gravedigger').
card_rarity('gravedigger'/'TPR', 'Common').
card_artist('gravedigger'/'TPR', 'Dermot Power').
card_number('gravedigger'/'TPR', '106').
card_flavor_text('gravedigger'/'TPR', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'TPR', '397518').

card_in_set('grindstone', 'TPR').
card_original_type('grindstone'/'TPR', 'Artifact').
card_original_text('grindstone'/'TPR', '{3}, {T}: Target player puts the top two cards of his or her library into his or her graveyard. If both cards share a color, repeat this process.').
card_image_name('grindstone'/'TPR', 'grindstone').
card_uid('grindstone'/'TPR', 'TPR:Grindstone:grindstone').
card_rarity('grindstone'/'TPR', 'Mythic Rare').
card_artist('grindstone'/'TPR', 'Greg Simanson').
card_number('grindstone'/'TPR', '223').
card_flavor_text('grindstone'/'TPR', '\"Memory is a burden that wears at the soul as weather wears at stone.\"\n—Crovax').
card_multiverse_id('grindstone'/'TPR', '397443').

card_in_set('hammerhead shark', 'TPR').
card_original_type('hammerhead shark'/'TPR', 'Creature — Fish').
card_original_text('hammerhead shark'/'TPR', 'Hammerhead Shark can\'t attack unless defending player controls an Island.').
card_image_name('hammerhead shark'/'TPR', 'hammerhead shark').
card_uid('hammerhead shark'/'TPR', 'TPR:Hammerhead Shark:hammerhead shark').
card_rarity('hammerhead shark'/'TPR', 'Common').
card_artist('hammerhead shark'/'TPR', 'Stephen Daniele').
card_number('hammerhead shark'/'TPR', '52').
card_flavor_text('hammerhead shark'/'TPR', 'Cross the eyes of a hammerhead and you\'ll dot its teeth.').
card_multiverse_id('hammerhead shark'/'TPR', '397472').

card_in_set('harrow', 'TPR').
card_original_type('harrow'/'TPR', 'Instant').
card_original_text('harrow'/'TPR', 'As an additional cost to cast Harrow, sacrifice a land.\nSearch your library for up to two basic land cards and put them onto the battlefield. Then shuffle your library.').
card_image_name('harrow'/'TPR', 'harrow').
card_uid('harrow'/'TPR', 'TPR:Harrow:harrow').
card_rarity('harrow'/'TPR', 'Common').
card_artist('harrow'/'TPR', 'Eric David Anderson').
card_number('harrow'/'TPR', '172').
card_flavor_text('harrow'/'TPR', 'Changing Rath\'s landscape is like tearing the scab from a wound.').
card_multiverse_id('harrow'/'TPR', '397415').

card_in_set('heartwood dryad', 'TPR').
card_original_type('heartwood dryad'/'TPR', 'Creature — Dryad').
card_original_text('heartwood dryad'/'TPR', 'Heartwood Dryad can block creatures with shadow as though Heartwood Dryad had shadow.').
card_image_name('heartwood dryad'/'TPR', 'heartwood dryad').
card_uid('heartwood dryad'/'TPR', 'TPR:Heartwood Dryad:heartwood dryad').
card_rarity('heartwood dryad'/'TPR', 'Common').
card_artist('heartwood dryad'/'TPR', 'Rebecca Guay').
card_number('heartwood dryad'/'TPR', '173').
card_flavor_text('heartwood dryad'/'TPR', 'To snare what lies between worlds takes the patient reach of trees.').
card_multiverse_id('heartwood dryad'/'TPR', '397488').

card_in_set('heartwood giant', 'TPR').
card_original_type('heartwood giant'/'TPR', 'Creature — Giant').
card_original_text('heartwood giant'/'TPR', '{T}, Sacrifice a Forest: Heartwood Giant deals 2 damage to target player.').
card_image_name('heartwood giant'/'TPR', 'heartwood giant').
card_uid('heartwood giant'/'TPR', 'TPR:Heartwood Giant:heartwood giant').
card_rarity('heartwood giant'/'TPR', 'Rare').
card_artist('heartwood giant'/'TPR', 'Randy Elliott').
card_number('heartwood giant'/'TPR', '174').
card_flavor_text('heartwood giant'/'TPR', 'Wind in the trees is soothing, but the same can\'t be said for trees on the wind.').
card_multiverse_id('heartwood giant'/'TPR', '397440').

card_in_set('hermit druid', 'TPR').
card_original_type('hermit druid'/'TPR', 'Creature — Human Druid').
card_original_text('hermit druid'/'TPR', '{G}, {T}: Reveal cards from the top of your library until you reveal a basic land card. Put that card into your hand and all other cards revealed this way into your graveyard.').
card_image_name('hermit druid'/'TPR', 'hermit druid').
card_uid('hermit druid'/'TPR', 'TPR:Hermit Druid:hermit druid').
card_rarity('hermit druid'/'TPR', 'Uncommon').
card_artist('hermit druid'/'TPR', 'Heather Hudson').
card_number('hermit druid'/'TPR', '175').
card_flavor_text('hermit druid'/'TPR', 'Seeking the company of plants ensures that your wits will go to seed.').
card_multiverse_id('hermit druid'/'TPR', '397556').

card_in_set('hibernation sliver', 'TPR').
card_original_type('hibernation sliver'/'TPR', 'Creature — Sliver').
card_original_text('hibernation sliver'/'TPR', 'All Slivers have \"Pay 2 life: Return this permanent to its owner\'s hand.\"').
card_image_name('hibernation sliver'/'TPR', 'hibernation sliver').
card_uid('hibernation sliver'/'TPR', 'TPR:Hibernation Sliver:hibernation sliver').
card_rarity('hibernation sliver'/'TPR', 'Uncommon').
card_artist('hibernation sliver'/'TPR', 'Scott Kirschner').
card_number('hibernation sliver'/'TPR', '209').
card_flavor_text('hibernation sliver'/'TPR', 'Mogglings have been known to play ball with hibernating slivers, completely unaware of their true nature.').
card_multiverse_id('hibernation sliver'/'TPR', '397495').

card_in_set('horned sliver', 'TPR').
card_original_type('horned sliver'/'TPR', 'Creature — Sliver').
card_original_text('horned sliver'/'TPR', 'All Sliver creatures have trample.').
card_image_name('horned sliver'/'TPR', 'horned sliver').
card_uid('horned sliver'/'TPR', 'TPR:Horned Sliver:horned sliver').
card_rarity('horned sliver'/'TPR', 'Common').
card_artist('horned sliver'/'TPR', 'Allen Williams').
card_number('horned sliver'/'TPR', '176').
card_flavor_text('horned sliver'/'TPR', 'A bristling wave of slivers broke against the Weatherlight\'s timbers.').
card_multiverse_id('horned sliver'/'TPR', '397390').

card_in_set('horned turtle', 'TPR').
card_original_type('horned turtle'/'TPR', 'Creature — Turtle').
card_original_text('horned turtle'/'TPR', '').
card_image_name('horned turtle'/'TPR', 'horned turtle').
card_uid('horned turtle'/'TPR', 'TPR:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'TPR', 'Common').
card_artist('horned turtle'/'TPR', 'DiTerlizzi').
card_number('horned turtle'/'TPR', '53').
card_flavor_text('horned turtle'/'TPR', '\"So like men are they: a hardened shell with fragile flesh beneath.\"\n—Selenia, dark angel').
card_multiverse_id('horned turtle'/'TPR', '397526').

card_in_set('humility', 'TPR').
card_original_type('humility'/'TPR', 'Enchantment').
card_original_text('humility'/'TPR', 'All creatures lose all abilities and have base power and toughness 1/1.').
card_image_name('humility'/'TPR', 'humility').
card_uid('humility'/'TPR', 'TPR:Humility:humility').
card_rarity('humility'/'TPR', 'Mythic Rare').
card_artist('humility'/'TPR', 'Phil Foglio').
card_number('humility'/'TPR', '16').
card_flavor_text('humility'/'TPR', '\"One cannot cleanse the wounds of failure.\"\n—Karn, silver golem').
card_multiverse_id('humility'/'TPR', '397614').

card_in_set('intuition', 'TPR').
card_original_type('intuition'/'TPR', 'Instant').
card_original_text('intuition'/'TPR', 'Search your library for three cards and reveal them. Target opponent chooses one. Put that card into your hand and the rest into your graveyard. Then shuffle your library.').
card_image_name('intuition'/'TPR', 'intuition').
card_uid('intuition'/'TPR', 'TPR:Intuition:intuition').
card_rarity('intuition'/'TPR', 'Rare').
card_artist('intuition'/'TPR', 'April Lee').
card_number('intuition'/'TPR', '54').
card_multiverse_id('intuition'/'TPR', '397633').

card_in_set('island', 'TPR').
card_original_type('island'/'TPR', 'Basic Land — Island').
card_original_text('island'/'TPR', 'U').
card_image_name('island'/'TPR', 'island1').
card_uid('island'/'TPR', 'TPR:Island:island1').
card_rarity('island'/'TPR', 'Basic Land').
card_artist('island'/'TPR', 'Randy Gallegos').
card_number('island'/'TPR', '254').
card_multiverse_id('island'/'TPR', '397456').

card_in_set('island', 'TPR').
card_original_type('island'/'TPR', 'Basic Land — Island').
card_original_text('island'/'TPR', 'U').
card_image_name('island'/'TPR', 'island2').
card_uid('island'/'TPR', 'TPR:Island:island2').
card_rarity('island'/'TPR', 'Basic Land').
card_artist('island'/'TPR', 'Randy Gallegos').
card_number('island'/'TPR', '255').
card_multiverse_id('island'/'TPR', '397476').

card_in_set('island', 'TPR').
card_original_type('island'/'TPR', 'Basic Land — Island').
card_original_text('island'/'TPR', 'U').
card_image_name('island'/'TPR', 'island3').
card_uid('island'/'TPR', 'TPR:Island:island3').
card_rarity('island'/'TPR', 'Basic Land').
card_artist('island'/'TPR', 'Randy Gallegos').
card_number('island'/'TPR', '256').
card_multiverse_id('island'/'TPR', '397596').

card_in_set('island', 'TPR').
card_original_type('island'/'TPR', 'Basic Land — Island').
card_original_text('island'/'TPR', 'U').
card_image_name('island'/'TPR', 'island4').
card_uid('island'/'TPR', 'TPR:Island:island4').
card_rarity('island'/'TPR', 'Basic Land').
card_artist('island'/'TPR', 'Randy Gallegos').
card_number('island'/'TPR', '257').
card_multiverse_id('island'/'TPR', '397654').

card_in_set('jinxed idol', 'TPR').
card_original_type('jinxed idol'/'TPR', 'Artifact').
card_original_text('jinxed idol'/'TPR', 'At the beginning of your upkeep, Jinxed Idol deals 2 damage to you.\nSacrifice a creature: Target opponent gains control of Jinxed Idol.').
card_image_name('jinxed idol'/'TPR', 'jinxed idol').
card_uid('jinxed idol'/'TPR', 'TPR:Jinxed Idol:jinxed idol').
card_rarity('jinxed idol'/'TPR', 'Rare').
card_artist('jinxed idol'/'TPR', 'John Matson').
card_number('jinxed idol'/'TPR', '224').
card_flavor_text('jinxed idol'/'TPR', '\"Here.\"').
card_multiverse_id('jinxed idol'/'TPR', '397505').

card_in_set('kezzerdrix', 'TPR').
card_original_type('kezzerdrix'/'TPR', 'Creature — Rabbit Beast').
card_original_text('kezzerdrix'/'TPR', 'First strike\nAt the beginning of your upkeep, if your opponents control no creatures, Kezzerdrix deals 4 damage to you.').
card_image_name('kezzerdrix'/'TPR', 'kezzerdrix').
card_uid('kezzerdrix'/'TPR', 'TPR:Kezzerdrix:kezzerdrix').
card_rarity('kezzerdrix'/'TPR', 'Rare').
card_artist('kezzerdrix'/'TPR', 'Matthew D. Wilson').
card_number('kezzerdrix'/'TPR', '107').
card_multiverse_id('kezzerdrix'/'TPR', '397470').

card_in_set('killer whale', 'TPR').
card_original_type('killer whale'/'TPR', 'Creature — Whale').
card_original_text('killer whale'/'TPR', '{U}: Killer Whale gains flying until end of turn.').
card_image_name('killer whale'/'TPR', 'killer whale').
card_uid('killer whale'/'TPR', 'TPR:Killer Whale:killer whale').
card_rarity('killer whale'/'TPR', 'Uncommon').
card_artist('killer whale'/'TPR', 'Stephen Daniele').
card_number('killer whale'/'TPR', '55').
card_flavor_text('killer whale'/'TPR', 'Hunger is like the sea: deep, endless, and unforgiving.').
card_multiverse_id('killer whale'/'TPR', '397618').

card_in_set('kindle', 'TPR').
card_original_type('kindle'/'TPR', 'Instant').
card_original_text('kindle'/'TPR', 'Kindle deals X damage to target creature or player, where X is 2 plus the number of cards named Kindle in all graveyards.').
card_image_name('kindle'/'TPR', 'kindle').
card_uid('kindle'/'TPR', 'TPR:Kindle:kindle').
card_rarity('kindle'/'TPR', 'Common').
card_artist('kindle'/'TPR', 'Donato Giancola').
card_number('kindle'/'TPR', '137').
card_flavor_text('kindle'/'TPR', 'Hope of deliverance is scorched by the fire of futility.').
card_multiverse_id('kindle'/'TPR', '397431').

card_in_set('kor chant', 'TPR').
card_original_type('kor chant'/'TPR', 'Instant').
card_original_text('kor chant'/'TPR', 'All damage that would be dealt this turn to target creature you control by a source of your choice is dealt to another target creature instead.').
card_image_name('kor chant'/'TPR', 'kor chant').
card_uid('kor chant'/'TPR', 'TPR:Kor Chant:kor chant').
card_rarity('kor chant'/'TPR', 'Uncommon').
card_artist('kor chant'/'TPR', 'John Matson').
card_number('kor chant'/'TPR', '17').
card_flavor_text('kor chant'/'TPR', 'The true treasure no thief can touch.').
card_multiverse_id('kor chant'/'TPR', '397541').

card_in_set('krakilin', 'TPR').
card_original_type('krakilin'/'TPR', 'Creature — Beast').
card_original_text('krakilin'/'TPR', 'Krakilin enters the battlefield with X +1/+1 counters on it.\n{1}{G}: Regenerate Krakilin.').
card_image_name('krakilin'/'TPR', 'krakilin').
card_uid('krakilin'/'TPR', 'TPR:Krakilin:krakilin').
card_rarity('krakilin'/'TPR', 'Rare').
card_artist('krakilin'/'TPR', 'Richard Kane Ferguson').
card_number('krakilin'/'TPR', '177').
card_flavor_text('krakilin'/'TPR', '\"A perfect reflection of its world: brutal, fetal, and lacking truth.\"\n—Oracle en-Vec').
card_multiverse_id('krakilin'/'TPR', '397521').

card_in_set('lab rats', 'TPR').
card_original_type('lab rats'/'TPR', 'Sorcery').
card_original_text('lab rats'/'TPR', 'Buyback {4} (You may pay an additional {4} as you cast this spell. If you do, put this card into your hand as it resolves.)\nPut a 1/1 black Rat creature token onto the battlefield.').
card_image_name('lab rats'/'TPR', 'lab rats').
card_uid('lab rats'/'TPR', 'TPR:Lab Rats:lab rats').
card_rarity('lab rats'/'TPR', 'Common').
card_artist('lab rats'/'TPR', 'DiTerlizzi').
card_number('lab rats'/'TPR', '108').
card_multiverse_id('lab rats'/'TPR', '397640').

card_in_set('legacy\'s allure', 'TPR').
card_original_type('legacy\'s allure'/'TPR', 'Enchantment').
card_original_text('legacy\'s allure'/'TPR', 'At the beginning of your upkeep, you may put a treasure counter on Legacy\'s Allure.\nSacrifice Legacy\'s Allure: Gain control of target creature with power less than or equal to the number of treasure counters on Legacy\'s Allure.').
card_image_name('legacy\'s allure'/'TPR', 'legacy\'s allure').
card_uid('legacy\'s allure'/'TPR', 'TPR:Legacy\'s Allure:legacy\'s allure').
card_rarity('legacy\'s allure'/'TPR', 'Rare').
card_artist('legacy\'s allure'/'TPR', 'Daren Bader').
card_number('legacy\'s allure'/'TPR', '56').
card_multiverse_id('legacy\'s allure'/'TPR', '397409').

card_in_set('legerdemain', 'TPR').
card_original_type('legerdemain'/'TPR', 'Sorcery').
card_original_text('legerdemain'/'TPR', 'Exchange control of target artifact or creature and another target permanent that shares one of those types with it.').
card_image_name('legerdemain'/'TPR', 'legerdemain').
card_uid('legerdemain'/'TPR', 'TPR:Legerdemain:legerdemain').
card_rarity('legerdemain'/'TPR', 'Uncommon').
card_artist('legerdemain'/'TPR', 'Daren Bader').
card_number('legerdemain'/'TPR', '57').
card_flavor_text('legerdemain'/'TPR', 'Squee tucked the warm ball in his pocket and slipped a pebble in its place. \"Glok,\" he mumbled, and hid.').
card_multiverse_id('legerdemain'/'TPR', '397503').

card_in_set('lightning blast', 'TPR').
card_original_type('lightning blast'/'TPR', 'Instant').
card_original_text('lightning blast'/'TPR', 'Lightning Blast deals 4 damage to target creature or player.').
card_image_name('lightning blast'/'TPR', 'lightning blast').
card_uid('lightning blast'/'TPR', 'TPR:Lightning Blast:lightning blast').
card_rarity('lightning blast'/'TPR', 'Common').
card_artist('lightning blast'/'TPR', 'Richard Thomas').
card_number('lightning blast'/'TPR', '138').
card_flavor_text('lightning blast'/'TPR', '\"Those who fear the darkness have never seen what the light can do.\"\n—Selenia, dark angel').
card_multiverse_id('lightning blast'/'TPR', '397444').

card_in_set('living death', 'TPR').
card_original_type('living death'/'TPR', 'Sorcery').
card_original_text('living death'/'TPR', 'Each player exiles all creature cards from his or her graveyard, then sacrifices all creatures he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_image_name('living death'/'TPR', 'living death').
card_uid('living death'/'TPR', 'TPR:Living Death:living death').
card_rarity('living death'/'TPR', 'Mythic Rare').
card_artist('living death'/'TPR', 'Charles Gillespie').
card_number('living death'/'TPR', '109').
card_multiverse_id('living death'/'TPR', '397523').

card_in_set('lotus petal', 'TPR').
card_original_type('lotus petal'/'TPR', 'Artifact').
card_original_text('lotus petal'/'TPR', '{T}, Sacrifice Lotus Petal: Add one mana of any color to your mana pool.').
card_image_name('lotus petal'/'TPR', 'lotus petal').
card_uid('lotus petal'/'TPR', 'TPR:Lotus Petal:lotus petal').
card_rarity('lotus petal'/'TPR', 'Uncommon').
card_artist('lotus petal'/'TPR', 'April Lee').
card_number('lotus petal'/'TPR', '225').
card_flavor_text('lotus petal'/'TPR', '\"Hard to imagine,\" mused Hanna, stroking the petal, \"such a lovely flower inspiring such greed.\"').
card_multiverse_id('lotus petal'/'TPR', '397468').

card_in_set('lowland basilisk', 'TPR').
card_original_type('lowland basilisk'/'TPR', 'Creature — Basilisk').
card_original_text('lowland basilisk'/'TPR', 'Whenever Lowland Basilisk deals damage to a creature, destroy that creature at end of combat.').
card_image_name('lowland basilisk'/'TPR', 'lowland basilisk').
card_uid('lowland basilisk'/'TPR', 'TPR:Lowland Basilisk:lowland basilisk').
card_rarity('lowland basilisk'/'TPR', 'Uncommon').
card_artist('lowland basilisk'/'TPR', 'Randy Gallegos').
card_number('lowland basilisk'/'TPR', '178').
card_flavor_text('lowland basilisk'/'TPR', 'Unlike their cousins, Rathi basilisks turn their victims into puddles of flowstone.').
card_multiverse_id('lowland basilisk'/'TPR', '397398').

card_in_set('lowland giant', 'TPR').
card_original_type('lowland giant'/'TPR', 'Creature — Giant').
card_original_text('lowland giant'/'TPR', '').
card_image_name('lowland giant'/'TPR', 'lowland giant').
card_uid('lowland giant'/'TPR', 'TPR:Lowland Giant:lowland giant').
card_rarity('lowland giant'/'TPR', 'Common').
card_artist('lowland giant'/'TPR', 'Paolo Parente').
card_number('lowland giant'/'TPR', '139').
card_flavor_text('lowland giant'/'TPR', '\"Faugh!\" snorted Tahngarth. \"Why would it make a meal of something like you?\" Squee looked relieved. \"No,\" he continued, \"you\'d make a much better toothpick.\"').
card_multiverse_id('lowland giant'/'TPR', '397572').

card_in_set('mage il-vec', 'TPR').
card_original_type('mage il-vec'/'TPR', 'Creature — Human Wizard').
card_original_text('mage il-vec'/'TPR', '{T}, Discard a card at random: Mage il-Vec deals 1 damage to target creature or player.').
card_image_name('mage il-vec'/'TPR', 'mage il-vec').
card_uid('mage il-vec'/'TPR', 'TPR:Mage il-Vec:mage il-vec').
card_rarity('mage il-vec'/'TPR', 'Uncommon').
card_artist('mage il-vec'/'TPR', 'John Matson').
card_number('mage il-vec'/'TPR', '140').
card_flavor_text('mage il-vec'/'TPR', 'Living below a flowstone foundry produces a disproportionate number of fire mages among the il.').
card_multiverse_id('mage il-vec'/'TPR', '397599').

card_in_set('magmasaur', 'TPR').
card_original_type('magmasaur'/'TPR', 'Creature — Elemental Lizard').
card_original_text('magmasaur'/'TPR', 'Magmasaur enters the battlefield with five +1/+1 counters on it.\nAt the beginning of your upkeep, you may remove a +1/+1 counter from Magmasaur. If you don\'t, sacrifice Magmasaur and it deals damage equal to the number of +1/+1 counters on it to each creature without flying and each player.').
card_image_name('magmasaur'/'TPR', 'magmasaur').
card_uid('magmasaur'/'TPR', 'TPR:Magmasaur:magmasaur').
card_rarity('magmasaur'/'TPR', 'Rare').
card_artist('magmasaur'/'TPR', 'Daniel Gelon').
card_number('magmasaur'/'TPR', '141').
card_multiverse_id('magmasaur'/'TPR', '397434').

card_in_set('mana leak', 'TPR').
card_original_type('mana leak'/'TPR', 'Instant').
card_original_text('mana leak'/'TPR', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'TPR', 'mana leak').
card_uid('mana leak'/'TPR', 'TPR:Mana Leak:mana leak').
card_rarity('mana leak'/'TPR', 'Common').
card_artist('mana leak'/'TPR', 'Christopher Rush').
card_number('mana leak'/'TPR', '58').
card_flavor_text('mana leak'/'TPR', '\"The fatal flaw in every plan is the assumption that you know more than your enemy.\"\n—Volrath').
card_multiverse_id('mana leak'/'TPR', '397471').

card_in_set('manabond', 'TPR').
card_original_type('manabond'/'TPR', 'Enchantment').
card_original_text('manabond'/'TPR', 'At the beginning of your end step, you may reveal your hand and put all land cards from it onto the battlefield. If you do, discard your hand.').
card_image_name('manabond'/'TPR', 'manabond').
card_uid('manabond'/'TPR', 'TPR:Manabond:manabond').
card_rarity('manabond'/'TPR', 'Rare').
card_artist('manabond'/'TPR', 'Stephen Daniele').
card_number('manabond'/'TPR', '179').
card_multiverse_id('manabond'/'TPR', '397461').

card_in_set('maniacal rage', 'TPR').
card_original_type('maniacal rage'/'TPR', 'Enchantment — Aura').
card_original_text('maniacal rage'/'TPR', 'Enchant creature\nEnchanted creature gets +2/+2 and can\'t block.').
card_image_name('maniacal rage'/'TPR', 'maniacal rage').
card_uid('maniacal rage'/'TPR', 'TPR:Maniacal Rage:maniacal rage').
card_rarity('maniacal rage'/'TPR', 'Common').
card_artist('maniacal rage'/'TPR', 'Pete Venters').
card_number('maniacal rage'/'TPR', '142').
card_flavor_text('maniacal rage'/'TPR', 'Only by sacrificing any semblance of defense could Gerrard best Greven.').
card_multiverse_id('maniacal rage'/'TPR', '397568').

card_in_set('master decoy', 'TPR').
card_original_type('master decoy'/'TPR', 'Creature — Human Soldier').
card_original_text('master decoy'/'TPR', '{W}, {T}: Tap target creature.').
card_image_name('master decoy'/'TPR', 'master decoy').
card_uid('master decoy'/'TPR', 'TPR:Master Decoy:master decoy').
card_rarity('master decoy'/'TPR', 'Common').
card_artist('master decoy'/'TPR', 'Phil Foglio').
card_number('master decoy'/'TPR', '18').
card_flavor_text('master decoy'/'TPR', '\"A skilled decoy can throw your enemies off your trail. A master decoy can survive to do it again.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('master decoy'/'TPR', '397588').

card_in_set('mawcor', 'TPR').
card_original_type('mawcor'/'TPR', 'Creature — Beast').
card_original_text('mawcor'/'TPR', 'Flying\n{T}: Mawcor deals 1 damage to target creature or player.').
card_image_name('mawcor'/'TPR', 'mawcor').
card_uid('mawcor'/'TPR', 'TPR:Mawcor:mawcor').
card_rarity('mawcor'/'TPR', 'Rare').
card_artist('mawcor'/'TPR', 'John Matson').
card_number('mawcor'/'TPR', '59').
card_flavor_text('mawcor'/'TPR', 'From its maw comes neither word nor whisper—only wind.').
card_multiverse_id('mawcor'/'TPR', '397511').

card_in_set('maze of shadows', 'TPR').
card_original_type('maze of shadows'/'TPR', 'Land').
card_original_text('maze of shadows'/'TPR', '{T}: Add {1} to your mana pool.\n{T}: Untap target attacking creature with shadow. Prevent all combat damage that would be dealt to and dealt by that creature this turn.').
card_image_name('maze of shadows'/'TPR', 'maze of shadows').
card_uid('maze of shadows'/'TPR', 'TPR:Maze of Shadows:maze of shadows').
card_rarity('maze of shadows'/'TPR', 'Uncommon').
card_artist('maze of shadows'/'TPR', 'D. Alexander Gregory').
card_number('maze of shadows'/'TPR', '238').
card_multiverse_id('maze of shadows'/'TPR', '397445').

card_in_set('meditate', 'TPR').
card_original_type('meditate'/'TPR', 'Instant').
card_original_text('meditate'/'TPR', 'Draw four cards. You skip your next turn.').
card_image_name('meditate'/'TPR', 'meditate').
card_uid('meditate'/'TPR', 'TPR:Meditate:meditate').
card_rarity('meditate'/'TPR', 'Rare').
card_artist('meditate'/'TPR', 'Susan Van Camp').
card_number('meditate'/'TPR', '60').
card_flavor_text('meditate'/'TPR', '\"Part of me believes that Barrin taught me meditation simply to shut me up.\"\n—Ertai, wizard adept').
card_multiverse_id('meditate'/'TPR', '397394').

card_in_set('merfolk looter', 'TPR').
card_original_type('merfolk looter'/'TPR', 'Creature — Merfolk Rogue').
card_original_text('merfolk looter'/'TPR', '{T}: Draw a card, then discard a card.').
card_image_name('merfolk looter'/'TPR', 'merfolk looter').
card_uid('merfolk looter'/'TPR', 'TPR:Merfolk Looter:merfolk looter').
card_rarity('merfolk looter'/'TPR', 'Common').
card_artist('merfolk looter'/'TPR', 'Ron Spencer').
card_number('merfolk looter'/'TPR', '61').
card_flavor_text('merfolk looter'/'TPR', 'In the depths of Rootwater, salvage and sewage differ only in texture.').
card_multiverse_id('merfolk looter'/'TPR', '397427').

card_in_set('metallic sliver', 'TPR').
card_original_type('metallic sliver'/'TPR', 'Artifact Creature — Sliver').
card_original_text('metallic sliver'/'TPR', '').
card_image_name('metallic sliver'/'TPR', 'metallic sliver').
card_uid('metallic sliver'/'TPR', 'TPR:Metallic Sliver:metallic sliver').
card_rarity('metallic sliver'/'TPR', 'Common').
card_artist('metallic sliver'/'TPR', 'Allen Williams').
card_number('metallic sliver'/'TPR', '226').
card_flavor_text('metallic sliver'/'TPR', 'When the clever counterfeit was accepted by the hive, Volrath\'s influence upon the slivers grew even stronger.').
card_multiverse_id('metallic sliver'/'TPR', '397569').

card_in_set('mindless automaton', 'TPR').
card_original_type('mindless automaton'/'TPR', 'Artifact Creature — Construct').
card_original_text('mindless automaton'/'TPR', 'Mindless Automaton enters the battlefield with two +1/+1 counters on it.\n{1}, Discard a card: Put a +1/+1 counter on Mindless Automaton.\nRemove two +1/+1 counters from Mindless Automaton: Draw a card.').
card_image_name('mindless automaton'/'TPR', 'mindless automaton').
card_uid('mindless automaton'/'TPR', 'TPR:Mindless Automaton:mindless automaton').
card_rarity('mindless automaton'/'TPR', 'Rare').
card_artist('mindless automaton'/'TPR', 'Brian Snõddy').
card_number('mindless automaton'/'TPR', '227').
card_multiverse_id('mindless automaton'/'TPR', '397449').

card_in_set('mirri, cat warrior', 'TPR').
card_original_type('mirri, cat warrior'/'TPR', 'Legendary Creature — Cat Warrior').
card_original_text('mirri, cat warrior'/'TPR', 'First strike, forestwalk, vigilance').
card_image_name('mirri, cat warrior'/'TPR', 'mirri, cat warrior').
card_uid('mirri, cat warrior'/'TPR', 'TPR:Mirri, Cat Warrior:mirri, cat warrior').
card_rarity('mirri, cat warrior'/'TPR', 'Rare').
card_artist('mirri, cat warrior'/'TPR', 'Daren Bader').
card_number('mirri, cat warrior'/'TPR', '180').
card_flavor_text('mirri, cat warrior'/'TPR', '\"Full of beauty and grace, with a predator\'s instincts . . . Although she wanders, I have always thought Mirri belongs in Llanowar most of all.\"\n—Rofellos, Llanowar emissary').
card_multiverse_id('mirri, cat warrior'/'TPR', '397392').

card_in_set('mnemonic sliver', 'TPR').
card_original_type('mnemonic sliver'/'TPR', 'Creature — Sliver').
card_original_text('mnemonic sliver'/'TPR', 'All Slivers have \"{2}, Sacrifice this permanent: Draw a card.\"').
card_image_name('mnemonic sliver'/'TPR', 'mnemonic sliver').
card_uid('mnemonic sliver'/'TPR', 'TPR:Mnemonic Sliver:mnemonic sliver').
card_rarity('mnemonic sliver'/'TPR', 'Common').
card_artist('mnemonic sliver'/'TPR', 'Randy Gallegos').
card_number('mnemonic sliver'/'TPR', '62').
card_flavor_text('mnemonic sliver'/'TPR', '\"A jigsaw puzzle that lives, breeds, and thinks.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('mnemonic sliver'/'TPR', '397643').

card_in_set('mogg conscripts', 'TPR').
card_original_type('mogg conscripts'/'TPR', 'Creature — Goblin').
card_original_text('mogg conscripts'/'TPR', 'Mogg Conscripts can\'t attack unless you\'ve cast a creature spell this turn.').
card_image_name('mogg conscripts'/'TPR', 'mogg conscripts').
card_uid('mogg conscripts'/'TPR', 'TPR:Mogg Conscripts:mogg conscripts').
card_rarity('mogg conscripts'/'TPR', 'Common').
card_artist('mogg conscripts'/'TPR', 'Pete Venters').
card_number('mogg conscripts'/'TPR', '143').
card_flavor_text('mogg conscripts'/'TPR', '\"Torahn\'s horns! They\'ve seen us. Now, statue, you must fight to save yourself!\"\n—Tahngarth, aboard the Predator').
card_multiverse_id('mogg conscripts'/'TPR', '397404').

card_in_set('mogg fanatic', 'TPR').
card_original_type('mogg fanatic'/'TPR', 'Creature — Goblin').
card_original_text('mogg fanatic'/'TPR', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_image_name('mogg fanatic'/'TPR', 'mogg fanatic').
card_uid('mogg fanatic'/'TPR', 'TPR:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'TPR', 'Common').
card_artist('mogg fanatic'/'TPR', 'Brom').
card_number('mogg fanatic'/'TPR', '144').
card_flavor_text('mogg fanatic'/'TPR', '\"I got it! I got it! I—\"').
card_multiverse_id('mogg fanatic'/'TPR', '397498').

card_in_set('mogg flunkies', 'TPR').
card_original_type('mogg flunkies'/'TPR', 'Creature — Goblin').
card_original_text('mogg flunkies'/'TPR', 'Mogg Flunkies can\'t attack or block alone.').
card_image_name('mogg flunkies'/'TPR', 'mogg flunkies').
card_uid('mogg flunkies'/'TPR', 'TPR:Mogg Flunkies:mogg flunkies').
card_rarity('mogg flunkies'/'TPR', 'Common').
card_artist('mogg flunkies'/'TPR', 'Brom').
card_number('mogg flunkies'/'TPR', '145').
card_flavor_text('mogg flunkies'/'TPR', 'They\'ll attack whatever\'s in front of them—as long as you tell them where that is.').
card_multiverse_id('mogg flunkies'/'TPR', '397534').

card_in_set('mogg hollows', 'TPR').
card_original_type('mogg hollows'/'TPR', 'Land').
card_original_text('mogg hollows'/'TPR', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Mogg Hollows doesn\'t untap during your next untap step.').
card_image_name('mogg hollows'/'TPR', 'mogg hollows').
card_uid('mogg hollows'/'TPR', 'TPR:Mogg Hollows:mogg hollows').
card_rarity('mogg hollows'/'TPR', 'Uncommon').
card_artist('mogg hollows'/'TPR', 'Jeff Laubenstein').
card_number('mogg hollows'/'TPR', '239').
card_multiverse_id('mogg hollows'/'TPR', '397459').

card_in_set('mogg infestation', 'TPR').
card_original_type('mogg infestation'/'TPR', 'Sorcery').
card_original_text('mogg infestation'/'TPR', 'Destroy all creatures target player controls. For each creature that died this way, put two 1/1 red Goblin creature tokens onto the battlefield under that player\'s control.').
card_image_name('mogg infestation'/'TPR', 'mogg infestation').
card_uid('mogg infestation'/'TPR', 'TPR:Mogg Infestation:mogg infestation').
card_rarity('mogg infestation'/'TPR', 'Rare').
card_artist('mogg infestation'/'TPR', 'Pete Venters').
card_number('mogg infestation'/'TPR', '146').
card_multiverse_id('mogg infestation'/'TPR', '397418').

card_in_set('mogg maniac', 'TPR').
card_original_type('mogg maniac'/'TPR', 'Creature — Goblin').
card_original_text('mogg maniac'/'TPR', 'Whenever Mogg Maniac is dealt damage, it deals that much damage to target opponent.').
card_image_name('mogg maniac'/'TPR', 'mogg maniac').
card_uid('mogg maniac'/'TPR', 'TPR:Mogg Maniac:mogg maniac').
card_rarity('mogg maniac'/'TPR', 'Uncommon').
card_artist('mogg maniac'/'TPR', 'Brian Snõddy').
card_number('mogg maniac'/'TPR', '147').
card_flavor_text('mogg maniac'/'TPR', 'Stand clear if he gets an itch.').
card_multiverse_id('mogg maniac'/'TPR', '397480').

card_in_set('mountain', 'TPR').
card_original_type('mountain'/'TPR', 'Basic Land — Mountain').
card_original_text('mountain'/'TPR', 'R').
card_image_name('mountain'/'TPR', 'mountain1').
card_uid('mountain'/'TPR', 'TPR:Mountain:mountain1').
card_rarity('mountain'/'TPR', 'Basic Land').
card_artist('mountain'/'TPR', 'Mark Poole').
card_number('mountain'/'TPR', '262').
card_multiverse_id('mountain'/'TPR', '397605').

card_in_set('mountain', 'TPR').
card_original_type('mountain'/'TPR', 'Basic Land — Mountain').
card_original_text('mountain'/'TPR', 'R').
card_image_name('mountain'/'TPR', 'mountain2').
card_uid('mountain'/'TPR', 'TPR:Mountain:mountain2').
card_rarity('mountain'/'TPR', 'Basic Land').
card_artist('mountain'/'TPR', 'Mark Poole').
card_number('mountain'/'TPR', '263').
card_multiverse_id('mountain'/'TPR', '397561').

card_in_set('mountain', 'TPR').
card_original_type('mountain'/'TPR', 'Basic Land — Mountain').
card_original_text('mountain'/'TPR', 'R').
card_image_name('mountain'/'TPR', 'mountain3').
card_uid('mountain'/'TPR', 'TPR:Mountain:mountain3').
card_rarity('mountain'/'TPR', 'Basic Land').
card_artist('mountain'/'TPR', 'Mark Poole').
card_number('mountain'/'TPR', '264').
card_multiverse_id('mountain'/'TPR', '397546').

card_in_set('mountain', 'TPR').
card_original_type('mountain'/'TPR', 'Basic Land — Mountain').
card_original_text('mountain'/'TPR', 'R').
card_image_name('mountain'/'TPR', 'mountain4').
card_uid('mountain'/'TPR', 'TPR:Mountain:mountain4').
card_rarity('mountain'/'TPR', 'Basic Land').
card_artist('mountain'/'TPR', 'Mark Poole').
card_number('mountain'/'TPR', '265').
card_multiverse_id('mountain'/'TPR', '397597').

card_in_set('mounted archers', 'TPR').
card_original_type('mounted archers'/'TPR', 'Creature — Human Soldier Archer').
card_original_text('mounted archers'/'TPR', 'Reach (This creature can block creatures with flying.)\n{W}: Mounted Archers can block an additional creature this turn.').
card_image_name('mounted archers'/'TPR', 'mounted archers').
card_uid('mounted archers'/'TPR', 'TPR:Mounted Archers:mounted archers').
card_rarity('mounted archers'/'TPR', 'Common').
card_artist('mounted archers'/'TPR', 'Kev Walker').
card_number('mounted archers'/'TPR', '19').
card_multiverse_id('mounted archers'/'TPR', '397510').

card_in_set('mox diamond', 'TPR').
card_original_type('mox diamond'/'TPR', 'Artifact').
card_original_text('mox diamond'/'TPR', 'If Mox Diamond would enter the battlefield, you may discard a land card instead. If you do, put Mox Diamond onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('mox diamond'/'TPR', 'mox diamond').
card_uid('mox diamond'/'TPR', 'TPR:Mox Diamond:mox diamond').
card_rarity('mox diamond'/'TPR', 'Mythic Rare').
card_artist('mox diamond'/'TPR', 'Dan Frazier').
card_number('mox diamond'/'TPR', '228').
card_multiverse_id('mox diamond'/'TPR', '397482').

card_in_set('mulch', 'TPR').
card_original_type('mulch'/'TPR', 'Sorcery').
card_original_text('mulch'/'TPR', 'Reveal the top four cards of your library. Put all land cards revealed this way into your hand and the rest into your graveyard.').
card_image_name('mulch'/'TPR', 'mulch').
card_uid('mulch'/'TPR', 'TPR:Mulch:mulch').
card_rarity('mulch'/'TPR', 'Common').
card_artist('mulch'/'TPR', 'Rebecca Guay').
card_number('mulch'/'TPR', '181').
card_flavor_text('mulch'/'TPR', 'Hope is the one crop that can grow in any climate.').
card_multiverse_id('mulch'/'TPR', '397563').

card_in_set('muscle sliver', 'TPR').
card_original_type('muscle sliver'/'TPR', 'Creature — Sliver').
card_original_text('muscle sliver'/'TPR', 'All Sliver creatures get +1/+1.').
card_image_name('muscle sliver'/'TPR', 'muscle sliver').
card_uid('muscle sliver'/'TPR', 'TPR:Muscle Sliver:muscle sliver').
card_rarity('muscle sliver'/'TPR', 'Common').
card_artist('muscle sliver'/'TPR', 'Richard Kane Ferguson').
card_number('muscle sliver'/'TPR', '182').
card_flavor_text('muscle sliver'/'TPR', 'The air was filled with the cracks and snaps of flesh hardening as the new sliver joined the battle.').
card_multiverse_id('muscle sliver'/'TPR', '397442').

card_in_set('necrologia', 'TPR').
card_original_type('necrologia'/'TPR', 'Instant').
card_original_text('necrologia'/'TPR', 'Cast Necrologia only during your end step.\nAs an additional cost to cast Necrologia, pay X life.\nDraw X cards.').
card_image_name('necrologia'/'TPR', 'necrologia').
card_uid('necrologia'/'TPR', 'TPR:Necrologia:necrologia').
card_rarity('necrologia'/'TPR', 'Rare').
card_artist('necrologia'/'TPR', 'Brom').
card_number('necrologia'/'TPR', '110').
card_flavor_text('necrologia'/'TPR', '\"My enemies\' death yields threefold benefit: removal, reuse, and research.\"\n—Volrath').
card_multiverse_id('necrologia'/'TPR', '397595').

card_in_set('needle storm', 'TPR').
card_original_type('needle storm'/'TPR', 'Sorcery').
card_original_text('needle storm'/'TPR', 'Needle Storm deals 4 damage to each creature with flying.').
card_image_name('needle storm'/'TPR', 'needle storm').
card_uid('needle storm'/'TPR', 'TPR:Needle Storm:needle storm').
card_rarity('needle storm'/'TPR', 'Uncommon').
card_artist('needle storm'/'TPR', 'Val Mayerik').
card_number('needle storm'/'TPR', '183').
card_flavor_text('needle storm'/'TPR', 'The rain is driven like nails.').
card_multiverse_id('needle storm'/'TPR', '397499').

card_in_set('nomads en-kor', 'TPR').
card_original_type('nomads en-kor'/'TPR', 'Creature — Kor Nomad Soldier').
card_original_text('nomads en-kor'/'TPR', '{0}: The next 1 damage that would be dealt to Nomads en-Kor this turn is dealt to target creature you control instead.').
card_image_name('nomads en-kor'/'TPR', 'nomads en-kor').
card_uid('nomads en-kor'/'TPR', 'TPR:Nomads en-Kor:nomads en-kor').
card_rarity('nomads en-kor'/'TPR', 'Common').
card_artist('nomads en-kor'/'TPR', 'Val Mayerik').
card_number('nomads en-kor'/'TPR', '20').
card_flavor_text('nomads en-kor'/'TPR', 'The kor forsake roots for the winding of the path, forsake voices for the silence of the mind, forsake all else for the poverty of isolation.').
card_multiverse_id('nomads en-kor'/'TPR', '397520').

card_in_set('oath of druids', 'TPR').
card_original_type('oath of druids'/'TPR', 'Enchantment').
card_original_text('oath of druids'/'TPR', 'At the beginning of each player\'s upkeep, that player chooses target player who controls more creatures than he or she does and is his or her opponent. The first player may reveal cards from the top of his or her library until he or she reveals a creature card. If he or she does, that player puts that card onto the battlefield and all other cards revealed this way into his or her graveyard.').
card_image_name('oath of druids'/'TPR', 'oath of druids').
card_uid('oath of druids'/'TPR', 'TPR:Oath of Druids:oath of druids').
card_rarity('oath of druids'/'TPR', 'Mythic Rare').
card_artist('oath of druids'/'TPR', 'Daren Bader').
card_number('oath of druids'/'TPR', '184').
card_multiverse_id('oath of druids'/'TPR', '397400').

card_in_set('ogre shaman', 'TPR').
card_original_type('ogre shaman'/'TPR', 'Creature — Ogre Shaman').
card_original_text('ogre shaman'/'TPR', '{2}, Discard a card at random: Ogre Shaman deals 2 damage to target creature or player.').
card_image_name('ogre shaman'/'TPR', 'ogre shaman').
card_uid('ogre shaman'/'TPR', 'TPR:Ogre Shaman:ogre shaman').
card_rarity('ogre shaman'/'TPR', 'Rare').
card_artist('ogre shaman'/'TPR', 'Paolo Parente').
card_number('ogre shaman'/'TPR', '148').
card_flavor_text('ogre shaman'/'TPR', 'Ogre shamans must be bright enough to learn their invocations and dim enough to use them.').
card_multiverse_id('ogre shaman'/'TPR', '397465').

card_in_set('orim, samite healer', 'TPR').
card_original_type('orim, samite healer'/'TPR', 'Legendary Creature — Human Cleric').
card_original_text('orim, samite healer'/'TPR', '{T}: Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_image_name('orim, samite healer'/'TPR', 'orim, samite healer').
card_uid('orim, samite healer'/'TPR', 'TPR:Orim, Samite Healer:orim, samite healer').
card_rarity('orim, samite healer'/'TPR', 'Rare').
card_artist('orim, samite healer'/'TPR', 'Kaja Foglio').
card_number('orim, samite healer'/'TPR', '21').
card_flavor_text('orim, samite healer'/'TPR', '\"The silkworm spins itself a new existence. So the healer weaves the threads of life.\"\n—Orim, Samite healer').
card_multiverse_id('orim, samite healer'/'TPR', '397460').

card_in_set('overrun', 'TPR').
card_original_type('overrun'/'TPR', 'Sorcery').
card_original_text('overrun'/'TPR', 'Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('overrun'/'TPR', 'overrun').
card_uid('overrun'/'TPR', 'TPR:Overrun:overrun').
card_rarity('overrun'/'TPR', 'Uncommon').
card_artist('overrun'/'TPR', 'Jeff Miracola').
card_number('overrun'/'TPR', '185').
card_flavor_text('overrun'/'TPR', '\"You want to know your enemy? Look at your feet while you trample him.\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('overrun'/'TPR', '397574').

card_in_set('pacifism', 'TPR').
card_original_type('pacifism'/'TPR', 'Enchantment — Aura').
card_original_text('pacifism'/'TPR', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'TPR', 'pacifism').
card_uid('pacifism'/'TPR', 'TPR:Pacifism:pacifism').
card_rarity('pacifism'/'TPR', 'Uncommon').
card_artist('pacifism'/'TPR', 'Adam Rex').
card_number('pacifism'/'TPR', '22').
card_flavor_text('pacifism'/'TPR', 'Frozen by conscience, Karn did not resist as the moggs carried him to the Predator.').
card_multiverse_id('pacifism'/'TPR', '397528').

card_in_set('paladin en-vec', 'TPR').
card_original_type('paladin en-vec'/'TPR', 'Creature — Human Knight').
card_original_text('paladin en-vec'/'TPR', 'First strike, protection from black and from red').
card_image_name('paladin en-vec'/'TPR', 'paladin en-vec').
card_uid('paladin en-vec'/'TPR', 'TPR:Paladin en-Vec:paladin en-vec').
card_rarity('paladin en-vec'/'TPR', 'Rare').
card_artist('paladin en-vec'/'TPR', 'Randy Elliott').
card_number('paladin en-vec'/'TPR', '23').
card_flavor_text('paladin en-vec'/'TPR', '\"Our belief shall be the lance that pierces Volrath\'s heart.\"').
card_multiverse_id('paladin en-vec'/'TPR', '397600').

card_in_set('pandemonium', 'TPR').
card_original_type('pandemonium'/'TPR', 'Enchantment').
card_original_text('pandemonium'/'TPR', 'Whenever a creature enters the battlefield, that creature\'s controller may have it deal damage equal to its power to target creature or player of his or her choice.').
card_image_name('pandemonium'/'TPR', 'pandemonium').
card_uid('pandemonium'/'TPR', 'TPR:Pandemonium:pandemonium').
card_rarity('pandemonium'/'TPR', 'Rare').
card_artist('pandemonium'/'TPR', 'Pete Venters').
card_number('pandemonium'/'TPR', '149').
card_flavor_text('pandemonium'/'TPR', '\"If we cannot live proudly, we die so!\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('pandemonium'/'TPR', '397575').

card_in_set('patchwork gnomes', 'TPR').
card_original_type('patchwork gnomes'/'TPR', 'Artifact Creature — Gnome').
card_original_text('patchwork gnomes'/'TPR', 'Discard a card: Regenerate Patchwork Gnomes.').
card_image_name('patchwork gnomes'/'TPR', 'patchwork gnomes').
card_uid('patchwork gnomes'/'TPR', 'TPR:Patchwork Gnomes:patchwork gnomes').
card_rarity('patchwork gnomes'/'TPR', 'Common').
card_artist('patchwork gnomes'/'TPR', 'Mike Raabe').
card_number('patchwork gnomes'/'TPR', '229').
card_flavor_text('patchwork gnomes'/'TPR', '\"Just when I get them working, they always start fighting and tearing at each other for spare parts.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('patchwork gnomes'/'TPR', '397408').

card_in_set('pegasus stampede', 'TPR').
card_original_type('pegasus stampede'/'TPR', 'Sorcery').
card_original_text('pegasus stampede'/'TPR', 'Buyback—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nPut a 1/1 white Pegasus creature token with flying onto the battlefield.').
card_image_name('pegasus stampede'/'TPR', 'pegasus stampede').
card_uid('pegasus stampede'/'TPR', 'TPR:Pegasus Stampede:pegasus stampede').
card_rarity('pegasus stampede'/'TPR', 'Uncommon').
card_artist('pegasus stampede'/'TPR', 'Mark Zug').
card_number('pegasus stampede'/'TPR', '24').
card_multiverse_id('pegasus stampede'/'TPR', '397430').

card_in_set('phyrexian hulk', 'TPR').
card_original_type('phyrexian hulk'/'TPR', 'Artifact Creature — Golem').
card_original_text('phyrexian hulk'/'TPR', '').
card_image_name('phyrexian hulk'/'TPR', 'phyrexian hulk').
card_uid('phyrexian hulk'/'TPR', 'TPR:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'TPR', 'Uncommon').
card_artist('phyrexian hulk'/'TPR', 'Matthew D. Wilson').
card_number('phyrexian hulk'/'TPR', '230').
card_flavor_text('phyrexian hulk'/'TPR', '\"They are convenient in having no souls, but less so in having no spirit.\"\n—Volrath').
card_multiverse_id('phyrexian hulk'/'TPR', '397571').

card_in_set('pine barrens', 'TPR').
card_original_type('pine barrens'/'TPR', 'Land').
card_original_text('pine barrens'/'TPR', 'Pine Barrens enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Pine Barrens deals 1 damage to you.').
card_image_name('pine barrens'/'TPR', 'pine barrens').
card_uid('pine barrens'/'TPR', 'TPR:Pine Barrens:pine barrens').
card_rarity('pine barrens'/'TPR', 'Uncommon').
card_artist('pine barrens'/'TPR', 'Rebecca Guay').
card_number('pine barrens'/'TPR', '240').
card_multiverse_id('pine barrens'/'TPR', '397425').

card_in_set('plains', 'TPR').
card_original_type('plains'/'TPR', 'Basic Land — Plains').
card_original_text('plains'/'TPR', 'W').
card_image_name('plains'/'TPR', 'plains1').
card_uid('plains'/'TPR', 'TPR:Plains:plains1').
card_rarity('plains'/'TPR', 'Basic Land').
card_artist('plains'/'TPR', 'Terese Nielsen').
card_number('plains'/'TPR', '250').
card_multiverse_id('plains'/'TPR', '397550').

card_in_set('plains', 'TPR').
card_original_type('plains'/'TPR', 'Basic Land — Plains').
card_original_text('plains'/'TPR', 'W').
card_image_name('plains'/'TPR', 'plains2').
card_uid('plains'/'TPR', 'TPR:Plains:plains2').
card_rarity('plains'/'TPR', 'Basic Land').
card_artist('plains'/'TPR', 'Terese Nielsen').
card_number('plains'/'TPR', '251').
card_multiverse_id('plains'/'TPR', '397469').

card_in_set('plains', 'TPR').
card_original_type('plains'/'TPR', 'Basic Land — Plains').
card_original_text('plains'/'TPR', 'W').
card_image_name('plains'/'TPR', 'plains3').
card_uid('plains'/'TPR', 'TPR:Plains:plains3').
card_rarity('plains'/'TPR', 'Basic Land').
card_artist('plains'/'TPR', 'Terese Nielsen').
card_number('plains'/'TPR', '252').
card_multiverse_id('plains'/'TPR', '397610').

card_in_set('plains', 'TPR').
card_original_type('plains'/'TPR', 'Basic Land — Plains').
card_original_text('plains'/'TPR', 'W').
card_image_name('plains'/'TPR', 'plains4').
card_uid('plains'/'TPR', 'TPR:Plains:plains4').
card_rarity('plains'/'TPR', 'Basic Land').
card_artist('plains'/'TPR', 'Terese Nielsen').
card_number('plains'/'TPR', '253').
card_multiverse_id('plains'/'TPR', '397433').

card_in_set('provoke', 'TPR').
card_original_type('provoke'/'TPR', 'Instant').
card_original_text('provoke'/'TPR', 'Untap target creature you don\'t control. That creature blocks this turn if able.\nDraw a card.').
card_image_name('provoke'/'TPR', 'provoke').
card_uid('provoke'/'TPR', 'TPR:Provoke:provoke').
card_rarity('provoke'/'TPR', 'Common').
card_artist('provoke'/'TPR', 'Terese Nielsen').
card_number('provoke'/'TPR', '186').
card_flavor_text('provoke'/'TPR', 'Mirri did not have time to think, only to react.').
card_multiverse_id('provoke'/'TPR', '397604').

card_in_set('rampant growth', 'TPR').
card_original_type('rampant growth'/'TPR', 'Sorcery').
card_original_text('rampant growth'/'TPR', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('rampant growth'/'TPR', 'rampant growth').
card_uid('rampant growth'/'TPR', 'TPR:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'TPR', 'Common').
card_artist('rampant growth'/'TPR', 'Tom Kyffin').
card_number('rampant growth'/'TPR', '187').
card_flavor_text('rampant growth'/'TPR', 'Rath forces plants from its surface as one presses whey from cheese.').
card_multiverse_id('rampant growth'/'TPR', '397623').

card_in_set('rathi dragon', 'TPR').
card_original_type('rathi dragon'/'TPR', 'Creature — Dragon').
card_original_text('rathi dragon'/'TPR', 'Flying\nWhen Rathi Dragon enters the battlefield, sacrifice it unless you sacrifice two Mountains.').
card_image_name('rathi dragon'/'TPR', 'rathi dragon').
card_uid('rathi dragon'/'TPR', 'TPR:Rathi Dragon:rathi dragon').
card_rarity('rathi dragon'/'TPR', 'Rare').
card_artist('rathi dragon'/'TPR', 'Christopher Rush').
card_number('rathi dragon'/'TPR', '150').
card_flavor_text('rathi dragon'/'TPR', 'Wrap the flame as twine,\nKingdoms will be thine.').
card_multiverse_id('rathi dragon'/'TPR', '397602').

card_in_set('rats of rath', 'TPR').
card_original_type('rats of rath'/'TPR', 'Creature — Rat').
card_original_text('rats of rath'/'TPR', '{B}: Destroy target artifact, creature, or land you control.').
card_image_name('rats of rath'/'TPR', 'rats of rath').
card_uid('rats of rath'/'TPR', 'TPR:Rats of Rath:rats of rath').
card_rarity('rats of rath'/'TPR', 'Common').
card_artist('rats of rath'/'TPR', 'John Matson').
card_number('rats of rath'/'TPR', '111').
card_flavor_text('rats of rath'/'TPR', '\"These creatures offer new challenges in the scrutiny of entrails.\"\n—Oracle en-Vec').
card_multiverse_id('rats of rath'/'TPR', '397611').

card_in_set('reality anchor', 'TPR').
card_original_type('reality anchor'/'TPR', 'Instant').
card_original_text('reality anchor'/'TPR', 'Target creature loses shadow until end of turn.\nDraw a card.').
card_image_name('reality anchor'/'TPR', 'reality anchor').
card_uid('reality anchor'/'TPR', 'TPR:Reality Anchor:reality anchor').
card_rarity('reality anchor'/'TPR', 'Common').
card_artist('reality anchor'/'TPR', 'Randy Gallegos').
card_number('reality anchor'/'TPR', '188').
card_flavor_text('reality anchor'/'TPR', '\"See how you like it.\"\n—Ertai, wizard adept').
card_multiverse_id('reality anchor'/'TPR', '397421').

card_in_set('reanimate', 'TPR').
card_original_type('reanimate'/'TPR', 'Sorcery').
card_original_text('reanimate'/'TPR', 'Put target creature card from a graveyard onto the battlefield under your control. You lose life equal to its converted mana cost.').
card_image_name('reanimate'/'TPR', 'reanimate').
card_uid('reanimate'/'TPR', 'TPR:Reanimate:reanimate').
card_rarity('reanimate'/'TPR', 'Uncommon').
card_artist('reanimate'/'TPR', 'Robert Bliss').
card_number('reanimate'/'TPR', '112').
card_flavor_text('reanimate'/'TPR', '\"You will learn to earn death.\"\n—Volrath').
card_multiverse_id('reanimate'/'TPR', '397423').

card_in_set('recurring nightmare', 'TPR').
card_original_type('recurring nightmare'/'TPR', 'Enchantment').
card_original_text('recurring nightmare'/'TPR', 'Sacrifice a creature, Return Recurring Nightmare to its owner\'s hand: Return target creature card from your graveyard to the battlefield. Activate this ability only any time you could cast a sorcery.').
card_image_name('recurring nightmare'/'TPR', 'recurring nightmare').
card_uid('recurring nightmare'/'TPR', 'TPR:Recurring Nightmare:recurring nightmare').
card_rarity('recurring nightmare'/'TPR', 'Mythic Rare').
card_artist('recurring nightmare'/'TPR', 'Jeff Laubenstein').
card_number('recurring nightmare'/'TPR', '113').
card_flavor_text('recurring nightmare'/'TPR', '\"I am confined by sleep and defined by nightmare.\"\n—Crovax').
card_multiverse_id('recurring nightmare'/'TPR', '397441').

card_in_set('recycle', 'TPR').
card_original_type('recycle'/'TPR', 'Enchantment').
card_original_text('recycle'/'TPR', 'Skip your draw step.\nWhenever you play a card, draw a card.\nYour maximum hand size is two. (You discard down to your maximum hand size as your turn ends.)').
card_image_name('recycle'/'TPR', 'recycle').
card_uid('recycle'/'TPR', 'TPR:Recycle:recycle').
card_rarity('recycle'/'TPR', 'Rare').
card_artist('recycle'/'TPR', 'Phil Foglio').
card_number('recycle'/'TPR', '189').
card_multiverse_id('recycle'/'TPR', '397573').

card_in_set('renegade warlord', 'TPR').
card_original_type('renegade warlord'/'TPR', 'Creature — Human Soldier').
card_original_text('renegade warlord'/'TPR', 'First strike\nWhenever Renegade Warlord attacks, each other attacking creature gets +1/+0 until end of turn.').
card_image_name('renegade warlord'/'TPR', 'renegade warlord').
card_uid('renegade warlord'/'TPR', 'TPR:Renegade Warlord:renegade warlord').
card_rarity('renegade warlord'/'TPR', 'Uncommon').
card_artist('renegade warlord'/'TPR', 'Ron Spencer').
card_number('renegade warlord'/'TPR', '151').
card_flavor_text('renegade warlord'/'TPR', '\"His tongue must be honey, the way he gathers ants to fight with him.\"\n—Volrath').
card_multiverse_id('renegade warlord'/'TPR', '397463').

card_in_set('repentance', 'TPR').
card_original_type('repentance'/'TPR', 'Sorcery').
card_original_text('repentance'/'TPR', 'Target creature deals damage to itself equal to its power.').
card_image_name('repentance'/'TPR', 'repentance').
card_uid('repentance'/'TPR', 'TPR:Repentance:repentance').
card_rarity('repentance'/'TPR', 'Uncommon').
card_artist('repentance'/'TPR', 'Ron Spencer').
card_number('repentance'/'TPR', '25').
card_flavor_text('repentance'/'TPR', '\"The cannon wasn\'t aimed at you!\" pleaded Vhati.\n\"I\'m not sure which is more pathetic,\" replied Greven, \"your judgment or your aim.\"').
card_multiverse_id('repentance'/'TPR', '397644').

card_in_set('revenant', 'TPR').
card_original_type('revenant'/'TPR', 'Creature — Spirit').
card_original_text('revenant'/'TPR', 'Flying\nRevenant\'s power and toughness are each equal to the number of creature cards in your graveyard.').
card_image_name('revenant'/'TPR', 'revenant').
card_uid('revenant'/'TPR', 'TPR:Revenant:revenant').
card_rarity('revenant'/'TPR', 'Uncommon').
card_artist('revenant'/'TPR', 'Terese Nielsen').
card_number('revenant'/'TPR', '114').
card_flavor_text('revenant'/'TPR', '\"Not again.\"\n—Hans').
card_multiverse_id('revenant'/'TPR', '397475').

card_in_set('rolling thunder', 'TPR').
card_original_type('rolling thunder'/'TPR', 'Sorcery').
card_original_text('rolling thunder'/'TPR', 'Rolling Thunder deals X damage divided as you choose among any number of target creatures and/or players.').
card_image_name('rolling thunder'/'TPR', 'rolling thunder').
card_uid('rolling thunder'/'TPR', 'TPR:Rolling Thunder:rolling thunder').
card_rarity('rolling thunder'/'TPR', 'Uncommon').
card_artist('rolling thunder'/'TPR', 'Richard Thomas').
card_number('rolling thunder'/'TPR', '152').
card_flavor_text('rolling thunder'/'TPR', '\"Such rage,\" thought Vhati, gazing up at the thunderhead from the Predator. \"It is Greven\'s mind manifest.\"').
card_multiverse_id('rolling thunder'/'TPR', '397419').

card_in_set('rootbreaker wurm', 'TPR').
card_original_type('rootbreaker wurm'/'TPR', 'Creature — Wurm').
card_original_text('rootbreaker wurm'/'TPR', 'Trample').
card_image_name('rootbreaker wurm'/'TPR', 'rootbreaker wurm').
card_uid('rootbreaker wurm'/'TPR', 'TPR:Rootbreaker Wurm:rootbreaker wurm').
card_rarity('rootbreaker wurm'/'TPR', 'Uncommon').
card_artist('rootbreaker wurm'/'TPR', 'Richard Kane Ferguson').
card_number('rootbreaker wurm'/'TPR', '190').
card_flavor_text('rootbreaker wurm'/'TPR', 'As Gerrard made his escape, the wurm covered his flight by helping itself to three great mouthfuls of merfolk.').
card_multiverse_id('rootbreaker wurm'/'TPR', '397567').

card_in_set('rootwalla', 'TPR').
card_original_type('rootwalla'/'TPR', 'Creature — Lizard').
card_original_text('rootwalla'/'TPR', '{1}{G}: Rootwalla gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_image_name('rootwalla'/'TPR', 'rootwalla').
card_uid('rootwalla'/'TPR', 'TPR:Rootwalla:rootwalla').
card_rarity('rootwalla'/'TPR', 'Common').
card_artist('rootwalla'/'TPR', 'Roger Raupp').
card_number('rootwalla'/'TPR', '191').
card_flavor_text('rootwalla'/'TPR', 'If you try to sneak up on a rootwalla, you\'ll suddenly find yourself dealing with twice the lizard.').
card_multiverse_id('rootwalla'/'TPR', '397580').

card_in_set('rootwater depths', 'TPR').
card_original_type('rootwater depths'/'TPR', 'Land').
card_original_text('rootwater depths'/'TPR', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Rootwater Depths doesn\'t untap during your next untap step.').
card_image_name('rootwater depths'/'TPR', 'rootwater depths').
card_uid('rootwater depths'/'TPR', 'TPR:Rootwater Depths:rootwater depths').
card_rarity('rootwater depths'/'TPR', 'Uncommon').
card_artist('rootwater depths'/'TPR', 'Roger Raupp').
card_number('rootwater depths'/'TPR', '241').
card_multiverse_id('rootwater depths'/'TPR', '397536').

card_in_set('rootwater hunter', 'TPR').
card_original_type('rootwater hunter'/'TPR', 'Creature — Merfolk').
card_original_text('rootwater hunter'/'TPR', '{T}: Rootwater Hunter deals 1 damage to target creature or player.').
card_image_name('rootwater hunter'/'TPR', 'rootwater hunter').
card_uid('rootwater hunter'/'TPR', 'TPR:Rootwater Hunter:rootwater hunter').
card_rarity('rootwater hunter'/'TPR', 'Uncommon').
card_artist('rootwater hunter'/'TPR', 'Brom').
card_number('rootwater hunter'/'TPR', '63').
card_flavor_text('rootwater hunter'/'TPR', '\"Bitter water, vicious wave;\nShadow-cold shallows, root-made maze.\nHome\'s angry embrace.\"\n—Rootwater Saga').
card_multiverse_id('rootwater hunter'/'TPR', '397607').

card_in_set('sabertooth wyvern', 'TPR').
card_original_type('sabertooth wyvern'/'TPR', 'Creature — Drake').
card_original_text('sabertooth wyvern'/'TPR', 'Flying, first strike').
card_image_name('sabertooth wyvern'/'TPR', 'sabertooth wyvern').
card_uid('sabertooth wyvern'/'TPR', 'TPR:Sabertooth Wyvern:sabertooth wyvern').
card_rarity('sabertooth wyvern'/'TPR', 'Uncommon').
card_artist('sabertooth wyvern'/'TPR', 'Keith Parkinson').
card_number('sabertooth wyvern'/'TPR', '153').
card_flavor_text('sabertooth wyvern'/'TPR', 'Most creatures grow out of their baby teeth, but sabertooth wyverns grow into theirs.').
card_multiverse_id('sabertooth wyvern'/'TPR', '397628').

card_in_set('salt flats', 'TPR').
card_original_type('salt flats'/'TPR', 'Land').
card_original_text('salt flats'/'TPR', 'Salt Flats enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Salt Flats deals 1 damage to you.').
card_image_name('salt flats'/'TPR', 'salt flats').
card_uid('salt flats'/'TPR', 'TPR:Salt Flats:salt flats').
card_rarity('salt flats'/'TPR', 'Uncommon').
card_artist('salt flats'/'TPR', 'Scott Kirschner').
card_number('salt flats'/'TPR', '242').
card_multiverse_id('salt flats'/'TPR', '397590').

card_in_set('sandstone warrior', 'TPR').
card_original_type('sandstone warrior'/'TPR', 'Creature — Human Soldier Warrior').
card_original_text('sandstone warrior'/'TPR', 'First strike\n{R}: Sandstone Warrior gets +1/+0 until end of turn.').
card_image_name('sandstone warrior'/'TPR', 'sandstone warrior').
card_uid('sandstone warrior'/'TPR', 'TPR:Sandstone Warrior:sandstone warrior').
card_rarity('sandstone warrior'/'TPR', 'Common').
card_artist('sandstone warrior'/'TPR', 'Stephen Daniele').
card_number('sandstone warrior'/'TPR', '154').
card_flavor_text('sandstone warrior'/'TPR', '\"I used to describe something stable as ‘rock solid.\' So much for that expression.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('sandstone warrior'/'TPR', '397464').

card_in_set('sarcomancy', 'TPR').
card_original_type('sarcomancy'/'TPR', 'Enchantment').
card_original_text('sarcomancy'/'TPR', 'When Sarcomancy enters the battlefield, put a 2/2 black Zombie creature token onto the battlefield.\nAt the beginning of your upkeep, if there are no Zombies on the battlefield, Sarcomancy deals 1 damage to you.').
card_image_name('sarcomancy'/'TPR', 'sarcomancy').
card_uid('sarcomancy'/'TPR', 'TPR:Sarcomancy:sarcomancy').
card_rarity('sarcomancy'/'TPR', 'Uncommon').
card_artist('sarcomancy'/'TPR', 'Daren Bader').
card_number('sarcomancy'/'TPR', '115').
card_multiverse_id('sarcomancy'/'TPR', '397586').

card_in_set('scabland', 'TPR').
card_original_type('scabland'/'TPR', 'Land').
card_original_text('scabland'/'TPR', 'Scabland enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Scabland deals 1 damage to you.').
card_image_name('scabland'/'TPR', 'scabland').
card_uid('scabland'/'TPR', 'TPR:Scabland:scabland').
card_rarity('scabland'/'TPR', 'Uncommon').
card_artist('scabland'/'TPR', 'Andrew Robinson').
card_number('scabland'/'TPR', '243').
card_multiverse_id('scabland'/'TPR', '397438').

card_in_set('screeching harpy', 'TPR').
card_original_type('screeching harpy'/'TPR', 'Creature — Harpy Beast').
card_original_text('screeching harpy'/'TPR', 'Flying\n{1}{B}: Regenerate Screeching Harpy.').
card_image_name('screeching harpy'/'TPR', 'screeching harpy').
card_uid('screeching harpy'/'TPR', 'TPR:Screeching Harpy:screeching harpy').
card_rarity('screeching harpy'/'TPR', 'Uncommon').
card_artist('screeching harpy'/'TPR', 'Una Fricker').
card_number('screeching harpy'/'TPR', '116').
card_flavor_text('screeching harpy'/'TPR', '\"They are called ‘fowl\' for a reason.\"\n—Mirri of the Weatherlight').
card_multiverse_id('screeching harpy'/'TPR', '397507').

card_in_set('scrivener', 'TPR').
card_original_type('scrivener'/'TPR', 'Creature — Human Wizard').
card_original_text('scrivener'/'TPR', 'When Scrivener enters the battlefield, you may return target instant card from your graveyard to your hand.').
card_image_name('scrivener'/'TPR', 'scrivener').
card_uid('scrivener'/'TPR', 'TPR:Scrivener:scrivener').
card_rarity('scrivener'/'TPR', 'Common').
card_artist('scrivener'/'TPR', 'Heather Hudson').
card_number('scrivener'/'TPR', '64').
card_flavor_text('scrivener'/'TPR', '\"History is a potent weapon.\"\n—Karn, silver golem').
card_multiverse_id('scrivener'/'TPR', '397587').

card_in_set('sea monster', 'TPR').
card_original_type('sea monster'/'TPR', 'Creature — Serpent').
card_original_text('sea monster'/'TPR', 'Sea Monster can\'t attack unless defending player controls an Island.').
card_image_name('sea monster'/'TPR', 'sea monster').
card_uid('sea monster'/'TPR', 'TPR:Sea Monster:sea monster').
card_rarity('sea monster'/'TPR', 'Common').
card_artist('sea monster'/'TPR', 'Daniel Gelon').
card_number('sea monster'/'TPR', '65').
card_flavor_text('sea monster'/'TPR', 'Water is life. Or perhaps it is just something to wash it down with.').
card_multiverse_id('sea monster'/'TPR', '397516').

card_in_set('searing touch', 'TPR').
card_original_type('searing touch'/'TPR', 'Instant').
card_original_text('searing touch'/'TPR', 'Buyback {4} (You may pay an additional {4} as you cast this spell. If you do, put this card into your hand as it resolves.)\nSearing Touch deals 1 damage to target creature or player.').
card_image_name('searing touch'/'TPR', 'searing touch').
card_uid('searing touch'/'TPR', 'TPR:Searing Touch:searing touch').
card_rarity('searing touch'/'TPR', 'Uncommon').
card_artist('searing touch'/'TPR', 'D. Alexander Gregory').
card_number('searing touch'/'TPR', '155').
card_multiverse_id('searing touch'/'TPR', '397530').

card_in_set('seething anger', 'TPR').
card_original_type('seething anger'/'TPR', 'Sorcery').
card_original_text('seething anger'/'TPR', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget creature gets +3/+0 until end of turn.').
card_image_name('seething anger'/'TPR', 'seething anger').
card_uid('seething anger'/'TPR', 'TPR:Seething Anger:seething anger').
card_rarity('seething anger'/'TPR', 'Common').
card_artist('seething anger'/'TPR', 'Val Mayerik').
card_number('seething anger'/'TPR', '156').
card_multiverse_id('seething anger'/'TPR', '397651').

card_in_set('selenia, dark angel', 'TPR').
card_original_type('selenia, dark angel'/'TPR', 'Legendary Creature — Angel').
card_original_text('selenia, dark angel'/'TPR', 'Flying\nPay 2 life: Return Selenia, Dark Angel to its owner\'s hand.').
card_image_name('selenia, dark angel'/'TPR', 'selenia, dark angel').
card_uid('selenia, dark angel'/'TPR', 'TPR:Selenia, Dark Angel:selenia, dark angel').
card_rarity('selenia, dark angel'/'TPR', 'Rare').
card_artist('selenia, dark angel'/'TPR', 'Matthew D. Wilson').
card_number('selenia, dark angel'/'TPR', '210').
card_flavor_text('selenia, dark angel'/'TPR', '\"I am light. I am dark. I must give my life to serve; not even death can release me.\"').
card_multiverse_id('selenia, dark angel'/'TPR', '397639').

card_in_set('serpent warrior', 'TPR').
card_original_type('serpent warrior'/'TPR', 'Creature — Snake Warrior').
card_original_text('serpent warrior'/'TPR', 'When Serpent Warrior enters the battlefield, you lose 3 life.').
card_image_name('serpent warrior'/'TPR', 'serpent warrior').
card_uid('serpent warrior'/'TPR', 'TPR:Serpent Warrior:serpent warrior').
card_rarity('serpent warrior'/'TPR', 'Common').
card_artist('serpent warrior'/'TPR', 'Ron Spencer').
card_number('serpent warrior'/'TPR', '117').
card_flavor_text('serpent warrior'/'TPR', 'A hiss before dying.').
card_multiverse_id('serpent warrior'/'TPR', '397652').

card_in_set('shackles', 'TPR').
card_original_type('shackles'/'TPR', 'Enchantment — Aura').
card_original_text('shackles'/'TPR', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\n{W}: Return Shackles to its owner\'s hand.').
card_image_name('shackles'/'TPR', 'shackles').
card_uid('shackles'/'TPR', 'TPR:Shackles:shackles').
card_rarity('shackles'/'TPR', 'Common').
card_artist('shackles'/'TPR', 'Heather Hudson').
card_number('shackles'/'TPR', '26').
card_flavor_text('shackles'/'TPR', 'Shackles of gold are still shackles.').
card_multiverse_id('shackles'/'TPR', '397403').

card_in_set('shadow rift', 'TPR').
card_original_type('shadow rift'/'TPR', 'Instant').
card_original_text('shadow rift'/'TPR', 'Target creature gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)\nDraw a card.').
card_image_name('shadow rift'/'TPR', 'shadow rift').
card_uid('shadow rift'/'TPR', 'TPR:Shadow Rift:shadow rift').
card_rarity('shadow rift'/'TPR', 'Common').
card_artist('shadow rift'/'TPR', 'Adam Rex').
card_number('shadow rift'/'TPR', '66').
card_multiverse_id('shadow rift'/'TPR', '397405').

card_in_set('shadowstorm', 'TPR').
card_original_type('shadowstorm'/'TPR', 'Sorcery').
card_original_text('shadowstorm'/'TPR', 'Shadowstorm deals 2 damage to each creature with shadow.').
card_image_name('shadowstorm'/'TPR', 'shadowstorm').
card_uid('shadowstorm'/'TPR', 'TPR:Shadowstorm:shadowstorm').
card_rarity('shadowstorm'/'TPR', 'Uncommon').
card_artist('shadowstorm'/'TPR', 'Adam Rex').
card_number('shadowstorm'/'TPR', '157').
card_flavor_text('shadowstorm'/'TPR', '\"You\'ve not seen a storm until you\'ve seen the kind that roars between worlds.\"\n—Lyna, soltari emissary').
card_multiverse_id('shadowstorm'/'TPR', '397537').

card_in_set('shaman en-kor', 'TPR').
card_original_type('shaman en-kor'/'TPR', 'Creature — Kor Cleric Shaman').
card_original_text('shaman en-kor'/'TPR', '{0}: The next 1 damage that would be dealt to Shaman en-Kor this turn is dealt to target creature you control instead.\n{1}{W}: The next time a source of your choice would deal damage to target creature this turn, that damage is dealt to Shaman en-Kor instead.').
card_image_name('shaman en-kor'/'TPR', 'shaman en-kor').
card_uid('shaman en-kor'/'TPR', 'TPR:Shaman en-Kor:shaman en-kor').
card_rarity('shaman en-kor'/'TPR', 'Rare').
card_artist('shaman en-kor'/'TPR', 'Jeff Miracola').
card_number('shaman en-kor'/'TPR', '27').
card_multiverse_id('shaman en-kor'/'TPR', '397648').

card_in_set('shard phoenix', 'TPR').
card_original_type('shard phoenix'/'TPR', 'Creature — Phoenix').
card_original_text('shard phoenix'/'TPR', 'Flying\nSacrifice Shard Phoenix: Shard Phoenix deals 2 damage to each creature without flying.\n{R}{R}{R}: Return Shard Phoenix from your graveyard to your hand. Activate this ability only during your upkeep.').
card_image_name('shard phoenix'/'TPR', 'shard phoenix').
card_uid('shard phoenix'/'TPR', 'TPR:Shard Phoenix:shard phoenix').
card_rarity('shard phoenix'/'TPR', 'Mythic Rare').
card_artist('shard phoenix'/'TPR', 'Paolo Parente').
card_number('shard phoenix'/'TPR', '158').
card_multiverse_id('shard phoenix'/'TPR', '397414').

card_in_set('shatter', 'TPR').
card_original_type('shatter'/'TPR', 'Instant').
card_original_text('shatter'/'TPR', 'Destroy target artifact.').
card_image_name('shatter'/'TPR', 'shatter').
card_uid('shatter'/'TPR', 'TPR:Shatter:shatter').
card_rarity('shatter'/'TPR', 'Common').
card_artist('shatter'/'TPR', 'Jason Alexander Behnke').
card_number('shatter'/'TPR', '159').
card_flavor_text('shatter'/'TPR', '\"Such fuss over machines, Gerrard. Remember that you are the prize of the Legacy. Throw away your toys!\"\n—Orim, Samite healer').
card_multiverse_id('shatter'/'TPR', '397429').

card_in_set('sift', 'TPR').
card_original_type('sift'/'TPR', 'Sorcery').
card_original_text('sift'/'TPR', 'Draw three cards, then discard a card.').
card_image_name('sift'/'TPR', 'sift').
card_uid('sift'/'TPR', 'TPR:Sift:sift').
card_rarity('sift'/'TPR', 'Common').
card_artist('sift'/'TPR', 'Pete Venters').
card_number('sift'/'TPR', '67').
card_flavor_text('sift'/'TPR', '\"Twice I have let the Legacy slip away. Never again.\"\n—Karn, silver golem').
card_multiverse_id('sift'/'TPR', '397514').

card_in_set('silver wyvern', 'TPR').
card_original_type('silver wyvern'/'TPR', 'Creature — Drake').
card_original_text('silver wyvern'/'TPR', 'Flying\n{U}: Change the target of target spell or ability that targets only Silver Wyvern. The new target must be a creature.').
card_image_name('silver wyvern'/'TPR', 'silver wyvern').
card_uid('silver wyvern'/'TPR', 'TPR:Silver Wyvern:silver wyvern').
card_rarity('silver wyvern'/'TPR', 'Rare').
card_artist('silver wyvern'/'TPR', 'Colin MacNeil').
card_number('silver wyvern'/'TPR', '68').
card_multiverse_id('silver wyvern'/'TPR', '397484').

card_in_set('skyshaper', 'TPR').
card_original_type('skyshaper'/'TPR', 'Artifact').
card_original_text('skyshaper'/'TPR', 'Sacrifice Skyshaper: Creatures you control gain flying until end of turn.').
card_image_name('skyshaper'/'TPR', 'skyshaper').
card_uid('skyshaper'/'TPR', 'TPR:Skyshaper:skyshaper').
card_rarity('skyshaper'/'TPR', 'Common').
card_artist('skyshaper'/'TPR', 'Donato Giancola').
card_number('skyshaper'/'TPR', '231').
card_flavor_text('skyshaper'/'TPR', '\"It\'ll get us to the portal, but I can\'t guarantee what it\'ll do to the ship.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('skyshaper'/'TPR', '397424').

card_in_set('skyshroud elf', 'TPR').
card_original_type('skyshroud elf'/'TPR', 'Creature — Elf Druid').
card_original_text('skyshroud elf'/'TPR', '{T}: Add {G} to your mana pool.\n{1}: Add {R} or {W} to your mana pool.').
card_image_name('skyshroud elf'/'TPR', 'skyshroud elf').
card_uid('skyshroud elf'/'TPR', 'TPR:Skyshroud Elf:skyshroud elf').
card_rarity('skyshroud elf'/'TPR', 'Common').
card_artist('skyshroud elf'/'TPR', 'Jeff Miracola').
card_number('skyshroud elf'/'TPR', '192').
card_flavor_text('skyshroud elf'/'TPR', '\"It is our duty to endure.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('skyshroud elf'/'TPR', '397435').

card_in_set('skyshroud forest', 'TPR').
card_original_type('skyshroud forest'/'TPR', 'Land').
card_original_text('skyshroud forest'/'TPR', 'Skyshroud Forest enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {G} or {U} to your mana pool. Skyshroud Forest deals 1 damage to you.').
card_image_name('skyshroud forest'/'TPR', 'skyshroud forest').
card_uid('skyshroud forest'/'TPR', 'TPR:Skyshroud Forest:skyshroud forest').
card_rarity('skyshroud forest'/'TPR', 'Uncommon').
card_artist('skyshroud forest'/'TPR', 'Roger Raupp').
card_number('skyshroud forest'/'TPR', '244').
card_multiverse_id('skyshroud forest'/'TPR', '397616').

card_in_set('skyshroud troll', 'TPR').
card_original_type('skyshroud troll'/'TPR', 'Creature — Troll Giant').
card_original_text('skyshroud troll'/'TPR', '{1}{G}: Regenerate Skyshroud Troll.').
card_image_name('skyshroud troll'/'TPR', 'skyshroud troll').
card_uid('skyshroud troll'/'TPR', 'TPR:Skyshroud Troll:skyshroud troll').
card_rarity('skyshroud troll'/'TPR', 'Common').
card_artist('skyshroud troll'/'TPR', 'Matthew D. Wilson').
card_number('skyshroud troll'/'TPR', '193').
card_flavor_text('skyshroud troll'/'TPR', 'The elves and merfolk have nothing but bitterness for each other. The trolls, however, find them both rather tasty.').
card_multiverse_id('skyshroud troll'/'TPR', '397589').

card_in_set('skyshroud vampire', 'TPR').
card_original_type('skyshroud vampire'/'TPR', 'Creature — Vampire').
card_original_text('skyshroud vampire'/'TPR', 'Flying\nDiscard a creature card: Skyshroud Vampire gets +2/+2 until end of turn.').
card_image_name('skyshroud vampire'/'TPR', 'skyshroud vampire').
card_uid('skyshroud vampire'/'TPR', 'TPR:Skyshroud Vampire:skyshroud vampire').
card_rarity('skyshroud vampire'/'TPR', 'Uncommon').
card_artist('skyshroud vampire'/'TPR', 'Gary Leach').
card_number('skyshroud vampire'/'TPR', '118').
card_flavor_text('skyshroud vampire'/'TPR', '\"If it tastes one drop of elvish blood I will cast it from the shroud to see it burn.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('skyshroud vampire'/'TPR', '397655').

card_in_set('sliver queen', 'TPR').
card_original_type('sliver queen'/'TPR', 'Legendary Creature — Sliver').
card_original_text('sliver queen'/'TPR', '{2}: Put a 1/1 colorless Sliver creature token onto the battlefield.').
card_image_name('sliver queen'/'TPR', 'sliver queen').
card_uid('sliver queen'/'TPR', 'TPR:Sliver Queen:sliver queen').
card_rarity('sliver queen'/'TPR', 'Mythic Rare').
card_artist('sliver queen'/'TPR', 'Ron Spencer').
card_number('sliver queen'/'TPR', '211').
card_flavor_text('sliver queen'/'TPR', 'Her children are ever part of her.').
card_multiverse_id('sliver queen'/'TPR', '397585').

card_in_set('smite', 'TPR').
card_original_type('smite'/'TPR', 'Instant').
card_original_text('smite'/'TPR', 'Destroy target blocked creature.').
card_image_name('smite'/'TPR', 'smite').
card_uid('smite'/'TPR', 'TPR:Smite:smite').
card_rarity('smite'/'TPR', 'Common').
card_artist('smite'/'TPR', 'Daren Bader').
card_number('smite'/'TPR', '28').
card_flavor_text('smite'/'TPR', '\"You\'ve got your childhood wish at last. Now you get to die.\"\n—Gerrard, to Volrath').
card_multiverse_id('smite'/'TPR', '397527').

card_in_set('soltari champion', 'TPR').
card_original_type('soltari champion'/'TPR', 'Creature — Soltari Soldier').
card_original_text('soltari champion'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Soltari Champion attacks, other creatures you control get +1/+1 until end of turn.').
card_image_name('soltari champion'/'TPR', 'soltari champion').
card_uid('soltari champion'/'TPR', 'TPR:Soltari Champion:soltari champion').
card_rarity('soltari champion'/'TPR', 'Uncommon').
card_artist('soltari champion'/'TPR', 'Adam Rex').
card_number('soltari champion'/'TPR', '29').
card_multiverse_id('soltari champion'/'TPR', '397487').

card_in_set('soltari guerrillas', 'TPR').
card_original_type('soltari guerrillas'/'TPR', 'Creature — Soltari Soldier').
card_original_text('soltari guerrillas'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{0}: The next time Soltari Guerrillas would deal combat damage to an opponent this turn, it deals that damage to target creature instead.').
card_image_name('soltari guerrillas'/'TPR', 'soltari guerrillas').
card_uid('soltari guerrillas'/'TPR', 'TPR:Soltari Guerrillas:soltari guerrillas').
card_rarity('soltari guerrillas'/'TPR', 'Rare').
card_artist('soltari guerrillas'/'TPR', 'Val Mayerik').
card_number('soltari guerrillas'/'TPR', '212').
card_multiverse_id('soltari guerrillas'/'TPR', '397650').

card_in_set('soltari lancer', 'TPR').
card_original_type('soltari lancer'/'TPR', 'Creature — Soltari Knight').
card_original_text('soltari lancer'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nSoltari Lancer has first strike as long as it\'s attacking.').
card_image_name('soltari lancer'/'TPR', 'soltari lancer').
card_uid('soltari lancer'/'TPR', 'TPR:Soltari Lancer:soltari lancer').
card_rarity('soltari lancer'/'TPR', 'Common').
card_artist('soltari lancer'/'TPR', 'Matthew D. Wilson').
card_number('soltari lancer'/'TPR', '30').
card_flavor_text('soltari lancer'/'TPR', '\"In times of war the victors rarely save their best for last.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('soltari lancer'/'TPR', '397483').

card_in_set('soltari monk', 'TPR').
card_original_type('soltari monk'/'TPR', 'Creature — Soltari Monk Cleric').
card_original_text('soltari monk'/'TPR', 'Protection from black\nShadow (This creature can block or be blocked by only creatures with shadow.)').
card_image_name('soltari monk'/'TPR', 'soltari monk').
card_uid('soltari monk'/'TPR', 'TPR:Soltari Monk:soltari monk').
card_rarity('soltari monk'/'TPR', 'Uncommon').
card_artist('soltari monk'/'TPR', 'Janet Aulisio').
card_number('soltari monk'/'TPR', '31').
card_flavor_text('soltari monk'/'TPR', '\"Prayer rarely explains.\"\n—Orim, Samite healer').
card_multiverse_id('soltari monk'/'TPR', '397592').

card_in_set('soltari priest', 'TPR').
card_original_type('soltari priest'/'TPR', 'Creature — Soltari Cleric').
card_original_text('soltari priest'/'TPR', 'Protection from red\nShadow (This creature can block or be blocked by only creatures with shadow.)').
card_image_name('soltari priest'/'TPR', 'soltari priest').
card_uid('soltari priest'/'TPR', 'TPR:Soltari Priest:soltari priest').
card_rarity('soltari priest'/'TPR', 'Uncommon').
card_artist('soltari priest'/'TPR', 'Janet Aulisio').
card_number('soltari priest'/'TPR', '32').
card_flavor_text('soltari priest'/'TPR', '\"In Rath,\" the priest said, \"there is even greater need for prayer.\"').
card_multiverse_id('soltari priest'/'TPR', '397657').

card_in_set('soltari trooper', 'TPR').
card_original_type('soltari trooper'/'TPR', 'Creature — Soltari Soldier').
card_original_text('soltari trooper'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Soltari Trooper attacks, it gets +1/+1 until end of turn.').
card_image_name('soltari trooper'/'TPR', 'soltari trooper').
card_uid('soltari trooper'/'TPR', 'TPR:Soltari Trooper:soltari trooper').
card_rarity('soltari trooper'/'TPR', 'Common').
card_artist('soltari trooper'/'TPR', 'Kev Walker').
card_number('soltari trooper'/'TPR', '33').
card_flavor_text('soltari trooper'/'TPR', '\"Dauthi blood is soltari wine.\"\n—Soltari Tales of Life').
card_multiverse_id('soltari trooper'/'TPR', '397452').

card_in_set('spell blast', 'TPR').
card_original_type('spell blast'/'TPR', 'Instant').
card_original_text('spell blast'/'TPR', 'Counter target spell with converted mana cost X.').
card_image_name('spell blast'/'TPR', 'spell blast').
card_uid('spell blast'/'TPR', 'TPR:Spell Blast:spell blast').
card_rarity('spell blast'/'TPR', 'Common').
card_artist('spell blast'/'TPR', 'Steve Luke').
card_number('spell blast'/'TPR', '69').
card_flavor_text('spell blast'/'TPR', 'Call it the thinking mage\'s version of brute force.').
card_multiverse_id('spell blast'/'TPR', '397501').

card_in_set('spellshock', 'TPR').
card_original_type('spellshock'/'TPR', 'Enchantment').
card_original_text('spellshock'/'TPR', 'Whenever a player casts a spell, Spellshock deals 2 damage to that player.').
card_image_name('spellshock'/'TPR', 'spellshock').
card_uid('spellshock'/'TPR', 'TPR:Spellshock:spellshock').
card_rarity('spellshock'/'TPR', 'Uncommon').
card_artist('spellshock'/'TPR', 'Thomas M. Baxa').
card_number('spellshock'/'TPR', '160').
card_flavor_text('spellshock'/'TPR', 'A snap of fingers, a snap of teeth.').
card_multiverse_id('spellshock'/'TPR', '397489').

card_in_set('spike breeder', 'TPR').
card_original_type('spike breeder'/'TPR', 'Creature — Spike').
card_original_text('spike breeder'/'TPR', 'Spike Breeder enters the battlefield with three +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Breeder: Put a +1/+1 counter on target creature.\n{2}, Remove a +1/+1 counter from Spike Breeder: Put a 1/1 green Spike creature token onto the battlefield.').
card_image_name('spike breeder'/'TPR', 'spike breeder').
card_uid('spike breeder'/'TPR', 'TPR:Spike Breeder:spike breeder').
card_rarity('spike breeder'/'TPR', 'Uncommon').
card_artist('spike breeder'/'TPR', 'Adam Rex').
card_number('spike breeder'/'TPR', '194').
card_multiverse_id('spike breeder'/'TPR', '397406').

card_in_set('spike colony', 'TPR').
card_original_type('spike colony'/'TPR', 'Creature — Spike').
card_original_text('spike colony'/'TPR', 'Spike Colony enters the battlefield with four +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Colony: Put a +1/+1 counter on target creature.').
card_image_name('spike colony'/'TPR', 'spike colony').
card_uid('spike colony'/'TPR', 'TPR:Spike Colony:spike colony').
card_rarity('spike colony'/'TPR', 'Common').
card_artist('spike colony'/'TPR', 'Douglas Shuler').
card_number('spike colony'/'TPR', '195').
card_multiverse_id('spike colony'/'TPR', '397457').

card_in_set('spike feeder', 'TPR').
card_original_type('spike feeder'/'TPR', 'Creature — Spike').
card_original_text('spike feeder'/'TPR', 'Spike Feeder enters the battlefield with two +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Feeder: Put a +1/+1 counter on target creature.\nRemove a +1/+1 counter from Spike Feeder: You gain 2 life.').
card_image_name('spike feeder'/'TPR', 'spike feeder').
card_uid('spike feeder'/'TPR', 'TPR:Spike Feeder:spike feeder').
card_rarity('spike feeder'/'TPR', 'Uncommon').
card_artist('spike feeder'/'TPR', 'Heather Hudson').
card_number('spike feeder'/'TPR', '196').
card_multiverse_id('spike feeder'/'TPR', '397486').

card_in_set('spike hatcher', 'TPR').
card_original_type('spike hatcher'/'TPR', 'Creature — Spike').
card_original_text('spike hatcher'/'TPR', 'Spike Hatcher enters the battlefield with six +1/+1 counters on it.\n{2}, Remove a +1/+1 counter from Spike Hatcher: Put a +1/+1 counter on target creature.\n{1}, Remove a +1/+1 counter from Spike Hatcher: Regenerate Spike Hatcher.').
card_image_name('spike hatcher'/'TPR', 'spike hatcher').
card_uid('spike hatcher'/'TPR', 'TPR:Spike Hatcher:spike hatcher').
card_rarity('spike hatcher'/'TPR', 'Rare').
card_artist('spike hatcher'/'TPR', 'Stephen Daniele').
card_number('spike hatcher'/'TPR', '197').
card_multiverse_id('spike hatcher'/'TPR', '397591').

card_in_set('spinal graft', 'TPR').
card_original_type('spinal graft'/'TPR', 'Enchantment — Aura').
card_original_text('spinal graft'/'TPR', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature becomes the target of a spell or ability, destroy that creature. It can\'t be regenerated.').
card_image_name('spinal graft'/'TPR', 'spinal graft').
card_uid('spinal graft'/'TPR', 'TPR:Spinal Graft:spinal graft').
card_rarity('spinal graft'/'TPR', 'Common').
card_artist('spinal graft'/'TPR', 'Ron Spencer').
card_number('spinal graft'/'TPR', '119').
card_multiverse_id('spinal graft'/'TPR', '397609').

card_in_set('spined sliver', 'TPR').
card_original_type('spined sliver'/'TPR', 'Creature — Sliver').
card_original_text('spined sliver'/'TPR', 'Whenever a Sliver becomes blocked, that Sliver gets +1/+1 until end of turn for each creature blocking it.').
card_image_name('spined sliver'/'TPR', 'spined sliver').
card_uid('spined sliver'/'TPR', 'TPR:Spined Sliver:spined sliver').
card_rarity('spined sliver'/'TPR', 'Uncommon').
card_artist('spined sliver'/'TPR', 'Ron Spencer').
card_number('spined sliver'/'TPR', '213').
card_flavor_text('spined sliver'/'TPR', '\"Slivers are evil and slivers are sly;\nAnd if you get eaten, then no one will cry.\"\n—Mogg children\'s rhyme').
card_multiverse_id('spined sliver'/'TPR', '397578').

card_in_set('spined wurm', 'TPR').
card_original_type('spined wurm'/'TPR', 'Creature — Wurm').
card_original_text('spined wurm'/'TPR', '').
card_image_name('spined wurm'/'TPR', 'spined wurm').
card_uid('spined wurm'/'TPR', 'TPR:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'TPR', 'Common').
card_artist('spined wurm'/'TPR', 'Keith Parkinson').
card_number('spined wurm'/'TPR', '198').
card_flavor_text('spined wurm'/'TPR', '\"As it moved, the wurm\'s spines gathered up bits of flowstone, which took the shapes of dead villagers\' heads. Each head spoke a single sound, but if taken together, they said, ‘Alas for the living.\'\"\n—Dal myth of the wurm').
card_multiverse_id('spined wurm'/'TPR', '397454').

card_in_set('spirit en-kor', 'TPR').
card_original_type('spirit en-kor'/'TPR', 'Creature — Kor Spirit').
card_original_text('spirit en-kor'/'TPR', 'Flying\n{0}: The next 1 damage that would be dealt to Spirit en-Kor this turn is dealt to target creature you control instead.').
card_image_name('spirit en-kor'/'TPR', 'spirit en-kor').
card_uid('spirit en-kor'/'TPR', 'TPR:Spirit en-Kor:spirit en-kor').
card_rarity('spirit en-kor'/'TPR', 'Common').
card_artist('spirit en-kor'/'TPR', 'John Matson').
card_number('spirit en-kor'/'TPR', '34').
card_flavor_text('spirit en-kor'/'TPR', 'Death free throat from thirst, mouth from speech, feet from earth.\n—Kor requiem').
card_multiverse_id('spirit en-kor'/'TPR', '397638').

card_in_set('spirit mirror', 'TPR').
card_original_type('spirit mirror'/'TPR', 'Enchantment').
card_original_text('spirit mirror'/'TPR', 'At the beginning of your upkeep, if there are no Reflection tokens on the battlefield, put a 2/2 white Reflection creature token onto the battlefield.\n{0}: Destroy target Reflection.').
card_image_name('spirit mirror'/'TPR', 'spirit mirror').
card_uid('spirit mirror'/'TPR', 'TPR:Spirit Mirror:spirit mirror').
card_rarity('spirit mirror'/'TPR', 'Rare').
card_artist('spirit mirror'/'TPR', 'D. Alexander Gregory').
card_number('spirit mirror'/'TPR', '35').
card_multiverse_id('spirit mirror'/'TPR', '397646').

card_in_set('spitting hydra', 'TPR').
card_original_type('spitting hydra'/'TPR', 'Creature — Hydra').
card_original_text('spitting hydra'/'TPR', 'Spitting Hydra enters the battlefield with four +1/+1 counters on it.\n{1}{R}, Remove a +1/+1 counter from Spitting Hydra: Spitting Hydra deals 1 damage to target creature.').
card_image_name('spitting hydra'/'TPR', 'spitting hydra').
card_uid('spitting hydra'/'TPR', 'TPR:Spitting Hydra:spitting hydra').
card_rarity('spitting hydra'/'TPR', 'Rare').
card_artist('spitting hydra'/'TPR', 'Daren Bader').
card_number('spitting hydra'/'TPR', '161').
card_multiverse_id('spitting hydra'/'TPR', '397642').

card_in_set('stalking stones', 'TPR').
card_original_type('stalking stones'/'TPR', 'Land').
card_original_text('stalking stones'/'TPR', '{T}: Add {1} to your mana pool.\n{6}: Stalking Stones becomes a 3/3 Elemental artifact creature that\'s still a land.').
card_image_name('stalking stones'/'TPR', 'stalking stones').
card_uid('stalking stones'/'TPR', 'TPR:Stalking Stones:stalking stones').
card_rarity('stalking stones'/'TPR', 'Uncommon').
card_artist('stalking stones'/'TPR', 'Stephen Daniele').
card_number('stalking stones'/'TPR', '245').
card_multiverse_id('stalking stones'/'TPR', '397410').

card_in_set('standing troops', 'TPR').
card_original_type('standing troops'/'TPR', 'Creature — Human Soldier').
card_original_text('standing troops'/'TPR', 'Vigilance').
card_image_name('standing troops'/'TPR', 'standing troops').
card_uid('standing troops'/'TPR', 'TPR:Standing Troops:standing troops').
card_rarity('standing troops'/'TPR', 'Common').
card_artist('standing troops'/'TPR', 'Daren Bader').
card_number('standing troops'/'TPR', '36').
card_flavor_text('standing troops'/'TPR', 'The less you have, the harder you fight for it.').
card_multiverse_id('standing troops'/'TPR', '397627').

card_in_set('starke of rath', 'TPR').
card_original_type('starke of rath'/'TPR', 'Legendary Creature — Human Rogue').
card_original_text('starke of rath'/'TPR', '{T}: Destroy target artifact or creature. That permanent\'s controller gains control of Starke of Rath.').
card_image_name('starke of rath'/'TPR', 'starke of rath').
card_uid('starke of rath'/'TPR', 'TPR:Starke of Rath:starke of rath').
card_rarity('starke of rath'/'TPR', 'Mythic Rare').
card_artist('starke of rath'/'TPR', 'Dan Frazier').
card_number('starke of rath'/'TPR', '162').
card_flavor_text('starke of rath'/'TPR', '\"I know to whom I owe the most loyalty, and I see him in the mirror every day.\"\n—Starke').
card_multiverse_id('starke of rath'/'TPR', '397432').

card_in_set('staunch defenders', 'TPR').
card_original_type('staunch defenders'/'TPR', 'Creature — Human Soldier').
card_original_text('staunch defenders'/'TPR', 'When Staunch Defenders enters the battlefield, you gain 4 life.').
card_image_name('staunch defenders'/'TPR', 'staunch defenders').
card_uid('staunch defenders'/'TPR', 'TPR:Staunch Defenders:staunch defenders').
card_rarity('staunch defenders'/'TPR', 'Common').
card_artist('staunch defenders'/'TPR', 'Mark Poole').
card_number('staunch defenders'/'TPR', '37').
card_flavor_text('staunch defenders'/'TPR', '\"Hold your position! Leave doubt for the dying!\"\n—Tahngarth of the Weatherlight').
card_multiverse_id('staunch defenders'/'TPR', '397552').

card_in_set('stronghold assassin', 'TPR').
card_original_type('stronghold assassin'/'TPR', 'Creature — Zombie Assassin').
card_original_text('stronghold assassin'/'TPR', '{T}, Sacrifice a creature: Destroy target nonblack creature.').
card_image_name('stronghold assassin'/'TPR', 'stronghold assassin').
card_uid('stronghold assassin'/'TPR', 'TPR:Stronghold Assassin:stronghold assassin').
card_rarity('stronghold assassin'/'TPR', 'Rare').
card_artist('stronghold assassin'/'TPR', 'Matthew D. Wilson').
card_number('stronghold assassin'/'TPR', '120').
card_flavor_text('stronghold assassin'/'TPR', 'The assassin sees only throats and hears only heartbeats.').
card_multiverse_id('stronghold assassin'/'TPR', '397450').

card_in_set('stun', 'TPR').
card_original_type('stun'/'TPR', 'Instant').
card_original_text('stun'/'TPR', 'Target creature can\'t block this turn.\nDraw a card.').
card_image_name('stun'/'TPR', 'stun').
card_uid('stun'/'TPR', 'TPR:Stun:stun').
card_rarity('stun'/'TPR', 'Common').
card_artist('stun'/'TPR', 'Terese Nielsen').
card_number('stun'/'TPR', '163').
card_flavor_text('stun'/'TPR', '\"I concede it was a cheap shot, but it was the only one I could afford.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('stun'/'TPR', '397466').

card_in_set('survival of the fittest', 'TPR').
card_original_type('survival of the fittest'/'TPR', 'Enchantment').
card_original_text('survival of the fittest'/'TPR', '{G}, Discard a creature card: Search your library for a creature card, reveal that card, and put it into your hand. Then shuffle your library.').
card_image_name('survival of the fittest'/'TPR', 'survival of the fittest').
card_uid('survival of the fittest'/'TPR', 'TPR:Survival of the Fittest:survival of the fittest').
card_rarity('survival of the fittest'/'TPR', 'Mythic Rare').
card_artist('survival of the fittest'/'TPR', 'Pete Venters').
card_number('survival of the fittest'/'TPR', '199').
card_multiverse_id('survival of the fittest'/'TPR', '397535').

card_in_set('swamp', 'TPR').
card_original_type('swamp'/'TPR', 'Basic Land — Swamp').
card_original_text('swamp'/'TPR', 'B').
card_image_name('swamp'/'TPR', 'swamp1').
card_uid('swamp'/'TPR', 'TPR:Swamp:swamp1').
card_rarity('swamp'/'TPR', 'Basic Land').
card_artist('swamp'/'TPR', 'Brom').
card_number('swamp'/'TPR', '258').
card_multiverse_id('swamp'/'TPR', '397529').

card_in_set('swamp', 'TPR').
card_original_type('swamp'/'TPR', 'Basic Land — Swamp').
card_original_text('swamp'/'TPR', 'B').
card_image_name('swamp'/'TPR', 'swamp2').
card_uid('swamp'/'TPR', 'TPR:Swamp:swamp2').
card_rarity('swamp'/'TPR', 'Basic Land').
card_artist('swamp'/'TPR', 'Brom').
card_number('swamp'/'TPR', '259').
card_multiverse_id('swamp'/'TPR', '397635').

card_in_set('swamp', 'TPR').
card_original_type('swamp'/'TPR', 'Basic Land — Swamp').
card_original_text('swamp'/'TPR', 'B').
card_image_name('swamp'/'TPR', 'swamp3').
card_uid('swamp'/'TPR', 'TPR:Swamp:swamp3').
card_rarity('swamp'/'TPR', 'Basic Land').
card_artist('swamp'/'TPR', 'Brom').
card_number('swamp'/'TPR', '260').
card_multiverse_id('swamp'/'TPR', '397426').

card_in_set('swamp', 'TPR').
card_original_type('swamp'/'TPR', 'Basic Land — Swamp').
card_original_text('swamp'/'TPR', 'B').
card_image_name('swamp'/'TPR', 'swamp4').
card_uid('swamp'/'TPR', 'TPR:Swamp:swamp4').
card_rarity('swamp'/'TPR', 'Basic Land').
card_artist('swamp'/'TPR', 'Brom').
card_number('swamp'/'TPR', '261').
card_multiverse_id('swamp'/'TPR', '397467').

card_in_set('telethopter', 'TPR').
card_original_type('telethopter'/'TPR', 'Artifact Creature — Thopter').
card_original_text('telethopter'/'TPR', 'Tap an untapped creature you control: Telethopter gains flying until end of turn.').
card_image_name('telethopter'/'TPR', 'telethopter').
card_uid('telethopter'/'TPR', 'TPR:Telethopter:telethopter').
card_rarity('telethopter'/'TPR', 'Common').
card_artist('telethopter'/'TPR', 'Thomas M. Baxa').
card_number('telethopter'/'TPR', '232').
card_flavor_text('telethopter'/'TPR', 'After losing several of the devices to midair collisions, Greven forbade moggs from operating telethopters.').
card_multiverse_id('telethopter'/'TPR', '397462').

card_in_set('thalakos drifters', 'TPR').
card_original_type('thalakos drifters'/'TPR', 'Creature — Thalakos').
card_original_text('thalakos drifters'/'TPR', 'Discard a card: Thalakos Drifters gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_image_name('thalakos drifters'/'TPR', 'thalakos drifters').
card_uid('thalakos drifters'/'TPR', 'TPR:Thalakos Drifters:thalakos drifters').
card_rarity('thalakos drifters'/'TPR', 'Uncommon').
card_artist('thalakos drifters'/'TPR', 'Andrew Robinson').
card_number('thalakos drifters'/'TPR', '70').
card_flavor_text('thalakos drifters'/'TPR', 'They drift in and out of shadow and ever on toward madness.').
card_multiverse_id('thalakos drifters'/'TPR', '397630').

card_in_set('thalakos lowlands', 'TPR').
card_original_type('thalakos lowlands'/'TPR', 'Land').
card_original_text('thalakos lowlands'/'TPR', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Thalakos Lowlands doesn\'t untap during your next untap step.').
card_image_name('thalakos lowlands'/'TPR', 'thalakos lowlands').
card_uid('thalakos lowlands'/'TPR', 'TPR:Thalakos Lowlands:thalakos lowlands').
card_rarity('thalakos lowlands'/'TPR', 'Uncommon').
card_artist('thalakos lowlands'/'TPR', 'Jeff A. Menges').
card_number('thalakos lowlands'/'TPR', '246').
card_multiverse_id('thalakos lowlands'/'TPR', '397446').

card_in_set('thalakos scout', 'TPR').
card_original_type('thalakos scout'/'TPR', 'Creature — Thalakos Soldier Scout').
card_original_text('thalakos scout'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDiscard a card: Return Thalakos Scout to its owner\'s hand.').
card_image_name('thalakos scout'/'TPR', 'thalakos scout').
card_uid('thalakos scout'/'TPR', 'TPR:Thalakos Scout:thalakos scout').
card_rarity('thalakos scout'/'TPR', 'Common').
card_artist('thalakos scout'/'TPR', 'Daren Bader').
card_number('thalakos scout'/'TPR', '71').
card_multiverse_id('thalakos scout'/'TPR', '397474').

card_in_set('thalakos seer', 'TPR').
card_original_type('thalakos seer'/'TPR', 'Creature — Thalakos Wizard').
card_original_text('thalakos seer'/'TPR', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhen Thalakos Seer leaves the battlefield, draw a card.').
card_image_name('thalakos seer'/'TPR', 'thalakos seer').
card_uid('thalakos seer'/'TPR', 'TPR:Thalakos Seer:thalakos seer').
card_rarity('thalakos seer'/'TPR', 'Common').
card_artist('thalakos seer'/'TPR', 'Ron Spencer').
card_number('thalakos seer'/'TPR', '72').
card_flavor_text('thalakos seer'/'TPR', '\"You see our world when you shut your eyes so tightly that tiny shapes float before them.\"\n—Lyna, to Ertai').
card_multiverse_id('thalakos seer'/'TPR', '397525').

card_in_set('thopter squadron', 'TPR').
card_original_type('thopter squadron'/'TPR', 'Artifact Creature — Thopter').
card_original_text('thopter squadron'/'TPR', 'Flying\nThopter Squadron enters the battlefield with three +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Thopter Squadron: Put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield. Activate this ability only any time you could cast a sorcery.\n{1}, Sacrifice another Thopter: Put a +1/+1 counter on Thopter Squadron. Activate this ability only any time you could cast a sorcery.').
card_image_name('thopter squadron'/'TPR', 'thopter squadron').
card_uid('thopter squadron'/'TPR', 'TPR:Thopter Squadron:thopter squadron').
card_rarity('thopter squadron'/'TPR', 'Rare').
card_artist('thopter squadron'/'TPR', 'Doug Chaffee').
card_number('thopter squadron'/'TPR', '233').
card_multiverse_id('thopter squadron'/'TPR', '397624').

card_in_set('thrull surgeon', 'TPR').
card_original_type('thrull surgeon'/'TPR', 'Creature — Thrull').
card_original_text('thrull surgeon'/'TPR', '{1}{B}, Sacrifice Thrull Surgeon: Look at target player\'s hand and choose a card from it. That player discards that card. Activate this ability only any time you could cast a sorcery.').
card_image_name('thrull surgeon'/'TPR', 'thrull surgeon').
card_uid('thrull surgeon'/'TPR', 'TPR:Thrull Surgeon:thrull surgeon').
card_rarity('thrull surgeon'/'TPR', 'Common').
card_artist('thrull surgeon'/'TPR', 'rk post').
card_number('thrull surgeon'/'TPR', '121').
card_flavor_text('thrull surgeon'/'TPR', '\"Just take a little off the top.\"').
card_multiverse_id('thrull surgeon'/'TPR', '397393').

card_in_set('time ebb', 'TPR').
card_original_type('time ebb'/'TPR', 'Sorcery').
card_original_text('time ebb'/'TPR', 'Put target creature on top of its owner\'s library.').
card_image_name('time ebb'/'TPR', 'time ebb').
card_uid('time ebb'/'TPR', 'TPR:Time Ebb:time ebb').
card_rarity('time ebb'/'TPR', 'Common').
card_artist('time ebb'/'TPR', 'Thomas M. Baxa').
card_number('time ebb'/'TPR', '73').
card_flavor_text('time ebb'/'TPR', '\"Gone today, here tomorrow.\"\n—Ertai, wizard adept').
card_multiverse_id('time ebb'/'TPR', '397608').

card_in_set('time warp', 'TPR').
card_original_type('time warp'/'TPR', 'Sorcery').
card_original_text('time warp'/'TPR', 'Target player takes an extra turn after this one.').
card_image_name('time warp'/'TPR', 'time warp').
card_uid('time warp'/'TPR', 'TPR:Time Warp:time warp').
card_rarity('time warp'/'TPR', 'Mythic Rare').
card_artist('time warp'/'TPR', 'Pete Venters').
card_number('time warp'/'TPR', '74').
card_flavor_text('time warp'/'TPR', '\"Let\'s do it again!\"\n—Squee, goblin cabin hand').
card_multiverse_id('time warp'/'TPR', '397391').

card_in_set('tradewind rider', 'TPR').
card_original_type('tradewind rider'/'TPR', 'Creature — Spirit').
card_original_text('tradewind rider'/'TPR', 'Flying\n{T}, Tap two untapped creatures you control: Return target permanent to its owner\'s hand.').
card_image_name('tradewind rider'/'TPR', 'tradewind rider').
card_uid('tradewind rider'/'TPR', 'TPR:Tradewind Rider:tradewind rider').
card_rarity('tradewind rider'/'TPR', 'Rare').
card_artist('tradewind rider'/'TPR', 'John Matson').
card_number('tradewind rider'/'TPR', '75').
card_flavor_text('tradewind rider'/'TPR', 'It is said that the wind will blow the world past if you wait long enough.').
card_multiverse_id('tradewind rider'/'TPR', '397576').

card_in_set('trained armodon', 'TPR').
card_original_type('trained armodon'/'TPR', 'Creature — Elephant').
card_original_text('trained armodon'/'TPR', '').
card_image_name('trained armodon'/'TPR', 'trained armodon').
card_uid('trained armodon'/'TPR', 'TPR:Trained Armodon:trained armodon').
card_rarity('trained armodon'/'TPR', 'Common').
card_artist('trained armodon'/'TPR', 'Gary Leach').
card_number('trained armodon'/'TPR', '200').
card_flavor_text('trained armodon'/'TPR', 'These are its last days. Better to grow broad and heavy. Better that the enemy is crushed beneath its carcass.').
card_multiverse_id('trained armodon'/'TPR', '397448').

card_in_set('tranquility', 'TPR').
card_original_type('tranquility'/'TPR', 'Sorcery').
card_original_text('tranquility'/'TPR', 'Destroy all enchantments.').
card_image_name('tranquility'/'TPR', 'tranquility').
card_uid('tranquility'/'TPR', 'TPR:Tranquility:tranquility').
card_rarity('tranquility'/'TPR', 'Uncommon').
card_artist('tranquility'/'TPR', 'Margaret Organ-Kean').
card_number('tranquility'/'TPR', '201').
card_flavor_text('tranquility'/'TPR', '\"Peace will come, but whether born of harmony or entropy I cannot say.\"\n—Oracle en-Vec').
card_multiverse_id('tranquility'/'TPR', '397519').

card_in_set('twitch', 'TPR').
card_original_type('twitch'/'TPR', 'Instant').
card_original_text('twitch'/'TPR', 'You may tap or untap target artifact, creature, or land.\nDraw a card.').
card_image_name('twitch'/'TPR', 'twitch').
card_uid('twitch'/'TPR', 'TPR:Twitch:twitch').
card_rarity('twitch'/'TPR', 'Common').
card_artist('twitch'/'TPR', 'DiTerlizzi').
card_number('twitch'/'TPR', '76').
card_flavor_text('twitch'/'TPR', 'Battles are won in nuance.').
card_multiverse_id('twitch'/'TPR', '397420').

card_in_set('vampire hounds', 'TPR').
card_original_type('vampire hounds'/'TPR', 'Creature — Vampire Hound').
card_original_text('vampire hounds'/'TPR', 'Discard a creature card: Vampire Hounds gets +2/+2 until end of turn.').
card_image_name('vampire hounds'/'TPR', 'vampire hounds').
card_uid('vampire hounds'/'TPR', 'TPR:Vampire Hounds:vampire hounds').
card_rarity('vampire hounds'/'TPR', 'Common').
card_artist('vampire hounds'/'TPR', 'Kev Walker').
card_number('vampire hounds'/'TPR', '122').
card_flavor_text('vampire hounds'/'TPR', 'The hounds\' barks are a horrifying chorus of screams, moans, and whispers.').
card_multiverse_id('vampire hounds'/'TPR', '397477').

card_in_set('vec townships', 'TPR').
card_original_type('vec townships'/'TPR', 'Land').
card_original_text('vec townships'/'TPR', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Vec Townships doesn\'t untap during your next untap step.').
card_image_name('vec townships'/'TPR', 'vec townships').
card_uid('vec townships'/'TPR', 'TPR:Vec Townships:vec townships').
card_rarity('vec townships'/'TPR', 'Uncommon').
card_artist('vec townships'/'TPR', 'Eric David Anderson').
card_number('vec townships'/'TPR', '247').
card_multiverse_id('vec townships'/'TPR', '397451').

card_in_set('verdant force', 'TPR').
card_original_type('verdant force'/'TPR', 'Creature — Elemental').
card_original_text('verdant force'/'TPR', 'At the beginning of each upkeep, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('verdant force'/'TPR', 'verdant force').
card_uid('verdant force'/'TPR', 'TPR:Verdant Force:verdant force').
card_rarity('verdant force'/'TPR', 'Rare').
card_artist('verdant force'/'TPR', 'DiTerlizzi').
card_number('verdant force'/'TPR', '202').
card_flavor_text('verdant force'/'TPR', 'Burl, scurf, and bower\nBirth fern and flower.').
card_multiverse_id('verdant force'/'TPR', '397577').

card_in_set('verdant touch', 'TPR').
card_original_type('verdant touch'/'TPR', 'Sorcery').
card_original_text('verdant touch'/'TPR', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget land becomes a 2/2 creature that\'s still a land.').
card_image_name('verdant touch'/'TPR', 'verdant touch').
card_uid('verdant touch'/'TPR', 'TPR:Verdant Touch:verdant touch').
card_rarity('verdant touch'/'TPR', 'Uncommon').
card_artist('verdant touch'/'TPR', 'M. W. Kaluta & DiTerlizzi').
card_number('verdant touch'/'TPR', '203').
card_multiverse_id('verdant touch'/'TPR', '397496').

card_in_set('verdigris', 'TPR').
card_original_type('verdigris'/'TPR', 'Instant').
card_original_text('verdigris'/'TPR', 'Destroy target artifact.').
card_image_name('verdigris'/'TPR', 'verdigris').
card_uid('verdigris'/'TPR', 'TPR:Verdigris:verdigris').
card_rarity('verdigris'/'TPR', 'Common').
card_artist('verdigris'/'TPR', 'Zina Saunders').
card_number('verdigris'/'TPR', '204').
card_flavor_text('verdigris'/'TPR', '\"Only the most sophisticated inventions can survive nature\'s unsophisticated motivations.\"\n—Hanna, Weatherlight navigator').
card_multiverse_id('verdigris'/'TPR', '397538').

card_in_set('vhati il-dal', 'TPR').
card_original_type('vhati il-dal'/'TPR', 'Legendary Creature — Human Warrior').
card_original_text('vhati il-dal'/'TPR', '{T}: Until end of turn, target creature has base power 1 or base toughness 1.').
card_image_name('vhati il-dal'/'TPR', 'vhati il-dal').
card_uid('vhati il-dal'/'TPR', 'TPR:Vhati il-Dal:vhati il-dal').
card_rarity('vhati il-dal'/'TPR', 'Rare').
card_artist('vhati il-dal'/'TPR', 'Ron Spencer').
card_number('vhati il-dal'/'TPR', '214').
card_flavor_text('vhati il-dal'/'TPR', '\"Sir, I just thought . . . ,\" explained Vhati.\n\"Don\'t think,\" interrupted Greven. \"It doesn\'t suit you.\"').
card_multiverse_id('vhati il-dal'/'TPR', '397504').

card_in_set('victual sliver', 'TPR').
card_original_type('victual sliver'/'TPR', 'Creature — Sliver').
card_original_text('victual sliver'/'TPR', 'All Slivers have \"{2}, Sacrifice this permanent: You gain 4 life.\"').
card_image_name('victual sliver'/'TPR', 'victual sliver').
card_uid('victual sliver'/'TPR', 'TPR:Victual Sliver:victual sliver').
card_rarity('victual sliver'/'TPR', 'Uncommon').
card_artist('victual sliver'/'TPR', 'Terese Nielsen').
card_number('victual sliver'/'TPR', '215').
card_flavor_text('victual sliver'/'TPR', '\"We are kinfolk,\" explained Karn to the sliver queen. \"Just as you need your progeny to complete you, so do I need the pieces of the Legacy to make me whole.\"').
card_multiverse_id('victual sliver'/'TPR', '397416').

card_in_set('volrath\'s curse', 'TPR').
card_original_type('volrath\'s curse'/'TPR', 'Enchantment — Aura').
card_original_text('volrath\'s curse'/'TPR', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated. That creature\'s controller may sacrifice a permanent for that player to ignore this effect until end of turn.\n{1}{U}: Return Volrath\'s Curse to its owner\'s hand.').
card_image_name('volrath\'s curse'/'TPR', 'volrath\'s curse').
card_uid('volrath\'s curse'/'TPR', 'TPR:Volrath\'s Curse:volrath\'s curse').
card_rarity('volrath\'s curse'/'TPR', 'Uncommon').
card_artist('volrath\'s curse'/'TPR', 'Daren Bader').
card_number('volrath\'s curse'/'TPR', '77').
card_multiverse_id('volrath\'s curse'/'TPR', '397637').

card_in_set('volrath\'s laboratory', 'TPR').
card_original_type('volrath\'s laboratory'/'TPR', 'Artifact').
card_original_text('volrath\'s laboratory'/'TPR', 'As Volrath\'s Laboratory enters the battlefield, choose a color and a creature type.\n{5}, {T}: Put a 2/2 creature token of the chosen color and type onto the battlefield.').
card_image_name('volrath\'s laboratory'/'TPR', 'volrath\'s laboratory').
card_uid('volrath\'s laboratory'/'TPR', 'TPR:Volrath\'s Laboratory:volrath\'s laboratory').
card_rarity('volrath\'s laboratory'/'TPR', 'Rare').
card_artist('volrath\'s laboratory'/'TPR', 'Brom').
card_number('volrath\'s laboratory'/'TPR', '234').
card_multiverse_id('volrath\'s laboratory'/'TPR', '397656').

card_in_set('volrath\'s stronghold', 'TPR').
card_original_type('volrath\'s stronghold'/'TPR', 'Legendary Land').
card_original_text('volrath\'s stronghold'/'TPR', '{T}: Add {1} to your mana pool.\n{1}{B}, {T}: Put target creature card from your graveyard on top of your library.').
card_image_name('volrath\'s stronghold'/'TPR', 'volrath\'s stronghold').
card_uid('volrath\'s stronghold'/'TPR', 'TPR:Volrath\'s Stronghold:volrath\'s stronghold').
card_rarity('volrath\'s stronghold'/'TPR', 'Mythic Rare').
card_artist('volrath\'s stronghold'/'TPR', 'Kev Walker').
card_number('volrath\'s stronghold'/'TPR', '248').
card_flavor_text('volrath\'s stronghold'/'TPR', 'The seed of a world\'s evil.').
card_multiverse_id('volrath\'s stronghold'/'TPR', '397619').

card_in_set('wall of blossoms', 'TPR').
card_original_type('wall of blossoms'/'TPR', 'Creature — Plant Wall').
card_original_text('wall of blossoms'/'TPR', 'Defender\nWhen Wall of Blossoms enters the battlefield, draw a card.').
card_image_name('wall of blossoms'/'TPR', 'wall of blossoms').
card_uid('wall of blossoms'/'TPR', 'TPR:Wall of Blossoms:wall of blossoms').
card_rarity('wall of blossoms'/'TPR', 'Uncommon').
card_artist('wall of blossoms'/'TPR', 'Heather Hudson').
card_number('wall of blossoms'/'TPR', '205').
card_flavor_text('wall of blossoms'/'TPR', 'Each flower identical, every leaf and petal disturbingly exact.').
card_multiverse_id('wall of blossoms'/'TPR', '397479').

card_in_set('wall of diffusion', 'TPR').
card_original_type('wall of diffusion'/'TPR', 'Creature — Wall').
card_original_text('wall of diffusion'/'TPR', 'Defender\nWall of Diffusion can block creatures with shadow as though Wall of Diffusion had shadow.').
card_image_name('wall of diffusion'/'TPR', 'wall of diffusion').
card_uid('wall of diffusion'/'TPR', 'TPR:Wall of Diffusion:wall of diffusion').
card_rarity('wall of diffusion'/'TPR', 'Common').
card_artist('wall of diffusion'/'TPR', 'DiTerlizzi').
card_number('wall of diffusion'/'TPR', '164').
card_flavor_text('wall of diffusion'/'TPR', '\"The injury was being caught between this world and our own; the insult was to find walls within.\"\n—Lyna, soltari emissary').
card_multiverse_id('wall of diffusion'/'TPR', '397560').

card_in_set('wall of essence', 'TPR').
card_original_type('wall of essence'/'TPR', 'Creature — Wall').
card_original_text('wall of essence'/'TPR', 'Defender\nWhenever Wall of Essence is dealt combat damage, you gain that much life.').
card_image_name('wall of essence'/'TPR', 'wall of essence').
card_uid('wall of essence'/'TPR', 'TPR:Wall of Essence:wall of essence').
card_rarity('wall of essence'/'TPR', 'Uncommon').
card_artist('wall of essence'/'TPR', 'Adam Rex').
card_number('wall of essence'/'TPR', '38').
card_flavor_text('wall of essence'/'TPR', 'The ceiling and the floor fell in love, but only the wall knew.\n—Dal saying').
card_multiverse_id('wall of essence'/'TPR', '397439').

card_in_set('wall of souls', 'TPR').
card_original_type('wall of souls'/'TPR', 'Creature — Wall').
card_original_text('wall of souls'/'TPR', 'Defender\nWhenever Wall of Souls is dealt combat damage, it deals that much damage to target opponent.').
card_image_name('wall of souls'/'TPR', 'wall of souls').
card_uid('wall of souls'/'TPR', 'TPR:Wall of Souls:wall of souls').
card_rarity('wall of souls'/'TPR', 'Uncommon').
card_artist('wall of souls'/'TPR', 'John Matson').
card_number('wall of souls'/'TPR', '123').
card_flavor_text('wall of souls'/'TPR', '\"It is the nature of evil to turn you against yourself.\"\n—Starke').
card_multiverse_id('wall of souls'/'TPR', '397551').

card_in_set('warrior en-kor', 'TPR').
card_original_type('warrior en-kor'/'TPR', 'Creature — Kor Warrior Knight').
card_original_text('warrior en-kor'/'TPR', '{0}: The next 1 damage that would be dealt to Warrior en-Kor this turn is dealt to target creature you control instead.').
card_image_name('warrior en-kor'/'TPR', 'warrior en-kor').
card_uid('warrior en-kor'/'TPR', 'TPR:Warrior en-Kor:warrior en-kor').
card_rarity('warrior en-kor'/'TPR', 'Uncommon').
card_artist('warrior en-kor'/'TPR', 'Stephen Daniele').
card_number('warrior en-kor'/'TPR', '39').
card_flavor_text('warrior en-kor'/'TPR', 'Only a matter as vital as destroying Volrath could bring together the reclusive kor people.').
card_multiverse_id('warrior en-kor'/'TPR', '397402').

card_in_set('wasteland', 'TPR').
card_original_type('wasteland'/'TPR', 'Land').
card_original_text('wasteland'/'TPR', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Wasteland: Destroy target nonbasic land.').
card_image_name('wasteland'/'TPR', 'wasteland').
card_uid('wasteland'/'TPR', 'TPR:Wasteland:wasteland').
card_rarity('wasteland'/'TPR', 'Rare').
card_artist('wasteland'/'TPR', 'Una Fricker').
card_number('wasteland'/'TPR', '249').
card_flavor_text('wasteland'/'TPR', '\"The land promises nothing and keeps its promise.\"\n—Oracle en-Vec').
card_multiverse_id('wasteland'/'TPR', '397531').

card_in_set('wayward soul', 'TPR').
card_original_type('wayward soul'/'TPR', 'Creature — Spirit').
card_original_text('wayward soul'/'TPR', 'Flying\n{U}: Put Wayward Soul on top of its owner\'s library.').
card_image_name('wayward soul'/'TPR', 'wayward soul').
card_uid('wayward soul'/'TPR', 'TPR:Wayward Soul:wayward soul').
card_rarity('wayward soul'/'TPR', 'Common').
card_artist('wayward soul'/'TPR', 'M. W. Kaluta & DiTerlizzi').
card_number('wayward soul'/'TPR', '78').
card_flavor_text('wayward soul'/'TPR', '\"No home, no heart, no hope.\"\n—Stronghold graffito').
card_multiverse_id('wayward soul'/'TPR', '397593').

card_in_set('whispers of the muse', 'TPR').
card_original_type('whispers of the muse'/'TPR', 'Instant').
card_original_text('whispers of the muse'/'TPR', 'Buyback {5} (You may pay an additional {5} as you cast this spell. If you do, put this card into your hand as it resolves.)\nDraw a card.').
card_image_name('whispers of the muse'/'TPR', 'whispers of the muse').
card_uid('whispers of the muse'/'TPR', 'TPR:Whispers of the Muse:whispers of the muse').
card_rarity('whispers of the muse'/'TPR', 'Common').
card_artist('whispers of the muse'/'TPR', 'Quinton Hoover').
card_number('whispers of the muse'/'TPR', '79').
card_flavor_text('whispers of the muse'/'TPR', '\"I followed her song only to find it was a dirge.\"\n—Crovax').
card_multiverse_id('whispers of the muse'/'TPR', '397548').

card_in_set('wind dancer', 'TPR').
card_original_type('wind dancer'/'TPR', 'Creature — Faerie').
card_original_text('wind dancer'/'TPR', 'Flying\n{T}: Target creature gains flying until end of turn.').
card_image_name('wind dancer'/'TPR', 'wind dancer').
card_uid('wind dancer'/'TPR', 'TPR:Wind Dancer:wind dancer').
card_rarity('wind dancer'/'TPR', 'Uncommon').
card_artist('wind dancer'/'TPR', 'Susan Van Camp').
card_number('wind dancer'/'TPR', '80').
card_flavor_text('wind dancer'/'TPR', '\"Flying like a bird does not make you as free as one.\"\n—Volrath').
card_multiverse_id('wind dancer'/'TPR', '397478').

card_in_set('wind drake', 'TPR').
card_original_type('wind drake'/'TPR', 'Creature — Drake').
card_original_text('wind drake'/'TPR', 'Flying').
card_image_name('wind drake'/'TPR', 'wind drake').
card_uid('wind drake'/'TPR', 'TPR:Wind Drake:wind drake').
card_rarity('wind drake'/'TPR', 'Common').
card_artist('wind drake'/'TPR', 'Greg Simanson').
card_number('wind drake'/'TPR', '81').
card_flavor_text('wind drake'/'TPR', 'Orim regarded Gerrard coolly. \"You are like the drake,\" she said, her expression unchanging. \"It works hard to maintain its position, but to us it seems to glide along effortlessly. You must do the same.\"').
card_multiverse_id('wind drake'/'TPR', '397626').

card_in_set('winds of rath', 'TPR').
card_original_type('winds of rath'/'TPR', 'Sorcery').
card_original_text('winds of rath'/'TPR', 'Destroy all creatures that aren\'t enchanted. They can\'t be regenerated.').
card_image_name('winds of rath'/'TPR', 'winds of rath').
card_uid('winds of rath'/'TPR', 'TPR:Winds of Rath:winds of rath').
card_rarity('winds of rath'/'TPR', 'Rare').
card_artist('winds of rath'/'TPR', 'Drew Tucker').
card_number('winds of rath'/'TPR', '40').
card_flavor_text('winds of rath'/'TPR', '\"There shall be a vast shout and then a vaster silence.\"\n—Oracle en-Vec').
card_multiverse_id('winds of rath'/'TPR', '397606').

card_in_set('winged sliver', 'TPR').
card_original_type('winged sliver'/'TPR', 'Creature — Sliver').
card_original_text('winged sliver'/'TPR', 'All Sliver creatures have flying.').
card_image_name('winged sliver'/'TPR', 'winged sliver').
card_uid('winged sliver'/'TPR', 'TPR:Winged Sliver:winged sliver').
card_rarity('winged sliver'/'TPR', 'Common').
card_artist('winged sliver'/'TPR', 'Anthony S. Waters').
card_number('winged sliver'/'TPR', '82').
card_flavor_text('winged sliver'/'TPR', '\"Everything around here has cut a deal with gravity.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('winged sliver'/'TPR', '397579').

card_in_set('wood sage', 'TPR').
card_original_type('wood sage'/'TPR', 'Creature — Human Druid').
card_original_text('wood sage'/'TPR', '{T}: Name a creature card. Reveal the top four cards of your library and put all of them with that name into your hand. Put the rest into your graveyard.').
card_image_name('wood sage'/'TPR', 'wood sage').
card_uid('wood sage'/'TPR', 'TPR:Wood Sage:wood sage').
card_rarity('wood sage'/'TPR', 'Rare').
card_artist('wood sage'/'TPR', 'Paolo Parente').
card_number('wood sage'/'TPR', '216').
card_multiverse_id('wood sage'/'TPR', '397455').

card_in_set('youthful knight', 'TPR').
card_original_type('youthful knight'/'TPR', 'Creature — Human Knight').
card_original_text('youthful knight'/'TPR', 'First strike').
card_image_name('youthful knight'/'TPR', 'youthful knight').
card_uid('youthful knight'/'TPR', 'TPR:Youthful Knight:youthful knight').
card_rarity('youthful knight'/'TPR', 'Common').
card_artist('youthful knight'/'TPR', 'Rebecca Guay').
card_number('youthful knight'/'TPR', '41').
card_flavor_text('youthful knight'/'TPR', '\"Let no child be without a sword. We will all fight, for if we fail, we will certainly all die.\"\n—Oracle en-Vec').
card_multiverse_id('youthful knight'/'TPR', '397413').
