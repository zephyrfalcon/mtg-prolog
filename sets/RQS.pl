% Rivals Quick Start Set

set('RQS').
set_name('RQS', 'Rivals Quick Start Set').
set_release_date('RQS', '1996-07-01').
set_border('RQS', 'white').
set_type('RQS', 'box').

card_in_set('alabaster potion', 'RQS').
card_original_type('alabaster potion'/'RQS', 'Instant').
card_original_text('alabaster potion'/'RQS', 'Give target player X life, or prevent X damage to any creature or player.').
card_image_name('alabaster potion'/'RQS', 'alabaster potion').
card_uid('alabaster potion'/'RQS', 'RQS:Alabaster Potion:alabaster potion').
card_rarity('alabaster potion'/'RQS', 'Special').
card_artist('alabaster potion'/'RQS', 'Harold McNeill').
card_flavor_text('alabaster potion'/'RQS', '\"Healing is a matter of time, but it is sometimes also a matter of opportunity.\"\n—D\'Avenant proverb').

card_in_set('battering ram', 'RQS').
card_original_type('battering ram'/'RQS', 'Artifact Creature').
card_original_text('battering ram'/'RQS', 'Banding when attacking\nAt the end of combat, destroy all walls blocking Battering Ram.').
card_image_name('battering ram'/'RQS', 'battering ram').
card_uid('battering ram'/'RQS', 'RQS:Battering Ram:battering ram').
card_rarity('battering ram'/'RQS', 'Special').
card_artist('battering ram'/'RQS', 'Jeff A. Menges').
card_flavor_text('battering ram'/'RQS', 'By the time Mishra was defeated, no mage was foolish enough to rely heavily on walls.').

card_in_set('bog imp', 'RQS').
card_original_type('bog imp'/'RQS', 'Summon — Imp').
card_original_text('bog imp'/'RQS', 'Flying').
card_image_name('bog imp'/'RQS', 'bog imp').
card_uid('bog imp'/'RQS', 'RQS:Bog Imp:bog imp').
card_rarity('bog imp'/'RQS', 'Special').
card_artist('bog imp'/'RQS', 'Ron Spencer').
card_flavor_text('bog imp'/'RQS', 'On guard for larger dangers, we underestimated the power and speed of the Imp\'s muck-crusted claws.').

card_in_set('bog wraith', 'RQS').
card_original_type('bog wraith'/'RQS', 'Summon — Wraith').
card_original_text('bog wraith'/'RQS', 'Swampwalk').
card_image_name('bog wraith'/'RQS', 'bog wraith').
card_uid('bog wraith'/'RQS', 'RQS:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'RQS', 'Special').
card_artist('bog wraith'/'RQS', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'RQS', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').

card_in_set('circle of protection: black', 'RQS').
card_original_type('circle of protection: black'/'RQS', 'Enchantment').
card_original_text('circle of protection: black'/'RQS', '{1}: Prevent all damage against you from one black source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: black'/'RQS', 'circle of protection black').
card_uid('circle of protection: black'/'RQS', 'RQS:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'RQS', 'Special').
card_artist('circle of protection: black'/'RQS', 'Jesper Myrfors').

card_in_set('circle of protection: red', 'RQS').
card_original_type('circle of protection: red'/'RQS', 'Enchantment').
card_original_text('circle of protection: red'/'RQS', '{1}: Prevent all damage against you from one red source. If a source deals damage to you more than once in a turn, you may pay {1} each time to prevent the damage.').
card_image_name('circle of protection: red'/'RQS', 'circle of protection red').
card_uid('circle of protection: red'/'RQS', 'RQS:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'RQS', 'Special').
card_artist('circle of protection: red'/'RQS', 'Mark Tedin').

card_in_set('clockwork beast', 'RQS').
card_original_type('clockwork beast'/'RQS', 'Artifact Creature').
card_original_text('clockwork beast'/'RQS', 'When Clockwork Beast comes into play, put seven +1/+0 counters on it. At the end of any combat in which Clockwork Beast is assigned to attack or block, remove a counter.\n{X}, {T}: Put X +1/+0 counters on Clockwork Beast. You may have no more than seven of these counters on Clockwork Beast. Use this ability only during your upkeep.').
card_image_name('clockwork beast'/'RQS', 'clockwork beast').
card_uid('clockwork beast'/'RQS', 'RQS:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'RQS', 'Special').
card_artist('clockwork beast'/'RQS', 'Drew Tucker').

card_in_set('cursed land', 'RQS').
card_original_type('cursed land'/'RQS', 'Enchant Land').
card_original_text('cursed land'/'RQS', 'Cursed Land deals 1 damage to target land\'s controller during his or her upkeep.').
card_image_name('cursed land'/'RQS', 'cursed land').
card_uid('cursed land'/'RQS', 'RQS:Cursed Land:cursed land').
card_rarity('cursed land'/'RQS', 'Special').
card_artist('cursed land'/'RQS', 'Jesper Myrfors').

card_in_set('dark ritual', 'RQS').
card_original_type('dark ritual'/'RQS', 'Interrupt').
card_original_text('dark ritual'/'RQS', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'RQS', 'dark ritual').
card_uid('dark ritual'/'RQS', 'RQS:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'RQS', 'Special').
card_artist('dark ritual'/'RQS', 'Sandra Everingham').

card_in_set('detonate', 'RQS').
card_original_type('detonate'/'RQS', 'Sorcery').
card_original_text('detonate'/'RQS', 'Bury target artifact. Detonate deals X damage to the artifact\'s controller, where X is the casting cost of the artifact.').
card_image_name('detonate'/'RQS', 'detonate').
card_uid('detonate'/'RQS', 'RQS:Detonate:detonate').
card_rarity('detonate'/'RQS', 'Special').
card_artist('detonate'/'RQS', 'Randy Asplund-Faith').

card_in_set('disintegrate', 'RQS').
card_original_type('disintegrate'/'RQS', 'Sorcery').
card_original_text('disintegrate'/'RQS', 'Disintegrate deals X damage to target creature or player. The target cannot regenerate until end of turn. If the target receives lethal damage this turn, remove it from the game entirely.').
card_image_name('disintegrate'/'RQS', 'disintegrate').
card_uid('disintegrate'/'RQS', 'RQS:Disintegrate:disintegrate').
card_rarity('disintegrate'/'RQS', 'Special').
card_artist('disintegrate'/'RQS', 'Anson Maddocks').

card_in_set('durkwood boars', 'RQS').
card_original_type('durkwood boars'/'RQS', 'Summon — Boars').
card_original_text('durkwood boars'/'RQS', '').
card_image_name('durkwood boars'/'RQS', 'durkwood boars').
card_uid('durkwood boars'/'RQS', 'RQS:Durkwood Boars:durkwood boars').
card_rarity('durkwood boars'/'RQS', 'Special').
card_artist('durkwood boars'/'RQS', 'Mike Kimble').
card_flavor_text('durkwood boars'/'RQS', '\"And the unclean spirits went out, and entered the swine: and the herd ran violently . . . .\"\n—Mark 5:13').

card_in_set('elven riders', 'RQS').
card_original_type('elven riders'/'RQS', 'Summon — Riders').
card_original_text('elven riders'/'RQS', 'Cannot be blocked except by walls and by creatures with flying.').
card_image_name('elven riders'/'RQS', 'elven riders').
card_uid('elven riders'/'RQS', 'RQS:Elven Riders:elven riders').
card_rarity('elven riders'/'RQS', 'Special').
card_artist('elven riders'/'RQS', 'Melissa A. Benson').
card_flavor_text('elven riders'/'RQS', '\"Sometimes it is better to be swift of foot than strong of swordarm.\"\n—Elven proverb').

card_in_set('elvish archers', 'RQS').
card_original_type('elvish archers'/'RQS', 'Summon — Elves').
card_original_text('elvish archers'/'RQS', 'First strike').
card_image_name('elvish archers'/'RQS', 'elvish archers').
card_uid('elvish archers'/'RQS', 'RQS:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'RQS', 'Special').
card_artist('elvish archers'/'RQS', 'Anson Maddocks').
card_flavor_text('elvish archers'/'RQS', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').

card_in_set('energy flux', 'RQS').
card_original_type('energy flux'/'RQS', 'Enchantment').
card_original_text('energy flux'/'RQS', 'During each player\'s upkeep, destroy all artifacts that player controls. The player may pay an additional {2} for each artifact he or she wishes to prevent Energy Flux from destroying.').
card_image_name('energy flux'/'RQS', 'energy flux').
card_uid('energy flux'/'RQS', 'RQS:Energy Flux:energy flux').
card_rarity('energy flux'/'RQS', 'Special').
card_artist('energy flux'/'RQS', 'Kaja Foglio').

card_in_set('feedback', 'RQS').
card_original_type('feedback'/'RQS', 'Enchant Enchantment').
card_original_text('feedback'/'RQS', 'Feedback deals 1 damage to controller of target enchantment during that player\'s upkeep.').
card_image_name('feedback'/'RQS', 'feedback').
card_uid('feedback'/'RQS', 'RQS:Feedback:feedback').
card_rarity('feedback'/'RQS', 'Special').
card_artist('feedback'/'RQS', 'Quinton Hoover').

card_in_set('fireball', 'RQS').
card_original_type('fireball'/'RQS', 'Sorcery').
card_original_text('fireball'/'RQS', 'Fireball deals X damage, divided evenly (round down) among any number of target creatures and/or players. Pay an additional {1} for each target beyond the first.').
card_image_name('fireball'/'RQS', 'fireball').
card_uid('fireball'/'RQS', 'RQS:Fireball:fireball').
card_rarity('fireball'/'RQS', 'Special').
card_artist('fireball'/'RQS', 'Mark Tedin').

card_in_set('forest', 'RQS').
card_original_type('forest'/'RQS', 'Land').
card_original_text('forest'/'RQS', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'RQS', 'forest1').
card_uid('forest'/'RQS', 'RQS:Forest:forest1').
card_rarity('forest'/'RQS', 'Basic Land').
card_artist('forest'/'RQS', 'Christopher Rush').

card_in_set('glasses of urza', 'RQS').
card_original_type('glasses of urza'/'RQS', 'Artifact').
card_original_text('glasses of urza'/'RQS', '{T}: Look at target player\'s hand.').
card_image_name('glasses of urza'/'RQS', 'glasses of urza').
card_uid('glasses of urza'/'RQS', 'RQS:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'RQS', 'Special').
card_artist('glasses of urza'/'RQS', 'Douglas Shuler').

card_in_set('grizzly bears', 'RQS').
card_original_type('grizzly bears'/'RQS', 'Summon — Bears').
card_original_text('grizzly bears'/'RQS', '').
card_image_name('grizzly bears'/'RQS', 'grizzly bears').
card_uid('grizzly bears'/'RQS', 'RQS:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'RQS', 'Special').
card_artist('grizzly bears'/'RQS', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'RQS', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').

card_in_set('healing salve', 'RQS').
card_original_type('healing salve'/'RQS', 'Instant').
card_original_text('healing salve'/'RQS', 'Give target player 3 life, or prevent up to 3 damage to any creature or player.').
card_image_name('healing salve'/'RQS', 'healing salve').
card_uid('healing salve'/'RQS', 'RQS:Healing Salve:healing salve').
card_rarity('healing salve'/'RQS', 'Special').
card_artist('healing salve'/'RQS', 'Dan Frazier').

card_in_set('hill giant', 'RQS').
card_original_type('hill giant'/'RQS', 'Summon — Giant').
card_original_text('hill giant'/'RQS', '').
card_image_name('hill giant'/'RQS', 'hill giant').
card_uid('hill giant'/'RQS', 'RQS:Hill Giant:hill giant').
card_rarity('hill giant'/'RQS', 'Special').
card_artist('hill giant'/'RQS', 'Dan Frazier').
card_flavor_text('hill giant'/'RQS', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').

card_in_set('ironclaw orcs', 'RQS').
card_original_type('ironclaw orcs'/'RQS', 'Summon — Orcs').
card_original_text('ironclaw orcs'/'RQS', 'Cannot be assigned to block any creature with power greater than 1.').
card_image_name('ironclaw orcs'/'RQS', 'ironclaw orcs').
card_uid('ironclaw orcs'/'RQS', 'RQS:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'RQS', 'Special').
card_artist('ironclaw orcs'/'RQS', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'RQS', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').

card_in_set('island', 'RQS').
card_original_type('island'/'RQS', 'Land').
card_original_text('island'/'RQS', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'RQS', 'island1').
card_uid('island'/'RQS', 'RQS:Island:island1').
card_rarity('island'/'RQS', 'Basic Land').
card_artist('island'/'RQS', 'Mark Poole').

card_in_set('jayemdae tome', 'RQS').
card_original_type('jayemdae tome'/'RQS', 'Artifact').
card_original_text('jayemdae tome'/'RQS', '{4}, {T}: Draw one card.').
card_image_name('jayemdae tome'/'RQS', 'jayemdae tome').
card_uid('jayemdae tome'/'RQS', 'RQS:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'RQS', 'Special').
card_artist('jayemdae tome'/'RQS', 'Mark Tedin').

card_in_set('lost soul', 'RQS').
card_original_type('lost soul'/'RQS', 'Summon — Lost Soul').
card_original_text('lost soul'/'RQS', 'Swampwalk').
card_image_name('lost soul'/'RQS', 'lost soul').
card_uid('lost soul'/'RQS', 'RQS:Lost Soul:lost soul').
card_rarity('lost soul'/'RQS', 'Special').
card_artist('lost soul'/'RQS', 'Randy Asplund-Faith').
card_flavor_text('lost soul'/'RQS', 'Her hand gently beckons,\nshe whispers your name—\nBut those who go with her\nare never the same.').

card_in_set('merfolk of the pearl trident', 'RQS').
card_original_type('merfolk of the pearl trident'/'RQS', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'RQS', '').
card_image_name('merfolk of the pearl trident'/'RQS', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'RQS', 'RQS:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'RQS', 'Special').
card_artist('merfolk of the pearl trident'/'RQS', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'RQS', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').

card_in_set('mesa pegasus', 'RQS').
card_original_type('mesa pegasus'/'RQS', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'RQS', 'Flying, banding').
card_image_name('mesa pegasus'/'RQS', 'mesa pegasus').
card_uid('mesa pegasus'/'RQS', 'RQS:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'RQS', 'Special').
card_artist('mesa pegasus'/'RQS', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'RQS', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').

card_in_set('mons\'s goblin raiders', 'RQS').
card_original_type('mons\'s goblin raiders'/'RQS', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'RQS', '').
card_image_name('mons\'s goblin raiders'/'RQS', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'RQS', 'RQS:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'RQS', 'Special').
card_artist('mons\'s goblin raiders'/'RQS', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'RQS', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his Raiders are the thunderhead that leads in the storm.').

card_in_set('mountain', 'RQS').
card_original_type('mountain'/'RQS', 'Land').
card_original_text('mountain'/'RQS', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'RQS', 'mountain1').
card_uid('mountain'/'RQS', 'RQS:Mountain:mountain1').
card_rarity('mountain'/'RQS', 'Basic Land').
card_artist('mountain'/'RQS', 'Douglas Shuler').

card_in_set('murk dwellers', 'RQS').
card_original_type('murk dwellers'/'RQS', 'Summon — Murk Dwellers').
card_original_text('murk dwellers'/'RQS', 'When attacking and not blocked, Murk Dwellers gets +2/+0 until end of turn.').
card_image_name('murk dwellers'/'RQS', 'murk dwellers').
card_uid('murk dwellers'/'RQS', 'RQS:Murk Dwellers:murk dwellers').
card_rarity('murk dwellers'/'RQS', 'Special').
card_artist('murk dwellers'/'RQS', 'Drew Tucker').
card_flavor_text('murk dwellers'/'RQS', 'When Raganorn unsealed the catacombs, he found more than the dead and their treasures.').

card_in_set('orcish artillery', 'RQS').
card_original_type('orcish artillery'/'RQS', 'Summon — Orcs').
card_original_text('orcish artillery'/'RQS', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'RQS', 'orcish artillery').
card_uid('orcish artillery'/'RQS', 'RQS:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'RQS', 'Special').
card_artist('orcish artillery'/'RQS', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'RQS', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').

card_in_set('orcish oriflamme', 'RQS').
card_original_type('orcish oriflamme'/'RQS', 'Enchantment').
card_original_text('orcish oriflamme'/'RQS', 'All attacking creatures you control get +1/+0.').
card_image_name('orcish oriflamme'/'RQS', 'orcish oriflamme').
card_uid('orcish oriflamme'/'RQS', 'RQS:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'RQS', 'Special').
card_artist('orcish oriflamme'/'RQS', 'Dan Frazier').

card_in_set('pearled unicorn', 'RQS').
card_original_type('pearled unicorn'/'RQS', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'RQS', '').
card_image_name('pearled unicorn'/'RQS', 'pearled unicorn').
card_uid('pearled unicorn'/'RQS', 'RQS:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'RQS', 'Special').
card_artist('pearled unicorn'/'RQS', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'RQS', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\" —Lewis Carroll').

card_in_set('plains', 'RQS').
card_original_type('plains'/'RQS', 'Land').
card_original_text('plains'/'RQS', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'RQS', 'plains1').
card_uid('plains'/'RQS', 'RQS:Plains:plains1').
card_rarity('plains'/'RQS', 'Basic Land').
card_artist('plains'/'RQS', 'Jesper Myrfors').

card_in_set('power sink', 'RQS').
card_original_type('power sink'/'RQS', 'Interrupt').
card_original_text('power sink'/'RQS', 'Counter a target spell if its caster does not pay {X}. Target spell\'s caster must draw and pay all available mana from lands and mana pool until {X} is paid; he or she may also pay mana from other sources if desired.').
card_image_name('power sink'/'RQS', 'power sink').
card_uid('power sink'/'RQS', 'RQS:Power Sink:power sink').
card_rarity('power sink'/'RQS', 'Special').
card_artist('power sink'/'RQS', 'Richard Thomas').

card_in_set('pyrotechnics', 'RQS').
card_original_type('pyrotechnics'/'RQS', 'Sorcery').
card_original_text('pyrotechnics'/'RQS', 'Pyrotechnics deals 4 damage divided any way you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'RQS', 'pyrotechnics').
card_uid('pyrotechnics'/'RQS', 'RQS:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'RQS', 'Special').
card_artist('pyrotechnics'/'RQS', 'Anson Maddocks').
card_flavor_text('pyrotechnics'/'RQS', '\"Hi! ni! ya! Behold the man of flint, that\'s me!\nFour lightnings zigzag from me, strike and return.\" —Navajo war chant').

card_in_set('raise dead', 'RQS').
card_original_type('raise dead'/'RQS', 'Sorcery').
card_original_text('raise dead'/'RQS', 'Take target creature from your graveyard and put it into your hand.').
card_image_name('raise dead'/'RQS', 'raise dead').
card_uid('raise dead'/'RQS', 'RQS:Raise Dead:raise dead').
card_rarity('raise dead'/'RQS', 'Special').
card_artist('raise dead'/'RQS', 'Jeff A. Menges').

card_in_set('rod of ruin', 'RQS').
card_original_type('rod of ruin'/'RQS', 'Artifact').
card_original_text('rod of ruin'/'RQS', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'RQS', 'rod of ruin').
card_uid('rod of ruin'/'RQS', 'RQS:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'RQS', 'Special').
card_artist('rod of ruin'/'RQS', 'Christopher Rush').

card_in_set('scryb sprites', 'RQS').
card_original_type('scryb sprites'/'RQS', 'Summon — Faeries').
card_original_text('scryb sprites'/'RQS', 'Flying').
card_image_name('scryb sprites'/'RQS', 'scryb sprites').
card_uid('scryb sprites'/'RQS', 'RQS:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'RQS', 'Special').
card_artist('scryb sprites'/'RQS', 'Amy Weber').
card_flavor_text('scryb sprites'/'RQS', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').

card_in_set('sorceress queen', 'RQS').
card_original_type('sorceress queen'/'RQS', 'Summon — Sorceress').
card_original_text('sorceress queen'/'RQS', '{T}: Target creature other than Sorceress Queen becomes 0/2 until end of turn.').
card_image_name('sorceress queen'/'RQS', 'sorceress queen').
card_uid('sorceress queen'/'RQS', 'RQS:Sorceress Queen:sorceress queen').
card_rarity('sorceress queen'/'RQS', 'Special').
card_artist('sorceress queen'/'RQS', 'Kaja Foglio').

card_in_set('swamp', 'RQS').
card_original_type('swamp'/'RQS', 'Land').
card_original_text('swamp'/'RQS', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'RQS', 'swamp1').
card_uid('swamp'/'RQS', 'RQS:Swamp:swamp1').
card_rarity('swamp'/'RQS', 'Basic Land').
card_artist('swamp'/'RQS', 'Dan Frazier').

card_in_set('terror', 'RQS').
card_original_type('terror'/'RQS', 'Instant').
card_original_text('terror'/'RQS', 'Bury target non-black, non-artifact creature.').
card_image_name('terror'/'RQS', 'terror').
card_uid('terror'/'RQS', 'RQS:Terror:terror').
card_rarity('terror'/'RQS', 'Special').
card_artist('terror'/'RQS', 'Ron Spencer').

card_in_set('twiddle', 'RQS').
card_original_type('twiddle'/'RQS', 'Instant').
card_original_text('twiddle'/'RQS', 'Tap or untap target land, artifact, or creature.').
card_image_name('twiddle'/'RQS', 'twiddle').
card_uid('twiddle'/'RQS', 'RQS:Twiddle:twiddle').
card_rarity('twiddle'/'RQS', 'Special').
card_artist('twiddle'/'RQS', 'Rob Alexander').

card_in_set('unsummon', 'RQS').
card_original_type('unsummon'/'RQS', 'Instant').
card_original_text('unsummon'/'RQS', 'Return target creature to owner\'s hand.').
card_image_name('unsummon'/'RQS', 'unsummon').
card_uid('unsummon'/'RQS', 'RQS:Unsummon:unsummon').
card_rarity('unsummon'/'RQS', 'Special').
card_artist('unsummon'/'RQS', 'Douglas Shuler').

card_in_set('untamed wilds', 'RQS').
card_original_type('untamed wilds'/'RQS', 'Sorcery').
card_original_text('untamed wilds'/'RQS', 'Search your library for any one basic land and put it directly into play. This does not count towards your one land per turn limit. Reshuffle your library afterwards.').
card_image_name('untamed wilds'/'RQS', 'untamed wilds').
card_uid('untamed wilds'/'RQS', 'RQS:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'RQS', 'Special').
card_artist('untamed wilds'/'RQS', 'NéNé Thomas').

card_in_set('vampire bats', 'RQS').
card_original_type('vampire bats'/'RQS', 'Summon — Bats').
card_original_text('vampire bats'/'RQS', 'Flying\n{B} +1/+0 until end of turn. You cannot spend more than {B}{B} in this way each turn.').
card_image_name('vampire bats'/'RQS', 'vampire bats').
card_uid('vampire bats'/'RQS', 'RQS:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'RQS', 'Special').
card_artist('vampire bats'/'RQS', 'Anson Maddocks').
card_flavor_text('vampire bats'/'RQS', '\"For something is amiss or out of place\nWhen mice with wings can wear a human face.\"\n—Theodore Roethke, \"The Bat\"').

card_in_set('wall of bone', 'RQS').
card_original_type('wall of bone'/'RQS', 'Summon — Wall').
card_original_text('wall of bone'/'RQS', '{B} Regenerate').
card_image_name('wall of bone'/'RQS', 'wall of bone').
card_uid('wall of bone'/'RQS', 'RQS:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'RQS', 'Special').
card_artist('wall of bone'/'RQS', 'Anson Maddocks').
card_flavor_text('wall of bone'/'RQS', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').

card_in_set('war mammoth', 'RQS').
card_original_type('war mammoth'/'RQS', 'Summon — Mammoth').
card_original_text('war mammoth'/'RQS', 'Trample').
card_image_name('war mammoth'/'RQS', 'war mammoth').
card_uid('war mammoth'/'RQS', 'RQS:War Mammoth:war mammoth').
card_rarity('war mammoth'/'RQS', 'Special').
card_artist('war mammoth'/'RQS', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'RQS', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').

card_in_set('warp artifact', 'RQS').
card_original_type('warp artifact'/'RQS', 'Enchant Artifact').
card_original_text('warp artifact'/'RQS', 'Warp Artifact deals 1 damage to target artifact\'s controller during his or her upkeep.').
card_image_name('warp artifact'/'RQS', 'warp artifact').
card_uid('warp artifact'/'RQS', 'RQS:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'RQS', 'Special').
card_artist('warp artifact'/'RQS', 'Amy Weber').

card_in_set('weakness', 'RQS').
card_original_type('weakness'/'RQS', 'Enchant Creature').
card_original_text('weakness'/'RQS', 'Target creature gets -2/-1.').
card_image_name('weakness'/'RQS', 'weakness').
card_uid('weakness'/'RQS', 'RQS:Weakness:weakness').
card_rarity('weakness'/'RQS', 'Special').
card_artist('weakness'/'RQS', 'Douglas Shuler').

card_in_set('whirling dervish', 'RQS').
card_original_type('whirling dervish'/'RQS', 'Summon — Dervish').
card_original_text('whirling dervish'/'RQS', 'Protection from black\nPut a +1/+1 counter on Whirling Dervish at the end of each turn in which it damages opponent.').
card_image_name('whirling dervish'/'RQS', 'whirling dervish').
card_uid('whirling dervish'/'RQS', 'RQS:Whirling Dervish:whirling dervish').
card_rarity('whirling dervish'/'RQS', 'Special').
card_artist('whirling dervish'/'RQS', 'Susan Van Camp').

card_in_set('winter blast', 'RQS').
card_original_type('winter blast'/'RQS', 'Sorcery').
card_original_text('winter blast'/'RQS', 'Tap X target creatures. Winter Blast deals 2 damage to each of these target creatures with flying.').
card_image_name('winter blast'/'RQS', 'winter blast').
card_uid('winter blast'/'RQS', 'RQS:Winter Blast:winter blast').
card_rarity('winter blast'/'RQS', 'Special').
card_artist('winter blast'/'RQS', 'Kaja Foglio').
card_flavor_text('winter blast'/'RQS', '\"Blow, winds, and crack your cheeks! rage! blow!\" —William Shakespeare, King Lear').

card_in_set('zephyr falcon', 'RQS').
card_original_type('zephyr falcon'/'RQS', 'Summon — Falcon').
card_original_text('zephyr falcon'/'RQS', 'Flying\nAttacking does not cause Zephyr Falcon to tap.').
card_image_name('zephyr falcon'/'RQS', 'zephyr falcon').
card_uid('zephyr falcon'/'RQS', 'RQS:Zephyr Falcon:zephyr falcon').
card_rarity('zephyr falcon'/'RQS', 'Special').
card_artist('zephyr falcon'/'RQS', 'Heather Hudson').
card_flavor_text('zephyr falcon'/'RQS', 'Although greatly prized among falconers, the Zephyr Falcon is capricious and not easily tamed.').
