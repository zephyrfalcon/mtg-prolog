% Masters Edition II

set('ME2').
set_name('ME2', 'Masters Edition II').
set_release_date('ME2', '2008-09-22').
set_border('ME2', 'black').
set_type('ME2', 'masters').

card_in_set('abbey gargoyles', 'ME2').
card_original_type('abbey gargoyles'/'ME2', 'Creature — Gargoyle').
card_original_text('abbey gargoyles'/'ME2', 'Flying, protection from red').
card_image_name('abbey gargoyles'/'ME2', 'abbey gargoyles').
card_uid('abbey gargoyles'/'ME2', 'ME2:Abbey Gargoyles:abbey gargoyles').
card_rarity('abbey gargoyles'/'ME2', 'Uncommon').
card_artist('abbey gargoyles'/'ME2', 'Christopher Rush').
card_number('abbey gargoyles'/'ME2', '1').
card_flavor_text('abbey gargoyles'/'ME2', '\"Though Serra has not been seen for twenty years, her gargoyles still watch over our city and punish the guilty.\"\n—Gulsen, abbey matron').
card_multiverse_id('abbey gargoyles'/'ME2', '184585').

card_in_set('adarkar sentinel', 'ME2').
card_original_type('adarkar sentinel'/'ME2', 'Artifact Creature — Soldier').
card_original_text('adarkar sentinel'/'ME2', '{1}: Adarkar Sentinel gets +0/+1 until end of turn.').
card_image_name('adarkar sentinel'/'ME2', 'adarkar sentinel').
card_uid('adarkar sentinel'/'ME2', 'ME2:Adarkar Sentinel:adarkar sentinel').
card_rarity('adarkar sentinel'/'ME2', 'Common').
card_artist('adarkar sentinel'/'ME2', 'Melissa A. Benson').
card_number('adarkar sentinel'/'ME2', '201').
card_flavor_text('adarkar sentinel'/'ME2', '\"We encountered the sentinels in the wastes, near no living thing. Their purpose was inscrutable.\"\n—Disa the Restless, journal entry').
card_multiverse_id('adarkar sentinel'/'ME2', '184599').

card_in_set('aeolipile', 'ME2').
card_original_type('aeolipile'/'ME2', 'Artifact').
card_original_text('aeolipile'/'ME2', '{1}, {T}, Sacrifice Aeolipile: Aeolipile deals 2 damage to target creature or player.').
card_image_name('aeolipile'/'ME2', 'aeolipile').
card_uid('aeolipile'/'ME2', 'ME2:Aeolipile:aeolipile').
card_rarity('aeolipile'/'ME2', 'Common').
card_artist('aeolipile'/'ME2', 'Heather Hudson').
card_number('aeolipile'/'ME2', '202').
card_flavor_text('aeolipile'/'ME2', '\"Although fragile, the aeolipile could be quite destructive.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('aeolipile'/'ME2', '184464').

card_in_set('æther storm', 'ME2').
card_original_type('æther storm'/'ME2', 'Enchantment').
card_original_text('æther storm'/'ME2', 'Creature cards can\'t be played.\nPay 4 life: Destroy Æther Storm. It can\'t be regenerated. Any player may play this ability.').
card_image_name('æther storm'/'ME2', 'aether storm').
card_uid('æther storm'/'ME2', 'ME2:Æther Storm:aether storm').
card_rarity('æther storm'/'ME2', 'Uncommon').
card_artist('æther storm'/'ME2', 'Mark Tedin').
card_number('æther storm'/'ME2', '39').
card_flavor_text('æther storm'/'ME2', '\"I do love the rain . . . but this storm feels somehow wrong, Taysir.\"\n—Daria').
card_multiverse_id('æther storm'/'ME2', '184722').

card_in_set('ambush party', 'ME2').
card_original_type('ambush party'/'ME2', 'Creature — Human Rogue').
card_original_text('ambush party'/'ME2', 'First strike, haste').
card_image_name('ambush party'/'ME2', 'ambush party').
card_uid('ambush party'/'ME2', 'ME2:Ambush Party:ambush party').
card_rarity('ambush party'/'ME2', 'Common').
card_artist('ambush party'/'ME2', 'Charles Gillespie').
card_number('ambush party'/'ME2', '115').
card_flavor_text('ambush party'/'ME2', '\"Fair fight? I\'ll take survival over chivalry any day!\"\n—Tor Wauki, Bandit King').
card_multiverse_id('ambush party'/'ME2', '184543').

card_in_set('an-zerrin ruins', 'ME2').
card_original_type('an-zerrin ruins'/'ME2', 'Enchantment').
card_original_text('an-zerrin ruins'/'ME2', 'As An-Zerrin Ruins comes into play, choose a creature type.\nCreatures of the chosen type don\'t untap during their controllers\' untap steps.').
card_image_name('an-zerrin ruins'/'ME2', 'an-zerrin ruins').
card_uid('an-zerrin ruins'/'ME2', 'ME2:An-Zerrin Ruins:an-zerrin ruins').
card_rarity('an-zerrin ruins'/'ME2', 'Rare').
card_artist('an-zerrin ruins'/'ME2', 'Dennis Detwiller').
card_number('an-zerrin ruins'/'ME2', '117').
card_flavor_text('an-zerrin ruins'/'ME2', '\"The An-Zerrins have served me well, ever since I first killed them.\"\n—Baron Sengir').
card_multiverse_id('an-zerrin ruins'/'ME2', '184586').

card_in_set('anarchy', 'ME2').
card_original_type('anarchy'/'ME2', 'Sorcery').
card_original_text('anarchy'/'ME2', 'Destroy all white permanents.').
card_image_name('anarchy'/'ME2', 'anarchy').
card_uid('anarchy'/'ME2', 'ME2:Anarchy:anarchy').
card_rarity('anarchy'/'ME2', 'Rare').
card_artist('anarchy'/'ME2', 'Phil Foglio').
card_number('anarchy'/'ME2', '116').
card_flavor_text('anarchy'/'ME2', '\"The shaman waved the staff, and the land itself went mad.\"\n—Disa the Restless, journal entry').
card_multiverse_id('anarchy'/'ME2', '184600').

card_in_set('angel of fury', 'ME2').
card_original_type('angel of fury'/'ME2', 'Creature — Angel').
card_original_text('angel of fury'/'ME2', 'Flying\nWhen Angel of Fury is put into your graveyard from play, you may shuffle it into your library.').
card_image_name('angel of fury'/'ME2', 'angel of fury').
card_uid('angel of fury'/'ME2', 'ME2:Angel of Fury:angel of fury').
card_rarity('angel of fury'/'ME2', 'Rare').
card_artist('angel of fury'/'ME2', 'Keith Parkinson').
card_number('angel of fury'/'ME2', '2').
card_multiverse_id('angel of fury'/'ME2', '184746').

card_in_set('angel of light', 'ME2').
card_original_type('angel of light'/'ME2', 'Creature — Angel').
card_original_text('angel of light'/'ME2', 'Flying, vigilance').
card_image_name('angel of light'/'ME2', 'angel of light').
card_uid('angel of light'/'ME2', 'ME2:Angel of Light:angel of light').
card_rarity('angel of light'/'ME2', 'Uncommon').
card_artist('angel of light'/'ME2', 'Todd Lockwood').
card_number('angel of light'/'ME2', '3').
card_multiverse_id('angel of light'/'ME2', '184753').

card_in_set('armor of faith', 'ME2').
card_original_type('armor of faith'/'ME2', 'Enchantment — Aura').
card_original_text('armor of faith'/'ME2', 'Enchant creature\nEnchanted creature gets +1/+1.\n{W}: Enchanted creature gets +0/+1 until end of turn.').
card_image_name('armor of faith'/'ME2', 'armor of faith').
card_uid('armor of faith'/'ME2', 'ME2:Armor of Faith:armor of faith').
card_rarity('armor of faith'/'ME2', 'Common').
card_artist('armor of faith'/'ME2', 'Anson Maddocks').
card_number('armor of faith'/'ME2', '4').
card_flavor_text('armor of faith'/'ME2', '\"Keep your chainmail, warrior. I have my own form of protection.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('armor of faith'/'ME2', '184726').

card_in_set('armor thrull', 'ME2').
card_original_type('armor thrull'/'ME2', 'Creature — Thrull').
card_original_text('armor thrull'/'ME2', '{T}, Sacrifice Armor Thrull: Put a +1/+2 counter on target creature.').
card_image_name('armor thrull'/'ME2', 'armor thrull').
card_uid('armor thrull'/'ME2', 'ME2:Armor Thrull:armor thrull').
card_rarity('armor thrull'/'ME2', 'Common').
card_artist('armor thrull'/'ME2', 'Pete Venters').
card_number('armor thrull'/'ME2', '77').
card_flavor_text('armor thrull'/'ME2', '\"The worst thing about being a mercenary for the Ebon Hand is having to wear a dead thrull.\"\n—Ivra Jursdotter').
card_multiverse_id('armor thrull'/'ME2', '184684').

card_in_set('armored griffin', 'ME2').
card_original_type('armored griffin'/'ME2', 'Creature — Griffin').
card_original_text('armored griffin'/'ME2', 'Flying, vigilance').
card_image_name('armored griffin'/'ME2', 'armored griffin').
card_uid('armored griffin'/'ME2', 'ME2:Armored Griffin:armored griffin').
card_rarity('armored griffin'/'ME2', 'Common').
card_artist('armored griffin'/'ME2', 'Bradley Williams').
card_number('armored griffin'/'ME2', '5').
card_multiverse_id('armored griffin'/'ME2', '184728').

card_in_set('ashen ghoul', 'ME2').
card_original_type('ashen ghoul'/'ME2', 'Creature — Zombie').
card_original_text('ashen ghoul'/'ME2', 'Haste\n{B}: Return Ashen Ghoul from your graveyard to play. Play this ability only during your upkeep and only if three or more creature cards are above Ashen Ghoul.').
card_image_name('ashen ghoul'/'ME2', 'ashen ghoul').
card_uid('ashen ghoul'/'ME2', 'ME2:Ashen Ghoul:ashen ghoul').
card_rarity('ashen ghoul'/'ME2', 'Uncommon').
card_artist('ashen ghoul'/'ME2', 'Ron Spencer').
card_number('ashen ghoul'/'ME2', '78').
card_multiverse_id('ashen ghoul'/'ME2', '184601').

card_in_set('ashnod\'s cylix', 'ME2').
card_original_type('ashnod\'s cylix'/'ME2', 'Artifact').
card_original_text('ashnod\'s cylix'/'ME2', '{3}, {T}: Target player looks at the top three cards of his or her library, puts one of them back on top of his or her library, then removes the rest from the game.').
card_image_name('ashnod\'s cylix'/'ME2', 'ashnod\'s cylix').
card_uid('ashnod\'s cylix'/'ME2', 'ME2:Ashnod\'s Cylix:ashnod\'s cylix').
card_rarity('ashnod\'s cylix'/'ME2', 'Rare').
card_artist('ashnod\'s cylix'/'ME2', 'Nicola Leonard').
card_number('ashnod\'s cylix'/'ME2', '203').
card_flavor_text('ashnod\'s cylix'/'ME2', 'Few remember that Ashnod\'s defilement of Terisiare\'s resources outstripped even that of her peers.').
card_multiverse_id('ashnod\'s cylix'/'ME2', '184760').

card_in_set('aurochs', 'ME2').
card_original_type('aurochs'/'ME2', 'Creature — Aurochs').
card_original_text('aurochs'/'ME2', 'Trample\nWhenever Aurochs attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.').
card_image_name('aurochs'/'ME2', 'aurochs').
card_uid('aurochs'/'ME2', 'ME2:Aurochs:aurochs').
card_rarity('aurochs'/'ME2', 'Common').
card_artist('aurochs'/'ME2', 'Ken Meyer, Jr.').
card_number('aurochs'/'ME2', '153').
card_flavor_text('aurochs'/'ME2', 'One aurochs may feed a village, but a herd will flatten it.').
card_multiverse_id('aurochs'/'ME2', '184712').

card_in_set('aysen bureaucrats', 'ME2').
card_original_type('aysen bureaucrats'/'ME2', 'Creature — Human Advisor').
card_original_text('aysen bureaucrats'/'ME2', '{T}: Tap target creature with power 2 or less.').
card_image_name('aysen bureaucrats'/'ME2', 'aysen bureaucrats').
card_uid('aysen bureaucrats'/'ME2', 'ME2:Aysen Bureaucrats:aysen bureaucrats').
card_rarity('aysen bureaucrats'/'ME2', 'Common').
card_artist('aysen bureaucrats'/'ME2', 'Alan Rabinowitz').
card_number('aysen bureaucrats'/'ME2', '6').
card_flavor_text('aysen bureaucrats'/'ME2', '\"I would say that our bureaucrats are no better than vipers—but I shouldn\'t insult the vipers.\"\n—Murat, death speaker').
card_multiverse_id('aysen bureaucrats'/'ME2', '184490').

card_in_set('aysen crusader', 'ME2').
card_original_type('aysen crusader'/'ME2', 'Creature — Human Knight').
card_original_text('aysen crusader'/'ME2', 'Aysen Crusader\'s power and toughness are each equal to 2 plus the number of Soldiers and Warriors you control.').
card_image_name('aysen crusader'/'ME2', 'aysen crusader').
card_uid('aysen crusader'/'ME2', 'ME2:Aysen Crusader:aysen crusader').
card_rarity('aysen crusader'/'ME2', 'Uncommon').
card_artist('aysen crusader'/'ME2', 'NéNé Thomas').
card_number('aysen crusader'/'ME2', '7').
card_flavor_text('aysen crusader'/'ME2', '\"A renegade rallying the rabble does not a true crusader make.\"\n—Irini Sengir').
card_multiverse_id('aysen crusader'/'ME2', '184587').

card_in_set('badlands', 'ME2').
card_original_type('badlands'/'ME2', 'Land — Swamp Mountain').
card_original_text('badlands'/'ME2', '').
card_image_name('badlands'/'ME2', 'badlands').
card_uid('badlands'/'ME2', 'ME2:Badlands:badlands').
card_rarity('badlands'/'ME2', 'Rare').
card_artist('badlands'/'ME2', 'Rob Alexander').
card_number('badlands'/'ME2', '225').
card_multiverse_id('badlands'/'ME2', '184748').

card_in_set('balduvian conjurer', 'ME2').
card_original_type('balduvian conjurer'/'ME2', 'Creature — Human Wizard').
card_original_text('balduvian conjurer'/'ME2', '{T}: Target snow land becomes a 2/2 creature until end of turn. It\'s still a land.').
card_image_name('balduvian conjurer'/'ME2', 'balduvian conjurer').
card_uid('balduvian conjurer'/'ME2', 'ME2:Balduvian Conjurer:balduvian conjurer').
card_rarity('balduvian conjurer'/'ME2', 'Common').
card_artist('balduvian conjurer'/'ME2', 'Mark Tedin').
card_number('balduvian conjurer'/'ME2', '40').
card_flavor_text('balduvian conjurer'/'ME2', '\"The very lands of Balduvia are alive.\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('balduvian conjurer'/'ME2', '184603').

card_in_set('balduvian dead', 'ME2').
card_original_type('balduvian dead'/'ME2', 'Creature — Zombie').
card_original_text('balduvian dead'/'ME2', '{2}{R}, Remove a creature card in your graveyard from the game: Put a 3/1 black and red Graveborn creature token with haste into play. Sacrifice it at end of turn.').
card_image_name('balduvian dead'/'ME2', 'balduvian dead').
card_uid('balduvian dead'/'ME2', 'ME2:Balduvian Dead:balduvian dead').
card_rarity('balduvian dead'/'ME2', 'Uncommon').
card_artist('balduvian dead'/'ME2', 'Mike Kimble').
card_number('balduvian dead'/'ME2', '79').
card_multiverse_id('balduvian dead'/'ME2', '184735').

card_in_set('balduvian hydra', 'ME2').
card_original_type('balduvian hydra'/'ME2', 'Creature — Hydra').
card_original_text('balduvian hydra'/'ME2', 'Balduvian Hydra comes into play with X +1/+0 counters on it.\nRemove a +1/+0 counter from Balduvian Hydra: Prevent the next 1 damage that would be dealt to Balduvian Hydra this turn.\n{R}{R}{R}: Put a +1/+0 counter on Balduvian Hydra. Play this ability only during your upkeep.').
card_image_name('balduvian hydra'/'ME2', 'balduvian hydra').
card_uid('balduvian hydra'/'ME2', 'ME2:Balduvian Hydra:balduvian hydra').
card_rarity('balduvian hydra'/'ME2', 'Rare').
card_artist('balduvian hydra'/'ME2', 'Melissa A. Benson').
card_number('balduvian hydra'/'ME2', '118').
card_multiverse_id('balduvian hydra'/'ME2', '184757').

card_in_set('balduvian trading post', 'ME2').
card_original_type('balduvian trading post'/'ME2', 'Land').
card_original_text('balduvian trading post'/'ME2', 'If Balduvian Trading Post would come into play, sacrifice an untapped Mountain instead. If you do, put Balduvian Trading Post into play. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {1}{R} to your mana pool.\n{1}, {T}: Balduvian Trading Post deals 1 damage to target attacking creature.').
card_image_name('balduvian trading post'/'ME2', 'balduvian trading post').
card_uid('balduvian trading post'/'ME2', 'ME2:Balduvian Trading Post:balduvian trading post').
card_rarity('balduvian trading post'/'ME2', 'Rare').
card_artist('balduvian trading post'/'ME2', 'Tom Wänerstrand').
card_number('balduvian trading post'/'ME2', '226').
card_multiverse_id('balduvian trading post'/'ME2', '184533').

card_in_set('barbed sextant', 'ME2').
card_original_type('barbed sextant'/'ME2', 'Artifact').
card_original_text('barbed sextant'/'ME2', '{1}, {T}, Sacrifice Barbed Sextant: Add one mana of any color to your mana pool. Draw a card at the beginning of the next turn\'s upkeep.').
card_image_name('barbed sextant'/'ME2', 'barbed sextant').
card_uid('barbed sextant'/'ME2', 'ME2:Barbed Sextant:barbed sextant').
card_rarity('barbed sextant'/'ME2', 'Common').
card_artist('barbed sextant'/'ME2', 'Amy Weber').
card_number('barbed sextant'/'ME2', '204').
card_multiverse_id('barbed sextant'/'ME2', '184604').

card_in_set('binding grasp', 'ME2').
card_original_type('binding grasp'/'ME2', 'Enchantment — Aura').
card_original_text('binding grasp'/'ME2', 'Enchant creature\nAt the beginning of your upkeep, sacrifice Binding Grasp unless you pay {1}{U}.\nYou control enchanted creature.\nEnchanted creature gets +0/+1.').
card_image_name('binding grasp'/'ME2', 'binding grasp').
card_uid('binding grasp'/'ME2', 'ME2:Binding Grasp:binding grasp').
card_rarity('binding grasp'/'ME2', 'Rare').
card_artist('binding grasp'/'ME2', 'Ruth Thompson').
card_number('binding grasp'/'ME2', '41').
card_flavor_text('binding grasp'/'ME2', '\"What I want, I take.\"\n—Gustha Ebbasdotter, Kjeldoran royal mage').
card_multiverse_id('binding grasp'/'ME2', '184605').

card_in_set('bounty of the hunt', 'ME2').
card_original_type('bounty of the hunt'/'ME2', 'Instant').
card_original_text('bounty of the hunt'/'ME2', 'You may remove a green card in your hand from the game rather than pay Bounty of the Hunt\'s mana cost.\nUntil end of turn, target creature gets +1/+1, target creature gets +1/+1, and target creature gets +1/+1.').
card_image_name('bounty of the hunt'/'ME2', 'bounty of the hunt').
card_uid('bounty of the hunt'/'ME2', 'ME2:Bounty of the Hunt:bounty of the hunt').
card_rarity('bounty of the hunt'/'ME2', 'Rare').
card_artist('bounty of the hunt'/'ME2', 'Jeff A. Menges').
card_number('bounty of the hunt'/'ME2', '154').
card_multiverse_id('bounty of the hunt'/'ME2', '184534').

card_in_set('brainstorm', 'ME2').
card_original_type('brainstorm'/'ME2', 'Instant').
card_original_text('brainstorm'/'ME2', 'Draw three cards, then put two cards from your hand on top of your library in any order.').
card_image_name('brainstorm'/'ME2', 'brainstorm').
card_uid('brainstorm'/'ME2', 'ME2:Brainstorm:brainstorm').
card_rarity('brainstorm'/'ME2', 'Common').
card_artist('brainstorm'/'ME2', 'Christopher Rush').
card_number('brainstorm'/'ME2', '42').
card_flavor_text('brainstorm'/'ME2', '\"I reeled from the blow, and then suddenly, I knew exactly what to do. Within moments, victory was mine.\"\n—Gustha Ebbasdotter, Kjeldoran royal mage').
card_multiverse_id('brainstorm'/'ME2', '184606').

card_in_set('brassclaw orcs', 'ME2').
card_original_type('brassclaw orcs'/'ME2', 'Creature — Orc').
card_original_text('brassclaw orcs'/'ME2', 'Brassclaw Orcs can\'t block creatures with power 2 or greater.').
card_image_name('brassclaw orcs'/'ME2', 'brassclaw orcs').
card_uid('brassclaw orcs'/'ME2', 'ME2:Brassclaw Orcs:brassclaw orcs').
card_rarity('brassclaw orcs'/'ME2', 'Common').
card_artist('brassclaw orcs'/'ME2', 'Rob Alexander').
card_number('brassclaw orcs'/'ME2', '119').
card_flavor_text('brassclaw orcs'/'ME2', '\"A whole skin is worth a thousand victories.\"\n—Orcish veteran of the Battle of Montford').
card_multiverse_id('brassclaw orcs'/'ME2', '184685').

card_in_set('brimstone dragon', 'ME2').
card_original_type('brimstone dragon'/'ME2', 'Creature — Dragon').
card_original_text('brimstone dragon'/'ME2', 'Flying, haste').
card_image_name('brimstone dragon'/'ME2', 'brimstone dragon').
card_uid('brimstone dragon'/'ME2', 'ME2:Brimstone Dragon:brimstone dragon').
card_rarity('brimstone dragon'/'ME2', 'Rare').
card_artist('brimstone dragon'/'ME2', 'David A. Cherry').
card_number('brimstone dragon'/'ME2', '120').
card_multiverse_id('brimstone dragon'/'ME2', '184767').

card_in_set('brine shaman', 'ME2').
card_original_type('brine shaman'/'ME2', 'Creature — Human Cleric Shaman').
card_original_text('brine shaman'/'ME2', '{T}: Sacrifice a creature: Target creature gets +2/+2 until end of turn.\n{1}{U}{U}, Sacrifice a creature: Counter target creature spell.').
card_image_name('brine shaman'/'ME2', 'brine shaman').
card_uid('brine shaman'/'ME2', 'ME2:Brine Shaman:brine shaman').
card_rarity('brine shaman'/'ME2', 'Common').
card_artist('brine shaman'/'ME2', 'Cornelius Brudi').
card_number('brine shaman'/'ME2', '80').
card_flavor_text('brine shaman'/'ME2', '\"The shamans of Marit Lage do her bidding in secret, but they do it gladly.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('brine shaman'/'ME2', '184736').

card_in_set('broken visage', 'ME2').
card_original_type('broken visage'/'ME2', 'Instant').
card_original_text('broken visage'/'ME2', 'Destroy target nonartifact attacking creature. It can\'t be regenerated. Put a black Spirit creature token into play with that creature\'s power and toughness. Sacrifice the token at end of turn.').
card_image_name('broken visage'/'ME2', 'broken visage').
card_uid('broken visage'/'ME2', 'ME2:Broken Visage:broken visage').
card_rarity('broken visage'/'ME2', 'Uncommon').
card_artist('broken visage'/'ME2', 'Mike Kimble').
card_number('broken visage'/'ME2', '81').
card_multiverse_id('broken visage'/'ME2', '184589').

card_in_set('browse', 'ME2').
card_original_type('browse'/'ME2', 'Enchantment').
card_original_text('browse'/'ME2', '{2}{U}{U}: Look at the top five cards of your library and put one of them into your hand. Remove the rest from the game.').
card_image_name('browse'/'ME2', 'browse').
card_uid('browse'/'ME2', 'ME2:Browse:browse').
card_rarity('browse'/'ME2', 'Uncommon').
card_artist('browse'/'ME2', 'Phil Foglio').
card_number('browse'/'ME2', '43').
card_flavor_text('browse'/'ME2', '\"Once great literature—now great litter.\"\n—Jaya Ballard, task mage').
card_multiverse_id('browse'/'ME2', '184761').

card_in_set('burnout', 'ME2').
card_original_type('burnout'/'ME2', 'Instant').
card_original_text('burnout'/'ME2', 'Counter target instant spell if it\'s blue.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('burnout'/'ME2', 'burnout').
card_uid('burnout'/'ME2', 'ME2:Burnout:burnout').
card_rarity('burnout'/'ME2', 'Uncommon').
card_artist('burnout'/'ME2', 'Mike Raabe').
card_number('burnout'/'ME2', '121').
card_flavor_text('burnout'/'ME2', '\"GOTCHA!\"\n—Jaya Ballard, task mage').
card_multiverse_id('burnout'/'ME2', '184762').

card_in_set('carapace', 'ME2').
card_original_type('carapace'/'ME2', 'Enchantment — Aura').
card_original_text('carapace'/'ME2', 'Enchant creature\nEnchanted creature gets +0/+2.\nSacrifice Carapace: Regenerate enchanted creature.').
card_image_name('carapace'/'ME2', 'carapace').
card_uid('carapace'/'ME2', 'ME2:Carapace:carapace').
card_rarity('carapace'/'ME2', 'Common').
card_artist('carapace'/'ME2', 'Anson Maddocks').
card_number('carapace'/'ME2', '155').
card_multiverse_id('carapace'/'ME2', '184777').

card_in_set('caribou range', 'ME2').
card_original_type('caribou range'/'ME2', 'Enchantment — Aura').
card_original_text('caribou range'/'ME2', 'Enchant land you control\nEnchanted land has \"{W}{W}, {T}: Put a 0/1 white Caribou creature token into play.\"\nSacrifice a Caribou token: You gain 1 life.').
card_image_name('caribou range'/'ME2', 'caribou range').
card_uid('caribou range'/'ME2', 'ME2:Caribou Range:caribou range').
card_rarity('caribou range'/'ME2', 'Rare').
card_artist('caribou range'/'ME2', 'Ruth Thompson').
card_number('caribou range'/'ME2', '8').
card_multiverse_id('caribou range'/'ME2', '184607').

card_in_set('cloak of confusion', 'ME2').
card_original_type('cloak of confusion'/'ME2', 'Enchantment — Aura').
card_original_text('cloak of confusion'/'ME2', 'Enchant creature you control\nWhenever enchanted creature attacks and isn\'t blocked, you may have it deal no combat damage this turn. If you do, defending player discards a card at random.').
card_image_name('cloak of confusion'/'ME2', 'cloak of confusion').
card_uid('cloak of confusion'/'ME2', 'ME2:Cloak of Confusion:cloak of confusion').
card_rarity('cloak of confusion'/'ME2', 'Common').
card_artist('cloak of confusion'/'ME2', 'Margaret Organ-Kean').
card_number('cloak of confusion'/'ME2', '82').
card_multiverse_id('cloak of confusion'/'ME2', '184669').

card_in_set('clockwork steed', 'ME2').
card_original_type('clockwork steed'/'ME2', 'Artifact Creature — Horse').
card_original_text('clockwork steed'/'ME2', 'Clockwork Steed comes into play with four +1/+0 counters on it.\nClockwork Steed can\'t be blocked by artifact creatures.\nAt end of combat, if Clockwork Steed attacked or blocked this combat, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Steed. This ability can\'t cause the total number of +1/+0 counters on Clockwork Steed to be greater than four. Play this ability only during your upkeep.').
card_image_name('clockwork steed'/'ME2', 'clockwork steed').
card_uid('clockwork steed'/'ME2', 'ME2:Clockwork Steed:clockwork steed').
card_rarity('clockwork steed'/'ME2', 'Uncommon').
card_artist('clockwork steed'/'ME2', 'Terese Nielsen').
card_number('clockwork steed'/'ME2', '205').
card_multiverse_id('clockwork steed'/'ME2', '184590').

card_in_set('combat medic', 'ME2').
card_original_type('combat medic'/'ME2', 'Creature — Human Soldier').
card_original_text('combat medic'/'ME2', '{1}{W}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_image_name('combat medic'/'ME2', 'combat medic').
card_uid('combat medic'/'ME2', 'ME2:Combat Medic:combat medic').
card_rarity('combat medic'/'ME2', 'Common').
card_artist('combat medic'/'ME2', 'Anson Maddocks').
card_number('combat medic'/'ME2', '9').
card_flavor_text('combat medic'/'ME2', '\"Without combat medics, Icatia would probably not have withstood the forces of chaos as long as it did.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('combat medic'/'ME2', '184686').

card_in_set('conquer', 'ME2').
card_original_type('conquer'/'ME2', 'Enchantment — Aura').
card_original_text('conquer'/'ME2', 'Enchant land\nYou control enchanted land.').
card_image_name('conquer'/'ME2', 'conquer').
card_uid('conquer'/'ME2', 'ME2:Conquer:conquer').
card_rarity('conquer'/'ME2', 'Uncommon').
card_artist('conquer'/'ME2', 'Randy Gallegos').
card_number('conquer'/'ME2', '122').
card_flavor_text('conquer'/'ME2', '\"Why do we trade with those despicable elves? You don\'t live in forests, you burn them!\"\n—Avram Garrisson, leader of the Knights of Stromgald').
card_multiverse_id('conquer'/'ME2', '184609').

card_in_set('counterspell', 'ME2').
card_original_type('counterspell'/'ME2', 'Instant').
card_original_text('counterspell'/'ME2', 'Counter target spell.').
card_image_name('counterspell'/'ME2', 'counterspell').
card_uid('counterspell'/'ME2', 'ME2:Counterspell:counterspell').
card_rarity('counterspell'/'ME2', 'Uncommon').
card_artist('counterspell'/'ME2', 'L. A. Williams').
card_number('counterspell'/'ME2', '44').
card_flavor_text('counterspell'/'ME2', '\"The duel was going badly for me, and Zur thought I was finished. He boasted that he would eat my soul—but all he ate were his words.\"\n—Gustha Ebbasdotter, Kjeldoran royal mage').
card_multiverse_id('counterspell'/'ME2', '184687').

card_in_set('dance of the dead', 'ME2').
card_original_type('dance of the dead'/'ME2', 'Enchantment — Aura').
card_original_text('dance of the dead'/'ME2', 'Enchant creature card in a graveyard\nWhen Dance of the Dead comes into play, if it\'s in play, it loses \"enchant creature card in a graveyard\" and gains \"enchant creature put into play with Dance of the Dead.\" Return enchanted creature card to play tapped under your control and attach Dance of the Dead to it. When Dance of the Dead leaves play, that creature\'s controller sacrifices it.\nEnchanted creature gets +1/+1 and doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may pay {1}{B}. If he or she does, untap that creature.').
card_image_name('dance of the dead'/'ME2', 'dance of the dead').
card_uid('dance of the dead'/'ME2', 'ME2:Dance of the Dead:dance of the dead').
card_rarity('dance of the dead'/'ME2', 'Uncommon').
card_artist('dance of the dead'/'ME2', 'Randy Gallegos').
card_number('dance of the dead'/'ME2', '83').
card_multiverse_id('dance of the dead'/'ME2', '184612').

card_in_set('dark banishing', 'ME2').
card_original_type('dark banishing'/'ME2', 'Instant').
card_original_text('dark banishing'/'ME2', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'ME2', 'dark banishing').
card_uid('dark banishing'/'ME2', 'ME2:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'ME2', 'Common').
card_artist('dark banishing'/'ME2', 'Drew Tucker').
card_number('dark banishing'/'ME2', '84').
card_flavor_text('dark banishing'/'ME2', '\"Will not the mountains quake and hills melt at the coming of the darkness? Share this vision with your enemies, Lim-Dûl, and they shall wither.\"\n—Leshrac, Walker of Night').
card_multiverse_id('dark banishing'/'ME2', '184613').

card_in_set('death spark', 'ME2').
card_original_type('death spark'/'ME2', 'Instant').
card_original_text('death spark'/'ME2', 'Death Spark deals 1 damage to target creature or player.\nAt the beginning of your upkeep, if Death Spark is in your graveyard with a creature card directly above it, you may pay {1}. If you do, return Death Spark to your hand.').
card_image_name('death spark'/'ME2', 'death spark').
card_uid('death spark'/'ME2', 'ME2:Death Spark:death spark').
card_rarity('death spark'/'ME2', 'Common').
card_artist('death spark'/'ME2', 'Mark Tedin').
card_number('death spark'/'ME2', '123').
card_multiverse_id('death spark'/'ME2', '184537').

card_in_set('deep spawn', 'ME2').
card_original_type('deep spawn'/'ME2', 'Creature — Homarid').
card_original_text('deep spawn'/'ME2', 'Trample\nAt the beginning of your upkeep, sacrifice Deep Spawn unless you put the top two cards of your library into your graveyard.\n{U}: Deep Spawn gains shroud until end of turn and doesn\'t untap during your next untap step. Tap Deep Spawn.').
card_image_name('deep spawn'/'ME2', 'deep spawn').
card_uid('deep spawn'/'ME2', 'ME2:Deep Spawn:deep spawn').
card_rarity('deep spawn'/'ME2', 'Rare').
card_artist('deep spawn'/'ME2', 'Mark Tedin').
card_number('deep spawn'/'ME2', '45').
card_multiverse_id('deep spawn'/'ME2', '184477').

card_in_set('demonic consultation', 'ME2').
card_original_type('demonic consultation'/'ME2', 'Instant').
card_original_text('demonic consultation'/'ME2', 'Name a card. Remove the top six cards of your library from the game, then reveal cards from the top of your library until you reveal the named card. Put that card into your hand and remove all other cards revealed this way from the game.').
card_image_name('demonic consultation'/'ME2', 'demonic consultation').
card_uid('demonic consultation'/'ME2', 'ME2:Demonic Consultation:demonic consultation').
card_rarity('demonic consultation'/'ME2', 'Uncommon').
card_artist('demonic consultation'/'ME2', 'Rob Alexander').
card_number('demonic consultation'/'ME2', '85').
card_multiverse_id('demonic consultation'/'ME2', '184614').

card_in_set('despotic scepter', 'ME2').
card_original_type('despotic scepter'/'ME2', 'Artifact').
card_original_text('despotic scepter'/'ME2', '{T}: Destroy target permanent you own. It can\'t be regenerated.').
card_image_name('despotic scepter'/'ME2', 'despotic scepter').
card_uid('despotic scepter'/'ME2', 'ME2:Despotic Scepter:despotic scepter').
card_rarity('despotic scepter'/'ME2', 'Rare').
card_artist('despotic scepter'/'ME2', 'Richard Thomas').
card_number('despotic scepter'/'ME2', '206').
card_flavor_text('despotic scepter'/'ME2', '\"We were not meant to have such terrible things. They should be left where we found them, if not destroyed!\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('despotic scepter'/'ME2', '184615').

card_in_set('diabolic vision', 'ME2').
card_original_type('diabolic vision'/'ME2', 'Sorcery').
card_original_text('diabolic vision'/'ME2', 'Look at the top five cards of your library. Put one of them into your hand and the rest on top of your library in any order.').
card_image_name('diabolic vision'/'ME2', 'diabolic vision').
card_uid('diabolic vision'/'ME2', 'ME2:Diabolic Vision:diabolic vision').
card_rarity('diabolic vision'/'ME2', 'Uncommon').
card_artist('diabolic vision'/'ME2', 'Anthony S. Waters').
card_number('diabolic vision'/'ME2', '191').
card_flavor_text('diabolic vision'/'ME2', '\"I have seen the true path. I will not warm myself by the fire—I will become the flame.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('diabolic vision'/'ME2', '184616').

card_in_set('disenchant', 'ME2').
card_original_type('disenchant'/'ME2', 'Instant').
card_original_text('disenchant'/'ME2', 'Destroy target artifact or enchantment.').
card_image_name('disenchant'/'ME2', 'disenchant').
card_uid('disenchant'/'ME2', 'ME2:Disenchant:disenchant').
card_rarity('disenchant'/'ME2', 'Common').
card_artist('disenchant'/'ME2', 'Brian Snõddy').
card_number('disenchant'/'ME2', '10').
card_flavor_text('disenchant'/'ME2', '\"I implore you not to forget the horrors of the past. You would have us start the Brothers\' War anew!\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('disenchant'/'ME2', '184689').

card_in_set('dreams of the dead', 'ME2').
card_original_type('dreams of the dead'/'ME2', 'Enchantment').
card_original_text('dreams of the dead'/'ME2', '{1}{U}: Return target white or black creature card from your graveyard to play. That creature gains \"Cumulative upkeep {2}.\" If the creature would leave play, remove it from the game instead.').
card_image_name('dreams of the dead'/'ME2', 'dreams of the dead').
card_uid('dreams of the dead'/'ME2', 'ME2:Dreams of the Dead:dreams of the dead').
card_rarity('dreams of the dead'/'ME2', 'Rare').
card_artist('dreams of the dead'/'ME2', 'Heather Hudson').
card_number('dreams of the dead'/'ME2', '46').
card_multiverse_id('dreams of the dead'/'ME2', '184618').

card_in_set('drift of the dead', 'ME2').
card_original_type('drift of the dead'/'ME2', 'Creature — Wall').
card_original_text('drift of the dead'/'ME2', 'Defender\nDrift of the Dead\'s power and toughness are each equal to the number of snow lands you control.').
card_image_name('drift of the dead'/'ME2', 'drift of the dead').
card_uid('drift of the dead'/'ME2', 'ME2:Drift of the Dead:drift of the dead').
card_rarity('drift of the dead'/'ME2', 'Common').
card_artist('drift of the dead'/'ME2', 'Brian Snõddy').
card_number('drift of the dead'/'ME2', '86').
card_flavor_text('drift of the dead'/'ME2', '\"Take their dead, and entomb them in the snow. Risen, they shall serve a new purpose.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('drift of the dead'/'ME2', '184734').

card_in_set('dry spell', 'ME2').
card_original_type('dry spell'/'ME2', 'Sorcery').
card_original_text('dry spell'/'ME2', 'Dry Spell deals 1 damage to each creature and each player.').
card_image_name('dry spell'/'ME2', 'dry spell').
card_uid('dry spell'/'ME2', 'ME2:Dry Spell:dry spell').
card_rarity('dry spell'/'ME2', 'Common').
card_artist('dry spell'/'ME2', 'Brian Snõddy').
card_number('dry spell'/'ME2', '87').
card_flavor_text('dry spell'/'ME2', '\"Wherever water is lacking, all things suffer.\"\n—Autumn Willow').
card_multiverse_id('dry spell'/'ME2', '184703').

card_in_set('dwarven ruins', 'ME2').
card_original_type('dwarven ruins'/'ME2', 'Land').
card_original_text('dwarven ruins'/'ME2', 'Dwarven Ruins comes into play tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Dwarven Ruins: Add {R}{R} to your mana pool.').
card_image_name('dwarven ruins'/'ME2', 'dwarven ruins').
card_uid('dwarven ruins'/'ME2', 'ME2:Dwarven Ruins:dwarven ruins').
card_rarity('dwarven ruins'/'ME2', 'Uncommon').
card_artist('dwarven ruins'/'ME2', 'Mark Poole').
card_number('dwarven ruins'/'ME2', '227').
card_multiverse_id('dwarven ruins'/'ME2', '184478').

card_in_set('dystopia', 'ME2').
card_original_type('dystopia'/'ME2', 'Enchantment').
card_original_text('dystopia'/'ME2', 'Cumulative upkeep—Pay 1 life.\nAt the beginning of each player\'s upkeep, that player sacrifices a white or green permanent.').
card_image_name('dystopia'/'ME2', 'dystopia').
card_uid('dystopia'/'ME2', 'ME2:Dystopia:dystopia').
card_rarity('dystopia'/'ME2', 'Rare').
card_artist('dystopia'/'ME2', 'Ruth Thompson').
card_number('dystopia'/'ME2', '88').
card_multiverse_id('dystopia'/'ME2', '184538').

card_in_set('earthlink', 'ME2').
card_original_type('earthlink'/'ME2', 'Enchantment').
card_original_text('earthlink'/'ME2', 'At the beginning of your upkeep, sacrifice Earthlink unless you pay {2}.\nWhenever a creature is put into a graveyard from play, that creature\'s controller sacrifices a land.').
card_image_name('earthlink'/'ME2', 'earthlink').
card_uid('earthlink'/'ME2', 'ME2:Earthlink:earthlink').
card_rarity('earthlink'/'ME2', 'Rare').
card_artist('earthlink'/'ME2', 'Richard Kane Ferguson').
card_number('earthlink'/'ME2', '192').
card_multiverse_id('earthlink'/'ME2', '184776').

card_in_set('ebon praetor', 'ME2').
card_original_type('ebon praetor'/'ME2', 'Creature — Avatar').
card_original_text('ebon praetor'/'ME2', 'First strike, trample\nAt the beginning of your upkeep, put a -2/-2 counter on Ebon Praetor.\nSacrifice a creature: Remove a -2/-2 counter from Ebon Praetor. If the sacrificed creature was a Thrull, put a +1/+0 counter on Ebon Praetor. Play this ability only during your upkeep and only once each turn.').
card_image_name('ebon praetor'/'ME2', 'ebon praetor').
card_uid('ebon praetor'/'ME2', 'ME2:Ebon Praetor:ebon praetor').
card_rarity('ebon praetor'/'ME2', 'Rare').
card_artist('ebon praetor'/'ME2', 'Randy Asplund-Faith').
card_number('ebon praetor'/'ME2', '89').
card_multiverse_id('ebon praetor'/'ME2', '184482').

card_in_set('ebon stronghold', 'ME2').
card_original_type('ebon stronghold'/'ME2', 'Land').
card_original_text('ebon stronghold'/'ME2', 'Ebon Stronghold comes into play tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Ebon Stronghold: Add {B}{B} to your mana pool.').
card_image_name('ebon stronghold'/'ME2', 'ebon stronghold').
card_uid('ebon stronghold'/'ME2', 'ME2:Ebon Stronghold:ebon stronghold').
card_rarity('ebon stronghold'/'ME2', 'Uncommon').
card_artist('ebon stronghold'/'ME2', 'Mark Poole').
card_number('ebon stronghold'/'ME2', '228').
card_multiverse_id('ebon stronghold'/'ME2', '184483').

card_in_set('elemental augury', 'ME2').
card_original_type('elemental augury'/'ME2', 'Enchantment').
card_original_text('elemental augury'/'ME2', '{3}: Look at the top three cards of target player\'s library, then put them back in any order.').
card_image_name('elemental augury'/'ME2', 'elemental augury').
card_uid('elemental augury'/'ME2', 'ME2:Elemental Augury:elemental augury').
card_rarity('elemental augury'/'ME2', 'Rare').
card_artist('elemental augury'/'ME2', 'Anthony S. Waters').
card_number('elemental augury'/'ME2', '193').
card_flavor_text('elemental augury'/'ME2', '\"It is the changing of perception that is important.\"\n—Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('elemental augury'/'ME2', '184619').

card_in_set('elkin bottle', 'ME2').
card_original_type('elkin bottle'/'ME2', 'Artifact').
card_original_text('elkin bottle'/'ME2', '{3}, {T}: Remove the top card of your library from the game. Until the beginning of your next upkeep, you may play that card.').
card_image_name('elkin bottle'/'ME2', 'elkin bottle').
card_uid('elkin bottle'/'ME2', 'ME2:Elkin Bottle:elkin bottle').
card_rarity('elkin bottle'/'ME2', 'Rare').
card_artist('elkin bottle'/'ME2', 'Quinton Hoover').
card_number('elkin bottle'/'ME2', '207').
card_multiverse_id('elkin bottle'/'ME2', '184620').

card_in_set('elven lyre', 'ME2').
card_original_type('elven lyre'/'ME2', 'Artifact').
card_original_text('elven lyre'/'ME2', '{1}, {T}, Sacrifice Elven Lyre: Target creature gets +2/+2 until end of turn.').
card_image_name('elven lyre'/'ME2', 'elven lyre').
card_uid('elven lyre'/'ME2', 'ME2:Elven Lyre:elven lyre').
card_rarity('elven lyre'/'ME2', 'Common').
card_artist('elven lyre'/'ME2', 'Kaja Foglio').
card_number('elven lyre'/'ME2', '208').
card_flavor_text('elven lyre'/'ME2', 'Scholars are uncertain whether it was the actual sound or some other magical property of the elven lyre that transformed its player.').
card_multiverse_id('elven lyre'/'ME2', '184709').

card_in_set('elvish farmer', 'ME2').
card_original_type('elvish farmer'/'ME2', 'Creature — Elf').
card_original_text('elvish farmer'/'ME2', 'At the beginning of your upkeep, put a spore counter on Elvish Farmer.\nRemove three spore counters from Elvish Farmer: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: You gain 2 life.').
card_image_name('elvish farmer'/'ME2', 'elvish farmer').
card_uid('elvish farmer'/'ME2', 'ME2:Elvish Farmer:elvish farmer').
card_rarity('elvish farmer'/'ME2', 'Rare').
card_artist('elvish farmer'/'ME2', 'Richard Kane Ferguson').
card_number('elvish farmer'/'ME2', '156').
card_multiverse_id('elvish farmer'/'ME2', '184484').

card_in_set('elvish hunter', 'ME2').
card_original_type('elvish hunter'/'ME2', 'Creature — Elf Archer').
card_original_text('elvish hunter'/'ME2', '{1}{G}, {T}: Target creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('elvish hunter'/'ME2', 'elvish hunter').
card_uid('elvish hunter'/'ME2', 'ME2:Elvish Hunter:elvish hunter').
card_rarity('elvish hunter'/'ME2', 'Common').
card_artist('elvish hunter'/'ME2', 'Anson Maddocks').
card_number('elvish hunter'/'ME2', '157').
card_flavor_text('elvish hunter'/'ME2', '\"As the climate cooled, many elves turned to thallid farming for food, while the hunters honed their skills on what little game remained.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('elvish hunter'/'ME2', '184602').

card_in_set('elvish ranger', 'ME2').
card_original_type('elvish ranger'/'ME2', 'Creature — Elf').
card_original_text('elvish ranger'/'ME2', '').
card_image_name('elvish ranger'/'ME2', 'elvish ranger').
card_uid('elvish ranger'/'ME2', 'ME2:Elvish Ranger:elvish ranger').
card_rarity('elvish ranger'/'ME2', 'Common').
card_artist('elvish ranger'/'ME2', 'Terese Nielsen').
card_number('elvish ranger'/'ME2', '158').
card_flavor_text('elvish ranger'/'ME2', '\"How can any human hope to match our skills? We are children of Fyndhorn, and its sap runs in our veins.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('elvish ranger'/'ME2', '184691').

card_in_set('elvish spirit guide', 'ME2').
card_original_type('elvish spirit guide'/'ME2', 'Creature — Elf Spirit').
card_original_text('elvish spirit guide'/'ME2', 'Remove Elvish Spirit Guide in your hand from the game: Add {G} to your mana pool.').
card_image_name('elvish spirit guide'/'ME2', 'elvish spirit guide').
card_uid('elvish spirit guide'/'ME2', 'ME2:Elvish Spirit Guide:elvish spirit guide').
card_rarity('elvish spirit guide'/'ME2', 'Uncommon').
card_artist('elvish spirit guide'/'ME2', 'Julie Baroh').
card_number('elvish spirit guide'/'ME2', '159').
card_flavor_text('elvish spirit guide'/'ME2', '\"We are never without guidance, if we but seek it.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('elvish spirit guide'/'ME2', '184542').

card_in_set('energy storm', 'ME2').
card_original_type('energy storm'/'ME2', 'Enchantment').
card_original_text('energy storm'/'ME2', 'Cumulative upkeep {1}\nPrevent all damage that would be dealt by instant and sorcery spells.\nCreatures with flying don\'t untap during their controller\'s untap step.').
card_image_name('energy storm'/'ME2', 'energy storm').
card_uid('energy storm'/'ME2', 'ME2:Energy Storm:energy storm').
card_rarity('energy storm'/'ME2', 'Rare').
card_artist('energy storm'/'ME2', 'Sandra Everingham').
card_number('energy storm'/'ME2', '11').
card_multiverse_id('energy storm'/'ME2', '184621').

card_in_set('enervate', 'ME2').
card_original_type('enervate'/'ME2', 'Instant').
card_original_text('enervate'/'ME2', 'Tap target artifact, creature, or land.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('enervate'/'ME2', 'enervate').
card_uid('enervate'/'ME2', 'ME2:Enervate:enervate').
card_rarity('enervate'/'ME2', 'Common').
card_artist('enervate'/'ME2', 'L. A. Williams').
card_number('enervate'/'ME2', '47').
card_flavor_text('enervate'/'ME2', '\"Worlds turn in crucial moments of decision. Make your choice.\"\n—Gustha Ebbasdotter, Kjeldoran royal mage').
card_multiverse_id('enervate'/'ME2', '184622').

card_in_set('errand of duty', 'ME2').
card_original_type('errand of duty'/'ME2', 'Instant').
card_original_text('errand of duty'/'ME2', 'Put a 1/1 white Knight creature token with banding into play.').
card_image_name('errand of duty'/'ME2', 'errand of duty').
card_uid('errand of duty'/'ME2', 'ME2:Errand of Duty:errand of duty').
card_rarity('errand of duty'/'ME2', 'Uncommon').
card_artist('errand of duty'/'ME2', 'Julie Baroh').
card_number('errand of duty'/'ME2', '12').
card_flavor_text('errand of duty'/'ME2', '\"May they speed to their task, for the skyknights alone cannot hold Kjeldor safe.\"\n—Arna Kennerüd, skycaptain').
card_multiverse_id('errand of duty'/'ME2', '184730').

card_in_set('errantry', 'ME2').
card_original_type('errantry'/'ME2', 'Enchantment — Aura').
card_original_text('errantry'/'ME2', 'Enchant creature \nEnchanted creature gets +3/+0 and can only attack alone.').
card_image_name('errantry'/'ME2', 'errantry').
card_uid('errantry'/'ME2', 'ME2:Errantry:errantry').
card_rarity('errantry'/'ME2', 'Common').
card_artist('errantry'/'ME2', 'Scott Kirschner').
card_number('errantry'/'ME2', '124').
card_flavor_text('errantry'/'ME2', '\"There is no shame in solitude. The lone knight may succeed where a hundred founder.\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('errantry'/'ME2', '184741').

card_in_set('essence filter', 'ME2').
card_original_type('essence filter'/'ME2', 'Sorcery').
card_original_text('essence filter'/'ME2', 'Destroy all enchantments or all nonwhite enchantments.').
card_image_name('essence filter'/'ME2', 'essence filter').
card_uid('essence filter'/'ME2', 'ME2:Essence Filter:essence filter').
card_rarity('essence filter'/'ME2', 'Uncommon').
card_artist('essence filter'/'ME2', 'Rick Emond').
card_number('essence filter'/'ME2', '160').
card_flavor_text('essence filter'/'ME2', '\"Freyalise has cleansed our bodies and minds of the plagues of Kjeldor; all she asks in return is that we keep pure our newly given home in Fyndhorn.\"\n—Kolbjörn, elder druid of the Juniper Order').
card_multiverse_id('essence filter'/'ME2', '184718').

card_in_set('essence flare', 'ME2').
card_original_type('essence flare'/'ME2', 'Enchantment — Aura').
card_original_text('essence flare'/'ME2', 'Enchant creature\nEnchanted creature gets +2/+0.\nAt the beginning of the upkeep of enchanted creature\'s controller, put a -0/-1 counter on that creature.').
card_image_name('essence flare'/'ME2', 'essence flare').
card_uid('essence flare'/'ME2', 'ME2:Essence Flare:essence flare').
card_rarity('essence flare'/'ME2', 'Common').
card_artist('essence flare'/'ME2', 'Richard Kane Ferguson').
card_number('essence flare'/'ME2', '48').
card_flavor_text('essence flare'/'ME2', 'Never underestimate the power of the soul unleashed.').
card_multiverse_id('essence flare'/'ME2', '184731').

card_in_set('farrel\'s mantle', 'ME2').
card_original_type('farrel\'s mantle'/'ME2', 'Enchantment — Aura').
card_original_text('farrel\'s mantle'/'ME2', 'Enchant creature\nWhenever enchanted creature attacks and isn\'t blocked, its controller may have it deal damage equal to its power plus 2 to target creature. If that player does, the attacking creature deals no combat damage this turn.').
card_image_name('farrel\'s mantle'/'ME2', 'farrel\'s mantle').
card_uid('farrel\'s mantle'/'ME2', 'ME2:Farrel\'s Mantle:farrel\'s mantle').
card_rarity('farrel\'s mantle'/'ME2', 'Uncommon').
card_artist('farrel\'s mantle'/'ME2', 'Anthony S. Waters').
card_number('farrel\'s mantle'/'ME2', '13').
card_multiverse_id('farrel\'s mantle'/'ME2', '184643').

card_in_set('farrel\'s zealot', 'ME2').
card_original_type('farrel\'s zealot'/'ME2', 'Creature — Human').
card_original_text('farrel\'s zealot'/'ME2', 'Whenever Farrel\'s Zealot attacks and isn\'t blocked, you may have it deal 3 damage to target creature. If you do, Farrel\'s Zealot deals no combat damage this turn.').
card_image_name('farrel\'s zealot'/'ME2', 'farrel\'s zealot').
card_uid('farrel\'s zealot'/'ME2', 'ME2:Farrel\'s Zealot:farrel\'s zealot').
card_rarity('farrel\'s zealot'/'ME2', 'Uncommon').
card_artist('farrel\'s zealot'/'ME2', 'Richard Kane Ferguson').
card_number('farrel\'s zealot'/'ME2', '14').
card_flavor_text('farrel\'s zealot'/'ME2', 'Farrel and his followers became a formidable band of vigilantes, battling Icatians and followers of Tourach.').
card_multiverse_id('farrel\'s zealot'/'ME2', '184729').

card_in_set('feral thallid', 'ME2').
card_original_type('feral thallid'/'ME2', 'Creature — Fungus').
card_original_text('feral thallid'/'ME2', 'At the beginning of your upkeep, put a spore counter on Feral Thallid.\nRemove three spore counters from Feral Thallid: Regenerate Feral Thallid.').
card_image_name('feral thallid'/'ME2', 'feral thallid').
card_uid('feral thallid'/'ME2', 'ME2:Feral Thallid:feral thallid').
card_rarity('feral thallid'/'ME2', 'Common').
card_artist('feral thallid'/'ME2', 'Rob Alexander').
card_number('feral thallid'/'ME2', '161').
card_flavor_text('feral thallid'/'ME2', '\"Born and bred of fungus, thallids were nearly impossible to kill.\"\n—Sarpadian Empires, vol. I').
card_multiverse_id('feral thallid'/'ME2', '184485').

card_in_set('fire dragon', 'ME2').
card_original_type('fire dragon'/'ME2', 'Creature — Dragon').
card_original_text('fire dragon'/'ME2', 'Flying\nWhen Fire Dragon comes into play, it deals damage equal to the number of Mountains you control to target creature.').
card_image_name('fire dragon'/'ME2', 'fire dragon').
card_uid('fire dragon'/'ME2', 'ME2:Fire Dragon:fire dragon').
card_rarity('fire dragon'/'ME2', 'Rare').
card_artist('fire dragon'/'ME2', 'William Simpson').
card_number('fire dragon'/'ME2', '125').
card_multiverse_id('fire dragon'/'ME2', '184723').

card_in_set('flame spirit', 'ME2').
card_original_type('flame spirit'/'ME2', 'Creature — Elemental Spirit').
card_original_text('flame spirit'/'ME2', '{R}: Flame Spirit gets +1/+0 until end of turn.').
card_image_name('flame spirit'/'ME2', 'flame spirit').
card_uid('flame spirit'/'ME2', 'ME2:Flame Spirit:flame spirit').
card_rarity('flame spirit'/'ME2', 'Uncommon').
card_artist('flame spirit'/'ME2', 'Justin Hampton').
card_number('flame spirit'/'ME2', '126').
card_flavor_text('flame spirit'/'ME2', '\"The spirit of the flame is the spirit of change.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('flame spirit'/'ME2', '184743').

card_in_set('folk of the pines', 'ME2').
card_original_type('folk of the pines'/'ME2', 'Creature — Dryad').
card_original_text('folk of the pines'/'ME2', '{1}{G}: Folk of the Pines gets +1/+0 until end of turn.').
card_image_name('folk of the pines'/'ME2', 'folk of the pines').
card_uid('folk of the pines'/'ME2', 'ME2:Folk of the Pines:folk of the pines').
card_rarity('folk of the pines'/'ME2', 'Common').
card_artist('folk of the pines'/'ME2', 'NéNé Thomas & Catherine Buck').
card_number('folk of the pines'/'ME2', '162').
card_flavor_text('folk of the pines'/'ME2', '\"Our friends of the forest take many forms, yet all serve the will of Freyalise.\"\n—Laina of the Elvish Council').
card_multiverse_id('folk of the pines'/'ME2', '184623').

card_in_set('forbidden lore', 'ME2').
card_original_type('forbidden lore'/'ME2', 'Enchantment — Aura').
card_original_text('forbidden lore'/'ME2', 'Enchant land\nEnchanted land has \"{T}: Target creature gets +2/+1 until end of turn.\"').
card_image_name('forbidden lore'/'ME2', 'forbidden lore').
card_uid('forbidden lore'/'ME2', 'ME2:Forbidden Lore:forbidden lore').
card_rarity('forbidden lore'/'ME2', 'Uncommon').
card_artist('forbidden lore'/'ME2', 'Christopher Rush').
card_number('forbidden lore'/'ME2', '163').
card_multiverse_id('forbidden lore'/'ME2', '184573').

card_in_set('forgotten lore', 'ME2').
card_original_type('forgotten lore'/'ME2', 'Sorcery').
card_original_text('forgotten lore'/'ME2', 'Target opponent chooses a card in your graveyard. You may pay {G}. If you do, repeat this process except that opponent can\'t choose a card already chosen for Forgotten Lore. Then put the last chosen card into your hand.').
card_image_name('forgotten lore'/'ME2', 'forgotten lore').
card_uid('forgotten lore'/'ME2', 'ME2:Forgotten Lore:forgotten lore').
card_rarity('forgotten lore'/'ME2', 'Uncommon').
card_artist('forgotten lore'/'ME2', 'Harold McNeill').
card_number('forgotten lore'/'ME2', '164').
card_flavor_text('forgotten lore'/'ME2', 'In ashes are the gems of history.').
card_multiverse_id('forgotten lore'/'ME2', '184624').

card_in_set('foul familiar', 'ME2').
card_original_type('foul familiar'/'ME2', 'Creature — Spirit').
card_original_text('foul familiar'/'ME2', 'Foul Familiar can\'t block.\n{B}, Pay 1 life: Return Foul Familiar to its owner\'s hand.').
card_image_name('foul familiar'/'ME2', 'foul familiar').
card_uid('foul familiar'/'ME2', 'ME2:Foul Familiar:foul familiar').
card_rarity('foul familiar'/'ME2', 'Common').
card_artist('foul familiar'/'ME2', 'Anson Maddocks').
card_number('foul familiar'/'ME2', '90').
card_flavor_text('foul familiar'/'ME2', '\"Serve me, and live forever.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('foul familiar'/'ME2', '184625').

card_in_set('fumarole', 'ME2').
card_original_type('fumarole'/'ME2', 'Sorcery').
card_original_text('fumarole'/'ME2', 'As an additional cost to play Fumarole, pay 3 life.\nDestroy target creature and target land.').
card_image_name('fumarole'/'ME2', 'fumarole').
card_uid('fumarole'/'ME2', 'ME2:Fumarole:fumarole').
card_rarity('fumarole'/'ME2', 'Uncommon').
card_artist('fumarole'/'ME2', 'Drew Tucker').
card_number('fumarole'/'ME2', '194').
card_flavor_text('fumarole'/'ME2', '\"Too many of us have died in the explosions that wrack these hills.\"\n—Klazina Jansdotter, leader of the Order of the Sacred Torch').
card_multiverse_id('fumarole'/'ME2', '184626').

card_in_set('funeral march', 'ME2').
card_original_type('funeral march'/'ME2', 'Enchantment — Aura').
card_original_text('funeral march'/'ME2', 'Enchant creature\nWhen enchanted creature leaves play, its controller sacrifices a creature.').
card_image_name('funeral march'/'ME2', 'funeral march').
card_uid('funeral march'/'ME2', 'ME2:Funeral March:funeral march').
card_rarity('funeral march'/'ME2', 'Common').
card_artist('funeral march'/'ME2', 'John Coulthart').
card_number('funeral march'/'ME2', '91').
card_flavor_text('funeral march'/'ME2', '\"This party is such fun—but it\'s a shame to mourn just one.\"\n—Irini Sengir').
card_multiverse_id('funeral march'/'ME2', '184671').

card_in_set('fungal bloom', 'ME2').
card_original_type('fungal bloom'/'ME2', 'Enchantment').
card_original_text('fungal bloom'/'ME2', '{G}{G}: Put a spore counter on target Fungus.').
card_image_name('fungal bloom'/'ME2', 'fungal bloom').
card_uid('fungal bloom'/'ME2', 'ME2:Fungal Bloom:fungal bloom').
card_rarity('fungal bloom'/'ME2', 'Rare').
card_artist('fungal bloom'/'ME2', 'Daniel Gelon').
card_number('fungal bloom'/'ME2', '165').
card_flavor_text('fungal bloom'/'ME2', '\"Thallids could absorb energy from the forest itself. Even elves were at a disadvantage in fighting them.\"\n—Sarpadian Empires, vol. III').
card_multiverse_id('fungal bloom'/'ME2', '184771').

card_in_set('fyndhorn pollen', 'ME2').
card_original_type('fyndhorn pollen'/'ME2', 'Enchantment').
card_original_text('fyndhorn pollen'/'ME2', 'Cumulative upkeep {1}\nAll creatures get -1/-0.\n{1}{G}: All creatures get -1/-0 until end of turn.').
card_image_name('fyndhorn pollen'/'ME2', 'fyndhorn pollen').
card_uid('fyndhorn pollen'/'ME2', 'ME2:Fyndhorn Pollen:fyndhorn pollen').
card_rarity('fyndhorn pollen'/'ME2', 'Rare').
card_artist('fyndhorn pollen'/'ME2', 'Phil Foglio').
card_number('fyndhorn pollen'/'ME2', '166').
card_flavor_text('fyndhorn pollen'/'ME2', '\"I breathed deeply, and suddenly I knew not who or where I was.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('fyndhorn pollen'/'ME2', '184580').

card_in_set('gangrenous zombies', 'ME2').
card_original_type('gangrenous zombies'/'ME2', 'Creature — Zombie').
card_original_text('gangrenous zombies'/'ME2', '{T}, Sacrifice Gangrenous Zombies: Gangrenous Zombies deals 1 damage to each creature and each player. If you control a snow Swamp, Gangrenous Zombies deals 2 damage to each creature and each player instead.').
card_image_name('gangrenous zombies'/'ME2', 'gangrenous zombies').
card_uid('gangrenous zombies'/'ME2', 'ME2:Gangrenous Zombies:gangrenous zombies').
card_rarity('gangrenous zombies'/'ME2', 'Common').
card_artist('gangrenous zombies'/'ME2', 'Brian Snõddy').
card_number('gangrenous zombies'/'ME2', '92').
card_multiverse_id('gangrenous zombies'/'ME2', '184627').

card_in_set('giant growth', 'ME2').
card_original_type('giant growth'/'ME2', 'Instant').
card_original_text('giant growth'/'ME2', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'ME2', 'giant growth').
card_uid('giant growth'/'ME2', 'ME2:Giant Growth:giant growth').
card_rarity('giant growth'/'ME2', 'Common').
card_artist('giant growth'/'ME2', 'L. A. Williams').
card_number('giant growth'/'ME2', '167').
card_flavor_text('giant growth'/'ME2', '\"Here in Fyndhorn, the goddess Freyalise is generous to her children.\"\n—Kolbjörn, elder druid of the Juniper Order').
card_multiverse_id('giant growth'/'ME2', '184692').

card_in_set('giant trap door spider', 'ME2').
card_original_type('giant trap door spider'/'ME2', 'Creature — Spider').
card_original_text('giant trap door spider'/'ME2', '{1}{R}{G}, {T}: Remove from the game Giant Trap Door Spider and target creature without flying that\'s attacking you.').
card_image_name('giant trap door spider'/'ME2', 'giant trap door spider').
card_uid('giant trap door spider'/'ME2', 'ME2:Giant Trap Door Spider:giant trap door spider').
card_rarity('giant trap door spider'/'ME2', 'Uncommon').
card_artist('giant trap door spider'/'ME2', 'Heather Hudson').
card_number('giant trap door spider'/'ME2', '195').
card_flavor_text('giant trap door spider'/'ME2', '\"So large and so quiet—a combination best avoided.\"\n—Disa the Restless, journal entry').
card_multiverse_id('giant trap door spider'/'ME2', '184630').

card_in_set('glacial chasm', 'ME2').
card_original_type('glacial chasm'/'ME2', 'Land').
card_original_text('glacial chasm'/'ME2', 'Cumulative upkeep—Pay 2 life.\nWhen Glacial Chasm comes into play, sacrifice a land.\nCreatures you control can\'t attack.\nPrevent all damage that would be dealt to you.').
card_image_name('glacial chasm'/'ME2', 'glacial chasm').
card_uid('glacial chasm'/'ME2', 'ME2:Glacial Chasm:glacial chasm').
card_rarity('glacial chasm'/'ME2', 'Rare').
card_artist('glacial chasm'/'ME2', 'Liz Danforth').
card_number('glacial chasm'/'ME2', '229').
card_multiverse_id('glacial chasm'/'ME2', '184631').

card_in_set('glacial crevasses', 'ME2').
card_original_type('glacial crevasses'/'ME2', 'Enchantment').
card_original_text('glacial crevasses'/'ME2', 'Sacrifice a snow Mountain: Prevent all combat damage that would be dealt this turn.').
card_image_name('glacial crevasses'/'ME2', 'glacial crevasses').
card_uid('glacial crevasses'/'ME2', 'ME2:Glacial Crevasses:glacial crevasses').
card_rarity('glacial crevasses'/'ME2', 'Rare').
card_artist('glacial crevasses'/'ME2', 'Mike Raabe').
card_number('glacial crevasses'/'ME2', '127').
card_flavor_text('glacial crevasses'/'ME2', '\"We were chasing Lim-Dûl when the ridge in front of us suddenly crumbled. I can\'t believe it was mere coincidence.\"\n—Lucilde Fiksdotter, leader of the Order of the White Shield').
card_multiverse_id('glacial crevasses'/'ME2', '184632').

card_in_set('goblin ski patrol', 'ME2').
card_original_type('goblin ski patrol'/'ME2', 'Creature — Goblin').
card_original_text('goblin ski patrol'/'ME2', '{1}{R}: Goblin Ski Patrol gets +2/+0 and gains flying. Its controller sacrifices it at end of turn. Play this ability only once and only if you control a snow Mountain.').
card_image_name('goblin ski patrol'/'ME2', 'goblin ski patrol').
card_uid('goblin ski patrol'/'ME2', 'ME2:Goblin Ski Patrol:goblin ski patrol').
card_rarity('goblin ski patrol'/'ME2', 'Common').
card_artist('goblin ski patrol'/'ME2', 'Mark Poole').
card_number('goblin ski patrol'/'ME2', '128').
card_flavor_text('goblin ski patrol'/'ME2', '\"AIIIEEEE!\"\n—Ib Halfheart, goblin tactician').
card_multiverse_id('goblin ski patrol'/'ME2', '184690').

card_in_set('gorilla shaman', 'ME2').
card_original_type('gorilla shaman'/'ME2', 'Creature — Ape Shaman').
card_original_text('gorilla shaman'/'ME2', '{X}{X}{1}: Destroy target noncreature artifact with converted mana cost X.').
card_image_name('gorilla shaman'/'ME2', 'gorilla shaman').
card_uid('gorilla shaman'/'ME2', 'ME2:Gorilla Shaman:gorilla shaman').
card_rarity('gorilla shaman'/'ME2', 'Uncommon').
card_artist('gorilla shaman'/'ME2', 'Anthony S. Waters').
card_number('gorilla shaman'/'ME2', '129').
card_flavor_text('gorilla shaman'/'ME2', '\"Each generation teaches the next that artifice is the enemy of natural order.\"\n—Kaysa, elder druid of the Juniper Order').
card_multiverse_id('gorilla shaman'/'ME2', '184693').

card_in_set('grandmother sengir', 'ME2').
card_original_type('grandmother sengir'/'ME2', 'Legendary Creature — Human Wizard').
card_original_text('grandmother sengir'/'ME2', '{1}{B}, {T}: Target creature gets -1/-1 until end of turn.').
card_image_name('grandmother sengir'/'ME2', 'grandmother sengir').
card_uid('grandmother sengir'/'ME2', 'ME2:Grandmother Sengir:grandmother sengir').
card_rarity('grandmother sengir'/'ME2', 'Rare').
card_artist('grandmother sengir'/'ME2', 'Pete Venters').
card_number('grandmother sengir'/'ME2', '93').
card_flavor_text('grandmother sengir'/'ME2', '\"Rarely have power and madness been so delightfully wed as they have in our dear grandmother.\"\n—Baron Sengir').
card_multiverse_id('grandmother sengir'/'ME2', '184588').

card_in_set('gustha\'s scepter', 'ME2').
card_original_type('gustha\'s scepter'/'ME2', 'Artifact').
card_original_text('gustha\'s scepter'/'ME2', '{T}: Remove a card in your hand from the game face down. You may look at it as long as it remains removed from the game.\n{T}: Return a card you own removed from the game with Gustha\'s Scepter to your hand.\nWhen you lose control of Gustha\'s Scepter, put all cards removed from the game with Gustha\'s Scepter into their owners\' graveyards.').
card_image_name('gustha\'s scepter'/'ME2', 'gustha\'s scepter').
card_uid('gustha\'s scepter'/'ME2', 'ME2:Gustha\'s Scepter:gustha\'s scepter').
card_rarity('gustha\'s scepter'/'ME2', 'Rare').
card_artist('gustha\'s scepter'/'ME2', 'Sandra Everingham').
card_number('gustha\'s scepter'/'ME2', '209').
card_multiverse_id('gustha\'s scepter'/'ME2', '184548').

card_in_set('havenwood battleground', 'ME2').
card_original_type('havenwood battleground'/'ME2', 'Land').
card_original_text('havenwood battleground'/'ME2', 'Havenwood Battleground comes into play tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Havenwood Battleground: Add {G}{G} to your mana pool.').
card_image_name('havenwood battleground'/'ME2', 'havenwood battleground').
card_uid('havenwood battleground'/'ME2', 'ME2:Havenwood Battleground:havenwood battleground').
card_rarity('havenwood battleground'/'ME2', 'Uncommon').
card_artist('havenwood battleground'/'ME2', 'Mark Poole').
card_number('havenwood battleground'/'ME2', '230').
card_multiverse_id('havenwood battleground'/'ME2', '184486').

card_in_set('heart of yavimaya', 'ME2').
card_original_type('heart of yavimaya'/'ME2', 'Land').
card_original_text('heart of yavimaya'/'ME2', 'If Heart of Yavimaya would come into play, sacrifice a Forest instead. If you do, put Heart of Yavimaya into play. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {G} to your mana pool.\n{T}: Target creature gets +1/+1 until end of turn.').
card_image_name('heart of yavimaya'/'ME2', 'heart of yavimaya').
card_uid('heart of yavimaya'/'ME2', 'ME2:Heart of Yavimaya:heart of yavimaya').
card_rarity('heart of yavimaya'/'ME2', 'Rare').
card_artist('heart of yavimaya'/'ME2', 'Pete Venters').
card_number('heart of yavimaya'/'ME2', '231').
card_multiverse_id('heart of yavimaya'/'ME2', '184549').

card_in_set('helm of obedience', 'ME2').
card_original_type('helm of obedience'/'ME2', 'Artifact').
card_original_text('helm of obedience'/'ME2', '{1}{X}, {T}: Target opponent puts cards from the top of his or her library into his or her graveyard until a creature card or X cards are put into that graveyard this way, whichever comes first. If a creature card is put into that graveyard this way, sacrifice Helm of Obedience and put that card into play under your control. X can\'t be 0.').
card_image_name('helm of obedience'/'ME2', 'helm of obedience').
card_uid('helm of obedience'/'ME2', 'ME2:Helm of Obedience:helm of obedience').
card_rarity('helm of obedience'/'ME2', 'Rare').
card_artist('helm of obedience'/'ME2', 'Brian Snõddy').
card_number('helm of obedience'/'ME2', '210').
card_multiverse_id('helm of obedience'/'ME2', '184550').

card_in_set('icatian javelineers', 'ME2').
card_original_type('icatian javelineers'/'ME2', 'Creature — Human Soldier').
card_original_text('icatian javelineers'/'ME2', 'Icatian Javelineers comes into play with a javelin counter on it.\n{T}, Remove a javelin counter from Icatian Javelineers: Icatian Javelineers deals 1 damage to target creature or player.').
card_image_name('icatian javelineers'/'ME2', 'icatian javelineers').
card_uid('icatian javelineers'/'ME2', 'ME2:Icatian Javelineers:icatian javelineers').
card_rarity('icatian javelineers'/'ME2', 'Common').
card_artist('icatian javelineers'/'ME2', 'Melissa A. Benson').
card_number('icatian javelineers'/'ME2', '15').
card_multiverse_id('icatian javelineers'/'ME2', '184695').

card_in_set('icatian phalanx', 'ME2').
card_original_type('icatian phalanx'/'ME2', 'Creature — Human Soldier').
card_original_text('icatian phalanx'/'ME2', 'Banding').
card_image_name('icatian phalanx'/'ME2', 'icatian phalanx').
card_uid('icatian phalanx'/'ME2', 'ME2:Icatian Phalanx:icatian phalanx').
card_rarity('icatian phalanx'/'ME2', 'Common').
card_artist('icatian phalanx'/'ME2', 'Kaja Foglio').
card_number('icatian phalanx'/'ME2', '16').
card_flavor_text('icatian phalanx'/'ME2', 'Even after the wall was breached in half a dozen places, the phalanxes fought on, standing solidly against the onrushing raiders. Disciplined and dedicated, they held their ranks to the end, even in the face of tremendous losses.').
card_multiverse_id('icatian phalanx'/'ME2', '184491').

card_in_set('icatian scout', 'ME2').
card_original_type('icatian scout'/'ME2', 'Creature — Human Soldier').
card_original_text('icatian scout'/'ME2', '{1}, {T}: Target creature gains first strike until end of turn.').
card_image_name('icatian scout'/'ME2', 'icatian scout').
card_uid('icatian scout'/'ME2', 'ME2:Icatian Scout:icatian scout').
card_rarity('icatian scout'/'ME2', 'Common').
card_artist('icatian scout'/'ME2', 'Richard Kane Ferguson').
card_number('icatian scout'/'ME2', '17').
card_flavor_text('icatian scout'/'ME2', '\"Because the orc hordes attacked along the entire border, Scouts were essential to Icatia\'s defense.\"\n—Sarpadian Empires, vol. VI').
card_multiverse_id('icatian scout'/'ME2', '184696').

card_in_set('ice floe', 'ME2').
card_original_type('ice floe'/'ME2', 'Land').
card_original_text('ice floe'/'ME2', 'You may choose not to untap Ice Floe during your untap step.\n{T}: Tap target creature without flying that\'s attacking you. It doesn\'t untap during its controller\'s untap step as long as Ice Floe remains tapped.').
card_image_name('ice floe'/'ME2', 'ice floe').
card_uid('ice floe'/'ME2', 'ME2:Ice Floe:ice floe').
card_rarity('ice floe'/'ME2', 'Uncommon').
card_artist('ice floe'/'ME2', 'Jeff A. Menges').
card_number('ice floe'/'ME2', '232').
card_multiverse_id('ice floe'/'ME2', '184633').

card_in_set('iceberg', 'ME2').
card_original_type('iceberg'/'ME2', 'Enchantment').
card_original_text('iceberg'/'ME2', 'Iceberg comes into play with X ice counters on it.\n{3}: Put an ice counter on Iceberg.\nRemove an ice counter from Iceberg: Add {1} to your mana pool.').
card_image_name('iceberg'/'ME2', 'iceberg').
card_uid('iceberg'/'ME2', 'ME2:Iceberg:iceberg').
card_rarity('iceberg'/'ME2', 'Uncommon').
card_artist('iceberg'/'ME2', 'Jeff A. Menges').
card_number('iceberg'/'ME2', '49').
card_multiverse_id('iceberg'/'ME2', '184733').

card_in_set('icequake', 'ME2').
card_original_type('icequake'/'ME2', 'Sorcery').
card_original_text('icequake'/'ME2', 'Destroy target land. If that land was a snow land, Icequake deals 1 damage to that land\'s controller.').
card_image_name('icequake'/'ME2', 'icequake').
card_uid('icequake'/'ME2', 'ME2:Icequake:icequake').
card_rarity('icequake'/'ME2', 'Common').
card_artist('icequake'/'ME2', 'Richard Kane Ferguson').
card_number('icequake'/'ME2', '94').
card_flavor_text('icequake'/'ME2', '\"When the earth shakes, and their animals are swallowed up by the ground, perhaps they will think twice before attacking again.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('icequake'/'ME2', '184634').

card_in_set('icy prison', 'ME2').
card_original_type('icy prison'/'ME2', 'Enchantment').
card_original_text('icy prison'/'ME2', 'When Icy Prison comes into play, remove target creature from the game.\nAt the beginning of your upkeep, sacrifice Icy Prison unless any player pays {3}.\nWhen Icy Prison leaves play, return the removed creature to play under its owner\'s control.').
card_image_name('icy prison'/'ME2', 'icy prison').
card_uid('icy prison'/'ME2', 'ME2:Icy Prison:icy prison').
card_rarity('icy prison'/'ME2', 'Common').
card_artist('icy prison'/'ME2', 'Anson Maddocks').
card_number('icy prison'/'ME2', '50').
card_multiverse_id('icy prison'/'ME2', '184754').

card_in_set('ihsan\'s shade', 'ME2').
card_original_type('ihsan\'s shade'/'ME2', 'Legendary Creature — Shade Knight').
card_original_text('ihsan\'s shade'/'ME2', 'Protection from white').
card_image_name('ihsan\'s shade'/'ME2', 'ihsan\'s shade').
card_uid('ihsan\'s shade'/'ME2', 'ME2:Ihsan\'s Shade:ihsan\'s shade').
card_rarity('ihsan\'s shade'/'ME2', 'Rare').
card_artist('ihsan\'s shade'/'ME2', 'Christopher Rush').
card_number('ihsan\'s shade'/'ME2', '95').
card_flavor_text('ihsan\'s shade'/'ME2', '\"Ihsan, the weak. Ihsan, the fallen. Ihsan, the betrayer. He has brought shame to the Serra paladins where none existed before. May his suffering equal his betrayal.\"\n—Baris, Serra inquisitor').
card_multiverse_id('ihsan\'s shade'/'ME2', '184591').

card_in_set('imperial recruiter', 'ME2').
card_original_type('imperial recruiter'/'ME2', 'Creature — Human Advisor').
card_original_text('imperial recruiter'/'ME2', 'When Imperial Recruiter comes into play, search your library for a creature card with power 2 or less, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('imperial recruiter'/'ME2', 'imperial recruiter').
card_uid('imperial recruiter'/'ME2', 'ME2:Imperial Recruiter:imperial recruiter').
card_rarity('imperial recruiter'/'ME2', 'Rare').
card_artist('imperial recruiter'/'ME2', 'Mitsuaki Sagiri').
card_number('imperial recruiter'/'ME2', '130').
card_multiverse_id('imperial recruiter'/'ME2', '184716').

card_in_set('imperial seal', 'ME2').
card_original_type('imperial seal'/'ME2', 'Sorcery').
card_original_text('imperial seal'/'ME2', 'Search your library for a card, then shuffle your library and put that card on top of it. You lose 2 life.').
card_image_name('imperial seal'/'ME2', 'imperial seal').
card_uid('imperial seal'/'ME2', 'ME2:Imperial Seal:imperial seal').
card_rarity('imperial seal'/'ME2', 'Rare').
card_artist('imperial seal'/'ME2', 'Li Tie').
card_number('imperial seal'/'ME2', '96').
card_flavor_text('imperial seal'/'ME2', '\"If Heaven has placed it in your hands, it means that the throne is destined to be yours.\"').
card_multiverse_id('imperial seal'/'ME2', '184717').

card_in_set('incinerate', 'ME2').
card_original_type('incinerate'/'ME2', 'Instant').
card_original_text('incinerate'/'ME2', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_image_name('incinerate'/'ME2', 'incinerate').
card_uid('incinerate'/'ME2', 'ME2:Incinerate:incinerate').
card_rarity('incinerate'/'ME2', 'Common').
card_artist('incinerate'/'ME2', 'Mark Poole').
card_number('incinerate'/'ME2', '131').
card_flavor_text('incinerate'/'ME2', '\"Yes, I think ‘toast\' is an appropriate description.\"\n—Jaya Ballard, task mage').
card_multiverse_id('incinerate'/'ME2', '184636').

card_in_set('infernal darkness', 'ME2').
card_original_type('infernal darkness'/'ME2', 'Enchantment').
card_original_text('infernal darkness'/'ME2', 'Cumulative upkeep—Pay {B} and 1 life.\nIf a land is tapped for mana, it produces {B} instead of any other type.').
card_image_name('infernal darkness'/'ME2', 'infernal darkness').
card_uid('infernal darkness'/'ME2', 'ME2:Infernal Darkness:infernal darkness').
card_rarity('infernal darkness'/'ME2', 'Rare').
card_artist('infernal darkness'/'ME2', 'Phil Foglio').
card_number('infernal darkness'/'ME2', '97').
card_flavor_text('infernal darkness'/'ME2', '\"I thought the day had brought enough horrors for our ragged band, but the night was far worse.\"\n—Lucilde Fiksdotter, leader of the Order of the White Shield').
card_multiverse_id('infernal darkness'/'ME2', '184637').

card_in_set('inheritance', 'ME2').
card_original_type('inheritance'/'ME2', 'Enchantment').
card_original_text('inheritance'/'ME2', 'Whenever a creature is put into a graveyard from play, you may pay {3}. If you do, draw a card.').
card_image_name('inheritance'/'ME2', 'inheritance').
card_uid('inheritance'/'ME2', 'ME2:Inheritance:inheritance').
card_rarity('inheritance'/'ME2', 'Uncommon').
card_artist('inheritance'/'ME2', 'Kaja Foglio').
card_number('inheritance'/'ME2', '18').
card_flavor_text('inheritance'/'ME2', '\"More than lessons may be gained from the past.\"\n—Halvor Arensson, Kjeldoran priest').
card_multiverse_id('inheritance'/'ME2', '184635').

card_in_set('ironclaw orcs', 'ME2').
card_original_type('ironclaw orcs'/'ME2', 'Creature — Orc').
card_original_text('ironclaw orcs'/'ME2', 'Ironclaw Orcs can\'t block creatures with power 2 or greater.').
card_image_name('ironclaw orcs'/'ME2', 'ironclaw orcs').
card_uid('ironclaw orcs'/'ME2', 'ME2:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'ME2', 'Common').
card_artist('ironclaw orcs'/'ME2', 'Anson Maddocks').
card_number('ironclaw orcs'/'ME2', '132').
card_flavor_text('ironclaw orcs'/'ME2', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan.').
card_multiverse_id('ironclaw orcs'/'ME2', '184759').

card_in_set('ivory gargoyle', 'ME2').
card_original_type('ivory gargoyle'/'ME2', 'Creature — Gargoyle').
card_original_text('ivory gargoyle'/'ME2', 'Flying\nWhen Ivory Gargoyle is put into a graveyard from play, return it to play under its owner\'s control at end of turn and you skip your next draw step.\n{4}{W}: Remove Ivory Gargoyle from the game.').
card_image_name('ivory gargoyle'/'ME2', 'ivory gargoyle').
card_uid('ivory gargoyle'/'ME2', 'ME2:Ivory Gargoyle:ivory gargoyle').
card_rarity('ivory gargoyle'/'ME2', 'Rare').
card_artist('ivory gargoyle'/'ME2', 'Quinton Hoover').
card_number('ivory gargoyle'/'ME2', '19').
card_multiverse_id('ivory gargoyle'/'ME2', '184551').

card_in_set('jester\'s mask', 'ME2').
card_original_type('jester\'s mask'/'ME2', 'Artifact').
card_original_text('jester\'s mask'/'ME2', 'Jester\'s Mask comes into play tapped.\n{1}, {T}, Sacrifice Jester\'s Mask: Target opponent puts the cards from his or her hand on top of his or her library. Search that player\'s library for that many cards. That player puts those cards into his or her hand, then shuffles his or her library.').
card_image_name('jester\'s mask'/'ME2', 'jester\'s mask').
card_uid('jester\'s mask'/'ME2', 'ME2:Jester\'s Mask:jester\'s mask').
card_rarity('jester\'s mask'/'ME2', 'Rare').
card_artist('jester\'s mask'/'ME2', 'Dan Frazier').
card_number('jester\'s mask'/'ME2', '211').
card_multiverse_id('jester\'s mask'/'ME2', '184638').

card_in_set('jeweled amulet', 'ME2').
card_original_type('jeweled amulet'/'ME2', 'Artifact').
card_original_text('jeweled amulet'/'ME2', '{1}, {T}: Put a charge counter on Jeweled Amulet. Note the type of mana spent to pay this activation cost. Play this ability only if there are no charge counters on Jeweled Amulet.\n{T}, Remove a charge counter from Jeweled Amulet: Add one mana of Jeweled Amulet\'s last noted type to your mana pool.').
card_image_name('jeweled amulet'/'ME2', 'jeweled amulet').
card_uid('jeweled amulet'/'ME2', 'ME2:Jeweled Amulet:jeweled amulet').
card_rarity('jeweled amulet'/'ME2', 'Uncommon').
card_artist('jeweled amulet'/'ME2', 'Dan Frazier').
card_number('jeweled amulet'/'ME2', '212').
card_multiverse_id('jeweled amulet'/'ME2', '184639').

card_in_set('johtull wurm', 'ME2').
card_original_type('johtull wurm'/'ME2', 'Creature — Wurm').
card_original_text('johtull wurm'/'ME2', 'Whenever Johtull Wurm becomes blocked, it gets -2/-1 until end of turn for each creature blocking it beyond the first.').
card_image_name('johtull wurm'/'ME2', 'johtull wurm').
card_uid('johtull wurm'/'ME2', 'ME2:Johtull Wurm:johtull wurm').
card_rarity('johtull wurm'/'ME2', 'Uncommon').
card_artist('johtull wurm'/'ME2', 'Daniel Gelon').
card_number('johtull wurm'/'ME2', '168').
card_flavor_text('johtull wurm'/'ME2', '\"To bring her down we must be on all sides at once—leave one avenue open and we\'ll all be dead.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('johtull wurm'/'ME2', '184640').

card_in_set('joven\'s ferrets', 'ME2').
card_original_type('joven\'s ferrets'/'ME2', 'Creature — Ferret').
card_original_text('joven\'s ferrets'/'ME2', 'Whenever Joven\'s Ferrets attacks, it gets +0/+2 until end of turn.\nAt end of combat, tap all creatures that blocked Joven\'s Ferrets this turn. They don\'t untap during their controller\'s next untap step.').
card_image_name('joven\'s ferrets'/'ME2', 'joven\'s ferrets').
card_uid('joven\'s ferrets'/'ME2', 'ME2:Joven\'s Ferrets:joven\'s ferrets').
card_rarity('joven\'s ferrets'/'ME2', 'Uncommon').
card_artist('joven\'s ferrets'/'ME2', 'Amy Weber').
card_number('joven\'s ferrets'/'ME2', '169').
card_multiverse_id('joven\'s ferrets'/'ME2', '184592').

card_in_set('juniper order advocate', 'ME2').
card_original_type('juniper order advocate'/'ME2', 'Creature — Human Knight').
card_original_text('juniper order advocate'/'ME2', 'Green creatures you control get +1/+1 as long as Juniper Order Advocate is untapped.').
card_image_name('juniper order advocate'/'ME2', 'juniper order advocate').
card_uid('juniper order advocate'/'ME2', 'ME2:Juniper Order Advocate:juniper order advocate').
card_rarity('juniper order advocate'/'ME2', 'Uncommon').
card_artist('juniper order advocate'/'ME2', 'Douglas Shuler').
card_number('juniper order advocate'/'ME2', '20').
card_flavor_text('juniper order advocate'/'ME2', '\"In these troubled times, we alone carry the hope of reconciliation between Elf and Human.\"\n—Jaeuhl Carthalion, Juniper Order advocate').
card_multiverse_id('juniper order advocate'/'ME2', '184552').

card_in_set('karplusan giant', 'ME2').
card_original_type('karplusan giant'/'ME2', 'Creature — Giant').
card_original_text('karplusan giant'/'ME2', 'Tap an untapped snow land you control: Karplusan Giant gets +1/+1 until end of turn.').
card_image_name('karplusan giant'/'ME2', 'karplusan giant').
card_uid('karplusan giant'/'ME2', 'ME2:Karplusan Giant:karplusan giant').
card_rarity('karplusan giant'/'ME2', 'Uncommon').
card_artist('karplusan giant'/'ME2', 'Daniel Gelon').
card_number('karplusan giant'/'ME2', '133').
card_flavor_text('karplusan giant'/'ME2', '\"They aren\'t the brightest or the quickest of giants. For that matter, the same holds true if you compare them to rocks.\"\n—Disa the Restless, journal entry').
card_multiverse_id('karplusan giant'/'ME2', '184713').

card_in_set('kaysa', 'ME2').
card_original_type('kaysa'/'ME2', 'Legendary Creature — Elf Druid').
card_original_text('kaysa'/'ME2', 'Green creatures you control get +1/+1.').
card_image_name('kaysa'/'ME2', 'kaysa').
card_uid('kaysa'/'ME2', 'ME2:Kaysa:kaysa').
card_rarity('kaysa'/'ME2', 'Rare').
card_artist('kaysa'/'ME2', 'Rebecca Guay').
card_number('kaysa'/'ME2', '170').
card_flavor_text('kaysa'/'ME2', 'Kaysa speaks as the elder druid, but Yavimaya recognizes only one voice: its own.').
card_multiverse_id('kaysa'/'ME2', '184553').

card_in_set('kjeldoran dead', 'ME2').
card_original_type('kjeldoran dead'/'ME2', 'Creature — Skeleton').
card_original_text('kjeldoran dead'/'ME2', 'When Kjeldoran Dead comes into play, sacrifice a creature.\n{B}: Regenerate Kjeldoran Dead.').
card_image_name('kjeldoran dead'/'ME2', 'kjeldoran dead').
card_uid('kjeldoran dead'/'ME2', 'ME2:Kjeldoran Dead:kjeldoran dead').
card_rarity('kjeldoran dead'/'ME2', 'Common').
card_artist('kjeldoran dead'/'ME2', 'Melissa A. Benson').
card_number('kjeldoran dead'/'ME2', '98').
card_flavor_text('kjeldoran dead'/'ME2', '\"They shall kill those whom once they loved.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('kjeldoran dead'/'ME2', '184641').

card_in_set('kjeldoran elite guard', 'ME2').
card_original_type('kjeldoran elite guard'/'ME2', 'Creature — Human Soldier').
card_original_text('kjeldoran elite guard'/'ME2', '{T}: Target creature gets +2/+2 until end of turn. When that creature leaves play this turn, sacrifice Kjeldoran Elite Guard. Play this ability only during combat.').
card_image_name('kjeldoran elite guard'/'ME2', 'kjeldoran elite guard').
card_uid('kjeldoran elite guard'/'ME2', 'ME2:Kjeldoran Elite Guard:kjeldoran elite guard').
card_rarity('kjeldoran elite guard'/'ME2', 'Common').
card_artist('kjeldoran elite guard'/'ME2', 'Melissa A. Benson').
card_number('kjeldoran elite guard'/'ME2', '21').
card_flavor_text('kjeldoran elite guard'/'ME2', 'The winged helms of the guard are put on for pageants—but taken off for war.').
card_multiverse_id('kjeldoran elite guard'/'ME2', '184642').

card_in_set('kjeldoran home guard', 'ME2').
card_original_type('kjeldoran home guard'/'ME2', 'Creature — Human Soldier').
card_original_text('kjeldoran home guard'/'ME2', 'At end of combat, if Kjeldoran Home Guard attacked or blocked this combat, put a -0/-1 counter on Kjeldoran Home Guard and put a 0/1 white Deserter creature token into play.').
card_image_name('kjeldoran home guard'/'ME2', 'kjeldoran home guard').
card_uid('kjeldoran home guard'/'ME2', 'ME2:Kjeldoran Home Guard:kjeldoran home guard').
card_rarity('kjeldoran home guard'/'ME2', 'Uncommon').
card_artist('kjeldoran home guard'/'ME2', 'Andi Rusu').
card_number('kjeldoran home guard'/'ME2', '22').
card_multiverse_id('kjeldoran home guard'/'ME2', '184554').

card_in_set('kjeldoran outpost', 'ME2').
card_original_type('kjeldoran outpost'/'ME2', 'Land').
card_original_text('kjeldoran outpost'/'ME2', 'If Kjeldoran Outpost would come into play, sacrifice a Plains instead. If you do, put Kjeldoran Outpost into play. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {W} to your mana pool.\n{1}{W}, {T}: Put a 1/1 white Soldier creature token into play.').
card_image_name('kjeldoran outpost'/'ME2', 'kjeldoran outpost').
card_uid('kjeldoran outpost'/'ME2', 'ME2:Kjeldoran Outpost:kjeldoran outpost').
card_rarity('kjeldoran outpost'/'ME2', 'Rare').
card_artist('kjeldoran outpost'/'ME2', 'Jeff A. Menges').
card_number('kjeldoran outpost'/'ME2', '233').
card_multiverse_id('kjeldoran outpost'/'ME2', '184555').

card_in_set('kjeldoran skycaptain', 'ME2').
card_original_type('kjeldoran skycaptain'/'ME2', 'Creature — Human Soldier').
card_original_text('kjeldoran skycaptain'/'ME2', 'Banding, flying, first strike').
card_image_name('kjeldoran skycaptain'/'ME2', 'kjeldoran skycaptain').
card_uid('kjeldoran skycaptain'/'ME2', 'ME2:Kjeldoran Skycaptain:kjeldoran skycaptain').
card_rarity('kjeldoran skycaptain'/'ME2', 'Common').
card_artist('kjeldoran skycaptain'/'ME2', 'Mark Poole').
card_number('kjeldoran skycaptain'/'ME2', '23').
card_flavor_text('kjeldoran skycaptain'/'ME2', '\"If we do our duty and uphold our honor, Kjeldor shall stand for a thousand years.\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('kjeldoran skycaptain'/'ME2', '184644').

card_in_set('knight of stromgald', 'ME2').
card_original_type('knight of stromgald'/'ME2', 'Creature — Human Knight').
card_original_text('knight of stromgald'/'ME2', 'Protection from white\n{B}: Knight of Stromgald gains first strike until end of turn.\n{B}{B}: Knight of Stromgald gets +1/+0 until end of turn.').
card_image_name('knight of stromgald'/'ME2', 'knight of stromgald').
card_uid('knight of stromgald'/'ME2', 'ME2:Knight of Stromgald:knight of stromgald').
card_rarity('knight of stromgald'/'ME2', 'Uncommon').
card_artist('knight of stromgald'/'ME2', 'Mark Poole').
card_number('knight of stromgald'/'ME2', '99').
card_flavor_text('knight of stromgald'/'ME2', '\"Kjeldorans should rule supreme, and to the rest, death!\"\n—Avram Garrisson, leader of the Knights of Stromgald').
card_multiverse_id('knight of stromgald'/'ME2', '184645').

card_in_set('krovikan fetish', 'ME2').
card_original_type('krovikan fetish'/'ME2', 'Enchantment — Aura').
card_original_text('krovikan fetish'/'ME2', 'Enchant creature\nWhen Krovikan Fetish comes into play, draw a card at the beginning of the next turn\'s upkeep.\nEnchanted creature gets +1/+1.').
card_image_name('krovikan fetish'/'ME2', 'krovikan fetish').
card_uid('krovikan fetish'/'ME2', 'ME2:Krovikan Fetish:krovikan fetish').
card_rarity('krovikan fetish'/'ME2', 'Common').
card_artist('krovikan fetish'/'ME2', 'Heather Hudson').
card_number('krovikan fetish'/'ME2', '100').
card_flavor_text('krovikan fetish'/'ME2', 'Some Krovikans find strength in the ears and eyes of their victims and wear such fetishes into battle.').
card_multiverse_id('krovikan fetish'/'ME2', '184737').

card_in_set('krovikan horror', 'ME2').
card_original_type('krovikan horror'/'ME2', 'Creature — Horror Spirit').
card_original_text('krovikan horror'/'ME2', 'At end of turn, if Krovikan Horror is in your graveyard with a creature card directly above it, you may return Krovikan Horror to your hand.\n{1}, Sacrifice a creature: Krovikan Horror deals 1 damage to target creature or player.').
card_image_name('krovikan horror'/'ME2', 'krovikan horror').
card_uid('krovikan horror'/'ME2', 'ME2:Krovikan Horror:krovikan horror').
card_rarity('krovikan horror'/'ME2', 'Rare').
card_artist('krovikan horror'/'ME2', 'Christopher Rush').
card_number('krovikan horror'/'ME2', '101').
card_multiverse_id('krovikan horror'/'ME2', '184556').

card_in_set('krovikan sorcerer', 'ME2').
card_original_type('krovikan sorcerer'/'ME2', 'Creature — Human Wizard').
card_original_text('krovikan sorcerer'/'ME2', '{T}, Discard a nonblack card: Draw a card.\n{T}, Discard a black card: Draw two cards, then discard one of them.').
card_image_name('krovikan sorcerer'/'ME2', 'krovikan sorcerer').
card_uid('krovikan sorcerer'/'ME2', 'ME2:Krovikan Sorcerer:krovikan sorcerer').
card_rarity('krovikan sorcerer'/'ME2', 'Common').
card_artist('krovikan sorcerer'/'ME2', 'Pat Morrissey').
card_number('krovikan sorcerer'/'ME2', '51').
card_flavor_text('krovikan sorcerer'/'ME2', '\"These sorcerers always seem to have another surprise up their sleeves.\"\n—Zur the Enchanter').
card_multiverse_id('krovikan sorcerer'/'ME2', '184646').

card_in_set('krovikan vampire', 'ME2').
card_original_type('krovikan vampire'/'ME2', 'Creature — Vampire').
card_original_text('krovikan vampire'/'ME2', 'At end of turn, if a creature dealt damage by Krovikan Vampire this turn was put into a graveyard, put that card into play under your control. Sacrifice it when you lose control of Krovikan Vampire.').
card_image_name('krovikan vampire'/'ME2', 'krovikan vampire').
card_uid('krovikan vampire'/'ME2', 'ME2:Krovikan Vampire:krovikan vampire').
card_rarity('krovikan vampire'/'ME2', 'Uncommon').
card_artist('krovikan vampire'/'ME2', 'Quinton Hoover').
card_number('krovikan vampire'/'ME2', '102').
card_multiverse_id('krovikan vampire'/'ME2', '184647').

card_in_set('lat-nam\'s legacy', 'ME2').
card_original_type('lat-nam\'s legacy'/'ME2', 'Instant').
card_original_text('lat-nam\'s legacy'/'ME2', 'Shuffle a card from your hand into your library. If you do, draw two cards at the beginning of the next turn\'s upkeep.').
card_image_name('lat-nam\'s legacy'/'ME2', 'lat-nam\'s legacy').
card_uid('lat-nam\'s legacy'/'ME2', 'ME2:Lat-Nam\'s Legacy:lat-nam\'s legacy').
card_rarity('lat-nam\'s legacy'/'ME2', 'Common').
card_artist('lat-nam\'s legacy'/'ME2', 'Tom Wänerstrand').
card_number('lat-nam\'s legacy'/'ME2', '52').
card_flavor_text('lat-nam\'s legacy'/'ME2', '\"All the knowledge of Lat-Nam could not protect its sages from the Brothers\' War.\"\n—Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('lat-nam\'s legacy'/'ME2', '184697').

card_in_set('lava burst', 'ME2').
card_original_type('lava burst'/'ME2', 'Sorcery').
card_original_text('lava burst'/'ME2', 'Lava Burst deals X damage to target creature or player. If Lava Burst would deal damage to a creature, that damage can\'t be prevented or dealt instead to another creature or player.').
card_image_name('lava burst'/'ME2', 'lava burst').
card_uid('lava burst'/'ME2', 'ME2:Lava Burst:lava burst').
card_rarity('lava burst'/'ME2', 'Uncommon').
card_artist('lava burst'/'ME2', 'Tom Wänerstrand').
card_number('lava burst'/'ME2', '134').
card_flavor_text('lava burst'/'ME2', '\"Overkill? This isn\'t a game of Kick-the-Ouphe!\"\n—Jaya Ballard, task mage').
card_multiverse_id('lava burst'/'ME2', '184648').

card_in_set('leaping lizard', 'ME2').
card_original_type('leaping lizard'/'ME2', 'Creature — Lizard').
card_original_text('leaping lizard'/'ME2', '{1}{G}: Leaping Lizard gains flying and gets -0/-1 until end of turn.').
card_image_name('leaping lizard'/'ME2', 'leaping lizard').
card_uid('leaping lizard'/'ME2', 'ME2:Leaping Lizard:leaping lizard').
card_rarity('leaping lizard'/'ME2', 'Common').
card_artist('leaping lizard'/'ME2', 'Amy Weber').
card_number('leaping lizard'/'ME2', '171').
card_flavor_text('leaping lizard'/'ME2', '\"I never question the Autumn Willow about her motives, not even when she turns people into lizards. It\'s her way.\"\n—Devin, faerie noble').
card_multiverse_id('leaping lizard'/'ME2', '184772').

card_in_set('lim-dûl\'s high guard', 'ME2').
card_original_type('lim-dûl\'s high guard'/'ME2', 'Creature — Skeleton').
card_original_text('lim-dûl\'s high guard'/'ME2', 'First strike\n{1}{B}: Regenerate Lim-Dûl\'s High Guard.').
card_image_name('lim-dûl\'s high guard'/'ME2', 'lim-dul\'s high guard').
card_uid('lim-dûl\'s high guard'/'ME2', 'ME2:Lim-Dûl\'s High Guard:lim-dul\'s high guard').
card_rarity('lim-dûl\'s high guard'/'ME2', 'Uncommon').
card_artist('lim-dûl\'s high guard'/'ME2', 'Anson Maddocks').
card_number('lim-dûl\'s high guard'/'ME2', '103').
card_flavor_text('lim-dûl\'s high guard'/'ME2', '\"The guard will forever stand ready. For them, death is merely an inconvenience, not an ending.\"\n—Chaeska, Keeper of Tresserhorn').
card_multiverse_id('lim-dûl\'s high guard'/'ME2', '184714').

card_in_set('lodestone bauble', 'ME2').
card_original_type('lodestone bauble'/'ME2', 'Artifact').
card_original_text('lodestone bauble'/'ME2', '{1}, {T}, Sacrifice Lodestone Bauble: Put up to four target basic land cards from a player\'s graveyard on top of his or her library in any order. That player draws a card at the beginning of the next turn\'s upkeep.').
card_image_name('lodestone bauble'/'ME2', 'lodestone bauble').
card_uid('lodestone bauble'/'ME2', 'ME2:Lodestone Bauble:lodestone bauble').
card_rarity('lodestone bauble'/'ME2', 'Rare').
card_artist('lodestone bauble'/'ME2', 'Douglas Shuler').
card_number('lodestone bauble'/'ME2', '213').
card_multiverse_id('lodestone bauble'/'ME2', '184559').

card_in_set('lost order of jarkeld', 'ME2').
card_original_type('lost order of jarkeld'/'ME2', 'Creature — Human Knight').
card_original_text('lost order of jarkeld'/'ME2', 'As Lost Order of Jarkeld comes into play, choose an opponent.\nLost Order of Jarkeld\'s power and toughness are each equal to 1 plus the number of creatures that opponent controls.').
card_image_name('lost order of jarkeld'/'ME2', 'lost order of jarkeld').
card_uid('lost order of jarkeld'/'ME2', 'ME2:Lost Order of Jarkeld:lost order of jarkeld').
card_rarity('lost order of jarkeld'/'ME2', 'Rare').
card_artist('lost order of jarkeld'/'ME2', 'Andi Rusu').
card_number('lost order of jarkeld'/'ME2', '24').
card_flavor_text('lost order of jarkeld'/'ME2', '\"Let us remember brave Jarkeld and his troops, who perished in the Adarkar Wastes so long ago.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('lost order of jarkeld'/'ME2', '184649').

card_in_set('magus of the unseen', 'ME2').
card_original_type('magus of the unseen'/'ME2', 'Creature — Human Wizard').
card_original_text('magus of the unseen'/'ME2', '{1}{U}, {T}: Untap target artifact an opponent controls and gain control of it until end of turn. It gains haste until end of turn. When you lose control of the artifact, tap it.').
card_image_name('magus of the unseen'/'ME2', 'magus of the unseen').
card_uid('magus of the unseen'/'ME2', 'ME2:Magus of the Unseen:magus of the unseen').
card_rarity('magus of the unseen'/'ME2', 'Rare').
card_artist('magus of the unseen'/'ME2', 'Kaja Foglio').
card_number('magus of the unseen'/'ME2', '53').
card_multiverse_id('magus of the unseen'/'ME2', '184732').

card_in_set('mana crypt', 'ME2').
card_original_type('mana crypt'/'ME2', 'Artifact').
card_original_text('mana crypt'/'ME2', 'At the beginning of your upkeep, flip a coin. If you lose the flip, Mana Crypt deals 3 damage to you.\n{T}: Add {2} to your mana pool.').
card_image_name('mana crypt'/'ME2', 'mana crypt').
card_uid('mana crypt'/'ME2', 'ME2:Mana Crypt:mana crypt').
card_rarity('mana crypt'/'ME2', 'Rare').
card_artist('mana crypt'/'ME2', 'Mark Tedin').
card_number('mana crypt'/'ME2', '214').
card_multiverse_id('mana crypt'/'ME2', '184773').

card_in_set('marjhan', 'ME2').
card_original_type('marjhan'/'ME2', 'Creature — Leviathan').
card_original_text('marjhan'/'ME2', 'Marjhan doesn\'t untap during your untap step.\nMarjhan can\'t attack unless defending player controls an Island.\nWhen you control no Islands, sacrifice Marjhan.\n{U}{U}, Sacrifice a creature: Untap Marjhan. Play this ability only during your upkeep.\n{U}{U}: Marjhan gets -1/-0 until end of turn and deals 1 damage to target attacking creature without flying.').
card_image_name('marjhan'/'ME2', 'marjhan').
card_uid('marjhan'/'ME2', 'ME2:Marjhan:marjhan').
card_rarity('marjhan'/'ME2', 'Rare').
card_artist('marjhan'/'ME2', 'Daniel Gelon').
card_number('marjhan'/'ME2', '54').
card_multiverse_id('marjhan'/'ME2', '184593').

card_in_set('mesmeric trance', 'ME2').
card_original_type('mesmeric trance'/'ME2', 'Enchantment').
card_original_text('mesmeric trance'/'ME2', 'Cumulative upkeep {1}\n{U}, Discard a card: Draw a card.').
card_image_name('mesmeric trance'/'ME2', 'mesmeric trance').
card_uid('mesmeric trance'/'ME2', 'ME2:Mesmeric Trance:mesmeric trance').
card_rarity('mesmeric trance'/'ME2', 'Rare').
card_artist('mesmeric trance'/'ME2', 'Dan Frazier').
card_number('mesmeric trance'/'ME2', '55').
card_flavor_text('mesmeric trance'/'ME2', '\"Magic overused can freeze the mind. Creativity is more important than power.\"\n—Zur the Enchanter').
card_multiverse_id('mesmeric trance'/'ME2', '184747').

card_in_set('meteor shower', 'ME2').
card_original_type('meteor shower'/'ME2', 'Sorcery').
card_original_text('meteor shower'/'ME2', 'Meteor Shower deals X plus 1 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('meteor shower'/'ME2', 'meteor shower').
card_uid('meteor shower'/'ME2', 'ME2:Meteor Shower:meteor shower').
card_rarity('meteor shower'/'ME2', 'Common').
card_artist('meteor shower'/'ME2', 'Rick Emond').
card_number('meteor shower'/'ME2', '135').
card_flavor_text('meteor shower'/'ME2', '\"Eenie, meenie, minie, moe . . . oh, why not all of them?\"\n—Jaya Ballard, task mage').
card_multiverse_id('meteor shower'/'ME2', '184650').

card_in_set('minion of leshrac', 'ME2').
card_original_type('minion of leshrac'/'ME2', 'Creature — Demon Minion').
card_original_text('minion of leshrac'/'ME2', 'Protection from black\nAt the beginning of your upkeep, Minion of Leshrac deals 5 damage to you unless you sacrifice a creature other than Minion of Leshrac. If Minion of Leshrac deals damage to you this way, tap it.\n{T}: Destroy target creature or land.').
card_image_name('minion of leshrac'/'ME2', 'minion of leshrac').
card_uid('minion of leshrac'/'ME2', 'ME2:Minion of Leshrac:minion of leshrac').
card_rarity('minion of leshrac'/'ME2', 'Rare').
card_artist('minion of leshrac'/'ME2', 'L. A. Williams').
card_number('minion of leshrac'/'ME2', '104').
card_multiverse_id('minion of leshrac'/'ME2', '184740').

card_in_set('mishra\'s groundbreaker', 'ME2').
card_original_type('mishra\'s groundbreaker'/'ME2', 'Artifact').
card_original_text('mishra\'s groundbreaker'/'ME2', '{T}, Sacrifice Mishra\'s Groundbreaker: Target land becomes a 3/3 artifact creature that\'s still a land.').
card_image_name('mishra\'s groundbreaker'/'ME2', 'mishra\'s groundbreaker').
card_uid('mishra\'s groundbreaker'/'ME2', 'ME2:Mishra\'s Groundbreaker:mishra\'s groundbreaker').
card_rarity('mishra\'s groundbreaker'/'ME2', 'Uncommon').
card_artist('mishra\'s groundbreaker'/'ME2', 'Randy Gallegos').
card_number('mishra\'s groundbreaker'/'ME2', '215').
card_flavor_text('mishra\'s groundbreaker'/'ME2', 'The very ground yielded to Mishra\'s wishes.').
card_multiverse_id('mishra\'s groundbreaker'/'ME2', '184560').

card_in_set('misinformation', 'ME2').
card_original_type('misinformation'/'ME2', 'Instant').
card_original_text('misinformation'/'ME2', 'Put up to three target cards from an opponent\'s graveyard on top of his or her library in any order.').
card_image_name('misinformation'/'ME2', 'misinformation').
card_uid('misinformation'/'ME2', 'ME2:Misinformation:misinformation').
card_rarity('misinformation'/'ME2', 'Uncommon').
card_artist('misinformation'/'ME2', 'Richard Kane Ferguson').
card_number('misinformation'/'ME2', '105').
card_flavor_text('misinformation'/'ME2', '\"When you cannot rely on your sources, trust your own senses. When you cannot trust those, you must follow your instincts.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('misinformation'/'ME2', '184698').

card_in_set('mudslide', 'ME2').
card_original_type('mudslide'/'ME2', 'Enchantment').
card_original_text('mudslide'/'ME2', 'Creatures without flying don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player may choose any number of tapped creatures without flying he or she controls and pay {2} for each creature chosen this way. If the player does, untap those creatures.').
card_image_name('mudslide'/'ME2', 'mudslide').
card_uid('mudslide'/'ME2', 'ME2:Mudslide:mudslide').
card_rarity('mudslide'/'ME2', 'Rare').
card_artist('mudslide'/'ME2', 'Brian Snõddy').
card_number('mudslide'/'ME2', '136').
card_multiverse_id('mudslide'/'ME2', '184727').

card_in_set('musician', 'ME2').
card_original_type('musician'/'ME2', 'Creature — Human Wizard').
card_original_text('musician'/'ME2', 'Cumulative upkeep {1}\n{T}: Put a music counter on target creature. If it doesn\'t have \"At the beginning of your upkeep, destroy this creature unless you pay {1} for each music counter on it,\" it gains that ability.').
card_image_name('musician'/'ME2', 'musician').
card_uid('musician'/'ME2', 'ME2:Musician:musician').
card_rarity('musician'/'ME2', 'Rare').
card_artist('musician'/'ME2', 'Drew Tucker').
card_number('musician'/'ME2', '56').
card_multiverse_id('musician'/'ME2', '184755').

card_in_set('narwhal', 'ME2').
card_original_type('narwhal'/'ME2', 'Creature — Whale').
card_original_text('narwhal'/'ME2', 'First strike, protection from red').
card_image_name('narwhal'/'ME2', 'narwhal').
card_uid('narwhal'/'ME2', 'ME2:Narwhal:narwhal').
card_rarity('narwhal'/'ME2', 'Uncommon').
card_artist('narwhal'/'ME2', 'David A. Cherry').
card_number('narwhal'/'ME2', '57').
card_flavor_text('narwhal'/'ME2', '\"Who needs a spear? Ya break off the horn, and ya stab the fish with it. Easy!\"\n—Kakra, sea troll').
card_multiverse_id('narwhal'/'ME2', '184594').

card_in_set('nature\'s blessing', 'ME2').
card_original_type('nature\'s blessing'/'ME2', 'Enchantment').
card_original_text('nature\'s blessing'/'ME2', '{G}{W}: Discard a card: Put a +1/+1 counter on target creature or that creature gains banding, first strike, or trample.').
card_image_name('nature\'s blessing'/'ME2', 'nature\'s blessing').
card_uid('nature\'s blessing'/'ME2', 'ME2:Nature\'s Blessing:nature\'s blessing').
card_rarity('nature\'s blessing'/'ME2', 'Uncommon').
card_artist('nature\'s blessing'/'ME2', 'Sandra Everingham').
card_number('nature\'s blessing'/'ME2', '196').
card_flavor_text('nature\'s blessing'/'ME2', '\"Be open to the blessings, whatever their form.\"\n—Kaysa, elder druid of the Juniper Order').
card_multiverse_id('nature\'s blessing'/'ME2', '184779').

card_in_set('nature\'s wrath', 'ME2').
card_original_type('nature\'s wrath'/'ME2', 'Enchantment').
card_original_text('nature\'s wrath'/'ME2', 'At the beginning of your upkeep, sacrifice Nature\'s Wrath unless you pay {G}.\nWhenever a player puts a Swamp or black permanent into play, he or she sacrifices a Swamp or black permanent.\nWhenever a player puts an Island or blue permanent into play, he or she sacrifices an Island or blue permanent.').
card_image_name('nature\'s wrath'/'ME2', 'nature\'s wrath').
card_uid('nature\'s wrath'/'ME2', 'ME2:Nature\'s Wrath:nature\'s wrath').
card_rarity('nature\'s wrath'/'ME2', 'Rare').
card_artist('nature\'s wrath'/'ME2', 'Liz Danforth').
card_number('nature\'s wrath'/'ME2', '172').
card_multiverse_id('nature\'s wrath'/'ME2', '184561').

card_in_set('necrite', 'ME2').
card_original_type('necrite'/'ME2', 'Creature — Thrull').
card_original_text('necrite'/'ME2', 'Whenever Necrite attacks and isn\'t blocked, you may sacrifice it. If you do, destroy target creature defending player controls. It can\'t be regenerated.').
card_image_name('necrite'/'ME2', 'necrite').
card_uid('necrite'/'ME2', 'ME2:Necrite:necrite').
card_rarity('necrite'/'ME2', 'Common').
card_artist('necrite'/'ME2', 'Ron Spencer').
card_number('necrite'/'ME2', '106').
card_flavor_text('necrite'/'ME2', 'Necrites killed Jherana Rure, ending the counterinsurgency.').
card_multiverse_id('necrite'/'ME2', '184699').

card_in_set('necropotence', 'ME2').
card_original_type('necropotence'/'ME2', 'Enchantment').
card_original_text('necropotence'/'ME2', 'Skip your draw step.\nWhenever you discard a card, remove that card in your graveyard from the game.\nPay 1 life: Remove the top card of your library from the game face down. Put that card into your hand at the end of your turn.').
card_image_name('necropotence'/'ME2', 'necropotence').
card_uid('necropotence'/'ME2', 'ME2:Necropotence:necropotence').
card_rarity('necropotence'/'ME2', 'Rare').
card_artist('necropotence'/'ME2', 'Mark Tedin').
card_number('necropotence'/'ME2', '107').
card_multiverse_id('necropotence'/'ME2', '184651').

card_in_set('night soil', 'ME2').
card_original_type('night soil'/'ME2', 'Enchantment').
card_original_text('night soil'/'ME2', '{1}, Remove two creature cards in a single graveyard from the game: Put a 1/1 green Saproling creature token into play.').
card_image_name('night soil'/'ME2', 'night soil').
card_uid('night soil'/'ME2', 'ME2:Night Soil:night soil').
card_rarity('night soil'/'ME2', 'Uncommon').
card_artist('night soil'/'ME2', 'Heather Hudson').
card_number('night soil'/'ME2', '173').
card_flavor_text('night soil'/'ME2', 'The elves gathered huge piles of rot to grow fungus. Out of imitation or forethought, the thallids did the same.').
card_multiverse_id('night soil'/'ME2', '184700').

card_in_set('orc general', 'ME2').
card_original_type('orc general'/'ME2', 'Creature — Orc Warrior').
card_original_text('orc general'/'ME2', '{T}, Sacrifice another Orc or Goblin: Other Orc creatures get +1/+1 until end of turn.').
card_image_name('orc general'/'ME2', 'orc general').
card_uid('orc general'/'ME2', 'ME2:Orc General:orc general').
card_rarity('orc general'/'ME2', 'Uncommon').
card_artist('orc general'/'ME2', 'Jesper Myrfors').
card_number('orc general'/'ME2', '137').
card_flavor_text('orc general'/'ME2', '\"Your army must fear you more than the enemy. Only then will you triumph.\"\n—Malga Phlegmtooth').
card_multiverse_id('orc general'/'ME2', '184774').

card_in_set('orcish cannoneers', 'ME2').
card_original_type('orcish cannoneers'/'ME2', 'Creature — Orc Warrior').
card_original_text('orcish cannoneers'/'ME2', '{T}: Orcish Cannoneers deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish cannoneers'/'ME2', 'orcish cannoneers').
card_uid('orcish cannoneers'/'ME2', 'ME2:Orcish Cannoneers:orcish cannoneers').
card_rarity('orcish cannoneers'/'ME2', 'Uncommon').
card_artist('orcish cannoneers'/'ME2', 'Dan Frazier').
card_number('orcish cannoneers'/'ME2', '138').
card_flavor_text('orcish cannoneers'/'ME2', '\"It\'s a thankless job, and you\'ll probably die in an explosion. But the pay is pretty good.\"\n—Toothlicker Harj, orcish captain').
card_multiverse_id('orcish cannoneers'/'ME2', '184652').

card_in_set('orcish captain', 'ME2').
card_original_type('orcish captain'/'ME2', 'Creature — Orc Warrior').
card_original_text('orcish captain'/'ME2', '{1}: Flip a coin. If you win the flip, target Orc creature gets +2/+0 until end of turn. If you lose the flip, it gets -0/-2 until end of turn.').
card_image_name('orcish captain'/'ME2', 'orcish captain').
card_uid('orcish captain'/'ME2', 'ME2:Orcish Captain:orcish captain').
card_rarity('orcish captain'/'ME2', 'Uncommon').
card_artist('orcish captain'/'ME2', 'Mark Tedin').
card_number('orcish captain'/'ME2', '139').
card_flavor_text('orcish captain'/'ME2', 'There\'s a chance to win every battle.').
card_multiverse_id('orcish captain'/'ME2', '184505').

card_in_set('orcish conscripts', 'ME2').
card_original_type('orcish conscripts'/'ME2', 'Creature — Orc').
card_original_text('orcish conscripts'/'ME2', 'Orcish Conscripts can\'t attack unless at least two other creatures attack.\nOrcish Conscripts can\'t block unless at least two other creatures block.').
card_image_name('orcish conscripts'/'ME2', 'orcish conscripts').
card_uid('orcish conscripts'/'ME2', 'ME2:Orcish Conscripts:orcish conscripts').
card_rarity('orcish conscripts'/'ME2', 'Common').
card_artist('orcish conscripts'/'ME2', 'Douglas Shuler').
card_number('orcish conscripts'/'ME2', '140').
card_multiverse_id('orcish conscripts'/'ME2', '184653').

card_in_set('orcish farmer', 'ME2').
card_original_type('orcish farmer'/'ME2', 'Creature — Orc').
card_original_text('orcish farmer'/'ME2', '{T}: Target land becomes a Swamp until its controller\'s next untap step.').
card_image_name('orcish farmer'/'ME2', 'orcish farmer').
card_uid('orcish farmer'/'ME2', 'ME2:Orcish Farmer:orcish farmer').
card_rarity('orcish farmer'/'ME2', 'Common').
card_artist('orcish farmer'/'ME2', 'Dan Frazier').
card_number('orcish farmer'/'ME2', '141').
card_flavor_text('orcish farmer'/'ME2', '\"Yes, the farmers keep our soldiers fed. But why do they have to make every battlefield a pigpen?\"\n—Toothlicker Harj, orcish captain').
card_multiverse_id('orcish farmer'/'ME2', '184654').

card_in_set('orcish lumberjack', 'ME2').
card_original_type('orcish lumberjack'/'ME2', 'Creature — Orc').
card_original_text('orcish lumberjack'/'ME2', '{T}, Sacrifice a Forest: Add three mana in any combination of {R} and/or {G} to your mana pool.').
card_image_name('orcish lumberjack'/'ME2', 'orcish lumberjack').
card_uid('orcish lumberjack'/'ME2', 'ME2:Orcish Lumberjack:orcish lumberjack').
card_rarity('orcish lumberjack'/'ME2', 'Common').
card_artist('orcish lumberjack'/'ME2', 'Dan Frazier').
card_number('orcish lumberjack'/'ME2', '142').
card_flavor_text('orcish lumberjack'/'ME2', '\"How did I ever let myself get talked into this project?\"\n—Toothlicker Harj, orcish captain').
card_multiverse_id('orcish lumberjack'/'ME2', '184656').

card_in_set('orcish squatters', 'ME2').
card_original_type('orcish squatters'/'ME2', 'Creature — Orc').
card_original_text('orcish squatters'/'ME2', 'Whenever Orcish Squatters attacks and isn\'t blocked, you may gain control of target land defending player controls for as long as you control Orcish Squatters. If you do, Orcish Squatters deals no combat damage this turn.').
card_image_name('orcish squatters'/'ME2', 'orcish squatters').
card_uid('orcish squatters'/'ME2', 'ME2:Orcish Squatters:orcish squatters').
card_rarity('orcish squatters'/'ME2', 'Rare').
card_artist('orcish squatters'/'ME2', 'Richard Kane Ferguson').
card_number('orcish squatters'/'ME2', '143').
card_multiverse_id('orcish squatters'/'ME2', '184657').

card_in_set('orcish veteran', 'ME2').
card_original_type('orcish veteran'/'ME2', 'Creature — Orc').
card_original_text('orcish veteran'/'ME2', 'Orcish Veteran can\'t block white creatures with power 2 or greater.\n{R}: Orcish Veteran gains first strike until end of turn.').
card_image_name('orcish veteran'/'ME2', 'orcish veteran').
card_uid('orcish veteran'/'ME2', 'ME2:Orcish Veteran:orcish veteran').
card_rarity('orcish veteran'/'ME2', 'Common').
card_artist('orcish veteran'/'ME2', 'Quinton Hoover').
card_number('orcish veteran'/'ME2', '144').
card_flavor_text('orcish veteran'/'ME2', 'Unsuccessful in their early battle for Montford, the orcs quickly tempered their bloodlust with cowardice.').
card_multiverse_id('orcish veteran'/'ME2', '184701').

card_in_set('order of the sacred torch', 'ME2').
card_original_type('order of the sacred torch'/'ME2', 'Creature — Human Knight').
card_original_text('order of the sacred torch'/'ME2', '{T}, Pay 1 life: Counter target black spell.').
card_image_name('order of the sacred torch'/'ME2', 'order of the sacred torch').
card_uid('order of the sacred torch'/'ME2', 'ME2:Order of the Sacred Torch:order of the sacred torch').
card_rarity('order of the sacred torch'/'ME2', 'Rare').
card_artist('order of the sacred torch'/'ME2', 'Ruth Thompson').
card_number('order of the sacred torch'/'ME2', '25').
card_flavor_text('order of the sacred torch'/'ME2', 'A blazing light to drive out the shadows.').
card_multiverse_id('order of the sacred torch'/'ME2', '184658').

card_in_set('order of the white shield', 'ME2').
card_original_type('order of the white shield'/'ME2', 'Creature — Human Knight').
card_original_text('order of the white shield'/'ME2', 'Protection from black\n{W}: Order of the White Shield gains first strike until end of turn.\n{W}{W}: Order of the White Shield gets +1/+0 until end of turn.').
card_image_name('order of the white shield'/'ME2', 'order of the white shield').
card_uid('order of the white shield'/'ME2', 'ME2:Order of the White Shield:order of the white shield').
card_rarity('order of the white shield'/'ME2', 'Uncommon').
card_artist('order of the white shield'/'ME2', 'Ruth Thompson').
card_number('order of the white shield'/'ME2', '26').
card_flavor_text('order of the white shield'/'ME2', '\"Shall we turn away a worthy soul because his parents were peasants? I think not.\"\n—Lucilde Fiksdotter, leader of the Order of the White Shield').
card_multiverse_id('order of the white shield'/'ME2', '184659').

card_in_set('panic', 'ME2').
card_original_type('panic'/'ME2', 'Instant').
card_original_text('panic'/'ME2', 'Play Panic only during combat before blockers are declared.\nTarget creature can\'t block this turn. \nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('panic'/'ME2', 'panic').
card_uid('panic'/'ME2', 'ME2:Panic:panic').
card_rarity('panic'/'ME2', 'Common').
card_artist('panic'/'ME2', 'Mike Kimble').
card_number('panic'/'ME2', '145').
card_flavor_text('panic'/'ME2', '\"If you\'d been there, you would\'ve run from that deer, too!\"\n—Jaya Ballard, Task Mage').
card_multiverse_id('panic'/'ME2', '184742').

card_in_set('personal tutor', 'ME2').
card_original_type('personal tutor'/'ME2', 'Sorcery').
card_original_text('personal tutor'/'ME2', 'Search your library for a sorcery card and reveal that card. Shuffle your library, then put the card on top of it.').
card_image_name('personal tutor'/'ME2', 'personal tutor').
card_uid('personal tutor'/'ME2', 'ME2:Personal Tutor:personal tutor').
card_rarity('personal tutor'/'ME2', 'Uncommon').
card_artist('personal tutor'/'ME2', 'D. Alexander Gregory').
card_number('personal tutor'/'ME2', '58').
card_multiverse_id('personal tutor'/'ME2', '184775').

card_in_set('phantasmal fiend', 'ME2').
card_original_type('phantasmal fiend'/'ME2', 'Creature — Illusion').
card_original_text('phantasmal fiend'/'ME2', '{B}: Phantasmal Fiend gets +1/-1 until end of turn.\n{1}{U}: Switch Phantasmal Fiend\'s power and toughness until end of turn.').
card_image_name('phantasmal fiend'/'ME2', 'phantasmal fiend').
card_uid('phantasmal fiend'/'ME2', 'ME2:Phantasmal Fiend:phantasmal fiend').
card_rarity('phantasmal fiend'/'ME2', 'Common').
card_artist('phantasmal fiend'/'ME2', 'Scott Kirschner').
card_number('phantasmal fiend'/'ME2', '108').
card_multiverse_id('phantasmal fiend'/'ME2', '190426').

card_in_set('phantasmal mount', 'ME2').
card_original_type('phantasmal mount'/'ME2', 'Creature — Illusion Horse').
card_original_text('phantasmal mount'/'ME2', 'Flying\n{T}: Target creature you control with toughness 2 or less gets +1/+1 and gains flying until end of turn. When Phantasmal Mount leaves play this turn, sacrifice that creature. When the creature leaves play this turn, sacrifice Phantasmal Mount.').
card_image_name('phantasmal mount'/'ME2', 'phantasmal mount').
card_uid('phantasmal mount'/'ME2', 'ME2:Phantasmal Mount:phantasmal mount').
card_rarity('phantasmal mount'/'ME2', 'Common').
card_artist('phantasmal mount'/'ME2', 'Melissa A. Benson').
card_number('phantasmal mount'/'ME2', '59').
card_multiverse_id('phantasmal mount'/'ME2', '184708').

card_in_set('phyrexian devourer', 'ME2').
card_original_type('phyrexian devourer'/'ME2', 'Artifact Creature — Construct').
card_original_text('phyrexian devourer'/'ME2', 'When Phyrexian Devourer\'s power is 7 or greater, sacrifice it.\nRemove the top card of your library from the game: Put X +1/+1 counters on Phyrexian Devourer, where X is the removed card\'s converted mana cost. If Phyrexian Devourer\'s power is 7 or greater, sacrifice it.').
card_image_name('phyrexian devourer'/'ME2', 'phyrexian devourer').
card_uid('phyrexian devourer'/'ME2', 'ME2:Phyrexian Devourer:phyrexian devourer').
card_rarity('phyrexian devourer'/'ME2', 'Uncommon').
card_artist('phyrexian devourer'/'ME2', 'Mark Tedin').
card_number('phyrexian devourer'/'ME2', '216').
card_multiverse_id('phyrexian devourer'/'ME2', '184563').

card_in_set('phyrexian portal', 'ME2').
card_original_type('phyrexian portal'/'ME2', 'Artifact').
card_original_text('phyrexian portal'/'ME2', '{3}: Target opponent looks at the top ten cards of your library and separates them into two face-down piles. Remove one of those piles from the game. Search the other pile for a card and put it into your hand, then shuffle your library. Play this ability only if your library has ten or more cards in it.').
card_image_name('phyrexian portal'/'ME2', 'phyrexian portal').
card_uid('phyrexian portal'/'ME2', 'ME2:Phyrexian Portal:phyrexian portal').
card_rarity('phyrexian portal'/'ME2', 'Rare').
card_artist('phyrexian portal'/'ME2', 'Pete Venters').
card_number('phyrexian portal'/'ME2', '217').
card_multiverse_id('phyrexian portal'/'ME2', '184564').

card_in_set('pillage', 'ME2').
card_original_type('pillage'/'ME2', 'Sorcery').
card_original_text('pillage'/'ME2', 'Destroy target artifact or land. It can\'t be regenerated.').
card_image_name('pillage'/'ME2', 'pillage').
card_uid('pillage'/'ME2', 'ME2:Pillage:pillage').
card_rarity('pillage'/'ME2', 'Uncommon').
card_artist('pillage'/'ME2', 'Richard Kane Ferguson').
card_number('pillage'/'ME2', '146').
card_flavor_text('pillage'/'ME2', '\"Were they to reduce us to ash, we would clog their throats and sting their eyes in payment.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('pillage'/'ME2', '184567').

card_in_set('portent', 'ME2').
card_original_type('portent'/'ME2', 'Sorcery').
card_original_text('portent'/'ME2', 'Look at the top three cards of target player\'s library, then put them back in any order. You may have that player shuffle his or her library.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_image_name('portent'/'ME2', 'portent').
card_uid('portent'/'ME2', 'ME2:Portent:portent').
card_rarity('portent'/'ME2', 'Common').
card_artist('portent'/'ME2', 'Liz Danforth').
card_number('portent'/'ME2', '60').
card_multiverse_id('portent'/'ME2', '184661').

card_in_set('pyrokinesis', 'ME2').
card_original_type('pyrokinesis'/'ME2', 'Instant').
card_original_text('pyrokinesis'/'ME2', 'You may remove a red card in your hand from the game rather than pay Pyrokinesis\'s mana cost.\nPyrokinesis deals 4 damage divided as you choose among any number of target creatures.').
card_image_name('pyrokinesis'/'ME2', 'pyrokinesis').
card_uid('pyrokinesis'/'ME2', 'ME2:Pyrokinesis:pyrokinesis').
card_rarity('pyrokinesis'/'ME2', 'Rare').
card_artist('pyrokinesis'/'ME2', 'Ron Spencer').
card_number('pyrokinesis'/'ME2', '147').
card_flavor_text('pyrokinesis'/'ME2', '\"Anybody want some . . . toast?\"\n—Jaya Ballard, task mage').
card_multiverse_id('pyrokinesis'/'ME2', '184763').

card_in_set('ravages of war', 'ME2').
card_original_type('ravages of war'/'ME2', 'Sorcery').
card_original_text('ravages of war'/'ME2', 'Destroy all lands.').
card_image_name('ravages of war'/'ME2', 'ravages of war').
card_uid('ravages of war'/'ME2', 'ME2:Ravages of War:ravages of war').
card_rarity('ravages of war'/'ME2', 'Rare').
card_artist('ravages of war'/'ME2', 'Fang Yue').
card_number('ravages of war'/'ME2', '27').
card_flavor_text('ravages of war'/'ME2', '\"Thorn bushes spring up wherever the army has passed. Lean years follow in the wake of a great war.\"\n—Lao Tzu, Tao Te Ching, trans. Feng and English').
card_multiverse_id('ravages of war'/'ME2', '184724').

card_in_set('ray of command', 'ME2').
card_original_type('ray of command'/'ME2', 'Instant').
card_original_text('ray of command'/'ME2', 'Untap target creature an opponent controls and gain control of it until end of turn. That creature gains haste until end of turn. When you lose control of the creature, tap it.').
card_image_name('ray of command'/'ME2', 'ray of command').
card_uid('ray of command'/'ME2', 'ME2:Ray of Command:ray of command').
card_rarity('ray of command'/'ME2', 'Uncommon').
card_artist('ray of command'/'ME2', 'Harold McNeill').
card_number('ray of command'/'ME2', '61').
card_multiverse_id('ray of command'/'ME2', '184663').

card_in_set('red cliffs armada', 'ME2').
card_original_type('red cliffs armada'/'ME2', 'Creature — Human Soldier').
card_original_text('red cliffs armada'/'ME2', 'Red Cliffs Armada can\'t attack unless defending player controls an Island.').
card_image_name('red cliffs armada'/'ME2', 'red cliffs armada').
card_uid('red cliffs armada'/'ME2', 'ME2:Red Cliffs Armada:red cliffs armada').
card_rarity('red cliffs armada'/'ME2', 'Common').
card_artist('red cliffs armada'/'ME2', 'Zhang Jiazhen').
card_number('red cliffs armada'/'ME2', '62').
card_flavor_text('red cliffs armada'/'ME2', 'By the battle of Red Cliffs in the year 208, the Wu kingdom controlled more than seven thousand warships on the Yangtze.').
card_multiverse_id('red cliffs armada'/'ME2', '184725').

card_in_set('reinforcements', 'ME2').
card_original_type('reinforcements'/'ME2', 'Instant').
card_original_text('reinforcements'/'ME2', 'Put up to three target creature cards from your graveyard on top of your library.').
card_image_name('reinforcements'/'ME2', 'reinforcements').
card_uid('reinforcements'/'ME2', 'ME2:Reinforcements:reinforcements').
card_rarity('reinforcements'/'ME2', 'Common').
card_artist('reinforcements'/'ME2', 'Diana Vick').
card_number('reinforcements'/'ME2', '28').
card_flavor_text('reinforcements'/'ME2', '\"Let them send their legions! I will show them that my truth is stronger than their swords.\"\n—General Varchild').
card_multiverse_id('reinforcements'/'ME2', '184597').

card_in_set('reprisal', 'ME2').
card_original_type('reprisal'/'ME2', 'Instant').
card_original_text('reprisal'/'ME2', 'Destroy target creature with power 4 or greater. It can\'t be regenerated.').
card_image_name('reprisal'/'ME2', 'reprisal').
card_uid('reprisal'/'ME2', 'ME2:Reprisal:reprisal').
card_rarity('reprisal'/'ME2', 'Common').
card_artist('reprisal'/'ME2', 'Randy Asplund-Faith').
card_number('reprisal'/'ME2', '29').
card_flavor_text('reprisal'/'ME2', '\"The meek shall fight as one, and they shall overcome even the greatest of foes.\"\n—Halvor Arensson, Kjeldoran priest').
card_multiverse_id('reprisal'/'ME2', '184707').

card_in_set('retribution', 'ME2').
card_original_type('retribution'/'ME2', 'Sorcery').
card_original_text('retribution'/'ME2', 'Choose two target creatures an opponent controls. That player chooses and sacrifices one of those creatures. Put a -1/-1 counter on the other.').
card_image_name('retribution'/'ME2', 'retribution').
card_uid('retribution'/'ME2', 'ME2:Retribution:retribution').
card_rarity('retribution'/'ME2', 'Uncommon').
card_artist('retribution'/'ME2', 'Mark Tedin').
card_number('retribution'/'ME2', '148').
card_multiverse_id('retribution'/'ME2', '184595').

card_in_set('righteous fury', 'ME2').
card_original_type('righteous fury'/'ME2', 'Sorcery').
card_original_text('righteous fury'/'ME2', 'Destroy all tapped creatures. You gain 2 life for each creature destroyed this way.').
card_image_name('righteous fury'/'ME2', 'righteous fury').
card_uid('righteous fury'/'ME2', 'ME2:Righteous Fury:righteous fury').
card_rarity('righteous fury'/'ME2', 'Rare').
card_artist('righteous fury'/'ME2', 'Edward P. Beard, Jr.').
card_number('righteous fury'/'ME2', '30').
card_multiverse_id('righteous fury'/'ME2', '184756').

card_in_set('ritual of subdual', 'ME2').
card_original_type('ritual of subdual'/'ME2', 'Enchantment').
card_original_text('ritual of subdual'/'ME2', 'Cumulative upkeep {2}\nIf a land is tapped for mana, it produces colorless mana instead of any other type.').
card_image_name('ritual of subdual'/'ME2', 'ritual of subdual').
card_uid('ritual of subdual'/'ME2', 'ME2:Ritual of Subdual:ritual of subdual').
card_rarity('ritual of subdual'/'ME2', 'Rare').
card_artist('ritual of subdual'/'ME2', 'Justin Hampton').
card_number('ritual of subdual'/'ME2', '174').
card_flavor_text('ritual of subdual'/'ME2', '\"That which does not bend to the will of Freyalise shall surely break.\"\n—Kolbjörn, elder druid of the Juniper Order').
card_multiverse_id('ritual of subdual'/'ME2', '184664').

card_in_set('ritual of the machine', 'ME2').
card_original_type('ritual of the machine'/'ME2', 'Sorcery').
card_original_text('ritual of the machine'/'ME2', 'As an additional cost to play Ritual of the Machine, sacrifice a creature.\nGain control of target nonblack, nonartifact creature.').
card_image_name('ritual of the machine'/'ME2', 'ritual of the machine').
card_uid('ritual of the machine'/'ME2', 'ME2:Ritual of the Machine:ritual of the machine').
card_rarity('ritual of the machine'/'ME2', 'Rare').
card_artist('ritual of the machine'/'ME2', 'Anson Maddocks').
card_number('ritual of the machine'/'ME2', '109').
card_flavor_text('ritual of the machine'/'ME2', '\"Rumors persist of dark deeds performed in the depths of Soldev. When will Dagsson heed the danger therein?\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('ritual of the machine'/'ME2', '184570').

card_in_set('rogue skycaptain', 'ME2').
card_original_type('rogue skycaptain'/'ME2', 'Creature — Human Rogue Mercenary').
card_original_text('rogue skycaptain'/'ME2', 'Flying\nAt the beginning of your upkeep, put a wage counter on Rogue Skycaptain. You may pay {2} for each wage counter on it. If you don\'t, remove all wage counters from Rogue Skycaptain and an opponent gains control of it.').
card_image_name('rogue skycaptain'/'ME2', 'rogue skycaptain').
card_uid('rogue skycaptain'/'ME2', 'ME2:Rogue Skycaptain:rogue skycaptain').
card_rarity('rogue skycaptain'/'ME2', 'Rare').
card_artist('rogue skycaptain'/'ME2', 'Randy Asplund-Faith').
card_number('rogue skycaptain'/'ME2', '149').
card_multiverse_id('rogue skycaptain'/'ME2', '184571').

card_in_set('roterothopter', 'ME2').
card_original_type('roterothopter'/'ME2', 'Artifact Creature — Thopter').
card_original_text('roterothopter'/'ME2', 'Flying\n{2}: Roterothopter gets +1/+0 until end of turn. Play this ability no more than twice each turn.').
card_image_name('roterothopter'/'ME2', 'roterothopter').
card_uid('roterothopter'/'ME2', 'ME2:Roterothopter:roterothopter').
card_rarity('roterothopter'/'ME2', 'Common').
card_artist('roterothopter'/'ME2', 'Amy Weber').
card_number('roterothopter'/'ME2', '218').
card_flavor_text('roterothopter'/'ME2', '\"The roterothoper is as insidious as it is ingenious. It is one of the few creations of our school that I take no pride in.\"\n—Baki, wizard attendant').
card_multiverse_id('roterothopter'/'ME2', '184596').

card_in_set('royal decree', 'ME2').
card_original_type('royal decree'/'ME2', 'Enchantment').
card_original_text('royal decree'/'ME2', 'Cumulative upkeep {W}\nWhenever a Swamp, Mountain, black permanent, or red permanent becomes tapped, Royal Decree deals 1 damage to that permanent\'s controller.').
card_image_name('royal decree'/'ME2', 'royal decree').
card_uid('royal decree'/'ME2', 'ME2:Royal Decree:royal decree').
card_rarity('royal decree'/'ME2', 'Rare').
card_artist('royal decree'/'ME2', 'Pete Venters').
card_number('royal decree'/'ME2', '31').
card_multiverse_id('royal decree'/'ME2', '184715').

card_in_set('royal trooper', 'ME2').
card_original_type('royal trooper'/'ME2', 'Creature — Human Soldier').
card_original_text('royal trooper'/'ME2', 'Whenever Royal Trooper blocks, it gets +2/+2 until end of turn.').
card_image_name('royal trooper'/'ME2', 'royal trooper').
card_uid('royal trooper'/'ME2', 'ME2:Royal Trooper:royal trooper').
card_rarity('royal trooper'/'ME2', 'Common').
card_artist('royal trooper'/'ME2', 'Scott M. Fischer').
card_number('royal trooper'/'ME2', '32').
card_flavor_text('royal trooper'/'ME2', '\"Fortune does not side with the fainthearted.\"\n—Sophocles, Phaedra').
card_multiverse_id('royal trooper'/'ME2', '184758').

card_in_set('ruins of trokair', 'ME2').
card_original_type('ruins of trokair'/'ME2', 'Land').
card_original_text('ruins of trokair'/'ME2', 'Ruins of Trokair comes into play tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Ruins of Trokair: Add {W}{W} to your mana pool.').
card_image_name('ruins of trokair'/'ME2', 'ruins of trokair').
card_uid('ruins of trokair'/'ME2', 'ME2:Ruins of Trokair:ruins of trokair').
card_rarity('ruins of trokair'/'ME2', 'Uncommon').
card_artist('ruins of trokair'/'ME2', 'Mark Poole').
card_number('ruins of trokair'/'ME2', '234').
card_multiverse_id('ruins of trokair'/'ME2', '184516').

card_in_set('sacred boon', 'ME2').
card_original_type('sacred boon'/'ME2', 'Instant').
card_original_text('sacred boon'/'ME2', 'Prevent the next 3 damage that would be dealt to target creature this turn. At end of turn, put a +0/+1 counter on that creature for each 1 damage prevented this way.').
card_image_name('sacred boon'/'ME2', 'sacred boon').
card_uid('sacred boon'/'ME2', 'ME2:Sacred Boon:sacred boon').
card_rarity('sacred boon'/'ME2', 'Uncommon').
card_artist('sacred boon'/'ME2', 'Mike Raabe').
card_number('sacred boon'/'ME2', '33').
card_flavor_text('sacred boon'/'ME2', '\"Divine gifts are granted to those who are worthy.\"\n—Halvor Arenson, Kjeldoran priest').
card_multiverse_id('sacred boon'/'ME2', '184608').

card_in_set('savannah', 'ME2').
card_original_type('savannah'/'ME2', 'Land — Forest Plains').
card_original_text('savannah'/'ME2', '').
card_image_name('savannah'/'ME2', 'savannah').
card_uid('savannah'/'ME2', 'ME2:Savannah:savannah').
card_rarity('savannah'/'ME2', 'Rare').
card_artist('savannah'/'ME2', 'Rob Alexander').
card_number('savannah'/'ME2', '235').
card_multiverse_id('savannah'/'ME2', '184749').

card_in_set('scars of the veteran', 'ME2').
card_original_type('scars of the veteran'/'ME2', 'Instant').
card_original_text('scars of the veteran'/'ME2', 'You may remove a white card in your hand from the game rather than pay Scars of the Veteran\'s mana cost.\nPrevent the next 7 damage that would be dealt to target creature or player this turn. At end of turn, put a +0/+1 counter on that creature for each 1 damage prevented this way.').
card_image_name('scars of the veteran'/'ME2', 'scars of the veteran').
card_uid('scars of the veteran'/'ME2', 'ME2:Scars of the Veteran:scars of the veteran').
card_rarity('scars of the veteran'/'ME2', 'Rare').
card_artist('scars of the veteran'/'ME2', 'Dan Frazier').
card_number('scars of the veteran'/'ME2', '34').
card_multiverse_id('scars of the veteran'/'ME2', '184572').

card_in_set('screeching drake', 'ME2').
card_original_type('screeching drake'/'ME2', 'Creature — Drake').
card_original_text('screeching drake'/'ME2', 'Flying\nWhen Screeching Drake comes into play, draw a card, then discard a card.').
card_image_name('screeching drake'/'ME2', 'screeching drake').
card_uid('screeching drake'/'ME2', 'ME2:Screeching Drake:screeching drake').
card_rarity('screeching drake'/'ME2', 'Common').
card_artist('screeching drake'/'ME2', 'Anson Maddocks').
card_number('screeching drake'/'ME2', '63').
card_multiverse_id('screeching drake'/'ME2', '184738').

card_in_set('sea drake', 'ME2').
card_original_type('sea drake'/'ME2', 'Creature — Drake').
card_original_text('sea drake'/'ME2', 'Flying\nWhen Sea Drake comes into play, return two target lands you control to their owner\'s hand.').
card_image_name('sea drake'/'ME2', 'sea drake').
card_uid('sea drake'/'ME2', 'ME2:Sea Drake:sea drake').
card_rarity('sea drake'/'ME2', 'Rare').
card_artist('sea drake'/'ME2', 'Rebecca Guay').
card_number('sea drake'/'ME2', '64').
card_multiverse_id('sea drake'/'ME2', '184769').

card_in_set('sea spirit', 'ME2').
card_original_type('sea spirit'/'ME2', 'Creature — Elemental Spirit').
card_original_text('sea spirit'/'ME2', '{U}: Sea Spirit gets +1/+0 until end of turn.').
card_image_name('sea spirit'/'ME2', 'sea spirit').
card_uid('sea spirit'/'ME2', 'ME2:Sea Spirit:sea spirit').
card_rarity('sea spirit'/'ME2', 'Uncommon').
card_artist('sea spirit'/'ME2', 'Rob Alexander').
card_number('sea spirit'/'ME2', '65').
card_flavor_text('sea spirit'/'ME2', '\"It rose above our heads, above the ship, and still higher yet. No foggy, ice-laden sea in the world could frighten me more.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('sea spirit'/'ME2', '184660').

card_in_set('shield bearer', 'ME2').
card_original_type('shield bearer'/'ME2', 'Creature — Human Soldier').
card_original_text('shield bearer'/'ME2', 'Banding').
card_image_name('shield bearer'/'ME2', 'shield bearer').
card_uid('shield bearer'/'ME2', 'ME2:Shield Bearer:shield bearer').
card_rarity('shield bearer'/'ME2', 'Common').
card_artist('shield bearer'/'ME2', 'Dan Frazier').
card_number('shield bearer'/'ME2', '35').
card_flavor_text('shield bearer'/'ME2', '\"You have almost completed your four years, my son. Soon you shall be a skyknight.\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('shield bearer'/'ME2', '184702').

card_in_set('shrink', 'ME2').
card_original_type('shrink'/'ME2', 'Instant').
card_original_text('shrink'/'ME2', 'Target creature gets -5/-0 until end of turn.').
card_image_name('shrink'/'ME2', 'shrink').
card_uid('shrink'/'ME2', 'ME2:Shrink:shrink').
card_rarity('shrink'/'ME2', 'Common').
card_artist('shrink'/'ME2', 'Liz Danforth').
card_number('shrink'/'ME2', '175').
card_flavor_text('shrink'/'ME2', '\"Do not fear adversity. Let your courage be your strength.\"\n—Onatah, Anaba shaman').
card_multiverse_id('shrink'/'ME2', '184688').

card_in_set('shyft', 'ME2').
card_original_type('shyft'/'ME2', 'Creature — Shapeshifter').
card_original_text('shyft'/'ME2', 'At the beginning of your upkeep, you may have Shyft become the color or colors of your choice.').
card_image_name('shyft'/'ME2', 'shyft').
card_uid('shyft'/'ME2', 'ME2:Shyft:shyft').
card_rarity('shyft'/'ME2', 'Common').
card_artist('shyft'/'ME2', 'Richard Thomas').
card_number('shyft'/'ME2', '66').
card_flavor_text('shyft'/'ME2', '\"Capturing this specimen was not easy. Study it well, that you someday might be as versatile.\"\n—Gerda Äagesdotter, Archmage of the Unseen').
card_multiverse_id('shyft'/'ME2', '184721').

card_in_set('sibilant spirit', 'ME2').
card_original_type('sibilant spirit'/'ME2', 'Creature — Spirit').
card_original_text('sibilant spirit'/'ME2', 'Flying\nWhenever Sibilant Spirit attacks, defending player may draw a card.').
card_image_name('sibilant spirit'/'ME2', 'sibilant spirit').
card_uid('sibilant spirit'/'ME2', 'ME2:Sibilant Spirit:sibilant spirit').
card_rarity('sibilant spirit'/'ME2', 'Rare').
card_artist('sibilant spirit'/'ME2', 'Ron Spencer').
card_number('sibilant spirit'/'ME2', '67').
card_flavor_text('sibilant spirit'/'ME2', '\"Relax. I\'m sure it\'s just a snake hissing.\"\n—Avram Garrisson, leader of the Knights of Stromgald').
card_multiverse_id('sibilant spirit'/'ME2', '184665').

card_in_set('skeleton ship', 'ME2').
card_original_type('skeleton ship'/'ME2', 'Legendary Creature — Skeleton').
card_original_text('skeleton ship'/'ME2', 'When you control no Islands, sacrifice Skeleton Ship.\n{T}: Put a -1/-1 counter on target creature.').
card_image_name('skeleton ship'/'ME2', 'skeleton ship').
card_uid('skeleton ship'/'ME2', 'ME2:Skeleton Ship:skeleton ship').
card_rarity('skeleton ship'/'ME2', 'Rare').
card_artist('skeleton ship'/'ME2', 'Amy Weber & Tom Wänerstrand').
card_number('skeleton ship'/'ME2', '197').
card_flavor_text('skeleton ship'/'ME2', '\"The sea gives up her dead as easily as the soil.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('skeleton ship'/'ME2', '184666').

card_in_set('skull catapult', 'ME2').
card_original_type('skull catapult'/'ME2', 'Artifact').
card_original_text('skull catapult'/'ME2', '{1}, {T}, Sacrifice a creature: Skull Catapult deals 2 damage to target creature or player.').
card_image_name('skull catapult'/'ME2', 'skull catapult').
card_uid('skull catapult'/'ME2', 'ME2:Skull Catapult:skull catapult').
card_rarity('skull catapult'/'ME2', 'Uncommon').
card_artist('skull catapult'/'ME2', 'Bryon Wackwitz').
card_number('skull catapult'/'ME2', '219').
card_flavor_text('skull catapult'/'ME2', '\"Let any who doubt the evil of using the ancient devices look at this infernal machine. What manner of fiend would design such a sadistic device?\"\n—Sorine Relicbane, Soldevi heretic').
card_multiverse_id('skull catapult'/'ME2', '184667').

card_in_set('snow fortress', 'ME2').
card_original_type('snow fortress'/'ME2', 'Artifact Creature — Wall').
card_original_text('snow fortress'/'ME2', 'Defender\n{1}: Snow Fortress gets +1/+0 until end of turn.\n{1}: Snow Fortress gets +0/+1 until end of turn.\n{3}: Snow Fortress deals 1 damage to target creature without flying that\'s attacking you.').
card_image_name('snow fortress'/'ME2', 'snow fortress').
card_uid('snow fortress'/'ME2', 'ME2:Snow Fortress:snow fortress').
card_rarity('snow fortress'/'ME2', 'Uncommon').
card_artist('snow fortress'/'ME2', 'Jeff A. Menges').
card_number('snow fortress'/'ME2', '220').
card_multiverse_id('snow fortress'/'ME2', '184705').

card_in_set('snow-covered forest', 'ME2').
card_original_type('snow-covered forest'/'ME2', 'Basic Snow Land — Forest').
card_original_text('snow-covered forest'/'ME2', 'G').
card_image_name('snow-covered forest'/'ME2', 'snow-covered forest').
card_uid('snow-covered forest'/'ME2', 'ME2:Snow-Covered Forest:snow-covered forest').
card_rarity('snow-covered forest'/'ME2', 'Basic Land').
card_artist('snow-covered forest'/'ME2', 'Pat Morrissey').
card_number('snow-covered forest'/'ME2', '245').
card_multiverse_id('snow-covered forest'/'ME2', '184812').

card_in_set('snow-covered island', 'ME2').
card_original_type('snow-covered island'/'ME2', 'Basic Snow Land — Island').
card_original_text('snow-covered island'/'ME2', 'U').
card_image_name('snow-covered island'/'ME2', 'snow-covered island').
card_uid('snow-covered island'/'ME2', 'ME2:Snow-Covered Island:snow-covered island').
card_rarity('snow-covered island'/'ME2', 'Basic Land').
card_artist('snow-covered island'/'ME2', 'Anson Maddocks').
card_number('snow-covered island'/'ME2', '242').
card_multiverse_id('snow-covered island'/'ME2', '184813').

card_in_set('snow-covered mountain', 'ME2').
card_original_type('snow-covered mountain'/'ME2', 'Basic Snow Land — Mountain').
card_original_text('snow-covered mountain'/'ME2', 'R').
card_image_name('snow-covered mountain'/'ME2', 'snow-covered mountain').
card_uid('snow-covered mountain'/'ME2', 'ME2:Snow-Covered Mountain:snow-covered mountain').
card_rarity('snow-covered mountain'/'ME2', 'Basic Land').
card_artist('snow-covered mountain'/'ME2', 'Tom Wänerstrand').
card_number('snow-covered mountain'/'ME2', '244').
card_multiverse_id('snow-covered mountain'/'ME2', '184814').

card_in_set('snow-covered plains', 'ME2').
card_original_type('snow-covered plains'/'ME2', 'Basic Snow Land — Plains').
card_original_text('snow-covered plains'/'ME2', 'W').
card_image_name('snow-covered plains'/'ME2', 'snow-covered plains').
card_uid('snow-covered plains'/'ME2', 'ME2:Snow-Covered Plains:snow-covered plains').
card_rarity('snow-covered plains'/'ME2', 'Basic Land').
card_artist('snow-covered plains'/'ME2', 'Christopher Rush').
card_number('snow-covered plains'/'ME2', '241').
card_multiverse_id('snow-covered plains'/'ME2', '184815').

card_in_set('snow-covered swamp', 'ME2').
card_original_type('snow-covered swamp'/'ME2', 'Basic Snow Land — Swamp').
card_original_text('snow-covered swamp'/'ME2', 'B').
card_image_name('snow-covered swamp'/'ME2', 'snow-covered swamp').
card_uid('snow-covered swamp'/'ME2', 'ME2:Snow-Covered Swamp:snow-covered swamp').
card_rarity('snow-covered swamp'/'ME2', 'Basic Land').
card_artist('snow-covered swamp'/'ME2', 'Douglas Shuler').
card_number('snow-covered swamp'/'ME2', '243').
card_multiverse_id('snow-covered swamp'/'ME2', '184816').

card_in_set('soldevi digger', 'ME2').
card_original_type('soldevi digger'/'ME2', 'Artifact').
card_original_text('soldevi digger'/'ME2', '{2}: Put the top card of your graveyard on the bottom of your library.').
card_image_name('soldevi digger'/'ME2', 'soldevi digger').
card_uid('soldevi digger'/'ME2', 'ME2:Soldevi Digger:soldevi digger').
card_rarity('soldevi digger'/'ME2', 'Uncommon').
card_artist('soldevi digger'/'ME2', 'Amy Weber').
card_number('soldevi digger'/'ME2', '221').
card_flavor_text('soldevi digger'/'ME2', '\"This ceaseless device has helped uncover marvels unreachable by mere flesh.\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('soldevi digger'/'ME2', '184574').

card_in_set('soldevi excavations', 'ME2').
card_original_type('soldevi excavations'/'ME2', 'Land').
card_original_text('soldevi excavations'/'ME2', 'If Soldevi Excavations would come into play, sacrifice an untapped Island instead. If you do, put Soldevi Excavations into play. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {1}{U} to your mana pool.\n{1}, {T}: Look at the top card of your library. You may put that card on the bottom of your library.').
card_image_name('soldevi excavations'/'ME2', 'soldevi excavations').
card_uid('soldevi excavations'/'ME2', 'ME2:Soldevi Excavations:soldevi excavations').
card_rarity('soldevi excavations'/'ME2', 'Rare').
card_artist('soldevi excavations'/'ME2', 'Liz Danforth').
card_number('soldevi excavations'/'ME2', '236').
card_multiverse_id('soldevi excavations'/'ME2', '184575').

card_in_set('soldevi simulacrum', 'ME2').
card_original_type('soldevi simulacrum'/'ME2', 'Artifact Creature — Soldier').
card_original_text('soldevi simulacrum'/'ME2', 'Cumulative upkeep {1}\n{1}: Soldevi Simulacrum gets +1/+0 until end of turn.').
card_image_name('soldevi simulacrum'/'ME2', 'soldevi simulacrum').
card_uid('soldevi simulacrum'/'ME2', 'ME2:Soldevi Simulacrum:soldevi simulacrum').
card_rarity('soldevi simulacrum'/'ME2', 'Uncommon').
card_artist('soldevi simulacrum'/'ME2', 'Dan Frazier').
card_number('soldevi simulacrum'/'ME2', '222').
card_flavor_text('soldevi simulacrum'/'ME2', 'They look human—until they bleed.').
card_multiverse_id('soldevi simulacrum'/'ME2', '184668').

card_in_set('songs of the damned', 'ME2').
card_original_type('songs of the damned'/'ME2', 'Instant').
card_original_text('songs of the damned'/'ME2', 'Add {B} to your mana pool for each creature card in your graveyard.').
card_image_name('songs of the damned'/'ME2', 'songs of the damned').
card_uid('songs of the damned'/'ME2', 'ME2:Songs of the Damned:songs of the damned').
card_rarity('songs of the damned'/'ME2', 'Common').
card_artist('songs of the damned'/'ME2', 'Pete Venters').
card_number('songs of the damned'/'ME2', '110').
card_flavor_text('songs of the damned'/'ME2', 'Not wind, but the breath of the dead.').
card_multiverse_id('songs of the damned'/'ME2', '184745').

card_in_set('soul exchange', 'ME2').
card_original_type('soul exchange'/'ME2', 'Sorcery').
card_original_text('soul exchange'/'ME2', 'As an additional cost to play Soul Exchange, remove a creature you control from the game.\nReturn target creature card from your graveyard to play. Put a +2/+2 counter on that creature if the removed creature was a Thrull.').
card_image_name('soul exchange'/'ME2', 'soul exchange').
card_uid('soul exchange'/'ME2', 'ME2:Soul Exchange:soul exchange').
card_rarity('soul exchange'/'ME2', 'Uncommon').
card_artist('soul exchange'/'ME2', 'Anthony S. Waters').
card_number('soul exchange'/'ME2', '111').
card_multiverse_id('soul exchange'/'ME2', '184517').

card_in_set('soul kiss', 'ME2').
card_original_type('soul kiss'/'ME2', 'Enchantment — Aura').
card_original_text('soul kiss'/'ME2', 'Enchant creature\n{B}, Pay 1 life: Enchanted creature gets +2/+2 until end of turn. Play this ability no more than three times each turn.').
card_image_name('soul kiss'/'ME2', 'soul kiss').
card_uid('soul kiss'/'ME2', 'ME2:Soul Kiss:soul kiss').
card_rarity('soul kiss'/'ME2', 'Uncommon').
card_artist('soul kiss'/'ME2', 'Nicola Leonard').
card_number('soul kiss'/'ME2', '112').
card_multiverse_id('soul kiss'/'ME2', '184611').

card_in_set('spore cloud', 'ME2').
card_original_type('spore cloud'/'ME2', 'Instant').
card_original_text('spore cloud'/'ME2', 'Tap all blocking creatures. Prevent all combat damage that would be dealt this turn. Each attacking creature and each blocking creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('spore cloud'/'ME2', 'spore cloud').
card_uid('spore cloud'/'ME2', 'ME2:Spore Cloud:spore cloud').
card_rarity('spore cloud'/'ME2', 'Uncommon').
card_artist('spore cloud'/'ME2', 'Susan Van Camp').
card_number('spore cloud'/'ME2', '176').
card_multiverse_id('spore cloud'/'ME2', '184710').

card_in_set('spore flower', 'ME2').
card_original_type('spore flower'/'ME2', 'Creature — Fungus').
card_original_text('spore flower'/'ME2', 'At the beginning of your upkeep, put a spore counter on Spore Flower.\nRemove three spore counters from Spore Flower: Prevent all combat damage that would be dealt this turn.').
card_image_name('spore flower'/'ME2', 'spore flower').
card_uid('spore flower'/'ME2', 'ME2:Spore Flower:spore flower').
card_rarity('spore flower'/'ME2', 'Uncommon').
card_artist('spore flower'/'ME2', 'Margaret Organ-Kean').
card_number('spore flower'/'ME2', '177').
card_multiverse_id('spore flower'/'ME2', '184521').

card_in_set('stampede', 'ME2').
card_original_type('stampede'/'ME2', 'Instant').
card_original_text('stampede'/'ME2', 'Attacking creatures get +1/+0 and gain trample until end of turn.').
card_image_name('stampede'/'ME2', 'stampede').
card_uid('stampede'/'ME2', 'ME2:Stampede:stampede').
card_rarity('stampede'/'ME2', 'Uncommon').
card_artist('stampede'/'ME2', 'Jeff A. Menges').
card_number('stampede'/'ME2', '178').
card_flavor_text('stampede'/'ME2', '\"We could see the horizon blacken with the great beasts, but it was too late. The icefield offered no immediate safety, but luckily most of us reached a crevasse in which we could take cover.\"\n—Disa the Restless, journal entry').
card_multiverse_id('stampede'/'ME2', '184670').

card_in_set('stone spirit', 'ME2').
card_original_type('stone spirit'/'ME2', 'Creature — Elemental Spirit').
card_original_text('stone spirit'/'ME2', 'Stone Spirit can\'t be blocked by creatures with flying.').
card_image_name('stone spirit'/'ME2', 'stone spirit').
card_uid('stone spirit'/'ME2', 'ME2:Stone Spirit:stone spirit').
card_rarity('stone spirit'/'ME2', 'Uncommon').
card_artist('stone spirit'/'ME2', 'James Allen').
card_number('stone spirit'/'ME2', '150').
card_flavor_text('stone spirit'/'ME2', '\"The spirit of the stone is the spirit of strength.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('stone spirit'/'ME2', '184655').

card_in_set('stonehands', 'ME2').
card_original_type('stonehands'/'ME2', 'Enchantment — Aura').
card_original_text('stonehands'/'ME2', 'Enchant creature\nEnchanted creature gets +0/+2.\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('stonehands'/'ME2', 'stonehands').
card_uid('stonehands'/'ME2', 'ME2:Stonehands:stonehands').
card_rarity('stonehands'/'ME2', 'Common').
card_artist('stonehands'/'ME2', 'Dan Frazier').
card_number('stonehands'/'ME2', '151').
card_flavor_text('stonehands'/'ME2', '\"Trust in the power of stone. Stone is strong; stone shatters swords; stone breaks bones. Trust in stone.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('stonehands'/'ME2', '184694').

card_in_set('storm elemental', 'ME2').
card_original_type('storm elemental'/'ME2', 'Creature — Elemental').
card_original_text('storm elemental'/'ME2', 'Flying\n{U}, Remove the top card of your library from the game: Tap target creature with flying.\n{U}, Remove the top card of your library from the game: If the removed card is a snow land, Storm Elemental gets +1/+1 until end of turn.').
card_image_name('storm elemental'/'ME2', 'storm elemental').
card_uid('storm elemental'/'ME2', 'ME2:Storm Elemental:storm elemental').
card_rarity('storm elemental'/'ME2', 'Uncommon').
card_artist('storm elemental'/'ME2', 'John Matson').
card_number('storm elemental'/'ME2', '68').
card_multiverse_id('storm elemental'/'ME2', '184581').

card_in_set('storm spirit', 'ME2').
card_original_type('storm spirit'/'ME2', 'Creature — Elemental Spirit').
card_original_text('storm spirit'/'ME2', 'Flying\n{T}: Storm Spirit deals 2 damage to target creature.').
card_image_name('storm spirit'/'ME2', 'storm spirit').
card_uid('storm spirit'/'ME2', 'ME2:Storm Spirit:storm spirit').
card_rarity('storm spirit'/'ME2', 'Rare').
card_artist('storm spirit'/'ME2', 'Pete Venters').
card_number('storm spirit'/'ME2', '198').
card_flavor_text('storm spirit'/'ME2', '\"Come to us, with your lightning. Come to us, with your thunder. Serve us with your strength, and smite our foes with your power.\"\n—Steinar Icefist, Balduvian shaman').
card_multiverse_id('storm spirit'/'ME2', '184672').

card_in_set('stromgald cabal', 'ME2').
card_original_type('stromgald cabal'/'ME2', 'Creature — Human Knight').
card_original_text('stromgald cabal'/'ME2', '{T}, Pay 1 life: Counter target white spell.').
card_image_name('stromgald cabal'/'ME2', 'stromgald cabal').
card_uid('stromgald cabal'/'ME2', 'ME2:Stromgald Cabal:stromgald cabal').
card_rarity('stromgald cabal'/'ME2', 'Rare').
card_artist('stromgald cabal'/'ME2', 'Anson Maddocks').
card_number('stromgald cabal'/'ME2', '113').
card_flavor_text('stromgald cabal'/'ME2', '\"Kjeldor must be supreme at any cost.\"\n—Avram Garrisson, leader of the Knights of Stromgald').
card_multiverse_id('stromgald cabal'/'ME2', '184673').

card_in_set('stunted growth', 'ME2').
card_original_type('stunted growth'/'ME2', 'Sorcery').
card_original_text('stunted growth'/'ME2', 'Target player chooses three cards from his or her hand and puts them on top of his or her library in any order.').
card_image_name('stunted growth'/'ME2', 'stunted growth').
card_uid('stunted growth'/'ME2', 'ME2:Stunted Growth:stunted growth').
card_rarity('stunted growth'/'ME2', 'Rare').
card_artist('stunted growth'/'ME2', 'NéNé Thomas').
card_number('stunted growth'/'ME2', '179').
card_multiverse_id('stunted growth'/'ME2', '184674').

card_in_set('sustaining spirit', 'ME2').
card_original_type('sustaining spirit'/'ME2', 'Creature — Angel Spirit').
card_original_text('sustaining spirit'/'ME2', 'Cumulative upkeep {1}{W}\nDamage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_image_name('sustaining spirit'/'ME2', 'sustaining spirit').
card_uid('sustaining spirit'/'ME2', 'ME2:Sustaining Spirit:sustaining spirit').
card_rarity('sustaining spirit'/'ME2', 'Rare').
card_artist('sustaining spirit'/'ME2', 'Rebecca Guay').
card_number('sustaining spirit'/'ME2', '36').
card_flavor_text('sustaining spirit'/'ME2', '\"Faith is our greatest protector.\"\n—Halvor Arensson, Kjeldoran priest').
card_multiverse_id('sustaining spirit'/'ME2', '184744').

card_in_set('svyelunite temple', 'ME2').
card_original_type('svyelunite temple'/'ME2', 'Land').
card_original_text('svyelunite temple'/'ME2', 'Svyelunite Temple comes into play tapped.\n{T}: Add {U} to your mana pool.\n{T}, Sacrifice Svyelunite Temple: Add {U}{U} to your mana pool.').
card_image_name('svyelunite temple'/'ME2', 'svyelunite temple').
card_uid('svyelunite temple'/'ME2', 'ME2:Svyelunite Temple:svyelunite temple').
card_rarity('svyelunite temple'/'ME2', 'Uncommon').
card_artist('svyelunite temple'/'ME2', 'Mark Poole').
card_number('svyelunite temple'/'ME2', '237').
card_multiverse_id('svyelunite temple'/'ME2', '184522').

card_in_set('swords to plowshares', 'ME2').
card_original_type('swords to plowshares'/'ME2', 'Instant').
card_original_text('swords to plowshares'/'ME2', 'Remove target creature from the game. Its controller gains life equal to its power.').
card_image_name('swords to plowshares'/'ME2', 'swords to plowshares').
card_uid('swords to plowshares'/'ME2', 'ME2:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'ME2', 'Uncommon').
card_artist('swords to plowshares'/'ME2', 'Kaja Foglio').
card_number('swords to plowshares'/'ME2', '37').
card_flavor_text('swords to plowshares'/'ME2', '\"The so-called barbarians will not respect us for our military might—they will respect us for our honor.\"\n—Lucilde Fiksdotter, leader of the Order of the White Shield').
card_multiverse_id('swords to plowshares'/'ME2', '184675').

card_in_set('taiga', 'ME2').
card_original_type('taiga'/'ME2', 'Land — Mountain Forest').
card_original_text('taiga'/'ME2', '').
card_image_name('taiga'/'ME2', 'taiga').
card_uid('taiga'/'ME2', 'ME2:Taiga:taiga').
card_rarity('taiga'/'ME2', 'Rare').
card_artist('taiga'/'ME2', 'Rob Alexander').
card_number('taiga'/'ME2', '238').
card_multiverse_id('taiga'/'ME2', '184750').

card_in_set('temporal manipulation', 'ME2').
card_original_type('temporal manipulation'/'ME2', 'Sorcery').
card_original_text('temporal manipulation'/'ME2', 'Take an extra turn after this one.').
card_image_name('temporal manipulation'/'ME2', 'temporal manipulation').
card_uid('temporal manipulation'/'ME2', 'ME2:Temporal Manipulation:temporal manipulation').
card_rarity('temporal manipulation'/'ME2', 'Rare').
card_artist('temporal manipulation'/'ME2', 'Anson Maddocks').
card_number('temporal manipulation'/'ME2', '69').
card_flavor_text('temporal manipulation'/'ME2', 'Doing something at the last minute isn\'t so bad when you can make that minute last.').
card_multiverse_id('temporal manipulation'/'ME2', '184770').

card_in_set('thallid', 'ME2').
card_original_type('thallid'/'ME2', 'Creature — Fungus').
card_original_text('thallid'/'ME2', 'At the beginning of your upkeep, put a spore counter on Thallid.\nRemove three spore counters from Thallid: Put a 1/1 green Saproling creature token into play.').
card_image_name('thallid'/'ME2', 'thallid').
card_uid('thallid'/'ME2', 'ME2:Thallid:thallid').
card_rarity('thallid'/'ME2', 'Common').
card_artist('thallid'/'ME2', 'Jesper Myrfors').
card_number('thallid'/'ME2', '180').
card_multiverse_id('thallid'/'ME2', '184711').

card_in_set('thallid devourer', 'ME2').
card_original_type('thallid devourer'/'ME2', 'Creature — Fungus').
card_original_text('thallid devourer'/'ME2', 'At the beginning of your upkeep, put a spore counter on Thallid Devourer.\nRemove three spore counters from Thallid Devourer: Put a 1/1 green Saproling creature token into play.\nSacrifice a Saproling: Thallid Devourer gets +1/+2 until end of turn.').
card_image_name('thallid devourer'/'ME2', 'thallid devourer').
card_uid('thallid devourer'/'ME2', 'ME2:Thallid Devourer:thallid devourer').
card_rarity('thallid devourer'/'ME2', 'Common').
card_artist('thallid devourer'/'ME2', 'Ron Spencer').
card_number('thallid devourer'/'ME2', '181').
card_multiverse_id('thallid devourer'/'ME2', '184527').

card_in_set('thelonite druid', 'ME2').
card_original_type('thelonite druid'/'ME2', 'Creature — Human Cleric Druid').
card_original_text('thelonite druid'/'ME2', '{1}{G}, {T}, Sacrifice a creature: Forests you control become 2/3 creatures until end of turn. They\'re still lands.').
card_image_name('thelonite druid'/'ME2', 'thelonite druid').
card_uid('thelonite druid'/'ME2', 'ME2:Thelonite Druid:thelonite druid').
card_rarity('thelonite druid'/'ME2', 'Rare').
card_artist('thelonite druid'/'ME2', 'Margaret Organ-Kean').
card_number('thelonite druid'/'ME2', '182').
card_flavor_text('thelonite druid'/'ME2', '\"The magic at the heart of all living things can bear awe-inspiring fruit.\"\n—Kolevi of Havenwood, elder druid').
card_multiverse_id('thelonite druid'/'ME2', '184528').

card_in_set('thermokarst', 'ME2').
card_original_type('thermokarst'/'ME2', 'Sorcery').
card_original_text('thermokarst'/'ME2', 'Destroy target land. If that land was a snow land, you gain 1 life.').
card_image_name('thermokarst'/'ME2', 'thermokarst').
card_uid('thermokarst'/'ME2', 'ME2:Thermokarst:thermokarst').
card_rarity('thermokarst'/'ME2', 'Common').
card_artist('thermokarst'/'ME2', 'Ken Meyer, Jr.').
card_number('thermokarst'/'ME2', '183').
card_flavor_text('thermokarst'/'ME2', '\"Finally we understand the lesson of our home: loss begets renewal.\"\n—Kolbjörn, elder druid of the Juniper Order').
card_multiverse_id('thermokarst'/'ME2', '184676').

card_in_set('thought lash', 'ME2').
card_original_type('thought lash'/'ME2', 'Enchantment').
card_original_text('thought lash'/'ME2', 'Cumulative upkeep—Remove the top card of your library from the game.\nWhen Thought Lash\'s cumulative upkeep isn\'t paid, remove your library from the game.\nRemove the top card of your library from the game: Prevent the next 1 damage that would be dealt to you this turn.').
card_image_name('thought lash'/'ME2', 'thought lash').
card_uid('thought lash'/'ME2', 'ME2:Thought Lash:thought lash').
card_rarity('thought lash'/'ME2', 'Rare').
card_artist('thought lash'/'ME2', 'Mark Tedin').
card_number('thought lash'/'ME2', '70').
card_multiverse_id('thought lash'/'ME2', '184582').

card_in_set('thunder wall', 'ME2').
card_original_type('thunder wall'/'ME2', 'Creature — Wall').
card_original_text('thunder wall'/'ME2', 'Defender, flying\n{U}: Thunder Wall gets +1/+1 until end of turn.').
card_image_name('thunder wall'/'ME2', 'thunder wall').
card_uid('thunder wall'/'ME2', 'ME2:Thunder Wall:thunder wall').
card_rarity('thunder wall'/'ME2', 'Uncommon').
card_artist('thunder wall'/'ME2', 'Richard Thomas').
card_number('thunder wall'/'ME2', '71').
card_flavor_text('thunder wall'/'ME2', '\"The lemures had barely taken wing when the sky roared with thunder. The swarm of little beasts wavered, divided, and fell, crashing to the earth.\"\n—General Jarkeld, the Arctic Fox').
card_multiverse_id('thunder wall'/'ME2', '184739').

card_in_set('time bomb', 'ME2').
card_original_type('time bomb'/'ME2', 'Artifact').
card_original_text('time bomb'/'ME2', 'At the beginning of your upkeep, put a time counter on Time Bomb.\n{1}, {T}, Sacrifice Time Bomb: Time Bomb deals damage to each creature and each player equal to the number of time counters on Time Bomb.').
card_image_name('time bomb'/'ME2', 'time bomb').
card_uid('time bomb'/'ME2', 'ME2:Time Bomb:time bomb').
card_rarity('time bomb'/'ME2', 'Rare').
card_artist('time bomb'/'ME2', 'Amy Weber').
card_number('time bomb'/'ME2', '223').
card_multiverse_id('time bomb'/'ME2', '184677').

card_in_set('tinder wall', 'ME2').
card_original_type('tinder wall'/'ME2', 'Creature — Plant Wall').
card_original_text('tinder wall'/'ME2', 'Defender\nSacrifice Tinder Wall: Add {R}{R} to your mana pool.\n{R}, Sacrifice Tinder Wall: Tinder Wall deals 2 damage to target creature it\'s blocking.').
card_image_name('tinder wall'/'ME2', 'tinder wall').
card_uid('tinder wall'/'ME2', 'ME2:Tinder Wall:tinder wall').
card_rarity('tinder wall'/'ME2', 'Common').
card_artist('tinder wall'/'ME2', 'Rick Emond').
card_number('tinder wall'/'ME2', '184').
card_multiverse_id('tinder wall'/'ME2', '184678').

card_in_set('tundra', 'ME2').
card_original_type('tundra'/'ME2', 'Land — Plains Island').
card_original_text('tundra'/'ME2', '').
card_image_name('tundra'/'ME2', 'tundra').
card_uid('tundra'/'ME2', 'ME2:Tundra:tundra').
card_rarity('tundra'/'ME2', 'Rare').
card_artist('tundra'/'ME2', 'Jesper Myrfors').
card_number('tundra'/'ME2', '239').
card_multiverse_id('tundra'/'ME2', '184751').

card_in_set('underground sea', 'ME2').
card_original_type('underground sea'/'ME2', 'Land — Island Swamp').
card_original_text('underground sea'/'ME2', '').
card_image_name('underground sea'/'ME2', 'underground sea').
card_uid('underground sea'/'ME2', 'ME2:Underground Sea:underground sea').
card_rarity('underground sea'/'ME2', 'Rare').
card_artist('underground sea'/'ME2', 'Rob Alexander').
card_number('underground sea'/'ME2', '240').
card_multiverse_id('underground sea'/'ME2', '184752').

card_in_set('varchild\'s crusader', 'ME2').
card_original_type('varchild\'s crusader'/'ME2', 'Creature — Human Knight').
card_original_text('varchild\'s crusader'/'ME2', '{0}: Varchild\'s Crusader can\'t be blocked this turn except by Walls. Sacrifice Varchild\'s Crusader at end of turn.').
card_image_name('varchild\'s crusader'/'ME2', 'varchild\'s crusader').
card_uid('varchild\'s crusader'/'ME2', 'ME2:Varchild\'s Crusader:varchild\'s crusader').
card_rarity('varchild\'s crusader'/'ME2', 'Common').
card_artist('varchild\'s crusader'/'ME2', 'Mark Poole').
card_number('varchild\'s crusader'/'ME2', '152').
card_flavor_text('varchild\'s crusader'/'ME2', '\"Every patch of land must belong to Kjeldor, no matter what the cost!\"\n—General Varchild').
card_multiverse_id('varchild\'s crusader'/'ME2', '184720').

card_in_set('viscerid armor', 'ME2').
card_original_type('viscerid armor'/'ME2', 'Enchantment — Aura').
card_original_text('viscerid armor'/'ME2', 'Enchant creature\nEnchanted creature gets +1/+1. \n{1}{U}: Return Viscerid Armor to its owner\'s hand.').
card_image_name('viscerid armor'/'ME2', 'viscerid armor').
card_uid('viscerid armor'/'ME2', 'ME2:Viscerid Armor:viscerid armor').
card_rarity('viscerid armor'/'ME2', 'Common').
card_artist('viscerid armor'/'ME2', 'Heather Hudson').
card_number('viscerid armor'/'ME2', '72').
card_flavor_text('viscerid armor'/'ME2', 'A fallen viscerid\'s only tribute is to be worn by a comrade.').
card_multiverse_id('viscerid armor'/'ME2', '184706').

card_in_set('viscerid drone', 'ME2').
card_original_type('viscerid drone'/'ME2', 'Creature — Homarid Drone').
card_original_text('viscerid drone'/'ME2', '{T}, Sacrifice a creature and a Swamp: Destroy target nonartifact creature. It can\'t be regenerated.\n{T}, Sacrifice a creature and a snow Swamp: Destroy target creature. It can\'t be regenerated.').
card_image_name('viscerid drone'/'ME2', 'viscerid drone').
card_uid('viscerid drone'/'ME2', 'ME2:Viscerid Drone:viscerid drone').
card_rarity('viscerid drone'/'ME2', 'Uncommon').
card_artist('viscerid drone'/'ME2', 'Heather Hudson').
card_number('viscerid drone'/'ME2', '73').
card_flavor_text('viscerid drone'/'ME2', 'Not all of Terisiare\'s flooding was natural . . . .').
card_multiverse_id('viscerid drone'/'ME2', '184764').

card_in_set('wall of kelp', 'ME2').
card_original_type('wall of kelp'/'ME2', 'Creature — Plant Wall').
card_original_text('wall of kelp'/'ME2', 'Defender\n{U}{U}, {T}: Put a 0/1 blue Plant Wall creature token with defender named Kelp into play.').
card_image_name('wall of kelp'/'ME2', 'wall of kelp').
card_uid('wall of kelp'/'ME2', 'ME2:Wall of Kelp:wall of kelp').
card_rarity('wall of kelp'/'ME2', 'Common').
card_artist('wall of kelp'/'ME2', 'Alan Rabinowitz').
card_number('wall of kelp'/'ME2', '74').
card_flavor_text('wall of kelp'/'ME2', '\"Ya can eat it or ya can weave it, but ya can\'t fight in it.\"\n—Zeki, reef pirate').
card_multiverse_id('wall of kelp'/'ME2', '184598').

card_in_set('warning', 'ME2').
card_original_type('warning'/'ME2', 'Instant').
card_original_text('warning'/'ME2', 'Prevent all combat damage that would be dealt by target attacking creature this turn.').
card_image_name('warning'/'ME2', 'warning').
card_uid('warning'/'ME2', 'ME2:Warning:warning').
card_rarity('warning'/'ME2', 'Common').
card_artist('warning'/'ME2', 'Pat Morrissey').
card_number('warning'/'ME2', '38').
card_flavor_text('warning'/'ME2', '\"The folk of the Karplusan mountains are impossible to ambush.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('warning'/'ME2', '184719').

card_in_set('whirling catapult', 'ME2').
card_original_type('whirling catapult'/'ME2', 'Artifact').
card_original_text('whirling catapult'/'ME2', '{2}, Remove the top two cards of your library from the game: Whirling Catapult deals 1 damage to each creature with flying and each player.').
card_image_name('whirling catapult'/'ME2', 'whirling catapult').
card_uid('whirling catapult'/'ME2', 'ME2:Whirling Catapult:whirling catapult').
card_rarity('whirling catapult'/'ME2', 'Uncommon').
card_artist('whirling catapult'/'ME2', 'Dan Frazier').
card_number('whirling catapult'/'ME2', '224').
card_flavor_text('whirling catapult'/'ME2', '\"Direct confrontation never was to the orcs\' taste.\"\n—General Varchild').
card_multiverse_id('whirling catapult'/'ME2', '184583').

card_in_set('whiteout', 'ME2').
card_original_type('whiteout'/'ME2', 'Instant').
card_original_text('whiteout'/'ME2', 'All creatures lose flying until end of turn.\nSacrifice a snow land: Return Whiteout from your graveyard to your hand.').
card_image_name('whiteout'/'ME2', 'whiteout').
card_uid('whiteout'/'ME2', 'ME2:Whiteout:whiteout').
card_rarity('whiteout'/'ME2', 'Common').
card_artist('whiteout'/'ME2', 'NéNé Thomas').
card_number('whiteout'/'ME2', '185').
card_multiverse_id('whiteout'/'ME2', '184768').

card_in_set('wiitigo', 'ME2').
card_original_type('wiitigo'/'ME2', 'Creature — Yeti').
card_original_text('wiitigo'/'ME2', 'Wiitigo comes into play with six +1/+1 counters on it.\nAt the beginning of your upkeep, put a +1/+1 counter on Wiitigo if it has blocked or been blocked since your last upkeep. Otherwise, remove a +1/+1 counter from it.').
card_image_name('wiitigo'/'ME2', 'wiitigo').
card_uid('wiitigo'/'ME2', 'ME2:Wiitigo:wiitigo').
card_rarity('wiitigo'/'ME2', 'Rare').
card_artist('wiitigo'/'ME2', 'Melissa A. Benson').
card_number('wiitigo'/'ME2', '186').
card_multiverse_id('wiitigo'/'ME2', '184679').

card_in_set('wind spirit', 'ME2').
card_original_type('wind spirit'/'ME2', 'Creature — Elemental Spirit').
card_original_text('wind spirit'/'ME2', 'Flying\nWind Spirit can\'t be blocked except by two or more creatures.').
card_image_name('wind spirit'/'ME2', 'wind spirit').
card_uid('wind spirit'/'ME2', 'ME2:Wind Spirit:wind spirit').
card_rarity('wind spirit'/'ME2', 'Uncommon').
card_artist('wind spirit'/'ME2', 'Kaja Foglio').
card_number('wind spirit'/'ME2', '75').
card_flavor_text('wind spirit'/'ME2', '\"To visit the sky requires bravery, and thought, and little else. To master the sky requires the binding of its masters, and little else.\"\n—Arnjlot Olasson, sky mage').
card_multiverse_id('wind spirit'/'ME2', '184680').

card_in_set('wings of aesthir', 'ME2').
card_original_type('wings of aesthir'/'ME2', 'Enchantment — Aura').
card_original_text('wings of aesthir'/'ME2', 'Enchant creature\nEnchanted creature gets +1/+0 and has flying and first strike.').
card_image_name('wings of aesthir'/'ME2', 'wings of aesthir').
card_uid('wings of aesthir'/'ME2', 'ME2:Wings of Aesthir:wings of aesthir').
card_rarity('wings of aesthir'/'ME2', 'Uncommon').
card_artist('wings of aesthir'/'ME2', 'Edward P. Beard, Jr.').
card_number('wings of aesthir'/'ME2', '199').
card_flavor_text('wings of aesthir'/'ME2', '\"For those of courage, even the sky holds no limit.\"\n—Arnjlot Olasson, sky mage').
card_multiverse_id('wings of aesthir'/'ME2', '184628').

card_in_set('winter\'s night', 'ME2').
card_original_type('winter\'s night'/'ME2', 'World Enchantment').
card_original_text('winter\'s night'/'ME2', 'Whenever a player taps a snow land for mana, that player adds one mana to his or her mana pool of any type that land produced. That land doesn\'t untap during its controller\'s next untap step.').
card_image_name('winter\'s night'/'ME2', 'winter\'s night').
card_uid('winter\'s night'/'ME2', 'ME2:Winter\'s Night:winter\'s night').
card_rarity('winter\'s night'/'ME2', 'Rare').
card_artist('winter\'s night'/'ME2', 'Rob Alexander').
card_number('winter\'s night'/'ME2', '200').
card_multiverse_id('winter\'s night'/'ME2', '184584').

card_in_set('withering wisps', 'ME2').
card_original_type('withering wisps'/'ME2', 'Enchantment').
card_original_text('withering wisps'/'ME2', 'At end of turn, if no creatures are in play, sacrifice Withering Wisps.\n{B}: Withering Wisps deals 1 damage to each creature and each player. Play this ability no more times each turn than the number of snow Swamps you control.').
card_image_name('withering wisps'/'ME2', 'withering wisps').
card_uid('withering wisps'/'ME2', 'ME2:Withering Wisps:withering wisps').
card_rarity('withering wisps'/'ME2', 'Uncommon').
card_artist('withering wisps'/'ME2', 'NéNé Thomas').
card_number('withering wisps'/'ME2', '114').
card_multiverse_id('withering wisps'/'ME2', '184681').

card_in_set('wolf pack', 'ME2').
card_original_type('wolf pack'/'ME2', 'Creature — Wolf').
card_original_text('wolf pack'/'ME2', 'You may have Wolf Pack assign its combat damage as though it weren\'t blocked.').
card_image_name('wolf pack'/'ME2', 'wolf pack').
card_uid('wolf pack'/'ME2', 'ME2:Wolf Pack:wolf pack').
card_rarity('wolf pack'/'ME2', 'Rare').
card_artist('wolf pack'/'ME2', 'Yang Jun Kwon').
card_number('wolf pack'/'ME2', '187').
card_multiverse_id('wolf pack'/'ME2', '184778').

card_in_set('woolly mammoths', 'ME2').
card_original_type('woolly mammoths'/'ME2', 'Creature — Elephant').
card_original_text('woolly mammoths'/'ME2', 'Woolly Mammoths has trample as long as you control a snow land.').
card_image_name('woolly mammoths'/'ME2', 'woolly mammoths').
card_uid('woolly mammoths'/'ME2', 'ME2:Woolly Mammoths:woolly mammoths').
card_rarity('woolly mammoths'/'ME2', 'Common').
card_artist('woolly mammoths'/'ME2', 'Dan Frazier').
card_number('woolly mammoths'/'ME2', '188').
card_flavor_text('woolly mammoths'/'ME2', '\"Mammoths may be good to ride on, but they\'re certainly bad to fall off of!\"\n—Disa the Restless, journal entry').
card_multiverse_id('woolly mammoths'/'ME2', '184682').

card_in_set('woolly spider', 'ME2').
card_original_type('woolly spider'/'ME2', 'Creature — Spider').
card_original_text('woolly spider'/'ME2', 'Reach\nWhenever Woolly Spider blocks a creature with flying, Woolly Spider gets +0/+2 until end of turn.').
card_image_name('woolly spider'/'ME2', 'woolly spider').
card_uid('woolly spider'/'ME2', 'ME2:Woolly Spider:woolly spider').
card_rarity('woolly spider'/'ME2', 'Uncommon').
card_artist('woolly spider'/'ME2', 'Daniel Gelon').
card_number('woolly spider'/'ME2', '189').
card_flavor_text('woolly spider'/'ME2', '\"We need not fear the forces of the air; I\'ve yet to see a spider without an appetite.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('woolly spider'/'ME2', '184539').

card_in_set('yavimaya ancients', 'ME2').
card_original_type('yavimaya ancients'/'ME2', 'Creature — Treefolk').
card_original_text('yavimaya ancients'/'ME2', '{G}: Yavimaya Ancients gets +1/-2 until end of turn.').
card_image_name('yavimaya ancients'/'ME2', 'yavimaya ancients').
card_uid('yavimaya ancients'/'ME2', 'ME2:Yavimaya Ancients:yavimaya ancients').
card_rarity('yavimaya ancients'/'ME2', 'Uncommon').
card_artist('yavimaya ancients'/'ME2', 'Quinton Hoover').
card_number('yavimaya ancients'/'ME2', '190').
card_flavor_text('yavimaya ancients'/'ME2', '\"We orphans of Fyndhorn have found no welcome in this alien place.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('yavimaya ancients'/'ME2', '184765').

card_in_set('zuran spellcaster', 'ME2').
card_original_type('zuran spellcaster'/'ME2', 'Creature — Human Wizard').
card_original_text('zuran spellcaster'/'ME2', '{T}: Zuran Spellcaster deals 1 damage to target creature or player.').
card_image_name('zuran spellcaster'/'ME2', 'zuran spellcaster').
card_uid('zuran spellcaster'/'ME2', 'ME2:Zuran Spellcaster:zuran spellcaster').
card_rarity('zuran spellcaster'/'ME2', 'Common').
card_artist('zuran spellcaster'/'ME2', 'Edward P. Beard, Jr.').
card_number('zuran spellcaster'/'ME2', '76').
card_flavor_text('zuran spellcaster'/'ME2', '\"A mage must be precise as well as potent; cautious, as well as clever.\"\n—Zur the Enchanter').
card_multiverse_id('zuran spellcaster'/'ME2', '184683').
