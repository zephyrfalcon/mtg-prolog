% Journey into Nyx

set('JOU').
set_name('JOU', 'Journey into Nyx').
set_release_date('JOU', '2014-05-02').
set_border('JOU', 'black').
set_type('JOU', 'expansion').
set_block('JOU', 'Theros').

card_in_set('aegis of the gods', 'JOU').
card_original_type('aegis of the gods'/'JOU', 'Enchantment Creature — Human Soldier').
card_original_text('aegis of the gods'/'JOU', 'You have hexproof. (You can\'t be the target of spells or abilities your opponents control.)').
card_first_print('aegis of the gods', 'JOU').
card_image_name('aegis of the gods'/'JOU', 'aegis of the gods').
card_uid('aegis of the gods'/'JOU', 'JOU:Aegis of the Gods:aegis of the gods').
card_rarity('aegis of the gods'/'JOU', 'Rare').
card_artist('aegis of the gods'/'JOU', 'Yefim Kligerman').
card_number('aegis of the gods'/'JOU', '1').
card_flavor_text('aegis of the gods'/'JOU', 'Athreos cares little for the other gods\' conflict with mortals. He is concerned only with safe passage for the dead.').
card_multiverse_id('aegis of the gods'/'JOU', '380364').

card_in_set('aerial formation', 'JOU').
card_original_type('aerial formation'/'JOU', 'Instant').
card_original_text('aerial formation'/'JOU', 'Strive — Aerial Formation costs {2}{U} more to cast for each target beyond the first.\nAny number of target creatures each get +1/+1 and gain flying until end of turn.').
card_first_print('aerial formation', 'JOU').
card_image_name('aerial formation'/'JOU', 'aerial formation').
card_uid('aerial formation'/'JOU', 'JOU:Aerial Formation:aerial formation').
card_rarity('aerial formation'/'JOU', 'Common').
card_artist('aerial formation'/'JOU', 'Zack Stella').
card_number('aerial formation'/'JOU', '30').
card_multiverse_id('aerial formation'/'JOU', '380365').

card_in_set('agent of erebos', 'JOU').
card_original_type('agent of erebos'/'JOU', 'Enchantment Creature — Zombie').
card_original_text('agent of erebos'/'JOU', 'Constellation — Whenever Agent of Erebos or another enchantment enters the battlefield under your control, exile all cards from target player\'s graveyard.').
card_first_print('agent of erebos', 'JOU').
card_image_name('agent of erebos'/'JOU', 'agent of erebos').
card_uid('agent of erebos'/'JOU', 'JOU:Agent of Erebos:agent of erebos').
card_rarity('agent of erebos'/'JOU', 'Uncommon').
card_artist('agent of erebos'/'JOU', 'Cyril Van Der Haegen').
card_number('agent of erebos'/'JOU', '59').
card_flavor_text('agent of erebos'/'JOU', 'Erebos\'s minions hunt the Returned and warn those who consider the same folly.').
card_multiverse_id('agent of erebos'/'JOU', '380366').

card_in_set('ajani\'s presence', 'JOU').
card_original_type('ajani\'s presence'/'JOU', 'Instant').
card_original_text('ajani\'s presence'/'JOU', 'Strive — Ajani\'s Presence costs {2}{W} more to cast for each target beyond the first.\nAny number of target creatures each get +1/+1 and gain indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy them.)').
card_first_print('ajani\'s presence', 'JOU').
card_image_name('ajani\'s presence'/'JOU', 'ajani\'s presence').
card_uid('ajani\'s presence'/'JOU', 'JOU:Ajani\'s Presence:ajani\'s presence').
card_rarity('ajani\'s presence'/'JOU', 'Common').
card_artist('ajani\'s presence'/'JOU', 'Raymond Swanland').
card_number('ajani\'s presence'/'JOU', '2').
card_multiverse_id('ajani\'s presence'/'JOU', '380368').

card_in_set('ajani, mentor of heroes', 'JOU').
card_original_type('ajani, mentor of heroes'/'JOU', 'Planeswalker — Ajani').
card_original_text('ajani, mentor of heroes'/'JOU', '+1: Distribute three +1/+1 counters among one, two, or three target creatures you control.\n+1: Look at the top four cards of your library. You may reveal an Aura, creature, or planeswalker card from among them and put it into your hand. Put the rest on the bottom of your library in any order.\n-8: You gain 100 life.').
card_first_print('ajani, mentor of heroes', 'JOU').
card_image_name('ajani, mentor of heroes'/'JOU', 'ajani, mentor of heroes').
card_uid('ajani, mentor of heroes'/'JOU', 'JOU:Ajani, Mentor of Heroes:ajani, mentor of heroes').
card_rarity('ajani, mentor of heroes'/'JOU', 'Mythic Rare').
card_artist('ajani, mentor of heroes'/'JOU', 'Aaron Miller').
card_number('ajani, mentor of heroes'/'JOU', '145').
card_multiverse_id('ajani, mentor of heroes'/'JOU', '380367').

card_in_set('akroan line breaker', 'JOU').
card_original_type('akroan line breaker'/'JOU', 'Creature — Human Warrior').
card_original_text('akroan line breaker'/'JOU', 'Heroic — Whenever you cast a spell that targets Akroan Line Breaker, Akroan Line Breaker gets +2/+0 and gains intimidate until end of turn.').
card_first_print('akroan line breaker', 'JOU').
card_image_name('akroan line breaker'/'JOU', 'akroan line breaker').
card_uid('akroan line breaker'/'JOU', 'JOU:Akroan Line Breaker:akroan line breaker').
card_rarity('akroan line breaker'/'JOU', 'Uncommon').
card_artist('akroan line breaker'/'JOU', 'Marco Nelor').
card_number('akroan line breaker'/'JOU', '88').
card_flavor_text('akroan line breaker'/'JOU', 'The enemies\' shields are the first to shatter, and their battle line is never far behind.').
card_multiverse_id('akroan line breaker'/'JOU', '380369').

card_in_set('akroan mastiff', 'JOU').
card_original_type('akroan mastiff'/'JOU', 'Creature — Hound').
card_original_text('akroan mastiff'/'JOU', '{W}, {T}: Tap target creature.').
card_first_print('akroan mastiff', 'JOU').
card_image_name('akroan mastiff'/'JOU', 'akroan mastiff').
card_uid('akroan mastiff'/'JOU', 'JOU:Akroan Mastiff:akroan mastiff').
card_rarity('akroan mastiff'/'JOU', 'Common').
card_artist('akroan mastiff'/'JOU', 'Zoltan Boros').
card_number('akroan mastiff'/'JOU', '3').
card_flavor_text('akroan mastiff'/'JOU', 'Even when many of the soldiers who guarded Akros were called away, its safety was never in doubt.').
card_multiverse_id('akroan mastiff'/'JOU', '380370').

card_in_set('armament of nyx', 'JOU').
card_original_type('armament of nyx'/'JOU', 'Enchantment — Aura').
card_original_text('armament of nyx'/'JOU', 'Enchant creature\nEnchanted creature has double strike as long as it\'s an enchantment. Otherwise, prevent all damage that would be dealt by enchanted creature. (A creature with double strike deals both first-strike and regular combat damage.)').
card_first_print('armament of nyx', 'JOU').
card_image_name('armament of nyx'/'JOU', 'armament of nyx').
card_uid('armament of nyx'/'JOU', 'JOU:Armament of Nyx:armament of nyx').
card_rarity('armament of nyx'/'JOU', 'Common').
card_artist('armament of nyx'/'JOU', 'Slawomir Maniak').
card_number('armament of nyx'/'JOU', '4').
card_multiverse_id('armament of nyx'/'JOU', '380371').

card_in_set('armory of iroas', 'JOU').
card_original_type('armory of iroas'/'JOU', 'Artifact — Equipment').
card_original_text('armory of iroas'/'JOU', 'Whenever equipped creature attacks, put a +1/+1 counter on it.\nEquip {2}').
card_first_print('armory of iroas', 'JOU').
card_image_name('armory of iroas'/'JOU', 'armory of iroas').
card_uid('armory of iroas'/'JOU', 'JOU:Armory of Iroas:armory of iroas').
card_rarity('armory of iroas'/'JOU', 'Uncommon').
card_artist('armory of iroas'/'JOU', 'Yeong-Hao Han').
card_number('armory of iroas'/'JOU', '158').
card_flavor_text('armory of iroas'/'JOU', 'From a distance the weapons look pristine, but in fact they are tarnished and dented, each as unique as the champion who carried it to battle.').
card_multiverse_id('armory of iroas'/'JOU', '380372').

card_in_set('aspect of gorgon', 'JOU').
card_original_type('aspect of gorgon'/'JOU', 'Enchantment — Aura').
card_original_text('aspect of gorgon'/'JOU', 'Enchant creature\nEnchanted creature gets +1/+3 and has deathtouch. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('aspect of gorgon', 'JOU').
card_image_name('aspect of gorgon'/'JOU', 'aspect of gorgon').
card_uid('aspect of gorgon'/'JOU', 'JOU:Aspect of Gorgon:aspect of gorgon').
card_rarity('aspect of gorgon'/'JOU', 'Common').
card_artist('aspect of gorgon'/'JOU', 'Willian Murai').
card_number('aspect of gorgon'/'JOU', '60').
card_flavor_text('aspect of gorgon'/'JOU', '\"My adopted children are loved no less.\"\n—Pharika, god of affliction').
card_multiverse_id('aspect of gorgon'/'JOU', '380373').

card_in_set('athreos, god of passage', 'JOU').
card_original_type('athreos, god of passage'/'JOU', 'Legendary Enchantment Creature — God').
card_original_text('athreos, god of passage'/'JOU', 'Indestructible\nAs long as your devotion to white and black is less than seven, Athreos isn\'t a creature.\nWhenever another creature you own dies, return it to your hand unless target opponent pays 3 life.').
card_first_print('athreos, god of passage', 'JOU').
card_image_name('athreos, god of passage'/'JOU', 'athreos, god of passage').
card_uid('athreos, god of passage'/'JOU', 'JOU:Athreos, God of Passage:athreos, god of passage').
card_rarity('athreos, god of passage'/'JOU', 'Mythic Rare').
card_artist('athreos, god of passage'/'JOU', 'Ryan Barger').
card_number('athreos, god of passage'/'JOU', '146').
card_multiverse_id('athreos, god of passage'/'JOU', '380374').

card_in_set('banishing light', 'JOU').
card_original_type('banishing light'/'JOU', 'Enchantment').
card_original_text('banishing light'/'JOU', 'When Banishing Light enters the battlefield, exile target nonland permanent an opponent controls until Banishing Light leaves the battlefield. (That permanent returns under its owner\'s control.)').
card_image_name('banishing light'/'JOU', 'banishing light').
card_uid('banishing light'/'JOU', 'JOU:Banishing Light:banishing light').
card_rarity('banishing light'/'JOU', 'Uncommon').
card_artist('banishing light'/'JOU', 'Willian Murai').
card_number('banishing light'/'JOU', '5').
card_multiverse_id('banishing light'/'JOU', '380375').

card_in_set('bassara tower archer', 'JOU').
card_original_type('bassara tower archer'/'JOU', 'Creature — Human Archer').
card_original_text('bassara tower archer'/'JOU', 'Hexproof, reach').
card_first_print('bassara tower archer', 'JOU').
card_image_name('bassara tower archer'/'JOU', 'bassara tower archer').
card_uid('bassara tower archer'/'JOU', 'JOU:Bassara Tower Archer:bassara tower archer').
card_rarity('bassara tower archer'/'JOU', 'Uncommon').
card_artist('bassara tower archer'/'JOU', 'Slawomir Maniak').
card_number('bassara tower archer'/'JOU', '117').
card_flavor_text('bassara tower archer'/'JOU', 'Setessan warriors of Bassara Tower are known for their guerrilla tactics and skill with the bow. Interlopers into the Nessian Wood do not get far.').
card_multiverse_id('bassara tower archer'/'JOU', '380376').

card_in_set('battlefield thaumaturge', 'JOU').
card_original_type('battlefield thaumaturge'/'JOU', 'Creature — Human Wizard').
card_original_text('battlefield thaumaturge'/'JOU', 'Each instant and sorcery spell you cast costs {1} less to cast for each creature it targets.\nHeroic — Whenever you cast a spell that targets Battlefield Thaumaturge, Battlefield Thaumaturge gains hexproof until end of turn.').
card_first_print('battlefield thaumaturge', 'JOU').
card_image_name('battlefield thaumaturge'/'JOU', 'battlefield thaumaturge').
card_uid('battlefield thaumaturge'/'JOU', 'JOU:Battlefield Thaumaturge:battlefield thaumaturge').
card_rarity('battlefield thaumaturge'/'JOU', 'Rare').
card_artist('battlefield thaumaturge'/'JOU', 'Mike Sass').
card_number('battlefield thaumaturge'/'JOU', '31').
card_multiverse_id('battlefield thaumaturge'/'JOU', '380377').

card_in_set('bearer of the heavens', 'JOU').
card_original_type('bearer of the heavens'/'JOU', 'Creature — Giant').
card_original_text('bearer of the heavens'/'JOU', 'When Bearer of the Heavens dies, destroy all permanents at the beginning of the next end step.').
card_first_print('bearer of the heavens', 'JOU').
card_image_name('bearer of the heavens'/'JOU', 'bearer of the heavens').
card_uid('bearer of the heavens'/'JOU', 'JOU:Bearer of the Heavens:bearer of the heavens').
card_rarity('bearer of the heavens'/'JOU', 'Rare').
card_artist('bearer of the heavens'/'JOU', 'Ryan Alexander Lee').
card_number('bearer of the heavens'/'JOU', '89').
card_flavor_text('bearer of the heavens'/'JOU', 'To hold the heavens from the earth is no curse, but a titanic responsibility.').
card_multiverse_id('bearer of the heavens'/'JOU', '380378').

card_in_set('bladetusk boar', 'JOU').
card_original_type('bladetusk boar'/'JOU', 'Creature — Boar').
card_original_text('bladetusk boar'/'JOU', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_image_name('bladetusk boar'/'JOU', 'bladetusk boar').
card_uid('bladetusk boar'/'JOU', 'JOU:Bladetusk Boar:bladetusk boar').
card_rarity('bladetusk boar'/'JOU', 'Common').
card_artist('bladetusk boar'/'JOU', 'Sam Burley').
card_number('bladetusk boar'/'JOU', '90').
card_flavor_text('bladetusk boar'/'JOU', 'Deathbellow Canyon is home to minotaurs and many other creatures that share a love for the taste of human flesh.').
card_multiverse_id('bladetusk boar'/'JOU', '380379').

card_in_set('blinding flare', 'JOU').
card_original_type('blinding flare'/'JOU', 'Sorcery').
card_original_text('blinding flare'/'JOU', 'Strive — Blinding Flare costs {R} more to cast for each target beyond the first.\nAny number of target creatures can\'t block this turn.').
card_first_print('blinding flare', 'JOU').
card_image_name('blinding flare'/'JOU', 'blinding flare').
card_uid('blinding flare'/'JOU', 'JOU:Blinding Flare:blinding flare').
card_rarity('blinding flare'/'JOU', 'Uncommon').
card_artist('blinding flare'/'JOU', 'Evan Shipard').
card_number('blinding flare'/'JOU', '91').
card_multiverse_id('blinding flare'/'JOU', '380380').

card_in_set('bloodcrazed hoplite', 'JOU').
card_original_type('bloodcrazed hoplite'/'JOU', 'Creature — Human Soldier').
card_original_text('bloodcrazed hoplite'/'JOU', 'Heroic — Whenever you cast a spell that targets Bloodcrazed Hoplite, put a +1/+1 counter on it.\nWhenever a +1/+1 counter is placed on Bloodcrazed Hoplite, remove a +1/+1 counter from target creature an opponent controls.').
card_first_print('bloodcrazed hoplite', 'JOU').
card_image_name('bloodcrazed hoplite'/'JOU', 'bloodcrazed hoplite').
card_uid('bloodcrazed hoplite'/'JOU', 'JOU:Bloodcrazed Hoplite:bloodcrazed hoplite').
card_rarity('bloodcrazed hoplite'/'JOU', 'Common').
card_artist('bloodcrazed hoplite'/'JOU', 'Jeff Simpson').
card_number('bloodcrazed hoplite'/'JOU', '61').
card_multiverse_id('bloodcrazed hoplite'/'JOU', '380381').

card_in_set('brain maggot', 'JOU').
card_original_type('brain maggot'/'JOU', 'Enchantment Creature — Insect').
card_original_text('brain maggot'/'JOU', 'When Brain Maggot enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card until Brain Maggot leaves the battlefield.').
card_image_name('brain maggot'/'JOU', 'brain maggot').
card_uid('brain maggot'/'JOU', 'JOU:Brain Maggot:brain maggot').
card_rarity('brain maggot'/'JOU', 'Uncommon').
card_artist('brain maggot'/'JOU', 'Min Yum').
card_number('brain maggot'/'JOU', '62').
card_multiverse_id('brain maggot'/'JOU', '380382').

card_in_set('cast into darkness', 'JOU').
card_original_type('cast into darkness'/'JOU', 'Enchantment — Aura').
card_original_text('cast into darkness'/'JOU', 'Enchant creature\nEnchanted creature gets -2/-0 and can\'t block.').
card_first_print('cast into darkness', 'JOU').
card_image_name('cast into darkness'/'JOU', 'cast into darkness').
card_uid('cast into darkness'/'JOU', 'JOU:Cast into Darkness:cast into darkness').
card_rarity('cast into darkness'/'JOU', 'Common').
card_artist('cast into darkness'/'JOU', 'Clint Cearley').
card_number('cast into darkness'/'JOU', '63').
card_flavor_text('cast into darkness'/'JOU', 'In desperation the soldier prayed to Phenax, begging for the power to cheat the gorgon\'s gaze.').
card_multiverse_id('cast into darkness'/'JOU', '380383').

card_in_set('chariot of victory', 'JOU').
card_original_type('chariot of victory'/'JOU', 'Artifact — Equipment').
card_original_text('chariot of victory'/'JOU', 'Equipped creature has first strike, trample, and haste.\nEquip {1}').
card_first_print('chariot of victory', 'JOU').
card_image_name('chariot of victory'/'JOU', 'chariot of victory').
card_uid('chariot of victory'/'JOU', 'JOU:Chariot of Victory:chariot of victory').
card_rarity('chariot of victory'/'JOU', 'Uncommon').
card_artist('chariot of victory'/'JOU', 'John Stanko').
card_number('chariot of victory'/'JOU', '159').
card_flavor_text('chariot of victory'/'JOU', 'The greatest heroes are the first to stand, the first to take up arms, and the first to meet the enemy.').
card_multiverse_id('chariot of victory'/'JOU', '380384').

card_in_set('cloaked siren', 'JOU').
card_original_type('cloaked siren'/'JOU', 'Creature — Siren').
card_original_text('cloaked siren'/'JOU', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_first_print('cloaked siren', 'JOU').
card_image_name('cloaked siren'/'JOU', 'cloaked siren').
card_uid('cloaked siren'/'JOU', 'JOU:Cloaked Siren:cloaked siren').
card_rarity('cloaked siren'/'JOU', 'Common').
card_artist('cloaked siren'/'JOU', 'Mark Zug').
card_number('cloaked siren'/'JOU', '32').
card_flavor_text('cloaked siren'/'JOU', '\"That is not the voice of the wind singing on the stones.\"\n—Callaphe the mariner').
card_multiverse_id('cloaked siren'/'JOU', '380385').

card_in_set('colossal heroics', 'JOU').
card_original_type('colossal heroics'/'JOU', 'Instant').
card_original_text('colossal heroics'/'JOU', 'Strive — Colossal Heroics costs {1}{G} more to cast for each target beyond the first.\nAny number of target creatures each get +2/+2 until end of turn. Untap those creatures.').
card_first_print('colossal heroics', 'JOU').
card_image_name('colossal heroics'/'JOU', 'colossal heroics').
card_uid('colossal heroics'/'JOU', 'JOU:Colossal Heroics:colossal heroics').
card_rarity('colossal heroics'/'JOU', 'Uncommon').
card_artist('colossal heroics'/'JOU', 'Seb McKinnon').
card_number('colossal heroics'/'JOU', '118').
card_multiverse_id('colossal heroics'/'JOU', '380386').

card_in_set('consign to dust', 'JOU').
card_original_type('consign to dust'/'JOU', 'Instant').
card_original_text('consign to dust'/'JOU', 'Strive — Consign to Dust costs {2}{G} more to cast for each target beyond the first.\nDestroy any number of target artifacts and/or enchantments.').
card_first_print('consign to dust', 'JOU').
card_image_name('consign to dust'/'JOU', 'consign to dust').
card_uid('consign to dust'/'JOU', 'JOU:Consign to Dust:consign to dust').
card_rarity('consign to dust'/'JOU', 'Uncommon').
card_artist('consign to dust'/'JOU', 'Andreas Rocha').
card_number('consign to dust'/'JOU', '119').
card_multiverse_id('consign to dust'/'JOU', '380387').

card_in_set('countermand', 'JOU').
card_original_type('countermand'/'JOU', 'Instant').
card_original_text('countermand'/'JOU', 'Counter target spell. Its controller puts the top four cards of his or her library into his or her graveyard.').
card_first_print('countermand', 'JOU').
card_image_name('countermand'/'JOU', 'countermand').
card_uid('countermand'/'JOU', 'JOU:Countermand:countermand').
card_rarity('countermand'/'JOU', 'Common').
card_artist('countermand'/'JOU', 'Clint Cearley').
card_number('countermand'/'JOU', '33').
card_flavor_text('countermand'/'JOU', 'The physical pain was nothing compared to the agony in his mind.').
card_multiverse_id('countermand'/'JOU', '380388').

card_in_set('cruel feeding', 'JOU').
card_original_type('cruel feeding'/'JOU', 'Instant').
card_original_text('cruel feeding'/'JOU', 'Strive — Cruel Feeding costs {2}{B} more to cast for each target beyond the first.\nAny number of target creatures each get +1/+0 and gain lifelink until end of turn. (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_first_print('cruel feeding', 'JOU').
card_image_name('cruel feeding'/'JOU', 'cruel feeding').
card_uid('cruel feeding'/'JOU', 'JOU:Cruel Feeding:cruel feeding').
card_rarity('cruel feeding'/'JOU', 'Common').
card_artist('cruel feeding'/'JOU', 'Jason A. Engle').
card_number('cruel feeding'/'JOU', '64').
card_multiverse_id('cruel feeding'/'JOU', '380389').

card_in_set('crystalline nautilus', 'JOU').
card_original_type('crystalline nautilus'/'JOU', 'Enchantment Creature — Nautilus').
card_original_text('crystalline nautilus'/'JOU', 'Bestow {3}{U}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nWhen Crystalline Nautilus becomes the target of a spell or ability, sacrifice it.\nEnchanted creature gets +4/+4 and has \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_first_print('crystalline nautilus', 'JOU').
card_image_name('crystalline nautilus'/'JOU', 'crystalline nautilus').
card_uid('crystalline nautilus'/'JOU', 'JOU:Crystalline Nautilus:crystalline nautilus').
card_rarity('crystalline nautilus'/'JOU', 'Uncommon').
card_artist('crystalline nautilus'/'JOU', 'Brad Rigney').
card_number('crystalline nautilus'/'JOU', '34').
card_multiverse_id('crystalline nautilus'/'JOU', '380390').

card_in_set('cyclops of eternal fury', 'JOU').
card_original_type('cyclops of eternal fury'/'JOU', 'Enchantment Creature — Cyclops').
card_original_text('cyclops of eternal fury'/'JOU', 'Creatures you control have haste.').
card_first_print('cyclops of eternal fury', 'JOU').
card_image_name('cyclops of eternal fury'/'JOU', 'cyclops of eternal fury').
card_uid('cyclops of eternal fury'/'JOU', 'JOU:Cyclops of Eternal Fury:cyclops of eternal fury').
card_rarity('cyclops of eternal fury'/'JOU', 'Uncommon').
card_artist('cyclops of eternal fury'/'JOU', 'Matt Stewart').
card_number('cyclops of eternal fury'/'JOU', '92').
card_flavor_text('cyclops of eternal fury'/'JOU', '\"The anger that festers inside this one is contagious.\"\n—Anthousa of Setessa').
card_multiverse_id('cyclops of eternal fury'/'JOU', '380391').

card_in_set('dakra mystic', 'JOU').
card_original_type('dakra mystic'/'JOU', 'Creature — Merfolk Wizard').
card_original_text('dakra mystic'/'JOU', '{U}, {T}: Each player reveals the top card of his or her library. You may put the revealed cards into their owners\' graveyards. If you don\'t, each player draws a card.').
card_first_print('dakra mystic', 'JOU').
card_image_name('dakra mystic'/'JOU', 'dakra mystic').
card_uid('dakra mystic'/'JOU', 'JOU:Dakra Mystic:dakra mystic').
card_rarity('dakra mystic'/'JOU', 'Uncommon').
card_artist('dakra mystic'/'JOU', 'Wesley Burt').
card_number('dakra mystic'/'JOU', '35').
card_flavor_text('dakra mystic'/'JOU', 'Choices are rarely as simple as they seem.').
card_multiverse_id('dakra mystic'/'JOU', '380392').

card_in_set('daring thief', 'JOU').
card_original_type('daring thief'/'JOU', 'Creature — Human Rogue').
card_original_text('daring thief'/'JOU', 'Inspired — Whenever Daring Thief becomes untapped, you may exchange control of target nonland permanent you control and target permanent an opponent controls that shares a card type with it.').
card_first_print('daring thief', 'JOU').
card_image_name('daring thief'/'JOU', 'daring thief').
card_uid('daring thief'/'JOU', 'JOU:Daring Thief:daring thief').
card_rarity('daring thief'/'JOU', 'Rare').
card_artist('daring thief'/'JOU', 'Johann Bodin').
card_number('daring thief'/'JOU', '36').
card_flavor_text('daring thief'/'JOU', 'Honesty is the first casualty of war.').
card_multiverse_id('daring thief'/'JOU', '380393').

card_in_set('dawnbringer charioteers', 'JOU').
card_original_type('dawnbringer charioteers'/'JOU', 'Creature — Human Soldier').
card_original_text('dawnbringer charioteers'/'JOU', 'Flying, lifelink\nHeroic — Whenever you cast a spell that targets Dawnbringer Charioteers, put a +1/+1 counter on Dawnbringer Charioteers.').
card_image_name('dawnbringer charioteers'/'JOU', 'dawnbringer charioteers').
card_uid('dawnbringer charioteers'/'JOU', 'JOU:Dawnbringer Charioteers:dawnbringer charioteers').
card_rarity('dawnbringer charioteers'/'JOU', 'Rare').
card_artist('dawnbringer charioteers'/'JOU', 'Ryan Alexander Lee').
card_number('dawnbringer charioteers'/'JOU', '6').
card_flavor_text('dawnbringer charioteers'/'JOU', '\"Nyx may belong to the gods, but the skies of Theros are ours.\"').
card_multiverse_id('dawnbringer charioteers'/'JOU', '380394').

card_in_set('deicide', 'JOU').
card_original_type('deicide'/'JOU', 'Instant').
card_original_text('deicide'/'JOU', 'Exile target enchantment. If the exiled card is a God card, search its controller\'s graveyard, hand, and library for any number of cards with the same name as that card and exile them, then that player shuffles his or her library.').
card_first_print('deicide', 'JOU').
card_image_name('deicide'/'JOU', 'deicide').
card_uid('deicide'/'JOU', 'JOU:Deicide:deicide').
card_rarity('deicide'/'JOU', 'Rare').
card_artist('deicide'/'JOU', 'Jason Chan').
card_number('deicide'/'JOU', '7').
card_flavor_text('deicide'/'JOU', '\"It is done.\"\n—Elspeth').
card_multiverse_id('deicide'/'JOU', '380395').

card_in_set('desecration plague', 'JOU').
card_original_type('desecration plague'/'JOU', 'Sorcery').
card_original_text('desecration plague'/'JOU', 'Destroy target enchantment or land.').
card_first_print('desecration plague', 'JOU').
card_image_name('desecration plague'/'JOU', 'desecration plague').
card_uid('desecration plague'/'JOU', 'JOU:Desecration Plague:desecration plague').
card_rarity('desecration plague'/'JOU', 'Common').
card_artist('desecration plague'/'JOU', 'Ralph Horsley').
card_number('desecration plague'/'JOU', '120').
card_flavor_text('desecration plague'/'JOU', '\"The gods think of us as insects. Perhaps there is wisdom in that.\"\n—Perisophia the philosopher').
card_multiverse_id('desecration plague'/'JOU', '380396').

card_in_set('deserter\'s quarters', 'JOU').
card_original_type('deserter\'s quarters'/'JOU', 'Artifact').
card_original_text('deserter\'s quarters'/'JOU', 'You may choose not to untap Deserter\'s Quarters during your untap step.\n{6}, {T}: Tap target creature. It doesn\'t untap during its controller\'s untap step for as long as Deserter\'s Quarters remains tapped.').
card_first_print('deserter\'s quarters', 'JOU').
card_image_name('deserter\'s quarters'/'JOU', 'deserter\'s quarters').
card_uid('deserter\'s quarters'/'JOU', 'JOU:Deserter\'s Quarters:deserter\'s quarters').
card_rarity('deserter\'s quarters'/'JOU', 'Uncommon').
card_artist('deserter\'s quarters'/'JOU', 'Daniel Ljunggren').
card_number('deserter\'s quarters'/'JOU', '160').
card_flavor_text('deserter\'s quarters'/'JOU', 'In Akros, the penalty for running from battle is one night\'s stay in the Deserter\'s Quarters.').
card_multiverse_id('deserter\'s quarters'/'JOU', '380397').

card_in_set('desperate stand', 'JOU').
card_original_type('desperate stand'/'JOU', 'Sorcery').
card_original_text('desperate stand'/'JOU', 'Strive — Desperate Stand costs {R}{W} more to cast for each target beyond the first.\nAny number of target creatures each get +2/+0 and gain first strike and vigilance until end of turn.').
card_first_print('desperate stand', 'JOU').
card_image_name('desperate stand'/'JOU', 'desperate stand').
card_uid('desperate stand'/'JOU', 'JOU:Desperate Stand:desperate stand').
card_rarity('desperate stand'/'JOU', 'Uncommon').
card_artist('desperate stand'/'JOU', 'Raymond Swanland').
card_number('desperate stand'/'JOU', '147').
card_flavor_text('desperate stand'/'JOU', 'At the Akroan gates, hoplites revived the tactics of Kytheon Iora\'s infamous irregulars.').
card_multiverse_id('desperate stand'/'JOU', '380398').

card_in_set('dictate of erebos', 'JOU').
card_original_type('dictate of erebos'/'JOU', 'Enchantment').
card_original_text('dictate of erebos'/'JOU', 'Flash\nWhenever a creature you control dies, each opponent sacrifices a creature.').
card_first_print('dictate of erebos', 'JOU').
card_image_name('dictate of erebos'/'JOU', 'dictate of erebos').
card_uid('dictate of erebos'/'JOU', 'JOU:Dictate of Erebos:dictate of erebos').
card_rarity('dictate of erebos'/'JOU', 'Rare').
card_artist('dictate of erebos'/'JOU', 'Michael C. Hayes').
card_number('dictate of erebos'/'JOU', '65').
card_flavor_text('dictate of erebos'/'JOU', '\"I take no pleasure in your suffering, but it is necessary. Once you accept your fate you\'ll find eternity more tolerable.\"').
card_multiverse_id('dictate of erebos'/'JOU', '380399').

card_in_set('dictate of heliod', 'JOU').
card_original_type('dictate of heliod'/'JOU', 'Enchantment').
card_original_text('dictate of heliod'/'JOU', 'Flash\nCreatures you control get +2/+2.').
card_first_print('dictate of heliod', 'JOU').
card_image_name('dictate of heliod'/'JOU', 'dictate of heliod').
card_uid('dictate of heliod'/'JOU', 'JOU:Dictate of Heliod:dictate of heliod').
card_rarity('dictate of heliod'/'JOU', 'Rare').
card_artist('dictate of heliod'/'JOU', 'Terese Nielsen').
card_number('dictate of heliod'/'JOU', '8').
card_flavor_text('dictate of heliod'/'JOU', '\"In our war Heliod gave mortals some favor, yet other times he withheld aid. Are we still no more than game pieces to him?\"\n—Polyxene the Doubter').
card_multiverse_id('dictate of heliod'/'JOU', '380400').

card_in_set('dictate of karametra', 'JOU').
card_original_type('dictate of karametra'/'JOU', 'Enchantment').
card_original_text('dictate of karametra'/'JOU', 'Flash\nWhenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_first_print('dictate of karametra', 'JOU').
card_image_name('dictate of karametra'/'JOU', 'dictate of karametra').
card_uid('dictate of karametra'/'JOU', 'JOU:Dictate of Karametra:dictate of karametra').
card_rarity('dictate of karametra'/'JOU', 'Rare').
card_artist('dictate of karametra'/'JOU', 'Noah Bradley').
card_number('dictate of karametra'/'JOU', '121').
card_flavor_text('dictate of karametra'/'JOU', '\"I refuse to let the folly of mortals endanger the home I made for them.\"').
card_multiverse_id('dictate of karametra'/'JOU', '380401').

card_in_set('dictate of kruphix', 'JOU').
card_original_type('dictate of kruphix'/'JOU', 'Enchantment').
card_original_text('dictate of kruphix'/'JOU', 'Flash\nAt the beginning of each player\'s draw step, that player draws an additional card.').
card_image_name('dictate of kruphix'/'JOU', 'dictate of kruphix').
card_uid('dictate of kruphix'/'JOU', 'JOU:Dictate of Kruphix:dictate of kruphix').
card_rarity('dictate of kruphix'/'JOU', 'Rare').
card_artist('dictate of kruphix'/'JOU', 'Daarken').
card_number('dictate of kruphix'/'JOU', '37').
card_flavor_text('dictate of kruphix'/'JOU', '\"Knowledge is cruel. It will break your heart and test your allegiances. Are you certain you want this curse?\"').
card_multiverse_id('dictate of kruphix'/'JOU', '380402').

card_in_set('dictate of the twin gods', 'JOU').
card_original_type('dictate of the twin gods'/'JOU', 'Enchantment').
card_original_text('dictate of the twin gods'/'JOU', 'Flash\nIf a source would deal damage to a permanent or player, it deals double that damage to that permanent or player instead.').
card_image_name('dictate of the twin gods'/'JOU', 'dictate of the twin gods').
card_uid('dictate of the twin gods'/'JOU', 'JOU:Dictate of the Twin Gods:dictate of the twin gods').
card_rarity('dictate of the twin gods'/'JOU', 'Rare').
card_artist('dictate of the twin gods'/'JOU', 'Jaime Jones').
card_number('dictate of the twin gods'/'JOU', '93').
card_flavor_text('dictate of the twin gods'/'JOU', 'Iroas and Mogis are as different in appearance as they are in personality.').
card_multiverse_id('dictate of the twin gods'/'JOU', '380403').

card_in_set('disciple of deceit', 'JOU').
card_original_type('disciple of deceit'/'JOU', 'Creature — Human Rogue').
card_original_text('disciple of deceit'/'JOU', 'Inspired — Whenever Disciple of Deceit becomes untapped, you may discard a nonland card. If you do, search your library for a card with the same converted mana cost as that card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('disciple of deceit', 'JOU').
card_image_name('disciple of deceit'/'JOU', 'disciple of deceit').
card_uid('disciple of deceit'/'JOU', 'JOU:Disciple of Deceit:disciple of deceit').
card_rarity('disciple of deceit'/'JOU', 'Uncommon').
card_artist('disciple of deceit'/'JOU', 'Daarken').
card_number('disciple of deceit'/'JOU', '148').
card_multiverse_id('disciple of deceit'/'JOU', '380404').

card_in_set('doomwake giant', 'JOU').
card_original_type('doomwake giant'/'JOU', 'Enchantment Creature — Giant').
card_original_text('doomwake giant'/'JOU', 'Constellation — Whenever Doomwake Giant or another enchantment enters the battlefield under your control, creatures your opponents control get -1/-1 until end of turn.').
card_image_name('doomwake giant'/'JOU', 'doomwake giant').
card_uid('doomwake giant'/'JOU', 'JOU:Doomwake Giant:doomwake giant').
card_rarity('doomwake giant'/'JOU', 'Rare').
card_artist('doomwake giant'/'JOU', 'Kev Walker').
card_number('doomwake giant'/'JOU', '66').
card_flavor_text('doomwake giant'/'JOU', 'Crippling fear precedes it. A foul stench accompanies it. Death follows in its wake.').
card_multiverse_id('doomwake giant'/'JOU', '380405').

card_in_set('dreadbringer lampads', 'JOU').
card_original_type('dreadbringer lampads'/'JOU', 'Enchantment Creature — Nymph').
card_original_text('dreadbringer lampads'/'JOU', 'Constellation — Whenever Dreadbringer Lampads or another enchantment enters the battlefield under your control, target creature gains intimidate until end of turn. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('dreadbringer lampads', 'JOU').
card_image_name('dreadbringer lampads'/'JOU', 'dreadbringer lampads').
card_uid('dreadbringer lampads'/'JOU', 'JOU:Dreadbringer Lampads:dreadbringer lampads').
card_rarity('dreadbringer lampads'/'JOU', 'Common').
card_artist('dreadbringer lampads'/'JOU', 'Willian Murai').
card_number('dreadbringer lampads'/'JOU', '67').
card_multiverse_id('dreadbringer lampads'/'JOU', '380406').

card_in_set('eagle of the watch', 'JOU').
card_original_type('eagle of the watch'/'JOU', 'Creature — Bird').
card_original_text('eagle of the watch'/'JOU', 'Flying, vigilance').
card_first_print('eagle of the watch', 'JOU').
card_image_name('eagle of the watch'/'JOU', 'eagle of the watch').
card_uid('eagle of the watch'/'JOU', 'JOU:Eagle of the Watch:eagle of the watch').
card_rarity('eagle of the watch'/'JOU', 'Common').
card_artist('eagle of the watch'/'JOU', 'Scott Murphy').
card_number('eagle of the watch'/'JOU', '9').
card_flavor_text('eagle of the watch'/'JOU', '\"Even from miles away, I could see our eagles circling. That\'s when I gave the command to pick up the pace. I knew we were needed at home.\"\n—Kanlos, Akroan captain').
card_multiverse_id('eagle of the watch'/'JOU', '380407').

card_in_set('eidolon of blossoms', 'JOU').
card_original_type('eidolon of blossoms'/'JOU', 'Enchantment Creature — Spirit').
card_original_text('eidolon of blossoms'/'JOU', 'Constellation — Whenever Eidolon of Blossoms or another enchantment enters the battlefield under your control, draw a card.').
card_image_name('eidolon of blossoms'/'JOU', 'eidolon of blossoms').
card_uid('eidolon of blossoms'/'JOU', 'JOU:Eidolon of Blossoms:eidolon of blossoms').
card_rarity('eidolon of blossoms'/'JOU', 'Rare').
card_artist('eidolon of blossoms'/'JOU', 'Min Yum').
card_number('eidolon of blossoms'/'JOU', '122').
card_flavor_text('eidolon of blossoms'/'JOU', 'The emotional echoes of dryad gatherings attract lost souls.').
card_multiverse_id('eidolon of blossoms'/'JOU', '380408').

card_in_set('eidolon of rhetoric', 'JOU').
card_original_type('eidolon of rhetoric'/'JOU', 'Enchantment Creature — Spirit').
card_original_text('eidolon of rhetoric'/'JOU', 'Each player can\'t cast more than one spell each turn.').
card_first_print('eidolon of rhetoric', 'JOU').
card_image_name('eidolon of rhetoric'/'JOU', 'eidolon of rhetoric').
card_uid('eidolon of rhetoric'/'JOU', 'JOU:Eidolon of Rhetoric:eidolon of rhetoric').
card_rarity('eidolon of rhetoric'/'JOU', 'Uncommon').
card_artist('eidolon of rhetoric'/'JOU', 'Ryan Yee').
card_number('eidolon of rhetoric'/'JOU', '10').
card_flavor_text('eidolon of rhetoric'/'JOU', 'It is the soul of a philosopher who died of starvation contemplating the universe.').
card_multiverse_id('eidolon of rhetoric'/'JOU', '380409').

card_in_set('eidolon of the great revel', 'JOU').
card_original_type('eidolon of the great revel'/'JOU', 'Enchantment Creature — Spirit').
card_original_text('eidolon of the great revel'/'JOU', 'Whenever a player casts a spell with converted mana cost 3 or less, Eidolon of the Great Revel deals 2 damage to that player.').
card_first_print('eidolon of the great revel', 'JOU').
card_image_name('eidolon of the great revel'/'JOU', 'eidolon of the great revel').
card_uid('eidolon of the great revel'/'JOU', 'JOU:Eidolon of the Great Revel:eidolon of the great revel').
card_rarity('eidolon of the great revel'/'JOU', 'Rare').
card_artist('eidolon of the great revel'/'JOU', 'Cyril Van Der Haegen').
card_number('eidolon of the great revel'/'JOU', '94').
card_flavor_text('eidolon of the great revel'/'JOU', 'Xenagos reveled while Theros burned.').
card_multiverse_id('eidolon of the great revel'/'JOU', '380410').

card_in_set('extinguish all hope', 'JOU').
card_original_type('extinguish all hope'/'JOU', 'Sorcery').
card_original_text('extinguish all hope'/'JOU', 'Destroy all nonenchantment creatures.').
card_first_print('extinguish all hope', 'JOU').
card_image_name('extinguish all hope'/'JOU', 'extinguish all hope').
card_uid('extinguish all hope'/'JOU', 'JOU:Extinguish All Hope:extinguish all hope').
card_rarity('extinguish all hope'/'JOU', 'Rare').
card_artist('extinguish all hope'/'JOU', 'Chase Stone').
card_number('extinguish all hope'/'JOU', '68').
card_flavor_text('extinguish all hope'/'JOU', 'Phenax offers the hope of life, while Pharika grants the solace of death. Between them stands Erebos, who promises nothing but eternity.').
card_multiverse_id('extinguish all hope'/'JOU', '380411').

card_in_set('feast of dreams', 'JOU').
card_original_type('feast of dreams'/'JOU', 'Instant').
card_original_text('feast of dreams'/'JOU', 'Destroy target enchanted creature or enchantment creature.').
card_first_print('feast of dreams', 'JOU').
card_image_name('feast of dreams'/'JOU', 'feast of dreams').
card_uid('feast of dreams'/'JOU', 'JOU:Feast of Dreams:feast of dreams').
card_rarity('feast of dreams'/'JOU', 'Common').
card_artist('feast of dreams'/'JOU', 'Clint Cearley').
card_number('feast of dreams'/'JOU', '69').
card_flavor_text('feast of dreams'/'JOU', '\"We gave the gods the power to war against us. It is time we took that power back.\"').
card_multiverse_id('feast of dreams'/'JOU', '380412').

card_in_set('felhide petrifier', 'JOU').
card_original_type('felhide petrifier'/'JOU', 'Creature — Minotaur Warrior').
card_original_text('felhide petrifier'/'JOU', 'Deathtouch\nOther Minotaur creatures you control have deathtouch.').
card_first_print('felhide petrifier', 'JOU').
card_image_name('felhide petrifier'/'JOU', 'felhide petrifier').
card_uid('felhide petrifier'/'JOU', 'JOU:Felhide Petrifier:felhide petrifier').
card_rarity('felhide petrifier'/'JOU', 'Uncommon').
card_artist('felhide petrifier'/'JOU', 'James Ryman').
card_number('felhide petrifier'/'JOU', '70').
card_flavor_text('felhide petrifier'/'JOU', 'It\'s common practice among minotaurs to collect the heads of their victims as trophies. Sometimes the trophies prove more than ornamental.').
card_multiverse_id('felhide petrifier'/'JOU', '380413').

card_in_set('flamespeaker\'s will', 'JOU').
card_original_type('flamespeaker\'s will'/'JOU', 'Enchantment — Aura').
card_original_text('flamespeaker\'s will'/'JOU', 'Enchant creature you control\nEnchanted creature gets +1/+1.\nWhenever enchanted creature deals combat damage to a player, you may sacrifice Flamespeaker\'s Will. If you do, destroy target artifact.').
card_first_print('flamespeaker\'s will', 'JOU').
card_image_name('flamespeaker\'s will'/'JOU', 'flamespeaker\'s will').
card_uid('flamespeaker\'s will'/'JOU', 'JOU:Flamespeaker\'s Will:flamespeaker\'s will').
card_rarity('flamespeaker\'s will'/'JOU', 'Common').
card_artist('flamespeaker\'s will'/'JOU', 'Daniel Ljunggren').
card_number('flamespeaker\'s will'/'JOU', '95').
card_multiverse_id('flamespeaker\'s will'/'JOU', '380414').

card_in_set('fleetfeather cockatrice', 'JOU').
card_original_type('fleetfeather cockatrice'/'JOU', 'Creature — Cockatrice').
card_original_text('fleetfeather cockatrice'/'JOU', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying, deathtouch\n{5}{G}{U}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_first_print('fleetfeather cockatrice', 'JOU').
card_image_name('fleetfeather cockatrice'/'JOU', 'fleetfeather cockatrice').
card_uid('fleetfeather cockatrice'/'JOU', 'JOU:Fleetfeather Cockatrice:fleetfeather cockatrice').
card_rarity('fleetfeather cockatrice'/'JOU', 'Uncommon').
card_artist('fleetfeather cockatrice'/'JOU', 'Matt Stewart').
card_number('fleetfeather cockatrice'/'JOU', '149').
card_multiverse_id('fleetfeather cockatrice'/'JOU', '380415').

card_in_set('flurry of horns', 'JOU').
card_original_type('flurry of horns'/'JOU', 'Sorcery').
card_original_text('flurry of horns'/'JOU', 'Put two 2/3 red Minotaur creature tokens with haste onto the battlefield.').
card_first_print('flurry of horns', 'JOU').
card_image_name('flurry of horns'/'JOU', 'flurry of horns').
card_uid('flurry of horns'/'JOU', 'JOU:Flurry of Horns:flurry of horns').
card_rarity('flurry of horns'/'JOU', 'Common').
card_artist('flurry of horns'/'JOU', 'Phill Simmer').
card_number('flurry of horns'/'JOU', '96').
card_flavor_text('flurry of horns'/'JOU', 'A minotaur does not distinguish between human, satyr, and triton. They are all meat.').
card_multiverse_id('flurry of horns'/'JOU', '380416').

card_in_set('font of fertility', 'JOU').
card_original_type('font of fertility'/'JOU', 'Enchantment').
card_original_text('font of fertility'/'JOU', '{1}{G}, Sacrifice Font of Fertility: Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('font of fertility', 'JOU').
card_image_name('font of fertility'/'JOU', 'font of fertility').
card_uid('font of fertility'/'JOU', 'JOU:Font of Fertility:font of fertility').
card_rarity('font of fertility'/'JOU', 'Common').
card_artist('font of fertility'/'JOU', 'Jack Wang').
card_number('font of fertility'/'JOU', '123').
card_flavor_text('font of fertility'/'JOU', 'Drink deep, and know the path to the heart of the wilds.').
card_multiverse_id('font of fertility'/'JOU', '380417').

card_in_set('font of fortunes', 'JOU').
card_original_type('font of fortunes'/'JOU', 'Enchantment').
card_original_text('font of fortunes'/'JOU', '{1}{U}, Sacrifice Font of Fortunes: Draw two cards.').
card_first_print('font of fortunes', 'JOU').
card_image_name('font of fortunes'/'JOU', 'font of fortunes').
card_uid('font of fortunes'/'JOU', 'JOU:Font of Fortunes:font of fortunes').
card_rarity('font of fortunes'/'JOU', 'Common').
card_artist('font of fortunes'/'JOU', 'Jonas De Ro').
card_number('font of fortunes'/'JOU', '38').
card_flavor_text('font of fortunes'/'JOU', 'Drink deep, and your thoughts will flow freely.').
card_multiverse_id('font of fortunes'/'JOU', '380418').

card_in_set('font of ire', 'JOU').
card_original_type('font of ire'/'JOU', 'Enchantment').
card_original_text('font of ire'/'JOU', '{3}{R}, Sacrifice Font of Ire: Font of Ire deals 5 damage to target player.').
card_first_print('font of ire', 'JOU').
card_image_name('font of ire'/'JOU', 'font of ire').
card_uid('font of ire'/'JOU', 'JOU:Font of Ire:font of ire').
card_rarity('font of ire'/'JOU', 'Common').
card_artist('font of ire'/'JOU', 'Zack Stella').
card_number('font of ire'/'JOU', '97').
card_flavor_text('font of ire'/'JOU', 'Drink deep, and find your vengeance fulfilled.').
card_multiverse_id('font of ire'/'JOU', '380419').

card_in_set('font of return', 'JOU').
card_original_type('font of return'/'JOU', 'Enchantment').
card_original_text('font of return'/'JOU', '{3}{B}, Sacrifice Font of Return: Return up to three target creature cards from your graveyard to your hand.').
card_first_print('font of return', 'JOU').
card_image_name('font of return'/'JOU', 'font of return').
card_uid('font of return'/'JOU', 'JOU:Font of Return:font of return').
card_rarity('font of return'/'JOU', 'Common').
card_artist('font of return'/'JOU', 'Daarken').
card_number('font of return'/'JOU', '71').
card_flavor_text('font of return'/'JOU', 'Drink deep, and death will recoil from you.').
card_multiverse_id('font of return'/'JOU', '380420').

card_in_set('font of vigor', 'JOU').
card_original_type('font of vigor'/'JOU', 'Enchantment').
card_original_text('font of vigor'/'JOU', '{2}{W}, Sacrifice Font of Vigor: You gain 7 life.').
card_first_print('font of vigor', 'JOU').
card_image_name('font of vigor'/'JOU', 'font of vigor').
card_uid('font of vigor'/'JOU', 'JOU:Font of Vigor:font of vigor').
card_rarity('font of vigor'/'JOU', 'Common').
card_artist('font of vigor'/'JOU', 'Noah Bradley').
card_number('font of vigor'/'JOU', '11').
card_flavor_text('font of vigor'/'JOU', 'Drink deep, and be restored.').
card_multiverse_id('font of vigor'/'JOU', '380421').

card_in_set('forgeborn oreads', 'JOU').
card_original_type('forgeborn oreads'/'JOU', 'Enchantment Creature — Nymph').
card_original_text('forgeborn oreads'/'JOU', 'Constellation — Whenever Forgeborn Oreads or another enchantment enters the battlefield under your control, Forgeborn Oreads deals 1 damage to target creature or player.').
card_first_print('forgeborn oreads', 'JOU').
card_image_name('forgeborn oreads'/'JOU', 'forgeborn oreads').
card_uid('forgeborn oreads'/'JOU', 'JOU:Forgeborn Oreads:forgeborn oreads').
card_rarity('forgeborn oreads'/'JOU', 'Uncommon').
card_artist('forgeborn oreads'/'JOU', 'Ryan Yee').
card_number('forgeborn oreads'/'JOU', '98').
card_flavor_text('forgeborn oreads'/'JOU', 'Purphoros shaped the oreads out of stray coals from his forge.').
card_multiverse_id('forgeborn oreads'/'JOU', '380422').

card_in_set('gluttonous cyclops', 'JOU').
card_original_type('gluttonous cyclops'/'JOU', 'Creature — Cyclops').
card_original_text('gluttonous cyclops'/'JOU', '{5}{R}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_first_print('gluttonous cyclops', 'JOU').
card_image_name('gluttonous cyclops'/'JOU', 'gluttonous cyclops').
card_uid('gluttonous cyclops'/'JOU', 'JOU:Gluttonous Cyclops:gluttonous cyclops').
card_rarity('gluttonous cyclops'/'JOU', 'Common').
card_artist('gluttonous cyclops'/'JOU', 'Steve Prescott').
card_number('gluttonous cyclops'/'JOU', '99').
card_flavor_text('gluttonous cyclops'/'JOU', 'The cyclops had learned to never eat a shepherd. Instead he gently flung the \"pit\" aside to grow a new flock.').
card_multiverse_id('gluttonous cyclops'/'JOU', '380423').

card_in_set('gnarled scarhide', 'JOU').
card_original_type('gnarled scarhide'/'JOU', 'Enchantment Creature — Minotaur').
card_original_text('gnarled scarhide'/'JOU', 'Bestow {3}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nGnarled Scarhide can\'t block.\nEnchanted creature gets +2/+1 and can\'t block.').
card_first_print('gnarled scarhide', 'JOU').
card_image_name('gnarled scarhide'/'JOU', 'gnarled scarhide').
card_uid('gnarled scarhide'/'JOU', 'JOU:Gnarled Scarhide:gnarled scarhide').
card_rarity('gnarled scarhide'/'JOU', 'Uncommon').
card_artist('gnarled scarhide'/'JOU', 'Greg Staples').
card_number('gnarled scarhide'/'JOU', '72').
card_multiverse_id('gnarled scarhide'/'JOU', '380424').

card_in_set('godhunter octopus', 'JOU').
card_original_type('godhunter octopus'/'JOU', 'Creature — Octopus').
card_original_text('godhunter octopus'/'JOU', 'Godhunter Octopus can\'t attack unless defending player controls an enchantment or an enchanted permanent.').
card_first_print('godhunter octopus', 'JOU').
card_image_name('godhunter octopus'/'JOU', 'godhunter octopus').
card_uid('godhunter octopus'/'JOU', 'JOU:Godhunter Octopus:godhunter octopus').
card_rarity('godhunter octopus'/'JOU', 'Common').
card_artist('godhunter octopus'/'JOU', 'Tyler Jacobson').
card_number('godhunter octopus'/'JOU', '39').
card_flavor_text('godhunter octopus'/'JOU', '\"I will match Thassa drop for drop and show a god what true power is.\"\n—Kiora').
card_multiverse_id('godhunter octopus'/'JOU', '380425').

card_in_set('godsend', 'JOU').
card_original_type('godsend'/'JOU', 'Legendary Artifact — Equipment').
card_original_text('godsend'/'JOU', 'Equipped creature gets +3/+3.\nWhenever equipped creature blocks or becomes blocked by one or more creatures, you may exile one of those creatures.\nOpponents can\'t cast cards with the same name as cards exiled with Godsend.\nEquip {3}').
card_first_print('godsend', 'JOU').
card_image_name('godsend'/'JOU', 'godsend').
card_uid('godsend'/'JOU', 'JOU:Godsend:godsend').
card_rarity('godsend'/'JOU', 'Mythic Rare').
card_artist('godsend'/'JOU', 'Daniel Ljunggren').
card_number('godsend'/'JOU', '12').
card_multiverse_id('godsend'/'JOU', '380426').

card_in_set('gold-forged sentinel', 'JOU').
card_original_type('gold-forged sentinel'/'JOU', 'Artifact Creature — Chimera').
card_original_text('gold-forged sentinel'/'JOU', 'Flying').
card_first_print('gold-forged sentinel', 'JOU').
card_image_name('gold-forged sentinel'/'JOU', 'gold-forged sentinel').
card_uid('gold-forged sentinel'/'JOU', 'JOU:Gold-Forged Sentinel:gold-forged sentinel').
card_rarity('gold-forged sentinel'/'JOU', 'Uncommon').
card_artist('gold-forged sentinel'/'JOU', 'James Zapata').
card_number('gold-forged sentinel'/'JOU', '161').
card_flavor_text('gold-forged sentinel'/'JOU', 'Blessed by the gods. Coveted by mortals. Beholden to neither.').
card_multiverse_id('gold-forged sentinel'/'JOU', '380429').

card_in_set('golden hind', 'JOU').
card_original_type('golden hind'/'JOU', 'Creature — Elk').
card_original_text('golden hind'/'JOU', '{T}: Add {G} to your mana pool.').
card_first_print('golden hind', 'JOU').
card_image_name('golden hind'/'JOU', 'golden hind').
card_uid('golden hind'/'JOU', 'JOU:Golden Hind:golden hind').
card_rarity('golden hind'/'JOU', 'Common').
card_artist('golden hind'/'JOU', 'Cyril Van Der Haegen').
card_number('golden hind'/'JOU', '124').
card_flavor_text('golden hind'/'JOU', '\"It is not yours to hunt, mortal. Be content with its beauty alone.\"\n—Nylea, god of the hunt').
card_multiverse_id('golden hind'/'JOU', '380427').

card_in_set('goldenhide ox', 'JOU').
card_original_type('goldenhide ox'/'JOU', 'Enchantment Creature — Ox').
card_original_text('goldenhide ox'/'JOU', 'Constellation — Whenever Goldenhide Ox or another enchantment enters the battlefield under your control, target creature must be blocked this turn if able.').
card_first_print('goldenhide ox', 'JOU').
card_image_name('goldenhide ox'/'JOU', 'goldenhide ox').
card_uid('goldenhide ox'/'JOU', 'JOU:Goldenhide Ox:goldenhide ox').
card_rarity('goldenhide ox'/'JOU', 'Uncommon').
card_artist('goldenhide ox'/'JOU', 'Jack Wang').
card_number('goldenhide ox'/'JOU', '125').
card_flavor_text('goldenhide ox'/'JOU', 'The temptation to take the ox\'s golden hide is as irresistible as it is fatal.').
card_multiverse_id('goldenhide ox'/'JOU', '380428').

card_in_set('grim guardian', 'JOU').
card_original_type('grim guardian'/'JOU', 'Enchantment Creature — Zombie').
card_original_text('grim guardian'/'JOU', 'Constellation — Whenever Grim Guardian or another enchantment enters the battlefield under your control, each opponent loses 1 life.').
card_first_print('grim guardian', 'JOU').
card_image_name('grim guardian'/'JOU', 'grim guardian').
card_uid('grim guardian'/'JOU', 'JOU:Grim Guardian:grim guardian').
card_rarity('grim guardian'/'JOU', 'Common').
card_artist('grim guardian'/'JOU', 'Ryan Barger').
card_number('grim guardian'/'JOU', '73').
card_flavor_text('grim guardian'/'JOU', 'Occasionally the living wander to the Rivers, but the wardens of Athreos ensure that only the dead pass.').
card_multiverse_id('grim guardian'/'JOU', '380430').

card_in_set('hall of triumph', 'JOU').
card_original_type('hall of triumph'/'JOU', 'Legendary Artifact').
card_original_text('hall of triumph'/'JOU', 'As Hall of Triumph enters the battlefield, choose a color.\nCreatures you control of the chosen color get +1/+1.').
card_image_name('hall of triumph'/'JOU', 'hall of triumph').
card_uid('hall of triumph'/'JOU', 'JOU:Hall of Triumph:hall of triumph').
card_rarity('hall of triumph'/'JOU', 'Rare').
card_artist('hall of triumph'/'JOU', 'Ryan Yee').
card_number('hall of triumph'/'JOU', '162').
card_flavor_text('hall of triumph'/'JOU', 'Heroes act without thought of glory or reward. Accolades are a consequence, not a goal.').
card_multiverse_id('hall of triumph'/'JOU', '380431').

card_in_set('harness by force', 'JOU').
card_original_type('harness by force'/'JOU', 'Sorcery').
card_original_text('harness by force'/'JOU', 'Strive — Harness by Force costs {2}{R} more to cast for each target beyond the first.\nGain control of any number of target creatures until end of turn. Untap those creatures. They gain haste until end of turn.').
card_first_print('harness by force', 'JOU').
card_image_name('harness by force'/'JOU', 'harness by force').
card_uid('harness by force'/'JOU', 'JOU:Harness by Force:harness by force').
card_rarity('harness by force'/'JOU', 'Rare').
card_artist('harness by force'/'JOU', 'Yefim Kligerman').
card_number('harness by force'/'JOU', '100').
card_multiverse_id('harness by force'/'JOU', '380432').

card_in_set('harvestguard alseids', 'JOU').
card_original_type('harvestguard alseids'/'JOU', 'Enchantment Creature — Nymph').
card_original_text('harvestguard alseids'/'JOU', 'Constellation — Whenever Harvestguard Alseids or another enchantment enters the battlefield under your control, prevent all damage that would be dealt to target creature this turn.').
card_first_print('harvestguard alseids', 'JOU').
card_image_name('harvestguard alseids'/'JOU', 'harvestguard alseids').
card_uid('harvestguard alseids'/'JOU', 'JOU:Harvestguard Alseids:harvestguard alseids').
card_rarity('harvestguard alseids'/'JOU', 'Common').
card_artist('harvestguard alseids'/'JOU', 'Igor Kieryluk').
card_number('harvestguard alseids'/'JOU', '13').
card_flavor_text('harvestguard alseids'/'JOU', 'Not all shields are forged of iron or bronze.').
card_multiverse_id('harvestguard alseids'/'JOU', '380433').

card_in_set('heroes\' bane', 'JOU').
card_original_type('heroes\' bane'/'JOU', 'Creature — Hydra').
card_original_text('heroes\' bane'/'JOU', 'Heroes\' Bane enters the battlefield with four +1/+1 counters on it.\n{2}{G}{G}: Put X +1/+1 counters on Heroes\' Bane, where X is its power.').
card_image_name('heroes\' bane'/'JOU', 'heroes\' bane').
card_uid('heroes\' bane'/'JOU', 'JOU:Heroes\' Bane:heroes\' bane').
card_rarity('heroes\' bane'/'JOU', 'Rare').
card_artist('heroes\' bane'/'JOU', 'Raymond Swanland').
card_number('heroes\' bane'/'JOU', '126').
card_flavor_text('heroes\' bane'/'JOU', 'Travelers\' tales claim that hydras bite off their own heads to make themselves deadlier.').
card_multiverse_id('heroes\' bane'/'JOU', '380434').

card_in_set('hour of need', 'JOU').
card_original_type('hour of need'/'JOU', 'Instant').
card_original_text('hour of need'/'JOU', 'Strive — Hour of Need costs {1}{U} more to cast for each target beyond the first.\nExile any number of target creatures. For each creature exiled this way, its controller puts a 4/4 blue Sphinx creature token with flying onto the battlefield.').
card_first_print('hour of need', 'JOU').
card_image_name('hour of need'/'JOU', 'hour of need').
card_uid('hour of need'/'JOU', 'JOU:Hour of Need:hour of need').
card_rarity('hour of need'/'JOU', 'Uncommon').
card_artist('hour of need'/'JOU', 'Jesper Ejsing').
card_number('hour of need'/'JOU', '40').
card_multiverse_id('hour of need'/'JOU', '380435').

card_in_set('hubris', 'JOU').
card_original_type('hubris'/'JOU', 'Instant').
card_original_text('hubris'/'JOU', 'Return target creature and all Auras attached to it to their owners\' hands.').
card_first_print('hubris', 'JOU').
card_image_name('hubris'/'JOU', 'hubris').
card_uid('hubris'/'JOU', 'JOU:Hubris:hubris').
card_rarity('hubris'/'JOU', 'Common').
card_artist('hubris'/'JOU', 'Seb McKinnon').
card_number('hubris'/'JOU', '41').
card_flavor_text('hubris'/'JOU', '\"You wish to learn of Nyx? Very well.\"\n—Phenax, god of deception').
card_multiverse_id('hubris'/'JOU', '380436').

card_in_set('humbler of mortals', 'JOU').
card_original_type('humbler of mortals'/'JOU', 'Enchantment Creature — Elemental').
card_original_text('humbler of mortals'/'JOU', 'Constellation — Whenever Humbler of Mortals or another enchantment enters the battlefield under your control, creatures you control gain trample until end of turn.').
card_first_print('humbler of mortals', 'JOU').
card_image_name('humbler of mortals'/'JOU', 'humbler of mortals').
card_uid('humbler of mortals'/'JOU', 'JOU:Humbler of Mortals:humbler of mortals').
card_rarity('humbler of mortals'/'JOU', 'Common').
card_artist('humbler of mortals'/'JOU', 'Jason Felix').
card_number('humbler of mortals'/'JOU', '127').
card_flavor_text('humbler of mortals'/'JOU', 'The anger of the gods multiplied in Nyx, and the land awoke in dark echoes of their rage.').
card_multiverse_id('humbler of mortals'/'JOU', '380437').

card_in_set('hydra broodmaster', 'JOU').
card_original_type('hydra broodmaster'/'JOU', 'Creature — Hydra').
card_original_text('hydra broodmaster'/'JOU', '{X}{X}{G}: Monstrosity X. (If this creature isn\'t monstrous, put X +1/+1 counters on it and it becomes monstrous.)\nWhen Hydra Broodmaster becomes monstrous, put X X/X green Hydra creature tokens onto the battlefield.').
card_first_print('hydra broodmaster', 'JOU').
card_image_name('hydra broodmaster'/'JOU', 'hydra broodmaster').
card_uid('hydra broodmaster'/'JOU', 'JOU:Hydra Broodmaster:hydra broodmaster').
card_rarity('hydra broodmaster'/'JOU', 'Rare').
card_artist('hydra broodmaster'/'JOU', 'Steve Prescott').
card_number('hydra broodmaster'/'JOU', '128').
card_multiverse_id('hydra broodmaster'/'JOU', '380438').

card_in_set('hypnotic siren', 'JOU').
card_original_type('hypnotic siren'/'JOU', 'Enchantment Creature — Siren').
card_original_text('hypnotic siren'/'JOU', 'Bestow {5}{U}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying\nYou control enchanted creature.\nEnchanted creature gets +1/+1 and has flying.').
card_first_print('hypnotic siren', 'JOU').
card_image_name('hypnotic siren'/'JOU', 'hypnotic siren').
card_uid('hypnotic siren'/'JOU', 'JOU:Hypnotic Siren:hypnotic siren').
card_rarity('hypnotic siren'/'JOU', 'Rare').
card_artist('hypnotic siren'/'JOU', 'James Ryman').
card_number('hypnotic siren'/'JOU', '42').
card_multiverse_id('hypnotic siren'/'JOU', '380439').

card_in_set('interpret the signs', 'JOU').
card_original_type('interpret the signs'/'JOU', 'Sorcery').
card_original_text('interpret the signs'/'JOU', 'Scry 3, then reveal the top card of your library. Draw cards equal to that card\'s converted mana cost. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('interpret the signs', 'JOU').
card_image_name('interpret the signs'/'JOU', 'interpret the signs').
card_uid('interpret the signs'/'JOU', 'JOU:Interpret the Signs:interpret the signs').
card_rarity('interpret the signs'/'JOU', 'Uncommon').
card_artist('interpret the signs'/'JOU', 'Cynthia Sheppard').
card_number('interpret the signs'/'JOU', '43').
card_multiverse_id('interpret the signs'/'JOU', '380440').

card_in_set('iroas, god of victory', 'JOU').
card_original_type('iroas, god of victory'/'JOU', 'Legendary Enchantment Creature — God').
card_original_text('iroas, god of victory'/'JOU', 'Indestructible\nAs long as your devotion to red and white is less than seven, Iroas isn\'t a creature.\nCreatures you control can\'t be blocked except by two or more creatures.\nPrevent all damage that would be dealt to attacking creatures you control.').
card_first_print('iroas, god of victory', 'JOU').
card_image_name('iroas, god of victory'/'JOU', 'iroas, god of victory').
card_uid('iroas, god of victory'/'JOU', 'JOU:Iroas, God of Victory:iroas, god of victory').
card_rarity('iroas, god of victory'/'JOU', 'Mythic Rare').
card_artist('iroas, god of victory'/'JOU', 'Slawomir Maniak').
card_number('iroas, god of victory'/'JOU', '150').
card_multiverse_id('iroas, god of victory'/'JOU', '380441').

card_in_set('keranos, god of storms', 'JOU').
card_original_type('keranos, god of storms'/'JOU', 'Legendary Enchantment Creature — God').
card_original_text('keranos, god of storms'/'JOU', 'Indestructible\nAs long as your devotion to blue and red is less than seven, Keranos isn\'t a creature.\nReveal the first card you draw on each of your turns. Whenever you reveal a land card this way, draw a card. Whenever you reveal a nonland card this way, Keranos deals 3 damage to target creature or player.').
card_first_print('keranos, god of storms', 'JOU').
card_image_name('keranos, god of storms'/'JOU', 'keranos, god of storms').
card_uid('keranos, god of storms'/'JOU', 'JOU:Keranos, God of Storms:keranos, god of storms').
card_rarity('keranos, god of storms'/'JOU', 'Mythic Rare').
card_artist('keranos, god of storms'/'JOU', 'Daarken').
card_number('keranos, god of storms'/'JOU', '151').
card_multiverse_id('keranos, god of storms'/'JOU', '380442').

card_in_set('king macar, the gold-cursed', 'JOU').
card_original_type('king macar, the gold-cursed'/'JOU', 'Legendary Creature — Human').
card_original_text('king macar, the gold-cursed'/'JOU', 'Inspired — Whenever King Macar, the Gold-Cursed becomes untapped, you may exile target creature. If you do, put a colorless artifact token named Gold onto the battlefield. It has \"Sacrifice this artifact: Add one mana of any color to your mana pool.\"').
card_first_print('king macar, the gold-cursed', 'JOU').
card_image_name('king macar, the gold-cursed'/'JOU', 'king macar, the gold-cursed').
card_uid('king macar, the gold-cursed'/'JOU', 'JOU:King Macar, the Gold-Cursed:king macar, the gold-cursed').
card_rarity('king macar, the gold-cursed'/'JOU', 'Rare').
card_artist('king macar, the gold-cursed'/'JOU', 'Greg Staples').
card_number('king macar, the gold-cursed'/'JOU', '74').
card_multiverse_id('king macar, the gold-cursed'/'JOU', '380443').

card_in_set('kiora\'s dismissal', 'JOU').
card_original_type('kiora\'s dismissal'/'JOU', 'Instant').
card_original_text('kiora\'s dismissal'/'JOU', 'Strive — Kiora\'s Dismissal costs {U} more to cast for each target beyond the first.\nReturn any number of target enchantments to their owners\' hands.').
card_first_print('kiora\'s dismissal', 'JOU').
card_image_name('kiora\'s dismissal'/'JOU', 'kiora\'s dismissal').
card_uid('kiora\'s dismissal'/'JOU', 'JOU:Kiora\'s Dismissal:kiora\'s dismissal').
card_rarity('kiora\'s dismissal'/'JOU', 'Uncommon').
card_artist('kiora\'s dismissal'/'JOU', 'Mathias Kollros').
card_number('kiora\'s dismissal'/'JOU', '44').
card_multiverse_id('kiora\'s dismissal'/'JOU', '380444').

card_in_set('knowledge and power', 'JOU').
card_original_type('knowledge and power'/'JOU', 'Enchantment').
card_original_text('knowledge and power'/'JOU', 'Whenever you scry, you may pay {2}. If you do, Knowledge and Power deals 2 damage to target creature or player.').
card_first_print('knowledge and power', 'JOU').
card_image_name('knowledge and power'/'JOU', 'knowledge and power').
card_uid('knowledge and power'/'JOU', 'JOU:Knowledge and Power:knowledge and power').
card_rarity('knowledge and power'/'JOU', 'Uncommon').
card_artist('knowledge and power'/'JOU', 'Jung Park').
card_number('knowledge and power'/'JOU', '101').
card_flavor_text('knowledge and power'/'JOU', '\"Information is a powerful weapon.\"\n—Cymede, queen of Akros').
card_multiverse_id('knowledge and power'/'JOU', '380445').

card_in_set('kruphix\'s insight', 'JOU').
card_original_type('kruphix\'s insight'/'JOU', 'Sorcery').
card_original_text('kruphix\'s insight'/'JOU', 'Reveal the top six cards of your library. Put up to three enchantment cards from among them into your hand and the rest of the revealed cards into your graveyard.').
card_first_print('kruphix\'s insight', 'JOU').
card_image_name('kruphix\'s insight'/'JOU', 'kruphix\'s insight').
card_uid('kruphix\'s insight'/'JOU', 'JOU:Kruphix\'s Insight:kruphix\'s insight').
card_rarity('kruphix\'s insight'/'JOU', 'Common').
card_artist('kruphix\'s insight'/'JOU', 'Igor Kieryluk').
card_number('kruphix\'s insight'/'JOU', '129').
card_flavor_text('kruphix\'s insight'/'JOU', 'The horizon is tantalizing in both its uncertainty and its potential.').
card_multiverse_id('kruphix\'s insight'/'JOU', '380447').

card_in_set('kruphix, god of horizons', 'JOU').
card_original_type('kruphix, god of horizons'/'JOU', 'Legendary Enchantment Creature — God').
card_original_text('kruphix, god of horizons'/'JOU', 'Indestructible\nAs long as your devotion to green and blue is less than seven, Kruphix isn\'t a creature.\nYou have no maximum hand size.\nIf unused mana would empty from your mana pool, that mana becomes colorless instead.').
card_first_print('kruphix, god of horizons', 'JOU').
card_image_name('kruphix, god of horizons'/'JOU', 'kruphix, god of horizons').
card_uid('kruphix, god of horizons'/'JOU', 'JOU:Kruphix, God of Horizons:kruphix, god of horizons').
card_rarity('kruphix, god of horizons'/'JOU', 'Mythic Rare').
card_artist('kruphix, god of horizons'/'JOU', 'Daarken').
card_number('kruphix, god of horizons'/'JOU', '152').
card_multiverse_id('kruphix, god of horizons'/'JOU', '380446').

card_in_set('lagonna-band trailblazer', 'JOU').
card_original_type('lagonna-band trailblazer'/'JOU', 'Creature — Centaur Scout').
card_original_text('lagonna-band trailblazer'/'JOU', 'Heroic — Whenever you cast a spell that targets Lagonna-Band Trailblazer, put a +1/+1 counter on Lagonna-Band Trailblazer.').
card_first_print('lagonna-band trailblazer', 'JOU').
card_image_name('lagonna-band trailblazer'/'JOU', 'lagonna-band trailblazer').
card_uid('lagonna-band trailblazer'/'JOU', 'JOU:Lagonna-Band Trailblazer:lagonna-band trailblazer').
card_rarity('lagonna-band trailblazer'/'JOU', 'Common').
card_artist('lagonna-band trailblazer'/'JOU', 'James Zapata').
card_number('lagonna-band trailblazer'/'JOU', '14').
card_flavor_text('lagonna-band trailblazer'/'JOU', 'Go forth with honor, return with glory.').
card_multiverse_id('lagonna-band trailblazer'/'JOU', '380448').

card_in_set('launch the fleet', 'JOU').
card_original_type('launch the fleet'/'JOU', 'Sorcery').
card_original_text('launch the fleet'/'JOU', 'Strive — Launch the Fleet costs {1} more to cast for each target beyond the first.\nUntil end of turn, any number of target creatures each gain \"Whenever this creature attacks, put a 1/1 white Soldier creature token onto the battlefield tapped and attacking.\"').
card_first_print('launch the fleet', 'JOU').
card_image_name('launch the fleet'/'JOU', 'launch the fleet').
card_uid('launch the fleet'/'JOU', 'JOU:Launch the Fleet:launch the fleet').
card_rarity('launch the fleet'/'JOU', 'Rare').
card_artist('launch the fleet'/'JOU', 'Karl Kopinski').
card_number('launch the fleet'/'JOU', '15').
card_multiverse_id('launch the fleet'/'JOU', '380449').

card_in_set('leonin iconoclast', 'JOU').
card_original_type('leonin iconoclast'/'JOU', 'Creature — Cat Monk').
card_original_text('leonin iconoclast'/'JOU', 'Heroic — Whenever you cast a spell that targets Leonin Iconoclast, destroy target enchantment creature an opponent controls.').
card_first_print('leonin iconoclast', 'JOU').
card_image_name('leonin iconoclast'/'JOU', 'leonin iconoclast').
card_uid('leonin iconoclast'/'JOU', 'JOU:Leonin Iconoclast:leonin iconoclast').
card_rarity('leonin iconoclast'/'JOU', 'Uncommon').
card_artist('leonin iconoclast'/'JOU', 'Steve Prescott').
card_number('leonin iconoclast'/'JOU', '16').
card_flavor_text('leonin iconoclast'/'JOU', '\"The stars belong in the night sky. This is our world, and we are our own masters.\"').
card_multiverse_id('leonin iconoclast'/'JOU', '380450').

card_in_set('lightning diadem', 'JOU').
card_original_type('lightning diadem'/'JOU', 'Enchantment — Aura').
card_original_text('lightning diadem'/'JOU', 'Enchant creature\nWhen Lightning Diadem enters the battlefield, it deals 2 damage to target creature or player.\nEnchanted creature gets +2/+2.').
card_first_print('lightning diadem', 'JOU').
card_image_name('lightning diadem'/'JOU', 'lightning diadem').
card_uid('lightning diadem'/'JOU', 'JOU:Lightning Diadem:lightning diadem').
card_rarity('lightning diadem'/'JOU', 'Common').
card_artist('lightning diadem'/'JOU', 'Ryan Alexander Lee').
card_number('lightning diadem'/'JOU', '102').
card_flavor_text('lightning diadem'/'JOU', '\"I fight for Keranos and for mortals. I see no contradiction.\"').
card_multiverse_id('lightning diadem'/'JOU', '380451').

card_in_set('magma spray', 'JOU').
card_original_type('magma spray'/'JOU', 'Instant').
card_original_text('magma spray'/'JOU', 'Magma Spray deals 2 damage to target creature. If that creature would die this turn, exile it instead.').
card_image_name('magma spray'/'JOU', 'magma spray').
card_uid('magma spray'/'JOU', 'JOU:Magma Spray:magma spray').
card_rarity('magma spray'/'JOU', 'Common').
card_artist('magma spray'/'JOU', 'Richard Wright').
card_number('magma spray'/'JOU', '103').
card_flavor_text('magma spray'/'JOU', 'The ancient dragon Thraxes sleeps in Purphoros\'s sacred peak. When he stirs in dreams, so does the mountain.').
card_multiverse_id('magma spray'/'JOU', '380452').

card_in_set('mana confluence', 'JOU').
card_original_type('mana confluence'/'JOU', 'Land').
card_original_text('mana confluence'/'JOU', '{T}, Pay 1 life: Add one mana of any color to your mana pool.').
card_first_print('mana confluence', 'JOU').
card_image_name('mana confluence'/'JOU', 'mana confluence').
card_uid('mana confluence'/'JOU', 'JOU:Mana Confluence:mana confluence').
card_rarity('mana confluence'/'JOU', 'Rare').
card_artist('mana confluence'/'JOU', 'Richard Wright').
card_number('mana confluence'/'JOU', '163').
card_flavor_text('mana confluence'/'JOU', 'Five rivers encircle Theros, flowing with waters more ancient than the world itself.').
card_multiverse_id('mana confluence'/'JOU', '380453').

card_in_set('market festival', 'JOU').
card_original_type('market festival'/'JOU', 'Enchantment — Aura').
card_original_text('market festival'/'JOU', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds two mana in any combination of colors to his or her mana pool (in addition to the mana the land produces).').
card_first_print('market festival', 'JOU').
card_image_name('market festival'/'JOU', 'market festival').
card_uid('market festival'/'JOU', 'JOU:Market Festival:market festival').
card_rarity('market festival'/'JOU', 'Common').
card_artist('market festival'/'JOU', 'Ryan Barger').
card_number('market festival'/'JOU', '130').
card_flavor_text('market festival'/'JOU', 'Commerce is always the basis for peace.').
card_multiverse_id('market festival'/'JOU', '380454').

card_in_set('master of the feast', 'JOU').
card_original_type('master of the feast'/'JOU', 'Enchantment Creature — Demon').
card_original_text('master of the feast'/'JOU', 'Flying\nAt the beginning of your upkeep, each opponent draws a card.').
card_first_print('master of the feast', 'JOU').
card_image_name('master of the feast'/'JOU', 'master of the feast').
card_uid('master of the feast'/'JOU', 'JOU:Master of the Feast:master of the feast').
card_rarity('master of the feast'/'JOU', 'Rare').
card_artist('master of the feast'/'JOU', 'Chris Rahn').
card_number('master of the feast'/'JOU', '75').
card_flavor_text('master of the feast'/'JOU', 'Any pleasure, when taken to excess, becomes torment. It\'s a formula that demons have perfected.').
card_multiverse_id('master of the feast'/'JOU', '380455').

card_in_set('mogis\'s warhound', 'JOU').
card_original_type('mogis\'s warhound'/'JOU', 'Enchantment Creature — Hound').
card_original_text('mogis\'s warhound'/'JOU', 'Bestow {2}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nMogis\'s Warhound attacks each turn if able.\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_first_print('mogis\'s warhound', 'JOU').
card_image_name('mogis\'s warhound'/'JOU', 'mogis\'s warhound').
card_uid('mogis\'s warhound'/'JOU', 'JOU:Mogis\'s Warhound:mogis\'s warhound').
card_rarity('mogis\'s warhound'/'JOU', 'Uncommon').
card_artist('mogis\'s warhound'/'JOU', 'John Severin Brassell').
card_number('mogis\'s warhound'/'JOU', '104').
card_multiverse_id('mogis\'s warhound'/'JOU', '380456').

card_in_set('mortal obstinacy', 'JOU').
card_original_type('mortal obstinacy'/'JOU', 'Enchantment — Aura').
card_original_text('mortal obstinacy'/'JOU', 'Enchant creature you control\nEnchanted creature gets +1/+1.\nWhenever enchanted creature deals combat damage to a player, you may sacrifice Mortal Obstinacy. If you do, destroy target enchantment.').
card_first_print('mortal obstinacy', 'JOU').
card_image_name('mortal obstinacy'/'JOU', 'mortal obstinacy').
card_uid('mortal obstinacy'/'JOU', 'JOU:Mortal Obstinacy:mortal obstinacy').
card_rarity('mortal obstinacy'/'JOU', 'Common').
card_artist('mortal obstinacy'/'JOU', 'Slawomir Maniak').
card_number('mortal obstinacy'/'JOU', '17').
card_multiverse_id('mortal obstinacy'/'JOU', '380457').

card_in_set('nature\'s panoply', 'JOU').
card_original_type('nature\'s panoply'/'JOU', 'Instant').
card_original_text('nature\'s panoply'/'JOU', 'Strive — Nature\'s Panoply costs {2}{G} more to cast for each target beyond the first.\nChoose any number of target creatures. Put a +1/+1 counter on each of them.').
card_first_print('nature\'s panoply', 'JOU').
card_image_name('nature\'s panoply'/'JOU', 'nature\'s panoply').
card_uid('nature\'s panoply'/'JOU', 'JOU:Nature\'s Panoply:nature\'s panoply').
card_rarity('nature\'s panoply'/'JOU', 'Common').
card_artist('nature\'s panoply'/'JOU', 'John Avon').
card_number('nature\'s panoply'/'JOU', '131').
card_flavor_text('nature\'s panoply'/'JOU', 'Nature protects its own.').
card_multiverse_id('nature\'s panoply'/'JOU', '380458').

card_in_set('nessian game warden', 'JOU').
card_original_type('nessian game warden'/'JOU', 'Creature — Beast').
card_original_text('nessian game warden'/'JOU', 'When Nessian Game Warden enters the battlefield, look at the top X cards of your library, where X is the number of Forests you control. You may reveal a creature card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('nessian game warden', 'JOU').
card_image_name('nessian game warden'/'JOU', 'nessian game warden').
card_uid('nessian game warden'/'JOU', 'JOU:Nessian Game Warden:nessian game warden').
card_rarity('nessian game warden'/'JOU', 'Uncommon').
card_artist('nessian game warden'/'JOU', 'Vincent Proce').
card_number('nessian game warden'/'JOU', '132').
card_multiverse_id('nessian game warden'/'JOU', '380459').

card_in_set('nightmarish end', 'JOU').
card_original_type('nightmarish end'/'JOU', 'Instant').
card_original_text('nightmarish end'/'JOU', 'Target creature gets -X/-X until end of turn, where X is the number of cards in your hand.').
card_first_print('nightmarish end', 'JOU').
card_image_name('nightmarish end'/'JOU', 'nightmarish end').
card_uid('nightmarish end'/'JOU', 'JOU:Nightmarish End:nightmarish end').
card_rarity('nightmarish end'/'JOU', 'Uncommon').
card_artist('nightmarish end'/'JOU', 'Willian Murai').
card_number('nightmarish end'/'JOU', '76').
card_flavor_text('nightmarish end'/'JOU', '\"To think some believe it peaceful to die in one\'s sleep.\"\n—Ashiok, Nightmare Weaver').
card_multiverse_id('nightmarish end'/'JOU', '380460').

card_in_set('nyx infusion', 'JOU').
card_original_type('nyx infusion'/'JOU', 'Enchantment — Aura').
card_original_text('nyx infusion'/'JOU', 'Enchant creature\nEnchanted creature gets +2/+2 as long as it\'s an enchantment. Otherwise, it gets -2/-2.').
card_first_print('nyx infusion', 'JOU').
card_image_name('nyx infusion'/'JOU', 'nyx infusion').
card_uid('nyx infusion'/'JOU', 'JOU:Nyx Infusion:nyx infusion').
card_rarity('nyx infusion'/'JOU', 'Common').
card_artist('nyx infusion'/'JOU', 'Jason A. Engle').
card_number('nyx infusion'/'JOU', '77').
card_flavor_text('nyx infusion'/'JOU', 'A balm or a poison depending on how it is administered, and to whom.').
card_multiverse_id('nyx infusion'/'JOU', '380461').

card_in_set('nyx weaver', 'JOU').
card_original_type('nyx weaver'/'JOU', 'Enchantment Creature — Spider').
card_original_text('nyx weaver'/'JOU', 'Reach\nAt the beginning of your upkeep, put the top two cards of your library into your graveyard.\n{1}{B}{G}, Exile Nyx Weaver: Return target card from your graveyard to your hand.').
card_first_print('nyx weaver', 'JOU').
card_image_name('nyx weaver'/'JOU', 'nyx weaver').
card_uid('nyx weaver'/'JOU', 'JOU:Nyx Weaver:nyx weaver').
card_rarity('nyx weaver'/'JOU', 'Uncommon').
card_artist('nyx weaver'/'JOU', 'Jack Wang').
card_number('nyx weaver'/'JOU', '153').
card_multiverse_id('nyx weaver'/'JOU', '380462').

card_in_set('nyx-fleece ram', 'JOU').
card_original_type('nyx-fleece ram'/'JOU', 'Enchantment Creature — Sheep').
card_original_text('nyx-fleece ram'/'JOU', 'At the beginning of your upkeep, you gain 1 life.').
card_first_print('nyx-fleece ram', 'JOU').
card_image_name('nyx-fleece ram'/'JOU', 'nyx-fleece ram').
card_uid('nyx-fleece ram'/'JOU', 'JOU:Nyx-Fleece Ram:nyx-fleece ram').
card_rarity('nyx-fleece ram'/'JOU', 'Uncommon').
card_artist('nyx-fleece ram'/'JOU', 'Terese Nielsen').
card_number('nyx-fleece ram'/'JOU', '18').
card_flavor_text('nyx-fleece ram'/'JOU', 'The gods are fascinated by innocence, perhaps because it\'s a quality they themselves lack.').
card_multiverse_id('nyx-fleece ram'/'JOU', '380463').

card_in_set('oakheart dryads', 'JOU').
card_original_type('oakheart dryads'/'JOU', 'Enchantment Creature — Nymph Dryad').
card_original_text('oakheart dryads'/'JOU', 'Constellation — Whenever Oakheart Dryads or another enchantment enters the battlefield under your control, target creature gets +1/+1 until end of turn.').
card_first_print('oakheart dryads', 'JOU').
card_image_name('oakheart dryads'/'JOU', 'oakheart dryads').
card_uid('oakheart dryads'/'JOU', 'JOU:Oakheart Dryads:oakheart dryads').
card_rarity('oakheart dryads'/'JOU', 'Common').
card_artist('oakheart dryads'/'JOU', 'Johann Bodin').
card_number('oakheart dryads'/'JOU', '133').
card_flavor_text('oakheart dryads'/'JOU', '\"I do not know if the forest was different after they passed. But I know I was.\"\n—Ianthe, Setessan hunter').
card_multiverse_id('oakheart dryads'/'JOU', '380464').

card_in_set('oppressive rays', 'JOU').
card_original_type('oppressive rays'/'JOU', 'Enchantment — Aura').
card_original_text('oppressive rays'/'JOU', 'Enchant creature\nEnchanted creature can\'t attack or block unless its controller pays {3}.\nActivated abilities of enchanted creature cost {3} more to activate.').
card_first_print('oppressive rays', 'JOU').
card_image_name('oppressive rays'/'JOU', 'oppressive rays').
card_uid('oppressive rays'/'JOU', 'JOU:Oppressive Rays:oppressive rays').
card_rarity('oppressive rays'/'JOU', 'Common').
card_artist('oppressive rays'/'JOU', 'Mark Zug').
card_number('oppressive rays'/'JOU', '19').
card_multiverse_id('oppressive rays'/'JOU', '380465').

card_in_set('oreskos swiftclaw', 'JOU').
card_original_type('oreskos swiftclaw'/'JOU', 'Creature — Cat Warrior').
card_original_text('oreskos swiftclaw'/'JOU', '').
card_first_print('oreskos swiftclaw', 'JOU').
card_image_name('oreskos swiftclaw'/'JOU', 'oreskos swiftclaw').
card_uid('oreskos swiftclaw'/'JOU', 'JOU:Oreskos Swiftclaw:oreskos swiftclaw').
card_rarity('oreskos swiftclaw'/'JOU', 'Common').
card_artist('oreskos swiftclaw'/'JOU', 'James Ryman').
card_number('oreskos swiftclaw'/'JOU', '20').
card_flavor_text('oreskos swiftclaw'/'JOU', 'After the Battle of Pharagax Bridge, the Champion spent many months among the leonin of Oreskos. She found that they were quick to take offense, not because they were thin-skinned, but because they were always eager for a fight.\n—The Theriad').
card_multiverse_id('oreskos swiftclaw'/'JOU', '380466').

card_in_set('pensive minotaur', 'JOU').
card_original_type('pensive minotaur'/'JOU', 'Creature — Minotaur Warrior').
card_original_text('pensive minotaur'/'JOU', '').
card_first_print('pensive minotaur', 'JOU').
card_image_name('pensive minotaur'/'JOU', 'pensive minotaur').
card_uid('pensive minotaur'/'JOU', 'JOU:Pensive Minotaur:pensive minotaur').
card_rarity('pensive minotaur'/'JOU', 'Common').
card_artist('pensive minotaur'/'JOU', 'Svetlin Velinov').
card_number('pensive minotaur'/'JOU', '105').
card_flavor_text('pensive minotaur'/'JOU', 'The Champion and her companions marched through the night, but the battle was over before they arrived. In the middle of the carnage sat a solitary minotaur, lost in what seemed to the Champion to be thought.\n—The Theriad').
card_multiverse_id('pensive minotaur'/'JOU', '380467').

card_in_set('phalanx formation', 'JOU').
card_original_type('phalanx formation'/'JOU', 'Instant').
card_original_text('phalanx formation'/'JOU', 'Strive — Phalanx Formation costs {1}{W} more to cast for each target beyond the first.\nAny number of target creatures each gain double strike until end of turn. (They deal both first-strike and regular combat damage.)').
card_first_print('phalanx formation', 'JOU').
card_image_name('phalanx formation'/'JOU', 'phalanx formation').
card_uid('phalanx formation'/'JOU', 'JOU:Phalanx Formation:phalanx formation').
card_rarity('phalanx formation'/'JOU', 'Uncommon').
card_artist('phalanx formation'/'JOU', 'Dan Scott').
card_number('phalanx formation'/'JOU', '21').
card_multiverse_id('phalanx formation'/'JOU', '380468').

card_in_set('pharika\'s chosen', 'JOU').
card_original_type('pharika\'s chosen'/'JOU', 'Creature — Snake').
card_original_text('pharika\'s chosen'/'JOU', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('pharika\'s chosen', 'JOU').
card_image_name('pharika\'s chosen'/'JOU', 'pharika\'s chosen').
card_uid('pharika\'s chosen'/'JOU', 'JOU:Pharika\'s Chosen:pharika\'s chosen').
card_rarity('pharika\'s chosen'/'JOU', 'Common').
card_artist('pharika\'s chosen'/'JOU', 'Jonas De Ro').
card_number('pharika\'s chosen'/'JOU', '78').
card_flavor_text('pharika\'s chosen'/'JOU', 'The serpents\' dens built into statues of Pharika serve not only to consecrate her temples, but as a hidden line of defense.').
card_multiverse_id('pharika\'s chosen'/'JOU', '380470').

card_in_set('pharika, god of affliction', 'JOU').
card_original_type('pharika, god of affliction'/'JOU', 'Legendary Enchantment Creature — God').
card_original_text('pharika, god of affliction'/'JOU', 'Indestructible\nAs long as your devotion to black and green is less than seven, Pharika isn\'t a creature.\n{B}{G}: Exile target creature card from a graveyard. Its owner puts a 1/1 black and green Snake enchantment creature token with deathtouch onto the battlefield.').
card_first_print('pharika, god of affliction', 'JOU').
card_image_name('pharika, god of affliction'/'JOU', 'pharika, god of affliction').
card_uid('pharika, god of affliction'/'JOU', 'JOU:Pharika, God of Affliction:pharika, god of affliction').
card_rarity('pharika, god of affliction'/'JOU', 'Mythic Rare').
card_artist('pharika, god of affliction'/'JOU', 'Peter Mohrbacher').
card_number('pharika, god of affliction'/'JOU', '154').
card_multiverse_id('pharika, god of affliction'/'JOU', '380469').

card_in_set('pheres-band thunderhoof', 'JOU').
card_original_type('pheres-band thunderhoof'/'JOU', 'Creature — Centaur Warrior').
card_original_text('pheres-band thunderhoof'/'JOU', 'Heroic — Whenever you cast a spell that targets Pheres-Band Thunderhoof, put two +1/+1 counters on Pheres-Band Thunderhoof.').
card_first_print('pheres-band thunderhoof', 'JOU').
card_image_name('pheres-band thunderhoof'/'JOU', 'pheres-band thunderhoof').
card_uid('pheres-band thunderhoof'/'JOU', 'JOU:Pheres-Band Thunderhoof:pheres-band thunderhoof').
card_rarity('pheres-band thunderhoof'/'JOU', 'Common').
card_artist('pheres-band thunderhoof'/'JOU', 'Christopher Moeller').
card_number('pheres-band thunderhoof'/'JOU', '134').
card_flavor_text('pheres-band thunderhoof'/'JOU', '\"Flesh and bone or star and shadow. It makes no difference to my club.\"').
card_multiverse_id('pheres-band thunderhoof'/'JOU', '380471').

card_in_set('pheres-band warchief', 'JOU').
card_original_type('pheres-band warchief'/'JOU', 'Creature — Centaur Warrior').
card_original_text('pheres-band warchief'/'JOU', 'Vigilance, trample\nOther Centaur creatures you control get +1/+1 and have vigilance and trample.').
card_first_print('pheres-band warchief', 'JOU').
card_image_name('pheres-band warchief'/'JOU', 'pheres-band warchief').
card_uid('pheres-band warchief'/'JOU', 'JOU:Pheres-Band Warchief:pheres-band warchief').
card_rarity('pheres-band warchief'/'JOU', 'Rare').
card_artist('pheres-band warchief'/'JOU', 'James Ryman').
card_number('pheres-band warchief'/'JOU', '135').
card_flavor_text('pheres-band warchief'/'JOU', 'The renowned warchiefs of Pheres Band fight with the might of minotaurs and the sagacity of sphinxes.').
card_multiverse_id('pheres-band warchief'/'JOU', '380472').

card_in_set('pin to the earth', 'JOU').
card_original_type('pin to the earth'/'JOU', 'Enchantment — Aura').
card_original_text('pin to the earth'/'JOU', 'Enchant creature\nEnchanted creature gets -6/-0.').
card_first_print('pin to the earth', 'JOU').
card_image_name('pin to the earth'/'JOU', 'pin to the earth').
card_uid('pin to the earth'/'JOU', 'JOU:Pin to the Earth:pin to the earth').
card_rarity('pin to the earth'/'JOU', 'Common').
card_artist('pin to the earth'/'JOU', 'Johann Bodin').
card_number('pin to the earth'/'JOU', '45').
card_flavor_text('pin to the earth'/'JOU', '\"Mortal strength, like mortal hope, is so easily crushed.\"\n—Thassa, god of the sea').
card_multiverse_id('pin to the earth'/'JOU', '380473').

card_in_set('polymorphous rush', 'JOU').
card_original_type('polymorphous rush'/'JOU', 'Instant').
card_original_text('polymorphous rush'/'JOU', 'Strive — Polymorphous Rush costs {1}{U} more to cast for each target beyond the first.\nChoose a creature on the battlefield. Any number of target creatures you control each become a copy of that creature until end of turn.').
card_first_print('polymorphous rush', 'JOU').
card_image_name('polymorphous rush'/'JOU', 'polymorphous rush').
card_uid('polymorphous rush'/'JOU', 'JOU:Polymorphous Rush:polymorphous rush').
card_rarity('polymorphous rush'/'JOU', 'Rare').
card_artist('polymorphous rush'/'JOU', 'Ryan Barger').
card_number('polymorphous rush'/'JOU', '46').
card_multiverse_id('polymorphous rush'/'JOU', '380474').

card_in_set('prophetic flamespeaker', 'JOU').
card_original_type('prophetic flamespeaker'/'JOU', 'Creature — Human Shaman').
card_original_text('prophetic flamespeaker'/'JOU', 'Double strike, trample\nWhenever Prophetic Flamespeaker deals combat damage to a player, exile the top card of your library. You may play it this turn.').
card_first_print('prophetic flamespeaker', 'JOU').
card_image_name('prophetic flamespeaker'/'JOU', 'prophetic flamespeaker').
card_uid('prophetic flamespeaker'/'JOU', 'JOU:Prophetic Flamespeaker:prophetic flamespeaker').
card_rarity('prophetic flamespeaker'/'JOU', 'Mythic Rare').
card_artist('prophetic flamespeaker'/'JOU', 'Cynthia Sheppard').
card_number('prophetic flamespeaker'/'JOU', '106').
card_flavor_text('prophetic flamespeaker'/'JOU', 'Fire to destroy. Fire to create.').
card_multiverse_id('prophetic flamespeaker'/'JOU', '380475').

card_in_set('pull from the deep', 'JOU').
card_original_type('pull from the deep'/'JOU', 'Sorcery').
card_original_text('pull from the deep'/'JOU', 'Return up to one target instant card and up to one target sorcery card from your graveyard to your hand. Exile Pull from the Deep.').
card_first_print('pull from the deep', 'JOU').
card_image_name('pull from the deep'/'JOU', 'pull from the deep').
card_uid('pull from the deep'/'JOU', 'JOU:Pull from the Deep:pull from the deep').
card_rarity('pull from the deep'/'JOU', 'Uncommon').
card_artist('pull from the deep'/'JOU', 'Ryan Yee').
card_number('pull from the deep'/'JOU', '47').
card_flavor_text('pull from the deep'/'JOU', 'Ideas, not stones, form the true foundation of Meletis.').
card_multiverse_id('pull from the deep'/'JOU', '380476').

card_in_set('quarry colossus', 'JOU').
card_original_type('quarry colossus'/'JOU', 'Creature — Giant').
card_original_text('quarry colossus'/'JOU', 'When Quarry Colossus enters the battlefield, put target creature into its owner\'s library just beneath the top X cards of that library, where X is the number of Plains you control.').
card_first_print('quarry colossus', 'JOU').
card_image_name('quarry colossus'/'JOU', 'quarry colossus').
card_uid('quarry colossus'/'JOU', 'JOU:Quarry Colossus:quarry colossus').
card_rarity('quarry colossus'/'JOU', 'Uncommon').
card_artist('quarry colossus'/'JOU', 'Todd Lockwood').
card_number('quarry colossus'/'JOU', '22').
card_multiverse_id('quarry colossus'/'JOU', '380477').

card_in_set('ravenous leucrocota', 'JOU').
card_original_type('ravenous leucrocota'/'JOU', 'Creature — Beast').
card_original_text('ravenous leucrocota'/'JOU', 'Vigilance\n{6}{G}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_first_print('ravenous leucrocota', 'JOU').
card_image_name('ravenous leucrocota'/'JOU', 'ravenous leucrocota').
card_uid('ravenous leucrocota'/'JOU', 'JOU:Ravenous Leucrocota:ravenous leucrocota').
card_rarity('ravenous leucrocota'/'JOU', 'Common').
card_artist('ravenous leucrocota'/'JOU', 'Christopher Burdett').
card_number('ravenous leucrocota'/'JOU', '136').
card_flavor_text('ravenous leucrocota'/'JOU', 'Hunger makes a leucrocota dangerous. A full belly makes it angry and dangerous.').
card_multiverse_id('ravenous leucrocota'/'JOU', '380478').

card_in_set('renowned weaver', 'JOU').
card_original_type('renowned weaver'/'JOU', 'Creature — Human Shaman').
card_original_text('renowned weaver'/'JOU', '{1}{G}, Sacrifice Renowned Weaver: Put a 1/3 green Spider enchantment creature token with reach onto the battlefield. (It can block creatures with flying.)').
card_first_print('renowned weaver', 'JOU').
card_image_name('renowned weaver'/'JOU', 'renowned weaver').
card_uid('renowned weaver'/'JOU', 'JOU:Renowned Weaver:renowned weaver').
card_rarity('renowned weaver'/'JOU', 'Common').
card_artist('renowned weaver'/'JOU', 'Yohann Schepacz').
card_number('renowned weaver'/'JOU', '137').
card_flavor_text('renowned weaver'/'JOU', '\"You think your weaving as beautiful as mine? Let me show you the true meaning of the craft.\"').
card_multiverse_id('renowned weaver'/'JOU', '380479').

card_in_set('reprisal', 'JOU').
card_original_type('reprisal'/'JOU', 'Instant').
card_original_text('reprisal'/'JOU', 'Destroy target creature with power 4 or greater. It can\'t be regenerated.').
card_image_name('reprisal'/'JOU', 'reprisal').
card_uid('reprisal'/'JOU', 'JOU:Reprisal:reprisal').
card_rarity('reprisal'/'JOU', 'Uncommon').
card_artist('reprisal'/'JOU', 'Raymond Swanland').
card_number('reprisal'/'JOU', '23').
card_flavor_text('reprisal'/'JOU', '\"A tiny flame is enough to dispel the night.\"\n—Elspeth').
card_multiverse_id('reprisal'/'JOU', '380480').

card_in_set('returned reveler', 'JOU').
card_original_type('returned reveler'/'JOU', 'Creature — Zombie Satyr').
card_original_text('returned reveler'/'JOU', 'When Returned Reveler dies, each player puts the top three cards of his or her library into his or her graveyard.').
card_first_print('returned reveler', 'JOU').
card_image_name('returned reveler'/'JOU', 'returned reveler').
card_uid('returned reveler'/'JOU', 'JOU:Returned Reveler:returned reveler').
card_rarity('returned reveler'/'JOU', 'Common').
card_artist('returned reveler'/'JOU', 'Allen Williams').
card_number('returned reveler'/'JOU', '79').
card_flavor_text('returned reveler'/'JOU', 'The flesh is dead and the life forgotten, but old habits persist.').
card_multiverse_id('returned reveler'/'JOU', '380481').

card_in_set('revel of the fallen god', 'JOU').
card_original_type('revel of the fallen god'/'JOU', 'Sorcery').
card_original_text('revel of the fallen god'/'JOU', 'Put four 2/2 red and green Satyr creature tokens with haste onto the battlefield.').
card_first_print('revel of the fallen god', 'JOU').
card_image_name('revel of the fallen god'/'JOU', 'revel of the fallen god').
card_uid('revel of the fallen god'/'JOU', 'JOU:Revel of the Fallen God:revel of the fallen god').
card_rarity('revel of the fallen god'/'JOU', 'Rare').
card_artist('revel of the fallen god'/'JOU', 'Adam Paquette').
card_number('revel of the fallen god'/'JOU', '155').
card_flavor_text('revel of the fallen god'/'JOU', '\"Xenagos has fallen, and the world grows dark without him. What can ever fill that darkness but our rage?\"\n—Guruthes, disciple of Xenagos').
card_multiverse_id('revel of the fallen god'/'JOU', '380482').

card_in_set('reviving melody', 'JOU').
card_original_type('reviving melody'/'JOU', 'Sorcery').
card_original_text('reviving melody'/'JOU', 'Choose one or both — Return target creature card from your graveyard to your hand; and/or return target enchantment card from your graveyard to your hand.').
card_first_print('reviving melody', 'JOU').
card_image_name('reviving melody'/'JOU', 'reviving melody').
card_uid('reviving melody'/'JOU', 'JOU:Reviving Melody:reviving melody').
card_rarity('reviving melody'/'JOU', 'Uncommon').
card_artist('reviving melody'/'JOU', 'Cynthia Sheppard').
card_number('reviving melody'/'JOU', '138').
card_flavor_text('reviving melody'/'JOU', '\"Listen to my song, and you need never forge a golden mask to return to me.\"').
card_multiverse_id('reviving melody'/'JOU', '380483').

card_in_set('riddle of lightning', 'JOU').
card_original_type('riddle of lightning'/'JOU', 'Instant').
card_original_text('riddle of lightning'/'JOU', 'Choose target creature or player. Scry 3, then reveal the top card of your library. Riddle of Lightning deals damage equal to that card\'s converted mana cost to that creature or player. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('riddle of lightning'/'JOU', 'riddle of lightning').
card_uid('riddle of lightning'/'JOU', 'JOU:Riddle of Lightning:riddle of lightning').
card_rarity('riddle of lightning'/'JOU', 'Uncommon').
card_artist('riddle of lightning'/'JOU', 'Daarken').
card_number('riddle of lightning'/'JOU', '107').
card_multiverse_id('riddle of lightning'/'JOU', '380484').

card_in_set('riptide chimera', 'JOU').
card_original_type('riptide chimera'/'JOU', 'Enchantment Creature — Chimera').
card_original_text('riptide chimera'/'JOU', 'Flying\nAt the beginning of your upkeep, return an enchantment you control to its owner\'s hand.').
card_first_print('riptide chimera', 'JOU').
card_image_name('riptide chimera'/'JOU', 'riptide chimera').
card_uid('riptide chimera'/'JOU', 'JOU:Riptide Chimera:riptide chimera').
card_rarity('riptide chimera'/'JOU', 'Uncommon').
card_artist('riptide chimera'/'JOU', 'Ryan Barger').
card_number('riptide chimera'/'JOU', '48').
card_flavor_text('riptide chimera'/'JOU', '\"I want one.\"\n—Kiora').
card_multiverse_id('riptide chimera'/'JOU', '380485').

card_in_set('rise of eagles', 'JOU').
card_original_type('rise of eagles'/'JOU', 'Sorcery').
card_original_text('rise of eagles'/'JOU', 'Put two 2/2 blue Bird enchantment creature tokens with flying onto the battlefield. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('rise of eagles', 'JOU').
card_image_name('rise of eagles'/'JOU', 'rise of eagles').
card_uid('rise of eagles'/'JOU', 'JOU:Rise of Eagles:rise of eagles').
card_rarity('rise of eagles'/'JOU', 'Common').
card_artist('rise of eagles'/'JOU', 'Noah Bradley').
card_number('rise of eagles'/'JOU', '49').
card_flavor_text('rise of eagles'/'JOU', 'An offering to the sea brings an insight from the stars.').
card_multiverse_id('rise of eagles'/'JOU', '380486').

card_in_set('ritual of the returned', 'JOU').
card_original_type('ritual of the returned'/'JOU', 'Instant').
card_original_text('ritual of the returned'/'JOU', 'Exile target creature card from your graveyard. Put a black Zombie creature token onto the battlefield. Its power is equal to that card\'s power and its toughness is equal to that card\'s toughness.').
card_first_print('ritual of the returned', 'JOU').
card_image_name('ritual of the returned'/'JOU', 'ritual of the returned').
card_uid('ritual of the returned'/'JOU', 'JOU:Ritual of the Returned:ritual of the returned').
card_rarity('ritual of the returned'/'JOU', 'Uncommon').
card_artist('ritual of the returned'/'JOU', 'Zack Stella').
card_number('ritual of the returned'/'JOU', '80').
card_flavor_text('ritual of the returned'/'JOU', 'A mask forged to avenge a death forgotten.').
card_multiverse_id('ritual of the returned'/'JOU', '380487').

card_in_set('rollick of abandon', 'JOU').
card_original_type('rollick of abandon'/'JOU', 'Sorcery').
card_original_text('rollick of abandon'/'JOU', 'All creatures get +2/-2 until end of turn.').
card_first_print('rollick of abandon', 'JOU').
card_image_name('rollick of abandon'/'JOU', 'rollick of abandon').
card_uid('rollick of abandon'/'JOU', 'JOU:Rollick of Abandon:rollick of abandon').
card_rarity('rollick of abandon'/'JOU', 'Uncommon').
card_artist('rollick of abandon'/'JOU', 'Mathias Kollros').
card_number('rollick of abandon'/'JOU', '108').
card_flavor_text('rollick of abandon'/'JOU', '\"Embrace every moment, for it could be your last.\"\n—Xenagos, god of revels').
card_multiverse_id('rollick of abandon'/'JOU', '380488').

card_in_set('rotted hulk', 'JOU').
card_original_type('rotted hulk'/'JOU', 'Creature — Elemental').
card_original_text('rotted hulk'/'JOU', '').
card_first_print('rotted hulk', 'JOU').
card_image_name('rotted hulk'/'JOU', 'rotted hulk').
card_uid('rotted hulk'/'JOU', 'JOU:Rotted Hulk:rotted hulk').
card_rarity('rotted hulk'/'JOU', 'Common').
card_artist('rotted hulk'/'JOU', 'Raymond Swanland').
card_number('rotted hulk'/'JOU', '81').
card_flavor_text('rotted hulk'/'JOU', 'The hulk rose from the sea and loomed over the Champion. Pinned beneath the twisting, rotted planks of wood was the body of Kaliaros, the helmsman of her former crew, and beside him the captain, Photine.\n—The Theriad').
card_multiverse_id('rotted hulk'/'JOU', '380489').

card_in_set('rouse the mob', 'JOU').
card_original_type('rouse the mob'/'JOU', 'Instant').
card_original_text('rouse the mob'/'JOU', 'Strive — Rouse the Mob costs {2}{R} more to cast for each target beyond the first.\nAny number of target creatures each get +2/+0 and gain trample until end of turn.').
card_first_print('rouse the mob', 'JOU').
card_image_name('rouse the mob'/'JOU', 'rouse the mob').
card_uid('rouse the mob'/'JOU', 'JOU:Rouse the Mob:rouse the mob').
card_rarity('rouse the mob'/'JOU', 'Common').
card_artist('rouse the mob'/'JOU', 'John Stanko').
card_number('rouse the mob'/'JOU', '109').
card_multiverse_id('rouse the mob'/'JOU', '380490').

card_in_set('sage of hours', 'JOU').
card_original_type('sage of hours'/'JOU', 'Creature — Human Wizard').
card_original_text('sage of hours'/'JOU', 'Heroic — Whenever you cast a spell that targets Sage of Hours, put a +1/+1 counter on it.\nRemove all +1/+1 counters from Sage of Hours: For each five counters removed this way, take an extra turn after this one.').
card_first_print('sage of hours', 'JOU').
card_image_name('sage of hours'/'JOU', 'sage of hours').
card_uid('sage of hours'/'JOU', 'JOU:Sage of Hours:sage of hours').
card_rarity('sage of hours'/'JOU', 'Mythic Rare').
card_artist('sage of hours'/'JOU', 'Matt Stewart').
card_number('sage of hours'/'JOU', '50').
card_multiverse_id('sage of hours'/'JOU', '380491').

card_in_set('satyr grovedancer', 'JOU').
card_original_type('satyr grovedancer'/'JOU', 'Creature — Satyr Shaman').
card_original_text('satyr grovedancer'/'JOU', 'When Satyr Grovedancer enters the battlefield, put a +1/+1 counter on target creature.').
card_first_print('satyr grovedancer', 'JOU').
card_image_name('satyr grovedancer'/'JOU', 'satyr grovedancer').
card_uid('satyr grovedancer'/'JOU', 'JOU:Satyr Grovedancer:satyr grovedancer').
card_rarity('satyr grovedancer'/'JOU', 'Common').
card_artist('satyr grovedancer'/'JOU', 'Jason A. Engle').
card_number('satyr grovedancer'/'JOU', '139').
card_flavor_text('satyr grovedancer'/'JOU', '\"Some of my kin dance for themselves, without greater purpose. Nylea gives me purpose, and with it strength.\"').
card_multiverse_id('satyr grovedancer'/'JOU', '380492').

card_in_set('satyr hoplite', 'JOU').
card_original_type('satyr hoplite'/'JOU', 'Creature — Satyr Soldier').
card_original_text('satyr hoplite'/'JOU', 'Heroic — Whenever you cast a spell that targets Satyr Hoplite, put a +1/+1 counter on Satyr Hoplite.').
card_first_print('satyr hoplite', 'JOU').
card_image_name('satyr hoplite'/'JOU', 'satyr hoplite').
card_uid('satyr hoplite'/'JOU', 'JOU:Satyr Hoplite:satyr hoplite').
card_rarity('satyr hoplite'/'JOU', 'Common').
card_artist('satyr hoplite'/'JOU', 'Mark Zug').
card_number('satyr hoplite'/'JOU', '110').
card_flavor_text('satyr hoplite'/'JOU', '\"Xenagos has become what he once despised: a tyrant and an oppressor.\"').
card_multiverse_id('satyr hoplite'/'JOU', '380493').

card_in_set('scourge of fleets', 'JOU').
card_original_type('scourge of fleets'/'JOU', 'Creature — Kraken').
card_original_text('scourge of fleets'/'JOU', 'When Scourge of Fleets enters the battlefield, return each creature your opponents control with toughness X or less to its owner\'s hand, where X is the number of Islands you control.').
card_image_name('scourge of fleets'/'JOU', 'scourge of fleets').
card_uid('scourge of fleets'/'JOU', 'JOU:Scourge of Fleets:scourge of fleets').
card_rarity('scourge of fleets'/'JOU', 'Rare').
card_artist('scourge of fleets'/'JOU', 'Steven Belledin').
card_number('scourge of fleets'/'JOU', '51').
card_multiverse_id('scourge of fleets'/'JOU', '380494').

card_in_set('setessan tactics', 'JOU').
card_original_type('setessan tactics'/'JOU', 'Instant').
card_original_text('setessan tactics'/'JOU', 'Strive — Setessan Tactics costs {G} more to cast for each target beyond the first.\nUntil end of turn, any number of target creatures each get +1/+1 and gain \"{T}: This creature fights another target creature.\"').
card_first_print('setessan tactics', 'JOU').
card_image_name('setessan tactics'/'JOU', 'setessan tactics').
card_uid('setessan tactics'/'JOU', 'JOU:Setessan Tactics:setessan tactics').
card_rarity('setessan tactics'/'JOU', 'Rare').
card_artist('setessan tactics'/'JOU', 'Mark Winters').
card_number('setessan tactics'/'JOU', '140').
card_multiverse_id('setessan tactics'/'JOU', '380495').

card_in_set('sightless brawler', 'JOU').
card_original_type('sightless brawler'/'JOU', 'Enchantment Creature — Human Warrior').
card_original_text('sightless brawler'/'JOU', 'Bestow {4}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nSightless Brawler can\'t attack alone.\nEnchanted creature gets +3/+2 and can\'t attack alone.').
card_first_print('sightless brawler', 'JOU').
card_image_name('sightless brawler'/'JOU', 'sightless brawler').
card_uid('sightless brawler'/'JOU', 'JOU:Sightless Brawler:sightless brawler').
card_rarity('sightless brawler'/'JOU', 'Uncommon').
card_artist('sightless brawler'/'JOU', 'Johann Bodin').
card_number('sightless brawler'/'JOU', '24').
card_multiverse_id('sightless brawler'/'JOU', '380496').

card_in_set('sigiled skink', 'JOU').
card_original_type('sigiled skink'/'JOU', 'Creature — Lizard').
card_original_text('sigiled skink'/'JOU', 'Whenever Sigiled Skink attacks, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('sigiled skink', 'JOU').
card_image_name('sigiled skink'/'JOU', 'sigiled skink').
card_uid('sigiled skink'/'JOU', 'JOU:Sigiled Skink:sigiled skink').
card_rarity('sigiled skink'/'JOU', 'Common').
card_artist('sigiled skink'/'JOU', 'Yeong-Hao Han').
card_number('sigiled skink'/'JOU', '111').
card_flavor_text('sigiled skink'/'JOU', 'The runes seem to come alive as it moves, rippling like slow flames across its scales.').
card_multiverse_id('sigiled skink'/'JOU', '380497').

card_in_set('sigiled starfish', 'JOU').
card_original_type('sigiled starfish'/'JOU', 'Creature — Starfish').
card_original_text('sigiled starfish'/'JOU', '{T}: Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('sigiled starfish', 'JOU').
card_image_name('sigiled starfish'/'JOU', 'sigiled starfish').
card_uid('sigiled starfish'/'JOU', 'JOU:Sigiled Starfish:sigiled starfish').
card_rarity('sigiled starfish'/'JOU', 'Common').
card_artist('sigiled starfish'/'JOU', 'Nils Hamm').
card_number('sigiled starfish'/'JOU', '52').
card_flavor_text('sigiled starfish'/'JOU', 'Kruphix hid the most dire prophecies about humankind where humans would never find them and tritons wouldn\'t care to read them.').
card_multiverse_id('sigiled starfish'/'JOU', '380498').

card_in_set('silence the believers', 'JOU').
card_original_type('silence the believers'/'JOU', 'Instant').
card_original_text('silence the believers'/'JOU', 'Strive — Silence the Believers costs {2}{B} more to cast for each target beyond the first.\nExile any number of target creatures and all Auras attached to them.').
card_first_print('silence the believers', 'JOU').
card_image_name('silence the believers'/'JOU', 'silence the believers').
card_uid('silence the believers'/'JOU', 'JOU:Silence the Believers:silence the believers').
card_rarity('silence the believers'/'JOU', 'Rare').
card_artist('silence the believers'/'JOU', 'Slawomir Maniak').
card_number('silence the believers'/'JOU', '82').
card_multiverse_id('silence the believers'/'JOU', '380499').

card_in_set('skybind', 'JOU').
card_original_type('skybind'/'JOU', 'Enchantment').
card_original_text('skybind'/'JOU', 'Constellation — Whenever Skybind or another enchantment enters the battlefield under your control, exile target nonenchantment permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_first_print('skybind', 'JOU').
card_image_name('skybind'/'JOU', 'skybind').
card_uid('skybind'/'JOU', 'JOU:Skybind:skybind').
card_rarity('skybind'/'JOU', 'Rare').
card_artist('skybind'/'JOU', 'Igor Kieryluk').
card_number('skybind'/'JOU', '25').
card_multiverse_id('skybind'/'JOU', '380500').

card_in_set('skyspear cavalry', 'JOU').
card_original_type('skyspear cavalry'/'JOU', 'Creature — Human Soldier').
card_original_text('skyspear cavalry'/'JOU', 'Flying\nDouble strike (This creature deals both first-strike and regular combat damage.)').
card_first_print('skyspear cavalry', 'JOU').
card_image_name('skyspear cavalry'/'JOU', 'skyspear cavalry').
card_uid('skyspear cavalry'/'JOU', 'JOU:Skyspear Cavalry:skyspear cavalry').
card_rarity('skyspear cavalry'/'JOU', 'Uncommon').
card_artist('skyspear cavalry'/'JOU', 'Wayne Reynolds').
card_number('skyspear cavalry'/'JOU', '26').
card_flavor_text('skyspear cavalry'/'JOU', 'Pray her spear reaches you first. Her griffin is less kind.').
card_multiverse_id('skyspear cavalry'/'JOU', '380501').

card_in_set('solidarity of heroes', 'JOU').
card_original_type('solidarity of heroes'/'JOU', 'Instant').
card_original_text('solidarity of heroes'/'JOU', 'Strive — Solidarity of Heroes costs {1}{G} more to cast for each target beyond the first.\nChoose any number of target creatures. Double the number of +1/+1 counters on each of them.').
card_first_print('solidarity of heroes', 'JOU').
card_image_name('solidarity of heroes'/'JOU', 'solidarity of heroes').
card_uid('solidarity of heroes'/'JOU', 'JOU:Solidarity of Heroes:solidarity of heroes').
card_rarity('solidarity of heroes'/'JOU', 'Uncommon').
card_artist('solidarity of heroes'/'JOU', 'Eric Deschamps').
card_number('solidarity of heroes'/'JOU', '141').
card_multiverse_id('solidarity of heroes'/'JOU', '380502').

card_in_set('spawn of thraxes', 'JOU').
card_original_type('spawn of thraxes'/'JOU', 'Creature — Dragon').
card_original_text('spawn of thraxes'/'JOU', 'Flying\nWhen Spawn of Thraxes enters the battlefield, it deals damage to target creature or player equal to the number of Mountains you control.').
card_image_name('spawn of thraxes'/'JOU', 'spawn of thraxes').
card_uid('spawn of thraxes'/'JOU', 'JOU:Spawn of Thraxes:spawn of thraxes').
card_rarity('spawn of thraxes'/'JOU', 'Rare').
card_artist('spawn of thraxes'/'JOU', 'Svetlin Velinov').
card_number('spawn of thraxes'/'JOU', '112').
card_flavor_text('spawn of thraxes'/'JOU', 'Sparks from Purphoros\'s forge fill the belly of every dragon.').
card_multiverse_id('spawn of thraxes'/'JOU', '380503').

card_in_set('spirespine', 'JOU').
card_original_type('spirespine'/'JOU', 'Enchantment Creature — Beast').
card_original_text('spirespine'/'JOU', 'Bestow {4}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nSpirespine blocks each turn if able.\nEnchanted creature gets +4/+1 and blocks each turn if able.').
card_first_print('spirespine', 'JOU').
card_image_name('spirespine'/'JOU', 'spirespine').
card_uid('spirespine'/'JOU', 'JOU:Spirespine:spirespine').
card_rarity('spirespine'/'JOU', 'Uncommon').
card_artist('spirespine'/'JOU', 'Sam Burley').
card_number('spirespine'/'JOU', '142').
card_multiverse_id('spirespine'/'JOU', '380504').

card_in_set('spite of mogis', 'JOU').
card_original_type('spite of mogis'/'JOU', 'Sorcery').
card_original_text('spite of mogis'/'JOU', 'Spite of Mogis deals damage to target creature equal to the number of instant and sorcery cards in your graveyard. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('spite of mogis', 'JOU').
card_image_name('spite of mogis'/'JOU', 'spite of mogis').
card_uid('spite of mogis'/'JOU', 'JOU:Spite of Mogis:spite of mogis').
card_rarity('spite of mogis'/'JOU', 'Uncommon').
card_artist('spite of mogis'/'JOU', 'Vincent Proce').
card_number('spite of mogis'/'JOU', '113').
card_multiverse_id('spite of mogis'/'JOU', '380505').

card_in_set('spiteful blow', 'JOU').
card_original_type('spiteful blow'/'JOU', 'Sorcery').
card_original_text('spiteful blow'/'JOU', 'Destroy target creature and target land.').
card_first_print('spiteful blow', 'JOU').
card_image_name('spiteful blow'/'JOU', 'spiteful blow').
card_uid('spiteful blow'/'JOU', 'JOU:Spiteful Blow:spiteful blow').
card_rarity('spiteful blow'/'JOU', 'Uncommon').
card_artist('spiteful blow'/'JOU', 'Jason Chan').
card_number('spiteful blow'/'JOU', '83').
card_flavor_text('spiteful blow'/'JOU', '\"Only a fool takes Phenax at his word. What do you expect from a god born of lies and betrayal?\"\n—Verenes, priest of Iroas').
card_multiverse_id('spiteful blow'/'JOU', '380506').

card_in_set('squelching leeches', 'JOU').
card_original_type('squelching leeches'/'JOU', 'Creature — Leech').
card_original_text('squelching leeches'/'JOU', 'Squelching Leeches\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('squelching leeches'/'JOU', 'squelching leeches').
card_uid('squelching leeches'/'JOU', 'JOU:Squelching Leeches:squelching leeches').
card_rarity('squelching leeches'/'JOU', 'Uncommon').
card_artist('squelching leeches'/'JOU', 'Svetlin Velinov').
card_number('squelching leeches'/'JOU', '84').
card_flavor_text('squelching leeches'/'JOU', 'Leeches are sacred to followers of Pharika for drawing poison from a wound, but feared by everyone else for drawing blood from the flesh.').
card_multiverse_id('squelching leeches'/'JOU', '380507').

card_in_set('starfall', 'JOU').
card_original_type('starfall'/'JOU', 'Instant').
card_original_text('starfall'/'JOU', 'Starfall deals 3 damage to target creature. If that creature is an enchantment, Starfall deals 3 damage to that creature\'s controller.').
card_first_print('starfall', 'JOU').
card_image_name('starfall'/'JOU', 'starfall').
card_uid('starfall'/'JOU', 'JOU:Starfall:starfall').
card_rarity('starfall'/'JOU', 'Common').
card_artist('starfall'/'JOU', 'Chris Rahn').
card_number('starfall'/'JOU', '114').
card_flavor_text('starfall'/'JOU', '\"The most brilliant stars are not born of Nyx.\"\n—Rhexenor, Akroan flamespeaker').
card_multiverse_id('starfall'/'JOU', '380508').

card_in_set('stonewise fortifier', 'JOU').
card_original_type('stonewise fortifier'/'JOU', 'Creature — Human Wizard').
card_original_text('stonewise fortifier'/'JOU', '{4}{W}: Prevent all damage that would be dealt to Stonewise Fortifier by target creature this turn.').
card_first_print('stonewise fortifier', 'JOU').
card_image_name('stonewise fortifier'/'JOU', 'stonewise fortifier').
card_uid('stonewise fortifier'/'JOU', 'JOU:Stonewise Fortifier:stonewise fortifier').
card_rarity('stonewise fortifier'/'JOU', 'Common').
card_artist('stonewise fortifier'/'JOU', 'Ryan Alexander Lee').
card_number('stonewise fortifier'/'JOU', '27').
card_flavor_text('stonewise fortifier'/'JOU', 'Ephara taught some mortals to feel every vein in marble as though it flows with their own blood.').
card_multiverse_id('stonewise fortifier'/'JOU', '380509').

card_in_set('stormchaser chimera', 'JOU').
card_original_type('stormchaser chimera'/'JOU', 'Creature — Chimera').
card_original_text('stormchaser chimera'/'JOU', 'Flying\n{2}{U}{R}: Scry 1, then reveal the top card of your library. Stormchaser Chimera gets +X/+0 until end of turn, where X is that card\'s converted mana cost. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_first_print('stormchaser chimera', 'JOU').
card_image_name('stormchaser chimera'/'JOU', 'stormchaser chimera').
card_uid('stormchaser chimera'/'JOU', 'JOU:Stormchaser Chimera:stormchaser chimera').
card_rarity('stormchaser chimera'/'JOU', 'Uncommon').
card_artist('stormchaser chimera'/'JOU', 'Greg Staples').
card_number('stormchaser chimera'/'JOU', '156').
card_multiverse_id('stormchaser chimera'/'JOU', '380510').

card_in_set('strength from the fallen', 'JOU').
card_original_type('strength from the fallen'/'JOU', 'Enchantment').
card_original_text('strength from the fallen'/'JOU', 'Constellation — Whenever Strength from the Fallen or another enchantment enters the battlefield under your control, target creature gets +X/+X until end of turn, where X is the number of creature cards in your graveyard.').
card_first_print('strength from the fallen', 'JOU').
card_image_name('strength from the fallen'/'JOU', 'strength from the fallen').
card_uid('strength from the fallen'/'JOU', 'JOU:Strength from the Fallen:strength from the fallen').
card_rarity('strength from the fallen'/'JOU', 'Uncommon').
card_artist('strength from the fallen'/'JOU', 'Min Yum').
card_number('strength from the fallen'/'JOU', '143').
card_multiverse_id('strength from the fallen'/'JOU', '380511').

card_in_set('supply-line cranes', 'JOU').
card_original_type('supply-line cranes'/'JOU', 'Creature — Bird').
card_original_text('supply-line cranes'/'JOU', 'Flying\nWhen Supply-Line Cranes enters the battlefield, put a +1/+1 counter on target creature.').
card_first_print('supply-line cranes', 'JOU').
card_image_name('supply-line cranes'/'JOU', 'supply-line cranes').
card_uid('supply-line cranes'/'JOU', 'JOU:Supply-Line Cranes:supply-line cranes').
card_rarity('supply-line cranes'/'JOU', 'Common').
card_artist('supply-line cranes'/'JOU', 'Daniel Ljunggren').
card_number('supply-line cranes'/'JOU', '28').
card_flavor_text('supply-line cranes'/'JOU', 'They are trained from hatching to deliver aid where it is needed most.').
card_multiverse_id('supply-line cranes'/'JOU', '380512').

card_in_set('swarmborn giant', 'JOU').
card_original_type('swarmborn giant'/'JOU', 'Creature — Giant').
card_original_text('swarmborn giant'/'JOU', 'When you\'re dealt combat damage, sacrifice Swarmborn Giant.\n{4}{G}{G}: Monstrosity 2. (If this creature isn\'t monstrous, put two +1/+1 counters on it and it becomes monstrous.)\nAs long as Swarmborn Giant is monstrous, it has reach.').
card_first_print('swarmborn giant', 'JOU').
card_image_name('swarmborn giant'/'JOU', 'swarmborn giant').
card_uid('swarmborn giant'/'JOU', 'JOU:Swarmborn Giant:swarmborn giant').
card_rarity('swarmborn giant'/'JOU', 'Uncommon').
card_artist('swarmborn giant'/'JOU', 'Jaime Jones').
card_number('swarmborn giant'/'JOU', '144').
card_multiverse_id('swarmborn giant'/'JOU', '380513').

card_in_set('temple of epiphany', 'JOU').
card_original_type('temple of epiphany'/'JOU', 'Land').
card_original_text('temple of epiphany'/'JOU', 'Temple of Epiphany enters the battlefield tapped.\nWhen Temple of Epiphany enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {U} or {R} to your mana pool.').
card_first_print('temple of epiphany', 'JOU').
card_image_name('temple of epiphany'/'JOU', 'temple of epiphany').
card_uid('temple of epiphany'/'JOU', 'JOU:Temple of Epiphany:temple of epiphany').
card_rarity('temple of epiphany'/'JOU', 'Rare').
card_artist('temple of epiphany'/'JOU', 'Noah Bradley').
card_number('temple of epiphany'/'JOU', '164').
card_multiverse_id('temple of epiphany'/'JOU', '380514').

card_in_set('temple of malady', 'JOU').
card_original_type('temple of malady'/'JOU', 'Land').
card_original_text('temple of malady'/'JOU', 'Temple of Malady enters the battlefield tapped.\nWhen Temple of Malady enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {B} or {G} to your mana pool.').
card_first_print('temple of malady', 'JOU').
card_image_name('temple of malady'/'JOU', 'temple of malady').
card_uid('temple of malady'/'JOU', 'JOU:Temple of Malady:temple of malady').
card_rarity('temple of malady'/'JOU', 'Rare').
card_artist('temple of malady'/'JOU', 'James Paick').
card_number('temple of malady'/'JOU', '165').
card_multiverse_id('temple of malady'/'JOU', '380515').

card_in_set('tethmos high priest', 'JOU').
card_original_type('tethmos high priest'/'JOU', 'Creature — Cat Cleric').
card_original_text('tethmos high priest'/'JOU', 'Heroic — Whenever you cast a spell that targets Tethmos High Priest, return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.').
card_first_print('tethmos high priest', 'JOU').
card_image_name('tethmos high priest'/'JOU', 'tethmos high priest').
card_uid('tethmos high priest'/'JOU', 'JOU:Tethmos High Priest:tethmos high priest').
card_rarity('tethmos high priest'/'JOU', 'Uncommon').
card_artist('tethmos high priest'/'JOU', 'Mark Zug').
card_number('tethmos high priest'/'JOU', '29').
card_flavor_text('tethmos high priest'/'JOU', '\"Death is tyranny. Like all tyranny, it must be opposed.\"').
card_multiverse_id('tethmos high priest'/'JOU', '380516').

card_in_set('thassa\'s devourer', 'JOU').
card_original_type('thassa\'s devourer'/'JOU', 'Enchantment Creature — Elemental').
card_original_text('thassa\'s devourer'/'JOU', 'Constellation — Whenever Thassa\'s Devourer or another enchantment enters the battlefield under your control, target player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('thassa\'s devourer', 'JOU').
card_image_name('thassa\'s devourer'/'JOU', 'thassa\'s devourer').
card_uid('thassa\'s devourer'/'JOU', 'JOU:Thassa\'s Devourer:thassa\'s devourer').
card_rarity('thassa\'s devourer'/'JOU', 'Common').
card_artist('thassa\'s devourer'/'JOU', 'David Palumbo').
card_number('thassa\'s devourer'/'JOU', '53').
card_flavor_text('thassa\'s devourer'/'JOU', 'When mortals claim there are places Thassa cannot reach, the sea god laughs.').
card_multiverse_id('thassa\'s devourer'/'JOU', '380517').

card_in_set('thassa\'s ire', 'JOU').
card_original_type('thassa\'s ire'/'JOU', 'Enchantment').
card_original_text('thassa\'s ire'/'JOU', '{3}{U}: You may tap or untap target creature.').
card_first_print('thassa\'s ire', 'JOU').
card_image_name('thassa\'s ire'/'JOU', 'thassa\'s ire').
card_uid('thassa\'s ire'/'JOU', 'JOU:Thassa\'s Ire:thassa\'s ire').
card_rarity('thassa\'s ire'/'JOU', 'Uncommon').
card_artist('thassa\'s ire'/'JOU', 'Chris Rahn').
card_number('thassa\'s ire'/'JOU', '54').
card_flavor_text('thassa\'s ire'/'JOU', 'The sailor had never seen a god before. Now she had gazed upon Thassa. Shortly she would see Athreos, and then she would meet Erebos.').
card_multiverse_id('thassa\'s ire'/'JOU', '380518').

card_in_set('thoughtrender lamia', 'JOU').
card_original_type('thoughtrender lamia'/'JOU', 'Enchantment Creature — Lamia').
card_original_text('thoughtrender lamia'/'JOU', 'Constellation — Whenever Thoughtrender Lamia or another enchantment enters the battlefield under your control, each opponent discards a card.').
card_first_print('thoughtrender lamia', 'JOU').
card_image_name('thoughtrender lamia'/'JOU', 'thoughtrender lamia').
card_uid('thoughtrender lamia'/'JOU', 'JOU:Thoughtrender Lamia:thoughtrender lamia').
card_rarity('thoughtrender lamia'/'JOU', 'Uncommon').
card_artist('thoughtrender lamia'/'JOU', 'Magali Villeneuve').
card_number('thoughtrender lamia'/'JOU', '85').
card_flavor_text('thoughtrender lamia'/'JOU', 'Some predators can sense fear in their prey, but the lamia is drawn to madness.').
card_multiverse_id('thoughtrender lamia'/'JOU', '380519').

card_in_set('tormented thoughts', 'JOU').
card_original_type('tormented thoughts'/'JOU', 'Sorcery').
card_original_text('tormented thoughts'/'JOU', 'As an additional cost to cast Tormented Thoughts, sacrifice a creature.\nTarget player discards a number of cards equal to the sacrificed creature\'s power.').
card_first_print('tormented thoughts', 'JOU').
card_image_name('tormented thoughts'/'JOU', 'tormented thoughts').
card_uid('tormented thoughts'/'JOU', 'JOU:Tormented Thoughts:tormented thoughts').
card_rarity('tormented thoughts'/'JOU', 'Uncommon').
card_artist('tormented thoughts'/'JOU', 'Allen Williams').
card_number('tormented thoughts'/'JOU', '86').
card_flavor_text('tormented thoughts'/'JOU', '\"Not all nightmares can be escaped by waking.\"\n—Ashiok, Nightmare Weaver').
card_multiverse_id('tormented thoughts'/'JOU', '380520').

card_in_set('triton cavalry', 'JOU').
card_original_type('triton cavalry'/'JOU', 'Creature — Merfolk Soldier').
card_original_text('triton cavalry'/'JOU', 'Heroic — Whenever you cast a spell that targets Triton Cavalry, you may return target enchantment to its owner\'s hand.').
card_first_print('triton cavalry', 'JOU').
card_image_name('triton cavalry'/'JOU', 'triton cavalry').
card_uid('triton cavalry'/'JOU', 'JOU:Triton Cavalry:triton cavalry').
card_rarity('triton cavalry'/'JOU', 'Uncommon').
card_artist('triton cavalry'/'JOU', 'Kev Walker').
card_number('triton cavalry'/'JOU', '55').
card_flavor_text('triton cavalry'/'JOU', 'The tritons broke the surface, and the Nyxborn broke ranks.').
card_multiverse_id('triton cavalry'/'JOU', '380521').

card_in_set('triton shorestalker', 'JOU').
card_original_type('triton shorestalker'/'JOU', 'Creature — Merfolk Rogue').
card_original_text('triton shorestalker'/'JOU', 'Triton Shorestalker can\'t be blocked.').
card_first_print('triton shorestalker', 'JOU').
card_image_name('triton shorestalker'/'JOU', 'triton shorestalker').
card_uid('triton shorestalker'/'JOU', 'JOU:Triton Shorestalker:triton shorestalker').
card_rarity('triton shorestalker'/'JOU', 'Common').
card_artist('triton shorestalker'/'JOU', 'Svetlin Velinov').
card_number('triton shorestalker'/'JOU', '56').
card_flavor_text('triton shorestalker'/'JOU', '\"Who will miss you, drywalker? A wife? A child? Perhaps they will blame the sea for your fate, and teach future generations to stay far away from it.\"').
card_multiverse_id('triton shorestalker'/'JOU', '380522').

card_in_set('twinflame', 'JOU').
card_original_type('twinflame'/'JOU', 'Sorcery').
card_original_text('twinflame'/'JOU', 'Strive — Twinflame costs {2}{R} more to cast for each target beyond the first.\nChoose any number of target creatures you control. For each of them, put a token that\'s a copy of that creature onto the battlefield. Those tokens have haste. Exile them at the beginning of the next end step.').
card_first_print('twinflame', 'JOU').
card_image_name('twinflame'/'JOU', 'twinflame').
card_uid('twinflame'/'JOU', 'JOU:Twinflame:twinflame').
card_rarity('twinflame'/'JOU', 'Rare').
card_artist('twinflame'/'JOU', 'Chase Stone').
card_number('twinflame'/'JOU', '115').
card_multiverse_id('twinflame'/'JOU', '380523').

card_in_set('underworld coinsmith', 'JOU').
card_original_type('underworld coinsmith'/'JOU', 'Enchantment Creature — Human Cleric').
card_original_text('underworld coinsmith'/'JOU', 'Constellation — Whenever Underworld Coinsmith or another enchantment enters the battlefield under your control, you gain 1 life.\n{W}{B}, Pay 1 life: Each opponent loses 1 life.').
card_first_print('underworld coinsmith', 'JOU').
card_image_name('underworld coinsmith'/'JOU', 'underworld coinsmith').
card_uid('underworld coinsmith'/'JOU', 'JOU:Underworld Coinsmith:underworld coinsmith').
card_rarity('underworld coinsmith'/'JOU', 'Uncommon').
card_artist('underworld coinsmith'/'JOU', 'Mark Winters').
card_number('underworld coinsmith'/'JOU', '157').
card_flavor_text('underworld coinsmith'/'JOU', 'Coins of the Underworld are shaped from clay funerary masks.').
card_multiverse_id('underworld coinsmith'/'JOU', '380524').

card_in_set('war-wing siren', 'JOU').
card_original_type('war-wing siren'/'JOU', 'Creature — Siren Soldier').
card_original_text('war-wing siren'/'JOU', 'Flying\nHeroic — Whenever you cast a spell that targets War-Wing Siren, put a +1/+1 counter on War-Wing Siren.').
card_first_print('war-wing siren', 'JOU').
card_image_name('war-wing siren'/'JOU', 'war-wing siren').
card_uid('war-wing siren'/'JOU', 'JOU:War-Wing Siren:war-wing siren').
card_rarity('war-wing siren'/'JOU', 'Common').
card_artist('war-wing siren'/'JOU', 'Magali Villeneuve').
card_number('war-wing siren'/'JOU', '57').
card_flavor_text('war-wing siren'/'JOU', 'Once she sang sailors to their doom. Now she leads them to glory.').
card_multiverse_id('war-wing siren'/'JOU', '380525').

card_in_set('whitewater naiads', 'JOU').
card_original_type('whitewater naiads'/'JOU', 'Enchantment Creature — Nymph').
card_original_text('whitewater naiads'/'JOU', 'Constellation — Whenever Whitewater Naiads or another enchantment enters the battlefield under your control, target creature can\'t be blocked this turn.').
card_first_print('whitewater naiads', 'JOU').
card_image_name('whitewater naiads'/'JOU', 'whitewater naiads').
card_uid('whitewater naiads'/'JOU', 'JOU:Whitewater Naiads:whitewater naiads').
card_rarity('whitewater naiads'/'JOU', 'Uncommon').
card_artist('whitewater naiads'/'JOU', 'William Wu').
card_number('whitewater naiads'/'JOU', '58').
card_flavor_text('whitewater naiads'/'JOU', 'Stand against the crash of the river\'s rapids and you will know the power of the naiads.').
card_multiverse_id('whitewater naiads'/'JOU', '380526').

card_in_set('wildfire cerberus', 'JOU').
card_original_type('wildfire cerberus'/'JOU', 'Creature — Hound').
card_original_text('wildfire cerberus'/'JOU', '{5}{R}{R}: Monstrosity 1. (If this creature isn\'t monstrous, put a +1/+1 counter on it and it becomes monstrous.)\nWhen Wildfire Cerberus becomes monstrous, it deals 2 damage to each opponent and each creature your opponents control.').
card_first_print('wildfire cerberus', 'JOU').
card_image_name('wildfire cerberus'/'JOU', 'wildfire cerberus').
card_uid('wildfire cerberus'/'JOU', 'JOU:Wildfire Cerberus:wildfire cerberus').
card_rarity('wildfire cerberus'/'JOU', 'Uncommon').
card_artist('wildfire cerberus'/'JOU', 'Slawomir Maniak').
card_number('wildfire cerberus'/'JOU', '116').
card_multiverse_id('wildfire cerberus'/'JOU', '380527').

card_in_set('worst fears', 'JOU').
card_original_type('worst fears'/'JOU', 'Sorcery').
card_original_text('worst fears'/'JOU', 'You control target player during that player\'s next turn. Exile Worst Fears. (You see all cards that player could see and make all decisions for the player.)').
card_first_print('worst fears', 'JOU').
card_image_name('worst fears'/'JOU', 'worst fears').
card_uid('worst fears'/'JOU', 'JOU:Worst Fears:worst fears').
card_rarity('worst fears'/'JOU', 'Mythic Rare').
card_artist('worst fears'/'JOU', 'Eric Deschamps').
card_number('worst fears'/'JOU', '87').
card_flavor_text('worst fears'/'JOU', 'Elspeth feared that her trespass into Nyx would not go unanswered.').
card_multiverse_id('worst fears'/'JOU', '380528').
