% Apocalypse

set('APC').
set_name('APC', 'Apocalypse').
set_release_date('APC', '2001-06-04').
set_border('APC', 'black').
set_type('APC', 'expansion').
set_block('APC', 'Invasion').

card_in_set('æther mutation', 'APC').
card_original_type('æther mutation'/'APC', 'Sorcery').
card_original_text('æther mutation'/'APC', 'Return target creature to its owner\'s hand. Put X 1/1 green Saproling creature tokens into play, where X is its converted mana cost.').
card_first_print('æther mutation', 'APC').
card_image_name('æther mutation'/'APC', 'aether mutation').
card_uid('æther mutation'/'APC', 'APC:Æther Mutation:aether mutation').
card_rarity('æther mutation'/'APC', 'Uncommon').
card_artist('æther mutation'/'APC', 'Ron Spencer').
card_number('æther mutation'/'APC', '91').
card_multiverse_id('æther mutation'/'APC', '28669').

card_in_set('ana disciple', 'APC').
card_original_type('ana disciple'/'APC', 'Creature — Wizard').
card_original_text('ana disciple'/'APC', '{U}, {T}: Target creature gains flying until end of turn.\n{B}, {T}: Target creature gets -2/-0 until end of turn.').
card_first_print('ana disciple', 'APC').
card_image_name('ana disciple'/'APC', 'ana disciple').
card_uid('ana disciple'/'APC', 'APC:Ana Disciple:ana disciple').
card_rarity('ana disciple'/'APC', 'Common').
card_artist('ana disciple'/'APC', 'Darrell Riche').
card_number('ana disciple'/'APC', '73').
card_flavor_text('ana disciple'/'APC', '\"The blessings of Gaea alone are no longer enough.\"').
card_multiverse_id('ana disciple'/'APC', '25856').

card_in_set('ana sanctuary', 'APC').
card_original_type('ana sanctuary'/'APC', 'Enchantment').
card_original_text('ana sanctuary'/'APC', 'At the beginning of your upkeep, if you control a blue or black permanent, target creature gets +1/+1 until end of turn. If you control a blue permanent and a black permanent, that creature gets +5/+5 until end of turn instead.').
card_first_print('ana sanctuary', 'APC').
card_image_name('ana sanctuary'/'APC', 'ana sanctuary').
card_uid('ana sanctuary'/'APC', 'APC:Ana Sanctuary:ana sanctuary').
card_rarity('ana sanctuary'/'APC', 'Uncommon').
card_artist('ana sanctuary'/'APC', 'Rob Alexander').
card_number('ana sanctuary'/'APC', '74').
card_multiverse_id('ana sanctuary'/'APC', '26830').

card_in_set('anavolver', 'APC').
card_original_type('anavolver'/'APC', 'Creature — Volver').
card_original_text('anavolver'/'APC', 'Kicker {1}{U} and/or {B}\nIf you paid the {1}{U} kicker cost, Anavolver comes into play with two +1/+1 counters on it and has flying.\nIf you paid the {B} kicker cost, Anavolver comes into play with a +1/+1 counter on it and has \"Pay 3 life: Regenerate Anavolver.\"').
card_first_print('anavolver', 'APC').
card_image_name('anavolver'/'APC', 'anavolver').
card_uid('anavolver'/'APC', 'APC:Anavolver:anavolver').
card_rarity('anavolver'/'APC', 'Rare').
card_artist('anavolver'/'APC', 'Matt Cavotta').
card_number('anavolver'/'APC', '75').
card_multiverse_id('anavolver'/'APC', '25951').

card_in_set('angelfire crusader', 'APC').
card_original_type('angelfire crusader'/'APC', 'Creature — Soldier').
card_original_text('angelfire crusader'/'APC', '{R}: Angelfire Crusader gets +1/+0 until end of turn.').
card_first_print('angelfire crusader', 'APC').
card_image_name('angelfire crusader'/'APC', 'angelfire crusader').
card_uid('angelfire crusader'/'APC', 'APC:Angelfire Crusader:angelfire crusader').
card_rarity('angelfire crusader'/'APC', 'Common').
card_artist('angelfire crusader'/'APC', 'Edward P. Beard, Jr.').
card_number('angelfire crusader'/'APC', '1').
card_flavor_text('angelfire crusader'/'APC', 'Nothing burns hotter than the fires of holy rage.').
card_multiverse_id('angelfire crusader'/'APC', '21253').

card_in_set('battlefield forge', 'APC').
card_original_type('battlefield forge'/'APC', 'Land').
card_original_text('battlefield forge'/'APC', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Battlefield Forge deals 1 damage to you.').
card_first_print('battlefield forge', 'APC').
card_image_name('battlefield forge'/'APC', 'battlefield forge').
card_uid('battlefield forge'/'APC', 'APC:Battlefield Forge:battlefield forge').
card_rarity('battlefield forge'/'APC', 'Rare').
card_artist('battlefield forge'/'APC', 'Darrell Riche').
card_number('battlefield forge'/'APC', '139').
card_multiverse_id('battlefield forge'/'APC', '23247').

card_in_set('bloodfire colossus', 'APC').
card_original_type('bloodfire colossus'/'APC', 'Creature — Giant').
card_original_text('bloodfire colossus'/'APC', '{R}, Sacrifice Bloodfire Colossus: Bloodfire Colossus deals 6 damage to each creature and each player.').
card_first_print('bloodfire colossus', 'APC').
card_image_name('bloodfire colossus'/'APC', 'bloodfire colossus').
card_uid('bloodfire colossus'/'APC', 'APC:Bloodfire Colossus:bloodfire colossus').
card_rarity('bloodfire colossus'/'APC', 'Rare').
card_artist('bloodfire colossus'/'APC', 'Greg Staples').
card_number('bloodfire colossus'/'APC', '55').
card_flavor_text('bloodfire colossus'/'APC', 'It took all its strength to contain the fire within.').
card_multiverse_id('bloodfire colossus'/'APC', '28807').

card_in_set('bloodfire dwarf', 'APC').
card_original_type('bloodfire dwarf'/'APC', 'Creature — Dwarf').
card_original_text('bloodfire dwarf'/'APC', '{R}, Sacrifice Bloodfire Dwarf: Bloodfire Dwarf deals 1 damage to each creature without flying.').
card_first_print('bloodfire dwarf', 'APC').
card_image_name('bloodfire dwarf'/'APC', 'bloodfire dwarf').
card_uid('bloodfire dwarf'/'APC', 'APC:Bloodfire Dwarf:bloodfire dwarf').
card_rarity('bloodfire dwarf'/'APC', 'Common').
card_artist('bloodfire dwarf'/'APC', 'Ron Spencer').
card_number('bloodfire dwarf'/'APC', '56').
card_flavor_text('bloodfire dwarf'/'APC', 'For them, the only honorable death is one that leaves a crater.').
card_multiverse_id('bloodfire dwarf'/'APC', '26806').

card_in_set('bloodfire infusion', 'APC').
card_original_type('bloodfire infusion'/'APC', 'Enchant Creature').
card_original_text('bloodfire infusion'/'APC', 'Bloodfire Infusion can enchant only a creature you control.\n{R}, Sacrifice enchanted creature: Bloodfire Infusion deals damage equal to the enchanted creature\'s power to each creature.').
card_first_print('bloodfire infusion', 'APC').
card_image_name('bloodfire infusion'/'APC', 'bloodfire infusion').
card_uid('bloodfire infusion'/'APC', 'APC:Bloodfire Infusion:bloodfire infusion').
card_rarity('bloodfire infusion'/'APC', 'Common').
card_artist('bloodfire infusion'/'APC', 'Anthony S. Waters').
card_number('bloodfire infusion'/'APC', '57').
card_multiverse_id('bloodfire infusion'/'APC', '29303').

card_in_set('bloodfire kavu', 'APC').
card_original_type('bloodfire kavu'/'APC', 'Creature — Kavu').
card_original_text('bloodfire kavu'/'APC', '{R}, Sacrifice Bloodfire Kavu: Bloodfire Kavu deals 2 damage to each creature.').
card_first_print('bloodfire kavu', 'APC').
card_image_name('bloodfire kavu'/'APC', 'bloodfire kavu').
card_uid('bloodfire kavu'/'APC', 'APC:Bloodfire Kavu:bloodfire kavu').
card_rarity('bloodfire kavu'/'APC', 'Uncommon').
card_artist('bloodfire kavu'/'APC', 'Greg Staples').
card_number('bloodfire kavu'/'APC', '58').
card_flavor_text('bloodfire kavu'/'APC', 'Heart of fire, blood of lava, teeth of stone.').
card_multiverse_id('bloodfire kavu'/'APC', '28806').

card_in_set('bog gnarr', 'APC').
card_original_type('bog gnarr'/'APC', 'Creature — Beast').
card_original_text('bog gnarr'/'APC', 'Whenever a player plays a black spell, Bog Gnarr gets +2/+2 until end of turn.').
card_first_print('bog gnarr', 'APC').
card_image_name('bog gnarr'/'APC', 'bog gnarr').
card_uid('bog gnarr'/'APC', 'APC:Bog Gnarr:bog gnarr').
card_rarity('bog gnarr'/'APC', 'Common').
card_artist('bog gnarr'/'APC', 'Daren Bader').
card_number('bog gnarr'/'APC', '76').
card_flavor_text('bog gnarr'/'APC', 'Not all of Dominaria\'s defenders were nourished by the light.').
card_multiverse_id('bog gnarr'/'APC', '27153').

card_in_set('brass herald', 'APC').
card_original_type('brass herald'/'APC', 'Artifact Creature — Golem').
card_original_text('brass herald'/'APC', 'As Brass Herald comes into play, choose a creature type.\nWhen Brass Herald comes into play, reveal the top four cards of your library. Put all creature cards of the chosen type revealed this way into your hand and the rest on the bottom of your library.\nCreatures of the chosen type get +1/+1.').
card_first_print('brass herald', 'APC').
card_image_name('brass herald'/'APC', 'brass herald').
card_uid('brass herald'/'APC', 'APC:Brass Herald:brass herald').
card_rarity('brass herald'/'APC', 'Uncommon').
card_artist('brass herald'/'APC', 'Daren Bader').
card_number('brass herald'/'APC', '133').
card_multiverse_id('brass herald'/'APC', '27761').

card_in_set('captain\'s maneuver', 'APC').
card_original_type('captain\'s maneuver'/'APC', 'Instant').
card_original_text('captain\'s maneuver'/'APC', 'The next X damage that would be dealt to target creature or player this turn is dealt to another target creature or player instead.').
card_first_print('captain\'s maneuver', 'APC').
card_image_name('captain\'s maneuver'/'APC', 'captain\'s maneuver').
card_uid('captain\'s maneuver'/'APC', 'APC:Captain\'s Maneuver:captain\'s maneuver').
card_rarity('captain\'s maneuver'/'APC', 'Uncommon').
card_artist('captain\'s maneuver'/'APC', 'Ben Thompson').
card_number('captain\'s maneuver'/'APC', '92').
card_flavor_text('captain\'s maneuver'/'APC', 'Sisay discovered that the mirrored hull of the Weatherlight could be used as a defensive weapon.').
card_multiverse_id('captain\'s maneuver'/'APC', '25828').

card_in_set('caves of koilos', 'APC').
card_original_type('caves of koilos'/'APC', 'Land').
card_original_text('caves of koilos'/'APC', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Caves of Koilos deals 1 damage to you.').
card_first_print('caves of koilos', 'APC').
card_image_name('caves of koilos'/'APC', 'caves of koilos').
card_uid('caves of koilos'/'APC', 'APC:Caves of Koilos:caves of koilos').
card_rarity('caves of koilos'/'APC', 'Rare').
card_artist('caves of koilos'/'APC', 'Jim Nelson').
card_number('caves of koilos'/'APC', '140').
card_multiverse_id('caves of koilos'/'APC', '23246').

card_in_set('ceta disciple', 'APC').
card_original_type('ceta disciple'/'APC', 'Creature — Wizard').
card_original_text('ceta disciple'/'APC', '{R}, {T}: Target creature gets +2/+0 until end of turn.\n{G}, {T}: Add one mana of any color to your mana pool.').
card_first_print('ceta disciple', 'APC').
card_image_name('ceta disciple'/'APC', 'ceta disciple').
card_uid('ceta disciple'/'APC', 'APC:Ceta Disciple:ceta disciple').
card_rarity('ceta disciple'/'APC', 'Common').
card_artist('ceta disciple'/'APC', 'Greg & Tim Hildebrandt').
card_number('ceta disciple'/'APC', '19').
card_flavor_text('ceta disciple'/'APC', '\"The sea holds all that you need. You simply must know how to ask for it.\"').
card_multiverse_id('ceta disciple'/'APC', '25853').

card_in_set('ceta sanctuary', 'APC').
card_original_type('ceta sanctuary'/'APC', 'Enchantment').
card_original_text('ceta sanctuary'/'APC', 'At the beginning of your upkeep, if you control a red or green permanent, draw a card, then discard a card from your hand. If you control a red permanent and a green permanent, instead draw two cards, then discard a card from your hand.').
card_first_print('ceta sanctuary', 'APC').
card_image_name('ceta sanctuary'/'APC', 'ceta sanctuary').
card_uid('ceta sanctuary'/'APC', 'APC:Ceta Sanctuary:ceta sanctuary').
card_rarity('ceta sanctuary'/'APC', 'Uncommon').
card_artist('ceta sanctuary'/'APC', 'Franz Vohwinkel').
card_number('ceta sanctuary'/'APC', '20').
card_multiverse_id('ceta sanctuary'/'APC', '27173').

card_in_set('cetavolver', 'APC').
card_original_type('cetavolver'/'APC', 'Creature — Volver').
card_original_text('cetavolver'/'APC', 'Kicker {1}{R} and/or {G}\nIf you paid the {1}{R} kicker cost, Cetavolver comes into play with two +1/+1 counters on it and has first strike.\nIf you paid the {G} kicker cost, Cetavolver comes into play with a +1/+1 counter on it and has trample.').
card_first_print('cetavolver', 'APC').
card_image_name('cetavolver'/'APC', 'cetavolver').
card_uid('cetavolver'/'APC', 'APC:Cetavolver:cetavolver').
card_rarity('cetavolver'/'APC', 'Rare').
card_artist('cetavolver'/'APC', 'Gary Ruddell').
card_number('cetavolver'/'APC', '21').
card_multiverse_id('cetavolver'/'APC', '25948').

card_in_set('chaos', 'APC').
card_original_type('chaos'/'APC', 'Instant').
card_original_text('chaos'/'APC', 'Creatures can\'t block this turn.').
card_first_print('chaos', 'APC').
card_image_name('chaos'/'APC', 'orderchaos').
card_uid('chaos'/'APC', 'APC:Chaos:orderchaos').
card_rarity('chaos'/'APC', 'Uncommon').
card_artist('chaos'/'APC', 'Greg & Tim Hildebrandt').
card_number('chaos'/'APC', '132b').
card_multiverse_id('chaos'/'APC', '27168').

card_in_set('coalition flag', 'APC').
card_original_type('coalition flag'/'APC', 'Enchant Creature').
card_original_text('coalition flag'/'APC', 'Coalition Flag can enchant only a creature you control.\nEnchanted creature\'s type is Flagbearer.\nIf an opponent plays a spell or ability that could target a Flagbearer in play, that player chooses at least one Flagbearer as a target.').
card_first_print('coalition flag', 'APC').
card_image_name('coalition flag'/'APC', 'coalition flag').
card_uid('coalition flag'/'APC', 'APC:Coalition Flag:coalition flag').
card_rarity('coalition flag'/'APC', 'Uncommon').
card_artist('coalition flag'/'APC', 'Darrell Riche').
card_number('coalition flag'/'APC', '2').
card_multiverse_id('coalition flag'/'APC', '26791').

card_in_set('coalition honor guard', 'APC').
card_original_type('coalition honor guard'/'APC', 'Creature — Flagbearer').
card_original_text('coalition honor guard'/'APC', 'If an opponent plays a spell or ability that could target a Flagbearer in play, that player chooses at least one Flagbearer as a target.').
card_first_print('coalition honor guard', 'APC').
card_image_name('coalition honor guard'/'APC', 'coalition honor guard').
card_uid('coalition honor guard'/'APC', 'APC:Coalition Honor Guard:coalition honor guard').
card_rarity('coalition honor guard'/'APC', 'Common').
card_artist('coalition honor guard'/'APC', 'Eric Peterson').
card_number('coalition honor guard'/'APC', '3').
card_flavor_text('coalition honor guard'/'APC', 'Giving little thought to their own defense, they carried the flag that united their army.').
card_multiverse_id('coalition honor guard'/'APC', '26371').

card_in_set('coastal drake', 'APC').
card_original_type('coastal drake'/'APC', 'Creature — Drake').
card_original_text('coastal drake'/'APC', 'Flying\n{1}{U}, {T}: Return target Kavu to its owner\'s hand.').
card_first_print('coastal drake', 'APC').
card_image_name('coastal drake'/'APC', 'coastal drake').
card_uid('coastal drake'/'APC', 'APC:Coastal Drake:coastal drake').
card_rarity('coastal drake'/'APC', 'Common').
card_artist('coastal drake'/'APC', 'John Gallagher').
card_number('coastal drake'/'APC', '22').
card_flavor_text('coastal drake'/'APC', 'When the kavu emerged from their ancient nests, food chains long dormant became vibrant again.').
card_multiverse_id('coastal drake'/'APC', '28754').

card_in_set('consume strength', 'APC').
card_original_type('consume strength'/'APC', 'Instant').
card_original_text('consume strength'/'APC', 'Target creature gets +2/+2 until end of turn. Another target creature gets -2/-2 until end of turn.').
card_first_print('consume strength', 'APC').
card_image_name('consume strength'/'APC', 'consume strength').
card_uid('consume strength'/'APC', 'APC:Consume Strength:consume strength').
card_rarity('consume strength'/'APC', 'Common').
card_artist('consume strength'/'APC', 'Adam Rex').
card_number('consume strength'/'APC', '93').
card_flavor_text('consume strength'/'APC', 'As Multani\'s troops invaded Urborg, the decay of the swamp became a feast for the living.').
card_multiverse_id('consume strength'/'APC', '19163').

card_in_set('cromat', 'APC').
card_original_type('cromat'/'APC', 'Creature — Legend').
card_original_text('cromat'/'APC', '{W}{B}: Destroy target creature blocking or blocked by Cromat.\n{U}{R}: Cromat gains flying until end of turn.\n{B}{G}: Regenerate Cromat.\n{R}{W}: Cromat gets +1/+1 until end of turn.\n{G}{U}: Put Cromat on top of its owner\'s library.').
card_first_print('cromat', 'APC').
card_image_name('cromat'/'APC', 'cromat').
card_uid('cromat'/'APC', 'APC:Cromat:cromat').
card_rarity('cromat'/'APC', 'Rare').
card_artist('cromat'/'APC', 'Donato Giancola').
card_number('cromat'/'APC', '94').
card_multiverse_id('cromat'/'APC', '28670').

card_in_set('day', 'APC').
card_original_type('day'/'APC', 'Instant').
card_original_text('day'/'APC', 'Creatures target player controls get +1/+1 until end of turn.').
card_first_print('day', 'APC').
card_image_name('day'/'APC', 'nightday').
card_uid('day'/'APC', 'APC:Day:nightday').
card_rarity('day'/'APC', 'Uncommon').
card_artist('day'/'APC', 'Christopher Moeller').
card_number('day'/'APC', '131b').
card_multiverse_id('day'/'APC', '26691').

card_in_set('dead ringers', 'APC').
card_original_type('dead ringers'/'APC', 'Sorcery').
card_original_text('dead ringers'/'APC', 'Destroy two target nonblack creatures unless either one is a color the other isn\'t. They can\'t be regenerated.').
card_first_print('dead ringers', 'APC').
card_image_name('dead ringers'/'APC', 'dead ringers').
card_uid('dead ringers'/'APC', 'APC:Dead Ringers:dead ringers').
card_rarity('dead ringers'/'APC', 'Common').
card_artist('dead ringers'/'APC', 'Greg Staples').
card_number('dead ringers'/'APC', '37').
card_flavor_text('dead ringers'/'APC', '\"Beautiful, isn\'t it?\" crowed Crovax. \"They fought together. Now they will die together.\"').
card_multiverse_id('dead ringers'/'APC', '26848').

card_in_set('death', 'APC').
card_original_type('death'/'APC', 'Sorcery').
card_original_text('death'/'APC', 'Return target creature card from your graveyard to play. You lose life equal to its converted mana cost.').
card_image_name('death'/'APC', 'lifedeath').
card_uid('death'/'APC', 'APC:Death:lifedeath').
card_rarity('death'/'APC', 'Uncommon').
card_artist('death'/'APC', 'Edward P. Beard, Jr.').
card_number('death'/'APC', '130b').
card_multiverse_id('death'/'APC', '27162').

card_in_set('death grasp', 'APC').
card_original_type('death grasp'/'APC', 'Sorcery').
card_original_text('death grasp'/'APC', 'Death Grasp deals X damage to target creature or player. You gain X life.').
card_first_print('death grasp', 'APC').
card_image_name('death grasp'/'APC', 'death grasp').
card_uid('death grasp'/'APC', 'APC:Death Grasp:death grasp').
card_rarity('death grasp'/'APC', 'Rare').
card_artist('death grasp'/'APC', 'Eric Peterson').
card_number('death grasp'/'APC', '95').
card_flavor_text('death grasp'/'APC', 'Yawgmoth\'s greatest joy came from watching one hero defeat another.').
card_multiverse_id('death grasp'/'APC', '29415').

card_in_set('death mutation', 'APC').
card_original_type('death mutation'/'APC', 'Sorcery').
card_original_text('death mutation'/'APC', 'Destroy target nonblack creature. It can\'t be regenerated. Put X 1/1 green Saproling creature tokens into play, where X is its converted mana cost.').
card_first_print('death mutation', 'APC').
card_image_name('death mutation'/'APC', 'death mutation').
card_uid('death mutation'/'APC', 'APC:Death Mutation:death mutation').
card_rarity('death mutation'/'APC', 'Uncommon').
card_artist('death mutation'/'APC', 'Carl Critchlow').
card_number('death mutation'/'APC', '96').
card_multiverse_id('death mutation'/'APC', '25833').

card_in_set('dega disciple', 'APC').
card_original_type('dega disciple'/'APC', 'Creature — Wizard').
card_original_text('dega disciple'/'APC', '{B}, {T}: Target creature gets -2/-0 until end of turn.\n{R}, {T}: Target creature gets +2/+0 until end of turn.').
card_first_print('dega disciple', 'APC').
card_image_name('dega disciple'/'APC', 'dega disciple').
card_uid('dega disciple'/'APC', 'APC:Dega Disciple:dega disciple').
card_rarity('dega disciple'/'APC', 'Common').
card_artist('dega disciple'/'APC', 'Alan Pollack').
card_number('dega disciple'/'APC', '4').
card_flavor_text('dega disciple'/'APC', '\"There is no true equity of power. There is only more and less.\"').
card_multiverse_id('dega disciple'/'APC', '25852').

card_in_set('dega sanctuary', 'APC').
card_original_type('dega sanctuary'/'APC', 'Enchantment').
card_original_text('dega sanctuary'/'APC', 'At the beginning of your upkeep, if you control a black or red permanent, you gain 2 life. If you control a black permanent and a red permanent, you gain 4 life instead.').
card_first_print('dega sanctuary', 'APC').
card_image_name('dega sanctuary'/'APC', 'dega sanctuary').
card_uid('dega sanctuary'/'APC', 'APC:Dega Sanctuary:dega sanctuary').
card_rarity('dega sanctuary'/'APC', 'Uncommon').
card_artist('dega sanctuary'/'APC', 'Ben Thompson').
card_number('dega sanctuary'/'APC', '5').
card_multiverse_id('dega sanctuary'/'APC', '27178').

card_in_set('degavolver', 'APC').
card_original_type('degavolver'/'APC', 'Creature — Volver').
card_original_text('degavolver'/'APC', 'Kicker {1}{B} and/or {R}\nIf you paid the {1}{B} kicker cost, Degavolver comes into play with two +1/+1 counters on it and has \"Pay 3 life: Regenerate Degavolver.\"\nIf you paid the {R} kicker cost, Degavolver comes into play with a +1/+1 counter on it and has first strike.').
card_first_print('degavolver', 'APC').
card_image_name('degavolver'/'APC', 'degavolver').
card_uid('degavolver'/'APC', 'APC:Degavolver:degavolver').
card_rarity('degavolver'/'APC', 'Rare').
card_artist('degavolver'/'APC', 'Ron Spencer').
card_number('degavolver'/'APC', '6').
card_multiverse_id('degavolver'/'APC', '25947').

card_in_set('desolation angel', 'APC').
card_original_type('desolation angel'/'APC', 'Creature — Angel').
card_original_text('desolation angel'/'APC', 'Kicker {W}{W} (You may pay an additional {W}{W} as you play this spell.)\nFlying\nWhen Desolation Angel comes into play, destroy all lands you control. If you paid the kicker cost, destroy all lands instead.').
card_first_print('desolation angel', 'APC').
card_image_name('desolation angel'/'APC', 'desolation angel').
card_uid('desolation angel'/'APC', 'APC:Desolation Angel:desolation angel').
card_rarity('desolation angel'/'APC', 'Rare').
card_artist('desolation angel'/'APC', 'Brom').
card_number('desolation angel'/'APC', '38').
card_multiverse_id('desolation angel'/'APC', '26259').

card_in_set('desolation giant', 'APC').
card_original_type('desolation giant'/'APC', 'Creature — Giant').
card_original_text('desolation giant'/'APC', 'Kicker {W}{W} (You may pay an additional {W}{W} as you play this spell.)\nWhen Desolation Giant comes into play, destroy all other creatures you control. If you paid the kicker cost, destroy all other creatures instead.').
card_first_print('desolation giant', 'APC').
card_image_name('desolation giant'/'APC', 'desolation giant').
card_uid('desolation giant'/'APC', 'APC:Desolation Giant:desolation giant').
card_rarity('desolation giant'/'APC', 'Rare').
card_artist('desolation giant'/'APC', 'Alan Pollack').
card_number('desolation giant'/'APC', '59').
card_multiverse_id('desolation giant'/'APC', '29414').

card_in_set('diversionary tactics', 'APC').
card_original_type('diversionary tactics'/'APC', 'Enchantment').
card_original_text('diversionary tactics'/'APC', 'Tap two untapped creatures you control: Tap target creature.').
card_first_print('diversionary tactics', 'APC').
card_image_name('diversionary tactics'/'APC', 'diversionary tactics').
card_uid('diversionary tactics'/'APC', 'APC:Diversionary Tactics:diversionary tactics').
card_rarity('diversionary tactics'/'APC', 'Uncommon').
card_artist('diversionary tactics'/'APC', 'Jerry Tiritilli').
card_number('diversionary tactics'/'APC', '7').
card_flavor_text('diversionary tactics'/'APC', '\"It\'s the oldest trick in the book,\" said Guff. \"And I ought to know—I wrote it.\"').
card_multiverse_id('diversionary tactics'/'APC', '28810').

card_in_set('divine light', 'APC').
card_original_type('divine light'/'APC', 'Sorcery').
card_original_text('divine light'/'APC', 'Prevent all damage that would be dealt this turn to creatures you control.').
card_first_print('divine light', 'APC').
card_image_name('divine light'/'APC', 'divine light').
card_uid('divine light'/'APC', 'APC:Divine Light:divine light').
card_rarity('divine light'/'APC', 'Common').
card_artist('divine light'/'APC', 'Christopher Moeller').
card_number('divine light'/'APC', '8').
card_flavor_text('divine light'/'APC', '\"Phyrexia is an unforgiving place, and I am an angry lord in an unforgiving mood.\"\n—Lord Windgrace').
card_multiverse_id('divine light'/'APC', '27137').

card_in_set('dodecapod', 'APC').
card_original_type('dodecapod'/'APC', 'Artifact Creature').
card_original_text('dodecapod'/'APC', 'If a spell or ability an opponent controls causes you to discard Dodecapod from your hand, put it into play with two +1/+1 counters on it instead of putting it into your graveyard.').
card_first_print('dodecapod', 'APC').
card_image_name('dodecapod'/'APC', 'dodecapod').
card_uid('dodecapod'/'APC', 'APC:Dodecapod:dodecapod').
card_rarity('dodecapod'/'APC', 'Uncommon').
card_artist('dodecapod'/'APC', 'John Howe').
card_number('dodecapod'/'APC', '134').
card_multiverse_id('dodecapod'/'APC', '26733').

card_in_set('dragon arch', 'APC').
card_original_type('dragon arch'/'APC', 'Artifact').
card_original_text('dragon arch'/'APC', '{2}, {T}: Put a multicolored creature card from your hand into play.').
card_first_print('dragon arch', 'APC').
card_image_name('dragon arch'/'APC', 'dragon arch').
card_uid('dragon arch'/'APC', 'APC:Dragon Arch:dragon arch').
card_rarity('dragon arch'/'APC', 'Uncommon').
card_artist('dragon arch'/'APC', 'Dana Knutson').
card_number('dragon arch'/'APC', '135').
card_flavor_text('dragon arch'/'APC', 'In their hunger for the arch\'s power, mages often forget that it only makes dragons easier to summon. It doesn\'t make them easier to control.').
card_multiverse_id('dragon arch'/'APC', '26734').

card_in_set('dwarven landslide', 'APC').
card_original_type('dwarven landslide'/'APC', 'Sorcery').
card_original_text('dwarven landslide'/'APC', 'Kicker—{2}{R}, Sacrifice a land. (You may pay {2}{R} and sacrifice a land in addition to any other costs as you play this spell.)\nDestroy target land. If you paid the kicker cost, destroy another target land.').
card_first_print('dwarven landslide', 'APC').
card_image_name('dwarven landslide'/'APC', 'dwarven landslide').
card_uid('dwarven landslide'/'APC', 'APC:Dwarven Landslide:dwarven landslide').
card_rarity('dwarven landslide'/'APC', 'Common').
card_artist('dwarven landslide'/'APC', 'Tony Szczudlo').
card_number('dwarven landslide'/'APC', '60').
card_multiverse_id('dwarven landslide'/'APC', '26810').

card_in_set('dwarven patrol', 'APC').
card_original_type('dwarven patrol'/'APC', 'Creature — Dwarf').
card_original_text('dwarven patrol'/'APC', 'Dwarven Patrol doesn\'t untap during your untap step.\nWhenever you play a nonred spell, untap Dwarven Patrol.').
card_first_print('dwarven patrol', 'APC').
card_image_name('dwarven patrol'/'APC', 'dwarven patrol').
card_uid('dwarven patrol'/'APC', 'APC:Dwarven Patrol:dwarven patrol').
card_rarity('dwarven patrol'/'APC', 'Uncommon').
card_artist('dwarven patrol'/'APC', 'Greg & Tim Hildebrandt').
card_number('dwarven patrol'/'APC', '61').
card_flavor_text('dwarven patrol'/'APC', 'Dwarves never worry about who watches their backs.').
card_multiverse_id('dwarven patrol'/'APC', '26672').

card_in_set('ebony treefolk', 'APC').
card_original_type('ebony treefolk'/'APC', 'Creature — Treefolk').
card_original_text('ebony treefolk'/'APC', '{B}{G}: Ebony Treefolk gets +1/+1 until end of turn.').
card_first_print('ebony treefolk', 'APC').
card_image_name('ebony treefolk'/'APC', 'ebony treefolk').
card_uid('ebony treefolk'/'APC', 'APC:Ebony Treefolk:ebony treefolk').
card_rarity('ebony treefolk'/'APC', 'Uncommon').
card_artist('ebony treefolk'/'APC', 'Matt Cavotta').
card_number('ebony treefolk'/'APC', '97').
card_flavor_text('ebony treefolk'/'APC', 'Its roots are equally happy to drink the water of a clear stream or the oily blood of a Phyrexian warrior.').
card_multiverse_id('ebony treefolk'/'APC', '29450').

card_in_set('emblazoned golem', 'APC').
card_original_type('emblazoned golem'/'APC', 'Artifact Creature — Golem').
card_original_text('emblazoned golem'/'APC', 'Kicker {X} (You may pay an additional {X} as you play this spell.)\nSpend only colored mana on X. No more than one mana of each color may be spent this way.\nIf you paid the kicker cost, Emblazoned Golem comes into play with X +1/+1 counters on it.').
card_first_print('emblazoned golem', 'APC').
card_image_name('emblazoned golem'/'APC', 'emblazoned golem').
card_uid('emblazoned golem'/'APC', 'APC:Emblazoned Golem:emblazoned golem').
card_rarity('emblazoned golem'/'APC', 'Uncommon').
card_artist('emblazoned golem'/'APC', 'Greg Staples').
card_number('emblazoned golem'/'APC', '136').
card_multiverse_id('emblazoned golem'/'APC', '26264').

card_in_set('enlistment officer', 'APC').
card_original_type('enlistment officer'/'APC', 'Creature — Soldier').
card_original_text('enlistment officer'/'APC', 'First strike\nWhen Enlistment Officer comes into play, reveal the top four cards of your library. Put all Soldier cards revealed this way into your hand and the rest on the bottom of your library.').
card_first_print('enlistment officer', 'APC').
card_image_name('enlistment officer'/'APC', 'enlistment officer').
card_uid('enlistment officer'/'APC', 'APC:Enlistment Officer:enlistment officer').
card_rarity('enlistment officer'/'APC', 'Uncommon').
card_artist('enlistment officer'/'APC', 'Wayne England').
card_number('enlistment officer'/'APC', '9').
card_multiverse_id('enlistment officer'/'APC', '27657').

card_in_set('evasive action', 'APC').
card_original_type('evasive action'/'APC', 'Instant').
card_original_text('evasive action'/'APC', 'Counter target spell unless its controller pays {1} for each basic land type among lands you control.').
card_first_print('evasive action', 'APC').
card_image_name('evasive action'/'APC', 'evasive action').
card_uid('evasive action'/'APC', 'APC:Evasive Action:evasive action').
card_rarity('evasive action'/'APC', 'Uncommon').
card_artist('evasive action'/'APC', 'Brian Snõddy').
card_number('evasive action'/'APC', '23').
card_flavor_text('evasive action'/'APC', 'Effective use of terrain is a lesson good commanders learn quickly.').
card_multiverse_id('evasive action'/'APC', '26261').

card_in_set('false dawn', 'APC').
card_original_type('false dawn'/'APC', 'Sorcery').
card_original_text('false dawn'/'APC', 'Colored mana symbols on all permanents you control and on all cards you own that aren\'t in play become {W} until end of turn.\nDraw a card.').
card_first_print('false dawn', 'APC').
card_image_name('false dawn'/'APC', 'false dawn').
card_uid('false dawn'/'APC', 'APC:False Dawn:false dawn').
card_rarity('false dawn'/'APC', 'Rare').
card_artist('false dawn'/'APC', 'Dave Dorman').
card_number('false dawn'/'APC', '10').
card_multiverse_id('false dawn'/'APC', '27602').

card_in_set('fervent charge', 'APC').
card_original_type('fervent charge'/'APC', 'Enchantment').
card_original_text('fervent charge'/'APC', 'Whenever a creature you control attacks, it gets +2/+2 until end of turn.').
card_first_print('fervent charge', 'APC').
card_image_name('fervent charge'/'APC', 'fervent charge').
card_uid('fervent charge'/'APC', 'APC:Fervent Charge:fervent charge').
card_rarity('fervent charge'/'APC', 'Rare').
card_artist('fervent charge'/'APC', 'Mark Tedin').
card_number('fervent charge'/'APC', '98').
card_flavor_text('fervent charge'/'APC', 'Crovax was nearly buried beneath the weight of his opponents.').
card_multiverse_id('fervent charge'/'APC', '28011').

card_in_set('fire', 'APC').
card_original_type('fire'/'APC', 'Instant').
card_original_text('fire'/'APC', 'Fire deals 2 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('fire'/'APC', 'fireice').
card_uid('fire'/'APC', 'APC:Fire:fireice').
card_rarity('fire'/'APC', 'Uncommon').
card_artist('fire'/'APC', 'Franz Vohwinkel').
card_number('fire'/'APC', '128a').
card_multiverse_id('fire'/'APC', '27166').

card_in_set('flowstone charger', 'APC').
card_original_type('flowstone charger'/'APC', 'Creature — Beast').
card_original_text('flowstone charger'/'APC', 'Whenever Flowstone Charger attacks, it gets +3/-3 until end of turn.').
card_first_print('flowstone charger', 'APC').
card_image_name('flowstone charger'/'APC', 'flowstone charger').
card_uid('flowstone charger'/'APC', 'APC:Flowstone Charger:flowstone charger').
card_rarity('flowstone charger'/'APC', 'Uncommon').
card_artist('flowstone charger'/'APC', 'John Gallagher').
card_number('flowstone charger'/'APC', '99').
card_flavor_text('flowstone charger'/'APC', 'A mane of flowstone and a mien of fury.').
card_multiverse_id('flowstone charger'/'APC', '19117').

card_in_set('foul presence', 'APC').
card_original_type('foul presence'/'APC', 'Enchant Creature').
card_original_text('foul presence'/'APC', 'Enchanted creature gets -1/-1 and has \"{T}: Target creature gets -1/-1 until end of turn.\"').
card_first_print('foul presence', 'APC').
card_image_name('foul presence'/'APC', 'foul presence').
card_uid('foul presence'/'APC', 'APC:Foul Presence:foul presence').
card_rarity('foul presence'/'APC', 'Uncommon').
card_artist('foul presence'/'APC', 'Ray Lago').
card_number('foul presence'/'APC', '39').
card_flavor_text('foul presence'/'APC', 'The Phyrexians have one word that means both \"evolution\" and \"pain.\"').
card_multiverse_id('foul presence'/'APC', '28805').

card_in_set('fungal shambler', 'APC').
card_original_type('fungal shambler'/'APC', 'Creature — Beast').
card_original_text('fungal shambler'/'APC', 'Trample\nWhenever Fungal Shambler deals damage to an opponent, you draw a card and that opponent discards a card from his or her hand.').
card_image_name('fungal shambler'/'APC', 'fungal shambler').
card_uid('fungal shambler'/'APC', 'APC:Fungal Shambler:fungal shambler').
card_rarity('fungal shambler'/'APC', 'Rare').
card_artist('fungal shambler'/'APC', 'Jim Nelson').
card_number('fungal shambler'/'APC', '100').
card_multiverse_id('fungal shambler'/'APC', '26743').

card_in_set('gaea\'s balance', 'APC').
card_original_type('gaea\'s balance'/'APC', 'Sorcery').
card_original_text('gaea\'s balance'/'APC', 'As an additional cost to play Gaea\'s Balance, sacrifice five lands.\nSearch your library for a land card of each basic land type and put them into play. Then shuffle your library.').
card_first_print('gaea\'s balance', 'APC').
card_image_name('gaea\'s balance'/'APC', 'gaea\'s balance').
card_uid('gaea\'s balance'/'APC', 'APC:Gaea\'s Balance:gaea\'s balance').
card_rarity('gaea\'s balance'/'APC', 'Uncommon').
card_artist('gaea\'s balance'/'APC', 'Rebecca Guay').
card_number('gaea\'s balance'/'APC', '77').
card_multiverse_id('gaea\'s balance'/'APC', '28668').

card_in_set('gaea\'s skyfolk', 'APC').
card_original_type('gaea\'s skyfolk'/'APC', 'Creature — Elf Merfolk').
card_original_text('gaea\'s skyfolk'/'APC', 'Flying').
card_first_print('gaea\'s skyfolk', 'APC').
card_image_name('gaea\'s skyfolk'/'APC', 'gaea\'s skyfolk').
card_uid('gaea\'s skyfolk'/'APC', 'APC:Gaea\'s Skyfolk:gaea\'s skyfolk').
card_rarity('gaea\'s skyfolk'/'APC', 'Common').
card_artist('gaea\'s skyfolk'/'APC', 'Terese Nielsen').
card_number('gaea\'s skyfolk'/'APC', '101').
card_flavor_text('gaea\'s skyfolk'/'APC', 'The grace of the forest and the spirit of the sea.').
card_multiverse_id('gaea\'s skyfolk'/'APC', '26757').

card_in_set('gerrard capashen', 'APC').
card_original_type('gerrard capashen'/'APC', 'Creature — Legend').
card_original_text('gerrard capashen'/'APC', 'At the beginning of your upkeep, you gain 1 life for each card in target opponent\'s hand.\n{3}{W}: Tap target creature. Play this ability only if Gerrard Capashen is attacking.').
card_first_print('gerrard capashen', 'APC').
card_image_name('gerrard capashen'/'APC', 'gerrard capashen').
card_uid('gerrard capashen'/'APC', 'APC:Gerrard Capashen:gerrard capashen').
card_rarity('gerrard capashen'/'APC', 'Rare').
card_artist('gerrard capashen'/'APC', 'Brom').
card_number('gerrard capashen'/'APC', '11').
card_multiverse_id('gerrard capashen'/'APC', '26793').

card_in_set('gerrard\'s verdict', 'APC').
card_original_type('gerrard\'s verdict'/'APC', 'Sorcery').
card_original_text('gerrard\'s verdict'/'APC', 'Target player discards two cards from his or her hand. You gain 3 life for each land card discarded this way.').
card_image_name('gerrard\'s verdict'/'APC', 'gerrard\'s verdict').
card_uid('gerrard\'s verdict'/'APC', 'APC:Gerrard\'s Verdict:gerrard\'s verdict').
card_rarity('gerrard\'s verdict'/'APC', 'Uncommon').
card_artist('gerrard\'s verdict'/'APC', 'Carl Critchlow').
card_number('gerrard\'s verdict'/'APC', '102').
card_flavor_text('gerrard\'s verdict'/'APC', 'A heart of light and a soul of darkness cannot coexist.').
card_multiverse_id('gerrard\'s verdict'/'APC', '29416').

card_in_set('glade gnarr', 'APC').
card_original_type('glade gnarr'/'APC', 'Creature — Beast').
card_original_text('glade gnarr'/'APC', 'Whenever a player plays a blue spell, Glade Gnarr gets +2/+2 until end of turn.').
card_first_print('glade gnarr', 'APC').
card_image_name('glade gnarr'/'APC', 'glade gnarr').
card_uid('glade gnarr'/'APC', 'APC:Glade Gnarr:glade gnarr').
card_rarity('glade gnarr'/'APC', 'Common').
card_artist('glade gnarr'/'APC', 'Daren Bader').
card_number('glade gnarr'/'APC', '78').
card_flavor_text('glade gnarr'/'APC', 'Long thought merely a legend, the appearance of the gnarr was seen by the defenders as a sign of good luck.').
card_multiverse_id('glade gnarr'/'APC', '27154').

card_in_set('goblin legionnaire', 'APC').
card_original_type('goblin legionnaire'/'APC', 'Creature — Goblin Soldier').
card_original_text('goblin legionnaire'/'APC', '{R}, Sacrifice Goblin Legionnaire: Goblin Legionnaire deals 2 damage to target creature or player.\n{W}, Sacrifice Goblin Legionnaire: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_image_name('goblin legionnaire'/'APC', 'goblin legionnaire').
card_uid('goblin legionnaire'/'APC', 'APC:Goblin Legionnaire:goblin legionnaire').
card_rarity('goblin legionnaire'/'APC', 'Common').
card_artist('goblin legionnaire'/'APC', 'Mark Romanoski').
card_number('goblin legionnaire'/'APC', '103').
card_multiverse_id('goblin legionnaire'/'APC', '26760').

card_in_set('goblin ringleader', 'APC').
card_original_type('goblin ringleader'/'APC', 'Creature — Goblin').
card_original_text('goblin ringleader'/'APC', 'Haste (This creature may attack and {T} the turn it comes under your control.)\nWhen Goblin Ringleader comes into play, reveal the top four cards of your library. Put all Goblin cards revealed this way into your hand and the rest on the bottom of your library.').
card_image_name('goblin ringleader'/'APC', 'goblin ringleader').
card_uid('goblin ringleader'/'APC', 'APC:Goblin Ringleader:goblin ringleader').
card_rarity('goblin ringleader'/'APC', 'Uncommon').
card_artist('goblin ringleader'/'APC', 'Mark Romanoski').
card_number('goblin ringleader'/'APC', '62').
card_multiverse_id('goblin ringleader'/'APC', '27664').

card_in_set('goblin trenches', 'APC').
card_original_type('goblin trenches'/'APC', 'Enchantment').
card_original_text('goblin trenches'/'APC', '{2}, Sacrifice a land: Put two 1/1 red and white Goblin Soldier creature tokens into play.').
card_first_print('goblin trenches', 'APC').
card_image_name('goblin trenches'/'APC', 'goblin trenches').
card_uid('goblin trenches'/'APC', 'APC:Goblin Trenches:goblin trenches').
card_rarity('goblin trenches'/'APC', 'Rare').
card_artist('goblin trenches'/'APC', 'Wayne England').
card_number('goblin trenches'/'APC', '104').
card_flavor_text('goblin trenches'/'APC', 'The ground rose and formed into thousands of tiny warriors. This fight was far from over.').
card_multiverse_id('goblin trenches'/'APC', '29417').

card_in_set('grave defiler', 'APC').
card_original_type('grave defiler'/'APC', 'Creature — Zombie').
card_original_text('grave defiler'/'APC', 'When Grave Defiler comes into play, reveal the top four cards of your library. Put all Zombie cards revealed this way into your hand and the rest on the bottom of your library.\n{1}{B}: Regenerate Grave Defiler.').
card_first_print('grave defiler', 'APC').
card_image_name('grave defiler'/'APC', 'grave defiler').
card_uid('grave defiler'/'APC', 'APC:Grave Defiler:grave defiler').
card_rarity('grave defiler'/'APC', 'Uncommon').
card_artist('grave defiler'/'APC', 'Tony Szczudlo').
card_number('grave defiler'/'APC', '40').
card_multiverse_id('grave defiler'/'APC', '27662').

card_in_set('guided passage', 'APC').
card_original_type('guided passage'/'APC', 'Sorcery').
card_original_text('guided passage'/'APC', 'Reveal the cards in your library. An opponent chooses from among them a creature card, a land card, and a noncreature, nonland card. You put the chosen cards into your hand. Then shuffle your library.').
card_first_print('guided passage', 'APC').
card_image_name('guided passage'/'APC', 'guided passage').
card_uid('guided passage'/'APC', 'APC:Guided Passage:guided passage').
card_rarity('guided passage'/'APC', 'Rare').
card_artist('guided passage'/'APC', 'Alex Horley-Orlandelli').
card_number('guided passage'/'APC', '105').
card_multiverse_id('guided passage'/'APC', '27169').

card_in_set('haunted angel', 'APC').
card_original_type('haunted angel'/'APC', 'Creature — Angel').
card_original_text('haunted angel'/'APC', 'Flying\nWhen Haunted Angel is put into a graveyard from play, remove Haunted Angel from the game and each other player puts a 3/3 black Angel creature token with flying into play.').
card_first_print('haunted angel', 'APC').
card_image_name('haunted angel'/'APC', 'haunted angel').
card_uid('haunted angel'/'APC', 'APC:Haunted Angel:haunted angel').
card_rarity('haunted angel'/'APC', 'Uncommon').
card_artist('haunted angel'/'APC', 'Arnie Swekel').
card_number('haunted angel'/'APC', '12').
card_multiverse_id('haunted angel'/'APC', '25910').

card_in_set('helionaut', 'APC').
card_original_type('helionaut'/'APC', 'Creature — Soldier').
card_original_text('helionaut'/'APC', 'Flying\n{1}, {T}: Add one mana of any color to your mana pool.').
card_first_print('helionaut', 'APC').
card_image_name('helionaut'/'APC', 'helionaut').
card_uid('helionaut'/'APC', 'APC:Helionaut:helionaut').
card_rarity('helionaut'/'APC', 'Common').
card_artist('helionaut'/'APC', 'Franz Vohwinkel').
card_number('helionaut'/'APC', '13').
card_flavor_text('helionaut'/'APC', 'Hope kept the defenders aloft, but only courage kept them in the fight.').
card_multiverse_id('helionaut'/'APC', '26854').

card_in_set('ice', 'APC').
card_original_type('ice'/'APC', 'Instant').
card_original_text('ice'/'APC', 'Tap target permanent.\nDraw a card.').
card_image_name('ice'/'APC', 'fireice').
card_uid('ice'/'APC', 'APC:Ice:fireice').
card_rarity('ice'/'APC', 'Uncommon').
card_artist('ice'/'APC', 'Franz Vohwinkel').
card_number('ice'/'APC', '128b').
card_multiverse_id('ice'/'APC', '27166').

card_in_set('ice cave', 'APC').
card_original_type('ice cave'/'APC', 'Enchantment').
card_original_text('ice cave'/'APC', 'Whenever a player plays a spell, any other player may pay that spell\'s mana cost. If a player does, counter the spell. (Mana cost includes color.)').
card_first_print('ice cave', 'APC').
card_image_name('ice cave'/'APC', 'ice cave').
card_uid('ice cave'/'APC', 'APC:Ice Cave:ice cave').
card_rarity('ice cave'/'APC', 'Rare').
card_artist('ice cave'/'APC', 'Jerry Tiritilli').
card_number('ice cave'/'APC', '24').
card_flavor_text('ice cave'/'APC', 'Even the oppresive cold of the cave could not temper Eladamri\'s ire.').
card_multiverse_id('ice cave'/'APC', '26451').

card_in_set('illuminate', 'APC').
card_original_type('illuminate'/'APC', 'Sorcery').
card_original_text('illuminate'/'APC', 'Kicker {2}{R} and/or {3}{U} (You may pay an additional {2}{R} and/or {3}{U} as you play this spell.)\nIlluminate deals X damage to target creature. If you paid the {2}{R} kicker cost, Illuminate deals X damage to that creature\'s controller. If you paid the {3}{U} kicker cost, you draw X cards.').
card_first_print('illuminate', 'APC').
card_image_name('illuminate'/'APC', 'illuminate').
card_uid('illuminate'/'APC', 'APC:Illuminate:illuminate').
card_rarity('illuminate'/'APC', 'Uncommon').
card_artist('illuminate'/'APC', 'Christopher Moeller').
card_number('illuminate'/'APC', '63').
card_multiverse_id('illuminate'/'APC', '26447').

card_in_set('illusion', 'APC').
card_original_type('illusion'/'APC', 'Instant').
card_original_text('illusion'/'APC', 'Target spell or permanent becomes the color of your choice until end of turn.').
card_first_print('illusion', 'APC').
card_image_name('illusion'/'APC', 'illusionreality').
card_uid('illusion'/'APC', 'APC:Illusion:illusionreality').
card_rarity('illusion'/'APC', 'Uncommon').
card_artist('illusion'/'APC', 'David Martin').
card_number('illusion'/'APC', '129a').
card_multiverse_id('illusion'/'APC', '27164').

card_in_set('index', 'APC').
card_original_type('index'/'APC', 'Sorcery').
card_original_text('index'/'APC', 'Look at the top five cards of your library, then put them back in any order.').
card_first_print('index', 'APC').
card_image_name('index'/'APC', 'index').
card_uid('index'/'APC', 'APC:Index:index').
card_rarity('index'/'APC', 'Common').
card_artist('index'/'APC', 'Kev Walker').
card_number('index'/'APC', '25').
card_flavor_text('index'/'APC', '\"Let\'s see . . . Mercadia, mercenary, merfolk . . . you know, I really need a better filing system.\"').
card_multiverse_id('index'/'APC', '25969').

card_in_set('jaded response', 'APC').
card_original_type('jaded response'/'APC', 'Instant').
card_original_text('jaded response'/'APC', 'Counter target spell if it shares a color with a creature you control.').
card_first_print('jaded response', 'APC').
card_image_name('jaded response'/'APC', 'jaded response').
card_uid('jaded response'/'APC', 'APC:Jaded Response:jaded response').
card_rarity('jaded response'/'APC', 'Common').
card_artist('jaded response'/'APC', 'Matt Cavotta').
card_number('jaded response'/'APC', '26').
card_flavor_text('jaded response'/'APC', '\"Calm down everyone; it\'s nothing we haven\'t seen before.\"\n—Sisay').
card_multiverse_id('jaded response'/'APC', '26673').

card_in_set('jilt', 'APC').
card_original_type('jilt'/'APC', 'Instant').
card_original_text('jilt'/'APC', 'Kicker {1}{R} (You may pay an additional {1}{R} as you play this spell.)\nReturn target creature to its owner\'s hand. If you paid the kicker cost, Jilt deals 2 damage to another target creature.').
card_first_print('jilt', 'APC').
card_image_name('jilt'/'APC', 'jilt').
card_uid('jilt'/'APC', 'APC:Jilt:jilt').
card_rarity('jilt'/'APC', 'Common').
card_artist('jilt'/'APC', 'Terese Nielsen').
card_number('jilt'/'APC', '27').
card_flavor_text('jilt'/'APC', '\"You\'re not my Hanna\"\n—Gerrard, to Yawgmoth').
card_multiverse_id('jilt'/'APC', '26797').

card_in_set('jungle barrier', 'APC').
card_original_type('jungle barrier'/'APC', 'Creature — Wall').
card_original_text('jungle barrier'/'APC', '(Walls can\'t attack.)\nWhen Jungle Barrier comes into play, draw a card.').
card_first_print('jungle barrier', 'APC').
card_image_name('jungle barrier'/'APC', 'jungle barrier').
card_uid('jungle barrier'/'APC', 'APC:Jungle Barrier:jungle barrier').
card_rarity('jungle barrier'/'APC', 'Uncommon').
card_artist('jungle barrier'/'APC', 'Edward P. Beard, Jr.').
card_number('jungle barrier'/'APC', '106').
card_flavor_text('jungle barrier'/'APC', 'A massive pull of mana caused all of Dominaria to pause—Multani had transplanted Yavimaya to Urborg\'s edge.').
card_multiverse_id('jungle barrier'/'APC', '29304').

card_in_set('kavu glider', 'APC').
card_original_type('kavu glider'/'APC', 'Creature — Kavu').
card_original_text('kavu glider'/'APC', '{W}: Kavu Glider gets +0/+1 until end of turn.\n{U}: Kavu Glider gains flying until end of turn.').
card_first_print('kavu glider', 'APC').
card_image_name('kavu glider'/'APC', 'kavu glider').
card_uid('kavu glider'/'APC', 'APC:Kavu Glider:kavu glider').
card_rarity('kavu glider'/'APC', 'Common').
card_artist('kavu glider'/'APC', 'Heather Hudson').
card_number('kavu glider'/'APC', '64').
card_multiverse_id('kavu glider'/'APC', '26809').

card_in_set('kavu howler', 'APC').
card_original_type('kavu howler'/'APC', 'Creature — Kavu').
card_original_text('kavu howler'/'APC', 'When Kavu Howler comes into play, reveal the top four cards of your library. Put all Kavu cards revealed this way into your hand and the rest on the bottom of your library.').
card_first_print('kavu howler', 'APC').
card_image_name('kavu howler'/'APC', 'kavu howler').
card_uid('kavu howler'/'APC', 'APC:Kavu Howler:kavu howler').
card_rarity('kavu howler'/'APC', 'Uncommon').
card_artist('kavu howler'/'APC', 'Wayne England').
card_number('kavu howler'/'APC', '79').
card_multiverse_id('kavu howler'/'APC', '28750').

card_in_set('kavu mauler', 'APC').
card_original_type('kavu mauler'/'APC', 'Creature — Kavu').
card_original_text('kavu mauler'/'APC', 'Trample\nWhenever Kavu Mauler attacks, it gets +1/+1 until end of turn for each other attacking Kavu.').
card_first_print('kavu mauler', 'APC').
card_image_name('kavu mauler'/'APC', 'kavu mauler').
card_uid('kavu mauler'/'APC', 'APC:Kavu Mauler:kavu mauler').
card_rarity('kavu mauler'/'APC', 'Rare').
card_artist('kavu mauler'/'APC', 'Daren Bader').
card_number('kavu mauler'/'APC', '80').
card_flavor_text('kavu mauler'/'APC', 'To the kavu, all Phyrexians are merely crunchy snacks.').
card_multiverse_id('kavu mauler'/'APC', '27760').

card_in_set('last caress', 'APC').
card_original_type('last caress'/'APC', 'Sorcery').
card_original_text('last caress'/'APC', 'Target player loses 1 life and you gain 1 life.\nDraw a card.').
card_first_print('last caress', 'APC').
card_image_name('last caress'/'APC', 'last caress').
card_uid('last caress'/'APC', 'APC:Last Caress:last caress').
card_rarity('last caress'/'APC', 'Common').
card_artist('last caress'/'APC', 'Eric Peterson').
card_number('last caress'/'APC', '41').
card_multiverse_id('last caress'/'APC', '27591').

card_in_set('last stand', 'APC').
card_original_type('last stand'/'APC', 'Sorcery').
card_original_text('last stand'/'APC', 'Target opponent loses 2 life for each swamp you control. Last Stand deals damage equal to the number of mountains you control to target creature. Put a 1/1 green Saproling creature token into play for each forest you control. You gain 2 life for each plains you control. Draw a card for each island you control, then discard that many cards from your hand.').
card_first_print('last stand', 'APC').
card_image_name('last stand'/'APC', 'last stand').
card_uid('last stand'/'APC', 'APC:Last Stand:last stand').
card_rarity('last stand'/'APC', 'Rare').
card_artist('last stand'/'APC', 'Ron Spencer').
card_number('last stand'/'APC', '107').
card_multiverse_id('last stand'/'APC', '27231').

card_in_set('lay of the land', 'APC').
card_original_type('lay of the land'/'APC', 'Sorcery').
card_original_text('lay of the land'/'APC', 'Search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('lay of the land', 'APC').
card_image_name('lay of the land'/'APC', 'lay of the land').
card_uid('lay of the land'/'APC', 'APC:Lay of the Land:lay of the land').
card_rarity('lay of the land'/'APC', 'Common').
card_artist('lay of the land'/'APC', 'Mark Zug').
card_number('lay of the land'/'APC', '81').
card_flavor_text('lay of the land'/'APC', 'Victory favors neither the righteous nor the wicked. It favors the prepared.').
card_multiverse_id('lay of the land'/'APC', '26816').

card_in_set('legacy weapon', 'APC').
card_original_type('legacy weapon'/'APC', 'Legendary Artifact').
card_original_text('legacy weapon'/'APC', '{W}{U}{B}{R}{G}: Remove target permanent from the game.\nIf Legacy Weapon would be put into a graveyard from anywhere, reveal Legacy Weapon and shuffle it into its owner\'s library instead.').
card_first_print('legacy weapon', 'APC').
card_image_name('legacy weapon'/'APC', 'legacy weapon').
card_uid('legacy weapon'/'APC', 'APC:Legacy Weapon:legacy weapon').
card_rarity('legacy weapon'/'APC', 'Rare').
card_artist('legacy weapon'/'APC', 'John Avon').
card_number('legacy weapon'/'APC', '137').
card_multiverse_id('legacy weapon'/'APC', '26424').

card_in_set('life', 'APC').
card_original_type('life'/'APC', 'Sorcery').
card_original_text('life'/'APC', 'Until end of turn, all lands you control are 1/1 creatures that are still lands.').
card_image_name('life'/'APC', 'lifedeath').
card_uid('life'/'APC', 'APC:Life:lifedeath').
card_rarity('life'/'APC', 'Uncommon').
card_artist('life'/'APC', 'Edward P. Beard, Jr.').
card_number('life'/'APC', '130a').
card_multiverse_id('life'/'APC', '27162').

card_in_set('lightning angel', 'APC').
card_original_type('lightning angel'/'APC', 'Creature — Angel').
card_original_text('lightning angel'/'APC', 'Flying; haste (This creature may attack and {T} the turn it comes under your control.)\nAttacking doesn\'t cause Lightning Angel to tap.').
card_first_print('lightning angel', 'APC').
card_image_name('lightning angel'/'APC', 'lightning angel').
card_uid('lightning angel'/'APC', 'APC:Lightning Angel:lightning angel').
card_rarity('lightning angel'/'APC', 'Rare').
card_artist('lightning angel'/'APC', 'rk post').
card_number('lightning angel'/'APC', '108').
card_multiverse_id('lightning angel'/'APC', '27650').

card_in_set('living airship', 'APC').
card_original_type('living airship'/'APC', 'Creature — Ship').
card_original_text('living airship'/'APC', 'Flying\n{2}{G}: Regenerate Living Airship.').
card_first_print('living airship', 'APC').
card_image_name('living airship'/'APC', 'living airship').
card_uid('living airship'/'APC', 'APC:Living Airship:living airship').
card_rarity('living airship'/'APC', 'Common').
card_artist('living airship'/'APC', 'Mark Tedin').
card_number('living airship'/'APC', '28').
card_flavor_text('living airship'/'APC', 'The ship has a crew of three: the pilot, the gunner, and the ship itself.').
card_multiverse_id('living airship'/'APC', '26795').

card_in_set('llanowar dead', 'APC').
card_original_type('llanowar dead'/'APC', 'Creature — Zombie Elf').
card_original_text('llanowar dead'/'APC', '{T}: Add {B} to your mana pool.').
card_first_print('llanowar dead', 'APC').
card_image_name('llanowar dead'/'APC', 'llanowar dead').
card_uid('llanowar dead'/'APC', 'APC:Llanowar Dead:llanowar dead').
card_rarity('llanowar dead'/'APC', 'Common').
card_artist('llanowar dead'/'APC', 'Ben Thompson').
card_number('llanowar dead'/'APC', '109').
card_flavor_text('llanowar dead'/'APC', 'Just as leaves fall from the branches of a living tree, so too do the dead leave the elfhame.').
card_multiverse_id('llanowar dead'/'APC', '26759').

card_in_set('llanowar wastes', 'APC').
card_original_type('llanowar wastes'/'APC', 'Land').
card_original_text('llanowar wastes'/'APC', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Llanowar Wastes deals 1 damage to you.').
card_first_print('llanowar wastes', 'APC').
card_image_name('llanowar wastes'/'APC', 'llanowar wastes').
card_uid('llanowar wastes'/'APC', 'APC:Llanowar Wastes:llanowar wastes').
card_rarity('llanowar wastes'/'APC', 'Rare').
card_artist('llanowar wastes'/'APC', 'Rob Alexander').
card_number('llanowar wastes'/'APC', '141').
card_multiverse_id('llanowar wastes'/'APC', '23248').

card_in_set('manacles of decay', 'APC').
card_original_type('manacles of decay'/'APC', 'Enchant Creature').
card_original_text('manacles of decay'/'APC', 'Enchanted creature can\'t attack.\n{B}: Enchanted creature gets -1/-1 until end of turn.\n{R}: Enchanted creature can\'t block this turn.').
card_first_print('manacles of decay', 'APC').
card_image_name('manacles of decay'/'APC', 'manacles of decay').
card_uid('manacles of decay'/'APC', 'APC:Manacles of Decay:manacles of decay').
card_rarity('manacles of decay'/'APC', 'Common').
card_artist('manacles of decay'/'APC', 'Gary Ruddell').
card_number('manacles of decay'/'APC', '14').
card_multiverse_id('manacles of decay'/'APC', '26788').

card_in_set('martyrs\' tomb', 'APC').
card_original_type('martyrs\' tomb'/'APC', 'Enchantment').
card_original_text('martyrs\' tomb'/'APC', 'Pay 2 life: Prevent the next 1 damage that would be dealt to target creature this turn.').
card_first_print('martyrs\' tomb', 'APC').
card_image_name('martyrs\' tomb'/'APC', 'martyrs\' tomb').
card_uid('martyrs\' tomb'/'APC', 'APC:Martyrs\' Tomb:martyrs\' tomb').
card_rarity('martyrs\' tomb'/'APC', 'Uncommon').
card_artist('martyrs\' tomb'/'APC', 'Anthony S. Waters').
card_number('martyrs\' tomb'/'APC', '110').
card_flavor_text('martyrs\' tomb'/'APC', 'Honor the brave who fought,\nHonor the dead who fell,\nHonor the world they saved.\n—Memorial inscription').
card_multiverse_id('martyrs\' tomb'/'APC', '25825').

card_in_set('mask of intolerance', 'APC').
card_original_type('mask of intolerance'/'APC', 'Artifact').
card_original_text('mask of intolerance'/'APC', 'At the beginning of each player\'s upkeep, if there are four or more basic land types among lands that player controls, Mask of Intolerance deals 3 damage to him or her.').
card_first_print('mask of intolerance', 'APC').
card_image_name('mask of intolerance'/'APC', 'mask of intolerance').
card_uid('mask of intolerance'/'APC', 'APC:Mask of Intolerance:mask of intolerance').
card_rarity('mask of intolerance'/'APC', 'Rare').
card_artist('mask of intolerance'/'APC', 'Glen Angus').
card_number('mask of intolerance'/'APC', '138').
card_multiverse_id('mask of intolerance'/'APC', '29677').

card_in_set('mind extraction', 'APC').
card_original_type('mind extraction'/'APC', 'Sorcery').
card_original_text('mind extraction'/'APC', 'As an additional cost to play Mind Extraction, sacrifice a creature.\nTarget player reveals his or her hand and discards all cards of each of the sacrificed creature\'s colors from it.').
card_first_print('mind extraction', 'APC').
card_image_name('mind extraction'/'APC', 'mind extraction').
card_uid('mind extraction'/'APC', 'APC:Mind Extraction:mind extraction').
card_rarity('mind extraction'/'APC', 'Common').
card_artist('mind extraction'/'APC', 'Adam Rex').
card_number('mind extraction'/'APC', '42').
card_multiverse_id('mind extraction'/'APC', '27171').

card_in_set('minotaur illusionist', 'APC').
card_original_type('minotaur illusionist'/'APC', 'Creature — Minotaur').
card_original_text('minotaur illusionist'/'APC', '{1}{U}: Minotaur Illusionist can\'t be the target of spells or abilities this turn.\n{R}, Sacrifice Minotaur Illusionist: Minotaur Illusionist deals damage equal to its power to target creature.').
card_first_print('minotaur illusionist', 'APC').
card_image_name('minotaur illusionist'/'APC', 'minotaur illusionist').
card_uid('minotaur illusionist'/'APC', 'APC:Minotaur Illusionist:minotaur illusionist').
card_rarity('minotaur illusionist'/'APC', 'Uncommon').
card_artist('minotaur illusionist'/'APC', 'Mark Zug').
card_number('minotaur illusionist'/'APC', '111').
card_multiverse_id('minotaur illusionist'/'APC', '28008').

card_in_set('minotaur tactician', 'APC').
card_original_type('minotaur tactician'/'APC', 'Creature — Minotaur').
card_original_text('minotaur tactician'/'APC', 'Haste (This creature may attack and {T} the turn it comes under your control.)\nMinotaur Tactician gets +1/+1 as long as you control a white creature.\nMinotaur Tactician gets +1/+1 as long as you control a blue creature.').
card_first_print('minotaur tactician', 'APC').
card_image_name('minotaur tactician'/'APC', 'minotaur tactician').
card_uid('minotaur tactician'/'APC', 'APC:Minotaur Tactician:minotaur tactician').
card_rarity('minotaur tactician'/'APC', 'Common').
card_artist('minotaur tactician'/'APC', 'Carl Critchlow').
card_number('minotaur tactician'/'APC', '65').
card_multiverse_id('minotaur tactician'/'APC', '26849').

card_in_set('mournful zombie', 'APC').
card_original_type('mournful zombie'/'APC', 'Creature — Zombie').
card_original_text('mournful zombie'/'APC', '{W}, {T}: Target player gains 1 life.').
card_first_print('mournful zombie', 'APC').
card_image_name('mournful zombie'/'APC', 'mournful zombie').
card_uid('mournful zombie'/'APC', 'APC:Mournful Zombie:mournful zombie').
card_rarity('mournful zombie'/'APC', 'Common').
card_artist('mournful zombie'/'APC', 'John Matson').
card_number('mournful zombie'/'APC', '43').
card_flavor_text('mournful zombie'/'APC', 'Treated with a powerful mix of herbs and potions, its wrappings have a remarkable preservative effect, even for the living.').
card_multiverse_id('mournful zombie'/'APC', '26800').

card_in_set('mystic snake', 'APC').
card_original_type('mystic snake'/'APC', 'Creature — Snake').
card_original_text('mystic snake'/'APC', 'You may play Mystic Snake any time you could play an instant.\nWhen Mystic Snake comes into play, counter target spell.').
card_first_print('mystic snake', 'APC').
card_image_name('mystic snake'/'APC', 'mystic snake').
card_uid('mystic snake'/'APC', 'APC:Mystic Snake:mystic snake').
card_rarity('mystic snake'/'APC', 'Rare').
card_artist('mystic snake'/'APC', 'Daren Bader').
card_number('mystic snake'/'APC', '112').
card_flavor_text('mystic snake'/'APC', 'Its fangs are in your flesh before its hiss leaves your ears.').
card_multiverse_id('mystic snake'/'APC', '26421').

card_in_set('necra disciple', 'APC').
card_original_type('necra disciple'/'APC', 'Creature — Wizard').
card_original_text('necra disciple'/'APC', '{G}, {T}: Add one mana of any color to your mana pool.\n{W}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_first_print('necra disciple', 'APC').
card_image_name('necra disciple'/'APC', 'necra disciple').
card_uid('necra disciple'/'APC', 'APC:Necra Disciple:necra disciple').
card_rarity('necra disciple'/'APC', 'Common').
card_artist('necra disciple'/'APC', 'Jeff Miracola').
card_number('necra disciple'/'APC', '44').
card_flavor_text('necra disciple'/'APC', '\"The darkness merely hides the light.\"').
card_multiverse_id('necra disciple'/'APC', '25854').

card_in_set('necra sanctuary', 'APC').
card_original_type('necra sanctuary'/'APC', 'Enchantment').
card_original_text('necra sanctuary'/'APC', 'At the beginning of your upkeep, if you control a green or white permanent, target player loses 1 life. If you control a green permanent and a white permanent, that player loses 3 life instead.').
card_first_print('necra sanctuary', 'APC').
card_image_name('necra sanctuary'/'APC', 'necra sanctuary').
card_uid('necra sanctuary'/'APC', 'APC:Necra Sanctuary:necra sanctuary').
card_rarity('necra sanctuary'/'APC', 'Uncommon').
card_artist('necra sanctuary'/'APC', 'Eric Peterson').
card_number('necra sanctuary'/'APC', '45').
card_multiverse_id('necra sanctuary'/'APC', '27136').

card_in_set('necravolver', 'APC').
card_original_type('necravolver'/'APC', 'Creature — Volver').
card_original_text('necravolver'/'APC', 'Kicker {1}{G} and/or {W}\nIf you paid the {1}{G} kicker cost, Necravolver comes into play with two +1/+1 counters on it and has trample.\nIf you paid the {W} kicker cost, Necravolver comes into play with a +1/+1 counter on it and has \"Whenever Necravolver deals damage, you gain that much life.\"').
card_first_print('necravolver', 'APC').
card_image_name('necravolver'/'APC', 'necravolver').
card_uid('necravolver'/'APC', 'APC:Necravolver:necravolver').
card_rarity('necravolver'/'APC', 'Rare').
card_artist('necravolver'/'APC', 'Dave Dorman').
card_number('necravolver'/'APC', '46').
card_multiverse_id('necravolver'/'APC', '25949').

card_in_set('night', 'APC').
card_original_type('night'/'APC', 'Instant').
card_original_text('night'/'APC', 'Target creature gets -1/-1 until end of turn.').
card_first_print('night', 'APC').
card_image_name('night'/'APC', 'nightday').
card_uid('night'/'APC', 'APC:Night:nightday').
card_rarity('night'/'APC', 'Uncommon').
card_artist('night'/'APC', 'Christopher Moeller').
card_number('night'/'APC', '131a').
card_multiverse_id('night'/'APC', '26691').

card_in_set('order', 'APC').
card_original_type('order'/'APC', 'Instant').
card_original_text('order'/'APC', 'Remove target attacking creature from the game.').
card_first_print('order', 'APC').
card_image_name('order'/'APC', 'orderchaos').
card_uid('order'/'APC', 'APC:Order:orderchaos').
card_rarity('order'/'APC', 'Uncommon').
card_artist('order'/'APC', 'Greg & Tim Hildebrandt').
card_number('order'/'APC', '132a').
card_multiverse_id('order'/'APC', '27168').

card_in_set('orim\'s thunder', 'APC').
card_original_type('orim\'s thunder'/'APC', 'Instant').
card_original_text('orim\'s thunder'/'APC', 'Kicker {R} (You may pay an additional {R} as you play this spell.)\nDestroy target artifact or enchantment. If you paid the kicker cost, Orim\'s Thunder deals damage equal to that artifact or enchantment\'s converted mana cost to target creature.').
card_first_print('orim\'s thunder', 'APC').
card_image_name('orim\'s thunder'/'APC', 'orim\'s thunder').
card_uid('orim\'s thunder'/'APC', 'APC:Orim\'s Thunder:orim\'s thunder').
card_rarity('orim\'s thunder'/'APC', 'Common').
card_artist('orim\'s thunder'/'APC', 'Carl Critchlow').
card_number('orim\'s thunder'/'APC', '15').
card_multiverse_id('orim\'s thunder'/'APC', '28751').

card_in_set('overgrown estate', 'APC').
card_original_type('overgrown estate'/'APC', 'Enchantment').
card_original_text('overgrown estate'/'APC', 'Sacrifice a land: You gain 3 life.').
card_first_print('overgrown estate', 'APC').
card_image_name('overgrown estate'/'APC', 'overgrown estate').
card_uid('overgrown estate'/'APC', 'APC:Overgrown Estate:overgrown estate').
card_rarity('overgrown estate'/'APC', 'Rare').
card_artist('overgrown estate'/'APC', 'Brian Snõddy').
card_number('overgrown estate'/'APC', '113').
card_flavor_text('overgrown estate'/'APC', 'The decay of Crovax\'s ancestral home matched the state of his soul.').
card_multiverse_id('overgrown estate'/'APC', '25816').

card_in_set('penumbra bobcat', 'APC').
card_original_type('penumbra bobcat'/'APC', 'Creature — Cat').
card_original_text('penumbra bobcat'/'APC', 'When Penumbra Bobcat is put into a graveyard from play, put a 2/1 black Cat creature token into play.').
card_first_print('penumbra bobcat', 'APC').
card_image_name('penumbra bobcat'/'APC', 'penumbra bobcat').
card_uid('penumbra bobcat'/'APC', 'APC:Penumbra Bobcat:penumbra bobcat').
card_rarity('penumbra bobcat'/'APC', 'Common').
card_artist('penumbra bobcat'/'APC', 'Heather Hudson').
card_number('penumbra bobcat'/'APC', '82').
card_flavor_text('penumbra bobcat'/'APC', 'This cat has two lives and it\'s hunting for more.').
card_multiverse_id('penumbra bobcat'/'APC', '26815').

card_in_set('penumbra kavu', 'APC').
card_original_type('penumbra kavu'/'APC', 'Creature — Kavu').
card_original_text('penumbra kavu'/'APC', 'When Penumbra Kavu is put into a graveyard from play, put a 3/3 black Kavu creature token into play.').
card_first_print('penumbra kavu', 'APC').
card_image_name('penumbra kavu'/'APC', 'penumbra kavu').
card_uid('penumbra kavu'/'APC', 'APC:Penumbra Kavu:penumbra kavu').
card_rarity('penumbra kavu'/'APC', 'Uncommon').
card_artist('penumbra kavu'/'APC', 'Tony Szczudlo').
card_number('penumbra kavu'/'APC', '83').
card_flavor_text('penumbra kavu'/'APC', '\"These kavu are so stubborn they even refuse to die.\"\n—Tahngarth').
card_multiverse_id('penumbra kavu'/'APC', '25956').

card_in_set('penumbra wurm', 'APC').
card_original_type('penumbra wurm'/'APC', 'Creature — Wurm').
card_original_text('penumbra wurm'/'APC', 'Trample\nWhen Penumbra Wurm is put into a graveyard from play, put a 6/6 black Wurm creature token with trample into play.').
card_first_print('penumbra wurm', 'APC').
card_image_name('penumbra wurm'/'APC', 'penumbra wurm').
card_uid('penumbra wurm'/'APC', 'APC:Penumbra Wurm:penumbra wurm').
card_rarity('penumbra wurm'/'APC', 'Rare').
card_artist('penumbra wurm'/'APC', 'Jeff Easley').
card_number('penumbra wurm'/'APC', '84').
card_multiverse_id('penumbra wurm'/'APC', '27669').

card_in_set('pernicious deed', 'APC').
card_original_type('pernicious deed'/'APC', 'Enchantment').
card_original_text('pernicious deed'/'APC', '{X}, Sacrifice Pernicious Deed: Destroy each artifact, creature, and enchantment with converted mana cost X or less.').
card_image_name('pernicious deed'/'APC', 'pernicious deed').
card_uid('pernicious deed'/'APC', 'APC:Pernicious Deed:pernicious deed').
card_rarity('pernicious deed'/'APC', 'Rare').
card_artist('pernicious deed'/'APC', 'Christopher Moeller').
card_number('pernicious deed'/'APC', '114').
card_flavor_text('pernicious deed'/'APC', '\"Yawgmoth,\" Freyalise whispered as she set the bomb, \"now you will pay for your treachery.\"').
card_multiverse_id('pernicious deed'/'APC', '25953').

card_in_set('phyrexian arena', 'APC').
card_original_type('phyrexian arena'/'APC', 'Enchantment').
card_original_text('phyrexian arena'/'APC', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_first_print('phyrexian arena', 'APC').
card_image_name('phyrexian arena'/'APC', 'phyrexian arena').
card_uid('phyrexian arena'/'APC', 'APC:Phyrexian Arena:phyrexian arena').
card_rarity('phyrexian arena'/'APC', 'Rare').
card_artist('phyrexian arena'/'APC', 'Pete Venters').
card_number('phyrexian arena'/'APC', '47').
card_flavor_text('phyrexian arena'/'APC', 'An audience of one with the malice of thousands.').
card_multiverse_id('phyrexian arena'/'APC', '27663').

card_in_set('phyrexian gargantua', 'APC').
card_original_type('phyrexian gargantua'/'APC', 'Creature — Horror').
card_original_text('phyrexian gargantua'/'APC', 'When Phyrexian Gargantua comes into play, you draw two cards and you lose 2 life.').
card_first_print('phyrexian gargantua', 'APC').
card_image_name('phyrexian gargantua'/'APC', 'phyrexian gargantua').
card_uid('phyrexian gargantua'/'APC', 'APC:Phyrexian Gargantua:phyrexian gargantua').
card_rarity('phyrexian gargantua'/'APC', 'Uncommon').
card_artist('phyrexian gargantua'/'APC', 'Carl Critchlow').
card_number('phyrexian gargantua'/'APC', '48').
card_flavor_text('phyrexian gargantua'/'APC', 'Other Phyrexians have nightmares about the gargantua.').
card_multiverse_id('phyrexian gargantua'/'APC', '26699').

card_in_set('phyrexian rager', 'APC').
card_original_type('phyrexian rager'/'APC', 'Creature — Horror').
card_original_text('phyrexian rager'/'APC', 'When Phyrexian Rager comes into play, you draw a card and you lose 1 life.').
card_image_name('phyrexian rager'/'APC', 'phyrexian rager').
card_uid('phyrexian rager'/'APC', 'APC:Phyrexian Rager:phyrexian rager').
card_rarity('phyrexian rager'/'APC', 'Common').
card_artist('phyrexian rager'/'APC', 'Mark Tedin').
card_number('phyrexian rager'/'APC', '49').
card_flavor_text('phyrexian rager'/'APC', 'It takes no prisoners, but it keeps the choicest bits for Phyrexia.').
card_multiverse_id('phyrexian rager'/'APC', '27660').

card_in_set('planar despair', 'APC').
card_original_type('planar despair'/'APC', 'Sorcery').
card_original_text('planar despair'/'APC', 'All creatures get -1/-1 until end of turn for each basic land type among lands you control.').
card_first_print('planar despair', 'APC').
card_image_name('planar despair'/'APC', 'planar despair').
card_uid('planar despair'/'APC', 'APC:Planar Despair:planar despair').
card_rarity('planar despair'/'APC', 'Rare').
card_artist('planar despair'/'APC', 'Mike Sass').
card_number('planar despair'/'APC', '50').
card_flavor_text('planar despair'/'APC', 'All of Dominaria recoiled as Yawgmoth and his retainers arrived.').
card_multiverse_id('planar despair'/'APC', '25859').

card_in_set('powerstone minefield', 'APC').
card_original_type('powerstone minefield'/'APC', 'Enchantment').
card_original_text('powerstone minefield'/'APC', 'Whenever a creature attacks or blocks, Powerstone Minefield deals 2 damage to it.').
card_first_print('powerstone minefield', 'APC').
card_image_name('powerstone minefield'/'APC', 'powerstone minefield').
card_uid('powerstone minefield'/'APC', 'APC:Powerstone Minefield:powerstone minefield').
card_rarity('powerstone minefield'/'APC', 'Rare').
card_artist('powerstone minefield'/'APC', 'Greg & Tim Hildebrandt').
card_number('powerstone minefield'/'APC', '115').
card_flavor_text('powerstone minefield'/'APC', '\"We have fought this far and lost too much. We will not turn back.\"\n—Grizzlegom').
card_multiverse_id('powerstone minefield'/'APC', '23198').

card_in_set('prophetic bolt', 'APC').
card_original_type('prophetic bolt'/'APC', 'Instant').
card_original_text('prophetic bolt'/'APC', 'Prophetic Bolt deals 4 damage to target creature or player. Look at the top four cards of your library. Put one of those cards into your hand and the rest on the bottom of your library.').
card_first_print('prophetic bolt', 'APC').
card_image_name('prophetic bolt'/'APC', 'prophetic bolt').
card_uid('prophetic bolt'/'APC', 'APC:Prophetic Bolt:prophetic bolt').
card_rarity('prophetic bolt'/'APC', 'Rare').
card_artist('prophetic bolt'/'APC', 'Dave Dorman').
card_number('prophetic bolt'/'APC', '116').
card_multiverse_id('prophetic bolt'/'APC', '27187').

card_in_set('putrid warrior', 'APC').
card_original_type('putrid warrior'/'APC', 'Creature — Soldier Zombie').
card_original_text('putrid warrior'/'APC', 'Whenever Putrid Warrior deals damage, choose one each player loses 1 life; or each player gains 1 life.').
card_first_print('putrid warrior', 'APC').
card_image_name('putrid warrior'/'APC', 'putrid warrior').
card_uid('putrid warrior'/'APC', 'APC:Putrid Warrior:putrid warrior').
card_rarity('putrid warrior'/'APC', 'Common').
card_artist('putrid warrior'/'APC', 'Ray Lago').
card_number('putrid warrior'/'APC', '117').
card_flavor_text('putrid warrior'/'APC', '\"I do not like it,\" said Grizzlegom, \"but I will take every soldier I can get.\"').
card_multiverse_id('putrid warrior'/'APC', '26758').

card_in_set('quagmire druid', 'APC').
card_original_type('quagmire druid'/'APC', 'Creature — Zombie Druid').
card_original_text('quagmire druid'/'APC', '{G}, {T}, Sacrifice a creature: Destroy target enchantment.').
card_first_print('quagmire druid', 'APC').
card_image_name('quagmire druid'/'APC', 'quagmire druid').
card_uid('quagmire druid'/'APC', 'APC:Quagmire Druid:quagmire druid').
card_rarity('quagmire druid'/'APC', 'Common').
card_artist('quagmire druid'/'APC', 'Dana Knutson').
card_number('quagmire druid'/'APC', '51').
card_flavor_text('quagmire druid'/'APC', 'As the druids had devoted their lives to preserving Dominaria, so did they devote their deaths.').
card_multiverse_id('quagmire druid'/'APC', '27188').

card_in_set('quicksilver dagger', 'APC').
card_original_type('quicksilver dagger'/'APC', 'Enchant Creature').
card_original_text('quicksilver dagger'/'APC', 'Enchanted creature has \"{T}: This creature deals 1 damage to target player. You draw a card.\"').
card_first_print('quicksilver dagger', 'APC').
card_image_name('quicksilver dagger'/'APC', 'quicksilver dagger').
card_uid('quicksilver dagger'/'APC', 'APC:Quicksilver Dagger:quicksilver dagger').
card_rarity('quicksilver dagger'/'APC', 'Common').
card_artist('quicksilver dagger'/'APC', 'Alex Horley-Orlandelli').
card_number('quicksilver dagger'/'APC', '118').
card_flavor_text('quicksilver dagger'/'APC', 'The dwarves call their quicksilver techniques \"improvisational weaponsmithing.\"').
card_multiverse_id('quicksilver dagger'/'APC', '26373').

card_in_set('raka disciple', 'APC').
card_original_type('raka disciple'/'APC', 'Creature — Wizard').
card_original_text('raka disciple'/'APC', '{W}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{U}, {T}: Target creature gains flying until end of turn.').
card_first_print('raka disciple', 'APC').
card_image_name('raka disciple'/'APC', 'raka disciple').
card_uid('raka disciple'/'APC', 'APC:Raka Disciple:raka disciple').
card_rarity('raka disciple'/'APC', 'Common').
card_artist('raka disciple'/'APC', 'Arnie Swekel').
card_number('raka disciple'/'APC', '66').
card_flavor_text('raka disciple'/'APC', '\"The strong can also be subtle.\"').
card_multiverse_id('raka disciple'/'APC', '25855').

card_in_set('raka sanctuary', 'APC').
card_original_type('raka sanctuary'/'APC', 'Enchantment').
card_original_text('raka sanctuary'/'APC', 'At the beginning of your upkeep, if you control a white or blue permanent, Raka Sanctuary deals 1 damage to target creature. If you control a white permanent and a blue permanent, Raka Sanctuary deals 3 damage to that creature instead.').
card_first_print('raka sanctuary', 'APC').
card_image_name('raka sanctuary'/'APC', 'raka sanctuary').
card_uid('raka sanctuary'/'APC', 'APC:Raka Sanctuary:raka sanctuary').
card_rarity('raka sanctuary'/'APC', 'Uncommon').
card_artist('raka sanctuary'/'APC', 'David Martin').
card_number('raka sanctuary'/'APC', '67').
card_multiverse_id('raka sanctuary'/'APC', '27179').

card_in_set('rakavolver', 'APC').
card_original_type('rakavolver'/'APC', 'Creature — Volver').
card_original_text('rakavolver'/'APC', 'Kicker {1}{W} and/or {U}\nIf you paid the {1}{W} kicker cost, Rakavolver comes into play with two +1/+1 counters on it and has \"Whenever Rakavolver deals damage, you gain that much life.\"\nIf you paid the {U} kicker cost, Rakavolver comes into play with a +1/+1 counter on it and has flying.').
card_first_print('rakavolver', 'APC').
card_image_name('rakavolver'/'APC', 'rakavolver').
card_uid('rakavolver'/'APC', 'APC:Rakavolver:rakavolver').
card_rarity('rakavolver'/'APC', 'Rare').
card_artist('rakavolver'/'APC', 'Scott M. Fischer').
card_number('rakavolver'/'APC', '68').
card_multiverse_id('rakavolver'/'APC', '25950').

card_in_set('razorfin hunter', 'APC').
card_original_type('razorfin hunter'/'APC', 'Creature — Merfolk Goblin').
card_original_text('razorfin hunter'/'APC', '{T}: Razorfin Hunter deals 1 damage to target creature or player.').
card_first_print('razorfin hunter', 'APC').
card_image_name('razorfin hunter'/'APC', 'razorfin hunter').
card_uid('razorfin hunter'/'APC', 'APC:Razorfin Hunter:razorfin hunter').
card_rarity('razorfin hunter'/'APC', 'Common').
card_artist('razorfin hunter'/'APC', 'Jeff Easley').
card_number('razorfin hunter'/'APC', '119').
card_flavor_text('razorfin hunter'/'APC', 'No one knew if they\'d been lurking under the sea all along, or if they\'d been created by the Phyrexian overlay.').
card_multiverse_id('razorfin hunter'/'APC', '26756').

card_in_set('reality', 'APC').
card_original_type('reality'/'APC', 'Instant').
card_original_text('reality'/'APC', 'Destroy target artifact.').
card_first_print('reality', 'APC').
card_image_name('reality'/'APC', 'illusionreality').
card_uid('reality'/'APC', 'APC:Reality:illusionreality').
card_rarity('reality'/'APC', 'Uncommon').
card_artist('reality'/'APC', 'David Martin').
card_number('reality'/'APC', '129b').
card_multiverse_id('reality'/'APC', '27164').

card_in_set('reef shaman', 'APC').
card_original_type('reef shaman'/'APC', 'Creature — Merfolk').
card_original_text('reef shaman'/'APC', '{T}: Target land\'s type becomes the basic land type of your choice until end of turn.').
card_first_print('reef shaman', 'APC').
card_image_name('reef shaman'/'APC', 'reef shaman').
card_uid('reef shaman'/'APC', 'APC:Reef Shaman:reef shaman').
card_rarity('reef shaman'/'APC', 'Common').
card_artist('reef shaman'/'APC', 'Scott M. Fischer').
card_number('reef shaman'/'APC', '29').
card_flavor_text('reef shaman'/'APC', 'With one gesture, she can render a thousand years of navigational charts obsolete.').
card_multiverse_id('reef shaman'/'APC', '27158').

card_in_set('savage gorilla', 'APC').
card_original_type('savage gorilla'/'APC', 'Creature — Ape').
card_original_text('savage gorilla'/'APC', '{U}{B}, {T}, Sacrifice Savage Gorilla: Target creature gets -3/-3 until end of turn. Draw a card.').
card_first_print('savage gorilla', 'APC').
card_image_name('savage gorilla'/'APC', 'savage gorilla').
card_uid('savage gorilla'/'APC', 'APC:Savage Gorilla:savage gorilla').
card_rarity('savage gorilla'/'APC', 'Common').
card_artist('savage gorilla'/'APC', 'Dave Dorman').
card_number('savage gorilla'/'APC', '85').
card_flavor_text('savage gorilla'/'APC', 'These omnivorous gorillas have no qualms about a diet composed of equal parts fruit and Phyrexians.').
card_multiverse_id('savage gorilla'/'APC', '25971').

card_in_set('shield of duty and reason', 'APC').
card_original_type('shield of duty and reason'/'APC', 'Enchant Creature').
card_original_text('shield of duty and reason'/'APC', 'Enchanted creature has protection from green and from blue.').
card_first_print('shield of duty and reason', 'APC').
card_image_name('shield of duty and reason'/'APC', 'shield of duty and reason').
card_uid('shield of duty and reason'/'APC', 'APC:Shield of Duty and Reason:shield of duty and reason').
card_rarity('shield of duty and reason'/'APC', 'Common').
card_artist('shield of duty and reason'/'APC', 'Anthony S. Waters').
card_number('shield of duty and reason'/'APC', '16').
card_flavor_text('shield of duty and reason'/'APC', 'The duty: to defend. The reason: to survive.').
card_multiverse_id('shield of duty and reason'/'APC', '27674').

card_in_set('shimmering mirage', 'APC').
card_original_type('shimmering mirage'/'APC', 'Instant').
card_original_text('shimmering mirage'/'APC', 'Target land\'s type becomes the basic land type of your choice until end of turn.\nDraw a card.').
card_first_print('shimmering mirage', 'APC').
card_image_name('shimmering mirage'/'APC', 'shimmering mirage').
card_uid('shimmering mirage'/'APC', 'APC:Shimmering Mirage:shimmering mirage').
card_rarity('shimmering mirage'/'APC', 'Common').
card_artist('shimmering mirage'/'APC', 'Rebecca Guay').
card_number('shimmering mirage'/'APC', '30').
card_flavor_text('shimmering mirage'/'APC', '\"Oh, is that all you need? Why didn\'t you just say so?\"\n—Reef shaman').
card_multiverse_id('shimmering mirage'/'APC', '28753').

card_in_set('shivan reef', 'APC').
card_original_type('shivan reef'/'APC', 'Land').
card_original_text('shivan reef'/'APC', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Shivan Reef deals 1 damage to you.').
card_first_print('shivan reef', 'APC').
card_image_name('shivan reef'/'APC', 'shivan reef').
card_uid('shivan reef'/'APC', 'APC:Shivan Reef:shivan reef').
card_rarity('shivan reef'/'APC', 'Rare').
card_artist('shivan reef'/'APC', 'Rob Alexander').
card_number('shivan reef'/'APC', '142').
card_multiverse_id('shivan reef'/'APC', '23250').

card_in_set('smash', 'APC').
card_original_type('smash'/'APC', 'Instant').
card_original_text('smash'/'APC', 'Destroy target artifact.\nDraw a card.').
card_first_print('smash', 'APC').
card_image_name('smash'/'APC', 'smash').
card_uid('smash'/'APC', 'APC:Smash:smash').
card_rarity('smash'/'APC', 'Common').
card_artist('smash'/'APC', 'Pete Venters').
card_number('smash'/'APC', '69').
card_flavor_text('smash'/'APC', '\"They\'re ugly, and they\'re in my way.\"\n—Tahngarth').
card_multiverse_id('smash'/'APC', '29605').

card_in_set('soul link', 'APC').
card_original_type('soul link'/'APC', 'Enchant Creature').
card_original_text('soul link'/'APC', 'Whenever enchanted creature deals or is dealt damage, you gain that much life.').
card_first_print('soul link', 'APC').
card_image_name('soul link'/'APC', 'soul link').
card_uid('soul link'/'APC', 'APC:Soul Link:soul link').
card_rarity('soul link'/'APC', 'Common').
card_artist('soul link'/'APC', 'Jeff Easley').
card_number('soul link'/'APC', '120').
card_flavor_text('soul link'/'APC', 'They danced like puppets to a tune only Yawgmoth could hear.').
card_multiverse_id('soul link'/'APC', '26822').

card_in_set('spectral lynx', 'APC').
card_original_type('spectral lynx'/'APC', 'Creature — Cat').
card_original_text('spectral lynx'/'APC', 'Protection from green\n{B}: Regenerate Spectral Lynx.').
card_first_print('spectral lynx', 'APC').
card_image_name('spectral lynx'/'APC', 'spectral lynx').
card_uid('spectral lynx'/'APC', 'APC:Spectral Lynx:spectral lynx').
card_rarity('spectral lynx'/'APC', 'Rare').
card_artist('spectral lynx'/'APC', 'Heather Hudson').
card_number('spectral lynx'/'APC', '17').
card_flavor_text('spectral lynx'/'APC', 'A shadowy version of its corporeal cousin, this lynx hunts souls, not meals.').
card_multiverse_id('spectral lynx'/'APC', '26663').

card_in_set('spiritmonger', 'APC').
card_original_type('spiritmonger'/'APC', 'Creature — Beast').
card_original_text('spiritmonger'/'APC', 'Whenever Spiritmonger deals damage to a creature, put a +1/+1 counter on Spiritmonger.\n{B}: Regenerate Spiritmonger.\n{G}: Spiritmonger becomes the color of your choice until end of turn.').
card_first_print('spiritmonger', 'APC').
card_image_name('spiritmonger'/'APC', 'spiritmonger').
card_uid('spiritmonger'/'APC', 'APC:Spiritmonger:spiritmonger').
card_rarity('spiritmonger'/'APC', 'Rare').
card_artist('spiritmonger'/'APC', 'Glen Angus').
card_number('spiritmonger'/'APC', '121').
card_multiverse_id('spiritmonger'/'APC', '28009').

card_in_set('squee\'s embrace', 'APC').
card_original_type('squee\'s embrace'/'APC', 'Enchant Creature').
card_original_text('squee\'s embrace'/'APC', 'Enchanted creature gets +2/+2.\nWhen enchanted creature is put into a graveyard, return that creature card to its owner\'s hand.').
card_first_print('squee\'s embrace', 'APC').
card_image_name('squee\'s embrace'/'APC', 'squee\'s embrace').
card_uid('squee\'s embrace'/'APC', 'APC:Squee\'s Embrace:squee\'s embrace').
card_rarity('squee\'s embrace'/'APC', 'Common').
card_artist('squee\'s embrace'/'APC', 'Rebecca Guay').
card_number('squee\'s embrace'/'APC', '122').
card_multiverse_id('squee\'s embrace'/'APC', '26785').

card_in_set('squee\'s revenge', 'APC').
card_original_type('squee\'s revenge'/'APC', 'Sorcery').
card_original_text('squee\'s revenge'/'APC', 'Choose a number. Flip a coin that many times or until you lose a flip, whichever comes first. If you win all the flips, draw two cards for each flip.').
card_first_print('squee\'s revenge', 'APC').
card_image_name('squee\'s revenge'/'APC', 'squee\'s revenge').
card_uid('squee\'s revenge'/'APC', 'APC:Squee\'s Revenge:squee\'s revenge').
card_rarity('squee\'s revenge'/'APC', 'Uncommon').
card_artist('squee\'s revenge'/'APC', 'Kev Walker').
card_number('squee\'s revenge'/'APC', '123').
card_multiverse_id('squee\'s revenge'/'APC', '26831').

card_in_set('standard bearer', 'APC').
card_original_type('standard bearer'/'APC', 'Creature — Flagbearer').
card_original_text('standard bearer'/'APC', 'If an opponent plays a spell or ability that could target a Flagbearer in play, that player chooses at least one Flagbearer as a target.').
card_first_print('standard bearer', 'APC').
card_image_name('standard bearer'/'APC', 'standard bearer').
card_uid('standard bearer'/'APC', 'APC:Standard Bearer:standard bearer').
card_rarity('standard bearer'/'APC', 'Common').
card_artist('standard bearer'/'APC', 'Ron Spencer').
card_number('standard bearer'/'APC', '18').
card_flavor_text('standard bearer'/'APC', 'The standard was a rallying point for the army and a target for the enemy.').
card_multiverse_id('standard bearer'/'APC', '26783').

card_in_set('strength of night', 'APC').
card_original_type('strength of night'/'APC', 'Instant').
card_original_text('strength of night'/'APC', 'Kicker {B} (You may pay an additional {B} as you play this spell.)\nCreatures you control get +1/+1 until end of turn. If you paid the kicker cost, Zombies you control get an additional +2/+2 until end of turn.').
card_first_print('strength of night', 'APC').
card_image_name('strength of night'/'APC', 'strength of night').
card_uid('strength of night'/'APC', 'APC:Strength of Night:strength of night').
card_rarity('strength of night'/'APC', 'Common').
card_artist('strength of night'/'APC', 'John Avon').
card_number('strength of night'/'APC', '86').
card_multiverse_id('strength of night'/'APC', '28007').

card_in_set('suffocating blast', 'APC').
card_original_type('suffocating blast'/'APC', 'Instant').
card_original_text('suffocating blast'/'APC', 'Counter target spell and Suffocating Blast deals 3 damage to target creature.').
card_first_print('suffocating blast', 'APC').
card_image_name('suffocating blast'/'APC', 'suffocating blast').
card_uid('suffocating blast'/'APC', 'APC:Suffocating Blast:suffocating blast').
card_rarity('suffocating blast'/'APC', 'Rare').
card_artist('suffocating blast'/'APC', 'Christopher Moeller').
card_number('suffocating blast'/'APC', '124').
card_multiverse_id('suffocating blast'/'APC', '27583').

card_in_set('suppress', 'APC').
card_original_type('suppress'/'APC', 'Sorcery').
card_original_text('suppress'/'APC', 'Target player removes all cards in his or her hand from the game face down. At the end of that player\'s next turn, that player returns those cards to his or her hand.').
card_first_print('suppress', 'APC').
card_image_name('suppress'/'APC', 'suppress').
card_uid('suppress'/'APC', 'APC:Suppress:suppress').
card_rarity('suppress'/'APC', 'Uncommon').
card_artist('suppress'/'APC', 'Terese Nielsen & Thomas M. Baxa').
card_number('suppress'/'APC', '52').
card_multiverse_id('suppress'/'APC', '28749').

card_in_set('sylvan messenger', 'APC').
card_original_type('sylvan messenger'/'APC', 'Creature — Elf').
card_original_text('sylvan messenger'/'APC', 'Trample\nWhen Sylvan Messenger comes into play, reveal the top four cards of your library. Put all Elf cards revealed this way into your hand and the rest on the bottom of your library.').
card_first_print('sylvan messenger', 'APC').
card_image_name('sylvan messenger'/'APC', 'sylvan messenger').
card_uid('sylvan messenger'/'APC', 'APC:Sylvan Messenger:sylvan messenger').
card_rarity('sylvan messenger'/'APC', 'Uncommon').
card_artist('sylvan messenger'/'APC', 'Heather Hudson').
card_number('sylvan messenger'/'APC', '87').
card_multiverse_id('sylvan messenger'/'APC', '27666').

card_in_set('symbiotic deployment', 'APC').
card_original_type('symbiotic deployment'/'APC', 'Enchantment').
card_original_text('symbiotic deployment'/'APC', 'Skip your draw step.\n{1}, Tap two untapped creatures you control: Draw a card.').
card_first_print('symbiotic deployment', 'APC').
card_image_name('symbiotic deployment'/'APC', 'symbiotic deployment').
card_uid('symbiotic deployment'/'APC', 'APC:Symbiotic Deployment:symbiotic deployment').
card_rarity('symbiotic deployment'/'APC', 'Rare').
card_artist('symbiotic deployment'/'APC', 'Kev Walker').
card_number('symbiotic deployment'/'APC', '88').
card_flavor_text('symbiotic deployment'/'APC', '\"You know your orders. I know your hearts. Now the stronghold will know defeat.\"\n—Eladamri').
card_multiverse_id('symbiotic deployment'/'APC', '27160').

card_in_set('tahngarth\'s glare', 'APC').
card_original_type('tahngarth\'s glare'/'APC', 'Sorcery').
card_original_text('tahngarth\'s glare'/'APC', 'Look at the top three cards of target opponent\'s library, then put them back in any order. That player looks at the top three cards of your library, then puts them back in any order.').
card_first_print('tahngarth\'s glare', 'APC').
card_image_name('tahngarth\'s glare'/'APC', 'tahngarth\'s glare').
card_uid('tahngarth\'s glare'/'APC', 'APC:Tahngarth\'s Glare:tahngarth\'s glare').
card_rarity('tahngarth\'s glare'/'APC', 'Common').
card_artist('tahngarth\'s glare'/'APC', 'Pete Venters').
card_number('tahngarth\'s glare'/'APC', '70').
card_multiverse_id('tahngarth\'s glare'/'APC', '27156').

card_in_set('temporal spring', 'APC').
card_original_type('temporal spring'/'APC', 'Sorcery').
card_original_text('temporal spring'/'APC', 'Put target permanent on top of its owner\'s library.').
card_first_print('temporal spring', 'APC').
card_image_name('temporal spring'/'APC', 'temporal spring').
card_uid('temporal spring'/'APC', 'APC:Temporal Spring:temporal spring').
card_rarity('temporal spring'/'APC', 'Common').
card_artist('temporal spring'/'APC', 'John Matson').
card_number('temporal spring'/'APC', '125').
card_flavor_text('temporal spring'/'APC', 'One splash of the spring\'s water knocks you clear into last week.').
card_multiverse_id('temporal spring'/'APC', '25832').

card_in_set('tidal courier', 'APC').
card_original_type('tidal courier'/'APC', 'Creature — Merfolk').
card_original_text('tidal courier'/'APC', 'When Tidal Courier comes into play, reveal the top four cards of your library. Put all Merfolk cards revealed this way into your hand and the rest on the bottom of your library.\n{3}{U}: Tidal Courier gains flying until end of turn.').
card_first_print('tidal courier', 'APC').
card_image_name('tidal courier'/'APC', 'tidal courier').
card_uid('tidal courier'/'APC', 'APC:Tidal Courier:tidal courier').
card_rarity('tidal courier'/'APC', 'Uncommon').
card_artist('tidal courier'/'APC', 'Wayne England').
card_number('tidal courier'/'APC', '31').
card_multiverse_id('tidal courier'/'APC', '27659').

card_in_set('tranquil path', 'APC').
card_original_type('tranquil path'/'APC', 'Sorcery').
card_original_text('tranquil path'/'APC', 'Destroy all enchantments.\nDraw a card.').
card_first_print('tranquil path', 'APC').
card_image_name('tranquil path'/'APC', 'tranquil path').
card_uid('tranquil path'/'APC', 'APC:Tranquil Path:tranquil path').
card_rarity('tranquil path'/'APC', 'Common').
card_artist('tranquil path'/'APC', 'John Avon').
card_number('tranquil path'/'APC', '89').
card_flavor_text('tranquil path'/'APC', 'Freyalise bought the planeswalkers the time they needed by clearing a path through the misshapen hell of Phyrexia.').
card_multiverse_id('tranquil path'/'APC', '26720').

card_in_set('tundra kavu', 'APC').
card_original_type('tundra kavu'/'APC', 'Creature — Kavu').
card_original_text('tundra kavu'/'APC', '{T}: Target land becomes a plains or an island until end of turn.').
card_first_print('tundra kavu', 'APC').
card_image_name('tundra kavu'/'APC', 'tundra kavu').
card_uid('tundra kavu'/'APC', 'APC:Tundra Kavu:tundra kavu').
card_rarity('tundra kavu'/'APC', 'Common').
card_artist('tundra kavu'/'APC', 'Matt Cavotta').
card_number('tundra kavu'/'APC', '71').
card_flavor_text('tundra kavu'/'APC', 'A cross between a jaguar and an armadillo.').
card_multiverse_id('tundra kavu'/'APC', '26805').

card_in_set('unnatural selection', 'APC').
card_original_type('unnatural selection'/'APC', 'Enchantment').
card_original_text('unnatural selection'/'APC', '{1}: Choose a creature type other than Wall. Target creature\'s type becomes that type until end of turn.').
card_first_print('unnatural selection', 'APC').
card_image_name('unnatural selection'/'APC', 'unnatural selection').
card_uid('unnatural selection'/'APC', 'APC:Unnatural Selection:unnatural selection').
card_rarity('unnatural selection'/'APC', 'Rare').
card_artist('unnatural selection'/'APC', 'Kev Walker').
card_number('unnatural selection'/'APC', '32').
card_flavor_text('unnatural selection'/'APC', 'What nature makes, magic can modify.').
card_multiverse_id('unnatural selection'/'APC', '26776').

card_in_set('urborg elf', 'APC').
card_original_type('urborg elf'/'APC', 'Creature — Elf').
card_original_text('urborg elf'/'APC', '{T}: Add {G}, {U}, or {B} to your mana pool.').
card_first_print('urborg elf', 'APC').
card_image_name('urborg elf'/'APC', 'urborg elf').
card_uid('urborg elf'/'APC', 'APC:Urborg Elf:urborg elf').
card_rarity('urborg elf'/'APC', 'Common').
card_artist('urborg elf'/'APC', 'Bob Petillo').
card_number('urborg elf'/'APC', '90').
card_flavor_text('urborg elf'/'APC', 'The elves of Urborg are as deadly and capricious as the swamp itself.').
card_multiverse_id('urborg elf'/'APC', '26717').

card_in_set('urborg uprising', 'APC').
card_original_type('urborg uprising'/'APC', 'Sorcery').
card_original_text('urborg uprising'/'APC', 'Return up to two target creature cards from your graveyard to your hand.\nDraw a card.').
card_first_print('urborg uprising', 'APC').
card_image_name('urborg uprising'/'APC', 'urborg uprising').
card_uid('urborg uprising'/'APC', 'APC:Urborg Uprising:urborg uprising').
card_rarity('urborg uprising'/'APC', 'Common').
card_artist('urborg uprising'/'APC', 'Adam Rex').
card_number('urborg uprising'/'APC', '53').
card_flavor_text('urborg uprising'/'APC', 'In Urborg, few things are less permanent than death.').
card_multiverse_id('urborg uprising'/'APC', '26802').

card_in_set('vindicate', 'APC').
card_original_type('vindicate'/'APC', 'Sorcery').
card_original_text('vindicate'/'APC', 'Destroy target permanent.').
card_image_name('vindicate'/'APC', 'vindicate').
card_uid('vindicate'/'APC', 'APC:Vindicate:vindicate').
card_rarity('vindicate'/'APC', 'Rare').
card_artist('vindicate'/'APC', 'Brian Snõddy').
card_number('vindicate'/'APC', '126').
card_flavor_text('vindicate'/'APC', '\"Don\'t mourn for me. This is my destiny.\"\n—Gerrard').
card_multiverse_id('vindicate'/'APC', '19135').

card_in_set('vodalian mystic', 'APC').
card_original_type('vodalian mystic'/'APC', 'Creature — Merfolk').
card_original_text('vodalian mystic'/'APC', '{T}: Target instant or sorcery spell becomes the color of your choice.').
card_first_print('vodalian mystic', 'APC').
card_image_name('vodalian mystic'/'APC', 'vodalian mystic').
card_uid('vodalian mystic'/'APC', 'APC:Vodalian Mystic:vodalian mystic').
card_rarity('vodalian mystic'/'APC', 'Uncommon').
card_artist('vodalian mystic'/'APC', 'Bob Petillo').
card_number('vodalian mystic'/'APC', '33').
card_flavor_text('vodalian mystic'/'APC', '\"Sometimes when I look at the ocean, I could swear it changes colors.\"\n—Sisay').
card_multiverse_id('vodalian mystic'/'APC', '26855').

card_in_set('whirlpool drake', 'APC').
card_original_type('whirlpool drake'/'APC', 'Creature — Drake').
card_original_text('whirlpool drake'/'APC', 'Flying\nWhen Whirlpool Drake comes into play, shuffle the cards from your hand into your library, then draw that many cards.\nWhen Whirlpool Drake is put into a graveyard from play, shuffle the cards from your hand into your library, then draw that many cards.').
card_first_print('whirlpool drake', 'APC').
card_image_name('whirlpool drake'/'APC', 'whirlpool drake').
card_uid('whirlpool drake'/'APC', 'APC:Whirlpool Drake:whirlpool drake').
card_rarity('whirlpool drake'/'APC', 'Uncommon').
card_artist('whirlpool drake'/'APC', 'Alan Pollack').
card_number('whirlpool drake'/'APC', '34').
card_multiverse_id('whirlpool drake'/'APC', '27671').

card_in_set('whirlpool rider', 'APC').
card_original_type('whirlpool rider'/'APC', 'Creature — Merfolk').
card_original_text('whirlpool rider'/'APC', 'When Whirlpool Rider comes into play, shuffle the cards from your hand into your library, then draw that many cards.').
card_first_print('whirlpool rider', 'APC').
card_image_name('whirlpool rider'/'APC', 'whirlpool rider').
card_uid('whirlpool rider'/'APC', 'APC:Whirlpool Rider:whirlpool rider').
card_rarity('whirlpool rider'/'APC', 'Common').
card_artist('whirlpool rider'/'APC', 'Ray Lago').
card_number('whirlpool rider'/'APC', '35').
card_flavor_text('whirlpool rider'/'APC', 'Where the wind meets the water, change is inevitable.').
card_multiverse_id('whirlpool rider'/'APC', '27670').

card_in_set('whirlpool warrior', 'APC').
card_original_type('whirlpool warrior'/'APC', 'Creature — Merfolk').
card_original_text('whirlpool warrior'/'APC', 'When Whirlpool Warrior comes into play, shuffle the cards from your hand into your library, then draw that many cards.\n{R}, Sacrifice Whirlpool Warrior: Each player shuffles the cards from his or her hand into his or her library, then draws that many cards.').
card_first_print('whirlpool warrior', 'APC').
card_image_name('whirlpool warrior'/'APC', 'whirlpool warrior').
card_uid('whirlpool warrior'/'APC', 'APC:Whirlpool Warrior:whirlpool warrior').
card_rarity('whirlpool warrior'/'APC', 'Rare').
card_artist('whirlpool warrior'/'APC', 'Kev Walker').
card_number('whirlpool warrior'/'APC', '36').
card_multiverse_id('whirlpool warrior'/'APC', '27672').

card_in_set('wild research', 'APC').
card_original_type('wild research'/'APC', 'Enchantment').
card_original_text('wild research'/'APC', '{1}{W}: Search your library for an enchantment card and reveal that card. Put it into your hand, then discard a card at random from your hand. Then shuffle your library.\n{1}{U}: Search your library for an instant card and reveal that card. Put it into your hand, then discard a card at random from your hand. Then shuffle your library.').
card_first_print('wild research', 'APC').
card_image_name('wild research'/'APC', 'wild research').
card_uid('wild research'/'APC', 'APC:Wild Research:wild research').
card_rarity('wild research'/'APC', 'Rare').
card_artist('wild research'/'APC', 'Gary Ruddell').
card_number('wild research'/'APC', '72').
card_multiverse_id('wild research'/'APC', '23056').

card_in_set('yavimaya coast', 'APC').
card_original_type('yavimaya coast'/'APC', 'Land').
card_original_text('yavimaya coast'/'APC', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {G} or {U} to your mana pool. Yavimaya Coast deals 1 damage to you.').
card_first_print('yavimaya coast', 'APC').
card_image_name('yavimaya coast'/'APC', 'yavimaya coast').
card_uid('yavimaya coast'/'APC', 'APC:Yavimaya Coast:yavimaya coast').
card_rarity('yavimaya coast'/'APC', 'Rare').
card_artist('yavimaya coast'/'APC', 'Anthony S. Waters').
card_number('yavimaya coast'/'APC', '143').
card_multiverse_id('yavimaya coast'/'APC', '23249').

card_in_set('yavimaya\'s embrace', 'APC').
card_original_type('yavimaya\'s embrace'/'APC', 'Enchant Creature').
card_original_text('yavimaya\'s embrace'/'APC', 'You control enchanted creature.\nEnchanted creature gets +2/+2 and has trample.').
card_first_print('yavimaya\'s embrace', 'APC').
card_image_name('yavimaya\'s embrace'/'APC', 'yavimaya\'s embrace').
card_uid('yavimaya\'s embrace'/'APC', 'APC:Yavimaya\'s Embrace:yavimaya\'s embrace').
card_rarity('yavimaya\'s embrace'/'APC', 'Rare').
card_artist('yavimaya\'s embrace'/'APC', 'Eric Peterson').
card_number('yavimaya\'s embrace'/'APC', '127').
card_multiverse_id('yavimaya\'s embrace'/'APC', '19132').

card_in_set('zombie boa', 'APC').
card_original_type('zombie boa'/'APC', 'Creature — Zombie Snake').
card_original_text('zombie boa'/'APC', '{1}{B}: Choose a color. Whenever Zombie Boa becomes blocked by a creature of that color this turn, destroy that creature. Play this ability only any time you could play a sorcery.').
card_first_print('zombie boa', 'APC').
card_image_name('zombie boa'/'APC', 'zombie boa').
card_uid('zombie boa'/'APC', 'APC:Zombie Boa:zombie boa').
card_rarity('zombie boa'/'APC', 'Common').
card_artist('zombie boa'/'APC', 'Greg Staples').
card_number('zombie boa'/'APC', '54').
card_flavor_text('zombie boa'/'APC', 'It doesn\'t need to feed, but still it hungers.').
card_multiverse_id('zombie boa'/'APC', '26801').
