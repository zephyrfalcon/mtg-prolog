% Alara Reborn

set('ARB').
set_name('ARB', 'Alara Reborn').
set_release_date('ARB', '2009-04-30').
set_border('ARB', 'black').
set_type('ARB', 'expansion').
set_block('ARB', 'Alara').

card_in_set('anathemancer', 'ARB').
card_original_type('anathemancer'/'ARB', 'Creature — Zombie Wizard').
card_original_text('anathemancer'/'ARB', 'When Anathemancer comes into play, it deals damage to target player equal to the number of nonbasic lands that player controls.\nUnearth {5}{B}{R} ({5}{B}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_image_name('anathemancer'/'ARB', 'anathemancer').
card_uid('anathemancer'/'ARB', 'ARB:Anathemancer:anathemancer').
card_rarity('anathemancer'/'ARB', 'Uncommon').
card_artist('anathemancer'/'ARB', 'Richard Whitters').
card_number('anathemancer'/'ARB', '33').
card_multiverse_id('anathemancer'/'ARB', '179538').

card_in_set('architects of will', 'ARB').
card_original_type('architects of will'/'ARB', 'Artifact Creature — Human Wizard').
card_original_text('architects of will'/'ARB', 'When Architects of Will comes into play, look at the top three cards of target player\'s library, then put them back in any order.\nCycling {U/B} ({U/B}, Discard this card: Draw a card.)').
card_first_print('architects of will', 'ARB').
card_image_name('architects of will'/'ARB', 'architects of will').
card_uid('architects of will'/'ARB', 'ARB:Architects of Will:architects of will').
card_rarity('architects of will'/'ARB', 'Common').
card_artist('architects of will'/'ARB', 'Matt Stewart').
card_number('architects of will'/'ARB', '17').
card_flavor_text('architects of will'/'ARB', 'This secret society of mages manipulates the beliefs and opinions of others.').
card_multiverse_id('architects of will'/'ARB', '179597').

card_in_set('ardent plea', 'ARB').
card_original_type('ardent plea'/'ARB', 'Enchantment').
card_original_text('ardent plea'/'ARB', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nCascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)').
card_first_print('ardent plea', 'ARB').
card_image_name('ardent plea'/'ARB', 'ardent plea').
card_uid('ardent plea'/'ARB', 'ARB:Ardent Plea:ardent plea').
card_rarity('ardent plea'/'ARB', 'Uncommon').
card_artist('ardent plea'/'ARB', 'Chippy').
card_number('ardent plea'/'ARB', '1').
card_multiverse_id('ardent plea'/'ARB', '185054').

card_in_set('arsenal thresher', 'ARB').
card_original_type('arsenal thresher'/'ARB', 'Artifact Creature — Construct').
card_original_text('arsenal thresher'/'ARB', 'As Arsenal Thresher comes into play, you may reveal any number of other artifact cards from your hand. Arsenal Thresher comes into play with a +1/+1 counter on it for each card revealed this way.').
card_first_print('arsenal thresher', 'ARB').
card_image_name('arsenal thresher'/'ARB', 'arsenal thresher').
card_uid('arsenal thresher'/'ARB', 'ARB:Arsenal Thresher:arsenal thresher').
card_rarity('arsenal thresher'/'ARB', 'Common').
card_artist('arsenal thresher'/'ARB', 'Ralph Horsley').
card_number('arsenal thresher'/'ARB', '131').
card_multiverse_id('arsenal thresher'/'ARB', '189652').

card_in_set('aven mimeomancer', 'ARB').
card_original_type('aven mimeomancer'/'ARB', 'Creature — Bird Wizard').
card_original_text('aven mimeomancer'/'ARB', 'Flying\nAt the beginning of your upkeep, you may put a feather counter on target creature. If you do, that creature is 3/1 and has flying as long as it has a feather counter on it.').
card_first_print('aven mimeomancer', 'ARB').
card_image_name('aven mimeomancer'/'ARB', 'aven mimeomancer').
card_uid('aven mimeomancer'/'ARB', 'ARB:Aven Mimeomancer:aven mimeomancer').
card_rarity('aven mimeomancer'/'ARB', 'Rare').
card_artist('aven mimeomancer'/'ARB', 'Jesper Ejsing').
card_number('aven mimeomancer'/'ARB', '2').
card_flavor_text('aven mimeomancer'/'ARB', '\"If the skies are our only refuge, I will see all of us take wing.\"').
card_multiverse_id('aven mimeomancer'/'ARB', '180608').

card_in_set('bant sojourners', 'ARB').
card_original_type('bant sojourners'/'ARB', 'Creature — Human Soldier').
card_original_text('bant sojourners'/'ARB', 'When you cycle Bant Sojourners or it\'s put into a graveyard from play, you may put a 1/1 white Soldier creature token into play.\nCycling {2}{W} ({2}{W}, Discard this card: Draw a card.)').
card_first_print('bant sojourners', 'ARB').
card_image_name('bant sojourners'/'ARB', 'bant sojourners').
card_uid('bant sojourners'/'ARB', 'ARB:Bant Sojourners:bant sojourners').
card_rarity('bant sojourners'/'ARB', 'Common').
card_artist('bant sojourners'/'ARB', 'John Avon').
card_number('bant sojourners'/'ARB', '125').
card_flavor_text('bant sojourners'/'ARB', '\"We must spread Bant\'s light before we are shadowed over.\"').
card_multiverse_id('bant sojourners'/'ARB', '188963').

card_in_set('bant sureblade', 'ARB').
card_original_type('bant sureblade'/'ARB', 'Creature — Human Soldier').
card_original_text('bant sureblade'/'ARB', 'As long as you control another multicolored permanent, Bant Sureblade gets +1/+1 and has first strike.').
card_first_print('bant sureblade', 'ARB').
card_image_name('bant sureblade'/'ARB', 'bant sureblade').
card_uid('bant sureblade'/'ARB', 'ARB:Bant Sureblade:bant sureblade').
card_rarity('bant sureblade'/'ARB', 'Common').
card_artist('bant sureblade'/'ARB', 'Michael Komarck').
card_number('bant sureblade'/'ARB', '143').
card_flavor_text('bant sureblade'/'ARB', 'Soldiers who fought through Grixis learned to hit first, recite the prayer of Asha later.').
card_multiverse_id('bant sureblade'/'ARB', '188975').

card_in_set('behemoth sledge', 'ARB').
card_original_type('behemoth sledge'/'ARB', 'Artifact — Equipment').
card_original_text('behemoth sledge'/'ARB', 'Equipped creature gets +2/+2 and has lifelink and trample.\nEquip {3}').
card_first_print('behemoth sledge', 'ARB').
card_image_name('behemoth sledge'/'ARB', 'behemoth sledge').
card_uid('behemoth sledge'/'ARB', 'ARB:Behemoth Sledge:behemoth sledge').
card_rarity('behemoth sledge'/'ARB', 'Uncommon').
card_artist('behemoth sledge'/'ARB', 'Steve Prescott').
card_number('behemoth sledge'/'ARB', '65').
card_flavor_text('behemoth sledge'/'ARB', 'Those who worship the great gargantuans could hardly be expected to fight with a subtle weapon.').
card_multiverse_id('behemoth sledge'/'ARB', '179545').

card_in_set('bituminous blast', 'ARB').
card_original_type('bituminous blast'/'ARB', 'Instant').
card_original_text('bituminous blast'/'ARB', 'Cascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)\nBituminous Blast deals 4 damage to target creature.').
card_image_name('bituminous blast'/'ARB', 'bituminous blast').
card_uid('bituminous blast'/'ARB', 'ARB:Bituminous Blast:bituminous blast').
card_rarity('bituminous blast'/'ARB', 'Uncommon').
card_artist('bituminous blast'/'ARB', 'Raymond Swanland').
card_number('bituminous blast'/'ARB', '34').
card_multiverse_id('bituminous blast'/'ARB', '185057').

card_in_set('blitz hellion', 'ARB').
card_original_type('blitz hellion'/'ARB', 'Creature — Hellion').
card_original_text('blitz hellion'/'ARB', 'Trample, haste\nAt end of turn, Blitz Hellion\'s owner shuffles it into his or her library.').
card_first_print('blitz hellion', 'ARB').
card_image_name('blitz hellion'/'ARB', 'blitz hellion').
card_uid('blitz hellion'/'ARB', 'ARB:Blitz Hellion:blitz hellion').
card_rarity('blitz hellion'/'ARB', 'Rare').
card_artist('blitz hellion'/'ARB', 'Anthony S. Waters').
card_number('blitz hellion'/'ARB', '49').
card_flavor_text('blitz hellion'/'ARB', 'Alarans commemorated its appearances with new holidays bearing names like the Great Cataclysm and the Fall of Ilson Gate.').
card_multiverse_id('blitz hellion'/'ARB', '179406').

card_in_set('bloodbraid elf', 'ARB').
card_original_type('bloodbraid elf'/'ARB', 'Creature — Elf Berserker').
card_original_text('bloodbraid elf'/'ARB', 'Haste\nCascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)').
card_image_name('bloodbraid elf'/'ARB', 'bloodbraid elf').
card_uid('bloodbraid elf'/'ARB', 'ARB:Bloodbraid Elf:bloodbraid elf').
card_rarity('bloodbraid elf'/'ARB', 'Uncommon').
card_artist('bloodbraid elf'/'ARB', 'Dominick Domingo').
card_number('bloodbraid elf'/'ARB', '50').
card_multiverse_id('bloodbraid elf'/'ARB', '185053').

card_in_set('brainbite', 'ARB').
card_original_type('brainbite'/'ARB', 'Sorcery').
card_original_text('brainbite'/'ARB', 'Target opponent reveals his or her hand. You choose a card from it. That player discards that card.\nDraw a card.').
card_first_print('brainbite', 'ARB').
card_image_name('brainbite'/'ARB', 'brainbite').
card_uid('brainbite'/'ARB', 'ARB:Brainbite:brainbite').
card_rarity('brainbite'/'ARB', 'Common').
card_artist('brainbite'/'ARB', 'Mark Hyzer').
card_number('brainbite'/'ARB', '18').
card_flavor_text('brainbite'/'ARB', 'An Esper mage will leave a hole in your memory with surgical precision. A Grixis mage sees no reason to be so kind.').
card_multiverse_id('brainbite'/'ARB', '179607').

card_in_set('breath of malfegor', 'ARB').
card_original_type('breath of malfegor'/'ARB', 'Instant').
card_original_text('breath of malfegor'/'ARB', 'Breath of Malfegor deals 5 damage to each opponent.').
card_image_name('breath of malfegor'/'ARB', 'breath of malfegor').
card_uid('breath of malfegor'/'ARB', 'ARB:Breath of Malfegor:breath of malfegor').
card_rarity('breath of malfegor'/'ARB', 'Common').
card_artist('breath of malfegor'/'ARB', 'Vance Kovacs').
card_number('breath of malfegor'/'ARB', '35').
card_flavor_text('breath of malfegor'/'ARB', 'The blood of the demons quickened as their master rose over the battlefield. The blood of the angels also quickened: here was the chance to finally end the war started millennia ago.').
card_multiverse_id('breath of malfegor'/'ARB', '161291').

card_in_set('captured sunlight', 'ARB').
card_original_type('captured sunlight'/'ARB', 'Sorcery').
card_original_text('captured sunlight'/'ARB', 'Cascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)\nYou gain 4 life.').
card_first_print('captured sunlight', 'ARB').
card_image_name('captured sunlight'/'ARB', 'captured sunlight').
card_uid('captured sunlight'/'ARB', 'ARB:Captured Sunlight:captured sunlight').
card_rarity('captured sunlight'/'ARB', 'Common').
card_artist('captured sunlight'/'ARB', 'Nils Hamm').
card_number('captured sunlight'/'ARB', '66').
card_multiverse_id('captured sunlight'/'ARB', '185064').

card_in_set('cerodon yearling', 'ARB').
card_original_type('cerodon yearling'/'ARB', 'Creature — Beast').
card_original_text('cerodon yearling'/'ARB', 'Vigilance, haste').
card_first_print('cerodon yearling', 'ARB').
card_image_name('cerodon yearling'/'ARB', 'cerodon yearling').
card_uid('cerodon yearling'/'ARB', 'ARB:Cerodon Yearling:cerodon yearling').
card_rarity('cerodon yearling'/'ARB', 'Common').
card_artist('cerodon yearling'/'ARB', 'Christopher Moeller').
card_number('cerodon yearling'/'ARB', '96').
card_flavor_text('cerodon yearling'/'ARB', 'The unit of measurement used to chart a cerodon\'s growth is \"persons consumed.\"').
card_multiverse_id('cerodon yearling'/'ARB', '180604').

card_in_set('cloven casting', 'ARB').
card_original_type('cloven casting'/'ARB', 'Enchantment').
card_original_text('cloven casting'/'ARB', 'Whenever you play a multicolored instant or sorcery spell, you may pay {1}. If you do, copy that spell. You may choose new targets for the copy.').
card_first_print('cloven casting', 'ARB').
card_image_name('cloven casting'/'ARB', 'cloven casting').
card_uid('cloven casting'/'ARB', 'ARB:Cloven Casting:cloven casting').
card_rarity('cloven casting'/'ARB', 'Rare').
card_artist('cloven casting'/'ARB', 'Vance Kovacs').
card_number('cloven casting'/'ARB', '86').
card_flavor_text('cloven casting'/'ARB', '\"Let us flee to the deep jungle. Our city is no longer safe—and I suspect neither is the fallback retreat.\"').
card_multiverse_id('cloven casting'/'ARB', '189653').

card_in_set('colossal might', 'ARB').
card_original_type('colossal might'/'ARB', 'Instant').
card_original_text('colossal might'/'ARB', 'Target creature gets +4/+2 and gains trample until end of turn.').
card_first_print('colossal might', 'ARB').
card_image_name('colossal might'/'ARB', 'colossal might').
card_uid('colossal might'/'ARB', 'ARB:Colossal Might:colossal might').
card_rarity('colossal might'/'ARB', 'Common').
card_artist('colossal might'/'ARB', 'Justin Sweet').
card_number('colossal might'/'ARB', '51').
card_flavor_text('colossal might'/'ARB', '\"Never corner a barbarian. You become the path of least resistance.\"\n—Sarkhan Vol').
card_multiverse_id('colossal might'/'ARB', '179624').

card_in_set('crystallization', 'ARB').
card_original_type('crystallization'/'ARB', 'Enchantment — Aura').
card_original_text('crystallization'/'ARB', 'Enchant creature\nEnchanted creature can\'t attack or block.\nWhen enchanted creature becomes the target of a spell or ability, remove that creature from the game.').
card_first_print('crystallization', 'ARB').
card_image_name('crystallization'/'ARB', 'crystallization').
card_uid('crystallization'/'ARB', 'ARB:Crystallization:crystallization').
card_rarity('crystallization'/'ARB', 'Common').
card_artist('crystallization'/'ARB', 'Zoltan Boros & Gabor Szikszai').
card_number('crystallization'/'ARB', '144').
card_multiverse_id('crystallization'/'ARB', '179621').

card_in_set('dauntless escort', 'ARB').
card_original_type('dauntless escort'/'ARB', 'Creature — Rhino Soldier').
card_original_text('dauntless escort'/'ARB', 'Sacrifice Dauntless Escort: Creatures you control are indestructible this turn.').
card_first_print('dauntless escort', 'ARB').
card_image_name('dauntless escort'/'ARB', 'dauntless escort').
card_uid('dauntless escort'/'ARB', 'ARB:Dauntless Escort:dauntless escort').
card_rarity('dauntless escort'/'ARB', 'Rare').
card_artist('dauntless escort'/'ARB', 'Volkan Baga').
card_number('dauntless escort'/'ARB', '67').
card_flavor_text('dauntless escort'/'ARB', 'Elspeth\'s squires do not seek advancement. For them, no knightly glory could surpass the glory of serving their champion.').
card_multiverse_id('dauntless escort'/'ARB', '180614').

card_in_set('deadshot minotaur', 'ARB').
card_original_type('deadshot minotaur'/'ARB', 'Creature — Minotaur').
card_original_text('deadshot minotaur'/'ARB', 'When Deadshot Minotaur comes into play, it deals 3 damage to target creature with flying.\nCycling {R/G} ({R/G}, Discard this card: Draw a card.)').
card_first_print('deadshot minotaur', 'ARB').
card_image_name('deadshot minotaur'/'ARB', 'deadshot minotaur').
card_uid('deadshot minotaur'/'ARB', 'ARB:Deadshot Minotaur:deadshot minotaur').
card_rarity('deadshot minotaur'/'ARB', 'Common').
card_artist('deadshot minotaur'/'ARB', 'Jason Chan').
card_number('deadshot minotaur'/'ARB', '52').
card_flavor_text('deadshot minotaur'/'ARB', '\"May the earth rise up to meet you.\"').
card_multiverse_id('deadshot minotaur'/'ARB', '179543').

card_in_set('deathbringer thoctar', 'ARB').
card_original_type('deathbringer thoctar'/'ARB', 'Creature — Zombie Beast').
card_original_text('deathbringer thoctar'/'ARB', 'Whenever another creature is put into a graveyard from play, you may put a +1/+1 counter on Deathbringer Thoctar.\nRemove a +1/+1 counter from Deathbringer Thoctar: Deathbringer Thoctar deals 1 damage to target creature or player.').
card_first_print('deathbringer thoctar', 'ARB').
card_image_name('deathbringer thoctar'/'ARB', 'deathbringer thoctar').
card_uid('deathbringer thoctar'/'ARB', 'ARB:Deathbringer Thoctar:deathbringer thoctar').
card_rarity('deathbringer thoctar'/'ARB', 'Rare').
card_artist('deathbringer thoctar'/'ARB', 'Karl Kopinski').
card_number('deathbringer thoctar'/'ARB', '36').
card_multiverse_id('deathbringer thoctar'/'ARB', '180622').

card_in_set('defiler of souls', 'ARB').
card_original_type('defiler of souls'/'ARB', 'Creature — Demon').
card_original_text('defiler of souls'/'ARB', 'Flying\nAt the beginning of each player\'s upkeep, that player sacrifices a monocolored creature.').
card_first_print('defiler of souls', 'ARB').
card_image_name('defiler of souls'/'ARB', 'defiler of souls').
card_uid('defiler of souls'/'ARB', 'ARB:Defiler of Souls:defiler of souls').
card_rarity('defiler of souls'/'ARB', 'Mythic Rare').
card_artist('defiler of souls'/'ARB', 'Paul Bonner').
card_number('defiler of souls'/'ARB', '37').
card_flavor_text('defiler of souls'/'ARB', '\"Stray not into sin, lest you face the doorman of an insatiable tomb.\"\n—Prayer of Asha').
card_multiverse_id('defiler of souls'/'ARB', '189646').

card_in_set('demonic dread', 'ARB').
card_original_type('demonic dread'/'ARB', 'Sorcery').
card_original_text('demonic dread'/'ARB', 'Cascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)\nTarget creature can\'t block this turn.').
card_first_print('demonic dread', 'ARB').
card_image_name('demonic dread'/'ARB', 'demonic dread').
card_uid('demonic dread'/'ARB', 'ARB:Demonic Dread:demonic dread').
card_rarity('demonic dread'/'ARB', 'Common').
card_artist('demonic dread'/'ARB', 'Thomas M. Baxa').
card_number('demonic dread'/'ARB', '38').
card_multiverse_id('demonic dread'/'ARB', '185062').

card_in_set('demonspine whip', 'ARB').
card_original_type('demonspine whip'/'ARB', 'Artifact — Equipment').
card_original_text('demonspine whip'/'ARB', '{X}: Equipped creature gets +X/+0 until end of turn.\nEquip {1}').
card_first_print('demonspine whip', 'ARB').
card_image_name('demonspine whip'/'ARB', 'demonspine whip').
card_uid('demonspine whip'/'ARB', 'ARB:Demonspine Whip:demonspine whip').
card_rarity('demonspine whip'/'ARB', 'Uncommon').
card_artist('demonspine whip'/'ARB', 'Mark Hyzer').
card_number('demonspine whip'/'ARB', '39').
card_flavor_text('demonspine whip'/'ARB', 'Using a spine to break a slave\'s back appeals to the Grixis sense of irony.').
card_multiverse_id('demonspine whip'/'ARB', '180311').

card_in_set('deny reality', 'ARB').
card_original_type('deny reality'/'ARB', 'Sorcery').
card_original_text('deny reality'/'ARB', 'Cascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)\nReturn target permanent to its owner\'s hand.').
card_first_print('deny reality', 'ARB').
card_image_name('deny reality'/'ARB', 'deny reality').
card_uid('deny reality'/'ARB', 'ARB:Deny Reality:deny reality').
card_rarity('deny reality'/'ARB', 'Common').
card_artist('deny reality'/'ARB', 'Jean-Sébastien Rossbach').
card_number('deny reality'/'ARB', '19').
card_multiverse_id('deny reality'/'ARB', '185045').

card_in_set('double negative', 'ARB').
card_original_type('double negative'/'ARB', 'Instant').
card_original_text('double negative'/'ARB', 'Counter up to two target spells.').
card_first_print('double negative', 'ARB').
card_image_name('double negative'/'ARB', 'double negative').
card_uid('double negative'/'ARB', 'ARB:Double Negative:double negative').
card_rarity('double negative'/'ARB', 'Uncommon').
card_artist('double negative'/'ARB', 'Paolo Parente').
card_number('double negative'/'ARB', '87').
card_flavor_text('double negative'/'ARB', 'They feared each other for their differences but were absolutely terrified at their similarities.').
card_multiverse_id('double negative'/'ARB', '179544').

card_in_set('dragon appeasement', 'ARB').
card_original_type('dragon appeasement'/'ARB', 'Enchantment').
card_original_text('dragon appeasement'/'ARB', 'Skip your draw step.\nWhenever you sacrifice a creature, you may draw a card.').
card_first_print('dragon appeasement', 'ARB').
card_image_name('dragon appeasement'/'ARB', 'dragon appeasement').
card_uid('dragon appeasement'/'ARB', 'ARB:Dragon Appeasement:dragon appeasement').
card_rarity('dragon appeasement'/'ARB', 'Uncommon').
card_artist('dragon appeasement'/'ARB', 'Paul Bonner').
card_number('dragon appeasement'/'ARB', '115').
card_flavor_text('dragon appeasement'/'ARB', '\"Look there—they carry that one upon a sedan chair. It must be their leader. Wait . . . Oh! Perhaps not.\"\n—Kaeda, aven scout').
card_multiverse_id('dragon appeasement'/'ARB', '180616').

card_in_set('dragon broodmother', 'ARB').
card_original_type('dragon broodmother'/'ARB', 'Creature — Dragon').
card_original_text('dragon broodmother'/'ARB', 'Flying\nAt the beginning of each upkeep, put a 1/1 red and green Dragon creature token with flying and devour 2 into play. (As the token comes into play, you may sacrifice any number of creatures. It comes into play with twice that many +1/+1 counters on it.)').
card_image_name('dragon broodmother'/'ARB', 'dragon broodmother').
card_uid('dragon broodmother'/'ARB', 'ARB:Dragon Broodmother:dragon broodmother').
card_rarity('dragon broodmother'/'ARB', 'Mythic Rare').
card_artist('dragon broodmother'/'ARB', 'Jaime Jones').
card_number('dragon broodmother'/'ARB', '53').
card_multiverse_id('dragon broodmother'/'ARB', '189648').

card_in_set('drastic revelation', 'ARB').
card_original_type('drastic revelation'/'ARB', 'Sorcery').
card_original_text('drastic revelation'/'ARB', 'Discard your hand. Draw seven cards, then discard three cards at random.').
card_first_print('drastic revelation', 'ARB').
card_image_name('drastic revelation'/'ARB', 'drastic revelation').
card_uid('drastic revelation'/'ARB', 'ARB:Drastic Revelation:drastic revelation').
card_rarity('drastic revelation'/'ARB', 'Uncommon').
card_artist('drastic revelation'/'ARB', 'Trevor Claxton').
card_number('drastic revelation'/'ARB', '111').
card_flavor_text('drastic revelation'/'ARB', 'Every disaster holds mystery, for lack of a sane witness.').
card_multiverse_id('drastic revelation'/'ARB', '179587').

card_in_set('enigma sphinx', 'ARB').
card_original_type('enigma sphinx'/'ARB', 'Artifact Creature — Sphinx').
card_original_text('enigma sphinx'/'ARB', 'Flying\nWhen Enigma Sphinx is put into your graveyard from play, put it into your library third from the top.\nCascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)').
card_first_print('enigma sphinx', 'ARB').
card_image_name('enigma sphinx'/'ARB', 'enigma sphinx').
card_uid('enigma sphinx'/'ARB', 'ARB:Enigma Sphinx:enigma sphinx').
card_rarity('enigma sphinx'/'ARB', 'Rare').
card_artist('enigma sphinx'/'ARB', 'Chris Rahn').
card_number('enigma sphinx'/'ARB', '106').
card_multiverse_id('enigma sphinx'/'ARB', '180597').

card_in_set('enlisted wurm', 'ARB').
card_original_type('enlisted wurm'/'ARB', 'Creature — Wurm').
card_original_text('enlisted wurm'/'ARB', 'Cascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)').
card_first_print('enlisted wurm', 'ARB').
card_image_name('enlisted wurm'/'ARB', 'enlisted wurm').
card_uid('enlisted wurm'/'ARB', 'ARB:Enlisted Wurm:enlisted wurm').
card_rarity('enlisted wurm'/'ARB', 'Uncommon').
card_artist('enlisted wurm'/'ARB', 'Steve Prescott').
card_number('enlisted wurm'/'ARB', '68').
card_flavor_text('enlisted wurm'/'ARB', 'A match for any army—even its own.').
card_multiverse_id('enlisted wurm'/'ARB', '185047').

card_in_set('esper sojourners', 'ARB').
card_original_type('esper sojourners'/'ARB', 'Artifact Creature — Vedalken Wizard').
card_original_text('esper sojourners'/'ARB', 'When you cycle Esper Sojourners or it\'s put into a graveyard from play, you may tap or untap target permanent.\nCycling {2}{U} ({2}{U}, Discard this card: Draw a card.)').
card_first_print('esper sojourners', 'ARB').
card_image_name('esper sojourners'/'ARB', 'esper sojourners').
card_uid('esper sojourners'/'ARB', 'ARB:Esper Sojourners:esper sojourners').
card_rarity('esper sojourners'/'ARB', 'Common').
card_artist('esper sojourners'/'ARB', 'John Avon').
card_number('esper sojourners'/'ARB', '107').
card_flavor_text('esper sojourners'/'ARB', 'The wilder the frontier, the greater the need for a controlling hand.').
card_multiverse_id('esper sojourners'/'ARB', '188967').

card_in_set('esper stormblade', 'ARB').
card_original_type('esper stormblade'/'ARB', 'Artifact Creature — Vedalken Wizard').
card_original_text('esper stormblade'/'ARB', 'As long as you control another multicolored permanent, Esper Stormblade gets +1/+1 and has flying.').
card_first_print('esper stormblade', 'ARB').
card_image_name('esper stormblade'/'ARB', 'esper stormblade').
card_uid('esper stormblade'/'ARB', 'ARB:Esper Stormblade:esper stormblade').
card_rarity('esper stormblade'/'ARB', 'Common').
card_artist('esper stormblade'/'ARB', 'Matt Stewart').
card_number('esper stormblade'/'ARB', '132').
card_flavor_text('esper stormblade'/'ARB', 'Stormcallers came to appreciate the novelty of Jund\'s volcanic updrafts.').
card_multiverse_id('esper stormblade'/'ARB', '188968').

card_in_set('ethercaste knight', 'ARB').
card_original_type('ethercaste knight'/'ARB', 'Artifact Creature — Human Knight').
card_original_text('ethercaste knight'/'ARB', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('ethercaste knight', 'ARB').
card_image_name('ethercaste knight'/'ARB', 'ethercaste knight').
card_uid('ethercaste knight'/'ARB', 'ARB:Ethercaste Knight:ethercaste knight').
card_rarity('ethercaste knight'/'ARB', 'Common').
card_artist('ethercaste knight'/'ARB', 'Steven Belledin').
card_number('ethercaste knight'/'ARB', '3').
card_flavor_text('ethercaste knight'/'ARB', '\"We coat ourselves in steel every day. What is etherium but the next logical step?\"').
card_multiverse_id('ethercaste knight'/'ARB', '179542').

card_in_set('etherium abomination', 'ARB').
card_original_type('etherium abomination'/'ARB', 'Artifact Creature — Horror').
card_original_text('etherium abomination'/'ARB', 'Unearth {1}{U}{B} ({1}{U}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('etherium abomination', 'ARB').
card_image_name('etherium abomination'/'ARB', 'etherium abomination').
card_uid('etherium abomination'/'ARB', 'ARB:Etherium Abomination:etherium abomination').
card_rarity('etherium abomination'/'ARB', 'Common').
card_artist('etherium abomination'/'ARB', 'Mitch Cotie').
card_number('etherium abomination'/'ARB', '20').
card_flavor_text('etherium abomination'/'ARB', 'It was given life by the etherium of a hundred slain Esperites.').
card_multiverse_id('etherium abomination'/'ARB', '179573').

card_in_set('ethersworn shieldmage', 'ARB').
card_original_type('ethersworn shieldmage'/'ARB', 'Artifact Creature — Vedalken Wizard').
card_original_text('ethersworn shieldmage'/'ARB', 'Flash\nWhen Ethersworn Shieldmage comes into play, prevent all damage that would be dealt to artifact creatures this turn.').
card_first_print('ethersworn shieldmage', 'ARB').
card_image_name('ethersworn shieldmage'/'ARB', 'ethersworn shieldmage').
card_uid('ethersworn shieldmage'/'ARB', 'ARB:Ethersworn Shieldmage:ethersworn shieldmage').
card_rarity('ethersworn shieldmage'/'ARB', 'Common').
card_artist('ethersworn shieldmage'/'ARB', 'Daarken').
card_number('ethersworn shieldmage'/'ARB', '4').
card_flavor_text('ethersworn shieldmage'/'ARB', '\"See, children of metal? Already your bodies are rejecting harm. Continue on our path, and perfection will be yours.\"').
card_multiverse_id('ethersworn shieldmage'/'ARB', '179578').

card_in_set('etherwrought page', 'ARB').
card_original_type('etherwrought page'/'ARB', 'Artifact').
card_original_text('etherwrought page'/'ARB', 'At the beginning of your upkeep, choose one — You gain 2 life; or look at the top card of your library, then you may put that card into your graveyard; or each opponent loses 1 life.').
card_first_print('etherwrought page', 'ARB').
card_image_name('etherwrought page'/'ARB', 'etherwrought page').
card_uid('etherwrought page'/'ARB', 'ARB:Etherwrought Page:etherwrought page').
card_rarity('etherwrought page'/'ARB', 'Uncommon').
card_artist('etherwrought page'/'ARB', 'Howard Lyon').
card_number('etherwrought page'/'ARB', '108').
card_flavor_text('etherwrought page'/'ARB', 'An unbound page is a riddle, free of cover and of context, a favorite token of the hegemon.').
card_multiverse_id('etherwrought page'/'ARB', '179618').

card_in_set('fieldmist borderpost', 'ARB').
card_original_type('fieldmist borderpost'/'ARB', 'Artifact').
card_original_text('fieldmist borderpost'/'ARB', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Fieldmist Borderpost\'s mana cost.\nFieldmist Borderpost comes into play tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('fieldmist borderpost', 'ARB').
card_image_name('fieldmist borderpost'/'ARB', 'fieldmist borderpost').
card_uid('fieldmist borderpost'/'ARB', 'ARB:Fieldmist Borderpost:fieldmist borderpost').
card_rarity('fieldmist borderpost'/'ARB', 'Common').
card_artist('fieldmist borderpost'/'ARB', 'Michael Bruinsma').
card_number('fieldmist borderpost'/'ARB', '5').
card_multiverse_id('fieldmist borderpost'/'ARB', '183005').

card_in_set('fight to the death', 'ARB').
card_original_type('fight to the death'/'ARB', 'Instant').
card_original_text('fight to the death'/'ARB', 'Destroy all blocking creatures and all blocked creatures.').
card_first_print('fight to the death', 'ARB').
card_image_name('fight to the death'/'ARB', 'fight to the death').
card_uid('fight to the death'/'ARB', 'ARB:Fight to the Death:fight to the death').
card_rarity('fight to the death'/'ARB', 'Rare').
card_artist('fight to the death'/'ARB', 'Michael Komarck').
card_number('fight to the death'/'ARB', '97').
card_flavor_text('fight to the death'/'ARB', 'When a battlefield falls silent, it\'s rarely due to a cease-fire.').
card_multiverse_id('fight to the death'/'ARB', '179562').

card_in_set('filigree angel', 'ARB').
card_original_type('filigree angel'/'ARB', 'Artifact Creature — Angel').
card_original_text('filigree angel'/'ARB', 'Flying\nWhen Filigree Angel comes into play, you gain 3 life for each artifact you control.').
card_first_print('filigree angel', 'ARB').
card_image_name('filigree angel'/'ARB', 'filigree angel').
card_uid('filigree angel'/'ARB', 'ARB:Filigree Angel:filigree angel').
card_rarity('filigree angel'/'ARB', 'Rare').
card_artist('filigree angel'/'ARB', 'Richard Whitters').
card_number('filigree angel'/'ARB', '6').
card_flavor_text('filigree angel'/'ARB', '\"I craved enlightenment, and Crucius\'s etherium opened my eyes. I would share my sight with you, but first you must believe.\"').
card_multiverse_id('filigree angel'/'ARB', '188965').

card_in_set('finest hour', 'ARB').
card_original_type('finest hour'/'ARB', 'Enchantment').
card_original_text('finest hour'/'ARB', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, if it\'s the first combat phase of the turn, untap that creature. After this phase, there is an additional combat phase.').
card_first_print('finest hour', 'ARB').
card_image_name('finest hour'/'ARB', 'finest hour').
card_uid('finest hour'/'ARB', 'ARB:Finest Hour:finest hour').
card_rarity('finest hour'/'ARB', 'Rare').
card_artist('finest hour'/'ARB', 'Michael Komarck').
card_number('finest hour'/'ARB', '126').
card_multiverse_id('finest hour'/'ARB', '180609').

card_in_set('firewild borderpost', 'ARB').
card_original_type('firewild borderpost'/'ARB', 'Artifact').
card_original_text('firewild borderpost'/'ARB', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Firewild Borderpost\'s mana cost.\nFirewild Borderpost comes into play tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('firewild borderpost', 'ARB').
card_image_name('firewild borderpost'/'ARB', 'firewild borderpost').
card_uid('firewild borderpost'/'ARB', 'ARB:Firewild Borderpost:firewild borderpost').
card_rarity('firewild borderpost'/'ARB', 'Common').
card_artist('firewild borderpost'/'ARB', 'Jean-Sébastien Rossbach').
card_number('firewild borderpost'/'ARB', '54').
card_multiverse_id('firewild borderpost'/'ARB', '188974').

card_in_set('flurry of wings', 'ARB').
card_original_type('flurry of wings'/'ARB', 'Instant').
card_original_text('flurry of wings'/'ARB', 'Put X 1/1 white Bird Soldier creature tokens with flying into play, where X is the number of attacking creatures.').
card_first_print('flurry of wings', 'ARB').
card_image_name('flurry of wings'/'ARB', 'flurry of wings').
card_uid('flurry of wings'/'ARB', 'ARB:Flurry of Wings:flurry of wings').
card_rarity('flurry of wings'/'ARB', 'Uncommon').
card_artist('flurry of wings'/'ARB', 'Matt Cavotta').
card_number('flurry of wings'/'ARB', '127').
card_flavor_text('flurry of wings'/'ARB', '\"We are born soldiers. The call to battle is a homecoming.\"').
card_multiverse_id('flurry of wings'/'ARB', '179605').

card_in_set('giant ambush beetle', 'ARB').
card_original_type('giant ambush beetle'/'ARB', 'Creature — Insect').
card_original_text('giant ambush beetle'/'ARB', 'Haste\nWhen Giant Ambush Beetle comes into play, you may have target creature block it this turn if able.').
card_first_print('giant ambush beetle', 'ARB').
card_image_name('giant ambush beetle'/'ARB', 'giant ambush beetle').
card_uid('giant ambush beetle'/'ARB', 'ARB:Giant Ambush Beetle:giant ambush beetle').
card_rarity('giant ambush beetle'/'ARB', 'Uncommon').
card_artist('giant ambush beetle'/'ARB', 'Carl Critchlow').
card_number('giant ambush beetle'/'ARB', '137').
card_flavor_text('giant ambush beetle'/'ARB', 'It waits with absolute stillness. One might think it a statue if it weren\'t occasionally seen chewing on its victims.').
card_multiverse_id('giant ambush beetle'/'ARB', '179598').

card_in_set('glassdust hulk', 'ARB').
card_original_type('glassdust hulk'/'ARB', 'Artifact Creature — Golem').
card_original_text('glassdust hulk'/'ARB', 'Whenever another artifact comes into play under your control, Glassdust Hulk gets +1/+1 until end of turn and is unblockable this turn.\nCycling {W/U} ({W/U}, Discard this card: Draw a card.)').
card_first_print('glassdust hulk', 'ARB').
card_image_name('glassdust hulk'/'ARB', 'glassdust hulk').
card_uid('glassdust hulk'/'ARB', 'ARB:Glassdust Hulk:glassdust hulk').
card_rarity('glassdust hulk'/'ARB', 'Common').
card_artist('glassdust hulk'/'ARB', 'Franz Vohwinkel').
card_number('glassdust hulk'/'ARB', '7').
card_multiverse_id('glassdust hulk'/'ARB', '179576').

card_in_set('glory of warfare', 'ARB').
card_original_type('glory of warfare'/'ARB', 'Enchantment').
card_original_text('glory of warfare'/'ARB', 'As long as it\'s your turn, creatures you control get +2/+0.\nAs long as it\'s not your turn, creatures you control get +0/+2.').
card_first_print('glory of warfare', 'ARB').
card_image_name('glory of warfare'/'ARB', 'glory of warfare').
card_uid('glory of warfare'/'ARB', 'ARB:Glory of Warfare:glory of warfare').
card_rarity('glory of warfare'/'ARB', 'Rare').
card_artist('glory of warfare'/'ARB', 'Paolo Parente').
card_number('glory of warfare'/'ARB', '98').
card_flavor_text('glory of warfare'/'ARB', '\"Their hopes dashed on our shields! Their victory slashed by our swords!\"').
card_multiverse_id('glory of warfare'/'ARB', '179611').

card_in_set('gloryscale viashino', 'ARB').
card_original_type('gloryscale viashino'/'ARB', 'Creature — Viashino Soldier').
card_original_text('gloryscale viashino'/'ARB', 'Whenever you play a multicolored spell, Gloryscale Viashino gets +3/+3 until end of turn.').
card_first_print('gloryscale viashino', 'ARB').
card_image_name('gloryscale viashino'/'ARB', 'gloryscale viashino').
card_uid('gloryscale viashino'/'ARB', 'ARB:Gloryscale Viashino:gloryscale viashino').
card_rarity('gloryscale viashino'/'ARB', 'Uncommon').
card_artist('gloryscale viashino'/'ARB', 'Volkan Baga').
card_number('gloryscale viashino'/'ARB', '120').
card_flavor_text('gloryscale viashino'/'ARB', '\"This is a new world and a new war. We need warriors who know chaos and how to combat it.\"\n—Lisha of the Azure').
card_multiverse_id('gloryscale viashino'/'ARB', '189654').

card_in_set('godtracker of jund', 'ARB').
card_original_type('godtracker of jund'/'ARB', 'Creature — Elf Shaman').
card_original_text('godtracker of jund'/'ARB', 'Whenever a creature with power 5 or greater comes into play under your control, you may put a +1/+1 counter on Godtracker of Jund.').
card_first_print('godtracker of jund', 'ARB').
card_image_name('godtracker of jund'/'ARB', 'godtracker of jund').
card_uid('godtracker of jund'/'ARB', 'ARB:Godtracker of Jund:godtracker of jund').
card_rarity('godtracker of jund'/'ARB', 'Common').
card_artist('godtracker of jund'/'ARB', 'Justin Sweet').
card_number('godtracker of jund'/'ARB', '55').
card_flavor_text('godtracker of jund'/'ARB', '\"The behemoths are content to graze like cattle, while dragons rage and conquer. I know which example I\'ll follow.\"').
card_multiverse_id('godtracker of jund'/'ARB', '183994').

card_in_set('gorger wurm', 'ARB').
card_original_type('gorger wurm'/'ARB', 'Creature — Wurm').
card_original_text('gorger wurm'/'ARB', 'Devour 1 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with that many +1/+1 counters on it.)').
card_first_print('gorger wurm', 'ARB').
card_image_name('gorger wurm'/'ARB', 'gorger wurm').
card_uid('gorger wurm'/'ARB', 'ARB:Gorger Wurm:gorger wurm').
card_rarity('gorger wurm'/'ARB', 'Common').
card_artist('gorger wurm'/'ARB', 'Paolo Parente').
card_number('gorger wurm'/'ARB', '56').
card_flavor_text('gorger wurm'/'ARB', 'In war-torn Alara, sometimes a wurm\'s gullet is the safest place to be.').
card_multiverse_id('gorger wurm'/'ARB', '179561').

card_in_set('grixis grimblade', 'ARB').
card_original_type('grixis grimblade'/'ARB', 'Creature — Zombie Warrior').
card_original_text('grixis grimblade'/'ARB', 'As long as you control another multicolored permanent, Grixis Grimblade gets +1/+1 and has deathtouch. (Whenever it deals damage to a creature, destroy that creature.)').
card_first_print('grixis grimblade', 'ARB').
card_image_name('grixis grimblade'/'ARB', 'grixis grimblade').
card_uid('grixis grimblade'/'ARB', 'ARB:Grixis Grimblade:grixis grimblade').
card_rarity('grixis grimblade'/'ARB', 'Common').
card_artist('grixis grimblade'/'ARB', 'Trevor Claxton').
card_number('grixis grimblade'/'ARB', '134').
card_multiverse_id('grixis grimblade'/'ARB', '188976').

card_in_set('grixis sojourners', 'ARB').
card_original_type('grixis sojourners'/'ARB', 'Creature — Zombie Ogre').
card_original_text('grixis sojourners'/'ARB', 'When you cycle Grixis Sojourners or it\'s put into a graveyard from play, you may remove target card in a graveyard from the game.\nCycling {2}{B} ({2}{B}, Discard this card: Draw a card.)').
card_first_print('grixis sojourners', 'ARB').
card_image_name('grixis sojourners'/'ARB', 'grixis sojourners').
card_uid('grixis sojourners'/'ARB', 'ARB:Grixis Sojourners:grixis sojourners').
card_rarity('grixis sojourners'/'ARB', 'Common').
card_artist('grixis sojourners'/'ARB', 'John Avon').
card_number('grixis sojourners'/'ARB', '112').
card_multiverse_id('grixis sojourners'/'ARB', '188972').

card_in_set('grizzled leotau', 'ARB').
card_original_type('grizzled leotau'/'ARB', 'Creature — Cat').
card_original_text('grizzled leotau'/'ARB', '').
card_first_print('grizzled leotau', 'ARB').
card_image_name('grizzled leotau'/'ARB', 'grizzled leotau').
card_uid('grizzled leotau'/'ARB', 'ARB:Grizzled Leotau:grizzled leotau').
card_rarity('grizzled leotau'/'ARB', 'Common').
card_artist('grizzled leotau'/'ARB', 'Lars Grant-West').
card_number('grizzled leotau'/'ARB', '69').
card_flavor_text('grizzled leotau'/'ARB', '\"There is no glory in a death of age, as even the leotau know. As winter steals into their coats, they seek the deadliest lands, that they may die as they lived.\"\n—Aarsil the Blessed').
card_multiverse_id('grizzled leotau'/'ARB', '189647').

card_in_set('identity crisis', 'ARB').
card_original_type('identity crisis'/'ARB', 'Sorcery').
card_original_text('identity crisis'/'ARB', 'Remove all cards in target player\'s hand and graveyard from the game.').
card_first_print('identity crisis', 'ARB').
card_image_name('identity crisis'/'ARB', 'identity crisis').
card_uid('identity crisis'/'ARB', 'ARB:Identity Crisis:identity crisis').
card_rarity('identity crisis'/'ARB', 'Rare').
card_artist('identity crisis'/'ARB', 'Thomas M. Baxa').
card_number('identity crisis'/'ARB', '81').
card_flavor_text('identity crisis'/'ARB', '\"To join our ranks, one must be pure. To be pure, one must be blessed by etherium. To be blessed, one must forget oneself.\"\n—Ethersworn prayer').
card_multiverse_id('identity crisis'/'ARB', '175109').

card_in_set('igneous pouncer', 'ARB').
card_original_type('igneous pouncer'/'ARB', 'Creature — Elemental').
card_original_text('igneous pouncer'/'ARB', 'Haste\nSwampcycling {2}, mountaincycling {2} ({2}, Discard this card: Search your library for a Swamp or Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('igneous pouncer', 'ARB').
card_image_name('igneous pouncer'/'ARB', 'igneous pouncer').
card_uid('igneous pouncer'/'ARB', 'ARB:Igneous Pouncer:igneous pouncer').
card_rarity('igneous pouncer'/'ARB', 'Common').
card_artist('igneous pouncer'/'ARB', 'Chippy').
card_number('igneous pouncer'/'ARB', '40').
card_multiverse_id('igneous pouncer'/'ARB', '144276').

card_in_set('illusory demon', 'ARB').
card_original_type('illusory demon'/'ARB', 'Creature — Demon Illusion').
card_original_text('illusory demon'/'ARB', 'Flying\nWhen you play a spell, sacrifice Illusory Demon.').
card_first_print('illusory demon', 'ARB').
card_image_name('illusory demon'/'ARB', 'illusory demon').
card_uid('illusory demon'/'ARB', 'ARB:Illusory Demon:illusory demon').
card_rarity('illusory demon'/'ARB', 'Uncommon').
card_artist('illusory demon'/'ARB', 'Nils Hamm').
card_number('illusory demon'/'ARB', '21').
card_flavor_text('illusory demon'/'ARB', 'In the Maelstrom, a trick of the light can feast on human flesh.').
card_multiverse_id('illusory demon'/'ARB', '179596').

card_in_set('intimidation bolt', 'ARB').
card_original_type('intimidation bolt'/'ARB', 'Instant').
card_original_text('intimidation bolt'/'ARB', 'Intimidation Bolt deals 3 damage to target creature. Other creatures can\'t attack this turn.').
card_first_print('intimidation bolt', 'ARB').
card_image_name('intimidation bolt'/'ARB', 'intimidation bolt').
card_uid('intimidation bolt'/'ARB', 'ARB:Intimidation Bolt:intimidation bolt').
card_rarity('intimidation bolt'/'ARB', 'Uncommon').
card_artist('intimidation bolt'/'ARB', 'Michael Bruinsma').
card_number('intimidation bolt'/'ARB', '99').
card_flavor_text('intimidation bolt'/'ARB', 'For hours afterward, all they could see was the grisly silhouette of their master, screaming.').
card_multiverse_id('intimidation bolt'/'ARB', '179568').

card_in_set('jenara, asura of war', 'ARB').
card_original_type('jenara, asura of war'/'ARB', 'Legendary Creature — Angel').
card_original_text('jenara, asura of war'/'ARB', 'Flying\n{1}{W}: Put a +1/+1 counter on Jenara, Asura of War.').
card_first_print('jenara, asura of war', 'ARB').
card_image_name('jenara, asura of war'/'ARB', 'jenara, asura of war').
card_uid('jenara, asura of war'/'ARB', 'ARB:Jenara, Asura of War:jenara, asura of war').
card_rarity('jenara, asura of war'/'ARB', 'Mythic Rare').
card_artist('jenara, asura of war'/'ARB', 'Chris Rahn').
card_number('jenara, asura of war'/'ARB', '128').
card_flavor_text('jenara, asura of war'/'ARB', 'Wounded soldiers looked up, grateful for her appearance. But she passed over them, her eyes firmly on their foe.').
card_multiverse_id('jenara, asura of war'/'ARB', '180605').

card_in_set('jhessian zombies', 'ARB').
card_original_type('jhessian zombies'/'ARB', 'Creature — Zombie').
card_original_text('jhessian zombies'/'ARB', 'Fear\nIslandcycling {2}, swampcycling {2} ({2}, Discard this card: Search your library for an Island or Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('jhessian zombies', 'ARB').
card_image_name('jhessian zombies'/'ARB', 'jhessian zombies').
card_uid('jhessian zombies'/'ARB', 'ARB:Jhessian Zombies:jhessian zombies').
card_rarity('jhessian zombies'/'ARB', 'Common').
card_artist('jhessian zombies'/'ARB', 'Ash Wood').
card_number('jhessian zombies'/'ARB', '22').
card_multiverse_id('jhessian zombies'/'ARB', '144286').

card_in_set('jund hackblade', 'ARB').
card_original_type('jund hackblade'/'ARB', 'Creature — Goblin Berserker').
card_original_text('jund hackblade'/'ARB', 'As long as you control another multicolored permanent, Jund Hackblade gets +1/+1 and has haste.').
card_first_print('jund hackblade', 'ARB').
card_image_name('jund hackblade'/'ARB', 'jund hackblade').
card_uid('jund hackblade'/'ARB', 'ARB:Jund Hackblade:jund hackblade').
card_rarity('jund hackblade'/'ARB', 'Common').
card_artist('jund hackblade'/'ARB', 'Dan Dos Santos').
card_number('jund hackblade'/'ARB', '138').
card_flavor_text('jund hackblade'/'ARB', 'The knights were halfway through their precombat rites before they realized the foul brute meant to start without them.').
card_multiverse_id('jund hackblade'/'ARB', '188973').

card_in_set('jund sojourners', 'ARB').
card_original_type('jund sojourners'/'ARB', 'Creature — Viashino Shaman').
card_original_text('jund sojourners'/'ARB', 'When you cycle Jund Sojourners or it\'s put into a graveyard from play, you may have it deal 1 damage to target creature or player.\nCycling {2}{R} ({2}{R}, Discard this card: Draw a card.)').
card_first_print('jund sojourners', 'ARB').
card_image_name('jund sojourners'/'ARB', 'jund sojourners').
card_uid('jund sojourners'/'ARB', 'ARB:Jund Sojourners:jund sojourners').
card_rarity('jund sojourners'/'ARB', 'Common').
card_artist('jund sojourners'/'ARB', 'John Avon').
card_number('jund sojourners'/'ARB', '116').
card_multiverse_id('jund sojourners'/'ARB', '188966').

card_in_set('karrthus, tyrant of jund', 'ARB').
card_original_type('karrthus, tyrant of jund'/'ARB', 'Legendary Creature — Dragon').
card_original_text('karrthus, tyrant of jund'/'ARB', 'Flying, haste\nWhen Karrthus, Tyrant of Jund comes into play, gain control of all Dragons, then untap all Dragons.\nOther Dragon creatures you control have haste.').
card_first_print('karrthus, tyrant of jund', 'ARB').
card_image_name('karrthus, tyrant of jund'/'ARB', 'karrthus, tyrant of jund').
card_uid('karrthus, tyrant of jund'/'ARB', 'ARB:Karrthus, Tyrant of Jund:karrthus, tyrant of jund').
card_rarity('karrthus, tyrant of jund'/'ARB', 'Mythic Rare').
card_artist('karrthus, tyrant of jund'/'ARB', 'Dave Kendall').
card_number('karrthus, tyrant of jund'/'ARB', '117').
card_multiverse_id('karrthus, tyrant of jund'/'ARB', '180587').

card_in_set('kathari bomber', 'ARB').
card_original_type('kathari bomber'/'ARB', 'Creature — Bird Shaman').
card_original_text('kathari bomber'/'ARB', 'Flying\nWhen Kathari Bomber deals combat damage to a player, put two 1/1 red Goblin creature tokens into play and sacrifice Kathari Bomber.\nUnearth {3}{B}{R} ({3}{B}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('kathari bomber', 'ARB').
card_image_name('kathari bomber'/'ARB', 'kathari bomber').
card_uid('kathari bomber'/'ARB', 'ARB:Kathari Bomber:kathari bomber').
card_rarity('kathari bomber'/'ARB', 'Common').
card_artist('kathari bomber'/'ARB', 'Carl Critchlow').
card_number('kathari bomber'/'ARB', '41').
card_multiverse_id('kathari bomber'/'ARB', '179602').

card_in_set('kathari remnant', 'ARB').
card_original_type('kathari remnant'/'ARB', 'Creature — Bird Skeleton').
card_original_text('kathari remnant'/'ARB', 'Flying\n{B}: Regenerate Kathari Remnant.\nCascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)').
card_first_print('kathari remnant', 'ARB').
card_image_name('kathari remnant'/'ARB', 'kathari remnant').
card_uid('kathari remnant'/'ARB', 'ARB:Kathari Remnant:kathari remnant').
card_rarity('kathari remnant'/'ARB', 'Uncommon').
card_artist('kathari remnant'/'ARB', 'Anthony S. Waters').
card_number('kathari remnant'/'ARB', '23').
card_multiverse_id('kathari remnant'/'ARB', '185060').

card_in_set('knight of new alara', 'ARB').
card_original_type('knight of new alara'/'ARB', 'Creature — Human Knight').
card_original_text('knight of new alara'/'ARB', 'Each other multicolored creature you control gets +1/+1 for each of its colors.').
card_image_name('knight of new alara'/'ARB', 'knight of new alara').
card_uid('knight of new alara'/'ARB', 'ARB:Knight of New Alara:knight of new alara').
card_rarity('knight of new alara'/'ARB', 'Rare').
card_artist('knight of new alara'/'ARB', 'Chris Rahn').
card_number('knight of new alara'/'ARB', '70').
card_flavor_text('knight of new alara'/'ARB', '\"I embrace all of Alara, Grixis as much as Bant. Let my former home call me traitor. I do this not for accolades, but because it is right.\"').
card_multiverse_id('knight of new alara'/'ARB', '189642').

card_in_set('knotvine paladin', 'ARB').
card_original_type('knotvine paladin'/'ARB', 'Creature — Human Knight').
card_original_text('knotvine paladin'/'ARB', 'Whenever Knotvine Paladin attacks, it gets +1/+1 until end of turn for each untapped creature you control.').
card_first_print('knotvine paladin', 'ARB').
card_image_name('knotvine paladin'/'ARB', 'knotvine paladin').
card_uid('knotvine paladin'/'ARB', 'ARB:Knotvine Paladin:knotvine paladin').
card_rarity('knotvine paladin'/'ARB', 'Rare').
card_artist('knotvine paladin'/'ARB', 'Matt Stewart').
card_number('knotvine paladin'/'ARB', '71').
card_flavor_text('knotvine paladin'/'ARB', '\"He is part of a new breed of guardian, one who embraces the best of both his world and ours.\"\n—Mayael the Anima').
card_multiverse_id('knotvine paladin'/'ARB', '180624').

card_in_set('lavalanche', 'ARB').
card_original_type('lavalanche'/'ARB', 'Sorcery').
card_original_text('lavalanche'/'ARB', 'Lavalanche deals X damage to target player and each creature he or she controls.').
card_first_print('lavalanche', 'ARB').
card_image_name('lavalanche'/'ARB', 'lavalanche').
card_uid('lavalanche'/'ARB', 'ARB:Lavalanche:lavalanche').
card_rarity('lavalanche'/'ARB', 'Rare').
card_artist('lavalanche'/'ARB', 'Steve Argyle').
card_number('lavalanche'/'ARB', '118').
card_flavor_text('lavalanche'/'ARB', 'Alara\'s burning blood cannot be contained by mere earth and stone.').
card_multiverse_id('lavalanche'/'ARB', '180612').

card_in_set('leonin armorguard', 'ARB').
card_original_type('leonin armorguard'/'ARB', 'Creature — Cat Soldier').
card_original_text('leonin armorguard'/'ARB', 'When Leonin Armorguard comes into play, creatures you control get +1/+1 until end of turn.').
card_first_print('leonin armorguard', 'ARB').
card_image_name('leonin armorguard'/'ARB', 'leonin armorguard').
card_uid('leonin armorguard'/'ARB', 'ARB:Leonin Armorguard:leonin armorguard').
card_rarity('leonin armorguard'/'ARB', 'Common').
card_artist('leonin armorguard'/'ARB', 'Steven Belledin').
card_number('leonin armorguard'/'ARB', '72').
card_flavor_text('leonin armorguard'/'ARB', 'Well suited are the nacatl to a coat of steel and forge-tempered claws.').
card_multiverse_id('leonin armorguard'/'ARB', '180606').

card_in_set('lich lord of unx', 'ARB').
card_original_type('lich lord of unx'/'ARB', 'Creature — Zombie Wizard').
card_original_text('lich lord of unx'/'ARB', '{U}{B}, {T}: Put a 1/1 blue and black Zombie Wizard creature token into play.\n{U}{U}{B}{B}: Target player loses X life and puts the top X cards of his or her library into his or her graveyard, where X is the number of Zombies you control.').
card_first_print('lich lord of unx', 'ARB').
card_image_name('lich lord of unx'/'ARB', 'lich lord of unx').
card_uid('lich lord of unx'/'ARB', 'ARB:Lich Lord of Unx:lich lord of unx').
card_rarity('lich lord of unx'/'ARB', 'Rare').
card_artist('lich lord of unx'/'ARB', 'Dave Allsop').
card_number('lich lord of unx'/'ARB', '24').
card_multiverse_id('lich lord of unx'/'ARB', '179555').

card_in_set('lightning reaver', 'ARB').
card_original_type('lightning reaver'/'ARB', 'Creature — Zombie Beast').
card_original_text('lightning reaver'/'ARB', 'Fear, haste\nWhenever Lightning Reaver deals combat damage to a player, put a charge counter on it.\nAt the end of your turn, Lightning Reaver deals damage equal to the number of charge counters on it to each opponent.').
card_first_print('lightning reaver', 'ARB').
card_image_name('lightning reaver'/'ARB', 'lightning reaver').
card_uid('lightning reaver'/'ARB', 'ARB:Lightning Reaver:lightning reaver').
card_rarity('lightning reaver'/'ARB', 'Rare').
card_artist('lightning reaver'/'ARB', 'Cyril Van Der Haegen').
card_number('lightning reaver'/'ARB', '42').
card_multiverse_id('lightning reaver'/'ARB', '180617').

card_in_set('lord of extinction', 'ARB').
card_original_type('lord of extinction'/'ARB', 'Creature — Elemental').
card_original_text('lord of extinction'/'ARB', 'Lord of Extinction\'s power and toughness are each equal to the number of cards in all graveyards.').
card_first_print('lord of extinction', 'ARB').
card_image_name('lord of extinction'/'ARB', 'lord of extinction').
card_uid('lord of extinction'/'ARB', 'ARB:Lord of Extinction:lord of extinction').
card_rarity('lord of extinction'/'ARB', 'Mythic Rare').
card_artist('lord of extinction'/'ARB', 'Izzy').
card_number('lord of extinction'/'ARB', '91').
card_flavor_text('lord of extinction'/'ARB', 'The forlorn cries of the dead combine to make its laugh.').
card_multiverse_id('lord of extinction'/'ARB', '177922').

card_in_set('lorescale coatl', 'ARB').
card_original_type('lorescale coatl'/'ARB', 'Creature — Snake').
card_original_text('lorescale coatl'/'ARB', 'Whenever you draw a card, you may put a +1/+1 counter on Lorescale Coatl.').
card_first_print('lorescale coatl', 'ARB').
card_image_name('lorescale coatl'/'ARB', 'lorescale coatl').
card_uid('lorescale coatl'/'ARB', 'ARB:Lorescale Coatl:lorescale coatl').
card_rarity('lorescale coatl'/'ARB', 'Uncommon').
card_artist('lorescale coatl'/'ARB', 'Greg Staples').
card_number('lorescale coatl'/'ARB', '101').
card_flavor_text('lorescale coatl'/'ARB', '\"The enlightenment I never found in etherium I have found traced in the coatl\'s scales.\"\n—Ranalus, vedalken heretic').
card_multiverse_id('lorescale coatl'/'ARB', '179633').

card_in_set('madrush cyclops', 'ARB').
card_original_type('madrush cyclops'/'ARB', 'Creature — Cyclops Warrior').
card_original_text('madrush cyclops'/'ARB', 'Creatures you control have haste.').
card_first_print('madrush cyclops', 'ARB').
card_image_name('madrush cyclops'/'ARB', 'madrush cyclops').
card_uid('madrush cyclops'/'ARB', 'ARB:Madrush Cyclops:madrush cyclops').
card_rarity('madrush cyclops'/'ARB', 'Rare').
card_artist('madrush cyclops'/'ARB', 'Wayne Reynolds').
card_number('madrush cyclops'/'ARB', '119').
card_flavor_text('madrush cyclops'/'ARB', 'Death comes in the blink of an eye.').
card_multiverse_id('madrush cyclops'/'ARB', '180625').

card_in_set('maelstrom nexus', 'ARB').
card_original_type('maelstrom nexus'/'ARB', 'Enchantment').
card_original_text('maelstrom nexus'/'ARB', 'The first spell you play each turn has cascade. (When you play your first spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)').
card_first_print('maelstrom nexus', 'ARB').
card_image_name('maelstrom nexus'/'ARB', 'maelstrom nexus').
card_uid('maelstrom nexus'/'ARB', 'ARB:Maelstrom Nexus:maelstrom nexus').
card_rarity('maelstrom nexus'/'ARB', 'Mythic Rare').
card_artist('maelstrom nexus'/'ARB', 'Steven Belledin').
card_number('maelstrom nexus'/'ARB', '130').
card_multiverse_id('maelstrom nexus'/'ARB', '179540').

card_in_set('maelstrom pulse', 'ARB').
card_original_type('maelstrom pulse'/'ARB', 'Sorcery').
card_original_text('maelstrom pulse'/'ARB', 'Destroy target nonland permanent and all other permanents with the same name as that permanent.').
card_image_name('maelstrom pulse'/'ARB', 'maelstrom pulse').
card_uid('maelstrom pulse'/'ARB', 'ARB:Maelstrom Pulse:maelstrom pulse').
card_rarity('maelstrom pulse'/'ARB', 'Rare').
card_artist('maelstrom pulse'/'ARB', 'Anthony Francisco').
card_number('maelstrom pulse'/'ARB', '92').
card_flavor_text('maelstrom pulse'/'ARB', 'During the collision of the shards, entire ways of life disappeared without a trace.').
card_multiverse_id('maelstrom pulse'/'ARB', '180613').

card_in_set('mage slayer', 'ARB').
card_original_type('mage slayer'/'ARB', 'Artifact — Equipment').
card_original_text('mage slayer'/'ARB', 'Whenever equipped creature attacks, it deals damage equal to its power to defending player.\nEquip {3}').
card_first_print('mage slayer', 'ARB').
card_image_name('mage slayer'/'ARB', 'mage slayer').
card_uid('mage slayer'/'ARB', 'ARB:Mage Slayer:mage slayer').
card_rarity('mage slayer'/'ARB', 'Uncommon').
card_artist('mage slayer'/'ARB', 'Lars Grant-West').
card_number('mage slayer'/'ARB', '57').
card_flavor_text('mage slayer'/'ARB', '\"When the wielder is strong enough, any sword will do.\"\n—Kresh the Bloodbraided').
card_multiverse_id('mage slayer'/'ARB', '179567').

card_in_set('magefire wings', 'ARB').
card_original_type('magefire wings'/'ARB', 'Enchantment — Aura').
card_original_text('magefire wings'/'ARB', 'Enchant creature\nEnchanted creature gets +2/+0 and has flying.').
card_first_print('magefire wings', 'ARB').
card_image_name('magefire wings'/'ARB', 'magefire wings').
card_uid('magefire wings'/'ARB', 'ARB:Magefire Wings:magefire wings').
card_rarity('magefire wings'/'ARB', 'Common').
card_artist('magefire wings'/'ARB', 'Zoltan Boros & Gabor Szikszai').
card_number('magefire wings'/'ARB', '88').
card_flavor_text('magefire wings'/'ARB', 'Giving a thing wings does not take away its fear of flight—or its fear of fire, for that matter.').
card_multiverse_id('magefire wings'/'ARB', '179591').

card_in_set('marisi\'s twinclaws', 'ARB').
card_original_type('marisi\'s twinclaws'/'ARB', 'Creature — Cat Warrior').
card_original_text('marisi\'s twinclaws'/'ARB', 'Double strike').
card_image_name('marisi\'s twinclaws'/'ARB', 'marisi\'s twinclaws').
card_uid('marisi\'s twinclaws'/'ARB', 'ARB:Marisi\'s Twinclaws:marisi\'s twinclaws').
card_rarity('marisi\'s twinclaws'/'ARB', 'Uncommon').
card_artist('marisi\'s twinclaws'/'ARB', 'Izzy').
card_number('marisi\'s twinclaws'/'ARB', '140').
card_flavor_text('marisi\'s twinclaws'/'ARB', '\"Looks like we\'re completely outnumbered.\"\n\"Lucky for us. I thought we\'d be bored.\"').
card_multiverse_id('marisi\'s twinclaws'/'ARB', '179585').

card_in_set('marrow chomper', 'ARB').
card_original_type('marrow chomper'/'ARB', 'Creature — Zombie Lizard').
card_original_text('marrow chomper'/'ARB', 'Devour 2 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with twice that many +1/+1 counters on it.)\nWhen Marrow Chomper comes into play, you gain 2 life for each creature it devoured.').
card_first_print('marrow chomper', 'ARB').
card_image_name('marrow chomper'/'ARB', 'marrow chomper').
card_uid('marrow chomper'/'ARB', 'ARB:Marrow Chomper:marrow chomper').
card_rarity('marrow chomper'/'ARB', 'Uncommon').
card_artist('marrow chomper'/'ARB', 'Lars Grant-West').
card_number('marrow chomper'/'ARB', '93').
card_multiverse_id('marrow chomper'/'ARB', '183014').

card_in_set('mask of riddles', 'ARB').
card_original_type('mask of riddles'/'ARB', 'Artifact — Equipment').
card_original_text('mask of riddles'/'ARB', 'Equipped creature has fear.\nWhenever equipped creature deals combat damage to a player, you may draw a card.\nEquip {2}').
card_first_print('mask of riddles', 'ARB').
card_image_name('mask of riddles'/'ARB', 'mask of riddles').
card_uid('mask of riddles'/'ARB', 'ARB:Mask of Riddles:mask of riddles').
card_rarity('mask of riddles'/'ARB', 'Uncommon').
card_artist('mask of riddles'/'ARB', 'Matt Cavotta').
card_number('mask of riddles'/'ARB', '25').
card_multiverse_id('mask of riddles'/'ARB', '179584').

card_in_set('mayael\'s aria', 'ARB').
card_original_type('mayael\'s aria'/'ARB', 'Enchantment').
card_original_text('mayael\'s aria'/'ARB', 'At the beginning of your upkeep, put a +1/+1 counter on each creature you control if you control a creature with power 5 or greater. Then you gain 10 life if you control a creature with power 10 or greater. Then you win the game if you control a creature with power 20 or greater.').
card_first_print('mayael\'s aria', 'ARB').
card_image_name('mayael\'s aria'/'ARB', 'mayael\'s aria').
card_uid('mayael\'s aria'/'ARB', 'ARB:Mayael\'s Aria:mayael\'s aria').
card_rarity('mayael\'s aria'/'ARB', 'Rare').
card_artist('mayael\'s aria'/'ARB', 'Steve Argyle').
card_number('mayael\'s aria'/'ARB', '121').
card_multiverse_id('mayael\'s aria'/'ARB', '180620').

card_in_set('meddling mage', 'ARB').
card_original_type('meddling mage'/'ARB', 'Creature — Human Wizard').
card_original_text('meddling mage'/'ARB', 'As Meddling Mage comes into play, name a nonland card.\nThe named card can\'t be played.').
card_image_name('meddling mage'/'ARB', 'meddling mage').
card_uid('meddling mage'/'ARB', 'ARB:Meddling Mage:meddling mage').
card_rarity('meddling mage'/'ARB', 'Rare').
card_artist('meddling mage'/'ARB', 'Todd Lockwood').
card_number('meddling mage'/'ARB', '8').
card_flavor_text('meddling mage'/'ARB', '\"This violent wasteland is an indictment of its people. These cowards lack the will to oppose disorder.\"').
card_multiverse_id('meddling mage'/'ARB', '179547').

card_in_set('messenger falcons', 'ARB').
card_original_type('messenger falcons'/'ARB', 'Creature — Bird').
card_original_text('messenger falcons'/'ARB', 'Flying\nWhen Messenger Falcons comes into play, draw a card.').
card_first_print('messenger falcons', 'ARB').
card_image_name('messenger falcons'/'ARB', 'messenger falcons').
card_uid('messenger falcons'/'ARB', 'ARB:Messenger Falcons:messenger falcons').
card_rarity('messenger falcons'/'ARB', 'Uncommon').
card_artist('messenger falcons'/'ARB', 'David Palumbo').
card_number('messenger falcons'/'ARB', '145').
card_flavor_text('messenger falcons'/'ARB', 'So many aven had died scouting the distant fronts that the knight-captains recruited Nayan falcons as couriers.').
card_multiverse_id('messenger falcons'/'ARB', '189644').

card_in_set('mind funeral', 'ARB').
card_original_type('mind funeral'/'ARB', 'Sorcery').
card_original_text('mind funeral'/'ARB', 'Target opponent reveals cards from the top of his or her library until four land cards are revealed. That player puts all cards revealed this way into his or her graveyard.').
card_first_print('mind funeral', 'ARB').
card_image_name('mind funeral'/'ARB', 'mind funeral').
card_uid('mind funeral'/'ARB', 'ARB:Mind Funeral:mind funeral').
card_rarity('mind funeral'/'ARB', 'Uncommon').
card_artist('mind funeral'/'ARB', 'rk post').
card_number('mind funeral'/'ARB', '26').
card_flavor_text('mind funeral'/'ARB', '\"The soul is only as eternal as I say it is.\"\n—Sharuum the Hegemon').
card_multiverse_id('mind funeral'/'ARB', '179599').

card_in_set('mistvein borderpost', 'ARB').
card_original_type('mistvein borderpost'/'ARB', 'Artifact').
card_original_text('mistvein borderpost'/'ARB', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Mistvein Borderpost\'s mana cost.\nMistvein Borderpost comes into play tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('mistvein borderpost', 'ARB').
card_image_name('mistvein borderpost'/'ARB', 'mistvein borderpost').
card_uid('mistvein borderpost'/'ARB', 'ARB:Mistvein Borderpost:mistvein borderpost').
card_rarity('mistvein borderpost'/'ARB', 'Common').
card_artist('mistvein borderpost'/'ARB', 'Pete Venters').
card_number('mistvein borderpost'/'ARB', '27').
card_multiverse_id('mistvein borderpost'/'ARB', '161276').

card_in_set('monstrous carabid', 'ARB').
card_original_type('monstrous carabid'/'ARB', 'Creature — Insect').
card_original_text('monstrous carabid'/'ARB', 'Monstrous Carabid attacks each turn if able.\nCycling {B/R} ({B/R}, Discard this card: Draw a card.)').
card_first_print('monstrous carabid', 'ARB').
card_image_name('monstrous carabid'/'ARB', 'monstrous carabid').
card_uid('monstrous carabid'/'ARB', 'ARB:Monstrous Carabid:monstrous carabid').
card_rarity('monstrous carabid'/'ARB', 'Common').
card_artist('monstrous carabid'/'ARB', 'Pete Venters').
card_number('monstrous carabid'/'ARB', '43').
card_flavor_text('monstrous carabid'/'ARB', 'What happens when a plane of predators collides with four planes of prey?').
card_multiverse_id('monstrous carabid'/'ARB', '185051').

card_in_set('morbid bloom', 'ARB').
card_original_type('morbid bloom'/'ARB', 'Sorcery').
card_original_text('morbid bloom'/'ARB', 'Remove target creature card in a graveyard from the game, then put X 1/1 green Saproling creature tokens into play, where X is the removed card\'s toughness.').
card_first_print('morbid bloom', 'ARB').
card_image_name('morbid bloom'/'ARB', 'morbid bloom').
card_uid('morbid bloom'/'ARB', 'ARB:Morbid Bloom:morbid bloom').
card_rarity('morbid bloom'/'ARB', 'Uncommon').
card_artist('morbid bloom'/'ARB', 'Trevor Claxton').
card_number('morbid bloom'/'ARB', '94').
card_flavor_text('morbid bloom'/'ARB', '\"From the damned I make my garden.\"\n—Jorshu of Clan Nel Toth').
card_multiverse_id('morbid bloom'/'ARB', '179614').

card_in_set('mycoid shepherd', 'ARB').
card_original_type('mycoid shepherd'/'ARB', 'Creature — Fungus').
card_original_text('mycoid shepherd'/'ARB', 'Whenever Mycoid Shepherd or another creature you control with power 5 or greater is put into a graveyard from play, you may gain 5 life.').
card_image_name('mycoid shepherd'/'ARB', 'mycoid shepherd').
card_uid('mycoid shepherd'/'ARB', 'ARB:Mycoid Shepherd:mycoid shepherd').
card_rarity('mycoid shepherd'/'ARB', 'Rare').
card_artist('mycoid shepherd'/'ARB', 'Trevor Claxton').
card_number('mycoid shepherd'/'ARB', '73').
card_flavor_text('mycoid shepherd'/'ARB', 'As pure mana flooded across the shards, it brought a sense of unity even to Jund\'s vicious food chain.').
card_multiverse_id('mycoid shepherd'/'ARB', '179548').

card_in_set('naya hushblade', 'ARB').
card_original_type('naya hushblade'/'ARB', 'Creature — Elf Rogue').
card_original_text('naya hushblade'/'ARB', 'As long as you control another multicolored permanent, Naya Hushblade gets +1/+1 and has shroud. (It can\'t be the target of spells or abilities.)').
card_first_print('naya hushblade', 'ARB').
card_image_name('naya hushblade'/'ARB', 'naya hushblade').
card_uid('naya hushblade'/'ARB', 'ARB:Naya Hushblade:naya hushblade').
card_rarity('naya hushblade'/'ARB', 'Common').
card_artist('naya hushblade'/'ARB', 'Jason Chan').
card_number('naya hushblade'/'ARB', '141').
card_multiverse_id('naya hushblade'/'ARB', '188969').

card_in_set('naya sojourners', 'ARB').
card_original_type('naya sojourners'/'ARB', 'Creature — Elf Shaman').
card_original_text('naya sojourners'/'ARB', 'When you cycle Naya Sojourners or it\'s put into a graveyard from play, you may put a +1/+1 counter on target creature.\nCycling {2}{G} ({2}{G}, Discard this card: Draw a card.)').
card_image_name('naya sojourners'/'ARB', 'naya sojourners').
card_uid('naya sojourners'/'ARB', 'ARB:Naya Sojourners:naya sojourners').
card_rarity('naya sojourners'/'ARB', 'Common').
card_artist('naya sojourners'/'ARB', 'John Avon').
card_number('naya sojourners'/'ARB', '122').
card_flavor_text('naya sojourners'/'ARB', 'The elves had entered a land like an open grave—an act of fortitude or folly?').
card_multiverse_id('naya sojourners'/'ARB', '188970').

card_in_set('necromancer\'s covenant', 'ARB').
card_original_type('necromancer\'s covenant'/'ARB', 'Enchantment').
card_original_text('necromancer\'s covenant'/'ARB', 'When Necromancer\'s Covenant comes into play, remove all creature cards in target player\'s graveyard from the game, then put a 2/2 black Zombie creature token into play for each card removed this way.\nZombies you control have lifelink.').
card_first_print('necromancer\'s covenant', 'ARB').
card_image_name('necromancer\'s covenant'/'ARB', 'necromancer\'s covenant').
card_uid('necromancer\'s covenant'/'ARB', 'ARB:Necromancer\'s Covenant:necromancer\'s covenant').
card_rarity('necromancer\'s covenant'/'ARB', 'Rare').
card_artist('necromancer\'s covenant'/'ARB', 'Thomas M. Baxa').
card_number('necromancer\'s covenant'/'ARB', '82').
card_multiverse_id('necromancer\'s covenant'/'ARB', '183011').

card_in_set('nemesis of reason', 'ARB').
card_original_type('nemesis of reason'/'ARB', 'Creature — Leviathan Horror').
card_original_text('nemesis of reason'/'ARB', 'Whenever Nemesis of Reason attacks, defending player puts the top ten cards of his or her library into his or her graveyard.').
card_first_print('nemesis of reason', 'ARB').
card_image_name('nemesis of reason'/'ARB', 'nemesis of reason').
card_uid('nemesis of reason'/'ARB', 'ARB:Nemesis of Reason:nemesis of reason').
card_rarity('nemesis of reason'/'ARB', 'Rare').
card_artist('nemesis of reason'/'ARB', 'Mark Tedin').
card_number('nemesis of reason'/'ARB', '28').
card_flavor_text('nemesis of reason'/'ARB', 'Words describing it fail. Pages relating it shrivel. Tales recounting it end.').
card_multiverse_id('nemesis of reason'/'ARB', '188962').

card_in_set('nulltread gargantuan', 'ARB').
card_original_type('nulltread gargantuan'/'ARB', 'Creature — Beast').
card_original_text('nulltread gargantuan'/'ARB', 'When Nulltread Gargantuan comes into play, put a creature you control on top of its owner\'s library.').
card_first_print('nulltread gargantuan', 'ARB').
card_image_name('nulltread gargantuan'/'ARB', 'nulltread gargantuan').
card_uid('nulltread gargantuan'/'ARB', 'ARB:Nulltread Gargantuan:nulltread gargantuan').
card_rarity('nulltread gargantuan'/'ARB', 'Uncommon').
card_artist('nulltread gargantuan'/'ARB', 'Anthony Francisco').
card_number('nulltread gargantuan'/'ARB', '102').
card_flavor_text('nulltread gargantuan'/'ARB', 'It crushes all underfoot, yet leaves no trace of its passage—no trampled debris, no flattened souls, not even a hoofprint.').
card_multiverse_id('nulltread gargantuan'/'ARB', '179580').

card_in_set('offering to asha', 'ARB').
card_original_type('offering to asha'/'ARB', 'Instant').
card_original_text('offering to asha'/'ARB', 'Counter target spell unless its controller pays {4}. You gain 4 life.').
card_first_print('offering to asha', 'ARB').
card_image_name('offering to asha'/'ARB', 'offering to asha').
card_uid('offering to asha'/'ARB', 'ARB:Offering to Asha:offering to asha').
card_rarity('offering to asha'/'ARB', 'Common').
card_artist('offering to asha'/'ARB', 'Daarken').
card_number('offering to asha'/'ARB', '9').
card_flavor_text('offering to asha'/'ARB', '\"His body belongs to Bant now. His soul? I honestly don\'t know. I expect it belongs to the angels.\"\n—Rafiq of the Many').
card_multiverse_id('offering to asha'/'ARB', '183007').

card_in_set('pale recluse', 'ARB').
card_original_type('pale recluse'/'ARB', 'Creature — Spider').
card_original_text('pale recluse'/'ARB', 'Reach (This can block creatures with flying.)\nForestcycling {2}, plainscycling {2} ({2}, Discard this card: Search your library for a Forest or Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('pale recluse', 'ARB').
card_image_name('pale recluse'/'ARB', 'pale recluse').
card_uid('pale recluse'/'ARB', 'ARB:Pale Recluse:pale recluse').
card_rarity('pale recluse'/'ARB', 'Common').
card_artist('pale recluse'/'ARB', 'Cyril Van Der Haegen').
card_number('pale recluse'/'ARB', '74').
card_multiverse_id('pale recluse'/'ARB', '144249').

card_in_set('predatory advantage', 'ARB').
card_original_type('predatory advantage'/'ARB', 'Enchantment').
card_original_text('predatory advantage'/'ARB', 'At the end of each opponent\'s turn, if that player didn\'t play a creature spell this turn, put a 2/2 green Lizard creature token into play.').
card_first_print('predatory advantage', 'ARB').
card_image_name('predatory advantage'/'ARB', 'predatory advantage').
card_uid('predatory advantage'/'ARB', 'ARB:Predatory Advantage:predatory advantage').
card_rarity('predatory advantage'/'ARB', 'Rare').
card_artist('predatory advantage'/'ARB', 'Raymond Swanland').
card_number('predatory advantage'/'ARB', '58').
card_flavor_text('predatory advantage'/'ARB', 'Those who forget their past are doomed to be eaten by it.').
card_multiverse_id('predatory advantage'/'ARB', '180602').

card_in_set('putrid leech', 'ARB').
card_original_type('putrid leech'/'ARB', 'Creature — Zombie Leech').
card_original_text('putrid leech'/'ARB', 'Pay 2 life: Putrid Leech gets +2/+2 until end of turn. Play this ability only once each turn.').
card_first_print('putrid leech', 'ARB').
card_image_name('putrid leech'/'ARB', 'putrid leech').
card_uid('putrid leech'/'ARB', 'ARB:Putrid Leech:putrid leech').
card_rarity('putrid leech'/'ARB', 'Common').
card_artist('putrid leech'/'ARB', 'Dave Allsop').
card_number('putrid leech'/'ARB', '95').
card_flavor_text('putrid leech'/'ARB', 'It reacted to Naya\'s overabundant food supply by growing additional mouths.').
card_multiverse_id('putrid leech'/'ARB', '179612').

card_in_set('qasali pridemage', 'ARB').
card_original_type('qasali pridemage'/'ARB', 'Creature — Cat Wizard').
card_original_text('qasali pridemage'/'ARB', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{1}, Sacrifice Qasali Pridemage: Destroy target artifact or enchantment.').
card_image_name('qasali pridemage'/'ARB', 'qasali pridemage').
card_uid('qasali pridemage'/'ARB', 'ARB:Qasali Pridemage:qasali pridemage').
card_rarity('qasali pridemage'/'ARB', 'Common').
card_artist('qasali pridemage'/'ARB', 'Chris Rahn').
card_number('qasali pridemage'/'ARB', '75').
card_flavor_text('qasali pridemage'/'ARB', 'An elder in one pride, of the Sigiled caste in another.').
card_multiverse_id('qasali pridemage'/'ARB', '179556').

card_in_set('reborn hope', 'ARB').
card_original_type('reborn hope'/'ARB', 'Sorcery').
card_original_text('reborn hope'/'ARB', 'Return target multicolored card from your graveyard to your hand.').
card_first_print('reborn hope', 'ARB').
card_image_name('reborn hope'/'ARB', 'reborn hope').
card_uid('reborn hope'/'ARB', 'ARB:Reborn Hope:reborn hope').
card_rarity('reborn hope'/'ARB', 'Uncommon').
card_artist('reborn hope'/'ARB', 'Warren Mahy').
card_number('reborn hope'/'ARB', '76').
card_flavor_text('reborn hope'/'ARB', '\"In darkness is kindled the fiercest hope.\"\n—Hadran, sunseeder of Naya').
card_multiverse_id('reborn hope'/'ARB', '175256').

card_in_set('retaliator griffin', 'ARB').
card_original_type('retaliator griffin'/'ARB', 'Creature — Griffin').
card_original_text('retaliator griffin'/'ARB', 'Flying\nWhenever a source an opponent controls deals damage to you, you may put that many +1/+1 counters on Retaliator Griffin.').
card_image_name('retaliator griffin'/'ARB', 'retaliator griffin').
card_uid('retaliator griffin'/'ARB', 'ARB:Retaliator Griffin:retaliator griffin').
card_rarity('retaliator griffin'/'ARB', 'Rare').
card_artist('retaliator griffin'/'ARB', 'Jesper Ejsing').
card_number('retaliator griffin'/'ARB', '123').
card_flavor_text('retaliator griffin'/'ARB', 'For every eye, an eye. For every tooth, a tooth. Griffins are nothing if not fair.').
card_multiverse_id('retaliator griffin'/'ARB', '147530').

card_in_set('rhox brute', 'ARB').
card_original_type('rhox brute'/'ARB', 'Creature — Rhino Warrior').
card_original_text('rhox brute'/'ARB', '').
card_first_print('rhox brute', 'ARB').
card_image_name('rhox brute'/'ARB', 'rhox brute').
card_uid('rhox brute'/'ARB', 'ARB:Rhox Brute:rhox brute').
card_rarity('rhox brute'/'ARB', 'Common').
card_artist('rhox brute'/'ARB', 'Raymond Swanland').
card_number('rhox brute'/'ARB', '59').
card_flavor_text('rhox brute'/'ARB', '\"In this new world, I have seen the once-devout rhoxes take up arms with the savages of Jund. I would be offended, but I see the wisdom in their choice.\"\n—Gernan, Dawnray archer').
card_multiverse_id('rhox brute'/'ARB', '189650').

card_in_set('sages of the anima', 'ARB').
card_original_type('sages of the anima'/'ARB', 'Creature — Elf Wizard').
card_original_text('sages of the anima'/'ARB', 'If you would draw a card, instead reveal the top three cards of your library. Put all creature cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_first_print('sages of the anima', 'ARB').
card_image_name('sages of the anima'/'ARB', 'sages of the anima').
card_uid('sages of the anima'/'ARB', 'ARB:Sages of the Anima:sages of the anima').
card_rarity('sages of the anima'/'ARB', 'Rare').
card_artist('sages of the anima'/'ARB', 'Kev Walker').
card_number('sages of the anima'/'ARB', '103').
card_flavor_text('sages of the anima'/'ARB', 'Godtrackers found new pantheons of beast-gods beyond Naya\'s borders.').
card_multiverse_id('sages of the anima'/'ARB', '180601').

card_in_set('sanctum plowbeast', 'ARB').
card_original_type('sanctum plowbeast'/'ARB', 'Artifact Creature — Beast').
card_original_text('sanctum plowbeast'/'ARB', 'Defender\nPlainscycling {2}, islandcycling {2} ({2}, Discard this card: Search your library for a Plains or Island card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('sanctum plowbeast', 'ARB').
card_image_name('sanctum plowbeast'/'ARB', 'sanctum plowbeast').
card_uid('sanctum plowbeast'/'ARB', 'ARB:Sanctum Plowbeast:sanctum plowbeast').
card_rarity('sanctum plowbeast'/'ARB', 'Common').
card_artist('sanctum plowbeast'/'ARB', 'Anthony Francisco').
card_number('sanctum plowbeast'/'ARB', '10').
card_multiverse_id('sanctum plowbeast'/'ARB', '144282').

card_in_set('sangrite backlash', 'ARB').
card_original_type('sangrite backlash'/'ARB', 'Enchantment — Aura').
card_original_text('sangrite backlash'/'ARB', 'Enchant creature\nEnchanted creature gets +3/-3.').
card_first_print('sangrite backlash', 'ARB').
card_image_name('sangrite backlash'/'ARB', 'sangrite backlash').
card_uid('sangrite backlash'/'ARB', 'ARB:Sangrite Backlash:sangrite backlash').
card_rarity('sangrite backlash'/'ARB', 'Common').
card_artist('sangrite backlash'/'ARB', 'Cyril Van Der Haegen').
card_number('sangrite backlash'/'ARB', '139').
card_flavor_text('sangrite backlash'/'ARB', '\"Sangrite is the dragon\'s beating heart. Little wonder, then, that so many fools\' chests burst trying to contain it.\"\n—Sarkhan Vol').
card_multiverse_id('sangrite backlash'/'ARB', '179553').

card_in_set('sanity gnawers', 'ARB').
card_original_type('sanity gnawers'/'ARB', 'Creature — Rat').
card_original_text('sanity gnawers'/'ARB', 'When Sanity Gnawers comes into play, target player discards a card at random.').
card_first_print('sanity gnawers', 'ARB').
card_image_name('sanity gnawers'/'ARB', 'sanity gnawers').
card_uid('sanity gnawers'/'ARB', 'ARB:Sanity Gnawers:sanity gnawers').
card_rarity('sanity gnawers'/'ARB', 'Uncommon').
card_artist('sanity gnawers'/'ARB', 'Pete Venters').
card_number('sanity gnawers'/'ARB', '44').
card_flavor_text('sanity gnawers'/'ARB', 'The last thought to go through their victim\'s mind is the one they tear out through the scalp.').
card_multiverse_id('sanity gnawers'/'ARB', '179623').

card_in_set('sen triplets', 'ARB').
card_original_type('sen triplets'/'ARB', 'Legendary Artifact Creature — Human Wizard').
card_original_text('sen triplets'/'ARB', 'At the beginning of your upkeep, choose target opponent. This turn, that player can\'t play spells or activated abilities and plays with his or her hand revealed. You can play cards from that player\'s hand this turn.').
card_first_print('sen triplets', 'ARB').
card_image_name('sen triplets'/'ARB', 'sen triplets').
card_uid('sen triplets'/'ARB', 'ARB:Sen Triplets:sen triplets').
card_rarity('sen triplets'/'ARB', 'Mythic Rare').
card_artist('sen triplets'/'ARB', 'Greg Staples').
card_number('sen triplets'/'ARB', '109').
card_flavor_text('sen triplets'/'ARB', 'They are the masters of your mind.').
card_multiverse_id('sen triplets'/'ARB', '180607').

card_in_set('sewn-eye drake', 'ARB').
card_original_type('sewn-eye drake'/'ARB', 'Creature — Zombie Drake').
card_original_text('sewn-eye drake'/'ARB', 'Flying, haste').
card_first_print('sewn-eye drake', 'ARB').
card_image_name('sewn-eye drake'/'ARB', 'sewn-eye drake').
card_uid('sewn-eye drake'/'ARB', 'ARB:Sewn-Eye Drake:sewn-eye drake').
card_rarity('sewn-eye drake'/'ARB', 'Common').
card_artist('sewn-eye drake'/'ARB', 'Anthony S. Waters').
card_number('sewn-eye drake'/'ARB', '135').
card_flavor_text('sewn-eye drake'/'ARB', '\"Too wary to charge, too prudent to serve. Not anymore.\"\n—Sedris, the Traitor King').
card_multiverse_id('sewn-eye drake'/'ARB', '179594').

card_in_set('shield of the righteous', 'ARB').
card_original_type('shield of the righteous'/'ARB', 'Artifact — Equipment').
card_original_text('shield of the righteous'/'ARB', 'Equipped creature gets +0/+2 and has vigilance.\nWhenever equipped creature blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.\nEquip {2}').
card_first_print('shield of the righteous', 'ARB').
card_image_name('shield of the righteous'/'ARB', 'shield of the righteous').
card_uid('shield of the righteous'/'ARB', 'ARB:Shield of the Righteous:shield of the righteous').
card_rarity('shield of the righteous'/'ARB', 'Uncommon').
card_artist('shield of the righteous'/'ARB', 'David Palumbo').
card_number('shield of the righteous'/'ARB', '11').
card_multiverse_id('shield of the righteous'/'ARB', '185063').

card_in_set('sigil captain', 'ARB').
card_original_type('sigil captain'/'ARB', 'Creature — Rhino Soldier').
card_original_text('sigil captain'/'ARB', 'Whenever a creature comes into play under your control, if that creature is 1/1, put two +1/+1 counters on it.').
card_first_print('sigil captain', 'ARB').
card_image_name('sigil captain'/'ARB', 'sigil captain').
card_uid('sigil captain'/'ARB', 'ARB:Sigil Captain:sigil captain').
card_rarity('sigil captain'/'ARB', 'Uncommon').
card_artist('sigil captain'/'ARB', 'Howard Lyon').
card_number('sigil captain'/'ARB', '77').
card_flavor_text('sigil captain'/'ARB', '\"One sigil awarded for valor, the other awarded for surviving Hellkite\'s Pass.\"').
card_multiverse_id('sigil captain'/'ARB', '179560').

card_in_set('sigil of the nayan gods', 'ARB').
card_original_type('sigil of the nayan gods'/'ARB', 'Enchantment — Aura').
card_original_text('sigil of the nayan gods'/'ARB', 'Enchant creature\nEnchanted creature gets +1/+1 for each creature you control.\nCycling {G/W} ({G/W}, Discard this card: Draw a card.)').
card_first_print('sigil of the nayan gods', 'ARB').
card_image_name('sigil of the nayan gods'/'ARB', 'sigil of the nayan gods').
card_uid('sigil of the nayan gods'/'ARB', 'ARB:Sigil of the Nayan Gods:sigil of the nayan gods').
card_rarity('sigil of the nayan gods'/'ARB', 'Common').
card_artist('sigil of the nayan gods'/'ARB', 'Warren Mahy').
card_number('sigil of the nayan gods'/'ARB', '78').
card_flavor_text('sigil of the nayan gods'/'ARB', 'As the elves learned the hymns of angels, the knights learned the roars of titans.').
card_multiverse_id('sigil of the nayan gods'/'ARB', '179630').

card_in_set('sigiled behemoth', 'ARB').
card_original_type('sigiled behemoth'/'ARB', 'Creature — Beast').
card_original_text('sigiled behemoth'/'ARB', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('sigiled behemoth', 'ARB').
card_image_name('sigiled behemoth'/'ARB', 'sigiled behemoth').
card_uid('sigiled behemoth'/'ARB', 'ARB:Sigiled Behemoth:sigiled behemoth').
card_rarity('sigiled behemoth'/'ARB', 'Common').
card_artist('sigiled behemoth'/'ARB', 'Donato Giancola').
card_number('sigiled behemoth'/'ARB', '79').
card_flavor_text('sigiled behemoth'/'ARB', '\"Do not mistake a behemoth\'s assistance for subservience.\"\n—Mayael the Anima').
card_multiverse_id('sigiled behemoth'/'ARB', '159688').

card_in_set('singe-mind ogre', 'ARB').
card_original_type('singe-mind ogre'/'ARB', 'Creature — Ogre Mutant').
card_original_text('singe-mind ogre'/'ARB', 'When Singe-Mind Ogre comes into play, target player reveals a card at random from his or her hand, then loses life equal to that card\'s converted mana cost.').
card_first_print('singe-mind ogre', 'ARB').
card_image_name('singe-mind ogre'/'ARB', 'singe-mind ogre').
card_uid('singe-mind ogre'/'ARB', 'ARB:Singe-Mind Ogre:singe-mind ogre').
card_rarity('singe-mind ogre'/'ARB', 'Common').
card_artist('singe-mind ogre'/'ARB', 'Daarken').
card_number('singe-mind ogre'/'ARB', '45').
card_flavor_text('singe-mind ogre'/'ARB', 'Even the balmgivers of Bant couldn\'t heal the incurables, but sharing their torment helped.').
card_multiverse_id('singe-mind ogre'/'ARB', '179574').

card_in_set('skyclaw thrash', 'ARB').
card_original_type('skyclaw thrash'/'ARB', 'Artifact Creature — Viashino Warrior').
card_original_text('skyclaw thrash'/'ARB', 'Whenever Skyclaw Thrash attacks, flip a coin. If you win the flip, Skyclaw Thrash gets +1/+1 and gains flying until end of turn.').
card_first_print('skyclaw thrash', 'ARB').
card_image_name('skyclaw thrash'/'ARB', 'skyclaw thrash').
card_uid('skyclaw thrash'/'ARB', 'ARB:Skyclaw Thrash:skyclaw thrash').
card_rarity('skyclaw thrash'/'ARB', 'Uncommon').
card_artist('skyclaw thrash'/'ARB', 'Jaime Jones').
card_number('skyclaw thrash'/'ARB', '89').
card_flavor_text('skyclaw thrash'/'ARB', 'Their newfound wings shined elegantly and functioned partially.').
card_multiverse_id('skyclaw thrash'/'ARB', '179569').

card_in_set('slave of bolas', 'ARB').
card_original_type('slave of bolas'/'ARB', 'Sorcery').
card_original_text('slave of bolas'/'ARB', 'Gain control of target creature. Untap that creature. It gains haste until end of turn. Sacrifice it at end of turn.').
card_image_name('slave of bolas'/'ARB', 'slave of bolas').
card_uid('slave of bolas'/'ARB', 'ARB:Slave of Bolas:slave of bolas').
card_rarity('slave of bolas'/'ARB', 'Uncommon').
card_artist('slave of bolas'/'ARB', 'Steve Argyle').
card_number('slave of bolas'/'ARB', '136').
card_flavor_text('slave of bolas'/'ARB', 'Nicol Bolas doesn\'t distinguish between servants and victims.').
card_multiverse_id('slave of bolas'/'ARB', '188971').

card_in_set('soul manipulation', 'ARB').
card_original_type('soul manipulation'/'ARB', 'Instant').
card_original_text('soul manipulation'/'ARB', 'Choose one or both — Counter target creature spell; and/or return target creature card in your graveyard to your hand.').
card_first_print('soul manipulation', 'ARB').
card_image_name('soul manipulation'/'ARB', 'soul manipulation').
card_uid('soul manipulation'/'ARB', 'ARB:Soul Manipulation:soul manipulation').
card_rarity('soul manipulation'/'ARB', 'Common').
card_artist('soul manipulation'/'ARB', 'Carl Critchlow').
card_number('soul manipulation'/'ARB', '29').
card_flavor_text('soul manipulation'/'ARB', '\"Birth and death are both reversible.\"\n—Nicol Bolas').
card_multiverse_id('soul manipulation'/'ARB', '183008').

card_in_set('soulquake', 'ARB').
card_original_type('soulquake'/'ARB', 'Sorcery').
card_original_text('soulquake'/'ARB', 'Return all creatures in play and all creature cards in graveyards to their owners\' hands.').
card_first_print('soulquake', 'ARB').
card_image_name('soulquake'/'ARB', 'soulquake').
card_uid('soulquake'/'ARB', 'ARB:Soulquake:soulquake').
card_rarity('soulquake'/'ARB', 'Rare').
card_artist('soulquake'/'ARB', 'Warren Mahy').
card_number('soulquake'/'ARB', '30').
card_flavor_text('soulquake'/'ARB', 'The Maelstrom sent a wave of raw mana through the battlefield, carrying off the fighting and fallen alike.').
card_multiverse_id('soulquake'/'ARB', '179409').

card_in_set('sovereigns of lost alara', 'ARB').
card_original_type('sovereigns of lost alara'/'ARB', 'Creature — Spirit').
card_original_text('sovereigns of lost alara'/'ARB', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, you may search your library for an Aura card that could enchant that creature, put it into play attached to that creature, then shuffle your library.').
card_first_print('sovereigns of lost alara', 'ARB').
card_image_name('sovereigns of lost alara'/'ARB', 'sovereigns of lost alara').
card_uid('sovereigns of lost alara'/'ARB', 'ARB:Sovereigns of Lost Alara:sovereigns of lost alara').
card_rarity('sovereigns of lost alara'/'ARB', 'Rare').
card_artist('sovereigns of lost alara'/'ARB', 'Donato Giancola').
card_number('sovereigns of lost alara'/'ARB', '12').
card_multiverse_id('sovereigns of lost alara'/'ARB', '180600').

card_in_set('spellbound dragon', 'ARB').
card_original_type('spellbound dragon'/'ARB', 'Creature — Dragon').
card_original_text('spellbound dragon'/'ARB', 'Flying\nWhenever Spellbound Dragon attacks, draw a card, then discard a card. Spellbound Dragon gets +X/+0 until end of turn, where X is the discarded card\'s converted mana cost.').
card_first_print('spellbound dragon', 'ARB').
card_image_name('spellbound dragon'/'ARB', 'spellbound dragon').
card_uid('spellbound dragon'/'ARB', 'ARB:Spellbound Dragon:spellbound dragon').
card_rarity('spellbound dragon'/'ARB', 'Rare').
card_artist('spellbound dragon'/'ARB', 'Jesper Ejsing').
card_number('spellbound dragon'/'ARB', '90').
card_flavor_text('spellbound dragon'/'ARB', 'A king in Jund, a serf in Esper.').
card_multiverse_id('spellbound dragon'/'ARB', '180611').

card_in_set('spellbreaker behemoth', 'ARB').
card_original_type('spellbreaker behemoth'/'ARB', 'Creature — Beast').
card_original_text('spellbreaker behemoth'/'ARB', 'Spellbreaker Behemoth can\'t be countered.\nCreature spells you control with power 5 or greater can\'t be countered.').
card_first_print('spellbreaker behemoth', 'ARB').
card_image_name('spellbreaker behemoth'/'ARB', 'spellbreaker behemoth').
card_uid('spellbreaker behemoth'/'ARB', 'ARB:Spellbreaker Behemoth:spellbreaker behemoth').
card_rarity('spellbreaker behemoth'/'ARB', 'Rare').
card_artist('spellbreaker behemoth'/'ARB', 'Jason Chan').
card_number('spellbreaker behemoth'/'ARB', '60').
card_flavor_text('spellbreaker behemoth'/'ARB', '\"Some think its immunity to countermagic evolved because of the constant mage attacks on Naya. I think it just ate a bunch of wizards.\"\n—Broka, drumhunter').
card_multiverse_id('spellbreaker behemoth'/'ARB', '179551').

card_in_set('sphinx of the steel wind', 'ARB').
card_original_type('sphinx of the steel wind'/'ARB', 'Artifact Creature — Sphinx').
card_original_text('sphinx of the steel wind'/'ARB', 'Flying, first strike, vigilance, lifelink, protection from red and from green').
card_first_print('sphinx of the steel wind', 'ARB').
card_image_name('sphinx of the steel wind'/'ARB', 'sphinx of the steel wind').
card_uid('sphinx of the steel wind'/'ARB', 'ARB:Sphinx of the Steel Wind:sphinx of the steel wind').
card_rarity('sphinx of the steel wind'/'ARB', 'Mythic Rare').
card_artist('sphinx of the steel wind'/'ARB', 'Kev Walker').
card_number('sphinx of the steel wind'/'ARB', '110').
card_flavor_text('sphinx of the steel wind'/'ARB', 'No one has properly answered her favorite riddle: \"Why should I spare your life?\"').
card_multiverse_id('sphinx of the steel wind'/'ARB', '189641').

card_in_set('stormcaller\'s boon', 'ARB').
card_original_type('stormcaller\'s boon'/'ARB', 'Enchantment').
card_original_text('stormcaller\'s boon'/'ARB', 'Sacrifice Stormcaller\'s Boon: Creatures you control gain flying until end of turn.\nCascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)').
card_first_print('stormcaller\'s boon', 'ARB').
card_image_name('stormcaller\'s boon'/'ARB', 'stormcaller\'s boon').
card_uid('stormcaller\'s boon'/'ARB', 'ARB:Stormcaller\'s Boon:stormcaller\'s boon').
card_rarity('stormcaller\'s boon'/'ARB', 'Common').
card_artist('stormcaller\'s boon'/'ARB', 'Brandon Kitkouski').
card_number('stormcaller\'s boon'/'ARB', '13').
card_multiverse_id('stormcaller\'s boon'/'ARB', '179534').

card_in_set('stun sniper', 'ARB').
card_original_type('stun sniper'/'ARB', 'Creature — Human Archer').
card_original_text('stun sniper'/'ARB', '{1}, {T}: Stun Sniper deals 1 damage to target creature. Tap that creature.').
card_first_print('stun sniper', 'ARB').
card_image_name('stun sniper'/'ARB', 'stun sniper').
card_uid('stun sniper'/'ARB', 'ARB:Stun Sniper:stun sniper').
card_rarity('stun sniper'/'ARB', 'Uncommon').
card_artist('stun sniper'/'ARB', 'Steve Prescott').
card_number('stun sniper'/'ARB', '100').
card_flavor_text('stun sniper'/'ARB', 'The tips of her bolts are blunt, but her aim is deadly sharp.').
card_multiverse_id('stun sniper'/'ARB', '179589').

card_in_set('tainted sigil', 'ARB').
card_original_type('tainted sigil'/'ARB', 'Artifact').
card_original_text('tainted sigil'/'ARB', '{T}, Sacrifice Tainted Sigil: You gain life equal to the total life lost by all players this turn. (Damage causes loss of life.)').
card_first_print('tainted sigil', 'ARB').
card_image_name('tainted sigil'/'ARB', 'tainted sigil').
card_uid('tainted sigil'/'ARB', 'ARB:Tainted Sigil:tainted sigil').
card_rarity('tainted sigil'/'ARB', 'Uncommon').
card_artist('tainted sigil'/'ARB', 'Chippy').
card_number('tainted sigil'/'ARB', '83').
card_flavor_text('tainted sigil'/'ARB', 'Sigils corrupted by outsiders do not lose their power, only their purity.').
card_multiverse_id('tainted sigil'/'ARB', '180618').

card_in_set('talon trooper', 'ARB').
card_original_type('talon trooper'/'ARB', 'Creature — Bird Scout').
card_original_text('talon trooper'/'ARB', 'Flying').
card_first_print('talon trooper', 'ARB').
card_image_name('talon trooper'/'ARB', 'talon trooper').
card_uid('talon trooper'/'ARB', 'ARB:Talon Trooper:talon trooper').
card_rarity('talon trooper'/'ARB', 'Common').
card_artist('talon trooper'/'ARB', 'Matt Stewart').
card_number('talon trooper'/'ARB', '14').
card_flavor_text('talon trooper'/'ARB', '\"The volcanic winds make a maze of the skies of Jund. If we mean to invade, we must first learn their invisible paths.\"').
card_multiverse_id('talon trooper'/'ARB', '179537').

card_in_set('terminate', 'ARB').
card_original_type('terminate'/'ARB', 'Instant').
card_original_text('terminate'/'ARB', 'Destroy target creature. It can\'t be regenerated.').
card_image_name('terminate'/'ARB', 'terminate').
card_uid('terminate'/'ARB', 'ARB:Terminate:terminate').
card_rarity('terminate'/'ARB', 'Common').
card_artist('terminate'/'ARB', 'Wayne Reynolds').
card_number('terminate'/'ARB', '46').
card_flavor_text('terminate'/'ARB', '\"I\'ve seen death before. My mother succumbing to illness, my comrades bleeding on the battlefield . . . But I\'d never seen anything as dreadful as that.\"\n—Taani, berserker of Etlan').
card_multiverse_id('terminate'/'ARB', '176449').

card_in_set('thopter foundry', 'ARB').
card_original_type('thopter foundry'/'ARB', 'Artifact').
card_original_text('thopter foundry'/'ARB', '{1}, Sacrifice a nontoken artifact: Put a 1/1 blue Thopter artifact creature token with flying into play. You gain 1 life.').
card_first_print('thopter foundry', 'ARB').
card_image_name('thopter foundry'/'ARB', 'thopter foundry').
card_uid('thopter foundry'/'ARB', 'ARB:Thopter Foundry:thopter foundry').
card_rarity('thopter foundry'/'ARB', 'Uncommon').
card_artist('thopter foundry'/'ARB', 'Ralph Horsley').
card_number('thopter foundry'/'ARB', '133').
card_flavor_text('thopter foundry'/'ARB', '\"Etherium is limited. Innovation is not.\"\n—Tezzeret').
card_multiverse_id('thopter foundry'/'ARB', '183017').

card_in_set('thought hemorrhage', 'ARB').
card_original_type('thought hemorrhage'/'ARB', 'Sorcery').
card_original_text('thought hemorrhage'/'ARB', 'Name a nonland card. Target player reveals his or her hand. Thought Hemorrhage deals 3 damage to that player for each card with that name revealed this way. Search that player\'s graveyard, hand, and library for all cards with that name and remove them from the game. Then that player shuffles his or her library.').
card_first_print('thought hemorrhage', 'ARB').
card_image_name('thought hemorrhage'/'ARB', 'thought hemorrhage').
card_uid('thought hemorrhage'/'ARB', 'ARB:Thought Hemorrhage:thought hemorrhage').
card_rarity('thought hemorrhage'/'ARB', 'Rare').
card_artist('thought hemorrhage'/'ARB', 'Chippy').
card_number('thought hemorrhage'/'ARB', '47').
card_multiverse_id('thought hemorrhage'/'ARB', '180596').

card_in_set('thraximundar', 'ARB').
card_original_type('thraximundar'/'ARB', 'Legendary Creature — Zombie Assassin').
card_original_text('thraximundar'/'ARB', 'Haste\nWhenever Thraximundar attacks, defending player sacrifices a creature.\nWhenever a player sacrifices a creature, you may put a +1/+1 counter on Thraximundar.').
card_first_print('thraximundar', 'ARB').
card_image_name('thraximundar'/'ARB', 'thraximundar').
card_uid('thraximundar'/'ARB', 'ARB:Thraximundar:thraximundar').
card_rarity('thraximundar'/'ARB', 'Mythic Rare').
card_artist('thraximundar'/'ARB', 'Raymond Swanland').
card_number('thraximundar'/'ARB', '113').
card_flavor_text('thraximundar'/'ARB', 'His name means \"he who paints the earth red.\"').
card_multiverse_id('thraximundar'/'ARB', '180595').

card_in_set('time sieve', 'ARB').
card_original_type('time sieve'/'ARB', 'Artifact').
card_original_text('time sieve'/'ARB', '{T}, Sacrifice five artifacts: Take an extra turn after this one.').
card_first_print('time sieve', 'ARB').
card_image_name('time sieve'/'ARB', 'time sieve').
card_uid('time sieve'/'ARB', 'ARB:Time Sieve:time sieve').
card_rarity('time sieve'/'ARB', 'Rare').
card_artist('time sieve'/'ARB', 'Franz Vohwinkel').
card_number('time sieve'/'ARB', '31').
card_flavor_text('time sieve'/'ARB', '\"I pray that I am never considered useless or old.\"\n—Sharuum the Hegemon').
card_multiverse_id('time sieve'/'ARB', '189649').

card_in_set('trace of abundance', 'ARB').
card_original_type('trace of abundance'/'ARB', 'Enchantment — Aura').
card_original_text('trace of abundance'/'ARB', 'Enchant land\nEnchanted land has shroud. (It can\'t be the target of spells or abilities.)\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_first_print('trace of abundance', 'ARB').
card_image_name('trace of abundance'/'ARB', 'trace of abundance').
card_uid('trace of abundance'/'ARB', 'ARB:Trace of Abundance:trace of abundance').
card_rarity('trace of abundance'/'ARB', 'Common').
card_artist('trace of abundance'/'ARB', 'Dave Kendall').
card_number('trace of abundance'/'ARB', '142').
card_multiverse_id('trace of abundance'/'ARB', '161292').

card_in_set('unbender tine', 'ARB').
card_original_type('unbender tine'/'ARB', 'Artifact').
card_original_text('unbender tine'/'ARB', '{T}: Untap another target permanent.').
card_first_print('unbender tine', 'ARB').
card_image_name('unbender tine'/'ARB', 'unbender tine').
card_uid('unbender tine'/'ARB', 'ARB:Unbender Tine:unbender tine').
card_rarity('unbender tine'/'ARB', 'Uncommon').
card_artist('unbender tine'/'ARB', 'Greg Hildebrandt').
card_number('unbender tine'/'ARB', '15').
card_flavor_text('unbender tine'/'ARB', 'Creations made in madness may contain great power. The path to that power is as labyrinthine as the creator\'s mind.').
card_multiverse_id('unbender tine'/'ARB', '138217').

card_in_set('unscythe, killer of kings', 'ARB').
card_original_type('unscythe, killer of kings'/'ARB', 'Legendary Artifact — Equipment').
card_original_text('unscythe, killer of kings'/'ARB', 'Equipped creature gets +3/+3 and has first strike.\nWhenever a creature dealt damage by equipped creature this turn is put into a graveyard, you may remove that card from the game. If you do, put a 2/2 black Zombie creature token into play.\nEquip {2}').
card_first_print('unscythe, killer of kings', 'ARB').
card_image_name('unscythe, killer of kings'/'ARB', 'unscythe, killer of kings').
card_uid('unscythe, killer of kings'/'ARB', 'ARB:Unscythe, Killer of Kings:unscythe, killer of kings').
card_rarity('unscythe, killer of kings'/'ARB', 'Rare').
card_artist('unscythe, killer of kings'/'ARB', 'Karl Kopinski').
card_number('unscythe, killer of kings'/'ARB', '114').
card_multiverse_id('unscythe, killer of kings'/'ARB', '189651').

card_in_set('uril, the miststalker', 'ARB').
card_original_type('uril, the miststalker'/'ARB', 'Legendary Creature — Beast').
card_original_text('uril, the miststalker'/'ARB', 'Uril, the Miststalker can\'t be the target of spells or abilities your opponents control. \nUril gets +2/+2 for each Aura attached to it.').
card_first_print('uril, the miststalker', 'ARB').
card_image_name('uril, the miststalker'/'ARB', 'uril, the miststalker').
card_uid('uril, the miststalker'/'ARB', 'ARB:Uril, the Miststalker:uril, the miststalker').
card_rarity('uril, the miststalker'/'ARB', 'Mythic Rare').
card_artist('uril, the miststalker'/'ARB', 'Jaime Jones').
card_number('uril, the miststalker'/'ARB', '124').
card_flavor_text('uril, the miststalker'/'ARB', 'It strides through the whitecover, mists in which even gargantuans can hide.').
card_multiverse_id('uril, the miststalker'/'ARB', '189645').

card_in_set('valley rannet', 'ARB').
card_original_type('valley rannet'/'ARB', 'Creature — Beast').
card_original_text('valley rannet'/'ARB', 'Mountaincycling {2}, forestcycling {2} ({2}, Discard this card: Search your library for a Mountain or Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_first_print('valley rannet', 'ARB').
card_image_name('valley rannet'/'ARB', 'valley rannet').
card_uid('valley rannet'/'ARB', 'ARB:Valley Rannet:valley rannet').
card_rarity('valley rannet'/'ARB', 'Common').
card_artist('valley rannet'/'ARB', 'Dave Allsop').
card_number('valley rannet'/'ARB', '61').
card_multiverse_id('valley rannet'/'ARB', '144260').

card_in_set('vectis dominator', 'ARB').
card_original_type('vectis dominator'/'ARB', 'Artifact Creature — Human Wizard').
card_original_text('vectis dominator'/'ARB', '{T}: Tap target creature unless its controller pays 2 life.').
card_first_print('vectis dominator', 'ARB').
card_image_name('vectis dominator'/'ARB', 'vectis dominator').
card_uid('vectis dominator'/'ARB', 'ARB:Vectis Dominator:vectis dominator').
card_rarity('vectis dominator'/'ARB', 'Common').
card_artist('vectis dominator'/'ARB', 'Zoltan Boros & Gabor Szikszai').
card_number('vectis dominator'/'ARB', '84').
card_flavor_text('vectis dominator'/'ARB', '\"I appreciate the artistry of the telemins, but the creatures of the other worlds provide a much more dramatic performance.\"\n—Ennor, mentalist').
card_multiverse_id('vectis dominator'/'ARB', '179592').

card_in_set('vedalken ghoul', 'ARB').
card_original_type('vedalken ghoul'/'ARB', 'Creature — Vedalken Zombie').
card_original_text('vedalken ghoul'/'ARB', 'Whenever Vedalken Ghoul becomes blocked, defending player loses 4 life.').
card_first_print('vedalken ghoul', 'ARB').
card_image_name('vedalken ghoul'/'ARB', 'vedalken ghoul').
card_uid('vedalken ghoul'/'ARB', 'ARB:Vedalken Ghoul:vedalken ghoul').
card_rarity('vedalken ghoul'/'ARB', 'Common').
card_artist('vedalken ghoul'/'ARB', 'Dave Kendall').
card_number('vedalken ghoul'/'ARB', '32').
card_flavor_text('vedalken ghoul'/'ARB', 'For four nights following Ennor\'s death and the reclaiming of his etherium, its recipients were found slain, the etherium ripped from their bodies.').
card_multiverse_id('vedalken ghoul'/'ARB', '183010').

card_in_set('vedalken heretic', 'ARB').
card_original_type('vedalken heretic'/'ARB', 'Creature — Vedalken Rogue').
card_original_text('vedalken heretic'/'ARB', 'Whenever Vedalken Heretic deals damage to an opponent, you may draw a card.').
card_first_print('vedalken heretic', 'ARB').
card_image_name('vedalken heretic'/'ARB', 'vedalken heretic').
card_uid('vedalken heretic'/'ARB', 'ARB:Vedalken Heretic:vedalken heretic').
card_rarity('vedalken heretic'/'ARB', 'Rare').
card_artist('vedalken heretic'/'ARB', 'Greg Staples').
card_number('vedalken heretic'/'ARB', '104').
card_flavor_text('vedalken heretic'/'ARB', '\"Etherium clouded my eyes, clogged my ears, desensitized my skin. Now that I can feel, I can begin to learn.\"').
card_multiverse_id('vedalken heretic'/'ARB', '180619').

card_in_set('veinfire borderpost', 'ARB').
card_original_type('veinfire borderpost'/'ARB', 'Artifact').
card_original_text('veinfire borderpost'/'ARB', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Veinfire Borderpost\'s mana cost.\nVeinfire Borderpost comes into play tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('veinfire borderpost', 'ARB').
card_image_name('veinfire borderpost'/'ARB', 'veinfire borderpost').
card_uid('veinfire borderpost'/'ARB', 'ARB:Veinfire Borderpost:veinfire borderpost').
card_rarity('veinfire borderpost'/'ARB', 'Common').
card_artist('veinfire borderpost'/'ARB', 'Ralph Horsley').
card_number('veinfire borderpost'/'ARB', '48').
card_multiverse_id('veinfire borderpost'/'ARB', '189640').

card_in_set('vengeful rebirth', 'ARB').
card_original_type('vengeful rebirth'/'ARB', 'Sorcery').
card_original_text('vengeful rebirth'/'ARB', 'Return target card from your graveyard to your hand. If you return a nonland card to your hand this way, Vengeful Rebirth deals damage equal to that card\'s converted mana cost to target creature or player.\nRemove Vengeful Rebirth from the game.').
card_first_print('vengeful rebirth', 'ARB').
card_image_name('vengeful rebirth'/'ARB', 'vengeful rebirth').
card_uid('vengeful rebirth'/'ARB', 'ARB:Vengeful Rebirth:vengeful rebirth').
card_rarity('vengeful rebirth'/'ARB', 'Uncommon').
card_artist('vengeful rebirth'/'ARB', 'Vance Kovacs').
card_number('vengeful rebirth'/'ARB', '62').
card_multiverse_id('vengeful rebirth'/'ARB', '179628').

card_in_set('violent outburst', 'ARB').
card_original_type('violent outburst'/'ARB', 'Instant').
card_original_text('violent outburst'/'ARB', 'Cascade (When you play this spell, remove cards from the top of your library from the game until you remove a nonland card that costs less. You may play it without paying its mana cost. Put the removed cards on the bottom in a random order.)\nCreatures you control get +1/+0 until end of turn.').
card_first_print('violent outburst', 'ARB').
card_image_name('violent outburst'/'ARB', 'violent outburst').
card_uid('violent outburst'/'ARB', 'ARB:Violent Outburst:violent outburst').
card_rarity('violent outburst'/'ARB', 'Common').
card_artist('violent outburst'/'ARB', 'Richard Whitters').
card_number('violent outburst'/'ARB', '63').
card_multiverse_id('violent outburst'/'ARB', '185056').

card_in_set('vithian renegades', 'ARB').
card_original_type('vithian renegades'/'ARB', 'Creature — Human Shaman').
card_original_text('vithian renegades'/'ARB', 'When Vithian Renegades comes into play, destroy target artifact.').
card_first_print('vithian renegades', 'ARB').
card_image_name('vithian renegades'/'ARB', 'vithian renegades').
card_uid('vithian renegades'/'ARB', 'ARB:Vithian Renegades:vithian renegades').
card_rarity('vithian renegades'/'ARB', 'Uncommon').
card_artist('vithian renegades'/'ARB', 'rk post').
card_number('vithian renegades'/'ARB', '64').
card_flavor_text('vithian renegades'/'ARB', '\"Here is the proof that what has been done may yet be undone. The hope we thought dead will be gloriously reborn.\"').
card_multiverse_id('vithian renegades'/'ARB', '179535').

card_in_set('wall of denial', 'ARB').
card_original_type('wall of denial'/'ARB', 'Creature — Wall').
card_original_text('wall of denial'/'ARB', 'Defender, flying, shroud').
card_first_print('wall of denial', 'ARB').
card_image_name('wall of denial'/'ARB', 'wall of denial').
card_uid('wall of denial'/'ARB', 'ARB:Wall of Denial:wall of denial').
card_rarity('wall of denial'/'ARB', 'Uncommon').
card_artist('wall of denial'/'ARB', 'Howard Lyon').
card_number('wall of denial'/'ARB', '16').
card_flavor_text('wall of denial'/'ARB', 'It provides what every discerning mage requires—time to think.').
card_multiverse_id('wall of denial'/'ARB', '179601').

card_in_set('wargate', 'ARB').
card_original_type('wargate'/'ARB', 'Sorcery').
card_original_text('wargate'/'ARB', 'Search your library for a permanent card with converted mana cost X or less, put it into play, then shuffle your library.').
card_first_print('wargate', 'ARB').
card_image_name('wargate'/'ARB', 'wargate').
card_uid('wargate'/'ARB', 'ARB:Wargate:wargate').
card_rarity('wargate'/'ARB', 'Rare').
card_artist('wargate'/'ARB', 'Franz Vohwinkel').
card_number('wargate'/'ARB', '129').
card_flavor_text('wargate'/'ARB', 'Bant mages still call to the heavens for aid, but angels are not the only ones who answer.').
card_multiverse_id('wargate'/'ARB', '180592').

card_in_set('wildfield borderpost', 'ARB').
card_original_type('wildfield borderpost'/'ARB', 'Artifact').
card_original_text('wildfield borderpost'/'ARB', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Wildfield Borderpost\'s mana cost.\nWildfield Borderpost comes into play tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('wildfield borderpost', 'ARB').
card_image_name('wildfield borderpost'/'ARB', 'wildfield borderpost').
card_uid('wildfield borderpost'/'ARB', 'ARB:Wildfield Borderpost:wildfield borderpost').
card_rarity('wildfield borderpost'/'ARB', 'Common').
card_artist('wildfield borderpost'/'ARB', 'Zoltan Boros & Gabor Szikszai').
card_number('wildfield borderpost'/'ARB', '80').
card_multiverse_id('wildfield borderpost'/'ARB', '179590').

card_in_set('winged coatl', 'ARB').
card_original_type('winged coatl'/'ARB', 'Creature — Snake').
card_original_text('winged coatl'/'ARB', 'Flash\nFlying\nDeathtouch (Whenever this creature deals damage to a creature, destroy that creature.)').
card_first_print('winged coatl', 'ARB').
card_image_name('winged coatl'/'ARB', 'winged coatl').
card_uid('winged coatl'/'ARB', 'ARB:Winged Coatl:winged coatl').
card_rarity('winged coatl'/'ARB', 'Common').
card_artist('winged coatl'/'ARB', 'Izzy').
card_number('winged coatl'/'ARB', '105').
card_flavor_text('winged coatl'/'ARB', 'The nacatl called this new species \"vetli,\" their word for poison arrows.').
card_multiverse_id('winged coatl'/'ARB', '179603').

card_in_set('zealous persecution', 'ARB').
card_original_type('zealous persecution'/'ARB', 'Instant').
card_original_text('zealous persecution'/'ARB', 'Until end of turn, creatures you control get +1/+1 and creatures your opponents control get -1/-1.').
card_first_print('zealous persecution', 'ARB').
card_image_name('zealous persecution'/'ARB', 'zealous persecution').
card_uid('zealous persecution'/'ARB', 'ARB:Zealous Persecution:zealous persecution').
card_rarity('zealous persecution'/'ARB', 'Uncommon').
card_artist('zealous persecution'/'ARB', 'Christopher Moeller').
card_number('zealous persecution'/'ARB', '85').
card_flavor_text('zealous persecution'/'ARB', '\"Jharic returned from Grixis changed, a haunted look in her eyes. She destroyed them with an unholy glee that made me shudder.\"\n—Knight-Captain Wyhorn').
card_multiverse_id('zealous persecution'/'ARB', '179575').
