% Magic 2015 Core Set

set('M15').
set_name('M15', 'Magic 2015 Core Set').
set_release_date('M15', '2014-07-18').
set_border('M15', 'black').
set_type('M15', 'core').

card_in_set('accursed spirit', 'M15').
card_original_type('accursed spirit'/'M15', 'Creature — Spirit').
card_original_text('accursed spirit'/'M15', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_image_name('accursed spirit'/'M15', 'accursed spirit').
card_uid('accursed spirit'/'M15', 'M15:Accursed Spirit:accursed spirit').
card_rarity('accursed spirit'/'M15', 'Common').
card_artist('accursed spirit'/'M15', 'Kev Walker').
card_number('accursed spirit'/'M15', '85').
card_flavor_text('accursed spirit'/'M15', 'Many have heard the slither of dragging armor and the soft squelch of its voice. But only its victims ever meet its icy gaze.').
card_multiverse_id('accursed spirit'/'M15', '383175').

card_in_set('act on impulse', 'M15').
card_original_type('act on impulse'/'M15', 'Sorcery').
card_original_text('act on impulse'/'M15', 'Exile the top three cards of your library. Until end of turn, you may play cards exiled this way. (If you cast a spell this way, you still pay its costs. You can play a land this way only if you have an available land play remaining.)').
card_first_print('act on impulse', 'M15').
card_image_name('act on impulse'/'M15', 'act on impulse').
card_uid('act on impulse'/'M15', 'M15:Act on Impulse:act on impulse').
card_rarity('act on impulse'/'M15', 'Uncommon').
card_artist('act on impulse'/'M15', 'Brad Rigney').
card_number('act on impulse'/'M15', '126').
card_flavor_text('act on impulse'/'M15', '\"You don\'t want to know what happens after I put on the goggles.\"').
card_multiverse_id('act on impulse'/'M15', '383176').

card_in_set('aegis angel', 'M15').
card_original_type('aegis angel'/'M15', 'Creature — Angel').
card_original_text('aegis angel'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Aegis Angel enters the battlefield, another target permanent gains indestructible for as long as you control Aegis Angel. (Effects that say \"destroy\" don\'t destroy it. A creature with indestructible can\'t be destroyed by damage.)').
card_image_name('aegis angel'/'M15', 'aegis angel').
card_uid('aegis angel'/'M15', 'M15:Aegis Angel:aegis angel').
card_rarity('aegis angel'/'M15', 'Rare').
card_artist('aegis angel'/'M15', 'Aleksi Briclot').
card_number('aegis angel'/'M15', '270').
card_multiverse_id('aegis angel'/'M15', '383160').

card_in_set('aeronaut tinkerer', 'M15').
card_original_type('aeronaut tinkerer'/'M15', 'Creature — Human Artificer').
card_original_text('aeronaut tinkerer'/'M15', 'Aeronaut Tinkerer has flying as long as you control an artifact. (It can\'t be blocked except by creatures with flying or reach.)').
card_image_name('aeronaut tinkerer'/'M15', 'aeronaut tinkerer').
card_uid('aeronaut tinkerer'/'M15', 'M15:Aeronaut Tinkerer:aeronaut tinkerer').
card_rarity('aeronaut tinkerer'/'M15', 'Common').
card_artist('aeronaut tinkerer'/'M15', 'Willian Murai').
card_number('aeronaut tinkerer'/'M15', '43').
card_flavor_text('aeronaut tinkerer'/'M15', '\"All tinkerers have their heads in the clouds. I don\'t intend to stop there.\"').
card_multiverse_id('aeronaut tinkerer'/'M15', '383177').

card_in_set('ætherspouts', 'M15').
card_original_type('ætherspouts'/'M15', 'Instant').
card_original_text('ætherspouts'/'M15', 'For each attacking creature, its owner puts it on the top or bottom of his or her library.').
card_first_print('ætherspouts', 'M15').
card_image_name('ætherspouts'/'M15', 'aetherspouts').
card_uid('ætherspouts'/'M15', 'M15:Ætherspouts:aetherspouts').
card_rarity('ætherspouts'/'M15', 'Rare').
card_artist('ætherspouts'/'M15', 'Dan Scott').
card_number('ætherspouts'/'M15', '44').
card_flavor_text('ætherspouts'/'M15', '\"Don\'t worry, there\'s plenty for everyone.\"\n—Vickon, Eleventh Company battlemage').
card_multiverse_id('ætherspouts'/'M15', '383178').

card_in_set('aggressive mining', 'M15').
card_original_type('aggressive mining'/'M15', 'Enchantment').
card_original_text('aggressive mining'/'M15', 'You can\'t play lands.\nSacrifice a land: Draw two cards. Activate this ability only once each turn.').
card_first_print('aggressive mining', 'M15').
card_image_name('aggressive mining'/'M15', 'aggressive mining').
card_uid('aggressive mining'/'M15', 'M15:Aggressive Mining:aggressive mining').
card_rarity('aggressive mining'/'M15', 'Rare').
card_artist('aggressive mining'/'M15', 'Franz Vohwinkel').
card_number('aggressive mining'/'M15', '127').
card_flavor_text('aggressive mining'/'M15', 'Designed by Markus Persson').
card_multiverse_id('aggressive mining'/'M15', '383179').

card_in_set('ajani steadfast', 'M15').
card_original_type('ajani steadfast'/'M15', 'Planeswalker — Ajani').
card_original_text('ajani steadfast'/'M15', '+1: Until end of turn, up to one target creature gets +1/+1 and gains first strike, vigilance, and lifelink.\n−2: Put a +1/+1 counter on each creature you control and a loyalty counter on each other planeswalker you control.\n−7: You get an emblem with \"If a source would deal damage to you or a planeswalker you control, prevent all but 1 of that damage.\"').
card_image_name('ajani steadfast'/'M15', 'ajani steadfast').
card_uid('ajani steadfast'/'M15', 'M15:Ajani Steadfast:ajani steadfast').
card_rarity('ajani steadfast'/'M15', 'Mythic Rare').
card_artist('ajani steadfast'/'M15', 'Chris Rahn').
card_number('ajani steadfast'/'M15', '1').
card_multiverse_id('ajani steadfast'/'M15', '383180').

card_in_set('ajani\'s pridemate', 'M15').
card_original_type('ajani\'s pridemate'/'M15', 'Creature — Cat Soldier').
card_original_text('ajani\'s pridemate'/'M15', 'Whenever you gain life, you may put a +1/+1 counter on Ajani\'s Pridemate.').
card_image_name('ajani\'s pridemate'/'M15', 'ajani\'s pridemate').
card_uid('ajani\'s pridemate'/'M15', 'M15:Ajani\'s Pridemate:ajani\'s pridemate').
card_rarity('ajani\'s pridemate'/'M15', 'Uncommon').
card_artist('ajani\'s pridemate'/'M15', 'Svetlin Velinov').
card_number('ajani\'s pridemate'/'M15', '2').
card_flavor_text('ajani\'s pridemate'/'M15', '\"When one of us prospers, the pride prospers.\"\n—Jazal Goldmane').
card_multiverse_id('ajani\'s pridemate'/'M15', '383181').

card_in_set('altac bloodseeker', 'M15').
card_original_type('altac bloodseeker'/'M15', 'Creature — Human Berserker').
card_original_text('altac bloodseeker'/'M15', 'Whenever a creature an opponent controls dies, Altac Bloodseeker gets +2/+0 and gains first strike and haste until end of turn. (It deals combat damage before creatures without first strike, and it can attack and {T} as soon as it comes under your control.)').
card_first_print('altac bloodseeker', 'M15').
card_image_name('altac bloodseeker'/'M15', 'altac bloodseeker').
card_uid('altac bloodseeker'/'M15', 'M15:Altac Bloodseeker:altac bloodseeker').
card_rarity('altac bloodseeker'/'M15', 'Uncommon').
card_artist('altac bloodseeker'/'M15', 'Cynthia Sheppard').
card_number('altac bloodseeker'/'M15', '128').
card_multiverse_id('altac bloodseeker'/'M15', '383182').

card_in_set('amphin pathmage', 'M15').
card_original_type('amphin pathmage'/'M15', 'Creature — Salamander Wizard').
card_original_text('amphin pathmage'/'M15', '{2}{U}: Target creature can\'t be blocked this turn.').
card_first_print('amphin pathmage', 'M15').
card_image_name('amphin pathmage'/'M15', 'amphin pathmage').
card_uid('amphin pathmage'/'M15', 'M15:Amphin Pathmage:amphin pathmage').
card_rarity('amphin pathmage'/'M15', 'Common').
card_artist('amphin pathmage'/'M15', 'Mark Winters').
card_number('amphin pathmage'/'M15', '45').
card_flavor_text('amphin pathmage'/'M15', '\"There are those who do not believe in the existence of the amphin. This seems somehow to be of their own design.\"\n—Gor Muldrak, Cryptohistories').
card_multiverse_id('amphin pathmage'/'M15', '383183').

card_in_set('ancient silverback', 'M15').
card_original_type('ancient silverback'/'M15', 'Creature — Ape').
card_original_text('ancient silverback'/'M15', '{G}: Regenerate Ancient Silverback. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('ancient silverback'/'M15', 'ancient silverback').
card_uid('ancient silverback'/'M15', 'M15:Ancient Silverback:ancient silverback').
card_rarity('ancient silverback'/'M15', 'Uncommon').
card_artist('ancient silverback'/'M15', 'Scott M. Fischer').
card_number('ancient silverback'/'M15', '168').
card_multiverse_id('ancient silverback'/'M15', '383184').

card_in_set('avacyn, guardian angel', 'M15').
card_original_type('avacyn, guardian angel'/'M15', 'Legendary Creature — Angel').
card_original_text('avacyn, guardian angel'/'M15', 'Flying, vigilance\n{1}{W}: Prevent all damage that would be dealt to another target creature this turn by sources of the color of your choice.\n{5}{W}{W}: Prevent all damage that would be dealt to target player this turn by sources of the color of your choice.').
card_first_print('avacyn, guardian angel', 'M15').
card_image_name('avacyn, guardian angel'/'M15', 'avacyn, guardian angel').
card_uid('avacyn, guardian angel'/'M15', 'M15:Avacyn, Guardian Angel:avacyn, guardian angel').
card_rarity('avacyn, guardian angel'/'M15', 'Rare').
card_artist('avacyn, guardian angel'/'M15', 'Winona Nelson').
card_number('avacyn, guardian angel'/'M15', '3').
card_multiverse_id('avacyn, guardian angel'/'M15', '383185').

card_in_set('avarice amulet', 'M15').
card_original_type('avarice amulet'/'M15', 'Artifact — Equipment').
card_original_text('avarice amulet'/'M15', 'Equipped creature gets +2/+0 and has vigilance and \"At the beginning of your upkeep, draw a card.\"\nWhen equipped creature dies, target opponent gains control of Avarice Amulet.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('avarice amulet', 'M15').
card_image_name('avarice amulet'/'M15', 'avarice amulet').
card_uid('avarice amulet'/'M15', 'M15:Avarice Amulet:avarice amulet').
card_rarity('avarice amulet'/'M15', 'Rare').
card_artist('avarice amulet'/'M15', 'Steven Belledin').
card_number('avarice amulet'/'M15', '212').
card_flavor_text('avarice amulet'/'M15', 'Designed by Penny Arcade').
card_multiverse_id('avarice amulet'/'M15', '383186').

card_in_set('back to nature', 'M15').
card_original_type('back to nature'/'M15', 'Instant').
card_original_text('back to nature'/'M15', 'Destroy all enchantments.').
card_image_name('back to nature'/'M15', 'back to nature').
card_uid('back to nature'/'M15', 'M15:Back to Nature:back to nature').
card_rarity('back to nature'/'M15', 'Uncommon').
card_artist('back to nature'/'M15', 'Howard Lyon').
card_number('back to nature'/'M15', '169').
card_flavor_text('back to nature'/'M15', 'There will always be those who presume to improve nature, or dare to befoul it. And nature will always have a response.').
card_multiverse_id('back to nature'/'M15', '383187').

card_in_set('battle mastery', 'M15').
card_original_type('battle mastery'/'M15', 'Enchantment — Aura').
card_original_text('battle mastery'/'M15', 'Enchant creature\nEnchanted creature has double strike. (It deals both first-strike and regular combat damage.)').
card_image_name('battle mastery'/'M15', 'battle mastery').
card_uid('battle mastery'/'M15', 'M15:Battle Mastery:battle mastery').
card_rarity('battle mastery'/'M15', 'Uncommon').
card_artist('battle mastery'/'M15', 'Zoltan Boros & Gabor Szikszai').
card_number('battle mastery'/'M15', '4').
card_flavor_text('battle mastery'/'M15', '\"Boom! Boom! Boots the size of oxcarts, then an axe like a falling sun. Elves scattered. Trees scattered. Even the hills ran for the hills!\"\n—Clachan Tales').
card_multiverse_id('battle mastery'/'M15', '383188').

card_in_set('battlefield forge', 'M15').
card_original_type('battlefield forge'/'M15', 'Land').
card_original_text('battlefield forge'/'M15', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Battlefield Forge deals 1 damage to you.').
card_image_name('battlefield forge'/'M15', 'battlefield forge').
card_uid('battlefield forge'/'M15', 'M15:Battlefield Forge:battlefield forge').
card_rarity('battlefield forge'/'M15', 'Rare').
card_artist('battlefield forge'/'M15', 'Darrell Riche').
card_number('battlefield forge'/'M15', '240').
card_multiverse_id('battlefield forge'/'M15', '383189').

card_in_set('belligerent sliver', 'M15').
card_original_type('belligerent sliver'/'M15', 'Creature — Sliver').
card_original_text('belligerent sliver'/'M15', 'Sliver creatures you control have \"This creature can\'t be blocked except by two or more creatures.\"').
card_first_print('belligerent sliver', 'M15').
card_image_name('belligerent sliver'/'M15', 'belligerent sliver').
card_uid('belligerent sliver'/'M15', 'M15:Belligerent Sliver:belligerent sliver').
card_rarity('belligerent sliver'/'M15', 'Uncommon').
card_artist('belligerent sliver'/'M15', 'Raymond Swanland').
card_number('belligerent sliver'/'M15', '129').
card_flavor_text('belligerent sliver'/'M15', '\"The slivers became adept at provoking a fear response in other species.\"\n—Hastric, Thunian scout').
card_multiverse_id('belligerent sliver'/'M15', '383190').

card_in_set('black cat', 'M15').
card_original_type('black cat'/'M15', 'Creature — Zombie Cat').
card_original_text('black cat'/'M15', 'When Black Cat dies, target opponent discards a card at random.').
card_image_name('black cat'/'M15', 'black cat').
card_uid('black cat'/'M15', 'M15:Black Cat:black cat').
card_rarity('black cat'/'M15', 'Common').
card_artist('black cat'/'M15', 'David Palumbo').
card_number('black cat'/'M15', '86').
card_flavor_text('black cat'/'M15', 'Its last life is spent tormenting your dreams.').
card_multiverse_id('black cat'/'M15', '383191').

card_in_set('blastfire bolt', 'M15').
card_original_type('blastfire bolt'/'M15', 'Instant').
card_original_text('blastfire bolt'/'M15', 'Blastfire Bolt deals 5 damage to target creature. Destroy all Equipment attached to that creature.').
card_first_print('blastfire bolt', 'M15').
card_image_name('blastfire bolt'/'M15', 'blastfire bolt').
card_uid('blastfire bolt'/'M15', 'M15:Blastfire Bolt:blastfire bolt').
card_rarity('blastfire bolt'/'M15', 'Common').
card_artist('blastfire bolt'/'M15', 'Phill Simmer').
card_number('blastfire bolt'/'M15', '130').
card_flavor_text('blastfire bolt'/'M15', '\"Encase yourself in the most elaborate armor, and cower behind the heaviest shield. I would hate for you to feel helpless.\"\n—Korig the Ruiner').
card_multiverse_id('blastfire bolt'/'M15', '383192').

card_in_set('blood host', 'M15').
card_original_type('blood host'/'M15', 'Creature — Vampire').
card_original_text('blood host'/'M15', '{1}{B}, Sacrifice another creature: Put a +1/+1 counter on Blood Host and you gain 2 life.').
card_first_print('blood host', 'M15').
card_image_name('blood host'/'M15', 'blood host').
card_uid('blood host'/'M15', 'M15:Blood Host:blood host').
card_rarity('blood host'/'M15', 'Uncommon').
card_artist('blood host'/'M15', 'Cynthia Sheppard').
card_number('blood host'/'M15', '87').
card_flavor_text('blood host'/'M15', 'It would be ill-mannered to decline his invitation. It would be ill-advised to accept it.').
card_multiverse_id('blood host'/'M15', '383193').

card_in_set('boonweaver giant', 'M15').
card_original_type('boonweaver giant'/'M15', 'Creature — Giant Monk').
card_original_text('boonweaver giant'/'M15', 'When Boonweaver Giant enters the battlefield, you may search your graveyard, hand, and/or library for an Aura card and put it onto the battlefield attached to Boonweaver Giant. If you search your library this way, shuffle it.').
card_first_print('boonweaver giant', 'M15').
card_image_name('boonweaver giant'/'M15', 'boonweaver giant').
card_uid('boonweaver giant'/'M15', 'M15:Boonweaver Giant:boonweaver giant').
card_rarity('boonweaver giant'/'M15', 'Uncommon').
card_artist('boonweaver giant'/'M15', 'Adam Paquette').
card_number('boonweaver giant'/'M15', '5').
card_multiverse_id('boonweaver giant'/'M15', '383194').

card_in_set('borderland marauder', 'M15').
card_original_type('borderland marauder'/'M15', 'Creature — Human Warrior').
card_original_text('borderland marauder'/'M15', 'Whenever Borderland Marauder attacks, it gets +2/+0 until end of turn.').
card_first_print('borderland marauder', 'M15').
card_image_name('borderland marauder'/'M15', 'borderland marauder').
card_uid('borderland marauder'/'M15', 'M15:Borderland Marauder:borderland marauder').
card_rarity('borderland marauder'/'M15', 'Common').
card_artist('borderland marauder'/'M15', 'Scott M. Fischer').
card_number('borderland marauder'/'M15', '131').
card_flavor_text('borderland marauder'/'M15', 'Though she is rightly feared, there are relatively few tales of her deeds in battle, for few survive her raids.').
card_multiverse_id('borderland marauder'/'M15', '383195').

card_in_set('brawler\'s plate', 'M15').
card_original_type('brawler\'s plate'/'M15', 'Artifact — Equipment').
card_original_text('brawler\'s plate'/'M15', 'Equipped creature gets +2/+2 and has trample. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('brawler\'s plate', 'M15').
card_image_name('brawler\'s plate'/'M15', 'brawler\'s plate').
card_uid('brawler\'s plate'/'M15', 'M15:Brawler\'s Plate:brawler\'s plate').
card_rarity('brawler\'s plate'/'M15', 'Uncommon').
card_artist('brawler\'s plate'/'M15', 'Jung Park').
card_number('brawler\'s plate'/'M15', '213').
card_multiverse_id('brawler\'s plate'/'M15', '383196').

card_in_set('bronze sable', 'M15').
card_original_type('bronze sable'/'M15', 'Artifact Creature — Sable').
card_original_text('bronze sable'/'M15', '').
card_image_name('bronze sable'/'M15', 'bronze sable').
card_uid('bronze sable'/'M15', 'M15:Bronze Sable:bronze sable').
card_rarity('bronze sable'/'M15', 'Common').
card_artist('bronze sable'/'M15', 'Jasper Sandner').
card_number('bronze sable'/'M15', '214').
card_flavor_text('bronze sable'/'M15', 'The Champion stood alone between the horde of the Returned and the shrine to Karametra, cutting down scores among hundreds. She would have been overcome if not for the aid of the temple guardians whom Karametra awakened.\n—The Theriad').
card_multiverse_id('bronze sable'/'M15', '383197').

card_in_set('brood keeper', 'M15').
card_original_type('brood keeper'/'M15', 'Creature — Human Shaman').
card_original_text('brood keeper'/'M15', 'Whenever an Aura becomes attached to Brood Keeper, put a 2/2 red Dragon creature token with flying onto the battlefield. It has \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('brood keeper', 'M15').
card_image_name('brood keeper'/'M15', 'brood keeper').
card_uid('brood keeper'/'M15', 'M15:Brood Keeper:brood keeper').
card_rarity('brood keeper'/'M15', 'Uncommon').
card_artist('brood keeper'/'M15', 'Scott Murphy').
card_number('brood keeper'/'M15', '132').
card_flavor_text('brood keeper'/'M15', '\"Come, little one. Unfurl your wings, fill your lungs, and release your first fiery breath.\"').
card_multiverse_id('brood keeper'/'M15', '383198').

card_in_set('burning anger', 'M15').
card_original_type('burning anger'/'M15', 'Enchantment — Aura').
card_original_text('burning anger'/'M15', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals damage equal to its power to target creature or player.\"').
card_first_print('burning anger', 'M15').
card_image_name('burning anger'/'M15', 'burning anger').
card_uid('burning anger'/'M15', 'M15:Burning Anger:burning anger').
card_rarity('burning anger'/'M15', 'Rare').
card_artist('burning anger'/'M15', 'Anthony Palumbo').
card_number('burning anger'/'M15', '133').
card_flavor_text('burning anger'/'M15', '\"With rage as your forge, your hammer can smite anything.\"').
card_multiverse_id('burning anger'/'M15', '383199').

card_in_set('cancel', 'M15').
card_original_type('cancel'/'M15', 'Instant').
card_original_text('cancel'/'M15', 'Counter target spell.').
card_image_name('cancel'/'M15', 'cancel').
card_uid('cancel'/'M15', 'M15:Cancel:cancel').
card_rarity('cancel'/'M15', 'Common').
card_artist('cancel'/'M15', 'David Palumbo').
card_number('cancel'/'M15', '274').
card_flavor_text('cancel'/'M15', '\"What you are attempting is not against the law. It is, however, extremely foolish.\"').
card_multiverse_id('cancel'/'M15', '383161').

card_in_set('carnivorous moss-beast', 'M15').
card_original_type('carnivorous moss-beast'/'M15', 'Creature — Plant Elemental Beast').
card_original_text('carnivorous moss-beast'/'M15', '{5}{G}{G}: Put a +1/+1 counter on Carnivorous Moss-Beast.').
card_first_print('carnivorous moss-beast', 'M15').
card_image_name('carnivorous moss-beast'/'M15', 'carnivorous moss-beast').
card_uid('carnivorous moss-beast'/'M15', 'M15:Carnivorous Moss-Beast:carnivorous moss-beast').
card_rarity('carnivorous moss-beast'/'M15', 'Common').
card_artist('carnivorous moss-beast'/'M15', 'Filip Burburan').
card_number('carnivorous moss-beast'/'M15', '170').
card_flavor_text('carnivorous moss-beast'/'M15', 'Ranger wisdom dictates that when fleeing from a moss-beast, you must stay calm, find your bearings, and run south.').
card_multiverse_id('carnivorous moss-beast'/'M15', '383200').

card_in_set('carrion crow', 'M15').
card_original_type('carrion crow'/'M15', 'Creature — Zombie Bird').
card_original_text('carrion crow'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nCarrion Crow enters the battlefield tapped.').
card_first_print('carrion crow', 'M15').
card_image_name('carrion crow'/'M15', 'carrion crow').
card_uid('carrion crow'/'M15', 'M15:Carrion Crow:carrion crow').
card_rarity('carrion crow'/'M15', 'Common').
card_artist('carrion crow'/'M15', 'Aaron Miller').
card_number('carrion crow'/'M15', '88').
card_flavor_text('carrion crow'/'M15', 'When carrion feeds on carrion, dark days approach.').
card_multiverse_id('carrion crow'/'M15', '383201').

card_in_set('caustic tar', 'M15').
card_original_type('caustic tar'/'M15', 'Enchantment — Aura').
card_original_text('caustic tar'/'M15', 'Enchant land\nEnchanted land has \"{T}: Target player loses 3 life.\"').
card_image_name('caustic tar'/'M15', 'caustic tar').
card_uid('caustic tar'/'M15', 'M15:Caustic Tar:caustic tar').
card_rarity('caustic tar'/'M15', 'Uncommon').
card_artist('caustic tar'/'M15', 'Jung Park').
card_number('caustic tar'/'M15', '89').
card_flavor_text('caustic tar'/'M15', 'A forest fire can rejuvenate the land, but the tar\'s vile consumption leaves the land forever ruined.').
card_multiverse_id('caustic tar'/'M15', '383202').

card_in_set('caves of koilos', 'M15').
card_original_type('caves of koilos'/'M15', 'Land').
card_original_text('caves of koilos'/'M15', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Caves of Koilos deals 1 damage to you.').
card_image_name('caves of koilos'/'M15', 'caves of koilos').
card_uid('caves of koilos'/'M15', 'M15:Caves of Koilos:caves of koilos').
card_rarity('caves of koilos'/'M15', 'Rare').
card_artist('caves of koilos'/'M15', 'Jim Nelson').
card_number('caves of koilos'/'M15', '241').
card_multiverse_id('caves of koilos'/'M15', '383203').

card_in_set('centaur courser', 'M15').
card_original_type('centaur courser'/'M15', 'Creature — Centaur Warrior').
card_original_text('centaur courser'/'M15', '').
card_image_name('centaur courser'/'M15', 'centaur courser').
card_uid('centaur courser'/'M15', 'M15:Centaur Courser:centaur courser').
card_rarity('centaur courser'/'M15', 'Common').
card_artist('centaur courser'/'M15', 'Vance Kovacs').
card_number('centaur courser'/'M15', '282').
card_flavor_text('centaur courser'/'M15', '\"The centaurs are truly free. Never will they be tamed by temptation or controlled by fear. They live in total harmony, a feat not yet achieved by our kind.\"\n—Ramal, sage of Westgate').
card_multiverse_id('centaur courser'/'M15', '383162').

card_in_set('chandra, pyromaster', 'M15').
card_original_type('chandra, pyromaster'/'M15', 'Planeswalker — Chandra').
card_original_text('chandra, pyromaster'/'M15', '+1: Chandra, Pyromaster deals 1 damage to target player and 1 damage to up to one target creature that player controls. That creature can\'t block this turn.\n0: Exile the top card of your library. You may play it this turn.\n−7: Exile the top ten cards of your library. Choose an instant or sorcery card exiled this way and copy it three times. You may cast the copies without paying their mana costs.').
card_image_name('chandra, pyromaster'/'M15', 'chandra, pyromaster').
card_uid('chandra, pyromaster'/'M15', 'M15:Chandra, Pyromaster:chandra, pyromaster').
card_rarity('chandra, pyromaster'/'M15', 'Mythic Rare').
card_artist('chandra, pyromaster'/'M15', 'Winona Nelson').
card_number('chandra, pyromaster'/'M15', '134').
card_multiverse_id('chandra, pyromaster'/'M15', '383204').

card_in_set('charging rhino', 'M15').
card_original_type('charging rhino'/'M15', 'Creature — Rhino').
card_original_text('charging rhino'/'M15', 'Charging Rhino can\'t be blocked by more than one creature.').
card_image_name('charging rhino'/'M15', 'charging rhino').
card_uid('charging rhino'/'M15', 'M15:Charging Rhino:charging rhino').
card_rarity('charging rhino'/'M15', 'Common').
card_artist('charging rhino'/'M15', 'Daren Bader').
card_number('charging rhino'/'M15', '171').
card_flavor_text('charging rhino'/'M15', 'The knights of Troinir Keep took the rhino as their emblem due to its intense focus when charging down a perceived threat—a trait they discovered on a hunting excursion.').
card_multiverse_id('charging rhino'/'M15', '383205').

card_in_set('chasm skulker', 'M15').
card_original_type('chasm skulker'/'M15', 'Creature — Squid Horror').
card_original_text('chasm skulker'/'M15', 'Whenever you draw a card, put a +1/+1 counter on Chasm Skulker.\nWhen Chasm Skulker dies, put X 1/1 blue Squid creature tokens with islandwalk onto the battlefield, where X is the number of +1/+1 counters on Chasm Skulker. (They can\'t be blocked as long as defending player controls an Island.)').
card_first_print('chasm skulker', 'M15').
card_image_name('chasm skulker'/'M15', 'chasm skulker').
card_uid('chasm skulker'/'M15', 'M15:Chasm Skulker:chasm skulker').
card_rarity('chasm skulker'/'M15', 'Rare').
card_artist('chasm skulker'/'M15', 'Jack Wang').
card_number('chasm skulker'/'M15', '46').
card_flavor_text('chasm skulker'/'M15', 'Designed by Mike Neumann').
card_multiverse_id('chasm skulker'/'M15', '383206').

card_in_set('chief engineer', 'M15').
card_original_type('chief engineer'/'M15', 'Creature — Vedalken Artificer').
card_original_text('chief engineer'/'M15', 'Artifact spells you cast have convoke. (Your creatures can help cast those spells. Each creature you tap while casting an artifact spell pays for {1} or one mana of that creature\'s color.)').
card_image_name('chief engineer'/'M15', 'chief engineer').
card_uid('chief engineer'/'M15', 'M15:Chief Engineer:chief engineer').
card_rarity('chief engineer'/'M15', 'Rare').
card_artist('chief engineer'/'M15', 'Steven Belledin').
card_number('chief engineer'/'M15', '47').
card_flavor_text('chief engineer'/'M15', 'An eye for detail, a mind for numbers, a soul of clockwork.').
card_multiverse_id('chief engineer'/'M15', '383207').

card_in_set('child of night', 'M15').
card_original_type('child of night'/'M15', 'Creature — Vampire').
card_original_text('child of night'/'M15', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('child of night'/'M15', 'child of night').
card_uid('child of night'/'M15', 'M15:Child of Night:child of night').
card_rarity('child of night'/'M15', 'Common').
card_artist('child of night'/'M15', 'Ash Wood').
card_number('child of night'/'M15', '90').
card_flavor_text('child of night'/'M15', 'Sins that would be too gruesome in the light of day are made more pleasing in the dark of night.').
card_multiverse_id('child of night'/'M15', '383208').

card_in_set('chord of calling', 'M15').
card_original_type('chord of calling'/'M15', 'Instant').
card_original_text('chord of calling'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nSearch your library for a creature card with converted mana cost X or less and put it onto the battlefield. Then shuffle your library.').
card_image_name('chord of calling'/'M15', 'chord of calling').
card_uid('chord of calling'/'M15', 'M15:Chord of Calling:chord of calling').
card_rarity('chord of calling'/'M15', 'Rare').
card_artist('chord of calling'/'M15', 'Karl Kopinski').
card_number('chord of calling'/'M15', '172').
card_multiverse_id('chord of calling'/'M15', '383209').

card_in_set('chronostutter', 'M15').
card_original_type('chronostutter'/'M15', 'Instant').
card_original_text('chronostutter'/'M15', 'Put target creature into its owner\'s library second from the top.').
card_first_print('chronostutter', 'M15').
card_image_name('chronostutter'/'M15', 'chronostutter').
card_uid('chronostutter'/'M15', 'M15:Chronostutter:chronostutter').
card_rarity('chronostutter'/'M15', 'Common').
card_artist('chronostutter'/'M15', 'Seb McKinnon').
card_number('chronostutter'/'M15', '48').
card_flavor_text('chronostutter'/'M15', 'Timing is everything.').
card_multiverse_id('chronostutter'/'M15', '383210').

card_in_set('circle of flame', 'M15').
card_original_type('circle of flame'/'M15', 'Enchantment').
card_original_text('circle of flame'/'M15', 'Whenever a creature without flying attacks you or a planeswalker you control, Circle of Flame deals 1 damage to that creature.').
card_image_name('circle of flame'/'M15', 'circle of flame').
card_uid('circle of flame'/'M15', 'M15:Circle of Flame:circle of flame').
card_rarity('circle of flame'/'M15', 'Uncommon').
card_artist('circle of flame'/'M15', 'Jaime Jones').
card_number('circle of flame'/'M15', '135').
card_flavor_text('circle of flame'/'M15', '\"Which do you think is a better deterrent: a moat of water or one of fire?\"\n—Chandra Nalaar').
card_multiverse_id('circle of flame'/'M15', '383211').

card_in_set('clear a path', 'M15').
card_original_type('clear a path'/'M15', 'Sorcery').
card_original_text('clear a path'/'M15', 'Destroy target creature with defender.').
card_image_name('clear a path'/'M15', 'clear a path').
card_uid('clear a path'/'M15', 'M15:Clear a Path:clear a path').
card_rarity('clear a path'/'M15', 'Common').
card_artist('clear a path'/'M15', 'Karl Kopinski').
card_number('clear a path'/'M15', '136').
card_flavor_text('clear a path'/'M15', '\"Why do guards always look surprised when we bash them?\" asked Ruric.\n\"I think they expect a bribe,\" said Thar.').
card_multiverse_id('clear a path'/'M15', '383212').

card_in_set('cone of flame', 'M15').
card_original_type('cone of flame'/'M15', 'Sorcery').
card_original_text('cone of flame'/'M15', 'Cone of Flame deals 1 damage to target creature or player, 2 damage to another target creature or player, and 3 damage to a third target creature or player.').
card_image_name('cone of flame'/'M15', 'cone of flame').
card_uid('cone of flame'/'M15', 'M15:Cone of Flame:cone of flame').
card_rarity('cone of flame'/'M15', 'Uncommon').
card_artist('cone of flame'/'M15', 'Chippy').
card_number('cone of flame'/'M15', '137').
card_multiverse_id('cone of flame'/'M15', '383213').

card_in_set('congregate', 'M15').
card_original_type('congregate'/'M15', 'Instant').
card_original_text('congregate'/'M15', 'Target player gains 2 life for each creature on the battlefield.').
card_image_name('congregate'/'M15', 'congregate').
card_uid('congregate'/'M15', 'M15:Congregate:congregate').
card_rarity('congregate'/'M15', 'Uncommon').
card_artist('congregate'/'M15', 'Mark Zug').
card_number('congregate'/'M15', '6').
card_flavor_text('congregate'/'M15', '\"In the gathering there is strength for all who founder, renewal for all who languish, love for all who sing.\"\n—Song of All, canto 642').
card_multiverse_id('congregate'/'M15', '383214').

card_in_set('constricting sliver', 'M15').
card_original_type('constricting sliver'/'M15', 'Creature — Sliver').
card_original_text('constricting sliver'/'M15', 'Sliver creatures you control have \"When this creature enters the battlefield, you may exile target creature an opponent controls until this creature leaves the battlefield.\"').
card_first_print('constricting sliver', 'M15').
card_image_name('constricting sliver'/'M15', 'constricting sliver').
card_uid('constricting sliver'/'M15', 'M15:Constricting Sliver:constricting sliver').
card_rarity('constricting sliver'/'M15', 'Uncommon').
card_artist('constricting sliver'/'M15', 'Karl Kopinski').
card_number('constricting sliver'/'M15', '7').
card_flavor_text('constricting sliver'/'M15', 'Slivers are often seen toying with enemies they capture, not out of cruelty, but to fully learn their physical capabilities.').
card_multiverse_id('constricting sliver'/'M15', '383215').

card_in_set('coral barrier', 'M15').
card_original_type('coral barrier'/'M15', 'Creature — Wall').
card_original_text('coral barrier'/'M15', 'Defender (This creature can\'t attack.)\nWhen Coral Barrier enters the battlefield, put a 1/1 blue Squid creature token with islandwalk onto the battlefield. (It can\'t be blocked as long as defending player controls an Island.)').
card_first_print('coral barrier', 'M15').
card_image_name('coral barrier'/'M15', 'coral barrier').
card_uid('coral barrier'/'M15', 'M15:Coral Barrier:coral barrier').
card_rarity('coral barrier'/'M15', 'Common').
card_artist('coral barrier'/'M15', 'Florian de Gesincourt').
card_number('coral barrier'/'M15', '49').
card_multiverse_id('coral barrier'/'M15', '383216').

card_in_set('covenant of blood', 'M15').
card_original_type('covenant of blood'/'M15', 'Sorcery').
card_original_text('covenant of blood'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nCovenant of Blood deals 4 damage to target creature or player and you gain 4 life.').
card_first_print('covenant of blood', 'M15').
card_image_name('covenant of blood'/'M15', 'covenant of blood').
card_uid('covenant of blood'/'M15', 'M15:Covenant of Blood:covenant of blood').
card_rarity('covenant of blood'/'M15', 'Common').
card_artist('covenant of blood'/'M15', 'Seb McKinnon').
card_number('covenant of blood'/'M15', '91').
card_multiverse_id('covenant of blood'/'M15', '383217').

card_in_set('crippling blight', 'M15').
card_original_type('crippling blight'/'M15', 'Enchantment — Aura').
card_original_text('crippling blight'/'M15', 'Enchant creature\nEnchanted creature gets -1/-1 and can\'t block.').
card_image_name('crippling blight'/'M15', 'crippling blight').
card_uid('crippling blight'/'M15', 'M15:Crippling Blight:crippling blight').
card_rarity('crippling blight'/'M15', 'Common').
card_artist('crippling blight'/'M15', 'Lucas Graciano').
card_number('crippling blight'/'M15', '92').
card_flavor_text('crippling blight'/'M15', '\"Still alive? No matter. I\'ll leave you as a warning to others who would oppose me.\"\n—Vish Kal, Blood Arbiter').
card_multiverse_id('crippling blight'/'M15', '383218').

card_in_set('crowd\'s favor', 'M15').
card_original_type('crowd\'s favor'/'M15', 'Instant').
card_original_text('crowd\'s favor'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTarget creature gets +1/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_first_print('crowd\'s favor', 'M15').
card_image_name('crowd\'s favor'/'M15', 'crowd\'s favor').
card_uid('crowd\'s favor'/'M15', 'M15:Crowd\'s Favor:crowd\'s favor').
card_rarity('crowd\'s favor'/'M15', 'Common').
card_artist('crowd\'s favor'/'M15', 'Slawomir Maniak').
card_number('crowd\'s favor'/'M15', '138').
card_multiverse_id('crowd\'s favor'/'M15', '383219').

card_in_set('crucible of fire', 'M15').
card_original_type('crucible of fire'/'M15', 'Enchantment').
card_original_text('crucible of fire'/'M15', 'Dragon creatures you control get +3/+3.').
card_image_name('crucible of fire'/'M15', 'crucible of fire').
card_uid('crucible of fire'/'M15', 'M15:Crucible of Fire:crucible of fire').
card_rarity('crucible of fire'/'M15', 'Rare').
card_artist('crucible of fire'/'M15', 'Dominick Domingo').
card_number('crucible of fire'/'M15', '139').
card_flavor_text('crucible of fire'/'M15', '\"The dragon is a perfect marriage of power and the will to use it.\"\n—Sarkhan Vol').
card_multiverse_id('crucible of fire'/'M15', '383220').

card_in_set('cruel sadist', 'M15').
card_original_type('cruel sadist'/'M15', 'Creature — Human Assassin').
card_original_text('cruel sadist'/'M15', '{B}, {T}, Pay 1 life: Put a +1/+1 counter on Cruel Sadist.\n{2}{B}, {T}, Remove X +1/+1 counters from Cruel Sadist: Cruel Sadist deals X damage to target creature.').
card_first_print('cruel sadist', 'M15').
card_image_name('cruel sadist'/'M15', 'cruel sadist').
card_uid('cruel sadist'/'M15', 'M15:Cruel Sadist:cruel sadist').
card_rarity('cruel sadist'/'M15', 'Rare').
card_artist('cruel sadist'/'M15', 'Min Yum').
card_number('cruel sadist'/'M15', '93').
card_flavor_text('cruel sadist'/'M15', 'Face of innocence. Hand of death.\nDesigned by Edmund McMillen').
card_multiverse_id('cruel sadist'/'M15', '383221').

card_in_set('darksteel citadel', 'M15').
card_original_type('darksteel citadel'/'M15', 'Artifact Land').
card_original_text('darksteel citadel'/'M15', 'Indestructible (Effects that say \"destroy\" don\'t destroy this land.)\n{T}: Add {1} to your mana pool.').
card_image_name('darksteel citadel'/'M15', 'darksteel citadel').
card_uid('darksteel citadel'/'M15', 'M15:Darksteel Citadel:darksteel citadel').
card_rarity('darksteel citadel'/'M15', 'Uncommon').
card_artist('darksteel citadel'/'M15', 'John Avon').
card_number('darksteel citadel'/'M15', '242').
card_flavor_text('darksteel citadel'/'M15', 'Structures built from darksteel yield to neither assault nor age.').
card_multiverse_id('darksteel citadel'/'M15', '383222').

card_in_set('dauntless river marshal', 'M15').
card_original_type('dauntless river marshal'/'M15', 'Creature — Human Soldier').
card_original_text('dauntless river marshal'/'M15', 'Dauntless River Marshal gets +1/+1 as long as you control an Island.\n{3}{U}: Tap target creature.').
card_first_print('dauntless river marshal', 'M15').
card_image_name('dauntless river marshal'/'M15', 'dauntless river marshal').
card_uid('dauntless river marshal'/'M15', 'M15:Dauntless River Marshal:dauntless river marshal').
card_rarity('dauntless river marshal'/'M15', 'Uncommon').
card_artist('dauntless river marshal'/'M15', 'Mark Winters').
card_number('dauntless river marshal'/'M15', '8').
card_flavor_text('dauntless river marshal'/'M15', '\"Thieves and squid squirm the same way when you capture them.\"').
card_multiverse_id('dauntless river marshal'/'M15', '383223').

card_in_set('devouring light', 'M15').
card_original_type('devouring light'/'M15', 'Instant').
card_original_text('devouring light'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nExile target attacking or blocking creature.').
card_image_name('devouring light'/'M15', 'devouring light').
card_uid('devouring light'/'M15', 'M15:Devouring Light:devouring light').
card_rarity('devouring light'/'M15', 'Uncommon').
card_artist('devouring light'/'M15', 'Slawomir Maniak').
card_number('devouring light'/'M15', '9').
card_flavor_text('devouring light'/'M15', '\"Even your shadow is too foul to tolerate.\"').
card_multiverse_id('devouring light'/'M15', '383224').

card_in_set('diffusion sliver', 'M15').
card_original_type('diffusion sliver'/'M15', 'Creature — Sliver').
card_original_text('diffusion sliver'/'M15', 'Whenever a Sliver creature you control becomes the target of a spell or ability an opponent controls, counter that spell or ability unless its controller pays {2}.').
card_first_print('diffusion sliver', 'M15').
card_image_name('diffusion sliver'/'M15', 'diffusion sliver').
card_uid('diffusion sliver'/'M15', 'M15:Diffusion Sliver:diffusion sliver').
card_rarity('diffusion sliver'/'M15', 'Uncommon').
card_artist('diffusion sliver'/'M15', 'Trevor Claxton').
card_number('diffusion sliver'/'M15', '50').
card_flavor_text('diffusion sliver'/'M15', '\"The hive shimmered, and its walls seemed a living mirror.\"\n—Hastric, Thunian scout').
card_multiverse_id('diffusion sliver'/'M15', '383225').

card_in_set('dissipate', 'M15').
card_original_type('dissipate'/'M15', 'Instant').
card_original_text('dissipate'/'M15', 'Counter target spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_image_name('dissipate'/'M15', 'dissipate').
card_uid('dissipate'/'M15', 'M15:Dissipate:dissipate').
card_rarity('dissipate'/'M15', 'Uncommon').
card_artist('dissipate'/'M15', 'Tomasz Jedruszek').
card_number('dissipate'/'M15', '51').
card_flavor_text('dissipate'/'M15', '\"This abomination never belonged in our world. I\'m merely setting it free.\"\n—Dierk, geistmage').
card_multiverse_id('dissipate'/'M15', '383226').

card_in_set('divination', 'M15').
card_original_type('divination'/'M15', 'Sorcery').
card_original_text('divination'/'M15', 'Draw two cards.').
card_image_name('divination'/'M15', 'divination').
card_uid('divination'/'M15', 'M15:Divination:divination').
card_rarity('divination'/'M15', 'Common').
card_artist('divination'/'M15', 'Howard Lyon').
card_number('divination'/'M15', '52').
card_flavor_text('divination'/'M15', '\"The key to unlocking this puzzle is within you.\"\n—Doriel, mentor of Mistral Isle').
card_multiverse_id('divination'/'M15', '383227').

card_in_set('divine favor', 'M15').
card_original_type('divine favor'/'M15', 'Enchantment — Aura').
card_original_text('divine favor'/'M15', 'Enchant creature\nWhen Divine Favor enters the battlefield, you gain 3 life.\nEnchanted creature gets +1/+3.').
card_image_name('divine favor'/'M15', 'divine favor').
card_uid('divine favor'/'M15', 'M15:Divine Favor:divine favor').
card_rarity('divine favor'/'M15', 'Common').
card_artist('divine favor'/'M15', 'Allen Williams').
card_number('divine favor'/'M15', '10').
card_flavor_text('divine favor'/'M15', 'With an armory of light, even the squire may champion her people.').
card_multiverse_id('divine favor'/'M15', '383228').

card_in_set('divine verdict', 'M15').
card_original_type('divine verdict'/'M15', 'Instant').
card_original_text('divine verdict'/'M15', 'Destroy target attacking or blocking creature.').
card_image_name('divine verdict'/'M15', 'divine verdict').
card_uid('divine verdict'/'M15', 'M15:Divine Verdict:divine verdict').
card_rarity('divine verdict'/'M15', 'Common').
card_artist('divine verdict'/'M15', 'Kev Walker').
card_number('divine verdict'/'M15', '271').
card_flavor_text('divine verdict'/'M15', '\"Guilty.\"').
card_multiverse_id('divine verdict'/'M15', '383163').

card_in_set('elvish mystic', 'M15').
card_original_type('elvish mystic'/'M15', 'Creature — Elf Druid').
card_original_text('elvish mystic'/'M15', '{T}: Add {G} to your mana pool.').
card_image_name('elvish mystic'/'M15', 'elvish mystic').
card_uid('elvish mystic'/'M15', 'M15:Elvish Mystic:elvish mystic').
card_rarity('elvish mystic'/'M15', 'Common').
card_artist('elvish mystic'/'M15', 'Wesley Burt').
card_number('elvish mystic'/'M15', '173').
card_flavor_text('elvish mystic'/'M15', '\"Life grows everywhere. My kin merely find those places where it grows strongest.\"\n—Nissa Revane').
card_multiverse_id('elvish mystic'/'M15', '383229').

card_in_set('encrust', 'M15').
card_original_type('encrust'/'M15', 'Enchantment — Aura').
card_original_text('encrust'/'M15', 'Enchant artifact or creature\nEnchanted permanent doesn\'t untap during its controller\'s untap step and its activated abilities can\'t be activated.').
card_image_name('encrust'/'M15', 'encrust').
card_uid('encrust'/'M15', 'M15:Encrust:encrust').
card_rarity('encrust'/'M15', 'Common').
card_artist('encrust'/'M15', 'Jason Felix').
card_number('encrust'/'M15', '53').
card_flavor_text('encrust'/'M15', '\"The sea blesses the tiny with the power to fell the mighty.\"\n—Talrand, sky summoner').
card_multiverse_id('encrust'/'M15', '383230').

card_in_set('endless obedience', 'M15').
card_original_type('endless obedience'/'M15', 'Sorcery').
card_original_text('endless obedience'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPut target creature card from a graveyard onto the battlefield under your control.').
card_first_print('endless obedience', 'M15').
card_image_name('endless obedience'/'M15', 'endless obedience').
card_uid('endless obedience'/'M15', 'M15:Endless Obedience:endless obedience').
card_rarity('endless obedience'/'M15', 'Uncommon').
card_artist('endless obedience'/'M15', 'Karl Kopinski').
card_number('endless obedience'/'M15', '94').
card_multiverse_id('endless obedience'/'M15', '383231').

card_in_set('ensoul artifact', 'M15').
card_original_type('ensoul artifact'/'M15', 'Enchantment — Aura').
card_original_text('ensoul artifact'/'M15', 'Enchant artifact\nEnchanted artifact is a creature with base power and toughness 5/5 in addition to its other types.').
card_first_print('ensoul artifact', 'M15').
card_image_name('ensoul artifact'/'M15', 'ensoul artifact').
card_uid('ensoul artifact'/'M15', 'M15:Ensoul Artifact:ensoul artifact').
card_rarity('ensoul artifact'/'M15', 'Uncommon').
card_artist('ensoul artifact'/'M15', 'Jasper Sandner').
card_number('ensoul artifact'/'M15', '54').
card_flavor_text('ensoul artifact'/'M15', 'Too often, some self-described genius comes up with a \"labor-saving device\" that ends up creating far more work.').
card_multiverse_id('ensoul artifact'/'M15', '383232').

card_in_set('ephemeral shields', 'M15').
card_original_type('ephemeral shields'/'M15', 'Instant').
card_original_text('ephemeral shields'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTarget creature gains indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy it.)').
card_first_print('ephemeral shields', 'M15').
card_image_name('ephemeral shields'/'M15', 'ephemeral shields').
card_uid('ephemeral shields'/'M15', 'M15:Ephemeral Shields:ephemeral shields').
card_rarity('ephemeral shields'/'M15', 'Common').
card_artist('ephemeral shields'/'M15', 'Yohann Schepacz').
card_number('ephemeral shields'/'M15', '11').
card_multiverse_id('ephemeral shields'/'M15', '383233').

card_in_set('eternal thirst', 'M15').
card_original_type('eternal thirst'/'M15', 'Enchantment — Aura').
card_original_text('eternal thirst'/'M15', 'Enchant creature\nEnchanted creature has lifelink and \"Whenever a creature an opponent controls dies, put a +1/+1 counter on this creature.\" (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_first_print('eternal thirst', 'M15').
card_image_name('eternal thirst'/'M15', 'eternal thirst').
card_uid('eternal thirst'/'M15', 'M15:Eternal Thirst:eternal thirst').
card_rarity('eternal thirst'/'M15', 'Common').
card_artist('eternal thirst'/'M15', 'Clint Cearley').
card_number('eternal thirst'/'M15', '95').
card_multiverse_id('eternal thirst'/'M15', '383234').

card_in_set('evolving wilds', 'M15').
card_original_type('evolving wilds'/'M15', 'Land').
card_original_text('evolving wilds'/'M15', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'M15', 'evolving wilds').
card_uid('evolving wilds'/'M15', 'M15:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'M15', 'Common').
card_artist('evolving wilds'/'M15', 'Steven Belledin').
card_number('evolving wilds'/'M15', '243').
card_flavor_text('evolving wilds'/'M15', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'M15', '383235').

card_in_set('feast on the fallen', 'M15').
card_original_type('feast on the fallen'/'M15', 'Enchantment').
card_original_text('feast on the fallen'/'M15', 'At the beginning of each upkeep, if an opponent lost life last turn, put a +1/+1 counter on target creature you control.').
card_first_print('feast on the fallen', 'M15').
card_image_name('feast on the fallen'/'M15', 'feast on the fallen').
card_uid('feast on the fallen'/'M15', 'M15:Feast on the Fallen:feast on the fallen').
card_rarity('feast on the fallen'/'M15', 'Uncommon').
card_artist('feast on the fallen'/'M15', 'Dave Kendall').
card_number('feast on the fallen'/'M15', '96').
card_flavor_text('feast on the fallen'/'M15', '\"As our numbers dwindle, the ranks of the dead grow ever stronger.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('feast on the fallen'/'M15', '383236').

card_in_set('feral incarnation', 'M15').
card_original_type('feral incarnation'/'M15', 'Sorcery').
card_original_text('feral incarnation'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPut three 3/3 green Beast creature tokens onto the battlefield.').
card_first_print('feral incarnation', 'M15').
card_image_name('feral incarnation'/'M15', 'feral incarnation').
card_uid('feral incarnation'/'M15', 'M15:Feral Incarnation:feral incarnation').
card_rarity('feral incarnation'/'M15', 'Uncommon').
card_artist('feral incarnation'/'M15', 'Eytan Zana').
card_number('feral incarnation'/'M15', '174').
card_flavor_text('feral incarnation'/'M15', 'Nature is itself wild—in all its forms.').
card_multiverse_id('feral incarnation'/'M15', '383237').

card_in_set('festergloom', 'M15').
card_original_type('festergloom'/'M15', 'Sorcery').
card_original_text('festergloom'/'M15', 'Nonblack creatures get -1/-1 until end of turn.').
card_first_print('festergloom', 'M15').
card_image_name('festergloom'/'M15', 'festergloom').
card_uid('festergloom'/'M15', 'M15:Festergloom:festergloom').
card_rarity('festergloom'/'M15', 'Common').
card_artist('festergloom'/'M15', 'Mathias Kollros').
card_number('festergloom'/'M15', '97').
card_flavor_text('festergloom'/'M15', 'The death of a scout can be as informative as a safe return.').
card_multiverse_id('festergloom'/'M15', '383238').

card_in_set('first response', 'M15').
card_original_type('first response'/'M15', 'Enchantment').
card_original_text('first response'/'M15', 'At the beginning of each upkeep, if you lost life last turn, put a 1/1 white Soldier creature token onto the battlefield.').
card_first_print('first response', 'M15').
card_image_name('first response'/'M15', 'first response').
card_uid('first response'/'M15', 'M15:First Response:first response').
card_rarity('first response'/'M15', 'Uncommon').
card_artist('first response'/'M15', 'Slawomir Maniak').
card_number('first response'/'M15', '12').
card_flavor_text('first response'/'M15', '\"There\'s never a good time for a disaster or an attack. That\'s why we\'re here.\"\n—Oren, militia captain').
card_multiverse_id('first response'/'M15', '383239').

card_in_set('flesh to dust', 'M15').
card_original_type('flesh to dust'/'M15', 'Instant').
card_original_text('flesh to dust'/'M15', 'Destroy target creature. It can\'t be regenerated.').
card_first_print('flesh to dust', 'M15').
card_image_name('flesh to dust'/'M15', 'flesh to dust').
card_uid('flesh to dust'/'M15', 'M15:Flesh to Dust:flesh to dust').
card_rarity('flesh to dust'/'M15', 'Common').
card_artist('flesh to dust'/'M15', 'Julie Dillon').
card_number('flesh to dust'/'M15', '98').
card_flavor_text('flesh to dust'/'M15', '\"Pain is temporary. So is life.\"\n—Liliana Vess').
card_multiverse_id('flesh to dust'/'M15', '383240').

card_in_set('forest', 'M15').
card_original_type('forest'/'M15', 'Basic Land — Forest').
card_original_text('forest'/'M15', 'G').
card_image_name('forest'/'M15', 'forest1').
card_uid('forest'/'M15', 'M15:Forest:forest1').
card_rarity('forest'/'M15', 'Basic Land').
card_artist('forest'/'M15', 'John Avon').
card_number('forest'/'M15', '266').
card_multiverse_id('forest'/'M15', '383244').

card_in_set('forest', 'M15').
card_original_type('forest'/'M15', 'Basic Land — Forest').
card_original_text('forest'/'M15', 'G').
card_image_name('forest'/'M15', 'forest2').
card_uid('forest'/'M15', 'M15:Forest:forest2').
card_rarity('forest'/'M15', 'Basic Land').
card_artist('forest'/'M15', 'Steven Belledin').
card_number('forest'/'M15', '267').
card_multiverse_id('forest'/'M15', '383242').

card_in_set('forest', 'M15').
card_original_type('forest'/'M15', 'Basic Land — Forest').
card_original_text('forest'/'M15', 'G').
card_image_name('forest'/'M15', 'forest3').
card_uid('forest'/'M15', 'M15:Forest:forest3').
card_rarity('forest'/'M15', 'Basic Land').
card_artist('forest'/'M15', 'Noah Bradley').
card_number('forest'/'M15', '268').
card_multiverse_id('forest'/'M15', '383243').

card_in_set('forest', 'M15').
card_original_type('forest'/'M15', 'Basic Land — Forest').
card_original_text('forest'/'M15', 'G').
card_image_name('forest'/'M15', 'forest4').
card_uid('forest'/'M15', 'M15:Forest:forest4').
card_rarity('forest'/'M15', 'Basic Land').
card_artist('forest'/'M15', 'Jonas De Ro').
card_number('forest'/'M15', '269').
card_multiverse_id('forest'/'M15', '383241').

card_in_set('forge devil', 'M15').
card_original_type('forge devil'/'M15', 'Creature — Devil').
card_original_text('forge devil'/'M15', 'When Forge Devil enters the battlefield, it deals 1 damage to target creature and 1 damage to you.').
card_image_name('forge devil'/'M15', 'forge devil').
card_uid('forge devil'/'M15', 'M15:Forge Devil:forge devil').
card_rarity('forge devil'/'M15', 'Common').
card_artist('forge devil'/'M15', 'Austin Hsu').
card_number('forge devil'/'M15', '140').
card_flavor_text('forge devil'/'M15', 'A bit of pain never hurts.').
card_multiverse_id('forge devil'/'M15', '383245').

card_in_set('foundry street denizen', 'M15').
card_original_type('foundry street denizen'/'M15', 'Creature — Goblin Warrior').
card_original_text('foundry street denizen'/'M15', 'Whenever another red creature enters the battlefield under your control, Foundry Street Denizen gets +1/+0 until end of turn.').
card_image_name('foundry street denizen'/'M15', 'foundry street denizen').
card_uid('foundry street denizen'/'M15', 'M15:Foundry Street Denizen:foundry street denizen').
card_rarity('foundry street denizen'/'M15', 'Common').
card_artist('foundry street denizen'/'M15', 'Raoul Vitale').
card_number('foundry street denizen'/'M15', '141').
card_flavor_text('foundry street denizen'/'M15', 'After the Foundry Street riot, Arrester Hulbein tried to ban bludgeons. Which, inevitably, resulted in another riot.').
card_multiverse_id('foundry street denizen'/'M15', '383246').

card_in_set('frenzied goblin', 'M15').
card_original_type('frenzied goblin'/'M15', 'Creature — Goblin Berserker').
card_original_text('frenzied goblin'/'M15', 'Whenever Frenzied Goblin attacks, you may pay {R}. If you do, target creature can\'t block this turn.').
card_image_name('frenzied goblin'/'M15', 'frenzied goblin').
card_uid('frenzied goblin'/'M15', 'M15:Frenzied Goblin:frenzied goblin').
card_rarity('frenzied goblin'/'M15', 'Uncommon').
card_artist('frenzied goblin'/'M15', 'Carl Critchlow').
card_number('frenzied goblin'/'M15', '142').
card_flavor_text('frenzied goblin'/'M15', 'What he lacks in stature, he makes up for with enthusiasm.').
card_multiverse_id('frenzied goblin'/'M15', '383247').

card_in_set('frost lynx', 'M15').
card_original_type('frost lynx'/'M15', 'Creature — Elemental Cat').
card_original_text('frost lynx'/'M15', 'When Frost Lynx enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_first_print('frost lynx', 'M15').
card_image_name('frost lynx'/'M15', 'frost lynx').
card_uid('frost lynx'/'M15', 'M15:Frost Lynx:frost lynx').
card_rarity('frost lynx'/'M15', 'Common').
card_artist('frost lynx'/'M15', 'Izzy').
card_number('frost lynx'/'M15', '55').
card_flavor_text('frost lynx'/'M15', 'It readily attacks much larger prey, knowing retaliation is impossible.').
card_multiverse_id('frost lynx'/'M15', '383248').

card_in_set('fugitive wizard', 'M15').
card_original_type('fugitive wizard'/'M15', 'Creature — Human Wizard').
card_original_text('fugitive wizard'/'M15', '').
card_image_name('fugitive wizard'/'M15', 'fugitive wizard').
card_uid('fugitive wizard'/'M15', 'M15:Fugitive Wizard:fugitive wizard').
card_rarity('fugitive wizard'/'M15', 'Common').
card_artist('fugitive wizard'/'M15', 'Mark Zug').
card_number('fugitive wizard'/'M15', '56').
card_flavor_text('fugitive wizard'/'M15', '\"The law has its place—as a footnote in my spellbook.\"\n—Siyani, fugitive mage').
card_multiverse_id('fugitive wizard'/'M15', '383249').

card_in_set('furnace whelp', 'M15').
card_original_type('furnace whelp'/'M15', 'Creature — Dragon').
card_original_text('furnace whelp'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_image_name('furnace whelp'/'M15', 'furnace whelp').
card_uid('furnace whelp'/'M15', 'M15:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'M15', 'Uncommon').
card_artist('furnace whelp'/'M15', 'Matt Cavotta').
card_number('furnace whelp'/'M15', '279').
card_flavor_text('furnace whelp'/'M15', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'M15', '383164').

card_in_set('gargoyle sentinel', 'M15').
card_original_type('gargoyle sentinel'/'M15', 'Artifact Creature — Gargoyle').
card_original_text('gargoyle sentinel'/'M15', 'Defender (This creature can\'t attack.)\n{3}: Until end of turn, Gargoyle Sentinel loses defender and gains flying.').
card_image_name('gargoyle sentinel'/'M15', 'gargoyle sentinel').
card_uid('gargoyle sentinel'/'M15', 'M15:Gargoyle Sentinel:gargoyle sentinel').
card_rarity('gargoyle sentinel'/'M15', 'Uncommon').
card_artist('gargoyle sentinel'/'M15', 'Drew Baker').
card_number('gargoyle sentinel'/'M15', '216').
card_flavor_text('gargoyle sentinel'/'M15', 'The beating of a gargoyle\'s wings on the air is like the cracking of stones. Intruders who rouse a gargoyle are certain to hear the cracking of bones.').
card_multiverse_id('gargoyle sentinel'/'M15', '383250').

card_in_set('garruk\'s packleader', 'M15').
card_original_type('garruk\'s packleader'/'M15', 'Creature — Beast').
card_original_text('garruk\'s packleader'/'M15', 'Whenever another creature with power 3 or greater enters the battlefield under your control, you may draw a card.').
card_image_name('garruk\'s packleader'/'M15', 'garruk\'s packleader').
card_uid('garruk\'s packleader'/'M15', 'M15:Garruk\'s Packleader:garruk\'s packleader').
card_rarity('garruk\'s packleader'/'M15', 'Uncommon').
card_artist('garruk\'s packleader'/'M15', 'Nils Hamm').
card_number('garruk\'s packleader'/'M15', '283').
card_flavor_text('garruk\'s packleader'/'M15', '\"He has learned much in his long years. And unlike selfish humans, he\'s willing to share.\"\n—Garruk Wildspeaker').
card_multiverse_id('garruk\'s packleader'/'M15', '383165').

card_in_set('garruk, apex predator', 'M15').
card_original_type('garruk, apex predator'/'M15', 'Planeswalker — Garruk').
card_original_text('garruk, apex predator'/'M15', '+1: Destroy another target planeswalker.\n+1: Put a 3/3 black Beast creature token with deathtouch onto the battlefield.\n−3: Destroy target creature. You gain life equal to its toughness.\n−8: Target opponent gets an emblem with \"Whenever a creature attacks you, it gets +5/+5 and gains trample until end of turn.\"').
card_image_name('garruk, apex predator'/'M15', 'garruk, apex predator').
card_uid('garruk, apex predator'/'M15', 'M15:Garruk, Apex Predator:garruk, apex predator').
card_rarity('garruk, apex predator'/'M15', 'Mythic Rare').
card_artist('garruk, apex predator'/'M15', 'Tyler Jacobson').
card_number('garruk, apex predator'/'M15', '210').
card_multiverse_id('garruk, apex predator'/'M15', '383251').

card_in_set('gather courage', 'M15').
card_original_type('gather courage'/'M15', 'Instant').
card_original_text('gather courage'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTarget creature gets +2/+2 until end of turn.').
card_image_name('gather courage'/'M15', 'gather courage').
card_uid('gather courage'/'M15', 'M15:Gather Courage:gather courage').
card_rarity('gather courage'/'M15', 'Uncommon').
card_artist('gather courage'/'M15', 'Brian Despain').
card_number('gather courage'/'M15', '175').
card_multiverse_id('gather courage'/'M15', '383252').

card_in_set('geist of the moors', 'M15').
card_original_type('geist of the moors'/'M15', 'Creature — Spirit').
card_original_text('geist of the moors'/'M15', 'Flying').
card_first_print('geist of the moors', 'M15').
card_image_name('geist of the moors'/'M15', 'geist of the moors').
card_uid('geist of the moors'/'M15', 'M15:Geist of the Moors:geist of the moors').
card_rarity('geist of the moors'/'M15', 'Uncommon').
card_artist('geist of the moors'/'M15', 'Aaron Miller').
card_number('geist of the moors'/'M15', '13').
card_flavor_text('geist of the moors'/'M15', '\"The battle is won. There\'s work to be done. / The Blessed Sleep must wait. / A fiend is about. It stalks the devout. / I\'ll save them from my fate.\"\n—\"The Good Geist\'s Vow\"').
card_multiverse_id('geist of the moors'/'M15', '383253').

card_in_set('generator servant', 'M15').
card_original_type('generator servant'/'M15', 'Creature — Elemental').
card_original_text('generator servant'/'M15', '{T}, Sacrifice Generator Servant: Add {2} to your mana pool. If that mana is spent on a creature spell, it gains haste until end of turn. (That creature can attack and {T} as soon as it comes under your control.)').
card_first_print('generator servant', 'M15').
card_image_name('generator servant'/'M15', 'generator servant').
card_uid('generator servant'/'M15', 'M15:Generator Servant:generator servant').
card_rarity('generator servant'/'M15', 'Common').
card_artist('generator servant'/'M15', 'Mathias Kollros').
card_number('generator servant'/'M15', '143').
card_multiverse_id('generator servant'/'M15', '383254').

card_in_set('genesis hydra', 'M15').
card_original_type('genesis hydra'/'M15', 'Creature — Plant Hydra').
card_original_text('genesis hydra'/'M15', 'When you cast Genesis Hydra, reveal the top X cards of your library. You may put a nonland permanent card with converted mana cost X or less from among them onto the battlefield. Then shuffle the rest into your library.\nGenesis Hydra enters the battlefield with X +1/+1 counters on it.').
card_first_print('genesis hydra', 'M15').
card_image_name('genesis hydra'/'M15', 'genesis hydra').
card_uid('genesis hydra'/'M15', 'M15:Genesis Hydra:genesis hydra').
card_rarity('genesis hydra'/'M15', 'Rare').
card_artist('genesis hydra'/'M15', 'Peter Mohrbacher').
card_number('genesis hydra'/'M15', '176').
card_flavor_text('genesis hydra'/'M15', 'Designed by George Fan').
card_multiverse_id('genesis hydra'/'M15', '383255').

card_in_set('glacial crasher', 'M15').
card_original_type('glacial crasher'/'M15', 'Creature — Elemental').
card_original_text('glacial crasher'/'M15', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nGlacial Crasher can\'t attack unless there is a Mountain on the battlefield.').
card_first_print('glacial crasher', 'M15').
card_image_name('glacial crasher'/'M15', 'glacial crasher').
card_uid('glacial crasher'/'M15', 'M15:Glacial Crasher:glacial crasher').
card_rarity('glacial crasher'/'M15', 'Common').
card_artist('glacial crasher'/'M15', 'Mark Winters').
card_number('glacial crasher'/'M15', '57').
card_multiverse_id('glacial crasher'/'M15', '383256').

card_in_set('goblin kaboomist', 'M15').
card_original_type('goblin kaboomist'/'M15', 'Creature — Goblin Warrior').
card_original_text('goblin kaboomist'/'M15', 'At the beginning of your upkeep, put a colorless artifact token named Land Mine onto the battlefield with \"{R}, Sacrifice this artifact: This artifact deals 2 damage to target attacking creature without flying.\" Then flip a coin. If you lose the flip, Goblin Kaboomist deals 2 damage to itself.').
card_first_print('goblin kaboomist', 'M15').
card_image_name('goblin kaboomist'/'M15', 'goblin kaboomist').
card_uid('goblin kaboomist'/'M15', 'M15:Goblin Kaboomist:goblin kaboomist').
card_rarity('goblin kaboomist'/'M15', 'Rare').
card_artist('goblin kaboomist'/'M15', 'Kev Walker').
card_number('goblin kaboomist'/'M15', '144').
card_flavor_text('goblin kaboomist'/'M15', 'Designed by Stone Librande').
card_multiverse_id('goblin kaboomist'/'M15', '383257').

card_in_set('goblin rabblemaster', 'M15').
card_original_type('goblin rabblemaster'/'M15', 'Creature — Goblin Warrior').
card_original_text('goblin rabblemaster'/'M15', 'Other Goblin creatures you control attack each turn if able.\nAt the beginning of combat on your turn, put a 1/1 red Goblin creature token with haste onto the battlefield.\nWhenever Goblin Rabblemaster attacks, it gets +1/+0 until end of turn for each other attacking Goblin.').
card_image_name('goblin rabblemaster'/'M15', 'goblin rabblemaster').
card_uid('goblin rabblemaster'/'M15', 'M15:Goblin Rabblemaster:goblin rabblemaster').
card_rarity('goblin rabblemaster'/'M15', 'Rare').
card_artist('goblin rabblemaster'/'M15', 'Svetlin Velinov').
card_number('goblin rabblemaster'/'M15', '145').
card_multiverse_id('goblin rabblemaster'/'M15', '383258').

card_in_set('goblin roughrider', 'M15').
card_original_type('goblin roughrider'/'M15', 'Creature — Goblin Knight').
card_original_text('goblin roughrider'/'M15', '').
card_image_name('goblin roughrider'/'M15', 'goblin roughrider').
card_uid('goblin roughrider'/'M15', 'M15:Goblin Roughrider:goblin roughrider').
card_rarity('goblin roughrider'/'M15', 'Common').
card_artist('goblin roughrider'/'M15', 'Jesper Ejsing').
card_number('goblin roughrider'/'M15', '146').
card_flavor_text('goblin roughrider'/'M15', 'Astride the bucking creature, Gribble hurtled down the mountainside while his Grotag brethren cheered. It was at that moment that the legend of the Skrill Tamer was born.').
card_multiverse_id('goblin roughrider'/'M15', '383259').

card_in_set('gravedigger', 'M15').
card_original_type('gravedigger'/'M15', 'Creature — Zombie').
card_original_text('gravedigger'/'M15', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'M15', 'gravedigger').
card_uid('gravedigger'/'M15', 'M15:Gravedigger:gravedigger').
card_rarity('gravedigger'/'M15', 'Uncommon').
card_artist('gravedigger'/'M15', 'Dermot Power').
card_number('gravedigger'/'M15', '99').
card_flavor_text('gravedigger'/'M15', 'A grave is not always for burial.').
card_multiverse_id('gravedigger'/'M15', '383260').

card_in_set('grindclock', 'M15').
card_original_type('grindclock'/'M15', 'Artifact').
card_original_text('grindclock'/'M15', '{T}: Put a charge counter on Grindclock.\n{T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of charge counters on Grindclock.').
card_image_name('grindclock'/'M15', 'grindclock').
card_uid('grindclock'/'M15', 'M15:Grindclock:grindclock').
card_rarity('grindclock'/'M15', 'Rare').
card_artist('grindclock'/'M15', 'Nils Hamm').
card_number('grindclock'/'M15', '217').
card_flavor_text('grindclock'/'M15', 'Pray you never hear it chime.').
card_multiverse_id('grindclock'/'M15', '383261').

card_in_set('hammerhand', 'M15').
card_original_type('hammerhand'/'M15', 'Enchantment — Aura').
card_original_text('hammerhand'/'M15', 'Enchant creature\nWhen Hammerhand enters the battlefield, target creature can\'t block this turn.\nEnchanted creature gets +1/+1 and has haste. (It can attack and {T} no matter when it came under your control.)').
card_first_print('hammerhand', 'M15').
card_image_name('hammerhand'/'M15', 'hammerhand').
card_uid('hammerhand'/'M15', 'M15:Hammerhand:hammerhand').
card_rarity('hammerhand'/'M15', 'Common').
card_artist('hammerhand'/'M15', 'Tomasz Jedruszek').
card_number('hammerhand'/'M15', '147').
card_multiverse_id('hammerhand'/'M15', '383262').

card_in_set('haunted plate mail', 'M15').
card_original_type('haunted plate mail'/'M15', 'Artifact — Equipment').
card_original_text('haunted plate mail'/'M15', 'Equipped creature gets +4/+4.\n{0}: Until end of turn, Haunted Plate Mail becomes a 4/4 Spirit artifact creature that\'s no longer an Equipment. Activate this ability only if you control no creatures.\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('haunted plate mail'/'M15', 'haunted plate mail').
card_uid('haunted plate mail'/'M15', 'M15:Haunted Plate Mail:haunted plate mail').
card_rarity('haunted plate mail'/'M15', 'Rare').
card_artist('haunted plate mail'/'M15', 'Izzy').
card_number('haunted plate mail'/'M15', '218').
card_multiverse_id('haunted plate mail'/'M15', '383263').

card_in_set('heat ray', 'M15').
card_original_type('heat ray'/'M15', 'Instant').
card_original_text('heat ray'/'M15', 'Heat Ray deals X damage to target creature.').
card_image_name('heat ray'/'M15', 'heat ray').
card_uid('heat ray'/'M15', 'M15:Heat Ray:heat ray').
card_rarity('heat ray'/'M15', 'Uncommon').
card_artist('heat ray'/'M15', 'Austin Hsu').
card_number('heat ray'/'M15', '148').
card_flavor_text('heat ray'/'M15', '\"There was clearly a scream. I\'m not sure if there was a mouth.\"\n—Sachir, Akoum Expeditionary House').
card_multiverse_id('heat ray'/'M15', '383264').

card_in_set('heliod\'s pilgrim', 'M15').
card_original_type('heliod\'s pilgrim'/'M15', 'Creature — Human Cleric').
card_original_text('heliod\'s pilgrim'/'M15', 'When Heliod\'s Pilgrim enters the battlefield, you may search your library for an Aura card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('heliod\'s pilgrim', 'M15').
card_image_name('heliod\'s pilgrim'/'M15', 'heliod\'s pilgrim').
card_uid('heliod\'s pilgrim'/'M15', 'M15:Heliod\'s Pilgrim:heliod\'s pilgrim').
card_rarity('heliod\'s pilgrim'/'M15', 'Common').
card_artist('heliod\'s pilgrim'/'M15', 'Izzy').
card_number('heliod\'s pilgrim'/'M15', '14').
card_flavor_text('heliod\'s pilgrim'/'M15', 'The blessings of Heliod are apparent for all to see.').
card_multiverse_id('heliod\'s pilgrim'/'M15', '383265').

card_in_set('hoarding dragon', 'M15').
card_original_type('hoarding dragon'/'M15', 'Creature — Dragon').
card_original_text('hoarding dragon'/'M15', 'Flying\nWhen Hoarding Dragon enters the battlefield, you may search your library for an artifact card, exile it, then shuffle your library.\nWhen Hoarding Dragon dies, you may put the exiled card into its owner\'s hand.').
card_image_name('hoarding dragon'/'M15', 'hoarding dragon').
card_uid('hoarding dragon'/'M15', 'M15:Hoarding Dragon:hoarding dragon').
card_rarity('hoarding dragon'/'M15', 'Rare').
card_artist('hoarding dragon'/'M15', 'Matt Cavotta').
card_number('hoarding dragon'/'M15', '149').
card_multiverse_id('hoarding dragon'/'M15', '383266').

card_in_set('hornet nest', 'M15').
card_original_type('hornet nest'/'M15', 'Creature — Insect').
card_original_text('hornet nest'/'M15', 'Defender (This creature can\'t attack.)\nWhenever Hornet Nest is dealt damage, put that many 1/1 green Insect creature tokens with flying and deathtouch onto the battlefield. (Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_first_print('hornet nest', 'M15').
card_image_name('hornet nest'/'M15', 'hornet nest').
card_uid('hornet nest'/'M15', 'M15:Hornet Nest:hornet nest').
card_rarity('hornet nest'/'M15', 'Rare').
card_artist('hornet nest'/'M15', 'Adam Paquette').
card_number('hornet nest'/'M15', '177').
card_multiverse_id('hornet nest'/'M15', '383267').

card_in_set('hornet queen', 'M15').
card_original_type('hornet queen'/'M15', 'Creature — Insect').
card_original_text('hornet queen'/'M15', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Hornet Queen enters the battlefield, put four 1/1 green Insect creature tokens with flying and deathtouch onto the battlefield.').
card_image_name('hornet queen'/'M15', 'hornet queen').
card_uid('hornet queen'/'M15', 'M15:Hornet Queen:hornet queen').
card_rarity('hornet queen'/'M15', 'Rare').
card_artist('hornet queen'/'M15', 'Martina Pilcerova').
card_number('hornet queen'/'M15', '178').
card_multiverse_id('hornet queen'/'M15', '383268').

card_in_set('hot soup', 'M15').
card_original_type('hot soup'/'M15', 'Artifact — Equipment').
card_original_text('hot soup'/'M15', 'Equipped creature can\'t be blocked.\nWhenever equipped creature is dealt damage, destroy it.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('hot soup', 'M15').
card_image_name('hot soup'/'M15', 'hot soup').
card_uid('hot soup'/'M15', 'M15:Hot Soup:hot soup').
card_rarity('hot soup'/'M15', 'Uncommon').
card_artist('hot soup'/'M15', 'David Palumbo').
card_number('hot soup'/'M15', '219').
card_flavor_text('hot soup'/'M15', '\"Comin\' through!\"\nDesigned by James Ernest').
card_multiverse_id('hot soup'/'M15', '383269').

card_in_set('hunt the weak', 'M15').
card_original_type('hunt the weak'/'M15', 'Sorcery').
card_original_text('hunt the weak'/'M15', 'Put a +1/+1 counter on target creature you control. Then that creature fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_image_name('hunt the weak'/'M15', 'hunt the weak').
card_uid('hunt the weak'/'M15', 'M15:Hunt the Weak:hunt the weak').
card_rarity('hunt the weak'/'M15', 'Common').
card_artist('hunt the weak'/'M15', 'Raoul Vitale').
card_number('hunt the weak'/'M15', '179').
card_flavor_text('hunt the weak'/'M15', 'He who hesitates is lunch.').
card_multiverse_id('hunt the weak'/'M15', '383270').

card_in_set('hunter\'s ambush', 'M15').
card_original_type('hunter\'s ambush'/'M15', 'Instant').
card_original_text('hunter\'s ambush'/'M15', 'Prevent all combat damage that would be dealt by nongreen creatures this turn.').
card_first_print('hunter\'s ambush', 'M15').
card_image_name('hunter\'s ambush'/'M15', 'hunter\'s ambush').
card_uid('hunter\'s ambush'/'M15', 'M15:Hunter\'s Ambush:hunter\'s ambush').
card_rarity('hunter\'s ambush'/'M15', 'Common').
card_artist('hunter\'s ambush'/'M15', 'David Palumbo').
card_number('hunter\'s ambush'/'M15', '180').
card_flavor_text('hunter\'s ambush'/'M15', 'First you lose your enemy\'s trail. Then you lose all sense of direction. Then you hear the growls . . .').
card_multiverse_id('hunter\'s ambush'/'M15', '383271').

card_in_set('hushwing gryff', 'M15').
card_original_type('hushwing gryff'/'M15', 'Creature — Hippogriff').
card_original_text('hushwing gryff'/'M15', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nCreatures entering the battlefield don\'t cause abilities to trigger.').
card_first_print('hushwing gryff', 'M15').
card_image_name('hushwing gryff'/'M15', 'hushwing gryff').
card_uid('hushwing gryff'/'M15', 'M15:Hushwing Gryff:hushwing gryff').
card_rarity('hushwing gryff'/'M15', 'Rare').
card_artist('hushwing gryff'/'M15', 'John Severin Brassell').
card_number('hushwing gryff'/'M15', '15').
card_flavor_text('hushwing gryff'/'M15', 'An overwhelming sense of calm accompanies the gryffs that wheel above the roofs of Gavony.').
card_multiverse_id('hushwing gryff'/'M15', '383272').

card_in_set('hydrosurge', 'M15').
card_original_type('hydrosurge'/'M15', 'Instant').
card_original_text('hydrosurge'/'M15', 'Target creature gets -5/-0 until end of turn.').
card_image_name('hydrosurge'/'M15', 'hydrosurge').
card_uid('hydrosurge'/'M15', 'M15:Hydrosurge:hydrosurge').
card_rarity('hydrosurge'/'M15', 'Common').
card_artist('hydrosurge'/'M15', 'Steve Prescott').
card_number('hydrosurge'/'M15', '58').
card_flavor_text('hydrosurge'/'M15', '\"Thirsty?\"\n—Drunvalus, hydromancer').
card_multiverse_id('hydrosurge'/'M15', '383273').

card_in_set('illusory angel', 'M15').
card_original_type('illusory angel'/'M15', 'Creature — Angel Illusion').
card_original_text('illusory angel'/'M15', 'Flying\nCast Illusory Angel only if you\'ve cast another spell this turn.').
card_image_name('illusory angel'/'M15', 'illusory angel').
card_uid('illusory angel'/'M15', 'M15:Illusory Angel:illusory angel').
card_rarity('illusory angel'/'M15', 'Uncommon').
card_artist('illusory angel'/'M15', 'Allen Williams').
card_number('illusory angel'/'M15', '59').
card_flavor_text('illusory angel'/'M15', '\"In a daze, I woke and looked upon the battlefield, where I could swear my dreams were laying waste to the enemy.\"\n—Letter from a soldier').
card_multiverse_id('illusory angel'/'M15', '383274').

card_in_set('in garruk\'s wake', 'M15').
card_original_type('in garruk\'s wake'/'M15', 'Sorcery').
card_original_text('in garruk\'s wake'/'M15', 'Destroy all creatures you don\'t control and all planeswalkers you don\'t control.').
card_image_name('in garruk\'s wake'/'M15', 'in garruk\'s wake').
card_uid('in garruk\'s wake'/'M15', 'M15:In Garruk\'s Wake:in garruk\'s wake').
card_rarity('in garruk\'s wake'/'M15', 'Rare').
card_artist('in garruk\'s wake'/'M15', 'Chase Stone').
card_number('in garruk\'s wake'/'M15', '100').
card_flavor_text('in garruk\'s wake'/'M15', 'Beyond pain, beyond obsession and wild despair, there lies a place of twisted power only the most tormented souls can reach.').
card_multiverse_id('in garruk\'s wake'/'M15', '383275').

card_in_set('indulgent tormentor', 'M15').
card_original_type('indulgent tormentor'/'M15', 'Creature — Demon').
card_original_text('indulgent tormentor'/'M15', 'Flying\nAt the beginning of your upkeep, draw a card unless target opponent sacrifices a creature or pays 3 life.').
card_image_name('indulgent tormentor'/'M15', 'indulgent tormentor').
card_uid('indulgent tormentor'/'M15', 'M15:Indulgent Tormentor:indulgent tormentor').
card_rarity('indulgent tormentor'/'M15', 'Rare').
card_artist('indulgent tormentor'/'M15', 'Wesley Burt').
card_number('indulgent tormentor'/'M15', '101').
card_flavor_text('indulgent tormentor'/'M15', 'The promise of anguish is payment enough for services rendered.').
card_multiverse_id('indulgent tormentor'/'M15', '383276').

card_in_set('inferno fist', 'M15').
card_original_type('inferno fist'/'M15', 'Enchantment — Aura').
card_original_text('inferno fist'/'M15', 'Enchant creature you control\nEnchanted creature gets +2/+0.\n{R}, Sacrifice Inferno Fist: Inferno Fist deals 2 damage to target creature or player.').
card_first_print('inferno fist', 'M15').
card_image_name('inferno fist'/'M15', 'inferno fist').
card_uid('inferno fist'/'M15', 'M15:Inferno Fist:inferno fist').
card_rarity('inferno fist'/'M15', 'Common').
card_artist('inferno fist'/'M15', 'James Ryman').
card_number('inferno fist'/'M15', '150').
card_flavor_text('inferno fist'/'M15', '\"I\'ve never been above throwing the first punch.\"').
card_multiverse_id('inferno fist'/'M15', '383277').

card_in_set('inspired charge', 'M15').
card_original_type('inspired charge'/'M15', 'Instant').
card_original_text('inspired charge'/'M15', 'Creatures you control get +2/+1 until end of turn.').
card_image_name('inspired charge'/'M15', 'inspired charge').
card_uid('inspired charge'/'M15', 'M15:Inspired Charge:inspired charge').
card_rarity('inspired charge'/'M15', 'Common').
card_artist('inspired charge'/'M15', 'Wayne Reynolds').
card_number('inspired charge'/'M15', '272').
card_flavor_text('inspired charge'/'M15', '\"Impossible! How could they overwhelm us? We had barricades, war elephants, . . . and they were barely a tenth of our number!\"\n—General Avitora').
card_multiverse_id('inspired charge'/'M15', '383166').

card_in_set('into the void', 'M15').
card_original_type('into the void'/'M15', 'Sorcery').
card_original_text('into the void'/'M15', 'Return up to two target creatures to their owners\' hands.').
card_image_name('into the void'/'M15', 'into the void').
card_uid('into the void'/'M15', 'M15:Into the Void:into the void').
card_rarity('into the void'/'M15', 'Uncommon').
card_artist('into the void'/'M15', 'Daarken').
card_number('into the void'/'M15', '60').
card_flavor_text('into the void'/'M15', '\"The cathars have their swords, the inquisitors their axes. I prefer the ‘diplomatic\' approach.\"\n—Terhold, archmage of Drunau').
card_multiverse_id('into the void'/'M15', '383278').

card_in_set('invasive species', 'M15').
card_original_type('invasive species'/'M15', 'Creature — Insect').
card_original_text('invasive species'/'M15', 'When Invasive Species enters the battlefield, return another permanent you control to its owner\'s hand.').
card_first_print('invasive species', 'M15').
card_image_name('invasive species'/'M15', 'invasive species').
card_uid('invasive species'/'M15', 'M15:Invasive Species:invasive species').
card_rarity('invasive species'/'M15', 'Common').
card_artist('invasive species'/'M15', 'Christopher Moeller').
card_number('invasive species'/'M15', '181').
card_flavor_text('invasive species'/'M15', 'It\'s easier to relocate a village that lies in their path than to turn the bugs aside.').
card_multiverse_id('invasive species'/'M15', '383279').

card_in_set('invisibility', 'M15').
card_original_type('invisibility'/'M15', 'Enchantment — Aura').
card_original_text('invisibility'/'M15', 'Enchant creature\nEnchanted creature can\'t be blocked except by Walls.').
card_image_name('invisibility'/'M15', 'invisibility').
card_uid('invisibility'/'M15', 'M15:Invisibility:invisibility').
card_rarity('invisibility'/'M15', 'Common').
card_artist('invisibility'/'M15', 'Pete Venters').
card_number('invisibility'/'M15', '61').
card_flavor_text('invisibility'/'M15', 'Verick held his breath. Breathing wouldn\'t reveal his position, but it would force him to smell the goblins.').
card_multiverse_id('invisibility'/'M15', '383280').

card_in_set('island', 'M15').
card_original_type('island'/'M15', 'Basic Land — Island').
card_original_text('island'/'M15', 'U').
card_image_name('island'/'M15', 'island1').
card_uid('island'/'M15', 'M15:Island:island1').
card_rarity('island'/'M15', 'Basic Land').
card_artist('island'/'M15', 'Cliff Childs').
card_number('island'/'M15', '254').
card_multiverse_id('island'/'M15', '383284').

card_in_set('island', 'M15').
card_original_type('island'/'M15', 'Basic Land — Island').
card_original_text('island'/'M15', 'U').
card_image_name('island'/'M15', 'island2').
card_uid('island'/'M15', 'M15:Island:island2').
card_rarity('island'/'M15', 'Basic Land').
card_artist('island'/'M15', 'Florian de Gesincourt').
card_number('island'/'M15', '255').
card_multiverse_id('island'/'M15', '383281').

card_in_set('island', 'M15').
card_original_type('island'/'M15', 'Basic Land — Island').
card_original_text('island'/'M15', 'U').
card_image_name('island'/'M15', 'island3').
card_uid('island'/'M15', 'M15:Island:island3').
card_rarity('island'/'M15', 'Basic Land').
card_artist('island'/'M15', 'Peter Mohrbacher').
card_number('island'/'M15', '256').
card_multiverse_id('island'/'M15', '383283').

card_in_set('island', 'M15').
card_original_type('island'/'M15', 'Basic Land — Island').
card_original_text('island'/'M15', 'U').
card_image_name('island'/'M15', 'island4').
card_uid('island'/'M15', 'M15:Island:island4').
card_rarity('island'/'M15', 'Basic Land').
card_artist('island'/'M15', 'Andreas Rocha').
card_number('island'/'M15', '257').
card_multiverse_id('island'/'M15', '383282').

card_in_set('jace\'s ingenuity', 'M15').
card_original_type('jace\'s ingenuity'/'M15', 'Instant').
card_original_text('jace\'s ingenuity'/'M15', 'Draw three cards.').
card_image_name('jace\'s ingenuity'/'M15', 'jace\'s ingenuity').
card_uid('jace\'s ingenuity'/'M15', 'M15:Jace\'s Ingenuity:jace\'s ingenuity').
card_rarity('jace\'s ingenuity'/'M15', 'Uncommon').
card_artist('jace\'s ingenuity'/'M15', 'Igor Kieryluk').
card_number('jace\'s ingenuity'/'M15', '63').
card_flavor_text('jace\'s ingenuity'/'M15', '\"Brute force can sometimes kick down a locked door, but knowledge is a skeleton key.\"').
card_multiverse_id('jace\'s ingenuity'/'M15', '383286').

card_in_set('jace, the living guildpact', 'M15').
card_original_type('jace, the living guildpact'/'M15', 'Planeswalker — Jace').
card_original_text('jace, the living guildpact'/'M15', '+1: Look at the top two cards of your library. Put one of them into your graveyard.\n−3: Return another target nonland permanent to its owner\'s hand.\n−8: Each player shuffles his or her hand and graveyard into his or her library. You draw seven cards.').
card_image_name('jace, the living guildpact'/'M15', 'jace, the living guildpact').
card_uid('jace, the living guildpact'/'M15', 'M15:Jace, the Living Guildpact:jace, the living guildpact').
card_rarity('jace, the living guildpact'/'M15', 'Mythic Rare').
card_artist('jace, the living guildpact'/'M15', 'Chase Stone').
card_number('jace, the living guildpact'/'M15', '62').
card_multiverse_id('jace, the living guildpact'/'M15', '383285').

card_in_set('jalira, master polymorphist', 'M15').
card_original_type('jalira, master polymorphist'/'M15', 'Legendary Creature — Human Wizard').
card_original_text('jalira, master polymorphist'/'M15', '{2}{U}, {T}, Sacrifice another creature: Reveal cards from the top of your library until you reveal a nonlegendary creature card. Put that card onto the battlefield and the rest on the bottom of your library in a random order.').
card_first_print('jalira, master polymorphist', 'M15').
card_image_name('jalira, master polymorphist'/'M15', 'jalira, master polymorphist').
card_uid('jalira, master polymorphist'/'M15', 'M15:Jalira, Master Polymorphist:jalira, master polymorphist').
card_rarity('jalira, master polymorphist'/'M15', 'Rare').
card_artist('jalira, master polymorphist'/'M15', 'Steve Prescott').
card_number('jalira, master polymorphist'/'M15', '64').
card_flavor_text('jalira, master polymorphist'/'M15', '\"You can become anything if I just put my mind to it.\"').
card_multiverse_id('jalira, master polymorphist'/'M15', '383287').

card_in_set('jorubai murk lurker', 'M15').
card_original_type('jorubai murk lurker'/'M15', 'Creature — Leech').
card_original_text('jorubai murk lurker'/'M15', 'Jorubai Murk Lurker gets +1/+1 as long as you control a Swamp.\n{1}{B}: Target creature gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_first_print('jorubai murk lurker', 'M15').
card_image_name('jorubai murk lurker'/'M15', 'jorubai murk lurker').
card_uid('jorubai murk lurker'/'M15', 'M15:Jorubai Murk Lurker:jorubai murk lurker').
card_rarity('jorubai murk lurker'/'M15', 'Uncommon').
card_artist('jorubai murk lurker'/'M15', 'Clint Cearley').
card_number('jorubai murk lurker'/'M15', '65').
card_multiverse_id('jorubai murk lurker'/'M15', '383288').

card_in_set('juggernaut', 'M15').
card_original_type('juggernaut'/'M15', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'M15', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_image_name('juggernaut'/'M15', 'juggernaut').
card_uid('juggernaut'/'M15', 'M15:Juggernaut:juggernaut').
card_rarity('juggernaut'/'M15', 'Uncommon').
card_artist('juggernaut'/'M15', 'Kev Walker').
card_number('juggernaut'/'M15', '220').
card_flavor_text('juggernaut'/'M15', 'Many a besieged city has surrendered upon hearing the distinctive rumble of the juggernaut.').
card_multiverse_id('juggernaut'/'M15', '383289').

card_in_set('kalonian twingrove', 'M15').
card_original_type('kalonian twingrove'/'M15', 'Creature — Treefolk Warrior').
card_original_text('kalonian twingrove'/'M15', 'Kalonian Twingrove\'s power and toughness are each equal to the number of Forests you control.\nWhen Kalonian Twingrove enters the battlefield, put a green Treefolk Warrior creature token onto the battlefield with \"This creature\'s power and toughness are each equal to the number of Forests you control.\"').
card_first_print('kalonian twingrove', 'M15').
card_image_name('kalonian twingrove'/'M15', 'kalonian twingrove').
card_uid('kalonian twingrove'/'M15', 'M15:Kalonian Twingrove:kalonian twingrove').
card_rarity('kalonian twingrove'/'M15', 'Rare').
card_artist('kalonian twingrove'/'M15', 'Todd Lockwood').
card_number('kalonian twingrove'/'M15', '182').
card_multiverse_id('kalonian twingrove'/'M15', '383290').

card_in_set('kapsho kitefins', 'M15').
card_original_type('kapsho kitefins'/'M15', 'Creature — Fish').
card_original_text('kapsho kitefins'/'M15', 'Flying\nWhenever Kapsho Kitefins or another creature enters the battlefield under your control, tap target creature an opponent controls.').
card_first_print('kapsho kitefins', 'M15').
card_image_name('kapsho kitefins'/'M15', 'kapsho kitefins').
card_uid('kapsho kitefins'/'M15', 'M15:Kapsho Kitefins:kapsho kitefins').
card_rarity('kapsho kitefins'/'M15', 'Uncommon').
card_artist('kapsho kitefins'/'M15', 'Ryan Yee').
card_number('kapsho kitefins'/'M15', '66').
card_flavor_text('kapsho kitefins'/'M15', '\"It\'s a truly disconcerting sight to see their shadows cast upon the deck.\"\n—Captain Triff').
card_multiverse_id('kapsho kitefins'/'M15', '383291').

card_in_set('kinsbaile skirmisher', 'M15').
card_original_type('kinsbaile skirmisher'/'M15', 'Creature — Kithkin Soldier').
card_original_text('kinsbaile skirmisher'/'M15', 'When Kinsbaile Skirmisher enters the battlefield, target creature gets +1/+1 until end of turn.').
card_image_name('kinsbaile skirmisher'/'M15', 'kinsbaile skirmisher').
card_uid('kinsbaile skirmisher'/'M15', 'M15:Kinsbaile Skirmisher:kinsbaile skirmisher').
card_rarity('kinsbaile skirmisher'/'M15', 'Common').
card_artist('kinsbaile skirmisher'/'M15', 'Thomas Denmark').
card_number('kinsbaile skirmisher'/'M15', '16').
card_flavor_text('kinsbaile skirmisher'/'M15', '\"If a boggart even dares breathe near one of my kin, I\'ll know. And I\'ll not be happy.\"').
card_multiverse_id('kinsbaile skirmisher'/'M15', '383292').

card_in_set('kird chieftain', 'M15').
card_original_type('kird chieftain'/'M15', 'Creature — Ape').
card_original_text('kird chieftain'/'M15', 'Kird Chieftain gets +1/+1 as long as you control a Forest.\n{4}{G}: Target creature gets +2/+2 and gains trample until end of turn. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('kird chieftain', 'M15').
card_image_name('kird chieftain'/'M15', 'kird chieftain').
card_uid('kird chieftain'/'M15', 'M15:Kird Chieftain:kird chieftain').
card_rarity('kird chieftain'/'M15', 'Uncommon').
card_artist('kird chieftain'/'M15', 'Lars Grant-West').
card_number('kird chieftain'/'M15', '151').
card_multiverse_id('kird chieftain'/'M15', '383293').

card_in_set('krenko\'s enforcer', 'M15').
card_original_type('krenko\'s enforcer'/'M15', 'Creature — Goblin Warrior').
card_original_text('krenko\'s enforcer'/'M15', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('krenko\'s enforcer', 'M15').
card_image_name('krenko\'s enforcer'/'M15', 'krenko\'s enforcer').
card_uid('krenko\'s enforcer'/'M15', 'M15:Krenko\'s Enforcer:krenko\'s enforcer').
card_rarity('krenko\'s enforcer'/'M15', 'Common').
card_artist('krenko\'s enforcer'/'M15', 'Karl Kopinski').
card_number('krenko\'s enforcer'/'M15', '152').
card_flavor_text('krenko\'s enforcer'/'M15', 'He just likes to break legs. Collecting the debt is a bonus.').
card_multiverse_id('krenko\'s enforcer'/'M15', '383294').

card_in_set('kurkesh, onakke ancient', 'M15').
card_original_type('kurkesh, onakke ancient'/'M15', 'Legendary Creature — Ogre Spirit').
card_original_text('kurkesh, onakke ancient'/'M15', 'Whenever you activate an ability of an artifact, if it isn\'t a mana ability, you may pay {R}. If you do, copy that ability. You may choose new targets for the copy.').
card_first_print('kurkesh, onakke ancient', 'M15').
card_image_name('kurkesh, onakke ancient'/'M15', 'kurkesh, onakke ancient').
card_uid('kurkesh, onakke ancient'/'M15', 'M15:Kurkesh, Onakke Ancient:kurkesh, onakke ancient').
card_rarity('kurkesh, onakke ancient'/'M15', 'Rare').
card_artist('kurkesh, onakke ancient'/'M15', 'Slawomir Maniak').
card_number('kurkesh, onakke ancient'/'M15', '153').
card_flavor_text('kurkesh, onakke ancient'/'M15', 'The Onakke were masters of dark arts and artifice. Their creations continue to spread foul magic, paving the way for their return.').
card_multiverse_id('kurkesh, onakke ancient'/'M15', '383295').

card_in_set('lava axe', 'M15').
card_original_type('lava axe'/'M15', 'Sorcery').
card_original_text('lava axe'/'M15', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'M15', 'lava axe').
card_uid('lava axe'/'M15', 'M15:Lava Axe:lava axe').
card_rarity('lava axe'/'M15', 'Common').
card_artist('lava axe'/'M15', 'Brian Snõddy').
card_number('lava axe'/'M15', '154').
card_flavor_text('lava axe'/'M15', 'A strict upgrade over the cinder hatchet.').
card_multiverse_id('lava axe'/'M15', '383296').

card_in_set('leeching sliver', 'M15').
card_original_type('leeching sliver'/'M15', 'Creature — Sliver').
card_original_text('leeching sliver'/'M15', 'Whenever a Sliver you control attacks, defending player loses 1 life.').
card_first_print('leeching sliver', 'M15').
card_image_name('leeching sliver'/'M15', 'leeching sliver').
card_uid('leeching sliver'/'M15', 'M15:Leeching Sliver:leeching sliver').
card_rarity('leeching sliver'/'M15', 'Uncommon').
card_artist('leeching sliver'/'M15', 'Svetlin Velinov').
card_number('leeching sliver'/'M15', '102').
card_flavor_text('leeching sliver'/'M15', '\"Seeing one slowly devour one of my party begged the question—do they know what cruelty is?\"\n—Hastric, Thunian scout').
card_multiverse_id('leeching sliver'/'M15', '383297').

card_in_set('life\'s legacy', 'M15').
card_original_type('life\'s legacy'/'M15', 'Sorcery').
card_original_text('life\'s legacy'/'M15', 'As an additional cost to cast Life\'s Legacy, sacrifice a creature.\nDraw cards equal to the sacrificed creature\'s power.').
card_first_print('life\'s legacy', 'M15').
card_image_name('life\'s legacy'/'M15', 'life\'s legacy').
card_uid('life\'s legacy'/'M15', 'M15:Life\'s Legacy:life\'s legacy').
card_rarity('life\'s legacy'/'M15', 'Rare').
card_artist('life\'s legacy'/'M15', 'Howard Lyon').
card_number('life\'s legacy'/'M15', '183').
card_flavor_text('life\'s legacy'/'M15', 'At the instant of death, the mystery of life.').
card_multiverse_id('life\'s legacy'/'M15', '383298').

card_in_set('lightning strike', 'M15').
card_original_type('lightning strike'/'M15', 'Instant').
card_original_text('lightning strike'/'M15', 'Lightning Strike deals 3 damage to target creature or player.').
card_image_name('lightning strike'/'M15', 'lightning strike').
card_uid('lightning strike'/'M15', 'M15:Lightning Strike:lightning strike').
card_rarity('lightning strike'/'M15', 'Common').
card_artist('lightning strike'/'M15', 'Adam Paquette').
card_number('lightning strike'/'M15', '155').
card_flavor_text('lightning strike'/'M15', 'To wield lightning is to tame chaos.').
card_multiverse_id('lightning strike'/'M15', '383299').

card_in_set('liliana vess', 'M15').
card_original_type('liliana vess'/'M15', 'Planeswalker — Liliana').
card_original_text('liliana vess'/'M15', '+1: Target player discards a card.\n−2: Search your library for a card, then shuffle your library and put that card on top of it.\n−8: Put all creature cards from all graveyards onto the battlefield under your control.').
card_image_name('liliana vess'/'M15', 'liliana vess').
card_uid('liliana vess'/'M15', 'M15:Liliana Vess:liliana vess').
card_rarity('liliana vess'/'M15', 'Mythic Rare').
card_artist('liliana vess'/'M15', 'Aleksi Briclot').
card_number('liliana vess'/'M15', '103').
card_multiverse_id('liliana vess'/'M15', '383300').

card_in_set('living totem', 'M15').
card_original_type('living totem'/'M15', 'Creature — Plant Elemental').
card_original_text('living totem'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nWhen Living Totem enters the battlefield, you may put a +1/+1 counter on another target creature.').
card_first_print('living totem', 'M15').
card_image_name('living totem'/'M15', 'living totem').
card_uid('living totem'/'M15', 'M15:Living Totem:living totem').
card_rarity('living totem'/'M15', 'Common').
card_artist('living totem'/'M15', 'Svetlin Velinov').
card_number('living totem'/'M15', '184').
card_multiverse_id('living totem'/'M15', '383301').

card_in_set('llanowar wastes', 'M15').
card_original_type('llanowar wastes'/'M15', 'Land').
card_original_text('llanowar wastes'/'M15', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Llanowar Wastes deals 1 damage to you.').
card_image_name('llanowar wastes'/'M15', 'llanowar wastes').
card_uid('llanowar wastes'/'M15', 'M15:Llanowar Wastes:llanowar wastes').
card_rarity('llanowar wastes'/'M15', 'Rare').
card_artist('llanowar wastes'/'M15', 'Rob Alexander').
card_number('llanowar wastes'/'M15', '244').
card_multiverse_id('llanowar wastes'/'M15', '383302').

card_in_set('mahamoti djinn', 'M15').
card_original_type('mahamoti djinn'/'M15', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('mahamoti djinn'/'M15', 'mahamoti djinn').
card_uid('mahamoti djinn'/'M15', 'M15:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'M15', 'Rare').
card_artist('mahamoti djinn'/'M15', 'Greg Staples').
card_number('mahamoti djinn'/'M15', '275').
card_flavor_text('mahamoti djinn'/'M15', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'M15', '383167').

card_in_set('marked by honor', 'M15').
card_original_type('marked by honor'/'M15', 'Enchantment — Aura').
card_original_text('marked by honor'/'M15', 'Enchant creature\nEnchanted creature gets +2/+2 and has vigilance. (Attacking doesn\'t cause it to tap.)').
card_first_print('marked by honor', 'M15').
card_image_name('marked by honor'/'M15', 'marked by honor').
card_uid('marked by honor'/'M15', 'M15:Marked by Honor:marked by honor').
card_rarity('marked by honor'/'M15', 'Common').
card_artist('marked by honor'/'M15', 'David Palumbo').
card_number('marked by honor'/'M15', '17').
card_flavor_text('marked by honor'/'M15', 'Stand your post for duty. Stand your ground for honor.').
card_multiverse_id('marked by honor'/'M15', '383303').

card_in_set('mass calcify', 'M15').
card_original_type('mass calcify'/'M15', 'Sorcery').
card_original_text('mass calcify'/'M15', 'Destroy all nonwhite creatures.').
card_image_name('mass calcify'/'M15', 'mass calcify').
card_uid('mass calcify'/'M15', 'M15:Mass Calcify:mass calcify').
card_rarity('mass calcify'/'M15', 'Rare').
card_artist('mass calcify'/'M15', 'Brandon Kitkouski').
card_number('mass calcify'/'M15', '18').
card_flavor_text('mass calcify'/'M15', 'The dead serve as their own tombstones.').
card_multiverse_id('mass calcify'/'M15', '383304').

card_in_set('master of predicaments', 'M15').
card_original_type('master of predicaments'/'M15', 'Creature — Sphinx').
card_original_text('master of predicaments'/'M15', 'Flying\nWhenever Master of Predicaments deals combat damage to a player, choose a card in your hand. That player guesses whether the card\'s converted mana cost is greater than 4. If the player guessed wrong, you may cast the card without paying its mana cost.').
card_first_print('master of predicaments', 'M15').
card_image_name('master of predicaments'/'M15', 'master of predicaments').
card_uid('master of predicaments'/'M15', 'M15:Master of Predicaments:master of predicaments').
card_rarity('master of predicaments'/'M15', 'Rare').
card_artist('master of predicaments'/'M15', 'Matt Stewart').
card_number('master of predicaments'/'M15', '67').
card_flavor_text('master of predicaments'/'M15', 'Designed by David Sirlin').
card_multiverse_id('master of predicaments'/'M15', '383305').

card_in_set('meditation puzzle', 'M15').
card_original_type('meditation puzzle'/'M15', 'Instant').
card_original_text('meditation puzzle'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nYou gain 8 life.').
card_first_print('meditation puzzle', 'M15').
card_image_name('meditation puzzle'/'M15', 'meditation puzzle').
card_uid('meditation puzzle'/'M15', 'M15:Meditation Puzzle:meditation puzzle').
card_rarity('meditation puzzle'/'M15', 'Common').
card_artist('meditation puzzle'/'M15', 'Mark Winters').
card_number('meditation puzzle'/'M15', '19').
card_flavor_text('meditation puzzle'/'M15', 'Find your center, and you will find your way.').
card_multiverse_id('meditation puzzle'/'M15', '383306').

card_in_set('mercurial pretender', 'M15').
card_original_type('mercurial pretender'/'M15', 'Creature — Shapeshifter').
card_original_text('mercurial pretender'/'M15', 'You may have Mercurial Pretender enter the battlefield as a copy of any creature you control except it gains \"{2}{U}{U}: Return this creature to its owner\'s hand.\"').
card_image_name('mercurial pretender'/'M15', 'mercurial pretender').
card_uid('mercurial pretender'/'M15', 'M15:Mercurial Pretender:mercurial pretender').
card_rarity('mercurial pretender'/'M15', 'Rare').
card_artist('mercurial pretender'/'M15', 'Izzy').
card_number('mercurial pretender'/'M15', '68').
card_flavor_text('mercurial pretender'/'M15', 'The king went off to find himself. Imagine his terror when he succeeded.').
card_multiverse_id('mercurial pretender'/'M15', '383307').

card_in_set('meteorite', 'M15').
card_original_type('meteorite'/'M15', 'Artifact').
card_original_text('meteorite'/'M15', 'When Meteorite enters the battlefield, it deals 2 damage to target creature or player.\n{T}: Add one mana of any color to your mana pool.').
card_first_print('meteorite', 'M15').
card_image_name('meteorite'/'M15', 'meteorite').
card_uid('meteorite'/'M15', 'M15:Meteorite:meteorite').
card_rarity('meteorite'/'M15', 'Uncommon').
card_artist('meteorite'/'M15', 'Scott Murphy').
card_number('meteorite'/'M15', '221').
card_flavor_text('meteorite'/'M15', '\"And if I\'m lying,\" he began . . .').
card_multiverse_id('meteorite'/'M15', '383308').

card_in_set('midnight guard', 'M15').
card_original_type('midnight guard'/'M15', 'Creature — Human Soldier').
card_original_text('midnight guard'/'M15', 'Whenever another creature enters the battlefield, untap Midnight Guard.').
card_image_name('midnight guard'/'M15', 'midnight guard').
card_uid('midnight guard'/'M15', 'M15:Midnight Guard:midnight guard').
card_rarity('midnight guard'/'M15', 'Common').
card_artist('midnight guard'/'M15', 'Jason A. Engle').
card_number('midnight guard'/'M15', '20').
card_flavor_text('midnight guard'/'M15', '\"When you\'re on watch, no noise is harmless and no shadow can be ignored.\"\n—Olgard of the Skiltfolk').
card_multiverse_id('midnight guard'/'M15', '383309').

card_in_set('might makes right', 'M15').
card_original_type('might makes right'/'M15', 'Enchantment').
card_original_text('might makes right'/'M15', 'At the beginning of combat on your turn, if you control each creature on the battlefield with the greatest power, gain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_first_print('might makes right', 'M15').
card_image_name('might makes right'/'M15', 'might makes right').
card_uid('might makes right'/'M15', 'M15:Might Makes Right:might makes right').
card_rarity('might makes right'/'M15', 'Uncommon').
card_artist('might makes right'/'M15', 'Mark Winters').
card_number('might makes right'/'M15', '156').
card_flavor_text('might makes right'/'M15', 'An oath of fealty sworn with a handshake.').
card_multiverse_id('might makes right'/'M15', '383310').

card_in_set('military intelligence', 'M15').
card_original_type('military intelligence'/'M15', 'Enchantment').
card_original_text('military intelligence'/'M15', 'Whenever you attack with two or more creatures, draw a card.').
card_first_print('military intelligence', 'M15').
card_image_name('military intelligence'/'M15', 'military intelligence').
card_uid('military intelligence'/'M15', 'M15:Military Intelligence:military intelligence').
card_rarity('military intelligence'/'M15', 'Uncommon').
card_artist('military intelligence'/'M15', 'Craig J Spearing').
card_number('military intelligence'/'M15', '69').
card_flavor_text('military intelligence'/'M15', 'To know the battlefield is to anticipate the enemy. To know the enemy is to anticipate victory.').
card_multiverse_id('military intelligence'/'M15', '383311').

card_in_set('mind rot', 'M15').
card_original_type('mind rot'/'M15', 'Sorcery').
card_original_text('mind rot'/'M15', 'Target player discards two cards.').
card_image_name('mind rot'/'M15', 'mind rot').
card_uid('mind rot'/'M15', 'M15:Mind Rot:mind rot').
card_rarity('mind rot'/'M15', 'Common').
card_artist('mind rot'/'M15', 'Steve Luke').
card_number('mind rot'/'M15', '104').
card_flavor_text('mind rot'/'M15', '\"It saddens me to lose a source of inspiration. This one seemed especially promising.\"\n—Ashiok').
card_multiverse_id('mind rot'/'M15', '383312').

card_in_set('mind sculpt', 'M15').
card_original_type('mind sculpt'/'M15', 'Sorcery').
card_original_text('mind sculpt'/'M15', 'Target opponent puts the top seven cards of his or her library into his or her graveyard.').
card_image_name('mind sculpt'/'M15', 'mind sculpt').
card_uid('mind sculpt'/'M15', 'M15:Mind Sculpt:mind sculpt').
card_rarity('mind sculpt'/'M15', 'Common').
card_artist('mind sculpt'/'M15', 'Michael C. Hayes').
card_number('mind sculpt'/'M15', '70').
card_flavor_text('mind sculpt'/'M15', '\"Your mind was a curious mix of madness and genius. I just took away the genius.\"\n—Jace Beleren').
card_multiverse_id('mind sculpt'/'M15', '383313').

card_in_set('miner\'s bane', 'M15').
card_original_type('miner\'s bane'/'M15', 'Creature — Elemental').
card_original_text('miner\'s bane'/'M15', '{2}{R}: Miner\'s Bane gets +1/+0 and gains trample until end of turn. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('miner\'s bane', 'M15').
card_image_name('miner\'s bane'/'M15', 'miner\'s bane').
card_uid('miner\'s bane'/'M15', 'M15:Miner\'s Bane:miner\'s bane').
card_rarity('miner\'s bane'/'M15', 'Common').
card_artist('miner\'s bane'/'M15', 'Adam Paquette').
card_number('miner\'s bane'/'M15', '157').
card_flavor_text('miner\'s bane'/'M15', 'There are certain stones even dwarves know to leave in the earth.').
card_multiverse_id('miner\'s bane'/'M15', '383314').

card_in_set('mountain', 'M15').
card_original_type('mountain'/'M15', 'Basic Land — Mountain').
card_original_text('mountain'/'M15', 'R').
card_image_name('mountain'/'M15', 'mountain1').
card_uid('mountain'/'M15', 'M15:Mountain:mountain1').
card_rarity('mountain'/'M15', 'Basic Land').
card_artist('mountain'/'M15', 'Cliff Childs').
card_number('mountain'/'M15', '262').
card_multiverse_id('mountain'/'M15', '383315').

card_in_set('mountain', 'M15').
card_original_type('mountain'/'M15', 'Basic Land — Mountain').
card_original_text('mountain'/'M15', 'R').
card_image_name('mountain'/'M15', 'mountain2').
card_uid('mountain'/'M15', 'M15:Mountain:mountain2').
card_rarity('mountain'/'M15', 'Basic Land').
card_artist('mountain'/'M15', 'Florian de Gesincourt').
card_number('mountain'/'M15', '263').
card_multiverse_id('mountain'/'M15', '383317').

card_in_set('mountain', 'M15').
card_original_type('mountain'/'M15', 'Basic Land — Mountain').
card_original_text('mountain'/'M15', 'R').
card_image_name('mountain'/'M15', 'mountain3').
card_uid('mountain'/'M15', 'M15:Mountain:mountain3').
card_rarity('mountain'/'M15', 'Basic Land').
card_artist('mountain'/'M15', 'Nils Hamm').
card_number('mountain'/'M15', '264').
card_multiverse_id('mountain'/'M15', '383318').

card_in_set('mountain', 'M15').
card_original_type('mountain'/'M15', 'Basic Land — Mountain').
card_original_text('mountain'/'M15', 'R').
card_image_name('mountain'/'M15', 'mountain4').
card_uid('mountain'/'M15', 'M15:Mountain:mountain4').
card_rarity('mountain'/'M15', 'Basic Land').
card_artist('mountain'/'M15', 'Karl Kopinski').
card_number('mountain'/'M15', '265').
card_multiverse_id('mountain'/'M15', '383316').

card_in_set('naturalize', 'M15').
card_original_type('naturalize'/'M15', 'Instant').
card_original_text('naturalize'/'M15', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'M15', 'naturalize').
card_uid('naturalize'/'M15', 'M15:Naturalize:naturalize').
card_rarity('naturalize'/'M15', 'Common').
card_artist('naturalize'/'M15', 'Tim Hildebrandt').
card_number('naturalize'/'M15', '185').
card_flavor_text('naturalize'/'M15', '\"When your cities and trinkets crumble, only nature will remain.\"\n—Garruk Wildspeaker').
card_multiverse_id('naturalize'/'M15', '383319').

card_in_set('necrobite', 'M15').
card_original_type('necrobite'/'M15', 'Instant').
card_original_text('necrobite'/'M15', 'Target creature gains deathtouch until end of turn. Regenerate it. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat. Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_image_name('necrobite'/'M15', 'necrobite').
card_uid('necrobite'/'M15', 'M15:Necrobite:necrobite').
card_rarity('necrobite'/'M15', 'Common').
card_artist('necrobite'/'M15', 'Nils Hamm').
card_number('necrobite'/'M15', '105').
card_multiverse_id('necrobite'/'M15', '383320').

card_in_set('necrogen scudder', 'M15').
card_original_type('necrogen scudder'/'M15', 'Creature — Horror').
card_original_text('necrogen scudder'/'M15', 'Flying\nWhen Necrogen Scudder enters the battlefield, you lose 3 life.').
card_image_name('necrogen scudder'/'M15', 'necrogen scudder').
card_uid('necrogen scudder'/'M15', 'M15:Necrogen Scudder:necrogen scudder').
card_rarity('necrogen scudder'/'M15', 'Uncommon').
card_artist('necrogen scudder'/'M15', 'Raymond Swanland').
card_number('necrogen scudder'/'M15', '106').
card_flavor_text('necrogen scudder'/'M15', 'Contrary to popular belief, it\'s kept aloft by necrogen gas, not the screaming agony of a thousand murdered souls.').
card_multiverse_id('necrogen scudder'/'M15', '383321').

card_in_set('necromancer\'s assistant', 'M15').
card_original_type('necromancer\'s assistant'/'M15', 'Creature — Zombie').
card_original_text('necromancer\'s assistant'/'M15', 'When Necromancer\'s Assistant enters the battlefield, put the top three cards of your library into your graveyard.').
card_first_print('necromancer\'s assistant', 'M15').
card_image_name('necromancer\'s assistant'/'M15', 'necromancer\'s assistant').
card_uid('necromancer\'s assistant'/'M15', 'M15:Necromancer\'s Assistant:necromancer\'s assistant').
card_rarity('necromancer\'s assistant'/'M15', 'Common').
card_artist('necromancer\'s assistant'/'M15', 'Svetlin Velinov').
card_number('necromancer\'s assistant'/'M15', '107').
card_flavor_text('necromancer\'s assistant'/'M15', 'Zombies and necromancers agree: easy access to brains is preferred.').
card_multiverse_id('necromancer\'s assistant'/'M15', '383322').

card_in_set('necromancer\'s stockpile', 'M15').
card_original_type('necromancer\'s stockpile'/'M15', 'Enchantment').
card_original_text('necromancer\'s stockpile'/'M15', '{1}{B}, Discard a creature card: Draw a card. If the discarded card was a Zombie card, put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_first_print('necromancer\'s stockpile', 'M15').
card_image_name('necromancer\'s stockpile'/'M15', 'necromancer\'s stockpile').
card_uid('necromancer\'s stockpile'/'M15', 'M15:Necromancer\'s Stockpile:necromancer\'s stockpile').
card_rarity('necromancer\'s stockpile'/'M15', 'Rare').
card_artist('necromancer\'s stockpile'/'M15', 'Seb McKinnon').
card_number('necromancer\'s stockpile'/'M15', '108').
card_flavor_text('necromancer\'s stockpile'/'M15', 'The experiments decided to perform some research of their own.').
card_multiverse_id('necromancer\'s stockpile'/'M15', '383323').

card_in_set('negate', 'M15').
card_original_type('negate'/'M15', 'Instant').
card_original_text('negate'/'M15', 'Counter target noncreature spell.').
card_image_name('negate'/'M15', 'negate').
card_uid('negate'/'M15', 'M15:Negate:negate').
card_rarity('negate'/'M15', 'Common').
card_artist('negate'/'M15', 'Jeremy Jarvis').
card_number('negate'/'M15', '71').
card_flavor_text('negate'/'M15', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'M15', '383324').

card_in_set('netcaster spider', 'M15').
card_original_type('netcaster spider'/'M15', 'Creature — Spider').
card_original_text('netcaster spider'/'M15', 'Reach (This creature can block creatures with flying.)\nWhenever Netcaster Spider blocks a creature with flying, Netcaster Spider gets +2/+0 until end of turn.').
card_first_print('netcaster spider', 'M15').
card_image_name('netcaster spider'/'M15', 'netcaster spider').
card_uid('netcaster spider'/'M15', 'M15:Netcaster Spider:netcaster spider').
card_rarity('netcaster spider'/'M15', 'Common').
card_artist('netcaster spider'/'M15', 'Yohann Schepacz').
card_number('netcaster spider'/'M15', '186').
card_flavor_text('netcaster spider'/'M15', 'It is an expert at culling individuals who stray too far from the herd.').
card_multiverse_id('netcaster spider'/'M15', '383325').

card_in_set('nightfire giant', 'M15').
card_original_type('nightfire giant'/'M15', 'Creature — Zombie Giant').
card_original_text('nightfire giant'/'M15', 'Nightfire Giant gets +1/+1 as long as you control a Mountain.\n{4}{R}: Nightfire Giant deals 2 damage to target creature or player.').
card_first_print('nightfire giant', 'M15').
card_image_name('nightfire giant'/'M15', 'nightfire giant').
card_uid('nightfire giant'/'M15', 'M15:Nightfire Giant:nightfire giant').
card_rarity('nightfire giant'/'M15', 'Uncommon').
card_artist('nightfire giant'/'M15', 'Dave Kendall').
card_number('nightfire giant'/'M15', '109').
card_flavor_text('nightfire giant'/'M15', 'Nightfire turns the greatest weakness of the undead into formidable strength.').
card_multiverse_id('nightfire giant'/'M15', '383326').

card_in_set('nightmare', 'M15').
card_original_type('nightmare'/'M15', 'Creature — Nightmare Horse').
card_original_text('nightmare'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('nightmare'/'M15', 'nightmare').
card_uid('nightmare'/'M15', 'M15:Nightmare:nightmare').
card_rarity('nightmare'/'M15', 'Rare').
card_artist('nightmare'/'M15', 'Vance Kovacs').
card_number('nightmare'/'M15', '276').
card_flavor_text('nightmare'/'M15', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'M15', '383168').

card_in_set('nimbus of the isles', 'M15').
card_original_type('nimbus of the isles'/'M15', 'Creature — Elemental').
card_original_text('nimbus of the isles'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_first_print('nimbus of the isles', 'M15').
card_image_name('nimbus of the isles'/'M15', 'nimbus of the isles').
card_uid('nimbus of the isles'/'M15', 'M15:Nimbus of the Isles:nimbus of the isles').
card_rarity('nimbus of the isles'/'M15', 'Common').
card_artist('nimbus of the isles'/'M15', 'Cliff Childs').
card_number('nimbus of the isles'/'M15', '72').
card_flavor_text('nimbus of the isles'/'M15', 'The people of the Sevick Isles have a unique understanding of the term \"ominous clouds.\"').
card_multiverse_id('nimbus of the isles'/'M15', '383327').

card_in_set('nissa\'s expedition', 'M15').
card_original_type('nissa\'s expedition'/'M15', 'Sorcery').
card_original_text('nissa\'s expedition'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nSearch your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_first_print('nissa\'s expedition', 'M15').
card_image_name('nissa\'s expedition'/'M15', 'nissa\'s expedition').
card_uid('nissa\'s expedition'/'M15', 'M15:Nissa\'s Expedition:nissa\'s expedition').
card_rarity('nissa\'s expedition'/'M15', 'Uncommon').
card_artist('nissa\'s expedition'/'M15', 'Dan Scott').
card_number('nissa\'s expedition'/'M15', '188').
card_multiverse_id('nissa\'s expedition'/'M15', '383329').

card_in_set('nissa, worldwaker', 'M15').
card_original_type('nissa, worldwaker'/'M15', 'Planeswalker — Nissa').
card_original_text('nissa, worldwaker'/'M15', '+1: Target land you control becomes a 4/4 Elemental creature with trample. It\'s still a land.\n+1: Untap up to four target Forests.\n−7: Search your library for any number of basic land cards, put them onto the battlefield, then shuffle your library. Those lands become 4/4 Elemental creatures with trample. They\'re still lands.').
card_image_name('nissa, worldwaker'/'M15', 'nissa, worldwaker').
card_uid('nissa, worldwaker'/'M15', 'M15:Nissa, Worldwaker:nissa, worldwaker').
card_rarity('nissa, worldwaker'/'M15', 'Mythic Rare').
card_artist('nissa, worldwaker'/'M15', 'Peter Mohrbacher').
card_number('nissa, worldwaker'/'M15', '187').
card_multiverse_id('nissa, worldwaker'/'M15', '383328').

card_in_set('ob nixilis, unshackled', 'M15').
card_original_type('ob nixilis, unshackled'/'M15', 'Legendary Creature — Demon').
card_original_text('ob nixilis, unshackled'/'M15', 'Flying, trample\nWhenever an opponent searches his or her library, that player sacrifices a creature and loses 10 life.\nWhenever another creature dies, put a +1/+1 counter on Ob Nixilis, Unshackled.').
card_first_print('ob nixilis, unshackled', 'M15').
card_image_name('ob nixilis, unshackled'/'M15', 'ob nixilis, unshackled').
card_uid('ob nixilis, unshackled'/'M15', 'M15:Ob Nixilis, Unshackled:ob nixilis, unshackled').
card_rarity('ob nixilis, unshackled'/'M15', 'Rare').
card_artist('ob nixilis, unshackled'/'M15', 'Karl Kopinski').
card_number('ob nixilis, unshackled'/'M15', '110').
card_flavor_text('ob nixilis, unshackled'/'M15', 'Designed by Brad Muir').
card_multiverse_id('ob nixilis, unshackled'/'M15', '383330').

card_in_set('obelisk of urd', 'M15').
card_original_type('obelisk of urd'/'M15', 'Artifact').
card_original_text('obelisk of urd'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nAs Obelisk of Urd enters the battlefield, choose a creature type.\nCreatures you control of the chosen type get +2/+2.').
card_first_print('obelisk of urd', 'M15').
card_image_name('obelisk of urd'/'M15', 'obelisk of urd').
card_uid('obelisk of urd'/'M15', 'M15:Obelisk of Urd:obelisk of urd').
card_rarity('obelisk of urd'/'M15', 'Rare').
card_artist('obelisk of urd'/'M15', 'John Severin Brassell').
card_number('obelisk of urd'/'M15', '222').
card_multiverse_id('obelisk of urd'/'M15', '383331').

card_in_set('oppressive rays', 'M15').
card_original_type('oppressive rays'/'M15', 'Enchantment — Aura').
card_original_text('oppressive rays'/'M15', 'Enchant creature\nEnchanted creature can\'t attack or block unless its controller pays {3}.\nActivated abilities of enchanted creature cost {3} more to activate.').
card_image_name('oppressive rays'/'M15', 'oppressive rays').
card_uid('oppressive rays'/'M15', 'M15:Oppressive Rays:oppressive rays').
card_rarity('oppressive rays'/'M15', 'Common').
card_artist('oppressive rays'/'M15', 'Mark Zug').
card_number('oppressive rays'/'M15', '21').
card_multiverse_id('oppressive rays'/'M15', '383332').

card_in_set('oreskos swiftclaw', 'M15').
card_original_type('oreskos swiftclaw'/'M15', 'Creature — Cat Warrior').
card_original_text('oreskos swiftclaw'/'M15', '').
card_image_name('oreskos swiftclaw'/'M15', 'oreskos swiftclaw').
card_uid('oreskos swiftclaw'/'M15', 'M15:Oreskos Swiftclaw:oreskos swiftclaw').
card_rarity('oreskos swiftclaw'/'M15', 'Common').
card_artist('oreskos swiftclaw'/'M15', 'James Ryman').
card_number('oreskos swiftclaw'/'M15', '22').
card_flavor_text('oreskos swiftclaw'/'M15', 'After the Battle of Pharagax Bridge, the Champion spent many months among the leonin of Oreskos. She found that they were quick to take offense, not because they were thin-skinned, but because they were always eager for a fight.\n—The Theriad').
card_multiverse_id('oreskos swiftclaw'/'M15', '383333').

card_in_set('ornithopter', 'M15').
card_original_type('ornithopter'/'M15', 'Artifact Creature — Thopter').
card_original_text('ornithopter'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('ornithopter'/'M15', 'ornithopter').
card_uid('ornithopter'/'M15', 'M15:Ornithopter:ornithopter').
card_rarity('ornithopter'/'M15', 'Common').
card_artist('ornithopter'/'M15', 'Franz Vohwinkel').
card_number('ornithopter'/'M15', '223').
card_flavor_text('ornithopter'/'M15', 'Once a year, the skies over Paliano fill with the flying machines of those who hope to be taken on as pupils by the artificer Muzzio.').
card_multiverse_id('ornithopter'/'M15', '383334').

card_in_set('overwhelm', 'M15').
card_original_type('overwhelm'/'M15', 'Sorcery').
card_original_text('overwhelm'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nCreatures you control get +3/+3 until end of turn.').
card_image_name('overwhelm'/'M15', 'overwhelm').
card_uid('overwhelm'/'M15', 'M15:Overwhelm:overwhelm').
card_rarity('overwhelm'/'M15', 'Uncommon').
card_artist('overwhelm'/'M15', 'Wayne Reynolds').
card_number('overwhelm'/'M15', '189').
card_flavor_text('overwhelm'/'M15', 'The Conclave acts with a single will, expressed by countless warriors.').
card_multiverse_id('overwhelm'/'M15', '383335').

card_in_set('paragon of eternal wilds', 'M15').
card_original_type('paragon of eternal wilds'/'M15', 'Creature — Human Druid').
card_original_text('paragon of eternal wilds'/'M15', 'Other green creatures you control get +1/+1.\n{G}, {T}: Another target green creature you control gains trample until end of turn. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('paragon of eternal wilds', 'M15').
card_image_name('paragon of eternal wilds'/'M15', 'paragon of eternal wilds').
card_uid('paragon of eternal wilds'/'M15', 'M15:Paragon of Eternal Wilds:paragon of eternal wilds').
card_rarity('paragon of eternal wilds'/'M15', 'Uncommon').
card_artist('paragon of eternal wilds'/'M15', 'Winona Nelson').
card_number('paragon of eternal wilds'/'M15', '190').
card_multiverse_id('paragon of eternal wilds'/'M15', '383336').

card_in_set('paragon of fierce defiance', 'M15').
card_original_type('paragon of fierce defiance'/'M15', 'Creature — Human Warrior').
card_original_text('paragon of fierce defiance'/'M15', 'Other red creatures you control get +1/+1.\n{R}, {T}: Another target red creature you control gains haste until end of turn. (It can attack and {T} this turn.)').
card_first_print('paragon of fierce defiance', 'M15').
card_image_name('paragon of fierce defiance'/'M15', 'paragon of fierce defiance').
card_uid('paragon of fierce defiance'/'M15', 'M15:Paragon of Fierce Defiance:paragon of fierce defiance').
card_rarity('paragon of fierce defiance'/'M15', 'Uncommon').
card_artist('paragon of fierce defiance'/'M15', 'Slawomir Maniak').
card_number('paragon of fierce defiance'/'M15', '158').
card_multiverse_id('paragon of fierce defiance'/'M15', '383337').

card_in_set('paragon of gathering mists', 'M15').
card_original_type('paragon of gathering mists'/'M15', 'Creature — Human Wizard').
card_original_text('paragon of gathering mists'/'M15', 'Other blue creatures you control get +1/+1.\n{U}, {T}: Another target blue creature you control gains flying until end of turn.').
card_first_print('paragon of gathering mists', 'M15').
card_image_name('paragon of gathering mists'/'M15', 'paragon of gathering mists').
card_uid('paragon of gathering mists'/'M15', 'M15:Paragon of Gathering Mists:paragon of gathering mists').
card_rarity('paragon of gathering mists'/'M15', 'Uncommon').
card_artist('paragon of gathering mists'/'M15', 'Michael C. Hayes').
card_number('paragon of gathering mists'/'M15', '73').
card_multiverse_id('paragon of gathering mists'/'M15', '383338').

card_in_set('paragon of new dawns', 'M15').
card_original_type('paragon of new dawns'/'M15', 'Creature — Human Soldier').
card_original_text('paragon of new dawns'/'M15', 'Other white creatures you control get +1/+1.\n{W}, {T}: Another target white creature you control gains vigilance until end of turn. (Attacking doesn\'t cause it to tap.)').
card_first_print('paragon of new dawns', 'M15').
card_image_name('paragon of new dawns'/'M15', 'paragon of new dawns').
card_uid('paragon of new dawns'/'M15', 'M15:Paragon of New Dawns:paragon of new dawns').
card_rarity('paragon of new dawns'/'M15', 'Uncommon').
card_artist('paragon of new dawns'/'M15', 'John Stanko').
card_number('paragon of new dawns'/'M15', '23').
card_multiverse_id('paragon of new dawns'/'M15', '383339').

card_in_set('paragon of open graves', 'M15').
card_original_type('paragon of open graves'/'M15', 'Creature — Skeleton Warrior').
card_original_text('paragon of open graves'/'M15', 'Other black creatures you control get +1/+1.\n{2}{B}, {T}: Another target black creature you control gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('paragon of open graves', 'M15').
card_image_name('paragon of open graves'/'M15', 'paragon of open graves').
card_uid('paragon of open graves'/'M15', 'M15:Paragon of Open Graves:paragon of open graves').
card_rarity('paragon of open graves'/'M15', 'Uncommon').
card_artist('paragon of open graves'/'M15', 'Min Yum').
card_number('paragon of open graves'/'M15', '111').
card_multiverse_id('paragon of open graves'/'M15', '383340').

card_in_set('peel from reality', 'M15').
card_original_type('peel from reality'/'M15', 'Instant').
card_original_text('peel from reality'/'M15', 'Return target creature you control and target creature you don\'t control to their owners\' hands.').
card_image_name('peel from reality'/'M15', 'peel from reality').
card_uid('peel from reality'/'M15', 'M15:Peel from Reality:peel from reality').
card_rarity('peel from reality'/'M15', 'Common').
card_artist('peel from reality'/'M15', 'Jason Felix').
card_number('peel from reality'/'M15', '74').
card_flavor_text('peel from reality'/'M15', '\"Soulless demon, you are bound to me. Now we will both dwell in oblivion.\"').
card_multiverse_id('peel from reality'/'M15', '383341').

card_in_set('perilous vault', 'M15').
card_original_type('perilous vault'/'M15', 'Artifact').
card_original_text('perilous vault'/'M15', '{5}, {T}, Exile Perilous Vault: Exile all nonland permanents.').
card_first_print('perilous vault', 'M15').
card_image_name('perilous vault'/'M15', 'perilous vault').
card_uid('perilous vault'/'M15', 'M15:Perilous Vault:perilous vault').
card_rarity('perilous vault'/'M15', 'Mythic Rare').
card_artist('perilous vault'/'M15', 'Sam Burley').
card_number('perilous vault'/'M15', '224').
card_flavor_text('perilous vault'/'M15', 'The spirit dragon Ugin arranged the hedrons of Zendikar to direct leylines of energy. To disrupt one is to unleash devastation and chaos.').
card_multiverse_id('perilous vault'/'M15', '383342').

card_in_set('phyrexian revoker', 'M15').
card_original_type('phyrexian revoker'/'M15', 'Artifact Creature — Horror').
card_original_text('phyrexian revoker'/'M15', 'As Phyrexian Revoker enters the battlefield, name a nonland card.\nActivated abilities of sources with the chosen name can\'t be activated.').
card_image_name('phyrexian revoker'/'M15', 'phyrexian revoker').
card_uid('phyrexian revoker'/'M15', 'M15:Phyrexian Revoker:phyrexian revoker').
card_rarity('phyrexian revoker'/'M15', 'Rare').
card_artist('phyrexian revoker'/'M15', 'Kev Walker').
card_number('phyrexian revoker'/'M15', '225').
card_flavor_text('phyrexian revoker'/'M15', '\"To be in the presence of a revoker is to be truly alone.\"\n—Kara Vrist, Neurok agent').
card_multiverse_id('phyrexian revoker'/'M15', '383343').

card_in_set('phytotitan', 'M15').
card_original_type('phytotitan'/'M15', 'Creature — Plant Elemental').
card_original_text('phytotitan'/'M15', 'When Phytotitan dies, return it to the battlefield tapped under its owner\'s control at the beginning of his or her next upkeep.').
card_image_name('phytotitan'/'M15', 'phytotitan').
card_uid('phytotitan'/'M15', 'M15:Phytotitan:phytotitan').
card_rarity('phytotitan'/'M15', 'Rare').
card_artist('phytotitan'/'M15', 'Marco Nelor').
card_number('phytotitan'/'M15', '191').
card_flavor_text('phytotitan'/'M15', 'Its root system spans the entire floor of the jungle, making eradication impossible.').
card_multiverse_id('phytotitan'/'M15', '383344').

card_in_set('pillar of light', 'M15').
card_original_type('pillar of light'/'M15', 'Instant').
card_original_text('pillar of light'/'M15', 'Exile target creature with toughness 4 or greater.').
card_first_print('pillar of light', 'M15').
card_image_name('pillar of light'/'M15', 'pillar of light').
card_uid('pillar of light'/'M15', 'M15:Pillar of Light:pillar of light').
card_rarity('pillar of light'/'M15', 'Common').
card_artist('pillar of light'/'M15', 'Erica Yang').
card_number('pillar of light'/'M15', '24').
card_flavor_text('pillar of light'/'M15', '\"The vaulted ceiling of our faith rests upon such pillars.\"\n—Darugand, banisher priest').
card_multiverse_id('pillar of light'/'M15', '383345').

card_in_set('plains', 'M15').
card_original_type('plains'/'M15', 'Basic Land — Plains').
card_original_text('plains'/'M15', 'W').
card_image_name('plains'/'M15', 'plains1').
card_uid('plains'/'M15', 'M15:Plains:plains1').
card_rarity('plains'/'M15', 'Basic Land').
card_artist('plains'/'M15', 'John Avon').
card_number('plains'/'M15', '250').
card_multiverse_id('plains'/'M15', '383347').

card_in_set('plains', 'M15').
card_original_type('plains'/'M15', 'Basic Land — Plains').
card_original_text('plains'/'M15', 'W').
card_image_name('plains'/'M15', 'plains2').
card_uid('plains'/'M15', 'M15:Plains:plains2').
card_rarity('plains'/'M15', 'Basic Land').
card_artist('plains'/'M15', 'Nils Hamm').
card_number('plains'/'M15', '251').
card_multiverse_id('plains'/'M15', '383346').

card_in_set('plains', 'M15').
card_original_type('plains'/'M15', 'Basic Land — Plains').
card_original_text('plains'/'M15', 'W').
card_image_name('plains'/'M15', 'plains3').
card_uid('plains'/'M15', 'M15:Plains:plains3').
card_rarity('plains'/'M15', 'Basic Land').
card_artist('plains'/'M15', 'Howard Lyon').
card_number('plains'/'M15', '252').
card_multiverse_id('plains'/'M15', '383348').

card_in_set('plains', 'M15').
card_original_type('plains'/'M15', 'Basic Land — Plains').
card_original_text('plains'/'M15', 'W').
card_image_name('plains'/'M15', 'plains4').
card_uid('plains'/'M15', 'M15:Plains:plains4').
card_rarity('plains'/'M15', 'Basic Land').
card_artist('plains'/'M15', 'Andreas Rocha').
card_number('plains'/'M15', '253').
card_multiverse_id('plains'/'M15', '383349').

card_in_set('plummet', 'M15').
card_original_type('plummet'/'M15', 'Instant').
card_original_text('plummet'/'M15', 'Destroy target creature with flying.').
card_image_name('plummet'/'M15', 'plummet').
card_uid('plummet'/'M15', 'M15:Plummet:plummet').
card_rarity('plummet'/'M15', 'Common').
card_artist('plummet'/'M15', 'Pete Venters').
card_number('plummet'/'M15', '192').
card_flavor_text('plummet'/'M15', '\"Let nothing own the skies but the wind.\"\n—Dejara, Giltwood druid').
card_multiverse_id('plummet'/'M15', '383350').

card_in_set('polymorphist\'s jest', 'M15').
card_original_type('polymorphist\'s jest'/'M15', 'Instant').
card_original_text('polymorphist\'s jest'/'M15', 'Until end of turn, each creature target player controls loses all abilities and becomes a blue Frog with base power and toughness 1/1.').
card_first_print('polymorphist\'s jest', 'M15').
card_image_name('polymorphist\'s jest'/'M15', 'polymorphist\'s jest').
card_uid('polymorphist\'s jest'/'M15', 'M15:Polymorphist\'s Jest:polymorphist\'s jest').
card_rarity('polymorphist\'s jest'/'M15', 'Rare').
card_artist('polymorphist\'s jest'/'M15', 'Craig J Spearing').
card_number('polymorphist\'s jest'/'M15', '75').
card_flavor_text('polymorphist\'s jest'/'M15', '\"The flies were bothering me.\"\n—Jalira, master polymorphist').
card_multiverse_id('polymorphist\'s jest'/'M15', '383351').

card_in_set('preeminent captain', 'M15').
card_original_type('preeminent captain'/'M15', 'Creature — Kithkin Soldier').
card_original_text('preeminent captain'/'M15', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhenever Preeminent Captain attacks, you may put a Soldier creature card from your hand onto the battlefield tapped and attacking.').
card_image_name('preeminent captain'/'M15', 'preeminent captain').
card_uid('preeminent captain'/'M15', 'M15:Preeminent Captain:preeminent captain').
card_rarity('preeminent captain'/'M15', 'Rare').
card_artist('preeminent captain'/'M15', 'Greg Staples').
card_number('preeminent captain'/'M15', '25').
card_multiverse_id('preeminent captain'/'M15', '383352').

card_in_set('profane memento', 'M15').
card_original_type('profane memento'/'M15', 'Artifact').
card_original_text('profane memento'/'M15', 'Whenever a creature card is put into an opponent\'s graveyard from anywhere, you gain 1 life.').
card_first_print('profane memento', 'M15').
card_image_name('profane memento'/'M15', 'profane memento').
card_uid('profane memento'/'M15', 'M15:Profane Memento:profane memento').
card_rarity('profane memento'/'M15', 'Uncommon').
card_artist('profane memento'/'M15', 'Franz Vohwinkel').
card_number('profane memento'/'M15', '226').
card_flavor_text('profane memento'/'M15', '\"An angel\'s skull is left too plain by death. I made a few aesthetic modifications.\"\n—Dommique, blood artist').
card_multiverse_id('profane memento'/'M15', '383353').

card_in_set('quickling', 'M15').
card_original_type('quickling'/'M15', 'Creature — Faerie Rogue').
card_original_text('quickling'/'M15', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Quickling enters the battlefield, sacrifice it unless you return another creature you control to its owner\'s hand.').
card_first_print('quickling', 'M15').
card_image_name('quickling'/'M15', 'quickling').
card_uid('quickling'/'M15', 'M15:Quickling:quickling').
card_rarity('quickling'/'M15', 'Uncommon').
card_artist('quickling'/'M15', 'Clint Cearley').
card_number('quickling'/'M15', '76').
card_multiverse_id('quickling'/'M15', '383354').

card_in_set('radiant fountain', 'M15').
card_original_type('radiant fountain'/'M15', 'Land').
card_original_text('radiant fountain'/'M15', 'When Radiant Fountain enters the battlefield, you gain 2 life.\n{T}: Add {1} to your mana pool.').
card_first_print('radiant fountain', 'M15').
card_image_name('radiant fountain'/'M15', 'radiant fountain').
card_uid('radiant fountain'/'M15', 'M15:Radiant Fountain:radiant fountain').
card_rarity('radiant fountain'/'M15', 'Common').
card_artist('radiant fountain'/'M15', 'Adam Paquette').
card_number('radiant fountain'/'M15', '245').
card_flavor_text('radiant fountain'/'M15', '\"All peoples treasure a place where the weary traveler may drink in peace.\"\n—Ajani Goldmane').
card_multiverse_id('radiant fountain'/'M15', '383355').

card_in_set('raise the alarm', 'M15').
card_original_type('raise the alarm'/'M15', 'Instant').
card_original_text('raise the alarm'/'M15', 'Put two 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('raise the alarm'/'M15', 'raise the alarm').
card_uid('raise the alarm'/'M15', 'M15:Raise the Alarm:raise the alarm').
card_rarity('raise the alarm'/'M15', 'Common').
card_artist('raise the alarm'/'M15', 'Zoltan Boros').
card_number('raise the alarm'/'M15', '26').
card_flavor_text('raise the alarm'/'M15', 'Like blinking or breathing, responding to an alarm is an involuntary reflex.').
card_multiverse_id('raise the alarm'/'M15', '383356').

card_in_set('ranger\'s guile', 'M15').
card_original_type('ranger\'s guile'/'M15', 'Instant').
card_original_text('ranger\'s guile'/'M15', 'Target creature you control gets +1/+1 and gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_image_name('ranger\'s guile'/'M15', 'ranger\'s guile').
card_uid('ranger\'s guile'/'M15', 'M15:Ranger\'s Guile:ranger\'s guile').
card_rarity('ranger\'s guile'/'M15', 'Common').
card_artist('ranger\'s guile'/'M15', 'Steve Prescott').
card_number('ranger\'s guile'/'M15', '193').
card_flavor_text('ranger\'s guile'/'M15', '\"You don\'t survive in the wild by standing in plain sight.\"\n—Garruk Wildspeaker').
card_multiverse_id('ranger\'s guile'/'M15', '383357').

card_in_set('razorfoot griffin', 'M15').
card_original_type('razorfoot griffin'/'M15', 'Creature — Griffin').
card_original_text('razorfoot griffin'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('razorfoot griffin'/'M15', 'razorfoot griffin').
card_uid('razorfoot griffin'/'M15', 'M15:Razorfoot Griffin:razorfoot griffin').
card_rarity('razorfoot griffin'/'M15', 'Common').
card_artist('razorfoot griffin'/'M15', 'Ben Thompson').
card_number('razorfoot griffin'/'M15', '27').
card_flavor_text('razorfoot griffin'/'M15', 'Like a meteor, it strikes from above without warning. Unlike a meteor, it then carries you off and eats you.').
card_multiverse_id('razorfoot griffin'/'M15', '383358').

card_in_set('reclamation sage', 'M15').
card_original_type('reclamation sage'/'M15', 'Creature — Elf Shaman').
card_original_text('reclamation sage'/'M15', 'When Reclamation Sage enters the battlefield, you may destroy target artifact or enchantment.').
card_image_name('reclamation sage'/'M15', 'reclamation sage').
card_uid('reclamation sage'/'M15', 'M15:Reclamation Sage:reclamation sage').
card_rarity('reclamation sage'/'M15', 'Uncommon').
card_artist('reclamation sage'/'M15', 'Christopher Moeller').
card_number('reclamation sage'/'M15', '194').
card_flavor_text('reclamation sage'/'M15', '\"What was once formed by masons, shaped by smiths, or given life by mages, I will return to the embrace of the earth.\"').
card_multiverse_id('reclamation sage'/'M15', '383359').

card_in_set('research assistant', 'M15').
card_original_type('research assistant'/'M15', 'Creature — Human Wizard').
card_original_text('research assistant'/'M15', '{3}{U}, {T}: Draw a card, then discard a card.').
card_first_print('research assistant', 'M15').
card_image_name('research assistant'/'M15', 'research assistant').
card_uid('research assistant'/'M15', 'M15:Research Assistant:research assistant').
card_rarity('research assistant'/'M15', 'Common').
card_artist('research assistant'/'M15', 'Svetlin Velinov').
card_number('research assistant'/'M15', '77').
card_flavor_text('research assistant'/'M15', 'There are many words and phrases that can cause an experienced wizard to tremble in fear. Chief among them is \"oops.\"').
card_multiverse_id('research assistant'/'M15', '383360').

card_in_set('resolute archangel', 'M15').
card_original_type('resolute archangel'/'M15', 'Creature — Angel').
card_original_text('resolute archangel'/'M15', 'Flying\nWhen Resolute Archangel enters the battlefield, if your life total is less than your starting life total, it becomes equal to your starting life total.').
card_image_name('resolute archangel'/'M15', 'resolute archangel').
card_uid('resolute archangel'/'M15', 'M15:Resolute Archangel:resolute archangel').
card_rarity('resolute archangel'/'M15', 'Rare').
card_artist('resolute archangel'/'M15', 'Anthony Palumbo').
card_number('resolute archangel'/'M15', '28').
card_flavor_text('resolute archangel'/'M15', 'Cut it down, bury it in snow, put it to the torch. The rose will still bloom again.').
card_multiverse_id('resolute archangel'/'M15', '383361').

card_in_set('restock', 'M15').
card_original_type('restock'/'M15', 'Sorcery').
card_original_text('restock'/'M15', 'Return two target cards from your graveyard to your hand. Exile Restock.').
card_image_name('restock'/'M15', 'restock').
card_uid('restock'/'M15', 'M15:Restock:restock').
card_rarity('restock'/'M15', 'Uncommon').
card_artist('restock'/'M15', 'Daren Bader').
card_number('restock'/'M15', '195').
card_flavor_text('restock'/'M15', 'What looked like a retreat was actually a replenishing.').
card_multiverse_id('restock'/'M15', '383362').

card_in_set('return to the ranks', 'M15').
card_original_type('return to the ranks'/'M15', 'Sorcery').
card_original_text('return to the ranks'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nReturn X target creature cards with converted mana cost 2 or less from your graveyard to the battlefield.').
card_first_print('return to the ranks', 'M15').
card_image_name('return to the ranks'/'M15', 'return to the ranks').
card_uid('return to the ranks'/'M15', 'M15:Return to the Ranks:return to the ranks').
card_rarity('return to the ranks'/'M15', 'Rare').
card_artist('return to the ranks'/'M15', 'Michael Komarck').
card_number('return to the ranks'/'M15', '29').
card_multiverse_id('return to the ranks'/'M15', '383363').

card_in_set('roaring primadox', 'M15').
card_original_type('roaring primadox'/'M15', 'Creature — Beast').
card_original_text('roaring primadox'/'M15', 'At the beginning of your upkeep, return a creature you control to its owner\'s hand.').
card_image_name('roaring primadox'/'M15', 'roaring primadox').
card_uid('roaring primadox'/'M15', 'M15:Roaring Primadox:roaring primadox').
card_rarity('roaring primadox'/'M15', 'Uncommon').
card_artist('roaring primadox'/'M15', 'James Ryman').
card_number('roaring primadox'/'M15', '196').
card_flavor_text('roaring primadox'/'M15', '\"They\'re easy enough to find. Question is, are you sure you want to find one?\"\n—Juruk, Kalonian tracker').
card_multiverse_id('roaring primadox'/'M15', '383364').

card_in_set('rogue\'s gloves', 'M15').
card_original_type('rogue\'s gloves'/'M15', 'Artifact — Equipment').
card_original_text('rogue\'s gloves'/'M15', 'Whenever equipped creature deals combat damage to a player, you may draw a card.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('rogue\'s gloves', 'M15').
card_image_name('rogue\'s gloves'/'M15', 'rogue\'s gloves').
card_uid('rogue\'s gloves'/'M15', 'M15:Rogue\'s Gloves:rogue\'s gloves').
card_rarity('rogue\'s gloves'/'M15', 'Uncommon').
card_artist('rogue\'s gloves'/'M15', 'Cyril Van Der Haegen').
card_number('rogue\'s gloves'/'M15', '227').
card_flavor_text('rogue\'s gloves'/'M15', 'Professional pilferers prefer proper preparation.').
card_multiverse_id('rogue\'s gloves'/'M15', '383365').

card_in_set('rotfeaster maggot', 'M15').
card_original_type('rotfeaster maggot'/'M15', 'Creature — Insect').
card_original_text('rotfeaster maggot'/'M15', 'When Rotfeaster Maggot enters the battlefield, exile target creature card from a graveyard. You gain life equal to that card\'s toughness.').
card_first_print('rotfeaster maggot', 'M15').
card_image_name('rotfeaster maggot'/'M15', 'rotfeaster maggot').
card_uid('rotfeaster maggot'/'M15', 'M15:Rotfeaster Maggot:rotfeaster maggot').
card_rarity('rotfeaster maggot'/'M15', 'Common').
card_artist('rotfeaster maggot'/'M15', 'Yeong-Hao Han').
card_number('rotfeaster maggot'/'M15', '112').
card_flavor_text('rotfeaster maggot'/'M15', 'Is it at the top of the food chain or the bottom?').
card_multiverse_id('rotfeaster maggot'/'M15', '383366').

card_in_set('rummaging goblin', 'M15').
card_original_type('rummaging goblin'/'M15', 'Creature — Goblin Rogue').
card_original_text('rummaging goblin'/'M15', '{T}, Discard a card: Draw a card.').
card_image_name('rummaging goblin'/'M15', 'rummaging goblin').
card_uid('rummaging goblin'/'M15', 'M15:Rummaging Goblin:rummaging goblin').
card_rarity('rummaging goblin'/'M15', 'Common').
card_artist('rummaging goblin'/'M15', 'Karl Kopinski').
card_number('rummaging goblin'/'M15', '159').
card_flavor_text('rummaging goblin'/'M15', 'To a goblin, value is based on the four S\'s: shiny, stabby, smelly, and super smelly.').
card_multiverse_id('rummaging goblin'/'M15', '383367').

card_in_set('runeclaw bear', 'M15').
card_original_type('runeclaw bear'/'M15', 'Creature — Bear').
card_original_text('runeclaw bear'/'M15', '').
card_image_name('runeclaw bear'/'M15', 'runeclaw bear').
card_uid('runeclaw bear'/'M15', 'M15:Runeclaw Bear:runeclaw bear').
card_rarity('runeclaw bear'/'M15', 'Common').
card_artist('runeclaw bear'/'M15', 'Jesper Ejsing').
card_number('runeclaw bear'/'M15', '197').
card_flavor_text('runeclaw bear'/'M15', 'The magic of the elves leaves its mark on the forest. The magic of the forest leaves its mark on the animals who live there. The animals of the forest leave their mark on all who trespass.').
card_multiverse_id('runeclaw bear'/'M15', '383368').

card_in_set('sacred armory', 'M15').
card_original_type('sacred armory'/'M15', 'Artifact').
card_original_text('sacred armory'/'M15', '{2}: Target creature gets +1/+0 until end of turn.').
card_first_print('sacred armory', 'M15').
card_image_name('sacred armory'/'M15', 'sacred armory').
card_uid('sacred armory'/'M15', 'M15:Sacred Armory:sacred armory').
card_rarity('sacred armory'/'M15', 'Uncommon').
card_artist('sacred armory'/'M15', 'Yeong-Hao Han').
card_number('sacred armory'/'M15', '228').
card_flavor_text('sacred armory'/'M15', 'Arrive for worship. Leave for war.').
card_multiverse_id('sacred armory'/'M15', '383369').

card_in_set('sanctified charge', 'M15').
card_original_type('sanctified charge'/'M15', 'Instant').
card_original_text('sanctified charge'/'M15', 'Creatures you control get +2/+1 until end of turn. White creatures you control also gain first strike until end of turn. (They deal combat damage before creatures without first strike.)').
card_first_print('sanctified charge', 'M15').
card_image_name('sanctified charge'/'M15', 'sanctified charge').
card_uid('sanctified charge'/'M15', 'M15:Sanctified Charge:sanctified charge').
card_rarity('sanctified charge'/'M15', 'Common').
card_artist('sanctified charge'/'M15', 'Dave Kendall').
card_number('sanctified charge'/'M15', '30').
card_flavor_text('sanctified charge'/'M15', 'You need only raise your spear to receive this blessing.').
card_multiverse_id('sanctified charge'/'M15', '383370').

card_in_set('satyr wayfinder', 'M15').
card_original_type('satyr wayfinder'/'M15', 'Creature — Satyr').
card_original_text('satyr wayfinder'/'M15', 'When Satyr Wayfinder enters the battlefield, reveal the top four cards of your library. You may put a land card from among them into your hand. Put the rest into your graveyard.').
card_image_name('satyr wayfinder'/'M15', 'satyr wayfinder').
card_uid('satyr wayfinder'/'M15', 'M15:Satyr Wayfinder:satyr wayfinder').
card_rarity('satyr wayfinder'/'M15', 'Common').
card_artist('satyr wayfinder'/'M15', 'Steve Prescott').
card_number('satyr wayfinder'/'M15', '198').
card_flavor_text('satyr wayfinder'/'M15', 'The first satyr to wake after a revel must search for the site of the next one.').
card_multiverse_id('satyr wayfinder'/'M15', '383371').

card_in_set('scrapyard mongrel', 'M15').
card_original_type('scrapyard mongrel'/'M15', 'Creature — Hound').
card_original_text('scrapyard mongrel'/'M15', 'As long as you control an artifact, Scrapyard Mongrel gets +2/+0 and has trample. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('scrapyard mongrel', 'M15').
card_image_name('scrapyard mongrel'/'M15', 'scrapyard mongrel').
card_uid('scrapyard mongrel'/'M15', 'M15:Scrapyard Mongrel:scrapyard mongrel').
card_rarity('scrapyard mongrel'/'M15', 'Common').
card_artist('scrapyard mongrel'/'M15', 'Svetlin Velinov').
card_number('scrapyard mongrel'/'M15', '160').
card_flavor_text('scrapyard mongrel'/'M15', 'Trespassers are welcome to try.').
card_multiverse_id('scrapyard mongrel'/'M15', '383372').

card_in_set('scuttling doom engine', 'M15').
card_original_type('scuttling doom engine'/'M15', 'Artifact Creature — Construct').
card_original_text('scuttling doom engine'/'M15', 'Scuttling Doom Engine can\'t be blocked by creatures with power 2 or less.\nWhen Scuttling Doom Engine dies, it deals 6 damage to target opponent.').
card_first_print('scuttling doom engine', 'M15').
card_image_name('scuttling doom engine'/'M15', 'scuttling doom engine').
card_uid('scuttling doom engine'/'M15', 'M15:Scuttling Doom Engine:scuttling doom engine').
card_rarity('scuttling doom engine'/'M15', 'Rare').
card_artist('scuttling doom engine'/'M15', 'Filip Burburan').
card_number('scuttling doom engine'/'M15', '229').
card_flavor_text('scuttling doom engine'/'M15', 'A masterwork of spite, inspired by madness.').
card_multiverse_id('scuttling doom engine'/'M15', '383373').

card_in_set('seismic strike', 'M15').
card_original_type('seismic strike'/'M15', 'Instant').
card_original_text('seismic strike'/'M15', 'Seismic Strike deals damage to target creature equal to the number of Mountains you control.').
card_image_name('seismic strike'/'M15', 'seismic strike').
card_uid('seismic strike'/'M15', 'M15:Seismic Strike:seismic strike').
card_rarity('seismic strike'/'M15', 'Common').
card_artist('seismic strike'/'M15', 'Christopher Moeller').
card_number('seismic strike'/'M15', '280').
card_flavor_text('seismic strike'/'M15', '\"Life up here is simple. Adapt to the ways of the mountains and they will reward you. Fight them and they will end you.\"\n—Kezim, prodigal pyromancer').
card_multiverse_id('seismic strike'/'M15', '383169').

card_in_set('selfless cathar', 'M15').
card_original_type('selfless cathar'/'M15', 'Creature — Human Cleric').
card_original_text('selfless cathar'/'M15', '{1}{W}, Sacrifice Selfless Cathar: Creatures you control get +1/+1 until end of turn.').
card_image_name('selfless cathar'/'M15', 'selfless cathar').
card_uid('selfless cathar'/'M15', 'M15:Selfless Cathar:selfless cathar').
card_rarity('selfless cathar'/'M15', 'Common').
card_artist('selfless cathar'/'M15', 'Slawomir Maniak').
card_number('selfless cathar'/'M15', '31').
card_flavor_text('selfless cathar'/'M15', '\"If I fail to offer myself, we will surely be overrun. My fate would be the same.\"').
card_multiverse_id('selfless cathar'/'M15', '383374').

card_in_set('sengir vampire', 'M15').
card_original_type('sengir vampire'/'M15', 'Creature — Vampire').
card_original_text('sengir vampire'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever a creature dealt damage by Sengir Vampire this turn dies, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'M15', 'sengir vampire').
card_uid('sengir vampire'/'M15', 'M15:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'M15', 'Uncommon').
card_artist('sengir vampire'/'M15', 'Kev Walker').
card_number('sengir vampire'/'M15', '277').
card_flavor_text('sengir vampire'/'M15', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'M15', '383170').

card_in_set('seraph of the masses', 'M15').
card_original_type('seraph of the masses'/'M15', 'Creature — Angel').
card_original_text('seraph of the masses'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nFlying\nSeraph of the Masses\'s power and toughness are each equal to the number of creatures you control.').
card_first_print('seraph of the masses', 'M15').
card_image_name('seraph of the masses'/'M15', 'seraph of the masses').
card_uid('seraph of the masses'/'M15', 'M15:Seraph of the Masses:seraph of the masses').
card_rarity('seraph of the masses'/'M15', 'Uncommon').
card_artist('seraph of the masses'/'M15', 'Zoltan Boros').
card_number('seraph of the masses'/'M15', '32').
card_multiverse_id('seraph of the masses'/'M15', '383375').

card_in_set('serra angel', 'M15').
card_original_type('serra angel'/'M15', 'Creature — Angel').
card_original_text('serra angel'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'M15', 'serra angel').
card_uid('serra angel'/'M15', 'M15:Serra Angel:serra angel').
card_rarity('serra angel'/'M15', 'Uncommon').
card_artist('serra angel'/'M15', 'Greg Staples').
card_number('serra angel'/'M15', '273').
card_flavor_text('serra angel'/'M15', 'Follow the light. In its absence, follow her.').
card_multiverse_id('serra angel'/'M15', '383171').

card_in_set('shadowcloak vampire', 'M15').
card_original_type('shadowcloak vampire'/'M15', 'Creature — Vampire').
card_original_text('shadowcloak vampire'/'M15', 'Pay 2 life: Shadowcloak Vampire gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)').
card_first_print('shadowcloak vampire', 'M15').
card_image_name('shadowcloak vampire'/'M15', 'shadowcloak vampire').
card_uid('shadowcloak vampire'/'M15', 'M15:Shadowcloak Vampire:shadowcloak vampire').
card_rarity('shadowcloak vampire'/'M15', 'Common').
card_artist('shadowcloak vampire'/'M15', 'Cynthia Sheppard').
card_number('shadowcloak vampire'/'M15', '113').
card_flavor_text('shadowcloak vampire'/'M15', '\"My favorite guilty pleasure? Are there innocent ones?\"').
card_multiverse_id('shadowcloak vampire'/'M15', '383376').

card_in_set('shaman of spring', 'M15').
card_original_type('shaman of spring'/'M15', 'Creature — Elf Shaman').
card_original_text('shaman of spring'/'M15', 'When Shaman of Spring enters the battlefield, draw a card.').
card_first_print('shaman of spring', 'M15').
card_image_name('shaman of spring'/'M15', 'shaman of spring').
card_uid('shaman of spring'/'M15', 'M15:Shaman of Spring:shaman of spring').
card_rarity('shaman of spring'/'M15', 'Common').
card_artist('shaman of spring'/'M15', 'Johannes Voss').
card_number('shaman of spring'/'M15', '199').
card_flavor_text('shaman of spring'/'M15', 'Some shamanic sects advocate the different seasons, each working to preserve nature\'s cycles.').
card_multiverse_id('shaman of spring'/'M15', '383377').

card_in_set('shield of the avatar', 'M15').
card_original_type('shield of the avatar'/'M15', 'Artifact — Equipment').
card_original_text('shield of the avatar'/'M15', 'If a source would deal damage to equipped creature, prevent X of that damage, where X is the number of creatures you control.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('shield of the avatar', 'M15').
card_image_name('shield of the avatar'/'M15', 'shield of the avatar').
card_uid('shield of the avatar'/'M15', 'M15:Shield of the Avatar:shield of the avatar').
card_rarity('shield of the avatar'/'M15', 'Rare').
card_artist('shield of the avatar'/'M15', 'Volkan Baga').
card_number('shield of the avatar'/'M15', '230').
card_flavor_text('shield of the avatar'/'M15', 'We are made stronger by those we fight for.\nDesigned by Richard Garriott').
card_multiverse_id('shield of the avatar'/'M15', '383378').

card_in_set('shivan dragon', 'M15').
card_original_type('shivan dragon'/'M15', 'Creature — Dragon').
card_original_text('shivan dragon'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'M15', 'shivan dragon').
card_uid('shivan dragon'/'M15', 'M15:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'M15', 'Rare').
card_artist('shivan dragon'/'M15', 'Donato Giancola').
card_number('shivan dragon'/'M15', '281').
card_flavor_text('shivan dragon'/'M15', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'M15', '383172').

card_in_set('shivan reef', 'M15').
card_original_type('shivan reef'/'M15', 'Land').
card_original_text('shivan reef'/'M15', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Shivan Reef deals 1 damage to you.').
card_image_name('shivan reef'/'M15', 'shivan reef').
card_uid('shivan reef'/'M15', 'M15:Shivan Reef:shivan reef').
card_rarity('shivan reef'/'M15', 'Rare').
card_artist('shivan reef'/'M15', 'Rob Alexander').
card_number('shivan reef'/'M15', '246').
card_multiverse_id('shivan reef'/'M15', '383379').

card_in_set('shrapnel blast', 'M15').
card_original_type('shrapnel blast'/'M15', 'Instant').
card_original_text('shrapnel blast'/'M15', 'As an additional cost to cast Shrapnel Blast, sacrifice an artifact.\nShrapnel Blast deals 5 damage to target creature or player.').
card_image_name('shrapnel blast'/'M15', 'shrapnel blast').
card_uid('shrapnel blast'/'M15', 'M15:Shrapnel Blast:shrapnel blast').
card_rarity('shrapnel blast'/'M15', 'Uncommon').
card_artist('shrapnel blast'/'M15', 'Hideaki Takamura').
card_number('shrapnel blast'/'M15', '161').
card_flavor_text('shrapnel blast'/'M15', 'It requires immense pressure to imbue an item with magic. If the spell is broken . . .').
card_multiverse_id('shrapnel blast'/'M15', '383380').

card_in_set('siege dragon', 'M15').
card_original_type('siege dragon'/'M15', 'Creature — Dragon').
card_original_text('siege dragon'/'M15', 'Flying\nWhen Siege Dragon enters the battlefield, destroy all Walls your opponents control.\nWhenever Siege Dragon attacks, if defending player controls no Walls, it deals 2 damage to each creature without flying that player controls.').
card_image_name('siege dragon'/'M15', 'siege dragon').
card_uid('siege dragon'/'M15', 'M15:Siege Dragon:siege dragon').
card_rarity('siege dragon'/'M15', 'Rare').
card_artist('siege dragon'/'M15', 'Karl Kopinski').
card_number('siege dragon'/'M15', '162').
card_multiverse_id('siege dragon'/'M15', '383381').

card_in_set('siege wurm', 'M15').
card_original_type('siege wurm'/'M15', 'Creature — Wurm').
card_original_text('siege wurm'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTrample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('siege wurm'/'M15', 'siege wurm').
card_uid('siege wurm'/'M15', 'M15:Siege Wurm:siege wurm').
card_rarity('siege wurm'/'M15', 'Common').
card_artist('siege wurm'/'M15', 'Carl Critchlow').
card_number('siege wurm'/'M15', '200').
card_multiverse_id('siege wurm'/'M15', '383382').

card_in_set('sign in blood', 'M15').
card_original_type('sign in blood'/'M15', 'Sorcery').
card_original_text('sign in blood'/'M15', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'M15', 'sign in blood').
card_uid('sign in blood'/'M15', 'M15:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'M15', 'Common').
card_artist('sign in blood'/'M15', 'Howard Lyon').
card_number('sign in blood'/'M15', '114').
card_flavor_text('sign in blood'/'M15', 'Little agonies pave the way to greater power.').
card_multiverse_id('sign in blood'/'M15', '383383').

card_in_set('sliver hive', 'M15').
card_original_type('sliver hive'/'M15', 'Land').
card_original_text('sliver hive'/'M15', '{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a Sliver spell.\n{5}, {T}: Put a 1/1 colorless Sliver creature token onto the battlefield. Activate this ability only if you control a Sliver.').
card_first_print('sliver hive', 'M15').
card_image_name('sliver hive'/'M15', 'sliver hive').
card_uid('sliver hive'/'M15', 'M15:Sliver Hive:sliver hive').
card_rarity('sliver hive'/'M15', 'Rare').
card_artist('sliver hive'/'M15', 'Igor Kieryluk').
card_number('sliver hive'/'M15', '247').
card_multiverse_id('sliver hive'/'M15', '383384').

card_in_set('sliver hivelord', 'M15').
card_original_type('sliver hivelord'/'M15', 'Legendary Creature — Sliver').
card_original_text('sliver hivelord'/'M15', 'Sliver creatures you control have indestructible. (Damage and effects that say \"destroy\" don\'t destroy them.)').
card_first_print('sliver hivelord', 'M15').
card_image_name('sliver hivelord'/'M15', 'sliver hivelord').
card_uid('sliver hivelord'/'M15', 'M15:Sliver Hivelord:sliver hivelord').
card_rarity('sliver hivelord'/'M15', 'Mythic Rare').
card_artist('sliver hivelord'/'M15', 'Aleksi Briclot').
card_number('sliver hivelord'/'M15', '211').
card_flavor_text('sliver hivelord'/'M15', '\"This is the source, the line unbroken since the calamity that brought such monsters to our shores.\"\n—Hastric, Thunian scout').
card_multiverse_id('sliver hivelord'/'M15', '383385').

card_in_set('solemn offering', 'M15').
card_original_type('solemn offering'/'M15', 'Sorcery').
card_original_text('solemn offering'/'M15', 'Destroy target artifact or enchantment. You gain 4 life.').
card_image_name('solemn offering'/'M15', 'solemn offering').
card_uid('solemn offering'/'M15', 'M15:Solemn Offering:solemn offering').
card_rarity('solemn offering'/'M15', 'Common').
card_artist('solemn offering'/'M15', 'Sam Wood').
card_number('solemn offering'/'M15', '33').
card_flavor_text('solemn offering'/'M15', '\"You will be reimbursed for your donation.\"\n\"The reimbursement is spiritual.\"\n—Temple signs').
card_multiverse_id('solemn offering'/'M15', '383386').

card_in_set('soul of innistrad', 'M15').
card_original_type('soul of innistrad'/'M15', 'Creature — Avatar').
card_original_text('soul of innistrad'/'M15', 'Deathtouch\n{3}{B}{B}: Return up to three target creature cards from your graveyard to your hand.\n{3}{B}{B}, Exile Soul of Innistrad from your graveyard: Return up to three target creature cards from your graveyard to your hand.').
card_first_print('soul of innistrad', 'M15').
card_image_name('soul of innistrad'/'M15', 'soul of innistrad').
card_uid('soul of innistrad'/'M15', 'M15:Soul of Innistrad:soul of innistrad').
card_rarity('soul of innistrad'/'M15', 'Mythic Rare').
card_artist('soul of innistrad'/'M15', 'Adam Paquette').
card_number('soul of innistrad'/'M15', '115').
card_multiverse_id('soul of innistrad'/'M15', '383387').

card_in_set('soul of new phyrexia', 'M15').
card_original_type('soul of new phyrexia'/'M15', 'Artifact Creature — Avatar').
card_original_text('soul of new phyrexia'/'M15', 'Trample\n{5}: Permanents you control gain indestructible until end of turn.\n{5}, Exile Soul of New Phyrexia from your graveyard: Permanents you control gain indestructible until end of turn.').
card_first_print('soul of new phyrexia', 'M15').
card_image_name('soul of new phyrexia'/'M15', 'soul of new phyrexia').
card_uid('soul of new phyrexia'/'M15', 'M15:Soul of New Phyrexia:soul of new phyrexia').
card_rarity('soul of new phyrexia'/'M15', 'Mythic Rare').
card_artist('soul of new phyrexia'/'M15', 'Daarken').
card_number('soul of new phyrexia'/'M15', '231').
card_multiverse_id('soul of new phyrexia'/'M15', '383388').

card_in_set('soul of ravnica', 'M15').
card_original_type('soul of ravnica'/'M15', 'Creature — Avatar').
card_original_text('soul of ravnica'/'M15', 'Flying\n{5}{U}{U}: Draw a card for each color among permanents you control.\n{5}{U}{U}, Exile Soul of Ravnica from your graveyard: Draw a card for each color among permanents you control.').
card_image_name('soul of ravnica'/'M15', 'soul of ravnica').
card_uid('soul of ravnica'/'M15', 'M15:Soul of Ravnica:soul of ravnica').
card_rarity('soul of ravnica'/'M15', 'Mythic Rare').
card_artist('soul of ravnica'/'M15', 'Stephan Martiniere').
card_number('soul of ravnica'/'M15', '78').
card_multiverse_id('soul of ravnica'/'M15', '383389').

card_in_set('soul of shandalar', 'M15').
card_original_type('soul of shandalar'/'M15', 'Creature — Avatar').
card_original_text('soul of shandalar'/'M15', 'First strike\n{3}{R}{R}: Soul of Shandalar deals 3 damage to target player and 3 damage to up to one target creature that player controls.\n{3}{R}{R}, Exile Soul of Shandalar from your graveyard: Soul of Shandalar deals 3 damage to target player and 3 damage to up to one target creature that player controls.').
card_first_print('soul of shandalar', 'M15').
card_image_name('soul of shandalar'/'M15', 'soul of shandalar').
card_uid('soul of shandalar'/'M15', 'M15:Soul of Shandalar:soul of shandalar').
card_rarity('soul of shandalar'/'M15', 'Mythic Rare').
card_artist('soul of shandalar'/'M15', 'Raymond Swanland').
card_number('soul of shandalar'/'M15', '163').
card_multiverse_id('soul of shandalar'/'M15', '383390').

card_in_set('soul of theros', 'M15').
card_original_type('soul of theros'/'M15', 'Creature — Avatar').
card_original_text('soul of theros'/'M15', 'Vigilance\n{4}{W}{W}: Creatures you control get +2/+2 and gain first strike and lifelink until end of turn.\n{4}{W}{W}, Exile Soul of Theros from your graveyard: Creatures you control get +2/+2 and gain first strike and lifelink until end of turn.').
card_first_print('soul of theros', 'M15').
card_image_name('soul of theros'/'M15', 'soul of theros').
card_uid('soul of theros'/'M15', 'M15:Soul of Theros:soul of theros').
card_rarity('soul of theros'/'M15', 'Mythic Rare').
card_artist('soul of theros'/'M15', 'Zack Stella').
card_number('soul of theros'/'M15', '34').
card_multiverse_id('soul of theros'/'M15', '383391').

card_in_set('soul of zendikar', 'M15').
card_original_type('soul of zendikar'/'M15', 'Creature — Avatar').
card_original_text('soul of zendikar'/'M15', 'Reach\n{3}{G}{G}: Put a 3/3 green Beast creature token onto the battlefield.\n{3}{G}{G}, Exile Soul of Zendikar from your graveyard: Put a 3/3 green Beast creature token onto the battlefield.').
card_image_name('soul of zendikar'/'M15', 'soul of zendikar').
card_uid('soul of zendikar'/'M15', 'M15:Soul of Zendikar:soul of zendikar').
card_rarity('soul of zendikar'/'M15', 'Mythic Rare').
card_artist('soul of zendikar'/'M15', 'Vincent Proce').
card_number('soul of zendikar'/'M15', '201').
card_multiverse_id('soul of zendikar'/'M15', '383392').

card_in_set('soulmender', 'M15').
card_original_type('soulmender'/'M15', 'Creature — Human Cleric').
card_original_text('soulmender'/'M15', '{T}: You gain 1 life.').
card_image_name('soulmender'/'M15', 'soulmender').
card_uid('soulmender'/'M15', 'M15:Soulmender:soulmender').
card_rarity('soulmender'/'M15', 'Common').
card_artist('soulmender'/'M15', 'James Ryman').
card_number('soulmender'/'M15', '35').
card_flavor_text('soulmender'/'M15', '\"Healing is more art than magic. Well, there is still quite a bit of magic.\"').
card_multiverse_id('soulmender'/'M15', '383393').

card_in_set('spectra ward', 'M15').
card_original_type('spectra ward'/'M15', 'Enchantment — Aura').
card_original_text('spectra ward'/'M15', 'Enchant creature\nEnchanted creature gets +2/+2 and has protection from all colors. This effect doesn\'t remove Auras. (It can\'t be blocked, targeted, or dealt damage by anything that\'s white, blue, black, red, or green.)').
card_first_print('spectra ward', 'M15').
card_image_name('spectra ward'/'M15', 'spectra ward').
card_uid('spectra ward'/'M15', 'M15:Spectra Ward:spectra ward').
card_rarity('spectra ward'/'M15', 'Rare').
card_artist('spectra ward'/'M15', 'Ryan Alexander Lee').
card_number('spectra ward'/'M15', '36').
card_multiverse_id('spectra ward'/'M15', '383394').

card_in_set('spirit bonds', 'M15').
card_original_type('spirit bonds'/'M15', 'Enchantment').
card_original_text('spirit bonds'/'M15', 'Whenever a nontoken creature enters the battlefield under your control, you may pay {W}. If you do, put a 1/1 white Spirit creature token with flying onto the battlefield.\n{1}{W}, Sacrifice a Spirit: Target non-Spirit creature gains indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy it.)').
card_first_print('spirit bonds', 'M15').
card_image_name('spirit bonds'/'M15', 'spirit bonds').
card_uid('spirit bonds'/'M15', 'M15:Spirit Bonds:spirit bonds').
card_rarity('spirit bonds'/'M15', 'Rare').
card_artist('spirit bonds'/'M15', 'Willian Murai').
card_number('spirit bonds'/'M15', '37').
card_flavor_text('spirit bonds'/'M15', 'Designed by Justin Gary').
card_multiverse_id('spirit bonds'/'M15', '383395').

card_in_set('stab wound', 'M15').
card_original_type('stab wound'/'M15', 'Enchantment — Aura').
card_original_text('stab wound'/'M15', 'Enchant creature\nEnchanted creature gets -2/-2.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player loses 2 life.').
card_image_name('stab wound'/'M15', 'stab wound').
card_uid('stab wound'/'M15', 'M15:Stab Wound:stab wound').
card_rarity('stab wound'/'M15', 'Uncommon').
card_artist('stab wound'/'M15', 'Scott Chou').
card_number('stab wound'/'M15', '116').
card_multiverse_id('stab wound'/'M15', '383396').

card_in_set('staff of the death magus', 'M15').
card_original_type('staff of the death magus'/'M15', 'Artifact').
card_original_text('staff of the death magus'/'M15', 'Whenever you cast a black spell or a Swamp enters the battlefield under your control, you gain 1 life.').
card_image_name('staff of the death magus'/'M15', 'staff of the death magus').
card_uid('staff of the death magus'/'M15', 'M15:Staff of the Death Magus:staff of the death magus').
card_rarity('staff of the death magus'/'M15', 'Uncommon').
card_artist('staff of the death magus'/'M15', 'Daniel Ljunggren').
card_number('staff of the death magus'/'M15', '232').
card_flavor_text('staff of the death magus'/'M15', 'A symbol of ambition in ruthless times.').
card_multiverse_id('staff of the death magus'/'M15', '383397').

card_in_set('staff of the flame magus', 'M15').
card_original_type('staff of the flame magus'/'M15', 'Artifact').
card_original_text('staff of the flame magus'/'M15', 'Whenever you cast a red spell or a Mountain enters the battlefield under your control, you gain 1 life.').
card_image_name('staff of the flame magus'/'M15', 'staff of the flame magus').
card_uid('staff of the flame magus'/'M15', 'M15:Staff of the Flame Magus:staff of the flame magus').
card_rarity('staff of the flame magus'/'M15', 'Uncommon').
card_artist('staff of the flame magus'/'M15', 'Daniel Ljunggren').
card_number('staff of the flame magus'/'M15', '233').
card_flavor_text('staff of the flame magus'/'M15', 'A symbol of passion in indifferent times.').
card_multiverse_id('staff of the flame magus'/'M15', '383398').

card_in_set('staff of the mind magus', 'M15').
card_original_type('staff of the mind magus'/'M15', 'Artifact').
card_original_text('staff of the mind magus'/'M15', 'Whenever you cast a blue spell or an Island enters the battlefield under your control, you gain 1 life.').
card_image_name('staff of the mind magus'/'M15', 'staff of the mind magus').
card_uid('staff of the mind magus'/'M15', 'M15:Staff of the Mind Magus:staff of the mind magus').
card_rarity('staff of the mind magus'/'M15', 'Uncommon').
card_artist('staff of the mind magus'/'M15', 'Daniel Ljunggren').
card_number('staff of the mind magus'/'M15', '234').
card_flavor_text('staff of the mind magus'/'M15', 'A symbol of sagacity in bewildering times.').
card_multiverse_id('staff of the mind magus'/'M15', '383399').

card_in_set('staff of the sun magus', 'M15').
card_original_type('staff of the sun magus'/'M15', 'Artifact').
card_original_text('staff of the sun magus'/'M15', 'Whenever you cast a white spell or a Plains enters the battlefield under your control, you gain 1 life.').
card_image_name('staff of the sun magus'/'M15', 'staff of the sun magus').
card_uid('staff of the sun magus'/'M15', 'M15:Staff of the Sun Magus:staff of the sun magus').
card_rarity('staff of the sun magus'/'M15', 'Uncommon').
card_artist('staff of the sun magus'/'M15', 'Daniel Ljunggren').
card_number('staff of the sun magus'/'M15', '235').
card_flavor_text('staff of the sun magus'/'M15', 'A symbol of conviction in uncertain times.').
card_multiverse_id('staff of the sun magus'/'M15', '383400').

card_in_set('staff of the wild magus', 'M15').
card_original_type('staff of the wild magus'/'M15', 'Artifact').
card_original_text('staff of the wild magus'/'M15', 'Whenever you cast a green spell or a Forest enters the battlefield under your control, you gain 1 life.').
card_image_name('staff of the wild magus'/'M15', 'staff of the wild magus').
card_uid('staff of the wild magus'/'M15', 'M15:Staff of the Wild Magus:staff of the wild magus').
card_rarity('staff of the wild magus'/'M15', 'Uncommon').
card_artist('staff of the wild magus'/'M15', 'Daniel Ljunggren').
card_number('staff of the wild magus'/'M15', '236').
card_flavor_text('staff of the wild magus'/'M15', 'A symbol of ferocity in oppressive times.').
card_multiverse_id('staff of the wild magus'/'M15', '383401').

card_in_set('stain the mind', 'M15').
card_original_type('stain the mind'/'M15', 'Sorcery').
card_original_text('stain the mind'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nName a nonland card. Search target player\'s graveyard, hand, and library for any number of cards with that name and exile them. Then that player shuffles his or her library.').
card_first_print('stain the mind', 'M15').
card_image_name('stain the mind'/'M15', 'stain the mind').
card_uid('stain the mind'/'M15', 'M15:Stain the Mind:stain the mind').
card_rarity('stain the mind'/'M15', 'Rare').
card_artist('stain the mind'/'M15', 'Jason Rainville').
card_number('stain the mind'/'M15', '117').
card_multiverse_id('stain the mind'/'M15', '383402').

card_in_set('statute of denial', 'M15').
card_original_type('statute of denial'/'M15', 'Instant').
card_original_text('statute of denial'/'M15', 'Counter target spell. If you control a blue creature, draw a card, then discard a card.').
card_first_print('statute of denial', 'M15').
card_image_name('statute of denial'/'M15', 'statute of denial').
card_uid('statute of denial'/'M15', 'M15:Statute of Denial:statute of denial').
card_rarity('statute of denial'/'M15', 'Common').
card_artist('statute of denial'/'M15', 'Zoltan Boros').
card_number('statute of denial'/'M15', '79').
card_flavor_text('statute of denial'/'M15', '\"Pyrotechnic activity of any sort is strictly prohibited. It is irrelevant that today is a holiday.\"').
card_multiverse_id('statute of denial'/'M15', '383403').

card_in_set('stoke the flames', 'M15').
card_original_type('stoke the flames'/'M15', 'Instant').
card_original_text('stoke the flames'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nStoke the Flames deals 4 damage to target creature or player.').
card_image_name('stoke the flames'/'M15', 'stoke the flames').
card_uid('stoke the flames'/'M15', 'M15:Stoke the Flames:stoke the flames').
card_rarity('stoke the flames'/'M15', 'Uncommon').
card_artist('stoke the flames'/'M15', 'Ryan Barger').
card_number('stoke the flames'/'M15', '164').
card_multiverse_id('stoke the flames'/'M15', '383404').

card_in_set('stormtide leviathan', 'M15').
card_original_type('stormtide leviathan'/'M15', 'Creature — Leviathan').
card_original_text('stormtide leviathan'/'M15', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nAll lands are Islands in addition to their other types.\nCreatures without flying or islandwalk can\'t attack.').
card_image_name('stormtide leviathan'/'M15', 'stormtide leviathan').
card_uid('stormtide leviathan'/'M15', 'M15:Stormtide Leviathan:stormtide leviathan').
card_rarity('stormtide leviathan'/'M15', 'Rare').
card_artist('stormtide leviathan'/'M15', 'Karl Kopinski').
card_number('stormtide leviathan'/'M15', '80').
card_multiverse_id('stormtide leviathan'/'M15', '383405').

card_in_set('sunblade elf', 'M15').
card_original_type('sunblade elf'/'M15', 'Creature — Elf Warrior').
card_original_text('sunblade elf'/'M15', 'Sunblade Elf gets +1/+1 as long as you control a Plains.\n{4}{W}: Creatures you control get +1/+1 until end of turn.').
card_first_print('sunblade elf', 'M15').
card_image_name('sunblade elf'/'M15', 'sunblade elf').
card_uid('sunblade elf'/'M15', 'M15:Sunblade Elf:sunblade elf').
card_rarity('sunblade elf'/'M15', 'Uncommon').
card_artist('sunblade elf'/'M15', 'Lucas Graciano').
card_number('sunblade elf'/'M15', '202').
card_flavor_text('sunblade elf'/'M15', '\"We patrol the steppes to keep enemies from the forest verge.\"').
card_multiverse_id('sunblade elf'/'M15', '383406').

card_in_set('sungrace pegasus', 'M15').
card_original_type('sungrace pegasus'/'M15', 'Creature — Pegasus').
card_original_text('sungrace pegasus'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('sungrace pegasus', 'M15').
card_image_name('sungrace pegasus'/'M15', 'sungrace pegasus').
card_uid('sungrace pegasus'/'M15', 'M15:Sungrace Pegasus:sungrace pegasus').
card_rarity('sungrace pegasus'/'M15', 'Common').
card_artist('sungrace pegasus'/'M15', 'Phill Simmer').
card_number('sungrace pegasus'/'M15', '38').
card_flavor_text('sungrace pegasus'/'M15', 'The sacred feathers of the pegasus are said to have healing powers.').
card_multiverse_id('sungrace pegasus'/'M15', '383407').

card_in_set('swamp', 'M15').
card_original_type('swamp'/'M15', 'Basic Land — Swamp').
card_original_text('swamp'/'M15', 'B').
card_image_name('swamp'/'M15', 'swamp1').
card_uid('swamp'/'M15', 'M15:Swamp:swamp1').
card_rarity('swamp'/'M15', 'Basic Land').
card_artist('swamp'/'M15', 'John Avon').
card_number('swamp'/'M15', '258').
card_multiverse_id('swamp'/'M15', '383409').

card_in_set('swamp', 'M15').
card_original_type('swamp'/'M15', 'Basic Land — Swamp').
card_original_text('swamp'/'M15', 'B').
card_image_name('swamp'/'M15', 'swamp2').
card_uid('swamp'/'M15', 'M15:Swamp:swamp2').
card_rarity('swamp'/'M15', 'Basic Land').
card_artist('swamp'/'M15', 'Cliff Childs').
card_number('swamp'/'M15', '259').
card_multiverse_id('swamp'/'M15', '383410').

card_in_set('swamp', 'M15').
card_original_type('swamp'/'M15', 'Basic Land — Swamp').
card_original_text('swamp'/'M15', 'B').
card_image_name('swamp'/'M15', 'swamp3').
card_uid('swamp'/'M15', 'M15:Swamp:swamp3').
card_rarity('swamp'/'M15', 'Basic Land').
card_artist('swamp'/'M15', 'Jonas De Ro').
card_number('swamp'/'M15', '260').
card_multiverse_id('swamp'/'M15', '383411').

card_in_set('swamp', 'M15').
card_original_type('swamp'/'M15', 'Basic Land — Swamp').
card_original_text('swamp'/'M15', 'B').
card_image_name('swamp'/'M15', 'swamp4').
card_uid('swamp'/'M15', 'M15:Swamp:swamp4').
card_rarity('swamp'/'M15', 'Basic Land').
card_artist('swamp'/'M15', 'Jung Park').
card_number('swamp'/'M15', '261').
card_multiverse_id('swamp'/'M15', '383408').

card_in_set('terra stomper', 'M15').
card_original_type('terra stomper'/'M15', 'Creature — Beast').
card_original_text('terra stomper'/'M15', 'Terra Stomper can\'t be countered.\nTrample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('terra stomper'/'M15', 'terra stomper').
card_uid('terra stomper'/'M15', 'M15:Terra Stomper:terra stomper').
card_rarity('terra stomper'/'M15', 'Rare').
card_artist('terra stomper'/'M15', 'Goran Josic').
card_number('terra stomper'/'M15', '284').
card_flavor_text('terra stomper'/'M15', 'Its footfalls cause violent earthquakes, hurtling boulders, and unseasonable dust storms.').
card_multiverse_id('terra stomper'/'M15', '383173').

card_in_set('the chain veil', 'M15').
card_original_type('the chain veil'/'M15', 'Legendary Artifact').
card_original_text('the chain veil'/'M15', 'At the beginning of your end step, if you didn\'t activate a loyalty ability of a planeswalker this turn, you lose 2 life.\n{4}, {T}: For each planeswalker you control, you may activate one of its loyalty abilities once this turn as though none of its loyalty abilities have been activated this turn.').
card_first_print('the chain veil', 'M15').
card_image_name('the chain veil'/'M15', 'the chain veil').
card_uid('the chain veil'/'M15', 'M15:The Chain Veil:the chain veil').
card_rarity('the chain veil'/'M15', 'Mythic Rare').
card_artist('the chain veil'/'M15', 'Volkan Baga').
card_number('the chain veil'/'M15', '215').
card_multiverse_id('the chain veil'/'M15', '383412').

card_in_set('thundering giant', 'M15').
card_original_type('thundering giant'/'M15', 'Creature — Giant').
card_original_text('thundering giant'/'M15', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('thundering giant'/'M15', 'thundering giant').
card_uid('thundering giant'/'M15', 'M15:Thundering Giant:thundering giant').
card_rarity('thundering giant'/'M15', 'Common').
card_artist('thundering giant'/'M15', 'Mark Zug').
card_number('thundering giant'/'M15', '165').
card_flavor_text('thundering giant'/'M15', 'The giant was felt a few seconds before he was seen.').
card_multiverse_id('thundering giant'/'M15', '383413').

card_in_set('tireless missionaries', 'M15').
card_original_type('tireless missionaries'/'M15', 'Creature — Human Cleric').
card_original_text('tireless missionaries'/'M15', 'When Tireless Missionaries enters the battlefield, you gain 3 life.').
card_image_name('tireless missionaries'/'M15', 'tireless missionaries').
card_uid('tireless missionaries'/'M15', 'M15:Tireless Missionaries:tireless missionaries').
card_rarity('tireless missionaries'/'M15', 'Common').
card_artist('tireless missionaries'/'M15', 'Dave Kendall').
card_number('tireless missionaries'/'M15', '39').
card_flavor_text('tireless missionaries'/'M15', 'If they succeed in their holy work, their order will vanish into welcome obscurity, for there will be no more souls to redeem.').
card_multiverse_id('tireless missionaries'/'M15', '383414').

card_in_set('titanic growth', 'M15').
card_original_type('titanic growth'/'M15', 'Instant').
card_original_text('titanic growth'/'M15', 'Target creature gets +4/+4 until end of turn.').
card_image_name('titanic growth'/'M15', 'titanic growth').
card_uid('titanic growth'/'M15', 'M15:Titanic Growth:titanic growth').
card_rarity('titanic growth'/'M15', 'Common').
card_artist('titanic growth'/'M15', 'Ryan Pancoast').
card_number('titanic growth'/'M15', '203').
card_flavor_text('titanic growth'/'M15', 'The massive dominate through might. The tiny survive with guile. Beware the tiny who become massive.').
card_multiverse_id('titanic growth'/'M15', '383415').

card_in_set('torch fiend', 'M15').
card_original_type('torch fiend'/'M15', 'Creature — Devil').
card_original_text('torch fiend'/'M15', '{R}, Sacrifice Torch Fiend: Destroy target artifact.').
card_image_name('torch fiend'/'M15', 'torch fiend').
card_uid('torch fiend'/'M15', 'M15:Torch Fiend:torch fiend').
card_rarity('torch fiend'/'M15', 'Common').
card_artist('torch fiend'/'M15', 'Winona Nelson').
card_number('torch fiend'/'M15', '166').
card_flavor_text('torch fiend'/'M15', 'Devils redecorate every room with fire.').
card_multiverse_id('torch fiend'/'M15', '383416').

card_in_set('tormod\'s crypt', 'M15').
card_original_type('tormod\'s crypt'/'M15', 'Artifact').
card_original_text('tormod\'s crypt'/'M15', '{T}, Sacrifice Tormod\'s Crypt: Exile all cards from target player\'s graveyard.').
card_image_name('tormod\'s crypt'/'M15', 'tormod\'s crypt').
card_uid('tormod\'s crypt'/'M15', 'M15:Tormod\'s Crypt:tormod\'s crypt').
card_rarity('tormod\'s crypt'/'M15', 'Uncommon').
card_artist('tormod\'s crypt'/'M15', 'Lars Grant-West').
card_number('tormod\'s crypt'/'M15', '237').
card_flavor_text('tormod\'s crypt'/'M15', 'Dominaria\'s most extravagant crypt nevertheless holds an empty grave.').
card_multiverse_id('tormod\'s crypt'/'M15', '383417').

card_in_set('triplicate spirits', 'M15').
card_original_type('triplicate spirits'/'M15', 'Sorcery').
card_original_text('triplicate spirits'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPut three 1/1 white Spirit creature tokens with flying onto the battlefield. (They can\'t be blocked except by creatures with flying or reach.)').
card_first_print('triplicate spirits', 'M15').
card_image_name('triplicate spirits'/'M15', 'triplicate spirits').
card_uid('triplicate spirits'/'M15', 'M15:Triplicate Spirits:triplicate spirits').
card_rarity('triplicate spirits'/'M15', 'Common').
card_artist('triplicate spirits'/'M15', 'Izzy').
card_number('triplicate spirits'/'M15', '40').
card_multiverse_id('triplicate spirits'/'M15', '383418').

card_in_set('turn to frog', 'M15').
card_original_type('turn to frog'/'M15', 'Instant').
card_original_text('turn to frog'/'M15', 'Until end of turn, target creature loses all abilities and becomes a blue Frog with base power and toughness 1/1.').
card_image_name('turn to frog'/'M15', 'turn to frog').
card_uid('turn to frog'/'M15', 'M15:Turn to Frog:turn to frog').
card_rarity('turn to frog'/'M15', 'Uncommon').
card_artist('turn to frog'/'M15', 'Warren Mahy').
card_number('turn to frog'/'M15', '81').
card_flavor_text('turn to frog'/'M15', '\"Ribbit.\"').
card_multiverse_id('turn to frog'/'M15', '383419').

card_in_set('typhoid rats', 'M15').
card_original_type('typhoid rats'/'M15', 'Creature — Rat').
card_original_text('typhoid rats'/'M15', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('typhoid rats'/'M15', 'typhoid rats').
card_uid('typhoid rats'/'M15', 'M15:Typhoid Rats:typhoid rats').
card_rarity('typhoid rats'/'M15', 'Common').
card_artist('typhoid rats'/'M15', 'Kev Walker').
card_number('typhoid rats'/'M15', '118').
card_flavor_text('typhoid rats'/'M15', 'Kidnappers caught in Havengul are given two choices: languish in prison or become rat catchers. The smart ones go to prison.').
card_multiverse_id('typhoid rats'/'M15', '383420').

card_in_set('tyrant\'s machine', 'M15').
card_original_type('tyrant\'s machine'/'M15', 'Artifact').
card_original_text('tyrant\'s machine'/'M15', '{4}, {T}: Tap target creature.').
card_first_print('tyrant\'s machine', 'M15').
card_image_name('tyrant\'s machine'/'M15', 'tyrant\'s machine').
card_uid('tyrant\'s machine'/'M15', 'M15:Tyrant\'s Machine:tyrant\'s machine').
card_rarity('tyrant\'s machine'/'M15', 'Common').
card_artist('tyrant\'s machine'/'M15', 'Yeong-Hao Han').
card_number('tyrant\'s machine'/'M15', '238').
card_flavor_text('tyrant\'s machine'/'M15', '\"Though tempered differently, all wills can be broken.\"\n—Inquisitor Kyrik').
card_multiverse_id('tyrant\'s machine'/'M15', '383421').

card_in_set('ulcerate', 'M15').
card_original_type('ulcerate'/'M15', 'Instant').
card_original_text('ulcerate'/'M15', 'Target creature gets -3/-3 until end of turn. You lose 3 life.').
card_first_print('ulcerate', 'M15').
card_image_name('ulcerate'/'M15', 'ulcerate').
card_uid('ulcerate'/'M15', 'M15:Ulcerate:ulcerate').
card_rarity('ulcerate'/'M15', 'Uncommon').
card_artist('ulcerate'/'M15', 'Johann Bodin').
card_number('ulcerate'/'M15', '119').
card_flavor_text('ulcerate'/'M15', '\"If it were merely lethal, that would be sufficient. The art, however, is in maximizing the suffering it causes.\"\n—Liliana Vess').
card_multiverse_id('ulcerate'/'M15', '383422').

card_in_set('undergrowth scavenger', 'M15').
card_original_type('undergrowth scavenger'/'M15', 'Creature — Fungus Horror').
card_original_text('undergrowth scavenger'/'M15', 'Undergrowth Scavenger enters the battlefield with a number of +1/+1 counters on it equal to the number of creature cards in all graveyards.').
card_first_print('undergrowth scavenger', 'M15').
card_image_name('undergrowth scavenger'/'M15', 'undergrowth scavenger').
card_uid('undergrowth scavenger'/'M15', 'M15:Undergrowth Scavenger:undergrowth scavenger').
card_rarity('undergrowth scavenger'/'M15', 'Common').
card_artist('undergrowth scavenger'/'M15', 'Nils Hamm').
card_number('undergrowth scavenger'/'M15', '204').
card_flavor_text('undergrowth scavenger'/'M15', 'It sees a rotting carcass as a good wine which has been aged properly.').
card_multiverse_id('undergrowth scavenger'/'M15', '383423').

card_in_set('unmake the graves', 'M15').
card_original_type('unmake the graves'/'M15', 'Instant').
card_original_text('unmake the graves'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nReturn up to two target creature cards from your graveyard to your hand.').
card_first_print('unmake the graves', 'M15').
card_image_name('unmake the graves'/'M15', 'unmake the graves').
card_uid('unmake the graves'/'M15', 'M15:Unmake the Graves:unmake the graves').
card_rarity('unmake the graves'/'M15', 'Common').
card_artist('unmake the graves'/'M15', 'Aaron Miller').
card_number('unmake the graves'/'M15', '120').
card_flavor_text('unmake the graves'/'M15', '\"I\'m raising an army. Any volunteers?\"').
card_multiverse_id('unmake the graves'/'M15', '383424').

card_in_set('urborg, tomb of yawgmoth', 'M15').
card_original_type('urborg, tomb of yawgmoth'/'M15', 'Legendary Land').
card_original_text('urborg, tomb of yawgmoth'/'M15', 'Each land is a Swamp in addition to its other land types.').
card_image_name('urborg, tomb of yawgmoth'/'M15', 'urborg, tomb of yawgmoth').
card_uid('urborg, tomb of yawgmoth'/'M15', 'M15:Urborg, Tomb of Yawgmoth:urborg, tomb of yawgmoth').
card_rarity('urborg, tomb of yawgmoth'/'M15', 'Rare').
card_artist('urborg, tomb of yawgmoth'/'M15', 'John Avon').
card_number('urborg, tomb of yawgmoth'/'M15', '248').
card_flavor_text('urborg, tomb of yawgmoth'/'M15', '\"Yawgmoth\'s corpse is a wound in the universe. His foul blood seeps out, infecting the land with his final curse.\"\n—Lord Windgrace').
card_multiverse_id('urborg, tomb of yawgmoth'/'M15', '383425').

card_in_set('venom sliver', 'M15').
card_original_type('venom sliver'/'M15', 'Creature — Sliver').
card_original_text('venom sliver'/'M15', 'Sliver creatures you control have deathtouch. (Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_first_print('venom sliver', 'M15').
card_image_name('venom sliver'/'M15', 'venom sliver').
card_uid('venom sliver'/'M15', 'M15:Venom Sliver:venom sliver').
card_rarity('venom sliver'/'M15', 'Uncommon').
card_artist('venom sliver'/'M15', 'Dave Kendall').
card_number('venom sliver'/'M15', '205').
card_flavor_text('venom sliver'/'M15', '\"We attacked with arrows dipped in poison. The slivers that did not die began to change.\"\n—Hastric, Thunian scout').
card_multiverse_id('venom sliver'/'M15', '383426').

card_in_set('verdant haven', 'M15').
card_original_type('verdant haven'/'M15', 'Enchantment — Aura').
card_original_text('verdant haven'/'M15', 'Enchant land\nWhen Verdant Haven enters the battlefield, you gain 2 life.\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_image_name('verdant haven'/'M15', 'verdant haven').
card_uid('verdant haven'/'M15', 'M15:Verdant Haven:verdant haven').
card_rarity('verdant haven'/'M15', 'Common').
card_artist('verdant haven'/'M15', 'Daniel Ljunggren').
card_number('verdant haven'/'M15', '206').
card_multiverse_id('verdant haven'/'M15', '383427').

card_in_set('vineweft', 'M15').
card_original_type('vineweft'/'M15', 'Enchantment — Aura').
card_original_text('vineweft'/'M15', 'Enchant creature\nEnchanted creature gets +1/+1.\n{4}{G}: Return Vineweft from your graveyard to your hand.').
card_first_print('vineweft', 'M15').
card_image_name('vineweft'/'M15', 'vineweft').
card_uid('vineweft'/'M15', 'M15:Vineweft:vineweft').
card_rarity('vineweft'/'M15', 'Common').
card_artist('vineweft'/'M15', 'Lucas Graciano').
card_number('vineweft'/'M15', '207').
card_flavor_text('vineweft'/'M15', 'Fortified by the wilds.').
card_multiverse_id('vineweft'/'M15', '383428').

card_in_set('void snare', 'M15').
card_original_type('void snare'/'M15', 'Sorcery').
card_original_text('void snare'/'M15', 'Return target nonland permanent to its owner\'s hand.').
card_first_print('void snare', 'M15').
card_image_name('void snare'/'M15', 'void snare').
card_uid('void snare'/'M15', 'M15:Void Snare:void snare').
card_rarity('void snare'/'M15', 'Common').
card_artist('void snare'/'M15', 'Zack Stella').
card_number('void snare'/'M15', '82').
card_flavor_text('void snare'/'M15', '\"I\'ve tried so many variations on how to get rid of annoying things that it\'s hard to decide which one I like best.\"\n—Ashurel, voidmage').
card_multiverse_id('void snare'/'M15', '383429').

card_in_set('walking corpse', 'M15').
card_original_type('walking corpse'/'M15', 'Creature — Zombie').
card_original_text('walking corpse'/'M15', '').
card_image_name('walking corpse'/'M15', 'walking corpse').
card_uid('walking corpse'/'M15', 'M15:Walking Corpse:walking corpse').
card_rarity('walking corpse'/'M15', 'Common').
card_artist('walking corpse'/'M15', 'Igor Kieryluk').
card_number('walking corpse'/'M15', '278').
card_flavor_text('walking corpse'/'M15', '\"Feeding a normal army is a problem of logistics. With zombies, it is an asset. Feeding is why they fight. Feeding is why they are feared.\"\n—Jadar, ghoulcaller of Nephalia').
card_multiverse_id('walking corpse'/'M15', '383174').

card_in_set('wall of essence', 'M15').
card_original_type('wall of essence'/'M15', 'Creature — Wall').
card_original_text('wall of essence'/'M15', 'Defender (This creature can\'t attack.)\nWhenever Wall of Essence is dealt combat damage, you gain that much life.').
card_image_name('wall of essence'/'M15', 'wall of essence').
card_uid('wall of essence'/'M15', 'M15:Wall of Essence:wall of essence').
card_rarity('wall of essence'/'M15', 'Uncommon').
card_artist('wall of essence'/'M15', 'Adam Rex').
card_number('wall of essence'/'M15', '41').
card_flavor_text('wall of essence'/'M15', '\"If you cannot turn your enemy\'s strength to weakness, then make that strength your own.\"\n—Gresha, warrior sage').
card_multiverse_id('wall of essence'/'M15', '383430').

card_in_set('wall of fire', 'M15').
card_original_type('wall of fire'/'M15', 'Creature — Wall').
card_original_text('wall of fire'/'M15', 'Defender (This creature can\'t attack.)\n{R}: Wall of Fire gets +1/+0 until end of turn.').
card_image_name('wall of fire'/'M15', 'wall of fire').
card_uid('wall of fire'/'M15', 'M15:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'M15', 'Common').
card_artist('wall of fire'/'M15', 'Dan Dos Santos').
card_number('wall of fire'/'M15', '167').
card_flavor_text('wall of fire'/'M15', 'Mercy is for those who keep their distance.').
card_multiverse_id('wall of fire'/'M15', '383431').

card_in_set('wall of frost', 'M15').
card_original_type('wall of frost'/'M15', 'Creature — Wall').
card_original_text('wall of frost'/'M15', 'Defender (This creature can\'t attack.)\nWhenever Wall of Frost blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('wall of frost'/'M15', 'wall of frost').
card_uid('wall of frost'/'M15', 'M15:Wall of Frost:wall of frost').
card_rarity('wall of frost'/'M15', 'Uncommon').
card_artist('wall of frost'/'M15', 'Mike Bierek').
card_number('wall of frost'/'M15', '83').
card_flavor_text('wall of frost'/'M15', '\"I have seen countless petty warmongers gaze on it for a time before turning away.\"\n—Sarlena, paladin of the Northern Verge').
card_multiverse_id('wall of frost'/'M15', '383432').

card_in_set('wall of limbs', 'M15').
card_original_type('wall of limbs'/'M15', 'Creature — Zombie Wall').
card_original_text('wall of limbs'/'M15', 'Defender (This creature can\'t attack.)\nWhenever you gain life, put a +1/+1 counter on Wall of Limbs.\n{5}{B}{B}, Sacrifice Wall of Limbs: Target player loses X life, where X is Wall of Limbs\'s power.').
card_first_print('wall of limbs', 'M15').
card_image_name('wall of limbs'/'M15', 'wall of limbs').
card_uid('wall of limbs'/'M15', 'M15:Wall of Limbs:wall of limbs').
card_rarity('wall of limbs'/'M15', 'Uncommon').
card_artist('wall of limbs'/'M15', 'Yeong-Hao Han').
card_number('wall of limbs'/'M15', '121').
card_multiverse_id('wall of limbs'/'M15', '383433').

card_in_set('wall of mulch', 'M15').
card_original_type('wall of mulch'/'M15', 'Creature — Wall').
card_original_text('wall of mulch'/'M15', 'Defender (This creature can\'t attack.)\n{G}, Sacrifice a Wall: Draw a card.').
card_image_name('wall of mulch'/'M15', 'wall of mulch').
card_uid('wall of mulch'/'M15', 'M15:Wall of Mulch:wall of mulch').
card_rarity('wall of mulch'/'M15', 'Uncommon').
card_artist('wall of mulch'/'M15', 'Anthony S. Waters').
card_number('wall of mulch'/'M15', '208').
card_flavor_text('wall of mulch'/'M15', 'Mulch is the fabric of life in the forest. Plants live in it, they die in it, and then they become part of it, feeding countless generations to come.').
card_multiverse_id('wall of mulch'/'M15', '383434').

card_in_set('warden of the beyond', 'M15').
card_original_type('warden of the beyond'/'M15', 'Creature — Human Wizard').
card_original_text('warden of the beyond'/'M15', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nWarden of the Beyond gets +2/+2 as long as an opponent owns a card in exile.').
card_first_print('warden of the beyond', 'M15').
card_image_name('warden of the beyond'/'M15', 'warden of the beyond').
card_uid('warden of the beyond'/'M15', 'M15:Warden of the Beyond:warden of the beyond').
card_rarity('warden of the beyond'/'M15', 'Uncommon').
card_artist('warden of the beyond'/'M15', 'Raymond Swanland').
card_number('warden of the beyond'/'M15', '42').
card_flavor_text('warden of the beyond'/'M15', 'He draws strength from a vast source few mortals can fathom.\nDesigned by Isaiah Cartwright').
card_multiverse_id('warden of the beyond'/'M15', '383435').

card_in_set('waste not', 'M15').
card_original_type('waste not'/'M15', 'Enchantment').
card_original_text('waste not'/'M15', 'Whenever an opponent discards a creature card, put a 2/2 black Zombie creature token onto the battlefield.\nWhenever an opponent discards a land card, add {B}{B} to your mana pool.\nWhenever an opponent discards a noncreature, nonland card, draw a card.').
card_first_print('waste not', 'M15').
card_image_name('waste not'/'M15', 'waste not').
card_uid('waste not'/'M15', 'M15:Waste Not:waste not').
card_rarity('waste not'/'M15', 'Rare').
card_artist('waste not'/'M15', 'Matt Stewart').
card_number('waste not'/'M15', '122').
card_flavor_text('waste not'/'M15', 'Designed by the Magic community').
card_multiverse_id('waste not'/'M15', '383436').

card_in_set('welkin tern', 'M15').
card_original_type('welkin tern'/'M15', 'Creature — Bird').
card_original_text('welkin tern'/'M15', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWelkin Tern can block only creatures with flying.').
card_image_name('welkin tern'/'M15', 'welkin tern').
card_uid('welkin tern'/'M15', 'M15:Welkin Tern:welkin tern').
card_rarity('welkin tern'/'M15', 'Common').
card_artist('welkin tern'/'M15', 'Austin Hsu').
card_number('welkin tern'/'M15', '84').
card_flavor_text('welkin tern'/'M15', 'Sailors have come to regard them as bad luck, for they falsely bring hope of land.').
card_multiverse_id('welkin tern'/'M15', '383437').

card_in_set('will-forged golem', 'M15').
card_original_type('will-forged golem'/'M15', 'Artifact Creature — Golem').
card_original_text('will-forged golem'/'M15', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)').
card_first_print('will-forged golem', 'M15').
card_image_name('will-forged golem'/'M15', 'will-forged golem').
card_uid('will-forged golem'/'M15', 'M15:Will-Forged Golem:will-forged golem').
card_rarity('will-forged golem'/'M15', 'Common').
card_artist('will-forged golem'/'M15', 'Jason Felix').
card_number('will-forged golem'/'M15', '239').
card_flavor_text('will-forged golem'/'M15', 'The modular nature of the automaton\'s design makes assembly perfectly intuitive.').
card_multiverse_id('will-forged golem'/'M15', '383438').

card_in_set('witch\'s familiar', 'M15').
card_original_type('witch\'s familiar'/'M15', 'Creature — Frog').
card_original_text('witch\'s familiar'/'M15', '').
card_first_print('witch\'s familiar', 'M15').
card_image_name('witch\'s familiar'/'M15', 'witch\'s familiar').
card_uid('witch\'s familiar'/'M15', 'M15:Witch\'s Familiar:witch\'s familiar').
card_rarity('witch\'s familiar'/'M15', 'Common').
card_artist('witch\'s familiar'/'M15', 'Jack Wang').
card_number('witch\'s familiar'/'M15', '123').
card_flavor_text('witch\'s familiar'/'M15', 'Some bog witches practice the strange art of batrachomancy, reading portents in the number, size, and color of warts on a toad\'s hide.').
card_multiverse_id('witch\'s familiar'/'M15', '383439').

card_in_set('xathrid slyblade', 'M15').
card_original_type('xathrid slyblade'/'M15', 'Creature — Human Assassin').
card_original_text('xathrid slyblade'/'M15', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\n{3}{B}: Until end of turn, Xathrid Slyblade loses hexproof and gains first strike and deathtouch. (It deals combat damage before creatures without first strike. Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('xathrid slyblade', 'M15').
card_image_name('xathrid slyblade'/'M15', 'xathrid slyblade').
card_uid('xathrid slyblade'/'M15', 'M15:Xathrid Slyblade:xathrid slyblade').
card_rarity('xathrid slyblade'/'M15', 'Uncommon').
card_artist('xathrid slyblade'/'M15', 'Steve Prescott').
card_number('xathrid slyblade'/'M15', '124').
card_flavor_text('xathrid slyblade'/'M15', 'Designed by Rob Pardo').
card_multiverse_id('xathrid slyblade'/'M15', '383440').

card_in_set('yavimaya coast', 'M15').
card_original_type('yavimaya coast'/'M15', 'Land').
card_original_text('yavimaya coast'/'M15', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {U} to your mana pool. Yavimaya Coast deals 1 damage to you.').
card_image_name('yavimaya coast'/'M15', 'yavimaya coast').
card_uid('yavimaya coast'/'M15', 'M15:Yavimaya Coast:yavimaya coast').
card_rarity('yavimaya coast'/'M15', 'Rare').
card_artist('yavimaya coast'/'M15', 'Anthony S. Waters').
card_number('yavimaya coast'/'M15', '249').
card_multiverse_id('yavimaya coast'/'M15', '383441').

card_in_set('yisan, the wanderer bard', 'M15').
card_original_type('yisan, the wanderer bard'/'M15', 'Legendary Creature — Human Rogue').
card_original_text('yisan, the wanderer bard'/'M15', '{2}{G}, {T}, Put a verse counter on Yisan, the Wanderer Bard: Search your library for a creature card with converted mana cost equal to the number of verse counters on Yisan, put it onto the battlefield, then shuffle your library.').
card_first_print('yisan, the wanderer bard', 'M15').
card_image_name('yisan, the wanderer bard'/'M15', 'yisan, the wanderer bard').
card_uid('yisan, the wanderer bard'/'M15', 'M15:Yisan, the Wanderer Bard:yisan, the wanderer bard').
card_rarity('yisan, the wanderer bard'/'M15', 'Rare').
card_artist('yisan, the wanderer bard'/'M15', 'Chase Stone').
card_number('yisan, the wanderer bard'/'M15', '209').
card_flavor_text('yisan, the wanderer bard'/'M15', 'Designed by Brian Fargo').
card_multiverse_id('yisan, the wanderer bard'/'M15', '383442').

card_in_set('zof shade', 'M15').
card_original_type('zof shade'/'M15', 'Creature — Shade').
card_original_text('zof shade'/'M15', '{2}{B}: Zof Shade gets +2/+2 until end of turn.').
card_image_name('zof shade'/'M15', 'zof shade').
card_uid('zof shade'/'M15', 'M15:Zof Shade:zof shade').
card_rarity('zof shade'/'M15', 'Common').
card_artist('zof shade'/'M15', 'Jason A. Engle').
card_number('zof shade'/'M15', '125').
card_flavor_text('zof shade'/'M15', 'Shades are drawn to places of power, often rooting themselves in a single area to feed.').
card_multiverse_id('zof shade'/'M15', '383443').
