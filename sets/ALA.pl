% Shards of Alara

set('ALA').
set_name('ALA', 'Shards of Alara').
set_release_date('ALA', '2008-10-03').
set_border('ALA', 'black').
set_type('ALA', 'expansion').
set_block('ALA', 'Alara').

card_in_set('ad nauseam', 'ALA').
card_original_type('ad nauseam'/'ALA', 'Instant').
card_original_text('ad nauseam'/'ALA', 'Reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost. You may repeat this process any number of times.').
card_first_print('ad nauseam', 'ALA').
card_image_name('ad nauseam'/'ALA', 'ad nauseam').
card_uid('ad nauseam'/'ALA', 'ALA:Ad Nauseam:ad nauseam').
card_rarity('ad nauseam'/'ALA', 'Rare').
card_artist('ad nauseam'/'ALA', 'Jeremy Jarvis').
card_number('ad nauseam'/'ALA', '63').
card_flavor_text('ad nauseam'/'ALA', 'When the task spilled over into undeath, he stopped calling it his life\'s work.').
card_multiverse_id('ad nauseam'/'ALA', '174915').

card_in_set('agony warp', 'ALA').
card_original_type('agony warp'/'ALA', 'Instant').
card_original_text('agony warp'/'ALA', 'Target creature gets -3/-0 until end of turn.\nTarget creature gets -0/-3 until end of turn.').
card_first_print('agony warp', 'ALA').
card_image_name('agony warp'/'ALA', 'agony warp').
card_uid('agony warp'/'ALA', 'ALA:Agony Warp:agony warp').
card_rarity('agony warp'/'ALA', 'Common').
card_artist('agony warp'/'ALA', 'Dave Allsop').
card_number('agony warp'/'ALA', '153').
card_flavor_text('agony warp'/'ALA', 'Life\'s circle has become inverted in Grixis. The same energy is endlessly recycled and becomes more stagnant with each pass.').
card_multiverse_id('agony warp'/'ALA', '175052').

card_in_set('ajani vengeant', 'ALA').
card_original_type('ajani vengeant'/'ALA', 'Planeswalker — Ajani').
card_original_text('ajani vengeant'/'ALA', '+1: Target permanent doesn\'t untap during its controller\'s next untap step.\n-2: Ajani Vengeant deals 3 damage to target creature or player and you gain 3 life.\n-7: Destroy all lands target player controls.').
card_image_name('ajani vengeant'/'ALA', 'ajani vengeant').
card_uid('ajani vengeant'/'ALA', 'ALA:Ajani Vengeant:ajani vengeant').
card_rarity('ajani vengeant'/'ALA', 'Mythic Rare').
card_artist('ajani vengeant'/'ALA', 'Wayne Reynolds').
card_number('ajani vengeant'/'ALA', '154').
card_multiverse_id('ajani vengeant'/'ALA', '174852').

card_in_set('akrasan squire', 'ALA').
card_original_type('akrasan squire'/'ALA', 'Creature — Human Soldier').
card_original_text('akrasan squire'/'ALA', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('akrasan squire', 'ALA').
card_image_name('akrasan squire'/'ALA', 'akrasan squire').
card_uid('akrasan squire'/'ALA', 'ALA:Akrasan Squire:akrasan squire').
card_rarity('akrasan squire'/'ALA', 'Common').
card_artist('akrasan squire'/'ALA', 'Todd Lockwood').
card_number('akrasan squire'/'ALA', '1').
card_flavor_text('akrasan squire'/'ALA', 'Bant\'s armies are primarily composed of members of the Mortar caste, loyal commoners who haven\'t yet earned a sigil.').
card_multiverse_id('akrasan squire'/'ALA', '174963').

card_in_set('algae gharial', 'ALA').
card_original_type('algae gharial'/'ALA', 'Creature — Crocodile').
card_original_text('algae gharial'/'ALA', 'Shroud\nWhenever another creature is put into a graveyard from play, you may put a +1/+1 counter on Algae Gharial.').
card_first_print('algae gharial', 'ALA').
card_image_name('algae gharial'/'ALA', 'algae gharial').
card_uid('algae gharial'/'ALA', 'ALA:Algae Gharial:algae gharial').
card_rarity('algae gharial'/'ALA', 'Uncommon').
card_artist('algae gharial'/'ALA', 'Michael Ryan').
card_number('algae gharial'/'ALA', '123').
card_flavor_text('algae gharial'/'ALA', 'It lurks just under the surface, using the algae-choked tar pits of Jund as both home and hunting blind.').
card_multiverse_id('algae gharial'/'ALA', '174986').

card_in_set('angel\'s herald', 'ALA').
card_original_type('angel\'s herald'/'ALA', 'Creature — Human Cleric').
card_original_text('angel\'s herald'/'ALA', '{2}{W}, {T}, Sacrifice a green creature, a white creature, and a blue creature: Search your library for a card named Empyrial Archangel and put it into play. Then shuffle your library.').
card_first_print('angel\'s herald', 'ALA').
card_image_name('angel\'s herald'/'ALA', 'angel\'s herald').
card_uid('angel\'s herald'/'ALA', 'ALA:Angel\'s Herald:angel\'s herald').
card_rarity('angel\'s herald'/'ALA', 'Uncommon').
card_artist('angel\'s herald'/'ALA', 'Greg Staples').
card_number('angel\'s herald'/'ALA', '2').
card_flavor_text('angel\'s herald'/'ALA', 'Rigorous faith and belief are rewarded on occasion, and richly so.').
card_multiverse_id('angel\'s herald'/'ALA', '175141').

card_in_set('angelic benediction', 'ALA').
card_original_type('angelic benediction'/'ALA', 'Enchantment').
card_original_text('angelic benediction'/'ALA', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, you may tap target creature.').
card_first_print('angelic benediction', 'ALA').
card_image_name('angelic benediction'/'ALA', 'angelic benediction').
card_uid('angelic benediction'/'ALA', 'ALA:Angelic Benediction:angelic benediction').
card_rarity('angelic benediction'/'ALA', 'Uncommon').
card_artist('angelic benediction'/'ALA', 'Michael Komarck').
card_number('angelic benediction'/'ALA', '3').
card_flavor_text('angelic benediction'/'ALA', '\"Even in single combat, I am never alone.\"\n—Rafiq of the Many').
card_multiverse_id('angelic benediction'/'ALA', '174826').

card_in_set('angelsong', 'ALA').
card_original_type('angelsong'/'ALA', 'Instant').
card_original_text('angelsong'/'ALA', 'Prevent all combat damage that would be dealt this turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('angelsong', 'ALA').
card_image_name('angelsong'/'ALA', 'angelsong').
card_uid('angelsong'/'ALA', 'ALA:Angelsong:angelsong').
card_rarity('angelsong'/'ALA', 'Common').
card_artist('angelsong'/'ALA', 'Sal Villagran').
card_number('angelsong'/'ALA', '4').
card_flavor_text('angelsong'/'ALA', 'Clash of sword and cry of beast fall mute when angels sound the call to prayer.').
card_multiverse_id('angelsong'/'ALA', '175000').

card_in_set('arcane sanctum', 'ALA').
card_original_type('arcane sanctum'/'ALA', 'Land').
card_original_text('arcane sanctum'/'ALA', 'Arcane Sanctum comes into play tapped.\n{T}: Add {W}, {U}, or {B} to your mana pool.').
card_first_print('arcane sanctum', 'ALA').
card_image_name('arcane sanctum'/'ALA', 'arcane sanctum').
card_uid('arcane sanctum'/'ALA', 'ALA:Arcane Sanctum:arcane sanctum').
card_rarity('arcane sanctum'/'ALA', 'Uncommon').
card_artist('arcane sanctum'/'ALA', 'Anthony Francisco').
card_number('arcane sanctum'/'ALA', '220').
card_flavor_text('arcane sanctum'/'ALA', '\"We must rely on our own knowledge, not on the dogma of the seekers or the mutterings of the sphinxes.\"\n—Tullus of Palandius').
card_multiverse_id('arcane sanctum'/'ALA', '175128').

card_in_set('archdemon of unx', 'ALA').
card_original_type('archdemon of unx'/'ALA', 'Creature — Demon').
card_original_text('archdemon of unx'/'ALA', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a non-Zombie creature, then put a 2/2 black Zombie creature token into play.').
card_first_print('archdemon of unx', 'ALA').
card_image_name('archdemon of unx'/'ALA', 'archdemon of unx').
card_uid('archdemon of unx'/'ALA', 'ALA:Archdemon of Unx:archdemon of unx').
card_rarity('archdemon of unx'/'ALA', 'Rare').
card_artist('archdemon of unx'/'ALA', 'Dave Allsop').
card_number('archdemon of unx'/'ALA', '64').
card_flavor_text('archdemon of unx'/'ALA', 'The necropolis at Unx was once a living city, its streets untrodden by death.').
card_multiverse_id('archdemon of unx'/'ALA', '174847').

card_in_set('banewasp affliction', 'ALA').
card_original_type('banewasp affliction'/'ALA', 'Enchantment — Aura').
card_original_text('banewasp affliction'/'ALA', 'Enchant creature\nWhen enchanted creature is put into a graveyard, that creature\'s controller loses life equal to its toughness.').
card_first_print('banewasp affliction', 'ALA').
card_image_name('banewasp affliction'/'ALA', 'banewasp affliction').
card_uid('banewasp affliction'/'ALA', 'ALA:Banewasp Affliction:banewasp affliction').
card_rarity('banewasp affliction'/'ALA', 'Common').
card_artist('banewasp affliction'/'ALA', 'Dave Allsop').
card_number('banewasp affliction'/'ALA', '65').
card_flavor_text('banewasp affliction'/'ALA', 'The consuming undeath of Grixis terrifies the survivors of Vithia, but its fauna is just as fearsome.').
card_multiverse_id('banewasp affliction'/'ALA', '174817').

card_in_set('bant battlemage', 'ALA').
card_original_type('bant battlemage'/'ALA', 'Creature — Human Wizard').
card_original_text('bant battlemage'/'ALA', '{G}, {T}: Target creature gains trample until end of turn.\n{U}, {T}: Target creature gains flying until end of turn.').
card_first_print('bant battlemage', 'ALA').
card_image_name('bant battlemage'/'ALA', 'bant battlemage').
card_uid('bant battlemage'/'ALA', 'ALA:Bant Battlemage:bant battlemage').
card_rarity('bant battlemage'/'ALA', 'Uncommon').
card_artist('bant battlemage'/'ALA', 'Donato Giancola').
card_number('bant battlemage'/'ALA', '5').
card_flavor_text('bant battlemage'/'ALA', '\"A night attack will be easy. We\'ll make an air raid over the Akrasan border. Just get me some flint to light the war torches.\"').
card_multiverse_id('bant battlemage'/'ALA', '175123').

card_in_set('bant charm', 'ALA').
card_original_type('bant charm'/'ALA', 'Instant').
card_original_text('bant charm'/'ALA', 'Choose one Destroy target artifact; or put target creature on the bottom of its owner\'s library; or counter target instant spell.').
card_first_print('bant charm', 'ALA').
card_image_name('bant charm'/'ALA', 'bant charm').
card_uid('bant charm'/'ALA', 'ALA:Bant Charm:bant charm').
card_rarity('bant charm'/'ALA', 'Uncommon').
card_artist('bant charm'/'ALA', 'Randy Gallegos').
card_number('bant charm'/'ALA', '155').
card_flavor_text('bant charm'/'ALA', 'Bant is a world where death and chaos hold no sway.').
card_multiverse_id('bant charm'/'ALA', '137931').

card_in_set('bant panorama', 'ALA').
card_original_type('bant panorama'/'ALA', 'Land').
card_original_text('bant panorama'/'ALA', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Bant Panorama: Search your library for a basic Forest, Plains, or Island card and put it into play tapped. Then shuffle your library.').
card_first_print('bant panorama', 'ALA').
card_image_name('bant panorama'/'ALA', 'bant panorama').
card_uid('bant panorama'/'ALA', 'ALA:Bant Panorama:bant panorama').
card_rarity('bant panorama'/'ALA', 'Common').
card_artist('bant panorama'/'ALA', 'Donato Giancola').
card_number('bant panorama'/'ALA', '221').
card_flavor_text('bant panorama'/'ALA', 'Bant\'s hearts are as pure as its air and as bright as its skies.').
card_multiverse_id('bant panorama'/'ALA', '174842').

card_in_set('battlegrace angel', 'ALA').
card_original_type('battlegrace angel'/'ALA', 'Creature — Angel').
card_original_text('battlegrace angel'/'ALA', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, it gains lifelink until end of turn.').
card_first_print('battlegrace angel', 'ALA').
card_image_name('battlegrace angel'/'ALA', 'battlegrace angel').
card_uid('battlegrace angel'/'ALA', 'ALA:Battlegrace Angel:battlegrace angel').
card_rarity('battlegrace angel'/'ALA', 'Rare').
card_artist('battlegrace angel'/'ALA', 'Matt Stewart').
card_number('battlegrace angel'/'ALA', '6').
card_multiverse_id('battlegrace angel'/'ALA', '175394').

card_in_set('behemoth\'s herald', 'ALA').
card_original_type('behemoth\'s herald'/'ALA', 'Creature — Elf Shaman').
card_original_text('behemoth\'s herald'/'ALA', '{2}{G}, {T}, Sacrifice a red creature, a green creature, and a white creature: Search your library for a card named Godsire and put it into play. Then shuffle your library.').
card_first_print('behemoth\'s herald', 'ALA').
card_image_name('behemoth\'s herald'/'ALA', 'behemoth\'s herald').
card_uid('behemoth\'s herald'/'ALA', 'ALA:Behemoth\'s Herald:behemoth\'s herald').
card_rarity('behemoth\'s herald'/'ALA', 'Uncommon').
card_artist('behemoth\'s herald'/'ALA', 'Paolo Parente').
card_number('behemoth\'s herald'/'ALA', '124').
card_flavor_text('behemoth\'s herald'/'ALA', '\"Mine is an insatiable god, with appetites as magnificent as the jungle itself.\"').
card_multiverse_id('behemoth\'s herald'/'ALA', '175263').

card_in_set('blightning', 'ALA').
card_original_type('blightning'/'ALA', 'Sorcery').
card_original_text('blightning'/'ALA', 'Blightning deals 3 damage to target player. That player discards two cards.').
card_image_name('blightning'/'ALA', 'blightning').
card_uid('blightning'/'ALA', 'ALA:Blightning:blightning').
card_rarity('blightning'/'ALA', 'Common').
card_artist('blightning'/'ALA', 'Thomas M. Baxa').
card_number('blightning'/'ALA', '156').
card_flavor_text('blightning'/'ALA', 'While the smell of burning flesh is common in Grixis, the odor of smoldering brain matter is a rare treat.').
card_multiverse_id('blightning'/'ALA', '174917').

card_in_set('blister beetle', 'ALA').
card_original_type('blister beetle'/'ALA', 'Creature — Insect').
card_original_text('blister beetle'/'ALA', 'When Blister Beetle comes into play, target creature gets -1/-1 until end of turn.').
card_first_print('blister beetle', 'ALA').
card_image_name('blister beetle'/'ALA', 'blister beetle').
card_uid('blister beetle'/'ALA', 'ALA:Blister Beetle:blister beetle').
card_rarity('blister beetle'/'ALA', 'Common').
card_artist('blister beetle'/'ALA', 'Anthony S. Waters').
card_number('blister beetle'/'ALA', '66').
card_flavor_text('blister beetle'/'ALA', 'Warriors of the Rip Clan wear their beetle-acid scars proudly, even modifying clothing and armor to better display the trophy.').
card_multiverse_id('blister beetle'/'ALA', '174893').

card_in_set('blood cultist', 'ALA').
card_original_type('blood cultist'/'ALA', 'Creature — Human Wizard').
card_original_text('blood cultist'/'ALA', '{T}: Blood Cultist deals 1 damage to target creature.\nWhenever a creature dealt damage by Blood Cultist this turn is put into a graveyard, put a +1/+1 counter on Blood Cultist.').
card_first_print('blood cultist', 'ALA').
card_image_name('blood cultist'/'ALA', 'blood cultist').
card_uid('blood cultist'/'ALA', 'ALA:Blood Cultist:blood cultist').
card_rarity('blood cultist'/'ALA', 'Uncommon').
card_artist('blood cultist'/'ALA', 'Karl Kopinski').
card_number('blood cultist'/'ALA', '157').
card_flavor_text('blood cultist'/'ALA', '\"To immortality.\"').
card_multiverse_id('blood cultist'/'ALA', '175100').

card_in_set('bloodpyre elemental', 'ALA').
card_original_type('bloodpyre elemental'/'ALA', 'Creature — Elemental').
card_original_text('bloodpyre elemental'/'ALA', 'Sacrifice Bloodpyre Elemental: Bloodpyre Elemental deals 4 damage to target creature. Play this ability only any time you could play a sorcery.').
card_first_print('bloodpyre elemental', 'ALA').
card_image_name('bloodpyre elemental'/'ALA', 'bloodpyre elemental').
card_uid('bloodpyre elemental'/'ALA', 'ALA:Bloodpyre Elemental:bloodpyre elemental').
card_rarity('bloodpyre elemental'/'ALA', 'Common').
card_artist('bloodpyre elemental'/'ALA', 'Trevor Claxton').
card_number('bloodpyre elemental'/'ALA', '93').
card_flavor_text('bloodpyre elemental'/'ALA', 'Elementals born of Jund are as cruel and unstable as the plane itself.').
card_multiverse_id('bloodpyre elemental'/'ALA', '175117').

card_in_set('bloodthorn taunter', 'ALA').
card_original_type('bloodthorn taunter'/'ALA', 'Creature — Human Scout').
card_original_text('bloodthorn taunter'/'ALA', 'Haste\n{T}: Target creature with power 5 or greater gains haste until end of turn.').
card_first_print('bloodthorn taunter', 'ALA').
card_image_name('bloodthorn taunter'/'ALA', 'bloodthorn taunter').
card_uid('bloodthorn taunter'/'ALA', 'ALA:Bloodthorn Taunter:bloodthorn taunter').
card_rarity('bloodthorn taunter'/'ALA', 'Common').
card_artist('bloodthorn taunter'/'ALA', 'Jesper Ejsing').
card_number('bloodthorn taunter'/'ALA', '94').
card_flavor_text('bloodthorn taunter'/'ALA', 'Naya\'s celebrants stoke the gargantuans into a rage, loosing a tide of muscle with the precision of an arrow.').
card_multiverse_id('bloodthorn taunter'/'ALA', '174821').

card_in_set('bone splinters', 'ALA').
card_original_type('bone splinters'/'ALA', 'Sorcery').
card_original_text('bone splinters'/'ALA', 'As an additional cost to play Bone Splinters, sacrifice a creature.\nDestroy target creature.').
card_first_print('bone splinters', 'ALA').
card_image_name('bone splinters'/'ALA', 'bone splinters').
card_uid('bone splinters'/'ALA', 'ALA:Bone Splinters:bone splinters').
card_rarity('bone splinters'/'ALA', 'Common').
card_artist('bone splinters'/'ALA', 'Cole Eastburn').
card_number('bone splinters'/'ALA', '67').
card_flavor_text('bone splinters'/'ALA', 'Witches of the Split-Eye Coven speak of a future when Grixis will overflow with life energy. For now, they must harvest vis from the living to fuel their dark magics.').
card_multiverse_id('bone splinters'/'ALA', '174967').

card_in_set('branching bolt', 'ALA').
card_original_type('branching bolt'/'ALA', 'Instant').
card_original_text('branching bolt'/'ALA', 'Choose one or both Branching Bolt deals 3 damage to target creature with flying; and/or Branching Bolt deals 3 damage to target creature without flying.').
card_first_print('branching bolt', 'ALA').
card_image_name('branching bolt'/'ALA', 'branching bolt').
card_uid('branching bolt'/'ALA', 'ALA:Branching Bolt:branching bolt').
card_rarity('branching bolt'/'ALA', 'Common').
card_artist('branching bolt'/'ALA', 'Vance Kovacs').
card_number('branching bolt'/'ALA', '158').
card_flavor_text('branching bolt'/'ALA', '\"Lightning lives in everything, in living flesh and growing things. It must be set free.\"\n—Rakka Mar').
card_multiverse_id('branching bolt'/'ALA', '177602').

card_in_set('brilliant ultimatum', 'ALA').
card_original_type('brilliant ultimatum'/'ALA', 'Sorcery').
card_original_text('brilliant ultimatum'/'ALA', 'Remove the top five cards of your library from the game. An opponent separates those cards into two piles. You may play any number of cards from one of those piles without paying their mana costs.').
card_first_print('brilliant ultimatum', 'ALA').
card_image_name('brilliant ultimatum'/'ALA', 'brilliant ultimatum').
card_uid('brilliant ultimatum'/'ALA', 'ALA:Brilliant Ultimatum:brilliant ultimatum').
card_rarity('brilliant ultimatum'/'ALA', 'Rare').
card_artist('brilliant ultimatum'/'ALA', 'Anthony Francisco').
card_number('brilliant ultimatum'/'ALA', '159').
card_flavor_text('brilliant ultimatum'/'ALA', 'Revealing the truth only deepened Tezzeret\'s curiosity for the secrets still buried.').
card_multiverse_id('brilliant ultimatum'/'ALA', '175143').

card_in_set('broodmate dragon', 'ALA').
card_original_type('broodmate dragon'/'ALA', 'Creature — Dragon').
card_original_text('broodmate dragon'/'ALA', 'Flying\nWhen Broodmate Dragon comes into play, put a 4/4 red Dragon creature token with flying into play.').
card_image_name('broodmate dragon'/'ALA', 'broodmate dragon').
card_uid('broodmate dragon'/'ALA', 'ALA:Broodmate Dragon:broodmate dragon').
card_rarity('broodmate dragon'/'ALA', 'Rare').
card_artist('broodmate dragon'/'ALA', 'Vance Kovacs').
card_number('broodmate dragon'/'ALA', '160').
card_flavor_text('broodmate dragon'/'ALA', 'Frozen in fear, the goblins stared upward at the circling hunter—and were promptly eaten by its diving mate.').
card_multiverse_id('broodmate dragon'/'ALA', '178101').

card_in_set('bull cerodon', 'ALA').
card_original_type('bull cerodon'/'ALA', 'Creature — Beast').
card_original_text('bull cerodon'/'ALA', 'Vigilance, haste').
card_first_print('bull cerodon', 'ALA').
card_image_name('bull cerodon'/'ALA', 'bull cerodon').
card_uid('bull cerodon'/'ALA', 'ALA:Bull Cerodon:bull cerodon').
card_rarity('bull cerodon'/'ALA', 'Uncommon').
card_artist('bull cerodon'/'ALA', 'Jesper Ejsing').
card_number('bull cerodon'/'ALA', '161').
card_flavor_text('bull cerodon'/'ALA', 'It holds motionless vigil, watching Naya in silence through the screen of the whitecover. When it senses anything amiss, it launches forward with the uncanny sound of torn fog.').
card_multiverse_id('bull cerodon'/'ALA', '174952').

card_in_set('caldera hellion', 'ALA').
card_original_type('caldera hellion'/'ALA', 'Creature — Hellion').
card_original_text('caldera hellion'/'ALA', 'Devour 1 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with that many +1/+1 counters on it.)\nWhen Caldera Hellion comes into play, it deals 3 damage to each creature.').
card_first_print('caldera hellion', 'ALA').
card_image_name('caldera hellion'/'ALA', 'caldera hellion').
card_uid('caldera hellion'/'ALA', 'ALA:Caldera Hellion:caldera hellion').
card_rarity('caldera hellion'/'ALA', 'Rare').
card_artist('caldera hellion'/'ALA', 'Raymond Swanland').
card_number('caldera hellion'/'ALA', '95').
card_multiverse_id('caldera hellion'/'ALA', '175072').

card_in_set('call to heel', 'ALA').
card_original_type('call to heel'/'ALA', 'Instant').
card_original_text('call to heel'/'ALA', 'Return target creature to its owner\'s hand. Its controller draws a card.').
card_first_print('call to heel', 'ALA').
card_image_name('call to heel'/'ALA', 'call to heel').
card_uid('call to heel'/'ALA', 'ALA:Call to Heel:call to heel').
card_rarity('call to heel'/'ALA', 'Common').
card_artist('call to heel'/'ALA', 'Randy Gallegos').
card_number('call to heel'/'ALA', '32').
card_flavor_text('call to heel'/'ALA', 'On Bant, a sigil is both a prize of honor and a bond of duty. The one who bears it may be called to fulfill that charge at any moment.').
card_multiverse_id('call to heel'/'ALA', '175053').

card_in_set('cancel', 'ALA').
card_original_type('cancel'/'ALA', 'Instant').
card_original_text('cancel'/'ALA', 'Counter target spell.').
card_image_name('cancel'/'ALA', 'cancel').
card_uid('cancel'/'ALA', 'ALA:Cancel:cancel').
card_rarity('cancel'/'ALA', 'Common').
card_artist('cancel'/'ALA', 'David Palumbo').
card_number('cancel'/'ALA', '33').
card_flavor_text('cancel'/'ALA', '\"What you are attempting is not against the law. It is, however, extremely foolish.\"').
card_multiverse_id('cancel'/'ALA', '178092').

card_in_set('carrion thrash', 'ALA').
card_original_type('carrion thrash'/'ALA', 'Creature — Viashino Warrior').
card_original_text('carrion thrash'/'ALA', 'When Carrion Thrash is put into a graveyard from play, you may pay {2}. If you do, return another target creature card from your graveyard to your hand.').
card_first_print('carrion thrash', 'ALA').
card_image_name('carrion thrash'/'ALA', 'carrion thrash').
card_uid('carrion thrash'/'ALA', 'ALA:Carrion Thrash:carrion thrash').
card_rarity('carrion thrash'/'ALA', 'Common').
card_artist('carrion thrash'/'ALA', 'Jaime Jones').
card_number('carrion thrash'/'ALA', '162').
card_flavor_text('carrion thrash'/'ALA', 'Viashino hunt in gangs called thrashes. Not all thrashes hunt the freshest game.').
card_multiverse_id('carrion thrash'/'ALA', '176443').

card_in_set('cathartic adept', 'ALA').
card_original_type('cathartic adept'/'ALA', 'Creature — Human Wizard').
card_original_text('cathartic adept'/'ALA', '{T}: Target player puts the top card of his or her library into his or her graveyard.').
card_first_print('cathartic adept', 'ALA').
card_image_name('cathartic adept'/'ALA', 'cathartic adept').
card_uid('cathartic adept'/'ALA', 'ALA:Cathartic Adept:cathartic adept').
card_rarity('cathartic adept'/'ALA', 'Common').
card_artist('cathartic adept'/'ALA', 'Carl Critchlow').
card_number('cathartic adept'/'ALA', '34').
card_flavor_text('cathartic adept'/'ALA', '\"Forget. Only then will there be space for hope between the pain and fear.\"').
card_multiverse_id('cathartic adept'/'ALA', '176433').

card_in_set('cavern thoctar', 'ALA').
card_original_type('cavern thoctar'/'ALA', 'Creature — Beast').
card_original_text('cavern thoctar'/'ALA', '{1}{R}: Cavern Thoctar gets +1/+0 until end of turn.').
card_first_print('cavern thoctar', 'ALA').
card_image_name('cavern thoctar'/'ALA', 'cavern thoctar').
card_uid('cavern thoctar'/'ALA', 'ALA:Cavern Thoctar:cavern thoctar').
card_rarity('cavern thoctar'/'ALA', 'Common').
card_artist('cavern thoctar'/'ALA', 'Jean-Sébastien Rossbach').
card_number('cavern thoctar'/'ALA', '125').
card_flavor_text('cavern thoctar'/'ALA', 'Natives of Naya know better than to loiter near the mouth of a cave. Two glowing red eyes and the stench of foul breath are all the warning you\'re likely to get.').
card_multiverse_id('cavern thoctar'/'ALA', '174994').

card_in_set('clarion ultimatum', 'ALA').
card_original_type('clarion ultimatum'/'ALA', 'Sorcery').
card_original_text('clarion ultimatum'/'ALA', 'Choose five permanents you control. For each of those permanents, you may search your library for a card with the same name as that permanent. Put those cards into play tapped, then shuffle your library.').
card_first_print('clarion ultimatum', 'ALA').
card_image_name('clarion ultimatum'/'ALA', 'clarion ultimatum').
card_uid('clarion ultimatum'/'ALA', 'ALA:Clarion Ultimatum:clarion ultimatum').
card_rarity('clarion ultimatum'/'ALA', 'Rare').
card_artist('clarion ultimatum'/'ALA', 'Michael Komarck').
card_number('clarion ultimatum'/'ALA', '163').
card_flavor_text('clarion ultimatum'/'ALA', 'Be worthy of a single blessing, and many more will follow.').
card_multiverse_id('clarion ultimatum'/'ALA', '175142').

card_in_set('cloudheath drake', 'ALA').
card_original_type('cloudheath drake'/'ALA', 'Artifact Creature — Drake').
card_original_text('cloudheath drake'/'ALA', 'Flying\n{1}{W}: Cloudheath Drake gains vigilance until end of turn.').
card_first_print('cloudheath drake', 'ALA').
card_image_name('cloudheath drake'/'ALA', 'cloudheath drake').
card_uid('cloudheath drake'/'ALA', 'ALA:Cloudheath Drake:cloudheath drake').
card_rarity('cloudheath drake'/'ALA', 'Common').
card_artist('cloudheath drake'/'ALA', 'Izzy').
card_number('cloudheath drake'/'ALA', '35').
card_flavor_text('cloudheath drake'/'ALA', 'A permanent storm rages over the plain of Cloudheath, and drakes ride its currents—two reminders that some elements of Esper will not be controlled.').
card_multiverse_id('cloudheath drake'/'ALA', '174814').

card_in_set('coma veil', 'ALA').
card_original_type('coma veil'/'ALA', 'Enchantment — Aura').
card_original_text('coma veil'/'ALA', 'Enchant artifact or creature\nEnchanted permanent doesn\'t untap during its controller\'s untap step.').
card_first_print('coma veil', 'ALA').
card_image_name('coma veil'/'ALA', 'coma veil').
card_uid('coma veil'/'ALA', 'ALA:Coma Veil:coma veil').
card_rarity('coma veil'/'ALA', 'Common').
card_artist('coma veil'/'ALA', 'Dan Scott').
card_number('coma veil'/'ALA', '36').
card_flavor_text('coma veil'/'ALA', 'Etherium does not know sleep. Only magic can teach it the resting state of cold steel.').
card_multiverse_id('coma veil'/'ALA', '175042').

card_in_set('corpse connoisseur', 'ALA').
card_original_type('corpse connoisseur'/'ALA', 'Creature — Zombie Wizard').
card_original_text('corpse connoisseur'/'ALA', 'When Corpse Connoisseur comes into play, you may search your library for a creature card and put that card into your graveyard. If you do, shuffle your library.\nUnearth {3}{B} ({3}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('corpse connoisseur', 'ALA').
card_image_name('corpse connoisseur'/'ALA', 'corpse connoisseur').
card_uid('corpse connoisseur'/'ALA', 'ALA:Corpse Connoisseur:corpse connoisseur').
card_rarity('corpse connoisseur'/'ALA', 'Uncommon').
card_artist('corpse connoisseur'/'ALA', 'Mark Hyzer').
card_number('corpse connoisseur'/'ALA', '68').
card_multiverse_id('corpse connoisseur'/'ALA', '176448').

card_in_set('courier\'s capsule', 'ALA').
card_original_type('courier\'s capsule'/'ALA', 'Artifact').
card_original_text('courier\'s capsule'/'ALA', '{1}{U}, {T}, Sacrifice Courier\'s Capsule: Draw two cards.').
card_first_print('courier\'s capsule', 'ALA').
card_image_name('courier\'s capsule'/'ALA', 'courier\'s capsule').
card_uid('courier\'s capsule'/'ALA', 'ALA:Courier\'s Capsule:courier\'s capsule').
card_rarity('courier\'s capsule'/'ALA', 'Common').
card_artist('courier\'s capsule'/'ALA', 'Andrew Murray').
card_number('courier\'s capsule'/'ALA', '37').
card_flavor_text('courier\'s capsule'/'ALA', 'In ages past, Esper couriers bore messages written on ornate scrolls. The medium has grown more sophisticated, but the principle remains the same.').
card_multiverse_id('courier\'s capsule'/'ALA', '174800').

card_in_set('court archers', 'ALA').
card_original_type('court archers'/'ALA', 'Creature — Human Archer').
card_original_text('court archers'/'ALA', 'Reach (This can block creatures with flying.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('court archers', 'ALA').
card_image_name('court archers'/'ALA', 'court archers').
card_uid('court archers'/'ALA', 'ALA:Court Archers:court archers').
card_rarity('court archers'/'ALA', 'Common').
card_artist('court archers'/'ALA', 'Randy Gallegos').
card_number('court archers'/'ALA', '126').
card_flavor_text('court archers'/'ALA', 'The Sun-Dappled Court of Valeron stands secure behind a wall of steel-tipped arrows.').
card_multiverse_id('court archers'/'ALA', '174960').

card_in_set('covenant of minds', 'ALA').
card_original_type('covenant of minds'/'ALA', 'Sorcery').
card_original_text('covenant of minds'/'ALA', 'Reveal the top three cards of your library. Target opponent may choose to put those cards into your hand. If he or she doesn\'t, put those cards into your graveyard and draw five cards.').
card_first_print('covenant of minds', 'ALA').
card_image_name('covenant of minds'/'ALA', 'covenant of minds').
card_uid('covenant of minds'/'ALA', 'ALA:Covenant of Minds:covenant of minds').
card_rarity('covenant of minds'/'ALA', 'Rare').
card_artist('covenant of minds'/'ALA', 'Dan Seagrave').
card_number('covenant of minds'/'ALA', '38').
card_flavor_text('covenant of minds'/'ALA', 'Before signing anything on Grixis, always read the fine print.').
card_multiverse_id('covenant of minds'/'ALA', '178091').

card_in_set('cradle of vitality', 'ALA').
card_original_type('cradle of vitality'/'ALA', 'Enchantment').
card_original_text('cradle of vitality'/'ALA', 'Whenever you gain life, you may pay {1}{W}. If you do, put a +1/+1 counter on target creature for each 1 life you gained.').
card_first_print('cradle of vitality', 'ALA').
card_image_name('cradle of vitality'/'ALA', 'cradle of vitality').
card_uid('cradle of vitality'/'ALA', 'ALA:Cradle of Vitality:cradle of vitality').
card_rarity('cradle of vitality'/'ALA', 'Rare').
card_artist('cradle of vitality'/'ALA', 'Trevor Hairsine').
card_number('cradle of vitality'/'ALA', '7').
card_flavor_text('cradle of vitality'/'ALA', 'Naya\'s trees grow tall and sturdy. Their foliage intertwines to form dewcups, rainwater pools where the elves gather to celebrate life.').
card_multiverse_id('cradle of vitality'/'ALA', '174789').

card_in_set('crucible of fire', 'ALA').
card_original_type('crucible of fire'/'ALA', 'Enchantment').
card_original_text('crucible of fire'/'ALA', 'Dragon creatures you control get +3/+3.').
card_first_print('crucible of fire', 'ALA').
card_image_name('crucible of fire'/'ALA', 'crucible of fire').
card_uid('crucible of fire'/'ALA', 'ALA:Crucible of Fire:crucible of fire').
card_rarity('crucible of fire'/'ALA', 'Rare').
card_artist('crucible of fire'/'ALA', 'Dominick Domingo').
card_number('crucible of fire'/'ALA', '96').
card_flavor_text('crucible of fire'/'ALA', '\"The dragon is a perfect marriage of power and the will to use it.\"\n—Sarkhan Vol').
card_multiverse_id('crucible of fire'/'ALA', '179426').

card_in_set('cruel ultimatum', 'ALA').
card_original_type('cruel ultimatum'/'ALA', 'Sorcery').
card_original_text('cruel ultimatum'/'ALA', 'Target opponent sacrifices a creature, discards three cards, then loses 5 life. You return a creature card from your graveyard to your hand, draw three cards, then gain 5 life.').
card_first_print('cruel ultimatum', 'ALA').
card_image_name('cruel ultimatum'/'ALA', 'cruel ultimatum').
card_uid('cruel ultimatum'/'ALA', 'ALA:Cruel Ultimatum:cruel ultimatum').
card_rarity('cruel ultimatum'/'ALA', 'Rare').
card_artist('cruel ultimatum'/'ALA', 'Ralph Horsley').
card_number('cruel ultimatum'/'ALA', '164').
card_flavor_text('cruel ultimatum'/'ALA', 'There is always a greater power.').
card_multiverse_id('cruel ultimatum'/'ALA', '175079').

card_in_set('crumbling necropolis', 'ALA').
card_original_type('crumbling necropolis'/'ALA', 'Land').
card_original_text('crumbling necropolis'/'ALA', 'Crumbling Necropolis comes into play tapped.\n{T}: Add {U}, {B}, or {R} to your mana pool.').
card_first_print('crumbling necropolis', 'ALA').
card_image_name('crumbling necropolis'/'ALA', 'crumbling necropolis').
card_uid('crumbling necropolis'/'ALA', 'ALA:Crumbling Necropolis:crumbling necropolis').
card_rarity('crumbling necropolis'/'ALA', 'Uncommon').
card_artist('crumbling necropolis'/'ALA', 'Dave Kendall').
card_number('crumbling necropolis'/'ALA', '222').
card_flavor_text('crumbling necropolis'/'ALA', '\"They say the ruins of Sedraxis were once a shining capital in Vithia. Now it is a blight, a place to be avoided by the living.\"\n—Olcot, Rider of Joffik').
card_multiverse_id('crumbling necropolis'/'ALA', '175112').

card_in_set('cunning lethemancer', 'ALA').
card_original_type('cunning lethemancer'/'ALA', 'Creature — Human Wizard').
card_original_text('cunning lethemancer'/'ALA', 'At the beginning of your upkeep, each player discards a card.').
card_first_print('cunning lethemancer', 'ALA').
card_image_name('cunning lethemancer'/'ALA', 'cunning lethemancer').
card_uid('cunning lethemancer'/'ALA', 'ALA:Cunning Lethemancer:cunning lethemancer').
card_rarity('cunning lethemancer'/'ALA', 'Rare').
card_artist('cunning lethemancer'/'ALA', 'Paul Bonner').
card_number('cunning lethemancer'/'ALA', '69').
card_flavor_text('cunning lethemancer'/'ALA', '\"Give me your memories, and you will have release from the suffering they bring you.\"').
card_multiverse_id('cunning lethemancer'/'ALA', '176429').

card_in_set('cylian elf', 'ALA').
card_original_type('cylian elf'/'ALA', 'Creature — Elf Scout').
card_original_text('cylian elf'/'ALA', '').
card_first_print('cylian elf', 'ALA').
card_image_name('cylian elf'/'ALA', 'cylian elf').
card_uid('cylian elf'/'ALA', 'ALA:Cylian Elf:cylian elf').
card_rarity('cylian elf'/'ALA', 'Common').
card_artist('cylian elf'/'ALA', 'Steve Prescott').
card_number('cylian elf'/'ALA', '127').
card_flavor_text('cylian elf'/'ALA', 'From her sunsail tent high above the forest floor, an elf harkener can hear the footfalls of a single creature through the cacophony of Naya\'s jungle sounds.').
card_multiverse_id('cylian elf'/'ALA', '174935').

card_in_set('dawnray archer', 'ALA').
card_original_type('dawnray archer'/'ALA', 'Creature — Human Archer').
card_original_text('dawnray archer'/'ALA', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{W}, {T}: Dawnray Archer deals 1 damage to target attacking or blocking creature.').
card_first_print('dawnray archer', 'ALA').
card_image_name('dawnray archer'/'ALA', 'dawnray archer').
card_uid('dawnray archer'/'ALA', 'ALA:Dawnray Archer:dawnray archer').
card_rarity('dawnray archer'/'ALA', 'Uncommon').
card_artist('dawnray archer'/'ALA', 'Dan Dos Santos').
card_number('dawnray archer'/'ALA', '39').
card_flavor_text('dawnray archer'/'ALA', 'Their shots are fired most often as signals—or at those who didn\'t get the message.').
card_multiverse_id('dawnray archer'/'ALA', '175085').

card_in_set('death baron', 'ALA').
card_original_type('death baron'/'ALA', 'Creature — Zombie Wizard').
card_original_text('death baron'/'ALA', 'Skeleton creatures you control and other Zombie creatures you control get +1/+1 and have deathtouch.').
card_first_print('death baron', 'ALA').
card_image_name('death baron'/'ALA', 'death baron').
card_uid('death baron'/'ALA', 'ALA:Death Baron:death baron').
card_rarity('death baron'/'ALA', 'Rare').
card_artist('death baron'/'ALA', 'Nils Hamm').
card_number('death baron'/'ALA', '70').
card_flavor_text('death baron'/'ALA', 'For the necromancer barons, killing and recruitment are one and the same.').
card_multiverse_id('death baron'/'ALA', '176430').

card_in_set('deathgreeter', 'ALA').
card_original_type('deathgreeter'/'ALA', 'Creature — Human Shaman').
card_original_text('deathgreeter'/'ALA', 'Whenever another creature is put into a graveyard from play, you may gain 1 life.').
card_first_print('deathgreeter', 'ALA').
card_image_name('deathgreeter'/'ALA', 'deathgreeter').
card_uid('deathgreeter'/'ALA', 'ALA:Deathgreeter:deathgreeter').
card_rarity('deathgreeter'/'ALA', 'Common').
card_artist('deathgreeter'/'ALA', 'Dominick Domingo').
card_number('deathgreeter'/'ALA', '71').
card_flavor_text('deathgreeter'/'ALA', '\"The bones of allies grant wisdom. The bones of enemies grant strength. The bones of dragons grant life eternal.\"').
card_multiverse_id('deathgreeter'/'ALA', '175073').

card_in_set('deft duelist', 'ALA').
card_original_type('deft duelist'/'ALA', 'Creature — Human Rogue').
card_original_text('deft duelist'/'ALA', 'First strike\nShroud (This creature can\'t be the target of spells or abilities.)').
card_first_print('deft duelist', 'ALA').
card_image_name('deft duelist'/'ALA', 'deft duelist').
card_uid('deft duelist'/'ALA', 'ALA:Deft Duelist:deft duelist').
card_rarity('deft duelist'/'ALA', 'Common').
card_artist('deft duelist'/'ALA', 'David Palumbo').
card_number('deft duelist'/'ALA', '165').
card_flavor_text('deft duelist'/'ALA', 'Some Serul Cove rogues traffic in stolen sigils, which fetch a high price from those who would rather pay for honor than earn it.').
card_multiverse_id('deft duelist'/'ALA', '175121').

card_in_set('demon\'s herald', 'ALA').
card_original_type('demon\'s herald'/'ALA', 'Creature — Human Wizard').
card_original_text('demon\'s herald'/'ALA', '{2}{B}, {T}, Sacrifice a blue creature, a black creature, and a red creature: Search your library for a card named Prince of Thralls and put it into play. Then shuffle your library.').
card_first_print('demon\'s herald', 'ALA').
card_image_name('demon\'s herald'/'ALA', 'demon\'s herald').
card_uid('demon\'s herald'/'ALA', 'ALA:Demon\'s Herald:demon\'s herald').
card_rarity('demon\'s herald'/'ALA', 'Uncommon').
card_artist('demon\'s herald'/'ALA', 'Karl Kopinski').
card_number('demon\'s herald'/'ALA', '72').
card_flavor_text('demon\'s herald'/'ALA', '\"May these deaths be the first of many.\"').
card_multiverse_id('demon\'s herald'/'ALA', '175243').

card_in_set('dispeller\'s capsule', 'ALA').
card_original_type('dispeller\'s capsule'/'ALA', 'Artifact').
card_original_text('dispeller\'s capsule'/'ALA', '{2}{W}, {T}, Sacrifice Dispeller\'s Capsule: Destroy target artifact or enchantment.').
card_first_print('dispeller\'s capsule', 'ALA').
card_image_name('dispeller\'s capsule'/'ALA', 'dispeller\'s capsule').
card_uid('dispeller\'s capsule'/'ALA', 'ALA:Dispeller\'s Capsule:dispeller\'s capsule').
card_rarity('dispeller\'s capsule'/'ALA', 'Common').
card_artist('dispeller\'s capsule'/'ALA', 'Franz Vohwinkel').
card_number('dispeller\'s capsule'/'ALA', '8').
card_flavor_text('dispeller\'s capsule'/'ALA', '\"I find its symmetry pleasing. It rids our world of offensive refuse while disposing of itself.\"\n—Dolomarus, Proctor of the Clean').
card_multiverse_id('dispeller\'s capsule'/'ALA', '174830').

card_in_set('dragon fodder', 'ALA').
card_original_type('dragon fodder'/'ALA', 'Sorcery').
card_original_text('dragon fodder'/'ALA', 'Put two 1/1 red Goblin creature tokens into play.').
card_image_name('dragon fodder'/'ALA', 'dragon fodder').
card_uid('dragon fodder'/'ALA', 'ALA:Dragon Fodder:dragon fodder').
card_rarity('dragon fodder'/'ALA', 'Common').
card_artist('dragon fodder'/'ALA', 'Jaime Jones').
card_number('dragon fodder'/'ALA', '97').
card_flavor_text('dragon fodder'/'ALA', 'Goblins journey to the sacrificial peaks in pairs so that the rare survivor might be able to relate the details of the other\'s grisly demise.').
card_multiverse_id('dragon fodder'/'ALA', '174936').

card_in_set('dragon\'s herald', 'ALA').
card_original_type('dragon\'s herald'/'ALA', 'Creature — Goblin Shaman').
card_original_text('dragon\'s herald'/'ALA', '{2}{R}, {T}, Sacrifice a black creature, a red creature, and a green creature: Search your library for a card named Hellkite Overlord and put it into play. Then shuffle your library.').
card_first_print('dragon\'s herald', 'ALA').
card_image_name('dragon\'s herald'/'ALA', 'dragon\'s herald').
card_uid('dragon\'s herald'/'ALA', 'ALA:Dragon\'s Herald:dragon\'s herald').
card_rarity('dragon\'s herald'/'ALA', 'Uncommon').
card_artist('dragon\'s herald'/'ALA', 'Daarken').
card_number('dragon\'s herald'/'ALA', '98').
card_flavor_text('dragon\'s herald'/'ALA', 'The penultimate step of the ritual involves bathing in a delicious glaze.').
card_multiverse_id('dragon\'s herald'/'ALA', '175239').

card_in_set('dreg reaver', 'ALA').
card_original_type('dreg reaver'/'ALA', 'Creature — Zombie Beast').
card_original_text('dreg reaver'/'ALA', '').
card_first_print('dreg reaver', 'ALA').
card_image_name('dreg reaver'/'ALA', 'dreg reaver').
card_uid('dreg reaver'/'ALA', 'ALA:Dreg Reaver:dreg reaver').
card_rarity('dreg reaver'/'ALA', 'Common').
card_artist('dreg reaver'/'ALA', 'Thomas M. Baxa').
card_number('dreg reaver'/'ALA', '73').
card_flavor_text('dreg reaver'/'ALA', '\"On our thirty-fourth day of digging, we unearthed a chamber that contained the intact remains of several species long extinct from Grixis. One in particular should make a fine siege engine . . . .\"\n—Last notes of Shungus Nod, fleshcrafter').
card_multiverse_id('dreg reaver'/'ALA', '174848').

card_in_set('dregscape zombie', 'ALA').
card_original_type('dregscape zombie'/'ALA', 'Creature — Zombie').
card_original_text('dregscape zombie'/'ALA', 'Unearth {B} ({B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('dregscape zombie', 'ALA').
card_image_name('dregscape zombie'/'ALA', 'dregscape zombie').
card_uid('dregscape zombie'/'ALA', 'ALA:Dregscape Zombie:dregscape zombie').
card_rarity('dregscape zombie'/'ALA', 'Common').
card_artist('dregscape zombie'/'ALA', 'Lars Grant-West').
card_number('dregscape zombie'/'ALA', '74').
card_flavor_text('dregscape zombie'/'ALA', 'The undead of Grixis are fueled by their hatred of the living.').
card_multiverse_id('dregscape zombie'/'ALA', '174835').

card_in_set('druid of the anima', 'ALA').
card_original_type('druid of the anima'/'ALA', 'Creature — Elf Druid').
card_original_text('druid of the anima'/'ALA', '{T}: Add {R}, {G}, or {W} to your mana pool.').
card_first_print('druid of the anima', 'ALA').
card_image_name('druid of the anima'/'ALA', 'druid of the anima').
card_uid('druid of the anima'/'ALA', 'ALA:Druid of the Anima:druid of the anima').
card_rarity('druid of the anima'/'ALA', 'Common').
card_artist('druid of the anima'/'ALA', 'Jim Murray').
card_number('druid of the anima'/'ALA', '128').
card_flavor_text('druid of the anima'/'ALA', 'Although the Anima herself remains at the Sacellum, her druids roam Naya, collecting mana bonds with every location in the world.').
card_multiverse_id('druid of the anima'/'ALA', '174903').

card_in_set('drumhunter', 'ALA').
card_original_type('drumhunter'/'ALA', 'Creature — Human Druid Warrior').
card_original_text('drumhunter'/'ALA', 'At the end of your turn, if you control a creature with power 5 or greater, you may draw a card.\n{T}: Add {1} to your mana pool.').
card_first_print('drumhunter', 'ALA').
card_image_name('drumhunter'/'ALA', 'drumhunter').
card_uid('drumhunter'/'ALA', 'ALA:Drumhunter:drumhunter').
card_rarity('drumhunter'/'ALA', 'Uncommon').
card_artist('drumhunter'/'ALA', 'Jim Murray').
card_number('drumhunter'/'ALA', '129').
card_flavor_text('drumhunter'/'ALA', '\"I feel the vine-rhythm as my friends drive the beast to me. I will not miss my mark.\"').
card_multiverse_id('drumhunter'/'ALA', '174943').

card_in_set('elspeth, knight-errant', 'ALA').
card_original_type('elspeth, knight-errant'/'ALA', 'Planeswalker — Elspeth').
card_original_text('elspeth, knight-errant'/'ALA', '+1: Put a 1/1 white Soldier creature token into play.\n+1: Target creature gets +3/+3 and gains flying until end of turn.\n-8: For the rest of the game, artifacts, creatures, enchantments, and lands you control are indestructible.').
card_first_print('elspeth, knight-errant', 'ALA').
card_image_name('elspeth, knight-errant'/'ALA', 'elspeth, knight-errant').
card_uid('elspeth, knight-errant'/'ALA', 'ALA:Elspeth, Knight-Errant:elspeth, knight-errant').
card_rarity('elspeth, knight-errant'/'ALA', 'Mythic Rare').
card_artist('elspeth, knight-errant'/'ALA', 'Volkan Baga').
card_number('elspeth, knight-errant'/'ALA', '9').
card_multiverse_id('elspeth, knight-errant'/'ALA', '174859').

card_in_set('elvish visionary', 'ALA').
card_original_type('elvish visionary'/'ALA', 'Creature — Elf Shaman').
card_original_text('elvish visionary'/'ALA', 'When Elvish Visionary comes into play, draw a card.').
card_image_name('elvish visionary'/'ALA', 'elvish visionary').
card_uid('elvish visionary'/'ALA', 'ALA:Elvish Visionary:elvish visionary').
card_rarity('elvish visionary'/'ALA', 'Common').
card_artist('elvish visionary'/'ALA', 'D. Alexander Gregory').
card_number('elvish visionary'/'ALA', '130').
card_flavor_text('elvish visionary'/'ALA', 'Before any major undertaking, a Cylian elf seeks the guidance of a visionary to learn the will of the gargantuan ancients.').
card_multiverse_id('elvish visionary'/'ALA', '175124').

card_in_set('empyrial archangel', 'ALA').
card_original_type('empyrial archangel'/'ALA', 'Creature — Angel').
card_original_text('empyrial archangel'/'ALA', 'Flying, shroud\nAll damage that would be dealt to you is dealt to Empyrial Archangel instead.').
card_first_print('empyrial archangel', 'ALA').
card_image_name('empyrial archangel'/'ALA', 'empyrial archangel').
card_uid('empyrial archangel'/'ALA', 'ALA:Empyrial Archangel:empyrial archangel').
card_rarity('empyrial archangel'/'ALA', 'Mythic Rare').
card_artist('empyrial archangel'/'ALA', 'Greg Staples').
card_number('empyrial archangel'/'ALA', '166').
card_flavor_text('empyrial archangel'/'ALA', 'Her wings are the prayers of the devoted; her sword, the despair of the vile.').
card_multiverse_id('empyrial archangel'/'ALA', '175104').

card_in_set('esper battlemage', 'ALA').
card_original_type('esper battlemage'/'ALA', 'Artifact Creature — Human Wizard').
card_original_text('esper battlemage'/'ALA', '{W}, {T}: Prevent the next 2 damage that would be dealt to you this turn.\n{B}, {T}: Target creature gets -1/-1 until end of turn.').
card_first_print('esper battlemage', 'ALA').
card_image_name('esper battlemage'/'ALA', 'esper battlemage').
card_uid('esper battlemage'/'ALA', 'ALA:Esper Battlemage:esper battlemage').
card_rarity('esper battlemage'/'ALA', 'Uncommon').
card_artist('esper battlemage'/'ALA', 'Matt Cavotta').
card_number('esper battlemage'/'ALA', '40').
card_flavor_text('esper battlemage'/'ALA', 'She can heal the flesh, or exploit its many weaknesses.').
card_multiverse_id('esper battlemage'/'ALA', '174872').

card_in_set('esper charm', 'ALA').
card_original_type('esper charm'/'ALA', 'Instant').
card_original_text('esper charm'/'ALA', 'Choose one Destroy target enchantment; or draw two cards; or target player discards two cards.').
card_first_print('esper charm', 'ALA').
card_image_name('esper charm'/'ALA', 'esper charm').
card_uid('esper charm'/'ALA', 'ALA:Esper Charm:esper charm').
card_rarity('esper charm'/'ALA', 'Uncommon').
card_artist('esper charm'/'ALA', 'Michael Bruinsma').
card_number('esper charm'/'ALA', '167').
card_flavor_text('esper charm'/'ALA', '\"Thoughts are commodities. Someone will pay a good price for them. Even ones as simplistic as yours . . .\"\n—Ennor, mentalist').
card_multiverse_id('esper charm'/'ALA', '137913').

card_in_set('esper panorama', 'ALA').
card_original_type('esper panorama'/'ALA', 'Land').
card_original_text('esper panorama'/'ALA', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Esper Panorama: Search your library for a basic Plains, Island, or Swamp card and put it into play tapped. Then shuffle your library.').
card_first_print('esper panorama', 'ALA').
card_image_name('esper panorama'/'ALA', 'esper panorama').
card_uid('esper panorama'/'ALA', 'ALA:Esper Panorama:esper panorama').
card_rarity('esper panorama'/'ALA', 'Common').
card_artist('esper panorama'/'ALA', 'Franz Vohwinkel').
card_number('esper panorama'/'ALA', '223').
card_flavor_text('esper panorama'/'ALA', 'Esper is an expansive canvas painted by precise, controlling hands.').
card_multiverse_id('esper panorama'/'ALA', '179431').

card_in_set('etherium astrolabe', 'ALA').
card_original_type('etherium astrolabe'/'ALA', 'Artifact').
card_original_text('etherium astrolabe'/'ALA', 'Flash\n{B}, {T}, Sacrifice an artifact: Draw a card.').
card_first_print('etherium astrolabe', 'ALA').
card_image_name('etherium astrolabe'/'ALA', 'etherium astrolabe').
card_uid('etherium astrolabe'/'ALA', 'ALA:Etherium Astrolabe:etherium astrolabe').
card_rarity('etherium astrolabe'/'ALA', 'Uncommon').
card_artist('etherium astrolabe'/'ALA', 'Michael Bruinsma').
card_number('etherium astrolabe'/'ALA', '41').
card_flavor_text('etherium astrolabe'/'ALA', '\"Speculation is foolish when the tools of certainty are available.\"\n—Cinna, vedalken consul').
card_multiverse_id('etherium astrolabe'/'ALA', '175138').

card_in_set('etherium sculptor', 'ALA').
card_original_type('etherium sculptor'/'ALA', 'Artifact Creature — Vedalken Artificer').
card_original_text('etherium sculptor'/'ALA', 'Artifact spells you play cost {1} less to play.').
card_first_print('etherium sculptor', 'ALA').
card_image_name('etherium sculptor'/'ALA', 'etherium sculptor').
card_uid('etherium sculptor'/'ALA', 'ALA:Etherium Sculptor:etherium sculptor').
card_rarity('etherium sculptor'/'ALA', 'Common').
card_artist('etherium sculptor'/'ALA', 'Steven Belledin').
card_number('etherium sculptor'/'ALA', '42').
card_flavor_text('etherium sculptor'/'ALA', 'The greatest masters of the craft abandon tools altogether, shaping metal with hand and mind alone.').
card_multiverse_id('etherium sculptor'/'ALA', '176435').

card_in_set('ethersworn canonist', 'ALA').
card_original_type('ethersworn canonist'/'ALA', 'Artifact Creature — Human Cleric').
card_original_text('ethersworn canonist'/'ALA', 'Each player who has played a nonartifact spell this turn can\'t play additional nonartifact spells.').
card_first_print('ethersworn canonist', 'ALA').
card_image_name('ethersworn canonist'/'ALA', 'ethersworn canonist').
card_uid('ethersworn canonist'/'ALA', 'ALA:Ethersworn Canonist:ethersworn canonist').
card_rarity('ethersworn canonist'/'ALA', 'Rare').
card_artist('ethersworn canonist'/'ALA', 'Izzy').
card_number('ethersworn canonist'/'ALA', '10').
card_flavor_text('ethersworn canonist'/'ALA', '\"The noble work of our order is to infuse all life on Esper with etherium. Our goal will be reached more rapidly if new life is . . . suppressed.\"').
card_multiverse_id('ethersworn canonist'/'ALA', '174931').

card_in_set('excommunicate', 'ALA').
card_original_type('excommunicate'/'ALA', 'Sorcery').
card_original_text('excommunicate'/'ALA', 'Put target creature on top of its owner\'s library.').
card_first_print('excommunicate', 'ALA').
card_image_name('excommunicate'/'ALA', 'excommunicate').
card_uid('excommunicate'/'ALA', 'ALA:Excommunicate:excommunicate').
card_rarity('excommunicate'/'ALA', 'Common').
card_artist('excommunicate'/'ALA', 'Matt Stewart').
card_number('excommunicate'/'ALA', '11').
card_flavor_text('excommunicate'/'ALA', 'Rebels and other malcontents forced into the ritual awake lost in Topa\'s vast savannahs. Those who find their way back return humble and repentant.').
card_multiverse_id('excommunicate'/'ALA', '180144').

card_in_set('executioner\'s capsule', 'ALA').
card_original_type('executioner\'s capsule'/'ALA', 'Artifact').
card_original_text('executioner\'s capsule'/'ALA', '{1}{B}, {T}, Sacrifice Executioner\'s Capsule: Destroy target nonblack creature.').
card_first_print('executioner\'s capsule', 'ALA').
card_image_name('executioner\'s capsule'/'ALA', 'executioner\'s capsule').
card_uid('executioner\'s capsule'/'ALA', 'ALA:Executioner\'s Capsule:executioner\'s capsule').
card_rarity('executioner\'s capsule'/'ALA', 'Common').
card_artist('executioner\'s capsule'/'ALA', 'Warren Mahy').
card_number('executioner\'s capsule'/'ALA', '75').
card_flavor_text('executioner\'s capsule'/'ALA', 'There is always a moment of trepidation before opening a message capsule, for fear of the judgment that might be contained within.').
card_multiverse_id('executioner\'s capsule'/'ALA', '174895').

card_in_set('exuberant firestoker', 'ALA').
card_original_type('exuberant firestoker'/'ALA', 'Creature — Human Druid Shaman').
card_original_text('exuberant firestoker'/'ALA', 'At the end of your turn, if you control a creature with power 5 or greater, you may have Exuberant Firestoker deal 2 damage to target player.\n{T}: Add {1} to your mana pool.').
card_first_print('exuberant firestoker', 'ALA').
card_image_name('exuberant firestoker'/'ALA', 'exuberant firestoker').
card_uid('exuberant firestoker'/'ALA', 'ALA:Exuberant Firestoker:exuberant firestoker').
card_rarity('exuberant firestoker'/'ALA', 'Uncommon').
card_artist('exuberant firestoker'/'ALA', 'Zoltan Boros & Gabor Szikszai').
card_number('exuberant firestoker'/'ALA', '99').
card_flavor_text('exuberant firestoker'/'ALA', 'The artistry of Etlan\'s firedancers is more spectacular when behemoths join in.').
card_multiverse_id('exuberant firestoker'/'ALA', '175049').

card_in_set('fatestitcher', 'ALA').
card_original_type('fatestitcher'/'ALA', 'Creature — Zombie Wizard').
card_original_text('fatestitcher'/'ALA', '{T}: You may tap or untap another target permanent.\nUnearth {U} ({U}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('fatestitcher', 'ALA').
card_image_name('fatestitcher'/'ALA', 'fatestitcher').
card_uid('fatestitcher'/'ALA', 'ALA:Fatestitcher:fatestitcher').
card_rarity('fatestitcher'/'ALA', 'Uncommon').
card_artist('fatestitcher'/'ALA', 'E. M. Gist').
card_number('fatestitcher'/'ALA', '43').
card_multiverse_id('fatestitcher'/'ALA', '176456').

card_in_set('feral hydra', 'ALA').
card_original_type('feral hydra'/'ALA', 'Creature — Hydra Beast').
card_original_text('feral hydra'/'ALA', 'Feral Hydra comes into play with X +1/+1 counters on it.\n{3}: Put a +1/+1 counter on Feral Hydra. Any player may play this ability.').
card_first_print('feral hydra', 'ALA').
card_image_name('feral hydra'/'ALA', 'feral hydra').
card_uid('feral hydra'/'ALA', 'ALA:Feral Hydra:feral hydra').
card_rarity('feral hydra'/'ALA', 'Rare').
card_artist('feral hydra'/'ALA', 'Steve Prescott').
card_number('feral hydra'/'ALA', '131').
card_flavor_text('feral hydra'/'ALA', 'It shreds its prey as each head fights for the choicest bits.').
card_multiverse_id('feral hydra'/'ALA', '174973').

card_in_set('filigree sages', 'ALA').
card_original_type('filigree sages'/'ALA', 'Artifact Creature — Vedalken Wizard').
card_original_text('filigree sages'/'ALA', '{2}{U}: Untap target artifact.').
card_first_print('filigree sages', 'ALA').
card_image_name('filigree sages'/'ALA', 'filigree sages').
card_uid('filigree sages'/'ALA', 'ALA:Filigree Sages:filigree sages').
card_rarity('filigree sages'/'ALA', 'Uncommon').
card_artist('filigree sages'/'ALA', 'Dan Scott').
card_number('filigree sages'/'ALA', '44').
card_flavor_text('filigree sages'/'ALA', '\"We of the Sanctum Arcanum have pondered every word on every page of the Filigree Texts. If you can\'t say the same, don\'t bother speaking.\"').
card_multiverse_id('filigree sages'/'ALA', '174888').

card_in_set('fire-field ogre', 'ALA').
card_original_type('fire-field ogre'/'ALA', 'Creature — Ogre Mutant').
card_original_text('fire-field ogre'/'ALA', 'First strike\nUnearth {U}{B}{R} ({U}{B}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('fire-field ogre', 'ALA').
card_image_name('fire-field ogre'/'ALA', 'fire-field ogre').
card_uid('fire-field ogre'/'ALA', 'ALA:Fire-Field Ogre:fire-field ogre').
card_rarity('fire-field ogre'/'ALA', 'Uncommon').
card_artist('fire-field ogre'/'ALA', 'Mitch Cotie').
card_number('fire-field ogre'/'ALA', '168').
card_multiverse_id('fire-field ogre'/'ALA', '176447').

card_in_set('flameblast dragon', 'ALA').
card_original_type('flameblast dragon'/'ALA', 'Creature — Dragon').
card_original_text('flameblast dragon'/'ALA', 'Flying\nWhenever Flameblast Dragon attacks, you may pay {X}{R}. If you do, Flameblast Dragon deals X damage to target creature or player.').
card_first_print('flameblast dragon', 'ALA').
card_image_name('flameblast dragon'/'ALA', 'flameblast dragon').
card_uid('flameblast dragon'/'ALA', 'ALA:Flameblast Dragon:flameblast dragon').
card_rarity('flameblast dragon'/'ALA', 'Rare').
card_artist('flameblast dragon'/'ALA', 'Jaime Jones').
card_number('flameblast dragon'/'ALA', '100').
card_flavor_text('flameblast dragon'/'ALA', 'Dragon lords of Jund rule by edicts of flame.').
card_multiverse_id('flameblast dragon'/'ALA', '177476').

card_in_set('fleshbag marauder', 'ALA').
card_original_type('fleshbag marauder'/'ALA', 'Creature — Zombie Warrior').
card_original_text('fleshbag marauder'/'ALA', 'When Fleshbag Marauder comes into play, each player sacrifices a creature.').
card_first_print('fleshbag marauder', 'ALA').
card_image_name('fleshbag marauder'/'ALA', 'fleshbag marauder').
card_uid('fleshbag marauder'/'ALA', 'ALA:Fleshbag Marauder:fleshbag marauder').
card_rarity('fleshbag marauder'/'ALA', 'Uncommon').
card_artist('fleshbag marauder'/'ALA', 'Pete Venters').
card_number('fleshbag marauder'/'ALA', '76').
card_flavor_text('fleshbag marauder'/'ALA', 'Grixis is a world where the only things found in abundance are death and decay. Corpses, whole or in part, are the standard currency among necromancers and demons.').
card_multiverse_id('fleshbag marauder'/'ALA', '174871').

card_in_set('forest', 'ALA').
card_original_type('forest'/'ALA', 'Basic Land — Forest').
card_original_text('forest'/'ALA', 'G').
card_image_name('forest'/'ALA', 'forest1').
card_uid('forest'/'ALA', 'ALA:Forest:forest1').
card_rarity('forest'/'ALA', 'Basic Land').
card_artist('forest'/'ALA', 'Aleksi Briclot').
card_number('forest'/'ALA', '246').
card_multiverse_id('forest'/'ALA', '174927').

card_in_set('forest', 'ALA').
card_original_type('forest'/'ALA', 'Basic Land — Forest').
card_original_text('forest'/'ALA', 'G').
card_image_name('forest'/'ALA', 'forest2').
card_uid('forest'/'ALA', 'ALA:Forest:forest2').
card_rarity('forest'/'ALA', 'Basic Land').
card_artist('forest'/'ALA', 'Zoltan Boros & Gabor Szikszai').
card_number('forest'/'ALA', '247').
card_multiverse_id('forest'/'ALA', '174929').

card_in_set('forest', 'ALA').
card_original_type('forest'/'ALA', 'Basic Land — Forest').
card_original_text('forest'/'ALA', 'G').
card_image_name('forest'/'ALA', 'forest3').
card_uid('forest'/'ALA', 'ALA:Forest:forest3').
card_rarity('forest'/'ALA', 'Basic Land').
card_artist('forest'/'ALA', 'Zoltan Boros & Gabor Szikszai').
card_number('forest'/'ALA', '248').
card_multiverse_id('forest'/'ALA', '174928').

card_in_set('forest', 'ALA').
card_original_type('forest'/'ALA', 'Basic Land — Forest').
card_original_text('forest'/'ALA', 'G').
card_image_name('forest'/'ALA', 'forest4').
card_uid('forest'/'ALA', 'ALA:Forest:forest4').
card_rarity('forest'/'ALA', 'Basic Land').
card_artist('forest'/'ALA', 'Michael Komarck').
card_number('forest'/'ALA', '249').
card_multiverse_id('forest'/'ALA', '174930').

card_in_set('gather specimens', 'ALA').
card_original_type('gather specimens'/'ALA', 'Instant').
card_original_text('gather specimens'/'ALA', 'If a creature would come into play under an opponent\'s control this turn, it comes into play under your control instead.').
card_first_print('gather specimens', 'ALA').
card_image_name('gather specimens'/'ALA', 'gather specimens').
card_uid('gather specimens'/'ALA', 'ALA:Gather Specimens:gather specimens').
card_rarity('gather specimens'/'ALA', 'Rare').
card_artist('gather specimens'/'ALA', 'Michael Bruinsma').
card_number('gather specimens'/'ALA', '45').
card_flavor_text('gather specimens'/'ALA', '\"It\'s free to do whatever it wants. I\'ve merely told it what that is.\"\n—Xorin, Architect of Will').
card_multiverse_id('gather specimens'/'ALA', '175011').

card_in_set('gift of the gargantuan', 'ALA').
card_original_type('gift of the gargantuan'/'ALA', 'Sorcery').
card_original_text('gift of the gargantuan'/'ALA', 'Look at the top four cards of your library. You may reveal a creature card and/or a land card from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('gift of the gargantuan', 'ALA').
card_image_name('gift of the gargantuan'/'ALA', 'gift of the gargantuan').
card_uid('gift of the gargantuan'/'ALA', 'ALA:Gift of the Gargantuan:gift of the gargantuan').
card_rarity('gift of the gargantuan'/'ALA', 'Common').
card_artist('gift of the gargantuan'/'ALA', 'Jean-Sébastien Rossbach').
card_number('gift of the gargantuan'/'ALA', '132').
card_multiverse_id('gift of the gargantuan'/'ALA', '175036').

card_in_set('glaze fiend', 'ALA').
card_original_type('glaze fiend'/'ALA', 'Artifact Creature — Illusion').
card_original_text('glaze fiend'/'ALA', 'Flying\nWhenever another artifact comes into play under your control, Glaze Fiend gets +2/+2 until end of turn.').
card_first_print('glaze fiend', 'ALA').
card_image_name('glaze fiend'/'ALA', 'glaze fiend').
card_uid('glaze fiend'/'ALA', 'ALA:Glaze Fiend:glaze fiend').
card_rarity('glaze fiend'/'ALA', 'Common').
card_artist('glaze fiend'/'ALA', 'Joshua Hagler').
card_number('glaze fiend'/'ALA', '77').
card_flavor_text('glaze fiend'/'ALA', 'Before the zealots of the Ethersworn came to power, Esper illusionists dreamed up creations to mimic a variety of substances.').
card_multiverse_id('glaze fiend'/'ALA', '174816').

card_in_set('goblin assault', 'ALA').
card_original_type('goblin assault'/'ALA', 'Enchantment').
card_original_text('goblin assault'/'ALA', 'At the beginning of your upkeep, put a 1/1 red Goblin creature token with haste into play.\nGoblin creatures attack each turn if able.').
card_first_print('goblin assault', 'ALA').
card_image_name('goblin assault'/'ALA', 'goblin assault').
card_uid('goblin assault'/'ALA', 'ALA:Goblin Assault:goblin assault').
card_rarity('goblin assault'/'ALA', 'Rare').
card_artist('goblin assault'/'ALA', 'Jaime Jones').
card_number('goblin assault'/'ALA', '101').
card_flavor_text('goblin assault'/'ALA', 'A goblin raid is a delicate gambit—a blend of stealth, precision, and screaming death.').
card_multiverse_id('goblin assault'/'ALA', '174939').

card_in_set('goblin deathraiders', 'ALA').
card_original_type('goblin deathraiders'/'ALA', 'Creature — Goblin Warrior').
card_original_text('goblin deathraiders'/'ALA', 'Trample').
card_first_print('goblin deathraiders', 'ALA').
card_image_name('goblin deathraiders'/'ALA', 'goblin deathraiders').
card_uid('goblin deathraiders'/'ALA', 'ALA:Goblin Deathraiders:goblin deathraiders').
card_rarity('goblin deathraiders'/'ALA', 'Common').
card_artist('goblin deathraiders'/'ALA', 'Raymond Swanland').
card_number('goblin deathraiders'/'ALA', '169').
card_flavor_text('goblin deathraiders'/'ALA', 'Every once in a while, when they aren\'t getting incinerated in lava, crushed under rock slides, or devoured by dragons, goblins experience moments of unmitigated glory in battle.').
card_multiverse_id('goblin deathraiders'/'ALA', '177596').

card_in_set('goblin mountaineer', 'ALA').
card_original_type('goblin mountaineer'/'ALA', 'Creature — Goblin Scout').
card_original_text('goblin mountaineer'/'ALA', 'Mountainwalk').
card_image_name('goblin mountaineer'/'ALA', 'goblin mountaineer').
card_uid('goblin mountaineer'/'ALA', 'ALA:Goblin Mountaineer:goblin mountaineer').
card_rarity('goblin mountaineer'/'ALA', 'Common').
card_artist('goblin mountaineer'/'ALA', 'Michael Ryan').
card_number('goblin mountaineer'/'ALA', '102').
card_flavor_text('goblin mountaineer'/'ALA', 'Goblin lairs sit high in Jund\'s mountains. This gives the goblins easy access to the sacrificial peaks above and lets them build up a head of steam before attacking prey down below.').
card_multiverse_id('goblin mountaineer'/'ALA', '174938').

card_in_set('godsire', 'ALA').
card_original_type('godsire'/'ALA', 'Creature — Beast').
card_original_text('godsire'/'ALA', 'Vigilance\n{T}: Put an 8/8 Beast creature token into play that\'s red, green, and white.').
card_first_print('godsire', 'ALA').
card_image_name('godsire'/'ALA', 'godsire').
card_uid('godsire'/'ALA', 'ALA:Godsire:godsire').
card_rarity('godsire'/'ALA', 'Mythic Rare').
card_artist('godsire'/'ALA', 'Jim Murray').
card_number('godsire'/'ALA', '170').
card_flavor_text('godsire'/'ALA', '\"Lay sacrifice to the godsire that it may spare us, but save some for his progeny who also will hunger.\"\n—Syeena, elvish godtoucher').
card_multiverse_id('godsire'/'ALA', '175105').

card_in_set('godtoucher', 'ALA').
card_original_type('godtoucher'/'ALA', 'Creature — Elf Cleric').
card_original_text('godtoucher'/'ALA', '{1}{W}, {T}: Prevent all damage that would be dealt to target creature with power 5 or greater this turn.').
card_first_print('godtoucher', 'ALA').
card_image_name('godtoucher'/'ALA', 'godtoucher').
card_uid('godtoucher'/'ALA', 'ALA:Godtoucher:godtoucher').
card_rarity('godtoucher'/'ALA', 'Common').
card_artist('godtoucher'/'ALA', 'Jesper Ejsing').
card_number('godtoucher'/'ALA', '133').
card_flavor_text('godtoucher'/'ALA', '\"Gargantuans die not in moments but in moons. There is still time to aid this noble ancient.\"').
card_multiverse_id('godtoucher'/'ALA', '174822').

card_in_set('grixis battlemage', 'ALA').
card_original_type('grixis battlemage'/'ALA', 'Creature — Human Wizard').
card_original_text('grixis battlemage'/'ALA', '{U}, {T}: Draw a card, then discard a card.\n{R}, {T}: Target creature can\'t block this turn.').
card_first_print('grixis battlemage', 'ALA').
card_image_name('grixis battlemage'/'ALA', 'grixis battlemage').
card_uid('grixis battlemage'/'ALA', 'ALA:Grixis Battlemage:grixis battlemage').
card_rarity('grixis battlemage'/'ALA', 'Uncommon').
card_artist('grixis battlemage'/'ALA', 'Nils Hamm').
card_number('grixis battlemage'/'ALA', '78').
card_flavor_text('grixis battlemage'/'ALA', 'Vitals of Grixis who eschew undeath must scrape and scratch to retain their mortality. The result is a breed of inventive mages.').
card_multiverse_id('grixis battlemage'/'ALA', '174868').

card_in_set('grixis charm', 'ALA').
card_original_type('grixis charm'/'ALA', 'Instant').
card_original_text('grixis charm'/'ALA', 'Choose one Return target permanent to its owner\'s hand; or target creature gets -4/-4 until end of turn; or creatures you control get +2/+0 until end of turn.').
card_first_print('grixis charm', 'ALA').
card_image_name('grixis charm'/'ALA', 'grixis charm').
card_uid('grixis charm'/'ALA', 'ALA:Grixis Charm:grixis charm').
card_rarity('grixis charm'/'ALA', 'Uncommon').
card_artist('grixis charm'/'ALA', 'Lars Grant-West').
card_number('grixis charm'/'ALA', '171').
card_flavor_text('grixis charm'/'ALA', '\"So many choices. Shall I choose loathing, hate, or malice today?\"\n—Eliza of the Keep').
card_multiverse_id('grixis charm'/'ALA', '137927').

card_in_set('grixis panorama', 'ALA').
card_original_type('grixis panorama'/'ALA', 'Land').
card_original_text('grixis panorama'/'ALA', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Grixis Panorama: Search your library for a basic Island, Swamp, or Mountain card and put it into play tapped. Then shuffle your library.').
card_first_print('grixis panorama', 'ALA').
card_image_name('grixis panorama'/'ALA', 'grixis panorama').
card_uid('grixis panorama'/'ALA', 'ALA:Grixis Panorama:grixis panorama').
card_rarity('grixis panorama'/'ALA', 'Common').
card_artist('grixis panorama'/'ALA', 'Nils Hamm').
card_number('grixis panorama'/'ALA', '224').
card_flavor_text('grixis panorama'/'ALA', 'There is no height above Grixis that is free from the stench of death.').
card_multiverse_id('grixis panorama'/'ALA', '179433').

card_in_set('guardians of akrasa', 'ALA').
card_original_type('guardians of akrasa'/'ALA', 'Creature — Human Soldier').
card_original_text('guardians of akrasa'/'ALA', 'Defender\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('guardians of akrasa', 'ALA').
card_image_name('guardians of akrasa'/'ALA', 'guardians of akrasa').
card_uid('guardians of akrasa'/'ALA', 'ALA:Guardians of Akrasa:guardians of akrasa').
card_rarity('guardians of akrasa'/'ALA', 'Common').
card_artist('guardians of akrasa'/'ALA', 'Alan Pollack').
card_number('guardians of akrasa'/'ALA', '12').
card_flavor_text('guardians of akrasa'/'ALA', '\"Only by the bravery of those who put loyalty above glory is our home kept safe.\"\n—Elspeth').
card_multiverse_id('guardians of akrasa'/'ALA', '175103').

card_in_set('gustrider exuberant', 'ALA').
card_original_type('gustrider exuberant'/'ALA', 'Creature — Human Wizard').
card_original_text('gustrider exuberant'/'ALA', 'Flying\nSacrifice Gustrider Exuberant: Creatures you control with power 5 or greater gain flying until end of turn.').
card_first_print('gustrider exuberant', 'ALA').
card_image_name('gustrider exuberant'/'ALA', 'gustrider exuberant').
card_uid('gustrider exuberant'/'ALA', 'ALA:Gustrider Exuberant:gustrider exuberant').
card_rarity('gustrider exuberant'/'ALA', 'Common').
card_artist('gustrider exuberant'/'ALA', 'Wayne Reynolds').
card_number('gustrider exuberant'/'ALA', '13').
card_flavor_text('gustrider exuberant'/'ALA', '\"The elves claim the canopy. The nacatl claim the mountains. I suppose you think we ought to stay on the jungle floor?\"').
card_multiverse_id('gustrider exuberant'/'ALA', '174805').

card_in_set('hell\'s thunder', 'ALA').
card_original_type('hell\'s thunder'/'ALA', 'Creature — Elemental').
card_original_text('hell\'s thunder'/'ALA', 'Flying, haste\nAt end of turn, sacrifice Hell\'s Thunder.\nUnearth {4}{R} ({4}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('hell\'s thunder', 'ALA').
card_image_name('hell\'s thunder'/'ALA', 'hell\'s thunder').
card_uid('hell\'s thunder'/'ALA', 'ALA:Hell\'s Thunder:hell\'s thunder').
card_rarity('hell\'s thunder'/'ALA', 'Rare').
card_artist('hell\'s thunder'/'ALA', 'Karl Kopinski').
card_number('hell\'s thunder'/'ALA', '103').
card_multiverse_id('hell\'s thunder'/'ALA', '176455').

card_in_set('hellkite overlord', 'ALA').
card_original_type('hellkite overlord'/'ALA', 'Creature — Dragon').
card_original_text('hellkite overlord'/'ALA', 'Flying, trample, haste\n{R}: Hellkite Overlord gets +1/+0 until end of turn.\n{B}{G}: Regenerate Hellkite Overlord.').
card_image_name('hellkite overlord'/'ALA', 'hellkite overlord').
card_uid('hellkite overlord'/'ALA', 'ALA:Hellkite Overlord:hellkite overlord').
card_rarity('hellkite overlord'/'ALA', 'Mythic Rare').
card_artist('hellkite overlord'/'ALA', 'Justin Sweet').
card_number('hellkite overlord'/'ALA', '172').
card_flavor_text('hellkite overlord'/'ALA', '\"The dragon has no pretense of compassion, no false mask of civilization—just hunger, heat, and need.\"\n—Sarkhan Vol').
card_multiverse_id('hellkite overlord'/'ALA', '175057').

card_in_set('hindering light', 'ALA').
card_original_type('hindering light'/'ALA', 'Instant').
card_original_text('hindering light'/'ALA', 'Counter target spell that targets you or a permanent you control.\nDraw a card.').
card_first_print('hindering light', 'ALA').
card_image_name('hindering light'/'ALA', 'hindering light').
card_uid('hindering light'/'ALA', 'ALA:Hindering Light:hindering light').
card_rarity('hindering light'/'ALA', 'Common').
card_artist('hindering light'/'ALA', 'Chris Rahn').
card_number('hindering light'/'ALA', '173').
card_flavor_text('hindering light'/'ALA', 'Centuries of careful practice have elevated the casting of protective spells to an art form. What little offensive magic remains on Bant stands little chance of breaching them.').
card_multiverse_id('hindering light'/'ALA', '177598').

card_in_set('hissing iguanar', 'ALA').
card_original_type('hissing iguanar'/'ALA', 'Creature — Lizard').
card_original_text('hissing iguanar'/'ALA', 'Whenever another creature is put into a graveyard from play, you may have Hissing Iguanar deal 1 damage to target player.').
card_first_print('hissing iguanar', 'ALA').
card_image_name('hissing iguanar'/'ALA', 'hissing iguanar').
card_uid('hissing iguanar'/'ALA', 'ALA:Hissing Iguanar:hissing iguanar').
card_rarity('hissing iguanar'/'ALA', 'Common').
card_artist('hissing iguanar'/'ALA', 'Brandon Kitkouski').
card_number('hissing iguanar'/'ALA', '104').
card_flavor_text('hissing iguanar'/'ALA', 'Viashino thrashes keep iguanars as hunting companions, giving them wounded captives as playthings.').
card_multiverse_id('hissing iguanar'/'ALA', '174873').

card_in_set('immortal coil', 'ALA').
card_original_type('immortal coil'/'ALA', 'Artifact').
card_original_text('immortal coil'/'ALA', '{T}, Remove two cards in your graveyard from the game: Draw a card.\nIf damage would be dealt to you, prevent that damage. Remove a card in your graveyard from the game for each 1 damage prevented this way.\nWhen there are no cards in your graveyard, you lose the game.').
card_first_print('immortal coil', 'ALA').
card_image_name('immortal coil'/'ALA', 'immortal coil').
card_uid('immortal coil'/'ALA', 'ALA:Immortal Coil:immortal coil').
card_rarity('immortal coil'/'ALA', 'Rare').
card_artist('immortal coil'/'ALA', 'Dan Scott').
card_number('immortal coil'/'ALA', '79').
card_multiverse_id('immortal coil'/'ALA', '175125').

card_in_set('incurable ogre', 'ALA').
card_original_type('incurable ogre'/'ALA', 'Creature — Ogre Mutant').
card_original_text('incurable ogre'/'ALA', '').
card_first_print('incurable ogre', 'ALA').
card_image_name('incurable ogre'/'ALA', 'incurable ogre').
card_uid('incurable ogre'/'ALA', 'ALA:Incurable Ogre:incurable ogre').
card_rarity('incurable ogre'/'ALA', 'Common').
card_artist('incurable ogre'/'ALA', 'Carl Critchlow').
card_number('incurable ogre'/'ALA', '105').
card_flavor_text('incurable ogre'/'ALA', 'Each mutation causes the incurables to look vastly different from one another. They are left with only one thing in common: their insatiable lust for the slaughter.').
card_multiverse_id('incurable ogre'/'ALA', '174806').

card_in_set('infest', 'ALA').
card_original_type('infest'/'ALA', 'Sorcery').
card_original_text('infest'/'ALA', 'All creatures get -2/-2 until end of turn.').
card_image_name('infest'/'ALA', 'infest').
card_uid('infest'/'ALA', 'ALA:Infest:infest').
card_rarity('infest'/'ALA', 'Uncommon').
card_artist('infest'/'ALA', 'Karl Kopinski').
card_number('infest'/'ALA', '80').
card_flavor_text('infest'/'ALA', '\"This is why we don\'t go out in banewasp weather.\"\n—Rannon, Vithian holdout').
card_multiverse_id('infest'/'ALA', '179424').

card_in_set('invincible hymn', 'ALA').
card_original_type('invincible hymn'/'ALA', 'Sorcery').
card_original_text('invincible hymn'/'ALA', 'Count the number of cards in your library. Your life total becomes that number.').
card_first_print('invincible hymn', 'ALA').
card_image_name('invincible hymn'/'ALA', 'invincible hymn').
card_uid('invincible hymn'/'ALA', 'ALA:Invincible Hymn:invincible hymn').
card_rarity('invincible hymn'/'ALA', 'Rare').
card_artist('invincible hymn'/'ALA', 'Matt Stewart').
card_number('invincible hymn'/'ALA', '14').
card_flavor_text('invincible hymn'/'ALA', 'Bantians believe those who are born into the highest caste and live a life of discipline and virtue can ascend to become angels.').
card_multiverse_id('invincible hymn'/'ALA', '175039').

card_in_set('island', 'ALA').
card_original_type('island'/'ALA', 'Basic Land — Island').
card_original_text('island'/'ALA', 'U').
card_image_name('island'/'ALA', 'island1').
card_uid('island'/'ALA', 'ALA:Island:island1').
card_rarity('island'/'ALA', 'Basic Land').
card_artist('island'/'ALA', 'Michael Komarck').
card_number('island'/'ALA', '234').
card_multiverse_id('island'/'ALA', '174977').

card_in_set('island', 'ALA').
card_original_type('island'/'ALA', 'Basic Land — Island').
card_original_text('island'/'ALA', 'U').
card_image_name('island'/'ALA', 'island2').
card_uid('island'/'ALA', 'ALA:Island:island2').
card_rarity('island'/'ALA', 'Basic Land').
card_artist('island'/'ALA', 'Chippy').
card_number('island'/'ALA', '235').
card_multiverse_id('island'/'ALA', '174978').

card_in_set('island', 'ALA').
card_original_type('island'/'ALA', 'Basic Land — Island').
card_original_text('island'/'ALA', 'U').
card_image_name('island'/'ALA', 'island3').
card_uid('island'/'ALA', 'ALA:Island:island3').
card_rarity('island'/'ALA', 'Basic Land').
card_artist('island'/'ALA', 'Chippy').
card_number('island'/'ALA', '236').
card_multiverse_id('island'/'ALA', '174980').

card_in_set('island', 'ALA').
card_original_type('island'/'ALA', 'Basic Land — Island').
card_original_text('island'/'ALA', 'U').
card_image_name('island'/'ALA', 'island4').
card_uid('island'/'ALA', 'ALA:Island:island4').
card_rarity('island'/'ALA', 'Basic Land').
card_artist('island'/'ALA', 'Mark Tedin').
card_number('island'/'ALA', '237').
card_multiverse_id('island'/'ALA', '174979').

card_in_set('jhessian infiltrator', 'ALA').
card_original_type('jhessian infiltrator'/'ALA', 'Creature — Human Rogue').
card_original_text('jhessian infiltrator'/'ALA', 'Jhessian Infiltrator is unblockable.').
card_first_print('jhessian infiltrator', 'ALA').
card_image_name('jhessian infiltrator'/'ALA', 'jhessian infiltrator').
card_uid('jhessian infiltrator'/'ALA', 'ALA:Jhessian Infiltrator:jhessian infiltrator').
card_rarity('jhessian infiltrator'/'ALA', 'Uncommon').
card_artist('jhessian infiltrator'/'ALA', 'Donato Giancola').
card_number('jhessian infiltrator'/'ALA', '174').
card_flavor_text('jhessian infiltrator'/'ALA', 'The Jhessian navy makes successful raids on Valeron\'s coastal towns thanks to their spies planted during peacetime.').
card_multiverse_id('jhessian infiltrator'/'ALA', '175392').

card_in_set('jhessian lookout', 'ALA').
card_original_type('jhessian lookout'/'ALA', 'Creature — Human Scout').
card_original_text('jhessian lookout'/'ALA', '').
card_first_print('jhessian lookout', 'ALA').
card_image_name('jhessian lookout'/'ALA', 'jhessian lookout').
card_uid('jhessian lookout'/'ALA', 'ALA:Jhessian Lookout:jhessian lookout').
card_rarity('jhessian lookout'/'ALA', 'Common').
card_artist('jhessian lookout'/'ALA', 'Donato Giancola').
card_number('jhessian lookout'/'ALA', '46').
card_flavor_text('jhessian lookout'/'ALA', 'She stands ready, always watchful, knowing that weeks of peace and serenity can be overturned by a single distant sail.').
card_multiverse_id('jhessian lookout'/'ALA', '176428').

card_in_set('jund battlemage', 'ALA').
card_original_type('jund battlemage'/'ALA', 'Creature — Human Shaman').
card_original_text('jund battlemage'/'ALA', '{B}, {T}: Target player loses 1 life.\n{G}, {T}: Put a 1/1 green Saproling creature token into play.').
card_first_print('jund battlemage', 'ALA').
card_image_name('jund battlemage'/'ALA', 'jund battlemage').
card_uid('jund battlemage'/'ALA', 'ALA:Jund Battlemage:jund battlemage').
card_rarity('jund battlemage'/'ALA', 'Uncommon').
card_artist('jund battlemage'/'ALA', 'Vance Kovacs').
card_number('jund battlemage'/'ALA', '106').
card_flavor_text('jund battlemage'/'ALA', '\"Of your blood I will make my mead, an offering to the thirsty jungle.\"').
card_multiverse_id('jund battlemage'/'ALA', '175050').

card_in_set('jund charm', 'ALA').
card_original_type('jund charm'/'ALA', 'Instant').
card_original_text('jund charm'/'ALA', 'Choose one Remove target player\'s graveyard from the game; or Jund Charm deals 2 damage to each creature; or put two +1/+1 counters on target creature.').
card_first_print('jund charm', 'ALA').
card_image_name('jund charm'/'ALA', 'jund charm').
card_uid('jund charm'/'ALA', 'ALA:Jund Charm:jund charm').
card_rarity('jund charm'/'ALA', 'Uncommon').
card_artist('jund charm'/'ALA', 'Brandon Kitkouski').
card_number('jund charm'/'ALA', '175').
card_flavor_text('jund charm'/'ALA', 'Jund pulses with raw energy, waiting for shamans to shape it into useful form.').
card_multiverse_id('jund charm'/'ALA', '137900').

card_in_set('jund panorama', 'ALA').
card_original_type('jund panorama'/'ALA', 'Land').
card_original_text('jund panorama'/'ALA', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Jund Panorama: Search your library for a basic Swamp, Mountain, or Forest card and put it into play tapped. Then shuffle your library.').
card_first_print('jund panorama', 'ALA').
card_image_name('jund panorama'/'ALA', 'jund panorama').
card_uid('jund panorama'/'ALA', 'ALA:Jund Panorama:jund panorama').
card_rarity('jund panorama'/'ALA', 'Common').
card_artist('jund panorama'/'ALA', 'Jaime Jones').
card_number('jund panorama'/'ALA', '225').
card_flavor_text('jund panorama'/'ALA', 'The sky is the dragon\'s throne, the seat from which it issues its fiery decrees.').
card_multiverse_id('jund panorama'/'ALA', '179425').

card_in_set('jungle shrine', 'ALA').
card_original_type('jungle shrine'/'ALA', 'Land').
card_original_text('jungle shrine'/'ALA', 'Jungle Shrine comes into play tapped.\n{T}: Add {R}, {G}, or {W} to your mana pool.').
card_first_print('jungle shrine', 'ALA').
card_image_name('jungle shrine'/'ALA', 'jungle shrine').
card_uid('jungle shrine'/'ALA', 'ALA:Jungle Shrine:jungle shrine').
card_rarity('jungle shrine'/'ALA', 'Uncommon').
card_artist('jungle shrine'/'ALA', 'Wayne Reynolds').
card_number('jungle shrine'/'ALA', '226').
card_flavor_text('jungle shrine'/'ALA', 'On Naya, ambition and treachery are scarce, hunted nearly to extinction by the awe owed to terrestrial gods.').
card_multiverse_id('jungle shrine'/'ALA', '175060').

card_in_set('jungle weaver', 'ALA').
card_original_type('jungle weaver'/'ALA', 'Creature — Spider').
card_original_text('jungle weaver'/'ALA', 'Reach (This can block creatures with flying.)\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('jungle weaver', 'ALA').
card_image_name('jungle weaver'/'ALA', 'jungle weaver').
card_uid('jungle weaver'/'ALA', 'ALA:Jungle Weaver:jungle weaver').
card_rarity('jungle weaver'/'ALA', 'Common').
card_artist('jungle weaver'/'ALA', 'Trevor Hairsine').
card_number('jungle weaver'/'ALA', '134').
card_flavor_text('jungle weaver'/'ALA', 'Weavers\' webs wall off swaths of territory more effectively than any portcullis made of iron.').
card_multiverse_id('jungle weaver'/'ALA', '175262').

card_in_set('kathari screecher', 'ALA').
card_original_type('kathari screecher'/'ALA', 'Creature — Bird Soldier').
card_original_text('kathari screecher'/'ALA', 'Flying\nUnearth {2}{U} ({2}{U}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('kathari screecher', 'ALA').
card_image_name('kathari screecher'/'ALA', 'kathari screecher').
card_uid('kathari screecher'/'ALA', 'ALA:Kathari Screecher:kathari screecher').
card_rarity('kathari screecher'/'ALA', 'Common').
card_artist('kathari screecher'/'ALA', 'Pete Venters').
card_number('kathari screecher'/'ALA', '47').
card_multiverse_id('kathari screecher'/'ALA', '174837').

card_in_set('kederekt creeper', 'ALA').
card_original_type('kederekt creeper'/'ALA', 'Creature — Horror').
card_original_text('kederekt creeper'/'ALA', 'Deathtouch (Whenever this creature deals damage to a creature, destroy that creature.)\nKederekt Creeper can\'t be blocked except by two or more creatures.').
card_first_print('kederekt creeper', 'ALA').
card_image_name('kederekt creeper'/'ALA', 'kederekt creeper').
card_uid('kederekt creeper'/'ALA', 'ALA:Kederekt Creeper:kederekt creeper').
card_rarity('kederekt creeper'/'ALA', 'Common').
card_artist('kederekt creeper'/'ALA', 'Mark Hyzer').
card_number('kederekt creeper'/'ALA', '176').
card_flavor_text('kederekt creeper'/'ALA', 'Bloated with venom, it crawls Grixis looking for victims to ooze onto.').
card_multiverse_id('kederekt creeper'/'ALA', '179430').

card_in_set('kederekt leviathan', 'ALA').
card_original_type('kederekt leviathan'/'ALA', 'Creature — Leviathan').
card_original_text('kederekt leviathan'/'ALA', 'When Kederekt Leviathan comes into play, return all other nonland permanents to their owners\' hands.\nUnearth {6}{U} ({6}{U}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('kederekt leviathan', 'ALA').
card_image_name('kederekt leviathan'/'ALA', 'kederekt leviathan').
card_uid('kederekt leviathan'/'ALA', 'ALA:Kederekt Leviathan:kederekt leviathan').
card_rarity('kederekt leviathan'/'ALA', 'Rare').
card_artist('kederekt leviathan'/'ALA', 'Mark Hyzer').
card_number('kederekt leviathan'/'ALA', '48').
card_multiverse_id('kederekt leviathan'/'ALA', '174882').

card_in_set('keeper of progenitus', 'ALA').
card_original_type('keeper of progenitus'/'ALA', 'Creature — Elf Druid').
card_original_text('keeper of progenitus'/'ALA', 'Whenever a player taps a Mountain, Forest, or Plains for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_first_print('keeper of progenitus', 'ALA').
card_image_name('keeper of progenitus'/'ALA', 'keeper of progenitus').
card_uid('keeper of progenitus'/'ALA', 'ALA:Keeper of Progenitus:keeper of progenitus').
card_rarity('keeper of progenitus'/'ALA', 'Rare').
card_artist('keeper of progenitus'/'ALA', 'Kev Walker').
card_number('keeper of progenitus'/'ALA', '135').
card_flavor_text('keeper of progenitus'/'ALA', 'He watches over the valley where Progenitus sleeps, culling signs from its steaming breaths.').
card_multiverse_id('keeper of progenitus'/'ALA', '175033').

card_in_set('kiss of the amesha', 'ALA').
card_original_type('kiss of the amesha'/'ALA', 'Sorcery').
card_original_text('kiss of the amesha'/'ALA', 'Target player gains 7 life and draws two cards.').
card_first_print('kiss of the amesha', 'ALA').
card_image_name('kiss of the amesha'/'ALA', 'kiss of the amesha').
card_uid('kiss of the amesha'/'ALA', 'ALA:Kiss of the Amesha:kiss of the amesha').
card_rarity('kiss of the amesha'/'ALA', 'Uncommon').
card_artist('kiss of the amesha'/'ALA', 'Todd Lockwood').
card_number('kiss of the amesha'/'ALA', '177').
card_flavor_text('kiss of the amesha'/'ALA', 'Once a year, Akrasa holds a joust that draws knights and their leotaus from every nation on Bant. The prize is an Akrasan sigil, but the true reward is the angel\'s kiss.').
card_multiverse_id('kiss of the amesha'/'ALA', '174976').

card_in_set('knight of the skyward eye', 'ALA').
card_original_type('knight of the skyward eye'/'ALA', 'Creature — Human Knight').
card_original_text('knight of the skyward eye'/'ALA', '{3}{G}: Knight of the Skyward Eye gets +3/+3 until end of turn. Play this ability only once each turn.').
card_first_print('knight of the skyward eye', 'ALA').
card_image_name('knight of the skyward eye'/'ALA', 'knight of the skyward eye').
card_uid('knight of the skyward eye'/'ALA', 'ALA:Knight of the Skyward Eye:knight of the skyward eye').
card_rarity('knight of the skyward eye'/'ALA', 'Common').
card_artist('knight of the skyward eye'/'ALA', 'Matt Stewart').
card_number('knight of the skyward eye'/'ALA', '15').
card_flavor_text('knight of the skyward eye'/'ALA', 'The Order of the Skyward Eye does the bidding of an evil force, unwittingly stirring fear and mistrust across Bant in accordance with his plans.').
card_multiverse_id('knight of the skyward eye'/'ALA', '175047').

card_in_set('knight of the white orchid', 'ALA').
card_original_type('knight of the white orchid'/'ALA', 'Creature — Human Knight').
card_original_text('knight of the white orchid'/'ALA', 'First strike\nWhen Knight of the White Orchid comes into play, if an opponent controls more lands than you, you may search your library for a Plains card, put it into play, then shuffle your library.').
card_first_print('knight of the white orchid', 'ALA').
card_image_name('knight of the white orchid'/'ALA', 'knight of the white orchid').
card_uid('knight of the white orchid'/'ALA', 'ALA:Knight of the White Orchid:knight of the white orchid').
card_rarity('knight of the white orchid'/'ALA', 'Rare').
card_artist('knight of the white orchid'/'ALA', 'Mark Zug').
card_number('knight of the white orchid'/'ALA', '16').
card_flavor_text('knight of the white orchid'/'ALA', 'Both guide and guard on open plain.').
card_multiverse_id('knight of the white orchid'/'ALA', '178094').

card_in_set('knight-captain of eos', 'ALA').
card_original_type('knight-captain of eos'/'ALA', 'Creature — Human Knight').
card_original_text('knight-captain of eos'/'ALA', 'When Knight-Captain of Eos comes into play, put two 1/1 white Soldier creature tokens into play.\n{W}, Sacrifice a Soldier: Prevent all combat damage that would be dealt this turn.').
card_first_print('knight-captain of eos', 'ALA').
card_image_name('knight-captain of eos'/'ALA', 'knight-captain of eos').
card_uid('knight-captain of eos'/'ALA', 'ALA:Knight-Captain of Eos:knight-captain of eos').
card_rarity('knight-captain of eos'/'ALA', 'Rare').
card_artist('knight-captain of eos'/'ALA', 'Chris Rahn').
card_number('knight-captain of eos'/'ALA', '17').
card_flavor_text('knight-captain of eos'/'ALA', 'The strength of Bant\'s caste system is the unfailing loyalty of its meekest members.').
card_multiverse_id('knight-captain of eos'/'ALA', '179421').

card_in_set('kresh the bloodbraided', 'ALA').
card_original_type('kresh the bloodbraided'/'ALA', 'Legendary Creature — Human Warrior').
card_original_text('kresh the bloodbraided'/'ALA', 'Whenever another creature is put into a graveyard from play, you may put X +1/+1 counters on Kresh the Bloodbraided, where X is that creature\'s power.').
card_first_print('kresh the bloodbraided', 'ALA').
card_image_name('kresh the bloodbraided'/'ALA', 'kresh the bloodbraided').
card_uid('kresh the bloodbraided'/'ALA', 'ALA:Kresh the Bloodbraided:kresh the bloodbraided').
card_rarity('kresh the bloodbraided'/'ALA', 'Mythic Rare').
card_artist('kresh the bloodbraided'/'ALA', 'Raymond Swanland').
card_number('kresh the bloodbraided'/'ALA', '178').
card_flavor_text('kresh the bloodbraided'/'ALA', 'Each of his twenty-two braids is bound with bone and leather from a foe.').
card_multiverse_id('kresh the bloodbraided'/'ALA', '174876').

card_in_set('lich\'s mirror', 'ALA').
card_original_type('lich\'s mirror'/'ALA', 'Artifact').
card_original_text('lich\'s mirror'/'ALA', 'If you would lose the game, instead shuffle your hand, your graveyard, and all permanents you own into your library, then draw seven cards and your life total becomes 20.').
card_first_print('lich\'s mirror', 'ALA').
card_image_name('lich\'s mirror'/'ALA', 'lich\'s mirror').
card_uid('lich\'s mirror'/'ALA', 'ALA:Lich\'s Mirror:lich\'s mirror').
card_rarity('lich\'s mirror'/'ALA', 'Mythic Rare').
card_artist('lich\'s mirror'/'ALA', 'Ash Wood').
card_number('lich\'s mirror'/'ALA', '210').
card_flavor_text('lich\'s mirror'/'ALA', 'It shows not what you are but what you were.').
card_multiverse_id('lich\'s mirror'/'ALA', '174818').

card_in_set('lightning talons', 'ALA').
card_original_type('lightning talons'/'ALA', 'Enchantment — Aura').
card_original_text('lightning talons'/'ALA', 'Enchant creature\nEnchanted creature gets +3/+0 and has first strike.').
card_first_print('lightning talons', 'ALA').
card_image_name('lightning talons'/'ALA', 'lightning talons').
card_uid('lightning talons'/'ALA', 'ALA:Lightning Talons:lightning talons').
card_rarity('lightning talons'/'ALA', 'Common').
card_artist('lightning talons'/'ALA', 'Pete Venters').
card_number('lightning talons'/'ALA', '107').
card_flavor_text('lightning talons'/'ALA', '\"This is going to hurt a lot, but on the bright side, you\'ll be dead soon.\"\n—Sedris, the Traitor King').
card_multiverse_id('lightning talons'/'ALA', '176446').

card_in_set('lush growth', 'ALA').
card_original_type('lush growth'/'ALA', 'Enchantment — Aura').
card_original_text('lush growth'/'ALA', 'Enchant land\nEnchanted land is a Mountain, Forest, and Plains.').
card_first_print('lush growth', 'ALA').
card_image_name('lush growth'/'ALA', 'lush growth').
card_uid('lush growth'/'ALA', 'ALA:Lush Growth:lush growth').
card_rarity('lush growth'/'ALA', 'Common').
card_artist('lush growth'/'ALA', 'Jesper Ejsing').
card_number('lush growth'/'ALA', '136').
card_flavor_text('lush growth'/'ALA', 'Naya\'s bounty spills over upon itself. Each leaf and flower struggles against the next with thrashing thorns, acidic pollen, and strangling roots.').
card_multiverse_id('lush growth'/'ALA', '176441').

card_in_set('magma spray', 'ALA').
card_original_type('magma spray'/'ALA', 'Instant').
card_original_text('magma spray'/'ALA', 'Magma Spray deals 2 damage to target creature. If that creature would be put into a graveyard this turn, remove it from the game instead.').
card_image_name('magma spray'/'ALA', 'magma spray').
card_uid('magma spray'/'ALA', 'ALA:Magma Spray:magma spray').
card_rarity('magma spray'/'ALA', 'Common').
card_artist('magma spray'/'ALA', 'Jarreau Wimberly').
card_number('magma spray'/'ALA', '108').
card_flavor_text('magma spray'/'ALA', '\"Jund is a wounded world. Beware its searing blood spilling out onto the surface.\"\n—Rakka Mar').
card_multiverse_id('magma spray'/'ALA', '175245').

card_in_set('manaplasm', 'ALA').
card_original_type('manaplasm'/'ALA', 'Creature — Ooze').
card_original_text('manaplasm'/'ALA', 'Whenever you play a spell, Manaplasm gets +X/+X until end of turn, where X is that spell\'s converted mana cost.').
card_first_print('manaplasm', 'ALA').
card_image_name('manaplasm'/'ALA', 'manaplasm').
card_uid('manaplasm'/'ALA', 'ALA:Manaplasm:manaplasm').
card_rarity('manaplasm'/'ALA', 'Rare').
card_artist('manaplasm'/'ALA', 'Daarken').
card_number('manaplasm'/'ALA', '138').
card_flavor_text('manaplasm'/'ALA', 'Urak froze when he heard it. That was his first mistake. He turned and cast a dramatic ward spell. That was his last.').
card_multiverse_id('manaplasm'/'ALA', '175006').

card_in_set('marble chalice', 'ALA').
card_original_type('marble chalice'/'ALA', 'Artifact').
card_original_text('marble chalice'/'ALA', '{T}: You gain 1 life.').
card_first_print('marble chalice', 'ALA').
card_image_name('marble chalice'/'ALA', 'marble chalice').
card_uid('marble chalice'/'ALA', 'ALA:Marble Chalice:marble chalice').
card_rarity('marble chalice'/'ALA', 'Common').
card_artist('marble chalice'/'ALA', 'Howard Lyon').
card_number('marble chalice'/'ALA', '18').
card_flavor_text('marble chalice'/'ALA', 'The cup was a gift from the sphinx Tameron, who hoped that those who drank from it would live long enough to decrypt the sphinxes\' wisdom.').
card_multiverse_id('marble chalice'/'ALA', '175249').

card_in_set('master of etherium', 'ALA').
card_original_type('master of etherium'/'ALA', 'Artifact Creature — Vedalken Wizard').
card_original_text('master of etherium'/'ALA', 'Master of Etherium\'s power and toughness are each equal to the number of artifacts you control.\nOther artifact creatures you control get +1/+1.').
card_first_print('master of etherium', 'ALA').
card_image_name('master of etherium'/'ALA', 'master of etherium').
card_uid('master of etherium'/'ALA', 'ALA:Master of Etherium:master of etherium').
card_rarity('master of etherium'/'ALA', 'Rare').
card_artist('master of etherium'/'ALA', 'Matt Cavotta').
card_number('master of etherium'/'ALA', '49').
card_flavor_text('master of etherium'/'ALA', '\"Only a mind unfettered with the concerns of the flesh can see the world as it truly is.\"').
card_multiverse_id('master of etherium'/'ALA', '175114').

card_in_set('mayael the anima', 'ALA').
card_original_type('mayael the anima'/'ALA', 'Legendary Creature — Elf Shaman').
card_original_text('mayael the anima'/'ALA', '{3}{R}{G}{W}, {T}: Look at the top five cards of your library. You may put a creature card with power 5 or greater from among them into play. Put the rest on the bottom of your library in any order.').
card_first_print('mayael the anima', 'ALA').
card_image_name('mayael the anima'/'ALA', 'mayael the anima').
card_uid('mayael the anima'/'ALA', 'ALA:Mayael the Anima:mayael the anima').
card_rarity('mayael the anima'/'ALA', 'Mythic Rare').
card_artist('mayael the anima'/'ALA', 'Jason Chan').
card_number('mayael the anima'/'ALA', '179').
card_flavor_text('mayael the anima'/'ALA', 'The sacred Anima\'s eyes are blind to all but the grandest truths.').
card_multiverse_id('mayael the anima'/'ALA', '175058').

card_in_set('memory erosion', 'ALA').
card_original_type('memory erosion'/'ALA', 'Enchantment').
card_original_text('memory erosion'/'ALA', 'Whenever an opponent plays a spell, that player puts the top two cards of his or her library into his or her graveyard.').
card_first_print('memory erosion', 'ALA').
card_image_name('memory erosion'/'ALA', 'memory erosion').
card_uid('memory erosion'/'ALA', 'ALA:Memory Erosion:memory erosion').
card_rarity('memory erosion'/'ALA', 'Rare').
card_artist('memory erosion'/'ALA', 'Howard Lyon').
card_number('memory erosion'/'ALA', '50').
card_flavor_text('memory erosion'/'ALA', '\"The Filigree Texts do not compel you to act in accordance with their precepts. They only specify the consequences should you fail.\"\n—Lodus of the Ethersworn').
card_multiverse_id('memory erosion'/'ALA', '175108').

card_in_set('metallurgeon', 'ALA').
card_original_type('metallurgeon'/'ALA', 'Artifact Creature — Human Artificer').
card_original_text('metallurgeon'/'ALA', '{W}, {T}: Regenerate target artifact.').
card_first_print('metallurgeon', 'ALA').
card_image_name('metallurgeon'/'ALA', 'metallurgeon').
card_uid('metallurgeon'/'ALA', 'ALA:Metallurgeon:metallurgeon').
card_rarity('metallurgeon'/'ALA', 'Uncommon').
card_artist('metallurgeon'/'ALA', 'Warren Mahy').
card_number('metallurgeon'/'ALA', '19').
card_flavor_text('metallurgeon'/'ALA', '\"By the time I got there, the heart had stopped. Fortunately, I was able to replace it with something better.\"').
card_multiverse_id('metallurgeon'/'ALA', '175113').

card_in_set('mighty emergence', 'ALA').
card_original_type('mighty emergence'/'ALA', 'Enchantment').
card_original_text('mighty emergence'/'ALA', 'Whenever a creature with power 5 or greater comes into play under your control, you may put two +1/+1 counters on it.').
card_first_print('mighty emergence', 'ALA').
card_image_name('mighty emergence'/'ALA', 'mighty emergence').
card_uid('mighty emergence'/'ALA', 'ALA:Mighty Emergence:mighty emergence').
card_rarity('mighty emergence'/'ALA', 'Uncommon').
card_artist('mighty emergence'/'ALA', 'Steve Prescott').
card_number('mighty emergence'/'ALA', '137').
card_flavor_text('mighty emergence'/'ALA', 'Why settle for mere enormity?').
card_multiverse_id('mighty emergence'/'ALA', '178097').

card_in_set('mindlock orb', 'ALA').
card_original_type('mindlock orb'/'ALA', 'Artifact').
card_original_text('mindlock orb'/'ALA', 'Players can\'t search libraries.').
card_first_print('mindlock orb', 'ALA').
card_image_name('mindlock orb'/'ALA', 'mindlock orb').
card_uid('mindlock orb'/'ALA', 'ALA:Mindlock Orb:mindlock orb').
card_rarity('mindlock orb'/'ALA', 'Rare').
card_artist('mindlock orb'/'ALA', 'rk post').
card_number('mindlock orb'/'ALA', '51').
card_flavor_text('mindlock orb'/'ALA', 'Rogue mechanists once rummaged through the wastes of the Tidehollow, constructing unauthorized golems to threaten the hegemony of the sphinxes. The orbs put a stop to that.').
card_multiverse_id('mindlock orb'/'ALA', '175385').

card_in_set('minion reflector', 'ALA').
card_original_type('minion reflector'/'ALA', 'Artifact').
card_original_text('minion reflector'/'ALA', 'Whenever a nontoken creature comes into play under your control, you may pay {2}. If you do, put a token into play that\'s a copy of that creature. That token has haste and \"At end of turn, sacrifice this permanent.\"').
card_first_print('minion reflector', 'ALA').
card_image_name('minion reflector'/'ALA', 'minion reflector').
card_uid('minion reflector'/'ALA', 'ALA:Minion Reflector:minion reflector').
card_rarity('minion reflector'/'ALA', 'Rare').
card_artist('minion reflector'/'ALA', 'Mark Tedin').
card_number('minion reflector'/'ALA', '211').
card_flavor_text('minion reflector'/'ALA', 'To crack a mirror is to provide an opening for the reflection to escape.').
card_multiverse_id('minion reflector'/'ALA', '174796').

card_in_set('mosstodon', 'ALA').
card_original_type('mosstodon'/'ALA', 'Creature — Plant Elephant').
card_original_text('mosstodon'/'ALA', '{1}: Target creature with power 5 or greater gains trample until end of turn.').
card_first_print('mosstodon', 'ALA').
card_image_name('mosstodon'/'ALA', 'mosstodon').
card_uid('mosstodon'/'ALA', 'ALA:Mosstodon:mosstodon').
card_rarity('mosstodon'/'ALA', 'Common').
card_artist('mosstodon'/'ALA', 'Paolo Parente').
card_number('mosstodon'/'ALA', '139').
card_flavor_text('mosstodon'/'ALA', 'Whether gargantuans are the manifested will of Progenitus or simply the result of overabundant resources is moot when a herd of them is thundering at you.').
card_multiverse_id('mosstodon'/'ALA', '174787').

card_in_set('mountain', 'ALA').
card_original_type('mountain'/'ALA', 'Basic Land — Mountain').
card_original_text('mountain'/'ALA', 'R').
card_image_name('mountain'/'ALA', 'mountain1').
card_uid('mountain'/'ALA', 'ALA:Mountain:mountain1').
card_rarity('mountain'/'ALA', 'Basic Land').
card_artist('mountain'/'ALA', 'Mark Tedin').
card_number('mountain'/'ALA', '242').
card_multiverse_id('mountain'/'ALA', '175019').

card_in_set('mountain', 'ALA').
card_original_type('mountain'/'ALA', 'Basic Land — Mountain').
card_original_text('mountain'/'ALA', 'R').
card_image_name('mountain'/'ALA', 'mountain2').
card_uid('mountain'/'ALA', 'ALA:Mountain:mountain2').
card_rarity('mountain'/'ALA', 'Basic Land').
card_artist('mountain'/'ALA', 'Aleksi Briclot').
card_number('mountain'/'ALA', '243').
card_multiverse_id('mountain'/'ALA', '175018').

card_in_set('mountain', 'ALA').
card_original_type('mountain'/'ALA', 'Basic Land — Mountain').
card_original_text('mountain'/'ALA', 'R').
card_image_name('mountain'/'ALA', 'mountain3').
card_uid('mountain'/'ALA', 'ALA:Mountain:mountain3').
card_rarity('mountain'/'ALA', 'Basic Land').
card_artist('mountain'/'ALA', 'Aleksi Briclot').
card_number('mountain'/'ALA', '244').
card_multiverse_id('mountain'/'ALA', '175017').

card_in_set('mountain', 'ALA').
card_original_type('mountain'/'ALA', 'Basic Land — Mountain').
card_original_text('mountain'/'ALA', 'R').
card_image_name('mountain'/'ALA', 'mountain4').
card_uid('mountain'/'ALA', 'ALA:Mountain:mountain4').
card_rarity('mountain'/'ALA', 'Basic Land').
card_artist('mountain'/'ALA', 'Zoltan Boros & Gabor Szikszai').
card_number('mountain'/'ALA', '245').
card_multiverse_id('mountain'/'ALA', '175020').

card_in_set('mycoloth', 'ALA').
card_original_type('mycoloth'/'ALA', 'Creature — Fungus').
card_original_text('mycoloth'/'ALA', 'Devour 2 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with twice that many +1/+1 counters on it.)\nAt the beginning of your upkeep, put a 1/1 green Saproling creature token into play for each +1/+1 counter on Mycoloth.').
card_first_print('mycoloth', 'ALA').
card_image_name('mycoloth'/'ALA', 'mycoloth').
card_uid('mycoloth'/'ALA', 'ALA:Mycoloth:mycoloth').
card_rarity('mycoloth'/'ALA', 'Rare').
card_artist('mycoloth'/'ALA', 'Raymond Swanland').
card_number('mycoloth'/'ALA', '140').
card_multiverse_id('mycoloth'/'ALA', '174975').

card_in_set('naturalize', 'ALA').
card_original_type('naturalize'/'ALA', 'Instant').
card_original_text('naturalize'/'ALA', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'ALA', 'naturalize').
card_uid('naturalize'/'ALA', 'ALA:Naturalize:naturalize').
card_rarity('naturalize'/'ALA', 'Common').
card_artist('naturalize'/'ALA', 'Trevor Hairsine').
card_number('naturalize'/'ALA', '141').
card_flavor_text('naturalize'/'ALA', 'The idols of old are rich in minerals and magic. They are torn apart when nature aims to eat well.').
card_multiverse_id('naturalize'/'ALA', '174890').

card_in_set('naya battlemage', 'ALA').
card_original_type('naya battlemage'/'ALA', 'Creature — Human Shaman').
card_original_text('naya battlemage'/'ALA', '{R}, {T}: Target creature gets +2/+0 until end of turn.\n{W}, {T}: Tap target creature.').
card_first_print('naya battlemage', 'ALA').
card_image_name('naya battlemage'/'ALA', 'naya battlemage').
card_uid('naya battlemage'/'ALA', 'ALA:Naya Battlemage:naya battlemage').
card_rarity('naya battlemage'/'ALA', 'Uncommon').
card_artist('naya battlemage'/'ALA', 'Steve Argyle').
card_number('naya battlemage'/'ALA', '142').
card_flavor_text('naya battlemage'/'ALA', '\"I have trained in all three schools of magic.\"').
card_multiverse_id('naya battlemage'/'ALA', '174944').

card_in_set('naya charm', 'ALA').
card_original_type('naya charm'/'ALA', 'Instant').
card_original_text('naya charm'/'ALA', 'Choose one Naya Charm deals 3 damage to target creature; or return target card in a graveyard to its owner\'s hand; or tap all creatures target player controls.').
card_first_print('naya charm', 'ALA').
card_image_name('naya charm'/'ALA', 'naya charm').
card_uid('naya charm'/'ALA', 'ALA:Naya Charm:naya charm').
card_rarity('naya charm'/'ALA', 'Uncommon').
card_artist('naya charm'/'ALA', 'Jesper Ejsing').
card_number('naya charm'/'ALA', '180').
card_flavor_text('naya charm'/'ALA', 'Deep in nature\'s core lies a potential unsullied by greed or civilization.').
card_multiverse_id('naya charm'/'ALA', '137905').

card_in_set('naya panorama', 'ALA').
card_original_type('naya panorama'/'ALA', 'Land').
card_original_text('naya panorama'/'ALA', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Naya Panorama: Search your library for a basic Mountain, Forest, or Plains card and put it into play tapped. Then shuffle your library.').
card_first_print('naya panorama', 'ALA').
card_image_name('naya panorama'/'ALA', 'naya panorama').
card_uid('naya panorama'/'ALA', 'ALA:Naya Panorama:naya panorama').
card_rarity('naya panorama'/'ALA', 'Common').
card_artist('naya panorama'/'ALA', 'Hideaki Takamura').
card_number('naya panorama'/'ALA', '227').
card_flavor_text('naya panorama'/'ALA', 'Between the thunderous footfalls of Naya\'s behemoths lie moments of perfect quiet.').
card_multiverse_id('naya panorama'/'ALA', '179423').

card_in_set('necrogenesis', 'ALA').
card_original_type('necrogenesis'/'ALA', 'Enchantment').
card_original_text('necrogenesis'/'ALA', '{2}: Remove target creature card in a graveyard from the game. Put a 1/1 green Saproling creature token into play.').
card_first_print('necrogenesis', 'ALA').
card_image_name('necrogenesis'/'ALA', 'necrogenesis').
card_uid('necrogenesis'/'ALA', 'ALA:Necrogenesis:necrogenesis').
card_rarity('necrogenesis'/'ALA', 'Uncommon').
card_artist('necrogenesis'/'ALA', 'Trevor Claxton').
card_number('necrogenesis'/'ALA', '181').
card_flavor_text('necrogenesis'/'ALA', '\"Those may be the squirms of one life ending or of another beginning. Either way, I\'d leave it alone.\"\n—Rakka Mar').
card_multiverse_id('necrogenesis'/'ALA', '175023').

card_in_set('obelisk of bant', 'ALA').
card_original_type('obelisk of bant'/'ALA', 'Artifact').
card_original_text('obelisk of bant'/'ALA', '{T}: Add {G}, {W}, or {U} to your mana pool.').
card_first_print('obelisk of bant', 'ALA').
card_image_name('obelisk of bant'/'ALA', 'obelisk of bant').
card_uid('obelisk of bant'/'ALA', 'ALA:Obelisk of Bant:obelisk of bant').
card_rarity('obelisk of bant'/'ALA', 'Common').
card_artist('obelisk of bant'/'ALA', 'David Palumbo').
card_number('obelisk of bant'/'ALA', '212').
card_flavor_text('obelisk of bant'/'ALA', 'Whether incorporated into the structure of castles or admired as lone symbols, the obelisks stand for Bant\'s values of loyalty, honor, and truth.').
card_multiverse_id('obelisk of bant'/'ALA', '175102').

card_in_set('obelisk of esper', 'ALA').
card_original_type('obelisk of esper'/'ALA', 'Artifact').
card_original_text('obelisk of esper'/'ALA', '{T}: Add {W}, {U}, or {B} to your mana pool.').
card_first_print('obelisk of esper', 'ALA').
card_image_name('obelisk of esper'/'ALA', 'obelisk of esper').
card_uid('obelisk of esper'/'ALA', 'ALA:Obelisk of Esper:obelisk of esper').
card_rarity('obelisk of esper'/'ALA', 'Common').
card_artist('obelisk of esper'/'ALA', 'Francis Tsai').
card_number('obelisk of esper'/'ALA', '213').
card_flavor_text('obelisk of esper'/'ALA', 'It is a monument as austere, unyielding, and inscrutable as Esper itself.').
card_multiverse_id('obelisk of esper'/'ALA', '174906').

card_in_set('obelisk of grixis', 'ALA').
card_original_type('obelisk of grixis'/'ALA', 'Artifact').
card_original_text('obelisk of grixis'/'ALA', '{T}: Add {U}, {B}, or {R} to your mana pool.').
card_first_print('obelisk of grixis', 'ALA').
card_image_name('obelisk of grixis'/'ALA', 'obelisk of grixis').
card_uid('obelisk of grixis'/'ALA', 'ALA:Obelisk of Grixis:obelisk of grixis').
card_rarity('obelisk of grixis'/'ALA', 'Common').
card_artist('obelisk of grixis'/'ALA', 'Nils Hamm').
card_number('obelisk of grixis'/'ALA', '214').
card_flavor_text('obelisk of grixis'/'ALA', 'Like most features of Grixis, the obelisks that remain from the time of Alara now exist only for dark exploitation.').
card_multiverse_id('obelisk of grixis'/'ALA', '174887').

card_in_set('obelisk of jund', 'ALA').
card_original_type('obelisk of jund'/'ALA', 'Artifact').
card_original_text('obelisk of jund'/'ALA', '{T}: Add {B}, {R}, or {G} to your mana pool.').
card_first_print('obelisk of jund', 'ALA').
card_image_name('obelisk of jund'/'ALA', 'obelisk of jund').
card_uid('obelisk of jund'/'ALA', 'ALA:Obelisk of Jund:obelisk of jund').
card_rarity('obelisk of jund'/'ALA', 'Common').
card_artist('obelisk of jund'/'ALA', 'Brandon Kitkouski').
card_number('obelisk of jund'/'ALA', '215').
card_flavor_text('obelisk of jund'/'ALA', 'Volcanic ash-winds batter it, climbing vines overwhelm it, dragonfire roasts it, and yet it still stands as a testament to a forgotten world.').
card_multiverse_id('obelisk of jund'/'ALA', '174892').

card_in_set('obelisk of naya', 'ALA').
card_original_type('obelisk of naya'/'ALA', 'Artifact').
card_original_text('obelisk of naya'/'ALA', '{T}: Add {R}, {G}, or {W} to your mana pool.').
card_first_print('obelisk of naya', 'ALA').
card_image_name('obelisk of naya'/'ALA', 'obelisk of naya').
card_uid('obelisk of naya'/'ALA', 'ALA:Obelisk of Naya:obelisk of naya').
card_rarity('obelisk of naya'/'ALA', 'Common').
card_artist('obelisk of naya'/'ALA', 'Steve Prescott').
card_number('obelisk of naya'/'ALA', '216').
card_flavor_text('obelisk of naya'/'ALA', 'Centuries have passed since the plane shattered, yet the obelisks of each shard faithfully serve their long-forgotten purpose.').
card_multiverse_id('obelisk of naya'/'ALA', '175061').

card_in_set('oblivion ring', 'ALA').
card_original_type('oblivion ring'/'ALA', 'Enchantment').
card_original_text('oblivion ring'/'ALA', 'When Oblivion Ring comes into play, remove another target nonland permanent from the game. \nWhen Oblivion Ring leaves play, return the removed card to play under its owner\'s control.').
card_image_name('oblivion ring'/'ALA', 'oblivion ring').
card_uid('oblivion ring'/'ALA', 'ALA:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'ALA', 'Common').
card_artist('oblivion ring'/'ALA', 'Franz Vohwinkel').
card_number('oblivion ring'/'ALA', '20').
card_flavor_text('oblivion ring'/'ALA', 'A circle of light and a word of confinement.').
card_multiverse_id('oblivion ring'/'ALA', '174909').

card_in_set('onyx goblet', 'ALA').
card_original_type('onyx goblet'/'ALA', 'Artifact').
card_original_text('onyx goblet'/'ALA', '{T}: Target player loses 1 life.').
card_first_print('onyx goblet', 'ALA').
card_image_name('onyx goblet'/'ALA', 'onyx goblet').
card_uid('onyx goblet'/'ALA', 'ALA:Onyx Goblet:onyx goblet').
card_rarity('onyx goblet'/'ALA', 'Common').
card_artist('onyx goblet'/'ALA', 'rk post').
card_number('onyx goblet'/'ALA', '81').
card_flavor_text('onyx goblet'/'ALA', 'The goblet was a gift from the sphinx Gorael, who hoped humans and vedalken would eventually destroy each other to acquire it, leaving all of Esper to her own kind.').
card_multiverse_id('onyx goblet'/'ALA', '174831').

card_in_set('ooze garden', 'ALA').
card_original_type('ooze garden'/'ALA', 'Enchantment').
card_original_text('ooze garden'/'ALA', '{1}{G}, Sacrifice a non-Ooze creature: Put an X/X green Ooze creature token into play, where X is the sacrificed creature\'s power. Play this ability only any time you could play a sorcery.').
card_first_print('ooze garden', 'ALA').
card_image_name('ooze garden'/'ALA', 'ooze garden').
card_uid('ooze garden'/'ALA', 'ALA:Ooze Garden:ooze garden').
card_rarity('ooze garden'/'ALA', 'Rare').
card_artist('ooze garden'/'ALA', 'Anthony S. Waters').
card_number('ooze garden'/'ALA', '143').
card_flavor_text('ooze garden'/'ALA', 'Jund has little necromancy. Instead, the flesh of its dead gives rise to algal fiends.').
card_multiverse_id('ooze garden'/'ALA', '174886').

card_in_set('outrider of jhess', 'ALA').
card_original_type('outrider of jhess'/'ALA', 'Creature — Human Knight').
card_original_text('outrider of jhess'/'ALA', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('outrider of jhess', 'ALA').
card_image_name('outrider of jhess'/'ALA', 'outrider of jhess').
card_uid('outrider of jhess'/'ALA', 'ALA:Outrider of Jhess:outrider of jhess').
card_rarity('outrider of jhess'/'ALA', 'Common').
card_artist('outrider of jhess'/'ALA', 'Alan Pollack').
card_number('outrider of jhess'/'ALA', '52').
card_flavor_text('outrider of jhess'/'ALA', 'The island nation of Jhess is under constant siege from Valeron on the mainland. Riders traverse the coastline, always on the lookout for sails or aven.').
card_multiverse_id('outrider of jhess'/'ALA', '175252').

card_in_set('plains', 'ALA').
card_original_type('plains'/'ALA', 'Basic Land — Plains').
card_original_text('plains'/'ALA', 'W').
card_image_name('plains'/'ALA', 'plains1').
card_uid('plains'/'ALA', 'ALA:Plains:plains1').
card_rarity('plains'/'ALA', 'Basic Land').
card_artist('plains'/'ALA', 'Zoltan Boros & Gabor Szikszai').
card_number('plains'/'ALA', '230').
card_multiverse_id('plains'/'ALA', '175029').

card_in_set('plains', 'ALA').
card_original_type('plains'/'ALA', 'Basic Land — Plains').
card_original_text('plains'/'ALA', 'W').
card_image_name('plains'/'ALA', 'plains2').
card_uid('plains'/'ALA', 'ALA:Plains:plains2').
card_rarity('plains'/'ALA', 'Basic Land').
card_artist('plains'/'ALA', 'Michael Komarck').
card_number('plains'/'ALA', '231').
card_multiverse_id('plains'/'ALA', '175030').

card_in_set('plains', 'ALA').
card_original_type('plains'/'ALA', 'Basic Land — Plains').
card_original_text('plains'/'ALA', 'W').
card_image_name('plains'/'ALA', 'plains3').
card_uid('plains'/'ALA', 'ALA:Plains:plains3').
card_rarity('plains'/'ALA', 'Basic Land').
card_artist('plains'/'ALA', 'Michael Komarck').
card_number('plains'/'ALA', '232').
card_multiverse_id('plains'/'ALA', '175031').

card_in_set('plains', 'ALA').
card_original_type('plains'/'ALA', 'Basic Land — Plains').
card_original_text('plains'/'ALA', 'W').
card_image_name('plains'/'ALA', 'plains4').
card_uid('plains'/'ALA', 'ALA:Plains:plains4').
card_rarity('plains'/'ALA', 'Basic Land').
card_artist('plains'/'ALA', 'Chippy').
card_number('plains'/'ALA', '233').
card_multiverse_id('plains'/'ALA', '175032').

card_in_set('predator dragon', 'ALA').
card_original_type('predator dragon'/'ALA', 'Creature — Dragon').
card_original_text('predator dragon'/'ALA', 'Flying, haste\nDevour 2 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with twice that many +1/+1 counters on it.)').
card_first_print('predator dragon', 'ALA').
card_image_name('predator dragon'/'ALA', 'predator dragon').
card_uid('predator dragon'/'ALA', 'ALA:Predator Dragon:predator dragon').
card_rarity('predator dragon'/'ALA', 'Rare').
card_artist('predator dragon'/'ALA', 'Raymond Swanland').
card_number('predator dragon'/'ALA', '109').
card_flavor_text('predator dragon'/'ALA', 'Dragons make for spiteful gods.').
card_multiverse_id('predator dragon'/'ALA', '174968').

card_in_set('prince of thralls', 'ALA').
card_original_type('prince of thralls'/'ALA', 'Creature — Demon').
card_original_text('prince of thralls'/'ALA', 'Whenever a permanent an opponent controls is put into a graveyard, put that card into play under your control unless that opponent pays 3 life.').
card_first_print('prince of thralls', 'ALA').
card_image_name('prince of thralls'/'ALA', 'prince of thralls').
card_uid('prince of thralls'/'ALA', 'ALA:Prince of Thralls:prince of thralls').
card_rarity('prince of thralls'/'ALA', 'Mythic Rare').
card_artist('prince of thralls'/'ALA', 'Paul Bonner').
card_number('prince of thralls'/'ALA', '182').
card_flavor_text('prince of thralls'/'ALA', '\"There are none alive that I cannot crush. There are none dead that I do not command.\"').
card_multiverse_id('prince of thralls'/'ALA', '175106').

card_in_set('protomatter powder', 'ALA').
card_original_type('protomatter powder'/'ALA', 'Artifact').
card_original_text('protomatter powder'/'ALA', '{4}{W}, {T}, Sacrifice Protomatter Powder: Return target artifact card from your graveyard to play.').
card_first_print('protomatter powder', 'ALA').
card_image_name('protomatter powder'/'ALA', 'protomatter powder').
card_uid('protomatter powder'/'ALA', 'ALA:Protomatter Powder:protomatter powder').
card_rarity('protomatter powder'/'ALA', 'Uncommon').
card_artist('protomatter powder'/'ALA', 'Francis Tsai').
card_number('protomatter powder'/'ALA', '53').
card_flavor_text('protomatter powder'/'ALA', '\"There is no such thing as scrap metal. All such material can be repaired with the proper bonding agent.\"\n—Quennus, metallurgeon').
card_multiverse_id('protomatter powder'/'ALA', '174833').

card_in_set('punish ignorance', 'ALA').
card_original_type('punish ignorance'/'ALA', 'Instant').
card_original_text('punish ignorance'/'ALA', 'Counter target spell. Its controller loses 3 life and you gain 3 life.').
card_first_print('punish ignorance', 'ALA').
card_image_name('punish ignorance'/'ALA', 'punish ignorance').
card_uid('punish ignorance'/'ALA', 'ALA:Punish Ignorance:punish ignorance').
card_rarity('punish ignorance'/'ALA', 'Rare').
card_artist('punish ignorance'/'ALA', 'Shelly Wan').
card_number('punish ignorance'/'ALA', '183').
card_flavor_text('punish ignorance'/'ALA', '\"Amateurish. Nearsighted. A waste of my time and everyone else\'s.\"\n—Indra, nullmage of Vectis').
card_multiverse_id('punish ignorance'/'ALA', '176450').

card_in_set('puppet conjurer', 'ALA').
card_original_type('puppet conjurer'/'ALA', 'Artifact Creature — Human Wizard').
card_original_text('puppet conjurer'/'ALA', '{U}, {T}: Put a 0/1 blue Homunculus artifact creature token into play.\nAt the beginning of your upkeep, sacrifice a Homunculus.').
card_first_print('puppet conjurer', 'ALA').
card_image_name('puppet conjurer'/'ALA', 'puppet conjurer').
card_uid('puppet conjurer'/'ALA', 'ALA:Puppet Conjurer:puppet conjurer').
card_rarity('puppet conjurer'/'ALA', 'Uncommon').
card_artist('puppet conjurer'/'ALA', 'Steven Belledin').
card_number('puppet conjurer'/'ALA', '82').
card_flavor_text('puppet conjurer'/'ALA', 'He scowls at his imperfect creation, then crushes it to scrap. He salvages the etherium and begins anew.').
card_multiverse_id('puppet conjurer'/'ALA', '175012').

card_in_set('qasali ambusher', 'ALA').
card_original_type('qasali ambusher'/'ALA', 'Creature — Cat Warrior').
card_original_text('qasali ambusher'/'ALA', 'Reach\nIf a creature is attacking you and you control a Forest and a Plains, you may play Qasali Ambusher without paying its mana cost and as though it had flash.').
card_first_print('qasali ambusher', 'ALA').
card_image_name('qasali ambusher'/'ALA', 'qasali ambusher').
card_uid('qasali ambusher'/'ALA', 'ALA:Qasali Ambusher:qasali ambusher').
card_rarity('qasali ambusher'/'ALA', 'Uncommon').
card_artist('qasali ambusher'/'ALA', 'Kev Walker').
card_number('qasali ambusher'/'ALA', '184').
card_flavor_text('qasali ambusher'/'ALA', 'The surprise of a battle cry can be as deadly as a blade.').
card_multiverse_id('qasali ambusher'/'ALA', '174869').

card_in_set('quietus spike', 'ALA').
card_original_type('quietus spike'/'ALA', 'Artifact — Equipment').
card_original_text('quietus spike'/'ALA', 'Equipped creature has deathtouch.\nWhenever equipped creature deals combat damage to a player, that player loses half his or her life, rounded up.\nEquip {3}').
card_first_print('quietus spike', 'ALA').
card_image_name('quietus spike'/'ALA', 'quietus spike').
card_uid('quietus spike'/'ALA', 'ALA:Quietus Spike:quietus spike').
card_rarity('quietus spike'/'ALA', 'Rare').
card_artist('quietus spike'/'ALA', 'Mark Brill').
card_number('quietus spike'/'ALA', '217').
card_multiverse_id('quietus spike'/'ALA', '185774').

card_in_set('rafiq of the many', 'ALA').
card_original_type('rafiq of the many'/'ALA', 'Legendary Creature — Human Knight').
card_original_text('rafiq of the many'/'ALA', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, it gains double strike until end of turn.').
card_first_print('rafiq of the many', 'ALA').
card_image_name('rafiq of the many'/'ALA', 'rafiq of the many').
card_uid('rafiq of the many'/'ALA', 'ALA:Rafiq of the Many:rafiq of the many').
card_rarity('rafiq of the many'/'ALA', 'Mythic Rare').
card_artist('rafiq of the many'/'ALA', 'Michael Komarck').
card_number('rafiq of the many'/'ALA', '185').
card_flavor_text('rafiq of the many'/'ALA', 'Many sigils, one purpose.').
card_multiverse_id('rafiq of the many'/'ALA', '174948').

card_in_set('rakeclaw gargantuan', 'ALA').
card_original_type('rakeclaw gargantuan'/'ALA', 'Creature — Beast').
card_original_text('rakeclaw gargantuan'/'ALA', '{1}: Target creature with power 5 or greater gains first strike until end of turn.').
card_first_print('rakeclaw gargantuan', 'ALA').
card_image_name('rakeclaw gargantuan'/'ALA', 'rakeclaw gargantuan').
card_uid('rakeclaw gargantuan'/'ALA', 'ALA:Rakeclaw Gargantuan:rakeclaw gargantuan').
card_rarity('rakeclaw gargantuan'/'ALA', 'Common').
card_artist('rakeclaw gargantuan'/'ALA', 'Jesper Ejsing').
card_number('rakeclaw gargantuan'/'ALA', '186').
card_flavor_text('rakeclaw gargantuan'/'ALA', 'Naya teems with gargantuans, titanic monsters to whom both nature and civilization defer.').
card_multiverse_id('rakeclaw gargantuan'/'ALA', '175253').

card_in_set('ranger of eos', 'ALA').
card_original_type('ranger of eos'/'ALA', 'Creature — Human Soldier').
card_original_text('ranger of eos'/'ALA', 'When Ranger of Eos comes into play, you may search your library for up to two creature cards with converted mana cost 1 or less, reveal them, and put them into your hand. If you do, shuffle your library.').
card_first_print('ranger of eos', 'ALA').
card_image_name('ranger of eos'/'ALA', 'ranger of eos').
card_uid('ranger of eos'/'ALA', 'ALA:Ranger of Eos:ranger of eos').
card_rarity('ranger of eos'/'ALA', 'Rare').
card_artist('ranger of eos'/'ALA', 'Volkan Baga').
card_number('ranger of eos'/'ALA', '21').
card_flavor_text('ranger of eos'/'ALA', 'At his side, humble beasts become weapons more deadly than sharpened steel.').
card_multiverse_id('ranger of eos'/'ALA', '174823').

card_in_set('realm razer', 'ALA').
card_original_type('realm razer'/'ALA', 'Creature — Beast').
card_original_text('realm razer'/'ALA', 'When Realm Razer comes into play, remove all lands from the game.\nWhen Realm Razer leaves play, return the removed cards to play tapped under their owners\' control.').
card_first_print('realm razer', 'ALA').
card_image_name('realm razer'/'ALA', 'realm razer').
card_uid('realm razer'/'ALA', 'ALA:Realm Razer:realm razer').
card_rarity('realm razer'/'ALA', 'Rare').
card_artist('realm razer'/'ALA', 'Hideaki Takamura').
card_number('realm razer'/'ALA', '187').
card_flavor_text('realm razer'/'ALA', 'The behemoth roared, and the world was blasted free of empires and tyrants.').
card_multiverse_id('realm razer'/'ALA', '179422').

card_in_set('relic of progenitus', 'ALA').
card_original_type('relic of progenitus'/'ALA', 'Artifact').
card_original_text('relic of progenitus'/'ALA', '{T}: Target player removes a card in his or her graveyard from the game.\n{1}, Remove Relic of Progenitus from the game: Remove all graveyards from the game. Draw a card.').
card_first_print('relic of progenitus', 'ALA').
card_image_name('relic of progenitus'/'ALA', 'relic of progenitus').
card_uid('relic of progenitus'/'ALA', 'ALA:Relic of Progenitus:relic of progenitus').
card_rarity('relic of progenitus'/'ALA', 'Common').
card_artist('relic of progenitus'/'ALA', 'Jean-Sébastien Rossbach').
card_number('relic of progenitus'/'ALA', '218').
card_flavor_text('relic of progenitus'/'ALA', 'Elves believe the hydra-god Progenitus sleeps beneath Naya, feeding on forgotten magics.').
card_multiverse_id('relic of progenitus'/'ALA', '174824').

card_in_set('resounding roar', 'ALA').
card_original_type('resounding roar'/'ALA', 'Instant').
card_original_text('resounding roar'/'ALA', 'Target creature gets +3/+3 until end of turn.\nCycling {5}{R}{G}{W} ({5}{R}{G}{W}, Discard this card: Draw a card.)\nWhen you cycle Resounding Roar, target creature gets +6/+6 until end of turn.').
card_first_print('resounding roar', 'ALA').
card_image_name('resounding roar'/'ALA', 'resounding roar').
card_uid('resounding roar'/'ALA', 'ALA:Resounding Roar:resounding roar').
card_rarity('resounding roar'/'ALA', 'Common').
card_artist('resounding roar'/'ALA', 'Steve Prescott').
card_number('resounding roar'/'ALA', '144').
card_multiverse_id('resounding roar'/'ALA', '174901').

card_in_set('resounding scream', 'ALA').
card_original_type('resounding scream'/'ALA', 'Sorcery').
card_original_text('resounding scream'/'ALA', 'Target player discards a card at random.\nCycling {5}{U}{B}{R} ({5}{U}{B}{R}, Discard this card: Draw a card.)\nWhen you cycle Resounding Scream, target player discards two cards at random.').
card_first_print('resounding scream', 'ALA').
card_image_name('resounding scream'/'ALA', 'resounding scream').
card_uid('resounding scream'/'ALA', 'ALA:Resounding Scream:resounding scream').
card_rarity('resounding scream'/'ALA', 'Common').
card_artist('resounding scream'/'ALA', 'Thomas M. Baxa').
card_number('resounding scream'/'ALA', '83').
card_multiverse_id('resounding scream'/'ALA', '176444').

card_in_set('resounding silence', 'ALA').
card_original_type('resounding silence'/'ALA', 'Instant').
card_original_text('resounding silence'/'ALA', 'Remove target attacking creature from the game.\nCycling {5}{G}{W}{U} ({5}{G}{W}{U}, Discard this card: Draw a card.)\nWhen you cycle Resounding Silence, remove up to two target attacking creatures from the game.').
card_first_print('resounding silence', 'ALA').
card_image_name('resounding silence'/'ALA', 'resounding silence').
card_uid('resounding silence'/'ALA', 'ALA:Resounding Silence:resounding silence').
card_rarity('resounding silence'/'ALA', 'Common').
card_artist('resounding silence'/'ALA', 'Mark Zug').
card_number('resounding silence'/'ALA', '22').
card_multiverse_id('resounding silence'/'ALA', '174985').

card_in_set('resounding thunder', 'ALA').
card_original_type('resounding thunder'/'ALA', 'Instant').
card_original_text('resounding thunder'/'ALA', 'Resounding Thunder deals 3 damage to target creature or player.\nCycling {5}{B}{R}{G} ({5}{B}{R}{G}, Discard this card: Draw a card.)\nWhen you cycle Resounding Thunder, it deals 6 damage to target creature or player.').
card_first_print('resounding thunder', 'ALA').
card_image_name('resounding thunder'/'ALA', 'resounding thunder').
card_uid('resounding thunder'/'ALA', 'ALA:Resounding Thunder:resounding thunder').
card_rarity('resounding thunder'/'ALA', 'Common').
card_artist('resounding thunder'/'ALA', 'Jon Foster').
card_number('resounding thunder'/'ALA', '110').
card_multiverse_id('resounding thunder'/'ALA', '175043').

card_in_set('resounding wave', 'ALA').
card_original_type('resounding wave'/'ALA', 'Instant').
card_original_text('resounding wave'/'ALA', 'Return target permanent to its owner\'s hand.\nCycling {5}{W}{U}{B} ({5}{W}{U}{B}, Discard this card: Draw a card.)\nWhen you cycle Resounding Wave, return two target permanents to their owners\' hands.').
card_first_print('resounding wave', 'ALA').
card_image_name('resounding wave'/'ALA', 'resounding wave').
card_uid('resounding wave'/'ALA', 'ALA:Resounding Wave:resounding wave').
card_rarity('resounding wave'/'ALA', 'Common').
card_artist('resounding wave'/'ALA', 'Izzy').
card_number('resounding wave'/'ALA', '54').
card_multiverse_id('resounding wave'/'ALA', '175098').

card_in_set('rhox charger', 'ALA').
card_original_type('rhox charger'/'ALA', 'Creature — Rhino Soldier').
card_original_text('rhox charger'/'ALA', 'Trample\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('rhox charger', 'ALA').
card_image_name('rhox charger'/'ALA', 'rhox charger').
card_uid('rhox charger'/'ALA', 'ALA:Rhox Charger:rhox charger').
card_rarity('rhox charger'/'ALA', 'Uncommon').
card_artist('rhox charger'/'ALA', 'Chris Rahn').
card_number('rhox charger'/'ALA', '145').
card_flavor_text('rhox charger'/'ALA', '\"Momentum is a great ally.\"').
card_multiverse_id('rhox charger'/'ALA', '174959').

card_in_set('rhox war monk', 'ALA').
card_original_type('rhox war monk'/'ALA', 'Creature — Rhino Monk').
card_original_text('rhox war monk'/'ALA', 'Lifelink').
card_image_name('rhox war monk'/'ALA', 'rhox war monk').
card_uid('rhox war monk'/'ALA', 'ALA:Rhox War Monk:rhox war monk').
card_rarity('rhox war monk'/'ALA', 'Uncommon').
card_artist('rhox war monk'/'ALA', 'Dan Dos Santos').
card_number('rhox war monk'/'ALA', '188').
card_flavor_text('rhox war monk'/'ALA', 'Rhox monks are dedicated to spiritual growth and learning, and most bear the sigils of many students. However, they do not gladly suffer fools or those who disagree with their carefully wrought dogma.').
card_multiverse_id('rhox war monk'/'ALA', '174957').

card_in_set('ridge rannet', 'ALA').
card_original_type('ridge rannet'/'ALA', 'Creature — Beast').
card_original_text('ridge rannet'/'ALA', 'Cycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('ridge rannet', 'ALA').
card_image_name('ridge rannet'/'ALA', 'ridge rannet').
card_uid('ridge rannet'/'ALA', 'ALA:Ridge Rannet:ridge rannet').
card_rarity('ridge rannet'/'ALA', 'Common').
card_artist('ridge rannet'/'ALA', 'Jim Murray').
card_number('ridge rannet'/'ALA', '111').
card_flavor_text('ridge rannet'/'ALA', '\"Only those with the strength to seize their destiny deserve to have one.\"\n—Nacatl scratchforms').
card_multiverse_id('ridge rannet'/'ALA', '175257').

card_in_set('rip-clan crasher', 'ALA').
card_original_type('rip-clan crasher'/'ALA', 'Creature — Human Warrior').
card_original_text('rip-clan crasher'/'ALA', 'Haste').
card_first_print('rip-clan crasher', 'ALA').
card_image_name('rip-clan crasher'/'ALA', 'rip-clan crasher').
card_uid('rip-clan crasher'/'ALA', 'ALA:Rip-Clan Crasher:rip-clan crasher').
card_rarity('rip-clan crasher'/'ALA', 'Common').
card_artist('rip-clan crasher'/'ALA', 'Justin Sweet').
card_number('rip-clan crasher'/'ALA', '189').
card_flavor_text('rip-clan crasher'/'ALA', 'If you breathe, she will fight you. If you breathe fire, she must fight you.').
card_multiverse_id('rip-clan crasher'/'ALA', '177600').

card_in_set('rockcaster platoon', 'ALA').
card_original_type('rockcaster platoon'/'ALA', 'Creature — Rhino Soldier').
card_original_text('rockcaster platoon'/'ALA', '{4}{G}: Rockcaster Platoon deals 2 damage to each creature with flying and each player.').
card_first_print('rockcaster platoon', 'ALA').
card_image_name('rockcaster platoon'/'ALA', 'rockcaster platoon').
card_uid('rockcaster platoon'/'ALA', 'ALA:Rockcaster Platoon:rockcaster platoon').
card_rarity('rockcaster platoon'/'ALA', 'Uncommon').
card_artist('rockcaster platoon'/'ALA', 'David Palumbo').
card_number('rockcaster platoon'/'ALA', '23').
card_flavor_text('rockcaster platoon'/'ALA', '\"Aven bandits can be sly. You\'ve got to fill the sky with boulders—and then seek cover immediately.\"\n—Knight-Captain Wyhorn').
card_multiverse_id('rockcaster platoon'/'ALA', '175024').

card_in_set('rockslide elemental', 'ALA').
card_original_type('rockslide elemental'/'ALA', 'Creature — Elemental').
card_original_text('rockslide elemental'/'ALA', 'First strike\nWhenever another creature is put into a graveyard from play, you may put a +1/+1 counter on Rockslide Elemental.').
card_first_print('rockslide elemental', 'ALA').
card_image_name('rockslide elemental'/'ALA', 'rockslide elemental').
card_uid('rockslide elemental'/'ALA', 'ALA:Rockslide Elemental:rockslide elemental').
card_rarity('rockslide elemental'/'ALA', 'Uncommon').
card_artist('rockslide elemental'/'ALA', 'Joshua Hagler').
card_number('rockslide elemental'/'ALA', '112').
card_flavor_text('rockslide elemental'/'ALA', 'On Jund, even the landscape is a deadly hunter. Volcanoes, tar pits, and rock slides seem to prey upon the living.').
card_multiverse_id('rockslide elemental'/'ALA', '174988').

card_in_set('sacellum godspeaker', 'ALA').
card_original_type('sacellum godspeaker'/'ALA', 'Creature — Elf Druid').
card_original_text('sacellum godspeaker'/'ALA', '{T}: Reveal any number of creature cards with power 5 or greater from your hand. Add {G} to your mana pool for each card revealed this way.').
card_first_print('sacellum godspeaker', 'ALA').
card_image_name('sacellum godspeaker'/'ALA', 'sacellum godspeaker').
card_uid('sacellum godspeaker'/'ALA', 'ALA:Sacellum Godspeaker:sacellum godspeaker').
card_rarity('sacellum godspeaker'/'ALA', 'Rare').
card_artist('sacellum godspeaker'/'ALA', 'Wayne Reynolds').
card_number('sacellum godspeaker'/'ALA', '146').
card_flavor_text('sacellum godspeaker'/'ALA', 'Her connection to the gargantuans puts her in line to become Anima, a sacred but daunting duty.').
card_multiverse_id('sacellum godspeaker'/'ALA', '174828').

card_in_set('salvage titan', 'ALA').
card_original_type('salvage titan'/'ALA', 'Artifact Creature — Golem').
card_original_text('salvage titan'/'ALA', 'You may sacrifice three artifacts rather than pay Salvage Titan\'s mana cost.\nRemove three artifact cards in your graveyard from the game: Return Salvage Titan from your graveyard to your hand.').
card_first_print('salvage titan', 'ALA').
card_image_name('salvage titan'/'ALA', 'salvage titan').
card_uid('salvage titan'/'ALA', 'ALA:Salvage Titan:salvage titan').
card_rarity('salvage titan'/'ALA', 'Rare').
card_artist('salvage titan'/'ALA', 'Anthony Francisco').
card_number('salvage titan'/'ALA', '84').
card_multiverse_id('salvage titan'/'ALA', '174900').

card_in_set('sanctum gargoyle', 'ALA').
card_original_type('sanctum gargoyle'/'ALA', 'Artifact Creature — Gargoyle').
card_original_text('sanctum gargoyle'/'ALA', 'Flying\nWhen Sanctum Gargoyle comes into play, you may return target artifact card from your graveyard to your hand.').
card_first_print('sanctum gargoyle', 'ALA').
card_image_name('sanctum gargoyle'/'ALA', 'sanctum gargoyle').
card_uid('sanctum gargoyle'/'ALA', 'ALA:Sanctum Gargoyle:sanctum gargoyle').
card_rarity('sanctum gargoyle'/'ALA', 'Common').
card_artist('sanctum gargoyle'/'ALA', 'Shelly Wan').
card_number('sanctum gargoyle'/'ALA', '24').
card_flavor_text('sanctum gargoyle'/'ALA', 'As their supplies of etherium dwindled, mechanists sent gargoyles farther and farther afield in search of salvage.').
card_multiverse_id('sanctum gargoyle'/'ALA', '174925').

card_in_set('sangrite surge', 'ALA').
card_original_type('sangrite surge'/'ALA', 'Sorcery').
card_original_text('sangrite surge'/'ALA', 'Target creature gets +3/+3 and gains double strike until end of turn.').
card_first_print('sangrite surge', 'ALA').
card_image_name('sangrite surge'/'ALA', 'sangrite surge').
card_uid('sangrite surge'/'ALA', 'ALA:Sangrite Surge:sangrite surge').
card_rarity('sangrite surge'/'ALA', 'Uncommon').
card_artist('sangrite surge'/'ALA', 'Jarreau Wimberly').
card_number('sangrite surge'/'ALA', '190').
card_flavor_text('sangrite surge'/'ALA', 'Some believe the spiky crystal called sangrite is concentrated dragon\'s breath. Others think it\'s crystallized life energy. The clans care only about the power it unleashes when crushed.').
card_multiverse_id('sangrite surge'/'ALA', '176438').

card_in_set('sarkhan vol', 'ALA').
card_original_type('sarkhan vol'/'ALA', 'Planeswalker — Sarkhan').
card_original_text('sarkhan vol'/'ALA', '+1: Creatures you control get +1/+1 and gain haste until end of turn.\n-2: Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn.\n-6: Put five 4/4 red Dragon creature tokens with flying into play.').
card_first_print('sarkhan vol', 'ALA').
card_image_name('sarkhan vol'/'ALA', 'sarkhan vol').
card_uid('sarkhan vol'/'ALA', 'ALA:Sarkhan Vol:sarkhan vol').
card_rarity('sarkhan vol'/'ALA', 'Mythic Rare').
card_artist('sarkhan vol'/'ALA', 'Daarken').
card_number('sarkhan vol'/'ALA', '191').
card_multiverse_id('sarkhan vol'/'ALA', '174983').

card_in_set('savage hunger', 'ALA').
card_original_type('savage hunger'/'ALA', 'Enchantment — Aura').
card_original_text('savage hunger'/'ALA', 'Enchant creature\nEnchanted creature gets +1/+0 and has trample.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('savage hunger', 'ALA').
card_image_name('savage hunger'/'ALA', 'savage hunger').
card_uid('savage hunger'/'ALA', 'ALA:Savage Hunger:savage hunger').
card_rarity('savage hunger'/'ALA', 'Common').
card_artist('savage hunger'/'ALA', 'Trevor Claxton').
card_number('savage hunger'/'ALA', '147').
card_multiverse_id('savage hunger'/'ALA', '174941').

card_in_set('savage lands', 'ALA').
card_original_type('savage lands'/'ALA', 'Land').
card_original_text('savage lands'/'ALA', 'Savage Lands comes into play tapped.\n{T}: Add {B}, {R}, or {G} to your mana pool.').
card_image_name('savage lands'/'ALA', 'savage lands').
card_uid('savage lands'/'ALA', 'ALA:Savage Lands:savage lands').
card_rarity('savage lands'/'ALA', 'Uncommon').
card_artist('savage lands'/'ALA', 'Vance Kovacs').
card_number('savage lands'/'ALA', '228').
card_flavor_text('savage lands'/'ALA', 'Jund is a world as cruel as those who call it home. Their brutal struggles scar the land even as it carves them in its image, a vicious circle spiraling out of control.').
card_multiverse_id('savage lands'/'ALA', '174877').

card_in_set('scavenger drake', 'ALA').
card_original_type('scavenger drake'/'ALA', 'Creature — Drake').
card_original_text('scavenger drake'/'ALA', 'Flying\nWhenever another creature is put into a graveyard from play, you may put a +1/+1 counter on Scavenger Drake.').
card_first_print('scavenger drake', 'ALA').
card_image_name('scavenger drake'/'ALA', 'scavenger drake').
card_uid('scavenger drake'/'ALA', 'ALA:Scavenger Drake:scavenger drake').
card_rarity('scavenger drake'/'ALA', 'Uncommon').
card_artist('scavenger drake'/'ALA', 'Trevor Claxton').
card_number('scavenger drake'/'ALA', '85').
card_flavor_text('scavenger drake'/'ALA', 'Dragons consider drakes to be mockeries of their perfection, destroying them on sight.').
card_multiverse_id('scavenger drake'/'ALA', '174987').

card_in_set('scourge devil', 'ALA').
card_original_type('scourge devil'/'ALA', 'Creature — Devil').
card_original_text('scourge devil'/'ALA', 'When Scourge Devil comes into play, creatures you control get +1/+0 until end of turn.\nUnearth {2}{R} ({2}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('scourge devil', 'ALA').
card_image_name('scourge devil'/'ALA', 'scourge devil').
card_uid('scourge devil'/'ALA', 'ALA:Scourge Devil:scourge devil').
card_rarity('scourge devil'/'ALA', 'Uncommon').
card_artist('scourge devil'/'ALA', 'Dave Kendall').
card_number('scourge devil'/'ALA', '113').
card_multiverse_id('scourge devil'/'ALA', '174845').

card_in_set('scourglass', 'ALA').
card_original_type('scourglass'/'ALA', 'Artifact').
card_original_text('scourglass'/'ALA', '{T}, Sacrifice Scourglass: Destroy all permanents except for artifacts and lands. Play this ability only during your upkeep.').
card_first_print('scourglass', 'ALA').
card_image_name('scourglass'/'ALA', 'scourglass').
card_uid('scourglass'/'ALA', 'ALA:Scourglass:scourglass').
card_rarity('scourglass'/'ALA', 'Rare').
card_artist('scourglass'/'ALA', 'Matt Cavotta').
card_number('scourglass'/'ALA', '25').
card_flavor_text('scourglass'/'ALA', '\"Soon I will sleep, eyes closing to a world tarnished with flesh, and then wake to a polished vision.\"\n—Sharuum').
card_multiverse_id('scourglass'/'ALA', '174853').

card_in_set('seaside citadel', 'ALA').
card_original_type('seaside citadel'/'ALA', 'Land').
card_original_text('seaside citadel'/'ALA', 'Seaside Citadel comes into play tapped.\n{T}: Add {G}, {W}, or {U} to your mana pool.').
card_first_print('seaside citadel', 'ALA').
card_image_name('seaside citadel'/'ALA', 'seaside citadel').
card_uid('seaside citadel'/'ALA', 'ALA:Seaside Citadel:seaside citadel').
card_rarity('seaside citadel'/'ALA', 'Uncommon').
card_artist('seaside citadel'/'ALA', 'Volkan Baga').
card_number('seaside citadel'/'ALA', '229').
card_flavor_text('seaside citadel'/'ALA', 'For wisdom\'s sake, it was built high to gaze on all things. For glory\'s sake, it was built high as a testament of power. For strength\'s sake, it was built high to repel all attacks.').
card_multiverse_id('seaside citadel'/'ALA', '174950').

card_in_set('sedraxis specter', 'ALA').
card_original_type('sedraxis specter'/'ALA', 'Creature — Specter').
card_original_text('sedraxis specter'/'ALA', 'Flying\nWhenever Sedraxis Specter deals combat damage to a player, that player discards a card.\nUnearth {1}{B} ({1}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('sedraxis specter', 'ALA').
card_image_name('sedraxis specter'/'ALA', 'sedraxis specter').
card_uid('sedraxis specter'/'ALA', 'ALA:Sedraxis Specter:sedraxis specter').
card_rarity('sedraxis specter'/'ALA', 'Rare').
card_artist('sedraxis specter'/'ALA', 'Cole Eastburn').
card_number('sedraxis specter'/'ALA', '192').
card_multiverse_id('sedraxis specter'/'ALA', '174838').

card_in_set('sedris, the traitor king', 'ALA').
card_original_type('sedris, the traitor king'/'ALA', 'Legendary Creature — Zombie Warrior').
card_original_text('sedris, the traitor king'/'ALA', 'Each creature card in your graveyard has unearth {2}{B}. ({2}{B}: Return the card to play. The creature gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('sedris, the traitor king', 'ALA').
card_image_name('sedris, the traitor king'/'ALA', 'sedris, the traitor king').
card_uid('sedris, the traitor king'/'ALA', 'ALA:Sedris, the Traitor King:sedris, the traitor king').
card_rarity('sedris, the traitor king'/'ALA', 'Mythic Rare').
card_artist('sedris, the traitor king'/'ALA', 'Paul Bonner').
card_number('sedris, the traitor king'/'ALA', '193').
card_flavor_text('sedris, the traitor king'/'ALA', 'He bids his minions rise from their graves to their knees.').
card_multiverse_id('sedris, the traitor king'/'ALA', '175111').

card_in_set('shadowfeed', 'ALA').
card_original_type('shadowfeed'/'ALA', 'Instant').
card_original_text('shadowfeed'/'ALA', 'Remove target card in a graveyard from the game. You gain 3 life.').
card_first_print('shadowfeed', 'ALA').
card_image_name('shadowfeed'/'ALA', 'shadowfeed').
card_uid('shadowfeed'/'ALA', 'ALA:Shadowfeed:shadowfeed').
card_rarity('shadowfeed'/'ALA', 'Common').
card_artist('shadowfeed'/'ALA', 'Dave Kendall').
card_number('shadowfeed'/'ALA', '86').
card_flavor_text('shadowfeed'/'ALA', '\"The future is a snake, devouring your life backwards through time. And when you die, believe me, it doesn\'t stop feeding.\"\n—Sedris, the Traitor King').
card_multiverse_id('shadowfeed'/'ALA', '177473').

card_in_set('sharding sphinx', 'ALA').
card_original_type('sharding sphinx'/'ALA', 'Artifact Creature — Sphinx').
card_original_text('sharding sphinx'/'ALA', 'Flying\nWhenever an artifact creature you control deals combat damage to a player, you may put a 1/1 blue Thopter artifact creature token with flying into play.').
card_first_print('sharding sphinx', 'ALA').
card_image_name('sharding sphinx'/'ALA', 'sharding sphinx').
card_uid('sharding sphinx'/'ALA', 'ALA:Sharding Sphinx:sharding sphinx').
card_rarity('sharding sphinx'/'ALA', 'Rare').
card_artist('sharding sphinx'/'ALA', 'Michael Bruinsma').
card_number('sharding sphinx'/'ALA', '55').
card_flavor_text('sharding sphinx'/'ALA', 'Whether mechanical or biological, life finds a way to propagate itself.').
card_multiverse_id('sharding sphinx'/'ALA', '174881').

card_in_set('sharuum the hegemon', 'ALA').
card_original_type('sharuum the hegemon'/'ALA', 'Legendary Artifact Creature — Sphinx').
card_original_text('sharuum the hegemon'/'ALA', 'Flying\nWhen Sharuum the Hegemon comes into play, you may return target artifact card from your graveyard to play.').
card_first_print('sharuum the hegemon', 'ALA').
card_image_name('sharuum the hegemon'/'ALA', 'sharuum the hegemon').
card_uid('sharuum the hegemon'/'ALA', 'ALA:Sharuum the Hegemon:sharuum the hegemon').
card_rarity('sharuum the hegemon'/'ALA', 'Mythic Rare').
card_artist('sharuum the hegemon'/'ALA', 'Izzy').
card_number('sharuum the hegemon'/'ALA', '194').
card_flavor_text('sharuum the hegemon'/'ALA', 'To gain audience with the hegemon, one must bring a riddle she has not heard.').
card_multiverse_id('sharuum the hegemon'/'ALA', '175127').

card_in_set('shore snapper', 'ALA').
card_original_type('shore snapper'/'ALA', 'Creature — Beast').
card_original_text('shore snapper'/'ALA', '{U}: Shore Snapper gains islandwalk until end of turn.').
card_first_print('shore snapper', 'ALA').
card_image_name('shore snapper'/'ALA', 'shore snapper').
card_uid('shore snapper'/'ALA', 'ALA:Shore Snapper:shore snapper').
card_rarity('shore snapper'/'ALA', 'Common').
card_artist('shore snapper'/'ALA', 'Dave Kendall').
card_number('shore snapper'/'ALA', '87').
card_flavor_text('shore snapper'/'ALA', 'Kathari, the sickly aven of Grixis, have learned that the corpses by the shoreline are more trap than treat.').
card_multiverse_id('shore snapper'/'ALA', '175390').

card_in_set('sighted-caste sorcerer', 'ALA').
card_original_type('sighted-caste sorcerer'/'ALA', 'Creature — Human Wizard').
card_original_text('sighted-caste sorcerer'/'ALA', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{U}: Sighted-Caste Sorcerer gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_first_print('sighted-caste sorcerer', 'ALA').
card_image_name('sighted-caste sorcerer'/'ALA', 'sighted-caste sorcerer').
card_uid('sighted-caste sorcerer'/'ALA', 'ALA:Sighted-Caste Sorcerer:sighted-caste sorcerer').
card_rarity('sighted-caste sorcerer'/'ALA', 'Common').
card_artist('sighted-caste sorcerer'/'ALA', 'Chris Rahn').
card_number('sighted-caste sorcerer'/'ALA', '26').
card_multiverse_id('sighted-caste sorcerer'/'ALA', '174956').

card_in_set('sigil blessing', 'ALA').
card_original_type('sigil blessing'/'ALA', 'Instant').
card_original_text('sigil blessing'/'ALA', 'Until end of turn, target creature you control gets +3/+3 and other creatures you control get +1/+1.').
card_first_print('sigil blessing', 'ALA').
card_image_name('sigil blessing'/'ALA', 'sigil blessing').
card_uid('sigil blessing'/'ALA', 'ALA:Sigil Blessing:sigil blessing').
card_rarity('sigil blessing'/'ALA', 'Common').
card_artist('sigil blessing'/'ALA', 'Matt Stewart').
card_number('sigil blessing'/'ALA', '195').
card_flavor_text('sigil blessing'/'ALA', '\"For unwavering commitment and unflinching strength, the Order of the White Orchid confers its sigil. Rise, knight-captain, and do your duty.\"').
card_multiverse_id('sigil blessing'/'ALA', '177597').

card_in_set('sigil of distinction', 'ALA').
card_original_type('sigil of distinction'/'ALA', 'Artifact — Equipment').
card_original_text('sigil of distinction'/'ALA', 'Sigil of Distinction comes into play with X charge counters on it.\nEquipped creature gets +1/+1 for each charge counter on Sigil of Distinction.\nEquip—Remove a charge counter from Sigil of Distinction.').
card_first_print('sigil of distinction', 'ALA').
card_image_name('sigil of distinction'/'ALA', 'sigil of distinction').
card_uid('sigil of distinction'/'ALA', 'ALA:Sigil of Distinction:sigil of distinction').
card_rarity('sigil of distinction'/'ALA', 'Rare').
card_artist('sigil of distinction'/'ALA', 'Alan Pollack').
card_number('sigil of distinction'/'ALA', '219').
card_multiverse_id('sigil of distinction'/'ALA', '174867').

card_in_set('sigiled paladin', 'ALA').
card_original_type('sigiled paladin'/'ALA', 'Creature — Human Knight').
card_original_text('sigiled paladin'/'ALA', 'First strike\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('sigiled paladin', 'ALA').
card_image_name('sigiled paladin'/'ALA', 'sigiled paladin').
card_uid('sigiled paladin'/'ALA', 'ALA:Sigiled Paladin:sigiled paladin').
card_rarity('sigiled paladin'/'ALA', 'Uncommon').
card_artist('sigiled paladin'/'ALA', 'Greg Staples').
card_number('sigiled paladin'/'ALA', '27').
card_flavor_text('sigiled paladin'/'ALA', 'Each sigil marks the recognition of a great deed and signifies a duty owed to the one who granted it.').
card_multiverse_id('sigiled paladin'/'ALA', '174958').

card_in_set('skeletal kathari', 'ALA').
card_original_type('skeletal kathari'/'ALA', 'Creature — Bird Skeleton').
card_original_text('skeletal kathari'/'ALA', 'Flying\n{B}, Sacrifice a creature: Regenerate Skeletal Kathari.').
card_first_print('skeletal kathari', 'ALA').
card_image_name('skeletal kathari'/'ALA', 'skeletal kathari').
card_uid('skeletal kathari'/'ALA', 'ALA:Skeletal Kathari:skeletal kathari').
card_rarity('skeletal kathari'/'ALA', 'Common').
card_artist('skeletal kathari'/'ALA', 'Carl Critchlow').
card_number('skeletal kathari'/'ALA', '88').
card_flavor_text('skeletal kathari'/'ALA', 'Undeath doesn\'t end the kathari\'s search for carrion; it only removes one corpse from the Dregscape.').
card_multiverse_id('skeletal kathari'/'ALA', '175119').

card_in_set('skeletonize', 'ALA').
card_original_type('skeletonize'/'ALA', 'Instant').
card_original_text('skeletonize'/'ALA', 'Skeletonize deals 3 damage to target creature. When a creature dealt damage this way is put into a graveyard this turn, put a 1/1 black Skeleton creature token into play with \"{B}: Regenerate this creature.\"').
card_first_print('skeletonize', 'ALA').
card_image_name('skeletonize'/'ALA', 'skeletonize').
card_uid('skeletonize'/'ALA', 'ALA:Skeletonize:skeletonize').
card_rarity('skeletonize'/'ALA', 'Uncommon').
card_artist('skeletonize'/'ALA', 'Karl Kopinski').
card_number('skeletonize'/'ALA', '114').
card_flavor_text('skeletonize'/'ALA', 'Fire burns away both flesh and freedom.').
card_multiverse_id('skeletonize'/'ALA', '175070').

card_in_set('skill borrower', 'ALA').
card_original_type('skill borrower'/'ALA', 'Artifact Creature — Human Wizard').
card_original_text('skill borrower'/'ALA', 'Play with the top card of your library revealed.\nAs long as the top card of your library is an artifact or creature card, Skill Borrower has all activated abilities of that card. (If any of the abilities use that card\'s name, use this creature\'s name instead.)').
card_first_print('skill borrower', 'ALA').
card_image_name('skill borrower'/'ALA', 'skill borrower').
card_uid('skill borrower'/'ALA', 'ALA:Skill Borrower:skill borrower').
card_rarity('skill borrower'/'ALA', 'Rare').
card_artist('skill borrower'/'ALA', 'Shelly Wan').
card_number('skill borrower'/'ALA', '56').
card_multiverse_id('skill borrower'/'ALA', '179428').

card_in_set('skullmulcher', 'ALA').
card_original_type('skullmulcher'/'ALA', 'Creature — Elemental').
card_original_text('skullmulcher'/'ALA', 'Devour 1 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with that many +1/+1 counters on it.)\nWhen Skullmulcher comes into play, draw a card for each creature it devoured.').
card_first_print('skullmulcher', 'ALA').
card_image_name('skullmulcher'/'ALA', 'skullmulcher').
card_uid('skullmulcher'/'ALA', 'ALA:Skullmulcher:skullmulcher').
card_rarity('skullmulcher'/'ALA', 'Rare').
card_artist('skullmulcher'/'ALA', 'Michael Ryan').
card_number('skullmulcher'/'ALA', '148').
card_multiverse_id('skullmulcher'/'ALA', '176436').

card_in_set('soul\'s fire', 'ALA').
card_original_type('soul\'s fire'/'ALA', 'Instant').
card_original_text('soul\'s fire'/'ALA', 'Target creature you control in play deals damage equal to its power to target creature or player.').
card_first_print('soul\'s fire', 'ALA').
card_image_name('soul\'s fire'/'ALA', 'soul\'s fire').
card_uid('soul\'s fire'/'ALA', 'ALA:Soul\'s Fire:soul\'s fire').
card_rarity('soul\'s fire'/'ALA', 'Common').
card_artist('soul\'s fire'/'ALA', 'Wayne Reynolds').
card_number('soul\'s fire'/'ALA', '115').
card_flavor_text('soul\'s fire'/'ALA', 'An avatar he sculpts of anger and flame.').
card_multiverse_id('soul\'s fire'/'ALA', '174951').

card_in_set('soul\'s grace', 'ALA').
card_original_type('soul\'s grace'/'ALA', 'Instant').
card_original_text('soul\'s grace'/'ALA', 'You gain life equal to target creature\'s power.').
card_first_print('soul\'s grace', 'ALA').
card_image_name('soul\'s grace'/'ALA', 'soul\'s grace').
card_uid('soul\'s grace'/'ALA', 'ALA:Soul\'s Grace:soul\'s grace').
card_rarity('soul\'s grace'/'ALA', 'Common').
card_artist('soul\'s grace'/'ALA', 'Christopher Moeller').
card_number('soul\'s grace'/'ALA', '28').
card_flavor_text('soul\'s grace'/'ALA', 'An avatar he sculpts of fellowship and light.').
card_multiverse_id('soul\'s grace'/'ALA', '174813').

card_in_set('soul\'s might', 'ALA').
card_original_type('soul\'s might'/'ALA', 'Sorcery').
card_original_text('soul\'s might'/'ALA', 'Put X +1/+1 counters on target creature, where X is that creature\'s power.').
card_first_print('soul\'s might', 'ALA').
card_image_name('soul\'s might'/'ALA', 'soul\'s might').
card_uid('soul\'s might'/'ALA', 'ALA:Soul\'s Might:soul\'s might').
card_rarity('soul\'s might'/'ALA', 'Common').
card_artist('soul\'s might'/'ALA', 'Kev Walker').
card_number('soul\'s might'/'ALA', '149').
card_flavor_text('soul\'s might'/'ALA', 'An avatar he sculpts of instinct and force.').
card_multiverse_id('soul\'s might'/'ALA', '174808').

card_in_set('spearbreaker behemoth', 'ALA').
card_original_type('spearbreaker behemoth'/'ALA', 'Creature — Beast').
card_original_text('spearbreaker behemoth'/'ALA', 'Spearbreaker Behemoth is indestructible.\n{1}: Target creature with power 5 or greater is indestructible this turn.').
card_first_print('spearbreaker behemoth', 'ALA').
card_image_name('spearbreaker behemoth'/'ALA', 'spearbreaker behemoth').
card_uid('spearbreaker behemoth'/'ALA', 'ALA:Spearbreaker Behemoth:spearbreaker behemoth').
card_rarity('spearbreaker behemoth'/'ALA', 'Rare').
card_artist('spearbreaker behemoth'/'ALA', 'Christopher Moeller').
card_number('spearbreaker behemoth'/'ALA', '150').
card_flavor_text('spearbreaker behemoth'/'ALA', 'Few Nayans dare hunt the gargantuans. They\'re regarded not as animals but as forces of nature, like landslides or typhoons.').
card_multiverse_id('spearbreaker behemoth'/'ALA', '174916').

card_in_set('spell snip', 'ALA').
card_original_type('spell snip'/'ALA', 'Instant').
card_original_text('spell snip'/'ALA', 'Counter target spell unless its controller pays {1}.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('spell snip', 'ALA').
card_image_name('spell snip'/'ALA', 'spell snip').
card_uid('spell snip'/'ALA', 'ALA:Spell Snip:spell snip').
card_rarity('spell snip'/'ALA', 'Common').
card_artist('spell snip'/'ALA', 'Michael Sutfin').
card_number('spell snip'/'ALA', '57').
card_flavor_text('spell snip'/'ALA', '\"Magic cast through a hand of flesh is just as weak as the flesh itself.\"\n—Lodus of the Ethersworn').
card_multiverse_id('spell snip'/'ALA', '175038').

card_in_set('sphinx sovereign', 'ALA').
card_original_type('sphinx sovereign'/'ALA', 'Artifact Creature — Sphinx').
card_original_text('sphinx sovereign'/'ALA', 'Flying\nAt the end of your turn, you gain 3 life if Sphinx Sovereign is untapped. Otherwise, each opponent loses 3 life.').
card_first_print('sphinx sovereign', 'ALA').
card_image_name('sphinx sovereign'/'ALA', 'sphinx sovereign').
card_uid('sphinx sovereign'/'ALA', 'ALA:Sphinx Sovereign:sphinx sovereign').
card_rarity('sphinx sovereign'/'ALA', 'Mythic Rare').
card_artist('sphinx sovereign'/'ALA', 'Chippy').
card_number('sphinx sovereign'/'ALA', '196').
card_flavor_text('sphinx sovereign'/'ALA', '\"What rises without legs, whispers without a voice, bites without teeth, and dies without having life?\"').
card_multiverse_id('sphinx sovereign'/'ALA', '175107').

card_in_set('sphinx\'s herald', 'ALA').
card_original_type('sphinx\'s herald'/'ALA', 'Artifact Creature — Vedalken Wizard').
card_original_text('sphinx\'s herald'/'ALA', '{2}{U}, {T}, Sacrifice a white creature, a blue creature, and a black creature: Search your library for a card named Sphinx Sovereign and put it into play. Then shuffle your library.').
card_first_print('sphinx\'s herald', 'ALA').
card_image_name('sphinx\'s herald'/'ALA', 'sphinx\'s herald').
card_uid('sphinx\'s herald'/'ALA', 'ALA:Sphinx\'s Herald:sphinx\'s herald').
card_rarity('sphinx\'s herald'/'ALA', 'Uncommon').
card_artist('sphinx\'s herald'/'ALA', 'Dan Scott').
card_number('sphinx\'s herald'/'ALA', '58').
card_flavor_text('sphinx\'s herald'/'ALA', 'He calls a riddle through the gaps of cloven clouds, hoping for an answer.').
card_multiverse_id('sphinx\'s herald'/'ALA', '175242').

card_in_set('sprouting thrinax', 'ALA').
card_original_type('sprouting thrinax'/'ALA', 'Creature — Lizard').
card_original_text('sprouting thrinax'/'ALA', 'When Sprouting Thrinax is put into a graveyard from play, put three 1/1 green Saproling creature tokens into play.').
card_image_name('sprouting thrinax'/'ALA', 'sprouting thrinax').
card_uid('sprouting thrinax'/'ALA', 'ALA:Sprouting Thrinax:sprouting thrinax').
card_rarity('sprouting thrinax'/'ALA', 'Uncommon').
card_artist('sprouting thrinax'/'ALA', 'Jarreau Wimberly').
card_number('sprouting thrinax'/'ALA', '197').
card_flavor_text('sprouting thrinax'/'ALA', 'The vast network of predation on Jund has actually caused some strange creatures to adapt to being eaten.').
card_multiverse_id('sprouting thrinax'/'ALA', '174863').

card_in_set('steelclad serpent', 'ALA').
card_original_type('steelclad serpent'/'ALA', 'Artifact Creature — Serpent').
card_original_text('steelclad serpent'/'ALA', 'Steelclad Serpent can\'t attack unless you control another artifact.').
card_first_print('steelclad serpent', 'ALA').
card_image_name('steelclad serpent'/'ALA', 'steelclad serpent').
card_uid('steelclad serpent'/'ALA', 'ALA:Steelclad Serpent:steelclad serpent').
card_rarity('steelclad serpent'/'ALA', 'Common').
card_artist('steelclad serpent'/'ALA', 'Carl Critchlow').
card_number('steelclad serpent'/'ALA', '59').
card_flavor_text('steelclad serpent'/'ALA', '\"From the highest towers to the deepest sea, all life must be touched by etherium.\"\n—Lodus of the Ethersworn').
card_multiverse_id('steelclad serpent'/'ALA', '175009').

card_in_set('steward of valeron', 'ALA').
card_original_type('steward of valeron'/'ALA', 'Creature — Human Druid Knight').
card_original_text('steward of valeron'/'ALA', 'Vigilance\n{T}: Add {G} to your mana pool.').
card_image_name('steward of valeron'/'ALA', 'steward of valeron').
card_uid('steward of valeron'/'ALA', 'ALA:Steward of Valeron:steward of valeron').
card_rarity('steward of valeron'/'ALA', 'Common').
card_artist('steward of valeron'/'ALA', 'Greg Staples').
card_number('steward of valeron'/'ALA', '198').
card_flavor_text('steward of valeron'/'ALA', 'Knight-stewards guard the Sun-Dappled Court, a grove of immense, sculptured olive trees that represent Valeron\'s twelve noble families.').
card_multiverse_id('steward of valeron'/'ALA', '175134').

card_in_set('stoic angel', 'ALA').
card_original_type('stoic angel'/'ALA', 'Creature — Angel').
card_original_text('stoic angel'/'ALA', 'Flying, vigilance\nPlayers can\'t untap more than one creature during their untap steps.').
card_first_print('stoic angel', 'ALA').
card_image_name('stoic angel'/'ALA', 'stoic angel').
card_uid('stoic angel'/'ALA', 'ALA:Stoic Angel:stoic angel').
card_rarity('stoic angel'/'ALA', 'Rare').
card_artist('stoic angel'/'ALA', 'Volkan Baga').
card_number('stoic angel'/'ALA', '199').
card_flavor_text('stoic angel'/'ALA', 'Even the most battle-hardened soldiers pause in her presence for a moment of introspection.').
card_multiverse_id('stoic angel'/'ALA', '175396').

card_in_set('sunseed nurturer', 'ALA').
card_original_type('sunseed nurturer'/'ALA', 'Creature — Human Druid Wizard').
card_original_text('sunseed nurturer'/'ALA', 'At the end of your turn, if you control a creature with power 5 or greater, you may gain 2 life.\n{T}: Add {1} to your mana pool.').
card_first_print('sunseed nurturer', 'ALA').
card_image_name('sunseed nurturer'/'ALA', 'sunseed nurturer').
card_uid('sunseed nurturer'/'ALA', 'ALA:Sunseed Nurturer:sunseed nurturer').
card_rarity('sunseed nurturer'/'ALA', 'Uncommon').
card_artist('sunseed nurturer'/'ALA', 'Steve Argyle').
card_number('sunseed nurturer'/'ALA', '29').
card_flavor_text('sunseed nurturer'/'ALA', 'Sunseeders quest for areas of open sky. They train plowbeasts to beat back the dense jungle long enough to cultivate a crop.').
card_multiverse_id('sunseed nurturer'/'ALA', '175122').

card_in_set('swamp', 'ALA').
card_original_type('swamp'/'ALA', 'Basic Land — Swamp').
card_original_text('swamp'/'ALA', 'B').
card_image_name('swamp'/'ALA', 'swamp1').
card_uid('swamp'/'ALA', 'ALA:Swamp:swamp1').
card_rarity('swamp'/'ALA', 'Basic Land').
card_artist('swamp'/'ALA', 'Chippy').
card_number('swamp'/'ALA', '238').
card_multiverse_id('swamp'/'ALA', '175090').

card_in_set('swamp', 'ALA').
card_original_type('swamp'/'ALA', 'Basic Land — Swamp').
card_original_text('swamp'/'ALA', 'B').
card_image_name('swamp'/'ALA', 'swamp2').
card_uid('swamp'/'ALA', 'ALA:Swamp:swamp2').
card_rarity('swamp'/'ALA', 'Basic Land').
card_artist('swamp'/'ALA', 'Mark Tedin').
card_number('swamp'/'ALA', '239').
card_multiverse_id('swamp'/'ALA', '175089').

card_in_set('swamp', 'ALA').
card_original_type('swamp'/'ALA', 'Basic Land — Swamp').
card_original_text('swamp'/'ALA', 'B').
card_image_name('swamp'/'ALA', 'swamp3').
card_uid('swamp'/'ALA', 'ALA:Swamp:swamp3').
card_rarity('swamp'/'ALA', 'Basic Land').
card_artist('swamp'/'ALA', 'Mark Tedin').
card_number('swamp'/'ALA', '240').
card_multiverse_id('swamp'/'ALA', '175088').

card_in_set('swamp', 'ALA').
card_original_type('swamp'/'ALA', 'Basic Land — Swamp').
card_original_text('swamp'/'ALA', 'B').
card_image_name('swamp'/'ALA', 'swamp4').
card_uid('swamp'/'ALA', 'ALA:Swamp:swamp4').
card_rarity('swamp'/'ALA', 'Basic Land').
card_artist('swamp'/'ALA', 'Aleksi Briclot').
card_number('swamp'/'ALA', '241').
card_multiverse_id('swamp'/'ALA', '175091').

card_in_set('swerve', 'ALA').
card_original_type('swerve'/'ALA', 'Instant').
card_original_text('swerve'/'ALA', 'Change the target of target spell with a single target.').
card_first_print('swerve', 'ALA').
card_image_name('swerve'/'ALA', 'swerve').
card_uid('swerve'/'ALA', 'ALA:Swerve:swerve').
card_rarity('swerve'/'ALA', 'Uncommon').
card_artist('swerve'/'ALA', 'Karl Kopinski').
card_number('swerve'/'ALA', '200').
card_flavor_text('swerve'/'ALA', '\"We can\'t beat the necromancers in numbers or raw power. We must beat them with ingenuity and timing.\"\n—Rannon, Vithian holdout').
card_multiverse_id('swerve'/'ALA', '175010').

card_in_set('tar fiend', 'ALA').
card_original_type('tar fiend'/'ALA', 'Creature — Elemental').
card_original_text('tar fiend'/'ALA', 'Devour 2 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with twice that many +1/+1 counters on it.)\nWhen Tar Fiend comes into play, target player discards a card for each creature it devoured.').
card_first_print('tar fiend', 'ALA').
card_image_name('tar fiend'/'ALA', 'tar fiend').
card_uid('tar fiend'/'ALA', 'ALA:Tar Fiend:tar fiend').
card_rarity('tar fiend'/'ALA', 'Rare').
card_artist('tar fiend'/'ALA', 'Anthony S. Waters').
card_number('tar fiend'/'ALA', '89').
card_multiverse_id('tar fiend'/'ALA', '175101').

card_in_set('tezzeret the seeker', 'ALA').
card_original_type('tezzeret the seeker'/'ALA', 'Planeswalker — Tezzeret').
card_original_text('tezzeret the seeker'/'ALA', '+1: Untap up to two target artifacts.\n-X: Search your library for an artifact card with converted mana cost X or less and put it into play. Then shuffle your library.\n-5: Artifacts you control become 5/5 artifact creatures until end of turn.').
card_first_print('tezzeret the seeker', 'ALA').
card_image_name('tezzeret the seeker'/'ALA', 'tezzeret the seeker').
card_uid('tezzeret the seeker'/'ALA', 'ALA:Tezzeret the Seeker:tezzeret the seeker').
card_rarity('tezzeret the seeker'/'ALA', 'Mythic Rare').
card_artist('tezzeret the seeker'/'ALA', 'Anthony Francisco').
card_number('tezzeret the seeker'/'ALA', '60').
card_multiverse_id('tezzeret the seeker'/'ALA', '174912').

card_in_set('thorn-thrash viashino', 'ALA').
card_original_type('thorn-thrash viashino'/'ALA', 'Creature — Viashino Warrior').
card_original_text('thorn-thrash viashino'/'ALA', 'Devour 2 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with twice that many +1/+1 counters on it.)\n{G}: Thorn-Thrash Viashino gains trample until end of turn.').
card_first_print('thorn-thrash viashino', 'ALA').
card_image_name('thorn-thrash viashino'/'ALA', 'thorn-thrash viashino').
card_uid('thorn-thrash viashino'/'ALA', 'ALA:Thorn-Thrash Viashino:thorn-thrash viashino').
card_rarity('thorn-thrash viashino'/'ALA', 'Common').
card_artist('thorn-thrash viashino'/'ALA', 'Jon Foster').
card_number('thorn-thrash viashino'/'ALA', '116').
card_multiverse_id('thorn-thrash viashino'/'ALA', '175064').

card_in_set('thoughtcutter agent', 'ALA').
card_original_type('thoughtcutter agent'/'ALA', 'Artifact Creature — Human Rogue').
card_original_text('thoughtcutter agent'/'ALA', '{U}{B}, {T}: Target player loses 1 life and reveals his or her hand.').
card_first_print('thoughtcutter agent', 'ALA').
card_image_name('thoughtcutter agent'/'ALA', 'thoughtcutter agent').
card_uid('thoughtcutter agent'/'ALA', 'ALA:Thoughtcutter Agent:thoughtcutter agent').
card_rarity('thoughtcutter agent'/'ALA', 'Uncommon').
card_artist('thoughtcutter agent'/'ALA', 'Cyril Van Der Haegen').
card_number('thoughtcutter agent'/'ALA', '201').
card_flavor_text('thoughtcutter agent'/'ALA', 'Agents of the Architects of Will mark their victims as a reminder that their secrets are secret no longer.').
card_multiverse_id('thoughtcutter agent'/'ALA', '177601').

card_in_set('thunder-thrash elder', 'ALA').
card_original_type('thunder-thrash elder'/'ALA', 'Creature — Viashino Warrior').
card_original_text('thunder-thrash elder'/'ALA', 'Devour 3 (As this comes into play, you may sacrifice any number of creatures. This creature comes into play with three times that many +1/+1 counters on it.)').
card_first_print('thunder-thrash elder', 'ALA').
card_image_name('thunder-thrash elder'/'ALA', 'thunder-thrash elder').
card_uid('thunder-thrash elder'/'ALA', 'ALA:Thunder-Thrash Elder:thunder-thrash elder').
card_rarity('thunder-thrash elder'/'ALA', 'Uncommon').
card_artist('thunder-thrash elder'/'ALA', 'Brandon Kitkouski').
card_number('thunder-thrash elder'/'ALA', '117').
card_flavor_text('thunder-thrash elder'/'ALA', 'Viashino thrashes are led by elders who have survived countless challenges.').
card_multiverse_id('thunder-thrash elder'/'ALA', '174870').

card_in_set('tidehollow sculler', 'ALA').
card_original_type('tidehollow sculler'/'ALA', 'Artifact Creature — Zombie').
card_original_text('tidehollow sculler'/'ALA', 'When Tidehollow Sculler comes into play, target opponent reveals his or her hand and you choose a nonland card from it. Remove that card from the game.\nWhen Tidehollow Sculler leaves play, return the removed card to its owner\'s hand.').
card_image_name('tidehollow sculler'/'ALA', 'tidehollow sculler').
card_uid('tidehollow sculler'/'ALA', 'ALA:Tidehollow Sculler:tidehollow sculler').
card_rarity('tidehollow sculler'/'ALA', 'Uncommon').
card_artist('tidehollow sculler'/'ALA', 'rk post').
card_number('tidehollow sculler'/'ALA', '202').
card_multiverse_id('tidehollow sculler'/'ALA', '175054').

card_in_set('tidehollow strix', 'ALA').
card_original_type('tidehollow strix'/'ALA', 'Artifact Creature — Bird').
card_original_text('tidehollow strix'/'ALA', 'Flying\nDeathtouch (Whenever this creature deals damage to a creature, destroy that creature.)').
card_first_print('tidehollow strix', 'ALA').
card_image_name('tidehollow strix'/'ALA', 'tidehollow strix').
card_uid('tidehollow strix'/'ALA', 'ALA:Tidehollow Strix:tidehollow strix').
card_rarity('tidehollow strix'/'ALA', 'Common').
card_artist('tidehollow strix'/'ALA', 'Cyril Van Der Haegen').
card_number('tidehollow strix'/'ALA', '203').
card_flavor_text('tidehollow strix'/'ALA', 'The scullers beneath Esper keep strixes as trained pets, and set them loose when a fare refuses to pay.').
card_multiverse_id('tidehollow strix'/'ALA', '175015').

card_in_set('titanic ultimatum', 'ALA').
card_original_type('titanic ultimatum'/'ALA', 'Sorcery').
card_original_text('titanic ultimatum'/'ALA', 'Until end of turn, creatures you control get +5/+5 and gain first strike, lifelink, and trample.').
card_first_print('titanic ultimatum', 'ALA').
card_image_name('titanic ultimatum'/'ALA', 'titanic ultimatum').
card_uid('titanic ultimatum'/'ALA', 'ALA:Titanic Ultimatum:titanic ultimatum').
card_rarity('titanic ultimatum'/'ALA', 'Rare').
card_artist('titanic ultimatum'/'ALA', 'Steve Prescott').
card_number('titanic ultimatum'/'ALA', '204').
card_flavor_text('titanic ultimatum'/'ALA', '\"Retribution is best delivered by claws and rage, with both magnified.\"\n—Ajani').
card_multiverse_id('titanic ultimatum'/'ALA', '174839').

card_in_set('topan ascetic', 'ALA').
card_original_type('topan ascetic'/'ALA', 'Creature — Human Monk').
card_original_text('topan ascetic'/'ALA', 'Tap an untapped creature you control: Topan Ascetic gets +1/+1 until end of turn.').
card_first_print('topan ascetic', 'ALA').
card_image_name('topan ascetic'/'ALA', 'topan ascetic').
card_uid('topan ascetic'/'ALA', 'ALA:Topan Ascetic:topan ascetic').
card_rarity('topan ascetic'/'ALA', 'Uncommon').
card_artist('topan ascetic'/'ALA', 'Sal Villagran').
card_number('topan ascetic'/'ALA', '151').
card_flavor_text('topan ascetic'/'ALA', 'Monks from Topa wander all of Bant, encouraging the Unbeholden to find their place in society through honorable combat.').
card_multiverse_id('topan ascetic'/'ALA', '174999').

card_in_set('tortoise formation', 'ALA').
card_original_type('tortoise formation'/'ALA', 'Instant').
card_original_text('tortoise formation'/'ALA', 'Creatures you control gain shroud until end of turn. (They can\'t be the targets of spells or abilities.)').
card_first_print('tortoise formation', 'ALA').
card_image_name('tortoise formation'/'ALA', 'tortoise formation').
card_uid('tortoise formation'/'ALA', 'ALA:Tortoise Formation:tortoise formation').
card_rarity('tortoise formation'/'ALA', 'Common').
card_artist('tortoise formation'/'ALA', 'Mark Zug').
card_number('tortoise formation'/'ALA', '61').
card_flavor_text('tortoise formation'/'ALA', 'At sea, the Jhessian fleet strikes swiftly and decisively. On land, facing the elite cavalry of Valeron, its marines must rely on more cautious strategies.').
card_multiverse_id('tortoise formation'/'ALA', '179432').

card_in_set('tower gargoyle', 'ALA').
card_original_type('tower gargoyle'/'ALA', 'Artifact Creature — Gargoyle').
card_original_text('tower gargoyle'/'ALA', 'Flying').
card_first_print('tower gargoyle', 'ALA').
card_image_name('tower gargoyle'/'ALA', 'tower gargoyle').
card_uid('tower gargoyle'/'ALA', 'ALA:Tower Gargoyle:tower gargoyle').
card_rarity('tower gargoyle'/'ALA', 'Uncommon').
card_artist('tower gargoyle'/'ALA', 'Matt Cavotta').
card_number('tower gargoyle'/'ALA', '205').
card_flavor_text('tower gargoyle'/'ALA', '\"Esper, like any work of art, can be truly appreciated only from a distance.\"\n—Tezzeret').
card_multiverse_id('tower gargoyle'/'ALA', '174924').

card_in_set('undead leotau', 'ALA').
card_original_type('undead leotau'/'ALA', 'Creature — Zombie Cat').
card_original_text('undead leotau'/'ALA', '{R}: Undead Leotau gets +1/-1 until end of turn.\nUnearth {2}{B} ({2}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('undead leotau', 'ALA').
card_image_name('undead leotau'/'ALA', 'undead leotau').
card_uid('undead leotau'/'ALA', 'ALA:Undead Leotau:undead leotau').
card_rarity('undead leotau'/'ALA', 'Common').
card_artist('undead leotau'/'ALA', 'Carl Critchlow').
card_number('undead leotau'/'ALA', '90').
card_multiverse_id('undead leotau'/'ALA', '176452').

card_in_set('vectis silencers', 'ALA').
card_original_type('vectis silencers'/'ALA', 'Artifact Creature — Human Rogue').
card_original_text('vectis silencers'/'ALA', '{2}{B}: Vectis Silencers gains deathtouch until end of turn. (Whenever it deals damage to a creature, destroy that creature.)').
card_first_print('vectis silencers', 'ALA').
card_image_name('vectis silencers'/'ALA', 'vectis silencers').
card_uid('vectis silencers'/'ALA', 'ALA:Vectis Silencers:vectis silencers').
card_rarity('vectis silencers'/'ALA', 'Common').
card_artist('vectis silencers'/'ALA', 'Steven Belledin').
card_number('vectis silencers'/'ALA', '62').
card_flavor_text('vectis silencers'/'ALA', 'Even on Esper, there are those who eschew the use of magic in favor of simpler methods.').
card_multiverse_id('vectis silencers'/'ALA', '175147').

card_in_set('vein drinker', 'ALA').
card_original_type('vein drinker'/'ALA', 'Creature — Vampire').
card_original_text('vein drinker'/'ALA', 'Flying\n{R}, {T}: Vein Drinker deals damage equal to its power to target creature. That creature deals damage equal to its power to Vein Drinker.\nWhenever a creature dealt damage by Vein Drinker this turn is put into a graveyard, put a +1/+1 counter on Vein Drinker.').
card_first_print('vein drinker', 'ALA').
card_image_name('vein drinker'/'ALA', 'vein drinker').
card_uid('vein drinker'/'ALA', 'ALA:Vein Drinker:vein drinker').
card_rarity('vein drinker'/'ALA', 'Rare').
card_artist('vein drinker'/'ALA', 'Lars Grant-West').
card_number('vein drinker'/'ALA', '91').
card_multiverse_id('vein drinker'/'ALA', '175004').

card_in_set('viashino skeleton', 'ALA').
card_original_type('viashino skeleton'/'ALA', 'Creature — Viashino Skeleton').
card_original_text('viashino skeleton'/'ALA', '{1}{B}, Discard a card: Regenerate Viashino Skeleton.').
card_first_print('viashino skeleton', 'ALA').
card_image_name('viashino skeleton'/'ALA', 'viashino skeleton').
card_uid('viashino skeleton'/'ALA', 'ALA:Viashino Skeleton:viashino skeleton').
card_rarity('viashino skeleton'/'ALA', 'Common').
card_artist('viashino skeleton'/'ALA', 'Cole Eastburn').
card_number('viashino skeleton'/'ALA', '118').
card_flavor_text('viashino skeleton'/'ALA', 'Underneath the Dregscape lay the remains of creatures long extinct from Grixis.').
card_multiverse_id('viashino skeleton'/'ALA', '175097').

card_in_set('vicious shadows', 'ALA').
card_original_type('vicious shadows'/'ALA', 'Enchantment').
card_original_text('vicious shadows'/'ALA', 'Whenever a creature is put into a graveyard from play, you may have Vicious Shadows deal damage to target player equal to the number of cards in that player\'s hand.').
card_first_print('vicious shadows', 'ALA').
card_image_name('vicious shadows'/'ALA', 'vicious shadows').
card_uid('vicious shadows'/'ALA', 'ALA:Vicious Shadows:vicious shadows').
card_rarity('vicious shadows'/'ALA', 'Rare').
card_artist('vicious shadows'/'ALA', 'Joshua Hagler').
card_number('vicious shadows'/'ALA', '119').
card_flavor_text('vicious shadows'/'ALA', '\"Thrashes of the ancient past still linger in spirit and shadow, desperate to sink their claws into our souls.\"\n—Rakka Mar').
card_multiverse_id('vicious shadows'/'ALA', '175048').

card_in_set('violent ultimatum', 'ALA').
card_original_type('violent ultimatum'/'ALA', 'Sorcery').
card_original_text('violent ultimatum'/'ALA', 'Destroy three target permanents.').
card_first_print('violent ultimatum', 'ALA').
card_image_name('violent ultimatum'/'ALA', 'violent ultimatum').
card_uid('violent ultimatum'/'ALA', 'ALA:Violent Ultimatum:violent ultimatum').
card_rarity('violent ultimatum'/'ALA', 'Rare').
card_artist('violent ultimatum'/'ALA', 'Raymond Swanland').
card_number('violent ultimatum'/'ALA', '206').
card_flavor_text('violent ultimatum'/'ALA', '\"Words are a waste of time. Destruction is a language everyone understands.\"\n—Sarkhan Vol').
card_multiverse_id('violent ultimatum'/'ALA', '175135').

card_in_set('viscera dragger', 'ALA').
card_original_type('viscera dragger'/'ALA', 'Creature — Zombie Ogre Warrior').
card_original_text('viscera dragger'/'ALA', 'Cycling {2} ({2}, Discard this card: Draw a card.)\nUnearth {1}{B} ({1}{B}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('viscera dragger', 'ALA').
card_image_name('viscera dragger'/'ALA', 'viscera dragger').
card_uid('viscera dragger'/'ALA', 'ALA:Viscera Dragger:viscera dragger').
card_rarity('viscera dragger'/'ALA', 'Common').
card_artist('viscera dragger'/'ALA', 'Ralph Horsley').
card_number('viscera dragger'/'ALA', '92').
card_multiverse_id('viscera dragger'/'ALA', '175144').

card_in_set('vithian stinger', 'ALA').
card_original_type('vithian stinger'/'ALA', 'Creature — Human Shaman').
card_original_text('vithian stinger'/'ALA', '{T}: Vithian Stinger deals 1 damage to target creature or player.\nUnearth {1}{R} ({1}{R}: Return this card from your graveyard to play. It gains haste. Remove it from the game at end of turn or if it would leave play. Unearth only as a sorcery.)').
card_first_print('vithian stinger', 'ALA').
card_image_name('vithian stinger'/'ALA', 'vithian stinger').
card_uid('vithian stinger'/'ALA', 'ALA:Vithian Stinger:vithian stinger').
card_rarity('vithian stinger'/'ALA', 'Common').
card_artist('vithian stinger'/'ALA', 'Dave Kendall').
card_number('vithian stinger'/'ALA', '120').
card_multiverse_id('vithian stinger'/'ALA', '174922').

card_in_set('volcanic submersion', 'ALA').
card_original_type('volcanic submersion'/'ALA', 'Sorcery').
card_original_text('volcanic submersion'/'ALA', 'Destroy target artifact or land.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('volcanic submersion', 'ALA').
card_image_name('volcanic submersion'/'ALA', 'volcanic submersion').
card_uid('volcanic submersion'/'ALA', 'ALA:Volcanic Submersion:volcanic submersion').
card_rarity('volcanic submersion'/'ALA', 'Common').
card_artist('volcanic submersion'/'ALA', 'Trevor Claxton').
card_number('volcanic submersion'/'ALA', '121').
card_flavor_text('volcanic submersion'/'ALA', 'A dragon\'s death is almost as feared as its life. Old, dying dragons throw themselves into volcanoes, causing massive upheaval and widespread disaster.').
card_multiverse_id('volcanic submersion'/'ALA', '174889').

card_in_set('waveskimmer aven', 'ALA').
card_original_type('waveskimmer aven'/'ALA', 'Creature — Bird Soldier').
card_original_text('waveskimmer aven'/'ALA', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('waveskimmer aven', 'ALA').
card_image_name('waveskimmer aven'/'ALA', 'waveskimmer aven').
card_uid('waveskimmer aven'/'ALA', 'ALA:Waveskimmer Aven:waveskimmer aven').
card_rarity('waveskimmer aven'/'ALA', 'Common').
card_artist('waveskimmer aven'/'ALA', 'Mark Zug').
card_number('waveskimmer aven'/'ALA', '207').
card_flavor_text('waveskimmer aven'/'ALA', 'The greatest gift you can give a sea aven is solitude.').
card_multiverse_id('waveskimmer aven'/'ALA', '174955').

card_in_set('welkin guide', 'ALA').
card_original_type('welkin guide'/'ALA', 'Creature — Bird Cleric').
card_original_text('welkin guide'/'ALA', 'Flying\nWhen Welkin Guide comes into play, target creature gets +2/+2 and gains flying until end of turn.').
card_first_print('welkin guide', 'ALA').
card_image_name('welkin guide'/'ALA', 'welkin guide').
card_uid('welkin guide'/'ALA', 'ALA:Welkin Guide:welkin guide').
card_rarity('welkin guide'/'ALA', 'Common').
card_artist('welkin guide'/'ALA', 'David Palumbo').
card_number('welkin guide'/'ALA', '30').
card_flavor_text('welkin guide'/'ALA', '\"Those talons really dig into your skin, but it\'s better than being dropped.\"\n—Rafiq of the Many').
card_multiverse_id('welkin guide'/'ALA', '178096').

card_in_set('where ancients tread', 'ALA').
card_original_type('where ancients tread'/'ALA', 'Enchantment').
card_original_text('where ancients tread'/'ALA', 'Whenever a creature with power 5 or greater comes into play under your control, you may have Where Ancients Tread deal 5 damage to target creature or player.').
card_first_print('where ancients tread', 'ALA').
card_image_name('where ancients tread'/'ALA', 'where ancients tread').
card_uid('where ancients tread'/'ALA', 'ALA:Where Ancients Tread:where ancients tread').
card_rarity('where ancients tread'/'ALA', 'Rare').
card_artist('where ancients tread'/'ALA', 'Zoltan Boros & Gabor Szikszai').
card_number('where ancients tread'/'ALA', '122').
card_flavor_text('where ancients tread'/'ALA', '\"Never subtle nor cryptic is the reckoning of the mighty.\"\n—Mayael the Anima').
card_multiverse_id('where ancients tread'/'ALA', '175115').

card_in_set('wild nacatl', 'ALA').
card_original_type('wild nacatl'/'ALA', 'Creature — Cat Warrior').
card_original_text('wild nacatl'/'ALA', 'Wild Nacatl gets +1/+1 as long as you control a Mountain.\nWild Nacatl gets +1/+1 as long as you control a Plains.').
card_image_name('wild nacatl'/'ALA', 'wild nacatl').
card_uid('wild nacatl'/'ALA', 'ALA:Wild Nacatl:wild nacatl').
card_rarity('wild nacatl'/'ALA', 'Common').
card_artist('wild nacatl'/'ALA', 'Wayne Reynolds').
card_number('wild nacatl'/'ALA', '152').
card_flavor_text('wild nacatl'/'ALA', '\"The Cloud Nacatl sit and think, a bunch of soft paws. We are the Claws of Marisi, stalking, pouncing, drawing blood.\"').
card_multiverse_id('wild nacatl'/'ALA', '174989').

card_in_set('windwright mage', 'ALA').
card_original_type('windwright mage'/'ALA', 'Artifact Creature — Human Wizard').
card_original_text('windwright mage'/'ALA', 'Lifelink (Whenever this creature deals damage, you gain that much life.)\nWindwright Mage has flying as long as an artifact card is in your graveyard.').
card_first_print('windwright mage', 'ALA').
card_image_name('windwright mage'/'ALA', 'windwright mage').
card_uid('windwright mage'/'ALA', 'ALA:Windwright Mage:windwright mage').
card_rarity('windwright mage'/'ALA', 'Common').
card_artist('windwright mage'/'ALA', 'Chippy').
card_number('windwright mage'/'ALA', '208').
card_flavor_text('windwright mage'/'ALA', 'She knows the names and secrets of each of Esper\'s twenty-three winds.').
card_multiverse_id('windwright mage'/'ALA', '175014').

card_in_set('woolly thoctar', 'ALA').
card_original_type('woolly thoctar'/'ALA', 'Creature — Beast').
card_original_text('woolly thoctar'/'ALA', '').
card_image_name('woolly thoctar'/'ALA', 'woolly thoctar').
card_uid('woolly thoctar'/'ALA', 'ALA:Woolly Thoctar:woolly thoctar').
card_rarity('woolly thoctar'/'ALA', 'Uncommon').
card_artist('woolly thoctar'/'ALA', 'Wayne Reynolds').
card_number('woolly thoctar'/'ALA', '209').
card_flavor_text('woolly thoctar'/'ALA', 'One of the most ferocious and deadly gargantuans, the thoctar never sees its worshippers, but it often awakens surrounded by gifts and sacrifices.').
card_multiverse_id('woolly thoctar'/'ALA', '175062').

card_in_set('yoked plowbeast', 'ALA').
card_original_type('yoked plowbeast'/'ALA', 'Creature — Beast').
card_original_text('yoked plowbeast'/'ALA', 'Cycling {2} ({2}, Discard this card: Draw a card.)').
card_first_print('yoked plowbeast', 'ALA').
card_image_name('yoked plowbeast'/'ALA', 'yoked plowbeast').
card_uid('yoked plowbeast'/'ALA', 'ALA:Yoked Plowbeast:yoked plowbeast').
card_rarity('yoked plowbeast'/'ALA', 'Common').
card_artist('yoked plowbeast'/'ALA', 'Steve Argyle').
card_number('yoked plowbeast'/'ALA', '31').
card_flavor_text('yoked plowbeast'/'ALA', '\"It is sacrilege to confine a gargantuan to the grinding of straight lines. I will pray that it remembers who is the master of this land.\"\n—Syeena, elvish godtoucher').
card_multiverse_id('yoked plowbeast'/'ALA', '175078').
