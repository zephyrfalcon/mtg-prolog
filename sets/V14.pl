% From the Vault: Annihilation (2014)

set('V14').
set_name('V14', 'From the Vault: Annihilation (2014)').
set_release_date('V14', '2014-08-22').
set_border('V14', 'black').
set_type('V14', 'from the vault').

card_in_set('armageddon', 'V14').
card_original_type('armageddon'/'V14', 'Sorcery').
card_original_text('armageddon'/'V14', 'Destroy all lands.').
card_image_name('armageddon'/'V14', 'armageddon').
card_uid('armageddon'/'V14', 'V14:Armageddon:armageddon').
card_rarity('armageddon'/'V14', 'Mythic Rare').
card_artist('armageddon'/'V14', 'Chris Rahn').
card_number('armageddon'/'V14', '1').
card_flavor_text('armageddon'/'V14', 'A bloody dawn broke over a scabbed and tortured world.').
card_multiverse_id('armageddon'/'V14', '386284').

card_in_set('burning of xinye', 'V14').
card_original_type('burning of xinye'/'V14', 'Sorcery').
card_original_text('burning of xinye'/'V14', 'You destroy four lands you control, then target opponent destroys four lands he or she controls. Then Burning of Xinye deals 4 damage to each creature.').
card_image_name('burning of xinye'/'V14', 'burning of xinye').
card_uid('burning of xinye'/'V14', 'V14:Burning of Xinye:burning of xinye').
card_rarity('burning of xinye'/'V14', 'Mythic Rare').
card_artist('burning of xinye'/'V14', 'Yang Hong').
card_number('burning of xinye'/'V14', '2').
card_multiverse_id('burning of xinye'/'V14', '386285').

card_in_set('cataclysm', 'V14').
card_original_type('cataclysm'/'V14', 'Sorcery').
card_original_text('cataclysm'/'V14', 'Each player chooses from among the permanents he or she controls an artifact, a creature, an enchantment, and a land, then sacrifices the rest.').
card_image_name('cataclysm'/'V14', 'cataclysm').
card_uid('cataclysm'/'V14', 'V14:Cataclysm:cataclysm').
card_rarity('cataclysm'/'V14', 'Mythic Rare').
card_artist('cataclysm'/'V14', 'Eric Deschamps').
card_number('cataclysm'/'V14', '3').
card_flavor_text('cataclysm'/'V14', '\"Keep with you only this much: your mount, your blade, and your wits. The rest you can find, build, or take.\"\n—Horse clans\' wisdom').
card_multiverse_id('cataclysm'/'V14', '386286').

card_in_set('child of alara', 'V14').
card_original_type('child of alara'/'V14', 'Legendary Creature — Avatar').
card_original_text('child of alara'/'V14', 'Trample\nWhen Child of Alara dies, destroy all nonland permanents. They can\'t be regenerated.').
card_image_name('child of alara'/'V14', 'child of alara').
card_uid('child of alara'/'V14', 'V14:Child of Alara:child of alara').
card_rarity('child of alara'/'V14', 'Mythic Rare').
card_artist('child of alara'/'V14', 'Steve Argyle').
card_number('child of alara'/'V14', '4').
card_flavor_text('child of alara'/'V14', 'The progeny of the Maelstrom shows no allegiance—and no mercy—to any of the five shards.').
card_multiverse_id('child of alara'/'V14', '386287').

card_in_set('decree of annihilation', 'V14').
card_original_type('decree of annihilation'/'V14', 'Sorcery').
card_original_text('decree of annihilation'/'V14', 'Exile all artifacts, creatures, and lands from the battlefield, all cards from all graveyards, and all cards from all hands.\nCycling {5}{R}{R} ({5}{R}{R}, Discard this card: Draw a card.)\nWhen you cycle Decree of Annihilation, destroy all lands.').
card_image_name('decree of annihilation'/'V14', 'decree of annihilation').
card_uid('decree of annihilation'/'V14', 'V14:Decree of Annihilation:decree of annihilation').
card_rarity('decree of annihilation'/'V14', 'Mythic Rare').
card_artist('decree of annihilation'/'V14', 'John Avon').
card_number('decree of annihilation'/'V14', '5').
card_multiverse_id('decree of annihilation'/'V14', '386288').

card_in_set('firespout', 'V14').
card_original_type('firespout'/'V14', 'Sorcery').
card_original_text('firespout'/'V14', 'Firespout deals 3 damage to each creature without flying if {R} was spent to cast Firespout and 3 damage to each creature with flying if {G} was spent to cast it. (Do both if {R}{G} was spent.)').
card_image_name('firespout'/'V14', 'firespout').
card_uid('firespout'/'V14', 'V14:Firespout:firespout').
card_rarity('firespout'/'V14', 'Mythic Rare').
card_artist('firespout'/'V14', 'Raymond Swanland').
card_number('firespout'/'V14', '6').
card_multiverse_id('firespout'/'V14', '386289').

card_in_set('fracturing gust', 'V14').
card_original_type('fracturing gust'/'V14', 'Instant').
card_original_text('fracturing gust'/'V14', 'Destroy all artifacts and enchantments. You gain 2 life for each permanent destroyed this way.').
card_image_name('fracturing gust'/'V14', 'fracturing gust').
card_uid('fracturing gust'/'V14', 'V14:Fracturing Gust:fracturing gust').
card_rarity('fracturing gust'/'V14', 'Mythic Rare').
card_artist('fracturing gust'/'V14', 'Michael Sutfin').
card_number('fracturing gust'/'V14', '7').
card_flavor_text('fracturing gust'/'V14', 'Elvish dawnhands test a relic\'s worthiness before collecting it for the safehold. If it can\'t stand up to a stiff breeze, it\'s left behind.').
card_multiverse_id('fracturing gust'/'V14', '386290').

card_in_set('living death', 'V14').
card_original_type('living death'/'V14', 'Sorcery').
card_original_text('living death'/'V14', 'Each player exiles all creature cards from his or her graveyard, then sacrifices all creatures he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_image_name('living death'/'V14', 'living death').
card_uid('living death'/'V14', 'V14:Living Death:living death').
card_rarity('living death'/'V14', 'Mythic Rare').
card_artist('living death'/'V14', 'Mark Winters').
card_number('living death'/'V14', '8').
card_multiverse_id('living death'/'V14', '386291').

card_in_set('martial coup', 'V14').
card_original_type('martial coup'/'V14', 'Sorcery').
card_original_text('martial coup'/'V14', 'Put X 1/1 white Soldier creature tokens onto the battlefield. If X is 5 or more, destroy all other creatures.').
card_image_name('martial coup'/'V14', 'martial coup').
card_uid('martial coup'/'V14', 'V14:Martial Coup:martial coup').
card_rarity('martial coup'/'V14', 'Mythic Rare').
card_artist('martial coup'/'V14', 'Greg Staples').
card_number('martial coup'/'V14', '9').
card_flavor_text('martial coup'/'V14', 'Their war forgotten, the nations of Bant stood united in the face of a common threat.').
card_multiverse_id('martial coup'/'V14', '386292').

card_in_set('rolling earthquake', 'V14').
card_original_type('rolling earthquake'/'V14', 'Sorcery').
card_original_text('rolling earthquake'/'V14', 'Rolling Earthquake deals X damage to each creature without horsemanship and each player.').
card_image_name('rolling earthquake'/'V14', 'rolling earthquake').
card_uid('rolling earthquake'/'V14', 'V14:Rolling Earthquake:rolling earthquake').
card_rarity('rolling earthquake'/'V14', 'Mythic Rare').
card_artist('rolling earthquake'/'V14', 'Yang Hong').
card_number('rolling earthquake'/'V14', '10').
card_multiverse_id('rolling earthquake'/'V14', '386293').

card_in_set('smokestack', 'V14').
card_original_type('smokestack'/'V14', 'Artifact').
card_original_text('smokestack'/'V14', 'At the beginning of your upkeep, you may put a soot counter on Smokestack.\nAt the beginning of each player\'s upkeep, that player sacrifices a permanent for each soot counter on Smokestack.').
card_image_name('smokestack'/'V14', 'smokestack').
card_uid('smokestack'/'V14', 'V14:Smokestack:smokestack').
card_rarity('smokestack'/'V14', 'Mythic Rare').
card_artist('smokestack'/'V14', 'Daniel Ljunggren').
card_number('smokestack'/'V14', '11').
card_multiverse_id('smokestack'/'V14', '386294').

card_in_set('terminus', 'V14').
card_original_type('terminus'/'V14', 'Sorcery').
card_original_text('terminus'/'V14', 'Put all creatures on the bottom of their owners\' libraries.\nMiracle {W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_image_name('terminus'/'V14', 'terminus').
card_uid('terminus'/'V14', 'V14:Terminus:terminus').
card_rarity('terminus'/'V14', 'Mythic Rare').
card_artist('terminus'/'V14', 'James Paick').
card_number('terminus'/'V14', '12').
card_multiverse_id('terminus'/'V14', '386295').

card_in_set('upheaval', 'V14').
card_original_type('upheaval'/'V14', 'Sorcery').
card_original_text('upheaval'/'V14', 'Return all permanents to their owners\' hands.').
card_image_name('upheaval'/'V14', 'upheaval').
card_uid('upheaval'/'V14', 'V14:Upheaval:upheaval').
card_rarity('upheaval'/'V14', 'Mythic Rare').
card_artist('upheaval'/'V14', 'Kev Walker').
card_number('upheaval'/'V14', '13').
card_flavor_text('upheaval'/'V14', 'The calm comes after the storm.').
card_multiverse_id('upheaval'/'V14', '386296').

card_in_set('virtue\'s ruin', 'V14').
card_original_type('virtue\'s ruin'/'V14', 'Sorcery').
card_original_text('virtue\'s ruin'/'V14', 'Destroy all white creatures.').
card_image_name('virtue\'s ruin'/'V14', 'virtue\'s ruin').
card_uid('virtue\'s ruin'/'V14', 'V14:Virtue\'s Ruin:virtue\'s ruin').
card_rarity('virtue\'s ruin'/'V14', 'Mythic Rare').
card_artist('virtue\'s ruin'/'V14', 'Mike Dringenberg').
card_number('virtue\'s ruin'/'V14', '14').
card_flavor_text('virtue\'s ruin'/'V14', 'All must fall, and those who stand highest fall hardest.').
card_multiverse_id('virtue\'s ruin'/'V14', '386297').

card_in_set('wrath of god', 'V14').
card_original_type('wrath of god'/'V14', 'Sorcery').
card_original_text('wrath of god'/'V14', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'V14', 'wrath of god').
card_uid('wrath of god'/'V14', 'V14:Wrath of God:wrath of god').
card_rarity('wrath of god'/'V14', 'Mythic Rare').
card_artist('wrath of god'/'V14', 'Willian Murai').
card_number('wrath of god'/'V14', '15').
card_flavor_text('wrath of god'/'V14', 'Legend speaks of the lost coastal polis of Olantin, whose inhabitants\' hubris enraged the sun god Heliod.').
card_multiverse_id('wrath of god'/'V14', '386298').
