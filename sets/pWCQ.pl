% World Magic Cup Qualifiers

set('pWCQ').
set_name('pWCQ', 'World Magic Cup Qualifiers').
set_release_date('pWCQ', '2013-04-06').
set_border('pWCQ', 'black').
set_type('pWCQ', 'promo').

card_in_set('geist of saint traft', 'pWCQ').
card_original_type('geist of saint traft'/'pWCQ', 'Legendary Creature — Spirit Cleric').
card_original_text('geist of saint traft'/'pWCQ', '').
card_image_name('geist of saint traft'/'pWCQ', 'geist of saint traft').
card_uid('geist of saint traft'/'pWCQ', 'pWCQ:Geist of Saint Traft:geist of saint traft').
card_rarity('geist of saint traft'/'pWCQ', 'Special').
card_artist('geist of saint traft'/'pWCQ', 'Izzy').
card_number('geist of saint traft'/'pWCQ', '2').

card_in_set('vengevine', 'pWCQ').
card_original_type('vengevine'/'pWCQ', 'Creature — Elemental').
card_original_text('vengevine'/'pWCQ', '').
card_image_name('vengevine'/'pWCQ', 'vengevine').
card_uid('vengevine'/'pWCQ', 'pWCQ:Vengevine:vengevine').
card_rarity('vengevine'/'pWCQ', 'Special').
card_artist('vengevine'/'pWCQ', 'Trevor Claxton').
card_number('vengevine'/'pWCQ', '1').
card_flavor_text('vengevine'/'pWCQ', 'Leave but a shred of root and it will return, bursting with vigor.').
