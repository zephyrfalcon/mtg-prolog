% Commander 2014

set('C14').
set_name('C14', 'Commander 2014').
set_release_date('C14', '2014-11-07').
set_border('C14', 'black').
set_type('C14', 'commander').

card_in_set('abyssal persecutor', 'C14').
card_original_type('abyssal persecutor'/'C14', 'Creature — Demon').
card_original_text('abyssal persecutor'/'C14', 'Flying, trample\nYou can\'t win the game and your opponents can\'t lose the game.').
card_image_name('abyssal persecutor'/'C14', 'abyssal persecutor').
card_uid('abyssal persecutor'/'C14', 'C14:Abyssal Persecutor:abyssal persecutor').
card_rarity('abyssal persecutor'/'C14', 'Mythic Rare').
card_artist('abyssal persecutor'/'C14', 'Chippy').
card_number('abyssal persecutor'/'C14', '132').
card_flavor_text('abyssal persecutor'/'C14', 'His slaves crave death more than they desire freedom. He denies them both.').
card_multiverse_id('abyssal persecutor'/'C14', '389422').

card_in_set('adarkar valkyrie', 'C14').
card_original_type('adarkar valkyrie'/'C14', 'Snow Creature — Angel').
card_original_text('adarkar valkyrie'/'C14', 'Flying, vigilance\n{T}: When target creature other than Adarkar Valkyrie dies this turn, return that card to the battlefield under your control.').
card_image_name('adarkar valkyrie'/'C14', 'adarkar valkyrie').
card_uid('adarkar valkyrie'/'C14', 'C14:Adarkar Valkyrie:adarkar valkyrie').
card_rarity('adarkar valkyrie'/'C14', 'Rare').
card_artist('adarkar valkyrie'/'C14', 'Jeremy Jarvis').
card_number('adarkar valkyrie'/'C14', '63').
card_flavor_text('adarkar valkyrie'/'C14', 'She doesn\'t escort the dead to the afterlife, but instead raises them to fight and die again.').
card_multiverse_id('adarkar valkyrie'/'C14', '389423').

card_in_set('æther gale', 'C14').
card_original_type('æther gale'/'C14', 'Sorcery').
card_original_text('æther gale'/'C14', 'Return six target nonland permanents to their owners\' hands.').
card_first_print('æther gale', 'C14').
card_image_name('æther gale'/'C14', 'aether gale').
card_uid('æther gale'/'C14', 'C14:Æther Gale:aether gale').
card_rarity('æther gale'/'C14', 'Rare').
card_artist('æther gale'/'C14', 'Jack Wang').
card_number('æther gale'/'C14', '11').
card_flavor_text('æther gale'/'C14', 'The militia formed up in the central plaza awaiting the onslaught, but the giants never quite made it.').
card_multiverse_id('æther gale'/'C14', '389424').

card_in_set('æther snap', 'C14').
card_original_type('æther snap'/'C14', 'Sorcery').
card_original_text('æther snap'/'C14', 'Remove all counters from all permanents and exile all tokens.').
card_image_name('æther snap'/'C14', 'aether snap').
card_uid('æther snap'/'C14', 'C14:Æther Snap:aether snap').
card_rarity('æther snap'/'C14', 'Rare').
card_artist('æther snap'/'C14', 'Kev Walker').
card_number('æther snap'/'C14', '133').
card_flavor_text('æther snap'/'C14', '\"May you wake to find you were only ever a dream.\"\n—Mephidross curse').
card_multiverse_id('æther snap'/'C14', '389425').

card_in_set('afterlife', 'C14').
card_original_type('afterlife'/'C14', 'Instant').
card_original_text('afterlife'/'C14', 'Destroy target creature. It can\'t be regenerated. Its controller puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('afterlife'/'C14', 'afterlife').
card_uid('afterlife'/'C14', 'C14:Afterlife:afterlife').
card_rarity('afterlife'/'C14', 'Uncommon').
card_artist('afterlife'/'C14', 'Brian Snõddy').
card_number('afterlife'/'C14', '64').
card_multiverse_id('afterlife'/'C14', '389426').

card_in_set('angel of the dire hour', 'C14').
card_original_type('angel of the dire hour'/'C14', 'Creature — Angel').
card_original_text('angel of the dire hour'/'C14', 'Flash\nFlying\nWhen Angel of the Dire Hour enters the battlefield, if you cast it from your hand, exile all attacking creatures.').
card_first_print('angel of the dire hour', 'C14').
card_image_name('angel of the dire hour'/'C14', 'angel of the dire hour').
card_uid('angel of the dire hour'/'C14', 'C14:Angel of the Dire Hour:angel of the dire hour').
card_rarity('angel of the dire hour'/'C14', 'Rare').
card_artist('angel of the dire hour'/'C14', 'Jack Wang').
card_number('angel of the dire hour'/'C14', '1').
card_flavor_text('angel of the dire hour'/'C14', 'She arrived like a meteorite, and the enemy fell in a shock wave of holy vengeance.').
card_multiverse_id('angel of the dire hour'/'C14', '389427').

card_in_set('angelic field marshal', 'C14').
card_original_type('angelic field marshal'/'C14', 'Creature — Angel').
card_original_text('angelic field marshal'/'C14', 'Flying\nLieutenant — As long as you control your commander, Angelic Field Marshal gets +2/+2 and creatures you control have vigilance.').
card_first_print('angelic field marshal', 'C14').
card_image_name('angelic field marshal'/'C14', 'angelic field marshal').
card_uid('angelic field marshal'/'C14', 'C14:Angelic Field Marshal:angelic field marshal').
card_rarity('angelic field marshal'/'C14', 'Rare').
card_artist('angelic field marshal'/'C14', 'Scott Murphy').
card_number('angelic field marshal'/'C14', '2').
card_flavor_text('angelic field marshal'/'C14', 'Her presence on the battlefield can transform an unruly mob into a disciplined army.').
card_multiverse_id('angelic field marshal'/'C14', '389428').

card_in_set('annihilate', 'C14').
card_original_type('annihilate'/'C14', 'Instant').
card_original_text('annihilate'/'C14', 'Destroy target nonblack creature. It can\'t be regenerated.\nDraw a card.').
card_image_name('annihilate'/'C14', 'annihilate').
card_uid('annihilate'/'C14', 'C14:Annihilate:annihilate').
card_rarity('annihilate'/'C14', 'Uncommon').
card_artist('annihilate'/'C14', 'Kev Walker').
card_number('annihilate'/'C14', '134').
card_flavor_text('annihilate'/'C14', '\"Whatever Yawgmoth marks, dies. There is no escape.\"\n—Tsabo Tavoc, Phyrexian general').
card_multiverse_id('annihilate'/'C14', '389429').

card_in_set('arcane lighthouse', 'C14').
card_original_type('arcane lighthouse'/'C14', 'Land').
card_original_text('arcane lighthouse'/'C14', '{T}: Add {1} to your mana pool.\n{1}, {T}: Until end of turn, creatures your opponents control lose hexproof and shroud and can\'t have hexproof or shroud.').
card_first_print('arcane lighthouse', 'C14').
card_image_name('arcane lighthouse'/'C14', 'arcane lighthouse').
card_uid('arcane lighthouse'/'C14', 'C14:Arcane Lighthouse:arcane lighthouse').
card_rarity('arcane lighthouse'/'C14', 'Uncommon').
card_artist('arcane lighthouse'/'C14', 'Igor Kieryluk').
card_number('arcane lighthouse'/'C14', '59').
card_multiverse_id('arcane lighthouse'/'C14', '389430').

card_in_set('argentum armor', 'C14').
card_original_type('argentum armor'/'C14', 'Artifact — Equipment').
card_original_text('argentum armor'/'C14', 'Equipped creature gets +6/+6.\nWhenever equipped creature attacks, destroy target permanent.\nEquip {6}').
card_image_name('argentum armor'/'C14', 'argentum armor').
card_uid('argentum armor'/'C14', 'C14:Argentum Armor:argentum armor').
card_rarity('argentum armor'/'C14', 'Rare').
card_artist('argentum armor'/'C14', 'Matt Cavotta').
card_number('argentum armor'/'C14', '228').
card_flavor_text('argentum armor'/'C14', 'Mirrodin\'s creator still lives, still shapes metal, and still commands world-shaking power.').
card_multiverse_id('argentum armor'/'C14', '389431').

card_in_set('armistice', 'C14').
card_original_type('armistice'/'C14', 'Enchantment').
card_original_text('armistice'/'C14', '{3}{W}{W}: You draw a card and target opponent gains 3 life.').
card_image_name('armistice'/'C14', 'armistice').
card_uid('armistice'/'C14', 'C14:Armistice:armistice').
card_rarity('armistice'/'C14', 'Rare').
card_artist('armistice'/'C14', 'Dan Frazier').
card_number('armistice'/'C14', '65').
card_flavor_text('armistice'/'C14', 'We all must profit for peace to last.\n—Mercadian saying').
card_multiverse_id('armistice'/'C14', '389432').

card_in_set('artisan of kozilek', 'C14').
card_original_type('artisan of kozilek'/'C14', 'Creature — Eldrazi').
card_original_text('artisan of kozilek'/'C14', 'When you cast Artisan of Kozilek, you may return target creature card from your graveyard to the battlefield.\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)').
card_image_name('artisan of kozilek'/'C14', 'artisan of kozilek').
card_uid('artisan of kozilek'/'C14', 'C14:Artisan of Kozilek:artisan of kozilek').
card_rarity('artisan of kozilek'/'C14', 'Uncommon').
card_artist('artisan of kozilek'/'C14', 'Jason Felix').
card_number('artisan of kozilek'/'C14', '62').
card_multiverse_id('artisan of kozilek'/'C14', '389433').

card_in_set('assault suit', 'C14').
card_original_type('assault suit'/'C14', 'Artifact — Equipment').
card_original_text('assault suit'/'C14', 'Equipped creature gets +2/+2, has haste, can\'t attack you or a planeswalker you control, and can\'t be sacrificed.\nAt the beginning of each opponent\'s upkeep, you may have that player gain control of equipped creature until end of turn. If you do, untap it.\nEquip {3}').
card_first_print('assault suit', 'C14').
card_image_name('assault suit'/'C14', 'assault suit').
card_uid('assault suit'/'C14', 'C14:Assault Suit:assault suit').
card_rarity('assault suit'/'C14', 'Uncommon').
card_artist('assault suit'/'C14', 'James Paick').
card_number('assault suit'/'C14', '53').
card_multiverse_id('assault suit'/'C14', '389434').

card_in_set('azure mage', 'C14').
card_original_type('azure mage'/'C14', 'Creature — Human Wizard').
card_original_text('azure mage'/'C14', '{3}{U}: Draw a card.').
card_image_name('azure mage'/'C14', 'azure mage').
card_uid('azure mage'/'C14', 'C14:Azure Mage:azure mage').
card_rarity('azure mage'/'C14', 'Uncommon').
card_artist('azure mage'/'C14', 'Izzy').
card_number('azure mage'/'C14', '98').
card_flavor_text('azure mage'/'C14', '\"We draw our power from the infinite ocean of the mind, where all manner of things can be conceived.\"\n—Azure creed').
card_multiverse_id('azure mage'/'C14', '389435').

card_in_set('bad moon', 'C14').
card_original_type('bad moon'/'C14', 'Enchantment').
card_original_text('bad moon'/'C14', 'Black creatures get +1/+1.').
card_image_name('bad moon'/'C14', 'bad moon').
card_uid('bad moon'/'C14', 'C14:Bad Moon:bad moon').
card_rarity('bad moon'/'C14', 'Rare').
card_artist('bad moon'/'C14', 'Gary Leach').
card_number('bad moon'/'C14', '135').
card_multiverse_id('bad moon'/'C14', '389436').

card_in_set('barren moor', 'C14').
card_original_type('barren moor'/'C14', 'Land').
card_original_text('barren moor'/'C14', 'Barren Moor enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'C14', 'barren moor').
card_uid('barren moor'/'C14', 'C14:Barren Moor:barren moor').
card_rarity('barren moor'/'C14', 'Common').
card_artist('barren moor'/'C14', 'Heather Hudson').
card_number('barren moor'/'C14', '284').
card_multiverse_id('barren moor'/'C14', '389437').

card_in_set('beastmaster ascension', 'C14').
card_original_type('beastmaster ascension'/'C14', 'Enchantment').
card_original_text('beastmaster ascension'/'C14', 'Whenever a creature you control attacks, you may put a quest counter on Beastmaster Ascension.\nAs long as Beastmaster Ascension has seven or more quest counters on it, creatures you control get +5/+5.').
card_image_name('beastmaster ascension'/'C14', 'beastmaster ascension').
card_uid('beastmaster ascension'/'C14', 'C14:Beastmaster Ascension:beastmaster ascension').
card_rarity('beastmaster ascension'/'C14', 'Rare').
card_artist('beastmaster ascension'/'C14', 'Alex Horley-Orlandelli').
card_number('beastmaster ascension'/'C14', '186').
card_multiverse_id('beastmaster ascension'/'C14', '389438').

card_in_set('beetleback chief', 'C14').
card_original_type('beetleback chief'/'C14', 'Creature — Goblin Warrior').
card_original_text('beetleback chief'/'C14', 'When Beetleback Chief enters the battlefield, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('beetleback chief'/'C14', 'beetleback chief').
card_uid('beetleback chief'/'C14', 'C14:Beetleback Chief:beetleback chief').
card_rarity('beetleback chief'/'C14', 'Uncommon').
card_artist('beetleback chief'/'C14', 'Wayne England').
card_number('beetleback chief'/'C14', '171').
card_flavor_text('beetleback chief'/'C14', 'Whether trained, ridden, or eaten, few goblin military innovations have rivaled the bug.').
card_multiverse_id('beetleback chief'/'C14', '389439').

card_in_set('benevolent offering', 'C14').
card_original_type('benevolent offering'/'C14', 'Instant').
card_original_text('benevolent offering'/'C14', 'Choose an opponent. You and that player each put three 1/1 white Spirit creature tokens with flying onto the battlefield.\nChoose an opponent. You gain 2 life for each creature you control and that player gains 2 life for each creature he or she controls.').
card_first_print('benevolent offering', 'C14').
card_image_name('benevolent offering'/'C14', 'benevolent offering').
card_uid('benevolent offering'/'C14', 'C14:Benevolent Offering:benevolent offering').
card_rarity('benevolent offering'/'C14', 'Rare').
card_artist('benevolent offering'/'C14', 'Ryan Yee').
card_number('benevolent offering'/'C14', '3').
card_multiverse_id('benevolent offering'/'C14', '389440').

card_in_set('bitter feud', 'C14').
card_original_type('bitter feud'/'C14', 'Enchantment').
card_original_text('bitter feud'/'C14', 'As Bitter Feud enters the battlefield, choose two players.\nIf a source controlled by one of the chosen players would deal damage to the other chosen player or a permanent that player controls, that source deals double that damage to that player or permanent instead.').
card_first_print('bitter feud', 'C14').
card_image_name('bitter feud'/'C14', 'bitter feud').
card_uid('bitter feud'/'C14', 'C14:Bitter Feud:bitter feud').
card_rarity('bitter feud'/'C14', 'Rare').
card_artist('bitter feud'/'C14', 'Aaron Miller').
card_number('bitter feud'/'C14', '32').
card_multiverse_id('bitter feud'/'C14', '389441').

card_in_set('black sun\'s zenith', 'C14').
card_original_type('black sun\'s zenith'/'C14', 'Sorcery').
card_original_text('black sun\'s zenith'/'C14', 'Put X -1/-1 counters on each creature. Shuffle Black Sun\'s Zenith into its owner\'s library.').
card_image_name('black sun\'s zenith'/'C14', 'black sun\'s zenith').
card_uid('black sun\'s zenith'/'C14', 'C14:Black Sun\'s Zenith:black sun\'s zenith').
card_rarity('black sun\'s zenith'/'C14', 'Rare').
card_artist('black sun\'s zenith'/'C14', 'Daniel Ljunggren').
card_number('black sun\'s zenith'/'C14', '136').
card_flavor_text('black sun\'s zenith'/'C14', '\"Under the suns, Mirrodin kneels and begs us for perfection.\"\n—Geth, Lord of the Vault').
card_multiverse_id('black sun\'s zenith'/'C14', '389442').

card_in_set('blasphemous act', 'C14').
card_original_type('blasphemous act'/'C14', 'Sorcery').
card_original_text('blasphemous act'/'C14', 'Blasphemous Act costs {1} less to cast for each creature on the battlefield.\nBlasphemous Act deals 13 damage to each creature.').
card_image_name('blasphemous act'/'C14', 'blasphemous act').
card_uid('blasphemous act'/'C14', 'C14:Blasphemous Act:blasphemous act').
card_rarity('blasphemous act'/'C14', 'Rare').
card_artist('blasphemous act'/'C14', 'Daarken').
card_number('blasphemous act'/'C14', '172').
card_flavor_text('blasphemous act'/'C14', '\"Holy places are no longer sanctuary from death, and death is no longer sanctuary from anything.\"\n—Thalia, Knight-Cathar').
card_multiverse_id('blasphemous act'/'C14', '389443').

card_in_set('bloodgift demon', 'C14').
card_original_type('bloodgift demon'/'C14', 'Creature — Demon').
card_original_text('bloodgift demon'/'C14', 'Flying\nAt the beginning of your upkeep, target player draws a card and loses 1 life.').
card_image_name('bloodgift demon'/'C14', 'bloodgift demon').
card_uid('bloodgift demon'/'C14', 'C14:Bloodgift Demon:bloodgift demon').
card_rarity('bloodgift demon'/'C14', 'Rare').
card_artist('bloodgift demon'/'C14', 'Peter Mohrbacher').
card_number('bloodgift demon'/'C14', '137').
card_flavor_text('bloodgift demon'/'C14', 'He relishes the devotion of his Skirsdag puppets and their belief that it will earn them immortality.').
card_multiverse_id('bloodgift demon'/'C14', '389444').

card_in_set('bogardan hellkite', 'C14').
card_original_type('bogardan hellkite'/'C14', 'Creature — Dragon').
card_original_text('bogardan hellkite'/'C14', 'Flash\nFlying\nWhen Bogardan Hellkite enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or players.').
card_image_name('bogardan hellkite'/'C14', 'bogardan hellkite').
card_uid('bogardan hellkite'/'C14', 'C14:Bogardan Hellkite:bogardan hellkite').
card_rarity('bogardan hellkite'/'C14', 'Mythic Rare').
card_artist('bogardan hellkite'/'C14', 'Scott M. Fischer').
card_number('bogardan hellkite'/'C14', '173').
card_multiverse_id('bogardan hellkite'/'C14', '389445').

card_in_set('bojuka bog', 'C14').
card_original_type('bojuka bog'/'C14', 'Land').
card_original_text('bojuka bog'/'C14', 'Bojuka Bog enters the battlefield tapped.\nWhen Bojuka Bog enters the battlefield, exile all cards from target player\'s graveyard.\n{T}: Add {B} to your mana pool.').
card_image_name('bojuka bog'/'C14', 'bojuka bog').
card_uid('bojuka bog'/'C14', 'C14:Bojuka Bog:bojuka bog').
card_rarity('bojuka bog'/'C14', 'Common').
card_artist('bojuka bog'/'C14', 'Howard Lyon').
card_number('bojuka bog'/'C14', '285').
card_multiverse_id('bojuka bog'/'C14', '389446').

card_in_set('bonehoard', 'C14').
card_original_type('bonehoard'/'C14', 'Artifact — Equipment').
card_original_text('bonehoard'/'C14', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +X/+X, where X is the number of creature cards in all graveyards.\nEquip {2}').
card_image_name('bonehoard'/'C14', 'bonehoard').
card_uid('bonehoard'/'C14', 'C14:Bonehoard:bonehoard').
card_rarity('bonehoard'/'C14', 'Rare').
card_artist('bonehoard'/'C14', 'Chippy').
card_number('bonehoard'/'C14', '229').
card_multiverse_id('bonehoard'/'C14', '389447').

card_in_set('bosh, iron golem', 'C14').
card_original_type('bosh, iron golem'/'C14', 'Legendary Artifact Creature — Golem').
card_original_text('bosh, iron golem'/'C14', 'Trample\n{3}{R}, Sacrifice an artifact: Bosh, Iron Golem deals damage equal to the sacrificed artifact\'s converted mana cost to target creature or player.').
card_image_name('bosh, iron golem'/'C14', 'bosh, iron golem').
card_uid('bosh, iron golem'/'C14', 'C14:Bosh, Iron Golem:bosh, iron golem').
card_rarity('bosh, iron golem'/'C14', 'Rare').
card_artist('bosh, iron golem'/'C14', 'Brom').
card_number('bosh, iron golem'/'C14', '230').
card_flavor_text('bosh, iron golem'/'C14', 'As Glissa searches for the truth about Memnarch, Bosh searches to unearth the secrets of his past.').
card_multiverse_id('bosh, iron golem'/'C14', '389448').

card_in_set('bottle gnomes', 'C14').
card_original_type('bottle gnomes'/'C14', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'C14', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_image_name('bottle gnomes'/'C14', 'bottle gnomes').
card_uid('bottle gnomes'/'C14', 'C14:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'C14', 'Uncommon').
card_artist('bottle gnomes'/'C14', 'Ben Thompson').
card_number('bottle gnomes'/'C14', '231').
card_flavor_text('bottle gnomes'/'C14', 'Reinforcements . . . or refreshments?').
card_multiverse_id('bottle gnomes'/'C14', '389449').

card_in_set('brave the elements', 'C14').
card_original_type('brave the elements'/'C14', 'Instant').
card_original_text('brave the elements'/'C14', 'Choose a color. White creatures you control gain protection from the chosen color until end of turn.').
card_image_name('brave the elements'/'C14', 'brave the elements').
card_uid('brave the elements'/'C14', 'C14:Brave the Elements:brave the elements').
card_rarity('brave the elements'/'C14', 'Uncommon').
card_artist('brave the elements'/'C14', 'Goran Josic').
card_number('brave the elements'/'C14', '66').
card_flavor_text('brave the elements'/'C14', '\"Trust me, your lost fingers and toes are nothing compared to the lost treasures of Sejiri.\"\n—Zahr Gada, Halimar expedition leader').
card_multiverse_id('brave the elements'/'C14', '389450').

card_in_set('breaching leviathan', 'C14').
card_original_type('breaching leviathan'/'C14', 'Creature — Leviathan').
card_original_text('breaching leviathan'/'C14', 'When Breaching Leviathan enters the battlefield, if you cast it from your hand, tap all nonblue creatures. Those creatures don\'t untap during their controllers\' next untap steps.').
card_first_print('breaching leviathan', 'C14').
card_image_name('breaching leviathan'/'C14', 'breaching leviathan').
card_uid('breaching leviathan'/'C14', 'C14:Breaching Leviathan:breaching leviathan').
card_rarity('breaching leviathan'/'C14', 'Rare').
card_artist('breaching leviathan'/'C14', 'Tomasz Jedruszek').
card_number('breaching leviathan'/'C14', '12').
card_flavor_text('breaching leviathan'/'C14', 'These waters remain uncharted, but not for lack of trying.').
card_multiverse_id('breaching leviathan'/'C14', '389451').

card_in_set('brine elemental', 'C14').
card_original_type('brine elemental'/'C14', 'Creature — Elemental').
card_original_text('brine elemental'/'C14', 'Morph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Brine Elemental is turned face up, each opponent skips his or her next untap step.').
card_image_name('brine elemental'/'C14', 'brine elemental').
card_uid('brine elemental'/'C14', 'C14:Brine Elemental:brine elemental').
card_rarity('brine elemental'/'C14', 'Uncommon').
card_artist('brine elemental'/'C14', 'Stephen Tappin').
card_number('brine elemental'/'C14', '99').
card_flavor_text('brine elemental'/'C14', 'Water calls to water, and the world is left exhausted and withered in its wake.').
card_multiverse_id('brine elemental'/'C14', '389452').

card_in_set('buried ruin', 'C14').
card_original_type('buried ruin'/'C14', 'Land').
card_original_text('buried ruin'/'C14', '{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Buried Ruin: Return target artifact card from your graveyard to your hand.').
card_image_name('buried ruin'/'C14', 'buried ruin').
card_uid('buried ruin'/'C14', 'C14:Buried Ruin:buried ruin').
card_rarity('buried ruin'/'C14', 'Uncommon').
card_artist('buried ruin'/'C14', 'Franz Vohwinkel').
card_number('buried ruin'/'C14', '286').
card_flavor_text('buried ruin'/'C14', 'History has buried its treasures deep.').
card_multiverse_id('buried ruin'/'C14', '389453').

card_in_set('burnished hart', 'C14').
card_original_type('burnished hart'/'C14', 'Artifact Creature — Elk').
card_original_text('burnished hart'/'C14', '{3}, Sacrifice Burnished Hart: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_image_name('burnished hart'/'C14', 'burnished hart').
card_uid('burnished hart'/'C14', 'C14:Burnished Hart:burnished hart').
card_rarity('burnished hart'/'C14', 'Uncommon').
card_artist('burnished hart'/'C14', 'Yeong-Hao Han').
card_number('burnished hart'/'C14', '232').
card_flavor_text('burnished hart'/'C14', 'Forged by divine hands to wander mortal realms.').
card_multiverse_id('burnished hart'/'C14', '389454').

card_in_set('butcher of malakir', 'C14').
card_original_type('butcher of malakir'/'C14', 'Creature — Vampire Warrior').
card_original_text('butcher of malakir'/'C14', 'Flying\nWhenever Butcher of Malakir or another creature you control dies, each opponent sacrifices a creature.').
card_image_name('butcher of malakir'/'C14', 'butcher of malakir').
card_uid('butcher of malakir'/'C14', 'C14:Butcher of Malakir:butcher of malakir').
card_rarity('butcher of malakir'/'C14', 'Rare').
card_artist('butcher of malakir'/'C14', 'Jason Chan').
card_number('butcher of malakir'/'C14', '138').
card_flavor_text('butcher of malakir'/'C14', 'His verdict is always guilty. His sentence is always death.').
card_multiverse_id('butcher of malakir'/'C14', '389455').

card_in_set('cackling counterpart', 'C14').
card_original_type('cackling counterpart'/'C14', 'Instant').
card_original_text('cackling counterpart'/'C14', 'Put a token onto the battlefield that\'s a copy of target creature you control.\nFlashback {5}{U}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('cackling counterpart'/'C14', 'cackling counterpart').
card_uid('cackling counterpart'/'C14', 'C14:Cackling Counterpart:cackling counterpart').
card_rarity('cackling counterpart'/'C14', 'Rare').
card_artist('cackling counterpart'/'C14', 'David Rapoza').
card_number('cackling counterpart'/'C14', '100').
card_multiverse_id('cackling counterpart'/'C14', '389456').

card_in_set('caged sun', 'C14').
card_original_type('caged sun'/'C14', 'Artifact').
card_original_text('caged sun'/'C14', 'As Caged Sun enters the battlefield, choose a color.\nCreatures you control of the chosen color get +1/+1.\nWhenever a land\'s ability adds one or more mana of the chosen color to your mana pool, add one additional mana of that color to your mana pool.').
card_image_name('caged sun'/'C14', 'caged sun').
card_uid('caged sun'/'C14', 'C14:Caged Sun:caged sun').
card_rarity('caged sun'/'C14', 'Rare').
card_artist('caged sun'/'C14', 'Scott Chou').
card_number('caged sun'/'C14', '233').
card_multiverse_id('caged sun'/'C14', '389457').

card_in_set('call to mind', 'C14').
card_original_type('call to mind'/'C14', 'Sorcery').
card_original_text('call to mind'/'C14', 'Return target instant or sorcery card from your graveyard to your hand.').
card_image_name('call to mind'/'C14', 'call to mind').
card_uid('call to mind'/'C14', 'C14:Call to Mind:call to mind').
card_rarity('call to mind'/'C14', 'Uncommon').
card_artist('call to mind'/'C14', 'Terese Nielsen').
card_number('call to mind'/'C14', '101').
card_flavor_text('call to mind'/'C14', '\"It\'s hard to say which is more satisfying: the search for that missing piece or fitting that piece into place.\"\n—Evo Ragus').
card_multiverse_id('call to mind'/'C14', '389458').

card_in_set('cathars\' crusade', 'C14').
card_original_type('cathars\' crusade'/'C14', 'Enchantment').
card_original_text('cathars\' crusade'/'C14', 'Whenever a creature enters the battlefield under your control, put a +1/+1 counter on each creature you control.').
card_image_name('cathars\' crusade'/'C14', 'cathars\' crusade').
card_uid('cathars\' crusade'/'C14', 'C14:Cathars\' Crusade:cathars\' crusade').
card_rarity('cathars\' crusade'/'C14', 'Rare').
card_artist('cathars\' crusade'/'C14', 'Karl Kopinski').
card_number('cathars\' crusade'/'C14', '67').
card_flavor_text('cathars\' crusade'/'C14', 'Avacyn\'s holy warriors kept hope alive in her darkest hours. Now they will carry that hope across Innistrad.').
card_multiverse_id('cathars\' crusade'/'C14', '389459').

card_in_set('cathodion', 'C14').
card_original_type('cathodion'/'C14', 'Artifact Creature — Construct').
card_original_text('cathodion'/'C14', 'When Cathodion dies, add {3} to your mana pool.').
card_image_name('cathodion'/'C14', 'cathodion').
card_uid('cathodion'/'C14', 'C14:Cathodion:cathodion').
card_rarity('cathodion'/'C14', 'Uncommon').
card_artist('cathodion'/'C14', 'Izzy').
card_number('cathodion'/'C14', '234').
card_flavor_text('cathodion'/'C14', 'Instead of creating a tool that would be damaged by heat, the Thran built one that was charged by it.').
card_multiverse_id('cathodion'/'C14', '389460').

card_in_set('celestial crusader', 'C14').
card_original_type('celestial crusader'/'C14', 'Creature — Spirit').
card_original_text('celestial crusader'/'C14', 'Flash (You may cast this spell any time you could cast an instant.)\nSplit second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nFlying\nOther white creatures get +1/+1.').
card_image_name('celestial crusader'/'C14', 'celestial crusader').
card_uid('celestial crusader'/'C14', 'C14:Celestial Crusader:celestial crusader').
card_rarity('celestial crusader'/'C14', 'Uncommon').
card_artist('celestial crusader'/'C14', 'Jim Murray').
card_number('celestial crusader'/'C14', '68').
card_multiverse_id('celestial crusader'/'C14', '389461').

card_in_set('chaos warp', 'C14').
card_original_type('chaos warp'/'C14', 'Instant').
card_original_text('chaos warp'/'C14', 'The owner of target permanent shuffles it into his or her library, then reveals the top card of his or her library. If it\'s a permanent card, he or she puts it onto the battlefield.').
card_image_name('chaos warp'/'C14', 'chaos warp').
card_uid('chaos warp'/'C14', 'C14:Chaos Warp:chaos warp').
card_rarity('chaos warp'/'C14', 'Rare').
card_artist('chaos warp'/'C14', 'Trevor Claxton').
card_number('chaos warp'/'C14', '174').
card_multiverse_id('chaos warp'/'C14', '389462').

card_in_set('charcoal diamond', 'C14').
card_original_type('charcoal diamond'/'C14', 'Artifact').
card_original_text('charcoal diamond'/'C14', 'Charcoal Diamond enters the battlefield tapped.\n{T}: Add {B} to your mana pool.').
card_image_name('charcoal diamond'/'C14', 'charcoal diamond').
card_uid('charcoal diamond'/'C14', 'C14:Charcoal Diamond:charcoal diamond').
card_rarity('charcoal diamond'/'C14', 'Uncommon').
card_artist('charcoal diamond'/'C14', 'Lindsey Look').
card_number('charcoal diamond'/'C14', '235').
card_multiverse_id('charcoal diamond'/'C14', '389463').

card_in_set('collective unconscious', 'C14').
card_original_type('collective unconscious'/'C14', 'Sorcery').
card_original_text('collective unconscious'/'C14', 'Draw a card for each creature you control.').
card_image_name('collective unconscious'/'C14', 'collective unconscious').
card_uid('collective unconscious'/'C14', 'C14:Collective Unconscious:collective unconscious').
card_rarity('collective unconscious'/'C14', 'Rare').
card_artist('collective unconscious'/'C14', 'Andrew Goldhawk').
card_number('collective unconscious'/'C14', '187').
card_flavor_text('collective unconscious'/'C14', '\"The beasts of the wild are my senses. It is through their eyes that all knowledge shall come to me.\"').
card_multiverse_id('collective unconscious'/'C14', '389464').

card_in_set('comeuppance', 'C14').
card_original_type('comeuppance'/'C14', 'Instant').
card_original_text('comeuppance'/'C14', 'Prevent all damage that would be dealt to you and planeswalkers you control this turn by sources you don\'t control. If damage from a creature source is prevented this way, Comeuppance deals that much damage to that creature. If damage from a noncreature source is prevented this way, Comeuppance deals that much damage to the source\'s controller.').
card_first_print('comeuppance', 'C14').
card_image_name('comeuppance'/'C14', 'comeuppance').
card_uid('comeuppance'/'C14', 'C14:Comeuppance:comeuppance').
card_rarity('comeuppance'/'C14', 'Rare').
card_artist('comeuppance'/'C14', 'Marco Nelor').
card_number('comeuppance'/'C14', '4').
card_multiverse_id('comeuppance'/'C14', '389465').

card_in_set('commander\'s sphere', 'C14').
card_original_type('commander\'s sphere'/'C14', 'Artifact').
card_original_text('commander\'s sphere'/'C14', '{T}: Add to your mana pool one mana of any color in your commander\'s color identity.\nSacrifice Commander\'s Sphere: Draw a card.').
card_first_print('commander\'s sphere', 'C14').
card_image_name('commander\'s sphere'/'C14', 'commander\'s sphere').
card_uid('commander\'s sphere'/'C14', 'C14:Commander\'s Sphere:commander\'s sphere').
card_rarity('commander\'s sphere'/'C14', 'Common').
card_artist('commander\'s sphere'/'C14', 'Ryan Alexander Lee').
card_number('commander\'s sphere'/'C14', '54').
card_flavor_text('commander\'s sphere'/'C14', 'It harmonizes with the essence of its master.').
card_multiverse_id('commander\'s sphere'/'C14', '389466').

card_in_set('compulsive research', 'C14').
card_original_type('compulsive research'/'C14', 'Sorcery').
card_original_text('compulsive research'/'C14', 'Target player draws three cards. Then that player discards two cards unless he or she discards a land card.').
card_image_name('compulsive research'/'C14', 'compulsive research').
card_uid('compulsive research'/'C14', 'C14:Compulsive Research:compulsive research').
card_rarity('compulsive research'/'C14', 'Common').
card_artist('compulsive research'/'C14', 'Michael Sutfin').
card_number('compulsive research'/'C14', '102').
card_flavor_text('compulsive research'/'C14', '\"Four parts molten bronze, yes . . . one part frozen mercury, yes, yes . . . but then what?\"').
card_multiverse_id('compulsive research'/'C14', '389467').

card_in_set('concentrate', 'C14').
card_original_type('concentrate'/'C14', 'Sorcery').
card_original_text('concentrate'/'C14', 'Draw three cards.').
card_image_name('concentrate'/'C14', 'concentrate').
card_uid('concentrate'/'C14', 'C14:Concentrate:concentrate').
card_rarity('concentrate'/'C14', 'Uncommon').
card_artist('concentrate'/'C14', 'rk post').
card_number('concentrate'/'C14', '103').
card_flavor_text('concentrate'/'C14', '\"The treasure in my mind is greater than any worldly glory.\"').
card_multiverse_id('concentrate'/'C14', '389468').

card_in_set('condemn', 'C14').
card_original_type('condemn'/'C14', 'Instant').
card_original_text('condemn'/'C14', 'Put target attacking creature on the bottom of its owner\'s library. Its controller gains life equal to its toughness.').
card_image_name('condemn'/'C14', 'condemn').
card_uid('condemn'/'C14', 'C14:Condemn:condemn').
card_rarity('condemn'/'C14', 'Uncommon').
card_artist('condemn'/'C14', 'Daren Bader').
card_number('condemn'/'C14', '69').
card_flavor_text('condemn'/'C14', '\"No doubt the arbiters would put you away, after all the documents are signed. But I will have justice now.\"\n—Alovnek, Boros guildmage').
card_multiverse_id('condemn'/'C14', '389469').

card_in_set('containment priest', 'C14').
card_original_type('containment priest'/'C14', 'Creature — Human Cleric').
card_original_text('containment priest'/'C14', 'Flash\nIf a nontoken creature would enter the battlefield and it wasn\'t cast, exile it instead.').
card_first_print('containment priest', 'C14').
card_image_name('containment priest'/'C14', 'containment priest').
card_uid('containment priest'/'C14', 'C14:Containment Priest:containment priest').
card_rarity('containment priest'/'C14', 'Rare').
card_artist('containment priest'/'C14', 'John Stanko').
card_number('containment priest'/'C14', '5').
card_flavor_text('containment priest'/'C14', 'Some protection requires a bit of finesse.').
card_multiverse_id('containment priest'/'C14', '389470').

card_in_set('coral atoll', 'C14').
card_original_type('coral atoll'/'C14', 'Land').
card_original_text('coral atoll'/'C14', 'Coral Atoll enters the battlefield tapped.\nWhen Coral Atoll enters the battlefield, sacrifice it unless you return an untapped Island you control to its owner\'s hand.\n{T}: Add {1}{U} to your mana pool.').
card_image_name('coral atoll'/'C14', 'coral atoll').
card_uid('coral atoll'/'C14', 'C14:Coral Atoll:coral atoll').
card_rarity('coral atoll'/'C14', 'Uncommon').
card_artist('coral atoll'/'C14', 'John Avon').
card_number('coral atoll'/'C14', '287').
card_multiverse_id('coral atoll'/'C14', '389471').

card_in_set('creeperhulk', 'C14').
card_original_type('creeperhulk'/'C14', 'Creature — Plant Elemental').
card_original_text('creeperhulk'/'C14', 'Trample\n{1}{G}: Until end of turn, target creature you control has base power and toughness 5/5 and gains trample.').
card_first_print('creeperhulk', 'C14').
card_image_name('creeperhulk'/'C14', 'creeperhulk').
card_uid('creeperhulk'/'C14', 'C14:Creeperhulk:creeperhulk').
card_rarity('creeperhulk'/'C14', 'Rare').
card_artist('creeperhulk'/'C14', 'Ralph Horsley').
card_number('creeperhulk'/'C14', '42').
card_flavor_text('creeperhulk'/'C14', '\"I saw the devastation when the juggernauts crashed the walls of An Karras. Then I got to thinking.\"\n—Janji, sylvan druid').
card_multiverse_id('creeperhulk'/'C14', '389472').

card_in_set('crown of doom', 'C14').
card_original_type('crown of doom'/'C14', 'Artifact').
card_original_text('crown of doom'/'C14', 'Whenever a creature attacks you or a planeswalker you control, it gets +2/+0 until end of turn.\n{2}: Target player other than Crown of Doom\'s owner gains control of it. Activate this ability only during your turn.').
card_first_print('crown of doom', 'C14').
card_image_name('crown of doom'/'C14', 'crown of doom').
card_uid('crown of doom'/'C14', 'C14:Crown of Doom:crown of doom').
card_rarity('crown of doom'/'C14', 'Rare').
card_artist('crown of doom'/'C14', 'Jasper Sandner').
card_number('crown of doom'/'C14', '55').
card_flavor_text('crown of doom'/'C14', 'Every head it rests upon winds up severed.').
card_multiverse_id('crown of doom'/'C14', '389473').

card_in_set('crypt ghast', 'C14').
card_original_type('crypt ghast'/'C14', 'Creature — Spirit').
card_original_text('crypt ghast'/'C14', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nWhenever you tap a Swamp for mana, add {B} to your mana pool (in addition to the mana the land produces).').
card_image_name('crypt ghast'/'C14', 'crypt ghast').
card_uid('crypt ghast'/'C14', 'C14:Crypt Ghast:crypt ghast').
card_rarity('crypt ghast'/'C14', 'Rare').
card_artist('crypt ghast'/'C14', 'Chris Rahn').
card_number('crypt ghast'/'C14', '139').
card_multiverse_id('crypt ghast'/'C14', '389474').

card_in_set('crypt of agadeem', 'C14').
card_original_type('crypt of agadeem'/'C14', 'Land').
card_original_text('crypt of agadeem'/'C14', 'Crypt of Agadeem enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\n{2}, {T}: Add {B} to your mana pool for each black creature card in your graveyard.').
card_image_name('crypt of agadeem'/'C14', 'crypt of agadeem').
card_uid('crypt of agadeem'/'C14', 'C14:Crypt of Agadeem:crypt of agadeem').
card_rarity('crypt of agadeem'/'C14', 'Rare').
card_artist('crypt of agadeem'/'C14', 'Jason Felix').
card_number('crypt of agadeem'/'C14', '288').
card_multiverse_id('crypt of agadeem'/'C14', '389475').

card_in_set('crystal vein', 'C14').
card_original_type('crystal vein'/'C14', 'Land').
card_original_text('crystal vein'/'C14', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Crystal Vein: Add {2} to your mana pool.').
card_image_name('crystal vein'/'C14', 'crystal vein').
card_uid('crystal vein'/'C14', 'C14:Crystal Vein:crystal vein').
card_rarity('crystal vein'/'C14', 'Uncommon').
card_artist('crystal vein'/'C14', 'Pat Lewis').
card_number('crystal vein'/'C14', '289').
card_multiverse_id('crystal vein'/'C14', '389476').

card_in_set('cyclonic rift', 'C14').
card_original_type('cyclonic rift'/'C14', 'Instant').
card_original_text('cyclonic rift'/'C14', 'Return target nonland permanent you don\'t control to its owner\'s hand.\nOverload {6}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_image_name('cyclonic rift'/'C14', 'cyclonic rift').
card_uid('cyclonic rift'/'C14', 'C14:Cyclonic Rift:cyclonic rift').
card_rarity('cyclonic rift'/'C14', 'Rare').
card_artist('cyclonic rift'/'C14', 'Chris Rahn').
card_number('cyclonic rift'/'C14', '104').
card_flavor_text('cyclonic rift'/'C14', 'The Izzet specialize in unnatural disaster.').
card_multiverse_id('cyclonic rift'/'C14', '389477').

card_in_set('daretti, scrap savant', 'C14').
card_original_type('daretti, scrap savant'/'C14', 'Planeswalker — Daretti').
card_original_text('daretti, scrap savant'/'C14', '+2: Discard up to two cards, then draw that many cards.\n−2: Sacrifice an artifact. If you do, return target artifact card from your graveyard to the battlefield.\n−10: You get an emblem with \"Whenever an artifact is put into your graveyard from the battlefield, return that card to the battlefield at the beginning of the next end step.\"\nDaretti, Scrap Savant can be your commander.').
card_first_print('daretti, scrap savant', 'C14').
card_image_name('daretti, scrap savant'/'C14', 'daretti, scrap savant').
card_uid('daretti, scrap savant'/'C14', 'C14:Daretti, Scrap Savant:daretti, scrap savant').
card_rarity('daretti, scrap savant'/'C14', 'Mythic Rare').
card_artist('daretti, scrap savant'/'C14', 'Dan Scott').
card_number('daretti, scrap savant'/'C14', '33').
card_multiverse_id('daretti, scrap savant'/'C14', '389478').

card_in_set('darksteel citadel', 'C14').
card_original_type('darksteel citadel'/'C14', 'Artifact Land').
card_original_text('darksteel citadel'/'C14', 'Indestructible (Effects that say \"destroy\" don\'t destroy this land.)\n{T}: Add {1} to your mana pool.').
card_image_name('darksteel citadel'/'C14', 'darksteel citadel').
card_uid('darksteel citadel'/'C14', 'C14:Darksteel Citadel:darksteel citadel').
card_rarity('darksteel citadel'/'C14', 'Uncommon').
card_artist('darksteel citadel'/'C14', 'John Avon').
card_number('darksteel citadel'/'C14', '290').
card_flavor_text('darksteel citadel'/'C14', 'Structures built from darksteel yield to neither assault nor age.').
card_multiverse_id('darksteel citadel'/'C14', '389479').

card_in_set('decree of justice', 'C14').
card_original_type('decree of justice'/'C14', 'Sorcery').
card_original_text('decree of justice'/'C14', 'Put X 4/4 white Angel creature tokens with flying onto the battlefield.\nCycling {2}{W} ({2}{W}, Discard this card: Draw a card.)\nWhen you cycle Decree of Justice, you may pay {X}. If you do, put X 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('decree of justice'/'C14', 'decree of justice').
card_uid('decree of justice'/'C14', 'C14:Decree of Justice:decree of justice').
card_rarity('decree of justice'/'C14', 'Rare').
card_artist('decree of justice'/'C14', 'Adam Rex').
card_number('decree of justice'/'C14', '70').
card_multiverse_id('decree of justice'/'C14', '389480').

card_in_set('deep-sea kraken', 'C14').
card_original_type('deep-sea kraken'/'C14', 'Creature — Kraken').
card_original_text('deep-sea kraken'/'C14', 'Deep-Sea Kraken can\'t be blocked.\nSuspend 9—{2}{U} (Rather than cast this card from your hand, you may pay {2}{U} and exile it with nine time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever an opponent casts a spell, if Deep-Sea Kraken is suspended, remove a time counter from it.').
card_image_name('deep-sea kraken'/'C14', 'deep-sea kraken').
card_uid('deep-sea kraken'/'C14', 'C14:Deep-Sea Kraken:deep-sea kraken').
card_rarity('deep-sea kraken'/'C14', 'Rare').
card_artist('deep-sea kraken'/'C14', 'Christopher Moeller').
card_number('deep-sea kraken'/'C14', '105').
card_multiverse_id('deep-sea kraken'/'C14', '389481').

card_in_set('demon of wailing agonies', 'C14').
card_original_type('demon of wailing agonies'/'C14', 'Creature — Demon').
card_original_text('demon of wailing agonies'/'C14', 'Flying\nLieutenant — As long as you control your commander, Demon of Wailing Agonies gets +2/+2 and has \"Whenever Demon of Wailing Agonies deals combat damage to a player, that player sacrifices a creature.\"').
card_first_print('demon of wailing agonies', 'C14').
card_image_name('demon of wailing agonies'/'C14', 'demon of wailing agonies').
card_uid('demon of wailing agonies'/'C14', 'C14:Demon of Wailing Agonies:demon of wailing agonies').
card_rarity('demon of wailing agonies'/'C14', 'Rare').
card_artist('demon of wailing agonies'/'C14', 'Dave Kendall').
card_number('demon of wailing agonies'/'C14', '21').
card_multiverse_id('demon of wailing agonies'/'C14', '389482').

card_in_set('deploy to the front', 'C14').
card_original_type('deploy to the front'/'C14', 'Sorcery').
card_original_text('deploy to the front'/'C14', 'Put X 1/1 white Soldier creature tokens onto the battlefield, where X is the number of creatures on the battlefield.').
card_first_print('deploy to the front', 'C14').
card_image_name('deploy to the front'/'C14', 'deploy to the front').
card_uid('deploy to the front'/'C14', 'C14:Deploy to the Front:deploy to the front').
card_rarity('deploy to the front'/'C14', 'Rare').
card_artist('deploy to the front'/'C14', 'Aaron Miller').
card_number('deploy to the front'/'C14', '6').
card_flavor_text('deploy to the front'/'C14', '\"We will descend upon you like a plague of locusts to purge you from our lands!\"\n—Hirov, commander of the Thousand Swords').
card_multiverse_id('deploy to the front'/'C14', '389483').

card_in_set('desert twister', 'C14').
card_original_type('desert twister'/'C14', 'Sorcery').
card_original_text('desert twister'/'C14', 'Destroy target permanent.').
card_image_name('desert twister'/'C14', 'desert twister').
card_uid('desert twister'/'C14', 'C14:Desert Twister:desert twister').
card_rarity('desert twister'/'C14', 'Uncommon').
card_artist('desert twister'/'C14', 'Noah Bradley').
card_number('desert twister'/'C14', '188').
card_flavor_text('desert twister'/'C14', 'Massive, mindless, and imbued with one terrible purpose.').
card_multiverse_id('desert twister'/'C14', '389484').

card_in_set('disciple of bolas', 'C14').
card_original_type('disciple of bolas'/'C14', 'Creature — Human Wizard').
card_original_text('disciple of bolas'/'C14', 'When Disciple of Bolas enters the battlefield, sacrifice another creature. You gain X life and draw X cards, where X is that creature\'s power.').
card_image_name('disciple of bolas'/'C14', 'disciple of bolas').
card_uid('disciple of bolas'/'C14', 'C14:Disciple of Bolas:disciple of bolas').
card_rarity('disciple of bolas'/'C14', 'Rare').
card_artist('disciple of bolas'/'C14', 'Slawomir Maniak').
card_number('disciple of bolas'/'C14', '140').
card_flavor_text('disciple of bolas'/'C14', '\"Lord Bolas has shown me how each miserable little life holds untold resources.\"').
card_multiverse_id('disciple of bolas'/'C14', '389485').

card_in_set('dismiss', 'C14').
card_original_type('dismiss'/'C14', 'Instant').
card_original_text('dismiss'/'C14', 'Counter target spell.\nDraw a card.').
card_image_name('dismiss'/'C14', 'dismiss').
card_uid('dismiss'/'C14', 'C14:Dismiss:dismiss').
card_rarity('dismiss'/'C14', 'Uncommon').
card_artist('dismiss'/'C14', 'Donato Giancola').
card_number('dismiss'/'C14', '106').
card_flavor_text('dismiss'/'C14', '\"There is nothing you can do that I cannot simply deny.\"\n—Ertai, wizard adept').
card_multiverse_id('dismiss'/'C14', '389486').

card_in_set('distorting wake', 'C14').
card_original_type('distorting wake'/'C14', 'Sorcery').
card_original_text('distorting wake'/'C14', 'Return X target nonland permanents to their owners\' hands.').
card_image_name('distorting wake'/'C14', 'distorting wake').
card_uid('distorting wake'/'C14', 'C14:Distorting Wake:distorting wake').
card_rarity('distorting wake'/'C14', 'Rare').
card_artist('distorting wake'/'C14', 'Arnie Swekel').
card_number('distorting wake'/'C14', '107').
card_flavor_text('distorting wake'/'C14', 'Gerrard savored a grim smile as the Phyrexian portals disappeared behind the Weatherlight.').
card_multiverse_id('distorting wake'/'C14', '389487').

card_in_set('domineering will', 'C14').
card_original_type('domineering will'/'C14', 'Instant').
card_original_text('domineering will'/'C14', 'Target player gains control of up to three target nonattacking creatures until end of turn. Untap those creatures. They block this turn if able.').
card_first_print('domineering will', 'C14').
card_image_name('domineering will'/'C14', 'domineering will').
card_uid('domineering will'/'C14', 'C14:Domineering Will:domineering will').
card_rarity('domineering will'/'C14', 'Rare').
card_artist('domineering will'/'C14', 'Mike Sass').
card_number('domineering will'/'C14', '13').
card_multiverse_id('domineering will'/'C14', '389488').

card_in_set('dormant volcano', 'C14').
card_original_type('dormant volcano'/'C14', 'Land').
card_original_text('dormant volcano'/'C14', 'Dormant Volcano enters the battlefield tapped.\nWhen Dormant Volcano enters the battlefield, sacrifice it unless you return an untapped Mountain you control to its owner\'s hand.\n{T}: Add {1}{R} to your mana pool.').
card_image_name('dormant volcano'/'C14', 'dormant volcano').
card_uid('dormant volcano'/'C14', 'C14:Dormant Volcano:dormant volcano').
card_rarity('dormant volcano'/'C14', 'Uncommon').
card_artist('dormant volcano'/'C14', 'John Avon').
card_number('dormant volcano'/'C14', '291').
card_multiverse_id('dormant volcano'/'C14', '389489').

card_in_set('drana, kalastria bloodchief', 'C14').
card_original_type('drana, kalastria bloodchief'/'C14', 'Legendary Creature — Vampire Shaman').
card_original_text('drana, kalastria bloodchief'/'C14', 'Flying\n{X}{B}{B}: Target creature gets -0/-X until end of turn and Drana, Kalastria Bloodchief gets +X/+0 until end of turn.').
card_image_name('drana, kalastria bloodchief'/'C14', 'drana, kalastria bloodchief').
card_uid('drana, kalastria bloodchief'/'C14', 'C14:Drana, Kalastria Bloodchief:drana, kalastria bloodchief').
card_rarity('drana, kalastria bloodchief'/'C14', 'Rare').
card_artist('drana, kalastria bloodchief'/'C14', 'Mike Bierek').
card_number('drana, kalastria bloodchief'/'C14', '141').
card_flavor_text('drana, kalastria bloodchief'/'C14', '\"If our former masters would have us kneel again, they shall feel our defiance slashed across their membranes.\"').
card_multiverse_id('drana, kalastria bloodchief'/'C14', '389490').

card_in_set('dread return', 'C14').
card_original_type('dread return'/'C14', 'Sorcery').
card_original_text('dread return'/'C14', 'Return target creature card from your graveyard to the battlefield.\nFlashback—Sacrifice three creatures. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('dread return'/'C14', 'dread return').
card_uid('dread return'/'C14', 'C14:Dread Return:dread return').
card_rarity('dread return'/'C14', 'Uncommon').
card_artist('dread return'/'C14', 'Kev Walker').
card_number('dread return'/'C14', '142').
card_flavor_text('dread return'/'C14', 'Those who forget the horrors of the past are doomed to re-meet them.').
card_multiverse_id('dread return'/'C14', '389491').

card_in_set('dreamstone hedron', 'C14').
card_original_type('dreamstone hedron'/'C14', 'Artifact').
card_original_text('dreamstone hedron'/'C14', '{T}: Add {3} to your mana pool.\n{3}, {T}, Sacrifice Dreamstone Hedron: Draw three cards.').
card_image_name('dreamstone hedron'/'C14', 'dreamstone hedron').
card_uid('dreamstone hedron'/'C14', 'C14:Dreamstone Hedron:dreamstone hedron').
card_rarity('dreamstone hedron'/'C14', 'Uncommon').
card_artist('dreamstone hedron'/'C14', 'Eric Deschamps').
card_number('dreamstone hedron'/'C14', '236').
card_flavor_text('dreamstone hedron'/'C14', 'Only the Eldrazi mind thinks in the warped paths required to open the hedrons and tap the power within.').
card_multiverse_id('dreamstone hedron'/'C14', '389492').

card_in_set('dregs of sorrow', 'C14').
card_original_type('dregs of sorrow'/'C14', 'Sorcery').
card_original_text('dregs of sorrow'/'C14', 'Destroy X target nonblack creatures. Draw X cards.').
card_image_name('dregs of sorrow'/'C14', 'dregs of sorrow').
card_uid('dregs of sorrow'/'C14', 'C14:Dregs of Sorrow:dregs of sorrow').
card_rarity('dregs of sorrow'/'C14', 'Rare').
card_artist('dregs of sorrow'/'C14', 'Thomas Gianni').
card_number('dregs of sorrow'/'C14', '143').
card_flavor_text('dregs of sorrow'/'C14', 'Crovax gazed on the dead, and for one dark moment he saw a banquet.').
card_multiverse_id('dregs of sorrow'/'C14', '389493').

card_in_set('drifting meadow', 'C14').
card_original_type('drifting meadow'/'C14', 'Land').
card_original_text('drifting meadow'/'C14', 'Drifting Meadow enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('drifting meadow'/'C14', 'drifting meadow').
card_uid('drifting meadow'/'C14', 'C14:Drifting Meadow:drifting meadow').
card_rarity('drifting meadow'/'C14', 'Common').
card_artist('drifting meadow'/'C14', 'Jonas De Ro').
card_number('drifting meadow'/'C14', '292').
card_multiverse_id('drifting meadow'/'C14', '389494').

card_in_set('drove of elves', 'C14').
card_original_type('drove of elves'/'C14', 'Creature — Elf').
card_original_text('drove of elves'/'C14', 'Hexproof\nDrove of Elves\'s power and toughness are each equal to the number of green permanents you control.').
card_image_name('drove of elves'/'C14', 'drove of elves').
card_uid('drove of elves'/'C14', 'C14:Drove of Elves:drove of elves').
card_rarity('drove of elves'/'C14', 'Uncommon').
card_artist('drove of elves'/'C14', 'Larry MacDougall').
card_number('drove of elves'/'C14', '189').
card_flavor_text('drove of elves'/'C14', '\"The light of beauty protects our journeys through darkness.\"').
card_multiverse_id('drove of elves'/'C14', '389495').

card_in_set('dualcaster mage', 'C14').
card_original_type('dualcaster mage'/'C14', 'Creature — Human Wizard').
card_original_text('dualcaster mage'/'C14', 'Flash\nWhen Dualcaster Mage enters the battlefield, copy target instant or sorcery spell. You may choose new targets for the copy.').
card_first_print('dualcaster mage', 'C14').
card_image_name('dualcaster mage'/'C14', 'dualcaster mage').
card_uid('dualcaster mage'/'C14', 'C14:Dualcaster Mage:dualcaster mage').
card_rarity('dualcaster mage'/'C14', 'Rare').
card_artist('dualcaster mage'/'C14', 'Matt Stewart').
card_number('dualcaster mage'/'C14', '34').
card_flavor_text('dualcaster mage'/'C14', '\"It has been my experience that disasters like to strike in pairs.\"').
card_multiverse_id('dualcaster mage'/'C14', '389496').

card_in_set('dulcet sirens', 'C14').
card_original_type('dulcet sirens'/'C14', 'Creature — Siren').
card_original_text('dulcet sirens'/'C14', '{U}, {T}: Target creature attacks target opponent this turn if able.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('dulcet sirens', 'C14').
card_image_name('dulcet sirens'/'C14', 'dulcet sirens').
card_uid('dulcet sirens'/'C14', 'C14:Dulcet Sirens:dulcet sirens').
card_rarity('dulcet sirens'/'C14', 'Rare').
card_artist('dulcet sirens'/'C14', 'Magali Villeneuve').
card_number('dulcet sirens'/'C14', '14').
card_flavor_text('dulcet sirens'/'C14', 'What are the orders of a king or the demands of a creed next to such melodies?').
card_multiverse_id('dulcet sirens'/'C14', '389497').

card_in_set('elvish archdruid', 'C14').
card_original_type('elvish archdruid'/'C14', 'Creature — Elf Druid').
card_original_text('elvish archdruid'/'C14', 'Other Elf creatures you control get +1/+1.\n{T}: Add {G} to your mana pool for each Elf you control.').
card_image_name('elvish archdruid'/'C14', 'elvish archdruid').
card_uid('elvish archdruid'/'C14', 'C14:Elvish Archdruid:elvish archdruid').
card_rarity('elvish archdruid'/'C14', 'Rare').
card_artist('elvish archdruid'/'C14', 'Karl Kopinski').
card_number('elvish archdruid'/'C14', '190').
card_flavor_text('elvish archdruid'/'C14', 'He knows the name of every elf born in the last four centuries. More importantly, they all know his.').
card_multiverse_id('elvish archdruid'/'C14', '389498').

card_in_set('elvish mystic', 'C14').
card_original_type('elvish mystic'/'C14', 'Creature — Elf Druid').
card_original_text('elvish mystic'/'C14', '{T}: Add {G} to your mana pool.').
card_image_name('elvish mystic'/'C14', 'elvish mystic').
card_uid('elvish mystic'/'C14', 'C14:Elvish Mystic:elvish mystic').
card_rarity('elvish mystic'/'C14', 'Common').
card_artist('elvish mystic'/'C14', 'Wesley Burt').
card_number('elvish mystic'/'C14', '191').
card_flavor_text('elvish mystic'/'C14', '\"Life grows everywhere. My kin merely find those places where it grows strongest.\"\n—Nissa Revane').
card_multiverse_id('elvish mystic'/'C14', '389499').

card_in_set('elvish skysweeper', 'C14').
card_original_type('elvish skysweeper'/'C14', 'Creature — Elf Warrior').
card_original_text('elvish skysweeper'/'C14', '{4}{G}, Sacrifice a creature: Destroy target creature with flying.').
card_image_name('elvish skysweeper'/'C14', 'elvish skysweeper').
card_uid('elvish skysweeper'/'C14', 'C14:Elvish Skysweeper:elvish skysweeper').
card_rarity('elvish skysweeper'/'C14', 'Common').
card_artist('elvish skysweeper'/'C14', 'Mark Tedin').
card_number('elvish skysweeper'/'C14', '192').
card_flavor_text('elvish skysweeper'/'C14', 'The spires of Ravnica are no different from the tall trees of other planes. The elves navigate and protect them just the same.').
card_multiverse_id('elvish skysweeper'/'C14', '389500').

card_in_set('elvish visionary', 'C14').
card_original_type('elvish visionary'/'C14', 'Creature — Elf Shaman').
card_original_text('elvish visionary'/'C14', 'When Elvish Visionary enters the battlefield, draw a card.').
card_image_name('elvish visionary'/'C14', 'elvish visionary').
card_uid('elvish visionary'/'C14', 'C14:Elvish Visionary:elvish visionary').
card_rarity('elvish visionary'/'C14', 'Common').
card_artist('elvish visionary'/'C14', 'D. Alexander Gregory').
card_number('elvish visionary'/'C14', '193').
card_flavor_text('elvish visionary'/'C14', '\"From a tiny sprout, the greatest trees grow and flourish. May the seeds of your mind be equally fruitful.\"').
card_multiverse_id('elvish visionary'/'C14', '389501').

card_in_set('emerald medallion', 'C14').
card_original_type('emerald medallion'/'C14', 'Artifact').
card_original_text('emerald medallion'/'C14', 'Green spells you cast cost {1} less to cast.').
card_image_name('emerald medallion'/'C14', 'emerald medallion').
card_uid('emerald medallion'/'C14', 'C14:Emerald Medallion:emerald medallion').
card_rarity('emerald medallion'/'C14', 'Rare').
card_artist('emerald medallion'/'C14', 'Daniel Ljunggren').
card_number('emerald medallion'/'C14', '237').
card_multiverse_id('emerald medallion'/'C14', '389502').

card_in_set('emeria, the sky ruin', 'C14').
card_original_type('emeria, the sky ruin'/'C14', 'Land').
card_original_text('emeria, the sky ruin'/'C14', 'Emeria, the Sky Ruin enters the battlefield tapped.\nAt the beginning of your upkeep, if you control seven or more Plains, you may return target creature card from your graveyard to the battlefield.\n{T}: Add {W} to your mana pool.').
card_image_name('emeria, the sky ruin'/'C14', 'emeria, the sky ruin').
card_uid('emeria, the sky ruin'/'C14', 'C14:Emeria, the Sky Ruin:emeria, the sky ruin').
card_rarity('emeria, the sky ruin'/'C14', 'Rare').
card_artist('emeria, the sky ruin'/'C14', 'Jaime Jones').
card_number('emeria, the sky ruin'/'C14', '293').
card_multiverse_id('emeria, the sky ruin'/'C14', '389503').

card_in_set('epochrasite', 'C14').
card_original_type('epochrasite'/'C14', 'Artifact Creature — Construct').
card_original_text('epochrasite'/'C14', 'Epochrasite enters the battlefield with three +1/+1 counters on it if you didn\'t cast it from your hand.\nWhen Epochrasite dies, exile it with three time counters on it and it gains suspend. (At the beginning of your upkeep, remove a time counter. When the last is removed, cast this card without paying its mana cost. It has haste.)').
card_image_name('epochrasite'/'C14', 'epochrasite').
card_uid('epochrasite'/'C14', 'C14:Epochrasite:epochrasite').
card_rarity('epochrasite'/'C14', 'Rare').
card_artist('epochrasite'/'C14', 'Michael Bruinsma').
card_number('epochrasite'/'C14', '238').
card_multiverse_id('epochrasite'/'C14', '389504').

card_in_set('essence warden', 'C14').
card_original_type('essence warden'/'C14', 'Creature — Elf Shaman').
card_original_text('essence warden'/'C14', 'Whenever another creature enters the battlefield, you gain 1 life.').
card_image_name('essence warden'/'C14', 'essence warden').
card_uid('essence warden'/'C14', 'C14:Essence Warden:essence warden').
card_rarity('essence warden'/'C14', 'Common').
card_artist('essence warden'/'C14', 'Terese Nielsen').
card_number('essence warden'/'C14', '194').
card_flavor_text('essence warden'/'C14', '\"The more our numbers grow, the more I gain hope that Volrath and his cursed stronghold will one day fall.\"\n—Eladamri, Lord of Leaves').
card_multiverse_id('essence warden'/'C14', '389505').

card_in_set('everflowing chalice', 'C14').
card_original_type('everflowing chalice'/'C14', 'Artifact').
card_original_text('everflowing chalice'/'C14', 'Multikicker {2} (You may pay an additional {2} any number of times as you cast this spell.)\nEverflowing Chalice enters the battlefield with a charge counter on it for each time it was kicked.\n{T}: Add {1} to your mana pool for each charge counter on Everflowing Chalice.').
card_image_name('everflowing chalice'/'C14', 'everflowing chalice').
card_uid('everflowing chalice'/'C14', 'C14:Everflowing Chalice:everflowing chalice').
card_rarity('everflowing chalice'/'C14', 'Uncommon').
card_artist('everflowing chalice'/'C14', 'Steve Argyle').
card_number('everflowing chalice'/'C14', '239').
card_multiverse_id('everflowing chalice'/'C14', '389506').

card_in_set('everglades', 'C14').
card_original_type('everglades'/'C14', 'Land').
card_original_text('everglades'/'C14', 'Everglades enters the battlefield tapped.\nWhen Everglades enters the battlefield, sacrifice it unless you return an untapped Swamp you control to its owner\'s hand.\n{T}: Add {1}{B} to your mana pool.').
card_image_name('everglades'/'C14', 'everglades').
card_uid('everglades'/'C14', 'C14:Everglades:everglades').
card_rarity('everglades'/'C14', 'Uncommon').
card_artist('everglades'/'C14', 'Bob Eggleton').
card_number('everglades'/'C14', '294').
card_multiverse_id('everglades'/'C14', '389507').

card_in_set('evernight shade', 'C14').
card_original_type('evernight shade'/'C14', 'Creature — Shade').
card_original_text('evernight shade'/'C14', '{B}: Evernight Shade gets +1/+1 until end of turn.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_image_name('evernight shade'/'C14', 'evernight shade').
card_uid('evernight shade'/'C14', 'C14:Evernight Shade:evernight shade').
card_rarity('evernight shade'/'C14', 'Uncommon').
card_artist('evernight shade'/'C14', 'Nic Klein').
card_number('evernight shade'/'C14', '144').
card_multiverse_id('evernight shade'/'C14', '389508').

card_in_set('evolving wilds', 'C14').
card_original_type('evolving wilds'/'C14', 'Land').
card_original_text('evolving wilds'/'C14', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'C14', 'evolving wilds').
card_uid('evolving wilds'/'C14', 'C14:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'C14', 'Common').
card_artist('evolving wilds'/'C14', 'Steven Belledin').
card_number('evolving wilds'/'C14', '295').
card_flavor_text('evolving wilds'/'C14', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'C14', '389509').

card_in_set('exclude', 'C14').
card_original_type('exclude'/'C14', 'Instant').
card_original_text('exclude'/'C14', 'Counter target creature spell.\nDraw a card.').
card_image_name('exclude'/'C14', 'exclude').
card_uid('exclude'/'C14', 'C14:Exclude:exclude').
card_rarity('exclude'/'C14', 'Common').
card_artist('exclude'/'C14', 'Greg Staples').
card_number('exclude'/'C14', '108').
card_flavor_text('exclude'/'C14', '\"I don\'t have time for you right now.\"\n—Teferi').
card_multiverse_id('exclude'/'C14', '389510').

card_in_set('ezuri, renegade leader', 'C14').
card_original_type('ezuri, renegade leader'/'C14', 'Legendary Creature — Elf Warrior').
card_original_text('ezuri, renegade leader'/'C14', '{G}: Regenerate another target Elf.\n{2}{G}{G}{G}: Elf creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('ezuri, renegade leader'/'C14', 'ezuri, renegade leader').
card_uid('ezuri, renegade leader'/'C14', 'C14:Ezuri, Renegade Leader:ezuri, renegade leader').
card_rarity('ezuri, renegade leader'/'C14', 'Rare').
card_artist('ezuri, renegade leader'/'C14', 'Karl Kopinski').
card_number('ezuri, renegade leader'/'C14', '195').
card_flavor_text('ezuri, renegade leader'/'C14', 'The infamous Ezuri commands the highest bounty the vedalken have ever placed upon an outlaw.').
card_multiverse_id('ezuri, renegade leader'/'C14', '389511').

card_in_set('faithless looting', 'C14').
card_original_type('faithless looting'/'C14', 'Sorcery').
card_original_text('faithless looting'/'C14', 'Draw two cards, then discard two cards.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('faithless looting'/'C14', 'faithless looting').
card_uid('faithless looting'/'C14', 'C14:Faithless Looting:faithless looting').
card_rarity('faithless looting'/'C14', 'Common').
card_artist('faithless looting'/'C14', 'Gabor Szikszai').
card_number('faithless looting'/'C14', '175').
card_flavor_text('faithless looting'/'C14', '\"Avacyn has abandoned us! We have nothing left except what we can take!\"').
card_multiverse_id('faithless looting'/'C14', '389512').

card_in_set('farhaven elf', 'C14').
card_original_type('farhaven elf'/'C14', 'Creature — Elf Druid').
card_original_text('farhaven elf'/'C14', 'When Farhaven Elf enters the battlefield, you may search your library for a basic land card and put it onto the battlefield tapped. If you do, shuffle your library.').
card_image_name('farhaven elf'/'C14', 'farhaven elf').
card_uid('farhaven elf'/'C14', 'C14:Farhaven Elf:farhaven elf').
card_rarity('farhaven elf'/'C14', 'Common').
card_artist('farhaven elf'/'C14', 'Brandon Kitkouski').
card_number('farhaven elf'/'C14', '196').
card_flavor_text('farhaven elf'/'C14', '\"Verdant bloom does exist. It merely hides for its own safety.\"').
card_multiverse_id('farhaven elf'/'C14', '389513').

card_in_set('fathom seer', 'C14').
card_original_type('fathom seer'/'C14', 'Creature — Illusion').
card_original_text('fathom seer'/'C14', 'Morph—Return two Islands you control to their owner\'s hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Fathom Seer is turned face up, draw two cards.').
card_image_name('fathom seer'/'C14', 'fathom seer').
card_uid('fathom seer'/'C14', 'C14:Fathom Seer:fathom seer').
card_rarity('fathom seer'/'C14', 'Common').
card_artist('fathom seer'/'C14', 'Ralph Horsley').
card_number('fathom seer'/'C14', '109').
card_multiverse_id('fathom seer'/'C14', '389514').

card_in_set('feldon of the third path', 'C14').
card_original_type('feldon of the third path'/'C14', 'Legendary Creature — Human Artificer').
card_original_text('feldon of the third path'/'C14', '{2}{R}, {T}: Put a token onto the battlefield that\'s a copy of target creature card in your graveyard, except it\'s an artifact in addition to its other types. It gains haste. Sacrifice it at the beginning of the next end step.').
card_first_print('feldon of the third path', 'C14').
card_image_name('feldon of the third path'/'C14', 'feldon of the third path').
card_uid('feldon of the third path'/'C14', 'C14:Feldon of the Third Path:feldon of the third path').
card_rarity('feldon of the third path'/'C14', 'Mythic Rare').
card_artist('feldon of the third path'/'C14', 'Chase Stone').
card_number('feldon of the third path'/'C14', '35').
card_flavor_text('feldon of the third path'/'C14', '\"She will come back to me.\"').
card_multiverse_id('feldon of the third path'/'C14', '389515').

card_in_set('fell the mighty', 'C14').
card_original_type('fell the mighty'/'C14', 'Sorcery').
card_original_text('fell the mighty'/'C14', 'Destroy all creatures with power greater than target creature\'s power.').
card_first_print('fell the mighty', 'C14').
card_image_name('fell the mighty'/'C14', 'fell the mighty').
card_uid('fell the mighty'/'C14', 'C14:Fell the Mighty:fell the mighty').
card_rarity('fell the mighty'/'C14', 'Rare').
card_artist('fell the mighty'/'C14', 'Raymond Swanland').
card_number('fell the mighty'/'C14', '7').
card_flavor_text('fell the mighty'/'C14', '\"They had it coming.\"\n—Squire Imalia, savior of Thiswick').
card_multiverse_id('fell the mighty'/'C14', '389516').

card_in_set('fire diamond', 'C14').
card_original_type('fire diamond'/'C14', 'Artifact').
card_original_text('fire diamond'/'C14', 'Fire Diamond enters the battlefield tapped.\n{T}: Add {R} to your mana pool.').
card_image_name('fire diamond'/'C14', 'fire diamond').
card_uid('fire diamond'/'C14', 'C14:Fire Diamond:fire diamond').
card_rarity('fire diamond'/'C14', 'Uncommon').
card_artist('fire diamond'/'C14', 'Lindsey Look').
card_number('fire diamond'/'C14', '240').
card_multiverse_id('fire diamond'/'C14', '389517').

card_in_set('flamekin village', 'C14').
card_original_type('flamekin village'/'C14', 'Land').
card_original_text('flamekin village'/'C14', 'As Flamekin Village enters the battlefield, you may reveal an Elemental card from your hand. If you don\'t, Flamekin Village enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{R}, {T}: Target creature gains haste until end of turn.').
card_first_print('flamekin village', 'C14').
card_image_name('flamekin village'/'C14', 'flamekin village').
card_uid('flamekin village'/'C14', 'C14:Flamekin Village:flamekin village').
card_rarity('flamekin village'/'C14', 'Rare').
card_artist('flamekin village'/'C14', 'Ron Spears').
card_number('flamekin village'/'C14', '60').
card_multiverse_id('flamekin village'/'C14', '389518').

card_in_set('flametongue kavu', 'C14').
card_original_type('flametongue kavu'/'C14', 'Creature — Kavu').
card_original_text('flametongue kavu'/'C14', 'When Flametongue Kavu enters the battlefield, it deals 4 damage to target creature.').
card_image_name('flametongue kavu'/'C14', 'flametongue kavu').
card_uid('flametongue kavu'/'C14', 'C14:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'C14', 'Uncommon').
card_artist('flametongue kavu'/'C14', 'Slawomir Maniak').
card_number('flametongue kavu'/'C14', '176').
card_flavor_text('flametongue kavu'/'C14', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('flametongue kavu'/'C14', '389519').

card_in_set('flesh carver', 'C14').
card_original_type('flesh carver'/'C14', 'Creature — Human Wizard').
card_original_text('flesh carver'/'C14', 'Intimidate\n{1}{B}, Sacrifice another creature: Put two +1/+1 counters on Flesh Carver.\nWhen Flesh Carver dies, put an X/X black Horror creature token onto the battlefield, where X is Flesh Carver\'s power.').
card_first_print('flesh carver', 'C14').
card_image_name('flesh carver'/'C14', 'flesh carver').
card_uid('flesh carver'/'C14', 'C14:Flesh Carver:flesh carver').
card_rarity('flesh carver'/'C14', 'Rare').
card_artist('flesh carver'/'C14', 'Jesper Ejsing').
card_number('flesh carver'/'C14', '22').
card_flavor_text('flesh carver'/'C14', '\"Just a taste?\"').
card_multiverse_id('flesh carver'/'C14', '389520').

card_in_set('flickerwisp', 'C14').
card_original_type('flickerwisp'/'C14', 'Creature — Elemental').
card_original_text('flickerwisp'/'C14', 'Flying\nWhen Flickerwisp enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('flickerwisp'/'C14', 'flickerwisp').
card_uid('flickerwisp'/'C14', 'C14:Flickerwisp:flickerwisp').
card_rarity('flickerwisp'/'C14', 'Uncommon').
card_artist('flickerwisp'/'C14', 'Jeremy Enecio').
card_number('flickerwisp'/'C14', '71').
card_flavor_text('flickerwisp'/'C14', 'Its wings disturb more than air.').
card_multiverse_id('flickerwisp'/'C14', '389521').

card_in_set('fog bank', 'C14').
card_original_type('fog bank'/'C14', 'Creature — Wall').
card_original_text('fog bank'/'C14', 'Defender, flying\nPrevent all combat damage that would be dealt to and dealt by Fog Bank.').
card_image_name('fog bank'/'C14', 'fog bank').
card_uid('fog bank'/'C14', 'C14:Fog Bank:fog bank').
card_rarity('fog bank'/'C14', 'Uncommon').
card_artist('fog bank'/'C14', 'Howard Lyon').
card_number('fog bank'/'C14', '110').
card_flavor_text('fog bank'/'C14', '\"The union of sea and sky makes a perfect cover.\"\n—Talrand, sky summoner').
card_multiverse_id('fog bank'/'C14', '389522').

card_in_set('fool\'s demise', 'C14').
card_original_type('fool\'s demise'/'C14', 'Enchantment — Aura').
card_original_text('fool\'s demise'/'C14', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under your control.\nWhen Fool\'s Demise is put into a graveyard from the battlefield, return Fool\'s Demise to its owner\'s hand.').
card_image_name('fool\'s demise'/'C14', 'fool\'s demise').
card_uid('fool\'s demise'/'C14', 'C14:Fool\'s Demise:fool\'s demise').
card_rarity('fool\'s demise'/'C14', 'Uncommon').
card_artist('fool\'s demise'/'C14', 'Zoltan Boros & Gabor Szikszai').
card_number('fool\'s demise'/'C14', '111').
card_multiverse_id('fool\'s demise'/'C14', '389523').

card_in_set('forest', 'C14').
card_original_type('forest'/'C14', 'Basic Land — Forest').
card_original_text('forest'/'C14', 'G').
card_image_name('forest'/'C14', 'forest1').
card_uid('forest'/'C14', 'C14:Forest:forest1').
card_rarity('forest'/'C14', 'Basic Land').
card_artist('forest'/'C14', 'Rob Alexander').
card_number('forest'/'C14', '334').
card_multiverse_id('forest'/'C14', '389525').

card_in_set('forest', 'C14').
card_original_type('forest'/'C14', 'Basic Land — Forest').
card_original_text('forest'/'C14', 'G').
card_image_name('forest'/'C14', 'forest2').
card_uid('forest'/'C14', 'C14:Forest:forest2').
card_rarity('forest'/'C14', 'Basic Land').
card_artist('forest'/'C14', 'Vance Kovacs').
card_number('forest'/'C14', '335').
card_multiverse_id('forest'/'C14', '389527').

card_in_set('forest', 'C14').
card_original_type('forest'/'C14', 'Basic Land — Forest').
card_original_text('forest'/'C14', 'G').
card_image_name('forest'/'C14', 'forest3').
card_uid('forest'/'C14', 'C14:Forest:forest3').
card_rarity('forest'/'C14', 'Basic Land').
card_artist('forest'/'C14', 'Craig Mullins').
card_number('forest'/'C14', '336').
card_multiverse_id('forest'/'C14', '389526').

card_in_set('forest', 'C14').
card_original_type('forest'/'C14', 'Basic Land — Forest').
card_original_text('forest'/'C14', 'G').
card_image_name('forest'/'C14', 'forest4').
card_uid('forest'/'C14', 'C14:Forest:forest4').
card_rarity('forest'/'C14', 'Basic Land').
card_artist('forest'/'C14', 'Stephen Tappin').
card_number('forest'/'C14', '337').
card_multiverse_id('forest'/'C14', '389524').

card_in_set('forgotten cave', 'C14').
card_original_type('forgotten cave'/'C14', 'Land').
card_original_text('forgotten cave'/'C14', 'Forgotten Cave enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nCycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('forgotten cave'/'C14', 'forgotten cave').
card_uid('forgotten cave'/'C14', 'C14:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'C14', 'Common').
card_artist('forgotten cave'/'C14', 'Noah Bradley').
card_number('forgotten cave'/'C14', '296').
card_multiverse_id('forgotten cave'/'C14', '389528').

card_in_set('fresh meat', 'C14').
card_original_type('fresh meat'/'C14', 'Instant').
card_original_text('fresh meat'/'C14', 'Put a 3/3 green Beast creature token onto the battlefield for each creature put into your graveyard from the battlefield this turn.').
card_image_name('fresh meat'/'C14', 'fresh meat').
card_uid('fresh meat'/'C14', 'C14:Fresh Meat:fresh meat').
card_rarity('fresh meat'/'C14', 'Rare').
card_artist('fresh meat'/'C14', 'Dave Allsop').
card_number('fresh meat'/'C14', '197').
card_flavor_text('fresh meat'/'C14', 'A scavenger\'s favorite appetizer is death.').
card_multiverse_id('fresh meat'/'C14', '389529').

card_in_set('freyalise, llanowar\'s fury', 'C14').
card_original_type('freyalise, llanowar\'s fury'/'C14', 'Planeswalker — Freyalise').
card_original_text('freyalise, llanowar\'s fury'/'C14', '+2: Put a 1/1 green Elf Druid creature token onto the battlefield with \"{T}: Add {G} to your mana pool.\"\n−2: Destroy target artifact or enchantment.\n−6: Draw a card for each green creature you control.\nFreyalise, Llanowar\'s Fury can be your commander.').
card_first_print('freyalise, llanowar\'s fury', 'C14').
card_image_name('freyalise, llanowar\'s fury'/'C14', 'freyalise, llanowar\'s fury').
card_uid('freyalise, llanowar\'s fury'/'C14', 'C14:Freyalise, Llanowar\'s Fury:freyalise, llanowar\'s fury').
card_rarity('freyalise, llanowar\'s fury'/'C14', 'Mythic Rare').
card_artist('freyalise, llanowar\'s fury'/'C14', 'Adam Paquette').
card_number('freyalise, llanowar\'s fury'/'C14', '43').
card_multiverse_id('freyalise, llanowar\'s fury'/'C14', '389530').

card_in_set('frost titan', 'C14').
card_original_type('frost titan'/'C14', 'Creature — Giant').
card_original_text('frost titan'/'C14', 'Whenever Frost Titan becomes the target of a spell or ability an opponent controls, counter that spell or ability unless its controller pays {2}.\nWhenever Frost Titan enters the battlefield or attacks, tap target permanent. It doesn\'t untap during its controller\'s next untap step.').
card_image_name('frost titan'/'C14', 'frost titan').
card_uid('frost titan'/'C14', 'C14:Frost Titan:frost titan').
card_rarity('frost titan'/'C14', 'Mythic Rare').
card_artist('frost titan'/'C14', 'Mike Bierek').
card_number('frost titan'/'C14', '112').
card_multiverse_id('frost titan'/'C14', '389531').

card_in_set('gargoyle castle', 'C14').
card_original_type('gargoyle castle'/'C14', 'Land').
card_original_text('gargoyle castle'/'C14', '{T}: Add {1} to your mana pool.\n{5}, {T}, Sacrifice Gargoyle Castle: Put a 3/4 colorless Gargoyle artifact creature token with flying onto the battlefield.').
card_image_name('gargoyle castle'/'C14', 'gargoyle castle').
card_uid('gargoyle castle'/'C14', 'C14:Gargoyle Castle:gargoyle castle').
card_rarity('gargoyle castle'/'C14', 'Rare').
card_artist('gargoyle castle'/'C14', 'Paul Bonner').
card_number('gargoyle castle'/'C14', '297').
card_flavor_text('gargoyle castle'/'C14', 'Most knew of the tower\'s wizard and gargoyle. Few realized which was guardian and which was master.').
card_multiverse_id('gargoyle castle'/'C14', '389532').

card_in_set('geist-honored monk', 'C14').
card_original_type('geist-honored monk'/'C14', 'Creature — Human Monk').
card_original_text('geist-honored monk'/'C14', 'Vigilance\nGeist-Honored Monk\'s power and toughness are each equal to the number of creatures you control.\nWhen Geist-Honored Monk enters the battlefield, put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('geist-honored monk'/'C14', 'geist-honored monk').
card_uid('geist-honored monk'/'C14', 'C14:Geist-Honored Monk:geist-honored monk').
card_rarity('geist-honored monk'/'C14', 'Rare').
card_artist('geist-honored monk'/'C14', 'Clint Cearley').
card_number('geist-honored monk'/'C14', '72').
card_multiverse_id('geist-honored monk'/'C14', '389533').

card_in_set('ghost quarter', 'C14').
card_original_type('ghost quarter'/'C14', 'Land').
card_original_text('ghost quarter'/'C14', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Ghost Quarter: Destroy target land. Its controller may search his or her library for a basic land card, put it onto the battlefield, then shuffle his or her library.').
card_image_name('ghost quarter'/'C14', 'ghost quarter').
card_uid('ghost quarter'/'C14', 'C14:Ghost Quarter:ghost quarter').
card_rarity('ghost quarter'/'C14', 'Uncommon').
card_artist('ghost quarter'/'C14', 'Peter Mohrbacher').
card_number('ghost quarter'/'C14', '298').
card_flavor_text('ghost quarter'/'C14', 'Deserted, but not uninhabited.').
card_multiverse_id('ghost quarter'/'C14', '389534').

card_in_set('ghoulcaller gisa', 'C14').
card_original_type('ghoulcaller gisa'/'C14', 'Legendary Creature — Human Wizard').
card_original_text('ghoulcaller gisa'/'C14', '{B}, {T}, Sacrifice another creature: Put X 2/2 black Zombie creature tokens onto the battlefield, where X is the sacrificed creature\'s power.').
card_first_print('ghoulcaller gisa', 'C14').
card_image_name('ghoulcaller gisa'/'C14', 'ghoulcaller gisa').
card_uid('ghoulcaller gisa'/'C14', 'C14:Ghoulcaller Gisa:ghoulcaller gisa').
card_rarity('ghoulcaller gisa'/'C14', 'Mythic Rare').
card_artist('ghoulcaller gisa'/'C14', 'Karla Ortiz').
card_number('ghoulcaller gisa'/'C14', '23').
card_flavor_text('ghoulcaller gisa'/'C14', '\"Geralf, must you always whine? I agreed to nothing. I\'ll raise ghouls anytime I wish.\"').
card_multiverse_id('ghoulcaller gisa'/'C14', '389535').

card_in_set('gift of estates', 'C14').
card_original_type('gift of estates'/'C14', 'Sorcery').
card_original_text('gift of estates'/'C14', 'If an opponent controls more lands than you, search your library for up to three Plains cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('gift of estates'/'C14', 'gift of estates').
card_uid('gift of estates'/'C14', 'C14:Gift of Estates:gift of estates').
card_rarity('gift of estates'/'C14', 'Uncommon').
card_artist('gift of estates'/'C14', 'Hugh Jamieson').
card_number('gift of estates'/'C14', '73').
card_multiverse_id('gift of estates'/'C14', '389536').

card_in_set('goblin welder', 'C14').
card_original_type('goblin welder'/'C14', 'Creature — Goblin Artificer').
card_original_text('goblin welder'/'C14', '{T}: Choose target artifact a player controls and target artifact card in that player\'s graveyard. If both targets are still legal as this ability resolves, that player simultaneously sacrifices the artifact and returns the artifact card to the battlefield.').
card_image_name('goblin welder'/'C14', 'goblin welder').
card_uid('goblin welder'/'C14', 'C14:Goblin Welder:goblin welder').
card_rarity('goblin welder'/'C14', 'Rare').
card_artist('goblin welder'/'C14', 'Scott M. Fischer').
card_number('goblin welder'/'C14', '177').
card_flavor_text('goblin welder'/'C14', '\"I wrecked your metal guy, boss. But look! I made you an ashtray.\"').
card_multiverse_id('goblin welder'/'C14', '389537').

card_in_set('grand abolisher', 'C14').
card_original_type('grand abolisher'/'C14', 'Creature — Human Cleric').
card_original_text('grand abolisher'/'C14', 'During your turn, your opponents can\'t cast spells or activate abilities of artifacts, creatures, or enchantments.').
card_image_name('grand abolisher'/'C14', 'grand abolisher').
card_uid('grand abolisher'/'C14', 'C14:Grand Abolisher:grand abolisher').
card_rarity('grand abolisher'/'C14', 'Rare').
card_artist('grand abolisher'/'C14', 'Eric Deschamps').
card_number('grand abolisher'/'C14', '74').
card_flavor_text('grand abolisher'/'C14', '\"Your superstitions and mumblings are useless chaff before my righteousness.\"').
card_multiverse_id('grand abolisher'/'C14', '389538').

card_in_set('grave sifter', 'C14').
card_original_type('grave sifter'/'C14', 'Creature — Elemental Beast').
card_original_text('grave sifter'/'C14', 'When Grave Sifter enters the battlefield, each player chooses a creature type and returns any number of cards of that type from his or her graveyard to his or her hand.').
card_first_print('grave sifter', 'C14').
card_image_name('grave sifter'/'C14', 'grave sifter').
card_uid('grave sifter'/'C14', 'C14:Grave Sifter:grave sifter').
card_rarity('grave sifter'/'C14', 'Rare').
card_artist('grave sifter'/'C14', 'Jesper Ejsing').
card_number('grave sifter'/'C14', '44').
card_flavor_text('grave sifter'/'C14', 'It claws at the earth in a vain attempt to reclaim what it once was.').
card_multiverse_id('grave sifter'/'C14', '389539').

card_in_set('grave titan', 'C14').
card_original_type('grave titan'/'C14', 'Creature — Giant').
card_original_text('grave titan'/'C14', 'Deathtouch\nWhenever Grave Titan enters the battlefield or attacks, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_image_name('grave titan'/'C14', 'grave titan').
card_uid('grave titan'/'C14', 'C14:Grave Titan:grave titan').
card_rarity('grave titan'/'C14', 'Mythic Rare').
card_artist('grave titan'/'C14', 'Nils Hamm').
card_number('grave titan'/'C14', '145').
card_flavor_text('grave titan'/'C14', 'Death in form and function.').
card_multiverse_id('grave titan'/'C14', '389540').

card_in_set('gray merchant of asphodel', 'C14').
card_original_type('gray merchant of asphodel'/'C14', 'Creature — Zombie').
card_original_text('gray merchant of asphodel'/'C14', 'When Gray Merchant of Asphodel enters the battlefield, each opponent loses X life, where X is your devotion to black. You gain life equal to the life lost this way. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_image_name('gray merchant of asphodel'/'C14', 'gray merchant of asphodel').
card_uid('gray merchant of asphodel'/'C14', 'C14:Gray Merchant of Asphodel:gray merchant of asphodel').
card_rarity('gray merchant of asphodel'/'C14', 'Common').
card_artist('gray merchant of asphodel'/'C14', 'Robbie Trevino').
card_number('gray merchant of asphodel'/'C14', '146').
card_multiverse_id('gray merchant of asphodel'/'C14', '389541').

card_in_set('great furnace', 'C14').
card_original_type('great furnace'/'C14', 'Artifact Land').
card_original_text('great furnace'/'C14', '{T}: Add {R} to your mana pool.').
card_image_name('great furnace'/'C14', 'great furnace').
card_uid('great furnace'/'C14', 'C14:Great Furnace:great furnace').
card_rarity('great furnace'/'C14', 'Common').
card_artist('great furnace'/'C14', 'Rob Alexander').
card_number('great furnace'/'C14', '299').
card_flavor_text('great furnace'/'C14', 'Kuldotha, wellspring of molten metal, temple of the goblin horde.').
card_multiverse_id('great furnace'/'C14', '389542').

card_in_set('grim flowering', 'C14').
card_original_type('grim flowering'/'C14', 'Sorcery').
card_original_text('grim flowering'/'C14', 'Draw a card for each creature card in your graveyard.').
card_image_name('grim flowering'/'C14', 'grim flowering').
card_uid('grim flowering'/'C14', 'C14:Grim Flowering:grim flowering').
card_rarity('grim flowering'/'C14', 'Uncommon').
card_artist('grim flowering'/'C14', 'Adam Paquette').
card_number('grim flowering'/'C14', '198').
card_flavor_text('grim flowering'/'C14', '\"Nothing in nature goes to waste, not even the rotting corpse of a good-for-nothing, blood-sucking vampire.\"\n—Halana of Ulvenwald').
card_multiverse_id('grim flowering'/'C14', '389543').

card_in_set('hallowed spiritkeeper', 'C14').
card_original_type('hallowed spiritkeeper'/'C14', 'Creature — Avatar').
card_original_text('hallowed spiritkeeper'/'C14', 'Vigilance\nWhen Hallowed Spiritkeeper dies, put X 1/1 white Spirit creature tokens with flying onto the battlefield, where X is the number of creature cards in your graveyard.').
card_first_print('hallowed spiritkeeper', 'C14').
card_image_name('hallowed spiritkeeper'/'C14', 'hallowed spiritkeeper').
card_uid('hallowed spiritkeeper'/'C14', 'C14:Hallowed Spiritkeeper:hallowed spiritkeeper').
card_rarity('hallowed spiritkeeper'/'C14', 'Rare').
card_artist('hallowed spiritkeeper'/'C14', 'Steve Prescott').
card_number('hallowed spiritkeeper'/'C14', '8').
card_multiverse_id('hallowed spiritkeeper'/'C14', '389544').

card_in_set('harrow', 'C14').
card_original_type('harrow'/'C14', 'Instant').
card_original_text('harrow'/'C14', 'As an additional cost to cast Harrow, sacrifice a land.\nSearch your library for up to two basic land cards and put them onto the battlefield. Then shuffle your library.').
card_image_name('harrow'/'C14', 'harrow').
card_uid('harrow'/'C14', 'C14:Harrow:harrow').
card_rarity('harrow'/'C14', 'Common').
card_artist('harrow'/'C14', 'Rob Alexander').
card_number('harrow'/'C14', '199').
card_flavor_text('harrow'/'C14', 'No spot in nature is truly barren.').
card_multiverse_id('harrow'/'C14', '389545').

card_in_set('haunted fengraf', 'C14').
card_original_type('haunted fengraf'/'C14', 'Land').
card_original_text('haunted fengraf'/'C14', '{T}: Add {1} to your mana pool.\n{3}, {T}, Sacrifice Haunted Fengraf: Return a creature card at random from your graveyard to your hand.').
card_image_name('haunted fengraf'/'C14', 'haunted fengraf').
card_uid('haunted fengraf'/'C14', 'C14:Haunted Fengraf:haunted fengraf').
card_rarity('haunted fengraf'/'C14', 'Common').
card_artist('haunted fengraf'/'C14', 'Adam Paquette').
card_number('haunted fengraf'/'C14', '300').
card_flavor_text('haunted fengraf'/'C14', 'A ghoulcaller\'s playground.').
card_multiverse_id('haunted fengraf'/'C14', '389546').

card_in_set('havenwood battleground', 'C14').
card_original_type('havenwood battleground'/'C14', 'Land').
card_original_text('havenwood battleground'/'C14', 'Havenwood Battleground enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Havenwood Battleground: Add {G}{G} to your mana pool.').
card_image_name('havenwood battleground'/'C14', 'havenwood battleground').
card_uid('havenwood battleground'/'C14', 'C14:Havenwood Battleground:havenwood battleground').
card_rarity('havenwood battleground'/'C14', 'Uncommon').
card_artist('havenwood battleground'/'C14', 'Liz Danforth').
card_number('havenwood battleground'/'C14', '301').
card_multiverse_id('havenwood battleground'/'C14', '389547').

card_in_set('hoard-smelter dragon', 'C14').
card_original_type('hoard-smelter dragon'/'C14', 'Creature — Dragon').
card_original_text('hoard-smelter dragon'/'C14', 'Flying\n{3}{R}: Destroy target artifact. Hoard-Smelter Dragon gets +X/+0 until end of turn, where X is that artifact\'s converted mana cost.').
card_image_name('hoard-smelter dragon'/'C14', 'hoard-smelter dragon').
card_uid('hoard-smelter dragon'/'C14', 'C14:Hoard-Smelter Dragon:hoard-smelter dragon').
card_rarity('hoard-smelter dragon'/'C14', 'Rare').
card_artist('hoard-smelter dragon'/'C14', 'Eric Deschamps').
card_number('hoard-smelter dragon'/'C14', '178').
card_flavor_text('hoard-smelter dragon'/'C14', 'He collects the precious metals, and then converts the rest to propellant.').
card_multiverse_id('hoard-smelter dragon'/'C14', '389548').

card_in_set('hoverguard sweepers', 'C14').
card_original_type('hoverguard sweepers'/'C14', 'Creature — Drone').
card_original_text('hoverguard sweepers'/'C14', 'Flying\nWhen Hoverguard Sweepers enters the battlefield, you may return up to two target creatures to their owners\' hands.').
card_image_name('hoverguard sweepers'/'C14', 'hoverguard sweepers').
card_uid('hoverguard sweepers'/'C14', 'C14:Hoverguard Sweepers:hoverguard sweepers').
card_rarity('hoverguard sweepers'/'C14', 'Rare').
card_artist('hoverguard sweepers'/'C14', 'Mark A. Nelson').
card_number('hoverguard sweepers'/'C14', '113').
card_flavor_text('hoverguard sweepers'/'C14', 'They only appear in emergencies, and emergencies never last long.').
card_multiverse_id('hoverguard sweepers'/'C14', '389549').

card_in_set('hunting triad', 'C14').
card_original_type('hunting triad'/'C14', 'Tribal Sorcery — Elf').
card_original_text('hunting triad'/'C14', 'Put three 1/1 green Elf Warrior creature tokens onto the battlefield.\nReinforce 3—{3}{G} ({3}{G}, Discard this card: Put three +1/+1 counters on target creature.)').
card_image_name('hunting triad'/'C14', 'hunting triad').
card_uid('hunting triad'/'C14', 'C14:Hunting Triad:hunting triad').
card_rarity('hunting triad'/'C14', 'Uncommon').
card_artist('hunting triad'/'C14', 'Jim Nelson').
card_number('hunting triad'/'C14', '200').
card_flavor_text('hunting triad'/'C14', '\"Eyeblights are easy to track. Just follow the imperfections.\"').
card_multiverse_id('hunting triad'/'C14', '389550').

card_in_set('ichor wellspring', 'C14').
card_original_type('ichor wellspring'/'C14', 'Artifact').
card_original_text('ichor wellspring'/'C14', 'When Ichor Wellspring enters the battlefield or is put into a graveyard from the battlefield, draw a card.').
card_image_name('ichor wellspring'/'C14', 'ichor wellspring').
card_uid('ichor wellspring'/'C14', 'C14:Ichor Wellspring:ichor wellspring').
card_rarity('ichor wellspring'/'C14', 'Common').
card_artist('ichor wellspring'/'C14', 'Steven Belledin').
card_number('ichor wellspring'/'C14', '241').
card_flavor_text('ichor wellspring'/'C14', '\"Our glorious infection has taken hold.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('ichor wellspring'/'C14', '389551').

card_in_set('immaculate magistrate', 'C14').
card_original_type('immaculate magistrate'/'C14', 'Creature — Elf Shaman').
card_original_text('immaculate magistrate'/'C14', '{T}: Put a +1/+1 counter on target creature for each Elf you control.').
card_image_name('immaculate magistrate'/'C14', 'immaculate magistrate').
card_uid('immaculate magistrate'/'C14', 'C14:Immaculate Magistrate:immaculate magistrate').
card_rarity('immaculate magistrate'/'C14', 'Rare').
card_artist('immaculate magistrate'/'C14', 'Jim Nelson').
card_number('immaculate magistrate'/'C14', '201').
card_flavor_text('immaculate magistrate'/'C14', 'Elves of the immaculate class weave flora into living creatures—sometimes to endorse an elite warrior, sometimes to create a breathing work of art.').
card_multiverse_id('immaculate magistrate'/'C14', '389552').

card_in_set('impact resonance', 'C14').
card_original_type('impact resonance'/'C14', 'Instant').
card_original_text('impact resonance'/'C14', 'Impact Resonance deals X damage divided as you choose among any number of target creatures, where X is the greatest amount of damage dealt by a source to a permanent or player this turn.').
card_first_print('impact resonance', 'C14').
card_image_name('impact resonance'/'C14', 'impact resonance').
card_uid('impact resonance'/'C14', 'C14:Impact Resonance:impact resonance').
card_rarity('impact resonance'/'C14', 'Rare').
card_artist('impact resonance'/'C14', 'Jesper Ejsing').
card_number('impact resonance'/'C14', '36').
card_multiverse_id('impact resonance'/'C14', '389553').

card_in_set('imperious perfect', 'C14').
card_original_type('imperious perfect'/'C14', 'Creature — Elf Warrior').
card_original_text('imperious perfect'/'C14', 'Other Elf creatures you control get +1/+1.\n{G}, {T}: Put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_image_name('imperious perfect'/'C14', 'imperious perfect').
card_uid('imperious perfect'/'C14', 'C14:Imperious Perfect:imperious perfect').
card_rarity('imperious perfect'/'C14', 'Uncommon').
card_artist('imperious perfect'/'C14', 'Scott M. Fischer').
card_number('imperious perfect'/'C14', '202').
card_flavor_text('imperious perfect'/'C14', 'In a culture of beauty, the most beautiful are worshipped as gods.').
card_multiverse_id('imperious perfect'/'C14', '389554').

card_in_set('incite rebellion', 'C14').
card_original_type('incite rebellion'/'C14', 'Sorcery').
card_original_text('incite rebellion'/'C14', 'For each player, Incite Rebellion deals damage to that player and each creature that player controls equal to the number of creatures he or she controls.').
card_first_print('incite rebellion', 'C14').
card_image_name('incite rebellion'/'C14', 'incite rebellion').
card_uid('incite rebellion'/'C14', 'C14:Incite Rebellion:incite rebellion').
card_rarity('incite rebellion'/'C14', 'Rare').
card_artist('incite rebellion'/'C14', 'Alex Horley-Orlandelli').
card_number('incite rebellion'/'C14', '37').
card_flavor_text('incite rebellion'/'C14', 'Nothing breaks the monotony like a good old-fashioned riot.').
card_multiverse_id('incite rebellion'/'C14', '389555').

card_in_set('infernal offering', 'C14').
card_original_type('infernal offering'/'C14', 'Sorcery').
card_original_text('infernal offering'/'C14', 'Choose an opponent. You and that player each sacrifice a creature. Each player who sacrificed a creature this way draws two cards.\nChoose an opponent. Return a creature card from your graveyard to the battlefield, then that player returns a creature card from his or her graveyard to the battlefield.').
card_first_print('infernal offering', 'C14').
card_image_name('infernal offering'/'C14', 'infernal offering').
card_uid('infernal offering'/'C14', 'C14:Infernal Offering:infernal offering').
card_rarity('infernal offering'/'C14', 'Rare').
card_artist('infernal offering'/'C14', 'Anthony Palumbo').
card_number('infernal offering'/'C14', '24').
card_multiverse_id('infernal offering'/'C14', '389556').

card_in_set('infinite reflection', 'C14').
card_original_type('infinite reflection'/'C14', 'Enchantment — Aura').
card_original_text('infinite reflection'/'C14', 'Enchant creature\nWhen Infinite Reflection enters the battlefield attached to a creature, each other nontoken creature you control becomes a copy of that creature.\nNontoken creatures you control enter the battlefield as a copy of enchanted creature.').
card_image_name('infinite reflection'/'C14', 'infinite reflection').
card_uid('infinite reflection'/'C14', 'C14:Infinite Reflection:infinite reflection').
card_rarity('infinite reflection'/'C14', 'Rare').
card_artist('infinite reflection'/'C14', 'Igor Kieryluk').
card_number('infinite reflection'/'C14', '114').
card_multiverse_id('infinite reflection'/'C14', '389557').

card_in_set('ingot chewer', 'C14').
card_original_type('ingot chewer'/'C14', 'Creature — Elemental').
card_original_text('ingot chewer'/'C14', 'When Ingot Chewer enters the battlefield, destroy target artifact.\nEvoke {R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('ingot chewer'/'C14', 'ingot chewer').
card_uid('ingot chewer'/'C14', 'C14:Ingot Chewer:ingot chewer').
card_rarity('ingot chewer'/'C14', 'Common').
card_artist('ingot chewer'/'C14', 'Kev Walker').
card_number('ingot chewer'/'C14', '179').
card_flavor_text('ingot chewer'/'C14', 'Elementals are ideas given form. This one is the idea of \"smashitude.\"').
card_multiverse_id('ingot chewer'/'C14', '389558').

card_in_set('intellectual offering', 'C14').
card_original_type('intellectual offering'/'C14', 'Instant').
card_original_text('intellectual offering'/'C14', 'Choose an opponent. You and that player each draw three cards.\nChoose an opponent. Untap all nonland permanents you control and all nonland permanents that player controls.').
card_first_print('intellectual offering', 'C14').
card_image_name('intellectual offering'/'C14', 'intellectual offering').
card_uid('intellectual offering'/'C14', 'C14:Intellectual Offering:intellectual offering').
card_rarity('intellectual offering'/'C14', 'Rare').
card_artist('intellectual offering'/'C14', 'Mark Winters').
card_number('intellectual offering'/'C14', '15').
card_multiverse_id('intellectual offering'/'C14', '389559').

card_in_set('into the roil', 'C14').
card_original_type('into the roil'/'C14', 'Instant').
card_original_text('into the roil'/'C14', 'Kicker {1}{U} (You may pay an additional {1}{U} as you cast this spell.)\nReturn target nonland permanent to its owner\'s hand. If Into the Roil was kicked, draw a card.').
card_image_name('into the roil'/'C14', 'into the roil').
card_uid('into the roil'/'C14', 'C14:Into the Roil:into the roil').
card_rarity('into the roil'/'C14', 'Common').
card_artist('into the roil'/'C14', 'Kieran Yanner').
card_number('into the roil'/'C14', '115').
card_flavor_text('into the roil'/'C14', '\"Roil tide! Roil tide! Tie yourselves down!\"').
card_multiverse_id('into the roil'/'C14', '389560').

card_in_set('island', 'C14').
card_original_type('island'/'C14', 'Basic Land — Island').
card_original_text('island'/'C14', 'U').
card_image_name('island'/'C14', 'island1').
card_uid('island'/'C14', 'C14:Island:island1').
card_rarity('island'/'C14', 'Basic Land').
card_artist('island'/'C14', 'Rob Alexander').
card_number('island'/'C14', '322').
card_multiverse_id('island'/'C14', '389561').

card_in_set('island', 'C14').
card_original_type('island'/'C14', 'Basic Land — Island').
card_original_text('island'/'C14', 'U').
card_image_name('island'/'C14', 'island2').
card_uid('island'/'C14', 'C14:Island:island2').
card_rarity('island'/'C14', 'Basic Land').
card_artist('island'/'C14', 'Jeremy Jarvis').
card_number('island'/'C14', '323').
card_multiverse_id('island'/'C14', '389563').

card_in_set('island', 'C14').
card_original_type('island'/'C14', 'Basic Land — Island').
card_original_text('island'/'C14', 'U').
card_image_name('island'/'C14', 'island3').
card_uid('island'/'C14', 'C14:Island:island3').
card_rarity('island'/'C14', 'Basic Land').
card_artist('island'/'C14', 'Craig Mullins').
card_number('island'/'C14', '324').
card_multiverse_id('island'/'C14', '389562').

card_in_set('island', 'C14').
card_original_type('island'/'C14', 'Basic Land — Island').
card_original_text('island'/'C14', 'U').
card_image_name('island'/'C14', 'island4').
card_uid('island'/'C14', 'C14:Island:island4').
card_rarity('island'/'C14', 'Basic Land').
card_artist('island'/'C14', 'Richard Wright').
card_number('island'/'C14', '325').
card_multiverse_id('island'/'C14', '389564').

card_in_set('ixidron', 'C14').
card_original_type('ixidron'/'C14', 'Creature — Illusion').
card_original_text('ixidron'/'C14', 'As Ixidron enters the battlefield, turn all other nontoken creatures face down. (They\'re 2/2 creatures.)\nIxidron\'s power and toughness are each equal to the number of face-down creatures on the battlefield.').
card_image_name('ixidron'/'C14', 'ixidron').
card_uid('ixidron'/'C14', 'C14:Ixidron:ixidron').
card_rarity('ixidron'/'C14', 'Rare').
card_artist('ixidron'/'C14', 'Terese Nielsen').
card_number('ixidron'/'C14', '116').
card_multiverse_id('ixidron'/'C14', '389565').

card_in_set('jalum tome', 'C14').
card_original_type('jalum tome'/'C14', 'Artifact').
card_original_text('jalum tome'/'C14', '{2}, {T}: Draw a card, then discard a card.').
card_image_name('jalum tome'/'C14', 'jalum tome').
card_uid('jalum tome'/'C14', 'C14:Jalum Tome:jalum tome').
card_rarity('jalum tome'/'C14', 'Rare').
card_artist('jalum tome'/'C14', 'Jerry Tiritilli').
card_number('jalum tome'/'C14', '242').
card_flavor_text('jalum tome'/'C14', 'Most books simply deliver knowledge. This one expects some in return.').
card_multiverse_id('jalum tome'/'C14', '389566').

card_in_set('jazal goldmane', 'C14').
card_original_type('jazal goldmane'/'C14', 'Legendary Creature — Cat Warrior').
card_original_text('jazal goldmane'/'C14', 'First strike\n{3}{W}{W}: Attacking creatures you control get +X/+X until end of turn, where X is the number of attacking creatures.').
card_first_print('jazal goldmane', 'C14').
card_image_name('jazal goldmane'/'C14', 'jazal goldmane').
card_uid('jazal goldmane'/'C14', 'C14:Jazal Goldmane:jazal goldmane').
card_rarity('jazal goldmane'/'C14', 'Mythic Rare').
card_artist('jazal goldmane'/'C14', 'Aaron Miller').
card_number('jazal goldmane'/'C14', '9').
card_flavor_text('jazal goldmane'/'C14', '\"As my kha, he is the source of my inspiration. As my brother, he is the embodiment of my aspirations.\"\n—Ajani Goldmane').
card_multiverse_id('jazal goldmane'/'C14', '389567').

card_in_set('jet medallion', 'C14').
card_original_type('jet medallion'/'C14', 'Artifact').
card_original_text('jet medallion'/'C14', 'Black spells you cast cost {1} less to cast.').
card_image_name('jet medallion'/'C14', 'jet medallion').
card_uid('jet medallion'/'C14', 'C14:Jet Medallion:jet medallion').
card_rarity('jet medallion'/'C14', 'Rare').
card_artist('jet medallion'/'C14', 'Daniel Ljunggren').
card_number('jet medallion'/'C14', '243').
card_multiverse_id('jet medallion'/'C14', '389568').

card_in_set('joraga warcaller', 'C14').
card_original_type('joraga warcaller'/'C14', 'Creature — Elf Warrior').
card_original_text('joraga warcaller'/'C14', 'Multikicker {1}{G} (You may pay an additional {1}{G} any number of times as you cast this spell.)\nJoraga Warcaller enters the battlefield with a +1/+1 counter on it for each time it was kicked.\nOther Elf creatures you control get +1/+1 for each +1/+1 counter on Joraga Warcaller.').
card_image_name('joraga warcaller'/'C14', 'joraga warcaller').
card_uid('joraga warcaller'/'C14', 'C14:Joraga Warcaller:joraga warcaller').
card_rarity('joraga warcaller'/'C14', 'Rare').
card_artist('joraga warcaller'/'C14', 'Steven Belledin').
card_number('joraga warcaller'/'C14', '203').
card_multiverse_id('joraga warcaller'/'C14', '389569').

card_in_set('jungle basin', 'C14').
card_original_type('jungle basin'/'C14', 'Land').
card_original_text('jungle basin'/'C14', 'Jungle Basin enters the battlefield tapped.\nWhen Jungle Basin enters the battlefield, sacrifice it unless you return an untapped Forest you control to its owner\'s hand.\n{T}: Add {1}{G} to your mana pool.').
card_image_name('jungle basin'/'C14', 'jungle basin').
card_uid('jungle basin'/'C14', 'C14:Jungle Basin:jungle basin').
card_rarity('jungle basin'/'C14', 'Uncommon').
card_artist('jungle basin'/'C14', 'John Avon').
card_number('jungle basin'/'C14', '302').
card_multiverse_id('jungle basin'/'C14', '389570').

card_in_set('junk diver', 'C14').
card_original_type('junk diver'/'C14', 'Artifact Creature — Bird').
card_original_text('junk diver'/'C14', 'Flying\nWhen Junk Diver dies, return another target artifact card from your graveyard to your hand.').
card_image_name('junk diver'/'C14', 'junk diver').
card_uid('junk diver'/'C14', 'C14:Junk Diver:junk diver').
card_rarity('junk diver'/'C14', 'Rare').
card_artist('junk diver'/'C14', 'Eric Peterson').
card_number('junk diver'/'C14', '244').
card_flavor_text('junk diver'/'C14', 'Garbage in, treasure out.').
card_multiverse_id('junk diver'/'C14', '389571').

card_in_set('karoo', 'C14').
card_original_type('karoo'/'C14', 'Land').
card_original_text('karoo'/'C14', 'Karoo enters the battlefield tapped.\nWhen Karoo enters the battlefield, sacrifice it unless you return an untapped Plains you control to its owner\'s hand.\n{T}: Add {1}{W} to your mana pool.').
card_image_name('karoo'/'C14', 'karoo').
card_uid('karoo'/'C14', 'C14:Karoo:karoo').
card_rarity('karoo'/'C14', 'Uncommon').
card_artist('karoo'/'C14', 'Zina Saunders').
card_number('karoo'/'C14', '303').
card_multiverse_id('karoo'/'C14', '389572').

card_in_set('kemba, kha regent', 'C14').
card_original_type('kemba, kha regent'/'C14', 'Legendary Creature — Cat Cleric').
card_original_text('kemba, kha regent'/'C14', 'At the beginning of your upkeep, put a 2/2 white Cat creature token onto the battlefield for each Equipment attached to Kemba, Kha Regent.').
card_image_name('kemba, kha regent'/'C14', 'kemba, kha regent').
card_uid('kemba, kha regent'/'C14', 'C14:Kemba, Kha Regent:kemba, kha regent').
card_rarity('kemba, kha regent'/'C14', 'Rare').
card_artist('kemba, kha regent'/'C14', 'Todd Lockwood').
card_number('kemba, kha regent'/'C14', '75').
card_flavor_text('kemba, kha regent'/'C14', '\"I am not Raksha. I never will be. But I refuse to be the kha who watched her pride be torn asunder.\"').
card_multiverse_id('kemba, kha regent'/'C14', '389573').

card_in_set('kor sanctifiers', 'C14').
card_original_type('kor sanctifiers'/'C14', 'Creature — Kor Cleric').
card_original_text('kor sanctifiers'/'C14', 'Kicker {W} (You may pay an additional {W} as you cast this spell.)\nWhen Kor Sanctifiers enters the battlefield, if it was kicked, destroy target artifact or enchantment.').
card_image_name('kor sanctifiers'/'C14', 'kor sanctifiers').
card_uid('kor sanctifiers'/'C14', 'C14:Kor Sanctifiers:kor sanctifiers').
card_rarity('kor sanctifiers'/'C14', 'Common').
card_artist('kor sanctifiers'/'C14', 'Dan Scott').
card_number('kor sanctifiers'/'C14', '76').
card_flavor_text('kor sanctifiers'/'C14', '\"Why keep such trinkets? They only add weight to your travels.\"').
card_multiverse_id('kor sanctifiers'/'C14', '389574').

card_in_set('lashwrithe', 'C14').
card_original_type('lashwrithe'/'C14', 'Artifact — Equipment').
card_original_text('lashwrithe'/'C14', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +1/+1 for each Swamp you control.\nEquip {B/P}{B/P} ({B/P} can be paid with either {B} or 2 life.)').
card_image_name('lashwrithe'/'C14', 'lashwrithe').
card_uid('lashwrithe'/'C14', 'C14:Lashwrithe:lashwrithe').
card_rarity('lashwrithe'/'C14', 'Rare').
card_artist('lashwrithe'/'C14', 'Jason Felix').
card_number('lashwrithe'/'C14', '245').
card_multiverse_id('lashwrithe'/'C14', '389575').

card_in_set('lifeblood hydra', 'C14').
card_original_type('lifeblood hydra'/'C14', 'Creature — Hydra').
card_original_text('lifeblood hydra'/'C14', 'Trample\nLifeblood Hydra enters the battlefield with X +1/+1 counters on it.\nWhen Lifeblood Hydra dies, you gain life and draw cards equal to its power.').
card_first_print('lifeblood hydra', 'C14').
card_image_name('lifeblood hydra'/'C14', 'lifeblood hydra').
card_uid('lifeblood hydra'/'C14', 'C14:Lifeblood Hydra:lifeblood hydra').
card_rarity('lifeblood hydra'/'C14', 'Rare').
card_artist('lifeblood hydra'/'C14', 'Alex Horley-Orlandelli').
card_number('lifeblood hydra'/'C14', '45').
card_flavor_text('lifeblood hydra'/'C14', 'Pharika has written her secrets on its bones so that only the worthy may discover them.').
card_multiverse_id('lifeblood hydra'/'C14', '389576').

card_in_set('liliana\'s reaver', 'C14').
card_original_type('liliana\'s reaver'/'C14', 'Creature — Zombie').
card_original_text('liliana\'s reaver'/'C14', 'Deathtouch\nWhenever Liliana\'s Reaver deals combat damage to a player, that player discards a card and you put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_image_name('liliana\'s reaver'/'C14', 'liliana\'s reaver').
card_uid('liliana\'s reaver'/'C14', 'C14:Liliana\'s Reaver:liliana\'s reaver').
card_rarity('liliana\'s reaver'/'C14', 'Rare').
card_artist('liliana\'s reaver'/'C14', 'Karl Kopinski').
card_number('liliana\'s reaver'/'C14', '147').
card_multiverse_id('liliana\'s reaver'/'C14', '389577').

card_in_set('liquimetal coating', 'C14').
card_original_type('liquimetal coating'/'C14', 'Artifact').
card_original_text('liquimetal coating'/'C14', '{T}: Target permanent becomes an artifact in addition to its other types until end of turn.').
card_image_name('liquimetal coating'/'C14', 'liquimetal coating').
card_uid('liquimetal coating'/'C14', 'C14:Liquimetal Coating:liquimetal coating').
card_rarity('liquimetal coating'/'C14', 'Uncommon').
card_artist('liquimetal coating'/'C14', 'Johann Bodin').
card_number('liquimetal coating'/'C14', '246').
card_flavor_text('liquimetal coating'/'C14', '\"They\'ll soon become accustomed to wearing skin that is not their own.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('liquimetal coating'/'C14', '389578').

card_in_set('llanowar elves', 'C14').
card_original_type('llanowar elves'/'C14', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'C14', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'C14', 'llanowar elves').
card_uid('llanowar elves'/'C14', 'C14:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'C14', 'Common').
card_artist('llanowar elves'/'C14', 'Kev Walker').
card_number('llanowar elves'/'C14', '204').
card_flavor_text('llanowar elves'/'C14', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'C14', '389579').

card_in_set('lonely sandbar', 'C14').
card_original_type('lonely sandbar'/'C14', 'Land').
card_original_text('lonely sandbar'/'C14', 'Lonely Sandbar enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nCycling {U} ({U}, Discard this card: Draw a card.)').
card_image_name('lonely sandbar'/'C14', 'lonely sandbar').
card_uid('lonely sandbar'/'C14', 'C14:Lonely Sandbar:lonely sandbar').
card_rarity('lonely sandbar'/'C14', 'Common').
card_artist('lonely sandbar'/'C14', 'Heather Hudson').
card_number('lonely sandbar'/'C14', '304').
card_multiverse_id('lonely sandbar'/'C14', '389580').

card_in_set('loreseeker\'s stone', 'C14').
card_original_type('loreseeker\'s stone'/'C14', 'Artifact').
card_original_text('loreseeker\'s stone'/'C14', '{3}, {T}: Draw three cards. This ability costs {1} more to activate for each card in your hand.').
card_first_print('loreseeker\'s stone', 'C14').
card_image_name('loreseeker\'s stone'/'C14', 'loreseeker\'s stone').
card_uid('loreseeker\'s stone'/'C14', 'C14:Loreseeker\'s Stone:loreseeker\'s stone').
card_rarity('loreseeker\'s stone'/'C14', 'Uncommon').
card_artist('loreseeker\'s stone'/'C14', 'Franz Vohwinkel').
card_number('loreseeker\'s stone'/'C14', '56').
card_flavor_text('loreseeker\'s stone'/'C14', 'Mages come from far and wide to bathe in its wisdom.').
card_multiverse_id('loreseeker\'s stone'/'C14', '389581').

card_in_set('lorthos, the tidemaker', 'C14').
card_original_type('lorthos, the tidemaker'/'C14', 'Legendary Creature — Octopus').
card_original_text('lorthos, the tidemaker'/'C14', 'Whenever Lorthos, the Tidemaker attacks, you may pay {8}. If you do, tap up to eight target permanents. Those permanents don\'t untap during their controllers\' next untap steps.').
card_image_name('lorthos, the tidemaker'/'C14', 'lorthos, the tidemaker').
card_uid('lorthos, the tidemaker'/'C14', 'C14:Lorthos, the Tidemaker:lorthos, the tidemaker').
card_rarity('lorthos, the tidemaker'/'C14', 'Mythic Rare').
card_artist('lorthos, the tidemaker'/'C14', 'Kekai Kotaki').
card_number('lorthos, the tidemaker'/'C14', '117').
card_flavor_text('lorthos, the tidemaker'/'C14', 'When Lorthos emerges from his deepwater realm, the tides bow to his will and the coastline cowers in his presence.').
card_multiverse_id('lorthos, the tidemaker'/'C14', '389582').

card_in_set('loxodon warhammer', 'C14').
card_original_type('loxodon warhammer'/'C14', 'Artifact — Equipment').
card_original_text('loxodon warhammer'/'C14', 'Equipped creature gets +3/+0 and has trample and lifelink.\nEquip {3}').
card_image_name('loxodon warhammer'/'C14', 'loxodon warhammer').
card_uid('loxodon warhammer'/'C14', 'C14:Loxodon Warhammer:loxodon warhammer').
card_rarity('loxodon warhammer'/'C14', 'Rare').
card_artist('loxodon warhammer'/'C14', 'Jeremy Jarvis').
card_number('loxodon warhammer'/'C14', '247').
card_flavor_text('loxodon warhammer'/'C14', '\"A crafter must imbue her weapon with part of her soul—something the Phyrexians are incapable of doing.\"\n—Melira').
card_multiverse_id('loxodon warhammer'/'C14', '389583').

card_in_set('lys alana huntmaster', 'C14').
card_original_type('lys alana huntmaster'/'C14', 'Creature — Elf Warrior').
card_original_text('lys alana huntmaster'/'C14', 'Whenever you cast an Elf spell, you may put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_image_name('lys alana huntmaster'/'C14', 'lys alana huntmaster').
card_uid('lys alana huntmaster'/'C14', 'C14:Lys Alana Huntmaster:lys alana huntmaster').
card_rarity('lys alana huntmaster'/'C14', 'Common').
card_artist('lys alana huntmaster'/'C14', 'Pete Venters').
card_number('lys alana huntmaster'/'C14', '205').
card_flavor_text('lys alana huntmaster'/'C14', 'From the highest tiers of Dawn\'s Light Palace to the deepest shade of Wren\'s Run, the silver notes of the horn shimmer through the air, and all who hear it feel its pull.').
card_multiverse_id('lys alana huntmaster'/'C14', '389584').

card_in_set('magmaquake', 'C14').
card_original_type('magmaquake'/'C14', 'Instant').
card_original_text('magmaquake'/'C14', 'Magmaquake deals X damage to each creature without flying and each planeswalker.').
card_image_name('magmaquake'/'C14', 'magmaquake').
card_uid('magmaquake'/'C14', 'C14:Magmaquake:magmaquake').
card_rarity('magmaquake'/'C14', 'Rare').
card_artist('magmaquake'/'C14', 'Gabor Szikszai').
card_number('magmaquake'/'C14', '180').
card_flavor_text('magmaquake'/'C14', '\"Where will you run when I punish you with the very ground you flee on?\"\n—Nicol Bolas').
card_multiverse_id('magmaquake'/'C14', '389585').

card_in_set('magus of the coffers', 'C14').
card_original_type('magus of the coffers'/'C14', 'Creature — Human Wizard').
card_original_text('magus of the coffers'/'C14', '{2}, {T}: Add {B} to your mana pool for each Swamp you control.').
card_image_name('magus of the coffers'/'C14', 'magus of the coffers').
card_uid('magus of the coffers'/'C14', 'C14:Magus of the Coffers:magus of the coffers').
card_rarity('magus of the coffers'/'C14', 'Rare').
card_artist('magus of the coffers'/'C14', 'Don Hazeltine').
card_number('magus of the coffers'/'C14', '148').
card_flavor_text('magus of the coffers'/'C14', 'All that remains of the Cabal are the echoes in its deepest vault. Yet those who hear the echoes feel a power undiminished by the dust of ages.').
card_multiverse_id('magus of the coffers'/'C14', '389586').

card_in_set('malicious affliction', 'C14').
card_original_type('malicious affliction'/'C14', 'Instant').
card_original_text('malicious affliction'/'C14', 'Morbid — When you cast Malicious Affliction, if a creature died this turn, you may copy Malicious Affliction and may choose a new target for the copy.\nDestroy target nonblack creature.').
card_first_print('malicious affliction', 'C14').
card_image_name('malicious affliction'/'C14', 'malicious affliction').
card_uid('malicious affliction'/'C14', 'C14:Malicious Affliction:malicious affliction').
card_rarity('malicious affliction'/'C14', 'Rare').
card_artist('malicious affliction'/'C14', 'Erica Yang').
card_number('malicious affliction'/'C14', '25').
card_flavor_text('malicious affliction'/'C14', '\"Such an affliction appears to seek out the pure.\"\n—Byro, plague doctor').
card_multiverse_id('malicious affliction'/'C14', '389587').

card_in_set('marble diamond', 'C14').
card_original_type('marble diamond'/'C14', 'Artifact').
card_original_text('marble diamond'/'C14', 'Marble Diamond enters the battlefield tapped.\n{T}: Add {W} to your mana pool.').
card_image_name('marble diamond'/'C14', 'marble diamond').
card_uid('marble diamond'/'C14', 'C14:Marble Diamond:marble diamond').
card_rarity('marble diamond'/'C14', 'Uncommon').
card_artist('marble diamond'/'C14', 'Lindsey Look').
card_number('marble diamond'/'C14', '248').
card_multiverse_id('marble diamond'/'C14', '389588').

card_in_set('marshal\'s anthem', 'C14').
card_original_type('marshal\'s anthem'/'C14', 'Enchantment').
card_original_text('marshal\'s anthem'/'C14', 'Multikicker {1}{W} (You may pay an additional {1}{W} any number of times as you cast this spell.)\nCreatures you control get +1/+1.\nWhen Marshal\'s Anthem enters the battlefield, return up to X target creature cards from your graveyard to the battlefield, where X is the number of times Marshal\'s Anthem was kicked.').
card_image_name('marshal\'s anthem'/'C14', 'marshal\'s anthem').
card_uid('marshal\'s anthem'/'C14', 'C14:Marshal\'s Anthem:marshal\'s anthem').
card_rarity('marshal\'s anthem'/'C14', 'Rare').
card_artist('marshal\'s anthem'/'C14', 'Matt Stewart').
card_number('marshal\'s anthem'/'C14', '77').
card_multiverse_id('marshal\'s anthem'/'C14', '389589').

card_in_set('martial coup', 'C14').
card_original_type('martial coup'/'C14', 'Sorcery').
card_original_text('martial coup'/'C14', 'Put X 1/1 white Soldier creature tokens onto the battlefield. If X is 5 or more, destroy all other creatures.').
card_image_name('martial coup'/'C14', 'martial coup').
card_uid('martial coup'/'C14', 'C14:Martial Coup:martial coup').
card_rarity('martial coup'/'C14', 'Rare').
card_artist('martial coup'/'C14', 'Greg Staples').
card_number('martial coup'/'C14', '78').
card_flavor_text('martial coup'/'C14', 'Their war forgotten, the nations of Bant stood united in the face of a common threat.').
card_multiverse_id('martial coup'/'C14', '389590').

card_in_set('mask of memory', 'C14').
card_original_type('mask of memory'/'C14', 'Artifact — Equipment').
card_original_text('mask of memory'/'C14', 'Whenever equipped creature deals combat damage to a player, you may draw two cards. If you do, discard a card.\nEquip {1}').
card_image_name('mask of memory'/'C14', 'mask of memory').
card_uid('mask of memory'/'C14', 'C14:Mask of Memory:mask of memory').
card_rarity('mask of memory'/'C14', 'Uncommon').
card_artist('mask of memory'/'C14', 'Alan Pollack').
card_number('mask of memory'/'C14', '249').
card_multiverse_id('mask of memory'/'C14', '389591').

card_in_set('masked admirers', 'C14').
card_original_type('masked admirers'/'C14', 'Creature — Elf Shaman').
card_original_text('masked admirers'/'C14', 'When Masked Admirers enters the battlefield, draw a card.\nWhenever you cast a creature spell, you may pay {G}{G}. If you do, return Masked Admirers from your graveyard to your hand.').
card_image_name('masked admirers'/'C14', 'masked admirers').
card_uid('masked admirers'/'C14', 'C14:Masked Admirers:masked admirers').
card_rarity('masked admirers'/'C14', 'Rare').
card_artist('masked admirers'/'C14', 'Eric Fortune').
card_number('masked admirers'/'C14', '206').
card_flavor_text('masked admirers'/'C14', '\"Beauty determines value, and we determine beauty.\"').
card_multiverse_id('masked admirers'/'C14', '389592').

card_in_set('masterwork of ingenuity', 'C14').
card_original_type('masterwork of ingenuity'/'C14', 'Artifact — Equipment').
card_original_text('masterwork of ingenuity'/'C14', 'You may have Masterwork of Ingenuity enter the battlefield as a copy of any Equipment on the battlefield.').
card_first_print('masterwork of ingenuity', 'C14').
card_image_name('masterwork of ingenuity'/'C14', 'masterwork of ingenuity').
card_uid('masterwork of ingenuity'/'C14', 'C14:Masterwork of Ingenuity:masterwork of ingenuity').
card_rarity('masterwork of ingenuity'/'C14', 'Rare').
card_artist('masterwork of ingenuity'/'C14', 'Ulrich Brunin').
card_number('masterwork of ingenuity'/'C14', '57').
card_flavor_text('masterwork of ingenuity'/'C14', '\"The crucible yields what you have forged in your mind. You need only reach in and draw the masterwork forth.\"\n—Wahara, Keeper of the Crucible').
card_multiverse_id('masterwork of ingenuity'/'C14', '389593').

card_in_set('mentor of the meek', 'C14').
card_original_type('mentor of the meek'/'C14', 'Creature — Human Soldier').
card_original_text('mentor of the meek'/'C14', 'Whenever another creature with power 2 or less enters the battlefield under your control, you may pay {1}. If you do, draw a card.').
card_image_name('mentor of the meek'/'C14', 'mentor of the meek').
card_uid('mentor of the meek'/'C14', 'C14:Mentor of the Meek:mentor of the meek').
card_rarity('mentor of the meek'/'C14', 'Rare').
card_artist('mentor of the meek'/'C14', 'Jana Schirmer & Johannes Voss').
card_number('mentor of the meek'/'C14', '79').
card_flavor_text('mentor of the meek'/'C14', '\"In these halls there is no pass or fail. Your true test comes with the first full moon.\"').
card_multiverse_id('mentor of the meek'/'C14', '389594').

card_in_set('midnight haunting', 'C14').
card_original_type('midnight haunting'/'C14', 'Instant').
card_original_text('midnight haunting'/'C14', 'Put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('midnight haunting'/'C14', 'midnight haunting').
card_uid('midnight haunting'/'C14', 'C14:Midnight Haunting:midnight haunting').
card_rarity('midnight haunting'/'C14', 'Uncommon').
card_artist('midnight haunting'/'C14', 'Matt Stewart').
card_number('midnight haunting'/'C14', '80').
card_flavor_text('midnight haunting'/'C14', 'The path back to the world of the living is murky and bewildering. A geist may not even realize that it\'s terrifying its own loved ones.').
card_multiverse_id('midnight haunting'/'C14', '389595').

card_in_set('mind stone', 'C14').
card_original_type('mind stone'/'C14', 'Artifact').
card_original_text('mind stone'/'C14', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Mind Stone: Draw a card.').
card_image_name('mind stone'/'C14', 'mind stone').
card_uid('mind stone'/'C14', 'C14:Mind Stone:mind stone').
card_rarity('mind stone'/'C14', 'Uncommon').
card_artist('mind stone'/'C14', 'Adam Rex').
card_number('mind stone'/'C14', '250').
card_flavor_text('mind stone'/'C14', '\"What is mana but possibility, an idea not yet given form?\"\n—Jhoira, master artificer').
card_multiverse_id('mind stone'/'C14', '389596').

card_in_set('mobilization', 'C14').
card_original_type('mobilization'/'C14', 'Enchantment').
card_original_text('mobilization'/'C14', 'Soldier creatures have vigilance.\n{2}{W}: Put a 1/1 white Soldier creature token onto the battlefield.').
card_image_name('mobilization'/'C14', 'mobilization').
card_uid('mobilization'/'C14', 'C14:Mobilization:mobilization').
card_rarity('mobilization'/'C14', 'Rare').
card_artist('mobilization'/'C14', 'Carl Critchlow').
card_number('mobilization'/'C14', '81').
card_flavor_text('mobilization'/'C14', 'Wars are won with strength, valor, and numbers—especially numbers.').
card_multiverse_id('mobilization'/'C14', '389597').

card_in_set('moonsilver spear', 'C14').
card_original_type('moonsilver spear'/'C14', 'Artifact — Equipment').
card_original_text('moonsilver spear'/'C14', 'Equipped creature has first strike.\nWhenever equipped creature attacks, put a 4/4 white Angel creature token with flying onto the battlefield.\nEquip {4}').
card_image_name('moonsilver spear'/'C14', 'moonsilver spear').
card_uid('moonsilver spear'/'C14', 'C14:Moonsilver Spear:moonsilver spear').
card_rarity('moonsilver spear'/'C14', 'Rare').
card_artist('moonsilver spear'/'C14', 'James Paick').
card_number('moonsilver spear'/'C14', '251').
card_multiverse_id('moonsilver spear'/'C14', '389598').

card_in_set('morkrut banshee', 'C14').
card_original_type('morkrut banshee'/'C14', 'Creature — Spirit').
card_original_text('morkrut banshee'/'C14', 'Morbid — When Morkrut Banshee enters the battlefield, if a creature died this turn, target creature gets -4/-4 until end of turn.').
card_image_name('morkrut banshee'/'C14', 'morkrut banshee').
card_uid('morkrut banshee'/'C14', 'C14:Morkrut Banshee:morkrut banshee').
card_rarity('morkrut banshee'/'C14', 'Uncommon').
card_artist('morkrut banshee'/'C14', 'Svetlin Velinov').
card_number('morkrut banshee'/'C14', '149').
card_flavor_text('morkrut banshee'/'C14', '\"Let go your grudges, or risk wandering the bogs forever in a murderous rage.\"\n—Hildin, priest of Avacyn').
card_multiverse_id('morkrut banshee'/'C14', '389599').

card_in_set('moss diamond', 'C14').
card_original_type('moss diamond'/'C14', 'Artifact').
card_original_text('moss diamond'/'C14', 'Moss Diamond enters the battlefield tapped.\n{T}: Add {G} to your mana pool.').
card_image_name('moss diamond'/'C14', 'moss diamond').
card_uid('moss diamond'/'C14', 'C14:Moss Diamond:moss diamond').
card_rarity('moss diamond'/'C14', 'Uncommon').
card_artist('moss diamond'/'C14', 'Lindsey Look').
card_number('moss diamond'/'C14', '252').
card_multiverse_id('moss diamond'/'C14', '389600').

card_in_set('mountain', 'C14').
card_original_type('mountain'/'C14', 'Basic Land — Mountain').
card_original_text('mountain'/'C14', 'R').
card_image_name('mountain'/'C14', 'mountain1').
card_uid('mountain'/'C14', 'C14:Mountain:mountain1').
card_rarity('mountain'/'C14', 'Basic Land').
card_artist('mountain'/'C14', 'Cliff Childs').
card_number('mountain'/'C14', '330').
card_multiverse_id('mountain'/'C14', '389602').

card_in_set('mountain', 'C14').
card_original_type('mountain'/'C14', 'Basic Land — Mountain').
card_original_text('mountain'/'C14', 'R').
card_image_name('mountain'/'C14', 'mountain2').
card_uid('mountain'/'C14', 'C14:Mountain:mountain2').
card_rarity('mountain'/'C14', 'Basic Land').
card_artist('mountain'/'C14', 'Jonas De Ro').
card_number('mountain'/'C14', '331').
card_multiverse_id('mountain'/'C14', '389601').

card_in_set('mountain', 'C14').
card_original_type('mountain'/'C14', 'Basic Land — Mountain').
card_original_text('mountain'/'C14', 'R').
card_image_name('mountain'/'C14', 'mountain3').
card_uid('mountain'/'C14', 'C14:Mountain:mountain3').
card_rarity('mountain'/'C14', 'Basic Land').
card_artist('mountain'/'C14', 'Karl Kopinski').
card_number('mountain'/'C14', '332').
card_multiverse_id('mountain'/'C14', '389603').

card_in_set('mountain', 'C14').
card_original_type('mountain'/'C14', 'Basic Land — Mountain').
card_original_text('mountain'/'C14', 'R').
card_image_name('mountain'/'C14', 'mountain4').
card_uid('mountain'/'C14', 'C14:Mountain:mountain4').
card_rarity('mountain'/'C14', 'Basic Land').
card_artist('mountain'/'C14', 'Andreas Rocha').
card_number('mountain'/'C14', '333').
card_multiverse_id('mountain'/'C14', '389604').

card_in_set('mulldrifter', 'C14').
card_original_type('mulldrifter'/'C14', 'Creature — Elemental').
card_original_text('mulldrifter'/'C14', 'Flying\nWhen Mulldrifter enters the battlefield, draw two cards.\nEvoke {2}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('mulldrifter'/'C14', 'mulldrifter').
card_uid('mulldrifter'/'C14', 'C14:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'C14', 'Common').
card_artist('mulldrifter'/'C14', 'Eric Fortune').
card_number('mulldrifter'/'C14', '118').
card_multiverse_id('mulldrifter'/'C14', '389605').

card_in_set('mutilate', 'C14').
card_original_type('mutilate'/'C14', 'Sorcery').
card_original_text('mutilate'/'C14', 'All creatures get -1/-1 until end of turn for each Swamp you control.').
card_image_name('mutilate'/'C14', 'mutilate').
card_uid('mutilate'/'C14', 'C14:Mutilate:mutilate').
card_rarity('mutilate'/'C14', 'Rare').
card_artist('mutilate'/'C14', 'Tyler Jacobson').
card_number('mutilate'/'C14', '150').
card_flavor_text('mutilate'/'C14', '\"It\'s only torture if you\'re strong enough to survive. Otherwise, it\'s a simple, gruesome death. I\'m happy either way.\"\n—Liliana Vess').
card_multiverse_id('mutilate'/'C14', '389606').

card_in_set('mycosynth wellspring', 'C14').
card_original_type('mycosynth wellspring'/'C14', 'Artifact').
card_original_text('mycosynth wellspring'/'C14', 'When Mycosynth Wellspring enters the battlefield or is put into a graveyard from the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('mycosynth wellspring'/'C14', 'mycosynth wellspring').
card_uid('mycosynth wellspring'/'C14', 'C14:Mycosynth Wellspring:mycosynth wellspring').
card_rarity('mycosynth wellspring'/'C14', 'Common').
card_artist('mycosynth wellspring'/'C14', 'David Rapoza').
card_number('mycosynth wellspring'/'C14', '253').
card_flavor_text('mycosynth wellspring'/'C14', 'The oil created the mycosynth. The mycosynth created New Phyrexia.').
card_multiverse_id('mycosynth wellspring'/'C14', '389607').

card_in_set('myr battlesphere', 'C14').
card_original_type('myr battlesphere'/'C14', 'Artifact Creature — Myr Construct').
card_original_text('myr battlesphere'/'C14', 'When Myr Battlesphere enters the battlefield, put four 1/1 colorless Myr artifact creature tokens onto the battlefield.\nWhenever Myr Battlesphere attacks, you may tap X untapped Myr you control. If you do, Myr Battlesphere gets +X/+0 until end of turn and deals X damage to defending player.').
card_image_name('myr battlesphere'/'C14', 'myr battlesphere').
card_uid('myr battlesphere'/'C14', 'C14:Myr Battlesphere:myr battlesphere').
card_rarity('myr battlesphere'/'C14', 'Rare').
card_artist('myr battlesphere'/'C14', 'Franz Vohwinkel').
card_number('myr battlesphere'/'C14', '254').
card_multiverse_id('myr battlesphere'/'C14', '389608').

card_in_set('myr retriever', 'C14').
card_original_type('myr retriever'/'C14', 'Artifact Creature — Myr').
card_original_text('myr retriever'/'C14', 'When Myr Retriever dies, return another target artifact card from your graveyard to your hand.').
card_image_name('myr retriever'/'C14', 'myr retriever').
card_uid('myr retriever'/'C14', 'C14:Myr Retriever:myr retriever').
card_rarity('myr retriever'/'C14', 'Uncommon').
card_artist('myr retriever'/'C14', 'Trevor Hairsine').
card_number('myr retriever'/'C14', '255').
card_flavor_text('myr retriever'/'C14', 'Mephidross gives up treasure easily . . . as long as you take its place.').
card_multiverse_id('myr retriever'/'C14', '389609').

card_in_set('myr sire', 'C14').
card_original_type('myr sire'/'C14', 'Artifact Creature — Myr').
card_original_text('myr sire'/'C14', 'When Myr Sire dies, put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_image_name('myr sire'/'C14', 'myr sire').
card_uid('myr sire'/'C14', 'C14:Myr Sire:myr sire').
card_rarity('myr sire'/'C14', 'Common').
card_artist('myr sire'/'C14', 'Jaime Jones').
card_number('myr sire'/'C14', '256').
card_flavor_text('myr sire'/'C14', 'For the Phyrexians, death is not an end, nor a one-time occurrence.').
card_multiverse_id('myr sire'/'C14', '389610').

card_in_set('myriad landscape', 'C14').
card_original_type('myriad landscape'/'C14', 'Land').
card_original_text('myriad landscape'/'C14', 'Myriad Landscape enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Myriad Landscape: Search your library for up to two basic land cards that share a land type, put them onto the battlefield tapped, then shuffle your library.').
card_first_print('myriad landscape', 'C14').
card_image_name('myriad landscape'/'C14', 'myriad landscape').
card_uid('myriad landscape'/'C14', 'C14:Myriad Landscape:myriad landscape').
card_rarity('myriad landscape'/'C14', 'Uncommon').
card_artist('myriad landscape'/'C14', 'Richard Wright').
card_number('myriad landscape'/'C14', '61').
card_multiverse_id('myriad landscape'/'C14', '389611').

card_in_set('nahiri, the lithomancer', 'C14').
card_original_type('nahiri, the lithomancer'/'C14', 'Planeswalker — Nahiri').
card_original_text('nahiri, the lithomancer'/'C14', '+2: Put a 1/1 white Kor Soldier creature token onto the battlefield. You may attach an Equipment you control to it.\n−2: You may put an Equipment card from your hand or graveyard onto the battlefield.\n−10: Put a colorless Equipment artifact token named Stoneforged Blade onto the battlefield. It has indestructible, \"Equipped creature gets +5/+5 and has double strike,\" and equip {0}.\nNahiri, the Lithomancer can be your commander.').
card_first_print('nahiri, the lithomancer', 'C14').
card_image_name('nahiri, the lithomancer'/'C14', 'nahiri, the lithomancer').
card_uid('nahiri, the lithomancer'/'C14', 'C14:Nahiri, the Lithomancer:nahiri, the lithomancer').
card_rarity('nahiri, the lithomancer'/'C14', 'Mythic Rare').
card_artist('nahiri, the lithomancer'/'C14', 'Eric Deschamps').
card_number('nahiri, the lithomancer'/'C14', '10').
card_multiverse_id('nahiri, the lithomancer'/'C14', '389612').

card_in_set('nantuko shade', 'C14').
card_original_type('nantuko shade'/'C14', 'Creature — Insect Shade').
card_original_text('nantuko shade'/'C14', '{B}: Nantuko Shade gets +1/+1 until end of turn.').
card_image_name('nantuko shade'/'C14', 'nantuko shade').
card_uid('nantuko shade'/'C14', 'C14:Nantuko Shade:nantuko shade').
card_rarity('nantuko shade'/'C14', 'Rare').
card_artist('nantuko shade'/'C14', 'Brian Snõddy').
card_number('nantuko shade'/'C14', '151').
card_flavor_text('nantuko shade'/'C14', 'In life, the nantuko study nature by revering it. In death, they study nature by disemboweling it.').
card_multiverse_id('nantuko shade'/'C14', '389613').

card_in_set('necromantic selection', 'C14').
card_original_type('necromantic selection'/'C14', 'Sorcery').
card_original_text('necromantic selection'/'C14', 'Destroy all creatures, then return a creature card put into a graveyard this way to the battlefield under your control. It\'s a black Zombie in addition to its other colors and types. Exile Necromantic Selection.').
card_first_print('necromantic selection', 'C14').
card_image_name('necromantic selection'/'C14', 'necromantic selection').
card_uid('necromantic selection'/'C14', 'C14:Necromantic Selection:necromantic selection').
card_rarity('necromantic selection'/'C14', 'Rare').
card_artist('necromantic selection'/'C14', 'Dave Kendall').
card_number('necromantic selection'/'C14', '26').
card_flavor_text('necromantic selection'/'C14', 'It shambled from the cloud of ash and bone dust, undeterred by its own extinction.').
card_multiverse_id('necromantic selection'/'C14', '389614').

card_in_set('nekrataal', 'C14').
card_original_type('nekrataal'/'C14', 'Creature — Human Assassin').
card_original_text('nekrataal'/'C14', 'First strike\nWhen Nekrataal enters the battlefield, destroy target nonartifact, nonblack creature. That creature can\'t be regenerated.').
card_image_name('nekrataal'/'C14', 'nekrataal').
card_uid('nekrataal'/'C14', 'C14:Nekrataal:nekrataal').
card_rarity('nekrataal'/'C14', 'Uncommon').
card_artist('nekrataal'/'C14', 'Christopher Moeller').
card_number('nekrataal'/'C14', '152').
card_flavor_text('nekrataal'/'C14', 'His victims don\'t have time to feel despair.').
card_multiverse_id('nekrataal'/'C14', '389615').

card_in_set('nevinyrral\'s disk', 'C14').
card_original_type('nevinyrral\'s disk'/'C14', 'Artifact').
card_original_text('nevinyrral\'s disk'/'C14', 'Nevinyrral\'s Disk enters the battlefield tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_image_name('nevinyrral\'s disk'/'C14', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'C14', 'C14:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'C14', 'Rare').
card_artist('nevinyrral\'s disk'/'C14', 'Steve Argyle').
card_number('nevinyrral\'s disk'/'C14', '257').
card_multiverse_id('nevinyrral\'s disk'/'C14', '389616').

card_in_set('nomads\' assembly', 'C14').
card_original_type('nomads\' assembly'/'C14', 'Sorcery').
card_original_text('nomads\' assembly'/'C14', 'Put a 1/1 white Kor Soldier creature token onto the battlefield for each creature you control.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_image_name('nomads\' assembly'/'C14', 'nomads\' assembly').
card_uid('nomads\' assembly'/'C14', 'C14:Nomads\' Assembly:nomads\' assembly').
card_rarity('nomads\' assembly'/'C14', 'Rare').
card_artist('nomads\' assembly'/'C14', 'Erica Yang').
card_number('nomads\' assembly'/'C14', '82').
card_multiverse_id('nomads\' assembly'/'C14', '389617').

card_in_set('ob nixilis of the black oath', 'C14').
card_original_type('ob nixilis of the black oath'/'C14', 'Planeswalker — Nixilis').
card_original_text('ob nixilis of the black oath'/'C14', '+2: Each opponent loses 1 life. You gain life equal to the life lost this way.\n−2: Put a 5/5 black Demon creature token with flying onto the battlefield. You lose 2 life.\n−8: You get an emblem with \"{1}{B}, Sacrifice a creature: You gain X life and draw X cards, where X is the sacrificed creature\'s power.\"\nOb Nixilis of the Black Oath can be your commander.').
card_first_print('ob nixilis of the black oath', 'C14').
card_image_name('ob nixilis of the black oath'/'C14', 'ob nixilis of the black oath').
card_uid('ob nixilis of the black oath'/'C14', 'C14:Ob Nixilis of the Black Oath:ob nixilis of the black oath').
card_rarity('ob nixilis of the black oath'/'C14', 'Mythic Rare').
card_artist('ob nixilis of the black oath'/'C14', 'Daarken').
card_number('ob nixilis of the black oath'/'C14', '27').
card_multiverse_id('ob nixilis of the black oath'/'C14', '389618').

card_in_set('oblation', 'C14').
card_original_type('oblation'/'C14', 'Instant').
card_original_text('oblation'/'C14', 'The owner of target nonland permanent shuffles it into his or her library, then draws two cards.').
card_image_name('oblation'/'C14', 'oblation').
card_uid('oblation'/'C14', 'C14:Oblation:oblation').
card_rarity('oblation'/'C14', 'Rare').
card_artist('oblation'/'C14', 'Doug Chaffee').
card_number('oblation'/'C14', '83').
card_flavor_text('oblation'/'C14', '\"A richer people could give more but they could never give as much.\"').
card_multiverse_id('oblation'/'C14', '389619').

card_in_set('oran-rief, the vastwood', 'C14').
card_original_type('oran-rief, the vastwood'/'C14', 'Land').
card_original_text('oran-rief, the vastwood'/'C14', 'Oran-Rief, the Vastwood enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{T}: Put a +1/+1 counter on each green creature that entered the battlefield this turn.').
card_image_name('oran-rief, the vastwood'/'C14', 'oran-rief, the vastwood').
card_uid('oran-rief, the vastwood'/'C14', 'C14:Oran-Rief, the Vastwood:oran-rief, the vastwood').
card_rarity('oran-rief, the vastwood'/'C14', 'Rare').
card_artist('oran-rief, the vastwood'/'C14', 'Mike Bierek').
card_number('oran-rief, the vastwood'/'C14', '305').
card_multiverse_id('oran-rief, the vastwood'/'C14', '389620').

card_in_set('overrun', 'C14').
card_original_type('overrun'/'C14', 'Sorcery').
card_original_text('overrun'/'C14', 'Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('overrun'/'C14', 'overrun').
card_uid('overrun'/'C14', 'C14:Overrun:overrun').
card_rarity('overrun'/'C14', 'Uncommon').
card_artist('overrun'/'C14', 'Carl Critchlow').
card_number('overrun'/'C14', '207').
card_flavor_text('overrun'/'C14', 'Nature doesn\'t walk.').
card_multiverse_id('overrun'/'C14', '389621').

card_in_set('overseer of the damned', 'C14').
card_original_type('overseer of the damned'/'C14', 'Creature — Demon').
card_original_text('overseer of the damned'/'C14', 'Flying\nWhen Overseer of the Damned enters the battlefield, you may destroy target creature.\nWhenever a nontoken creature an opponent controls dies, put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_first_print('overseer of the damned', 'C14').
card_image_name('overseer of the damned'/'C14', 'overseer of the damned').
card_uid('overseer of the damned'/'C14', 'C14:Overseer of the Damned:overseer of the damned').
card_rarity('overseer of the damned'/'C14', 'Rare').
card_artist('overseer of the damned'/'C14', 'rk post').
card_number('overseer of the damned'/'C14', '28').
card_multiverse_id('overseer of the damned'/'C14', '389622').

card_in_set('overwhelming stampede', 'C14').
card_original_type('overwhelming stampede'/'C14', 'Sorcery').
card_original_text('overwhelming stampede'/'C14', 'Until end of turn, creatures you control gain trample and get +X/+X, where X is the greatest power among creatures you control.').
card_image_name('overwhelming stampede'/'C14', 'overwhelming stampede').
card_uid('overwhelming stampede'/'C14', 'C14:Overwhelming Stampede:overwhelming stampede').
card_rarity('overwhelming stampede'/'C14', 'Rare').
card_artist('overwhelming stampede'/'C14', 'Steven Belledin').
card_number('overwhelming stampede'/'C14', '208').
card_multiverse_id('overwhelming stampede'/'C14', '389623').

card_in_set('palladium myr', 'C14').
card_original_type('palladium myr'/'C14', 'Artifact Creature — Myr').
card_original_text('palladium myr'/'C14', '{T}: Add {2} to your mana pool.').
card_image_name('palladium myr'/'C14', 'palladium myr').
card_uid('palladium myr'/'C14', 'C14:Palladium Myr:palladium myr').
card_rarity('palladium myr'/'C14', 'Uncommon').
card_artist('palladium myr'/'C14', 'Alan Pollack').
card_number('palladium myr'/'C14', '258').
card_flavor_text('palladium myr'/'C14', 'The myr are like the Glimmervoid: blank canvases on which to build grand creations.').
card_multiverse_id('palladium myr'/'C14', '389624').

card_in_set('panic spellbomb', 'C14').
card_original_type('panic spellbomb'/'C14', 'Artifact').
card_original_text('panic spellbomb'/'C14', '{T}, Sacrifice Panic Spellbomb: Target creature can\'t block this turn.\nWhen Panic Spellbomb is put into a graveyard from the battlefield, you may pay {R}. If you do, draw a card.').
card_image_name('panic spellbomb'/'C14', 'panic spellbomb').
card_uid('panic spellbomb'/'C14', 'C14:Panic Spellbomb:panic spellbomb').
card_rarity('panic spellbomb'/'C14', 'Common').
card_artist('panic spellbomb'/'C14', 'Franz Vohwinkel').
card_number('panic spellbomb'/'C14', '259').
card_multiverse_id('panic spellbomb'/'C14', '389625').

card_in_set('pearl medallion', 'C14').
card_original_type('pearl medallion'/'C14', 'Artifact').
card_original_text('pearl medallion'/'C14', 'White spells you cast cost {1} less to cast.').
card_image_name('pearl medallion'/'C14', 'pearl medallion').
card_uid('pearl medallion'/'C14', 'C14:Pearl Medallion:pearl medallion').
card_rarity('pearl medallion'/'C14', 'Rare').
card_artist('pearl medallion'/'C14', 'Daniel Ljunggren').
card_number('pearl medallion'/'C14', '260').
card_multiverse_id('pearl medallion'/'C14', '389626').

card_in_set('pentavus', 'C14').
card_original_type('pentavus'/'C14', 'Artifact Creature — Construct').
card_original_text('pentavus'/'C14', 'Pentavus enters the battlefield with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Pentavus: Put a 1/1 colorless Pentavite artifact creature token with flying onto the battlefield.\n{1}, Sacrifice a Pentavite: Put a +1/+1 counter on Pentavus.').
card_image_name('pentavus'/'C14', 'pentavus').
card_uid('pentavus'/'C14', 'C14:Pentavus:pentavus').
card_rarity('pentavus'/'C14', 'Rare').
card_artist('pentavus'/'C14', 'Greg Staples').
card_number('pentavus'/'C14', '261').
card_multiverse_id('pentavus'/'C14', '389627').

card_in_set('pestilence demon', 'C14').
card_original_type('pestilence demon'/'C14', 'Creature — Demon').
card_original_text('pestilence demon'/'C14', 'Flying\n{B}: Pestilence Demon deals 1 damage to each creature and each player.').
card_image_name('pestilence demon'/'C14', 'pestilence demon').
card_uid('pestilence demon'/'C14', 'C14:Pestilence Demon:pestilence demon').
card_rarity('pestilence demon'/'C14', 'Rare').
card_artist('pestilence demon'/'C14', 'Justin Sweet').
card_number('pestilence demon'/'C14', '153').
card_flavor_text('pestilence demon'/'C14', '\"I have schemed too long to be supplanted by dead gods. If I cannot have this world, no one can.\"').
card_multiverse_id('pestilence demon'/'C14', '389628').

card_in_set('phyrexia\'s core', 'C14').
card_original_type('phyrexia\'s core'/'C14', 'Land').
card_original_text('phyrexia\'s core'/'C14', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice an artifact: You gain 1 life.').
card_image_name('phyrexia\'s core'/'C14', 'phyrexia\'s core').
card_uid('phyrexia\'s core'/'C14', 'C14:Phyrexia\'s Core:phyrexia\'s core').
card_rarity('phyrexia\'s core'/'C14', 'Uncommon').
card_artist('phyrexia\'s core'/'C14', 'Franz Vohwinkel').
card_number('phyrexia\'s core'/'C14', '306').
card_flavor_text('phyrexia\'s core'/'C14', '\"So even the heart of our world has succumbed.\"\n—Koth of the Hammer').
card_multiverse_id('phyrexia\'s core'/'C14', '389629').

card_in_set('phyrexian gargantua', 'C14').
card_original_type('phyrexian gargantua'/'C14', 'Creature — Horror').
card_original_text('phyrexian gargantua'/'C14', 'When Phyrexian Gargantua enters the battlefield, you draw two cards and you lose 2 life.').
card_image_name('phyrexian gargantua'/'C14', 'phyrexian gargantua').
card_uid('phyrexian gargantua'/'C14', 'C14:Phyrexian Gargantua:phyrexian gargantua').
card_rarity('phyrexian gargantua'/'C14', 'Uncommon').
card_artist('phyrexian gargantua'/'C14', 'Igor Kieryluk').
card_number('phyrexian gargantua'/'C14', '154').
card_flavor_text('phyrexian gargantua'/'C14', 'Other Phyrexians have nightmares about the gargantua.').
card_multiverse_id('phyrexian gargantua'/'C14', '389630').

card_in_set('phyrexian ingester', 'C14').
card_original_type('phyrexian ingester'/'C14', 'Creature — Beast').
card_original_text('phyrexian ingester'/'C14', 'Imprint — When Phyrexian Ingester enters the battlefield, you may exile target nontoken creature.\nPhyrexian Ingester gets +X/+Y, where X is the exiled creature card\'s power and Y is its toughness.').
card_image_name('phyrexian ingester'/'C14', 'phyrexian ingester').
card_uid('phyrexian ingester'/'C14', 'C14:Phyrexian Ingester:phyrexian ingester').
card_rarity('phyrexian ingester'/'C14', 'Rare').
card_artist('phyrexian ingester'/'C14', 'Chris Rahn').
card_number('phyrexian ingester'/'C14', '119').
card_multiverse_id('phyrexian ingester'/'C14', '389631').

card_in_set('pilgrim\'s eye', 'C14').
card_original_type('pilgrim\'s eye'/'C14', 'Artifact Creature — Thopter').
card_original_text('pilgrim\'s eye'/'C14', 'Flying\nWhen Pilgrim\'s Eye enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('pilgrim\'s eye'/'C14', 'pilgrim\'s eye').
card_uid('pilgrim\'s eye'/'C14', 'C14:Pilgrim\'s Eye:pilgrim\'s eye').
card_rarity('pilgrim\'s eye'/'C14', 'Common').
card_artist('pilgrim\'s eye'/'C14', 'Dan Scott').
card_number('pilgrim\'s eye'/'C14', '262').
card_flavor_text('pilgrim\'s eye'/'C14', 'The kor send their thopter kites to see if the land is in a welcoming mood.').
card_multiverse_id('pilgrim\'s eye'/'C14', '389632').

card_in_set('plains', 'C14').
card_original_type('plains'/'C14', 'Basic Land — Plains').
card_original_text('plains'/'C14', 'W').
card_image_name('plains'/'C14', 'plains1').
card_uid('plains'/'C14', 'C14:Plains:plains1').
card_rarity('plains'/'C14', 'Basic Land').
card_artist('plains'/'C14', 'John Avon').
card_number('plains'/'C14', '318').
card_multiverse_id('plains'/'C14', '389633').

card_in_set('plains', 'C14').
card_original_type('plains'/'C14', 'Basic Land — Plains').
card_original_text('plains'/'C14', 'W').
card_image_name('plains'/'C14', 'plains2').
card_uid('plains'/'C14', 'C14:Plains:plains2').
card_rarity('plains'/'C14', 'Basic Land').
card_artist('plains'/'C14', 'Véronique Meignaud').
card_number('plains'/'C14', '319').
card_multiverse_id('plains'/'C14', '389635').

card_in_set('plains', 'C14').
card_original_type('plains'/'C14', 'Basic Land — Plains').
card_original_text('plains'/'C14', 'W').
card_image_name('plains'/'C14', 'plains3').
card_uid('plains'/'C14', 'C14:Plains:plains3').
card_rarity('plains'/'C14', 'Basic Land').
card_artist('plains'/'C14', 'Jung Park').
card_number('plains'/'C14', '320').
card_multiverse_id('plains'/'C14', '389636').

card_in_set('plains', 'C14').
card_original_type('plains'/'C14', 'Basic Land — Plains').
card_original_text('plains'/'C14', 'W').
card_image_name('plains'/'C14', 'plains4').
card_uid('plains'/'C14', 'C14:Plains:plains4').
card_rarity('plains'/'C14', 'Basic Land').
card_artist('plains'/'C14', 'Vincent Proce').
card_number('plains'/'C14', '321').
card_multiverse_id('plains'/'C14', '389634').

card_in_set('polluted mire', 'C14').
card_original_type('polluted mire'/'C14', 'Land').
card_original_text('polluted mire'/'C14', 'Polluted Mire enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('polluted mire'/'C14', 'polluted mire').
card_uid('polluted mire'/'C14', 'C14:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'C14', 'Common').
card_artist('polluted mire'/'C14', 'Stephen Daniele').
card_number('polluted mire'/'C14', '307').
card_multiverse_id('polluted mire'/'C14', '389637').

card_in_set('pongify', 'C14').
card_original_type('pongify'/'C14', 'Instant').
card_original_text('pongify'/'C14', 'Destroy target creature. It can\'t be regenerated. Its controller puts a 3/3 green Ape creature token onto the battlefield.').
card_image_name('pongify'/'C14', 'pongify').
card_uid('pongify'/'C14', 'C14:Pongify:pongify').
card_rarity('pongify'/'C14', 'Uncommon').
card_artist('pongify'/'C14', 'Heather Hudson').
card_number('pongify'/'C14', '120').
card_flavor_text('pongify'/'C14', 'Some spellcrafting mistakes go on to become spells of their own.').
card_multiverse_id('pongify'/'C14', '389638').

card_in_set('pontiff of blight', 'C14').
card_original_type('pontiff of blight'/'C14', 'Creature — Zombie Cleric').
card_original_text('pontiff of blight'/'C14', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nOther creatures you control have extort. (If a creature has multiple instances of extort, each triggers separately.)').
card_image_name('pontiff of blight'/'C14', 'pontiff of blight').
card_uid('pontiff of blight'/'C14', 'C14:Pontiff of Blight:pontiff of blight').
card_rarity('pontiff of blight'/'C14', 'Rare').
card_artist('pontiff of blight'/'C14', 'Seb McKinnon').
card_number('pontiff of blight'/'C14', '155').
card_multiverse_id('pontiff of blight'/'C14', '389639').

card_in_set('praetor\'s counsel', 'C14').
card_original_type('praetor\'s counsel'/'C14', 'Sorcery').
card_original_text('praetor\'s counsel'/'C14', 'Return all cards from your graveyard to your hand. Exile Praetor\'s Counsel. You have no maximum hand size for the rest of the game.').
card_image_name('praetor\'s counsel'/'C14', 'praetor\'s counsel').
card_uid('praetor\'s counsel'/'C14', 'C14:Praetor\'s Counsel:praetor\'s counsel').
card_rarity('praetor\'s counsel'/'C14', 'Mythic Rare').
card_artist('praetor\'s counsel'/'C14', 'Daarken').
card_number('praetor\'s counsel'/'C14', '209').
card_flavor_text('praetor\'s counsel'/'C14', 'As the Phyrexian contagion corroded Karn\'s body, the praetors whispered psalms to corrupt his mind.').
card_multiverse_id('praetor\'s counsel'/'C14', '389640').

card_in_set('predator, flagship', 'C14').
card_original_type('predator, flagship'/'C14', 'Legendary Artifact').
card_original_text('predator, flagship'/'C14', '{2}: Target creature gains flying until end of turn.\n{5}, {T}: Destroy target creature with flying.').
card_image_name('predator, flagship'/'C14', 'predator, flagship').
card_uid('predator, flagship'/'C14', 'C14:Predator, Flagship:predator, flagship').
card_rarity('predator, flagship'/'C14', 'Rare').
card_artist('predator, flagship'/'C14', 'Mark Tedin').
card_number('predator, flagship'/'C14', '263').
card_flavor_text('predator, flagship'/'C14', '\"The scourge of Skyshroud is airborne once more.\"\n—Oracle en-Vec').
card_multiverse_id('predator, flagship'/'C14', '389641').

card_in_set('priest of titania', 'C14').
card_original_type('priest of titania'/'C14', 'Creature — Elf Druid').
card_original_text('priest of titania'/'C14', '{T}: Add {G} to your mana pool for each Elf on the battlefield.').
card_image_name('priest of titania'/'C14', 'priest of titania').
card_uid('priest of titania'/'C14', 'C14:Priest of Titania:priest of titania').
card_rarity('priest of titania'/'C14', 'Common').
card_artist('priest of titania'/'C14', 'Rebecca Guay').
card_number('priest of titania'/'C14', '210').
card_flavor_text('priest of titania'/'C14', 'Titania rewards all who honor the forest by making them a living part of it.').
card_multiverse_id('priest of titania'/'C14', '389642').

card_in_set('primordial sage', 'C14').
card_original_type('primordial sage'/'C14', 'Creature — Spirit').
card_original_text('primordial sage'/'C14', 'Whenever you cast a creature spell, you may draw a card.').
card_image_name('primordial sage'/'C14', 'primordial sage').
card_uid('primordial sage'/'C14', 'C14:Primordial Sage:primordial sage').
card_rarity('primordial sage'/'C14', 'Rare').
card_artist('primordial sage'/'C14', 'Justin Sweet').
card_number('primordial sage'/'C14', '211').
card_flavor_text('primordial sage'/'C14', 'For each creature that arrives in its audience, the sage imparts another piece of ancient wisdom for all to hear.').
card_multiverse_id('primordial sage'/'C14', '389643').

card_in_set('pristine talisman', 'C14').
card_original_type('pristine talisman'/'C14', 'Artifact').
card_original_text('pristine talisman'/'C14', '{T}: Add {1} to your mana pool. You gain 1 life.').
card_image_name('pristine talisman'/'C14', 'pristine talisman').
card_uid('pristine talisman'/'C14', 'C14:Pristine Talisman:pristine talisman').
card_rarity('pristine talisman'/'C14', 'Common').
card_artist('pristine talisman'/'C14', 'Matt Cavotta').
card_number('pristine talisman'/'C14', '264').
card_flavor_text('pristine talisman'/'C14', '\"Tools and artisans can be destroyed, but the act of creation is inviolate.\"\n—Elspeth Tirel').
card_multiverse_id('pristine talisman'/'C14', '389644').

card_in_set('profane command', 'C14').
card_original_type('profane command'/'C14', 'Sorcery').
card_original_text('profane command'/'C14', 'Choose two — \n• Target player loses X life. \n• Return target creature card with converted mana cost X or less from your graveyard to the battlefield.\n• Target creature gets -X/-X until end of turn.\n• Up to X target creatures gain fear until end of turn. (They can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('profane command'/'C14', 'profane command').
card_uid('profane command'/'C14', 'C14:Profane Command:profane command').
card_rarity('profane command'/'C14', 'Rare').
card_artist('profane command'/'C14', 'Wayne England').
card_number('profane command'/'C14', '156').
card_multiverse_id('profane command'/'C14', '389645').

card_in_set('promise of power', 'C14').
card_original_type('promise of power'/'C14', 'Sorcery').
card_original_text('promise of power'/'C14', 'Choose one — \n• You draw five cards and you lose 5 life.\n• Put an X/X black Demon creature token with flying onto the battlefield, where X is the number of cards in your hand as the token enters the battlefield.\nEntwine {4} (Choose both if you pay the entwine cost.)').
card_image_name('promise of power'/'C14', 'promise of power').
card_uid('promise of power'/'C14', 'C14:Promise of Power:promise of power').
card_rarity('promise of power'/'C14', 'Rare').
card_artist('promise of power'/'C14', 'Kev Walker').
card_number('promise of power'/'C14', '157').
card_multiverse_id('promise of power'/'C14', '389646').

card_in_set('rampaging baloths', 'C14').
card_original_type('rampaging baloths'/'C14', 'Creature — Beast').
card_original_text('rampaging baloths'/'C14', 'Trample\nLandfall — Whenever a land enters the battlefield under your control, you may put a 4/4 green Beast creature token onto the battlefield.').
card_image_name('rampaging baloths'/'C14', 'rampaging baloths').
card_uid('rampaging baloths'/'C14', 'C14:Rampaging Baloths:rampaging baloths').
card_rarity('rampaging baloths'/'C14', 'Mythic Rare').
card_artist('rampaging baloths'/'C14', 'Steve Prescott').
card_number('rampaging baloths'/'C14', '212').
card_flavor_text('rampaging baloths'/'C14', '\"When the land is angry, so are they.\"\n—Nissa Revane').
card_multiverse_id('rampaging baloths'/'C14', '389647').

card_in_set('raving dead', 'C14').
card_original_type('raving dead'/'C14', 'Creature — Zombie').
card_original_text('raving dead'/'C14', 'Deathtouch\nAt the beginning of combat on your turn, choose an opponent at random. Raving Dead attacks that player this combat if able.\nWhenever Raving Dead deals combat damage to a player, that player loses half his or her life, rounded down.').
card_first_print('raving dead', 'C14').
card_image_name('raving dead'/'C14', 'raving dead').
card_uid('raving dead'/'C14', 'C14:Raving Dead:raving dead').
card_rarity('raving dead'/'C14', 'Rare').
card_artist('raving dead'/'C14', 'Daarken').
card_number('raving dead'/'C14', '29').
card_multiverse_id('raving dead'/'C14', '389648').

card_in_set('read the bones', 'C14').
card_original_type('read the bones'/'C14', 'Sorcery').
card_original_text('read the bones'/'C14', 'Scry 2, then draw two cards. You lose 2 life. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('read the bones'/'C14', 'read the bones').
card_uid('read the bones'/'C14', 'C14:Read the Bones:read the bones').
card_rarity('read the bones'/'C14', 'Common').
card_artist('read the bones'/'C14', 'Lars Grant-West').
card_number('read the bones'/'C14', '158').
card_flavor_text('read the bones'/'C14', 'The dead know lessons the living haven\'t learned.').
card_multiverse_id('read the bones'/'C14', '389649').

card_in_set('reaper from the abyss', 'C14').
card_original_type('reaper from the abyss'/'C14', 'Creature — Demon').
card_original_text('reaper from the abyss'/'C14', 'Flying\nMorbid — At the beginning of each end step, if a creature died this turn, destroy target non-Demon creature.').
card_image_name('reaper from the abyss'/'C14', 'reaper from the abyss').
card_uid('reaper from the abyss'/'C14', 'C14:Reaper from the Abyss:reaper from the abyss').
card_rarity('reaper from the abyss'/'C14', 'Mythic Rare').
card_artist('reaper from the abyss'/'C14', 'Matt Stewart').
card_number('reaper from the abyss'/'C14', '159').
card_flavor_text('reaper from the abyss'/'C14', '\"Avacyn has deserted you. I welcome your devotion in her stead.\"').
card_multiverse_id('reaper from the abyss'/'C14', '389650').

card_in_set('reclamation sage', 'C14').
card_original_type('reclamation sage'/'C14', 'Creature — Elf Shaman').
card_original_text('reclamation sage'/'C14', 'When Reclamation Sage enters the battlefield, you may destroy target artifact or enchantment.').
card_image_name('reclamation sage'/'C14', 'reclamation sage').
card_uid('reclamation sage'/'C14', 'C14:Reclamation Sage:reclamation sage').
card_rarity('reclamation sage'/'C14', 'Uncommon').
card_artist('reclamation sage'/'C14', 'Christopher Moeller').
card_number('reclamation sage'/'C14', '213').
card_flavor_text('reclamation sage'/'C14', '\"What was once formed by masons, shaped by smiths, or given life by mages, I will return to the embrace of the earth.\"').
card_multiverse_id('reclamation sage'/'C14', '389651').

card_in_set('reef worm', 'C14').
card_original_type('reef worm'/'C14', 'Creature — Worm').
card_original_text('reef worm'/'C14', 'When Reef Worm dies, put a 3/3 blue Fish creature token onto the battlefield with \"When this creature dies, put a 6/6 blue Whale creature token onto the battlefield with ‘When this creature dies, put a 9/9 blue Kraken creature token onto the battlefield.\'\"').
card_first_print('reef worm', 'C14').
card_image_name('reef worm'/'C14', 'reef worm').
card_uid('reef worm'/'C14', 'C14:Reef Worm:reef worm').
card_rarity('reef worm'/'C14', 'Rare').
card_artist('reef worm'/'C14', 'Dan Scott').
card_number('reef worm'/'C14', '16').
card_multiverse_id('reef worm'/'C14', '389652').

card_in_set('reliquary tower', 'C14').
card_original_type('reliquary tower'/'C14', 'Land').
card_original_text('reliquary tower'/'C14', 'You have no maximum hand size.\n{T}: Add {1} to your mana pool.').
card_image_name('reliquary tower'/'C14', 'reliquary tower').
card_uid('reliquary tower'/'C14', 'C14:Reliquary Tower:reliquary tower').
card_rarity('reliquary tower'/'C14', 'Uncommon').
card_artist('reliquary tower'/'C14', 'Jesper Ejsing').
card_number('reliquary tower'/'C14', '308').
card_flavor_text('reliquary tower'/'C14', 'Once guarded by the Knights of the Reliquary, the tower stands now protected only by its own remoteness, its dusty treasures open to plunder by anyone.').
card_multiverse_id('reliquary tower'/'C14', '389653').

card_in_set('remote isle', 'C14').
card_original_type('remote isle'/'C14', 'Land').
card_original_text('remote isle'/'C14', 'Remote Isle enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('remote isle'/'C14', 'remote isle').
card_uid('remote isle'/'C14', 'C14:Remote Isle:remote isle').
card_rarity('remote isle'/'C14', 'Common').
card_artist('remote isle'/'C14', 'Ciruelo').
card_number('remote isle'/'C14', '309').
card_multiverse_id('remote isle'/'C14', '389654').

card_in_set('requiem angel', 'C14').
card_original_type('requiem angel'/'C14', 'Creature — Angel').
card_original_text('requiem angel'/'C14', 'Flying\nWhenever another non-Spirit creature you control dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('requiem angel'/'C14', 'requiem angel').
card_uid('requiem angel'/'C14', 'C14:Requiem Angel:requiem angel').
card_rarity('requiem angel'/'C14', 'Rare').
card_artist('requiem angel'/'C14', 'Eric Deschamps').
card_number('requiem angel'/'C14', '84').
card_flavor_text('requiem angel'/'C14', 'When angels despair, what hope can remain for mortals?').
card_multiverse_id('requiem angel'/'C14', '389655').

card_in_set('return to dust', 'C14').
card_original_type('return to dust'/'C14', 'Instant').
card_original_text('return to dust'/'C14', 'Exile target artifact or enchantment. If you cast this spell during your main phase, you may exile up to one other target artifact or enchantment.').
card_image_name('return to dust'/'C14', 'return to dust').
card_uid('return to dust'/'C14', 'C14:Return to Dust:return to dust').
card_rarity('return to dust'/'C14', 'Uncommon').
card_artist('return to dust'/'C14', 'Wayne Reynolds').
card_number('return to dust'/'C14', '85').
card_flavor_text('return to dust'/'C14', 'Some timelines forever fray, branch, and intermingle. Others end abruptly.').
card_multiverse_id('return to dust'/'C14', '389656').

card_in_set('riptide survivor', 'C14').
card_original_type('riptide survivor'/'C14', 'Creature — Human Wizard').
card_original_text('riptide survivor'/'C14', 'Morph {1}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Riptide Survivor is turned face up, discard two cards, then draw three cards.').
card_image_name('riptide survivor'/'C14', 'riptide survivor').
card_uid('riptide survivor'/'C14', 'C14:Riptide Survivor:riptide survivor').
card_rarity('riptide survivor'/'C14', 'Uncommon').
card_artist('riptide survivor'/'C14', 'Thomas M. Baxa').
card_number('riptide survivor'/'C14', '121').
card_multiverse_id('riptide survivor'/'C14', '389657').

card_in_set('rite of replication', 'C14').
card_original_type('rite of replication'/'C14', 'Sorcery').
card_original_text('rite of replication'/'C14', 'Kicker {5} (You may pay an additional {5} as you cast this spell.)\nPut a token onto the battlefield that\'s a copy of target creature. If Rite of Replication was kicked, put five of those tokens onto the battlefield instead.').
card_image_name('rite of replication'/'C14', 'rite of replication').
card_uid('rite of replication'/'C14', 'C14:Rite of Replication:rite of replication').
card_rarity('rite of replication'/'C14', 'Rare').
card_artist('rite of replication'/'C14', 'Matt Cavotta').
card_number('rite of replication'/'C14', '122').
card_multiverse_id('rite of replication'/'C14', '389658').

card_in_set('ruby medallion', 'C14').
card_original_type('ruby medallion'/'C14', 'Artifact').
card_original_text('ruby medallion'/'C14', 'Red spells you cast cost {1} less to cast.').
card_image_name('ruby medallion'/'C14', 'ruby medallion').
card_uid('ruby medallion'/'C14', 'C14:Ruby Medallion:ruby medallion').
card_rarity('ruby medallion'/'C14', 'Rare').
card_artist('ruby medallion'/'C14', 'Daniel Ljunggren').
card_number('ruby medallion'/'C14', '265').
card_multiverse_id('ruby medallion'/'C14', '389659').

card_in_set('rush of knowledge', 'C14').
card_original_type('rush of knowledge'/'C14', 'Sorcery').
card_original_text('rush of knowledge'/'C14', 'Draw cards equal to the highest converted mana cost among permanents you control.').
card_image_name('rush of knowledge'/'C14', 'rush of knowledge').
card_uid('rush of knowledge'/'C14', 'C14:Rush of Knowledge:rush of knowledge').
card_rarity('rush of knowledge'/'C14', 'Common').
card_artist('rush of knowledge'/'C14', 'Eric Peterson').
card_number('rush of knowledge'/'C14', '123').
card_flavor_text('rush of knowledge'/'C14', '\"Limitless power is glorious until you gain limitless understanding.\"\n—Ixidor, reality sculptor').
card_multiverse_id('rush of knowledge'/'C14', '389660').

card_in_set('sacred mesa', 'C14').
card_original_type('sacred mesa'/'C14', 'Enchantment').
card_original_text('sacred mesa'/'C14', 'At the beginning of your upkeep, sacrifice Sacred Mesa unless you sacrifice a Pegasus.\n{1}{W}: Put a 1/1 white Pegasus creature token with flying onto the battlefield.').
card_image_name('sacred mesa'/'C14', 'sacred mesa').
card_uid('sacred mesa'/'C14', 'C14:Sacred Mesa:sacred mesa').
card_rarity('sacred mesa'/'C14', 'Rare').
card_artist('sacred mesa'/'C14', 'Robbie Trevino').
card_number('sacred mesa'/'C14', '86').
card_multiverse_id('sacred mesa'/'C14', '389661').

card_in_set('sapphire medallion', 'C14').
card_original_type('sapphire medallion'/'C14', 'Artifact').
card_original_text('sapphire medallion'/'C14', 'Blue spells you cast cost {1} less to cast.').
card_image_name('sapphire medallion'/'C14', 'sapphire medallion').
card_uid('sapphire medallion'/'C14', 'C14:Sapphire Medallion:sapphire medallion').
card_rarity('sapphire medallion'/'C14', 'Rare').
card_artist('sapphire medallion'/'C14', 'Daniel Ljunggren').
card_number('sapphire medallion'/'C14', '266').
card_multiverse_id('sapphire medallion'/'C14', '389662').

card_in_set('scrap mastery', 'C14').
card_original_type('scrap mastery'/'C14', 'Sorcery').
card_original_text('scrap mastery'/'C14', 'Each player exiles all artifact cards from his or her graveyard, then sacrifices all artifacts he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_first_print('scrap mastery', 'C14').
card_image_name('scrap mastery'/'C14', 'scrap mastery').
card_uid('scrap mastery'/'C14', 'C14:Scrap Mastery:scrap mastery').
card_rarity('scrap mastery'/'C14', 'Rare').
card_artist('scrap mastery'/'C14', 'Dan Scott').
card_number('scrap mastery'/'C14', '38').
card_flavor_text('scrap mastery'/'C14', '\"Where a lesser mind sees junk, I see infinite potential.\"').
card_multiverse_id('scrap mastery'/'C14', '389663').

card_in_set('sea gate oracle', 'C14').
card_original_type('sea gate oracle'/'C14', 'Creature — Human Wizard').
card_original_text('sea gate oracle'/'C14', 'When Sea Gate Oracle enters the battlefield, look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.').
card_image_name('sea gate oracle'/'C14', 'sea gate oracle').
card_uid('sea gate oracle'/'C14', 'C14:Sea Gate Oracle:sea gate oracle').
card_rarity('sea gate oracle'/'C14', 'Common').
card_artist('sea gate oracle'/'C14', 'Daniel Ljunggren').
card_number('sea gate oracle'/'C14', '124').
card_flavor_text('sea gate oracle'/'C14', '\"The secret entrance should be near.\"').
card_multiverse_id('sea gate oracle'/'C14', '389664').

card_in_set('secluded steppe', 'C14').
card_original_type('secluded steppe'/'C14', 'Land').
card_original_text('secluded steppe'/'C14', 'Secluded Steppe enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'C14', 'secluded steppe').
card_uid('secluded steppe'/'C14', 'C14:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'C14', 'Common').
card_artist('secluded steppe'/'C14', 'Heather Hudson').
card_number('secluded steppe'/'C14', '310').
card_multiverse_id('secluded steppe'/'C14', '389665').

card_in_set('seer\'s sundial', 'C14').
card_original_type('seer\'s sundial'/'C14', 'Artifact').
card_original_text('seer\'s sundial'/'C14', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {2}. If you do, draw a card.').
card_image_name('seer\'s sundial'/'C14', 'seer\'s sundial').
card_uid('seer\'s sundial'/'C14', 'C14:Seer\'s Sundial:seer\'s sundial').
card_rarity('seer\'s sundial'/'C14', 'Rare').
card_artist('seer\'s sundial'/'C14', 'Franz Vohwinkel').
card_number('seer\'s sundial'/'C14', '267').
card_flavor_text('seer\'s sundial'/'C14', '\"The shadow travels toward the apex. I predict we will soon see the true measure of darkness.\"').
card_multiverse_id('seer\'s sundial'/'C14', '389666').

card_in_set('serra avatar', 'C14').
card_original_type('serra avatar'/'C14', 'Creature — Avatar').
card_original_text('serra avatar'/'C14', 'Serra Avatar\'s power and toughness are each equal to your life total.\nWhen Serra Avatar is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('serra avatar'/'C14', 'serra avatar').
card_uid('serra avatar'/'C14', 'C14:Serra Avatar:serra avatar').
card_rarity('serra avatar'/'C14', 'Mythic Rare').
card_artist('serra avatar'/'C14', 'Dermot Power').
card_number('serra avatar'/'C14', '87').
card_flavor_text('serra avatar'/'C14', '\"Serra isn\'t dead. She lives on through me.\"').
card_multiverse_id('serra avatar'/'C14', '389667').

card_in_set('shaper parasite', 'C14').
card_original_type('shaper parasite'/'C14', 'Creature — Illusion').
card_original_text('shaper parasite'/'C14', 'Morph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Shaper Parasite is turned face up, target creature gets +2/-2 or -2/+2 until end of turn.').
card_image_name('shaper parasite'/'C14', 'shaper parasite').
card_uid('shaper parasite'/'C14', 'C14:Shaper Parasite:shaper parasite').
card_rarity('shaper parasite'/'C14', 'Common').
card_artist('shaper parasite'/'C14', 'rk post').
card_number('shaper parasite'/'C14', '125').
card_multiverse_id('shaper parasite'/'C14', '389668').

card_in_set('shriekmaw', 'C14').
card_original_type('shriekmaw'/'C14', 'Creature — Elemental').
card_original_text('shriekmaw'/'C14', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen Shriekmaw enters the battlefield, destroy target nonartifact, nonblack creature.\nEvoke {1}{B} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('shriekmaw'/'C14', 'shriekmaw').
card_uid('shriekmaw'/'C14', 'C14:Shriekmaw:shriekmaw').
card_rarity('shriekmaw'/'C14', 'Uncommon').
card_artist('shriekmaw'/'C14', 'Steve Prescott').
card_number('shriekmaw'/'C14', '160').
card_multiverse_id('shriekmaw'/'C14', '389669').

card_in_set('siege behemoth', 'C14').
card_original_type('siege behemoth'/'C14', 'Creature — Beast').
card_original_text('siege behemoth'/'C14', 'Hexproof\nAs long as Siege Behemoth is attacking, for each creature you control, you may have that creature assign its combat damage as though it weren\'t blocked.').
card_first_print('siege behemoth', 'C14').
card_image_name('siege behemoth'/'C14', 'siege behemoth').
card_uid('siege behemoth'/'C14', 'C14:Siege Behemoth:siege behemoth').
card_rarity('siege behemoth'/'C14', 'Rare').
card_artist('siege behemoth'/'C14', 'Jason A. Engle').
card_number('siege behemoth'/'C14', '46').
card_flavor_text('siege behemoth'/'C14', '\"Hold the line, hold the li—\"\n—General Srok, last words').
card_multiverse_id('siege behemoth'/'C14', '389670').

card_in_set('sign in blood', 'C14').
card_original_type('sign in blood'/'C14', 'Sorcery').
card_original_text('sign in blood'/'C14', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'C14', 'sign in blood').
card_uid('sign in blood'/'C14', 'C14:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'C14', 'Common').
card_artist('sign in blood'/'C14', 'Howard Lyon').
card_number('sign in blood'/'C14', '161').
card_flavor_text('sign in blood'/'C14', 'Little agonies pave the way to greater power.').
card_multiverse_id('sign in blood'/'C14', '389671').

card_in_set('silklash spider', 'C14').
card_original_type('silklash spider'/'C14', 'Creature — Spider').
card_original_text('silklash spider'/'C14', 'Reach\n{X}{G}{G}: Silklash Spider deals X damage to each creature with flying.').
card_image_name('silklash spider'/'C14', 'silklash spider').
card_uid('silklash spider'/'C14', 'C14:Silklash Spider:silklash spider').
card_rarity('silklash spider'/'C14', 'Rare').
card_artist('silklash spider'/'C14', 'Iain McCaig').
card_number('silklash spider'/'C14', '214').
card_flavor_text('silklash spider'/'C14', 'By the time the spider comes to slurp up its dinner, its victims have been partially dissolved by acidic silk.').
card_multiverse_id('silklash spider'/'C14', '389672').

card_in_set('silverblade paladin', 'C14').
card_original_type('silverblade paladin'/'C14', 'Creature — Human Knight').
card_original_text('silverblade paladin'/'C14', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Silverblade Paladin is paired with another creature, both creatures have double strike.').
card_image_name('silverblade paladin'/'C14', 'silverblade paladin').
card_uid('silverblade paladin'/'C14', 'C14:Silverblade Paladin:silverblade paladin').
card_rarity('silverblade paladin'/'C14', 'Rare').
card_artist('silverblade paladin'/'C14', 'Jason Chan').
card_number('silverblade paladin'/'C14', '88').
card_multiverse_id('silverblade paladin'/'C14', '389673').

card_in_set('skeletal scrying', 'C14').
card_original_type('skeletal scrying'/'C14', 'Instant').
card_original_text('skeletal scrying'/'C14', 'As an additional cost to cast Skeletal Scrying, exile X cards from your graveyard.\nYou draw X cards and you lose X life.').
card_image_name('skeletal scrying'/'C14', 'skeletal scrying').
card_uid('skeletal scrying'/'C14', 'C14:Skeletal Scrying:skeletal scrying').
card_rarity('skeletal scrying'/'C14', 'Uncommon').
card_artist('skeletal scrying'/'C14', 'Bob Petillo').
card_number('skeletal scrying'/'C14', '162').
card_multiverse_id('skeletal scrying'/'C14', '389674').

card_in_set('skirsdag high priest', 'C14').
card_original_type('skirsdag high priest'/'C14', 'Creature — Human Cleric').
card_original_text('skirsdag high priest'/'C14', 'Morbid — {T}, Tap two untapped creatures you control: Put a 5/5 black Demon creature token with flying onto the battlefield. Activate this ability only if a creature died this turn.').
card_image_name('skirsdag high priest'/'C14', 'skirsdag high priest').
card_uid('skirsdag high priest'/'C14', 'C14:Skirsdag High Priest:skirsdag high priest').
card_rarity('skirsdag high priest'/'C14', 'Rare').
card_artist('skirsdag high priest'/'C14', 'Jason A. Engle').
card_number('skirsdag high priest'/'C14', '163').
card_flavor_text('skirsdag high priest'/'C14', '\"Thraben\'s pleas fall on deaf ears. Ours do not.\"').
card_multiverse_id('skirsdag high priest'/'C14', '389675').

card_in_set('skullclamp', 'C14').
card_original_type('skullclamp'/'C14', 'Artifact — Equipment').
card_original_text('skullclamp'/'C14', 'Equipped creature gets +1/-1.\nWhenever equipped creature dies, draw two cards.\nEquip {1}').
card_image_name('skullclamp'/'C14', 'skullclamp').
card_uid('skullclamp'/'C14', 'C14:Skullclamp:skullclamp').
card_rarity('skullclamp'/'C14', 'Uncommon').
card_artist('skullclamp'/'C14', 'Daniel Ljunggren').
card_number('skullclamp'/'C14', '268').
card_flavor_text('skullclamp'/'C14', 'The mind is a beautiful bounty encased in an annoying bone container.').
card_multiverse_id('skullclamp'/'C14', '389676').

card_in_set('sky diamond', 'C14').
card_original_type('sky diamond'/'C14', 'Artifact').
card_original_text('sky diamond'/'C14', 'Sky Diamond enters the battlefield tapped.\n{T}: Add {U} to your mana pool.').
card_image_name('sky diamond'/'C14', 'sky diamond').
card_uid('sky diamond'/'C14', 'C14:Sky Diamond:sky diamond').
card_rarity('sky diamond'/'C14', 'Uncommon').
card_artist('sky diamond'/'C14', 'Lindsey Look').
card_number('sky diamond'/'C14', '269').
card_multiverse_id('sky diamond'/'C14', '389677').

card_in_set('skyhunter skirmisher', 'C14').
card_original_type('skyhunter skirmisher'/'C14', 'Creature — Cat Knight').
card_original_text('skyhunter skirmisher'/'C14', 'Flying, double strike').
card_image_name('skyhunter skirmisher'/'C14', 'skyhunter skirmisher').
card_uid('skyhunter skirmisher'/'C14', 'C14:Skyhunter Skirmisher:skyhunter skirmisher').
card_rarity('skyhunter skirmisher'/'C14', 'Uncommon').
card_artist('skyhunter skirmisher'/'C14', 'Greg Staples').
card_number('skyhunter skirmisher'/'C14', '89').
card_flavor_text('skyhunter skirmisher'/'C14', '\"Like dawn\'s first light, blind the unprepared and banish the shadows.\"\n—Skyhunter creed').
card_multiverse_id('skyhunter skirmisher'/'C14', '389678').

card_in_set('slippery karst', 'C14').
card_original_type('slippery karst'/'C14', 'Land').
card_original_text('slippery karst'/'C14', 'Slippery Karst enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('slippery karst'/'C14', 'slippery karst').
card_uid('slippery karst'/'C14', 'C14:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'C14', 'Common').
card_artist('slippery karst'/'C14', 'Stephen Daniele').
card_number('slippery karst'/'C14', '311').
card_multiverse_id('slippery karst'/'C14', '389679').

card_in_set('smoldering crater', 'C14').
card_original_type('smoldering crater'/'C14', 'Land').
card_original_text('smoldering crater'/'C14', 'Smoldering Crater enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('smoldering crater'/'C14', 'smoldering crater').
card_uid('smoldering crater'/'C14', 'C14:Smoldering Crater:smoldering crater').
card_rarity('smoldering crater'/'C14', 'Common').
card_artist('smoldering crater'/'C14', 'Mark Tedin').
card_number('smoldering crater'/'C14', '312').
card_multiverse_id('smoldering crater'/'C14', '389680').

card_in_set('sol ring', 'C14').
card_original_type('sol ring'/'C14', 'Artifact').
card_original_text('sol ring'/'C14', '{T}: Add {2} to your mana pool.').
card_image_name('sol ring'/'C14', 'sol ring').
card_uid('sol ring'/'C14', 'C14:Sol Ring:sol ring').
card_rarity('sol ring'/'C14', 'Uncommon').
card_artist('sol ring'/'C14', 'Mike Bierek').
card_number('sol ring'/'C14', '270').
card_flavor_text('sol ring'/'C14', 'Lost to time is the artificer\'s art of trapping light from a distant star in a ring of purest gold.').
card_multiverse_id('sol ring'/'C14', '389681').

card_in_set('solemn simulacrum', 'C14').
card_original_type('solemn simulacrum'/'C14', 'Artifact Creature — Golem').
card_original_text('solemn simulacrum'/'C14', 'When Solemn Simulacrum enters the battlefield, you may search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.\nWhen Solemn Simulacrum dies, you may draw a card.').
card_image_name('solemn simulacrum'/'C14', 'solemn simulacrum').
card_uid('solemn simulacrum'/'C14', 'C14:Solemn Simulacrum:solemn simulacrum').
card_rarity('solemn simulacrum'/'C14', 'Rare').
card_artist('solemn simulacrum'/'C14', 'Dan Scott').
card_number('solemn simulacrum'/'C14', '271').
card_multiverse_id('solemn simulacrum'/'C14', '389682').

card_in_set('song of the dryads', 'C14').
card_original_type('song of the dryads'/'C14', 'Enchantment — Aura').
card_original_text('song of the dryads'/'C14', 'Enchant permanent\nEnchanted permanent is a colorless Forest land.').
card_first_print('song of the dryads', 'C14').
card_image_name('song of the dryads'/'C14', 'song of the dryads').
card_uid('song of the dryads'/'C14', 'C14:Song of the Dryads:song of the dryads').
card_rarity('song of the dryads'/'C14', 'Rare').
card_artist('song of the dryads'/'C14', 'Lars Grant-West').
card_number('song of the dryads'/'C14', '47').
card_flavor_text('song of the dryads'/'C14', 'Few who encounter the strange, human-like grove leave once the lilting chorus of the dryads reaches their ears.').
card_multiverse_id('song of the dryads'/'C14', '389683').

card_in_set('soul of the harvest', 'C14').
card_original_type('soul of the harvest'/'C14', 'Creature — Elemental').
card_original_text('soul of the harvest'/'C14', 'Trample\nWhenever another nontoken creature enters the battlefield under your control, you may draw a card.').
card_image_name('soul of the harvest'/'C14', 'soul of the harvest').
card_uid('soul of the harvest'/'C14', 'C14:Soul of the Harvest:soul of the harvest').
card_rarity('soul of the harvest'/'C14', 'Rare').
card_artist('soul of the harvest'/'C14', 'Eytan Zana').
card_number('soul of the harvest'/'C14', '215').
card_flavor_text('soul of the harvest'/'C14', 'It\'s there when a seed sprouts, when gourds ripen on the vines, and when the reapers cut the grains under the Harvest Moon.').
card_multiverse_id('soul of the harvest'/'C14', '389684').

card_in_set('spectral procession', 'C14').
card_original_type('spectral procession'/'C14', 'Sorcery').
card_original_text('spectral procession'/'C14', 'Put three 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('spectral procession'/'C14', 'spectral procession').
card_uid('spectral procession'/'C14', 'C14:Spectral Procession:spectral procession').
card_rarity('spectral procession'/'C14', 'Uncommon').
card_artist('spectral procession'/'C14', 'Jeremy Enecio').
card_number('spectral procession'/'C14', '90').
card_flavor_text('spectral procession'/'C14', '\"The dead have it easy. They suffer no more. If breaking their rest helps the living, so be it.\"\n—Olka, mistmeadow witch').
card_multiverse_id('spectral procession'/'C14', '389685').

card_in_set('sphinx of jwar isle', 'C14').
card_original_type('sphinx of jwar isle'/'C14', 'Creature — Sphinx').
card_original_text('sphinx of jwar isle'/'C14', 'Flying\nShroud (This creature can\'t be the target of spells or abilities.)\nYou may look at the top card of your library. (You may do this at any time.)').
card_image_name('sphinx of jwar isle'/'C14', 'sphinx of jwar isle').
card_uid('sphinx of jwar isle'/'C14', 'C14:Sphinx of Jwar Isle:sphinx of jwar isle').
card_rarity('sphinx of jwar isle'/'C14', 'Rare').
card_artist('sphinx of jwar isle'/'C14', 'Justin Sweet').
card_number('sphinx of jwar isle'/'C14', '126').
card_multiverse_id('sphinx of jwar isle'/'C14', '389686').

card_in_set('sphinx of magosi', 'C14').
card_original_type('sphinx of magosi'/'C14', 'Creature — Sphinx').
card_original_text('sphinx of magosi'/'C14', 'Flying\n{2}{U}: Draw a card, then put a +1/+1 counter on Sphinx of Magosi.').
card_image_name('sphinx of magosi'/'C14', 'sphinx of magosi').
card_uid('sphinx of magosi'/'C14', 'C14:Sphinx of Magosi:sphinx of magosi').
card_rarity('sphinx of magosi'/'C14', 'Rare').
card_artist('sphinx of magosi'/'C14', 'James Ryman').
card_number('sphinx of magosi'/'C14', '127').
card_flavor_text('sphinx of magosi'/'C14', '\"A riddle is nothing more than a trap for small minds, baited with the promise of understanding.\"').
card_multiverse_id('sphinx of magosi'/'C14', '389687').

card_in_set('sphinx of uthuun', 'C14').
card_original_type('sphinx of uthuun'/'C14', 'Creature — Sphinx').
card_original_text('sphinx of uthuun'/'C14', 'Flying\nWhen Sphinx of Uthuun enters the battlefield, reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('sphinx of uthuun'/'C14', 'sphinx of uthuun').
card_uid('sphinx of uthuun'/'C14', 'C14:Sphinx of Uthuun:sphinx of uthuun').
card_rarity('sphinx of uthuun'/'C14', 'Rare').
card_artist('sphinx of uthuun'/'C14', 'Kekai Kotaki').
card_number('sphinx of uthuun'/'C14', '128').
card_multiverse_id('sphinx of uthuun'/'C14', '389688').

card_in_set('spine of ish sah', 'C14').
card_original_type('spine of ish sah'/'C14', 'Artifact').
card_original_text('spine of ish sah'/'C14', 'When Spine of Ish Sah enters the battlefield, destroy target permanent.\nWhen Spine of Ish Sah is put into a graveyard from the battlefield, return Spine of Ish Sah to its owner\'s hand.').
card_image_name('spine of ish sah'/'C14', 'spine of ish sah').
card_uid('spine of ish sah'/'C14', 'C14:Spine of Ish Sah:spine of ish sah').
card_rarity('spine of ish sah'/'C14', 'Rare').
card_artist('spine of ish sah'/'C14', 'Daniel Ljunggren').
card_number('spine of ish sah'/'C14', '272').
card_multiverse_id('spine of ish sah'/'C14', '389689').

card_in_set('spitebellows', 'C14').
card_original_type('spitebellows'/'C14', 'Creature — Elemental').
card_original_text('spitebellows'/'C14', 'When Spitebellows leaves the battlefield, it deals 6 damage to target creature.\nEvoke {1}{R}{R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('spitebellows'/'C14', 'spitebellows').
card_uid('spitebellows'/'C14', 'C14:Spitebellows:spitebellows').
card_rarity('spitebellows'/'C14', 'Uncommon').
card_artist('spitebellows'/'C14', 'Larry MacDougall').
card_number('spitebellows'/'C14', '181').
card_flavor_text('spitebellows'/'C14', 'Disaster stalks with gaping jaws across unready lands.').
card_multiverse_id('spitebellows'/'C14', '389690').

card_in_set('spoils of blood', 'C14').
card_original_type('spoils of blood'/'C14', 'Instant').
card_original_text('spoils of blood'/'C14', 'Put an X/X black Horror creature token onto the battlefield, where X is the number of creatures that died this turn.').
card_first_print('spoils of blood', 'C14').
card_image_name('spoils of blood'/'C14', 'spoils of blood').
card_uid('spoils of blood'/'C14', 'C14:Spoils of Blood:spoils of blood').
card_rarity('spoils of blood'/'C14', 'Rare').
card_artist('spoils of blood'/'C14', 'Erica Yang').
card_number('spoils of blood'/'C14', '30').
card_flavor_text('spoils of blood'/'C14', '\"There are always leftovers.\"\n—Derecht, flesh carver').
card_multiverse_id('spoils of blood'/'C14', '389691').

card_in_set('starstorm', 'C14').
card_original_type('starstorm'/'C14', 'Instant').
card_original_text('starstorm'/'C14', 'Starstorm deals X damage to each creature.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_image_name('starstorm'/'C14', 'starstorm').
card_uid('starstorm'/'C14', 'C14:Starstorm:starstorm').
card_rarity('starstorm'/'C14', 'Rare').
card_artist('starstorm'/'C14', 'Jonas De Ro').
card_number('starstorm'/'C14', '182').
card_flavor_text('starstorm'/'C14', 'Ever since the disaster, the city\'s survivors viewed the star-strewn sky with dread.').
card_multiverse_id('starstorm'/'C14', '389692').

card_in_set('steel hellkite', 'C14').
card_original_type('steel hellkite'/'C14', 'Artifact Creature — Dragon').
card_original_text('steel hellkite'/'C14', 'Flying\n{2}: Steel Hellkite gets +1/+0 until end of turn.\n{X}: Destroy each nonland permanent with converted mana cost X whose controller was dealt combat damage by Steel Hellkite this turn. Activate this ability only once each turn.').
card_image_name('steel hellkite'/'C14', 'steel hellkite').
card_uid('steel hellkite'/'C14', 'C14:Steel Hellkite:steel hellkite').
card_rarity('steel hellkite'/'C14', 'Rare').
card_artist('steel hellkite'/'C14', 'James Paick').
card_number('steel hellkite'/'C14', '273').
card_multiverse_id('steel hellkite'/'C14', '389693').

card_in_set('stitcher geralf', 'C14').
card_original_type('stitcher geralf'/'C14', 'Legendary Creature — Human Wizard').
card_original_text('stitcher geralf'/'C14', '{2}{U}, {T}: Each player puts the top three cards of his or her library into his or her graveyard. Exile up to two creature cards put into graveyards this way. Put an X/X blue Zombie creature token onto the battlefield, where X is the total power of the cards exiled this way.').
card_first_print('stitcher geralf', 'C14').
card_image_name('stitcher geralf'/'C14', 'stitcher geralf').
card_uid('stitcher geralf'/'C14', 'C14:Stitcher Geralf:stitcher geralf').
card_rarity('stitcher geralf'/'C14', 'Mythic Rare').
card_artist('stitcher geralf'/'C14', 'Karla Ortiz').
card_number('stitcher geralf'/'C14', '17').
card_multiverse_id('stitcher geralf'/'C14', '389694').

card_in_set('stormsurge kraken', 'C14').
card_original_type('stormsurge kraken'/'C14', 'Creature — Kraken').
card_original_text('stormsurge kraken'/'C14', 'Hexproof\nLieutenant — As long as you control your commander, Stormsurge Kraken gets +2/+2 and has \"Whenever Stormsurge Kraken becomes blocked, you may draw two cards.\"').
card_first_print('stormsurge kraken', 'C14').
card_image_name('stormsurge kraken'/'C14', 'stormsurge kraken').
card_uid('stormsurge kraken'/'C14', 'C14:Stormsurge Kraken:stormsurge kraken').
card_rarity('stormsurge kraken'/'C14', 'Rare').
card_artist('stormsurge kraken'/'C14', 'Svetlin Velinov').
card_number('stormsurge kraken'/'C14', '18').
card_flavor_text('stormsurge kraken'/'C14', 'Most see krakens as wantonly violent, failing to notice their meticulous attention to detail when dismantling a vessel.').
card_multiverse_id('stormsurge kraken'/'C14', '389695').

card_in_set('strata scythe', 'C14').
card_original_type('strata scythe'/'C14', 'Artifact — Equipment').
card_original_text('strata scythe'/'C14', 'Imprint — When Strata Scythe enters the battlefield, search your library for a land card, exile it, then shuffle your library.\nEquipped creature gets +1/+1 for each land on the battlefield with the same name as the exiled card.\nEquip {3}').
card_image_name('strata scythe'/'C14', 'strata scythe').
card_uid('strata scythe'/'C14', 'C14:Strata Scythe:strata scythe').
card_rarity('strata scythe'/'C14', 'Rare').
card_artist('strata scythe'/'C14', 'Scott Chou').
card_number('strata scythe'/'C14', '274').
card_multiverse_id('strata scythe'/'C14', '389696').

card_in_set('stroke of genius', 'C14').
card_original_type('stroke of genius'/'C14', 'Instant').
card_original_text('stroke of genius'/'C14', 'Target player draws X cards.').
card_image_name('stroke of genius'/'C14', 'stroke of genius').
card_uid('stroke of genius'/'C14', 'C14:Stroke of Genius:stroke of genius').
card_rarity('stroke of genius'/'C14', 'Rare').
card_artist('stroke of genius'/'C14', 'Stephen Daniele').
card_number('stroke of genius'/'C14', '129').
card_flavor_text('stroke of genius'/'C14', 'After a hundred failed experiments, Urza was stunned to find that common silver passed through the portal undamaged. He immediately designed a golem made of the metal.').
card_multiverse_id('stroke of genius'/'C14', '389697').

card_in_set('sudden spoiling', 'C14').
card_original_type('sudden spoiling'/'C14', 'Instant').
card_original_text('sudden spoiling'/'C14', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nUntil end of turn, creatures target player controls lose all abilities and have base power and toughness 0/2.').
card_image_name('sudden spoiling'/'C14', 'sudden spoiling').
card_uid('sudden spoiling'/'C14', 'C14:Sudden Spoiling:sudden spoiling').
card_rarity('sudden spoiling'/'C14', 'Rare').
card_artist('sudden spoiling'/'C14', 'Alan Pollack').
card_number('sudden spoiling'/'C14', '164').
card_multiverse_id('sudden spoiling'/'C14', '389698').

card_in_set('sun titan', 'C14').
card_original_type('sun titan'/'C14', 'Creature — Giant').
card_original_text('sun titan'/'C14', 'Vigilance\nWhenever Sun Titan enters the battlefield or attacks, you may return target permanent card with converted mana cost 3 or less from your graveyard to the battlefield.').
card_image_name('sun titan'/'C14', 'sun titan').
card_uid('sun titan'/'C14', 'C14:Sun Titan:sun titan').
card_rarity('sun titan'/'C14', 'Mythic Rare').
card_artist('sun titan'/'C14', 'Todd Lockwood').
card_number('sun titan'/'C14', '91').
card_flavor_text('sun titan'/'C14', 'A blazing sun that never sets.').
card_multiverse_id('sun titan'/'C14', '389699').

card_in_set('sunblast angel', 'C14').
card_original_type('sunblast angel'/'C14', 'Creature — Angel').
card_original_text('sunblast angel'/'C14', 'Flying\nWhen Sunblast Angel enters the battlefield, destroy all tapped creatures.').
card_image_name('sunblast angel'/'C14', 'sunblast angel').
card_uid('sunblast angel'/'C14', 'C14:Sunblast Angel:sunblast angel').
card_rarity('sunblast angel'/'C14', 'Rare').
card_artist('sunblast angel'/'C14', 'Jason Chan').
card_number('sunblast angel'/'C14', '92').
card_flavor_text('sunblast angel'/'C14', 'There may exist powers even greater than Phyrexia.').
card_multiverse_id('sunblast angel'/'C14', '389700').

card_in_set('swamp', 'C14').
card_original_type('swamp'/'C14', 'Basic Land — Swamp').
card_original_text('swamp'/'C14', 'B').
card_image_name('swamp'/'C14', 'swamp1').
card_uid('swamp'/'C14', 'C14:Swamp:swamp1').
card_rarity('swamp'/'C14', 'Basic Land').
card_artist('swamp'/'C14', 'John Avon').
card_number('swamp'/'C14', '326').
card_multiverse_id('swamp'/'C14', '389703').

card_in_set('swamp', 'C14').
card_original_type('swamp'/'C14', 'Basic Land — Swamp').
card_original_text('swamp'/'C14', 'B').
card_image_name('swamp'/'C14', 'swamp2').
card_uid('swamp'/'C14', 'C14:Swamp:swamp2').
card_rarity('swamp'/'C14', 'Basic Land').
card_artist('swamp'/'C14', 'Véronique Meignaud').
card_number('swamp'/'C14', '327').
card_multiverse_id('swamp'/'C14', '389704').

card_in_set('swamp', 'C14').
card_original_type('swamp'/'C14', 'Basic Land — Swamp').
card_original_text('swamp'/'C14', 'B').
card_image_name('swamp'/'C14', 'swamp3').
card_uid('swamp'/'C14', 'C14:Swamp:swamp3').
card_rarity('swamp'/'C14', 'Basic Land').
card_artist('swamp'/'C14', 'Jung Park').
card_number('swamp'/'C14', '328').
card_multiverse_id('swamp'/'C14', '389701').

card_in_set('swamp', 'C14').
card_original_type('swamp'/'C14', 'Basic Land — Swamp').
card_original_text('swamp'/'C14', 'B').
card_image_name('swamp'/'C14', 'swamp4').
card_uid('swamp'/'C14', 'C14:Swamp:swamp4').
card_rarity('swamp'/'C14', 'Basic Land').
card_artist('swamp'/'C14', 'Vincent Proce').
card_number('swamp'/'C14', '329').
card_multiverse_id('swamp'/'C14', '389702').

card_in_set('swiftfoot boots', 'C14').
card_original_type('swiftfoot boots'/'C14', 'Artifact — Equipment').
card_original_text('swiftfoot boots'/'C14', 'Equipped creature has hexproof and haste.\nEquip {1}').
card_image_name('swiftfoot boots'/'C14', 'swiftfoot boots').
card_uid('swiftfoot boots'/'C14', 'C14:Swiftfoot Boots:swiftfoot boots').
card_rarity('swiftfoot boots'/'C14', 'Uncommon').
card_artist('swiftfoot boots'/'C14', 'Svetlin Velinov').
card_number('swiftfoot boots'/'C14', '275').
card_flavor_text('swiftfoot boots'/'C14', '\"There is great wisdom in rushing headlong into combat . . . if you\'re prepared.\"\n—Kenjek, captain of the Thousand Swords').
card_multiverse_id('swiftfoot boots'/'C14', '389705').

card_in_set('sword of vengeance', 'C14').
card_original_type('sword of vengeance'/'C14', 'Artifact — Equipment').
card_original_text('sword of vengeance'/'C14', 'Equipped creature gets +2/+0 and has first strike, vigilance, trample, and haste.\nEquip {3}').
card_image_name('sword of vengeance'/'C14', 'sword of vengeance').
card_uid('sword of vengeance'/'C14', 'C14:Sword of Vengeance:sword of vengeance').
card_rarity('sword of vengeance'/'C14', 'Rare').
card_artist('sword of vengeance'/'C14', 'Dan Scott').
card_number('sword of vengeance'/'C14', '276').
card_flavor_text('sword of vengeance'/'C14', 'When wielded by a true believer, it matters little whether the sword is a relic or a replica.').
card_multiverse_id('sword of vengeance'/'C14', '389706').

card_in_set('sylvan offering', 'C14').
card_original_type('sylvan offering'/'C14', 'Sorcery').
card_original_text('sylvan offering'/'C14', 'Choose an opponent. You and that player each put an X/X green Treefolk creature token onto the battlefield.\nChoose an opponent. You and that player each put X 1/1 green Elf Warrior creature tokens onto the battlefield.').
card_first_print('sylvan offering', 'C14').
card_image_name('sylvan offering'/'C14', 'sylvan offering').
card_uid('sylvan offering'/'C14', 'C14:Sylvan Offering:sylvan offering').
card_rarity('sylvan offering'/'C14', 'Rare').
card_artist('sylvan offering'/'C14', 'Zoltan Boros').
card_number('sylvan offering'/'C14', '48').
card_multiverse_id('sylvan offering'/'C14', '389707').

card_in_set('sylvan ranger', 'C14').
card_original_type('sylvan ranger'/'C14', 'Creature — Elf Scout').
card_original_text('sylvan ranger'/'C14', 'When Sylvan Ranger enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('sylvan ranger'/'C14', 'sylvan ranger').
card_uid('sylvan ranger'/'C14', 'C14:Sylvan Ranger:sylvan ranger').
card_rarity('sylvan ranger'/'C14', 'Common').
card_artist('sylvan ranger'/'C14', 'Christopher Moeller').
card_number('sylvan ranger'/'C14', '216').
card_flavor_text('sylvan ranger'/'C14', '\"Not all paths are found on the forest floor.\"').
card_multiverse_id('sylvan ranger'/'C14', '389708').

card_in_set('sylvan safekeeper', 'C14').
card_original_type('sylvan safekeeper'/'C14', 'Creature — Human Wizard').
card_original_text('sylvan safekeeper'/'C14', 'Sacrifice a land: Target creature you control gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_image_name('sylvan safekeeper'/'C14', 'sylvan safekeeper').
card_uid('sylvan safekeeper'/'C14', 'C14:Sylvan Safekeeper:sylvan safekeeper').
card_rarity('sylvan safekeeper'/'C14', 'Rare').
card_artist('sylvan safekeeper'/'C14', 'Magali Villeneuve').
card_number('sylvan safekeeper'/'C14', '217').
card_flavor_text('sylvan safekeeper'/'C14', '\"He has been to the heart of the wood. Did you truly expect to wield power over him?\"\n—Oakhide druid').
card_multiverse_id('sylvan safekeeper'/'C14', '389709').

card_in_set('syphon mind', 'C14').
card_original_type('syphon mind'/'C14', 'Sorcery').
card_original_text('syphon mind'/'C14', 'Each other player discards a card. You draw a card for each card discarded this way.').
card_image_name('syphon mind'/'C14', 'syphon mind').
card_uid('syphon mind'/'C14', 'C14:Syphon Mind:syphon mind').
card_rarity('syphon mind'/'C14', 'Common').
card_artist('syphon mind'/'C14', 'Jeff Easley').
card_number('syphon mind'/'C14', '165').
card_flavor_text('syphon mind'/'C14', 'When tempers run high, it\'s easy to lose your head.').
card_multiverse_id('syphon mind'/'C14', '389710').

card_in_set('tectonic edge', 'C14').
card_original_type('tectonic edge'/'C14', 'Land').
card_original_text('tectonic edge'/'C14', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Tectonic Edge: Destroy target nonbasic land. Activate this ability only if an opponent controls four or more lands.').
card_image_name('tectonic edge'/'C14', 'tectonic edge').
card_uid('tectonic edge'/'C14', 'C14:Tectonic Edge:tectonic edge').
card_rarity('tectonic edge'/'C14', 'Uncommon').
card_artist('tectonic edge'/'C14', 'Vincent Proce').
card_number('tectonic edge'/'C14', '313').
card_flavor_text('tectonic edge'/'C14', '\"We move because the earth does.\"\n—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('tectonic edge'/'C14', '389711').

card_in_set('teferi, temporal archmage', 'C14').
card_original_type('teferi, temporal archmage'/'C14', 'Planeswalker — Teferi').
card_original_text('teferi, temporal archmage'/'C14', '+1: Look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.\n−1: Untap up to four target permanents.\n−10: You get an emblem with \"You may activate loyalty abilities of planeswalkers you control on any player\'s turn any time you could cast an instant.\"\nTeferi, Temporal Archmage can be your commander.').
card_first_print('teferi, temporal archmage', 'C14').
card_image_name('teferi, temporal archmage'/'C14', 'teferi, temporal archmage').
card_uid('teferi, temporal archmage'/'C14', 'C14:Teferi, Temporal Archmage:teferi, temporal archmage').
card_rarity('teferi, temporal archmage'/'C14', 'Mythic Rare').
card_artist('teferi, temporal archmage'/'C14', 'Tyler Jacobson').
card_number('teferi, temporal archmage'/'C14', '19').
card_multiverse_id('teferi, temporal archmage'/'C14', '389712').

card_in_set('temple of the false god', 'C14').
card_original_type('temple of the false god'/'C14', 'Land').
card_original_text('temple of the false god'/'C14', '{T}: Add {2} to your mana pool. Activate this ability only if you control five or more lands.').
card_image_name('temple of the false god'/'C14', 'temple of the false god').
card_uid('temple of the false god'/'C14', 'C14:Temple of the False God:temple of the false god').
card_rarity('temple of the false god'/'C14', 'Uncommon').
card_artist('temple of the false god'/'C14', 'Brian Snõddy').
card_number('temple of the false god'/'C14', '314').
card_flavor_text('temple of the false god'/'C14', 'Those who bring nothing to the temple take nothing away.').
card_multiverse_id('temple of the false god'/'C14', '389713').

card_in_set('tendrils of corruption', 'C14').
card_original_type('tendrils of corruption'/'C14', 'Instant').
card_original_text('tendrils of corruption'/'C14', 'Tendrils of Corruption deals X damage to target creature and you gain X life, where X is the number of Swamps you control.').
card_image_name('tendrils of corruption'/'C14', 'tendrils of corruption').
card_uid('tendrils of corruption'/'C14', 'C14:Tendrils of Corruption:tendrils of corruption').
card_rarity('tendrils of corruption'/'C14', 'Common').
card_artist('tendrils of corruption'/'C14', 'Vance Kovacs').
card_number('tendrils of corruption'/'C14', '166').
card_flavor_text('tendrils of corruption'/'C14', 'Darkness doesn\'t always send its minions to do its dirty work.').
card_multiverse_id('tendrils of corruption'/'C14', '389714').

card_in_set('terastodon', 'C14').
card_original_type('terastodon'/'C14', 'Creature — Elephant').
card_original_text('terastodon'/'C14', 'When Terastodon enters the battlefield, you may destroy up to three target noncreature permanents. For each permanent put into a graveyard this way, its controller puts a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('terastodon'/'C14', 'terastodon').
card_uid('terastodon'/'C14', 'C14:Terastodon:terastodon').
card_rarity('terastodon'/'C14', 'Rare').
card_artist('terastodon'/'C14', 'Lars Grant-West').
card_number('terastodon'/'C14', '218').
card_multiverse_id('terastodon'/'C14', '389715').

card_in_set('terramorphic expanse', 'C14').
card_original_type('terramorphic expanse'/'C14', 'Land').
card_original_text('terramorphic expanse'/'C14', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'C14', 'terramorphic expanse').
card_uid('terramorphic expanse'/'C14', 'C14:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'C14', 'Common').
card_artist('terramorphic expanse'/'C14', 'Dan Scott').
card_number('terramorphic expanse'/'C14', '315').
card_multiverse_id('terramorphic expanse'/'C14', '389716').

card_in_set('thornweald archer', 'C14').
card_original_type('thornweald archer'/'C14', 'Creature — Elf Archer').
card_original_text('thornweald archer'/'C14', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('thornweald archer'/'C14', 'thornweald archer').
card_uid('thornweald archer'/'C14', 'C14:Thornweald Archer:thornweald archer').
card_rarity('thornweald archer'/'C14', 'Common').
card_artist('thornweald archer'/'C14', 'Dave Kendall').
card_number('thornweald archer'/'C14', '219').
card_flavor_text('thornweald archer'/'C14', 'Their arrows are tipped with basilisk eyes and fletched with cockatrice feathers.').
card_multiverse_id('thornweald archer'/'C14', '389717').

card_in_set('thran dynamo', 'C14').
card_original_type('thran dynamo'/'C14', 'Artifact').
card_original_text('thran dynamo'/'C14', '{T}: Add {3} to your mana pool.').
card_image_name('thran dynamo'/'C14', 'thran dynamo').
card_uid('thran dynamo'/'C14', 'C14:Thran Dynamo:thran dynamo').
card_rarity('thran dynamo'/'C14', 'Uncommon').
card_artist('thran dynamo'/'C14', 'Ron Spears').
card_number('thran dynamo'/'C14', '277').
card_flavor_text('thran dynamo'/'C14', 'Urza\'s metathran children were conceived, birthed, and nurtured by an integrated system of machines.').
card_multiverse_id('thran dynamo'/'C14', '389718').

card_in_set('thunderfoot baloth', 'C14').
card_original_type('thunderfoot baloth'/'C14', 'Creature — Beast').
card_original_text('thunderfoot baloth'/'C14', 'Trample\nLieutenant — As long as you control your commander, Thunderfoot Baloth gets +2/+2 and other creatures you control get +2/+2 and have trample.').
card_first_print('thunderfoot baloth', 'C14').
card_image_name('thunderfoot baloth'/'C14', 'thunderfoot baloth').
card_uid('thunderfoot baloth'/'C14', 'C14:Thunderfoot Baloth:thunderfoot baloth').
card_rarity('thunderfoot baloth'/'C14', 'Rare').
card_artist('thunderfoot baloth'/'C14', 'Marco Nelor').
card_number('thunderfoot baloth'/'C14', '49').
card_flavor_text('thunderfoot baloth'/'C14', 'If you hear it, it\'s too late.').
card_multiverse_id('thunderfoot baloth'/'C14', '389719').

card_in_set('timberwatch elf', 'C14').
card_original_type('timberwatch elf'/'C14', 'Creature — Elf').
card_original_text('timberwatch elf'/'C14', '{T}: Target creature gets +X/+X until end of turn, where X is the number of Elves on the battlefield.').
card_image_name('timberwatch elf'/'C14', 'timberwatch elf').
card_uid('timberwatch elf'/'C14', 'C14:Timberwatch Elf:timberwatch elf').
card_rarity('timberwatch elf'/'C14', 'Common').
card_artist('timberwatch elf'/'C14', 'Dave Dorman').
card_number('timberwatch elf'/'C14', '220').
card_flavor_text('timberwatch elf'/'C14', 'Even through the Mirari\'s voice, the elves still hear the call of their kinship.').
card_multiverse_id('timberwatch elf'/'C14', '389720').

card_in_set('titania\'s chosen', 'C14').
card_original_type('titania\'s chosen'/'C14', 'Creature — Elf Archer').
card_original_text('titania\'s chosen'/'C14', 'Whenever a player casts a green spell, put a +1/+1 counter on Titania\'s Chosen.').
card_image_name('titania\'s chosen'/'C14', 'titania\'s chosen').
card_uid('titania\'s chosen'/'C14', 'C14:Titania\'s Chosen:titania\'s chosen').
card_rarity('titania\'s chosen'/'C14', 'Uncommon').
card_artist('titania\'s chosen'/'C14', 'Mark Zug').
card_number('titania\'s chosen'/'C14', '221').
card_flavor_text('titania\'s chosen'/'C14', '\"What do a hero and an arrow have in common? In times of war are many more made.\"\n—Elvish riddle').
card_multiverse_id('titania\'s chosen'/'C14', '389722').

card_in_set('titania, protector of argoth', 'C14').
card_original_type('titania, protector of argoth'/'C14', 'Legendary Creature — Elemental').
card_original_text('titania, protector of argoth'/'C14', 'When Titania, Protector of Argoth enters the battlefield, return target land card from your graveyard to the battlefield.\nWhenever a land you control is put into a graveyard from the battlefield, put a 5/3 green Elemental creature token onto the battlefield.').
card_first_print('titania, protector of argoth', 'C14').
card_image_name('titania, protector of argoth'/'C14', 'titania, protector of argoth').
card_uid('titania, protector of argoth'/'C14', 'C14:Titania, Protector of Argoth:titania, protector of argoth').
card_rarity('titania, protector of argoth'/'C14', 'Mythic Rare').
card_artist('titania, protector of argoth'/'C14', 'Magali Villeneuve').
card_number('titania, protector of argoth'/'C14', '50').
card_multiverse_id('titania, protector of argoth'/'C14', '389721').

card_in_set('tormod\'s crypt', 'C14').
card_original_type('tormod\'s crypt'/'C14', 'Artifact').
card_original_text('tormod\'s crypt'/'C14', '{T}, Sacrifice Tormod\'s Crypt: Exile all cards from target player\'s graveyard.').
card_image_name('tormod\'s crypt'/'C14', 'tormod\'s crypt').
card_uid('tormod\'s crypt'/'C14', 'C14:Tormod\'s Crypt:tormod\'s crypt').
card_rarity('tormod\'s crypt'/'C14', 'Uncommon').
card_artist('tormod\'s crypt'/'C14', 'Lars Grant-West').
card_number('tormod\'s crypt'/'C14', '278').
card_flavor_text('tormod\'s crypt'/'C14', 'Dominaria\'s most extravagant crypt nevertheless holds an empty grave.').
card_multiverse_id('tormod\'s crypt'/'C14', '389723').

card_in_set('tornado elemental', 'C14').
card_original_type('tornado elemental'/'C14', 'Creature — Elemental').
card_original_text('tornado elemental'/'C14', 'When Tornado Elemental enters the battlefield, it deals 6 damage to each creature with flying.\nYou may have Tornado Elemental assign its combat damage as though it weren\'t blocked.').
card_image_name('tornado elemental'/'C14', 'tornado elemental').
card_uid('tornado elemental'/'C14', 'C14:Tornado Elemental:tornado elemental').
card_rarity('tornado elemental'/'C14', 'Rare').
card_artist('tornado elemental'/'C14', 'Richard Wright').
card_number('tornado elemental'/'C14', '222').
card_multiverse_id('tornado elemental'/'C14', '389724').

card_in_set('trading post', 'C14').
card_original_type('trading post'/'C14', 'Artifact').
card_original_text('trading post'/'C14', '{1}, {T}, Discard a card: You gain 4 life.\n{1}, {T}, Pay 1 life: Put a 0/1 white Goat creature token onto the battlefield.\n{1}, {T}, Sacrifice a creature: Return target artifact card from your graveyard to your hand.\n{1}, {T}, Sacrifice an artifact: Draw a card.').
card_image_name('trading post'/'C14', 'trading post').
card_uid('trading post'/'C14', 'C14:Trading Post:trading post').
card_rarity('trading post'/'C14', 'Rare').
card_artist('trading post'/'C14', 'Adam Paquette').
card_number('trading post'/'C14', '279').
card_multiverse_id('trading post'/'C14', '389725').

card_in_set('tragic slip', 'C14').
card_original_type('tragic slip'/'C14', 'Instant').
card_original_text('tragic slip'/'C14', 'Target creature gets -1/-1 until end of turn.\nMorbid — That creature gets -13/-13 until end of turn instead if a creature died this turn.').
card_image_name('tragic slip'/'C14', 'tragic slip').
card_uid('tragic slip'/'C14', 'C14:Tragic Slip:tragic slip').
card_rarity('tragic slip'/'C14', 'Common').
card_artist('tragic slip'/'C14', 'Christopher Moeller').
card_number('tragic slip'/'C14', '167').
card_flavor_text('tragic slip'/'C14', 'Linger on death\'s door and risk being invited in.').
card_multiverse_id('tragic slip'/'C14', '389726').

card_in_set('tranquil thicket', 'C14').
card_original_type('tranquil thicket'/'C14', 'Land').
card_original_text('tranquil thicket'/'C14', 'Tranquil Thicket enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'C14', 'tranquil thicket').
card_uid('tranquil thicket'/'C14', 'C14:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'C14', 'Common').
card_artist('tranquil thicket'/'C14', 'Heather Hudson').
card_number('tranquil thicket'/'C14', '316').
card_multiverse_id('tranquil thicket'/'C14', '389727').

card_in_set('true conviction', 'C14').
card_original_type('true conviction'/'C14', 'Enchantment').
card_original_text('true conviction'/'C14', 'Creatures you control have double strike and lifelink.').
card_image_name('true conviction'/'C14', 'true conviction').
card_uid('true conviction'/'C14', 'C14:True Conviction:true conviction').
card_rarity('true conviction'/'C14', 'Rare').
card_artist('true conviction'/'C14', 'Svetlin Velinov').
card_number('true conviction'/'C14', '93').
card_flavor_text('true conviction'/'C14', 'Dozens of spells known only to the Auriok elders were lost, but the next generation, galvanized by war, devised even more potent magic.').
card_multiverse_id('true conviction'/'C14', '389728').

card_in_set('tuktuk the explorer', 'C14').
card_original_type('tuktuk the explorer'/'C14', 'Legendary Creature — Goblin').
card_original_text('tuktuk the explorer'/'C14', 'Haste\nWhen Tuktuk the Explorer dies, put a legendary 5/5 colorless Goblin Golem artifact creature token named Tuktuk the Returned onto the battlefield.').
card_image_name('tuktuk the explorer'/'C14', 'tuktuk the explorer').
card_uid('tuktuk the explorer'/'C14', 'C14:Tuktuk the Explorer:tuktuk the explorer').
card_rarity('tuktuk the explorer'/'C14', 'Rare').
card_artist('tuktuk the explorer'/'C14', 'Volkan Baga').
card_number('tuktuk the explorer'/'C14', '183').
card_flavor_text('tuktuk the explorer'/'C14', 'Remade by Eldrazi magic, Tuktuk inspired an entire clan of misguided goblins.').
card_multiverse_id('tuktuk the explorer'/'C14', '389729').

card_in_set('turn to frog', 'C14').
card_original_type('turn to frog'/'C14', 'Instant').
card_original_text('turn to frog'/'C14', 'Until end of turn, target creature loses all abilities and becomes a blue Frog with base power and toughness 1/1.').
card_image_name('turn to frog'/'C14', 'turn to frog').
card_uid('turn to frog'/'C14', 'C14:Turn to Frog:turn to frog').
card_rarity('turn to frog'/'C14', 'Uncommon').
card_artist('turn to frog'/'C14', 'Warren Mahy').
card_number('turn to frog'/'C14', '130').
card_flavor_text('turn to frog'/'C14', '\"Ribbit.\"').
card_multiverse_id('turn to frog'/'C14', '389730').

card_in_set('twilight shepherd', 'C14').
card_original_type('twilight shepherd'/'C14', 'Creature — Angel').
card_original_text('twilight shepherd'/'C14', 'Flying, vigilance\nWhen Twilight Shepherd enters the battlefield, return to your hand all cards in your graveyard that were put there from the battlefield this turn.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('twilight shepherd'/'C14', 'twilight shepherd').
card_uid('twilight shepherd'/'C14', 'C14:Twilight Shepherd:twilight shepherd').
card_rarity('twilight shepherd'/'C14', 'Rare').
card_artist('twilight shepherd'/'C14', 'Jason Chan').
card_number('twilight shepherd'/'C14', '94').
card_multiverse_id('twilight shepherd'/'C14', '389731').

card_in_set('tyrant\'s familiar', 'C14').
card_original_type('tyrant\'s familiar'/'C14', 'Creature — Dragon').
card_original_text('tyrant\'s familiar'/'C14', 'Flying, haste\nLieutenant — As long as you control your commander, Tyrant\'s Familiar gets +2/+2 and has \"Whenever Tyrant\'s Familiar attacks, it deals 7 damage to target creature defending player controls.\"').
card_first_print('tyrant\'s familiar', 'C14').
card_image_name('tyrant\'s familiar'/'C14', 'tyrant\'s familiar').
card_uid('tyrant\'s familiar'/'C14', 'C14:Tyrant\'s Familiar:tyrant\'s familiar').
card_rarity('tyrant\'s familiar'/'C14', 'Rare').
card_artist('tyrant\'s familiar'/'C14', 'Todd Lockwood').
card_number('tyrant\'s familiar'/'C14', '39').
card_flavor_text('tyrant\'s familiar'/'C14', 'Nothing escapes its gaze—or its breath.').
card_multiverse_id('tyrant\'s familiar'/'C14', '389732').

card_in_set('unstable obelisk', 'C14').
card_original_type('unstable obelisk'/'C14', 'Artifact').
card_original_text('unstable obelisk'/'C14', '{T}: Add {1} to your mana pool.\n{7}, {T}, Sacrifice Unstable Obelisk: Destroy target permanent.').
card_first_print('unstable obelisk', 'C14').
card_image_name('unstable obelisk'/'C14', 'unstable obelisk').
card_uid('unstable obelisk'/'C14', 'C14:Unstable Obelisk:unstable obelisk').
card_rarity('unstable obelisk'/'C14', 'Uncommon').
card_artist('unstable obelisk'/'C14', 'William Wu').
card_number('unstable obelisk'/'C14', '58').
card_flavor_text('unstable obelisk'/'C14', 'Its collapse is like the lashing out of a long-dead civilization that resents being forgotten.').
card_multiverse_id('unstable obelisk'/'C14', '389733').

card_in_set('ur-golem\'s eye', 'C14').
card_original_type('ur-golem\'s eye'/'C14', 'Artifact').
card_original_text('ur-golem\'s eye'/'C14', '{T}: Add {2} to your mana pool.').
card_image_name('ur-golem\'s eye'/'C14', 'ur-golem\'s eye').
card_uid('ur-golem\'s eye'/'C14', 'C14:Ur-Golem\'s Eye:ur-golem\'s eye').
card_rarity('ur-golem\'s eye'/'C14', 'Uncommon').
card_artist('ur-golem\'s eye'/'C14', 'Heather Hudson').
card_number('ur-golem\'s eye'/'C14', '280').
card_flavor_text('ur-golem\'s eye'/'C14', 'Ur-golems once dominated the plane. Even now, they maintain a deep connection to it.').
card_multiverse_id('ur-golem\'s eye'/'C14', '389734').

card_in_set('vampire hexmage', 'C14').
card_original_type('vampire hexmage'/'C14', 'Creature — Vampire Shaman').
card_original_text('vampire hexmage'/'C14', 'First strike\nSacrifice Vampire Hexmage: Remove all counters from target permanent.').
card_image_name('vampire hexmage'/'C14', 'vampire hexmage').
card_uid('vampire hexmage'/'C14', 'C14:Vampire Hexmage:vampire hexmage').
card_rarity('vampire hexmage'/'C14', 'Uncommon').
card_artist('vampire hexmage'/'C14', 'Eric Deschamps').
card_number('vampire hexmage'/'C14', '168').
card_flavor_text('vampire hexmage'/'C14', 'When the blood hunt lost its thrill, she looked for less tangible means of domination.').
card_multiverse_id('vampire hexmage'/'C14', '389735').

card_in_set('victimize', 'C14').
card_original_type('victimize'/'C14', 'Sorcery').
card_original_text('victimize'/'C14', 'Choose two target creature cards in your graveyard. Sacrifice a creature. If you do, return the chosen cards to the battlefield tapped.').
card_image_name('victimize'/'C14', 'victimize').
card_uid('victimize'/'C14', 'C14:Victimize:victimize').
card_rarity('victimize'/'C14', 'Uncommon').
card_artist('victimize'/'C14', 'Val Mayerik').
card_number('victimize'/'C14', '169').
card_flavor_text('victimize'/'C14', 'The priest cast Xantcha to the ground. \"It is defective. We must scrap it.\"').
card_multiverse_id('victimize'/'C14', '389736').

card_in_set('volcanic offering', 'C14').
card_original_type('volcanic offering'/'C14', 'Instant').
card_original_text('volcanic offering'/'C14', 'Destroy target nonbasic land you don\'t control and target nonbasic land of an opponent\'s choice you don\'t control.\nVolcanic Offering deals 7 damage to target creature you don\'t control and 7 damage to target creature of an opponent\'s choice you don\'t control.').
card_first_print('volcanic offering', 'C14').
card_image_name('volcanic offering'/'C14', 'volcanic offering').
card_uid('volcanic offering'/'C14', 'C14:Volcanic Offering:volcanic offering').
card_rarity('volcanic offering'/'C14', 'Rare').
card_artist('volcanic offering'/'C14', 'Eric Velhagen').
card_number('volcanic offering'/'C14', '40').
card_multiverse_id('volcanic offering'/'C14', '389737').

card_in_set('wake the dead', 'C14').
card_original_type('wake the dead'/'C14', 'Instant').
card_original_text('wake the dead'/'C14', 'Cast Wake the Dead only during combat on an opponent\'s turn.\nReturn X target creature cards from your graveyard to the battlefield. Sacrifice those creatures at the beginning of the next end step.').
card_first_print('wake the dead', 'C14').
card_image_name('wake the dead'/'C14', 'wake the dead').
card_uid('wake the dead'/'C14', 'C14:Wake the Dead:wake the dead').
card_rarity('wake the dead'/'C14', 'Rare').
card_artist('wake the dead'/'C14', 'Christopher Moeller').
card_number('wake the dead'/'C14', '31').
card_multiverse_id('wake the dead'/'C14', '389738').

card_in_set('warmonger hellkite', 'C14').
card_original_type('warmonger hellkite'/'C14', 'Creature — Dragon').
card_original_text('warmonger hellkite'/'C14', 'Flying\nAll creatures attack each combat if able.\n{1}{R}: Attacking creatures get +1/+0 until end of turn.').
card_first_print('warmonger hellkite', 'C14').
card_image_name('warmonger hellkite'/'C14', 'warmonger hellkite').
card_uid('warmonger hellkite'/'C14', 'C14:Warmonger Hellkite:warmonger hellkite').
card_rarity('warmonger hellkite'/'C14', 'Rare').
card_artist('warmonger hellkite'/'C14', 'Trevor Claxton').
card_number('warmonger hellkite'/'C14', '41').
card_flavor_text('warmonger hellkite'/'C14', 'Its wings beat to the howls of war and fan the fires of rage.').
card_multiverse_id('warmonger hellkite'/'C14', '389739').

card_in_set('wave of vitriol', 'C14').
card_original_type('wave of vitriol'/'C14', 'Sorcery').
card_original_text('wave of vitriol'/'C14', 'Each player sacrifices all artifacts, enchantments, and nonbasic lands he or she controls. For each land sacrificed this way, its controller may search his or her library for a basic land card and put it onto the battlefield tapped. Then each player who searched his or her library this way shuffles it.').
card_first_print('wave of vitriol', 'C14').
card_image_name('wave of vitriol'/'C14', 'wave of vitriol').
card_uid('wave of vitriol'/'C14', 'C14:Wave of Vitriol:wave of vitriol').
card_rarity('wave of vitriol'/'C14', 'Rare').
card_artist('wave of vitriol'/'C14', 'Zoltan Boros').
card_number('wave of vitriol'/'C14', '51').
card_multiverse_id('wave of vitriol'/'C14', '389740').

card_in_set('wayfarer\'s bauble', 'C14').
card_original_type('wayfarer\'s bauble'/'C14', 'Artifact').
card_original_text('wayfarer\'s bauble'/'C14', '{2}, {T}, Sacrifice Wayfarer\'s Bauble: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('wayfarer\'s bauble'/'C14', 'wayfarer\'s bauble').
card_uid('wayfarer\'s bauble'/'C14', 'C14:Wayfarer\'s Bauble:wayfarer\'s bauble').
card_rarity('wayfarer\'s bauble'/'C14', 'Common').
card_artist('wayfarer\'s bauble'/'C14', 'Darrell Riche').
card_number('wayfarer\'s bauble'/'C14', '281').
card_flavor_text('wayfarer\'s bauble'/'C14', 'It is the forest beyond the horizon, the mountain waiting to be climbed, the new land across the endless sea.').
card_multiverse_id('wayfarer\'s bauble'/'C14', '389741').

card_in_set('well of ideas', 'C14').
card_original_type('well of ideas'/'C14', 'Enchantment').
card_original_text('well of ideas'/'C14', 'When Well of Ideas enters the battlefield, draw two cards.\nAt the beginning of each other player\'s draw step, that player draws an additional card.\nAt the beginning of your draw step, draw two additional cards.').
card_first_print('well of ideas', 'C14').
card_image_name('well of ideas'/'C14', 'well of ideas').
card_uid('well of ideas'/'C14', 'C14:Well of Ideas:well of ideas').
card_rarity('well of ideas'/'C14', 'Rare').
card_artist('well of ideas'/'C14', 'Steve Prescott').
card_number('well of ideas'/'C14', '20').
card_multiverse_id('well of ideas'/'C14', '389742').

card_in_set('wellwisher', 'C14').
card_original_type('wellwisher'/'C14', 'Creature — Elf').
card_original_text('wellwisher'/'C14', '{T}: You gain 1 life for each Elf on the battlefield.').
card_image_name('wellwisher'/'C14', 'wellwisher').
card_uid('wellwisher'/'C14', 'C14:Wellwisher:wellwisher').
card_rarity('wellwisher'/'C14', 'Common').
card_artist('wellwisher'/'C14', 'Karl Kopinski').
card_number('wellwisher'/'C14', '223').
card_flavor_text('wellwisher'/'C14', '\"Close your ears to the voice of greed, and you can turn a gift for one into a gift for many.\"').
card_multiverse_id('wellwisher'/'C14', '389743').

card_in_set('whipflare', 'C14').
card_original_type('whipflare'/'C14', 'Sorcery').
card_original_text('whipflare'/'C14', 'Whipflare deals 2 damage to each nonartifact creature.').
card_image_name('whipflare'/'C14', 'whipflare').
card_uid('whipflare'/'C14', 'C14:Whipflare:whipflare').
card_rarity('whipflare'/'C14', 'Uncommon').
card_artist('whipflare'/'C14', 'Johann Bodin').
card_number('whipflare'/'C14', '184').
card_flavor_text('whipflare'/'C14', 'The slag-workers wasted no time in creating their own flare pulses to cleanse the area of the incompleat.').
card_multiverse_id('whipflare'/'C14', '389744').

card_in_set('whirlwind', 'C14').
card_original_type('whirlwind'/'C14', 'Sorcery').
card_original_text('whirlwind'/'C14', 'Destroy all creatures with flying.').
card_image_name('whirlwind'/'C14', 'whirlwind').
card_uid('whirlwind'/'C14', 'C14:Whirlwind:whirlwind').
card_rarity('whirlwind'/'C14', 'Rare').
card_artist('whirlwind'/'C14', 'John Matson').
card_number('whirlwind'/'C14', '224').
card_flavor_text('whirlwind'/'C14', 'Urza tried to rule the air, but Gaea taught him that she controlled all the elements.').
card_multiverse_id('whirlwind'/'C14', '389745').

card_in_set('white sun\'s zenith', 'C14').
card_original_type('white sun\'s zenith'/'C14', 'Instant').
card_original_text('white sun\'s zenith'/'C14', 'Put X 2/2 white Cat creature tokens onto the battlefield. Shuffle White Sun\'s Zenith into its owner\'s library.').
card_image_name('white sun\'s zenith'/'C14', 'white sun\'s zenith').
card_uid('white sun\'s zenith'/'C14', 'C14:White Sun\'s Zenith:white sun\'s zenith').
card_rarity('white sun\'s zenith'/'C14', 'Rare').
card_artist('white sun\'s zenith'/'C14', 'Mike Bierek').
card_number('white sun\'s zenith'/'C14', '95').
card_flavor_text('white sun\'s zenith'/'C14', 'After the Battle of Liet Field, the white sun crested above Taj-Nar, bringing hope to all who survived the carnage.').
card_multiverse_id('white sun\'s zenith'/'C14', '389746').

card_in_set('whitemane lion', 'C14').
card_original_type('whitemane lion'/'C14', 'Creature — Cat').
card_original_text('whitemane lion'/'C14', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Whitemane Lion enters the battlefield, return a creature you control to its owner\'s hand.').
card_image_name('whitemane lion'/'C14', 'whitemane lion').
card_uid('whitemane lion'/'C14', 'C14:Whitemane Lion:whitemane lion').
card_rarity('whitemane lion'/'C14', 'Common').
card_artist('whitemane lion'/'C14', 'Zoltan Boros & Gabor Szikszai').
card_number('whitemane lion'/'C14', '96').
card_flavor_text('whitemane lion'/'C14', 'Saltfield nomads call a sudden storm a \"whitemane.\"').
card_multiverse_id('whitemane lion'/'C14', '389747').

card_in_set('willbender', 'C14').
card_original_type('willbender'/'C14', 'Creature — Human Wizard').
card_original_text('willbender'/'C14', 'Morph {1}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Willbender is turned face up, change the target of target spell or ability with a single target.').
card_image_name('willbender'/'C14', 'willbender').
card_uid('willbender'/'C14', 'C14:Willbender:willbender').
card_rarity('willbender'/'C14', 'Uncommon').
card_artist('willbender'/'C14', 'Eric Peterson').
card_number('willbender'/'C14', '131').
card_multiverse_id('willbender'/'C14', '389748').

card_in_set('wing shards', 'C14').
card_original_type('wing shards'/'C14', 'Instant').
card_original_text('wing shards'/'C14', 'Target player sacrifices an attacking creature.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_image_name('wing shards'/'C14', 'wing shards').
card_uid('wing shards'/'C14', 'C14:Wing Shards:wing shards').
card_rarity('wing shards'/'C14', 'Uncommon').
card_artist('wing shards'/'C14', 'Daren Bader').
card_number('wing shards'/'C14', '97').
card_multiverse_id('wing shards'/'C14', '389749').

card_in_set('wolfbriar elemental', 'C14').
card_original_type('wolfbriar elemental'/'C14', 'Creature — Elemental').
card_original_text('wolfbriar elemental'/'C14', 'Multikicker {G} (You may pay an additional {G} any number of times as you cast this spell.)\nWhen Wolfbriar Elemental enters the battlefield, put a 2/2 green Wolf creature token onto the battlefield for each time it was kicked.').
card_image_name('wolfbriar elemental'/'C14', 'wolfbriar elemental').
card_uid('wolfbriar elemental'/'C14', 'C14:Wolfbriar Elemental:wolfbriar elemental').
card_rarity('wolfbriar elemental'/'C14', 'Rare').
card_artist('wolfbriar elemental'/'C14', 'Chippy').
card_number('wolfbriar elemental'/'C14', '225').
card_multiverse_id('wolfbriar elemental'/'C14', '389750').

card_in_set('wolfcaller\'s howl', 'C14').
card_original_type('wolfcaller\'s howl'/'C14', 'Enchantment').
card_original_text('wolfcaller\'s howl'/'C14', 'At the beginning of your upkeep, put X 2/2 green Wolf creature tokens onto the battlefield, where X is the number of your opponents with four or more cards in hand.').
card_first_print('wolfcaller\'s howl', 'C14').
card_image_name('wolfcaller\'s howl'/'C14', 'wolfcaller\'s howl').
card_uid('wolfcaller\'s howl'/'C14', 'C14:Wolfcaller\'s Howl:wolfcaller\'s howl').
card_rarity('wolfcaller\'s howl'/'C14', 'Rare').
card_artist('wolfcaller\'s howl'/'C14', 'Ralph Horsley').
card_number('wolfcaller\'s howl'/'C14', '52').
card_flavor_text('wolfcaller\'s howl'/'C14', '\"Knowledge is a stain that only instinct and ferocity can cleanse.\"\n—Drarg, wolfcaller').
card_multiverse_id('wolfcaller\'s howl'/'C14', '389751').

card_in_set('wood elves', 'C14').
card_original_type('wood elves'/'C14', 'Creature — Elf Scout').
card_original_text('wood elves'/'C14', 'When Wood Elves enters the battlefield, search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('wood elves'/'C14', 'wood elves').
card_uid('wood elves'/'C14', 'C14:Wood Elves:wood elves').
card_rarity('wood elves'/'C14', 'Common').
card_artist('wood elves'/'C14', 'Christopher Moeller').
card_number('wood elves'/'C14', '226').
card_flavor_text('wood elves'/'C14', 'Every branch a crossroads, every vine a swift steed.').
card_multiverse_id('wood elves'/'C14', '389752').

card_in_set('word of seizing', 'C14').
card_original_type('word of seizing'/'C14', 'Instant').
card_original_text('word of seizing'/'C14', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nUntap target permanent and gain control of it until end of turn. It gains haste until end of turn.').
card_image_name('word of seizing'/'C14', 'word of seizing').
card_uid('word of seizing'/'C14', 'C14:Word of Seizing:word of seizing').
card_rarity('word of seizing'/'C14', 'Rare').
card_artist('word of seizing'/'C14', 'Vance Kovacs').
card_number('word of seizing'/'C14', '185').
card_flavor_text('word of seizing'/'C14', '\"Obey!\"').
card_multiverse_id('word of seizing'/'C14', '389753').

card_in_set('worn powerstone', 'C14').
card_original_type('worn powerstone'/'C14', 'Artifact').
card_original_text('worn powerstone'/'C14', 'Worn Powerstone enters the battlefield tapped.\n{T}: Add {2} to your mana pool.').
card_image_name('worn powerstone'/'C14', 'worn powerstone').
card_uid('worn powerstone'/'C14', 'C14:Worn Powerstone:worn powerstone').
card_rarity('worn powerstone'/'C14', 'Uncommon').
card_artist('worn powerstone'/'C14', 'Henry G. Higgenbotham').
card_number('worn powerstone'/'C14', '282').
card_flavor_text('worn powerstone'/'C14', 'Even a fragment of a powerstone contains within it not just energy, but space—vast dimensions trapped in fragile crystal.').
card_multiverse_id('worn powerstone'/'C14', '389754').

card_in_set('wren\'s run packmaster', 'C14').
card_original_type('wren\'s run packmaster'/'C14', 'Creature — Elf Warrior').
card_original_text('wren\'s run packmaster'/'C14', 'Champion an Elf (When this creature enters the battlefield, sacrifice it unless you exile another Elf you control. When this creature leaves the battlefield, that card returns to the battlefield.)\n{2}{G}: Put a 2/2 green Wolf creature token onto the battlefield.\nWolves you control have deathtouch.').
card_image_name('wren\'s run packmaster'/'C14', 'wren\'s run packmaster').
card_uid('wren\'s run packmaster'/'C14', 'C14:Wren\'s Run Packmaster:wren\'s run packmaster').
card_rarity('wren\'s run packmaster'/'C14', 'Rare').
card_artist('wren\'s run packmaster'/'C14', 'Mark Zug').
card_number('wren\'s run packmaster'/'C14', '227').
card_multiverse_id('wren\'s run packmaster'/'C14', '389755').

card_in_set('wurmcoil engine', 'C14').
card_original_type('wurmcoil engine'/'C14', 'Artifact Creature — Wurm').
card_original_text('wurmcoil engine'/'C14', 'Deathtouch, lifelink\nWhen Wurmcoil Engine dies, put a 3/3 colorless Wurm artifact creature token with deathtouch and a 3/3 colorless Wurm artifact creature token with lifelink onto the battlefield.').
card_image_name('wurmcoil engine'/'C14', 'wurmcoil engine').
card_uid('wurmcoil engine'/'C14', 'C14:Wurmcoil Engine:wurmcoil engine').
card_rarity('wurmcoil engine'/'C14', 'Mythic Rare').
card_artist('wurmcoil engine'/'C14', 'Raymond Swanland').
card_number('wurmcoil engine'/'C14', '283').
card_multiverse_id('wurmcoil engine'/'C14', '389756').

card_in_set('xathrid demon', 'C14').
card_original_type('xathrid demon'/'C14', 'Creature — Demon').
card_original_text('xathrid demon'/'C14', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Xathrid Demon, then each opponent loses life equal to the sacrificed creature\'s power. If you can\'t sacrifice a creature, tap Xathrid Demon and you lose 7 life.').
card_image_name('xathrid demon'/'C14', 'xathrid demon').
card_uid('xathrid demon'/'C14', 'C14:Xathrid Demon:xathrid demon').
card_rarity('xathrid demon'/'C14', 'Mythic Rare').
card_artist('xathrid demon'/'C14', 'Wayne Reynolds').
card_number('xathrid demon'/'C14', '170').
card_multiverse_id('xathrid demon'/'C14', '389757').

card_in_set('zoetic cavern', 'C14').
card_original_type('zoetic cavern'/'C14', 'Land').
card_original_text('zoetic cavern'/'C14', '{T}: Add {1} to your mana pool.\nMorph {2} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('zoetic cavern'/'C14', 'zoetic cavern').
card_uid('zoetic cavern'/'C14', 'C14:Zoetic Cavern:zoetic cavern').
card_rarity('zoetic cavern'/'C14', 'Uncommon').
card_artist('zoetic cavern'/'C14', 'Lars Grant-West').
card_number('zoetic cavern'/'C14', '317').
card_flavor_text('zoetic cavern'/'C14', 'Nothing lives within it, yet there is life.').
card_multiverse_id('zoetic cavern'/'C14', '389758').
