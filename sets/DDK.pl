% Duel Decks: Sorin vs. Tibalt

set('DDK').
set_name('DDK', 'Duel Decks: Sorin vs. Tibalt').
set_release_date('DDK', '2013-03-15').
set_border('DDK', 'black').
set_type('DDK', 'duel deck').

card_in_set('absorb vis', 'DDK').
card_original_type('absorb vis'/'DDK', 'Sorcery').
card_original_text('absorb vis'/'DDK', 'Target player loses 4 life and you gain 4 life.\nBasic landcycling {1}{B} ({1}{B}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('absorb vis'/'DDK', 'absorb vis').
card_uid('absorb vis'/'DDK', 'DDK:Absorb Vis:absorb vis').
card_rarity('absorb vis'/'DDK', 'Common').
card_artist('absorb vis'/'DDK', 'Brandon Kitkouski').
card_number('absorb vis'/'DDK', '31').
card_multiverse_id('absorb vis'/'DDK', '368527').

card_in_set('akoum refuge', 'DDK').
card_original_type('akoum refuge'/'DDK', 'Land').
card_original_text('akoum refuge'/'DDK', 'Akoum Refuge enters the battlefield tapped.\nWhen Akoum Refuge enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('akoum refuge'/'DDK', 'akoum refuge').
card_uid('akoum refuge'/'DDK', 'DDK:Akoum Refuge:akoum refuge').
card_rarity('akoum refuge'/'DDK', 'Uncommon').
card_artist('akoum refuge'/'DDK', 'Fred Fields').
card_number('akoum refuge'/'DDK', '73').
card_multiverse_id('akoum refuge'/'DDK', '368523').

card_in_set('ancient craving', 'DDK').
card_original_type('ancient craving'/'DDK', 'Sorcery').
card_original_text('ancient craving'/'DDK', 'You draw three cards and you lose 3 life.').
card_image_name('ancient craving'/'DDK', 'ancient craving').
card_uid('ancient craving'/'DDK', 'DDK:Ancient Craving:ancient craving').
card_rarity('ancient craving'/'DDK', 'Rare').
card_artist('ancient craving'/'DDK', 'Rob Alexander').
card_number('ancient craving'/'DDK', '28').
card_flavor_text('ancient craving'/'DDK', 'Knowledge demands sacrifice.').
card_multiverse_id('ancient craving'/'DDK', '368540').

card_in_set('ashmouth hound', 'DDK').
card_original_type('ashmouth hound'/'DDK', 'Creature — Elemental Hound').
card_original_text('ashmouth hound'/'DDK', 'Whenever Ashmouth Hound blocks or becomes blocked by a creature, Ashmouth Hound deals 1 damage to that creature.').
card_image_name('ashmouth hound'/'DDK', 'ashmouth hound').
card_uid('ashmouth hound'/'DDK', 'DDK:Ashmouth Hound:ashmouth hound').
card_rarity('ashmouth hound'/'DDK', 'Common').
card_artist('ashmouth hound'/'DDK', 'Daarken').
card_number('ashmouth hound'/'DDK', '45').
card_flavor_text('ashmouth hound'/'DDK', 'Its fiery paws make it easy to track, which is very useful when you want to go in exactly the opposite direction.').
card_multiverse_id('ashmouth hound'/'DDK', '368476').

card_in_set('blazing salvo', 'DDK').
card_original_type('blazing salvo'/'DDK', 'Instant').
card_original_text('blazing salvo'/'DDK', 'Blazing Salvo deals 3 damage to target creature unless that creature\'s controller has Blazing Salvo deal 5 damage to him or her.').
card_image_name('blazing salvo'/'DDK', 'blazing salvo').
card_uid('blazing salvo'/'DDK', 'DDK:Blazing Salvo:blazing salvo').
card_rarity('blazing salvo'/'DDK', 'Common').
card_artist('blazing salvo'/'DDK', 'rk post').
card_number('blazing salvo'/'DDK', '58').
card_flavor_text('blazing salvo'/'DDK', 'Barbarians equate courage with the ability to endure pain.').
card_multiverse_id('blazing salvo'/'DDK', '368507').

card_in_set('blightning', 'DDK').
card_original_type('blightning'/'DDK', 'Sorcery').
card_original_text('blightning'/'DDK', 'Blightning deals 3 damage to target player. That player discards two cards.').
card_image_name('blightning'/'DDK', 'blightning').
card_uid('blightning'/'DDK', 'DDK:Blightning:blightning').
card_rarity('blightning'/'DDK', 'Common').
card_artist('blightning'/'DDK', 'Dan Scott').
card_number('blightning'/'DDK', '69').
card_flavor_text('blightning'/'DDK', 'In the spirit of inquiry, Tibalt applied a battery of agony spells. They yielded nothing on the vampire\'s whereabouts, but the screams were reward enough.').
card_multiverse_id('blightning'/'DDK', '368483').

card_in_set('bloodrage vampire', 'DDK').
card_original_type('bloodrage vampire'/'DDK', 'Creature — Vampire').
card_original_text('bloodrage vampire'/'DDK', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)').
card_image_name('bloodrage vampire'/'DDK', 'bloodrage vampire').
card_uid('bloodrage vampire'/'DDK', 'DDK:Bloodrage Vampire:bloodrage vampire').
card_rarity('bloodrage vampire'/'DDK', 'Common').
card_artist('bloodrage vampire'/'DDK', 'Steve Prescott').
card_number('bloodrage vampire'/'DDK', '10').
card_flavor_text('bloodrage vampire'/'DDK', 'The most genteel facades hide the most abominable desires.').
card_multiverse_id('bloodrage vampire'/'DDK', '368543').

card_in_set('breaking point', 'DDK').
card_original_type('breaking point'/'DDK', 'Sorcery').
card_original_text('breaking point'/'DDK', 'Any player may have Breaking Point deal 6 damage to him or her. If no one does, destroy all creatures. Creatures destroyed this way can\'t be regenerated.').
card_image_name('breaking point'/'DDK', 'breaking point').
card_uid('breaking point'/'DDK', 'DDK:Breaking Point:breaking point').
card_rarity('breaking point'/'DDK', 'Rare').
card_artist('breaking point'/'DDK', 'Matthew D. Wilson').
card_number('breaking point'/'DDK', '67').
card_flavor_text('breaking point'/'DDK', '\"Enough!\"').
card_multiverse_id('breaking point'/'DDK', '368486').

card_in_set('browbeat', 'DDK').
card_original_type('browbeat'/'DDK', 'Sorcery').
card_original_text('browbeat'/'DDK', 'Any player may have Browbeat deal 5 damage to him or her. If no one does, target player draws three cards.').
card_image_name('browbeat'/'DDK', 'browbeat').
card_uid('browbeat'/'DDK', 'DDK:Browbeat:browbeat').
card_rarity('browbeat'/'DDK', 'Uncommon').
card_artist('browbeat'/'DDK', 'Chris Rahn').
card_number('browbeat'/'DDK', '66').
card_flavor_text('browbeat'/'DDK', '\"Pained defiance or shameful surrender—every reaction is a pleasant surprise.\"\n—Tibalt').
card_multiverse_id('browbeat'/'DDK', '368528').

card_in_set('bump in the night', 'DDK').
card_original_type('bump in the night'/'DDK', 'Sorcery').
card_original_text('bump in the night'/'DDK', 'Target opponent loses 3 life.\nFlashback {5}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('bump in the night'/'DDK', 'bump in the night').
card_uid('bump in the night'/'DDK', 'DDK:Bump in the Night:bump in the night').
card_rarity('bump in the night'/'DDK', 'Common').
card_artist('bump in the night'/'DDK', 'Kev Walker').
card_number('bump in the night'/'DDK', '57').
card_flavor_text('bump in the night'/'DDK', 'It\'s not just the wind. It\'s not all in your head. And it\'s definitely something to worry about.').
card_multiverse_id('bump in the night'/'DDK', '368490').

card_in_set('butcher of malakir', 'DDK').
card_original_type('butcher of malakir'/'DDK', 'Creature — Vampire Warrior').
card_original_text('butcher of malakir'/'DDK', 'Flying\nWhenever Butcher of Malakir or another creature you control dies, each opponent sacrifices a creature.').
card_image_name('butcher of malakir'/'DDK', 'butcher of malakir').
card_uid('butcher of malakir'/'DDK', 'DDK:Butcher of Malakir:butcher of malakir').
card_rarity('butcher of malakir'/'DDK', 'Rare').
card_artist('butcher of malakir'/'DDK', 'Jason Chan').
card_number('butcher of malakir'/'DDK', '18').
card_flavor_text('butcher of malakir'/'DDK', 'His verdict is always guilty. His sentence is always death.').
card_multiverse_id('butcher of malakir'/'DDK', '368501').

card_in_set('child of night', 'DDK').
card_original_type('child of night'/'DDK', 'Creature — Vampire').
card_original_text('child of night'/'DDK', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('child of night'/'DDK', 'child of night').
card_uid('child of night'/'DDK', 'DDK:Child of Night:child of night').
card_rarity('child of night'/'DDK', 'Common').
card_artist('child of night'/'DDK', 'Ash Wood').
card_number('child of night'/'DDK', '5').
card_flavor_text('child of night'/'DDK', 'Sins that would be too gruesome in the light of day are made more pleasing in the dark of night.').
card_multiverse_id('child of night'/'DDK', '368520').

card_in_set('coal stoker', 'DDK').
card_original_type('coal stoker'/'DDK', 'Creature — Elemental').
card_original_text('coal stoker'/'DDK', 'When Coal Stoker enters the battlefield, if you cast it from your hand, add {R}{R}{R} to your mana pool.').
card_image_name('coal stoker'/'DDK', 'coal stoker').
card_uid('coal stoker'/'DDK', 'DDK:Coal Stoker:coal stoker').
card_rarity('coal stoker'/'DDK', 'Common').
card_artist('coal stoker'/'DDK', 'Mark Zug').
card_number('coal stoker'/'DDK', '49').
card_flavor_text('coal stoker'/'DDK', '\"The day is mine! I sent three such creatures against my foe, then watched as my magefire popped her soldiers like overripe spleenfruits.\"\n—Dobruk the Unstable, pyromancer').
card_multiverse_id('coal stoker'/'DDK', '368533').

card_in_set('corpse connoisseur', 'DDK').
card_original_type('corpse connoisseur'/'DDK', 'Creature — Zombie Wizard').
card_original_text('corpse connoisseur'/'DDK', 'When Corpse Connoisseur enters the battlefield, you may search your library for a creature card and put that card into your graveyard. If you do, shuffle your library.\nUnearth {3}{B} ({3}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('corpse connoisseur'/'DDK', 'corpse connoisseur').
card_uid('corpse connoisseur'/'DDK', 'DDK:Corpse Connoisseur:corpse connoisseur').
card_rarity('corpse connoisseur'/'DDK', 'Uncommon').
card_artist('corpse connoisseur'/'DDK', 'Mark Hyzer').
card_number('corpse connoisseur'/'DDK', '54').
card_multiverse_id('corpse connoisseur'/'DDK', '368487').

card_in_set('death grasp', 'DDK').
card_original_type('death grasp'/'DDK', 'Sorcery').
card_original_text('death grasp'/'DDK', 'Death Grasp deals X damage to target creature or player. You gain X life.').
card_image_name('death grasp'/'DDK', 'death grasp').
card_uid('death grasp'/'DDK', 'DDK:Death Grasp:death grasp').
card_rarity('death grasp'/'DDK', 'Rare').
card_artist('death grasp'/'DDK', 'Raymond Swanland').
card_number('death grasp'/'DDK', '32').
card_flavor_text('death grasp'/'DDK', 'Plate mail, the skin of the chest, the rib cage, the heart—all useless against the mage who can reach directly for the soul.').
card_multiverse_id('death grasp'/'DDK', '368495').

card_in_set('decompose', 'DDK').
card_original_type('decompose'/'DDK', 'Sorcery').
card_original_text('decompose'/'DDK', 'Exile up to three target cards from a single graveyard.').
card_image_name('decompose'/'DDK', 'decompose').
card_uid('decompose'/'DDK', 'DDK:Decompose:decompose').
card_rarity('decompose'/'DDK', 'Uncommon').
card_artist('decompose'/'DDK', 'Tony Szczudlo').
card_number('decompose'/'DDK', '20').
card_flavor_text('decompose'/'DDK', '\"Sheesh! How am I supposed to make a decent living around here?\"\n—Cabal grave robber').
card_multiverse_id('decompose'/'DDK', '368506').

card_in_set('devil\'s play', 'DDK').
card_original_type('devil\'s play'/'DDK', 'Sorcery').
card_original_text('devil\'s play'/'DDK', 'Devil\'s Play deals X damage to target creature or player.\nFlashback {X}{R}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('devil\'s play'/'DDK', 'devil\'s play').
card_uid('devil\'s play'/'DDK', 'DDK:Devil\'s Play:devil\'s play').
card_rarity('devil\'s play'/'DDK', 'Rare').
card_artist('devil\'s play'/'DDK', 'Austin Hsu').
card_number('devil\'s play'/'DDK', '72').
card_flavor_text('devil\'s play'/'DDK', 'A devil\'s hands are never idle.').
card_multiverse_id('devil\'s play'/'DDK', '368518').

card_in_set('doomed traveler', 'DDK').
card_original_type('doomed traveler'/'DDK', 'Creature — Human Soldier').
card_original_text('doomed traveler'/'DDK', 'When Doomed Traveler dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('doomed traveler'/'DDK', 'doomed traveler').
card_uid('doomed traveler'/'DDK', 'DDK:Doomed Traveler:doomed traveler').
card_rarity('doomed traveler'/'DDK', 'Common').
card_artist('doomed traveler'/'DDK', 'Lars Grant-West').
card_number('doomed traveler'/'DDK', '2').
card_flavor_text('doomed traveler'/'DDK', 'He vowed he would never rest until he reached his destination. He doesn\'t know how right he was.').
card_multiverse_id('doomed traveler'/'DDK', '368503').

card_in_set('duskhunter bat', 'DDK').
card_original_type('duskhunter bat'/'DDK', 'Creature — Bat').
card_original_text('duskhunter bat'/'DDK', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFlying').
card_image_name('duskhunter bat'/'DDK', 'duskhunter bat').
card_uid('duskhunter bat'/'DDK', 'DDK:Duskhunter Bat:duskhunter bat').
card_rarity('duskhunter bat'/'DDK', 'Common').
card_artist('duskhunter bat'/'DDK', 'Jesper Ejsing').
card_number('duskhunter bat'/'DDK', '6').
card_flavor_text('duskhunter bat'/'DDK', 'The swamps go silent at its approach, but it still hears the heartbeat of the prey within.').
card_multiverse_id('duskhunter bat'/'DDK', '368541').

card_in_set('evolving wilds', 'DDK').
card_original_type('evolving wilds'/'DDK', 'Land').
card_original_text('evolving wilds'/'DDK', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'DDK', 'evolving wilds').
card_uid('evolving wilds'/'DDK', 'DDK:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'DDK', 'Common').
card_artist('evolving wilds'/'DDK', 'Steven Belledin').
card_number('evolving wilds'/'DDK', '33').
card_flavor_text('evolving wilds'/'DDK', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'DDK', '368505').

card_in_set('faithless looting', 'DDK').
card_original_type('faithless looting'/'DDK', 'Sorcery').
card_original_text('faithless looting'/'DDK', 'Draw two cards, then discard two cards.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('faithless looting'/'DDK', 'faithless looting').
card_uid('faithless looting'/'DDK', 'DDK:Faithless Looting:faithless looting').
card_rarity('faithless looting'/'DDK', 'Common').
card_artist('faithless looting'/'DDK', 'Gabor Szikszai').
card_number('faithless looting'/'DDK', '59').
card_flavor_text('faithless looting'/'DDK', '\"Avacyn has abandoned us! We have nothing left except what we can take!\"').
card_multiverse_id('faithless looting'/'DDK', '368475').

card_in_set('field of souls', 'DDK').
card_original_type('field of souls'/'DDK', 'Enchantment').
card_original_text('field of souls'/'DDK', 'Whenever a nontoken creature is put into your graveyard from the battlefield, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('field of souls'/'DDK', 'field of souls').
card_uid('field of souls'/'DDK', 'DDK:Field of Souls:field of souls').
card_rarity('field of souls'/'DDK', 'Rare').
card_artist('field of souls'/'DDK', 'Richard Kane Ferguson').
card_number('field of souls'/'DDK', '30').
card_multiverse_id('field of souls'/'DDK', '368510').

card_in_set('fiend hunter', 'DDK').
card_original_type('fiend hunter'/'DDK', 'Creature — Human Cleric').
card_original_text('fiend hunter'/'DDK', 'When Fiend Hunter enters the battlefield, you may exile another target creature.\nWhen Fiend Hunter leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('fiend hunter'/'DDK', 'fiend hunter').
card_uid('fiend hunter'/'DDK', 'DDK:Fiend Hunter:fiend hunter').
card_rarity('fiend hunter'/'DDK', 'Uncommon').
card_artist('fiend hunter'/'DDK', 'Wayne Reynolds').
card_number('fiend hunter'/'DDK', '11').
card_multiverse_id('fiend hunter'/'DDK', '368496').

card_in_set('flame javelin', 'DDK').
card_original_type('flame javelin'/'DDK', 'Instant').
card_original_text('flame javelin'/'DDK', 'Flame Javelin deals 4 damage to target creature or player.').
card_image_name('flame javelin'/'DDK', 'flame javelin').
card_uid('flame javelin'/'DDK', 'DDK:Flame Javelin:flame javelin').
card_rarity('flame javelin'/'DDK', 'Uncommon').
card_artist('flame javelin'/'DDK', 'Trevor Hairsine').
card_number('flame javelin'/'DDK', '70').
card_flavor_text('flame javelin'/'DDK', 'Gyara Spearhurler would have been renowned for her deadly accuracy, if it weren\'t for her deadly accuracy.').
card_multiverse_id('flame javelin'/'DDK', '368544').

card_in_set('flame slash', 'DDK').
card_original_type('flame slash'/'DDK', 'Sorcery').
card_original_text('flame slash'/'DDK', 'Flame Slash deals 4 damage to target creature.').
card_image_name('flame slash'/'DDK', 'flame slash').
card_uid('flame slash'/'DDK', 'DDK:Flame Slash:flame slash').
card_rarity('flame slash'/'DDK', 'Common').
card_artist('flame slash'/'DDK', 'Raymond Swanland').
card_number('flame slash'/'DDK', '60').
card_flavor_text('flame slash'/'DDK', 'After millennia asleep, the Eldrazi had forgotten about Zendikar\'s fiery temper and dislike of strangers.').
card_multiverse_id('flame slash'/'DDK', '368536').

card_in_set('gang of devils', 'DDK').
card_original_type('gang of devils'/'DDK', 'Creature — Devil').
card_original_text('gang of devils'/'DDK', 'When Gang of Devils dies, it deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_image_name('gang of devils'/'DDK', 'gang of devils').
card_uid('gang of devils'/'DDK', 'DDK:Gang of Devils:gang of devils').
card_rarity('gang of devils'/'DDK', 'Uncommon').
card_artist('gang of devils'/'DDK', 'Erica Yang').
card_number('gang of devils'/'DDK', '56').
card_flavor_text('gang of devils'/'DDK', '\"Well, how could I know they\'d explode?\"\n—Gregel, township militia').
card_multiverse_id('gang of devils'/'DDK', '368539').

card_in_set('gatekeeper of malakir', 'DDK').
card_original_type('gatekeeper of malakir'/'DDK', 'Creature — Vampire Warrior').
card_original_text('gatekeeper of malakir'/'DDK', 'Kicker {B} (You may pay an additional {B} as you cast this spell.)\nWhen Gatekeeper of Malakir enters the battlefield, if it was kicked, target player sacrifices a creature.').
card_image_name('gatekeeper of malakir'/'DDK', 'gatekeeper of malakir').
card_uid('gatekeeper of malakir'/'DDK', 'DDK:Gatekeeper of Malakir:gatekeeper of malakir').
card_rarity('gatekeeper of malakir'/'DDK', 'Uncommon').
card_artist('gatekeeper of malakir'/'DDK', 'Karl Kopinski').
card_number('gatekeeper of malakir'/'DDK', '8').
card_flavor_text('gatekeeper of malakir'/'DDK', '\"You may enter the city—once the toll is paid.\"').
card_multiverse_id('gatekeeper of malakir'/'DDK', '368479').

card_in_set('geistflame', 'DDK').
card_original_type('geistflame'/'DDK', 'Instant').
card_original_text('geistflame'/'DDK', 'Geistflame deals 1 damage to target creature or player.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('geistflame'/'DDK', 'geistflame').
card_uid('geistflame'/'DDK', 'DDK:Geistflame:geistflame').
card_rarity('geistflame'/'DDK', 'Common').
card_artist('geistflame'/'DDK', 'Scott Chou').
card_number('geistflame'/'DDK', '61').
card_flavor_text('geistflame'/'DDK', 'Innistrad pyromancers find their best raw materials in the fury of the dead.').
card_multiverse_id('geistflame'/'DDK', '368524').

card_in_set('goblin arsonist', 'DDK').
card_original_type('goblin arsonist'/'DDK', 'Creature — Goblin Shaman').
card_original_text('goblin arsonist'/'DDK', 'When Goblin Arsonist dies, you may have it deal 1 damage to target creature or player.').
card_image_name('goblin arsonist'/'DDK', 'goblin arsonist').
card_uid('goblin arsonist'/'DDK', 'DDK:Goblin Arsonist:goblin arsonist').
card_rarity('goblin arsonist'/'DDK', 'Common').
card_artist('goblin arsonist'/'DDK', 'Wayne Reynolds').
card_number('goblin arsonist'/'DDK', '42').
card_flavor_text('goblin arsonist'/'DDK', 'With great power comes great risk of getting yourself killed.').
card_multiverse_id('goblin arsonist'/'DDK', '368478').

card_in_set('hellrider', 'DDK').
card_original_type('hellrider'/'DDK', 'Creature — Devil').
card_original_text('hellrider'/'DDK', 'Haste\nWhenever a creature you control attacks, Hellrider deals 1 damage to defending player.').
card_image_name('hellrider'/'DDK', 'hellrider').
card_uid('hellrider'/'DDK', 'DDK:Hellrider:hellrider').
card_rarity('hellrider'/'DDK', 'Rare').
card_artist('hellrider'/'DDK', 'Svetlin Velinov').
card_number('hellrider'/'DDK', '52').
card_flavor_text('hellrider'/'DDK', '\"Behind every devil\'s mayhem lurks a demon\'s scheme.\"\n—Rem Karolus, Blade of the Inquisitors').
card_multiverse_id('hellrider'/'DDK', '368530').

card_in_set('hellspark elemental', 'DDK').
card_original_type('hellspark elemental'/'DDK', 'Creature — Elemental').
card_original_text('hellspark elemental'/'DDK', 'Trample, haste\nAt the beginning of the end step, sacrifice Hellspark Elemental.\nUnearth {1}{R} ({1}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('hellspark elemental'/'DDK', 'hellspark elemental').
card_uid('hellspark elemental'/'DDK', 'DDK:Hellspark Elemental:hellspark elemental').
card_rarity('hellspark elemental'/'DDK', 'Uncommon').
card_artist('hellspark elemental'/'DDK', 'Justin Sweet').
card_number('hellspark elemental'/'DDK', '46').
card_multiverse_id('hellspark elemental'/'DDK', '368477').

card_in_set('lavaborn muse', 'DDK').
card_original_type('lavaborn muse'/'DDK', 'Creature — Spirit').
card_original_text('lavaborn muse'/'DDK', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Lavaborn Muse deals 3 damage to him or her.').
card_image_name('lavaborn muse'/'DDK', 'lavaborn muse').
card_uid('lavaborn muse'/'DDK', 'DDK:Lavaborn Muse:lavaborn muse').
card_rarity('lavaborn muse'/'DDK', 'Rare').
card_artist('lavaborn muse'/'DDK', 'Brian Snõddy').
card_number('lavaborn muse'/'DDK', '50').
card_flavor_text('lavaborn muse'/'DDK', '\"Her voice is disaster, painful and final.\"\n—Matoc, lavamancer').
card_multiverse_id('lavaborn muse'/'DDK', '368526').

card_in_set('lingering souls', 'DDK').
card_original_type('lingering souls'/'DDK', 'Sorcery').
card_original_text('lingering souls'/'DDK', 'Put two 1/1 white Spirit creature tokens with flying onto the battlefield.\nFlashback {1}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('lingering souls'/'DDK', 'lingering souls').
card_uid('lingering souls'/'DDK', 'DDK:Lingering Souls:lingering souls').
card_rarity('lingering souls'/'DDK', 'Uncommon').
card_artist('lingering souls'/'DDK', 'John Stanko').
card_number('lingering souls'/'DDK', '24').
card_flavor_text('lingering souls'/'DDK', 'The murdered inhabitants of Hollowhenge impart to the living the terror they felt in death.').
card_multiverse_id('lingering souls'/'DDK', '368485').

card_in_set('mad prophet', 'DDK').
card_original_type('mad prophet'/'DDK', 'Creature — Human Shaman').
card_original_text('mad prophet'/'DDK', 'Haste\n{T}, Discard a card: Draw a card.').
card_image_name('mad prophet'/'DDK', 'mad prophet').
card_uid('mad prophet'/'DDK', 'DDK:Mad Prophet:mad prophet').
card_rarity('mad prophet'/'DDK', 'Common').
card_artist('mad prophet'/'DDK', 'Wayne Reynolds').
card_number('mad prophet'/'DDK', '51').
card_flavor_text('mad prophet'/'DDK', '\"There\'s no heron in the moon! It\'s a shrew, a five-legged shrew, with a voice like whispering thunder!\"').
card_multiverse_id('mad prophet'/'DDK', '368474').

card_in_set('mark of the vampire', 'DDK').
card_original_type('mark of the vampire'/'DDK', 'Enchantment — Aura').
card_original_text('mark of the vampire'/'DDK', 'Enchant creature\nEnchanted creature gets +2/+2 and has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_image_name('mark of the vampire'/'DDK', 'mark of the vampire').
card_uid('mark of the vampire'/'DDK', 'DDK:Mark of the Vampire:mark of the vampire').
card_rarity('mark of the vampire'/'DDK', 'Common').
card_artist('mark of the vampire'/'DDK', 'Winona Nelson').
card_number('mark of the vampire'/'DDK', '29').
card_flavor_text('mark of the vampire'/'DDK', '\"My ‘condition\' is a trial. The weak are consumed by it. The strong transcend it.\"\n—Sorin Markov').
card_multiverse_id('mark of the vampire'/'DDK', '368499').

card_in_set('mausoleum guard', 'DDK').
card_original_type('mausoleum guard'/'DDK', 'Creature — Human Scout').
card_original_text('mausoleum guard'/'DDK', 'When Mausoleum Guard dies, put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('mausoleum guard'/'DDK', 'mausoleum guard').
card_uid('mausoleum guard'/'DDK', 'DDK:Mausoleum Guard:mausoleum guard').
card_rarity('mausoleum guard'/'DDK', 'Uncommon').
card_artist('mausoleum guard'/'DDK', 'David Palumbo').
card_number('mausoleum guard'/'DDK', '13').
card_flavor_text('mausoleum guard'/'DDK', '\"Ghoulcallers trying to get in, geists trying to get out . . . . This duty is never dull.\"').
card_multiverse_id('mausoleum guard'/'DDK', '368547').

card_in_set('mesmeric fiend', 'DDK').
card_original_type('mesmeric fiend'/'DDK', 'Creature — Nightmare Horror').
card_original_text('mesmeric fiend'/'DDK', 'When Mesmeric Fiend enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card.\nWhen Mesmeric Fiend leaves the battlefield, return the exiled card to its owner\'s hand.').
card_image_name('mesmeric fiend'/'DDK', 'mesmeric fiend').
card_uid('mesmeric fiend'/'DDK', 'DDK:Mesmeric Fiend:mesmeric fiend').
card_rarity('mesmeric fiend'/'DDK', 'Common').
card_artist('mesmeric fiend'/'DDK', 'Dana Knutson').
card_number('mesmeric fiend'/'DDK', '7').
card_multiverse_id('mesmeric fiend'/'DDK', '368519').

card_in_set('mortify', 'DDK').
card_original_type('mortify'/'DDK', 'Instant').
card_original_text('mortify'/'DDK', 'Destroy target creature or enchantment.').
card_image_name('mortify'/'DDK', 'mortify').
card_uid('mortify'/'DDK', 'DDK:Mortify:mortify').
card_rarity('mortify'/'DDK', 'Uncommon').
card_artist('mortify'/'DDK', 'Nils Hamm').
card_number('mortify'/'DDK', '25').
card_flavor_text('mortify'/'DDK', 'Many who cross Sorin\'s path come down with a sudden and fatal case of being-in-the-way-of-a-millennia-old-vampire.').
card_multiverse_id('mortify'/'DDK', '368498').

card_in_set('mountain', 'DDK').
card_original_type('mountain'/'DDK', 'Basic Land — Mountain').
card_original_text('mountain'/'DDK', 'B').
card_image_name('mountain'/'DDK', 'mountain1').
card_uid('mountain'/'DDK', 'DDK:Mountain:mountain1').
card_rarity('mountain'/'DDK', 'Basic Land').
card_artist('mountain'/'DDK', 'James Paick').
card_number('mountain'/'DDK', '75').
card_multiverse_id('mountain'/'DDK', '368502').

card_in_set('mountain', 'DDK').
card_original_type('mountain'/'DDK', 'Basic Land — Mountain').
card_original_text('mountain'/'DDK', 'R').
card_image_name('mountain'/'DDK', 'mountain2').
card_uid('mountain'/'DDK', 'DDK:Mountain:mountain2').
card_rarity('mountain'/'DDK', 'Basic Land').
card_artist('mountain'/'DDK', 'Adam Paquette').
card_number('mountain'/'DDK', '76').
card_multiverse_id('mountain'/'DDK', '368500').

card_in_set('mountain', 'DDK').
card_original_type('mountain'/'DDK', 'Basic Land — Mountain').
card_original_text('mountain'/'DDK', 'R').
card_image_name('mountain'/'DDK', 'mountain3').
card_uid('mountain'/'DDK', 'DDK:Mountain:mountain3').
card_rarity('mountain'/'DDK', 'Basic Land').
card_artist('mountain'/'DDK', 'Eytan Zana').
card_number('mountain'/'DDK', '77').
card_multiverse_id('mountain'/'DDK', '368512').

card_in_set('phantom general', 'DDK').
card_original_type('phantom general'/'DDK', 'Creature — Spirit Soldier').
card_original_text('phantom general'/'DDK', 'Creature tokens you control get +1/+1.').
card_image_name('phantom general'/'DDK', 'phantom general').
card_uid('phantom general'/'DDK', 'DDK:Phantom General:phantom general').
card_rarity('phantom general'/'DDK', 'Uncommon').
card_artist('phantom general'/'DDK', 'Christopher Moeller').
card_number('phantom general'/'DDK', '14').
card_flavor_text('phantom general'/'DDK', 'After his death, the general was reunited with fallen soldiers from over thirty of his campaigns.').
card_multiverse_id('phantom general'/'DDK', '368546').

card_in_set('plains', 'DDK').
card_original_type('plains'/'DDK', 'Basic Land — Plains').
card_original_text('plains'/'DDK', 'W').
card_image_name('plains'/'DDK', 'plains1').
card_uid('plains'/'DDK', 'DDK:Plains:plains1').
card_rarity('plains'/'DDK', 'Basic Land').
card_artist('plains'/'DDK', 'Adam Paquette').
card_number('plains'/'DDK', '38').
card_multiverse_id('plains'/'DDK', '368473').

card_in_set('plains', 'DDK').
card_original_type('plains'/'DDK', 'Basic Land — Plains').
card_original_text('plains'/'DDK', 'W').
card_image_name('plains'/'DDK', 'plains2').
card_uid('plains'/'DDK', 'DDK:Plains:plains2').
card_rarity('plains'/'DDK', 'Basic Land').
card_artist('plains'/'DDK', 'Jung Park').
card_number('plains'/'DDK', '39').
card_multiverse_id('plains'/'DDK', '368480').

card_in_set('plains', 'DDK').
card_original_type('plains'/'DDK', 'Basic Land — Plains').
card_original_text('plains'/'DDK', 'W').
card_image_name('plains'/'DDK', 'plains3').
card_uid('plains'/'DDK', 'DDK:Plains:plains3').
card_rarity('plains'/'DDK', 'Basic Land').
card_artist('plains'/'DDK', 'Eytan Zana').
card_number('plains'/'DDK', '40').
card_multiverse_id('plains'/'DDK', '368508').

card_in_set('pyroclasm', 'DDK').
card_original_type('pyroclasm'/'DDK', 'Sorcery').
card_original_text('pyroclasm'/'DDK', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'DDK', 'pyroclasm').
card_uid('pyroclasm'/'DDK', 'DDK:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'DDK', 'Uncommon').
card_artist('pyroclasm'/'DDK', 'John Avon').
card_number('pyroclasm'/'DDK', '62').
card_flavor_text('pyroclasm'/'DDK', 'Chaos does not choose its enemies.').
card_multiverse_id('pyroclasm'/'DDK', '368545').

card_in_set('rakdos carnarium', 'DDK').
card_original_type('rakdos carnarium'/'DDK', 'Land').
card_original_text('rakdos carnarium'/'DDK', 'Rakdos Carnarium enters the battlefield tapped.\nWhen Rakdos Carnarium enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{R} to your mana pool.').
card_image_name('rakdos carnarium'/'DDK', 'rakdos carnarium').
card_uid('rakdos carnarium'/'DDK', 'DDK:Rakdos Carnarium:rakdos carnarium').
card_rarity('rakdos carnarium'/'DDK', 'Common').
card_artist('rakdos carnarium'/'DDK', 'John Avon').
card_number('rakdos carnarium'/'DDK', '74').
card_multiverse_id('rakdos carnarium'/'DDK', '368493').

card_in_set('reassembling skeleton', 'DDK').
card_original_type('reassembling skeleton'/'DDK', 'Creature — Skeleton Warrior').
card_original_text('reassembling skeleton'/'DDK', '{1}{B}: Return Reassembling Skeleton from your graveyard to the battlefield tapped.').
card_image_name('reassembling skeleton'/'DDK', 'reassembling skeleton').
card_uid('reassembling skeleton'/'DDK', 'DDK:Reassembling Skeleton:reassembling skeleton').
card_rarity('reassembling skeleton'/'DDK', 'Uncommon').
card_artist('reassembling skeleton'/'DDK', 'Austin Hsu').
card_number('reassembling skeleton'/'DDK', '44').
card_flavor_text('reassembling skeleton'/'DDK', '\"They may show up with the wrong thigh bone or mandible, but they always show up.\"\n—Zul Ashur, lich lord').
card_multiverse_id('reassembling skeleton'/'DDK', '368551').

card_in_set('recoup', 'DDK').
card_original_type('recoup'/'DDK', 'Sorcery').
card_original_text('recoup'/'DDK', 'Target sorcery card in your graveyard gains flashback until end of turn. The flashback cost is equal to its mana cost.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('recoup'/'DDK', 'recoup').
card_uid('recoup'/'DDK', 'DDK:Recoup:recoup').
card_rarity('recoup'/'DDK', 'Uncommon').
card_artist('recoup'/'DDK', 'Dave Dorman').
card_number('recoup'/'DDK', '63').
card_multiverse_id('recoup'/'DDK', '368517').

card_in_set('revenant patriarch', 'DDK').
card_original_type('revenant patriarch'/'DDK', 'Creature — Spirit').
card_original_text('revenant patriarch'/'DDK', 'When Revenant Patriarch enters the battlefield, if {W} was spent to cast it, target player skips his or her next combat phase.\nRevenant Patriarch can\'t block.').
card_image_name('revenant patriarch'/'DDK', 'revenant patriarch').
card_uid('revenant patriarch'/'DDK', 'DDK:Revenant Patriarch:revenant patriarch').
card_rarity('revenant patriarch'/'DDK', 'Uncommon').
card_artist('revenant patriarch'/'DDK', 'Nick Percival').
card_number('revenant patriarch'/'DDK', '16').
card_multiverse_id('revenant patriarch'/'DDK', '368532').

card_in_set('scorched rusalka', 'DDK').
card_original_type('scorched rusalka'/'DDK', 'Creature — Spirit').
card_original_text('scorched rusalka'/'DDK', '{R}, Sacrifice a creature: Scorched Rusalka deals 1 damage to target player.').
card_image_name('scorched rusalka'/'DDK', 'scorched rusalka').
card_uid('scorched rusalka'/'DDK', 'DDK:Scorched Rusalka:scorched rusalka').
card_rarity('scorched rusalka'/'DDK', 'Uncommon').
card_artist('scorched rusalka'/'DDK', 'Luca Zontini').
card_number('scorched rusalka'/'DDK', '43').
card_flavor_text('scorched rusalka'/'DDK', 'Each small blaze she sets jogs her memory, letting her piece together the mystery of her own fiery end.').
card_multiverse_id('scorched rusalka'/'DDK', '368504').

card_in_set('scourge devil', 'DDK').
card_original_type('scourge devil'/'DDK', 'Creature — Devil').
card_original_text('scourge devil'/'DDK', 'When Scourge Devil enters the battlefield, creatures you control get +1/+0 until end of turn.\nUnearth {2}{R} ({2}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('scourge devil'/'DDK', 'scourge devil').
card_uid('scourge devil'/'DDK', 'DDK:Scourge Devil:scourge devil').
card_rarity('scourge devil'/'DDK', 'Uncommon').
card_artist('scourge devil'/'DDK', 'Dave Kendall').
card_number('scourge devil'/'DDK', '55').
card_multiverse_id('scourge devil'/'DDK', '368513').

card_in_set('sengir vampire', 'DDK').
card_original_type('sengir vampire'/'DDK', 'Creature — Vampire').
card_original_text('sengir vampire'/'DDK', 'Flying\nWhenever a creature dealt damage by Sengir Vampire this turn dies, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'DDK', 'sengir vampire').
card_uid('sengir vampire'/'DDK', 'DDK:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'DDK', 'Uncommon').
card_artist('sengir vampire'/'DDK', 'Kev Walker').
card_number('sengir vampire'/'DDK', '17').
card_flavor_text('sengir vampire'/'DDK', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'DDK', '368525').

card_in_set('shambling remains', 'DDK').
card_original_type('shambling remains'/'DDK', 'Creature — Zombie Horror').
card_original_text('shambling remains'/'DDK', 'Shambling Remains can\'t block.\nUnearth {B}{R} ({B}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('shambling remains'/'DDK', 'shambling remains').
card_uid('shambling remains'/'DDK', 'DDK:Shambling Remains:shambling remains').
card_rarity('shambling remains'/'DDK', 'Uncommon').
card_artist('shambling remains'/'DDK', 'Nils Hamm').
card_number('shambling remains'/'DDK', '48').
card_multiverse_id('shambling remains'/'DDK', '368489').

card_in_set('skirsdag cultist', 'DDK').
card_original_type('skirsdag cultist'/'DDK', 'Creature — Human Shaman').
card_original_text('skirsdag cultist'/'DDK', '{R}, {T}, Sacrifice a creature: Skirsdag Cultist deals 2 damage to target creature or player.').
card_image_name('skirsdag cultist'/'DDK', 'skirsdag cultist').
card_uid('skirsdag cultist'/'DDK', 'DDK:Skirsdag Cultist:skirsdag cultist').
card_rarity('skirsdag cultist'/'DDK', 'Uncommon').
card_artist('skirsdag cultist'/'DDK', 'Slawomir Maniak').
card_number('skirsdag cultist'/'DDK', '53').
card_flavor_text('skirsdag cultist'/'DDK', '\"Within blood is life. Within life is fire. Within fire is the path to our masters\' glory!\"').
card_multiverse_id('skirsdag cultist'/'DDK', '368488').

card_in_set('sorin\'s thirst', 'DDK').
card_original_type('sorin\'s thirst'/'DDK', 'Instant').
card_original_text('sorin\'s thirst'/'DDK', 'Sorin\'s Thirst deals 2 damage to target creature and you gain 2 life.').
card_image_name('sorin\'s thirst'/'DDK', 'sorin\'s thirst').
card_uid('sorin\'s thirst'/'DDK', 'DDK:Sorin\'s Thirst:sorin\'s thirst').
card_rarity('sorin\'s thirst'/'DDK', 'Common').
card_artist('sorin\'s thirst'/'DDK', 'Karl Kopinski').
card_number('sorin\'s thirst'/'DDK', '21').
card_flavor_text('sorin\'s thirst'/'DDK', '\"All your steel won\'t protect you if your will is weak.\"').
card_multiverse_id('sorin\'s thirst'/'DDK', '368509').

card_in_set('sorin, lord of innistrad', 'DDK').
card_original_type('sorin, lord of innistrad'/'DDK', 'Planeswalker — Sorin').
card_original_text('sorin, lord of innistrad'/'DDK', '+1: Put a 1/1 black Vampire creature token with lifelink onto the battlefield.\n-2: You get an emblem with \"Creatures you control get +1/+0.\"\n-6: Destroy up to three target creatures and/or other planeswalkers. Return each card put into a graveyard this way to the battlefield under your control.').
card_image_name('sorin, lord of innistrad'/'DDK', 'sorin, lord of innistrad').
card_uid('sorin, lord of innistrad'/'DDK', 'DDK:Sorin, Lord of Innistrad:sorin, lord of innistrad').
card_rarity('sorin, lord of innistrad'/'DDK', 'Mythic Rare').
card_artist('sorin, lord of innistrad'/'DDK', 'Chase Stone').
card_number('sorin, lord of innistrad'/'DDK', '1').
card_multiverse_id('sorin, lord of innistrad'/'DDK', '368535').

card_in_set('spectral procession', 'DDK').
card_original_type('spectral procession'/'DDK', 'Sorcery').
card_original_text('spectral procession'/'DDK', 'Put three 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('spectral procession'/'DDK', 'spectral procession').
card_uid('spectral procession'/'DDK', 'DDK:Spectral Procession:spectral procession').
card_rarity('spectral procession'/'DDK', 'Uncommon').
card_artist('spectral procession'/'DDK', 'Tomasz Jedruszek').
card_number('spectral procession'/'DDK', '26').
card_flavor_text('spectral procession'/'DDK', '\"I need never remember my past associates, my victims, my mistakes. Innistrad preserves them all for me.\"\n—Sorin Markov').
card_multiverse_id('spectral procession'/'DDK', '368515').

card_in_set('strangling soot', 'DDK').
card_original_type('strangling soot'/'DDK', 'Instant').
card_original_text('strangling soot'/'DDK', 'Destroy target creature with toughness 3 or less.\nFlashback {5}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('strangling soot'/'DDK', 'strangling soot').
card_uid('strangling soot'/'DDK', 'DDK:Strangling Soot:strangling soot').
card_rarity('strangling soot'/'DDK', 'Common').
card_artist('strangling soot'/'DDK', 'Jim Murray').
card_number('strangling soot'/'DDK', '65').
card_flavor_text('strangling soot'/'DDK', '\"What good is your blade without your breath?\"\n—Greht, Gathan warlord').
card_multiverse_id('strangling soot'/'DDK', '368521').

card_in_set('sulfuric vortex', 'DDK').
card_original_type('sulfuric vortex'/'DDK', 'Enchantment').
card_original_text('sulfuric vortex'/'DDK', 'At the beginning of each player\'s upkeep, Sulfuric Vortex deals 2 damage to that player.\nIf a player would gain life, that player gains no life instead.').
card_image_name('sulfuric vortex'/'DDK', 'sulfuric vortex').
card_uid('sulfuric vortex'/'DDK', 'DDK:Sulfuric Vortex:sulfuric vortex').
card_rarity('sulfuric vortex'/'DDK', 'Rare').
card_artist('sulfuric vortex'/'DDK', 'Greg Staples').
card_number('sulfuric vortex'/'DDK', '68').
card_multiverse_id('sulfuric vortex'/'DDK', '368550').

card_in_set('swamp', 'DDK').
card_original_type('swamp'/'DDK', 'Basic Land — Swamp').
card_original_text('swamp'/'DDK', 'B').
card_image_name('swamp'/'DDK', 'swamp1').
card_uid('swamp'/'DDK', 'DDK:Swamp:swamp1').
card_rarity('swamp'/'DDK', 'Basic Land').
card_artist('swamp'/'DDK', 'James Paick').
card_number('swamp'/'DDK', '35').
card_multiverse_id('swamp'/'DDK', '368492').

card_in_set('swamp', 'DDK').
card_original_type('swamp'/'DDK', 'Basic Land — Swamp').
card_original_text('swamp'/'DDK', 'B').
card_image_name('swamp'/'DDK', 'swamp2').
card_uid('swamp'/'DDK', 'DDK:Swamp:swamp2').
card_rarity('swamp'/'DDK', 'Basic Land').
card_artist('swamp'/'DDK', 'Adam Paquette').
card_number('swamp'/'DDK', '36').
card_multiverse_id('swamp'/'DDK', '368538').

card_in_set('swamp', 'DDK').
card_original_type('swamp'/'DDK', 'Basic Land — Swamp').
card_original_text('swamp'/'DDK', 'B').
card_image_name('swamp'/'DDK', 'swamp3').
card_uid('swamp'/'DDK', 'DDK:Swamp:swamp3').
card_rarity('swamp'/'DDK', 'Basic Land').
card_artist('swamp'/'DDK', 'Jung Park').
card_number('swamp'/'DDK', '37').
card_multiverse_id('swamp'/'DDK', '368497').

card_in_set('swamp', 'DDK').
card_original_type('swamp'/'DDK', 'Basic Land — Swamp').
card_original_text('swamp'/'DDK', 'B').
card_image_name('swamp'/'DDK', 'swamp4').
card_uid('swamp'/'DDK', 'DDK:Swamp:swamp4').
card_rarity('swamp'/'DDK', 'Basic Land').
card_artist('swamp'/'DDK', 'James Paick').
card_number('swamp'/'DDK', '78').
card_multiverse_id('swamp'/'DDK', '368494').

card_in_set('swamp', 'DDK').
card_original_type('swamp'/'DDK', 'Basic Land — Swamp').
card_original_text('swamp'/'DDK', 'B').
card_image_name('swamp'/'DDK', 'swamp5').
card_uid('swamp'/'DDK', 'DDK:Swamp:swamp5').
card_rarity('swamp'/'DDK', 'Basic Land').
card_artist('swamp'/'DDK', 'Adam Paquette').
card_number('swamp'/'DDK', '79').
card_multiverse_id('swamp'/'DDK', '368484').

card_in_set('swamp', 'DDK').
card_original_type('swamp'/'DDK', 'Basic Land — Swamp').
card_original_text('swamp'/'DDK', 'B').
card_image_name('swamp'/'DDK', 'swamp6').
card_uid('swamp'/'DDK', 'DDK:Swamp:swamp6').
card_rarity('swamp'/'DDK', 'Basic Land').
card_artist('swamp'/'DDK', 'Jung Park').
card_number('swamp'/'DDK', '80').
card_multiverse_id('swamp'/'DDK', '368482').

card_in_set('tainted field', 'DDK').
card_original_type('tainted field'/'DDK', 'Land').
card_original_text('tainted field'/'DDK', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Activate this ability only if you control a Swamp.').
card_image_name('tainted field'/'DDK', 'tainted field').
card_uid('tainted field'/'DDK', 'DDK:Tainted Field:tainted field').
card_rarity('tainted field'/'DDK', 'Uncommon').
card_artist('tainted field'/'DDK', 'Don Hazeltine').
card_number('tainted field'/'DDK', '34').
card_multiverse_id('tainted field'/'DDK', '368529').

card_in_set('terminate', 'DDK').
card_original_type('terminate'/'DDK', 'Instant').
card_original_text('terminate'/'DDK', 'Destroy target creature. It can\'t be regenerated.').
card_image_name('terminate'/'DDK', 'terminate').
card_uid('terminate'/'DDK', 'DDK:Terminate:terminate').
card_rarity('terminate'/'DDK', 'Common').
card_artist('terminate'/'DDK', 'Wayne Reynolds').
card_number('terminate'/'DDK', '64').
card_flavor_text('terminate'/'DDK', '\"I\'ve seen death before. My mother succumbing to illness, my comrades bleeding on the battlefield . . . But I\'d never seen anything as dreadful as that.\"\n—Taani, berserker of Etlan').
card_multiverse_id('terminate'/'DDK', '368491').

card_in_set('tibalt, the fiend-blooded', 'DDK').
card_original_type('tibalt, the fiend-blooded'/'DDK', 'Planeswalker — Tibalt').
card_original_text('tibalt, the fiend-blooded'/'DDK', '+1: Draw a card, then discard a card at random.\n-4: Tibalt, the Fiend-Blooded deals damage equal to the number of cards in target player\'s hand to that player.\n-6: Gain control of all creatures until end of turn. Untap them. They gain haste until end of turn.').
card_image_name('tibalt, the fiend-blooded'/'DDK', 'tibalt, the fiend-blooded').
card_uid('tibalt, the fiend-blooded'/'DDK', 'DDK:Tibalt, the Fiend-Blooded:tibalt, the fiend-blooded').
card_rarity('tibalt, the fiend-blooded'/'DDK', 'Mythic Rare').
card_artist('tibalt, the fiend-blooded'/'DDK', 'Chase Stone').
card_number('tibalt, the fiend-blooded'/'DDK', '41').
card_multiverse_id('tibalt, the fiend-blooded'/'DDK', '368531').

card_in_set('torrent of souls', 'DDK').
card_original_type('torrent of souls'/'DDK', 'Sorcery').
card_original_text('torrent of souls'/'DDK', 'Return up to one target creature card from your graveyard to the battlefield if {B} was spent to cast Torrent of Souls. Creatures target player controls get +2/+0 and gain haste until end of turn if {R} was spent to cast Torrent of Souls. (Do both if {B}{R} was spent.)').
card_image_name('torrent of souls'/'DDK', 'torrent of souls').
card_uid('torrent of souls'/'DDK', 'DDK:Torrent of Souls:torrent of souls').
card_rarity('torrent of souls'/'DDK', 'Uncommon').
card_artist('torrent of souls'/'DDK', 'Ian Edward Ameling').
card_number('torrent of souls'/'DDK', '71').
card_multiverse_id('torrent of souls'/'DDK', '368511').

card_in_set('twilight drover', 'DDK').
card_original_type('twilight drover'/'DDK', 'Creature — Spirit').
card_original_text('twilight drover'/'DDK', 'Whenever a creature token leaves the battlefield, put a +1/+1 counter on Twilight Drover.\n{2}{W}, Remove a +1/+1 counter from Twilight Drover: Put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('twilight drover'/'DDK', 'twilight drover').
card_uid('twilight drover'/'DDK', 'DDK:Twilight Drover:twilight drover').
card_rarity('twilight drover'/'DDK', 'Rare').
card_artist('twilight drover'/'DDK', 'Dave Allsop').
card_number('twilight drover'/'DDK', '9').
card_multiverse_id('twilight drover'/'DDK', '368549').

card_in_set('unmake', 'DDK').
card_original_type('unmake'/'DDK', 'Instant').
card_original_text('unmake'/'DDK', 'Exile target creature.').
card_image_name('unmake'/'DDK', 'unmake').
card_uid('unmake'/'DDK', 'DDK:Unmake:unmake').
card_rarity('unmake'/'DDK', 'Common').
card_artist('unmake'/'DDK', 'Steven Belledin').
card_number('unmake'/'DDK', '27').
card_flavor_text('unmake'/'DDK', 'A gwyllion\'s favorite trap is the vanity mirror. A bewitched piece of glass traps the looker\'s soul and does away with the body.').
card_multiverse_id('unmake'/'DDK', '368514').

card_in_set('urge to feed', 'DDK').
card_original_type('urge to feed'/'DDK', 'Instant').
card_original_text('urge to feed'/'DDK', 'Target creature gets -3/-3 until end of turn. You may tap any number of untapped Vampire creatures you control. If you do, put a +1/+1 counter on each of those Vampires.').
card_image_name('urge to feed'/'DDK', 'urge to feed').
card_uid('urge to feed'/'DDK', 'DDK:Urge to Feed:urge to feed').
card_rarity('urge to feed'/'DDK', 'Uncommon').
card_artist('urge to feed'/'DDK', 'Johann Bodin').
card_number('urge to feed'/'DDK', '22').
card_multiverse_id('urge to feed'/'DDK', '368522').

card_in_set('vampire lacerator', 'DDK').
card_original_type('vampire lacerator'/'DDK', 'Creature — Vampire Warrior').
card_original_text('vampire lacerator'/'DDK', 'At the beginning of your upkeep, you lose 1 life unless an opponent has 10 or less life.').
card_image_name('vampire lacerator'/'DDK', 'vampire lacerator').
card_uid('vampire lacerator'/'DDK', 'DDK:Vampire Lacerator:vampire lacerator').
card_rarity('vampire lacerator'/'DDK', 'Common').
card_artist('vampire lacerator'/'DDK', 'Steve Argyle').
card_number('vampire lacerator'/'DDK', '3').
card_flavor_text('vampire lacerator'/'DDK', 'The lacerators cut themselves before each hunt. They must feed before the sun rises or bleed to death.').
card_multiverse_id('vampire lacerator'/'DDK', '368481').

card_in_set('vampire nighthawk', 'DDK').
card_original_type('vampire nighthawk'/'DDK', 'Creature — Vampire Shaman').
card_original_text('vampire nighthawk'/'DDK', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vampire nighthawk'/'DDK', 'vampire nighthawk').
card_uid('vampire nighthawk'/'DDK', 'DDK:Vampire Nighthawk:vampire nighthawk').
card_rarity('vampire nighthawk'/'DDK', 'Uncommon').
card_artist('vampire nighthawk'/'DDK', 'Jason Chan').
card_number('vampire nighthawk'/'DDK', '12').
card_multiverse_id('vampire nighthawk'/'DDK', '368534').

card_in_set('vampire outcasts', 'DDK').
card_original_type('vampire outcasts'/'DDK', 'Creature — Vampire').
card_original_text('vampire outcasts'/'DDK', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vampire outcasts'/'DDK', 'vampire outcasts').
card_uid('vampire outcasts'/'DDK', 'DDK:Vampire Outcasts:vampire outcasts').
card_rarity('vampire outcasts'/'DDK', 'Uncommon').
card_artist('vampire outcasts'/'DDK', 'Clint Cearley').
card_number('vampire outcasts'/'DDK', '15').
card_multiverse_id('vampire outcasts'/'DDK', '368537').

card_in_set('vampire\'s bite', 'DDK').
card_original_type('vampire\'s bite'/'DDK', 'Instant').
card_original_text('vampire\'s bite'/'DDK', 'Kicker {2}{B} (You may pay an additional {2}{B} as you cast this spell.)\nTarget creature gets +3/+0 until end of turn. If Vampire\'s Bite was kicked, that creature gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_image_name('vampire\'s bite'/'DDK', 'vampire\'s bite').
card_uid('vampire\'s bite'/'DDK', 'DDK:Vampire\'s Bite:vampire\'s bite').
card_rarity('vampire\'s bite'/'DDK', 'Common').
card_artist('vampire\'s bite'/'DDK', 'Christopher Moeller').
card_number('vampire\'s bite'/'DDK', '19').
card_multiverse_id('vampire\'s bite'/'DDK', '368472').

card_in_set('vithian stinger', 'DDK').
card_original_type('vithian stinger'/'DDK', 'Creature — Human Shaman').
card_original_text('vithian stinger'/'DDK', '{T}: Vithian Stinger deals 1 damage to target creature or player.\nUnearth {1}{R} ({1}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('vithian stinger'/'DDK', 'vithian stinger').
card_uid('vithian stinger'/'DDK', 'DDK:Vithian Stinger:vithian stinger').
card_rarity('vithian stinger'/'DDK', 'Common').
card_artist('vithian stinger'/'DDK', 'Dave Kendall').
card_number('vithian stinger'/'DDK', '47').
card_multiverse_id('vithian stinger'/'DDK', '368548').

card_in_set('wall of omens', 'DDK').
card_original_type('wall of omens'/'DDK', 'Creature — Wall').
card_original_text('wall of omens'/'DDK', 'Defender\nWhen Wall of Omens enters the battlefield, draw a card.').
card_image_name('wall of omens'/'DDK', 'wall of omens').
card_uid('wall of omens'/'DDK', 'DDK:Wall of Omens:wall of omens').
card_rarity('wall of omens'/'DDK', 'Uncommon').
card_artist('wall of omens'/'DDK', 'James Paick').
card_number('wall of omens'/'DDK', '4').
card_flavor_text('wall of omens'/'DDK', '\"I search for a vision of Zendikar that does not include the Eldrazi.\"\n—Expedition journal entry').
card_multiverse_id('wall of omens'/'DDK', '368516').

card_in_set('zealous persecution', 'DDK').
card_original_type('zealous persecution'/'DDK', 'Instant').
card_original_text('zealous persecution'/'DDK', 'Until end of turn, creatures you control get +1/+1 and creatures your opponents control get -1/-1.').
card_image_name('zealous persecution'/'DDK', 'zealous persecution').
card_uid('zealous persecution'/'DDK', 'DDK:Zealous Persecution:zealous persecution').
card_rarity('zealous persecution'/'DDK', 'Uncommon').
card_artist('zealous persecution'/'DDK', 'Christopher Moeller').
card_number('zealous persecution'/'DDK', '23').
card_flavor_text('zealous persecution'/'DDK', '\"Jharic returned from Grixis changed, a haunted look in her eyes. She destroyed them with an unholy glee that made me shudder.\"\n—Knight-Captain Wyhorn').
card_multiverse_id('zealous persecution'/'DDK', '368542').
