% Wizards of the Coast Online Store

set('pWOS').
set_name('pWOS', 'Wizards of the Coast Online Store').
set_release_date('pWOS', '1999-09-04').
set_border('pWOS', 'black').
set_type('pWOS', 'promo').

card_in_set('serra angel', 'pWOS').
card_original_type('serra angel'/'pWOS', 'Creature — Angel').
card_original_text('serra angel'/'pWOS', '').
card_image_name('serra angel'/'pWOS', 'serra angel').
card_uid('serra angel'/'pWOS', 'pWOS:Serra Angel:serra angel').
card_rarity('serra angel'/'pWOS', 'Special').
card_artist('serra angel'/'pWOS', 'Douglas Shuler').
card_number('serra angel'/'pWOS', '1').
card_flavor_text('serra angel'/'pWOS', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
