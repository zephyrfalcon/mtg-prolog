% Battle for Zendikar

set('BFZ').
set_name('BFZ', 'Battle for Zendikar').
set_release_date('BFZ', '2015-10-02').
set_border('BFZ', 'black').
set_type('BFZ', 'expansion').
set_block('BFZ', 'Battle for Zendikar').

card_in_set('adverse conditions', 'BFZ').
card_original_type('adverse conditions'/'BFZ', 'Instant').
card_original_text('adverse conditions'/'BFZ', 'Devoid (This card has no color.)Tap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step. Put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('adverse conditions', 'BFZ').
card_image_name('adverse conditions'/'BFZ', 'adverse conditions').
card_uid('adverse conditions'/'BFZ', 'BFZ:Adverse Conditions:adverse conditions').
card_rarity('adverse conditions'/'BFZ', 'Uncommon').
card_artist('adverse conditions'/'BFZ', 'Jason Rainville').
card_number('adverse conditions'/'BFZ', '54').
card_multiverse_id('adverse conditions'/'BFZ', '401803').

card_in_set('akoum firebird', 'BFZ').
card_original_type('akoum firebird'/'BFZ', 'Creature — Phoenix').
card_original_text('akoum firebird'/'BFZ', 'Flying, hasteAkoum Firebird attacks each turn if able.Landfall — Whenever a land enters the battlefield under your control, you may pay {4}{R}{R}. If you do, return Akoum Firebird from your graveyard to the battlefield.').
card_first_print('akoum firebird', 'BFZ').
card_image_name('akoum firebird'/'BFZ', 'akoum firebird').
card_uid('akoum firebird'/'BFZ', 'BFZ:Akoum Firebird:akoum firebird').
card_rarity('akoum firebird'/'BFZ', 'Mythic Rare').
card_artist('akoum firebird'/'BFZ', 'Chris Rahn').
card_number('akoum firebird'/'BFZ', '138').
card_multiverse_id('akoum firebird'/'BFZ', '401804').

card_in_set('akoum hellkite', 'BFZ').
card_original_type('akoum hellkite'/'BFZ', 'Creature — Dragon').
card_original_text('akoum hellkite'/'BFZ', 'FlyingLandfall — Whenever a land enters the battlefield under your control, Akoum Hellkite deals 1 damage to target creature or player. If that land is a Mountain, Akoum Hellkite deals 2 damage to that creature or player instead.').
card_first_print('akoum hellkite', 'BFZ').
card_image_name('akoum hellkite'/'BFZ', 'akoum hellkite').
card_uid('akoum hellkite'/'BFZ', 'BFZ:Akoum Hellkite:akoum hellkite').
card_rarity('akoum hellkite'/'BFZ', 'Rare').
card_artist('akoum hellkite'/'BFZ', 'Jaime Jones').
card_number('akoum hellkite'/'BFZ', '139').
card_multiverse_id('akoum hellkite'/'BFZ', '401805').

card_in_set('akoum stonewaker', 'BFZ').
card_original_type('akoum stonewaker'/'BFZ', 'Creature — Human Shaman').
card_original_text('akoum stonewaker'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {2}{R}. If you do, put a 3/1 red Elemental creature token with trample and haste onto the battlefield. Exile that token at the beginning of the next end step.').
card_first_print('akoum stonewaker', 'BFZ').
card_image_name('akoum stonewaker'/'BFZ', 'akoum stonewaker').
card_uid('akoum stonewaker'/'BFZ', 'BFZ:Akoum Stonewaker:akoum stonewaker').
card_rarity('akoum stonewaker'/'BFZ', 'Uncommon').
card_artist('akoum stonewaker'/'BFZ', 'Victor Adame Minguez').
card_number('akoum stonewaker'/'BFZ', '140').
card_multiverse_id('akoum stonewaker'/'BFZ', '401806').

card_in_set('aligned hedron network', 'BFZ').
card_original_type('aligned hedron network'/'BFZ', 'Artifact').
card_original_text('aligned hedron network'/'BFZ', 'When Aligned Hedron Network enters the battlefield, exile all creatures with power 5 or greater until Aligned Hedron Network leaves the battlefield. (Those creatures return under their owners\' control.)').
card_first_print('aligned hedron network', 'BFZ').
card_image_name('aligned hedron network'/'BFZ', 'aligned hedron network').
card_uid('aligned hedron network'/'BFZ', 'BFZ:Aligned Hedron Network:aligned hedron network').
card_rarity('aligned hedron network'/'BFZ', 'Rare').
card_artist('aligned hedron network'/'BFZ', 'Richard Wright').
card_number('aligned hedron network'/'BFZ', '222').
card_flavor_text('aligned hedron network'/'BFZ', 'The last hedron slotted into place, locking Ulamog in an infinite loop of binding energy.').
card_multiverse_id('aligned hedron network'/'BFZ', '401807').

card_in_set('ally encampment', 'BFZ').
card_original_type('ally encampment'/'BFZ', 'Land').
card_original_text('ally encampment'/'BFZ', '{T}: Add {1} to your mana pool.{T}: Add one mana of any color to your mana pool. Spend this mana only to cast an Ally spell.{1}, {T}, Sacrifice Ally Encampment: Return target Ally you control to its owner\'s hand.').
card_first_print('ally encampment', 'BFZ').
card_image_name('ally encampment'/'BFZ', 'ally encampment').
card_uid('ally encampment'/'BFZ', 'BFZ:Ally Encampment:ally encampment').
card_rarity('ally encampment'/'BFZ', 'Rare').
card_artist('ally encampment'/'BFZ', 'Jonas De Ro').
card_number('ally encampment'/'BFZ', '228').
card_multiverse_id('ally encampment'/'BFZ', '401808').

card_in_set('altar\'s reap', 'BFZ').
card_original_type('altar\'s reap'/'BFZ', 'Instant').
card_original_text('altar\'s reap'/'BFZ', 'As an additional cost to cast Altar\'s Reap, sacrifice a creature.Draw two cards.').
card_image_name('altar\'s reap'/'BFZ', 'altar\'s reap').
card_uid('altar\'s reap'/'BFZ', 'BFZ:Altar\'s Reap:altar\'s reap').
card_rarity('altar\'s reap'/'BFZ', 'Common').
card_artist('altar\'s reap'/'BFZ', 'Tyler Jacobson').
card_number('altar\'s reap'/'BFZ', '103').
card_flavor_text('altar\'s reap'/'BFZ', '\"I will secure my revenge on Zendikar one soul at a time.\"—Ob Nixilis').
card_multiverse_id('altar\'s reap'/'BFZ', '401809').

card_in_set('angel of renewal', 'BFZ').
card_original_type('angel of renewal'/'BFZ', 'Creature — Angel Ally').
card_original_text('angel of renewal'/'BFZ', 'FlyingWhen Angel of Renewal enters the battlefield, you gain 1 life for each creature you control.').
card_first_print('angel of renewal', 'BFZ').
card_image_name('angel of renewal'/'BFZ', 'angel of renewal').
card_uid('angel of renewal'/'BFZ', 'BFZ:Angel of Renewal:angel of renewal').
card_rarity('angel of renewal'/'BFZ', 'Uncommon').
card_artist('angel of renewal'/'BFZ', 'Todd Lockwood').
card_number('angel of renewal'/'BFZ', '18').
card_flavor_text('angel of renewal'/'BFZ', '\"No more fear. No more failure. No more death. No more!\"').
card_multiverse_id('angel of renewal'/'BFZ', '401810').

card_in_set('angelic captain', 'BFZ').
card_original_type('angelic captain'/'BFZ', 'Creature — Angel Ally').
card_original_text('angelic captain'/'BFZ', 'FlyingWhenever Angelic Captain attacks, it gets +1/+1 until end of turn for each other attacking Ally.').
card_first_print('angelic captain', 'BFZ').
card_image_name('angelic captain'/'BFZ', 'angelic captain').
card_uid('angelic captain'/'BFZ', 'BFZ:Angelic Captain:angelic captain').
card_rarity('angelic captain'/'BFZ', 'Rare').
card_artist('angelic captain'/'BFZ', 'Volkan Baga').
card_number('angelic captain'/'BFZ', '208').
card_flavor_text('angelic captain'/'BFZ', 'As the soldiers beneath her raised their voices, the angel spread her wings, their span seeming to cover the sky.').
card_multiverse_id('angelic captain'/'BFZ', '401811').

card_in_set('angelic gift', 'BFZ').
card_original_type('angelic gift'/'BFZ', 'Enchantment — Aura').
card_original_text('angelic gift'/'BFZ', 'Enchant creatureWhen Angelic Gift enters the battlefield, draw a card.Enchanted creature has flying.').
card_first_print('angelic gift', 'BFZ').
card_image_name('angelic gift'/'BFZ', 'angelic gift').
card_uid('angelic gift'/'BFZ', 'BFZ:Angelic Gift:angelic gift').
card_rarity('angelic gift'/'BFZ', 'Common').
card_artist('angelic gift'/'BFZ', 'Josu Hernaiz').
card_number('angelic gift'/'BFZ', '19').
card_flavor_text('angelic gift'/'BFZ', '\"Zendikar will triumph this day, and our armies will fly with angels.\"').
card_multiverse_id('angelic gift'/'BFZ', '401812').

card_in_set('anticipate', 'BFZ').
card_original_type('anticipate'/'BFZ', 'Instant').
card_original_text('anticipate'/'BFZ', 'Look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_image_name('anticipate'/'BFZ', 'anticipate').
card_uid('anticipate'/'BFZ', 'BFZ:Anticipate:anticipate').
card_rarity('anticipate'/'BFZ', 'Common').
card_artist('anticipate'/'BFZ', 'Tyler Jacobson').
card_number('anticipate'/'BFZ', '69').
card_flavor_text('anticipate'/'BFZ', 'Divining the future is easy when you hold the power to sway it.').
card_multiverse_id('anticipate'/'BFZ', '401813').

card_in_set('bane of bala ged', 'BFZ').
card_original_type('bane of bala ged'/'BFZ', 'Creature — Eldrazi').
card_original_text('bane of bala ged'/'BFZ', 'Whenever Bane of Bala Ged attacks, defending player exiles two permanents he or she controls.').
card_first_print('bane of bala ged', 'BFZ').
card_image_name('bane of bala ged'/'BFZ', 'bane of bala ged').
card_uid('bane of bala ged'/'BFZ', 'BFZ:Bane of Bala Ged:bane of bala ged').
card_rarity('bane of bala ged'/'BFZ', 'Uncommon').
card_artist('bane of bala ged'/'BFZ', 'Chase Stone').
card_number('bane of bala ged'/'BFZ', '1').
card_flavor_text('bane of bala ged'/'BFZ', 'The continent of Bala Ged was ravaged by massive Eldrazi of Ulamog\'s lineage, its lush jungles and cascading rivers reduced to dust.').
card_multiverse_id('bane of bala ged'/'BFZ', '401814').

card_in_set('barrage tyrant', 'BFZ').
card_original_type('barrage tyrant'/'BFZ', 'Creature — Eldrazi').
card_original_text('barrage tyrant'/'BFZ', 'Devoid (This card has no color.){2}{R}, Sacrifice another colorless creature: Barrage Tyrant deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_first_print('barrage tyrant', 'BFZ').
card_image_name('barrage tyrant'/'BFZ', 'barrage tyrant').
card_uid('barrage tyrant'/'BFZ', 'BFZ:Barrage Tyrant:barrage tyrant').
card_rarity('barrage tyrant'/'BFZ', 'Rare').
card_artist('barrage tyrant'/'BFZ', 'Chris Rallis').
card_number('barrage tyrant'/'BFZ', '127').
card_flavor_text('barrage tyrant'/'BFZ', 'Terrible fury. Excellent aim.').
card_multiverse_id('barrage tyrant'/'BFZ', '401815').

card_in_set('beastcaller savant', 'BFZ').
card_original_type('beastcaller savant'/'BFZ', 'Creature — Elf Shaman Ally').
card_original_text('beastcaller savant'/'BFZ', 'Haste{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a creature spell.').
card_first_print('beastcaller savant', 'BFZ').
card_image_name('beastcaller savant'/'BFZ', 'beastcaller savant').
card_uid('beastcaller savant'/'BFZ', 'BFZ:Beastcaller Savant:beastcaller savant').
card_rarity('beastcaller savant'/'BFZ', 'Rare').
card_artist('beastcaller savant'/'BFZ', 'Anthony Palumbo').
card_number('beastcaller savant'/'BFZ', '170').
card_flavor_text('beastcaller savant'/'BFZ', '\"They come because I call. They stay because I listen.\"').
card_multiverse_id('beastcaller savant'/'BFZ', '401816').

card_in_set('belligerent whiptail', 'BFZ').
card_original_type('belligerent whiptail'/'BFZ', 'Creature — Wurm').
card_original_text('belligerent whiptail'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, Belligerent Whiptail gains first strike until end of turn.').
card_first_print('belligerent whiptail', 'BFZ').
card_image_name('belligerent whiptail'/'BFZ', 'belligerent whiptail').
card_uid('belligerent whiptail'/'BFZ', 'BFZ:Belligerent Whiptail:belligerent whiptail').
card_rarity('belligerent whiptail'/'BFZ', 'Common').
card_artist('belligerent whiptail'/'BFZ', 'Jakub Kasper').
card_number('belligerent whiptail'/'BFZ', '141').
card_flavor_text('belligerent whiptail'/'BFZ', '\"Whiptails can sense a traveler\'s footsteps from a great distance. Measuring that distance has been . . . difficult.\"—Jalun, Affa sentry').
card_multiverse_id('belligerent whiptail'/'BFZ', '401817').

card_in_set('benthic infiltrator', 'BFZ').
card_original_type('benthic infiltrator'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('benthic infiltrator'/'BFZ', 'Devoid (This card has no color.)Ingest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)Benthic Infiltrator can\'t be blocked.').
card_first_print('benthic infiltrator', 'BFZ').
card_image_name('benthic infiltrator'/'BFZ', 'benthic infiltrator').
card_uid('benthic infiltrator'/'BFZ', 'BFZ:Benthic Infiltrator:benthic infiltrator').
card_rarity('benthic infiltrator'/'BFZ', 'Common').
card_artist('benthic infiltrator'/'BFZ', 'Mathias Kollros').
card_number('benthic infiltrator'/'BFZ', '55').
card_multiverse_id('benthic infiltrator'/'BFZ', '401818').

card_in_set('blight herder', 'BFZ').
card_original_type('blight herder'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('blight herder'/'BFZ', 'When you cast Blight Herder, you may put two cards your opponents own from exile into their owners\' graveyards. If you do, put three 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('blight herder', 'BFZ').
card_image_name('blight herder'/'BFZ', 'blight herder').
card_uid('blight herder'/'BFZ', 'BFZ:Blight Herder:blight herder').
card_rarity('blight herder'/'BFZ', 'Rare').
card_artist('blight herder'/'BFZ', 'Todd Lockwood').
card_number('blight herder'/'BFZ', '2').
card_multiverse_id('blight herder'/'BFZ', '401819').

card_in_set('blighted cataract', 'BFZ').
card_original_type('blighted cataract'/'BFZ', 'Land').
card_original_text('blighted cataract'/'BFZ', '{T}: Add {1} to your mana pool.{5}{U}, {T}, Sacrifice Blighted Cataract: Draw two cards.').
card_first_print('blighted cataract', 'BFZ').
card_image_name('blighted cataract'/'BFZ', 'blighted cataract').
card_uid('blighted cataract'/'BFZ', 'BFZ:Blighted Cataract:blighted cataract').
card_rarity('blighted cataract'/'BFZ', 'Uncommon').
card_artist('blighted cataract'/'BFZ', 'Vincent Proce').
card_number('blighted cataract'/'BFZ', '229').
card_flavor_text('blighted cataract'/'BFZ', 'Once, water ran here. Now only dust and ash fall from the clifftops.').
card_multiverse_id('blighted cataract'/'BFZ', '401820').

card_in_set('blighted fen', 'BFZ').
card_original_type('blighted fen'/'BFZ', 'Land').
card_original_text('blighted fen'/'BFZ', '{T}: Add {1} to your mana pool.{4}{B}, {T}, Sacrifice Blighted Fen: Target opponent sacrifices a creature.').
card_first_print('blighted fen', 'BFZ').
card_image_name('blighted fen'/'BFZ', 'blighted fen').
card_uid('blighted fen'/'BFZ', 'BFZ:Blighted Fen:blighted fen').
card_rarity('blighted fen'/'BFZ', 'Uncommon').
card_artist('blighted fen'/'BFZ', 'Jonas De Ro').
card_number('blighted fen'/'BFZ', '230').
card_flavor_text('blighted fen'/'BFZ', '\"We came to a place where the skin of Zendikar was peeled back and its bones lay bare to the sky.\"—Greenweaver Mina').
card_multiverse_id('blighted fen'/'BFZ', '401821').

card_in_set('blighted gorge', 'BFZ').
card_original_type('blighted gorge'/'BFZ', 'Land').
card_original_text('blighted gorge'/'BFZ', '{T}: Add {1} to your mana pool.{4}{R}, {T}, Sacrifice Blighted Gorge: Blighted Gorge deals 2 damage to target creature or player.').
card_first_print('blighted gorge', 'BFZ').
card_image_name('blighted gorge'/'BFZ', 'blighted gorge').
card_uid('blighted gorge'/'BFZ', 'BFZ:Blighted Gorge:blighted gorge').
card_rarity('blighted gorge'/'BFZ', 'Uncommon').
card_artist('blighted gorge'/'BFZ', 'Jung Park').
card_number('blighted gorge'/'BFZ', '231').
card_flavor_text('blighted gorge'/'BFZ', 'The land is dying, but it will not go peacefully.').
card_multiverse_id('blighted gorge'/'BFZ', '401822').

card_in_set('blighted steppe', 'BFZ').
card_original_type('blighted steppe'/'BFZ', 'Land').
card_original_text('blighted steppe'/'BFZ', '{T}: Add {1} to your mana pool.{3}{W}, {T}, Sacrifice Blighted Steppe: You gain 2 life for each creature you control.').
card_first_print('blighted steppe', 'BFZ').
card_image_name('blighted steppe'/'BFZ', 'blighted steppe').
card_uid('blighted steppe'/'BFZ', 'BFZ:Blighted Steppe:blighted steppe').
card_rarity('blighted steppe'/'BFZ', 'Uncommon').
card_artist('blighted steppe'/'BFZ', 'Yeong-Hao Han').
card_number('blighted steppe'/'BFZ', '232').
card_flavor_text('blighted steppe'/'BFZ', '\"When a limb is gangrenous, you cut it off. This is no different.\"—Greenweaver Mina').
card_multiverse_id('blighted steppe'/'BFZ', '401823').

card_in_set('blighted woodland', 'BFZ').
card_original_type('blighted woodland'/'BFZ', 'Land').
card_original_text('blighted woodland'/'BFZ', '{T}: Add {1} to your mana pool.{3}{G}, {T}, Sacrifice Blighted Woodland: Search your library for up to two basic land cards and put them onto the battlefield tapped. Then shuffle your library.').
card_first_print('blighted woodland', 'BFZ').
card_image_name('blighted woodland'/'BFZ', 'blighted woodland').
card_uid('blighted woodland'/'BFZ', 'BFZ:Blighted Woodland:blighted woodland').
card_rarity('blighted woodland'/'BFZ', 'Uncommon').
card_artist('blighted woodland'/'BFZ', 'Jason Felix').
card_number('blighted woodland'/'BFZ', '233').
card_multiverse_id('blighted woodland'/'BFZ', '401824').

card_in_set('blisterpod', 'BFZ').
card_original_type('blisterpod'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('blisterpod'/'BFZ', 'Devoid (This card has no color.)When Blisterpod dies, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('blisterpod', 'BFZ').
card_image_name('blisterpod'/'BFZ', 'blisterpod').
card_uid('blisterpod'/'BFZ', 'BFZ:Blisterpod:blisterpod').
card_rarity('blisterpod'/'BFZ', 'Common').
card_artist('blisterpod'/'BFZ', 'Ryan Barger').
card_number('blisterpod'/'BFZ', '163').
card_flavor_text('blisterpod'/'BFZ', 'A blisterpod\'s corpse is a scion\'s cradle.').
card_multiverse_id('blisterpod'/'BFZ', '401825').

card_in_set('bloodbond vampire', 'BFZ').
card_original_type('bloodbond vampire'/'BFZ', 'Creature — Vampire Shaman Ally').
card_original_text('bloodbond vampire'/'BFZ', 'Whenever you gain life, put a +1/+1 counter on Bloodbond Vampire.').
card_first_print('bloodbond vampire', 'BFZ').
card_image_name('bloodbond vampire'/'BFZ', 'bloodbond vampire').
card_uid('bloodbond vampire'/'BFZ', 'BFZ:Bloodbond Vampire:bloodbond vampire').
card_rarity('bloodbond vampire'/'BFZ', 'Uncommon').
card_artist('bloodbond vampire'/'BFZ', 'Anna Steinbauer').
card_number('bloodbond vampire'/'BFZ', '104').
card_flavor_text('bloodbond vampire'/'BFZ', '\"Let the vampires join us. In this war, we no longer have the luxury of choosing our comrades.\"—General Tazri, allied commander').
card_multiverse_id('bloodbond vampire'/'BFZ', '401826').

card_in_set('boiling earth', 'BFZ').
card_original_type('boiling earth'/'BFZ', 'Sorcery').
card_original_text('boiling earth'/'BFZ', 'Boiling Earth deals 1 damage to each creature your opponents control.Awaken 4—{6}{R} (If you cast this spell for {6}{R}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('boiling earth', 'BFZ').
card_image_name('boiling earth'/'BFZ', 'boiling earth').
card_uid('boiling earth'/'BFZ', 'BFZ:Boiling Earth:boiling earth').
card_rarity('boiling earth'/'BFZ', 'Common').
card_artist('boiling earth'/'BFZ', 'Titus Lunter').
card_number('boiling earth'/'BFZ', '142').
card_multiverse_id('boiling earth'/'BFZ', '401827').

card_in_set('bone splinters', 'BFZ').
card_original_type('bone splinters'/'BFZ', 'Sorcery').
card_original_text('bone splinters'/'BFZ', 'As an additional cost to cast Bone Splinters, sacrifice a creature.Destroy target creature.').
card_image_name('bone splinters'/'BFZ', 'bone splinters').
card_uid('bone splinters'/'BFZ', 'BFZ:Bone Splinters:bone splinters').
card_rarity('bone splinters'/'BFZ', 'Common').
card_artist('bone splinters'/'BFZ', 'Deruchenko Alexander').
card_number('bone splinters'/'BFZ', '105').
card_flavor_text('bone splinters'/'BFZ', 'Vastly outnumbered, the Zendikari found creative ways to even the odds.').
card_multiverse_id('bone splinters'/'BFZ', '401828').

card_in_set('breaker of armies', 'BFZ').
card_original_type('breaker of armies'/'BFZ', 'Creature — Eldrazi').
card_original_text('breaker of armies'/'BFZ', 'All creatures able to block Breaker of Armies do so.').
card_first_print('breaker of armies', 'BFZ').
card_image_name('breaker of armies'/'BFZ', 'breaker of armies').
card_uid('breaker of armies'/'BFZ', 'BFZ:Breaker of Armies:breaker of armies').
card_rarity('breaker of armies'/'BFZ', 'Uncommon').
card_artist('breaker of armies'/'BFZ', 'Richard Wright').
card_number('breaker of armies'/'BFZ', '3').
card_flavor_text('breaker of armies'/'BFZ', '\"There are some who have lost everyone and everything. They actively seek out the Eldrazi, eager for the smallest measure of vengeance.\"—Munda, ambush leader').
card_multiverse_id('breaker of armies'/'BFZ', '401829').

card_in_set('brilliant spectrum', 'BFZ').
card_original_type('brilliant spectrum'/'BFZ', 'Sorcery').
card_original_text('brilliant spectrum'/'BFZ', 'Converge — Draw X cards, where X is the number of colors of mana spent to cast Brilliant Spectrum. Then discard two cards.').
card_first_print('brilliant spectrum', 'BFZ').
card_image_name('brilliant spectrum'/'BFZ', 'brilliant spectrum').
card_uid('brilliant spectrum'/'BFZ', 'BFZ:Brilliant Spectrum:brilliant spectrum').
card_rarity('brilliant spectrum'/'BFZ', 'Common').
card_artist('brilliant spectrum'/'BFZ', 'Winona Nelson').
card_number('brilliant spectrum'/'BFZ', '70').
card_flavor_text('brilliant spectrum'/'BFZ', 'There are more sights on Zendikar than the eyes can see.').
card_multiverse_id('brilliant spectrum'/'BFZ', '401830').

card_in_set('bring to light', 'BFZ').
card_original_type('bring to light'/'BFZ', 'Sorcery').
card_original_text('bring to light'/'BFZ', 'Converge — Search your library for a creature, instant, or sorcery card with converted mana cost less than or equal to the number of colors of mana spent to cast Bring to Light, exile that card, then shuffle your library. You may cast that card without paying its mana cost.').
card_first_print('bring to light', 'BFZ').
card_image_name('bring to light'/'BFZ', 'bring to light').
card_uid('bring to light'/'BFZ', 'BFZ:Bring to Light:bring to light').
card_rarity('bring to light'/'BFZ', 'Rare').
card_artist('bring to light'/'BFZ', 'Jonas De Ro').
card_number('bring to light'/'BFZ', '209').
card_multiverse_id('bring to light'/'BFZ', '401831').

card_in_set('brood butcher', 'BFZ').
card_original_type('brood butcher'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('brood butcher'/'BFZ', 'Devoid (This card has no color.)When Brood Butcher enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"{B}{G}, Sacrifice a creature: Target creature gets -2/-2 until end of turn.').
card_first_print('brood butcher', 'BFZ').
card_image_name('brood butcher'/'BFZ', 'brood butcher').
card_uid('brood butcher'/'BFZ', 'BFZ:Brood Butcher:brood butcher').
card_rarity('brood butcher'/'BFZ', 'Rare').
card_artist('brood butcher'/'BFZ', 'Daarken').
card_number('brood butcher'/'BFZ', '199').
card_multiverse_id('brood butcher'/'BFZ', '401832').

card_in_set('brood monitor', 'BFZ').
card_original_type('brood monitor'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('brood monitor'/'BFZ', 'Devoid (This card has no color.)When Brood Monitor enters the battlefield, put three 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('brood monitor', 'BFZ').
card_image_name('brood monitor'/'BFZ', 'brood monitor').
card_uid('brood monitor'/'BFZ', 'BFZ:Brood Monitor:brood monitor').
card_rarity('brood monitor'/'BFZ', 'Uncommon').
card_artist('brood monitor'/'BFZ', 'Izzy').
card_number('brood monitor'/'BFZ', '164').
card_flavor_text('brood monitor'/'BFZ', 'The tenderness of a mother. The pity of a mantis.').
card_multiverse_id('brood monitor'/'BFZ', '401833').

card_in_set('broodhunter wurm', 'BFZ').
card_original_type('broodhunter wurm'/'BFZ', 'Creature — Wurm').
card_original_text('broodhunter wurm'/'BFZ', '').
card_first_print('broodhunter wurm', 'BFZ').
card_image_name('broodhunter wurm'/'BFZ', 'broodhunter wurm').
card_uid('broodhunter wurm'/'BFZ', 'BFZ:Broodhunter Wurm:broodhunter wurm').
card_rarity('broodhunter wurm'/'BFZ', 'Common').
card_artist('broodhunter wurm'/'BFZ', 'Svetlin Velinov').
card_number('broodhunter wurm'/'BFZ', '171').
card_flavor_text('broodhunter wurm'/'BFZ', 'The native creatures of Zendikar have adapted to live under rocks and crawl through underbrush in order to avoid the most voracious predators. The Eldrazi have yet to make such adjustments.').
card_multiverse_id('broodhunter wurm'/'BFZ', '401834').

card_in_set('brutal expulsion', 'BFZ').
card_original_type('brutal expulsion'/'BFZ', 'Instant').
card_original_text('brutal expulsion'/'BFZ', 'Devoid (This card has no color.)Choose one or both —• Return target spell or creature to its owner\'s hand.• Brutal Expulsion deals 2 damage to target creature or planeswalker. If that permanent would be put into a graveyard this turn, exile it instead.').
card_first_print('brutal expulsion', 'BFZ').
card_image_name('brutal expulsion'/'BFZ', 'brutal expulsion').
card_uid('brutal expulsion'/'BFZ', 'BFZ:Brutal Expulsion:brutal expulsion').
card_rarity('brutal expulsion'/'BFZ', 'Rare').
card_artist('brutal expulsion'/'BFZ', 'Victor Adame Minguez').
card_number('brutal expulsion'/'BFZ', '200').
card_multiverse_id('brutal expulsion'/'BFZ', '401835').

card_in_set('call the scions', 'BFZ').
card_original_type('call the scions'/'BFZ', 'Sorcery').
card_original_text('call the scions'/'BFZ', 'Devoid (This card has no color.)Put two 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('call the scions', 'BFZ').
card_image_name('call the scions'/'BFZ', 'call the scions').
card_uid('call the scions'/'BFZ', 'BFZ:Call the Scions:call the scions').
card_rarity('call the scions'/'BFZ', 'Common').
card_artist('call the scions'/'BFZ', 'Winona Nelson').
card_number('call the scions'/'BFZ', '165').
card_flavor_text('call the scions'/'BFZ', 'Ulamog\'s progeny swarm the land, draining Zendikar\'s mana to feed the titan\'s insatiable hunger.').
card_multiverse_id('call the scions'/'BFZ', '401836').

card_in_set('canopy vista', 'BFZ').
card_original_type('canopy vista'/'BFZ', 'Land — Forest Plains').
card_original_text('canopy vista'/'BFZ', '({T}: Add {G} or {W} to your mana pool.)Canopy Vista enters the battlefield tapped unless you control two or more basic lands.').
card_first_print('canopy vista', 'BFZ').
card_image_name('canopy vista'/'BFZ', 'canopy vista').
card_uid('canopy vista'/'BFZ', 'BFZ:Canopy Vista:canopy vista').
card_rarity('canopy vista'/'BFZ', 'Rare').
card_artist('canopy vista'/'BFZ', 'Adam Paquette').
card_number('canopy vista'/'BFZ', '234').
card_flavor_text('canopy vista'/'BFZ', 'The continent of Murasa lies beneath a blanket of dense vegetation, its enormous branches tangled so thickly that some inhabitants never see the ground.').
card_multiverse_id('canopy vista'/'BFZ', '401837').

card_in_set('carrier thrall', 'BFZ').
card_original_type('carrier thrall'/'BFZ', 'Creature — Vampire').
card_original_text('carrier thrall'/'BFZ', 'When Carrier Thrall dies, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('carrier thrall', 'BFZ').
card_image_name('carrier thrall'/'BFZ', 'carrier thrall').
card_uid('carrier thrall'/'BFZ', 'BFZ:Carrier Thrall:carrier thrall').
card_rarity('carrier thrall'/'BFZ', 'Uncommon').
card_artist('carrier thrall'/'BFZ', 'Lius Lasahido').
card_number('carrier thrall'/'BFZ', '106').
card_flavor_text('carrier thrall'/'BFZ', '\"Later we will mourn those we have lost. First we must end their suffering.\"—Drana, Kalastria bloodchief').
card_multiverse_id('carrier thrall'/'BFZ', '401838').

card_in_set('catacomb sifter', 'BFZ').
card_original_type('catacomb sifter'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('catacomb sifter'/'BFZ', 'Devoid (This card has no color.)When Catacomb Sifter enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"Whenever another creature you control dies, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('catacomb sifter', 'BFZ').
card_image_name('catacomb sifter'/'BFZ', 'catacomb sifter').
card_uid('catacomb sifter'/'BFZ', 'BFZ:Catacomb Sifter:catacomb sifter').
card_rarity('catacomb sifter'/'BFZ', 'Uncommon').
card_artist('catacomb sifter'/'BFZ', 'Craig J Spearing').
card_number('catacomb sifter'/'BFZ', '201').
card_multiverse_id('catacomb sifter'/'BFZ', '401839').

card_in_set('chasm guide', 'BFZ').
card_original_type('chasm guide'/'BFZ', 'Creature — Goblin Scout Ally').
card_original_text('chasm guide'/'BFZ', 'Rally — Whenever Chasm Guide or another Ally enters the battlefield under your control, creatures you control gain haste until end of turn.').
card_first_print('chasm guide', 'BFZ').
card_image_name('chasm guide'/'BFZ', 'chasm guide').
card_uid('chasm guide'/'BFZ', 'BFZ:Chasm Guide:chasm guide').
card_rarity('chasm guide'/'BFZ', 'Uncommon').
card_artist('chasm guide'/'BFZ', 'Johannes Voss').
card_number('chasm guide'/'BFZ', '143').
card_flavor_text('chasm guide'/'BFZ', 'With a single act of bravery, she went from expendable to indispensable.').
card_multiverse_id('chasm guide'/'BFZ', '401840').

card_in_set('cinder glade', 'BFZ').
card_original_type('cinder glade'/'BFZ', 'Land — Mountain Forest').
card_original_text('cinder glade'/'BFZ', '({T}: Add {R} or {G} to your mana pool.)Cinder Glade enters the battlefield tapped unless you control two or more basic lands.').
card_first_print('cinder glade', 'BFZ').
card_image_name('cinder glade'/'BFZ', 'cinder glade').
card_uid('cinder glade'/'BFZ', 'BFZ:Cinder Glade:cinder glade').
card_rarity('cinder glade'/'BFZ', 'Rare').
card_artist('cinder glade'/'BFZ', 'Adam Paquette').
card_number('cinder glade'/'BFZ', '235').
card_flavor_text('cinder glade'/'BFZ', 'On the volcanic continent of Akoum, bizarre vegetation clusters around gas vents, and jagged mountain peaks rise high into the air.').
card_multiverse_id('cinder glade'/'BFZ', '401841').

card_in_set('cliffside lookout', 'BFZ').
card_original_type('cliffside lookout'/'BFZ', 'Creature — Kor Scout Ally').
card_original_text('cliffside lookout'/'BFZ', '{4}{W}: Creatures you control get +1/+1 until end of turn.').
card_first_print('cliffside lookout', 'BFZ').
card_image_name('cliffside lookout'/'BFZ', 'cliffside lookout').
card_uid('cliffside lookout'/'BFZ', 'BFZ:Cliffside Lookout:cliffside lookout').
card_rarity('cliffside lookout'/'BFZ', 'Common').
card_artist('cliffside lookout'/'BFZ', 'Eric Deschamps').
card_number('cliffside lookout'/'BFZ', '20').
card_flavor_text('cliffside lookout'/'BFZ', 'Though losses run high among the scouts of the Stone Havens, they never flinch from their duty.').
card_multiverse_id('cliffside lookout'/'BFZ', '401842').

card_in_set('cloud manta', 'BFZ').
card_original_type('cloud manta'/'BFZ', 'Creature — Fish').
card_original_text('cloud manta'/'BFZ', 'Flying').
card_first_print('cloud manta', 'BFZ').
card_image_name('cloud manta'/'BFZ', 'cloud manta').
card_uid('cloud manta'/'BFZ', 'BFZ:Cloud Manta:cloud manta').
card_rarity('cloud manta'/'BFZ', 'Common').
card_artist('cloud manta'/'BFZ', 'Mike Bierek').
card_number('cloud manta'/'BFZ', '71').
card_flavor_text('cloud manta'/'BFZ', 'When Emeria\'s worshippers learned that she was no more than a twisted memory of Emrakul, they abandoned their temples to the god. The deserted shrines now serve as breeding grounds for the mantas.').
card_multiverse_id('cloud manta'/'BFZ', '401843').

card_in_set('clutch of currents', 'BFZ').
card_original_type('clutch of currents'/'BFZ', 'Sorcery').
card_original_text('clutch of currents'/'BFZ', 'Return target creature to its owner\'s hand.Awaken 3—{4}{U} (If you cast this spell for {4}{U}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('clutch of currents', 'BFZ').
card_image_name('clutch of currents'/'BFZ', 'clutch of currents').
card_uid('clutch of currents'/'BFZ', 'BFZ:Clutch of Currents:clutch of currents').
card_rarity('clutch of currents'/'BFZ', 'Common').
card_artist('clutch of currents'/'BFZ', 'Igor Kieryluk').
card_number('clutch of currents'/'BFZ', '72').
card_multiverse_id('clutch of currents'/'BFZ', '401844').

card_in_set('coastal discovery', 'BFZ').
card_original_type('coastal discovery'/'BFZ', 'Sorcery').
card_original_text('coastal discovery'/'BFZ', 'Draw two cards.Awaken 4—{5}{U} (If you cast this spell for {5}{U}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('coastal discovery', 'BFZ').
card_image_name('coastal discovery'/'BFZ', 'coastal discovery').
card_uid('coastal discovery'/'BFZ', 'BFZ:Coastal Discovery:coastal discovery').
card_rarity('coastal discovery'/'BFZ', 'Uncommon').
card_artist('coastal discovery'/'BFZ', 'Kieran Yanner').
card_number('coastal discovery'/'BFZ', '73').
card_multiverse_id('coastal discovery'/'BFZ', '401845').

card_in_set('complete disregard', 'BFZ').
card_original_type('complete disregard'/'BFZ', 'Instant').
card_original_text('complete disregard'/'BFZ', 'Devoid (This card has no color.)Exile target creature with power 3 or less.').
card_first_print('complete disregard', 'BFZ').
card_image_name('complete disregard'/'BFZ', 'complete disregard').
card_uid('complete disregard'/'BFZ', 'BFZ:Complete Disregard:complete disregard').
card_rarity('complete disregard'/'BFZ', 'Common').
card_artist('complete disregard'/'BFZ', 'Peter Mohrbacher').
card_number('complete disregard'/'BFZ', '90').
card_flavor_text('complete disregard'/'BFZ', '\"We returned to the field and found poor Len, every detail of his final moment perfectly cast in that awful dust.\"—Javad Nasrin, outrider captain').
card_multiverse_id('complete disregard'/'BFZ', '401846').

card_in_set('conduit of ruin', 'BFZ').
card_original_type('conduit of ruin'/'BFZ', 'Creature — Eldrazi').
card_original_text('conduit of ruin'/'BFZ', 'When you cast Conduit of Ruin, you may search your library for a colorless creature card with converted mana cost 7 or greater, reveal it, then shuffle your library and put that card on top of it.The first creature spell you cast each turn costs {2} less to cast.').
card_first_print('conduit of ruin', 'BFZ').
card_image_name('conduit of ruin'/'BFZ', 'conduit of ruin').
card_uid('conduit of ruin'/'BFZ', 'BFZ:Conduit of Ruin:conduit of ruin').
card_rarity('conduit of ruin'/'BFZ', 'Rare').
card_artist('conduit of ruin'/'BFZ', 'Slawomir Maniak').
card_number('conduit of ruin'/'BFZ', '4').
card_multiverse_id('conduit of ruin'/'BFZ', '401847').

card_in_set('coralhelm guide', 'BFZ').
card_original_type('coralhelm guide'/'BFZ', 'Creature — Merfolk Scout Ally').
card_original_text('coralhelm guide'/'BFZ', '{4}{U}: Target creature can\'t be blocked this turn.').
card_first_print('coralhelm guide', 'BFZ').
card_image_name('coralhelm guide'/'BFZ', 'coralhelm guide').
card_uid('coralhelm guide'/'BFZ', 'BFZ:Coralhelm Guide:coralhelm guide').
card_rarity('coralhelm guide'/'BFZ', 'Common').
card_artist('coralhelm guide'/'BFZ', 'Viktor Titov').
card_number('coralhelm guide'/'BFZ', '74').
card_flavor_text('coralhelm guide'/'BFZ', '\"She knows every step of this coastline, both above and below the surface, and she has hideouts all along the way. She will get you there.\"—Jori En, expedition leader').
card_multiverse_id('coralhelm guide'/'BFZ', '401848').

card_in_set('courier griffin', 'BFZ').
card_original_type('courier griffin'/'BFZ', 'Creature — Griffin').
card_original_text('courier griffin'/'BFZ', 'FlyingWhen Courier Griffin enters the battlefield, you gain 2 life.').
card_first_print('courier griffin', 'BFZ').
card_image_name('courier griffin'/'BFZ', 'courier griffin').
card_uid('courier griffin'/'BFZ', 'BFZ:Courier Griffin:courier griffin').
card_rarity('courier griffin'/'BFZ', 'Common').
card_artist('courier griffin'/'BFZ', 'Kieran Yanner').
card_number('courier griffin'/'BFZ', '21').
card_flavor_text('courier griffin'/'BFZ', '\"Sea Gate has fallen. Survivors are on the move. Will send another griffin when we find refuge. Stay hidden. Stay safe.\"—Message from Tars Olan, kor world-gift').
card_multiverse_id('courier griffin'/'BFZ', '401849').

card_in_set('crumble to dust', 'BFZ').
card_original_type('crumble to dust'/'BFZ', 'Sorcery').
card_original_text('crumble to dust'/'BFZ', 'Devoid (This card has no color.)Exile target nonbasic land. Search its controller\'s graveyard, hand, and library for any number of cards with the same name as that land and exile them. Then that player shuffles his or her library.').
card_first_print('crumble to dust', 'BFZ').
card_image_name('crumble to dust'/'BFZ', 'crumble to dust').
card_uid('crumble to dust'/'BFZ', 'BFZ:Crumble to Dust:crumble to dust').
card_rarity('crumble to dust'/'BFZ', 'Uncommon').
card_artist('crumble to dust'/'BFZ', 'James Paick').
card_number('crumble to dust'/'BFZ', '128').
card_multiverse_id('crumble to dust'/'BFZ', '401850').

card_in_set('cryptic cruiser', 'BFZ').
card_original_type('cryptic cruiser'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('cryptic cruiser'/'BFZ', 'Devoid (This card has no color.){2}{U}, Put a card an opponent owns from exile into that player\'s graveyard: Tap target creature.').
card_first_print('cryptic cruiser', 'BFZ').
card_image_name('cryptic cruiser'/'BFZ', 'cryptic cruiser').
card_uid('cryptic cruiser'/'BFZ', 'BFZ:Cryptic Cruiser:cryptic cruiser').
card_rarity('cryptic cruiser'/'BFZ', 'Uncommon').
card_artist('cryptic cruiser'/'BFZ', 'Svetlin Velinov').
card_number('cryptic cruiser'/'BFZ', '56').
card_flavor_text('cryptic cruiser'/'BFZ', '\"The seas are no less imperiled than the dry lands. This fight is mine as much as it is yours.\"—Kiora, to Gideon Jura').
card_multiverse_id('cryptic cruiser'/'BFZ', '401851').

card_in_set('culling drone', 'BFZ').
card_original_type('culling drone'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('culling drone'/'BFZ', 'Devoid (This card has no color.)Ingest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)').
card_first_print('culling drone', 'BFZ').
card_image_name('culling drone'/'BFZ', 'culling drone').
card_uid('culling drone'/'BFZ', 'BFZ:Culling Drone:culling drone').
card_rarity('culling drone'/'BFZ', 'Common').
card_artist('culling drone'/'BFZ', 'Alejandro Mirabal').
card_number('culling drone'/'BFZ', '91').
card_flavor_text('culling drone'/'BFZ', 'Silence rules Bala Ged, broken only by the skittering of Eldrazi.').
card_multiverse_id('culling drone'/'BFZ', '401852').

card_in_set('dampening pulse', 'BFZ').
card_original_type('dampening pulse'/'BFZ', 'Enchantment').
card_original_text('dampening pulse'/'BFZ', 'Creatures your opponents control get -1/-0.').
card_first_print('dampening pulse', 'BFZ').
card_image_name('dampening pulse'/'BFZ', 'dampening pulse').
card_uid('dampening pulse'/'BFZ', 'BFZ:Dampening Pulse:dampening pulse').
card_rarity('dampening pulse'/'BFZ', 'Uncommon').
card_artist('dampening pulse'/'BFZ', 'Johann Bodin').
card_number('dampening pulse'/'BFZ', '75').
card_flavor_text('dampening pulse'/'BFZ', '\"Quiet now, you dreadful vermin.\"—Noyan Dar, Tazeem roilmage').
card_multiverse_id('dampening pulse'/'BFZ', '401853').

card_in_set('deathless behemoth', 'BFZ').
card_original_type('deathless behemoth'/'BFZ', 'Creature — Eldrazi').
card_original_text('deathless behemoth'/'BFZ', 'VigilanceSacrifice two Eldrazi Scions: Return Deathless Behemoth from your graveyard to your hand. Activate this ability only any time you could cast a sorcery.').
card_first_print('deathless behemoth', 'BFZ').
card_image_name('deathless behemoth'/'BFZ', 'deathless behemoth').
card_uid('deathless behemoth'/'BFZ', 'BFZ:Deathless Behemoth:deathless behemoth').
card_rarity('deathless behemoth'/'BFZ', 'Uncommon').
card_artist('deathless behemoth'/'BFZ', 'Jason Felix').
card_number('deathless behemoth'/'BFZ', '5').
card_flavor_text('deathless behemoth'/'BFZ', '\"Over and over it rose. Over and over we fell.\"—Ganda, Goma Fada deserter').
card_multiverse_id('deathless behemoth'/'BFZ', '401854').

card_in_set('defiant bloodlord', 'BFZ').
card_original_type('defiant bloodlord'/'BFZ', 'Creature — Vampire').
card_original_text('defiant bloodlord'/'BFZ', 'FlyingWhenever you gain life, target opponent loses that much life.').
card_first_print('defiant bloodlord', 'BFZ').
card_image_name('defiant bloodlord'/'BFZ', 'defiant bloodlord').
card_uid('defiant bloodlord'/'BFZ', 'BFZ:Defiant Bloodlord:defiant bloodlord').
card_rarity('defiant bloodlord'/'BFZ', 'Rare').
card_artist('defiant bloodlord'/'BFZ', 'Craig J Spearing').
card_number('defiant bloodlord'/'BFZ', '107').
card_flavor_text('defiant bloodlord'/'BFZ', '\"Despite their bravado, mortals are fragile. If Zendikar is to endure, its lords must save it.\"—Drana, Kalastria bloodchief').
card_multiverse_id('defiant bloodlord'/'BFZ', '401855').

card_in_set('demon\'s grasp', 'BFZ').
card_original_type('demon\'s grasp'/'BFZ', 'Sorcery').
card_original_text('demon\'s grasp'/'BFZ', 'Target creature gets -5/-5 until end of turn.').
card_first_print('demon\'s grasp', 'BFZ').
card_image_name('demon\'s grasp'/'BFZ', 'demon\'s grasp').
card_uid('demon\'s grasp'/'BFZ', 'BFZ:Demon\'s Grasp:demon\'s grasp').
card_rarity('demon\'s grasp'/'BFZ', 'Common').
card_artist('demon\'s grasp'/'BFZ', 'David Gaillet').
card_number('demon\'s grasp'/'BFZ', '108').
card_flavor_text('demon\'s grasp'/'BFZ', '\"Take what solace you can in the knowledge that you will not be here to witness Zendikar\'s demise.\"—Ob Nixilis').
card_multiverse_id('demon\'s grasp'/'BFZ', '401856').

card_in_set('desolation twin', 'BFZ').
card_original_type('desolation twin'/'BFZ', 'Creature — Eldrazi').
card_original_text('desolation twin'/'BFZ', 'When you cast Desolation Twin, put a 10/10 colorless Eldrazi creature token onto the battlefield.').
card_first_print('desolation twin', 'BFZ').
card_image_name('desolation twin'/'BFZ', 'desolation twin').
card_uid('desolation twin'/'BFZ', 'BFZ:Desolation Twin:desolation twin').
card_rarity('desolation twin'/'BFZ', 'Rare').
card_artist('desolation twin'/'BFZ', 'Jack Wang').
card_number('desolation twin'/'BFZ', '6').
card_flavor_text('desolation twin'/'BFZ', '\"With precise coordination and enough blood spilled, one can be driven off, even brought down. But two . . . that\'s a lot of blood.\"—Munda, ambush leader').
card_multiverse_id('desolation twin'/'BFZ', '401857').

card_in_set('dispel', 'BFZ').
card_original_type('dispel'/'BFZ', 'Instant').
card_original_text('dispel'/'BFZ', 'Counter target instant spell.').
card_image_name('dispel'/'BFZ', 'dispel').
card_uid('dispel'/'BFZ', 'BFZ:Dispel:dispel').
card_rarity('dispel'/'BFZ', 'Common').
card_artist('dispel'/'BFZ', 'Chase Stone').
card_number('dispel'/'BFZ', '76').
card_flavor_text('dispel'/'BFZ', '\"I said we should destroy the Eldrazi. Ugin wanted to return them to stasis. Ugin\'s arguments were . . . unconvincing.\"—Jace Beleren').
card_multiverse_id('dispel'/'BFZ', '401858').

card_in_set('dominator drone', 'BFZ').
card_original_type('dominator drone'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('dominator drone'/'BFZ', 'Devoid (This card has no color.)Ingest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)When Dominator Drone enters the battlefield, if you control another colorless creature, each opponent loses 2 life.').
card_image_name('dominator drone'/'BFZ', 'dominator drone').
card_uid('dominator drone'/'BFZ', 'BFZ:Dominator Drone:dominator drone').
card_rarity('dominator drone'/'BFZ', 'Common').
card_artist('dominator drone'/'BFZ', 'James Zapata').
card_number('dominator drone'/'BFZ', '92').
card_multiverse_id('dominator drone'/'BFZ', '401859').

card_in_set('dragonmaster outcast', 'BFZ').
card_original_type('dragonmaster outcast'/'BFZ', 'Creature — Human Shaman').
card_original_text('dragonmaster outcast'/'BFZ', 'At the beginning of your upkeep, if you control six or more lands, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_image_name('dragonmaster outcast'/'BFZ', 'dragonmaster outcast').
card_uid('dragonmaster outcast'/'BFZ', 'BFZ:Dragonmaster Outcast:dragonmaster outcast').
card_rarity('dragonmaster outcast'/'BFZ', 'Mythic Rare').
card_artist('dragonmaster outcast'/'BFZ', 'Raymond Swanland').
card_number('dragonmaster outcast'/'BFZ', '144').
card_flavor_text('dragonmaster outcast'/'BFZ', '\"This world will be ruled by its rightful sovereigns, not these hideous pretenders.\"').
card_multiverse_id('dragonmaster outcast'/'BFZ', '401860').

card_in_set('drana\'s emissary', 'BFZ').
card_original_type('drana\'s emissary'/'BFZ', 'Creature — Vampire Cleric Ally').
card_original_text('drana\'s emissary'/'BFZ', 'FlyingAt the beginning of your upkeep, each opponent loses 1 life and you gain 1 life.').
card_first_print('drana\'s emissary', 'BFZ').
card_image_name('drana\'s emissary'/'BFZ', 'drana\'s emissary').
card_uid('drana\'s emissary'/'BFZ', 'BFZ:Drana\'s Emissary:drana\'s emissary').
card_rarity('drana\'s emissary'/'BFZ', 'Uncommon').
card_artist('drana\'s emissary'/'BFZ', 'Karl Kopinski').
card_number('drana\'s emissary'/'BFZ', '210').
card_flavor_text('drana\'s emissary'/'BFZ', '\"The taste of freedom is sweeter than blood.\"').
card_multiverse_id('drana\'s emissary'/'BFZ', '401862').

card_in_set('drana, liberator of malakir', 'BFZ').
card_original_type('drana, liberator of malakir'/'BFZ', 'Legendary Creature — Vampire Ally').
card_original_text('drana, liberator of malakir'/'BFZ', 'Flying, first strikeWhenever Drana, Liberator of Malakir deals combat damage to a player, put a +1/+1 counter on each attacking creature you control.').
card_first_print('drana, liberator of malakir', 'BFZ').
card_image_name('drana, liberator of malakir'/'BFZ', 'drana, liberator of malakir').
card_uid('drana, liberator of malakir'/'BFZ', 'BFZ:Drana, Liberator of Malakir:drana, liberator of malakir').
card_rarity('drana, liberator of malakir'/'BFZ', 'Mythic Rare').
card_artist('drana, liberator of malakir'/'BFZ', 'Mike Bierek').
card_number('drana, liberator of malakir'/'BFZ', '109').
card_flavor_text('drana, liberator of malakir'/'BFZ', '\"I will not live as a slave. If you would be free, then fight alongside me.\"').
card_multiverse_id('drana, liberator of malakir'/'BFZ', '401861').

card_in_set('drowner of hope', 'BFZ').
card_original_type('drowner of hope'/'BFZ', 'Creature — Eldrazi').
card_original_text('drowner of hope'/'BFZ', 'Devoid (This card has no color.)When Drowner of Hope enters the battlefield, put two 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"Sacrifice an Eldrazi Scion: Tap target creature.').
card_first_print('drowner of hope', 'BFZ').
card_image_name('drowner of hope'/'BFZ', 'drowner of hope').
card_uid('drowner of hope'/'BFZ', 'BFZ:Drowner of Hope:drowner of hope').
card_rarity('drowner of hope'/'BFZ', 'Rare').
card_artist('drowner of hope'/'BFZ', 'Tomasz Jedruszek').
card_number('drowner of hope'/'BFZ', '57').
card_multiverse_id('drowner of hope'/'BFZ', '401863').

card_in_set('dust stalker', 'BFZ').
card_original_type('dust stalker'/'BFZ', 'Creature — Eldrazi').
card_original_text('dust stalker'/'BFZ', 'Devoid (This card has no color.)HasteAt the beginning of each end step, if you control no other colorless creatures, return Dust Stalker to its owner\'s hand.').
card_first_print('dust stalker', 'BFZ').
card_image_name('dust stalker'/'BFZ', 'dust stalker').
card_uid('dust stalker'/'BFZ', 'BFZ:Dust Stalker:dust stalker').
card_rarity('dust stalker'/'BFZ', 'Rare').
card_artist('dust stalker'/'BFZ', 'Clint Cearley').
card_number('dust stalker'/'BFZ', '202').
card_flavor_text('dust stalker'/'BFZ', 'The only traces it leaves are silent gusts and prodigious amounts of wreckage.').
card_multiverse_id('dust stalker'/'BFZ', '401864').

card_in_set('dutiful return', 'BFZ').
card_original_type('dutiful return'/'BFZ', 'Sorcery').
card_original_text('dutiful return'/'BFZ', 'Return up to two target creature cards from your graveyard to your hand.').
card_image_name('dutiful return'/'BFZ', 'dutiful return').
card_uid('dutiful return'/'BFZ', 'BFZ:Dutiful Return:dutiful return').
card_rarity('dutiful return'/'BFZ', 'Common').
card_artist('dutiful return'/'BFZ', 'Anna Steinbauer').
card_number('dutiful return'/'BFZ', '110').
card_flavor_text('dutiful return'/'BFZ', 'When Malakir\'s surgeons failed, its death-shamans took over.').
card_multiverse_id('dutiful return'/'BFZ', '401865').

card_in_set('earthen arms', 'BFZ').
card_original_type('earthen arms'/'BFZ', 'Sorcery').
card_original_text('earthen arms'/'BFZ', 'Put two +1/+1 counters on target permanent.Awaken 4—{6}{G} (If you cast this spell for {6}{G}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('earthen arms', 'BFZ').
card_image_name('earthen arms'/'BFZ', 'earthen arms').
card_uid('earthen arms'/'BFZ', 'BFZ:Earthen Arms:earthen arms').
card_rarity('earthen arms'/'BFZ', 'Common').
card_artist('earthen arms'/'BFZ', 'Dan Scott').
card_number('earthen arms'/'BFZ', '172').
card_multiverse_id('earthen arms'/'BFZ', '401866').

card_in_set('eldrazi devastator', 'BFZ').
card_original_type('eldrazi devastator'/'BFZ', 'Creature — Eldrazi').
card_original_text('eldrazi devastator'/'BFZ', 'Trample').
card_first_print('eldrazi devastator', 'BFZ').
card_image_name('eldrazi devastator'/'BFZ', 'eldrazi devastator').
card_uid('eldrazi devastator'/'BFZ', 'BFZ:Eldrazi Devastator:eldrazi devastator').
card_rarity('eldrazi devastator'/'BFZ', 'Common').
card_artist('eldrazi devastator'/'BFZ', 'Joseph Meehan').
card_number('eldrazi devastator'/'BFZ', '7').
card_flavor_text('eldrazi devastator'/'BFZ', '\"No matter how big your champion, theirs is bigger. No matter how great your numbers, theirs are greater. No matter how voracious your appetite, they are hungrier. That is why the Eldrazi will win.\"—Kalitas, thrall of Ulamog').
card_multiverse_id('eldrazi devastator'/'BFZ', '401867').

card_in_set('eldrazi skyspawner', 'BFZ').
card_original_type('eldrazi skyspawner'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('eldrazi skyspawner'/'BFZ', 'Devoid (This card has no color.)FlyingWhen Eldrazi Skyspawner enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('eldrazi skyspawner', 'BFZ').
card_image_name('eldrazi skyspawner'/'BFZ', 'eldrazi skyspawner').
card_uid('eldrazi skyspawner'/'BFZ', 'BFZ:Eldrazi Skyspawner:eldrazi skyspawner').
card_rarity('eldrazi skyspawner'/'BFZ', 'Common').
card_artist('eldrazi skyspawner'/'BFZ', 'Chase Stone').
card_number('eldrazi skyspawner'/'BFZ', '58').
card_multiverse_id('eldrazi skyspawner'/'BFZ', '401868').

card_in_set('emeria shepherd', 'BFZ').
card_original_type('emeria shepherd'/'BFZ', 'Creature — Angel').
card_original_text('emeria shepherd'/'BFZ', 'FlyingLandfall — Whenever a land enters the battlefield under your control, you may return target nonland permanent card from your graveyard to your hand. If that land is a Plains, you may return that nonland permanent card to the battlefield instead.').
card_first_print('emeria shepherd', 'BFZ').
card_image_name('emeria shepherd'/'BFZ', 'emeria shepherd').
card_uid('emeria shepherd'/'BFZ', 'BFZ:Emeria Shepherd:emeria shepherd').
card_rarity('emeria shepherd'/'BFZ', 'Rare').
card_artist('emeria shepherd'/'BFZ', 'Cynthia Sheppard').
card_number('emeria shepherd'/'BFZ', '22').
card_multiverse_id('emeria shepherd'/'BFZ', '401869').

card_in_set('encircling fissure', 'BFZ').
card_original_type('encircling fissure'/'BFZ', 'Instant').
card_original_text('encircling fissure'/'BFZ', 'Prevent all combat damage that would be dealt this turn by creatures target opponent controls.Awaken 2—{4}{W} (If you cast this spell for {4}{W}, also put two +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('encircling fissure', 'BFZ').
card_image_name('encircling fissure'/'BFZ', 'encircling fissure').
card_uid('encircling fissure'/'BFZ', 'BFZ:Encircling Fissure:encircling fissure').
card_rarity('encircling fissure'/'BFZ', 'Uncommon').
card_artist('encircling fissure'/'BFZ', 'Igor Kieryluk').
card_number('encircling fissure'/'BFZ', '23').
card_multiverse_id('encircling fissure'/'BFZ', '401870').

card_in_set('endless one', 'BFZ').
card_original_type('endless one'/'BFZ', 'Creature — Eldrazi').
card_original_text('endless one'/'BFZ', 'Endless One enters the battlefield with X +1/+1 counters on it.').
card_first_print('endless one', 'BFZ').
card_image_name('endless one'/'BFZ', 'endless one').
card_uid('endless one'/'BFZ', 'BFZ:Endless One:endless one').
card_rarity('endless one'/'BFZ', 'Rare').
card_artist('endless one'/'BFZ', 'Jason Felix').
card_number('endless one'/'BFZ', '8').
card_flavor_text('endless one'/'BFZ', 'It embodies all possible meanings of the word \"infinite.\"').
card_multiverse_id('endless one'/'BFZ', '401871').

card_in_set('evolving wilds', 'BFZ').
card_original_type('evolving wilds'/'BFZ', 'Land').
card_original_text('evolving wilds'/'BFZ', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'BFZ', 'evolving wilds').
card_uid('evolving wilds'/'BFZ', 'BFZ:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'BFZ', 'Common').
card_artist('evolving wilds'/'BFZ', 'Izzy').
card_number('evolving wilds'/'BFZ', '236').
card_flavor_text('evolving wilds'/'BFZ', 'The Eldrazi may be ruthless, but Zendikar is relentless.').
card_multiverse_id('evolving wilds'/'BFZ', '401872').

card_in_set('exert influence', 'BFZ').
card_original_type('exert influence'/'BFZ', 'Sorcery').
card_original_text('exert influence'/'BFZ', 'Converge — Gain control of target creature if its power is less than or equal to the number of colors of mana spent to cast Exert Influence.').
card_first_print('exert influence', 'BFZ').
card_image_name('exert influence'/'BFZ', 'exert influence').
card_uid('exert influence'/'BFZ', 'BFZ:Exert Influence:exert influence').
card_rarity('exert influence'/'BFZ', 'Rare').
card_artist('exert influence'/'BFZ', 'Magali Villeneuve').
card_number('exert influence'/'BFZ', '77').
card_flavor_text('exert influence'/'BFZ', '\"I can\'t control its mind—I\'m not even sure it has one. But I can dominate its will.\"').
card_multiverse_id('exert influence'/'BFZ', '401873').

card_in_set('expedition envoy', 'BFZ').
card_original_type('expedition envoy'/'BFZ', 'Creature — Human Scout Ally').
card_original_text('expedition envoy'/'BFZ', '').
card_first_print('expedition envoy', 'BFZ').
card_image_name('expedition envoy'/'BFZ', 'expedition envoy').
card_uid('expedition envoy'/'BFZ', 'BFZ:Expedition Envoy:expedition envoy').
card_rarity('expedition envoy'/'BFZ', 'Uncommon').
card_artist('expedition envoy'/'BFZ', 'David Palumbo').
card_number('expedition envoy'/'BFZ', '24').
card_flavor_text('expedition envoy'/'BFZ', 'The unofficial motto of every expedition house is \"ready for anything,\" a phrase whose significance has been amplified since the emergence of the Eldrazi.').
card_multiverse_id('expedition envoy'/'BFZ', '401874').

card_in_set('eyeless watcher', 'BFZ').
card_original_type('eyeless watcher'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('eyeless watcher'/'BFZ', 'Devoid (This card has no color.)When Eyeless Watcher enters the battlefield, put two 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('eyeless watcher', 'BFZ').
card_image_name('eyeless watcher'/'BFZ', 'eyeless watcher').
card_uid('eyeless watcher'/'BFZ', 'BFZ:Eyeless Watcher:eyeless watcher').
card_rarity('eyeless watcher'/'BFZ', 'Common').
card_artist('eyeless watcher'/'BFZ', 'Yohann Schepacz').
card_number('eyeless watcher'/'BFZ', '166').
card_flavor_text('eyeless watcher'/'BFZ', 'Every Eldrazi in Ulamog\'s lineage is an extension of the titan\'s will.').
card_multiverse_id('eyeless watcher'/'BFZ', '401875').

card_in_set('fathom feeder', 'BFZ').
card_original_type('fathom feeder'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('fathom feeder'/'BFZ', 'Devoid (This card has no color.)DeathtouchIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.){3}{U}{B}: Draw a card. Each opponent exiles the top card of his or her library.').
card_first_print('fathom feeder', 'BFZ').
card_image_name('fathom feeder'/'BFZ', 'fathom feeder').
card_uid('fathom feeder'/'BFZ', 'BFZ:Fathom Feeder:fathom feeder').
card_rarity('fathom feeder'/'BFZ', 'Rare').
card_artist('fathom feeder'/'BFZ', 'Clint Cearley').
card_number('fathom feeder'/'BFZ', '203').
card_multiverse_id('fathom feeder'/'BFZ', '401876').

card_in_set('felidar cub', 'BFZ').
card_original_type('felidar cub'/'BFZ', 'Creature — Cat Beast').
card_original_text('felidar cub'/'BFZ', 'Sacrifice Felidar Cub: Destroy target enchantment.').
card_first_print('felidar cub', 'BFZ').
card_image_name('felidar cub'/'BFZ', 'felidar cub').
card_uid('felidar cub'/'BFZ', 'BFZ:Felidar Cub:felidar cub').
card_rarity('felidar cub'/'BFZ', 'Common').
card_artist('felidar cub'/'BFZ', 'Steve Prescott').
card_number('felidar cub'/'BFZ', '25').
card_flavor_text('felidar cub'/'BFZ', 'Felidars that make it through their first year of life can easily live to see a hundred years more.').
card_multiverse_id('felidar cub'/'BFZ', '401877').

card_in_set('felidar sovereign', 'BFZ').
card_original_type('felidar sovereign'/'BFZ', 'Creature — Cat Beast').
card_original_text('felidar sovereign'/'BFZ', 'Vigilance, lifelinkAt the beginning of your upkeep, if you have 40 or more life, you win the game.').
card_image_name('felidar sovereign'/'BFZ', 'felidar sovereign').
card_uid('felidar sovereign'/'BFZ', 'BFZ:Felidar Sovereign:felidar sovereign').
card_rarity('felidar sovereign'/'BFZ', 'Rare').
card_artist('felidar sovereign'/'BFZ', 'Zoltan Boros & Gabor Szikszai').
card_number('felidar sovereign'/'BFZ', '26').
card_flavor_text('felidar sovereign'/'BFZ', 'It surveys the withering landscape, waiting for a victory only it can see.').
card_multiverse_id('felidar sovereign'/'BFZ', '401878').

card_in_set('fertile thicket', 'BFZ').
card_original_type('fertile thicket'/'BFZ', 'Land').
card_original_text('fertile thicket'/'BFZ', 'Fertile Thicket enters the battlefield tapped.When Fertile Thicket enters the battlefield, you may look at the top five cards of your library. If you do, reveal up to one basic land card from among them, then put that card on top of your library and the rest on the bottom in any order.{T}: Add {G} to your mana pool.').
card_first_print('fertile thicket', 'BFZ').
card_image_name('fertile thicket'/'BFZ', 'fertile thicket').
card_uid('fertile thicket'/'BFZ', 'BFZ:Fertile Thicket:fertile thicket').
card_rarity('fertile thicket'/'BFZ', 'Common').
card_artist('fertile thicket'/'BFZ', 'Andreas Rocha').
card_number('fertile thicket'/'BFZ', '237').
card_multiverse_id('fertile thicket'/'BFZ', '401879').

card_in_set('firemantle mage', 'BFZ').
card_original_type('firemantle mage'/'BFZ', 'Creature — Human Shaman Ally').
card_original_text('firemantle mage'/'BFZ', 'Rally — Whenever Firemantle Mage or another Ally enters the battlefield under your control, creatures you control gain menace until end of turn. (A creature with menace can\'t be blocked except by two or more creatures.)').
card_first_print('firemantle mage', 'BFZ').
card_image_name('firemantle mage'/'BFZ', 'firemantle mage').
card_uid('firemantle mage'/'BFZ', 'BFZ:Firemantle Mage:firemantle mage').
card_rarity('firemantle mage'/'BFZ', 'Uncommon').
card_artist('firemantle mage'/'BFZ', 'Chris Rahn').
card_number('firemantle mage'/'BFZ', '145').
card_flavor_text('firemantle mage'/'BFZ', '\"Come on, you twisted things! Don\'t you want to get acquainted?\"').
card_multiverse_id('firemantle mage'/'BFZ', '401880').

card_in_set('forerunner of slaughter', 'BFZ').
card_original_type('forerunner of slaughter'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('forerunner of slaughter'/'BFZ', 'Devoid (This card has no color.){1}: Target colorless creature gains haste until end of turn.').
card_image_name('forerunner of slaughter'/'BFZ', 'forerunner of slaughter').
card_uid('forerunner of slaughter'/'BFZ', 'BFZ:Forerunner of Slaughter:forerunner of slaughter').
card_rarity('forerunner of slaughter'/'BFZ', 'Uncommon').
card_artist('forerunner of slaughter'/'BFZ', 'James Zapata').
card_number('forerunner of slaughter'/'BFZ', '204').
card_flavor_text('forerunner of slaughter'/'BFZ', '\"While we delay, they\'re on the move. They will not wait as we bicker. We are losing this war by moments as our goal slips beyond our reach.\"—Gideon Jura').
card_multiverse_id('forerunner of slaughter'/'BFZ', '401881').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest1').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest1').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Noah Bradley').
card_number('forest'/'BFZ', '270').
card_multiverse_id('forest'/'BFZ', '401889').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest10').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest10').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Tianhua X').
card_number('forest'/'BFZ', '274').
card_multiverse_id('forest'/'BFZ', '401883').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest2').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest2').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Noah Bradley').
card_number('forest'/'BFZ', '270').
card_multiverse_id('forest'/'BFZ', '401887').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest3').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest3').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Sam Burley').
card_number('forest'/'BFZ', '271').
card_multiverse_id('forest'/'BFZ', '401888').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest4').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest4').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Sam Burley').
card_number('forest'/'BFZ', '271').
card_multiverse_id('forest'/'BFZ', '401885').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest5').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest5').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Adam Paquette').
card_number('forest'/'BFZ', '272').
card_multiverse_id('forest'/'BFZ', '401884').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest6').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest6').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Adam Paquette').
card_number('forest'/'BFZ', '272').
card_multiverse_id('forest'/'BFZ', '401891').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest7').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest7').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Vincent Proce').
card_number('forest'/'BFZ', '273').
card_multiverse_id('forest'/'BFZ', '401882').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest8').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest8').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Vincent Proce').
card_number('forest'/'BFZ', '273').
card_multiverse_id('forest'/'BFZ', '401886').

card_in_set('forest', 'BFZ').
card_original_type('forest'/'BFZ', 'Basic Land — Forest').
card_original_text('forest'/'BFZ', 'G').
card_image_name('forest'/'BFZ', 'forest9').
card_uid('forest'/'BFZ', 'BFZ:Forest:forest9').
card_rarity('forest'/'BFZ', 'Basic Land').
card_artist('forest'/'BFZ', 'Tianhua X').
card_number('forest'/'BFZ', '274').
card_multiverse_id('forest'/'BFZ', '401890').

card_in_set('fortified rampart', 'BFZ').
card_original_type('fortified rampart'/'BFZ', 'Creature — Wall').
card_original_text('fortified rampart'/'BFZ', 'Defender').
card_first_print('fortified rampart', 'BFZ').
card_image_name('fortified rampart'/'BFZ', 'fortified rampart').
card_uid('fortified rampart'/'BFZ', 'BFZ:Fortified Rampart:fortified rampart').
card_rarity('fortified rampart'/'BFZ', 'Common').
card_artist('fortified rampart'/'BFZ', 'David Gaillet').
card_number('fortified rampart'/'BFZ', '27').
card_flavor_text('fortified rampart'/'BFZ', 'The refuge\'s defenses allow new recruits to see lesser Eldrazi up close, steeling their stomachs for what\'s to come.').
card_multiverse_id('fortified rampart'/'BFZ', '401892').

card_in_set('from beyond', 'BFZ').
card_original_type('from beyond'/'BFZ', 'Enchantment').
card_original_text('from beyond'/'BFZ', 'Devoid (This card has no color.)At the beginning of your upkeep, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"{1}{G}, Sacrifice From Beyond: Search your library for an Eldrazi card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('from beyond', 'BFZ').
card_image_name('from beyond'/'BFZ', 'from beyond').
card_uid('from beyond'/'BFZ', 'BFZ:From Beyond:from beyond').
card_rarity('from beyond'/'BFZ', 'Rare').
card_artist('from beyond'/'BFZ', 'Mathias Kollros').
card_number('from beyond'/'BFZ', '167').
card_multiverse_id('from beyond'/'BFZ', '401893').

card_in_set('geyserfield stalker', 'BFZ').
card_original_type('geyserfield stalker'/'BFZ', 'Creature — Elemental').
card_original_text('geyserfield stalker'/'BFZ', 'Menace (This creature can\'t be blocked except by two or more creatures.)Landfall — Whenever a land enters the battlefield under your control, Geyserfield Stalker gets +2/+2 until end of turn.').
card_first_print('geyserfield stalker', 'BFZ').
card_image_name('geyserfield stalker'/'BFZ', 'geyserfield stalker').
card_uid('geyserfield stalker'/'BFZ', 'BFZ:Geyserfield Stalker:geyserfield stalker').
card_rarity('geyserfield stalker'/'BFZ', 'Common').
card_artist('geyserfield stalker'/'BFZ', 'Deruchenko Alexander').
card_number('geyserfield stalker'/'BFZ', '111').
card_multiverse_id('geyserfield stalker'/'BFZ', '401894').

card_in_set('ghostly sentinel', 'BFZ').
card_original_type('ghostly sentinel'/'BFZ', 'Creature — Kor Spirit').
card_original_text('ghostly sentinel'/'BFZ', 'Flying, vigilance').
card_first_print('ghostly sentinel', 'BFZ').
card_image_name('ghostly sentinel'/'BFZ', 'ghostly sentinel').
card_uid('ghostly sentinel'/'BFZ', 'BFZ:Ghostly Sentinel:ghostly sentinel').
card_rarity('ghostly sentinel'/'BFZ', 'Common').
card_artist('ghostly sentinel'/'BFZ', 'Daarken').
card_number('ghostly sentinel'/'BFZ', '28').
card_flavor_text('ghostly sentinel'/'BFZ', 'Mystics of the Stone Havens call upon the spirits of fallen heroes to defend the refuges.').
card_multiverse_id('ghostly sentinel'/'BFZ', '401895').

card_in_set('giant mantis', 'BFZ').
card_original_type('giant mantis'/'BFZ', 'Creature — Insect').
card_original_text('giant mantis'/'BFZ', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant mantis'/'BFZ', 'giant mantis').
card_uid('giant mantis'/'BFZ', 'BFZ:Giant Mantis:giant mantis').
card_rarity('giant mantis'/'BFZ', 'Common').
card_artist('giant mantis'/'BFZ', 'Lake Hurwitz').
card_number('giant mantis'/'BFZ', '173').
card_flavor_text('giant mantis'/'BFZ', '\"I always thought the great insects of Tazeem were frightening until I saw the Eldrazi. Now I welcome the sight of mantises perched in the treetops.\"—Ryza, Oran-Rief scout').
card_multiverse_id('giant mantis'/'BFZ', '401896').

card_in_set('gideon\'s reproach', 'BFZ').
card_original_type('gideon\'s reproach'/'BFZ', 'Instant').
card_original_text('gideon\'s reproach'/'BFZ', 'Gideon\'s Reproach deals 4 damage to target attacking or blocking creature.').
card_first_print('gideon\'s reproach', 'BFZ').
card_image_name('gideon\'s reproach'/'BFZ', 'gideon\'s reproach').
card_uid('gideon\'s reproach'/'BFZ', 'BFZ:Gideon\'s Reproach:gideon\'s reproach').
card_rarity('gideon\'s reproach'/'BFZ', 'Common').
card_artist('gideon\'s reproach'/'BFZ', 'Dan Scott').
card_number('gideon\'s reproach'/'BFZ', '30').
card_flavor_text('gideon\'s reproach'/'BFZ', '\"Suddenly, Gideon was there with us, clearing the way so that we could escape.\"—The War Diaries').
card_multiverse_id('gideon\'s reproach'/'BFZ', '401898').

card_in_set('gideon, ally of zendikar', 'BFZ').
card_original_type('gideon, ally of zendikar'/'BFZ', 'Planeswalker — Gideon').
card_original_text('gideon, ally of zendikar'/'BFZ', '+1: Until end of turn, Gideon, Ally of Zendikar becomes a 5/5 Human Soldier Ally creature with indestructible that\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.0: Put a 2/2 white Knight Ally creature token onto the battlefield.−4: You get an emblem with \"Creatures you control get +1/+1.\"').
card_first_print('gideon, ally of zendikar', 'BFZ').
card_image_name('gideon, ally of zendikar'/'BFZ', 'gideon, ally of zendikar').
card_uid('gideon, ally of zendikar'/'BFZ', 'BFZ:Gideon, Ally of Zendikar:gideon, ally of zendikar').
card_rarity('gideon, ally of zendikar'/'BFZ', 'Mythic Rare').
card_artist('gideon, ally of zendikar'/'BFZ', 'Eric Deschamps').
card_number('gideon, ally of zendikar'/'BFZ', '29').
card_multiverse_id('gideon, ally of zendikar'/'BFZ', '401897').

card_in_set('goblin war paint', 'BFZ').
card_original_type('goblin war paint'/'BFZ', 'Enchantment — Aura').
card_original_text('goblin war paint'/'BFZ', 'Enchant creatureEnchanted creature gets +2/+2 and has haste.').
card_image_name('goblin war paint'/'BFZ', 'goblin war paint').
card_uid('goblin war paint'/'BFZ', 'BFZ:Goblin War Paint:goblin war paint').
card_rarity('goblin war paint'/'BFZ', 'Common').
card_artist('goblin war paint'/'BFZ', 'Karl Kopinski').
card_number('goblin war paint'/'BFZ', '146').
card_flavor_text('goblin war paint'/'BFZ', '\"The Eldrazi may not be intimidated, but if it boosts the goblins\' confidence, I see no reason to discourage it.\"—Jalun, Affa sentry').
card_multiverse_id('goblin war paint'/'BFZ', '401899').

card_in_set('grave birthing', 'BFZ').
card_original_type('grave birthing'/'BFZ', 'Instant').
card_original_text('grave birthing'/'BFZ', 'Devoid (This card has no color.)Target opponent exiles a card from his or her graveyard. You put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"Draw a card.').
card_first_print('grave birthing', 'BFZ').
card_image_name('grave birthing'/'BFZ', 'grave birthing').
card_uid('grave birthing'/'BFZ', 'BFZ:Grave Birthing:grave birthing').
card_rarity('grave birthing'/'BFZ', 'Common').
card_artist('grave birthing'/'BFZ', 'Craig J Spearing').
card_number('grave birthing'/'BFZ', '93').
card_multiverse_id('grave birthing'/'BFZ', '401900').

card_in_set('greenwarden of murasa', 'BFZ').
card_original_type('greenwarden of murasa'/'BFZ', 'Creature — Elemental').
card_original_text('greenwarden of murasa'/'BFZ', 'When Greenwarden of Murasa enters the battlefield, you may return target card from your graveyard to your hand.When Greenwarden of Murasa dies, you may exile it. If you do, return target card from your graveyard to your hand.').
card_first_print('greenwarden of murasa', 'BFZ').
card_image_name('greenwarden of murasa'/'BFZ', 'greenwarden of murasa').
card_uid('greenwarden of murasa'/'BFZ', 'BFZ:Greenwarden of Murasa:greenwarden of murasa').
card_rarity('greenwarden of murasa'/'BFZ', 'Mythic Rare').
card_artist('greenwarden of murasa'/'BFZ', 'Eric Deschamps').
card_number('greenwarden of murasa'/'BFZ', '174').
card_multiverse_id('greenwarden of murasa'/'BFZ', '401901').

card_in_set('grip of desolation', 'BFZ').
card_original_type('grip of desolation'/'BFZ', 'Instant').
card_original_text('grip of desolation'/'BFZ', 'Devoid (This card has no color.)Exile target creature and target land.').
card_first_print('grip of desolation', 'BFZ').
card_image_name('grip of desolation'/'BFZ', 'grip of desolation').
card_uid('grip of desolation'/'BFZ', 'BFZ:Grip of Desolation:grip of desolation').
card_rarity('grip of desolation'/'BFZ', 'Uncommon').
card_artist('grip of desolation'/'BFZ', 'Lius Lasahido').
card_number('grip of desolation'/'BFZ', '94').
card_flavor_text('grip of desolation'/'BFZ', 'As the hand closed, there was first terror, then numbness, and finally nothingness.').
card_multiverse_id('grip of desolation'/'BFZ', '401902').

card_in_set('grove rumbler', 'BFZ').
card_original_type('grove rumbler'/'BFZ', 'Creature — Elemental').
card_original_text('grove rumbler'/'BFZ', 'TrampleLandfall — Whenever a land enters the battlefield under your control, Grove Rumbler gets +2/+2 until end of turn.').
card_first_print('grove rumbler', 'BFZ').
card_image_name('grove rumbler'/'BFZ', 'grove rumbler').
card_uid('grove rumbler'/'BFZ', 'BFZ:Grove Rumbler:grove rumbler').
card_rarity('grove rumbler'/'BFZ', 'Uncommon').
card_artist('grove rumbler'/'BFZ', 'Greg Opalinski').
card_number('grove rumbler'/'BFZ', '211').
card_flavor_text('grove rumbler'/'BFZ', '\"The land will not wait for the enemy to arrive.\"—Nissa Revane').
card_multiverse_id('grove rumbler'/'BFZ', '401903').

card_in_set('grovetender druids', 'BFZ').
card_original_type('grovetender druids'/'BFZ', 'Creature — Elf Druid Ally').
card_original_text('grovetender druids'/'BFZ', 'Rally — Whenever Grovetender Druids or another Ally enters the battlefield under your control, you may pay {1}. If you do, put a 1/1 green Plant creature token onto the battlefield.').
card_first_print('grovetender druids', 'BFZ').
card_image_name('grovetender druids'/'BFZ', 'grovetender druids').
card_uid('grovetender druids'/'BFZ', 'BFZ:Grovetender Druids:grovetender druids').
card_rarity('grovetender druids'/'BFZ', 'Uncommon').
card_artist('grovetender druids'/'BFZ', 'Chase Stone').
card_number('grovetender druids'/'BFZ', '212').
card_flavor_text('grovetender druids'/'BFZ', '\"The seedlings scream for us to loose them upon the Eldrazi, and we shall oblige.\"').
card_multiverse_id('grovetender druids'/'BFZ', '401904').

card_in_set('gruesome slaughter', 'BFZ').
card_original_type('gruesome slaughter'/'BFZ', 'Sorcery').
card_original_text('gruesome slaughter'/'BFZ', 'Until end of turn, colorless creatures you control gain \"{T}: This creature deals damage equal to its power to target creature.\"').
card_first_print('gruesome slaughter', 'BFZ').
card_image_name('gruesome slaughter'/'BFZ', 'gruesome slaughter').
card_uid('gruesome slaughter'/'BFZ', 'BFZ:Gruesome Slaughter:gruesome slaughter').
card_rarity('gruesome slaughter'/'BFZ', 'Rare').
card_artist('gruesome slaughter'/'BFZ', 'Aleksi Briclot').
card_number('gruesome slaughter'/'BFZ', '9').
card_flavor_text('gruesome slaughter'/'BFZ', 'Though Gideon arrived at Sea Gate too late to prevent the massacre, he still had enough time to lead its survivors to relative safety.').
card_multiverse_id('gruesome slaughter'/'BFZ', '401905').

card_in_set('guardian of tazeem', 'BFZ').
card_original_type('guardian of tazeem'/'BFZ', 'Creature — Sphinx').
card_original_text('guardian of tazeem'/'BFZ', 'FlyingLandfall — Whenever a land enters the battlefield under your control, tap target creature an opponent controls. If that land is an Island, that creature doesn\'t untap during its controller\'s next untap step.').
card_first_print('guardian of tazeem', 'BFZ').
card_image_name('guardian of tazeem'/'BFZ', 'guardian of tazeem').
card_uid('guardian of tazeem'/'BFZ', 'BFZ:Guardian of Tazeem:guardian of tazeem').
card_rarity('guardian of tazeem'/'BFZ', 'Rare').
card_artist('guardian of tazeem'/'BFZ', 'Zack Stella').
card_number('guardian of tazeem'/'BFZ', '78').
card_multiverse_id('guardian of tazeem'/'BFZ', '401906').

card_in_set('guul draz overseer', 'BFZ').
card_original_type('guul draz overseer'/'BFZ', 'Creature — Vampire').
card_original_text('guul draz overseer'/'BFZ', 'FlyingLandfall — Whenever a land enters the battlefield under your control, other creatures you control get +1/+0 until end of turn. If that land is a Swamp, those creatures get +2/+0 until end of turn instead.').
card_first_print('guul draz overseer', 'BFZ').
card_image_name('guul draz overseer'/'BFZ', 'guul draz overseer').
card_uid('guul draz overseer'/'BFZ', 'BFZ:Guul Draz Overseer:guul draz overseer').
card_rarity('guul draz overseer'/'BFZ', 'Rare').
card_artist('guul draz overseer'/'BFZ', 'Karl Kopinski').
card_number('guul draz overseer'/'BFZ', '112').
card_multiverse_id('guul draz overseer'/'BFZ', '401907').

card_in_set('hagra sharpshooter', 'BFZ').
card_original_type('hagra sharpshooter'/'BFZ', 'Creature — Human Assassin Ally').
card_original_text('hagra sharpshooter'/'BFZ', '{4}{B}: Target creature gets -1/-1 until end of turn.').
card_first_print('hagra sharpshooter', 'BFZ').
card_image_name('hagra sharpshooter'/'BFZ', 'hagra sharpshooter').
card_uid('hagra sharpshooter'/'BFZ', 'BFZ:Hagra Sharpshooter:hagra sharpshooter').
card_rarity('hagra sharpshooter'/'BFZ', 'Uncommon').
card_artist('hagra sharpshooter'/'BFZ', 'Josu Hernaiz').
card_number('hagra sharpshooter'/'BFZ', '113').
card_flavor_text('hagra sharpshooter'/'BFZ', '\"It\'s hard to find their weak points, but I very much enjoy the discovery process.\"').
card_multiverse_id('hagra sharpshooter'/'BFZ', '401908').

card_in_set('halimar tidecaller', 'BFZ').
card_original_type('halimar tidecaller'/'BFZ', 'Creature — Human Wizard Ally').
card_original_text('halimar tidecaller'/'BFZ', 'When Halimar Tidecaller enters the battlefield, you may return target card with awaken from your graveyard to your hand.Land creatures you control have flying.').
card_first_print('halimar tidecaller', 'BFZ').
card_image_name('halimar tidecaller'/'BFZ', 'halimar tidecaller').
card_uid('halimar tidecaller'/'BFZ', 'BFZ:Halimar Tidecaller:halimar tidecaller').
card_rarity('halimar tidecaller'/'BFZ', 'Uncommon').
card_artist('halimar tidecaller'/'BFZ', 'Magali Villeneuve').
card_number('halimar tidecaller'/'BFZ', '79').
card_flavor_text('halimar tidecaller'/'BFZ', '\"Quiet the waves? Why would I want to do that?\"').
card_multiverse_id('halimar tidecaller'/'BFZ', '401909').

card_in_set('hedron archive', 'BFZ').
card_original_type('hedron archive'/'BFZ', 'Artifact').
card_original_text('hedron archive'/'BFZ', '{T}: Add {2} to your mana pool.{2}, {T}, Sacrifice Hedron Archive: Draw two cards.').
card_first_print('hedron archive', 'BFZ').
card_image_name('hedron archive'/'BFZ', 'hedron archive').
card_uid('hedron archive'/'BFZ', 'BFZ:Hedron Archive:hedron archive').
card_rarity('hedron archive'/'BFZ', 'Uncommon').
card_artist('hedron archive'/'BFZ', 'Craig J Spearing').
card_number('hedron archive'/'BFZ', '223').
card_flavor_text('hedron archive'/'BFZ', '\"You\'ve begun to understand the hedrons\' true purpose,\" said Ugin. \"The Eldrazi can be imprisoned.\"\"And how did that work out last time?\" asked Jace.').
card_multiverse_id('hedron archive'/'BFZ', '401910').

card_in_set('hedron blade', 'BFZ').
card_original_type('hedron blade'/'BFZ', 'Artifact — Equipment').
card_original_text('hedron blade'/'BFZ', 'Equipped creature gets +1/+1.Whenever equipped creature becomes blocked by one or more colorless creatures, it gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)Equip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('hedron blade', 'BFZ').
card_image_name('hedron blade'/'BFZ', 'hedron blade').
card_uid('hedron blade'/'BFZ', 'BFZ:Hedron Blade:hedron blade').
card_rarity('hedron blade'/'BFZ', 'Common').
card_artist('hedron blade'/'BFZ', 'Zack Stella').
card_number('hedron blade'/'BFZ', '224').
card_multiverse_id('hedron blade'/'BFZ', '401911').

card_in_set('herald of kozilek', 'BFZ').
card_original_type('herald of kozilek'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('herald of kozilek'/'BFZ', 'Devoid (This card has no color.)Colorless spells you cast cost {1} less to cast.').
card_first_print('herald of kozilek', 'BFZ').
card_image_name('herald of kozilek'/'BFZ', 'herald of kozilek').
card_uid('herald of kozilek'/'BFZ', 'BFZ:Herald of Kozilek:herald of kozilek').
card_rarity('herald of kozilek'/'BFZ', 'Uncommon').
card_artist('herald of kozilek'/'BFZ', 'Johannes Voss').
card_number('herald of kozilek'/'BFZ', '205').
card_flavor_text('herald of kozilek'/'BFZ', '\"Some Eldrazi, their purpose is obvious: suck the life out of the land, birth new Eldrazi. Others aren\'t so obvious, and those are the ones that make me really nervous.\"—Chadir the Navigator').
card_multiverse_id('herald of kozilek'/'BFZ', '401912').

card_in_set('hero of goma fada', 'BFZ').
card_original_type('hero of goma fada'/'BFZ', 'Creature — Human Knight Ally').
card_original_text('hero of goma fada'/'BFZ', 'Rally — Whenever Hero of Goma Fada or another Ally enters the battlefield under your control, creatures you control gain indestructible until end of turn.').
card_first_print('hero of goma fada', 'BFZ').
card_image_name('hero of goma fada'/'BFZ', 'hero of goma fada').
card_uid('hero of goma fada'/'BFZ', 'BFZ:Hero of Goma Fada:hero of goma fada').
card_rarity('hero of goma fada'/'BFZ', 'Rare').
card_artist('hero of goma fada'/'BFZ', 'Lake Hurwitz').
card_number('hero of goma fada'/'BFZ', '31').
card_flavor_text('hero of goma fada'/'BFZ', '\"They seek to break us. Let us show them our strength!\"').
card_multiverse_id('hero of goma fada'/'BFZ', '401913').

card_in_set('horribly awry', 'BFZ').
card_original_type('horribly awry'/'BFZ', 'Instant').
card_original_text('horribly awry'/'BFZ', 'Devoid (This card has no color.)Counter target creature spell with converted mana cost 4 or less. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_first_print('horribly awry', 'BFZ').
card_image_name('horribly awry'/'BFZ', 'horribly awry').
card_uid('horribly awry'/'BFZ', 'BFZ:Horribly Awry:horribly awry').
card_rarity('horribly awry'/'BFZ', 'Uncommon').
card_artist('horribly awry'/'BFZ', 'Clint Cearley').
card_number('horribly awry'/'BFZ', '59').
card_flavor_text('horribly awry'/'BFZ', '\"We are nothing but dust to them—our bodies, our actions, even our words.\"—Jori En, expedition leader').
card_multiverse_id('horribly awry'/'BFZ', '401914').

card_in_set('incubator drone', 'BFZ').
card_original_type('incubator drone'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('incubator drone'/'BFZ', 'Devoid (This card has no color.)When Incubator Drone enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('incubator drone', 'BFZ').
card_image_name('incubator drone'/'BFZ', 'incubator drone').
card_uid('incubator drone'/'BFZ', 'BFZ:Incubator Drone:incubator drone').
card_rarity('incubator drone'/'BFZ', 'Common').
card_artist('incubator drone'/'BFZ', 'Cynthia Sheppard').
card_number('incubator drone'/'BFZ', '60').
card_flavor_text('incubator drone'/'BFZ', 'The touch of an Eldrazi brings life and death in turn.').
card_multiverse_id('incubator drone'/'BFZ', '401915').

card_in_set('infuse with the elements', 'BFZ').
card_original_type('infuse with the elements'/'BFZ', 'Instant').
card_original_text('infuse with the elements'/'BFZ', 'Converge — Put X +1/+1 counters on target creature, where X is the number of colors of mana spent to cast Infuse with the Elements. That creature gains trample until end of turn.').
card_first_print('infuse with the elements', 'BFZ').
card_image_name('infuse with the elements'/'BFZ', 'infuse with the elements').
card_uid('infuse with the elements'/'BFZ', 'BFZ:Infuse with the Elements:infuse with the elements').
card_rarity('infuse with the elements'/'BFZ', 'Uncommon').
card_artist('infuse with the elements'/'BFZ', 'Daniel Ljunggren').
card_number('infuse with the elements'/'BFZ', '175').
card_flavor_text('infuse with the elements'/'BFZ', '\"Zendikar has given us the weapons to wage war on the appropriate scale.\"—Najiya, leader of the Tajuru').
card_multiverse_id('infuse with the elements'/'BFZ', '401916').

card_in_set('inspired charge', 'BFZ').
card_original_type('inspired charge'/'BFZ', 'Instant').
card_original_text('inspired charge'/'BFZ', 'Creatures you control get +2/+1 until end of turn.').
card_image_name('inspired charge'/'BFZ', 'inspired charge').
card_uid('inspired charge'/'BFZ', 'BFZ:Inspired Charge:inspired charge').
card_rarity('inspired charge'/'BFZ', 'Common').
card_artist('inspired charge'/'BFZ', 'Willian Murai').
card_number('inspired charge'/'BFZ', '32').
card_flavor_text('inspired charge'/'BFZ', '\"For the lands you have lost and the beauty that remains, for freedom and the future, we must hold nothing back!\"—Gideon Jura').
card_multiverse_id('inspired charge'/'BFZ', '401917').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island1').
card_uid('island'/'BFZ', 'BFZ:Island:island1').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Noah Bradley').
card_number('island'/'BFZ', '255').
card_multiverse_id('island'/'BFZ', '401925').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island10').
card_uid('island'/'BFZ', 'BFZ:Island:island10').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Tianhua X').
card_number('island'/'BFZ', '259').
card_multiverse_id('island'/'BFZ', '401919').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island2').
card_uid('island'/'BFZ', 'BFZ:Island:island2').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Noah Bradley').
card_number('island'/'BFZ', '255').
card_multiverse_id('island'/'BFZ', '401922').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island3').
card_uid('island'/'BFZ', 'BFZ:Island:island3').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Sam Burley').
card_number('island'/'BFZ', '256').
card_multiverse_id('island'/'BFZ', '401918').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island4').
card_uid('island'/'BFZ', 'BFZ:Island:island4').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Sam Burley').
card_number('island'/'BFZ', '256').
card_multiverse_id('island'/'BFZ', '401923').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island5').
card_uid('island'/'BFZ', 'BFZ:Island:island5').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Adam Paquette').
card_number('island'/'BFZ', '257').
card_multiverse_id('island'/'BFZ', '401926').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island6').
card_uid('island'/'BFZ', 'BFZ:Island:island6').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Adam Paquette').
card_number('island'/'BFZ', '257').
card_multiverse_id('island'/'BFZ', '401920').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island7').
card_uid('island'/'BFZ', 'BFZ:Island:island7').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Vincent Proce').
card_number('island'/'BFZ', '258').
card_multiverse_id('island'/'BFZ', '401924').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island8').
card_uid('island'/'BFZ', 'BFZ:Island:island8').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Vincent Proce').
card_number('island'/'BFZ', '258').
card_multiverse_id('island'/'BFZ', '401927').

card_in_set('island', 'BFZ').
card_original_type('island'/'BFZ', 'Basic Land — Island').
card_original_text('island'/'BFZ', 'U').
card_image_name('island'/'BFZ', 'island9').
card_uid('island'/'BFZ', 'BFZ:Island:island9').
card_rarity('island'/'BFZ', 'Basic Land').
card_artist('island'/'BFZ', 'Tianhua X').
card_number('island'/'BFZ', '259').
card_multiverse_id('island'/'BFZ', '401921').

card_in_set('jaddi offshoot', 'BFZ').
card_original_type('jaddi offshoot'/'BFZ', 'Creature — Plant').
card_original_text('jaddi offshoot'/'BFZ', 'DefenderLandfall — Whenever a land enters the battlefield under your control, you gain 1 life.').
card_first_print('jaddi offshoot', 'BFZ').
card_image_name('jaddi offshoot'/'BFZ', 'jaddi offshoot').
card_uid('jaddi offshoot'/'BFZ', 'BFZ:Jaddi Offshoot:jaddi offshoot').
card_rarity('jaddi offshoot'/'BFZ', 'Uncommon').
card_artist('jaddi offshoot'/'BFZ', 'Daarken').
card_number('jaddi offshoot'/'BFZ', '176').
card_flavor_text('jaddi offshoot'/'BFZ', 'On Murasa, even the trees grow trees.').
card_multiverse_id('jaddi offshoot'/'BFZ', '401928').

card_in_set('kalastria healer', 'BFZ').
card_original_type('kalastria healer'/'BFZ', 'Creature — Vampire Cleric Ally').
card_original_text('kalastria healer'/'BFZ', 'Rally — Whenever Kalastria Healer or another Ally enters the battlefield under your control, each opponent loses 1 life and you gain 1 life.').
card_first_print('kalastria healer', 'BFZ').
card_image_name('kalastria healer'/'BFZ', 'kalastria healer').
card_uid('kalastria healer'/'BFZ', 'BFZ:Kalastria Healer:kalastria healer').
card_rarity('kalastria healer'/'BFZ', 'Common').
card_artist('kalastria healer'/'BFZ', 'Anthony Palumbo').
card_number('kalastria healer'/'BFZ', '114').
card_flavor_text('kalastria healer'/'BFZ', '\"Time and again, I have demonstrated my skills as a healer. Why, then, are you so ill at ease?\"').
card_multiverse_id('kalastria healer'/'BFZ', '401929').

card_in_set('kalastria nightwatch', 'BFZ').
card_original_type('kalastria nightwatch'/'BFZ', 'Creature — Vampire Warrior Ally').
card_original_text('kalastria nightwatch'/'BFZ', 'Whenever you gain life, Kalastria Nightwatch gains flying until end of turn.').
card_first_print('kalastria nightwatch', 'BFZ').
card_image_name('kalastria nightwatch'/'BFZ', 'kalastria nightwatch').
card_uid('kalastria nightwatch'/'BFZ', 'BFZ:Kalastria Nightwatch:kalastria nightwatch').
card_rarity('kalastria nightwatch'/'BFZ', 'Common').
card_artist('kalastria nightwatch'/'BFZ', 'Jama Jurabaev').
card_number('kalastria nightwatch'/'BFZ', '115').
card_flavor_text('kalastria nightwatch'/'BFZ', '\"Kalitas may have returned to mindless servitude beneath the Eldrazi, but we say never again.\"—Drana, Kalastria bloodchief').
card_multiverse_id('kalastria nightwatch'/'BFZ', '401930').

card_in_set('kiora, master of the depths', 'BFZ').
card_original_type('kiora, master of the depths'/'BFZ', 'Planeswalker — Kiora').
card_original_text('kiora, master of the depths'/'BFZ', '+1: Untap up to one target creature and up to one target land.−2: Reveal the top four cards of your library. You may put a creature card and/or a land card from among them into your hand. Put the rest into your graveyard.−8: You get an emblem with \"Whenever a creature enters the battlefield under your control, you may have it fight target creature.\" Then put three 8/8 blue Octopus creature tokens onto the battlefield.').
card_first_print('kiora, master of the depths', 'BFZ').
card_image_name('kiora, master of the depths'/'BFZ', 'kiora, master of the depths').
card_uid('kiora, master of the depths'/'BFZ', 'BFZ:Kiora, Master of the Depths:kiora, master of the depths').
card_rarity('kiora, master of the depths'/'BFZ', 'Mythic Rare').
card_artist('kiora, master of the depths'/'BFZ', 'Jason Chan').
card_number('kiora, master of the depths'/'BFZ', '213').
card_multiverse_id('kiora, master of the depths'/'BFZ', '401931').

card_in_set('kitesail scout', 'BFZ').
card_original_type('kitesail scout'/'BFZ', 'Creature — Kor Scout').
card_original_text('kitesail scout'/'BFZ', 'Flying').
card_first_print('kitesail scout', 'BFZ').
card_image_name('kitesail scout'/'BFZ', 'kitesail scout').
card_uid('kitesail scout'/'BFZ', 'BFZ:Kitesail Scout:kitesail scout').
card_rarity('kitesail scout'/'BFZ', 'Common').
card_artist('kitesail scout'/'BFZ', 'Dan Scott').
card_number('kitesail scout'/'BFZ', '33').
card_flavor_text('kitesail scout'/'BFZ', '\"The wind in one\'s hair, the sun on one\'s back, the joy of open skies . . . the next generation of kor must know this kind of peace.\"').
card_multiverse_id('kitesail scout'/'BFZ', '401932').

card_in_set('kor bladewhirl', 'BFZ').
card_original_type('kor bladewhirl'/'BFZ', 'Creature — Kor Soldier Ally').
card_original_text('kor bladewhirl'/'BFZ', 'Rally — Whenever Kor Bladewhirl or another Ally enters the battlefield under your control, creatures you control gain first strike until end of turn.').
card_first_print('kor bladewhirl', 'BFZ').
card_image_name('kor bladewhirl'/'BFZ', 'kor bladewhirl').
card_uid('kor bladewhirl'/'BFZ', 'BFZ:Kor Bladewhirl:kor bladewhirl').
card_rarity('kor bladewhirl'/'BFZ', 'Uncommon').
card_artist('kor bladewhirl'/'BFZ', 'Steven Belledin').
card_number('kor bladewhirl'/'BFZ', '34').
card_flavor_text('kor bladewhirl'/'BFZ', 'The high-pitched whirl of a hook is the only battle cry she needs.').
card_multiverse_id('kor bladewhirl'/'BFZ', '401933').

card_in_set('kor castigator', 'BFZ').
card_original_type('kor castigator'/'BFZ', 'Creature — Kor Wizard Ally').
card_original_text('kor castigator'/'BFZ', 'Kor Castigator can\'t be blocked by Eldrazi Scions.').
card_first_print('kor castigator', 'BFZ').
card_image_name('kor castigator'/'BFZ', 'kor castigator').
card_uid('kor castigator'/'BFZ', 'BFZ:Kor Castigator:kor castigator').
card_rarity('kor castigator'/'BFZ', 'Common').
card_artist('kor castigator'/'BFZ', 'Greg Opalinski').
card_number('kor castigator'/'BFZ', '35').
card_flavor_text('kor castigator'/'BFZ', '\"He told us not to worry, and he walked into the cavern. The swirling mass of Eldrazi parted to admit him, then closed in his wake.\"—Akiri, kor line-slinger').
card_multiverse_id('kor castigator'/'BFZ', '401934').

card_in_set('kor entanglers', 'BFZ').
card_original_type('kor entanglers'/'BFZ', 'Creature — Kor Soldier Ally').
card_original_text('kor entanglers'/'BFZ', 'Rally — Whenever Kor Entanglers or another Ally enters the battlefield under your control, tap target creature an opponent controls.').
card_first_print('kor entanglers', 'BFZ').
card_image_name('kor entanglers'/'BFZ', 'kor entanglers').
card_uid('kor entanglers'/'BFZ', 'BFZ:Kor Entanglers:kor entanglers').
card_rarity('kor entanglers'/'BFZ', 'Uncommon').
card_artist('kor entanglers'/'BFZ', 'Jason Rainville').
card_number('kor entanglers'/'BFZ', '36').
card_flavor_text('kor entanglers'/'BFZ', '\"We came into this world together. We fight for this world together. We\'ll leave this world together.\"').
card_multiverse_id('kor entanglers'/'BFZ', '401935').

card_in_set('kozilek\'s channeler', 'BFZ').
card_original_type('kozilek\'s channeler'/'BFZ', 'Creature — Eldrazi').
card_original_text('kozilek\'s channeler'/'BFZ', '{T}: Add {2} to your mana pool.').
card_first_print('kozilek\'s channeler', 'BFZ').
card_image_name('kozilek\'s channeler'/'BFZ', 'kozilek\'s channeler').
card_uid('kozilek\'s channeler'/'BFZ', 'BFZ:Kozilek\'s Channeler:kozilek\'s channeler').
card_rarity('kozilek\'s channeler'/'BFZ', 'Common').
card_artist('kozilek\'s channeler'/'BFZ', 'Jason Felix').
card_number('kozilek\'s channeler'/'BFZ', '10').
card_flavor_text('kozilek\'s channeler'/'BFZ', '\"In the dark places of our world, something horrible is growing. I fear our foes may be more numerous than we had imagined.\"—Nissa Revane').
card_multiverse_id('kozilek\'s channeler'/'BFZ', '401936').

card_in_set('kozilek\'s sentinel', 'BFZ').
card_original_type('kozilek\'s sentinel'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('kozilek\'s sentinel'/'BFZ', 'Devoid (This card has no color.)Whenever you cast a colorless spell, Kozilek\'s Sentinel gets +1/+0 until end of turn.').
card_first_print('kozilek\'s sentinel', 'BFZ').
card_image_name('kozilek\'s sentinel'/'BFZ', 'kozilek\'s sentinel').
card_uid('kozilek\'s sentinel'/'BFZ', 'BFZ:Kozilek\'s Sentinel:kozilek\'s sentinel').
card_rarity('kozilek\'s sentinel'/'BFZ', 'Common').
card_artist('kozilek\'s sentinel'/'BFZ', 'Raymond Swanland').
card_number('kozilek\'s sentinel'/'BFZ', '129').
card_flavor_text('kozilek\'s sentinel'/'BFZ', 'It moves through the tunnels as slowly and surely as the shifting stones.').
card_multiverse_id('kozilek\'s sentinel'/'BFZ', '401937').

card_in_set('lantern scout', 'BFZ').
card_original_type('lantern scout'/'BFZ', 'Creature — Human Scout Ally').
card_original_text('lantern scout'/'BFZ', 'Rally — Whenever Lantern Scout or another Ally enters the battlefield under your control, creatures you control gain lifelink until end of turn.').
card_first_print('lantern scout', 'BFZ').
card_image_name('lantern scout'/'BFZ', 'lantern scout').
card_uid('lantern scout'/'BFZ', 'BFZ:Lantern Scout:lantern scout').
card_rarity('lantern scout'/'BFZ', 'Rare').
card_artist('lantern scout'/'BFZ', 'Steven Belledin').
card_number('lantern scout'/'BFZ', '37').
card_flavor_text('lantern scout'/'BFZ', 'Hedron lanterns fend off more than just the darkness.').
card_multiverse_id('lantern scout'/'BFZ', '401938').

card_in_set('lavastep raider', 'BFZ').
card_original_type('lavastep raider'/'BFZ', 'Creature — Goblin Warrior').
card_original_text('lavastep raider'/'BFZ', '{2}{R}: Lavastep Raider gets +2/+0 until end of turn.').
card_first_print('lavastep raider', 'BFZ').
card_image_name('lavastep raider'/'BFZ', 'lavastep raider').
card_uid('lavastep raider'/'BFZ', 'BFZ:Lavastep Raider:lavastep raider').
card_rarity('lavastep raider'/'BFZ', 'Common').
card_artist('lavastep raider'/'BFZ', 'Matt Stewart').
card_number('lavastep raider'/'BFZ', '147').
card_flavor_text('lavastep raider'/'BFZ', 'Goblins were first to see the potential of hedrons in the fight against the Eldrazi, for the magical stones came ready-made with pointy bits.').
card_multiverse_id('lavastep raider'/'BFZ', '401939').

card_in_set('lifespring druid', 'BFZ').
card_original_type('lifespring druid'/'BFZ', 'Creature — Elf Druid').
card_original_text('lifespring druid'/'BFZ', '{T}: Add one mana of any color to your mana pool.').
card_first_print('lifespring druid', 'BFZ').
card_image_name('lifespring druid'/'BFZ', 'lifespring druid').
card_uid('lifespring druid'/'BFZ', 'BFZ:Lifespring Druid:lifespring druid').
card_rarity('lifespring druid'/'BFZ', 'Common').
card_artist('lifespring druid'/'BFZ', 'Willian Murai').
card_number('lifespring druid'/'BFZ', '177').
card_flavor_text('lifespring druid'/'BFZ', '\"The land is not dead, merely sick. I will nurse it back to health.\"').
card_multiverse_id('lifespring druid'/'BFZ', '401940').

card_in_set('lithomancer\'s focus', 'BFZ').
card_original_type('lithomancer\'s focus'/'BFZ', 'Instant').
card_original_text('lithomancer\'s focus'/'BFZ', 'Target creature gets +2/+2 until end of turn. Prevent all damage that would be dealt to that creature this turn by colorless sources.').
card_first_print('lithomancer\'s focus', 'BFZ').
card_image_name('lithomancer\'s focus'/'BFZ', 'lithomancer\'s focus').
card_uid('lithomancer\'s focus'/'BFZ', 'BFZ:Lithomancer\'s Focus:lithomancer\'s focus').
card_rarity('lithomancer\'s focus'/'BFZ', 'Common').
card_artist('lithomancer\'s focus'/'BFZ', 'Cynthia Sheppard').
card_number('lithomancer\'s focus'/'BFZ', '38').
card_flavor_text('lithomancer\'s focus'/'BFZ', '\"Even in the hands of amateurs, Nahiri\'s hedrons can harm the Eldrazi.\"—Ugin').
card_multiverse_id('lithomancer\'s focus'/'BFZ', '401941').

card_in_set('looming spires', 'BFZ').
card_original_type('looming spires'/'BFZ', 'Land').
card_original_text('looming spires'/'BFZ', 'Looming Spires enters the battlefield tapped.When Looming Spires enters the battlefield, target creature gets +1/+1 and gains first strike until end of turn.{T}: Add {R} to your mana pool.').
card_first_print('looming spires', 'BFZ').
card_image_name('looming spires'/'BFZ', 'looming spires').
card_uid('looming spires'/'BFZ', 'BFZ:Looming Spires:looming spires').
card_rarity('looming spires'/'BFZ', 'Common').
card_artist('looming spires'/'BFZ', 'Florian de Gesincourt').
card_number('looming spires'/'BFZ', '238').
card_multiverse_id('looming spires'/'BFZ', '401942').

card_in_set('lumbering falls', 'BFZ').
card_original_type('lumbering falls'/'BFZ', 'Land').
card_original_text('lumbering falls'/'BFZ', 'Lumbering Falls enters the battlefield tapped.{T}: Add {G} or {U} to your mana pool.{2}{G}{U}: Lumbering Falls becomes a 3/3 green and blue Elemental creature with hexproof until end of turn. It\'s still a land.').
card_first_print('lumbering falls', 'BFZ').
card_image_name('lumbering falls'/'BFZ', 'lumbering falls').
card_uid('lumbering falls'/'BFZ', 'BFZ:Lumbering Falls:lumbering falls').
card_rarity('lumbering falls'/'BFZ', 'Rare').
card_artist('lumbering falls'/'BFZ', 'Titus Lunter').
card_number('lumbering falls'/'BFZ', '239').
card_multiverse_id('lumbering falls'/'BFZ', '401943').

card_in_set('makindi patrol', 'BFZ').
card_original_type('makindi patrol'/'BFZ', 'Creature — Human Knight Ally').
card_original_text('makindi patrol'/'BFZ', 'Rally — Whenever Makindi Patrol or another Ally enters the battlefield under your control, creatures you control gain vigilance until end of turn.').
card_first_print('makindi patrol', 'BFZ').
card_image_name('makindi patrol'/'BFZ', 'makindi patrol').
card_uid('makindi patrol'/'BFZ', 'BFZ:Makindi Patrol:makindi patrol').
card_rarity('makindi patrol'/'BFZ', 'Common').
card_artist('makindi patrol'/'BFZ', 'David Palumbo').
card_number('makindi patrol'/'BFZ', '39').
card_flavor_text('makindi patrol'/'BFZ', 'Working with his noble mount, he notices every form on the horizon, every scent in the air, every tremor in the earth.').
card_multiverse_id('makindi patrol'/'BFZ', '401944').

card_in_set('makindi sliderunner', 'BFZ').
card_original_type('makindi sliderunner'/'BFZ', 'Creature — Beast').
card_original_text('makindi sliderunner'/'BFZ', 'TrampleLandfall — Whenever a land enters the battlefield under your control, Makindi Sliderunner gets +1/+1 until end of turn.').
card_first_print('makindi sliderunner', 'BFZ').
card_image_name('makindi sliderunner'/'BFZ', 'makindi sliderunner').
card_uid('makindi sliderunner'/'BFZ', 'BFZ:Makindi Sliderunner:makindi sliderunner').
card_rarity('makindi sliderunner'/'BFZ', 'Common').
card_artist('makindi sliderunner'/'BFZ', 'Matt Stewart').
card_number('makindi sliderunner'/'BFZ', '148').
card_flavor_text('makindi sliderunner'/'BFZ', 'After a battle, it breaks the hillsides into manageable pieces to prepare for next time.').
card_multiverse_id('makindi sliderunner'/'BFZ', '401945').

card_in_set('malakir familiar', 'BFZ').
card_original_type('malakir familiar'/'BFZ', 'Creature — Bat').
card_original_text('malakir familiar'/'BFZ', 'Flying, deathtouchWhenever you gain life, Malakir Familiar gets +1/+1 until end of turn.').
card_first_print('malakir familiar', 'BFZ').
card_image_name('malakir familiar'/'BFZ', 'malakir familiar').
card_uid('malakir familiar'/'BFZ', 'BFZ:Malakir Familiar:malakir familiar').
card_rarity('malakir familiar'/'BFZ', 'Uncommon').
card_artist('malakir familiar'/'BFZ', 'Alejandro Mirabal').
card_number('malakir familiar'/'BFZ', '116').
card_flavor_text('malakir familiar'/'BFZ', '\"They are deadly, and they are loyal. We can spare them a little blood.\"—Harak, Malakir bloodwitch').
card_multiverse_id('malakir familiar'/'BFZ', '401946').

card_in_set('march from the tomb', 'BFZ').
card_original_type('march from the tomb'/'BFZ', 'Sorcery').
card_original_text('march from the tomb'/'BFZ', 'Return any number of target Ally creature cards with total converted mana cost 8 or less from your graveyard to the battlefield.').
card_first_print('march from the tomb', 'BFZ').
card_image_name('march from the tomb'/'BFZ', 'march from the tomb').
card_uid('march from the tomb'/'BFZ', 'BFZ:March from the Tomb:march from the tomb').
card_rarity('march from the tomb'/'BFZ', 'Rare').
card_artist('march from the tomb'/'BFZ', 'Lake Hurwitz').
card_number('march from the tomb'/'BFZ', '214').
card_flavor_text('march from the tomb'/'BFZ', 'They came from all walks of life, from every part of Zendikar, and they returned with one unifying cause.').
card_multiverse_id('march from the tomb'/'BFZ', '401947').

card_in_set('mind raker', 'BFZ').
card_original_type('mind raker'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('mind raker'/'BFZ', 'Devoid (This card has no color.)When Mind Raker enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, each opponent discards a card.').
card_first_print('mind raker', 'BFZ').
card_image_name('mind raker'/'BFZ', 'mind raker').
card_uid('mind raker'/'BFZ', 'BFZ:Mind Raker:mind raker').
card_rarity('mind raker'/'BFZ', 'Common').
card_artist('mind raker'/'BFZ', 'Lius Lasahido').
card_number('mind raker'/'BFZ', '95').
card_flavor_text('mind raker'/'BFZ', 'It consumes any dreams of victory.').
card_multiverse_id('mind raker'/'BFZ', '401948').

card_in_set('mire\'s malice', 'BFZ').
card_original_type('mire\'s malice'/'BFZ', 'Sorcery').
card_original_text('mire\'s malice'/'BFZ', 'Target opponent discards two cards.Awaken 3—{5}{B} (If you cast this spell for {5}{B}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('mire\'s malice', 'BFZ').
card_image_name('mire\'s malice'/'BFZ', 'mire\'s malice').
card_uid('mire\'s malice'/'BFZ', 'BFZ:Mire\'s Malice:mire\'s malice').
card_rarity('mire\'s malice'/'BFZ', 'Common').
card_artist('mire\'s malice'/'BFZ', 'Jakub Kasper').
card_number('mire\'s malice'/'BFZ', '117').
card_multiverse_id('mire\'s malice'/'BFZ', '401949').

card_in_set('mist intruder', 'BFZ').
card_original_type('mist intruder'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('mist intruder'/'BFZ', 'Devoid (This card has no color.)FlyingIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)').
card_first_print('mist intruder', 'BFZ').
card_image_name('mist intruder'/'BFZ', 'mist intruder').
card_uid('mist intruder'/'BFZ', 'BFZ:Mist Intruder:mist intruder').
card_rarity('mist intruder'/'BFZ', 'Common').
card_artist('mist intruder'/'BFZ', 'Jason Rainville').
card_number('mist intruder'/'BFZ', '61').
card_flavor_text('mist intruder'/'BFZ', 'The wastes spread as quickly as the wind.').
card_multiverse_id('mist intruder'/'BFZ', '401950').

card_in_set('molten nursery', 'BFZ').
card_original_type('molten nursery'/'BFZ', 'Enchantment').
card_original_text('molten nursery'/'BFZ', 'Devoid (This card has no color.)Whenever you cast a colorless spell, Molten Nursery deals 1 damage to target creature or player.').
card_first_print('molten nursery', 'BFZ').
card_image_name('molten nursery'/'BFZ', 'molten nursery').
card_uid('molten nursery'/'BFZ', 'BFZ:Molten Nursery:molten nursery').
card_rarity('molten nursery'/'BFZ', 'Uncommon').
card_artist('molten nursery'/'BFZ', 'Raymond Swanland').
card_number('molten nursery'/'BFZ', '130').
card_flavor_text('molten nursery'/'BFZ', '\"Some detours just aren\'t worth it.\"—Raff Slugeater, goblin shortcutter').
card_multiverse_id('molten nursery'/'BFZ', '401951').

card_in_set('mortuary mire', 'BFZ').
card_original_type('mortuary mire'/'BFZ', 'Land').
card_original_text('mortuary mire'/'BFZ', 'Mortuary Mire enters the battlefield tapped.When Mortuary Mire enters the battlefield, you may put target creature card from your graveyard on top of your library.{T}: Add {B} to your mana pool.').
card_first_print('mortuary mire', 'BFZ').
card_image_name('mortuary mire'/'BFZ', 'mortuary mire').
card_uid('mortuary mire'/'BFZ', 'BFZ:Mortuary Mire:mortuary mire').
card_rarity('mortuary mire'/'BFZ', 'Common').
card_artist('mortuary mire'/'BFZ', 'James Paick').
card_number('mortuary mire'/'BFZ', '240').
card_multiverse_id('mortuary mire'/'BFZ', '401952').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain1').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain1').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Noah Bradley').
card_number('mountain'/'BFZ', '265').
card_multiverse_id('mountain'/'BFZ', '401962').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain10').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain10').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Tianhua X').
card_number('mountain'/'BFZ', '269').
card_multiverse_id('mountain'/'BFZ', '401953').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain2').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain2').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Noah Bradley').
card_number('mountain'/'BFZ', '265').
card_multiverse_id('mountain'/'BFZ', '401958').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain3').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain3').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Sam Burley').
card_number('mountain'/'BFZ', '266').
card_multiverse_id('mountain'/'BFZ', '401957').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain4').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain4').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Sam Burley').
card_number('mountain'/'BFZ', '266').
card_multiverse_id('mountain'/'BFZ', '401959').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain5').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain5').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Véronique Meignaud').
card_number('mountain'/'BFZ', '267').
card_multiverse_id('mountain'/'BFZ', '401954').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain6').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain6').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Véronique Meignaud').
card_number('mountain'/'BFZ', '267').
card_multiverse_id('mountain'/'BFZ', '401956').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain7').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain7').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Adam Paquette').
card_number('mountain'/'BFZ', '268').
card_multiverse_id('mountain'/'BFZ', '401961').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain8').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain8').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Adam Paquette').
card_number('mountain'/'BFZ', '268').
card_multiverse_id('mountain'/'BFZ', '401955').

card_in_set('mountain', 'BFZ').
card_original_type('mountain'/'BFZ', 'Basic Land — Mountain').
card_original_text('mountain'/'BFZ', 'R').
card_image_name('mountain'/'BFZ', 'mountain9').
card_uid('mountain'/'BFZ', 'BFZ:Mountain:mountain9').
card_rarity('mountain'/'BFZ', 'Basic Land').
card_artist('mountain'/'BFZ', 'Tianhua X').
card_number('mountain'/'BFZ', '269').
card_multiverse_id('mountain'/'BFZ', '401960').

card_in_set('munda, ambush leader', 'BFZ').
card_original_type('munda, ambush leader'/'BFZ', 'Legendary Creature — Kor Ally').
card_original_text('munda, ambush leader'/'BFZ', 'HasteRally — Whenever Munda, Ambush Leader or another Ally enters the battlefield under your control, you may look at the top four cards of your library. If you do, reveal any number of Ally cards from among them, then put those cards on top of your library in any order and the rest on the bottom in any order.').
card_first_print('munda, ambush leader', 'BFZ').
card_image_name('munda, ambush leader'/'BFZ', 'munda, ambush leader').
card_uid('munda, ambush leader'/'BFZ', 'BFZ:Munda, Ambush Leader:munda, ambush leader').
card_rarity('munda, ambush leader'/'BFZ', 'Rare').
card_artist('munda, ambush leader'/'BFZ', 'Johannes Voss').
card_number('munda, ambush leader'/'BFZ', '215').
card_multiverse_id('munda, ambush leader'/'BFZ', '401963').

card_in_set('murasa ranger', 'BFZ').
card_original_type('murasa ranger'/'BFZ', 'Creature — Human Warrior').
card_original_text('murasa ranger'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {3}{G}. If you do, put two +1/+1 counters on Murasa Ranger.').
card_first_print('murasa ranger', 'BFZ').
card_image_name('murasa ranger'/'BFZ', 'murasa ranger').
card_uid('murasa ranger'/'BFZ', 'BFZ:Murasa Ranger:murasa ranger').
card_rarity('murasa ranger'/'BFZ', 'Uncommon').
card_artist('murasa ranger'/'BFZ', 'Eric Deschamps').
card_number('murasa ranger'/'BFZ', '178').
card_flavor_text('murasa ranger'/'BFZ', '\"If you\'re not prepared to fight, you\'d best be prepared to die.\"').
card_multiverse_id('murasa ranger'/'BFZ', '401964').

card_in_set('murk strider', 'BFZ').
card_original_type('murk strider'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('murk strider'/'BFZ', 'Devoid (This card has no color.)When Murk Strider enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, return target creature to its owner\'s hand.').
card_first_print('murk strider', 'BFZ').
card_image_name('murk strider'/'BFZ', 'murk strider').
card_uid('murk strider'/'BFZ', 'BFZ:Murk Strider:murk strider').
card_rarity('murk strider'/'BFZ', 'Common').
card_artist('murk strider'/'BFZ', 'Chase Stone').
card_number('murk strider'/'BFZ', '62').
card_flavor_text('murk strider'/'BFZ', 'If you can see it, it\'s too late.').
card_multiverse_id('murk strider'/'BFZ', '401965').

card_in_set('natural connection', 'BFZ').
card_original_type('natural connection'/'BFZ', 'Instant').
card_original_text('natural connection'/'BFZ', 'Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('natural connection', 'BFZ').
card_image_name('natural connection'/'BFZ', 'natural connection').
card_uid('natural connection'/'BFZ', 'BFZ:Natural Connection:natural connection').
card_rarity('natural connection'/'BFZ', 'Common').
card_artist('natural connection'/'BFZ', 'Wesley Burt').
card_number('natural connection'/'BFZ', '179').
card_flavor_text('natural connection'/'BFZ', '\"Zendikar will not surrender, and neither can we.\"').
card_multiverse_id('natural connection'/'BFZ', '401966').

card_in_set('nettle drone', 'BFZ').
card_original_type('nettle drone'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('nettle drone'/'BFZ', 'Devoid (This card has no color.){T}: Nettle Drone deals 1 damage to each opponent.Whenever you cast a colorless spell, untap Nettle Drone.').
card_first_print('nettle drone', 'BFZ').
card_image_name('nettle drone'/'BFZ', 'nettle drone').
card_uid('nettle drone'/'BFZ', 'BFZ:Nettle Drone:nettle drone').
card_rarity('nettle drone'/'BFZ', 'Common').
card_artist('nettle drone'/'BFZ', 'Clint Cearley').
card_number('nettle drone'/'BFZ', '131').
card_multiverse_id('nettle drone'/'BFZ', '401967').

card_in_set('nirkana assassin', 'BFZ').
card_original_type('nirkana assassin'/'BFZ', 'Creature — Vampire Assassin Ally').
card_original_text('nirkana assassin'/'BFZ', 'Whenever you gain life, Nirkana Assassin gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('nirkana assassin', 'BFZ').
card_image_name('nirkana assassin'/'BFZ', 'nirkana assassin').
card_uid('nirkana assassin'/'BFZ', 'BFZ:Nirkana Assassin:nirkana assassin').
card_rarity('nirkana assassin'/'BFZ', 'Common').
card_artist('nirkana assassin'/'BFZ', 'Viktor Titov').
card_number('nirkana assassin'/'BFZ', '118').
card_flavor_text('nirkana assassin'/'BFZ', 'Nirkana assassins craft incurable concoctions by mixing basilisk marrow and deathwillow sap with the vital fluids of a dozen other poisonous species.').
card_multiverse_id('nirkana assassin'/'BFZ', '401968').

card_in_set('nissa\'s renewal', 'BFZ').
card_original_type('nissa\'s renewal'/'BFZ', 'Sorcery').
card_original_text('nissa\'s renewal'/'BFZ', 'Search your library for up to three basic land cards, put them onto the battlefield tapped, then shuffle your library. You gain 7 life.').
card_first_print('nissa\'s renewal', 'BFZ').
card_image_name('nissa\'s renewal'/'BFZ', 'nissa\'s renewal').
card_uid('nissa\'s renewal'/'BFZ', 'BFZ:Nissa\'s Renewal:nissa\'s renewal').
card_rarity('nissa\'s renewal'/'BFZ', 'Rare').
card_artist('nissa\'s renewal'/'BFZ', 'Lius Lasahido').
card_number('nissa\'s renewal'/'BFZ', '180').
card_flavor_text('nissa\'s renewal'/'BFZ', 'Her connection to the plane restored, Nissa felt the power of Zendikar surge through her.').
card_multiverse_id('nissa\'s renewal'/'BFZ', '401969').

card_in_set('noyan dar, roil shaper', 'BFZ').
card_original_type('noyan dar, roil shaper'/'BFZ', 'Legendary Creature — Merfolk Ally').
card_original_text('noyan dar, roil shaper'/'BFZ', 'Whenever you cast an instant or sorcery spell, you may put three +1/+1 counters on target land you control. If you do, that land becomes a 0/0 Elemental creature with haste that\'s still a land.').
card_first_print('noyan dar, roil shaper', 'BFZ').
card_image_name('noyan dar, roil shaper'/'BFZ', 'noyan dar, roil shaper').
card_uid('noyan dar, roil shaper'/'BFZ', 'BFZ:Noyan Dar, Roil Shaper:noyan dar, roil shaper').
card_rarity('noyan dar, roil shaper'/'BFZ', 'Rare').
card_artist('noyan dar, roil shaper'/'BFZ', 'Karl Kopinski').
card_number('noyan dar, roil shaper'/'BFZ', '216').
card_flavor_text('noyan dar, roil shaper'/'BFZ', 'After many years as a lullmage, Noyan Dar turned his talents from allaying Zendikar\'s fury to the considerably easier task of rousing it.').
card_multiverse_id('noyan dar, roil shaper'/'BFZ', '401970').

card_in_set('ob nixilis reignited', 'BFZ').
card_original_type('ob nixilis reignited'/'BFZ', 'Planeswalker — Nixilis').
card_original_text('ob nixilis reignited'/'BFZ', '+1: You draw a card and you lose 1 life.−3: Destroy target creature.−8: Target opponent gets an emblem with \"Whenever a player draws a card, you lose 2 life.\"').
card_first_print('ob nixilis reignited', 'BFZ').
card_image_name('ob nixilis reignited'/'BFZ', 'ob nixilis reignited').
card_uid('ob nixilis reignited'/'BFZ', 'BFZ:Ob Nixilis Reignited:ob nixilis reignited').
card_rarity('ob nixilis reignited'/'BFZ', 'Mythic Rare').
card_artist('ob nixilis reignited'/'BFZ', 'Chris Rahn').
card_number('ob nixilis reignited'/'BFZ', '119').
card_multiverse_id('ob nixilis reignited'/'BFZ', '401971').

card_in_set('oblivion sower', 'BFZ').
card_original_type('oblivion sower'/'BFZ', 'Creature — Eldrazi').
card_original_text('oblivion sower'/'BFZ', 'When you cast Oblivion Sower, target opponent exiles the top four cards of his or her library, then you may put any number of land cards that player owns from exile onto the battlefield under your control.').
card_image_name('oblivion sower'/'BFZ', 'oblivion sower').
card_uid('oblivion sower'/'BFZ', 'BFZ:Oblivion Sower:oblivion sower').
card_rarity('oblivion sower'/'BFZ', 'Mythic Rare').
card_artist('oblivion sower'/'BFZ', 'Jaime Jones').
card_number('oblivion sower'/'BFZ', '11').
card_flavor_text('oblivion sower'/'BFZ', 'The Eldrazi hunger without limit and consume without pause.').
card_multiverse_id('oblivion sower'/'BFZ', '401972').

card_in_set('omnath, locus of rage', 'BFZ').
card_original_type('omnath, locus of rage'/'BFZ', 'Legendary Creature — Elemental').
card_original_text('omnath, locus of rage'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, put a 5/5 red and green Elemental creature token onto the battlefield.Whenever Omnath, Locus of Rage or another Elemental you control dies, Omnath deals 3 damage to target creature or player.').
card_first_print('omnath, locus of rage', 'BFZ').
card_image_name('omnath, locus of rage'/'BFZ', 'omnath, locus of rage').
card_uid('omnath, locus of rage'/'BFZ', 'BFZ:Omnath, Locus of Rage:omnath, locus of rage').
card_rarity('omnath, locus of rage'/'BFZ', 'Mythic Rare').
card_artist('omnath, locus of rage'/'BFZ', 'Brad Rigney').
card_number('omnath, locus of rage'/'BFZ', '217').
card_multiverse_id('omnath, locus of rage'/'BFZ', '401973').

card_in_set('ondu champion', 'BFZ').
card_original_type('ondu champion'/'BFZ', 'Creature — Minotaur Warrior Ally').
card_original_text('ondu champion'/'BFZ', 'Rally — Whenever Ondu Champion or another Ally enters the battlefield under your control, creatures you control gain trample until end of turn.').
card_first_print('ondu champion', 'BFZ').
card_image_name('ondu champion'/'BFZ', 'ondu champion').
card_uid('ondu champion'/'BFZ', 'BFZ:Ondu Champion:ondu champion').
card_rarity('ondu champion'/'BFZ', 'Common').
card_artist('ondu champion'/'BFZ', 'Volkan Baga').
card_number('ondu champion'/'BFZ', '149').
card_flavor_text('ondu champion'/'BFZ', '\"Put a minotaur at the head of an army and suddenly all the soldiers act like they have horns.\"—Gideon Jura').
card_multiverse_id('ondu champion'/'BFZ', '401974').

card_in_set('ondu greathorn', 'BFZ').
card_original_type('ondu greathorn'/'BFZ', 'Creature — Beast').
card_original_text('ondu greathorn'/'BFZ', 'First strikeLandfall — Whenever a land enters the battlefield under your control, Ondu Greathorn gets +2/+2 until end of turn.').
card_first_print('ondu greathorn', 'BFZ').
card_image_name('ondu greathorn'/'BFZ', 'ondu greathorn').
card_uid('ondu greathorn'/'BFZ', 'BFZ:Ondu Greathorn:ondu greathorn').
card_rarity('ondu greathorn'/'BFZ', 'Common').
card_artist('ondu greathorn'/'BFZ', 'Aaron Miller').
card_number('ondu greathorn'/'BFZ', '40').
card_flavor_text('ondu greathorn'/'BFZ', '\"May your horns get lodged in an Eldrazi\'s bony face, ornery brute!\"—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('ondu greathorn'/'BFZ', '401975').

card_in_set('ondu rising', 'BFZ').
card_original_type('ondu rising'/'BFZ', 'Sorcery').
card_original_text('ondu rising'/'BFZ', 'Whenever a creature attacks this turn, it gains lifelink until end of turn.Awaken 4—{4}{W} (If you cast this spell for {4}{W}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('ondu rising', 'BFZ').
card_image_name('ondu rising'/'BFZ', 'ondu rising').
card_uid('ondu rising'/'BFZ', 'BFZ:Ondu Rising:ondu rising').
card_rarity('ondu rising'/'BFZ', 'Uncommon').
card_artist('ondu rising'/'BFZ', 'Igor Kieryluk').
card_number('ondu rising'/'BFZ', '41').
card_multiverse_id('ondu rising'/'BFZ', '401976').

card_in_set('oracle of dust', 'BFZ').
card_original_type('oracle of dust'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('oracle of dust'/'BFZ', 'Devoid (This card has no color.){2}, Put a card an opponent owns from exile into that player\'s graveyard: Draw a card, then discard a card.').
card_first_print('oracle of dust', 'BFZ').
card_image_name('oracle of dust'/'BFZ', 'oracle of dust').
card_uid('oracle of dust'/'BFZ', 'BFZ:Oracle of Dust:oracle of dust').
card_rarity('oracle of dust'/'BFZ', 'Common').
card_artist('oracle of dust'/'BFZ', 'Jason Rainville').
card_number('oracle of dust'/'BFZ', '63').
card_flavor_text('oracle of dust'/'BFZ', 'Whatever it is searching for, it will stop at nothing to find it.').
card_multiverse_id('oracle of dust'/'BFZ', '401977').

card_in_set('oran-rief hydra', 'BFZ').
card_original_type('oran-rief hydra'/'BFZ', 'Creature — Hydra').
card_original_text('oran-rief hydra'/'BFZ', 'TrampleLandfall — Whenever a land enters the battlefield under your control, put a +1/+1 counter on Oran-Rief Hydra. If that land is a Forest, put two +1/+1 counters on Oran-Rief Hydra instead.').
card_first_print('oran-rief hydra', 'BFZ').
card_image_name('oran-rief hydra'/'BFZ', 'oran-rief hydra').
card_uid('oran-rief hydra'/'BFZ', 'BFZ:Oran-Rief Hydra:oran-rief hydra').
card_rarity('oran-rief hydra'/'BFZ', 'Rare').
card_artist('oran-rief hydra'/'BFZ', 'Chris Rahn').
card_number('oran-rief hydra'/'BFZ', '181').
card_multiverse_id('oran-rief hydra'/'BFZ', '401978').

card_in_set('oran-rief invoker', 'BFZ').
card_original_type('oran-rief invoker'/'BFZ', 'Creature — Human Shaman').
card_original_text('oran-rief invoker'/'BFZ', '{8}: Oran-Rief Invoker gets +5/+5 and gains trample until end of turn.').
card_first_print('oran-rief invoker', 'BFZ').
card_image_name('oran-rief invoker'/'BFZ', 'oran-rief invoker').
card_uid('oran-rief invoker'/'BFZ', 'BFZ:Oran-Rief Invoker:oran-rief invoker').
card_rarity('oran-rief invoker'/'BFZ', 'Common').
card_artist('oran-rief invoker'/'BFZ', 'Anastasia Ovchinnikova').
card_number('oran-rief invoker'/'BFZ', '182').
card_flavor_text('oran-rief invoker'/'BFZ', '\"The world was not hostile to us—we were beneath its notice, and presented no danger.\"—The Invokers\' Tales').
card_multiverse_id('oran-rief invoker'/'BFZ', '401979').

card_in_set('outnumber', 'BFZ').
card_original_type('outnumber'/'BFZ', 'Instant').
card_original_text('outnumber'/'BFZ', 'Outnumber deals damage to target creature equal to the number of creatures you control.').
card_first_print('outnumber', 'BFZ').
card_image_name('outnumber'/'BFZ', 'outnumber').
card_uid('outnumber'/'BFZ', 'BFZ:Outnumber:outnumber').
card_rarity('outnumber'/'BFZ', 'Common').
card_artist('outnumber'/'BFZ', 'Tyler Jacobson').
card_number('outnumber'/'BFZ', '150').
card_flavor_text('outnumber'/'BFZ', 'Everyone who could still lift a weapon had a part in retaking Sea Gate.').
card_multiverse_id('outnumber'/'BFZ', '401980').

card_in_set('painful truths', 'BFZ').
card_original_type('painful truths'/'BFZ', 'Sorcery').
card_original_text('painful truths'/'BFZ', 'Converge — You draw X cards and you lose X life, where X is the number of colors of mana spent to cast Painful Truths.').
card_first_print('painful truths', 'BFZ').
card_image_name('painful truths'/'BFZ', 'painful truths').
card_uid('painful truths'/'BFZ', 'BFZ:Painful Truths:painful truths').
card_rarity('painful truths'/'BFZ', 'Rare').
card_artist('painful truths'/'BFZ', 'Winona Nelson').
card_number('painful truths'/'BFZ', '120').
card_flavor_text('painful truths'/'BFZ', 'As the Eldrazi spread, no secret that could give Zendikar\'s defenders an advantage was considered too dangerous to seek.').
card_multiverse_id('painful truths'/'BFZ', '401981').

card_in_set('part the waterveil', 'BFZ').
card_original_type('part the waterveil'/'BFZ', 'Sorcery').
card_original_text('part the waterveil'/'BFZ', 'Take an extra turn after this one. Exile Part the Waterveil.Awaken 6—{6}{U}{U}{U} (If you cast this spell for {6}{U}{U}{U}, also put six +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('part the waterveil', 'BFZ').
card_image_name('part the waterveil'/'BFZ', 'part the waterveil').
card_uid('part the waterveil'/'BFZ', 'BFZ:Part the Waterveil:part the waterveil').
card_rarity('part the waterveil'/'BFZ', 'Mythic Rare').
card_artist('part the waterveil'/'BFZ', 'Titus Lunter').
card_number('part the waterveil'/'BFZ', '80').
card_multiverse_id('part the waterveil'/'BFZ', '401982').

card_in_set('pathway arrows', 'BFZ').
card_original_type('pathway arrows'/'BFZ', 'Artifact — Equipment').
card_original_text('pathway arrows'/'BFZ', 'Equipped creature has \"{2}, {T}: This creature deals 1 damage to target creature. If a colorless creature is dealt damage this way, tap it.\"Equip {2}').
card_first_print('pathway arrows', 'BFZ').
card_image_name('pathway arrows'/'BFZ', 'pathway arrows').
card_uid('pathway arrows'/'BFZ', 'BFZ:Pathway Arrows:pathway arrows').
card_rarity('pathway arrows'/'BFZ', 'Uncommon').
card_artist('pathway arrows'/'BFZ', 'Kev Walker').
card_number('pathway arrows'/'BFZ', '225').
card_flavor_text('pathway arrows'/'BFZ', 'Also called \"pathway stones,\" hedron shards can immobilize Eldrazi on contact.').
card_multiverse_id('pathway arrows'/'BFZ', '401983').

card_in_set('pilgrim\'s eye', 'BFZ').
card_original_type('pilgrim\'s eye'/'BFZ', 'Artifact Creature — Thopter').
card_original_text('pilgrim\'s eye'/'BFZ', 'FlyingWhen Pilgrim\'s Eye enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('pilgrim\'s eye'/'BFZ', 'pilgrim\'s eye').
card_uid('pilgrim\'s eye'/'BFZ', 'BFZ:Pilgrim\'s Eye:pilgrim\'s eye').
card_rarity('pilgrim\'s eye'/'BFZ', 'Uncommon').
card_artist('pilgrim\'s eye'/'BFZ', 'Dan Scott').
card_number('pilgrim\'s eye'/'BFZ', '226').
card_flavor_text('pilgrim\'s eye'/'BFZ', 'Each time it is sent to find a safe haven, it takes longer to return.').
card_multiverse_id('pilgrim\'s eye'/'BFZ', '401984').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains1').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains1').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Noah Bradley').
card_number('plains'/'BFZ', '250').
card_multiverse_id('plains'/'BFZ', '401985').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains10').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains10').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Tianhua X').
card_number('plains'/'BFZ', '254').
card_multiverse_id('plains'/'BFZ', '401990').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains2').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains2').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Noah Bradley').
card_number('plains'/'BFZ', '250').
card_multiverse_id('plains'/'BFZ', '401992').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains3').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains3').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Sam Burley').
card_number('plains'/'BFZ', '251').
card_multiverse_id('plains'/'BFZ', '401988').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains4').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains4').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Sam Burley').
card_number('plains'/'BFZ', '251').
card_multiverse_id('plains'/'BFZ', '401993').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains5').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains5').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Adam Paquette').
card_number('plains'/'BFZ', '252').
card_multiverse_id('plains'/'BFZ', '401989').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains6').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains6').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Adam Paquette').
card_number('plains'/'BFZ', '252').
card_multiverse_id('plains'/'BFZ', '401991').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains7').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains7').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Vincent Proce').
card_number('plains'/'BFZ', '253').
card_multiverse_id('plains'/'BFZ', '401986').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains8').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains8').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Vincent Proce').
card_number('plains'/'BFZ', '253').
card_multiverse_id('plains'/'BFZ', '401994').

card_in_set('plains', 'BFZ').
card_original_type('plains'/'BFZ', 'Basic Land — Plains').
card_original_text('plains'/'BFZ', 'W').
card_image_name('plains'/'BFZ', 'plains9').
card_uid('plains'/'BFZ', 'BFZ:Plains:plains9').
card_rarity('plains'/'BFZ', 'Basic Land').
card_artist('plains'/'BFZ', 'Tianhua X').
card_number('plains'/'BFZ', '254').
card_multiverse_id('plains'/'BFZ', '401987').

card_in_set('planar outburst', 'BFZ').
card_original_type('planar outburst'/'BFZ', 'Sorcery').
card_original_text('planar outburst'/'BFZ', 'Destroy all nonland creatures.Awaken 4—{5}{W}{W}{W} (If you cast this spell for {5}{W}{W}{W}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('planar outburst', 'BFZ').
card_image_name('planar outburst'/'BFZ', 'planar outburst').
card_uid('planar outburst'/'BFZ', 'BFZ:Planar Outburst:planar outburst').
card_rarity('planar outburst'/'BFZ', 'Rare').
card_artist('planar outburst'/'BFZ', 'Vincent Proce').
card_number('planar outburst'/'BFZ', '42').
card_multiverse_id('planar outburst'/'BFZ', '401995').

card_in_set('plated crusher', 'BFZ').
card_original_type('plated crusher'/'BFZ', 'Creature — Beast').
card_original_text('plated crusher'/'BFZ', 'Trample, hexproof').
card_first_print('plated crusher', 'BFZ').
card_image_name('plated crusher'/'BFZ', 'plated crusher').
card_uid('plated crusher'/'BFZ', 'BFZ:Plated Crusher:plated crusher').
card_rarity('plated crusher'/'BFZ', 'Uncommon').
card_artist('plated crusher'/'BFZ', 'Jama Jurabaev').
card_number('plated crusher'/'BFZ', '183').
card_flavor_text('plated crusher'/'BFZ', '\"It might fight the Eldrazi, but don\'t mistake it for a companion, Gideon. It\'s not interested in your concept of strength through unity.\"—Najiya, leader of the Tajuru').
card_multiverse_id('plated crusher'/'BFZ', '401996').

card_in_set('plummet', 'BFZ').
card_original_type('plummet'/'BFZ', 'Instant').
card_original_text('plummet'/'BFZ', 'Destroy target creature with flying.').
card_image_name('plummet'/'BFZ', 'plummet').
card_uid('plummet'/'BFZ', 'BFZ:Plummet:plummet').
card_rarity('plummet'/'BFZ', 'Common').
card_artist('plummet'/'BFZ', 'Aaron Miller').
card_number('plummet'/'BFZ', '184').
card_flavor_text('plummet'/'BFZ', 'The elf\'s blade carved deep into the Eldrazi drone\'s fleshy lattices. Without a sound, the drone lost equilibrium and tumbled from the sky.').
card_multiverse_id('plummet'/'BFZ', '401997').

card_in_set('prairie stream', 'BFZ').
card_original_type('prairie stream'/'BFZ', 'Land — Plains Island').
card_original_text('prairie stream'/'BFZ', '({T}: Add {W} or {U} to your mana pool.)Prairie Stream enters the battlefield tapped unless you control two or more basic lands.').
card_first_print('prairie stream', 'BFZ').
card_image_name('prairie stream'/'BFZ', 'prairie stream').
card_uid('prairie stream'/'BFZ', 'BFZ:Prairie Stream:prairie stream').
card_rarity('prairie stream'/'BFZ', 'Rare').
card_artist('prairie stream'/'BFZ', 'Adam Paquette').
card_number('prairie stream'/'BFZ', '241').
card_flavor_text('prairie stream'/'BFZ', 'The continent of Ondu is a vast plateau crisscrossed by deep trenches and meandering rivers.').
card_multiverse_id('prairie stream'/'BFZ', '401998').

card_in_set('prism array', 'BFZ').
card_original_type('prism array'/'BFZ', 'Enchantment').
card_original_text('prism array'/'BFZ', 'Converge — Prism Array enters the battlefield with a crystal counter on it for each color of mana spent to cast it.Remove a crystal counter from Prism Array: Tap target creature.{W}{U}{B}{R}{G}: Scry 3. (Look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('prism array', 'BFZ').
card_image_name('prism array'/'BFZ', 'prism array').
card_uid('prism array'/'BFZ', 'BFZ:Prism Array:prism array').
card_rarity('prism array'/'BFZ', 'Rare').
card_artist('prism array'/'BFZ', 'Philip Straub').
card_number('prism array'/'BFZ', '81').
card_multiverse_id('prism array'/'BFZ', '401999').

card_in_set('processor assault', 'BFZ').
card_original_type('processor assault'/'BFZ', 'Sorcery').
card_original_text('processor assault'/'BFZ', 'Devoid (This card has no color.)As an additional cost to cast Processor Assault, put a card an opponent owns from exile into that player\'s graveyard.Processor Assault deals 5 damage to target creature.').
card_first_print('processor assault', 'BFZ').
card_image_name('processor assault'/'BFZ', 'processor assault').
card_uid('processor assault'/'BFZ', 'BFZ:Processor Assault:processor assault').
card_rarity('processor assault'/'BFZ', 'Uncommon').
card_artist('processor assault'/'BFZ', 'Jama Jurabaev').
card_number('processor assault'/'BFZ', '132').
card_flavor_text('processor assault'/'BFZ', 'Ulamog\'s processors trail behind him, converting ruined matter into furious energy.').
card_multiverse_id('processor assault'/'BFZ', '402000').

card_in_set('quarantine field', 'BFZ').
card_original_type('quarantine field'/'BFZ', 'Enchantment').
card_original_text('quarantine field'/'BFZ', 'Quarantine Field enters the battlefield with X isolation counters on it.When Quarantine Field enters the battlefield, for each isolation counter on it, exile up to one target nonland permanent an opponent controls until Quarantine Field leaves the battlefield.').
card_first_print('quarantine field', 'BFZ').
card_image_name('quarantine field'/'BFZ', 'quarantine field').
card_uid('quarantine field'/'BFZ', 'BFZ:Quarantine Field:quarantine field').
card_rarity('quarantine field'/'BFZ', 'Mythic Rare').
card_artist('quarantine field'/'BFZ', 'Daarken').
card_number('quarantine field'/'BFZ', '43').
card_multiverse_id('quarantine field'/'BFZ', '402001').

card_in_set('radiant flames', 'BFZ').
card_original_type('radiant flames'/'BFZ', 'Sorcery').
card_original_text('radiant flames'/'BFZ', 'Converge — Radiant Flames deals X damage to each creature, where X is the number of colors of mana spent to cast Radiant Flames.').
card_first_print('radiant flames', 'BFZ').
card_image_name('radiant flames'/'BFZ', 'radiant flames').
card_uid('radiant flames'/'BFZ', 'BFZ:Radiant Flames:radiant flames').
card_rarity('radiant flames'/'BFZ', 'Rare').
card_artist('radiant flames'/'BFZ', 'Slawomir Maniak').
card_number('radiant flames'/'BFZ', '151').
card_flavor_text('radiant flames'/'BFZ', 'The fires of Zendikar are always hungry, and there is much on which they can feed.').
card_multiverse_id('radiant flames'/'BFZ', '402002').

card_in_set('reckless cohort', 'BFZ').
card_original_type('reckless cohort'/'BFZ', 'Creature — Human Warrior Ally').
card_original_text('reckless cohort'/'BFZ', 'Reckless Cohort attacks each combat if able unless you control another Ally.').
card_first_print('reckless cohort', 'BFZ').
card_image_name('reckless cohort'/'BFZ', 'reckless cohort').
card_uid('reckless cohort'/'BFZ', 'BFZ:Reckless Cohort:reckless cohort').
card_rarity('reckless cohort'/'BFZ', 'Common').
card_artist('reckless cohort'/'BFZ', 'Volkan Baga').
card_number('reckless cohort'/'BFZ', '152').
card_flavor_text('reckless cohort'/'BFZ', '\"You have a family. Mine died at Sea Gate. You go to yours, and I\'ll go to mine.\"').
card_multiverse_id('reckless cohort'/'BFZ', '402003').

card_in_set('reclaiming vines', 'BFZ').
card_original_type('reclaiming vines'/'BFZ', 'Sorcery').
card_original_text('reclaiming vines'/'BFZ', 'Destroy target artifact, enchantment, or land.').
card_first_print('reclaiming vines', 'BFZ').
card_image_name('reclaiming vines'/'BFZ', 'reclaiming vines').
card_uid('reclaiming vines'/'BFZ', 'BFZ:Reclaiming Vines:reclaiming vines').
card_rarity('reclaiming vines'/'BFZ', 'Common').
card_artist('reclaiming vines'/'BFZ', 'Bastien L. Deharme').
card_number('reclaiming vines'/'BFZ', '185').
card_flavor_text('reclaiming vines'/'BFZ', '\"This world tires of its long occupation.\"—Nissa Revane').
card_multiverse_id('reclaiming vines'/'BFZ', '402004').

card_in_set('resolute blademaster', 'BFZ').
card_original_type('resolute blademaster'/'BFZ', 'Creature — Human Soldier Ally').
card_original_text('resolute blademaster'/'BFZ', 'Rally — Whenever Resolute Blademaster or another Ally enters the battlefield under your control, creatures you control gain double strike until end of turn.').
card_first_print('resolute blademaster', 'BFZ').
card_image_name('resolute blademaster'/'BFZ', 'resolute blademaster').
card_uid('resolute blademaster'/'BFZ', 'BFZ:Resolute Blademaster:resolute blademaster').
card_rarity('resolute blademaster'/'BFZ', 'Uncommon').
card_artist('resolute blademaster'/'BFZ', 'Joseph Meehan').
card_number('resolute blademaster'/'BFZ', '218').
card_flavor_text('resolute blademaster'/'BFZ', '\"Great steel is born in the hottest forges. Great soldiers are born in war.\"').
card_multiverse_id('resolute blademaster'/'BFZ', '402005').

card_in_set('retreat to coralhelm', 'BFZ').
card_original_type('retreat to coralhelm'/'BFZ', 'Enchantment').
card_original_text('retreat to coralhelm'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, choose one —• You may tap or untap target creature.• Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('retreat to coralhelm', 'BFZ').
card_image_name('retreat to coralhelm'/'BFZ', 'retreat to coralhelm').
card_uid('retreat to coralhelm'/'BFZ', 'BFZ:Retreat to Coralhelm:retreat to coralhelm').
card_rarity('retreat to coralhelm'/'BFZ', 'Uncommon').
card_artist('retreat to coralhelm'/'BFZ', 'Kieran Yanner').
card_number('retreat to coralhelm'/'BFZ', '82').
card_multiverse_id('retreat to coralhelm'/'BFZ', '402006').

card_in_set('retreat to emeria', 'BFZ').
card_original_type('retreat to emeria'/'BFZ', 'Enchantment').
card_original_text('retreat to emeria'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, choose one —• Put a 1/1 white Kor Ally creature token onto the battlefield.• Creatures you control get +1/+1 until end of turn.').
card_first_print('retreat to emeria', 'BFZ').
card_image_name('retreat to emeria'/'BFZ', 'retreat to emeria').
card_uid('retreat to emeria'/'BFZ', 'BFZ:Retreat to Emeria:retreat to emeria').
card_rarity('retreat to emeria'/'BFZ', 'Uncommon').
card_artist('retreat to emeria'/'BFZ', 'Kieran Yanner').
card_number('retreat to emeria'/'BFZ', '44').
card_multiverse_id('retreat to emeria'/'BFZ', '402007').

card_in_set('retreat to hagra', 'BFZ').
card_original_type('retreat to hagra'/'BFZ', 'Enchantment').
card_original_text('retreat to hagra'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, choose one —• Target creature gets +1/+0 and gains deathtouch until end of turn.• Each opponent loses 1 life and you gain 1 life.').
card_first_print('retreat to hagra', 'BFZ').
card_image_name('retreat to hagra'/'BFZ', 'retreat to hagra').
card_uid('retreat to hagra'/'BFZ', 'BFZ:Retreat to Hagra:retreat to hagra').
card_rarity('retreat to hagra'/'BFZ', 'Uncommon').
card_artist('retreat to hagra'/'BFZ', 'Kieran Yanner').
card_number('retreat to hagra'/'BFZ', '121').
card_multiverse_id('retreat to hagra'/'BFZ', '402008').

card_in_set('retreat to kazandu', 'BFZ').
card_original_type('retreat to kazandu'/'BFZ', 'Enchantment').
card_original_text('retreat to kazandu'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, choose one —• Put a +1/+1 counter on target creature.• You gain 2 life.').
card_image_name('retreat to kazandu'/'BFZ', 'retreat to kazandu').
card_uid('retreat to kazandu'/'BFZ', 'BFZ:Retreat to Kazandu:retreat to kazandu').
card_rarity('retreat to kazandu'/'BFZ', 'Uncommon').
card_artist('retreat to kazandu'/'BFZ', 'Kieran Yanner').
card_number('retreat to kazandu'/'BFZ', '186').
card_multiverse_id('retreat to kazandu'/'BFZ', '402009').

card_in_set('retreat to valakut', 'BFZ').
card_original_type('retreat to valakut'/'BFZ', 'Enchantment').
card_original_text('retreat to valakut'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, choose one —• Target creature gets +2/+0 until end of turn.• Target creature can\'t block this turn.').
card_first_print('retreat to valakut', 'BFZ').
card_image_name('retreat to valakut'/'BFZ', 'retreat to valakut').
card_uid('retreat to valakut'/'BFZ', 'BFZ:Retreat to Valakut:retreat to valakut').
card_rarity('retreat to valakut'/'BFZ', 'Uncommon').
card_artist('retreat to valakut'/'BFZ', 'Kieran Yanner').
card_number('retreat to valakut'/'BFZ', '153').
card_multiverse_id('retreat to valakut'/'BFZ', '402010').

card_in_set('rising miasma', 'BFZ').
card_original_type('rising miasma'/'BFZ', 'Sorcery').
card_original_text('rising miasma'/'BFZ', 'All creatures get -2/-2 until end of turn.Awaken 3—{5}{B}{B} (If you cast this spell for {5}{B}{B}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('rising miasma', 'BFZ').
card_image_name('rising miasma'/'BFZ', 'rising miasma').
card_uid('rising miasma'/'BFZ', 'BFZ:Rising Miasma:rising miasma').
card_rarity('rising miasma'/'BFZ', 'Uncommon').
card_artist('rising miasma'/'BFZ', 'Deruchenko Alexander').
card_number('rising miasma'/'BFZ', '122').
card_multiverse_id('rising miasma'/'BFZ', '402011').

card_in_set('roil spout', 'BFZ').
card_original_type('roil spout'/'BFZ', 'Sorcery').
card_original_text('roil spout'/'BFZ', 'Put target creature on top of its owner\'s library.Awaken 4—{4}{W}{U} (If you cast this spell for {4}{W}{U}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('roil spout', 'BFZ').
card_image_name('roil spout'/'BFZ', 'roil spout').
card_uid('roil spout'/'BFZ', 'BFZ:Roil Spout:roil spout').
card_rarity('roil spout'/'BFZ', 'Uncommon').
card_artist('roil spout'/'BFZ', 'Igor Kieryluk').
card_number('roil spout'/'BFZ', '219').
card_multiverse_id('roil spout'/'BFZ', '402012').

card_in_set('roil\'s retribution', 'BFZ').
card_original_type('roil\'s retribution'/'BFZ', 'Instant').
card_original_text('roil\'s retribution'/'BFZ', 'Roil\'s Retribution deals 5 damage divided as you choose among any number of target attacking or blocking creatures.').
card_first_print('roil\'s retribution', 'BFZ').
card_image_name('roil\'s retribution'/'BFZ', 'roil\'s retribution').
card_uid('roil\'s retribution'/'BFZ', 'BFZ:Roil\'s Retribution:roil\'s retribution').
card_rarity('roil\'s retribution'/'BFZ', 'Uncommon').
card_artist('roil\'s retribution'/'BFZ', 'Raymond Swanland').
card_number('roil\'s retribution'/'BFZ', '45').
card_flavor_text('roil\'s retribution'/'BFZ', '\"We can use their hunger to lead them into danger. If we\'re already prey, we may as well be bait!\"—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('roil\'s retribution'/'BFZ', '402013').

card_in_set('roilmage\'s trick', 'BFZ').
card_original_type('roilmage\'s trick'/'BFZ', 'Instant').
card_original_text('roilmage\'s trick'/'BFZ', 'Converge — Creatures your opponents control get -X/-0 until end of turn, where X is the number of colors of mana spent to cast Roilmage\'s Trick.Draw a card.').
card_first_print('roilmage\'s trick', 'BFZ').
card_image_name('roilmage\'s trick'/'BFZ', 'roilmage\'s trick').
card_uid('roilmage\'s trick'/'BFZ', 'BFZ:Roilmage\'s Trick:roilmage\'s trick').
card_rarity('roilmage\'s trick'/'BFZ', 'Common').
card_artist('roilmage\'s trick'/'BFZ', 'Johann Bodin').
card_number('roilmage\'s trick'/'BFZ', '83').
card_flavor_text('roilmage\'s trick'/'BFZ', 'Weather on Zendikar is unpredictable—unless you\'re the one telling it what to do.').
card_multiverse_id('roilmage\'s trick'/'BFZ', '402014').

card_in_set('rolling thunder', 'BFZ').
card_original_type('rolling thunder'/'BFZ', 'Sorcery').
card_original_text('rolling thunder'/'BFZ', 'Rolling Thunder deals X damage divided as you choose among any number of target creatures and/or players.').
card_image_name('rolling thunder'/'BFZ', 'rolling thunder').
card_uid('rolling thunder'/'BFZ', 'BFZ:Rolling Thunder:rolling thunder').
card_rarity('rolling thunder'/'BFZ', 'Uncommon').
card_artist('rolling thunder'/'BFZ', 'Yohann Schepacz').
card_number('rolling thunder'/'BFZ', '154').
card_flavor_text('rolling thunder'/'BFZ', '\"Zendikar is always angry, though I\'ve never seen that anger so precisely directed.\"—Nablus, North Hada trapper').
card_multiverse_id('rolling thunder'/'BFZ', '402015').

card_in_set('rot shambler', 'BFZ').
card_original_type('rot shambler'/'BFZ', 'Creature — Fungus').
card_original_text('rot shambler'/'BFZ', 'Whenever another creature you control dies, put a +1/+1 counter on Rot Shambler.').
card_first_print('rot shambler', 'BFZ').
card_image_name('rot shambler'/'BFZ', 'rot shambler').
card_uid('rot shambler'/'BFZ', 'BFZ:Rot Shambler:rot shambler').
card_rarity('rot shambler'/'BFZ', 'Uncommon').
card_artist('rot shambler'/'BFZ', 'Yeong-Hao Han').
card_number('rot shambler'/'BFZ', '187').
card_flavor_text('rot shambler'/'BFZ', '\"If I die, feed me to a shambler. At least that way I\'ll be doing some good.\"—Ryza, Oran-Rief scout').
card_multiverse_id('rot shambler'/'BFZ', '402016').

card_in_set('ruin processor', 'BFZ').
card_original_type('ruin processor'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('ruin processor'/'BFZ', 'When you cast Ruin Processor, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, you gain 5 life.').
card_first_print('ruin processor', 'BFZ').
card_image_name('ruin processor'/'BFZ', 'ruin processor').
card_uid('ruin processor'/'BFZ', 'BFZ:Ruin Processor:ruin processor').
card_rarity('ruin processor'/'BFZ', 'Common').
card_artist('ruin processor'/'BFZ', 'Slawomir Maniak').
card_number('ruin processor'/'BFZ', '12').
card_flavor_text('ruin processor'/'BFZ', 'The Eldrazi continue to sweep through the wastes, compounding destruction that already seemed absolute.').
card_multiverse_id('ruin processor'/'BFZ', '402017').

card_in_set('ruination guide', 'BFZ').
card_original_type('ruination guide'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('ruination guide'/'BFZ', 'Devoid (This card has no color.)Ingest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)Other colorless creatures you control get +1/+0.').
card_first_print('ruination guide', 'BFZ').
card_image_name('ruination guide'/'BFZ', 'ruination guide').
card_uid('ruination guide'/'BFZ', 'BFZ:Ruination Guide:ruination guide').
card_rarity('ruination guide'/'BFZ', 'Uncommon').
card_artist('ruination guide'/'BFZ', 'Mathias Kollros').
card_number('ruination guide'/'BFZ', '64').
card_multiverse_id('ruination guide'/'BFZ', '402018').

card_in_set('ruinous path', 'BFZ').
card_original_type('ruinous path'/'BFZ', 'Sorcery').
card_original_text('ruinous path'/'BFZ', 'Destroy target creature or planeswalker.Awaken 4—{5}{B}{B} (If you cast this spell for {5}{B}{B}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('ruinous path', 'BFZ').
card_image_name('ruinous path'/'BFZ', 'ruinous path').
card_uid('ruinous path'/'BFZ', 'BFZ:Ruinous Path:ruinous path').
card_rarity('ruinous path'/'BFZ', 'Rare').
card_artist('ruinous path'/'BFZ', 'Jaime Jones').
card_number('ruinous path'/'BFZ', '123').
card_multiverse_id('ruinous path'/'BFZ', '402019').

card_in_set('rush of ice', 'BFZ').
card_original_type('rush of ice'/'BFZ', 'Sorcery').
card_original_text('rush of ice'/'BFZ', 'Tap target creature. It doesn\'t untap during its controller\'s next untap step.Awaken 3—{4}{U} (If you cast this spell for {4}{U}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('rush of ice', 'BFZ').
card_image_name('rush of ice'/'BFZ', 'rush of ice').
card_uid('rush of ice'/'BFZ', 'BFZ:Rush of Ice:rush of ice').
card_rarity('rush of ice'/'BFZ', 'Common').
card_artist('rush of ice'/'BFZ', 'Deruchenko Alexander').
card_number('rush of ice'/'BFZ', '84').
card_multiverse_id('rush of ice'/'BFZ', '402020').

card_in_set('salvage drone', 'BFZ').
card_original_type('salvage drone'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('salvage drone'/'BFZ', 'Devoid (This card has no color.)Ingest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)When Salvage Drone dies, you may draw a card. If you do, discard a card.').
card_first_print('salvage drone', 'BFZ').
card_image_name('salvage drone'/'BFZ', 'salvage drone').
card_uid('salvage drone'/'BFZ', 'BFZ:Salvage Drone:salvage drone').
card_rarity('salvage drone'/'BFZ', 'Common').
card_artist('salvage drone'/'BFZ', 'Slawomir Maniak').
card_number('salvage drone'/'BFZ', '65').
card_multiverse_id('salvage drone'/'BFZ', '402021').

card_in_set('sanctum of ugin', 'BFZ').
card_original_type('sanctum of ugin'/'BFZ', 'Land').
card_original_text('sanctum of ugin'/'BFZ', '{T}: Add {1} to your mana pool.Whenever you cast a colorless spell with converted mana cost 7 or greater, you may sacrifice Sanctum of Ugin. If you do, search your library for a colorless creature card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('sanctum of ugin', 'BFZ').
card_image_name('sanctum of ugin'/'BFZ', 'sanctum of ugin').
card_uid('sanctum of ugin'/'BFZ', 'BFZ:Sanctum of Ugin:sanctum of ugin').
card_rarity('sanctum of ugin'/'BFZ', 'Rare').
card_artist('sanctum of ugin'/'BFZ', 'James Paick').
card_number('sanctum of ugin'/'BFZ', '242').
card_multiverse_id('sanctum of ugin'/'BFZ', '402022').

card_in_set('sandstone bridge', 'BFZ').
card_original_type('sandstone bridge'/'BFZ', 'Land').
card_original_text('sandstone bridge'/'BFZ', 'Sandstone Bridge enters the battlefield tapped.When Sandstone Bridge enters the battlefield, target creature gets +1/+1 and gains vigilance until end of turn.{T}: Add {W} to your mana pool.').
card_first_print('sandstone bridge', 'BFZ').
card_image_name('sandstone bridge'/'BFZ', 'sandstone bridge').
card_uid('sandstone bridge'/'BFZ', 'BFZ:Sandstone Bridge:sandstone bridge').
card_rarity('sandstone bridge'/'BFZ', 'Common').
card_artist('sandstone bridge'/'BFZ', 'Cliff Childs').
card_number('sandstone bridge'/'BFZ', '243').
card_multiverse_id('sandstone bridge'/'BFZ', '402023').

card_in_set('scatter to the winds', 'BFZ').
card_original_type('scatter to the winds'/'BFZ', 'Instant').
card_original_text('scatter to the winds'/'BFZ', 'Counter target spell.Awaken 3—{4}{U}{U} (If you cast this spell for {4}{U}{U}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('scatter to the winds', 'BFZ').
card_image_name('scatter to the winds'/'BFZ', 'scatter to the winds').
card_uid('scatter to the winds'/'BFZ', 'BFZ:Scatter to the Winds:scatter to the winds').
card_rarity('scatter to the winds'/'BFZ', 'Rare').
card_artist('scatter to the winds'/'BFZ', 'Raymond Swanland').
card_number('scatter to the winds'/'BFZ', '85').
card_multiverse_id('scatter to the winds'/'BFZ', '402024').

card_in_set('scour from existence', 'BFZ').
card_original_type('scour from existence'/'BFZ', 'Instant').
card_original_text('scour from existence'/'BFZ', 'Exile target permanent.').
card_first_print('scour from existence', 'BFZ').
card_image_name('scour from existence'/'BFZ', 'scour from existence').
card_uid('scour from existence'/'BFZ', 'BFZ:Scour from Existence:scour from existence').
card_rarity('scour from existence'/'BFZ', 'Common').
card_artist('scour from existence'/'BFZ', 'Clint Cearley').
card_number('scour from existence'/'BFZ', '13').
card_flavor_text('scour from existence'/'BFZ', '\"Our people and our very lands disappear as if they never were. We no longer fight for glory, or honor. We battle now for the right to exist.\"—General Tazri, allied commander').
card_multiverse_id('scour from existence'/'BFZ', '402025').

card_in_set('scythe leopard', 'BFZ').
card_original_type('scythe leopard'/'BFZ', 'Creature — Cat').
card_original_text('scythe leopard'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, Scythe Leopard gets +1/+1 until end of turn.').
card_first_print('scythe leopard', 'BFZ').
card_image_name('scythe leopard'/'BFZ', 'scythe leopard').
card_uid('scythe leopard'/'BFZ', 'BFZ:Scythe Leopard:scythe leopard').
card_rarity('scythe leopard'/'BFZ', 'Uncommon').
card_artist('scythe leopard'/'BFZ', 'Daniel Ljunggren').
card_number('scythe leopard'/'BFZ', '188').
card_flavor_text('scythe leopard'/'BFZ', 'Eldrazi are not the leopard\'s preferred prey, but they are better than no prey at all.').
card_multiverse_id('scythe leopard'/'BFZ', '402026').

card_in_set('seek the wilds', 'BFZ').
card_original_type('seek the wilds'/'BFZ', 'Sorcery').
card_original_text('seek the wilds'/'BFZ', 'Look at the top four cards of your library. You may reveal a creature or land card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('seek the wilds', 'BFZ').
card_image_name('seek the wilds'/'BFZ', 'seek the wilds').
card_uid('seek the wilds'/'BFZ', 'BFZ:Seek the Wilds:seek the wilds').
card_rarity('seek the wilds'/'BFZ', 'Common').
card_artist('seek the wilds'/'BFZ', 'Anna Steinbauer').
card_number('seek the wilds'/'BFZ', '189').
card_flavor_text('seek the wilds'/'BFZ', '\"In my downtime I used to paint the vistas. Now the vistas disappear faster than my downtime.\"').
card_multiverse_id('seek the wilds'/'BFZ', '402027').

card_in_set('serene steward', 'BFZ').
card_original_type('serene steward'/'BFZ', 'Creature — Human Cleric Ally').
card_original_text('serene steward'/'BFZ', 'Whenever you gain life, you may pay {W}. If you do, put a +1/+1 counter on target creature.').
card_first_print('serene steward', 'BFZ').
card_image_name('serene steward'/'BFZ', 'serene steward').
card_uid('serene steward'/'BFZ', 'BFZ:Serene Steward:serene steward').
card_rarity('serene steward'/'BFZ', 'Uncommon').
card_artist('serene steward'/'BFZ', 'Magali Villeneuve').
card_number('serene steward'/'BFZ', '46').
card_flavor_text('serene steward'/'BFZ', '\"I can give you strength, but you\'ll have to bring your own courage.\"').
card_multiverse_id('serene steward'/'BFZ', '402028').

card_in_set('serpentine spike', 'BFZ').
card_original_type('serpentine spike'/'BFZ', 'Sorcery').
card_original_text('serpentine spike'/'BFZ', 'Devoid (This card has no color.)Serpentine Spike deals 2 damage to target creature, 3 damage to another target creature, and 4 damage to a third target creature. If a creature dealt damage this way would die this turn, exile it instead.').
card_first_print('serpentine spike', 'BFZ').
card_image_name('serpentine spike'/'BFZ', 'serpentine spike').
card_uid('serpentine spike'/'BFZ', 'BFZ:Serpentine Spike:serpentine spike').
card_rarity('serpentine spike'/'BFZ', 'Rare').
card_artist('serpentine spike'/'BFZ', 'Jaime Jones').
card_number('serpentine spike'/'BFZ', '133').
card_multiverse_id('serpentine spike'/'BFZ', '402029').

card_in_set('shadow glider', 'BFZ').
card_original_type('shadow glider'/'BFZ', 'Creature — Kor Soldier').
card_original_text('shadow glider'/'BFZ', 'Flying').
card_first_print('shadow glider', 'BFZ').
card_image_name('shadow glider'/'BFZ', 'shadow glider').
card_uid('shadow glider'/'BFZ', 'BFZ:Shadow Glider:shadow glider').
card_rarity('shadow glider'/'BFZ', 'Common').
card_artist('shadow glider'/'BFZ', 'Steve Prescott').
card_number('shadow glider'/'BFZ', '47').
card_flavor_text('shadow glider'/'BFZ', 'A few bands of kor sought refuge from the Eldrazi in Zendikar\'s vast cave networks, relying on their ability to survive in harsh vertical landscapes.').
card_multiverse_id('shadow glider'/'BFZ', '402030').

card_in_set('shambling vent', 'BFZ').
card_original_type('shambling vent'/'BFZ', 'Land').
card_original_text('shambling vent'/'BFZ', 'Shambling Vent enters the battlefield tapped.{T}: Add {W} or {B} to your mana pool.{1}{W}{B}: Shambling Vent becomes a 2/3 white and black Elemental creature with lifelink until end of turn. It\'s still a land.').
card_first_print('shambling vent', 'BFZ').
card_image_name('shambling vent'/'BFZ', 'shambling vent').
card_uid('shambling vent'/'BFZ', 'BFZ:Shambling Vent:shambling vent').
card_rarity('shambling vent'/'BFZ', 'Rare').
card_artist('shambling vent'/'BFZ', 'Jung Park').
card_number('shambling vent'/'BFZ', '244').
card_multiverse_id('shambling vent'/'BFZ', '402031').

card_in_set('shatterskull recruit', 'BFZ').
card_original_type('shatterskull recruit'/'BFZ', 'Creature — Giant Warrior Ally').
card_original_text('shatterskull recruit'/'BFZ', 'Menace (This creature can\'t be blocked except by two or more creatures.)').
card_first_print('shatterskull recruit', 'BFZ').
card_image_name('shatterskull recruit'/'BFZ', 'shatterskull recruit').
card_uid('shatterskull recruit'/'BFZ', 'BFZ:Shatterskull Recruit:shatterskull recruit').
card_rarity('shatterskull recruit'/'BFZ', 'Common').
card_artist('shatterskull recruit'/'BFZ', 'David Palumbo').
card_number('shatterskull recruit'/'BFZ', '155').
card_flavor_text('shatterskull recruit'/'BFZ', 'Saved from certain death by the kor, he considers himself bound to them by an unbreakable blood oath.').
card_multiverse_id('shatterskull recruit'/'BFZ', '402032').

card_in_set('sheer drop', 'BFZ').
card_original_type('sheer drop'/'BFZ', 'Sorcery').
card_original_text('sheer drop'/'BFZ', 'Destroy target tapped creature.Awaken 3—{5}{W} (If you cast this spell for {5}{W}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_image_name('sheer drop'/'BFZ', 'sheer drop').
card_uid('sheer drop'/'BFZ', 'BFZ:Sheer Drop:sheer drop').
card_rarity('sheer drop'/'BFZ', 'Common').
card_artist('sheer drop'/'BFZ', 'Clint Cearley').
card_number('sheer drop'/'BFZ', '48').
card_multiverse_id('sheer drop'/'BFZ', '402033').

card_in_set('shrine of the forsaken gods', 'BFZ').
card_original_type('shrine of the forsaken gods'/'BFZ', 'Land').
card_original_text('shrine of the forsaken gods'/'BFZ', '{T}: Add {1} to your mana pool.{T}: Add {2} to your mana pool. Spend this mana only to cast colorless spells. Activate this ability only if you control seven or more lands.').
card_first_print('shrine of the forsaken gods', 'BFZ').
card_image_name('shrine of the forsaken gods'/'BFZ', 'shrine of the forsaken gods').
card_uid('shrine of the forsaken gods'/'BFZ', 'BFZ:Shrine of the Forsaken Gods:shrine of the forsaken gods').
card_rarity('shrine of the forsaken gods'/'BFZ', 'Rare').
card_artist('shrine of the forsaken gods'/'BFZ', 'Daniel Ljunggren').
card_number('shrine of the forsaken gods'/'BFZ', '245').
card_flavor_text('shrine of the forsaken gods'/'BFZ', 'The gods known to the merfolk as Ula, Emeria, and Cosi were nothing more than false memories of a monstrous trinity: the Eldrazi titans Ulamog, Emrakul, and Kozilek.').
card_multiverse_id('shrine of the forsaken gods'/'BFZ', '402034').

card_in_set('silent skimmer', 'BFZ').
card_original_type('silent skimmer'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('silent skimmer'/'BFZ', 'Devoid (This card has no color.)FlyingWhenever Silent Skimmer attacks, defending player loses 2 life.').
card_first_print('silent skimmer', 'BFZ').
card_image_name('silent skimmer'/'BFZ', 'silent skimmer').
card_uid('silent skimmer'/'BFZ', 'BFZ:Silent Skimmer:silent skimmer').
card_rarity('silent skimmer'/'BFZ', 'Common').
card_artist('silent skimmer'/'BFZ', 'Alejandro Mirabal').
card_number('silent skimmer'/'BFZ', '96').
card_flavor_text('silent skimmer'/'BFZ', '\"I watched it pass at a distance. When I looked back to my companions, Najet was dead.\"—Akiri, kor line-slinger').
card_multiverse_id('silent skimmer'/'BFZ', '402035').

card_in_set('sire of stagnation', 'BFZ').
card_original_type('sire of stagnation'/'BFZ', 'Creature — Eldrazi').
card_original_text('sire of stagnation'/'BFZ', 'Devoid (This card has no color.)Whenever a land enters the battlefield under an opponent\'s control, that player exiles the top two cards of his or her library and you draw two cards.').
card_first_print('sire of stagnation', 'BFZ').
card_image_name('sire of stagnation'/'BFZ', 'sire of stagnation').
card_uid('sire of stagnation'/'BFZ', 'BFZ:Sire of Stagnation:sire of stagnation').
card_rarity('sire of stagnation'/'BFZ', 'Mythic Rare').
card_artist('sire of stagnation'/'BFZ', 'Tyler Jacobson').
card_number('sire of stagnation'/'BFZ', '206').
card_flavor_text('sire of stagnation'/'BFZ', 'As the great scourge passed by, Zendikar shivered, then lay silent.').
card_multiverse_id('sire of stagnation'/'BFZ', '402036').

card_in_set('skitterskin', 'BFZ').
card_original_type('skitterskin'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('skitterskin'/'BFZ', 'Devoid (This card has no color.)Skitterskin can\'t block.{1}{B}: Regenerate Skitterskin. Activate this ability only if you control another colorless creature.').
card_first_print('skitterskin', 'BFZ').
card_image_name('skitterskin'/'BFZ', 'skitterskin').
card_uid('skitterskin'/'BFZ', 'BFZ:Skitterskin:skitterskin').
card_rarity('skitterskin'/'BFZ', 'Uncommon').
card_artist('skitterskin'/'BFZ', 'Michael Komarck').
card_number('skitterskin'/'BFZ', '97').
card_multiverse_id('skitterskin'/'BFZ', '402037').

card_in_set('skyline cascade', 'BFZ').
card_original_type('skyline cascade'/'BFZ', 'Land').
card_original_text('skyline cascade'/'BFZ', 'Skyline Cascade enters the battlefield tapped.When Skyline Cascade enters the battlefield, target creature an opponent controls doesn\'t untap during its controller\'s next untap step.{T}: Add {U} to your mana pool.').
card_first_print('skyline cascade', 'BFZ').
card_image_name('skyline cascade'/'BFZ', 'skyline cascade').
card_uid('skyline cascade'/'BFZ', 'BFZ:Skyline Cascade:skyline cascade').
card_rarity('skyline cascade'/'BFZ', 'Common').
card_artist('skyline cascade'/'BFZ', 'Philip Straub').
card_number('skyline cascade'/'BFZ', '246').
card_multiverse_id('skyline cascade'/'BFZ', '402038').

card_in_set('skyrider elf', 'BFZ').
card_original_type('skyrider elf'/'BFZ', 'Creature — Elf Warrior Ally').
card_original_text('skyrider elf'/'BFZ', 'FlyingConverge — Skyrider Elf enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.').
card_first_print('skyrider elf', 'BFZ').
card_image_name('skyrider elf'/'BFZ', 'skyrider elf').
card_uid('skyrider elf'/'BFZ', 'BFZ:Skyrider Elf:skyrider elf').
card_rarity('skyrider elf'/'BFZ', 'Uncommon').
card_artist('skyrider elf'/'BFZ', 'Dan Scott').
card_number('skyrider elf'/'BFZ', '220').
card_flavor_text('skyrider elf'/'BFZ', '\"It may be unconventional, but I\'ll take any steady mount in a storm.\"').
card_multiverse_id('skyrider elf'/'BFZ', '402039').

card_in_set('slab hammer', 'BFZ').
card_original_type('slab hammer'/'BFZ', 'Artifact — Equipment').
card_original_text('slab hammer'/'BFZ', 'Whenever equipped creature attacks, you may return a land you control to its owner\'s hand. If you do, the creature gets +2/+2 until end of turn.Equip {2}').
card_first_print('slab hammer', 'BFZ').
card_image_name('slab hammer'/'BFZ', 'slab hammer').
card_uid('slab hammer'/'BFZ', 'BFZ:Slab Hammer:slab hammer').
card_rarity('slab hammer'/'BFZ', 'Uncommon').
card_artist('slab hammer'/'BFZ', 'Joseph Meehan').
card_number('slab hammer'/'BFZ', '227').
card_flavor_text('slab hammer'/'BFZ', 'Neither subtle nor pretty.').
card_multiverse_id('slab hammer'/'BFZ', '402040').

card_in_set('sludge crawler', 'BFZ').
card_original_type('sludge crawler'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('sludge crawler'/'BFZ', 'Devoid (This card has no color.)Ingest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.){2}: Sludge Crawler gets +1/+1 until end of turn.').
card_first_print('sludge crawler', 'BFZ').
card_image_name('sludge crawler'/'BFZ', 'sludge crawler').
card_uid('sludge crawler'/'BFZ', 'BFZ:Sludge Crawler:sludge crawler').
card_rarity('sludge crawler'/'BFZ', 'Common').
card_artist('sludge crawler'/'BFZ', 'Johann Bodin').
card_number('sludge crawler'/'BFZ', '98').
card_multiverse_id('sludge crawler'/'BFZ', '402041').

card_in_set('smite the monstrous', 'BFZ').
card_original_type('smite the monstrous'/'BFZ', 'Instant').
card_original_text('smite the monstrous'/'BFZ', 'Destroy target creature with power 4 or greater.').
card_image_name('smite the monstrous'/'BFZ', 'smite the monstrous').
card_uid('smite the monstrous'/'BFZ', 'BFZ:Smite the Monstrous:smite the monstrous').
card_rarity('smite the monstrous'/'BFZ', 'Common').
card_artist('smite the monstrous'/'BFZ', 'Jakub Kasper').
card_number('smite the monstrous'/'BFZ', '49').
card_flavor_text('smite the monstrous'/'BFZ', 'After watching the kor in battle, Gideon understood the difference between working together and working as one.').
card_multiverse_id('smite the monstrous'/'BFZ', '402042').

card_in_set('smoldering marsh', 'BFZ').
card_original_type('smoldering marsh'/'BFZ', 'Land — Swamp Mountain').
card_original_text('smoldering marsh'/'BFZ', '({T}: Add {B} or {R} to your mana pool.)Smoldering Marsh enters the battlefield tapped unless you control two or more basic lands.').
card_first_print('smoldering marsh', 'BFZ').
card_image_name('smoldering marsh'/'BFZ', 'smoldering marsh').
card_uid('smoldering marsh'/'BFZ', 'BFZ:Smoldering Marsh:smoldering marsh').
card_rarity('smoldering marsh'/'BFZ', 'Rare').
card_artist('smoldering marsh'/'BFZ', 'Adam Paquette').
card_number('smoldering marsh'/'BFZ', '247').
card_flavor_text('smoldering marsh'/'BFZ', 'The continent of Guul Draz is a geothermal swampland reeking of heat and decay.').
card_multiverse_id('smoldering marsh'/'BFZ', '402043').

card_in_set('smothering abomination', 'BFZ').
card_original_type('smothering abomination'/'BFZ', 'Creature — Eldrazi').
card_original_text('smothering abomination'/'BFZ', 'Devoid (This card has no color.)FlyingAt the beginning of your upkeep, sacrifice a creature.Whenever you sacrifice a creature, draw a card.').
card_first_print('smothering abomination', 'BFZ').
card_image_name('smothering abomination'/'BFZ', 'smothering abomination').
card_uid('smothering abomination'/'BFZ', 'BFZ:Smothering Abomination:smothering abomination').
card_rarity('smothering abomination'/'BFZ', 'Rare').
card_artist('smothering abomination'/'BFZ', 'Aleksi Briclot').
card_number('smothering abomination'/'BFZ', '99').
card_multiverse_id('smothering abomination'/'BFZ', '402044').

card_in_set('snapping gnarlid', 'BFZ').
card_original_type('snapping gnarlid'/'BFZ', 'Creature — Beast').
card_original_text('snapping gnarlid'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, Snapping Gnarlid gets +1/+1 until end of turn.').
card_first_print('snapping gnarlid', 'BFZ').
card_image_name('snapping gnarlid'/'BFZ', 'snapping gnarlid').
card_uid('snapping gnarlid'/'BFZ', 'BFZ:Snapping Gnarlid:snapping gnarlid').
card_rarity('snapping gnarlid'/'BFZ', 'Common').
card_artist('snapping gnarlid'/'BFZ', 'Kev Walker').
card_number('snapping gnarlid'/'BFZ', '190').
card_flavor_text('snapping gnarlid'/'BFZ', 'All of Zendikar\'s beings sense the upheaval that accompanies the Eldrazi.').
card_multiverse_id('snapping gnarlid'/'BFZ', '402045').

card_in_set('spawning bed', 'BFZ').
card_original_type('spawning bed'/'BFZ', 'Land').
card_original_text('spawning bed'/'BFZ', '{T}: Add {1} to your mana pool.{6}, {T}, Sacrifice Spawning Bed: Put three 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('spawning bed', 'BFZ').
card_image_name('spawning bed'/'BFZ', 'spawning bed').
card_uid('spawning bed'/'BFZ', 'BFZ:Spawning Bed:spawning bed').
card_rarity('spawning bed'/'BFZ', 'Uncommon').
card_artist('spawning bed'/'BFZ', 'Jason Felix').
card_number('spawning bed'/'BFZ', '248').
card_flavor_text('spawning bed'/'BFZ', 'Young Eldrazi crawl forth from the dust, compelled by a greater will.').
card_multiverse_id('spawning bed'/'BFZ', '402046').

card_in_set('spell shrivel', 'BFZ').
card_original_type('spell shrivel'/'BFZ', 'Instant').
card_original_text('spell shrivel'/'BFZ', 'Devoid (This card has no color.)Counter target spell unless its controller pays {4}. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_first_print('spell shrivel', 'BFZ').
card_image_name('spell shrivel'/'BFZ', 'spell shrivel').
card_uid('spell shrivel'/'BFZ', 'BFZ:Spell Shrivel:spell shrivel').
card_rarity('spell shrivel'/'BFZ', 'Common').
card_artist('spell shrivel'/'BFZ', 'Jack Wang').
card_number('spell shrivel'/'BFZ', '66').
card_multiverse_id('spell shrivel'/'BFZ', '402047').

card_in_set('stasis snare', 'BFZ').
card_original_type('stasis snare'/'BFZ', 'Enchantment').
card_original_text('stasis snare'/'BFZ', 'Flash (You may cast this spell any time you could cast an instant.)When Stasis Snare enters the battlefield, exile target creature an opponent controls until Stasis Snare leaves the battlefield. (That creature returns under its owner\'s control.)').
card_first_print('stasis snare', 'BFZ').
card_image_name('stasis snare'/'BFZ', 'stasis snare').
card_uid('stasis snare'/'BFZ', 'BFZ:Stasis Snare:stasis snare').
card_rarity('stasis snare'/'BFZ', 'Uncommon').
card_artist('stasis snare'/'BFZ', 'Jason Felix').
card_number('stasis snare'/'BFZ', '50').
card_multiverse_id('stasis snare'/'BFZ', '402048').

card_in_set('stone haven medic', 'BFZ').
card_original_type('stone haven medic'/'BFZ', 'Creature — Kor Cleric').
card_original_text('stone haven medic'/'BFZ', '{W}, {T}: You gain 1 life.').
card_first_print('stone haven medic', 'BFZ').
card_image_name('stone haven medic'/'BFZ', 'stone haven medic').
card_uid('stone haven medic'/'BFZ', 'BFZ:Stone Haven Medic:stone haven medic').
card_rarity('stone haven medic'/'BFZ', 'Common').
card_artist('stone haven medic'/'BFZ', 'Anna Steinbauer').
card_number('stone haven medic'/'BFZ', '51').
card_flavor_text('stone haven medic'/'BFZ', '\"These days, soldiers never stick around long enough for a proper healing. They just want me to knit them up so they can hurry back to the fight.\"').
card_multiverse_id('stone haven medic'/'BFZ', '402049').

card_in_set('stonefury', 'BFZ').
card_original_type('stonefury'/'BFZ', 'Instant').
card_original_text('stonefury'/'BFZ', 'Stonefury deals damage to target creature equal to the number of lands you control.').
card_first_print('stonefury', 'BFZ').
card_image_name('stonefury'/'BFZ', 'stonefury').
card_uid('stonefury'/'BFZ', 'BFZ:Stonefury:stonefury').
card_rarity('stonefury'/'BFZ', 'Common').
card_artist('stonefury'/'BFZ', 'Chris Rallis').
card_number('stonefury'/'BFZ', '156').
card_flavor_text('stonefury'/'BFZ', 'Akoum can make any intruder look small.').
card_multiverse_id('stonefury'/'BFZ', '402050').

card_in_set('sunken hollow', 'BFZ').
card_original_type('sunken hollow'/'BFZ', 'Land — Island Swamp').
card_original_text('sunken hollow'/'BFZ', '({T}: Add {U} or {B} to your mana pool.)Sunken Hollow enters the battlefield tapped unless you control two or more basic lands.').
card_first_print('sunken hollow', 'BFZ').
card_image_name('sunken hollow'/'BFZ', 'sunken hollow').
card_uid('sunken hollow'/'BFZ', 'BFZ:Sunken Hollow:sunken hollow').
card_rarity('sunken hollow'/'BFZ', 'Rare').
card_artist('sunken hollow'/'BFZ', 'Adam Paquette').
card_number('sunken hollow'/'BFZ', '249').
card_flavor_text('sunken hollow'/'BFZ', 'On the continent of Tazeem, rushing waters plunge through narrow canyons into mist-cloaked lakes.').
card_multiverse_id('sunken hollow'/'BFZ', '402051').

card_in_set('sure strike', 'BFZ').
card_original_type('sure strike'/'BFZ', 'Instant').
card_original_text('sure strike'/'BFZ', 'Target creature gets +3/+0 and gains first strike until end of turn.').
card_first_print('sure strike', 'BFZ').
card_image_name('sure strike'/'BFZ', 'sure strike').
card_uid('sure strike'/'BFZ', 'BFZ:Sure Strike:sure strike').
card_rarity('sure strike'/'BFZ', 'Common').
card_artist('sure strike'/'BFZ', 'Jakub Kasper').
card_number('sure strike'/'BFZ', '157').
card_flavor_text('sure strike'/'BFZ', 'To survive imminent doom, it sometimes takes a foolhardy soul who acts first and fears later.').
card_multiverse_id('sure strike'/'BFZ', '402052').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp1').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp1').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Noah Bradley').
card_number('swamp'/'BFZ', '260').
card_multiverse_id('swamp'/'BFZ', '402055').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp10').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp10').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Tianhua X').
card_number('swamp'/'BFZ', '264').
card_multiverse_id('swamp'/'BFZ', '402057').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp2').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp2').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Noah Bradley').
card_number('swamp'/'BFZ', '260').
card_multiverse_id('swamp'/'BFZ', '402061').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp3').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp3').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Sam Burley').
card_number('swamp'/'BFZ', '261').
card_multiverse_id('swamp'/'BFZ', '402053').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp4').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp4').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Sam Burley').
card_number('swamp'/'BFZ', '261').
card_multiverse_id('swamp'/'BFZ', '402058').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp5').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp5').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Adam Paquette').
card_number('swamp'/'BFZ', '262').
card_multiverse_id('swamp'/'BFZ', '402060').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp6').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp6').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Adam Paquette').
card_number('swamp'/'BFZ', '262').
card_multiverse_id('swamp'/'BFZ', '402054').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp7').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp7').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Jung Park').
card_number('swamp'/'BFZ', '263').
card_multiverse_id('swamp'/'BFZ', '402059').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp8').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp8').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Jung Park').
card_number('swamp'/'BFZ', '263').
card_multiverse_id('swamp'/'BFZ', '402056').

card_in_set('swamp', 'BFZ').
card_original_type('swamp'/'BFZ', 'Basic Land — Swamp').
card_original_text('swamp'/'BFZ', 'B').
card_image_name('swamp'/'BFZ', 'swamp9').
card_uid('swamp'/'BFZ', 'BFZ:Swamp:swamp9').
card_rarity('swamp'/'BFZ', 'Basic Land').
card_artist('swamp'/'BFZ', 'Tianhua X').
card_number('swamp'/'BFZ', '264').
card_multiverse_id('swamp'/'BFZ', '402062').

card_in_set('swarm surge', 'BFZ').
card_original_type('swarm surge'/'BFZ', 'Sorcery').
card_original_text('swarm surge'/'BFZ', 'Devoid (This card has no color.)Creatures you control get +2/+0 until end of turn. Colorless creatures you control also gain first strike until end of turn.').
card_first_print('swarm surge', 'BFZ').
card_image_name('swarm surge'/'BFZ', 'swarm surge').
card_uid('swarm surge'/'BFZ', 'BFZ:Swarm Surge:swarm surge').
card_rarity('swarm surge'/'BFZ', 'Common').
card_artist('swarm surge'/'BFZ', 'Svetlin Velinov').
card_number('swarm surge'/'BFZ', '100').
card_flavor_text('swarm surge'/'BFZ', 'The Eldrazi rampaged across Zendikar in a tide of chittering, heaving flesh.').
card_multiverse_id('swarm surge'/'BFZ', '402063').

card_in_set('swell of growth', 'BFZ').
card_original_type('swell of growth'/'BFZ', 'Instant').
card_original_text('swell of growth'/'BFZ', 'Target creature gets +2/+2 until end of turn. You may put a land card from your hand onto the battlefield.').
card_first_print('swell of growth', 'BFZ').
card_image_name('swell of growth'/'BFZ', 'swell of growth').
card_uid('swell of growth'/'BFZ', 'BFZ:Swell of Growth:swell of growth').
card_rarity('swell of growth'/'BFZ', 'Common').
card_artist('swell of growth'/'BFZ', 'Magali Villeneuve').
card_number('swell of growth'/'BFZ', '191').
card_flavor_text('swell of growth'/'BFZ', '\"Nissa is not the only one with a deep connection to Zendikar\'s primal elements.\"—Kiora').
card_multiverse_id('swell of growth'/'BFZ', '402064').

card_in_set('sylvan scrying', 'BFZ').
card_original_type('sylvan scrying'/'BFZ', 'Sorcery').
card_original_text('sylvan scrying'/'BFZ', 'Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('sylvan scrying'/'BFZ', 'sylvan scrying').
card_uid('sylvan scrying'/'BFZ', 'BFZ:Sylvan Scrying:sylvan scrying').
card_rarity('sylvan scrying'/'BFZ', 'Uncommon').
card_artist('sylvan scrying'/'BFZ', 'Daniel Ljunggren').
card_number('sylvan scrying'/'BFZ', '192').
card_flavor_text('sylvan scrying'/'BFZ', '\"As long as there is a single shred of life, there is hope.\"—Nissa Revane').
card_multiverse_id('sylvan scrying'/'BFZ', '402065').

card_in_set('tajuru beastmaster', 'BFZ').
card_original_type('tajuru beastmaster'/'BFZ', 'Creature — Elf Warrior Ally').
card_original_text('tajuru beastmaster'/'BFZ', 'Rally — Whenever Tajuru Beastmaster or another Ally enters the battlefield under your control, creatures you control get +1/+1 until end of turn.').
card_first_print('tajuru beastmaster', 'BFZ').
card_image_name('tajuru beastmaster'/'BFZ', 'tajuru beastmaster').
card_uid('tajuru beastmaster'/'BFZ', 'BFZ:Tajuru Beastmaster:tajuru beastmaster').
card_rarity('tajuru beastmaster'/'BFZ', 'Common').
card_artist('tajuru beastmaster'/'BFZ', 'Greg Opalinski').
card_number('tajuru beastmaster'/'BFZ', '193').
card_flavor_text('tajuru beastmaster'/'BFZ', '\"My mount needs neither reins nor bridle. We both share a common purpose.\"').
card_multiverse_id('tajuru beastmaster'/'BFZ', '402066').

card_in_set('tajuru stalwart', 'BFZ').
card_original_type('tajuru stalwart'/'BFZ', 'Creature — Elf Scout Ally').
card_original_text('tajuru stalwart'/'BFZ', 'Converge — Tajuru Stalwart enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.').
card_first_print('tajuru stalwart', 'BFZ').
card_image_name('tajuru stalwart'/'BFZ', 'tajuru stalwart').
card_uid('tajuru stalwart'/'BFZ', 'BFZ:Tajuru Stalwart:tajuru stalwart').
card_rarity('tajuru stalwart'/'BFZ', 'Common').
card_artist('tajuru stalwart'/'BFZ', 'Wesley Burt').
card_number('tajuru stalwart'/'BFZ', '194').
card_flavor_text('tajuru stalwart'/'BFZ', '\"In its time of hardship, all of Zendikar cries out in pain. Are you listening?\"').
card_multiverse_id('tajuru stalwart'/'BFZ', '402067').

card_in_set('tajuru warcaller', 'BFZ').
card_original_type('tajuru warcaller'/'BFZ', 'Creature — Elf Warrior Ally').
card_original_text('tajuru warcaller'/'BFZ', 'Rally — Whenever Tajuru Warcaller or another Ally enters the battlefield under your control, creatures you control get +2/+2 until end of turn.').
card_first_print('tajuru warcaller', 'BFZ').
card_image_name('tajuru warcaller'/'BFZ', 'tajuru warcaller').
card_uid('tajuru warcaller'/'BFZ', 'BFZ:Tajuru Warcaller:tajuru warcaller').
card_rarity('tajuru warcaller'/'BFZ', 'Uncommon').
card_artist('tajuru warcaller'/'BFZ', 'Anastasia Ovchinnikova').
card_number('tajuru warcaller'/'BFZ', '195').
card_flavor_text('tajuru warcaller'/'BFZ', 'Her rallying cry reaches far beyond the Tajuru elves, inspiring all those who hear it.').
card_multiverse_id('tajuru warcaller'/'BFZ', '402068').

card_in_set('tandem tactics', 'BFZ').
card_original_type('tandem tactics'/'BFZ', 'Instant').
card_original_text('tandem tactics'/'BFZ', 'Up to two target creatures each get +1/+2 until end of turn. You gain 2 life.').
card_first_print('tandem tactics', 'BFZ').
card_image_name('tandem tactics'/'BFZ', 'tandem tactics').
card_uid('tandem tactics'/'BFZ', 'BFZ:Tandem Tactics:tandem tactics').
card_rarity('tandem tactics'/'BFZ', 'Common').
card_artist('tandem tactics'/'BFZ', 'David Gaillet').
card_number('tandem tactics'/'BFZ', '52').
card_flavor_text('tandem tactics'/'BFZ', 'In times of infestation and war, Zendikar favors the blades that strike in unison.').
card_multiverse_id('tandem tactics'/'BFZ', '402069').

card_in_set('territorial baloth', 'BFZ').
card_original_type('territorial baloth'/'BFZ', 'Creature — Beast').
card_original_text('territorial baloth'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, Territorial Baloth gets +2/+2 until end of turn.').
card_image_name('territorial baloth'/'BFZ', 'territorial baloth').
card_uid('territorial baloth'/'BFZ', 'BFZ:Territorial Baloth:territorial baloth').
card_rarity('territorial baloth'/'BFZ', 'Common').
card_artist('territorial baloth'/'BFZ', 'Matt Stewart').
card_number('territorial baloth'/'BFZ', '196').
card_flavor_text('territorial baloth'/'BFZ', 'It knows nothing of Eldrazi or Planeswalkers, just that there are new intruders in its territory.').
card_multiverse_id('territorial baloth'/'BFZ', '402070').

card_in_set('tide drifter', 'BFZ').
card_original_type('tide drifter'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('tide drifter'/'BFZ', 'Devoid (This card has no color.)Other colorless creatures you control get +0/+1.').
card_first_print('tide drifter', 'BFZ').
card_image_name('tide drifter'/'BFZ', 'tide drifter').
card_uid('tide drifter'/'BFZ', 'BFZ:Tide Drifter:tide drifter').
card_rarity('tide drifter'/'BFZ', 'Uncommon').
card_artist('tide drifter'/'BFZ', 'Daarken').
card_number('tide drifter'/'BFZ', '67').
card_flavor_text('tide drifter'/'BFZ', 'It didn\'t do much, but the residents of Sea Gate gave it a wide berth all the same.').
card_multiverse_id('tide drifter'/'BFZ', '402071').

card_in_set('tightening coils', 'BFZ').
card_original_type('tightening coils'/'BFZ', 'Enchantment — Aura').
card_original_text('tightening coils'/'BFZ', 'Enchant creatureEnchanted creature gets -6/-0 and loses flying.').
card_first_print('tightening coils', 'BFZ').
card_image_name('tightening coils'/'BFZ', 'tightening coils').
card_uid('tightening coils'/'BFZ', 'BFZ:Tightening Coils:tightening coils').
card_rarity('tightening coils'/'BFZ', 'Common').
card_artist('tightening coils'/'BFZ', 'Tyler Jacobson').
card_number('tightening coils'/'BFZ', '86').
card_flavor_text('tightening coils'/'BFZ', 'Having gone to the trouble of stealing a bident from a god on another world, Kiora was eager to test its capabilities.').
card_multiverse_id('tightening coils'/'BFZ', '402072').

card_in_set('titan\'s presence', 'BFZ').
card_original_type('titan\'s presence'/'BFZ', 'Instant').
card_original_text('titan\'s presence'/'BFZ', 'As an additional cost to cast Titan\'s Presence, reveal a colorless creature card from your hand.Exile target creature if its power is less than or equal to the revealed card\'s power.').
card_first_print('titan\'s presence', 'BFZ').
card_image_name('titan\'s presence'/'BFZ', 'titan\'s presence').
card_uid('titan\'s presence'/'BFZ', 'BFZ:Titan\'s Presence:titan\'s presence').
card_rarity('titan\'s presence'/'BFZ', 'Uncommon').
card_artist('titan\'s presence'/'BFZ', 'Slawomir Maniak').
card_number('titan\'s presence'/'BFZ', '14').
card_flavor_text('titan\'s presence'/'BFZ', 'Dust and memory are all that remain in Ulamog\'s wake.').
card_multiverse_id('titan\'s presence'/'BFZ', '402073').

card_in_set('touch of the void', 'BFZ').
card_original_type('touch of the void'/'BFZ', 'Sorcery').
card_original_text('touch of the void'/'BFZ', 'Devoid (This card has no color.)Touch of the Void deals 3 damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.').
card_first_print('touch of the void', 'BFZ').
card_image_name('touch of the void'/'BFZ', 'touch of the void').
card_uid('touch of the void'/'BFZ', 'BFZ:Touch of the Void:touch of the void').
card_rarity('touch of the void'/'BFZ', 'Common').
card_artist('touch of the void'/'BFZ', 'David Gaillet').
card_number('touch of the void'/'BFZ', '134').
card_flavor_text('touch of the void'/'BFZ', 'Some wounds never heal.').
card_multiverse_id('touch of the void'/'BFZ', '402074').

card_in_set('transgress the mind', 'BFZ').
card_original_type('transgress the mind'/'BFZ', 'Sorcery').
card_original_text('transgress the mind'/'BFZ', 'Devoid (This card has no color.)Target player reveals his or her hand. You choose a card from it with converted mana cost 3 or greater and exile that card.').
card_first_print('transgress the mind', 'BFZ').
card_image_name('transgress the mind'/'BFZ', 'transgress the mind').
card_uid('transgress the mind'/'BFZ', 'BFZ:Transgress the Mind:transgress the mind').
card_rarity('transgress the mind'/'BFZ', 'Uncommon').
card_artist('transgress the mind'/'BFZ', 'Cynthia Sheppard').
card_number('transgress the mind'/'BFZ', '101').
card_flavor_text('transgress the mind'/'BFZ', '\"Fear of the unknown can be a wonderfully innocent thing, relatively speaking.\"—Anowon, the Ruin Sage').
card_multiverse_id('transgress the mind'/'BFZ', '402075').

card_in_set('tunneling geopede', 'BFZ').
card_original_type('tunneling geopede'/'BFZ', 'Creature — Insect').
card_original_text('tunneling geopede'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, Tunneling Geopede deals 1 damage to each opponent.').
card_first_print('tunneling geopede', 'BFZ').
card_image_name('tunneling geopede'/'BFZ', 'tunneling geopede').
card_uid('tunneling geopede'/'BFZ', 'BFZ:Tunneling Geopede:tunneling geopede').
card_rarity('tunneling geopede'/'BFZ', 'Uncommon').
card_artist('tunneling geopede'/'BFZ', 'Tomasz Jedruszek').
card_number('tunneling geopede'/'BFZ', '158').
card_flavor_text('tunneling geopede'/'BFZ', 'As Ulamog\'s brood reduces the earth to dust, geopedes burst from their tunnels in search of solid ground.').
card_multiverse_id('tunneling geopede'/'BFZ', '402076').

card_in_set('turn against', 'BFZ').
card_original_type('turn against'/'BFZ', 'Instant').
card_original_text('turn against'/'BFZ', 'Devoid (This card has no color.)Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_first_print('turn against', 'BFZ').
card_image_name('turn against'/'BFZ', 'turn against').
card_uid('turn against'/'BFZ', 'BFZ:Turn Against:turn against').
card_rarity('turn against'/'BFZ', 'Uncommon').
card_artist('turn against'/'BFZ', 'Chris Rallis').
card_number('turn against'/'BFZ', '135').
card_flavor_text('turn against'/'BFZ', '\"No, don\'t! Nal, it\'s me—\"').
card_multiverse_id('turn against'/'BFZ', '402077').

card_in_set('ugin\'s insight', 'BFZ').
card_original_type('ugin\'s insight'/'BFZ', 'Sorcery').
card_original_text('ugin\'s insight'/'BFZ', 'Scry X, where X is the highest converted mana cost among permanents you control, then draw three cards.').
card_first_print('ugin\'s insight', 'BFZ').
card_image_name('ugin\'s insight'/'BFZ', 'ugin\'s insight').
card_uid('ugin\'s insight'/'BFZ', 'BFZ:Ugin\'s Insight:ugin\'s insight').
card_rarity('ugin\'s insight'/'BFZ', 'Rare').
card_artist('ugin\'s insight'/'BFZ', 'Tyler Jacobson').
card_number('ugin\'s insight'/'BFZ', '87').
card_flavor_text('ugin\'s insight'/'BFZ', '\"You speak to me of the threat of Ulamog. But do not forget: they came as three.\"—Ugin, to Jace Beleren').
card_multiverse_id('ugin\'s insight'/'BFZ', '402078').

card_in_set('ulamog\'s despoiler', 'BFZ').
card_original_type('ulamog\'s despoiler'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('ulamog\'s despoiler'/'BFZ', 'As Ulamog\'s Despoiler enters the battlefield, you may put two cards your opponents own from exile into their owners\' graveyards. If you do, Ulamog\'s Despoiler enters the battlefield with four +1/+1 counters on it.').
card_first_print('ulamog\'s despoiler', 'BFZ').
card_image_name('ulamog\'s despoiler'/'BFZ', 'ulamog\'s despoiler').
card_uid('ulamog\'s despoiler'/'BFZ', 'BFZ:Ulamog\'s Despoiler:ulamog\'s despoiler').
card_rarity('ulamog\'s despoiler'/'BFZ', 'Uncommon').
card_artist('ulamog\'s despoiler'/'BFZ', 'Peter Mohrbacher').
card_number('ulamog\'s despoiler'/'BFZ', '16').
card_multiverse_id('ulamog\'s despoiler'/'BFZ', '402080').

card_in_set('ulamog\'s nullifier', 'BFZ').
card_original_type('ulamog\'s nullifier'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('ulamog\'s nullifier'/'BFZ', 'Devoid (This card has no color.)FlashFlyingWhen Ulamog\'s Nullifier enters the battlefield, you may put two cards your opponents own from exile into their owners\' graveyards. If you do, counter target spell.').
card_first_print('ulamog\'s nullifier', 'BFZ').
card_image_name('ulamog\'s nullifier'/'BFZ', 'ulamog\'s nullifier').
card_uid('ulamog\'s nullifier'/'BFZ', 'BFZ:Ulamog\'s Nullifier:ulamog\'s nullifier').
card_rarity('ulamog\'s nullifier'/'BFZ', 'Uncommon').
card_artist('ulamog\'s nullifier'/'BFZ', 'Aleksi Briclot').
card_number('ulamog\'s nullifier'/'BFZ', '207').
card_multiverse_id('ulamog\'s nullifier'/'BFZ', '402081').

card_in_set('ulamog\'s reclaimer', 'BFZ').
card_original_type('ulamog\'s reclaimer'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('ulamog\'s reclaimer'/'BFZ', 'Devoid (This card has no color.)When Ulamog\'s Reclaimer enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, return target instant or sorcery card from your graveyard to your hand.').
card_first_print('ulamog\'s reclaimer', 'BFZ').
card_image_name('ulamog\'s reclaimer'/'BFZ', 'ulamog\'s reclaimer').
card_uid('ulamog\'s reclaimer'/'BFZ', 'BFZ:Ulamog\'s Reclaimer:ulamog\'s reclaimer').
card_rarity('ulamog\'s reclaimer'/'BFZ', 'Uncommon').
card_artist('ulamog\'s reclaimer'/'BFZ', 'Svetlin Velinov').
card_number('ulamog\'s reclaimer'/'BFZ', '68').
card_multiverse_id('ulamog\'s reclaimer'/'BFZ', '402082').

card_in_set('ulamog, the ceaseless hunger', 'BFZ').
card_original_type('ulamog, the ceaseless hunger'/'BFZ', 'Legendary Creature — Eldrazi').
card_original_text('ulamog, the ceaseless hunger'/'BFZ', 'When you cast Ulamog, the Ceaseless Hunger, exile two target permanents.IndestructibleWhenever Ulamog attacks, defending player exiles the top twenty cards of his or her library.').
card_first_print('ulamog, the ceaseless hunger', 'BFZ').
card_image_name('ulamog, the ceaseless hunger'/'BFZ', 'ulamog, the ceaseless hunger').
card_uid('ulamog, the ceaseless hunger'/'BFZ', 'BFZ:Ulamog, the Ceaseless Hunger:ulamog, the ceaseless hunger').
card_rarity('ulamog, the ceaseless hunger'/'BFZ', 'Mythic Rare').
card_artist('ulamog, the ceaseless hunger'/'BFZ', 'Michael Komarck').
card_number('ulamog, the ceaseless hunger'/'BFZ', '15').
card_flavor_text('ulamog, the ceaseless hunger'/'BFZ', 'A force as voracious as time itself.').
card_multiverse_id('ulamog, the ceaseless hunger'/'BFZ', '402079').

card_in_set('undergrowth champion', 'BFZ').
card_original_type('undergrowth champion'/'BFZ', 'Creature — Elemental').
card_original_text('undergrowth champion'/'BFZ', 'If damage would be dealt to Undergrowth Champion while it has a +1/+1 counter on it, prevent that damage and remove a +1/+1 counter from Undergrowth Champion.Landfall — Whenever a land enters the battlefield under your control, put a +1/+1 counter on Undergrowth Champion.').
card_first_print('undergrowth champion', 'BFZ').
card_image_name('undergrowth champion'/'BFZ', 'undergrowth champion').
card_uid('undergrowth champion'/'BFZ', 'BFZ:Undergrowth Champion:undergrowth champion').
card_rarity('undergrowth champion'/'BFZ', 'Mythic Rare').
card_artist('undergrowth champion'/'BFZ', 'Tyler Jacobson').
card_number('undergrowth champion'/'BFZ', '197').
card_multiverse_id('undergrowth champion'/'BFZ', '402083').

card_in_set('unified front', 'BFZ').
card_original_type('unified front'/'BFZ', 'Sorcery').
card_original_text('unified front'/'BFZ', 'Converge — Put a 1/1 white Kor Ally creature token onto the battlefield for each color of mana spent to cast Unified Front.').
card_first_print('unified front', 'BFZ').
card_image_name('unified front'/'BFZ', 'unified front').
card_uid('unified front'/'BFZ', 'BFZ:Unified Front:unified front').
card_rarity('unified front'/'BFZ', 'Uncommon').
card_artist('unified front'/'BFZ', 'Dan Scott').
card_number('unified front'/'BFZ', '53').
card_flavor_text('unified front'/'BFZ', 'The kor\'s pride in their independence has given way to a strong sense of community that embraces all of Zendikar.').
card_multiverse_id('unified front'/'BFZ', '402084').

card_in_set('unnatural aggression', 'BFZ').
card_original_type('unnatural aggression'/'BFZ', 'Instant').
card_original_text('unnatural aggression'/'BFZ', 'Devoid (This card has no color.)Target creature you control fights target creature an opponent controls. If the creature an opponent controls would die this turn, exile it instead.').
card_first_print('unnatural aggression', 'BFZ').
card_image_name('unnatural aggression'/'BFZ', 'unnatural aggression').
card_uid('unnatural aggression'/'BFZ', 'BFZ:Unnatural Aggression:unnatural aggression').
card_rarity('unnatural aggression'/'BFZ', 'Common').
card_artist('unnatural aggression'/'BFZ', 'James Ryman').
card_number('unnatural aggression'/'BFZ', '168').
card_flavor_text('unnatural aggression'/'BFZ', 'The battle served as a grim reminder of what a final victory for the Eldrazi would mean.').
card_multiverse_id('unnatural aggression'/'BFZ', '402085').

card_in_set('valakut invoker', 'BFZ').
card_original_type('valakut invoker'/'BFZ', 'Creature — Human Shaman').
card_original_text('valakut invoker'/'BFZ', '{8}: Valakut Invoker deals 3 damage to target creature or player.').
card_first_print('valakut invoker', 'BFZ').
card_image_name('valakut invoker'/'BFZ', 'valakut invoker').
card_uid('valakut invoker'/'BFZ', 'BFZ:Valakut Invoker:valakut invoker').
card_rarity('valakut invoker'/'BFZ', 'Common').
card_artist('valakut invoker'/'BFZ', 'Joseph Meehan').
card_number('valakut invoker'/'BFZ', '159').
card_flavor_text('valakut invoker'/'BFZ', '\"As long as we have walked this world, we have known it as a harsh and unforgiving place.\"—The Invokers\' Tales').
card_multiverse_id('valakut invoker'/'BFZ', '402086').

card_in_set('valakut predator', 'BFZ').
card_original_type('valakut predator'/'BFZ', 'Creature — Elemental').
card_original_text('valakut predator'/'BFZ', 'Landfall — Whenever a land enters the battlefield under your control, Valakut Predator gets +2/+2 until end of turn.').
card_first_print('valakut predator', 'BFZ').
card_image_name('valakut predator'/'BFZ', 'valakut predator').
card_uid('valakut predator'/'BFZ', 'BFZ:Valakut Predator:valakut predator').
card_rarity('valakut predator'/'BFZ', 'Common').
card_artist('valakut predator'/'BFZ', 'Kev Walker').
card_number('valakut predator'/'BFZ', '160').
card_flavor_text('valakut predator'/'BFZ', '\"Whatever volcanoes dream of, it seems like they always wake up grumpy.\"—Raff Slugeater, goblin shortcutter').
card_multiverse_id('valakut predator'/'BFZ', '402087').

card_in_set('vampiric rites', 'BFZ').
card_original_type('vampiric rites'/'BFZ', 'Enchantment').
card_original_text('vampiric rites'/'BFZ', '{1}{B}, Sacrifice a creature: You gain 1 life and draw a card.').
card_first_print('vampiric rites', 'BFZ').
card_image_name('vampiric rites'/'BFZ', 'vampiric rites').
card_uid('vampiric rites'/'BFZ', 'BFZ:Vampiric Rites:vampiric rites').
card_rarity('vampiric rites'/'BFZ', 'Uncommon').
card_artist('vampiric rites'/'BFZ', 'Anastasia Ovchinnikova').
card_number('vampiric rites'/'BFZ', '124').
card_flavor_text('vampiric rites'/'BFZ', 'For vampires, blood carries more than life. Memories long eroded from the minds of bloodchiefs can be reclaimed from the veins of their sired kin.').
card_multiverse_id('vampiric rites'/'BFZ', '402088').

card_in_set('vestige of emrakul', 'BFZ').
card_original_type('vestige of emrakul'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('vestige of emrakul'/'BFZ', 'Devoid (This card has no color.)Trample').
card_first_print('vestige of emrakul', 'BFZ').
card_image_name('vestige of emrakul'/'BFZ', 'vestige of emrakul').
card_uid('vestige of emrakul'/'BFZ', 'BFZ:Vestige of Emrakul:vestige of emrakul').
card_rarity('vestige of emrakul'/'BFZ', 'Common').
card_artist('vestige of emrakul'/'BFZ', 'Tyler Jacobson').
card_number('vestige of emrakul'/'BFZ', '136').
card_flavor_text('vestige of emrakul'/'BFZ', 'Emrakul has not been seen in months. Though her brood\'s numbers have dwindled in her absence, each drone is still a deadly threat.').
card_multiverse_id('vestige of emrakul'/'BFZ', '402089').

card_in_set('veteran warleader', 'BFZ').
card_original_type('veteran warleader'/'BFZ', 'Creature — Human Soldier Ally').
card_original_text('veteran warleader'/'BFZ', 'Veteran Warleader\'s power and toughness are each equal to the number of creatures you control.Tap another untapped Ally you control: Veteran Warleader gains your choice of first strike, vigilance, or trample until end of turn.').
card_image_name('veteran warleader'/'BFZ', 'veteran warleader').
card_uid('veteran warleader'/'BFZ', 'BFZ:Veteran Warleader:veteran warleader').
card_rarity('veteran warleader'/'BFZ', 'Rare').
card_artist('veteran warleader'/'BFZ', 'Josu Hernaiz').
card_number('veteran warleader'/'BFZ', '221').
card_multiverse_id('veteran warleader'/'BFZ', '402090').

card_in_set('vile aggregate', 'BFZ').
card_original_type('vile aggregate'/'BFZ', 'Creature — Eldrazi Drone').
card_original_text('vile aggregate'/'BFZ', 'Devoid (This card has no color.)Vile Aggregate\'s power is equal to the number of colorless creatures you control.TrampleIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)').
card_first_print('vile aggregate', 'BFZ').
card_image_name('vile aggregate'/'BFZ', 'vile aggregate').
card_uid('vile aggregate'/'BFZ', 'BFZ:Vile Aggregate:vile aggregate').
card_rarity('vile aggregate'/'BFZ', 'Uncommon').
card_artist('vile aggregate'/'BFZ', 'Chris Rallis').
card_number('vile aggregate'/'BFZ', '137').
card_multiverse_id('vile aggregate'/'BFZ', '402091').

card_in_set('void attendant', 'BFZ').
card_original_type('void attendant'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('void attendant'/'BFZ', 'Devoid (This card has no color.){1}{G}, Put a card an opponent owns from exile into that player\'s graveyard: Put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('void attendant', 'BFZ').
card_image_name('void attendant'/'BFZ', 'void attendant').
card_uid('void attendant'/'BFZ', 'BFZ:Void Attendant:void attendant').
card_rarity('void attendant'/'BFZ', 'Uncommon').
card_artist('void attendant'/'BFZ', 'Viktor Titov').
card_number('void attendant'/'BFZ', '169').
card_flavor_text('void attendant'/'BFZ', 'The Eldrazi don\'t merely extinguish life—they supplant it.').
card_multiverse_id('void attendant'/'BFZ', '402092').

card_in_set('void winnower', 'BFZ').
card_original_type('void winnower'/'BFZ', 'Creature — Eldrazi').
card_original_text('void winnower'/'BFZ', 'Your opponents can\'t cast spells with even converted mana costs. (Zero is even.)Your opponents can\'t block with creatures with even converted mana costs.').
card_first_print('void winnower', 'BFZ').
card_image_name('void winnower'/'BFZ', 'void winnower').
card_uid('void winnower'/'BFZ', 'BFZ:Void Winnower:void winnower').
card_rarity('void winnower'/'BFZ', 'Mythic Rare').
card_artist('void winnower'/'BFZ', 'Chase Stone').
card_number('void winnower'/'BFZ', '17').
card_flavor_text('void winnower'/'BFZ', '\"Have you ever seen a world fall to its knees? Watch, and learn.\"—Ob Nixilis').
card_multiverse_id('void winnower'/'BFZ', '402093').

card_in_set('volcanic upheaval', 'BFZ').
card_original_type('volcanic upheaval'/'BFZ', 'Instant').
card_original_text('volcanic upheaval'/'BFZ', 'Destroy target land.').
card_first_print('volcanic upheaval', 'BFZ').
card_image_name('volcanic upheaval'/'BFZ', 'volcanic upheaval').
card_uid('volcanic upheaval'/'BFZ', 'BFZ:Volcanic Upheaval:volcanic upheaval').
card_rarity('volcanic upheaval'/'BFZ', 'Common').
card_artist('volcanic upheaval'/'BFZ', 'Yeong-Hao Han').
card_number('volcanic upheaval'/'BFZ', '161').
card_flavor_text('volcanic upheaval'/'BFZ', 'Like a living organism, Zendikar rids itself of infection, and it does so abruptly and ruthlessly.').
card_multiverse_id('volcanic upheaval'/'BFZ', '402094').

card_in_set('voracious null', 'BFZ').
card_original_type('voracious null'/'BFZ', 'Creature — Zombie').
card_original_text('voracious null'/'BFZ', '{1}{B}, Sacrifice another creature: Put two +1/+1 counters on Voracious Null. Activate this ability only any time you could cast a sorcery.').
card_first_print('voracious null', 'BFZ').
card_image_name('voracious null'/'BFZ', 'voracious null').
card_uid('voracious null'/'BFZ', 'BFZ:Voracious Null:voracious null').
card_rarity('voracious null'/'BFZ', 'Common').
card_artist('voracious null'/'BFZ', 'Karl Kopinski').
card_number('voracious null'/'BFZ', '125').
card_flavor_text('voracious null'/'BFZ', '\"These days, there\'s no shortage of food for the nulls of Guul Draz.\"—Drana, Kalastria bloodchief').
card_multiverse_id('voracious null'/'BFZ', '402095').

card_in_set('wasteland strangler', 'BFZ').
card_original_type('wasteland strangler'/'BFZ', 'Creature — Eldrazi Processor').
card_original_text('wasteland strangler'/'BFZ', 'Devoid (This card has no color.)When Wasteland Strangler enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, target creature gets -3/-3 until end of turn.').
card_first_print('wasteland strangler', 'BFZ').
card_image_name('wasteland strangler'/'BFZ', 'wasteland strangler').
card_uid('wasteland strangler'/'BFZ', 'BFZ:Wasteland Strangler:wasteland strangler').
card_rarity('wasteland strangler'/'BFZ', 'Rare').
card_artist('wasteland strangler'/'BFZ', 'Jack Wang').
card_number('wasteland strangler'/'BFZ', '102').
card_multiverse_id('wasteland strangler'/'BFZ', '402096').

card_in_set('wave-wing elemental', 'BFZ').
card_original_type('wave-wing elemental'/'BFZ', 'Creature — Elemental').
card_original_text('wave-wing elemental'/'BFZ', 'FlyingLandfall — Whenever a land enters the battlefield under your control, Wave-Wing Elemental gets +2/+2 until end of turn.').
card_first_print('wave-wing elemental', 'BFZ').
card_image_name('wave-wing elemental'/'BFZ', 'wave-wing elemental').
card_uid('wave-wing elemental'/'BFZ', 'BFZ:Wave-Wing Elemental:wave-wing elemental').
card_rarity('wave-wing elemental'/'BFZ', 'Common').
card_artist('wave-wing elemental'/'BFZ', 'John Severin Brassell').
card_number('wave-wing elemental'/'BFZ', '88').
card_flavor_text('wave-wing elemental'/'BFZ', '\"Do you see? All of Tazeem strains at its tether.\"—Noyan Dar, Tazeem roilmage').
card_multiverse_id('wave-wing elemental'/'BFZ', '402097').

card_in_set('windrider patrol', 'BFZ').
card_original_type('windrider patrol'/'BFZ', 'Creature — Merfolk Wizard').
card_original_text('windrider patrol'/'BFZ', 'FlyingWhenever Windrider Patrol deals combat damage to a player, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('windrider patrol', 'BFZ').
card_image_name('windrider patrol'/'BFZ', 'windrider patrol').
card_uid('windrider patrol'/'BFZ', 'BFZ:Windrider Patrol:windrider patrol').
card_rarity('windrider patrol'/'BFZ', 'Uncommon').
card_artist('windrider patrol'/'BFZ', 'Svetlin Velinov').
card_number('windrider patrol'/'BFZ', '89').
card_multiverse_id('windrider patrol'/'BFZ', '402098').

card_in_set('woodland wanderer', 'BFZ').
card_original_type('woodland wanderer'/'BFZ', 'Creature — Elemental').
card_original_text('woodland wanderer'/'BFZ', 'Vigilance, trampleConverge — Woodland Wanderer enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.').
card_first_print('woodland wanderer', 'BFZ').
card_image_name('woodland wanderer'/'BFZ', 'woodland wanderer').
card_uid('woodland wanderer'/'BFZ', 'BFZ:Woodland Wanderer:woodland wanderer').
card_rarity('woodland wanderer'/'BFZ', 'Rare').
card_artist('woodland wanderer'/'BFZ', 'Vincent Proce').
card_number('woodland wanderer'/'BFZ', '198').
card_flavor_text('woodland wanderer'/'BFZ', '\"The elements understand better than anyone that actions speak louder than words.\"—Gideon Jura').
card_multiverse_id('woodland wanderer'/'BFZ', '402099').

card_in_set('zada, hedron grinder', 'BFZ').
card_original_type('zada, hedron grinder'/'BFZ', 'Legendary Creature — Goblin Ally').
card_original_text('zada, hedron grinder'/'BFZ', 'Whenever you cast an instant or sorcery spell that targets only Zada, Hedron Grinder, copy that spell for each other creature you control that the spell could target. Each copy targets a different one of those creatures.').
card_first_print('zada, hedron grinder', 'BFZ').
card_image_name('zada, hedron grinder'/'BFZ', 'zada, hedron grinder').
card_uid('zada, hedron grinder'/'BFZ', 'BFZ:Zada, Hedron Grinder:zada, hedron grinder').
card_rarity('zada, hedron grinder'/'BFZ', 'Rare').
card_artist('zada, hedron grinder'/'BFZ', 'Chris Rallis').
card_number('zada, hedron grinder'/'BFZ', '162').
card_flavor_text('zada, hedron grinder'/'BFZ', '\"A hedron holds magic for a thousand years—or less, if need be.\"').
card_multiverse_id('zada, hedron grinder'/'BFZ', '402100').

card_in_set('zulaport cutthroat', 'BFZ').
card_original_type('zulaport cutthroat'/'BFZ', 'Creature — Human Rogue Ally').
card_original_text('zulaport cutthroat'/'BFZ', 'Whenever Zulaport Cutthroat or another creature you control dies, each opponent loses 1 life and you gain 1 life.').
card_first_print('zulaport cutthroat', 'BFZ').
card_image_name('zulaport cutthroat'/'BFZ', 'zulaport cutthroat').
card_uid('zulaport cutthroat'/'BFZ', 'BFZ:Zulaport Cutthroat:zulaport cutthroat').
card_rarity('zulaport cutthroat'/'BFZ', 'Uncommon').
card_artist('zulaport cutthroat'/'BFZ', 'Jason Rainville').
card_number('zulaport cutthroat'/'BFZ', '126').
card_flavor_text('zulaport cutthroat'/'BFZ', '\"Eldrazi? Ha! Try walking through Zulaport at night with your pockets full. Now that\'s dangerous.\"').
card_multiverse_id('zulaport cutthroat'/'BFZ', '402101').
