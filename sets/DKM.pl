% Deckmasters

set('DKM').
set_name('DKM', 'Deckmasters').
set_release_date('DKM', '2001-12-01').
set_border('DKM', 'white').
set_type('DKM', 'box').

card_in_set('abyssal specter', 'DKM').
card_original_type('abyssal specter'/'DKM', 'Creature — Specter').
card_original_text('abyssal specter'/'DKM', 'Flying\nWhenever Abyssal Specter deals damage to a player, that player discards a card from his or her hand.').
card_image_name('abyssal specter'/'DKM', 'abyssal specter').
card_uid('abyssal specter'/'DKM', 'DKM:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'DKM', 'Uncommon').
card_artist('abyssal specter'/'DKM', 'Ruth Thompson').
card_number('abyssal specter'/'DKM', '1').
card_flavor_text('abyssal specter'/'DKM', '\"Open the gates, and bid them enter.\"\n—Lim-Dûl, the Necromancer').

card_in_set('balduvian bears', 'DKM').
card_original_type('balduvian bears'/'DKM', 'Creature — Bear').
card_original_text('balduvian bears'/'DKM', '').
card_image_name('balduvian bears'/'DKM', 'balduvian bears').
card_uid('balduvian bears'/'DKM', 'DKM:Balduvian Bears:balduvian bears').
card_rarity('balduvian bears'/'DKM', 'Common').
card_artist('balduvian bears'/'DKM', 'Quinton Hoover').
card_number('balduvian bears'/'DKM', '22').
card_flavor_text('balduvian bears'/'DKM', '\"They\'re a hardy bunch, but I\'d still bet that they just slept through the worst of the cold times.\"\n—Disa the Restless, journal entry').

card_in_set('balduvian horde', 'DKM').
card_original_type('balduvian horde'/'DKM', 'Creature — Barbarians').
card_original_text('balduvian horde'/'DKM', 'When Balduvian Horde comes into play, sacrifice it unless you discard a card at random from your hand.').
card_image_name('balduvian horde'/'DKM', 'balduvian horde').
card_uid('balduvian horde'/'DKM', 'DKM:Balduvian Horde:balduvian horde').
card_rarity('balduvian horde'/'DKM', 'Rare').
card_artist('balduvian horde'/'DKM', 'Brian Snõddy').
card_number('balduvian horde'/'DKM', '10').
card_flavor_text('balduvian horde'/'DKM', '\"Peace will come only when we have taken Varchild\'s head.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').

card_in_set('barbed sextant', 'DKM').
card_original_type('barbed sextant'/'DKM', 'Artifact').
card_original_text('barbed sextant'/'DKM', '{1}, {T}, Sacrifice Barbed Sextant: Add one mana of any color to your mana pool. Draw a card at the beginning of next turn\'s upkeep.').
card_image_name('barbed sextant'/'DKM', 'barbed sextant').
card_uid('barbed sextant'/'DKM', 'DKM:Barbed Sextant:barbed sextant').
card_rarity('barbed sextant'/'DKM', 'Common').
card_artist('barbed sextant'/'DKM', 'Amy Weber').
card_number('barbed sextant'/'DKM', '34').

card_in_set('bounty of the hunt', 'DKM').
card_original_type('bounty of the hunt'/'DKM', 'Instant').
card_original_text('bounty of the hunt'/'DKM', 'You may remove a green card in your hand from the game rather than pay Bounty of the Hunt\'s mana cost.\nChoose one — Target creature gets +3/+3 until end of turn; or target creature gets +2/+2 and another target creature gets +1/+1 until end of turn; or three target creatures each get +1/+1 until end of turn.').
card_image_name('bounty of the hunt'/'DKM', 'bounty of the hunt').
card_uid('bounty of the hunt'/'DKM', 'DKM:Bounty of the Hunt:bounty of the hunt').
card_rarity('bounty of the hunt'/'DKM', 'Uncommon').
card_artist('bounty of the hunt'/'DKM', 'Jeff A. Menges').
card_number('bounty of the hunt'/'DKM', '23').

card_in_set('contagion', 'DKM').
card_original_type('contagion'/'DKM', 'Instant').
card_original_text('contagion'/'DKM', 'You may pay 1 life and remove a black card in your hand from the game rather than pay Contagion\'s mana cost.\nPut two -2/-1 counters, distributed as you choose, on one or two target creatures.').
card_image_name('contagion'/'DKM', 'contagion').
card_uid('contagion'/'DKM', 'DKM:Contagion:contagion').
card_rarity('contagion'/'DKM', 'Uncommon').
card_artist('contagion'/'DKM', 'Mike Raabe').
card_number('contagion'/'DKM', '2').

card_in_set('dark banishing', 'DKM').
card_original_type('dark banishing'/'DKM', 'Instant').
card_original_text('dark banishing'/'DKM', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'DKM', 'dark banishing').
card_uid('dark banishing'/'DKM', 'DKM:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'DKM', 'Common').
card_artist('dark banishing'/'DKM', 'Drew Tucker').
card_number('dark banishing'/'DKM', '3').
card_flavor_text('dark banishing'/'DKM', '\"Will not the mountains quake and hills melt at the coming of the darkness? Share this vision with your enemies, Lim-Dûl, and they shall wither.\"\n—Leshrac, Walker of Night').

card_in_set('dark ritual', 'DKM').
card_original_type('dark ritual'/'DKM', 'Interrupt').
card_original_text('dark ritual'/'DKM', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'DKM', 'dark ritual').
card_uid('dark ritual'/'DKM', 'DKM:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'DKM', 'Common').
card_artist('dark ritual'/'DKM', 'Justin Hampton').
card_number('dark ritual'/'DKM', '4').
card_flavor_text('dark ritual'/'DKM', '\"Leshrac, my liege, grant me the power I am due.\"\n—Lim-Dûl, the Necromancer').

card_in_set('death spark', 'DKM').
card_original_type('death spark'/'DKM', 'Instant').
card_original_text('death spark'/'DKM', 'Death Spark deals 1 damage to target creature or player.\nAt the beginning of your upkeep, if Death Spark is in your graveyard with a creature card directly above it, you may pay {1}. If you do, return Death Spark to your hand.').
card_image_name('death spark'/'DKM', 'death spark').
card_uid('death spark'/'DKM', 'DKM:Death Spark:death spark').
card_rarity('death spark'/'DKM', 'Uncommon').
card_artist('death spark'/'DKM', 'Mark Tedin').
card_number('death spark'/'DKM', '11').

card_in_set('elkin bottle', 'DKM').
card_original_type('elkin bottle'/'DKM', 'Artifact').
card_original_text('elkin bottle'/'DKM', '{3}, {T}: Remove the top card of your library from the game face up. Until the beginning of your next upkeep, you may play that card as though it were in your hand. At the beginning of your next upkeep, if you haven\'t played the card, put it into your graveyard.').
card_image_name('elkin bottle'/'DKM', 'elkin bottle').
card_uid('elkin bottle'/'DKM', 'DKM:Elkin Bottle:elkin bottle').
card_rarity('elkin bottle'/'DKM', 'Rare').
card_artist('elkin bottle'/'DKM', 'Quinton Hoover').
card_number('elkin bottle'/'DKM', '35').

card_in_set('elvish bard', 'DKM').
card_original_type('elvish bard'/'DKM', 'Creature — Elf').
card_original_text('elvish bard'/'DKM', 'All creatures able to block Elvish Bard do so.').
card_image_name('elvish bard'/'DKM', 'elvish bard').
card_uid('elvish bard'/'DKM', 'DKM:Elvish Bard:elvish bard').
card_rarity('elvish bard'/'DKM', 'Uncommon').
card_artist('elvish bard'/'DKM', 'Susan Van Camp').
card_number('elvish bard'/'DKM', '24').

card_in_set('folk of the pines', 'DKM').
card_original_type('folk of the pines'/'DKM', 'Creature — Dryads').
card_original_text('folk of the pines'/'DKM', '{1}{G}: Folk of the Pines gets +1/+0 until end of turn.').
card_image_name('folk of the pines'/'DKM', 'folk of the pines').
card_uid('folk of the pines'/'DKM', 'DKM:Folk of the Pines:folk of the pines').
card_rarity('folk of the pines'/'DKM', 'Common').
card_artist('folk of the pines'/'DKM', 'NéNé Thomas & Catherine Buck').
card_number('folk of the pines'/'DKM', '25').
card_flavor_text('folk of the pines'/'DKM', '\"Our friends of the forest take many forms, yet all serve the will of Freyalise.\"\n—Laina of the Elvish Council').

card_in_set('forest', 'DKM').
card_original_type('forest'/'DKM', 'Land').
card_original_text('forest'/'DKM', '{G}').
card_image_name('forest'/'DKM', 'forest1').
card_uid('forest'/'DKM', 'DKM:Forest:forest1').
card_rarity('forest'/'DKM', 'Basic Land').
card_artist('forest'/'DKM', 'Pat Morrissey').
card_number('forest'/'DKM', '48').

card_in_set('forest', 'DKM').
card_original_type('forest'/'DKM', 'Land').
card_original_text('forest'/'DKM', '{G}').
card_image_name('forest'/'DKM', 'forest2').
card_uid('forest'/'DKM', 'DKM:Forest:forest2').
card_rarity('forest'/'DKM', 'Basic Land').
card_artist('forest'/'DKM', 'Pat Morrissey').
card_number('forest'/'DKM', '49').

card_in_set('forest', 'DKM').
card_original_type('forest'/'DKM', 'Land').
card_original_text('forest'/'DKM', '{G}').
card_image_name('forest'/'DKM', 'forest3').
card_uid('forest'/'DKM', 'DKM:Forest:forest3').
card_rarity('forest'/'DKM', 'Basic Land').
card_artist('forest'/'DKM', 'Pat Morrissey').
card_number('forest'/'DKM', '50').

card_in_set('foul familiar', 'DKM').
card_original_type('foul familiar'/'DKM', 'Creature — Spirit').
card_original_text('foul familiar'/'DKM', 'Foul Familiar can\'t black.\n{B}, Pay 1 life: Return Foul Familiar to its owner\'s hand.').
card_image_name('foul familiar'/'DKM', 'foul familiar').
card_uid('foul familiar'/'DKM', 'DKM:Foul Familiar:foul familiar').
card_rarity('foul familiar'/'DKM', 'Common').
card_artist('foul familiar'/'DKM', 'Anson Maddocks').
card_number('foul familiar'/'DKM', '5').
card_flavor_text('foul familiar'/'DKM', '\"Serve me, and live forever.\"\n—Lim-Dûl, the Necromancer').

card_in_set('fyndhorn elves', 'DKM').
card_original_type('fyndhorn elves'/'DKM', 'Creature — Elves').
card_original_text('fyndhorn elves'/'DKM', '{T}: Add {G} to your mana pool.').
card_image_name('fyndhorn elves'/'DKM', 'fyndhorn elves').
card_uid('fyndhorn elves'/'DKM', 'DKM:Fyndhorn Elves:fyndhorn elves').
card_rarity('fyndhorn elves'/'DKM', 'Common').
card_artist('fyndhorn elves'/'DKM', 'Justin Hampton').
card_number('fyndhorn elves'/'DKM', '26').
card_flavor_text('fyndhorn elves'/'DKM', '\"Living side by side with the Elves for so long leaves me with no doubt that we serve the same goddess.\"\n—Kolbjörn, Elder Druid of the Juniper Order').

card_in_set('giant growth', 'DKM').
card_original_type('giant growth'/'DKM', 'Instant').
card_original_text('giant growth'/'DKM', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'DKM', 'giant growth').
card_uid('giant growth'/'DKM', 'DKM:Giant Growth:giant growth').
card_rarity('giant growth'/'DKM', 'Common').
card_artist('giant growth'/'DKM', 'L. A. Williams').
card_number('giant growth'/'DKM', '27').
card_flavor_text('giant growth'/'DKM', '\"Here in Fyndhorn, the goddess Freyalise is generous to her children.\"\n—Kolbjörn, Elder Druid of the Juniper Order').

card_in_set('giant trap door spider', 'DKM').
card_original_type('giant trap door spider'/'DKM', 'Creature — Spider').
card_original_text('giant trap door spider'/'DKM', '{1}{R}{G}, {T}: Remove from the game Giant Trap Door Spider and target creature without flying that\'s attacking you.').
card_image_name('giant trap door spider'/'DKM', 'giant trap door spider').
card_uid('giant trap door spider'/'DKM', 'DKM:Giant Trap Door Spider:giant trap door spider').
card_rarity('giant trap door spider'/'DKM', 'Special').
card_artist('giant trap door spider'/'DKM', 'Heather Hudson').
card_number('giant trap door spider'/'DKM', '33').
card_flavor_text('giant trap door spider'/'DKM', '\"So large and so quiet—a combination best avoided.\"\n—Disa the Restless, journal entry').

card_in_set('goblin mutant', 'DKM').
card_original_type('goblin mutant'/'DKM', 'Creature — Goblin').
card_original_text('goblin mutant'/'DKM', 'Trample\nGoblin Mutant can\'t attack if defending player controls an untapped creature with power 3 or greater.\nGoblin Mutant can\'t block creatures with power 3 or greater.').
card_image_name('goblin mutant'/'DKM', 'goblin mutant').
card_uid('goblin mutant'/'DKM', 'DKM:Goblin Mutant:goblin mutant').
card_rarity('goblin mutant'/'DKM', 'Uncommon').
card_artist('goblin mutant'/'DKM', 'Daniel Gelon').
card_number('goblin mutant'/'DKM', '12').
card_flavor_text('goblin mutant'/'DKM', 'If only it had three brains, too.').

card_in_set('guerrilla tactics', 'DKM').
card_original_type('guerrilla tactics'/'DKM', 'Instant').
card_original_text('guerrilla tactics'/'DKM', 'Guerrilla Tactics deals 2 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Guerrilla Tactics from your hand, Guerrilla Tactics deals 4 damage to target creature or player.').
card_image_name('guerrilla tactics'/'DKM', 'guerrilla tactics1').
card_uid('guerrilla tactics'/'DKM', 'DKM:Guerrilla Tactics:guerrilla tactics1').
card_rarity('guerrilla tactics'/'DKM', 'Common').
card_artist('guerrilla tactics'/'DKM', 'Amy Weber').
card_number('guerrilla tactics'/'DKM', '13a').

card_in_set('guerrilla tactics', 'DKM').
card_original_type('guerrilla tactics'/'DKM', 'Instant').
card_original_text('guerrilla tactics'/'DKM', 'Guerrilla Tactics deals 2 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Guerrilla Tactics from your hand, Guerrilla Tactics deals 4 damage to target creature or player.').
card_image_name('guerrilla tactics'/'DKM', 'guerrilla tactics2').
card_uid('guerrilla tactics'/'DKM', 'DKM:Guerrilla Tactics:guerrilla tactics2').
card_rarity('guerrilla tactics'/'DKM', 'Common').
card_artist('guerrilla tactics'/'DKM', 'Randy Asplund-Faith').
card_number('guerrilla tactics'/'DKM', '13b').

card_in_set('hurricane', 'DKM').
card_original_type('hurricane'/'DKM', 'Sorcery').
card_original_text('hurricane'/'DKM', 'Hurricane deals X damage to each creature with flying and each player.').
card_image_name('hurricane'/'DKM', 'hurricane').
card_uid('hurricane'/'DKM', 'DKM:Hurricane:hurricane').
card_rarity('hurricane'/'DKM', 'Uncommon').
card_artist('hurricane'/'DKM', 'Cornelius Brudi').
card_number('hurricane'/'DKM', '28').
card_flavor_text('hurricane'/'DKM', '\"This was quite possibly the least pleasant occurrence on the entire expedition.\"\n—Disa the Restless, journal entry').

card_in_set('icy manipulator', 'DKM').
card_original_type('icy manipulator'/'DKM', 'Artifact').
card_original_text('icy manipulator'/'DKM', '{1},{T}: Tap target artifact, creature, or land.').
card_image_name('icy manipulator'/'DKM', 'icy manipulator').
card_uid('icy manipulator'/'DKM', 'DKM:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'DKM', 'Uncommon').
card_artist('icy manipulator'/'DKM', 'Amy Weber').
card_number('icy manipulator'/'DKM', '36').
card_flavor_text('icy manipulator'/'DKM', '\"The scavengers who first found it called it the ‘Bone Crank.\' Not a bad name, I\'d say.\"\n—Arcum Dagsson, Soldevi Machinist').

card_in_set('incinerate', 'DKM').
card_original_type('incinerate'/'DKM', 'Instant').
card_original_text('incinerate'/'DKM', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_image_name('incinerate'/'DKM', 'incinerate').
card_uid('incinerate'/'DKM', 'DKM:Incinerate:incinerate').
card_rarity('incinerate'/'DKM', 'Common').
card_artist('incinerate'/'DKM', 'Mark Poole').
card_number('incinerate'/'DKM', '14').
card_flavor_text('incinerate'/'DKM', '\"Yes, I think ‘toast\' is an appropriate description.\"\n—Jaya Ballard, Task Mage').

card_in_set('jokulhaups', 'DKM').
card_original_type('jokulhaups'/'DKM', 'Sorcery').
card_original_text('jokulhaups'/'DKM', 'Destroy all artifacts, creatures, and lands. They can\'t be regenerated.').
card_image_name('jokulhaups'/'DKM', 'jokulhaups').
card_uid('jokulhaups'/'DKM', 'DKM:Jokulhaups:jokulhaups').
card_rarity('jokulhaups'/'DKM', 'Rare').
card_artist('jokulhaups'/'DKM', 'Richard Thomas').
card_number('jokulhaups'/'DKM', '15').
card_flavor_text('jokulhaups'/'DKM', '\"I was shocked when I first saw the aftermath of the Yavimaya Valley disaster. The raging waters had swept away trees, bridges, and even houses. My healers had much work to do.\"\n—Halvor Arenson, Kjeldoran Priest').

card_in_set('karplusan forest', 'DKM').
card_original_type('karplusan forest'/'DKM', 'Land').
card_original_text('karplusan forest'/'DKM', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_image_name('karplusan forest'/'DKM', 'karplusan forest').
card_uid('karplusan forest'/'DKM', 'DKM:Karplusan Forest:karplusan forest').
card_rarity('karplusan forest'/'DKM', 'Rare').
card_artist('karplusan forest'/'DKM', 'Nicola Leonard').
card_number('karplusan forest'/'DKM', '39').

card_in_set('lava burst', 'DKM').
card_original_type('lava burst'/'DKM', 'Sorcery').
card_original_text('lava burst'/'DKM', 'Lava Burst deals X damage to target creature or player. If Lava Burst would deal damage to a creature, that damage can\'t be prevented or dealt instead to another creature or player.').
card_image_name('lava burst'/'DKM', 'lava burst').
card_uid('lava burst'/'DKM', 'DKM:Lava Burst:lava burst').
card_rarity('lava burst'/'DKM', 'Common').
card_artist('lava burst'/'DKM', 'Tom Wänerstrand').
card_number('lava burst'/'DKM', '16').
card_flavor_text('lava burst'/'DKM', '\"Overkill? This isn\'t a game of Kick-the-Ouphe!\"\n—Jaya Ballard, Task Mage').

card_in_set('lhurgoyf', 'DKM').
card_original_type('lhurgoyf'/'DKM', 'Creature — Lhurgoyf').
card_original_text('lhurgoyf'/'DKM', 'Lhurgoyf\'s power is equal to the number of creature cards in all graveyards and its toughness is equal to that number plus 1.').
card_border('lhurgoyf'/'DKM', 'black').
card_image_name('lhurgoyf'/'DKM', 'lhurgoyf').
card_uid('lhurgoyf'/'DKM', 'DKM:Lhurgoyf:lhurgoyf').
card_rarity('lhurgoyf'/'DKM', 'Rare').
card_artist('lhurgoyf'/'DKM', 'Pete Venters').
card_number('lhurgoyf'/'DKM', '29').
card_flavor_text('lhurgoyf'/'DKM', '\"Ach! Hans, run! It\'s the Lhurgoyf!\"\n—Saffi Eriksdotter, last words').

card_in_set('lim-dûl\'s high guard', 'DKM').
card_original_type('lim-dûl\'s high guard'/'DKM', 'Creature — Skeleton').
card_original_text('lim-dûl\'s high guard'/'DKM', 'First strike\n{1}{B}: Regenerate').
card_image_name('lim-dûl\'s high guard'/'DKM', 'lim-dul\'s high guard1').
card_uid('lim-dûl\'s high guard'/'DKM', 'DKM:Lim-Dûl\'s High Guard:lim-dul\'s high guard1').
card_rarity('lim-dûl\'s high guard'/'DKM', 'Common').
card_artist('lim-dûl\'s high guard'/'DKM', 'Anson Maddocks').
card_number('lim-dûl\'s high guard'/'DKM', '6a').
card_flavor_text('lim-dûl\'s high guard'/'DKM', '\"The Guard will forever stand ready. For them, death is merely an inconvenience, not an ending.\"\n—Chaeska, Keeper of Tresserhorn').

card_in_set('lim-dûl\'s high guard', 'DKM').
card_original_type('lim-dûl\'s high guard'/'DKM', 'Creature — Skeleton').
card_original_text('lim-dûl\'s high guard'/'DKM', 'First strike\n{1}{B}: Regenerate').
card_image_name('lim-dûl\'s high guard'/'DKM', 'lim-dul\'s high guard2').
card_uid('lim-dûl\'s high guard'/'DKM', 'DKM:Lim-Dûl\'s High Guard:lim-dul\'s high guard2').
card_rarity('lim-dûl\'s high guard'/'DKM', 'Common').
card_artist('lim-dûl\'s high guard'/'DKM', 'Anson Maddocks').
card_number('lim-dûl\'s high guard'/'DKM', '6b').
card_flavor_text('lim-dûl\'s high guard'/'DKM', '\"The Guard will forever stand ready. For them, death is merely an inconvenience, not an ending.\"\n—Chaeska, Keeper of Tresserhorn').

card_in_set('mountain', 'DKM').
card_original_type('mountain'/'DKM', 'Land').
card_original_text('mountain'/'DKM', '{R}').
card_image_name('mountain'/'DKM', 'mountain1').
card_uid('mountain'/'DKM', 'DKM:Mountain:mountain1').
card_rarity('mountain'/'DKM', 'Basic Land').
card_artist('mountain'/'DKM', 'Tom Wänerstrand').
card_number('mountain'/'DKM', '45').

card_in_set('mountain', 'DKM').
card_original_type('mountain'/'DKM', 'Land').
card_original_text('mountain'/'DKM', '{R}').
card_image_name('mountain'/'DKM', 'mountain2').
card_uid('mountain'/'DKM', 'DKM:Mountain:mountain2').
card_rarity('mountain'/'DKM', 'Basic Land').
card_artist('mountain'/'DKM', 'Tom Wänerstrand').
card_number('mountain'/'DKM', '46').

card_in_set('mountain', 'DKM').
card_original_type('mountain'/'DKM', 'Land').
card_original_text('mountain'/'DKM', '{R}').
card_image_name('mountain'/'DKM', 'mountain3').
card_uid('mountain'/'DKM', 'DKM:Mountain:mountain3').
card_rarity('mountain'/'DKM', 'Basic Land').
card_artist('mountain'/'DKM', 'Tom Wänerstrand').
card_number('mountain'/'DKM', '47').

card_in_set('necropotence', 'DKM').
card_original_type('necropotence'/'DKM', 'Enchantment').
card_original_text('necropotence'/'DKM', 'Skip your draw step.\nIf you would discard a card from your hand, remove that card from the game instead.\nPay 1 life: Remove the top card of your library from the game face down. At the end of your turn, put that card into your hand.').
card_border('necropotence'/'DKM', 'black').
card_image_name('necropotence'/'DKM', 'necropotence').
card_uid('necropotence'/'DKM', 'DKM:Necropotence:necropotence').
card_rarity('necropotence'/'DKM', 'Rare').
card_artist('necropotence'/'DKM', 'Mark Tedin').
card_number('necropotence'/'DKM', '7').

card_in_set('orcish cannoneers', 'DKM').
card_original_type('orcish cannoneers'/'DKM', 'Creature — Orcs').
card_original_text('orcish cannoneers'/'DKM', '{T}: Orcish Cannoneers deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish cannoneers'/'DKM', 'orcish cannoneers').
card_uid('orcish cannoneers'/'DKM', 'DKM:Orcish Cannoneers:orcish cannoneers').
card_rarity('orcish cannoneers'/'DKM', 'Uncommon').
card_artist('orcish cannoneers'/'DKM', 'Dan Frazier').
card_number('orcish cannoneers'/'DKM', '17').
card_flavor_text('orcish cannoneers'/'DKM', '\"It\'s a thankless job, and you\'ll probably die in an explosion. But the pay is pretty good.\"\n—Toothlicker Harj, Orcish Captain').

card_in_set('phantasmal fiend', 'DKM').
card_original_type('phantasmal fiend'/'DKM', 'Creature — Phantasm').
card_original_text('phantasmal fiend'/'DKM', '{B}: Phantasmal Fiend gets +1/-1 until end of turn.\n{1}{U}: Switch Phantasmal Fiend\'s power and toughness until end of turn. Effects that would alter Phantasmal Fiend\'s power this turn alter its toughness instead, and vice versa.').
card_image_name('phantasmal fiend'/'DKM', 'phantasmal fiend1').
card_uid('phantasmal fiend'/'DKM', 'DKM:Phantasmal Fiend:phantasmal fiend1').
card_rarity('phantasmal fiend'/'DKM', 'Common').
card_artist('phantasmal fiend'/'DKM', 'Scott Kirschner').
card_number('phantasmal fiend'/'DKM', '8a').

card_in_set('phantasmal fiend', 'DKM').
card_original_type('phantasmal fiend'/'DKM', 'Creature — Phantasm').
card_original_text('phantasmal fiend'/'DKM', '{B}: Phantasmal Fiend gets +1/-1 until end of turn.\n{1}{U}: Switch Phantasmal Fiend\'s power and toughness until end of turn. Effects that would alter Phantasmal Fiend\'s power this turn alter its toughness instead, and vice versa.').
card_image_name('phantasmal fiend'/'DKM', 'phantasmal fiend2').
card_uid('phantasmal fiend'/'DKM', 'DKM:Phantasmal Fiend:phantasmal fiend2').
card_rarity('phantasmal fiend'/'DKM', 'Common').
card_artist('phantasmal fiend'/'DKM', 'Scott Kirschner').
card_number('phantasmal fiend'/'DKM', '8b').

card_in_set('phyrexian war beast', 'DKM').
card_original_type('phyrexian war beast'/'DKM', 'Artifact Creature').
card_original_text('phyrexian war beast'/'DKM', 'When Phyrexian War Beast leaves play, sacrifice a land and Phyrexian War Beast deals 1 damage to you.').
card_image_name('phyrexian war beast'/'DKM', 'phyrexian war beast1').
card_uid('phyrexian war beast'/'DKM', 'DKM:Phyrexian War Beast:phyrexian war beast1').
card_rarity('phyrexian war beast'/'DKM', 'Common').
card_artist('phyrexian war beast'/'DKM', 'Bill Sienkiewicz').
card_number('phyrexian war beast'/'DKM', '37a').
card_flavor_text('phyrexian war beast'/'DKM', '\"Deal with the spawn of Phyrexia cautiously; only with time may we control them.\"\n—Arcum Dagsson,\nSoldevi Machinist').

card_in_set('phyrexian war beast', 'DKM').
card_original_type('phyrexian war beast'/'DKM', 'Artifact Creature').
card_original_text('phyrexian war beast'/'DKM', 'When Phyrexian War Beast leaves play, sacrifice a land and Phyrexian War Beast deals 1 damage to you.').
card_image_name('phyrexian war beast'/'DKM', 'phyrexian war beast2').
card_uid('phyrexian war beast'/'DKM', 'DKM:Phyrexian War Beast:phyrexian war beast2').
card_rarity('phyrexian war beast'/'DKM', 'Common').
card_artist('phyrexian war beast'/'DKM', 'Bill Sienkiewicz').
card_number('phyrexian war beast'/'DKM', '37b').
card_flavor_text('phyrexian war beast'/'DKM', '\"Deal with the spawn of Phyrexia cautiously; only with time may we control them.\"\n—Arcum Dagsson,\nSoldevi Machinist').

card_in_set('pillage', 'DKM').
card_original_type('pillage'/'DKM', 'Sorcery').
card_original_text('pillage'/'DKM', 'Destroy target artifact or land. It can\'t be regenerated.').
card_image_name('pillage'/'DKM', 'pillage').
card_uid('pillage'/'DKM', 'DKM:Pillage:pillage').
card_rarity('pillage'/'DKM', 'Uncommon').
card_artist('pillage'/'DKM', 'Richard Kane Ferguson').
card_number('pillage'/'DKM', '18').
card_flavor_text('pillage'/'DKM', '\"Were they to reduce us to ash, we would clog their throats and sting their eyes in payment.\"\n—Lovisa Coldeyes,\nBalduvian Chieftain').

card_in_set('pyroclasm', 'DKM').
card_original_type('pyroclasm'/'DKM', 'Sorcery').
card_original_text('pyroclasm'/'DKM', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'DKM', 'pyroclasm').
card_uid('pyroclasm'/'DKM', 'DKM:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'DKM', 'Uncommon').
card_artist('pyroclasm'/'DKM', 'Pat Morrissey').
card_number('pyroclasm'/'DKM', '19').
card_flavor_text('pyroclasm'/'DKM', '\"Leaves more room for the big ones to fight in, you know.\"\n—Jaya Ballard, Task Mage').

card_in_set('shatter', 'DKM').
card_original_type('shatter'/'DKM', 'Instant').
card_original_text('shatter'/'DKM', 'Destroy target artifact.').
card_image_name('shatter'/'DKM', 'shatter').
card_uid('shatter'/'DKM', 'DKM:Shatter:shatter').
card_rarity('shatter'/'DKM', 'Common').
card_artist('shatter'/'DKM', 'Bryon Wackwitz').
card_number('shatter'/'DKM', '20').
card_flavor_text('shatter'/'DKM', '\"Let the past be the past. Do not call up that which you cannot put down. Destroy that which destroyed us, so long ago.\"\n—Sorine Relicbane, Soldevi Heretic').

card_in_set('soul burn', 'DKM').
card_original_type('soul burn'/'DKM', 'Sorcery').
card_original_text('soul burn'/'DKM', 'Spend only black and/or red mana on X.\nSoul Burn deals X damage to target creature or player. You gain life equal to the damage dealt, but not more than the amount of {B} spent on X, the player\'s life total before Soul Burn dealt damage, or the creature\'s toughness.').
card_image_name('soul burn'/'DKM', 'soul burn').
card_uid('soul burn'/'DKM', 'DKM:Soul Burn:soul burn').
card_rarity('soul burn'/'DKM', 'Common').
card_artist('soul burn'/'DKM', 'Rob Alexander').
card_number('soul burn'/'DKM', '9').

card_in_set('storm shaman', 'DKM').
card_original_type('storm shaman'/'DKM', 'Creature — Cleric').
card_original_text('storm shaman'/'DKM', '{R}: Storm Shaman gets +1/+0 until end of turn.').
card_image_name('storm shaman'/'DKM', 'storm shaman1').
card_uid('storm shaman'/'DKM', 'DKM:Storm Shaman:storm shaman1').
card_rarity('storm shaman'/'DKM', 'Common').
card_artist('storm shaman'/'DKM', 'Carol Heyer').
card_number('storm shaman'/'DKM', '21a').
card_flavor_text('storm shaman'/'DKM', '\"Embrace the storm. Its voice shall echo within you, and its fire shall become your touch!\"\n—Lovisa Coldeyes, Balduvian chieftain').

card_in_set('storm shaman', 'DKM').
card_original_type('storm shaman'/'DKM', 'Creature — Cleric').
card_original_text('storm shaman'/'DKM', '{R}: Storm Shaman gets +1/+0 until end of turn.').
card_image_name('storm shaman'/'DKM', 'storm shaman2').
card_uid('storm shaman'/'DKM', 'DKM:Storm Shaman:storm shaman2').
card_rarity('storm shaman'/'DKM', 'Common').
card_artist('storm shaman'/'DKM', 'Carol Heyer').
card_number('storm shaman'/'DKM', '21b').
card_flavor_text('storm shaman'/'DKM', '\"Embrace the storm. Its voice shall echo within you, and its fire shall become your touch!\"\n—Lovisa Coldeyes, Balduvian chieftain').

card_in_set('sulfurous springs', 'DKM').
card_original_type('sulfurous springs'/'DKM', 'Land').
card_original_text('sulfurous springs'/'DKM', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Sulfurous Springs deals 1 damage to you.').
card_image_name('sulfurous springs'/'DKM', 'sulfurous springs').
card_uid('sulfurous springs'/'DKM', 'DKM:Sulfurous Springs:sulfurous springs').
card_rarity('sulfurous springs'/'DKM', 'Rare').
card_artist('sulfurous springs'/'DKM', 'Phil Foglio').
card_number('sulfurous springs'/'DKM', '40').

card_in_set('swamp', 'DKM').
card_original_type('swamp'/'DKM', 'Land').
card_original_text('swamp'/'DKM', '{B}').
card_image_name('swamp'/'DKM', 'swamp1').
card_uid('swamp'/'DKM', 'DKM:Swamp:swamp1').
card_rarity('swamp'/'DKM', 'Basic Land').
card_artist('swamp'/'DKM', 'Douglas Shuler').
card_number('swamp'/'DKM', '42').

card_in_set('swamp', 'DKM').
card_original_type('swamp'/'DKM', 'Land').
card_original_text('swamp'/'DKM', '{B}').
card_image_name('swamp'/'DKM', 'swamp2').
card_uid('swamp'/'DKM', 'DKM:Swamp:swamp2').
card_rarity('swamp'/'DKM', 'Basic Land').
card_artist('swamp'/'DKM', 'Douglas Shuler').
card_number('swamp'/'DKM', '43').

card_in_set('swamp', 'DKM').
card_original_type('swamp'/'DKM', 'Land').
card_original_text('swamp'/'DKM', '{B}').
card_image_name('swamp'/'DKM', 'swamp3').
card_uid('swamp'/'DKM', 'DKM:Swamp:swamp3').
card_rarity('swamp'/'DKM', 'Basic Land').
card_artist('swamp'/'DKM', 'Douglas Shuler').
card_number('swamp'/'DKM', '44').

card_in_set('underground river', 'DKM').
card_original_type('underground river'/'DKM', 'Land').
card_original_text('underground river'/'DKM', '{T}: Add one colorless mana to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Underground River deals 1 damage to you.').
card_image_name('underground river'/'DKM', 'underground river').
card_uid('underground river'/'DKM', 'DKM:Underground River:underground river').
card_rarity('underground river'/'DKM', 'Rare').
card_artist('underground river'/'DKM', 'NéNé Thomas').
card_number('underground river'/'DKM', '41').

card_in_set('walking wall', 'DKM').
card_original_type('walking wall'/'DKM', 'Artifact Creature').
card_original_text('walking wall'/'DKM', '(Walls can\'t attack.)\n{3}: Walking Wall gets +3/-1 until end of turn and may attack this turn as though it weren\'t a Wall. Play this ability only once each turn.').
card_image_name('walking wall'/'DKM', 'walking wall').
card_uid('walking wall'/'DKM', 'DKM:Walking Wall:walking wall').
card_rarity('walking wall'/'DKM', 'Uncommon').
card_artist('walking wall'/'DKM', 'Anthony S. Waters').
card_number('walking wall'/'DKM', '38').
card_flavor_text('walking wall'/'DKM', '\"The fortress is not what it seems.\"\n—Arcum Dagsson, Soldevi Machinist').

card_in_set('woolly spider', 'DKM').
card_original_type('woolly spider'/'DKM', 'Creature — Spider').
card_original_text('woolly spider'/'DKM', 'Woolly Spider may block as though it had flying.\nWhenever Woolly Spider blocks a creature with flying, Woolly Spider gets +0/+2 until end of turn.').
card_image_name('woolly spider'/'DKM', 'woolly spider').
card_uid('woolly spider'/'DKM', 'DKM:Woolly Spider:woolly spider').
card_rarity('woolly spider'/'DKM', 'Common').
card_artist('woolly spider'/'DKM', 'Daniel Gelon').
card_number('woolly spider'/'DKM', '30').
card_flavor_text('woolly spider'/'DKM', '\"We need not fear the forces of the air; I\'ve yet to see a Spider without an appetite.\"\n—Taaveti of Kelsinko, Elvish Hunter').

card_in_set('yavimaya ancients', 'DKM').
card_original_type('yavimaya ancients'/'DKM', 'Creature — Treefolk').
card_original_text('yavimaya ancients'/'DKM', '{G}: Yavimaya Ancients gets +1/-2 until end of turn.').
card_image_name('yavimaya ancients'/'DKM', 'yavimaya ancients1').
card_uid('yavimaya ancients'/'DKM', 'DKM:Yavimaya Ancients:yavimaya ancients1').
card_rarity('yavimaya ancients'/'DKM', 'Common').
card_artist('yavimaya ancients'/'DKM', 'Quinton Hoover').
card_number('yavimaya ancients'/'DKM', '31a').
card_flavor_text('yavimaya ancients'/'DKM', '\"We orphans of Fyndhorn have found no welcome in this alient place.\"\n—Taaveti of Kelsinko, elvish hunter').

card_in_set('yavimaya ancients', 'DKM').
card_original_type('yavimaya ancients'/'DKM', 'Creature — Treefolk').
card_original_text('yavimaya ancients'/'DKM', '{G}: Yavimaya Ancients gets +1/-2 until end of turn.').
card_image_name('yavimaya ancients'/'DKM', 'yavimaya ancients2').
card_uid('yavimaya ancients'/'DKM', 'DKM:Yavimaya Ancients:yavimaya ancients2').
card_rarity('yavimaya ancients'/'DKM', 'Common').
card_artist('yavimaya ancients'/'DKM', 'Quinton Hoover').
card_number('yavimaya ancients'/'DKM', '31b').
card_flavor_text('yavimaya ancients'/'DKM', '\"We orphans of Fyndhorn have found no welcome in this alient place.\"\n—Taaveti of Kelsinko, elvish hunter').

card_in_set('yavimaya ants', 'DKM').
card_original_type('yavimaya ants'/'DKM', 'Creature — Swarm').
card_original_text('yavimaya ants'/'DKM', 'Trample; haste (This creature may attack and {T} the turn it comes under your control.)\nCumulative upkeep {G}{G} (At the beginning of your upkeep, put an age counter on this creature, then sacrifice it unless you pay {G}{G} for each age counter on it.)').
card_image_name('yavimaya ants'/'DKM', 'yavimaya ants').
card_uid('yavimaya ants'/'DKM', 'DKM:Yavimaya Ants:yavimaya ants').
card_rarity('yavimaya ants'/'DKM', 'Uncommon').
card_artist('yavimaya ants'/'DKM', 'Pat Morrissey').
card_number('yavimaya ants'/'DKM', '32').
card_flavor_text('yavimaya ants'/'DKM', '\"Few natural forces are as devastating as hunger.\"\n—Kaysa, Elder Druid of the Juniper Order').
