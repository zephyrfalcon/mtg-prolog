% Coldsnap

set('CSP').
set_name('CSP', 'Coldsnap').
set_release_date('CSP', '2006-07-21').
set_border('CSP', 'black').
set_type('CSP', 'expansion').
set_block('CSP', 'Ice Age').

card_in_set('adarkar valkyrie', 'CSP').
card_original_type('adarkar valkyrie'/'CSP', 'Snow Creature — Angel').
card_original_text('adarkar valkyrie'/'CSP', 'Flying, vigilance\n{T}: When target creature other than Adarkar Valkyrie is put into a graveyard this turn, return that card to play under your control.').
card_first_print('adarkar valkyrie', 'CSP').
card_image_name('adarkar valkyrie'/'CSP', 'adarkar valkyrie').
card_uid('adarkar valkyrie'/'CSP', 'CSP:Adarkar Valkyrie:adarkar valkyrie').
card_rarity('adarkar valkyrie'/'CSP', 'Rare').
card_artist('adarkar valkyrie'/'CSP', 'Jeremy Jarvis').
card_number('adarkar valkyrie'/'CSP', '1').
card_flavor_text('adarkar valkyrie'/'CSP', 'She doesn\'t escort the dead to the afterlife, but instead raises them to fight and die again.').
card_multiverse_id('adarkar valkyrie'/'CSP', '121196').

card_in_set('adarkar windform', 'CSP').
card_original_type('adarkar windform'/'CSP', 'Snow Creature — Illusion').
card_original_text('adarkar windform'/'CSP', 'Flying\n{1}{S}: Target creature loses flying until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('adarkar windform', 'CSP').
card_image_name('adarkar windform'/'CSP', 'adarkar windform').
card_uid('adarkar windform'/'CSP', 'CSP:Adarkar Windform:adarkar windform').
card_rarity('adarkar windform'/'CSP', 'Uncommon').
card_artist('adarkar windform'/'CSP', 'Randy Gallegos').
card_number('adarkar windform'/'CSP', '26').
card_flavor_text('adarkar windform'/'CSP', 'Terisiare has only two kinds of weather: cold and weird.').
card_multiverse_id('adarkar windform'/'CSP', '121268').

card_in_set('allosaurus rider', 'CSP').
card_original_type('allosaurus rider'/'CSP', 'Creature — Elf Warrior').
card_original_text('allosaurus rider'/'CSP', 'You may remove two green cards in your hand from the game rather than pay Allosaurus Rider\'s mana cost.\nAllosaurus Rider\'s power and toughness are each equal to 1 plus the number of lands you control.').
card_image_name('allosaurus rider'/'CSP', 'allosaurus rider').
card_uid('allosaurus rider'/'CSP', 'CSP:Allosaurus Rider:allosaurus rider').
card_rarity('allosaurus rider'/'CSP', 'Rare').
card_artist('allosaurus rider'/'CSP', 'Daren Bader').
card_number('allosaurus rider'/'CSP', '101').
card_multiverse_id('allosaurus rider'/'CSP', '121157').

card_in_set('arctic flats', 'CSP').
card_original_type('arctic flats'/'CSP', 'Snow Land').
card_original_text('arctic flats'/'CSP', 'Arctic Flats comes into play tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('arctic flats', 'CSP').
card_image_name('arctic flats'/'CSP', 'arctic flats').
card_uid('arctic flats'/'CSP', 'CSP:Arctic Flats:arctic flats').
card_rarity('arctic flats'/'CSP', 'Uncommon').
card_artist('arctic flats'/'CSP', 'John Avon').
card_number('arctic flats'/'CSP', '143').
card_flavor_text('arctic flats'/'CSP', '\"A realm once green now ruled by Frost,\nWhere flesh and field both pay its cost.\nThose dearly loved, forever lost . . .\"\n—The Dynasty of Winter Kings').
card_multiverse_id('arctic flats'/'CSP', '121158').

card_in_set('arctic nishoba', 'CSP').
card_original_type('arctic nishoba'/'CSP', 'Creature — Cat Warrior').
card_original_text('arctic nishoba'/'CSP', 'Trample\nCumulative upkeep {G} or {W} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Arctic Nishoba is put into a graveyard from play, you gain 2 life for each age counter on it.').
card_first_print('arctic nishoba', 'CSP').
card_image_name('arctic nishoba'/'CSP', 'arctic nishoba').
card_uid('arctic nishoba'/'CSP', 'CSP:Arctic Nishoba:arctic nishoba').
card_rarity('arctic nishoba'/'CSP', 'Uncommon').
card_artist('arctic nishoba'/'CSP', 'Dave Kendall').
card_number('arctic nishoba'/'CSP', '102').
card_multiverse_id('arctic nishoba'/'CSP', '121229').

card_in_set('arcum dagsson', 'CSP').
card_original_type('arcum dagsson'/'CSP', 'Legendary Creature — Human Artificer').
card_original_text('arcum dagsson'/'CSP', '{T}: Target artifact creature\'s controller sacrifices it. That player may search his or her library for a noncreature artifact card, put it into play, then shuffle his or her library.').
card_first_print('arcum dagsson', 'CSP').
card_image_name('arcum dagsson'/'CSP', 'arcum dagsson').
card_uid('arcum dagsson'/'CSP', 'CSP:Arcum Dagsson:arcum dagsson').
card_rarity('arcum dagsson'/'CSP', 'Rare').
card_artist('arcum dagsson'/'CSP', 'Pete Venters').
card_number('arcum dagsson'/'CSP', '27').
card_flavor_text('arcum dagsson'/'CSP', '\"Artifice sundered the world. It shall not again wreak such sorrow.\"').
card_multiverse_id('arcum dagsson'/'CSP', '122124').

card_in_set('aurochs herd', 'CSP').
card_original_type('aurochs herd'/'CSP', 'Creature — Aurochs').
card_original_text('aurochs herd'/'CSP', 'Trample\nWhen Aurochs Herd comes into play, you may search your library for an Aurochs card, reveal it, and put it into your hand. If you do, shuffle your library.\nWhenever Aurochs Herd attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.').
card_first_print('aurochs herd', 'CSP').
card_image_name('aurochs herd'/'CSP', 'aurochs herd').
card_uid('aurochs herd'/'CSP', 'CSP:Aurochs Herd:aurochs herd').
card_rarity('aurochs herd'/'CSP', 'Common').
card_artist('aurochs herd'/'CSP', 'Darrell Riche').
card_number('aurochs herd'/'CSP', '103').
card_multiverse_id('aurochs herd'/'CSP', '121260').

card_in_set('balduvian fallen', 'CSP').
card_original_type('balduvian fallen'/'CSP', 'Creature — Zombie').
card_original_text('balduvian fallen'/'CSP', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhenever Balduvian Fallen\'s cumulative upkeep is paid, it gets +1/+0 until end of turn for each {B} or {R} spent this way.').
card_first_print('balduvian fallen', 'CSP').
card_image_name('balduvian fallen'/'CSP', 'balduvian fallen').
card_uid('balduvian fallen'/'CSP', 'CSP:Balduvian Fallen:balduvian fallen').
card_rarity('balduvian fallen'/'CSP', 'Uncommon').
card_artist('balduvian fallen'/'CSP', 'Dave Kendall').
card_number('balduvian fallen'/'CSP', '51').
card_multiverse_id('balduvian fallen'/'CSP', '121272').

card_in_set('balduvian frostwaker', 'CSP').
card_original_type('balduvian frostwaker'/'CSP', 'Creature — Human Wizard').
card_original_text('balduvian frostwaker'/'CSP', '{U}, {T}: Target snow land becomes a 2/2 blue Elemental creature with flying. It\'s still a land.').
card_first_print('balduvian frostwaker', 'CSP').
card_image_name('balduvian frostwaker'/'CSP', 'balduvian frostwaker').
card_uid('balduvian frostwaker'/'CSP', 'CSP:Balduvian Frostwaker:balduvian frostwaker').
card_rarity('balduvian frostwaker'/'CSP', 'Uncommon').
card_artist('balduvian frostwaker'/'CSP', 'Stephen Tappin').
card_number('balduvian frostwaker'/'CSP', '28').
card_flavor_text('balduvian frostwaker'/'CSP', '\"Whether frozen or fertile, the land is our ally. It awaits the call to battle.\"').
card_multiverse_id('balduvian frostwaker'/'CSP', '121175').

card_in_set('balduvian rage', 'CSP').
card_original_type('balduvian rage'/'CSP', 'Instant').
card_original_text('balduvian rage'/'CSP', 'Target attacking creature gets +X/+0 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('balduvian rage', 'CSP').
card_image_name('balduvian rage'/'CSP', 'balduvian rage').
card_uid('balduvian rage'/'CSP', 'CSP:Balduvian Rage:balduvian rage').
card_rarity('balduvian rage'/'CSP', 'Uncommon').
card_artist('balduvian rage'/'CSP', 'John Matson').
card_number('balduvian rage'/'CSP', '76').
card_flavor_text('balduvian rage'/'CSP', '\"Let your heartbeat be the thunder in the valley. Let your swords be the lightning on the peaks.\"\n—Lothar Lovisason, Balduvian chieftain').
card_multiverse_id('balduvian rage'/'CSP', '121248').

card_in_set('balduvian warlord', 'CSP').
card_original_type('balduvian warlord'/'CSP', 'Creature — Human Barbarian').
card_original_text('balduvian warlord'/'CSP', '{T}: Remove target blocking creature from combat. Creatures it blocked that no other creature blocked this combat become unblocked, then it blocks an attacking creature of your choice. Play this ability only during the declare blockers step.').
card_first_print('balduvian warlord', 'CSP').
card_image_name('balduvian warlord'/'CSP', 'balduvian warlord').
card_uid('balduvian warlord'/'CSP', 'CSP:Balduvian Warlord:balduvian warlord').
card_rarity('balduvian warlord'/'CSP', 'Uncommon').
card_artist('balduvian warlord'/'CSP', 'Paolo Parente').
card_number('balduvian warlord'/'CSP', '77').
card_multiverse_id('balduvian warlord'/'CSP', '122056').

card_in_set('blizzard specter', 'CSP').
card_original_type('blizzard specter'/'CSP', 'Snow Creature — Specter').
card_original_text('blizzard specter'/'CSP', 'Flying\nWhenever Blizzard Specter deals combat damage to a player, choose one That player returns a permanent he or she controls to its owner\'s hand; or that player discards a card.').
card_first_print('blizzard specter', 'CSP').
card_image_name('blizzard specter'/'CSP', 'blizzard specter').
card_uid('blizzard specter'/'CSP', 'CSP:Blizzard Specter:blizzard specter').
card_rarity('blizzard specter'/'CSP', 'Uncommon').
card_artist('blizzard specter'/'CSP', 'Hideaki Takamura').
card_number('blizzard specter'/'CSP', '126').
card_multiverse_id('blizzard specter'/'CSP', '121151').

card_in_set('boreal centaur', 'CSP').
card_original_type('boreal centaur'/'CSP', 'Snow Creature — Centaur Warrior').
card_original_text('boreal centaur'/'CSP', '{S}: Boreal Centaur gets +1/+1 until end of turn. Play this ability only once each turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('boreal centaur', 'CSP').
card_image_name('boreal centaur'/'CSP', 'boreal centaur').
card_uid('boreal centaur'/'CSP', 'CSP:Boreal Centaur:boreal centaur').
card_rarity('boreal centaur'/'CSP', 'Common').
card_artist('boreal centaur'/'CSP', 'Marcelo Vignali').
card_number('boreal centaur'/'CSP', '104').
card_flavor_text('boreal centaur'/'CSP', 'Those who used reindeer as beasts of burden knew to avoid the centaurs, who raged at the enslavement of their spirit-kin.').
card_multiverse_id('boreal centaur'/'CSP', '121200').

card_in_set('boreal druid', 'CSP').
card_original_type('boreal druid'/'CSP', 'Snow Creature — Elf Druid').
card_original_text('boreal druid'/'CSP', '{T}: Add {1} to your mana pool.').
card_first_print('boreal druid', 'CSP').
card_image_name('boreal druid'/'CSP', 'boreal druid').
card_uid('boreal druid'/'CSP', 'CSP:Boreal Druid:boreal druid').
card_rarity('boreal druid'/'CSP', 'Common').
card_artist('boreal druid'/'CSP', 'Dan Dos Santos').
card_number('boreal druid'/'CSP', '105').
card_flavor_text('boreal druid'/'CSP', 'Some creatures are bound to the cold by Rimewind magic. Others seek it out, adapted by two thousand years of the Ice. The Boreal, where Heidar\'s frigid sway is strongest, is home to both.').
card_multiverse_id('boreal druid'/'CSP', '121193').

card_in_set('boreal griffin', 'CSP').
card_original_type('boreal griffin'/'CSP', 'Snow Creature — Griffin').
card_original_text('boreal griffin'/'CSP', 'Flying\n{S}: Boreal Griffin gains first strike until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('boreal griffin', 'CSP').
card_image_name('boreal griffin'/'CSP', 'boreal griffin').
card_uid('boreal griffin'/'CSP', 'CSP:Boreal Griffin:boreal griffin').
card_rarity('boreal griffin'/'CSP', 'Common').
card_artist('boreal griffin'/'CSP', 'Cyril Van Der Haegen').
card_number('boreal griffin'/'CSP', '2').
card_flavor_text('boreal griffin'/'CSP', '\"More than snow falls from the Boreal skies.\"\n—Arkin Egilsson, skycaptain').
card_multiverse_id('boreal griffin'/'CSP', '121244').

card_in_set('boreal shelf', 'CSP').
card_original_type('boreal shelf'/'CSP', 'Snow Land').
card_original_text('boreal shelf'/'CSP', 'Boreal Shelf comes into play tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('boreal shelf', 'CSP').
card_image_name('boreal shelf'/'CSP', 'boreal shelf').
card_uid('boreal shelf'/'CSP', 'CSP:Boreal Shelf:boreal shelf').
card_rarity('boreal shelf'/'CSP', 'Uncommon').
card_artist('boreal shelf'/'CSP', 'Heather Hudson').
card_number('boreal shelf'/'CSP', '144').
card_flavor_text('boreal shelf'/'CSP', '\"Who will stay the Tyrant\'s hand,\nAs ice and woe both grip our land?\nThe helpless feel his frozen brand.\"\n—The Dynasty of Winter Kings').
card_multiverse_id('boreal shelf'/'CSP', '121189').

card_in_set('braid of fire', 'CSP').
card_original_type('braid of fire'/'CSP', 'Enchantment').
card_original_text('braid of fire'/'CSP', 'Cumulative upkeep—Add {R} to your mana pool. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_first_print('braid of fire', 'CSP').
card_image_name('braid of fire'/'CSP', 'braid of fire').
card_uid('braid of fire'/'CSP', 'CSP:Braid of Fire:braid of fire').
card_rarity('braid of fire'/'CSP', 'Rare').
card_artist('braid of fire'/'CSP', 'Cyril Van Der Haegen').
card_number('braid of fire'/'CSP', '78').
card_flavor_text('braid of fire'/'CSP', '\"To a trained mind, the cold is but a momentary distraction.\"').
card_multiverse_id('braid of fire'/'CSP', '122123').

card_in_set('brooding saurian', 'CSP').
card_original_type('brooding saurian'/'CSP', 'Creature — Lizard').
card_original_text('brooding saurian'/'CSP', 'At the end of each turn, each player gains control of all nontoken permanents he or she owns.').
card_first_print('brooding saurian', 'CSP').
card_image_name('brooding saurian'/'CSP', 'brooding saurian').
card_uid('brooding saurian'/'CSP', 'CSP:Brooding Saurian:brooding saurian').
card_rarity('brooding saurian'/'CSP', 'Rare').
card_artist('brooding saurian'/'CSP', 'rk post').
card_number('brooding saurian'/'CSP', '106').
card_flavor_text('brooding saurian'/'CSP', '\"I have been trapped in the lizard\'s lair for days. It seems to regard me as its child and will not let me leave.\"\n—Aevar Borg, northern guide, journal entry').
card_multiverse_id('brooding saurian'/'CSP', '122055').

card_in_set('bull aurochs', 'CSP').
card_original_type('bull aurochs'/'CSP', 'Creature — Aurochs').
card_original_text('bull aurochs'/'CSP', 'Trample\nWhenever Bull Aurochs attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.').
card_first_print('bull aurochs', 'CSP').
card_image_name('bull aurochs'/'CSP', 'bull aurochs').
card_uid('bull aurochs'/'CSP', 'CSP:Bull Aurochs:bull aurochs').
card_rarity('bull aurochs'/'CSP', 'Common').
card_artist('bull aurochs'/'CSP', 'Kev Walker').
card_number('bull aurochs'/'CSP', '107').
card_flavor_text('bull aurochs'/'CSP', 'The aurochs herd is content to graze—until the bull provides the dissenting urge to charge.').
card_multiverse_id('bull aurochs'/'CSP', '121152').

card_in_set('chill to the bone', 'CSP').
card_original_type('chill to the bone'/'CSP', 'Instant').
card_original_text('chill to the bone'/'CSP', 'Destroy target nonsnow creature.').
card_first_print('chill to the bone', 'CSP').
card_image_name('chill to the bone'/'CSP', 'chill to the bone').
card_uid('chill to the bone'/'CSP', 'CSP:Chill to the Bone:chill to the bone').
card_rarity('chill to the bone'/'CSP', 'Common').
card_artist('chill to the bone'/'CSP', 'Jim Murray').
card_number('chill to the bone'/'CSP', '52').
card_flavor_text('chill to the bone'/'CSP', '\"Thick hides and layered armor won\'t help when I freeze you from the inside out.\"\n—Heidar, Rimewind master').
card_multiverse_id('chill to the bone'/'CSP', '121190').

card_in_set('chilling shade', 'CSP').
card_original_type('chilling shade'/'CSP', 'Snow Creature — Shade').
card_original_text('chilling shade'/'CSP', 'Flying\n{S}: Chilling Shade gets +1/+1 until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('chilling shade', 'CSP').
card_image_name('chilling shade'/'CSP', 'chilling shade').
card_uid('chilling shade'/'CSP', 'CSP:Chilling Shade:chilling shade').
card_rarity('chilling shade'/'CSP', 'Common').
card_artist('chilling shade'/'CSP', 'Jeff Nentrup').
card_number('chilling shade'/'CSP', '53').
card_flavor_text('chilling shade'/'CSP', 'Rimewind necromancers caught travelers in mystical blizzards then trapped their spirits in the mist of their final frozen breaths.').
card_multiverse_id('chilling shade'/'CSP', '121258').

card_in_set('coldsteel heart', 'CSP').
card_original_type('coldsteel heart'/'CSP', 'Snow Artifact').
card_original_text('coldsteel heart'/'CSP', 'Coldsteel Heart comes into play tapped.\nAs Coldsteel Heart comes into play, choose a color.\n{T}: Add one mana of the chosen color to your mana pool.').
card_first_print('coldsteel heart', 'CSP').
card_image_name('coldsteel heart'/'CSP', 'coldsteel heart').
card_uid('coldsteel heart'/'CSP', 'CSP:Coldsteel Heart:coldsteel heart').
card_rarity('coldsteel heart'/'CSP', 'Uncommon').
card_artist('coldsteel heart'/'CSP', 'Mark Romanoski').
card_number('coldsteel heart'/'CSP', '136').
card_flavor_text('coldsteel heart'/'CSP', 'The Phyrexian death machine awoke, its coldsteel heart imbuing it with sinister new power.').
card_multiverse_id('coldsteel heart'/'CSP', '121123').

card_in_set('commandeer', 'CSP').
card_original_type('commandeer'/'CSP', 'Instant').
card_original_text('commandeer'/'CSP', 'You may remove two blue cards in your hand from the game rather than pay Commandeer\'s mana cost.\nGain control of target noncreature spell. You may choose new targets for it. (If that spell is an artifact or enchantment, the permanent comes into play under your control.)').
card_first_print('commandeer', 'CSP').
card_image_name('commandeer'/'CSP', 'commandeer').
card_uid('commandeer'/'CSP', 'CSP:Commandeer:commandeer').
card_rarity('commandeer'/'CSP', 'Rare').
card_artist('commandeer'/'CSP', 'John Matson').
card_number('commandeer'/'CSP', '29').
card_multiverse_id('commandeer'/'CSP', '121243').

card_in_set('controvert', 'CSP').
card_original_type('controvert'/'CSP', 'Instant').
card_original_text('controvert'/'CSP', 'Counter target spell.\nRecover {2}{U}{U} (When a creature is put into your graveyard from play, you may pay {2}{U}{U}. If you do, return this card from your graveyard to your hand. Otherwise, remove this card from the game.)').
card_first_print('controvert', 'CSP').
card_image_name('controvert'/'CSP', 'controvert').
card_uid('controvert'/'CSP', 'CSP:Controvert:controvert').
card_rarity('controvert'/'CSP', 'Uncommon').
card_artist('controvert'/'CSP', 'Joel Thomas').
card_number('controvert'/'CSP', '30').
card_multiverse_id('controvert'/'CSP', '122121').

card_in_set('counterbalance', 'CSP').
card_original_type('counterbalance'/'CSP', 'Enchantment').
card_original_text('counterbalance'/'CSP', 'Whenever an opponent plays a spell, you may reveal the top card of your library. If you do, counter that spell if it has the same converted mana cost as the revealed card.').
card_first_print('counterbalance', 'CSP').
card_image_name('counterbalance'/'CSP', 'counterbalance').
card_uid('counterbalance'/'CSP', 'CSP:Counterbalance:counterbalance').
card_rarity('counterbalance'/'CSP', 'Uncommon').
card_artist('counterbalance'/'CSP', 'John Zeleznik').
card_number('counterbalance'/'CSP', '31').
card_multiverse_id('counterbalance'/'CSP', '121159').

card_in_set('cover of winter', 'CSP').
card_original_type('cover of winter'/'CSP', 'Snow Enchantment').
card_original_text('cover of winter'/'CSP', 'Cumulative upkeep {S} ({S} can be paid with one mana from a snow permanent.)\nIf a creature would deal combat damage to you and/or one or more creatures you control, prevent X of that damage, where X is the number of age counters on Cover of Winter.\n{S}: Put an age counter on Cover of Winter.').
card_first_print('cover of winter', 'CSP').
card_image_name('cover of winter'/'CSP', 'cover of winter').
card_uid('cover of winter'/'CSP', 'CSP:Cover of Winter:cover of winter').
card_rarity('cover of winter'/'CSP', 'Rare').
card_artist('cover of winter'/'CSP', 'Wayne Reynolds').
card_number('cover of winter'/'CSP', '3').
card_multiverse_id('cover of winter'/'CSP', '121140').

card_in_set('cryoclasm', 'CSP').
card_original_type('cryoclasm'/'CSP', 'Sorcery').
card_original_text('cryoclasm'/'CSP', 'Destroy target Plains or Island. Cryoclasm deals 3 damage to that land\'s controller.').
card_first_print('cryoclasm', 'CSP').
card_image_name('cryoclasm'/'CSP', 'cryoclasm').
card_uid('cryoclasm'/'CSP', 'CSP:Cryoclasm:cryoclasm').
card_rarity('cryoclasm'/'CSP', 'Uncommon').
card_artist('cryoclasm'/'CSP', 'Zoltan Boros & Gabor Szikszai').
card_number('cryoclasm'/'CSP', '79').
card_flavor_text('cryoclasm'/'CSP', 'The people of Terisiare had come to live on frozen fields as though on solid ground. Nothing reminded them of the difference more clearly than the rifts brought on by the Thaw.').
card_multiverse_id('cryoclasm'/'CSP', '121169').

card_in_set('darien, king of kjeldor', 'CSP').
card_original_type('darien, king of kjeldor'/'CSP', 'Legendary Creature — Human Lord').
card_original_text('darien, king of kjeldor'/'CSP', 'Whenever you\'re dealt damage, you may put that many 1/1 white Soldier creature tokens into play.').
card_first_print('darien, king of kjeldor', 'CSP').
card_image_name('darien, king of kjeldor'/'CSP', 'darien, king of kjeldor').
card_uid('darien, king of kjeldor'/'CSP', 'CSP:Darien, King of Kjeldor:darien, king of kjeldor').
card_rarity('darien, king of kjeldor'/'CSP', 'Rare').
card_artist('darien, king of kjeldor'/'CSP', 'Michael Phillippi').
card_number('darien, king of kjeldor'/'CSP', '4').
card_flavor_text('darien, king of kjeldor'/'CSP', '\"With his dream of unification fulfilled, Darien became the last king of Kjeldor. Those who followed were known as the kings of New Argive.\"\n—Kjeldor: Ice Civilization').
card_multiverse_id('darien, king of kjeldor'/'CSP', '122049').

card_in_set('dark depths', 'CSP').
card_original_type('dark depths'/'CSP', 'Legendary Snow Land').
card_original_text('dark depths'/'CSP', 'Dark Depths comes into play with ten ice counters on it.\n{3}: Remove an ice counter from Dark Depths.\nWhen Dark Depths has no ice counters on it, sacrifice it. If you do, put an indestructible legendary 20/20 black Avatar creature token with flying named Marit Lage into play.').
card_first_print('dark depths', 'CSP').
card_image_name('dark depths'/'CSP', 'dark depths').
card_uid('dark depths'/'CSP', 'CSP:Dark Depths:dark depths').
card_rarity('dark depths'/'CSP', 'Rare').
card_artist('dark depths'/'CSP', 'Stephan Martiniere').
card_number('dark depths'/'CSP', '145').
card_multiverse_id('dark depths'/'CSP', '121155').

card_in_set('deathmark', 'CSP').
card_original_type('deathmark'/'CSP', 'Sorcery').
card_original_text('deathmark'/'CSP', 'Destroy target green or white creature.').
card_first_print('deathmark', 'CSP').
card_image_name('deathmark'/'CSP', 'deathmark').
card_uid('deathmark'/'CSP', 'CSP:Deathmark:deathmark').
card_rarity('deathmark'/'CSP', 'Uncommon').
card_artist('deathmark'/'CSP', 'Jeremy Jarvis').
card_number('deathmark'/'CSP', '54').
card_flavor_text('deathmark'/'CSP', '\"I hope it\'s true that every snowflake is unique, because I never want to see one like this again. Now clean up that body.\"\n—Thangbrand Gyrdsson, Kjeldoran patrol').
card_multiverse_id('deathmark'/'CSP', '121119').

card_in_set('deepfire elemental', 'CSP').
card_original_type('deepfire elemental'/'CSP', 'Creature — Elemental').
card_original_text('deepfire elemental'/'CSP', '{X}{X}{1}: Destroy target artifact or creature with converted mana cost X.').
card_first_print('deepfire elemental', 'CSP').
card_image_name('deepfire elemental'/'CSP', 'deepfire elemental').
card_uid('deepfire elemental'/'CSP', 'CSP:Deepfire Elemental:deepfire elemental').
card_rarity('deepfire elemental'/'CSP', 'Uncommon').
card_artist('deepfire elemental'/'CSP', 'Joel Thomas').
card_number('deepfire elemental'/'CSP', '127').
card_flavor_text('deepfire elemental'/'CSP', '\"There are deep, deep places in this world that have long burned hot with little regard for the whitening of the earth\'s thin skin.\"\n—Sek\'Kuar, Deathkeeper').
card_multiverse_id('deepfire elemental'/'CSP', '122057').

card_in_set('diamond faerie', 'CSP').
card_original_type('diamond faerie'/'CSP', 'Snow Creature — Faerie').
card_original_text('diamond faerie'/'CSP', 'Flying\n{1}{S}: Snow creatures you control get +1/+1 until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('diamond faerie', 'CSP').
card_image_name('diamond faerie'/'CSP', 'diamond faerie').
card_uid('diamond faerie'/'CSP', 'CSP:Diamond Faerie:diamond faerie').
card_rarity('diamond faerie'/'CSP', 'Rare').
card_artist('diamond faerie'/'CSP', 'Heather Hudson').
card_number('diamond faerie'/'CSP', '128').
card_flavor_text('diamond faerie'/'CSP', '\"That such delicate creatures could become so powerful in the embrace of winter is yet more proof that I am right.\"\n—Heidar, Rimewind master').
card_multiverse_id('diamond faerie'/'CSP', '121138').

card_in_set('disciple of tevesh szat', 'CSP').
card_original_type('disciple of tevesh szat'/'CSP', 'Creature — Human Cleric').
card_original_text('disciple of tevesh szat'/'CSP', '{T}: Target creature gets -1/-1 until end of turn.\n{4}{B}{B}, {T}, Sacrifice Disciple of Tevesh Szat: Target creature gets -6/-6 until end of turn.').
card_first_print('disciple of tevesh szat', 'CSP').
card_image_name('disciple of tevesh szat'/'CSP', 'disciple of tevesh szat').
card_uid('disciple of tevesh szat'/'CSP', 'CSP:Disciple of Tevesh Szat:disciple of tevesh szat').
card_rarity('disciple of tevesh szat'/'CSP', 'Common').
card_artist('disciple of tevesh szat'/'CSP', 'Pete Venters').
card_number('disciple of tevesh szat'/'CSP', '55').
card_flavor_text('disciple of tevesh szat'/'CSP', '\"You pray to Freyalise, but she cannot hear your pleas. It is Tevesh Szat who will claim your soul.\"').
card_multiverse_id('disciple of tevesh szat'/'CSP', '121222').

card_in_set('drelnoch', 'CSP').
card_original_type('drelnoch'/'CSP', 'Creature — Yeti Mutant').
card_original_text('drelnoch'/'CSP', 'Whenever Drelnoch becomes blocked, you may draw two cards.').
card_first_print('drelnoch', 'CSP').
card_image_name('drelnoch'/'CSP', 'drelnoch').
card_uid('drelnoch'/'CSP', 'CSP:Drelnoch:drelnoch').
card_rarity('drelnoch'/'CSP', 'Common').
card_artist('drelnoch'/'CSP', 'Zoltan Boros & Gabor Szikszai').
card_number('drelnoch'/'CSP', '32').
card_flavor_text('drelnoch'/'CSP', 'Adarkar\'s blank wastes make the mind wander—and stray thoughts are quickly caught and devoured.').
card_multiverse_id('drelnoch'/'CSP', '121174').

card_in_set('earthen goo', 'CSP').
card_original_type('earthen goo'/'CSP', 'Creature — Ooze').
card_original_text('earthen goo'/'CSP', 'Trample\nCumulative upkeep {R} or {G} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nEarthen Goo gets +1/+1 for each age counter on it.').
card_first_print('earthen goo', 'CSP').
card_image_name('earthen goo'/'CSP', 'earthen goo').
card_uid('earthen goo'/'CSP', 'CSP:Earthen Goo:earthen goo').
card_rarity('earthen goo'/'CSP', 'Uncommon').
card_artist('earthen goo'/'CSP', 'Nick Percival').
card_number('earthen goo'/'CSP', '80').
card_multiverse_id('earthen goo'/'CSP', '121254').

card_in_set('feast of flesh', 'CSP').
card_original_type('feast of flesh'/'CSP', 'Sorcery').
card_original_text('feast of flesh'/'CSP', 'Feast of Flesh deals X damage to target creature and you gain X life, where X is 1 plus the number of cards named Feast of Flesh in all graveyards.').
card_first_print('feast of flesh', 'CSP').
card_image_name('feast of flesh'/'CSP', 'feast of flesh').
card_uid('feast of flesh'/'CSP', 'CSP:Feast of Flesh:feast of flesh').
card_rarity('feast of flesh'/'CSP', 'Common').
card_artist('feast of flesh'/'CSP', 'Volkan Baga').
card_number('feast of flesh'/'CSP', '56').
card_flavor_text('feast of flesh'/'CSP', '\"Starving an army that feeds on its enemies is a sound strategy.\"\n—Garza Zol, plague queen').
card_multiverse_id('feast of flesh'/'CSP', '121137').

card_in_set('field marshal', 'CSP').
card_original_type('field marshal'/'CSP', 'Creature — Human Soldier').
card_original_text('field marshal'/'CSP', 'Other Soldiers get +1/+1 and have first strike.').
card_first_print('field marshal', 'CSP').
card_image_name('field marshal'/'CSP', 'field marshal').
card_uid('field marshal'/'CSP', 'CSP:Field Marshal:field marshal').
card_rarity('field marshal'/'CSP', 'Rare').
card_artist('field marshal'/'CSP', 'Stephen Tappin').
card_number('field marshal'/'CSP', '5').
card_flavor_text('field marshal'/'CSP', 'The Knights of Stromgald struck Darien\'s Roost from two sides. They would have caught Kjeldor in a pincer attack were it not for Darien\'s tacticians.').
card_multiverse_id('field marshal'/'CSP', '121265').

card_in_set('flashfreeze', 'CSP').
card_original_type('flashfreeze'/'CSP', 'Instant').
card_original_text('flashfreeze'/'CSP', 'Counter target red or green spell.').
card_first_print('flashfreeze', 'CSP').
card_image_name('flashfreeze'/'CSP', 'flashfreeze').
card_uid('flashfreeze'/'CSP', 'CSP:Flashfreeze:flashfreeze').
card_rarity('flashfreeze'/'CSP', 'Uncommon').
card_artist('flashfreeze'/'CSP', 'Brian Despain').
card_number('flashfreeze'/'CSP', '33').
card_flavor_text('flashfreeze'/'CSP', '\"Nature? Fire? Bah! Both are chaotic and difficult to control. Ice is structured, latticed, light as a feather, massive as a glacier. In ice, there is power!\"\n—Heidar, Rimewind master').
card_multiverse_id('flashfreeze'/'CSP', '121218').

card_in_set('freyalise\'s radiance', 'CSP').
card_original_type('freyalise\'s radiance'/'CSP', 'Enchantment').
card_original_text('freyalise\'s radiance'/'CSP', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nSnow permanents don\'t untap during their controllers\' untap steps.').
card_first_print('freyalise\'s radiance', 'CSP').
card_image_name('freyalise\'s radiance'/'CSP', 'freyalise\'s radiance').
card_uid('freyalise\'s radiance'/'CSP', 'CSP:Freyalise\'s Radiance:freyalise\'s radiance').
card_rarity('freyalise\'s radiance'/'CSP', 'Uncommon').
card_artist('freyalise\'s radiance'/'CSP', 'Thomas M. Baxa').
card_number('freyalise\'s radiance'/'CSP', '108').
card_multiverse_id('freyalise\'s radiance'/'CSP', '122059').

card_in_set('frost marsh', 'CSP').
card_original_type('frost marsh'/'CSP', 'Snow Land').
card_original_text('frost marsh'/'CSP', 'Frost Marsh comes into play tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('frost marsh', 'CSP').
card_image_name('frost marsh'/'CSP', 'frost marsh').
card_uid('frost marsh'/'CSP', 'CSP:Frost Marsh:frost marsh').
card_rarity('frost marsh'/'CSP', 'Uncommon').
card_artist('frost marsh'/'CSP', 'Jim Pavelec').
card_number('frost marsh'/'CSP', '146').
card_flavor_text('frost marsh'/'CSP', '\"Now caged in Winter\'s bitter chill,\nOur people cry, their voices shrill—\nEternal cold breaks down their will.\"\n—The Dynasty of Winter Kings').
card_multiverse_id('frost marsh'/'CSP', '121212').

card_in_set('frost raptor', 'CSP').
card_original_type('frost raptor'/'CSP', 'Snow Creature — Bird').
card_original_text('frost raptor'/'CSP', 'Flying\n{S}{S}: Frost Raptor can\'t be the target of spells or abilities this turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('frost raptor', 'CSP').
card_image_name('frost raptor'/'CSP', 'frost raptor').
card_uid('frost raptor'/'CSP', 'CSP:Frost Raptor:frost raptor').
card_rarity('frost raptor'/'CSP', 'Common').
card_artist('frost raptor'/'CSP', 'Lars Grant-West').
card_number('frost raptor'/'CSP', '34').
card_flavor_text('frost raptor'/'CSP', 'The Rimewind wizards strove to perpetuate a magical winter. Their unnatural snows birthed unnatural beasts.').
card_multiverse_id('frost raptor'/'CSP', '121132').

card_in_set('frostweb spider', 'CSP').
card_original_type('frostweb spider'/'CSP', 'Snow Creature — Spider').
card_original_text('frostweb spider'/'CSP', 'Frostweb Spider can block as though it had flying.\nWhenever Frostweb Spider blocks a creature with flying, put a +1/+1 counter on Frostweb Spider at end of combat.').
card_first_print('frostweb spider', 'CSP').
card_image_name('frostweb spider'/'CSP', 'frostweb spider').
card_uid('frostweb spider'/'CSP', 'CSP:Frostweb Spider:frostweb spider').
card_rarity('frostweb spider'/'CSP', 'Common').
card_artist('frostweb spider'/'CSP', 'Greg Hildebrandt').
card_number('frostweb spider'/'CSP', '109').
card_flavor_text('frostweb spider'/'CSP', 'Its white webs glitter, mimicking the frost on the tree\'s branches.').
card_multiverse_id('frostweb spider'/'CSP', '121197').

card_in_set('frozen solid', 'CSP').
card_original_type('frozen solid'/'CSP', 'Enchantment — Aura').
card_original_text('frozen solid'/'CSP', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nWhen damage is dealt to enchanted creature, destroy it.').
card_image_name('frozen solid'/'CSP', 'frozen solid').
card_uid('frozen solid'/'CSP', 'CSP:Frozen Solid:frozen solid').
card_rarity('frozen solid'/'CSP', 'Common').
card_artist('frozen solid'/'CSP', 'Ralph Horsley').
card_number('frozen solid'/'CSP', '35').
card_flavor_text('frozen solid'/'CSP', '\"Guard, fetch me a mallet.\"\n—Heidar, Rimewind master').
card_multiverse_id('frozen solid'/'CSP', '121228').

card_in_set('fury of the horde', 'CSP').
card_original_type('fury of the horde'/'CSP', 'Sorcery').
card_original_text('fury of the horde'/'CSP', 'You may remove two red cards in your hand from the game rather than pay Fury of the Horde\'s mana cost.\nUntap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_first_print('fury of the horde', 'CSP').
card_image_name('fury of the horde'/'CSP', 'fury of the horde').
card_uid('fury of the horde'/'CSP', 'CSP:Fury of the Horde:fury of the horde').
card_rarity('fury of the horde'/'CSP', 'Rare').
card_artist('fury of the horde'/'CSP', 'Stephen Tappin').
card_number('fury of the horde'/'CSP', '81').
card_multiverse_id('fury of the horde'/'CSP', '121181').

card_in_set('garza zol, plague queen', 'CSP').
card_original_type('garza zol, plague queen'/'CSP', 'Legendary Creature — Vampire').
card_original_text('garza zol, plague queen'/'CSP', 'Flying, haste\nWhenever a creature dealt damage by Garza Zol, Plague Queen this turn is put into a graveyard, put a +1/+1 counter on Garza Zol.\nWhenever Garza Zol deals combat damage to a player, you may draw a card.').
card_first_print('garza zol, plague queen', 'CSP').
card_image_name('garza zol, plague queen'/'CSP', 'garza zol, plague queen').
card_uid('garza zol, plague queen'/'CSP', 'CSP:Garza Zol, Plague Queen:garza zol, plague queen').
card_rarity('garza zol, plague queen'/'CSP', 'Rare').
card_artist('garza zol, plague queen'/'CSP', 'Darrell Riche').
card_number('garza zol, plague queen'/'CSP', '129').
card_multiverse_id('garza zol, plague queen'/'CSP', '121126').

card_in_set('garza\'s assassin', 'CSP').
card_original_type('garza\'s assassin'/'CSP', 'Creature — Human Assassin').
card_original_text('garza\'s assassin'/'CSP', 'Sacrifice Garza\'s Assassin: Destroy target nonblack creature.\nRecover—Pay half your life, rounded up. (When another creature is put into your graveyard from play, you may pay half your life, rounded up. If you do, return this card from your graveyard to your hand. Otherwise, remove this card from the game.)').
card_first_print('garza\'s assassin', 'CSP').
card_image_name('garza\'s assassin'/'CSP', 'garza\'s assassin').
card_uid('garza\'s assassin'/'CSP', 'CSP:Garza\'s Assassin:garza\'s assassin').
card_rarity('garza\'s assassin'/'CSP', 'Rare').
card_artist('garza\'s assassin'/'CSP', 'Paolo Parente').
card_number('garza\'s assassin'/'CSP', '57').
card_multiverse_id('garza\'s assassin'/'CSP', '122051').

card_in_set('gelid shackles', 'CSP').
card_original_type('gelid shackles'/'CSP', 'Snow Enchantment — Aura').
card_original_text('gelid shackles'/'CSP', 'Enchant creature\nEnchanted creature can\'t block and its activated abilities can\'t be played.\n{S}: Enchanted creature gains defender until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('gelid shackles', 'CSP').
card_image_name('gelid shackles'/'CSP', 'gelid shackles').
card_uid('gelid shackles'/'CSP', 'CSP:Gelid Shackles:gelid shackles').
card_rarity('gelid shackles'/'CSP', 'Common').
card_artist('gelid shackles'/'CSP', 'Alex Horley-Orlandelli').
card_number('gelid shackles'/'CSP', '6').
card_multiverse_id('gelid shackles'/'CSP', '121213').

card_in_set('glacial plating', 'CSP').
card_original_type('glacial plating'/'CSP', 'Snow Enchantment — Aura').
card_original_text('glacial plating'/'CSP', 'Enchant creature\nCumulative upkeep {S} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it. {S} can be paid with one mana from a snow permanent.)\nEnchanted creature gets +3/+3 for each age counter on Glacial Plating.').
card_first_print('glacial plating', 'CSP').
card_image_name('glacial plating'/'CSP', 'glacial plating').
card_uid('glacial plating'/'CSP', 'CSP:Glacial Plating:glacial plating').
card_rarity('glacial plating'/'CSP', 'Uncommon').
card_artist('glacial plating'/'CSP', 'Brian Despain').
card_number('glacial plating'/'CSP', '7').
card_multiverse_id('glacial plating'/'CSP', '121211').

card_in_set('goblin furrier', 'CSP').
card_original_type('goblin furrier'/'CSP', 'Creature — Goblin Warrior').
card_original_text('goblin furrier'/'CSP', 'Prevent all damage that Goblin Furrier would deal to snow creatures.').
card_first_print('goblin furrier', 'CSP').
card_image_name('goblin furrier'/'CSP', 'goblin furrier').
card_uid('goblin furrier'/'CSP', 'CSP:Goblin Furrier:goblin furrier').
card_rarity('goblin furrier'/'CSP', 'Common').
card_artist('goblin furrier'/'CSP', 'Warren Mahy').
card_number('goblin furrier'/'CSP', '82').
card_flavor_text('goblin furrier'/'CSP', 'Clambering up and over the powdery slope, the goblin chanced upon a small furry thing. There they regarded each other: the goblin and his new pair of earmuffs.').
card_multiverse_id('goblin furrier'/'CSP', '121185').

card_in_set('goblin rimerunner', 'CSP').
card_original_type('goblin rimerunner'/'CSP', 'Snow Creature — Goblin Warrior').
card_original_text('goblin rimerunner'/'CSP', '{T}: Target creature can\'t block this turn.\n{S}: Goblin Rimerunner gains haste until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('goblin rimerunner', 'CSP').
card_image_name('goblin rimerunner'/'CSP', 'goblin rimerunner').
card_uid('goblin rimerunner'/'CSP', 'CSP:Goblin Rimerunner:goblin rimerunner').
card_rarity('goblin rimerunner'/'CSP', 'Common').
card_artist('goblin rimerunner'/'CSP', 'Christopher Rush').
card_number('goblin rimerunner'/'CSP', '83').
card_flavor_text('goblin rimerunner'/'CSP', 'Rimerunners set off avalanches to immobilize foes as well as to create more challenging slopes.').
card_multiverse_id('goblin rimerunner'/'CSP', '121219').

card_in_set('greater stone spirit', 'CSP').
card_original_type('greater stone spirit'/'CSP', 'Creature — Elemental Spirit').
card_original_text('greater stone spirit'/'CSP', 'Greater Stone Spirit can\'t be blocked by creatures with flying.\n{2}{R}: Until end of turn, target creature gets +0/+2 and gains \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('greater stone spirit', 'CSP').
card_image_name('greater stone spirit'/'CSP', 'greater stone spirit').
card_uid('greater stone spirit'/'CSP', 'CSP:Greater Stone Spirit:greater stone spirit').
card_rarity('greater stone spirit'/'CSP', 'Uncommon').
card_artist('greater stone spirit'/'CSP', 'Yokota Katsumi').
card_number('greater stone spirit'/'CSP', '84').
card_flavor_text('greater stone spirit'/'CSP', 'Having charted their way up the difficult face, the two mountaineers had to contend with the mountain\'s decision to stand up.').
card_multiverse_id('greater stone spirit'/'CSP', '121208').

card_in_set('grim harvest', 'CSP').
card_original_type('grim harvest'/'CSP', 'Instant').
card_original_text('grim harvest'/'CSP', 'Return target creature card from your graveyard to your hand.\nRecover {2}{B} (When a creature is put into your graveyard from play, you may pay {2}{B}. If you do, return this card from your graveyard to your hand. Otherwise, remove this card from the game.)').
card_first_print('grim harvest', 'CSP').
card_image_name('grim harvest'/'CSP', 'grim harvest').
card_uid('grim harvest'/'CSP', 'CSP:Grim Harvest:grim harvest').
card_rarity('grim harvest'/'CSP', 'Common').
card_artist('grim harvest'/'CSP', 'Zoltan Boros & Gabor Szikszai').
card_number('grim harvest'/'CSP', '58').
card_multiverse_id('grim harvest'/'CSP', '122114').

card_in_set('gristle grinner', 'CSP').
card_original_type('gristle grinner'/'CSP', 'Creature — Zombie').
card_original_text('gristle grinner'/'CSP', 'Whenever a creature is put into a graveyard from play, Gristle Grinner gets +2/+2 until end of turn.').
card_first_print('gristle grinner', 'CSP').
card_image_name('gristle grinner'/'CSP', 'gristle grinner').
card_uid('gristle grinner'/'CSP', 'CSP:Gristle Grinner:gristle grinner').
card_rarity('gristle grinner'/'CSP', 'Uncommon').
card_artist('gristle grinner'/'CSP', 'Dave Allsop').
card_number('gristle grinner'/'CSP', '59').
card_flavor_text('gristle grinner'/'CSP', '\"There it was, clawing the ice as it chittered and clicked its teeth. We fled in horror before it unearthed its grisly meal.\"\n—Aevar Borg, northern guide, journal entry').
card_multiverse_id('gristle grinner'/'CSP', '121133').

card_in_set('gutless ghoul', 'CSP').
card_original_type('gutless ghoul'/'CSP', 'Snow Creature — Zombie').
card_original_text('gutless ghoul'/'CSP', '{1}, Sacrifice a creature: You gain 2 life.').
card_first_print('gutless ghoul', 'CSP').
card_image_name('gutless ghoul'/'CSP', 'gutless ghoul').
card_uid('gutless ghoul'/'CSP', 'CSP:Gutless Ghoul:gutless ghoul').
card_rarity('gutless ghoul'/'CSP', 'Common').
card_artist('gutless ghoul'/'CSP', 'Kensuke Okabayashi').
card_number('gutless ghoul'/'CSP', '60').
card_flavor_text('gutless ghoul'/'CSP', '\"Make sure those wretches feed only upon the plagued. The blood of the healthy is reserved for me alone.\"\n—Garza Zol, plague queen').
card_multiverse_id('gutless ghoul'/'CSP', '121194').

card_in_set('haakon, stromgald scourge', 'CSP').
card_original_type('haakon, stromgald scourge'/'CSP', 'Legendary Creature — Zombie Knight').
card_original_text('haakon, stromgald scourge'/'CSP', 'You may play Haakon, Stromgald Scourge from your graveyard, but not from anywhere else.\nAs long as Haakon is in play, you may play Knight cards from your graveyard.\nWhen Haakon is put into a graveyard from play, you lose 2 life.').
card_first_print('haakon, stromgald scourge', 'CSP').
card_image_name('haakon, stromgald scourge'/'CSP', 'haakon, stromgald scourge').
card_uid('haakon, stromgald scourge'/'CSP', 'CSP:Haakon, Stromgald Scourge:haakon, stromgald scourge').
card_rarity('haakon, stromgald scourge'/'CSP', 'Rare').
card_artist('haakon, stromgald scourge'/'CSP', 'Mark Zug').
card_number('haakon, stromgald scourge'/'CSP', '61').
card_multiverse_id('haakon, stromgald scourge'/'CSP', '122045').

card_in_set('heidar, rimewind master', 'CSP').
card_original_type('heidar, rimewind master'/'CSP', 'Legendary Creature — Human Wizard').
card_original_text('heidar, rimewind master'/'CSP', '{2}, {T}: Return target permanent to its owner\'s hand. Play this ability only if you control four or more snow permanents.').
card_first_print('heidar, rimewind master', 'CSP').
card_image_name('heidar, rimewind master'/'CSP', 'heidar, rimewind master').
card_uid('heidar, rimewind master'/'CSP', 'CSP:Heidar, Rimewind Master:heidar, rimewind master').
card_rarity('heidar, rimewind master'/'CSP', 'Rare').
card_artist('heidar, rimewind master'/'CSP', 'Ron Spears').
card_number('heidar, rimewind master'/'CSP', '36').
card_flavor_text('heidar, rimewind master'/'CSP', '\"The umber stain of the Thaw will be bleached away. Once again the world will be sharp, white, perfect!\"').
card_multiverse_id('heidar, rimewind master'/'CSP', '121147').

card_in_set('herald of leshrac', 'CSP').
card_original_type('herald of leshrac'/'CSP', 'Creature — Avatar').
card_original_text('herald of leshrac'/'CSP', 'Flying\nCumulative upkeep—Gain control of a land you don\'t control.\nHerald of Leshrac gets +1/+1 for each land you control but don\'t own.\nWhen Herald of Leshrac leaves play, each player gains control of each land he or she owns that you control.').
card_first_print('herald of leshrac', 'CSP').
card_image_name('herald of leshrac'/'CSP', 'herald of leshrac').
card_uid('herald of leshrac'/'CSP', 'CSP:Herald of Leshrac:herald of leshrac').
card_rarity('herald of leshrac'/'CSP', 'Rare').
card_artist('herald of leshrac'/'CSP', 'Alex Horley-Orlandelli').
card_number('herald of leshrac'/'CSP', '62').
card_multiverse_id('herald of leshrac'/'CSP', '121250').

card_in_set('hibernation\'s end', 'CSP').
card_original_type('hibernation\'s end'/'CSP', 'Enchantment').
card_original_text('hibernation\'s end'/'CSP', 'Cumulative upkeep {1}\nWhenever you pay Hibernation\'s End\'s cumulative upkeep, you may search your library for a creature card with converted mana cost equal to the number of age counters on Hibernation\'s End and put it into play. If you do, shuffle your library.').
card_first_print('hibernation\'s end', 'CSP').
card_image_name('hibernation\'s end'/'CSP', 'hibernation\'s end').
card_uid('hibernation\'s end'/'CSP', 'CSP:Hibernation\'s End:hibernation\'s end').
card_rarity('hibernation\'s end'/'CSP', 'Rare').
card_artist('hibernation\'s end'/'CSP', 'Steven Belledin').
card_number('hibernation\'s end'/'CSP', '110').
card_multiverse_id('hibernation\'s end'/'CSP', '121139').

card_in_set('highland weald', 'CSP').
card_original_type('highland weald'/'CSP', 'Snow Land').
card_original_text('highland weald'/'CSP', 'Highland Weald comes into play tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('highland weald', 'CSP').
card_image_name('highland weald'/'CSP', 'highland weald').
card_uid('highland weald'/'CSP', 'CSP:Highland Weald:highland weald').
card_rarity('highland weald'/'CSP', 'Uncommon').
card_artist('highland weald'/'CSP', 'John Avon').
card_number('highland weald'/'CSP', '147').
card_flavor_text('highland weald'/'CSP', '\"Our noble land, enslaved by Rime,\nIs it our fate to freeze through time?\nIf this be true, oh wicked crime.\"\n—The Dynasty of Winter Kings').
card_multiverse_id('highland weald'/'CSP', '121259').

card_in_set('icefall', 'CSP').
card_original_type('icefall'/'CSP', 'Sorcery').
card_original_text('icefall'/'CSP', 'Destroy target artifact or land.\nRecover {R}{R} (When a creature is put into your graveyard from play, you may pay {R}{R}. If you do, return this card from your graveyard to your hand. Otherwise, remove this card from the game.)').
card_first_print('icefall', 'CSP').
card_image_name('icefall'/'CSP', 'icefall').
card_uid('icefall'/'CSP', 'CSP:Icefall:icefall').
card_rarity('icefall'/'CSP', 'Common').
card_artist('icefall'/'CSP', 'Warren Mahy').
card_number('icefall'/'CSP', '85').
card_flavor_text('icefall'/'CSP', 'As the Thaw met Terisiare, mountains shed their icy skins, crushing homes and hopes alike.').
card_multiverse_id('icefall'/'CSP', '121143').

card_in_set('into the north', 'CSP').
card_original_type('into the north'/'CSP', 'Sorcery').
card_original_text('into the north'/'CSP', 'Search your library for a snow land card and put it into play tapped. Then shuffle your library.').
card_first_print('into the north', 'CSP').
card_image_name('into the north'/'CSP', 'into the north').
card_uid('into the north'/'CSP', 'CSP:Into the North:into the north').
card_rarity('into the north'/'CSP', 'Common').
card_artist('into the north'/'CSP', 'Richard Sardinha').
card_number('into the north'/'CSP', '111').
card_flavor_text('into the north'/'CSP', '\"Our border lands fall under the grip of Heidar\'s second winter. Go forth and reclaim them for Yavimaya.\"\n—Kaysa, elder druid of the Juniper Order').
card_multiverse_id('into the north'/'CSP', '121199').

card_in_set('jester\'s scepter', 'CSP').
card_original_type('jester\'s scepter'/'CSP', 'Artifact').
card_original_text('jester\'s scepter'/'CSP', 'When Jester\'s Scepter comes into play, remove the top five cards of target player\'s library from the game face down. You may look at those cards as long as they remain removed from the game.\n{2}, {T}, Put a card removed from the game with Jester\'s Scepter into its owner\'s graveyard: Counter target spell if it has the same name as that card.').
card_first_print('jester\'s scepter', 'CSP').
card_image_name('jester\'s scepter'/'CSP', 'jester\'s scepter').
card_uid('jester\'s scepter'/'CSP', 'CSP:Jester\'s Scepter:jester\'s scepter').
card_rarity('jester\'s scepter'/'CSP', 'Rare').
card_artist('jester\'s scepter'/'CSP', 'Matt Cavotta').
card_number('jester\'s scepter'/'CSP', '137').
card_multiverse_id('jester\'s scepter'/'CSP', '122066').

card_in_set('jokulmorder', 'CSP').
card_original_type('jokulmorder'/'CSP', 'Creature — Leviathan').
card_original_text('jokulmorder'/'CSP', 'Trample\nJokulmorder comes into play tapped.\nWhen Jokulmorder comes into play, sacrifice it unless you sacrifice five lands.\nJokulmorder doesn\'t untap during your untap step.\nWhenever you play an Island, you may untap Jokulmorder.').
card_first_print('jokulmorder', 'CSP').
card_image_name('jokulmorder'/'CSP', 'jokulmorder').
card_uid('jokulmorder'/'CSP', 'CSP:Jokulmorder:jokulmorder').
card_rarity('jokulmorder'/'CSP', 'Rare').
card_artist('jokulmorder'/'CSP', 'Mark Zug').
card_number('jokulmorder'/'CSP', '37').
card_multiverse_id('jokulmorder'/'CSP', '121198').

card_in_set('jötun grunt', 'CSP').
card_original_type('jötun grunt'/'CSP', 'Creature — Giant Soldier').
card_original_text('jötun grunt'/'CSP', 'Cumulative upkeep—Put two cards in a single graveyard on the bottom of their owner\'s library. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_first_print('jötun grunt', 'CSP').
card_image_name('jötun grunt'/'CSP', 'jotun grunt').
card_uid('jötun grunt'/'CSP', 'CSP:Jötun Grunt:jotun grunt').
card_rarity('jötun grunt'/'CSP', 'Uncommon').
card_artist('jötun grunt'/'CSP', 'Franz Vohwinkel').
card_number('jötun grunt'/'CSP', '8').
card_multiverse_id('jötun grunt'/'CSP', '122075').

card_in_set('jötun owl keeper', 'CSP').
card_original_type('jötun owl keeper'/'CSP', 'Creature — Giant').
card_original_text('jötun owl keeper'/'CSP', 'Cumulative upkeep {W} or {U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Jötun Owl Keeper is put into a graveyard from play, put a 1/1 white Bird creature token with flying into play for each age counter on it.').
card_first_print('jötun owl keeper', 'CSP').
card_image_name('jötun owl keeper'/'CSP', 'jotun owl keeper').
card_uid('jötun owl keeper'/'CSP', 'CSP:Jötun Owl Keeper:jotun owl keeper').
card_rarity('jötun owl keeper'/'CSP', 'Uncommon').
card_artist('jötun owl keeper'/'CSP', 'Dave Dorman').
card_number('jötun owl keeper'/'CSP', '9').
card_multiverse_id('jötun owl keeper'/'CSP', '121236').

card_in_set('juniper order ranger', 'CSP').
card_original_type('juniper order ranger'/'CSP', 'Creature — Human Knight').
card_original_text('juniper order ranger'/'CSP', 'Whenever another creature comes into play under your control, put a +1/+1 counter on that creature and a +1/+1 counter on Juniper Order Ranger.').
card_first_print('juniper order ranger', 'CSP').
card_image_name('juniper order ranger'/'CSP', 'juniper order ranger').
card_uid('juniper order ranger'/'CSP', 'CSP:Juniper Order Ranger:juniper order ranger').
card_rarity('juniper order ranger'/'CSP', 'Uncommon').
card_artist('juniper order ranger'/'CSP', 'Greg Hildebrandt').
card_number('juniper order ranger'/'CSP', '130').
card_flavor_text('juniper order ranger'/'CSP', 'To protect the alliance between Kjeldor and Yavimaya, they trained in the ways of both.').
card_multiverse_id('juniper order ranger'/'CSP', '121172').

card_in_set('karplusan minotaur', 'CSP').
card_original_type('karplusan minotaur'/'CSP', 'Creature — Minotaur Warrior').
card_original_text('karplusan minotaur'/'CSP', 'Cumulative upkeep—Flip a coin.\nWhenever you win a coin flip, Karplusan Minotaur deals 1 damage to target creature or player.\nWhenever you lose a coin flip, Karplusan Minotaur deals 1 damage to target creature or player of an opponent\'s choice.').
card_first_print('karplusan minotaur', 'CSP').
card_image_name('karplusan minotaur'/'CSP', 'karplusan minotaur').
card_uid('karplusan minotaur'/'CSP', 'CSP:Karplusan Minotaur:karplusan minotaur').
card_rarity('karplusan minotaur'/'CSP', 'Rare').
card_artist('karplusan minotaur'/'CSP', 'Wayne England').
card_number('karplusan minotaur'/'CSP', '86').
card_multiverse_id('karplusan minotaur'/'CSP', '122070').

card_in_set('karplusan strider', 'CSP').
card_original_type('karplusan strider'/'CSP', 'Creature — Yeti').
card_original_text('karplusan strider'/'CSP', 'Karplusan Strider can\'t be the target of blue or black spells.').
card_first_print('karplusan strider', 'CSP').
card_image_name('karplusan strider'/'CSP', 'karplusan strider').
card_uid('karplusan strider'/'CSP', 'CSP:Karplusan Strider:karplusan strider').
card_rarity('karplusan strider'/'CSP', 'Uncommon').
card_artist('karplusan strider'/'CSP', 'Dan Scott').
card_number('karplusan strider'/'CSP', '112').
card_flavor_text('karplusan strider'/'CSP', 'The strider\'s long, loping gait is an adaptation that allows it to move quickly in deep snow.').
card_multiverse_id('karplusan strider'/'CSP', '121214').

card_in_set('karplusan wolverine', 'CSP').
card_original_type('karplusan wolverine'/'CSP', 'Snow Creature — Beast').
card_original_text('karplusan wolverine'/'CSP', 'Whenever Karplusan Wolverine becomes blocked, you may have it deal 1 damage to target creature or player.').
card_first_print('karplusan wolverine', 'CSP').
card_image_name('karplusan wolverine'/'CSP', 'karplusan wolverine').
card_uid('karplusan wolverine'/'CSP', 'CSP:Karplusan Wolverine:karplusan wolverine').
card_rarity('karplusan wolverine'/'CSP', 'Common').
card_artist('karplusan wolverine'/'CSP', 'Greg Hildebrandt').
card_number('karplusan wolverine'/'CSP', '87').
card_flavor_text('karplusan wolverine'/'CSP', '\"They\'re not easily trained. Fortunately, they already know how to kill.\"\n—Lovisa Coldeyes, Balduvian chieftain').
card_multiverse_id('karplusan wolverine'/'CSP', '121179').

card_in_set('kjeldoran gargoyle', 'CSP').
card_original_type('kjeldoran gargoyle'/'CSP', 'Creature — Gargoyle').
card_original_text('kjeldoran gargoyle'/'CSP', 'Flying, first strike\nWhenever Kjeldoran Gargoyle deals damage, you gain that much life.').
card_first_print('kjeldoran gargoyle', 'CSP').
card_image_name('kjeldoran gargoyle'/'CSP', 'kjeldoran gargoyle').
card_uid('kjeldoran gargoyle'/'CSP', 'CSP:Kjeldoran Gargoyle:kjeldoran gargoyle').
card_rarity('kjeldoran gargoyle'/'CSP', 'Uncommon').
card_artist('kjeldoran gargoyle'/'CSP', 'Marcelo Vignali').
card_number('kjeldoran gargoyle'/'CSP', '10').
card_flavor_text('kjeldoran gargoyle'/'CSP', 'The gargoyles were taken from the walls of flooded Kjeldoran cities and brought upland to guard the outposts of New Argive.').
card_multiverse_id('kjeldoran gargoyle'/'CSP', '121207').

card_in_set('kjeldoran javelineer', 'CSP').
card_original_type('kjeldoran javelineer'/'CSP', 'Creature — Human Soldier').
card_original_text('kjeldoran javelineer'/'CSP', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\n{T}: Kjeldoran Javelineer deals damage to target attacking or blocking creature equal to the number of age counters on Kjeldoran Javelineer.').
card_first_print('kjeldoran javelineer', 'CSP').
card_image_name('kjeldoran javelineer'/'CSP', 'kjeldoran javelineer').
card_uid('kjeldoran javelineer'/'CSP', 'CSP:Kjeldoran Javelineer:kjeldoran javelineer').
card_rarity('kjeldoran javelineer'/'CSP', 'Common').
card_artist('kjeldoran javelineer'/'CSP', 'Dave Dorman').
card_number('kjeldoran javelineer'/'CSP', '11').
card_multiverse_id('kjeldoran javelineer'/'CSP', '121235').

card_in_set('kjeldoran outrider', 'CSP').
card_original_type('kjeldoran outrider'/'CSP', 'Creature — Human Soldier').
card_original_text('kjeldoran outrider'/'CSP', '{W}: Kjeldoran Outrider gets +0/+1 until end of turn.').
card_first_print('kjeldoran outrider', 'CSP').
card_image_name('kjeldoran outrider'/'CSP', 'kjeldoran outrider').
card_uid('kjeldoran outrider'/'CSP', 'CSP:Kjeldoran Outrider:kjeldoran outrider').
card_rarity('kjeldoran outrider'/'CSP', 'Common').
card_artist('kjeldoran outrider'/'CSP', 'Carl Critchlow').
card_number('kjeldoran outrider'/'CSP', '12').
card_flavor_text('kjeldoran outrider'/'CSP', '\"I listen to people. He sniffs the wind. And we both sense something big coming. Something very big.\"').
card_multiverse_id('kjeldoran outrider'/'CSP', '121188').

card_in_set('kjeldoran war cry', 'CSP').
card_original_type('kjeldoran war cry'/'CSP', 'Instant').
card_original_text('kjeldoran war cry'/'CSP', 'Creatures you control get +X/+X until end of turn, where X is 1 plus the number of cards named Kjeldoran War Cry in all graveyards.').
card_first_print('kjeldoran war cry', 'CSP').
card_image_name('kjeldoran war cry'/'CSP', 'kjeldoran war cry').
card_uid('kjeldoran war cry'/'CSP', 'CSP:Kjeldoran War Cry:kjeldoran war cry').
card_rarity('kjeldoran war cry'/'CSP', 'Common').
card_artist('kjeldoran war cry'/'CSP', 'Michael Phillippi').
card_number('kjeldoran war cry'/'CSP', '13').
card_flavor_text('kjeldoran war cry'/'CSP', 'King Darien\'s breath crystallized and fell to the frozen battlefield, but its command carried true to all soldiers of New Argive.').
card_multiverse_id('kjeldoran war cry'/'CSP', '121148').

card_in_set('krovikan mist', 'CSP').
card_original_type('krovikan mist'/'CSP', 'Creature — Illusion').
card_original_text('krovikan mist'/'CSP', 'Flying\nKrovikan Mist\'s power and toughness are each equal to the number of Illusions in play.').
card_first_print('krovikan mist', 'CSP').
card_image_name('krovikan mist'/'CSP', 'krovikan mist').
card_uid('krovikan mist'/'CSP', 'CSP:Krovikan Mist:krovikan mist').
card_rarity('krovikan mist'/'CSP', 'Common').
card_artist('krovikan mist'/'CSP', 'Jeremy Jarvis').
card_number('krovikan mist'/'CSP', '38').
card_flavor_text('krovikan mist'/'CSP', 'Plague-induced dreams became real as they swirled above Krov, mingling with the magical residue of the World Spell.').
card_multiverse_id('krovikan mist'/'CSP', '122047').

card_in_set('krovikan rot', 'CSP').
card_original_type('krovikan rot'/'CSP', 'Instant').
card_original_text('krovikan rot'/'CSP', 'Destroy target creature with power 2 or less.\nRecover {1}{B}{B} (When a creature is put into your graveyard from play, you may pay {1}{B}{B}. If you do, return this card from your graveyard to your hand. Otherwise, remove this card from the game.)').
card_first_print('krovikan rot', 'CSP').
card_image_name('krovikan rot'/'CSP', 'krovikan rot').
card_uid('krovikan rot'/'CSP', 'CSP:Krovikan Rot:krovikan rot').
card_rarity('krovikan rot'/'CSP', 'Uncommon').
card_artist('krovikan rot'/'CSP', 'Michael Sutfin').
card_number('krovikan rot'/'CSP', '63').
card_multiverse_id('krovikan rot'/'CSP', '122050').

card_in_set('krovikan scoundrel', 'CSP').
card_original_type('krovikan scoundrel'/'CSP', 'Creature — Human Rogue').
card_original_text('krovikan scoundrel'/'CSP', '').
card_first_print('krovikan scoundrel', 'CSP').
card_image_name('krovikan scoundrel'/'CSP', 'krovikan scoundrel').
card_uid('krovikan scoundrel'/'CSP', 'CSP:Krovikan Scoundrel:krovikan scoundrel').
card_rarity('krovikan scoundrel'/'CSP', 'Common').
card_artist('krovikan scoundrel'/'CSP', 'Ralph Horsley').
card_number('krovikan scoundrel'/'CSP', '64').
card_flavor_text('krovikan scoundrel'/'CSP', 'The few surviving humans of Krov would have welcomed the return of winter and its sterilizing cold. They often peered northward, hoping that the snow\'s edge had reached their infested city-tomb.').
card_multiverse_id('krovikan scoundrel'/'CSP', '121226').

card_in_set('krovikan whispers', 'CSP').
card_original_type('krovikan whispers'/'CSP', 'Enchantment — Aura').
card_original_text('krovikan whispers'/'CSP', 'Enchant creature\nCumulative upkeep {U} or {B}\nYou control enchanted creature.\nWhen Krovikan Whispers is put into a graveyard from play, you lose 2 life for each age counter on it.').
card_first_print('krovikan whispers', 'CSP').
card_image_name('krovikan whispers'/'CSP', 'krovikan whispers').
card_uid('krovikan whispers'/'CSP', 'CSP:Krovikan Whispers:krovikan whispers').
card_rarity('krovikan whispers'/'CSP', 'Uncommon').
card_artist('krovikan whispers'/'CSP', 'Nick Percival').
card_number('krovikan whispers'/'CSP', '39').
card_multiverse_id('krovikan whispers'/'CSP', '121493').

card_in_set('lightning serpent', 'CSP').
card_original_type('lightning serpent'/'CSP', 'Creature — Elemental Serpent').
card_original_text('lightning serpent'/'CSP', 'Trample, haste\nLightning Serpent comes into play with X +1/+0 counters on it.\nAt end of turn, sacrifice Lightning Serpent.').
card_first_print('lightning serpent', 'CSP').
card_image_name('lightning serpent'/'CSP', 'lightning serpent').
card_uid('lightning serpent'/'CSP', 'CSP:Lightning Serpent:lightning serpent').
card_rarity('lightning serpent'/'CSP', 'Rare').
card_artist('lightning serpent'/'CSP', 'John Avon').
card_number('lightning serpent'/'CSP', '88').
card_flavor_text('lightning serpent'/'CSP', 'Ice exploded into steam in its wake, hissing like a thousand serpents hailing their blazing master.').
card_multiverse_id('lightning serpent'/'CSP', '121220').

card_in_set('lightning storm', 'CSP').
card_original_type('lightning storm'/'CSP', 'Instant').
card_original_text('lightning storm'/'CSP', 'Lightning Storm deals X damage to target creature or player, where X is 3 plus the number of charge counters on it.\nDiscard a land card: Put two charge counters on Lightning Storm. You may choose a new target for it. Any player may play this ability but only if Lightning Storm is on the stack.').
card_first_print('lightning storm', 'CSP').
card_image_name('lightning storm'/'CSP', 'lightning storm').
card_uid('lightning storm'/'CSP', 'CSP:Lightning Storm:lightning storm').
card_rarity('lightning storm'/'CSP', 'Uncommon').
card_artist('lightning storm'/'CSP', 'Luca Zontini').
card_number('lightning storm'/'CSP', '89').
card_multiverse_id('lightning storm'/'CSP', '121495').

card_in_set('lovisa coldeyes', 'CSP').
card_original_type('lovisa coldeyes'/'CSP', 'Legendary Creature — Human Lord').
card_original_text('lovisa coldeyes'/'CSP', 'Barbarians, Warriors, and Berserkers get +2/+2 and have haste.').
card_first_print('lovisa coldeyes', 'CSP').
card_image_name('lovisa coldeyes'/'CSP', 'lovisa coldeyes').
card_uid('lovisa coldeyes'/'CSP', 'CSP:Lovisa Coldeyes:lovisa coldeyes').
card_rarity('lovisa coldeyes'/'CSP', 'Rare').
card_artist('lovisa coldeyes'/'CSP', 'Brian Snõddy').
card_number('lovisa coldeyes'/'CSP', '90').
card_flavor_text('lovisa coldeyes'/'CSP', '\"I lead my horde to the northwest. I won\'t return until the ice wizards fall and their fiendish contraptions are crushed!\"').
card_multiverse_id('lovisa coldeyes'/'CSP', '113542').

card_in_set('luminesce', 'CSP').
card_original_type('luminesce'/'CSP', 'Instant').
card_original_text('luminesce'/'CSP', 'Prevent all damage that black and/or red sources would deal this turn.').
card_first_print('luminesce', 'CSP').
card_image_name('luminesce'/'CSP', 'luminesce').
card_uid('luminesce'/'CSP', 'CSP:Luminesce:luminesce').
card_rarity('luminesce'/'CSP', 'Uncommon').
card_artist('luminesce'/'CSP', 'Daren Bader').
card_number('luminesce'/'CSP', '14').
card_flavor_text('luminesce'/'CSP', '\"The White Shield is not the burnished metal you lash to your forearm but the conviction that burns in your chest.\"\n—Lucilde Fiksdotter,\nleader of the Order of the White Shield').
card_multiverse_id('luminesce'/'CSP', '121153').

card_in_set('magmatic core', 'CSP').
card_original_type('magmatic core'/'CSP', 'Enchantment').
card_original_text('magmatic core'/'CSP', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the end of your turn, Magmatic Core deals X damage divided as you choose among any number of target creatures, where X is the number of age counters on it.').
card_first_print('magmatic core', 'CSP').
card_image_name('magmatic core'/'CSP', 'magmatic core').
card_uid('magmatic core'/'CSP', 'CSP:Magmatic Core:magmatic core').
card_rarity('magmatic core'/'CSP', 'Uncommon').
card_artist('magmatic core'/'CSP', 'Matt Cavotta').
card_number('magmatic core'/'CSP', '91').
card_multiverse_id('magmatic core'/'CSP', '121163').

card_in_set('martyr of ashes', 'CSP').
card_original_type('martyr of ashes'/'CSP', 'Creature — Human Shaman').
card_original_text('martyr of ashes'/'CSP', '{2}, Reveal X red cards from your hand, Sacrifice Martyr of Ashes: Martyr of Ashes deals X damage to each creature without flying.').
card_first_print('martyr of ashes', 'CSP').
card_image_name('martyr of ashes'/'CSP', 'martyr of ashes').
card_uid('martyr of ashes'/'CSP', 'CSP:Martyr of Ashes:martyr of ashes').
card_rarity('martyr of ashes'/'CSP', 'Common').
card_artist('martyr of ashes'/'CSP', 'Ralph Horsley').
card_number('martyr of ashes'/'CSP', '92').
card_flavor_text('martyr of ashes'/'CSP', 'She turns ash into flame and burns enemies to ash.').
card_multiverse_id('martyr of ashes'/'CSP', '121170').

card_in_set('martyr of bones', 'CSP').
card_original_type('martyr of bones'/'CSP', 'Creature — Human Wizard').
card_original_text('martyr of bones'/'CSP', '{1}, Reveal X black cards from your hand, Sacrifice Martyr of Bones: Remove up to X target cards in a single graveyard from the game.').
card_first_print('martyr of bones', 'CSP').
card_image_name('martyr of bones'/'CSP', 'martyr of bones').
card_uid('martyr of bones'/'CSP', 'CSP:Martyr of Bones:martyr of bones').
card_rarity('martyr of bones'/'CSP', 'Common').
card_artist('martyr of bones'/'CSP', 'E. M. Gist').
card_number('martyr of bones'/'CSP', '65').
card_flavor_text('martyr of bones'/'CSP', 'Puppeteered by Heidar\'s icy hand, they were sacrificed to the Frost Marsh to raise Haakon\'s army.').
card_multiverse_id('martyr of bones'/'CSP', '121131').

card_in_set('martyr of frost', 'CSP').
card_original_type('martyr of frost'/'CSP', 'Creature — Human Wizard').
card_original_text('martyr of frost'/'CSP', '{2}, Reveal X blue cards from your hand, Sacrifice Martyr of Frost: Counter target spell unless its controller pays {X}.').
card_first_print('martyr of frost', 'CSP').
card_image_name('martyr of frost'/'CSP', 'martyr of frost').
card_uid('martyr of frost'/'CSP', 'CSP:Martyr of Frost:martyr of frost').
card_rarity('martyr of frost'/'CSP', 'Common').
card_artist('martyr of frost'/'CSP', 'Wayne England').
card_number('martyr of frost'/'CSP', '40').
card_flavor_text('martyr of frost'/'CSP', '\"Do not call the Balduvians barbaric. Their magic is as potent as it is primal.\"\n—Zur the Enchanter').
card_multiverse_id('martyr of frost'/'CSP', '121164').

card_in_set('martyr of sands', 'CSP').
card_original_type('martyr of sands'/'CSP', 'Creature — Human Cleric').
card_original_text('martyr of sands'/'CSP', '{1}, Reveal X white cards from your hand, Sacrifice Martyr of Sands: You gain three times X life.').
card_first_print('martyr of sands', 'CSP').
card_image_name('martyr of sands'/'CSP', 'martyr of sands').
card_uid('martyr of sands'/'CSP', 'CSP:Martyr of Sands:martyr of sands').
card_rarity('martyr of sands'/'CSP', 'Common').
card_artist('martyr of sands'/'CSP', 'Randy Gallegos').
card_number('martyr of sands'/'CSP', '15').
card_flavor_text('martyr of sands'/'CSP', '\"Only in our vulnerability is there true power, and that power is life itself.\"').
card_multiverse_id('martyr of sands'/'CSP', '121263').

card_in_set('martyr of spores', 'CSP').
card_original_type('martyr of spores'/'CSP', 'Creature — Human Shaman').
card_original_text('martyr of spores'/'CSP', '{1}, Reveal X green cards from your hand, Sacrifice Martyr of Spores: Target creature gets +X/+X until end of turn.').
card_first_print('martyr of spores', 'CSP').
card_image_name('martyr of spores'/'CSP', 'martyr of spores').
card_uid('martyr of spores'/'CSP', 'CSP:Martyr of Spores:martyr of spores').
card_rarity('martyr of spores'/'CSP', 'Common').
card_artist('martyr of spores'/'CSP', 'Dan Scott').
card_number('martyr of spores'/'CSP', '113').
card_flavor_text('martyr of spores'/'CSP', '\"Weep not for me. I will rise with the spring\'s new growth.\"').
card_multiverse_id('martyr of spores'/'CSP', '121145').

card_in_set('mishra\'s bauble', 'CSP').
card_original_type('mishra\'s bauble'/'CSP', 'Artifact').
card_original_text('mishra\'s bauble'/'CSP', '{T}, Sacrifice Mishra\'s Bauble: Look at the top card of target player\'s library. Draw a card at the beginning of the next turn\'s upkeep.').
card_first_print('mishra\'s bauble', 'CSP').
card_image_name('mishra\'s bauble'/'CSP', 'mishra\'s bauble').
card_uid('mishra\'s bauble'/'CSP', 'CSP:Mishra\'s Bauble:mishra\'s bauble').
card_rarity('mishra\'s bauble'/'CSP', 'Uncommon').
card_artist('mishra\'s bauble'/'CSP', 'Chippy').
card_number('mishra\'s bauble'/'CSP', '138').
card_flavor_text('mishra\'s bauble'/'CSP', '\"Arcum is a babbling fool Phyrexian technology is our greatest blessing. Take this delightful trinket for instance . . .\"\n—Heidar, Rimewind master').
card_multiverse_id('mishra\'s bauble'/'CSP', '122122').

card_in_set('mouth of ronom', 'CSP').
card_original_type('mouth of ronom'/'CSP', 'Snow Land').
card_original_text('mouth of ronom'/'CSP', '{T}: Add {1} to your mana pool.\n{4}{S}, {T}, Sacrifice Mouth of Ronom: Mouth of Ronom deals 4 damage to target creature. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('mouth of ronom', 'CSP').
card_image_name('mouth of ronom'/'CSP', 'mouth of ronom').
card_uid('mouth of ronom'/'CSP', 'CSP:Mouth of Ronom:mouth of ronom').
card_rarity('mouth of ronom'/'CSP', 'Uncommon').
card_artist('mouth of ronom'/'CSP', 'Daren Bader').
card_number('mouth of ronom'/'CSP', '148').
card_multiverse_id('mouth of ronom'/'CSP', '121234').

card_in_set('mystic melting', 'CSP').
card_original_type('mystic melting'/'CSP', 'Instant').
card_original_text('mystic melting'/'CSP', 'Destroy target artifact or enchantment.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('mystic melting', 'CSP').
card_image_name('mystic melting'/'CSP', 'mystic melting').
card_uid('mystic melting'/'CSP', 'CSP:Mystic Melting:mystic melting').
card_rarity('mystic melting'/'CSP', 'Uncommon').
card_artist('mystic melting'/'CSP', 'Chippy').
card_number('mystic melting'/'CSP', '114').
card_flavor_text('mystic melting'/'CSP', 'The sun-hot glow radiated from the druids, leaving scattered metal in steaming pools as it passed.').
card_multiverse_id('mystic melting'/'CSP', '121141').

card_in_set('ohran viper', 'CSP').
card_original_type('ohran viper'/'CSP', 'Snow Creature — Snake').
card_original_text('ohran viper'/'CSP', 'Whenever Ohran Viper deals combat damage to a creature, destroy that creature at the end of combat.\nWhenever Ohran Viper deals combat damage to a player, you may draw a card.').
card_first_print('ohran viper', 'CSP').
card_image_name('ohran viper'/'CSP', 'ohran viper').
card_uid('ohran viper'/'CSP', 'CSP:Ohran Viper:ohran viper').
card_rarity('ohran viper'/'CSP', 'Rare').
card_artist('ohran viper'/'CSP', 'Kev Walker').
card_number('ohran viper'/'CSP', '115').
card_flavor_text('ohran viper'/'CSP', 'The ohran viper is not cold-blooded. Its veins course with the same antigelid venom used to kill its prey.').
card_multiverse_id('ohran viper'/'CSP', '121266').

card_in_set('ohran yeti', 'CSP').
card_original_type('ohran yeti'/'CSP', 'Snow Creature — Yeti').
card_original_text('ohran yeti'/'CSP', '{2}{S}: Target snow creature gains first strike until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('ohran yeti', 'CSP').
card_image_name('ohran yeti'/'CSP', 'ohran yeti').
card_uid('ohran yeti'/'CSP', 'CSP:Ohran Yeti:ohran yeti').
card_rarity('ohran yeti'/'CSP', 'Common').
card_artist('ohran yeti'/'CSP', 'Wayne Reynolds').
card_number('ohran yeti'/'CSP', '93').
card_flavor_text('ohran yeti'/'CSP', 'When a snowdrift shows you its eyes, it\'s already too late.').
card_multiverse_id('ohran yeti'/'CSP', '121144').

card_in_set('orcish bloodpainter', 'CSP').
card_original_type('orcish bloodpainter'/'CSP', 'Creature — Orc Shaman').
card_original_text('orcish bloodpainter'/'CSP', '{T}, Sacrifice a creature: Orcish Bloodpainter deals 1 damage to target creature or player.').
card_first_print('orcish bloodpainter', 'CSP').
card_image_name('orcish bloodpainter'/'CSP', 'orcish bloodpainter').
card_uid('orcish bloodpainter'/'CSP', 'CSP:Orcish Bloodpainter:orcish bloodpainter').
card_rarity('orcish bloodpainter'/'CSP', 'Common').
card_artist('orcish bloodpainter'/'CSP', 'Alan Pollack').
card_number('orcish bloodpainter'/'CSP', '94').
card_flavor_text('orcish bloodpainter'/'CSP', 'Blood calls to blood.').
card_multiverse_id('orcish bloodpainter'/'CSP', '121232').

card_in_set('panglacial wurm', 'CSP').
card_original_type('panglacial wurm'/'CSP', 'Creature — Wurm').
card_original_text('panglacial wurm'/'CSP', 'Trample\nWhile you\'re searching your library, you may play Panglacial Wurm from your library.').
card_first_print('panglacial wurm', 'CSP').
card_image_name('panglacial wurm'/'CSP', 'panglacial wurm').
card_uid('panglacial wurm'/'CSP', 'CSP:Panglacial Wurm:panglacial wurm').
card_rarity('panglacial wurm'/'CSP', 'Rare').
card_artist('panglacial wurm'/'CSP', 'Jim Pavelec').
card_number('panglacial wurm'/'CSP', '116').
card_flavor_text('panglacial wurm'/'CSP', '\"Step lightly and we might be able to use it as a bridge.\"\n—Ib Halfheart, goblin tactician').
card_multiverse_id('panglacial wurm'/'CSP', '121264').

card_in_set('perilous research', 'CSP').
card_original_type('perilous research'/'CSP', 'Instant').
card_original_text('perilous research'/'CSP', 'Draw two cards, then sacrifice a permanent.').
card_first_print('perilous research', 'CSP').
card_image_name('perilous research'/'CSP', 'perilous research').
card_uid('perilous research'/'CSP', 'CSP:Perilous Research:perilous research').
card_rarity('perilous research'/'CSP', 'Uncommon').
card_artist('perilous research'/'CSP', 'Dany Orizio').
card_number('perilous research'/'CSP', '41').
card_flavor_text('perilous research'/'CSP', 'When the School of the Unseen fell, so many magical treasures lay abandoned that no amount of death could deter the stream of thieves and desperate scholars.').
card_multiverse_id('perilous research'/'CSP', '121238').

card_in_set('phobian phantasm', 'CSP').
card_original_type('phobian phantasm'/'CSP', 'Creature — Illusion').
card_original_text('phobian phantasm'/'CSP', 'Flying, fear\nCumulative upkeep {B} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_first_print('phobian phantasm', 'CSP').
card_image_name('phobian phantasm'/'CSP', 'phobian phantasm').
card_uid('phobian phantasm'/'CSP', 'CSP:Phobian Phantasm:phobian phantasm').
card_rarity('phobian phantasm'/'CSP', 'Uncommon').
card_artist('phobian phantasm'/'CSP', 'Steven Belledin').
card_number('phobian phantasm'/'CSP', '66').
card_multiverse_id('phobian phantasm'/'CSP', '121146').

card_in_set('phyrexian etchings', 'CSP').
card_original_type('phyrexian etchings'/'CSP', 'Enchantment').
card_original_text('phyrexian etchings'/'CSP', 'Cumulative upkeep {B} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the end of your turn, draw a card for each age counter on Phyrexian Etchings.\nWhen Phyrexian Etchings is put into a graveyard from play, you lose 2 life for each age counter on it.').
card_first_print('phyrexian etchings', 'CSP').
card_image_name('phyrexian etchings'/'CSP', 'phyrexian etchings').
card_uid('phyrexian etchings'/'CSP', 'CSP:Phyrexian Etchings:phyrexian etchings').
card_rarity('phyrexian etchings'/'CSP', 'Rare').
card_artist('phyrexian etchings'/'CSP', 'Ron Spears').
card_number('phyrexian etchings'/'CSP', '67').
card_multiverse_id('phyrexian etchings'/'CSP', '121121').

card_in_set('phyrexian ironfoot', 'CSP').
card_original_type('phyrexian ironfoot'/'CSP', 'Snow Artifact Creature — Construct').
card_original_text('phyrexian ironfoot'/'CSP', 'Phyrexian Ironfoot doesn\'t untap during your untap step.\n{1}{S}: Untap Phyrexian Ironfoot. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('phyrexian ironfoot', 'CSP').
card_image_name('phyrexian ironfoot'/'CSP', 'phyrexian ironfoot').
card_uid('phyrexian ironfoot'/'CSP', 'CSP:Phyrexian Ironfoot:phyrexian ironfoot').
card_rarity('phyrexian ironfoot'/'CSP', 'Uncommon').
card_artist('phyrexian ironfoot'/'CSP', 'Stephan Martiniere').
card_number('phyrexian ironfoot'/'CSP', '139').
card_flavor_text('phyrexian ironfoot'/'CSP', 'It took the Rimewind cultists days to realize they had successfully activated the creature—it just wasn\'t interested in moving.').
card_multiverse_id('phyrexian ironfoot'/'CSP', '121127').

card_in_set('phyrexian snowcrusher', 'CSP').
card_original_type('phyrexian snowcrusher'/'CSP', 'Snow Artifact Creature — Juggernaut').
card_original_text('phyrexian snowcrusher'/'CSP', 'Phyrexian Snowcrusher attacks each turn if able.\n{1}{S}: Phyrexian Snowcrusher gets +1/+0 until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('phyrexian snowcrusher', 'CSP').
card_image_name('phyrexian snowcrusher'/'CSP', 'phyrexian snowcrusher').
card_uid('phyrexian snowcrusher'/'CSP', 'CSP:Phyrexian Snowcrusher:phyrexian snowcrusher').
card_rarity('phyrexian snowcrusher'/'CSP', 'Uncommon').
card_artist('phyrexian snowcrusher'/'CSP', 'Dave Allsop').
card_number('phyrexian snowcrusher'/'CSP', '140').
card_flavor_text('phyrexian snowcrusher'/'CSP', 'Its plow wasn\'t designed for snow, but to carve a path through shrieking armies.').
card_multiverse_id('phyrexian snowcrusher'/'CSP', '121142').

card_in_set('phyrexian soulgorger', 'CSP').
card_original_type('phyrexian soulgorger'/'CSP', 'Snow Artifact Creature — Construct').
card_original_text('phyrexian soulgorger'/'CSP', 'Cumulative upkeep—Sacrifice a creature. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_first_print('phyrexian soulgorger', 'CSP').
card_image_name('phyrexian soulgorger'/'CSP', 'phyrexian soulgorger').
card_uid('phyrexian soulgorger'/'CSP', 'CSP:Phyrexian Soulgorger:phyrexian soulgorger').
card_rarity('phyrexian soulgorger'/'CSP', 'Rare').
card_artist('phyrexian soulgorger'/'CSP', 'Brian Snõddy').
card_number('phyrexian soulgorger'/'CSP', '141').
card_flavor_text('phyrexian soulgorger'/'CSP', 'The Phyrexian minds that constructed it did not live past its awakening.').
card_multiverse_id('phyrexian soulgorger'/'CSP', '121160').

card_in_set('resize', 'CSP').
card_original_type('resize'/'CSP', 'Instant').
card_original_text('resize'/'CSP', 'Target creature gets +3/+3 until end of turn.\nRecover {1}{G} (When a creature is put into your graveyard from play, you may pay {1}{G}. If you do, return this card from your graveyard to your hand. Otherwise, remove this card from the game.)').
card_first_print('resize', 'CSP').
card_image_name('resize'/'CSP', 'resize').
card_uid('resize'/'CSP', 'CSP:Resize:resize').
card_rarity('resize'/'CSP', 'Uncommon').
card_artist('resize'/'CSP', 'Daren Bader').
card_number('resize'/'CSP', '117').
card_multiverse_id('resize'/'CSP', '121223').

card_in_set('rime transfusion', 'CSP').
card_original_type('rime transfusion'/'CSP', 'Snow Enchantment — Aura').
card_original_text('rime transfusion'/'CSP', 'Enchant creature\nEnchanted creature gets +2/+1 and has \"{S}: This creature can\'t be blocked this turn except by snow creatures.\" ({S} can be paid with one mana from a snow permanent.)').
card_first_print('rime transfusion', 'CSP').
card_image_name('rime transfusion'/'CSP', 'rime transfusion').
card_uid('rime transfusion'/'CSP', 'CSP:Rime Transfusion:rime transfusion').
card_rarity('rime transfusion'/'CSP', 'Uncommon').
card_artist('rime transfusion'/'CSP', 'Jeff Nentrup').
card_number('rime transfusion'/'CSP', '68').
card_flavor_text('rime transfusion'/'CSP', 'The unfortunates so transformed hungered not for flesh, but for warmth.').
card_multiverse_id('rime transfusion'/'CSP', '121257').

card_in_set('rimebound dead', 'CSP').
card_original_type('rimebound dead'/'CSP', 'Snow Creature — Skeleton').
card_original_text('rimebound dead'/'CSP', '{S}: Regenerate Rimebound Dead. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('rimebound dead', 'CSP').
card_image_name('rimebound dead'/'CSP', 'rimebound dead').
card_uid('rimebound dead'/'CSP', 'CSP:Rimebound Dead:rimebound dead').
card_rarity('rimebound dead'/'CSP', 'Common').
card_artist('rimebound dead'/'CSP', 'Dave Kendall').
card_number('rimebound dead'/'CSP', '69').
card_flavor_text('rimebound dead'/'CSP', '\"Tresserhorn is ours now. Dredge the lake and bring me all the bones you find. We\'ll build you a new army, the likes of which Terisiare has never seen.\"\n—Heidar, Rimewind master, to Haakon').
card_multiverse_id('rimebound dead'/'CSP', '121136').

card_in_set('rimefeather owl', 'CSP').
card_original_type('rimefeather owl'/'CSP', 'Snow Creature — Bird').
card_original_text('rimefeather owl'/'CSP', 'Flying\nRimefeather Owl\'s power and toughness are each equal to the number of snow permanents in play.\n{1}{S}: Put an ice counter on target permanent.\nPermanents with ice counters on them are snow.').
card_first_print('rimefeather owl', 'CSP').
card_image_name('rimefeather owl'/'CSP', 'rimefeather owl').
card_uid('rimefeather owl'/'CSP', 'CSP:Rimefeather Owl:rimefeather owl').
card_rarity('rimefeather owl'/'CSP', 'Rare').
card_artist('rimefeather owl'/'CSP', 'Kensuke Okabayashi').
card_number('rimefeather owl'/'CSP', '42').
card_multiverse_id('rimefeather owl'/'CSP', '122116').

card_in_set('rimehorn aurochs', 'CSP').
card_original_type('rimehorn aurochs'/'CSP', 'Snow Creature — Aurochs').
card_original_text('rimehorn aurochs'/'CSP', 'Trample\nWhenever Rimehorn Aurochs attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.\n{2}{S}: Target creature blocks target creature this turn if able. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('rimehorn aurochs', 'CSP').
card_image_name('rimehorn aurochs'/'CSP', 'rimehorn aurochs').
card_uid('rimehorn aurochs'/'CSP', 'CSP:Rimehorn Aurochs:rimehorn aurochs').
card_rarity('rimehorn aurochs'/'CSP', 'Uncommon').
card_artist('rimehorn aurochs'/'CSP', 'Brian Despain').
card_number('rimehorn aurochs'/'CSP', '118').
card_multiverse_id('rimehorn aurochs'/'CSP', '121210').

card_in_set('rimescale dragon', 'CSP').
card_original_type('rimescale dragon'/'CSP', 'Snow Creature — Dragon').
card_original_text('rimescale dragon'/'CSP', 'Flying\n{2}{S}: Tap target creature and put an ice counter on it. ({S} can be paid with one mana from a snow permanent.)\nCreatures with ice counters on them don\'t untap during their controllers\' untap steps.').
card_first_print('rimescale dragon', 'CSP').
card_image_name('rimescale dragon'/'CSP', 'rimescale dragon').
card_uid('rimescale dragon'/'CSP', 'CSP:Rimescale Dragon:rimescale dragon').
card_rarity('rimescale dragon'/'CSP', 'Rare').
card_artist('rimescale dragon'/'CSP', 'Jeff Easley').
card_number('rimescale dragon'/'CSP', '95').
card_multiverse_id('rimescale dragon'/'CSP', '121184').

card_in_set('rimewind cryomancer', 'CSP').
card_original_type('rimewind cryomancer'/'CSP', 'Creature — Human Wizard').
card_original_text('rimewind cryomancer'/'CSP', '{1}, {T}: Counter target activated ability. Play this ability only if you control four or more snow permanents. (Mana abilities can\'t be targeted.)').
card_first_print('rimewind cryomancer', 'CSP').
card_image_name('rimewind cryomancer'/'CSP', 'rimewind cryomancer').
card_uid('rimewind cryomancer'/'CSP', 'CSP:Rimewind Cryomancer:rimewind cryomancer').
card_rarity('rimewind cryomancer'/'CSP', 'Uncommon').
card_artist('rimewind cryomancer'/'CSP', 'Dan Scott').
card_number('rimewind cryomancer'/'CSP', '43').
card_flavor_text('rimewind cryomancer'/'CSP', '\"The Thaw brings flooding, disease, and death. We will do all in our power to bring frigid peace back to the world.\"').
card_multiverse_id('rimewind cryomancer'/'CSP', '121215').

card_in_set('rimewind taskmage', 'CSP').
card_original_type('rimewind taskmage'/'CSP', 'Creature — Human Wizard').
card_original_text('rimewind taskmage'/'CSP', '{1}, {T}: Tap or untap target permanent. Play this ability only if you control four or more snow permanents.').
card_first_print('rimewind taskmage', 'CSP').
card_image_name('rimewind taskmage'/'CSP', 'rimewind taskmage').
card_uid('rimewind taskmage'/'CSP', 'CSP:Rimewind Taskmage:rimewind taskmage').
card_rarity('rimewind taskmage'/'CSP', 'Common').
card_artist('rimewind taskmage'/'CSP', 'Ron Spears').
card_number('rimewind taskmage'/'CSP', '44').
card_flavor_text('rimewind taskmage'/'CSP', '\"Find solace in the sun? Burning, blinding, laying secrets bare? No, solace is found under the blanket of cold.\"').
card_multiverse_id('rimewind taskmage'/'CSP', '121239').

card_in_set('rite of flame', 'CSP').
card_original_type('rite of flame'/'CSP', 'Sorcery').
card_original_text('rite of flame'/'CSP', 'Add {R}{R} to your mana pool, then add {R} to your mana pool for each card named Rite of Flame in each graveyard.').
card_first_print('rite of flame', 'CSP').
card_image_name('rite of flame'/'CSP', 'rite of flame').
card_uid('rite of flame'/'CSP', 'CSP:Rite of Flame:rite of flame').
card_rarity('rite of flame'/'CSP', 'Common').
card_artist('rite of flame'/'CSP', 'Dany Orizio').
card_number('rite of flame'/'CSP', '96').
card_flavor_text('rite of flame'/'CSP', 'Deep beneath the ice, beneath the soil and the rock, Dominaria\'s fire still burned hot.').
card_multiverse_id('rite of flame'/'CSP', '121217').

card_in_set('ronom hulk', 'CSP').
card_original_type('ronom hulk'/'CSP', 'Creature — Beast').
card_original_text('ronom hulk'/'CSP', 'Protection from snow\nCumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_first_print('ronom hulk', 'CSP').
card_image_name('ronom hulk'/'CSP', 'ronom hulk').
card_uid('ronom hulk'/'CSP', 'CSP:Ronom Hulk:ronom hulk').
card_rarity('ronom hulk'/'CSP', 'Common').
card_artist('ronom hulk'/'CSP', 'Zoltan Boros & Gabor Szikszai').
card_number('ronom hulk'/'CSP', '119').
card_multiverse_id('ronom hulk'/'CSP', '121270').

card_in_set('ronom serpent', 'CSP').
card_original_type('ronom serpent'/'CSP', 'Snow Creature — Serpent').
card_original_text('ronom serpent'/'CSP', 'Ronom Serpent can\'t attack unless defending player controls a snow land.\nWhen you control no snow lands, sacrifice Ronom Serpent.').
card_first_print('ronom serpent', 'CSP').
card_image_name('ronom serpent'/'CSP', 'ronom serpent').
card_uid('ronom serpent'/'CSP', 'CSP:Ronom Serpent:ronom serpent').
card_rarity('ronom serpent'/'CSP', 'Common').
card_artist('ronom serpent'/'CSP', 'Ron Spencer').
card_number('ronom serpent'/'CSP', '45').
card_flavor_text('ronom serpent'/'CSP', 'With the spread of Rimewind\'s sorcerous winter, snow dwellers reclaimed their places atop the food chain.').
card_multiverse_id('ronom serpent'/'CSP', '121176').

card_in_set('ronom unicorn', 'CSP').
card_original_type('ronom unicorn'/'CSP', 'Creature — Unicorn').
card_original_text('ronom unicorn'/'CSP', 'Sacrifice Ronom Unicorn: Destroy target enchantment.').
card_first_print('ronom unicorn', 'CSP').
card_image_name('ronom unicorn'/'CSP', 'ronom unicorn').
card_uid('ronom unicorn'/'CSP', 'CSP:Ronom Unicorn:ronom unicorn').
card_rarity('ronom unicorn'/'CSP', 'Common').
card_artist('ronom unicorn'/'CSP', 'Carl Critchlow').
card_number('ronom unicorn'/'CSP', '16').
card_flavor_text('ronom unicorn'/'CSP', 'The aberrant magic of the Rimewind drew the unicorns back from the northern wastes to do battle once again.').
card_multiverse_id('ronom unicorn'/'CSP', '121237').

card_in_set('rune snag', 'CSP').
card_original_type('rune snag'/'CSP', 'Instant').
card_original_text('rune snag'/'CSP', 'Counter target spell unless its controller pays {2} plus an additional {2} for each card named Rune Snag in each graveyard.').
card_first_print('rune snag', 'CSP').
card_image_name('rune snag'/'CSP', 'rune snag').
card_uid('rune snag'/'CSP', 'CSP:Rune Snag:rune snag').
card_rarity('rune snag'/'CSP', 'Common').
card_artist('rune snag'/'CSP', 'Dave Dorman').
card_number('rune snag'/'CSP', '46').
card_flavor_text('rune snag'/'CSP', '\"Concentration is key. Without it, a mage conjures nothing but a splitting headache.\"\n—Zur the Enchanter').
card_multiverse_id('rune snag'/'CSP', '121247').

card_in_set('scrying sheets', 'CSP').
card_original_type('scrying sheets'/'CSP', 'Snow Land').
card_original_text('scrying sheets'/'CSP', '{T}: Add {1} to your mana pool.\n{1}{S}, {T}: Look at the top card of your library. If that card is snow, you may reveal it and put it into your hand. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('scrying sheets', 'CSP').
card_image_name('scrying sheets'/'CSP', 'scrying sheets').
card_uid('scrying sheets'/'CSP', 'CSP:Scrying Sheets:scrying sheets').
card_rarity('scrying sheets'/'CSP', 'Rare').
card_artist('scrying sheets'/'CSP', 'Thomas M. Baxa').
card_number('scrying sheets'/'CSP', '149').
card_multiverse_id('scrying sheets'/'CSP', '121204').

card_in_set('sek\'kuar, deathkeeper', 'CSP').
card_original_type('sek\'kuar, deathkeeper'/'CSP', 'Legendary Creature — Orc Shaman').
card_original_text('sek\'kuar, deathkeeper'/'CSP', 'Whenever another nontoken creature you control is put into a graveyard from play, put a 3/1 black and red Graveborn creature token with haste into play.').
card_first_print('sek\'kuar, deathkeeper', 'CSP').
card_image_name('sek\'kuar, deathkeeper'/'CSP', 'sek\'kuar, deathkeeper').
card_uid('sek\'kuar, deathkeeper'/'CSP', 'CSP:Sek\'Kuar, Deathkeeper:sek\'kuar, deathkeeper').
card_rarity('sek\'kuar, deathkeeper'/'CSP', 'Rare').
card_artist('sek\'kuar, deathkeeper'/'CSP', 'Jeff Miracola').
card_number('sek\'kuar, deathkeeper'/'CSP', '131').
card_flavor_text('sek\'kuar, deathkeeper'/'CSP', 'Karplusan legend told of an orc so cruel that he burned his own followers in rage—yet so revered that they rose from their pyres to serve him.').
card_multiverse_id('sek\'kuar, deathkeeper'/'CSP', '121261').

card_in_set('shape of the wiitigo', 'CSP').
card_original_type('shape of the wiitigo'/'CSP', 'Enchantment — Aura').
card_original_text('shape of the wiitigo'/'CSP', 'Enchant creature\nWhen Shape of the Wiitigo comes into play, put six +1/+1 counters on enchanted creature.\nAt the beginning of your upkeep, put a +1/+1 counter on enchanted creature if it attacked or blocked since your last upkeep. Otherwise, remove a +1/+1 counter from it.').
card_first_print('shape of the wiitigo', 'CSP').
card_image_name('shape of the wiitigo'/'CSP', 'shape of the wiitigo').
card_uid('shape of the wiitigo'/'CSP', 'CSP:Shape of the Wiitigo:shape of the wiitigo').
card_rarity('shape of the wiitigo'/'CSP', 'Rare').
card_artist('shape of the wiitigo'/'CSP', 'Ron Spencer').
card_number('shape of the wiitigo'/'CSP', '120').
card_multiverse_id('shape of the wiitigo'/'CSP', '122073').

card_in_set('sheltering ancient', 'CSP').
card_original_type('sheltering ancient'/'CSP', 'Creature — Treefolk').
card_original_text('sheltering ancient'/'CSP', 'Trample\nCumulative upkeep—Put a +1/+1 counter on a creature an opponent controls. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_first_print('sheltering ancient', 'CSP').
card_image_name('sheltering ancient'/'CSP', 'sheltering ancient').
card_uid('sheltering ancient'/'CSP', 'CSP:Sheltering Ancient:sheltering ancient').
card_rarity('sheltering ancient'/'CSP', 'Uncommon').
card_artist('sheltering ancient'/'CSP', 'Pete Venters').
card_number('sheltering ancient'/'CSP', '121').
card_multiverse_id('sheltering ancient'/'CSP', '122069').

card_in_set('simian brawler', 'CSP').
card_original_type('simian brawler'/'CSP', 'Creature — Ape Warrior').
card_original_text('simian brawler'/'CSP', 'Discard a land card: Simian Brawler gets +1/+1 until end of turn.').
card_first_print('simian brawler', 'CSP').
card_image_name('simian brawler'/'CSP', 'simian brawler').
card_uid('simian brawler'/'CSP', 'CSP:Simian Brawler:simian brawler').
card_rarity('simian brawler'/'CSP', 'Common').
card_artist('simian brawler'/'CSP', 'Warren Mahy').
card_number('simian brawler'/'CSP', '122').
card_flavor_text('simian brawler'/'CSP', '\"It\'s odd to see the apes rip down trees to arm themselves in defense of their forests.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('simian brawler'/'CSP', '121183').

card_in_set('skred', 'CSP').
card_original_type('skred'/'CSP', 'Instant').
card_original_text('skred'/'CSP', 'Skred deals damage to target creature equal to the number of snow permanents you control.').
card_first_print('skred', 'CSP').
card_image_name('skred'/'CSP', 'skred').
card_uid('skred'/'CSP', 'CSP:Skred:skred').
card_rarity('skred'/'CSP', 'Common').
card_artist('skred'/'CSP', 'Christopher Moeller').
card_number('skred'/'CSP', '97').
card_flavor_text('skred'/'CSP', '\"This close to Rimewind Keep, no avalanche is nature\'s doing.\"\n—Ukoten, centaur chieftain').
card_multiverse_id('skred'/'CSP', '122120').

card_in_set('snow-covered forest', 'CSP').
card_original_type('snow-covered forest'/'CSP', 'Basic Snow Land — Forest').
card_original_text('snow-covered forest'/'CSP', 'G').
card_image_name('snow-covered forest'/'CSP', 'snow-covered forest').
card_uid('snow-covered forest'/'CSP', 'CSP:Snow-Covered Forest:snow-covered forest').
card_rarity('snow-covered forest'/'CSP', 'Basic Land').
card_artist('snow-covered forest'/'CSP', 'Jim Nelson').
card_number('snow-covered forest'/'CSP', '155').
card_multiverse_id('snow-covered forest'/'CSP', '121192').

card_in_set('snow-covered island', 'CSP').
card_original_type('snow-covered island'/'CSP', 'Basic Snow Land — Island').
card_original_text('snow-covered island'/'CSP', 'U').
card_image_name('snow-covered island'/'CSP', 'snow-covered island').
card_uid('snow-covered island'/'CSP', 'CSP:Snow-Covered Island:snow-covered island').
card_rarity('snow-covered island'/'CSP', 'Basic Land').
card_artist('snow-covered island'/'CSP', 'Franz Vohwinkel').
card_number('snow-covered island'/'CSP', '152').
card_multiverse_id('snow-covered island'/'CSP', '121130').

card_in_set('snow-covered mountain', 'CSP').
card_original_type('snow-covered mountain'/'CSP', 'Basic Snow Land — Mountain').
card_original_text('snow-covered mountain'/'CSP', 'R').
card_image_name('snow-covered mountain'/'CSP', 'snow-covered mountain').
card_uid('snow-covered mountain'/'CSP', 'CSP:Snow-Covered Mountain:snow-covered mountain').
card_rarity('snow-covered mountain'/'CSP', 'Basic Land').
card_artist('snow-covered mountain'/'CSP', 'John Zeleznik').
card_number('snow-covered mountain'/'CSP', '154').
card_multiverse_id('snow-covered mountain'/'CSP', '121233').

card_in_set('snow-covered plains', 'CSP').
card_original_type('snow-covered plains'/'CSP', 'Basic Snow Land — Plains').
card_original_text('snow-covered plains'/'CSP', 'W').
card_image_name('snow-covered plains'/'CSP', 'snow-covered plains').
card_uid('snow-covered plains'/'CSP', 'CSP:Snow-Covered Plains:snow-covered plains').
card_rarity('snow-covered plains'/'CSP', 'Basic Land').
card_artist('snow-covered plains'/'CSP', 'Mark Romanoski').
card_number('snow-covered plains'/'CSP', '151').
card_multiverse_id('snow-covered plains'/'CSP', '121267').

card_in_set('snow-covered swamp', 'CSP').
card_original_type('snow-covered swamp'/'CSP', 'Basic Snow Land — Swamp').
card_original_text('snow-covered swamp'/'CSP', 'B').
card_image_name('snow-covered swamp'/'CSP', 'snow-covered swamp').
card_uid('snow-covered swamp'/'CSP', 'CSP:Snow-Covered Swamp:snow-covered swamp').
card_rarity('snow-covered swamp'/'CSP', 'Basic Land').
card_artist('snow-covered swamp'/'CSP', 'Rob Alexander').
card_number('snow-covered swamp'/'CSP', '153').
card_multiverse_id('snow-covered swamp'/'CSP', '121256').

card_in_set('soul spike', 'CSP').
card_original_type('soul spike'/'CSP', 'Instant').
card_original_text('soul spike'/'CSP', 'You may remove two black cards in your hand from the game rather than pay Soul Spike\'s mana cost.\nSoul Spike deals 4 damage to target creature or player and you gain 4 life.').
card_first_print('soul spike', 'CSP').
card_image_name('soul spike'/'CSP', 'soul spike').
card_uid('soul spike'/'CSP', 'CSP:Soul Spike:soul spike').
card_rarity('soul spike'/'CSP', 'Rare').
card_artist('soul spike'/'CSP', 'Wayne England').
card_number('soul spike'/'CSP', '70').
card_multiverse_id('soul spike'/'CSP', '121165').

card_in_set('sound the call', 'CSP').
card_original_type('sound the call'/'CSP', 'Sorcery').
card_original_text('sound the call'/'CSP', 'Put a 1/1 green Wolf creature token into play with \"This creature gets +1/+1 for each card named Sound the Call in each graveyard.\"').
card_first_print('sound the call', 'CSP').
card_image_name('sound the call'/'CSP', 'sound the call').
card_uid('sound the call'/'CSP', 'CSP:Sound the Call:sound the call').
card_rarity('sound the call'/'CSP', 'Common').
card_artist('sound the call'/'CSP', 'Jim Nelson').
card_number('sound the call'/'CSP', '123').
card_flavor_text('sound the call'/'CSP', '\"If you call the wolves, ensure you have a worthy hunt, or you may become the hunted.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('sound the call'/'CSP', '121156').

card_in_set('squall drifter', 'CSP').
card_original_type('squall drifter'/'CSP', 'Snow Creature — Elemental').
card_original_text('squall drifter'/'CSP', 'Flying\n{W}, {T}: Tap target creature.').
card_first_print('squall drifter', 'CSP').
card_image_name('squall drifter'/'CSP', 'squall drifter').
card_uid('squall drifter'/'CSP', 'CSP:Squall Drifter:squall drifter').
card_rarity('squall drifter'/'CSP', 'Common').
card_artist('squall drifter'/'CSP', 'Joel Thomas').
card_number('squall drifter'/'CSP', '17').
card_flavor_text('squall drifter'/'CSP', '\"The scouts claim the cold has malice, and a mind. I begin to believe them. Impassable drifts seem to appear before us at every turn.\"\n—Aevar Borg, northern guide, journal entry').
card_multiverse_id('squall drifter'/'CSP', '121273').

card_in_set('stalking yeti', 'CSP').
card_original_type('stalking yeti'/'CSP', 'Snow Creature — Yeti').
card_original_text('stalking yeti'/'CSP', 'When Stalking Yeti comes into play, if it\'s in play, it deals damage equal to its power to target creature an opponent controls and that creature deals damage equal to its power to Stalking Yeti.\n{2}{S}: Return Stalking Yeti to its owner\'s hand. Play this ability only any time you could play a sorcery. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('stalking yeti', 'CSP').
card_image_name('stalking yeti'/'CSP', 'stalking yeti').
card_uid('stalking yeti'/'CSP', 'CSP:Stalking Yeti:stalking yeti').
card_rarity('stalking yeti'/'CSP', 'Uncommon').
card_artist('stalking yeti'/'CSP', 'Brian Snõddy').
card_number('stalking yeti'/'CSP', '98').
card_multiverse_id('stalking yeti'/'CSP', '122046').

card_in_set('steam spitter', 'CSP').
card_original_type('steam spitter'/'CSP', 'Creature — Spider').
card_original_text('steam spitter'/'CSP', 'Steam Spitter can block as though it had flying.\n{R}: Steam Spitter gets +1/+0 until end of turn.').
card_first_print('steam spitter', 'CSP').
card_image_name('steam spitter'/'CSP', 'steam spitter').
card_uid('steam spitter'/'CSP', 'CSP:Steam Spitter:steam spitter').
card_rarity('steam spitter'/'CSP', 'Uncommon').
card_artist('steam spitter'/'CSP', 'Christopher Rush').
card_number('steam spitter'/'CSP', '124').
card_flavor_text('steam spitter'/'CSP', 'Its belly produces a boiling-hot fluid it can spit as a weapon or spin slowly enough to freeze into a defensive web.').
card_multiverse_id('steam spitter'/'CSP', '121240').

card_in_set('stromgald crusader', 'CSP').
card_original_type('stromgald crusader'/'CSP', 'Creature — Zombie Knight').
card_original_text('stromgald crusader'/'CSP', 'Protection from white\n{B}: Stromgald Crusader gains flying until end of turn.\n{B}{B}: Stromgald Crusader gets +1/+0 until end of turn.').
card_first_print('stromgald crusader', 'CSP').
card_image_name('stromgald crusader'/'CSP', 'stromgald crusader').
card_uid('stromgald crusader'/'CSP', 'CSP:Stromgald Crusader:stromgald crusader').
card_rarity('stromgald crusader'/'CSP', 'Uncommon').
card_artist('stromgald crusader'/'CSP', 'Volkan Baga').
card_number('stromgald crusader'/'CSP', '71').
card_flavor_text('stromgald crusader'/'CSP', '\"The White-Shielders think their code justifies their actions. In truth it only binds their hands.\"').
card_multiverse_id('stromgald crusader'/'CSP', '121253').

card_in_set('sun\'s bounty', 'CSP').
card_original_type('sun\'s bounty'/'CSP', 'Instant').
card_original_text('sun\'s bounty'/'CSP', 'You gain 4 life.\nRecover {1}{W} (When a creature is put into your graveyard from play, you may pay {1}{W}. If you do, return this card from your graveyard to your hand. Otherwise, remove this card from the game.)').
card_first_print('sun\'s bounty', 'CSP').
card_image_name('sun\'s bounty'/'CSP', 'sun\'s bounty').
card_uid('sun\'s bounty'/'CSP', 'CSP:Sun\'s Bounty:sun\'s bounty').
card_rarity('sun\'s bounty'/'CSP', 'Common').
card_artist('sun\'s bounty'/'CSP', 'Michael Sutfin').
card_number('sun\'s bounty'/'CSP', '18').
card_flavor_text('sun\'s bounty'/'CSP', '\"Heidar has forced the sun into hiding, but it has not forgotten us.\"').
card_multiverse_id('sun\'s bounty'/'CSP', '121494').

card_in_set('sunscour', 'CSP').
card_original_type('sunscour'/'CSP', 'Sorcery').
card_original_text('sunscour'/'CSP', 'You may remove two white cards in your hand from the game rather than pay Sunscour\'s mana cost.\nDestroy all creatures.').
card_first_print('sunscour', 'CSP').
card_image_name('sunscour'/'CSP', 'sunscour').
card_uid('sunscour'/'CSP', 'CSP:Sunscour:sunscour').
card_rarity('sunscour'/'CSP', 'Rare').
card_artist('sunscour'/'CSP', 'Jim Murray').
card_number('sunscour'/'CSP', '19').
card_multiverse_id('sunscour'/'CSP', '121251').

card_in_set('surging æther', 'CSP').
card_original_type('surging æther'/'CSP', 'Instant').
card_original_text('surging æther'/'CSP', 'Ripple 4 (When you play this spell, you may reveal the top four cards of your library. You may play any revealed cards with the same name as this spell without paying their mana costs. Put the rest on the bottom of your library.)\nReturn target permanent to its owner\'s hand.').
card_first_print('surging æther', 'CSP').
card_image_name('surging æther'/'CSP', 'surging aether').
card_uid('surging æther'/'CSP', 'CSP:Surging Æther:surging aether').
card_rarity('surging æther'/'CSP', 'Common').
card_artist('surging æther'/'CSP', 'Anthony S. Waters').
card_number('surging æther'/'CSP', '47').
card_multiverse_id('surging æther'/'CSP', '122052').

card_in_set('surging dementia', 'CSP').
card_original_type('surging dementia'/'CSP', 'Sorcery').
card_original_text('surging dementia'/'CSP', 'Ripple 4 (When you play this spell, you may reveal the top four cards of your library. You may play any revealed cards with the same name as this spell without paying their mana costs. Put the rest on the bottom of your library.)\nTarget player discards a card.').
card_first_print('surging dementia', 'CSP').
card_image_name('surging dementia'/'CSP', 'surging dementia').
card_uid('surging dementia'/'CSP', 'CSP:Surging Dementia:surging dementia').
card_rarity('surging dementia'/'CSP', 'Common').
card_artist('surging dementia'/'CSP', 'Hideaki Takamura').
card_number('surging dementia'/'CSP', '72').
card_multiverse_id('surging dementia'/'CSP', '122053').

card_in_set('surging flame', 'CSP').
card_original_type('surging flame'/'CSP', 'Instant').
card_original_text('surging flame'/'CSP', 'Ripple 4 (When you play this spell, you may reveal the top four cards of your library. You may play any revealed cards with the same name as this spell without paying their mana costs. Put the rest on the bottom of your library.)\nSurging Flame deals 2 damage to target creature or player.').
card_image_name('surging flame'/'CSP', 'surging flame').
card_uid('surging flame'/'CSP', 'CSP:Surging Flame:surging flame').
card_rarity('surging flame'/'CSP', 'Common').
card_artist('surging flame'/'CSP', 'Ron Spencer').
card_number('surging flame'/'CSP', '99').
card_multiverse_id('surging flame'/'CSP', '121269').

card_in_set('surging might', 'CSP').
card_original_type('surging might'/'CSP', 'Enchantment — Aura').
card_original_text('surging might'/'CSP', 'Enchant creature\nEnchanted creature gets +2/+2.\nRipple 4 (When you play this spell, you may reveal the top four cards of your library. You may play any revealed cards with the same name as this spell without paying their mana costs. Put the rest on the bottom of your library.)').
card_first_print('surging might', 'CSP').
card_image_name('surging might'/'CSP', 'surging might').
card_uid('surging might'/'CSP', 'CSP:Surging Might:surging might').
card_rarity('surging might'/'CSP', 'Common').
card_artist('surging might'/'CSP', 'Luca Zontini').
card_number('surging might'/'CSP', '125').
card_multiverse_id('surging might'/'CSP', '121205').

card_in_set('surging sentinels', 'CSP').
card_original_type('surging sentinels'/'CSP', 'Creature — Human Soldier').
card_original_text('surging sentinels'/'CSP', 'First strike\nRipple 4 (When you play this spell, you may reveal the top four cards of your library. You may play any revealed cards with the same name as this spell without paying their mana costs. Put the rest on the bottom of your library.)').
card_first_print('surging sentinels', 'CSP').
card_image_name('surging sentinels'/'CSP', 'surging sentinels').
card_uid('surging sentinels'/'CSP', 'CSP:Surging Sentinels:surging sentinels').
card_rarity('surging sentinels'/'CSP', 'Common').
card_artist('surging sentinels'/'CSP', 'Christopher Moeller').
card_number('surging sentinels'/'CSP', '20').
card_multiverse_id('surging sentinels'/'CSP', '122118').

card_in_set('survivor of the unseen', 'CSP').
card_original_type('survivor of the unseen'/'CSP', 'Creature — Human Wizard').
card_original_text('survivor of the unseen'/'CSP', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\n{T}: Draw two cards, then put a card from your hand on top of your library.').
card_first_print('survivor of the unseen', 'CSP').
card_image_name('survivor of the unseen'/'CSP', 'survivor of the unseen').
card_uid('survivor of the unseen'/'CSP', 'CSP:Survivor of the Unseen:survivor of the unseen').
card_rarity('survivor of the unseen'/'CSP', 'Common').
card_artist('survivor of the unseen'/'CSP', 'Alan Pollack').
card_number('survivor of the unseen'/'CSP', '48').
card_multiverse_id('survivor of the unseen'/'CSP', '121224').

card_in_set('swift maneuver', 'CSP').
card_original_type('swift maneuver'/'CSP', 'Instant').
card_original_text('swift maneuver'/'CSP', 'Prevent the next 2 damage that would be dealt to target creature or player this turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('swift maneuver', 'CSP').
card_image_name('swift maneuver'/'CSP', 'swift maneuver').
card_uid('swift maneuver'/'CSP', 'CSP:Swift Maneuver:swift maneuver').
card_rarity('swift maneuver'/'CSP', 'Common').
card_artist('swift maneuver'/'CSP', 'Michael Sutfin').
card_number('swift maneuver'/'CSP', '21').
card_flavor_text('swift maneuver'/'CSP', 'Darien\'s call to Yavimaya would have fallen red to the snow had it not been for the agility of the aesthir.').
card_multiverse_id('swift maneuver'/'CSP', '121255').

card_in_set('tamanoa', 'CSP').
card_original_type('tamanoa'/'CSP', 'Creature — Spirit').
card_original_text('tamanoa'/'CSP', 'Whenever a noncreature source you control deals damage, you gain that much life.').
card_first_print('tamanoa', 'CSP').
card_image_name('tamanoa'/'CSP', 'tamanoa').
card_uid('tamanoa'/'CSP', 'CSP:Tamanoa:tamanoa').
card_rarity('tamanoa'/'CSP', 'Rare').
card_artist('tamanoa'/'CSP', 'rk post').
card_number('tamanoa'/'CSP', '132').
card_flavor_text('tamanoa'/'CSP', 'When Freyalise spoke the World Spell, the tamanoa rose to bring a new morning to Terisiare.').
card_multiverse_id('tamanoa'/'CSP', '121262').

card_in_set('thermal flux', 'CSP').
card_original_type('thermal flux'/'CSP', 'Instant').
card_original_text('thermal flux'/'CSP', 'Choose one Target nonsnow permanent becomes snow until end of turn; or target snow permanent isn\'t snow until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_first_print('thermal flux', 'CSP').
card_image_name('thermal flux'/'CSP', 'thermal flux').
card_uid('thermal flux'/'CSP', 'CSP:Thermal Flux:thermal flux').
card_rarity('thermal flux'/'CSP', 'Common').
card_artist('thermal flux'/'CSP', 'Jeff Miracola').
card_number('thermal flux'/'CSP', '49').
card_multiverse_id('thermal flux'/'CSP', '122119').

card_in_set('thermopod', 'CSP').
card_original_type('thermopod'/'CSP', 'Snow Creature — Slug').
card_original_text('thermopod'/'CSP', '{S}: Thermopod gains haste until end of turn. ({S} can be paid with one mana from a snow permanent.)\nSacrifice a creature: Add {R} to your mana pool.').
card_first_print('thermopod', 'CSP').
card_image_name('thermopod'/'CSP', 'thermopod').
card_uid('thermopod'/'CSP', 'CSP:Thermopod:thermopod').
card_rarity('thermopod'/'CSP', 'Common').
card_artist('thermopod'/'CSP', 'Dan Dos Santos').
card_number('thermopod'/'CSP', '100').
card_flavor_text('thermopod'/'CSP', 'Slicking the ice with its heated underside, the thermopod skates at great speed to overtake its prey.').
card_multiverse_id('thermopod'/'CSP', '121173').

card_in_set('thrumming stone', 'CSP').
card_original_type('thrumming stone'/'CSP', 'Legendary Artifact').
card_original_text('thrumming stone'/'CSP', 'Spells you control have ripple 4. (Whenever you play a spell, you may reveal the top four cards of your library. You may play any revealed cards with the same name as the spell without paying their mana costs. Put the rest on the bottom of your library.)').
card_first_print('thrumming stone', 'CSP').
card_image_name('thrumming stone'/'CSP', 'thrumming stone').
card_uid('thrumming stone'/'CSP', 'CSP:Thrumming Stone:thrumming stone').
card_rarity('thrumming stone'/'CSP', 'Rare').
card_artist('thrumming stone'/'CSP', 'Rob Alexander').
card_number('thrumming stone'/'CSP', '142').
card_multiverse_id('thrumming stone'/'CSP', '124451').

card_in_set('tresserhorn sinks', 'CSP').
card_original_type('tresserhorn sinks'/'CSP', 'Snow Land').
card_original_text('tresserhorn sinks'/'CSP', 'Tresserhorn Sinks comes into play tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('tresserhorn sinks', 'CSP').
card_image_name('tresserhorn sinks'/'CSP', 'tresserhorn sinks').
card_uid('tresserhorn sinks'/'CSP', 'CSP:Tresserhorn Sinks:tresserhorn sinks').
card_rarity('tresserhorn sinks'/'CSP', 'Uncommon').
card_artist('tresserhorn sinks'/'CSP', 'Darrell Riche').
card_number('tresserhorn sinks'/'CSP', '150').
card_flavor_text('tresserhorn sinks'/'CSP', '\"Our land oppressed by silent Cold,\nWe weary wretches, young and old,\nPray that Springtime loose his hold.\"\n—The Dynasty of Winter Kings').
card_multiverse_id('tresserhorn sinks'/'CSP', '121125').

card_in_set('tresserhorn skyknight', 'CSP').
card_original_type('tresserhorn skyknight'/'CSP', 'Creature — Zombie Knight').
card_original_text('tresserhorn skyknight'/'CSP', 'Flying\nPrevent all damage that would be dealt to Tresserhorn Skyknight by creatures with first strike.').
card_first_print('tresserhorn skyknight', 'CSP').
card_image_name('tresserhorn skyknight'/'CSP', 'tresserhorn skyknight').
card_uid('tresserhorn skyknight'/'CSP', 'CSP:Tresserhorn Skyknight:tresserhorn skyknight').
card_rarity('tresserhorn skyknight'/'CSP', 'Uncommon').
card_artist('tresserhorn skyknight'/'CSP', 'Dan Dos Santos').
card_number('tresserhorn skyknight'/'CSP', '73').
card_flavor_text('tresserhorn skyknight'/'CSP', 'Kjeldorans who fled battle were exiled to the wastes. Those who strayed near Tresserhorn were pressed into service in Haakon\'s army.').
card_multiverse_id('tresserhorn skyknight'/'CSP', '121195').

card_in_set('ursine fylgja', 'CSP').
card_original_type('ursine fylgja'/'CSP', 'Creature — Spirit Bear').
card_original_text('ursine fylgja'/'CSP', 'Ursine Fylgja comes into play with four healing counters on it.\nRemove a healing counter from Ursine Fylgja: Prevent the next 1 damage that would be dealt to Ursine Fylgja this turn.\n{2}{W}: Put a healing counter on Ursine Fylgja.').
card_first_print('ursine fylgja', 'CSP').
card_image_name('ursine fylgja'/'CSP', 'ursine fylgja').
card_uid('ursine fylgja'/'CSP', 'CSP:Ursine Fylgja:ursine fylgja').
card_rarity('ursine fylgja'/'CSP', 'Uncommon').
card_artist('ursine fylgja'/'CSP', 'Yokota Katsumi').
card_number('ursine fylgja'/'CSP', '22').
card_multiverse_id('ursine fylgja'/'CSP', '121150').

card_in_set('vanish into memory', 'CSP').
card_original_type('vanish into memory'/'CSP', 'Instant').
card_original_text('vanish into memory'/'CSP', 'Remove target creature from the game. You draw cards equal to that creature\'s power. At the beginning of your next upkeep, return that card to play under its owner\'s control. If you do, discard cards equal to its toughness.').
card_first_print('vanish into memory', 'CSP').
card_image_name('vanish into memory'/'CSP', 'vanish into memory').
card_uid('vanish into memory'/'CSP', 'CSP:Vanish into Memory:vanish into memory').
card_rarity('vanish into memory'/'CSP', 'Uncommon').
card_artist('vanish into memory'/'CSP', 'Rebekah Lynn').
card_number('vanish into memory'/'CSP', '133').
card_flavor_text('vanish into memory'/'CSP', 'Out of sight, into mind.').
card_multiverse_id('vanish into memory'/'CSP', '121187').

card_in_set('vexing sphinx', 'CSP').
card_original_type('vexing sphinx'/'CSP', 'Creature — Sphinx').
card_original_text('vexing sphinx'/'CSP', 'Flying\nCumulative upkeep—Discard a card. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Vexing Sphinx is put into a graveyard from play, draw a card for each age counter on it.').
card_first_print('vexing sphinx', 'CSP').
card_image_name('vexing sphinx'/'CSP', 'vexing sphinx').
card_uid('vexing sphinx'/'CSP', 'CSP:Vexing Sphinx:vexing sphinx').
card_rarity('vexing sphinx'/'CSP', 'Rare').
card_artist('vexing sphinx'/'CSP', 'Lars Grant-West').
card_number('vexing sphinx'/'CSP', '50').
card_multiverse_id('vexing sphinx'/'CSP', '122071').

card_in_set('void maw', 'CSP').
card_original_type('void maw'/'CSP', 'Creature — Horror').
card_original_text('void maw'/'CSP', 'Trample\nIf another creature would be put into a graveyard from play, remove it from the game instead.\nPut a card removed from the game with Void Maw into its owner\'s graveyard: Void Maw gets +2/+2 until end of turn.').
card_first_print('void maw', 'CSP').
card_image_name('void maw'/'CSP', 'void maw').
card_uid('void maw'/'CSP', 'CSP:Void Maw:void maw').
card_rarity('void maw'/'CSP', 'Rare').
card_artist('void maw'/'CSP', 'E. M. Gist').
card_number('void maw'/'CSP', '74').
card_multiverse_id('void maw'/'CSP', '121129').

card_in_set('wall of shards', 'CSP').
card_original_type('wall of shards'/'CSP', 'Snow Creature — Wall').
card_original_text('wall of shards'/'CSP', 'Defender, flying\nCumulative upkeep—An opponent gains 1 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_first_print('wall of shards', 'CSP').
card_image_name('wall of shards'/'CSP', 'wall of shards').
card_uid('wall of shards'/'CSP', 'CSP:Wall of Shards:wall of shards').
card_rarity('wall of shards'/'CSP', 'Uncommon').
card_artist('wall of shards'/'CSP', 'Alex Horley-Orlandelli').
card_number('wall of shards'/'CSP', '23').
card_multiverse_id('wall of shards'/'CSP', '121225').

card_in_set('white shield crusader', 'CSP').
card_original_type('white shield crusader'/'CSP', 'Creature — Human Knight').
card_original_text('white shield crusader'/'CSP', 'Protection from black\n{W}: White Shield Crusader gains flying until end of turn.\n{W}{W}: White Shield Crusader gets +1/+0 until end of turn.').
card_first_print('white shield crusader', 'CSP').
card_image_name('white shield crusader'/'CSP', 'white shield crusader').
card_uid('white shield crusader'/'CSP', 'CSP:White Shield Crusader:white shield crusader').
card_rarity('white shield crusader'/'CSP', 'Uncommon').
card_artist('white shield crusader'/'CSP', 'Jeff Easley').
card_number('white shield crusader'/'CSP', '24').
card_flavor_text('white shield crusader'/'CSP', '\"Honor your steed and it will carry you to great deeds—and bring you home to tell of them.\"').
card_multiverse_id('white shield crusader'/'CSP', '121216').

card_in_set('wilderness elemental', 'CSP').
card_original_type('wilderness elemental'/'CSP', 'Creature — Elemental').
card_original_text('wilderness elemental'/'CSP', 'Trample\nWilderness Elemental\'s power is equal to the number of nonbasic lands your opponents control.').
card_first_print('wilderness elemental', 'CSP').
card_image_name('wilderness elemental'/'CSP', 'wilderness elemental').
card_uid('wilderness elemental'/'CSP', 'CSP:Wilderness Elemental:wilderness elemental').
card_rarity('wilderness elemental'/'CSP', 'Uncommon').
card_artist('wilderness elemental'/'CSP', 'Anthony S. Waters').
card_number('wilderness elemental'/'CSP', '134').
card_flavor_text('wilderness elemental'/'CSP', '\"Don\'t be so quick to reclaim the thawing lands. They might have acquired a taste for freedom.\"\n—Taaveti of Kelsinko, elvish hunter').
card_multiverse_id('wilderness elemental'/'CSP', '121191').

card_in_set('woolly razorback', 'CSP').
card_original_type('woolly razorback'/'CSP', 'Creature — Beast').
card_original_text('woolly razorback'/'CSP', 'Woolly Razorback comes into play with three ice counters on it.\nAs long as Woolly Razorback has an ice counter on it, it has defender and any combat damage it would deal is prevented.\nWhenever Woolly Razorback blocks, remove an ice counter from it.').
card_first_print('woolly razorback', 'CSP').
card_image_name('woolly razorback'/'CSP', 'woolly razorback').
card_uid('woolly razorback'/'CSP', 'CSP:Woolly Razorback:woolly razorback').
card_rarity('woolly razorback'/'CSP', 'Rare').
card_artist('woolly razorback'/'CSP', 'Richard Sardinha').
card_number('woolly razorback'/'CSP', '25').
card_multiverse_id('woolly razorback'/'CSP', '121230').

card_in_set('zombie musher', 'CSP').
card_original_type('zombie musher'/'CSP', 'Snow Creature — Zombie').
card_original_text('zombie musher'/'CSP', 'Snow landwalk\n{S}: Regenerate Zombie Musher. ({S} can be paid with one mana from a snow permanent.)').
card_first_print('zombie musher', 'CSP').
card_image_name('zombie musher'/'CSP', 'zombie musher').
card_uid('zombie musher'/'CSP', 'CSP:Zombie Musher:zombie musher').
card_rarity('zombie musher'/'CSP', 'Common').
card_artist('zombie musher'/'CSP', 'Kev Walker').
card_number('zombie musher'/'CSP', '75').
card_flavor_text('zombie musher'/'CSP', 'The World Spell thawed their icy graves, promising rest in soft, quiet earth at last. But Heidar called them to duty for a twisted cause.').
card_multiverse_id('zombie musher'/'CSP', '121154').

card_in_set('zur the enchanter', 'CSP').
card_original_type('zur the enchanter'/'CSP', 'Legendary Creature — Human Wizard').
card_original_text('zur the enchanter'/'CSP', 'Flying\nWhenever Zur the Enchanter attacks, you may search your library for an enchantment card with converted mana cost 3 or less and put it into play. If you do, shuffle your library.').
card_first_print('zur the enchanter', 'CSP').
card_image_name('zur the enchanter'/'CSP', 'zur the enchanter').
card_uid('zur the enchanter'/'CSP', 'CSP:Zur the Enchanter:zur the enchanter').
card_rarity('zur the enchanter'/'CSP', 'Rare').
card_artist('zur the enchanter'/'CSP', 'Pete Venters').
card_number('zur the enchanter'/'CSP', '135').
card_flavor_text('zur the enchanter'/'CSP', 'Zur remained aloof from Terisiare\'s suffering, intent only on his own perfection.').
card_multiverse_id('zur the enchanter'/'CSP', '121162').
