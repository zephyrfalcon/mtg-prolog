% Khans of Tarkir

set('KTK').
set_name('KTK', 'Khans of Tarkir').
set_release_date('KTK', '2014-09-26').
set_border('KTK', 'black').
set_type('KTK', 'expansion').
set_block('KTK', 'Khans of Tarkir').

card_in_set('abomination of gudul', 'KTK').
card_original_type('abomination of gudul'/'KTK', 'Creature — Horror').
card_original_text('abomination of gudul'/'KTK', 'Flying\nWhenever Abomination of Gudul deals combat damage to a player, you may draw a card. If you do, discard a card.\nMorph {2}{B}{G}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('abomination of gudul', 'KTK').
card_image_name('abomination of gudul'/'KTK', 'abomination of gudul').
card_uid('abomination of gudul'/'KTK', 'KTK:Abomination of Gudul:abomination of gudul').
card_rarity('abomination of gudul'/'KTK', 'Common').
card_artist('abomination of gudul'/'KTK', 'Erica Yang').
card_number('abomination of gudul'/'KTK', '159').
card_multiverse_id('abomination of gudul'/'KTK', '386463').
card_watermark('abomination of gudul'/'KTK', 'Sultai').

card_in_set('abzan ascendancy', 'KTK').
card_original_type('abzan ascendancy'/'KTK', 'Enchantment').
card_original_text('abzan ascendancy'/'KTK', 'When Abzan Ascendancy enters the battlefield, put a +1/+1 counter on each creature you control.\nWhenever a nontoken creature you control dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_image_name('abzan ascendancy'/'KTK', 'abzan ascendancy').
card_uid('abzan ascendancy'/'KTK', 'KTK:Abzan Ascendancy:abzan ascendancy').
card_rarity('abzan ascendancy'/'KTK', 'Rare').
card_artist('abzan ascendancy'/'KTK', 'Mark Winters').
card_number('abzan ascendancy'/'KTK', '160').
card_multiverse_id('abzan ascendancy'/'KTK', '386464').
card_watermark('abzan ascendancy'/'KTK', 'Abzan').

card_in_set('abzan banner', 'KTK').
card_original_type('abzan banner'/'KTK', 'Artifact').
card_original_text('abzan banner'/'KTK', '{T}: Add {W}, {B}, or {G} to your mana pool.\n{W}{B}{G}, {T}, Sacrifice Abzan Banner: Draw a card.').
card_first_print('abzan banner', 'KTK').
card_image_name('abzan banner'/'KTK', 'abzan banner').
card_uid('abzan banner'/'KTK', 'KTK:Abzan Banner:abzan banner').
card_rarity('abzan banner'/'KTK', 'Common').
card_artist('abzan banner'/'KTK', 'Daniel Ljunggren').
card_number('abzan banner'/'KTK', '215').
card_flavor_text('abzan banner'/'KTK', 'Stone to endure, roots to remember.').
card_multiverse_id('abzan banner'/'KTK', '386465').
card_watermark('abzan banner'/'KTK', 'Abzan').

card_in_set('abzan battle priest', 'KTK').
card_original_type('abzan battle priest'/'KTK', 'Creature — Human Cleric').
card_original_text('abzan battle priest'/'KTK', 'Outlast {W} ({W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has lifelink.').
card_first_print('abzan battle priest', 'KTK').
card_image_name('abzan battle priest'/'KTK', 'abzan battle priest').
card_uid('abzan battle priest'/'KTK', 'KTK:Abzan Battle Priest:abzan battle priest').
card_rarity('abzan battle priest'/'KTK', 'Uncommon').
card_artist('abzan battle priest'/'KTK', 'Chris Rahn').
card_number('abzan battle priest'/'KTK', '1').
card_flavor_text('abzan battle priest'/'KTK', '\"Wherever I walk, the ancestors walk too.\"').
card_multiverse_id('abzan battle priest'/'KTK', '386466').
card_watermark('abzan battle priest'/'KTK', 'Abzan').

card_in_set('abzan charm', 'KTK').
card_original_type('abzan charm'/'KTK', 'Instant').
card_original_text('abzan charm'/'KTK', 'Choose one —\n• Exile target creature with power 3 or greater.\n• You draw two cards and you lose 2 life.\n• Distribute two +1/+1 counters among one or two target creatures.').
card_first_print('abzan charm', 'KTK').
card_image_name('abzan charm'/'KTK', 'abzan charm').
card_uid('abzan charm'/'KTK', 'KTK:Abzan Charm:abzan charm').
card_rarity('abzan charm'/'KTK', 'Uncommon').
card_artist('abzan charm'/'KTK', 'Mathias Kollros').
card_number('abzan charm'/'KTK', '161').
card_multiverse_id('abzan charm'/'KTK', '386467').
card_watermark('abzan charm'/'KTK', 'Abzan').

card_in_set('abzan falconer', 'KTK').
card_original_type('abzan falconer'/'KTK', 'Creature — Human Soldier').
card_original_text('abzan falconer'/'KTK', 'Outlast {W} ({W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has flying.').
card_first_print('abzan falconer', 'KTK').
card_image_name('abzan falconer'/'KTK', 'abzan falconer').
card_uid('abzan falconer'/'KTK', 'KTK:Abzan Falconer:abzan falconer').
card_rarity('abzan falconer'/'KTK', 'Uncommon').
card_artist('abzan falconer'/'KTK', 'Steven Belledin').
card_number('abzan falconer'/'KTK', '2').
card_flavor_text('abzan falconer'/'KTK', 'The fastest way across the dunes is above.').
card_multiverse_id('abzan falconer'/'KTK', '386468').
card_watermark('abzan falconer'/'KTK', 'Abzan').

card_in_set('abzan guide', 'KTK').
card_original_type('abzan guide'/'KTK', 'Creature — Human Warrior').
card_original_text('abzan guide'/'KTK', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nMorph {2}{W}{B}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('abzan guide', 'KTK').
card_image_name('abzan guide'/'KTK', 'abzan guide').
card_uid('abzan guide'/'KTK', 'KTK:Abzan Guide:abzan guide').
card_rarity('abzan guide'/'KTK', 'Common').
card_artist('abzan guide'/'KTK', 'Steve Prescott').
card_number('abzan guide'/'KTK', '162').
card_flavor_text('abzan guide'/'KTK', '\"These roads are desolate and changeable. Follow me, or die in the wastes.\"').
card_multiverse_id('abzan guide'/'KTK', '386469').
card_watermark('abzan guide'/'KTK', 'Abzan').

card_in_set('act of treason', 'KTK').
card_original_type('act of treason'/'KTK', 'Sorcery').
card_original_text('act of treason'/'KTK', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_image_name('act of treason'/'KTK', 'act of treason').
card_uid('act of treason'/'KTK', 'KTK:Act of Treason:act of treason').
card_rarity('act of treason'/'KTK', 'Common').
card_artist('act of treason'/'KTK', 'Min Yum').
card_number('act of treason'/'KTK', '95').
card_flavor_text('act of treason'/'KTK', '\"The Sultai take our dead, so we shall take their living!\"\n—Taklai, Mardu ragesinger').
card_multiverse_id('act of treason'/'KTK', '386470').

card_in_set('ainok bond-kin', 'KTK').
card_original_type('ainok bond-kin'/'KTK', 'Creature — Hound Soldier').
card_original_text('ainok bond-kin'/'KTK', 'Outlast {1}{W} ({1}{W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has first strike.').
card_first_print('ainok bond-kin', 'KTK').
card_image_name('ainok bond-kin'/'KTK', 'ainok bond-kin').
card_uid('ainok bond-kin'/'KTK', 'KTK:Ainok Bond-Kin:ainok bond-kin').
card_rarity('ainok bond-kin'/'KTK', 'Common').
card_artist('ainok bond-kin'/'KTK', 'Chris Rahn').
card_number('ainok bond-kin'/'KTK', '3').
card_flavor_text('ainok bond-kin'/'KTK', '\"Hold the line, for family and the fallen!\"').
card_multiverse_id('ainok bond-kin'/'KTK', '386471').
card_watermark('ainok bond-kin'/'KTK', 'Abzan').

card_in_set('ainok tracker', 'KTK').
card_original_type('ainok tracker'/'KTK', 'Creature — Hound Scout').
card_original_text('ainok tracker'/'KTK', 'First strike\nMorph {4}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('ainok tracker', 'KTK').
card_image_name('ainok tracker'/'KTK', 'ainok tracker').
card_uid('ainok tracker'/'KTK', 'KTK:Ainok Tracker:ainok tracker').
card_rarity('ainok tracker'/'KTK', 'Common').
card_artist('ainok tracker'/'KTK', 'Evan Shipard').
card_number('ainok tracker'/'KTK', '96').
card_flavor_text('ainok tracker'/'KTK', 'Some ainok of the mountains are accepted among the Temur as trusted hunt-mates.').
card_multiverse_id('ainok tracker'/'KTK', '386472').
card_watermark('ainok tracker'/'KTK', 'Temur').

card_in_set('alabaster kirin', 'KTK').
card_original_type('alabaster kirin'/'KTK', 'Creature — Kirin').
card_original_text('alabaster kirin'/'KTK', 'Flying, vigilance').
card_first_print('alabaster kirin', 'KTK').
card_image_name('alabaster kirin'/'KTK', 'alabaster kirin').
card_uid('alabaster kirin'/'KTK', 'KTK:Alabaster Kirin:alabaster kirin').
card_rarity('alabaster kirin'/'KTK', 'Common').
card_artist('alabaster kirin'/'KTK', 'Igor Kieryluk').
card_number('alabaster kirin'/'KTK', '4').
card_flavor_text('alabaster kirin'/'KTK', 'The appearance of a kirin signifies the passing or arrival of an important figure. As word of sightings spread, all the khans took it to mean themselves. Only the shaman Chianul thought of Sarkhan Vol.').
card_multiverse_id('alabaster kirin'/'KTK', '386473').

card_in_set('alpine grizzly', 'KTK').
card_original_type('alpine grizzly'/'KTK', 'Creature — Bear').
card_original_text('alpine grizzly'/'KTK', '').
card_first_print('alpine grizzly', 'KTK').
card_image_name('alpine grizzly'/'KTK', 'alpine grizzly').
card_uid('alpine grizzly'/'KTK', 'KTK:Alpine Grizzly:alpine grizzly').
card_rarity('alpine grizzly'/'KTK', 'Common').
card_artist('alpine grizzly'/'KTK', 'John Severin Brassell').
card_number('alpine grizzly'/'KTK', '127').
card_flavor_text('alpine grizzly'/'KTK', 'The Temur welcome bears into the clan, fighting alongside them in battle. The relationship dates back to when they labored side by side under Sultai rule.').
card_multiverse_id('alpine grizzly'/'KTK', '386474').

card_in_set('altar of the brood', 'KTK').
card_original_type('altar of the brood'/'KTK', 'Artifact').
card_original_text('altar of the brood'/'KTK', 'Whenever another permanent enters the battlefield under your control, each opponent puts the top card of his or her library into his or her graveyard.').
card_first_print('altar of the brood', 'KTK').
card_image_name('altar of the brood'/'KTK', 'altar of the brood').
card_uid('altar of the brood'/'KTK', 'KTK:Altar of the Brood:altar of the brood').
card_rarity('altar of the brood'/'KTK', 'Rare').
card_artist('altar of the brood'/'KTK', 'Erica Yang').
card_number('altar of the brood'/'KTK', '216').
card_flavor_text('altar of the brood'/'KTK', 'Supplicants offer flesh and silver, flowers and blood. The altar takes what it will, eyes gleaming with unspoken promises.').
card_multiverse_id('altar of the brood'/'KTK', '386475').

card_in_set('anafenza, the foremost', 'KTK').
card_original_type('anafenza, the foremost'/'KTK', 'Legendary Creature — Human Soldier').
card_original_text('anafenza, the foremost'/'KTK', 'Whenever Anafenza, the Foremost attacks, put a +1/+1 counter on another target tapped creature you control.\nIf a creature card would be put into an opponent\'s graveyard from anywhere, exile it instead.').
card_image_name('anafenza, the foremost'/'KTK', 'anafenza, the foremost').
card_uid('anafenza, the foremost'/'KTK', 'KTK:Anafenza, the Foremost:anafenza, the foremost').
card_rarity('anafenza, the foremost'/'KTK', 'Mythic Rare').
card_artist('anafenza, the foremost'/'KTK', 'James Ryman').
card_number('anafenza, the foremost'/'KTK', '163').
card_flavor_text('anafenza, the foremost'/'KTK', 'Rarely at rest on the Amber Throne, Anafenza always leads the Abzan Houses to battle.').
card_multiverse_id('anafenza, the foremost'/'KTK', '386476').
card_watermark('anafenza, the foremost'/'KTK', 'Abzan').

card_in_set('ankle shanker', 'KTK').
card_original_type('ankle shanker'/'KTK', 'Creature — Goblin Berserker').
card_original_text('ankle shanker'/'KTK', 'Haste\nWhenever Ankle Shanker attacks, creatures you control gain first strike and deathtouch until end of turn.').
card_image_name('ankle shanker'/'KTK', 'ankle shanker').
card_uid('ankle shanker'/'KTK', 'KTK:Ankle Shanker:ankle shanker').
card_rarity('ankle shanker'/'KTK', 'Rare').
card_artist('ankle shanker'/'KTK', 'Zoltan Boros').
card_number('ankle shanker'/'KTK', '164').
card_flavor_text('ankle shanker'/'KTK', 'The stature of the fighter matters less than the depth of the cut.').
card_multiverse_id('ankle shanker'/'KTK', '386477').
card_watermark('ankle shanker'/'KTK', 'Mardu').

card_in_set('arc lightning', 'KTK').
card_original_type('arc lightning'/'KTK', 'Sorcery').
card_original_text('arc lightning'/'KTK', 'Arc Lightning deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_image_name('arc lightning'/'KTK', 'arc lightning').
card_uid('arc lightning'/'KTK', 'KTK:Arc Lightning:arc lightning').
card_rarity('arc lightning'/'KTK', 'Uncommon').
card_artist('arc lightning'/'KTK', 'Seb McKinnon').
card_number('arc lightning'/'KTK', '97').
card_flavor_text('arc lightning'/'KTK', 'Lightning burns its own path.').
card_multiverse_id('arc lightning'/'KTK', '386478').

card_in_set('archers\' parapet', 'KTK').
card_original_type('archers\' parapet'/'KTK', 'Creature — Wall').
card_original_text('archers\' parapet'/'KTK', 'Defender\n{1}{B}, {T}: Each opponent loses 1 life.').
card_first_print('archers\' parapet', 'KTK').
card_image_name('archers\' parapet'/'KTK', 'archers\' parapet').
card_uid('archers\' parapet'/'KTK', 'KTK:Archers\' Parapet:archers\' parapet').
card_rarity('archers\' parapet'/'KTK', 'Common').
card_artist('archers\' parapet'/'KTK', 'Wayne Reynolds').
card_number('archers\' parapet'/'KTK', '128').
card_flavor_text('archers\' parapet'/'KTK', 'Every shaft is graven with a name from a kin tree, calling upon the spirits of the ancestors to make it fly true.').
card_multiverse_id('archers\' parapet'/'KTK', '386479').
card_watermark('archers\' parapet'/'KTK', 'Abzan').

card_in_set('armament corps', 'KTK').
card_original_type('armament corps'/'KTK', 'Creature — Human Soldier').
card_original_text('armament corps'/'KTK', 'When Armament Corps enters the battlefield, distribute two +1/+1 counters among one or two target creatures you control.').
card_first_print('armament corps', 'KTK').
card_image_name('armament corps'/'KTK', 'armament corps').
card_uid('armament corps'/'KTK', 'KTK:Armament Corps:armament corps').
card_rarity('armament corps'/'KTK', 'Uncommon').
card_artist('armament corps'/'KTK', 'Steven Belledin').
card_number('armament corps'/'KTK', '165').
card_flavor_text('armament corps'/'KTK', 'The Abzan avoid extended supply lines by incorporating weapons stores into their battle formations.').
card_multiverse_id('armament corps'/'KTK', '386480').
card_watermark('armament corps'/'KTK', 'Abzan').

card_in_set('arrow storm', 'KTK').
card_original_type('arrow storm'/'KTK', 'Sorcery').
card_original_text('arrow storm'/'KTK', 'Arrow Storm deals 4 damage to target creature or player.\nRaid — If you attacked with a creature this turn, instead Arrow Storm deals 5 damage to that creature or player and the damage can\'t be prevented.').
card_first_print('arrow storm', 'KTK').
card_image_name('arrow storm'/'KTK', 'arrow storm').
card_uid('arrow storm'/'KTK', 'KTK:Arrow Storm:arrow storm').
card_rarity('arrow storm'/'KTK', 'Common').
card_artist('arrow storm'/'KTK', 'Steve Prescott').
card_number('arrow storm'/'KTK', '98').
card_flavor_text('arrow storm'/'KTK', 'First the thunder, then the rain.').
card_multiverse_id('arrow storm'/'KTK', '386481').
card_watermark('arrow storm'/'KTK', 'Mardu').

card_in_set('ashcloud phoenix', 'KTK').
card_original_type('ashcloud phoenix'/'KTK', 'Creature — Phoenix').
card_original_text('ashcloud phoenix'/'KTK', 'Flying\nWhen Ashcloud Phoenix dies, return it to the battlefield face down.\nMorph {4}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Ashcloud Phoenix is turned face up, it deals 2 damage to each player.').
card_first_print('ashcloud phoenix', 'KTK').
card_image_name('ashcloud phoenix'/'KTK', 'ashcloud phoenix').
card_uid('ashcloud phoenix'/'KTK', 'KTK:Ashcloud Phoenix:ashcloud phoenix').
card_rarity('ashcloud phoenix'/'KTK', 'Mythic Rare').
card_artist('ashcloud phoenix'/'KTK', 'Howard Lyon').
card_number('ashcloud phoenix'/'KTK', '99').
card_multiverse_id('ashcloud phoenix'/'KTK', '386482').

card_in_set('avalanche tusker', 'KTK').
card_original_type('avalanche tusker'/'KTK', 'Creature — Elephant Warrior').
card_original_text('avalanche tusker'/'KTK', 'Whenever Avalanche Tusker attacks, target creature defending player controls blocks it this combat if able.').
card_image_name('avalanche tusker'/'KTK', 'avalanche tusker').
card_uid('avalanche tusker'/'KTK', 'KTK:Avalanche Tusker:avalanche tusker').
card_rarity('avalanche tusker'/'KTK', 'Rare').
card_artist('avalanche tusker'/'KTK', 'Matt Stewart').
card_number('avalanche tusker'/'KTK', '166').
card_flavor_text('avalanche tusker'/'KTK', '\"Hold the high ground, then bring it to your enemy.\"\n—Surrak, khan of the Temur').
card_multiverse_id('avalanche tusker'/'KTK', '386483').
card_watermark('avalanche tusker'/'KTK', 'Temur').

card_in_set('awaken the bear', 'KTK').
card_original_type('awaken the bear'/'KTK', 'Instant').
card_original_text('awaken the bear'/'KTK', 'Target creature gets +3/+3 and gains trample until end of turn.').
card_first_print('awaken the bear', 'KTK').
card_image_name('awaken the bear'/'KTK', 'awaken the bear').
card_uid('awaken the bear'/'KTK', 'KTK:Awaken the Bear:awaken the bear').
card_rarity('awaken the bear'/'KTK', 'Common').
card_artist('awaken the bear'/'KTK', 'Svetlin Velinov').
card_number('awaken the bear'/'KTK', '129').
card_flavor_text('awaken the bear'/'KTK', 'When Temur warriors enter the battle trance known as \"awakening the bear,\" they lose all sense of enemy or friend, seeing only threats to the wilderness.').
card_multiverse_id('awaken the bear'/'KTK', '386484').

card_in_set('barrage of boulders', 'KTK').
card_original_type('barrage of boulders'/'KTK', 'Sorcery').
card_original_text('barrage of boulders'/'KTK', 'Barrage of Boulders deals 1 damage to each creature you don\'t control.\nFerocious — If you control a creature with power 4 or greater, creatures can\'t block this turn.').
card_first_print('barrage of boulders', 'KTK').
card_image_name('barrage of boulders'/'KTK', 'barrage of boulders').
card_uid('barrage of boulders'/'KTK', 'KTK:Barrage of Boulders:barrage of boulders').
card_rarity('barrage of boulders'/'KTK', 'Common').
card_artist('barrage of boulders'/'KTK', 'Zoltan Boros').
card_number('barrage of boulders'/'KTK', '100').
card_flavor_text('barrage of boulders'/'KTK', 'Crude tactics can be effective nonetheless.').
card_multiverse_id('barrage of boulders'/'KTK', '386485').
card_watermark('barrage of boulders'/'KTK', 'Temur').

card_in_set('bear\'s companion', 'KTK').
card_original_type('bear\'s companion'/'KTK', 'Creature — Human Warrior').
card_original_text('bear\'s companion'/'KTK', 'When Bear\'s Companion enters the battlefield, put a 4/4 green Bear creature token onto the battlefield.').
card_first_print('bear\'s companion', 'KTK').
card_image_name('bear\'s companion'/'KTK', 'bear\'s companion').
card_uid('bear\'s companion'/'KTK', 'KTK:Bear\'s Companion:bear\'s companion').
card_rarity('bear\'s companion'/'KTK', 'Uncommon').
card_artist('bear\'s companion'/'KTK', 'Winona Nelson').
card_number('bear\'s companion'/'KTK', '167').
card_flavor_text('bear\'s companion'/'KTK', '\"The Sultai came hunting for a bear hide. Now I have a belt of naga skin, and my friend has a full belly.\"').
card_multiverse_id('bear\'s companion'/'KTK', '386486').
card_watermark('bear\'s companion'/'KTK', 'Temur').

card_in_set('become immense', 'KTK').
card_original_type('become immense'/'KTK', 'Instant').
card_original_text('become immense'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTarget creature gets +6/+6 until end of turn.').
card_first_print('become immense', 'KTK').
card_image_name('become immense'/'KTK', 'become immense').
card_uid('become immense'/'KTK', 'KTK:Become Immense:become immense').
card_rarity('become immense'/'KTK', 'Uncommon').
card_artist('become immense'/'KTK', 'Jaime Jones').
card_number('become immense'/'KTK', '130').
card_multiverse_id('become immense'/'KTK', '386487').
card_watermark('become immense'/'KTK', 'Sultai').

card_in_set('bellowing saddlebrute', 'KTK').
card_original_type('bellowing saddlebrute'/'KTK', 'Creature — Orc Warrior').
card_original_text('bellowing saddlebrute'/'KTK', 'Raid — When Bellowing Saddlebrute enters the battlefield, you lose 4 life unless you attacked with a creature this turn.').
card_first_print('bellowing saddlebrute', 'KTK').
card_image_name('bellowing saddlebrute'/'KTK', 'bellowing saddlebrute').
card_uid('bellowing saddlebrute'/'KTK', 'KTK:Bellowing Saddlebrute:bellowing saddlebrute').
card_rarity('bellowing saddlebrute'/'KTK', 'Uncommon').
card_artist('bellowing saddlebrute'/'KTK', 'Torstein Nordstrand').
card_number('bellowing saddlebrute'/'KTK', '64').
card_multiverse_id('bellowing saddlebrute'/'KTK', '386488').
card_watermark('bellowing saddlebrute'/'KTK', 'Mardu').

card_in_set('bitter revelation', 'KTK').
card_original_type('bitter revelation'/'KTK', 'Sorcery').
card_original_text('bitter revelation'/'KTK', 'Look at the top four cards of your library. Put two of them into your hand and the rest into your graveyard. You lose 2 life.').
card_first_print('bitter revelation', 'KTK').
card_image_name('bitter revelation'/'KTK', 'bitter revelation').
card_uid('bitter revelation'/'KTK', 'KTK:Bitter Revelation:bitter revelation').
card_rarity('bitter revelation'/'KTK', 'Common').
card_artist('bitter revelation'/'KTK', 'Viktor Titov').
card_number('bitter revelation'/'KTK', '65').
card_flavor_text('bitter revelation'/'KTK', '\"Here you lie then, Ugin. The corpses of worlds will join you in the tomb.\"\n—Sorin Markov').
card_multiverse_id('bitter revelation'/'KTK', '386489').

card_in_set('blinding spray', 'KTK').
card_original_type('blinding spray'/'KTK', 'Instant').
card_original_text('blinding spray'/'KTK', 'Creatures your opponents control get -4/-0 until end of turn.\nDraw a card.').
card_first_print('blinding spray', 'KTK').
card_image_name('blinding spray'/'KTK', 'blinding spray').
card_uid('blinding spray'/'KTK', 'KTK:Blinding Spray:blinding spray').
card_rarity('blinding spray'/'KTK', 'Uncommon').
card_artist('blinding spray'/'KTK', 'Wayne Reynolds').
card_number('blinding spray'/'KTK', '32').
card_flavor_text('blinding spray'/'KTK', '\"The stronger our enemies seem, the more vulnerable they are.\"\n—Sultai secret').
card_multiverse_id('blinding spray'/'KTK', '386490').

card_in_set('bloodfell caves', 'KTK').
card_original_type('bloodfell caves'/'KTK', 'Land').
card_original_text('bloodfell caves'/'KTK', 'Bloodfell Caves enters the battlefield tapped.\nWhen Bloodfell Caves enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_first_print('bloodfell caves', 'KTK').
card_image_name('bloodfell caves'/'KTK', 'bloodfell caves').
card_uid('bloodfell caves'/'KTK', 'KTK:Bloodfell Caves:bloodfell caves').
card_rarity('bloodfell caves'/'KTK', 'Common').
card_artist('bloodfell caves'/'KTK', 'Adam Paquette').
card_number('bloodfell caves'/'KTK', '229').
card_multiverse_id('bloodfell caves'/'KTK', '386491').

card_in_set('bloodfire expert', 'KTK').
card_original_type('bloodfire expert'/'KTK', 'Creature — Efreet Monk').
card_original_text('bloodfire expert'/'KTK', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('bloodfire expert', 'KTK').
card_image_name('bloodfire expert'/'KTK', 'bloodfire expert').
card_uid('bloodfire expert'/'KTK', 'KTK:Bloodfire Expert:bloodfire expert').
card_rarity('bloodfire expert'/'KTK', 'Common').
card_artist('bloodfire expert'/'KTK', 'Raymond Swanland').
card_number('bloodfire expert'/'KTK', '101').
card_flavor_text('bloodfire expert'/'KTK', 'Some efreet abandon their homes in the volcanic Fire Rim to embrace the Jeskai Way and discipline their innate flames.').
card_multiverse_id('bloodfire expert'/'KTK', '386492').
card_watermark('bloodfire expert'/'KTK', 'Jeskai').

card_in_set('bloodfire mentor', 'KTK').
card_original_type('bloodfire mentor'/'KTK', 'Creature — Efreet Shaman').
card_original_text('bloodfire mentor'/'KTK', '{2}{U}, {T}: Draw a card, then discard a card.').
card_first_print('bloodfire mentor', 'KTK').
card_image_name('bloodfire mentor'/'KTK', 'bloodfire mentor').
card_uid('bloodfire mentor'/'KTK', 'KTK:Bloodfire Mentor:bloodfire mentor').
card_rarity('bloodfire mentor'/'KTK', 'Common').
card_artist('bloodfire mentor'/'KTK', 'Chase Stone').
card_number('bloodfire mentor'/'KTK', '102').
card_flavor_text('bloodfire mentor'/'KTK', 'The adept underwent months of preparation to withstand pain, until he was finally ready to receive the efreet master\'s teachings.').
card_multiverse_id('bloodfire mentor'/'KTK', '386493').
card_watermark('bloodfire mentor'/'KTK', 'Jeskai').

card_in_set('bloodsoaked champion', 'KTK').
card_original_type('bloodsoaked champion'/'KTK', 'Creature — Human Warrior').
card_original_text('bloodsoaked champion'/'KTK', 'Bloodsoaked Champion can\'t block.\nRaid — {1}{B}: Return Bloodsoaked Champion from your graveyard to the battlefield. Activate this ability only if you attacked with a creature this turn.').
card_image_name('bloodsoaked champion'/'KTK', 'bloodsoaked champion').
card_uid('bloodsoaked champion'/'KTK', 'KTK:Bloodsoaked Champion:bloodsoaked champion').
card_rarity('bloodsoaked champion'/'KTK', 'Rare').
card_artist('bloodsoaked champion'/'KTK', 'Aaron Miller').
card_number('bloodsoaked champion'/'KTK', '66').
card_flavor_text('bloodsoaked champion'/'KTK', '\"Death is merely another foe the Mardu will overcome.\"').
card_multiverse_id('bloodsoaked champion'/'KTK', '386494').
card_watermark('bloodsoaked champion'/'KTK', 'Mardu').

card_in_set('bloodstained mire', 'KTK').
card_original_type('bloodstained mire'/'KTK', 'Land').
card_original_text('bloodstained mire'/'KTK', '{T}, Pay 1 life, Sacrifice Bloodstained Mire: Search your library for a Swamp or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_image_name('bloodstained mire'/'KTK', 'bloodstained mire').
card_uid('bloodstained mire'/'KTK', 'KTK:Bloodstained Mire:bloodstained mire').
card_rarity('bloodstained mire'/'KTK', 'Rare').
card_artist('bloodstained mire'/'KTK', 'Daarken').
card_number('bloodstained mire'/'KTK', '230').
card_flavor_text('bloodstained mire'/'KTK', 'Where dragons once triumphed, their bones now molder.').
card_multiverse_id('bloodstained mire'/'KTK', '386495').

card_in_set('blossoming sands', 'KTK').
card_original_type('blossoming sands'/'KTK', 'Land').
card_original_text('blossoming sands'/'KTK', 'Blossoming Sands enters the battlefield tapped.\nWhen Blossoming Sands enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_first_print('blossoming sands', 'KTK').
card_image_name('blossoming sands'/'KTK', 'blossoming sands').
card_uid('blossoming sands'/'KTK', 'KTK:Blossoming Sands:blossoming sands').
card_rarity('blossoming sands'/'KTK', 'Common').
card_artist('blossoming sands'/'KTK', 'Sam Burley').
card_number('blossoming sands'/'KTK', '231').
card_multiverse_id('blossoming sands'/'KTK', '386496').

card_in_set('brave the sands', 'KTK').
card_original_type('brave the sands'/'KTK', 'Enchantment').
card_original_text('brave the sands'/'KTK', 'Creatures you control have vigilance.\nEach creature you control can block an additional creature.').
card_first_print('brave the sands', 'KTK').
card_image_name('brave the sands'/'KTK', 'brave the sands').
card_uid('brave the sands'/'KTK', 'KTK:Brave the Sands:brave the sands').
card_rarity('brave the sands'/'KTK', 'Uncommon').
card_artist('brave the sands'/'KTK', 'Dave Kendall').
card_number('brave the sands'/'KTK', '5').
card_flavor_text('brave the sands'/'KTK', 'Enduring the most desolate and dangerous conditions, Abzan sentries unfailingly guard the stronghold gates.').
card_multiverse_id('brave the sands'/'KTK', '386497').

card_in_set('briber\'s purse', 'KTK').
card_original_type('briber\'s purse'/'KTK', 'Artifact').
card_original_text('briber\'s purse'/'KTK', 'Briber\'s Purse enters the battlefield with X gem counters on it.\n{1}, {T}, Remove a gem counter from Briber\'s Purse: Target creature can\'t attack or block this turn.').
card_first_print('briber\'s purse', 'KTK').
card_image_name('briber\'s purse'/'KTK', 'briber\'s purse').
card_uid('briber\'s purse'/'KTK', 'KTK:Briber\'s Purse:briber\'s purse').
card_rarity('briber\'s purse'/'KTK', 'Uncommon').
card_artist('briber\'s purse'/'KTK', 'Steve Argyle').
card_number('briber\'s purse'/'KTK', '217').
card_flavor_text('briber\'s purse'/'KTK', 'Victory is certain. The price, negotiable.').
card_multiverse_id('briber\'s purse'/'KTK', '386498').

card_in_set('bring low', 'KTK').
card_original_type('bring low'/'KTK', 'Instant').
card_original_text('bring low'/'KTK', 'Bring Low deals 3 damage to target creature. If that creature has a +1/+1 counter on it, Bring Low deals 5 damage to it instead.').
card_first_print('bring low', 'KTK').
card_image_name('bring low'/'KTK', 'bring low').
card_uid('bring low'/'KTK', 'KTK:Bring Low:bring low').
card_rarity('bring low'/'KTK', 'Common').
card_artist('bring low'/'KTK', 'Slawomir Maniak').
card_number('bring low'/'KTK', '103').
card_flavor_text('bring low'/'KTK', '\"People are often humbled by the elements. But the elements, too, can be humbled.\"\n—Surrak, khan of the Temur').
card_multiverse_id('bring low'/'KTK', '386499').

card_in_set('burn away', 'KTK').
card_original_type('burn away'/'KTK', 'Instant').
card_original_text('burn away'/'KTK', 'Burn Away deals 6 damage to target creature. When that creature dies this turn, exile all cards from its controller\'s graveyard.').
card_first_print('burn away', 'KTK').
card_image_name('burn away'/'KTK', 'burn away').
card_uid('burn away'/'KTK', 'KTK:Burn Away:burn away').
card_rarity('burn away'/'KTK', 'Uncommon').
card_artist('burn away'/'KTK', 'Vincent Proce').
card_number('burn away'/'KTK', '104').
card_flavor_text('burn away'/'KTK', '\"Your corruption will burn, serpent, until there is nothing left to defile.\"\n—Asmala, bloodfire mystic').
card_multiverse_id('burn away'/'KTK', '386500').

card_in_set('butcher of the horde', 'KTK').
card_original_type('butcher of the horde'/'KTK', 'Creature — Demon').
card_original_text('butcher of the horde'/'KTK', 'Flying\nSacrifice another creature: Butcher of the Horde gains your choice of vigilance, lifelink, or haste until end of turn.').
card_image_name('butcher of the horde'/'KTK', 'butcher of the horde').
card_uid('butcher of the horde'/'KTK', 'KTK:Butcher of the Horde:butcher of the horde').
card_rarity('butcher of the horde'/'KTK', 'Rare').
card_artist('butcher of the horde'/'KTK', 'Karl Kopinski').
card_number('butcher of the horde'/'KTK', '168').
card_multiverse_id('butcher of the horde'/'KTK', '386501').
card_watermark('butcher of the horde'/'KTK', 'Mardu').

card_in_set('cancel', 'KTK').
card_original_type('cancel'/'KTK', 'Instant').
card_original_text('cancel'/'KTK', 'Counter target spell.').
card_image_name('cancel'/'KTK', 'cancel').
card_uid('cancel'/'KTK', 'KTK:Cancel:cancel').
card_rarity('cancel'/'KTK', 'Common').
card_artist('cancel'/'KTK', 'Slawomir Maniak').
card_number('cancel'/'KTK', '33').
card_flavor_text('cancel'/'KTK', '\"Even the greatest inferno begins as a spark. And anyone can snuff out a spark.\"\n—Chanyi, mistfire sage').
card_multiverse_id('cancel'/'KTK', '386502').

card_in_set('canyon lurkers', 'KTK').
card_original_type('canyon lurkers'/'KTK', 'Creature — Human Rogue').
card_original_text('canyon lurkers'/'KTK', 'Morph {3}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('canyon lurkers', 'KTK').
card_image_name('canyon lurkers'/'KTK', 'canyon lurkers').
card_uid('canyon lurkers'/'KTK', 'KTK:Canyon Lurkers:canyon lurkers').
card_rarity('canyon lurkers'/'KTK', 'Common').
card_artist('canyon lurkers'/'KTK', 'Cynthia Sheppard').
card_number('canyon lurkers'/'KTK', '105').
card_flavor_text('canyon lurkers'/'KTK', 'The broken Qal Sisma foothills make poor terrain for cavalry, but a perfect setting for ambushes.').
card_multiverse_id('canyon lurkers'/'KTK', '386503').
card_watermark('canyon lurkers'/'KTK', 'Mardu').

card_in_set('chief of the edge', 'KTK').
card_original_type('chief of the edge'/'KTK', 'Creature — Human Warrior').
card_original_text('chief of the edge'/'KTK', 'Other Warrior creatures you control get +1/+0.').
card_first_print('chief of the edge', 'KTK').
card_image_name('chief of the edge'/'KTK', 'chief of the edge').
card_uid('chief of the edge'/'KTK', 'KTK:Chief of the Edge:chief of the edge').
card_rarity('chief of the edge'/'KTK', 'Uncommon').
card_artist('chief of the edge'/'KTK', 'David Palumbo').
card_number('chief of the edge'/'KTK', '169').
card_flavor_text('chief of the edge'/'KTK', '\"We are the swift, the strong, the blade\'s sharp shriek! Fear nothing, and strike!\"').
card_multiverse_id('chief of the edge'/'KTK', '386504').
card_watermark('chief of the edge'/'KTK', 'Mardu').

card_in_set('chief of the scale', 'KTK').
card_original_type('chief of the scale'/'KTK', 'Creature — Human Warrior').
card_original_text('chief of the scale'/'KTK', 'Other Warrior creatures you control get +0/+1.').
card_first_print('chief of the scale', 'KTK').
card_image_name('chief of the scale'/'KTK', 'chief of the scale').
card_uid('chief of the scale'/'KTK', 'KTK:Chief of the Scale:chief of the scale').
card_rarity('chief of the scale'/'KTK', 'Uncommon').
card_artist('chief of the scale'/'KTK', 'David Palumbo').
card_number('chief of the scale'/'KTK', '170').
card_flavor_text('chief of the scale'/'KTK', '\"We are the shield unbroken. If we fall today, we will die well, and our trees will bear our names in honor.\"').
card_multiverse_id('chief of the scale'/'KTK', '386505').
card_watermark('chief of the scale'/'KTK', 'Abzan').

card_in_set('clever impersonator', 'KTK').
card_original_type('clever impersonator'/'KTK', 'Creature — Shapeshifter').
card_original_text('clever impersonator'/'KTK', 'You may have Clever Impersonator enter the battlefield as a copy of any nonland permanent on the battlefield.').
card_first_print('clever impersonator', 'KTK').
card_image_name('clever impersonator'/'KTK', 'clever impersonator').
card_uid('clever impersonator'/'KTK', 'KTK:Clever Impersonator:clever impersonator').
card_rarity('clever impersonator'/'KTK', 'Mythic Rare').
card_artist('clever impersonator'/'KTK', 'Slawomir Maniak').
card_number('clever impersonator'/'KTK', '34').
card_flavor_text('clever impersonator'/'KTK', '\"Our own selves are the greatest obstacles to enlightenment.\"\n—Narset, khan of the Jeskai').
card_multiverse_id('clever impersonator'/'KTK', '386506').

card_in_set('crackling doom', 'KTK').
card_original_type('crackling doom'/'KTK', 'Instant').
card_original_text('crackling doom'/'KTK', 'Crackling Doom deals 2 damage to each opponent. Each opponent sacrifices a creature with the greatest power among creatures he or she controls.').
card_image_name('crackling doom'/'KTK', 'crackling doom').
card_uid('crackling doom'/'KTK', 'KTK:Crackling Doom:crackling doom').
card_rarity('crackling doom'/'KTK', 'Rare').
card_artist('crackling doom'/'KTK', 'Yohann Schepacz').
card_number('crackling doom'/'KTK', '171').
card_flavor_text('crackling doom'/'KTK', 'Do not fear the lightning. Fear the one it obeys.').
card_multiverse_id('crackling doom'/'KTK', '386507').
card_watermark('crackling doom'/'KTK', 'Mardu').

card_in_set('cranial archive', 'KTK').
card_original_type('cranial archive'/'KTK', 'Artifact').
card_original_text('cranial archive'/'KTK', '{2}, Exile Cranial Archive: Target player shuffles his or her graveyard into his or her library. Draw a card.').
card_first_print('cranial archive', 'KTK').
card_image_name('cranial archive'/'KTK', 'cranial archive').
card_uid('cranial archive'/'KTK', 'KTK:Cranial Archive:cranial archive').
card_rarity('cranial archive'/'KTK', 'Uncommon').
card_artist('cranial archive'/'KTK', 'Volkan Baga').
card_number('cranial archive'/'KTK', '218').
card_flavor_text('cranial archive'/'KTK', 'The greatest idea the zombie ever had in its head wasn\'t even its own.').
card_multiverse_id('cranial archive'/'KTK', '386508').

card_in_set('crater\'s claws', 'KTK').
card_original_type('crater\'s claws'/'KTK', 'Sorcery').
card_original_text('crater\'s claws'/'KTK', 'Crater\'s Claws deals X damage to target creature or player.\nFerocious — Crater\'s Claws deals X plus 2 damage to that creature or player instead if you control a creature with power 4 or greater.').
card_image_name('crater\'s claws'/'KTK', 'crater\'s claws').
card_uid('crater\'s claws'/'KTK', 'KTK:Crater\'s Claws:crater\'s claws').
card_rarity('crater\'s claws'/'KTK', 'Rare').
card_artist('crater\'s claws'/'KTK', 'Noah Bradley').
card_number('crater\'s claws'/'KTK', '106').
card_multiverse_id('crater\'s claws'/'KTK', '386509').
card_watermark('crater\'s claws'/'KTK', 'Temur').

card_in_set('crippling chill', 'KTK').
card_original_type('crippling chill'/'KTK', 'Instant').
card_original_text('crippling chill'/'KTK', 'Tap target creature. It doesn\'t untap during its controller\'s next untap step.\nDraw a card.').
card_image_name('crippling chill'/'KTK', 'crippling chill').
card_uid('crippling chill'/'KTK', 'KTK:Crippling Chill:crippling chill').
card_rarity('crippling chill'/'KTK', 'Common').
card_artist('crippling chill'/'KTK', 'Torstein Nordstrand').
card_number('crippling chill'/'KTK', '35').
card_flavor_text('crippling chill'/'KTK', 'In the silence of the ice, even dreams become still.').
card_multiverse_id('crippling chill'/'KTK', '386510').

card_in_set('dazzling ramparts', 'KTK').
card_original_type('dazzling ramparts'/'KTK', 'Creature — Wall').
card_original_text('dazzling ramparts'/'KTK', 'Defender\n{1}{W}, {T}: Tap target creature.').
card_first_print('dazzling ramparts', 'KTK').
card_image_name('dazzling ramparts'/'KTK', 'dazzling ramparts').
card_uid('dazzling ramparts'/'KTK', 'KTK:Dazzling Ramparts:dazzling ramparts').
card_rarity('dazzling ramparts'/'KTK', 'Uncommon').
card_artist('dazzling ramparts'/'KTK', 'Jung Park').
card_number('dazzling ramparts'/'KTK', '6').
card_flavor_text('dazzling ramparts'/'KTK', 'When Anafenza holds court under the First Tree, the gates of Mer-Ek are sealed. No safer place exists in all of Tarkir.').
card_multiverse_id('dazzling ramparts'/'KTK', '386511').
card_watermark('dazzling ramparts'/'KTK', 'Abzan').

card_in_set('dead drop', 'KTK').
card_original_type('dead drop'/'KTK', 'Sorcery').
card_original_text('dead drop'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTarget player sacrifices two creatures.').
card_first_print('dead drop', 'KTK').
card_image_name('dead drop'/'KTK', 'dead drop').
card_uid('dead drop'/'KTK', 'KTK:Dead Drop:dead drop').
card_rarity('dead drop'/'KTK', 'Uncommon').
card_artist('dead drop'/'KTK', 'Greg Staples').
card_number('dead drop'/'KTK', '67').
card_flavor_text('dead drop'/'KTK', 'Got a diving lesson\n—Sultai expression meaning\n\"was fed to the crocodiles\"').
card_multiverse_id('dead drop'/'KTK', '386512').
card_watermark('dead drop'/'KTK', 'Sultai').

card_in_set('death frenzy', 'KTK').
card_original_type('death frenzy'/'KTK', 'Sorcery').
card_original_text('death frenzy'/'KTK', 'All creatures get -2/-2 until end of turn. Whenever a creature dies this turn, you gain 1 life.').
card_first_print('death frenzy', 'KTK').
card_image_name('death frenzy'/'KTK', 'death frenzy').
card_uid('death frenzy'/'KTK', 'KTK:Death Frenzy:death frenzy').
card_rarity('death frenzy'/'KTK', 'Uncommon').
card_artist('death frenzy'/'KTK', 'Steve Prescott').
card_number('death frenzy'/'KTK', '172').
card_flavor_text('death frenzy'/'KTK', 'The crocodiles\' putrid jaws swallow everything but the screams.').
card_multiverse_id('death frenzy'/'KTK', '386513').
card_watermark('death frenzy'/'KTK', 'Sultai').

card_in_set('debilitating injury', 'KTK').
card_original_type('debilitating injury'/'KTK', 'Enchantment — Aura').
card_original_text('debilitating injury'/'KTK', 'Enchant creature\nEnchanted creature gets -2/-2.').
card_first_print('debilitating injury', 'KTK').
card_image_name('debilitating injury'/'KTK', 'debilitating injury').
card_uid('debilitating injury'/'KTK', 'KTK:Debilitating Injury:debilitating injury').
card_rarity('debilitating injury'/'KTK', 'Common').
card_artist('debilitating injury'/'KTK', 'Slawomir Maniak').
card_number('debilitating injury'/'KTK', '68').
card_flavor_text('debilitating injury'/'KTK', '\"If weakness does not exist within the Temur then we shall force it upon them.\"\n—Sidisi, khan of the Sultai').
card_multiverse_id('debilitating injury'/'KTK', '386514').

card_in_set('defiant strike', 'KTK').
card_original_type('defiant strike'/'KTK', 'Instant').
card_original_text('defiant strike'/'KTK', 'Target creature gets +1/+0 until end of turn.\nDraw a card.').
card_first_print('defiant strike', 'KTK').
card_image_name('defiant strike'/'KTK', 'defiant strike').
card_uid('defiant strike'/'KTK', 'KTK:Defiant Strike:defiant strike').
card_rarity('defiant strike'/'KTK', 'Common').
card_artist('defiant strike'/'KTK', 'Anastasia Ovchinnikova').
card_number('defiant strike'/'KTK', '7').
card_flavor_text('defiant strike'/'KTK', '\"Stand where the whole battle can see you. Strike so they\'ll never forget.\"\n—Anafenza, khan of the Abzan').
card_multiverse_id('defiant strike'/'KTK', '386515').

card_in_set('deflecting palm', 'KTK').
card_original_type('deflecting palm'/'KTK', 'Instant').
card_original_text('deflecting palm'/'KTK', 'The next time a source of your choice would deal damage to you this turn, prevent that damage. If damage is prevented this way, Deflecting Palm deals that much damage to that source\'s controller.').
card_image_name('deflecting palm'/'KTK', 'deflecting palm').
card_uid('deflecting palm'/'KTK', 'KTK:Deflecting Palm:deflecting palm').
card_rarity('deflecting palm'/'KTK', 'Rare').
card_artist('deflecting palm'/'KTK', 'Eric Deschamps').
card_number('deflecting palm'/'KTK', '173').
card_multiverse_id('deflecting palm'/'KTK', '386516').

card_in_set('despise', 'KTK').
card_original_type('despise'/'KTK', 'Sorcery').
card_original_text('despise'/'KTK', 'Target opponent reveals his or her hand. You choose a creature or planeswalker card from it. That player discards that card.').
card_image_name('despise'/'KTK', 'despise').
card_uid('despise'/'KTK', 'KTK:Despise:despise').
card_rarity('despise'/'KTK', 'Uncommon').
card_artist('despise'/'KTK', 'Todd Lockwood').
card_number('despise'/'KTK', '69').
card_flavor_text('despise'/'KTK', '\"You have returned from fire, traitor. This time I will see you leave as ashes.\"\n—Zurgo, to Sarkhan Vol').
card_multiverse_id('despise'/'KTK', '386517').

card_in_set('dig through time', 'KTK').
card_original_type('dig through time'/'KTK', 'Instant').
card_original_text('dig through time'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nLook at the top seven cards of your library. Put two of them into your hand and the rest on the bottom of your library in any order.').
card_image_name('dig through time'/'KTK', 'dig through time').
card_uid('dig through time'/'KTK', 'KTK:Dig Through Time:dig through time').
card_rarity('dig through time'/'KTK', 'Rare').
card_artist('dig through time'/'KTK', 'Ryan Yee').
card_number('dig through time'/'KTK', '36').
card_multiverse_id('dig through time'/'KTK', '386518').
card_watermark('dig through time'/'KTK', 'Sultai').

card_in_set('disdainful stroke', 'KTK').
card_original_type('disdainful stroke'/'KTK', 'Instant').
card_original_text('disdainful stroke'/'KTK', 'Counter target spell with converted mana cost 4 or greater.').
card_image_name('disdainful stroke'/'KTK', 'disdainful stroke').
card_uid('disdainful stroke'/'KTK', 'KTK:Disdainful Stroke:disdainful stroke').
card_rarity('disdainful stroke'/'KTK', 'Common').
card_artist('disdainful stroke'/'KTK', 'Svetlin Velinov').
card_number('disdainful stroke'/'KTK', '37').
card_flavor_text('disdainful stroke'/'KTK', '\"You are beneath contempt. Your lineage will be forgotten.\"').
card_multiverse_id('disdainful stroke'/'KTK', '386519').

card_in_set('dismal backwater', 'KTK').
card_original_type('dismal backwater'/'KTK', 'Land').
card_original_text('dismal backwater'/'KTK', 'Dismal Backwater enters the battlefield tapped.\nWhen Dismal Backwater enters the battlefield, you gain 1 life.\n{T}: Add {U} or {B} to your mana pool.').
card_first_print('dismal backwater', 'KTK').
card_image_name('dismal backwater'/'KTK', 'dismal backwater').
card_uid('dismal backwater'/'KTK', 'KTK:Dismal Backwater:dismal backwater').
card_rarity('dismal backwater'/'KTK', 'Common').
card_artist('dismal backwater'/'KTK', 'Sam Burley').
card_number('dismal backwater'/'KTK', '232').
card_multiverse_id('dismal backwater'/'KTK', '386520').

card_in_set('disowned ancestor', 'KTK').
card_original_type('disowned ancestor'/'KTK', 'Creature — Spirit Warrior').
card_original_text('disowned ancestor'/'KTK', 'Outlast {1}{B} ({1}{B}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)').
card_first_print('disowned ancestor', 'KTK').
card_image_name('disowned ancestor'/'KTK', 'disowned ancestor').
card_uid('disowned ancestor'/'KTK', 'KTK:Disowned Ancestor:disowned ancestor').
card_rarity('disowned ancestor'/'KTK', 'Common').
card_artist('disowned ancestor'/'KTK', 'Zack Stella').
card_number('disowned ancestor'/'KTK', '70').
card_flavor_text('disowned ancestor'/'KTK', 'Long after death, the spirits of the Disowned continue to seek redemption among their Abzan kin.').
card_multiverse_id('disowned ancestor'/'KTK', '386521').
card_watermark('disowned ancestor'/'KTK', 'Abzan').

card_in_set('dragon grip', 'KTK').
card_original_type('dragon grip'/'KTK', 'Enchantment — Aura').
card_original_text('dragon grip'/'KTK', 'Ferocious — If you control a creature with power 4 or greater, you may cast Dragon Grip as though it had flash. (You may cast it any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +2/+0 and has first strike.').
card_first_print('dragon grip', 'KTK').
card_image_name('dragon grip'/'KTK', 'dragon grip').
card_uid('dragon grip'/'KTK', 'KTK:Dragon Grip:dragon grip').
card_rarity('dragon grip'/'KTK', 'Uncommon').
card_artist('dragon grip'/'KTK', 'Jason Rainville').
card_number('dragon grip'/'KTK', '107').
card_multiverse_id('dragon grip'/'KTK', '386522').
card_watermark('dragon grip'/'KTK', 'Temur').

card_in_set('dragon throne of tarkir', 'KTK').
card_original_type('dragon throne of tarkir'/'KTK', 'Legendary Artifact — Equipment').
card_original_text('dragon throne of tarkir'/'KTK', 'Equipped creature has defender and \"{2}, {T}: Other creatures you control gain trample and get +X/+X until end of turn, where X is this creature\'s power.\"\nEquip {3}').
card_image_name('dragon throne of tarkir'/'KTK', 'dragon throne of tarkir').
card_uid('dragon throne of tarkir'/'KTK', 'KTK:Dragon Throne of Tarkir:dragon throne of tarkir').
card_rarity('dragon throne of tarkir'/'KTK', 'Rare').
card_artist('dragon throne of tarkir'/'KTK', 'Daarken').
card_number('dragon throne of tarkir'/'KTK', '219').
card_flavor_text('dragon throne of tarkir'/'KTK', 'What once soared high above Tarkir is now reduced to a seat.').
card_multiverse_id('dragon throne of tarkir'/'KTK', '386523').

card_in_set('dragon\'s eye savants', 'KTK').
card_original_type('dragon\'s eye savants'/'KTK', 'Creature — Human Wizard').
card_original_text('dragon\'s eye savants'/'KTK', 'Morph—Reveal a blue card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Dragon\'s Eye Savants is turned face up, look at target opponent\'s hand.').
card_first_print('dragon\'s eye savants', 'KTK').
card_image_name('dragon\'s eye savants'/'KTK', 'dragon\'s eye savants').
card_uid('dragon\'s eye savants'/'KTK', 'KTK:Dragon\'s Eye Savants:dragon\'s eye savants').
card_rarity('dragon\'s eye savants'/'KTK', 'Uncommon').
card_artist('dragon\'s eye savants'/'KTK', 'Jason Rainville').
card_number('dragon\'s eye savants'/'KTK', '38').
card_multiverse_id('dragon\'s eye savants'/'KTK', '386524').
card_watermark('dragon\'s eye savants'/'KTK', 'Jeskai').

card_in_set('dragon-style twins', 'KTK').
card_original_type('dragon-style twins'/'KTK', 'Creature — Human Monk').
card_original_text('dragon-style twins'/'KTK', 'Double strike\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_image_name('dragon-style twins'/'KTK', 'dragon-style twins').
card_uid('dragon-style twins'/'KTK', 'KTK:Dragon-Style Twins:dragon-style twins').
card_rarity('dragon-style twins'/'KTK', 'Rare').
card_artist('dragon-style twins'/'KTK', 'Wesley Burt').
card_number('dragon-style twins'/'KTK', '108').
card_flavor_text('dragon-style twins'/'KTK', '\"We are the flicker of the flame and its heat, the two sides of a single blade.\"').
card_multiverse_id('dragon-style twins'/'KTK', '386526').
card_watermark('dragon-style twins'/'KTK', 'Jeskai').

card_in_set('dragonscale boon', 'KTK').
card_original_type('dragonscale boon'/'KTK', 'Instant').
card_original_text('dragonscale boon'/'KTK', 'Put two +1/+1 counters on target creature and untap it.').
card_first_print('dragonscale boon', 'KTK').
card_image_name('dragonscale boon'/'KTK', 'dragonscale boon').
card_uid('dragonscale boon'/'KTK', 'KTK:Dragonscale Boon:dragonscale boon').
card_rarity('dragonscale boon'/'KTK', 'Common').
card_artist('dragonscale boon'/'KTK', 'Mark Winters').
card_number('dragonscale boon'/'KTK', '131').
card_flavor_text('dragonscale boon'/'KTK', '\"When we were lost and weary, the ainok showed us how to survive. They have earned the right to call themselves Abzan, and to wear the Scale.\"\n—Anafenza, khan of the Abzan').
card_multiverse_id('dragonscale boon'/'KTK', '386525').

card_in_set('duneblast', 'KTK').
card_original_type('duneblast'/'KTK', 'Sorcery').
card_original_text('duneblast'/'KTK', 'Choose up to one creature. Destroy the rest.').
card_image_name('duneblast'/'KTK', 'duneblast').
card_uid('duneblast'/'KTK', 'KTK:Duneblast:duneblast').
card_rarity('duneblast'/'KTK', 'Rare').
card_artist('duneblast'/'KTK', 'Ryan Alexander Lee').
card_number('duneblast'/'KTK', '174').
card_flavor_text('duneblast'/'KTK', 'The Abzan turn to this spell only as a last resort, for its inevitable result is what they most dread: to be alone.').
card_multiverse_id('duneblast'/'KTK', '386527').
card_watermark('duneblast'/'KTK', 'Abzan').

card_in_set('dutiful return', 'KTK').
card_original_type('dutiful return'/'KTK', 'Sorcery').
card_original_text('dutiful return'/'KTK', 'Return up to two target creature cards from your graveyard to your hand.').
card_first_print('dutiful return', 'KTK').
card_image_name('dutiful return'/'KTK', 'dutiful return').
card_uid('dutiful return'/'KTK', 'KTK:Dutiful Return:dutiful return').
card_rarity('dutiful return'/'KTK', 'Common').
card_artist('dutiful return'/'KTK', 'Seb McKinnon').
card_number('dutiful return'/'KTK', '71').
card_flavor_text('dutiful return'/'KTK', '\"We have a word for enemies too mutilated for military service: furniture.\"\n—Taigam, Sidisi\'s Hand').
card_multiverse_id('dutiful return'/'KTK', '386528').

card_in_set('efreet weaponmaster', 'KTK').
card_original_type('efreet weaponmaster'/'KTK', 'Creature — Efreet Monk').
card_original_text('efreet weaponmaster'/'KTK', 'First strike\nWhen Efreet Weaponmaster enters the battlefield or is turned face up, another target creature you control gets +3/+0 until end of turn.\nMorph {2}{U}{R}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('efreet weaponmaster', 'KTK').
card_image_name('efreet weaponmaster'/'KTK', 'efreet weaponmaster').
card_uid('efreet weaponmaster'/'KTK', 'KTK:Efreet Weaponmaster:efreet weaponmaster').
card_rarity('efreet weaponmaster'/'KTK', 'Common').
card_artist('efreet weaponmaster'/'KTK', 'Ryan Alexander Lee').
card_number('efreet weaponmaster'/'KTK', '175').
card_multiverse_id('efreet weaponmaster'/'KTK', '386529').
card_watermark('efreet weaponmaster'/'KTK', 'Jeskai').

card_in_set('embodiment of spring', 'KTK').
card_original_type('embodiment of spring'/'KTK', 'Creature — Elemental').
card_original_text('embodiment of spring'/'KTK', '{1}{G}, {T}, Sacrifice Embodiment of Spring: Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('embodiment of spring', 'KTK').
card_image_name('embodiment of spring'/'KTK', 'embodiment of spring').
card_uid('embodiment of spring'/'KTK', 'KTK:Embodiment of Spring:embodiment of spring').
card_rarity('embodiment of spring'/'KTK', 'Common').
card_artist('embodiment of spring'/'KTK', 'Wayne Reynolds').
card_number('embodiment of spring'/'KTK', '39').
card_flavor_text('embodiment of spring'/'KTK', 'Arel dreamed of winter\'s end. The next morning she followed a strange trail and found a seedling in the snow.').
card_multiverse_id('embodiment of spring'/'KTK', '386530').
card_watermark('embodiment of spring'/'KTK', 'Temur').

card_in_set('empty the pits', 'KTK').
card_original_type('empty the pits'/'KTK', 'Instant').
card_original_text('empty the pits'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nPut X 2/2 black Zombie creature tokens onto the battlefield tapped.').
card_first_print('empty the pits', 'KTK').
card_image_name('empty the pits'/'KTK', 'empty the pits').
card_uid('empty the pits'/'KTK', 'KTK:Empty the Pits:empty the pits').
card_rarity('empty the pits'/'KTK', 'Mythic Rare').
card_artist('empty the pits'/'KTK', 'Ryan Alexander Lee').
card_number('empty the pits'/'KTK', '72').
card_flavor_text('empty the pits'/'KTK', 'The Sultai would rebuild the empire on the backs of the dead.').
card_multiverse_id('empty the pits'/'KTK', '386531').
card_watermark('empty the pits'/'KTK', 'Sultai').

card_in_set('end hostilities', 'KTK').
card_original_type('end hostilities'/'KTK', 'Sorcery').
card_original_text('end hostilities'/'KTK', 'Destroy all creatures and all permanents attached to creatures.').
card_first_print('end hostilities', 'KTK').
card_image_name('end hostilities'/'KTK', 'end hostilities').
card_uid('end hostilities'/'KTK', 'KTK:End Hostilities:end hostilities').
card_rarity('end hostilities'/'KTK', 'Rare').
card_artist('end hostilities'/'KTK', 'Jason Rainville').
card_number('end hostilities'/'KTK', '8').
card_flavor_text('end hostilities'/'KTK', 'Her palm flared like the eye of a waking dragon. Then all was calm.').
card_multiverse_id('end hostilities'/'KTK', '386532').

card_in_set('erase', 'KTK').
card_original_type('erase'/'KTK', 'Instant').
card_original_text('erase'/'KTK', 'Exile target enchantment.').
card_image_name('erase'/'KTK', 'erase').
card_uid('erase'/'KTK', 'KTK:Erase:erase').
card_rarity('erase'/'KTK', 'Common').
card_artist('erase'/'KTK', 'Zack Stella').
card_number('erase'/'KTK', '9').
card_flavor_text('erase'/'KTK', '\"Truth is hard enough to see, let alone understand. We must remove all distractions to find clarity.\"\n—Zogye, wandering sage').
card_multiverse_id('erase'/'KTK', '386533').

card_in_set('feat of resistance', 'KTK').
card_original_type('feat of resistance'/'KTK', 'Instant').
card_original_text('feat of resistance'/'KTK', 'Put a +1/+1 counter on target creature you control. It gains protection from the color of your choice until end of turn.').
card_first_print('feat of resistance', 'KTK').
card_image_name('feat of resistance'/'KTK', 'feat of resistance').
card_uid('feat of resistance'/'KTK', 'KTK:Feat of Resistance:feat of resistance').
card_rarity('feat of resistance'/'KTK', 'Common').
card_artist('feat of resistance'/'KTK', 'David Gaillet').
card_number('feat of resistance'/'KTK', '10').
card_flavor_text('feat of resistance'/'KTK', 'Dragons are extinct on Tarkir, but Abzan magic still emulates their endurance.').
card_multiverse_id('feat of resistance'/'KTK', '386534').

card_in_set('feed the clan', 'KTK').
card_original_type('feed the clan'/'KTK', 'Instant').
card_original_text('feed the clan'/'KTK', 'You gain 5 life.\nFerocious — You gain 10 life instead if you control a creature with power 4 or greater.').
card_first_print('feed the clan', 'KTK').
card_image_name('feed the clan'/'KTK', 'feed the clan').
card_uid('feed the clan'/'KTK', 'KTK:Feed the Clan:feed the clan').
card_rarity('feed the clan'/'KTK', 'Common').
card_artist('feed the clan'/'KTK', 'Winona Nelson').
card_number('feed the clan'/'KTK', '132').
card_flavor_text('feed the clan'/'KTK', 'The Temur believe three things only are needed in life: a hot fire, a full belly, and a strong companion.').
card_multiverse_id('feed the clan'/'KTK', '386535').
card_watermark('feed the clan'/'KTK', 'Temur').

card_in_set('firehoof cavalry', 'KTK').
card_original_type('firehoof cavalry'/'KTK', 'Creature — Human Berserker').
card_original_text('firehoof cavalry'/'KTK', '{3}{R}: Firehoof Cavalry gets +2/+0 and gains trample until end of turn.').
card_first_print('firehoof cavalry', 'KTK').
card_image_name('firehoof cavalry'/'KTK', 'firehoof cavalry').
card_uid('firehoof cavalry'/'KTK', 'KTK:Firehoof Cavalry:firehoof cavalry').
card_rarity('firehoof cavalry'/'KTK', 'Common').
card_artist('firehoof cavalry'/'KTK', 'YW Tang').
card_number('firehoof cavalry'/'KTK', '11').
card_flavor_text('firehoof cavalry'/'KTK', '\"What warrior worth the name fears to leave a trail? If my enemies seek me, let them follow the ashes in my wake.\"').
card_multiverse_id('firehoof cavalry'/'KTK', '386536').
card_watermark('firehoof cavalry'/'KTK', 'Mardu').

card_in_set('flooded strand', 'KTK').
card_original_type('flooded strand'/'KTK', 'Land').
card_original_text('flooded strand'/'KTK', '{T}, Pay 1 life, Sacrifice Flooded Strand: Search your library for a Plains or Island card and put it onto the battlefield. Then shuffle your library.').
card_image_name('flooded strand'/'KTK', 'flooded strand').
card_uid('flooded strand'/'KTK', 'KTK:Flooded Strand:flooded strand').
card_rarity('flooded strand'/'KTK', 'Rare').
card_artist('flooded strand'/'KTK', 'Andreas Rocha').
card_number('flooded strand'/'KTK', '233').
card_flavor_text('flooded strand'/'KTK', 'Where dragons once slept, their bones now rest.').
card_multiverse_id('flooded strand'/'KTK', '386537').

card_in_set('flying crane technique', 'KTK').
card_original_type('flying crane technique'/'KTK', 'Instant').
card_original_text('flying crane technique'/'KTK', 'Untap all creatures you control. They gain flying and double strike until end of turn.').
card_image_name('flying crane technique'/'KTK', 'flying crane technique').
card_uid('flying crane technique'/'KTK', 'KTK:Flying Crane Technique:flying crane technique').
card_rarity('flying crane technique'/'KTK', 'Rare').
card_artist('flying crane technique'/'KTK', 'Jack Wang').
card_number('flying crane technique'/'KTK', '176').
card_flavor_text('flying crane technique'/'KTK', 'There are many Jeskai styles: Riverwalk imitates flowing water, Dragonfist the ancient hellkites, and Flying Crane the wild aven of the high peaks.').
card_multiverse_id('flying crane technique'/'KTK', '386538').
card_watermark('flying crane technique'/'KTK', 'Jeskai').

card_in_set('force away', 'KTK').
card_original_type('force away'/'KTK', 'Instant').
card_original_text('force away'/'KTK', 'Return target creature to its owner\'s hand.\nFerocious — If you control a creature with power 4 or greater, you may draw a card. If you do, discard a card.').
card_first_print('force away', 'KTK').
card_image_name('force away'/'KTK', 'force away').
card_uid('force away'/'KTK', 'KTK:Force Away:force away').
card_rarity('force away'/'KTK', 'Common').
card_artist('force away'/'KTK', 'Mark Winters').
card_number('force away'/'KTK', '40').
card_flavor_text('force away'/'KTK', 'Where an enemy once rode, not even a whisper remains.').
card_multiverse_id('force away'/'KTK', '386539').
card_watermark('force away'/'KTK', 'Temur').

card_in_set('forest', 'KTK').
card_original_type('forest'/'KTK', 'Basic Land — Forest').
card_original_text('forest'/'KTK', 'G').
card_image_name('forest'/'KTK', 'forest1').
card_uid('forest'/'KTK', 'KTK:Forest:forest1').
card_rarity('forest'/'KTK', 'Basic Land').
card_artist('forest'/'KTK', 'Sam Burley').
card_number('forest'/'KTK', '266').
card_multiverse_id('forest'/'KTK', '386542').

card_in_set('forest', 'KTK').
card_original_type('forest'/'KTK', 'Basic Land — Forest').
card_original_text('forest'/'KTK', 'G').
card_image_name('forest'/'KTK', 'forest2').
card_uid('forest'/'KTK', 'KTK:Forest:forest2').
card_rarity('forest'/'KTK', 'Basic Land').
card_artist('forest'/'KTK', 'Titus Lunter').
card_number('forest'/'KTK', '267').
card_multiverse_id('forest'/'KTK', '386540').

card_in_set('forest', 'KTK').
card_original_type('forest'/'KTK', 'Basic Land — Forest').
card_original_text('forest'/'KTK', 'G').
card_image_name('forest'/'KTK', 'forest3').
card_uid('forest'/'KTK', 'KTK:Forest:forest3').
card_rarity('forest'/'KTK', 'Basic Land').
card_artist('forest'/'KTK', 'Titus Lunter').
card_number('forest'/'KTK', '268').
card_multiverse_id('forest'/'KTK', '386541').

card_in_set('forest', 'KTK').
card_original_type('forest'/'KTK', 'Basic Land — Forest').
card_original_text('forest'/'KTK', 'G').
card_image_name('forest'/'KTK', 'forest4').
card_uid('forest'/'KTK', 'KTK:Forest:forest4').
card_rarity('forest'/'KTK', 'Basic Land').
card_artist('forest'/'KTK', 'Adam Paquette').
card_number('forest'/'KTK', '269').
card_multiverse_id('forest'/'KTK', '386543').

card_in_set('frontier bivouac', 'KTK').
card_original_type('frontier bivouac'/'KTK', 'Land').
card_original_text('frontier bivouac'/'KTK', 'Frontier Bivouac enters the battlefield tapped.\n{T}: Add {G}, {U}, or {R} to your mana pool.').
card_first_print('frontier bivouac', 'KTK').
card_image_name('frontier bivouac'/'KTK', 'frontier bivouac').
card_uid('frontier bivouac'/'KTK', 'KTK:Frontier Bivouac:frontier bivouac').
card_rarity('frontier bivouac'/'KTK', 'Uncommon').
card_artist('frontier bivouac'/'KTK', 'Titus Lunter').
card_number('frontier bivouac'/'KTK', '234').
card_flavor_text('frontier bivouac'/'KTK', '\"The most powerful dreams visit those who shelter in a dragon\'s skull.\"\n—Chianul, Who Whispers Twice').
card_multiverse_id('frontier bivouac'/'KTK', '386544').
card_watermark('frontier bivouac'/'KTK', 'Temur').

card_in_set('ghostfire blade', 'KTK').
card_original_type('ghostfire blade'/'KTK', 'Artifact — Equipment').
card_original_text('ghostfire blade'/'KTK', 'Equipped creature gets +2/+2.\nEquip {3}\nGhostfire Blade\'s equip ability costs {2} less to activate if it targets a colorless creature.').
card_first_print('ghostfire blade', 'KTK').
card_image_name('ghostfire blade'/'KTK', 'ghostfire blade').
card_uid('ghostfire blade'/'KTK', 'KTK:Ghostfire Blade:ghostfire blade').
card_rarity('ghostfire blade'/'KTK', 'Rare').
card_artist('ghostfire blade'/'KTK', 'Cyril Van Der Haegen').
card_number('ghostfire blade'/'KTK', '220').
card_flavor_text('ghostfire blade'/'KTK', 'If you fear the dragon\'s fire, you are unworthy to wield it.').
card_multiverse_id('ghostfire blade'/'KTK', '386545').

card_in_set('glacial stalker', 'KTK').
card_original_type('glacial stalker'/'KTK', 'Creature — Elemental').
card_original_text('glacial stalker'/'KTK', 'Morph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('glacial stalker', 'KTK').
card_image_name('glacial stalker'/'KTK', 'glacial stalker').
card_uid('glacial stalker'/'KTK', 'KTK:Glacial Stalker:glacial stalker').
card_rarity('glacial stalker'/'KTK', 'Common').
card_artist('glacial stalker'/'KTK', 'Daarken').
card_number('glacial stalker'/'KTK', '41').
card_flavor_text('glacial stalker'/'KTK', '\"Have you spent a winter high in the mountains, where the ice walks and speaks to the wind? It is not a place for those who have not learned respect.\"\n—Ulnok, Temur shaman').
card_multiverse_id('glacial stalker'/'KTK', '386546').
card_watermark('glacial stalker'/'KTK', 'Temur').

card_in_set('goblinslide', 'KTK').
card_original_type('goblinslide'/'KTK', 'Enchantment').
card_original_text('goblinslide'/'KTK', 'Whenever you cast a noncreature spell, you may pay {1}. If you do, put a 1/1 red Goblin creature token with haste onto the battlefield.').
card_first_print('goblinslide', 'KTK').
card_image_name('goblinslide'/'KTK', 'goblinslide').
card_uid('goblinslide'/'KTK', 'KTK:Goblinslide:goblinslide').
card_rarity('goblinslide'/'KTK', 'Uncommon').
card_artist('goblinslide'/'KTK', 'Kev Walker').
card_number('goblinslide'/'KTK', '109').
card_flavor_text('goblinslide'/'KTK', 'Goblins, like snowflakes, are only dangerous in numbers.').
card_multiverse_id('goblinslide'/'KTK', '386547').

card_in_set('grim haruspex', 'KTK').
card_original_type('grim haruspex'/'KTK', 'Creature — Human Wizard').
card_original_text('grim haruspex'/'KTK', 'Morph {B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhenever another nontoken creature you control dies, draw a card.').
card_image_name('grim haruspex'/'KTK', 'grim haruspex').
card_uid('grim haruspex'/'KTK', 'KTK:Grim Haruspex:grim haruspex').
card_rarity('grim haruspex'/'KTK', 'Rare').
card_artist('grim haruspex'/'KTK', 'Seb McKinnon').
card_number('grim haruspex'/'KTK', '73').
card_flavor_text('grim haruspex'/'KTK', '\"We all want to know what\'s going on in someone else\'s head. I simply open it up and look.\"').
card_multiverse_id('grim haruspex'/'KTK', '386548').
card_watermark('grim haruspex'/'KTK', 'Sultai').

card_in_set('gurmag swiftwing', 'KTK').
card_original_type('gurmag swiftwing'/'KTK', 'Creature — Bat').
card_original_text('gurmag swiftwing'/'KTK', 'Flying, first strike, haste').
card_first_print('gurmag swiftwing', 'KTK').
card_image_name('gurmag swiftwing'/'KTK', 'gurmag swiftwing').
card_uid('gurmag swiftwing'/'KTK', 'KTK:Gurmag Swiftwing:gurmag swiftwing').
card_rarity('gurmag swiftwing'/'KTK', 'Uncommon').
card_artist('gurmag swiftwing'/'KTK', 'Jeff Simpson').
card_number('gurmag swiftwing'/'KTK', '74').
card_flavor_text('gurmag swiftwing'/'KTK', '\"Anything a falcon can do, a bat can do in pitch darkness.\"\n—Urdnan the Wanderer').
card_multiverse_id('gurmag swiftwing'/'KTK', '386549').

card_in_set('hardened scales', 'KTK').
card_original_type('hardened scales'/'KTK', 'Enchantment').
card_original_text('hardened scales'/'KTK', 'If one or more +1/+1 counters would be placed on a creature you control, that many plus one +1/+1 counters are placed on it instead.').
card_image_name('hardened scales'/'KTK', 'hardened scales').
card_uid('hardened scales'/'KTK', 'KTK:Hardened Scales:hardened scales').
card_rarity('hardened scales'/'KTK', 'Rare').
card_artist('hardened scales'/'KTK', 'Mark Winters').
card_number('hardened scales'/'KTK', '133').
card_flavor_text('hardened scales'/'KTK', '\"Naga shed their scales. We wear ours with pride.\"\n—Golran, dragonscale captain').
card_multiverse_id('hardened scales'/'KTK', '386550').
card_watermark('hardened scales'/'KTK', 'Abzan').

card_in_set('heart-piercer bow', 'KTK').
card_original_type('heart-piercer bow'/'KTK', 'Artifact — Equipment').
card_original_text('heart-piercer bow'/'KTK', 'Whenever equipped creature attacks, Heart-Piercer Bow deals 1 damage to target creature defending player controls.\nEquip {1}').
card_first_print('heart-piercer bow', 'KTK').
card_image_name('heart-piercer bow'/'KTK', 'heart-piercer bow').
card_uid('heart-piercer bow'/'KTK', 'KTK:Heart-Piercer Bow:heart-piercer bow').
card_rarity('heart-piercer bow'/'KTK', 'Uncommon').
card_artist('heart-piercer bow'/'KTK', 'Franz Vohwinkel').
card_number('heart-piercer bow'/'KTK', '221').
card_flavor_text('heart-piercer bow'/'KTK', 'Designed by an ancient artificer, the finest Mardu bows are carved from dragon bone and strung with the wind itself.').
card_multiverse_id('heart-piercer bow'/'KTK', '386551').

card_in_set('heir of the wilds', 'KTK').
card_original_type('heir of the wilds'/'KTK', 'Creature — Human Warrior').
card_original_text('heir of the wilds'/'KTK', 'Deathtouch\nFerocious — Whenever Heir of the Wilds attacks, if you control a creature with power 4 or greater, Heir of the Wilds gets +1/+1 until end of turn.').
card_image_name('heir of the wilds'/'KTK', 'heir of the wilds').
card_uid('heir of the wilds'/'KTK', 'KTK:Heir of the Wilds:heir of the wilds').
card_rarity('heir of the wilds'/'KTK', 'Uncommon').
card_artist('heir of the wilds'/'KTK', 'Winona Nelson').
card_number('heir of the wilds'/'KTK', '134').
card_flavor_text('heir of the wilds'/'KTK', 'In the high caves of the Qal Sisma mountains, young hunters quest to hear the echoes of their fierce ancestors.').
card_multiverse_id('heir of the wilds'/'KTK', '386552').
card_watermark('heir of the wilds'/'KTK', 'Temur').

card_in_set('herald of anafenza', 'KTK').
card_original_type('herald of anafenza'/'KTK', 'Creature — Human Soldier').
card_original_text('herald of anafenza'/'KTK', 'Outlast {2}{W} ({2}{W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nWhenever you activate Herald of Anafenza\'s outlast ability, put a 1/1 white Warrior creature token onto the battlefield.').
card_image_name('herald of anafenza'/'KTK', 'herald of anafenza').
card_uid('herald of anafenza'/'KTK', 'KTK:Herald of Anafenza:herald of anafenza').
card_rarity('herald of anafenza'/'KTK', 'Rare').
card_artist('herald of anafenza'/'KTK', 'Aaron Miller').
card_number('herald of anafenza'/'KTK', '12').
card_multiverse_id('herald of anafenza'/'KTK', '386553').
card_watermark('herald of anafenza'/'KTK', 'Abzan').

card_in_set('high sentinels of arashin', 'KTK').
card_original_type('high sentinels of arashin'/'KTK', 'Creature — Bird Soldier').
card_original_text('high sentinels of arashin'/'KTK', 'Flying\nHigh Sentinels of Arashin gets +1/+1 for each other creature you control with a +1/+1 counter on it.\n{3}{W}: Put a +1/+1 counter on target creature.').
card_image_name('high sentinels of arashin'/'KTK', 'high sentinels of arashin').
card_uid('high sentinels of arashin'/'KTK', 'KTK:High Sentinels of Arashin:high sentinels of arashin').
card_rarity('high sentinels of arashin'/'KTK', 'Rare').
card_artist('high sentinels of arashin'/'KTK', 'James Ryman').
card_number('high sentinels of arashin'/'KTK', '13').
card_multiverse_id('high sentinels of arashin'/'KTK', '386554').
card_watermark('high sentinels of arashin'/'KTK', 'Abzan').

card_in_set('highland game', 'KTK').
card_original_type('highland game'/'KTK', 'Creature — Elk').
card_original_text('highland game'/'KTK', 'When Highland Game dies, you gain 2 life.').
card_first_print('highland game', 'KTK').
card_image_name('highland game'/'KTK', 'highland game').
card_uid('highland game'/'KTK', 'KTK:Highland Game:highland game').
card_rarity('highland game'/'KTK', 'Common').
card_artist('highland game'/'KTK', 'John Severin Brassell').
card_number('highland game'/'KTK', '135').
card_flavor_text('highland game'/'KTK', '\"Bring down a stag and fix its horns upon her head. This one hears the whispers.\"\n—Chianul, at the weaving of Arel').
card_multiverse_id('highland game'/'KTK', '386555').

card_in_set('highspire mantis', 'KTK').
card_original_type('highspire mantis'/'KTK', 'Creature — Insect').
card_original_text('highspire mantis'/'KTK', 'Flying, trample').
card_first_print('highspire mantis', 'KTK').
card_image_name('highspire mantis'/'KTK', 'highspire mantis').
card_uid('highspire mantis'/'KTK', 'KTK:Highspire Mantis:highspire mantis').
card_rarity('highspire mantis'/'KTK', 'Uncommon').
card_artist('highspire mantis'/'KTK', 'Igor Kieryluk').
card_number('highspire mantis'/'KTK', '177').
card_flavor_text('highspire mantis'/'KTK', 'Its wings produce a high-pitched, barely audible whirring sound in flight. Only Jeskai masters are quiet enough to hear one coming.').
card_multiverse_id('highspire mantis'/'KTK', '386556').
card_watermark('highspire mantis'/'KTK', 'Jeskai').

card_in_set('hooded hydra', 'KTK').
card_original_type('hooded hydra'/'KTK', 'Creature — Snake Hydra').
card_original_text('hooded hydra'/'KTK', 'Hooded Hydra enters the battlefield with X +1/+1 counters on it.\nWhen Hooded Hydra dies, put a 1/1 green Snake creature token onto the battlefield for each +1/+1 counter on it.\nMorph {3}{G}{G}\nAs Hooded Hydra is turned face up, put five +1/+1 counters on it.').
card_first_print('hooded hydra', 'KTK').
card_image_name('hooded hydra'/'KTK', 'hooded hydra').
card_uid('hooded hydra'/'KTK', 'KTK:Hooded Hydra:hooded hydra').
card_rarity('hooded hydra'/'KTK', 'Mythic Rare').
card_artist('hooded hydra'/'KTK', 'Chase Stone').
card_number('hooded hydra'/'KTK', '136').
card_multiverse_id('hooded hydra'/'KTK', '386557').
card_watermark('hooded hydra'/'KTK', 'Sultai').

card_in_set('hooting mandrills', 'KTK').
card_original_type('hooting mandrills'/'KTK', 'Creature — Ape').
card_original_text('hooting mandrills'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTrample').
card_first_print('hooting mandrills', 'KTK').
card_image_name('hooting mandrills'/'KTK', 'hooting mandrills').
card_uid('hooting mandrills'/'KTK', 'KTK:Hooting Mandrills:hooting mandrills').
card_rarity('hooting mandrills'/'KTK', 'Common').
card_artist('hooting mandrills'/'KTK', 'Mike Bierek').
card_number('hooting mandrills'/'KTK', '137').
card_flavor_text('hooting mandrills'/'KTK', 'Interlopers in Sultai territory usually end up as crocodile chow or baboon bait.').
card_multiverse_id('hooting mandrills'/'KTK', '386558').
card_watermark('hooting mandrills'/'KTK', 'Sultai').

card_in_set('horde ambusher', 'KTK').
card_original_type('horde ambusher'/'KTK', 'Creature — Human Berserker').
card_original_text('horde ambusher'/'KTK', 'Whenever Horde Ambusher blocks, it deals 1 damage to you.\nMorph—Reveal a red card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Horde Ambusher is turned face up, target creature can\'t block this turn.').
card_first_print('horde ambusher', 'KTK').
card_image_name('horde ambusher'/'KTK', 'horde ambusher').
card_uid('horde ambusher'/'KTK', 'KTK:Horde Ambusher:horde ambusher').
card_rarity('horde ambusher'/'KTK', 'Uncommon').
card_artist('horde ambusher'/'KTK', 'Tyler Jacobson').
card_number('horde ambusher'/'KTK', '110').
card_multiverse_id('horde ambusher'/'KTK', '386559').
card_watermark('horde ambusher'/'KTK', 'Mardu').

card_in_set('hordeling outburst', 'KTK').
card_original_type('hordeling outburst'/'KTK', 'Sorcery').
card_original_text('hordeling outburst'/'KTK', 'Put three 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('hordeling outburst'/'KTK', 'hordeling outburst').
card_uid('hordeling outburst'/'KTK', 'KTK:Hordeling Outburst:hordeling outburst').
card_rarity('hordeling outburst'/'KTK', 'Uncommon').
card_artist('hordeling outburst'/'KTK', 'Zoltan Boros').
card_number('hordeling outburst'/'KTK', '111').
card_flavor_text('hordeling outburst'/'KTK', '\"Leave no scraps, lest you attract pests.\"\n—Mardu threat').
card_multiverse_id('hordeling outburst'/'KTK', '386560').

card_in_set('howl of the horde', 'KTK').
card_original_type('howl of the horde'/'KTK', 'Sorcery').
card_original_text('howl of the horde'/'KTK', 'When you cast your next instant or sorcery spell this turn, copy that spell. You may choose new targets for the copy.\nRaid — If you attacked with a creature this turn, when you cast your next instant or sorcery spell this turn, copy that spell an additional time. You may choose new targets for the copy.').
card_first_print('howl of the horde', 'KTK').
card_image_name('howl of the horde'/'KTK', 'howl of the horde').
card_uid('howl of the horde'/'KTK', 'KTK:Howl of the Horde:howl of the horde').
card_rarity('howl of the horde'/'KTK', 'Rare').
card_artist('howl of the horde'/'KTK', 'Slawomir Maniak').
card_number('howl of the horde'/'KTK', '112').
card_multiverse_id('howl of the horde'/'KTK', '386561').
card_watermark('howl of the horde'/'KTK', 'Mardu').

card_in_set('icefeather aven', 'KTK').
card_original_type('icefeather aven'/'KTK', 'Creature — Bird Shaman').
card_original_text('icefeather aven'/'KTK', 'Flying\nMorph {1}{G}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Icefeather Aven is turned face up, you may return another target creature to its owner\'s hand.').
card_first_print('icefeather aven', 'KTK').
card_image_name('icefeather aven'/'KTK', 'icefeather aven').
card_uid('icefeather aven'/'KTK', 'KTK:Icefeather Aven:icefeather aven').
card_rarity('icefeather aven'/'KTK', 'Uncommon').
card_artist('icefeather aven'/'KTK', 'Slawomir Maniak').
card_number('icefeather aven'/'KTK', '178').
card_multiverse_id('icefeather aven'/'KTK', '386562').
card_watermark('icefeather aven'/'KTK', 'Temur').

card_in_set('icy blast', 'KTK').
card_original_type('icy blast'/'KTK', 'Instant').
card_original_text('icy blast'/'KTK', 'Tap X target creatures.\nFerocious — If you control a creature with power 4 or greater, those creatures don\'t untap during their controllers\' next untap steps.').
card_image_name('icy blast'/'KTK', 'icy blast').
card_uid('icy blast'/'KTK', 'KTK:Icy Blast:icy blast').
card_rarity('icy blast'/'KTK', 'Rare').
card_artist('icy blast'/'KTK', 'Eric Deschamps').
card_number('icy blast'/'KTK', '42').
card_flavor_text('icy blast'/'KTK', '\"Do not think the sand or the sun will hold back the breath of winter.\"').
card_multiverse_id('icy blast'/'KTK', '386563').
card_watermark('icy blast'/'KTK', 'Temur').

card_in_set('incremental growth', 'KTK').
card_original_type('incremental growth'/'KTK', 'Sorcery').
card_original_text('incremental growth'/'KTK', 'Put a +1/+1 counter on target creature, two +1/+1 counters on another target creature, and three +1/+1 counters on a third target creature.').
card_image_name('incremental growth'/'KTK', 'incremental growth').
card_uid('incremental growth'/'KTK', 'KTK:Incremental Growth:incremental growth').
card_rarity('incremental growth'/'KTK', 'Uncommon').
card_artist('incremental growth'/'KTK', 'Clint Cearley').
card_number('incremental growth'/'KTK', '138').
card_flavor_text('incremental growth'/'KTK', 'The bonds of family cross the boundaries of race.').
card_multiverse_id('incremental growth'/'KTK', '386564').

card_in_set('island', 'KTK').
card_original_type('island'/'KTK', 'Basic Land — Island').
card_original_text('island'/'KTK', 'U').
card_image_name('island'/'KTK', 'island1').
card_uid('island'/'KTK', 'KTK:Island:island1').
card_rarity('island'/'KTK', 'Basic Land').
card_artist('island'/'KTK', 'Florian de Gesincourt').
card_number('island'/'KTK', '254').
card_multiverse_id('island'/'KTK', '386565').

card_in_set('island', 'KTK').
card_original_type('island'/'KTK', 'Basic Land — Island').
card_original_text('island'/'KTK', 'U').
card_image_name('island'/'KTK', 'island2').
card_uid('island'/'KTK', 'KTK:Island:island2').
card_rarity('island'/'KTK', 'Basic Land').
card_artist('island'/'KTK', 'Florian de Gesincourt').
card_number('island'/'KTK', '255').
card_multiverse_id('island'/'KTK', '386566').

card_in_set('island', 'KTK').
card_original_type('island'/'KTK', 'Basic Land — Island').
card_original_text('island'/'KTK', 'U').
card_image_name('island'/'KTK', 'island3').
card_uid('island'/'KTK', 'KTK:Island:island3').
card_rarity('island'/'KTK', 'Basic Land').
card_artist('island'/'KTK', 'Titus Lunter').
card_number('island'/'KTK', '256').
card_multiverse_id('island'/'KTK', '386567').

card_in_set('island', 'KTK').
card_original_type('island'/'KTK', 'Basic Land — Island').
card_original_text('island'/'KTK', 'U').
card_image_name('island'/'KTK', 'island4').
card_uid('island'/'KTK', 'KTK:Island:island4').
card_rarity('island'/'KTK', 'Basic Land').
card_artist('island'/'KTK', 'Adam Paquette').
card_number('island'/'KTK', '257').
card_multiverse_id('island'/'KTK', '386568').

card_in_set('ivorytusk fortress', 'KTK').
card_original_type('ivorytusk fortress'/'KTK', 'Creature — Elephant').
card_original_text('ivorytusk fortress'/'KTK', 'Untap each creature you control with a +1/+1 counter on it during each other player\'s untap step.').
card_image_name('ivorytusk fortress'/'KTK', 'ivorytusk fortress').
card_uid('ivorytusk fortress'/'KTK', 'KTK:Ivorytusk Fortress:ivorytusk fortress').
card_rarity('ivorytusk fortress'/'KTK', 'Rare').
card_artist('ivorytusk fortress'/'KTK', 'Jasper Sandner').
card_number('ivorytusk fortress'/'KTK', '179').
card_flavor_text('ivorytusk fortress'/'KTK', 'Abzan soldiers march to war confident that their Houses march with them.').
card_multiverse_id('ivorytusk fortress'/'KTK', '386569').
card_watermark('ivorytusk fortress'/'KTK', 'Abzan').

card_in_set('jeering instigator', 'KTK').
card_original_type('jeering instigator'/'KTK', 'Creature — Goblin Rogue').
card_original_text('jeering instigator'/'KTK', 'Morph {2}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Jeering Instigator is turned face up, if it\'s your turn, gain control of another target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_image_name('jeering instigator'/'KTK', 'jeering instigator').
card_uid('jeering instigator'/'KTK', 'KTK:Jeering Instigator:jeering instigator').
card_rarity('jeering instigator'/'KTK', 'Rare').
card_artist('jeering instigator'/'KTK', 'Willian Murai').
card_number('jeering instigator'/'KTK', '113').
card_multiverse_id('jeering instigator'/'KTK', '386570').
card_watermark('jeering instigator'/'KTK', 'Mardu').

card_in_set('jeskai ascendancy', 'KTK').
card_original_type('jeskai ascendancy'/'KTK', 'Enchantment').
card_original_text('jeskai ascendancy'/'KTK', 'Whenever you cast a noncreature spell, creatures you control get +1/+1 until end of turn. Untap those creatures.\nWhenever you cast a noncreature spell, you may draw a card. If you do, discard a card.').
card_image_name('jeskai ascendancy'/'KTK', 'jeskai ascendancy').
card_uid('jeskai ascendancy'/'KTK', 'KTK:Jeskai Ascendancy:jeskai ascendancy').
card_rarity('jeskai ascendancy'/'KTK', 'Rare').
card_artist('jeskai ascendancy'/'KTK', 'Dan Scott').
card_number('jeskai ascendancy'/'KTK', '180').
card_multiverse_id('jeskai ascendancy'/'KTK', '386571').
card_watermark('jeskai ascendancy'/'KTK', 'Jeskai').

card_in_set('jeskai banner', 'KTK').
card_original_type('jeskai banner'/'KTK', 'Artifact').
card_original_text('jeskai banner'/'KTK', '{T}: Add {U}, {R}, or {W} to your mana pool.\n{U}{R}{W}, {T}, Sacrifice Jeskai Banner: Draw a card.').
card_first_print('jeskai banner', 'KTK').
card_image_name('jeskai banner'/'KTK', 'jeskai banner').
card_uid('jeskai banner'/'KTK', 'KTK:Jeskai Banner:jeskai banner').
card_rarity('jeskai banner'/'KTK', 'Common').
card_artist('jeskai banner'/'KTK', 'Daniel Ljunggren').
card_number('jeskai banner'/'KTK', '222').
card_flavor_text('jeskai banner'/'KTK', 'Discipline to persevere, insight to discover.').
card_multiverse_id('jeskai banner'/'KTK', '386572').
card_watermark('jeskai banner'/'KTK', 'Jeskai').

card_in_set('jeskai charm', 'KTK').
card_original_type('jeskai charm'/'KTK', 'Instant').
card_original_text('jeskai charm'/'KTK', 'Choose one —\n• Put target creature on top of its owner\'s library.\n• Jeskai Charm deals 4 damage to target opponent.\n• Creatures you control get +1/+1 and gain lifelink until end of turn.').
card_first_print('jeskai charm', 'KTK').
card_image_name('jeskai charm'/'KTK', 'jeskai charm').
card_uid('jeskai charm'/'KTK', 'KTK:Jeskai Charm:jeskai charm').
card_rarity('jeskai charm'/'KTK', 'Uncommon').
card_artist('jeskai charm'/'KTK', 'Mathias Kollros').
card_number('jeskai charm'/'KTK', '181').
card_multiverse_id('jeskai charm'/'KTK', '386573').
card_watermark('jeskai charm'/'KTK', 'Jeskai').

card_in_set('jeskai elder', 'KTK').
card_original_type('jeskai elder'/'KTK', 'Creature — Human Monk').
card_original_text('jeskai elder'/'KTK', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever Jeskai Elder deals combat damage to a player, you may draw a card. If you do, discard a card.').
card_image_name('jeskai elder'/'KTK', 'jeskai elder').
card_uid('jeskai elder'/'KTK', 'KTK:Jeskai Elder:jeskai elder').
card_rarity('jeskai elder'/'KTK', 'Uncommon').
card_artist('jeskai elder'/'KTK', 'Craig J Spearing').
card_number('jeskai elder'/'KTK', '43').
card_multiverse_id('jeskai elder'/'KTK', '386574').
card_watermark('jeskai elder'/'KTK', 'Jeskai').

card_in_set('jeskai student', 'KTK').
card_original_type('jeskai student'/'KTK', 'Creature — Human Monk').
card_original_text('jeskai student'/'KTK', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('jeskai student', 'KTK').
card_image_name('jeskai student'/'KTK', 'jeskai student').
card_uid('jeskai student'/'KTK', 'KTK:Jeskai Student:jeskai student').
card_rarity('jeskai student'/'KTK', 'Common').
card_artist('jeskai student'/'KTK', 'David Gaillet').
card_number('jeskai student'/'KTK', '14').
card_flavor_text('jeskai student'/'KTK', 'Discipline is the first pillar of the Jeskai Way. Each member of the clan trains in a weapon, perfecting its use over a lifetime.').
card_multiverse_id('jeskai student'/'KTK', '386575').
card_watermark('jeskai student'/'KTK', 'Jeskai').

card_in_set('jeskai windscout', 'KTK').
card_original_type('jeskai windscout'/'KTK', 'Creature — Bird Scout').
card_original_text('jeskai windscout'/'KTK', 'Flying\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('jeskai windscout', 'KTK').
card_image_name('jeskai windscout'/'KTK', 'jeskai windscout').
card_uid('jeskai windscout'/'KTK', 'KTK:Jeskai Windscout:jeskai windscout').
card_rarity('jeskai windscout'/'KTK', 'Common').
card_artist('jeskai windscout'/'KTK', 'Johann Bodin').
card_number('jeskai windscout'/'KTK', '44').
card_flavor_text('jeskai windscout'/'KTK', 'They range from Sage-Eye Stronghold to the farthest reaches of Tarkir.').
card_multiverse_id('jeskai windscout'/'KTK', '386576').
card_watermark('jeskai windscout'/'KTK', 'Jeskai').

card_in_set('jungle hollow', 'KTK').
card_original_type('jungle hollow'/'KTK', 'Land').
card_original_text('jungle hollow'/'KTK', 'Jungle Hollow enters the battlefield tapped.\nWhen Jungle Hollow enters the battlefield, you gain 1 life.\n{T}: Add {B} or {G} to your mana pool.').
card_first_print('jungle hollow', 'KTK').
card_image_name('jungle hollow'/'KTK', 'jungle hollow').
card_uid('jungle hollow'/'KTK', 'KTK:Jungle Hollow:jungle hollow').
card_rarity('jungle hollow'/'KTK', 'Common').
card_artist('jungle hollow'/'KTK', 'Eytan Zana').
card_number('jungle hollow'/'KTK', '235').
card_multiverse_id('jungle hollow'/'KTK', '386577').

card_in_set('kheru bloodsucker', 'KTK').
card_original_type('kheru bloodsucker'/'KTK', 'Creature — Vampire').
card_original_text('kheru bloodsucker'/'KTK', 'Whenever a creature you control with toughness 4 or greater dies, each opponent loses 2 life and you gain 2 life.\n{2}{B}, Sacrifice another creature: Put a +1/+1 counter on Kheru Bloodsucker.').
card_first_print('kheru bloodsucker', 'KTK').
card_image_name('kheru bloodsucker'/'KTK', 'kheru bloodsucker').
card_uid('kheru bloodsucker'/'KTK', 'KTK:Kheru Bloodsucker:kheru bloodsucker').
card_rarity('kheru bloodsucker'/'KTK', 'Uncommon').
card_artist('kheru bloodsucker'/'KTK', 'Daniel Ljunggren').
card_number('kheru bloodsucker'/'KTK', '75').
card_flavor_text('kheru bloodsucker'/'KTK', 'It stares through the empty, pain-twisted faces of those it has drained.').
card_multiverse_id('kheru bloodsucker'/'KTK', '386578').
card_watermark('kheru bloodsucker'/'KTK', 'Sultai').

card_in_set('kheru dreadmaw', 'KTK').
card_original_type('kheru dreadmaw'/'KTK', 'Creature — Zombie Crocodile').
card_original_text('kheru dreadmaw'/'KTK', 'Defender\n{1}{G}, Sacrifice another creature: You gain life equal to the sacrificed creature\'s toughness.').
card_first_print('kheru dreadmaw', 'KTK').
card_image_name('kheru dreadmaw'/'KTK', 'kheru dreadmaw').
card_uid('kheru dreadmaw'/'KTK', 'KTK:Kheru Dreadmaw:kheru dreadmaw').
card_rarity('kheru dreadmaw'/'KTK', 'Common').
card_artist('kheru dreadmaw'/'KTK', 'Ryan Yee').
card_number('kheru dreadmaw'/'KTK', '76').
card_flavor_text('kheru dreadmaw'/'KTK', 'Its hunting instincts have long since rotted away. Its hunger, however, remains.').
card_multiverse_id('kheru dreadmaw'/'KTK', '386579').
card_watermark('kheru dreadmaw'/'KTK', 'Sultai').

card_in_set('kheru lich lord', 'KTK').
card_original_type('kheru lich lord'/'KTK', 'Creature — Zombie Wizard').
card_original_text('kheru lich lord'/'KTK', 'At the beginning of your upkeep, you may pay {2}{B}. If you do, return a creature card at random from your graveyard to the battlefield. It gains flying, trample, and haste. Exile that card at the beginning of your next end step. If it would leave the battlefield, exile it instead of putting it anywhere else.').
card_image_name('kheru lich lord'/'KTK', 'kheru lich lord').
card_uid('kheru lich lord'/'KTK', 'KTK:Kheru Lich Lord:kheru lich lord').
card_rarity('kheru lich lord'/'KTK', 'Rare').
card_artist('kheru lich lord'/'KTK', 'Karl Kopinski').
card_number('kheru lich lord'/'KTK', '182').
card_multiverse_id('kheru lich lord'/'KTK', '386580').
card_watermark('kheru lich lord'/'KTK', 'Sultai').

card_in_set('kheru spellsnatcher', 'KTK').
card_original_type('kheru spellsnatcher'/'KTK', 'Creature — Naga Wizard').
card_original_text('kheru spellsnatcher'/'KTK', 'Morph {4}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Kheru Spellsnatcher is turned face up, counter target spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard. You may cast that card without paying its mana cost for as long as it remains exiled.').
card_first_print('kheru spellsnatcher', 'KTK').
card_image_name('kheru spellsnatcher'/'KTK', 'kheru spellsnatcher').
card_uid('kheru spellsnatcher'/'KTK', 'KTK:Kheru Spellsnatcher:kheru spellsnatcher').
card_rarity('kheru spellsnatcher'/'KTK', 'Rare').
card_artist('kheru spellsnatcher'/'KTK', 'Clint Cearley').
card_number('kheru spellsnatcher'/'KTK', '45').
card_multiverse_id('kheru spellsnatcher'/'KTK', '386581').
card_watermark('kheru spellsnatcher'/'KTK', 'Sultai').

card_in_set('kill shot', 'KTK').
card_original_type('kill shot'/'KTK', 'Instant').
card_original_text('kill shot'/'KTK', 'Destroy target attacking creature.').
card_first_print('kill shot', 'KTK').
card_image_name('kill shot'/'KTK', 'kill shot').
card_uid('kill shot'/'KTK', 'KTK:Kill Shot:kill shot').
card_rarity('kill shot'/'KTK', 'Common').
card_artist('kill shot'/'KTK', 'Michael C. Hayes').
card_number('kill shot'/'KTK', '15').
card_flavor_text('kill shot'/'KTK', 'Mardu archers are trained in Dakla, the way of the bow. They never miss their target, no matter how small, how fast, or how far away.').
card_multiverse_id('kill shot'/'KTK', '386582').

card_in_set('kin-tree invocation', 'KTK').
card_original_type('kin-tree invocation'/'KTK', 'Sorcery').
card_original_text('kin-tree invocation'/'KTK', 'Put an X/X black and green Spirit Warrior creature token onto the battlefield, where X is the greatest toughness among creatures you control.').
card_first_print('kin-tree invocation', 'KTK').
card_image_name('kin-tree invocation'/'KTK', 'kin-tree invocation').
card_uid('kin-tree invocation'/'KTK', 'KTK:Kin-Tree Invocation:kin-tree invocation').
card_rarity('kin-tree invocation'/'KTK', 'Uncommon').
card_artist('kin-tree invocation'/'KTK', 'Ryan Alexander Lee').
card_number('kin-tree invocation'/'KTK', '183').
card_flavor_text('kin-tree invocation'/'KTK', 'The passing years add new rings to the tree\'s trunk, bolstering the spirits that dwell within.').
card_multiverse_id('kin-tree invocation'/'KTK', '386583').

card_in_set('kin-tree warden', 'KTK').
card_original_type('kin-tree warden'/'KTK', 'Creature — Human Warrior').
card_original_text('kin-tree warden'/'KTK', '{2}: Regenerate Kin-Tree Warden.\nMorph {G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('kin-tree warden', 'KTK').
card_image_name('kin-tree warden'/'KTK', 'kin-tree warden').
card_uid('kin-tree warden'/'KTK', 'KTK:Kin-Tree Warden:kin-tree warden').
card_rarity('kin-tree warden'/'KTK', 'Common').
card_artist('kin-tree warden'/'KTK', 'Volkan Baga').
card_number('kin-tree warden'/'KTK', '139').
card_flavor_text('kin-tree warden'/'KTK', '\"The amber of the tree and the blood of my veins are the same.\"').
card_multiverse_id('kin-tree warden'/'KTK', '386584').
card_watermark('kin-tree warden'/'KTK', 'Abzan').

card_in_set('krumar bond-kin', 'KTK').
card_original_type('krumar bond-kin'/'KTK', 'Creature — Orc Warrior').
card_original_text('krumar bond-kin'/'KTK', 'Morph {4}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('krumar bond-kin', 'KTK').
card_image_name('krumar bond-kin'/'KTK', 'krumar bond-kin').
card_uid('krumar bond-kin'/'KTK', 'KTK:Krumar Bond-Kin:krumar bond-kin').
card_rarity('krumar bond-kin'/'KTK', 'Common').
card_artist('krumar bond-kin'/'KTK', 'Kev Walker').
card_number('krumar bond-kin'/'KTK', '77').
card_flavor_text('krumar bond-kin'/'KTK', '\"The Abzan replaced my savagery with a noble heritage. I would give my life for my House.\"').
card_multiverse_id('krumar bond-kin'/'KTK', '386585').
card_watermark('krumar bond-kin'/'KTK', 'Abzan').

card_in_set('leaping master', 'KTK').
card_original_type('leaping master'/'KTK', 'Creature — Human Monk').
card_original_text('leaping master'/'KTK', '{2}{W}: Leaping Master gains flying until end of turn.').
card_first_print('leaping master', 'KTK').
card_image_name('leaping master'/'KTK', 'leaping master').
card_uid('leaping master'/'KTK', 'KTK:Leaping Master:leaping master').
card_rarity('leaping master'/'KTK', 'Common').
card_artist('leaping master'/'KTK', 'Anastasia Ovchinnikova').
card_number('leaping master'/'KTK', '114').
card_flavor_text('leaping master'/'KTK', '\"Strength batters down barriers. Discipline ignores them.\"').
card_multiverse_id('leaping master'/'KTK', '386586').
card_watermark('leaping master'/'KTK', 'Jeskai').

card_in_set('lens of clarity', 'KTK').
card_original_type('lens of clarity'/'KTK', 'Artifact').
card_original_text('lens of clarity'/'KTK', 'You may look at the top card of your library and at face-down creatures you don\'t control. (You may do this at any time.)').
card_first_print('lens of clarity', 'KTK').
card_image_name('lens of clarity'/'KTK', 'lens of clarity').
card_uid('lens of clarity'/'KTK', 'KTK:Lens of Clarity:lens of clarity').
card_rarity('lens of clarity'/'KTK', 'Common').
card_artist('lens of clarity'/'KTK', 'Raymond Swanland').
card_number('lens of clarity'/'KTK', '223').
card_flavor_text('lens of clarity'/'KTK', '\"Water shifts and confuses, but as ice it holds the stillness of truth.\"\n—Arel the Whisperer').
card_multiverse_id('lens of clarity'/'KTK', '386587').

card_in_set('longshot squad', 'KTK').
card_original_type('longshot squad'/'KTK', 'Creature — Hound Archer').
card_original_text('longshot squad'/'KTK', 'Outlast {1}{G} ({1}{G}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has reach. (A creature with reach can block creatures with flying.)').
card_first_print('longshot squad', 'KTK').
card_image_name('longshot squad'/'KTK', 'longshot squad').
card_uid('longshot squad'/'KTK', 'KTK:Longshot Squad:longshot squad').
card_rarity('longshot squad'/'KTK', 'Common').
card_artist('longshot squad'/'KTK', 'Wesley Burt').
card_number('longshot squad'/'KTK', '140').
card_multiverse_id('longshot squad'/'KTK', '386588').
card_watermark('longshot squad'/'KTK', 'Abzan').

card_in_set('mantis rider', 'KTK').
card_original_type('mantis rider'/'KTK', 'Creature — Human Monk').
card_original_text('mantis rider'/'KTK', 'Flying, vigilance, haste').
card_first_print('mantis rider', 'KTK').
card_image_name('mantis rider'/'KTK', 'mantis rider').
card_uid('mantis rider'/'KTK', 'KTK:Mantis Rider:mantis rider').
card_rarity('mantis rider'/'KTK', 'Rare').
card_artist('mantis rider'/'KTK', 'Johann Bodin').
card_number('mantis rider'/'KTK', '184').
card_flavor_text('mantis rider'/'KTK', 'Mantis riders know their mounts owe them no allegiance. Even a mantis ridden for years would consume a rider who loses focus for only a moment.').
card_multiverse_id('mantis rider'/'KTK', '386589').
card_watermark('mantis rider'/'KTK', 'Jeskai').

card_in_set('mardu ascendancy', 'KTK').
card_original_type('mardu ascendancy'/'KTK', 'Enchantment').
card_original_text('mardu ascendancy'/'KTK', 'Whenever a nontoken creature you control attacks, put a 1/1 red Goblin creature token onto the battlefield tapped and attacking.\nSacrifice Mardu Ascendancy: Creatures you control get +0/+3 until end of turn.').
card_image_name('mardu ascendancy'/'KTK', 'mardu ascendancy').
card_uid('mardu ascendancy'/'KTK', 'KTK:Mardu Ascendancy:mardu ascendancy').
card_rarity('mardu ascendancy'/'KTK', 'Rare').
card_artist('mardu ascendancy'/'KTK', 'Jason Chan').
card_number('mardu ascendancy'/'KTK', '185').
card_multiverse_id('mardu ascendancy'/'KTK', '386590').
card_watermark('mardu ascendancy'/'KTK', 'Mardu').

card_in_set('mardu banner', 'KTK').
card_original_type('mardu banner'/'KTK', 'Artifact').
card_original_text('mardu banner'/'KTK', '{T}: Add {R}, {W}, or {B} to your mana pool.\n{R}{W}{B}, {T}, Sacrifice Mardu Banner: Draw a card.').
card_first_print('mardu banner', 'KTK').
card_image_name('mardu banner'/'KTK', 'mardu banner').
card_uid('mardu banner'/'KTK', 'KTK:Mardu Banner:mardu banner').
card_rarity('mardu banner'/'KTK', 'Common').
card_artist('mardu banner'/'KTK', 'Daniel Ljunggren').
card_number('mardu banner'/'KTK', '224').
card_flavor_text('mardu banner'/'KTK', 'Speed to strike, fury to smash.').
card_multiverse_id('mardu banner'/'KTK', '386591').
card_watermark('mardu banner'/'KTK', 'Mardu').

card_in_set('mardu blazebringer', 'KTK').
card_original_type('mardu blazebringer'/'KTK', 'Creature — Ogre Warrior').
card_original_text('mardu blazebringer'/'KTK', 'When Mardu Blazebringer attacks or blocks, sacrifice it at end of combat.').
card_first_print('mardu blazebringer', 'KTK').
card_image_name('mardu blazebringer'/'KTK', 'mardu blazebringer').
card_uid('mardu blazebringer'/'KTK', 'KTK:Mardu Blazebringer:mardu blazebringer').
card_rarity('mardu blazebringer'/'KTK', 'Uncommon').
card_artist('mardu blazebringer'/'KTK', 'Peter Mohrbacher').
card_number('mardu blazebringer'/'KTK', '115').
card_flavor_text('mardu blazebringer'/'KTK', '\"Make sure he\'s pointed in the right direction before you light him. And don\'t let the goblins anywhere near the torch.\"\n—Kerai Suddenblade, Mardu hordechief').
card_multiverse_id('mardu blazebringer'/'KTK', '386592').
card_watermark('mardu blazebringer'/'KTK', 'Mardu').

card_in_set('mardu charm', 'KTK').
card_original_type('mardu charm'/'KTK', 'Instant').
card_original_text('mardu charm'/'KTK', 'Choose one —\n• Mardu Charm deals 4 damage to target creature.\n• Put two 1/1 white Warrior creature tokens onto the battlefield. They gain first strike until end of turn.\n• Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_first_print('mardu charm', 'KTK').
card_image_name('mardu charm'/'KTK', 'mardu charm').
card_uid('mardu charm'/'KTK', 'KTK:Mardu Charm:mardu charm').
card_rarity('mardu charm'/'KTK', 'Uncommon').
card_artist('mardu charm'/'KTK', 'Mathias Kollros').
card_number('mardu charm'/'KTK', '186').
card_multiverse_id('mardu charm'/'KTK', '386593').
card_watermark('mardu charm'/'KTK', 'Mardu').

card_in_set('mardu hateblade', 'KTK').
card_original_type('mardu hateblade'/'KTK', 'Creature — Human Warrior').
card_original_text('mardu hateblade'/'KTK', '{B}: Mardu Hateblade gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('mardu hateblade', 'KTK').
card_image_name('mardu hateblade'/'KTK', 'mardu hateblade').
card_uid('mardu hateblade'/'KTK', 'KTK:Mardu Hateblade:mardu hateblade').
card_rarity('mardu hateblade'/'KTK', 'Common').
card_artist('mardu hateblade'/'KTK', 'Ryan Alexander Lee').
card_number('mardu hateblade'/'KTK', '16').
card_flavor_text('mardu hateblade'/'KTK', '\"There may be little honor in my tactics, but there is no honor in losing.\"').
card_multiverse_id('mardu hateblade'/'KTK', '386594').
card_watermark('mardu hateblade'/'KTK', 'Mardu').

card_in_set('mardu heart-piercer', 'KTK').
card_original_type('mardu heart-piercer'/'KTK', 'Creature — Human Archer').
card_original_text('mardu heart-piercer'/'KTK', 'Raid — When Mardu Heart-Piercer enters the battlefield, if you attacked with a creature this turn, Mardu Heart-Piercer deals 2 damage to target creature or player.').
card_image_name('mardu heart-piercer'/'KTK', 'mardu heart-piercer').
card_uid('mardu heart-piercer'/'KTK', 'KTK:Mardu Heart-Piercer:mardu heart-piercer').
card_rarity('mardu heart-piercer'/'KTK', 'Uncommon').
card_artist('mardu heart-piercer'/'KTK', 'Karl Kopinski').
card_number('mardu heart-piercer'/'KTK', '116').
card_flavor_text('mardu heart-piercer'/'KTK', '\"Those who have never ridden before the wind do not know the true joy of war.\"').
card_multiverse_id('mardu heart-piercer'/'KTK', '386595').
card_watermark('mardu heart-piercer'/'KTK', 'Mardu').

card_in_set('mardu hordechief', 'KTK').
card_original_type('mardu hordechief'/'KTK', 'Creature — Human Warrior').
card_original_text('mardu hordechief'/'KTK', 'Raid — When Mardu Hordechief enters the battlefield, if you attacked with a creature this turn, put a 1/1 white Warrior creature token onto the battlefield.').
card_first_print('mardu hordechief', 'KTK').
card_image_name('mardu hordechief'/'KTK', 'mardu hordechief').
card_uid('mardu hordechief'/'KTK', 'KTK:Mardu Hordechief:mardu hordechief').
card_rarity('mardu hordechief'/'KTK', 'Common').
card_artist('mardu hordechief'/'KTK', 'Torstein Nordstrand').
card_number('mardu hordechief'/'KTK', '17').
card_flavor_text('mardu hordechief'/'KTK', 'The horde grows with each assault.').
card_multiverse_id('mardu hordechief'/'KTK', '386596').
card_watermark('mardu hordechief'/'KTK', 'Mardu').

card_in_set('mardu roughrider', 'KTK').
card_original_type('mardu roughrider'/'KTK', 'Creature — Orc Warrior').
card_original_text('mardu roughrider'/'KTK', 'Whenever Mardu Roughrider attacks, target creature can\'t block this turn.').
card_first_print('mardu roughrider', 'KTK').
card_image_name('mardu roughrider'/'KTK', 'mardu roughrider').
card_uid('mardu roughrider'/'KTK', 'KTK:Mardu Roughrider:mardu roughrider').
card_rarity('mardu roughrider'/'KTK', 'Uncommon').
card_artist('mardu roughrider'/'KTK', 'Kev Walker').
card_number('mardu roughrider'/'KTK', '187').
card_flavor_text('mardu roughrider'/'KTK', 'The most ferocious saddlebrutes lead the assault, ramming through massed pikes and stout barricades as if they were paper and silk.').
card_multiverse_id('mardu roughrider'/'KTK', '386597').
card_watermark('mardu roughrider'/'KTK', 'Mardu').

card_in_set('mardu skullhunter', 'KTK').
card_original_type('mardu skullhunter'/'KTK', 'Creature — Human Warrior').
card_original_text('mardu skullhunter'/'KTK', 'Mardu Skullhunter enters the battlefield tapped.\nRaid — When Mardu Skullhunter enters the battlefield, if you attacked with a creature this turn, target opponent discards a card.').
card_first_print('mardu skullhunter', 'KTK').
card_image_name('mardu skullhunter'/'KTK', 'mardu skullhunter').
card_uid('mardu skullhunter'/'KTK', 'KTK:Mardu Skullhunter:mardu skullhunter').
card_rarity('mardu skullhunter'/'KTK', 'Common').
card_artist('mardu skullhunter'/'KTK', 'Jason Rainville').
card_number('mardu skullhunter'/'KTK', '78').
card_multiverse_id('mardu skullhunter'/'KTK', '386598').
card_watermark('mardu skullhunter'/'KTK', 'Mardu').

card_in_set('mardu warshrieker', 'KTK').
card_original_type('mardu warshrieker'/'KTK', 'Creature — Orc Shaman').
card_original_text('mardu warshrieker'/'KTK', 'Raid — When Mardu Warshrieker enters the battlefield, if you attacked with a creature this turn, add {R}{W}{B} to your mana pool.').
card_first_print('mardu warshrieker', 'KTK').
card_image_name('mardu warshrieker'/'KTK', 'mardu warshrieker').
card_uid('mardu warshrieker'/'KTK', 'KTK:Mardu Warshrieker:mardu warshrieker').
card_rarity('mardu warshrieker'/'KTK', 'Common').
card_artist('mardu warshrieker'/'KTK', 'Yefim Kligerman').
card_number('mardu warshrieker'/'KTK', '117').
card_flavor_text('mardu warshrieker'/'KTK', '\"No body can contain so much fury. It reminds me of another battle, long past.\"\n—Sarkhan Vol').
card_multiverse_id('mardu warshrieker'/'KTK', '386599').
card_watermark('mardu warshrieker'/'KTK', 'Mardu').

card_in_set('master of pearls', 'KTK').
card_original_type('master of pearls'/'KTK', 'Creature — Human Monk').
card_original_text('master of pearls'/'KTK', 'Morph {3}{W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Master of Pearls is turned face up, creatures you control get +2/+2 until end of turn.').
card_image_name('master of pearls'/'KTK', 'master of pearls').
card_uid('master of pearls'/'KTK', 'KTK:Master of Pearls:master of pearls').
card_rarity('master of pearls'/'KTK', 'Rare').
card_artist('master of pearls'/'KTK', 'David Gaillet').
card_number('master of pearls'/'KTK', '18').
card_multiverse_id('master of pearls'/'KTK', '386600').
card_watermark('master of pearls'/'KTK', 'Jeskai').

card_in_set('master the way', 'KTK').
card_original_type('master the way'/'KTK', 'Sorcery').
card_original_text('master the way'/'KTK', 'Draw a card. Master the Way deals damage to target creature or player equal to the number of cards in your hand.').
card_first_print('master the way', 'KTK').
card_image_name('master the way'/'KTK', 'master the way').
card_uid('master the way'/'KTK', 'KTK:Master the Way:master the way').
card_rarity('master the way'/'KTK', 'Uncommon').
card_artist('master the way'/'KTK', 'Howard Lyon').
card_number('master the way'/'KTK', '188').
card_flavor_text('master the way'/'KTK', '\"The Way has no beginning and no end. It is simply the path.\"\n—Narset, khan of the Jeskai').
card_multiverse_id('master the way'/'KTK', '386601').

card_in_set('meandering towershell', 'KTK').
card_original_type('meandering towershell'/'KTK', 'Creature — Turtle').
card_original_text('meandering towershell'/'KTK', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhenever Meandering Towershell attacks, exile it. Return it to the battlefield under your control tapped and attacking at the beginning of the declare attackers step on your next turn.').
card_first_print('meandering towershell', 'KTK').
card_image_name('meandering towershell'/'KTK', 'meandering towershell').
card_uid('meandering towershell'/'KTK', 'KTK:Meandering Towershell:meandering towershell').
card_rarity('meandering towershell'/'KTK', 'Rare').
card_artist('meandering towershell'/'KTK', 'YW Tang').
card_number('meandering towershell'/'KTK', '141').
card_multiverse_id('meandering towershell'/'KTK', '386602').
card_watermark('meandering towershell'/'KTK', 'Sultai').

card_in_set('mer-ek nightblade', 'KTK').
card_original_type('mer-ek nightblade'/'KTK', 'Creature — Orc Assassin').
card_original_text('mer-ek nightblade'/'KTK', 'Outlast {B} ({B}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has deathtouch.').
card_first_print('mer-ek nightblade', 'KTK').
card_image_name('mer-ek nightblade'/'KTK', 'mer-ek nightblade').
card_uid('mer-ek nightblade'/'KTK', 'KTK:Mer-Ek Nightblade:mer-ek nightblade').
card_rarity('mer-ek nightblade'/'KTK', 'Uncommon').
card_artist('mer-ek nightblade'/'KTK', 'Lucas Graciano').
card_number('mer-ek nightblade'/'KTK', '79').
card_multiverse_id('mer-ek nightblade'/'KTK', '386603').
card_watermark('mer-ek nightblade'/'KTK', 'Abzan').

card_in_set('mindswipe', 'KTK').
card_original_type('mindswipe'/'KTK', 'Instant').
card_original_text('mindswipe'/'KTK', 'Counter target spell unless its controller pays {X}. Mindswipe deals X damage to that spell\'s controller.').
card_first_print('mindswipe', 'KTK').
card_image_name('mindswipe'/'KTK', 'mindswipe').
card_uid('mindswipe'/'KTK', 'KTK:Mindswipe:mindswipe').
card_rarity('mindswipe'/'KTK', 'Rare').
card_artist('mindswipe'/'KTK', 'Ryan Alexander Lee').
card_number('mindswipe'/'KTK', '189').
card_flavor_text('mindswipe'/'KTK', '\"The past and the unwritten are frozen. To understand their meaning requires heat.\"\n—Arel the Whisperer').
card_multiverse_id('mindswipe'/'KTK', '386604').

card_in_set('mistfire weaver', 'KTK').
card_original_type('mistfire weaver'/'KTK', 'Creature — Djinn Wizard').
card_original_text('mistfire weaver'/'KTK', 'Flying\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Mistfire Weaver is turned face up, target creature you control gains hexproof until end of turn.').
card_first_print('mistfire weaver', 'KTK').
card_image_name('mistfire weaver'/'KTK', 'mistfire weaver').
card_uid('mistfire weaver'/'KTK', 'KTK:Mistfire Weaver:mistfire weaver').
card_rarity('mistfire weaver'/'KTK', 'Uncommon').
card_artist('mistfire weaver'/'KTK', 'Chris Rahn').
card_number('mistfire weaver'/'KTK', '46').
card_multiverse_id('mistfire weaver'/'KTK', '386605').
card_watermark('mistfire weaver'/'KTK', 'Jeskai').

card_in_set('molting snakeskin', 'KTK').
card_original_type('molting snakeskin'/'KTK', 'Enchantment — Aura').
card_original_text('molting snakeskin'/'KTK', 'Enchant creature\nEnchanted creature gets +2/+0 and has \"{2}{B}: Regenerate this creature.\"').
card_first_print('molting snakeskin', 'KTK').
card_image_name('molting snakeskin'/'KTK', 'molting snakeskin').
card_uid('molting snakeskin'/'KTK', 'KTK:Molting Snakeskin:molting snakeskin').
card_rarity('molting snakeskin'/'KTK', 'Common').
card_artist('molting snakeskin'/'KTK', 'YW Tang').
card_number('molting snakeskin'/'KTK', '80').
card_flavor_text('molting snakeskin'/'KTK', 'Flesh wounds are meaningless to those who discard their flesh so readily.').
card_multiverse_id('molting snakeskin'/'KTK', '386606').

card_in_set('monastery flock', 'KTK').
card_original_type('monastery flock'/'KTK', 'Creature — Bird').
card_original_text('monastery flock'/'KTK', 'Defender, flying\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('monastery flock', 'KTK').
card_image_name('monastery flock'/'KTK', 'monastery flock').
card_uid('monastery flock'/'KTK', 'KTK:Monastery Flock:monastery flock').
card_rarity('monastery flock'/'KTK', 'Common').
card_artist('monastery flock'/'KTK', 'John Avon').
card_number('monastery flock'/'KTK', '47').
card_flavor_text('monastery flock'/'KTK', '\"The arrow strikes one bird down, but the flock remains.\"\n—Jeskai teaching').
card_multiverse_id('monastery flock'/'KTK', '386607').

card_in_set('monastery swiftspear', 'KTK').
card_original_type('monastery swiftspear'/'KTK', 'Creature — Human Monk').
card_original_text('monastery swiftspear'/'KTK', 'Haste\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('monastery swiftspear', 'KTK').
card_image_name('monastery swiftspear'/'KTK', 'monastery swiftspear').
card_uid('monastery swiftspear'/'KTK', 'KTK:Monastery Swiftspear:monastery swiftspear').
card_rarity('monastery swiftspear'/'KTK', 'Uncommon').
card_artist('monastery swiftspear'/'KTK', 'Steve Argyle').
card_number('monastery swiftspear'/'KTK', '118').
card_flavor_text('monastery swiftspear'/'KTK', 'The calligraphy of combat is written with strokes of sudden blood.').
card_multiverse_id('monastery swiftspear'/'KTK', '386608').
card_watermark('monastery swiftspear'/'KTK', 'Jeskai').

card_in_set('mountain', 'KTK').
card_original_type('mountain'/'KTK', 'Basic Land — Mountain').
card_original_text('mountain'/'KTK', 'R').
card_image_name('mountain'/'KTK', 'mountain1').
card_uid('mountain'/'KTK', 'KTK:Mountain:mountain1').
card_rarity('mountain'/'KTK', 'Basic Land').
card_artist('mountain'/'KTK', 'Noah Bradley').
card_number('mountain'/'KTK', '262').
card_multiverse_id('mountain'/'KTK', '386612').

card_in_set('mountain', 'KTK').
card_original_type('mountain'/'KTK', 'Basic Land — Mountain').
card_original_text('mountain'/'KTK', 'R').
card_image_name('mountain'/'KTK', 'mountain2').
card_uid('mountain'/'KTK', 'KTK:Mountain:mountain2').
card_rarity('mountain'/'KTK', 'Basic Land').
card_artist('mountain'/'KTK', 'Noah Bradley').
card_number('mountain'/'KTK', '263').
card_multiverse_id('mountain'/'KTK', '386611').

card_in_set('mountain', 'KTK').
card_original_type('mountain'/'KTK', 'Basic Land — Mountain').
card_original_text('mountain'/'KTK', 'R').
card_image_name('mountain'/'KTK', 'mountain3').
card_uid('mountain'/'KTK', 'KTK:Mountain:mountain3').
card_rarity('mountain'/'KTK', 'Basic Land').
card_artist('mountain'/'KTK', 'Florian de Gesincourt').
card_number('mountain'/'KTK', '264').
card_multiverse_id('mountain'/'KTK', '386609').

card_in_set('mountain', 'KTK').
card_original_type('mountain'/'KTK', 'Basic Land — Mountain').
card_original_text('mountain'/'KTK', 'R').
card_image_name('mountain'/'KTK', 'mountain4').
card_uid('mountain'/'KTK', 'KTK:Mountain:mountain4').
card_rarity('mountain'/'KTK', 'Basic Land').
card_artist('mountain'/'KTK', 'Titus Lunter').
card_number('mountain'/'KTK', '265').
card_multiverse_id('mountain'/'KTK', '386610').

card_in_set('murderous cut', 'KTK').
card_original_type('murderous cut'/'KTK', 'Instant').
card_original_text('murderous cut'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nDestroy target creature.').
card_first_print('murderous cut', 'KTK').
card_image_name('murderous cut'/'KTK', 'murderous cut').
card_uid('murderous cut'/'KTK', 'KTK:Murderous Cut:murderous cut').
card_rarity('murderous cut'/'KTK', 'Uncommon').
card_artist('murderous cut'/'KTK', 'Yohann Schepacz').
card_number('murderous cut'/'KTK', '81').
card_flavor_text('murderous cut'/'KTK', 'The blades of a Sultai assassin stab like the fangs of a dragon.').
card_multiverse_id('murderous cut'/'KTK', '386613').
card_watermark('murderous cut'/'KTK', 'Sultai').

card_in_set('mystic monastery', 'KTK').
card_original_type('mystic monastery'/'KTK', 'Land').
card_original_text('mystic monastery'/'KTK', 'Mystic Monastery enters the battlefield tapped.\n{T}: Add {U}, {R}, or {W} to your mana pool.').
card_image_name('mystic monastery'/'KTK', 'mystic monastery').
card_uid('mystic monastery'/'KTK', 'KTK:Mystic Monastery:mystic monastery').
card_rarity('mystic monastery'/'KTK', 'Uncommon').
card_artist('mystic monastery'/'KTK', 'Florian de Gesincourt').
card_number('mystic monastery'/'KTK', '236').
card_flavor_text('mystic monastery'/'KTK', 'When asked how many paths reach enlightenment, the monk kicked a heap of sand. \"Count,\" he smiled, \"and then find more grains.\"').
card_multiverse_id('mystic monastery'/'KTK', '386614').
card_watermark('mystic monastery'/'KTK', 'Jeskai').

card_in_set('mystic of the hidden way', 'KTK').
card_original_type('mystic of the hidden way'/'KTK', 'Creature — Human Monk').
card_original_text('mystic of the hidden way'/'KTK', 'Mystic of the Hidden Way can\'t be blocked.\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('mystic of the hidden way', 'KTK').
card_image_name('mystic of the hidden way'/'KTK', 'mystic of the hidden way').
card_uid('mystic of the hidden way'/'KTK', 'KTK:Mystic of the Hidden Way:mystic of the hidden way').
card_rarity('mystic of the hidden way'/'KTK', 'Common').
card_artist('mystic of the hidden way'/'KTK', 'Ryan Alexander Lee').
card_number('mystic of the hidden way'/'KTK', '48').
card_flavor_text('mystic of the hidden way'/'KTK', '\"There are no obstacles, only different paths.\"').
card_multiverse_id('mystic of the hidden way'/'KTK', '386615').
card_watermark('mystic of the hidden way'/'KTK', 'Jeskai').

card_in_set('narset, enlightened master', 'KTK').
card_original_type('narset, enlightened master'/'KTK', 'Legendary Creature — Human Monk').
card_original_text('narset, enlightened master'/'KTK', 'First strike, hexproof\nWhenever Narset, Enlightened Master attacks, exile the top four cards of your library. Until end of turn, you may cast noncreature cards exiled with Narset this turn without paying their mana costs.').
card_image_name('narset, enlightened master'/'KTK', 'narset, enlightened master').
card_uid('narset, enlightened master'/'KTK', 'KTK:Narset, Enlightened Master:narset, enlightened master').
card_rarity('narset, enlightened master'/'KTK', 'Mythic Rare').
card_artist('narset, enlightened master'/'KTK', 'Magali Villeneuve').
card_number('narset, enlightened master'/'KTK', '190').
card_multiverse_id('narset, enlightened master'/'KTK', '386616').
card_watermark('narset, enlightened master'/'KTK', 'Jeskai').

card_in_set('naturalize', 'KTK').
card_original_type('naturalize'/'KTK', 'Instant').
card_original_text('naturalize'/'KTK', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'KTK', 'naturalize').
card_uid('naturalize'/'KTK', 'KTK:Naturalize:naturalize').
card_rarity('naturalize'/'KTK', 'Common').
card_artist('naturalize'/'KTK', 'James Paick').
card_number('naturalize'/'KTK', '142').
card_flavor_text('naturalize'/'KTK', 'The remains of ancient sky tyrants now feed the war-torn land.').
card_multiverse_id('naturalize'/'KTK', '386617').

card_in_set('necropolis fiend', 'KTK').
card_original_type('necropolis fiend'/'KTK', 'Creature — Demon').
card_original_text('necropolis fiend'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nFlying\n{X}, {T}, Exile X cards from your graveyard: Target creature gets -X/-X until end of turn.').
card_image_name('necropolis fiend'/'KTK', 'necropolis fiend').
card_uid('necropolis fiend'/'KTK', 'KTK:Necropolis Fiend:necropolis fiend').
card_rarity('necropolis fiend'/'KTK', 'Rare').
card_artist('necropolis fiend'/'KTK', 'Seb McKinnon').
card_number('necropolis fiend'/'KTK', '82').
card_multiverse_id('necropolis fiend'/'KTK', '386618').
card_watermark('necropolis fiend'/'KTK', 'Sultai').

card_in_set('nomad outpost', 'KTK').
card_original_type('nomad outpost'/'KTK', 'Land').
card_original_text('nomad outpost'/'KTK', 'Nomad Outpost enters the battlefield tapped.\n{T}: Add {R}, {W}, or {B} to your mana pool.').
card_image_name('nomad outpost'/'KTK', 'nomad outpost').
card_uid('nomad outpost'/'KTK', 'KTK:Nomad Outpost:nomad outpost').
card_rarity('nomad outpost'/'KTK', 'Uncommon').
card_artist('nomad outpost'/'KTK', 'Noah Bradley').
card_number('nomad outpost'/'KTK', '237').
card_flavor_text('nomad outpost'/'KTK', '\"Only the weak imprison themselves behind walls. We live free under the wind, and our freedom makes us strong.\"\n—Zurgo, khan of the Mardu').
card_multiverse_id('nomad outpost'/'KTK', '386619').
card_watermark('nomad outpost'/'KTK', 'Mardu').

card_in_set('opulent palace', 'KTK').
card_original_type('opulent palace'/'KTK', 'Land').
card_original_text('opulent palace'/'KTK', 'Opulent Palace enters the battlefield tapped.\n{T}: Add {B}, {G}, or {U} to your mana pool.').
card_first_print('opulent palace', 'KTK').
card_image_name('opulent palace'/'KTK', 'opulent palace').
card_uid('opulent palace'/'KTK', 'KTK:Opulent Palace:opulent palace').
card_rarity('opulent palace'/'KTK', 'Uncommon').
card_artist('opulent palace'/'KTK', 'Adam Paquette').
card_number('opulent palace'/'KTK', '238').
card_flavor_text('opulent palace'/'KTK', 'The dense jungle surrenders to a lush and lavish expanse. At its center uncoil the spires of Qarsi Palace.').
card_multiverse_id('opulent palace'/'KTK', '386620').
card_watermark('opulent palace'/'KTK', 'Sultai').

card_in_set('pearl lake ancient', 'KTK').
card_original_type('pearl lake ancient'/'KTK', 'Creature — Leviathan').
card_original_text('pearl lake ancient'/'KTK', 'Flash\nPearl Lake Ancient can\'t be countered.\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nReturn three lands you control to their owner\'s hand: Return Pearl Lake Ancient to its owner\'s hand.').
card_first_print('pearl lake ancient', 'KTK').
card_image_name('pearl lake ancient'/'KTK', 'pearl lake ancient').
card_uid('pearl lake ancient'/'KTK', 'KTK:Pearl Lake Ancient:pearl lake ancient').
card_rarity('pearl lake ancient'/'KTK', 'Mythic Rare').
card_artist('pearl lake ancient'/'KTK', 'Richard Wright').
card_number('pearl lake ancient'/'KTK', '49').
card_multiverse_id('pearl lake ancient'/'KTK', '386621').
card_watermark('pearl lake ancient'/'KTK', 'Jeskai').

card_in_set('pine walker', 'KTK').
card_original_type('pine walker'/'KTK', 'Creature — Elemental').
card_original_text('pine walker'/'KTK', 'Morph {4}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhenever Pine Walker or another creature you control is turned face up, untap that creature.').
card_first_print('pine walker', 'KTK').
card_image_name('pine walker'/'KTK', 'pine walker').
card_uid('pine walker'/'KTK', 'KTK:Pine Walker:pine walker').
card_rarity('pine walker'/'KTK', 'Uncommon').
card_artist('pine walker'/'KTK', 'Dave Kendall').
card_number('pine walker'/'KTK', '143').
card_multiverse_id('pine walker'/'KTK', '386622').
card_watermark('pine walker'/'KTK', 'Temur').

card_in_set('plains', 'KTK').
card_original_type('plains'/'KTK', 'Basic Land — Plains').
card_original_text('plains'/'KTK', 'W').
card_image_name('plains'/'KTK', 'plains1').
card_uid('plains'/'KTK', 'KTK:Plains:plains1').
card_rarity('plains'/'KTK', 'Basic Land').
card_artist('plains'/'KTK', 'Noah Bradley').
card_number('plains'/'KTK', '250').
card_multiverse_id('plains'/'KTK', '386624').

card_in_set('plains', 'KTK').
card_original_type('plains'/'KTK', 'Basic Land — Plains').
card_original_text('plains'/'KTK', 'W').
card_image_name('plains'/'KTK', 'plains2').
card_uid('plains'/'KTK', 'KTK:Plains:plains2').
card_rarity('plains'/'KTK', 'Basic Land').
card_artist('plains'/'KTK', 'Sam Burley').
card_number('plains'/'KTK', '251').
card_multiverse_id('plains'/'KTK', '386625').

card_in_set('plains', 'KTK').
card_original_type('plains'/'KTK', 'Basic Land — Plains').
card_original_text('plains'/'KTK', 'W').
card_image_name('plains'/'KTK', 'plains3').
card_uid('plains'/'KTK', 'KTK:Plains:plains3').
card_rarity('plains'/'KTK', 'Basic Land').
card_artist('plains'/'KTK', 'Sam Burley').
card_number('plains'/'KTK', '252').
card_multiverse_id('plains'/'KTK', '386623').

card_in_set('plains', 'KTK').
card_original_type('plains'/'KTK', 'Basic Land — Plains').
card_original_text('plains'/'KTK', 'W').
card_image_name('plains'/'KTK', 'plains4').
card_uid('plains'/'KTK', 'KTK:Plains:plains4').
card_rarity('plains'/'KTK', 'Basic Land').
card_artist('plains'/'KTK', 'Florian de Gesincourt').
card_number('plains'/'KTK', '253').
card_multiverse_id('plains'/'KTK', '386626').

card_in_set('polluted delta', 'KTK').
card_original_type('polluted delta'/'KTK', 'Land').
card_original_text('polluted delta'/'KTK', '{T}, Pay 1 life, Sacrifice Polluted Delta: Search your library for an Island or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_image_name('polluted delta'/'KTK', 'polluted delta').
card_uid('polluted delta'/'KTK', 'KTK:Polluted Delta:polluted delta').
card_rarity('polluted delta'/'KTK', 'Rare').
card_artist('polluted delta'/'KTK', 'Vincent Proce').
card_number('polluted delta'/'KTK', '239').
card_flavor_text('polluted delta'/'KTK', 'Where dragons once prevailed, their bones now sink.').
card_multiverse_id('polluted delta'/'KTK', '386627').

card_in_set('ponyback brigade', 'KTK').
card_original_type('ponyback brigade'/'KTK', 'Creature — Goblin Warrior').
card_original_text('ponyback brigade'/'KTK', 'When Ponyback Brigade enters the battlefield or is turned face up, put three 1/1 red Goblin creature tokens onto the battlefield.\nMorph {2}{R}{W}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('ponyback brigade', 'KTK').
card_image_name('ponyback brigade'/'KTK', 'ponyback brigade').
card_uid('ponyback brigade'/'KTK', 'KTK:Ponyback Brigade:ponyback brigade').
card_rarity('ponyback brigade'/'KTK', 'Common').
card_artist('ponyback brigade'/'KTK', 'Mark Zug').
card_number('ponyback brigade'/'KTK', '191').
card_multiverse_id('ponyback brigade'/'KTK', '386628').
card_watermark('ponyback brigade'/'KTK', 'Mardu').

card_in_set('quiet contemplation', 'KTK').
card_original_type('quiet contemplation'/'KTK', 'Enchantment').
card_original_text('quiet contemplation'/'KTK', 'Whenever you cast a noncreature spell, you may pay {1}. If you do, tap target creature an opponent controls and it doesn\'t untap during its controller\'s next untap step.').
card_first_print('quiet contemplation', 'KTK').
card_image_name('quiet contemplation'/'KTK', 'quiet contemplation').
card_uid('quiet contemplation'/'KTK', 'KTK:Quiet Contemplation:quiet contemplation').
card_rarity('quiet contemplation'/'KTK', 'Uncommon').
card_artist('quiet contemplation'/'KTK', 'Magali Villeneuve').
card_number('quiet contemplation'/'KTK', '50').
card_multiverse_id('quiet contemplation'/'KTK', '386629').
card_watermark('quiet contemplation'/'KTK', 'Jeskai').

card_in_set('raiders\' spoils', 'KTK').
card_original_type('raiders\' spoils'/'KTK', 'Enchantment').
card_original_text('raiders\' spoils'/'KTK', 'Creatures you control get +1/+0.\nWhenever a Warrior you control deals combat damage to a player, you may pay 1 life. If you do, draw a card.').
card_first_print('raiders\' spoils', 'KTK').
card_image_name('raiders\' spoils'/'KTK', 'raiders\' spoils').
card_uid('raiders\' spoils'/'KTK', 'KTK:Raiders\' Spoils:raiders\' spoils').
card_rarity('raiders\' spoils'/'KTK', 'Uncommon').
card_artist('raiders\' spoils'/'KTK', 'Wayne Reynolds').
card_number('raiders\' spoils'/'KTK', '83').
card_flavor_text('raiders\' spoils'/'KTK', '\"To conquer is to eat.\"\n—Edicts of Ilagra').
card_multiverse_id('raiders\' spoils'/'KTK', '386630').

card_in_set('rakshasa deathdealer', 'KTK').
card_original_type('rakshasa deathdealer'/'KTK', 'Creature — Cat Demon').
card_original_text('rakshasa deathdealer'/'KTK', '{B}{G}: Rakshasa Deathdealer gets +2/+2 until end of turn.\n{B}{G}: Regenerate Rakshasa Deathdealer.').
card_first_print('rakshasa deathdealer', 'KTK').
card_image_name('rakshasa deathdealer'/'KTK', 'rakshasa deathdealer').
card_uid('rakshasa deathdealer'/'KTK', 'KTK:Rakshasa Deathdealer:rakshasa deathdealer').
card_rarity('rakshasa deathdealer'/'KTK', 'Rare').
card_artist('rakshasa deathdealer'/'KTK', 'John Severin Brassell').
card_number('rakshasa deathdealer'/'KTK', '192').
card_flavor_text('rakshasa deathdealer'/'KTK', '\"Death fills me and makes me strong. You, it will reduce to nothing.\"').
card_multiverse_id('rakshasa deathdealer'/'KTK', '386631').
card_watermark('rakshasa deathdealer'/'KTK', 'Sultai').

card_in_set('rakshasa vizier', 'KTK').
card_original_type('rakshasa vizier'/'KTK', 'Creature — Cat Demon').
card_original_text('rakshasa vizier'/'KTK', 'Whenever one or more cards are put into exile from your graveyard, put that many +1/+1 counters on Rakshasa Vizier.').
card_image_name('rakshasa vizier'/'KTK', 'rakshasa vizier').
card_uid('rakshasa vizier'/'KTK', 'KTK:Rakshasa Vizier:rakshasa vizier').
card_rarity('rakshasa vizier'/'KTK', 'Rare').
card_artist('rakshasa vizier'/'KTK', 'Nils Hamm').
card_number('rakshasa vizier'/'KTK', '193').
card_flavor_text('rakshasa vizier'/'KTK', 'Rakshasa offer deals that sound advantageous to those who forget who they are dealing with.').
card_multiverse_id('rakshasa vizier'/'KTK', '386632').
card_watermark('rakshasa vizier'/'KTK', 'Sultai').

card_in_set('rakshasa\'s secret', 'KTK').
card_original_type('rakshasa\'s secret'/'KTK', 'Sorcery').
card_original_text('rakshasa\'s secret'/'KTK', 'Target opponent discards two cards. Put the top two cards of your library into your graveyard.').
card_first_print('rakshasa\'s secret', 'KTK').
card_image_name('rakshasa\'s secret'/'KTK', 'rakshasa\'s secret').
card_uid('rakshasa\'s secret'/'KTK', 'KTK:Rakshasa\'s Secret:rakshasa\'s secret').
card_rarity('rakshasa\'s secret'/'KTK', 'Common').
card_artist('rakshasa\'s secret'/'KTK', 'Magali Villeneuve').
card_number('rakshasa\'s secret'/'KTK', '84').
card_flavor_text('rakshasa\'s secret'/'KTK', 'The voice of a rakshasa is soft, its breath sweet. But every word is the murmur of madness.').
card_multiverse_id('rakshasa\'s secret'/'KTK', '386633').

card_in_set('rattleclaw mystic', 'KTK').
card_original_type('rattleclaw mystic'/'KTK', 'Creature — Human Shaman').
card_original_text('rattleclaw mystic'/'KTK', '{T}: Add {G}, {U}, or {R} to your mana pool.\nMorph {2} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Rattleclaw Mystic is turned face up, add {G}{U}{R} to your mana pool.').
card_image_name('rattleclaw mystic'/'KTK', 'rattleclaw mystic').
card_uid('rattleclaw mystic'/'KTK', 'KTK:Rattleclaw Mystic:rattleclaw mystic').
card_rarity('rattleclaw mystic'/'KTK', 'Rare').
card_artist('rattleclaw mystic'/'KTK', 'Tyler Jacobson').
card_number('rattleclaw mystic'/'KTK', '144').
card_multiverse_id('rattleclaw mystic'/'KTK', '386634').
card_watermark('rattleclaw mystic'/'KTK', 'Temur').

card_in_set('retribution of the ancients', 'KTK').
card_original_type('retribution of the ancients'/'KTK', 'Enchantment').
card_original_text('retribution of the ancients'/'KTK', '{B}, Remove X +1/+1 counters from among creatures you control: Target creature gets -X/-X until end of turn.').
card_first_print('retribution of the ancients', 'KTK').
card_image_name('retribution of the ancients'/'KTK', 'retribution of the ancients').
card_uid('retribution of the ancients'/'KTK', 'KTK:Retribution of the Ancients:retribution of the ancients').
card_rarity('retribution of the ancients'/'KTK', 'Rare').
card_artist('retribution of the ancients'/'KTK', 'Svetlin Velinov').
card_number('retribution of the ancients'/'KTK', '85').
card_flavor_text('retribution of the ancients'/'KTK', 'Abzan ancestors died to protect their Houses, and they protect them still.').
card_multiverse_id('retribution of the ancients'/'KTK', '386635').
card_watermark('retribution of the ancients'/'KTK', 'Abzan').

card_in_set('ride down', 'KTK').
card_original_type('ride down'/'KTK', 'Instant').
card_original_text('ride down'/'KTK', 'Destroy target blocking creature. Creatures that were blocked by that creature this combat gain trample until end of turn.').
card_first_print('ride down', 'KTK').
card_image_name('ride down'/'KTK', 'ride down').
card_uid('ride down'/'KTK', 'KTK:Ride Down:ride down').
card_rarity('ride down'/'KTK', 'Uncommon').
card_artist('ride down'/'KTK', 'Daarken').
card_number('ride down'/'KTK', '194').
card_flavor_text('ride down'/'KTK', '\"I will wash you from my hooves!\"\n—Mardu taunt').
card_multiverse_id('ride down'/'KTK', '386636').

card_in_set('rite of the serpent', 'KTK').
card_original_type('rite of the serpent'/'KTK', 'Sorcery').
card_original_text('rite of the serpent'/'KTK', 'Destroy target creature. If that creature had a +1/+1 counter on it, put a 1/1 green Snake creature token onto the battlefield.').
card_first_print('rite of the serpent', 'KTK').
card_image_name('rite of the serpent'/'KTK', 'rite of the serpent').
card_uid('rite of the serpent'/'KTK', 'KTK:Rite of the Serpent:rite of the serpent').
card_rarity('rite of the serpent'/'KTK', 'Common').
card_artist('rite of the serpent'/'KTK', 'Seb McKinnon').
card_number('rite of the serpent'/'KTK', '86').
card_flavor_text('rite of the serpent'/'KTK', '\"From your death, new life. From your loss, our profit.\"\n—Kirada, Qarsi overseer').
card_multiverse_id('rite of the serpent'/'KTK', '386637').

card_in_set('riverwheel aerialists', 'KTK').
card_original_type('riverwheel aerialists'/'KTK', 'Creature — Djinn Monk').
card_original_text('riverwheel aerialists'/'KTK', 'Flying\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('riverwheel aerialists', 'KTK').
card_image_name('riverwheel aerialists'/'KTK', 'riverwheel aerialists').
card_uid('riverwheel aerialists'/'KTK', 'KTK:Riverwheel Aerialists:riverwheel aerialists').
card_rarity('riverwheel aerialists'/'KTK', 'Uncommon').
card_artist('riverwheel aerialists'/'KTK', 'Jack Wang').
card_number('riverwheel aerialists'/'KTK', '51').
card_flavor_text('riverwheel aerialists'/'KTK', 'Adepts of the Riverwheel Stronghold can run through rain and never get wet; masters use the raindrops as stepping stones.').
card_multiverse_id('riverwheel aerialists'/'KTK', '386638').
card_watermark('riverwheel aerialists'/'KTK', 'Jeskai').

card_in_set('roar of challenge', 'KTK').
card_original_type('roar of challenge'/'KTK', 'Sorcery').
card_original_text('roar of challenge'/'KTK', 'All creatures able to block target creature this turn do so.\nFerocious — That creature gains indestructible until end of turn if you control a creature with power 4 or greater.').
card_first_print('roar of challenge', 'KTK').
card_image_name('roar of challenge'/'KTK', 'roar of challenge').
card_uid('roar of challenge'/'KTK', 'KTK:Roar of Challenge:roar of challenge').
card_rarity('roar of challenge'/'KTK', 'Uncommon').
card_artist('roar of challenge'/'KTK', 'Viktor Titov').
card_number('roar of challenge'/'KTK', '145').
card_multiverse_id('roar of challenge'/'KTK', '386639').
card_watermark('roar of challenge'/'KTK', 'Temur').

card_in_set('rotting mastodon', 'KTK').
card_original_type('rotting mastodon'/'KTK', 'Creature — Zombie Elephant').
card_original_text('rotting mastodon'/'KTK', '').
card_first_print('rotting mastodon', 'KTK').
card_image_name('rotting mastodon'/'KTK', 'rotting mastodon').
card_uid('rotting mastodon'/'KTK', 'KTK:Rotting Mastodon:rotting mastodon').
card_rarity('rotting mastodon'/'KTK', 'Common').
card_artist('rotting mastodon'/'KTK', 'Nils Hamm').
card_number('rotting mastodon'/'KTK', '87').
card_flavor_text('rotting mastodon'/'KTK', 'Mastodons became extinct long ago, but foul forces of the Gurmag Swamp sometimes animate their decaying remains. The Sultai happily exploit such creatures but consider them inferior to their own necromantic creations.').
card_multiverse_id('rotting mastodon'/'KTK', '386640').

card_in_set('rugged highlands', 'KTK').
card_original_type('rugged highlands'/'KTK', 'Land').
card_original_text('rugged highlands'/'KTK', 'Rugged Highlands enters the battlefield tapped.\nWhen Rugged Highlands enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_first_print('rugged highlands', 'KTK').
card_image_name('rugged highlands'/'KTK', 'rugged highlands').
card_uid('rugged highlands'/'KTK', 'KTK:Rugged Highlands:rugged highlands').
card_rarity('rugged highlands'/'KTK', 'Common').
card_artist('rugged highlands'/'KTK', 'Eytan Zana').
card_number('rugged highlands'/'KTK', '240').
card_multiverse_id('rugged highlands'/'KTK', '386641').

card_in_set('rush of battle', 'KTK').
card_original_type('rush of battle'/'KTK', 'Sorcery').
card_original_text('rush of battle'/'KTK', 'Creatures you control get +2/+1 until end of turn. Warrior creatures you control gain lifelink until end of turn. (Damage dealt by those Warriors also causes their controller to gain that much life.)').
card_first_print('rush of battle', 'KTK').
card_image_name('rush of battle'/'KTK', 'rush of battle').
card_uid('rush of battle'/'KTK', 'KTK:Rush of Battle:rush of battle').
card_rarity('rush of battle'/'KTK', 'Common').
card_artist('rush of battle'/'KTK', 'Dan Scott').
card_number('rush of battle'/'KTK', '19').
card_flavor_text('rush of battle'/'KTK', 'The Mardu charge reflects the dragon\'s speed—and its hunger.').
card_multiverse_id('rush of battle'/'KTK', '386642').

card_in_set('ruthless ripper', 'KTK').
card_original_type('ruthless ripper'/'KTK', 'Creature — Human Assassin').
card_original_text('ruthless ripper'/'KTK', 'Deathtouch\nMorph—Reveal a black card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Ruthless Ripper is turned face up, target player loses 2 life.').
card_first_print('ruthless ripper', 'KTK').
card_image_name('ruthless ripper'/'KTK', 'ruthless ripper').
card_uid('ruthless ripper'/'KTK', 'KTK:Ruthless Ripper:ruthless ripper').
card_rarity('ruthless ripper'/'KTK', 'Uncommon').
card_artist('ruthless ripper'/'KTK', 'Clint Cearley').
card_number('ruthless ripper'/'KTK', '88').
card_multiverse_id('ruthless ripper'/'KTK', '386643').
card_watermark('ruthless ripper'/'KTK', 'Sultai').

card_in_set('sage of the inward eye', 'KTK').
card_original_type('sage of the inward eye'/'KTK', 'Creature — Djinn Wizard').
card_original_text('sage of the inward eye'/'KTK', 'Flying\nWhenever you cast a noncreature spell, creatures you control gain lifelink until end of turn.').
card_image_name('sage of the inward eye'/'KTK', 'sage of the inward eye').
card_uid('sage of the inward eye'/'KTK', 'KTK:Sage of the Inward Eye:sage of the inward eye').
card_rarity('sage of the inward eye'/'KTK', 'Rare').
card_artist('sage of the inward eye'/'KTK', 'Chase Stone').
card_number('sage of the inward eye'/'KTK', '195').
card_flavor_text('sage of the inward eye'/'KTK', '\"No one petal claims beauty for the lotus.\"').
card_multiverse_id('sage of the inward eye'/'KTK', '386644').
card_watermark('sage of the inward eye'/'KTK', 'Jeskai').

card_in_set('sage-eye harrier', 'KTK').
card_original_type('sage-eye harrier'/'KTK', 'Creature — Bird Warrior').
card_original_text('sage-eye harrier'/'KTK', 'Flying\nMorph {3}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('sage-eye harrier', 'KTK').
card_image_name('sage-eye harrier'/'KTK', 'sage-eye harrier').
card_uid('sage-eye harrier'/'KTK', 'KTK:Sage-Eye Harrier:sage-eye harrier').
card_rarity('sage-eye harrier'/'KTK', 'Common').
card_artist('sage-eye harrier'/'KTK', 'Chase Stone').
card_number('sage-eye harrier'/'KTK', '20').
card_flavor_text('sage-eye harrier'/'KTK', 'These winged warriors meditate in flight, tracing mandalas in the clouds.').
card_multiverse_id('sage-eye harrier'/'KTK', '386645').
card_watermark('sage-eye harrier'/'KTK', 'Jeskai').

card_in_set('sagu archer', 'KTK').
card_original_type('sagu archer'/'KTK', 'Creature — Naga Archer').
card_original_text('sagu archer'/'KTK', 'Reach (This creature can block creatures with flying.)\nMorph {4}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('sagu archer', 'KTK').
card_image_name('sagu archer'/'KTK', 'sagu archer').
card_uid('sagu archer'/'KTK', 'KTK:Sagu Archer:sagu archer').
card_rarity('sagu archer'/'KTK', 'Common').
card_artist('sagu archer'/'KTK', 'Steven Belledin').
card_number('sagu archer'/'KTK', '146').
card_flavor_text('sagu archer'/'KTK', 'His arrows whistle like a serpent\'s hiss.').
card_multiverse_id('sagu archer'/'KTK', '386646').
card_watermark('sagu archer'/'KTK', 'Sultai').

card_in_set('sagu mauler', 'KTK').
card_original_type('sagu mauler'/'KTK', 'Creature — Beast').
card_original_text('sagu mauler'/'KTK', 'Trample, hexproof\nMorph {3}{G}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('sagu mauler', 'KTK').
card_image_name('sagu mauler'/'KTK', 'sagu mauler').
card_uid('sagu mauler'/'KTK', 'KTK:Sagu Mauler:sagu mauler').
card_rarity('sagu mauler'/'KTK', 'Rare').
card_artist('sagu mauler'/'KTK', 'Raymond Swanland').
card_number('sagu mauler'/'KTK', '196').
card_flavor_text('sagu mauler'/'KTK', 'The Sagu Jungle\'s thick undergrowth conceals even the largest predators—if they wish to remain hidden.').
card_multiverse_id('sagu mauler'/'KTK', '386647').

card_in_set('salt road patrol', 'KTK').
card_original_type('salt road patrol'/'KTK', 'Creature — Human Scout').
card_original_text('salt road patrol'/'KTK', 'Outlast {1}{W} ({1}{W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)').
card_first_print('salt road patrol', 'KTK').
card_image_name('salt road patrol'/'KTK', 'salt road patrol').
card_uid('salt road patrol'/'KTK', 'KTK:Salt Road Patrol:salt road patrol').
card_rarity('salt road patrol'/'KTK', 'Common').
card_artist('salt road patrol'/'KTK', 'Scott Murphy').
card_number('salt road patrol'/'KTK', '21').
card_flavor_text('salt road patrol'/'KTK', '\"Soldiers win battles, but supplies win wars.\"\n—Kadri, Abzan caravan master').
card_multiverse_id('salt road patrol'/'KTK', '386648').
card_watermark('salt road patrol'/'KTK', 'Abzan').

card_in_set('sandsteppe citadel', 'KTK').
card_original_type('sandsteppe citadel'/'KTK', 'Land').
card_original_text('sandsteppe citadel'/'KTK', 'Sandsteppe Citadel enters the battlefield tapped.\n{T}: Add {W}, {B}, or {G} to your mana pool.').
card_first_print('sandsteppe citadel', 'KTK').
card_image_name('sandsteppe citadel'/'KTK', 'sandsteppe citadel').
card_uid('sandsteppe citadel'/'KTK', 'KTK:Sandsteppe Citadel:sandsteppe citadel').
card_rarity('sandsteppe citadel'/'KTK', 'Uncommon').
card_artist('sandsteppe citadel'/'KTK', 'Sam Burley').
card_number('sandsteppe citadel'/'KTK', '241').
card_flavor_text('sandsteppe citadel'/'KTK', 'That which endures, survives.').
card_multiverse_id('sandsteppe citadel'/'KTK', '386649').
card_watermark('sandsteppe citadel'/'KTK', 'Abzan').

card_in_set('sarkhan, the dragonspeaker', 'KTK').
card_original_type('sarkhan, the dragonspeaker'/'KTK', 'Planeswalker — Sarkhan').
card_original_text('sarkhan, the dragonspeaker'/'KTK', '+1: Until end of turn, Sarkhan, the Dragonspeaker becomes a legendary 4/4 red Dragon creature with flying, indestructible, and haste. (He doesn\'t lose loyalty while he\'s not a planeswalker.)\n−3: Sarkhan, the Dragonspeaker deals 4 damage to target creature.\n−6: You get an emblem with \"At the beginning of your draw step, draw two additional cards\" and \"At the beginning of your end step, discard your hand.\"').
card_first_print('sarkhan, the dragonspeaker', 'KTK').
card_image_name('sarkhan, the dragonspeaker'/'KTK', 'sarkhan, the dragonspeaker').
card_uid('sarkhan, the dragonspeaker'/'KTK', 'KTK:Sarkhan, the Dragonspeaker:sarkhan, the dragonspeaker').
card_rarity('sarkhan, the dragonspeaker'/'KTK', 'Mythic Rare').
card_artist('sarkhan, the dragonspeaker'/'KTK', 'Daarken').
card_number('sarkhan, the dragonspeaker'/'KTK', '119').
card_multiverse_id('sarkhan, the dragonspeaker'/'KTK', '386650').

card_in_set('savage knuckleblade', 'KTK').
card_original_type('savage knuckleblade'/'KTK', 'Creature — Ogre Warrior').
card_original_text('savage knuckleblade'/'KTK', '{2}{G}: Savage Knuckleblade gets +2/+2 until end of turn. Activate this ability only once each turn.\n{2}{U}: Return Savage Knuckleblade to its owner\'s hand.\n{R}: Savage Knuckleblade gains haste until end of turn.').
card_first_print('savage knuckleblade', 'KTK').
card_image_name('savage knuckleblade'/'KTK', 'savage knuckleblade').
card_uid('savage knuckleblade'/'KTK', 'KTK:Savage Knuckleblade:savage knuckleblade').
card_rarity('savage knuckleblade'/'KTK', 'Rare').
card_artist('savage knuckleblade'/'KTK', 'Chris Rahn').
card_number('savage knuckleblade'/'KTK', '197').
card_multiverse_id('savage knuckleblade'/'KTK', '386651').
card_watermark('savage knuckleblade'/'KTK', 'Temur').

card_in_set('savage punch', 'KTK').
card_original_type('savage punch'/'KTK', 'Sorcery').
card_original_text('savage punch'/'KTK', 'Target creature you control fights target creature you don\'t control.\nFerocious — The creature you control gets +2/+2 until end of turn before it fights if you control a creature with power 4 or greater.').
card_first_print('savage punch', 'KTK').
card_image_name('savage punch'/'KTK', 'savage punch').
card_uid('savage punch'/'KTK', 'KTK:Savage Punch:savage punch').
card_rarity('savage punch'/'KTK', 'Common').
card_artist('savage punch'/'KTK', 'Wesley Burt').
card_number('savage punch'/'KTK', '147').
card_multiverse_id('savage punch'/'KTK', '386652').
card_watermark('savage punch'/'KTK', 'Temur').

card_in_set('scaldkin', 'KTK').
card_original_type('scaldkin'/'KTK', 'Creature — Elemental').
card_original_text('scaldkin'/'KTK', 'Flying\n{2}{R}, Sacrifice Scaldkin: Scaldkin deals 2 damage to target creature or player.').
card_first_print('scaldkin', 'KTK').
card_image_name('scaldkin'/'KTK', 'scaldkin').
card_uid('scaldkin'/'KTK', 'KTK:Scaldkin:scaldkin').
card_rarity('scaldkin'/'KTK', 'Common').
card_artist('scaldkin'/'KTK', 'Cliff Childs').
card_number('scaldkin'/'KTK', '52').
card_flavor_text('scaldkin'/'KTK', 'The Temur believe that scaldkin are born when eruptions melt the frozen whispers of sleeping ancestors.').
card_multiverse_id('scaldkin'/'KTK', '386653').
card_watermark('scaldkin'/'KTK', 'Temur').

card_in_set('scion of glaciers', 'KTK').
card_original_type('scion of glaciers'/'KTK', 'Creature — Elemental').
card_original_text('scion of glaciers'/'KTK', '{U}: Scion of Glaciers gets +1/-1 until end of turn.').
card_first_print('scion of glaciers', 'KTK').
card_image_name('scion of glaciers'/'KTK', 'scion of glaciers').
card_uid('scion of glaciers'/'KTK', 'KTK:Scion of Glaciers:scion of glaciers').
card_rarity('scion of glaciers'/'KTK', 'Uncommon').
card_artist('scion of glaciers'/'KTK', 'Titus Lunter').
card_number('scion of glaciers'/'KTK', '53').
card_flavor_text('scion of glaciers'/'KTK', '\"There is nothing so free as the spring river born of winter\'s ice.\"\n—Nitula, the Hunt Caller').
card_multiverse_id('scion of glaciers'/'KTK', '386654').
card_watermark('scion of glaciers'/'KTK', 'Temur').

card_in_set('scoured barrens', 'KTK').
card_original_type('scoured barrens'/'KTK', 'Land').
card_original_text('scoured barrens'/'KTK', 'Scoured Barrens enters the battlefield tapped.\nWhen Scoured Barrens enters the battlefield, you gain 1 life.\n{T}: Add {W} or {B} to your mana pool.').
card_first_print('scoured barrens', 'KTK').
card_image_name('scoured barrens'/'KTK', 'scoured barrens').
card_uid('scoured barrens'/'KTK', 'KTK:Scoured Barrens:scoured barrens').
card_rarity('scoured barrens'/'KTK', 'Common').
card_artist('scoured barrens'/'KTK', 'Eytan Zana').
card_number('scoured barrens'/'KTK', '242').
card_multiverse_id('scoured barrens'/'KTK', '386655').

card_in_set('scout the borders', 'KTK').
card_original_type('scout the borders'/'KTK', 'Sorcery').
card_original_text('scout the borders'/'KTK', 'Reveal the top five cards of your library. You may put a creature or land card from among them into your hand. Put the rest into your graveyard.').
card_first_print('scout the borders', 'KTK').
card_image_name('scout the borders'/'KTK', 'scout the borders').
card_uid('scout the borders'/'KTK', 'KTK:Scout the Borders:scout the borders').
card_rarity('scout the borders'/'KTK', 'Common').
card_artist('scout the borders'/'KTK', 'James Paick').
card_number('scout the borders'/'KTK', '148').
card_flavor_text('scout the borders'/'KTK', '\"I am in my element: the element of surprise.\"\n—Mogai, Sultai scout').
card_multiverse_id('scout the borders'/'KTK', '386656').
card_watermark('scout the borders'/'KTK', 'Sultai').

card_in_set('secret plans', 'KTK').
card_original_type('secret plans'/'KTK', 'Enchantment').
card_original_text('secret plans'/'KTK', 'Face-down creatures you control get +0/+1.\nWhenever a permanent you control is turned face up, draw a card.').
card_first_print('secret plans', 'KTK').
card_image_name('secret plans'/'KTK', 'secret plans').
card_uid('secret plans'/'KTK', 'KTK:Secret Plans:secret plans').
card_rarity('secret plans'/'KTK', 'Uncommon').
card_artist('secret plans'/'KTK', 'Daarken').
card_number('secret plans'/'KTK', '198').
card_flavor_text('secret plans'/'KTK', 'Rakshasa trade in secrets, amassing wealth from their careful revelation.').
card_multiverse_id('secret plans'/'KTK', '386657').

card_in_set('see the unwritten', 'KTK').
card_original_type('see the unwritten'/'KTK', 'Sorcery').
card_original_text('see the unwritten'/'KTK', 'Reveal the top eight cards of your library. You may put a creature card from among them onto the battlefield. Put the rest into your graveyard.\nFerocious — If you control a creature with power 4 or greater, you may put two creature cards onto the battlefield instead of one.').
card_first_print('see the unwritten', 'KTK').
card_image_name('see the unwritten'/'KTK', 'see the unwritten').
card_uid('see the unwritten'/'KTK', 'KTK:See the Unwritten:see the unwritten').
card_rarity('see the unwritten'/'KTK', 'Mythic Rare').
card_artist('see the unwritten'/'KTK', 'Ryan Barger').
card_number('see the unwritten'/'KTK', '149').
card_multiverse_id('see the unwritten'/'KTK', '386658').
card_watermark('see the unwritten'/'KTK', 'Temur').

card_in_set('seek the horizon', 'KTK').
card_original_type('seek the horizon'/'KTK', 'Sorcery').
card_original_text('seek the horizon'/'KTK', 'Search your library for up to three basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('seek the horizon'/'KTK', 'seek the horizon').
card_uid('seek the horizon'/'KTK', 'KTK:Seek the Horizon:seek the horizon').
card_rarity('seek the horizon'/'KTK', 'Uncommon').
card_artist('seek the horizon'/'KTK', 'Min Yum').
card_number('seek the horizon'/'KTK', '150').
card_flavor_text('seek the horizon'/'KTK', 'The Temur call the flickering lights the Path of Whispers, believing that they lead the way to ancestral knowledge.').
card_multiverse_id('seek the horizon'/'KTK', '386659').

card_in_set('seeker of the way', 'KTK').
card_original_type('seeker of the way'/'KTK', 'Creature — Human Warrior').
card_original_text('seeker of the way'/'KTK', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever you cast a noncreature spell, Seeker of the Way gains lifelink until end of turn.').
card_first_print('seeker of the way', 'KTK').
card_image_name('seeker of the way'/'KTK', 'seeker of the way').
card_uid('seeker of the way'/'KTK', 'KTK:Seeker of the Way:seeker of the way').
card_rarity('seeker of the way'/'KTK', 'Uncommon').
card_artist('seeker of the way'/'KTK', 'Craig J Spearing').
card_number('seeker of the way'/'KTK', '22').
card_flavor_text('seeker of the way'/'KTK', '\"I don\'t know where my destiny lies, but I know it isn\'t here.\"').
card_multiverse_id('seeker of the way'/'KTK', '386660').
card_watermark('seeker of the way'/'KTK', 'Jeskai').

card_in_set('set adrift', 'KTK').
card_original_type('set adrift'/'KTK', 'Sorcery').
card_original_text('set adrift'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nPut target nonland permanent on top of its owner\'s library.').
card_first_print('set adrift', 'KTK').
card_image_name('set adrift'/'KTK', 'set adrift').
card_uid('set adrift'/'KTK', 'KTK:Set Adrift:set adrift').
card_rarity('set adrift'/'KTK', 'Uncommon').
card_artist('set adrift'/'KTK', 'Karl Kopinski').
card_number('set adrift'/'KTK', '54').
card_flavor_text('set adrift'/'KTK', 'The envoy spoke, and Sidisi replied.').
card_multiverse_id('set adrift'/'KTK', '386661').
card_watermark('set adrift'/'KTK', 'Sultai').

card_in_set('shambling attendants', 'KTK').
card_original_type('shambling attendants'/'KTK', 'Creature — Zombie').
card_original_text('shambling attendants'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('shambling attendants', 'KTK').
card_image_name('shambling attendants'/'KTK', 'shambling attendants').
card_uid('shambling attendants'/'KTK', 'KTK:Shambling Attendants:shambling attendants').
card_rarity('shambling attendants'/'KTK', 'Common').
card_artist('shambling attendants'/'KTK', 'Daarken').
card_number('shambling attendants'/'KTK', '89').
card_flavor_text('shambling attendants'/'KTK', '\"Let the world behold what becomes of those who defy us.\"\n—Taigam, Sidisi\'s Hand').
card_multiverse_id('shambling attendants'/'KTK', '386662').
card_watermark('shambling attendants'/'KTK', 'Sultai').

card_in_set('shatter', 'KTK').
card_original_type('shatter'/'KTK', 'Instant').
card_original_text('shatter'/'KTK', 'Destroy target artifact.').
card_image_name('shatter'/'KTK', 'shatter').
card_uid('shatter'/'KTK', 'KTK:Shatter:shatter').
card_rarity('shatter'/'KTK', 'Common').
card_artist('shatter'/'KTK', 'Zoltan Boros').
card_number('shatter'/'KTK', '120').
card_flavor_text('shatter'/'KTK', 'The ogre\'s mind snapped. The bow was next. The archer followed quickly after.').
card_multiverse_id('shatter'/'KTK', '386663').

card_in_set('sidisi\'s pet', 'KTK').
card_original_type('sidisi\'s pet'/'KTK', 'Creature — Zombie Ape').
card_original_text('sidisi\'s pet'/'KTK', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nMorph {1}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('sidisi\'s pet', 'KTK').
card_image_name('sidisi\'s pet'/'KTK', 'sidisi\'s pet').
card_uid('sidisi\'s pet'/'KTK', 'KTK:Sidisi\'s Pet:sidisi\'s pet').
card_rarity('sidisi\'s pet'/'KTK', 'Common').
card_artist('sidisi\'s pet'/'KTK', 'Evan Shipard').
card_number('sidisi\'s pet'/'KTK', '90').
card_flavor_text('sidisi\'s pet'/'KTK', 'The Sultai distinguish between pet and slave by the material of the chain.').
card_multiverse_id('sidisi\'s pet'/'KTK', '386665').
card_watermark('sidisi\'s pet'/'KTK', 'Sultai').

card_in_set('sidisi, brood tyrant', 'KTK').
card_original_type('sidisi, brood tyrant'/'KTK', 'Legendary Creature — Naga Shaman').
card_original_text('sidisi, brood tyrant'/'KTK', 'Whenever Sidisi, Brood Tyrant enters the battlefield or attacks, put the top three cards of your library into your graveyard.\nWhenever one or more creature cards are put into your graveyard from your library, put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('sidisi, brood tyrant'/'KTK', 'sidisi, brood tyrant').
card_uid('sidisi, brood tyrant'/'KTK', 'KTK:Sidisi, Brood Tyrant:sidisi, brood tyrant').
card_rarity('sidisi, brood tyrant'/'KTK', 'Mythic Rare').
card_artist('sidisi, brood tyrant'/'KTK', 'Karl Kopinski').
card_number('sidisi, brood tyrant'/'KTK', '199').
card_multiverse_id('sidisi, brood tyrant'/'KTK', '386664').
card_watermark('sidisi, brood tyrant'/'KTK', 'Sultai').

card_in_set('siege rhino', 'KTK').
card_original_type('siege rhino'/'KTK', 'Creature — Rhino').
card_original_text('siege rhino'/'KTK', 'Trample\nWhen Siege Rhino enters the battlefield, each opponent loses 3 life and you gain 3 life.').
card_image_name('siege rhino'/'KTK', 'siege rhino').
card_uid('siege rhino'/'KTK', 'KTK:Siege Rhino:siege rhino').
card_rarity('siege rhino'/'KTK', 'Rare').
card_artist('siege rhino'/'KTK', 'Volkan Baga').
card_number('siege rhino'/'KTK', '200').
card_flavor_text('siege rhino'/'KTK', 'The mere approach of an Abzan war beast is enough to send enemies fleeing in panic.').
card_multiverse_id('siege rhino'/'KTK', '386666').
card_watermark('siege rhino'/'KTK', 'Abzan').

card_in_set('siegecraft', 'KTK').
card_original_type('siegecraft'/'KTK', 'Enchantment — Aura').
card_original_text('siegecraft'/'KTK', 'Enchant creature\nEnchanted creature gets +2/+4.').
card_first_print('siegecraft', 'KTK').
card_image_name('siegecraft'/'KTK', 'siegecraft').
card_uid('siegecraft'/'KTK', 'KTK:Siegecraft:siegecraft').
card_rarity('siegecraft'/'KTK', 'Common').
card_artist('siegecraft'/'KTK', 'Viktor Titov').
card_number('siegecraft'/'KTK', '23').
card_flavor_text('siegecraft'/'KTK', '\"They thought their fortress impregnable . . . until we marched up with ours, and blocked out the sun.\"\n—Golran, dragonscale captain').
card_multiverse_id('siegecraft'/'KTK', '386667').

card_in_set('singing bell strike', 'KTK').
card_original_type('singing bell strike'/'KTK', 'Enchantment — Aura').
card_original_text('singing bell strike'/'KTK', 'Enchant creature\nWhen Singing Bell Strike enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nEnchanted creature has \"{6}: Untap this creature.\"').
card_first_print('singing bell strike', 'KTK').
card_image_name('singing bell strike'/'KTK', 'singing bell strike').
card_uid('singing bell strike'/'KTK', 'KTK:Singing Bell Strike:singing bell strike').
card_rarity('singing bell strike'/'KTK', 'Common').
card_artist('singing bell strike'/'KTK', 'Chase Stone').
card_number('singing bell strike'/'KTK', '55').
card_multiverse_id('singing bell strike'/'KTK', '386668').

card_in_set('smite the monstrous', 'KTK').
card_original_type('smite the monstrous'/'KTK', 'Instant').
card_original_text('smite the monstrous'/'KTK', 'Destroy target creature with power 4 or greater.').
card_image_name('smite the monstrous'/'KTK', 'smite the monstrous').
card_uid('smite the monstrous'/'KTK', 'KTK:Smite the Monstrous:smite the monstrous').
card_rarity('smite the monstrous'/'KTK', 'Common').
card_artist('smite the monstrous'/'KTK', 'Greg Staples').
card_number('smite the monstrous'/'KTK', '24').
card_flavor_text('smite the monstrous'/'KTK', '\"The dragons thought they were too strong to be tamed, too large to fall. And where are they now?\"\n—Khibat the Revered').
card_multiverse_id('smite the monstrous'/'KTK', '386669').

card_in_set('smoke teller', 'KTK').
card_original_type('smoke teller'/'KTK', 'Creature — Human Shaman').
card_original_text('smoke teller'/'KTK', '{1}{U}: Look at target face-down creature.').
card_first_print('smoke teller', 'KTK').
card_image_name('smoke teller'/'KTK', 'smoke teller').
card_uid('smoke teller'/'KTK', 'KTK:Smoke Teller:smoke teller').
card_rarity('smoke teller'/'KTK', 'Common').
card_artist('smoke teller'/'KTK', 'Min Yum').
card_number('smoke teller'/'KTK', '151').
card_flavor_text('smoke teller'/'KTK', '\"See your enemies from a thousand sides. Then you will find a thousand ways to kill them.\"').
card_multiverse_id('smoke teller'/'KTK', '386670').
card_watermark('smoke teller'/'KTK', 'Sultai').

card_in_set('snowhorn rider', 'KTK').
card_original_type('snowhorn rider'/'KTK', 'Creature — Human Warrior').
card_original_text('snowhorn rider'/'KTK', 'Trample\nMorph {2}{G}{U}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('snowhorn rider', 'KTK').
card_image_name('snowhorn rider'/'KTK', 'snowhorn rider').
card_uid('snowhorn rider'/'KTK', 'KTK:Snowhorn Rider:snowhorn rider').
card_rarity('snowhorn rider'/'KTK', 'Common').
card_artist('snowhorn rider'/'KTK', 'Tomasz Jedruszek').
card_number('snowhorn rider'/'KTK', '201').
card_flavor_text('snowhorn rider'/'KTK', 'Sure-footed, strong-willed, and ill-tempered—and so is the ram.').
card_multiverse_id('snowhorn rider'/'KTK', '386671').
card_watermark('snowhorn rider'/'KTK', 'Temur').

card_in_set('sorin, solemn visitor', 'KTK').
card_original_type('sorin, solemn visitor'/'KTK', 'Planeswalker — Sorin').
card_original_text('sorin, solemn visitor'/'KTK', '+1: Until your next turn, creatures you control get +1/+0 and gain lifelink.\n−2: Put a 2/2 black Vampire creature token with flying onto the battlefield.\n−6: You get an emblem with \"At the beginning of each opponent\'s upkeep, that player sacrifices a creature.\"').
card_first_print('sorin, solemn visitor', 'KTK').
card_image_name('sorin, solemn visitor'/'KTK', 'sorin, solemn visitor').
card_uid('sorin, solemn visitor'/'KTK', 'KTK:Sorin, Solemn Visitor:sorin, solemn visitor').
card_rarity('sorin, solemn visitor'/'KTK', 'Mythic Rare').
card_artist('sorin, solemn visitor'/'KTK', 'Cynthia Sheppard').
card_number('sorin, solemn visitor'/'KTK', '202').
card_multiverse_id('sorin, solemn visitor'/'KTK', '386672').

card_in_set('stubborn denial', 'KTK').
card_original_type('stubborn denial'/'KTK', 'Instant').
card_original_text('stubborn denial'/'KTK', 'Counter target noncreature spell unless its controller pays {1}.\nFerocious — If you control a creature with power 4 or greater, counter that spell instead.').
card_first_print('stubborn denial', 'KTK').
card_image_name('stubborn denial'/'KTK', 'stubborn denial').
card_uid('stubborn denial'/'KTK', 'KTK:Stubborn Denial:stubborn denial').
card_rarity('stubborn denial'/'KTK', 'Uncommon').
card_artist('stubborn denial'/'KTK', 'James Ryman').
card_number('stubborn denial'/'KTK', '56').
card_flavor_text('stubborn denial'/'KTK', 'The Temur have no patience for subtlety.').
card_multiverse_id('stubborn denial'/'KTK', '386673').
card_watermark('stubborn denial'/'KTK', 'Temur').

card_in_set('sultai ascendancy', 'KTK').
card_original_type('sultai ascendancy'/'KTK', 'Enchantment').
card_original_text('sultai ascendancy'/'KTK', 'At the beginning of your upkeep, look at the top two cards of your library. Put any number of them into your graveyard and the rest back on top of your library in any order.').
card_image_name('sultai ascendancy'/'KTK', 'sultai ascendancy').
card_uid('sultai ascendancy'/'KTK', 'KTK:Sultai Ascendancy:sultai ascendancy').
card_rarity('sultai ascendancy'/'KTK', 'Rare').
card_artist('sultai ascendancy'/'KTK', 'Karl Kopinski').
card_number('sultai ascendancy'/'KTK', '203').
card_multiverse_id('sultai ascendancy'/'KTK', '386674').
card_watermark('sultai ascendancy'/'KTK', 'Sultai').

card_in_set('sultai banner', 'KTK').
card_original_type('sultai banner'/'KTK', 'Artifact').
card_original_text('sultai banner'/'KTK', '{T}: Add {B}, {G}, or {U} to your mana pool.\n{B}{G}{U}, {T}, Sacrifice Sultai Banner: Draw a card.').
card_first_print('sultai banner', 'KTK').
card_image_name('sultai banner'/'KTK', 'sultai banner').
card_uid('sultai banner'/'KTK', 'KTK:Sultai Banner:sultai banner').
card_rarity('sultai banner'/'KTK', 'Common').
card_artist('sultai banner'/'KTK', 'Daniel Ljunggren').
card_number('sultai banner'/'KTK', '225').
card_flavor_text('sultai banner'/'KTK', 'Power to dominate, cruelty to rule.').
card_multiverse_id('sultai banner'/'KTK', '386675').
card_watermark('sultai banner'/'KTK', 'Sultai').

card_in_set('sultai charm', 'KTK').
card_original_type('sultai charm'/'KTK', 'Instant').
card_original_text('sultai charm'/'KTK', 'Choose one —\n• Destroy target monocolored creature.\n• Destroy target artifact or enchantment.\n• Draw two cards, then discard a card.').
card_image_name('sultai charm'/'KTK', 'sultai charm').
card_uid('sultai charm'/'KTK', 'KTK:Sultai Charm:sultai charm').
card_rarity('sultai charm'/'KTK', 'Uncommon').
card_artist('sultai charm'/'KTK', 'Mathias Kollros').
card_number('sultai charm'/'KTK', '204').
card_flavor_text('sultai charm'/'KTK', '\"Strike,\" the fumes hiss. \"Raise an empire with your ambition.\"').
card_multiverse_id('sultai charm'/'KTK', '386676').
card_watermark('sultai charm'/'KTK', 'Sultai').

card_in_set('sultai flayer', 'KTK').
card_original_type('sultai flayer'/'KTK', 'Creature — Naga Shaman').
card_original_text('sultai flayer'/'KTK', 'Whenever a creature you control with toughness 4 or greater dies, you gain 4 life.').
card_first_print('sultai flayer', 'KTK').
card_image_name('sultai flayer'/'KTK', 'sultai flayer').
card_uid('sultai flayer'/'KTK', 'KTK:Sultai Flayer:sultai flayer').
card_rarity('sultai flayer'/'KTK', 'Uncommon').
card_artist('sultai flayer'/'KTK', 'Izzy').
card_number('sultai flayer'/'KTK', '152').
card_flavor_text('sultai flayer'/'KTK', '\"You can have the body, necromancer. I just want the skin.\"').
card_multiverse_id('sultai flayer'/'KTK', '386677').
card_watermark('sultai flayer'/'KTK', 'Sultai').

card_in_set('sultai scavenger', 'KTK').
card_original_type('sultai scavenger'/'KTK', 'Creature — Bird Warrior').
card_original_text('sultai scavenger'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nFlying').
card_first_print('sultai scavenger', 'KTK').
card_image_name('sultai scavenger'/'KTK', 'sultai scavenger').
card_uid('sultai scavenger'/'KTK', 'KTK:Sultai Scavenger:sultai scavenger').
card_rarity('sultai scavenger'/'KTK', 'Common').
card_artist('sultai scavenger'/'KTK', 'Anthony Palumbo').
card_number('sultai scavenger'/'KTK', '91').
card_flavor_text('sultai scavenger'/'KTK', 'Since they guard armies of walking carrion, Sultai aven are never far from a meal.').
card_multiverse_id('sultai scavenger'/'KTK', '386678').
card_watermark('sultai scavenger'/'KTK', 'Sultai').

card_in_set('sultai soothsayer', 'KTK').
card_original_type('sultai soothsayer'/'KTK', 'Creature — Naga Shaman').
card_original_text('sultai soothsayer'/'KTK', 'When Sultai Soothsayer enters the battlefield, look at the top four cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_first_print('sultai soothsayer', 'KTK').
card_image_name('sultai soothsayer'/'KTK', 'sultai soothsayer').
card_uid('sultai soothsayer'/'KTK', 'KTK:Sultai Soothsayer:sultai soothsayer').
card_rarity('sultai soothsayer'/'KTK', 'Uncommon').
card_artist('sultai soothsayer'/'KTK', 'Cynthia Sheppard').
card_number('sultai soothsayer'/'KTK', '205').
card_flavor_text('sultai soothsayer'/'KTK', 'The naga of the Sultai Brood made deals with dark forces to keep their power.').
card_multiverse_id('sultai soothsayer'/'KTK', '386679').
card_watermark('sultai soothsayer'/'KTK', 'Sultai').

card_in_set('summit prowler', 'KTK').
card_original_type('summit prowler'/'KTK', 'Creature — Yeti').
card_original_text('summit prowler'/'KTK', '').
card_first_print('summit prowler', 'KTK').
card_image_name('summit prowler'/'KTK', 'summit prowler').
card_uid('summit prowler'/'KTK', 'KTK:Summit Prowler:summit prowler').
card_rarity('summit prowler'/'KTK', 'Common').
card_artist('summit prowler'/'KTK', 'Filip Burburan').
card_number('summit prowler'/'KTK', '121').
card_flavor_text('summit prowler'/'KTK', '\"Do you hunt the yetis of the high peaks, stripling? They are as fierce as the bear that fears no foe and as sly as the mink that creeps unseen. You will be as much prey as they.\"\n—Nitula, the Hunt Caller').
card_multiverse_id('summit prowler'/'KTK', '386680').

card_in_set('surrak dragonclaw', 'KTK').
card_original_type('surrak dragonclaw'/'KTK', 'Legendary Creature — Human Warrior').
card_original_text('surrak dragonclaw'/'KTK', 'Flash\nSurrak Dragonclaw can\'t be countered.\nCreature spells you control can\'t be countered.\nOther creatures you control have trample.').
card_image_name('surrak dragonclaw'/'KTK', 'surrak dragonclaw').
card_uid('surrak dragonclaw'/'KTK', 'KTK:Surrak Dragonclaw:surrak dragonclaw').
card_rarity('surrak dragonclaw'/'KTK', 'Mythic Rare').
card_artist('surrak dragonclaw'/'KTK', 'Jaime Jones').
card_number('surrak dragonclaw'/'KTK', '206').
card_flavor_text('surrak dragonclaw'/'KTK', 'Both his rank and his scars were earned in single combat against a cave bear.').
card_multiverse_id('surrak dragonclaw'/'KTK', '386681').
card_watermark('surrak dragonclaw'/'KTK', 'Temur').

card_in_set('suspension field', 'KTK').
card_original_type('suspension field'/'KTK', 'Enchantment').
card_original_text('suspension field'/'KTK', 'When Suspension Field enters the battlefield, you may exile target creature with toughness 3 or greater until Suspension Field leaves the battlefield. (That creature returns under its owner\'s control.)').
card_image_name('suspension field'/'KTK', 'suspension field').
card_uid('suspension field'/'KTK', 'KTK:Suspension Field:suspension field').
card_rarity('suspension field'/'KTK', 'Uncommon').
card_artist('suspension field'/'KTK', 'Seb McKinnon').
card_number('suspension field'/'KTK', '25').
card_multiverse_id('suspension field'/'KTK', '386682').

card_in_set('swamp', 'KTK').
card_original_type('swamp'/'KTK', 'Basic Land — Swamp').
card_original_text('swamp'/'KTK', 'B').
card_image_name('swamp'/'KTK', 'swamp1').
card_uid('swamp'/'KTK', 'KTK:Swamp:swamp1').
card_rarity('swamp'/'KTK', 'Basic Land').
card_artist('swamp'/'KTK', 'Noah Bradley').
card_number('swamp'/'KTK', '258').
card_multiverse_id('swamp'/'KTK', '386685').

card_in_set('swamp', 'KTK').
card_original_type('swamp'/'KTK', 'Basic Land — Swamp').
card_original_text('swamp'/'KTK', 'B').
card_image_name('swamp'/'KTK', 'swamp2').
card_uid('swamp'/'KTK', 'KTK:Swamp:swamp2').
card_rarity('swamp'/'KTK', 'Basic Land').
card_artist('swamp'/'KTK', 'Sam Burley').
card_number('swamp'/'KTK', '259').
card_multiverse_id('swamp'/'KTK', '386686').

card_in_set('swamp', 'KTK').
card_original_type('swamp'/'KTK', 'Basic Land — Swamp').
card_original_text('swamp'/'KTK', 'B').
card_image_name('swamp'/'KTK', 'swamp3').
card_uid('swamp'/'KTK', 'KTK:Swamp:swamp3').
card_rarity('swamp'/'KTK', 'Basic Land').
card_artist('swamp'/'KTK', 'Adam Paquette').
card_number('swamp'/'KTK', '260').
card_multiverse_id('swamp'/'KTK', '386683').

card_in_set('swamp', 'KTK').
card_original_type('swamp'/'KTK', 'Basic Land — Swamp').
card_original_text('swamp'/'KTK', 'B').
card_image_name('swamp'/'KTK', 'swamp4').
card_uid('swamp'/'KTK', 'KTK:Swamp:swamp4').
card_rarity('swamp'/'KTK', 'Basic Land').
card_artist('swamp'/'KTK', 'Adam Paquette').
card_number('swamp'/'KTK', '261').
card_multiverse_id('swamp'/'KTK', '386684').

card_in_set('swarm of bloodflies', 'KTK').
card_original_type('swarm of bloodflies'/'KTK', 'Creature — Insect').
card_original_text('swarm of bloodflies'/'KTK', 'Flying\nSwarm of Bloodflies enters the battlefield with two +1/+1 counters on it.\nWhenever another creature dies, put a +1/+1 counter on Swarm of Bloodflies.').
card_first_print('swarm of bloodflies', 'KTK').
card_image_name('swarm of bloodflies'/'KTK', 'swarm of bloodflies').
card_uid('swarm of bloodflies'/'KTK', 'KTK:Swarm of Bloodflies:swarm of bloodflies').
card_rarity('swarm of bloodflies'/'KTK', 'Uncommon').
card_artist('swarm of bloodflies'/'KTK', 'Marco Nelor').
card_number('swarm of bloodflies'/'KTK', '92').
card_flavor_text('swarm of bloodflies'/'KTK', 'In the Gudul delta, bloodfly bites are indistinguishable from spear wounds.').
card_multiverse_id('swarm of bloodflies'/'KTK', '386687').

card_in_set('swift kick', 'KTK').
card_original_type('swift kick'/'KTK', 'Instant').
card_original_text('swift kick'/'KTK', 'Target creature you control gets +1/+0 until end of turn. It fights target creature you don\'t control.').
card_first_print('swift kick', 'KTK').
card_image_name('swift kick'/'KTK', 'swift kick').
card_uid('swift kick'/'KTK', 'KTK:Swift Kick:swift kick').
card_rarity('swift kick'/'KTK', 'Common').
card_artist('swift kick'/'KTK', 'Mathias Kollros').
card_number('swift kick'/'KTK', '122').
card_flavor_text('swift kick'/'KTK', 'Shintan sensed the malice in his opponent, but he did not strike until the orc\'s muscles tensed in preparation to throw the first punch.').
card_multiverse_id('swift kick'/'KTK', '386688').

card_in_set('swiftwater cliffs', 'KTK').
card_original_type('swiftwater cliffs'/'KTK', 'Land').
card_original_text('swiftwater cliffs'/'KTK', 'Swiftwater Cliffs enters the battlefield tapped.\nWhen Swiftwater Cliffs enters the battlefield, you gain 1 life.\n{T}: Add {U} or {R} to your mana pool.').
card_first_print('swiftwater cliffs', 'KTK').
card_image_name('swiftwater cliffs'/'KTK', 'swiftwater cliffs').
card_uid('swiftwater cliffs'/'KTK', 'KTK:Swiftwater Cliffs:swiftwater cliffs').
card_rarity('swiftwater cliffs'/'KTK', 'Common').
card_artist('swiftwater cliffs'/'KTK', 'Eytan Zana').
card_number('swiftwater cliffs'/'KTK', '243').
card_multiverse_id('swiftwater cliffs'/'KTK', '386689').

card_in_set('taigam\'s scheming', 'KTK').
card_original_type('taigam\'s scheming'/'KTK', 'Sorcery').
card_original_text('taigam\'s scheming'/'KTK', 'Look at the top five cards of your library. Put any number of them into your graveyard and the rest back on top of your library in any order.').
card_first_print('taigam\'s scheming', 'KTK').
card_image_name('taigam\'s scheming'/'KTK', 'taigam\'s scheming').
card_uid('taigam\'s scheming'/'KTK', 'KTK:Taigam\'s Scheming:taigam\'s scheming').
card_rarity('taigam\'s scheming'/'KTK', 'Common').
card_artist('taigam\'s scheming'/'KTK', 'Svetlin Velinov').
card_number('taigam\'s scheming'/'KTK', '57').
card_flavor_text('taigam\'s scheming'/'KTK', '\"The Jeskai would have me bow in restraint. So I have found a people unafraid of true power.\"').
card_multiverse_id('taigam\'s scheming'/'KTK', '386690').
card_watermark('taigam\'s scheming'/'KTK', 'Sultai').

card_in_set('take up arms', 'KTK').
card_original_type('take up arms'/'KTK', 'Instant').
card_original_text('take up arms'/'KTK', 'Put three 1/1 white Warrior creature tokens onto the battlefield.').
card_first_print('take up arms', 'KTK').
card_image_name('take up arms'/'KTK', 'take up arms').
card_uid('take up arms'/'KTK', 'KTK:Take Up Arms:take up arms').
card_rarity('take up arms'/'KTK', 'Uncommon').
card_artist('take up arms'/'KTK', 'Craig J Spearing').
card_number('take up arms'/'KTK', '26').
card_flavor_text('take up arms'/'KTK', '\"Many scales make the skin of a dragon.\"\n—Abzan wisdom').
card_multiverse_id('take up arms'/'KTK', '386691').

card_in_set('temur ascendancy', 'KTK').
card_original_type('temur ascendancy'/'KTK', 'Enchantment').
card_original_text('temur ascendancy'/'KTK', 'Creatures you control have haste.\nWhenever a creature with power 4 or greater enters the battlefield under your control, you may draw a card.').
card_image_name('temur ascendancy'/'KTK', 'temur ascendancy').
card_uid('temur ascendancy'/'KTK', 'KTK:Temur Ascendancy:temur ascendancy').
card_rarity('temur ascendancy'/'KTK', 'Rare').
card_artist('temur ascendancy'/'KTK', 'Jaime Jones').
card_number('temur ascendancy'/'KTK', '207').
card_multiverse_id('temur ascendancy'/'KTK', '386692').
card_watermark('temur ascendancy'/'KTK', 'Temur').

card_in_set('temur banner', 'KTK').
card_original_type('temur banner'/'KTK', 'Artifact').
card_original_text('temur banner'/'KTK', '{T}: Add {G}, {U}, or {R} to your mana pool.\n{G}{U}{R}, {T}, Sacrifice Temur Banner: Draw a card.').
card_first_print('temur banner', 'KTK').
card_image_name('temur banner'/'KTK', 'temur banner').
card_uid('temur banner'/'KTK', 'KTK:Temur Banner:temur banner').
card_rarity('temur banner'/'KTK', 'Common').
card_artist('temur banner'/'KTK', 'Daniel Ljunggren').
card_number('temur banner'/'KTK', '226').
card_flavor_text('temur banner'/'KTK', 'Savagery to survive, courage to triumph.').
card_multiverse_id('temur banner'/'KTK', '386693').
card_watermark('temur banner'/'KTK', 'Temur').

card_in_set('temur charger', 'KTK').
card_original_type('temur charger'/'KTK', 'Creature — Horse').
card_original_text('temur charger'/'KTK', 'Morph—Reveal a green card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Temur Charger is turned face up, target creature gains trample until end of turn.').
card_first_print('temur charger', 'KTK').
card_image_name('temur charger'/'KTK', 'temur charger').
card_uid('temur charger'/'KTK', 'KTK:Temur Charger:temur charger').
card_rarity('temur charger'/'KTK', 'Uncommon').
card_artist('temur charger'/'KTK', 'Mark Zug').
card_number('temur charger'/'KTK', '153').
card_multiverse_id('temur charger'/'KTK', '386694').
card_watermark('temur charger'/'KTK', 'Temur').

card_in_set('temur charm', 'KTK').
card_original_type('temur charm'/'KTK', 'Instant').
card_original_text('temur charm'/'KTK', 'Choose one —\n• Target creature you control gets +1/+1 until end of turn. It fights target creature you don\'t control.\n• Counter target spell unless its controller pays {3}.\n• Creatures with power 3 or less can\'t block this turn.').
card_first_print('temur charm', 'KTK').
card_image_name('temur charm'/'KTK', 'temur charm').
card_uid('temur charm'/'KTK', 'KTK:Temur Charm:temur charm').
card_rarity('temur charm'/'KTK', 'Uncommon').
card_artist('temur charm'/'KTK', 'Mathias Kollros').
card_number('temur charm'/'KTK', '208').
card_multiverse_id('temur charm'/'KTK', '386695').
card_watermark('temur charm'/'KTK', 'Temur').

card_in_set('thornwood falls', 'KTK').
card_original_type('thornwood falls'/'KTK', 'Land').
card_original_text('thornwood falls'/'KTK', 'Thornwood Falls enters the battlefield tapped.\nWhen Thornwood Falls enters the battlefield, you gain 1 life.\n{T}: Add {G} or {U} to your mana pool.').
card_first_print('thornwood falls', 'KTK').
card_image_name('thornwood falls'/'KTK', 'thornwood falls').
card_uid('thornwood falls'/'KTK', 'KTK:Thornwood Falls:thornwood falls').
card_rarity('thornwood falls'/'KTK', 'Common').
card_artist('thornwood falls'/'KTK', 'Eytan Zana').
card_number('thornwood falls'/'KTK', '244').
card_multiverse_id('thornwood falls'/'KTK', '386696').

card_in_set('thousand winds', 'KTK').
card_original_type('thousand winds'/'KTK', 'Creature — Elemental').
card_original_text('thousand winds'/'KTK', 'Flying\nMorph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Thousand Winds is turned face up, return all other tapped creatures to their owners\' hands.').
card_image_name('thousand winds'/'KTK', 'thousand winds').
card_uid('thousand winds'/'KTK', 'KTK:Thousand Winds:thousand winds').
card_rarity('thousand winds'/'KTK', 'Rare').
card_artist('thousand winds'/'KTK', 'Raymond Swanland').
card_number('thousand winds'/'KTK', '58').
card_multiverse_id('thousand winds'/'KTK', '386697').
card_watermark('thousand winds'/'KTK', 'Jeskai').

card_in_set('throttle', 'KTK').
card_original_type('throttle'/'KTK', 'Instant').
card_original_text('throttle'/'KTK', 'Target creature gets -4/-4 until end of turn.').
card_first_print('throttle', 'KTK').
card_image_name('throttle'/'KTK', 'throttle').
card_uid('throttle'/'KTK', 'KTK:Throttle:throttle').
card_rarity('throttle'/'KTK', 'Common').
card_artist('throttle'/'KTK', 'Wayne Reynolds').
card_number('throttle'/'KTK', '93').
card_flavor_text('throttle'/'KTK', '\"The best servants are made from those who died without a scratch.\"\n—Sidisi, khan of the Sultai').
card_multiverse_id('throttle'/'KTK', '386698').

card_in_set('timely hordemate', 'KTK').
card_original_type('timely hordemate'/'KTK', 'Creature — Human Warrior').
card_original_text('timely hordemate'/'KTK', 'Raid — When Timely Hordemate enters the battlefield, if you attacked with a creature this turn, return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.').
card_first_print('timely hordemate', 'KTK').
card_image_name('timely hordemate'/'KTK', 'timely hordemate').
card_uid('timely hordemate'/'KTK', 'KTK:Timely Hordemate:timely hordemate').
card_rarity('timely hordemate'/'KTK', 'Uncommon').
card_artist('timely hordemate'/'KTK', 'Steve Prescott').
card_number('timely hordemate'/'KTK', '27').
card_multiverse_id('timely hordemate'/'KTK', '386699').
card_watermark('timely hordemate'/'KTK', 'Mardu').

card_in_set('tomb of the spirit dragon', 'KTK').
card_original_type('tomb of the spirit dragon'/'KTK', 'Land').
card_original_text('tomb of the spirit dragon'/'KTK', '{T}: Add {1} to your mana pool.\n{2}, {T}: You gain 1 life for each colorless creature you control.').
card_first_print('tomb of the spirit dragon', 'KTK').
card_image_name('tomb of the spirit dragon'/'KTK', 'tomb of the spirit dragon').
card_uid('tomb of the spirit dragon'/'KTK', 'KTK:Tomb of the Spirit Dragon:tomb of the spirit dragon').
card_rarity('tomb of the spirit dragon'/'KTK', 'Uncommon').
card_artist('tomb of the spirit dragon'/'KTK', 'Sam Burley').
card_number('tomb of the spirit dragon'/'KTK', '245').
card_flavor_text('tomb of the spirit dragon'/'KTK', '\"The voice calls me here, yet I see only bones. Is this more dragon trickery?\"\n—Sarkhan Vol').
card_multiverse_id('tomb of the spirit dragon'/'KTK', '386700').

card_in_set('tormenting voice', 'KTK').
card_original_type('tormenting voice'/'KTK', 'Sorcery').
card_original_text('tormenting voice'/'KTK', 'As an additional cost to cast Tormenting Voice, discard a card.\nDraw two cards.').
card_first_print('tormenting voice', 'KTK').
card_image_name('tormenting voice'/'KTK', 'tormenting voice').
card_uid('tormenting voice'/'KTK', 'KTK:Tormenting Voice:tormenting voice').
card_rarity('tormenting voice'/'KTK', 'Common').
card_artist('tormenting voice'/'KTK', 'Volkan Baga').
card_number('tormenting voice'/'KTK', '123').
card_flavor_text('tormenting voice'/'KTK', '\"Unwelcome thoughts crowd my mind. Are they my own madness, or the whispers of another?\"\n—Sarkhan Vol').
card_multiverse_id('tormenting voice'/'KTK', '386701').

card_in_set('trail of mystery', 'KTK').
card_original_type('trail of mystery'/'KTK', 'Enchantment').
card_original_text('trail of mystery'/'KTK', 'Whenever a face-down creature enters the battlefield under your control, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.\nWhenever a permanent you control is turned face up, if it\'s a creature, it gets +2/+2 until end of turn.').
card_image_name('trail of mystery'/'KTK', 'trail of mystery').
card_uid('trail of mystery'/'KTK', 'KTK:Trail of Mystery:trail of mystery').
card_rarity('trail of mystery'/'KTK', 'Rare').
card_artist('trail of mystery'/'KTK', 'Raymond Swanland').
card_number('trail of mystery'/'KTK', '154').
card_multiverse_id('trail of mystery'/'KTK', '386702').

card_in_set('tranquil cove', 'KTK').
card_original_type('tranquil cove'/'KTK', 'Land').
card_original_text('tranquil cove'/'KTK', 'Tranquil Cove enters the battlefield tapped.\nWhen Tranquil Cove enters the battlefield, you gain 1 life.\n{T}: Add {W} or {U} to your mana pool.').
card_first_print('tranquil cove', 'KTK').
card_image_name('tranquil cove'/'KTK', 'tranquil cove').
card_uid('tranquil cove'/'KTK', 'KTK:Tranquil Cove:tranquil cove').
card_rarity('tranquil cove'/'KTK', 'Common').
card_artist('tranquil cove'/'KTK', 'John Avon').
card_number('tranquil cove'/'KTK', '246').
card_multiverse_id('tranquil cove'/'KTK', '386703').

card_in_set('trap essence', 'KTK').
card_original_type('trap essence'/'KTK', 'Instant').
card_original_text('trap essence'/'KTK', 'Counter target creature spell. Put two +1/+1 counters on up to one target creature.').
card_image_name('trap essence'/'KTK', 'trap essence').
card_uid('trap essence'/'KTK', 'KTK:Trap Essence:trap essence').
card_rarity('trap essence'/'KTK', 'Rare').
card_artist('trap essence'/'KTK', 'Raymond Swanland').
card_number('trap essence'/'KTK', '209').
card_flavor_text('trap essence'/'KTK', '\"Meat sustains the body. The spirit requires different sustenance.\"\n—Arel the Whisperer').
card_multiverse_id('trap essence'/'KTK', '386704').
card_watermark('trap essence'/'KTK', 'Temur').

card_in_set('treasure cruise', 'KTK').
card_original_type('treasure cruise'/'KTK', 'Sorcery').
card_original_text('treasure cruise'/'KTK', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nDraw three cards.').
card_first_print('treasure cruise', 'KTK').
card_image_name('treasure cruise'/'KTK', 'treasure cruise').
card_uid('treasure cruise'/'KTK', 'KTK:Treasure Cruise:treasure cruise').
card_rarity('treasure cruise'/'KTK', 'Common').
card_artist('treasure cruise'/'KTK', 'Cynthia Sheppard').
card_number('treasure cruise'/'KTK', '59').
card_flavor_text('treasure cruise'/'KTK', 'Countless delights drift on the surface while dark schemes run below.').
card_multiverse_id('treasure cruise'/'KTK', '386705').
card_watermark('treasure cruise'/'KTK', 'Sultai').

card_in_set('trumpet blast', 'KTK').
card_original_type('trumpet blast'/'KTK', 'Instant').
card_original_text('trumpet blast'/'KTK', 'Attacking creatures get +2/+0 until end of turn.').
card_image_name('trumpet blast'/'KTK', 'trumpet blast').
card_uid('trumpet blast'/'KTK', 'KTK:Trumpet Blast:trumpet blast').
card_rarity('trumpet blast'/'KTK', 'Common').
card_artist('trumpet blast'/'KTK', 'Steve Prescott').
card_number('trumpet blast'/'KTK', '124').
card_flavor_text('trumpet blast'/'KTK', '\"Do you hear that, Sarkhan? The glory of the horde! I made a legend from what you abandoned.\"\n—Zurgo, khan of the Mardu').
card_multiverse_id('trumpet blast'/'KTK', '386706').

card_in_set('tusked colossodon', 'KTK').
card_original_type('tusked colossodon'/'KTK', 'Creature — Beast').
card_original_text('tusked colossodon'/'KTK', '').
card_first_print('tusked colossodon', 'KTK').
card_image_name('tusked colossodon'/'KTK', 'tusked colossodon').
card_uid('tusked colossodon'/'KTK', 'KTK:Tusked Colossodon:tusked colossodon').
card_rarity('tusked colossodon'/'KTK', 'Common').
card_artist('tusked colossodon'/'KTK', 'Yeong-Hao Han').
card_number('tusked colossodon'/'KTK', '155').
card_flavor_text('tusked colossodon'/'KTK', 'A band of Temur hunters, fleeing the Mardu, dug a hideout beneath such a creature as it slept. The horde found them and attacked. For three days the Temur held them at bay, and all the while the great beast slumbered.').
card_multiverse_id('tusked colossodon'/'KTK', '386707').

card_in_set('tuskguard captain', 'KTK').
card_original_type('tuskguard captain'/'KTK', 'Creature — Human Warrior').
card_original_text('tuskguard captain'/'KTK', 'Outlast {G} ({G}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has trample.').
card_first_print('tuskguard captain', 'KTK').
card_image_name('tuskguard captain'/'KTK', 'tuskguard captain').
card_uid('tuskguard captain'/'KTK', 'KTK:Tuskguard Captain:tuskguard captain').
card_rarity('tuskguard captain'/'KTK', 'Uncommon').
card_artist('tuskguard captain'/'KTK', 'Aaron Miller').
card_number('tuskguard captain'/'KTK', '156').
card_flavor_text('tuskguard captain'/'KTK', 'One quiet word sets off the stampede.').
card_multiverse_id('tuskguard captain'/'KTK', '386708').
card_watermark('tuskguard captain'/'KTK', 'Abzan').

card_in_set('ugin\'s nexus', 'KTK').
card_original_type('ugin\'s nexus'/'KTK', 'Legendary Artifact').
card_original_text('ugin\'s nexus'/'KTK', 'If a player would begin an extra turn, that player skips that turn instead.\nIf Ugin\'s Nexus would be put into a graveyard from the battlefield, instead exile it and take an extra turn after this one.').
card_first_print('ugin\'s nexus', 'KTK').
card_image_name('ugin\'s nexus'/'KTK', 'ugin\'s nexus').
card_uid('ugin\'s nexus'/'KTK', 'KTK:Ugin\'s Nexus:ugin\'s nexus').
card_rarity('ugin\'s nexus'/'KTK', 'Mythic Rare').
card_artist('ugin\'s nexus'/'KTK', 'Sam Burley').
card_number('ugin\'s nexus'/'KTK', '227').
card_flavor_text('ugin\'s nexus'/'KTK', 'All at once Sarkhan\'s mind fell silent.').
card_multiverse_id('ugin\'s nexus'/'KTK', '386709').

card_in_set('unyielding krumar', 'KTK').
card_original_type('unyielding krumar'/'KTK', 'Creature — Orc Warrior').
card_original_text('unyielding krumar'/'KTK', '{1}{W}: Unyielding Krumar gains first strike until end of turn.').
card_first_print('unyielding krumar', 'KTK').
card_image_name('unyielding krumar'/'KTK', 'unyielding krumar').
card_uid('unyielding krumar'/'KTK', 'KTK:Unyielding Krumar:unyielding krumar').
card_rarity('unyielding krumar'/'KTK', 'Common').
card_artist('unyielding krumar'/'KTK', 'Viktor Titov').
card_number('unyielding krumar'/'KTK', '94').
card_flavor_text('unyielding krumar'/'KTK', '\"The man whom I call father killed the orc who sired me, offering his world and his blade in return.\"').
card_multiverse_id('unyielding krumar'/'KTK', '386710').
card_watermark('unyielding krumar'/'KTK', 'Abzan').

card_in_set('utter end', 'KTK').
card_original_type('utter end'/'KTK', 'Instant').
card_original_text('utter end'/'KTK', 'Exile target nonland permanent.').
card_image_name('utter end'/'KTK', 'utter end').
card_uid('utter end'/'KTK', 'KTK:Utter End:utter end').
card_rarity('utter end'/'KTK', 'Rare').
card_artist('utter end'/'KTK', 'Mark Winters').
card_number('utter end'/'KTK', '210').
card_flavor_text('utter end'/'KTK', '\"I came seeking a challenge. All I found was you.\"\n—Zurgo, khan of the Mardu').
card_multiverse_id('utter end'/'KTK', '386711').

card_in_set('valley dasher', 'KTK').
card_original_type('valley dasher'/'KTK', 'Creature — Human Berserker').
card_original_text('valley dasher'/'KTK', 'Haste\nValley Dasher attacks each turn if able.').
card_first_print('valley dasher', 'KTK').
card_image_name('valley dasher'/'KTK', 'valley dasher').
card_uid('valley dasher'/'KTK', 'KTK:Valley Dasher:valley dasher').
card_rarity('valley dasher'/'KTK', 'Common').
card_artist('valley dasher'/'KTK', 'Matt Stewart').
card_number('valley dasher'/'KTK', '125').
card_flavor_text('valley dasher'/'KTK', 'Mardu riders\' greatest fear is that a battle might end before their weapons draw blood.').
card_multiverse_id('valley dasher'/'KTK', '386712').
card_watermark('valley dasher'/'KTK', 'Mardu').

card_in_set('venerable lammasu', 'KTK').
card_original_type('venerable lammasu'/'KTK', 'Creature — Lammasu').
card_original_text('venerable lammasu'/'KTK', 'Flying').
card_first_print('venerable lammasu', 'KTK').
card_image_name('venerable lammasu'/'KTK', 'venerable lammasu').
card_uid('venerable lammasu'/'KTK', 'KTK:Venerable Lammasu:venerable lammasu').
card_rarity('venerable lammasu'/'KTK', 'Uncommon').
card_artist('venerable lammasu'/'KTK', 'YW Tang').
card_number('venerable lammasu'/'KTK', '28').
card_flavor_text('venerable lammasu'/'KTK', 'Lammasu are the enigmatic travelers of Tarkir, soaring high above all lands in all seasons. None know their true purpose, but they often arrive on the eve of great conflicts or turning points in history.').
card_multiverse_id('venerable lammasu'/'KTK', '386713').

card_in_set('villainous wealth', 'KTK').
card_original_type('villainous wealth'/'KTK', 'Sorcery').
card_original_text('villainous wealth'/'KTK', 'Target opponent exiles the top X cards of his or her library. You may cast any number of nonland cards with converted mana cost X or less from among them without paying their mana costs.').
card_image_name('villainous wealth'/'KTK', 'villainous wealth').
card_uid('villainous wealth'/'KTK', 'KTK:Villainous Wealth:villainous wealth').
card_rarity('villainous wealth'/'KTK', 'Rare').
card_artist('villainous wealth'/'KTK', 'Erica Yang').
card_number('villainous wealth'/'KTK', '211').
card_flavor_text('villainous wealth'/'KTK', 'Gold buys death. Death earns gold.').
card_multiverse_id('villainous wealth'/'KTK', '386714').
card_watermark('villainous wealth'/'KTK', 'Sultai').

card_in_set('war behemoth', 'KTK').
card_original_type('war behemoth'/'KTK', 'Creature — Beast').
card_original_text('war behemoth'/'KTK', 'Morph {4}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('war behemoth', 'KTK').
card_image_name('war behemoth'/'KTK', 'war behemoth').
card_uid('war behemoth'/'KTK', 'KTK:War Behemoth:war behemoth').
card_rarity('war behemoth'/'KTK', 'Common').
card_artist('war behemoth'/'KTK', 'Zoltan Boros').
card_number('war behemoth'/'KTK', '29').
card_flavor_text('war behemoth'/'KTK', '\"The Houses always hope for peace, but we always pack for war.\"\n—Gvar Barzeel, krumar commander').
card_multiverse_id('war behemoth'/'KTK', '386715').
card_watermark('war behemoth'/'KTK', 'Abzan').

card_in_set('war-name aspirant', 'KTK').
card_original_type('war-name aspirant'/'KTK', 'Creature — Human Warrior').
card_original_text('war-name aspirant'/'KTK', 'Raid — War-Name Aspirant enters the battlefield with a +1/+1 counter on it if you attacked with a creature this turn.\nWar-Name Aspirant can\'t be blocked by creatures with power 1 or less.').
card_first_print('war-name aspirant', 'KTK').
card_image_name('war-name aspirant'/'KTK', 'war-name aspirant').
card_uid('war-name aspirant'/'KTK', 'KTK:War-Name Aspirant:war-name aspirant').
card_rarity('war-name aspirant'/'KTK', 'Uncommon').
card_artist('war-name aspirant'/'KTK', 'David Gaillet').
card_number('war-name aspirant'/'KTK', '126').
card_flavor_text('war-name aspirant'/'KTK', 'No battle means more to a Mardu warrior than the one that earns her war name.').
card_multiverse_id('war-name aspirant'/'KTK', '386717').
card_watermark('war-name aspirant'/'KTK', 'Mardu').

card_in_set('warden of the eye', 'KTK').
card_original_type('warden of the eye'/'KTK', 'Creature — Djinn Wizard').
card_original_text('warden of the eye'/'KTK', 'When Warden of the Eye enters the battlefield, return target noncreature, nonland card from your graveyard to your hand.').
card_first_print('warden of the eye', 'KTK').
card_image_name('warden of the eye'/'KTK', 'warden of the eye').
card_uid('warden of the eye'/'KTK', 'KTK:Warden of the Eye:warden of the eye').
card_rarity('warden of the eye'/'KTK', 'Uncommon').
card_artist('warden of the eye'/'KTK', 'Howard Lyon').
card_number('warden of the eye'/'KTK', '212').
card_flavor_text('warden of the eye'/'KTK', 'The wardens guard the sacred documents of Tarkir\'s history, though they are forbidden to read the words.').
card_multiverse_id('warden of the eye'/'KTK', '386716').
card_watermark('warden of the eye'/'KTK', 'Jeskai').

card_in_set('watcher of the roost', 'KTK').
card_original_type('watcher of the roost'/'KTK', 'Creature — Bird Soldier').
card_original_text('watcher of the roost'/'KTK', 'Flying\nMorph—Reveal a white card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Watcher of the Roost is turned face up, you gain 2 life.').
card_first_print('watcher of the roost', 'KTK').
card_image_name('watcher of the roost'/'KTK', 'watcher of the roost').
card_uid('watcher of the roost'/'KTK', 'KTK:Watcher of the Roost:watcher of the roost').
card_rarity('watcher of the roost'/'KTK', 'Uncommon').
card_artist('watcher of the roost'/'KTK', 'Jack Wang').
card_number('watcher of the roost'/'KTK', '30').
card_multiverse_id('watcher of the roost'/'KTK', '386718').
card_watermark('watcher of the roost'/'KTK', 'Abzan').

card_in_set('waterwhirl', 'KTK').
card_original_type('waterwhirl'/'KTK', 'Instant').
card_original_text('waterwhirl'/'KTK', 'Return up to two target creatures to their owners\' hands.').
card_first_print('waterwhirl', 'KTK').
card_image_name('waterwhirl'/'KTK', 'waterwhirl').
card_uid('waterwhirl'/'KTK', 'KTK:Waterwhirl:waterwhirl').
card_rarity('waterwhirl'/'KTK', 'Uncommon').
card_artist('waterwhirl'/'KTK', 'Clint Cearley').
card_number('waterwhirl'/'KTK', '60').
card_flavor_text('waterwhirl'/'KTK', '\"Be as water: untamed and unheld, yet inexorably flowing toward a greater goal.\"\n—Shensu, Riverwheel mystic').
card_multiverse_id('waterwhirl'/'KTK', '386719').

card_in_set('weave fate', 'KTK').
card_original_type('weave fate'/'KTK', 'Instant').
card_original_text('weave fate'/'KTK', 'Draw two cards.').
card_first_print('weave fate', 'KTK').
card_image_name('weave fate'/'KTK', 'weave fate').
card_uid('weave fate'/'KTK', 'KTK:Weave Fate:weave fate').
card_rarity('weave fate'/'KTK', 'Common').
card_artist('weave fate'/'KTK', 'Zack Stella').
card_number('weave fate'/'KTK', '61').
card_flavor_text('weave fate'/'KTK', 'Temur shamans speak of three destinies: the now, the echo of the past, and the unwritten. They find flickering paths among tangled possibilities.').
card_multiverse_id('weave fate'/'KTK', '386720').

card_in_set('wetland sambar', 'KTK').
card_original_type('wetland sambar'/'KTK', 'Creature — Elk').
card_original_text('wetland sambar'/'KTK', '').
card_first_print('wetland sambar', 'KTK').
card_image_name('wetland sambar'/'KTK', 'wetland sambar').
card_uid('wetland sambar'/'KTK', 'KTK:Wetland Sambar:wetland sambar').
card_rarity('wetland sambar'/'KTK', 'Common').
card_artist('wetland sambar'/'KTK', 'James Zapata').
card_number('wetland sambar'/'KTK', '62').
card_flavor_text('wetland sambar'/'KTK', 'As a test of calm and compassion, a Jeskai monk softly approaches a grazing sambar and offers it a lotus from his or her hand. If the creature eats, the student ascends to the next level of training.').
card_multiverse_id('wetland sambar'/'KTK', '386721').

card_in_set('whirlwind adept', 'KTK').
card_original_type('whirlwind adept'/'KTK', 'Creature — Djinn Monk').
card_original_text('whirlwind adept'/'KTK', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('whirlwind adept', 'KTK').
card_image_name('whirlwind adept'/'KTK', 'whirlwind adept').
card_uid('whirlwind adept'/'KTK', 'KTK:Whirlwind Adept:whirlwind adept').
card_rarity('whirlwind adept'/'KTK', 'Common').
card_artist('whirlwind adept'/'KTK', 'Steve Argyle').
card_number('whirlwind adept'/'KTK', '63').
card_multiverse_id('whirlwind adept'/'KTK', '386722').
card_watermark('whirlwind adept'/'KTK', 'Jeskai').

card_in_set('wind-scarred crag', 'KTK').
card_original_type('wind-scarred crag'/'KTK', 'Land').
card_original_text('wind-scarred crag'/'KTK', 'Wind-Scarred Crag enters the battlefield tapped.\nWhen Wind-Scarred Crag enters the battlefield, you gain 1 life.\n{T}: Add {R} or {W} to your mana pool.').
card_first_print('wind-scarred crag', 'KTK').
card_image_name('wind-scarred crag'/'KTK', 'wind-scarred crag').
card_uid('wind-scarred crag'/'KTK', 'KTK:Wind-Scarred Crag:wind-scarred crag').
card_rarity('wind-scarred crag'/'KTK', 'Common').
card_artist('wind-scarred crag'/'KTK', 'Eytan Zana').
card_number('wind-scarred crag'/'KTK', '247').
card_multiverse_id('wind-scarred crag'/'KTK', '386723').

card_in_set('windstorm', 'KTK').
card_original_type('windstorm'/'KTK', 'Instant').
card_original_text('windstorm'/'KTK', 'Windstorm deals X damage to each creature with flying.').
card_image_name('windstorm'/'KTK', 'windstorm').
card_uid('windstorm'/'KTK', 'KTK:Windstorm:windstorm').
card_rarity('windstorm'/'KTK', 'Uncommon').
card_artist('windstorm'/'KTK', 'Jonas De Ro').
card_number('windstorm'/'KTK', '157').
card_flavor_text('windstorm'/'KTK', '\"When the last dragon fell, its spirit escaped as a roar into the wind.\"\n—Temur tale').
card_multiverse_id('windstorm'/'KTK', '386724').

card_in_set('windswept heath', 'KTK').
card_original_type('windswept heath'/'KTK', 'Land').
card_original_text('windswept heath'/'KTK', '{T}, Pay 1 life, Sacrifice Windswept Heath: Search your library for a Forest or Plains card and put it onto the battlefield. Then shuffle your library.').
card_image_name('windswept heath'/'KTK', 'windswept heath').
card_uid('windswept heath'/'KTK', 'KTK:Windswept Heath:windswept heath').
card_rarity('windswept heath'/'KTK', 'Rare').
card_artist('windswept heath'/'KTK', 'Yeong-Hao Han').
card_number('windswept heath'/'KTK', '248').
card_flavor_text('windswept heath'/'KTK', 'Where dragons once roared, their bones now keen.').
card_multiverse_id('windswept heath'/'KTK', '386725').

card_in_set('wingmate roc', 'KTK').
card_original_type('wingmate roc'/'KTK', 'Creature — Bird').
card_original_text('wingmate roc'/'KTK', 'Flying\nRaid — When Wingmate Roc enters the battlefield, if you attacked with a creature this turn, put a 3/4 white Bird creature token with flying onto the battlefield.\nWhenever Wingmate Roc attacks, you gain 1 life for each attacking creature.').
card_first_print('wingmate roc', 'KTK').
card_image_name('wingmate roc'/'KTK', 'wingmate roc').
card_uid('wingmate roc'/'KTK', 'KTK:Wingmate Roc:wingmate roc').
card_rarity('wingmate roc'/'KTK', 'Mythic Rare').
card_artist('wingmate roc'/'KTK', 'Mark Zug').
card_number('wingmate roc'/'KTK', '31').
card_multiverse_id('wingmate roc'/'KTK', '386726').
card_watermark('wingmate roc'/'KTK', 'Mardu').

card_in_set('winterflame', 'KTK').
card_original_type('winterflame'/'KTK', 'Instant').
card_original_text('winterflame'/'KTK', 'Choose one or both —\n• Tap target creature.\n• Winterflame deals 2 damage to target creature.').
card_first_print('winterflame', 'KTK').
card_image_name('winterflame'/'KTK', 'winterflame').
card_uid('winterflame'/'KTK', 'KTK:Winterflame:winterflame').
card_rarity('winterflame'/'KTK', 'Uncommon').
card_artist('winterflame'/'KTK', 'Richard Wright').
card_number('winterflame'/'KTK', '213').
card_flavor_text('winterflame'/'KTK', '\"The mountains scream with the dragons\' throats.\"\n—Chianul, Who Whispers Twice').
card_multiverse_id('winterflame'/'KTK', '386727').

card_in_set('witness of the ages', 'KTK').
card_original_type('witness of the ages'/'KTK', 'Artifact Creature — Golem').
card_original_text('witness of the ages'/'KTK', 'Morph {5} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('witness of the ages', 'KTK').
card_image_name('witness of the ages'/'KTK', 'witness of the ages').
card_uid('witness of the ages'/'KTK', 'KTK:Witness of the Ages:witness of the ages').
card_rarity('witness of the ages'/'KTK', 'Uncommon').
card_artist('witness of the ages'/'KTK', 'Izzy').
card_number('witness of the ages'/'KTK', '228').
card_flavor_text('witness of the ages'/'KTK', 'It strode through the clash of dragons, the fall of Ugin, and the rise of the khans.').
card_multiverse_id('witness of the ages'/'KTK', '386728').

card_in_set('wooded foothills', 'KTK').
card_original_type('wooded foothills'/'KTK', 'Land').
card_original_text('wooded foothills'/'KTK', '{T}, Pay 1 life, Sacrifice Wooded Foothills: Search your library for a Mountain or Forest card and put it onto the battlefield. Then shuffle your library.').
card_image_name('wooded foothills'/'KTK', 'wooded foothills').
card_uid('wooded foothills'/'KTK', 'KTK:Wooded Foothills:wooded foothills').
card_rarity('wooded foothills'/'KTK', 'Rare').
card_artist('wooded foothills'/'KTK', 'Jonas De Ro').
card_number('wooded foothills'/'KTK', '249').
card_flavor_text('wooded foothills'/'KTK', 'Where dragons\' breath once burned, their bones now freeze.').
card_multiverse_id('wooded foothills'/'KTK', '386729').

card_in_set('woolly loxodon', 'KTK').
card_original_type('woolly loxodon'/'KTK', 'Creature — Elephant Warrior').
card_original_text('woolly loxodon'/'KTK', 'Morph {5}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('woolly loxodon', 'KTK').
card_image_name('woolly loxodon'/'KTK', 'woolly loxodon').
card_uid('woolly loxodon'/'KTK', 'KTK:Woolly Loxodon:woolly loxodon').
card_rarity('woolly loxodon'/'KTK', 'Common').
card_artist('woolly loxodon'/'KTK', 'Karla Ortiz').
card_number('woolly loxodon'/'KTK', '158').
card_flavor_text('woolly loxodon'/'KTK', 'Even among the hardiest warriors of the Temur, loxodons are respected for their adaptation to the mountain snows.').
card_multiverse_id('woolly loxodon'/'KTK', '386730').
card_watermark('woolly loxodon'/'KTK', 'Temur').

card_in_set('zurgo helmsmasher', 'KTK').
card_original_type('zurgo helmsmasher'/'KTK', 'Legendary Creature — Orc Warrior').
card_original_text('zurgo helmsmasher'/'KTK', 'Haste\nZurgo Helmsmasher attacks each combat if able.\nZurgo Helmsmasher has indestructible as long as it\'s your turn.\nWhenever a creature dealt damage by Zurgo Helmsmasher this turn dies, put a +1/+1 counter on Zurgo Helmsmasher.').
card_image_name('zurgo helmsmasher'/'KTK', 'zurgo helmsmasher').
card_uid('zurgo helmsmasher'/'KTK', 'KTK:Zurgo Helmsmasher:zurgo helmsmasher').
card_rarity('zurgo helmsmasher'/'KTK', 'Mythic Rare').
card_artist('zurgo helmsmasher'/'KTK', 'Aleksi Briclot').
card_number('zurgo helmsmasher'/'KTK', '214').
card_multiverse_id('zurgo helmsmasher'/'KTK', '386731').
card_watermark('zurgo helmsmasher'/'KTK', 'Mardu').
