% Duel Decks Anthology, Jace vs. Chandra

set('DD3_JVC').
set_name('DD3_JVC', 'Duel Decks Anthology, Jace vs. Chandra').
set_release_date('DD3_JVC', '2014-12-05').
set_border('DD3_JVC', 'black').
set_type('DD3_JVC', 'duel deck').

card_in_set('æthersnipe', 'DD3_JVC').
card_original_type('æthersnipe'/'DD3_JVC', 'Creature — Elemental').
card_original_text('æthersnipe'/'DD3_JVC', 'When Æthersnipe enters the battlefield, return target nonland permanent to its owner\'s hand.\nEvoke {1}{U}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('æthersnipe'/'DD3_JVC', 'aethersnipe').
card_uid('æthersnipe'/'DD3_JVC', 'DD3_JVC:Æthersnipe:aethersnipe').
card_rarity('æthersnipe'/'DD3_JVC', 'Common').
card_artist('æthersnipe'/'DD3_JVC', 'Zoltan Boros & Gabor Szikszai').
card_number('æthersnipe'/'DD3_JVC', '17').
card_multiverse_id('æthersnipe'/'DD3_JVC', '393816').

card_in_set('air elemental', 'DD3_JVC').
card_original_type('air elemental'/'DD3_JVC', 'Creature — Elemental').
card_original_text('air elemental'/'DD3_JVC', 'Flying').
card_image_name('air elemental'/'DD3_JVC', 'air elemental').
card_uid('air elemental'/'DD3_JVC', 'DD3_JVC:Air Elemental:air elemental').
card_rarity('air elemental'/'DD3_JVC', 'Uncommon').
card_artist('air elemental'/'DD3_JVC', 'Kev Walker').
card_number('air elemental'/'DD3_JVC', '13').
card_flavor_text('air elemental'/'DD3_JVC', 'Pray that it doesn\'t seek the safety of your lungs.').
card_multiverse_id('air elemental'/'DD3_JVC', '393817').

card_in_set('ancestral vision', 'DD3_JVC').
card_original_type('ancestral vision'/'DD3_JVC', 'Sorcery').
card_original_text('ancestral vision'/'DD3_JVC', 'Suspend 4—{U} (Rather than cast this card from your hand, pay {U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\nTarget player draws three cards.').
card_image_name('ancestral vision'/'DD3_JVC', 'ancestral vision').
card_uid('ancestral vision'/'DD3_JVC', 'DD3_JVC:Ancestral Vision:ancestral vision').
card_rarity('ancestral vision'/'DD3_JVC', 'Rare').
card_artist('ancestral vision'/'DD3_JVC', 'Mark Poole').
card_number('ancestral vision'/'DD3_JVC', '21').
card_multiverse_id('ancestral vision'/'DD3_JVC', '393818').

card_in_set('bottle gnomes', 'DD3_JVC').
card_original_type('bottle gnomes'/'DD3_JVC', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'DD3_JVC', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_image_name('bottle gnomes'/'DD3_JVC', 'bottle gnomes').
card_uid('bottle gnomes'/'DD3_JVC', 'DD3_JVC:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'DD3_JVC', 'Uncommon').
card_artist('bottle gnomes'/'DD3_JVC', 'Ben Thompson').
card_number('bottle gnomes'/'DD3_JVC', '7').
card_flavor_text('bottle gnomes'/'DD3_JVC', 'Reinforcements . . . or refreshments?').
card_multiverse_id('bottle gnomes'/'DD3_JVC', '393819').

card_in_set('brine elemental', 'DD3_JVC').
card_original_type('brine elemental'/'DD3_JVC', 'Creature — Elemental').
card_original_text('brine elemental'/'DD3_JVC', 'Morph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Brine Elemental is turned face up, each opponent skips his or her next untap step.').
card_image_name('brine elemental'/'DD3_JVC', 'brine elemental').
card_uid('brine elemental'/'DD3_JVC', 'DD3_JVC:Brine Elemental:brine elemental').
card_rarity('brine elemental'/'DD3_JVC', 'Uncommon').
card_artist('brine elemental'/'DD3_JVC', 'Stephen Tappin').
card_number('brine elemental'/'DD3_JVC', '18').
card_flavor_text('brine elemental'/'DD3_JVC', 'Water calls to water, and the world is left exhausted and withered in its wake.').
card_multiverse_id('brine elemental'/'DD3_JVC', '393820').

card_in_set('chandra nalaar', 'DD3_JVC').
card_original_type('chandra nalaar'/'DD3_JVC', 'Planeswalker — Chandra').
card_original_text('chandra nalaar'/'DD3_JVC', '+1: Chandra Nalaar deals 1 damage to target player.\n–X: Chandra Nalaar deals X damage to target creature.\n–8: Chandra Nalaar deals 10 damage to target player and each creature he or she controls.').
card_image_name('chandra nalaar'/'DD3_JVC', 'chandra nalaar').
card_uid('chandra nalaar'/'DD3_JVC', 'DD3_JVC:Chandra Nalaar:chandra nalaar').
card_rarity('chandra nalaar'/'DD3_JVC', 'Mythic Rare').
card_artist('chandra nalaar'/'DD3_JVC', 'Kev Walker').
card_number('chandra nalaar'/'DD3_JVC', '34').
card_multiverse_id('chandra nalaar'/'DD3_JVC', '393821').

card_in_set('chartooth cougar', 'DD3_JVC').
card_original_type('chartooth cougar'/'DD3_JVC', 'Creature — Cat Beast').
card_original_text('chartooth cougar'/'DD3_JVC', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card: Search your library for a Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('chartooth cougar'/'DD3_JVC', 'chartooth cougar').
card_uid('chartooth cougar'/'DD3_JVC', 'DD3_JVC:Chartooth Cougar:chartooth cougar').
card_rarity('chartooth cougar'/'DD3_JVC', 'Common').
card_artist('chartooth cougar'/'DD3_JVC', 'Tony Szczudlo').
card_number('chartooth cougar'/'DD3_JVC', '47').
card_multiverse_id('chartooth cougar'/'DD3_JVC', '393822').

card_in_set('condescend', 'DD3_JVC').
card_original_type('condescend'/'DD3_JVC', 'Instant').
card_original_text('condescend'/'DD3_JVC', 'Counter target spell unless its controller pays {X}. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('condescend'/'DD3_JVC', 'condescend').
card_uid('condescend'/'DD3_JVC', 'DD3_JVC:Condescend:condescend').
card_rarity('condescend'/'DD3_JVC', 'Common').
card_artist('condescend'/'DD3_JVC', 'Ron Spears').
card_number('condescend'/'DD3_JVC', '28').
card_multiverse_id('condescend'/'DD3_JVC', '393823').

card_in_set('cone of flame', 'DD3_JVC').
card_original_type('cone of flame'/'DD3_JVC', 'Sorcery').
card_original_text('cone of flame'/'DD3_JVC', 'Cone of Flame deals 1 damage to target creature or player, 2 damage to another target creature or player, and 3 damage to a third target creature or player.').
card_image_name('cone of flame'/'DD3_JVC', 'cone of flame').
card_uid('cone of flame'/'DD3_JVC', 'DD3_JVC:Cone of Flame:cone of flame').
card_rarity('cone of flame'/'DD3_JVC', 'Uncommon').
card_artist('cone of flame'/'DD3_JVC', 'Chippy').
card_number('cone of flame'/'DD3_JVC', '54').
card_multiverse_id('cone of flame'/'DD3_JVC', '393824').

card_in_set('counterspell', 'DD3_JVC').
card_original_type('counterspell'/'DD3_JVC', 'Instant').
card_original_text('counterspell'/'DD3_JVC', 'Counter target spell.').
card_image_name('counterspell'/'DD3_JVC', 'counterspell').
card_uid('counterspell'/'DD3_JVC', 'DD3_JVC:Counterspell:counterspell').
card_rarity('counterspell'/'DD3_JVC', 'Common').
card_artist('counterspell'/'DD3_JVC', 'Jason Chan').
card_number('counterspell'/'DD3_JVC', '24').
card_flavor_text('counterspell'/'DD3_JVC', 'The pyromancer summoned up her mightiest onslaught of fire and rage. Jace feigned interest.').
card_multiverse_id('counterspell'/'DD3_JVC', '393825').

card_in_set('daze', 'DD3_JVC').
card_original_type('daze'/'DD3_JVC', 'Instant').
card_original_text('daze'/'DD3_JVC', 'You may return an Island you control to its owner\'s hand rather than pay Daze\'s mana cost.\nCounter target spell unless its controller pays {1}.').
card_image_name('daze'/'DD3_JVC', 'daze').
card_uid('daze'/'DD3_JVC', 'DD3_JVC:Daze:daze').
card_rarity('daze'/'DD3_JVC', 'Common').
card_artist('daze'/'DD3_JVC', 'Matthew D. Wilson').
card_number('daze'/'DD3_JVC', '23').
card_multiverse_id('daze'/'DD3_JVC', '393826').

card_in_set('demonfire', 'DD3_JVC').
card_original_type('demonfire'/'DD3_JVC', 'Sorcery').
card_original_text('demonfire'/'DD3_JVC', 'Demonfire deals X damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.\nHellbent — If you have no cards in hand, Demonfire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_image_name('demonfire'/'DD3_JVC', 'demonfire').
card_uid('demonfire'/'DD3_JVC', 'DD3_JVC:Demonfire:demonfire').
card_rarity('demonfire'/'DD3_JVC', 'Rare').
card_artist('demonfire'/'DD3_JVC', 'Greg Staples').
card_number('demonfire'/'DD3_JVC', '57').
card_multiverse_id('demonfire'/'DD3_JVC', '393827').

card_in_set('errant ephemeron', 'DD3_JVC').
card_original_type('errant ephemeron'/'DD3_JVC', 'Creature — Illusion').
card_original_text('errant ephemeron'/'DD3_JVC', 'Flying\nSuspend 4—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('errant ephemeron'/'DD3_JVC', 'errant ephemeron').
card_uid('errant ephemeron'/'DD3_JVC', 'DD3_JVC:Errant Ephemeron:errant ephemeron').
card_rarity('errant ephemeron'/'DD3_JVC', 'Common').
card_artist('errant ephemeron'/'DD3_JVC', 'Luca Zontini').
card_number('errant ephemeron'/'DD3_JVC', '20').
card_multiverse_id('errant ephemeron'/'DD3_JVC', '393828').

card_in_set('fact or fiction', 'DD3_JVC').
card_original_type('fact or fiction'/'DD3_JVC', 'Instant').
card_original_text('fact or fiction'/'DD3_JVC', 'Reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('fact or fiction'/'DD3_JVC', 'fact or fiction').
card_uid('fact or fiction'/'DD3_JVC', 'DD3_JVC:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'DD3_JVC', 'Uncommon').
card_artist('fact or fiction'/'DD3_JVC', 'Matt Cavotta').
card_number('fact or fiction'/'DD3_JVC', '26').
card_flavor_text('fact or fiction'/'DD3_JVC', '\"Try to pretend like you understand what\'s important.\"').
card_multiverse_id('fact or fiction'/'DD3_JVC', '393829').

card_in_set('fathom seer', 'DD3_JVC').
card_original_type('fathom seer'/'DD3_JVC', 'Creature — Illusion').
card_original_text('fathom seer'/'DD3_JVC', 'Morph—Return two Islands you control to their owner\'s hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Fathom Seer is turned face up, draw two cards.').
card_image_name('fathom seer'/'DD3_JVC', 'fathom seer').
card_uid('fathom seer'/'DD3_JVC', 'DD3_JVC:Fathom Seer:fathom seer').
card_rarity('fathom seer'/'DD3_JVC', 'Common').
card_artist('fathom seer'/'DD3_JVC', 'Ralph Horsley').
card_number('fathom seer'/'DD3_JVC', '3').
card_multiverse_id('fathom seer'/'DD3_JVC', '393830').

card_in_set('fireball', 'DD3_JVC').
card_original_type('fireball'/'DD3_JVC', 'Sorcery').
card_original_text('fireball'/'DD3_JVC', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'DD3_JVC', 'fireball').
card_uid('fireball'/'DD3_JVC', 'DD3_JVC:Fireball:fireball').
card_rarity('fireball'/'DD3_JVC', 'Uncommon').
card_artist('fireball'/'DD3_JVC', 'Dave Dorman').
card_number('fireball'/'DD3_JVC', '56').
card_flavor_text('fireball'/'DD3_JVC', 'The spell fell upon the crowd like a dragon, ancient and full of death.').
card_multiverse_id('fireball'/'DD3_JVC', '393831').

card_in_set('fireblast', 'DD3_JVC').
card_original_type('fireblast'/'DD3_JVC', 'Instant').
card_original_text('fireblast'/'DD3_JVC', 'You may sacrifice two Mountains rather than pay Fireblast\'s mana cost.\nFireblast deals 4 damage to target creature or player.').
card_image_name('fireblast'/'DD3_JVC', 'fireblast').
card_uid('fireblast'/'DD3_JVC', 'DD3_JVC:Fireblast:fireblast').
card_rarity('fireblast'/'DD3_JVC', 'Common').
card_artist('fireblast'/'DD3_JVC', 'Michael Danza').
card_number('fireblast'/'DD3_JVC', '55').
card_flavor_text('fireblast'/'DD3_JVC', 'Embermages aren\'t well known for their diplomatic skills.').
card_multiverse_id('fireblast'/'DD3_JVC', '393832').

card_in_set('firebolt', 'DD3_JVC').
card_original_type('firebolt'/'DD3_JVC', 'Sorcery').
card_original_text('firebolt'/'DD3_JVC', 'Firebolt deals 2 damage to target creature or player.\nFlashback {4}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('firebolt'/'DD3_JVC', 'firebolt').
card_uid('firebolt'/'DD3_JVC', 'DD3_JVC:Firebolt:firebolt').
card_rarity('firebolt'/'DD3_JVC', 'Common').
card_artist('firebolt'/'DD3_JVC', 'Ron Spencer').
card_number('firebolt'/'DD3_JVC', '49').
card_flavor_text('firebolt'/'DD3_JVC', 'Reach out and torch someone.').
card_multiverse_id('firebolt'/'DD3_JVC', '393833').

card_in_set('fireslinger', 'DD3_JVC').
card_original_type('fireslinger'/'DD3_JVC', 'Creature — Human Wizard').
card_original_text('fireslinger'/'DD3_JVC', '{T}: Fireslinger deals 1 damage to target creature or player and 1 damage to you.').
card_image_name('fireslinger'/'DD3_JVC', 'fireslinger').
card_uid('fireslinger'/'DD3_JVC', 'DD3_JVC:Fireslinger:fireslinger').
card_rarity('fireslinger'/'DD3_JVC', 'Common').
card_artist('fireslinger'/'DD3_JVC', 'Jeff Reitz').
card_number('fireslinger'/'DD3_JVC', '36').
card_flavor_text('fireslinger'/'DD3_JVC', '\"Remember the moral of the fireslinger fable: with power comes isolation.\"\n—Karn, silver golem').
card_multiverse_id('fireslinger'/'DD3_JVC', '393834').

card_in_set('flame javelin', 'DD3_JVC').
card_original_type('flame javelin'/'DD3_JVC', 'Instant').
card_original_text('flame javelin'/'DD3_JVC', '({2/R} can be paid with any two mana or with {R}. This card\'s converted mana cost is 6.)\nFlame Javelin deals 4 damage to target creature or player.').
card_image_name('flame javelin'/'DD3_JVC', 'flame javelin').
card_uid('flame javelin'/'DD3_JVC', 'DD3_JVC:Flame Javelin:flame javelin').
card_rarity('flame javelin'/'DD3_JVC', 'Uncommon').
card_artist('flame javelin'/'DD3_JVC', 'Trevor Hairsine').
card_number('flame javelin'/'DD3_JVC', '53').
card_flavor_text('flame javelin'/'DD3_JVC', 'Gyara Spearhurler would have been renowned for her deadly accuracy, if it weren\'t for her deadly accuracy.').
card_multiverse_id('flame javelin'/'DD3_JVC', '393835').

card_in_set('flamekin brawler', 'DD3_JVC').
card_original_type('flamekin brawler'/'DD3_JVC', 'Creature — Elemental Warrior').
card_original_text('flamekin brawler'/'DD3_JVC', '{R}: Flamekin Brawler gets +1/+0 until end of turn.').
card_image_name('flamekin brawler'/'DD3_JVC', 'flamekin brawler').
card_uid('flamekin brawler'/'DD3_JVC', 'DD3_JVC:Flamekin Brawler:flamekin brawler').
card_rarity('flamekin brawler'/'DD3_JVC', 'Common').
card_artist('flamekin brawler'/'DD3_JVC', 'Daren Bader').
card_number('flamekin brawler'/'DD3_JVC', '35').
card_flavor_text('flamekin brawler'/'DD3_JVC', 'When he hits people, they stay hit.').
card_multiverse_id('flamekin brawler'/'DD3_JVC', '393836').

card_in_set('flametongue kavu', 'DD3_JVC').
card_original_type('flametongue kavu'/'DD3_JVC', 'Creature — Kavu').
card_original_text('flametongue kavu'/'DD3_JVC', 'When Flametongue Kavu enters the battlefield, it deals 4 damage to target creature.').
card_image_name('flametongue kavu'/'DD3_JVC', 'flametongue kavu').
card_uid('flametongue kavu'/'DD3_JVC', 'DD3_JVC:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'DD3_JVC', 'Uncommon').
card_artist('flametongue kavu'/'DD3_JVC', 'Pete Venters').
card_number('flametongue kavu'/'DD3_JVC', '42').
card_flavor_text('flametongue kavu'/'DD3_JVC', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('flametongue kavu'/'DD3_JVC', '393837').

card_in_set('flamewave invoker', 'DD3_JVC').
card_original_type('flamewave invoker'/'DD3_JVC', 'Creature — Goblin Mutant').
card_original_text('flamewave invoker'/'DD3_JVC', '{7}{R}: Flamewave Invoker deals 5 damage to target player.').
card_image_name('flamewave invoker'/'DD3_JVC', 'flamewave invoker').
card_uid('flamewave invoker'/'DD3_JVC', 'DD3_JVC:Flamewave Invoker:flamewave invoker').
card_rarity('flamewave invoker'/'DD3_JVC', 'Uncommon').
card_artist('flamewave invoker'/'DD3_JVC', 'Dave Dorman').
card_number('flamewave invoker'/'DD3_JVC', '40').
card_flavor_text('flamewave invoker'/'DD3_JVC', 'Inside even the humblest goblin lurks the potential for far greater things—and far worse.').
card_multiverse_id('flamewave invoker'/'DD3_JVC', '393838').

card_in_set('fledgling mawcor', 'DD3_JVC').
card_original_type('fledgling mawcor'/'DD3_JVC', 'Creature — Beast').
card_original_text('fledgling mawcor'/'DD3_JVC', 'Flying\n{T}: Fledgling Mawcor deals 1 damage to target creature or player.\nMorph {U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('fledgling mawcor'/'DD3_JVC', 'fledgling mawcor').
card_uid('fledgling mawcor'/'DD3_JVC', 'DD3_JVC:Fledgling Mawcor:fledgling mawcor').
card_rarity('fledgling mawcor'/'DD3_JVC', 'Uncommon').
card_artist('fledgling mawcor'/'DD3_JVC', 'Kev Walker').
card_number('fledgling mawcor'/'DD3_JVC', '10').
card_multiverse_id('fledgling mawcor'/'DD3_JVC', '393839').

card_in_set('furnace whelp', 'DD3_JVC').
card_original_type('furnace whelp'/'DD3_JVC', 'Creature — Dragon').
card_original_text('furnace whelp'/'DD3_JVC', 'Flying\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_image_name('furnace whelp'/'DD3_JVC', 'furnace whelp').
card_uid('furnace whelp'/'DD3_JVC', 'DD3_JVC:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'DD3_JVC', 'Uncommon').
card_artist('furnace whelp'/'DD3_JVC', 'Matt Cavotta').
card_number('furnace whelp'/'DD3_JVC', '43').
card_flavor_text('furnace whelp'/'DD3_JVC', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'DD3_JVC', '393840').

card_in_set('guile', 'DD3_JVC').
card_original_type('guile'/'DD3_JVC', 'Creature — Elemental Incarnation').
card_original_text('guile'/'DD3_JVC', 'Guile can\'t be blocked except by three or more creatures.\nIf a spell or ability you control would counter a spell, instead exile that spell and you may play that card without paying its mana cost.\nWhen Guile is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('guile'/'DD3_JVC', 'guile').
card_uid('guile'/'DD3_JVC', 'DD3_JVC:Guile:guile').
card_rarity('guile'/'DD3_JVC', 'Rare').
card_artist('guile'/'DD3_JVC', 'Zoltan Boros & Gabor Szikszai').
card_number('guile'/'DD3_JVC', '14').
card_multiverse_id('guile'/'DD3_JVC', '393841').

card_in_set('gush', 'DD3_JVC').
card_original_type('gush'/'DD3_JVC', 'Instant').
card_original_text('gush'/'DD3_JVC', 'You may return two Islands you control to their owner\'s hand rather than pay Gush\'s mana cost.\nDraw two cards.').
card_image_name('gush'/'DD3_JVC', 'gush').
card_uid('gush'/'DD3_JVC', 'DD3_JVC:Gush:gush').
card_rarity('gush'/'DD3_JVC', 'Common').
card_artist('gush'/'DD3_JVC', 'Kev Walker').
card_number('gush'/'DD3_JVC', '27').
card_flavor_text('gush'/'DD3_JVC', 'Don\'t trust your secrets to the sea.').
card_multiverse_id('gush'/'DD3_JVC', '393842').

card_in_set('hostility', 'DD3_JVC').
card_original_type('hostility'/'DD3_JVC', 'Creature — Elemental Incarnation').
card_original_text('hostility'/'DD3_JVC', 'Haste\nIf a spell you control would deal damage to an opponent, prevent that damage. Put a 3/1 red Elemental Shaman creature token with haste onto the battlefield for each 1 damage prevented this way.\nWhen Hostility is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('hostility'/'DD3_JVC', 'hostility').
card_uid('hostility'/'DD3_JVC', 'DD3_JVC:Hostility:hostility').
card_rarity('hostility'/'DD3_JVC', 'Rare').
card_artist('hostility'/'DD3_JVC', 'Omar Rayyan').
card_number('hostility'/'DD3_JVC', '48').
card_multiverse_id('hostility'/'DD3_JVC', '393843').

card_in_set('incinerate', 'DD3_JVC').
card_original_type('incinerate'/'DD3_JVC', 'Instant').
card_original_text('incinerate'/'DD3_JVC', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_image_name('incinerate'/'DD3_JVC', 'incinerate').
card_uid('incinerate'/'DD3_JVC', 'DD3_JVC:Incinerate:incinerate').
card_rarity('incinerate'/'DD3_JVC', 'Common').
card_artist('incinerate'/'DD3_JVC', 'Steve Prescott').
card_number('incinerate'/'DD3_JVC', '51').
card_flavor_text('incinerate'/'DD3_JVC', '\"Who said there are no assurances in life? I assure you this is going to hurt.\"').
card_multiverse_id('incinerate'/'DD3_JVC', '393844').

card_in_set('ingot chewer', 'DD3_JVC').
card_original_type('ingot chewer'/'DD3_JVC', 'Creature — Elemental').
card_original_text('ingot chewer'/'DD3_JVC', 'When Ingot Chewer enters the battlefield, destroy target artifact.\nEvoke {R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('ingot chewer'/'DD3_JVC', 'ingot chewer').
card_uid('ingot chewer'/'DD3_JVC', 'DD3_JVC:Ingot Chewer:ingot chewer').
card_rarity('ingot chewer'/'DD3_JVC', 'Common').
card_artist('ingot chewer'/'DD3_JVC', 'Kev Walker').
card_number('ingot chewer'/'DD3_JVC', '45').
card_flavor_text('ingot chewer'/'DD3_JVC', 'Elementals are ideas given form. This one is the idea of \"smashitude.\"').
card_multiverse_id('ingot chewer'/'DD3_JVC', '393845').

card_in_set('inner-flame acolyte', 'DD3_JVC').
card_original_type('inner-flame acolyte'/'DD3_JVC', 'Creature — Elemental Shaman').
card_original_text('inner-flame acolyte'/'DD3_JVC', 'When Inner-Flame Acolyte enters the battlefield, target creature gets +2/+0 and gains haste until end of turn.\nEvoke {R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('inner-flame acolyte'/'DD3_JVC', 'inner-flame acolyte').
card_uid('inner-flame acolyte'/'DD3_JVC', 'DD3_JVC:Inner-Flame Acolyte:inner-flame acolyte').
card_rarity('inner-flame acolyte'/'DD3_JVC', 'Common').
card_artist('inner-flame acolyte'/'DD3_JVC', 'Ron Spears').
card_number('inner-flame acolyte'/'DD3_JVC', '41').
card_multiverse_id('inner-flame acolyte'/'DD3_JVC', '393846').

card_in_set('island', 'DD3_JVC').
card_original_type('island'/'DD3_JVC', 'Basic Land — Island').
card_original_text('island'/'DD3_JVC', 'U').
card_image_name('island'/'DD3_JVC', 'island1').
card_uid('island'/'DD3_JVC', 'DD3_JVC:Island:island1').
card_rarity('island'/'DD3_JVC', 'Basic Land').
card_artist('island'/'DD3_JVC', 'John Avon').
card_number('island'/'DD3_JVC', '30').
card_multiverse_id('island'/'DD3_JVC', '393847').

card_in_set('island', 'DD3_JVC').
card_original_type('island'/'DD3_JVC', 'Basic Land — Island').
card_original_text('island'/'DD3_JVC', 'U').
card_image_name('island'/'DD3_JVC', 'island2').
card_uid('island'/'DD3_JVC', 'DD3_JVC:Island:island2').
card_rarity('island'/'DD3_JVC', 'Basic Land').
card_artist('island'/'DD3_JVC', 'Scott Bailey').
card_number('island'/'DD3_JVC', '31').
card_multiverse_id('island'/'DD3_JVC', '393849').

card_in_set('island', 'DD3_JVC').
card_original_type('island'/'DD3_JVC', 'Basic Land — Island').
card_original_text('island'/'DD3_JVC', 'U').
card_image_name('island'/'DD3_JVC', 'island3').
card_uid('island'/'DD3_JVC', 'DD3_JVC:Island:island3').
card_rarity('island'/'DD3_JVC', 'Basic Land').
card_artist('island'/'DD3_JVC', 'Donato Giancola').
card_number('island'/'DD3_JVC', '32').
card_multiverse_id('island'/'DD3_JVC', '393848').

card_in_set('island', 'DD3_JVC').
card_original_type('island'/'DD3_JVC', 'Basic Land — Island').
card_original_text('island'/'DD3_JVC', 'U').
card_image_name('island'/'DD3_JVC', 'island4').
card_uid('island'/'DD3_JVC', 'DD3_JVC:Island:island4').
card_rarity('island'/'DD3_JVC', 'Basic Land').
card_artist('island'/'DD3_JVC', 'Christopher Moeller').
card_number('island'/'DD3_JVC', '33').
card_multiverse_id('island'/'DD3_JVC', '393850').

card_in_set('jace beleren', 'DD3_JVC').
card_original_type('jace beleren'/'DD3_JVC', 'Planeswalker — Jace').
card_original_text('jace beleren'/'DD3_JVC', '+2: Each player draws a card.\n–1: Target player draws a card.\n–10: Target player puts the top twenty cards of his or her library into his or her graveyard.').
card_image_name('jace beleren'/'DD3_JVC', 'jace beleren').
card_uid('jace beleren'/'DD3_JVC', 'DD3_JVC:Jace Beleren:jace beleren').
card_rarity('jace beleren'/'DD3_JVC', 'Mythic Rare').
card_artist('jace beleren'/'DD3_JVC', 'Kev Walker').
card_number('jace beleren'/'DD3_JVC', '1').
card_multiverse_id('jace beleren'/'DD3_JVC', '393851').

card_in_set('keldon megaliths', 'DD3_JVC').
card_original_type('keldon megaliths'/'DD3_JVC', 'Land').
card_original_text('keldon megaliths'/'DD3_JVC', 'Keldon Megaliths enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\nHellbent — {1}{R}, {T}: Keldon Megaliths deals 1 damage to target creature or player. Activate this ability only if you have no cards in hand.').
card_image_name('keldon megaliths'/'DD3_JVC', 'keldon megaliths').
card_uid('keldon megaliths'/'DD3_JVC', 'DD3_JVC:Keldon Megaliths:keldon megaliths').
card_rarity('keldon megaliths'/'DD3_JVC', 'Uncommon').
card_artist('keldon megaliths'/'DD3_JVC', 'Philip Straub').
card_number('keldon megaliths'/'DD3_JVC', '58').
card_multiverse_id('keldon megaliths'/'DD3_JVC', '393852').

card_in_set('magma jet', 'DD3_JVC').
card_original_type('magma jet'/'DD3_JVC', 'Instant').
card_original_text('magma jet'/'DD3_JVC', 'Magma Jet deals 2 damage to target creature or player. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('magma jet'/'DD3_JVC', 'magma jet').
card_uid('magma jet'/'DD3_JVC', 'DD3_JVC:Magma Jet:magma jet').
card_rarity('magma jet'/'DD3_JVC', 'Uncommon').
card_artist('magma jet'/'DD3_JVC', 'Justin Sweet').
card_number('magma jet'/'DD3_JVC', '52').
card_multiverse_id('magma jet'/'DD3_JVC', '393853').

card_in_set('man-o\'-war', 'DD3_JVC').
card_original_type('man-o\'-war'/'DD3_JVC', 'Creature — Jellyfish').
card_original_text('man-o\'-war'/'DD3_JVC', 'When Man-o\'-War enters the battlefield, return target creature to its owner\'s hand.').
card_image_name('man-o\'-war'/'DD3_JVC', 'man-o\'-war').
card_uid('man-o\'-war'/'DD3_JVC', 'DD3_JVC:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'DD3_JVC', 'Common').
card_artist('man-o\'-war'/'DD3_JVC', 'Jon J. Muth').
card_number('man-o\'-war'/'DD3_JVC', '8').
card_flavor_text('man-o\'-war'/'DD3_JVC', '\"Beauty to the eye does not always translate to the touch.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('man-o\'-war'/'DD3_JVC', '393854').

card_in_set('martyr of frost', 'DD3_JVC').
card_original_type('martyr of frost'/'DD3_JVC', 'Creature — Human Wizard').
card_original_text('martyr of frost'/'DD3_JVC', '{2}, Reveal X blue cards from your hand, Sacrifice Martyr of Frost: Counter target spell unless its controller pays {X}.').
card_image_name('martyr of frost'/'DD3_JVC', 'martyr of frost').
card_uid('martyr of frost'/'DD3_JVC', 'DD3_JVC:Martyr of Frost:martyr of frost').
card_rarity('martyr of frost'/'DD3_JVC', 'Common').
card_artist('martyr of frost'/'DD3_JVC', 'Wayne England').
card_number('martyr of frost'/'DD3_JVC', '2').
card_flavor_text('martyr of frost'/'DD3_JVC', '\"Do not call the Balduvians barbaric. Their magic is as potent as it is primal.\"\n—Zur the Enchanter').
card_multiverse_id('martyr of frost'/'DD3_JVC', '393855').

card_in_set('mind stone', 'DD3_JVC').
card_original_type('mind stone'/'DD3_JVC', 'Artifact').
card_original_text('mind stone'/'DD3_JVC', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Mind Stone: Draw a card.').
card_image_name('mind stone'/'DD3_JVC', 'mind stone').
card_uid('mind stone'/'DD3_JVC', 'DD3_JVC:Mind Stone:mind stone').
card_rarity('mind stone'/'DD3_JVC', 'Uncommon').
card_artist('mind stone'/'DD3_JVC', 'Adam Rex').
card_number('mind stone'/'DD3_JVC', '22').
card_flavor_text('mind stone'/'DD3_JVC', '\"What is mana but possibility, an idea not yet given form?\"\n—Jhoira, master artificer').
card_multiverse_id('mind stone'/'DD3_JVC', '393856').

card_in_set('mountain', 'DD3_JVC').
card_original_type('mountain'/'DD3_JVC', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_JVC', 'R').
card_image_name('mountain'/'DD3_JVC', 'mountain1').
card_uid('mountain'/'DD3_JVC', 'DD3_JVC:Mountain:mountain1').
card_rarity('mountain'/'DD3_JVC', 'Basic Land').
card_artist('mountain'/'DD3_JVC', 'Rob Alexander').
card_number('mountain'/'DD3_JVC', '59').
card_multiverse_id('mountain'/'DD3_JVC', '393857').

card_in_set('mountain', 'DD3_JVC').
card_original_type('mountain'/'DD3_JVC', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_JVC', 'R').
card_image_name('mountain'/'DD3_JVC', 'mountain2').
card_uid('mountain'/'DD3_JVC', 'DD3_JVC:Mountain:mountain2').
card_rarity('mountain'/'DD3_JVC', 'Basic Land').
card_artist('mountain'/'DD3_JVC', 'Christopher Moeller').
card_number('mountain'/'DD3_JVC', '60').
card_multiverse_id('mountain'/'DD3_JVC', '393860').

card_in_set('mountain', 'DD3_JVC').
card_original_type('mountain'/'DD3_JVC', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_JVC', 'R').
card_image_name('mountain'/'DD3_JVC', 'mountain3').
card_uid('mountain'/'DD3_JVC', 'DD3_JVC:Mountain:mountain3').
card_rarity('mountain'/'DD3_JVC', 'Basic Land').
card_artist('mountain'/'DD3_JVC', 'Greg Staples').
card_number('mountain'/'DD3_JVC', '61').
card_multiverse_id('mountain'/'DD3_JVC', '393858').

card_in_set('mountain', 'DD3_JVC').
card_original_type('mountain'/'DD3_JVC', 'Basic Land — Mountain').
card_original_text('mountain'/'DD3_JVC', 'R').
card_image_name('mountain'/'DD3_JVC', 'mountain4').
card_uid('mountain'/'DD3_JVC', 'DD3_JVC:Mountain:mountain4').
card_rarity('mountain'/'DD3_JVC', 'Basic Land').
card_artist('mountain'/'DD3_JVC', 'Mark Tedin').
card_number('mountain'/'DD3_JVC', '62').
card_multiverse_id('mountain'/'DD3_JVC', '393859').

card_in_set('mulldrifter', 'DD3_JVC').
card_original_type('mulldrifter'/'DD3_JVC', 'Creature — Elemental').
card_original_text('mulldrifter'/'DD3_JVC', 'Flying\nWhen Mulldrifter enters the battlefield, draw two cards.\nEvoke {2}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('mulldrifter'/'DD3_JVC', 'mulldrifter').
card_uid('mulldrifter'/'DD3_JVC', 'DD3_JVC:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'DD3_JVC', 'Common').
card_artist('mulldrifter'/'DD3_JVC', 'Eric Fortune').
card_number('mulldrifter'/'DD3_JVC', '12').
card_multiverse_id('mulldrifter'/'DD3_JVC', '393861').

card_in_set('ophidian', 'DD3_JVC').
card_original_type('ophidian'/'DD3_JVC', 'Creature — Snake').
card_original_text('ophidian'/'DD3_JVC', 'Whenever Ophidian attacks and isn\'t blocked, you may draw a card. If you do, Ophidian assigns no combat damage this turn.').
card_image_name('ophidian'/'DD3_JVC', 'ophidian').
card_uid('ophidian'/'DD3_JVC', 'DD3_JVC:Ophidian:ophidian').
card_rarity('ophidian'/'DD3_JVC', 'Common').
card_artist('ophidian'/'DD3_JVC', 'Cliff Nielsen').
card_number('ophidian'/'DD3_JVC', '9').
card_multiverse_id('ophidian'/'DD3_JVC', '393862').

card_in_set('oxidda golem', 'DD3_JVC').
card_original_type('oxidda golem'/'DD3_JVC', 'Artifact Creature — Golem').
card_original_text('oxidda golem'/'DD3_JVC', 'Affinity for Mountains (This spell costs {1} less to cast for each Mountain you control.)\nHaste').
card_image_name('oxidda golem'/'DD3_JVC', 'oxidda golem').
card_uid('oxidda golem'/'DD3_JVC', 'DD3_JVC:Oxidda Golem:oxidda golem').
card_rarity('oxidda golem'/'DD3_JVC', 'Common').
card_artist('oxidda golem'/'DD3_JVC', 'Greg Staples').
card_number('oxidda golem'/'DD3_JVC', '46').
card_flavor_text('oxidda golem'/'DD3_JVC', 'The longer it patrols the smoldering crags of the Oxidda Chain, the more it adopts their fiery temper.').
card_multiverse_id('oxidda golem'/'DD3_JVC', '393863').

card_in_set('pyre charger', 'DD3_JVC').
card_original_type('pyre charger'/'DD3_JVC', 'Creature — Elemental Warrior').
card_original_text('pyre charger'/'DD3_JVC', 'Haste\n{R}: Pyre Charger gets +1/+0 until end of turn.').
card_image_name('pyre charger'/'DD3_JVC', 'pyre charger').
card_uid('pyre charger'/'DD3_JVC', 'DD3_JVC:Pyre Charger:pyre charger').
card_rarity('pyre charger'/'DD3_JVC', 'Uncommon').
card_artist('pyre charger'/'DD3_JVC', 'Mark Zug').
card_number('pyre charger'/'DD3_JVC', '38').
card_flavor_text('pyre charger'/'DD3_JVC', 'His blade was forged over coals of moaning treefolk, curved at the optimum angle for severing heads, and heated to volcanic temperatures by his touch.').
card_multiverse_id('pyre charger'/'DD3_JVC', '393864').

card_in_set('quicksilver dragon', 'DD3_JVC').
card_original_type('quicksilver dragon'/'DD3_JVC', 'Creature — Dragon').
card_original_text('quicksilver dragon'/'DD3_JVC', 'Flying\n{U}: If target spell has only one target and that target is Quicksilver Dragon, change that spell\'s target to another creature.\nMorph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('quicksilver dragon'/'DD3_JVC', 'quicksilver dragon').
card_uid('quicksilver dragon'/'DD3_JVC', 'DD3_JVC:Quicksilver Dragon:quicksilver dragon').
card_rarity('quicksilver dragon'/'DD3_JVC', 'Rare').
card_artist('quicksilver dragon'/'DD3_JVC', 'Ron Spencer').
card_number('quicksilver dragon'/'DD3_JVC', '19').
card_multiverse_id('quicksilver dragon'/'DD3_JVC', '393865').

card_in_set('rakdos pit dragon', 'DD3_JVC').
card_original_type('rakdos pit dragon'/'DD3_JVC', 'Creature — Dragon').
card_original_text('rakdos pit dragon'/'DD3_JVC', '{R}{R}: Rakdos Pit Dragon gains flying until end of turn.\n{R}: Rakdos Pit Dragon gets +1/+0 until end of turn.\nHellbent — Rakdos Pit Dragon has double strike as long as you have no cards in hand.').
card_image_name('rakdos pit dragon'/'DD3_JVC', 'rakdos pit dragon').
card_uid('rakdos pit dragon'/'DD3_JVC', 'DD3_JVC:Rakdos Pit Dragon:rakdos pit dragon').
card_rarity('rakdos pit dragon'/'DD3_JVC', 'Rare').
card_artist('rakdos pit dragon'/'DD3_JVC', 'Kev Walker').
card_number('rakdos pit dragon'/'DD3_JVC', '44').
card_multiverse_id('rakdos pit dragon'/'DD3_JVC', '393866').

card_in_set('repulse', 'DD3_JVC').
card_original_type('repulse'/'DD3_JVC', 'Instant').
card_original_text('repulse'/'DD3_JVC', 'Return target creature to its owner\'s hand.\nDraw a card.').
card_image_name('repulse'/'DD3_JVC', 'repulse').
card_uid('repulse'/'DD3_JVC', 'DD3_JVC:Repulse:repulse').
card_rarity('repulse'/'DD3_JVC', 'Common').
card_artist('repulse'/'DD3_JVC', 'Aaron Boyd').
card_number('repulse'/'DD3_JVC', '25').
card_flavor_text('repulse'/'DD3_JVC', '\"You aren\'t invited.\"').
card_multiverse_id('repulse'/'DD3_JVC', '393867').

card_in_set('riftwing cloudskate', 'DD3_JVC').
card_original_type('riftwing cloudskate'/'DD3_JVC', 'Creature — Illusion').
card_original_text('riftwing cloudskate'/'DD3_JVC', 'Flying\nWhen Riftwing Cloudskate enters the battlefield, return target permanent to its owner\'s hand.\nSuspend 3—{1}{U} (Rather than cast this card from your hand, you may pay {1}{U} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_image_name('riftwing cloudskate'/'DD3_JVC', 'riftwing cloudskate').
card_uid('riftwing cloudskate'/'DD3_JVC', 'DD3_JVC:Riftwing Cloudskate:riftwing cloudskate').
card_rarity('riftwing cloudskate'/'DD3_JVC', 'Uncommon').
card_artist('riftwing cloudskate'/'DD3_JVC', 'Carl Critchlow').
card_number('riftwing cloudskate'/'DD3_JVC', '15').
card_multiverse_id('riftwing cloudskate'/'DD3_JVC', '393868').

card_in_set('seal of fire', 'DD3_JVC').
card_original_type('seal of fire'/'DD3_JVC', 'Enchantment').
card_original_text('seal of fire'/'DD3_JVC', 'Sacrifice Seal of Fire: Seal of Fire deals 2 damage to target creature or player.').
card_image_name('seal of fire'/'DD3_JVC', 'seal of fire').
card_uid('seal of fire'/'DD3_JVC', 'DD3_JVC:Seal of Fire:seal of fire').
card_rarity('seal of fire'/'DD3_JVC', 'Common').
card_artist('seal of fire'/'DD3_JVC', 'Christopher Moeller').
card_number('seal of fire'/'DD3_JVC', '50').
card_flavor_text('seal of fire'/'DD3_JVC', 'After the fourth time Chandra left a place engulfed in flames, she decided to just go ahead and make it her thing.').
card_multiverse_id('seal of fire'/'DD3_JVC', '393869').

card_in_set('slith firewalker', 'DD3_JVC').
card_original_type('slith firewalker'/'DD3_JVC', 'Creature — Slith').
card_original_text('slith firewalker'/'DD3_JVC', 'Haste\nWhenever Slith Firewalker deals combat damage to a player, put a +1/+1 counter on it.').
card_image_name('slith firewalker'/'DD3_JVC', 'slith firewalker').
card_uid('slith firewalker'/'DD3_JVC', 'DD3_JVC:Slith Firewalker:slith firewalker').
card_rarity('slith firewalker'/'DD3_JVC', 'Uncommon').
card_artist('slith firewalker'/'DD3_JVC', 'Justin Sweet').
card_number('slith firewalker'/'DD3_JVC', '39').
card_flavor_text('slith firewalker'/'DD3_JVC', 'The slith incubate in the Great Furnace\'s heat, emerging on Mirrodin\'s surface only when the four suns have aligned overhead.').
card_multiverse_id('slith firewalker'/'DD3_JVC', '393870').

card_in_set('soulbright flamekin', 'DD3_JVC').
card_original_type('soulbright flamekin'/'DD3_JVC', 'Creature — Elemental Shaman').
card_original_text('soulbright flamekin'/'DD3_JVC', '{2}: Target creature gains trample until end of turn. If this is the third time this ability has resolved this turn, you may add {R}{R}{R}{R}{R}{R}{R}{R} to your mana pool.').
card_image_name('soulbright flamekin'/'DD3_JVC', 'soulbright flamekin').
card_uid('soulbright flamekin'/'DD3_JVC', 'DD3_JVC:Soulbright Flamekin:soulbright flamekin').
card_rarity('soulbright flamekin'/'DD3_JVC', 'Common').
card_artist('soulbright flamekin'/'DD3_JVC', 'Kev Walker').
card_number('soulbright flamekin'/'DD3_JVC', '37').
card_flavor_text('soulbright flamekin'/'DD3_JVC', 'When provoked, a flamekin\'s inner fire burns far hotter than any giant\'s forge.').
card_multiverse_id('soulbright flamekin'/'DD3_JVC', '393871').

card_in_set('spire golem', 'DD3_JVC').
card_original_type('spire golem'/'DD3_JVC', 'Artifact Creature — Golem').
card_original_text('spire golem'/'DD3_JVC', 'Affinity for Islands (This spell costs {1} less to cast for each Island you control.)\nFlying').
card_image_name('spire golem'/'DD3_JVC', 'spire golem').
card_uid('spire golem'/'DD3_JVC', 'DD3_JVC:Spire Golem:spire golem').
card_rarity('spire golem'/'DD3_JVC', 'Common').
card_artist('spire golem'/'DD3_JVC', 'Daren Bader').
card_number('spire golem'/'DD3_JVC', '16').
card_flavor_text('spire golem'/'DD3_JVC', 'The longer it soars above the shimmering swirls of the Quicksilver Sea, the more it adopts their unpredictability.').
card_multiverse_id('spire golem'/'DD3_JVC', '393872').

card_in_set('terrain generator', 'DD3_JVC').
card_original_type('terrain generator'/'DD3_JVC', 'Land').
card_original_text('terrain generator'/'DD3_JVC', '{T}: Add {1} to your mana pool.\n{2}, {T}: You may put a basic land card from your hand onto the battlefield tapped.').
card_image_name('terrain generator'/'DD3_JVC', 'terrain generator').
card_uid('terrain generator'/'DD3_JVC', 'DD3_JVC:Terrain Generator:terrain generator').
card_rarity('terrain generator'/'DD3_JVC', 'Uncommon').
card_artist('terrain generator'/'DD3_JVC', 'Alan Pollack').
card_number('terrain generator'/'DD3_JVC', '29').
card_multiverse_id('terrain generator'/'DD3_JVC', '393873').

card_in_set('voidmage apprentice', 'DD3_JVC').
card_original_type('voidmage apprentice'/'DD3_JVC', 'Creature — Human Wizard').
card_original_text('voidmage apprentice'/'DD3_JVC', 'Morph {2}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Voidmage Apprentice is turned face up, counter target spell.').
card_image_name('voidmage apprentice'/'DD3_JVC', 'voidmage apprentice').
card_uid('voidmage apprentice'/'DD3_JVC', 'DD3_JVC:Voidmage Apprentice:voidmage apprentice').
card_rarity('voidmage apprentice'/'DD3_JVC', 'Common').
card_artist('voidmage apprentice'/'DD3_JVC', 'Jim Nelson').
card_number('voidmage apprentice'/'DD3_JVC', '4').
card_multiverse_id('voidmage apprentice'/'DD3_JVC', '393874').

card_in_set('wall of deceit', 'DD3_JVC').
card_original_type('wall of deceit'/'DD3_JVC', 'Creature — Wall').
card_original_text('wall of deceit'/'DD3_JVC', 'Defender\n{3}: Turn Wall of Deceit face down.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('wall of deceit'/'DD3_JVC', 'wall of deceit').
card_uid('wall of deceit'/'DD3_JVC', 'DD3_JVC:Wall of Deceit:wall of deceit').
card_rarity('wall of deceit'/'DD3_JVC', 'Uncommon').
card_artist('wall of deceit'/'DD3_JVC', 'John Avon').
card_number('wall of deceit'/'DD3_JVC', '5').
card_multiverse_id('wall of deceit'/'DD3_JVC', '393875').

card_in_set('waterspout djinn', 'DD3_JVC').
card_original_type('waterspout djinn'/'DD3_JVC', 'Creature — Djinn').
card_original_text('waterspout djinn'/'DD3_JVC', 'Flying\nAt the beginning of your upkeep, sacrifice Waterspout Djinn unless you return an untapped Island you control to its owner\'s hand.').
card_image_name('waterspout djinn'/'DD3_JVC', 'waterspout djinn').
card_uid('waterspout djinn'/'DD3_JVC', 'DD3_JVC:Waterspout Djinn:waterspout djinn').
card_rarity('waterspout djinn'/'DD3_JVC', 'Uncommon').
card_artist('waterspout djinn'/'DD3_JVC', 'Thomas Gianni').
card_number('waterspout djinn'/'DD3_JVC', '11').
card_flavor_text('waterspout djinn'/'DD3_JVC', '\"Fly us higher, out of its storm.\"\n—Sisay, captain of the Weatherlight').
card_multiverse_id('waterspout djinn'/'DD3_JVC', '393876').

card_in_set('willbender', 'DD3_JVC').
card_original_type('willbender'/'DD3_JVC', 'Creature — Human Wizard').
card_original_text('willbender'/'DD3_JVC', 'Morph {1}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Willbender is turned face up, change the target of target spell or ability with a single target.').
card_image_name('willbender'/'DD3_JVC', 'willbender').
card_uid('willbender'/'DD3_JVC', 'DD3_JVC:Willbender:willbender').
card_rarity('willbender'/'DD3_JVC', 'Uncommon').
card_artist('willbender'/'DD3_JVC', 'Eric Peterson').
card_number('willbender'/'DD3_JVC', '6').
card_multiverse_id('willbender'/'DD3_JVC', '393877').
