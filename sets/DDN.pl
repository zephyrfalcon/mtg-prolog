% Duel Decks: Speed vs. Cunning

set('DDN').
set_name('DDN', 'Duel Decks: Speed vs. Cunning').
set_release_date('DDN', '2014-09-05').
set_border('DDN', 'black').
set_type('DDN', 'duel deck').

card_in_set('act of treason', 'DDN').
card_original_type('act of treason'/'DDN', 'Sorcery').
card_original_text('act of treason'/'DDN', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_image_name('act of treason'/'DDN', 'act of treason').
card_uid('act of treason'/'DDN', 'DDN:Act of Treason:act of treason').
card_rarity('act of treason'/'DDN', 'Common').
card_artist('act of treason'/'DDN', 'Eric Deschamps').
card_number('act of treason'/'DDN', '26').
card_flavor_text('act of treason'/'DDN', '\"Rage courses in every heart, yearning to betray its rational prison.\"\n—Sarkhan Vol').
card_multiverse_id('act of treason'/'DDN', '386299').

card_in_set('aquamorph entity', 'DDN').
card_original_type('aquamorph entity'/'DDN', 'Creature — Shapeshifter').
card_original_text('aquamorph entity'/'DDN', 'As Aquamorph Entity enters the battlefield or is turned face up, it becomes your choice of 5/1 or 1/5.\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('aquamorph entity'/'DDN', 'aquamorph entity').
card_uid('aquamorph entity'/'DDN', 'DDN:Aquamorph Entity:aquamorph entity').
card_rarity('aquamorph entity'/'DDN', 'Common').
card_artist('aquamorph entity'/'DDN', 'Brian Despain').
card_number('aquamorph entity'/'DDN', '54').
card_multiverse_id('aquamorph entity'/'DDN', '386300').

card_in_set('arc trail', 'DDN').
card_original_type('arc trail'/'DDN', 'Sorcery').
card_original_text('arc trail'/'DDN', 'Arc Trail deals 2 damage to target creature or player and 1 damage to another target creature or player.').
card_image_name('arc trail'/'DDN', 'arc trail').
card_uid('arc trail'/'DDN', 'DDN:Arc Trail:arc trail').
card_rarity('arc trail'/'DDN', 'Uncommon').
card_artist('arc trail'/'DDN', 'Marc Simonetti').
card_number('arc trail'/'DDN', '23').
card_flavor_text('arc trail'/'DDN', '\"Don\'t try to hit your enemies. Concentrate on the space between them, and fill the air with doom.\"\n—Spear-Tribe teaching').
card_multiverse_id('arc trail'/'DDN', '386301').

card_in_set('arcanis the omnipotent', 'DDN').
card_original_type('arcanis the omnipotent'/'DDN', 'Legendary Creature — Wizard').
card_original_text('arcanis the omnipotent'/'DDN', '{T}: Draw three cards.\n{2}{U}{U}: Return Arcanis the Omnipotent to its owner\'s hand.').
card_image_name('arcanis the omnipotent'/'DDN', 'arcanis the omnipotent').
card_uid('arcanis the omnipotent'/'DDN', 'DDN:Arcanis the Omnipotent:arcanis the omnipotent').
card_rarity('arcanis the omnipotent'/'DDN', 'Mythic Rare').
card_artist('arcanis the omnipotent'/'DDN', 'Ryan Alexander Lee').
card_number('arcanis the omnipotent'/'DDN', '42').
card_flavor_text('arcanis the omnipotent'/'DDN', '\"Do not concern yourself with my origin, my race, or my ancestry. Seek my record in the pits, and then make your wager.\"').
card_multiverse_id('arcanis the omnipotent'/'DDN', '386302').

card_in_set('arrow volley trap', 'DDN').
card_original_type('arrow volley trap'/'DDN', 'Instant — Trap').
card_original_text('arrow volley trap'/'DDN', 'If four or more creatures are attacking, you may pay {1}{W} rather than pay Arrow Volley Trap\'s mana cost.\nArrow Volley Trap deals 5 damage divided as you choose among any number of target attacking creatures.').
card_image_name('arrow volley trap'/'DDN', 'arrow volley trap').
card_uid('arrow volley trap'/'DDN', 'DDN:Arrow Volley Trap:arrow volley trap').
card_rarity('arrow volley trap'/'DDN', 'Uncommon').
card_artist('arrow volley trap'/'DDN', 'Steve Argyle').
card_number('arrow volley trap'/'DDN', '71').
card_multiverse_id('arrow volley trap'/'DDN', '386303').

card_in_set('banefire', 'DDN').
card_original_type('banefire'/'DDN', 'Sorcery').
card_original_text('banefire'/'DDN', 'Banefire deals X damage to target creature or player.\nIf X is 5 or more, Banefire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_image_name('banefire'/'DDN', 'banefire').
card_uid('banefire'/'DDN', 'DDN:Banefire:banefire').
card_rarity('banefire'/'DDN', 'Rare').
card_artist('banefire'/'DDN', 'Raymond Swanland').
card_number('banefire'/'DDN', '31').
card_flavor_text('banefire'/'DDN', 'For Sarkhan Vol, the dragon is the purest expression of life\'s savage splendor.').
card_multiverse_id('banefire'/'DDN', '386304').

card_in_set('beetleback chief', 'DDN').
card_original_type('beetleback chief'/'DDN', 'Creature — Goblin Warrior').
card_original_text('beetleback chief'/'DDN', 'When Beetleback Chief enters the battlefield, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('beetleback chief'/'DDN', 'beetleback chief').
card_uid('beetleback chief'/'DDN', 'DDN:Beetleback Chief:beetleback chief').
card_rarity('beetleback chief'/'DDN', 'Uncommon').
card_artist('beetleback chief'/'DDN', 'Wayne England').
card_number('beetleback chief'/'DDN', '14').
card_flavor_text('beetleback chief'/'DDN', 'Whether trained, ridden, or eaten, few goblin military innovations have rivaled the bug.').
card_multiverse_id('beetleback chief'/'DDN', '386305').

card_in_set('bone splinters', 'DDN').
card_original_type('bone splinters'/'DDN', 'Sorcery').
card_original_text('bone splinters'/'DDN', 'As an additional cost to cast Bone Splinters, sacrifice a creature.\nDestroy target creature.').
card_image_name('bone splinters'/'DDN', 'bone splinters').
card_uid('bone splinters'/'DDN', 'DDN:Bone Splinters:bone splinters').
card_rarity('bone splinters'/'DDN', 'Common').
card_artist('bone splinters'/'DDN', 'Cole Eastburn').
card_number('bone splinters'/'DDN', '22').
card_flavor_text('bone splinters'/'DDN', 'Witches of the Split-Eye Coven speak of a future when Grixis will overflow with life energy. For now, they must harvest vis from the living to fuel their dark magics.').
card_multiverse_id('bone splinters'/'DDN', '386306').

card_in_set('coral trickster', 'DDN').
card_original_type('coral trickster'/'DDN', 'Creature — Merfolk Rogue').
card_original_text('coral trickster'/'DDN', 'Morph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Coral Trickster is turned face up, you may tap or untap target permanent.').
card_image_name('coral trickster'/'DDN', 'coral trickster').
card_uid('coral trickster'/'DDN', 'DDN:Coral Trickster:coral trickster').
card_rarity('coral trickster'/'DDN', 'Common').
card_artist('coral trickster'/'DDN', 'D. Alexander Gregory').
card_number('coral trickster'/'DDN', '44').
card_flavor_text('coral trickster'/'DDN', 'They wait in darkened depths, laughing eagerly.').
card_multiverse_id('coral trickster'/'DDN', '386307').

card_in_set('dauntless onslaught', 'DDN').
card_original_type('dauntless onslaught'/'DDN', 'Instant').
card_original_text('dauntless onslaught'/'DDN', 'Up to two target creatures each get +2/+2 until end of turn.').
card_image_name('dauntless onslaught'/'DDN', 'dauntless onslaught').
card_uid('dauntless onslaught'/'DDN', 'DDN:Dauntless Onslaught:dauntless onslaught').
card_rarity('dauntless onslaught'/'DDN', 'Uncommon').
card_artist('dauntless onslaught'/'DDN', 'Peter Mohrbacher').
card_number('dauntless onslaught'/'DDN', '27').
card_flavor_text('dauntless onslaught'/'DDN', '\"The people of Akros must learn from our leonin adversaries. If we match their staunch ferocity with our superior faith, we cannot fail.\"\n—Cymede, queen of Akros').
card_multiverse_id('dauntless onslaught'/'DDN', '386308').

card_in_set('dregscape zombie', 'DDN').
card_original_type('dregscape zombie'/'DDN', 'Creature — Zombie').
card_original_text('dregscape zombie'/'DDN', 'Unearth {B} ({B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('dregscape zombie'/'DDN', 'dregscape zombie').
card_uid('dregscape zombie'/'DDN', 'DDN:Dregscape Zombie:dregscape zombie').
card_rarity('dregscape zombie'/'DDN', 'Common').
card_artist('dregscape zombie'/'DDN', 'Lars Grant-West').
card_number('dregscape zombie'/'DDN', '5').
card_flavor_text('dregscape zombie'/'DDN', 'The undead of Grixis are fueled by their hatred of the living.').
card_multiverse_id('dregscape zombie'/'DDN', '386309').

card_in_set('echo tracer', 'DDN').
card_original_type('echo tracer'/'DDN', 'Creature — Human Wizard').
card_original_text('echo tracer'/'DDN', 'Morph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Echo Tracer is turned face up, return target creature to its owner\'s hand.').
card_image_name('echo tracer'/'DDN', 'echo tracer').
card_uid('echo tracer'/'DDN', 'DDN:Echo Tracer:echo tracer').
card_rarity('echo tracer'/'DDN', 'Common').
card_artist('echo tracer'/'DDN', 'Scott M. Fischer').
card_number('echo tracer'/'DDN', '51').
card_multiverse_id('echo tracer'/'DDN', '386310').

card_in_set('evolving wilds', 'DDN').
card_original_type('evolving wilds'/'DDN', 'Land').
card_original_text('evolving wilds'/'DDN', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'DDN', 'evolving wilds').
card_uid('evolving wilds'/'DDN', 'DDN:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'DDN', 'Common').
card_artist('evolving wilds'/'DDN', 'Steven Belledin').
card_number('evolving wilds'/'DDN', '32').
card_flavor_text('evolving wilds'/'DDN', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'DDN', '386311').

card_in_set('faerie impostor', 'DDN').
card_original_type('faerie impostor'/'DDN', 'Creature — Faerie Rogue').
card_original_text('faerie impostor'/'DDN', 'Flying\nWhen Faerie Impostor enters the battlefield, sacrifice it unless you return another creature you control to its owner\'s hand.').
card_image_name('faerie impostor'/'DDN', 'faerie impostor').
card_uid('faerie impostor'/'DDN', 'DDN:Faerie Impostor:faerie impostor').
card_rarity('faerie impostor'/'DDN', 'Uncommon').
card_artist('faerie impostor'/'DDN', 'Johann Bodin').
card_number('faerie impostor'/'DDN', '43').
card_flavor_text('faerie impostor'/'DDN', 'Many Tin Street shops display a sign on the door: \"No cloaks allowed.\"').
card_multiverse_id('faerie impostor'/'DDN', '386312').

card_in_set('faerie invaders', 'DDN').
card_original_type('faerie invaders'/'DDN', 'Creature — Faerie Rogue').
card_original_text('faerie invaders'/'DDN', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_image_name('faerie invaders'/'DDN', 'faerie invaders').
card_uid('faerie invaders'/'DDN', 'DDN:Faerie Invaders:faerie invaders').
card_rarity('faerie invaders'/'DDN', 'Common').
card_artist('faerie invaders'/'DDN', 'Ryan Pancoast').
card_number('faerie invaders'/'DDN', '57').
card_flavor_text('faerie invaders'/'DDN', 'Small enough to penetrate the narrowest crack in a castle wall and numerous enough to hack apart a griffin.').
card_multiverse_id('faerie invaders'/'DDN', '386313').

card_in_set('fathom seer', 'DDN').
card_original_type('fathom seer'/'DDN', 'Creature — Illusion').
card_original_text('fathom seer'/'DDN', 'Morph—Return two Islands you control to their owner\'s hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Fathom Seer is turned face up, draw two cards.').
card_image_name('fathom seer'/'DDN', 'fathom seer').
card_uid('fathom seer'/'DDN', 'DDN:Fathom Seer:fathom seer').
card_rarity('fathom seer'/'DDN', 'Common').
card_artist('fathom seer'/'DDN', 'Ralph Horsley').
card_number('fathom seer'/'DDN', '45').
card_multiverse_id('fathom seer'/'DDN', '386314').

card_in_set('fiery fall', 'DDN').
card_original_type('fiery fall'/'DDN', 'Instant').
card_original_text('fiery fall'/'DDN', 'Fiery Fall deals 5 damage to target creature.\nBasic landcycling {1}{R} ({1}{R}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('fiery fall'/'DDN', 'fiery fall').
card_uid('fiery fall'/'DDN', 'DDN:Fiery Fall:fiery fall').
card_rarity('fiery fall'/'DDN', 'Common').
card_artist('fiery fall'/'DDN', 'Daarken').
card_number('fiery fall'/'DDN', '29').
card_flavor_text('fiery fall'/'DDN', 'Jund feasts on the unprepared.').
card_multiverse_id('fiery fall'/'DDN', '386315').

card_in_set('flame-kin zealot', 'DDN').
card_original_type('flame-kin zealot'/'DDN', 'Creature — Elemental Berserker').
card_original_text('flame-kin zealot'/'DDN', 'When Flame-Kin Zealot enters the battlefield, creatures you control get +1/+1 and gain haste until end of turn.').
card_image_name('flame-kin zealot'/'DDN', 'flame-kin zealot').
card_uid('flame-kin zealot'/'DDN', 'DDN:Flame-Kin Zealot:flame-kin zealot').
card_rarity('flame-kin zealot'/'DDN', 'Uncommon').
card_artist('flame-kin zealot'/'DDN', 'Arnie Swekel').
card_number('flame-kin zealot'/'DDN', '17').
card_flavor_text('flame-kin zealot'/'DDN', 'Boros soldiers are like a cache of bombs ready to explode, and the flame-kin light their fuses.').
card_multiverse_id('flame-kin zealot'/'DDN', '386316').

card_in_set('fleeting distraction', 'DDN').
card_original_type('fleeting distraction'/'DDN', 'Instant').
card_original_text('fleeting distraction'/'DDN', 'Target creature gets -1/-0 until end of turn.\nDraw a card.').
card_image_name('fleeting distraction'/'DDN', 'fleeting distraction').
card_uid('fleeting distraction'/'DDN', 'DDN:Fleeting Distraction:fleeting distraction').
card_rarity('fleeting distraction'/'DDN', 'Common').
card_artist('fleeting distraction'/'DDN', 'Ryan Yee').
card_number('fleeting distraction'/'DDN', '60').
card_flavor_text('fleeting distraction'/'DDN', 'The first spell taught to young mages, it has spared the lives of many whose hands are too small to hold a sword.').
card_multiverse_id('fleeting distraction'/'DDN', '386317').

card_in_set('fleshbag marauder', 'DDN').
card_original_type('fleshbag marauder'/'DDN', 'Creature — Zombie Warrior').
card_original_text('fleshbag marauder'/'DDN', 'When Fleshbag Marauder enters the battlefield, each player sacrifices a creature.').
card_image_name('fleshbag marauder'/'DDN', 'fleshbag marauder').
card_uid('fleshbag marauder'/'DDN', 'DDN:Fleshbag Marauder:fleshbag marauder').
card_rarity('fleshbag marauder'/'DDN', 'Uncommon').
card_artist('fleshbag marauder'/'DDN', 'Mark Zug').
card_number('fleshbag marauder'/'DDN', '8').
card_flavor_text('fleshbag marauder'/'DDN', 'Grixis is a world where the only things found in abundance are death and decay. Corpses, whole or in part, are the standard currency among necromancers and demons.').
card_multiverse_id('fleshbag marauder'/'DDN', '386318').

card_in_set('frenzied goblin', 'DDN').
card_original_type('frenzied goblin'/'DDN', 'Creature — Goblin Berserker').
card_original_text('frenzied goblin'/'DDN', 'Whenever Frenzied Goblin attacks, you may pay {R}. If you do, target creature can\'t block this turn.').
card_image_name('frenzied goblin'/'DDN', 'frenzied goblin').
card_uid('frenzied goblin'/'DDN', 'DDN:Frenzied Goblin:frenzied goblin').
card_rarity('frenzied goblin'/'DDN', 'Uncommon').
card_artist('frenzied goblin'/'DDN', 'Carl Critchlow').
card_number('frenzied goblin'/'DDN', '2').
card_flavor_text('frenzied goblin'/'DDN', 'The upside to not thinking about the consequences is that you\'ll always surprise those who do.').
card_multiverse_id('frenzied goblin'/'DDN', '386319').

card_in_set('fury of the horde', 'DDN').
card_original_type('fury of the horde'/'DDN', 'Sorcery').
card_original_text('fury of the horde'/'DDN', 'You may exile two red cards from your hand rather than pay Fury of the Horde\'s mana cost.\nUntap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_image_name('fury of the horde'/'DDN', 'fury of the horde').
card_uid('fury of the horde'/'DDN', 'DDN:Fury of the Horde:fury of the horde').
card_rarity('fury of the horde'/'DDN', 'Rare').
card_artist('fury of the horde'/'DDN', 'Stephen Tappin').
card_number('fury of the horde'/'DDN', '30').
card_multiverse_id('fury of the horde'/'DDN', '386320').

card_in_set('ghitu encampment', 'DDN').
card_original_type('ghitu encampment'/'DDN', 'Land').
card_original_text('ghitu encampment'/'DDN', 'Ghitu Encampment enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{1}{R}: Ghitu Encampment becomes a 2/1 red Warrior creature with first strike until end of turn. It\'s still a land.').
card_image_name('ghitu encampment'/'DDN', 'ghitu encampment').
card_uid('ghitu encampment'/'DDN', 'DDN:Ghitu Encampment:ghitu encampment').
card_rarity('ghitu encampment'/'DDN', 'Uncommon').
card_artist('ghitu encampment'/'DDN', 'John Avon').
card_number('ghitu encampment'/'DDN', '33').
card_multiverse_id('ghitu encampment'/'DDN', '386321').

card_in_set('goblin', 'DDN').
card_original_type('goblin'/'DDN', 'Token Creature — Goblin').
card_original_text('goblin'/'DDN', '').
card_image_name('goblin'/'DDN', 'goblin').
card_uid('goblin'/'DDN', 'DDN:Goblin:goblin').
card_rarity('goblin'/'DDN', 'Common').
card_artist('goblin'/'DDN', 'Karl Kopinski').
card_number('goblin'/'DDN', '82').
card_multiverse_id('goblin'/'DDN', '386322').

card_in_set('goblin bombardment', 'DDN').
card_original_type('goblin bombardment'/'DDN', 'Enchantment').
card_original_text('goblin bombardment'/'DDN', 'Sacrifice a creature: Goblin Bombardment deals 1 damage to target creature or player.').
card_image_name('goblin bombardment'/'DDN', 'goblin bombardment').
card_uid('goblin bombardment'/'DDN', 'DDN:Goblin Bombardment:goblin bombardment').
card_rarity('goblin bombardment'/'DDN', 'Uncommon').
card_artist('goblin bombardment'/'DDN', 'Dave Kendall').
card_number('goblin bombardment'/'DDN', '24').
card_flavor_text('goblin bombardment'/'DDN', 'With one motion, a pest is removed and a scourge inflicted.').
card_multiverse_id('goblin bombardment'/'DDN', '386323').

card_in_set('goblin deathraiders', 'DDN').
card_original_type('goblin deathraiders'/'DDN', 'Creature — Goblin Warrior').
card_original_text('goblin deathraiders'/'DDN', 'Trample').
card_image_name('goblin deathraiders'/'DDN', 'goblin deathraiders').
card_uid('goblin deathraiders'/'DDN', 'DDN:Goblin Deathraiders:goblin deathraiders').
card_rarity('goblin deathraiders'/'DDN', 'Common').
card_artist('goblin deathraiders'/'DDN', 'Raymond Swanland').
card_number('goblin deathraiders'/'DDN', '6').
card_flavor_text('goblin deathraiders'/'DDN', 'Every once in a while, when they aren\'t getting incinerated in lava, crushed under rock slides, or devoured by dragons, goblins experience moments of unmitigated glory in battle.').
card_multiverse_id('goblin deathraiders'/'DDN', '386324').

card_in_set('goblin warchief', 'DDN').
card_original_type('goblin warchief'/'DDN', 'Creature — Goblin Warrior').
card_original_text('goblin warchief'/'DDN', 'Goblin spells you cast cost {1} less to cast.\nGoblin creatures you control have haste.').
card_image_name('goblin warchief'/'DDN', 'goblin warchief').
card_uid('goblin warchief'/'DDN', 'DDN:Goblin Warchief:goblin warchief').
card_rarity('goblin warchief'/'DDN', 'Uncommon').
card_artist('goblin warchief'/'DDN', 'Greg & Tim Hildebrandt').
card_number('goblin warchief'/'DDN', '9').
card_flavor_text('goblin warchief'/'DDN', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').
card_multiverse_id('goblin warchief'/'DDN', '386325').

card_in_set('hell\'s thunder', 'DDN').
card_original_type('hell\'s thunder'/'DDN', 'Creature — Elemental').
card_original_text('hell\'s thunder'/'DDN', 'Flying, haste\nAt the beginning of the end step, sacrifice Hell\'s Thunder.\nUnearth {4}{R} ({4}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('hell\'s thunder'/'DDN', 'hell\'s thunder').
card_uid('hell\'s thunder'/'DDN', 'DDN:Hell\'s Thunder:hell\'s thunder').
card_rarity('hell\'s thunder'/'DDN', 'Rare').
card_artist('hell\'s thunder'/'DDN', 'Karl Kopinski').
card_number('hell\'s thunder'/'DDN', '10').
card_multiverse_id('hell\'s thunder'/'DDN', '386326').

card_in_set('hellraiser goblin', 'DDN').
card_original_type('hellraiser goblin'/'DDN', 'Creature — Goblin Berserker').
card_original_text('hellraiser goblin'/'DDN', 'Creatures you control have haste and attack each combat if able.').
card_image_name('hellraiser goblin'/'DDN', 'hellraiser goblin').
card_uid('hellraiser goblin'/'DDN', 'DDN:Hellraiser Goblin:hellraiser goblin').
card_rarity('hellraiser goblin'/'DDN', 'Uncommon').
card_artist('hellraiser goblin'/'DDN', 'Karl Kopinski').
card_number('hellraiser goblin'/'DDN', '7').
card_flavor_text('hellraiser goblin'/'DDN', 'Don\'t let him lead the soldiers, but by all means let him lead the way.').
card_multiverse_id('hellraiser goblin'/'DDN', '386327').

card_in_set('hold the line', 'DDN').
card_original_type('hold the line'/'DDN', 'Instant').
card_original_text('hold the line'/'DDN', 'Blocking creatures get +7/+7 until end of turn.').
card_image_name('hold the line'/'DDN', 'hold the line').
card_uid('hold the line'/'DDN', 'DDN:Hold the Line:hold the line').
card_rarity('hold the line'/'DDN', 'Rare').
card_artist('hold the line'/'DDN', 'Ron Spears').
card_number('hold the line'/'DDN', '66').
card_flavor_text('hold the line'/'DDN', '\"Forgive me, Master Kami, but in the interest of my people I must halt your advance.\"').
card_multiverse_id('hold the line'/'DDN', '386328').

card_in_set('hussar patrol', 'DDN').
card_original_type('hussar patrol'/'DDN', 'Creature — Human Knight').
card_original_text('hussar patrol'/'DDN', 'Flash (You may cast this spell any time you could cast an instant.)\nVigilance').
card_image_name('hussar patrol'/'DDN', 'hussar patrol').
card_uid('hussar patrol'/'DDN', 'DDN:Hussar Patrol:hussar patrol').
card_rarity('hussar patrol'/'DDN', 'Common').
card_artist('hussar patrol'/'DDN', 'Seb McKinnon').
card_number('hussar patrol'/'DDN', '55').
card_flavor_text('hussar patrol'/'DDN', '\"You think no one is watching, you think you\'re smart enough to escape, and most foolish of all, you think no one cares.\"\n—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('hussar patrol'/'DDN', '386329').

card_in_set('impulse', 'DDN').
card_original_type('impulse'/'DDN', 'Instant').
card_original_text('impulse'/'DDN', 'Look at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_image_name('impulse'/'DDN', 'impulse').
card_uid('impulse'/'DDN', 'DDN:Impulse:impulse').
card_rarity('impulse'/'DDN', 'Common').
card_artist('impulse'/'DDN', 'Izzy').
card_number('impulse'/'DDN', '63').
card_flavor_text('impulse'/'DDN', '\"You call it luck. I call it preparation.\"').
card_multiverse_id('impulse'/'DDN', '386330').

card_in_set('infantry veteran', 'DDN').
card_original_type('infantry veteran'/'DDN', 'Creature — Human Soldier').
card_original_text('infantry veteran'/'DDN', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_image_name('infantry veteran'/'DDN', 'infantry veteran').
card_uid('infantry veteran'/'DDN', 'DDN:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'DDN', 'Common').
card_artist('infantry veteran'/'DDN', 'Zoltan Boros').
card_number('infantry veteran'/'DDN', '3').
card_flavor_text('infantry veteran'/'DDN', '\"Swords, to me! We must engage their line to protect our archers and mages. Let no one fail this task!\"').
card_multiverse_id('infantry veteran'/'DDN', '386331').

card_in_set('inferno trap', 'DDN').
card_original_type('inferno trap'/'DDN', 'Instant — Trap').
card_original_text('inferno trap'/'DDN', 'If you\'ve been dealt damage by two or more creatures this turn, you may pay {R} rather than pay Inferno Trap\'s mana cost.\nInferno Trap deals 4 damage to target creature.').
card_image_name('inferno trap'/'DDN', 'inferno trap').
card_uid('inferno trap'/'DDN', 'DDN:Inferno Trap:inferno trap').
card_rarity('inferno trap'/'DDN', 'Uncommon').
card_artist('inferno trap'/'DDN', 'Philip Straub').
card_number('inferno trap'/'DDN', '67').
card_flavor_text('inferno trap'/'DDN', '\"Do you smell something burning?\"').
card_multiverse_id('inferno trap'/'DDN', '386332').

card_in_set('island', 'DDN').
card_original_type('island'/'DDN', 'Basic Land — Island').
card_original_text('island'/'DDN', 'U').
card_image_name('island'/'DDN', 'island1').
card_uid('island'/'DDN', 'DDN:Island:island1').
card_rarity('island'/'DDN', 'Basic Land').
card_artist('island'/'DDN', 'Rob Alexander').
card_number('island'/'DDN', '75').
card_multiverse_id('island'/'DDN', '386333').

card_in_set('island', 'DDN').
card_original_type('island'/'DDN', 'Basic Land — Island').
card_original_text('island'/'DDN', 'U').
card_image_name('island'/'DDN', 'island2').
card_uid('island'/'DDN', 'DDN:Island:island2').
card_rarity('island'/'DDN', 'Basic Land').
card_artist('island'/'DDN', 'Rob Alexander').
card_number('island'/'DDN', '76').
card_multiverse_id('island'/'DDN', '386335').

card_in_set('island', 'DDN').
card_original_type('island'/'DDN', 'Basic Land — Island').
card_original_text('island'/'DDN', 'U').
card_image_name('island'/'DDN', 'island3').
card_uid('island'/'DDN', 'DDN:Island:island3').
card_rarity('island'/'DDN', 'Basic Land').
card_artist('island'/'DDN', 'Andreas Rocha').
card_number('island'/'DDN', '77').
card_multiverse_id('island'/'DDN', '386334').

card_in_set('jeskai elder', 'DDN').
card_original_type('jeskai elder'/'DDN', 'Creature — Human Monk').
card_original_text('jeskai elder'/'DDN', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever Jeskai Elder deals combat damage to a player, you may draw a card. If you do, discard a card.').
card_first_print('jeskai elder', 'DDN').
card_image_name('jeskai elder'/'DDN', 'jeskai elder').
card_uid('jeskai elder'/'DDN', 'DDN:Jeskai Elder:jeskai elder').
card_rarity('jeskai elder'/'DDN', 'Uncommon').
card_artist('jeskai elder'/'DDN', 'Craig J Spearing').
card_number('jeskai elder'/'DDN', '46').
card_multiverse_id('jeskai elder'/'DDN', '386336').

card_in_set('kathari bomber', 'DDN').
card_original_type('kathari bomber'/'DDN', 'Creature — Bird Shaman').
card_original_text('kathari bomber'/'DDN', 'Flying\nWhen Kathari Bomber deals combat damage to a player, put two 1/1 red Goblin creature tokens onto the battlefield and sacrifice Kathari Bomber.\nUnearth {3}{B}{R} ({3}{B}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('kathari bomber'/'DDN', 'kathari bomber').
card_uid('kathari bomber'/'DDN', 'DDN:Kathari Bomber:kathari bomber').
card_rarity('kathari bomber'/'DDN', 'Common').
card_artist('kathari bomber'/'DDN', 'Carl Critchlow').
card_number('kathari bomber'/'DDN', '11').
card_multiverse_id('kathari bomber'/'DDN', '386337').

card_in_set('kor hookmaster', 'DDN').
card_original_type('kor hookmaster'/'DDN', 'Creature — Kor Soldier').
card_original_text('kor hookmaster'/'DDN', 'When Kor Hookmaster enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('kor hookmaster'/'DDN', 'kor hookmaster').
card_uid('kor hookmaster'/'DDN', 'DDN:Kor Hookmaster:kor hookmaster').
card_rarity('kor hookmaster'/'DDN', 'Common').
card_artist('kor hookmaster'/'DDN', 'Wayne Reynolds').
card_number('kor hookmaster'/'DDN', '52').
card_flavor_text('kor hookmaster'/'DDN', '\"For us, a rope represents the ties that bind the kor. For you, it\'s more literal.\"').
card_multiverse_id('kor hookmaster'/'DDN', '386338').

card_in_set('krenko\'s command', 'DDN').
card_original_type('krenko\'s command'/'DDN', 'Sorcery').
card_original_text('krenko\'s command'/'DDN', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('krenko\'s command'/'DDN', 'krenko\'s command').
card_uid('krenko\'s command'/'DDN', 'DDN:Krenko\'s Command:krenko\'s command').
card_rarity('krenko\'s command'/'DDN', 'Common').
card_artist('krenko\'s command'/'DDN', 'Karl Kopinski').
card_number('krenko\'s command'/'DDN', '25').
card_flavor_text('krenko\'s command'/'DDN', 'Goblins are eager to follow orders, especially when those orders involve stealing, hurting, annoying, eating, destroying, or swearing.').
card_multiverse_id('krenko\'s command'/'DDN', '386340').

card_in_set('krenko, mob boss', 'DDN').
card_original_type('krenko, mob boss'/'DDN', 'Legendary Creature — Goblin Warrior').
card_original_text('krenko, mob boss'/'DDN', '{T}: Put X 1/1 red Goblin creature tokens onto the battlefield, where X is the number of Goblins you control.').
card_image_name('krenko, mob boss'/'DDN', 'krenko, mob boss').
card_uid('krenko, mob boss'/'DDN', 'DDN:Krenko, Mob Boss:krenko, mob boss').
card_rarity('krenko, mob boss'/'DDN', 'Rare').
card_artist('krenko, mob boss'/'DDN', 'Karl Kopinski').
card_number('krenko, mob boss'/'DDN', '15').
card_flavor_text('krenko, mob boss'/'DDN', '\"He displays a perverse charisma fueled by avarice. Highly dangerous. Recommend civil sanctions.\"\n—Agmand Sarv, Azorius hussar').
card_multiverse_id('krenko, mob boss'/'DDN', '386339').

card_in_set('leonin snarecaster', 'DDN').
card_original_type('leonin snarecaster'/'DDN', 'Creature — Cat Soldier').
card_original_text('leonin snarecaster'/'DDN', 'When Leonin Snarecaster enters the battlefield, you may tap target creature.').
card_image_name('leonin snarecaster'/'DDN', 'leonin snarecaster').
card_uid('leonin snarecaster'/'DDN', 'DDN:Leonin Snarecaster:leonin snarecaster').
card_rarity('leonin snarecaster'/'DDN', 'Common').
card_artist('leonin snarecaster'/'DDN', 'Kev Walker').
card_number('leonin snarecaster'/'DDN', '4').
card_flavor_text('leonin snarecaster'/'DDN', 'Formerly oppressed by the polis of Meletis, leonin occasionally \"mistake\" their old enemies for game.').
card_multiverse_id('leonin snarecaster'/'DDN', '386341').

card_in_set('lightning angel', 'DDN').
card_original_type('lightning angel'/'DDN', 'Creature — Angel').
card_original_text('lightning angel'/'DDN', 'Flying, vigilance, haste').
card_image_name('lightning angel'/'DDN', 'lightning angel').
card_uid('lightning angel'/'DDN', 'DDN:Lightning Angel:lightning angel').
card_rarity('lightning angel'/'DDN', 'Rare').
card_artist('lightning angel'/'DDN', 'rk post').
card_number('lightning angel'/'DDN', '56').
card_flavor_text('lightning angel'/'DDN', 'Sudden vengeance, echoing fury.').
card_multiverse_id('lightning angel'/'DDN', '386342').

card_in_set('lightning helix', 'DDN').
card_original_type('lightning helix'/'DDN', 'Instant').
card_original_text('lightning helix'/'DDN', 'Lightning Helix deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('lightning helix'/'DDN', 'lightning helix').
card_uid('lightning helix'/'DDN', 'DDN:Lightning Helix:lightning helix').
card_rarity('lightning helix'/'DDN', 'Uncommon').
card_artist('lightning helix'/'DDN', 'Kev Walker').
card_number('lightning helix'/'DDN', '65').
card_flavor_text('lightning helix'/'DDN', 'Though less well-known than its army of soldiers, the Boros Legion\'s mage-priests are as respected by the innocent as they are hated by the ghosts of the guilty.').
card_multiverse_id('lightning helix'/'DDN', '386343').

card_in_set('lone missionary', 'DDN').
card_original_type('lone missionary'/'DDN', 'Creature — Kor Monk').
card_original_text('lone missionary'/'DDN', 'When Lone Missionary enters the battlefield, you gain 4 life.').
card_image_name('lone missionary'/'DDN', 'lone missionary').
card_uid('lone missionary'/'DDN', 'DDN:Lone Missionary:lone missionary').
card_rarity('lone missionary'/'DDN', 'Common').
card_artist('lone missionary'/'DDN', 'Svetlin Velinov').
card_number('lone missionary'/'DDN', '49').
card_flavor_text('lone missionary'/'DDN', 'His mission has become a grim pilgrimage, a tour of the Eldrazi-stricken outposts across Zendikar. But he marches on alone, stubborn as the daily dawn.').
card_multiverse_id('lone missionary'/'DDN', '386344').

card_in_set('mana leak', 'DDN').
card_original_type('mana leak'/'DDN', 'Instant').
card_original_text('mana leak'/'DDN', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'DDN', 'mana leak').
card_uid('mana leak'/'DDN', 'DDN:Mana Leak:mana leak').
card_rarity('mana leak'/'DDN', 'Common').
card_artist('mana leak'/'DDN', 'Howard Lyon').
card_number('mana leak'/'DDN', '64').
card_flavor_text('mana leak'/'DDN', 'The fatal flaw in every plan is the assumption that you know more than your enemy.').
card_multiverse_id('mana leak'/'DDN', '386345').

card_in_set('mardu heart-piercer', 'DDN').
card_original_type('mardu heart-piercer'/'DDN', 'Creature — Human Archer').
card_original_text('mardu heart-piercer'/'DDN', 'Raid — When Mardu Heart-Piercer enters the battlefield, if you attacked with a creature this turn, Mardu Heart-Piercer deals 2 damage to target creature or player.').
card_first_print('mardu heart-piercer', 'DDN').
card_image_name('mardu heart-piercer'/'DDN', 'mardu heart-piercer').
card_uid('mardu heart-piercer'/'DDN', 'DDN:Mardu Heart-Piercer:mardu heart-piercer').
card_rarity('mardu heart-piercer'/'DDN', 'Uncommon').
card_artist('mardu heart-piercer'/'DDN', 'Karl Kopinski').
card_number('mardu heart-piercer'/'DDN', '13').
card_flavor_text('mardu heart-piercer'/'DDN', '\"Those who have never ridden before the wind do not know the true joy of war.\"').
card_multiverse_id('mardu heart-piercer'/'DDN', '386346').

card_in_set('master decoy', 'DDN').
card_original_type('master decoy'/'DDN', 'Creature — Human Soldier').
card_original_text('master decoy'/'DDN', '{W}, {T}: Tap target creature.').
card_image_name('master decoy'/'DDN', 'master decoy').
card_uid('master decoy'/'DDN', 'DDN:Master Decoy:master decoy').
card_rarity('master decoy'/'DDN', 'Common').
card_artist('master decoy'/'DDN', 'Ben Thompson').
card_number('master decoy'/'DDN', '50').
card_flavor_text('master decoy'/'DDN', 'A skilled decoy can throw your enemies off your trail. A master decoy can survive to do it again.').
card_multiverse_id('master decoy'/'DDN', '386347').

card_in_set('mountain', 'DDN').
card_original_type('mountain'/'DDN', 'Basic Land — Mountain').
card_original_text('mountain'/'DDN', 'R').
card_image_name('mountain'/'DDN', 'mountain1').
card_uid('mountain'/'DDN', 'DDN:Mountain:mountain1').
card_rarity('mountain'/'DDN', 'Basic Land').
card_artist('mountain'/'DDN', 'Jonas De Ro').
card_number('mountain'/'DDN', '35').
card_multiverse_id('mountain'/'DDN', '386348').

card_in_set('mountain', 'DDN').
card_original_type('mountain'/'DDN', 'Basic Land — Mountain').
card_original_text('mountain'/'DDN', 'R').
card_image_name('mountain'/'DDN', 'mountain2').
card_uid('mountain'/'DDN', 'DDN:Mountain:mountain2').
card_rarity('mountain'/'DDN', 'Basic Land').
card_artist('mountain'/'DDN', 'Karl Kopinski').
card_number('mountain'/'DDN', '36').
card_multiverse_id('mountain'/'DDN', '386351').

card_in_set('mountain', 'DDN').
card_original_type('mountain'/'DDN', 'Basic Land — Mountain').
card_original_text('mountain'/'DDN', 'R').
card_image_name('mountain'/'DDN', 'mountain3').
card_uid('mountain'/'DDN', 'DDN:Mountain:mountain3').
card_rarity('mountain'/'DDN', 'Basic Land').
card_artist('mountain'/'DDN', 'Andreas Rocha').
card_number('mountain'/'DDN', '37').
card_multiverse_id('mountain'/'DDN', '386349').

card_in_set('mountain', 'DDN').
card_original_type('mountain'/'DDN', 'Basic Land — Mountain').
card_original_text('mountain'/'DDN', 'R').
card_image_name('mountain'/'DDN', 'mountain4').
card_uid('mountain'/'DDN', 'DDN:Mountain:mountain4').
card_rarity('mountain'/'DDN', 'Basic Land').
card_artist('mountain'/'DDN', 'Raoul Vitale').
card_number('mountain'/'DDN', '78').
card_multiverse_id('mountain'/'DDN', '386350').

card_in_set('mystic monastery', 'DDN').
card_original_type('mystic monastery'/'DDN', 'Land').
card_original_text('mystic monastery'/'DDN', 'Mystic Monastery enters the battlefield tapped.\n{T}: Add {U}, {R}, or {W} to your mana pool.').
card_first_print('mystic monastery', 'DDN').
card_image_name('mystic monastery'/'DDN', 'mystic monastery').
card_uid('mystic monastery'/'DDN', 'DDN:Mystic Monastery:mystic monastery').
card_rarity('mystic monastery'/'DDN', 'Uncommon').
card_artist('mystic monastery'/'DDN', 'Florian de Gesincourt').
card_number('mystic monastery'/'DDN', '73').
card_flavor_text('mystic monastery'/'DDN', 'When asked how many paths reach enlightenment, the monk kicked a heap of sand. \"Count,\" he smiled, \"and then find more grains.\"').
card_multiverse_id('mystic monastery'/'DDN', '386352').

card_in_set('nomad outpost', 'DDN').
card_original_type('nomad outpost'/'DDN', 'Land').
card_original_text('nomad outpost'/'DDN', 'Nomad Outpost enters the battlefield tapped.\n{T}: Add {R}, {W}, or {B} to your mana pool.').
card_first_print('nomad outpost', 'DDN').
card_image_name('nomad outpost'/'DDN', 'nomad outpost').
card_uid('nomad outpost'/'DDN', 'DDN:Nomad Outpost:nomad outpost').
card_rarity('nomad outpost'/'DDN', 'Uncommon').
card_artist('nomad outpost'/'DDN', 'Noah Bradley').
card_number('nomad outpost'/'DDN', '34').
card_flavor_text('nomad outpost'/'DDN', '\"Only the weak imprison themselves behind walls. We live free under the wind, and our freedom makes us strong.\"\n—Zurgo, khan of the Mardu').
card_multiverse_id('nomad outpost'/'DDN', '386353').

card_in_set('ogre battledriver', 'DDN').
card_original_type('ogre battledriver'/'DDN', 'Creature — Ogre Warrior').
card_original_text('ogre battledriver'/'DDN', 'Whenever another creature enters the battlefield under your control, that creature gets +2/+0 and gains haste until end of turn.').
card_image_name('ogre battledriver'/'DDN', 'ogre battledriver').
card_uid('ogre battledriver'/'DDN', 'DDN:Ogre Battledriver:ogre battledriver').
card_rarity('ogre battledriver'/'DDN', 'Rare').
card_artist('ogre battledriver'/'DDN', 'Greg Staples').
card_number('ogre battledriver'/'DDN', '16').
card_flavor_text('ogre battledriver'/'DDN', 'Ogres are driven by passion, rage, and another ogre standing behind them with a whip.').
card_multiverse_id('ogre battledriver'/'DDN', '386354').

card_in_set('oni of wild places', 'DDN').
card_original_type('oni of wild places'/'DDN', 'Creature — Demon Spirit').
card_original_text('oni of wild places'/'DDN', 'Haste\nAt the beginning of your upkeep, return a red creature you control to its owner\'s hand.').
card_image_name('oni of wild places'/'DDN', 'oni of wild places').
card_uid('oni of wild places'/'DDN', 'DDN:Oni of Wild Places:oni of wild places').
card_rarity('oni of wild places'/'DDN', 'Uncommon').
card_artist('oni of wild places'/'DDN', 'Mark Tedin').
card_number('oni of wild places'/'DDN', '19').
card_flavor_text('oni of wild places'/'DDN', 'The oni leapt easily from peak to peak, toying with its victims, its voice a purr from the rumbling depths of nightmare.').
card_multiverse_id('oni of wild places'/'DDN', '386355').

card_in_set('orcish cannonade', 'DDN').
card_original_type('orcish cannonade'/'DDN', 'Instant').
card_original_text('orcish cannonade'/'DDN', 'Orcish Cannonade deals 2 damage to target creature or player and 3 damage to you.\nDraw a card.').
card_image_name('orcish cannonade'/'DDN', 'orcish cannonade').
card_uid('orcish cannonade'/'DDN', 'DDN:Orcish Cannonade:orcish cannonade').
card_rarity('orcish cannonade'/'DDN', 'Common').
card_artist('orcish cannonade'/'DDN', 'Pete Venters').
card_number('orcish cannonade'/'DDN', '28').
card_flavor_text('orcish cannonade'/'DDN', '\"Crispy! Scarback! Load another volcano-ball.\"\n—Stumphobbler Thuj, orcish captain').
card_multiverse_id('orcish cannonade'/'DDN', '386356').

card_in_set('plains', 'DDN').
card_original_type('plains'/'DDN', 'Basic Land — Plains').
card_original_text('plains'/'DDN', 'W').
card_image_name('plains'/'DDN', 'plains1').
card_uid('plains'/'DDN', 'DDN:Plains:plains1').
card_rarity('plains'/'DDN', 'Basic Land').
card_artist('plains'/'DDN', 'Jonas De Ro').
card_number('plains'/'DDN', '38').
card_multiverse_id('plains'/'DDN', '386360').

card_in_set('plains', 'DDN').
card_original_type('plains'/'DDN', 'Basic Land — Plains').
card_original_text('plains'/'DDN', 'W').
card_image_name('plains'/'DDN', 'plains2').
card_uid('plains'/'DDN', 'DDN:Plains:plains2').
card_rarity('plains'/'DDN', 'Basic Land').
card_artist('plains'/'DDN', 'Adam Paquette').
card_number('plains'/'DDN', '79').
card_multiverse_id('plains'/'DDN', '386357').

card_in_set('plains', 'DDN').
card_original_type('plains'/'DDN', 'Basic Land — Plains').
card_original_text('plains'/'DDN', 'W').
card_image_name('plains'/'DDN', 'plains3').
card_uid('plains'/'DDN', 'DDN:Plains:plains3').
card_rarity('plains'/'DDN', 'Basic Land').
card_artist('plains'/'DDN', 'Jung Park').
card_number('plains'/'DDN', '80').
card_multiverse_id('plains'/'DDN', '386359').

card_in_set('plains', 'DDN').
card_original_type('plains'/'DDN', 'Basic Land — Plains').
card_original_text('plains'/'DDN', 'W').
card_image_name('plains'/'DDN', 'plains4').
card_uid('plains'/'DDN', 'DDN:Plains:plains4').
card_rarity('plains'/'DDN', 'Basic Land').
card_artist('plains'/'DDN', 'Andreas Rocha').
card_number('plains'/'DDN', '81').
card_multiverse_id('plains'/'DDN', '386358').

card_in_set('reckless abandon', 'DDN').
card_original_type('reckless abandon'/'DDN', 'Sorcery').
card_original_text('reckless abandon'/'DDN', 'As an additional cost to cast Reckless Abandon, sacrifice a creature.\nReckless Abandon deals 4 damage to target creature or player.').
card_image_name('reckless abandon'/'DDN', 'reckless abandon').
card_uid('reckless abandon'/'DDN', 'DDN:Reckless Abandon:reckless abandon').
card_rarity('reckless abandon'/'DDN', 'Common').
card_artist('reckless abandon'/'DDN', 'Ron Spears').
card_number('reckless abandon'/'DDN', '20').
card_flavor_text('reckless abandon'/'DDN', 'The climax of a warlord\'s career is always death.').
card_multiverse_id('reckless abandon'/'DDN', '386361').

card_in_set('repeal', 'DDN').
card_original_type('repeal'/'DDN', 'Instant').
card_original_text('repeal'/'DDN', 'Return target nonland permanent with converted mana cost X to its owner\'s hand.\nDraw a card.').
card_image_name('repeal'/'DDN', 'repeal').
card_uid('repeal'/'DDN', 'DDN:Repeal:repeal').
card_rarity('repeal'/'DDN', 'Common').
card_artist('repeal'/'DDN', 'Anthony Palumbo').
card_number('repeal'/'DDN', '72').
card_flavor_text('repeal'/'DDN', '\"Your deed cannot be undone. You, however, can be.\"\n—Agosto, Azorius imperator').
card_multiverse_id('repeal'/'DDN', '386362').

card_in_set('scourge devil', 'DDN').
card_original_type('scourge devil'/'DDN', 'Creature — Devil').
card_original_text('scourge devil'/'DDN', 'When Scourge Devil enters the battlefield, creatures you control get +1/+0 until end of turn.\nUnearth {2}{R} ({2}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('scourge devil'/'DDN', 'scourge devil').
card_uid('scourge devil'/'DDN', 'DDN:Scourge Devil:scourge devil').
card_rarity('scourge devil'/'DDN', 'Uncommon').
card_artist('scourge devil'/'DDN', 'Dave Kendall').
card_number('scourge devil'/'DDN', '18').
card_multiverse_id('scourge devil'/'DDN', '386363').

card_in_set('shambling remains', 'DDN').
card_original_type('shambling remains'/'DDN', 'Creature — Zombie Horror').
card_original_text('shambling remains'/'DDN', 'Shambling Remains can\'t block.\nUnearth {B}{R} ({B}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('shambling remains'/'DDN', 'shambling remains').
card_uid('shambling remains'/'DDN', 'DDN:Shambling Remains:shambling remains').
card_rarity('shambling remains'/'DDN', 'Uncommon').
card_artist('shambling remains'/'DDN', 'Nils Hamm').
card_number('shambling remains'/'DDN', '12').
card_multiverse_id('shambling remains'/'DDN', '386364').

card_in_set('shock', 'DDN').
card_original_type('shock'/'DDN', 'Instant').
card_original_text('shock'/'DDN', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'DDN', 'shock').
card_uid('shock'/'DDN', 'DDN:Shock:shock').
card_rarity('shock'/'DDN', 'Common').
card_artist('shock'/'DDN', 'Jon Foster').
card_number('shock'/'DDN', '21').
card_flavor_text('shock'/'DDN', '\"The beauty of it is they never see it coming. Ever.\"\n—Razzix, sparkmage').
card_multiverse_id('shock'/'DDN', '386365').

card_in_set('sparkmage apprentice', 'DDN').
card_original_type('sparkmage apprentice'/'DDN', 'Creature — Human Wizard').
card_original_text('sparkmage apprentice'/'DDN', 'When Sparkmage Apprentice enters the battlefield, it deals 1 damage to target creature or player.').
card_image_name('sparkmage apprentice'/'DDN', 'sparkmage apprentice').
card_uid('sparkmage apprentice'/'DDN', 'DDN:Sparkmage Apprentice:sparkmage apprentice').
card_rarity('sparkmage apprentice'/'DDN', 'Common').
card_artist('sparkmage apprentice'/'DDN', 'Jaime Jones').
card_number('sparkmage apprentice'/'DDN', '48').
card_flavor_text('sparkmage apprentice'/'DDN', 'Sparkmages are performance artists of sorts, but pain is the price of admission.').
card_multiverse_id('sparkmage apprentice'/'DDN', '386366').

card_in_set('sphinx of uthuun', 'DDN').
card_original_type('sphinx of uthuun'/'DDN', 'Creature — Sphinx').
card_original_text('sphinx of uthuun'/'DDN', 'Flying\nWhen Sphinx of Uthuun enters the battlefield, reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('sphinx of uthuun'/'DDN', 'sphinx of uthuun').
card_uid('sphinx of uthuun'/'DDN', 'DDN:Sphinx of Uthuun:sphinx of uthuun').
card_rarity('sphinx of uthuun'/'DDN', 'Rare').
card_artist('sphinx of uthuun'/'DDN', 'Kekai Kotaki').
card_number('sphinx of uthuun'/'DDN', '59').
card_multiverse_id('sphinx of uthuun'/'DDN', '386367').

card_in_set('stave off', 'DDN').
card_original_type('stave off'/'DDN', 'Instant').
card_original_text('stave off'/'DDN', 'Target creature gains protection from the color of your choice until end of turn. (It can\'t be blocked, targeted, dealt damage, or enchanted by anything of that color.)').
card_image_name('stave off'/'DDN', 'stave off').
card_uid('stave off'/'DDN', 'DDN:Stave Off:stave off').
card_rarity('stave off'/'DDN', 'Common').
card_artist('stave off'/'DDN', 'Mark Zug').
card_number('stave off'/'DDN', '61').
card_multiverse_id('stave off'/'DDN', '386368').

card_in_set('steam augury', 'DDN').
card_original_type('steam augury'/'DDN', 'Instant').
card_original_text('steam augury'/'DDN', 'Reveal the top five cards of your library and separate them into two piles. An opponent chooses one of those piles. Put that pile into your hand and the other into your graveyard.').
card_image_name('steam augury'/'DDN', 'steam augury').
card_uid('steam augury'/'DDN', 'DDN:Steam Augury:steam augury').
card_rarity('steam augury'/'DDN', 'Rare').
card_artist('steam augury'/'DDN', 'Dave Kendall').
card_number('steam augury'/'DDN', '68').
card_flavor_text('steam augury'/'DDN', 'Keranos is a fickle god, delivering punishment as readily as prophecy.').
card_multiverse_id('steam augury'/'DDN', '386369').

card_in_set('stonecloaker', 'DDN').
card_original_type('stonecloaker'/'DDN', 'Creature — Gargoyle').
card_original_text('stonecloaker'/'DDN', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Stonecloaker enters the battlefield, return a creature you control to its owner\'s hand.\nWhen Stonecloaker enters the battlefield, exile target card from a graveyard.').
card_image_name('stonecloaker'/'DDN', 'stonecloaker').
card_uid('stonecloaker'/'DDN', 'DDN:Stonecloaker:stonecloaker').
card_rarity('stonecloaker'/'DDN', 'Uncommon').
card_artist('stonecloaker'/'DDN', 'Tomas Giorello').
card_number('stonecloaker'/'DDN', '53').
card_multiverse_id('stonecloaker'/'DDN', '386370').

card_in_set('swamp', 'DDN').
card_original_type('swamp'/'DDN', 'Basic Land — Swamp').
card_original_text('swamp'/'DDN', 'B').
card_image_name('swamp'/'DDN', 'swamp1').
card_uid('swamp'/'DDN', 'DDN:Swamp:swamp1').
card_rarity('swamp'/'DDN', 'Basic Land').
card_artist('swamp'/'DDN', 'Adam Paquette').
card_number('swamp'/'DDN', '39').
card_multiverse_id('swamp'/'DDN', '386372').

card_in_set('swamp', 'DDN').
card_original_type('swamp'/'DDN', 'Basic Land — Swamp').
card_original_text('swamp'/'DDN', 'B').
card_image_name('swamp'/'DDN', 'swamp2').
card_uid('swamp'/'DDN', 'DDN:Swamp:swamp2').
card_rarity('swamp'/'DDN', 'Basic Land').
card_artist('swamp'/'DDN', 'Jung Park').
card_number('swamp'/'DDN', '40').
card_multiverse_id('swamp'/'DDN', '386373').

card_in_set('swamp', 'DDN').
card_original_type('swamp'/'DDN', 'Basic Land — Swamp').
card_original_text('swamp'/'DDN', 'B').
card_image_name('swamp'/'DDN', 'swamp3').
card_uid('swamp'/'DDN', 'DDN:Swamp:swamp3').
card_rarity('swamp'/'DDN', 'Basic Land').
card_artist('swamp'/'DDN', 'Andreas Rocha').
card_number('swamp'/'DDN', '41').
card_multiverse_id('swamp'/'DDN', '386371').

card_in_set('swift justice', 'DDN').
card_original_type('swift justice'/'DDN', 'Instant').
card_original_text('swift justice'/'DDN', 'Until end of turn, target creature gets +1/+0 and gains first strike and lifelink.').
card_image_name('swift justice'/'DDN', 'swift justice').
card_uid('swift justice'/'DDN', 'DDN:Swift Justice:swift justice').
card_rarity('swift justice'/'DDN', 'Common').
card_artist('swift justice'/'DDN', 'Karl Kopinski').
card_number('swift justice'/'DDN', '62').
card_flavor_text('swift justice'/'DDN', '\"Having conviction is more important than being righteous.\"\n—Aurelia').
card_multiverse_id('swift justice'/'DDN', '386374').

card_in_set('terramorphic expanse', 'DDN').
card_original_type('terramorphic expanse'/'DDN', 'Land').
card_original_text('terramorphic expanse'/'DDN', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'DDN', 'terramorphic expanse').
card_uid('terramorphic expanse'/'DDN', 'DDN:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'DDN', 'Common').
card_artist('terramorphic expanse'/'DDN', 'Dan Scott').
card_number('terramorphic expanse'/'DDN', '74').
card_flavor_text('terramorphic expanse'/'DDN', 'Take two steps north into the unsettled future, south into the unquiet past, east into the present day, or west into the great unknown.').
card_multiverse_id('terramorphic expanse'/'DDN', '386375').

card_in_set('thousand winds', 'DDN').
card_original_type('thousand winds'/'DDN', 'Creature — Elemental').
card_original_text('thousand winds'/'DDN', 'Flying\nMorph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Thousand Winds is turned face up, return all other tapped creatures to their owners\' hands.').
card_image_name('thousand winds'/'DDN', 'thousand winds').
card_uid('thousand winds'/'DDN', 'DDN:Thousand Winds:thousand winds').
card_rarity('thousand winds'/'DDN', 'Rare').
card_artist('thousand winds'/'DDN', 'Raymond Swanland').
card_number('thousand winds'/'DDN', '58').
card_multiverse_id('thousand winds'/'DDN', '386376').

card_in_set('traumatic visions', 'DDN').
card_original_type('traumatic visions'/'DDN', 'Instant').
card_original_text('traumatic visions'/'DDN', 'Counter target spell.\nBasic landcycling {1}{U} ({1}{U}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('traumatic visions'/'DDN', 'traumatic visions').
card_uid('traumatic visions'/'DDN', 'DDN:Traumatic Visions:traumatic visions').
card_rarity('traumatic visions'/'DDN', 'Common').
card_artist('traumatic visions'/'DDN', 'Cyril Van Der Haegen').
card_number('traumatic visions'/'DDN', '69').
card_multiverse_id('traumatic visions'/'DDN', '386377').

card_in_set('whiplash trap', 'DDN').
card_original_type('whiplash trap'/'DDN', 'Instant — Trap').
card_original_text('whiplash trap'/'DDN', 'If an opponent had two or more creatures enter the battlefield under his or her control this turn, you may pay {U} rather than pay Whiplash Trap\'s mana cost.\nReturn two target creatures to their owners\' hands.').
card_image_name('whiplash trap'/'DDN', 'whiplash trap').
card_uid('whiplash trap'/'DDN', 'DDN:Whiplash Trap:whiplash trap').
card_rarity('whiplash trap'/'DDN', 'Common').
card_artist('whiplash trap'/'DDN', 'Zoltan Boros & Gabor Szikszai').
card_number('whiplash trap'/'DDN', '70').
card_multiverse_id('whiplash trap'/'DDN', '386378').

card_in_set('willbender', 'DDN').
card_original_type('willbender'/'DDN', 'Creature — Human Wizard').
card_original_text('willbender'/'DDN', 'Morph {1}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Willbender is turned face up, change the target of target spell or ability with a single target.').
card_image_name('willbender'/'DDN', 'willbender').
card_uid('willbender'/'DDN', 'DDN:Willbender:willbender').
card_rarity('willbender'/'DDN', 'Uncommon').
card_artist('willbender'/'DDN', 'Eric Peterson').
card_number('willbender'/'DDN', '47').
card_multiverse_id('willbender'/'DDN', '386379').

card_in_set('zurgo helmsmasher', 'DDN').
card_original_type('zurgo helmsmasher'/'DDN', 'Legendary Creature — Orc Warrior').
card_original_text('zurgo helmsmasher'/'DDN', 'Haste\nZurgo Helmsmasher attacks each combat if able.\nZurgo Helmsmasher has indestructible as long as it\'s your turn.\nWhenever a creature dealt damage by Zurgo Helmsmasher this turn dies, put a +1/+1 counter on Zurgo Helmsmasher.').
card_image_name('zurgo helmsmasher'/'DDN', 'zurgo helmsmasher').
card_uid('zurgo helmsmasher'/'DDN', 'DDN:Zurgo Helmsmasher:zurgo helmsmasher').
card_rarity('zurgo helmsmasher'/'DDN', 'Mythic Rare').
card_artist('zurgo helmsmasher'/'DDN', 'Ryan Alexander Lee').
card_number('zurgo helmsmasher'/'DDN', '1').
card_multiverse_id('zurgo helmsmasher'/'DDN', '386380').
