% Multiverse Gift Box

set('MGB').
set_name('MGB', 'Multiverse Gift Box').
set_release_date('MGB', '1996-11-01').
set_border('MGB', 'black').
set_type('MGB', 'box').

card_in_set('bull elephant', 'MGB').
card_original_type('bull elephant'/'MGB', 'Creature — Elephant').
card_original_text('bull elephant'/'MGB', '').
card_first_print('bull elephant', 'MGB').
card_image_name('bull elephant'/'MGB', 'bull elephant').
card_uid('bull elephant'/'MGB', 'MGB:Bull Elephant:bull elephant').
card_rarity('bull elephant'/'MGB', 'Special').
card_artist('bull elephant'/'MGB', 'Steve White').
card_flavor_text('bull elephant'/'MGB', 'Four gray trees and a long, coiling snake. What am I? —Zhalfirin riddle').

card_in_set('dark privilege', 'MGB').
card_original_type('dark privilege'/'MGB', 'Enchantment — Aura').
card_original_text('dark privilege'/'MGB', '').
card_first_print('dark privilege', 'MGB').
card_image_name('dark privilege'/'MGB', 'dark privilege').
card_uid('dark privilege'/'MGB', 'MGB:Dark Privilege:dark privilege').
card_rarity('dark privilege'/'MGB', 'Special').
card_artist('dark privilege'/'MGB', 'Tom Kyffin').
card_flavor_text('dark privilege'/'MGB', '\"As you breathe your last, understand why I accept such forbidden gifts.\"\n—Purraj of Urborg').

card_in_set('king cheetah', 'MGB').
card_original_type('king cheetah'/'MGB', 'Creature — Cat').
card_original_text('king cheetah'/'MGB', '').
card_first_print('king cheetah', 'MGB').
card_image_name('king cheetah'/'MGB', 'king cheetah').
card_uid('king cheetah'/'MGB', 'MGB:King Cheetah:king cheetah').
card_rarity('king cheetah'/'MGB', 'Special').
card_artist('king cheetah'/'MGB', 'Terese Nielsen').
card_flavor_text('king cheetah'/'MGB', 'If you find yourself and a friend being chased by a king cheetah, you have but one chance: Trip your friend. —Suq\'Ata wisdom').

card_in_set('necrosavant', 'MGB').
card_original_type('necrosavant'/'MGB', 'Creature — Zombie Giant').
card_original_text('necrosavant'/'MGB', '').
card_first_print('necrosavant', 'MGB').
card_image_name('necrosavant'/'MGB', 'necrosavant').
card_uid('necrosavant'/'MGB', 'MGB:Necrosavant:necrosavant').
card_rarity('necrosavant'/'MGB', 'Special').
card_artist('necrosavant'/'MGB', 'John Coulthart').
card_flavor_text('necrosavant'/'MGB', '\"Ah, I remember my first death.\"\n—Zarkuu, necrosavant').

card_in_set('ovinomancer', 'MGB').
card_original_type('ovinomancer'/'MGB', 'Creature — Human Wizard').
card_original_text('ovinomancer'/'MGB', '').
card_first_print('ovinomancer', 'MGB').
card_image_name('ovinomancer'/'MGB', 'ovinomancer').
card_uid('ovinomancer'/'MGB', 'MGB:Ovinomancer:ovinomancer').
card_rarity('ovinomancer'/'MGB', 'Special').
card_artist('ovinomancer'/'MGB', 'Kev Walker').

card_in_set('peace talks', 'MGB').
card_original_type('peace talks'/'MGB', 'Sorcery').
card_original_text('peace talks'/'MGB', '').
card_first_print('peace talks', 'MGB').
card_image_name('peace talks'/'MGB', 'peace talks').
card_uid('peace talks'/'MGB', 'MGB:Peace Talks:peace talks').
card_rarity('peace talks'/'MGB', 'Special').
card_artist('peace talks'/'MGB', 'Roger Raupp').
card_flavor_text('peace talks'/'MGB', '\"By the tongues of a thousand serpents, this time I do not lie.\"\n—Ahmahz il Kin, Suq\'Ata diplomat').

card_in_set('urborg mindsucker', 'MGB').
card_original_type('urborg mindsucker'/'MGB', 'Creature — Horror').
card_original_text('urborg mindsucker'/'MGB', '').
card_first_print('urborg mindsucker', 'MGB').
card_image_name('urborg mindsucker'/'MGB', 'urborg mindsucker').
card_uid('urborg mindsucker'/'MGB', 'MGB:Urborg Mindsucker:urborg mindsucker').
card_rarity('urborg mindsucker'/'MGB', 'Special').
card_artist('urborg mindsucker'/'MGB', 'Tony Diterlizzi').
card_flavor_text('urborg mindsucker'/'MGB', '\"My pet will pick the ripe fruit from your tortured brain as if it were hanging from a vine.\"\n—Kaervek').

card_in_set('vampirism', 'MGB').
card_original_type('vampirism'/'MGB', 'Enchantment — Aura').
card_original_text('vampirism'/'MGB', '').
card_first_print('vampirism', 'MGB').
card_image_name('vampirism'/'MGB', 'vampirism').
card_uid('vampirism'/'MGB', 'MGB:Vampirism:vampirism').
card_rarity('vampirism'/'MGB', 'Special').
card_artist('vampirism'/'MGB', 'Gary Leach').

card_in_set('viashino sandstalker', 'MGB').
card_original_type('viashino sandstalker'/'MGB', 'Creature — Viashino Warrior').
card_original_text('viashino sandstalker'/'MGB', '').
card_first_print('viashino sandstalker', 'MGB').
card_image_name('viashino sandstalker'/'MGB', 'viashino sandstalker').
card_uid('viashino sandstalker'/'MGB', 'MGB:Viashino Sandstalker:viashino sandstalker').
card_rarity('viashino sandstalker'/'MGB', 'Special').
card_artist('viashino sandstalker'/'MGB', 'Andrew Robinson').
card_flavor_text('viashino sandstalker'/'MGB', '\"Some believe sandstalkers to be illusions; those with scars know better.\"\n—Zhalfirin Guide to the Desert').

card_in_set('wicked reward', 'MGB').
card_original_type('wicked reward'/'MGB', 'Instant').
card_original_text('wicked reward'/'MGB', '').
card_first_print('wicked reward', 'MGB').
card_image_name('wicked reward'/'MGB', 'wicked reward').
card_uid('wicked reward'/'MGB', 'MGB:Wicked Reward:wicked reward').
card_rarity('wicked reward'/'MGB', 'Special').
card_artist('wicked reward'/'MGB', 'D. Alexander Gregory').
card_flavor_text('wicked reward'/'MGB', '\"The blood on my hands is merely proof of my ambition.\"\n—Kaervek').
