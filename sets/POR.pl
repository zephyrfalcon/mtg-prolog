% Portal

set('POR').
set_name('POR', 'Portal').
set_release_date('POR', '1997-05-01').
set_border('POR', 'black').
set_type('POR', 'starter').

card_in_set('alabaster dragon', 'POR').
card_original_type('alabaster dragon'/'POR', 'Summon — Creature').
card_original_text('alabaster dragon'/'POR', 'Flying\nIf Alabaster Dragon is put into your discard pile from play, shuffle Alabaster Dragon back into your deck.').
card_first_print('alabaster dragon', 'POR').
card_image_name('alabaster dragon'/'POR', 'alabaster dragon').
card_uid('alabaster dragon'/'POR', 'POR:Alabaster Dragon:alabaster dragon').
card_rarity('alabaster dragon'/'POR', 'Rare').
card_artist('alabaster dragon'/'POR', 'Ted Naifeh').
card_multiverse_id('alabaster dragon'/'POR', '4369').

card_in_set('alluring scent', 'POR').
card_original_type('alluring scent'/'POR', 'Sorcery').
card_original_text('alluring scent'/'POR', 'Choose any one creature. This turn, all creatures able to intercept that creature do so.').
card_first_print('alluring scent', 'POR').
card_image_name('alluring scent'/'POR', 'alluring scent').
card_uid('alluring scent'/'POR', 'POR:Alluring Scent:alluring scent').
card_rarity('alluring scent'/'POR', 'Rare').
card_artist('alluring scent'/'POR', 'Ted Naifeh').
card_flavor_text('alluring scent'/'POR', 'Doom rarely smells this sweet.').
card_multiverse_id('alluring scent'/'POR', '4286').

card_in_set('anaconda', 'POR').
card_original_type('anaconda'/'POR', 'Summon — Creature').
card_original_text('anaconda'/'POR', 'Swampwalk (If defending player has any swamps in play, Anaconda can\'t be intercepted. Swamps are in play regardless of whether they\'re tapped or untapped.)').
card_first_print('anaconda', 'POR').
card_image_name('anaconda'/'POR', 'anaconda1').
card_uid('anaconda'/'POR', 'POR:Anaconda:anaconda1').
card_rarity('anaconda'/'POR', 'Uncommon').
card_artist('anaconda'/'POR', 'Andrew Robinson').
card_multiverse_id('anaconda'/'POR', '4287').

card_in_set('anaconda', 'POR').
card_original_type('anaconda'/'POR', 'Summon — Creature').
card_original_text('anaconda'/'POR', 'Swampwalk (If defending player has any swamps in play, Anaconda can\'t be intercepted.)').
card_image_name('anaconda'/'POR', 'anaconda2').
card_uid('anaconda'/'POR', 'POR:Anaconda:anaconda2').
card_rarity('anaconda'/'POR', 'Uncommon').
card_artist('anaconda'/'POR', 'Andrew Robinson').
card_flavor_text('anaconda'/'POR', 'Something soft bumped against the rowboat, then was gone.').
card_multiverse_id('anaconda'/'POR', '4288').

card_in_set('ancestral memories', 'POR').
card_original_type('ancestral memories'/'POR', 'Sorcery').
card_original_text('ancestral memories'/'POR', 'Look at the top seven cards of your deck. Put two of them into your hand and the rest into your discard pile.').
card_image_name('ancestral memories'/'POR', 'ancestral memories').
card_uid('ancestral memories'/'POR', 'POR:Ancestral Memories:ancestral memories').
card_rarity('ancestral memories'/'POR', 'Rare').
card_artist('ancestral memories'/'POR', 'Dan Frazier').
card_multiverse_id('ancestral memories'/'POR', '4247').

card_in_set('angelic blessing', 'POR').
card_original_type('angelic blessing'/'POR', 'Sorcery').
card_original_text('angelic blessing'/'POR', 'Any one creature gets +3/+3 and gains flying until the end of the turn.').
card_first_print('angelic blessing', 'POR').
card_image_name('angelic blessing'/'POR', 'angelic blessing').
card_uid('angelic blessing'/'POR', 'POR:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'POR', 'Common').
card_artist('angelic blessing'/'POR', 'DiTerlizzi').
card_flavor_text('angelic blessing'/'POR', 'A peasant can do more by faith than a king by proclamation.').
card_multiverse_id('angelic blessing'/'POR', '4370').

card_in_set('archangel', 'POR').
card_original_type('archangel'/'POR', 'Summon — Creature').
card_original_text('archangel'/'POR', 'Flying\nAttacking doesn\'t cause Archangel to tap.').
card_image_name('archangel'/'POR', 'archangel').
card_uid('archangel'/'POR', 'POR:Archangel:archangel').
card_rarity('archangel'/'POR', 'Rare').
card_artist('archangel'/'POR', 'Quinton Hoover').
card_multiverse_id('archangel'/'POR', '4371').

card_in_set('ardent militia', 'POR').
card_original_type('ardent militia'/'POR', 'Summon — Creature').
card_original_text('ardent militia'/'POR', 'Attacking doesn\'t cause Ardent Militia to tap.').
card_first_print('ardent militia', 'POR').
card_image_name('ardent militia'/'POR', 'ardent militia').
card_uid('ardent militia'/'POR', 'POR:Ardent Militia:ardent militia').
card_rarity('ardent militia'/'POR', 'Uncommon').
card_artist('ardent militia'/'POR', 'Mike Raabe').
card_flavor_text('ardent militia'/'POR', 'Some fight for honor and some for gold, but the militia fights for hearth and home.').
card_multiverse_id('ardent militia'/'POR', '4372').

card_in_set('armageddon', 'POR').
card_original_type('armageddon'/'POR', 'Sorcery').
card_original_text('armageddon'/'POR', 'Destroy all lands. (This includes your lands.)').
card_image_name('armageddon'/'POR', 'armageddon').
card_uid('armageddon'/'POR', 'POR:Armageddon:armageddon').
card_rarity('armageddon'/'POR', 'Rare').
card_artist('armageddon'/'POR', 'John Avon').
card_flavor_text('armageddon'/'POR', '\"‘O miserable of happy! Is this the end\nOf this new glorious world . . . ?\'\"\n—John Milton, Paradise Lost').
card_multiverse_id('armageddon'/'POR', '4373').

card_in_set('armored pegasus', 'POR').
card_original_type('armored pegasus'/'POR', 'Summon — Creature').
card_original_text('armored pegasus'/'POR', 'Flying').
card_first_print('armored pegasus', 'POR').
card_image_name('armored pegasus'/'POR', 'armored pegasus').
card_uid('armored pegasus'/'POR', 'POR:Armored Pegasus:armored pegasus').
card_rarity('armored pegasus'/'POR', 'Common').
card_artist('armored pegasus'/'POR', 'Andrew Robinson').
card_flavor_text('armored pegasus'/'POR', 'Asked how it survived a run-in with a bog imp, the pegasus just shook its mane and burped.').
card_multiverse_id('armored pegasus'/'POR', '4374').

card_in_set('arrogant vampire', 'POR').
card_original_type('arrogant vampire'/'POR', 'Summon — Creature').
card_original_text('arrogant vampire'/'POR', 'Flying').
card_first_print('arrogant vampire', 'POR').
card_image_name('arrogant vampire'/'POR', 'arrogant vampire').
card_uid('arrogant vampire'/'POR', 'POR:Arrogant Vampire:arrogant vampire').
card_rarity('arrogant vampire'/'POR', 'Uncommon').
card_artist('arrogant vampire'/'POR', 'Zina Saunders').
card_flavor_text('arrogant vampire'/'POR', 'Charm and grace may hide a foul heart.').
card_multiverse_id('arrogant vampire'/'POR', '4207').

card_in_set('assassin\'s blade', 'POR').
card_original_type('assassin\'s blade'/'POR', 'Sorcery').
card_original_text('assassin\'s blade'/'POR', 'Play Assassin\'s Blade only after you\'re attacked, before you declare interceptors.\nDestroy any one attacking creature that isn\'t black.').
card_first_print('assassin\'s blade', 'POR').
card_image_name('assassin\'s blade'/'POR', 'assassin\'s blade').
card_uid('assassin\'s blade'/'POR', 'POR:Assassin\'s Blade:assassin\'s blade').
card_rarity('assassin\'s blade'/'POR', 'Uncommon').
card_artist('assassin\'s blade'/'POR', 'John Matson').
card_multiverse_id('assassin\'s blade'/'POR', '4208').

card_in_set('balance of power', 'POR').
card_original_type('balance of power'/'POR', 'Sorcery').
card_original_text('balance of power'/'POR', 'If you have fewer cards in your hand than your opponent does, draw until you have the same number. (When you play Balance of Power, it doesn\'t count as in your hand.)').
card_first_print('balance of power', 'POR').
card_image_name('balance of power'/'POR', 'balance of power').
card_uid('balance of power'/'POR', 'POR:Balance of Power:balance of power').
card_rarity('balance of power'/'POR', 'Rare').
card_artist('balance of power'/'POR', 'Adam Rex').
card_multiverse_id('balance of power'/'POR', '4248').

card_in_set('baleful stare', 'POR').
card_original_type('baleful stare'/'POR', 'Sorcery').
card_original_text('baleful stare'/'POR', 'Look at your opponent\'s hand. For each mountain and red card there, you draw a card. (You draw from your deck.)').
card_first_print('baleful stare', 'POR').
card_image_name('baleful stare'/'POR', 'baleful stare').
card_uid('baleful stare'/'POR', 'POR:Baleful Stare:baleful stare').
card_rarity('baleful stare'/'POR', 'Uncommon').
card_artist('baleful stare'/'POR', 'John Coulthart').
card_multiverse_id('baleful stare'/'POR', '4249').

card_in_set('bee sting', 'POR').
card_original_type('bee sting'/'POR', 'Sorcery').
card_original_text('bee sting'/'POR', 'Bee Sting deals 2 damage to any one creature or player.').
card_first_print('bee sting', 'POR').
card_image_name('bee sting'/'POR', 'bee sting').
card_uid('bee sting'/'POR', 'POR:Bee Sting:bee sting').
card_rarity('bee sting'/'POR', 'Uncommon').
card_artist('bee sting'/'POR', 'Phil Foglio').
card_flavor_text('bee sting'/'POR', 'There are few things as motivating as a swarm of bees.').
card_multiverse_id('bee sting'/'POR', '4289').

card_in_set('blaze', 'POR').
card_original_type('blaze'/'POR', 'Sorcery').
card_original_text('blaze'/'POR', 'Blaze deals X damage to any one creature or player.').
card_first_print('blaze', 'POR').
card_image_name('blaze'/'POR', 'blaze1').
card_uid('blaze'/'POR', 'POR:Blaze:blaze1').
card_rarity('blaze'/'POR', 'Uncommon').
card_artist('blaze'/'POR', 'Gerry Grace').
card_flavor_text('blaze'/'POR', 'Fire never dies alone.').
card_multiverse_id('blaze'/'POR', '4328').

card_in_set('blaze', 'POR').
card_original_type('blaze'/'POR', 'Sorcery').
card_original_text('blaze'/'POR', 'Blaze deals X damage to any one creature or player. (For example, if you tap a mountain plus four other lands when you play Blaze, it deals 4 damage.)').
card_image_name('blaze'/'POR', 'blaze2').
card_uid('blaze'/'POR', 'POR:Blaze:blaze2').
card_rarity('blaze'/'POR', 'Uncommon').
card_artist('blaze'/'POR', 'Gerry Grace').
card_multiverse_id('blaze'/'POR', '4329').

card_in_set('blessed reversal', 'POR').
card_original_type('blessed reversal'/'POR', 'Sorcery').
card_original_text('blessed reversal'/'POR', 'Play Blessed Reversal only after you\'re attacked, before you declare interceptors.\nFor each attacking creature, you gain 3 life.').
card_first_print('blessed reversal', 'POR').
card_image_name('blessed reversal'/'POR', 'blessed reversal').
card_uid('blessed reversal'/'POR', 'POR:Blessed Reversal:blessed reversal').
card_rarity('blessed reversal'/'POR', 'Rare').
card_artist('blessed reversal'/'POR', 'Zina Saunders').
card_multiverse_id('blessed reversal'/'POR', '4375').

card_in_set('blinding light', 'POR').
card_original_type('blinding light'/'POR', 'Sorcery').
card_original_text('blinding light'/'POR', 'Tap all creatures except for white creatures. (This includes your creatures.)').
card_image_name('blinding light'/'POR', 'blinding light').
card_uid('blinding light'/'POR', 'POR:Blinding Light:blinding light').
card_rarity('blinding light'/'POR', 'Rare').
card_artist('blinding light'/'POR', 'John Coulthart').
card_flavor_text('blinding light'/'POR', 'Let the unjust avert their faces and contemplate their peril.').
card_multiverse_id('blinding light'/'POR', '4376').

card_in_set('bog imp', 'POR').
card_original_type('bog imp'/'POR', 'Summon — Creature').
card_original_text('bog imp'/'POR', 'Flying').
card_image_name('bog imp'/'POR', 'bog imp').
card_uid('bog imp'/'POR', 'POR:Bog Imp:bog imp').
card_rarity('bog imp'/'POR', 'Common').
card_artist('bog imp'/'POR', 'Christopher Rush').
card_flavor_text('bog imp'/'POR', 'Don\'t be fooled by their looks. Think of them as little knives with wings.').
card_multiverse_id('bog imp'/'POR', '4209').

card_in_set('bog raiders', 'POR').
card_original_type('bog raiders'/'POR', 'Summon — Creature').
card_original_text('bog raiders'/'POR', 'Swampwalk (If defending player has any swamps in play, Bog Raiders can\'t be intercepted.)').
card_first_print('bog raiders', 'POR').
card_image_name('bog raiders'/'POR', 'bog raiders').
card_uid('bog raiders'/'POR', 'POR:Bog Raiders:bog raiders').
card_rarity('bog raiders'/'POR', 'Common').
card_artist('bog raiders'/'POR', 'Steve Luke').
card_flavor_text('bog raiders'/'POR', 'Those who live amid decay must expect scavengers.').
card_multiverse_id('bog raiders'/'POR', '4210').

card_in_set('bog wraith', 'POR').
card_original_type('bog wraith'/'POR', 'Summon — Creature').
card_original_text('bog wraith'/'POR', 'Swampwalk (If defending player has any swamps in play, Bog Wraith can\'t be intercepted.)').
card_image_name('bog wraith'/'POR', 'bog wraith').
card_uid('bog wraith'/'POR', 'POR:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'POR', 'Uncommon').
card_artist('bog wraith'/'POR', 'Ted Naifeh').
card_flavor_text('bog wraith'/'POR', 'It moved through the troops leaving no footprints, save on their souls.').
card_multiverse_id('bog wraith'/'POR', '4211').

card_in_set('boiling seas', 'POR').
card_original_type('boiling seas'/'POR', 'Sorcery').
card_original_text('boiling seas'/'POR', 'Destroy all islands. (This includes your islands.)').
card_first_print('boiling seas', 'POR').
card_image_name('boiling seas'/'POR', 'boiling seas').
card_uid('boiling seas'/'POR', 'POR:Boiling Seas:boiling seas').
card_rarity('boiling seas'/'POR', 'Uncommon').
card_artist('boiling seas'/'POR', 'Tom Wänerstrand').
card_flavor_text('boiling seas'/'POR', 'What burns the land, boils the seas.').
card_multiverse_id('boiling seas'/'POR', '4330').

card_in_set('border guard', 'POR').
card_original_type('border guard'/'POR', 'Summon — Creature').
card_original_text('border guard'/'POR', '').
card_first_print('border guard', 'POR').
card_image_name('border guard'/'POR', 'border guard').
card_uid('border guard'/'POR', 'POR:Border Guard:border guard').
card_rarity('border guard'/'POR', 'Common').
card_artist('border guard'/'POR', 'Kev Walker').
card_flavor_text('border guard'/'POR', '\"Join the army, see foreign countries!\" they\'d said.').
card_multiverse_id('border guard'/'POR', '4377').

card_in_set('breath of life', 'POR').
card_original_type('breath of life'/'POR', 'Sorcery').
card_original_text('breath of life'/'POR', 'Take any one summon creature from your discard pile and put that card into play. Treat it as though you just played it from your hand.').
card_first_print('breath of life', 'POR').
card_image_name('breath of life'/'POR', 'breath of life').
card_uid('breath of life'/'POR', 'POR:Breath of Life:breath of life').
card_rarity('breath of life'/'POR', 'Common').
card_artist('breath of life'/'POR', 'DiTerlizzi').
card_multiverse_id('breath of life'/'POR', '4378').

card_in_set('bull hippo', 'POR').
card_original_type('bull hippo'/'POR', 'Summon — Creature').
card_original_text('bull hippo'/'POR', 'Islandwalk (If defending player has any islands in play, Bull Hippo can\'t be intercepted.)').
card_first_print('bull hippo', 'POR').
card_image_name('bull hippo'/'POR', 'bull hippo').
card_uid('bull hippo'/'POR', 'POR:Bull Hippo:bull hippo').
card_rarity('bull hippo'/'POR', 'Uncommon').
card_artist('bull hippo'/'POR', 'Roger Raupp').
card_flavor_text('bull hippo'/'POR', 'Stay alert: his lumbering may shake the ground itself, but in water, he glides.').
card_multiverse_id('bull hippo'/'POR', '4290').

card_in_set('burning cloak', 'POR').
card_original_type('burning cloak'/'POR', 'Sorcery').
card_original_text('burning cloak'/'POR', 'Any one creature gets +2/+0 until the end of the turn.\nBurning Cloak deals 2 damage to that creature.').
card_first_print('burning cloak', 'POR').
card_image_name('burning cloak'/'POR', 'burning cloak').
card_uid('burning cloak'/'POR', 'POR:Burning Cloak:burning cloak').
card_rarity('burning cloak'/'POR', 'Common').
card_artist('burning cloak'/'POR', 'Scott M. Fischer').
card_multiverse_id('burning cloak'/'POR', '4331').

card_in_set('capricious sorcerer', 'POR').
card_original_type('capricious sorcerer'/'POR', 'Summon — Creature').
card_original_text('capricious sorcerer'/'POR', 'On your turn, before you attack, you may tap Capricious Sorcerer to have it deal 1 damage to any one creature or player.').
card_first_print('capricious sorcerer', 'POR').
card_image_name('capricious sorcerer'/'POR', 'capricious sorcerer').
card_uid('capricious sorcerer'/'POR', 'POR:Capricious Sorcerer:capricious sorcerer').
card_rarity('capricious sorcerer'/'POR', 'Rare').
card_artist('capricious sorcerer'/'POR', 'Zina Saunders').
card_multiverse_id('capricious sorcerer'/'POR', '4250').

card_in_set('charging bandits', 'POR').
card_original_type('charging bandits'/'POR', 'Summon — Creature').
card_original_text('charging bandits'/'POR', 'If Charging Bandits attacks, it gets +2/+0 until the end of the turn.').
card_first_print('charging bandits', 'POR').
card_image_name('charging bandits'/'POR', 'charging bandits').
card_uid('charging bandits'/'POR', 'POR:Charging Bandits:charging bandits').
card_rarity('charging bandits'/'POR', 'Uncommon').
card_artist('charging bandits'/'POR', 'Dermot Power').
card_flavor_text('charging bandits'/'POR', 'The fear in their victims\' eyes is their most cherished reward.').
card_multiverse_id('charging bandits'/'POR', '4212').

card_in_set('charging paladin', 'POR').
card_original_type('charging paladin'/'POR', 'Summon — Creature').
card_original_text('charging paladin'/'POR', 'If Charging Paladin attacks, it gets +0/+3 until the end of the turn.').
card_first_print('charging paladin', 'POR').
card_image_name('charging paladin'/'POR', 'charging paladin').
card_uid('charging paladin'/'POR', 'POR:Charging Paladin:charging paladin').
card_rarity('charging paladin'/'POR', 'Uncommon').
card_artist('charging paladin'/'POR', 'Kev Walker').
card_flavor_text('charging paladin'/'POR', 'A true warrior\'s thoughts are of victory, not death.').
card_multiverse_id('charging paladin'/'POR', '4379').

card_in_set('charging rhino', 'POR').
card_original_type('charging rhino'/'POR', 'Summon — Creature').
card_original_text('charging rhino'/'POR', 'Charging Rhino can\'t be intercepted by more than one creature.').
card_first_print('charging rhino', 'POR').
card_image_name('charging rhino'/'POR', 'charging rhino').
card_uid('charging rhino'/'POR', 'POR:Charging Rhino:charging rhino').
card_rarity('charging rhino'/'POR', 'Rare').
card_artist('charging rhino'/'POR', 'Una Fricker').
card_flavor_text('charging rhino'/'POR', 'A rhino rarely uses its horn to announce its charge, only to end it.').
card_multiverse_id('charging rhino'/'POR', '4291').

card_in_set('cloak of feathers', 'POR').
card_original_type('cloak of feathers'/'POR', 'Sorcery').
card_original_text('cloak of feathers'/'POR', 'Any one creature gains flying until the end of the turn. You draw a card.').
card_first_print('cloak of feathers', 'POR').
card_image_name('cloak of feathers'/'POR', 'cloak of feathers').
card_uid('cloak of feathers'/'POR', 'POR:Cloak of Feathers:cloak of feathers').
card_rarity('cloak of feathers'/'POR', 'Common').
card_artist('cloak of feathers'/'POR', 'Rebecca Guay').
card_flavor_text('cloak of feathers'/'POR', 'A thousand feathers from a thousand birds, sewn with magic and song.').
card_multiverse_id('cloak of feathers'/'POR', '4251').

card_in_set('cloud dragon', 'POR').
card_original_type('cloud dragon'/'POR', 'Summon — Creature').
card_original_text('cloud dragon'/'POR', 'Flying\nCloud Dragon can intercept only creatures with flying.').
card_first_print('cloud dragon', 'POR').
card_image_name('cloud dragon'/'POR', 'cloud dragon').
card_uid('cloud dragon'/'POR', 'POR:Cloud Dragon:cloud dragon').
card_rarity('cloud dragon'/'POR', 'Rare').
card_artist('cloud dragon'/'POR', 'John Avon').
card_multiverse_id('cloud dragon'/'POR', '4252').

card_in_set('cloud pirates', 'POR').
card_original_type('cloud pirates'/'POR', 'Summon — Creature').
card_original_text('cloud pirates'/'POR', 'Flying\nCloud Pirates can intercept only creatures with flying.').
card_first_print('cloud pirates', 'POR').
card_image_name('cloud pirates'/'POR', 'cloud pirates').
card_uid('cloud pirates'/'POR', 'POR:Cloud Pirates:cloud pirates').
card_rarity('cloud pirates'/'POR', 'Common').
card_artist('cloud pirates'/'POR', 'Phil Foglio').
card_multiverse_id('cloud pirates'/'POR', '4253').

card_in_set('cloud spirit', 'POR').
card_original_type('cloud spirit'/'POR', 'Summon — Creature').
card_original_text('cloud spirit'/'POR', 'Flying\nCloud Spirit can intercept only creatures with flying.').
card_first_print('cloud spirit', 'POR').
card_image_name('cloud spirit'/'POR', 'cloud spirit').
card_uid('cloud spirit'/'POR', 'POR:Cloud Spirit:cloud spirit').
card_rarity('cloud spirit'/'POR', 'Uncommon').
card_artist('cloud spirit'/'POR', 'DiTerlizzi').
card_multiverse_id('cloud spirit'/'POR', '4254').

card_in_set('command of unsummoning', 'POR').
card_original_type('command of unsummoning'/'POR', 'Sorcery').
card_original_text('command of unsummoning'/'POR', 'Play Command of Unsummoning only after you\'re attacked, before you declare interceptors.\nReturn any one or two attacking creatures to their owner\'s hand.').
card_first_print('command of unsummoning', 'POR').
card_image_name('command of unsummoning'/'POR', 'command of unsummoning').
card_uid('command of unsummoning'/'POR', 'POR:Command of Unsummoning:command of unsummoning').
card_rarity('command of unsummoning'/'POR', 'Uncommon').
card_artist('command of unsummoning'/'POR', 'Phil Foglio').
card_multiverse_id('command of unsummoning'/'POR', '4255').

card_in_set('coral eel', 'POR').
card_original_type('coral eel'/'POR', 'Summon — Creature').
card_original_text('coral eel'/'POR', '').
card_first_print('coral eel', 'POR').
card_image_name('coral eel'/'POR', 'coral eel').
card_uid('coral eel'/'POR', 'POR:Coral Eel:coral eel').
card_rarity('coral eel'/'POR', 'Common').
card_artist('coral eel'/'POR', 'Una Fricker').
card_flavor_text('coral eel'/'POR', 'Some fishers like to eat eels, and some eels like to eat fishers.').
card_multiverse_id('coral eel'/'POR', '4256').

card_in_set('craven giant', 'POR').
card_original_type('craven giant'/'POR', 'Summon — Creature').
card_original_text('craven giant'/'POR', 'Craven Giant can\'t intercept.').
card_first_print('craven giant', 'POR').
card_image_name('craven giant'/'POR', 'craven giant').
card_uid('craven giant'/'POR', 'POR:Craven Giant:craven giant').
card_rarity('craven giant'/'POR', 'Common').
card_artist('craven giant'/'POR', 'Ron Spencer').
card_flavor_text('craven giant'/'POR', '\"The best armor is to keep out of range.\"\n—Italian proverb').
card_multiverse_id('craven giant'/'POR', '4332').

card_in_set('craven knight', 'POR').
card_original_type('craven knight'/'POR', 'Summon — Creature').
card_original_text('craven knight'/'POR', 'Craven Knight can\'t intercept.').
card_first_print('craven knight', 'POR').
card_image_name('craven knight'/'POR', 'craven knight').
card_uid('craven knight'/'POR', 'POR:Craven Knight:craven knight').
card_rarity('craven knight'/'POR', 'Common').
card_artist('craven knight'/'POR', 'Charles Gillespie').
card_flavor_text('craven knight'/'POR', '\"I say victory is better than honor.\"').
card_multiverse_id('craven knight'/'POR', '4213').

card_in_set('cruel bargain', 'POR').
card_original_type('cruel bargain'/'POR', 'Sorcery').
card_original_text('cruel bargain'/'POR', 'Draw four cards. You lose half your life, rounded up. (For example, if you have 11 life, you lose 6 life.)').
card_first_print('cruel bargain', 'POR').
card_image_name('cruel bargain'/'POR', 'cruel bargain').
card_uid('cruel bargain'/'POR', 'POR:Cruel Bargain:cruel bargain').
card_rarity('cruel bargain'/'POR', 'Rare').
card_artist('cruel bargain'/'POR', 'Adrian Smith').
card_multiverse_id('cruel bargain'/'POR', '4214').

card_in_set('cruel fate', 'POR').
card_original_type('cruel fate'/'POR', 'Sorcery').
card_original_text('cruel fate'/'POR', 'Look at the top five cards of your opponent\'s deck. Put one of them into your opponent\'s discard pile and the rest on top of his or her deck in any order.').
card_first_print('cruel fate', 'POR').
card_image_name('cruel fate'/'POR', 'cruel fate').
card_uid('cruel fate'/'POR', 'POR:Cruel Fate:cruel fate').
card_rarity('cruel fate'/'POR', 'Rare').
card_artist('cruel fate'/'POR', 'Adrian Smith').
card_multiverse_id('cruel fate'/'POR', '4257').

card_in_set('cruel tutor', 'POR').
card_original_type('cruel tutor'/'POR', 'Sorcery').
card_original_text('cruel tutor'/'POR', 'Search your deck for any card. Shuffle your deck and put that card on top of it. You lose 2 life.').
card_first_print('cruel tutor', 'POR').
card_image_name('cruel tutor'/'POR', 'cruel tutor').
card_uid('cruel tutor'/'POR', 'POR:Cruel Tutor:cruel tutor').
card_rarity('cruel tutor'/'POR', 'Rare').
card_artist('cruel tutor'/'POR', 'Kev Walker').
card_flavor_text('cruel tutor'/'POR', 'The more you pay for the lesson, the better you\'ll remember it.').
card_multiverse_id('cruel tutor'/'POR', '4215').

card_in_set('deep wood', 'POR').
card_original_type('deep wood'/'POR', 'Sorcery').
card_original_text('deep wood'/'POR', 'Play Deep Wood only after you\'re attacked, before you declare interceptors.\nThis turn, all damage dealt to you by attacking creatures is reduced to 0.').
card_first_print('deep wood', 'POR').
card_image_name('deep wood'/'POR', 'deep wood').
card_uid('deep wood'/'POR', 'POR:Deep Wood:deep wood').
card_rarity('deep wood'/'POR', 'Uncommon').
card_artist('deep wood'/'POR', 'Paolo Parente').
card_multiverse_id('deep wood'/'POR', '4292').

card_in_set('deep-sea serpent', 'POR').
card_original_type('deep-sea serpent'/'POR', 'Summon — Creature').
card_original_text('deep-sea serpent'/'POR', 'Deep-Sea Serpent can attack only if the defending player has an island in play.').
card_first_print('deep-sea serpent', 'POR').
card_image_name('deep-sea serpent'/'POR', 'deep-sea serpent').
card_uid('deep-sea serpent'/'POR', 'POR:Deep-Sea Serpent:deep-sea serpent').
card_rarity('deep-sea serpent'/'POR', 'Uncommon').
card_artist('deep-sea serpent'/'POR', 'Scott M. Fischer').
card_flavor_text('deep-sea serpent'/'POR', 'Sailors fear the deep-sea serpent; others thank the gods it has no legs.').
card_multiverse_id('deep-sea serpent'/'POR', '4258').

card_in_set('defiant stand', 'POR').
card_original_type('defiant stand'/'POR', 'Sorcery').
card_original_text('defiant stand'/'POR', 'Play Defiant Stand only after you\'re attacked, before you declare interceptors.\nAny one creature gets +1/+3 until the end of the turn. If that creature is tapped, untap it.').
card_first_print('defiant stand', 'POR').
card_image_name('defiant stand'/'POR', 'defiant stand').
card_uid('defiant stand'/'POR', 'POR:Defiant Stand:defiant stand').
card_rarity('defiant stand'/'POR', 'Uncommon').
card_artist('defiant stand'/'POR', 'Hannibal King').
card_multiverse_id('defiant stand'/'POR', '4380').

card_in_set('déjà vu', 'POR').
card_original_type('déjà vu'/'POR', 'Sorcery').
card_original_text('déjà vu'/'POR', 'Return any one sorcery card from your discard pile to your hand.').
card_first_print('déjà vu', 'POR').
card_image_name('déjà vu'/'POR', 'deja vu').
card_uid('déjà vu'/'POR', 'POR:Déjà Vu:deja vu').
card_rarity('déjà vu'/'POR', 'Common').
card_artist('déjà vu'/'POR', 'Hannibal King').
card_flavor_text('déjà vu'/'POR', 'The past is a mirror of the future.').
card_multiverse_id('déjà vu'/'POR', '4259').

card_in_set('desert drake', 'POR').
card_original_type('desert drake'/'POR', 'Summon — Creature').
card_original_text('desert drake'/'POR', 'Flying').
card_first_print('desert drake', 'POR').
card_image_name('desert drake'/'POR', 'desert drake').
card_uid('desert drake'/'POR', 'POR:Desert Drake:desert drake').
card_rarity('desert drake'/'POR', 'Uncommon').
card_artist('desert drake'/'POR', 'Gerry Grace').
card_flavor_text('desert drake'/'POR', 'Never doubt a dragon just because of its size.').
card_multiverse_id('desert drake'/'POR', '4333').

card_in_set('devastation', 'POR').
card_original_type('devastation'/'POR', 'Sorcery').
card_original_text('devastation'/'POR', 'Destroy all creatures and lands. (This includes your creatures and lands.)').
card_first_print('devastation', 'POR').
card_image_name('devastation'/'POR', 'devastation').
card_uid('devastation'/'POR', 'POR:Devastation:devastation').
card_rarity('devastation'/'POR', 'Rare').
card_artist('devastation'/'POR', 'Steve Luke').
card_flavor_text('devastation'/'POR', 'There is much talk about the art of creation. What about the art of destruction?').
card_multiverse_id('devastation'/'POR', '4334').

card_in_set('devoted hero', 'POR').
card_original_type('devoted hero'/'POR', 'Summon — Creature').
card_original_text('devoted hero'/'POR', '').
card_first_print('devoted hero', 'POR').
card_image_name('devoted hero'/'POR', 'devoted hero').
card_uid('devoted hero'/'POR', 'POR:Devoted Hero:devoted hero').
card_rarity('devoted hero'/'POR', 'Common').
card_artist('devoted hero'/'POR', 'DiTerlizzi').
card_flavor_text('devoted hero'/'POR', 'The heart\'s courage is the soul\'s guardian.').
card_multiverse_id('devoted hero'/'POR', '4381').

card_in_set('djinn of the lamp', 'POR').
card_original_type('djinn of the lamp'/'POR', 'Summon — Creature').
card_original_text('djinn of the lamp'/'POR', 'Flying').
card_first_print('djinn of the lamp', 'POR').
card_image_name('djinn of the lamp'/'POR', 'djinn of the lamp').
card_uid('djinn of the lamp'/'POR', 'POR:Djinn of the Lamp:djinn of the lamp').
card_rarity('djinn of the lamp'/'POR', 'Rare').
card_artist('djinn of the lamp'/'POR', 'DiTerlizzi').
card_flavor_text('djinn of the lamp'/'POR', 'Once they learn the trick of carrying their own lamps, they never touch the ground again.').
card_multiverse_id('djinn of the lamp'/'POR', '4260').

card_in_set('dread charge', 'POR').
card_original_type('dread charge'/'POR', 'Sorcery').
card_original_text('dread charge'/'POR', 'This turn, your black creatures can be intercepted only by other black creatures.').
card_first_print('dread charge', 'POR').
card_image_name('dread charge'/'POR', 'dread charge').
card_uid('dread charge'/'POR', 'POR:Dread Charge:dread charge').
card_rarity('dread charge'/'POR', 'Rare').
card_artist('dread charge'/'POR', 'Ted Naifeh').
card_flavor_text('dread charge'/'POR', '\"As equal were their souls, so equal was their fate.\" —John Dryden, \"Ode to Mrs. Anne Killigrew\"').
card_multiverse_id('dread charge'/'POR', '4216').

card_in_set('dread reaper', 'POR').
card_original_type('dread reaper'/'POR', 'Summon — Creature').
card_original_text('dread reaper'/'POR', 'Flying\nWhen Dread Reaper comes into play from your hand, you lose 5 life. (The person who plays Dread Reaper loses the life.)').
card_first_print('dread reaper', 'POR').
card_image_name('dread reaper'/'POR', 'dread reaper').
card_uid('dread reaper'/'POR', 'POR:Dread Reaper:dread reaper').
card_rarity('dread reaper'/'POR', 'Rare').
card_artist('dread reaper'/'POR', 'Christopher Rush').
card_multiverse_id('dread reaper'/'POR', '4217').

card_in_set('dry spell', 'POR').
card_original_type('dry spell'/'POR', 'Sorcery').
card_original_text('dry spell'/'POR', 'Dry Spell deals 1 damage to each creature and player. (This includes your creatures and you.)').
card_image_name('dry spell'/'POR', 'dry spell').
card_uid('dry spell'/'POR', 'POR:Dry Spell:dry spell').
card_rarity('dry spell'/'POR', 'Uncommon').
card_artist('dry spell'/'POR', 'Roger Raupp').
card_flavor_text('dry spell'/'POR', 'A fist of dust to line your throat, a bowl of sand to fill your belly.').
card_multiverse_id('dry spell'/'POR', '4218').

card_in_set('earthquake', 'POR').
card_original_type('earthquake'/'POR', 'Sorcery').
card_original_text('earthquake'/'POR', 'Earthquake deals X damage to each player and each creature without flying. (This includes you and your creatures without flying.)').
card_image_name('earthquake'/'POR', 'earthquake').
card_uid('earthquake'/'POR', 'POR:Earthquake:earthquake').
card_rarity('earthquake'/'POR', 'Rare').
card_artist('earthquake'/'POR', 'Adrian Smith').
card_multiverse_id('earthquake'/'POR', '4335').

card_in_set('ebon dragon', 'POR').
card_original_type('ebon dragon'/'POR', 'Summon — Creature').
card_original_text('ebon dragon'/'POR', 'Flying\nWhen Ebon Dragon comes into play from your hand, you may force your opponent to choose and discard a card from his or her hand.').
card_first_print('ebon dragon', 'POR').
card_image_name('ebon dragon'/'POR', 'ebon dragon').
card_uid('ebon dragon'/'POR', 'POR:Ebon Dragon:ebon dragon').
card_rarity('ebon dragon'/'POR', 'Rare').
card_artist('ebon dragon'/'POR', 'Donato Giancola').
card_multiverse_id('ebon dragon'/'POR', '4219').

card_in_set('elite cat warrior', 'POR').
card_original_type('elite cat warrior'/'POR', 'Summon — Creature').
card_original_text('elite cat warrior'/'POR', 'Forestwalk (If defending player has any forests in play, Elite Cat Warrior can\'t be intercepted. Forests are in play regardless of whether they\'re tapped or untapped.)').
card_first_print('elite cat warrior', 'POR').
card_image_name('elite cat warrior'/'POR', 'elite cat warrior1').
card_uid('elite cat warrior'/'POR', 'POR:Elite Cat Warrior:elite cat warrior1').
card_rarity('elite cat warrior'/'POR', 'Common').
card_artist('elite cat warrior'/'POR', 'Eric Peterson').
card_multiverse_id('elite cat warrior'/'POR', '4293').

card_in_set('elite cat warrior', 'POR').
card_original_type('elite cat warrior'/'POR', 'Summon — Creature').
card_original_text('elite cat warrior'/'POR', 'Forestwalk (If defending player has any forests in play, Elite Cat Warrior can\'t be intercepted.)').
card_image_name('elite cat warrior'/'POR', 'elite cat warrior2').
card_uid('elite cat warrior'/'POR', 'POR:Elite Cat Warrior:elite cat warrior2').
card_rarity('elite cat warrior'/'POR', 'Common').
card_artist('elite cat warrior'/'POR', 'Eric Peterson').
card_flavor_text('elite cat warrior'/'POR', '\"Hear that? No? That\'s a cat warrior.\"').
card_multiverse_id('elite cat warrior'/'POR', '4294').

card_in_set('elven cache', 'POR').
card_original_type('elven cache'/'POR', 'Sorcery').
card_original_text('elven cache'/'POR', 'Return any one card from your discard pile to your hand.').
card_image_name('elven cache'/'POR', 'elven cache').
card_uid('elven cache'/'POR', 'POR:Elven Cache:elven cache').
card_rarity('elven cache'/'POR', 'Common').
card_artist('elven cache'/'POR', 'Rebecca Guay').
card_flavor_text('elven cache'/'POR', 'Elves know where to harvest the best of the forest, for they planted it themselves.').
card_multiverse_id('elven cache'/'POR', '4295').

card_in_set('elvish ranger', 'POR').
card_original_type('elvish ranger'/'POR', 'Summon — Creature').
card_original_text('elvish ranger'/'POR', '').
card_image_name('elvish ranger'/'POR', 'elvish ranger').
card_uid('elvish ranger'/'POR', 'POR:Elvish Ranger:elvish ranger').
card_rarity('elvish ranger'/'POR', 'Common').
card_artist('elvish ranger'/'POR', 'DiTerlizzi').
card_flavor_text('elvish ranger'/'POR', 'It\'s up to you if you enter their woods—and up to them if you leave.').
card_multiverse_id('elvish ranger'/'POR', '4296').

card_in_set('endless cockroaches', 'POR').
card_original_type('endless cockroaches'/'POR', 'Summon — Creature').
card_original_text('endless cockroaches'/'POR', 'If Endless Cockroaches is put into your discard pile from play, return Endless Cockroaches to your hand.').
card_first_print('endless cockroaches', 'POR').
card_image_name('endless cockroaches'/'POR', 'endless cockroaches').
card_uid('endless cockroaches'/'POR', 'POR:Endless Cockroaches:endless cockroaches').
card_rarity('endless cockroaches'/'POR', 'Rare').
card_artist('endless cockroaches'/'POR', 'Ron Spencer').
card_multiverse_id('endless cockroaches'/'POR', '4220').

card_in_set('exhaustion', 'POR').
card_original_type('exhaustion'/'POR', 'Sorcery').
card_original_text('exhaustion'/'POR', 'At the beginning of your opponent\'s next turn, he or she skips untapping his or her creatures and lands.').
card_first_print('exhaustion', 'POR').
card_image_name('exhaustion'/'POR', 'exhaustion').
card_uid('exhaustion'/'POR', 'POR:Exhaustion:exhaustion').
card_rarity('exhaustion'/'POR', 'Rare').
card_artist('exhaustion'/'POR', 'DiTerlizzi').
card_multiverse_id('exhaustion'/'POR', '4261').

card_in_set('false peace', 'POR').
card_original_type('false peace'/'POR', 'Sorcery').
card_original_text('false peace'/'POR', 'Choose any one player. That player can\'t attack on his or her next turn.').
card_first_print('false peace', 'POR').
card_image_name('false peace'/'POR', 'false peace').
card_uid('false peace'/'POR', 'POR:False Peace:false peace').
card_rarity('false peace'/'POR', 'Common').
card_artist('false peace'/'POR', 'Zina Saunders').
card_flavor_text('false peace'/'POR', 'Mutual consent is not required for war.').
card_multiverse_id('false peace'/'POR', '4382').

card_in_set('feral shadow', 'POR').
card_original_type('feral shadow'/'POR', 'Summon — Creature').
card_original_text('feral shadow'/'POR', 'Flying').
card_image_name('feral shadow'/'POR', 'feral shadow').
card_uid('feral shadow'/'POR', 'POR:Feral Shadow:feral shadow').
card_rarity('feral shadow'/'POR', 'Common').
card_artist('feral shadow'/'POR', 'Colin MacNeil').
card_flavor_text('feral shadow'/'POR', 'Not all shadows are cast by light—some are cast by darkness.').
card_multiverse_id('feral shadow'/'POR', '4221').

card_in_set('final strike', 'POR').
card_original_type('final strike'/'POR', 'Sorcery').
card_original_text('final strike'/'POR', 'Choose one of your creatures. Final Strike deals to your opponent damage equal to that creature\'s offense. Then, put the creature in your discard pile.').
card_first_print('final strike', 'POR').
card_image_name('final strike'/'POR', 'final strike').
card_uid('final strike'/'POR', 'POR:Final Strike:final strike').
card_rarity('final strike'/'POR', 'Rare').
card_artist('final strike'/'POR', 'John Coulthart').
card_multiverse_id('final strike'/'POR', '4222').

card_in_set('fire dragon', 'POR').
card_original_type('fire dragon'/'POR', 'Summon — Creature').
card_original_text('fire dragon'/'POR', 'Flying\nWhen Fire Dragon comes into play from your hand, it deals to any one creature damage equal to the number of mountains you have in play.').
card_first_print('fire dragon', 'POR').
card_image_name('fire dragon'/'POR', 'fire dragon').
card_uid('fire dragon'/'POR', 'POR:Fire Dragon:fire dragon').
card_rarity('fire dragon'/'POR', 'Rare').
card_artist('fire dragon'/'POR', 'William Simpson').
card_multiverse_id('fire dragon'/'POR', '4336').

card_in_set('fire imp', 'POR').
card_original_type('fire imp'/'POR', 'Summon — Creature').
card_original_text('fire imp'/'POR', 'When Fire Imp comes into play from your hand, it deals 2 damage to any one creature. (If you\'re the only player with creatures, Fire Imp deals 2 damage to one of your creatures.)').
card_first_print('fire imp', 'POR').
card_image_name('fire imp'/'POR', 'fire imp').
card_uid('fire imp'/'POR', 'POR:Fire Imp:fire imp').
card_rarity('fire imp'/'POR', 'Uncommon').
card_artist('fire imp'/'POR', 'DiTerlizzi').
card_multiverse_id('fire imp'/'POR', '4337').

card_in_set('fire snake', 'POR').
card_original_type('fire snake'/'POR', 'Summon — Creature').
card_original_text('fire snake'/'POR', 'If Fire Snake is put into your discard pile from play, destroy any one land.').
card_first_print('fire snake', 'POR').
card_image_name('fire snake'/'POR', 'fire snake').
card_uid('fire snake'/'POR', 'POR:Fire Snake:fire snake').
card_rarity('fire snake'/'POR', 'Common').
card_artist('fire snake'/'POR', 'Steve Luke').
card_flavor_text('fire snake'/'POR', 'The snake\'s final thrashings only spread the fire within it.').
card_multiverse_id('fire snake'/'POR', '4338').

card_in_set('fire tempest', 'POR').
card_original_type('fire tempest'/'POR', 'Sorcery').
card_original_text('fire tempest'/'POR', 'Fire Tempest deals 6 damage to each creature and player. (This includes your creatures and you. If all players drop to 0 life or less, the game is a draw.)').
card_first_print('fire tempest', 'POR').
card_image_name('fire tempest'/'POR', 'fire tempest').
card_uid('fire tempest'/'POR', 'POR:Fire Tempest:fire tempest').
card_rarity('fire tempest'/'POR', 'Rare').
card_artist('fire tempest'/'POR', 'Mike Dringenberg').
card_multiverse_id('fire tempest'/'POR', '4339').

card_in_set('flashfires', 'POR').
card_original_type('flashfires'/'POR', 'Sorcery').
card_original_text('flashfires'/'POR', 'Destroy all plains. (This includes your plains.)').
card_image_name('flashfires'/'POR', 'flashfires').
card_uid('flashfires'/'POR', 'POR:Flashfires:flashfires').
card_rarity('flashfires'/'POR', 'Uncommon').
card_artist('flashfires'/'POR', 'Randy Gallegos').
card_flavor_text('flashfires'/'POR', 'Dry grass is tinder before the spark.').
card_multiverse_id('flashfires'/'POR', '4340').

card_in_set('fleet-footed monk', 'POR').
card_original_type('fleet-footed monk'/'POR', 'Summon — Creature').
card_original_text('fleet-footed monk'/'POR', 'Fleet-Footed Monk can\'t be intercepted by any creature with offense 2 or greater.').
card_first_print('fleet-footed monk', 'POR').
card_image_name('fleet-footed monk'/'POR', 'fleet-footed monk').
card_uid('fleet-footed monk'/'POR', 'POR:Fleet-Footed Monk:fleet-footed monk').
card_rarity('fleet-footed monk'/'POR', 'Common').
card_artist('fleet-footed monk'/'POR', 'D. Alexander Gregory').
card_flavor_text('fleet-footed monk'/'POR', '\"Hesitation is for the faithless. My belief lends me speed.\"').
card_multiverse_id('fleet-footed monk'/'POR', '4383').

card_in_set('flux', 'POR').
card_original_type('flux'/'POR', 'Sorcery').
card_original_text('flux'/'POR', 'Each player chooses and discards from his or her hand any number of cards and then draws that many cards. You then draw a card. (You choose first.)').
card_first_print('flux', 'POR').
card_image_name('flux'/'POR', 'flux').
card_uid('flux'/'POR', 'POR:Flux:flux').
card_rarity('flux'/'POR', 'Uncommon').
card_artist('flux'/'POR', 'Ted Naifeh').
card_multiverse_id('flux'/'POR', '4262').

card_in_set('foot soldiers', 'POR').
card_original_type('foot soldiers'/'POR', 'Summon — Creature').
card_original_text('foot soldiers'/'POR', '').
card_first_print('foot soldiers', 'POR').
card_image_name('foot soldiers'/'POR', 'foot soldiers').
card_uid('foot soldiers'/'POR', 'POR:Foot Soldiers:foot soldiers').
card_rarity('foot soldiers'/'POR', 'Common').
card_artist('foot soldiers'/'POR', 'Kev Walker').
card_flavor_text('foot soldiers'/'POR', 'Infantry deployment is the art of putting your troops in the wrong place at the right time.').
card_multiverse_id('foot soldiers'/'POR', '4384').

card_in_set('forest', 'POR').
card_original_type('forest'/'POR', 'Land').
card_original_text('forest'/'POR', 'G').
card_image_name('forest'/'POR', 'forest1').
card_uid('forest'/'POR', 'POR:Forest:forest1').
card_rarity('forest'/'POR', 'Basic Land').
card_artist('forest'/'POR', 'John Avon').
card_multiverse_id('forest'/'POR', '4413').

card_in_set('forest', 'POR').
card_original_type('forest'/'POR', 'Land').
card_original_text('forest'/'POR', 'G').
card_image_name('forest'/'POR', 'forest2').
card_uid('forest'/'POR', 'POR:Forest:forest2').
card_rarity('forest'/'POR', 'Basic Land').
card_artist('forest'/'POR', 'John Avon').
card_multiverse_id('forest'/'POR', '4414').

card_in_set('forest', 'POR').
card_original_type('forest'/'POR', 'Land').
card_original_text('forest'/'POR', 'G').
card_image_name('forest'/'POR', 'forest3').
card_uid('forest'/'POR', 'POR:Forest:forest3').
card_rarity('forest'/'POR', 'Basic Land').
card_artist('forest'/'POR', 'John Avon').
card_multiverse_id('forest'/'POR', '4415').

card_in_set('forest', 'POR').
card_original_type('forest'/'POR', 'Land').
card_original_text('forest'/'POR', 'G').
card_image_name('forest'/'POR', 'forest4').
card_uid('forest'/'POR', 'POR:Forest:forest4').
card_rarity('forest'/'POR', 'Basic Land').
card_artist('forest'/'POR', 'John Avon').
card_multiverse_id('forest'/'POR', '4416').

card_in_set('forked lightning', 'POR').
card_original_type('forked lightning'/'POR', 'Sorcery').
card_original_text('forked lightning'/'POR', 'Forked Lightning deals 4 damage divided any way you choose among any one, two, or three creatures.').
card_first_print('forked lightning', 'POR').
card_image_name('forked lightning'/'POR', 'forked lightning').
card_uid('forked lightning'/'POR', 'POR:Forked Lightning:forked lightning').
card_rarity('forked lightning'/'POR', 'Rare').
card_artist('forked lightning'/'POR', 'Ted Naifeh').
card_multiverse_id('forked lightning'/'POR', '4341').

card_in_set('fruition', 'POR').
card_original_type('fruition'/'POR', 'Sorcery').
card_original_text('fruition'/'POR', 'For each forest you and your opponent have in play, you gain 1 life.').
card_first_print('fruition', 'POR').
card_image_name('fruition'/'POR', 'fruition').
card_uid('fruition'/'POR', 'POR:Fruition:fruition').
card_rarity('fruition'/'POR', 'Common').
card_artist('fruition'/'POR', 'Steve Luke').
card_flavor_text('fruition'/'POR', 'Come to these woods and suffer no pain; you\'ve burdens to lose and new life to gain.').
card_multiverse_id('fruition'/'POR', '4297').

card_in_set('giant octopus', 'POR').
card_original_type('giant octopus'/'POR', 'Summon — Creature').
card_original_text('giant octopus'/'POR', '').
card_first_print('giant octopus', 'POR').
card_image_name('giant octopus'/'POR', 'giant octopus').
card_uid('giant octopus'/'POR', 'POR:Giant Octopus:giant octopus').
card_rarity('giant octopus'/'POR', 'Common').
card_artist('giant octopus'/'POR', 'John Matson').
card_flavor_text('giant octopus'/'POR', 'At the sight of the thing the calamari vendor\'s eyes went wide, but from fear or avarice none could tell.').
card_multiverse_id('giant octopus'/'POR', '4263').

card_in_set('giant spider', 'POR').
card_original_type('giant spider'/'POR', 'Summon — Creature').
card_original_text('giant spider'/'POR', 'Giant Spider can intercept as though it had flying.').
card_image_name('giant spider'/'POR', 'giant spider').
card_uid('giant spider'/'POR', 'POR:Giant Spider:giant spider').
card_rarity('giant spider'/'POR', 'Common').
card_artist('giant spider'/'POR', 'Randy Gallegos').
card_flavor_text('giant spider'/'POR', 'It\'s not far into the trap, as the crow flies.').
card_multiverse_id('giant spider'/'POR', '4298').

card_in_set('gift of estates', 'POR').
card_original_type('gift of estates'/'POR', 'Sorcery').
card_original_text('gift of estates'/'POR', 'If your opponent has more lands in play than you do, search your deck for up to three plains and put them into your hand. Shuffle your deck afterwards.').
card_first_print('gift of estates', 'POR').
card_image_name('gift of estates'/'POR', 'gift of estates').
card_uid('gift of estates'/'POR', 'POR:Gift of Estates:gift of estates').
card_rarity('gift of estates'/'POR', 'Rare').
card_artist('gift of estates'/'POR', 'Kaja Foglio').
card_multiverse_id('gift of estates'/'POR', '4385').

card_in_set('goblin bully', 'POR').
card_original_type('goblin bully'/'POR', 'Summon — Creature').
card_original_text('goblin bully'/'POR', '').
card_first_print('goblin bully', 'POR').
card_image_name('goblin bully'/'POR', 'goblin bully').
card_uid('goblin bully'/'POR', 'POR:Goblin Bully:goblin bully').
card_rarity('goblin bully'/'POR', 'Common').
card_artist('goblin bully'/'POR', 'Pete Venters').
card_flavor_text('goblin bully'/'POR', 'It\'s easy to stand head and shoulders over those with no backbone.').
card_multiverse_id('goblin bully'/'POR', '4342').

card_in_set('gorilla warrior', 'POR').
card_original_type('gorilla warrior'/'POR', 'Summon — Creature').
card_original_text('gorilla warrior'/'POR', '').
card_first_print('gorilla warrior', 'POR').
card_image_name('gorilla warrior'/'POR', 'gorilla warrior').
card_uid('gorilla warrior'/'POR', 'POR:Gorilla Warrior:gorilla warrior').
card_rarity('gorilla warrior'/'POR', 'Common').
card_artist('gorilla warrior'/'POR', 'John Matson').
card_flavor_text('gorilla warrior'/'POR', 'They were formidable even before they learned the use of weapons.').
card_multiverse_id('gorilla warrior'/'POR', '4299').

card_in_set('gravedigger', 'POR').
card_original_type('gravedigger'/'POR', 'Summon — Creature').
card_original_text('gravedigger'/'POR', 'When Gravedigger comes into play from your hand, you may choose to return a summon creature from your discard pile to your hand.').
card_first_print('gravedigger', 'POR').
card_image_name('gravedigger'/'POR', 'gravedigger').
card_uid('gravedigger'/'POR', 'POR:Gravedigger:gravedigger').
card_rarity('gravedigger'/'POR', 'Uncommon').
card_artist('gravedigger'/'POR', 'Scott M. Fischer').
card_multiverse_id('gravedigger'/'POR', '4223').

card_in_set('grizzly bears', 'POR').
card_original_type('grizzly bears'/'POR', 'Summon — Creature').
card_original_text('grizzly bears'/'POR', '').
card_image_name('grizzly bears'/'POR', 'grizzly bears').
card_uid('grizzly bears'/'POR', 'POR:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'POR', 'Common').
card_artist('grizzly bears'/'POR', 'Zina Saunders').
card_flavor_text('grizzly bears'/'POR', 'Don\'t worry about provoking grizzly bears; they come that way.').
card_multiverse_id('grizzly bears'/'POR', '4300').

card_in_set('hand of death', 'POR').
card_original_type('hand of death'/'POR', 'Sorcery').
card_original_text('hand of death'/'POR', 'Destroy any one creature that isn\'t black. (A creature is black if it has {B} in its cost.)').
card_first_print('hand of death', 'POR').
card_image_name('hand of death'/'POR', 'hand of death1').
card_uid('hand of death'/'POR', 'POR:Hand of Death:hand of death1').
card_rarity('hand of death'/'POR', 'Common').
card_artist('hand of death'/'POR', 'John Coulthart').
card_flavor_text('hand of death'/'POR', 'Death claims all but the darkest souls.').
card_multiverse_id('hand of death'/'POR', '4224').

card_in_set('hand of death', 'POR').
card_original_type('hand of death'/'POR', 'Sorcery').
card_original_text('hand of death'/'POR', 'Destroy any one creature that isn\'t black.').
card_image_name('hand of death'/'POR', 'hand of death2').
card_uid('hand of death'/'POR', 'POR:Hand of Death:hand of death2').
card_rarity('hand of death'/'POR', 'Common').
card_artist('hand of death'/'POR', 'John Coulthart').
card_flavor_text('hand of death'/'POR', 'Death claims all but the darkest souls.').
card_multiverse_id('hand of death'/'POR', '4225').

card_in_set('harsh justice', 'POR').
card_original_type('harsh justice'/'POR', 'Sorcery').
card_original_text('harsh justice'/'POR', 'Play Harsh Justice only after you\'re attacked, before you declare interceptors.\nThis turn, each attacking creature that damages you also deals equal damage to the attacking player.').
card_first_print('harsh justice', 'POR').
card_image_name('harsh justice'/'POR', 'harsh justice').
card_uid('harsh justice'/'POR', 'POR:Harsh Justice:harsh justice').
card_rarity('harsh justice'/'POR', 'Rare').
card_artist('harsh justice'/'POR', 'John Coulthart').
card_multiverse_id('harsh justice'/'POR', '4386').

card_in_set('highland giant', 'POR').
card_original_type('highland giant'/'POR', 'Summon — Creature').
card_original_text('highland giant'/'POR', '').
card_first_print('highland giant', 'POR').
card_image_name('highland giant'/'POR', 'highland giant').
card_uid('highland giant'/'POR', 'POR:Highland Giant:highland giant').
card_rarity('highland giant'/'POR', 'Common').
card_artist('highland giant'/'POR', 'Ron Spencer').
card_flavor_text('highland giant'/'POR', 'Though slow-witted and slow-moving, they are quick to anger.').
card_multiverse_id('highland giant'/'POR', '4343').

card_in_set('hill giant', 'POR').
card_original_type('hill giant'/'POR', 'Summon — Creature').
card_original_text('hill giant'/'POR', '').
card_image_name('hill giant'/'POR', 'hill giant').
card_uid('hill giant'/'POR', 'POR:Hill Giant:hill giant').
card_rarity('hill giant'/'POR', 'Common').
card_artist('hill giant'/'POR', 'Randy Gallegos').
card_flavor_text('hill giant'/'POR', 'Hill giants are mostly just big. Of course, that does count for a lot!').
card_multiverse_id('hill giant'/'POR', '4344').

card_in_set('horned turtle', 'POR').
card_original_type('horned turtle'/'POR', 'Summon — Creature').
card_original_text('horned turtle'/'POR', '').
card_first_print('horned turtle', 'POR').
card_image_name('horned turtle'/'POR', 'horned turtle').
card_uid('horned turtle'/'POR', 'POR:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'POR', 'Common').
card_artist('horned turtle'/'POR', 'Adrian Smith').
card_flavor_text('horned turtle'/'POR', 'Its spear has no shaft, its shield no handle.').
card_multiverse_id('horned turtle'/'POR', '4264').

card_in_set('howling fury', 'POR').
card_original_type('howling fury'/'POR', 'Sorcery').
card_original_text('howling fury'/'POR', 'Any one creature gets +4/+0 until the end of the turn.').
card_first_print('howling fury', 'POR').
card_image_name('howling fury'/'POR', 'howling fury').
card_uid('howling fury'/'POR', 'POR:Howling Fury:howling fury').
card_rarity('howling fury'/'POR', 'Common').
card_artist('howling fury'/'POR', 'Mike Dringenberg').
card_flavor_text('howling fury'/'POR', 'I howl my soul to the moon, and the moon howls with me.').
card_multiverse_id('howling fury'/'POR', '4226').

card_in_set('hulking cyclops', 'POR').
card_original_type('hulking cyclops'/'POR', 'Summon — Creature').
card_original_text('hulking cyclops'/'POR', 'Hulking Cyclops can\'t intercept.').
card_image_name('hulking cyclops'/'POR', 'hulking cyclops').
card_uid('hulking cyclops'/'POR', 'POR:Hulking Cyclops:hulking cyclops').
card_rarity('hulking cyclops'/'POR', 'Uncommon').
card_artist('hulking cyclops'/'POR', 'Paolo Parente').
card_flavor_text('hulking cyclops'/'POR', 'Anyone can get around a cyclops, but few can stand in its way.').
card_multiverse_id('hulking cyclops'/'POR', '4345').

card_in_set('hulking goblin', 'POR').
card_original_type('hulking goblin'/'POR', 'Summon — Creature').
card_original_text('hulking goblin'/'POR', 'Hulking Goblin can\'t intercept.').
card_first_print('hulking goblin', 'POR').
card_image_name('hulking goblin'/'POR', 'hulking goblin').
card_uid('hulking goblin'/'POR', 'POR:Hulking Goblin:hulking goblin').
card_rarity('hulking goblin'/'POR', 'Common').
card_artist('hulking goblin'/'POR', 'Pete Venters').
card_flavor_text('hulking goblin'/'POR', 'The bigger they are, the harder they avoid work.').
card_multiverse_id('hulking goblin'/'POR', '4346').

card_in_set('hurricane', 'POR').
card_original_type('hurricane'/'POR', 'Sorcery').
card_original_text('hurricane'/'POR', 'Hurricane deals X damage to each player and each creature with flying. (This includes you and your creatures with flying.)').
card_image_name('hurricane'/'POR', 'hurricane').
card_uid('hurricane'/'POR', 'POR:Hurricane:hurricane').
card_rarity('hurricane'/'POR', 'Rare').
card_artist('hurricane'/'POR', 'Andrew Robinson').
card_multiverse_id('hurricane'/'POR', '4301').

card_in_set('ingenious thief', 'POR').
card_original_type('ingenious thief'/'POR', 'Summon — Creature').
card_original_text('ingenious thief'/'POR', 'Flying\nWhen Ingenious Thief comes into play from your hand, look at your opponent\'s hand.').
card_first_print('ingenious thief', 'POR').
card_image_name('ingenious thief'/'POR', 'ingenious thief').
card_uid('ingenious thief'/'POR', 'POR:Ingenious Thief:ingenious thief').
card_rarity('ingenious thief'/'POR', 'Uncommon').
card_artist('ingenious thief'/'POR', 'Dan Frazier').
card_multiverse_id('ingenious thief'/'POR', '4265').

card_in_set('island', 'POR').
card_original_type('island'/'POR', 'Land').
card_original_text('island'/'POR', 'U').
card_image_name('island'/'POR', 'island1').
card_uid('island'/'POR', 'POR:Island:island1').
card_rarity('island'/'POR', 'Basic Land').
card_artist('island'/'POR', 'Eric Peterson').
card_multiverse_id('island'/'POR', '4421').

card_in_set('island', 'POR').
card_original_type('island'/'POR', 'Land').
card_original_text('island'/'POR', 'U').
card_image_name('island'/'POR', 'island2').
card_uid('island'/'POR', 'POR:Island:island2').
card_rarity('island'/'POR', 'Basic Land').
card_artist('island'/'POR', 'Eric Peterson').
card_multiverse_id('island'/'POR', '4422').

card_in_set('island', 'POR').
card_original_type('island'/'POR', 'Land').
card_original_text('island'/'POR', 'U').
card_image_name('island'/'POR', 'island3').
card_uid('island'/'POR', 'POR:Island:island3').
card_rarity('island'/'POR', 'Basic Land').
card_artist('island'/'POR', 'Eric Peterson').
card_multiverse_id('island'/'POR', '4423').

card_in_set('island', 'POR').
card_original_type('island'/'POR', 'Land').
card_original_text('island'/'POR', 'U').
card_image_name('island'/'POR', 'island4').
card_uid('island'/'POR', 'POR:Island:island4').
card_rarity('island'/'POR', 'Basic Land').
card_artist('island'/'POR', 'Eric Peterson').
card_multiverse_id('island'/'POR', '4424').

card_in_set('jungle lion', 'POR').
card_original_type('jungle lion'/'POR', 'Summon — Creature').
card_original_text('jungle lion'/'POR', 'Jungle Lion can\'t intercept.').
card_first_print('jungle lion', 'POR').
card_image_name('jungle lion'/'POR', 'jungle lion').
card_uid('jungle lion'/'POR', 'POR:Jungle Lion:jungle lion').
card_rarity('jungle lion'/'POR', 'Common').
card_artist('jungle lion'/'POR', 'Janine Johnston').
card_flavor_text('jungle lion'/'POR', 'The lion\'s only loyalty is to its hunger.').
card_multiverse_id('jungle lion'/'POR', '4302').

card_in_set('keen-eyed archers', 'POR').
card_original_type('keen-eyed archers'/'POR', 'Summon — Creature').
card_original_text('keen-eyed archers'/'POR', 'Keen-Eyed Archers can intercept as though it had flying.').
card_first_print('keen-eyed archers', 'POR').
card_image_name('keen-eyed archers'/'POR', 'keen-eyed archers').
card_uid('keen-eyed archers'/'POR', 'POR:Keen-Eyed Archers:keen-eyed archers').
card_rarity('keen-eyed archers'/'POR', 'Common').
card_artist('keen-eyed archers'/'POR', 'Alan Rabinowitz').
card_flavor_text('keen-eyed archers'/'POR', '\"If it has wings, shoot it. If it doesn\'t, shoot it anyway.\"').
card_multiverse_id('keen-eyed archers'/'POR', '4387').

card_in_set('king\'s assassin', 'POR').
card_original_type('king\'s assassin'/'POR', 'Summon — Creature').
card_original_text('king\'s assassin'/'POR', 'On your turn, before you attack, you may tap King\'s Assassin to destroy any one tapped creature.').
card_first_print('king\'s assassin', 'POR').
card_image_name('king\'s assassin'/'POR', 'king\'s assassin').
card_uid('king\'s assassin'/'POR', 'POR:King\'s Assassin:king\'s assassin').
card_rarity('king\'s assassin'/'POR', 'Rare').
card_artist('king\'s assassin'/'POR', 'Zina Saunders').
card_flavor_text('king\'s assassin'/'POR', 'Fear is as effective a weapon as the blade.').
card_multiverse_id('king\'s assassin'/'POR', '4227').

card_in_set('knight errant', 'POR').
card_original_type('knight errant'/'POR', 'Summon — Creature').
card_original_text('knight errant'/'POR', '').
card_first_print('knight errant', 'POR').
card_image_name('knight errant'/'POR', 'knight errant').
card_uid('knight errant'/'POR', 'POR:Knight Errant:knight errant').
card_rarity('knight errant'/'POR', 'Common').
card_artist('knight errant'/'POR', 'Dan Frazier').
card_flavor_text('knight errant'/'POR', '\". . . Before honor is humility.\"\n—The Bible, Proverbs 15:33').
card_multiverse_id('knight errant'/'POR', '4388').

card_in_set('last chance', 'POR').
card_original_type('last chance'/'POR', 'Sorcery').
card_original_text('last chance'/'POR', 'Take another turn after this one. You lose the game at the end of that turn. (You don\'t lose if you\'ve already won.)').
card_first_print('last chance', 'POR').
card_image_name('last chance'/'POR', 'last chance').
card_uid('last chance'/'POR', 'POR:Last Chance:last chance').
card_rarity('last chance'/'POR', 'Rare').
card_artist('last chance'/'POR', 'Hannibal King').
card_multiverse_id('last chance'/'POR', '4347').

card_in_set('lava axe', 'POR').
card_original_type('lava axe'/'POR', 'Sorcery').
card_original_text('lava axe'/'POR', 'Lava Axe deals 5 damage to your opponent.').
card_first_print('lava axe', 'POR').
card_image_name('lava axe'/'POR', 'lava axe').
card_uid('lava axe'/'POR', 'POR:Lava Axe:lava axe').
card_rarity('lava axe'/'POR', 'Common').
card_artist('lava axe'/'POR', 'Adrian Smith').
card_flavor_text('lava axe'/'POR', 'Swing your axe as a broom, to sweep away the foe.').
card_multiverse_id('lava axe'/'POR', '4348').

card_in_set('lava flow', 'POR').
card_original_type('lava flow'/'POR', 'Sorcery').
card_original_text('lava flow'/'POR', 'Destroy any one creature or land.').
card_first_print('lava flow', 'POR').
card_image_name('lava flow'/'POR', 'lava flow').
card_uid('lava flow'/'POR', 'POR:Lava Flow:lava flow').
card_rarity('lava flow'/'POR', 'Uncommon').
card_artist('lava flow'/'POR', 'Mike Dringenberg').
card_flavor_text('lava flow'/'POR', 'People ran as never before at the thought of being buried and cremated at the same time.').
card_multiverse_id('lava flow'/'POR', '4349').

card_in_set('lizard warrior', 'POR').
card_original_type('lizard warrior'/'POR', 'Summon — Creature').
card_original_text('lizard warrior'/'POR', '').
card_first_print('lizard warrior', 'POR').
card_image_name('lizard warrior'/'POR', 'lizard warrior').
card_uid('lizard warrior'/'POR', 'POR:Lizard Warrior:lizard warrior').
card_rarity('lizard warrior'/'POR', 'Common').
card_artist('lizard warrior'/'POR', 'Roger Raupp').
card_flavor_text('lizard warrior'/'POR', 'Don\'t let its appearance frighten you. Let its claws and teeth do that.').
card_multiverse_id('lizard warrior'/'POR', '4350').

card_in_set('man-o\'-war', 'POR').
card_original_type('man-o\'-war'/'POR', 'Summon — Creature').
card_original_text('man-o\'-war'/'POR', 'When Man-o\'-War comes into play from your hand, return any one creature to its owner\'s hand. (If you\'re the only one with creatures, return one of them to your hand.)').
card_image_name('man-o\'-war'/'POR', 'man-o\'-war').
card_uid('man-o\'-war'/'POR', 'POR:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'POR', 'Uncommon').
card_artist('man-o\'-war'/'POR', 'Una Fricker').
card_multiverse_id('man-o\'-war'/'POR', '4266').

card_in_set('mercenary knight', 'POR').
card_original_type('mercenary knight'/'POR', 'Summon — Creature').
card_original_text('mercenary knight'/'POR', 'When Mercenary Knight comes into play from your hand, choose and discard a summon creature from your hand or destroy Mercenary Knight.').
card_first_print('mercenary knight', 'POR').
card_image_name('mercenary knight'/'POR', 'mercenary knight').
card_uid('mercenary knight'/'POR', 'POR:Mercenary Knight:mercenary knight').
card_rarity('mercenary knight'/'POR', 'Rare').
card_artist('mercenary knight'/'POR', 'Adrian Smith').
card_multiverse_id('mercenary knight'/'POR', '4228').

card_in_set('merfolk of the pearl trident', 'POR').
card_original_type('merfolk of the pearl trident'/'POR', 'Summon — Creature').
card_original_text('merfolk of the pearl trident'/'POR', '').
card_image_name('merfolk of the pearl trident'/'POR', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'POR', 'POR:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'POR', 'Common').
card_artist('merfolk of the pearl trident'/'POR', 'DiTerlizzi').
card_flavor_text('merfolk of the pearl trident'/'POR', 'Are merfolk humans with fins, or are humans merfolk with feet?').
card_multiverse_id('merfolk of the pearl trident'/'POR', '4267').

card_in_set('mind knives', 'POR').
card_original_type('mind knives'/'POR', 'Sorcery').
card_original_text('mind knives'/'POR', 'Your opponent discards a card at random from his or her hand.').
card_first_print('mind knives', 'POR').
card_image_name('mind knives'/'POR', 'mind knives').
card_uid('mind knives'/'POR', 'POR:Mind Knives:mind knives').
card_rarity('mind knives'/'POR', 'Common').
card_artist('mind knives'/'POR', 'Rebecca Guay').
card_flavor_text('mind knives'/'POR', 'To dodge the unerring knife, one must be free of thought.').
card_multiverse_id('mind knives'/'POR', '4229').

card_in_set('mind rot', 'POR').
card_original_type('mind rot'/'POR', 'Sorcery').
card_original_text('mind rot'/'POR', 'Your opponent chooses and discards two cards from his or her hand. (If your opponent has only one card, he or she discards it.)').
card_first_print('mind rot', 'POR').
card_image_name('mind rot'/'POR', 'mind rot').
card_uid('mind rot'/'POR', 'POR:Mind Rot:mind rot').
card_rarity('mind rot'/'POR', 'Common').
card_artist('mind rot'/'POR', 'Steve Luke').
card_multiverse_id('mind rot'/'POR', '4230').

card_in_set('minotaur warrior', 'POR').
card_original_type('minotaur warrior'/'POR', 'Summon — Creature').
card_original_text('minotaur warrior'/'POR', '').
card_first_print('minotaur warrior', 'POR').
card_image_name('minotaur warrior'/'POR', 'minotaur warrior').
card_uid('minotaur warrior'/'POR', 'POR:Minotaur Warrior:minotaur warrior').
card_rarity('minotaur warrior'/'POR', 'Common').
card_artist('minotaur warrior'/'POR', 'Scott M. Fischer').
card_flavor_text('minotaur warrior'/'POR', 'The herd\'s patience, the stampede\'s fury.').
card_multiverse_id('minotaur warrior'/'POR', '4351').

card_in_set('mobilize', 'POR').
card_original_type('mobilize'/'POR', 'Sorcery').
card_original_text('mobilize'/'POR', 'Untap all your creatures.').
card_first_print('mobilize', 'POR').
card_image_name('mobilize'/'POR', 'mobilize').
card_uid('mobilize'/'POR', 'POR:Mobilize:mobilize').
card_rarity('mobilize'/'POR', 'Common').
card_artist('mobilize'/'POR', 'Rebecca Guay').
card_flavor_text('mobilize'/'POR', 'A rested mind is the sharpest weapon.').
card_multiverse_id('mobilize'/'POR', '4303').

card_in_set('monstrous growth', 'POR').
card_original_type('monstrous growth'/'POR', 'Sorcery').
card_original_text('monstrous growth'/'POR', 'Any one creature gets +4/+4 until the end of the turn. (For example, a 6/3 creature would become 10/7.)').
card_first_print('monstrous growth', 'POR').
card_image_name('monstrous growth'/'POR', 'monstrous growth1').
card_uid('monstrous growth'/'POR', 'POR:Monstrous Growth:monstrous growth1').
card_rarity('monstrous growth'/'POR', 'Common').
card_artist('monstrous growth'/'POR', 'Dan Frazier').
card_multiverse_id('monstrous growth'/'POR', '4304').

card_in_set('monstrous growth', 'POR').
card_original_type('monstrous growth'/'POR', 'Sorcery').
card_original_text('monstrous growth'/'POR', 'Any one creature gets +4/+4 until the end of the turn.').
card_image_name('monstrous growth'/'POR', 'monstrous growth2').
card_uid('monstrous growth'/'POR', 'POR:Monstrous Growth:monstrous growth2').
card_rarity('monstrous growth'/'POR', 'Common').
card_artist('monstrous growth'/'POR', 'Dan Frazier').
card_flavor_text('monstrous growth'/'POR', 'Some cats are born fighters; others need a little persuading.').
card_multiverse_id('monstrous growth'/'POR', '4305').

card_in_set('moon sprite', 'POR').
card_original_type('moon sprite'/'POR', 'Summon — Creature').
card_original_text('moon sprite'/'POR', 'Flying').
card_first_print('moon sprite', 'POR').
card_image_name('moon sprite'/'POR', 'moon sprite').
card_uid('moon sprite'/'POR', 'POR:Moon Sprite:moon sprite').
card_rarity('moon sprite'/'POR', 'Uncommon').
card_artist('moon sprite'/'POR', 'Terese Nielsen').
card_flavor_text('moon sprite'/'POR', '\"I am that merry wanderer of the night.\"\n—William Shakespeare,\nA Midsummer Night\'s Dream').
card_multiverse_id('moon sprite'/'POR', '4306').

card_in_set('mountain', 'POR').
card_original_type('mountain'/'POR', 'Land').
card_original_text('mountain'/'POR', 'R').
card_image_name('mountain'/'POR', 'mountain1').
card_uid('mountain'/'POR', 'POR:Mountain:mountain1').
card_rarity('mountain'/'POR', 'Basic Land').
card_artist('mountain'/'POR', 'Brian Durfee').
card_multiverse_id('mountain'/'POR', '4417').

card_in_set('mountain', 'POR').
card_original_type('mountain'/'POR', 'Land').
card_original_text('mountain'/'POR', 'R').
card_image_name('mountain'/'POR', 'mountain2').
card_uid('mountain'/'POR', 'POR:Mountain:mountain2').
card_rarity('mountain'/'POR', 'Basic Land').
card_artist('mountain'/'POR', 'Brian Durfee').
card_multiverse_id('mountain'/'POR', '4419').

card_in_set('mountain', 'POR').
card_original_type('mountain'/'POR', 'Land').
card_original_text('mountain'/'POR', 'R').
card_image_name('mountain'/'POR', 'mountain3').
card_uid('mountain'/'POR', 'POR:Mountain:mountain3').
card_rarity('mountain'/'POR', 'Basic Land').
card_artist('mountain'/'POR', 'Brian Durfee').
card_multiverse_id('mountain'/'POR', '4418').

card_in_set('mountain', 'POR').
card_original_type('mountain'/'POR', 'Land').
card_original_text('mountain'/'POR', 'R').
card_image_name('mountain'/'POR', 'mountain4').
card_uid('mountain'/'POR', 'POR:Mountain:mountain4').
card_rarity('mountain'/'POR', 'Basic Land').
card_artist('mountain'/'POR', 'Brian Durfee').
card_multiverse_id('mountain'/'POR', '4420').

card_in_set('mountain goat', 'POR').
card_original_type('mountain goat'/'POR', 'Summon — Creature').
card_original_text('mountain goat'/'POR', 'Mountainwalk (If defending player has any mountains in play, Mountain Goat can\'t be intercepted.)').
card_image_name('mountain goat'/'POR', 'mountain goat').
card_uid('mountain goat'/'POR', 'POR:Mountain Goat:mountain goat').
card_rarity('mountain goat'/'POR', 'Uncommon').
card_artist('mountain goat'/'POR', 'Una Fricker').
card_flavor_text('mountain goat'/'POR', 'Only the heroic and the mad follow mountain goat trails.').
card_multiverse_id('mountain goat'/'POR', '4352').

card_in_set('muck rats', 'POR').
card_original_type('muck rats'/'POR', 'Summon — Creature').
card_original_text('muck rats'/'POR', '').
card_first_print('muck rats', 'POR').
card_image_name('muck rats'/'POR', 'muck rats').
card_uid('muck rats'/'POR', 'POR:Muck Rats:muck rats').
card_rarity('muck rats'/'POR', 'Common').
card_artist('muck rats'/'POR', 'Colin MacNeil').
card_flavor_text('muck rats'/'POR', 'The difference between a nuisance and a threat is often merely a matter of numbers.').
card_multiverse_id('muck rats'/'POR', '4231').

card_in_set('mystic denial', 'POR').
card_original_type('mystic denial'/'POR', 'Sorcery').
card_original_text('mystic denial'/'POR', 'Play Mystic Denial only in response to another player playing a summon creature or a sorcery. That card has no effect, and that player puts it into his or her discard pile.').
card_first_print('mystic denial', 'POR').
card_image_name('mystic denial'/'POR', 'mystic denial').
card_uid('mystic denial'/'POR', 'POR:Mystic Denial:mystic denial').
card_rarity('mystic denial'/'POR', 'Uncommon').
card_artist('mystic denial'/'POR', 'Hannibal King').
card_multiverse_id('mystic denial'/'POR', '4268').

card_in_set('natural order', 'POR').
card_original_type('natural order'/'POR', 'Sorcery').
card_original_text('natural order'/'POR', 'Search your deck for a green summon creature and put that card into play. Treat it as though you just played it from your hand. Then put one of your green creatures into your discard pile. Shuffle your deck afterwards.').
card_image_name('natural order'/'POR', 'natural order').
card_uid('natural order'/'POR', 'POR:Natural Order:natural order').
card_rarity('natural order'/'POR', 'Rare').
card_artist('natural order'/'POR', 'Alan Rabinowitz').
card_multiverse_id('natural order'/'POR', '4307').

card_in_set('natural spring', 'POR').
card_original_type('natural spring'/'POR', 'Sorcery').
card_original_text('natural spring'/'POR', 'You gain 8 life.').
card_first_print('natural spring', 'POR').
card_image_name('natural spring'/'POR', 'natural spring').
card_uid('natural spring'/'POR', 'POR:Natural Spring:natural spring').
card_rarity('natural spring'/'POR', 'Uncommon').
card_artist('natural spring'/'POR', 'Janine Johnston').
card_flavor_text('natural spring'/'POR', 'Though many have come to this place to wash their wounds, the water has never turned red.').
card_multiverse_id('natural spring'/'POR', '4308').

card_in_set('nature\'s cloak', 'POR').
card_original_type('nature\'s cloak'/'POR', 'Sorcery').
card_original_text('nature\'s cloak'/'POR', 'All your green creatures gain forestwalk until the end of the turn. (If defending player has any forests in play, none of your green creatures can be intercepted.)').
card_first_print('nature\'s cloak', 'POR').
card_image_name('nature\'s cloak'/'POR', 'nature\'s cloak').
card_uid('nature\'s cloak'/'POR', 'POR:Nature\'s Cloak:nature\'s cloak').
card_rarity('nature\'s cloak'/'POR', 'Rare').
card_artist('nature\'s cloak'/'POR', 'Rebecca Guay').
card_multiverse_id('nature\'s cloak'/'POR', '4309').

card_in_set('nature\'s lore', 'POR').
card_original_type('nature\'s lore'/'POR', 'Sorcery').
card_original_text('nature\'s lore'/'POR', 'Search your deck for a forest and put that card into play. Shuffle your deck afterwards.').
card_image_name('nature\'s lore'/'POR', 'nature\'s lore').
card_uid('nature\'s lore'/'POR', 'POR:Nature\'s Lore:nature\'s lore').
card_rarity('nature\'s lore'/'POR', 'Common').
card_artist('nature\'s lore'/'POR', 'Terese Nielsen').
card_flavor_text('nature\'s lore'/'POR', 'Nature\'s secrets can be read on every tree, every branch, every leaf.').
card_multiverse_id('nature\'s lore'/'POR', '4310').

card_in_set('nature\'s ruin', 'POR').
card_original_type('nature\'s ruin'/'POR', 'Sorcery').
card_original_text('nature\'s ruin'/'POR', 'Destroy all green creatures. (This includes your green creatures.)').
card_first_print('nature\'s ruin', 'POR').
card_image_name('nature\'s ruin'/'POR', 'nature\'s ruin').
card_uid('nature\'s ruin'/'POR', 'POR:Nature\'s Ruin:nature\'s ruin').
card_rarity('nature\'s ruin'/'POR', 'Uncommon').
card_artist('nature\'s ruin'/'POR', 'Mike Dringenberg').
card_flavor_text('nature\'s ruin'/'POR', 'One chill blast—the exhalation of the grave.').
card_multiverse_id('nature\'s ruin'/'POR', '4232').

card_in_set('needle storm', 'POR').
card_original_type('needle storm'/'POR', 'Sorcery').
card_original_text('needle storm'/'POR', 'Needle Storm deals 4 damage to each creature with flying. (This includes your creatures with flying.)').
card_first_print('needle storm', 'POR').
card_image_name('needle storm'/'POR', 'needle storm').
card_uid('needle storm'/'POR', 'POR:Needle Storm:needle storm').
card_rarity('needle storm'/'POR', 'Uncommon').
card_artist('needle storm'/'POR', 'Charles Gillespie').
card_multiverse_id('needle storm'/'POR', '4311').

card_in_set('noxious toad', 'POR').
card_original_type('noxious toad'/'POR', 'Summon — Creature').
card_original_text('noxious toad'/'POR', 'If Noxious Toad is put into your discard pile from play, your opponent chooses and discards a card from his or her hand.').
card_first_print('noxious toad', 'POR').
card_image_name('noxious toad'/'POR', 'noxious toad').
card_uid('noxious toad'/'POR', 'POR:Noxious Toad:noxious toad').
card_rarity('noxious toad'/'POR', 'Uncommon').
card_artist('noxious toad'/'POR', 'Adrian Smith').
card_multiverse_id('noxious toad'/'POR', '4233').

card_in_set('omen', 'POR').
card_original_type('omen'/'POR', 'Sorcery').
card_original_text('omen'/'POR', 'Look at the top three cards of your deck and return them in any order. You may choose to shuffle your deck. Then draw a card.').
card_first_print('omen', 'POR').
card_image_name('omen'/'POR', 'omen').
card_uid('omen'/'POR', 'POR:Omen:omen').
card_rarity('omen'/'POR', 'Common').
card_artist('omen'/'POR', 'Eric Peterson').
card_multiverse_id('omen'/'POR', '4269').

card_in_set('owl familiar', 'POR').
card_original_type('owl familiar'/'POR', 'Summon — Creature').
card_original_text('owl familiar'/'POR', 'Flying\nWhen Owl Familiar comes into play from your hand, draw a card, then choose and discard a card from your hand.').
card_first_print('owl familiar', 'POR').
card_image_name('owl familiar'/'POR', 'owl familiar').
card_uid('owl familiar'/'POR', 'POR:Owl Familiar:owl familiar').
card_rarity('owl familiar'/'POR', 'Common').
card_artist('owl familiar'/'POR', 'Janine Johnston').
card_multiverse_id('owl familiar'/'POR', '4270').

card_in_set('panther warriors', 'POR').
card_original_type('panther warriors'/'POR', 'Summon — Creature').
card_original_text('panther warriors'/'POR', '').
card_image_name('panther warriors'/'POR', 'panther warriors').
card_uid('panther warriors'/'POR', 'POR:Panther Warriors:panther warriors').
card_rarity('panther warriors'/'POR', 'Common').
card_artist('panther warriors'/'POR', 'Eric Peterson').
card_flavor_text('panther warriors'/'POR', 'The dogs of war are nothing compared to the cats.').
card_multiverse_id('panther warriors'/'POR', '9845').

card_in_set('path of peace', 'POR').
card_original_type('path of peace'/'POR', 'Sorcery').
card_original_text('path of peace'/'POR', 'Destroy any one creature. That creature\'s owner gains 4 life.').
card_first_print('path of peace', 'POR').
card_image_name('path of peace'/'POR', 'path of peace').
card_uid('path of peace'/'POR', 'POR:Path of Peace:path of peace').
card_rarity('path of peace'/'POR', 'Common').
card_artist('path of peace'/'POR', 'Pete Venters').
card_flavor_text('path of peace'/'POR', 'The soldier reaped the profits of peace.').
card_multiverse_id('path of peace'/'POR', '4389').

card_in_set('personal tutor', 'POR').
card_original_type('personal tutor'/'POR', 'Sorcery').
card_original_text('personal tutor'/'POR', 'Search your deck for a sorcery and reveal that card to all players. Shuffle your deck and put the revealed card on top of it.').
card_first_print('personal tutor', 'POR').
card_image_name('personal tutor'/'POR', 'personal tutor').
card_uid('personal tutor'/'POR', 'POR:Personal Tutor:personal tutor').
card_rarity('personal tutor'/'POR', 'Uncommon').
card_artist('personal tutor'/'POR', 'D. Alexander Gregory').
card_multiverse_id('personal tutor'/'POR', '4271').

card_in_set('phantom warrior', 'POR').
card_original_type('phantom warrior'/'POR', 'Summon — Creature').
card_original_text('phantom warrior'/'POR', 'Phantom Warrior can\'t be intercepted.').
card_first_print('phantom warrior', 'POR').
card_image_name('phantom warrior'/'POR', 'phantom warrior').
card_uid('phantom warrior'/'POR', 'POR:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'POR', 'Rare').
card_artist('phantom warrior'/'POR', 'Dan Frazier').
card_flavor_text('phantom warrior'/'POR', 'The phantom warrior comes from nowhere and returns there just as quickly.').
card_multiverse_id('phantom warrior'/'POR', '4272').

card_in_set('pillaging horde', 'POR').
card_original_type('pillaging horde'/'POR', 'Summon — Creature').
card_original_text('pillaging horde'/'POR', 'When Pillaging Horde comes into play from your hand, discard a card at random from your hand or destroy Pillaging Horde.').
card_first_print('pillaging horde', 'POR').
card_image_name('pillaging horde'/'POR', 'pillaging horde').
card_uid('pillaging horde'/'POR', 'POR:Pillaging Horde:pillaging horde').
card_rarity('pillaging horde'/'POR', 'Rare').
card_artist('pillaging horde'/'POR', 'Kev Walker').
card_multiverse_id('pillaging horde'/'POR', '4353').

card_in_set('plains', 'POR').
card_original_type('plains'/'POR', 'Land').
card_original_text('plains'/'POR', 'W').
card_image_name('plains'/'POR', 'plains1').
card_uid('plains'/'POR', 'POR:Plains:plains1').
card_rarity('plains'/'POR', 'Basic Land').
card_artist('plains'/'POR', 'Douglas Shuler').
card_multiverse_id('plains'/'POR', '4425').

card_in_set('plains', 'POR').
card_original_type('plains'/'POR', 'Land').
card_original_text('plains'/'POR', 'W').
card_image_name('plains'/'POR', 'plains2').
card_uid('plains'/'POR', 'POR:Plains:plains2').
card_rarity('plains'/'POR', 'Basic Land').
card_artist('plains'/'POR', 'Douglas Shuler').
card_multiverse_id('plains'/'POR', '4426').

card_in_set('plains', 'POR').
card_original_type('plains'/'POR', 'Land').
card_original_text('plains'/'POR', 'W').
card_image_name('plains'/'POR', 'plains3').
card_uid('plains'/'POR', 'POR:Plains:plains3').
card_rarity('plains'/'POR', 'Basic Land').
card_artist('plains'/'POR', 'Douglas Shuler').
card_multiverse_id('plains'/'POR', '4427').

card_in_set('plains', 'POR').
card_original_type('plains'/'POR', 'Land').
card_original_text('plains'/'POR', 'W').
card_image_name('plains'/'POR', 'plains4').
card_uid('plains'/'POR', 'POR:Plains:plains4').
card_rarity('plains'/'POR', 'Basic Land').
card_artist('plains'/'POR', 'Douglas Shuler').
card_multiverse_id('plains'/'POR', '4428').

card_in_set('plant elemental', 'POR').
card_original_type('plant elemental'/'POR', 'Summon — Creature').
card_original_text('plant elemental'/'POR', 'When Plant Elemental comes into play from your hand, destroy one of your forests or destroy Plant Elemental.').
card_first_print('plant elemental', 'POR').
card_image_name('plant elemental'/'POR', 'plant elemental').
card_uid('plant elemental'/'POR', 'POR:Plant Elemental:plant elemental').
card_rarity('plant elemental'/'POR', 'Uncommon').
card_artist('plant elemental'/'POR', 'Ted Naifeh').
card_multiverse_id('plant elemental'/'POR', '4313').

card_in_set('primeval force', 'POR').
card_original_type('primeval force'/'POR', 'Summon — Creature').
card_original_text('primeval force'/'POR', 'When Primeval Force comes into play from your hand, destroy three of your forests or destroy Primeval Force.').
card_first_print('primeval force', 'POR').
card_image_name('primeval force'/'POR', 'primeval force').
card_uid('primeval force'/'POR', 'POR:Primeval Force:primeval force').
card_rarity('primeval force'/'POR', 'Rare').
card_artist('primeval force'/'POR', 'Randy Gallegos').
card_multiverse_id('primeval force'/'POR', '4314').

card_in_set('prosperity', 'POR').
card_original_type('prosperity'/'POR', 'Sorcery').
card_original_text('prosperity'/'POR', 'Each player draws X cards.').
card_image_name('prosperity'/'POR', 'prosperity').
card_uid('prosperity'/'POR', 'POR:Prosperity:prosperity').
card_rarity('prosperity'/'POR', 'Rare').
card_artist('prosperity'/'POR', 'Phil Foglio').
card_flavor_text('prosperity'/'POR', '\"Life can never be too good.\"').
card_multiverse_id('prosperity'/'POR', '4273').

card_in_set('pyroclasm', 'POR').
card_original_type('pyroclasm'/'POR', 'Sorcery').
card_original_text('pyroclasm'/'POR', 'Pyroclasm deals 2 damage to each creature. (This includes your creatures.)').
card_image_name('pyroclasm'/'POR', 'pyroclasm').
card_uid('pyroclasm'/'POR', 'POR:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'POR', 'Rare').
card_artist('pyroclasm'/'POR', 'John Matson').
card_flavor_text('pyroclasm'/'POR', 'Chaos does not choose its enemies.').
card_multiverse_id('pyroclasm'/'POR', '4354').

card_in_set('python', 'POR').
card_original_type('python'/'POR', 'Summon — Creature').
card_original_text('python'/'POR', '').
card_image_name('python'/'POR', 'python').
card_uid('python'/'POR', 'POR:Python:python').
card_rarity('python'/'POR', 'Common').
card_artist('python'/'POR', 'Alan Rabinowitz').
card_flavor_text('python'/'POR', 'What could it have swallowed to grow so large?').
card_multiverse_id('python'/'POR', '4234').

card_in_set('raging cougar', 'POR').
card_original_type('raging cougar'/'POR', 'Summon — Creature').
card_original_text('raging cougar'/'POR', 'Raging Cougar is unaffected by summoning sickness.').
card_first_print('raging cougar', 'POR').
card_image_name('raging cougar'/'POR', 'raging cougar').
card_uid('raging cougar'/'POR', 'POR:Raging Cougar:raging cougar').
card_rarity('raging cougar'/'POR', 'Common').
card_artist('raging cougar'/'POR', 'Terese Nielsen').
card_flavor_text('raging cougar'/'POR', 'Mountaineers quickly learn to travel with their spears always pointed up.').
card_multiverse_id('raging cougar'/'POR', '4355').

card_in_set('raging goblin', 'POR').
card_original_type('raging goblin'/'POR', 'Summon — Creature').
card_original_text('raging goblin'/'POR', 'Raging Goblin is unaffected by summoning sickness. (It can attack the turn you play it, unless another card prevents this.)').
card_first_print('raging goblin', 'POR').
card_image_name('raging goblin'/'POR', 'raging goblin1').
card_uid('raging goblin'/'POR', 'POR:Raging Goblin:raging goblin1').
card_rarity('raging goblin'/'POR', 'Common').
card_artist('raging goblin'/'POR', 'Pete Venters').
card_multiverse_id('raging goblin'/'POR', '4356').

card_in_set('raging goblin', 'POR').
card_original_type('raging goblin'/'POR', 'Summon — Creature').
card_original_text('raging goblin'/'POR', 'Raging Goblin is unaffected by summoning sickness.').
card_image_name('raging goblin'/'POR', 'raging goblin2').
card_uid('raging goblin'/'POR', 'POR:Raging Goblin:raging goblin2').
card_rarity('raging goblin'/'POR', 'Common').
card_artist('raging goblin'/'POR', 'Pete Venters').
card_flavor_text('raging goblin'/'POR', 'Charging alone takes uncommon daring or uncommon stupidity. Or both.').
card_multiverse_id('raging goblin'/'POR', '4357').

card_in_set('raging minotaur', 'POR').
card_original_type('raging minotaur'/'POR', 'Summon — Creature').
card_original_text('raging minotaur'/'POR', 'Raging Minotaur is unaffected by summoning sickness.').
card_first_print('raging minotaur', 'POR').
card_image_name('raging minotaur'/'POR', 'raging minotaur').
card_uid('raging minotaur'/'POR', 'POR:Raging Minotaur:raging minotaur').
card_rarity('raging minotaur'/'POR', 'Common').
card_artist('raging minotaur'/'POR', 'Scott M. Fischer').
card_flavor_text('raging minotaur'/'POR', 'The only thing worse than a minotaur with an axe is an angry minotaur with an axe.').
card_multiverse_id('raging minotaur'/'POR', '4358').

card_in_set('rain of salt', 'POR').
card_original_type('rain of salt'/'POR', 'Sorcery').
card_original_text('rain of salt'/'POR', 'Destroy any two lands.').
card_first_print('rain of salt', 'POR').
card_image_name('rain of salt'/'POR', 'rain of salt').
card_uid('rain of salt'/'POR', 'POR:Rain of Salt:rain of salt').
card_rarity('rain of salt'/'POR', 'Uncommon').
card_artist('rain of salt'/'POR', 'Charles Gillespie').
card_flavor_text('rain of salt'/'POR', 'It pelts the land with its fury,\nspoiling what was once sweet.').
card_multiverse_id('rain of salt'/'POR', '4359').

card_in_set('rain of tears', 'POR').
card_original_type('rain of tears'/'POR', 'Sorcery').
card_original_text('rain of tears'/'POR', 'Destroy any one land.').
card_first_print('rain of tears', 'POR').
card_image_name('rain of tears'/'POR', 'rain of tears').
card_uid('rain of tears'/'POR', 'POR:Rain of Tears:rain of tears').
card_rarity('rain of tears'/'POR', 'Uncommon').
card_artist('rain of tears'/'POR', 'Eric Peterson').
card_flavor_text('rain of tears'/'POR', 'When emotions rain down, what rivers will be born?').
card_multiverse_id('rain of tears'/'POR', '4235').

card_in_set('raise dead', 'POR').
card_original_type('raise dead'/'POR', 'Sorcery').
card_original_text('raise dead'/'POR', 'Return any one summon creature from your discard pile to your hand.').
card_image_name('raise dead'/'POR', 'raise dead').
card_uid('raise dead'/'POR', 'POR:Raise Dead:raise dead').
card_rarity('raise dead'/'POR', 'Common').
card_artist('raise dead'/'POR', 'Charles Gillespie').
card_flavor_text('raise dead'/'POR', 'The earth cannot hold that which magic commands.').
card_multiverse_id('raise dead'/'POR', '4236').

card_in_set('redwood treefolk', 'POR').
card_original_type('redwood treefolk'/'POR', 'Summon — Creature').
card_original_text('redwood treefolk'/'POR', '').
card_first_print('redwood treefolk', 'POR').
card_image_name('redwood treefolk'/'POR', 'redwood treefolk').
card_uid('redwood treefolk'/'POR', 'POR:Redwood Treefolk:redwood treefolk').
card_rarity('redwood treefolk'/'POR', 'Common').
card_artist('redwood treefolk'/'POR', 'Steve Luke').
card_flavor_text('redwood treefolk'/'POR', 'The soldiers chopped and chopped, and still the great tree stood, frowning down at them.').
card_multiverse_id('redwood treefolk'/'POR', '4315').

card_in_set('regal unicorn', 'POR').
card_original_type('regal unicorn'/'POR', 'Summon — Creature').
card_original_text('regal unicorn'/'POR', '').
card_first_print('regal unicorn', 'POR').
card_image_name('regal unicorn'/'POR', 'regal unicorn').
card_uid('regal unicorn'/'POR', 'POR:Regal Unicorn:regal unicorn').
card_rarity('regal unicorn'/'POR', 'Common').
card_artist('regal unicorn'/'POR', 'Zina Saunders').
card_flavor_text('regal unicorn'/'POR', 'Unicorns don\'t care if you believe in them any more than you care if they believe in you.').
card_multiverse_id('regal unicorn'/'POR', '4390').

card_in_set('renewing dawn', 'POR').
card_original_type('renewing dawn'/'POR', 'Sorcery').
card_original_text('renewing dawn'/'POR', 'For each mountain your opponent has in play, you gain 2 life.').
card_first_print('renewing dawn', 'POR').
card_image_name('renewing dawn'/'POR', 'renewing dawn').
card_uid('renewing dawn'/'POR', 'POR:Renewing Dawn:renewing dawn').
card_rarity('renewing dawn'/'POR', 'Uncommon').
card_artist('renewing dawn'/'POR', 'John Avon').
card_flavor_text('renewing dawn'/'POR', 'Dawn brings a new day, and a new day brings hope.').
card_multiverse_id('renewing dawn'/'POR', '4391').

card_in_set('rowan treefolk', 'POR').
card_original_type('rowan treefolk'/'POR', 'Summon — Creature').
card_original_text('rowan treefolk'/'POR', '').
card_first_print('rowan treefolk', 'POR').
card_image_name('rowan treefolk'/'POR', 'rowan treefolk').
card_uid('rowan treefolk'/'POR', 'POR:Rowan Treefolk:rowan treefolk').
card_rarity('rowan treefolk'/'POR', 'Common').
card_artist('rowan treefolk'/'POR', 'Gerry Grace').
card_flavor_text('rowan treefolk'/'POR', 'One of the forest\'s best protectors is the forest itself.').
card_multiverse_id('rowan treefolk'/'POR', '4316').

card_in_set('sacred knight', 'POR').
card_original_type('sacred knight'/'POR', 'Summon — Creature').
card_original_text('sacred knight'/'POR', 'Sacred Knight can\'t be intercepted by black or red creatures.').
card_first_print('sacred knight', 'POR').
card_image_name('sacred knight'/'POR', 'sacred knight').
card_uid('sacred knight'/'POR', 'POR:Sacred Knight:sacred knight').
card_rarity('sacred knight'/'POR', 'Common').
card_artist('sacred knight'/'POR', 'Donato Giancola').
card_flavor_text('sacred knight'/'POR', '\"No flame, no horror can sway me from my cause.\"').
card_multiverse_id('sacred knight'/'POR', '4392').

card_in_set('sacred nectar', 'POR').
card_original_type('sacred nectar'/'POR', 'Sorcery').
card_original_text('sacred nectar'/'POR', 'You gain 4 life.').
card_first_print('sacred nectar', 'POR').
card_image_name('sacred nectar'/'POR', 'sacred nectar').
card_uid('sacred nectar'/'POR', 'POR:Sacred Nectar:sacred nectar').
card_rarity('sacred nectar'/'POR', 'Common').
card_artist('sacred nectar'/'POR', 'Janine Johnston').
card_flavor_text('sacred nectar'/'POR', '\"For he on honey-dew hath fed,\nAnd drunk the milk of Paradise.\"\n—Samuel Taylor Coleridge, \"Kubla Khan\"').
card_multiverse_id('sacred nectar'/'POR', '4393').

card_in_set('scorching spear', 'POR').
card_original_type('scorching spear'/'POR', 'Sorcery').
card_original_text('scorching spear'/'POR', 'Scorching Spear deals 1 damage to any one creature or player.').
card_first_print('scorching spear', 'POR').
card_image_name('scorching spear'/'POR', 'scorching spear').
card_uid('scorching spear'/'POR', 'POR:Scorching Spear:scorching spear').
card_rarity('scorching spear'/'POR', 'Common').
card_artist('scorching spear'/'POR', 'Mike Raabe').
card_flavor_text('scorching spear'/'POR', 'Lift your spear as you might lift your glass, and toast your enemy.').
card_multiverse_id('scorching spear'/'POR', '4360').

card_in_set('scorching winds', 'POR').
card_original_type('scorching winds'/'POR', 'Sorcery').
card_original_text('scorching winds'/'POR', 'Play Scorching Winds only after you\'re attacked, before you declare interceptors.\nScorching Winds deals 1 damage to each attacking creature.').
card_first_print('scorching winds', 'POR').
card_image_name('scorching winds'/'POR', 'scorching winds').
card_uid('scorching winds'/'POR', 'POR:Scorching Winds:scorching winds').
card_rarity('scorching winds'/'POR', 'Uncommon').
card_artist('scorching winds'/'POR', 'D. Alexander Gregory').
card_multiverse_id('scorching winds'/'POR', '4361').

card_in_set('seasoned marshal', 'POR').
card_original_type('seasoned marshal'/'POR', 'Summon — Creature').
card_original_text('seasoned marshal'/'POR', 'If Seasoned Marshal attacks, you may choose to tap any one creature. (Tapped creatures can\'t intercept.)').
card_first_print('seasoned marshal', 'POR').
card_image_name('seasoned marshal'/'POR', 'seasoned marshal').
card_uid('seasoned marshal'/'POR', 'POR:Seasoned Marshal:seasoned marshal').
card_rarity('seasoned marshal'/'POR', 'Uncommon').
card_artist('seasoned marshal'/'POR', 'Zina Saunders').
card_multiverse_id('seasoned marshal'/'POR', '4394').

card_in_set('serpent assassin', 'POR').
card_original_type('serpent assassin'/'POR', 'Summon — Creature').
card_original_text('serpent assassin'/'POR', 'When Serpent Assassin comes into play from your hand, you may choose to destroy any one creature that isn\'t black.').
card_first_print('serpent assassin', 'POR').
card_image_name('serpent assassin'/'POR', 'serpent assassin').
card_uid('serpent assassin'/'POR', 'POR:Serpent Assassin:serpent assassin').
card_rarity('serpent assassin'/'POR', 'Rare').
card_artist('serpent assassin'/'POR', 'Roger Raupp').
card_multiverse_id('serpent assassin'/'POR', '4237').

card_in_set('serpent warrior', 'POR').
card_original_type('serpent warrior'/'POR', 'Summon — Creature').
card_original_text('serpent warrior'/'POR', 'When Serpent Warrior comes into play from your hand, you lose 3 life. (The person who plays Serpent Warrior loses the life.)').
card_first_print('serpent warrior', 'POR').
card_image_name('serpent warrior'/'POR', 'serpent warrior').
card_uid('serpent warrior'/'POR', 'POR:Serpent Warrior:serpent warrior').
card_rarity('serpent warrior'/'POR', 'Common').
card_artist('serpent warrior'/'POR', 'Roger Raupp').
card_multiverse_id('serpent warrior'/'POR', '4238').

card_in_set('skeletal crocodile', 'POR').
card_original_type('skeletal crocodile'/'POR', 'Summon — Creature').
card_original_text('skeletal crocodile'/'POR', '').
card_first_print('skeletal crocodile', 'POR').
card_image_name('skeletal crocodile'/'POR', 'skeletal crocodile').
card_uid('skeletal crocodile'/'POR', 'POR:Skeletal Crocodile:skeletal crocodile').
card_rarity('skeletal crocodile'/'POR', 'Common').
card_artist('skeletal crocodile'/'POR', 'Mike Dringenberg').
card_flavor_text('skeletal crocodile'/'POR', 'The less flesh there is, the more teeth there seem to be.').
card_multiverse_id('skeletal crocodile'/'POR', '4239').

card_in_set('skeletal snake', 'POR').
card_original_type('skeletal snake'/'POR', 'Summon — Creature').
card_original_text('skeletal snake'/'POR', '').
card_first_print('skeletal snake', 'POR').
card_image_name('skeletal snake'/'POR', 'skeletal snake').
card_uid('skeletal snake'/'POR', 'POR:Skeletal Snake:skeletal snake').
card_rarity('skeletal snake'/'POR', 'Common').
card_artist('skeletal snake'/'POR', 'John Matson').
card_flavor_text('skeletal snake'/'POR', 'They watched as the snake shed layer after layer until only cold bone remained.').
card_multiverse_id('skeletal snake'/'POR', '4240').

card_in_set('snapping drake', 'POR').
card_original_type('snapping drake'/'POR', 'Summon — Creature').
card_original_text('snapping drake'/'POR', 'Flying').
card_first_print('snapping drake', 'POR').
card_image_name('snapping drake'/'POR', 'snapping drake').
card_uid('snapping drake'/'POR', 'POR:Snapping Drake:snapping drake').
card_rarity('snapping drake'/'POR', 'Common').
card_artist('snapping drake'/'POR', 'Christopher Rush').
card_flavor_text('snapping drake'/'POR', 'Drakes claim to be dragons—until the dragons show up.').
card_multiverse_id('snapping drake'/'POR', '4274').

card_in_set('sorcerous sight', 'POR').
card_original_type('sorcerous sight'/'POR', 'Sorcery').
card_original_text('sorcerous sight'/'POR', 'Look at your opponent\'s hand. You draw a card. (Draw the card from your deck.)').
card_first_print('sorcerous sight', 'POR').
card_image_name('sorcerous sight'/'POR', 'sorcerous sight').
card_uid('sorcerous sight'/'POR', 'POR:Sorcerous Sight:sorcerous sight').
card_rarity('sorcerous sight'/'POR', 'Common').
card_artist('sorcerous sight'/'POR', 'Kaja Foglio').
card_flavor_text('sorcerous sight'/'POR', 'Do not react; anticipate.').
card_multiverse_id('sorcerous sight'/'POR', '4275').

card_in_set('soul shred', 'POR').
card_original_type('soul shred'/'POR', 'Sorcery').
card_original_text('soul shred'/'POR', 'Soul Shred deals 3 damage to any one creature that isn\'t black. You gain 3 life.').
card_first_print('soul shred', 'POR').
card_image_name('soul shred'/'POR', 'soul shred').
card_uid('soul shred'/'POR', 'POR:Soul Shred:soul shred').
card_rarity('soul shred'/'POR', 'Common').
card_artist('soul shred'/'POR', 'Alan Rabinowitz').
card_flavor_text('soul shred'/'POR', 'It would be a shame to let life slip away to nothing.').
card_multiverse_id('soul shred'/'POR', '4241').

card_in_set('spined wurm', 'POR').
card_original_type('spined wurm'/'POR', 'Summon — Creature').
card_original_text('spined wurm'/'POR', '').
card_image_name('spined wurm'/'POR', 'spined wurm').
card_uid('spined wurm'/'POR', 'POR:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'POR', 'Common').
card_artist('spined wurm'/'POR', 'Colin MacNeil').
card_flavor_text('spined wurm'/'POR', 'It has more teeth than fit in its mouth.').
card_multiverse_id('spined wurm'/'POR', '4317').

card_in_set('spiritual guardian', 'POR').
card_original_type('spiritual guardian'/'POR', 'Summon — Creature').
card_original_text('spiritual guardian'/'POR', 'When Spiritual Guardian comes into play from your hand, you gain 4 life.').
card_first_print('spiritual guardian', 'POR').
card_image_name('spiritual guardian'/'POR', 'spiritual guardian').
card_uid('spiritual guardian'/'POR', 'POR:Spiritual Guardian:spiritual guardian').
card_rarity('spiritual guardian'/'POR', 'Rare').
card_artist('spiritual guardian'/'POR', 'Terese Nielsen').
card_flavor_text('spiritual guardian'/'POR', 'Hope is born within.').
card_multiverse_id('spiritual guardian'/'POR', '4395').

card_in_set('spitting earth', 'POR').
card_original_type('spitting earth'/'POR', 'Sorcery').
card_original_text('spitting earth'/'POR', 'Spitting Earth deals to any one creature damage equal to the number of mountains you have in play. (This includes both tapped and untapped mountains.)').
card_image_name('spitting earth'/'POR', 'spitting earth').
card_uid('spitting earth'/'POR', 'POR:Spitting Earth:spitting earth').
card_rarity('spitting earth'/'POR', 'Common').
card_artist('spitting earth'/'POR', 'Hannibal King').
card_multiverse_id('spitting earth'/'POR', '4362').

card_in_set('spotted griffin', 'POR').
card_original_type('spotted griffin'/'POR', 'Summon — Creature').
card_original_text('spotted griffin'/'POR', 'Flying').
card_first_print('spotted griffin', 'POR').
card_image_name('spotted griffin'/'POR', 'spotted griffin').
card_uid('spotted griffin'/'POR', 'POR:Spotted Griffin:spotted griffin').
card_rarity('spotted griffin'/'POR', 'Common').
card_artist('spotted griffin'/'POR', 'William Simpson').
card_flavor_text('spotted griffin'/'POR', 'When the cat flies and the bird stalks, guard your horses carefully.').
card_multiverse_id('spotted griffin'/'POR', '4396').

card_in_set('stalking tiger', 'POR').
card_original_type('stalking tiger'/'POR', 'Summon — Creature').
card_original_text('stalking tiger'/'POR', 'Stalking Tiger can\'t be intercepted by more than one creature.').
card_image_name('stalking tiger'/'POR', 'stalking tiger').
card_uid('stalking tiger'/'POR', 'POR:Stalking Tiger:stalking tiger').
card_rarity('stalking tiger'/'POR', 'Common').
card_artist('stalking tiger'/'POR', 'Colin MacNeil').
card_flavor_text('stalking tiger'/'POR', 'No one is happy when they notice a tiger.').
card_multiverse_id('stalking tiger'/'POR', '4318').

card_in_set('starlight', 'POR').
card_original_type('starlight'/'POR', 'Sorcery').
card_original_text('starlight'/'POR', 'For each black creature your opponent has in play, you gain 3 life.').
card_first_print('starlight', 'POR').
card_image_name('starlight'/'POR', 'starlight').
card_uid('starlight'/'POR', 'POR:Starlight:starlight').
card_rarity('starlight'/'POR', 'Uncommon').
card_artist('starlight'/'POR', 'John Avon').
card_flavor_text('starlight'/'POR', 'Stars are like coins dropped into the night: their light buys safe passage.').
card_multiverse_id('starlight'/'POR', '4397').

card_in_set('starlit angel', 'POR').
card_original_type('starlit angel'/'POR', 'Summon — Creature').
card_original_text('starlit angel'/'POR', 'Flying').
card_first_print('starlit angel', 'POR').
card_image_name('starlit angel'/'POR', 'starlit angel').
card_uid('starlit angel'/'POR', 'POR:Starlit Angel:starlit angel').
card_rarity('starlit angel'/'POR', 'Uncommon').
card_artist('starlit angel'/'POR', 'Rebecca Guay').
card_flavor_text('starlit angel'/'POR', 'To soar as high as hope, to dive as swift as justice.').
card_multiverse_id('starlit angel'/'POR', '4398').

card_in_set('steadfastness', 'POR').
card_original_type('steadfastness'/'POR', 'Sorcery').
card_original_text('steadfastness'/'POR', 'All your creatures get +0/+3 until the end of the turn.').
card_first_print('steadfastness', 'POR').
card_image_name('steadfastness'/'POR', 'steadfastness').
card_uid('steadfastness'/'POR', 'POR:Steadfastness:steadfastness').
card_rarity('steadfastness'/'POR', 'Common').
card_artist('steadfastness'/'POR', 'Kev Walker').
card_flavor_text('steadfastness'/'POR', 'Brute force wins the battles. Conviction wins the wars.').
card_multiverse_id('steadfastness'/'POR', '4399').

card_in_set('stern marshal', 'POR').
card_original_type('stern marshal'/'POR', 'Summon — Creature').
card_original_text('stern marshal'/'POR', 'On your turn, before you attack, you may tap Stern Marshal to give any one creature +2/+2 until the end of the turn.').
card_first_print('stern marshal', 'POR').
card_image_name('stern marshal'/'POR', 'stern marshal').
card_uid('stern marshal'/'POR', 'POR:Stern Marshal:stern marshal').
card_rarity('stern marshal'/'POR', 'Rare').
card_artist('stern marshal'/'POR', 'D. Alexander Gregory').
card_multiverse_id('stern marshal'/'POR', '4400').

card_in_set('stone rain', 'POR').
card_original_type('stone rain'/'POR', 'Sorcery').
card_original_text('stone rain'/'POR', 'Destroy any one land.').
card_image_name('stone rain'/'POR', 'stone rain').
card_uid('stone rain'/'POR', 'POR:Stone Rain:stone rain').
card_rarity('stone rain'/'POR', 'Common').
card_artist('stone rain'/'POR', 'John Matson').
card_flavor_text('stone rain'/'POR', 'I cast a thousand tiny suns—\nBeware my many dawns.').
card_multiverse_id('stone rain'/'POR', '4363').

card_in_set('storm crow', 'POR').
card_original_type('storm crow'/'POR', 'Summon — Creature').
card_original_text('storm crow'/'POR', 'Flying').
card_image_name('storm crow'/'POR', 'storm crow').
card_uid('storm crow'/'POR', 'POR:Storm Crow:storm crow').
card_rarity('storm crow'/'POR', 'Common').
card_artist('storm crow'/'POR', 'Una Fricker').
card_flavor_text('storm crow'/'POR', 'Storm crow descending, winter unending.  Storm crow departing, summer is starting.').
card_multiverse_id('storm crow'/'POR', '4276').

card_in_set('summer bloom', 'POR').
card_original_type('summer bloom'/'POR', 'Sorcery').
card_original_text('summer bloom'/'POR', 'Take up to three lands from your hand and put them into play.').
card_image_name('summer bloom'/'POR', 'summer bloom').
card_uid('summer bloom'/'POR', 'POR:Summer Bloom:summer bloom').
card_rarity('summer bloom'/'POR', 'Rare').
card_artist('summer bloom'/'POR', 'Kaja Foglio').
card_flavor_text('summer bloom'/'POR', 'Summer sends its kiss with warmth and blooming life.').
card_multiverse_id('summer bloom'/'POR', '4319').

card_in_set('swamp', 'POR').
card_original_type('swamp'/'POR', 'Land').
card_original_text('swamp'/'POR', 'B').
card_image_name('swamp'/'POR', 'swamp1').
card_uid('swamp'/'POR', 'POR:Swamp:swamp1').
card_rarity('swamp'/'POR', 'Basic Land').
card_artist('swamp'/'POR', 'Romas Kukalis').
card_multiverse_id('swamp'/'POR', '4409').

card_in_set('swamp', 'POR').
card_original_type('swamp'/'POR', 'Land').
card_original_text('swamp'/'POR', 'B').
card_image_name('swamp'/'POR', 'swamp2').
card_uid('swamp'/'POR', 'POR:Swamp:swamp2').
card_rarity('swamp'/'POR', 'Basic Land').
card_artist('swamp'/'POR', 'Romas Kukalis').
card_multiverse_id('swamp'/'POR', '4410').

card_in_set('swamp', 'POR').
card_original_type('swamp'/'POR', 'Land').
card_original_text('swamp'/'POR', 'B').
card_image_name('swamp'/'POR', 'swamp3').
card_uid('swamp'/'POR', 'POR:Swamp:swamp3').
card_rarity('swamp'/'POR', 'Basic Land').
card_artist('swamp'/'POR', 'Romas Kukalis').
card_multiverse_id('swamp'/'POR', '4411').

card_in_set('swamp', 'POR').
card_original_type('swamp'/'POR', 'Land').
card_original_text('swamp'/'POR', 'B').
card_image_name('swamp'/'POR', 'swamp4').
card_uid('swamp'/'POR', 'POR:Swamp:swamp4').
card_rarity('swamp'/'POR', 'Basic Land').
card_artist('swamp'/'POR', 'Romas Kukalis').
card_multiverse_id('swamp'/'POR', '4412').

card_in_set('sylvan tutor', 'POR').
card_original_type('sylvan tutor'/'POR', 'Sorcery').
card_original_text('sylvan tutor'/'POR', 'Search your deck for a summon creature and reveal that card to all players. Then shuffle your deck and put the revealed card on top of it.').
card_first_print('sylvan tutor', 'POR').
card_image_name('sylvan tutor'/'POR', 'sylvan tutor').
card_uid('sylvan tutor'/'POR', 'POR:Sylvan Tutor:sylvan tutor').
card_rarity('sylvan tutor'/'POR', 'Rare').
card_artist('sylvan tutor'/'POR', 'Kaja Foglio').
card_multiverse_id('sylvan tutor'/'POR', '4320').

card_in_set('symbol of unsummoning', 'POR').
card_original_type('symbol of unsummoning'/'POR', 'Sorcery').
card_original_text('symbol of unsummoning'/'POR', 'Return any one creature to its owner\'s hand. You draw a card.').
card_first_print('symbol of unsummoning', 'POR').
card_image_name('symbol of unsummoning'/'POR', 'symbol of unsummoning').
card_uid('symbol of unsummoning'/'POR', 'POR:Symbol of Unsummoning:symbol of unsummoning').
card_rarity('symbol of unsummoning'/'POR', 'Common').
card_artist('symbol of unsummoning'/'POR', 'Adam Rex').
card_flavor_text('symbol of unsummoning'/'POR', '\". . . inviting the soul to wander for a spell in abysses of solitude . . . .\"\n—Kate Chopin, The Awakening').
card_multiverse_id('symbol of unsummoning'/'POR', '4277').

card_in_set('taunt', 'POR').
card_original_type('taunt'/'POR', 'Sorcery').
card_original_text('taunt'/'POR', 'Choose any one player. On that player\'s next turn, all his or her creatures that can attack you must do so.').
card_first_print('taunt', 'POR').
card_image_name('taunt'/'POR', 'taunt').
card_uid('taunt'/'POR', 'POR:Taunt:taunt').
card_rarity('taunt'/'POR', 'Rare').
card_artist('taunt'/'POR', 'Phil Foglio').
card_multiverse_id('taunt'/'POR', '4278').

card_in_set('temporary truce', 'POR').
card_original_type('temporary truce'/'POR', 'Sorcery').
card_original_text('temporary truce'/'POR', 'Each player may draw up to two cards. For each card less than two any player draws, that player gains 2 life. (You choose whether to draw first.)').
card_first_print('temporary truce', 'POR').
card_image_name('temporary truce'/'POR', 'temporary truce').
card_uid('temporary truce'/'POR', 'POR:Temporary Truce:temporary truce').
card_rarity('temporary truce'/'POR', 'Rare').
card_artist('temporary truce'/'POR', 'Mike Raabe').
card_multiverse_id('temporary truce'/'POR', '4401').

card_in_set('theft of dreams', 'POR').
card_original_type('theft of dreams'/'POR', 'Sorcery').
card_original_text('theft of dreams'/'POR', 'For each tapped creature your opponent has in play, you draw a card.').
card_first_print('theft of dreams', 'POR').
card_image_name('theft of dreams'/'POR', 'theft of dreams').
card_uid('theft of dreams'/'POR', 'POR:Theft of Dreams:theft of dreams').
card_rarity('theft of dreams'/'POR', 'Uncommon').
card_artist('theft of dreams'/'POR', 'Adam Rex').
card_flavor_text('theft of dreams'/'POR', 'Energy is never lost, only transformed.').
card_multiverse_id('theft of dreams'/'POR', '4279').

card_in_set('thing from the deep', 'POR').
card_original_type('thing from the deep'/'POR', 'Summon — Creature').
card_original_text('thing from the deep'/'POR', 'If Thing from the Deep attacks, destroy one of your islands or destroy Thing from the Deep.').
card_first_print('thing from the deep', 'POR').
card_image_name('thing from the deep'/'POR', 'thing from the deep').
card_uid('thing from the deep'/'POR', 'POR:Thing from the Deep:thing from the deep').
card_rarity('thing from the deep'/'POR', 'Rare').
card_artist('thing from the deep'/'POR', 'Paolo Parente').
card_flavor_text('thing from the deep'/'POR', 'Seafarers fear sailing off the world\'s edge not so much as down its gullet.').
card_multiverse_id('thing from the deep'/'POR', '4280').

card_in_set('thundering wurm', 'POR').
card_original_type('thundering wurm'/'POR', 'Summon — Creature').
card_original_text('thundering wurm'/'POR', 'When Thundering Wurm comes into play from your hand, discard a land from your hand or destroy Thundering Wurm.').
card_first_print('thundering wurm', 'POR').
card_image_name('thundering wurm'/'POR', 'thundering wurm').
card_uid('thundering wurm'/'POR', 'POR:Thundering Wurm:thundering wurm').
card_rarity('thundering wurm'/'POR', 'Rare').
card_artist('thundering wurm'/'POR', 'Paolo Parente').
card_multiverse_id('thundering wurm'/'POR', '4321').

card_in_set('thundermare', 'POR').
card_original_type('thundermare'/'POR', 'Summon — Creature').
card_original_text('thundermare'/'POR', 'Thundermare is unaffected by summoning sickness. \nWhen Thundermare comes into play from your hand, tap all other creatures. (This includes your creatures.)').
card_first_print('thundermare', 'POR').
card_image_name('thundermare'/'POR', 'thundermare').
card_uid('thundermare'/'POR', 'POR:Thundermare:thundermare').
card_rarity('thundermare'/'POR', 'Rare').
card_artist('thundermare'/'POR', 'Bob Eggleton').
card_multiverse_id('thundermare'/'POR', '4364').

card_in_set('tidal surge', 'POR').
card_original_type('tidal surge'/'POR', 'Sorcery').
card_original_text('tidal surge'/'POR', 'Tap any one, two, or three creatures without flying. (Tapped creatures can\'t intercept.)').
card_first_print('tidal surge', 'POR').
card_image_name('tidal surge'/'POR', 'tidal surge').
card_uid('tidal surge'/'POR', 'POR:Tidal Surge:tidal surge').
card_rarity('tidal surge'/'POR', 'Common').
card_artist('tidal surge'/'POR', 'Douglas Shuler').
card_flavor_text('tidal surge'/'POR', '\"\'Twas  when the seas were roaring\nWith hollow blasts of wind . . . .\"\n—John Gay, \"The What D\'Ye Call It\"').
card_multiverse_id('tidal surge'/'POR', '4281').

card_in_set('time ebb', 'POR').
card_original_type('time ebb'/'POR', 'Sorcery').
card_original_text('time ebb'/'POR', 'Return any one creature to the top of its owner\'s deck.').
card_first_print('time ebb', 'POR').
card_image_name('time ebb'/'POR', 'time ebb').
card_uid('time ebb'/'POR', 'POR:Time Ebb:time ebb').
card_rarity('time ebb'/'POR', 'Common').
card_artist('time ebb'/'POR', 'Alan Rabinowitz').
card_flavor_text('time ebb'/'POR', 'Like the tide, time both ebbs and flows.').
card_multiverse_id('time ebb'/'POR', '4282').

card_in_set('touch of brilliance', 'POR').
card_original_type('touch of brilliance'/'POR', 'Sorcery').
card_original_text('touch of brilliance'/'POR', 'Draw two cards.').
card_first_print('touch of brilliance', 'POR').
card_image_name('touch of brilliance'/'POR', 'touch of brilliance').
card_uid('touch of brilliance'/'POR', 'POR:Touch of Brilliance:touch of brilliance').
card_rarity('touch of brilliance'/'POR', 'Common').
card_artist('touch of brilliance'/'POR', 'John Coulthart').
card_flavor_text('touch of brilliance'/'POR', 'Acting on one good idea is better than hoarding all the world\'s knowledge.').
card_multiverse_id('touch of brilliance'/'POR', '4283').

card_in_set('treetop defense', 'POR').
card_original_type('treetop defense'/'POR', 'Sorcery').
card_original_text('treetop defense'/'POR', 'Play Treetop Defense only after you\'re attacked, before you declare interceptors.\nThis turn, all your creatures can intercept as though they had flying.').
card_first_print('treetop defense', 'POR').
card_image_name('treetop defense'/'POR', 'treetop defense').
card_uid('treetop defense'/'POR', 'POR:Treetop Defense:treetop defense').
card_rarity('treetop defense'/'POR', 'Rare').
card_artist('treetop defense'/'POR', 'Zina Saunders').
card_multiverse_id('treetop defense'/'POR', '4322').

card_in_set('undying beast', 'POR').
card_original_type('undying beast'/'POR', 'Summon — Creature').
card_original_text('undying beast'/'POR', 'If Undying Beast is put into your discard pile from play, put Undying Beast on top of your deck.').
card_first_print('undying beast', 'POR').
card_image_name('undying beast'/'POR', 'undying beast').
card_uid('undying beast'/'POR', 'POR:Undying Beast:undying beast').
card_rarity('undying beast'/'POR', 'Common').
card_artist('undying beast'/'POR', 'Steve Luke').
card_multiverse_id('undying beast'/'POR', '4242').

card_in_set('untamed wilds', 'POR').
card_original_type('untamed wilds'/'POR', 'Sorcery').
card_original_text('untamed wilds'/'POR', 'Search your deck for a plains, island, swamp, mountain, or forest and put that card into play. Shuffle your deck afterwards.').
card_image_name('untamed wilds'/'POR', 'untamed wilds').
card_uid('untamed wilds'/'POR', 'POR:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'POR', 'Uncommon').
card_artist('untamed wilds'/'POR', 'Romas Kukalis').
card_multiverse_id('untamed wilds'/'POR', '4323').

card_in_set('valorous charge', 'POR').
card_original_type('valorous charge'/'POR', 'Sorcery').
card_original_text('valorous charge'/'POR', 'All white creatures get +2/+0 until the end of the turn. (This includes other players\' white creatures.)').
card_first_print('valorous charge', 'POR').
card_image_name('valorous charge'/'POR', 'valorous charge').
card_uid('valorous charge'/'POR', 'POR:Valorous Charge:valorous charge').
card_rarity('valorous charge'/'POR', 'Uncommon').
card_artist('valorous charge'/'POR', 'Douglas Shuler').
card_flavor_text('valorous charge'/'POR', 'Stand in the way of truth at your peril.').
card_multiverse_id('valorous charge'/'POR', '4402').

card_in_set('vampiric feast', 'POR').
card_original_type('vampiric feast'/'POR', 'Sorcery').
card_original_text('vampiric feast'/'POR', 'Vampiric Feast deals 4 damage to any one creature or player. You gain 4 life.').
card_first_print('vampiric feast', 'POR').
card_image_name('vampiric feast'/'POR', 'vampiric feast').
card_uid('vampiric feast'/'POR', 'POR:Vampiric Feast:vampiric feast').
card_rarity('vampiric feast'/'POR', 'Uncommon').
card_artist('vampiric feast'/'POR', 'D. Alexander Gregory').
card_flavor_text('vampiric feast'/'POR', 'It\'s not always gold the thief is after.').
card_multiverse_id('vampiric feast'/'POR', '4243').

card_in_set('vampiric touch', 'POR').
card_original_type('vampiric touch'/'POR', 'Sorcery').
card_original_text('vampiric touch'/'POR', 'Vampiric Touch deals 2 damage to your opponent. You gain 2 life.').
card_first_print('vampiric touch', 'POR').
card_image_name('vampiric touch'/'POR', 'vampiric touch').
card_uid('vampiric touch'/'POR', 'POR:Vampiric Touch:vampiric touch').
card_rarity('vampiric touch'/'POR', 'Common').
card_artist('vampiric touch'/'POR', 'Zina Saunders').
card_flavor_text('vampiric touch'/'POR', 'A touch, not comforting, but of death.').
card_multiverse_id('vampiric touch'/'POR', '4244').

card_in_set('venerable monk', 'POR').
card_original_type('venerable monk'/'POR', 'Summon — Creature').
card_original_text('venerable monk'/'POR', 'When Venerable Monk comes into play from your hand, you gain 2 life.').
card_first_print('venerable monk', 'POR').
card_image_name('venerable monk'/'POR', 'venerable monk').
card_uid('venerable monk'/'POR', 'POR:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'POR', 'Uncommon').
card_artist('venerable monk'/'POR', 'D. Alexander Gregory').
card_flavor_text('venerable monk'/'POR', 'His presence brings not only a strong arm but also renewed hope.').
card_multiverse_id('venerable monk'/'POR', '4403').

card_in_set('vengeance', 'POR').
card_original_type('vengeance'/'POR', 'Sorcery').
card_original_text('vengeance'/'POR', 'Destroy any one tapped creature.').
card_first_print('vengeance', 'POR').
card_image_name('vengeance'/'POR', 'vengeance').
card_uid('vengeance'/'POR', 'POR:Vengeance:vengeance').
card_rarity('vengeance'/'POR', 'Uncommon').
card_artist('vengeance'/'POR', 'Andrew Robinson').
card_flavor_text('vengeance'/'POR', 'Bitter as wormwood, sweet as mulled wine.').
card_multiverse_id('vengeance'/'POR', '4404').

card_in_set('virtue\'s ruin', 'POR').
card_original_type('virtue\'s ruin'/'POR', 'Sorcery').
card_original_text('virtue\'s ruin'/'POR', 'Destroy all white creatures. (This includes your white creatures.)').
card_first_print('virtue\'s ruin', 'POR').
card_image_name('virtue\'s ruin'/'POR', 'virtue\'s ruin').
card_uid('virtue\'s ruin'/'POR', 'POR:Virtue\'s Ruin:virtue\'s ruin').
card_rarity('virtue\'s ruin'/'POR', 'Uncommon').
card_artist('virtue\'s ruin'/'POR', 'Mike Dringenberg').
card_flavor_text('virtue\'s ruin'/'POR', 'All must fall, and those who stand highest fall hardest.').
card_multiverse_id('virtue\'s ruin'/'POR', '4245').

card_in_set('volcanic dragon', 'POR').
card_original_type('volcanic dragon'/'POR', 'Summon — Creature').
card_original_text('volcanic dragon'/'POR', 'Flying\nVolcanic Dragon is unaffected by summoning sickness.').
card_image_name('volcanic dragon'/'POR', 'volcanic dragon').
card_uid('volcanic dragon'/'POR', 'POR:Volcanic Dragon:volcanic dragon').
card_rarity('volcanic dragon'/'POR', 'Rare').
card_artist('volcanic dragon'/'POR', 'Tom Wänerstrand').
card_multiverse_id('volcanic dragon'/'POR', '4365').

card_in_set('volcanic hammer', 'POR').
card_original_type('volcanic hammer'/'POR', 'Sorcery').
card_original_text('volcanic hammer'/'POR', 'Volcanic Hammer deals 3 damage to any one creature or player.').
card_first_print('volcanic hammer', 'POR').
card_image_name('volcanic hammer'/'POR', 'volcanic hammer').
card_uid('volcanic hammer'/'POR', 'POR:Volcanic Hammer:volcanic hammer').
card_rarity('volcanic hammer'/'POR', 'Common').
card_artist('volcanic hammer'/'POR', 'Christopher Rush').
card_flavor_text('volcanic hammer'/'POR', 'Cast the weight as though it were a die, to see a rival\'s fate.').
card_multiverse_id('volcanic hammer'/'POR', '4366').

card_in_set('wall of granite', 'POR').
card_original_type('wall of granite'/'POR', 'Summon — Creature').
card_original_text('wall of granite'/'POR', 'Wall of Granite can\'t attack.').
card_first_print('wall of granite', 'POR').
card_image_name('wall of granite'/'POR', 'wall of granite').
card_uid('wall of granite'/'POR', 'POR:Wall of Granite:wall of granite').
card_rarity('wall of granite'/'POR', 'Uncommon').
card_artist('wall of granite'/'POR', 'Kev Walker').
card_flavor_text('wall of granite'/'POR', 'The wisest man builds his house behind the rock.').
card_multiverse_id('wall of granite'/'POR', '4367').

card_in_set('wall of swords', 'POR').
card_original_type('wall of swords'/'POR', 'Summon — Creature').
card_original_text('wall of swords'/'POR', 'Flying\nWall of Swords can\'t attack.').
card_image_name('wall of swords'/'POR', 'wall of swords').
card_uid('wall of swords'/'POR', 'POR:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'POR', 'Uncommon').
card_artist('wall of swords'/'POR', 'Douglas Shuler').
card_flavor_text('wall of swords'/'POR', 'Sharper than wind, lighter than air.').
card_multiverse_id('wall of swords'/'POR', '4405').

card_in_set('warrior\'s charge', 'POR').
card_original_type('warrior\'s charge'/'POR', 'Sorcery').
card_original_text('warrior\'s charge'/'POR', 'All your creatures get +1/+1 until the end of the turn. (For example, a 1/2 creature would become 2/3.)').
card_first_print('warrior\'s charge', 'POR').
card_image_name('warrior\'s charge'/'POR', 'warrior\'s charge1').
card_uid('warrior\'s charge'/'POR', 'POR:Warrior\'s Charge:warrior\'s charge1').
card_rarity('warrior\'s charge'/'POR', 'Common').
card_artist('warrior\'s charge'/'POR', 'Ted Naifeh').
card_multiverse_id('warrior\'s charge'/'POR', '4406').

card_in_set('warrior\'s charge', 'POR').
card_original_type('warrior\'s charge'/'POR', 'Sorcery').
card_original_text('warrior\'s charge'/'POR', 'All your creatures get +1/+1 until the end of the turn.').
card_image_name('warrior\'s charge'/'POR', 'warrior\'s charge2').
card_uid('warrior\'s charge'/'POR', 'POR:Warrior\'s Charge:warrior\'s charge2').
card_rarity('warrior\'s charge'/'POR', 'Common').
card_artist('warrior\'s charge'/'POR', 'Ted Naifeh').
card_flavor_text('warrior\'s charge'/'POR', 'It is not the absence of fear that makes a warrior, but its domination.').
card_multiverse_id('warrior\'s charge'/'POR', '4407').

card_in_set('whiptail wurm', 'POR').
card_original_type('whiptail wurm'/'POR', 'Summon — Creature').
card_original_text('whiptail wurm'/'POR', '').
card_first_print('whiptail wurm', 'POR').
card_image_name('whiptail wurm'/'POR', 'whiptail wurm').
card_uid('whiptail wurm'/'POR', 'POR:Whiptail Wurm:whiptail wurm').
card_rarity('whiptail wurm'/'POR', 'Uncommon').
card_artist('whiptail wurm'/'POR', 'Una Fricker').
card_flavor_text('whiptail wurm'/'POR', 'It\'s hard to say for certain which end is more dangerous.').
card_multiverse_id('whiptail wurm'/'POR', '4324').

card_in_set('wicked pact', 'POR').
card_original_type('wicked pact'/'POR', 'Sorcery').
card_original_text('wicked pact'/'POR', 'Destroy any two creatures that aren\'t black. You lose 5 life. (You can\'t play Wicked Pact unless you can choose two creatures to destroy.)').
card_first_print('wicked pact', 'POR').
card_image_name('wicked pact'/'POR', 'wicked pact').
card_uid('wicked pact'/'POR', 'POR:Wicked Pact:wicked pact').
card_rarity('wicked pact'/'POR', 'Rare').
card_artist('wicked pact'/'POR', 'Adam Rex').
card_multiverse_id('wicked pact'/'POR', '4246').

card_in_set('willow dryad', 'POR').
card_original_type('willow dryad'/'POR', 'Summon — Creature').
card_original_text('willow dryad'/'POR', 'Forestwalk (If defending player has any forests in play, Willow Dryad can\'t be intercepted.)').
card_first_print('willow dryad', 'POR').
card_image_name('willow dryad'/'POR', 'willow dryad').
card_uid('willow dryad'/'POR', 'POR:Willow Dryad:willow dryad').
card_rarity('willow dryad'/'POR', 'Common').
card_artist('willow dryad'/'POR', 'D. Alexander Gregory').
card_flavor_text('willow dryad'/'POR', 'Some think that dryads are the dreams of trees.').
card_multiverse_id('willow dryad'/'POR', '4325').

card_in_set('wind drake', 'POR').
card_original_type('wind drake'/'POR', 'Summon — Creature').
card_original_text('wind drake'/'POR', 'Flying').
card_first_print('wind drake', 'POR').
card_image_name('wind drake'/'POR', 'wind drake').
card_uid('wind drake'/'POR', 'POR:Wind Drake:wind drake').
card_rarity('wind drake'/'POR', 'Common').
card_artist('wind drake'/'POR', 'Zina Saunders').
card_flavor_text('wind drake'/'POR', '\"No bird soars too high, if he soars with his own wings.\" —William Blake, The Marriage of Heaven and Hell').
card_multiverse_id('wind drake'/'POR', '4284').

card_in_set('winds of change', 'POR').
card_original_type('winds of change'/'POR', 'Sorcery').
card_original_text('winds of change'/'POR', 'Each player counts the cards in his or her hand, shuffles those cards into his or her deck, and then draws that many cards. (When you play Winds of Change, it doesn\'t count as a card in your hand.)').
card_image_name('winds of change'/'POR', 'winds of change').
card_uid('winds of change'/'POR', 'POR:Winds of Change:winds of change').
card_rarity('winds of change'/'POR', 'Rare').
card_artist('winds of change'/'POR', 'Adam Rex').
card_multiverse_id('winds of change'/'POR', '4368').

card_in_set('winter\'s grasp', 'POR').
card_original_type('winter\'s grasp'/'POR', 'Sorcery').
card_original_text('winter\'s grasp'/'POR', 'Destroy any one land.').
card_first_print('winter\'s grasp', 'POR').
card_image_name('winter\'s grasp'/'POR', 'winter\'s grasp').
card_uid('winter\'s grasp'/'POR', 'POR:Winter\'s Grasp:winter\'s grasp').
card_rarity('winter\'s grasp'/'POR', 'Uncommon').
card_artist('winter\'s grasp'/'POR', 'Paolo Parente').
card_flavor_text('winter\'s grasp'/'POR', 'Winter settles on the land, and the land prays it will wake.').
card_multiverse_id('winter\'s grasp'/'POR', '4326').

card_in_set('withering gaze', 'POR').
card_original_type('withering gaze'/'POR', 'Sorcery').
card_original_text('withering gaze'/'POR', 'Look at your opponent\'s hand. For each forest and green card there, you draw a card. (You draw from your deck.)').
card_first_print('withering gaze', 'POR').
card_image_name('withering gaze'/'POR', 'withering gaze').
card_uid('withering gaze'/'POR', 'POR:Withering Gaze:withering gaze').
card_rarity('withering gaze'/'POR', 'Uncommon').
card_artist('withering gaze'/'POR', 'Scott M. Fischer').
card_multiverse_id('withering gaze'/'POR', '4285').

card_in_set('wood elves', 'POR').
card_original_type('wood elves'/'POR', 'Summon — Creature').
card_original_text('wood elves'/'POR', 'When Wood Elves comes into play from your hand, search your deck for a forest and put that card into play. Shuffle your deck afterwards.').
card_first_print('wood elves', 'POR').
card_image_name('wood elves'/'POR', 'wood elves').
card_uid('wood elves'/'POR', 'POR:Wood Elves:wood elves').
card_rarity('wood elves'/'POR', 'Rare').
card_artist('wood elves'/'POR', 'Rebecca Guay').
card_multiverse_id('wood elves'/'POR', '4327').

card_in_set('wrath of god', 'POR').
card_original_type('wrath of god'/'POR', 'Sorcery').
card_original_text('wrath of god'/'POR', 'Put all creatures into their owners\' discard piles. (This includes your creatures.)').
card_image_name('wrath of god'/'POR', 'wrath of god').
card_uid('wrath of god'/'POR', 'POR:Wrath of God:wrath of god').
card_rarity('wrath of god'/'POR', 'Rare').
card_artist('wrath of god'/'POR', 'Mike Raabe').
card_flavor_text('wrath of god'/'POR', '\"As flies to wanton boys, are we to the gods. They kill us for their sport.\"\n—William Shakespeare, King Lear').
card_multiverse_id('wrath of god'/'POR', '4408').
