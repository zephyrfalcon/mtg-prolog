% Duel Decks: Divine vs. Demonic

set('DDC').
set_name('DDC', 'Duel Decks: Divine vs. Demonic').
set_release_date('DDC', '2009-04-10').
set_border('DDC', 'black').
set_type('DDC', 'duel deck').

card_in_set('abyssal gatekeeper', 'DDC').
card_original_type('abyssal gatekeeper'/'DDC', 'Creature — Horror').
card_original_text('abyssal gatekeeper'/'DDC', 'When Abyssal Gatekeeper is put into a graveyard from play, each player sacrifices a creature.').
card_image_name('abyssal gatekeeper'/'DDC', 'abyssal gatekeeper').
card_uid('abyssal gatekeeper'/'DDC', 'DDC:Abyssal Gatekeeper:abyssal gatekeeper').
card_rarity('abyssal gatekeeper'/'DDC', 'Common').
card_artist('abyssal gatekeeper'/'DDC', 'Mark Tedin').
card_number('abyssal gatekeeper'/'DDC', '31').
card_flavor_text('abyssal gatekeeper'/'DDC', '\"There are two ways for me to pass this gate. One involves you remaining conscious.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('abyssal gatekeeper'/'DDC', '197037').

card_in_set('abyssal specter', 'DDC').
card_original_type('abyssal specter'/'DDC', 'Creature — Specter').
card_original_text('abyssal specter'/'DDC', 'Flying\nWhenever Abyssal Specter deals damage to a player, that player discards a card.').
card_image_name('abyssal specter'/'DDC', 'abyssal specter').
card_uid('abyssal specter'/'DDC', 'DDC:Abyssal Specter:abyssal specter').
card_rarity('abyssal specter'/'DDC', 'Uncommon').
card_artist('abyssal specter'/'DDC', 'Michael Sutfin').
card_number('abyssal specter'/'DDC', '40').
card_flavor_text('abyssal specter'/'DDC', 'To gaze under its hood is to invite death.').
card_multiverse_id('abyssal specter'/'DDC', '197008').

card_in_set('akroma, angel of wrath', 'DDC').
card_original_type('akroma, angel of wrath'/'DDC', 'Legendary Creature — Angel').
card_original_text('akroma, angel of wrath'/'DDC', 'Flying, first strike, vigilance, trample, haste, protection from black and from red').
card_image_name('akroma, angel of wrath'/'DDC', 'akroma, angel of wrath').
card_uid('akroma, angel of wrath'/'DDC', 'DDC:Akroma, Angel of Wrath:akroma, angel of wrath').
card_rarity('akroma, angel of wrath'/'DDC', 'Mythic Rare').
card_artist('akroma, angel of wrath'/'DDC', 'Chippy').
card_number('akroma, angel of wrath'/'DDC', '1').
card_flavor_text('akroma, angel of wrath'/'DDC', '\"Wrath is no vice when inflicted upon the deserving.\"').
card_multiverse_id('akroma, angel of wrath'/'DDC', '193871').

card_in_set('angel of mercy', 'DDC').
card_original_type('angel of mercy'/'DDC', 'Creature — Angel').
card_original_text('angel of mercy'/'DDC', 'Flying\nWhen Angel of Mercy comes into play, you gain 3 life.').
card_image_name('angel of mercy'/'DDC', 'angel of mercy').
card_uid('angel of mercy'/'DDC', 'DDC:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'DDC', 'Uncommon').
card_artist('angel of mercy'/'DDC', 'Volkan Baga').
card_number('angel of mercy'/'DDC', '9').
card_flavor_text('angel of mercy'/'DDC', 'Every tear shed is a drop of immortality.').
card_multiverse_id('angel of mercy'/'DDC', '196990').

card_in_set('angel\'s feather', 'DDC').
card_original_type('angel\'s feather'/'DDC', 'Artifact').
card_original_text('angel\'s feather'/'DDC', 'Whenever a player plays a white spell, you may gain 1 life.').
card_image_name('angel\'s feather'/'DDC', 'angel\'s feather').
card_uid('angel\'s feather'/'DDC', 'DDC:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'DDC', 'Uncommon').
card_artist('angel\'s feather'/'DDC', 'Alan Pollack').
card_number('angel\'s feather'/'DDC', '23').
card_flavor_text('angel\'s feather'/'DDC', 'If taken, it cuts the hand that clutches it. If given, it heals the hand that holds it.').
card_multiverse_id('angel\'s feather'/'DDC', '196991').

card_in_set('angelic benediction', 'DDC').
card_original_type('angelic benediction'/'DDC', 'Enchantment').
card_original_text('angelic benediction'/'DDC', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, you may tap target creature.').
card_image_name('angelic benediction'/'DDC', 'angelic benediction').
card_uid('angelic benediction'/'DDC', 'DDC:Angelic Benediction:angelic benediction').
card_rarity('angelic benediction'/'DDC', 'Uncommon').
card_artist('angelic benediction'/'DDC', 'Michael Komarck').
card_number('angelic benediction'/'DDC', '19').
card_flavor_text('angelic benediction'/'DDC', '\"Even in single combat, I am never alone.\"\n—Rafiq of the Many').
card_multiverse_id('angelic benediction'/'DDC', '197013').

card_in_set('angelic page', 'DDC').
card_original_type('angelic page'/'DDC', 'Creature — Angel Spirit').
card_original_text('angelic page'/'DDC', 'Flying\n{T}: Target attacking or blocking creature gets +1/+1 until end of turn.').
card_image_name('angelic page'/'DDC', 'angelic page').
card_uid('angelic page'/'DDC', 'DDC:Angelic Page:angelic page').
card_rarity('angelic page'/'DDC', 'Common').
card_artist('angelic page'/'DDC', 'Marc Fishman').
card_number('angelic page'/'DDC', '3').
card_flavor_text('angelic page'/'DDC', 'If only every message were as perfect as its bearer.').
card_multiverse_id('angelic page'/'DDC', '197009').

card_in_set('angelic protector', 'DDC').
card_original_type('angelic protector'/'DDC', 'Creature — Angel').
card_original_text('angelic protector'/'DDC', 'Flying\nWhenever Angelic Protector becomes the target of a spell or ability, Angelic Protector gets +0/+3 until end of turn.').
card_image_name('angelic protector'/'DDC', 'angelic protector').
card_uid('angelic protector'/'DDC', 'DDC:Angelic Protector:angelic protector').
card_rarity('angelic protector'/'DDC', 'Uncommon').
card_artist('angelic protector'/'DDC', 'DiTerlizzi').
card_number('angelic protector'/'DDC', '6').
card_flavor_text('angelic protector'/'DDC', '\"My family sheltered in her light, the dark was content to wait.\"\n—Crovax').
card_multiverse_id('angelic protector'/'DDC', '197035').

card_in_set('angelsong', 'DDC').
card_original_type('angelsong'/'DDC', 'Instant').
card_original_text('angelsong'/'DDC', 'Prevent all combat damage that would be dealt this turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('angelsong'/'DDC', 'angelsong').
card_uid('angelsong'/'DDC', 'DDC:Angelsong:angelsong').
card_rarity('angelsong'/'DDC', 'Common').
card_artist('angelsong'/'DDC', 'Sal Villagran').
card_number('angelsong'/'DDC', '15').
card_flavor_text('angelsong'/'DDC', 'Clash of sword and cry of beast fall mute when angels sound the call to prayer.').
card_multiverse_id('angelsong'/'DDC', '197014').

card_in_set('barren moor', 'DDC').
card_original_type('barren moor'/'DDC', 'Land').
card_original_text('barren moor'/'DDC', 'Barren Moor comes into play tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'DDC', 'barren moor').
card_uid('barren moor'/'DDC', 'DDC:Barren Moor:barren moor').
card_rarity('barren moor'/'DDC', 'Common').
card_artist('barren moor'/'DDC', 'Heather Hudson').
card_number('barren moor'/'DDC', '58').
card_multiverse_id('barren moor'/'DDC', '197029').

card_in_set('barter in blood', 'DDC').
card_original_type('barter in blood'/'DDC', 'Sorcery').
card_original_text('barter in blood'/'DDC', 'Each player sacrifices two creatures.').
card_image_name('barter in blood'/'DDC', 'barter in blood').
card_uid('barter in blood'/'DDC', 'DDC:Barter in Blood:barter in blood').
card_rarity('barter in blood'/'DDC', 'Uncommon').
card_artist('barter in blood'/'DDC', 'Paolo Parente').
card_number('barter in blood'/'DDC', '52').
card_flavor_text('barter in blood'/'DDC', '\"In the game of conquest, who cares about the pawns if the king yet reigns?\"\n—Geth, keeper of the Vault').
card_multiverse_id('barter in blood'/'DDC', '197025').

card_in_set('breeding pit', 'DDC').
card_original_type('breeding pit'/'DDC', 'Enchantment').
card_original_text('breeding pit'/'DDC', 'At the beginning of your upkeep, sacrifice Breeding Pit unless you pay {B}{B}.\nAt the end of your turn, put a 0/1 black Thrull creature token into play.').
card_image_name('breeding pit'/'DDC', 'breeding pit').
card_uid('breeding pit'/'DDC', 'DDC:Breeding Pit:breeding pit').
card_rarity('breeding pit'/'DDC', 'Uncommon').
card_artist('breeding pit'/'DDC', 'Adrian Smith').
card_number('breeding pit'/'DDC', '53').
card_flavor_text('breeding pit'/'DDC', 'The thrulls bred at a terrifying pace. In the end, they overwhelmed the Order of the Ebon Hand.').
card_multiverse_id('breeding pit'/'DDC', '197024').

card_in_set('cackling imp', 'DDC').
card_original_type('cackling imp'/'DDC', 'Creature — Imp').
card_original_text('cackling imp'/'DDC', 'Flying\n{T}: Target player loses 1 life.').
card_image_name('cackling imp'/'DDC', 'cackling imp').
card_uid('cackling imp'/'DDC', 'DDC:Cackling Imp:cackling imp').
card_rarity('cackling imp'/'DDC', 'Common').
card_artist('cackling imp'/'DDC', 'Matt Thompson').
card_number('cackling imp'/'DDC', '41').
card_flavor_text('cackling imp'/'DDC', 'Its laughter drives needles through your mind.').
card_multiverse_id('cackling imp'/'DDC', '197003').

card_in_set('charging paladin', 'DDC').
card_original_type('charging paladin'/'DDC', 'Creature — Human Knight').
card_original_text('charging paladin'/'DDC', 'Whenever Charging Paladin attacks, it gets +0/+3 until end of turn.').
card_image_name('charging paladin'/'DDC', 'charging paladin').
card_uid('charging paladin'/'DDC', 'DDC:Charging Paladin:charging paladin').
card_rarity('charging paladin'/'DDC', 'Common').
card_artist('charging paladin'/'DDC', 'Ciruelo').
card_number('charging paladin'/'DDC', '4').
card_flavor_text('charging paladin'/'DDC', '\"Hope shall be the blade that severs our bonds.\"').
card_multiverse_id('charging paladin'/'DDC', '197021').

card_in_set('consume spirit', 'DDC').
card_original_type('consume spirit'/'DDC', 'Sorcery').
card_original_text('consume spirit'/'DDC', 'Spend only black mana on X.\nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_image_name('consume spirit'/'DDC', 'consume spirit').
card_uid('consume spirit'/'DDC', 'DDC:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'DDC', 'Uncommon').
card_artist('consume spirit'/'DDC', 'Matt Thompson').
card_number('consume spirit'/'DDC', '56').
card_flavor_text('consume spirit'/'DDC', '\"Your blood, your marrow, your spirit—all are mine.\"\n—Mayvar, minion of Geth').
card_multiverse_id('consume spirit'/'DDC', '196992').

card_in_set('corrupt', 'DDC').
card_original_type('corrupt'/'DDC', 'Sorcery').
card_original_text('corrupt'/'DDC', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'DDC', 'corrupt').
card_uid('corrupt'/'DDC', 'DDC:Corrupt:corrupt').
card_rarity('corrupt'/'DDC', 'Uncommon').
card_artist('corrupt'/'DDC', 'Dave Allsop').
card_number('corrupt'/'DDC', '55').
card_flavor_text('corrupt'/'DDC', 'There are things beneath the murk for whom the bog is a stew, a thin broth in need of meat.').
card_multiverse_id('corrupt'/'DDC', '197033').

card_in_set('cruel edict', 'DDC').
card_original_type('cruel edict'/'DDC', 'Sorcery').
card_original_text('cruel edict'/'DDC', 'Target opponent sacrifices a creature.').
card_image_name('cruel edict'/'DDC', 'cruel edict').
card_uid('cruel edict'/'DDC', 'DDC:Cruel Edict:cruel edict').
card_rarity('cruel edict'/'DDC', 'Uncommon').
card_artist('cruel edict'/'DDC', 'Michael Sutfin').
card_number('cruel edict'/'DDC', '48').
card_flavor_text('cruel edict'/'DDC', '\"Choose your next words carefully. They will be your last.\"\n—Phage the Untouchable').
card_multiverse_id('cruel edict'/'DDC', '196993').

card_in_set('daggerclaw imp', 'DDC').
card_original_type('daggerclaw imp'/'DDC', 'Creature — Imp').
card_original_text('daggerclaw imp'/'DDC', 'Flying\nDaggerclaw Imp can\'t block.').
card_image_name('daggerclaw imp'/'DDC', 'daggerclaw imp').
card_uid('daggerclaw imp'/'DDC', 'DDC:Daggerclaw Imp:daggerclaw imp').
card_rarity('daggerclaw imp'/'DDC', 'Uncommon').
card_artist('daggerclaw imp'/'DDC', 'Pete Venters').
card_number('daggerclaw imp'/'DDC', '33').
card_flavor_text('daggerclaw imp'/'DDC', 'The Simic use the claws as scalpels, while the Rakdos use them for tattooing and torture. The Gruul use them to pick their teeth after lunching on the rest of the carcass.').
card_multiverse_id('daggerclaw imp'/'DDC', '197022').

card_in_set('dark banishing', 'DDC').
card_original_type('dark banishing'/'DDC', 'Instant').
card_original_text('dark banishing'/'DDC', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'DDC', 'dark banishing').
card_uid('dark banishing'/'DDC', 'DDC:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'DDC', 'Common').
card_artist('dark banishing'/'DDC', 'Dermot Power').
card_number('dark banishing'/'DDC', '50').
card_flavor_text('dark banishing'/'DDC', '\"Ha, banishment? Be merciful, say ‘death,\'\nFor exile hath more terror in his look,\nMuch more than death.\"\n—William Shakespeare, Romeo and Juliet').
card_multiverse_id('dark banishing'/'DDC', '197023').

card_in_set('dark ritual', 'DDC').
card_original_type('dark ritual'/'DDC', 'Instant').
card_original_text('dark ritual'/'DDC', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'DDC', 'dark ritual').
card_uid('dark ritual'/'DDC', 'DDC:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'DDC', 'Common').
card_artist('dark ritual'/'DDC', 'Clint Langley').
card_number('dark ritual'/'DDC', '45').
card_flavor_text('dark ritual'/'DDC', '\"Leshrac, my liege, grant me the power I am due.\"\n—Lim-Dûl, the Necromancer').
card_multiverse_id('dark ritual'/'DDC', '197019').

card_in_set('demon', 'DDC').
card_original_type('demon'/'DDC', 'Creature — Demon').
card_original_text('demon'/'DDC', 'Flying').
card_first_print('demon', 'DDC').
card_image_name('demon'/'DDC', 'demon').
card_uid('demon'/'DDC', 'DDC:Demon:demon').
card_rarity('demon'/'DDC', 'Common').
card_artist('demon'/'DDC', 'Pete Venters').
card_number('demon'/'DDC', 'T2').
card_multiverse_id('demon'/'DDC', '197936').

card_in_set('demon\'s horn', 'DDC').
card_original_type('demon\'s horn'/'DDC', 'Artifact').
card_original_text('demon\'s horn'/'DDC', 'Whenever a player plays a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'DDC', 'demon\'s horn').
card_uid('demon\'s horn'/'DDC', 'DDC:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'DDC', 'Uncommon').
card_artist('demon\'s horn'/'DDC', 'Alan Pollack').
card_number('demon\'s horn'/'DDC', '57').
card_flavor_text('demon\'s horn'/'DDC', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'DDC', '196994').

card_in_set('demon\'s jester', 'DDC').
card_original_type('demon\'s jester'/'DDC', 'Creature — Imp').
card_original_text('demon\'s jester'/'DDC', 'Flying\nHellbent — Demon\'s Jester gets +2/+1 as long as you have no cards in hand.').
card_image_name('demon\'s jester'/'DDC', 'demon\'s jester').
card_uid('demon\'s jester'/'DDC', 'DDC:Demon\'s Jester:demon\'s jester').
card_rarity('demon\'s jester'/'DDC', 'Common').
card_artist('demon\'s jester'/'DDC', 'Pete Venters').
card_number('demon\'s jester'/'DDC', '38').
card_flavor_text('demon\'s jester'/'DDC', 'They knock \'em dead, with or without the punch line.').
card_multiverse_id('demon\'s jester'/'DDC', '197018').

card_in_set('demonic tutor', 'DDC').
card_original_type('demonic tutor'/'DDC', 'Sorcery').
card_original_text('demonic tutor'/'DDC', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('demonic tutor'/'DDC', 'demonic tutor').
card_uid('demonic tutor'/'DDC', 'DDC:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'DDC', 'Uncommon').
card_artist('demonic tutor'/'DDC', 'Scott Chou').
card_number('demonic tutor'/'DDC', '49').
card_flavor_text('demonic tutor'/'DDC', 'Liliana learned the secrets she sought, but at a price that was etched on her fate.').
card_multiverse_id('demonic tutor'/'DDC', '193867').

card_in_set('duress', 'DDC').
card_original_type('duress'/'DDC', 'Sorcery').
card_original_text('duress'/'DDC', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'DDC', 'duress').
card_uid('duress'/'DDC', 'DDC:Duress:duress').
card_rarity('duress'/'DDC', 'Common').
card_artist('duress'/'DDC', 'Steven Belledin').
card_number('duress'/'DDC', '46').
card_flavor_text('duress'/'DDC', '\"Change your mind, or I\'ll change it for you.\"').
card_multiverse_id('duress'/'DDC', '197004').

card_in_set('dusk imp', 'DDC').
card_original_type('dusk imp'/'DDC', 'Creature — Imp').
card_original_text('dusk imp'/'DDC', 'Flying').
card_image_name('dusk imp'/'DDC', 'dusk imp').
card_uid('dusk imp'/'DDC', 'DDC:Dusk Imp:dusk imp').
card_rarity('dusk imp'/'DDC', 'Common').
card_artist('dusk imp'/'DDC', 'Pete Venters').
card_number('dusk imp'/'DDC', '34').
card_flavor_text('dusk imp'/'DDC', 'Imps are just intelligent enough to have an understanding of cruelty.').
card_multiverse_id('dusk imp'/'DDC', '196995').

card_in_set('faith\'s fetters', 'DDC').
card_original_type('faith\'s fetters'/'DDC', 'Enchantment — Aura').
card_original_text('faith\'s fetters'/'DDC', 'Enchant permanent\nWhen Faith\'s Fetters comes into play, you gain 4 life.\nEnchanted permanent\'s activated abilities can\'t be played unless they\'re mana abilities. If enchanted permanent is a creature, it can\'t attack or block.').
card_image_name('faith\'s fetters'/'DDC', 'faith\'s fetters').
card_uid('faith\'s fetters'/'DDC', 'DDC:Faith\'s Fetters:faith\'s fetters').
card_rarity('faith\'s fetters'/'DDC', 'Common').
card_artist('faith\'s fetters'/'DDC', 'Brian Despain').
card_number('faith\'s fetters'/'DDC', '20').
card_multiverse_id('faith\'s fetters'/'DDC', '193869').

card_in_set('fallen angel', 'DDC').
card_original_type('fallen angel'/'DDC', 'Creature — Angel').
card_original_text('fallen angel'/'DDC', 'Flying\nSacrifice a creature: Fallen Angel gets +2/+1 until end of turn.').
card_image_name('fallen angel'/'DDC', 'fallen angel').
card_uid('fallen angel'/'DDC', 'DDC:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'DDC', 'Rare').
card_artist('fallen angel'/'DDC', 'Matthew D. Wilson').
card_number('fallen angel'/'DDC', '42').
card_flavor_text('fallen angel'/'DDC', '\"Why do you weep for the dead? I rejoice, for they have died for me.\"').
card_multiverse_id('fallen angel'/'DDC', '197010').

card_in_set('foul imp', 'DDC').
card_original_type('foul imp'/'DDC', 'Creature — Imp').
card_original_text('foul imp'/'DDC', 'Flying\nWhen Foul Imp comes into play, you lose 2 life.').
card_image_name('foul imp'/'DDC', 'foul imp').
card_uid('foul imp'/'DDC', 'DDC:Foul Imp:foul imp').
card_rarity('foul imp'/'DDC', 'Common').
card_artist('foul imp'/'DDC', 'Kev Walker').
card_number('foul imp'/'DDC', '32').
card_flavor_text('foul imp'/'DDC', 'The imp, unaware of its own odor, paused to catch its breath . . . and promptly died.').
card_multiverse_id('foul imp'/'DDC', '197012').

card_in_set('healing salve', 'DDC').
card_original_type('healing salve'/'DDC', 'Instant').
card_original_text('healing salve'/'DDC', 'Choose one — Target player gains 3 life; or prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_image_name('healing salve'/'DDC', 'healing salve').
card_uid('healing salve'/'DDC', 'DDC:Healing Salve:healing salve').
card_rarity('healing salve'/'DDC', 'Common').
card_artist('healing salve'/'DDC', 'Greg & Tim Hildebrandt').
card_number('healing salve'/'DDC', '14').
card_multiverse_id('healing salve'/'DDC', '197011').

card_in_set('icatian priest', 'DDC').
card_original_type('icatian priest'/'DDC', 'Creature — Human Cleric').
card_original_text('icatian priest'/'DDC', '{1}{W}{W}: Target creature gets +1/+1 until end of turn.').
card_image_name('icatian priest'/'DDC', 'icatian priest').
card_uid('icatian priest'/'DDC', 'DDC:Icatian Priest:icatian priest').
card_rarity('icatian priest'/'DDC', 'Uncommon').
card_artist('icatian priest'/'DDC', 'Stephen Tappin').
card_number('icatian priest'/'DDC', '2').
card_flavor_text('icatian priest'/'DDC', 'Grelden knelt and felt the cool, dry hand of the priest on his brow. Hours later, when his wits returned, he was covered in his enemies\' blood on the field of victory.').
card_multiverse_id('icatian priest'/'DDC', '196996').

card_in_set('kuro, pitlord', 'DDC').
card_original_type('kuro, pitlord'/'DDC', 'Legendary Creature — Demon Spirit').
card_original_text('kuro, pitlord'/'DDC', 'At the beginning of your upkeep, sacrifice Kuro, Pitlord unless you pay {B}{B}{B}{B}.\nPay 1 life: Target creature gets -1/-1 until end of turn.').
card_image_name('kuro, pitlord'/'DDC', 'kuro, pitlord').
card_uid('kuro, pitlord'/'DDC', 'DDC:Kuro, Pitlord:kuro, pitlord').
card_rarity('kuro, pitlord'/'DDC', 'Rare').
card_artist('kuro, pitlord'/'DDC', 'Jon Foster').
card_number('kuro, pitlord'/'DDC', '44').
card_multiverse_id('kuro, pitlord'/'DDC', '197015').

card_in_set('lord of the pit', 'DDC').
card_original_type('lord of the pit'/'DDC', 'Creature — Demon').
card_original_text('lord of the pit'/'DDC', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Lord of the Pit. If you can\'t, Lord of the Pit deals 7 damage to you.').
card_image_name('lord of the pit'/'DDC', 'lord of the pit').
card_uid('lord of the pit'/'DDC', 'DDC:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'DDC', 'Mythic Rare').
card_artist('lord of the pit'/'DDC', 'Chippy').
card_number('lord of the pit'/'DDC', '30').
card_flavor_text('lord of the pit'/'DDC', '\"My summoning begins your debt, planeswalker.\"').
card_multiverse_id('lord of the pit'/'DDC', '193868').

card_in_set('luminous angel', 'DDC').
card_original_type('luminous angel'/'DDC', 'Creature — Angel').
card_original_text('luminous angel'/'DDC', 'Flying\nAt the beginning of your upkeep, you may put a 1/1 white Spirit creature token with flying into play.').
card_image_name('luminous angel'/'DDC', 'luminous angel').
card_uid('luminous angel'/'DDC', 'DDC:Luminous Angel:luminous angel').
card_rarity('luminous angel'/'DDC', 'Rare').
card_artist('luminous angel'/'DDC', 'Jason Chan').
card_number('luminous angel'/'DDC', '12').
card_flavor_text('luminous angel'/'DDC', '\"The spread of evil\'s destruction shall always be matched by the rise of the noble fallen.\"').
card_multiverse_id('luminous angel'/'DDC', '193872').

card_in_set('marble diamond', 'DDC').
card_original_type('marble diamond'/'DDC', 'Artifact').
card_original_text('marble diamond'/'DDC', 'Marble Diamond comes into play tapped.\n{T}: Add {W} to your mana pool.').
card_image_name('marble diamond'/'DDC', 'marble diamond').
card_uid('marble diamond'/'DDC', 'DDC:Marble Diamond:marble diamond').
card_rarity('marble diamond'/'DDC', 'Uncommon').
card_artist('marble diamond'/'DDC', 'David Martin').
card_number('marble diamond'/'DDC', '24').
card_multiverse_id('marble diamond'/'DDC', '197005').

card_in_set('oni possession', 'DDC').
card_original_type('oni possession'/'DDC', 'Enchantment — Aura').
card_original_text('oni possession'/'DDC', 'Enchant creature\nAt the beginning of your upkeep, sacrifice a creature.\nEnchanted creature gets +3/+3 and has trample.\nEnchanted creature is a Demon Spirit.').
card_image_name('oni possession'/'DDC', 'oni possession').
card_uid('oni possession'/'DDC', 'DDC:Oni Possession:oni possession').
card_rarity('oni possession'/'DDC', 'Uncommon').
card_artist('oni possession'/'DDC', 'Aleksi Briclot').
card_number('oni possession'/'DDC', '51').
card_multiverse_id('oni possession'/'DDC', '197016').

card_in_set('otherworldly journey', 'DDC').
card_original_type('otherworldly journey'/'DDC', 'Instant — Arcane').
card_original_text('otherworldly journey'/'DDC', 'Remove target creature from the game. At end of turn, return that card to play under its owner\'s control with a +1/+1 counter on it.').
card_image_name('otherworldly journey'/'DDC', 'otherworldly journey').
card_uid('otherworldly journey'/'DDC', 'DDC:Otherworldly Journey:otherworldly journey').
card_rarity('otherworldly journey'/'DDC', 'Uncommon').
card_artist('otherworldly journey'/'DDC', 'Vance Kovacs').
card_number('otherworldly journey'/'DDC', '16').
card_flavor_text('otherworldly journey'/'DDC', '\"The landscape shimmered and I felt a chill breeze. When my vision cleared, I found myself alone among the corpses of my fallen friends.\"\n—Journal found in Numai').
card_multiverse_id('otherworldly journey'/'DDC', '197017').

card_in_set('overeager apprentice', 'DDC').
card_original_type('overeager apprentice'/'DDC', 'Creature — Human Minion').
card_original_text('overeager apprentice'/'DDC', 'Discard a card, Sacrifice Overeager Apprentice: Add {B}{B}{B} to your mana pool.').
card_image_name('overeager apprentice'/'DDC', 'overeager apprentice').
card_uid('overeager apprentice'/'DDC', 'DDC:Overeager Apprentice:overeager apprentice').
card_rarity('overeager apprentice'/'DDC', 'Common').
card_artist('overeager apprentice'/'DDC', 'Ray Lago').
card_number('overeager apprentice'/'DDC', '35').
card_flavor_text('overeager apprentice'/'DDC', 'Slow and steady may not always win the race, but at least it doesn\'t end up splattered on the walls.').
card_multiverse_id('overeager apprentice'/'DDC', '197028').

card_in_set('pacifism', 'DDC').
card_original_type('pacifism'/'DDC', 'Enchantment — Aura').
card_original_text('pacifism'/'DDC', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'DDC', 'pacifism').
card_uid('pacifism'/'DDC', 'DDC:Pacifism:pacifism').
card_rarity('pacifism'/'DDC', 'Common').
card_artist('pacifism'/'DDC', 'Matthew D. Wilson').
card_number('pacifism'/'DDC', '17').
card_flavor_text('pacifism'/'DDC', 'Even those born to battle could only lay their blades at Akroma\'s feet.').
card_multiverse_id('pacifism'/'DDC', '196997').

card_in_set('plains', 'DDC').
card_original_type('plains'/'DDC', 'Basic Land — Plains').
card_original_text('plains'/'DDC', 'W').
card_image_name('plains'/'DDC', 'plains1').
card_uid('plains'/'DDC', 'DDC:Plains:plains1').
card_rarity('plains'/'DDC', 'Basic Land').
card_artist('plains'/'DDC', 'Rob Alexander').
card_number('plains'/'DDC', '26').
card_multiverse_id('plains'/'DDC', '197254').

card_in_set('plains', 'DDC').
card_original_type('plains'/'DDC', 'Basic Land — Plains').
card_original_text('plains'/'DDC', 'W').
card_image_name('plains'/'DDC', 'plains2').
card_uid('plains'/'DDC', 'DDC:Plains:plains2').
card_rarity('plains'/'DDC', 'Basic Land').
card_artist('plains'/'DDC', 'Rob Alexander').
card_number('plains'/'DDC', '27').
card_multiverse_id('plains'/'DDC', '197256').

card_in_set('plains', 'DDC').
card_original_type('plains'/'DDC', 'Basic Land — Plains').
card_original_text('plains'/'DDC', 'W').
card_image_name('plains'/'DDC', 'plains3').
card_uid('plains'/'DDC', 'DDC:Plains:plains3').
card_rarity('plains'/'DDC', 'Basic Land').
card_artist('plains'/'DDC', 'Rob Alexander').
card_number('plains'/'DDC', '28').
card_multiverse_id('plains'/'DDC', '197255').

card_in_set('plains', 'DDC').
card_original_type('plains'/'DDC', 'Basic Land — Plains').
card_original_text('plains'/'DDC', 'W').
card_image_name('plains'/'DDC', 'plains4').
card_uid('plains'/'DDC', 'DDC:Plains:plains4').
card_rarity('plains'/'DDC', 'Basic Land').
card_artist('plains'/'DDC', 'Rob Alexander').
card_number('plains'/'DDC', '29').
card_multiverse_id('plains'/'DDC', '197253').

card_in_set('promise of power', 'DDC').
card_original_type('promise of power'/'DDC', 'Sorcery').
card_original_text('promise of power'/'DDC', 'Choose one — You draw five cards and you lose 5 life; or put a black Demon creature token with flying into play with power and toughness each equal to the number of cards in your hand as the token comes into play.\nEntwine {4} (Choose both if you pay the entwine cost.)').
card_image_name('promise of power'/'DDC', 'promise of power').
card_uid('promise of power'/'DDC', 'DDC:Promise of Power:promise of power').
card_rarity('promise of power'/'DDC', 'Rare').
card_artist('promise of power'/'DDC', 'Kev Walker').
card_number('promise of power'/'DDC', '54').
card_multiverse_id('promise of power'/'DDC', '197026').

card_in_set('reiver demon', 'DDC').
card_original_type('reiver demon'/'DDC', 'Creature — Demon').
card_original_text('reiver demon'/'DDC', 'Flying\nWhen Reiver Demon comes into play, if you played it from your hand, destroy all nonartifact, nonblack creatures. They can\'t be regenerated.').
card_image_name('reiver demon'/'DDC', 'reiver demon').
card_uid('reiver demon'/'DDC', 'DDC:Reiver Demon:reiver demon').
card_rarity('reiver demon'/'DDC', 'Rare').
card_artist('reiver demon'/'DDC', 'Brom').
card_number('reiver demon'/'DDC', '43').
card_multiverse_id('reiver demon'/'DDC', '197027').

card_in_set('reya dawnbringer', 'DDC').
card_original_type('reya dawnbringer'/'DDC', 'Legendary Creature — Angel').
card_original_text('reya dawnbringer'/'DDC', 'Flying\nAt the beginning of your upkeep, you may return target creature card from your graveyard to play.').
card_image_name('reya dawnbringer'/'DDC', 'reya dawnbringer').
card_uid('reya dawnbringer'/'DDC', 'DDC:Reya Dawnbringer:reya dawnbringer').
card_rarity('reya dawnbringer'/'DDC', 'Rare').
card_artist('reya dawnbringer'/'DDC', 'Matthew D. Wilson').
card_number('reya dawnbringer'/'DDC', '13').
card_flavor_text('reya dawnbringer'/'DDC', '\"You have not died until I consent.\"').
card_multiverse_id('reya dawnbringer'/'DDC', '196998').

card_in_set('righteous cause', 'DDC').
card_original_type('righteous cause'/'DDC', 'Enchantment').
card_original_text('righteous cause'/'DDC', 'Whenever a creature attacks, you gain 1 life.').
card_image_name('righteous cause'/'DDC', 'righteous cause').
card_uid('righteous cause'/'DDC', 'DDC:Righteous Cause:righteous cause').
card_rarity('righteous cause'/'DDC', 'Uncommon').
card_artist('righteous cause'/'DDC', 'Scott M. Fischer').
card_number('righteous cause'/'DDC', '22').
card_flavor_text('righteous cause'/'DDC', '\"Until the world unites in vengeful fury and Phage is destroyed, I will not stay my hand.\"\n—Akroma, angelic avenger').
card_multiverse_id('righteous cause'/'DDC', '197030').

card_in_set('secluded steppe', 'DDC').
card_original_type('secluded steppe'/'DDC', 'Land').
card_original_text('secluded steppe'/'DDC', 'Secluded Steppe comes into play tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'DDC', 'secluded steppe').
card_uid('secluded steppe'/'DDC', 'DDC:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'DDC', 'Common').
card_artist('secluded steppe'/'DDC', 'Heather Hudson').
card_number('secluded steppe'/'DDC', '25').
card_multiverse_id('secluded steppe'/'DDC', '197031').

card_in_set('serra advocate', 'DDC').
card_original_type('serra advocate'/'DDC', 'Creature — Angel').
card_original_text('serra advocate'/'DDC', 'Flying\n{T}: Target attacking or blocking creature gets +2/+2 until end of turn.').
card_image_name('serra advocate'/'DDC', 'serra advocate').
card_uid('serra advocate'/'DDC', 'DDC:Serra Advocate:serra advocate').
card_rarity('serra advocate'/'DDC', 'Uncommon').
card_artist('serra advocate'/'DDC', 'Matthew D. Wilson').
card_number('serra advocate'/'DDC', '7').
card_flavor_text('serra advocate'/'DDC', 'An angel\'s blessing prepares a soldier for war better than a thousand military drills.').
card_multiverse_id('serra advocate'/'DDC', '197006').

card_in_set('serra angel', 'DDC').
card_original_type('serra angel'/'DDC', 'Creature — Angel').
card_original_text('serra angel'/'DDC', 'Flying, vigilance').
card_image_name('serra angel'/'DDC', 'serra angel').
card_uid('serra angel'/'DDC', 'DDC:Serra Angel:serra angel').
card_rarity('serra angel'/'DDC', 'Rare').
card_artist('serra angel'/'DDC', 'Greg Staples').
card_number('serra angel'/'DDC', '10').
card_flavor_text('serra angel'/'DDC', 'Her sword sings more beautifully than any choir.').
card_multiverse_id('serra angel'/'DDC', '196999').

card_in_set('serra\'s boon', 'DDC').
card_original_type('serra\'s boon'/'DDC', 'Enchantment — Aura').
card_original_text('serra\'s boon'/'DDC', 'Enchant creature\nEnchanted creature gets +1/+2 as long as it\'s white. Otherwise, it gets -2/-1.').
card_image_name('serra\'s boon'/'DDC', 'serra\'s boon').
card_uid('serra\'s boon'/'DDC', 'DDC:Serra\'s Boon:serra\'s boon').
card_rarity('serra\'s boon'/'DDC', 'Uncommon').
card_artist('serra\'s boon'/'DDC', 'Steven Belledin').
card_number('serra\'s boon'/'DDC', '18').
card_flavor_text('serra\'s boon'/'DDC', '\"The light of an angel\'s glance is warm, but her fixed stare blinds and burns.\"\n—Calexis, deacon of the New Order of Serra').
card_multiverse_id('serra\'s boon'/'DDC', '197032').

card_in_set('serra\'s embrace', 'DDC').
card_original_type('serra\'s embrace'/'DDC', 'Enchantment — Aura').
card_original_text('serra\'s embrace'/'DDC', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying and vigilance.').
card_image_name('serra\'s embrace'/'DDC', 'serra\'s embrace').
card_uid('serra\'s embrace'/'DDC', 'DDC:Serra\'s Embrace:serra\'s embrace').
card_rarity('serra\'s embrace'/'DDC', 'Uncommon').
card_artist('serra\'s embrace'/'DDC', 'Zoltan Boros & Gabor Szikszai').
card_number('serra\'s embrace'/'DDC', '21').
card_flavor_text('serra\'s embrace'/'DDC', 'The touch of Serra\'s angels bears hopes aloft and empowers noble causes.').
card_multiverse_id('serra\'s embrace'/'DDC', '197000').

card_in_set('soot imp', 'DDC').
card_original_type('soot imp'/'DDC', 'Creature — Imp').
card_original_text('soot imp'/'DDC', 'Flying\nWhenever a player plays a nonblack spell, that player loses 1 life.').
card_image_name('soot imp'/'DDC', 'soot imp').
card_uid('soot imp'/'DDC', 'DDC:Soot Imp:soot imp').
card_rarity('soot imp'/'DDC', 'Uncommon').
card_artist('soot imp'/'DDC', 'Jesper Ejsing').
card_number('soot imp'/'DDC', '37').
card_flavor_text('soot imp'/'DDC', '\"If one gets in your chimney, you\'re going to need a long wick and a barrel of bangstuff to get it out.\"\n—Hob Heddil').
card_multiverse_id('soot imp'/'DDC', '197020').

card_in_set('souldrinker', 'DDC').
card_original_type('souldrinker'/'DDC', 'Creature — Spirit').
card_original_text('souldrinker'/'DDC', 'Pay 3 life: Put a +1/+1 counter on Souldrinker.').
card_image_name('souldrinker'/'DDC', 'souldrinker').
card_uid('souldrinker'/'DDC', 'DDC:Souldrinker:souldrinker').
card_rarity('souldrinker'/'DDC', 'Uncommon').
card_artist('souldrinker'/'DDC', 'Dermot Power').
card_number('souldrinker'/'DDC', '39').
card_flavor_text('souldrinker'/'DDC', 'Don\'t drink and thrive.').
card_multiverse_id('souldrinker'/'DDC', '197036').

card_in_set('spirit', 'DDC').
card_original_type('spirit'/'DDC', 'Creature — Spirit').
card_original_text('spirit'/'DDC', 'Flying').
card_first_print('spirit', 'DDC').
card_image_name('spirit'/'DDC', 'spirit').
card_uid('spirit'/'DDC', 'DDC:Spirit:spirit').
card_rarity('spirit'/'DDC', 'Common').
card_artist('spirit'/'DDC', 'Luca Zontini').
card_number('spirit'/'DDC', 'T1').
card_multiverse_id('spirit'/'DDC', '197937').

card_in_set('stinkweed imp', 'DDC').
card_original_type('stinkweed imp'/'DDC', 'Creature — Imp').
card_original_text('stinkweed imp'/'DDC', 'Flying\nWhenever Stinkweed Imp deals combat damage to a creature, destroy that creature.\nDredge 5 (If you would draw a card, instead you may put exactly five cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_image_name('stinkweed imp'/'DDC', 'stinkweed imp').
card_uid('stinkweed imp'/'DDC', 'DDC:Stinkweed Imp:stinkweed imp').
card_rarity('stinkweed imp'/'DDC', 'Common').
card_artist('stinkweed imp'/'DDC', 'Nils Hamm').
card_number('stinkweed imp'/'DDC', '36').
card_multiverse_id('stinkweed imp'/'DDC', '193870').

card_in_set('sustainer of the realm', 'DDC').
card_original_type('sustainer of the realm'/'DDC', 'Creature — Angel').
card_original_text('sustainer of the realm'/'DDC', 'Flying\nWhenever Sustainer of the Realm blocks, it gets +0/+2 until end of turn.').
card_image_name('sustainer of the realm'/'DDC', 'sustainer of the realm').
card_uid('sustainer of the realm'/'DDC', 'DDC:Sustainer of the Realm:sustainer of the realm').
card_rarity('sustainer of the realm'/'DDC', 'Uncommon').
card_artist('sustainer of the realm'/'DDC', 'Mark Zug').
card_number('sustainer of the realm'/'DDC', '8').
card_flavor_text('sustainer of the realm'/'DDC', 'You may break her shield, but you\'ll never break her spirit.').
card_multiverse_id('sustainer of the realm'/'DDC', '197007').

card_in_set('swamp', 'DDC').
card_original_type('swamp'/'DDC', 'Basic Land — Swamp').
card_original_text('swamp'/'DDC', 'B').
card_image_name('swamp'/'DDC', 'swamp1').
card_uid('swamp'/'DDC', 'DDC:Swamp:swamp1').
card_rarity('swamp'/'DDC', 'Basic Land').
card_artist('swamp'/'DDC', 'Mark Tedin').
card_number('swamp'/'DDC', '59').
card_multiverse_id('swamp'/'DDC', '197257').

card_in_set('swamp', 'DDC').
card_original_type('swamp'/'DDC', 'Basic Land — Swamp').
card_original_text('swamp'/'DDC', 'B').
card_image_name('swamp'/'DDC', 'swamp2').
card_uid('swamp'/'DDC', 'DDC:Swamp:swamp2').
card_rarity('swamp'/'DDC', 'Basic Land').
card_artist('swamp'/'DDC', 'Mark Tedin').
card_number('swamp'/'DDC', '60').
card_multiverse_id('swamp'/'DDC', '197258').

card_in_set('swamp', 'DDC').
card_original_type('swamp'/'DDC', 'Basic Land — Swamp').
card_original_text('swamp'/'DDC', 'B').
card_image_name('swamp'/'DDC', 'swamp3').
card_uid('swamp'/'DDC', 'DDC:Swamp:swamp3').
card_rarity('swamp'/'DDC', 'Basic Land').
card_artist('swamp'/'DDC', 'John Avon').
card_number('swamp'/'DDC', '61').
card_multiverse_id('swamp'/'DDC', '197259').

card_in_set('swamp', 'DDC').
card_original_type('swamp'/'DDC', 'Basic Land — Swamp').
card_original_text('swamp'/'DDC', 'B').
card_image_name('swamp'/'DDC', 'swamp4').
card_uid('swamp'/'DDC', 'DDC:Swamp:swamp4').
card_rarity('swamp'/'DDC', 'Basic Land').
card_artist('swamp'/'DDC', 'Vance Kovacs').
card_number('swamp'/'DDC', '62').
card_multiverse_id('swamp'/'DDC', '197260').

card_in_set('thrull', 'DDC').
card_original_type('thrull'/'DDC', 'Creature — Thrull').
card_original_text('thrull'/'DDC', '').
card_first_print('thrull', 'DDC').
card_image_name('thrull'/'DDC', 'thrull').
card_uid('thrull'/'DDC', 'DDC:Thrull:thrull').
card_rarity('thrull'/'DDC', 'Common').
card_artist('thrull'/'DDC', 'Véronique Meignaud').
card_number('thrull'/'DDC', 'T3').
card_multiverse_id('thrull'/'DDC', '197261').

card_in_set('twilight shepherd', 'DDC').
card_original_type('twilight shepherd'/'DDC', 'Creature — Angel').
card_original_text('twilight shepherd'/'DDC', 'Flying, vigilance\nWhen Twilight Shepherd comes into play, return to your hand all cards in your graveyard put there from play this turn.\nPersist (When this creature is put into a graveyard from play, if it had no -1/-1 counters on it, return it to play under its owner\'s control with a -1/-1 counter on it.)').
card_image_name('twilight shepherd'/'DDC', 'twilight shepherd').
card_uid('twilight shepherd'/'DDC', 'DDC:Twilight Shepherd:twilight shepherd').
card_rarity('twilight shepherd'/'DDC', 'Rare').
card_artist('twilight shepherd'/'DDC', 'Jason Chan').
card_number('twilight shepherd'/'DDC', '11').
card_multiverse_id('twilight shepherd'/'DDC', '197034').

card_in_set('unholy strength', 'DDC').
card_original_type('unholy strength'/'DDC', 'Enchantment — Aura').
card_original_text('unholy strength'/'DDC', 'Enchant creature\nEnchanted creature gets +2/+1.').
card_image_name('unholy strength'/'DDC', 'unholy strength').
card_uid('unholy strength'/'DDC', 'DDC:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'DDC', 'Common').
card_artist('unholy strength'/'DDC', 'Terese Nielsen').
card_number('unholy strength'/'DDC', '47').
card_flavor_text('unholy strength'/'DDC', 'Such power grows the body as it shrinks the soul.').
card_multiverse_id('unholy strength'/'DDC', '197001').

card_in_set('venerable monk', 'DDC').
card_original_type('venerable monk'/'DDC', 'Creature — Human Monk Cleric').
card_original_text('venerable monk'/'DDC', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'DDC', 'venerable monk').
card_uid('venerable monk'/'DDC', 'DDC:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'DDC', 'Common').
card_artist('venerable monk'/'DDC', 'D. Alexander Gregory').
card_number('venerable monk'/'DDC', '5').
card_flavor_text('venerable monk'/'DDC', 'Age wears the flesh but galvanizes the soul.').
card_multiverse_id('venerable monk'/'DDC', '197002').
