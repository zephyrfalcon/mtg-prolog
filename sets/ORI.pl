% Magic Origins

set('ORI').
set_name('ORI', 'Magic Origins').
set_release_date('ORI', '2015-07-17').
set_border('ORI', 'black').
set_type('ORI', 'core').

card_in_set('abbot of keral keep', 'ORI').
card_original_type('abbot of keral keep'/'ORI', 'Creature — Human Monk').
card_original_text('abbot of keral keep'/'ORI', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)When Abbot of Keral Keep enters the battlefield, exile the top card of your library. Until end of turn, you may play that card.').
card_first_print('abbot of keral keep', 'ORI').
card_image_name('abbot of keral keep'/'ORI', 'abbot of keral keep').
card_uid('abbot of keral keep'/'ORI', 'ORI:Abbot of Keral Keep:abbot of keral keep').
card_rarity('abbot of keral keep'/'ORI', 'Rare').
card_artist('abbot of keral keep'/'ORI', 'Deruchenko Alexander').
card_number('abbot of keral keep'/'ORI', '127').
card_multiverse_id('abbot of keral keep'/'ORI', '398411').

card_in_set('acolyte of the inferno', 'ORI').
card_original_type('acolyte of the inferno'/'ORI', 'Creature — Human Monk').
card_original_text('acolyte of the inferno'/'ORI', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)Whenever Acolyte of the Inferno becomes blocked by a creature, it deals 2 damage to that creature.').
card_first_print('acolyte of the inferno', 'ORI').
card_image_name('acolyte of the inferno'/'ORI', 'acolyte of the inferno').
card_uid('acolyte of the inferno'/'ORI', 'ORI:Acolyte of the Inferno:acolyte of the inferno').
card_rarity('acolyte of the inferno'/'ORI', 'Uncommon').
card_artist('acolyte of the inferno'/'ORI', 'Joseph Meehan').
card_number('acolyte of the inferno'/'ORI', '128').
card_multiverse_id('acolyte of the inferno'/'ORI', '398574').

card_in_set('act of treason', 'ORI').
card_original_type('act of treason'/'ORI', 'Sorcery').
card_original_text('act of treason'/'ORI', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('act of treason'/'ORI', 'act of treason').
card_uid('act of treason'/'ORI', 'ORI:Act of Treason:act of treason').
card_rarity('act of treason'/'ORI', 'Common').
card_artist('act of treason'/'ORI', 'Eric Deschamps').
card_number('act of treason'/'ORI', '129').
card_flavor_text('act of treason'/'ORI', 'She learned a tragic lesson that day: even the purest can be corrupted when the heart\'s emotions are twisted.').
card_multiverse_id('act of treason'/'ORI', '398578').

card_in_set('aegis angel', 'ORI').
card_original_type('aegis angel'/'ORI', 'Creature — Angel').
card_original_text('aegis angel'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Aegis Angel enters the battlefield, another target permanent gains indestructible for as long as you control Aegis Angel. (Effects that say \"destroy\" don\'t destroy it. A creature with indestructible can\'t be destroyed by damage.)').
card_image_name('aegis angel'/'ORI', 'aegis angel').
card_uid('aegis angel'/'ORI', 'ORI:Aegis Angel:aegis angel').
card_rarity('aegis angel'/'ORI', 'Rare').
card_artist('aegis angel'/'ORI', 'Aleksi Briclot').
card_number('aegis angel'/'ORI', '273').
card_multiverse_id('aegis angel'/'ORI', '401452').

card_in_set('aerial volley', 'ORI').
card_original_type('aerial volley'/'ORI', 'Instant').
card_original_text('aerial volley'/'ORI', 'Aerial Volley deals 3 damage divided as you choose among one, two, or three target creatures with flying.').
card_first_print('aerial volley', 'ORI').
card_image_name('aerial volley'/'ORI', 'aerial volley').
card_uid('aerial volley'/'ORI', 'ORI:Aerial Volley:aerial volley').
card_rarity('aerial volley'/'ORI', 'Common').
card_artist('aerial volley'/'ORI', 'Lake Hurwitz').
card_number('aerial volley'/'ORI', '168').
card_flavor_text('aerial volley'/'ORI', 'Drakes can swerve to avoid a single arrow, but they can\'t dodge the whole sky.').
card_multiverse_id('aerial volley'/'ORI', '398565').

card_in_set('akroan jailer', 'ORI').
card_original_type('akroan jailer'/'ORI', 'Creature — Human Soldier').
card_original_text('akroan jailer'/'ORI', '{2}{W}, {T}: Tap target creature.').
card_first_print('akroan jailer', 'ORI').
card_image_name('akroan jailer'/'ORI', 'akroan jailer').
card_uid('akroan jailer'/'ORI', 'ORI:Akroan Jailer:akroan jailer').
card_rarity('akroan jailer'/'ORI', 'Common').
card_artist('akroan jailer'/'ORI', 'Anastasia Ovchinnikova').
card_number('akroan jailer'/'ORI', '1').
card_flavor_text('akroan jailer'/'ORI', 'He ensures escape attempts are just that—attempts.').
card_multiverse_id('akroan jailer'/'ORI', '398656').

card_in_set('akroan sergeant', 'ORI').
card_original_type('akroan sergeant'/'ORI', 'Creature — Human Soldier').
card_original_text('akroan sergeant'/'ORI', 'First strike (This creature deals combat damage before creatures without first strike.)Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_first_print('akroan sergeant', 'ORI').
card_image_name('akroan sergeant'/'ORI', 'akroan sergeant').
card_uid('akroan sergeant'/'ORI', 'ORI:Akroan Sergeant:akroan sergeant').
card_rarity('akroan sergeant'/'ORI', 'Common').
card_artist('akroan sergeant'/'ORI', 'Zack Stella').
card_number('akroan sergeant'/'ORI', '130').
card_multiverse_id('akroan sergeant'/'ORI', '398604').

card_in_set('alchemist\'s vial', 'ORI').
card_original_type('alchemist\'s vial'/'ORI', 'Artifact').
card_original_text('alchemist\'s vial'/'ORI', 'When Alchemist\'s Vial enters the battlefield, draw a card.{1}, {T}, Sacrifice Alchemist\'s Vial: Target creature can\'t attack or block this turn.').
card_first_print('alchemist\'s vial', 'ORI').
card_image_name('alchemist\'s vial'/'ORI', 'alchemist\'s vial').
card_uid('alchemist\'s vial'/'ORI', 'ORI:Alchemist\'s Vial:alchemist\'s vial').
card_rarity('alchemist\'s vial'/'ORI', 'Common').
card_artist('alchemist\'s vial'/'ORI', 'Lindsey Look').
card_number('alchemist\'s vial'/'ORI', '220').
card_flavor_text('alchemist\'s vial'/'ORI', 'A weapon best suited for those with good aim and steady hands.').
card_multiverse_id('alchemist\'s vial'/'ORI', '398640').

card_in_set('alhammarret\'s archive', 'ORI').
card_original_type('alhammarret\'s archive'/'ORI', 'Legendary Artifact').
card_original_text('alhammarret\'s archive'/'ORI', 'If you would gain life, you gain twice that much life instead.If you would draw a card except the first one you draw in each of your draw steps, draw two cards instead.').
card_first_print('alhammarret\'s archive', 'ORI').
card_image_name('alhammarret\'s archive'/'ORI', 'alhammarret\'s archive').
card_uid('alhammarret\'s archive'/'ORI', 'ORI:Alhammarret\'s Archive:alhammarret\'s archive').
card_rarity('alhammarret\'s archive'/'ORI', 'Mythic Rare').
card_artist('alhammarret\'s archive'/'ORI', 'Richard Wright').
card_number('alhammarret\'s archive'/'ORI', '221').
card_flavor_text('alhammarret\'s archive'/'ORI', 'It contains the totality of Alhammarret\'s notes, theories, and ponderings.').
card_multiverse_id('alhammarret\'s archive'/'ORI', '398564').

card_in_set('alhammarret, high arbiter', 'ORI').
card_original_type('alhammarret, high arbiter'/'ORI', 'Legendary Creature — Sphinx').
card_original_text('alhammarret, high arbiter'/'ORI', 'FlyingAs Alhammarret, High Arbiter enters the battlefield, each opponent reveals his or her hand. You choose the name of a nonland card revealed this way.Your opponents can\'t cast spells with the chosen name (as long as this creature is on the battlefield).').
card_first_print('alhammarret, high arbiter', 'ORI').
card_image_name('alhammarret, high arbiter'/'ORI', 'alhammarret, high arbiter').
card_uid('alhammarret, high arbiter'/'ORI', 'ORI:Alhammarret, High Arbiter:alhammarret, high arbiter').
card_rarity('alhammarret, high arbiter'/'ORI', 'Rare').
card_artist('alhammarret, high arbiter'/'ORI', 'Richard Wright').
card_number('alhammarret, high arbiter'/'ORI', '43').
card_multiverse_id('alhammarret, high arbiter'/'ORI', '398436').

card_in_set('ampryn tactician', 'ORI').
card_original_type('ampryn tactician'/'ORI', 'Creature — Human Soldier').
card_original_text('ampryn tactician'/'ORI', 'When Ampryn Tactician enters the battlefield, creatures you control get +1/+1 until end of turn.').
card_first_print('ampryn tactician', 'ORI').
card_image_name('ampryn tactician'/'ORI', 'ampryn tactician').
card_uid('ampryn tactician'/'ORI', 'ORI:Ampryn Tactician:ampryn tactician').
card_rarity('ampryn tactician'/'ORI', 'Common').
card_artist('ampryn tactician'/'ORI', 'Cynthia Sheppard').
card_number('ampryn tactician'/'ORI', '2').
card_flavor_text('ampryn tactician'/'ORI', '\"It\'s all a game. You shouldn\'t get too attached to the pieces.\"').
card_multiverse_id('ampryn tactician'/'ORI', '398603').

card_in_set('anchor to the æther', 'ORI').
card_original_type('anchor to the æther'/'ORI', 'Sorcery').
card_original_text('anchor to the æther'/'ORI', 'Put target creature on top of its owner\'s library. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('anchor to the æther', 'ORI').
card_image_name('anchor to the æther'/'ORI', 'anchor to the aether').
card_uid('anchor to the æther'/'ORI', 'ORI:Anchor to the Æther:anchor to the aether').
card_rarity('anchor to the æther'/'ORI', 'Uncommon').
card_artist('anchor to the æther'/'ORI', 'Zoltan Boros').
card_number('anchor to the æther'/'ORI', '44').
card_multiverse_id('anchor to the æther'/'ORI', '398474').

card_in_set('angel\'s tomb', 'ORI').
card_original_type('angel\'s tomb'/'ORI', 'Artifact').
card_original_text('angel\'s tomb'/'ORI', 'Whenever a creature enters the battlefield under your control, you may have Angel\'s Tomb become a 3/3 white Angel artifact creature with flying until end of turn.').
card_image_name('angel\'s tomb'/'ORI', 'angel\'s tomb').
card_uid('angel\'s tomb'/'ORI', 'ORI:Angel\'s Tomb:angel\'s tomb').
card_rarity('angel\'s tomb'/'ORI', 'Uncommon').
card_artist('angel\'s tomb'/'ORI', 'Dan Scott').
card_number('angel\'s tomb'/'ORI', '222').
card_flavor_text('angel\'s tomb'/'ORI', '\"Faith can quicken the stones themselves with life.\"—Writings of Mikaeus').
card_multiverse_id('angel\'s tomb'/'ORI', '398418').

card_in_set('animist\'s awakening', 'ORI').
card_original_type('animist\'s awakening'/'ORI', 'Sorcery').
card_original_text('animist\'s awakening'/'ORI', 'Reveal the top X cards of your library. Put all land cards from among them onto the battlefield tapped and the rest on the bottom of your library in a random order.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, untap those lands.').
card_first_print('animist\'s awakening', 'ORI').
card_image_name('animist\'s awakening'/'ORI', 'animist\'s awakening').
card_uid('animist\'s awakening'/'ORI', 'ORI:Animist\'s Awakening:animist\'s awakening').
card_rarity('animist\'s awakening'/'ORI', 'Rare').
card_artist('animist\'s awakening'/'ORI', 'Chris Rahn').
card_number('animist\'s awakening'/'ORI', '169').
card_multiverse_id('animist\'s awakening'/'ORI', '398437').

card_in_set('anointer of champions', 'ORI').
card_original_type('anointer of champions'/'ORI', 'Creature — Human Cleric').
card_original_text('anointer of champions'/'ORI', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_first_print('anointer of champions', 'ORI').
card_image_name('anointer of champions'/'ORI', 'anointer of champions').
card_uid('anointer of champions'/'ORI', 'ORI:Anointer of Champions:anointer of champions').
card_rarity('anointer of champions'/'ORI', 'Uncommon').
card_artist('anointer of champions'/'ORI', 'Anna Steinbauer').
card_number('anointer of champions'/'ORI', '3').
card_flavor_text('anointer of champions'/'ORI', '\"Arise. You have been anointed by the light. Go forth and fight without fear, for you shall be victorious.\"').
card_multiverse_id('anointer of champions'/'ORI', '398455').

card_in_set('archangel of tithes', 'ORI').
card_original_type('archangel of tithes'/'ORI', 'Creature — Angel').
card_original_text('archangel of tithes'/'ORI', 'FlyingAs long as Archangel of Tithes is untapped, creatures can\'t attack you or a planeswalker you control unless their controller pays {1} for each of those creatures.As long as Archangel of Tithes is attacking, creatures can\'t block unless their controller pays {1} for each of those creatures.').
card_first_print('archangel of tithes', 'ORI').
card_image_name('archangel of tithes'/'ORI', 'archangel of tithes').
card_uid('archangel of tithes'/'ORI', 'ORI:Archangel of Tithes:archangel of tithes').
card_rarity('archangel of tithes'/'ORI', 'Mythic Rare').
card_artist('archangel of tithes'/'ORI', 'Cynthia Sheppard').
card_number('archangel of tithes'/'ORI', '4').
card_multiverse_id('archangel of tithes'/'ORI', '398571').

card_in_set('artificer\'s epiphany', 'ORI').
card_original_type('artificer\'s epiphany'/'ORI', 'Instant').
card_original_text('artificer\'s epiphany'/'ORI', 'Draw two cards. If you control no artifacts, discard a card.').
card_first_print('artificer\'s epiphany', 'ORI').
card_image_name('artificer\'s epiphany'/'ORI', 'artificer\'s epiphany').
card_uid('artificer\'s epiphany'/'ORI', 'ORI:Artificer\'s Epiphany:artificer\'s epiphany').
card_rarity('artificer\'s epiphany'/'ORI', 'Common').
card_artist('artificer\'s epiphany'/'ORI', 'Kieran Yanner').
card_number('artificer\'s epiphany'/'ORI', '45').
card_flavor_text('artificer\'s epiphany'/'ORI', 'The artificers of Kaladesh strive ceaselessly for perfection, progress, and the ultimate expression of elegance.').
card_multiverse_id('artificer\'s epiphany'/'ORI', '398462').

card_in_set('aspiring aeronaut', 'ORI').
card_original_type('aspiring aeronaut'/'ORI', 'Creature — Human Artificer').
card_original_text('aspiring aeronaut'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)When Aspiring Aeronaut enters the battlefield, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield.').
card_first_print('aspiring aeronaut', 'ORI').
card_image_name('aspiring aeronaut'/'ORI', 'aspiring aeronaut').
card_uid('aspiring aeronaut'/'ORI', 'ORI:Aspiring Aeronaut:aspiring aeronaut').
card_rarity('aspiring aeronaut'/'ORI', 'Common').
card_artist('aspiring aeronaut'/'ORI', 'Willian Murai').
card_number('aspiring aeronaut'/'ORI', '46').
card_multiverse_id('aspiring aeronaut'/'ORI', '398674').

card_in_set('auramancer', 'ORI').
card_original_type('auramancer'/'ORI', 'Creature — Human Wizard').
card_original_text('auramancer'/'ORI', 'When Auramancer enters the battlefield, you may return target enchantment card from your graveyard to your hand.').
card_image_name('auramancer'/'ORI', 'auramancer').
card_uid('auramancer'/'ORI', 'ORI:Auramancer:auramancer').
card_rarity('auramancer'/'ORI', 'Common').
card_artist('auramancer'/'ORI', 'Rebecca Guay').
card_number('auramancer'/'ORI', '5').
card_flavor_text('auramancer'/'ORI', '\"In memories, we can find our deepest reserves of strength.\"').
card_multiverse_id('auramancer'/'ORI', '398678').

card_in_set('avaricious dragon', 'ORI').
card_original_type('avaricious dragon'/'ORI', 'Creature — Dragon').
card_original_text('avaricious dragon'/'ORI', 'FlyingAt the beginning of your draw step, draw an additional card.At the beginning of your end step, discard your hand.').
card_first_print('avaricious dragon', 'ORI').
card_image_name('avaricious dragon'/'ORI', 'avaricious dragon').
card_uid('avaricious dragon'/'ORI', 'ORI:Avaricious Dragon:avaricious dragon').
card_rarity('avaricious dragon'/'ORI', 'Mythic Rare').
card_artist('avaricious dragon'/'ORI', 'Chris Rahn').
card_number('avaricious dragon'/'ORI', '131').
card_flavor_text('avaricious dragon'/'ORI', 'Waste follows want.').
card_multiverse_id('avaricious dragon'/'ORI', '398667').

card_in_set('aven battle priest', 'ORI').
card_original_type('aven battle priest'/'ORI', 'Creature — Bird Cleric').
card_original_text('aven battle priest'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)When Aven Battle Priest enters the battlefield, you gain 3 life.').
card_first_print('aven battle priest', 'ORI').
card_image_name('aven battle priest'/'ORI', 'aven battle priest').
card_uid('aven battle priest'/'ORI', 'ORI:Aven Battle Priest:aven battle priest').
card_rarity('aven battle priest'/'ORI', 'Common').
card_artist('aven battle priest'/'ORI', 'John Severin Brassell').
card_number('aven battle priest'/'ORI', '6').
card_flavor_text('aven battle priest'/'ORI', 'When the shadow of the aven falls across the battlefield, hope rises in the hearts of the soldiers.').
card_multiverse_id('aven battle priest'/'ORI', '398627').

card_in_set('battlefield forge', 'ORI').
card_original_type('battlefield forge'/'ORI', 'Land').
card_original_text('battlefield forge'/'ORI', '{T}: Add {1} to your mana pool.{T}: Add {R} or {W} to your mana pool. Battlefield Forge deals 1 damage to you.').
card_image_name('battlefield forge'/'ORI', 'battlefield forge').
card_uid('battlefield forge'/'ORI', 'ORI:Battlefield Forge:battlefield forge').
card_rarity('battlefield forge'/'ORI', 'Rare').
card_artist('battlefield forge'/'ORI', 'Darrell Riche').
card_number('battlefield forge'/'ORI', '244').
card_multiverse_id('battlefield forge'/'ORI', '398417').

card_in_set('bellows lizard', 'ORI').
card_original_type('bellows lizard'/'ORI', 'Creature — Lizard').
card_original_text('bellows lizard'/'ORI', '{1}{R}: Bellows Lizard gets +1/+0 until end of turn.').
card_image_name('bellows lizard'/'ORI', 'bellows lizard').
card_uid('bellows lizard'/'ORI', 'ORI:Bellows Lizard:bellows lizard').
card_rarity('bellows lizard'/'ORI', 'Common').
card_artist('bellows lizard'/'ORI', 'Jack Wang').
card_number('bellows lizard'/'ORI', '132').
card_flavor_text('bellows lizard'/'ORI', 'As the price of wood and coal rose, smiths found creative ways to keep their forges burning.').
card_multiverse_id('bellows lizard'/'ORI', '398439').

card_in_set('blazing hellhound', 'ORI').
card_original_type('blazing hellhound'/'ORI', 'Creature — Elemental Hound').
card_original_text('blazing hellhound'/'ORI', '{1}, Sacrifice another creature: Blazing Hellhound deals 1 damage to target creature or player.').
card_first_print('blazing hellhound', 'ORI').
card_image_name('blazing hellhound'/'ORI', 'blazing hellhound').
card_uid('blazing hellhound'/'ORI', 'ORI:Blazing Hellhound:blazing hellhound').
card_rarity('blazing hellhound'/'ORI', 'Uncommon').
card_artist('blazing hellhound'/'ORI', 'Eric Velhagen').
card_number('blazing hellhound'/'ORI', '210').
card_flavor_text('blazing hellhound'/'ORI', 'It tears the flesh from your bones and then swallows the ash with its fiery maw.').
card_multiverse_id('blazing hellhound'/'ORI', '398660').

card_in_set('blessed spirits', 'ORI').
card_original_type('blessed spirits'/'ORI', 'Creature — Spirit').
card_original_text('blessed spirits'/'ORI', 'FlyingWhenever you cast an enchantment spell, put a +1/+1 counter on Blessed Spirits.').
card_first_print('blessed spirits', 'ORI').
card_image_name('blessed spirits'/'ORI', 'blessed spirits').
card_uid('blessed spirits'/'ORI', 'ORI:Blessed Spirits:blessed spirits').
card_rarity('blessed spirits'/'ORI', 'Uncommon').
card_artist('blessed spirits'/'ORI', 'Anna Steinbauer').
card_number('blessed spirits'/'ORI', '7').
card_flavor_text('blessed spirits'/'ORI', 'Not all heroes die in armor.').
card_multiverse_id('blessed spirits'/'ORI', '398628').

card_in_set('blightcaster', 'ORI').
card_original_type('blightcaster'/'ORI', 'Creature — Human Wizard').
card_original_text('blightcaster'/'ORI', 'Whenever you cast an enchantment spell, you may have target creature get -2/-2 until end of turn.').
card_image_name('blightcaster'/'ORI', 'blightcaster').
card_uid('blightcaster'/'ORI', 'ORI:Blightcaster:blightcaster').
card_rarity('blightcaster'/'ORI', 'Uncommon').
card_artist('blightcaster'/'ORI', 'Winona Nelson').
card_number('blightcaster'/'ORI', '85').
card_flavor_text('blightcaster'/'ORI', '\"Your flesh is unprepared for my gifts.\"').
card_multiverse_id('blightcaster'/'ORI', '398618').

card_in_set('blood-cursed knight', 'ORI').
card_original_type('blood-cursed knight'/'ORI', 'Creature — Vampire Knight').
card_original_text('blood-cursed knight'/'ORI', 'As long as you control an enchantment, Blood-Cursed Knight gets +1/+1 and has lifelink. (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('blood-cursed knight', 'ORI').
card_image_name('blood-cursed knight'/'ORI', 'blood-cursed knight').
card_uid('blood-cursed knight'/'ORI', 'ORI:Blood-Cursed Knight:blood-cursed knight').
card_rarity('blood-cursed knight'/'ORI', 'Uncommon').
card_artist('blood-cursed knight'/'ORI', 'Winona Nelson').
card_number('blood-cursed knight'/'ORI', '211').
card_flavor_text('blood-cursed knight'/'ORI', '\"The bloodlust shall not control me, for my oath is my greatest compulsion.\"').
card_multiverse_id('blood-cursed knight'/'ORI', '398527').

card_in_set('boggart brute', 'ORI').
card_original_type('boggart brute'/'ORI', 'Creature — Goblin Warrior').
card_original_text('boggart brute'/'ORI', 'Menace (This creature can\'t be blocked except by two or more creatures.)').
card_first_print('boggart brute', 'ORI').
card_image_name('boggart brute'/'ORI', 'boggart brute').
card_uid('boggart brute'/'ORI', 'ORI:Boggart Brute:boggart brute').
card_rarity('boggart brute'/'ORI', 'Common').
card_artist('boggart brute'/'ORI', 'Igor Kieryluk').
card_number('boggart brute'/'ORI', '133').
card_flavor_text('boggart brute'/'ORI', 'He has the biggest bashing stick, so it\'s a safe bet he\'s the leader.').
card_multiverse_id('boggart brute'/'ORI', '398606').

card_in_set('bonded construct', 'ORI').
card_original_type('bonded construct'/'ORI', 'Artifact Creature — Construct').
card_original_text('bonded construct'/'ORI', 'Bonded Construct can\'t attack alone.').
card_first_print('bonded construct', 'ORI').
card_image_name('bonded construct'/'ORI', 'bonded construct').
card_uid('bonded construct'/'ORI', 'ORI:Bonded Construct:bonded construct').
card_rarity('bonded construct'/'ORI', 'Common').
card_artist('bonded construct'/'ORI', 'Craig J Spearing').
card_number('bonded construct'/'ORI', '223').
card_flavor_text('bonded construct'/'ORI', 'Loyal to a fault, it heeds its master\'s every command.').
card_multiverse_id('bonded construct'/'ORI', '398665').

card_in_set('bone to ash', 'ORI').
card_original_type('bone to ash'/'ORI', 'Instant').
card_original_text('bone to ash'/'ORI', 'Counter target creature spell.Draw a card.').
card_image_name('bone to ash'/'ORI', 'bone to ash').
card_uid('bone to ash'/'ORI', 'ORI:Bone to Ash:bone to ash').
card_rarity('bone to ash'/'ORI', 'Common').
card_artist('bone to ash'/'ORI', 'Clint Cearley').
card_number('bone to ash'/'ORI', '47').
card_flavor_text('bone to ash'/'ORI', '\"I can think of worse ways to go. On second thought, maybe not.\"—Ludevic, necro-alchemist').
card_multiverse_id('bone to ash'/'ORI', '398539').

card_in_set('bounding krasis', 'ORI').
card_original_type('bounding krasis'/'ORI', 'Creature — Fish Lizard').
card_original_text('bounding krasis'/'ORI', 'Flash (You may cast this spell any time you could cast an instant.)When Bounding Krasis enters the battlefield, you may tap or untap target creature.').
card_first_print('bounding krasis', 'ORI').
card_image_name('bounding krasis'/'ORI', 'bounding krasis').
card_uid('bounding krasis'/'ORI', 'ORI:Bounding Krasis:bounding krasis').
card_rarity('bounding krasis'/'ORI', 'Uncommon').
card_artist('bounding krasis'/'ORI', 'Jack Wang').
card_number('bounding krasis'/'ORI', '212').
card_flavor_text('bounding krasis'/'ORI', 'Unpredictable as a storm and destructive as a tidal wave.').
card_multiverse_id('bounding krasis'/'ORI', '398635').

card_in_set('brawler\'s plate', 'ORI').
card_original_type('brawler\'s plate'/'ORI', 'Artifact — Equipment').
card_original_text('brawler\'s plate'/'ORI', 'Equipped creature gets +2/+2 and has trample. (It can deal excess combat damage to defending player or planeswalker while attacking.)Equip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('brawler\'s plate'/'ORI', 'brawler\'s plate').
card_uid('brawler\'s plate'/'ORI', 'ORI:Brawler\'s Plate:brawler\'s plate').
card_rarity('brawler\'s plate'/'ORI', 'Uncommon').
card_artist('brawler\'s plate'/'ORI', 'Jung Park').
card_number('brawler\'s plate'/'ORI', '224').
card_multiverse_id('brawler\'s plate'/'ORI', '398532').

card_in_set('calculated dismissal', 'ORI').
card_original_type('calculated dismissal'/'ORI', 'Instant').
card_original_text('calculated dismissal'/'ORI', 'Counter target spell unless its controller pays {3}.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, scry 2. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_first_print('calculated dismissal', 'ORI').
card_image_name('calculated dismissal'/'ORI', 'calculated dismissal').
card_uid('calculated dismissal'/'ORI', 'ORI:Calculated Dismissal:calculated dismissal').
card_rarity('calculated dismissal'/'ORI', 'Common').
card_artist('calculated dismissal'/'ORI', 'Karl Kopinski').
card_number('calculated dismissal'/'ORI', '48').
card_multiverse_id('calculated dismissal'/'ORI', '398480').

card_in_set('call of the full moon', 'ORI').
card_original_type('call of the full moon'/'ORI', 'Enchantment — Aura').
card_original_text('call of the full moon'/'ORI', 'Enchant creatureEnchanted creature gets +3/+2 and has trample. (It can deal excess combat damage to defending player or planeswalker while attacking.)At the beginning of each upkeep, if a player cast two or more spells last turn, sacrifice Call of the Full Moon.').
card_first_print('call of the full moon', 'ORI').
card_image_name('call of the full moon'/'ORI', 'call of the full moon').
card_uid('call of the full moon'/'ORI', 'ORI:Call of the Full Moon:call of the full moon').
card_rarity('call of the full moon'/'ORI', 'Uncommon').
card_artist('call of the full moon'/'ORI', 'Nils Hamm').
card_number('call of the full moon'/'ORI', '134').
card_multiverse_id('call of the full moon'/'ORI', '398457').

card_in_set('catacomb slug', 'ORI').
card_original_type('catacomb slug'/'ORI', 'Creature — Slug').
card_original_text('catacomb slug'/'ORI', '').
card_image_name('catacomb slug'/'ORI', 'catacomb slug').
card_uid('catacomb slug'/'ORI', 'ORI:Catacomb Slug:catacomb slug').
card_rarity('catacomb slug'/'ORI', 'Common').
card_artist('catacomb slug'/'ORI', 'Nils Hamm').
card_number('catacomb slug'/'ORI', '86').
card_flavor_text('catacomb slug'/'ORI', '\"The entire murder scene was covered in dripping, oozing slime. No need for a soothsayer to solve that one.\"—Pel Javya, Wojek investigator').
card_multiverse_id('catacomb slug'/'ORI', '398473').

card_in_set('caustic caterpillar', 'ORI').
card_original_type('caustic caterpillar'/'ORI', 'Creature — Insect').
card_original_text('caustic caterpillar'/'ORI', '{1}{G}, Sacrifice Caustic Caterpillar: Destroy target artifact or enchantment.').
card_first_print('caustic caterpillar', 'ORI').
card_image_name('caustic caterpillar'/'ORI', 'caustic caterpillar').
card_uid('caustic caterpillar'/'ORI', 'ORI:Caustic Caterpillar:caustic caterpillar').
card_rarity('caustic caterpillar'/'ORI', 'Common').
card_artist('caustic caterpillar'/'ORI', 'Jack Wang').
card_number('caustic caterpillar'/'ORI', '170').
card_flavor_text('caustic caterpillar'/'ORI', '\"The rare and beautiful butterflies inspire the design of our thopters. The larvae, however, are a different story entirely.\"—Kiran Nalaar, Ghirapur inventor').
card_multiverse_id('caustic caterpillar'/'ORI', '398409').

card_in_set('caves of koilos', 'ORI').
card_original_type('caves of koilos'/'ORI', 'Land').
card_original_text('caves of koilos'/'ORI', '{T}: Add {1} to your mana pool.{T}: Add {W} or {B} to your mana pool. Caves of Koilos deals 1 damage to you.').
card_image_name('caves of koilos'/'ORI', 'caves of koilos').
card_uid('caves of koilos'/'ORI', 'ORI:Caves of Koilos:caves of koilos').
card_rarity('caves of koilos'/'ORI', 'Rare').
card_artist('caves of koilos'/'ORI', 'Jim Nelson').
card_number('caves of koilos'/'ORI', '245').
card_multiverse_id('caves of koilos'/'ORI', '398504').

card_in_set('celestial flare', 'ORI').
card_original_type('celestial flare'/'ORI', 'Instant').
card_original_text('celestial flare'/'ORI', 'Target player sacrifices an attacking or blocking creature.').
card_image_name('celestial flare'/'ORI', 'celestial flare').
card_uid('celestial flare'/'ORI', 'ORI:Celestial Flare:celestial flare').
card_rarity('celestial flare'/'ORI', 'Common').
card_artist('celestial flare'/'ORI', 'Clint Cearley').
card_number('celestial flare'/'ORI', '8').
card_flavor_text('celestial flare'/'ORI', '\"You were defeated the moment you declared your aggression.\"—Gideon Jura').
card_multiverse_id('celestial flare'/'ORI', '398488').

card_in_set('chandra\'s fury', 'ORI').
card_original_type('chandra\'s fury'/'ORI', 'Instant').
card_original_text('chandra\'s fury'/'ORI', 'Chandra\'s Fury deals 4 damage to target player and 1 damage to each creature that player controls.').
card_image_name('chandra\'s fury'/'ORI', 'chandra\'s fury').
card_uid('chandra\'s fury'/'ORI', 'ORI:Chandra\'s Fury:chandra\'s fury').
card_rarity('chandra\'s fury'/'ORI', 'Common').
card_artist('chandra\'s fury'/'ORI', 'Volkan Baga').
card_number('chandra\'s fury'/'ORI', '136').
card_flavor_text('chandra\'s fury'/'ORI', '\"They started it.\"').
card_multiverse_id('chandra\'s fury'/'ORI', '398632').

card_in_set('chandra\'s ignition', 'ORI').
card_original_type('chandra\'s ignition'/'ORI', 'Sorcery').
card_original_text('chandra\'s ignition'/'ORI', 'Target creature you control deals damage equal to its power to each other creature and each opponent.').
card_first_print('chandra\'s ignition', 'ORI').
card_image_name('chandra\'s ignition'/'ORI', 'chandra\'s ignition').
card_uid('chandra\'s ignition'/'ORI', 'ORI:Chandra\'s Ignition:chandra\'s ignition').
card_rarity('chandra\'s ignition'/'ORI', 'Rare').
card_artist('chandra\'s ignition'/'ORI', 'Eric Deschamps').
card_number('chandra\'s ignition'/'ORI', '137').
card_flavor_text('chandra\'s ignition'/'ORI', 'In the moment before her execution, she realized what it meant to be a pyromancer, to be alive, to be Chandra.').
card_multiverse_id('chandra\'s ignition'/'ORI', '398416').

card_in_set('chandra, fire of kaladesh', 'ORI').
card_original_type('chandra, fire of kaladesh'/'ORI', 'Legendary Creature — Human Shaman').
card_original_text('chandra, fire of kaladesh'/'ORI', 'Whenever you cast a red spell, untap Chandra, Fire of Kaladesh.{T}: Chandra, Fire of Kaladesh deals 1 damage to target player. If Chandra has dealt 3 or more damage this turn, exile her, then return her to the battlefield transformed under her owner\'s control.').
card_first_print('chandra, fire of kaladesh', 'ORI').
card_image_name('chandra, fire of kaladesh'/'ORI', 'chandra, fire of kaladesh').
card_uid('chandra, fire of kaladesh'/'ORI', 'ORI:Chandra, Fire of Kaladesh:chandra, fire of kaladesh').
card_rarity('chandra, fire of kaladesh'/'ORI', 'Mythic Rare').
card_artist('chandra, fire of kaladesh'/'ORI', 'Eric Deschamps').
card_number('chandra, fire of kaladesh'/'ORI', '135a').
card_multiverse_id('chandra, fire of kaladesh'/'ORI', '398422').

card_in_set('chandra, roaring flame', 'ORI').
card_original_type('chandra, roaring flame'/'ORI', 'Planeswalker — Chandra').
card_original_text('chandra, roaring flame'/'ORI', '+1: Chandra, Roaring Flame deals 2 damage to target player.−2: Chandra, Roaring Flame deals 2 damage to target creature.−7: Chandra, Roaring Flame deals 6 damage to each opponent. Each player dealt damage this way gets an emblem with \"At the beginning of your upkeep, this emblem deals 3 damage to you.\"').
card_first_print('chandra, roaring flame', 'ORI').
card_image_name('chandra, roaring flame'/'ORI', 'chandra, roaring flame').
card_uid('chandra, roaring flame'/'ORI', 'ORI:Chandra, Roaring Flame:chandra, roaring flame').
card_rarity('chandra, roaring flame'/'ORI', 'Mythic Rare').
card_artist('chandra, roaring flame'/'ORI', 'Eric Deschamps').
card_number('chandra, roaring flame'/'ORI', '135b').
card_multiverse_id('chandra, roaring flame'/'ORI', '398423').

card_in_set('charging griffin', 'ORI').
card_original_type('charging griffin'/'ORI', 'Creature — Griffin').
card_original_text('charging griffin'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)Whenever Charging Griffin attacks, it gets +1/+1 until end of turn.').
card_image_name('charging griffin'/'ORI', 'charging griffin').
card_uid('charging griffin'/'ORI', 'ORI:Charging Griffin:charging griffin').
card_rarity('charging griffin'/'ORI', 'Common').
card_artist('charging griffin'/'ORI', 'Erica Yang').
card_number('charging griffin'/'ORI', '9').
card_flavor_text('charging griffin'/'ORI', 'Four claws, two wings, one beak, no fear.').
card_multiverse_id('charging griffin'/'ORI', '398560').

card_in_set('chief of the foundry', 'ORI').
card_original_type('chief of the foundry'/'ORI', 'Artifact Creature — Construct').
card_original_text('chief of the foundry'/'ORI', 'Other artifact creatures you control get +1/+1.').
card_first_print('chief of the foundry', 'ORI').
card_image_name('chief of the foundry'/'ORI', 'chief of the foundry').
card_uid('chief of the foundry'/'ORI', 'ORI:Chief of the Foundry:chief of the foundry').
card_rarity('chief of the foundry'/'ORI', 'Uncommon').
card_artist('chief of the foundry'/'ORI', 'Daniel Ljunggren').
card_number('chief of the foundry'/'ORI', '225').
card_flavor_text('chief of the foundry'/'ORI', 'The foundries of Kaladesh run like clockwork under the supervision of their formidable overseers.').
card_multiverse_id('chief of the foundry'/'ORI', '398581').

card_in_set('citadel castellan', 'ORI').
card_original_type('citadel castellan'/'ORI', 'Creature — Human Knight').
card_original_text('citadel castellan'/'ORI', 'Vigilance (Attacking doesn\'t cause this creature to tap.)Renown 2 (When this creature deals combat damage to a player, if it isn\'t renowned, put two +1/+1 counters on it and it becomes renowned.)').
card_first_print('citadel castellan', 'ORI').
card_image_name('citadel castellan'/'ORI', 'citadel castellan').
card_uid('citadel castellan'/'ORI', 'ORI:Citadel Castellan:citadel castellan').
card_rarity('citadel castellan'/'ORI', 'Uncommon').
card_artist('citadel castellan'/'ORI', 'Anastasia Ovchinnikova').
card_number('citadel castellan'/'ORI', '213').
card_flavor_text('citadel castellan'/'ORI', '\"I am the first line of defense. You will not encounter the second.\"').
card_multiverse_id('citadel castellan'/'ORI', '398672').

card_in_set('clash of wills', 'ORI').
card_original_type('clash of wills'/'ORI', 'Instant').
card_original_text('clash of wills'/'ORI', 'Counter target spell unless its controller pays {X}.').
card_first_print('clash of wills', 'ORI').
card_image_name('clash of wills'/'ORI', 'clash of wills').
card_uid('clash of wills'/'ORI', 'ORI:Clash of Wills:clash of wills').
card_rarity('clash of wills'/'ORI', 'Uncommon').
card_artist('clash of wills'/'ORI', 'Yan Li').
card_number('clash of wills'/'ORI', '49').
card_flavor_text('clash of wills'/'ORI', 'Alhammarret rose to his feet, and the full force of his mind hit Jace like a storm front.').
card_multiverse_id('clash of wills'/'ORI', '398542').

card_in_set('claustrophobia', 'ORI').
card_original_type('claustrophobia'/'ORI', 'Enchantment — Aura').
card_original_text('claustrophobia'/'ORI', 'Enchant creatureWhen Claustrophobia enters the battlefield, tap enchanted creature.Enchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('claustrophobia'/'ORI', 'claustrophobia').
card_uid('claustrophobia'/'ORI', 'ORI:Claustrophobia:claustrophobia').
card_rarity('claustrophobia'/'ORI', 'Common').
card_artist('claustrophobia'/'ORI', 'Ryan Pancoast').
card_number('claustrophobia'/'ORI', '50').
card_flavor_text('claustrophobia'/'ORI', 'Six feet of earth muffled his cries.').
card_multiverse_id('claustrophobia'/'ORI', '398607').

card_in_set('cleric of the forward order', 'ORI').
card_original_type('cleric of the forward order'/'ORI', 'Creature — Human Cleric').
card_original_text('cleric of the forward order'/'ORI', 'When Cleric of the Forward Order enters the battlefield, you gain 2 life for each creature you control named Cleric of the Forward Order.').
card_first_print('cleric of the forward order', 'ORI').
card_image_name('cleric of the forward order'/'ORI', 'cleric of the forward order').
card_uid('cleric of the forward order'/'ORI', 'ORI:Cleric of the Forward Order:cleric of the forward order').
card_rarity('cleric of the forward order'/'ORI', 'Common').
card_artist('cleric of the forward order'/'ORI', 'Chris Rallis').
card_number('cleric of the forward order'/'ORI', '10').
card_flavor_text('cleric of the forward order'/'ORI', 'War after war, a healer\'s enemy is always the same—death itself.').
card_multiverse_id('cleric of the forward order'/'ORI', '398451').

card_in_set('cobblebrute', 'ORI').
card_original_type('cobblebrute'/'ORI', 'Creature — Elemental').
card_original_text('cobblebrute'/'ORI', '').
card_image_name('cobblebrute'/'ORI', 'cobblebrute').
card_uid('cobblebrute'/'ORI', 'ORI:Cobblebrute:cobblebrute').
card_rarity('cobblebrute'/'ORI', 'Common').
card_artist('cobblebrute'/'ORI', 'Eytan Zana').
card_number('cobblebrute'/'ORI', '138').
card_flavor_text('cobblebrute'/'ORI', 'Ravnica\'s most ancient streets take on a life of their own. A few have decided to move to nicer neighborhoods.').
card_multiverse_id('cobblebrute'/'ORI', '398616').

card_in_set('conclave naturalists', 'ORI').
card_original_type('conclave naturalists'/'ORI', 'Creature — Dryad').
card_original_text('conclave naturalists'/'ORI', 'When Conclave Naturalists enters the battlefield, you may destroy target artifact or enchantment.').
card_first_print('conclave naturalists', 'ORI').
card_image_name('conclave naturalists'/'ORI', 'conclave naturalists').
card_uid('conclave naturalists'/'ORI', 'ORI:Conclave Naturalists:conclave naturalists').
card_rarity('conclave naturalists'/'ORI', 'Uncommon').
card_artist('conclave naturalists'/'ORI', 'Howard Lyon').
card_number('conclave naturalists'/'ORI', '171').
card_flavor_text('conclave naturalists'/'ORI', '\"Your swords and wards have no power here.\"').
card_multiverse_id('conclave naturalists'/'ORI', '398419').

card_in_set('consecrated by blood', 'ORI').
card_original_type('consecrated by blood'/'ORI', 'Enchantment — Aura').
card_original_text('consecrated by blood'/'ORI', 'Enchant creatureEnchanted creature gets +2/+2 and has flying and \"Sacrifice two other creatures: Regenerate this creature.\" (The next time the creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('consecrated by blood', 'ORI').
card_image_name('consecrated by blood'/'ORI', 'consecrated by blood').
card_uid('consecrated by blood'/'ORI', 'ORI:Consecrated by Blood:consecrated by blood').
card_rarity('consecrated by blood'/'ORI', 'Uncommon').
card_artist('consecrated by blood'/'ORI', 'John Stanko').
card_number('consecrated by blood'/'ORI', '87').
card_multiverse_id('consecrated by blood'/'ORI', '398512').

card_in_set('consul\'s lieutenant', 'ORI').
card_original_type('consul\'s lieutenant'/'ORI', 'Creature — Human Soldier').
card_original_text('consul\'s lieutenant'/'ORI', 'First strikeRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)Whenever Consul\'s Lieutenant attacks, if it\'s renowned, other attacking creatures you control get +1/+1 until end of turn.').
card_first_print('consul\'s lieutenant', 'ORI').
card_image_name('consul\'s lieutenant'/'ORI', 'consul\'s lieutenant').
card_uid('consul\'s lieutenant'/'ORI', 'ORI:Consul\'s Lieutenant:consul\'s lieutenant').
card_rarity('consul\'s lieutenant'/'ORI', 'Uncommon').
card_artist('consul\'s lieutenant'/'ORI', 'Daarken').
card_number('consul\'s lieutenant'/'ORI', '11').
card_multiverse_id('consul\'s lieutenant'/'ORI', '398446').

card_in_set('cruel revival', 'ORI').
card_original_type('cruel revival'/'ORI', 'Instant').
card_original_text('cruel revival'/'ORI', 'Destroy target non-Zombie creature. It can\'t be regenerated. Return up to one target Zombie card from your graveyard to your hand.').
card_image_name('cruel revival'/'ORI', 'cruel revival').
card_uid('cruel revival'/'ORI', 'ORI:Cruel Revival:cruel revival').
card_rarity('cruel revival'/'ORI', 'Uncommon').
card_artist('cruel revival'/'ORI', 'Miles Johnston').
card_number('cruel revival'/'ORI', '88').
card_flavor_text('cruel revival'/'ORI', 'On Innistrad, Liliana embraced necromancy, honing her powers by practicing on scores of undead—and sometimes the living.').
card_multiverse_id('cruel revival'/'ORI', '398415').

card_in_set('dark dabbling', 'ORI').
card_original_type('dark dabbling'/'ORI', 'Instant').
card_original_text('dark dabbling'/'ORI', 'Regenerate target creature. Draw a card. (The next time the creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, also regenerate each other creature you control.').
card_first_print('dark dabbling', 'ORI').
card_image_name('dark dabbling'/'ORI', 'dark dabbling').
card_uid('dark dabbling'/'ORI', 'ORI:Dark Dabbling:dark dabbling').
card_rarity('dark dabbling'/'ORI', 'Common').
card_artist('dark dabbling'/'ORI', 'Bastien L. Deharme').
card_number('dark dabbling'/'ORI', '89').
card_multiverse_id('dark dabbling'/'ORI', '398466').

card_in_set('dark petition', 'ORI').
card_original_type('dark petition'/'ORI', 'Sorcery').
card_original_text('dark petition'/'ORI', 'Search your library for a card and put that card into your hand. Then shuffle your library.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, add {B}{B}{B} to your mana pool.').
card_first_print('dark petition', 'ORI').
card_image_name('dark petition'/'ORI', 'dark petition').
card_uid('dark petition'/'ORI', 'ORI:Dark Petition:dark petition').
card_rarity('dark petition'/'ORI', 'Rare').
card_artist('dark petition'/'ORI', 'Igor Kieryluk').
card_number('dark petition'/'ORI', '90').
card_multiverse_id('dark petition'/'ORI', '398525').

card_in_set('day\'s undoing', 'ORI').
card_original_type('day\'s undoing'/'ORI', 'Sorcery').
card_original_text('day\'s undoing'/'ORI', 'Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. If it\'s your turn, end the turn. (Exile all spells and abilities on the stack, including this card. Discard down to your maximum hand size. Damage wears off, and \"this turn\" and \"until end of turn\" effects end.)').
card_first_print('day\'s undoing', 'ORI').
card_image_name('day\'s undoing'/'ORI', 'day\'s undoing').
card_uid('day\'s undoing'/'ORI', 'ORI:Day\'s Undoing:day\'s undoing').
card_rarity('day\'s undoing'/'ORI', 'Mythic Rare').
card_artist('day\'s undoing'/'ORI', 'Jonas De Ro').
card_number('day\'s undoing'/'ORI', '51').
card_multiverse_id('day\'s undoing'/'ORI', '398652').

card_in_set('deadbridge shaman', 'ORI').
card_original_type('deadbridge shaman'/'ORI', 'Creature — Elf Shaman').
card_original_text('deadbridge shaman'/'ORI', 'When Deadbridge Shaman dies, target opponent discards a card.').
card_first_print('deadbridge shaman', 'ORI').
card_image_name('deadbridge shaman'/'ORI', 'deadbridge shaman').
card_uid('deadbridge shaman'/'ORI', 'ORI:Deadbridge Shaman:deadbridge shaman').
card_rarity('deadbridge shaman'/'ORI', 'Common').
card_artist('deadbridge shaman'/'ORI', 'Nils Hamm').
card_number('deadbridge shaman'/'ORI', '91').
card_flavor_text('deadbridge shaman'/'ORI', 'Ending her life won\'t end the infestation.').
card_multiverse_id('deadbridge shaman'/'ORI', '398500').

card_in_set('deep-sea terror', 'ORI').
card_original_type('deep-sea terror'/'ORI', 'Creature — Serpent').
card_original_text('deep-sea terror'/'ORI', 'Deep-Sea Terror can\'t attack unless there are seven or more cards in your graveyard.').
card_first_print('deep-sea terror', 'ORI').
card_image_name('deep-sea terror'/'ORI', 'deep-sea terror').
card_uid('deep-sea terror'/'ORI', 'ORI:Deep-Sea Terror:deep-sea terror').
card_rarity('deep-sea terror'/'ORI', 'Common').
card_artist('deep-sea terror'/'ORI', 'Marco Nelor').
card_number('deep-sea terror'/'ORI', '52').
card_flavor_text('deep-sea terror'/'ORI', 'After stripping the sunken ships, it rises to the surface for another helping.').
card_multiverse_id('deep-sea terror'/'ORI', '398605').

card_in_set('demolish', 'ORI').
card_original_type('demolish'/'ORI', 'Sorcery').
card_original_text('demolish'/'ORI', 'Destroy target artifact or land.').
card_image_name('demolish'/'ORI', 'demolish').
card_uid('demolish'/'ORI', 'ORI:Demolish:demolish').
card_rarity('demolish'/'ORI', 'Common').
card_artist('demolish'/'ORI', 'John Avon').
card_number('demolish'/'ORI', '139').
card_flavor_text('demolish'/'ORI', 'Nothing of nature nor of mortals can stand forever.').
card_multiverse_id('demolish'/'ORI', '398486').

card_in_set('demonic pact', 'ORI').
card_original_type('demonic pact'/'ORI', 'Enchantment').
card_original_text('demonic pact'/'ORI', 'At the beginning of your upkeep, choose one that hasn\'t been chosen —• Demonic Pact deals 4 damage to target creature or player and you gain 4 life.• Target opponent discards two cards.• Draw two cards.• You lose the game.').
card_first_print('demonic pact', 'ORI').
card_image_name('demonic pact'/'ORI', 'demonic pact').
card_uid('demonic pact'/'ORI', 'ORI:Demonic Pact:demonic pact').
card_rarity('demonic pact'/'ORI', 'Mythic Rare').
card_artist('demonic pact'/'ORI', 'Aleksi Briclot').
card_number('demonic pact'/'ORI', '92').
card_multiverse_id('demonic pact'/'ORI', '398433').

card_in_set('despoiler of souls', 'ORI').
card_original_type('despoiler of souls'/'ORI', 'Creature — Horror').
card_original_text('despoiler of souls'/'ORI', 'Despoiler of Souls can\'t block.{B}{B}, Exile two other creature cards from your graveyard: Return Despoiler of Souls from your graveyard to the battlefield.').
card_first_print('despoiler of souls', 'ORI').
card_image_name('despoiler of souls'/'ORI', 'despoiler of souls').
card_uid('despoiler of souls'/'ORI', 'ORI:Despoiler of Souls:despoiler of souls').
card_rarity('despoiler of souls'/'ORI', 'Rare').
card_artist('despoiler of souls'/'ORI', 'Greg Staples').
card_number('despoiler of souls'/'ORI', '93').
card_flavor_text('despoiler of souls'/'ORI', 'Rats and vultures devour. It desecrates.').
card_multiverse_id('despoiler of souls'/'ORI', '398587').

card_in_set('disciple of the ring', 'ORI').
card_original_type('disciple of the ring'/'ORI', 'Creature — Human Wizard').
card_original_text('disciple of the ring'/'ORI', '{1}, Exile an instant or sorcery card from your graveyard: Choose one —• Counter target noncreature spell unless its controller pays {2}.• Disciple of the Ring gets +1/+1 until end of turn.• Tap target creature.• Untap target creature.').
card_first_print('disciple of the ring', 'ORI').
card_image_name('disciple of the ring'/'ORI', 'disciple of the ring').
card_uid('disciple of the ring'/'ORI', 'ORI:Disciple of the Ring:disciple of the ring').
card_rarity('disciple of the ring'/'ORI', 'Mythic Rare').
card_artist('disciple of the ring'/'ORI', 'Clint Cearley').
card_number('disciple of the ring'/'ORI', '53').
card_multiverse_id('disciple of the ring'/'ORI', '398583').

card_in_set('disperse', 'ORI').
card_original_type('disperse'/'ORI', 'Instant').
card_original_text('disperse'/'ORI', 'Return target nonland permanent to its owner\'s hand.').
card_image_name('disperse'/'ORI', 'disperse').
card_uid('disperse'/'ORI', 'ORI:Disperse:disperse').
card_rarity('disperse'/'ORI', 'Common').
card_artist('disperse'/'ORI', 'Ryan Yee').
card_number('disperse'/'ORI', '54').
card_flavor_text('disperse'/'ORI', 'It\'s pointless to hold on when you have nothing to hold on with.').
card_multiverse_id('disperse'/'ORI', '398528').

card_in_set('displacement wave', 'ORI').
card_original_type('displacement wave'/'ORI', 'Sorcery').
card_original_text('displacement wave'/'ORI', 'Return all nonland permanents with converted mana cost X or less to their owners\' hands.').
card_first_print('displacement wave', 'ORI').
card_image_name('displacement wave'/'ORI', 'displacement wave').
card_uid('displacement wave'/'ORI', 'ORI:Displacement Wave:displacement wave').
card_rarity('displacement wave'/'ORI', 'Rare').
card_artist('displacement wave'/'ORI', 'Seb McKinnon').
card_number('displacement wave'/'ORI', '55').
card_flavor_text('displacement wave'/'ORI', 'The sea respects no boundaries, least of all the coast.').
card_multiverse_id('displacement wave'/'ORI', '398580').

card_in_set('divine verdict', 'ORI').
card_original_type('divine verdict'/'ORI', 'Instant').
card_original_text('divine verdict'/'ORI', 'Destroy target attacking or blocking creature.').
card_image_name('divine verdict'/'ORI', 'divine verdict').
card_uid('divine verdict'/'ORI', 'ORI:Divine Verdict:divine verdict').
card_rarity('divine verdict'/'ORI', 'Common').
card_artist('divine verdict'/'ORI', 'Kev Walker').
card_number('divine verdict'/'ORI', '274').
card_flavor_text('divine verdict'/'ORI', '\"Guilty.\"').
card_multiverse_id('divine verdict'/'ORI', '401453').

card_in_set('dragon fodder', 'ORI').
card_original_type('dragon fodder'/'ORI', 'Sorcery').
card_original_text('dragon fodder'/'ORI', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('dragon fodder'/'ORI', 'dragon fodder').
card_uid('dragon fodder'/'ORI', 'ORI:Dragon Fodder:dragon fodder').
card_rarity('dragon fodder'/'ORI', 'Common').
card_artist('dragon fodder'/'ORI', 'Jaime Jones').
card_number('dragon fodder'/'ORI', '140').
card_flavor_text('dragon fodder'/'ORI', 'Goblins journey to the sacrificial peaks in pairs so that the rare survivor might be able to relate the details of the other\'s grisly demise.').
card_multiverse_id('dragon fodder'/'ORI', '398647').

card_in_set('dreadwaters', 'ORI').
card_original_type('dreadwaters'/'ORI', 'Sorcery').
card_original_text('dreadwaters'/'ORI', 'Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of lands you control.').
card_image_name('dreadwaters'/'ORI', 'dreadwaters').
card_uid('dreadwaters'/'ORI', 'ORI:Dreadwaters:dreadwaters').
card_rarity('dreadwaters'/'ORI', 'Common').
card_artist('dreadwaters'/'ORI', 'Cliff Childs').
card_number('dreadwaters'/'ORI', '56').
card_flavor_text('dreadwaters'/'ORI', 'Stray into the Morkrut and it may rise to claim you.').
card_multiverse_id('dreadwaters'/'ORI', '398405').

card_in_set('dwynen\'s elite', 'ORI').
card_original_type('dwynen\'s elite'/'ORI', 'Creature — Elf Warrior').
card_original_text('dwynen\'s elite'/'ORI', 'When Dwynen\'s Elite enters the battlefield, if you control another Elf, put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_first_print('dwynen\'s elite', 'ORI').
card_image_name('dwynen\'s elite'/'ORI', 'dwynen\'s elite').
card_uid('dwynen\'s elite'/'ORI', 'ORI:Dwynen\'s Elite:dwynen\'s elite').
card_rarity('dwynen\'s elite'/'ORI', 'Uncommon').
card_artist('dwynen\'s elite'/'ORI', 'Lius Lasahido').
card_number('dwynen\'s elite'/'ORI', '173').
card_flavor_text('dwynen\'s elite'/'ORI', '\"Come, my beautiful brothers and sisters. Let us hunt.\"—Dwynen, Gilt-Leaf daen').
card_multiverse_id('dwynen\'s elite'/'ORI', '398609').

card_in_set('dwynen, gilt-leaf daen', 'ORI').
card_original_type('dwynen, gilt-leaf daen'/'ORI', 'Legendary Creature — Elf Warrior').
card_original_text('dwynen, gilt-leaf daen'/'ORI', 'ReachOther Elf creatures you control get +1/+1.Whenever Dwynen, Gilt-Leaf Daen attacks, you gain 1 life for each attacking Elf you control.').
card_first_print('dwynen, gilt-leaf daen', 'ORI').
card_image_name('dwynen, gilt-leaf daen'/'ORI', 'dwynen, gilt-leaf daen').
card_uid('dwynen, gilt-leaf daen'/'ORI', 'ORI:Dwynen, Gilt-Leaf Daen:dwynen, gilt-leaf daen').
card_rarity('dwynen, gilt-leaf daen'/'ORI', 'Rare').
card_artist('dwynen, gilt-leaf daen'/'ORI', 'Johannes Voss').
card_number('dwynen, gilt-leaf daen'/'ORI', '172').
card_flavor_text('dwynen, gilt-leaf daen'/'ORI', '\"This is our land. We will not allow eyeblights to poison its beauty.\"').
card_multiverse_id('dwynen, gilt-leaf daen'/'ORI', '398546').

card_in_set('eagle of the watch', 'ORI').
card_original_type('eagle of the watch'/'ORI', 'Creature — Bird').
card_original_text('eagle of the watch'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('eagle of the watch'/'ORI', 'eagle of the watch').
card_uid('eagle of the watch'/'ORI', 'ORI:Eagle of the Watch:eagle of the watch').
card_rarity('eagle of the watch'/'ORI', 'Common').
card_artist('eagle of the watch'/'ORI', 'Scott Murphy').
card_number('eagle of the watch'/'ORI', '275').
card_flavor_text('eagle of the watch'/'ORI', '\"Even from miles away, I could see our eagles circling. That\'s when I knew we were needed at home.\"\n—Kanlos, Akroan captain').
card_multiverse_id('eagle of the watch'/'ORI', '401454').

card_in_set('elemental bond', 'ORI').
card_original_type('elemental bond'/'ORI', 'Enchantment').
card_original_text('elemental bond'/'ORI', 'Whenever a creature with power 3 or greater enters the battlefield under your control, draw a card.').
card_first_print('elemental bond', 'ORI').
card_image_name('elemental bond'/'ORI', 'elemental bond').
card_uid('elemental bond'/'ORI', 'ORI:Elemental Bond:elemental bond').
card_rarity('elemental bond'/'ORI', 'Uncommon').
card_artist('elemental bond'/'ORI', 'David Gaillet').
card_number('elemental bond'/'ORI', '174').
card_flavor_text('elemental bond'/'ORI', '\"I want to help Zendikar. Show me the way.\"—Nissa Revane').
card_multiverse_id('elemental bond'/'ORI', '398406').

card_in_set('elvish visionary', 'ORI').
card_original_type('elvish visionary'/'ORI', 'Creature — Elf Shaman').
card_original_text('elvish visionary'/'ORI', 'When Elvish Visionary enters the battlefield, draw a card.').
card_image_name('elvish visionary'/'ORI', 'elvish visionary').
card_uid('elvish visionary'/'ORI', 'ORI:Elvish Visionary:elvish visionary').
card_rarity('elvish visionary'/'ORI', 'Common').
card_artist('elvish visionary'/'ORI', 'D. Alexander Gregory').
card_number('elvish visionary'/'ORI', '175').
card_flavor_text('elvish visionary'/'ORI', '\"From a tiny sprout, the greatest trees grow and flourish. May the seeds of your mind be equally fruitful.\"').
card_multiverse_id('elvish visionary'/'ORI', '398554').

card_in_set('embermaw hellion', 'ORI').
card_original_type('embermaw hellion'/'ORI', 'Creature — Hellion').
card_original_text('embermaw hellion'/'ORI', 'Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)If another red source you control would deal damage to a permanent or player, it deals that much damage plus 1 to that permanent or player instead.').
card_first_print('embermaw hellion', 'ORI').
card_image_name('embermaw hellion'/'ORI', 'embermaw hellion').
card_uid('embermaw hellion'/'ORI', 'ORI:Embermaw Hellion:embermaw hellion').
card_rarity('embermaw hellion'/'ORI', 'Rare').
card_artist('embermaw hellion'/'ORI', 'James Paick').
card_number('embermaw hellion'/'ORI', '141').
card_multiverse_id('embermaw hellion'/'ORI', '398576').

card_in_set('enlightened ascetic', 'ORI').
card_original_type('enlightened ascetic'/'ORI', 'Creature — Cat Monk').
card_original_text('enlightened ascetic'/'ORI', 'When Enlightened Ascetic enters the battlefield, you may destroy target enchantment.').
card_first_print('enlightened ascetic', 'ORI').
card_image_name('enlightened ascetic'/'ORI', 'enlightened ascetic').
card_uid('enlightened ascetic'/'ORI', 'ORI:Enlightened Ascetic:enlightened ascetic').
card_rarity('enlightened ascetic'/'ORI', 'Common').
card_artist('enlightened ascetic'/'ORI', 'James Zapata').
card_number('enlightened ascetic'/'ORI', '12').
card_flavor_text('enlightened ascetic'/'ORI', '\"I do not reject the gods. I reject their authority, their pettiness, and their arrogance.\"').
card_multiverse_id('enlightened ascetic'/'ORI', '398414').

card_in_set('enshrouding mist', 'ORI').
card_original_type('enshrouding mist'/'ORI', 'Instant').
card_original_text('enshrouding mist'/'ORI', 'Target creature gets +1/+1 until end of turn. Prevent all damage that would be dealt to it this turn. If it\'s renowned, untap it.').
card_first_print('enshrouding mist', 'ORI').
card_image_name('enshrouding mist'/'ORI', 'enshrouding mist').
card_uid('enshrouding mist'/'ORI', 'ORI:Enshrouding Mist:enshrouding mist').
card_rarity('enshrouding mist'/'ORI', 'Common').
card_artist('enshrouding mist'/'ORI', 'Howard Lyon').
card_number('enshrouding mist'/'ORI', '13').
card_flavor_text('enshrouding mist'/'ORI', 'True faith is letting the blade fall without parry. True cunning is using that moment to strike a deadly blow.').
card_multiverse_id('enshrouding mist'/'ORI', '398654').

card_in_set('enthralling victor', 'ORI').
card_original_type('enthralling victor'/'ORI', 'Creature — Human Warrior').
card_original_text('enthralling victor'/'ORI', 'When Enthralling Victor enters the battlefield, gain control of target creature an opponent controls with power 2 or less until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_first_print('enthralling victor', 'ORI').
card_image_name('enthralling victor'/'ORI', 'enthralling victor').
card_uid('enthralling victor'/'ORI', 'ORI:Enthralling Victor:enthralling victor').
card_rarity('enthralling victor'/'ORI', 'Uncommon').
card_artist('enthralling victor'/'ORI', 'Winona Nelson').
card_number('enthralling victor'/'ORI', '142').
card_flavor_text('enthralling victor'/'ORI', 'If you stare at the sun for too long, you\'ll lose your ability to see clearly.').
card_multiverse_id('enthralling victor'/'ORI', '398493').

card_in_set('erebos\'s titan', 'ORI').
card_original_type('erebos\'s titan'/'ORI', 'Creature — Giant').
card_original_text('erebos\'s titan'/'ORI', 'As long as your opponents control no creatures, Erebos\'s Titan has indestructible. (Damage and effects that say \"destroy\" don\'t destroy it.)Whenever a creature card leaves an opponent\'s graveyard, you may discard a card. If you do, return Erebos\'s Titan from your graveyard to your hand.').
card_first_print('erebos\'s titan', 'ORI').
card_image_name('erebos\'s titan'/'ORI', 'erebos\'s titan').
card_uid('erebos\'s titan'/'ORI', 'ORI:Erebos\'s Titan:erebos\'s titan').
card_rarity('erebos\'s titan'/'ORI', 'Mythic Rare').
card_artist('erebos\'s titan'/'ORI', 'Peter Mohrbacher').
card_number('erebos\'s titan'/'ORI', '94').
card_multiverse_id('erebos\'s titan'/'ORI', '398584').

card_in_set('evolutionary leap', 'ORI').
card_original_type('evolutionary leap'/'ORI', 'Enchantment').
card_original_text('evolutionary leap'/'ORI', '{G}, Sacrifice a creature: Reveal cards from the top of your library until you reveal a creature card. Put that card into your hand and the rest on the bottom of your library in a random order.').
card_first_print('evolutionary leap', 'ORI').
card_image_name('evolutionary leap'/'ORI', 'evolutionary leap').
card_uid('evolutionary leap'/'ORI', 'ORI:Evolutionary Leap:evolutionary leap').
card_rarity('evolutionary leap'/'ORI', 'Rare').
card_artist('evolutionary leap'/'ORI', 'Chris Rahn').
card_number('evolutionary leap'/'ORI', '176').
card_flavor_text('evolutionary leap'/'ORI', 'The essence of nature is change.').
card_multiverse_id('evolutionary leap'/'ORI', '398573').

card_in_set('evolving wilds', 'ORI').
card_original_type('evolving wilds'/'ORI', 'Land').
card_original_text('evolving wilds'/'ORI', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'ORI', 'evolving wilds').
card_uid('evolving wilds'/'ORI', 'ORI:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'ORI', 'Common').
card_artist('evolving wilds'/'ORI', 'Steven Belledin').
card_number('evolving wilds'/'ORI', '246').
card_flavor_text('evolving wilds'/'ORI', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'ORI', '398548').

card_in_set('exquisite firecraft', 'ORI').
card_original_type('exquisite firecraft'/'ORI', 'Sorcery').
card_original_text('exquisite firecraft'/'ORI', 'Exquisite Firecraft deals 4 damage to target creature or player.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, Exquisite Firecraft can\'t be countered by spells or abilities.').
card_first_print('exquisite firecraft', 'ORI').
card_image_name('exquisite firecraft'/'ORI', 'exquisite firecraft').
card_uid('exquisite firecraft'/'ORI', 'ORI:Exquisite Firecraft:exquisite firecraft').
card_rarity('exquisite firecraft'/'ORI', 'Rare').
card_artist('exquisite firecraft'/'ORI', 'Chase Stone').
card_number('exquisite firecraft'/'ORI', '143').
card_flavor_text('exquisite firecraft'/'ORI', 'Chandra\'s gift set her apart and others aflame.').
card_multiverse_id('exquisite firecraft'/'ORI', '398513').

card_in_set('eyeblight assassin', 'ORI').
card_original_type('eyeblight assassin'/'ORI', 'Creature — Elf Assassin').
card_original_text('eyeblight assassin'/'ORI', 'When Eyeblight Assassin enters the battlefield, target creature an opponent controls gets -1/-1 until end of turn.').
card_first_print('eyeblight assassin', 'ORI').
card_image_name('eyeblight assassin'/'ORI', 'eyeblight assassin').
card_uid('eyeblight assassin'/'ORI', 'ORI:Eyeblight Assassin:eyeblight assassin').
card_rarity('eyeblight assassin'/'ORI', 'Common').
card_artist('eyeblight assassin'/'ORI', 'Dan Scott').
card_number('eyeblight assassin'/'ORI', '95').
card_flavor_text('eyeblight assassin'/'ORI', '\"Boggart blood disgusts me almost as much as the foul creatures themselves.\"').
card_multiverse_id('eyeblight assassin'/'ORI', '398535').

card_in_set('eyeblight massacre', 'ORI').
card_original_type('eyeblight massacre'/'ORI', 'Sorcery').
card_original_text('eyeblight massacre'/'ORI', 'Non-Elf creatures get -2/-2 until end of turn.').
card_first_print('eyeblight massacre', 'ORI').
card_image_name('eyeblight massacre'/'ORI', 'eyeblight massacre').
card_uid('eyeblight massacre'/'ORI', 'ORI:Eyeblight Massacre:eyeblight massacre').
card_rarity('eyeblight massacre'/'ORI', 'Uncommon').
card_artist('eyeblight massacre'/'ORI', 'Igor Kieryluk').
card_number('eyeblight massacre'/'ORI', '96').
card_flavor_text('eyeblight massacre'/'ORI', '\"I\'ve already seen so much evil, so much darkness. Why do you insist on adding more?\"—Nissa Revane, to Dwynen').
card_multiverse_id('eyeblight massacre'/'ORI', '398645').

card_in_set('faerie miscreant', 'ORI').
card_original_type('faerie miscreant'/'ORI', 'Creature — Faerie Rogue').
card_original_text('faerie miscreant'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)When Faerie Miscreant enters the battlefield, if you control another creature named Faerie Miscreant, draw a card.').
card_first_print('faerie miscreant', 'ORI').
card_image_name('faerie miscreant'/'ORI', 'faerie miscreant').
card_uid('faerie miscreant'/'ORI', 'ORI:Faerie Miscreant:faerie miscreant').
card_rarity('faerie miscreant'/'ORI', 'Common').
card_artist('faerie miscreant'/'ORI', 'Steven Belledin').
card_number('faerie miscreant'/'ORI', '57').
card_flavor_text('faerie miscreant'/'ORI', 'One faerie distracts the judge while a second steals the law.').
card_multiverse_id('faerie miscreant'/'ORI', '398459').

card_in_set('fetid imp', 'ORI').
card_original_type('fetid imp'/'ORI', 'Creature — Imp').
card_original_text('fetid imp'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.){B}: Fetid Imp gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('fetid imp', 'ORI').
card_image_name('fetid imp'/'ORI', 'fetid imp').
card_uid('fetid imp'/'ORI', 'ORI:Fetid Imp:fetid imp').
card_rarity('fetid imp'/'ORI', 'Common').
card_artist('fetid imp'/'ORI', 'Nils Hamm').
card_number('fetid imp'/'ORI', '97').
card_multiverse_id('fetid imp'/'ORI', '398633').

card_in_set('fiery conclusion', 'ORI').
card_original_type('fiery conclusion'/'ORI', 'Instant').
card_original_text('fiery conclusion'/'ORI', 'As an additional cost to cast Fiery Conclusion, sacrifice a creature.Fiery Conclusion deals 5 damage to target creature.').
card_image_name('fiery conclusion'/'ORI', 'fiery conclusion').
card_uid('fiery conclusion'/'ORI', 'ORI:Fiery Conclusion:fiery conclusion').
card_rarity('fiery conclusion'/'ORI', 'Uncommon').
card_artist('fiery conclusion'/'ORI', 'Paolo Parente').
card_number('fiery conclusion'/'ORI', '144').
card_flavor_text('fiery conclusion'/'ORI', 'The Boros legionnaire saw a noble sacrifice, the Rakdos thug a blazing suicide, and the Izzet alchemist an experiment gone awry.').
card_multiverse_id('fiery conclusion'/'ORI', '398497').

card_in_set('fiery hellhound', 'ORI').
card_original_type('fiery hellhound'/'ORI', 'Creature — Elemental Hound').
card_original_text('fiery hellhound'/'ORI', '{R}: Fiery Hellhound gets +1/+0 until end of turn.').
card_image_name('fiery hellhound'/'ORI', 'fiery hellhound').
card_uid('fiery hellhound'/'ORI', 'ORI:Fiery Hellhound:fiery hellhound').
card_rarity('fiery hellhound'/'ORI', 'Common').
card_artist('fiery hellhound'/'ORI', 'Ted Galaday').
card_number('fiery hellhound'/'ORI', '284').
card_flavor_text('fiery hellhound'/'ORI', '\"I had hoped to instill in it the loyalty of a guard dog, but with fire\'s power comes its unpredictability.\"\n—Maggath, Sardian elementalist').
card_multiverse_id('fiery hellhound'/'ORI', '401463').

card_in_set('fiery impulse', 'ORI').
card_original_type('fiery impulse'/'ORI', 'Instant').
card_original_text('fiery impulse'/'ORI', 'Fiery Impulse deals 2 damage to target creature.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, Fiery Impulse deals 3 damage to that creature instead.').
card_first_print('fiery impulse', 'ORI').
card_image_name('fiery impulse'/'ORI', 'fiery impulse').
card_uid('fiery impulse'/'ORI', 'ORI:Fiery Impulse:fiery impulse').
card_rarity('fiery impulse'/'ORI', 'Common').
card_artist('fiery impulse'/'ORI', 'Daarken').
card_number('fiery impulse'/'ORI', '145').
card_flavor_text('fiery impulse'/'ORI', '\"Well, this got out of hand.\"').
card_multiverse_id('fiery impulse'/'ORI', '398516').

card_in_set('firefiend elemental', 'ORI').
card_original_type('firefiend elemental'/'ORI', 'Creature — Elemental').
card_original_text('firefiend elemental'/'ORI', 'Haste (This creature can attack and {T} as soon as it comes under your control.)Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_first_print('firefiend elemental', 'ORI').
card_image_name('firefiend elemental'/'ORI', 'firefiend elemental').
card_uid('firefiend elemental'/'ORI', 'ORI:Firefiend Elemental:firefiend elemental').
card_rarity('firefiend elemental'/'ORI', 'Common').
card_artist('firefiend elemental'/'ORI', 'Torstein Nordstrand').
card_number('firefiend elemental'/'ORI', '146').
card_multiverse_id('firefiend elemental'/'ORI', '398590').

card_in_set('flameshadow conjuring', 'ORI').
card_original_type('flameshadow conjuring'/'ORI', 'Enchantment').
card_original_text('flameshadow conjuring'/'ORI', 'Whenever a nontoken creature enters the battlefield under your control, you may pay {R}. If you do, put a token onto the battlefield that\'s a copy of that creature. That token gains haste. Exile it at the beginning of the next end step.').
card_first_print('flameshadow conjuring', 'ORI').
card_image_name('flameshadow conjuring'/'ORI', 'flameshadow conjuring').
card_uid('flameshadow conjuring'/'ORI', 'ORI:Flameshadow Conjuring:flameshadow conjuring').
card_rarity('flameshadow conjuring'/'ORI', 'Rare').
card_artist('flameshadow conjuring'/'ORI', 'Seb McKinnon').
card_number('flameshadow conjuring'/'ORI', '147').
card_multiverse_id('flameshadow conjuring'/'ORI', '398547').

card_in_set('flesh to dust', 'ORI').
card_original_type('flesh to dust'/'ORI', 'Instant').
card_original_text('flesh to dust'/'ORI', 'Destroy target creature. It can\'t be regenerated.').
card_image_name('flesh to dust'/'ORI', 'flesh to dust').
card_uid('flesh to dust'/'ORI', 'ORI:Flesh to Dust:flesh to dust').
card_rarity('flesh to dust'/'ORI', 'Common').
card_artist('flesh to dust'/'ORI', 'Julie Dillon').
card_number('flesh to dust'/'ORI', '280').
card_flavor_text('flesh to dust'/'ORI', '\"Another day. Another avenging angel. Another clump of feathers to toss in the trash.\"\n—Liliana Vess').
card_multiverse_id('flesh to dust'/'ORI', '401459').

card_in_set('fleshbag marauder', 'ORI').
card_original_type('fleshbag marauder'/'ORI', 'Creature — Zombie Warrior').
card_original_text('fleshbag marauder'/'ORI', 'When Fleshbag Marauder enters the battlefield, each player sacrifices a creature.').
card_image_name('fleshbag marauder'/'ORI', 'fleshbag marauder').
card_uid('fleshbag marauder'/'ORI', 'ORI:Fleshbag Marauder:fleshbag marauder').
card_rarity('fleshbag marauder'/'ORI', 'Uncommon').
card_artist('fleshbag marauder'/'ORI', 'Mark Zug').
card_number('fleshbag marauder'/'ORI', '98').
card_flavor_text('fleshbag marauder'/'ORI', 'Grixis is a world where the only things found in abundance are death and decay. Corpses, whole or in part, are the standard currency among necromancers and demons.').
card_multiverse_id('fleshbag marauder'/'ORI', '398625').

card_in_set('forest', 'ORI').
card_original_type('forest'/'ORI', 'Basic Land — Forest').
card_original_text('forest'/'ORI', 'G').
card_image_name('forest'/'ORI', 'forest1').
card_uid('forest'/'ORI', 'ORI:Forest:forest1').
card_rarity('forest'/'ORI', 'Basic Land').
card_artist('forest'/'ORI', 'John Avon').
card_number('forest'/'ORI', '269').
card_multiverse_id('forest'/'ORI', '398421').

card_in_set('forest', 'ORI').
card_original_type('forest'/'ORI', 'Basic Land — Forest').
card_original_text('forest'/'ORI', 'G').
card_image_name('forest'/'ORI', 'forest2').
card_uid('forest'/'ORI', 'ORI:Forest:forest2').
card_rarity('forest'/'ORI', 'Basic Land').
card_artist('forest'/'ORI', 'Jonas De Ro').
card_number('forest'/'ORI', '270').
card_multiverse_id('forest'/'ORI', '398570').

card_in_set('forest', 'ORI').
card_original_type('forest'/'ORI', 'Basic Land — Forest').
card_original_text('forest'/'ORI', 'G').
card_image_name('forest'/'ORI', 'forest3').
card_uid('forest'/'ORI', 'ORI:Forest:forest3').
card_rarity('forest'/'ORI', 'Basic Land').
card_artist('forest'/'ORI', 'Jonas De Ro').
card_number('forest'/'ORI', '271').
card_multiverse_id('forest'/'ORI', '398454').

card_in_set('forest', 'ORI').
card_original_type('forest'/'ORI', 'Basic Land — Forest').
card_original_text('forest'/'ORI', 'G').
card_image_name('forest'/'ORI', 'forest4').
card_uid('forest'/'ORI', 'ORI:Forest:forest4').
card_rarity('forest'/'ORI', 'Basic Land').
card_artist('forest'/'ORI', 'Vincent Proce').
card_number('forest'/'ORI', '272').
card_multiverse_id('forest'/'ORI', '398617').

card_in_set('foundry of the consuls', 'ORI').
card_original_type('foundry of the consuls'/'ORI', 'Land').
card_original_text('foundry of the consuls'/'ORI', '{T}: Add {1} to your mana pool.{5}, {T}, Sacrifice Foundry of the Consuls: Put two 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.').
card_first_print('foundry of the consuls', 'ORI').
card_image_name('foundry of the consuls'/'ORI', 'foundry of the consuls').
card_uid('foundry of the consuls'/'ORI', 'ORI:Foundry of the Consuls:foundry of the consuls').
card_rarity('foundry of the consuls'/'ORI', 'Uncommon').
card_artist('foundry of the consuls'/'ORI', 'Sam Burley').
card_number('foundry of the consuls'/'ORI', '247').
card_flavor_text('foundry of the consuls'/'ORI', 'All around the foundry, automatons unhitched themselves from their mounts and pivoted to face Chandra.').
card_multiverse_id('foundry of the consuls'/'ORI', '398613').

card_in_set('gaea\'s revenge', 'ORI').
card_original_type('gaea\'s revenge'/'ORI', 'Creature — Elemental').
card_original_text('gaea\'s revenge'/'ORI', 'Gaea\'s Revenge can\'t be countered.HasteGaea\'s Revenge can\'t be the target of nongreen spells or abilities from nongreen sources.').
card_image_name('gaea\'s revenge'/'ORI', 'gaea\'s revenge').
card_uid('gaea\'s revenge'/'ORI', 'ORI:Gaea\'s Revenge:gaea\'s revenge').
card_rarity('gaea\'s revenge'/'ORI', 'Rare').
card_artist('gaea\'s revenge'/'ORI', 'Kekai Kotaki').
card_number('gaea\'s revenge'/'ORI', '177').
card_multiverse_id('gaea\'s revenge'/'ORI', '398501').

card_in_set('gather the pack', 'ORI').
card_original_type('gather the pack'/'ORI', 'Sorcery').
card_original_text('gather the pack'/'ORI', 'Reveal the top five cards of your library. You may put a creature card from among them into your hand. Put the rest into your graveyard.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, put up to two creature cards from among the revealed cards into your hand instead of one.').
card_first_print('gather the pack', 'ORI').
card_image_name('gather the pack'/'ORI', 'gather the pack').
card_uid('gather the pack'/'ORI', 'ORI:Gather the Pack:gather the pack').
card_rarity('gather the pack'/'ORI', 'Uncommon').
card_artist('gather the pack'/'ORI', 'Igor Kieryluk').
card_number('gather the pack'/'ORI', '178').
card_multiverse_id('gather the pack'/'ORI', '398448').

card_in_set('ghirapur æther grid', 'ORI').
card_original_type('ghirapur æther grid'/'ORI', 'Enchantment').
card_original_text('ghirapur æther grid'/'ORI', 'Tap two untapped artifacts you control: Ghirapur Æther Grid deals 1 damage to target creature or player.').
card_first_print('ghirapur æther grid', 'ORI').
card_image_name('ghirapur æther grid'/'ORI', 'ghirapur aether grid').
card_uid('ghirapur æther grid'/'ORI', 'ORI:Ghirapur Æther Grid:ghirapur aether grid').
card_rarity('ghirapur æther grid'/'ORI', 'Uncommon').
card_artist('ghirapur æther grid'/'ORI', 'Cynthia Sheppard').
card_number('ghirapur æther grid'/'ORI', '148').
card_flavor_text('ghirapur æther grid'/'ORI', 'The city of Ghirapur is a living thing, and living things defend themselves.').
card_multiverse_id('ghirapur æther grid'/'ORI', '398517').

card_in_set('ghirapur gearcrafter', 'ORI').
card_original_type('ghirapur gearcrafter'/'ORI', 'Creature — Human Artificer').
card_original_text('ghirapur gearcrafter'/'ORI', 'When Ghirapur Gearcrafter enters the battlefield, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield. (A creature with flying can\'t be blocked except by creatures with flying or reach.)').
card_first_print('ghirapur gearcrafter', 'ORI').
card_image_name('ghirapur gearcrafter'/'ORI', 'ghirapur gearcrafter').
card_uid('ghirapur gearcrafter'/'ORI', 'ORI:Ghirapur Gearcrafter:ghirapur gearcrafter').
card_rarity('ghirapur gearcrafter'/'ORI', 'Common').
card_artist('ghirapur gearcrafter'/'ORI', 'Victor Adame Minguez').
card_number('ghirapur gearcrafter'/'ORI', '149').
card_flavor_text('ghirapur gearcrafter'/'ORI', 'The metal must be pure. The hand must be steady. The product must be perfect.').
card_multiverse_id('ghirapur gearcrafter'/'ORI', '398681').

card_in_set('gideon\'s phalanx', 'ORI').
card_original_type('gideon\'s phalanx'/'ORI', 'Instant').
card_original_text('gideon\'s phalanx'/'ORI', 'Put four 2/2 white Knight creature tokens with vigilance onto the battlefield.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, creatures you control gain indestructible until end of turn.').
card_first_print('gideon\'s phalanx', 'ORI').
card_image_name('gideon\'s phalanx'/'ORI', 'gideon\'s phalanx').
card_uid('gideon\'s phalanx'/'ORI', 'ORI:Gideon\'s Phalanx:gideon\'s phalanx').
card_rarity('gideon\'s phalanx'/'ORI', 'Rare').
card_artist('gideon\'s phalanx'/'ORI', 'James Ryman').
card_number('gideon\'s phalanx'/'ORI', '14').
card_multiverse_id('gideon\'s phalanx'/'ORI', '398471').

card_in_set('gideon, battle-forged', 'ORI').
card_original_type('gideon, battle-forged'/'ORI', 'Planeswalker — Gideon').
card_original_text('gideon, battle-forged'/'ORI', '+2: Up to one target creature an opponent controls attacks Gideon, Battle-Forged during its controller\'s next turn if able.+1: Until your next turn, target creature gains indestructible. Untap that creature.0: Until end of turn, Gideon, Battle-Forged becomes a 4/4 Human Soldier creature with indestructible that\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.').
card_first_print('gideon, battle-forged', 'ORI').
card_image_name('gideon, battle-forged'/'ORI', 'gideon, battle-forged').
card_uid('gideon, battle-forged'/'ORI', 'ORI:Gideon, Battle-Forged:gideon, battle-forged').
card_rarity('gideon, battle-forged'/'ORI', 'Mythic Rare').
card_artist('gideon, battle-forged'/'ORI', 'Willian Murai').
card_number('gideon, battle-forged'/'ORI', '23b').
card_multiverse_id('gideon, battle-forged'/'ORI', '398429').

card_in_set('gilt-leaf winnower', 'ORI').
card_original_type('gilt-leaf winnower'/'ORI', 'Creature — Elf Warrior').
card_original_text('gilt-leaf winnower'/'ORI', 'Menace (This creature can\'t be blocked except by two or more creatures.)When Gilt-Leaf Winnower enters the battlefield, you may destroy target non-Elf creature whose power and toughness aren\'t equal.').
card_first_print('gilt-leaf winnower', 'ORI').
card_image_name('gilt-leaf winnower'/'ORI', 'gilt-leaf winnower').
card_uid('gilt-leaf winnower'/'ORI', 'ORI:Gilt-Leaf Winnower:gilt-leaf winnower').
card_rarity('gilt-leaf winnower'/'ORI', 'Rare').
card_artist('gilt-leaf winnower'/'ORI', 'Viktor Titov').
card_number('gilt-leaf winnower'/'ORI', '99').
card_multiverse_id('gilt-leaf winnower'/'ORI', '398495').

card_in_set('gnarlroot trapper', 'ORI').
card_original_type('gnarlroot trapper'/'ORI', 'Creature — Elf Druid').
card_original_text('gnarlroot trapper'/'ORI', '{T}, Pay 1 life: Add {G} to your mana pool. Spend this mana only to cast an Elf creature spell.{T}: Target attacking Elf you control gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('gnarlroot trapper', 'ORI').
card_image_name('gnarlroot trapper'/'ORI', 'gnarlroot trapper').
card_uid('gnarlroot trapper'/'ORI', 'ORI:Gnarlroot Trapper:gnarlroot trapper').
card_rarity('gnarlroot trapper'/'ORI', 'Uncommon').
card_artist('gnarlroot trapper'/'ORI', 'Christopher Moeller').
card_number('gnarlroot trapper'/'ORI', '100').
card_multiverse_id('gnarlroot trapper'/'ORI', '398413').

card_in_set('goblin glory chaser', 'ORI').
card_original_type('goblin glory chaser'/'ORI', 'Creature — Goblin Warrior').
card_original_text('goblin glory chaser'/'ORI', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)As long as Goblin Glory Chaser is renowned, it has menace. (It can\'t be blocked except by two or more creatures.)').
card_first_print('goblin glory chaser', 'ORI').
card_image_name('goblin glory chaser'/'ORI', 'goblin glory chaser').
card_uid('goblin glory chaser'/'ORI', 'ORI:Goblin Glory Chaser:goblin glory chaser').
card_rarity('goblin glory chaser'/'ORI', 'Uncommon').
card_artist('goblin glory chaser'/'ORI', 'Greg Staples').
card_number('goblin glory chaser'/'ORI', '150').
card_multiverse_id('goblin glory chaser'/'ORI', '398598').

card_in_set('goblin piledriver', 'ORI').
card_original_type('goblin piledriver'/'ORI', 'Creature — Goblin Warrior').
card_original_text('goblin piledriver'/'ORI', 'Protection from blue (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything blue.)Whenever Goblin Piledriver attacks, it gets +2/+0 until end of turn for each other attacking Goblin.').
card_image_name('goblin piledriver'/'ORI', 'goblin piledriver').
card_uid('goblin piledriver'/'ORI', 'ORI:Goblin Piledriver:goblin piledriver').
card_rarity('goblin piledriver'/'ORI', 'Rare').
card_artist('goblin piledriver'/'ORI', 'Matt Cavotta').
card_number('goblin piledriver'/'ORI', '151').
card_multiverse_id('goblin piledriver'/'ORI', '398537').

card_in_set('gold-forged sentinel', 'ORI').
card_original_type('gold-forged sentinel'/'ORI', 'Artifact Creature — Chimera').
card_original_text('gold-forged sentinel'/'ORI', 'Flying').
card_image_name('gold-forged sentinel'/'ORI', 'gold-forged sentinel').
card_uid('gold-forged sentinel'/'ORI', 'ORI:Gold-Forged Sentinel:gold-forged sentinel').
card_rarity('gold-forged sentinel'/'ORI', 'Uncommon').
card_artist('gold-forged sentinel'/'ORI', 'James Zapata').
card_number('gold-forged sentinel'/'ORI', '226').
card_flavor_text('gold-forged sentinel'/'ORI', 'Blessed by the gods. Coveted by mortals. Beholden to neither.').
card_multiverse_id('gold-forged sentinel'/'ORI', '398585').

card_in_set('grasp of the hieromancer', 'ORI').
card_original_type('grasp of the hieromancer'/'ORI', 'Enchantment — Aura').
card_original_text('grasp of the hieromancer'/'ORI', 'Enchant creatureEnchanted creature gets +1/+1 and has \"Whenever this creature attacks, tap target creature defending player controls.\"').
card_first_print('grasp of the hieromancer', 'ORI').
card_image_name('grasp of the hieromancer'/'ORI', 'grasp of the hieromancer').
card_uid('grasp of the hieromancer'/'ORI', 'ORI:Grasp of the Hieromancer:grasp of the hieromancer').
card_rarity('grasp of the hieromancer'/'ORI', 'Common').
card_artist('grasp of the hieromancer'/'ORI', 'Igor Kieryluk').
card_number('grasp of the hieromancer'/'ORI', '15').
card_flavor_text('grasp of the hieromancer'/'ORI', '\"There is power in abandon, but far more in control.\"—Warden Hixus, to Kytheon').
card_multiverse_id('grasp of the hieromancer'/'ORI', '398558').

card_in_set('graveblade marauder', 'ORI').
card_original_type('graveblade marauder'/'ORI', 'Creature — Human Warrior').
card_original_text('graveblade marauder'/'ORI', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)Whenever Graveblade Marauder deals combat damage to a player, that player loses life equal to the number of creature cards in your graveyard.').
card_first_print('graveblade marauder', 'ORI').
card_image_name('graveblade marauder'/'ORI', 'graveblade marauder').
card_uid('graveblade marauder'/'ORI', 'ORI:Graveblade Marauder:graveblade marauder').
card_rarity('graveblade marauder'/'ORI', 'Rare').
card_artist('graveblade marauder'/'ORI', 'Jason Rainville').
card_number('graveblade marauder'/'ORI', '101').
card_multiverse_id('graveblade marauder'/'ORI', '398526').

card_in_set('guardian automaton', 'ORI').
card_original_type('guardian automaton'/'ORI', 'Artifact Creature — Construct').
card_original_text('guardian automaton'/'ORI', 'When Guardian Automaton dies, you gain 3 life.').
card_first_print('guardian automaton', 'ORI').
card_image_name('guardian automaton'/'ORI', 'guardian automaton').
card_uid('guardian automaton'/'ORI', 'ORI:Guardian Automaton:guardian automaton').
card_rarity('guardian automaton'/'ORI', 'Common').
card_artist('guardian automaton'/'ORI', 'Vincent Proce').
card_number('guardian automaton'/'ORI', '227').
card_flavor_text('guardian automaton'/'ORI', 'The wealthy in the city of Ghirapur outfit their lives with grand machines, entrusting even their children to filigree and gears.').
card_multiverse_id('guardian automaton'/'ORI', '398509').

card_in_set('guardians of meletis', 'ORI').
card_original_type('guardians of meletis'/'ORI', 'Artifact Creature — Golem').
card_original_text('guardians of meletis'/'ORI', 'Defender (This creature can\'t attack.)').
card_image_name('guardians of meletis'/'ORI', 'guardians of meletis').
card_uid('guardians of meletis'/'ORI', 'ORI:Guardians of Meletis:guardians of meletis').
card_rarity('guardians of meletis'/'ORI', 'Common').
card_artist('guardians of meletis'/'ORI', 'Magali Villeneuve').
card_number('guardians of meletis'/'ORI', '228').
card_flavor_text('guardians of meletis'/'ORI', 'The histories speak of two feuding rulers whose deaths were celebrated and whose monuments symbolized the end of their wars. In truth they were peaceful lovers, their story lost to the ages.').
card_multiverse_id('guardians of meletis'/'ORI', '398553').

card_in_set('hallowed moonlight', 'ORI').
card_original_type('hallowed moonlight'/'ORI', 'Instant').
card_original_text('hallowed moonlight'/'ORI', 'Until end of turn, if a creature would enter the battlefield and it wasn\'t cast, exile it instead.Draw a card.').
card_first_print('hallowed moonlight', 'ORI').
card_image_name('hallowed moonlight'/'ORI', 'hallowed moonlight').
card_uid('hallowed moonlight'/'ORI', 'ORI:Hallowed Moonlight:hallowed moonlight').
card_rarity('hallowed moonlight'/'ORI', 'Rare').
card_artist('hallowed moonlight'/'ORI', 'Mike Bierek').
card_number('hallowed moonlight'/'ORI', '16').
card_flavor_text('hallowed moonlight'/'ORI', 'Creatures of darkness instinctively hide from the light.').
card_multiverse_id('hallowed moonlight'/'ORI', '398505').

card_in_set('hangarback walker', 'ORI').
card_original_type('hangarback walker'/'ORI', 'Artifact Creature — Construct').
card_original_text('hangarback walker'/'ORI', 'Hangarback Walker enters the battlefield with X +1/+1 counters on it.When Hangarback Walker dies, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield for each +1/+1 counter on Hangarback Walker.{1}, {T}: Put a +1/+1 counter on Hangarback Walker.').
card_first_print('hangarback walker', 'ORI').
card_image_name('hangarback walker'/'ORI', 'hangarback walker').
card_uid('hangarback walker'/'ORI', 'ORI:Hangarback Walker:hangarback walker').
card_rarity('hangarback walker'/'ORI', 'Rare').
card_artist('hangarback walker'/'ORI', 'Daarken').
card_number('hangarback walker'/'ORI', '229').
card_multiverse_id('hangarback walker'/'ORI', '398572').

card_in_set('harbinger of the tides', 'ORI').
card_original_type('harbinger of the tides'/'ORI', 'Creature — Merfolk Wizard').
card_original_text('harbinger of the tides'/'ORI', 'You may cast Harbinger of the Tides as though it had flash if you pay {2} more to cast it. (You may cast it any time you could cast an instant.)When Harbinger of the Tides enters the battlefield, you may return target tapped creature an opponent controls to its owner\'s hand.').
card_first_print('harbinger of the tides', 'ORI').
card_image_name('harbinger of the tides'/'ORI', 'harbinger of the tides').
card_uid('harbinger of the tides'/'ORI', 'ORI:Harbinger of the Tides:harbinger of the tides').
card_rarity('harbinger of the tides'/'ORI', 'Rare').
card_artist('harbinger of the tides'/'ORI', 'Svetlin Velinov').
card_number('harbinger of the tides'/'ORI', '58').
card_multiverse_id('harbinger of the tides'/'ORI', '398569').

card_in_set('healing hands', 'ORI').
card_original_type('healing hands'/'ORI', 'Sorcery').
card_original_text('healing hands'/'ORI', 'Target player gains 4 life.Draw a card.').
card_first_print('healing hands', 'ORI').
card_image_name('healing hands'/'ORI', 'healing hands').
card_uid('healing hands'/'ORI', 'ORI:Healing Hands:healing hands').
card_rarity('healing hands'/'ORI', 'Common').
card_artist('healing hands'/'ORI', 'Josu Hernaiz').
card_number('healing hands'/'ORI', '17').
card_flavor_text('healing hands'/'ORI', 'The ethereal touch brings relief. The physical touch brings peace.').
card_multiverse_id('healing hands'/'ORI', '398563').

card_in_set('heavy infantry', 'ORI').
card_original_type('heavy infantry'/'ORI', 'Creature — Human Soldier').
card_original_text('heavy infantry'/'ORI', 'When Heavy Infantry enters the battlefield, tap target creature an opponent controls.').
card_first_print('heavy infantry', 'ORI').
card_image_name('heavy infantry'/'ORI', 'heavy infantry').
card_uid('heavy infantry'/'ORI', 'ORI:Heavy Infantry:heavy infantry').
card_rarity('heavy infantry'/'ORI', 'Common').
card_artist('heavy infantry'/'ORI', 'David Gaillet').
card_number('heavy infantry'/'ORI', '18').
card_flavor_text('heavy infantry'/'ORI', 'Doors, walls, skulls . . . it matters not. All barriers will be broken.').
card_multiverse_id('heavy infantry'/'ORI', '398408').

card_in_set('helm of the gods', 'ORI').
card_original_type('helm of the gods'/'ORI', 'Artifact — Equipment').
card_original_text('helm of the gods'/'ORI', 'Equipped creature gets +1/+1 for each enchantment you control.Equip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('helm of the gods', 'ORI').
card_image_name('helm of the gods'/'ORI', 'helm of the gods').
card_uid('helm of the gods'/'ORI', 'ORI:Helm of the Gods:helm of the gods').
card_rarity('helm of the gods'/'ORI', 'Rare').
card_artist('helm of the gods'/'ORI', 'Anthony Palumbo').
card_number('helm of the gods'/'ORI', '230').
card_flavor_text('helm of the gods'/'ORI', 'Its blessings are as numerous as the stars of Nyx.').
card_multiverse_id('helm of the gods'/'ORI', '398588').

card_in_set('herald of the pantheon', 'ORI').
card_original_type('herald of the pantheon'/'ORI', 'Creature — Centaur Shaman').
card_original_text('herald of the pantheon'/'ORI', 'Enchantment spells you cast cost {1} less to cast.Whenever you cast an enchantment spell, you gain 1 life.').
card_first_print('herald of the pantheon', 'ORI').
card_image_name('herald of the pantheon'/'ORI', 'herald of the pantheon').
card_uid('herald of the pantheon'/'ORI', 'ORI:Herald of the Pantheon:herald of the pantheon').
card_rarity('herald of the pantheon'/'ORI', 'Rare').
card_artist('herald of the pantheon'/'ORI', 'Jason A. Engle').
card_number('herald of the pantheon'/'ORI', '180').
card_flavor_text('herald of the pantheon'/'ORI', 'The distinction of bearing the gods\' banner is nothing compared to the glory of being closer to Nyx.').
card_multiverse_id('herald of the pantheon'/'ORI', '398460').

card_in_set('hitchclaw recluse', 'ORI').
card_original_type('hitchclaw recluse'/'ORI', 'Creature — Spider').
card_original_text('hitchclaw recluse'/'ORI', 'Reach (This creature can block creatures with flying.)').
card_first_print('hitchclaw recluse', 'ORI').
card_image_name('hitchclaw recluse'/'ORI', 'hitchclaw recluse').
card_uid('hitchclaw recluse'/'ORI', 'ORI:Hitchclaw Recluse:hitchclaw recluse').
card_rarity('hitchclaw recluse'/'ORI', 'Common').
card_artist('hitchclaw recluse'/'ORI', 'Jeff Simpson').
card_number('hitchclaw recluse'/'ORI', '181').
card_flavor_text('hitchclaw recluse'/'ORI', 'Not all spiders need webs to catch their prey.').
card_multiverse_id('hitchclaw recluse'/'ORI', '398676').

card_in_set('hixus, prison warden', 'ORI').
card_original_type('hixus, prison warden'/'ORI', 'Legendary Creature — Human Soldier').
card_original_text('hixus, prison warden'/'ORI', 'Flash (You may cast this spell any time you could cast an instant.)Whenever a creature deals combat damage to you, if Hixus, Prison Warden entered the battlefield this turn, exile that creature until Hixus leaves the battlefield. (That creature returns under its owner\'s control.)').
card_first_print('hixus, prison warden', 'ORI').
card_image_name('hixus, prison warden'/'ORI', 'hixus, prison warden').
card_uid('hixus, prison warden'/'ORI', 'ORI:Hixus, Prison Warden:hixus, prison warden').
card_rarity('hixus, prison warden'/'ORI', 'Rare').
card_artist('hixus, prison warden'/'ORI', 'Chris Rallis').
card_number('hixus, prison warden'/'ORI', '19').
card_multiverse_id('hixus, prison warden'/'ORI', '398611').

card_in_set('honored hierarch', 'ORI').
card_original_type('honored hierarch'/'ORI', 'Creature — Human Druid').
card_original_text('honored hierarch'/'ORI', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)As long as Honored Hierarch is renowned, it has vigilance and \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('honored hierarch', 'ORI').
card_image_name('honored hierarch'/'ORI', 'honored hierarch').
card_uid('honored hierarch'/'ORI', 'ORI:Honored Hierarch:honored hierarch').
card_rarity('honored hierarch'/'ORI', 'Rare').
card_artist('honored hierarch'/'ORI', 'Matt Stewart').
card_number('honored hierarch'/'ORI', '182').
card_multiverse_id('honored hierarch'/'ORI', '398450').

card_in_set('hydrolash', 'ORI').
card_original_type('hydrolash'/'ORI', 'Instant').
card_original_text('hydrolash'/'ORI', 'Attacking creatures get -2/-0 until end of turn.Draw a card.').
card_first_print('hydrolash', 'ORI').
card_image_name('hydrolash'/'ORI', 'hydrolash').
card_uid('hydrolash'/'ORI', 'ORI:Hydrolash:hydrolash').
card_rarity('hydrolash'/'ORI', 'Uncommon').
card_artist('hydrolash'/'ORI', 'Clint Cearley').
card_number('hydrolash'/'ORI', '59').
card_flavor_text('hydrolash'/'ORI', '\"I don\'t need your mage-rings or your mana equations. I call, and the sea answers.\"—Keyta, rebel hydromancer').
card_multiverse_id('hydrolash'/'ORI', '398666').

card_in_set('infectious bloodlust', 'ORI').
card_original_type('infectious bloodlust'/'ORI', 'Enchantment — Aura').
card_original_text('infectious bloodlust'/'ORI', 'Enchant creatureEnchanted creature gets +2/+1, has haste, and attacks each turn if able.When enchanted creature dies, you may search your library for a card named Infectious Bloodlust, reveal it, put it into your hand, then shuffle your library.').
card_first_print('infectious bloodlust', 'ORI').
card_image_name('infectious bloodlust'/'ORI', 'infectious bloodlust').
card_uid('infectious bloodlust'/'ORI', 'ORI:Infectious Bloodlust:infectious bloodlust').
card_rarity('infectious bloodlust'/'ORI', 'Common').
card_artist('infectious bloodlust'/'ORI', 'Zoltan Boros').
card_number('infectious bloodlust'/'ORI', '152').
card_multiverse_id('infectious bloodlust'/'ORI', '398582').

card_in_set('infernal scarring', 'ORI').
card_original_type('infernal scarring'/'ORI', 'Enchantment — Aura').
card_original_text('infernal scarring'/'ORI', 'Enchant creatureEnchanted creature gets +2/+0 and has \"When this creature dies, draw a card.\"').
card_first_print('infernal scarring', 'ORI').
card_image_name('infernal scarring'/'ORI', 'infernal scarring').
card_uid('infernal scarring'/'ORI', 'ORI:Infernal Scarring:infernal scarring').
card_rarity('infernal scarring'/'ORI', 'Common').
card_artist('infernal scarring'/'ORI', 'Mike Bierek').
card_number('infernal scarring'/'ORI', '102').
card_flavor_text('infernal scarring'/'ORI', 'One who is marked by a demon in life is sure to be remembered as one in death.').
card_multiverse_id('infernal scarring'/'ORI', '398638').

card_in_set('infinite obliteration', 'ORI').
card_original_type('infinite obliteration'/'ORI', 'Sorcery').
card_original_text('infinite obliteration'/'ORI', 'Name a creature card. Search target opponent\'s graveyard, hand, and library for any number of cards with that name and exile them. Then that player shuffles his or her library.').
card_first_print('infinite obliteration', 'ORI').
card_image_name('infinite obliteration'/'ORI', 'infinite obliteration').
card_uid('infinite obliteration'/'ORI', 'ORI:Infinite Obliteration:infinite obliteration').
card_rarity('infinite obliteration'/'ORI', 'Rare').
card_artist('infinite obliteration'/'ORI', 'Yeong-Hao Han').
card_number('infinite obliteration'/'ORI', '103').
card_multiverse_id('infinite obliteration'/'ORI', '398503').

card_in_set('into the void', 'ORI').
card_original_type('into the void'/'ORI', 'Sorcery').
card_original_text('into the void'/'ORI', 'Return up to two target creatures to their owners\' hands.').
card_image_name('into the void'/'ORI', 'into the void').
card_uid('into the void'/'ORI', 'ORI:Into the Void:into the void').
card_rarity('into the void'/'ORI', 'Uncommon').
card_artist('into the void'/'ORI', 'Daarken').
card_number('into the void'/'ORI', '277').
card_flavor_text('into the void'/'ORI', '\"The cathars have their swords, the inquisitors their axes. I prefer the ‘diplomatic\' approach.\"\n—Terhold, archmage of Drunau').
card_multiverse_id('into the void'/'ORI', '401456').

card_in_set('iroas\'s champion', 'ORI').
card_original_type('iroas\'s champion'/'ORI', 'Creature — Human Soldier').
card_original_text('iroas\'s champion'/'ORI', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_first_print('iroas\'s champion', 'ORI').
card_image_name('iroas\'s champion'/'ORI', 'iroas\'s champion').
card_uid('iroas\'s champion'/'ORI', 'ORI:Iroas\'s Champion:iroas\'s champion').
card_rarity('iroas\'s champion'/'ORI', 'Uncommon').
card_artist('iroas\'s champion'/'ORI', 'Marco Nelor').
card_number('iroas\'s champion'/'ORI', '214').
card_flavor_text('iroas\'s champion'/'ORI', 'Accustomed to battling before an audience in the arena, Iroas\'s champions know how to put on a good show.').
card_multiverse_id('iroas\'s champion'/'ORI', '398653').

card_in_set('island', 'ORI').
card_original_type('island'/'ORI', 'Basic Land — Island').
card_original_text('island'/'ORI', 'U').
card_image_name('island'/'ORI', 'island1').
card_uid('island'/'ORI', 'ORI:Island:island1').
card_rarity('island'/'ORI', 'Basic Land').
card_artist('island'/'ORI', 'Jung Park').
card_number('island'/'ORI', '257').
card_multiverse_id('island'/'ORI', '398664').

card_in_set('island', 'ORI').
card_original_type('island'/'ORI', 'Basic Land — Island').
card_original_text('island'/'ORI', 'U').
card_image_name('island'/'ORI', 'island2').
card_uid('island'/'ORI', 'ORI:Island:island2').
card_rarity('island'/'ORI', 'Basic Land').
card_artist('island'/'ORI', 'Jung Park').
card_number('island'/'ORI', '258').
card_multiverse_id('island'/'ORI', '398586').

card_in_set('island', 'ORI').
card_original_type('island'/'ORI', 'Basic Land — Island').
card_original_text('island'/'ORI', 'U').
card_image_name('island'/'ORI', 'island3').
card_uid('island'/'ORI', 'ORI:Island:island3').
card_rarity('island'/'ORI', 'Basic Land').
card_artist('island'/'ORI', 'Richard Wright').
card_number('island'/'ORI', '259').
card_multiverse_id('island'/'ORI', '398431').

card_in_set('island', 'ORI').
card_original_type('island'/'ORI', 'Basic Land — Island').
card_original_text('island'/'ORI', 'U').
card_image_name('island'/'ORI', 'island4').
card_uid('island'/'ORI', 'ORI:Island:island4').
card_rarity('island'/'ORI', 'Basic Land').
card_artist('island'/'ORI', 'Richard Wright').
card_number('island'/'ORI', '260').
card_multiverse_id('island'/'ORI', '398477').

card_in_set('jace\'s sanctum', 'ORI').
card_original_type('jace\'s sanctum'/'ORI', 'Enchantment').
card_original_text('jace\'s sanctum'/'ORI', 'Instant and sorcery spells you cast cost {1} less to cast.Whenever you cast an instant or sorcery spell, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('jace\'s sanctum', 'ORI').
card_image_name('jace\'s sanctum'/'ORI', 'jace\'s sanctum').
card_uid('jace\'s sanctum'/'ORI', 'ORI:Jace\'s Sanctum:jace\'s sanctum').
card_rarity('jace\'s sanctum'/'ORI', 'Rare').
card_artist('jace\'s sanctum'/'ORI', 'Adam Paquette').
card_number('jace\'s sanctum'/'ORI', '61').
card_multiverse_id('jace\'s sanctum'/'ORI', '398614').

card_in_set('jace, telepath unbound', 'ORI').
card_original_type('jace, telepath unbound'/'ORI', 'Planeswalker — Jace').
card_original_text('jace, telepath unbound'/'ORI', '+1: Up to one target creature gets -2/-0 until your next turn.−3: You may cast target instant or sorcery card from your graveyard this turn. If that card would be put into a graveyard this turn, exile it instead.−9: You get an emblem with \"Whenever you cast a spell, target opponent puts the top five cards of his or her library into his or her graveyard.\"').
card_first_print('jace, telepath unbound', 'ORI').
card_image_name('jace, telepath unbound'/'ORI', 'jace, telepath unbound').
card_uid('jace, telepath unbound'/'ORI', 'ORI:Jace, Telepath Unbound:jace, telepath unbound').
card_rarity('jace, telepath unbound'/'ORI', 'Mythic Rare').
card_artist('jace, telepath unbound'/'ORI', 'Jaime Jones').
card_number('jace, telepath unbound'/'ORI', '60b').
card_multiverse_id('jace, telepath unbound'/'ORI', '398435').

card_in_set('jace, vryn\'s prodigy', 'ORI').
card_original_type('jace, vryn\'s prodigy'/'ORI', 'Legendary Creature — Human Wizard').
card_original_text('jace, vryn\'s prodigy'/'ORI', '{T}: Draw a card, then discard a card. If there are five or more cards in your graveyard, exile Jace, Vryn\'s Prodigy, then return him to the battlefield transformed under his owner\'s control.').
card_first_print('jace, vryn\'s prodigy', 'ORI').
card_image_name('jace, vryn\'s prodigy'/'ORI', 'jace, vryn\'s prodigy').
card_uid('jace, vryn\'s prodigy'/'ORI', 'ORI:Jace, Vryn\'s Prodigy:jace, vryn\'s prodigy').
card_rarity('jace, vryn\'s prodigy'/'ORI', 'Mythic Rare').
card_artist('jace, vryn\'s prodigy'/'ORI', 'Jaime Jones').
card_number('jace, vryn\'s prodigy'/'ORI', '60a').
card_flavor_text('jace, vryn\'s prodigy'/'ORI', '\"People\'s thoughts just come to me. Sometimes I don\'t know if it\'s them or me thinking.\"').
card_multiverse_id('jace, vryn\'s prodigy'/'ORI', '398434').

card_in_set('jayemdae tome', 'ORI').
card_original_type('jayemdae tome'/'ORI', 'Artifact').
card_original_text('jayemdae tome'/'ORI', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'ORI', 'jayemdae tome').
card_uid('jayemdae tome'/'ORI', 'ORI:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'ORI', 'Uncommon').
card_artist('jayemdae tome'/'ORI', 'Donato Giancola').
card_number('jayemdae tome'/'ORI', '231').
card_flavor_text('jayemdae tome'/'ORI', 'A true scribe devotes an entire lifetime to the creation of a single volume, a masterpiece as unique as its maker.').
card_multiverse_id('jayemdae tome'/'ORI', '398530').

card_in_set('jhessian thief', 'ORI').
card_original_type('jhessian thief'/'ORI', 'Creature — Human Rogue').
card_original_text('jhessian thief'/'ORI', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)Whenever Jhessian Thief deals combat damage to a player, draw a card.').
card_first_print('jhessian thief', 'ORI').
card_image_name('jhessian thief'/'ORI', 'jhessian thief').
card_uid('jhessian thief'/'ORI', 'ORI:Jhessian Thief:jhessian thief').
card_rarity('jhessian thief'/'ORI', 'Uncommon').
card_artist('jhessian thief'/'ORI', 'Miles Johnston').
card_number('jhessian thief'/'ORI', '62').
card_flavor_text('jhessian thief'/'ORI', '\"Where\'s the fun in an escape if it\'s not at least a little daring?\"').
card_multiverse_id('jhessian thief'/'ORI', '398536').

card_in_set('joraga invocation', 'ORI').
card_original_type('joraga invocation'/'ORI', 'Sorcery').
card_original_text('joraga invocation'/'ORI', 'Each creature you control gets +3/+3 until end of turn and must be blocked this turn if able.').
card_first_print('joraga invocation', 'ORI').
card_image_name('joraga invocation'/'ORI', 'joraga invocation').
card_uid('joraga invocation'/'ORI', 'ORI:Joraga Invocation:joraga invocation').
card_rarity('joraga invocation'/'ORI', 'Uncommon').
card_artist('joraga invocation'/'ORI', 'Kieran Yanner').
card_number('joraga invocation'/'ORI', '183').
card_flavor_text('joraga invocation'/'ORI', '\"A single tree does not a forest make. We are stronger when we stand together.\"—Numa, Joraga chieftain').
card_multiverse_id('joraga invocation'/'ORI', '398642').

card_in_set('knight of the pilgrim\'s road', 'ORI').
card_original_type('knight of the pilgrim\'s road'/'ORI', 'Creature — Human Knight').
card_original_text('knight of the pilgrim\'s road'/'ORI', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_first_print('knight of the pilgrim\'s road', 'ORI').
card_image_name('knight of the pilgrim\'s road'/'ORI', 'knight of the pilgrim\'s road').
card_uid('knight of the pilgrim\'s road'/'ORI', 'ORI:Knight of the Pilgrim\'s Road:knight of the pilgrim\'s road').
card_rarity('knight of the pilgrim\'s road'/'ORI', 'Common').
card_artist('knight of the pilgrim\'s road'/'ORI', 'David Gaillet').
card_number('knight of the pilgrim\'s road'/'ORI', '20').
card_flavor_text('knight of the pilgrim\'s road'/'ORI', '\"To be a knight, Gideon, is to be the shield for the meek against the cruel.\"').
card_multiverse_id('knight of the pilgrim\'s road'/'ORI', '398420').

card_in_set('knight of the white orchid', 'ORI').
card_original_type('knight of the white orchid'/'ORI', 'Creature — Human Knight').
card_original_text('knight of the white orchid'/'ORI', 'First strikeWhen Knight of the White Orchid enters the battlefield, if an opponent controls more lands than you, you may search your library for a Plains card, put it onto the battlefield, then shuffle your library.').
card_image_name('knight of the white orchid'/'ORI', 'knight of the white orchid').
card_uid('knight of the white orchid'/'ORI', 'ORI:Knight of the White Orchid:knight of the white orchid').
card_rarity('knight of the white orchid'/'ORI', 'Rare').
card_artist('knight of the white orchid'/'ORI', 'Mark Zug').
card_number('knight of the white orchid'/'ORI', '21').
card_multiverse_id('knight of the white orchid'/'ORI', '398594').

card_in_set('knightly valor', 'ORI').
card_original_type('knightly valor'/'ORI', 'Enchantment — Aura').
card_original_text('knightly valor'/'ORI', 'Enchant creatureWhen Knightly Valor enters the battlefield, put a 2/2 white Knight creature token with vigilance onto the battlefield. (Attacking doesn\'t cause it to tap.)Enchanted creature gets +2/+2 and has vigilance.').
card_image_name('knightly valor'/'ORI', 'knightly valor').
card_uid('knightly valor'/'ORI', 'ORI:Knightly Valor:knightly valor').
card_rarity('knightly valor'/'ORI', 'Uncommon').
card_artist('knightly valor'/'ORI', 'Matt Stewart').
card_number('knightly valor'/'ORI', '22').
card_multiverse_id('knightly valor'/'ORI', '398622').

card_in_set('kothophed, soul hoarder', 'ORI').
card_original_type('kothophed, soul hoarder'/'ORI', 'Legendary Creature — Demon').
card_original_text('kothophed, soul hoarder'/'ORI', 'FlyingWhenever a permanent owned by another player is put into a graveyard from the battlefield, you draw a card and you lose 1 life.').
card_first_print('kothophed, soul hoarder', 'ORI').
card_image_name('kothophed, soul hoarder'/'ORI', 'kothophed, soul hoarder').
card_uid('kothophed, soul hoarder'/'ORI', 'ORI:Kothophed, Soul Hoarder:kothophed, soul hoarder').
card_rarity('kothophed, soul hoarder'/'ORI', 'Rare').
card_artist('kothophed, soul hoarder'/'ORI', 'Jakub Kasper').
card_number('kothophed, soul hoarder'/'ORI', '104').
card_flavor_text('kothophed, soul hoarder'/'ORI', '\"I will be your most demanding master, Liliana. But I have great things in mind for you.\"').
card_multiverse_id('kothophed, soul hoarder'/'ORI', '398443').

card_in_set('kytheon\'s irregulars', 'ORI').
card_original_type('kytheon\'s irregulars'/'ORI', 'Creature — Human Soldier').
card_original_text('kytheon\'s irregulars'/'ORI', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.){W}{W}: Tap target creature.').
card_first_print('kytheon\'s irregulars', 'ORI').
card_image_name('kytheon\'s irregulars'/'ORI', 'kytheon\'s irregulars').
card_uid('kytheon\'s irregulars'/'ORI', 'ORI:Kytheon\'s Irregulars:kytheon\'s irregulars').
card_rarity('kytheon\'s irregulars'/'ORI', 'Rare').
card_artist('kytheon\'s irregulars'/'ORI', 'Mark Winters').
card_number('kytheon\'s irregulars'/'ORI', '24').
card_flavor_text('kytheon\'s irregulars'/'ORI', 'Kytheon and his irregulars worked outside the law to bring justice to the streets of Akros.').
card_multiverse_id('kytheon\'s irregulars'/'ORI', '398561').

card_in_set('kytheon\'s tactics', 'ORI').
card_original_type('kytheon\'s tactics'/'ORI', 'Sorcery').
card_original_text('kytheon\'s tactics'/'ORI', 'Creatures you control get +2/+1 until end of turn.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, those creatures also gain vigilance until end of turn. (Attacking doesn\'t cause them to tap.)').
card_first_print('kytheon\'s tactics', 'ORI').
card_image_name('kytheon\'s tactics'/'ORI', 'kytheon\'s tactics').
card_uid('kytheon\'s tactics'/'ORI', 'ORI:Kytheon\'s Tactics:kytheon\'s tactics').
card_rarity('kytheon\'s tactics'/'ORI', 'Common').
card_artist('kytheon\'s tactics'/'ORI', 'Raymond Swanland').
card_number('kytheon\'s tactics'/'ORI', '25').
card_flavor_text('kytheon\'s tactics'/'ORI', '\"Seal the gate. This is our fight now.\"').
card_multiverse_id('kytheon\'s tactics'/'ORI', '398470').

card_in_set('kytheon, hero of akros', 'ORI').
card_original_type('kytheon, hero of akros'/'ORI', 'Legendary Creature — Human Soldier').
card_original_text('kytheon, hero of akros'/'ORI', 'At end of combat, if Kytheon, Hero of Akros and at least two other creatures attacked this combat, exile Kytheon, then return him to the battlefield transformed under his owner\'s control.{2}{W}: Kytheon gains indestructible until end of turn.').
card_first_print('kytheon, hero of akros', 'ORI').
card_image_name('kytheon, hero of akros'/'ORI', 'kytheon, hero of akros').
card_uid('kytheon, hero of akros'/'ORI', 'ORI:Kytheon, Hero of Akros:kytheon, hero of akros').
card_rarity('kytheon, hero of akros'/'ORI', 'Mythic Rare').
card_artist('kytheon, hero of akros'/'ORI', 'Willian Murai').
card_number('kytheon, hero of akros'/'ORI', '23a').
card_multiverse_id('kytheon, hero of akros'/'ORI', '398428').

card_in_set('languish', 'ORI').
card_original_type('languish'/'ORI', 'Sorcery').
card_original_text('languish'/'ORI', 'All creatures get -4/-4 until end of turn.').
card_first_print('languish', 'ORI').
card_image_name('languish'/'ORI', 'languish').
card_uid('languish'/'ORI', 'ORI:Languish:languish').
card_rarity('languish'/'ORI', 'Rare').
card_artist('languish'/'ORI', 'Jeff Simpson').
card_number('languish'/'ORI', '105').
card_flavor_text('languish'/'ORI', 'Life is such a fragile thing.').
card_multiverse_id('languish'/'ORI', '398597').

card_in_set('leaf gilder', 'ORI').
card_original_type('leaf gilder'/'ORI', 'Creature — Elf Druid').
card_original_text('leaf gilder'/'ORI', '{T}: Add {G} to your mana pool.').
card_image_name('leaf gilder'/'ORI', 'leaf gilder').
card_uid('leaf gilder'/'ORI', 'ORI:Leaf Gilder:leaf gilder').
card_rarity('leaf gilder'/'ORI', 'Common').
card_artist('leaf gilder'/'ORI', 'Quinton Hoover').
card_number('leaf gilder'/'ORI', '184').
card_flavor_text('leaf gilder'/'ORI', 'Eidren, perfect of Lys Alana, ordered hundreds of trees uprooted and rearranged into a pattern he deemed beautiful. Thus the Gilt-Leaf Wood was born.').
card_multiverse_id('leaf gilder'/'ORI', '398634').

card_in_set('lightning javelin', 'ORI').
card_original_type('lightning javelin'/'ORI', 'Sorcery').
card_original_text('lightning javelin'/'ORI', 'Lightning Javelin deals 3 damage to target creature or player. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_first_print('lightning javelin', 'ORI').
card_image_name('lightning javelin'/'ORI', 'lightning javelin').
card_uid('lightning javelin'/'ORI', 'ORI:Lightning Javelin:lightning javelin').
card_rarity('lightning javelin'/'ORI', 'Common').
card_artist('lightning javelin'/'ORI', 'Seb McKinnon').
card_number('lightning javelin'/'ORI', '153').
card_flavor_text('lightning javelin'/'ORI', 'The harpies descended without mercy upon Akros, only to find their attack put them within range of the javelineers.').
card_multiverse_id('lightning javelin'/'ORI', '398538').

card_in_set('liliana, defiant necromancer', 'ORI').
card_original_type('liliana, defiant necromancer'/'ORI', 'Planeswalker — Liliana').
card_original_text('liliana, defiant necromancer'/'ORI', '+2: Each player discards a card.−X: Return target nonlegendary creature card with converted mana cost X from your graveyard to the battlefield.−8: You get an emblem with \"Whenever a creature dies, return it to the battlefield under your control at the beginning of the next end step.\"').
card_first_print('liliana, defiant necromancer', 'ORI').
card_image_name('liliana, defiant necromancer'/'ORI', 'liliana, defiant necromancer').
card_uid('liliana, defiant necromancer'/'ORI', 'ORI:Liliana, Defiant Necromancer:liliana, defiant necromancer').
card_rarity('liliana, defiant necromancer'/'ORI', 'Mythic Rare').
card_artist('liliana, defiant necromancer'/'ORI', 'Karla Ortiz').
card_number('liliana, defiant necromancer'/'ORI', '106b').
card_multiverse_id('liliana, defiant necromancer'/'ORI', '398442').

card_in_set('liliana, heretical healer', 'ORI').
card_original_type('liliana, heretical healer'/'ORI', 'Legendary Creature — Human Cleric').
card_original_text('liliana, heretical healer'/'ORI', 'LifelinkWhenever another nontoken creature you control dies, exile Liliana, Heretical Healer, then return her to the battlefield transformed under her owner\'s control. If you do, put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('liliana, heretical healer', 'ORI').
card_image_name('liliana, heretical healer'/'ORI', 'liliana, heretical healer').
card_uid('liliana, heretical healer'/'ORI', 'ORI:Liliana, Heretical Healer:liliana, heretical healer').
card_rarity('liliana, heretical healer'/'ORI', 'Mythic Rare').
card_artist('liliana, heretical healer'/'ORI', 'Karla Ortiz').
card_number('liliana, heretical healer'/'ORI', '106a').
card_multiverse_id('liliana, heretical healer'/'ORI', '398441').

card_in_set('llanowar empath', 'ORI').
card_original_type('llanowar empath'/'ORI', 'Creature — Elf Shaman').
card_original_text('llanowar empath'/'ORI', 'When Llanowar Empath enters the battlefield, scry 2, then reveal the top card of your library. If it\'s a creature card, put it into your hand. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('llanowar empath'/'ORI', 'llanowar empath').
card_uid('llanowar empath'/'ORI', 'ORI:Llanowar Empath:llanowar empath').
card_rarity('llanowar empath'/'ORI', 'Common').
card_artist('llanowar empath'/'ORI', 'Warren Mahy').
card_number('llanowar empath'/'ORI', '185').
card_multiverse_id('llanowar empath'/'ORI', '398482').

card_in_set('llanowar wastes', 'ORI').
card_original_type('llanowar wastes'/'ORI', 'Land').
card_original_text('llanowar wastes'/'ORI', '{T}: Add {1} to your mana pool.{T}: Add {B} or {G} to your mana pool. Llanowar Wastes deals 1 damage to you.').
card_image_name('llanowar wastes'/'ORI', 'llanowar wastes').
card_uid('llanowar wastes'/'ORI', 'ORI:Llanowar Wastes:llanowar wastes').
card_rarity('llanowar wastes'/'ORI', 'Rare').
card_artist('llanowar wastes'/'ORI', 'Rob Alexander').
card_number('llanowar wastes'/'ORI', '248').
card_multiverse_id('llanowar wastes'/'ORI', '398589').

card_in_set('macabre waltz', 'ORI').
card_original_type('macabre waltz'/'ORI', 'Sorcery').
card_original_text('macabre waltz'/'ORI', 'Return up to two target creature cards from your graveyard to your hand, then discard a card.').
card_image_name('macabre waltz'/'ORI', 'macabre waltz').
card_uid('macabre waltz'/'ORI', 'ORI:Macabre Waltz:macabre waltz').
card_rarity('macabre waltz'/'ORI', 'Common').
card_artist('macabre waltz'/'ORI', 'Jim Murray').
card_number('macabre waltz'/'ORI', '107').
card_flavor_text('macabre waltz'/'ORI', '\"All dead move to the hollow rhythm of necromancy.\"—Savra').
card_multiverse_id('macabre waltz'/'ORI', '398494').

card_in_set('mage-ring bully', 'ORI').
card_original_type('mage-ring bully'/'ORI', 'Creature — Human Warrior').
card_original_text('mage-ring bully'/'ORI', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)Mage-Ring Bully attacks each turn if able.').
card_first_print('mage-ring bully', 'ORI').
card_image_name('mage-ring bully'/'ORI', 'mage-ring bully').
card_uid('mage-ring bully'/'ORI', 'ORI:Mage-Ring Bully:mage-ring bully').
card_rarity('mage-ring bully'/'ORI', 'Common').
card_artist('mage-ring bully'/'ORI', 'Karl Kopinski').
card_number('mage-ring bully'/'ORI', '154').
card_flavor_text('mage-ring bully'/'ORI', '\"Don\'t be rude, Beleren. We just want to enjoy the view with you.\"').
card_multiverse_id('mage-ring bully'/'ORI', '398426').

card_in_set('mage-ring network', 'ORI').
card_original_type('mage-ring network'/'ORI', 'Land').
card_original_text('mage-ring network'/'ORI', '{T}: Add {1} to your mana pool.{1}, {T}: Put a storage counter on Mage-Ring Network.{T}, Remove X storage counters from Mage-Ring Network: Add {X} to your mana pool.').
card_first_print('mage-ring network', 'ORI').
card_image_name('mage-ring network'/'ORI', 'mage-ring network').
card_uid('mage-ring network'/'ORI', 'ORI:Mage-Ring Network:mage-ring network').
card_rarity('mage-ring network'/'ORI', 'Uncommon').
card_artist('mage-ring network'/'ORI', 'Jung Park').
card_number('mage-ring network'/'ORI', '249').
card_multiverse_id('mage-ring network'/'ORI', '398533').

card_in_set('mage-ring responder', 'ORI').
card_original_type('mage-ring responder'/'ORI', 'Artifact Creature — Golem').
card_original_text('mage-ring responder'/'ORI', 'Mage-Ring Responder doesn\'t untap during your untap step.{7}: Untap Mage-Ring Responder.Whenever Mage-Ring Responder attacks, it deals 7 damage to target creature defending player controls.').
card_first_print('mage-ring responder', 'ORI').
card_image_name('mage-ring responder'/'ORI', 'mage-ring responder').
card_uid('mage-ring responder'/'ORI', 'ORI:Mage-Ring Responder:mage-ring responder').
card_rarity('mage-ring responder'/'ORI', 'Rare').
card_artist('mage-ring responder'/'ORI', 'Adam Paquette').
card_number('mage-ring responder'/'ORI', '232').
card_multiverse_id('mage-ring responder'/'ORI', '398620').

card_in_set('magmatic insight', 'ORI').
card_original_type('magmatic insight'/'ORI', 'Sorcery').
card_original_text('magmatic insight'/'ORI', 'As an additional cost to cast Magmatic Insight, discard a land card.Draw two cards.').
card_first_print('magmatic insight', 'ORI').
card_image_name('magmatic insight'/'ORI', 'magmatic insight').
card_uid('magmatic insight'/'ORI', 'ORI:Magmatic Insight:magmatic insight').
card_rarity('magmatic insight'/'ORI', 'Uncommon').
card_artist('magmatic insight'/'ORI', 'Ryan Barger').
card_number('magmatic insight'/'ORI', '155').
card_flavor_text('magmatic insight'/'ORI', 'Chief among the tenets of Purphoros is that one must destroy in order to create.').
card_multiverse_id('magmatic insight'/'ORI', '398496').

card_in_set('mahamoti djinn', 'ORI').
card_original_type('mahamoti djinn'/'ORI', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_image_name('mahamoti djinn'/'ORI', 'mahamoti djinn').
card_uid('mahamoti djinn'/'ORI', 'ORI:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'ORI', 'Rare').
card_artist('mahamoti djinn'/'ORI', 'Greg Staples').
card_number('mahamoti djinn'/'ORI', '278').
card_flavor_text('mahamoti djinn'/'ORI', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'ORI', '401457').

card_in_set('malakir cullblade', 'ORI').
card_original_type('malakir cullblade'/'ORI', 'Creature — Vampire Warrior').
card_original_text('malakir cullblade'/'ORI', 'Whenever a creature an opponent controls dies, put a +1/+1 counter on Malakir Cullblade.').
card_first_print('malakir cullblade', 'ORI').
card_image_name('malakir cullblade'/'ORI', 'malakir cullblade').
card_uid('malakir cullblade'/'ORI', 'ORI:Malakir Cullblade:malakir cullblade').
card_rarity('malakir cullblade'/'ORI', 'Uncommon').
card_artist('malakir cullblade'/'ORI', 'Igor Kieryluk').
card_number('malakir cullblade'/'ORI', '108').
card_flavor_text('malakir cullblade'/'ORI', 'Each cut stains his blade with fresh blood that he samples to increase his strength.').
card_multiverse_id('malakir cullblade'/'ORI', '398556').

card_in_set('managorger hydra', 'ORI').
card_original_type('managorger hydra'/'ORI', 'Creature — Hydra').
card_original_text('managorger hydra'/'ORI', 'Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)Whenever a player casts a spell, put a +1/+1 counter on Managorger Hydra.').
card_first_print('managorger hydra', 'ORI').
card_image_name('managorger hydra'/'ORI', 'managorger hydra').
card_uid('managorger hydra'/'ORI', 'ORI:Managorger Hydra:managorger hydra').
card_rarity('managorger hydra'/'ORI', 'Rare').
card_artist('managorger hydra'/'ORI', 'Lucas Graciano').
card_number('managorger hydra'/'ORI', '186').
card_multiverse_id('managorger hydra'/'ORI', '398456').

card_in_set('mantle of webs', 'ORI').
card_original_type('mantle of webs'/'ORI', 'Enchantment — Aura').
card_original_text('mantle of webs'/'ORI', 'Enchant creatureEnchanted creature gets +1/+3 and has reach. (It can block creatures with flying.)').
card_first_print('mantle of webs', 'ORI').
card_image_name('mantle of webs'/'ORI', 'mantle of webs').
card_uid('mantle of webs'/'ORI', 'ORI:Mantle of Webs:mantle of webs').
card_rarity('mantle of webs'/'ORI', 'Common').
card_artist('mantle of webs'/'ORI', 'Mathias Kollros').
card_number('mantle of webs'/'ORI', '187').
card_flavor_text('mantle of webs'/'ORI', '\"Why does everything the Golgari touch end up sticky?\"—Arrester Lavinia, Tenth Precinct').
card_multiverse_id('mantle of webs'/'ORI', '398577').

card_in_set('maritime guard', 'ORI').
card_original_type('maritime guard'/'ORI', 'Creature — Merfolk Soldier').
card_original_text('maritime guard'/'ORI', '').
card_image_name('maritime guard'/'ORI', 'maritime guard').
card_uid('maritime guard'/'ORI', 'ORI:Maritime Guard:maritime guard').
card_rarity('maritime guard'/'ORI', 'Common').
card_artist('maritime guard'/'ORI', 'Allen Williams').
card_number('maritime guard'/'ORI', '63').
card_flavor_text('maritime guard'/'ORI', 'To stand as a guardian of the sea, one must possess the resolve of a mounting wave.').
card_multiverse_id('maritime guard'/'ORI', '398670').

card_in_set('meteorite', 'ORI').
card_original_type('meteorite'/'ORI', 'Artifact').
card_original_text('meteorite'/'ORI', 'When Meteorite enters the battlefield, it deals 2 damage to target creature or player.{T}: Add one mana of any color to your mana pool.').
card_image_name('meteorite'/'ORI', 'meteorite').
card_uid('meteorite'/'ORI', 'ORI:Meteorite:meteorite').
card_rarity('meteorite'/'ORI', 'Uncommon').
card_artist('meteorite'/'ORI', 'Scott Murphy').
card_number('meteorite'/'ORI', '233').
card_flavor_text('meteorite'/'ORI', '\"And if I\'m lying,\" he began . . .').
card_multiverse_id('meteorite'/'ORI', '398499').

card_in_set('might of the masses', 'ORI').
card_original_type('might of the masses'/'ORI', 'Instant').
card_original_text('might of the masses'/'ORI', 'Target creature gets +1/+1 until end of turn for each creature you control.').
card_image_name('might of the masses'/'ORI', 'might of the masses').
card_uid('might of the masses'/'ORI', 'ORI:Might of the Masses:might of the masses').
card_rarity('might of the masses'/'ORI', 'Common').
card_artist('might of the masses'/'ORI', 'Johann Bodin').
card_number('might of the masses'/'ORI', '188').
card_flavor_text('might of the masses'/'ORI', 'The Joraga elves never need ask a troll to leave their territory. They merely grant it their combined strength, and it can\'t resist embarking on a merry rampage.').
card_multiverse_id('might of the masses'/'ORI', '398662').

card_in_set('mighty leap', 'ORI').
card_original_type('mighty leap'/'ORI', 'Instant').
card_original_text('mighty leap'/'ORI', 'Target creature gets +2/+2 and gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)').
card_image_name('mighty leap'/'ORI', 'mighty leap').
card_uid('mighty leap'/'ORI', 'ORI:Mighty Leap:mighty leap').
card_rarity('mighty leap'/'ORI', 'Common').
card_artist('mighty leap'/'ORI', 'rk post').
card_number('mighty leap'/'ORI', '26').
card_flavor_text('mighty leap'/'ORI', '\"The southern fortress taken by invaders? Heh, sure . . . when elephants fly.\"—Brezard Skeinbow, captain of the guard').
card_multiverse_id('mighty leap'/'ORI', '398630').

card_in_set('mind rot', 'ORI').
card_original_type('mind rot'/'ORI', 'Sorcery').
card_original_text('mind rot'/'ORI', 'Target player discards two cards.').
card_image_name('mind rot'/'ORI', 'mind rot').
card_uid('mind rot'/'ORI', 'ORI:Mind Rot:mind rot').
card_rarity('mind rot'/'ORI', 'Common').
card_artist('mind rot'/'ORI', 'Steve Luke').
card_number('mind rot'/'ORI', '281').
card_flavor_text('mind rot'/'ORI', '\"It saddens me to lose a source of inspiration. This one seemed especially promising.\"\n—Ashiok').
card_multiverse_id('mind rot'/'ORI', '401460').

card_in_set('mizzium meddler', 'ORI').
card_original_type('mizzium meddler'/'ORI', 'Creature — Vedalken Wizard').
card_original_text('mizzium meddler'/'ORI', 'Flash (You may cast this spell any time you could cast an instant.)When Mizzium Meddler enters the battlefield, you may change a target of target spell or ability to Mizzium Meddler.').
card_first_print('mizzium meddler', 'ORI').
card_image_name('mizzium meddler'/'ORI', 'mizzium meddler').
card_uid('mizzium meddler'/'ORI', 'ORI:Mizzium Meddler:mizzium meddler').
card_rarity('mizzium meddler'/'ORI', 'Rare').
card_artist('mizzium meddler'/'ORI', 'Johann Bodin').
card_number('mizzium meddler'/'ORI', '64').
card_flavor_text('mizzium meddler'/'ORI', 'Energy surges in Izzet labs are opportunities for experimentation.').
card_multiverse_id('mizzium meddler'/'ORI', '398596').

card_in_set('molten vortex', 'ORI').
card_original_type('molten vortex'/'ORI', 'Enchantment').
card_original_text('molten vortex'/'ORI', '{R}, Discard a land card: Molten Vortex deals 2 damage to target creature or player.').
card_first_print('molten vortex', 'ORI').
card_image_name('molten vortex'/'ORI', 'molten vortex').
card_uid('molten vortex'/'ORI', 'ORI:Molten Vortex:molten vortex').
card_rarity('molten vortex'/'ORI', 'Rare').
card_artist('molten vortex'/'ORI', 'Philip Straub').
card_number('molten vortex'/'ORI', '156').
card_flavor_text('molten vortex'/'ORI', 'If you can\'t take the heat . . . well, that\'s going to be a problem.').
card_multiverse_id('molten vortex'/'ORI', '398649').

card_in_set('mountain', 'ORI').
card_original_type('mountain'/'ORI', 'Basic Land — Mountain').
card_original_text('mountain'/'ORI', 'R').
card_image_name('mountain'/'ORI', 'mountain1').
card_uid('mountain'/'ORI', 'ORI:Mountain:mountain1').
card_rarity('mountain'/'ORI', 'Basic Land').
card_artist('mountain'/'ORI', 'Noah Bradley').
card_number('mountain'/'ORI', '265').
card_multiverse_id('mountain'/'ORI', '398490').

card_in_set('mountain', 'ORI').
card_original_type('mountain'/'ORI', 'Basic Land — Mountain').
card_original_text('mountain'/'ORI', 'R').
card_image_name('mountain'/'ORI', 'mountain2').
card_uid('mountain'/'ORI', 'ORI:Mountain:mountain2').
card_rarity('mountain'/'ORI', 'Basic Land').
card_artist('mountain'/'ORI', 'Noah Bradley').
card_number('mountain'/'ORI', '266').
card_multiverse_id('mountain'/'ORI', '398510').

card_in_set('mountain', 'ORI').
card_original_type('mountain'/'ORI', 'Basic Land — Mountain').
card_original_text('mountain'/'ORI', 'R').
card_image_name('mountain'/'ORI', 'mountain3').
card_uid('mountain'/'ORI', 'ORI:Mountain:mountain3').
card_rarity('mountain'/'ORI', 'Basic Land').
card_artist('mountain'/'ORI', 'Sam Burley').
card_number('mountain'/'ORI', '267').
card_multiverse_id('mountain'/'ORI', '398412').

card_in_set('mountain', 'ORI').
card_original_type('mountain'/'ORI', 'Basic Land — Mountain').
card_original_text('mountain'/'ORI', 'R').
card_image_name('mountain'/'ORI', 'mountain4').
card_uid('mountain'/'ORI', 'ORI:Mountain:mountain4').
card_rarity('mountain'/'ORI', 'Basic Land').
card_artist('mountain'/'ORI', 'Sam Burley').
card_number('mountain'/'ORI', '268').
card_multiverse_id('mountain'/'ORI', '398425').

card_in_set('murder investigation', 'ORI').
card_original_type('murder investigation'/'ORI', 'Enchantment — Aura').
card_original_text('murder investigation'/'ORI', 'Enchant creature you controlWhen enchanted creature dies, put X 1/1 white Soldier creature tokens onto the battlefield, where X is its power.').
card_image_name('murder investigation'/'ORI', 'murder investigation').
card_uid('murder investigation'/'ORI', 'ORI:Murder Investigation:murder investigation').
card_rarity('murder investigation'/'ORI', 'Uncommon').
card_artist('murder investigation'/'ORI', 'Igor Kieryluk').
card_number('murder investigation'/'ORI', '27').
card_flavor_text('murder investigation'/'ORI', '\"Every death has a web of consequences. Our job is to find the spider.\"').
card_multiverse_id('murder investigation'/'ORI', '398641').

card_in_set('nantuko husk', 'ORI').
card_original_type('nantuko husk'/'ORI', 'Creature — Zombie Insect').
card_original_text('nantuko husk'/'ORI', 'Sacrifice a creature: Nantuko Husk gets +2/+2 until end of turn.').
card_image_name('nantuko husk'/'ORI', 'nantuko husk').
card_uid('nantuko husk'/'ORI', 'ORI:Nantuko Husk:nantuko husk').
card_rarity('nantuko husk'/'ORI', 'Common').
card_artist('nantuko husk'/'ORI', 'Carl Critchlow').
card_number('nantuko husk'/'ORI', '109').
card_flavor_text('nantuko husk'/'ORI', '\"The soul sheds light, and death is its shadow. When the light dims, life and death embrace.\"—Nantuko teaching').
card_multiverse_id('nantuko husk'/'ORI', '398440').

card_in_set('necromantic summons', 'ORI').
card_original_type('necromantic summons'/'ORI', 'Sorcery').
card_original_text('necromantic summons'/'ORI', 'Put target creature card from a graveyard onto the battlefield under your control.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, that creature enters the battlefield with two additional +1/+1 counters on it.').
card_first_print('necromantic summons', 'ORI').
card_image_name('necromantic summons'/'ORI', 'necromantic summons').
card_uid('necromantic summons'/'ORI', 'ORI:Necromantic Summons:necromantic summons').
card_rarity('necromantic summons'/'ORI', 'Uncommon').
card_artist('necromantic summons'/'ORI', 'Ryan Yee').
card_number('necromantic summons'/'ORI', '110').
card_multiverse_id('necromantic summons'/'ORI', '398491').

card_in_set('negate', 'ORI').
card_original_type('negate'/'ORI', 'Instant').
card_original_text('negate'/'ORI', 'Counter target noncreature spell.').
card_image_name('negate'/'ORI', 'negate').
card_uid('negate'/'ORI', 'ORI:Negate:negate').
card_rarity('negate'/'ORI', 'Common').
card_artist('negate'/'ORI', 'Jeremy Jarvis').
card_number('negate'/'ORI', '65').
card_flavor_text('negate'/'ORI', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'ORI', '398629').

card_in_set('nightmare', 'ORI').
card_original_type('nightmare'/'ORI', 'Creature — Nightmare Horse').
card_original_text('nightmare'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('nightmare'/'ORI', 'nightmare').
card_uid('nightmare'/'ORI', 'ORI:Nightmare:nightmare').
card_rarity('nightmare'/'ORI', 'Rare').
card_artist('nightmare'/'ORI', 'Vance Kovacs').
card_number('nightmare'/'ORI', '282').
card_flavor_text('nightmare'/'ORI', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'ORI', '401461').

card_in_set('nightsnare', 'ORI').
card_original_type('nightsnare'/'ORI', 'Sorcery').
card_original_text('nightsnare'/'ORI', 'Target opponent reveals his or her hand. You may choose a nonland card from it. If you do, that player discards that card. If you don\'t, that player discards two cards.').
card_first_print('nightsnare', 'ORI').
card_image_name('nightsnare'/'ORI', 'nightsnare').
card_uid('nightsnare'/'ORI', 'ORI:Nightsnare:nightsnare').
card_rarity('nightsnare'/'ORI', 'Common').
card_artist('nightsnare'/'ORI', 'Min Yum').
card_number('nightsnare'/'ORI', '111').
card_flavor_text('nightsnare'/'ORI', 'Ravnica would be safer if mere coin was all the Dimir could steal.').
card_multiverse_id('nightsnare'/'ORI', '398562').

card_in_set('nissa\'s pilgrimage', 'ORI').
card_original_type('nissa\'s pilgrimage'/'ORI', 'Sorcery').
card_original_text('nissa\'s pilgrimage'/'ORI', 'Search your library for up to two basic Forest cards, reveal those cards, and put one onto the battlefield tapped and the rest into your hand. Then shuffle your library.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, search your library for up to three basic Forest cards instead of two.').
card_first_print('nissa\'s pilgrimage', 'ORI').
card_image_name('nissa\'s pilgrimage'/'ORI', 'nissa\'s pilgrimage').
card_uid('nissa\'s pilgrimage'/'ORI', 'ORI:Nissa\'s Pilgrimage:nissa\'s pilgrimage').
card_rarity('nissa\'s pilgrimage'/'ORI', 'Common').
card_artist('nissa\'s pilgrimage'/'ORI', 'Matt Stewart').
card_number('nissa\'s pilgrimage'/'ORI', '190').
card_multiverse_id('nissa\'s pilgrimage'/'ORI', '398593').

card_in_set('nissa\'s revelation', 'ORI').
card_original_type('nissa\'s revelation'/'ORI', 'Sorcery').
card_original_text('nissa\'s revelation'/'ORI', 'Scry 5, then reveal the top card of your library. If it\'s a creature card, you draw cards equal to its power and you gain life equal to its toughness.').
card_first_print('nissa\'s revelation', 'ORI').
card_image_name('nissa\'s revelation'/'ORI', 'nissa\'s revelation').
card_uid('nissa\'s revelation'/'ORI', 'ORI:Nissa\'s Revelation:nissa\'s revelation').
card_rarity('nissa\'s revelation'/'ORI', 'Rare').
card_artist('nissa\'s revelation'/'ORI', 'Izzy').
card_number('nissa\'s revelation'/'ORI', '191').
card_flavor_text('nissa\'s revelation'/'ORI', 'The evil inside the mountain pulsed at Nissa in a shock wave of madness.').
card_multiverse_id('nissa\'s revelation'/'ORI', '398506').

card_in_set('nissa, sage animist', 'ORI').
card_original_type('nissa, sage animist'/'ORI', 'Planeswalker — Nissa').
card_original_text('nissa, sage animist'/'ORI', '+1: Reveal the top card of your library. If it\'s a land card, put it onto the battlefield. Otherwise, put it into your hand.−2: Put a legendary 4/4 green Elemental creature token named Ashaya, the Awoken World onto the battlefield.−7: Untap up to six target lands. They become 6/6 Elemental creatures. They\'re still lands.').
card_first_print('nissa, sage animist', 'ORI').
card_image_name('nissa, sage animist'/'ORI', 'nissa, sage animist').
card_uid('nissa, sage animist'/'ORI', 'ORI:Nissa, Sage Animist:nissa, sage animist').
card_rarity('nissa, sage animist'/'ORI', 'Mythic Rare').
card_artist('nissa, sage animist'/'ORI', 'Wesley Burt').
card_number('nissa, sage animist'/'ORI', '189b').
card_multiverse_id('nissa, sage animist'/'ORI', '398432').

card_in_set('nissa, vastwood seer', 'ORI').
card_original_type('nissa, vastwood seer'/'ORI', 'Legendary Creature — Elf Scout').
card_original_text('nissa, vastwood seer'/'ORI', 'When Nissa, Vastwood Seer enters the battlefield, you may search your library for a basic Forest card, reveal it, put it into your hand, then shuffle your library.Whenever a land enters the battlefield under your control, if you control seven or more lands, exile Nissa, then return her to the battlefield transformed under her owner\'s control.').
card_first_print('nissa, vastwood seer', 'ORI').
card_image_name('nissa, vastwood seer'/'ORI', 'nissa, vastwood seer').
card_uid('nissa, vastwood seer'/'ORI', 'ORI:Nissa, Vastwood Seer:nissa, vastwood seer').
card_rarity('nissa, vastwood seer'/'ORI', 'Mythic Rare').
card_artist('nissa, vastwood seer'/'ORI', 'Wesley Burt').
card_number('nissa, vastwood seer'/'ORI', '189a').
card_multiverse_id('nissa, vastwood seer'/'ORI', '398438').

card_in_set('nivix barrier', 'ORI').
card_original_type('nivix barrier'/'ORI', 'Creature — Illusion Wall').
card_original_text('nivix barrier'/'ORI', 'Flash (You may cast this spell any time you could cast an instant.)Defender (This creature can\'t attack.)When Nivix Barrier enters the battlefield, target attacking creature gets -4/-0 until end of turn.').
card_first_print('nivix barrier', 'ORI').
card_image_name('nivix barrier'/'ORI', 'nivix barrier').
card_uid('nivix barrier'/'ORI', 'ORI:Nivix Barrier:nivix barrier').
card_rarity('nivix barrier'/'ORI', 'Common').
card_artist('nivix barrier'/'ORI', 'Mathias Kollros').
card_number('nivix barrier'/'ORI', '66').
card_multiverse_id('nivix barrier'/'ORI', '398465').

card_in_set('orbs of warding', 'ORI').
card_original_type('orbs of warding'/'ORI', 'Artifact').
card_original_text('orbs of warding'/'ORI', 'You have hexproof. (You can\'t be the target of spells or abilities your opponents control.)If a creature would deal damage to you, prevent 1 of that damage.').
card_first_print('orbs of warding', 'ORI').
card_image_name('orbs of warding'/'ORI', 'orbs of warding').
card_uid('orbs of warding'/'ORI', 'ORI:Orbs of Warding:orbs of warding').
card_rarity('orbs of warding'/'ORI', 'Rare').
card_artist('orbs of warding'/'ORI', 'Johann Bodin').
card_number('orbs of warding'/'ORI', '234').
card_flavor_text('orbs of warding'/'ORI', 'One orb to guard the body, one to protect the mind, and one to shield the soul.').
card_multiverse_id('orbs of warding'/'ORI', '398551').

card_in_set('orchard spirit', 'ORI').
card_original_type('orchard spirit'/'ORI', 'Creature — Spirit').
card_original_text('orchard spirit'/'ORI', 'Orchard Spirit can\'t be blocked except by creatures with flying or reach.').
card_image_name('orchard spirit'/'ORI', 'orchard spirit').
card_uid('orchard spirit'/'ORI', 'ORI:Orchard Spirit:orchard spirit').
card_rarity('orchard spirit'/'ORI', 'Common').
card_artist('orchard spirit'/'ORI', 'Howard Lyon').
card_number('orchard spirit'/'ORI', '192').
card_flavor_text('orchard spirit'/'ORI', '\"Pick not the rotten fruit, but neither touch the best. Leave those as an offering for our unseen guests.\"—Radwick, farmer of Gatstaf').
card_multiverse_id('orchard spirit'/'ORI', '398644').

card_in_set('outland colossus', 'ORI').
card_original_type('outland colossus'/'ORI', 'Creature — Giant').
card_original_text('outland colossus'/'ORI', 'Renown 6 (When this creature deals combat damage to a player, if it isn\'t renowned, put six +1/+1 counters on it and it becomes renowned.)Outland Colossus can\'t be blocked by more than one creature.').
card_first_print('outland colossus', 'ORI').
card_image_name('outland colossus'/'ORI', 'outland colossus').
card_uid('outland colossus'/'ORI', 'ORI:Outland Colossus:outland colossus').
card_rarity('outland colossus'/'ORI', 'Rare').
card_artist('outland colossus'/'ORI', 'Ryan Pancoast').
card_number('outland colossus'/'ORI', '193').
card_multiverse_id('outland colossus'/'ORI', '398668').

card_in_set('patron of the valiant', 'ORI').
card_original_type('patron of the valiant'/'ORI', 'Creature — Angel').
card_original_text('patron of the valiant'/'ORI', 'FlyingWhen Patron of the Valiant enters the battlefield, put a +1/+1 counter on each creature you control with a +1/+1 counter on it.').
card_first_print('patron of the valiant', 'ORI').
card_image_name('patron of the valiant'/'ORI', 'patron of the valiant').
card_uid('patron of the valiant'/'ORI', 'ORI:Patron of the Valiant:patron of the valiant').
card_rarity('patron of the valiant'/'ORI', 'Uncommon').
card_artist('patron of the valiant'/'ORI', 'Steve Argyle').
card_number('patron of the valiant'/'ORI', '28').
card_flavor_text('patron of the valiant'/'ORI', '\"Bant\'s most gallant knights wear my sigil, and for that I am humbled.\"').
card_multiverse_id('patron of the valiant'/'ORI', '398631').

card_in_set('pharika\'s disciple', 'ORI').
card_original_type('pharika\'s disciple'/'ORI', 'Creature — Centaur Warrior').
card_original_text('pharika\'s disciple'/'ORI', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_first_print('pharika\'s disciple', 'ORI').
card_image_name('pharika\'s disciple'/'ORI', 'pharika\'s disciple').
card_uid('pharika\'s disciple'/'ORI', 'ORI:Pharika\'s Disciple:pharika\'s disciple').
card_rarity('pharika\'s disciple'/'ORI', 'Common').
card_artist('pharika\'s disciple'/'ORI', 'Karl Kopinski').
card_number('pharika\'s disciple'/'ORI', '194').
card_multiverse_id('pharika\'s disciple'/'ORI', '398531').

card_in_set('pia and kiran nalaar', 'ORI').
card_original_type('pia and kiran nalaar'/'ORI', 'Legendary Creature — Human Artificer').
card_original_text('pia and kiran nalaar'/'ORI', 'When Pia and Kiran Nalaar enters the battlefield, put two 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.{2}{R}, Sacrifice an artifact: Pia and Kiran Nalaar deals 2 damage to target creature or player.').
card_first_print('pia and kiran nalaar', 'ORI').
card_image_name('pia and kiran nalaar'/'ORI', 'pia and kiran nalaar').
card_uid('pia and kiran nalaar'/'ORI', 'ORI:Pia and Kiran Nalaar:pia and kiran nalaar').
card_rarity('pia and kiran nalaar'/'ORI', 'Rare').
card_artist('pia and kiran nalaar'/'ORI', 'Eric Deschamps').
card_number('pia and kiran nalaar'/'ORI', '157').
card_multiverse_id('pia and kiran nalaar'/'ORI', '398453').

card_in_set('plains', 'ORI').
card_original_type('plains'/'ORI', 'Basic Land — Plains').
card_original_text('plains'/'ORI', 'W').
card_image_name('plains'/'ORI', 'plains1').
card_uid('plains'/'ORI', 'ORI:Plains:plains1').
card_rarity('plains'/'ORI', 'Basic Land').
card_artist('plains'/'ORI', 'Michael Komarck').
card_number('plains'/'ORI', '253').
card_multiverse_id('plains'/'ORI', '398534').

card_in_set('plains', 'ORI').
card_original_type('plains'/'ORI', 'Basic Land — Plains').
card_original_text('plains'/'ORI', 'W').
card_image_name('plains'/'ORI', 'plains2').
card_uid('plains'/'ORI', 'ORI:Plains:plains2').
card_rarity('plains'/'ORI', 'Basic Land').
card_artist('plains'/'ORI', 'Michael Komarck').
card_number('plains'/'ORI', '254').
card_multiverse_id('plains'/'ORI', '398675').

card_in_set('plains', 'ORI').
card_original_type('plains'/'ORI', 'Basic Land — Plains').
card_original_text('plains'/'ORI', 'W').
card_image_name('plains'/'ORI', 'plains3').
card_uid('plains'/'ORI', 'ORI:Plains:plains3').
card_rarity('plains'/'ORI', 'Basic Land').
card_artist('plains'/'ORI', 'Adam Paquette').
card_number('plains'/'ORI', '255').
card_multiverse_id('plains'/'ORI', '398550').

card_in_set('plains', 'ORI').
card_original_type('plains'/'ORI', 'Basic Land — Plains').
card_original_text('plains'/'ORI', 'W').
card_image_name('plains'/'ORI', 'plains4').
card_uid('plains'/'ORI', 'ORI:Plains:plains4').
card_rarity('plains'/'ORI', 'Basic Land').
card_artist('plains'/'ORI', 'Raoul Vitale').
card_number('plains'/'ORI', '256').
card_multiverse_id('plains'/'ORI', '398541').

card_in_set('plummet', 'ORI').
card_original_type('plummet'/'ORI', 'Instant').
card_original_text('plummet'/'ORI', 'Destroy target creature with flying.').
card_image_name('plummet'/'ORI', 'plummet').
card_uid('plummet'/'ORI', 'ORI:Plummet:plummet').
card_rarity('plummet'/'ORI', 'Common').
card_artist('plummet'/'ORI', 'Pete Venters').
card_number('plummet'/'ORI', '286').
card_flavor_text('plummet'/'ORI', '\"Let nothing own the skies but the wind.\"\n—Dejara, Giltwood druid').
card_multiverse_id('plummet'/'ORI', '401465').

card_in_set('possessed skaab', 'ORI').
card_original_type('possessed skaab'/'ORI', 'Creature — Zombie').
card_original_text('possessed skaab'/'ORI', 'When Possessed Skaab enters the battlefield, return target instant, sorcery, or creature card from your graveyard to your hand.If Possessed Skaab would die, exile it instead.').
card_first_print('possessed skaab', 'ORI').
card_image_name('possessed skaab'/'ORI', 'possessed skaab').
card_uid('possessed skaab'/'ORI', 'ORI:Possessed Skaab:possessed skaab').
card_rarity('possessed skaab'/'ORI', 'Uncommon').
card_artist('possessed skaab'/'ORI', 'John Stanko').
card_number('possessed skaab'/'ORI', '215').
card_flavor_text('possessed skaab'/'ORI', 'A stitcher\'s attempt at a skaab with a soul.').
card_multiverse_id('possessed skaab'/'ORI', '398657').

card_in_set('prickleboar', 'ORI').
card_original_type('prickleboar'/'ORI', 'Creature — Boar').
card_original_text('prickleboar'/'ORI', 'As long as it\'s your turn, Prickleboar gets +2/+0 and has first strike. (It deals combat damage before creatures without first strike.)').
card_first_print('prickleboar', 'ORI').
card_image_name('prickleboar'/'ORI', 'prickleboar').
card_uid('prickleboar'/'ORI', 'ORI:Prickleboar:prickleboar').
card_rarity('prickleboar'/'ORI', 'Common').
card_artist('prickleboar'/'ORI', 'Jesper Ejsing').
card_number('prickleboar'/'ORI', '158').
card_flavor_text('prickleboar'/'ORI', '\"Canyons in the Phoberos badlands are essentially gates to the Underworld.\"—Mirinthes, Akroan captain').
card_multiverse_id('prickleboar'/'ORI', '398449').

card_in_set('priest of the blood rite', 'ORI').
card_original_type('priest of the blood rite'/'ORI', 'Creature — Human Cleric').
card_original_text('priest of the blood rite'/'ORI', 'When Priest of the Blood Rite enters the battlefield, put a 5/5 black Demon creature token with flying onto the battlefield.At the beginning of your upkeep, you lose 2 life.').
card_first_print('priest of the blood rite', 'ORI').
card_image_name('priest of the blood rite'/'ORI', 'priest of the blood rite').
card_uid('priest of the blood rite'/'ORI', 'ORI:Priest of the Blood Rite:priest of the blood rite').
card_rarity('priest of the blood rite'/'ORI', 'Rare').
card_artist('priest of the blood rite'/'ORI', 'David Palumbo').
card_number('priest of the blood rite'/'ORI', '112').
card_flavor_text('priest of the blood rite'/'ORI', 'Some will sacrifice everything for but a taste of power.').
card_multiverse_id('priest of the blood rite'/'ORI', '398639').

card_in_set('prism ring', 'ORI').
card_original_type('prism ring'/'ORI', 'Artifact').
card_original_text('prism ring'/'ORI', 'As Prism Ring enters the battlefield, choose a color.Whenever you cast a spell of the chosen color, you gain 1 life.').
card_first_print('prism ring', 'ORI').
card_image_name('prism ring'/'ORI', 'prism ring').
card_uid('prism ring'/'ORI', 'ORI:Prism Ring:prism ring').
card_rarity('prism ring'/'ORI', 'Uncommon').
card_artist('prism ring'/'ORI', 'Daniel Ljunggren').
card_number('prism ring'/'ORI', '235').
card_flavor_text('prism ring'/'ORI', 'Wear it on your dominant hand and it will follow your lead.').
card_multiverse_id('prism ring'/'ORI', '398646').

card_in_set('prized unicorn', 'ORI').
card_original_type('prized unicorn'/'ORI', 'Creature — Unicorn').
card_original_text('prized unicorn'/'ORI', 'All creatures able to block Prized Unicorn do so.').
card_image_name('prized unicorn'/'ORI', 'prized unicorn').
card_uid('prized unicorn'/'ORI', 'ORI:Prized Unicorn:prized unicorn').
card_rarity('prized unicorn'/'ORI', 'Uncommon').
card_artist('prized unicorn'/'ORI', 'Sam Wood').
card_number('prized unicorn'/'ORI', '287').
card_flavor_text('prized unicorn'/'ORI', '\"The desire for its magic horn inspires such bloodthirsty greed that all who see the unicorn will kill to possess it.\"\n—Dionus, elvish archdruid').
card_multiverse_id('prized unicorn'/'ORI', '401466').

card_in_set('psychic rebuttal', 'ORI').
card_original_type('psychic rebuttal'/'ORI', 'Instant').
card_original_text('psychic rebuttal'/'ORI', 'Counter target instant or sorcery spell that targets you.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, you may copy the spell countered this way. You may choose new targets for the copy.').
card_first_print('psychic rebuttal', 'ORI').
card_image_name('psychic rebuttal'/'ORI', 'psychic rebuttal').
card_uid('psychic rebuttal'/'ORI', 'ORI:Psychic Rebuttal:psychic rebuttal').
card_rarity('psychic rebuttal'/'ORI', 'Uncommon').
card_artist('psychic rebuttal'/'ORI', 'Todd Lockwood').
card_number('psychic rebuttal'/'ORI', '67').
card_flavor_text('psychic rebuttal'/'ORI', '\"I know what you\'re thinking,\" Jace whispered in the mage\'s mind. \"It\'s not going to work.\"').
card_multiverse_id('psychic rebuttal'/'ORI', '398479').

card_in_set('pyromancer\'s goggles', 'ORI').
card_original_type('pyromancer\'s goggles'/'ORI', 'Legendary Artifact').
card_original_text('pyromancer\'s goggles'/'ORI', '{T}: Add {R} to your mana pool. When that mana is spent to cast a red instant or sorcery spell, copy that spell and you may choose new targets for the copy.').
card_first_print('pyromancer\'s goggles', 'ORI').
card_image_name('pyromancer\'s goggles'/'ORI', 'pyromancer\'s goggles').
card_uid('pyromancer\'s goggles'/'ORI', 'ORI:Pyromancer\'s Goggles:pyromancer\'s goggles').
card_rarity('pyromancer\'s goggles'/'ORI', 'Mythic Rare').
card_artist('pyromancer\'s goggles'/'ORI', 'James Paick').
card_number('pyromancer\'s goggles'/'ORI', '236').
card_flavor_text('pyromancer\'s goggles'/'ORI', '\"I hope to meet Jaya Ballard someday. I think we\'d get along.\"—Chandra Nalaar').
card_multiverse_id('pyromancer\'s goggles'/'ORI', '398427').

card_in_set('rabid bloodsucker', 'ORI').
card_original_type('rabid bloodsucker'/'ORI', 'Creature — Vampire').
card_original_text('rabid bloodsucker'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)When Rabid Bloodsucker enters the battlefield, each player loses 2 life.').
card_first_print('rabid bloodsucker', 'ORI').
card_image_name('rabid bloodsucker'/'ORI', 'rabid bloodsucker').
card_uid('rabid bloodsucker'/'ORI', 'ORI:Rabid Bloodsucker:rabid bloodsucker').
card_rarity('rabid bloodsucker'/'ORI', 'Common').
card_artist('rabid bloodsucker'/'ORI', 'Seb McKinnon').
card_number('rabid bloodsucker'/'ORI', '113').
card_flavor_text('rabid bloodsucker'/'ORI', 'She haunts the streets, wings spread wide, with carrion on her breath and blood in her eyes.').
card_multiverse_id('rabid bloodsucker'/'ORI', '398557').

card_in_set('ramroller', 'ORI').
card_original_type('ramroller'/'ORI', 'Artifact Creature — Juggernaut').
card_original_text('ramroller'/'ORI', 'Ramroller attacks each turn if able.Ramroller gets +2/+0 as long as you control another artifact.').
card_first_print('ramroller', 'ORI').
card_image_name('ramroller'/'ORI', 'ramroller').
card_uid('ramroller'/'ORI', 'ORI:Ramroller:ramroller').
card_rarity('ramroller'/'ORI', 'Uncommon').
card_artist('ramroller'/'ORI', 'Craig J Spearing').
card_number('ramroller'/'ORI', '237').
card_flavor_text('ramroller'/'ORI', 'As unstoppable as it is unsteerable.').
card_multiverse_id('ramroller'/'ORI', '398467').

card_in_set('ravaging blaze', 'ORI').
card_original_type('ravaging blaze'/'ORI', 'Instant').
card_original_text('ravaging blaze'/'ORI', 'Ravaging Blaze deals X damage to target creature. Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, Ravaging Blaze also deals X damage to that creature\'s controller.').
card_first_print('ravaging blaze', 'ORI').
card_image_name('ravaging blaze'/'ORI', 'ravaging blaze').
card_uid('ravaging blaze'/'ORI', 'ORI:Ravaging Blaze:ravaging blaze').
card_rarity('ravaging blaze'/'ORI', 'Uncommon').
card_artist('ravaging blaze'/'ORI', 'Aleksi Briclot').
card_number('ravaging blaze'/'ORI', '159').
card_flavor_text('ravaging blaze'/'ORI', 'In time, not even the fire monks could keep up with Chandra.').
card_multiverse_id('ravaging blaze'/'ORI', '398601').

card_in_set('read the bones', 'ORI').
card_original_type('read the bones'/'ORI', 'Sorcery').
card_original_text('read the bones'/'ORI', 'Scry 2, then draw two cards. You lose 2 life. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('read the bones'/'ORI', 'read the bones').
card_uid('read the bones'/'ORI', 'ORI:Read the Bones:read the bones').
card_rarity('read the bones'/'ORI', 'Common').
card_artist('read the bones'/'ORI', 'Lars Grant-West').
card_number('read the bones'/'ORI', '114').
card_flavor_text('read the bones'/'ORI', 'The dead know lessons the living haven\'t learned.').
card_multiverse_id('read the bones'/'ORI', '398637').

card_in_set('reave soul', 'ORI').
card_original_type('reave soul'/'ORI', 'Sorcery').
card_original_text('reave soul'/'ORI', 'Destroy target creature with power 3 or less.').
card_first_print('reave soul', 'ORI').
card_image_name('reave soul'/'ORI', 'reave soul').
card_uid('reave soul'/'ORI', 'ORI:Reave Soul:reave soul').
card_rarity('reave soul'/'ORI', 'Common').
card_artist('reave soul'/'ORI', 'David Palumbo').
card_number('reave soul'/'ORI', '115').
card_flavor_text('reave soul'/'ORI', '\"I was not convinced you had a soul until I saw it for myself.\"—Liliana Vess').
card_multiverse_id('reave soul'/'ORI', '398591').

card_in_set('reclaim', 'ORI').
card_original_type('reclaim'/'ORI', 'Instant').
card_original_text('reclaim'/'ORI', 'Put target card from your graveyard on top of your library.').
card_image_name('reclaim'/'ORI', 'reclaim').
card_uid('reclaim'/'ORI', 'ORI:Reclaim:reclaim').
card_rarity('reclaim'/'ORI', 'Common').
card_artist('reclaim'/'ORI', 'Andrew Robinson').
card_number('reclaim'/'ORI', '195').
card_flavor_text('reclaim'/'ORI', 'The wise pay as much attention to what they throw away as to what they keep.').
card_multiverse_id('reclaim'/'ORI', '398543').

card_in_set('reclusive artificer', 'ORI').
card_original_type('reclusive artificer'/'ORI', 'Creature — Human Artificer').
card_original_text('reclusive artificer'/'ORI', 'Haste (This creature can attack and {T} as soon as it comes under your control.)When Reclusive Artificer enters the battlefield, you may have it deal damage to target creature equal to the number of artifacts you control.').
card_first_print('reclusive artificer', 'ORI').
card_image_name('reclusive artificer'/'ORI', 'reclusive artificer').
card_uid('reclusive artificer'/'ORI', 'ORI:Reclusive Artificer:reclusive artificer').
card_rarity('reclusive artificer'/'ORI', 'Uncommon').
card_artist('reclusive artificer'/'ORI', 'Cynthia Sheppard').
card_number('reclusive artificer'/'ORI', '216').
card_multiverse_id('reclusive artificer'/'ORI', '398521').

card_in_set('relic seeker', 'ORI').
card_original_type('relic seeker'/'ORI', 'Creature — Human Soldier').
card_original_text('relic seeker'/'ORI', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)When Relic Seeker becomes renowned, you may search your library for an Equipment card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('relic seeker', 'ORI').
card_image_name('relic seeker'/'ORI', 'relic seeker').
card_uid('relic seeker'/'ORI', 'ORI:Relic Seeker:relic seeker').
card_rarity('relic seeker'/'ORI', 'Rare').
card_artist('relic seeker'/'ORI', 'Volkan Baga').
card_number('relic seeker'/'ORI', '29').
card_multiverse_id('relic seeker'/'ORI', '398476').

card_in_set('returned centaur', 'ORI').
card_original_type('returned centaur'/'ORI', 'Creature — Zombie Centaur').
card_original_text('returned centaur'/'ORI', 'When Returned Centaur enters the battlefield, target player puts the top four cards of his or her library into his or her graveyard.').
card_image_name('returned centaur'/'ORI', 'returned centaur').
card_uid('returned centaur'/'ORI', 'ORI:Returned Centaur:returned centaur').
card_rarity('returned centaur'/'ORI', 'Common').
card_artist('returned centaur'/'ORI', 'Lucas Graciano').
card_number('returned centaur'/'ORI', '116').
card_flavor_text('returned centaur'/'ORI', 'Driven away by his living kin, he wanders mourning through the wilderness, seeking the dead city of Asphodel.').
card_multiverse_id('returned centaur'/'ORI', '398468').

card_in_set('revenant', 'ORI').
card_original_type('revenant'/'ORI', 'Creature — Spirit').
card_original_text('revenant'/'ORI', 'FlyingRevenant\'s power and toughness are each equal to the number of creature cards in your graveyard.').
card_image_name('revenant'/'ORI', 'revenant').
card_uid('revenant'/'ORI', 'ORI:Revenant:revenant').
card_rarity('revenant'/'ORI', 'Uncommon').
card_artist('revenant'/'ORI', 'Terese Nielsen').
card_number('revenant'/'ORI', '117').
card_flavor_text('revenant'/'ORI', 'Born from the slaughter of an entire army, it bears the wrath of a hundred souls.').
card_multiverse_id('revenant'/'ORI', '398575').

card_in_set('rhox maulers', 'ORI').
card_original_type('rhox maulers'/'ORI', 'Creature — Rhino Soldier').
card_original_text('rhox maulers'/'ORI', 'Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)Renown 2 (When this creature deals combat damage to a player, if it isn\'t renowned, put two +1/+1 counters on it and it becomes renowned.)').
card_first_print('rhox maulers', 'ORI').
card_image_name('rhox maulers'/'ORI', 'rhox maulers').
card_uid('rhox maulers'/'ORI', 'ORI:Rhox Maulers:rhox maulers').
card_rarity('rhox maulers'/'ORI', 'Common').
card_artist('rhox maulers'/'ORI', 'Zoltan Boros').
card_number('rhox maulers'/'ORI', '196').
card_multiverse_id('rhox maulers'/'ORI', '398663').

card_in_set('ringwarden owl', 'ORI').
card_original_type('ringwarden owl'/'ORI', 'Creature — Bird').
card_original_text('ringwarden owl'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_first_print('ringwarden owl', 'ORI').
card_image_name('ringwarden owl'/'ORI', 'ringwarden owl').
card_uid('ringwarden owl'/'ORI', 'ORI:Ringwarden Owl:ringwarden owl').
card_rarity('ringwarden owl'/'ORI', 'Common').
card_artist('ringwarden owl'/'ORI', 'Titus Lunter').
card_number('ringwarden owl'/'ORI', '68').
card_flavor_text('ringwarden owl'/'ORI', 'The owls learn of mana from the mages who know it best.').
card_multiverse_id('ringwarden owl'/'ORI', '398464').

card_in_set('rogue\'s passage', 'ORI').
card_original_type('rogue\'s passage'/'ORI', 'Land').
card_original_text('rogue\'s passage'/'ORI', '{T}: Add {1} to your mana pool.{4}, {T}: Target creature can\'t be blocked this turn.').
card_image_name('rogue\'s passage'/'ORI', 'rogue\'s passage').
card_uid('rogue\'s passage'/'ORI', 'ORI:Rogue\'s Passage:rogue\'s passage').
card_rarity('rogue\'s passage'/'ORI', 'Uncommon').
card_artist('rogue\'s passage'/'ORI', 'Christine Choi').
card_number('rogue\'s passage'/'ORI', '250').
card_flavor_text('rogue\'s passage'/'ORI', 'Rumors quickly spread among thieves about a labyrinth without walls and a prize beyond all measures of worth.').
card_multiverse_id('rogue\'s passage'/'ORI', '398523').

card_in_set('runed servitor', 'ORI').
card_original_type('runed servitor'/'ORI', 'Artifact Creature — Construct').
card_original_text('runed servitor'/'ORI', 'When Runed Servitor dies, each player draws a card.').
card_image_name('runed servitor'/'ORI', 'runed servitor').
card_uid('runed servitor'/'ORI', 'ORI:Runed Servitor:runed servitor').
card_rarity('runed servitor'/'ORI', 'Uncommon').
card_artist('runed servitor'/'ORI', 'Mike Bierek').
card_number('runed servitor'/'ORI', '238').
card_flavor_text('runed servitor'/'ORI', 'Scholars had puzzled for centuries over the ruins at Tal Terig. Its secrets had always lived within one rune-carved head.').
card_multiverse_id('runed servitor'/'ORI', '398643').

card_in_set('scab-clan berserker', 'ORI').
card_original_type('scab-clan berserker'/'ORI', 'Creature — Human Berserker').
card_original_text('scab-clan berserker'/'ORI', 'HasteRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)Whenever an opponent casts a noncreature spell, if Scab-Clan Berserker is renowned, Scab-Clan Berserker deals 2 damage to that player.').
card_first_print('scab-clan berserker', 'ORI').
card_image_name('scab-clan berserker'/'ORI', 'scab-clan berserker').
card_uid('scab-clan berserker'/'ORI', 'ORI:Scab-Clan Berserker:scab-clan berserker').
card_rarity('scab-clan berserker'/'ORI', 'Rare').
card_artist('scab-clan berserker'/'ORI', 'Dave Kendall').
card_number('scab-clan berserker'/'ORI', '160').
card_multiverse_id('scab-clan berserker'/'ORI', '398461').

card_in_set('scrapskin drake', 'ORI').
card_original_type('scrapskin drake'/'ORI', 'Creature — Zombie Drake').
card_original_text('scrapskin drake'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)Scrapskin Drake can block only creatures with flying.').
card_image_name('scrapskin drake'/'ORI', 'scrapskin drake').
card_uid('scrapskin drake'/'ORI', 'ORI:Scrapskin Drake:scrapskin drake').
card_rarity('scrapskin drake'/'ORI', 'Common').
card_artist('scrapskin drake'/'ORI', 'Kev Walker').
card_number('scrapskin drake'/'ORI', '69').
card_flavor_text('scrapskin drake'/'ORI', '\"The cathars killed my skaabs down below. Let\'s see how high their swords can reach.\"—Ludevic, necro-alchemist').
card_multiverse_id('scrapskin drake'/'ORI', '398478').

card_in_set('screeching skaab', 'ORI').
card_original_type('screeching skaab'/'ORI', 'Creature — Zombie').
card_original_text('screeching skaab'/'ORI', 'When Screeching Skaab enters the battlefield, put the top two cards of your library into your graveyard.').
card_image_name('screeching skaab'/'ORI', 'screeching skaab').
card_uid('screeching skaab'/'ORI', 'ORI:Screeching Skaab:screeching skaab').
card_rarity('screeching skaab'/'ORI', 'Common').
card_artist('screeching skaab'/'ORI', 'Clint Cearley').
card_number('screeching skaab'/'ORI', '70').
card_flavor_text('screeching skaab'/'ORI', 'Its screeching is the sound of you losing your mind.').
card_multiverse_id('screeching skaab'/'ORI', '398487').

card_in_set('seismic elemental', 'ORI').
card_original_type('seismic elemental'/'ORI', 'Creature — Elemental').
card_original_text('seismic elemental'/'ORI', 'When Seismic Elemental enters the battlefield, creatures without flying can\'t block this turn.').
card_first_print('seismic elemental', 'ORI').
card_image_name('seismic elemental'/'ORI', 'seismic elemental').
card_uid('seismic elemental'/'ORI', 'ORI:Seismic Elemental:seismic elemental').
card_rarity('seismic elemental'/'ORI', 'Uncommon').
card_artist('seismic elemental'/'ORI', 'Jasper Sandner').
card_number('seismic elemental'/'ORI', '161').
card_flavor_text('seismic elemental'/'ORI', '\"Trying to stop it would be foolish. If you feel it coming—run!\"—Sergeant Ryda').
card_multiverse_id('seismic elemental'/'ORI', '398579').

card_in_set('send to sleep', 'ORI').
card_original_type('send to sleep'/'ORI', 'Instant').
card_original_text('send to sleep'/'ORI', 'Tap up to two target creatures.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, those creatures don\'t untap during their controllers\' next untap steps.').
card_first_print('send to sleep', 'ORI').
card_image_name('send to sleep'/'ORI', 'send to sleep').
card_uid('send to sleep'/'ORI', 'ORI:Send to Sleep:send to sleep').
card_rarity('send to sleep'/'ORI', 'Common').
card_artist('send to sleep'/'ORI', 'Cynthia Sheppard').
card_number('send to sleep'/'ORI', '71').
card_multiverse_id('send to sleep'/'ORI', '398545').

card_in_set('sengir vampire', 'ORI').
card_original_type('sengir vampire'/'ORI', 'Creature — Vampire').
card_original_text('sengir vampire'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever a creature dealt damage by Sengir Vampire this turn dies, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'ORI', 'sengir vampire').
card_uid('sengir vampire'/'ORI', 'ORI:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'ORI', 'Uncommon').
card_artist('sengir vampire'/'ORI', 'Kev Walker').
card_number('sengir vampire'/'ORI', '283').
card_flavor_text('sengir vampire'/'ORI', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'ORI', '401462').

card_in_set('sentinel of the eternal watch', 'ORI').
card_original_type('sentinel of the eternal watch'/'ORI', 'Creature — Giant Soldier').
card_original_text('sentinel of the eternal watch'/'ORI', 'Vigilance (Attacking doesn\'t cause this creature to tap.)At the beginning of combat on each opponent\'s turn, tap target creature that player controls.').
card_first_print('sentinel of the eternal watch', 'ORI').
card_image_name('sentinel of the eternal watch'/'ORI', 'sentinel of the eternal watch').
card_uid('sentinel of the eternal watch'/'ORI', 'ORI:Sentinel of the Eternal Watch:sentinel of the eternal watch').
card_rarity('sentinel of the eternal watch'/'ORI', 'Uncommon').
card_artist('sentinel of the eternal watch'/'ORI', 'Bastien L. Deharme').
card_number('sentinel of the eternal watch'/'ORI', '30').
card_flavor_text('sentinel of the eternal watch'/'ORI', 'The portcullis is superfluous.').
card_multiverse_id('sentinel of the eternal watch'/'ORI', '398659').

card_in_set('separatist voidmage', 'ORI').
card_original_type('separatist voidmage'/'ORI', 'Creature — Human Wizard').
card_original_text('separatist voidmage'/'ORI', 'When Separatist Voidmage enters the battlefield, you may return target creature to its owner\'s hand.').
card_first_print('separatist voidmage', 'ORI').
card_image_name('separatist voidmage'/'ORI', 'separatist voidmage').
card_uid('separatist voidmage'/'ORI', 'ORI:Separatist Voidmage:separatist voidmage').
card_rarity('separatist voidmage'/'ORI', 'Common').
card_artist('separatist voidmage'/'ORI', 'Jason Rainville').
card_number('separatist voidmage'/'ORI', '72').
card_flavor_text('separatist voidmage'/'ORI', '\"As long as each side thinks it can win, the balance holds, and the mage-rings stand.\"—Alhammarret').
card_multiverse_id('separatist voidmage'/'ORI', '398608').

card_in_set('serra angel', 'ORI').
card_original_type('serra angel'/'ORI', 'Creature — Angel').
card_original_text('serra angel'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'ORI', 'serra angel').
card_uid('serra angel'/'ORI', 'ORI:Serra Angel:serra angel').
card_rarity('serra angel'/'ORI', 'Uncommon').
card_artist('serra angel'/'ORI', 'Greg Staples').
card_number('serra angel'/'ORI', '276').
card_flavor_text('serra angel'/'ORI', 'Follow the light. In its absence, follow her.').
card_multiverse_id('serra angel'/'ORI', '401455').

card_in_set('shadows of the past', 'ORI').
card_original_type('shadows of the past'/'ORI', 'Enchantment').
card_original_text('shadows of the past'/'ORI', 'Whenever a creature dies, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.){4}{B}: Each opponent loses 2 life and you gain 2 life. Activate this ability only if there are four or more creature cards in your graveyard.').
card_first_print('shadows of the past', 'ORI').
card_image_name('shadows of the past'/'ORI', 'shadows of the past').
card_uid('shadows of the past'/'ORI', 'ORI:Shadows of the Past:shadows of the past').
card_rarity('shadows of the past'/'ORI', 'Uncommon').
card_artist('shadows of the past'/'ORI', 'Ryan Yee').
card_number('shadows of the past'/'ORI', '118').
card_multiverse_id('shadows of the past'/'ORI', '398522').

card_in_set('shaman of the pack', 'ORI').
card_original_type('shaman of the pack'/'ORI', 'Creature — Elf Shaman').
card_original_text('shaman of the pack'/'ORI', 'When Shaman of the Pack enters the battlefield, target opponent loses life equal to the number of Elves you control.').
card_first_print('shaman of the pack', 'ORI').
card_image_name('shaman of the pack'/'ORI', 'shaman of the pack').
card_uid('shaman of the pack'/'ORI', 'ORI:Shaman of the Pack:shaman of the pack').
card_rarity('shaman of the pack'/'ORI', 'Uncommon').
card_artist('shaman of the pack'/'ORI', 'Dan Scott').
card_number('shaman of the pack'/'ORI', '217').
card_flavor_text('shaman of the pack'/'ORI', 'To the elves, her spear is a compass; to the boggarts, a harbinger of doom.').
card_multiverse_id('shaman of the pack'/'ORI', '398489').

card_in_set('shambling ghoul', 'ORI').
card_original_type('shambling ghoul'/'ORI', 'Creature — Zombie').
card_original_text('shambling ghoul'/'ORI', 'Shambling Ghoul enters the battlefield tapped.').
card_first_print('shambling ghoul', 'ORI').
card_image_name('shambling ghoul'/'ORI', 'shambling ghoul').
card_uid('shambling ghoul'/'ORI', 'ORI:Shambling Ghoul:shambling ghoul').
card_rarity('shambling ghoul'/'ORI', 'Common').
card_artist('shambling ghoul'/'ORI', 'Joseph Meehan').
card_number('shambling ghoul'/'ORI', '119').
card_flavor_text('shambling ghoul'/'ORI', 'Once he gets up and going, he will forever shamble on.').
card_multiverse_id('shambling ghoul'/'ORI', '398552').

card_in_set('shivan dragon', 'ORI').
card_original_type('shivan dragon'/'ORI', 'Creature — Dragon').
card_original_text('shivan dragon'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'ORI', 'shivan dragon').
card_uid('shivan dragon'/'ORI', 'ORI:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'ORI', 'Rare').
card_artist('shivan dragon'/'ORI', 'Donato Giancola').
card_number('shivan dragon'/'ORI', '285').
card_flavor_text('shivan dragon'/'ORI', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'ORI', '401464').

card_in_set('shivan reef', 'ORI').
card_original_type('shivan reef'/'ORI', 'Land').
card_original_text('shivan reef'/'ORI', '{T}: Add {1} to your mana pool.{T}: Add {U} or {R} to your mana pool. Shivan Reef deals 1 damage to you.').
card_image_name('shivan reef'/'ORI', 'shivan reef').
card_uid('shivan reef'/'ORI', 'ORI:Shivan Reef:shivan reef').
card_rarity('shivan reef'/'ORI', 'Rare').
card_artist('shivan reef'/'ORI', 'Rob Alexander').
card_number('shivan reef'/'ORI', '251').
card_multiverse_id('shivan reef'/'ORI', '398444').

card_in_set('sigil of the empty throne', 'ORI').
card_original_type('sigil of the empty throne'/'ORI', 'Enchantment').
card_original_text('sigil of the empty throne'/'ORI', 'Whenever you cast an enchantment spell, put a 4/4 white Angel creature token with flying onto the battlefield.').
card_image_name('sigil of the empty throne'/'ORI', 'sigil of the empty throne').
card_uid('sigil of the empty throne'/'ORI', 'ORI:Sigil of the Empty Throne:sigil of the empty throne').
card_rarity('sigil of the empty throne'/'ORI', 'Rare').
card_artist('sigil of the empty throne'/'ORI', 'Cyril Van Der Haegen').
card_number('sigil of the empty throne'/'ORI', '31').
card_flavor_text('sigil of the empty throne'/'ORI', 'When Asha left Bant, she ensured that the world would have protection and order in her absence.').
card_multiverse_id('sigil of the empty throne'/'ORI', '398524').

card_in_set('sigil of valor', 'ORI').
card_original_type('sigil of valor'/'ORI', 'Artifact — Equipment').
card_original_text('sigil of valor'/'ORI', 'Whenever equipped creature attacks alone, it gets +1/+1 until end of turn for each other creature you control.Equip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('sigil of valor', 'ORI').
card_image_name('sigil of valor'/'ORI', 'sigil of valor').
card_uid('sigil of valor'/'ORI', 'ORI:Sigil of Valor:sigil of valor').
card_rarity('sigil of valor'/'ORI', 'Uncommon').
card_artist('sigil of valor'/'ORI', 'Dan Scott').
card_number('sigil of valor'/'ORI', '239').
card_flavor_text('sigil of valor'/'ORI', 'With each honor he earns, Gideon paves the path to his redemption.').
card_multiverse_id('sigil of valor'/'ORI', '398515').

card_in_set('sigiled starfish', 'ORI').
card_original_type('sigiled starfish'/'ORI', 'Creature — Starfish').
card_original_text('sigiled starfish'/'ORI', '{T}: Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_image_name('sigiled starfish'/'ORI', 'sigiled starfish').
card_uid('sigiled starfish'/'ORI', 'ORI:Sigiled Starfish:sigiled starfish').
card_rarity('sigiled starfish'/'ORI', 'Uncommon').
card_artist('sigiled starfish'/'ORI', 'Nils Hamm').
card_number('sigiled starfish'/'ORI', '73').
card_flavor_text('sigiled starfish'/'ORI', 'Kruphix hid the most dire prophecies about humankind where humans would never find them and tritons wouldn\'t care to read them.').
card_multiverse_id('sigiled starfish'/'ORI', '398592').

card_in_set('skaab goliath', 'ORI').
card_original_type('skaab goliath'/'ORI', 'Creature — Zombie Giant').
card_original_text('skaab goliath'/'ORI', 'As an additional cost to cast Skaab Goliath, exile two creature cards from your graveyard.Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)').
card_image_name('skaab goliath'/'ORI', 'skaab goliath').
card_uid('skaab goliath'/'ORI', 'ORI:Skaab Goliath:skaab goliath').
card_rarity('skaab goliath'/'ORI', 'Uncommon').
card_artist('skaab goliath'/'ORI', 'Volkan Baga').
card_number('skaab goliath'/'ORI', '74').
card_flavor_text('skaab goliath'/'ORI', 'Three heads are better than one.').
card_multiverse_id('skaab goliath'/'ORI', '398559').

card_in_set('skyraker giant', 'ORI').
card_original_type('skyraker giant'/'ORI', 'Creature — Giant').
card_original_text('skyraker giant'/'ORI', 'Reach (This creature can block creatures with flying.)').
card_first_print('skyraker giant', 'ORI').
card_image_name('skyraker giant'/'ORI', 'skyraker giant').
card_uid('skyraker giant'/'ORI', 'ORI:Skyraker Giant:skyraker giant').
card_rarity('skyraker giant'/'ORI', 'Uncommon').
card_artist('skyraker giant'/'ORI', 'Anastasia Ovchinnikova').
card_number('skyraker giant'/'ORI', '162').
card_flavor_text('skyraker giant'/'ORI', 'She will not be content until hers is the highest head in the sky.').
card_multiverse_id('skyraker giant'/'ORI', '398430').

card_in_set('skysnare spider', 'ORI').
card_original_type('skysnare spider'/'ORI', 'Creature — Spider').
card_original_text('skysnare spider'/'ORI', 'Vigilance (Attacking doesn\'t cause this creature to tap.)Reach (This creature can block creatures with flying.)').
card_first_print('skysnare spider', 'ORI').
card_image_name('skysnare spider'/'ORI', 'skysnare spider').
card_uid('skysnare spider'/'ORI', 'ORI:Skysnare Spider:skysnare spider').
card_rarity('skysnare spider'/'ORI', 'Uncommon').
card_artist('skysnare spider'/'ORI', 'Filip Burburan').
card_number('skysnare spider'/'ORI', '197').
card_flavor_text('skysnare spider'/'ORI', 'The only thing more ill-tempered than a griffin in a web is the spider that must subdue it.').
card_multiverse_id('skysnare spider'/'ORI', '398650').

card_in_set('smash to smithereens', 'ORI').
card_original_type('smash to smithereens'/'ORI', 'Instant').
card_original_text('smash to smithereens'/'ORI', 'Destroy target artifact. Smash to Smithereens deals 3 damage to that artifact\'s controller.').
card_image_name('smash to smithereens'/'ORI', 'smash to smithereens').
card_uid('smash to smithereens'/'ORI', 'ORI:Smash to Smithereens:smash to smithereens').
card_rarity('smash to smithereens'/'ORI', 'Common').
card_artist('smash to smithereens'/'ORI', 'Pete Venters').
card_number('smash to smithereens'/'ORI', '163').
card_flavor_text('smash to smithereens'/'ORI', 'The giant Tarvik dreamed that trinkets and machines caused all the world\'s woe. When he awoke from his troubled sleep, he took the name Tarvik Relicsmasher.').
card_multiverse_id('smash to smithereens'/'ORI', '398669').

card_in_set('somberwald alpha', 'ORI').
card_original_type('somberwald alpha'/'ORI', 'Creature — Wolf').
card_original_text('somberwald alpha'/'ORI', 'Whenever a creature you control becomes blocked, it gets +1/+1 until end of turn.{1}{G}: Target creature you control gains trample until end of turn. (It can deal excess combat damage to defending player or planeswalker while attacking.)').
card_first_print('somberwald alpha', 'ORI').
card_image_name('somberwald alpha'/'ORI', 'somberwald alpha').
card_uid('somberwald alpha'/'ORI', 'ORI:Somberwald Alpha:somberwald alpha').
card_rarity('somberwald alpha'/'ORI', 'Uncommon').
card_artist('somberwald alpha'/'ORI', 'Filip Burburan').
card_number('somberwald alpha'/'ORI', '198').
card_multiverse_id('somberwald alpha'/'ORI', '398626').

card_in_set('soulblade djinn', 'ORI').
card_original_type('soulblade djinn'/'ORI', 'Creature — Djinn').
card_original_text('soulblade djinn'/'ORI', 'FlyingWhenever you cast a noncreature spell, creatures you control get +1/+1 until end of turn.').
card_first_print('soulblade djinn', 'ORI').
card_image_name('soulblade djinn'/'ORI', 'soulblade djinn').
card_uid('soulblade djinn'/'ORI', 'ORI:Soulblade Djinn:soulblade djinn').
card_rarity('soulblade djinn'/'ORI', 'Rare').
card_artist('soulblade djinn'/'ORI', 'Viktor Titov').
card_number('soulblade djinn'/'ORI', '75').
card_flavor_text('soulblade djinn'/'ORI', 'He grants endless wishes, as long as you always wish for a blade.').
card_multiverse_id('soulblade djinn'/'ORI', '398485').

card_in_set('sphinx\'s tutelage', 'ORI').
card_original_type('sphinx\'s tutelage'/'ORI', 'Enchantment').
card_original_text('sphinx\'s tutelage'/'ORI', 'Whenever you draw a card, target opponent puts the top two cards of his or her library into his or her graveyard. If they\'re both nonland cards that share a color, repeat this process.{5}{U}: Draw a card, then discard a card.').
card_first_print('sphinx\'s tutelage', 'ORI').
card_image_name('sphinx\'s tutelage'/'ORI', 'sphinx\'s tutelage').
card_uid('sphinx\'s tutelage'/'ORI', 'ORI:Sphinx\'s Tutelage:sphinx\'s tutelage').
card_rarity('sphinx\'s tutelage'/'ORI', 'Uncommon').
card_artist('sphinx\'s tutelage'/'ORI', 'Slawomir Maniak').
card_number('sphinx\'s tutelage'/'ORI', '76').
card_flavor_text('sphinx\'s tutelage'/'ORI', '\"You\'ve never considered the implications of dealing with another telepath, Jace.\"').
card_multiverse_id('sphinx\'s tutelage'/'ORI', '398520').

card_in_set('stalwart aven', 'ORI').
card_original_type('stalwart aven'/'ORI', 'Creature — Bird Soldier').
card_original_text('stalwart aven'/'ORI', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_first_print('stalwart aven', 'ORI').
card_image_name('stalwart aven'/'ORI', 'stalwart aven').
card_uid('stalwart aven'/'ORI', 'ORI:Stalwart Aven:stalwart aven').
card_rarity('stalwart aven'/'ORI', 'Common').
card_artist('stalwart aven'/'ORI', 'Scott Murphy').
card_number('stalwart aven'/'ORI', '32').
card_multiverse_id('stalwart aven'/'ORI', '398624').

card_in_set('starfield of nyx', 'ORI').
card_original_type('starfield of nyx'/'ORI', 'Enchantment').
card_original_text('starfield of nyx'/'ORI', 'At the beginning of your upkeep, you may return target enchantment card from your graveyard to the battlefield.As long as you control five or more enchantments, each other non-Aura enchantment you control is a creature in addition to its other types and has base power and base toughness each equal to its converted mana cost.').
card_first_print('starfield of nyx', 'ORI').
card_image_name('starfield of nyx'/'ORI', 'starfield of nyx').
card_uid('starfield of nyx'/'ORI', 'ORI:Starfield of Nyx:starfield of nyx').
card_rarity('starfield of nyx'/'ORI', 'Mythic Rare').
card_artist('starfield of nyx'/'ORI', 'Tyler Jacobson').
card_number('starfield of nyx'/'ORI', '33').
card_multiverse_id('starfield of nyx'/'ORI', '398475').

card_in_set('stratus walk', 'ORI').
card_original_type('stratus walk'/'ORI', 'Enchantment — Aura').
card_original_text('stratus walk'/'ORI', 'Enchant creatureWhen Stratus Walk enters the battlefield, draw a card.Enchanted creature has flying. (It can\'t be blocked except by creatures with flying or reach.)Enchanted creature can block only creatures with flying.').
card_image_name('stratus walk'/'ORI', 'stratus walk').
card_uid('stratus walk'/'ORI', 'ORI:Stratus Walk:stratus walk').
card_rarity('stratus walk'/'ORI', 'Common').
card_artist('stratus walk'/'ORI', 'Aaron Miller').
card_number('stratus walk'/'ORI', '77').
card_multiverse_id('stratus walk'/'ORI', '398549').

card_in_set('subterranean scout', 'ORI').
card_original_type('subterranean scout'/'ORI', 'Creature — Goblin Scout').
card_original_text('subterranean scout'/'ORI', 'When Subterranean Scout enters the battlefield, target creature with power 2 or less can\'t be blocked this turn.').
card_first_print('subterranean scout', 'ORI').
card_image_name('subterranean scout'/'ORI', 'subterranean scout').
card_uid('subterranean scout'/'ORI', 'ORI:Subterranean Scout:subterranean scout').
card_rarity('subterranean scout'/'ORI', 'Common').
card_artist('subterranean scout'/'ORI', 'Lucas Graciano').
card_number('subterranean scout'/'ORI', '164').
card_flavor_text('subterranean scout'/'ORI', 'When things get ugly above ground, goblins resort to alternate routes of passage.').
card_multiverse_id('subterranean scout'/'ORI', '398458').

card_in_set('suppression bonds', 'ORI').
card_original_type('suppression bonds'/'ORI', 'Enchantment — Aura').
card_original_text('suppression bonds'/'ORI', 'Enchant nonland permanentEnchanted permanent can\'t attack or block, and its activated abilities can\'t be activated.').
card_first_print('suppression bonds', 'ORI').
card_image_name('suppression bonds'/'ORI', 'suppression bonds').
card_uid('suppression bonds'/'ORI', 'ORI:Suppression Bonds:suppression bonds').
card_rarity('suppression bonds'/'ORI', 'Common').
card_artist('suppression bonds'/'ORI', 'Chris Rallis').
card_number('suppression bonds'/'ORI', '34').
card_flavor_text('suppression bonds'/'ORI', '\"A master of hieromancy can see the advantage in any scenario.\"—Warden Hixus, to Kytheon').
card_multiverse_id('suppression bonds'/'ORI', '398602').

card_in_set('swamp', 'ORI').
card_original_type('swamp'/'ORI', 'Basic Land — Swamp').
card_original_text('swamp'/'ORI', 'B').
card_image_name('swamp'/'ORI', 'swamp1').
card_uid('swamp'/'ORI', 'ORI:Swamp:swamp1').
card_rarity('swamp'/'ORI', 'Basic Land').
card_artist('swamp'/'ORI', 'Larry Elmore').
card_number('swamp'/'ORI', '261').
card_multiverse_id('swamp'/'ORI', '398677').

card_in_set('swamp', 'ORI').
card_original_type('swamp'/'ORI', 'Basic Land — Swamp').
card_original_text('swamp'/'ORI', 'B').
card_image_name('swamp'/'ORI', 'swamp2').
card_uid('swamp'/'ORI', 'ORI:Swamp:swamp2').
card_rarity('swamp'/'ORI', 'Basic Land').
card_artist('swamp'/'ORI', 'Dan Frazier').
card_number('swamp'/'ORI', '262').
card_multiverse_id('swamp'/'ORI', '398555').

card_in_set('swamp', 'ORI').
card_original_type('swamp'/'ORI', 'Basic Land — Swamp').
card_original_text('swamp'/'ORI', 'B').
card_image_name('swamp'/'ORI', 'swamp3').
card_uid('swamp'/'ORI', 'ORI:Swamp:swamp3').
card_rarity('swamp'/'ORI', 'Basic Land').
card_artist('swamp'/'ORI', 'James Paick').
card_number('swamp'/'ORI', '263').
card_multiverse_id('swamp'/'ORI', '398507').

card_in_set('swamp', 'ORI').
card_original_type('swamp'/'ORI', 'Basic Land — Swamp').
card_original_text('swamp'/'ORI', 'B').
card_image_name('swamp'/'ORI', 'swamp4').
card_uid('swamp'/'ORI', 'ORI:Swamp:swamp4').
card_rarity('swamp'/'ORI', 'Basic Land').
card_artist('swamp'/'ORI', 'Jung Park').
card_number('swamp'/'ORI', '264').
card_multiverse_id('swamp'/'ORI', '398472').

card_in_set('swift reckoning', 'ORI').
card_original_type('swift reckoning'/'ORI', 'Sorcery').
card_original_text('swift reckoning'/'ORI', 'Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, you may cast Swift Reckoning as though it had flash. (You may cast it any time you could cast an instant.)Destroy target tapped creature.').
card_first_print('swift reckoning', 'ORI').
card_image_name('swift reckoning'/'ORI', 'swift reckoning').
card_uid('swift reckoning'/'ORI', 'ORI:Swift Reckoning:swift reckoning').
card_rarity('swift reckoning'/'ORI', 'Uncommon').
card_artist('swift reckoning'/'ORI', 'Chris Rahn').
card_number('swift reckoning'/'ORI', '35').
card_multiverse_id('swift reckoning'/'ORI', '398481').

card_in_set('sword of the animist', 'ORI').
card_original_type('sword of the animist'/'ORI', 'Legendary Artifact — Equipment').
card_original_text('sword of the animist'/'ORI', 'Equipped creature gets +1/+1.Whenever equipped creature attacks, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.Equip {2}').
card_first_print('sword of the animist', 'ORI').
card_image_name('sword of the animist'/'ORI', 'sword of the animist').
card_uid('sword of the animist'/'ORI', 'ORI:Sword of the Animist:sword of the animist').
card_rarity('sword of the animist'/'ORI', 'Rare').
card_artist('sword of the animist'/'ORI', 'Daniel Ljunggren').
card_number('sword of the animist'/'ORI', '240').
card_flavor_text('sword of the animist'/'ORI', 'The blade glows only for Zendikar\'s chosen.').
card_multiverse_id('sword of the animist'/'ORI', '398492').

card_in_set('sylvan messenger', 'ORI').
card_original_type('sylvan messenger'/'ORI', 'Creature — Elf').
card_original_text('sylvan messenger'/'ORI', 'Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)When Sylvan Messenger enters the battlefield, reveal the top four cards of your library. Put all Elf cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_image_name('sylvan messenger'/'ORI', 'sylvan messenger').
card_uid('sylvan messenger'/'ORI', 'ORI:Sylvan Messenger:sylvan messenger').
card_rarity('sylvan messenger'/'ORI', 'Uncommon').
card_artist('sylvan messenger'/'ORI', 'Anthony Palumbo').
card_number('sylvan messenger'/'ORI', '199').
card_multiverse_id('sylvan messenger'/'ORI', '398651').

card_in_set('tainted remedy', 'ORI').
card_original_type('tainted remedy'/'ORI', 'Enchantment').
card_original_text('tainted remedy'/'ORI', 'If an opponent would gain life, that player loses that much life instead.').
card_first_print('tainted remedy', 'ORI').
card_image_name('tainted remedy'/'ORI', 'tainted remedy').
card_uid('tainted remedy'/'ORI', 'ORI:Tainted Remedy:tainted remedy').
card_rarity('tainted remedy'/'ORI', 'Rare').
card_artist('tainted remedy'/'ORI', 'Izzy').
card_number('tainted remedy'/'ORI', '120').
card_flavor_text('tainted remedy'/'ORI', '\"Drink this, brother. It will bring you rest.\"—Liliana Vess').
card_multiverse_id('tainted remedy'/'ORI', '398612').

card_in_set('talent of the telepath', 'ORI').
card_original_type('talent of the telepath'/'ORI', 'Sorcery').
card_original_text('talent of the telepath'/'ORI', 'Target opponent reveals the top seven cards of his or her library. You may cast an instant or sorcery card from among them without paying its mana cost. Then that player puts the rest into his or her graveyard.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, you may cast up to two revealed instant and/or sorcery cards instead of one.').
card_first_print('talent of the telepath', 'ORI').
card_image_name('talent of the telepath'/'ORI', 'talent of the telepath').
card_uid('talent of the telepath'/'ORI', 'ORI:Talent of the Telepath:talent of the telepath').
card_rarity('talent of the telepath'/'ORI', 'Rare').
card_artist('talent of the telepath'/'ORI', 'Peter Mohrbacher').
card_number('talent of the telepath'/'ORI', '78').
card_multiverse_id('talent of the telepath'/'ORI', '398483').

card_in_set('terra stomper', 'ORI').
card_original_type('terra stomper'/'ORI', 'Creature — Beast').
card_original_text('terra stomper'/'ORI', 'Terra Stomper can\'t be countered.\nTrample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)').
card_image_name('terra stomper'/'ORI', 'terra stomper').
card_uid('terra stomper'/'ORI', 'ORI:Terra Stomper:terra stomper').
card_rarity('terra stomper'/'ORI', 'Rare').
card_artist('terra stomper'/'ORI', 'Goran Josic').
card_number('terra stomper'/'ORI', '288').
card_flavor_text('terra stomper'/'ORI', 'Sometimes violent earthquakes, hurtling boulders, and unseasonable dust storms are wrongly attributed to the Roil.').
card_multiverse_id('terra stomper'/'ORI', '401467').

card_in_set('the great aurora', 'ORI').
card_original_type('the great aurora'/'ORI', 'Sorcery').
card_original_text('the great aurora'/'ORI', 'Each player shuffles all cards from his or her hand and all permanents he or she owns into his or her library, then draws that many cards. Each player may put any number of land cards from his or her hand onto the battlefield. Exile The Great Aurora.').
card_first_print('the great aurora', 'ORI').
card_image_name('the great aurora'/'ORI', 'the great aurora').
card_uid('the great aurora'/'ORI', 'ORI:The Great Aurora:the great aurora').
card_rarity('the great aurora'/'ORI', 'Mythic Rare').
card_artist('the great aurora'/'ORI', 'Sam Burley').
card_number('the great aurora'/'ORI', '179').
card_multiverse_id('the great aurora'/'ORI', '398679').

card_in_set('thopter engineer', 'ORI').
card_original_type('thopter engineer'/'ORI', 'Creature — Human Artificer').
card_original_text('thopter engineer'/'ORI', 'When Thopter Engineer enters the battlefield, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield.Artifact creatures you control have haste. (They can attack and {T} as soon as they come under your control.)').
card_first_print('thopter engineer', 'ORI').
card_image_name('thopter engineer'/'ORI', 'thopter engineer').
card_uid('thopter engineer'/'ORI', 'ORI:Thopter Engineer:thopter engineer').
card_rarity('thopter engineer'/'ORI', 'Uncommon').
card_artist('thopter engineer'/'ORI', 'Steve Prescott').
card_number('thopter engineer'/'ORI', '165').
card_multiverse_id('thopter engineer'/'ORI', '398514').

card_in_set('thopter spy network', 'ORI').
card_original_type('thopter spy network'/'ORI', 'Enchantment').
card_original_text('thopter spy network'/'ORI', 'At the beginning of your upkeep, if you control an artifact, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield.Whenever one or more artifact creatures you control deal combat damage to a player, draw a card.').
card_first_print('thopter spy network', 'ORI').
card_image_name('thopter spy network'/'ORI', 'thopter spy network').
card_uid('thopter spy network'/'ORI', 'ORI:Thopter Spy Network:thopter spy network').
card_rarity('thopter spy network'/'ORI', 'Rare').
card_artist('thopter spy network'/'ORI', 'Jung Park').
card_number('thopter spy network'/'ORI', '79').
card_multiverse_id('thopter spy network'/'ORI', '398519').

card_in_set('thornbow archer', 'ORI').
card_original_type('thornbow archer'/'ORI', 'Creature — Elf Archer').
card_original_text('thornbow archer'/'ORI', 'Whenever Thornbow Archer attacks, each opponent who doesn\'t control an Elf loses 1 life.').
card_first_print('thornbow archer', 'ORI').
card_image_name('thornbow archer'/'ORI', 'thornbow archer').
card_uid('thornbow archer'/'ORI', 'ORI:Thornbow Archer:thornbow archer').
card_rarity('thornbow archer'/'ORI', 'Common').
card_artist('thornbow archer'/'ORI', 'Kev Walker').
card_number('thornbow archer'/'ORI', '121').
card_flavor_text('thornbow archer'/'ORI', '\"I will not rest until the stench of eyeblights is eradicated from these woods.\"').
card_multiverse_id('thornbow archer'/'ORI', '398407').

card_in_set('throwing knife', 'ORI').
card_original_type('throwing knife'/'ORI', 'Artifact — Equipment').
card_original_text('throwing knife'/'ORI', 'Equipped creature gets +2/+0.Whenever equipped creature attacks, you may sacrifice Throwing Knife. If you do, Throwing Knife deals 2 damage to target creature or player.Equip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('throwing knife', 'ORI').
card_image_name('throwing knife'/'ORI', 'throwing knife').
card_uid('throwing knife'/'ORI', 'ORI:Throwing Knife:throwing knife').
card_rarity('throwing knife'/'ORI', 'Uncommon').
card_artist('throwing knife'/'ORI', 'Mathias Kollros').
card_number('throwing knife'/'ORI', '241').
card_multiverse_id('throwing knife'/'ORI', '398595').

card_in_set('thunderclap wyvern', 'ORI').
card_original_type('thunderclap wyvern'/'ORI', 'Creature — Drake').
card_original_text('thunderclap wyvern'/'ORI', 'Flash (You may cast this spell any time you could cast an instant.)FlyingOther creatures you control with flying get +1/+1.').
card_first_print('thunderclap wyvern', 'ORI').
card_image_name('thunderclap wyvern'/'ORI', 'thunderclap wyvern').
card_uid('thunderclap wyvern'/'ORI', 'ORI:Thunderclap Wyvern:thunderclap wyvern').
card_rarity('thunderclap wyvern'/'ORI', 'Uncommon').
card_artist('thunderclap wyvern'/'ORI', 'Jason Felix').
card_number('thunderclap wyvern'/'ORI', '218').
card_flavor_text('thunderclap wyvern'/'ORI', 'Thunder doesn\'t always mean rain. Sometimes it means ruin.').
card_multiverse_id('thunderclap wyvern'/'ORI', '398655').

card_in_set('timberpack wolf', 'ORI').
card_original_type('timberpack wolf'/'ORI', 'Creature — Wolf').
card_original_text('timberpack wolf'/'ORI', 'Timberpack Wolf gets +1/+1 for each other creature you control named Timberpack Wolf.').
card_image_name('timberpack wolf'/'ORI', 'timberpack wolf').
card_uid('timberpack wolf'/'ORI', 'ORI:Timberpack Wolf:timberpack wolf').
card_rarity('timberpack wolf'/'ORI', 'Common').
card_artist('timberpack wolf'/'ORI', 'John Avon').
card_number('timberpack wolf'/'ORI', '200').
card_flavor_text('timberpack wolf'/'ORI', 'Their strength is no mere numbers game. An intangible force augments the wolves when they run together.').
card_multiverse_id('timberpack wolf'/'ORI', '398445').

card_in_set('titan\'s strength', 'ORI').
card_original_type('titan\'s strength'/'ORI', 'Instant').
card_original_text('titan\'s strength'/'ORI', 'Target creature gets +3/+1 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_image_name('titan\'s strength'/'ORI', 'titan\'s strength').
card_uid('titan\'s strength'/'ORI', 'ORI:Titan\'s Strength:titan\'s strength').
card_rarity('titan\'s strength'/'ORI', 'Common').
card_artist('titan\'s strength'/'ORI', 'Karl Kopinski').
card_number('titan\'s strength'/'ORI', '166').
card_multiverse_id('titan\'s strength'/'ORI', '398680').

card_in_set('titanic growth', 'ORI').
card_original_type('titanic growth'/'ORI', 'Instant').
card_original_text('titanic growth'/'ORI', 'Target creature gets +4/+4 until end of turn.').
card_image_name('titanic growth'/'ORI', 'titanic growth').
card_uid('titanic growth'/'ORI', 'ORI:Titanic Growth:titanic growth').
card_rarity('titanic growth'/'ORI', 'Common').
card_artist('titanic growth'/'ORI', 'Ryan Pancoast').
card_number('titanic growth'/'ORI', '201').
card_flavor_text('titanic growth'/'ORI', 'The massive dominate through might. The tiny survive with guile. Beware the tiny who become massive.').
card_multiverse_id('titanic growth'/'ORI', '398648').

card_in_set('topan freeblade', 'ORI').
card_original_type('topan freeblade'/'ORI', 'Creature — Human Soldier').
card_original_text('topan freeblade'/'ORI', 'Vigilance (Attacking doesn\'t cause this creature to tap.)Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_first_print('topan freeblade', 'ORI').
card_image_name('topan freeblade'/'ORI', 'topan freeblade').
card_uid('topan freeblade'/'ORI', 'ORI:Topan Freeblade:topan freeblade').
card_rarity('topan freeblade'/'ORI', 'Common').
card_artist('topan freeblade'/'ORI', 'Johannes Voss').
card_number('topan freeblade'/'ORI', '36').
card_flavor_text('topan freeblade'/'ORI', '\"My scars are my sigils. I will wear them with pride long after you\'re gone.\"').
card_multiverse_id('topan freeblade'/'ORI', '398619').

card_in_set('tormented thoughts', 'ORI').
card_original_type('tormented thoughts'/'ORI', 'Sorcery').
card_original_text('tormented thoughts'/'ORI', 'As an additional cost to cast Tormented Thoughts, sacrifice a creature.Target player discards a number of cards equal to the sacrificed creature\'s power.').
card_image_name('tormented thoughts'/'ORI', 'tormented thoughts').
card_uid('tormented thoughts'/'ORI', 'ORI:Tormented Thoughts:tormented thoughts').
card_rarity('tormented thoughts'/'ORI', 'Uncommon').
card_artist('tormented thoughts'/'ORI', 'Allen Williams').
card_number('tormented thoughts'/'ORI', '122').
card_flavor_text('tormented thoughts'/'ORI', '\"Not all nightmares can be escaped by waking.\"—Ashiok').
card_multiverse_id('tormented thoughts'/'ORI', '398623').

card_in_set('totem-guide hartebeest', 'ORI').
card_original_type('totem-guide hartebeest'/'ORI', 'Creature — Antelope').
card_original_text('totem-guide hartebeest'/'ORI', 'When Totem-Guide Hartebeest enters the battlefield, you may search your library for an Aura card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('totem-guide hartebeest'/'ORI', 'totem-guide hartebeest').
card_uid('totem-guide hartebeest'/'ORI', 'ORI:Totem-Guide Hartebeest:totem-guide hartebeest').
card_rarity('totem-guide hartebeest'/'ORI', 'Uncommon').
card_artist('totem-guide hartebeest'/'ORI', 'John Avon').
card_number('totem-guide hartebeest'/'ORI', '37').
card_flavor_text('totem-guide hartebeest'/'ORI', 'It is hunted, but not for the kill. The magic within draws others to its presence.').
card_multiverse_id('totem-guide hartebeest'/'ORI', '398599').

card_in_set('touch of moonglove', 'ORI').
card_original_type('touch of moonglove'/'ORI', 'Instant').
card_original_text('touch of moonglove'/'ORI', 'Target creature you control gets +1/+0 and gains deathtouch until end of turn. Whenever a creature dealt damage by that creature dies this turn, its controller loses 2 life. (Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_first_print('touch of moonglove', 'ORI').
card_image_name('touch of moonglove'/'ORI', 'touch of moonglove').
card_uid('touch of moonglove'/'ORI', 'ORI:Touch of Moonglove:touch of moonglove').
card_rarity('touch of moonglove'/'ORI', 'Common').
card_artist('touch of moonglove'/'ORI', 'Scott Murphy').
card_number('touch of moonglove'/'ORI', '123').
card_multiverse_id('touch of moonglove'/'ORI', '398600').

card_in_set('tower geist', 'ORI').
card_original_type('tower geist'/'ORI', 'Creature — Spirit').
card_original_text('tower geist'/'ORI', 'FlyingWhen Tower Geist enters the battlefield, look at the top two cards of your library. Put one of them into your hand and the other into your graveyard.').
card_image_name('tower geist'/'ORI', 'tower geist').
card_uid('tower geist'/'ORI', 'ORI:Tower Geist:tower geist').
card_rarity('tower geist'/'ORI', 'Uncommon').
card_artist('tower geist'/'ORI', 'Izzy').
card_number('tower geist'/'ORI', '80').
card_flavor_text('tower geist'/'ORI', 'Jenrik\'s tower is served by those who once sought to enter it uninvited.').
card_multiverse_id('tower geist'/'ORI', '398615').

card_in_set('tragic arrogance', 'ORI').
card_original_type('tragic arrogance'/'ORI', 'Sorcery').
card_original_text('tragic arrogance'/'ORI', 'For each player, you choose from among the permanents that player controls an artifact, a creature, an enchantment, and a planeswalker. Then each player sacrifices all other nonland permanents he or she controls.').
card_first_print('tragic arrogance', 'ORI').
card_image_name('tragic arrogance'/'ORI', 'tragic arrogance').
card_uid('tragic arrogance'/'ORI', 'ORI:Tragic Arrogance:tragic arrogance').
card_rarity('tragic arrogance'/'ORI', 'Rare').
card_artist('tragic arrogance'/'ORI', 'Winona Nelson').
card_number('tragic arrogance'/'ORI', '38').
card_flavor_text('tragic arrogance'/'ORI', 'The spear thrown by Kytheon\'s own hand was the weapon that felled his friends.').
card_multiverse_id('tragic arrogance'/'ORI', '398610').

card_in_set('turn to frog', 'ORI').
card_original_type('turn to frog'/'ORI', 'Instant').
card_original_text('turn to frog'/'ORI', 'Until end of turn, target creature loses all abilities and becomes a blue Frog with base power and toughness 1/1.').
card_image_name('turn to frog'/'ORI', 'turn to frog').
card_uid('turn to frog'/'ORI', 'ORI:Turn to Frog:turn to frog').
card_rarity('turn to frog'/'ORI', 'Uncommon').
card_artist('turn to frog'/'ORI', 'Warren Mahy').
card_number('turn to frog'/'ORI', '81').
card_flavor_text('turn to frog'/'ORI', '\"Ribbit.\"').
card_multiverse_id('turn to frog'/'ORI', '398463').

card_in_set('undead servant', 'ORI').
card_original_type('undead servant'/'ORI', 'Creature — Zombie').
card_original_text('undead servant'/'ORI', 'When Undead Servant enters the battlefield, put a 2/2 black Zombie creature token onto the battlefield for each card named Undead Servant in your graveyard.').
card_first_print('undead servant', 'ORI').
card_image_name('undead servant'/'ORI', 'undead servant').
card_uid('undead servant'/'ORI', 'ORI:Undead Servant:undead servant').
card_rarity('undead servant'/'ORI', 'Common').
card_artist('undead servant'/'ORI', 'James Zapata').
card_number('undead servant'/'ORI', '124').
card_flavor_text('undead servant'/'ORI', '\"Get up. You\'re not finished yet.\"—Liliana Vess').
card_multiverse_id('undead servant'/'ORI', '398484').

card_in_set('undercity troll', 'ORI').
card_original_type('undercity troll'/'ORI', 'Creature — Troll').
card_original_text('undercity troll'/'ORI', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.){2}{G}: Regenerate Undercity Troll. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('undercity troll', 'ORI').
card_image_name('undercity troll'/'ORI', 'undercity troll').
card_uid('undercity troll'/'ORI', 'ORI:Undercity Troll:undercity troll').
card_rarity('undercity troll'/'ORI', 'Uncommon').
card_artist('undercity troll'/'ORI', 'Jason Felix').
card_number('undercity troll'/'ORI', '202').
card_multiverse_id('undercity troll'/'ORI', '398568').

card_in_set('unholy hunger', 'ORI').
card_original_type('unholy hunger'/'ORI', 'Instant').
card_original_text('unholy hunger'/'ORI', 'Destroy target creature.Spell mastery — If there are two or more instant and/or sorcery cards in your graveyard, you gain 2 life.').
card_first_print('unholy hunger', 'ORI').
card_image_name('unholy hunger'/'ORI', 'unholy hunger').
card_uid('unholy hunger'/'ORI', 'ORI:Unholy Hunger:unholy hunger').
card_rarity('unholy hunger'/'ORI', 'Common').
card_artist('unholy hunger'/'ORI', 'Lius Lasahido').
card_number('unholy hunger'/'ORI', '125').
card_flavor_text('unholy hunger'/'ORI', 'Liliana viewed the attention of the angels as a testament to her power. Even so, they were an annoyance.').
card_multiverse_id('unholy hunger'/'ORI', '398452').

card_in_set('valeron wardens', 'ORI').
card_original_type('valeron wardens'/'ORI', 'Creature — Human Monk').
card_original_text('valeron wardens'/'ORI', 'Renown 2 (When this creature deals combat damage to a player, if it isn\'t renowned, put two +1/+1 counters on it and it becomes renowned.)Whenever a creature you control becomes renowned, draw a card.').
card_first_print('valeron wardens', 'ORI').
card_image_name('valeron wardens'/'ORI', 'valeron wardens').
card_uid('valeron wardens'/'ORI', 'ORI:Valeron Wardens:valeron wardens').
card_rarity('valeron wardens'/'ORI', 'Uncommon').
card_artist('valeron wardens'/'ORI', 'Howard Lyon').
card_number('valeron wardens'/'ORI', '203').
card_multiverse_id('valeron wardens'/'ORI', '398447').

card_in_set('valor in akros', 'ORI').
card_original_type('valor in akros'/'ORI', 'Enchantment').
card_original_text('valor in akros'/'ORI', 'Whenever a creature enters the battlefield under your control, creatures you control get +1/+1 until end of turn.').
card_first_print('valor in akros', 'ORI').
card_image_name('valor in akros'/'ORI', 'valor in akros').
card_uid('valor in akros'/'ORI', 'ORI:Valor in Akros:valor in akros').
card_rarity('valor in akros'/'ORI', 'Uncommon').
card_artist('valor in akros'/'ORI', 'Igor Kieryluk').
card_number('valor in akros'/'ORI', '39').
card_flavor_text('valor in akros'/'ORI', 'They became a single entity, a phalanx in the Temple of Triumph standing against a host of enemies.').
card_multiverse_id('valor in akros'/'ORI', '398529').

card_in_set('vastwood gorger', 'ORI').
card_original_type('vastwood gorger'/'ORI', 'Creature — Wurm').
card_original_text('vastwood gorger'/'ORI', '').
card_image_name('vastwood gorger'/'ORI', 'vastwood gorger').
card_uid('vastwood gorger'/'ORI', 'ORI:Vastwood Gorger:vastwood gorger').
card_rarity('vastwood gorger'/'ORI', 'Common').
card_artist('vastwood gorger'/'ORI', 'Kieran Yanner').
card_number('vastwood gorger'/'ORI', '204').
card_flavor_text('vastwood gorger'/'ORI', '\"A long and difficult incision revealed that all vital organs are housed in the head, save for a long chain of stomachs, forty in all, leading from its throat to the end of the tail.\"—Mulak Ffar, Vastwood Biodiversity').
card_multiverse_id('vastwood gorger'/'ORI', '398469').

card_in_set('veteran\'s sidearm', 'ORI').
card_original_type('veteran\'s sidearm'/'ORI', 'Artifact — Equipment').
card_original_text('veteran\'s sidearm'/'ORI', 'Equipped creature gets +1/+1.Equip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('veteran\'s sidearm', 'ORI').
card_image_name('veteran\'s sidearm'/'ORI', 'veteran\'s sidearm').
card_uid('veteran\'s sidearm'/'ORI', 'ORI:Veteran\'s Sidearm:veteran\'s sidearm').
card_rarity('veteran\'s sidearm'/'ORI', 'Common').
card_artist('veteran\'s sidearm'/'ORI', 'Aaron Miller').
card_number('veteran\'s sidearm'/'ORI', '242').
card_flavor_text('veteran\'s sidearm'/'ORI', '\"I\'ve broken three swords, eighteen lances, and countless shields, but this little blade has survived every battle, just like I have.\"').
card_multiverse_id('veteran\'s sidearm'/'ORI', '398498').

card_in_set('vine snare', 'ORI').
card_original_type('vine snare'/'ORI', 'Instant').
card_original_text('vine snare'/'ORI', 'Prevent all combat damage that would be dealt this turn by creatures with power 4 or less.').
card_first_print('vine snare', 'ORI').
card_image_name('vine snare'/'ORI', 'vine snare').
card_uid('vine snare'/'ORI', 'ORI:Vine Snare:vine snare').
card_rarity('vine snare'/'ORI', 'Common').
card_artist('vine snare'/'ORI', 'Igor Kieryluk').
card_number('vine snare'/'ORI', '205').
card_flavor_text('vine snare'/'ORI', 'Nissa found that the vines of the marsh could ensnare just as well as forest vines could—maybe even better.').
card_multiverse_id('vine snare'/'ORI', '398658').

card_in_set('volcanic rambler', 'ORI').
card_original_type('volcanic rambler'/'ORI', 'Creature — Elemental').
card_original_text('volcanic rambler'/'ORI', '{2}{R}: Volcanic Rambler deals 1 damage to target player.').
card_first_print('volcanic rambler', 'ORI').
card_image_name('volcanic rambler'/'ORI', 'volcanic rambler').
card_uid('volcanic rambler'/'ORI', 'ORI:Volcanic Rambler:volcanic rambler').
card_rarity('volcanic rambler'/'ORI', 'Common').
card_artist('volcanic rambler'/'ORI', 'Vincent Proce').
card_number('volcanic rambler'/'ORI', '167').
card_flavor_text('volcanic rambler'/'ORI', 'It moves through lava with the force of an erupting volcano.').
card_multiverse_id('volcanic rambler'/'ORI', '398636').

card_in_set('vryn wingmare', 'ORI').
card_original_type('vryn wingmare'/'ORI', 'Creature — Pegasus').
card_original_text('vryn wingmare'/'ORI', 'FlyingNoncreature spells cost {1} more to cast.').
card_first_print('vryn wingmare', 'ORI').
card_image_name('vryn wingmare'/'ORI', 'vryn wingmare').
card_uid('vryn wingmare'/'ORI', 'ORI:Vryn Wingmare:vryn wingmare').
card_rarity('vryn wingmare'/'ORI', 'Rare').
card_artist('vryn wingmare'/'ORI', 'Seb McKinnon').
card_number('vryn wingmare'/'ORI', '40').
card_flavor_text('vryn wingmare'/'ORI', 'It\'s the favored mount of military commanders as well as anyone with a flair for the dramatic.').
card_multiverse_id('vryn wingmare'/'ORI', '398567').

card_in_set('war horn', 'ORI').
card_original_type('war horn'/'ORI', 'Artifact').
card_original_text('war horn'/'ORI', 'Attacking creatures you control get +1/+0.').
card_first_print('war horn', 'ORI').
card_image_name('war horn'/'ORI', 'war horn').
card_uid('war horn'/'ORI', 'ORI:War Horn:war horn').
card_rarity('war horn'/'ORI', 'Uncommon').
card_artist('war horn'/'ORI', 'Lars Grant-West').
card_number('war horn'/'ORI', '243').
card_flavor_text('war horn'/'ORI', 'The reverberations course through the warriors\' veins, beat with their hearts, and pound with their footfalls as they charge into battle.').
card_multiverse_id('war horn'/'ORI', '398673').

card_in_set('war oracle', 'ORI').
card_original_type('war oracle'/'ORI', 'Creature — Human Cleric').
card_original_text('war oracle'/'ORI', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_first_print('war oracle', 'ORI').
card_image_name('war oracle'/'ORI', 'war oracle').
card_uid('war oracle'/'ORI', 'ORI:War Oracle:war oracle').
card_rarity('war oracle'/'ORI', 'Uncommon').
card_artist('war oracle'/'ORI', 'Steve Prescott').
card_number('war oracle'/'ORI', '41').
card_flavor_text('war oracle'/'ORI', '\"When you are felled by my mace, you shall know it was divine fate.\"').
card_multiverse_id('war oracle'/'ORI', '398621').

card_in_set('watercourser', 'ORI').
card_original_type('watercourser'/'ORI', 'Creature — Elemental').
card_original_text('watercourser'/'ORI', '{U}: Watercourser gets +1/-1 until end of turn.').
card_image_name('watercourser'/'ORI', 'watercourser').
card_uid('watercourser'/'ORI', 'ORI:Watercourser:watercourser').
card_rarity('watercourser'/'ORI', 'Common').
card_artist('watercourser'/'ORI', 'Mathias Kollros').
card_number('watercourser'/'ORI', '82').
card_flavor_text('watercourser'/'ORI', '\"Beware an eddy where there should be none or a stretch that flows too fast or too slow.\"—Old Fishbones, Martyne river guide').
card_multiverse_id('watercourser'/'ORI', '398540').

card_in_set('weave fate', 'ORI').
card_original_type('weave fate'/'ORI', 'Instant').
card_original_text('weave fate'/'ORI', 'Draw two cards.').
card_image_name('weave fate'/'ORI', 'weave fate').
card_uid('weave fate'/'ORI', 'ORI:Weave Fate:weave fate').
card_rarity('weave fate'/'ORI', 'Common').
card_artist('weave fate'/'ORI', 'Zack Stella').
card_number('weave fate'/'ORI', '279').
card_flavor_text('weave fate'/'ORI', 'Destiny is a flickering path among tangled possibilities.').
card_multiverse_id('weave fate'/'ORI', '401458').

card_in_set('weight of the underworld', 'ORI').
card_original_type('weight of the underworld'/'ORI', 'Enchantment — Aura').
card_original_text('weight of the underworld'/'ORI', 'Enchant creatureEnchanted creature gets -3/-2.').
card_image_name('weight of the underworld'/'ORI', 'weight of the underworld').
card_uid('weight of the underworld'/'ORI', 'ORI:Weight of the Underworld:weight of the underworld').
card_rarity('weight of the underworld'/'ORI', 'Common').
card_artist('weight of the underworld'/'ORI', 'Wesley Burt').
card_number('weight of the underworld'/'ORI', '126').
card_flavor_text('weight of the underworld'/'ORI', 'Proud Alkmenos, who would not bow to Erebos in death, is now bowed by his own hubris for all eternity.').
card_multiverse_id('weight of the underworld'/'ORI', '398544').

card_in_set('whirler rogue', 'ORI').
card_original_type('whirler rogue'/'ORI', 'Creature — Human Rogue Artificer').
card_original_text('whirler rogue'/'ORI', 'When Whirler Rogue enters the battlefield, put two 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.Tap two untapped artifacts you control: Target creature can\'t be blocked this turn.').
card_first_print('whirler rogue', 'ORI').
card_image_name('whirler rogue'/'ORI', 'whirler rogue').
card_uid('whirler rogue'/'ORI', 'ORI:Whirler Rogue:whirler rogue').
card_rarity('whirler rogue'/'ORI', 'Uncommon').
card_artist('whirler rogue'/'ORI', 'Winona Nelson').
card_number('whirler rogue'/'ORI', '83').
card_multiverse_id('whirler rogue'/'ORI', '398410').

card_in_set('wild instincts', 'ORI').
card_original_type('wild instincts'/'ORI', 'Sorcery').
card_original_text('wild instincts'/'ORI', 'Target creature you control gets +2/+2 until end of turn. It fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_first_print('wild instincts', 'ORI').
card_image_name('wild instincts'/'ORI', 'wild instincts').
card_uid('wild instincts'/'ORI', 'ORI:Wild Instincts:wild instincts').
card_rarity('wild instincts'/'ORI', 'Common').
card_artist('wild instincts'/'ORI', 'Igor Kieryluk').
card_number('wild instincts'/'ORI', '206').
card_flavor_text('wild instincts'/'ORI', 'Nissa had only the space of a heartbeat to react, her instincts steering her blade.').
card_multiverse_id('wild instincts'/'ORI', '398508').

card_in_set('willbreaker', 'ORI').
card_original_type('willbreaker'/'ORI', 'Creature — Human Wizard').
card_original_text('willbreaker'/'ORI', 'Whenever a creature an opponent controls becomes the target of a spell or ability you control, gain control of that creature for as long as you control Willbreaker.').
card_first_print('willbreaker', 'ORI').
card_image_name('willbreaker'/'ORI', 'willbreaker').
card_uid('willbreaker'/'ORI', 'ORI:Willbreaker:willbreaker').
card_rarity('willbreaker'/'ORI', 'Rare').
card_artist('willbreaker'/'ORI', 'Dan Scott').
card_number('willbreaker'/'ORI', '84').
card_flavor_text('willbreaker'/'ORI', '\"Master your mind, or I shall master it for you.\"').
card_multiverse_id('willbreaker'/'ORI', '398502').

card_in_set('woodland bellower', 'ORI').
card_original_type('woodland bellower'/'ORI', 'Creature — Beast').
card_original_text('woodland bellower'/'ORI', 'When Woodland Bellower enters the battlefield, you may search your library for a nonlegendary green creature card with converted mana cost 3 or less, put it onto the battlefield, then shuffle your library.').
card_first_print('woodland bellower', 'ORI').
card_image_name('woodland bellower'/'ORI', 'woodland bellower').
card_uid('woodland bellower'/'ORI', 'ORI:Woodland Bellower:woodland bellower').
card_rarity('woodland bellower'/'ORI', 'Mythic Rare').
card_artist('woodland bellower'/'ORI', 'Jasper Sandner').
card_number('woodland bellower'/'ORI', '207').
card_multiverse_id('woodland bellower'/'ORI', '398511').

card_in_set('yavimaya coast', 'ORI').
card_original_type('yavimaya coast'/'ORI', 'Land').
card_original_text('yavimaya coast'/'ORI', '{T}: Add {1} to your mana pool.{T}: Add {G} or {U} to your mana pool. Yavimaya Coast deals 1 damage to you.').
card_image_name('yavimaya coast'/'ORI', 'yavimaya coast').
card_uid('yavimaya coast'/'ORI', 'ORI:Yavimaya Coast:yavimaya coast').
card_rarity('yavimaya coast'/'ORI', 'Rare').
card_artist('yavimaya coast'/'ORI', 'Anthony S. Waters').
card_number('yavimaya coast'/'ORI', '252').
card_multiverse_id('yavimaya coast'/'ORI', '398566').

card_in_set('yeva\'s forcemage', 'ORI').
card_original_type('yeva\'s forcemage'/'ORI', 'Creature — Elf Shaman').
card_original_text('yeva\'s forcemage'/'ORI', 'When Yeva\'s Forcemage enters the battlefield, target creature gets +2/+2 until end of turn.').
card_image_name('yeva\'s forcemage'/'ORI', 'yeva\'s forcemage').
card_uid('yeva\'s forcemage'/'ORI', 'ORI:Yeva\'s Forcemage:yeva\'s forcemage').
card_rarity('yeva\'s forcemage'/'ORI', 'Common').
card_artist('yeva\'s forcemage'/'ORI', 'Eric Deschamps').
card_number('yeva\'s forcemage'/'ORI', '208').
card_flavor_text('yeva\'s forcemage'/'ORI', '\"Nature can\'t be stopped. It rips and tears at Ravnica\'s tallest buildings to claim its place in the sun.\"').
card_multiverse_id('yeva\'s forcemage'/'ORI', '398424').

card_in_set('yoked ox', 'ORI').
card_original_type('yoked ox'/'ORI', 'Creature — Ox').
card_original_text('yoked ox'/'ORI', '').
card_image_name('yoked ox'/'ORI', 'yoked ox').
card_uid('yoked ox'/'ORI', 'ORI:Yoked Ox:yoked ox').
card_rarity('yoked ox'/'ORI', 'Common').
card_artist('yoked ox'/'ORI', 'Ryan Yee').
card_number('yoked ox'/'ORI', '42').
card_flavor_text('yoked ox'/'ORI', 'It was in fields of grain, not fields of battle, that the Champion learned to bear the yoke of duty to the gods. She worked the land long before she was called on to defend it.—The Theriad').
card_multiverse_id('yoked ox'/'ORI', '398671').

card_in_set('zendikar incarnate', 'ORI').
card_original_type('zendikar incarnate'/'ORI', 'Creature — Elemental').
card_original_text('zendikar incarnate'/'ORI', 'Zendikar Incarnate\'s power is equal to the number of lands you control.').
card_first_print('zendikar incarnate', 'ORI').
card_image_name('zendikar incarnate'/'ORI', 'zendikar incarnate').
card_uid('zendikar incarnate'/'ORI', 'ORI:Zendikar Incarnate:zendikar incarnate').
card_rarity('zendikar incarnate'/'ORI', 'Uncommon').
card_artist('zendikar incarnate'/'ORI', 'Lucas Graciano').
card_number('zendikar incarnate'/'ORI', '219').
card_flavor_text('zendikar incarnate'/'ORI', '\"Her people angered Zendikar, and they faced the land\'s wrath. That is why Nissa is the last of the animists.\"—Numa, Joraga chieftain').
card_multiverse_id('zendikar incarnate'/'ORI', '398661').

card_in_set('zendikar\'s roil', 'ORI').
card_original_type('zendikar\'s roil'/'ORI', 'Enchantment').
card_original_text('zendikar\'s roil'/'ORI', 'Whenever a land enters the battlefield under your control, put a 2/2 green Elemental creature token onto the battlefield.').
card_first_print('zendikar\'s roil', 'ORI').
card_image_name('zendikar\'s roil'/'ORI', 'zendikar\'s roil').
card_uid('zendikar\'s roil'/'ORI', 'ORI:Zendikar\'s Roil:zendikar\'s roil').
card_rarity('zendikar\'s roil'/'ORI', 'Uncommon').
card_artist('zendikar\'s roil'/'ORI', 'Sam Burley').
card_number('zendikar\'s roil'/'ORI', '209').
card_flavor_text('zendikar\'s roil'/'ORI', '\"I was wrong. Zendikar isn\'t after me. It isn\'t after any of us. It\'s not evil or vengeful. It\'s magnificent . . . but it\'s in pain.\"—Nissa Revane').
card_multiverse_id('zendikar\'s roil'/'ORI', '398518').
