% Rise of the Eldrazi

set('ROE').
set_name('ROE', 'Rise of the Eldrazi').
set_release_date('ROE', '2010-04-23').
set_border('ROE', 'black').
set_type('ROE', 'expansion').
set_block('ROE', 'Zendikar').

card_in_set('affa guard hound', 'ROE').
card_original_type('affa guard hound'/'ROE', 'Creature — Hound').
card_original_text('affa guard hound'/'ROE', 'Flash\nWhen Affa Guard Hound enters the battlefield, target creature gets +0/+3 until end of turn.').
card_first_print('affa guard hound', 'ROE').
card_image_name('affa guard hound'/'ROE', 'affa guard hound').
card_uid('affa guard hound'/'ROE', 'ROE:Affa Guard Hound:affa guard hound').
card_rarity('affa guard hound'/'ROE', 'Uncommon').
card_artist('affa guard hound'/'ROE', 'Ryan Pancoast').
card_number('affa guard hound'/'ROE', '14').
card_flavor_text('affa guard hound'/'ROE', 'Once a welcoming hub for explorers, Affa became a place of guarded tongues and quick defenses.').
card_multiverse_id('affa guard hound'/'ROE', '198305').

card_in_set('akoum boulderfoot', 'ROE').
card_original_type('akoum boulderfoot'/'ROE', 'Creature — Giant Warrior').
card_original_text('akoum boulderfoot'/'ROE', 'When Akoum Boulderfoot enters the battlefield, it deals 1 damage to target creature or player.').
card_first_print('akoum boulderfoot', 'ROE').
card_image_name('akoum boulderfoot'/'ROE', 'akoum boulderfoot').
card_uid('akoum boulderfoot'/'ROE', 'ROE:Akoum Boulderfoot:akoum boulderfoot').
card_rarity('akoum boulderfoot'/'ROE', 'Uncommon').
card_artist('akoum boulderfoot'/'ROE', 'Alex Horley-Orlandelli').
card_number('akoum boulderfoot'/'ROE', '134').
card_flavor_text('akoum boulderfoot'/'ROE', '\"If you\'re in Akoum and you glimpse a shadow overhead, the worst thing you can do is duck.\"\n—Chadir the Navigator').
card_multiverse_id('akoum boulderfoot'/'ROE', '193506').

card_in_set('all is dust', 'ROE').
card_original_type('all is dust'/'ROE', 'Tribal Sorcery — Eldrazi').
card_original_text('all is dust'/'ROE', 'Each player sacrifices all colored permanents he or she controls.').
card_image_name('all is dust'/'ROE', 'all is dust').
card_uid('all is dust'/'ROE', 'ROE:All Is Dust:all is dust').
card_rarity('all is dust'/'ROE', 'Mythic Rare').
card_artist('all is dust'/'ROE', 'Jason Felix').
card_number('all is dust'/'ROE', '1').
card_flavor_text('all is dust'/'ROE', '\"The emergence of the Eldrazi isn\'t necessarily a bad thing, as long as you\'ve already lived a fulfilling and complete life without regrets.\"\n—Javad Nasrin, Ondu relic hunter').
card_multiverse_id('all is dust'/'ROE', '193658').

card_in_set('ancient stirrings', 'ROE').
card_original_type('ancient stirrings'/'ROE', 'Sorcery').
card_original_text('ancient stirrings'/'ROE', 'Look at the top five cards of your library. You may reveal a colorless card from among them and put it into your hand. Then put the rest on the bottom of your library in any order. (Cards with no colored mana in their mana costs are colorless. Lands are also colorless.)').
card_first_print('ancient stirrings', 'ROE').
card_image_name('ancient stirrings'/'ROE', 'ancient stirrings').
card_uid('ancient stirrings'/'ROE', 'ROE:Ancient Stirrings:ancient stirrings').
card_rarity('ancient stirrings'/'ROE', 'Common').
card_artist('ancient stirrings'/'ROE', 'Vincent Proce').
card_number('ancient stirrings'/'ROE', '174').
card_multiverse_id('ancient stirrings'/'ROE', '193437').

card_in_set('angelheart vial', 'ROE').
card_original_type('angelheart vial'/'ROE', 'Artifact').
card_original_text('angelheart vial'/'ROE', 'Whenever you\'re dealt damage, you may put that many charge counters on Angelheart Vial.\n{2}, {T}, Remove four charge counters from Angelheart Vial: You gain 2 life and draw a card.').
card_first_print('angelheart vial', 'ROE').
card_image_name('angelheart vial'/'ROE', 'angelheart vial').
card_uid('angelheart vial'/'ROE', 'ROE:Angelheart Vial:angelheart vial').
card_rarity('angelheart vial'/'ROE', 'Rare').
card_artist('angelheart vial'/'ROE', 'Chippy').
card_number('angelheart vial'/'ROE', '215').
card_flavor_text('angelheart vial'/'ROE', 'It holds a whisper from Iona: \"Persevere.\"').
card_multiverse_id('angelheart vial'/'ROE', '193436').

card_in_set('arrogant bloodlord', 'ROE').
card_original_type('arrogant bloodlord'/'ROE', 'Creature — Vampire Knight').
card_original_text('arrogant bloodlord'/'ROE', 'Whenever Arrogant Bloodlord blocks or becomes blocked by a creature with power 1 or less, destroy Arrogant Bloodlord at end of combat.').
card_first_print('arrogant bloodlord', 'ROE').
card_image_name('arrogant bloodlord'/'ROE', 'arrogant bloodlord').
card_uid('arrogant bloodlord'/'ROE', 'ROE:Arrogant Bloodlord:arrogant bloodlord').
card_rarity('arrogant bloodlord'/'ROE', 'Uncommon').
card_artist('arrogant bloodlord'/'ROE', 'Mike Bierek').
card_number('arrogant bloodlord'/'ROE', '94').
card_flavor_text('arrogant bloodlord'/'ROE', '\"I would rather take my own head than be thwarted by a presumptuous wretch with a shield and a dream.\"').
card_multiverse_id('arrogant bloodlord'/'ROE', '193511').

card_in_set('artisan of kozilek', 'ROE').
card_original_type('artisan of kozilek'/'ROE', 'Creature — Eldrazi').
card_original_text('artisan of kozilek'/'ROE', 'When you cast Artisan of Kozilek, you may return target creature card from your graveyard to the battlefield.\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)').
card_image_name('artisan of kozilek'/'ROE', 'artisan of kozilek').
card_uid('artisan of kozilek'/'ROE', 'ROE:Artisan of Kozilek:artisan of kozilek').
card_rarity('artisan of kozilek'/'ROE', 'Uncommon').
card_artist('artisan of kozilek'/'ROE', 'Jason Felix').
card_number('artisan of kozilek'/'ROE', '2').
card_multiverse_id('artisan of kozilek'/'ROE', '193429').

card_in_set('aura finesse', 'ROE').
card_original_type('aura finesse'/'ROE', 'Instant').
card_original_text('aura finesse'/'ROE', 'Attach target Aura you control to target creature.\nDraw a card.').
card_first_print('aura finesse', 'ROE').
card_image_name('aura finesse'/'ROE', 'aura finesse').
card_uid('aura finesse'/'ROE', 'ROE:Aura Finesse:aura finesse').
card_rarity('aura finesse'/'ROE', 'Common').
card_artist('aura finesse'/'ROE', 'Howard Lyon').
card_number('aura finesse'/'ROE', '54').
card_flavor_text('aura finesse'/'ROE', '\"Although armor and auras can be similar, the brains required to use them are worlds apart.\"\n—Zilan, Makindi auramancer').
card_multiverse_id('aura finesse'/'ROE', '193599').

card_in_set('aura gnarlid', 'ROE').
card_original_type('aura gnarlid'/'ROE', 'Creature — Beast').
card_original_text('aura gnarlid'/'ROE', 'Creatures with power less than Aura Gnarlid\'s power can\'t block it.\nAura Gnarlid gets +1/+1 for each Aura on the battlefield.').
card_first_print('aura gnarlid', 'ROE').
card_image_name('aura gnarlid'/'ROE', 'aura gnarlid').
card_uid('aura gnarlid'/'ROE', 'ROE:Aura Gnarlid:aura gnarlid').
card_rarity('aura gnarlid'/'ROE', 'Common').
card_artist('aura gnarlid'/'ROE', 'Lars Grant-West').
card_number('aura gnarlid'/'ROE', '175').
card_flavor_text('aura gnarlid'/'ROE', 'Kill a gnarlid with your first blow, or it\'ll cheerfully show you how it\'s done.').
card_multiverse_id('aura gnarlid'/'ROE', '193478').

card_in_set('awakening zone', 'ROE').
card_original_type('awakening zone'/'ROE', 'Enchantment').
card_original_text('awakening zone'/'ROE', 'At the beginning of your upkeep, you may put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('awakening zone', 'ROE').
card_image_name('awakening zone'/'ROE', 'awakening zone').
card_uid('awakening zone'/'ROE', 'ROE:Awakening Zone:awakening zone').
card_rarity('awakening zone'/'ROE', 'Rare').
card_artist('awakening zone'/'ROE', 'Johann Bodin').
card_number('awakening zone'/'ROE', '176').
card_flavor_text('awakening zone'/'ROE', 'The ground erupted in time with the hedron\'s thrum, a dirge of the last days.').
card_multiverse_id('awakening zone'/'ROE', '193507').

card_in_set('bala ged scorpion', 'ROE').
card_original_type('bala ged scorpion'/'ROE', 'Creature — Scorpion').
card_original_text('bala ged scorpion'/'ROE', 'When Bala Ged Scorpion enters the battlefield, you may destroy target creature with power 1 or less.').
card_first_print('bala ged scorpion', 'ROE').
card_image_name('bala ged scorpion'/'ROE', 'bala ged scorpion').
card_uid('bala ged scorpion'/'ROE', 'ROE:Bala Ged Scorpion:bala ged scorpion').
card_rarity('bala ged scorpion'/'ROE', 'Common').
card_artist('bala ged scorpion'/'ROE', 'Daarken').
card_number('bala ged scorpion'/'ROE', '95').
card_flavor_text('bala ged scorpion'/'ROE', 'Fast and lethal, with a penchant for the weak and infirm.').
card_multiverse_id('bala ged scorpion'/'ROE', '194944').

card_in_set('baneful omen', 'ROE').
card_original_type('baneful omen'/'ROE', 'Enchantment').
card_original_text('baneful omen'/'ROE', 'At the beginning of your end step, you may reveal the top card of your library. If you do, each opponent loses life equal to that card\'s converted mana cost.').
card_first_print('baneful omen', 'ROE').
card_image_name('baneful omen'/'ROE', 'baneful omen').
card_uid('baneful omen'/'ROE', 'ROE:Baneful Omen:baneful omen').
card_rarity('baneful omen'/'ROE', 'Rare').
card_artist('baneful omen'/'ROE', 'Karl Kopinski').
card_number('baneful omen'/'ROE', '96').
card_flavor_text('baneful omen'/'ROE', '\"Ah, again it does not bode well for you.\"').
card_multiverse_id('baneful omen'/'ROE', '198302').

card_in_set('battle rampart', 'ROE').
card_original_type('battle rampart'/'ROE', 'Creature — Wall').
card_original_text('battle rampart'/'ROE', 'Defender\n{T}: Target creature gains haste until end of turn.').
card_image_name('battle rampart'/'ROE', 'battle rampart').
card_uid('battle rampart'/'ROE', 'ROE:Battle Rampart:battle rampart').
card_rarity('battle rampart'/'ROE', 'Common').
card_artist('battle rampart'/'ROE', 'Steve Prescott').
card_number('battle rampart'/'ROE', '135').
card_flavor_text('battle rampart'/'ROE', '\"I will build no sanctuaries. The safety you seek can be won only by the sword.\"\n—Yannash, outpost builder').
card_multiverse_id('battle rampart'/'ROE', '193554').

card_in_set('battle-rattle shaman', 'ROE').
card_original_type('battle-rattle shaman'/'ROE', 'Creature — Goblin Shaman').
card_original_text('battle-rattle shaman'/'ROE', 'At the beginning of combat on your turn, you may have target creature get +2/+0 until end of turn.').
card_first_print('battle-rattle shaman', 'ROE').
card_image_name('battle-rattle shaman'/'ROE', 'battle-rattle shaman').
card_uid('battle-rattle shaman'/'ROE', 'ROE:Battle-Rattle Shaman:battle-rattle shaman').
card_rarity('battle-rattle shaman'/'ROE', 'Common').
card_artist('battle-rattle shaman'/'ROE', 'Warren Mahy').
card_number('battle-rattle shaman'/'ROE', '136').
card_flavor_text('battle-rattle shaman'/'ROE', 'The shaman rattles not to inspire a battle fury but to drown out the shrieks of those fleeing the Eldrazi.').
card_multiverse_id('battle-rattle shaman'/'ROE', '193523').

card_in_set('bear umbra', 'ROE').
card_original_type('bear umbra'/'ROE', 'Enchantment — Aura').
card_original_text('bear umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"Whenever this creature attacks, untap all lands you control.\"\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('bear umbra', 'ROE').
card_image_name('bear umbra'/'ROE', 'bear umbra').
card_uid('bear umbra'/'ROE', 'ROE:Bear Umbra:bear umbra').
card_rarity('bear umbra'/'ROE', 'Rare').
card_artist('bear umbra'/'ROE', 'Howard Lyon').
card_number('bear umbra'/'ROE', '177').
card_multiverse_id('bear umbra'/'ROE', '198304').

card_in_set('beastbreaker of bala ged', 'ROE').
card_original_type('beastbreaker of bala ged'/'ROE', 'Creature — Human Warrior').
card_original_text('beastbreaker of bala ged'/'ROE', 'Level up {2}{G} ({2}{G}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n4/4\nLEVEL 4+\n6/6\nTrample').
card_first_print('beastbreaker of bala ged', 'ROE').
card_image_name('beastbreaker of bala ged'/'ROE', 'beastbreaker of bala ged').
card_uid('beastbreaker of bala ged'/'ROE', 'ROE:Beastbreaker of Bala Ged:beastbreaker of bala ged').
card_rarity('beastbreaker of bala ged'/'ROE', 'Uncommon').
card_artist('beastbreaker of bala ged'/'ROE', 'Karl Kopinski').
card_number('beastbreaker of bala ged'/'ROE', '178').
card_multiverse_id('beastbreaker of bala ged'/'ROE', '193638').

card_in_set('bloodrite invoker', 'ROE').
card_original_type('bloodrite invoker'/'ROE', 'Creature — Vampire Shaman').
card_original_text('bloodrite invoker'/'ROE', '{8}: Target player loses 3 life and you gain 3 life.').
card_first_print('bloodrite invoker', 'ROE').
card_image_name('bloodrite invoker'/'ROE', 'bloodrite invoker').
card_uid('bloodrite invoker'/'ROE', 'ROE:Bloodrite Invoker:bloodrite invoker').
card_rarity('bloodrite invoker'/'ROE', 'Common').
card_artist('bloodrite invoker'/'ROE', 'Svetlin Velinov').
card_number('bloodrite invoker'/'ROE', '97').
card_flavor_text('bloodrite invoker'/'ROE', '\"The brood lineages unfolded across the world, each patterned after one of three progenitors, each a study in mindless consumption.\"\n—The Invokers\' Tales').
card_multiverse_id('bloodrite invoker'/'ROE', '193538').

card_in_set('bloodthrone vampire', 'ROE').
card_original_type('bloodthrone vampire'/'ROE', 'Creature — Vampire').
card_original_text('bloodthrone vampire'/'ROE', 'Sacrifice a creature: Bloodthrone Vampire gets +2/+2 until end of turn.').
card_image_name('bloodthrone vampire'/'ROE', 'bloodthrone vampire').
card_uid('bloodthrone vampire'/'ROE', 'ROE:Bloodthrone Vampire:bloodthrone vampire').
card_rarity('bloodthrone vampire'/'ROE', 'Common').
card_artist('bloodthrone vampire'/'ROE', 'Steve Argyle').
card_number('bloodthrone vampire'/'ROE', '98').
card_flavor_text('bloodthrone vampire'/'ROE', 'Some humans willingly offered up their blood, hoping it would grant the vampire families the strength to stave off the Eldrazi.').
card_multiverse_id('bloodthrone vampire'/'ROE', '198308').

card_in_set('boar umbra', 'ROE').
card_original_type('boar umbra'/'ROE', 'Enchantment — Aura').
card_original_text('boar umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +3/+3.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('boar umbra', 'ROE').
card_image_name('boar umbra'/'ROE', 'boar umbra').
card_uid('boar umbra'/'ROE', 'ROE:Boar Umbra:boar umbra').
card_rarity('boar umbra'/'ROE', 'Uncommon').
card_artist('boar umbra'/'ROE', 'Christopher Moeller').
card_number('boar umbra'/'ROE', '179').
card_flavor_text('boar umbra'/'ROE', '\"From in here, everything looks like dinner.\"').
card_multiverse_id('boar umbra'/'ROE', '193509').

card_in_set('bramblesnap', 'ROE').
card_original_type('bramblesnap'/'ROE', 'Creature — Elemental').
card_original_text('bramblesnap'/'ROE', 'Trample\nTap an untapped creature you control: Bramblesnap gets +1/+1 until end of turn.').
card_first_print('bramblesnap', 'ROE').
card_image_name('bramblesnap'/'ROE', 'bramblesnap').
card_uid('bramblesnap'/'ROE', 'ROE:Bramblesnap:bramblesnap').
card_rarity('bramblesnap'/'ROE', 'Uncommon').
card_artist('bramblesnap'/'ROE', 'James Ryman').
card_number('bramblesnap'/'ROE', '180').
card_flavor_text('bramblesnap'/'ROE', 'A bramblesnap is formed by grafting together thirteen different plants that already hunger for meat.').
card_multiverse_id('bramblesnap'/'ROE', '193465').

card_in_set('brimstone mage', 'ROE').
card_original_type('brimstone mage'/'ROE', 'Creature — Human Shaman').
card_original_text('brimstone mage'/'ROE', 'Level up {3}{R} ({3}{R}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n2/3\n{T}: Brimstone Mage deals 1 damage to target creature or player.\nLEVEL 3+\n2/4\n{T}: Brimstone Mage deals 3 damage to target creature or player.').
card_first_print('brimstone mage', 'ROE').
card_image_name('brimstone mage'/'ROE', 'brimstone mage').
card_uid('brimstone mage'/'ROE', 'ROE:Brimstone Mage:brimstone mage').
card_rarity('brimstone mage'/'ROE', 'Uncommon').
card_artist('brimstone mage'/'ROE', 'Volkan Baga').
card_number('brimstone mage'/'ROE', '137').
card_multiverse_id('brimstone mage'/'ROE', '193442').

card_in_set('brood birthing', 'ROE').
card_original_type('brood birthing'/'ROE', 'Sorcery').
card_original_text('brood birthing'/'ROE', 'If you control an Eldrazi Spawn, put three 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\" Otherwise, put one of those tokens onto the battlefield.').
card_first_print('brood birthing', 'ROE').
card_image_name('brood birthing'/'ROE', 'brood birthing').
card_uid('brood birthing'/'ROE', 'ROE:Brood Birthing:brood birthing').
card_rarity('brood birthing'/'ROE', 'Common').
card_artist('brood birthing'/'ROE', 'Anthony Francisco').
card_number('brood birthing'/'ROE', '138').
card_multiverse_id('brood birthing'/'ROE', '193633').

card_in_set('broodwarden', 'ROE').
card_original_type('broodwarden'/'ROE', 'Creature — Eldrazi Drone').
card_original_text('broodwarden'/'ROE', 'Eldrazi Spawn creatures you control get +2/+1.').
card_first_print('broodwarden', 'ROE').
card_image_name('broodwarden'/'ROE', 'broodwarden').
card_uid('broodwarden'/'ROE', 'ROE:Broodwarden:broodwarden').
card_rarity('broodwarden'/'ROE', 'Uncommon').
card_artist('broodwarden'/'ROE', 'Izzy').
card_number('broodwarden'/'ROE', '181').
card_flavor_text('broodwarden'/'ROE', '\"The least of gods shall tower over the mightiest of mortals, and death shall reign over all.\"\n—Crypt of Agadeem inscription').
card_multiverse_id('broodwarden'/'ROE', '193634').

card_in_set('cadaver imp', 'ROE').
card_original_type('cadaver imp'/'ROE', 'Creature — Imp').
card_original_text('cadaver imp'/'ROE', 'Flying\nWhen Cadaver Imp enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_first_print('cadaver imp', 'ROE').
card_image_name('cadaver imp'/'ROE', 'cadaver imp').
card_uid('cadaver imp'/'ROE', 'ROE:Cadaver Imp:cadaver imp').
card_rarity('cadaver imp'/'ROE', 'Common').
card_artist('cadaver imp'/'ROE', 'Dave Kendall').
card_number('cadaver imp'/'ROE', '99').
card_flavor_text('cadaver imp'/'ROE', 'The mouth must be pried open before rigor mortis sets in. Otherwise the returning soul can find no ingress.').
card_multiverse_id('cadaver imp'/'ROE', '193515').

card_in_set('caravan escort', 'ROE').
card_original_type('caravan escort'/'ROE', 'Creature — Human Knight').
card_original_text('caravan escort'/'ROE', 'Level up {2} ({2}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n2/2\nLEVEL 5+\n5/5\nFirst strike').
card_first_print('caravan escort', 'ROE').
card_image_name('caravan escort'/'ROE', 'caravan escort').
card_uid('caravan escort'/'ROE', 'ROE:Caravan Escort:caravan escort').
card_rarity('caravan escort'/'ROE', 'Common').
card_artist('caravan escort'/'ROE', 'Goran Josic').
card_number('caravan escort'/'ROE', '15').
card_multiverse_id('caravan escort'/'ROE', '194918').

card_in_set('cast through time', 'ROE').
card_original_type('cast through time'/'ROE', 'Enchantment').
card_original_text('cast through time'/'ROE', 'Instant and sorcery spells you control have rebound. (Exile the spell as it resolves if you cast it from your hand. At the beginning of your next upkeep, you may cast that card from exile without paying its mana cost.)').
card_first_print('cast through time', 'ROE').
card_image_name('cast through time'/'ROE', 'cast through time').
card_uid('cast through time'/'ROE', 'ROE:Cast Through Time:cast through time').
card_rarity('cast through time'/'ROE', 'Mythic Rare').
card_artist('cast through time'/'ROE', 'Zoltan Boros & Gabor Szikszai').
card_number('cast through time'/'ROE', '55').
card_multiverse_id('cast through time'/'ROE', '198301').

card_in_set('champion\'s drake', 'ROE').
card_original_type('champion\'s drake'/'ROE', 'Creature — Drake').
card_original_text('champion\'s drake'/'ROE', 'Flying\nChampion\'s Drake gets +3/+3 as long as you control a creature with three or more level counters on it.').
card_first_print('champion\'s drake', 'ROE').
card_image_name('champion\'s drake'/'ROE', 'champion\'s drake').
card_uid('champion\'s drake'/'ROE', 'ROE:Champion\'s Drake:champion\'s drake').
card_rarity('champion\'s drake'/'ROE', 'Common').
card_artist('champion\'s drake'/'ROE', 'Lars Grant-West').
card_number('champion\'s drake'/'ROE', '56').
card_flavor_text('champion\'s drake'/'ROE', '\"\'Fraid of heights? Ride a horse. \'Fraid of missing out? Rein up a drake.\"\n—Trinda, Hada spy patrol').
card_multiverse_id('champion\'s drake'/'ROE', '193440').

card_in_set('conquering manticore', 'ROE').
card_original_type('conquering manticore'/'ROE', 'Creature — Manticore').
card_original_text('conquering manticore'/'ROE', 'Flying\nWhen Conquering Manticore enters the battlefield, gain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn.').
card_first_print('conquering manticore', 'ROE').
card_image_name('conquering manticore'/'ROE', 'conquering manticore').
card_uid('conquering manticore'/'ROE', 'ROE:Conquering Manticore:conquering manticore').
card_rarity('conquering manticore'/'ROE', 'Rare').
card_artist('conquering manticore'/'ROE', 'Chris Rahn').
card_number('conquering manticore'/'ROE', '139').
card_flavor_text('conquering manticore'/'ROE', 'It speaks no commands but is always obeyed.').
card_multiverse_id('conquering manticore'/'ROE', '193468').

card_in_set('consume the meek', 'ROE').
card_original_type('consume the meek'/'ROE', 'Instant').
card_original_text('consume the meek'/'ROE', 'Destroy each creature with converted mana cost 3 or less. They can\'t be regenerated.').
card_first_print('consume the meek', 'ROE').
card_image_name('consume the meek'/'ROE', 'consume the meek').
card_uid('consume the meek'/'ROE', 'ROE:Consume the Meek:consume the meek').
card_rarity('consume the meek'/'ROE', 'Rare').
card_artist('consume the meek'/'ROE', 'Chippy').
card_number('consume the meek'/'ROE', '100').
card_flavor_text('consume the meek'/'ROE', '\"Why does it destroy? It does, and to talk of reasons wastes time and breath.\"\n—Nirthu, lone missionary').
card_multiverse_id('consume the meek'/'ROE', '193475').

card_in_set('consuming vapors', 'ROE').
card_original_type('consuming vapors'/'ROE', 'Sorcery').
card_original_text('consuming vapors'/'ROE', 'Target player sacrifices a creature. You gain life equal to that creature\'s toughness.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('consuming vapors', 'ROE').
card_image_name('consuming vapors'/'ROE', 'consuming vapors').
card_uid('consuming vapors'/'ROE', 'ROE:Consuming Vapors:consuming vapors').
card_rarity('consuming vapors'/'ROE', 'Rare').
card_artist('consuming vapors'/'ROE', 'Trevor Claxton').
card_number('consuming vapors'/'ROE', '101').
card_multiverse_id('consuming vapors'/'ROE', '205936').

card_in_set('contaminated ground', 'ROE').
card_original_type('contaminated ground'/'ROE', 'Enchantment — Aura').
card_original_text('contaminated ground'/'ROE', 'Enchant land\nEnchanted land is a Swamp.\nWhenever enchanted land becomes tapped, its controller loses 2 life.').
card_first_print('contaminated ground', 'ROE').
card_image_name('contaminated ground'/'ROE', 'contaminated ground').
card_uid('contaminated ground'/'ROE', 'ROE:Contaminated Ground:contaminated ground').
card_rarity('contaminated ground'/'ROE', 'Common').
card_artist('contaminated ground'/'ROE', 'Rob Alexander').
card_number('contaminated ground'/'ROE', '102').
card_flavor_text('contaminated ground'/'ROE', '\"The Na Plateau is lost. Do not ask what happened, and never return there.\"\n—Nirthu, lone missionary').
card_multiverse_id('contaminated ground'/'ROE', '193608').

card_in_set('coralhelm commander', 'ROE').
card_original_type('coralhelm commander'/'ROE', 'Creature — Merfolk Soldier').
card_original_text('coralhelm commander'/'ROE', 'Level up {1} ({1}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-3\n3/3\nFlying\nLEVEL 4+\n4/4\nFlying\nOther Merfolk creatures you control get +1/+1.').
card_first_print('coralhelm commander', 'ROE').
card_image_name('coralhelm commander'/'ROE', 'coralhelm commander').
card_uid('coralhelm commander'/'ROE', 'ROE:Coralhelm Commander:coralhelm commander').
card_rarity('coralhelm commander'/'ROE', 'Rare').
card_artist('coralhelm commander'/'ROE', 'Jaime Jones').
card_number('coralhelm commander'/'ROE', '57').
card_multiverse_id('coralhelm commander'/'ROE', '193651').

card_in_set('corpsehatch', 'ROE').
card_original_type('corpsehatch'/'ROE', 'Sorcery').
card_original_text('corpsehatch'/'ROE', 'Destroy target nonblack creature. Put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('corpsehatch', 'ROE').
card_image_name('corpsehatch'/'ROE', 'corpsehatch').
card_uid('corpsehatch'/'ROE', 'ROE:Corpsehatch:corpsehatch').
card_rarity('corpsehatch'/'ROE', 'Uncommon').
card_artist('corpsehatch'/'ROE', 'Kekai Kotaki').
card_number('corpsehatch'/'ROE', '103').
card_multiverse_id('corpsehatch'/'ROE', '193625').

card_in_set('crab umbra', 'ROE').
card_original_type('crab umbra'/'ROE', 'Enchantment — Aura').
card_original_text('crab umbra'/'ROE', 'Enchant creature\n{2}{U}: Untap enchanted creature.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('crab umbra', 'ROE').
card_image_name('crab umbra'/'ROE', 'crab umbra').
card_uid('crab umbra'/'ROE', 'ROE:Crab Umbra:crab umbra').
card_rarity('crab umbra'/'ROE', 'Uncommon').
card_artist('crab umbra'/'ROE', 'Christopher Moeller').
card_number('crab umbra'/'ROE', '58').
card_multiverse_id('crab umbra'/'ROE', '193500').

card_in_set('curse of wizardry', 'ROE').
card_original_type('curse of wizardry'/'ROE', 'Enchantment').
card_original_text('curse of wizardry'/'ROE', 'As Curse of Wizardry enters the battlefield, choose a color.\nWhenever a player casts a spell of the chosen color, that player loses 1 life.').
card_image_name('curse of wizardry'/'ROE', 'curse of wizardry').
card_uid('curse of wizardry'/'ROE', 'ROE:Curse of Wizardry:curse of wizardry').
card_rarity('curse of wizardry'/'ROE', 'Uncommon').
card_artist('curse of wizardry'/'ROE', 'Kekai Kotaki').
card_number('curse of wizardry'/'ROE', '104').
card_flavor_text('curse of wizardry'/'ROE', '\"We must all push through the pain to heal our world.\"\n—Ayli, Kamsa cleric').
card_multiverse_id('curse of wizardry'/'ROE', '193447').

card_in_set('daggerback basilisk', 'ROE').
card_original_type('daggerback basilisk'/'ROE', 'Creature — Basilisk').
card_original_text('daggerback basilisk'/'ROE', 'Deathtouch').
card_first_print('daggerback basilisk', 'ROE').
card_image_name('daggerback basilisk'/'ROE', 'daggerback basilisk').
card_uid('daggerback basilisk'/'ROE', 'ROE:Daggerback Basilisk:daggerback basilisk').
card_rarity('daggerback basilisk'/'ROE', 'Common').
card_artist('daggerback basilisk'/'ROE', 'Jesper Ejsing').
card_number('daggerback basilisk'/'ROE', '182').
card_flavor_text('daggerback basilisk'/'ROE', '\"Petrifying gaze, deadly fangs, knifelike dorsal spines, venomous saliva . . . Am I missing anything? . . . Toxic bones? Seriously?\"\n—Samila, Murasa Expeditionary House').
card_multiverse_id('daggerback basilisk'/'ROE', '194926').

card_in_set('dawnglare invoker', 'ROE').
card_original_type('dawnglare invoker'/'ROE', 'Creature — Kor Wizard').
card_original_text('dawnglare invoker'/'ROE', 'Flying\n{8}: Tap all creatures target player controls.').
card_first_print('dawnglare invoker', 'ROE').
card_image_name('dawnglare invoker'/'ROE', 'dawnglare invoker').
card_uid('dawnglare invoker'/'ROE', 'ROE:Dawnglare Invoker:dawnglare invoker').
card_rarity('dawnglare invoker'/'ROE', 'Common').
card_artist('dawnglare invoker'/'ROE', 'Steve Argyle').
card_number('dawnglare invoker'/'ROE', '16').
card_flavor_text('dawnglare invoker'/'ROE', '\"What we knew as Emeria, Ula, and Cosi were not divine beings at all, but a cruel trick, and a grave error.\"\n—The Invokers\' Tales').
card_multiverse_id('dawnglare invoker'/'ROE', '193558').

card_in_set('death cultist', 'ROE').
card_original_type('death cultist'/'ROE', 'Creature — Human Wizard').
card_original_text('death cultist'/'ROE', 'Sacrifice Death Cultist: Target player loses 1 life and you gain 1 life.').
card_first_print('death cultist', 'ROE').
card_image_name('death cultist'/'ROE', 'death cultist').
card_uid('death cultist'/'ROE', 'ROE:Death Cultist:death cultist').
card_rarity('death cultist'/'ROE', 'Common').
card_artist('death cultist'/'ROE', 'Igor Kieryluk').
card_number('death cultist'/'ROE', '105').
card_flavor_text('death cultist'/'ROE', 'Death inevitably seduces all who study it.').
card_multiverse_id('death cultist'/'ROE', '194948').

card_in_set('deathless angel', 'ROE').
card_original_type('deathless angel'/'ROE', 'Creature — Angel').
card_original_text('deathless angel'/'ROE', 'Flying\n{W}{W}: Target creature is indestructible this turn.').
card_image_name('deathless angel'/'ROE', 'deathless angel').
card_uid('deathless angel'/'ROE', 'ROE:Deathless Angel:deathless angel').
card_rarity('deathless angel'/'ROE', 'Rare').
card_artist('deathless angel'/'ROE', 'Johann Bodin').
card_number('deathless angel'/'ROE', '17').
card_flavor_text('deathless angel'/'ROE', '\"I should have died that day, but I suffered not a scratch. I awoke in a lake of blood, none of it apparently my own.\"\n—The War Diaries').
card_multiverse_id('deathless angel'/'ROE', '193629').

card_in_set('demonic appetite', 'ROE').
card_original_type('demonic appetite'/'ROE', 'Enchantment — Aura').
card_original_text('demonic appetite'/'ROE', 'Enchant creature you control\nEnchanted creature gets +3/+3.\nAt the beginning of your upkeep, sacrifice a creature.').
card_first_print('demonic appetite', 'ROE').
card_image_name('demonic appetite'/'ROE', 'demonic appetite').
card_uid('demonic appetite'/'ROE', 'ROE:Demonic Appetite:demonic appetite').
card_rarity('demonic appetite'/'ROE', 'Common').
card_artist('demonic appetite'/'ROE', 'Igor Kieryluk').
card_number('demonic appetite'/'ROE', '106').
card_flavor_text('demonic appetite'/'ROE', '\"Morality is just shorthand for the constraints of being powerless.\"\n—Ob Nixilis').
card_multiverse_id('demonic appetite'/'ROE', '193444').

card_in_set('demystify', 'ROE').
card_original_type('demystify'/'ROE', 'Instant').
card_original_text('demystify'/'ROE', 'Destroy target enchantment.').
card_image_name('demystify'/'ROE', 'demystify').
card_uid('demystify'/'ROE', 'ROE:Demystify:demystify').
card_rarity('demystify'/'ROE', 'Common').
card_artist('demystify'/'ROE', 'Véronique Meignaud').
card_number('demystify'/'ROE', '18').
card_flavor_text('demystify'/'ROE', 'The Singing City was silent, the pathway stones were still, and the winds of the Howling Spire ceased.').
card_multiverse_id('demystify'/'ROE', '198313').

card_in_set('deprive', 'ROE').
card_original_type('deprive'/'ROE', 'Instant').
card_original_text('deprive'/'ROE', 'As an additional cost to cast Deprive, return a land you control to its owner\'s hand.\nCounter target spell.').
card_first_print('deprive', 'ROE').
card_image_name('deprive'/'ROE', 'deprive').
card_uid('deprive'/'ROE', 'ROE:Deprive:deprive').
card_rarity('deprive'/'ROE', 'Common').
card_artist('deprive'/'ROE', 'Izzy').
card_number('deprive'/'ROE', '59').
card_flavor_text('deprive'/'ROE', '\"That would have brought shame to you as a mage. Tell you what—I\'ll keep your secret.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('deprive'/'ROE', '193519').

card_in_set('devastating summons', 'ROE').
card_original_type('devastating summons'/'ROE', 'Sorcery').
card_original_text('devastating summons'/'ROE', 'As an additional cost to cast Devastating Summons, sacrifice X lands.\nPut two X/X red Elemental creature tokens onto the battlefield.').
card_first_print('devastating summons', 'ROE').
card_image_name('devastating summons'/'ROE', 'devastating summons').
card_uid('devastating summons'/'ROE', 'ROE:Devastating Summons:devastating summons').
card_rarity('devastating summons'/'ROE', 'Rare').
card_artist('devastating summons'/'ROE', 'Jung Park').
card_number('devastating summons'/'ROE', '140').
card_flavor_text('devastating summons'/'ROE', 'Things born in magma are always angry.').
card_multiverse_id('devastating summons'/'ROE', '194927').

card_in_set('disaster radius', 'ROE').
card_original_type('disaster radius'/'ROE', 'Sorcery').
card_original_text('disaster radius'/'ROE', 'As an additional cost to cast Disaster Radius, reveal a creature card from your hand.\nDisaster Radius deals X damage to each creature your opponents control, where X is the revealed card\'s converted mana cost.').
card_first_print('disaster radius', 'ROE').
card_image_name('disaster radius'/'ROE', 'disaster radius').
card_uid('disaster radius'/'ROE', 'ROE:Disaster Radius:disaster radius').
card_rarity('disaster radius'/'ROE', 'Rare').
card_artist('disaster radius'/'ROE', 'James Paick').
card_number('disaster radius'/'ROE', '141').
card_multiverse_id('disaster radius'/'ROE', '193579').

card_in_set('distortion strike', 'ROE').
card_original_type('distortion strike'/'ROE', 'Sorcery').
card_original_text('distortion strike'/'ROE', 'Target creature gets +1/+0 until end of turn and is unblockable this turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('distortion strike', 'ROE').
card_image_name('distortion strike'/'ROE', 'distortion strike').
card_uid('distortion strike'/'ROE', 'ROE:Distortion Strike:distortion strike').
card_rarity('distortion strike'/'ROE', 'Common').
card_artist('distortion strike'/'ROE', 'Goran Josic').
card_number('distortion strike'/'ROE', '60').
card_multiverse_id('distortion strike'/'ROE', '198168').

card_in_set('domestication', 'ROE').
card_original_type('domestication'/'ROE', 'Enchantment — Aura').
card_original_text('domestication'/'ROE', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of your end step, if enchanted creature\'s power is 4 or greater, sacrifice Domestication.').
card_first_print('domestication', 'ROE').
card_image_name('domestication'/'ROE', 'domestication').
card_uid('domestication'/'ROE', 'ROE:Domestication:domestication').
card_rarity('domestication'/'ROE', 'Uncommon').
card_artist('domestication'/'ROE', 'Jesper Ejsing').
card_number('domestication'/'ROE', '61').
card_multiverse_id('domestication'/'ROE', '193600').

card_in_set('dormant gomazoa', 'ROE').
card_original_type('dormant gomazoa'/'ROE', 'Creature — Jellyfish').
card_original_text('dormant gomazoa'/'ROE', 'Flying\nDormant Gomazoa enters the battlefield tapped.\nDormant Gomazoa doesn\'t untap during your untap step.\nWhenever you become the target of a spell, you may untap Dormant Gomazoa.').
card_first_print('dormant gomazoa', 'ROE').
card_image_name('dormant gomazoa'/'ROE', 'dormant gomazoa').
card_uid('dormant gomazoa'/'ROE', 'ROE:Dormant Gomazoa:dormant gomazoa').
card_rarity('dormant gomazoa'/'ROE', 'Rare').
card_artist('dormant gomazoa'/'ROE', 'Chris Rahn').
card_number('dormant gomazoa'/'ROE', '62').
card_multiverse_id('dormant gomazoa'/'ROE', '193584').

card_in_set('drake umbra', 'ROE').
card_original_type('drake umbra'/'ROE', 'Enchantment — Aura').
card_original_text('drake umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +3/+3 and has flying.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('drake umbra', 'ROE').
card_image_name('drake umbra'/'ROE', 'drake umbra').
card_uid('drake umbra'/'ROE', 'ROE:Drake Umbra:drake umbra').
card_rarity('drake umbra'/'ROE', 'Uncommon').
card_artist('drake umbra'/'ROE', 'Howard Lyon').
card_number('drake umbra'/'ROE', '63').
card_multiverse_id('drake umbra'/'ROE', '193635').

card_in_set('drana, kalastria bloodchief', 'ROE').
card_original_type('drana, kalastria bloodchief'/'ROE', 'Legendary Creature — Vampire Shaman').
card_original_text('drana, kalastria bloodchief'/'ROE', 'Flying\n{X}{B}{B}: Target creature gets -0/-X until end of turn and Drana, Kalastria Bloodchief gets +X/+0 until end of turn.').
card_first_print('drana, kalastria bloodchief', 'ROE').
card_image_name('drana, kalastria bloodchief'/'ROE', 'drana, kalastria bloodchief').
card_uid('drana, kalastria bloodchief'/'ROE', 'ROE:Drana, Kalastria Bloodchief:drana, kalastria bloodchief').
card_rarity('drana, kalastria bloodchief'/'ROE', 'Rare').
card_artist('drana, kalastria bloodchief'/'ROE', 'Mike Bierek').
card_number('drana, kalastria bloodchief'/'ROE', '107').
card_flavor_text('drana, kalastria bloodchief'/'ROE', '\"If our former masters would have us kneel again, they shall feel our defiance slashed across their membranes.\"').
card_multiverse_id('drana, kalastria bloodchief'/'ROE', '193593').

card_in_set('dread drone', 'ROE').
card_original_type('dread drone'/'ROE', 'Creature — Eldrazi Drone').
card_original_text('dread drone'/'ROE', 'When Dread Drone enters the battlefield, put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('dread drone', 'ROE').
card_image_name('dread drone'/'ROE', 'dread drone').
card_uid('dread drone'/'ROE', 'ROE:Dread Drone:dread drone').
card_rarity('dread drone'/'ROE', 'Common').
card_artist('dread drone'/'ROE', 'Raymond Swanland').
card_number('dread drone'/'ROE', '108').
card_flavor_text('dread drone'/'ROE', 'Each Eldrazi ancient spawned a lineage of horrors in unfathomable shapes.').
card_multiverse_id('dread drone'/'ROE', '193542').

card_in_set('dreamstone hedron', 'ROE').
card_original_type('dreamstone hedron'/'ROE', 'Artifact').
card_original_text('dreamstone hedron'/'ROE', '{T}: Add {3} to your mana pool.\n{3}, {T}, Sacrifice Dreamstone Hedron: Draw three cards.').
card_first_print('dreamstone hedron', 'ROE').
card_image_name('dreamstone hedron'/'ROE', 'dreamstone hedron').
card_uid('dreamstone hedron'/'ROE', 'ROE:Dreamstone Hedron:dreamstone hedron').
card_rarity('dreamstone hedron'/'ROE', 'Uncommon').
card_artist('dreamstone hedron'/'ROE', 'Eric Deschamps').
card_number('dreamstone hedron'/'ROE', '216').
card_flavor_text('dreamstone hedron'/'ROE', 'Only the Eldrazi mind thinks in the warped paths required to open the hedrons and tap the power within.').
card_multiverse_id('dreamstone hedron'/'ROE', '193501').

card_in_set('echo mage', 'ROE').
card_original_type('echo mage'/'ROE', 'Creature — Human Wizard').
card_original_text('echo mage'/'ROE', 'Level up {1}{U} ({1}{U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-3\n2/4\n{U}{U}, {T}: Copy target instant or sorcery spell. You may choose new targets for the copy.\nLEVEL 4+\n2/5\n{U}{U}, {T}: Copy target instant or sorcery spell twice. You may choose new targets for the copies.').
card_first_print('echo mage', 'ROE').
card_image_name('echo mage'/'ROE', 'echo mage').
card_uid('echo mage'/'ROE', 'ROE:Echo Mage:echo mage').
card_rarity('echo mage'/'ROE', 'Rare').
card_artist('echo mage'/'ROE', 'Matt Stewart').
card_number('echo mage'/'ROE', '64').
card_multiverse_id('echo mage'/'ROE', '198163').

card_in_set('eel umbra', 'ROE').
card_original_type('eel umbra'/'ROE', 'Enchantment — Aura').
card_original_text('eel umbra'/'ROE', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +1/+1.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('eel umbra', 'ROE').
card_image_name('eel umbra'/'ROE', 'eel umbra').
card_uid('eel umbra'/'ROE', 'ROE:Eel Umbra:eel umbra').
card_rarity('eel umbra'/'ROE', 'Common').
card_artist('eel umbra'/'ROE', 'Howard Lyon').
card_number('eel umbra'/'ROE', '65').
card_multiverse_id('eel umbra'/'ROE', '193495').

card_in_set('eland umbra', 'ROE').
card_original_type('eland umbra'/'ROE', 'Enchantment — Aura').
card_original_text('eland umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +0/+4.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('eland umbra', 'ROE').
card_image_name('eland umbra'/'ROE', 'eland umbra').
card_uid('eland umbra'/'ROE', 'ROE:Eland Umbra:eland umbra').
card_rarity('eland umbra'/'ROE', 'Common').
card_artist('eland umbra'/'ROE', 'Howard Lyon').
card_number('eland umbra'/'ROE', '19').
card_multiverse_id('eland umbra'/'ROE', '194941').

card_in_set('eldrazi conscription', 'ROE').
card_original_type('eldrazi conscription'/'ROE', 'Tribal Enchantment — Eldrazi Aura').
card_original_text('eldrazi conscription'/'ROE', 'Enchant creature\nEnchanted creature gets +10/+10 and has trample and annihilator 2. (Whenever it attacks, defending player sacrifices two permanents.)').
card_first_print('eldrazi conscription', 'ROE').
card_image_name('eldrazi conscription'/'ROE', 'eldrazi conscription').
card_uid('eldrazi conscription'/'ROE', 'ROE:Eldrazi Conscription:eldrazi conscription').
card_rarity('eldrazi conscription'/'ROE', 'Rare').
card_artist('eldrazi conscription'/'ROE', 'Jaime Jones').
card_number('eldrazi conscription'/'ROE', '3').
card_flavor_text('eldrazi conscription'/'ROE', 'The barest taste of Eldrazi power shatters both realms and identities.').
card_multiverse_id('eldrazi conscription'/'ROE', '193492').

card_in_set('eldrazi temple', 'ROE').
card_original_type('eldrazi temple'/'ROE', 'Land').
card_original_text('eldrazi temple'/'ROE', '{T}: Add {1} to your mana pool.\n{T}: Add {2} to your mana pool. Spend this mana only to cast colorless Eldrazi spells or activate abilities of colorless Eldrazi.').
card_first_print('eldrazi temple', 'ROE').
card_image_name('eldrazi temple'/'ROE', 'eldrazi temple').
card_uid('eldrazi temple'/'ROE', 'ROE:Eldrazi Temple:eldrazi temple').
card_rarity('eldrazi temple'/'ROE', 'Rare').
card_artist('eldrazi temple'/'ROE', 'James Paick').
card_number('eldrazi temple'/'ROE', '227').
card_flavor_text('eldrazi temple'/'ROE', 'Each temple is a door to a horrible future.').
card_multiverse_id('eldrazi temple'/'ROE', '198312').

card_in_set('emerge unscathed', 'ROE').
card_original_type('emerge unscathed'/'ROE', 'Instant').
card_original_text('emerge unscathed'/'ROE', 'Target creature you control gains protection from the color of your choice until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('emerge unscathed', 'ROE').
card_image_name('emerge unscathed'/'ROE', 'emerge unscathed').
card_uid('emerge unscathed'/'ROE', 'ROE:Emerge Unscathed:emerge unscathed').
card_rarity('emerge unscathed'/'ROE', 'Uncommon').
card_artist('emerge unscathed'/'ROE', 'Steve Argyle').
card_number('emerge unscathed'/'ROE', '20').
card_multiverse_id('emerge unscathed'/'ROE', '194949').

card_in_set('emrakul\'s hatcher', 'ROE').
card_original_type('emrakul\'s hatcher'/'ROE', 'Creature — Eldrazi Drone').
card_original_text('emrakul\'s hatcher'/'ROE', 'When Emrakul\'s Hatcher enters the battlefield, put three 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('emrakul\'s hatcher', 'ROE').
card_image_name('emrakul\'s hatcher'/'ROE', 'emrakul\'s hatcher').
card_uid('emrakul\'s hatcher'/'ROE', 'ROE:Emrakul\'s Hatcher:emrakul\'s hatcher').
card_rarity('emrakul\'s hatcher'/'ROE', 'Common').
card_artist('emrakul\'s hatcher'/'ROE', 'Jaime Jones').
card_number('emrakul\'s hatcher'/'ROE', '142').
card_flavor_text('emrakul\'s hatcher'/'ROE', 'Wordlessly it leads its abhorrent brood.').
card_multiverse_id('emrakul\'s hatcher'/'ROE', '193485').

card_in_set('emrakul, the aeons torn', 'ROE').
card_original_type('emrakul, the aeons torn'/'ROE', 'Legendary Creature — Eldrazi').
card_original_text('emrakul, the aeons torn'/'ROE', 'Emrakul, the Aeons Torn can\'t be countered.\nWhen you cast Emrakul, take an extra turn after this one.\nFlying, protection from colored spells, annihilator 6\nWhen Emrakul is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_image_name('emrakul, the aeons torn'/'ROE', 'emrakul, the aeons torn').
card_uid('emrakul, the aeons torn'/'ROE', 'ROE:Emrakul, the Aeons Torn:emrakul, the aeons torn').
card_rarity('emrakul, the aeons torn'/'ROE', 'Mythic Rare').
card_artist('emrakul, the aeons torn'/'ROE', 'Mark Tedin').
card_number('emrakul, the aeons torn'/'ROE', '4').
card_multiverse_id('emrakul, the aeons torn'/'ROE', '193452').

card_in_set('enatu golem', 'ROE').
card_original_type('enatu golem'/'ROE', 'Artifact Creature — Golem').
card_original_text('enatu golem'/'ROE', 'When Enatu Golem is put into a graveyard from the battlefield, you gain 4 life.').
card_first_print('enatu golem', 'ROE').
card_image_name('enatu golem'/'ROE', 'enatu golem').
card_uid('enatu golem'/'ROE', 'ROE:Enatu Golem:enatu golem').
card_rarity('enatu golem'/'ROE', 'Uncommon').
card_artist('enatu golem'/'ROE', 'Daniel Ljunggren').
card_number('enatu golem'/'ROE', '217').
card_flavor_text('enatu golem'/'ROE', 'Golems conjured from the debris of Enatu Temple provided a sturdy but expendable first line of defense against the Eldrazi.').
card_multiverse_id('enatu golem'/'ROE', '193626').

card_in_set('enclave cryptologist', 'ROE').
card_original_type('enclave cryptologist'/'ROE', 'Creature — Merfolk Wizard').
card_original_text('enclave cryptologist'/'ROE', 'Level up {1}{U} ({1}{U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n0/1\n{T}: Draw a card, then discard a card.\nLEVEL 3+\n0/1\n{T}: Draw a card.').
card_first_print('enclave cryptologist', 'ROE').
card_image_name('enclave cryptologist'/'ROE', 'enclave cryptologist').
card_uid('enclave cryptologist'/'ROE', 'ROE:Enclave Cryptologist:enclave cryptologist').
card_rarity('enclave cryptologist'/'ROE', 'Uncommon').
card_artist('enclave cryptologist'/'ROE', 'Igor Kieryluk').
card_number('enclave cryptologist'/'ROE', '66').
card_multiverse_id('enclave cryptologist'/'ROE', '194903').

card_in_set('escaped null', 'ROE').
card_original_type('escaped null'/'ROE', 'Creature — Zombie Berserker').
card_original_text('escaped null'/'ROE', 'Lifelink\nWhenever Escaped Null blocks or becomes blocked, it gets +5/+0 until end of turn.').
card_first_print('escaped null', 'ROE').
card_image_name('escaped null'/'ROE', 'escaped null').
card_uid('escaped null'/'ROE', 'ROE:Escaped Null:escaped null').
card_rarity('escaped null'/'ROE', 'Uncommon').
card_artist('escaped null'/'ROE', 'Ryan Pancoast').
card_number('escaped null'/'ROE', '109').
card_flavor_text('escaped null'/'ROE', 'Left masterless by Kozilek, it wandered the wilds, raving in its ceaseless hunger.').
card_multiverse_id('escaped null'/'ROE', '193643').

card_in_set('essence feed', 'ROE').
card_original_type('essence feed'/'ROE', 'Sorcery').
card_original_text('essence feed'/'ROE', 'Target player loses 3 life. You gain 3 life and put three 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('essence feed', 'ROE').
card_image_name('essence feed'/'ROE', 'essence feed').
card_uid('essence feed'/'ROE', 'ROE:Essence Feed:essence feed').
card_rarity('essence feed'/'ROE', 'Common').
card_artist('essence feed'/'ROE', 'Daarken').
card_number('essence feed'/'ROE', '110').
card_flavor_text('essence feed'/'ROE', 'Eldrazi grow wherever something else dies.').
card_multiverse_id('essence feed'/'ROE', '194939').

card_in_set('evolving wilds', 'ROE').
card_original_type('evolving wilds'/'ROE', 'Land').
card_original_text('evolving wilds'/'ROE', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'ROE', 'evolving wilds').
card_uid('evolving wilds'/'ROE', 'ROE:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'ROE', 'Common').
card_artist('evolving wilds'/'ROE', 'Steven Belledin').
card_number('evolving wilds'/'ROE', '228').
card_flavor_text('evolving wilds'/'ROE', 'Every world is an organism, able to grow new lands. Some just do it faster than others.').
card_multiverse_id('evolving wilds'/'ROE', '193483').

card_in_set('explosive revelation', 'ROE').
card_original_type('explosive revelation'/'ROE', 'Sorcery').
card_original_text('explosive revelation'/'ROE', 'Choose target creature or player. Reveal cards from the top of your library until you reveal a nonland card. Explosive Revelation deals damage equal to that card\'s converted mana cost to that creature or player. Put the nonland card into your hand and the rest on the bottom of your library in any order.').
card_first_print('explosive revelation', 'ROE').
card_image_name('explosive revelation'/'ROE', 'explosive revelation').
card_uid('explosive revelation'/'ROE', 'ROE:Explosive Revelation:explosive revelation').
card_rarity('explosive revelation'/'ROE', 'Uncommon').
card_artist('explosive revelation'/'ROE', 'Justin Sweet').
card_number('explosive revelation'/'ROE', '143').
card_multiverse_id('explosive revelation'/'ROE', '193479').

card_in_set('fissure vent', 'ROE').
card_original_type('fissure vent'/'ROE', 'Sorcery').
card_original_text('fissure vent'/'ROE', 'Choose one or both — Destroy target artifact; and/or destroy target nonbasic land.').
card_first_print('fissure vent', 'ROE').
card_image_name('fissure vent'/'ROE', 'fissure vent').
card_uid('fissure vent'/'ROE', 'ROE:Fissure Vent:fissure vent').
card_rarity('fissure vent'/'ROE', 'Common').
card_artist('fissure vent'/'ROE', 'Philip Straub').
card_number('fissure vent'/'ROE', '144').
card_flavor_text('fissure vent'/'ROE', '\"Something down there has an appetite for what we\'re standing on. Let\'s hope it doesn\'t want seconds.\"\n—Samila, Murasa Expeditionary House').
card_multiverse_id('fissure vent'/'ROE', '198170').

card_in_set('flame slash', 'ROE').
card_original_type('flame slash'/'ROE', 'Sorcery').
card_original_text('flame slash'/'ROE', 'Flame Slash deals 4 damage to target creature.').
card_first_print('flame slash', 'ROE').
card_image_name('flame slash'/'ROE', 'flame slash').
card_uid('flame slash'/'ROE', 'ROE:Flame Slash:flame slash').
card_rarity('flame slash'/'ROE', 'Common').
card_artist('flame slash'/'ROE', 'Raymond Swanland').
card_number('flame slash'/'ROE', '145').
card_flavor_text('flame slash'/'ROE', 'After millennia asleep, the Eldrazi had forgotten about Zendikar\'s fiery temper and dislike of strangers.').
card_multiverse_id('flame slash'/'ROE', '193648').

card_in_set('fleeting distraction', 'ROE').
card_original_type('fleeting distraction'/'ROE', 'Instant').
card_original_text('fleeting distraction'/'ROE', 'Target creature gets -1/-0 until end of turn.\nDraw a card.').
card_first_print('fleeting distraction', 'ROE').
card_image_name('fleeting distraction'/'ROE', 'fleeting distraction').
card_uid('fleeting distraction'/'ROE', 'ROE:Fleeting Distraction:fleeting distraction').
card_rarity('fleeting distraction'/'ROE', 'Common').
card_artist('fleeting distraction'/'ROE', 'Kieran Yanner').
card_number('fleeting distraction'/'ROE', '67').
card_flavor_text('fleeting distraction'/'ROE', '\"To distract an archmage would take incredible power. But for a simpleton, something sparkly will do.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('fleeting distraction'/'ROE', '198311').

card_in_set('forest', 'ROE').
card_original_type('forest'/'ROE', 'Basic Land — Forest').
card_original_text('forest'/'ROE', 'G').
card_image_name('forest'/'ROE', 'forest1').
card_uid('forest'/'ROE', 'ROE:Forest:forest1').
card_rarity('forest'/'ROE', 'Basic Land').
card_artist('forest'/'ROE', 'Jung Park').
card_number('forest'/'ROE', '245').
card_multiverse_id('forest'/'ROE', '205935').

card_in_set('forest', 'ROE').
card_original_type('forest'/'ROE', 'Basic Land — Forest').
card_original_text('forest'/'ROE', 'G').
card_image_name('forest'/'ROE', 'forest2').
card_uid('forest'/'ROE', 'ROE:Forest:forest2').
card_rarity('forest'/'ROE', 'Basic Land').
card_artist('forest'/'ROE', 'Jung Park').
card_number('forest'/'ROE', '246').
card_multiverse_id('forest'/'ROE', '205933').

card_in_set('forest', 'ROE').
card_original_type('forest'/'ROE', 'Basic Land — Forest').
card_original_text('forest'/'ROE', 'G').
card_image_name('forest'/'ROE', 'forest3').
card_uid('forest'/'ROE', 'ROE:Forest:forest3').
card_rarity('forest'/'ROE', 'Basic Land').
card_artist('forest'/'ROE', 'Jung Park').
card_number('forest'/'ROE', '247').
card_multiverse_id('forest'/'ROE', '205927').

card_in_set('forest', 'ROE').
card_original_type('forest'/'ROE', 'Basic Land — Forest').
card_original_text('forest'/'ROE', 'G').
card_image_name('forest'/'ROE', 'forest4').
card_uid('forest'/'ROE', 'ROE:Forest:forest4').
card_rarity('forest'/'ROE', 'Basic Land').
card_artist('forest'/'ROE', 'Jung Park').
card_number('forest'/'ROE', '248').
card_multiverse_id('forest'/'ROE', '205949').

card_in_set('forked bolt', 'ROE').
card_original_type('forked bolt'/'ROE', 'Sorcery').
card_original_text('forked bolt'/'ROE', 'Forked Bolt deals 2 damage divided as you choose among one or two target creatures and/or players.').
card_first_print('forked bolt', 'ROE').
card_image_name('forked bolt'/'ROE', 'forked bolt').
card_uid('forked bolt'/'ROE', 'ROE:Forked Bolt:forked bolt').
card_rarity('forked bolt'/'ROE', 'Uncommon').
card_artist('forked bolt'/'ROE', 'Tomasz Jedruszek').
card_number('forked bolt'/'ROE', '146').
card_flavor_text('forked bolt'/'ROE', '\"Play no favorites. Everybody dies.\"\n—Sparkmage saying').
card_multiverse_id('forked bolt'/'ROE', '193512').

card_in_set('frostwind invoker', 'ROE').
card_original_type('frostwind invoker'/'ROE', 'Creature — Merfolk Wizard').
card_original_text('frostwind invoker'/'ROE', 'Flying\n{8}: Creatures you control gain flying until end of turn.').
card_first_print('frostwind invoker', 'ROE').
card_image_name('frostwind invoker'/'ROE', 'frostwind invoker').
card_uid('frostwind invoker'/'ROE', 'ROE:Frostwind Invoker:frostwind invoker').
card_rarity('frostwind invoker'/'ROE', 'Common').
card_artist('frostwind invoker'/'ROE', 'Svetlin Velinov').
card_number('frostwind invoker'/'ROE', '68').
card_flavor_text('frostwind invoker'/'ROE', '\"We thought Zendikar\'s rage was kindled by its explorers and plunderers. But the world had sensed the stirrings of the Eldrazi.\"\n—The Invokers\' Tales').
card_multiverse_id('frostwind invoker'/'ROE', '193476').

card_in_set('gelatinous genesis', 'ROE').
card_original_type('gelatinous genesis'/'ROE', 'Sorcery').
card_original_text('gelatinous genesis'/'ROE', 'Put X X/X green Ooze creature tokens onto the battlefield.').
card_first_print('gelatinous genesis', 'ROE').
card_image_name('gelatinous genesis'/'ROE', 'gelatinous genesis').
card_uid('gelatinous genesis'/'ROE', 'ROE:Gelatinous Genesis:gelatinous genesis').
card_rarity('gelatinous genesis'/'ROE', 'Rare').
card_artist('gelatinous genesis'/'ROE', 'Daniel Ljunggren').
card_number('gelatinous genesis'/'ROE', '183').
card_flavor_text('gelatinous genesis'/'ROE', 'Even as the world was left cracked and gory by the marauding Eldrazi, new life began to drip from its wounds.').
card_multiverse_id('gelatinous genesis'/'ROE', '193503').

card_in_set('gideon jura', 'ROE').
card_original_type('gideon jura'/'ROE', 'Planeswalker — Gideon').
card_original_text('gideon jura'/'ROE', '+2: During target opponent\'s next turn, creatures that player controls attack Gideon Jura if able.\n-2: Destroy target tapped creature.\n0: Until end of turn, Gideon Jura becomes a 6/6 Human Soldier creature that\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.').
card_first_print('gideon jura', 'ROE').
card_image_name('gideon jura'/'ROE', 'gideon jura').
card_uid('gideon jura'/'ROE', 'ROE:Gideon Jura:gideon jura').
card_rarity('gideon jura'/'ROE', 'Mythic Rare').
card_artist('gideon jura'/'ROE', 'Aleksi Briclot').
card_number('gideon jura'/'ROE', '21').
card_multiverse_id('gideon jura'/'ROE', '192218').

card_in_set('gigantomancer', 'ROE').
card_original_type('gigantomancer'/'ROE', 'Creature — Human Shaman').
card_original_text('gigantomancer'/'ROE', '{1}: Target creature you control becomes 7/7 until end of turn.').
card_first_print('gigantomancer', 'ROE').
card_image_name('gigantomancer'/'ROE', 'gigantomancer').
card_uid('gigantomancer'/'ROE', 'ROE:Gigantomancer:gigantomancer').
card_rarity('gigantomancer'/'ROE', 'Rare').
card_artist('gigantomancer'/'ROE', 'Chippy').
card_number('gigantomancer'/'ROE', '184').
card_flavor_text('gigantomancer'/'ROE', '\"All life is driven by an indomitable will to survive. I simply turn that into something more . . . tangible.\"').
card_multiverse_id('gigantomancer'/'ROE', '198172').

card_in_set('gloomhunter', 'ROE').
card_original_type('gloomhunter'/'ROE', 'Creature — Bat').
card_original_text('gloomhunter'/'ROE', 'Flying').
card_first_print('gloomhunter', 'ROE').
card_image_name('gloomhunter'/'ROE', 'gloomhunter').
card_uid('gloomhunter'/'ROE', 'ROE:Gloomhunter:gloomhunter').
card_rarity('gloomhunter'/'ROE', 'Common').
card_artist('gloomhunter'/'ROE', 'Lars Grant-West').
card_number('gloomhunter'/'ROE', '111').
card_flavor_text('gloomhunter'/'ROE', 'Scavengers both mundane and magical follow in its wake, feeding on the scraps of flesh and spirit it leaves behind.').
card_multiverse_id('gloomhunter'/'ROE', '193513').

card_in_set('glory seeker', 'ROE').
card_original_type('glory seeker'/'ROE', 'Creature — Human Soldier').
card_original_text('glory seeker'/'ROE', '').
card_image_name('glory seeker'/'ROE', 'glory seeker').
card_uid('glory seeker'/'ROE', 'ROE:Glory Seeker:glory seeker').
card_rarity('glory seeker'/'ROE', 'Common').
card_artist('glory seeker'/'ROE', 'Matt Cavotta').
card_number('glory seeker'/'ROE', '22').
card_flavor_text('glory seeker'/'ROE', '\"There\'s no contract to sign, no oath to swear. The enlistment procedure is to unsheathe your sword and point it at the enemy.\"').
card_multiverse_id('glory seeker'/'ROE', '193557').

card_in_set('goblin arsonist', 'ROE').
card_original_type('goblin arsonist'/'ROE', 'Creature — Goblin Shaman').
card_original_text('goblin arsonist'/'ROE', 'When Goblin Arsonist is put into a graveyard from the battlefield, you may have it deal 1 damage to target creature or player.').
card_first_print('goblin arsonist', 'ROE').
card_image_name('goblin arsonist'/'ROE', 'goblin arsonist').
card_uid('goblin arsonist'/'ROE', 'ROE:Goblin Arsonist:goblin arsonist').
card_rarity('goblin arsonist'/'ROE', 'Common').
card_artist('goblin arsonist'/'ROE', 'Wayne Reynolds').
card_number('goblin arsonist'/'ROE', '147').
card_flavor_text('goblin arsonist'/'ROE', 'With great power comes great risk of getting yourself killed.').
card_multiverse_id('goblin arsonist'/'ROE', '194902').

card_in_set('goblin tunneler', 'ROE').
card_original_type('goblin tunneler'/'ROE', 'Creature — Goblin Rogue').
card_original_text('goblin tunneler'/'ROE', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_first_print('goblin tunneler', 'ROE').
card_image_name('goblin tunneler'/'ROE', 'goblin tunneler').
card_uid('goblin tunneler'/'ROE', 'ROE:Goblin Tunneler:goblin tunneler').
card_rarity('goblin tunneler'/'ROE', 'Common').
card_artist('goblin tunneler'/'ROE', 'Jesper Ejsing').
card_number('goblin tunneler'/'ROE', '148').
card_flavor_text('goblin tunneler'/'ROE', '\"You never know what\'s going to be in goblin tunnels. Even in the best case, there will still be goblins.\"\n—Sachir, Akoum Expeditionary House').
card_multiverse_id('goblin tunneler'/'ROE', '193432').

card_in_set('gravitational shift', 'ROE').
card_original_type('gravitational shift'/'ROE', 'Enchantment').
card_original_text('gravitational shift'/'ROE', 'Creatures with flying get +2/+0.\nCreatures without flying get -2/-0.').
card_first_print('gravitational shift', 'ROE').
card_image_name('gravitational shift'/'ROE', 'gravitational shift').
card_uid('gravitational shift'/'ROE', 'ROE:Gravitational Shift:gravitational shift').
card_rarity('gravitational shift'/'ROE', 'Rare').
card_artist('gravitational shift'/'ROE', 'Svetlin Velinov').
card_number('gravitational shift'/'ROE', '69').
card_flavor_text('gravitational shift'/'ROE', 'As they awakened, the Eldrazi reasserted their mastery over all of Zendikar\'s natural forces.').
card_multiverse_id('gravitational shift'/'ROE', '193458').

card_in_set('gravity well', 'ROE').
card_original_type('gravity well'/'ROE', 'Enchantment').
card_original_text('gravity well'/'ROE', 'Whenever a creature with flying attacks, it loses flying until end of turn.').
card_first_print('gravity well', 'ROE').
card_image_name('gravity well'/'ROE', 'gravity well').
card_uid('gravity well'/'ROE', 'ROE:Gravity Well:gravity well').
card_rarity('gravity well'/'ROE', 'Uncommon').
card_artist('gravity well'/'ROE', 'Rob Alexander').
card_number('gravity well'/'ROE', '185').
card_flavor_text('gravity well'/'ROE', 'Storms spread throughout Oran-Rief, ripping the skies with wind and rain. Airborne creatures sought shelter where they could.').
card_multiverse_id('gravity well'/'ROE', '194919').

card_in_set('grotag siege-runner', 'ROE').
card_original_type('grotag siege-runner'/'ROE', 'Creature — Goblin Rogue').
card_original_text('grotag siege-runner'/'ROE', '{R}, Sacrifice Grotag Siege-Runner: Destroy target creature with defender. Grotag Siege-Runner deals 2 damage to that creature\'s controller.').
card_first_print('grotag siege-runner', 'ROE').
card_image_name('grotag siege-runner'/'ROE', 'grotag siege-runner').
card_uid('grotag siege-runner'/'ROE', 'ROE:Grotag Siege-Runner:grotag siege-runner').
card_rarity('grotag siege-runner'/'ROE', 'Common').
card_artist('grotag siege-runner'/'ROE', 'Zoltan Boros & Gabor Szikszai').
card_number('grotag siege-runner'/'ROE', '149').
card_flavor_text('grotag siege-runner'/'ROE', '\"I don\'t think he\'s bringing in the mail . . . .\"\n—Nundari, Sea Gate militia captain').
card_multiverse_id('grotag siege-runner'/'ROE', '193496').

card_in_set('growth spasm', 'ROE').
card_original_type('growth spasm'/'ROE', 'Sorcery').
card_original_text('growth spasm'/'ROE', 'Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library. Put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('growth spasm', 'ROE').
card_image_name('growth spasm'/'ROE', 'growth spasm').
card_uid('growth spasm'/'ROE', 'ROE:Growth Spasm:growth spasm').
card_rarity('growth spasm'/'ROE', 'Common').
card_artist('growth spasm'/'ROE', 'Dan Scott').
card_number('growth spasm'/'ROE', '186').
card_multiverse_id('growth spasm'/'ROE', '193526').

card_in_set('guard duty', 'ROE').
card_original_type('guard duty'/'ROE', 'Enchantment — Aura').
card_original_text('guard duty'/'ROE', 'Enchant creature\nEnchanted creature has defender.').
card_first_print('guard duty', 'ROE').
card_image_name('guard duty'/'ROE', 'guard duty').
card_uid('guard duty'/'ROE', 'ROE:Guard Duty:guard duty').
card_rarity('guard duty'/'ROE', 'Common').
card_artist('guard duty'/'ROE', 'Karl Kopinski').
card_number('guard duty'/'ROE', '23').
card_flavor_text('guard duty'/'ROE', '\"I was told these were standard issue. Do I look standard to you?\"').
card_multiverse_id('guard duty'/'ROE', '193445').

card_in_set('guard gomazoa', 'ROE').
card_original_type('guard gomazoa'/'ROE', 'Creature — Jellyfish').
card_original_text('guard gomazoa'/'ROE', 'Defender, flying\nPrevent all combat damage that would be dealt to Guard Gomazoa.').
card_first_print('guard gomazoa', 'ROE').
card_image_name('guard gomazoa'/'ROE', 'guard gomazoa').
card_uid('guard gomazoa'/'ROE', 'ROE:Guard Gomazoa:guard gomazoa').
card_rarity('guard gomazoa'/'ROE', 'Uncommon').
card_artist('guard gomazoa'/'ROE', 'Rob Alexander').
card_number('guard gomazoa'/'ROE', '70').
card_flavor_text('guard gomazoa'/'ROE', 'It lingers near the outposts of the Makindi Trenches, inadvertently granting another layer of defense.').
card_multiverse_id('guard gomazoa'/'ROE', '193602').

card_in_set('guul draz assassin', 'ROE').
card_original_type('guul draz assassin'/'ROE', 'Creature — Vampire Assassin').
card_original_text('guul draz assassin'/'ROE', 'Level up {1}{B} ({1}{B}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-3\n2/2\n{B}, {T}: Target creature gets -2/-2 until end of turn.\nLEVEL 4+\n4/4\n{B}, {T}: Target creature gets -4/-4 until end of turn.').
card_image_name('guul draz assassin'/'ROE', 'guul draz assassin').
card_uid('guul draz assassin'/'ROE', 'ROE:Guul Draz Assassin:guul draz assassin').
card_rarity('guul draz assassin'/'ROE', 'Rare').
card_artist('guul draz assassin'/'ROE', 'James Ryman').
card_number('guul draz assassin'/'ROE', '112').
card_multiverse_id('guul draz assassin'/'ROE', '193612').

card_in_set('hada spy patrol', 'ROE').
card_original_type('hada spy patrol'/'ROE', 'Creature — Human Rogue').
card_original_text('hada spy patrol'/'ROE', 'Level up {2}{U} ({2}{U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n2/2\nHada Spy Patrol is unblockable.\nLEVEL 3+\n3/3\nShroud\nHada Spy Patrol is unblockable.').
card_first_print('hada spy patrol', 'ROE').
card_image_name('hada spy patrol'/'ROE', 'hada spy patrol').
card_uid('hada spy patrol'/'ROE', 'ROE:Hada Spy Patrol:hada spy patrol').
card_rarity('hada spy patrol'/'ROE', 'Uncommon').
card_artist('hada spy patrol'/'ROE', 'Zoltan Boros & Gabor Szikszai').
card_number('hada spy patrol'/'ROE', '71').
card_multiverse_id('hada spy patrol'/'ROE', '194928').

card_in_set('halimar wavewatch', 'ROE').
card_original_type('halimar wavewatch'/'ROE', 'Creature — Merfolk Soldier').
card_original_text('halimar wavewatch'/'ROE', 'Level up {2} ({2}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n0/6\nLEVEL 5+\n6/6\nIslandwalk').
card_first_print('halimar wavewatch', 'ROE').
card_image_name('halimar wavewatch'/'ROE', 'halimar wavewatch').
card_uid('halimar wavewatch'/'ROE', 'ROE:Halimar Wavewatch:halimar wavewatch').
card_rarity('halimar wavewatch'/'ROE', 'Common').
card_artist('halimar wavewatch'/'ROE', 'Matt Stewart').
card_number('halimar wavewatch'/'ROE', '72').
card_multiverse_id('halimar wavewatch'/'ROE', '194947').

card_in_set('hand of emrakul', 'ROE').
card_original_type('hand of emrakul'/'ROE', 'Creature — Eldrazi').
card_original_text('hand of emrakul'/'ROE', 'You may sacrifice four Eldrazi Spawn rather than pay Hand of Emrakul\'s mana cost.\nAnnihilator 1 (Whenever this creature attacks, defending player sacrifices a permanent.)').
card_first_print('hand of emrakul', 'ROE').
card_image_name('hand of emrakul'/'ROE', 'hand of emrakul').
card_uid('hand of emrakul'/'ROE', 'ROE:Hand of Emrakul:hand of emrakul').
card_rarity('hand of emrakul'/'ROE', 'Common').
card_artist('hand of emrakul'/'ROE', 'Chippy').
card_number('hand of emrakul'/'ROE', '5').
card_flavor_text('hand of emrakul'/'ROE', '\"I believed in a beautiful god. But this is the true face of the divine.\"\n—Ayli, Kamsa cleric').
card_multiverse_id('hand of emrakul'/'ROE', '193616').

card_in_set('harmless assault', 'ROE').
card_original_type('harmless assault'/'ROE', 'Instant').
card_original_text('harmless assault'/'ROE', 'Prevent all combat damage that would be dealt this turn by attacking creatures.').
card_first_print('harmless assault', 'ROE').
card_image_name('harmless assault'/'ROE', 'harmless assault').
card_uid('harmless assault'/'ROE', 'ROE:Harmless Assault:harmless assault').
card_rarity('harmless assault'/'ROE', 'Common').
card_artist('harmless assault'/'ROE', 'Chippy').
card_number('harmless assault'/'ROE', '24').
card_flavor_text('harmless assault'/'ROE', 'After encasing it in a paralyzing beam of light, the angel studied the Eldrazi as a child would study a bug, curiously and without fear.').
card_multiverse_id('harmless assault'/'ROE', '198165').

card_in_set('haze frog', 'ROE').
card_original_type('haze frog'/'ROE', 'Creature — Frog').
card_original_text('haze frog'/'ROE', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Haze Frog enters the battlefield, prevent all combat damage that other creatures would deal this turn.').
card_first_print('haze frog', 'ROE').
card_image_name('haze frog'/'ROE', 'haze frog').
card_uid('haze frog'/'ROE', 'ROE:Haze Frog:haze frog').
card_rarity('haze frog'/'ROE', 'Common').
card_artist('haze frog'/'ROE', 'Jesper Ejsing').
card_number('haze frog'/'ROE', '187').
card_flavor_text('haze frog'/'ROE', 'Its breath leaves its prey too drowsy to struggle.').
card_multiverse_id('haze frog'/'ROE', '193569').

card_in_set('heat ray', 'ROE').
card_original_type('heat ray'/'ROE', 'Instant').
card_original_text('heat ray'/'ROE', 'Heat Ray deals X damage to target creature.').
card_image_name('heat ray'/'ROE', 'heat ray').
card_uid('heat ray'/'ROE', 'ROE:Heat Ray:heat ray').
card_rarity('heat ray'/'ROE', 'Common').
card_artist('heat ray'/'ROE', 'Austin Hsu').
card_number('heat ray'/'ROE', '150').
card_flavor_text('heat ray'/'ROE', '\"There was clearly a scream. I\'m not sure if there was a mouth.\"\n—Sachir, Akoum Expeditionary House').
card_multiverse_id('heat ray'/'ROE', '193649').

card_in_set('hedron matrix', 'ROE').
card_original_type('hedron matrix'/'ROE', 'Artifact — Equipment').
card_original_text('hedron matrix'/'ROE', 'Equipped creature gets +X/+X, where X is its converted mana cost.\nEquip {4}').
card_first_print('hedron matrix', 'ROE').
card_image_name('hedron matrix'/'ROE', 'hedron matrix').
card_uid('hedron matrix'/'ROE', 'ROE:Hedron Matrix:hedron matrix').
card_rarity('hedron matrix'/'ROE', 'Rare').
card_artist('hedron matrix'/'ROE', 'Jaime Jones').
card_number('hedron matrix'/'ROE', '218').
card_flavor_text('hedron matrix'/'ROE', 'The hedrons awoke along with the Eldrazi, forming superstructures that radiated overwhelming magic.').
card_multiverse_id('hedron matrix'/'ROE', '193536').

card_in_set('hedron-field purists', 'ROE').
card_original_type('hedron-field purists'/'ROE', 'Creature — Human Cleric').
card_original_text('hedron-field purists'/'ROE', 'Level up {2}{W} ({2}{W}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n1/4\nIf a source would deal damage to you or a creature you control, prevent 1 of that damage.\nLEVEL 5+\n2/5\nIf a source would deal damage to you or a creature you control, prevent 2 of that damage.').
card_first_print('hedron-field purists', 'ROE').
card_image_name('hedron-field purists'/'ROE', 'hedron-field purists').
card_uid('hedron-field purists'/'ROE', 'ROE:Hedron-Field Purists:hedron-field purists').
card_rarity('hedron-field purists'/'ROE', 'Rare').
card_artist('hedron-field purists'/'ROE', 'Scott Chou').
card_number('hedron-field purists'/'ROE', '25').
card_multiverse_id('hedron-field purists'/'ROE', '193421').

card_in_set('hellcarver demon', 'ROE').
card_original_type('hellcarver demon'/'ROE', 'Creature — Demon').
card_original_text('hellcarver demon'/'ROE', 'Flying\nWhenever Hellcarver Demon deals combat damage to a player, sacrifice all other permanents you control and discard your hand. Exile the top six cards of your library. You may cast any number of nonland cards exiled this way without paying their mana costs.').
card_first_print('hellcarver demon', 'ROE').
card_image_name('hellcarver demon'/'ROE', 'hellcarver demon').
card_uid('hellcarver demon'/'ROE', 'ROE:Hellcarver Demon:hellcarver demon').
card_rarity('hellcarver demon'/'ROE', 'Mythic Rare').
card_artist('hellcarver demon'/'ROE', 'Greg Staples').
card_number('hellcarver demon'/'ROE', '113').
card_multiverse_id('hellcarver demon'/'ROE', '193656').

card_in_set('hellion eruption', 'ROE').
card_original_type('hellion eruption'/'ROE', 'Sorcery').
card_original_text('hellion eruption'/'ROE', 'Sacrifice all creatures you control, then put that many 4/4 red Hellion creature tokens onto the battlefield.').
card_first_print('hellion eruption', 'ROE').
card_image_name('hellion eruption'/'ROE', 'hellion eruption').
card_uid('hellion eruption'/'ROE', 'ROE:Hellion Eruption:hellion eruption').
card_rarity('hellion eruption'/'ROE', 'Rare').
card_artist('hellion eruption'/'ROE', 'Anthony Francisco').
card_number('hellion eruption'/'ROE', '151').
card_flavor_text('hellion eruption'/'ROE', 'Just when you thought you\'d be safe, in the middle of an open field with no Eldrazi around for miles.').
card_multiverse_id('hellion eruption'/'ROE', '198306').

card_in_set('hyena umbra', 'ROE').
card_original_type('hyena umbra'/'ROE', 'Enchantment — Aura').
card_original_text('hyena umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +1/+1 and has first strike.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('hyena umbra', 'ROE').
card_image_name('hyena umbra'/'ROE', 'hyena umbra').
card_uid('hyena umbra'/'ROE', 'ROE:Hyena Umbra:hyena umbra').
card_rarity('hyena umbra'/'ROE', 'Common').
card_artist('hyena umbra'/'ROE', 'Howard Lyon').
card_number('hyena umbra'/'ROE', '26').
card_multiverse_id('hyena umbra'/'ROE', '198294').

card_in_set('ikiral outrider', 'ROE').
card_original_type('ikiral outrider'/'ROE', 'Creature — Human Soldier').
card_original_text('ikiral outrider'/'ROE', 'Level up {4} ({4}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n2/6\nVigilance\nLEVEL 4+\n3/10\nVigilance').
card_first_print('ikiral outrider', 'ROE').
card_image_name('ikiral outrider'/'ROE', 'ikiral outrider').
card_uid('ikiral outrider'/'ROE', 'ROE:Ikiral Outrider:ikiral outrider').
card_rarity('ikiral outrider'/'ROE', 'Common').
card_artist('ikiral outrider'/'ROE', 'Kekai Kotaki').
card_number('ikiral outrider'/'ROE', '27').
card_multiverse_id('ikiral outrider'/'ROE', '198176').

card_in_set('induce despair', 'ROE').
card_original_type('induce despair'/'ROE', 'Instant').
card_original_text('induce despair'/'ROE', 'As an additional cost to cast Induce Despair, reveal a creature card from your hand.\nTarget creature gets -X/-X until end of turn, where X is the revealed card\'s converted mana cost.').
card_first_print('induce despair', 'ROE').
card_image_name('induce despair'/'ROE', 'induce despair').
card_uid('induce despair'/'ROE', 'ROE:Induce Despair:induce despair').
card_rarity('induce despair'/'ROE', 'Common').
card_artist('induce despair'/'ROE', 'Igor Kieryluk').
card_number('induce despair'/'ROE', '114').
card_flavor_text('induce despair'/'ROE', 'All the angel saw was her doom.').
card_multiverse_id('induce despair'/'ROE', '193520').

card_in_set('inquisition of kozilek', 'ROE').
card_original_type('inquisition of kozilek'/'ROE', 'Sorcery').
card_original_text('inquisition of kozilek'/'ROE', 'Target player reveals his or her hand. You choose a nonland card from it with converted mana cost 3 or less. That player discards that card.').
card_first_print('inquisition of kozilek', 'ROE').
card_image_name('inquisition of kozilek'/'ROE', 'inquisition of kozilek').
card_uid('inquisition of kozilek'/'ROE', 'ROE:Inquisition of Kozilek:inquisition of kozilek').
card_rarity('inquisition of kozilek'/'ROE', 'Uncommon').
card_artist('inquisition of kozilek'/'ROE', 'Tomasz Jedruszek').
card_number('inquisition of kozilek'/'ROE', '115').
card_flavor_text('inquisition of kozilek'/'ROE', 'You will scream out your innermost secrets just to make it stop.').
card_multiverse_id('inquisition of kozilek'/'ROE', '193428').

card_in_set('irresistible prey', 'ROE').
card_original_type('irresistible prey'/'ROE', 'Sorcery').
card_original_text('irresistible prey'/'ROE', 'Target creature must be blocked this turn if able.\nDraw a card.').
card_first_print('irresistible prey', 'ROE').
card_image_name('irresistible prey'/'ROE', 'irresistible prey').
card_uid('irresistible prey'/'ROE', 'ROE:Irresistible Prey:irresistible prey').
card_rarity('irresistible prey'/'ROE', 'Uncommon').
card_artist('irresistible prey'/'ROE', 'Jesper Ejsing').
card_number('irresistible prey'/'ROE', '188').
card_flavor_text('irresistible prey'/'ROE', 'The baloth\'s instincts boiled, its muscles pulled taut, and its razor-spines bristled, as it turned to face—a runt meepling.').
card_multiverse_id('irresistible prey'/'ROE', '193527').

card_in_set('island', 'ROE').
card_original_type('island'/'ROE', 'Basic Land — Island').
card_original_text('island'/'ROE', 'U').
card_image_name('island'/'ROE', 'island1').
card_uid('island'/'ROE', 'ROE:Island:island1').
card_rarity('island'/'ROE', 'Basic Land').
card_artist('island'/'ROE', 'Vincent Proce').
card_number('island'/'ROE', '233').
card_multiverse_id('island'/'ROE', '205932').

card_in_set('island', 'ROE').
card_original_type('island'/'ROE', 'Basic Land — Island').
card_original_text('island'/'ROE', 'U').
card_image_name('island'/'ROE', 'island2').
card_uid('island'/'ROE', 'ROE:Island:island2').
card_rarity('island'/'ROE', 'Basic Land').
card_artist('island'/'ROE', 'Vincent Proce').
card_number('island'/'ROE', '234').
card_multiverse_id('island'/'ROE', '205939').

card_in_set('island', 'ROE').
card_original_type('island'/'ROE', 'Basic Land — Island').
card_original_text('island'/'ROE', 'U').
card_image_name('island'/'ROE', 'island3').
card_uid('island'/'ROE', 'ROE:Island:island3').
card_rarity('island'/'ROE', 'Basic Land').
card_artist('island'/'ROE', 'Vincent Proce').
card_number('island'/'ROE', '235').
card_multiverse_id('island'/'ROE', '205944').

card_in_set('island', 'ROE').
card_original_type('island'/'ROE', 'Basic Land — Island').
card_original_text('island'/'ROE', 'U').
card_image_name('island'/'ROE', 'island4').
card_uid('island'/'ROE', 'ROE:Island:island4').
card_rarity('island'/'ROE', 'Basic Land').
card_artist('island'/'ROE', 'Vincent Proce').
card_number('island'/'ROE', '236').
card_multiverse_id('island'/'ROE', '205945').

card_in_set('it that betrays', 'ROE').
card_original_type('it that betrays'/'ROE', 'Creature — Eldrazi').
card_original_text('it that betrays'/'ROE', 'Annihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)\nWhenever an opponent sacrifices a nontoken permanent, put that card onto the battlefield under your control.').
card_first_print('it that betrays', 'ROE').
card_image_name('it that betrays'/'ROE', 'it that betrays').
card_uid('it that betrays'/'ROE', 'ROE:It That Betrays:it that betrays').
card_rarity('it that betrays'/'ROE', 'Rare').
card_artist('it that betrays'/'ROE', 'Tomasz Jedruszek').
card_number('it that betrays'/'ROE', '7').
card_flavor_text('it that betrays'/'ROE', 'Your pleas for death shall go unheard.').
card_multiverse_id('it that betrays'/'ROE', '198171').

card_in_set('jaddi lifestrider', 'ROE').
card_original_type('jaddi lifestrider'/'ROE', 'Creature — Elemental').
card_original_text('jaddi lifestrider'/'ROE', 'When Jaddi Lifestrider enters the battlefield, you may tap any number of untapped creatures you control. You gain 2 life for each creature tapped this way.').
card_first_print('jaddi lifestrider', 'ROE').
card_image_name('jaddi lifestrider'/'ROE', 'jaddi lifestrider').
card_uid('jaddi lifestrider'/'ROE', 'ROE:Jaddi Lifestrider:jaddi lifestrider').
card_rarity('jaddi lifestrider'/'ROE', 'Uncommon').
card_artist('jaddi lifestrider'/'ROE', 'Vincent Proce').
card_number('jaddi lifestrider'/'ROE', '189').
card_flavor_text('jaddi lifestrider'/'ROE', 'Nature\'s arms encircle all who are willing to draw close.').
card_multiverse_id('jaddi lifestrider'/'ROE', '193464').

card_in_set('joraga treespeaker', 'ROE').
card_original_type('joraga treespeaker'/'ROE', 'Creature — Elf Druid').
card_original_text('joraga treespeaker'/'ROE', 'Level up {1}{G} ({1}{G}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n1/2\n{T}: Add {G}{G} to your mana pool.\nLEVEL 5+\n1/4\nElves you control have \"{T}: Add {G}{G} to your mana pool.\"').
card_first_print('joraga treespeaker', 'ROE').
card_image_name('joraga treespeaker'/'ROE', 'joraga treespeaker').
card_uid('joraga treespeaker'/'ROE', 'ROE:Joraga Treespeaker:joraga treespeaker').
card_rarity('joraga treespeaker'/'ROE', 'Uncommon').
card_artist('joraga treespeaker'/'ROE', 'Cyril Van Der Haegen').
card_number('joraga treespeaker'/'ROE', '190').
card_multiverse_id('joraga treespeaker'/'ROE', '193462').

card_in_set('jwari scuttler', 'ROE').
card_original_type('jwari scuttler'/'ROE', 'Creature — Crab').
card_original_text('jwari scuttler'/'ROE', '').
card_first_print('jwari scuttler', 'ROE').
card_image_name('jwari scuttler'/'ROE', 'jwari scuttler').
card_uid('jwari scuttler'/'ROE', 'ROE:Jwari Scuttler:jwari scuttler').
card_rarity('jwari scuttler'/'ROE', 'Common').
card_artist('jwari scuttler'/'ROE', 'Andrew Robinson').
card_number('jwari scuttler'/'ROE', '73').
card_flavor_text('jwari scuttler'/'ROE', '\"Yeah, they\'ve got a lot of meat. The only downside to eating \'em is that you often find human bones and body parts inside.\"\n—Jaby, Silundi Sea nomad').
card_multiverse_id('jwari scuttler'/'ROE', '193470').

card_in_set('kabira vindicator', 'ROE').
card_original_type('kabira vindicator'/'ROE', 'Creature — Human Knight').
card_original_text('kabira vindicator'/'ROE', 'Level up {2}{W} ({2}{W}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-4\n3/6\nOther creatures you control get +1/+1.\nLEVEL 5+\n4/8\nOther creatures you control get +2/+2.').
card_first_print('kabira vindicator', 'ROE').
card_image_name('kabira vindicator'/'ROE', 'kabira vindicator').
card_uid('kabira vindicator'/'ROE', 'ROE:Kabira Vindicator:kabira vindicator').
card_rarity('kabira vindicator'/'ROE', 'Uncommon').
card_artist('kabira vindicator'/'ROE', 'Steven Belledin').
card_number('kabira vindicator'/'ROE', '28').
card_multiverse_id('kabira vindicator'/'ROE', '193433').

card_in_set('kargan dragonlord', 'ROE').
card_original_type('kargan dragonlord'/'ROE', 'Creature — Human Warrior').
card_original_text('kargan dragonlord'/'ROE', 'Level up {R} ({R}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 4-7\n4/4\nFlying\nLEVEL 8+\n8/8\nFlying, trample\n{R}: Kargan Dragonlord gets +1/+0 until end of turn.').
card_first_print('kargan dragonlord', 'ROE').
card_image_name('kargan dragonlord'/'ROE', 'kargan dragonlord').
card_uid('kargan dragonlord'/'ROE', 'ROE:Kargan Dragonlord:kargan dragonlord').
card_rarity('kargan dragonlord'/'ROE', 'Mythic Rare').
card_artist('kargan dragonlord'/'ROE', 'Jason Chan').
card_number('kargan dragonlord'/'ROE', '152').
card_multiverse_id('kargan dragonlord'/'ROE', '193482').

card_in_set('kazandu tuskcaller', 'ROE').
card_original_type('kazandu tuskcaller'/'ROE', 'Creature — Human Shaman').
card_original_text('kazandu tuskcaller'/'ROE', 'Level up {1}{G} ({1}{G}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-5\n1/1\n{T}: Put a 3/3 green Elephant creature token onto the battlefield.\nLEVEL 6+\n1/1\n{T}: Put two 3/3 green Elephant creature tokens onto the battlefield.').
card_first_print('kazandu tuskcaller', 'ROE').
card_image_name('kazandu tuskcaller'/'ROE', 'kazandu tuskcaller').
card_uid('kazandu tuskcaller'/'ROE', 'ROE:Kazandu Tuskcaller:kazandu tuskcaller').
card_rarity('kazandu tuskcaller'/'ROE', 'Rare').
card_artist('kazandu tuskcaller'/'ROE', 'Mike Bierek').
card_number('kazandu tuskcaller'/'ROE', '191').
card_multiverse_id('kazandu tuskcaller'/'ROE', '193611').

card_in_set('keening stone', 'ROE').
card_original_type('keening stone'/'ROE', 'Artifact').
card_original_text('keening stone'/'ROE', '{5}, {T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of cards in that player\'s graveyard.').
card_first_print('keening stone', 'ROE').
card_image_name('keening stone'/'ROE', 'keening stone').
card_uid('keening stone'/'ROE', 'ROE:Keening Stone:keening stone').
card_rarity('keening stone'/'ROE', 'Rare').
card_artist('keening stone'/'ROE', 'Jung Park').
card_number('keening stone'/'ROE', '219').
card_flavor_text('keening stone'/'ROE', 'As it spins, dead voices shriek in an ever-increasing cacophony, rending the mind asunder.').
card_multiverse_id('keening stone'/'ROE', '193441').

card_in_set('khalni hydra', 'ROE').
card_original_type('khalni hydra'/'ROE', 'Creature — Hydra').
card_original_text('khalni hydra'/'ROE', 'Khalni Hydra costs {G} less to cast for each green creature you control.\nTrample').
card_first_print('khalni hydra', 'ROE').
card_image_name('khalni hydra'/'ROE', 'khalni hydra').
card_uid('khalni hydra'/'ROE', 'ROE:Khalni Hydra:khalni hydra').
card_rarity('khalni hydra'/'ROE', 'Mythic Rare').
card_artist('khalni hydra'/'ROE', 'Todd Lockwood').
card_number('khalni hydra'/'ROE', '192').
card_flavor_text('khalni hydra'/'ROE', '\"In ages past, bargains were struck and promises were made. Now we must collect on our debt. Begin the hymns.\"\n—Moruul, Khalni druid').
card_multiverse_id('khalni hydra'/'ROE', '193551').

card_in_set('kiln fiend', 'ROE').
card_original_type('kiln fiend'/'ROE', 'Creature — Elemental Beast').
card_original_text('kiln fiend'/'ROE', 'Whenever you cast an instant or sorcery spell, Kiln Fiend gets +3/+0 until end of turn.').
card_first_print('kiln fiend', 'ROE').
card_image_name('kiln fiend'/'ROE', 'kiln fiend').
card_uid('kiln fiend'/'ROE', 'ROE:Kiln Fiend:kiln fiend').
card_rarity('kiln fiend'/'ROE', 'Common').
card_artist('kiln fiend'/'ROE', 'Adi Granov').
card_number('kiln fiend'/'ROE', '153').
card_flavor_text('kiln fiend'/'ROE', 'It traps an explosion within its stony skin.').
card_multiverse_id('kiln fiend'/'ROE', '194912').

card_in_set('knight of cliffhaven', 'ROE').
card_original_type('knight of cliffhaven'/'ROE', 'Creature — Kor Knight').
card_original_text('knight of cliffhaven'/'ROE', 'Level up {3} ({3}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n2/3\nFlying\nLEVEL 4+\n4/4\nFlying, vigilance').
card_first_print('knight of cliffhaven', 'ROE').
card_image_name('knight of cliffhaven'/'ROE', 'knight of cliffhaven').
card_uid('knight of cliffhaven'/'ROE', 'ROE:Knight of Cliffhaven:knight of cliffhaven').
card_rarity('knight of cliffhaven'/'ROE', 'Common').
card_artist('knight of cliffhaven'/'ROE', 'Matt Cavotta').
card_number('knight of cliffhaven'/'ROE', '29').
card_multiverse_id('knight of cliffhaven'/'ROE', '194951').

card_in_set('kor line-slinger', 'ROE').
card_original_type('kor line-slinger'/'ROE', 'Creature — Kor Scout').
card_original_text('kor line-slinger'/'ROE', '{T}: Tap target creature with power 3 or less.').
card_first_print('kor line-slinger', 'ROE').
card_image_name('kor line-slinger'/'ROE', 'kor line-slinger').
card_uid('kor line-slinger'/'ROE', 'ROE:Kor Line-Slinger:kor line-slinger').
card_rarity('kor line-slinger'/'ROE', 'Common').
card_artist('kor line-slinger'/'ROE', 'Steve Prescott').
card_number('kor line-slinger'/'ROE', '30').
card_flavor_text('kor line-slinger'/'ROE', '\"I tried to tell her to stay behind, that this fight was too dangerous. I spent the next hour tied to the rafters.\"\n—Zahr Gada, Halimar expedition leader').
card_multiverse_id('kor line-slinger'/'ROE', '193550').

card_in_set('kor spiritdancer', 'ROE').
card_original_type('kor spiritdancer'/'ROE', 'Creature — Kor Wizard').
card_original_text('kor spiritdancer'/'ROE', 'Kor Spiritdancer gets +2/+2 for each Aura attached to it.\nWhenever you cast an Aura spell, you may draw a card.').
card_first_print('kor spiritdancer', 'ROE').
card_image_name('kor spiritdancer'/'ROE', 'kor spiritdancer').
card_uid('kor spiritdancer'/'ROE', 'ROE:Kor Spiritdancer:kor spiritdancer').
card_rarity('kor spiritdancer'/'ROE', 'Rare').
card_artist('kor spiritdancer'/'ROE', 'Scott Chou').
card_number('kor spiritdancer'/'ROE', '31').
card_flavor_text('kor spiritdancer'/'ROE', 'She reaches beyond the physical realm, touching the ideals from which all creatures draw their power.').
card_multiverse_id('kor spiritdancer'/'ROE', '193544').

card_in_set('kozilek\'s predator', 'ROE').
card_original_type('kozilek\'s predator'/'ROE', 'Creature — Eldrazi Drone').
card_original_text('kozilek\'s predator'/'ROE', 'When Kozilek\'s Predator enters the battlefield, put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('kozilek\'s predator', 'ROE').
card_image_name('kozilek\'s predator'/'ROE', 'kozilek\'s predator').
card_uid('kozilek\'s predator'/'ROE', 'ROE:Kozilek\'s Predator:kozilek\'s predator').
card_rarity('kozilek\'s predator'/'ROE', 'Common').
card_artist('kozilek\'s predator'/'ROE', 'Steve Argyle').
card_number('kozilek\'s predator'/'ROE', '193').
card_flavor_text('kozilek\'s predator'/'ROE', 'It\'s difficult to outwit something that doesn\'t speak, strategize, or even think.').
card_multiverse_id('kozilek\'s predator'/'ROE', '198303').

card_in_set('kozilek, butcher of truth', 'ROE').
card_original_type('kozilek, butcher of truth'/'ROE', 'Legendary Creature — Eldrazi').
card_original_text('kozilek, butcher of truth'/'ROE', 'When you cast Kozilek, Butcher of Truth, draw four cards.\nAnnihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.)\nWhen Kozilek is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_first_print('kozilek, butcher of truth', 'ROE').
card_image_name('kozilek, butcher of truth'/'ROE', 'kozilek, butcher of truth').
card_uid('kozilek, butcher of truth'/'ROE', 'ROE:Kozilek, Butcher of Truth:kozilek, butcher of truth').
card_rarity('kozilek, butcher of truth'/'ROE', 'Mythic Rare').
card_artist('kozilek, butcher of truth'/'ROE', 'Michael Komarck').
card_number('kozilek, butcher of truth'/'ROE', '6').
card_multiverse_id('kozilek, butcher of truth'/'ROE', '193632').

card_in_set('lagac lizard', 'ROE').
card_original_type('lagac lizard'/'ROE', 'Creature — Lizard').
card_original_text('lagac lizard'/'ROE', '').
card_first_print('lagac lizard', 'ROE').
card_image_name('lagac lizard'/'ROE', 'lagac lizard').
card_uid('lagac lizard'/'ROE', 'ROE:Lagac Lizard:lagac lizard').
card_rarity('lagac lizard'/'ROE', 'Common').
card_artist('lagac lizard'/'ROE', 'Svetlin Velinov').
card_number('lagac lizard'/'ROE', '154').
card_flavor_text('lagac lizard'/'ROE', 'Tracing their ancestry back to Zendikar\'s earliest forms of life, lagac lizards have seen the comings and goings of planeswalkers and the Eldrazi, and the rise of the vampire clans, none of which has changed them one bit.').
card_multiverse_id('lagac lizard'/'ROE', '198162').

card_in_set('last kiss', 'ROE').
card_original_type('last kiss'/'ROE', 'Instant').
card_original_text('last kiss'/'ROE', 'Last Kiss deals 2 damage to target creature and you gain 2 life.').
card_first_print('last kiss', 'ROE').
card_image_name('last kiss'/'ROE', 'last kiss').
card_uid('last kiss'/'ROE', 'ROE:Last Kiss:last kiss').
card_rarity('last kiss'/'ROE', 'Common').
card_artist('last kiss'/'ROE', 'Vance Kovacs').
card_number('last kiss'/'ROE', '116').
card_flavor_text('last kiss'/'ROE', '\"Romanticize it, glamorize it, call it what you will. To me, it will always be carnal, bloody murder.\"\n—Ayli, Kamsa cleric').
card_multiverse_id('last kiss'/'ROE', '193546').

card_in_set('lavafume invoker', 'ROE').
card_original_type('lavafume invoker'/'ROE', 'Creature — Goblin Shaman').
card_original_text('lavafume invoker'/'ROE', '{8}: Creatures you control get +3/+0 until end of turn.').
card_first_print('lavafume invoker', 'ROE').
card_image_name('lavafume invoker'/'ROE', 'lavafume invoker').
card_uid('lavafume invoker'/'ROE', 'ROE:Lavafume Invoker:lavafume invoker').
card_rarity('lavafume invoker'/'ROE', 'Common').
card_artist('lavafume invoker'/'ROE', 'Dave Kendall').
card_number('lavafume invoker'/'ROE', '155').
card_flavor_text('lavafume invoker'/'ROE', '\"Then the ancient masters themselves, towers of rapacity, rose and began their calamitous feast.\"\n—The Invokers\' Tales').
card_multiverse_id('lavafume invoker'/'ROE', '193624').

card_in_set('lay bare', 'ROE').
card_original_type('lay bare'/'ROE', 'Instant').
card_original_text('lay bare'/'ROE', 'Counter target spell. Look at its controller\'s hand.').
card_first_print('lay bare', 'ROE').
card_image_name('lay bare'/'ROE', 'lay bare').
card_uid('lay bare'/'ROE', 'ROE:Lay Bare:lay bare').
card_rarity('lay bare'/'ROE', 'Common').
card_artist('lay bare'/'ROE', 'Chippy').
card_number('lay bare'/'ROE', '74').
card_flavor_text('lay bare'/'ROE', '\"It\'s good to learn from your failures, but I prefer to learn from the failures of others.\"\n—Jace Beleren').
card_multiverse_id('lay bare'/'ROE', '193469').

card_in_set('leaf arrow', 'ROE').
card_original_type('leaf arrow'/'ROE', 'Instant').
card_original_text('leaf arrow'/'ROE', 'Leaf Arrow deals 3 damage to target creature with flying.').
card_first_print('leaf arrow', 'ROE').
card_image_name('leaf arrow'/'ROE', 'leaf arrow').
card_uid('leaf arrow'/'ROE', 'ROE:Leaf Arrow:leaf arrow').
card_rarity('leaf arrow'/'ROE', 'Common').
card_artist('leaf arrow'/'ROE', 'Eric Deschamps').
card_number('leaf arrow'/'ROE', '194').
card_flavor_text('leaf arrow'/'ROE', '\"Those who think the trees shall remain bystanders throughout this conflict shall be sorely mistaken.\"\n—Sutina, Speaker of the Tajuru').
card_multiverse_id('leaf arrow'/'ROE', '205929').

card_in_set('lighthouse chronologist', 'ROE').
card_original_type('lighthouse chronologist'/'ROE', 'Creature — Human Wizard').
card_original_text('lighthouse chronologist'/'ROE', 'Level up {U} ({U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 4-6\n2/4\nLEVEL 7+\n3/5\nAt the beginning of each end step, if it\'s not your turn, take an extra turn after this one.').
card_first_print('lighthouse chronologist', 'ROE').
card_image_name('lighthouse chronologist'/'ROE', 'lighthouse chronologist').
card_uid('lighthouse chronologist'/'ROE', 'ROE:Lighthouse Chronologist:lighthouse chronologist').
card_rarity('lighthouse chronologist'/'ROE', 'Mythic Rare').
card_artist('lighthouse chronologist'/'ROE', 'Steven Belledin').
card_number('lighthouse chronologist'/'ROE', '75').
card_multiverse_id('lighthouse chronologist'/'ROE', '193590').

card_in_set('lightmine field', 'ROE').
card_original_type('lightmine field'/'ROE', 'Enchantment').
card_original_text('lightmine field'/'ROE', 'Whenever one or more creatures attack, Lightmine Field deals damage to each of those creatures equal to the number of attacking creatures.').
card_first_print('lightmine field', 'ROE').
card_image_name('lightmine field'/'ROE', 'lightmine field').
card_uid('lightmine field'/'ROE', 'ROE:Lightmine Field:lightmine field').
card_rarity('lightmine field'/'ROE', 'Rare').
card_artist('lightmine field'/'ROE', 'Eric Deschamps').
card_number('lightmine field'/'ROE', '32').
card_flavor_text('lightmine field'/'ROE', 'If you want to kill a lot of goblins, just make sure your defenses look like fun.').
card_multiverse_id('lightmine field'/'ROE', '193463').

card_in_set('linvala, keeper of silence', 'ROE').
card_original_type('linvala, keeper of silence'/'ROE', 'Legendary Creature — Angel').
card_original_text('linvala, keeper of silence'/'ROE', 'Flying\nActivated abilities of creatures your opponents control can\'t be activated.').
card_first_print('linvala, keeper of silence', 'ROE').
card_image_name('linvala, keeper of silence'/'ROE', 'linvala, keeper of silence').
card_uid('linvala, keeper of silence'/'ROE', 'ROE:Linvala, Keeper of Silence:linvala, keeper of silence').
card_rarity('linvala, keeper of silence'/'ROE', 'Mythic Rare').
card_artist('linvala, keeper of silence'/'ROE', 'Igor Kieryluk').
card_number('linvala, keeper of silence'/'ROE', '33').
card_flavor_text('linvala, keeper of silence'/'ROE', 'Those who seek to disturb the harmony of life will see their instruments taken from them.').
card_multiverse_id('linvala, keeper of silence'/'ROE', '193660').

card_in_set('living destiny', 'ROE').
card_original_type('living destiny'/'ROE', 'Instant').
card_original_text('living destiny'/'ROE', 'As an additional cost to cast Living Destiny, reveal a creature card from your hand.\nYou gain life equal to the revealed card\'s converted mana cost.').
card_first_print('living destiny', 'ROE').
card_image_name('living destiny'/'ROE', 'living destiny').
card_uid('living destiny'/'ROE', 'ROE:Living Destiny:living destiny').
card_rarity('living destiny'/'ROE', 'Common').
card_artist('living destiny'/'ROE', 'James Ryman').
card_number('living destiny'/'ROE', '195').
card_flavor_text('living destiny'/'ROE', '\"That our enemies are great only brings us greater hope.\"').
card_multiverse_id('living destiny'/'ROE', '193489').

card_in_set('lone missionary', 'ROE').
card_original_type('lone missionary'/'ROE', 'Creature — Kor Monk').
card_original_text('lone missionary'/'ROE', 'When Lone Missionary enters the battlefield, you gain 4 life.').
card_first_print('lone missionary', 'ROE').
card_image_name('lone missionary'/'ROE', 'lone missionary').
card_uid('lone missionary'/'ROE', 'ROE:Lone Missionary:lone missionary').
card_rarity('lone missionary'/'ROE', 'Common').
card_artist('lone missionary'/'ROE', 'Svetlin Velinov').
card_number('lone missionary'/'ROE', '34').
card_flavor_text('lone missionary'/'ROE', 'His mission has become a grim pilgrimage, a tour of the Eldrazi-stricken outposts across Zendikar. But he marches on alone, stubborn as the daily dawn.').
card_multiverse_id('lone missionary'/'ROE', '193426').

card_in_set('lord of shatterskull pass', 'ROE').
card_original_type('lord of shatterskull pass'/'ROE', 'Creature — Minotaur Shaman').
card_original_text('lord of shatterskull pass'/'ROE', 'Level up {1}{R} ({1}{R}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-5\n6/6\nLEVEL 6+\n6/6\nWhenever Lord of Shatterskull Pass attacks, it deals 6 damage to each creature defending player controls.').
card_image_name('lord of shatterskull pass'/'ROE', 'lord of shatterskull pass').
card_uid('lord of shatterskull pass'/'ROE', 'ROE:Lord of Shatterskull Pass:lord of shatterskull pass').
card_rarity('lord of shatterskull pass'/'ROE', 'Rare').
card_artist('lord of shatterskull pass'/'ROE', 'Kekai Kotaki').
card_number('lord of shatterskull pass'/'ROE', '156').
card_multiverse_id('lord of shatterskull pass'/'ROE', '198164').

card_in_set('luminous wake', 'ROE').
card_original_type('luminous wake'/'ROE', 'Enchantment — Aura').
card_original_text('luminous wake'/'ROE', 'Enchant creature\nWhenever enchanted creature attacks or blocks, you gain 4 life.').
card_first_print('luminous wake', 'ROE').
card_image_name('luminous wake'/'ROE', 'luminous wake').
card_uid('luminous wake'/'ROE', 'ROE:Luminous Wake:luminous wake').
card_rarity('luminous wake'/'ROE', 'Uncommon').
card_artist('luminous wake'/'ROE', 'Mike Bierek').
card_number('luminous wake'/'ROE', '35').
card_flavor_text('luminous wake'/'ROE', '\"If destiny\'s champions have arisen to fight the Ancient Ones, then let me, too, do my part.\"\n—Ravuel of Graypelt').
card_multiverse_id('luminous wake'/'ROE', '193549').

card_in_set('lust for war', 'ROE').
card_original_type('lust for war'/'ROE', 'Enchantment — Aura').
card_original_text('lust for war'/'ROE', 'Enchant creature\nWhenever enchanted creature becomes tapped, Lust for War deals 3 damage to that creature\'s controller.\nEnchanted creature attacks each turn if able.').
card_first_print('lust for war', 'ROE').
card_image_name('lust for war'/'ROE', 'lust for war').
card_uid('lust for war'/'ROE', 'ROE:Lust for War:lust for war').
card_rarity('lust for war'/'ROE', 'Uncommon').
card_artist('lust for war'/'ROE', 'Raymond Swanland').
card_number('lust for war'/'ROE', '157').
card_multiverse_id('lust for war'/'ROE', '193518').

card_in_set('magmaw', 'ROE').
card_original_type('magmaw'/'ROE', 'Creature — Elemental').
card_original_text('magmaw'/'ROE', '{1}, Sacrifice a nonland permanent: Magmaw deals 1 damage to target creature or player.').
card_first_print('magmaw', 'ROE').
card_image_name('magmaw'/'ROE', 'magmaw').
card_uid('magmaw'/'ROE', 'ROE:Magmaw:magmaw').
card_rarity('magmaw'/'ROE', 'Rare').
card_artist('magmaw'/'ROE', 'Karl Kopinski').
card_number('magmaw'/'ROE', '158').
card_flavor_text('magmaw'/'ROE', '\"The purpose of existence is simple: everything is fuel for the magmaw.\"\n—Jaji, magmaw worshipper').
card_multiverse_id('magmaw'/'ROE', '193453').

card_in_set('makindi griffin', 'ROE').
card_original_type('makindi griffin'/'ROE', 'Creature — Griffin').
card_original_text('makindi griffin'/'ROE', 'Flying').
card_first_print('makindi griffin', 'ROE').
card_image_name('makindi griffin'/'ROE', 'makindi griffin').
card_uid('makindi griffin'/'ROE', 'ROE:Makindi Griffin:makindi griffin').
card_rarity('makindi griffin'/'ROE', 'Common').
card_artist('makindi griffin'/'ROE', 'Izzy').
card_number('makindi griffin'/'ROE', '36').
card_flavor_text('makindi griffin'/'ROE', 'As the hedrons began to coalesce into colossal Eldrazi superstructures, the griffins were forced to seek new territory lest their aeries be crushed between the massive stone monoliths.').
card_multiverse_id('makindi griffin'/'ROE', '193514').

card_in_set('mammoth umbra', 'ROE').
card_original_type('mammoth umbra'/'ROE', 'Enchantment — Aura').
card_original_text('mammoth umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +3/+3 and has vigilance.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('mammoth umbra', 'ROE').
card_image_name('mammoth umbra'/'ROE', 'mammoth umbra').
card_uid('mammoth umbra'/'ROE', 'ROE:Mammoth Umbra:mammoth umbra').
card_rarity('mammoth umbra'/'ROE', 'Uncommon').
card_artist('mammoth umbra'/'ROE', 'Howard Lyon').
card_number('mammoth umbra'/'ROE', '37').
card_multiverse_id('mammoth umbra'/'ROE', '193541').

card_in_set('merfolk observer', 'ROE').
card_original_type('merfolk observer'/'ROE', 'Creature — Merfolk Rogue').
card_original_text('merfolk observer'/'ROE', 'When Merfolk Observer enters the battlefield, look at the top card of target player\'s library.').
card_first_print('merfolk observer', 'ROE').
card_image_name('merfolk observer'/'ROE', 'merfolk observer').
card_uid('merfolk observer'/'ROE', 'ROE:Merfolk Observer:merfolk observer').
card_rarity('merfolk observer'/'ROE', 'Common').
card_artist('merfolk observer'/'ROE', 'Anthony Francisco').
card_number('merfolk observer'/'ROE', '76').
card_flavor_text('merfolk observer'/'ROE', '\"The structure gets bigger every hour, with hedrons coalescing even deep under the surface. If I could only get inside . . . .\"').
card_multiverse_id('merfolk observer'/'ROE', '198173').

card_in_set('merfolk skyscout', 'ROE').
card_original_type('merfolk skyscout'/'ROE', 'Creature — Merfolk Scout').
card_original_text('merfolk skyscout'/'ROE', 'Flying\nWhenever Merfolk Skyscout attacks or blocks, untap target permanent.').
card_first_print('merfolk skyscout', 'ROE').
card_image_name('merfolk skyscout'/'ROE', 'merfolk skyscout').
card_uid('merfolk skyscout'/'ROE', 'ROE:Merfolk Skyscout:merfolk skyscout').
card_rarity('merfolk skyscout'/'ROE', 'Uncommon').
card_artist('merfolk skyscout'/'ROE', 'rk post').
card_number('merfolk skyscout'/'ROE', '77').
card_flavor_text('merfolk skyscout'/'ROE', '\"Emeria is a pleasant lie, a figment to hide Emrakul\'s hideous face. I can only hope to uncover a truth that lies deeper still.\"').
card_multiverse_id('merfolk skyscout'/'ROE', '193505').

card_in_set('might of the masses', 'ROE').
card_original_type('might of the masses'/'ROE', 'Instant').
card_original_text('might of the masses'/'ROE', 'Target creature gets +1/+1 until end of turn for each creature you control.').
card_first_print('might of the masses', 'ROE').
card_image_name('might of the masses'/'ROE', 'might of the masses').
card_uid('might of the masses'/'ROE', 'ROE:Might of the Masses:might of the masses').
card_rarity('might of the masses'/'ROE', 'Common').
card_artist('might of the masses'/'ROE', 'Johann Bodin').
card_number('might of the masses'/'ROE', '196').
card_flavor_text('might of the masses'/'ROE', 'The Joraga elves never need ask a troll to leave their territory. They merely grant it their combined strength, and it can\'t resist embarking on a merry rampage.').
card_multiverse_id('might of the masses'/'ROE', '193547').

card_in_set('mnemonic wall', 'ROE').
card_original_type('mnemonic wall'/'ROE', 'Creature — Wall').
card_original_text('mnemonic wall'/'ROE', 'Defender\nWhen Mnemonic Wall enters the battlefield, you may return target instant or sorcery card from your graveyard to your hand.').
card_first_print('mnemonic wall', 'ROE').
card_image_name('mnemonic wall'/'ROE', 'mnemonic wall').
card_uid('mnemonic wall'/'ROE', 'ROE:Mnemonic Wall:mnemonic wall').
card_rarity('mnemonic wall'/'ROE', 'Common').
card_artist('mnemonic wall'/'ROE', 'Vance Kovacs').
card_number('mnemonic wall'/'ROE', '78').
card_flavor_text('mnemonic wall'/'ROE', '\"I\'d build an entire fortress of them if I could.\"\n—Mzali, Lighthouse archmage').
card_multiverse_id('mnemonic wall'/'ROE', '193491').

card_in_set('momentous fall', 'ROE').
card_original_type('momentous fall'/'ROE', 'Instant').
card_original_text('momentous fall'/'ROE', 'As an additional cost to cast Momentous Fall, sacrifice a creature.\nYou draw cards equal to the sacrificed creature\'s power, then you gain life equal to its toughness.').
card_first_print('momentous fall', 'ROE').
card_image_name('momentous fall'/'ROE', 'momentous fall').
card_uid('momentous fall'/'ROE', 'ROE:Momentous Fall:momentous fall').
card_rarity('momentous fall'/'ROE', 'Rare').
card_artist('momentous fall'/'ROE', 'Tomasz Jedruszek').
card_number('momentous fall'/'ROE', '197').
card_flavor_text('momentous fall'/'ROE', 'It wasn\'t long until the elves had developed a use for every part of the Eldrazi.').
card_multiverse_id('momentous fall'/'ROE', '193497').

card_in_set('mortician beetle', 'ROE').
card_original_type('mortician beetle'/'ROE', 'Creature — Insect').
card_original_text('mortician beetle'/'ROE', 'Whenever a player sacrifices a creature, you may put a +1/+1 counter on Mortician Beetle.').
card_first_print('mortician beetle', 'ROE').
card_image_name('mortician beetle'/'ROE', 'mortician beetle').
card_uid('mortician beetle'/'ROE', 'ROE:Mortician Beetle:mortician beetle').
card_rarity('mortician beetle'/'ROE', 'Rare').
card_artist('mortician beetle'/'ROE', 'Lars Grant-West').
card_number('mortician beetle'/'ROE', '117').
card_flavor_text('mortician beetle'/'ROE', 'To the soldier, war is famine; to the scavenger, a feast.').
card_multiverse_id('mortician beetle'/'ROE', '209108').

card_in_set('mountain', 'ROE').
card_original_type('mountain'/'ROE', 'Basic Land — Mountain').
card_original_text('mountain'/'ROE', 'R').
card_image_name('mountain'/'ROE', 'mountain1').
card_uid('mountain'/'ROE', 'ROE:Mountain:mountain1').
card_rarity('mountain'/'ROE', 'Basic Land').
card_artist('mountain'/'ROE', 'James Paick').
card_number('mountain'/'ROE', '241').
card_multiverse_id('mountain'/'ROE', '205947').

card_in_set('mountain', 'ROE').
card_original_type('mountain'/'ROE', 'Basic Land — Mountain').
card_original_text('mountain'/'ROE', 'R').
card_image_name('mountain'/'ROE', 'mountain2').
card_uid('mountain'/'ROE', 'ROE:Mountain:mountain2').
card_rarity('mountain'/'ROE', 'Basic Land').
card_artist('mountain'/'ROE', 'James Paick').
card_number('mountain'/'ROE', '242').
card_multiverse_id('mountain'/'ROE', '205926').

card_in_set('mountain', 'ROE').
card_original_type('mountain'/'ROE', 'Basic Land — Mountain').
card_original_text('mountain'/'ROE', 'R').
card_image_name('mountain'/'ROE', 'mountain3').
card_uid('mountain'/'ROE', 'ROE:Mountain:mountain3').
card_rarity('mountain'/'ROE', 'Basic Land').
card_artist('mountain'/'ROE', 'James Paick').
card_number('mountain'/'ROE', '243').
card_multiverse_id('mountain'/'ROE', '205923').

card_in_set('mountain', 'ROE').
card_original_type('mountain'/'ROE', 'Basic Land — Mountain').
card_original_text('mountain'/'ROE', 'R').
card_image_name('mountain'/'ROE', 'mountain4').
card_uid('mountain'/'ROE', 'ROE:Mountain:mountain4').
card_rarity('mountain'/'ROE', 'Basic Land').
card_artist('mountain'/'ROE', 'James Paick').
card_number('mountain'/'ROE', '244').
card_multiverse_id('mountain'/'ROE', '205937').

card_in_set('mul daya channelers', 'ROE').
card_original_type('mul daya channelers'/'ROE', 'Creature — Elf Druid Shaman').
card_original_text('mul daya channelers'/'ROE', 'Play with the top card of your library revealed.\nAs long as the top card of your library is a creature card, Mul Daya Channelers gets +3/+3.\nAs long as the top card of your library is a land card, Mul Daya Channelers has \"{T}: Add two mana of any one color to your mana pool.\"').
card_first_print('mul daya channelers', 'ROE').
card_image_name('mul daya channelers'/'ROE', 'mul daya channelers').
card_uid('mul daya channelers'/'ROE', 'ROE:Mul Daya Channelers:mul daya channelers').
card_rarity('mul daya channelers'/'ROE', 'Rare').
card_artist('mul daya channelers'/'ROE', 'Jason Chan').
card_number('mul daya channelers'/'ROE', '198').
card_multiverse_id('mul daya channelers'/'ROE', '198167').

card_in_set('narcolepsy', 'ROE').
card_original_type('narcolepsy'/'ROE', 'Enchantment — Aura').
card_original_text('narcolepsy'/'ROE', 'Enchant creature\nAt the beginning of each upkeep, if enchanted creature is untapped, tap it.').
card_first_print('narcolepsy', 'ROE').
card_image_name('narcolepsy'/'ROE', 'narcolepsy').
card_uid('narcolepsy'/'ROE', 'ROE:Narcolepsy:narcolepsy').
card_rarity('narcolepsy'/'ROE', 'Common').
card_artist('narcolepsy'/'ROE', 'Johann Bodin').
card_number('narcolepsy'/'ROE', '79').
card_flavor_text('narcolepsy'/'ROE', '\"It\'s so cute when it\'s sleeping, isn\'t it? Actually, it\'s still as abhorrent as ever, but at least it\'s not trying to kill us.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('narcolepsy'/'ROE', '193594').

card_in_set('naturalize', 'ROE').
card_original_type('naturalize'/'ROE', 'Instant').
card_original_text('naturalize'/'ROE', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'ROE', 'naturalize').
card_uid('naturalize'/'ROE', 'ROE:Naturalize:naturalize').
card_rarity('naturalize'/'ROE', 'Common').
card_artist('naturalize'/'ROE', 'Howard Lyon').
card_number('naturalize'/'ROE', '199').
card_flavor_text('naturalize'/'ROE', '\"The scholars wept when we destroyed the hedron. I had no such pity. What would this world be like had we done so sooner?\"\n—The War Diaries').
card_multiverse_id('naturalize'/'ROE', '193439').

card_in_set('near-death experience', 'ROE').
card_original_type('near-death experience'/'ROE', 'Enchantment').
card_original_text('near-death experience'/'ROE', 'At the beginning of your upkeep, if you have exactly 1 life, you win the game.').
card_first_print('near-death experience', 'ROE').
card_image_name('near-death experience'/'ROE', 'near-death experience').
card_uid('near-death experience'/'ROE', 'ROE:Near-Death Experience:near-death experience').
card_rarity('near-death experience'/'ROE', 'Rare').
card_artist('near-death experience'/'ROE', 'Dan Scott').
card_number('near-death experience'/'ROE', '38').
card_flavor_text('near-death experience'/'ROE', 'Lands ravaged, cities in ruins, so many lives sacrificed, and yet there was no other word for it but victory.').
card_multiverse_id('near-death experience'/'ROE', '193467').

card_in_set('nema siltlurker', 'ROE').
card_original_type('nema siltlurker'/'ROE', 'Creature — Lizard').
card_original_text('nema siltlurker'/'ROE', '').
card_first_print('nema siltlurker', 'ROE').
card_image_name('nema siltlurker'/'ROE', 'nema siltlurker').
card_uid('nema siltlurker'/'ROE', 'ROE:Nema Siltlurker:nema siltlurker').
card_rarity('nema siltlurker'/'ROE', 'Common').
card_artist('nema siltlurker'/'ROE', 'Wayne Reynolds').
card_number('nema siltlurker'/'ROE', '200').
card_flavor_text('nema siltlurker'/'ROE', 'In one gargantuan bite, it will swallow not only you but also the riverbank you were standing on.').
card_multiverse_id('nema siltlurker'/'ROE', '193628').

card_in_set('nest invader', 'ROE').
card_original_type('nest invader'/'ROE', 'Creature — Eldrazi Drone').
card_original_text('nest invader'/'ROE', 'When Nest Invader enters the battlefield, put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('nest invader', 'ROE').
card_image_name('nest invader'/'ROE', 'nest invader').
card_uid('nest invader'/'ROE', 'ROE:Nest Invader:nest invader').
card_rarity('nest invader'/'ROE', 'Common').
card_artist('nest invader'/'ROE', 'Trevor Claxton').
card_number('nest invader'/'ROE', '201').
card_flavor_text('nest invader'/'ROE', 'It nurtures its masters\' glorious future.').
card_multiverse_id('nest invader'/'ROE', '193420').

card_in_set('nighthaze', 'ROE').
card_original_type('nighthaze'/'ROE', 'Sorcery').
card_original_text('nighthaze'/'ROE', 'Target creature gains swampwalk until end of turn.\nDraw a card.').
card_first_print('nighthaze', 'ROE').
card_image_name('nighthaze'/'ROE', 'nighthaze').
card_uid('nighthaze'/'ROE', 'ROE:Nighthaze:nighthaze').
card_rarity('nighthaze'/'ROE', 'Common').
card_artist('nighthaze'/'ROE', 'Tomasz Jedruszek').
card_number('nighthaze'/'ROE', '118').
card_flavor_text('nighthaze'/'ROE', '\"To evade the brood lineages, one must be made dark to all the senses.\"\n—Traga, Zulaport runner').
card_multiverse_id('nighthaze'/'ROE', '193487').

card_in_set('nirkana cutthroat', 'ROE').
card_original_type('nirkana cutthroat'/'ROE', 'Creature — Vampire Warrior').
card_original_text('nirkana cutthroat'/'ROE', 'Level up {2}{B} ({2}{B}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n4/3\nDeathtouch\nLEVEL 3+\n5/4\nFirst strike, deathtouch').
card_first_print('nirkana cutthroat', 'ROE').
card_image_name('nirkana cutthroat'/'ROE', 'nirkana cutthroat').
card_uid('nirkana cutthroat'/'ROE', 'ROE:Nirkana Cutthroat:nirkana cutthroat').
card_rarity('nirkana cutthroat'/'ROE', 'Uncommon').
card_artist('nirkana cutthroat'/'ROE', 'Erica Yang').
card_number('nirkana cutthroat'/'ROE', '119').
card_multiverse_id('nirkana cutthroat'/'ROE', '194933').

card_in_set('nirkana revenant', 'ROE').
card_original_type('nirkana revenant'/'ROE', 'Creature — Vampire Shade').
card_original_text('nirkana revenant'/'ROE', 'Whenever you tap a Swamp for mana, add {B} to your mana pool (in addition to the mana the land produces).\n{B}: Nirkana Revenant gets +1/+1 until end of turn.').
card_first_print('nirkana revenant', 'ROE').
card_image_name('nirkana revenant'/'ROE', 'nirkana revenant').
card_uid('nirkana revenant'/'ROE', 'ROE:Nirkana Revenant:nirkana revenant').
card_rarity('nirkana revenant'/'ROE', 'Mythic Rare').
card_artist('nirkana revenant'/'ROE', 'Igor Kieryluk').
card_number('nirkana revenant'/'ROE', '120').
card_flavor_text('nirkana revenant'/'ROE', 'Hate is an everlasting wellspring from which it is eternally sustained.').
card_multiverse_id('nirkana revenant'/'ROE', '193472').

card_in_set('nomads\' assembly', 'ROE').
card_original_type('nomads\' assembly'/'ROE', 'Sorcery').
card_original_text('nomads\' assembly'/'ROE', 'Put a 1/1 white Kor Soldier creature token onto the battlefield for each creature you control.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('nomads\' assembly', 'ROE').
card_image_name('nomads\' assembly'/'ROE', 'nomads\' assembly').
card_uid('nomads\' assembly'/'ROE', 'ROE:Nomads\' Assembly:nomads\' assembly').
card_rarity('nomads\' assembly'/'ROE', 'Rare').
card_artist('nomads\' assembly'/'ROE', 'Erica Yang').
card_number('nomads\' assembly'/'ROE', '39').
card_multiverse_id('nomads\' assembly'/'ROE', '198180').

card_in_set('not of this world', 'ROE').
card_original_type('not of this world'/'ROE', 'Tribal Instant — Eldrazi').
card_original_text('not of this world'/'ROE', 'Counter target spell or ability that targets a permanent you control.\nNot of This World costs {7} less to cast if it targets a spell or ability that targets a creature you control with power 7 or greater.').
card_first_print('not of this world', 'ROE').
card_image_name('not of this world'/'ROE', 'not of this world').
card_uid('not of this world'/'ROE', 'ROE:Not of This World:not of this world').
card_rarity('not of this world'/'ROE', 'Uncommon').
card_artist('not of this world'/'ROE', 'Izzy').
card_number('not of this world'/'ROE', '8').
card_multiverse_id('not of this world'/'ROE', '198296').

card_in_set('null champion', 'ROE').
card_original_type('null champion'/'ROE', 'Creature — Zombie Warrior').
card_original_text('null champion'/'ROE', 'Level up {3} ({3}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n4/2\nLEVEL 4+\n7/3\n{B}: Regenerate Null Champion.').
card_first_print('null champion', 'ROE').
card_image_name('null champion'/'ROE', 'null champion').
card_uid('null champion'/'ROE', 'ROE:Null Champion:null champion').
card_rarity('null champion'/'ROE', 'Common').
card_artist('null champion'/'ROE', 'Chris Rahn').
card_number('null champion'/'ROE', '121').
card_multiverse_id('null champion'/'ROE', '193647').

card_in_set('ogre sentry', 'ROE').
card_original_type('ogre sentry'/'ROE', 'Creature — Ogre Warrior').
card_original_text('ogre sentry'/'ROE', 'Defender').
card_first_print('ogre sentry', 'ROE').
card_image_name('ogre sentry'/'ROE', 'ogre sentry').
card_uid('ogre sentry'/'ROE', 'ROE:Ogre Sentry:ogre sentry').
card_rarity('ogre sentry'/'ROE', 'Common').
card_artist('ogre sentry'/'ROE', 'Eric Deschamps').
card_number('ogre sentry'/'ROE', '159').
card_flavor_text('ogre sentry'/'ROE', '\"You have to appreciate the genius of it. Why bother building defenses when you can just fill the pass with angry ogres?\"\n—Javad Nasrin, Ondu relic hunter').
card_multiverse_id('ogre sentry'/'ROE', '193583').

card_in_set('ogre\'s cleaver', 'ROE').
card_original_type('ogre\'s cleaver'/'ROE', 'Artifact — Equipment').
card_original_text('ogre\'s cleaver'/'ROE', 'Equipped creature gets +5/+0.\nEquip {5}').
card_first_print('ogre\'s cleaver', 'ROE').
card_image_name('ogre\'s cleaver'/'ROE', 'ogre\'s cleaver').
card_uid('ogre\'s cleaver'/'ROE', 'ROE:Ogre\'s Cleaver:ogre\'s cleaver').
card_rarity('ogre\'s cleaver'/'ROE', 'Uncommon').
card_artist('ogre\'s cleaver'/'ROE', 'Adi Granov').
card_number('ogre\'s cleaver'/'ROE', '220').
card_flavor_text('ogre\'s cleaver'/'ROE', 'She adopted the weapon of the slave-lord Kazuul, and with it, all his cruelty.').
card_multiverse_id('ogre\'s cleaver'/'ROE', '193517').

card_in_set('ondu giant', 'ROE').
card_original_type('ondu giant'/'ROE', 'Creature — Giant Druid').
card_original_text('ondu giant'/'ROE', 'When Ondu Giant enters the battlefield, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('ondu giant', 'ROE').
card_image_name('ondu giant'/'ROE', 'ondu giant').
card_uid('ondu giant'/'ROE', 'ROE:Ondu Giant:ondu giant').
card_rarity('ondu giant'/'ROE', 'Common').
card_artist('ondu giant'/'ROE', 'Igor Kieryluk').
card_number('ondu giant'/'ROE', '202').
card_flavor_text('ondu giant'/'ROE', 'Some druids nurture gardens. Others nurture continents.').
card_multiverse_id('ondu giant'/'ROE', '194929').

card_in_set('oust', 'ROE').
card_original_type('oust'/'ROE', 'Sorcery').
card_original_text('oust'/'ROE', 'Put target creature into its owner\'s library second from the top. Its controller gains 3 life.').
card_first_print('oust', 'ROE').
card_image_name('oust'/'ROE', 'oust').
card_uid('oust'/'ROE', 'ROE:Oust:oust').
card_rarity('oust'/'ROE', 'Uncommon').
card_artist('oust'/'ROE', 'Mike Bierek').
card_number('oust'/'ROE', '40').
card_flavor_text('oust'/'ROE', '\"‘Invincible\' is just a word.\"\n—Gideon Jura').
card_multiverse_id('oust'/'ROE', '194923').

card_in_set('overgrown battlement', 'ROE').
card_original_type('overgrown battlement'/'ROE', 'Creature — Wall').
card_original_text('overgrown battlement'/'ROE', 'Defender\n{T}: Add {G} to your mana pool for each creature with defender you control.').
card_first_print('overgrown battlement', 'ROE').
card_image_name('overgrown battlement'/'ROE', 'overgrown battlement').
card_uid('overgrown battlement'/'ROE', 'ROE:Overgrown Battlement:overgrown battlement').
card_rarity('overgrown battlement'/'ROE', 'Common').
card_artist('overgrown battlement'/'ROE', 'Franz Vohwinkel').
card_number('overgrown battlement'/'ROE', '203').
card_flavor_text('overgrown battlement'/'ROE', '\"Our enemy is nothing less than the end of all life. We shall not want for allies.\"\n—The War Diaries').
card_multiverse_id('overgrown battlement'/'ROE', '193610').

card_in_set('pathrazer of ulamog', 'ROE').
card_original_type('pathrazer of ulamog'/'ROE', 'Creature — Eldrazi').
card_original_text('pathrazer of ulamog'/'ROE', 'Annihilator 3 (Whenever this creature attacks, defending player sacrifices three permanents.)\nPathrazer of Ulamog can\'t be blocked except by three or more creatures.').
card_image_name('pathrazer of ulamog'/'ROE', 'pathrazer of ulamog').
card_uid('pathrazer of ulamog'/'ROE', 'ROE:Pathrazer of Ulamog:pathrazer of ulamog').
card_rarity('pathrazer of ulamog'/'ROE', 'Uncommon').
card_artist('pathrazer of ulamog'/'ROE', 'Austin Hsu').
card_number('pathrazer of ulamog'/'ROE', '9').
card_flavor_text('pathrazer of ulamog'/'ROE', 'No thought but hunger. No strategy but destruction.').
card_multiverse_id('pathrazer of ulamog'/'ROE', '193607').

card_in_set('pawn of ulamog', 'ROE').
card_original_type('pawn of ulamog'/'ROE', 'Creature — Vampire Shaman').
card_original_text('pawn of ulamog'/'ROE', 'Whenever Pawn of Ulamog or another nontoken creature you control is put into a graveyard from the battlefield, you may put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('pawn of ulamog', 'ROE').
card_image_name('pawn of ulamog'/'ROE', 'pawn of ulamog').
card_uid('pawn of ulamog'/'ROE', 'ROE:Pawn of Ulamog:pawn of ulamog').
card_rarity('pawn of ulamog'/'ROE', 'Uncommon').
card_artist('pawn of ulamog'/'ROE', 'Daarken').
card_number('pawn of ulamog'/'ROE', '122').
card_multiverse_id('pawn of ulamog'/'ROE', '194907').

card_in_set('pelakka wurm', 'ROE').
card_original_type('pelakka wurm'/'ROE', 'Creature — Wurm').
card_original_text('pelakka wurm'/'ROE', 'Trample\nWhen Pelakka Wurm enters the battlefield, you gain 7 life.\nWhen Pelakka Wurm is put into a graveyard from the battlefield, draw a card.').
card_first_print('pelakka wurm', 'ROE').
card_image_name('pelakka wurm'/'ROE', 'pelakka wurm').
card_uid('pelakka wurm'/'ROE', 'ROE:Pelakka Wurm:pelakka wurm').
card_rarity('pelakka wurm'/'ROE', 'Uncommon').
card_artist('pelakka wurm'/'ROE', 'Daniel Ljunggren').
card_number('pelakka wurm'/'ROE', '204').
card_flavor_text('pelakka wurm'/'ROE', 'It eats what it wants to eat—which is anything that moves.').
card_multiverse_id('pelakka wurm'/'ROE', '193621').

card_in_set('pennon blade', 'ROE').
card_original_type('pennon blade'/'ROE', 'Artifact — Equipment').
card_original_text('pennon blade'/'ROE', 'Equipped creature gets +1/+1 for each creature you control.\nEquip {4}').
card_first_print('pennon blade', 'ROE').
card_image_name('pennon blade'/'ROE', 'pennon blade').
card_uid('pennon blade'/'ROE', 'ROE:Pennon Blade:pennon blade').
card_rarity('pennon blade'/'ROE', 'Uncommon').
card_artist('pennon blade'/'ROE', 'Alex Horley-Orlandelli').
card_number('pennon blade'/'ROE', '221').
card_flavor_text('pennon blade'/'ROE', 'Draped with the pennon of a fallen outpost, the sword came to symbolize the peoples of Zendikar becoming one.').
card_multiverse_id('pennon blade'/'ROE', '193528').

card_in_set('perish the thought', 'ROE').
card_original_type('perish the thought'/'ROE', 'Sorcery').
card_original_text('perish the thought'/'ROE', 'Target opponent reveals his or her hand. You choose a card from it. That player shuffles that card into his or her library.').
card_first_print('perish the thought', 'ROE').
card_image_name('perish the thought'/'ROE', 'perish the thought').
card_uid('perish the thought'/'ROE', 'ROE:Perish the Thought:perish the thought').
card_rarity('perish the thought'/'ROE', 'Common').
card_artist('perish the thought'/'ROE', 'Austin Hsu').
card_number('perish the thought'/'ROE', '123').
card_flavor_text('perish the thought'/'ROE', 'It removed only one idea from her mind: defiance.').
card_multiverse_id('perish the thought'/'ROE', '194915').

card_in_set('pestilence demon', 'ROE').
card_original_type('pestilence demon'/'ROE', 'Creature — Demon').
card_original_text('pestilence demon'/'ROE', 'Flying\n{B}: Pestilence Demon deals 1 damage to each creature and each player.').
card_first_print('pestilence demon', 'ROE').
card_image_name('pestilence demon'/'ROE', 'pestilence demon').
card_uid('pestilence demon'/'ROE', 'ROE:Pestilence Demon:pestilence demon').
card_rarity('pestilence demon'/'ROE', 'Rare').
card_artist('pestilence demon'/'ROE', 'Justin Sweet').
card_number('pestilence demon'/'ROE', '124').
card_flavor_text('pestilence demon'/'ROE', '\"I have schemed too long to be supplanted by dead gods. If I cannot have this world, no one can.\"').
card_multiverse_id('pestilence demon'/'ROE', '193431').

card_in_set('phantasmal abomination', 'ROE').
card_original_type('phantasmal abomination'/'ROE', 'Creature — Illusion').
card_original_text('phantasmal abomination'/'ROE', 'Defender\nWhen Phantasmal Abomination becomes the target of a spell or ability, sacrifice it.').
card_first_print('phantasmal abomination', 'ROE').
card_image_name('phantasmal abomination'/'ROE', 'phantasmal abomination').
card_uid('phantasmal abomination'/'ROE', 'ROE:Phantasmal Abomination:phantasmal abomination').
card_rarity('phantasmal abomination'/'ROE', 'Uncommon').
card_artist('phantasmal abomination'/'ROE', 'Goran Josic').
card_number('phantasmal abomination'/'ROE', '80').
card_flavor_text('phantasmal abomination'/'ROE', 'Create an illusion so strong that it becomes another\'s reality, and you can become the master of all.').
card_multiverse_id('phantasmal abomination'/'ROE', '198174').

card_in_set('plains', 'ROE').
card_original_type('plains'/'ROE', 'Basic Land — Plains').
card_original_text('plains'/'ROE', 'W').
card_image_name('plains'/'ROE', 'plains1').
card_uid('plains'/'ROE', 'ROE:Plains:plains1').
card_rarity('plains'/'ROE', 'Basic Land').
card_artist('plains'/'ROE', 'John Avon').
card_number('plains'/'ROE', '229').
card_multiverse_id('plains'/'ROE', '205925').

card_in_set('plains', 'ROE').
card_original_type('plains'/'ROE', 'Basic Land — Plains').
card_original_text('plains'/'ROE', 'W').
card_image_name('plains'/'ROE', 'plains2').
card_uid('plains'/'ROE', 'ROE:Plains:plains2').
card_rarity('plains'/'ROE', 'Basic Land').
card_artist('plains'/'ROE', 'John Avon').
card_number('plains'/'ROE', '230').
card_multiverse_id('plains'/'ROE', '205938').

card_in_set('plains', 'ROE').
card_original_type('plains'/'ROE', 'Basic Land — Plains').
card_original_text('plains'/'ROE', 'W').
card_image_name('plains'/'ROE', 'plains3').
card_uid('plains'/'ROE', 'ROE:Plains:plains3').
card_rarity('plains'/'ROE', 'Basic Land').
card_artist('plains'/'ROE', 'John Avon').
card_number('plains'/'ROE', '231').
card_multiverse_id('plains'/'ROE', '205931').

card_in_set('plains', 'ROE').
card_original_type('plains'/'ROE', 'Basic Land — Plains').
card_original_text('plains'/'ROE', 'W').
card_image_name('plains'/'ROE', 'plains4').
card_uid('plains'/'ROE', 'ROE:Plains:plains4').
card_rarity('plains'/'ROE', 'Basic Land').
card_artist('plains'/'ROE', 'John Avon').
card_number('plains'/'ROE', '232').
card_multiverse_id('plains'/'ROE', '205928').

card_in_set('prey\'s vengeance', 'ROE').
card_original_type('prey\'s vengeance'/'ROE', 'Instant').
card_original_text('prey\'s vengeance'/'ROE', 'Target creature gets +2/+2 until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('prey\'s vengeance', 'ROE').
card_image_name('prey\'s vengeance'/'ROE', 'prey\'s vengeance').
card_uid('prey\'s vengeance'/'ROE', 'ROE:Prey\'s Vengeance:prey\'s vengeance').
card_rarity('prey\'s vengeance'/'ROE', 'Uncommon').
card_artist('prey\'s vengeance'/'ROE', 'Jesper Ejsing').
card_number('prey\'s vengeance'/'ROE', '205').
card_multiverse_id('prey\'s vengeance'/'ROE', '193565').

card_in_set('prophetic prism', 'ROE').
card_original_type('prophetic prism'/'ROE', 'Artifact').
card_original_text('prophetic prism'/'ROE', 'When Prophetic Prism enters the battlefield, draw a card.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_first_print('prophetic prism', 'ROE').
card_image_name('prophetic prism'/'ROE', 'prophetic prism').
card_uid('prophetic prism'/'ROE', 'ROE:Prophetic Prism:prophetic prism').
card_rarity('prophetic prism'/'ROE', 'Common').
card_artist('prophetic prism'/'ROE', 'John Avon').
card_number('prophetic prism'/'ROE', '222').
card_flavor_text('prophetic prism'/'ROE', '\"The explorer who found it got a bad deal when he sold it. He merely got rich.\"').
card_multiverse_id('prophetic prism'/'ROE', '198295').

card_in_set('puncturing light', 'ROE').
card_original_type('puncturing light'/'ROE', 'Instant').
card_original_text('puncturing light'/'ROE', 'Destroy target attacking or blocking creature with power 3 or less.').
card_first_print('puncturing light', 'ROE').
card_image_name('puncturing light'/'ROE', 'puncturing light').
card_uid('puncturing light'/'ROE', 'ROE:Puncturing Light:puncturing light').
card_rarity('puncturing light'/'ROE', 'Common').
card_artist('puncturing light'/'ROE', 'Zoltan Boros & Gabor Szikszai').
card_number('puncturing light'/'ROE', '41').
card_flavor_text('puncturing light'/'ROE', '\"The vampires knew what was coming. I know it. And they did nothing. They deserve to feel the same agony they\'ve caused all of us.\"\n—Anitan, Ondu cleric').
card_multiverse_id('puncturing light'/'ROE', '193566').

card_in_set('rage nimbus', 'ROE').
card_original_type('rage nimbus'/'ROE', 'Creature — Elemental').
card_original_text('rage nimbus'/'ROE', 'Defender, flying\n{1}{R}: Target creature attacks this turn if able.').
card_first_print('rage nimbus', 'ROE').
card_image_name('rage nimbus'/'ROE', 'rage nimbus').
card_uid('rage nimbus'/'ROE', 'ROE:Rage Nimbus:rage nimbus').
card_rarity('rage nimbus'/'ROE', 'Rare').
card_artist('rage nimbus'/'ROE', 'Vincent Proce').
card_number('rage nimbus'/'ROE', '160').
card_flavor_text('rage nimbus'/'ROE', 'The settlement knew there was something strange about the cloud when the old priest Mandli suddenly grabbed an axe and ran screaming into the hills.').
card_multiverse_id('rage nimbus'/'ROE', '193454').

card_in_set('raid bombardment', 'ROE').
card_original_type('raid bombardment'/'ROE', 'Enchantment').
card_original_text('raid bombardment'/'ROE', 'Whenever a creature you control with power 2 or less attacks, Raid Bombardment deals 1 damage to defending player.').
card_first_print('raid bombardment', 'ROE').
card_image_name('raid bombardment'/'ROE', 'raid bombardment').
card_uid('raid bombardment'/'ROE', 'ROE:Raid Bombardment:raid bombardment').
card_rarity('raid bombardment'/'ROE', 'Common').
card_artist('raid bombardment'/'ROE', 'Matt Cavotta').
card_number('raid bombardment'/'ROE', '161').
card_flavor_text('raid bombardment'/'ROE', 'Goblins in the first wave of a fire raid always bemoan the aim of everyone else.').
card_multiverse_id('raid bombardment'/'ROE', '193573').

card_in_set('rapacious one', 'ROE').
card_original_type('rapacious one'/'ROE', 'Creature — Eldrazi Drone').
card_original_text('rapacious one'/'ROE', 'Trample\nWhenever Rapacious One deals combat damage to a player, put that many 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('rapacious one', 'ROE').
card_image_name('rapacious one'/'ROE', 'rapacious one').
card_uid('rapacious one'/'ROE', 'ROE:Rapacious One:rapacious one').
card_rarity('rapacious one'/'ROE', 'Uncommon').
card_artist('rapacious one'/'ROE', 'Jason A. Engle').
card_number('rapacious one'/'ROE', '162').
card_multiverse_id('rapacious one'/'ROE', '198181').

card_in_set('reality spasm', 'ROE').
card_original_type('reality spasm'/'ROE', 'Instant').
card_original_text('reality spasm'/'ROE', 'Choose one — Tap X target permanents; or untap X target permanents.').
card_first_print('reality spasm', 'ROE').
card_image_name('reality spasm'/'ROE', 'reality spasm').
card_uid('reality spasm'/'ROE', 'ROE:Reality Spasm:reality spasm').
card_rarity('reality spasm'/'ROE', 'Uncommon').
card_artist('reality spasm'/'ROE', 'Jason Felix').
card_number('reality spasm'/'ROE', '81').
card_flavor_text('reality spasm'/'ROE', 'From the moment the first Eldrazi spawn squirmed free, everything on Zendikar took on an adjusted meaning, a new slant toward or away from the waking giants.').
card_multiverse_id('reality spasm'/'ROE', '193652').

card_in_set('realms uncharted', 'ROE').
card_original_type('realms uncharted'/'ROE', 'Instant').
card_original_text('realms uncharted'/'ROE', 'Search your library for four land cards with different names and reveal them. An opponent chooses two of those cards. Put the chosen cards into your graveyard and the rest into your hand. Then shuffle your library.').
card_first_print('realms uncharted', 'ROE').
card_image_name('realms uncharted'/'ROE', 'realms uncharted').
card_uid('realms uncharted'/'ROE', 'ROE:Realms Uncharted:realms uncharted').
card_rarity('realms uncharted'/'ROE', 'Rare').
card_artist('realms uncharted'/'ROE', 'Volkan Baga').
card_number('realms uncharted'/'ROE', '206').
card_multiverse_id('realms uncharted'/'ROE', '193605').

card_in_set('recurring insight', 'ROE').
card_original_type('recurring insight'/'ROE', 'Sorcery').
card_original_text('recurring insight'/'ROE', 'Draw cards equal to the number of cards in target opponent\'s hand.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('recurring insight', 'ROE').
card_image_name('recurring insight'/'ROE', 'recurring insight').
card_uid('recurring insight'/'ROE', 'ROE:Recurring Insight:recurring insight').
card_rarity('recurring insight'/'ROE', 'Rare').
card_artist('recurring insight'/'ROE', 'Zoltan Boros & Gabor Szikszai').
card_number('recurring insight'/'ROE', '82').
card_multiverse_id('recurring insight'/'ROE', '194936').

card_in_set('regress', 'ROE').
card_original_type('regress'/'ROE', 'Instant').
card_original_text('regress'/'ROE', 'Return target permanent to its owner\'s hand.').
card_image_name('regress'/'ROE', 'regress').
card_uid('regress'/'ROE', 'ROE:Regress:regress').
card_rarity('regress'/'ROE', 'Common').
card_artist('regress'/'ROE', 'Izzy').
card_number('regress'/'ROE', '83').
card_flavor_text('regress'/'ROE', '\"Never look too deep, where the waters turn dark. What the sea has hidden there may not stay down forever.\"\n—Jaby, Silundi Sea nomad').
card_multiverse_id('regress'/'ROE', '193603').

card_in_set('reinforced bulwark', 'ROE').
card_original_type('reinforced bulwark'/'ROE', 'Artifact Creature — Wall').
card_original_text('reinforced bulwark'/'ROE', 'Defender\n{T}: Prevent the next 1 damage that would be dealt to you this turn.').
card_first_print('reinforced bulwark', 'ROE').
card_image_name('reinforced bulwark'/'ROE', 'reinforced bulwark').
card_uid('reinforced bulwark'/'ROE', 'ROE:Reinforced Bulwark:reinforced bulwark').
card_rarity('reinforced bulwark'/'ROE', 'Common').
card_artist('reinforced bulwark'/'ROE', 'Ryan Pancoast').
card_number('reinforced bulwark'/'ROE', '223').
card_flavor_text('reinforced bulwark'/'ROE', 'Built of wood and iron. Held together by hope and prayer.').
card_multiverse_id('reinforced bulwark'/'ROE', '193654').

card_in_set('renegade doppelganger', 'ROE').
card_original_type('renegade doppelganger'/'ROE', 'Creature — Shapeshifter').
card_original_text('renegade doppelganger'/'ROE', 'Whenever another creature enters the battlefield under your control, you may have Renegade Doppelganger become a copy of that creature until end of turn. (If it does, it loses this ability for the rest of the turn.)').
card_first_print('renegade doppelganger', 'ROE').
card_image_name('renegade doppelganger'/'ROE', 'renegade doppelganger').
card_uid('renegade doppelganger'/'ROE', 'ROE:Renegade Doppelganger:renegade doppelganger').
card_rarity('renegade doppelganger'/'ROE', 'Rare').
card_artist('renegade doppelganger'/'ROE', 'James Ryman').
card_number('renegade doppelganger'/'ROE', '84').
card_flavor_text('renegade doppelganger'/'ROE', 'To truly know a creature, slither a mile in its tentacles.').
card_multiverse_id('renegade doppelganger'/'ROE', '193582').

card_in_set('repay in kind', 'ROE').
card_original_type('repay in kind'/'ROE', 'Sorcery').
card_original_text('repay in kind'/'ROE', 'Each player\'s life total becomes the lowest life total among all players.').
card_first_print('repay in kind', 'ROE').
card_image_name('repay in kind'/'ROE', 'repay in kind').
card_uid('repay in kind'/'ROE', 'ROE:Repay in Kind:repay in kind').
card_rarity('repay in kind'/'ROE', 'Rare').
card_artist('repay in kind'/'ROE', 'Vance Kovacs').
card_number('repay in kind'/'ROE', '125').
card_flavor_text('repay in kind'/'ROE', 'Sorin has gathered grudges for centuries, patiently awaiting the day he can exact the perfect vengeance.').
card_multiverse_id('repay in kind'/'ROE', '193657').

card_in_set('repel the darkness', 'ROE').
card_original_type('repel the darkness'/'ROE', 'Instant').
card_original_text('repel the darkness'/'ROE', 'Tap up to two target creatures.\nDraw a card.').
card_first_print('repel the darkness', 'ROE').
card_image_name('repel the darkness'/'ROE', 'repel the darkness').
card_uid('repel the darkness'/'ROE', 'ROE:Repel the Darkness:repel the darkness').
card_rarity('repel the darkness'/'ROE', 'Common').
card_artist('repel the darkness'/'ROE', 'Scott Chou').
card_number('repel the darkness'/'ROE', '42').
card_flavor_text('repel the darkness'/'ROE', 'A boon to those who cannot see in the dark. A bane to those who live in it.').
card_multiverse_id('repel the darkness'/'ROE', '194930').

card_in_set('runed servitor', 'ROE').
card_original_type('runed servitor'/'ROE', 'Artifact Creature — Construct').
card_original_text('runed servitor'/'ROE', 'When Runed Servitor is put into a graveyard from the battlefield, each player draws a card.').
card_first_print('runed servitor', 'ROE').
card_image_name('runed servitor'/'ROE', 'runed servitor').
card_uid('runed servitor'/'ROE', 'ROE:Runed Servitor:runed servitor').
card_rarity('runed servitor'/'ROE', 'Uncommon').
card_artist('runed servitor'/'ROE', 'Mike Bierek').
card_number('runed servitor'/'ROE', '224').
card_flavor_text('runed servitor'/'ROE', 'Scholars had puzzled for centuries over the ruins at Tal Terig. Its secrets had always lived within one rune-carved head.').
card_multiverse_id('runed servitor'/'ROE', '194946').

card_in_set('sarkhan the mad', 'ROE').
card_original_type('sarkhan the mad'/'ROE', 'Planeswalker — Sarkhan').
card_original_text('sarkhan the mad'/'ROE', '0: Reveal the top card of your library and put it into your hand. Sarkhan the Mad deals damage to himself equal to that card\'s converted mana cost.\n-2: Target creature\'s controller sacrifices it, then that player puts a 5/5 red Dragon creature token with flying onto the battlefield.\n-4: Each Dragon creature you control deals damage equal to its power to target player.').
card_first_print('sarkhan the mad', 'ROE').
card_image_name('sarkhan the mad'/'ROE', 'sarkhan the mad').
card_uid('sarkhan the mad'/'ROE', 'ROE:Sarkhan the Mad:sarkhan the mad').
card_rarity('sarkhan the mad'/'ROE', 'Mythic Rare').
card_artist('sarkhan the mad'/'ROE', 'Chippy').
card_number('sarkhan the mad'/'ROE', '214').
card_multiverse_id('sarkhan the mad'/'ROE', '193659').

card_in_set('sea gate oracle', 'ROE').
card_original_type('sea gate oracle'/'ROE', 'Creature — Human Wizard').
card_original_text('sea gate oracle'/'ROE', 'When Sea Gate Oracle enters the battlefield, look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.').
card_first_print('sea gate oracle', 'ROE').
card_image_name('sea gate oracle'/'ROE', 'sea gate oracle').
card_uid('sea gate oracle'/'ROE', 'ROE:Sea Gate Oracle:sea gate oracle').
card_rarity('sea gate oracle'/'ROE', 'Common').
card_artist('sea gate oracle'/'ROE', 'Daniel Ljunggren').
card_number('sea gate oracle'/'ROE', '85').
card_flavor_text('sea gate oracle'/'ROE', '\"The secret entrance should be near.\"').
card_multiverse_id('sea gate oracle'/'ROE', '194938').

card_in_set('see beyond', 'ROE').
card_original_type('see beyond'/'ROE', 'Sorcery').
card_original_text('see beyond'/'ROE', 'Draw two cards, then shuffle a card from your hand into your library.').
card_first_print('see beyond', 'ROE').
card_image_name('see beyond'/'ROE', 'see beyond').
card_uid('see beyond'/'ROE', 'ROE:See Beyond:see beyond').
card_rarity('see beyond'/'ROE', 'Common').
card_artist('see beyond'/'ROE', 'Andrew Robinson').
card_number('see beyond'/'ROE', '86').
card_flavor_text('see beyond'/'ROE', 'Ancient lore locked in a mind driven mad is just as safe as when it was locked deep underground.').
card_multiverse_id('see beyond'/'ROE', '193577').

card_in_set('shared discovery', 'ROE').
card_original_type('shared discovery'/'ROE', 'Sorcery').
card_original_text('shared discovery'/'ROE', 'As an additional cost to cast Shared Discovery, tap four untapped creatures you control.\nDraw three cards.').
card_first_print('shared discovery', 'ROE').
card_image_name('shared discovery'/'ROE', 'shared discovery').
card_uid('shared discovery'/'ROE', 'ROE:Shared Discovery:shared discovery').
card_rarity('shared discovery'/'ROE', 'Common').
card_artist('shared discovery'/'ROE', 'Ryan Pancoast').
card_number('shared discovery'/'ROE', '87').
card_flavor_text('shared discovery'/'ROE', 'Riches must be divided, but real wealth can be shared.').
card_multiverse_id('shared discovery'/'ROE', '194950').

card_in_set('shrivel', 'ROE').
card_original_type('shrivel'/'ROE', 'Sorcery').
card_original_text('shrivel'/'ROE', 'All creatures get -1/-1 until end of turn.').
card_first_print('shrivel', 'ROE').
card_image_name('shrivel'/'ROE', 'shrivel').
card_uid('shrivel'/'ROE', 'ROE:Shrivel:shrivel').
card_rarity('shrivel'/'ROE', 'Common').
card_artist('shrivel'/'ROE', 'Jung Park').
card_number('shrivel'/'ROE', '126').
card_flavor_text('shrivel'/'ROE', '\"Have you ever killed insects nibbling at your crops? I think that\'s what the Eldrazi believe they\'re doing to us.\"\n—Sheyda, Ondu gamekeeper').
card_multiverse_id('shrivel'/'ROE', '193534').

card_in_set('skeletal wurm', 'ROE').
card_original_type('skeletal wurm'/'ROE', 'Creature — Skeleton Wurm').
card_original_text('skeletal wurm'/'ROE', '{B}: Regenerate Skeletal Wurm.').
card_first_print('skeletal wurm', 'ROE').
card_image_name('skeletal wurm'/'ROE', 'skeletal wurm').
card_uid('skeletal wurm'/'ROE', 'ROE:Skeletal Wurm:skeletal wurm').
card_rarity('skeletal wurm'/'ROE', 'Uncommon').
card_artist('skeletal wurm'/'ROE', 'Zoltan Boros & Gabor Szikszai').
card_number('skeletal wurm'/'ROE', '127').
card_flavor_text('skeletal wurm'/'ROE', 'Necromancers are judged by the most powerful undead they\'ve ever created. There are those who have animated just a single being, yet are considered the pinnacle of their dark craft.').
card_multiverse_id('skeletal wurm'/'ROE', '193415').

card_in_set('skittering invasion', 'ROE').
card_original_type('skittering invasion'/'ROE', 'Tribal Sorcery — Eldrazi').
card_original_text('skittering invasion'/'ROE', 'Put five 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('skittering invasion', 'ROE').
card_image_name('skittering invasion'/'ROE', 'skittering invasion').
card_uid('skittering invasion'/'ROE', 'ROE:Skittering Invasion:skittering invasion').
card_rarity('skittering invasion'/'ROE', 'Uncommon').
card_artist('skittering invasion'/'ROE', 'Eric Deschamps').
card_number('skittering invasion'/'ROE', '10').
card_flavor_text('skittering invasion'/'ROE', '\"The brood spread far faster than we could kill it. I fear the worst is yet to come.\"\n—The War Diaries').
card_multiverse_id('skittering invasion'/'ROE', '193614').

card_in_set('skywatcher adept', 'ROE').
card_original_type('skywatcher adept'/'ROE', 'Creature — Merfolk Wizard').
card_original_text('skywatcher adept'/'ROE', 'Level up {3} ({3}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n2/2\nFlying\nLEVEL 3+\n4/2\nFlying').
card_first_print('skywatcher adept', 'ROE').
card_image_name('skywatcher adept'/'ROE', 'skywatcher adept').
card_uid('skywatcher adept'/'ROE', 'ROE:Skywatcher Adept:skywatcher adept').
card_rarity('skywatcher adept'/'ROE', 'Common').
card_artist('skywatcher adept'/'ROE', 'Tomasz Jedruszek').
card_number('skywatcher adept'/'ROE', '88').
card_multiverse_id('skywatcher adept'/'ROE', '193591').

card_in_set('smite', 'ROE').
card_original_type('smite'/'ROE', 'Instant').
card_original_text('smite'/'ROE', 'Destroy target blocked creature.').
card_image_name('smite'/'ROE', 'smite').
card_uid('smite'/'ROE', 'ROE:Smite:smite').
card_rarity('smite'/'ROE', 'Common').
card_artist('smite'/'ROE', 'Cyril Van Der Haegen').
card_number('smite'/'ROE', '43').
card_flavor_text('smite'/'ROE', '\"The mighty rely solely on their strength. They are shocked when we do the same.\"\n—Tars Olan, kor world-gift').
card_multiverse_id('smite'/'ROE', '193595').

card_in_set('snake umbra', 'ROE').
card_original_type('snake umbra'/'ROE', 'Enchantment — Aura').
card_original_text('snake umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +1/+1 and has \"Whenever this creature deals damage to an opponent, you may draw a card.\"\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('snake umbra', 'ROE').
card_image_name('snake umbra'/'ROE', 'snake umbra').
card_uid('snake umbra'/'ROE', 'ROE:Snake Umbra:snake umbra').
card_rarity('snake umbra'/'ROE', 'Common').
card_artist('snake umbra'/'ROE', 'Christopher Moeller').
card_number('snake umbra'/'ROE', '207').
card_multiverse_id('snake umbra'/'ROE', '193585').

card_in_set('soul\'s attendant', 'ROE').
card_original_type('soul\'s attendant'/'ROE', 'Creature — Human Cleric').
card_original_text('soul\'s attendant'/'ROE', 'Whenever another creature enters the battlefield, you may gain 1 life.').
card_first_print('soul\'s attendant', 'ROE').
card_image_name('soul\'s attendant'/'ROE', 'soul\'s attendant').
card_uid('soul\'s attendant'/'ROE', 'ROE:Soul\'s Attendant:soul\'s attendant').
card_rarity('soul\'s attendant'/'ROE', 'Common').
card_artist('soul\'s attendant'/'ROE', 'Steve Prescott').
card_number('soul\'s attendant'/'ROE', '44').
card_flavor_text('soul\'s attendant'/'ROE', 'In truth, her own faith was gone, trodden in Ulamog\'s wake. She pantomimed the blessing in the hope that it would inspire others to continue to struggle.').
card_multiverse_id('soul\'s attendant'/'ROE', '193499').

card_in_set('soulbound guardians', 'ROE').
card_original_type('soulbound guardians'/'ROE', 'Creature — Kor Spirit').
card_original_text('soulbound guardians'/'ROE', 'Defender, flying').
card_first_print('soulbound guardians', 'ROE').
card_image_name('soulbound guardians'/'ROE', 'soulbound guardians').
card_uid('soulbound guardians'/'ROE', 'ROE:Soulbound Guardians:soulbound guardians').
card_rarity('soulbound guardians'/'ROE', 'Uncommon').
card_artist('soulbound guardians'/'ROE', 'Erica Yang').
card_number('soulbound guardians'/'ROE', '45').
card_flavor_text('soulbound guardians'/'ROE', 'The kor\'s most righteous dead are given the greatest reward: an eternity tied to the land they so cherish.').
card_multiverse_id('soulbound guardians'/'ROE', '193615').

card_in_set('soulsurge elemental', 'ROE').
card_original_type('soulsurge elemental'/'ROE', 'Creature — Elemental').
card_original_text('soulsurge elemental'/'ROE', 'First strike\nSoulsurge Elemental\'s power is equal to the number of creatures you control.').
card_first_print('soulsurge elemental', 'ROE').
card_image_name('soulsurge elemental'/'ROE', 'soulsurge elemental').
card_uid('soulsurge elemental'/'ROE', 'ROE:Soulsurge Elemental:soulsurge elemental').
card_rarity('soulsurge elemental'/'ROE', 'Uncommon').
card_artist('soulsurge elemental'/'ROE', 'Jason A. Engle').
card_number('soulsurge elemental'/'ROE', '163').
card_flavor_text('soulsurge elemental'/'ROE', 'It draws its strength from those around it, but the pleasure of destruction is all its own.').
card_multiverse_id('soulsurge elemental'/'ROE', '193575').

card_in_set('spawning breath', 'ROE').
card_original_type('spawning breath'/'ROE', 'Instant').
card_original_text('spawning breath'/'ROE', 'Spawning Breath deals 1 damage to target creature or player. Put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_first_print('spawning breath', 'ROE').
card_image_name('spawning breath'/'ROE', 'spawning breath').
card_uid('spawning breath'/'ROE', 'ROE:Spawning Breath:spawning breath').
card_rarity('spawning breath'/'ROE', 'Common').
card_artist('spawning breath'/'ROE', 'Trevor Claxton').
card_number('spawning breath'/'ROE', '164').
card_flavor_text('spawning breath'/'ROE', 'Death by death, Kozilek\'s lineage spread.').
card_multiverse_id('spawning breath'/'ROE', '193435').

card_in_set('spawnsire of ulamog', 'ROE').
card_original_type('spawnsire of ulamog'/'ROE', 'Creature — Eldrazi').
card_original_text('spawnsire of ulamog'/'ROE', 'Annihilator 1 (Whenever this creature attacks, defending player sacrifices a permanent.)\n{4}: Put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"\n{2}0: Cast any number of Eldrazi cards you own from outside the game without paying their mana costs.').
card_first_print('spawnsire of ulamog', 'ROE').
card_image_name('spawnsire of ulamog'/'ROE', 'spawnsire of ulamog').
card_uid('spawnsire of ulamog'/'ROE', 'ROE:Spawnsire of Ulamog:spawnsire of ulamog').
card_rarity('spawnsire of ulamog'/'ROE', 'Rare').
card_artist('spawnsire of ulamog'/'ROE', 'Izzy').
card_number('spawnsire of ulamog'/'ROE', '11').
card_multiverse_id('spawnsire of ulamog'/'ROE', '193535').

card_in_set('sphinx of magosi', 'ROE').
card_original_type('sphinx of magosi'/'ROE', 'Creature — Sphinx').
card_original_text('sphinx of magosi'/'ROE', 'Flying\n{2}{U}: Draw a card, then put a +1/+1 counter on Sphinx of Magosi.').
card_first_print('sphinx of magosi', 'ROE').
card_image_name('sphinx of magosi'/'ROE', 'sphinx of magosi').
card_uid('sphinx of magosi'/'ROE', 'ROE:Sphinx of Magosi:sphinx of magosi').
card_rarity('sphinx of magosi'/'ROE', 'Rare').
card_artist('sphinx of magosi'/'ROE', 'James Ryman').
card_number('sphinx of magosi'/'ROE', '89').
card_flavor_text('sphinx of magosi'/'ROE', '\"A riddle is nothing more than a trap for small minds, baited with the promise of understanding.\"').
card_multiverse_id('sphinx of magosi'/'ROE', '198169').

card_in_set('sphinx-bone wand', 'ROE').
card_original_type('sphinx-bone wand'/'ROE', 'Artifact').
card_original_text('sphinx-bone wand'/'ROE', 'Whenever you cast an instant or sorcery spell, you may put a charge counter on Sphinx-Bone Wand. If you do, Sphinx-Bone Wand deals damage equal to the number of charge counters on it to target creature or player.').
card_first_print('sphinx-bone wand', 'ROE').
card_image_name('sphinx-bone wand'/'ROE', 'sphinx-bone wand').
card_uid('sphinx-bone wand'/'ROE', 'ROE:Sphinx-Bone Wand:sphinx-bone wand').
card_rarity('sphinx-bone wand'/'ROE', 'Rare').
card_artist('sphinx-bone wand'/'ROE', 'Franz Vohwinkel').
card_number('sphinx-bone wand'/'ROE', '225').
card_flavor_text('sphinx-bone wand'/'ROE', 'Its host\'s final curse lies captured within.').
card_multiverse_id('sphinx-bone wand'/'ROE', '193596').

card_in_set('spider umbra', 'ROE').
card_original_type('spider umbra'/'ROE', 'Enchantment — Aura').
card_original_text('spider umbra'/'ROE', 'Enchant creature\nEnchanted creature gets +1/+1 and has reach. (It can block creatures with flying.)\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('spider umbra', 'ROE').
card_image_name('spider umbra'/'ROE', 'spider umbra').
card_uid('spider umbra'/'ROE', 'ROE:Spider Umbra:spider umbra').
card_rarity('spider umbra'/'ROE', 'Common').
card_artist('spider umbra'/'ROE', 'Christopher Moeller').
card_number('spider umbra'/'ROE', '208').
card_multiverse_id('spider umbra'/'ROE', '194925').

card_in_set('splinter twin', 'ROE').
card_original_type('splinter twin'/'ROE', 'Enchantment — Aura').
card_original_text('splinter twin'/'ROE', 'Enchant creature\nEnchanted creature has \"{T}: Put a token that\'s a copy of this creature onto the battlefield. That token has haste. Exile it at the beginning of the next end step.\"').
card_first_print('splinter twin', 'ROE').
card_image_name('splinter twin'/'ROE', 'splinter twin').
card_uid('splinter twin'/'ROE', 'ROE:Splinter Twin:splinter twin').
card_rarity('splinter twin'/'ROE', 'Rare').
card_artist('splinter twin'/'ROE', 'Goran Josic').
card_number('splinter twin'/'ROE', '165').
card_flavor_text('splinter twin'/'ROE', '\"I know just the person for that job.\"').
card_multiverse_id('splinter twin'/'ROE', '193474').

card_in_set('sporecap spider', 'ROE').
card_original_type('sporecap spider'/'ROE', 'Creature — Spider').
card_original_text('sporecap spider'/'ROE', 'Reach (This creature can block creatures with flying.)').
card_first_print('sporecap spider', 'ROE').
card_image_name('sporecap spider'/'ROE', 'sporecap spider').
card_uid('sporecap spider'/'ROE', 'ROE:Sporecap Spider:sporecap spider').
card_rarity('sporecap spider'/'ROE', 'Common').
card_artist('sporecap spider'/'ROE', 'Lars Grant-West').
card_number('sporecap spider'/'ROE', '209').
card_flavor_text('sporecap spider'/'ROE', '\"They don\'t move much, but then again, if you get caught in its web, it has all the time in the world to get to you.\"\n—Saidah, Joraga hunter').
card_multiverse_id('sporecap spider'/'ROE', '193537').

card_in_set('staggershock', 'ROE').
card_original_type('staggershock'/'ROE', 'Instant').
card_original_text('staggershock'/'ROE', 'Staggershock deals 2 damage to target creature or player.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_image_name('staggershock'/'ROE', 'staggershock').
card_uid('staggershock'/'ROE', 'ROE:Staggershock:staggershock').
card_rarity('staggershock'/'ROE', 'Common').
card_artist('staggershock'/'ROE', 'Raymond Swanland').
card_number('staggershock'/'ROE', '166').
card_multiverse_id('staggershock'/'ROE', '193571').

card_in_set('stalwart shield-bearers', 'ROE').
card_original_type('stalwart shield-bearers'/'ROE', 'Creature — Human Soldier').
card_original_text('stalwart shield-bearers'/'ROE', 'Defender\nOther creatures you control with defender get +0/+2.').
card_first_print('stalwart shield-bearers', 'ROE').
card_image_name('stalwart shield-bearers'/'ROE', 'stalwart shield-bearers').
card_uid('stalwart shield-bearers'/'ROE', 'ROE:Stalwart Shield-Bearers:stalwart shield-bearers').
card_rarity('stalwart shield-bearers'/'ROE', 'Common').
card_artist('stalwart shield-bearers'/'ROE', 'Austin Hsu').
card_number('stalwart shield-bearers'/'ROE', '46').
card_flavor_text('stalwart shield-bearers'/'ROE', '\"Hold fast the line! Either we stop them here or we wake up in their guts!\"\n—Tala Vertan, Makindi shieldmate').
card_multiverse_id('stalwart shield-bearers'/'ROE', '193525').

card_in_set('stomper cub', 'ROE').
card_original_type('stomper cub'/'ROE', 'Creature — Beast').
card_original_text('stomper cub'/'ROE', 'Trample').
card_first_print('stomper cub', 'ROE').
card_image_name('stomper cub'/'ROE', 'stomper cub').
card_uid('stomper cub'/'ROE', 'ROE:Stomper Cub:stomper cub').
card_rarity('stomper cub'/'ROE', 'Common').
card_artist('stomper cub'/'ROE', 'Karl Kopinski').
card_number('stomper cub'/'ROE', '210').
card_flavor_text('stomper cub'/'ROE', '\"This one is only a yearling, but it would be wise to move away before we provide it with excellent hunting practice.\"\n—Samila, Murasa Expeditionary House').
card_multiverse_id('stomper cub'/'ROE', '193567').

card_in_set('student of warfare', 'ROE').
card_original_type('student of warfare'/'ROE', 'Creature — Human Knight').
card_original_text('student of warfare'/'ROE', 'Level up {W} ({W}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-6\n3/3\nFirst strike\nLEVEL 7+\n4/4\nDouble strike').
card_first_print('student of warfare', 'ROE').
card_image_name('student of warfare'/'ROE', 'student of warfare').
card_uid('student of warfare'/'ROE', 'ROE:Student of Warfare:student of warfare').
card_rarity('student of warfare'/'ROE', 'Rare').
card_artist('student of warfare'/'ROE', 'Volkan Baga').
card_number('student of warfare'/'ROE', '47').
card_multiverse_id('student of warfare'/'ROE', '193598').

card_in_set('suffer the past', 'ROE').
card_original_type('suffer the past'/'ROE', 'Instant').
card_original_text('suffer the past'/'ROE', 'Exile X target cards from target player\'s graveyard. For each card exiled this way, that player loses 1 life and you gain 1 life.').
card_first_print('suffer the past', 'ROE').
card_image_name('suffer the past'/'ROE', 'suffer the past').
card_uid('suffer the past'/'ROE', 'ROE:Suffer the Past:suffer the past').
card_rarity('suffer the past'/'ROE', 'Uncommon').
card_artist('suffer the past'/'ROE', 'Trevor Claxton').
card_number('suffer the past'/'ROE', '128').
card_flavor_text('suffer the past'/'ROE', 'Ulamog\'s tentacles, their touch like burning ice, rummaged through his soul and stung his memories one by one.').
card_multiverse_id('suffer the past'/'ROE', '193619').

card_in_set('surrakar spellblade', 'ROE').
card_original_type('surrakar spellblade'/'ROE', 'Creature — Surrakar').
card_original_text('surrakar spellblade'/'ROE', 'Whenever you cast an instant or sorcery spell, you may put a charge counter on Surrakar Spellblade.\nWhenever Surrakar Spellblade deals combat damage to a player, you may draw X cards, where X is the number of charge counters on it.').
card_first_print('surrakar spellblade', 'ROE').
card_image_name('surrakar spellblade'/'ROE', 'surrakar spellblade').
card_uid('surrakar spellblade'/'ROE', 'ROE:Surrakar Spellblade:surrakar spellblade').
card_rarity('surrakar spellblade'/'ROE', 'Rare').
card_artist('surrakar spellblade'/'ROE', 'Dave Kendall').
card_number('surrakar spellblade'/'ROE', '90').
card_multiverse_id('surrakar spellblade'/'ROE', '198310').

card_in_set('surreal memoir', 'ROE').
card_original_type('surreal memoir'/'ROE', 'Sorcery').
card_original_text('surreal memoir'/'ROE', 'Return an instant card at random from your graveyard to your hand.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('surreal memoir', 'ROE').
card_image_name('surreal memoir'/'ROE', 'surreal memoir').
card_uid('surreal memoir'/'ROE', 'ROE:Surreal Memoir:surreal memoir').
card_rarity('surreal memoir'/'ROE', 'Uncommon').
card_artist('surreal memoir'/'ROE', 'Jaime Jones').
card_number('surreal memoir'/'ROE', '167').
card_multiverse_id('surreal memoir'/'ROE', '198179').

card_in_set('survival cache', 'ROE').
card_original_type('survival cache'/'ROE', 'Sorcery').
card_original_text('survival cache'/'ROE', 'You gain 2 life. Then if you have more life than an opponent, draw a card.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('survival cache', 'ROE').
card_image_name('survival cache'/'ROE', 'survival cache').
card_uid('survival cache'/'ROE', 'ROE:Survival Cache:survival cache').
card_rarity('survival cache'/'ROE', 'Uncommon').
card_artist('survival cache'/'ROE', 'Scott Chou').
card_number('survival cache'/'ROE', '48').
card_multiverse_id('survival cache'/'ROE', '193531').

card_in_set('swamp', 'ROE').
card_original_type('swamp'/'ROE', 'Basic Land — Swamp').
card_original_text('swamp'/'ROE', 'B').
card_image_name('swamp'/'ROE', 'swamp1').
card_uid('swamp'/'ROE', 'ROE:Swamp:swamp1').
card_rarity('swamp'/'ROE', 'Basic Land').
card_artist('swamp'/'ROE', 'Véronique Meignaud').
card_number('swamp'/'ROE', '237').
card_multiverse_id('swamp'/'ROE', '205940').

card_in_set('swamp', 'ROE').
card_original_type('swamp'/'ROE', 'Basic Land — Swamp').
card_original_text('swamp'/'ROE', 'B').
card_image_name('swamp'/'ROE', 'swamp2').
card_uid('swamp'/'ROE', 'ROE:Swamp:swamp2').
card_rarity('swamp'/'ROE', 'Basic Land').
card_artist('swamp'/'ROE', 'Véronique Meignaud').
card_number('swamp'/'ROE', '238').
card_multiverse_id('swamp'/'ROE', '205941').

card_in_set('swamp', 'ROE').
card_original_type('swamp'/'ROE', 'Basic Land — Swamp').
card_original_text('swamp'/'ROE', 'B').
card_image_name('swamp'/'ROE', 'swamp3').
card_uid('swamp'/'ROE', 'ROE:Swamp:swamp3').
card_rarity('swamp'/'ROE', 'Basic Land').
card_artist('swamp'/'ROE', 'Véronique Meignaud').
card_number('swamp'/'ROE', '239').
card_multiverse_id('swamp'/'ROE', '205942').

card_in_set('swamp', 'ROE').
card_original_type('swamp'/'ROE', 'Basic Land — Swamp').
card_original_text('swamp'/'ROE', 'B').
card_image_name('swamp'/'ROE', 'swamp4').
card_uid('swamp'/'ROE', 'ROE:Swamp:swamp4').
card_rarity('swamp'/'ROE', 'Basic Land').
card_artist('swamp'/'ROE', 'Véronique Meignaud').
card_number('swamp'/'ROE', '240').
card_multiverse_id('swamp'/'ROE', '205948').

card_in_set('tajuru preserver', 'ROE').
card_original_type('tajuru preserver'/'ROE', 'Creature — Elf Shaman').
card_original_text('tajuru preserver'/'ROE', 'Spells and abilities your opponents control can\'t cause you to sacrifice permanents.').
card_first_print('tajuru preserver', 'ROE').
card_image_name('tajuru preserver'/'ROE', 'tajuru preserver').
card_uid('tajuru preserver'/'ROE', 'ROE:Tajuru Preserver:tajuru preserver').
card_rarity('tajuru preserver'/'ROE', 'Rare').
card_artist('tajuru preserver'/'ROE', 'rk post').
card_number('tajuru preserver'/'ROE', '211').
card_flavor_text('tajuru preserver'/'ROE', '\"No more blood shall be spilled to satisfy another\'s deluded lust for power.\"').
card_multiverse_id('tajuru preserver'/'ROE', '193451').

card_in_set('thought gorger', 'ROE').
card_original_type('thought gorger'/'ROE', 'Creature — Horror').
card_original_text('thought gorger'/'ROE', 'Trample\nWhen Thought Gorger enters the battlefield, put a +1/+1 counter on it for each card in your hand. If you do, discard your hand.\nWhen Thought Gorger leaves the battlefield, draw a card for each +1/+1 counter on it.').
card_first_print('thought gorger', 'ROE').
card_image_name('thought gorger'/'ROE', 'thought gorger').
card_uid('thought gorger'/'ROE', 'ROE:Thought Gorger:thought gorger').
card_rarity('thought gorger'/'ROE', 'Rare').
card_artist('thought gorger'/'ROE', 'Jason Felix').
card_number('thought gorger'/'ROE', '129').
card_multiverse_id('thought gorger'/'ROE', '193455').

card_in_set('time of heroes', 'ROE').
card_original_type('time of heroes'/'ROE', 'Enchantment').
card_original_text('time of heroes'/'ROE', 'Each creature you control with a level counter on it gets +2/+2.').
card_first_print('time of heroes', 'ROE').
card_image_name('time of heroes'/'ROE', 'time of heroes').
card_uid('time of heroes'/'ROE', 'ROE:Time of Heroes:time of heroes').
card_rarity('time of heroes'/'ROE', 'Uncommon').
card_artist('time of heroes'/'ROE', 'Kekai Kotaki').
card_number('time of heroes'/'ROE', '49').
card_flavor_text('time of heroes'/'ROE', 'No one spoke. There was no need. The threat of the Eldrazi presented a simple choice: lay down your weapons and die for nothing, or hold them fast and die for something.').
card_multiverse_id('time of heroes'/'ROE', '193448').

card_in_set('totem-guide hartebeest', 'ROE').
card_original_type('totem-guide hartebeest'/'ROE', 'Creature — Antelope').
card_original_text('totem-guide hartebeest'/'ROE', 'When Totem-Guide Hartebeest enters the battlefield, you may search your library for an Aura card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('totem-guide hartebeest', 'ROE').
card_image_name('totem-guide hartebeest'/'ROE', 'totem-guide hartebeest').
card_uid('totem-guide hartebeest'/'ROE', 'ROE:Totem-Guide Hartebeest:totem-guide hartebeest').
card_rarity('totem-guide hartebeest'/'ROE', 'Common').
card_artist('totem-guide hartebeest'/'ROE', 'John Avon').
card_number('totem-guide hartebeest'/'ROE', '50').
card_flavor_text('totem-guide hartebeest'/'ROE', 'The kor track hartebeests in hopes of finding magic to help fight the Eldrazi.').
card_multiverse_id('totem-guide hartebeest'/'ROE', '198175').

card_in_set('training grounds', 'ROE').
card_original_type('training grounds'/'ROE', 'Enchantment').
card_original_text('training grounds'/'ROE', 'Activated abilities of creatures you control cost up to {2} less to activate. This effect can\'t reduce the amount of mana an ability costs to activate to less than one mana.').
card_first_print('training grounds', 'ROE').
card_image_name('training grounds'/'ROE', 'training grounds').
card_uid('training grounds'/'ROE', 'ROE:Training Grounds:training grounds').
card_rarity('training grounds'/'ROE', 'Rare').
card_artist('training grounds'/'ROE', 'James Ryman').
card_number('training grounds'/'ROE', '91').
card_flavor_text('training grounds'/'ROE', 'Under the master\'s eye, skills are honed sharper and spells cut deeper.').
card_multiverse_id('training grounds'/'ROE', '193490').

card_in_set('traitorous instinct', 'ROE').
card_original_type('traitorous instinct'/'ROE', 'Sorcery').
card_original_text('traitorous instinct'/'ROE', 'Gain control of target creature until end of turn. Untap that creature. Until end of turn, it gets +2/+0 and gains haste.').
card_first_print('traitorous instinct', 'ROE').
card_image_name('traitorous instinct'/'ROE', 'traitorous instinct').
card_uid('traitorous instinct'/'ROE', 'ROE:Traitorous Instinct:traitorous instinct').
card_rarity('traitorous instinct'/'ROE', 'Uncommon').
card_artist('traitorous instinct'/'ROE', 'Scott Chou').
card_number('traitorous instinct'/'ROE', '168').
card_flavor_text('traitorous instinct'/'ROE', 'The horror lurched about like a puppet from a madman\'s nightmare, squeezing the life from its own kind.').
card_multiverse_id('traitorous instinct'/'ROE', '198177').

card_in_set('transcendent master', 'ROE').
card_original_type('transcendent master'/'ROE', 'Creature — Human Cleric Avatar').
card_original_text('transcendent master'/'ROE', 'Level up {1} ({1}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 6-11\n6/6\nLifelink\nLEVEL 12+\n9/9\nLifelink\nTranscendent Master is indestructible.').
card_first_print('transcendent master', 'ROE').
card_image_name('transcendent master'/'ROE', 'transcendent master').
card_uid('transcendent master'/'ROE', 'ROE:Transcendent Master:transcendent master').
card_rarity('transcendent master'/'ROE', 'Mythic Rare').
card_artist('transcendent master'/'ROE', 'Steven Belledin').
card_number('transcendent master'/'ROE', '51').
card_multiverse_id('transcendent master'/'ROE', '194917').

card_in_set('tuktuk the explorer', 'ROE').
card_original_type('tuktuk the explorer'/'ROE', 'Legendary Creature — Goblin').
card_original_text('tuktuk the explorer'/'ROE', 'Haste\nWhen Tuktuk the Explorer is put into a graveyard from the battlefield, put a legendary 5/5 colorless Goblin Golem artifact creature token named Tuktuk the Returned onto the battlefield.').
card_first_print('tuktuk the explorer', 'ROE').
card_image_name('tuktuk the explorer'/'ROE', 'tuktuk the explorer').
card_uid('tuktuk the explorer'/'ROE', 'ROE:Tuktuk the Explorer:tuktuk the explorer').
card_rarity('tuktuk the explorer'/'ROE', 'Rare').
card_artist('tuktuk the explorer'/'ROE', 'Volkan Baga').
card_number('tuktuk the explorer'/'ROE', '169').
card_flavor_text('tuktuk the explorer'/'ROE', 'Remade by Eldrazi magic, Tuktuk inspired an entire clan of misguided goblins.').
card_multiverse_id('tuktuk the explorer'/'ROE', '194940').

card_in_set('ulamog\'s crusher', 'ROE').
card_original_type('ulamog\'s crusher'/'ROE', 'Creature — Eldrazi').
card_original_text('ulamog\'s crusher'/'ROE', 'Annihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)\nUlamog\'s Crusher attacks each turn if able.').
card_first_print('ulamog\'s crusher', 'ROE').
card_image_name('ulamog\'s crusher'/'ROE', 'ulamog\'s crusher').
card_uid('ulamog\'s crusher'/'ROE', 'ROE:Ulamog\'s Crusher:ulamog\'s crusher').
card_rarity('ulamog\'s crusher'/'ROE', 'Common').
card_artist('ulamog\'s crusher'/'ROE', 'Todd Lockwood').
card_number('ulamog\'s crusher'/'ROE', '13').
card_flavor_text('ulamog\'s crusher'/'ROE', '\"Whatever the Eldrazi\'s purpose is, it has nothing to do with something so insignificant as us.\"\n—Nirthu, lone missionary').
card_multiverse_id('ulamog\'s crusher'/'ROE', '194908').

card_in_set('ulamog, the infinite gyre', 'ROE').
card_original_type('ulamog, the infinite gyre'/'ROE', 'Legendary Creature — Eldrazi').
card_original_text('ulamog, the infinite gyre'/'ROE', 'When you cast Ulamog, the Infinite Gyre, destroy target permanent.\nAnnihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.)\nUlamog is indestructible.\nWhen Ulamog is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_first_print('ulamog, the infinite gyre', 'ROE').
card_image_name('ulamog, the infinite gyre'/'ROE', 'ulamog, the infinite gyre').
card_uid('ulamog, the infinite gyre'/'ROE', 'ROE:Ulamog, the Infinite Gyre:ulamog, the infinite gyre').
card_rarity('ulamog, the infinite gyre'/'ROE', 'Mythic Rare').
card_artist('ulamog, the infinite gyre'/'ROE', 'Aleksi Briclot').
card_number('ulamog, the infinite gyre'/'ROE', '12').
card_multiverse_id('ulamog, the infinite gyre'/'ROE', '194911').

card_in_set('umbra mystic', 'ROE').
card_original_type('umbra mystic'/'ROE', 'Creature — Human Wizard').
card_original_text('umbra mystic'/'ROE', 'Auras attached to permanents you control have totem armor. (If an enchanted permanent you control would be destroyed, instead remove all damage from it and destroy an Aura attached to it.)').
card_first_print('umbra mystic', 'ROE').
card_image_name('umbra mystic'/'ROE', 'umbra mystic').
card_uid('umbra mystic'/'ROE', 'ROE:Umbra Mystic:umbra mystic').
card_rarity('umbra mystic'/'ROE', 'Rare').
card_artist('umbra mystic'/'ROE', 'Matt Stewart').
card_number('umbra mystic'/'ROE', '52').
card_flavor_text('umbra mystic'/'ROE', '\"I have found the courage to face death—twice, if I must.\"').
card_multiverse_id('umbra mystic'/'ROE', '193416').

card_in_set('unified will', 'ROE').
card_original_type('unified will'/'ROE', 'Instant').
card_original_text('unified will'/'ROE', 'Counter target spell if you control more creatures than that spell\'s controller.').
card_first_print('unified will', 'ROE').
card_image_name('unified will'/'ROE', 'unified will').
card_uid('unified will'/'ROE', 'ROE:Unified Will:unified will').
card_rarity('unified will'/'ROE', 'Uncommon').
card_artist('unified will'/'ROE', 'Matt Cavotta').
card_number('unified will'/'ROE', '92').
card_flavor_text('unified will'/'ROE', '\"We\'re glad to be of service—to be an audience to your failure.\"\n—Maizah Shere, Tazeem lullmage').
card_multiverse_id('unified will'/'ROE', '193456').

card_in_set('valakut fireboar', 'ROE').
card_original_type('valakut fireboar'/'ROE', 'Creature — Elemental Boar').
card_original_text('valakut fireboar'/'ROE', 'Whenever Valakut Fireboar attacks, switch its power and toughness until end of turn.').
card_first_print('valakut fireboar', 'ROE').
card_image_name('valakut fireboar'/'ROE', 'valakut fireboar').
card_uid('valakut fireboar'/'ROE', 'ROE:Valakut Fireboar:valakut fireboar').
card_rarity('valakut fireboar'/'ROE', 'Uncommon').
card_artist('valakut fireboar'/'ROE', 'Zoltan Boros & Gabor Szikszai').
card_number('valakut fireboar'/'ROE', '170').
card_flavor_text('valakut fireboar'/'ROE', '\"Just move very slowly, and for the love of Ula, do not get it mad.\"\n—Chadir the Navigator').
card_multiverse_id('valakut fireboar'/'ROE', '193552').

card_in_set('vendetta', 'ROE').
card_original_type('vendetta'/'ROE', 'Instant').
card_original_text('vendetta'/'ROE', 'Destroy target nonblack creature. It can\'t be regenerated. You lose life equal to that creature\'s toughness.').
card_image_name('vendetta'/'ROE', 'vendetta').
card_uid('vendetta'/'ROE', 'ROE:Vendetta:vendetta').
card_rarity('vendetta'/'ROE', 'Common').
card_artist('vendetta'/'ROE', 'Karl Kopinski').
card_number('vendetta'/'ROE', '130').
card_flavor_text('vendetta'/'ROE', '\"Consider my debt to your kin paid in full.\"\n—Idrol, Guul Draz assassin').
card_multiverse_id('vendetta'/'ROE', '194922').

card_in_set('venerated teacher', 'ROE').
card_original_type('venerated teacher'/'ROE', 'Creature — Human Wizard').
card_original_text('venerated teacher'/'ROE', 'When Venerated Teacher enters the battlefield, put two level counters on each creature you control with level up.').
card_first_print('venerated teacher', 'ROE').
card_image_name('venerated teacher'/'ROE', 'venerated teacher').
card_uid('venerated teacher'/'ROE', 'ROE:Venerated Teacher:venerated teacher').
card_rarity('venerated teacher'/'ROE', 'Common').
card_artist('venerated teacher'/'ROE', 'Greg Staples').
card_number('venerated teacher'/'ROE', '93').
card_flavor_text('venerated teacher'/'ROE', '\"The unfathomable is upon us, students. I have no doubt you will rise to the occasion.\"').
card_multiverse_id('venerated teacher'/'ROE', '193443').

card_in_set('vengevine', 'ROE').
card_original_type('vengevine'/'ROE', 'Creature — Elemental').
card_original_text('vengevine'/'ROE', 'Haste\nWhenever you cast a spell, if it\'s the second creature spell you cast this turn, you may return Vengevine from your graveyard to the battlefield.').
card_first_print('vengevine', 'ROE').
card_image_name('vengevine'/'ROE', 'vengevine').
card_uid('vengevine'/'ROE', 'ROE:Vengevine:vengevine').
card_rarity('vengevine'/'ROE', 'Mythic Rare').
card_artist('vengevine'/'ROE', 'Raymond Swanland').
card_number('vengevine'/'ROE', '212').
card_flavor_text('vengevine'/'ROE', 'Leave but a shred of root and it will return, bursting with vigor.').
card_multiverse_id('vengevine'/'ROE', '193556').

card_in_set('vent sentinel', 'ROE').
card_original_type('vent sentinel'/'ROE', 'Creature — Elemental').
card_original_text('vent sentinel'/'ROE', 'Defender\n{1}{R}, {T}: Vent Sentinel deals damage to target player equal to the number of creatures with defender you control.').
card_first_print('vent sentinel', 'ROE').
card_image_name('vent sentinel'/'ROE', 'vent sentinel').
card_uid('vent sentinel'/'ROE', 'ROE:Vent Sentinel:vent sentinel').
card_rarity('vent sentinel'/'ROE', 'Common').
card_artist('vent sentinel'/'ROE', 'Chris Rahn').
card_number('vent sentinel'/'ROE', '171').
card_flavor_text('vent sentinel'/'ROE', '\"Zendikar fortifies itself against the Great Threat, making sentries of sky, sea, and stone.\"\n—The War Diaries').
card_multiverse_id('vent sentinel'/'ROE', '193480').

card_in_set('virulent swipe', 'ROE').
card_original_type('virulent swipe'/'ROE', 'Instant').
card_original_text('virulent swipe'/'ROE', 'Target creature gets +2/+0 and gains deathtouch until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('virulent swipe', 'ROE').
card_image_name('virulent swipe'/'ROE', 'virulent swipe').
card_uid('virulent swipe'/'ROE', 'ROE:Virulent Swipe:virulent swipe').
card_rarity('virulent swipe'/'ROE', 'Uncommon').
card_artist('virulent swipe'/'ROE', 'Raymond Swanland').
card_number('virulent swipe'/'ROE', '131').
card_multiverse_id('virulent swipe'/'ROE', '198298').

card_in_set('wall of omens', 'ROE').
card_original_type('wall of omens'/'ROE', 'Creature — Wall').
card_original_text('wall of omens'/'ROE', 'Defender\nWhen Wall of Omens enters the battlefield, draw a card.').
card_image_name('wall of omens'/'ROE', 'wall of omens').
card_uid('wall of omens'/'ROE', 'ROE:Wall of Omens:wall of omens').
card_rarity('wall of omens'/'ROE', 'Uncommon').
card_artist('wall of omens'/'ROE', 'James Paick').
card_number('wall of omens'/'ROE', '53').
card_flavor_text('wall of omens'/'ROE', '\"I search for a vision of Zendikar that does not include the Eldrazi.\"\n—Expedition journal entry').
card_multiverse_id('wall of omens'/'ROE', '193545').

card_in_set('warmonger\'s chariot', 'ROE').
card_original_type('warmonger\'s chariot'/'ROE', 'Artifact — Equipment').
card_original_text('warmonger\'s chariot'/'ROE', 'Equipped creature gets +2/+2.\nAs long as equipped creature has defender, it can attack as though it didn\'t have defender.\nEquip {3}').
card_first_print('warmonger\'s chariot', 'ROE').
card_image_name('warmonger\'s chariot'/'ROE', 'warmonger\'s chariot').
card_uid('warmonger\'s chariot'/'ROE', 'ROE:Warmonger\'s Chariot:warmonger\'s chariot').
card_rarity('warmonger\'s chariot'/'ROE', 'Uncommon').
card_artist('warmonger\'s chariot'/'ROE', 'Warren Mahy').
card_number('warmonger\'s chariot'/'ROE', '226').
card_flavor_text('warmonger\'s chariot'/'ROE', 'The best defense is a good offense.').
card_multiverse_id('warmonger\'s chariot'/'ROE', '194916').

card_in_set('wildheart invoker', 'ROE').
card_original_type('wildheart invoker'/'ROE', 'Creature — Elf Shaman').
card_original_text('wildheart invoker'/'ROE', '{8}: Target creature gets +5/+5 and gains trample until end of turn.').
card_first_print('wildheart invoker', 'ROE').
card_image_name('wildheart invoker'/'ROE', 'wildheart invoker').
card_uid('wildheart invoker'/'ROE', 'ROE:Wildheart Invoker:wildheart invoker').
card_rarity('wildheart invoker'/'ROE', 'Common').
card_artist('wildheart invoker'/'ROE', 'Erica Yang').
card_number('wildheart invoker'/'ROE', '213').
card_flavor_text('wildheart invoker'/'ROE', '\"Life as we know it dangles on the brink of extinction. We must show the strength they would steal from us.\"\n—The Invokers\' Tales').
card_multiverse_id('wildheart invoker'/'ROE', '193414').

card_in_set('world at war', 'ROE').
card_original_type('world at war'/'ROE', 'Sorcery').
card_original_text('world at war'/'ROE', 'After the first postcombat main phase this turn, there\'s an additional combat phase followed by an additional main phase. At the beginning of that combat, untap all creatures that attacked this turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('world at war', 'ROE').
card_image_name('world at war'/'ROE', 'world at war').
card_uid('world at war'/'ROE', 'ROE:World at War:world at war').
card_rarity('world at war'/'ROE', 'Rare').
card_artist('world at war'/'ROE', 'Igor Kieryluk').
card_number('world at war'/'ROE', '172').
card_multiverse_id('world at war'/'ROE', '193484').

card_in_set('wrap in flames', 'ROE').
card_original_type('wrap in flames'/'ROE', 'Sorcery').
card_original_text('wrap in flames'/'ROE', 'Wrap in Flames deals 1 damage to each of up to three target creatures. Those creatures can\'t block this turn.').
card_first_print('wrap in flames', 'ROE').
card_image_name('wrap in flames'/'ROE', 'wrap in flames').
card_uid('wrap in flames'/'ROE', 'ROE:Wrap in Flames:wrap in flames').
card_rarity('wrap in flames'/'ROE', 'Common').
card_artist('wrap in flames'/'ROE', 'Véronique Meignaud').
card_number('wrap in flames'/'ROE', '173').
card_flavor_text('wrap in flames'/'ROE', '\"Our pyromancer\'s flames may not have killed them, but they bought us the time we needed.\"\n—The War Diaries').
card_multiverse_id('wrap in flames'/'ROE', '193563').

card_in_set('zof shade', 'ROE').
card_original_type('zof shade'/'ROE', 'Creature — Shade').
card_original_text('zof shade'/'ROE', '{2}{B}: Zof Shade gets +2/+2 until end of turn.').
card_first_print('zof shade', 'ROE').
card_image_name('zof shade'/'ROE', 'zof shade').
card_uid('zof shade'/'ROE', 'ROE:Zof Shade:zof shade').
card_rarity('zof shade'/'ROE', 'Common').
card_artist('zof shade'/'ROE', 'Jason A. Engle').
card_number('zof shade'/'ROE', '132').
card_flavor_text('zof shade'/'ROE', 'It haunts the remnants of the Helix of Zof, leeching strength from the vast Eldrazi ruin.').
card_multiverse_id('zof shade'/'ROE', '193543').

card_in_set('zulaport enforcer', 'ROE').
card_original_type('zulaport enforcer'/'ROE', 'Creature — Human Warrior').
card_original_text('zulaport enforcer'/'ROE', 'Level up {4} ({4}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n3/3\nLEVEL 3+\n5/5\nZulaport Enforcer can\'t be blocked except by black creatures.').
card_first_print('zulaport enforcer', 'ROE').
card_image_name('zulaport enforcer'/'ROE', 'zulaport enforcer').
card_uid('zulaport enforcer'/'ROE', 'ROE:Zulaport Enforcer:zulaport enforcer').
card_rarity('zulaport enforcer'/'ROE', 'Common').
card_artist('zulaport enforcer'/'ROE', 'Matt Stewart').
card_number('zulaport enforcer'/'ROE', '133').
card_multiverse_id('zulaport enforcer'/'ROE', '198166').
