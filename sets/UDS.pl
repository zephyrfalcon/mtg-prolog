% Urza's Destiny

set('UDS').
set_name('UDS', 'Urza\'s Destiny').
set_release_date('UDS', '1999-06-07').
set_border('UDS', 'black').
set_type('UDS', 'expansion').
set_block('UDS', 'Urza\'s').

card_in_set('academy rector', 'UDS').
card_original_type('academy rector'/'UDS', 'Creature — Cleric').
card_original_text('academy rector'/'UDS', 'When Academy Rector is put into a graveyard from play, you may remove Academy Rector from the game. If you do, search your library for an enchantment card and put that card into play. Then shuffle your library.').
card_first_print('academy rector', 'UDS').
card_image_name('academy rector'/'UDS', 'academy rector').
card_uid('academy rector'/'UDS', 'UDS:Academy Rector:academy rector').
card_rarity('academy rector'/'UDS', 'Rare').
card_artist('academy rector'/'UDS', 'Heather Hudson').
card_number('academy rector'/'UDS', '1').
card_multiverse_id('academy rector'/'UDS', '15138').

card_in_set('æther sting', 'UDS').
card_original_type('æther sting'/'UDS', 'Enchantment').
card_original_text('æther sting'/'UDS', 'Whenever one of your opponents plays a creature spell, Æther Sting deals 1 damage to that player.').
card_first_print('æther sting', 'UDS').
card_image_name('æther sting'/'UDS', 'aether sting').
card_uid('æther sting'/'UDS', 'UDS:Æther Sting:aether sting').
card_rarity('æther sting'/'UDS', 'Uncommon').
card_artist('æther sting'/'UDS', 'Pete Venters').
card_number('æther sting'/'UDS', '76').
card_flavor_text('æther sting'/'UDS', 'When Gatha fell, he was called to account for each abomination he created.').
card_multiverse_id('æther sting'/'UDS', '12612').

card_in_set('ancient silverback', 'UDS').
card_original_type('ancient silverback'/'UDS', 'Creature — Ape').
card_original_text('ancient silverback'/'UDS', '{G} Regenerate Ancient Silverback.').
card_first_print('ancient silverback', 'UDS').
card_image_name('ancient silverback'/'UDS', 'ancient silverback').
card_uid('ancient silverback'/'UDS', 'UDS:Ancient Silverback:ancient silverback').
card_rarity('ancient silverback'/'UDS', 'Rare').
card_artist('ancient silverback'/'UDS', 'Paolo Parente').
card_number('ancient silverback'/'UDS', '101').
card_flavor_text('ancient silverback'/'UDS', 'The Phyrexian killing machines couldn\'t have known the seriousness of their mistake in wounding the ape—they\'d never seen it angry.').
card_multiverse_id('ancient silverback'/'UDS', '16424').

card_in_set('apprentice necromancer', 'UDS').
card_original_type('apprentice necromancer'/'UDS', 'Creature — Wizard').
card_original_text('apprentice necromancer'/'UDS', '{B}, {T}, Sacrifice Apprentice Necromancer: Return target creature card from your graveyard to play. That creature gains haste. At end of turn, sacrifice it. (The creature may attack and {T} the turn it comes under your control.)').
card_first_print('apprentice necromancer', 'UDS').
card_image_name('apprentice necromancer'/'UDS', 'apprentice necromancer').
card_uid('apprentice necromancer'/'UDS', 'UDS:Apprentice Necromancer:apprentice necromancer').
card_rarity('apprentice necromancer'/'UDS', 'Rare').
card_artist('apprentice necromancer'/'UDS', 'Pete Venters').
card_number('apprentice necromancer'/'UDS', '51').
card_multiverse_id('apprentice necromancer'/'UDS', '14442').

card_in_set('archery training', 'UDS').
card_original_type('archery training'/'UDS', 'Enchant Creature').
card_original_text('archery training'/'UDS', 'At the beginning of your upkeep, you may put an arrow counter on Archery Training.\nEnchanted creature gains \"{T}: This creature deals X damage to target attacking or blocking creature, where X is the number of arrow counters on the Archery Training enchanting this creature.\"').
card_first_print('archery training', 'UDS').
card_image_name('archery training'/'UDS', 'archery training').
card_uid('archery training'/'UDS', 'UDS:Archery Training:archery training').
card_rarity('archery training'/'UDS', 'Uncommon').
card_artist('archery training'/'UDS', 'Mark Brill').
card_number('archery training'/'UDS', '2').
card_multiverse_id('archery training'/'UDS', '15135').

card_in_set('attrition', 'UDS').
card_original_type('attrition'/'UDS', 'Enchantment').
card_original_text('attrition'/'UDS', '{B}, Sacrifice a creature: Destroy target nonblack creature.').
card_first_print('attrition', 'UDS').
card_image_name('attrition'/'UDS', 'attrition').
card_uid('attrition'/'UDS', 'UDS:Attrition:attrition').
card_rarity('attrition'/'UDS', 'Rare').
card_artist('attrition'/'UDS', 'Scott M. Fischer').
card_number('attrition'/'UDS', '52').
card_flavor_text('attrition'/'UDS', '\"I will trade life for life with the insurgents. Our resources, unlike theirs, are limitless.\"\n—Davvol, Evincar of Rath').
card_multiverse_id('attrition'/'UDS', '15191').

card_in_set('aura thief', 'UDS').
card_original_type('aura thief'/'UDS', 'Creature — Illusion').
card_original_text('aura thief'/'UDS', 'Flying\nWhen Aura Thief is put into a graveyard from play, you gain control of all enchantments. (You don\'t get to move local enchantments.)').
card_first_print('aura thief', 'UDS').
card_image_name('aura thief'/'UDS', 'aura thief').
card_uid('aura thief'/'UDS', 'UDS:Aura Thief:aura thief').
card_rarity('aura thief'/'UDS', 'Rare').
card_artist('aura thief'/'UDS', 'Ron Spears').
card_number('aura thief'/'UDS', '26').
card_flavor_text('aura thief'/'UDS', 'Illusion steals reality from the unwise.').
card_multiverse_id('aura thief'/'UDS', '15261').

card_in_set('blizzard elemental', 'UDS').
card_original_type('blizzard elemental'/'UDS', 'Creature — Elemental').
card_original_text('blizzard elemental'/'UDS', 'Flying\n{3}{U} Untap Blizzard Elemental.').
card_first_print('blizzard elemental', 'UDS').
card_image_name('blizzard elemental'/'UDS', 'blizzard elemental').
card_uid('blizzard elemental'/'UDS', 'UDS:Blizzard Elemental:blizzard elemental').
card_rarity('blizzard elemental'/'UDS', 'Rare').
card_artist('blizzard elemental'/'UDS', 'Thomas M. Baxa').
card_number('blizzard elemental'/'UDS', '27').
card_flavor_text('blizzard elemental'/'UDS', 'Students who had seen Rayne argue with Urza were certain she had summoned it to make him seem warmer by comparison.').
card_multiverse_id('blizzard elemental'/'UDS', '15146').

card_in_set('bloodshot cyclops', 'UDS').
card_original_type('bloodshot cyclops'/'UDS', 'Creature — Giant').
card_original_text('bloodshot cyclops'/'UDS', '{T}, Sacrifice a creature: Bloodshot Cyclops deals X damage to target creature or player, where X is the sacrificed creature\'s power.').
card_first_print('bloodshot cyclops', 'UDS').
card_image_name('bloodshot cyclops'/'UDS', 'bloodshot cyclops').
card_uid('bloodshot cyclops'/'UDS', 'UDS:Bloodshot Cyclops:bloodshot cyclops').
card_rarity('bloodshot cyclops'/'UDS', 'Rare').
card_artist('bloodshot cyclops'/'UDS', 'Ray Lago').
card_number('bloodshot cyclops'/'UDS', '77').
card_flavor_text('bloodshot cyclops'/'UDS', 'After their first encounter, the goblins named him Chuck.').
card_multiverse_id('bloodshot cyclops'/'UDS', '15214').

card_in_set('body snatcher', 'UDS').
card_original_type('body snatcher'/'UDS', 'Creature — Minion').
card_original_text('body snatcher'/'UDS', 'When Body Snatcher comes into play, you may choose and discard a creature card from your hand. If you don\'t, remove Body Snatcher from the game.\nWhen Body Snatcher is put into a graveyard from play, remove Body Snatcher from the game and return target creature card from your graveyard to play.').
card_first_print('body snatcher', 'UDS').
card_image_name('body snatcher'/'UDS', 'body snatcher').
card_uid('body snatcher'/'UDS', 'UDS:Body Snatcher:body snatcher').
card_rarity('body snatcher'/'UDS', 'Rare').
card_artist('body snatcher'/'UDS', 'Mark Zug').
card_number('body snatcher'/'UDS', '53').
card_multiverse_id('body snatcher'/'UDS', '15189').

card_in_set('braidwood cup', 'UDS').
card_original_type('braidwood cup'/'UDS', 'Artifact').
card_original_text('braidwood cup'/'UDS', '{T}: You gain 1 life.').
card_first_print('braidwood cup', 'UDS').
card_image_name('braidwood cup'/'UDS', 'braidwood cup').
card_uid('braidwood cup'/'UDS', 'UDS:Braidwood Cup:braidwood cup').
card_rarity('braidwood cup'/'UDS', 'Uncommon').
card_artist('braidwood cup'/'UDS', 'Greg & Tim Hildebrandt').
card_number('braidwood cup'/'UDS', '126').
card_flavor_text('braidwood cup'/'UDS', '\"I think it no accident that every civilized people has discovered the art of distillation.\"\n—Rofellos of Llanowar').
card_multiverse_id('braidwood cup'/'UDS', '15249').

card_in_set('braidwood sextant', 'UDS').
card_original_type('braidwood sextant'/'UDS', 'Artifact').
card_original_text('braidwood sextant'/'UDS', '{2}, {T}, Sacrifice Braidwood Sextant: Search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.').
card_first_print('braidwood sextant', 'UDS').
card_image_name('braidwood sextant'/'UDS', 'braidwood sextant').
card_uid('braidwood sextant'/'UDS', 'UDS:Braidwood Sextant:braidwood sextant').
card_rarity('braidwood sextant'/'UDS', 'Uncommon').
card_artist('braidwood sextant'/'UDS', 'Hazeltine').
card_number('braidwood sextant'/'UDS', '127').
card_multiverse_id('braidwood sextant'/'UDS', '15251').

card_in_set('brass secretary', 'UDS').
card_original_type('brass secretary'/'UDS', 'Artifact Creature').
card_original_text('brass secretary'/'UDS', '{2}, Sacrifice Brass Secretary: Draw a card.').
card_first_print('brass secretary', 'UDS').
card_image_name('brass secretary'/'UDS', 'brass secretary').
card_uid('brass secretary'/'UDS', 'UDS:Brass Secretary:brass secretary').
card_rarity('brass secretary'/'UDS', 'Uncommon').
card_artist('brass secretary'/'UDS', 'DiTerlizzi').
card_number('brass secretary'/'UDS', '128').
card_flavor_text('brass secretary'/'UDS', 'The students disliked the secretary for its unfailing memory—until they discovered it couldn\'t dodge thrown objects.').
card_multiverse_id('brass secretary'/'UDS', '15245').

card_in_set('brine seer', 'UDS').
card_original_type('brine seer'/'UDS', 'Creature — Wizard').
card_original_text('brine seer'/'UDS', '{2}{U}, {T}: Reveal any number of blue cards in your hand. Counter target spell unless its controller pays {1} for each card revealed this way.').
card_first_print('brine seer', 'UDS').
card_image_name('brine seer'/'UDS', 'brine seer').
card_uid('brine seer'/'UDS', 'UDS:Brine Seer:brine seer').
card_rarity('brine seer'/'UDS', 'Uncommon').
card_artist('brine seer'/'UDS', 'Donato Giancola').
card_number('brine seer'/'UDS', '28').
card_multiverse_id('brine seer'/'UDS', '15148').

card_in_set('bubbling beebles', 'UDS').
card_original_type('bubbling beebles'/'UDS', 'Creature — Beeble').
card_original_text('bubbling beebles'/'UDS', 'Bubbling Beebles is unblockable as long as defending player controls an enchantment.').
card_first_print('bubbling beebles', 'UDS').
card_image_name('bubbling beebles'/'UDS', 'bubbling beebles').
card_uid('bubbling beebles'/'UDS', 'UDS:Bubbling Beebles:bubbling beebles').
card_rarity('bubbling beebles'/'UDS', 'Common').
card_artist('bubbling beebles'/'UDS', 'Jeff Miracola').
card_number('bubbling beebles'/'UDS', '29').
card_flavor_text('bubbling beebles'/'UDS', '\"Chancellor Rayne canceled the annual beeble roast. I should have married a crueler woman.\"\n—Barrin').
card_multiverse_id('bubbling beebles'/'UDS', '15783').

card_in_set('bubbling muck', 'UDS').
card_original_type('bubbling muck'/'UDS', 'Sorcery').
card_original_text('bubbling muck'/'UDS', 'Until end of turn, whenever a player taps a swamp for mana, it produces an additional {B}.').
card_first_print('bubbling muck', 'UDS').
card_image_name('bubbling muck'/'UDS', 'bubbling muck').
card_uid('bubbling muck'/'UDS', 'UDS:Bubbling Muck:bubbling muck').
card_rarity('bubbling muck'/'UDS', 'Common').
card_artist('bubbling muck'/'UDS', 'Greg & Tim Hildebrandt').
card_number('bubbling muck'/'UDS', '54').
card_flavor_text('bubbling muck'/'UDS', 'The muck claims a hundred living things for each meager treasure it spews forth.').
card_multiverse_id('bubbling muck'/'UDS', '15179').

card_in_set('caltrops', 'UDS').
card_original_type('caltrops'/'UDS', 'Artifact').
card_original_text('caltrops'/'UDS', 'Whenever a creature attacks, Caltrops deals 1 damage to it.').
card_first_print('caltrops', 'UDS').
card_image_name('caltrops'/'UDS', 'caltrops').
card_uid('caltrops'/'UDS', 'UDS:Caltrops:caltrops').
card_rarity('caltrops'/'UDS', 'Uncommon').
card_artist('caltrops'/'UDS', 'Jeff Laubenstein').
card_number('caltrops'/'UDS', '129').
card_flavor_text('caltrops'/'UDS', 'Toward the end, Gatha struggled to keep his experiments in and his patrons out.').
card_multiverse_id('caltrops'/'UDS', '15252').

card_in_set('capashen knight', 'UDS').
card_original_type('capashen knight'/'UDS', 'Creature — Knight').
card_original_text('capashen knight'/'UDS', 'First strike\n{1}{W} Capashen Knight gets +1/+0 until end of turn.').
card_first_print('capashen knight', 'UDS').
card_image_name('capashen knight'/'UDS', 'capashen knight').
card_uid('capashen knight'/'UDS', 'UDS:Capashen Knight:capashen knight').
card_rarity('capashen knight'/'UDS', 'Common').
card_artist('capashen knight'/'UDS', 'Kev Walker').
card_number('capashen knight'/'UDS', '3').
card_flavor_text('capashen knight'/'UDS', 'Few warriors dare to challenge a knight of Capashen. Should one do so, there is one fewer.').
card_multiverse_id('capashen knight'/'UDS', '15125').

card_in_set('capashen standard', 'UDS').
card_original_type('capashen standard'/'UDS', 'Enchant Creature').
card_original_text('capashen standard'/'UDS', 'Enchanted creature gets +1/+1.\n{2}, Sacrifice Capashen Standard: Draw a card.').
card_first_print('capashen standard', 'UDS').
card_image_name('capashen standard'/'UDS', 'capashen standard').
card_uid('capashen standard'/'UDS', 'UDS:Capashen Standard:capashen standard').
card_rarity('capashen standard'/'UDS', 'Common').
card_artist('capashen standard'/'UDS', 'Todd Lockwood').
card_number('capashen standard'/'UDS', '4').
card_flavor_text('capashen standard'/'UDS', 'Benalia has no need for peacocks to serve as symbols of vanity. The Capashens strut more proudly than any bird.').
card_multiverse_id('capashen standard'/'UDS', '19095').

card_in_set('capashen templar', 'UDS').
card_original_type('capashen templar'/'UDS', 'Creature — Knight').
card_original_text('capashen templar'/'UDS', '{W} Capashen Templar gets +0/+1 until end of turn.').
card_first_print('capashen templar', 'UDS').
card_image_name('capashen templar'/'UDS', 'capashen templar').
card_uid('capashen templar'/'UDS', 'UDS:Capashen Templar:capashen templar').
card_rarity('capashen templar'/'UDS', 'Common').
card_artist('capashen templar'/'UDS', 'Todd Lockwood').
card_number('capashen templar'/'UDS', '5').
card_flavor_text('capashen templar'/'UDS', 'Their shields are Benalia\'s outermost battlements.').
card_multiverse_id('capashen templar'/'UDS', '15762').

card_in_set('carnival of souls', 'UDS').
card_original_type('carnival of souls'/'UDS', 'Enchantment').
card_original_text('carnival of souls'/'UDS', 'Whenever a creature comes into play, you lose 1 life and add {B} to your mana pool.').
card_first_print('carnival of souls', 'UDS').
card_image_name('carnival of souls'/'UDS', 'carnival of souls').
card_uid('carnival of souls'/'UDS', 'UDS:Carnival of Souls:carnival of souls').
card_rarity('carnival of souls'/'UDS', 'Rare').
card_artist('carnival of souls'/'UDS', 'Brian Snõddy').
card_number('carnival of souls'/'UDS', '55').
card_flavor_text('carnival of souls'/'UDS', '\"‘Davvol, blast those elves.\' ‘Davvol, transport those troops.\' No one cares that today is my birthday.\"').
card_multiverse_id('carnival of souls'/'UDS', '19115').

card_in_set('chime of night', 'UDS').
card_original_type('chime of night'/'UDS', 'Enchant Creature').
card_original_text('chime of night'/'UDS', 'When Chime of Night is put into a graveyard from play, destroy target nonblack creature.').
card_first_print('chime of night', 'UDS').
card_image_name('chime of night'/'UDS', 'chime of night').
card_uid('chime of night'/'UDS', 'UDS:Chime of Night:chime of night').
card_rarity('chime of night'/'UDS', 'Common').
card_artist('chime of night'/'UDS', 'Pete Venters').
card_number('chime of night'/'UDS', '56').
card_flavor_text('chime of night'/'UDS', 'Many sent to serve Davvol carried such instruments, as if to remind him who their true masters were.').
card_multiverse_id('chime of night'/'UDS', '15768').

card_in_set('cinder seer', 'UDS').
card_original_type('cinder seer'/'UDS', 'Creature — Wizard').
card_original_text('cinder seer'/'UDS', '{2}{R}, {T}: Reveal any number of red cards in your hand. Cinder Seer deals X damage to target creature or player, where X is the number of cards revealed this way.').
card_first_print('cinder seer', 'UDS').
card_image_name('cinder seer'/'UDS', 'cinder seer').
card_uid('cinder seer'/'UDS', 'UDS:Cinder Seer:cinder seer').
card_rarity('cinder seer'/'UDS', 'Uncommon').
card_artist('cinder seer'/'UDS', 'Donato Giancola').
card_number('cinder seer'/'UDS', '78').
card_multiverse_id('cinder seer'/'UDS', '15199').

card_in_set('colos yearling', 'UDS').
card_original_type('colos yearling'/'UDS', 'Creature — Beast').
card_original_text('colos yearling'/'UDS', 'Mountainwalk (This creature is unblockable as long as defending player controls a mountain.)\n{R} Colos Yearling gets +1/+0 until end of turn.').
card_first_print('colos yearling', 'UDS').
card_image_name('colos yearling'/'UDS', 'colos yearling').
card_uid('colos yearling'/'UDS', 'UDS:Colos Yearling:colos yearling').
card_rarity('colos yearling'/'UDS', 'Common').
card_artist('colos yearling'/'UDS', 'Patrick Ho').
card_number('colos yearling'/'UDS', '79').
card_flavor_text('colos yearling'/'UDS', 'A steed grows with its rider.').
card_multiverse_id('colos yearling'/'UDS', '15195').

card_in_set('compost', 'UDS').
card_original_type('compost'/'UDS', 'Enchantment').
card_original_text('compost'/'UDS', 'Whenever a black card is put into one of your opponents\' graveyards, you may draw a card.').
card_first_print('compost', 'UDS').
card_image_name('compost'/'UDS', 'compost').
card_uid('compost'/'UDS', 'UDS:Compost:compost').
card_rarity('compost'/'UDS', 'Uncommon').
card_artist('compost'/'UDS', 'Douglas Shuler').
card_number('compost'/'UDS', '102').
card_flavor_text('compost'/'UDS', 'A fruit must rot before its seed can sprout.\n—Yavimaya saying').
card_multiverse_id('compost'/'UDS', '15878').

card_in_set('covetous dragon', 'UDS').
card_original_type('covetous dragon'/'UDS', 'Creature — Dragon').
card_original_text('covetous dragon'/'UDS', 'Flying\nWhen you control no artifacts, sacrifice Covetous Dragon.').
card_first_print('covetous dragon', 'UDS').
card_image_name('covetous dragon'/'UDS', 'covetous dragon').
card_uid('covetous dragon'/'UDS', 'UDS:Covetous Dragon:covetous dragon').
card_rarity('covetous dragon'/'UDS', 'Rare').
card_artist('covetous dragon'/'UDS', 'rk post').
card_number('covetous dragon'/'UDS', '80').
card_flavor_text('covetous dragon'/'UDS', 'Gatha survived as long as he did by giving all Keld\'s predators exactly what they wanted.').
card_multiverse_id('covetous dragon'/'UDS', '15770').

card_in_set('disappear', 'UDS').
card_original_type('disappear'/'UDS', 'Enchant Creature').
card_original_text('disappear'/'UDS', '{U} Return enchanted creature and Disappear to their owners\' hands.').
card_first_print('disappear', 'UDS').
card_image_name('disappear'/'UDS', 'disappear').
card_uid('disappear'/'UDS', 'UDS:Disappear:disappear').
card_rarity('disappear'/'UDS', 'Uncommon').
card_artist('disappear'/'UDS', 'Edward P. Beard, Jr.').
card_number('disappear'/'UDS', '30').
card_flavor_text('disappear'/'UDS', 'If at first you don\'t succeed, run away and hide.').
card_multiverse_id('disappear'/'UDS', '15151').

card_in_set('disease carriers', 'UDS').
card_original_type('disease carriers'/'UDS', 'Creature — Rat').
card_original_text('disease carriers'/'UDS', 'When Disease Carriers is put into a graveyard from play, target creature gets -2/-2 until end of turn.').
card_first_print('disease carriers', 'UDS').
card_image_name('disease carriers'/'UDS', 'disease carriers').
card_uid('disease carriers'/'UDS', 'UDS:Disease Carriers:disease carriers').
card_rarity('disease carriers'/'UDS', 'Common').
card_artist('disease carriers'/'UDS', 'Chippy & Matthew Wilson').
card_number('disease carriers'/'UDS', '57').
card_flavor_text('disease carriers'/'UDS', 'Rath is a disease all its inhabitants carry.').
card_multiverse_id('disease carriers'/'UDS', '15169').

card_in_set('donate', 'UDS').
card_original_type('donate'/'UDS', 'Sorcery').
card_original_text('donate'/'UDS', 'Target player gains control of target permanent you control.').
card_first_print('donate', 'UDS').
card_image_name('donate'/'UDS', 'donate').
card_uid('donate'/'UDS', 'UDS:Donate:donate').
card_rarity('donate'/'UDS', 'Rare').
card_artist('donate'/'UDS', 'Jeff Miracola').
card_number('donate'/'UDS', '31').
card_flavor_text('donate'/'UDS', 'Campus pranksters initiate new students with the old \"beeble bomb\" routine.').
card_multiverse_id('donate'/'UDS', '15168').

card_in_set('dying wail', 'UDS').
card_original_type('dying wail'/'UDS', 'Enchant Creature').
card_original_text('dying wail'/'UDS', 'When enchanted creature is put into a graveyard from play, target player chooses and discards two cards from his or her hand.').
card_first_print('dying wail', 'UDS').
card_image_name('dying wail'/'UDS', 'dying wail').
card_uid('dying wail'/'UDS', 'UDS:Dying Wail:dying wail').
card_rarity('dying wail'/'UDS', 'Common').
card_artist('dying wail'/'UDS', 'Brian Snõddy').
card_number('dying wail'/'UDS', '58').
card_flavor_text('dying wail'/'UDS', 'This is a world of spiteful wasps that sting and kill even as they die.').
card_multiverse_id('dying wail'/'UDS', '15183').

card_in_set('elvish lookout', 'UDS').
card_original_type('elvish lookout'/'UDS', 'Creature — Elf').
card_original_text('elvish lookout'/'UDS', 'Elvish Lookout can\'t be the target of spells or abilities.').
card_first_print('elvish lookout', 'UDS').
card_image_name('elvish lookout'/'UDS', 'elvish lookout').
card_uid('elvish lookout'/'UDS', 'UDS:Elvish Lookout:elvish lookout').
card_rarity('elvish lookout'/'UDS', 'Common').
card_artist('elvish lookout'/'UDS', 'Greg & Tim Hildebrandt').
card_number('elvish lookout'/'UDS', '103').
card_flavor_text('elvish lookout'/'UDS', 'Like a masterful quilter, Yavimaya stitches together patches of color to warn its denizens of threats.').
card_multiverse_id('elvish lookout'/'UDS', '19109').

card_in_set('elvish piper', 'UDS').
card_original_type('elvish piper'/'UDS', 'Creature — Elf').
card_original_text('elvish piper'/'UDS', '{G}, {T}: Put a creature card from your hand into play.').
card_first_print('elvish piper', 'UDS').
card_image_name('elvish piper'/'UDS', 'elvish piper').
card_uid('elvish piper'/'UDS', 'UDS:Elvish Piper:elvish piper').
card_rarity('elvish piper'/'UDS', 'Rare').
card_artist('elvish piper'/'UDS', 'Scott M. Fischer').
card_number('elvish piper'/'UDS', '104').
card_flavor_text('elvish piper'/'UDS', 'From Gaea grew the world, and the world was silent. From Gaea grew the world\'s elves, and the world was silent no more.\n—Elvish teaching').
card_multiverse_id('elvish piper'/'UDS', '15241').

card_in_set('emperor crocodile', 'UDS').
card_original_type('emperor crocodile'/'UDS', 'Creature — Crocodile').
card_original_text('emperor crocodile'/'UDS', 'When you control no other creatures, sacrifice Emperor Crocodile.').
card_first_print('emperor crocodile', 'UDS').
card_image_name('emperor crocodile'/'UDS', 'emperor crocodile').
card_uid('emperor crocodile'/'UDS', 'UDS:Emperor Crocodile:emperor crocodile').
card_rarity('emperor crocodile'/'UDS', 'Rare').
card_artist('emperor crocodile'/'UDS', 'Kev Walker').
card_number('emperor crocodile'/'UDS', '105').
card_flavor_text('emperor crocodile'/'UDS', 'The king of Yavimaya\'s waters pays constant attention to his subjects . . . and thrives on their adulation.').
card_multiverse_id('emperor crocodile'/'UDS', '15773').

card_in_set('encroach', 'UDS').
card_original_type('encroach'/'UDS', 'Sorcery').
card_original_text('encroach'/'UDS', 'Look at target player\'s hand and choose a nonbasic land card from it. That player discards that card.').
card_first_print('encroach', 'UDS').
card_image_name('encroach'/'UDS', 'encroach').
card_uid('encroach'/'UDS', 'UDS:Encroach:encroach').
card_rarity('encroach'/'UDS', 'Uncommon').
card_artist('encroach'/'UDS', 'rk post').
card_number('encroach'/'UDS', '59').
card_flavor_text('encroach'/'UDS', '\"The rate of spread in this region is unacceptable. Start again.\"\n—Davvol, Evincar of Rath').
card_multiverse_id('encroach'/'UDS', '15175').

card_in_set('eradicate', 'UDS').
card_original_type('eradicate'/'UDS', 'Sorcery').
card_original_text('eradicate'/'UDS', 'Remove target nonblack creature from the game. Search its controller\'s graveyard, hand, and library for all copies of that card and remove them from the game. That player then shuffles his or her library.').
card_first_print('eradicate', 'UDS').
card_image_name('eradicate'/'UDS', 'eradicate').
card_uid('eradicate'/'UDS', 'UDS:Eradicate:eradicate').
card_rarity('eradicate'/'UDS', 'Uncommon').
card_artist('eradicate'/'UDS', 'Kev Walker').
card_number('eradicate'/'UDS', '60').
card_multiverse_id('eradicate'/'UDS', '15186').

card_in_set('extruder', 'UDS').
card_original_type('extruder'/'UDS', 'Artifact Creature').
card_original_text('extruder'/'UDS', 'Echo\nSacrifice an artifact: Put a +1/+1 counter on target creature.').
card_first_print('extruder', 'UDS').
card_image_name('extruder'/'UDS', 'extruder').
card_uid('extruder'/'UDS', 'UDS:Extruder:extruder').
card_rarity('extruder'/'UDS', 'Uncommon').
card_artist('extruder'/'UDS', 'Mark Tedin').
card_number('extruder'/'UDS', '130').
card_flavor_text('extruder'/'UDS', 'As the invasion drew closer, Urza\'s means began to resemble Phyrexia\'s end.').
card_multiverse_id('extruder'/'UDS', '15255').

card_in_set('false prophet', 'UDS').
card_original_type('false prophet'/'UDS', 'Creature — Cleric').
card_original_text('false prophet'/'UDS', 'When False Prophet is put into a graveyard from play, remove all creatures from the game.').
card_image_name('false prophet'/'UDS', 'false prophet').
card_uid('false prophet'/'UDS', 'UDS:False Prophet:false prophet').
card_rarity('false prophet'/'UDS', 'Rare').
card_artist('false prophet'/'UDS', 'Eric Peterson').
card_number('false prophet'/'UDS', '6').
card_flavor_text('false prophet'/'UDS', '\"You lived for Serra\'s love. Will you not die for it?\"').
card_multiverse_id('false prophet'/'UDS', '15765').

card_in_set('fatigue', 'UDS').
card_original_type('fatigue'/'UDS', 'Sorcery').
card_original_text('fatigue'/'UDS', 'Target player skips his or her next draw step.').
card_first_print('fatigue', 'UDS').
card_image_name('fatigue'/'UDS', 'fatigue').
card_uid('fatigue'/'UDS', 'UDS:Fatigue:fatigue').
card_rarity('fatigue'/'UDS', 'Common').
card_artist('fatigue'/'UDS', 'Jeff Miracola').
card_number('fatigue'/'UDS', '32').
card_flavor_text('fatigue'/'UDS', '\"Mind if I just lie down right here for a few weeks?\"').
card_multiverse_id('fatigue'/'UDS', '19098').

card_in_set('fend off', 'UDS').
card_original_type('fend off'/'UDS', 'Instant').
card_original_text('fend off'/'UDS', 'Cycling {2}\nTarget creature deals no combat damage this turn.').
card_first_print('fend off', 'UDS').
card_image_name('fend off'/'UDS', 'fend off').
card_uid('fend off'/'UDS', 'UDS:Fend Off:fend off').
card_rarity('fend off'/'UDS', 'Common').
card_artist('fend off'/'UDS', 'Paolo Parente').
card_number('fend off'/'UDS', '7').
card_flavor_text('fend off'/'UDS', 'The best defense is to not get hit.').
card_multiverse_id('fend off'/'UDS', '15763').

card_in_set('festering wound', 'UDS').
card_original_type('festering wound'/'UDS', 'Enchant Creature').
card_original_text('festering wound'/'UDS', 'At the beginning of your upkeep, you may put an infection counter on Festering Wound.\nAt the beginning of the upkeep of enchanted creature\'s controller, Festering Wound deals X damage to that player, where X is the number of infection counters on Festering Wound.').
card_first_print('festering wound', 'UDS').
card_image_name('festering wound'/'UDS', 'festering wound').
card_uid('festering wound'/'UDS', 'UDS:Festering Wound:festering wound').
card_rarity('festering wound'/'UDS', 'Uncommon').
card_artist('festering wound'/'UDS', 'Chippy & Matthew Wilson').
card_number('festering wound'/'UDS', '61').
card_multiverse_id('festering wound'/'UDS', '15184').

card_in_set('field surgeon', 'UDS').
card_original_type('field surgeon'/'UDS', 'Creature — Cleric').
card_original_text('field surgeon'/'UDS', 'Tap an untapped creature you control: Prevent the next 1 damage to target creature this turn.').
card_first_print('field surgeon', 'UDS').
card_image_name('field surgeon'/'UDS', 'field surgeon').
card_uid('field surgeon'/'UDS', 'UDS:Field Surgeon:field surgeon').
card_rarity('field surgeon'/'UDS', 'Common').
card_artist('field surgeon'/'UDS', 'Heather Hudson').
card_number('field surgeon'/'UDS', '8').
card_flavor_text('field surgeon'/'UDS', 'Commanders order soldiers to attack. Field surgeons order them to heal. Both are obeyed without question.').
card_multiverse_id('field surgeon'/'UDS', '15121').

card_in_set('flame jet', 'UDS').
card_original_type('flame jet'/'UDS', 'Sorcery').
card_original_text('flame jet'/'UDS', 'Cycling {2}\nFlame Jet deals 3 damage to target player.').
card_first_print('flame jet', 'UDS').
card_image_name('flame jet'/'UDS', 'flame jet').
card_uid('flame jet'/'UDS', 'UDS:Flame Jet:flame jet').
card_rarity('flame jet'/'UDS', 'Common').
card_artist('flame jet'/'UDS', 'John Avon').
card_number('flame jet'/'UDS', '81').
card_flavor_text('flame jet'/'UDS', 'Did the land make the Keldons? Did the Keldons make the land? Or were they simply meant for each other?').
card_multiverse_id('flame jet'/'UDS', '15201').

card_in_set('fledgling osprey', 'UDS').
card_original_type('fledgling osprey'/'UDS', 'Creature — Bird').
card_original_text('fledgling osprey'/'UDS', 'Fledgling Osprey gains flying as long as it\'s enchanted.').
card_first_print('fledgling osprey', 'UDS').
card_image_name('fledgling osprey'/'UDS', 'fledgling osprey').
card_uid('fledgling osprey'/'UDS', 'UDS:Fledgling Osprey:fledgling osprey').
card_rarity('fledgling osprey'/'UDS', 'Common').
card_artist('fledgling osprey'/'UDS', 'Heather Hudson').
card_number('fledgling osprey'/'UDS', '33').
card_flavor_text('fledgling osprey'/'UDS', 'It isn\'t truly born until its first flight.').
card_multiverse_id('fledgling osprey'/'UDS', '15145').

card_in_set('flicker', 'UDS').
card_original_type('flicker'/'UDS', 'Sorcery').
card_original_text('flicker'/'UDS', 'Remove target nontoken permanent from the game, then return it to play under its owner\'s control.').
card_first_print('flicker', 'UDS').
card_image_name('flicker'/'UDS', 'flicker').
card_uid('flicker'/'UDS', 'UDS:Flicker:flicker').
card_rarity('flicker'/'UDS', 'Rare').
card_artist('flicker'/'UDS', 'Douglas Shuler').
card_number('flicker'/'UDS', '9').
card_flavor_text('flicker'/'UDS', 'Who is truer: you who are, or you who are to be?').
card_multiverse_id('flicker'/'UDS', '15127').

card_in_set('fodder cannon', 'UDS').
card_original_type('fodder cannon'/'UDS', 'Artifact').
card_original_text('fodder cannon'/'UDS', '{4}, {T}, Sacrifice a creature: Fodder Cannon deals 4 damage to target creature.').
card_first_print('fodder cannon', 'UDS').
card_image_name('fodder cannon'/'UDS', 'fodder cannon').
card_uid('fodder cannon'/'UDS', 'UDS:Fodder Cannon:fodder cannon').
card_rarity('fodder cannon'/'UDS', 'Uncommon').
card_artist('fodder cannon'/'UDS', 'DiTerlizzi').
card_number('fodder cannon'/'UDS', '131').
card_flavor_text('fodder cannon'/'UDS', 'Step 1: Find your cousin.\nStep 2: Get your cousin in the cannon.\nStep 3: Find another cousin.').
card_multiverse_id('fodder cannon'/'UDS', '15250').

card_in_set('gamekeeper', 'UDS').
card_original_type('gamekeeper'/'UDS', 'Creature — Elf').
card_original_text('gamekeeper'/'UDS', 'When Gamekeeper is put into a graveyard from play, you may remove Gamekeeper from the game. If you do, reveal cards from your library until you reveal a creature card. Put that card into play and put the other cards revealed this way into your graveyard.').
card_first_print('gamekeeper', 'UDS').
card_image_name('gamekeeper'/'UDS', 'gamekeeper').
card_uid('gamekeeper'/'UDS', 'UDS:Gamekeeper:gamekeeper').
card_rarity('gamekeeper'/'UDS', 'Uncommon').
card_artist('gamekeeper'/'UDS', 'Scott Hampton').
card_number('gamekeeper'/'UDS', '106').
card_multiverse_id('gamekeeper'/'UDS', '15230').

card_in_set('goblin berserker', 'UDS').
card_original_type('goblin berserker'/'UDS', 'Creature — Goblin').
card_original_text('goblin berserker'/'UDS', 'First strike; haste (This creature may attack and {T} the turn it comes under your control.)').
card_first_print('goblin berserker', 'UDS').
card_image_name('goblin berserker'/'UDS', 'goblin berserker').
card_uid('goblin berserker'/'UDS', 'UDS:Goblin Berserker:goblin berserker').
card_rarity('goblin berserker'/'UDS', 'Uncommon').
card_artist('goblin berserker'/'UDS', 'Christopher Rush').
card_number('goblin berserker'/'UDS', '82').
card_flavor_text('goblin berserker'/'UDS', 'Goblins don\'t know the meaning of the word \"tactics\"—or the word \"meaning,\" for that matter.').
card_multiverse_id('goblin berserker'/'UDS', '15876').

card_in_set('goblin festival', 'UDS').
card_original_type('goblin festival'/'UDS', 'Enchantment').
card_original_text('goblin festival'/'UDS', '{2}: Goblin Festival deals 1 damage to target creature or player. Flip a coin. If you lose the flip, choose one of your opponents. That player gains control of Goblin Festival.').
card_first_print('goblin festival', 'UDS').
card_image_name('goblin festival'/'UDS', 'goblin festival').
card_uid('goblin festival'/'UDS', 'UDS:Goblin Festival:goblin festival').
card_rarity('goblin festival'/'UDS', 'Rare').
card_artist('goblin festival'/'UDS', 'Jeff Laubenstein').
card_number('goblin festival'/'UDS', '83').
card_flavor_text('goblin festival'/'UDS', '\"What are we celebratin\' again?\"').
card_multiverse_id('goblin festival'/'UDS', '15771').

card_in_set('goblin gardener', 'UDS').
card_original_type('goblin gardener'/'UDS', 'Creature — Goblin').
card_original_text('goblin gardener'/'UDS', 'When Goblin Gardener is put into a graveyard from play, destroy target land.').
card_first_print('goblin gardener', 'UDS').
card_image_name('goblin gardener'/'UDS', 'goblin gardener').
card_uid('goblin gardener'/'UDS', 'UDS:Goblin Gardener:goblin gardener').
card_rarity('goblin gardener'/'UDS', 'Common').
card_artist('goblin gardener'/'UDS', 'Dan Frazier').
card_number('goblin gardener'/'UDS', '84').
card_flavor_text('goblin gardener'/'UDS', '\"Grow food in dirt? Save time—eat dirt.\"').
card_multiverse_id('goblin gardener'/'UDS', '15197').

card_in_set('goblin marshal', 'UDS').
card_original_type('goblin marshal'/'UDS', 'Creature — Goblin').
card_original_text('goblin marshal'/'UDS', 'Echo\nWhenever Goblin Marshal comes into play or is put into a graveyard from play, put two 1/1 red Goblin creature tokens into play.').
card_first_print('goblin marshal', 'UDS').
card_image_name('goblin marshal'/'UDS', 'goblin marshal').
card_uid('goblin marshal'/'UDS', 'UDS:Goblin Marshal:goblin marshal').
card_rarity('goblin marshal'/'UDS', 'Rare').
card_artist('goblin marshal'/'UDS', 'DiTerlizzi').
card_number('goblin marshal'/'UDS', '85').
card_multiverse_id('goblin marshal'/'UDS', '15212').

card_in_set('goblin masons', 'UDS').
card_original_type('goblin masons'/'UDS', 'Creature — Goblin').
card_original_text('goblin masons'/'UDS', 'When Goblin Masons is put into a graveyard from play, destroy target Wall.').
card_first_print('goblin masons', 'UDS').
card_image_name('goblin masons'/'UDS', 'goblin masons').
card_uid('goblin masons'/'UDS', 'UDS:Goblin Masons:goblin masons').
card_rarity('goblin masons'/'UDS', 'Common').
card_artist('goblin masons'/'UDS', 'DiTerlizzi').
card_number('goblin masons'/'UDS', '86').
card_flavor_text('goblin masons'/'UDS', '\"Goblins build with true zeal—and anything else within arm\'s reach.\"').
card_multiverse_id('goblin masons'/'UDS', '15202').

card_in_set('goliath beetle', 'UDS').
card_original_type('goliath beetle'/'UDS', 'Creature — Insect').
card_original_text('goliath beetle'/'UDS', 'Trample').
card_first_print('goliath beetle', 'UDS').
card_image_name('goliath beetle'/'UDS', 'goliath beetle').
card_uid('goliath beetle'/'UDS', 'UDS:Goliath Beetle:goliath beetle').
card_rarity('goliath beetle'/'UDS', 'Common').
card_artist('goliath beetle'/'UDS', 'Hazeltine').
card_number('goliath beetle'/'UDS', '107').
card_flavor_text('goliath beetle'/'UDS', 'The balance of nature dictates that one day humans will be stepped on by bugs.').
card_multiverse_id('goliath beetle'/'UDS', '15223').

card_in_set('heart warden', 'UDS').
card_original_type('heart warden'/'UDS', 'Creature — Elf').
card_original_text('heart warden'/'UDS', '{T}: Add one green mana to your mana pool.\n{2}, Sacrifice Heart Warden: Draw a card.').
card_first_print('heart warden', 'UDS').
card_image_name('heart warden'/'UDS', 'heart warden').
card_uid('heart warden'/'UDS', 'UDS:Heart Warden:heart warden').
card_rarity('heart warden'/'UDS', 'Common').
card_artist('heart warden'/'UDS', 'Adam Rex').
card_number('heart warden'/'UDS', '108').
card_flavor_text('heart warden'/'UDS', '\"In Llanowar, we tend the forest\'s boughs and branches. In Yavimaya, we are a part of them.\"\n—Rofellos of Llanowar').
card_multiverse_id('heart warden'/'UDS', '15220').

card_in_set('hulking ogre', 'UDS').
card_original_type('hulking ogre'/'UDS', 'Creature — Ogre').
card_original_text('hulking ogre'/'UDS', 'Hulking Ogre can\'t block.').
card_first_print('hulking ogre', 'UDS').
card_image_name('hulking ogre'/'UDS', 'hulking ogre').
card_uid('hulking ogre'/'UDS', 'UDS:Hulking Ogre:hulking ogre').
card_rarity('hulking ogre'/'UDS', 'Common').
card_artist('hulking ogre'/'UDS', 'Greg & Tim Hildebrandt').
card_number('hulking ogre'/'UDS', '87').
card_flavor_text('hulking ogre'/'UDS', '\"The Keldons\' ogre campaign provided more body parts than my meager facility could properly use.\"\n—Gatha, Tolarian renegade').
card_multiverse_id('hulking ogre'/'UDS', '15196').

card_in_set('hunting moa', 'UDS').
card_original_type('hunting moa'/'UDS', 'Creature — Beast').
card_original_text('hunting moa'/'UDS', 'Echo\nWhenever Hunting Moa comes into play or is put into a graveyard from play, put a +1/+1 counter on target creature.').
card_first_print('hunting moa', 'UDS').
card_image_name('hunting moa'/'UDS', 'hunting moa').
card_uid('hunting moa'/'UDS', 'UDS:Hunting Moa:hunting moa').
card_rarity('hunting moa'/'UDS', 'Uncommon').
card_artist('hunting moa'/'UDS', 'DiTerlizzi').
card_number('hunting moa'/'UDS', '109').
card_multiverse_id('hunting moa'/'UDS', '15219').

card_in_set('illuminated wings', 'UDS').
card_original_type('illuminated wings'/'UDS', 'Enchant Creature').
card_original_text('illuminated wings'/'UDS', 'Enchanted creature gains flying.\n{2}, Sacrifice Illuminated Wings: Draw a card.').
card_first_print('illuminated wings', 'UDS').
card_image_name('illuminated wings'/'UDS', 'illuminated wings').
card_uid('illuminated wings'/'UDS', 'UDS:Illuminated Wings:illuminated wings').
card_rarity('illuminated wings'/'UDS', 'Common').
card_artist('illuminated wings'/'UDS', 'Jim Nelson').
card_number('illuminated wings'/'UDS', '34').
card_flavor_text('illuminated wings'/'UDS', 'For a moment, Urza thought not of war and destruction, but of the freedom of the skies.').
card_multiverse_id('illuminated wings'/'UDS', '19097').

card_in_set('impatience', 'UDS').
card_original_type('impatience'/'UDS', 'Enchantment').
card_original_text('impatience'/'UDS', 'At the end of each player\'s turn, if that player didn\'t play a spell that turn, Impatience deals 2 damage to him or her.').
card_first_print('impatience', 'UDS').
card_image_name('impatience'/'UDS', 'impatience').
card_uid('impatience'/'UDS', 'UDS:Impatience:impatience').
card_rarity('impatience'/'UDS', 'Rare').
card_artist('impatience'/'UDS', 'Mark Brill').
card_number('impatience'/'UDS', '88').
card_flavor_text('impatience'/'UDS', 'Ask a Keldon to hold his temper and you\'ll be left holding your guts.').
card_multiverse_id('impatience'/'UDS', '15217').

card_in_set('incendiary', 'UDS').
card_original_type('incendiary'/'UDS', 'Enchant Creature').
card_original_text('incendiary'/'UDS', 'At the beginning of your upkeep, you may put a fuse counter on Incendiary.\nWhen enchanted creature is put into a graveyard, Incendiary deals X damage to target creature or player, where X is the number of fuse counters on Incendiary.').
card_first_print('incendiary', 'UDS').
card_image_name('incendiary'/'UDS', 'incendiary').
card_uid('incendiary'/'UDS', 'UDS:Incendiary:incendiary').
card_rarity('incendiary'/'UDS', 'Uncommon').
card_artist('incendiary'/'UDS', 'Jeff Laubenstein').
card_number('incendiary'/'UDS', '89').
card_multiverse_id('incendiary'/'UDS', '15211').

card_in_set('iridescent drake', 'UDS').
card_original_type('iridescent drake'/'UDS', 'Creature — Drake').
card_original_text('iridescent drake'/'UDS', 'Flying\nWhen Iridescent Drake comes into play, return target enchant creature card from a graveyard to play enchanting Iridescent Drake. (You control that enchantment.)').
card_first_print('iridescent drake', 'UDS').
card_image_name('iridescent drake'/'UDS', 'iridescent drake').
card_uid('iridescent drake'/'UDS', 'UDS:Iridescent Drake:iridescent drake').
card_rarity('iridescent drake'/'UDS', 'Uncommon').
card_artist('iridescent drake'/'UDS', 'Jim Nelson').
card_number('iridescent drake'/'UDS', '35').
card_multiverse_id('iridescent drake'/'UDS', '15163').

card_in_set('ivy seer', 'UDS').
card_original_type('ivy seer'/'UDS', 'Creature — Wizard').
card_original_text('ivy seer'/'UDS', '{2}{G}, {T}: Reveal any number of green cards in your hand. Target creature gets +X/+X until end of turn, where X is the number of cards revealed this way.').
card_first_print('ivy seer', 'UDS').
card_image_name('ivy seer'/'UDS', 'ivy seer').
card_uid('ivy seer'/'UDS', 'UDS:Ivy Seer:ivy seer').
card_rarity('ivy seer'/'UDS', 'Uncommon').
card_artist('ivy seer'/'UDS', 'Donato Giancola').
card_number('ivy seer'/'UDS', '110').
card_multiverse_id('ivy seer'/'UDS', '15224').

card_in_set('jasmine seer', 'UDS').
card_original_type('jasmine seer'/'UDS', 'Creature — Wizard').
card_original_text('jasmine seer'/'UDS', '{2}{W}, {T}: Reveal any number of white cards in your hand. You gain 2 life for each card revealed this way.').
card_first_print('jasmine seer', 'UDS').
card_image_name('jasmine seer'/'UDS', 'jasmine seer').
card_uid('jasmine seer'/'UDS', 'UDS:Jasmine Seer:jasmine seer').
card_rarity('jasmine seer'/'UDS', 'Uncommon').
card_artist('jasmine seer'/'UDS', 'Donato Giancola').
card_number('jasmine seer'/'UDS', '10').
card_multiverse_id('jasmine seer'/'UDS', '15124').

card_in_set('junk diver', 'UDS').
card_original_type('junk diver'/'UDS', 'Artifact Creature').
card_original_text('junk diver'/'UDS', 'Flying\nWhen Junk Diver is put into a graveyard from play, return another target artifact card from your graveyard to your hand.').
card_first_print('junk diver', 'UDS').
card_image_name('junk diver'/'UDS', 'junk diver').
card_uid('junk diver'/'UDS', 'UDS:Junk Diver:junk diver').
card_rarity('junk diver'/'UDS', 'Rare').
card_artist('junk diver'/'UDS', 'Eric Peterson').
card_number('junk diver'/'UDS', '132').
card_flavor_text('junk diver'/'UDS', 'Garbage in, treasure out.').
card_multiverse_id('junk diver'/'UDS', '15256').

card_in_set('keldon champion', 'UDS').
card_original_type('keldon champion'/'UDS', 'Creature — Barbarian').
card_original_text('keldon champion'/'UDS', 'Echo; haste (This creature may attack and {T} the turn it comes under your control.)\nWhen Keldon Champion comes into play, it deals 3 damage to target player.').
card_first_print('keldon champion', 'UDS').
card_image_name('keldon champion'/'UDS', 'keldon champion').
card_uid('keldon champion'/'UDS', 'UDS:Keldon Champion:keldon champion').
card_rarity('keldon champion'/'UDS', 'Uncommon').
card_artist('keldon champion'/'UDS', 'Mark Tedin').
card_number('keldon champion'/'UDS', '90').
card_multiverse_id('keldon champion'/'UDS', '15206').

card_in_set('keldon vandals', 'UDS').
card_original_type('keldon vandals'/'UDS', 'Creature — Townsfolk').
card_original_text('keldon vandals'/'UDS', 'Echo\nWhen Keldon Vandals comes into play, destroy target artifact.').
card_first_print('keldon vandals', 'UDS').
card_image_name('keldon vandals'/'UDS', 'keldon vandals').
card_uid('keldon vandals'/'UDS', 'UDS:Keldon Vandals:keldon vandals').
card_rarity('keldon vandals'/'UDS', 'Common').
card_artist('keldon vandals'/'UDS', 'Greg Staples').
card_number('keldon vandals'/'UDS', '91').
card_flavor_text('keldon vandals'/'UDS', 'Keldons divide all their spoils into two groups: trophies and catapult ammunition.').
card_multiverse_id('keldon vandals'/'UDS', '15194').

card_in_set('kingfisher', 'UDS').
card_original_type('kingfisher'/'UDS', 'Creature — Bird').
card_original_text('kingfisher'/'UDS', 'Flying\nWhen Kingfisher is put into a graveyard from play, draw a card.').
card_first_print('kingfisher', 'UDS').
card_image_name('kingfisher'/'UDS', 'kingfisher').
card_uid('kingfisher'/'UDS', 'UDS:Kingfisher:kingfisher').
card_rarity('kingfisher'/'UDS', 'Common').
card_artist('kingfisher'/'UDS', 'Edward P. Beard, Jr.').
card_number('kingfisher'/'UDS', '36').
card_flavor_text('kingfisher'/'UDS', 'It\'s tastiest when served with the fish it stole.').
card_multiverse_id('kingfisher'/'UDS', '15147').

card_in_set('landslide', 'UDS').
card_original_type('landslide'/'UDS', 'Sorcery').
card_original_text('landslide'/'UDS', 'Sacrifice any number of mountains. Landslide deals that much damage to target player.').
card_first_print('landslide', 'UDS').
card_image_name('landslide'/'UDS', 'landslide').
card_uid('landslide'/'UDS', 'UDS:Landslide:landslide').
card_rarity('landslide'/'UDS', 'Uncommon').
card_artist('landslide'/'UDS', 'Jeff Laubenstein').
card_number('landslide'/'UDS', '92').
card_flavor_text('landslide'/'UDS', 'Sometimes the mountain takes.\n—Keldon saying').
card_multiverse_id('landslide'/'UDS', '13821').

card_in_set('lurking jackals', 'UDS').
card_original_type('lurking jackals'/'UDS', 'Enchantment').
card_original_text('lurking jackals'/'UDS', 'When one of your opponents has 10 life or less, if Lurking Jackals is an enchantment, it becomes a 3/2 Hound creature.').
card_first_print('lurking jackals', 'UDS').
card_image_name('lurking jackals'/'UDS', 'lurking jackals').
card_uid('lurking jackals'/'UDS', 'UDS:Lurking Jackals:lurking jackals').
card_rarity('lurking jackals'/'UDS', 'Uncommon').
card_artist('lurking jackals'/'UDS', 'Greg Staples').
card_number('lurking jackals'/'UDS', '62').
card_flavor_text('lurking jackals'/'UDS', 'Often it\'s not the hunter or the hunted but the opportunist who thrives in Rath.').
card_multiverse_id('lurking jackals'/'UDS', '19116').

card_in_set('magnify', 'UDS').
card_original_type('magnify'/'UDS', 'Instant').
card_original_text('magnify'/'UDS', 'All creatures get +1/+1 until end of turn.').
card_first_print('magnify', 'UDS').
card_image_name('magnify'/'UDS', 'magnify').
card_uid('magnify'/'UDS', 'UDS:Magnify:magnify').
card_rarity('magnify'/'UDS', 'Common').
card_artist('magnify'/'UDS', 'Michael Sutfin').
card_number('magnify'/'UDS', '111').
card_flavor_text('magnify'/'UDS', 'The seed torches\' pollen gives off light, but the elves find it heightens their senses as well.').
card_multiverse_id('magnify'/'UDS', '15819').

card_in_set('mantis engine', 'UDS').
card_original_type('mantis engine'/'UDS', 'Artifact Creature').
card_original_text('mantis engine'/'UDS', '{2}: Mantis Engine gains flying until end of turn.\n{2}: Mantis Engine gains first strike until end of turn.').
card_first_print('mantis engine', 'UDS').
card_image_name('mantis engine'/'UDS', 'mantis engine').
card_uid('mantis engine'/'UDS', 'UDS:Mantis Engine:mantis engine').
card_rarity('mantis engine'/'UDS', 'Uncommon').
card_artist('mantis engine'/'UDS', 'John Zeleznik').
card_number('mantis engine'/'UDS', '133').
card_flavor_text('mantis engine'/'UDS', 'Tawnos left a legacy of animal designs in many of Urza\'s creations.').
card_multiverse_id('mantis engine'/'UDS', '15247').

card_in_set('mark of fury', 'UDS').
card_original_type('mark of fury'/'UDS', 'Enchant Creature').
card_original_text('mark of fury'/'UDS', 'Enchanted creature gains haste. (It may attack and {T} the turn it comes under your control.) \nAt end of turn, return Mark of Fury to its owner\'s hand.').
card_first_print('mark of fury', 'UDS').
card_image_name('mark of fury'/'UDS', 'mark of fury').
card_uid('mark of fury'/'UDS', 'UDS:Mark of Fury:mark of fury').
card_rarity('mark of fury'/'UDS', 'Common').
card_artist('mark of fury'/'UDS', 'Thomas M. Baxa').
card_number('mark of fury'/'UDS', '93').
card_flavor_text('mark of fury'/'UDS', 'Many Keldon warriors bear the mark of intentional insanity.').
card_multiverse_id('mark of fury'/'UDS', '15203').

card_in_set('marker beetles', 'UDS').
card_original_type('marker beetles'/'UDS', 'Creature — Insect').
card_original_text('marker beetles'/'UDS', 'When Marker Beetles is put into a graveyard from play, target creature gets +1/+1 until end of turn.\n{2}, Sacrifice Marker Beetles: Draw a card.').
card_first_print('marker beetles', 'UDS').
card_image_name('marker beetles'/'UDS', 'marker beetles').
card_uid('marker beetles'/'UDS', 'UDS:Marker Beetles:marker beetles').
card_rarity('marker beetles'/'UDS', 'Common').
card_artist('marker beetles'/'UDS', 'Ron Spencer').
card_number('marker beetles'/'UDS', '112').
card_flavor_text('marker beetles'/'UDS', 'In case of emergency, crush bug.').
card_multiverse_id('marker beetles'/'UDS', '15222').

card_in_set('mask of law and grace', 'UDS').
card_original_type('mask of law and grace'/'UDS', 'Enchant Creature').
card_original_text('mask of law and grace'/'UDS', 'Enchanted creature gains protection from black and protection from red.').
card_first_print('mask of law and grace', 'UDS').
card_image_name('mask of law and grace'/'UDS', 'mask of law and grace').
card_uid('mask of law and grace'/'UDS', 'UDS:Mask of Law and Grace:mask of law and grace').
card_rarity('mask of law and grace'/'UDS', 'Common').
card_artist('mask of law and grace'/'UDS', 'Kev Walker').
card_number('mask of law and grace'/'UDS', '11').
card_flavor_text('mask of law and grace'/'UDS', 'The archangels zealously drove Serra\'s light into every corner of their new home as if their creator still commanded them.').
card_multiverse_id('mask of law and grace'/'UDS', '15129').

card_in_set('master healer', 'UDS').
card_original_type('master healer'/'UDS', 'Creature — Cleric').
card_original_text('master healer'/'UDS', '{T}: Prevent the next 4 damage to target creature or player this turn.').
card_first_print('master healer', 'UDS').
card_image_name('master healer'/'UDS', 'master healer').
card_uid('master healer'/'UDS', 'UDS:Master Healer:master healer').
card_rarity('master healer'/'UDS', 'Rare').
card_artist('master healer'/'UDS', 'Adam Rex').
card_number('master healer'/'UDS', '12').
card_flavor_text('master healer'/'UDS', 'Behind his eyes is the pain of every soldier his hands have healed.').
card_multiverse_id('master healer'/'UDS', '15874').

card_in_set('masticore', 'UDS').
card_original_type('masticore'/'UDS', 'Artifact Creature').
card_original_text('masticore'/'UDS', 'At the beginning of your upkeep, you may choose and discard a card from your hand. If you don\'t, sacrifice Masticore.\n{2}: Masticore deals 1 damage to target creature.\n{2}: Regenerate Masticore.').
card_first_print('masticore', 'UDS').
card_image_name('masticore'/'UDS', 'masticore').
card_uid('masticore'/'UDS', 'UDS:Masticore:masticore').
card_rarity('masticore'/'UDS', 'Rare').
card_artist('masticore'/'UDS', 'Paolo Parente').
card_number('masticore'/'UDS', '134').
card_multiverse_id('masticore'/'UDS', '13087').

card_in_set('mental discipline', 'UDS').
card_original_type('mental discipline'/'UDS', 'Enchantment').
card_original_text('mental discipline'/'UDS', '{1}{U}, Choose and discard a card from your hand: Draw a card.').
card_first_print('mental discipline', 'UDS').
card_image_name('mental discipline'/'UDS', 'mental discipline').
card_uid('mental discipline'/'UDS', 'UDS:Mental Discipline:mental discipline').
card_rarity('mental discipline'/'UDS', 'Common').
card_artist('mental discipline'/'UDS', 'Edward P. Beard, Jr.').
card_number('mental discipline'/'UDS', '37').
card_flavor_text('mental discipline'/'UDS', 'Barrin drowned his doubts about Urza\'s project by delving ever deeper into its details.').
card_multiverse_id('mental discipline'/'UDS', '15158').

card_in_set('metalworker', 'UDS').
card_original_type('metalworker'/'UDS', 'Artifact Creature').
card_original_text('metalworker'/'UDS', '{T}: Reveal any number of artifact cards in your hand. Add two colorless mana to your mana pool for each card revealed this way.').
card_first_print('metalworker', 'UDS').
card_image_name('metalworker'/'UDS', 'metalworker').
card_uid('metalworker'/'UDS', 'UDS:Metalworker:metalworker').
card_rarity('metalworker'/'UDS', 'Rare').
card_artist('metalworker'/'UDS', 'Hazeltine').
card_number('metalworker'/'UDS', '135').
card_flavor_text('metalworker'/'UDS', '\"At this rate I fully expect to be replaced by a clockwork golem by year\'s end.\"\n—Barrin').
card_multiverse_id('metalworker'/'UDS', '15246').

card_in_set('metathran elite', 'UDS').
card_original_type('metathran elite'/'UDS', 'Creature — Soldier').
card_original_text('metathran elite'/'UDS', 'Metathran Elite is unblockable as long as it\'s enchanted.').
card_first_print('metathran elite', 'UDS').
card_image_name('metathran elite'/'UDS', 'metathran elite').
card_uid('metathran elite'/'UDS', 'UDS:Metathran Elite:metathran elite').
card_rarity('metathran elite'/'UDS', 'Uncommon').
card_artist('metathran elite'/'UDS', 'Jim Nelson').
card_number('metathran elite'/'UDS', '38').
card_flavor_text('metathran elite'/'UDS', 'If the eyes are windows to the soul, their souls are not of this world.').
card_multiverse_id('metathran elite'/'UDS', '15157').

card_in_set('metathran soldier', 'UDS').
card_original_type('metathran soldier'/'UDS', 'Creature — Soldier').
card_original_text('metathran soldier'/'UDS', 'Metathran Soldier is unblockable.').
card_first_print('metathran soldier', 'UDS').
card_image_name('metathran soldier'/'UDS', 'metathran soldier').
card_uid('metathran soldier'/'UDS', 'UDS:Metathran Soldier:metathran soldier').
card_rarity('metathran soldier'/'UDS', 'Common').
card_artist('metathran soldier'/'UDS', 'Paolo Parente').
card_number('metathran soldier'/'UDS', '39').
card_flavor_text('metathran soldier'/'UDS', 'Just as Serra crafted angels of light and faith, Urza constructed an army of sorcery and power to resist the coming invasion.').
card_multiverse_id('metathran soldier'/'UDS', '19096').

card_in_set('momentum', 'UDS').
card_original_type('momentum'/'UDS', 'Enchant Creature').
card_original_text('momentum'/'UDS', 'At the beginning of your upkeep, you may put a growth counter on Momentum.\nEnchanted creature gets +1/+1 for each growth counter on Momentum.').
card_first_print('momentum', 'UDS').
card_image_name('momentum'/'UDS', 'momentum').
card_uid('momentum'/'UDS', 'UDS:Momentum:momentum').
card_rarity('momentum'/'UDS', 'Uncommon').
card_artist('momentum'/'UDS', 'Carl Critchlow').
card_number('momentum'/'UDS', '113').
card_multiverse_id('momentum'/'UDS', '15235').

card_in_set('multani\'s decree', 'UDS').
card_original_type('multani\'s decree'/'UDS', 'Sorcery').
card_original_text('multani\'s decree'/'UDS', 'Destroy all enchantments. You gain 2 life for each enchantment destroyed this way.').
card_first_print('multani\'s decree', 'UDS').
card_image_name('multani\'s decree'/'UDS', 'multani\'s decree').
card_uid('multani\'s decree'/'UDS', 'UDS:Multani\'s Decree:multani\'s decree').
card_rarity('multani\'s decree'/'UDS', 'Common').
card_artist('multani\'s decree'/'UDS', 'Eric Peterson').
card_number('multani\'s decree'/'UDS', '114').
card_flavor_text('multani\'s decree'/'UDS', '\"This is my place of power. Nothing can take root here unless I allow it.\"\n—Multani, maro-sorcerer').
card_multiverse_id('multani\'s decree'/'UDS', '15228').

card_in_set('nightshade seer', 'UDS').
card_original_type('nightshade seer'/'UDS', 'Creature — Wizard').
card_original_text('nightshade seer'/'UDS', '{2}{B}, {T}: Reveal any number of black cards in your hand. Target creature gets -X/-X until end of turn, where X is the number of cards revealed this way.').
card_first_print('nightshade seer', 'UDS').
card_image_name('nightshade seer'/'UDS', 'nightshade seer').
card_uid('nightshade seer'/'UDS', 'UDS:Nightshade Seer:nightshade seer').
card_rarity('nightshade seer'/'UDS', 'Uncommon').
card_artist('nightshade seer'/'UDS', 'Donato Giancola').
card_number('nightshade seer'/'UDS', '63').
card_multiverse_id('nightshade seer'/'UDS', '15173').

card_in_set('opalescence', 'UDS').
card_original_type('opalescence'/'UDS', 'Enchantment').
card_original_text('opalescence'/'UDS', 'Each other global enchantment is a creature with power and toughness each equal to its converted mana cost. It\'s still an enchantment.').
card_first_print('opalescence', 'UDS').
card_image_name('opalescence'/'UDS', 'opalescence').
card_uid('opalescence'/'UDS', 'UDS:Opalescence:opalescence').
card_rarity('opalescence'/'UDS', 'Rare').
card_artist('opalescence'/'UDS', 'John Avon').
card_number('opalescence'/'UDS', '13').
card_multiverse_id('opalescence'/'UDS', '15142').

card_in_set('opposition', 'UDS').
card_original_type('opposition'/'UDS', 'Enchantment').
card_original_text('opposition'/'UDS', 'Tap an untapped creature you control: Tap target artifact, creature, or land.').
card_first_print('opposition', 'UDS').
card_image_name('opposition'/'UDS', 'opposition').
card_uid('opposition'/'UDS', 'UDS:Opposition:opposition').
card_rarity('opposition'/'UDS', 'Rare').
card_artist('opposition'/'UDS', 'Todd Lockwood').
card_number('opposition'/'UDS', '40').
card_flavor_text('opposition'/'UDS', '\"Urza says he\'s sane. Perhaps, but measures of sanity among planeswalkers are hard to come by.\"\n—Barrin').
card_multiverse_id('opposition'/'UDS', '15162').

card_in_set('pattern of rebirth', 'UDS').
card_original_type('pattern of rebirth'/'UDS', 'Enchant Creature').
card_original_text('pattern of rebirth'/'UDS', 'When enchanted creature is put into a graveyard from play, that creature\'s controller may search his or her library for a creature card and put that card into play. If that player does, he or she then shuffles his or her library.').
card_first_print('pattern of rebirth', 'UDS').
card_image_name('pattern of rebirth'/'UDS', 'pattern of rebirth').
card_uid('pattern of rebirth'/'UDS', 'UDS:Pattern of Rebirth:pattern of rebirth').
card_rarity('pattern of rebirth'/'UDS', 'Rare').
card_artist('pattern of rebirth'/'UDS', 'Mark Brill').
card_number('pattern of rebirth'/'UDS', '115').
card_multiverse_id('pattern of rebirth'/'UDS', '15234').

card_in_set('phyrexian monitor', 'UDS').
card_original_type('phyrexian monitor'/'UDS', 'Creature — Skeleton').
card_original_text('phyrexian monitor'/'UDS', '{B} Regenerate Phyrexian Monitor.').
card_first_print('phyrexian monitor', 'UDS').
card_image_name('phyrexian monitor'/'UDS', 'phyrexian monitor').
card_uid('phyrexian monitor'/'UDS', 'UDS:Phyrexian Monitor:phyrexian monitor').
card_rarity('phyrexian monitor'/'UDS', 'Common').
card_artist('phyrexian monitor'/'UDS', 'Carl Critchlow').
card_number('phyrexian monitor'/'UDS', '64').
card_flavor_text('phyrexian monitor'/'UDS', 'Being one would be an honor, if the word \"honor\" had any meaning in Phyrexia.').
card_multiverse_id('phyrexian monitor'/'UDS', '19113').

card_in_set('phyrexian negator', 'UDS').
card_original_type('phyrexian negator'/'UDS', 'Creature — Horror').
card_original_text('phyrexian negator'/'UDS', 'Trample\nWhenever Phyrexian Negator is dealt damage, sacrifice a permanent for each 1 damage dealt to it.').
card_image_name('phyrexian negator'/'UDS', 'phyrexian negator').
card_uid('phyrexian negator'/'UDS', 'UDS:Phyrexian Negator:phyrexian negator').
card_rarity('phyrexian negator'/'UDS', 'Rare').
card_artist('phyrexian negator'/'UDS', 'John Zeleznik').
card_number('phyrexian negator'/'UDS', '65').
card_flavor_text('phyrexian negator'/'UDS', 'They exist to cease.').
card_multiverse_id('phyrexian negator'/'UDS', '5559').

card_in_set('plague dogs', 'UDS').
card_original_type('plague dogs'/'UDS', 'Creature — Hound').
card_original_text('plague dogs'/'UDS', 'When Plague Dogs is put into a graveyard from play, all creatures get -1/-1 until end of turn.\n{2}, Sacrifice Plague Dogs: Draw a card.').
card_first_print('plague dogs', 'UDS').
card_image_name('plague dogs'/'UDS', 'plague dogs').
card_uid('plague dogs'/'UDS', 'UDS:Plague Dogs:plague dogs').
card_rarity('plague dogs'/'UDS', 'Uncommon').
card_artist('plague dogs'/'UDS', 'Chippy & Matthew Wilson').
card_number('plague dogs'/'UDS', '66').
card_flavor_text('plague dogs'/'UDS', '\"They\'re not just retrievers, they\'re carriers.\"').
card_multiverse_id('plague dogs'/'UDS', '15180').

card_in_set('plated spider', 'UDS').
card_original_type('plated spider'/'UDS', 'Creature — Spider').
card_original_text('plated spider'/'UDS', 'Plated Spider may block as though it had flying.').
card_first_print('plated spider', 'UDS').
card_image_name('plated spider'/'UDS', 'plated spider').
card_uid('plated spider'/'UDS', 'UDS:Plated Spider:plated spider').
card_rarity('plated spider'/'UDS', 'Common').
card_artist('plated spider'/'UDS', 'Ron Spencer').
card_number('plated spider'/'UDS', '116').
card_flavor_text('plated spider'/'UDS', 'Most spiders wait patiently for their prey to arrive. Most spiders aren\'t forty feet tall.').
card_multiverse_id('plated spider'/'UDS', '19239').

card_in_set('plow under', 'UDS').
card_original_type('plow under'/'UDS', 'Sorcery').
card_original_text('plow under'/'UDS', 'Put two target lands on top of their owner\'s library.').
card_first_print('plow under', 'UDS').
card_image_name('plow under'/'UDS', 'plow under').
card_uid('plow under'/'UDS', 'UDS:Plow Under:plow under').
card_rarity('plow under'/'UDS', 'Rare').
card_artist('plow under'/'UDS', 'Edward P. Beard, Jr.').
card_number('plow under'/'UDS', '117').
card_flavor_text('plow under'/'UDS', 'To renew the land, plow the land. To destroy the land, do nothing.\n—Druids\' saying').
card_multiverse_id('plow under'/'UDS', '12628').

card_in_set('powder keg', 'UDS').
card_original_type('powder keg'/'UDS', 'Artifact').
card_original_text('powder keg'/'UDS', 'At the beginning of your upkeep, you may put a fuse counter on Powder Keg.\n{T}, Sacrifice Powder Keg: Destroy each artifact and creature with converted mana cost equal to the number of fuse counters on Powder Keg.').
card_first_print('powder keg', 'UDS').
card_image_name('powder keg'/'UDS', 'powder keg').
card_uid('powder keg'/'UDS', 'UDS:Powder Keg:powder keg').
card_rarity('powder keg'/'UDS', 'Rare').
card_artist('powder keg'/'UDS', 'Dan Frazier').
card_number('powder keg'/'UDS', '136').
card_multiverse_id('powder keg'/'UDS', '15259').

card_in_set('private research', 'UDS').
card_original_type('private research'/'UDS', 'Enchant Creature').
card_original_text('private research'/'UDS', 'At the beginning of your upkeep, you may put a page counter on Private Research.\nWhen enchanted creature is put into a graveyard, draw a card for each page counter on Private Research.').
card_first_print('private research', 'UDS').
card_image_name('private research'/'UDS', 'private research').
card_uid('private research'/'UDS', 'UDS:Private Research:private research').
card_rarity('private research'/'UDS', 'Uncommon').
card_artist('private research'/'UDS', 'Scott M. Fischer').
card_number('private research'/'UDS', '41').
card_multiverse_id('private research'/'UDS', '15161').

card_in_set('quash', 'UDS').
card_original_type('quash'/'UDS', 'Instant').
card_original_text('quash'/'UDS', 'Counter target instant or sorcery spell. Search its controller\'s graveyard, hand, and library for all copies of that card and remove them from the game. That player then shuffles his or her library.').
card_first_print('quash', 'UDS').
card_image_name('quash'/'UDS', 'quash').
card_uid('quash'/'UDS', 'UDS:Quash:quash').
card_rarity('quash'/'UDS', 'Uncommon').
card_artist('quash'/'UDS', 'Don Hazeltine').
card_number('quash'/'UDS', '42').
card_multiverse_id('quash'/'UDS', '15160').

card_in_set('rapid decay', 'UDS').
card_original_type('rapid decay'/'UDS', 'Instant').
card_original_text('rapid decay'/'UDS', 'Cycling {2}\nRemove from the game up to three target cards in a single graveyard.').
card_first_print('rapid decay', 'UDS').
card_image_name('rapid decay'/'UDS', 'rapid decay').
card_uid('rapid decay'/'UDS', 'UDS:Rapid Decay:rapid decay').
card_rarity('rapid decay'/'UDS', 'Rare').
card_artist('rapid decay'/'UDS', 'Chippy').
card_number('rapid decay'/'UDS', '67').
card_flavor_text('rapid decay'/'UDS', 'The grave robbers arrived the day after the burial. They were a day too late.').
card_multiverse_id('rapid decay'/'UDS', '15177').

card_in_set('ravenous rats', 'UDS').
card_original_type('ravenous rats'/'UDS', 'Creature — Rat').
card_original_text('ravenous rats'/'UDS', 'When Ravenous Rats comes into play, target opponent chooses and discards a card from his or her hand.').
card_image_name('ravenous rats'/'UDS', 'ravenous rats').
card_uid('ravenous rats'/'UDS', 'UDS:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'UDS', 'Common').
card_artist('ravenous rats'/'UDS', 'Carl Critchlow').
card_number('ravenous rats'/'UDS', '68').
card_flavor_text('ravenous rats'/'UDS', '\"For all the priceless tomes they have destroyed, one would think they would taste better.\"\n—Davvol, Evincar of Rath').
card_multiverse_id('ravenous rats'/'UDS', '15172').

card_in_set('rayne, academy chancellor', 'UDS').
card_original_type('rayne, academy chancellor'/'UDS', 'Creature — Wizard Legend').
card_original_text('rayne, academy chancellor'/'UDS', 'Whenever you or a permanent you control is the target of a spell or ability controlled by one of your opponents, you may draw a card, and if Rayne, Academy Chancellor is enchanted, you may draw another card.').
card_first_print('rayne, academy chancellor', 'UDS').
card_image_name('rayne, academy chancellor'/'UDS', 'rayne, academy chancellor').
card_uid('rayne, academy chancellor'/'UDS', 'UDS:Rayne, Academy Chancellor:rayne, academy chancellor').
card_rarity('rayne, academy chancellor'/'UDS', 'Rare').
card_artist('rayne, academy chancellor'/'UDS', 'Matthew D. Wilson').
card_number('rayne, academy chancellor'/'UDS', '43').
card_multiverse_id('rayne, academy chancellor'/'UDS', '15164').

card_in_set('reckless abandon', 'UDS').
card_original_type('reckless abandon'/'UDS', 'Sorcery').
card_original_text('reckless abandon'/'UDS', 'As an additional cost to play Reckless Abandon, sacrifice a creature.\nReckless Abandon deals 4 damage to target creature or player.').
card_first_print('reckless abandon', 'UDS').
card_image_name('reckless abandon'/'UDS', 'reckless abandon').
card_uid('reckless abandon'/'UDS', 'UDS:Reckless Abandon:reckless abandon').
card_rarity('reckless abandon'/'UDS', 'Common').
card_artist('reckless abandon'/'UDS', 'Ron Spears').
card_number('reckless abandon'/'UDS', '94').
card_flavor_text('reckless abandon'/'UDS', 'The climax of a warlord\'s career is always death.').
card_multiverse_id('reckless abandon'/'UDS', '15204').

card_in_set('reliquary monk', 'UDS').
card_original_type('reliquary monk'/'UDS', 'Creature — Cleric').
card_original_text('reliquary monk'/'UDS', 'When Reliquary Monk is put into a graveyard from play, destroy target artifact or enchantment.').
card_first_print('reliquary monk', 'UDS').
card_image_name('reliquary monk'/'UDS', 'reliquary monk').
card_uid('reliquary monk'/'UDS', 'UDS:Reliquary Monk:reliquary monk').
card_rarity('reliquary monk'/'UDS', 'Common').
card_artist('reliquary monk'/'UDS', 'Thomas M. Baxa').
card_number('reliquary monk'/'UDS', '14').
card_flavor_text('reliquary monk'/'UDS', 'A thing of Serra\'s realm exists only by the grace of her followers\' faith.').
card_multiverse_id('reliquary monk'/'UDS', '15123').

card_in_set('repercussion', 'UDS').
card_original_type('repercussion'/'UDS', 'Enchantment').
card_original_text('repercussion'/'UDS', 'Whenever a creature is dealt damage, Repercussion deals that much damage to that creature\'s controller.').
card_first_print('repercussion', 'UDS').
card_image_name('repercussion'/'UDS', 'repercussion').
card_uid('repercussion'/'UDS', 'UDS:Repercussion:repercussion').
card_rarity('repercussion'/'UDS', 'Rare').
card_artist('repercussion'/'UDS', 'Michael Sutfin').
card_number('repercussion'/'UDS', '95').
card_flavor_text('repercussion'/'UDS', 'Not all Keld\'s warriors are found on the battlefield.').
card_multiverse_id('repercussion'/'UDS', '15216').

card_in_set('replenish', 'UDS').
card_original_type('replenish'/'UDS', 'Sorcery').
card_original_text('replenish'/'UDS', 'Return all enchantment cards from your graveyard to play. (Local enchantments with no permanent to enchant remain in your graveyard.)').
card_first_print('replenish', 'UDS').
card_image_name('replenish'/'UDS', 'replenish').
card_uid('replenish'/'UDS', 'UDS:Replenish:replenish').
card_rarity('replenish'/'UDS', 'Rare').
card_artist('replenish'/'UDS', 'Jim Nelson').
card_number('replenish'/'UDS', '15').
card_flavor_text('replenish'/'UDS', 'Treasures, trinkets, trash—the relics of the past are brought forth again.').
card_multiverse_id('replenish'/'UDS', '15143').

card_in_set('rescue', 'UDS').
card_original_type('rescue'/'UDS', 'Instant').
card_original_text('rescue'/'UDS', 'Return target permanent you control to its owner\'s hand.').
card_first_print('rescue', 'UDS').
card_image_name('rescue'/'UDS', 'rescue').
card_uid('rescue'/'UDS', 'UDS:Rescue:rescue').
card_rarity('rescue'/'UDS', 'Common').
card_artist('rescue'/'UDS', 'Greg Staples').
card_number('rescue'/'UDS', '44').
card_flavor_text('rescue'/'UDS', '\"Urza, I\'ve discovered more promising students than you and my husband have, combined. And I haven\'t lost a single one.\"\n—Rayne, Academy Chancellor').
card_multiverse_id('rescue'/'UDS', '15154').

card_in_set('rofellos\'s gift', 'UDS').
card_original_type('rofellos\'s gift'/'UDS', 'Sorcery').
card_original_text('rofellos\'s gift'/'UDS', 'Reveal any number of green cards in your hand. Return an enchantment card from your graveyard to your hand for each card revealed this way.').
card_first_print('rofellos\'s gift', 'UDS').
card_image_name('rofellos\'s gift'/'UDS', 'rofellos\'s gift').
card_uid('rofellos\'s gift'/'UDS', 'UDS:Rofellos\'s Gift:rofellos\'s gift').
card_rarity('rofellos\'s gift'/'UDS', 'Common').
card_artist('rofellos\'s gift'/'UDS', 'Pete Venters').
card_number('rofellos\'s gift'/'UDS', '119').
card_flavor_text('rofellos\'s gift'/'UDS', '\"Rise, elf. We are both of Gaea, and thus we are equal.\"\n—Multani, to Rofellos').
card_multiverse_id('rofellos\'s gift'/'UDS', '15877').

card_in_set('rofellos, llanowar emissary', 'UDS').
card_original_type('rofellos, llanowar emissary'/'UDS', 'Creature — Elf Legend').
card_original_text('rofellos, llanowar emissary'/'UDS', '{T}: Add one green mana to your mana pool for each forest you control.').
card_first_print('rofellos, llanowar emissary', 'UDS').
card_image_name('rofellos, llanowar emissary'/'UDS', 'rofellos, llanowar emissary').
card_uid('rofellos, llanowar emissary'/'UDS', 'UDS:Rofellos, Llanowar Emissary:rofellos, llanowar emissary').
card_rarity('rofellos, llanowar emissary'/'UDS', 'Rare').
card_artist('rofellos, llanowar emissary'/'UDS', 'Michael Sutfin').
card_number('rofellos, llanowar emissary'/'UDS', '118').
card_multiverse_id('rofellos, llanowar emissary'/'UDS', '15237').

card_in_set('sanctimony', 'UDS').
card_original_type('sanctimony'/'UDS', 'Enchantment').
card_original_text('sanctimony'/'UDS', 'Whenever one of your opponents taps a mountain for mana, you may gain 1 life.').
card_first_print('sanctimony', 'UDS').
card_image_name('sanctimony'/'UDS', 'sanctimony').
card_uid('sanctimony'/'UDS', 'UDS:Sanctimony:sanctimony').
card_rarity('sanctimony'/'UDS', 'Uncommon').
card_artist('sanctimony'/'UDS', 'Mark Brill').
card_number('sanctimony'/'UDS', '16').
card_flavor_text('sanctimony'/'UDS', '\"To forgive our enemies is to forgive ourselves.\"').
card_multiverse_id('sanctimony'/'UDS', '15873').

card_in_set('scent of brine', 'UDS').
card_original_type('scent of brine'/'UDS', 'Instant').
card_original_text('scent of brine'/'UDS', 'Reveal any number of blue cards in your hand. Counter target spell unless its controller pays {1} for each card revealed this way.').
card_first_print('scent of brine', 'UDS').
card_image_name('scent of brine'/'UDS', 'scent of brine').
card_uid('scent of brine'/'UDS', 'UDS:Scent of Brine:scent of brine').
card_rarity('scent of brine'/'UDS', 'Common').
card_artist('scent of brine'/'UDS', 'Greg Staples').
card_number('scent of brine'/'UDS', '45').
card_multiverse_id('scent of brine'/'UDS', '15266').

card_in_set('scent of cinder', 'UDS').
card_original_type('scent of cinder'/'UDS', 'Sorcery').
card_original_text('scent of cinder'/'UDS', 'Reveal any number of red cards in your hand. Scent of Cinder deals X damage to target creature or player, where X is the number of cards revealed this way.').
card_image_name('scent of cinder'/'UDS', 'scent of cinder').
card_uid('scent of cinder'/'UDS', 'UDS:Scent of Cinder:scent of cinder').
card_rarity('scent of cinder'/'UDS', 'Common').
card_artist('scent of cinder'/'UDS', 'Marc Fishman').
card_number('scent of cinder'/'UDS', '96').
card_multiverse_id('scent of cinder'/'UDS', '15264').

card_in_set('scent of ivy', 'UDS').
card_original_type('scent of ivy'/'UDS', 'Instant').
card_original_text('scent of ivy'/'UDS', 'Reveal any number of green cards in your hand. Target creature gets +X/+X until end of turn, where X is the number of cards revealed this way.').
card_first_print('scent of ivy', 'UDS').
card_image_name('scent of ivy'/'UDS', 'scent of ivy').
card_uid('scent of ivy'/'UDS', 'UDS:Scent of Ivy:scent of ivy').
card_rarity('scent of ivy'/'UDS', 'Common').
card_artist('scent of ivy'/'UDS', 'John Avon').
card_number('scent of ivy'/'UDS', '120').
card_multiverse_id('scent of ivy'/'UDS', '15267').

card_in_set('scent of jasmine', 'UDS').
card_original_type('scent of jasmine'/'UDS', 'Instant').
card_original_text('scent of jasmine'/'UDS', 'Reveal any number of white cards in your hand. You gain 2 life for each card revealed this way.').
card_first_print('scent of jasmine', 'UDS').
card_image_name('scent of jasmine'/'UDS', 'scent of jasmine').
card_uid('scent of jasmine'/'UDS', 'UDS:Scent of Jasmine:scent of jasmine').
card_rarity('scent of jasmine'/'UDS', 'Common').
card_artist('scent of jasmine'/'UDS', 'Douglas Shuler').
card_number('scent of jasmine'/'UDS', '17').
card_multiverse_id('scent of jasmine'/'UDS', '15263').

card_in_set('scent of nightshade', 'UDS').
card_original_type('scent of nightshade'/'UDS', 'Instant').
card_original_text('scent of nightshade'/'UDS', 'Reveal any number of black cards in your hand. Target creature gets -X/-X until end of turn, where X is the number of cards revealed this way.').
card_first_print('scent of nightshade', 'UDS').
card_image_name('scent of nightshade'/'UDS', 'scent of nightshade').
card_uid('scent of nightshade'/'UDS', 'UDS:Scent of Nightshade:scent of nightshade').
card_rarity('scent of nightshade'/'UDS', 'Common').
card_artist('scent of nightshade'/'UDS', 'John Avon').
card_number('scent of nightshade'/'UDS', '69').
card_multiverse_id('scent of nightshade'/'UDS', '15265').

card_in_set('scour', 'UDS').
card_original_type('scour'/'UDS', 'Instant').
card_original_text('scour'/'UDS', 'Remove target enchantment from the game. Search its controller\'s graveyard, hand, and library for all copies of that card and remove them from the game. That player then shuffles his or her library.').
card_first_print('scour', 'UDS').
card_image_name('scour'/'UDS', 'scour').
card_uid('scour'/'UDS', 'UDS:Scour:scour').
card_rarity('scour'/'UDS', 'Uncommon').
card_artist('scour'/'UDS', 'Eric Peterson').
card_number('scour'/'UDS', '18').
card_multiverse_id('scour'/'UDS', '15134').

card_in_set('scrying glass', 'UDS').
card_original_type('scrying glass'/'UDS', 'Artifact').
card_original_text('scrying glass'/'UDS', '{3}, {T}: Choose a number greater than 0 and a color. Target opponent reveals his or her hand. If that opponent reveals exactly the chosen number of cards of the chosen color, you draw a card.').
card_first_print('scrying glass', 'UDS').
card_image_name('scrying glass'/'UDS', 'scrying glass').
card_uid('scrying glass'/'UDS', 'UDS:Scrying Glass:scrying glass').
card_rarity('scrying glass'/'UDS', 'Rare').
card_artist('scrying glass'/'UDS', 'Patrick Ho').
card_number('scrying glass'/'UDS', '137').
card_multiverse_id('scrying glass'/'UDS', '15262').

card_in_set('serra advocate', 'UDS').
card_original_type('serra advocate'/'UDS', 'Creature — Angel').
card_original_text('serra advocate'/'UDS', 'Flying\n{T}: Target attacking or blocking creature gets +2/+2 until end of turn.').
card_first_print('serra advocate', 'UDS').
card_image_name('serra advocate'/'UDS', 'serra advocate').
card_uid('serra advocate'/'UDS', 'UDS:Serra Advocate:serra advocate').
card_rarity('serra advocate'/'UDS', 'Uncommon').
card_artist('serra advocate'/'UDS', 'Scott Hampton').
card_number('serra advocate'/'UDS', '19').
card_flavor_text('serra advocate'/'UDS', 'An angel\'s touch can prepare a soldier for battle more than a thousand military drills.').
card_multiverse_id('serra advocate'/'UDS', '14485').

card_in_set('sigil of sleep', 'UDS').
card_original_type('sigil of sleep'/'UDS', 'Enchant Creature').
card_original_text('sigil of sleep'/'UDS', 'Whenever enchanted creature deals damage to a player, return target creature that player controls to its owner\'s hand.').
card_first_print('sigil of sleep', 'UDS').
card_image_name('sigil of sleep'/'UDS', 'sigil of sleep').
card_uid('sigil of sleep'/'UDS', 'UDS:Sigil of Sleep:sigil of sleep').
card_rarity('sigil of sleep'/'UDS', 'Common').
card_artist('sigil of sleep'/'UDS', 'Greg & Tim Hildebrandt').
card_number('sigil of sleep'/'UDS', '46').
card_flavor_text('sigil of sleep'/'UDS', 'Arrows are only one way to remove an enemy.').
card_multiverse_id('sigil of sleep'/'UDS', '15784').

card_in_set('skittering horror', 'UDS').
card_original_type('skittering horror'/'UDS', 'Creature — Horror').
card_original_text('skittering horror'/'UDS', 'When you play a creature spell, sacrifice Skittering Horror.').
card_first_print('skittering horror', 'UDS').
card_image_name('skittering horror'/'UDS', 'skittering horror').
card_uid('skittering horror'/'UDS', 'UDS:Skittering Horror:skittering horror').
card_rarity('skittering horror'/'UDS', 'Common').
card_artist('skittering horror'/'UDS', 'Mark Zug').
card_number('skittering horror'/'UDS', '70').
card_flavor_text('skittering horror'/'UDS', '\"This monstrosity will do—for now.\"\n—Davvol, Evincar of Rath').
card_multiverse_id('skittering horror'/'UDS', '15787').

card_in_set('slinking skirge', 'UDS').
card_original_type('slinking skirge'/'UDS', 'Creature — Imp').
card_original_text('slinking skirge'/'UDS', 'Flying\n{2}, Sacrifice Slinking Skirge: Draw a card.').
card_first_print('slinking skirge', 'UDS').
card_image_name('slinking skirge'/'UDS', 'slinking skirge').
card_uid('slinking skirge'/'UDS', 'UDS:Slinking Skirge:slinking skirge').
card_rarity('slinking skirge'/'UDS', 'Common').
card_artist('slinking skirge'/'UDS', 'Ron Spencer').
card_number('slinking skirge'/'UDS', '71').
card_flavor_text('slinking skirge'/'UDS', 'Davvol encouraged the skirges; they made excellent sentries and were quite edible if properly seasoned.').
card_multiverse_id('slinking skirge'/'UDS', '15786').

card_in_set('solidarity', 'UDS').
card_original_type('solidarity'/'UDS', 'Instant').
card_original_text('solidarity'/'UDS', 'Creatures you control get +0/+5 until end of turn.').
card_first_print('solidarity', 'UDS').
card_image_name('solidarity'/'UDS', 'solidarity').
card_uid('solidarity'/'UDS', 'UDS:Solidarity:solidarity').
card_rarity('solidarity'/'UDS', 'Common').
card_artist('solidarity'/'UDS', 'John Zeleznik').
card_number('solidarity'/'UDS', '20').
card_flavor_text('solidarity'/'UDS', 'Their commitment to Serra is as solid as any shield.').
card_multiverse_id('solidarity'/'UDS', '15764').

card_in_set('soul feast', 'UDS').
card_original_type('soul feast'/'UDS', 'Sorcery').
card_original_text('soul feast'/'UDS', 'Target player loses 4 life and you gain 4 life.').
card_first_print('soul feast', 'UDS').
card_image_name('soul feast'/'UDS', 'soul feast').
card_uid('soul feast'/'UDS', 'UDS:Soul Feast:soul feast').
card_rarity('soul feast'/'UDS', 'Uncommon').
card_artist('soul feast'/'UDS', 'Ray Lago').
card_number('soul feast'/'UDS', '72').
card_flavor_text('soul feast'/'UDS', 'As no one has ever accepted a second invitation to Davvol\'s table, the evincar often dines alone.').
card_multiverse_id('soul feast'/'UDS', '15767').

card_in_set('sowing salt', 'UDS').
card_original_type('sowing salt'/'UDS', 'Sorcery').
card_original_text('sowing salt'/'UDS', 'Remove target nonbasic land from the game. Search its controller\'s graveyard, hand, and library for all copies of that card and remove them from the game. That player then shuffles his or her library.').
card_first_print('sowing salt', 'UDS').
card_image_name('sowing salt'/'UDS', 'sowing salt').
card_uid('sowing salt'/'UDS', 'UDS:Sowing Salt:sowing salt').
card_rarity('sowing salt'/'UDS', 'Uncommon').
card_artist('sowing salt'/'UDS', 'Todd Lockwood').
card_number('sowing salt'/'UDS', '97').
card_multiverse_id('sowing salt'/'UDS', '19534').

card_in_set('splinter', 'UDS').
card_original_type('splinter'/'UDS', 'Sorcery').
card_original_text('splinter'/'UDS', 'Remove target artifact from the game. Search its controller\'s graveyard, hand, and library for all copies of that card and remove them from the game. That player then shuffles his or her library.').
card_first_print('splinter', 'UDS').
card_image_name('splinter'/'UDS', 'splinter').
card_uid('splinter'/'UDS', 'UDS:Splinter:splinter').
card_rarity('splinter'/'UDS', 'Uncommon').
card_artist('splinter'/'UDS', 'Daren Bader').
card_number('splinter'/'UDS', '121').
card_multiverse_id('splinter'/'UDS', '15208').

card_in_set('squirming mass', 'UDS').
card_original_type('squirming mass'/'UDS', 'Creature — Horror').
card_original_text('squirming mass'/'UDS', 'Squirming Mass can\'t be blocked except by artifact creatures and black creatures.').
card_first_print('squirming mass', 'UDS').
card_image_name('squirming mass'/'UDS', 'squirming mass').
card_uid('squirming mass'/'UDS', 'UDS:Squirming Mass:squirming mass').
card_rarity('squirming mass'/'UDS', 'Common').
card_artist('squirming mass'/'UDS', 'Ron Spencer').
card_number('squirming mass'/'UDS', '73').
card_flavor_text('squirming mass'/'UDS', 'Only the coldest hearts and the strongest stomachs can stand against it.').
card_multiverse_id('squirming mass'/'UDS', '15170').

card_in_set('storage matrix', 'UDS').
card_original_type('storage matrix'/'UDS', 'Artifact').
card_original_text('storage matrix'/'UDS', 'As long as Storage Matrix is untapped, instead of each player untapping the permanents he or she controls during his or her untap step, that player chooses artifacts, creatures, or lands and untaps all permanents of the chosen type he or she controls.').
card_first_print('storage matrix', 'UDS').
card_image_name('storage matrix'/'UDS', 'storage matrix').
card_uid('storage matrix'/'UDS', 'UDS:Storage Matrix:storage matrix').
card_rarity('storage matrix'/'UDS', 'Rare').
card_artist('storage matrix'/'UDS', 'Patrick Ho').
card_number('storage matrix'/'UDS', '138').
card_multiverse_id('storage matrix'/'UDS', '15260').

card_in_set('taunting elf', 'UDS').
card_original_type('taunting elf'/'UDS', 'Creature — Elf').
card_original_text('taunting elf'/'UDS', 'All creatures able to block Taunting Elf do so.').
card_first_print('taunting elf', 'UDS').
card_image_name('taunting elf'/'UDS', 'taunting elf').
card_uid('taunting elf'/'UDS', 'UDS:Taunting Elf:taunting elf').
card_rarity('taunting elf'/'UDS', 'Common').
card_artist('taunting elf'/'UDS', 'Scott M. Fischer').
card_number('taunting elf'/'UDS', '122').
card_flavor_text('taunting elf'/'UDS', 'Much to Multani\'s chagrin, Rofellos gleefully tutored Yavimaya\'s elves on the rudest and most vulgar words spoken in Llanowar.').
card_multiverse_id('taunting elf'/'UDS', '15221').

card_in_set('telepathic spies', 'UDS').
card_original_type('telepathic spies'/'UDS', 'Creature — Wizard').
card_original_text('telepathic spies'/'UDS', 'When Telepathic Spies comes into play, look at target opponent\'s hand.').
card_first_print('telepathic spies', 'UDS').
card_image_name('telepathic spies'/'UDS', 'telepathic spies').
card_uid('telepathic spies'/'UDS', 'UDS:Telepathic Spies:telepathic spies').
card_rarity('telepathic spies'/'UDS', 'Common').
card_artist('telepathic spies'/'UDS', 'Thomas M. Baxa').
card_number('telepathic spies'/'UDS', '47').
card_flavor_text('telepathic spies'/'UDS', '\"Do try to keep an open mind, would you?\"').
card_multiverse_id('telepathic spies'/'UDS', '15149').

card_in_set('temporal adept', 'UDS').
card_original_type('temporal adept'/'UDS', 'Creature — Wizard').
card_original_text('temporal adept'/'UDS', '{U}{U}{U}, {T}: Return target permanent to its owner\'s hand.').
card_first_print('temporal adept', 'UDS').
card_image_name('temporal adept'/'UDS', 'temporal adept').
card_uid('temporal adept'/'UDS', 'UDS:Temporal Adept:temporal adept').
card_rarity('temporal adept'/'UDS', 'Rare').
card_artist('temporal adept'/'UDS', 'Heather Hudson').
card_number('temporal adept'/'UDS', '48').
card_flavor_text('temporal adept'/'UDS', '\"Of course she\'s at the head of her class. All her classmates have disappeared.\"\n—Gatha, Tolarian renegade').
card_multiverse_id('temporal adept'/'UDS', '15875').

card_in_set('tethered griffin', 'UDS').
card_original_type('tethered griffin'/'UDS', 'Creature — Griffin').
card_original_text('tethered griffin'/'UDS', 'Flying\nWhen you control no enchantments, sacrifice Tethered Griffin.').
card_first_print('tethered griffin', 'UDS').
card_image_name('tethered griffin'/'UDS', 'tethered griffin').
card_uid('tethered griffin'/'UDS', 'UDS:Tethered Griffin:tethered griffin').
card_rarity('tethered griffin'/'UDS', 'Rare').
card_artist('tethered griffin'/'UDS', 'Matthew D. Wilson').
card_number('tethered griffin'/'UDS', '21').
card_flavor_text('tethered griffin'/'UDS', 'Poorly trained griffins sometimes attack the noble audiences that gather to watch their progress.').
card_multiverse_id('tethered griffin'/'UDS', '15769').

card_in_set('thieving magpie', 'UDS').
card_original_type('thieving magpie'/'UDS', 'Creature — Bird').
card_original_text('thieving magpie'/'UDS', 'Flying \nWhenever Thieving Magpie deals damage to one of your opponents, you draw a card.').
card_first_print('thieving magpie', 'UDS').
card_image_name('thieving magpie'/'UDS', 'thieving magpie').
card_uid('thieving magpie'/'UDS', 'UDS:Thieving Magpie:thieving magpie').
card_rarity('thieving magpie'/'UDS', 'Uncommon').
card_artist('thieving magpie'/'UDS', 'Una Fricker').
card_number('thieving magpie'/'UDS', '49').
card_flavor_text('thieving magpie'/'UDS', 'Rayne once made several unkind comparisons between the bird\'s naked opportunism and Urza\'s.').
card_multiverse_id('thieving magpie'/'UDS', '15156').

card_in_set('thorn elemental', 'UDS').
card_original_type('thorn elemental'/'UDS', 'Creature — Elemental').
card_original_text('thorn elemental'/'UDS', 'Thorn Elemental may deal its combat damage to defending player as though it weren\'t blocked.').
card_first_print('thorn elemental', 'UDS').
card_image_name('thorn elemental'/'UDS', 'thorn elemental').
card_uid('thorn elemental'/'UDS', 'UDS:Thorn Elemental:thorn elemental').
card_rarity('thorn elemental'/'UDS', 'Rare').
card_artist('thorn elemental'/'UDS', 'rk post').
card_number('thorn elemental'/'UDS', '123').
card_flavor_text('thorn elemental'/'UDS', 'Rain from this storm leaves you pinned to the ground like an insect.').
card_multiverse_id('thorn elemental'/'UDS', '15240').

card_in_set('thran dynamo', 'UDS').
card_original_type('thran dynamo'/'UDS', 'Artifact').
card_original_text('thran dynamo'/'UDS', '{T}: Add three colorless mana to your mana pool.').
card_first_print('thran dynamo', 'UDS').
card_image_name('thran dynamo'/'UDS', 'thran dynamo').
card_uid('thran dynamo'/'UDS', 'UDS:Thran Dynamo:thran dynamo').
card_rarity('thran dynamo'/'UDS', 'Uncommon').
card_artist('thran dynamo'/'UDS', 'Ron Spears').
card_number('thran dynamo'/'UDS', '139').
card_flavor_text('thran dynamo'/'UDS', 'Urza\'s metathran children were conceived, birthed, and nurtured by an integrated system of machines.').
card_multiverse_id('thran dynamo'/'UDS', '15248').

card_in_set('thran foundry', 'UDS').
card_original_type('thran foundry'/'UDS', 'Artifact').
card_original_text('thran foundry'/'UDS', '{1}, {T}, Remove Thran Foundry from the game: Target player shuffles his or her graveyard into his or her library.').
card_first_print('thran foundry', 'UDS').
card_image_name('thran foundry'/'UDS', 'thran foundry').
card_uid('thran foundry'/'UDS', 'UDS:Thran Foundry:thran foundry').
card_rarity('thran foundry'/'UDS', 'Uncommon').
card_artist('thran foundry'/'UDS', 'John Zeleznik').
card_number('thran foundry'/'UDS', '140').
card_flavor_text('thran foundry'/'UDS', '\"What we do not use up, we use again.\"\n—Urza').
card_multiverse_id('thran foundry'/'UDS', '19111').

card_in_set('thran golem', 'UDS').
card_original_type('thran golem'/'UDS', 'Artifact Creature — Golem').
card_original_text('thran golem'/'UDS', 'As long as Thran Golem is enchanted, it gets +2/+2 and gains flying, first strike, and trample.').
card_first_print('thran golem', 'UDS').
card_image_name('thran golem'/'UDS', 'thran golem').
card_uid('thran golem'/'UDS', 'UDS:Thran Golem:thran golem').
card_rarity('thran golem'/'UDS', 'Rare').
card_artist('thran golem'/'UDS', 'Ron Spears').
card_number('thran golem'/'UDS', '141').
card_flavor_text('thran golem'/'UDS', 'Karn felt more secure about his value to Urza when he realized he didn\'t need regular trimming.').
card_multiverse_id('thran golem'/'UDS', '15254').

card_in_set('tormented angel', 'UDS').
card_original_type('tormented angel'/'UDS', 'Creature — Angel').
card_original_text('tormented angel'/'UDS', 'Flying').
card_first_print('tormented angel', 'UDS').
card_image_name('tormented angel'/'UDS', 'tormented angel').
card_uid('tormented angel'/'UDS', 'UDS:Tormented Angel:tormented angel').
card_rarity('tormented angel'/'UDS', 'Common').
card_artist('tormented angel'/'UDS', 'Greg & Tim Hildebrandt').
card_number('tormented angel'/'UDS', '22').
card_flavor_text('tormented angel'/'UDS', 'Falling from heaven is not as painful as surviving the impact.').
card_multiverse_id('tormented angel'/'UDS', '18287').

card_in_set('treachery', 'UDS').
card_original_type('treachery'/'UDS', 'Enchant Creature').
card_original_text('treachery'/'UDS', 'When Treachery comes into play, untap up to five lands.\nYou control enchanted creature.').
card_first_print('treachery', 'UDS').
card_image_name('treachery'/'UDS', 'treachery').
card_uid('treachery'/'UDS', 'UDS:Treachery:treachery').
card_rarity('treachery'/'UDS', 'Rare').
card_artist('treachery'/'UDS', 'Matthew D. Wilson').
card_number('treachery'/'UDS', '50').
card_flavor_text('treachery'/'UDS', '\"The academy educates; I employ. It\'s a perfect arrangement.\"\n—Gatha, Tolarian renegade').
card_multiverse_id('treachery'/'UDS', '12148').

card_in_set('trumpet blast', 'UDS').
card_original_type('trumpet blast'/'UDS', 'Instant').
card_original_text('trumpet blast'/'UDS', 'Attacking creatures get +2/+0 until end of turn.').
card_first_print('trumpet blast', 'UDS').
card_image_name('trumpet blast'/'UDS', 'trumpet blast').
card_uid('trumpet blast'/'UDS', 'UDS:Trumpet Blast:trumpet blast').
card_rarity('trumpet blast'/'UDS', 'Common').
card_artist('trumpet blast'/'UDS', 'Carl Critchlow').
card_number('trumpet blast'/'UDS', '98').
card_flavor_text('trumpet blast'/'UDS', 'Keldon warriors don\'t need signals to tell them when to attack, but when to stop.').
card_multiverse_id('trumpet blast'/'UDS', '15200').

card_in_set('twisted experiment', 'UDS').
card_original_type('twisted experiment'/'UDS', 'Enchant Creature').
card_original_text('twisted experiment'/'UDS', 'Enchanted creature gets +3/-1.').
card_first_print('twisted experiment', 'UDS').
card_image_name('twisted experiment'/'UDS', 'twisted experiment').
card_uid('twisted experiment'/'UDS', 'UDS:Twisted Experiment:twisted experiment').
card_rarity('twisted experiment'/'UDS', 'Common').
card_artist('twisted experiment'/'UDS', 'rk post').
card_number('twisted experiment'/'UDS', '74').
card_flavor_text('twisted experiment'/'UDS', 'Gatha showed remarkable prowess in increasing his subjects\' stature. Their lifespans, however, were another matter.').
card_multiverse_id('twisted experiment'/'UDS', '19114').

card_in_set('urza\'s incubator', 'UDS').
card_original_type('urza\'s incubator'/'UDS', 'Artifact').
card_original_text('urza\'s incubator'/'UDS', 'When Urza\'s Incubator comes into play, choose a creature type.\nCreature spells of the chosen type cost {2} less to play.').
card_first_print('urza\'s incubator', 'UDS').
card_image_name('urza\'s incubator'/'UDS', 'urza\'s incubator').
card_uid('urza\'s incubator'/'UDS', 'UDS:Urza\'s Incubator:urza\'s incubator').
card_rarity('urza\'s incubator'/'UDS', 'Rare').
card_artist('urza\'s incubator'/'UDS', 'Pete Venters').
card_number('urza\'s incubator'/'UDS', '142').
card_flavor_text('urza\'s incubator'/'UDS', '\"Stop thinking like an artificer, Urza, and start thinking like a father!\"\n—Rayne, Academy Chancellor').
card_multiverse_id('urza\'s incubator'/'UDS', '15257').

card_in_set('voice of duty', 'UDS').
card_original_type('voice of duty'/'UDS', 'Creature — Angel').
card_original_text('voice of duty'/'UDS', 'Flying, protection from green').
card_first_print('voice of duty', 'UDS').
card_image_name('voice of duty'/'UDS', 'voice of duty').
card_uid('voice of duty'/'UDS', 'UDS:Voice of Duty:voice of duty').
card_rarity('voice of duty'/'UDS', 'Uncommon').
card_artist('voice of duty'/'UDS', 'Mark Zug').
card_number('voice of duty'/'UDS', '23').
card_flavor_text('voice of duty'/'UDS', '\"Next to Law is Duty, and Duty must be obeyed. If the frame of Duty is broken, none shall weave life\'s fabric.\"\n—Song of All, canto 167').
card_multiverse_id('voice of duty'/'UDS', '15132').

card_in_set('voice of reason', 'UDS').
card_original_type('voice of reason'/'UDS', 'Creature — Angel').
card_original_text('voice of reason'/'UDS', 'Flying, protection from blue').
card_first_print('voice of reason', 'UDS').
card_image_name('voice of reason'/'UDS', 'voice of reason').
card_uid('voice of reason'/'UDS', 'UDS:Voice of Reason:voice of reason').
card_rarity('voice of reason'/'UDS', 'Uncommon').
card_artist('voice of reason'/'UDS', 'Ray Lago').
card_number('voice of reason'/'UDS', '24').
card_flavor_text('voice of reason'/'UDS', '\"Next to Grace is Reason, and Reason must be retained. If the web of Reason comes unwoven, madness will escape.\"\n—Song of All, canto 167').
card_multiverse_id('voice of reason'/'UDS', '15131').

card_in_set('wake of destruction', 'UDS').
card_original_type('wake of destruction'/'UDS', 'Sorcery').
card_original_text('wake of destruction'/'UDS', 'Destroy target land and all lands with the same name as that land.').
card_first_print('wake of destruction', 'UDS').
card_image_name('wake of destruction'/'UDS', 'wake of destruction').
card_uid('wake of destruction'/'UDS', 'UDS:Wake of Destruction:wake of destruction').
card_rarity('wake of destruction'/'UDS', 'Rare').
card_artist('wake of destruction'/'UDS', 'Todd Lockwood').
card_number('wake of destruction'/'UDS', '99').
card_flavor_text('wake of destruction'/'UDS', 'Land charred black, rivers boiled,\nCrops and wells alike despoiled,\nMountains leveled, forests felled—\nFootprints of the beasts of Keld.').
card_multiverse_id('wake of destruction'/'UDS', '15218').

card_in_set('wall of glare', 'UDS').
card_original_type('wall of glare'/'UDS', 'Creature — Wall').
card_original_text('wall of glare'/'UDS', '(Walls can\'t attack.)\nWall of Glare may block any number of creatures each combat.').
card_first_print('wall of glare', 'UDS').
card_image_name('wall of glare'/'UDS', 'wall of glare').
card_uid('wall of glare'/'UDS', 'UDS:Wall of Glare:wall of glare').
card_rarity('wall of glare'/'UDS', 'Common').
card_artist('wall of glare'/'UDS', 'Patrick Ho').
card_number('wall of glare'/'UDS', '25').
card_flavor_text('wall of glare'/'UDS', 'The blinding barrier served Benalia better than a hundred shields.').
card_multiverse_id('wall of glare'/'UDS', '15120').

card_in_set('wild colos', 'UDS').
card_original_type('wild colos'/'UDS', 'Creature — Beast').
card_original_text('wild colos'/'UDS', 'Haste (This creature may attack and {T} the turn it comes under your control.)').
card_first_print('wild colos', 'UDS').
card_image_name('wild colos'/'UDS', 'wild colos').
card_uid('wild colos'/'UDS', 'UDS:Wild Colos:wild colos').
card_rarity('wild colos'/'UDS', 'Common').
card_artist('wild colos'/'UDS', 'Marc Fishman').
card_number('wild colos'/'UDS', '100').
card_flavor_text('wild colos'/'UDS', 'You\'ll never get a Keldon\'s goat.').
card_multiverse_id('wild colos'/'UDS', '15198').

card_in_set('yavimaya elder', 'UDS').
card_original_type('yavimaya elder'/'UDS', 'Creature — Druid').
card_original_text('yavimaya elder'/'UDS', 'When Yavimaya Elder is put into a graveyard from play, you may search your library for up to two basic land cards, reveal them, and put them into your hand. If you do, shuffle your library.\n{2}, Sacrifice Yavimaya Elder: Draw a card.').
card_first_print('yavimaya elder', 'UDS').
card_image_name('yavimaya elder'/'UDS', 'yavimaya elder').
card_uid('yavimaya elder'/'UDS', 'UDS:Yavimaya Elder:yavimaya elder').
card_rarity('yavimaya elder'/'UDS', 'Common').
card_artist('yavimaya elder'/'UDS', 'Ray Lago').
card_number('yavimaya elder'/'UDS', '124').
card_multiverse_id('yavimaya elder'/'UDS', '15225').

card_in_set('yavimaya enchantress', 'UDS').
card_original_type('yavimaya enchantress'/'UDS', 'Creature — Druid').
card_original_text('yavimaya enchantress'/'UDS', 'Yavimaya Enchantress gets +1/+1 for each enchantment in play.').
card_first_print('yavimaya enchantress', 'UDS').
card_image_name('yavimaya enchantress'/'UDS', 'yavimaya enchantress').
card_uid('yavimaya enchantress'/'UDS', 'UDS:Yavimaya Enchantress:yavimaya enchantress').
card_rarity('yavimaya enchantress'/'UDS', 'Uncommon').
card_artist('yavimaya enchantress'/'UDS', 'Matthew D. Wilson').
card_number('yavimaya enchantress'/'UDS', '125').
card_flavor_text('yavimaya enchantress'/'UDS', 'From each seed, a world. From each world, a thousand seeds.').
card_multiverse_id('yavimaya enchantress'/'UDS', '15236').

card_in_set('yavimaya hollow', 'UDS').
card_original_type('yavimaya hollow'/'UDS', 'Legendary Land').
card_original_text('yavimaya hollow'/'UDS', '{T}: Add one colorless mana to your mana pool.\n{G}, {T}: Regenerate target creature.').
card_first_print('yavimaya hollow', 'UDS').
card_image_name('yavimaya hollow'/'UDS', 'yavimaya hollow').
card_uid('yavimaya hollow'/'UDS', 'UDS:Yavimaya Hollow:yavimaya hollow').
card_rarity('yavimaya hollow'/'UDS', 'Rare').
card_artist('yavimaya hollow'/'UDS', 'Douglas Shuler').
card_number('yavimaya hollow'/'UDS', '143').
card_flavor_text('yavimaya hollow'/'UDS', 'For all its traps and defenses, Yavimaya has its havens, too.').
card_multiverse_id('yavimaya hollow'/'UDS', '15772').

card_in_set('yawgmoth\'s bargain', 'UDS').
card_original_type('yawgmoth\'s bargain'/'UDS', 'Enchantment').
card_original_text('yawgmoth\'s bargain'/'UDS', 'Skip your draw step.\nPay 1 life: Draw a card.').
card_first_print('yawgmoth\'s bargain', 'UDS').
card_image_name('yawgmoth\'s bargain'/'UDS', 'yawgmoth\'s bargain').
card_uid('yawgmoth\'s bargain'/'UDS', 'UDS:Yawgmoth\'s Bargain:yawgmoth\'s bargain').
card_rarity('yawgmoth\'s bargain'/'UDS', 'Rare').
card_artist('yawgmoth\'s bargain'/'UDS', 'Michael Sutfin').
card_number('yawgmoth\'s bargain'/'UDS', '75').
card_flavor_text('yawgmoth\'s bargain'/'UDS', 'He craves only one commodity.').
card_multiverse_id('yawgmoth\'s bargain'/'UDS', '15193').
