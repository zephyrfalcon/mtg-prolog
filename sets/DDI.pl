% Duel Decks: Venser vs. Koth

set('DDI').
set_name('DDI', 'Duel Decks: Venser vs. Koth').
set_release_date('DDI', '2012-03-30').
set_border('DDI', 'black').
set_type('DDI', 'duel deck').

card_in_set('æther membrane', 'DDI').
card_original_type('æther membrane'/'DDI', 'Creature — Wall').
card_original_text('æther membrane'/'DDI', 'Defender\nReach (This creature can block creatures with flying.)\nWhenever Æther Membrane blocks a creature, return that creature to its owner\'s hand at end of combat.').
card_image_name('æther membrane'/'DDI', 'aether membrane').
card_uid('æther membrane'/'DDI', 'DDI:Æther Membrane:aether membrane').
card_rarity('æther membrane'/'DDI', 'Uncommon').
card_artist('æther membrane'/'DDI', 'Zoltan Boros & Gabor Szikszai').
card_number('æther membrane'/'DDI', '48').
card_multiverse_id('æther membrane'/'DDI', '270856').

card_in_set('angelic shield', 'DDI').
card_original_type('angelic shield'/'DDI', 'Enchantment').
card_original_text('angelic shield'/'DDI', 'Creatures you control get +0/+1.\nSacrifice Angelic Shield: Return target creature to its owner\'s hand.').
card_image_name('angelic shield'/'DDI', 'angelic shield').
card_uid('angelic shield'/'DDI', 'DDI:Angelic Shield:angelic shield').
card_rarity('angelic shield'/'DDI', 'Uncommon').
card_artist('angelic shield'/'DDI', 'Adam Rex').
card_number('angelic shield'/'DDI', '27').
card_flavor_text('angelic shield'/'DDI', '\"If only an angel\'s wings could shelter us all.\"\n—Barrin, master wizard').
card_multiverse_id('angelic shield'/'DDI', '284487').

card_in_set('anger', 'DDI').
card_original_type('anger'/'DDI', 'Creature — Incarnation').
card_original_text('anger'/'DDI', 'Haste\nAs long as Anger is in your graveyard and you control a Mountain, creatures you control have haste.').
card_image_name('anger'/'DDI', 'anger').
card_uid('anger'/'DDI', 'DDI:Anger:anger').
card_rarity('anger'/'DDI', 'Uncommon').
card_artist('anger'/'DDI', 'Svetlin Velinov').
card_number('anger'/'DDI', '51').
card_flavor_text('anger'/'DDI', '\"Molten earth has no anger of its own. Only by infusing it with your own rage can the magma seethe and take shape.\"\n—Koth of the Hammer').
card_multiverse_id('anger'/'DDI', '266061').

card_in_set('armillary sphere', 'DDI').
card_original_type('armillary sphere'/'DDI', 'Artifact').
card_original_text('armillary sphere'/'DDI', '{2}, {T}, Sacrifice Armillary Sphere: Search your library for up to two basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('armillary sphere'/'DDI', 'armillary sphere').
card_uid('armillary sphere'/'DDI', 'DDI:Armillary Sphere:armillary sphere').
card_rarity('armillary sphere'/'DDI', 'Common').
card_artist('armillary sphere'/'DDI', 'Franz Vohwinkel').
card_number('armillary sphere'/'DDI', '64').
card_flavor_text('armillary sphere'/'DDI', 'The mysterious purpose of two of the rings had eluded Esper mages—until now.').
card_multiverse_id('armillary sphere'/'DDI', '270825').

card_in_set('augury owl', 'DDI').
card_original_type('augury owl'/'DDI', 'Creature — Bird').
card_original_text('augury owl'/'DDI', 'Flying\nWhen Augury Owl enters the battlefield, scry 3. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('augury owl'/'DDI', 'augury owl').
card_uid('augury owl'/'DDI', 'DDI:Augury Owl:augury owl').
card_rarity('augury owl'/'DDI', 'Common').
card_artist('augury owl'/'DDI', 'Jim Nelson').
card_number('augury owl'/'DDI', '3').
card_multiverse_id('augury owl'/'DDI', '270841').

card_in_set('azorius chancery', 'DDI').
card_original_type('azorius chancery'/'DDI', 'Land').
card_original_text('azorius chancery'/'DDI', 'Azorius Chancery enters the battlefield tapped.\nWhen Azorius Chancery enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {W}{U} to your mana pool.').
card_image_name('azorius chancery'/'DDI', 'azorius chancery').
card_uid('azorius chancery'/'DDI', 'DDI:Azorius Chancery:azorius chancery').
card_rarity('azorius chancery'/'DDI', 'Common').
card_artist('azorius chancery'/'DDI', 'John Avon').
card_number('azorius chancery'/'DDI', '33').
card_multiverse_id('azorius chancery'/'DDI', '270826').

card_in_set('bloodfire colossus', 'DDI').
card_original_type('bloodfire colossus'/'DDI', 'Creature — Giant').
card_original_text('bloodfire colossus'/'DDI', '{R}, Sacrifice Bloodfire Colossus: Bloodfire Colossus deals 6 damage to each creature and each player.').
card_image_name('bloodfire colossus'/'DDI', 'bloodfire colossus').
card_uid('bloodfire colossus'/'DDI', 'DDI:Bloodfire Colossus:bloodfire colossus').
card_rarity('bloodfire colossus'/'DDI', 'Rare').
card_artist('bloodfire colossus'/'DDI', 'Greg Staples').
card_number('bloodfire colossus'/'DDI', '62').
card_flavor_text('bloodfire colossus'/'DDI', 'It took all its strength to contain the fire within.').
card_multiverse_id('bloodfire colossus'/'DDI', '270805').

card_in_set('bloodfire kavu', 'DDI').
card_original_type('bloodfire kavu'/'DDI', 'Creature — Kavu').
card_original_text('bloodfire kavu'/'DDI', '{R}, Sacrifice Bloodfire Kavu: Bloodfire Kavu deals 2 damage to each creature.').
card_image_name('bloodfire kavu'/'DDI', 'bloodfire kavu').
card_uid('bloodfire kavu'/'DDI', 'DDI:Bloodfire Kavu:bloodfire kavu').
card_rarity('bloodfire kavu'/'DDI', 'Uncommon').
card_artist('bloodfire kavu'/'DDI', 'Greg Staples').
card_number('bloodfire kavu'/'DDI', '54').
card_flavor_text('bloodfire kavu'/'DDI', 'Heart of fire, blood of lava, teeth of stone.').
card_multiverse_id('bloodfire kavu'/'DDI', '270812').

card_in_set('cache raiders', 'DDI').
card_original_type('cache raiders'/'DDI', 'Creature — Merfolk Rogue').
card_original_text('cache raiders'/'DDI', 'At the beginning of your upkeep, return a permanent you control to its owner\'s hand.').
card_image_name('cache raiders'/'DDI', 'cache raiders').
card_uid('cache raiders'/'DDI', 'DDI:Cache Raiders:cache raiders').
card_rarity('cache raiders'/'DDI', 'Uncommon').
card_artist('cache raiders'/'DDI', 'Pete Venters').
card_number('cache raiders'/'DDI', '18').
card_flavor_text('cache raiders'/'DDI', 'What the current doesn\'t take, they do.').
card_multiverse_id('cache raiders'/'DDI', '282599').

card_in_set('chartooth cougar', 'DDI').
card_original_type('chartooth cougar'/'DDI', 'Creature — Cat Beast').
card_original_text('chartooth cougar'/'DDI', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card: Search your library for a Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('chartooth cougar'/'DDI', 'chartooth cougar').
card_uid('chartooth cougar'/'DDI', 'DDI:Chartooth Cougar:chartooth cougar').
card_rarity('chartooth cougar'/'DDI', 'Common').
card_artist('chartooth cougar'/'DDI', 'Chase Stone').
card_number('chartooth cougar'/'DDI', '59').
card_multiverse_id('chartooth cougar'/'DDI', '270828').

card_in_set('clone', 'DDI').
card_original_type('clone'/'DDI', 'Creature — Shapeshifter').
card_original_text('clone'/'DDI', 'You may have Clone enter the battlefield as a copy of any creature on the battlefield.').
card_image_name('clone'/'DDI', 'clone').
card_uid('clone'/'DDI', 'DDI:Clone:clone').
card_rarity('clone'/'DDI', 'Rare').
card_artist('clone'/'DDI', 'Kev Walker').
card_number('clone'/'DDI', '14').
card_flavor_text('clone'/'DDI', 'The shapeshifter mimics with a twin\'s esteem and a mirror\'s cruelty.').
card_multiverse_id('clone'/'DDI', '270842').

card_in_set('coral fighters', 'DDI').
card_original_type('coral fighters'/'DDI', 'Creature — Merfolk Soldier').
card_original_text('coral fighters'/'DDI', 'Whenever Coral Fighters attacks and isn\'t blocked, look at the top card of defending player\'s library. You may put that card on the bottom of that player\'s library.').
card_image_name('coral fighters'/'DDI', 'coral fighters').
card_uid('coral fighters'/'DDI', 'DDI:Coral Fighters:coral fighters').
card_rarity('coral fighters'/'DDI', 'Uncommon').
card_artist('coral fighters'/'DDI', 'Steve Luke').
card_number('coral fighters'/'DDI', '4').
card_multiverse_id('coral fighters'/'DDI', '282600').

card_in_set('cosi\'s ravager', 'DDI').
card_original_type('cosi\'s ravager'/'DDI', 'Creature — Elemental').
card_original_text('cosi\'s ravager'/'DDI', 'Landfall — Whenever a land enters the battlefield under your control, you may have Cosi\'s Ravager deal 1 damage to target player.').
card_image_name('cosi\'s ravager'/'DDI', 'cosi\'s ravager').
card_uid('cosi\'s ravager'/'DDI', 'DDI:Cosi\'s Ravager:cosi\'s ravager').
card_rarity('cosi\'s ravager'/'DDI', 'Common').
card_artist('cosi\'s ravager'/'DDI', 'Zoltan Boros & Gabor Szikszai').
card_number('cosi\'s ravager'/'DDI', '52').
card_flavor_text('cosi\'s ravager'/'DDI', '\"Cosi has many followers. Even the volcano finds a way to do his bidding.\"\n—Zakken, Enclave cleric').
card_multiverse_id('cosi\'s ravager'/'DDI', '270871').

card_in_set('cryptic annelid', 'DDI').
card_original_type('cryptic annelid'/'DDI', 'Creature — Worm Beast').
card_original_text('cryptic annelid'/'DDI', 'When Cryptic Annelid enters the battlefield, scry 1, then scry 2, then scry 3. (To scry X, look at the top X cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('cryptic annelid'/'DDI', 'cryptic annelid').
card_uid('cryptic annelid'/'DDI', 'DDI:Cryptic Annelid:cryptic annelid').
card_rarity('cryptic annelid'/'DDI', 'Uncommon').
card_artist('cryptic annelid'/'DDI', 'Anthony S. Waters').
card_number('cryptic annelid'/'DDI', '15').
card_multiverse_id('cryptic annelid'/'DDI', '270836').

card_in_set('downhill charge', 'DDI').
card_original_type('downhill charge'/'DDI', 'Instant').
card_original_text('downhill charge'/'DDI', 'You may sacrifice a Mountain rather than pay Downhill Charge\'s mana cost.\nTarget creature gets +X/+0 until end of turn, where X is the number of Mountains you control.').
card_image_name('downhill charge'/'DDI', 'downhill charge').
card_uid('downhill charge'/'DDI', 'DDI:Downhill Charge:downhill charge').
card_rarity('downhill charge'/'DDI', 'Common').
card_artist('downhill charge'/'DDI', 'Greg & Tim Hildebrandt').
card_number('downhill charge'/'DDI', '69').
card_multiverse_id('downhill charge'/'DDI', '270854').

card_in_set('earth servant', 'DDI').
card_original_type('earth servant'/'DDI', 'Creature — Elemental').
card_original_text('earth servant'/'DDI', 'Earth Servant gets +0/+1 for each Mountain you control.').
card_image_name('earth servant'/'DDI', 'earth servant').
card_uid('earth servant'/'DDI', 'DDI:Earth Servant:earth servant').
card_rarity('earth servant'/'DDI', 'Uncommon').
card_artist('earth servant'/'DDI', 'Lucas Graciano').
card_number('earth servant'/'DDI', '60').
card_flavor_text('earth servant'/'DDI', '\"Fire. Air. Water. These elements spread, filling in the spaces they are not. But earth is implacable like a sullen child.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('earth servant'/'DDI', '270843').

card_in_set('fiery hellhound', 'DDI').
card_original_type('fiery hellhound'/'DDI', 'Creature — Elemental Hound').
card_original_text('fiery hellhound'/'DDI', '{R}: Fiery Hellhound gets +1/+0 until end of turn.').
card_image_name('fiery hellhound'/'DDI', 'fiery hellhound').
card_uid('fiery hellhound'/'DDI', 'DDI:Fiery Hellhound:fiery hellhound').
card_rarity('fiery hellhound'/'DDI', 'Common').
card_artist('fiery hellhound'/'DDI', 'Ted Galaday').
card_number('fiery hellhound'/'DDI', '49').
card_flavor_text('fiery hellhound'/'DDI', '\"I had hoped to instill in it the loyalty of a guard dog, but with fire\'s power comes its unpredictability.\"\n—Maggath, Sardian elementalist').
card_multiverse_id('fiery hellhound'/'DDI', '279855').

card_in_set('flood plain', 'DDI').
card_original_type('flood plain'/'DDI', 'Land').
card_original_text('flood plain'/'DDI', 'Flood Plain enters the battlefield tapped.\n{T}, Sacrifice Flood Plain: Search your library for a Plains or Island card and put it onto the battlefield. Then shuffle your library.').
card_image_name('flood plain'/'DDI', 'flood plain').
card_uid('flood plain'/'DDI', 'DDI:Flood Plain:flood plain').
card_rarity('flood plain'/'DDI', 'Uncommon').
card_artist('flood plain'/'DDI', 'Pat Morrissey').
card_number('flood plain'/'DDI', '34').
card_multiverse_id('flood plain'/'DDI', '270849').

card_in_set('galepowder mage', 'DDI').
card_original_type('galepowder mage'/'DDI', 'Creature — Kithkin Wizard').
card_original_text('galepowder mage'/'DDI', 'Flying\nWhenever Galepowder Mage attacks, exile another target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('galepowder mage'/'DDI', 'galepowder mage').
card_uid('galepowder mage'/'DDI', 'DDI:Galepowder Mage:galepowder mage').
card_rarity('galepowder mage'/'DDI', 'Rare').
card_artist('galepowder mage'/'DDI', 'Jeremy Jarvis').
card_number('galepowder mage'/'DDI', '12').
card_multiverse_id('galepowder mage'/'DDI', '270839').

card_in_set('geyser glider', 'DDI').
card_original_type('geyser glider'/'DDI', 'Creature — Elemental Beast').
card_original_text('geyser glider'/'DDI', 'Landfall — Whenever a land enters the battlefield under your control, Geyser Glider gains flying until end of turn.').
card_image_name('geyser glider'/'DDI', 'geyser glider').
card_uid('geyser glider'/'DDI', 'DDI:Geyser Glider:geyser glider').
card_rarity('geyser glider'/'DDI', 'Uncommon').
card_artist('geyser glider'/'DDI', 'Warren Mahy').
card_number('geyser glider'/'DDI', '56').
card_flavor_text('geyser glider'/'DDI', '\"Quit pontificating, mage. The last thing we want to do is give it more hot air.\"\n—Tala Vertan, Makindi shieldmate').
card_multiverse_id('geyser glider'/'DDI', '270874').

card_in_set('greater stone spirit', 'DDI').
card_original_type('greater stone spirit'/'DDI', 'Creature — Elemental Spirit').
card_original_text('greater stone spirit'/'DDI', 'Greater Stone Spirit can\'t be blocked by creatures with flying.\n{2}{R}: Until end of turn, target creature gets +0/+2 and gains \"{R}: This creature gets +1/+0 until end of turn.\"').
card_image_name('greater stone spirit'/'DDI', 'greater stone spirit').
card_uid('greater stone spirit'/'DDI', 'DDI:Greater Stone Spirit:greater stone spirit').
card_rarity('greater stone spirit'/'DDI', 'Uncommon').
card_artist('greater stone spirit'/'DDI', 'Yokota Katsumi').
card_number('greater stone spirit'/'DDI', '61').
card_flavor_text('greater stone spirit'/'DDI', 'Having charted their way up the difficult face, the two mountaineers had to contend with the mountain\'s decision to stand up.').
card_multiverse_id('greater stone spirit'/'DDI', '279857').

card_in_set('island', 'DDI').
card_original_type('island'/'DDI', 'Basic Land — Island').
card_original_text('island'/'DDI', 'U').
card_image_name('island'/'DDI', 'island1').
card_uid('island'/'DDI', 'DDI:Island:island1').
card_rarity('island'/'DDI', 'Basic Land').
card_artist('island'/'DDI', 'Rob Alexander').
card_number('island'/'DDI', '41').
card_multiverse_id('island'/'DDI', '284491').

card_in_set('island', 'DDI').
card_original_type('island'/'DDI', 'Basic Land — Island').
card_original_text('island'/'DDI', 'U').
card_image_name('island'/'DDI', 'island2').
card_uid('island'/'DDI', 'DDI:Island:island2').
card_rarity('island'/'DDI', 'Basic Land').
card_artist('island'/'DDI', 'Jung Park').
card_number('island'/'DDI', '42').
card_multiverse_id('island'/'DDI', '284490').

card_in_set('island', 'DDI').
card_original_type('island'/'DDI', 'Basic Land — Island').
card_original_text('island'/'DDI', 'U').
card_image_name('island'/'DDI', 'island3').
card_uid('island'/'DDI', 'DDI:Island:island3').
card_rarity('island'/'DDI', 'Basic Land').
card_artist('island'/'DDI', 'Martina Pilcerova').
card_number('island'/'DDI', '43').
card_multiverse_id('island'/'DDI', '284492').

card_in_set('jaws of stone', 'DDI').
card_original_type('jaws of stone'/'DDI', 'Sorcery').
card_original_text('jaws of stone'/'DDI', 'Jaws of Stone deals X damage divided as you choose among any number of target creatures and/or players, where X is the number of Mountains you control as you cast Jaws of Stone.').
card_image_name('jaws of stone'/'DDI', 'jaws of stone').
card_uid('jaws of stone'/'DDI', 'DDI:Jaws of Stone:jaws of stone').
card_rarity('jaws of stone'/'DDI', 'Uncommon').
card_artist('jaws of stone'/'DDI', 'Zoltan Boros & Gabor Szikszai').
card_number('jaws of stone'/'DDI', '72').
card_flavor_text('jaws of stone'/'DDI', 'When giants find their mountain homes infested, they have a whole range of solutions.').
card_multiverse_id('jaws of stone'/'DDI', '270833').

card_in_set('jedit\'s dragoons', 'DDI').
card_original_type('jedit\'s dragoons'/'DDI', 'Creature — Cat Soldier').
card_original_text('jedit\'s dragoons'/'DDI', 'Vigilance\nWhen Jedit\'s Dragoons enters the battlefield, you gain 4 life.').
card_image_name('jedit\'s dragoons'/'DDI', 'jedit\'s dragoons').
card_uid('jedit\'s dragoons'/'DDI', 'DDI:Jedit\'s Dragoons:jedit\'s dragoons').
card_rarity('jedit\'s dragoons'/'DDI', 'Common').
card_artist('jedit\'s dragoons'/'DDI', 'John Matson').
card_number('jedit\'s dragoons'/'DDI', '20').
card_flavor_text('jedit\'s dragoons'/'DDI', 'After Efrava was destroyed, the cat warriors scattered across Dominaria. Those who followed Jedit\'s example were strong enough to survive the ravages of apocalypse.').
card_multiverse_id('jedit\'s dragoons'/'DDI', '282601').

card_in_set('journeyer\'s kite', 'DDI').
card_original_type('journeyer\'s kite'/'DDI', 'Artifact').
card_original_text('journeyer\'s kite'/'DDI', '{3}, {T}: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_image_name('journeyer\'s kite'/'DDI', 'journeyer\'s kite').
card_uid('journeyer\'s kite'/'DDI', 'DDI:Journeyer\'s Kite:journeyer\'s kite').
card_rarity('journeyer\'s kite'/'DDI', 'Rare').
card_artist('journeyer\'s kite'/'DDI', 'Hiro Izawa').
card_number('journeyer\'s kite'/'DDI', '65').
card_flavor_text('journeyer\'s kite'/'DDI', '\"From the clouds, you can see as far as the distant horizon. It\'s a reminder of the infinite possibilities of everyday life.\"\n—Noboru, master kitemaker').
card_multiverse_id('journeyer\'s kite'/'DDI', '282591').

card_in_set('kor cartographer', 'DDI').
card_original_type('kor cartographer'/'DDI', 'Creature — Kor Scout').
card_original_text('kor cartographer'/'DDI', 'When Kor Cartographer enters the battlefield, you may search your library for a Plains card, put it onto the battlefield tapped, then shuffle your library.').
card_image_name('kor cartographer'/'DDI', 'kor cartographer').
card_uid('kor cartographer'/'DDI', 'DDI:Kor Cartographer:kor cartographer').
card_rarity('kor cartographer'/'DDI', 'Common').
card_artist('kor cartographer'/'DDI', 'Ryan Pancoast').
card_number('kor cartographer'/'DDI', '13').
card_flavor_text('kor cartographer'/'DDI', 'Kor have no concept of exploration. They return to homelands forgotten.').
card_multiverse_id('kor cartographer'/'DDI', '270875').

card_in_set('koth of the hammer', 'DDI').
card_original_type('koth of the hammer'/'DDI', 'Planeswalker — Koth').
card_original_text('koth of the hammer'/'DDI', '+1: Untap target Mountain. It becomes a 4/4 red Elemental creature until end of turn. It\'s still a land.\n-2: Add {R} to your mana pool for each Mountain you control.\n-5: You get an emblem with \"Mountains you control have ‘{T}: This land deals 1 damage to target creature or player.\'\"').
card_image_name('koth of the hammer'/'DDI', 'koth of the hammer').
card_uid('koth of the hammer'/'DDI', 'DDI:Koth of the Hammer:koth of the hammer').
card_rarity('koth of the hammer'/'DDI', 'Mythic Rare').
card_artist('koth of the hammer'/'DDI', 'Eric Deschamps').
card_number('koth of the hammer'/'DDI', '44').
card_multiverse_id('koth of the hammer'/'DDI', '266362').

card_in_set('lithophage', 'DDI').
card_original_type('lithophage'/'DDI', 'Creature — Insect').
card_original_text('lithophage'/'DDI', 'At the beginning of your upkeep, sacrifice Lithophage unless you sacrifice a Mountain.').
card_image_name('lithophage'/'DDI', 'lithophage').
card_uid('lithophage'/'DDI', 'DDI:Lithophage:lithophage').
card_rarity('lithophage'/'DDI', 'Rare').
card_artist('lithophage'/'DDI', 'Mike Ploog').
card_number('lithophage'/'DDI', '57').
card_flavor_text('lithophage'/'DDI', 'It\'s viciously territorial, destroying every creature on the mountain it has chosen to consume.').
card_multiverse_id('lithophage'/'DDI', '270850').

card_in_set('minamo sightbender', 'DDI').
card_original_type('minamo sightbender'/'DDI', 'Creature — Human Wizard').
card_original_text('minamo sightbender'/'DDI', '{X}, {T}: Target creature with power X or less is unblockable this turn.').
card_image_name('minamo sightbender'/'DDI', 'minamo sightbender').
card_uid('minamo sightbender'/'DDI', 'DDI:Minamo Sightbender:minamo sightbender').
card_rarity('minamo sightbender'/'DDI', 'Uncommon').
card_artist('minamo sightbender'/'DDI', 'Luca Zontini').
card_number('minamo sightbender'/'DDI', '5').
card_flavor_text('minamo sightbender'/'DDI', 'Woven from threads of the spirit world, the mistcloak rendered its wearer invisible, but quickly fell to tatters.').
card_multiverse_id('minamo sightbender'/'DDI', '282602').

card_in_set('mistmeadow witch', 'DDI').
card_original_type('mistmeadow witch'/'DDI', 'Creature — Kithkin Wizard').
card_original_text('mistmeadow witch'/'DDI', '{2}{W}{U}: Exile target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('mistmeadow witch'/'DDI', 'mistmeadow witch').
card_uid('mistmeadow witch'/'DDI', 'DDI:Mistmeadow Witch:mistmeadow witch').
card_rarity('mistmeadow witch'/'DDI', 'Uncommon').
card_artist('mistmeadow witch'/'DDI', 'Greg Staples').
card_number('mistmeadow witch'/'DDI', '6').
card_flavor_text('mistmeadow witch'/'DDI', 'Olka collected the evening mist for years, studying its secrets. Once she learned its essence, she could vanish with a thought.').
card_multiverse_id('mistmeadow witch'/'DDI', '282603').

card_in_set('mountain', 'DDI').
card_original_type('mountain'/'DDI', 'Basic Land — Mountain').
card_original_text('mountain'/'DDI', 'R').
card_image_name('mountain'/'DDI', 'mountain1').
card_uid('mountain'/'DDI', 'DDI:Mountain:mountain1').
card_rarity('mountain'/'DDI', 'Basic Land').
card_artist('mountain'/'DDI', 'Tomasz Jedruszek').
card_number('mountain'/'DDI', '74').
card_multiverse_id('mountain'/'DDI', '284494').

card_in_set('mountain', 'DDI').
card_original_type('mountain'/'DDI', 'Basic Land — Mountain').
card_original_text('mountain'/'DDI', 'R').
card_image_name('mountain'/'DDI', 'mountain2').
card_uid('mountain'/'DDI', 'DDI:Mountain:mountain2').
card_rarity('mountain'/'DDI', 'Basic Land').
card_artist('mountain'/'DDI', 'Tomasz Jedruszek').
card_number('mountain'/'DDI', '75').
card_multiverse_id('mountain'/'DDI', '284496').

card_in_set('mountain', 'DDI').
card_original_type('mountain'/'DDI', 'Basic Land — Mountain').
card_original_text('mountain'/'DDI', 'R').
card_image_name('mountain'/'DDI', 'mountain3').
card_uid('mountain'/'DDI', 'DDI:Mountain:mountain3').
card_rarity('mountain'/'DDI', 'Basic Land').
card_artist('mountain'/'DDI', 'Martina Pilcerova').
card_number('mountain'/'DDI', '76').
card_multiverse_id('mountain'/'DDI', '284493').

card_in_set('mountain', 'DDI').
card_original_type('mountain'/'DDI', 'Basic Land — Mountain').
card_original_text('mountain'/'DDI', 'R').
card_image_name('mountain'/'DDI', 'mountain4').
card_uid('mountain'/'DDI', 'DDI:Mountain:mountain4').
card_rarity('mountain'/'DDI', 'Basic Land').
card_artist('mountain'/'DDI', 'Mark Tedin').
card_number('mountain'/'DDI', '77').
card_multiverse_id('mountain'/'DDI', '284495').

card_in_set('neurok invisimancer', 'DDI').
card_original_type('neurok invisimancer'/'DDI', 'Creature — Human Wizard').
card_original_text('neurok invisimancer'/'DDI', 'Neurok Invisimancer is unblockable.\nWhen Neurok Invisimancer enters the battlefield, target creature is unblockable this turn.').
card_image_name('neurok invisimancer'/'DDI', 'neurok invisimancer').
card_uid('neurok invisimancer'/'DDI', 'DDI:Neurok Invisimancer:neurok invisimancer').
card_rarity('neurok invisimancer'/'DDI', 'Common').
card_artist('neurok invisimancer'/'DDI', 'Izzy').
card_number('neurok invisimancer'/'DDI', '8').
card_flavor_text('neurok invisimancer'/'DDI', '\"They won\'t see your shadow or hear your breath, but they will feel your blade.\"').
card_multiverse_id('neurok invisimancer'/'DDI', '270868').

card_in_set('new benalia', 'DDI').
card_original_type('new benalia'/'DDI', 'Land').
card_original_text('new benalia'/'DDI', 'New Benalia enters the battlefield tapped.\nWhen New Benalia enters the battlefield, scry 1. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)\n{T}: Add {W} to your mana pool.').
card_image_name('new benalia'/'DDI', 'new benalia').
card_uid('new benalia'/'DDI', 'DDI:New Benalia:new benalia').
card_rarity('new benalia'/'DDI', 'Uncommon').
card_artist('new benalia'/'DDI', 'Richard Wright').
card_number('new benalia'/'DDI', '35').
card_multiverse_id('new benalia'/'DDI', '270837').

card_in_set('oblivion ring', 'DDI').
card_original_type('oblivion ring'/'DDI', 'Enchantment').
card_original_text('oblivion ring'/'DDI', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'DDI', 'oblivion ring').
card_uid('oblivion ring'/'DDI', 'DDI:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'DDI', 'Uncommon').
card_artist('oblivion ring'/'DDI', 'Franz Vohwinkel').
card_number('oblivion ring'/'DDI', '28').
card_multiverse_id('oblivion ring'/'DDI', '270848').

card_in_set('overrule', 'DDI').
card_original_type('overrule'/'DDI', 'Instant').
card_original_text('overrule'/'DDI', 'Counter target spell unless its controller pays {X}. You gain X life.').
card_image_name('overrule'/'DDI', 'overrule').
card_uid('overrule'/'DDI', 'DDI:Overrule:overrule').
card_rarity('overrule'/'DDI', 'Common').
card_artist('overrule'/'DDI', 'Alan Pollack').
card_number('overrule'/'DDI', '32').
card_flavor_text('overrule'/'DDI', 'With one commanding word, the spell was put down and a fine collected for its casting.').
card_multiverse_id('overrule'/'DDI', '270834').

card_in_set('path to exile', 'DDI').
card_original_type('path to exile'/'DDI', 'Instant').
card_original_text('path to exile'/'DDI', 'Exile target creature. Its controller may search his or her library for a basic land card, put that card onto the battlefield tapped, then shuffle his or her library.').
card_image_name('path to exile'/'DDI', 'path to exile').
card_uid('path to exile'/'DDI', 'DDI:Path to Exile:path to exile').
card_rarity('path to exile'/'DDI', 'Uncommon').
card_artist('path to exile'/'DDI', 'Erica Yang').
card_number('path to exile'/'DDI', '23').
card_multiverse_id('path to exile'/'DDI', '266017').

card_in_set('pilgrim\'s eye', 'DDI').
card_original_type('pilgrim\'s eye'/'DDI', 'Artifact Creature — Thopter').
card_original_text('pilgrim\'s eye'/'DDI', 'Flying\nWhen Pilgrim\'s Eye enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('pilgrim\'s eye'/'DDI', 'pilgrim\'s eye').
card_uid('pilgrim\'s eye'/'DDI', 'DDI:Pilgrim\'s Eye:pilgrim\'s eye').
card_rarity('pilgrim\'s eye'/'DDI', 'Common').
card_artist('pilgrim\'s eye'/'DDI', 'Dan Scott').
card_number('pilgrim\'s eye'/'DDI', '47').
card_flavor_text('pilgrim\'s eye'/'DDI', 'The kor send their thopter kites to see if the land is in a welcoming mood.').
card_multiverse_id('pilgrim\'s eye'/'DDI', '270872').

card_in_set('plains', 'DDI').
card_original_type('plains'/'DDI', 'Basic Land — Plains').
card_original_text('plains'/'DDI', 'W').
card_image_name('plains'/'DDI', 'plains1').
card_uid('plains'/'DDI', 'DDI:Plains:plains1').
card_rarity('plains'/'DDI', 'Basic Land').
card_artist('plains'/'DDI', 'John Avon').
card_number('plains'/'DDI', '38').
card_multiverse_id('plains'/'DDI', '284497').

card_in_set('plains', 'DDI').
card_original_type('plains'/'DDI', 'Basic Land — Plains').
card_original_text('plains'/'DDI', 'W').
card_image_name('plains'/'DDI', 'plains2').
card_uid('plains'/'DDI', 'DDI:Plains:plains2').
card_rarity('plains'/'DDI', 'Basic Land').
card_artist('plains'/'DDI', 'James Paick').
card_number('plains'/'DDI', '39').
card_multiverse_id('plains'/'DDI', '284499').

card_in_set('plains', 'DDI').
card_original_type('plains'/'DDI', 'Basic Land — Plains').
card_original_text('plains'/'DDI', 'W').
card_image_name('plains'/'DDI', 'plains3').
card_uid('plains'/'DDI', 'DDI:Plains:plains3').
card_rarity('plains'/'DDI', 'Basic Land').
card_artist('plains'/'DDI', 'Martina Pilcerova').
card_number('plains'/'DDI', '40').
card_multiverse_id('plains'/'DDI', '284498').

card_in_set('plated geopede', 'DDI').
card_original_type('plated geopede'/'DDI', 'Creature — Insect').
card_original_text('plated geopede'/'DDI', 'First strike\nLandfall — Whenever a land enters the battlefield under your control, Plated Geopede gets +2/+2 until end of turn.').
card_image_name('plated geopede'/'DDI', 'plated geopede').
card_uid('plated geopede'/'DDI', 'DDI:Plated Geopede:plated geopede').
card_rarity('plated geopede'/'DDI', 'Common').
card_artist('plated geopede'/'DDI', 'Johann Bodin').
card_number('plated geopede'/'DDI', '45').
card_flavor_text('plated geopede'/'DDI', 'It prefers a cooked meal, so it will eat you now and lava-bask later.').
card_multiverse_id('plated geopede'/'DDI', '266210').

card_in_set('preordain', 'DDI').
card_original_type('preordain'/'DDI', 'Sorcery').
card_original_text('preordain'/'DDI', 'Scry 2, then draw a card. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('preordain'/'DDI', 'preordain').
card_uid('preordain'/'DDI', 'DDI:Preordain:preordain').
card_rarity('preordain'/'DDI', 'Common').
card_artist('preordain'/'DDI', 'Scott Chou').
card_number('preordain'/'DDI', '24').
card_multiverse_id('preordain'/'DDI', '265979').

card_in_set('primal plasma', 'DDI').
card_original_type('primal plasma'/'DDI', 'Creature — Elemental Shapeshifter').
card_original_text('primal plasma'/'DDI', 'As Primal Plasma enters the battlefield, it becomes your choice of a 3/3 creature, a 2/2 creature with flying, or a 1/6 creature with defender.').
card_image_name('primal plasma'/'DDI', 'primal plasma').
card_uid('primal plasma'/'DDI', 'DDI:Primal Plasma:primal plasma').
card_rarity('primal plasma'/'DDI', 'Common').
card_artist('primal plasma'/'DDI', 'Luca Zontini').
card_number('primal plasma'/'DDI', '16').
card_flavor_text('primal plasma'/'DDI', 'Tocasia brushed the gears and cogs from the table. There, before two wide-eyed brothers, she began a lesson on raw elemental magic.').
card_multiverse_id('primal plasma'/'DDI', '270857').

card_in_set('pygmy pyrosaur', 'DDI').
card_original_type('pygmy pyrosaur'/'DDI', 'Creature — Lizard').
card_original_text('pygmy pyrosaur'/'DDI', 'Pygmy Pyrosaur can\'t block.\n{R}: Pygmy Pyrosaur gets +1/+0 until end of turn.').
card_image_name('pygmy pyrosaur'/'DDI', 'pygmy pyrosaur').
card_uid('pygmy pyrosaur'/'DDI', 'DDI:Pygmy Pyrosaur:pygmy pyrosaur').
card_rarity('pygmy pyrosaur'/'DDI', 'Common').
card_artist('pygmy pyrosaur'/'DDI', 'Dan Frazier').
card_number('pygmy pyrosaur'/'DDI', '46').
card_flavor_text('pygmy pyrosaur'/'DDI', 'Do not judge a lizard by its size.\n—Ghitu proverb').
card_multiverse_id('pygmy pyrosaur'/'DDI', '279859').

card_in_set('revoke existence', 'DDI').
card_original_type('revoke existence'/'DDI', 'Sorcery').
card_original_text('revoke existence'/'DDI', 'Exile target artifact or enchantment.').
card_image_name('revoke existence'/'DDI', 'revoke existence').
card_uid('revoke existence'/'DDI', 'DDI:Revoke Existence:revoke existence').
card_rarity('revoke existence'/'DDI', 'Common').
card_artist('revoke existence'/'DDI', 'Allen Williams').
card_number('revoke existence'/'DDI', '26').
card_flavor_text('revoke existence'/'DDI', '\"No half measures, no regrets. We\'ll tell no stories of this day. It will be as if it never existed at all.\"\n—Ganedor, loxodon mystic').
card_multiverse_id('revoke existence'/'DDI', '284488').

card_in_set('safe passage', 'DDI').
card_original_type('safe passage'/'DDI', 'Instant').
card_original_text('safe passage'/'DDI', 'Prevent all damage that would be dealt to you and creatures you control this turn.').
card_image_name('safe passage'/'DDI', 'safe passage').
card_uid('safe passage'/'DDI', 'DDI:Safe Passage:safe passage').
card_rarity('safe passage'/'DDI', 'Common').
card_artist('safe passage'/'DDI', 'Christopher Moeller').
card_number('safe passage'/'DDI', '29').
card_flavor_text('safe passage'/'DDI', 'With one flap of her wings, the angel beat back the fires of war.').
card_multiverse_id('safe passage'/'DDI', '270844').

card_in_set('sawtooth loon', 'DDI').
card_original_type('sawtooth loon'/'DDI', 'Creature — Bird').
card_original_text('sawtooth loon'/'DDI', 'Flying\nWhen Sawtooth Loon enters the battlefield, return a white or blue creature you control to its owner\'s hand.\nWhen Sawtooth Loon enters the battlefield, draw two cards, then put two cards from your hand on the bottom of your library.').
card_image_name('sawtooth loon'/'DDI', 'sawtooth loon').
card_uid('sawtooth loon'/'DDI', 'DDI:Sawtooth Loon:sawtooth loon').
card_rarity('sawtooth loon'/'DDI', 'Uncommon').
card_artist('sawtooth loon'/'DDI', 'Heather Hudson').
card_number('sawtooth loon'/'DDI', '17').
card_multiverse_id('sawtooth loon'/'DDI', '270861').

card_in_set('scroll thief', 'DDI').
card_original_type('scroll thief'/'DDI', 'Creature — Merfolk Rogue').
card_original_text('scroll thief'/'DDI', 'Whenever Scroll Thief deals combat damage to a player, draw a card.').
card_image_name('scroll thief'/'DDI', 'scroll thief').
card_uid('scroll thief'/'DDI', 'DDI:Scroll Thief:scroll thief').
card_rarity('scroll thief'/'DDI', 'Common').
card_artist('scroll thief'/'DDI', 'Alex Horley-Orlandelli').
card_number('scroll thief'/'DDI', '7').
card_flavor_text('scroll thief'/'DDI', 'I\'ve learned how to disable wards, pick locks, and decode the Agaran language—all before even reading the scroll!').
card_multiverse_id('scroll thief'/'DDI', '270845').

card_in_set('searing blaze', 'DDI').
card_original_type('searing blaze'/'DDI', 'Instant').
card_original_text('searing blaze'/'DDI', 'Searing Blaze deals 1 damage to target player and 1 damage to target creature that player controls.\nLandfall — If you had a land enter the battlefield under your control this turn, Searing Blaze deals 3 damage to that player and 3 damage to that creature instead.').
card_image_name('searing blaze'/'DDI', 'searing blaze').
card_uid('searing blaze'/'DDI', 'DDI:Searing Blaze:searing blaze').
card_rarity('searing blaze'/'DDI', 'Common').
card_artist('searing blaze'/'DDI', 'James Paick').
card_number('searing blaze'/'DDI', '67').
card_multiverse_id('searing blaze'/'DDI', '270873').

card_in_set('seismic strike', 'DDI').
card_original_type('seismic strike'/'DDI', 'Instant').
card_original_text('seismic strike'/'DDI', 'Seismic Strike deals damage to target creature equal to the number of Mountains you control.').
card_image_name('seismic strike'/'DDI', 'seismic strike').
card_uid('seismic strike'/'DDI', 'DDI:Seismic Strike:seismic strike').
card_rarity('seismic strike'/'DDI', 'Common').
card_artist('seismic strike'/'DDI', 'Christopher Moeller').
card_number('seismic strike'/'DDI', '70').
card_flavor_text('seismic strike'/'DDI', '\"Life up here is simple. Adapt to the ways of the mountains and they will reward you. Fight them and they will end you.\"\n—Kezim, prodigal pyromancer').
card_multiverse_id('seismic strike'/'DDI', '270840').

card_in_set('sejiri refuge', 'DDI').
card_original_type('sejiri refuge'/'DDI', 'Land').
card_original_text('sejiri refuge'/'DDI', 'Sejiri Refuge enters the battlefield tapped.\nWhen Sejiri Refuge enters the battlefield, you gain 1 life.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('sejiri refuge'/'DDI', 'sejiri refuge').
card_uid('sejiri refuge'/'DDI', 'DDI:Sejiri Refuge:sejiri refuge').
card_rarity('sejiri refuge'/'DDI', 'Uncommon').
card_artist('sejiri refuge'/'DDI', 'Ryan Pancoast').
card_number('sejiri refuge'/'DDI', '36').
card_multiverse_id('sejiri refuge'/'DDI', '270876').

card_in_set('sigil of sleep', 'DDI').
card_original_type('sigil of sleep'/'DDI', 'Enchantment — Aura').
card_original_text('sigil of sleep'/'DDI', 'Enchant creature\nWhenever enchanted creature deals damage to a player, return target creature that player controls to its owner\'s hand.').
card_image_name('sigil of sleep'/'DDI', 'sigil of sleep').
card_uid('sigil of sleep'/'DDI', 'DDI:Sigil of Sleep:sigil of sleep').
card_rarity('sigil of sleep'/'DDI', 'Common').
card_artist('sigil of sleep'/'DDI', 'Greg & Tim Hildebrandt').
card_number('sigil of sleep'/'DDI', '25').
card_flavor_text('sigil of sleep'/'DDI', 'Arrows are only one way to remove an enemy.').
card_multiverse_id('sigil of sleep'/'DDI', '270813').

card_in_set('sky spirit', 'DDI').
card_original_type('sky spirit'/'DDI', 'Creature — Spirit').
card_original_text('sky spirit'/'DDI', 'Flying, first strike').
card_image_name('sky spirit'/'DDI', 'sky spirit').
card_uid('sky spirit'/'DDI', 'DDI:Sky Spirit:sky spirit').
card_rarity('sky spirit'/'DDI', 'Uncommon').
card_artist('sky spirit'/'DDI', 'Rebecca Guay').
card_number('sky spirit'/'DDI', '10').
card_flavor_text('sky spirit'/'DDI', '\"Like a strain of music: easy to remember but impossible to catch.\"\n—Mirri of the Weatherlight').
card_multiverse_id('sky spirit'/'DDI', '284485').

card_in_set('slith strider', 'DDI').
card_original_type('slith strider'/'DDI', 'Creature — Slith').
card_original_text('slith strider'/'DDI', 'Whenever Slith Strider becomes blocked, draw a card.\nWhenever Slith Strider deals combat damage to a player, put a +1/+1 counter on it.').
card_image_name('slith strider'/'DDI', 'slith strider').
card_uid('slith strider'/'DDI', 'DDI:Slith Strider:slith strider').
card_rarity('slith strider'/'DDI', 'Uncommon').
card_artist('slith strider'/'DDI', 'Justin Sweet').
card_number('slith strider'/'DDI', '9').
card_flavor_text('slith strider'/'DDI', 'A slith\'s form and function are determined by the color of the sun under which it\'s born.').
card_multiverse_id('slith strider'/'DDI', '270851').

card_in_set('soaring seacliff', 'DDI').
card_original_type('soaring seacliff'/'DDI', 'Land').
card_original_text('soaring seacliff'/'DDI', 'Soaring Seacliff enters the battlefield tapped.\nWhen Soaring Seacliff enters the battlefield, target creature gains flying until end of turn.\n{T}: Add {U} to your mana pool.').
card_image_name('soaring seacliff'/'DDI', 'soaring seacliff').
card_uid('soaring seacliff'/'DDI', 'DDI:Soaring Seacliff:soaring seacliff').
card_rarity('soaring seacliff'/'DDI', 'Common').
card_artist('soaring seacliff'/'DDI', 'Izzy').
card_number('soaring seacliff'/'DDI', '37').
card_multiverse_id('soaring seacliff'/'DDI', '270877').

card_in_set('sphinx of uthuun', 'DDI').
card_original_type('sphinx of uthuun'/'DDI', 'Creature — Sphinx').
card_original_text('sphinx of uthuun'/'DDI', 'Flying\nWhen Sphinx of Uthuun enters the battlefield, reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('sphinx of uthuun'/'DDI', 'sphinx of uthuun').
card_uid('sphinx of uthuun'/'DDI', 'DDI:Sphinx of Uthuun:sphinx of uthuun').
card_rarity('sphinx of uthuun'/'DDI', 'Rare').
card_artist('sphinx of uthuun'/'DDI', 'Kekai Kotaki').
card_number('sphinx of uthuun'/'DDI', '22').
card_multiverse_id('sphinx of uthuun'/'DDI', '284486').

card_in_set('spire barrage', 'DDI').
card_original_type('spire barrage'/'DDI', 'Sorcery').
card_original_text('spire barrage'/'DDI', 'Spire Barrage deals damage to target creature or player equal to the number of Mountains you control.').
card_image_name('spire barrage'/'DDI', 'spire barrage').
card_uid('spire barrage'/'DDI', 'DDI:Spire Barrage:spire barrage').
card_rarity('spire barrage'/'DDI', 'Common').
card_artist('spire barrage'/'DDI', 'Ryan Pancoast').
card_number('spire barrage'/'DDI', '71').
card_flavor_text('spire barrage'/'DDI', 'Goblin lessons include the 2,071 tips for survival. Frek only remembered 2,070.').
card_multiverse_id('spire barrage'/'DDI', '270878').

card_in_set('steel of the godhead', 'DDI').
card_original_type('steel of the godhead'/'DDI', 'Enchantment — Aura').
card_original_text('steel of the godhead'/'DDI', 'Enchant creature\nAs long as enchanted creature is white, it gets +1/+1 and has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)\nAs long as enchanted creature is blue, it gets +1/+1 and is unblockable.').
card_image_name('steel of the godhead'/'DDI', 'steel of the godhead').
card_uid('steel of the godhead'/'DDI', 'DDI:Steel of the Godhead:steel of the godhead').
card_rarity('steel of the godhead'/'DDI', 'Common').
card_artist('steel of the godhead'/'DDI', 'Jason Chan').
card_number('steel of the godhead'/'DDI', '30').
card_multiverse_id('steel of the godhead'/'DDI', '270866').

card_in_set('stone giant', 'DDI').
card_original_type('stone giant'/'DDI', 'Creature — Giant').
card_original_text('stone giant'/'DDI', '{T}: Target creature you control with toughness less than Stone Giant\'s power gains flying until end of turn. Destroy that creature at the beginning of the next end step.').
card_image_name('stone giant'/'DDI', 'stone giant').
card_uid('stone giant'/'DDI', 'DDI:Stone Giant:stone giant').
card_rarity('stone giant'/'DDI', 'Uncommon').
card_artist('stone giant'/'DDI', 'Warren Mahy').
card_number('stone giant'/'DDI', '55').
card_flavor_text('stone giant'/'DDI', 'A typical day for a giant. A momentous event for a goat.').
card_multiverse_id('stone giant'/'DDI', '279856').

card_in_set('sunblast angel', 'DDI').
card_original_type('sunblast angel'/'DDI', 'Creature — Angel').
card_original_text('sunblast angel'/'DDI', 'Flying\nWhen Sunblast Angel enters the battlefield, destroy all tapped creatures.').
card_image_name('sunblast angel'/'DDI', 'sunblast angel').
card_uid('sunblast angel'/'DDI', 'DDI:Sunblast Angel:sunblast angel').
card_rarity('sunblast angel'/'DDI', 'Rare').
card_artist('sunblast angel'/'DDI', 'Jason Chan').
card_number('sunblast angel'/'DDI', '21').
card_flavor_text('sunblast angel'/'DDI', 'There may exist powers even greater than Phyrexia.').
card_multiverse_id('sunblast angel'/'DDI', '284489').

card_in_set('torchling', 'DDI').
card_original_type('torchling'/'DDI', 'Creature — Shapeshifter').
card_original_text('torchling'/'DDI', '{R}: Untap Torchling.\n{R}: Target creature blocks Torchling this turn if able.\n{R}: Change the target of target spell that targets only Torchling.\n{1}: Torchling gets +1/-1 until end of turn.\n{1}: Torchling gets -1/+1 until end of turn.').
card_image_name('torchling'/'DDI', 'torchling').
card_uid('torchling'/'DDI', 'DDI:Torchling:torchling').
card_rarity('torchling'/'DDI', 'Rare').
card_artist('torchling'/'DDI', 'rk post').
card_number('torchling'/'DDI', '58').
card_multiverse_id('torchling'/'DDI', '270858').

card_in_set('vanish into memory', 'DDI').
card_original_type('vanish into memory'/'DDI', 'Instant').
card_original_text('vanish into memory'/'DDI', 'Exile target creature. You draw cards equal to that creature\'s power. At the beginning of your next upkeep, return that card to the battlefield under its owner\'s control. If you do, discard cards equal to its toughness.').
card_image_name('vanish into memory'/'DDI', 'vanish into memory').
card_uid('vanish into memory'/'DDI', 'DDI:Vanish into Memory:vanish into memory').
card_rarity('vanish into memory'/'DDI', 'Uncommon').
card_artist('vanish into memory'/'DDI', 'Rebekah Lynn').
card_number('vanish into memory'/'DDI', '31').
card_flavor_text('vanish into memory'/'DDI', 'Out of sight, into mind.').
card_multiverse_id('vanish into memory'/'DDI', '270831').

card_in_set('venser, the sojourner', 'DDI').
card_original_type('venser, the sojourner'/'DDI', 'Planeswalker — Venser').
card_original_text('venser, the sojourner'/'DDI', '+2: Exile target permanent you own. Return it to the battlefield under your control at the beginning of the next end step.\n-1: Creatures are unblockable this turn.\n-8: You get an emblem with \"Whenever you cast a spell, exile target permanent.\"').
card_image_name('venser, the sojourner'/'DDI', 'venser, the sojourner').
card_uid('venser, the sojourner'/'DDI', 'DDI:Venser, the Sojourner:venser, the sojourner').
card_rarity('venser, the sojourner'/'DDI', 'Mythic Rare').
card_artist('venser, the sojourner'/'DDI', 'Eric Deschamps').
card_number('venser, the sojourner'/'DDI', '1').
card_multiverse_id('venser, the sojourner'/'DDI', '266078').

card_in_set('volley of boulders', 'DDI').
card_original_type('volley of boulders'/'DDI', 'Sorcery').
card_original_text('volley of boulders'/'DDI', 'Volley of Boulders deals 6 damage divided as you choose among any number of target creatures and/or players.\nFlashback {R}{R}{R}{R}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('volley of boulders'/'DDI', 'volley of boulders').
card_uid('volley of boulders'/'DDI', 'DDI:Volley of Boulders:volley of boulders').
card_rarity('volley of boulders'/'DDI', 'Rare').
card_artist('volley of boulders'/'DDI', 'Tony Szczudlo').
card_number('volley of boulders'/'DDI', '73').
card_multiverse_id('volley of boulders'/'DDI', '270855').

card_in_set('vulshok battlegear', 'DDI').
card_original_type('vulshok battlegear'/'DDI', 'Artifact — Equipment').
card_original_text('vulshok battlegear'/'DDI', 'Equipped creature gets +3/+3.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('vulshok battlegear'/'DDI', 'vulshok battlegear').
card_uid('vulshok battlegear'/'DDI', 'DDI:Vulshok Battlegear:vulshok battlegear').
card_rarity('vulshok battlegear'/'DDI', 'Uncommon').
card_artist('vulshok battlegear'/'DDI', 'Kevin Dobler').
card_number('vulshok battlegear'/'DDI', '68').
card_multiverse_id('vulshok battlegear'/'DDI', '270852').

card_in_set('vulshok berserker', 'DDI').
card_original_type('vulshok berserker'/'DDI', 'Creature — Human Berserker').
card_original_text('vulshok berserker'/'DDI', 'Haste').
card_image_name('vulshok berserker'/'DDI', 'vulshok berserker').
card_uid('vulshok berserker'/'DDI', 'DDI:Vulshok Berserker:vulshok berserker').
card_rarity('vulshok berserker'/'DDI', 'Common').
card_artist('vulshok berserker'/'DDI', 'Pete Venters').
card_number('vulshok berserker'/'DDI', '53').
card_flavor_text('vulshok berserker'/'DDI', 'He experiences every emotion with passion and repays every slight with vengeance.').
card_multiverse_id('vulshok berserker'/'DDI', '270846').

card_in_set('vulshok morningstar', 'DDI').
card_original_type('vulshok morningstar'/'DDI', 'Artifact — Equipment').
card_original_text('vulshok morningstar'/'DDI', 'Equipped creature gets +2/+2.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('vulshok morningstar'/'DDI', 'vulshok morningstar').
card_uid('vulshok morningstar'/'DDI', 'DDI:Vulshok Morningstar:vulshok morningstar').
card_rarity('vulshok morningstar'/'DDI', 'Uncommon').
card_artist('vulshok morningstar'/'DDI', 'David Martin').
card_number('vulshok morningstar'/'DDI', '66').
card_multiverse_id('vulshok morningstar'/'DDI', '270810').

card_in_set('vulshok sorcerer', 'DDI').
card_original_type('vulshok sorcerer'/'DDI', 'Creature — Human Shaman').
card_original_text('vulshok sorcerer'/'DDI', 'Haste\n{T}: Vulshok Sorcerer deals 1 damage to target creature or player.').
card_image_name('vulshok sorcerer'/'DDI', 'vulshok sorcerer').
card_uid('vulshok sorcerer'/'DDI', 'DDI:Vulshok Sorcerer:vulshok sorcerer').
card_rarity('vulshok sorcerer'/'DDI', 'Common').
card_artist('vulshok sorcerer'/'DDI', 'rk post').
card_number('vulshok sorcerer'/'DDI', '50').
card_flavor_text('vulshok sorcerer'/'DDI', 'Vulshok sorcerers train by leaping into electrified storm clouds. Dead or alive, they come back down with smiles on their faces.').
card_multiverse_id('vulshok sorcerer'/'DDI', '270806').

card_in_set('wall of denial', 'DDI').
card_original_type('wall of denial'/'DDI', 'Creature — Wall').
card_original_text('wall of denial'/'DDI', 'Defender, flying\nShroud (This creature can\'t be the target of spells or abilities.)').
card_image_name('wall of denial'/'DDI', 'wall of denial').
card_uid('wall of denial'/'DDI', 'DDI:Wall of Denial:wall of denial').
card_rarity('wall of denial'/'DDI', 'Uncommon').
card_artist('wall of denial'/'DDI', 'Howard Lyon').
card_number('wall of denial'/'DDI', '11').
card_flavor_text('wall of denial'/'DDI', 'It provides what every discerning mage requires—time to think.').
card_multiverse_id('wall of denial'/'DDI', '270829').

card_in_set('wayfarer\'s bauble', 'DDI').
card_original_type('wayfarer\'s bauble'/'DDI', 'Artifact').
card_original_text('wayfarer\'s bauble'/'DDI', '{2}, {T}, Sacrifice Wayfarer\'s Bauble: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('wayfarer\'s bauble'/'DDI', 'wayfarer\'s bauble').
card_uid('wayfarer\'s bauble'/'DDI', 'DDI:Wayfarer\'s Bauble:wayfarer\'s bauble').
card_rarity('wayfarer\'s bauble'/'DDI', 'Common').
card_artist('wayfarer\'s bauble'/'DDI', 'Darrell Riche').
card_number('wayfarer\'s bauble'/'DDI', '63').
card_flavor_text('wayfarer\'s bauble'/'DDI', 'It is the forest beyond the horizon, the mountain waiting to be climbed, the new land across the endless sea.').
card_multiverse_id('wayfarer\'s bauble'/'DDI', '270807').

card_in_set('whitemane lion', 'DDI').
card_original_type('whitemane lion'/'DDI', 'Creature — Cat').
card_original_text('whitemane lion'/'DDI', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Whitemane Lion enters the battlefield, return a creature you control to its owner\'s hand.').
card_image_name('whitemane lion'/'DDI', 'whitemane lion').
card_uid('whitemane lion'/'DDI', 'DDI:Whitemane Lion:whitemane lion').
card_rarity('whitemane lion'/'DDI', 'Common').
card_artist('whitemane lion'/'DDI', 'Zoltan Boros & Gabor Szikszai').
card_number('whitemane lion'/'DDI', '2').
card_flavor_text('whitemane lion'/'DDI', 'Saltfield nomads call a sudden storm a \"whitemane.\"').
card_multiverse_id('whitemane lion'/'DDI', '270859').

card_in_set('windreaver', 'DDI').
card_original_type('windreaver'/'DDI', 'Creature — Elemental').
card_original_text('windreaver'/'DDI', 'Flying\n{W}: Windreaver gains vigilance until end of turn.\n{W}: Windreaver gets +0/+1 until end of turn.\n{U}: Switch Windreaver\'s power and toughness until end of turn.\n{U}: Return Windreaver to its owner\'s hand.').
card_image_name('windreaver'/'DDI', 'windreaver').
card_uid('windreaver'/'DDI', 'DDI:Windreaver:windreaver').
card_rarity('windreaver'/'DDI', 'Rare').
card_artist('windreaver'/'DDI', 'Aleksi Briclot').
card_number('windreaver'/'DDI', '19').
card_multiverse_id('windreaver'/'DDI', '270835').
