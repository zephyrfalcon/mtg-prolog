% Magic Game Day

set('pMGD').
set_name('pMGD', 'Magic Game Day').
set_release_date('pMGD', '2007-07-14').
set_border('pMGD', 'black').
set_type('pMGD', 'promo').

card_in_set('black sun\'s zenith', 'pMGD').
card_original_type('black sun\'s zenith'/'pMGD', 'Sorcery').
card_original_text('black sun\'s zenith'/'pMGD', '').
card_first_print('black sun\'s zenith', 'pMGD').
card_image_name('black sun\'s zenith'/'pMGD', 'black sun\'s zenith').
card_uid('black sun\'s zenith'/'pMGD', 'pMGD:Black Sun\'s Zenith:black sun\'s zenith').
card_rarity('black sun\'s zenith'/'pMGD', 'Special').
card_artist('black sun\'s zenith'/'pMGD', 'James Paick').
card_number('black sun\'s zenith'/'pMGD', '7').

card_in_set('chief engineer', 'pMGD').
card_original_type('chief engineer'/'pMGD', 'Creature — Vedalken Artificer').
card_original_text('chief engineer'/'pMGD', '').
card_first_print('chief engineer', 'pMGD').
card_image_name('chief engineer'/'pMGD', 'chief engineer').
card_uid('chief engineer'/'pMGD', 'pMGD:Chief Engineer:chief engineer').
card_rarity('chief engineer'/'pMGD', 'Special').
card_artist('chief engineer'/'pMGD', 'Ryan Barger').
card_number('chief engineer'/'pMGD', '40').
card_flavor_text('chief engineer'/'pMGD', 'An eye for detail, a mind for numbers, a soul of clockwork.').

card_in_set('cryptborn horror', 'pMGD').
card_original_type('cryptborn horror'/'pMGD', 'Creature — Horror').
card_original_text('cryptborn horror'/'pMGD', '').
card_first_print('cryptborn horror', 'pMGD').
card_image_name('cryptborn horror'/'pMGD', 'cryptborn horror').
card_uid('cryptborn horror'/'pMGD', 'pMGD:Cryptborn Horror:cryptborn horror').
card_rarity('cryptborn horror'/'pMGD', 'Special').
card_artist('cryptborn horror'/'pMGD', 'Nils Hamm').
card_number('cryptborn horror'/'pMGD', '22').

card_in_set('dictate of kruphix', 'pMGD').
card_original_type('dictate of kruphix'/'pMGD', 'Enchantment').
card_original_text('dictate of kruphix'/'pMGD', '').
card_first_print('dictate of kruphix', 'pMGD').
card_image_name('dictate of kruphix'/'pMGD', 'dictate of kruphix').
card_uid('dictate of kruphix'/'pMGD', 'pMGD:Dictate of Kruphix:dictate of kruphix').
card_rarity('dictate of kruphix'/'pMGD', 'Special').
card_artist('dictate of kruphix'/'pMGD', 'Ryan Barger').
card_number('dictate of kruphix'/'pMGD', '35').

card_in_set('diregraf ghoul', 'pMGD').
card_original_type('diregraf ghoul'/'pMGD', 'Creature — Zombie').
card_original_text('diregraf ghoul'/'pMGD', '').
card_first_print('diregraf ghoul', 'pMGD').
card_image_name('diregraf ghoul'/'pMGD', 'diregraf ghoul').
card_uid('diregraf ghoul'/'pMGD', 'pMGD:Diregraf Ghoul:diregraf ghoul').
card_rarity('diregraf ghoul'/'pMGD', 'Special').
card_artist('diregraf ghoul'/'pMGD', 'Austin Hsu').
card_number('diregraf ghoul'/'pMGD', '12').

card_in_set('dryad militant', 'pMGD').
card_original_type('dryad militant'/'pMGD', 'Creature — Dryad Soldier').
card_original_text('dryad militant'/'pMGD', '').
card_first_print('dryad militant', 'pMGD').
card_image_name('dryad militant'/'pMGD', 'dryad militant').
card_uid('dryad militant'/'pMGD', 'pMGD:Dryad Militant:dryad militant').
card_rarity('dryad militant'/'pMGD', 'Special').
card_artist('dryad militant'/'pMGD', 'Ryan Pancoast').
card_number('dryad militant'/'pMGD', '23').

card_in_set('dungrove elder', 'pMGD').
card_original_type('dungrove elder'/'pMGD', 'Creature — Treefolk').
card_original_text('dungrove elder'/'pMGD', '').
card_first_print('dungrove elder', 'pMGD').
card_image_name('dungrove elder'/'pMGD', 'dungrove elder').
card_uid('dungrove elder'/'pMGD', 'pMGD:Dungrove Elder:dungrove elder').
card_rarity('dungrove elder'/'pMGD', 'Special').
card_artist('dungrove elder'/'pMGD', 'Scott Chou').
card_number('dungrove elder'/'pMGD', '11').

card_in_set('elite inquisitor', 'pMGD').
card_original_type('elite inquisitor'/'pMGD', 'Creature — Human Soldier').
card_original_text('elite inquisitor'/'pMGD', '').
card_first_print('elite inquisitor', 'pMGD').
card_image_name('elite inquisitor'/'pMGD', 'elite inquisitor').
card_uid('elite inquisitor'/'pMGD', 'pMGD:Elite Inquisitor:elite inquisitor').
card_rarity('elite inquisitor'/'pMGD', 'Special').
card_artist('elite inquisitor'/'pMGD', 'Igor Kieryluk').
card_number('elite inquisitor'/'pMGD', '13').

card_in_set('firemane avenger', 'pMGD').
card_original_type('firemane avenger'/'pMGD', 'Creature — Angel').
card_original_text('firemane avenger'/'pMGD', '').
card_first_print('firemane avenger', 'pMGD').
card_image_name('firemane avenger'/'pMGD', 'firemane avenger').
card_uid('firemane avenger'/'pMGD', 'pMGD:Firemane Avenger:firemane avenger').
card_rarity('firemane avenger'/'pMGD', 'Special').
card_artist('firemane avenger'/'pMGD', 'Ryan Barger').
card_number('firemane avenger'/'pMGD', '24').

card_in_set('goblin diplomats', 'pMGD').
card_original_type('goblin diplomats'/'pMGD', 'Creature — Goblin').
card_original_text('goblin diplomats'/'pMGD', '').
card_first_print('goblin diplomats', 'pMGD').
card_image_name('goblin diplomats'/'pMGD', 'goblin diplomats').
card_uid('goblin diplomats'/'pMGD', 'pMGD:Goblin Diplomats:goblin diplomats').
card_rarity('goblin diplomats'/'pMGD', 'Special').
card_artist('goblin diplomats'/'pMGD', 'Jesper Ejsing').
card_number('goblin diplomats'/'pMGD', '29').

card_in_set('hall of triumph', 'pMGD').
card_original_type('hall of triumph'/'pMGD', 'Legendary Artifact').
card_original_text('hall of triumph'/'pMGD', '').
card_first_print('hall of triumph', 'pMGD').
card_image_name('hall of triumph'/'pMGD', 'hall of triumph').
card_uid('hall of triumph'/'pMGD', 'pMGD:Hall of Triumph:hall of triumph').
card_rarity('hall of triumph'/'pMGD', 'Special').
card_artist('hall of triumph'/'pMGD', 'Chuck Lukacs').
card_number('hall of triumph'/'pMGD', '36').
card_flavor_text('hall of triumph'/'pMGD', 'Heroes act without thought of glory or reward. Accolades are a consequence, not a goal.').

card_in_set('heir of the wilds', 'pMGD').
card_original_type('heir of the wilds'/'pMGD', 'Creature — Human Warrior').
card_original_text('heir of the wilds'/'pMGD', '').
card_first_print('heir of the wilds', 'pMGD').
card_image_name('heir of the wilds'/'pMGD', 'heir of the wilds').
card_uid('heir of the wilds'/'pMGD', 'pMGD:Heir of the Wilds:heir of the wilds').
card_rarity('heir of the wilds'/'pMGD', 'Special').
card_artist('heir of the wilds'/'pMGD', 'David Palumbo').
card_number('heir of the wilds'/'pMGD', '37').
card_flavor_text('heir of the wilds'/'pMGD', 'In the high caves of the Qal Sisma mountains, young hunters quest to hear the echoes of their fierce ancestors.').

card_in_set('hive stirrings', 'pMGD').
card_original_type('hive stirrings'/'pMGD', 'Sorcery').
card_original_text('hive stirrings'/'pMGD', '').
card_first_print('hive stirrings', 'pMGD').
card_image_name('hive stirrings'/'pMGD', 'hive stirrings').
card_uid('hive stirrings'/'pMGD', 'pMGD:Hive Stirrings:hive stirrings').
card_rarity('hive stirrings'/'pMGD', 'Special').
card_artist('hive stirrings'/'pMGD', 'Johann Bodin').
card_number('hive stirrings'/'pMGD', '28').

card_in_set('killing wave', 'pMGD').
card_original_type('killing wave'/'pMGD', 'Sorcery').
card_original_text('killing wave'/'pMGD', '').
card_first_print('killing wave', 'pMGD').
card_image_name('killing wave'/'pMGD', 'killing wave').
card_uid('killing wave'/'pMGD', 'pMGD:Killing Wave:killing wave').
card_rarity('killing wave'/'pMGD', 'Special').
card_artist('killing wave'/'pMGD', 'Michael Komarck').
card_number('killing wave'/'pMGD', '19').

card_in_set('kiora\'s follower', 'pMGD').
card_original_type('kiora\'s follower'/'pMGD', 'Creature — Merfolk').
card_original_text('kiora\'s follower'/'pMGD', '').
card_first_print('kiora\'s follower', 'pMGD').
card_image_name('kiora\'s follower'/'pMGD', 'kiora\'s follower').
card_uid('kiora\'s follower'/'pMGD', 'pMGD:Kiora\'s Follower:kiora\'s follower').
card_rarity('kiora\'s follower'/'pMGD', 'Special').
card_artist('kiora\'s follower'/'pMGD', 'Steve Prescott').
card_number('kiora\'s follower'/'pMGD', '33').

card_in_set('latch seeker', 'pMGD').
card_original_type('latch seeker'/'pMGD', 'Creature — Spirit').
card_original_text('latch seeker'/'pMGD', '').
card_first_print('latch seeker', 'pMGD').
card_image_name('latch seeker'/'pMGD', 'latch seeker').
card_uid('latch seeker'/'pMGD', 'pMGD:Latch Seeker:latch seeker').
card_rarity('latch seeker'/'pMGD', 'Special').
card_artist('latch seeker'/'pMGD', 'Eytan Zana').
card_number('latch seeker'/'pMGD', '18').

card_in_set('liliana\'s specter', 'pMGD').
card_original_type('liliana\'s specter'/'pMGD', 'Creature — Specter').
card_original_text('liliana\'s specter'/'pMGD', '').
card_first_print('liliana\'s specter', 'pMGD').
card_image_name('liliana\'s specter'/'pMGD', 'liliana\'s specter').
card_uid('liliana\'s specter'/'pMGD', 'pMGD:Liliana\'s Specter:liliana\'s specter').
card_rarity('liliana\'s specter'/'pMGD', 'Special').
card_artist('liliana\'s specter'/'pMGD', 'Jaime Jones').
card_number('liliana\'s specter'/'pMGD', '2').

card_in_set('magmaquake', 'pMGD').
card_original_type('magmaquake'/'pMGD', 'Instant').
card_original_text('magmaquake'/'pMGD', '').
card_first_print('magmaquake', 'pMGD').
card_image_name('magmaquake'/'pMGD', 'magmaquake').
card_uid('magmaquake'/'pMGD', 'pMGD:Magmaquake:magmaquake').
card_rarity('magmaquake'/'pMGD', 'Special').
card_artist('magmaquake'/'pMGD', 'Sam Burley').
card_number('magmaquake'/'pMGD', '20').

card_in_set('mardu shadowspear', 'pMGD').
card_original_type('mardu shadowspear'/'pMGD', 'Creature — Human Warrior').
card_original_text('mardu shadowspear'/'pMGD', '').
card_first_print('mardu shadowspear', 'pMGD').
card_image_name('mardu shadowspear'/'pMGD', 'mardu shadowspear').
card_uid('mardu shadowspear'/'pMGD', 'pMGD:Mardu Shadowspear:mardu shadowspear').
card_rarity('mardu shadowspear'/'pMGD', 'Special').
card_artist('mardu shadowspear'/'pMGD', 'Dan Scott').
card_number('mardu shadowspear'/'pMGD', '41').

card_in_set('melek, izzet paragon', 'pMGD').
card_original_type('melek, izzet paragon'/'pMGD', 'Legendary Creature — Weird Wizard').
card_original_text('melek, izzet paragon'/'pMGD', '').
card_first_print('melek, izzet paragon', 'pMGD').
card_image_name('melek, izzet paragon'/'pMGD', 'melek, izzet paragon').
card_uid('melek, izzet paragon'/'pMGD', 'pMGD:Melek, Izzet Paragon:melek, izzet paragon').
card_rarity('melek, izzet paragon'/'pMGD', 'Special').
card_artist('melek, izzet paragon'/'pMGD', 'Johann Bodin').
card_number('melek, izzet paragon'/'pMGD', '26').

card_in_set('memnite', 'pMGD').
card_original_type('memnite'/'pMGD', 'Artifact Creature — Construct').
card_original_text('memnite'/'pMGD', '').
card_first_print('memnite', 'pMGD').
card_image_name('memnite'/'pMGD', 'memnite').
card_uid('memnite'/'pMGD', 'pMGD:Memnite:memnite').
card_rarity('memnite'/'pMGD', 'Special').
card_artist('memnite'/'pMGD', 'rk post').
card_number('memnite'/'pMGD', '4').
card_flavor_text('memnite'/'pMGD', 'Reminders of Memnarch\'s reign still skirr across Mirrodin, reminiscent of his form if not his power.').

card_in_set('mitotic slime', 'pMGD').
card_original_type('mitotic slime'/'pMGD', 'Creature — Ooze').
card_original_text('mitotic slime'/'pMGD', '').
card_first_print('mitotic slime', 'pMGD').
card_image_name('mitotic slime'/'pMGD', 'mitotic slime').
card_uid('mitotic slime'/'pMGD', 'pMGD:Mitotic Slime:mitotic slime').
card_rarity('mitotic slime'/'pMGD', 'Special').
card_artist('mitotic slime'/'pMGD', 'Ron Spencer').
card_number('mitotic slime'/'pMGD', '3').

card_in_set('mwonvuli beast tracker', 'pMGD').
card_original_type('mwonvuli beast tracker'/'pMGD', 'Creature — Human Scout').
card_original_text('mwonvuli beast tracker'/'pMGD', '').
card_first_print('mwonvuli beast tracker', 'pMGD').
card_image_name('mwonvuli beast tracker'/'pMGD', 'mwonvuli beast tracker').
card_uid('mwonvuli beast tracker'/'pMGD', 'pMGD:Mwonvuli Beast Tracker:mwonvuli beast tracker').
card_rarity('mwonvuli beast tracker'/'pMGD', 'Special').
card_artist('mwonvuli beast tracker'/'pMGD', 'Jason A. Engle').
card_number('mwonvuli beast tracker'/'pMGD', '21').

card_in_set('myr superion', 'pMGD').
card_original_type('myr superion'/'pMGD', 'Artifact Creature — Myr').
card_original_text('myr superion'/'pMGD', '').
card_first_print('myr superion', 'pMGD').
card_image_name('myr superion'/'pMGD', 'myr superion').
card_uid('myr superion'/'pMGD', 'pMGD:Myr Superion:myr superion').
card_rarity('myr superion'/'pMGD', 'Special').
card_artist('myr superion'/'pMGD', 'Greg Staples').
card_number('myr superion'/'pMGD', '8').

card_in_set('nighthowler', 'pMGD').
card_original_type('nighthowler'/'pMGD', 'Enchantment Creature — Horror').
card_original_text('nighthowler'/'pMGD', '').
card_first_print('nighthowler', 'pMGD').
card_image_name('nighthowler'/'pMGD', 'nighthowler').
card_uid('nighthowler'/'pMGD', 'pMGD:Nighthowler:nighthowler').
card_rarity('nighthowler'/'pMGD', 'Special').
card_artist('nighthowler'/'pMGD', 'Seb McKinnon').
card_number('nighthowler'/'pMGD', '31').

card_in_set('pain seer', 'pMGD').
card_original_type('pain seer'/'pMGD', 'Creature — Human Wizard').
card_original_text('pain seer'/'pMGD', '').
card_first_print('pain seer', 'pMGD').
card_image_name('pain seer'/'pMGD', 'pain seer').
card_uid('pain seer'/'pMGD', 'pMGD:Pain Seer:pain seer').
card_rarity('pain seer'/'pMGD', 'Special').
card_artist('pain seer'/'pMGD', 'Mark Winters').
card_number('pain seer'/'pMGD', '32').

card_in_set('phalanx leader', 'pMGD').
card_original_type('phalanx leader'/'pMGD', 'Creature — Human Soldier').
card_original_text('phalanx leader'/'pMGD', '').
card_first_print('phalanx leader', 'pMGD').
card_image_name('phalanx leader'/'pMGD', 'phalanx leader').
card_uid('phalanx leader'/'pMGD', 'pMGD:Phalanx Leader:phalanx leader').
card_rarity('phalanx leader'/'pMGD', 'Special').
card_artist('phalanx leader'/'pMGD', 'Raymond Swanland').
card_number('phalanx leader'/'pMGD', '30').

card_in_set('priest of urabrask', 'pMGD').
card_original_type('priest of urabrask'/'pMGD', 'Creature — Human Cleric').
card_original_text('priest of urabrask'/'pMGD', '').
card_first_print('priest of urabrask', 'pMGD').
card_image_name('priest of urabrask'/'pMGD', 'priest of urabrask').
card_uid('priest of urabrask'/'pMGD', 'pMGD:Priest of Urabrask:priest of urabrask').
card_rarity('priest of urabrask'/'pMGD', 'Special').
card_artist('priest of urabrask'/'pMGD', 'Clint Cearley').
card_number('priest of urabrask'/'pMGD', '9').

card_in_set('pristine talisman', 'pMGD').
card_original_type('pristine talisman'/'pMGD', 'Artifact').
card_original_text('pristine talisman'/'pMGD', '').
card_first_print('pristine talisman', 'pMGD').
card_image_name('pristine talisman'/'pMGD', 'pristine talisman').
card_uid('pristine talisman'/'pMGD', 'pMGD:Pristine Talisman:pristine talisman').
card_rarity('pristine talisman'/'pMGD', 'Special').
card_artist('pristine talisman'/'pMGD', 'Matt Cavotta').
card_number('pristine talisman'/'pMGD', '17').
card_flavor_text('pristine talisman'/'pMGD', '\"Tools and artisans can be destroyed, but the act of creation is inviolate.\"\n—Elspeth Tirel').

card_in_set('reclamation sage', 'pMGD').
card_original_type('reclamation sage'/'pMGD', 'Creature — Elf Shaman').
card_original_text('reclamation sage'/'pMGD', '').
card_first_print('reclamation sage', 'pMGD').
card_image_name('reclamation sage'/'pMGD', 'reclamation sage').
card_uid('reclamation sage'/'pMGD', 'pMGD:Reclamation Sage:reclamation sage').
card_rarity('reclamation sage'/'pMGD', 'Special').
card_artist('reclamation sage'/'pMGD', 'Clint Cearley').
card_number('reclamation sage'/'pMGD', '39').
card_flavor_text('reclamation sage'/'pMGD', '\"What was once formed by masons, shaped by smiths, or given life by mages, I will return to the embrace of the earth.\"').

card_in_set('reya dawnbringer', 'pMGD').
card_original_type('reya dawnbringer'/'pMGD', 'Legendary Creature — Angel').
card_original_text('reya dawnbringer'/'pMGD', '').
card_image_name('reya dawnbringer'/'pMGD', 'reya dawnbringer').
card_uid('reya dawnbringer'/'pMGD', 'pMGD:Reya Dawnbringer:reya dawnbringer').
card_rarity('reya dawnbringer'/'pMGD', 'Special').
card_artist('reya dawnbringer'/'pMGD', 'Matthew D. Wilson').
card_number('reya dawnbringer'/'pMGD', '1').
card_flavor_text('reya dawnbringer'/'pMGD', '\"You have not died until I consent.\"').

card_in_set('scaleguard sentinels', 'pMGD').
card_original_type('scaleguard sentinels'/'pMGD', 'Creature — Human Soldier').
card_original_text('scaleguard sentinels'/'pMGD', '').
card_first_print('scaleguard sentinels', 'pMGD').
card_image_name('scaleguard sentinels'/'pMGD', 'scaleguard sentinels').
card_uid('scaleguard sentinels'/'pMGD', 'pMGD:Scaleguard Sentinels:scaleguard sentinels').
card_rarity('scaleguard sentinels'/'pMGD', 'Special').
card_artist('scaleguard sentinels'/'pMGD', 'David Gaillet').
card_number('scaleguard sentinels'/'pMGD', '44').

card_in_set('squelching leeches', 'pMGD').
card_original_type('squelching leeches'/'pMGD', 'Creature — Leech').
card_original_text('squelching leeches'/'pMGD', '').
card_first_print('squelching leeches', 'pMGD').
card_image_name('squelching leeches'/'pMGD', 'squelching leeches').
card_uid('squelching leeches'/'pMGD', 'pMGD:Squelching Leeches:squelching leeches').
card_rarity('squelching leeches'/'pMGD', 'Special').
card_artist('squelching leeches'/'pMGD', 'Tomasz Jedruszek').
card_number('squelching leeches'/'pMGD', '34').

card_in_set('stormblood berserker', 'pMGD').
card_original_type('stormblood berserker'/'pMGD', 'Creature — Human Berserker').
card_original_text('stormblood berserker'/'pMGD', '').
card_first_print('stormblood berserker', 'pMGD').
card_image_name('stormblood berserker'/'pMGD', 'stormblood berserker').
card_uid('stormblood berserker'/'pMGD', 'pMGD:Stormblood Berserker:stormblood berserker').
card_rarity('stormblood berserker'/'pMGD', 'Special').
card_artist('stormblood berserker'/'pMGD', 'Greg Staples').
card_number('stormblood berserker'/'pMGD', '10').

card_in_set('strangleroot geist', 'pMGD').
card_original_type('strangleroot geist'/'pMGD', 'Creature — Spirit').
card_original_text('strangleroot geist'/'pMGD', '').
card_first_print('strangleroot geist', 'pMGD').
card_image_name('strangleroot geist'/'pMGD', 'strangleroot geist').
card_uid('strangleroot geist'/'pMGD', 'pMGD:Strangleroot Geist:strangleroot geist').
card_rarity('strangleroot geist'/'pMGD', 'Special').
card_artist('strangleroot geist'/'pMGD', 'Erica Yang').
card_number('strangleroot geist'/'pMGD', '15').

card_in_set('supplant form', 'pMGD').
card_original_type('supplant form'/'pMGD', 'Instant').
card_original_text('supplant form'/'pMGD', '').
card_first_print('supplant form', 'pMGD').
card_image_name('supplant form'/'pMGD', 'supplant form').
card_uid('supplant form'/'pMGD', 'pMGD:Supplant Form:supplant form').
card_rarity('supplant form'/'pMGD', 'Special').
card_artist('supplant form'/'pMGD', 'Mike Bierek').
card_number('supplant form'/'pMGD', '42').
card_flavor_text('supplant form'/'pMGD', '\"Ice can be shaped to any form, even the whisper of a memory.\"\n—Mytha, Temur shaman').

card_in_set('suture priest', 'pMGD').
card_original_type('suture priest'/'pMGD', 'Creature — Cleric').
card_original_text('suture priest'/'pMGD', '').
card_first_print('suture priest', 'pMGD').
card_image_name('suture priest'/'pMGD', 'suture priest').
card_uid('suture priest'/'pMGD', 'pMGD:Suture Priest:suture priest').
card_rarity('suture priest'/'pMGD', 'Special').
card_artist('suture priest'/'pMGD', 'Igor Kieryluk').
card_number('suture priest'/'pMGD', '16').

card_in_set('tempered steel', 'pMGD').
card_original_type('tempered steel'/'pMGD', 'Enchantment').
card_original_text('tempered steel'/'pMGD', '').
card_first_print('tempered steel', 'pMGD').
card_image_name('tempered steel'/'pMGD', 'tempered steel').
card_uid('tempered steel'/'pMGD', 'pMGD:Tempered Steel:tempered steel').
card_rarity('tempered steel'/'pMGD', 'Special').
card_artist('tempered steel'/'pMGD', 'jD').
card_number('tempered steel'/'pMGD', '5').

card_in_set('thunderbreak regent', 'pMGD').
card_original_type('thunderbreak regent'/'pMGD', 'Creature — Dragon').
card_original_text('thunderbreak regent'/'pMGD', '').
card_first_print('thunderbreak regent', 'pMGD').
card_image_name('thunderbreak regent'/'pMGD', 'thunderbreak regent').
card_uid('thunderbreak regent'/'pMGD', 'pMGD:Thunderbreak Regent:thunderbreak regent').
card_rarity('thunderbreak regent'/'pMGD', 'Special').
card_artist('thunderbreak regent'/'pMGD', 'Jason Rainville').
card_number('thunderbreak regent'/'pMGD', '43').
card_flavor_text('thunderbreak regent'/'pMGD', 'Attracting a dragon\'s attention may be the last mistake you make.').

card_in_set('treasure mage', 'pMGD').
card_original_type('treasure mage'/'pMGD', 'Creature — Human Wizard').
card_original_text('treasure mage'/'pMGD', '').
card_first_print('treasure mage', 'pMGD').
card_image_name('treasure mage'/'pMGD', 'treasure mage').
card_uid('treasure mage'/'pMGD', 'pMGD:Treasure Mage:treasure mage').
card_rarity('treasure mage'/'pMGD', 'Special').
card_artist('treasure mage'/'pMGD', 'Greg Staples').
card_number('treasure mage'/'pMGD', '6').

card_in_set('trostani\'s summoner', 'pMGD').
card_original_type('trostani\'s summoner'/'pMGD', 'Creature — Elf Shaman').
card_original_text('trostani\'s summoner'/'pMGD', '').
card_first_print('trostani\'s summoner', 'pMGD').
card_image_name('trostani\'s summoner'/'pMGD', 'trostani\'s summoner').
card_uid('trostani\'s summoner'/'pMGD', 'pMGD:Trostani\'s Summoner:trostani\'s summoner').
card_rarity('trostani\'s summoner'/'pMGD', 'Special').
card_artist('trostani\'s summoner'/'pMGD', 'Nils Hamm').
card_number('trostani\'s summoner'/'pMGD', '27').

card_in_set('utter end', 'pMGD').
card_original_type('utter end'/'pMGD', 'Instant').
card_original_text('utter end'/'pMGD', '').
card_image_name('utter end'/'pMGD', 'utter end').
card_uid('utter end'/'pMGD', 'pMGD:Utter End:utter end').
card_rarity('utter end'/'pMGD', 'Special').
card_artist('utter end'/'pMGD', 'Jack Wang').
card_number('utter end'/'pMGD', '38').
card_flavor_text('utter end'/'pMGD', '\"I came seeking a challenge. All I found was you.\"\n—Zurgo, khan of the Mardu').

card_in_set('zameck guildmage', 'pMGD').
card_original_type('zameck guildmage'/'pMGD', 'Creature — Elf Wizard').
card_original_text('zameck guildmage'/'pMGD', '').
card_first_print('zameck guildmage', 'pMGD').
card_image_name('zameck guildmage'/'pMGD', 'zameck guildmage').
card_uid('zameck guildmage'/'pMGD', 'pMGD:Zameck Guildmage:zameck guildmage').
card_rarity('zameck guildmage'/'pMGD', 'Special').
card_artist('zameck guildmage'/'pMGD', 'Raymond Swanland').
card_number('zameck guildmage'/'pMGD', '25').

card_in_set('zombie apocalypse', 'pMGD').
card_original_type('zombie apocalypse'/'pMGD', 'Sorcery').
card_original_text('zombie apocalypse'/'pMGD', '').
card_first_print('zombie apocalypse', 'pMGD').
card_image_name('zombie apocalypse'/'pMGD', 'zombie apocalypse').
card_uid('zombie apocalypse'/'pMGD', 'pMGD:Zombie Apocalypse:zombie apocalypse').
card_rarity('zombie apocalypse'/'pMGD', 'Special').
card_artist('zombie apocalypse'/'pMGD', 'Volkan Baga').
card_number('zombie apocalypse'/'pMGD', '14').
