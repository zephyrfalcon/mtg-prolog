% Modern Event Deck 2014

set('MD1').
set_name('MD1', 'Modern Event Deck 2014').
set_release_date('MD1', '2014-05-30').
set_border('MD1', 'black').
set_type('MD1', 'box').

card_in_set('burrenton forge-tender', 'MD1').
card_original_type('burrenton forge-tender'/'MD1', 'Creature — Kithkin Wizard').
card_original_text('burrenton forge-tender'/'MD1', 'Protection from red\nSacrifice Burrenton Forge-Tender: Prevent all damage a red source of your choice would deal this turn.').
card_image_name('burrenton forge-tender'/'MD1', 'burrenton forge-tender').
card_uid('burrenton forge-tender'/'MD1', 'MD1:Burrenton Forge-Tender:burrenton forge-tender').
card_rarity('burrenton forge-tender'/'MD1', 'Uncommon').
card_artist('burrenton forge-tender'/'MD1', 'Chuck Lukacs').
card_number('burrenton forge-tender'/'MD1', '22').
card_flavor_text('burrenton forge-tender'/'MD1', '\"We are a clachan of smiths. The forge is as comfortable to us as a small fire during a cool winter\'s evening.\"').
card_multiverse_id('burrenton forge-tender'/'MD1', '382179').

card_in_set('caves of koilos', 'MD1').
card_original_type('caves of koilos'/'MD1', 'Land').
card_original_text('caves of koilos'/'MD1', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Caves of Koilos deals 1 damage to you.').
card_image_name('caves of koilos'/'MD1', 'caves of koilos').
card_uid('caves of koilos'/'MD1', 'MD1:Caves of Koilos:caves of koilos').
card_rarity('caves of koilos'/'MD1', 'Rare').
card_artist('caves of koilos'/'MD1', 'Jim Nelson').
card_number('caves of koilos'/'MD1', '14').
card_multiverse_id('caves of koilos'/'MD1', '382180').

card_in_set('city of brass', 'MD1').
card_original_type('city of brass'/'MD1', 'Land').
card_original_text('city of brass'/'MD1', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('city of brass'/'MD1', 'city of brass').
card_uid('city of brass'/'MD1', 'MD1:City of Brass:city of brass').
card_rarity('city of brass'/'MD1', 'Rare').
card_artist('city of brass'/'MD1', 'Jung Park').
card_number('city of brass'/'MD1', '15').
card_flavor_text('city of brass'/'MD1', '\"There is much to learn here, but few can endure the ringing of the spires.\"\n—Nulakam the Archivist').
card_multiverse_id('city of brass'/'MD1', '382181').

card_in_set('dismember', 'MD1').
card_original_type('dismember'/'MD1', 'Instant').
card_original_text('dismember'/'MD1', '({B/P} can be paid with either {B} or 2 life.)\nTarget creature gets -5/-5 until end of turn.').
card_image_name('dismember'/'MD1', 'dismember').
card_uid('dismember'/'MD1', 'MD1:Dismember:dismember').
card_rarity('dismember'/'MD1', 'Uncommon').
card_artist('dismember'/'MD1', 'Terese Nielsen').
card_number('dismember'/'MD1', '25').
card_flavor_text('dismember'/'MD1', '\"You serve Phyrexia. Your pieces would better serve Phyrexia elsewhere.\"\n—Azax-Azog, the Demon Thane').
card_multiverse_id('dismember'/'MD1', '382182').

card_in_set('duress', 'MD1').
card_original_type('duress'/'MD1', 'Sorcery').
card_original_text('duress'/'MD1', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'MD1', 'duress').
card_uid('duress'/'MD1', 'MD1:Duress:duress').
card_rarity('duress'/'MD1', 'Common').
card_artist('duress'/'MD1', 'Steven Belledin').
card_number('duress'/'MD1', '23').
card_flavor_text('duress'/'MD1', '\"It hurts more if you think about it.\"\n—Hooks, Cabal torturer').
card_multiverse_id('duress'/'MD1', '382183').

card_in_set('elspeth, knight-errant', 'MD1').
card_original_type('elspeth, knight-errant'/'MD1', 'Planeswalker — Elspeth').
card_original_text('elspeth, knight-errant'/'MD1', '+1: Put a 1/1 white Soldier creature token onto the battlefield.\n+1: Target creature gets +3/+3 and gains flying until end of turn.\n-8: You get an emblem with \"Artifacts, creatures, enchantments, and lands you control have indestructible.\"').
card_image_name('elspeth, knight-errant'/'MD1', 'elspeth, knight-errant').
card_uid('elspeth, knight-errant'/'MD1', 'MD1:Elspeth, Knight-Errant:elspeth, knight-errant').
card_rarity('elspeth, knight-errant'/'MD1', 'Mythic Rare').
card_artist('elspeth, knight-errant'/'MD1', 'Volkan Baga').
card_number('elspeth, knight-errant'/'MD1', '13').
card_multiverse_id('elspeth, knight-errant'/'MD1', '382184').

card_in_set('ghost quarter', 'MD1').
card_original_type('ghost quarter'/'MD1', 'Land').
card_original_text('ghost quarter'/'MD1', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Ghost Quarter: Destroy target land. Its controller may search his or her library for a basic land card, put it onto the battlefield, then shuffle his or her library.').
card_image_name('ghost quarter'/'MD1', 'ghost quarter').
card_uid('ghost quarter'/'MD1', 'MD1:Ghost Quarter:ghost quarter').
card_rarity('ghost quarter'/'MD1', 'Uncommon').
card_artist('ghost quarter'/'MD1', 'Peter Mohrbacher').
card_number('ghost quarter'/'MD1', '26').
card_flavor_text('ghost quarter'/'MD1', 'Deserted, but not uninhabited.').
card_multiverse_id('ghost quarter'/'MD1', '382185').

card_in_set('honor of the pure', 'MD1').
card_original_type('honor of the pure'/'MD1', 'Enchantment').
card_original_text('honor of the pure'/'MD1', 'White creatures you control get +1/+1.').
card_image_name('honor of the pure'/'MD1', 'honor of the pure').
card_uid('honor of the pure'/'MD1', 'MD1:Honor of the Pure:honor of the pure').
card_rarity('honor of the pure'/'MD1', 'Rare').
card_artist('honor of the pure'/'MD1', 'Greg Staples').
card_number('honor of the pure'/'MD1', '6').
card_flavor_text('honor of the pure'/'MD1', 'Together the soldiers were like a golden blade, cutting down their enemies and scarring the darkness.').
card_multiverse_id('honor of the pure'/'MD1', '382186').

card_in_set('inquisition of kozilek', 'MD1').
card_original_type('inquisition of kozilek'/'MD1', 'Sorcery').
card_original_text('inquisition of kozilek'/'MD1', 'Target player reveals his or her hand. You choose a nonland card from it with converted mana cost 3 or less. That player discards that card.').
card_image_name('inquisition of kozilek'/'MD1', 'inquisition of kozilek').
card_uid('inquisition of kozilek'/'MD1', 'MD1:Inquisition of Kozilek:inquisition of kozilek').
card_rarity('inquisition of kozilek'/'MD1', 'Uncommon').
card_artist('inquisition of kozilek'/'MD1', 'Tomasz Jedruszek').
card_number('inquisition of kozilek'/'MD1', '4').
card_flavor_text('inquisition of kozilek'/'MD1', 'You will scream out your innermost secrets just to make it stop.').
card_multiverse_id('inquisition of kozilek'/'MD1', '382187').

card_in_set('intangible virtue', 'MD1').
card_original_type('intangible virtue'/'MD1', 'Enchantment').
card_original_text('intangible virtue'/'MD1', 'Creature tokens you control get +1/+1 and have vigilance.').
card_image_name('intangible virtue'/'MD1', 'intangible virtue').
card_uid('intangible virtue'/'MD1', 'MD1:Intangible Virtue:intangible virtue').
card_rarity('intangible virtue'/'MD1', 'Uncommon').
card_artist('intangible virtue'/'MD1', 'Clint Cearley').
card_number('intangible virtue'/'MD1', '7').
card_flavor_text('intangible virtue'/'MD1', 'In life, they were a motley crew: farmers, lords, cutpurses, priests. In death, they are united in singular, benevolent purpose.').
card_multiverse_id('intangible virtue'/'MD1', '382188').

card_in_set('isolated chapel', 'MD1').
card_original_type('isolated chapel'/'MD1', 'Land').
card_original_text('isolated chapel'/'MD1', 'Isolated Chapel enters the battlefield tapped unless you control a Plains or a Swamp.\n{T}: Add {W} or {B} to your mana pool.').
card_image_name('isolated chapel'/'MD1', 'isolated chapel').
card_uid('isolated chapel'/'MD1', 'MD1:Isolated Chapel:isolated chapel').
card_rarity('isolated chapel'/'MD1', 'Rare').
card_artist('isolated chapel'/'MD1', 'Cliff Childs').
card_number('isolated chapel'/'MD1', '16').
card_flavor_text('isolated chapel'/'MD1', 'Not every church is a place of faith.').
card_multiverse_id('isolated chapel'/'MD1', '382189').

card_in_set('kataki, war\'s wage', 'MD1').
card_original_type('kataki, war\'s wage'/'MD1', 'Legendary Creature — Spirit').
card_original_text('kataki, war\'s wage'/'MD1', 'All artifacts have \"At the beginning of your upkeep, sacrifice this artifact unless you pay {1}.\"').
card_image_name('kataki, war\'s wage'/'MD1', 'kataki, war\'s wage').
card_uid('kataki, war\'s wage'/'MD1', 'MD1:Kataki, War\'s Wage:kataki, war\'s wage').
card_rarity('kataki, war\'s wage'/'MD1', 'Rare').
card_artist('kataki, war\'s wage'/'MD1', 'Matt Thompson').
card_number('kataki, war\'s wage'/'MD1', '24').
card_flavor_text('kataki, war\'s wage'/'MD1', '\"Before the war, we prayed to Kataki to sharpen our swords and harden our armor. Without his blessing our weapons are all but useless against the kami hordes.\"\n—Kenzo the Hardhearted').
card_multiverse_id('kataki, war\'s wage'/'MD1', '382190').

card_in_set('lingering souls', 'MD1').
card_original_type('lingering souls'/'MD1', 'Sorcery').
card_original_text('lingering souls'/'MD1', 'Put two 1/1 white Spirit creature tokens with flying onto the battlefield.\nFlashback {1}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('lingering souls'/'MD1', 'lingering souls').
card_uid('lingering souls'/'MD1', 'MD1:Lingering Souls:lingering souls').
card_rarity('lingering souls'/'MD1', 'Uncommon').
card_artist('lingering souls'/'MD1', 'Bud Cook').
card_number('lingering souls'/'MD1', '11').
card_flavor_text('lingering souls'/'MD1', 'The murdered inhabitants of Hollowhenge impart to the living the terror they felt in death.').
card_multiverse_id('lingering souls'/'MD1', '382191').

card_in_set('path to exile', 'MD1').
card_original_type('path to exile'/'MD1', 'Instant').
card_original_text('path to exile'/'MD1', 'Exile target creature. Its controller may search his or her library for a basic land card, put that card onto the battlefield tapped, then shuffle his or her library.').
card_image_name('path to exile'/'MD1', 'path to exile').
card_uid('path to exile'/'MD1', 'MD1:Path to Exile:path to exile').
card_rarity('path to exile'/'MD1', 'Uncommon').
card_artist('path to exile'/'MD1', 'Todd Lockwood').
card_number('path to exile'/'MD1', '3').
card_multiverse_id('path to exile'/'MD1', '382192').

card_in_set('plains', 'MD1').
card_original_type('plains'/'MD1', 'Basic Land — Plains').
card_original_text('plains'/'MD1', 'W').
card_image_name('plains'/'MD1', 'plains').
card_uid('plains'/'MD1', 'MD1:Plains:plains').
card_rarity('plains'/'MD1', 'Basic Land').
card_artist('plains'/'MD1', 'Adam Paquette').
card_number('plains'/'MD1', '19').
card_multiverse_id('plains'/'MD1', '382193').

card_in_set('raise the alarm', 'MD1').
card_original_type('raise the alarm'/'MD1', 'Instant').
card_original_text('raise the alarm'/'MD1', 'Put two 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('raise the alarm'/'MD1', 'raise the alarm').
card_uid('raise the alarm'/'MD1', 'MD1:Raise the Alarm:raise the alarm').
card_rarity('raise the alarm'/'MD1', 'Common').
card_artist('raise the alarm'/'MD1', 'John Matson').
card_number('raise the alarm'/'MD1', '8').
card_flavor_text('raise the alarm'/'MD1', '\"The nim raid our homes without warning. We must defend our homes without hesitation.\"').
card_multiverse_id('raise the alarm'/'MD1', '382194').

card_in_set('relic of progenitus', 'MD1').
card_original_type('relic of progenitus'/'MD1', 'Artifact').
card_original_text('relic of progenitus'/'MD1', '{T}: Target player exiles a card from his or her graveyard.\n{1}, Exile Relic of Progenitus: Exile all cards from all graveyards. Draw a card.').
card_image_name('relic of progenitus'/'MD1', 'relic of progenitus').
card_uid('relic of progenitus'/'MD1', 'MD1:Relic of Progenitus:relic of progenitus').
card_rarity('relic of progenitus'/'MD1', 'Uncommon').
card_artist('relic of progenitus'/'MD1', 'Jean-Sébastien Rossbach').
card_number('relic of progenitus'/'MD1', '21').
card_flavor_text('relic of progenitus'/'MD1', 'Elves believe the hydra-god Progenitus sleeps beneath Naya, feeding on forgotten magics.').
card_multiverse_id('relic of progenitus'/'MD1', '382195').

card_in_set('shrine of loyal legions', 'MD1').
card_original_type('shrine of loyal legions'/'MD1', 'Artifact').
card_original_text('shrine of loyal legions'/'MD1', 'At the beginning of your upkeep or whenever you cast a white spell, put a charge counter on Shrine of Loyal Legions.\n{3}, {T}, Sacrifice Shrine of Loyal Legions: Put a 1/1 colorless Myr artifact creature token onto the battlefield for each charge counter on Shrine of Loyal Legions.').
card_image_name('shrine of loyal legions'/'MD1', 'shrine of loyal legions').
card_uid('shrine of loyal legions'/'MD1', 'MD1:Shrine of Loyal Legions:shrine of loyal legions').
card_rarity('shrine of loyal legions'/'MD1', 'Uncommon').
card_artist('shrine of loyal legions'/'MD1', 'Igor Kieryluk').
card_number('shrine of loyal legions'/'MD1', '5').
card_multiverse_id('shrine of loyal legions'/'MD1', '382196').

card_in_set('soul warden', 'MD1').
card_original_type('soul warden'/'MD1', 'Creature — Human Cleric').
card_original_text('soul warden'/'MD1', 'Whenever another creature enters the battlefield, you gain 1 life.').
card_image_name('soul warden'/'MD1', 'soul warden').
card_uid('soul warden'/'MD1', 'MD1:Soul Warden:soul warden').
card_rarity('soul warden'/'MD1', 'Common').
card_artist('soul warden'/'MD1', 'Randy Gallegos').
card_number('soul warden'/'MD1', '1').
card_flavor_text('soul warden'/'MD1', '\"One does not question the size or shape of the grains of sand in an hourglass. Nor do I question the temperament of the souls under my guidance.\"').
card_multiverse_id('soul warden'/'MD1', '382197').

card_in_set('spectral procession', 'MD1').
card_original_type('spectral procession'/'MD1', 'Sorcery').
card_original_text('spectral procession'/'MD1', 'Put three 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_image_name('spectral procession'/'MD1', 'spectral procession').
card_uid('spectral procession'/'MD1', 'MD1:Spectral Procession:spectral procession').
card_rarity('spectral procession'/'MD1', 'Uncommon').
card_artist('spectral procession'/'MD1', 'Tomasz Jedruszek').
card_number('spectral procession'/'MD1', '12').
card_flavor_text('spectral procession'/'MD1', '\"I need never remember my past associates, my victims, my mistakes. Innistrad preserves them all for me.\"\n—Sorin Markov').
card_multiverse_id('spectral procession'/'MD1', '382198').

card_in_set('swamp', 'MD1').
card_original_type('swamp'/'MD1', 'Basic Land — Swamp').
card_original_text('swamp'/'MD1', 'B').
card_image_name('swamp'/'MD1', 'swamp').
card_uid('swamp'/'MD1', 'MD1:Swamp:swamp').
card_rarity('swamp'/'MD1', 'Basic Land').
card_artist('swamp'/'MD1', 'Adam Paquette').
card_number('swamp'/'MD1', '20').
card_multiverse_id('swamp'/'MD1', '382199').

card_in_set('sword of feast and famine', 'MD1').
card_original_type('sword of feast and famine'/'MD1', 'Artifact — Equipment').
card_original_text('sword of feast and famine'/'MD1', 'Equipped creature gets +2/+2 and has protection from black and from green.\nWhenever equipped creature deals combat damage to a player, that player discards a card and you untap all lands you control.\nEquip {2}').
card_image_name('sword of feast and famine'/'MD1', 'sword of feast and famine').
card_uid('sword of feast and famine'/'MD1', 'MD1:Sword of Feast and Famine:sword of feast and famine').
card_rarity('sword of feast and famine'/'MD1', 'Mythic Rare').
card_artist('sword of feast and famine'/'MD1', 'Chris Rahn').
card_number('sword of feast and famine'/'MD1', '10').
card_multiverse_id('sword of feast and famine'/'MD1', '382200').

card_in_set('tidehollow sculler', 'MD1').
card_original_type('tidehollow sculler'/'MD1', 'Artifact Creature — Zombie').
card_original_text('tidehollow sculler'/'MD1', 'When Tidehollow Sculler enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card.\nWhen Tidehollow Sculler leaves the battlefield, return the exiled card to its owner\'s hand.').
card_image_name('tidehollow sculler'/'MD1', 'tidehollow sculler').
card_uid('tidehollow sculler'/'MD1', 'MD1:Tidehollow Sculler:tidehollow sculler').
card_rarity('tidehollow sculler'/'MD1', 'Uncommon').
card_artist('tidehollow sculler'/'MD1', 'rk post').
card_number('tidehollow sculler'/'MD1', '2').
card_multiverse_id('tidehollow sculler'/'MD1', '382201').

card_in_set('vault of the archangel', 'MD1').
card_original_type('vault of the archangel'/'MD1', 'Land').
card_original_text('vault of the archangel'/'MD1', '{T}: Add {1} to your mana pool.\n{2}{W}{B}, {T}: Creatures you control gain deathtouch and lifelink until end of turn.').
card_image_name('vault of the archangel'/'MD1', 'vault of the archangel').
card_uid('vault of the archangel'/'MD1', 'MD1:Vault of the Archangel:vault of the archangel').
card_rarity('vault of the archangel'/'MD1', 'Rare').
card_artist('vault of the archangel'/'MD1', 'John Avon').
card_number('vault of the archangel'/'MD1', '17').
card_flavor_text('vault of the archangel'/'MD1', '\"For centuries my creation kept this world in balance. Now only her shadow remains.\"\n—Sorin Markov').
card_multiverse_id('vault of the archangel'/'MD1', '382202').

card_in_set('windbrisk heights', 'MD1').
card_original_type('windbrisk heights'/'MD1', 'Land').
card_original_text('windbrisk heights'/'MD1', 'Hideaway (This land enters the battlefield tapped. When it does, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library.)\n{T}: Add {W} to your mana pool.\n{W}, {T}: You may play the exiled card without paying its mana cost if you attacked with three or more creatures this turn.').
card_image_name('windbrisk heights'/'MD1', 'windbrisk heights').
card_uid('windbrisk heights'/'MD1', 'MD1:Windbrisk Heights:windbrisk heights').
card_rarity('windbrisk heights'/'MD1', 'Rare').
card_artist('windbrisk heights'/'MD1', 'Omar Rayyan').
card_number('windbrisk heights'/'MD1', '18').
card_multiverse_id('windbrisk heights'/'MD1', '382203').

card_in_set('zealous persecution', 'MD1').
card_original_type('zealous persecution'/'MD1', 'Instant').
card_original_text('zealous persecution'/'MD1', 'Until end of turn, creatures you control get +1/+1 and creatures your opponents control get -1/-1.').
card_image_name('zealous persecution'/'MD1', 'zealous persecution').
card_uid('zealous persecution'/'MD1', 'MD1:Zealous Persecution:zealous persecution').
card_rarity('zealous persecution'/'MD1', 'Uncommon').
card_artist('zealous persecution'/'MD1', 'Christopher Moeller').
card_number('zealous persecution'/'MD1', '9').
card_flavor_text('zealous persecution'/'MD1', '\"Jharic returned from Grixis changed, a haunted look in her eyes. She destroyed them with an unholy glee that made me shudder.\"\n—Knight-Captain Wyhorn').
card_multiverse_id('zealous persecution'/'MD1', '382204').
