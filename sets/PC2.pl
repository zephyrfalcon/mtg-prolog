% Planechase 2012 Edition

set('PC2').
set_name('PC2', 'Planechase 2012 Edition').
set_release_date('PC2', '2012-06-01').
set_border('PC2', 'black').
set_type('PC2', 'planechase').

card_in_set('akoum', 'PC2').
card_original_type('akoum'/'PC2', 'Plane — Zendikar').
card_original_text('akoum'/'PC2', 'Players may cast enchantment cards as though they had flash.\nWhenever you roll {C}, destroy target creature that isn\'t enchanted.').
card_first_print('akoum', 'PC2').
card_image_name('akoum'/'PC2', 'akoum').
card_uid('akoum'/'PC2', 'PC2:Akoum:akoum').
card_rarity('akoum'/'PC2', 'Common').
card_artist('akoum'/'PC2', 'Rob Alexander').
card_number('akoum'/'PC2', '9').
card_multiverse_id('akoum'/'PC2', '226512').

card_in_set('arc trail', 'PC2').
card_original_type('arc trail'/'PC2', 'Sorcery').
card_original_text('arc trail'/'PC2', 'Arc Trail deals 2 damage to target creature or player and 1 damage to another target creature or player.').
card_image_name('arc trail'/'PC2', 'arc trail').
card_uid('arc trail'/'PC2', 'PC2:Arc Trail:arc trail').
card_rarity('arc trail'/'PC2', 'Uncommon').
card_artist('arc trail'/'PC2', 'Marc Simonetti').
card_number('arc trail'/'PC2', '39').
card_flavor_text('arc trail'/'PC2', '\"Don\'t try to hit your enemies. Concentrate on the space between them, and fill the air with doom.\"\n—Spear-Tribe teaching').
card_multiverse_id('arc trail'/'PC2', '275254').

card_in_set('aretopolis', 'PC2').
card_original_type('aretopolis'/'PC2', 'Plane — Kephalai').
card_original_text('aretopolis'/'PC2', 'When you planeswalk to Aretopolis or at the beginning of your upkeep, put a scroll counter on Aretopolis, then you gain life equal to the number of scroll counters on it.\nWhen Aretopolis has ten or more scroll counters on it, planeswalk.\nWhenever you roll {C}, put a scroll counter on Aretopolis, then draw cards equal to the number of scroll counters on it.').
card_first_print('aretopolis', 'PC2').
card_image_name('aretopolis'/'PC2', 'aretopolis').
card_uid('aretopolis'/'PC2', 'PC2:Aretopolis:aretopolis').
card_rarity('aretopolis'/'PC2', 'Common').
card_artist('aretopolis'/'PC2', 'Christopher Moeller').
card_number('aretopolis'/'PC2', '10').
card_multiverse_id('aretopolis'/'PC2', '226517').

card_in_set('armillary sphere', 'PC2').
card_original_type('armillary sphere'/'PC2', 'Artifact').
card_original_text('armillary sphere'/'PC2', '{2}, {T}, Sacrifice Armillary Sphere: Search your library for up to two basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('armillary sphere'/'PC2', 'armillary sphere').
card_uid('armillary sphere'/'PC2', 'PC2:Armillary Sphere:armillary sphere').
card_rarity('armillary sphere'/'PC2', 'Common').
card_artist('armillary sphere'/'PC2', 'Franz Vohwinkel').
card_number('armillary sphere'/'PC2', '108').
card_flavor_text('armillary sphere'/'PC2', 'The mysterious purpose of two of the rings had eluded Esper mages—until now.').
card_multiverse_id('armillary sphere'/'PC2', '271137').

card_in_set('armored griffin', 'PC2').
card_original_type('armored griffin'/'PC2', 'Creature — Griffin').
card_original_text('armored griffin'/'PC2', 'Flying, vigilance').
card_image_name('armored griffin'/'PC2', 'armored griffin').
card_uid('armored griffin'/'PC2', 'PC2:Armored Griffin:armored griffin').
card_rarity('armored griffin'/'PC2', 'Uncommon').
card_artist('armored griffin'/'PC2', 'Brad Rigney').
card_number('armored griffin'/'PC2', '1').
card_flavor_text('armored griffin'/'PC2', 'When an order for griffin barding comes in, blacksmiths prepare their forges for months of exacting work. They know griffins always detect imperfections and never haggle.').
card_multiverse_id('armored griffin'/'PC2', '271234').

card_in_set('assassinate', 'PC2').
card_original_type('assassinate'/'PC2', 'Sorcery').
card_original_text('assassinate'/'PC2', 'Destroy target tapped creature.').
card_image_name('assassinate'/'PC2', 'assassinate').
card_uid('assassinate'/'PC2', 'PC2:Assassinate:assassinate').
card_rarity('assassinate'/'PC2', 'Common').
card_artist('assassinate'/'PC2', 'Kev Walker').
card_number('assassinate'/'PC2', '30').
card_flavor_text('assassinate'/'PC2', '\"This is how wars are won—not with armies of soldiers but with a single knife blade, artfully placed.\"\n—Yurin, royal assassin').
card_multiverse_id('assassinate'/'PC2', '271218').

card_in_set('astral arena', 'PC2').
card_original_type('astral arena'/'PC2', 'Plane — Kolbahan').
card_original_text('astral arena'/'PC2', 'No more than one creature can attack each combat.\nNo more than one creature can block each combat.\nWhenever you roll {C}, Astral Arena deals 2 damage to each creature.').
card_first_print('astral arena', 'PC2').
card_image_name('astral arena'/'PC2', 'astral arena').
card_uid('astral arena'/'PC2', 'PC2:Astral Arena:astral arena').
card_rarity('astral arena'/'PC2', 'Common').
card_artist('astral arena'/'PC2', 'Sam Burley').
card_number('astral arena'/'PC2', '11').
card_multiverse_id('astral arena'/'PC2', '226526').

card_in_set('augury owl', 'PC2').
card_original_type('augury owl'/'PC2', 'Creature — Bird').
card_original_text('augury owl'/'PC2', 'Flying\nWhen Augury Owl enters the battlefield, scry 3. (To scry 3, look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('augury owl'/'PC2', 'augury owl').
card_uid('augury owl'/'PC2', 'PC2:Augury Owl:augury owl').
card_rarity('augury owl'/'PC2', 'Common').
card_artist('augury owl'/'PC2', 'Jim Nelson').
card_number('augury owl'/'PC2', '14').
card_multiverse_id('augury owl'/'PC2', '271192').

card_in_set('aura gnarlid', 'PC2').
card_original_type('aura gnarlid'/'PC2', 'Creature — Beast').
card_original_text('aura gnarlid'/'PC2', 'Creatures with power less than Aura Gnarlid\'s power can\'t block it.\nAura Gnarlid gets +1/+1 for each Aura on the battlefield.').
card_image_name('aura gnarlid'/'PC2', 'aura gnarlid').
card_uid('aura gnarlid'/'PC2', 'PC2:Aura Gnarlid:aura gnarlid').
card_rarity('aura gnarlid'/'PC2', 'Common').
card_artist('aura gnarlid'/'PC2', 'Lars Grant-West').
card_number('aura gnarlid'/'PC2', '55').
card_flavor_text('aura gnarlid'/'PC2', 'Kill a gnarlid with your first blow, or it\'ll cheerfully show you how it\'s done.').
card_multiverse_id('aura gnarlid'/'PC2', '271147').

card_in_set('auramancer', 'PC2').
card_original_type('auramancer'/'PC2', 'Creature — Human Wizard').
card_original_text('auramancer'/'PC2', 'When Auramancer enters the battlefield, you may return target enchantment card from your graveyard to your hand.').
card_image_name('auramancer'/'PC2', 'auramancer').
card_uid('auramancer'/'PC2', 'PC2:Auramancer:auramancer').
card_rarity('auramancer'/'PC2', 'Common').
card_artist('auramancer'/'PC2', 'Rebecca Guay').
card_number('auramancer'/'PC2', '2').
card_flavor_text('auramancer'/'PC2', '\"In memories, we can find our deepest reserves of strength.\"').
card_multiverse_id('auramancer'/'PC2', '271171').

card_in_set('auratouched mage', 'PC2').
card_original_type('auratouched mage'/'PC2', 'Creature — Human Wizard').
card_original_text('auratouched mage'/'PC2', 'When Auratouched Mage enters the battlefield, search your library for an Aura card that could enchant it. If Auratouched Mage is still on the battlefield, put that Aura card onto the battlefield attached to it. Otherwise, reveal the Aura card and put it into your hand. Then shuffle your library.').
card_image_name('auratouched mage'/'PC2', 'auratouched mage').
card_uid('auratouched mage'/'PC2', 'PC2:Auratouched Mage:auratouched mage').
card_rarity('auratouched mage'/'PC2', 'Uncommon').
card_artist('auratouched mage'/'PC2', 'Jeff Miracola').
card_number('auratouched mage'/'PC2', '3').
card_multiverse_id('auratouched mage'/'PC2', '271207').

card_in_set('awakening zone', 'PC2').
card_original_type('awakening zone'/'PC2', 'Enchantment').
card_original_text('awakening zone'/'PC2', 'At the beginning of your upkeep, you may put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('awakening zone'/'PC2', 'awakening zone').
card_uid('awakening zone'/'PC2', 'PC2:Awakening Zone:awakening zone').
card_rarity('awakening zone'/'PC2', 'Rare').
card_artist('awakening zone'/'PC2', 'Johann Bodin').
card_number('awakening zone'/'PC2', '56').
card_flavor_text('awakening zone'/'PC2', 'The ground erupted in time with the hedron\'s thrum, a dirge of the last days.').
card_multiverse_id('awakening zone'/'PC2', '271156').

card_in_set('baleful strix', 'PC2').
card_original_type('baleful strix'/'PC2', 'Artifact Creature — Bird').
card_original_text('baleful strix'/'PC2', 'Flying, deathtouch\nWhen Baleful Strix enters the battlefield, draw a card.').
card_first_print('baleful strix', 'PC2').
card_image_name('baleful strix'/'PC2', 'baleful strix').
card_uid('baleful strix'/'PC2', 'PC2:Baleful Strix:baleful strix').
card_rarity('baleful strix'/'PC2', 'Uncommon').
card_artist('baleful strix'/'PC2', 'Nils Hamm').
card_number('baleful strix'/'PC2', '82').
card_flavor_text('baleful strix'/'PC2', 'Its beak rends flesh and bone, exposing the tender marrow of dream.').
card_multiverse_id('baleful strix'/'PC2', '265156').

card_in_set('beast within', 'PC2').
card_original_type('beast within'/'PC2', 'Instant').
card_original_text('beast within'/'PC2', 'Destroy target permanent. Its controller puts a 3/3 green Beast creature token onto the battlefield.').
card_image_name('beast within'/'PC2', 'beast within').
card_uid('beast within'/'PC2', 'PC2:Beast Within:beast within').
card_rarity('beast within'/'PC2', 'Uncommon').
card_artist('beast within'/'PC2', 'Dave Allsop').
card_number('beast within'/'PC2', '57').
card_flavor_text('beast within'/'PC2', '\"Kill the weak so they can\'t drag the strong down to their level. This is true compassion.\"\n—Benzir, archdruid of Temple Might').
card_multiverse_id('beast within'/'PC2', '275255').

card_in_set('beetleback chief', 'PC2').
card_original_type('beetleback chief'/'PC2', 'Creature — Goblin Warrior').
card_original_text('beetleback chief'/'PC2', 'When Beetleback Chief enters the battlefield, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_first_print('beetleback chief', 'PC2').
card_image_name('beetleback chief'/'PC2', 'beetleback chief').
card_uid('beetleback chief'/'PC2', 'PC2:Beetleback Chief:beetleback chief').
card_rarity('beetleback chief'/'PC2', 'Uncommon').
card_artist('beetleback chief'/'PC2', 'Wayne England').
card_number('beetleback chief'/'PC2', '40').
card_flavor_text('beetleback chief'/'PC2', 'Whether trained, ridden, or eaten, few goblin military innovations have rivaled the bug.').
card_multiverse_id('beetleback chief'/'PC2', '265145').

card_in_set('bituminous blast', 'PC2').
card_original_type('bituminous blast'/'PC2', 'Instant').
card_original_text('bituminous blast'/'PC2', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nBituminous Blast deals 4 damage to target creature.').
card_image_name('bituminous blast'/'PC2', 'bituminous blast').
card_uid('bituminous blast'/'PC2', 'PC2:Bituminous Blast:bituminous blast').
card_rarity('bituminous blast'/'PC2', 'Uncommon').
card_artist('bituminous blast'/'PC2', 'Raymond Swanland').
card_number('bituminous blast'/'PC2', '83').
card_multiverse_id('bituminous blast'/'PC2', '271166').

card_in_set('bloodbraid elf', 'PC2').
card_original_type('bloodbraid elf'/'PC2', 'Creature — Elf Berserker').
card_original_text('bloodbraid elf'/'PC2', 'Haste\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_image_name('bloodbraid elf'/'PC2', 'bloodbraid elf').
card_uid('bloodbraid elf'/'PC2', 'PC2:Bloodbraid Elf:bloodbraid elf').
card_rarity('bloodbraid elf'/'PC2', 'Uncommon').
card_artist('bloodbraid elf'/'PC2', 'Steve Argyle').
card_number('bloodbraid elf'/'PC2', '84').
card_multiverse_id('bloodbraid elf'/'PC2', '271167').

card_in_set('bloodhill bastion', 'PC2').
card_original_type('bloodhill bastion'/'PC2', 'Plane — Equilor').
card_original_text('bloodhill bastion'/'PC2', 'Whenever a creature enters the battlefield, it gains double strike and haste until end of turn.\nWhenever you roll {C}, exile target nontoken creature you control, then return it to the battlefield under your control.').
card_first_print('bloodhill bastion', 'PC2').
card_image_name('bloodhill bastion'/'PC2', 'bloodhill bastion').
card_uid('bloodhill bastion'/'PC2', 'PC2:Bloodhill Bastion:bloodhill bastion').
card_rarity('bloodhill bastion'/'PC2', 'Common').
card_artist('bloodhill bastion'/'PC2', 'Mark Hyzer').
card_number('bloodhill bastion'/'PC2', '12').
card_multiverse_id('bloodhill bastion'/'PC2', '226539').

card_in_set('boar umbra', 'PC2').
card_original_type('boar umbra'/'PC2', 'Enchantment — Aura').
card_original_text('boar umbra'/'PC2', 'Enchant creature\nEnchanted creature gets +3/+3.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_image_name('boar umbra'/'PC2', 'boar umbra').
card_uid('boar umbra'/'PC2', 'PC2:Boar Umbra:boar umbra').
card_rarity('boar umbra'/'PC2', 'Uncommon').
card_artist('boar umbra'/'PC2', 'Christopher Moeller').
card_number('boar umbra'/'PC2', '58').
card_flavor_text('boar umbra'/'PC2', '\"From in here, everything looks like dinner.\"').
card_multiverse_id('boar umbra'/'PC2', '271148').

card_in_set('bramble elemental', 'PC2').
card_original_type('bramble elemental'/'PC2', 'Creature — Elemental').
card_original_text('bramble elemental'/'PC2', 'Whenever an Aura becomes attached to Bramble Elemental or enters the battlefield attached to Bramble Elemental, put two 1/1 green Saproling creature tokens onto the battlefield.').
card_image_name('bramble elemental'/'PC2', 'bramble elemental').
card_uid('bramble elemental'/'PC2', 'PC2:Bramble Elemental:bramble elemental').
card_rarity('bramble elemental'/'PC2', 'Common').
card_artist('bramble elemental'/'PC2', 'Tomas Giorello').
card_number('bramble elemental'/'PC2', '59').
card_flavor_text('bramble elemental'/'PC2', 'In abandoned corners of Ravnica, nature has begun to reclaim what it once owned.').
card_multiverse_id('bramble elemental'/'PC2', '271208').

card_in_set('brindle shoat', 'PC2').
card_original_type('brindle shoat'/'PC2', 'Creature — Boar').
card_original_text('brindle shoat'/'PC2', 'When Brindle Shoat dies, put a 3/3 green Boar creature token onto the battlefield.').
card_first_print('brindle shoat', 'PC2').
card_image_name('brindle shoat'/'PC2', 'brindle shoat').
card_uid('brindle shoat'/'PC2', 'PC2:Brindle Shoat:brindle shoat').
card_rarity('brindle shoat'/'PC2', 'Uncommon').
card_artist('brindle shoat'/'PC2', 'Steven Belledin').
card_number('brindle shoat'/'PC2', '60').
card_flavor_text('brindle shoat'/'PC2', 'Hunters lure the stripling boar into the open, hoping to trap greater prey.').
card_multiverse_id('brindle shoat'/'PC2', '265141').

card_in_set('brutalizer exarch', 'PC2').
card_original_type('brutalizer exarch'/'PC2', 'Creature — Cleric').
card_original_text('brutalizer exarch'/'PC2', 'When Brutalizer Exarch enters the battlefield, choose one — Search your library for a creature card, reveal it, then shuffle your library and put that card on top of it; or put target noncreature permanent on the bottom of its owner\'s library.').
card_image_name('brutalizer exarch'/'PC2', 'brutalizer exarch').
card_uid('brutalizer exarch'/'PC2', 'PC2:Brutalizer Exarch:brutalizer exarch').
card_rarity('brutalizer exarch'/'PC2', 'Uncommon').
card_artist('brutalizer exarch'/'PC2', 'Mark Zug').
card_number('brutalizer exarch'/'PC2', '61').
card_multiverse_id('brutalizer exarch'/'PC2', '275256').

card_in_set('cadaver imp', 'PC2').
card_original_type('cadaver imp'/'PC2', 'Creature — Imp').
card_original_text('cadaver imp'/'PC2', 'Flying\nWhen Cadaver Imp enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('cadaver imp'/'PC2', 'cadaver imp').
card_uid('cadaver imp'/'PC2', 'PC2:Cadaver Imp:cadaver imp').
card_rarity('cadaver imp'/'PC2', 'Common').
card_artist('cadaver imp'/'PC2', 'Dave Kendall').
card_number('cadaver imp'/'PC2', '31').
card_flavor_text('cadaver imp'/'PC2', 'The mouth must be pried open before rigor mortis sets in. Otherwise the returning soul can find no ingress.').
card_multiverse_id('cadaver imp'/'PC2', '271157').

card_in_set('cage of hands', 'PC2').
card_original_type('cage of hands'/'PC2', 'Enchantment — Aura').
card_original_text('cage of hands'/'PC2', 'Enchant creature\nEnchanted creature can\'t attack or block.\n{1}{W}: Return Cage of Hands to its owner\'s hand.').
card_image_name('cage of hands'/'PC2', 'cage of hands').
card_uid('cage of hands'/'PC2', 'PC2:Cage of Hands:cage of hands').
card_rarity('cage of hands'/'PC2', 'Common').
card_artist('cage of hands'/'PC2', 'Mark Tedin').
card_number('cage of hands'/'PC2', '4').
card_multiverse_id('cage of hands'/'PC2', '271205').

card_in_set('cancel', 'PC2').
card_original_type('cancel'/'PC2', 'Instant').
card_original_text('cancel'/'PC2', 'Counter target spell.').
card_image_name('cancel'/'PC2', 'cancel').
card_uid('cancel'/'PC2', 'PC2:Cancel:cancel').
card_rarity('cancel'/'PC2', 'Common').
card_artist('cancel'/'PC2', 'David Palumbo').
card_number('cancel'/'PC2', '15').
card_flavor_text('cancel'/'PC2', '\"What you are attempting is not against the law. It is, however, extremely foolish.\"').
card_multiverse_id('cancel'/'PC2', '271172').

card_in_set('celestial ancient', 'PC2').
card_original_type('celestial ancient'/'PC2', 'Creature — Elemental').
card_original_text('celestial ancient'/'PC2', 'Flying\nWhenever you cast an enchantment spell, put a +1/+1 counter on each creature you control.').
card_image_name('celestial ancient'/'PC2', 'celestial ancient').
card_uid('celestial ancient'/'PC2', 'PC2:Celestial Ancient:celestial ancient').
card_rarity('celestial ancient'/'PC2', 'Rare').
card_artist('celestial ancient'/'PC2', 'Mark Tedin').
card_number('celestial ancient'/'PC2', '5').
card_flavor_text('celestial ancient'/'PC2', '\"We thought the clouds had moved from the night sky. Then the night sky moved, and the horizon grew wings.\"\n—Josuri').
card_multiverse_id('celestial ancient'/'PC2', '271236').

card_in_set('chaotic æther', 'PC2').
card_original_type('chaotic æther'/'PC2', 'Phenomenon').
card_original_text('chaotic æther'/'PC2', 'When you encounter Chaotic Æther, each blank roll of the planar die is a {C} roll until a player planeswalks away from a plane. (Then planeswalk away from this phenomenon.)').
card_first_print('chaotic æther', 'PC2').
card_image_name('chaotic æther'/'PC2', 'chaotic aether').
card_uid('chaotic æther'/'PC2', 'PC2:Chaotic Æther:chaotic aether').
card_rarity('chaotic æther'/'PC2', 'Common').
card_artist('chaotic æther'/'PC2', 'Dan Scott').
card_number('chaotic æther'/'PC2', '1').
card_multiverse_id('chaotic æther'/'PC2', '226509').

card_in_set('concentrate', 'PC2').
card_original_type('concentrate'/'PC2', 'Sorcery').
card_original_text('concentrate'/'PC2', 'Draw three cards.').
card_image_name('concentrate'/'PC2', 'concentrate').
card_uid('concentrate'/'PC2', 'PC2:Concentrate:concentrate').
card_rarity('concentrate'/'PC2', 'Uncommon').
card_artist('concentrate'/'PC2', 'rk post').
card_number('concentrate'/'PC2', '16').
card_flavor_text('concentrate'/'PC2', '\"The treasure in my mind is greater than any worldly glory.\"').
card_multiverse_id('concentrate'/'PC2', '271220').

card_in_set('cultivate', 'PC2').
card_original_type('cultivate'/'PC2', 'Sorcery').
card_original_text('cultivate'/'PC2', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_image_name('cultivate'/'PC2', 'cultivate').
card_uid('cultivate'/'PC2', 'PC2:Cultivate:cultivate').
card_rarity('cultivate'/'PC2', 'Common').
card_artist('cultivate'/'PC2', 'Anthony Palumbo').
card_number('cultivate'/'PC2', '62').
card_flavor_text('cultivate'/'PC2', 'All seeds share a common bond, calling to each other across infinity.').
card_multiverse_id('cultivate'/'PC2', '271193').

card_in_set('dark hatchling', 'PC2').
card_original_type('dark hatchling'/'PC2', 'Creature — Horror').
card_original_text('dark hatchling'/'PC2', 'Flying\nWhen Dark Hatchling enters the battlefield, destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark hatchling'/'PC2', 'dark hatchling').
card_uid('dark hatchling'/'PC2', 'PC2:Dark Hatchling:dark hatchling').
card_rarity('dark hatchling'/'PC2', 'Rare').
card_artist('dark hatchling'/'PC2', 'Brad Rigney').
card_number('dark hatchling'/'PC2', '32').
card_multiverse_id('dark hatchling'/'PC2', '271217').

card_in_set('deny reality', 'PC2').
card_original_type('deny reality'/'PC2', 'Sorcery').
card_original_text('deny reality'/'PC2', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nReturn target permanent to its owner\'s hand.').
card_image_name('deny reality'/'PC2', 'deny reality').
card_uid('deny reality'/'PC2', 'PC2:Deny Reality:deny reality').
card_rarity('deny reality'/'PC2', 'Common').
card_artist('deny reality'/'PC2', 'Jean-Sébastien Rossbach').
card_number('deny reality'/'PC2', '85').
card_multiverse_id('deny reality'/'PC2', '271168').

card_in_set('dimir aqueduct', 'PC2').
card_original_type('dimir aqueduct'/'PC2', 'Land').
card_original_text('dimir aqueduct'/'PC2', 'Dimir Aqueduct enters the battlefield tapped.\nWhen Dimir Aqueduct enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {U}{B} to your mana pool.').
card_image_name('dimir aqueduct'/'PC2', 'dimir aqueduct').
card_uid('dimir aqueduct'/'PC2', 'PC2:Dimir Aqueduct:dimir aqueduct').
card_rarity('dimir aqueduct'/'PC2', 'Common').
card_artist('dimir aqueduct'/'PC2', 'John Avon').
card_number('dimir aqueduct'/'PC2', '116').
card_multiverse_id('dimir aqueduct'/'PC2', '271209').

card_in_set('dimir infiltrator', 'PC2').
card_original_type('dimir infiltrator'/'PC2', 'Creature — Spirit').
card_original_text('dimir infiltrator'/'PC2', 'Dimir Infiltrator is unblockable.\nTransmute {1}{U}{B} ({1}{U}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_image_name('dimir infiltrator'/'PC2', 'dimir infiltrator').
card_uid('dimir infiltrator'/'PC2', 'PC2:Dimir Infiltrator:dimir infiltrator').
card_rarity('dimir infiltrator'/'PC2', 'Common').
card_artist('dimir infiltrator'/'PC2', 'Jim Nelson').
card_number('dimir infiltrator'/'PC2', '86').
card_multiverse_id('dimir infiltrator'/'PC2', '271214').

card_in_set('dowsing shaman', 'PC2').
card_original_type('dowsing shaman'/'PC2', 'Creature — Centaur Shaman').
card_original_text('dowsing shaman'/'PC2', '{2}{G}, {T}: Return target enchantment card from your graveyard to your hand.').
card_image_name('dowsing shaman'/'PC2', 'dowsing shaman').
card_uid('dowsing shaman'/'PC2', 'PC2:Dowsing Shaman:dowsing shaman').
card_rarity('dowsing shaman'/'PC2', 'Uncommon').
card_artist('dowsing shaman'/'PC2', 'Wayne Reynolds').
card_number('dowsing shaman'/'PC2', '63').
card_flavor_text('dowsing shaman'/'PC2', 'He combs the streets looking for the lost light, the discarded enigmas of the city.').
card_multiverse_id('dowsing shaman'/'PC2', '271210').

card_in_set('dragonlair spider', 'PC2').
card_original_type('dragonlair spider'/'PC2', 'Creature — Spider').
card_original_text('dragonlair spider'/'PC2', 'Reach\nWhenever an opponent casts a spell, put a 1/1 green Insect creature token onto the battlefield.').
card_first_print('dragonlair spider', 'PC2').
card_image_name('dragonlair spider'/'PC2', 'dragonlair spider').
card_uid('dragonlair spider'/'PC2', 'PC2:Dragonlair Spider:dragonlair spider').
card_rarity('dragonlair spider'/'PC2', 'Rare').
card_artist('dragonlair spider'/'PC2', 'Carl Critchlow').
card_number('dragonlair spider'/'PC2', '87').
card_flavor_text('dragonlair spider'/'PC2', 'Swarms thrive in its nest, feeding on leathery bits of discarded wing.').
card_multiverse_id('dragonlair spider'/'PC2', '270726').

card_in_set('dreampod druid', 'PC2').
card_original_type('dreampod druid'/'PC2', 'Creature — Human Druid').
card_original_text('dreampod druid'/'PC2', 'At the beginning of each upkeep, if Dreampod Druid is enchanted, put a 1/1 green Saproling creature token onto the battlefield.').
card_first_print('dreampod druid', 'PC2').
card_image_name('dreampod druid'/'PC2', 'dreampod druid').
card_uid('dreampod druid'/'PC2', 'PC2:Dreampod Druid:dreampod druid').
card_rarity('dreampod druid'/'PC2', 'Uncommon').
card_artist('dreampod druid'/'PC2', 'Wayne Reynolds').
card_number('dreampod druid'/'PC2', '64').
card_flavor_text('dreampod druid'/'PC2', '\"Don\'t mistake my creations for mere vegetation. They are my children, loyal and fierce.\"').
card_multiverse_id('dreampod druid'/'PC2', '270742').

card_in_set('edge of malacol', 'PC2').
card_original_type('edge of malacol'/'PC2', 'Plane — Belenon').
card_original_text('edge of malacol'/'PC2', 'If a creature you control would untap during your untap step, put two +1/+1 counters on it instead.\nWhenever you roll {C}, untap each creature you control.').
card_first_print('edge of malacol', 'PC2').
card_image_name('edge of malacol'/'PC2', 'edge of malacol').
card_uid('edge of malacol'/'PC2', 'PC2:Edge of Malacol:edge of malacol').
card_rarity('edge of malacol'/'PC2', 'Common').
card_artist('edge of malacol'/'PC2', 'Goran Josic').
card_number('edge of malacol'/'PC2', '13').
card_multiverse_id('edge of malacol'/'PC2', '226527').

card_in_set('elderwood scion', 'PC2').
card_original_type('elderwood scion'/'PC2', 'Creature — Elemental').
card_original_text('elderwood scion'/'PC2', 'Trample, lifelink\nSpells you cast that target Elderwood Scion cost {2} less to cast.\nSpells your opponents cast that target Elderwood Scion cost {2} more to cast.').
card_first_print('elderwood scion', 'PC2').
card_image_name('elderwood scion'/'PC2', 'elderwood scion').
card_uid('elderwood scion'/'PC2', 'PC2:Elderwood Scion:elderwood scion').
card_rarity('elderwood scion'/'PC2', 'Rare').
card_artist('elderwood scion'/'PC2', 'Nils Hamm').
card_number('elderwood scion'/'PC2', '88').
card_flavor_text('elderwood scion'/'PC2', 'The Sunbriar druids believe that every life begins in its heart and ends under its hooves.').
card_multiverse_id('elderwood scion'/'PC2', '270725').

card_in_set('enigma sphinx', 'PC2').
card_original_type('enigma sphinx'/'PC2', 'Artifact Creature — Sphinx').
card_original_text('enigma sphinx'/'PC2', 'Flying\nWhen Enigma Sphinx is put into your graveyard from the battlefield, put it into your library third from the top.\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_image_name('enigma sphinx'/'PC2', 'enigma sphinx').
card_uid('enigma sphinx'/'PC2', 'PC2:Enigma Sphinx:enigma sphinx').
card_rarity('enigma sphinx'/'PC2', 'Rare').
card_artist('enigma sphinx'/'PC2', 'Chris Rahn').
card_number('enigma sphinx'/'PC2', '89').
card_multiverse_id('enigma sphinx'/'PC2', '271169').

card_in_set('enlisted wurm', 'PC2').
card_original_type('enlisted wurm'/'PC2', 'Creature — Wurm').
card_original_text('enlisted wurm'/'PC2', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_image_name('enlisted wurm'/'PC2', 'enlisted wurm').
card_uid('enlisted wurm'/'PC2', 'PC2:Enlisted Wurm:enlisted wurm').
card_rarity('enlisted wurm'/'PC2', 'Uncommon').
card_artist('enlisted wurm'/'PC2', 'Steve Prescott').
card_number('enlisted wurm'/'PC2', '90').
card_flavor_text('enlisted wurm'/'PC2', 'A match for any army—even its own.').
card_multiverse_id('enlisted wurm'/'PC2', '271170').

card_in_set('erratic explosion', 'PC2').
card_original_type('erratic explosion'/'PC2', 'Sorcery').
card_original_text('erratic explosion'/'PC2', 'Choose target creature or player. Reveal cards from the top of your library until you reveal a nonland card. Erratic Explosion deals damage equal to that card\'s converted mana cost to that creature or player. Put the revealed cards on the bottom of your library in any order.').
card_image_name('erratic explosion'/'PC2', 'erratic explosion').
card_uid('erratic explosion'/'PC2', 'PC2:Erratic Explosion:erratic explosion').
card_rarity('erratic explosion'/'PC2', 'Common').
card_artist('erratic explosion'/'PC2', 'Gary Ruddell').
card_number('erratic explosion'/'PC2', '41').
card_multiverse_id('erratic explosion'/'PC2', '271191').

card_in_set('etherium-horn sorcerer', 'PC2').
card_original_type('etherium-horn sorcerer'/'PC2', 'Artifact Creature — Minotaur Wizard').
card_original_text('etherium-horn sorcerer'/'PC2', '{1}{U}{R}: Return Etherium-Horn Sorcerer to its owner\'s hand.\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_first_print('etherium-horn sorcerer', 'PC2').
card_image_name('etherium-horn sorcerer'/'PC2', 'etherium-horn sorcerer').
card_uid('etherium-horn sorcerer'/'PC2', 'PC2:Etherium-Horn Sorcerer:etherium-horn sorcerer').
card_rarity('etherium-horn sorcerer'/'PC2', 'Rare').
card_artist('etherium-horn sorcerer'/'PC2', 'Franz Vohwinkel').
card_number('etherium-horn sorcerer'/'PC2', '91').
card_multiverse_id('etherium-horn sorcerer'/'PC2', '265144').

card_in_set('exotic orchard', 'PC2').
card_original_type('exotic orchard'/'PC2', 'Land').
card_original_text('exotic orchard'/'PC2', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_image_name('exotic orchard'/'PC2', 'exotic orchard').
card_uid('exotic orchard'/'PC2', 'PC2:Exotic Orchard:exotic orchard').
card_rarity('exotic orchard'/'PC2', 'Rare').
card_artist('exotic orchard'/'PC2', 'Steven Belledin').
card_number('exotic orchard'/'PC2', '117').
card_flavor_text('exotic orchard'/'PC2', '\"It was a strange morning. When we awoke, we found our trees transformed. We didn\'t know whether to water them or polish them.\"\n—Pulan, Bant orchardist').
card_multiverse_id('exotic orchard'/'PC2', '275257').

card_in_set('farsight mask', 'PC2').
card_original_type('farsight mask'/'PC2', 'Artifact').
card_original_text('farsight mask'/'PC2', 'Whenever a source an opponent controls deals damage to you, if Farsight Mask is untapped, you may draw a card.').
card_image_name('farsight mask'/'PC2', 'farsight mask').
card_uid('farsight mask'/'PC2', 'PC2:Farsight Mask:farsight mask').
card_rarity('farsight mask'/'PC2', 'Uncommon').
card_artist('farsight mask'/'PC2', 'Ben Thompson').
card_number('farsight mask'/'PC2', '109').
card_flavor_text('farsight mask'/'PC2', 'It turns the adversity of the moment into the knowledge of a lifetime.').
card_multiverse_id('farsight mask'/'PC2', '275263').

card_in_set('felidar umbra', 'PC2').
card_original_type('felidar umbra'/'PC2', 'Enchantment — Aura').
card_original_text('felidar umbra'/'PC2', 'Enchant creature\nEnchanted creature has lifelink.\n{1}{W}: Attach Felidar Umbra to target creature you control.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('felidar umbra', 'PC2').
card_image_name('felidar umbra'/'PC2', 'felidar umbra').
card_uid('felidar umbra'/'PC2', 'PC2:Felidar Umbra:felidar umbra').
card_rarity('felidar umbra'/'PC2', 'Uncommon').
card_artist('felidar umbra'/'PC2', 'Christopher Moeller').
card_number('felidar umbra'/'PC2', '6').
card_multiverse_id('felidar umbra'/'PC2', '270736').

card_in_set('fiery conclusion', 'PC2').
card_original_type('fiery conclusion'/'PC2', 'Instant').
card_original_text('fiery conclusion'/'PC2', 'As an additional cost to cast Fiery Conclusion, sacrifice a creature.\nFiery Conclusion deals 5 damage to target creature.').
card_image_name('fiery conclusion'/'PC2', 'fiery conclusion').
card_uid('fiery conclusion'/'PC2', 'PC2:Fiery Conclusion:fiery conclusion').
card_rarity('fiery conclusion'/'PC2', 'Common').
card_artist('fiery conclusion'/'PC2', 'Paolo Parente').
card_number('fiery conclusion'/'PC2', '42').
card_flavor_text('fiery conclusion'/'PC2', 'The Boros legionnaire saw a noble sacrifice, the Rakdos thug a blazing suicide, and the Izzet alchemist an experiment gone awry.').
card_multiverse_id('fiery conclusion'/'PC2', '275259').

card_in_set('fiery fall', 'PC2').
card_original_type('fiery fall'/'PC2', 'Instant').
card_original_text('fiery fall'/'PC2', 'Fiery Fall deals 5 damage to target creature.\nBasic landcycling {1}{R} ({1}{R}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('fiery fall'/'PC2', 'fiery fall').
card_uid('fiery fall'/'PC2', 'PC2:Fiery Fall:fiery fall').
card_rarity('fiery fall'/'PC2', 'Common').
card_artist('fiery fall'/'PC2', 'Daarken').
card_number('fiery fall'/'PC2', '43').
card_flavor_text('fiery fall'/'PC2', 'Jund feasts on the unprepared.').
card_multiverse_id('fiery fall'/'PC2', '271138').

card_in_set('fires of yavimaya', 'PC2').
card_original_type('fires of yavimaya'/'PC2', 'Enchantment').
card_original_text('fires of yavimaya'/'PC2', 'Creatures you control have haste.\nSacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_image_name('fires of yavimaya'/'PC2', 'fires of yavimaya').
card_uid('fires of yavimaya'/'PC2', 'PC2:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'PC2', 'Uncommon').
card_artist('fires of yavimaya'/'PC2', 'Val Mayerik').
card_number('fires of yavimaya'/'PC2', '92').
card_multiverse_id('fires of yavimaya'/'PC2', '276068').

card_in_set('flayer husk', 'PC2').
card_original_type('flayer husk'/'PC2', 'Artifact — Equipment').
card_original_text('flayer husk'/'PC2', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +1/+1.\nEquip {2}').
card_image_name('flayer husk'/'PC2', 'flayer husk').
card_uid('flayer husk'/'PC2', 'PC2:Flayer Husk:flayer husk').
card_rarity('flayer husk'/'PC2', 'Common').
card_artist('flayer husk'/'PC2', 'Igor Kieryluk').
card_number('flayer husk'/'PC2', '110').
card_multiverse_id('flayer husk'/'PC2', '271145').

card_in_set('fling', 'PC2').
card_original_type('fling'/'PC2', 'Instant').
card_original_text('fling'/'PC2', 'As an additional cost to cast Fling, sacrifice a creature.\nFling deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_image_name('fling'/'PC2', 'fling').
card_uid('fling'/'PC2', 'PC2:Fling:fling').
card_rarity('fling'/'PC2', 'Common').
card_artist('fling'/'PC2', 'Paolo Parente').
card_number('fling'/'PC2', '44').
card_flavor_text('fling'/'PC2', 'About halfway to the target, Snukk vowed that he had been tricked for the last time.').
card_multiverse_id('fling'/'PC2', '271225').

card_in_set('forest', 'PC2').
card_original_type('forest'/'PC2', 'Basic Land — Forest').
card_original_text('forest'/'PC2', 'G').
card_image_name('forest'/'PC2', 'forest1').
card_uid('forest'/'PC2', 'PC2:Forest:forest1').
card_rarity('forest'/'PC2', 'Basic Land').
card_artist('forest'/'PC2', 'John Avon').
card_number('forest'/'PC2', '151').
card_multiverse_id('forest'/'PC2', '276467').

card_in_set('forest', 'PC2').
card_original_type('forest'/'PC2', 'Basic Land — Forest').
card_original_text('forest'/'PC2', 'G').
card_image_name('forest'/'PC2', 'forest2').
card_uid('forest'/'PC2', 'PC2:Forest:forest2').
card_rarity('forest'/'PC2', 'Basic Land').
card_artist('forest'/'PC2', 'John Avon').
card_number('forest'/'PC2', '152').
card_multiverse_id('forest'/'PC2', '276468').

card_in_set('forest', 'PC2').
card_original_type('forest'/'PC2', 'Basic Land — Forest').
card_original_text('forest'/'PC2', 'G').
card_image_name('forest'/'PC2', 'forest3').
card_uid('forest'/'PC2', 'PC2:Forest:forest3').
card_rarity('forest'/'PC2', 'Basic Land').
card_artist('forest'/'PC2', 'Aleksi Briclot').
card_number('forest'/'PC2', '153').
card_multiverse_id('forest'/'PC2', '276460').

card_in_set('forest', 'PC2').
card_original_type('forest'/'PC2', 'Basic Land — Forest').
card_original_text('forest'/'PC2', 'G').
card_image_name('forest'/'PC2', 'forest4').
card_uid('forest'/'PC2', 'PC2:Forest:forest4').
card_rarity('forest'/'PC2', 'Basic Land').
card_artist('forest'/'PC2', 'Véronique Meignaud').
card_number('forest'/'PC2', '154').
card_multiverse_id('forest'/'PC2', '276464').

card_in_set('forest', 'PC2').
card_original_type('forest'/'PC2', 'Basic Land — Forest').
card_original_text('forest'/'PC2', 'G').
card_image_name('forest'/'PC2', 'forest5').
card_uid('forest'/'PC2', 'PC2:Forest:forest5').
card_rarity('forest'/'PC2', 'Basic Land').
card_artist('forest'/'PC2', 'Vincent Proce').
card_number('forest'/'PC2', '155').
card_multiverse_id('forest'/'PC2', '276456').

card_in_set('forest', 'PC2').
card_original_type('forest'/'PC2', 'Basic Land — Forest').
card_original_text('forest'/'PC2', 'G').
card_image_name('forest'/'PC2', 'forest6').
card_uid('forest'/'PC2', 'PC2:Forest:forest6').
card_rarity('forest'/'PC2', 'Basic Land').
card_artist('forest'/'PC2', 'Stephen Tappin').
card_number('forest'/'PC2', '156').
card_multiverse_id('forest'/'PC2', '276454').

card_in_set('fractured powerstone', 'PC2').
card_original_type('fractured powerstone'/'PC2', 'Artifact').
card_original_text('fractured powerstone'/'PC2', '{T}: Add {1} to your mana pool.\n{T}: Roll the planar die. Activate this ability only any time you could cast a sorcery.').
card_first_print('fractured powerstone', 'PC2').
card_image_name('fractured powerstone'/'PC2', 'fractured powerstone').
card_uid('fractured powerstone'/'PC2', 'PC2:Fractured Powerstone:fractured powerstone').
card_rarity('fractured powerstone'/'PC2', 'Common').
card_artist('fractured powerstone'/'PC2', 'Rob Alexander').
card_number('fractured powerstone'/'PC2', '111').
card_flavor_text('fractured powerstone'/'PC2', 'The Thran learned to capture mana, but power inevitably escapes its bonds.').
card_multiverse_id('fractured powerstone'/'PC2', '265151').

card_in_set('furnace layer', 'PC2').
card_original_type('furnace layer'/'PC2', 'Plane — New Phyrexia').
card_original_text('furnace layer'/'PC2', 'When you planeswalk to Furnace Layer or at the beginning of your upkeep, select target player at random. That player discards a card. If that player discards a land card this way, he or she loses 3 life.\nWhenever you roll {C}, you may destroy target nonland permanent.').
card_first_print('furnace layer', 'PC2').
card_image_name('furnace layer'/'PC2', 'furnace layer').
card_uid('furnace layer'/'PC2', 'PC2:Furnace Layer:furnace layer').
card_rarity('furnace layer'/'PC2', 'Common').
card_artist('furnace layer'/'PC2', 'Kekai Kotaki').
card_number('furnace layer'/'PC2', '14').
card_multiverse_id('furnace layer'/'PC2', '226536').

card_in_set('fusion elemental', 'PC2').
card_original_type('fusion elemental'/'PC2', 'Creature — Elemental').
card_original_text('fusion elemental'/'PC2', '').
card_image_name('fusion elemental'/'PC2', 'fusion elemental').
card_uid('fusion elemental'/'PC2', 'PC2:Fusion Elemental:fusion elemental').
card_rarity('fusion elemental'/'PC2', 'Uncommon').
card_artist('fusion elemental'/'PC2', 'Michael Komarck').
card_number('fusion elemental'/'PC2', '93').
card_flavor_text('fusion elemental'/'PC2', 'As the shards merged into the Maelstrom, their mana energies fused into new monstrosities.').
card_multiverse_id('fusion elemental'/'PC2', '271139').

card_in_set('gavony', 'PC2').
card_original_type('gavony'/'PC2', 'Plane — Innistrad').
card_original_text('gavony'/'PC2', 'All creatures have vigilance.\nWhenever you roll {C}, creatures you control are indestructible this turn.').
card_first_print('gavony', 'PC2').
card_image_name('gavony'/'PC2', 'gavony').
card_uid('gavony'/'PC2', 'PC2:Gavony:gavony').
card_rarity('gavony'/'PC2', 'Common').
card_artist('gavony'/'PC2', 'Dave Kendall').
card_number('gavony'/'PC2', '15').
card_multiverse_id('gavony'/'PC2', '226542').

card_in_set('ghostly prison', 'PC2').
card_original_type('ghostly prison'/'PC2', 'Enchantment').
card_original_text('ghostly prison'/'PC2', 'Creatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_image_name('ghostly prison'/'PC2', 'ghostly prison').
card_uid('ghostly prison'/'PC2', 'PC2:Ghostly Prison:ghostly prison').
card_rarity('ghostly prison'/'PC2', 'Uncommon').
card_artist('ghostly prison'/'PC2', 'Wayne England').
card_number('ghostly prison'/'PC2', '7').
card_flavor_text('ghostly prison'/'PC2', '\"May the memory of our fallen heroes ensnare the violent hearts of lesser men.\"\n—Great Threshold of Monserkat inscription').
card_multiverse_id('ghostly prison'/'PC2', '275264').

card_in_set('glen elendra', 'PC2').
card_original_type('glen elendra'/'PC2', 'Plane — Lorwyn').
card_original_text('glen elendra'/'PC2', 'At end of combat, you may exchange control of target creature you control that dealt combat damage to a player this combat and target creature that player controls.\nWhenever you roll {C}, gain control of target creature you own.').
card_first_print('glen elendra', 'PC2').
card_image_name('glen elendra'/'PC2', 'glen elendra').
card_uid('glen elendra'/'PC2', 'PC2:Glen Elendra:glen elendra').
card_rarity('glen elendra'/'PC2', 'Common').
card_artist('glen elendra'/'PC2', 'Omar Rayyan').
card_number('glen elendra'/'PC2', '16').
card_multiverse_id('glen elendra'/'PC2', '226535').

card_in_set('glen elendra liege', 'PC2').
card_original_type('glen elendra liege'/'PC2', 'Creature — Faerie Knight').
card_original_text('glen elendra liege'/'PC2', 'Flying\nOther blue creatures you control get +1/+1.\nOther black creatures you control get +1/+1.').
card_image_name('glen elendra liege'/'PC2', 'glen elendra liege').
card_uid('glen elendra liege'/'PC2', 'PC2:Glen Elendra Liege:glen elendra liege').
card_rarity('glen elendra liege'/'PC2', 'Rare').
card_artist('glen elendra liege'/'PC2', 'Kev Walker').
card_number('glen elendra liege'/'PC2', '94').
card_flavor_text('glen elendra liege'/'PC2', 'Those who displease Oona soon learn the extent of the armies she commands.').
card_multiverse_id('glen elendra liege'/'PC2', '271215').

card_in_set('gluttonous slime', 'PC2').
card_original_type('gluttonous slime'/'PC2', 'Creature — Ooze').
card_original_text('gluttonous slime'/'PC2', 'Flash\nDevour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)').
card_image_name('gluttonous slime'/'PC2', 'gluttonous slime').
card_uid('gluttonous slime'/'PC2', 'PC2:Gluttonous Slime:gluttonous slime').
card_rarity('gluttonous slime'/'PC2', 'Uncommon').
card_artist('gluttonous slime'/'PC2', 'Trevor Claxton').
card_number('gluttonous slime'/'PC2', '65').
card_flavor_text('gluttonous slime'/'PC2', 'On Jund, everything eventually ends up in something else\'s stomach.').
card_multiverse_id('gluttonous slime'/'PC2', '271140').

card_in_set('grand ossuary', 'PC2').
card_original_type('grand ossuary'/'PC2', 'Plane — Ravnica').
card_original_text('grand ossuary'/'PC2', 'Whenever a creature dies, its controller distributes a number of +1/+1 counters equal to its power among any number of target creatures he or she controls.\nWhenever you roll {C}, each player exiles all creatures he or she controls and puts X 1/1 green Saproling creature tokens onto the battlefield, where X is the total power of the creatures he or she exiled this way. Then planeswalk.').
card_first_print('grand ossuary', 'PC2').
card_image_name('grand ossuary'/'PC2', 'grand ossuary').
card_uid('grand ossuary'/'PC2', 'PC2:Grand Ossuary:grand ossuary').
card_rarity('grand ossuary'/'PC2', 'Common').
card_artist('grand ossuary'/'PC2', 'Kekai Kotaki').
card_number('grand ossuary'/'PC2', '17').
card_multiverse_id('grand ossuary'/'PC2', '226554').

card_in_set('graypelt refuge', 'PC2').
card_original_type('graypelt refuge'/'PC2', 'Land').
card_original_text('graypelt refuge'/'PC2', 'Graypelt Refuge enters the battlefield tapped.\nWhen Graypelt Refuge enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('graypelt refuge'/'PC2', 'graypelt refuge').
card_uid('graypelt refuge'/'PC2', 'PC2:Graypelt Refuge:graypelt refuge').
card_rarity('graypelt refuge'/'PC2', 'Uncommon').
card_artist('graypelt refuge'/'PC2', 'Philip Straub').
card_number('graypelt refuge'/'PC2', '118').
card_multiverse_id('graypelt refuge'/'PC2', '271131').

card_in_set('grove of the dreampods', 'PC2').
card_original_type('grove of the dreampods'/'PC2', 'Plane — Fabacin').
card_original_text('grove of the dreampods'/'PC2', 'When you planeswalk to Grove of the Dreampods or at the beginning of your upkeep, reveal cards from the top of your library until you reveal a creature card. Put that card onto the battlefield and the rest on the bottom of your library in a random order.\nWhenever you roll {C}, return target creature card from your graveyard to the battlefield.').
card_first_print('grove of the dreampods', 'PC2').
card_image_name('grove of the dreampods'/'PC2', 'grove of the dreampods').
card_uid('grove of the dreampods'/'PC2', 'PC2:Grove of the Dreampods:grove of the dreampods').
card_rarity('grove of the dreampods'/'PC2', 'Common').
card_artist('grove of the dreampods'/'PC2', 'Erica Yang').
card_number('grove of the dreampods'/'PC2', '18').
card_multiverse_id('grove of the dreampods'/'PC2', '226541').

card_in_set('gruul turf', 'PC2').
card_original_type('gruul turf'/'PC2', 'Land').
card_original_text('gruul turf'/'PC2', 'Gruul Turf enters the battlefield tapped.\nWhen Gruul Turf enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{G} to your mana pool.').
card_image_name('gruul turf'/'PC2', 'gruul turf').
card_uid('gruul turf'/'PC2', 'PC2:Gruul Turf:gruul turf').
card_rarity('gruul turf'/'PC2', 'Common').
card_artist('gruul turf'/'PC2', 'John Avon').
card_number('gruul turf'/'PC2', '119').
card_multiverse_id('gruul turf'/'PC2', '271228').

card_in_set('guard gomazoa', 'PC2').
card_original_type('guard gomazoa'/'PC2', 'Creature — Jellyfish').
card_original_text('guard gomazoa'/'PC2', 'Defender, flying\nPrevent all combat damage that would be dealt to Guard Gomazoa.').
card_image_name('guard gomazoa'/'PC2', 'guard gomazoa').
card_uid('guard gomazoa'/'PC2', 'PC2:Guard Gomazoa:guard gomazoa').
card_rarity('guard gomazoa'/'PC2', 'Uncommon').
card_artist('guard gomazoa'/'PC2', 'Rob Alexander').
card_number('guard gomazoa'/'PC2', '17').
card_flavor_text('guard gomazoa'/'PC2', 'It lingers near the outposts of the Makindi Trenches, inadvertently granting another layer of defense.').
card_multiverse_id('guard gomazoa'/'PC2', '279846').

card_in_set('hedron fields of agadeem', 'PC2').
card_original_type('hedron fields of agadeem'/'PC2', 'Plane — Zendikar').
card_original_text('hedron fields of agadeem'/'PC2', 'Creatures with power 7 or greater can\'t attack or block.\nWhenever you roll {C}, put a 7/7 colorless Eldrazi creature token with annihilator 1 onto the battlefield. (Whenever it attacks, defending player sacrifices a permanent.)').
card_first_print('hedron fields of agadeem', 'PC2').
card_image_name('hedron fields of agadeem'/'PC2', 'hedron fields of agadeem').
card_uid('hedron fields of agadeem'/'PC2', 'PC2:Hedron Fields of Agadeem:hedron fields of agadeem').
card_rarity('hedron fields of agadeem'/'PC2', 'Common').
card_artist('hedron fields of agadeem'/'PC2', 'Vincent Proce').
card_number('hedron fields of agadeem'/'PC2', '19').
card_multiverse_id('hedron fields of agadeem'/'PC2', '226544').

card_in_set('hellion eruption', 'PC2').
card_original_type('hellion eruption'/'PC2', 'Sorcery').
card_original_text('hellion eruption'/'PC2', 'Sacrifice all creatures you control, then put that many 4/4 red Hellion creature tokens onto the battlefield.').
card_image_name('hellion eruption'/'PC2', 'hellion eruption').
card_uid('hellion eruption'/'PC2', 'PC2:Hellion Eruption:hellion eruption').
card_rarity('hellion eruption'/'PC2', 'Rare').
card_artist('hellion eruption'/'PC2', 'Anthony Francisco').
card_number('hellion eruption'/'PC2', '45').
card_flavor_text('hellion eruption'/'PC2', 'Just when you thought you\'d be safe, in the middle of an open field with no Eldrazi around for miles.').
card_multiverse_id('hellion eruption'/'PC2', '271158').

card_in_set('hellkite hatchling', 'PC2').
card_original_type('hellkite hatchling'/'PC2', 'Creature — Dragon').
card_original_text('hellkite hatchling'/'PC2', 'Devour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)\nHellkite Hatchling has flying and trample if it devoured a creature.').
card_image_name('hellkite hatchling'/'PC2', 'hellkite hatchling').
card_uid('hellkite hatchling'/'PC2', 'PC2:Hellkite Hatchling:hellkite hatchling').
card_rarity('hellkite hatchling'/'PC2', 'Uncommon').
card_artist('hellkite hatchling'/'PC2', 'Daarken').
card_number('hellkite hatchling'/'PC2', '95').
card_flavor_text('hellkite hatchling'/'PC2', 'A killing machine from birth.').
card_multiverse_id('hellkite hatchling'/'PC2', '271141').

card_in_set('higure, the still wind', 'PC2').
card_original_type('higure, the still wind'/'PC2', 'Legendary Creature — Human Ninja').
card_original_text('higure, the still wind'/'PC2', 'Ninjutsu {2}{U}{U} ({2}{U}{U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Higure, the Still Wind deals combat damage to a player, you may search your library for a Ninja card, reveal it, and put it into your hand. If you do, shuffle your library.\n{2}: Target Ninja creature is unblockable this turn.').
card_image_name('higure, the still wind'/'PC2', 'higure, the still wind').
card_uid('higure, the still wind'/'PC2', 'PC2:Higure, the Still Wind:higure, the still wind').
card_rarity('higure, the still wind'/'PC2', 'Rare').
card_artist('higure, the still wind'/'PC2', 'Christopher Moeller').
card_number('higure, the still wind'/'PC2', '18').
card_multiverse_id('higure, the still wind'/'PC2', '271181').

card_in_set('hissing iguanar', 'PC2').
card_original_type('hissing iguanar'/'PC2', 'Creature — Lizard').
card_original_text('hissing iguanar'/'PC2', 'Whenever another creature dies, you may have Hissing Iguanar deal 1 damage to target player.').
card_image_name('hissing iguanar'/'PC2', 'hissing iguanar').
card_uid('hissing iguanar'/'PC2', 'PC2:Hissing Iguanar:hissing iguanar').
card_rarity('hissing iguanar'/'PC2', 'Common').
card_artist('hissing iguanar'/'PC2', 'Brandon Kitkouski').
card_number('hissing iguanar'/'PC2', '46').
card_flavor_text('hissing iguanar'/'PC2', 'Viashino thrashes keep iguanars as hunting companions, giving them wounded captives as playthings.').
card_multiverse_id('hissing iguanar'/'PC2', '271160').

card_in_set('hyena umbra', 'PC2').
card_original_type('hyena umbra'/'PC2', 'Enchantment — Aura').
card_original_text('hyena umbra'/'PC2', 'Enchant creature\nEnchanted creature gets +1/+1 and has first strike.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_image_name('hyena umbra'/'PC2', 'hyena umbra').
card_uid('hyena umbra'/'PC2', 'PC2:Hyena Umbra:hyena umbra').
card_rarity('hyena umbra'/'PC2', 'Common').
card_artist('hyena umbra'/'PC2', 'Howard Lyon').
card_number('hyena umbra'/'PC2', '8').
card_multiverse_id('hyena umbra'/'PC2', '271150').

card_in_set('illusory angel', 'PC2').
card_original_type('illusory angel'/'PC2', 'Creature — Angel Illusion').
card_original_text('illusory angel'/'PC2', 'Flying\nCast Illusory Angel only if you\'ve cast another spell this turn.').
card_first_print('illusory angel', 'PC2').
card_image_name('illusory angel'/'PC2', 'illusory angel').
card_uid('illusory angel'/'PC2', 'PC2:Illusory Angel:illusory angel').
card_rarity('illusory angel'/'PC2', 'Uncommon').
card_artist('illusory angel'/'PC2', 'Allen Williams').
card_number('illusory angel'/'PC2', '19').
card_flavor_text('illusory angel'/'PC2', '\"Yes, she is made of pure belief—but I hardly see why that makes her unique.\"\n—Reith, master illusionist').
card_multiverse_id('illusory angel'/'PC2', '265143').

card_in_set('indrik umbra', 'PC2').
card_original_type('indrik umbra'/'PC2', 'Enchantment — Aura').
card_original_text('indrik umbra'/'PC2', 'Enchant creature\nEnchanted creature gets +4/+4 and has first strike, and all creatures able to block it do so.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_first_print('indrik umbra', 'PC2').
card_image_name('indrik umbra'/'PC2', 'indrik umbra').
card_uid('indrik umbra'/'PC2', 'PC2:Indrik Umbra:indrik umbra').
card_rarity('indrik umbra'/'PC2', 'Rare').
card_artist('indrik umbra'/'PC2', 'Christopher Moeller').
card_number('indrik umbra'/'PC2', '96').
card_multiverse_id('indrik umbra'/'PC2', '270724').

card_in_set('ink-eyes, servant of oni', 'PC2').
card_original_type('ink-eyes, servant of oni'/'PC2', 'Legendary Creature — Rat Ninja').
card_original_text('ink-eyes, servant of oni'/'PC2', 'Ninjutsu {3}{B}{B} ({3}{B}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Ink-Eyes, Servant of Oni deals combat damage to a player, you may put target creature card from that player\'s graveyard onto the battlefield under your control.\n{1}{B}: Regenerate Ink-Eyes.').
card_image_name('ink-eyes, servant of oni'/'PC2', 'ink-eyes, servant of oni').
card_uid('ink-eyes, servant of oni'/'PC2', 'PC2:Ink-Eyes, Servant of Oni:ink-eyes, servant of oni').
card_rarity('ink-eyes, servant of oni'/'PC2', 'Rare').
card_artist('ink-eyes, servant of oni'/'PC2', 'Wayne Reynolds').
card_number('ink-eyes, servant of oni'/'PC2', '33').
card_multiverse_id('ink-eyes, servant of oni'/'PC2', '271182').

card_in_set('inkfathom witch', 'PC2').
card_original_type('inkfathom witch'/'PC2', 'Creature — Merfolk Wizard').
card_original_text('inkfathom witch'/'PC2', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{2}{U}{B}: Each unblocked creature becomes 4/1 until end of turn.').
card_image_name('inkfathom witch'/'PC2', 'inkfathom witch').
card_uid('inkfathom witch'/'PC2', 'PC2:Inkfathom Witch:inkfathom witch').
card_rarity('inkfathom witch'/'PC2', 'Uncommon').
card_artist('inkfathom witch'/'PC2', 'Larry MacDougall').
card_number('inkfathom witch'/'PC2', '97').
card_flavor_text('inkfathom witch'/'PC2', 'The murk of the Wanderbrine concealed unseemly rituals designed to bring out the worst in merrowkind.').
card_multiverse_id('inkfathom witch'/'PC2', '271216').

card_in_set('interplanar tunnel', 'PC2').
card_original_type('interplanar tunnel'/'PC2', 'Phenomenon').
card_original_text('interplanar tunnel'/'PC2', 'When you encounter Interplanar Tunnel, reveal cards from the top of your planar deck until you reveal five plane cards. Put a plane card from among them on top of your planar deck, then put the rest of the revealed cards on the bottom in a random order. (Then planeswalk away from this phenomenon.)').
card_first_print('interplanar tunnel', 'PC2').
card_image_name('interplanar tunnel'/'PC2', 'interplanar tunnel').
card_uid('interplanar tunnel'/'PC2', 'PC2:Interplanar Tunnel:interplanar tunnel').
card_rarity('interplanar tunnel'/'PC2', 'Common').
card_artist('interplanar tunnel'/'PC2', 'Chuck Lukacs').
card_number('interplanar tunnel'/'PC2', '2').
card_multiverse_id('interplanar tunnel'/'PC2', '226549').

card_in_set('island', 'PC2').
card_original_type('island'/'PC2', 'Basic Land — Island').
card_original_text('island'/'PC2', 'U').
card_image_name('island'/'PC2', 'island1').
card_uid('island'/'PC2', 'PC2:Island:island1').
card_rarity('island'/'PC2', 'Basic Land').
card_artist('island'/'PC2', 'John Avon').
card_number('island'/'PC2', '137').
card_multiverse_id('island'/'PC2', '276466').

card_in_set('island', 'PC2').
card_original_type('island'/'PC2', 'Basic Land — Island').
card_original_text('island'/'PC2', 'U').
card_image_name('island'/'PC2', 'island2').
card_uid('island'/'PC2', 'PC2:Island:island2').
card_rarity('island'/'PC2', 'Basic Land').
card_artist('island'/'PC2', 'Martina Pilcerova').
card_number('island'/'PC2', '138').
card_multiverse_id('island'/'PC2', '276452').

card_in_set('island', 'PC2').
card_original_type('island'/'PC2', 'Basic Land — Island').
card_original_text('island'/'PC2', 'U').
card_image_name('island'/'PC2', 'island3').
card_uid('island'/'PC2', 'PC2:Island:island3').
card_rarity('island'/'PC2', 'Basic Land').
card_artist('island'/'PC2', 'Martina Pilcerova').
card_number('island'/'PC2', '139').
card_multiverse_id('island'/'PC2', '276453').

card_in_set('island', 'PC2').
card_original_type('island'/'PC2', 'Basic Land — Island').
card_original_text('island'/'PC2', 'U').
card_image_name('island'/'PC2', 'island4').
card_uid('island'/'PC2', 'PC2:Island:island4').
card_rarity('island'/'PC2', 'Basic Land').
card_artist('island'/'PC2', 'Martina Pilcerova').
card_number('island'/'PC2', '140').
card_multiverse_id('island'/'PC2', '276471').

card_in_set('island', 'PC2').
card_original_type('island'/'PC2', 'Basic Land — Island').
card_original_text('island'/'PC2', 'U').
card_image_name('island'/'PC2', 'island5').
card_uid('island'/'PC2', 'PC2:Island:island5').
card_rarity('island'/'PC2', 'Basic Land').
card_artist('island'/'PC2', 'Martina Pilcerova').
card_number('island'/'PC2', '141').
card_multiverse_id('island'/'PC2', '276462').

card_in_set('jund', 'PC2').
card_original_type('jund'/'PC2', 'Plane — Alara').
card_original_text('jund'/'PC2', 'Whenever a player casts a black, red, or green creature spell, it gains devour 5. (As the creature enters the battlefield, its controller may sacrifice any number of creatures. The creature enters the battlefield with five times that many +1/+1 counters on it.)\nWhenever you roll {C}, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_first_print('jund', 'PC2').
card_image_name('jund'/'PC2', 'jund').
card_uid('jund'/'PC2', 'PC2:Jund:jund').
card_rarity('jund'/'PC2', 'Common').
card_artist('jund'/'PC2', 'Aleksi Briclot').
card_number('jund'/'PC2', '20').
card_multiverse_id('jund'/'PC2', '226551').

card_in_set('jwar isle refuge', 'PC2').
card_original_type('jwar isle refuge'/'PC2', 'Land').
card_original_text('jwar isle refuge'/'PC2', 'Jwar Isle Refuge enters the battlefield tapped.\nWhen Jwar Isle Refuge enters the battlefield, you gain 1 life.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('jwar isle refuge'/'PC2', 'jwar isle refuge').
card_uid('jwar isle refuge'/'PC2', 'PC2:Jwar Isle Refuge:jwar isle refuge').
card_rarity('jwar isle refuge'/'PC2', 'Uncommon').
card_artist('jwar isle refuge'/'PC2', 'Cyril Van Der Haegen').
card_number('jwar isle refuge'/'PC2', '120').
card_multiverse_id('jwar isle refuge'/'PC2', '271132').

card_in_set('kathari remnant', 'PC2').
card_original_type('kathari remnant'/'PC2', 'Creature — Bird Skeleton').
card_original_text('kathari remnant'/'PC2', 'Flying\n{B}: Regenerate Kathari Remnant.\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_image_name('kathari remnant'/'PC2', 'kathari remnant').
card_uid('kathari remnant'/'PC2', 'PC2:Kathari Remnant:kathari remnant').
card_rarity('kathari remnant'/'PC2', 'Uncommon').
card_artist('kathari remnant'/'PC2', 'Anthony S. Waters').
card_number('kathari remnant'/'PC2', '98').
card_multiverse_id('kathari remnant'/'PC2', '271189').

card_in_set('kazandu refuge', 'PC2').
card_original_type('kazandu refuge'/'PC2', 'Land').
card_original_text('kazandu refuge'/'PC2', 'Kazandu Refuge enters the battlefield tapped.\nWhen Kazandu Refuge enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('kazandu refuge'/'PC2', 'kazandu refuge').
card_uid('kazandu refuge'/'PC2', 'PC2:Kazandu Refuge:kazandu refuge').
card_rarity('kazandu refuge'/'PC2', 'Uncommon').
card_artist('kazandu refuge'/'PC2', 'Franz Vohwinkel').
card_number('kazandu refuge'/'PC2', '121').
card_multiverse_id('kazandu refuge'/'PC2', '271133').

card_in_set('kessig', 'PC2').
card_original_type('kessig'/'PC2', 'Plane — Innistrad').
card_original_text('kessig'/'PC2', 'Prevent all combat damage that would be dealt by non-Werewolf creatures.\nWhenever you roll {C}, each creature you control gets +2/+2, gains trample, and becomes a Werewolf in addition to its other types until end of turn.').
card_first_print('kessig', 'PC2').
card_image_name('kessig'/'PC2', 'kessig').
card_uid('kessig'/'PC2', 'PC2:Kessig:kessig').
card_rarity('kessig'/'PC2', 'Common').
card_artist('kessig'/'PC2', 'Adam Paquette').
card_number('kessig'/'PC2', '21').
card_multiverse_id('kessig'/'PC2', '226537').

card_in_set('khalni garden', 'PC2').
card_original_type('khalni garden'/'PC2', 'Land').
card_original_text('khalni garden'/'PC2', 'Khalni Garden enters the battlefield tapped.\nWhen Khalni Garden enters the battlefield, put a 0/1 green Plant creature token onto the battlefield.\n{T}: Add {G} to your mana pool.').
card_image_name('khalni garden'/'PC2', 'khalni garden').
card_uid('khalni garden'/'PC2', 'PC2:Khalni Garden:khalni garden').
card_rarity('khalni garden'/'PC2', 'Common').
card_artist('khalni garden'/'PC2', 'Ryan Pancoast').
card_number('khalni garden'/'PC2', '122').
card_multiverse_id('khalni garden'/'PC2', '271227').

card_in_set('kharasha foothills', 'PC2').
card_original_type('kharasha foothills'/'PC2', 'Plane — Mongseng').
card_original_text('kharasha foothills'/'PC2', 'Whenever a creature you control attacks a player, for each other opponent, you may put a token that\'s a copy of that creature onto the battlefield tapped and attacking that opponent. Exile those tokens at the beginning of the next end step.\nWhenever you roll {C}, you may sacrifice any number of creatures. If you do, Kharasha Foothills deals that much damage to target creature.').
card_first_print('kharasha foothills', 'PC2').
card_image_name('kharasha foothills'/'PC2', 'kharasha foothills').
card_uid('kharasha foothills'/'PC2', 'PC2:Kharasha Foothills:kharasha foothills').
card_rarity('kharasha foothills'/'PC2', 'Common').
card_artist('kharasha foothills'/'PC2', 'Trevor Claxton').
card_number('kharasha foothills'/'PC2', '22').
card_multiverse_id('kharasha foothills'/'PC2', '226534').

card_in_set('kilnspire district', 'PC2').
card_original_type('kilnspire district'/'PC2', 'Plane — Ravnica').
card_original_text('kilnspire district'/'PC2', 'When you planeswalk to Kilnspire District or at the beginning of your precombat main phase, put a charge counter on Kilnspire District, then add {R} to your mana pool for each charge counter on it.\nWhenever you roll {C}, you may pay {X}. If you do, Kilnspire District deals X damage to target creature or player.').
card_first_print('kilnspire district', 'PC2').
card_image_name('kilnspire district'/'PC2', 'kilnspire district').
card_uid('kilnspire district'/'PC2', 'PC2:Kilnspire District:kilnspire district').
card_rarity('kilnspire district'/'PC2', 'Common').
card_artist('kilnspire district'/'PC2', 'John Avon').
card_number('kilnspire district'/'PC2', '23').
card_multiverse_id('kilnspire district'/'PC2', '226523').

card_in_set('kor spiritdancer', 'PC2').
card_original_type('kor spiritdancer'/'PC2', 'Creature — Kor Wizard').
card_original_text('kor spiritdancer'/'PC2', 'Kor Spiritdancer gets +2/+2 for each Aura attached to it.\nWhenever you cast an Aura spell, you may draw a card.').
card_image_name('kor spiritdancer'/'PC2', 'kor spiritdancer').
card_uid('kor spiritdancer'/'PC2', 'PC2:Kor Spiritdancer:kor spiritdancer').
card_rarity('kor spiritdancer'/'PC2', 'Rare').
card_artist('kor spiritdancer'/'PC2', 'Scott Chou').
card_number('kor spiritdancer'/'PC2', '9').
card_flavor_text('kor spiritdancer'/'PC2', 'She reaches beyond the physical realm, touching the ideals from which all creatures draw their power.').
card_multiverse_id('kor spiritdancer'/'PC2', '271151').

card_in_set('krond the dawn-clad', 'PC2').
card_original_type('krond the dawn-clad'/'PC2', 'Legendary Creature — Archon').
card_original_text('krond the dawn-clad'/'PC2', 'Flying, vigilance\nWhenever Krond the Dawn-Clad attacks, if it\'s enchanted, exile target permanent.').
card_first_print('krond the dawn-clad', 'PC2').
card_image_name('krond the dawn-clad'/'PC2', 'krond the dawn-clad').
card_uid('krond the dawn-clad'/'PC2', 'PC2:Krond the Dawn-Clad:krond the dawn-clad').
card_rarity('krond the dawn-clad'/'PC2', 'Mythic Rare').
card_artist('krond the dawn-clad'/'PC2', 'Zoltan Boros').
card_number('krond the dawn-clad'/'PC2', '99').
card_flavor_text('krond the dawn-clad'/'PC2', 'Krond, the personification of the dawn\'s light, lives to exact justice on his nemesis Vela.').
card_multiverse_id('krond the dawn-clad'/'PC2', '270738').

card_in_set('krosan verge', 'PC2').
card_original_type('krosan verge'/'PC2', 'Land').
card_original_text('krosan verge'/'PC2', 'Krosan Verge enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Krosan Verge: Search your library for a Forest card and a Plains card and put them onto the battlefield tapped. Then shuffle your library.').
card_image_name('krosan verge'/'PC2', 'krosan verge').
card_uid('krosan verge'/'PC2', 'PC2:Krosan Verge:krosan verge').
card_rarity('krosan verge'/'PC2', 'Uncommon').
card_artist('krosan verge'/'PC2', 'Tony Szczudlo').
card_number('krosan verge'/'PC2', '123').
card_multiverse_id('krosan verge'/'PC2', '271177').

card_in_set('lair of the ashen idol', 'PC2').
card_original_type('lair of the ashen idol'/'PC2', 'Plane — Azgol').
card_original_text('lair of the ashen idol'/'PC2', 'At the beginning of your upkeep, sacrifice a creature. If you can\'t, planeswalk.\nWhenever you roll {C}, any number of target players each put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('lair of the ashen idol', 'PC2').
card_image_name('lair of the ashen idol'/'PC2', 'lair of the ashen idol').
card_uid('lair of the ashen idol'/'PC2', 'PC2:Lair of the Ashen Idol:lair of the ashen idol').
card_rarity('lair of the ashen idol'/'PC2', 'Common').
card_artist('lair of the ashen idol'/'PC2', 'Philip Straub').
card_number('lair of the ashen idol'/'PC2', '24').
card_multiverse_id('lair of the ashen idol'/'PC2', '226532').

card_in_set('last stand', 'PC2').
card_original_type('last stand'/'PC2', 'Sorcery').
card_original_text('last stand'/'PC2', 'Target opponent loses 2 life for each Swamp you control. Last Stand deals damage equal to the number of Mountains you control to target creature. Put a 1/1 green Saproling creature token onto the battlefield for each Forest you control. You gain 2 life for each Plains you control. Draw a card for each Island you control, then discard that many cards.').
card_image_name('last stand'/'PC2', 'last stand').
card_uid('last stand'/'PC2', 'PC2:Last Stand:last stand').
card_rarity('last stand'/'PC2', 'Rare').
card_artist('last stand'/'PC2', 'Ron Spencer').
card_number('last stand'/'PC2', '100').
card_multiverse_id('last stand'/'PC2', '271164').

card_in_set('liliana\'s specter', 'PC2').
card_original_type('liliana\'s specter'/'PC2', 'Creature — Specter').
card_original_text('liliana\'s specter'/'PC2', 'Flying\nWhen Liliana\'s Specter enters the battlefield, each opponent discards a card.').
card_image_name('liliana\'s specter'/'PC2', 'liliana\'s specter').
card_uid('liliana\'s specter'/'PC2', 'PC2:Liliana\'s Specter:liliana\'s specter').
card_rarity('liliana\'s specter'/'PC2', 'Common').
card_artist('liliana\'s specter'/'PC2', 'Vance Kovacs').
card_number('liliana\'s specter'/'PC2', '34').
card_flavor_text('liliana\'s specter'/'PC2', '\"The finest minions know what I need without me ever saying a thing.\"\n—Liliana Vess').
card_multiverse_id('liliana\'s specter'/'PC2', '271194').

card_in_set('lumberknot', 'PC2').
card_original_type('lumberknot'/'PC2', 'Creature — Treefolk').
card_original_text('lumberknot'/'PC2', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever a creature dies, put a +1/+1 counter on Lumberknot.').
card_image_name('lumberknot'/'PC2', 'lumberknot').
card_uid('lumberknot'/'PC2', 'PC2:Lumberknot:lumberknot').
card_rarity('lumberknot'/'PC2', 'Uncommon').
card_artist('lumberknot'/'PC2', 'Jason A. Engle').
card_number('lumberknot'/'PC2', '66').
card_flavor_text('lumberknot'/'PC2', 'Animated by geists fused in its oak, it hungers for more life to add to its core.').
card_multiverse_id('lumberknot'/'PC2', '271235').

card_in_set('maelstrom wanderer', 'PC2').
card_original_type('maelstrom wanderer'/'PC2', 'Legendary Creature — Elemental').
card_original_text('maelstrom wanderer'/'PC2', 'Creatures you control have haste.\nCascade, cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order. Then do it again.)').
card_first_print('maelstrom wanderer', 'PC2').
card_image_name('maelstrom wanderer'/'PC2', 'maelstrom wanderer').
card_uid('maelstrom wanderer'/'PC2', 'PC2:Maelstrom Wanderer:maelstrom wanderer').
card_rarity('maelstrom wanderer'/'PC2', 'Mythic Rare').
card_artist('maelstrom wanderer'/'PC2', 'Thomas M. Baxa').
card_number('maelstrom wanderer'/'PC2', '101').
card_multiverse_id('maelstrom wanderer'/'PC2', '265154').

card_in_set('mammoth umbra', 'PC2').
card_original_type('mammoth umbra'/'PC2', 'Enchantment — Aura').
card_original_text('mammoth umbra'/'PC2', 'Enchant creature\nEnchanted creature gets +3/+3 and has vigilance.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_image_name('mammoth umbra'/'PC2', 'mammoth umbra').
card_uid('mammoth umbra'/'PC2', 'PC2:Mammoth Umbra:mammoth umbra').
card_rarity('mammoth umbra'/'PC2', 'Uncommon').
card_artist('mammoth umbra'/'PC2', 'Howard Lyon').
card_number('mammoth umbra'/'PC2', '10').
card_multiverse_id('mammoth umbra'/'PC2', '271152').

card_in_set('mark of mutiny', 'PC2').
card_original_type('mark of mutiny'/'PC2', 'Sorcery').
card_original_text('mark of mutiny'/'PC2', 'Gain control of target creature until end of turn. Put a +1/+1 counter on it and untap it. That creature gains haste until end of turn.').
card_image_name('mark of mutiny'/'PC2', 'mark of mutiny').
card_uid('mark of mutiny'/'PC2', 'PC2:Mark of Mutiny:mark of mutiny').
card_rarity('mark of mutiny'/'PC2', 'Uncommon').
card_artist('mark of mutiny'/'PC2', 'Mike Bierek').
card_number('mark of mutiny'/'PC2', '47').
card_flavor_text('mark of mutiny'/'PC2', 'The flame of anger is hard to douse once lit.').
card_multiverse_id('mark of mutiny'/'PC2', '271135').

card_in_set('mass mutiny', 'PC2').
card_original_type('mass mutiny'/'PC2', 'Sorcery').
card_original_text('mass mutiny'/'PC2', 'For each opponent, gain control of target creature that player controls until end of turn. Untap those creatures. They gain haste until end of turn.').
card_first_print('mass mutiny', 'PC2').
card_image_name('mass mutiny'/'PC2', 'mass mutiny').
card_uid('mass mutiny'/'PC2', 'PC2:Mass Mutiny:mass mutiny').
card_rarity('mass mutiny'/'PC2', 'Rare').
card_artist('mass mutiny'/'PC2', 'Carl Critchlow').
card_number('mass mutiny'/'PC2', '48').
card_flavor_text('mass mutiny'/'PC2', '\"What say you, my most trusted advisors? . . . Advisors?\"\n—Edra, merfolk sovereign').
card_multiverse_id('mass mutiny'/'PC2', '270743').

card_in_set('mistblade shinobi', 'PC2').
card_original_type('mistblade shinobi'/'PC2', 'Creature — Human Ninja').
card_original_text('mistblade shinobi'/'PC2', 'Ninjutsu {U} ({U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Mistblade Shinobi deals combat damage to a player, you may return target creature that player controls to its owner\'s hand.').
card_image_name('mistblade shinobi'/'PC2', 'mistblade shinobi').
card_uid('mistblade shinobi'/'PC2', 'PC2:Mistblade Shinobi:mistblade shinobi').
card_rarity('mistblade shinobi'/'PC2', 'Common').
card_artist('mistblade shinobi'/'PC2', 'Kev Walker').
card_number('mistblade shinobi'/'PC2', '20').
card_multiverse_id('mistblade shinobi'/'PC2', '271183').

card_in_set('mitotic slime', 'PC2').
card_original_type('mitotic slime'/'PC2', 'Creature — Ooze').
card_original_text('mitotic slime'/'PC2', 'When Mitotic Slime dies, put two 2/2 green Ooze creature tokens onto the battlefield. They have \"When this creature dies, put two 1/1 green Ooze creature tokens onto the battlefield.\"').
card_image_name('mitotic slime'/'PC2', 'mitotic slime').
card_uid('mitotic slime'/'PC2', 'PC2:Mitotic Slime:mitotic slime').
card_rarity('mitotic slime'/'PC2', 'Rare').
card_artist('mitotic slime'/'PC2', 'Raymond Swanland').
card_number('mitotic slime'/'PC2', '67').
card_multiverse_id('mitotic slime'/'PC2', '271195').

card_in_set('morphic tide', 'PC2').
card_original_type('morphic tide'/'PC2', 'Phenomenon').
card_original_text('morphic tide'/'PC2', 'When you encounter Morphic Tide, each player shuffles all permanents he or she owns into his or her library, then reveals that many cards from the top of his or her library. Each player puts all artifact, creature, land, and planeswalker cards revealed this way onto the battlefield, then does the same for enchantment cards, then puts all cards revealed this way that weren\'t put onto the battlefield on the bottom of his or her library in any order. (Then planeswalk away from this phenomenon.)').
card_first_print('morphic tide', 'PC2').
card_image_name('morphic tide'/'PC2', 'morphic tide').
card_uid('morphic tide'/'PC2', 'PC2:Morphic Tide:morphic tide').
card_rarity('morphic tide'/'PC2', 'Common').
card_artist('morphic tide'/'PC2', 'Brandon Kitkouski').
card_number('morphic tide'/'PC2', '3').
card_multiverse_id('morphic tide'/'PC2', '226514').

card_in_set('mount keralia', 'PC2').
card_original_type('mount keralia'/'PC2', 'Plane — Regatha').
card_original_text('mount keralia'/'PC2', 'At the beginning of your end step, put a pressure counter on Mount Keralia.\nWhen you planeswalk away from Mount Keralia, it deals damage equal to the number of pressure counters on it to each creature and each planeswalker.\nWhenever you roll {C}, prevent all damage that planes named Mount Keralia would deal this game to permanents you control.').
card_first_print('mount keralia', 'PC2').
card_image_name('mount keralia'/'PC2', 'mount keralia').
card_uid('mount keralia'/'PC2', 'PC2:Mount Keralia:mount keralia').
card_rarity('mount keralia'/'PC2', 'Common').
card_artist('mount keralia'/'PC2', 'Franz Vohwinkel').
card_number('mount keralia'/'PC2', '25').
card_multiverse_id('mount keralia'/'PC2', '226515').

card_in_set('mountain', 'PC2').
card_original_type('mountain'/'PC2', 'Basic Land — Mountain').
card_original_text('mountain'/'PC2', 'R').
card_image_name('mountain'/'PC2', 'mountain1').
card_uid('mountain'/'PC2', 'PC2:Mountain:mountain1').
card_rarity('mountain'/'PC2', 'Basic Land').
card_artist('mountain'/'PC2', 'John Avon').
card_number('mountain'/'PC2', '147').
card_multiverse_id('mountain'/'PC2', '276447').

card_in_set('mountain', 'PC2').
card_original_type('mountain'/'PC2', 'Basic Land — Mountain').
card_original_text('mountain'/'PC2', 'R').
card_image_name('mountain'/'PC2', 'mountain2').
card_uid('mountain'/'PC2', 'PC2:Mountain:mountain2').
card_rarity('mountain'/'PC2', 'Basic Land').
card_artist('mountain'/'PC2', 'Aleksi Briclot').
card_number('mountain'/'PC2', '148').
card_multiverse_id('mountain'/'PC2', '276449').

card_in_set('mountain', 'PC2').
card_original_type('mountain'/'PC2', 'Basic Land — Mountain').
card_original_text('mountain'/'PC2', 'R').
card_image_name('mountain'/'PC2', 'mountain3').
card_uid('mountain'/'PC2', 'PC2:Mountain:mountain3').
card_rarity('mountain'/'PC2', 'Basic Land').
card_artist('mountain'/'PC2', 'D. Alexander Gregory').
card_number('mountain'/'PC2', '149').
card_multiverse_id('mountain'/'PC2', '276465').

card_in_set('mountain', 'PC2').
card_original_type('mountain'/'PC2', 'Basic Land — Mountain').
card_original_text('mountain'/'PC2', 'R').
card_image_name('mountain'/'PC2', 'mountain4').
card_uid('mountain'/'PC2', 'PC2:Mountain:mountain4').
card_rarity('mountain'/'PC2', 'Basic Land').
card_artist('mountain'/'PC2', 'Véronique Meignaud').
card_number('mountain'/'PC2', '150').
card_multiverse_id('mountain'/'PC2', '276470').

card_in_set('mudbutton torchrunner', 'PC2').
card_original_type('mudbutton torchrunner'/'PC2', 'Creature — Goblin Warrior').
card_original_text('mudbutton torchrunner'/'PC2', 'When Mudbutton Torchrunner dies, it deals 3 damage to target creature or player.').
card_image_name('mudbutton torchrunner'/'PC2', 'mudbutton torchrunner').
card_uid('mudbutton torchrunner'/'PC2', 'PC2:Mudbutton Torchrunner:mudbutton torchrunner').
card_rarity('mudbutton torchrunner'/'PC2', 'Common').
card_artist('mudbutton torchrunner'/'PC2', 'Steve Ellis').
card_number('mudbutton torchrunner'/'PC2', '49').
card_flavor_text('mudbutton torchrunner'/'PC2', 'The oil sloshes against his skull as he nears his destination: the Frogtosser Games and the lighting of the Flaming Boggart.').
card_multiverse_id('mudbutton torchrunner'/'PC2', '271222').

card_in_set('mutual epiphany', 'PC2').
card_original_type('mutual epiphany'/'PC2', 'Phenomenon').
card_original_text('mutual epiphany'/'PC2', 'When you encounter Mutual Epiphany, each player draws four cards. (Then planeswalk away from this phenomenon.)').
card_first_print('mutual epiphany', 'PC2').
card_image_name('mutual epiphany'/'PC2', 'mutual epiphany').
card_uid('mutual epiphany'/'PC2', 'PC2:Mutual Epiphany:mutual epiphany').
card_rarity('mutual epiphany'/'PC2', 'Common').
card_artist('mutual epiphany'/'PC2', 'Jason Felix').
card_number('mutual epiphany'/'PC2', '4').
card_multiverse_id('mutual epiphany'/'PC2', '226520').

card_in_set('mycoloth', 'PC2').
card_original_type('mycoloth'/'PC2', 'Creature — Fungus').
card_original_text('mycoloth'/'PC2', 'Devour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\nAt the beginning of your upkeep, put a 1/1 green Saproling creature token onto the battlefield for each +1/+1 counter on Mycoloth.').
card_image_name('mycoloth'/'PC2', 'mycoloth').
card_uid('mycoloth'/'PC2', 'PC2:Mycoloth:mycoloth').
card_rarity('mycoloth'/'PC2', 'Rare').
card_artist('mycoloth'/'PC2', 'Raymond Swanland').
card_number('mycoloth'/'PC2', '68').
card_multiverse_id('mycoloth'/'PC2', '271161').

card_in_set('nephalia', 'PC2').
card_original_type('nephalia'/'PC2', 'Plane — Innistrad').
card_original_text('nephalia'/'PC2', 'At the beginning of your end step, put the top seven cards of your library into your graveyard. Then return a card at random from your graveyard to your hand.\nWhenever you roll {C}, return target card from your graveyard to your hand.').
card_first_print('nephalia', 'PC2').
card_image_name('nephalia'/'PC2', 'nephalia').
card_uid('nephalia'/'PC2', 'PC2:Nephalia:nephalia').
card_rarity('nephalia'/'PC2', 'Common').
card_artist('nephalia'/'PC2', 'Daniel Ljunggren').
card_number('nephalia'/'PC2', '26').
card_multiverse_id('nephalia'/'PC2', '226543').

card_in_set('nest invader', 'PC2').
card_original_type('nest invader'/'PC2', 'Creature — Eldrazi Drone').
card_original_text('nest invader'/'PC2', 'When Nest Invader enters the battlefield, put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('nest invader'/'PC2', 'nest invader').
card_uid('nest invader'/'PC2', 'PC2:Nest Invader:nest invader').
card_rarity('nest invader'/'PC2', 'Common').
card_artist('nest invader'/'PC2', 'Trevor Claxton').
card_number('nest invader'/'PC2', '69').
card_flavor_text('nest invader'/'PC2', 'It nurtures its masters\' glorious future.').
card_multiverse_id('nest invader'/'PC2', '275261').

card_in_set('ninja of the deep hours', 'PC2').
card_original_type('ninja of the deep hours'/'PC2', 'Creature — Human Ninja').
card_original_text('ninja of the deep hours'/'PC2', 'Ninjutsu {1}{U} ({1}{U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Ninja of the Deep Hours deals combat damage to a player, you may draw a card.').
card_image_name('ninja of the deep hours'/'PC2', 'ninja of the deep hours').
card_uid('ninja of the deep hours'/'PC2', 'PC2:Ninja of the Deep Hours:ninja of the deep hours').
card_rarity('ninja of the deep hours'/'PC2', 'Common').
card_artist('ninja of the deep hours'/'PC2', 'Dan Scott').
card_number('ninja of the deep hours'/'PC2', '21').
card_multiverse_id('ninja of the deep hours'/'PC2', '271184').

card_in_set('noggle ransacker', 'PC2').
card_original_type('noggle ransacker'/'PC2', 'Creature — Noggle Rogue').
card_original_text('noggle ransacker'/'PC2', 'When Noggle Ransacker enters the battlefield, each player draws two cards, then discards a card at random.').
card_image_name('noggle ransacker'/'PC2', 'noggle ransacker').
card_uid('noggle ransacker'/'PC2', 'PC2:Noggle Ransacker:noggle ransacker').
card_rarity('noggle ransacker'/'PC2', 'Uncommon').
card_artist('noggle ransacker'/'PC2', 'Alex Horley-Orlandelli').
card_number('noggle ransacker'/'PC2', '102').
card_flavor_text('noggle ransacker'/'PC2', 'Noggles live purely by what they can scavenge. There is not a single thing a noggle eats, wears, or uses that did not once belong to another.').
card_multiverse_id('noggle ransacker'/'PC2', '271144').

card_in_set('norn\'s dominion', 'PC2').
card_original_type('norn\'s dominion'/'PC2', 'Plane — New Phyrexia').
card_original_text('norn\'s dominion'/'PC2', 'When you planeswalk away from Norn\'s Dominion, destroy each nonland permanent without a fate counter on it, then remove all fate counters from all permanents.\nWhenever you roll {C}, you may put a fate counter on target permanent.').
card_first_print('norn\'s dominion', 'PC2').
card_image_name('norn\'s dominion'/'PC2', 'norn\'s dominion').
card_uid('norn\'s dominion'/'PC2', 'PC2:Norn\'s Dominion:norn\'s dominion').
card_rarity('norn\'s dominion'/'PC2', 'Common').
card_artist('norn\'s dominion'/'PC2', 'Igor Kieryluk').
card_number('norn\'s dominion'/'PC2', '27').
card_multiverse_id('norn\'s dominion'/'PC2', '226540').

card_in_set('nullmage advocate', 'PC2').
card_original_type('nullmage advocate'/'PC2', 'Creature — Insect Druid').
card_original_text('nullmage advocate'/'PC2', '{T}: Return two target cards from an opponent\'s graveyard to his or her hand. Destroy target artifact or enchantment.').
card_image_name('nullmage advocate'/'PC2', 'nullmage advocate').
card_uid('nullmage advocate'/'PC2', 'PC2:Nullmage Advocate:nullmage advocate').
card_rarity('nullmage advocate'/'PC2', 'Common').
card_artist('nullmage advocate'/'PC2', 'Darrell Riche').
card_number('nullmage advocate'/'PC2', '70').
card_flavor_text('nullmage advocate'/'PC2', '\"Our unity unmasks your deceit.\"').
card_multiverse_id('nullmage advocate'/'PC2', '271178').

card_in_set('okiba-gang shinobi', 'PC2').
card_original_type('okiba-gang shinobi'/'PC2', 'Creature — Rat Ninja').
card_original_text('okiba-gang shinobi'/'PC2', 'Ninjutsu {3}{B} ({3}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Okiba-Gang Shinobi deals combat damage to a player, that player discards two cards.').
card_image_name('okiba-gang shinobi'/'PC2', 'okiba-gang shinobi').
card_uid('okiba-gang shinobi'/'PC2', 'PC2:Okiba-Gang Shinobi:okiba-gang shinobi').
card_rarity('okiba-gang shinobi'/'PC2', 'Common').
card_artist('okiba-gang shinobi'/'PC2', 'Mark Zug').
card_number('okiba-gang shinobi'/'PC2', '35').
card_multiverse_id('okiba-gang shinobi'/'PC2', '271185').

card_in_set('onakke catacomb', 'PC2').
card_original_type('onakke catacomb'/'PC2', 'Plane — Shandalar').
card_original_text('onakke catacomb'/'PC2', 'All creatures are black and have deathtouch.\nWhenever you roll {C}, creatures you control get +1/+0 and gain first strike until end of turn.').
card_first_print('onakke catacomb', 'PC2').
card_image_name('onakke catacomb'/'PC2', 'onakke catacomb').
card_uid('onakke catacomb'/'PC2', 'PC2:Onakke Catacomb:onakke catacomb').
card_rarity('onakke catacomb'/'PC2', 'Common').
card_artist('onakke catacomb'/'PC2', 'Nic Klein').
card_number('onakke catacomb'/'PC2', '28').
card_multiverse_id('onakke catacomb'/'PC2', '226513').

card_in_set('ondu giant', 'PC2').
card_original_type('ondu giant'/'PC2', 'Creature — Giant Druid').
card_original_text('ondu giant'/'PC2', 'When Ondu Giant enters the battlefield, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_image_name('ondu giant'/'PC2', 'ondu giant').
card_uid('ondu giant'/'PC2', 'PC2:Ondu Giant:ondu giant').
card_rarity('ondu giant'/'PC2', 'Common').
card_artist('ondu giant'/'PC2', 'Igor Kieryluk').
card_number('ondu giant'/'PC2', '71').
card_flavor_text('ondu giant'/'PC2', 'Some druids nurture gardens. Others nurture continents.').
card_multiverse_id('ondu giant'/'PC2', '271153').

card_in_set('orochi colony', 'PC2').
card_original_type('orochi colony'/'PC2', 'Plane — Kamigawa').
card_original_text('orochi colony'/'PC2', 'Whenever a creature you control deals combat damage to a player, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.\nWhenever you roll {C}, target creature is unblockable this turn.').
card_first_print('orochi colony', 'PC2').
card_image_name('orochi colony'/'PC2', 'orochi colony').
card_uid('orochi colony'/'PC2', 'PC2:Orochi Colony:orochi colony').
card_rarity('orochi colony'/'PC2', 'Common').
card_artist('orochi colony'/'PC2', 'Charles Urbach').
card_number('orochi colony'/'PC2', '29').
card_multiverse_id('orochi colony'/'PC2', '226524').

card_in_set('orzhova', 'PC2').
card_original_type('orzhova'/'PC2', 'Plane — Ravnica').
card_original_text('orzhova'/'PC2', 'When you planeswalk away from Orzhova, each player returns all creature cards from his or her graveyard to the battlefield.\nWhenever you roll {C}, for each opponent, exile up to one target creature card from that player\'s graveyard.').
card_first_print('orzhova', 'PC2').
card_image_name('orzhova'/'PC2', 'orzhova').
card_uid('orzhova'/'PC2', 'PC2:Orzhova:orzhova').
card_rarity('orzhova'/'PC2', 'Common').
card_artist('orzhova'/'PC2', 'Charles Urbach').
card_number('orzhova'/'PC2', '30').
card_multiverse_id('orzhova'/'PC2', '226507').

card_in_set('overrun', 'PC2').
card_original_type('overrun'/'PC2', 'Sorcery').
card_original_text('overrun'/'PC2', 'Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('overrun'/'PC2', 'overrun').
card_uid('overrun'/'PC2', 'PC2:Overrun:overrun').
card_rarity('overrun'/'PC2', 'Uncommon').
card_artist('overrun'/'PC2', 'Carl Critchlow').
card_number('overrun'/'PC2', '72').
card_flavor_text('overrun'/'PC2', 'Nature doesn\'t walk.').
card_multiverse_id('overrun'/'PC2', '271201').

card_in_set('penumbra spider', 'PC2').
card_original_type('penumbra spider'/'PC2', 'Creature — Spider').
card_original_text('penumbra spider'/'PC2', 'Reach\nWhen Penumbra Spider dies, put a 2/4 black Spider creature token with reach onto the battlefield.').
card_image_name('penumbra spider'/'PC2', 'penumbra spider').
card_uid('penumbra spider'/'PC2', 'PC2:Penumbra Spider:penumbra spider').
card_rarity('penumbra spider'/'PC2', 'Common').
card_artist('penumbra spider'/'PC2', 'Jeff Easley').
card_number('penumbra spider'/'PC2', '73').
card_flavor_text('penumbra spider'/'PC2', 'When it snared a passing cockatrice, its own soul darkly doubled.').
card_multiverse_id('penumbra spider'/'PC2', '271223').

card_in_set('peregrine drake', 'PC2').
card_original_type('peregrine drake'/'PC2', 'Creature — Drake').
card_original_text('peregrine drake'/'PC2', 'Flying\nWhen Peregrine Drake enters the battlefield, untap up to five lands.').
card_image_name('peregrine drake'/'PC2', 'peregrine drake').
card_uid('peregrine drake'/'PC2', 'PC2:Peregrine Drake:peregrine drake').
card_rarity('peregrine drake'/'PC2', 'Uncommon').
card_artist('peregrine drake'/'PC2', 'Bob Eggleton').
card_number('peregrine drake'/'PC2', '22').
card_flavor_text('peregrine drake'/'PC2', 'That the Tolarian mists parted for the drakes was warning enough to stay away.').
card_multiverse_id('peregrine drake'/'PC2', '271190').

card_in_set('plains', 'PC2').
card_original_type('plains'/'PC2', 'Basic Land — Plains').
card_original_text('plains'/'PC2', 'W').
card_image_name('plains'/'PC2', 'plains1').
card_uid('plains'/'PC2', 'PC2:Plains:plains1').
card_rarity('plains'/'PC2', 'Basic Land').
card_artist('plains'/'PC2', 'John Avon').
card_number('plains'/'PC2', '132').
card_multiverse_id('plains'/'PC2', '276473').

card_in_set('plains', 'PC2').
card_original_type('plains'/'PC2', 'Basic Land — Plains').
card_original_text('plains'/'PC2', 'W').
card_image_name('plains'/'PC2', 'plains2').
card_uid('plains'/'PC2', 'PC2:Plains:plains2').
card_rarity('plains'/'PC2', 'Basic Land').
card_artist('plains'/'PC2', 'John Avon').
card_number('plains'/'PC2', '133').
card_multiverse_id('plains'/'PC2', '276469').

card_in_set('plains', 'PC2').
card_original_type('plains'/'PC2', 'Basic Land — Plains').
card_original_text('plains'/'PC2', 'W').
card_image_name('plains'/'PC2', 'plains3').
card_uid('plains'/'PC2', 'PC2:Plains:plains3').
card_rarity('plains'/'PC2', 'Basic Land').
card_artist('plains'/'PC2', 'Véronique Meignaud').
card_number('plains'/'PC2', '134').
card_multiverse_id('plains'/'PC2', '276458').

card_in_set('plains', 'PC2').
card_original_type('plains'/'PC2', 'Basic Land — Plains').
card_original_text('plains'/'PC2', 'W').
card_image_name('plains'/'PC2', 'plains4').
card_uid('plains'/'PC2', 'PC2:Plains:plains4').
card_rarity('plains'/'PC2', 'Basic Land').
card_artist('plains'/'PC2', 'Jung Park').
card_number('plains'/'PC2', '135').
card_multiverse_id('plains'/'PC2', '276459').

card_in_set('plains', 'PC2').
card_original_type('plains'/'PC2', 'Basic Land — Plains').
card_original_text('plains'/'PC2', 'W').
card_image_name('plains'/'PC2', 'plains5').
card_uid('plains'/'PC2', 'PC2:Plains:plains5').
card_rarity('plains'/'PC2', 'Basic Land').
card_artist('plains'/'PC2', 'Vincent Proce').
card_number('plains'/'PC2', '136').
card_multiverse_id('plains'/'PC2', '276461').

card_in_set('planewide disaster', 'PC2').
card_original_type('planewide disaster'/'PC2', 'Phenomenon').
card_original_text('planewide disaster'/'PC2', 'When you encounter Planewide Disaster, destroy all creatures. (Then planeswalk away from this phenomenon.)').
card_first_print('planewide disaster', 'PC2').
card_image_name('planewide disaster'/'PC2', 'planewide disaster').
card_uid('planewide disaster'/'PC2', 'PC2:Planewide Disaster:planewide disaster').
card_rarity('planewide disaster'/'PC2', 'Common').
card_artist('planewide disaster'/'PC2', 'Dave Kendall').
card_number('planewide disaster'/'PC2', '5').
card_multiverse_id('planewide disaster'/'PC2', '226545').

card_in_set('pollenbright wings', 'PC2').
card_original_type('pollenbright wings'/'PC2', 'Enchantment — Aura').
card_original_text('pollenbright wings'/'PC2', 'Enchant creature\nEnchanted creature has flying.\nWhenever enchanted creature deals combat damage to a player, put that many 1/1 green Saproling creature tokens onto the battlefield.').
card_image_name('pollenbright wings'/'PC2', 'pollenbright wings').
card_uid('pollenbright wings'/'PC2', 'PC2:Pollenbright Wings:pollenbright wings').
card_rarity('pollenbright wings'/'PC2', 'Uncommon').
card_artist('pollenbright wings'/'PC2', 'Terese Nielsen').
card_number('pollenbright wings'/'PC2', '103').
card_multiverse_id('pollenbright wings'/'PC2', '271211').

card_in_set('prahv', 'PC2').
card_original_type('prahv'/'PC2', 'Plane — Ravnica').
card_original_text('prahv'/'PC2', 'If you cast a spell this turn, you can\'t attack with creatures.\nIf you attacked with creatures this turn, you can\'t cast spells.\nWhenever you roll {C}, you gain life equal to the number of cards in your hand.').
card_first_print('prahv', 'PC2').
card_image_name('prahv'/'PC2', 'prahv').
card_uid('prahv'/'PC2', 'PC2:Prahv:prahv').
card_rarity('prahv'/'PC2', 'Common').
card_artist('prahv'/'PC2', 'Drew Baker').
card_number('prahv'/'PC2', '31').
card_multiverse_id('prahv'/'PC2', '226552').

card_in_set('predatory urge', 'PC2').
card_original_type('predatory urge'/'PC2', 'Enchantment — Aura').
card_original_text('predatory urge'/'PC2', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals damage equal to its power to target creature. That creature deals damage equal to its power to this creature.\"').
card_image_name('predatory urge'/'PC2', 'predatory urge').
card_uid('predatory urge'/'PC2', 'PC2:Predatory Urge:predatory urge').
card_rarity('predatory urge'/'PC2', 'Rare').
card_artist('predatory urge'/'PC2', 'Scott Chou').
card_number('predatory urge'/'PC2', '74').
card_multiverse_id('predatory urge'/'PC2', '271136').

card_in_set('preyseizer dragon', 'PC2').
card_original_type('preyseizer dragon'/'PC2', 'Creature — Dragon').
card_original_text('preyseizer dragon'/'PC2', 'Flying\nDevour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\nWhenever Preyseizer Dragon attacks, it deals damage to target creature or player equal to the number of +1/+1 counters on Preyseizer Dragon.').
card_first_print('preyseizer dragon', 'PC2').
card_image_name('preyseizer dragon'/'PC2', 'preyseizer dragon').
card_uid('preyseizer dragon'/'PC2', 'PC2:Preyseizer Dragon:preyseizer dragon').
card_rarity('preyseizer dragon'/'PC2', 'Rare').
card_artist('preyseizer dragon'/'PC2', 'Daarken').
card_number('preyseizer dragon'/'PC2', '50').
card_multiverse_id('preyseizer dragon'/'PC2', '265150').

card_in_set('primal plasma', 'PC2').
card_original_type('primal plasma'/'PC2', 'Creature — Elemental Shapeshifter').
card_original_text('primal plasma'/'PC2', 'As Primal Plasma enters the battlefield, it becomes your choice of a 3/3 creature, a 2/2 creature with flying, or a 1/6 creature with defender.').
card_image_name('primal plasma'/'PC2', 'primal plasma').
card_uid('primal plasma'/'PC2', 'PC2:Primal Plasma:primal plasma').
card_rarity('primal plasma'/'PC2', 'Common').
card_artist('primal plasma'/'PC2', 'Luca Zontini').
card_number('primal plasma'/'PC2', '23').
card_flavor_text('primal plasma'/'PC2', 'Tocasia brushed the gears and cogs from the table. There, before two wide-eyed brothers, she began a lesson on raw elemental magic.').
card_multiverse_id('primal plasma'/'PC2', '271175').

card_in_set('quicksilver sea', 'PC2').
card_original_type('quicksilver sea'/'PC2', 'Plane — Mirrodin').
card_original_text('quicksilver sea'/'PC2', 'When you planeswalk to Quicksilver Sea or at the beginning of your upkeep, scry 4. (To scry 4, look at the top four cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)\nWhenever you roll {C}, reveal the top card of your library. You may play it without paying its mana cost.').
card_first_print('quicksilver sea', 'PC2').
card_image_name('quicksilver sea'/'PC2', 'quicksilver sea').
card_uid('quicksilver sea'/'PC2', 'PC2:Quicksilver Sea:quicksilver sea').
card_rarity('quicksilver sea'/'PC2', 'Common').
card_artist('quicksilver sea'/'PC2', 'Charles Urbach').
card_number('quicksilver sea'/'PC2', '32').
card_multiverse_id('quicksilver sea'/'PC2', '226518').

card_in_set('quiet disrepair', 'PC2').
card_original_type('quiet disrepair'/'PC2', 'Enchantment — Aura').
card_original_text('quiet disrepair'/'PC2', 'Enchant artifact or enchantment\nAt the beginning of your upkeep, choose one — Destroy enchanted permanent; or you gain 2 life.').
card_image_name('quiet disrepair'/'PC2', 'quiet disrepair').
card_uid('quiet disrepair'/'PC2', 'PC2:Quiet Disrepair:quiet disrepair').
card_rarity('quiet disrepair'/'PC2', 'Common').
card_artist('quiet disrepair'/'PC2', 'Glen Angus').
card_number('quiet disrepair'/'PC2', '75').
card_flavor_text('quiet disrepair'/'PC2', '\"Artifice has always stood upon nature\'s shoulders. Let us watch nature take a turn.\"\n—Freyalise').
card_multiverse_id('quiet disrepair'/'PC2', '271238').

card_in_set('quietus spike', 'PC2').
card_original_type('quietus spike'/'PC2', 'Artifact — Equipment').
card_original_text('quietus spike'/'PC2', 'Equipped creature has deathtouch.\nWhenever equipped creature deals combat damage to a player, that player loses half his or her life, rounded up.\nEquip {3}').
card_image_name('quietus spike'/'PC2', 'quietus spike').
card_uid('quietus spike'/'PC2', 'PC2:Quietus Spike:quietus spike').
card_rarity('quietus spike'/'PC2', 'Rare').
card_artist('quietus spike'/'PC2', 'Mark Brill').
card_number('quietus spike'/'PC2', '112').
card_multiverse_id('quietus spike'/'PC2', '271219').

card_in_set('rancor', 'PC2').
card_original_type('rancor'/'PC2', 'Enchantment — Aura').
card_original_text('rancor'/'PC2', 'Enchant creature\nEnchanted creature gets +2/+0 and has trample.\nWhen Rancor is put into a graveyard from the battlefield, return Rancor to its owner\'s hand.').
card_image_name('rancor'/'PC2', 'rancor').
card_uid('rancor'/'PC2', 'PC2:Rancor:rancor').
card_rarity('rancor'/'PC2', 'Common').
card_artist('rancor'/'PC2', 'Kev Walker').
card_number('rancor'/'PC2', '76').
card_multiverse_id('rancor'/'PC2', '275266').

card_in_set('reality shaping', 'PC2').
card_original_type('reality shaping'/'PC2', 'Phenomenon').
card_original_text('reality shaping'/'PC2', 'When you encounter Reality Shaping, starting with you, each player may put a permanent card from his or her hand onto the battlefield. (Then planeswalk away from this phenomenon.)').
card_first_print('reality shaping', 'PC2').
card_image_name('reality shaping'/'PC2', 'reality shaping').
card_uid('reality shaping'/'PC2', 'PC2:Reality Shaping:reality shaping').
card_rarity('reality shaping'/'PC2', 'Common').
card_artist('reality shaping'/'PC2', 'Dan Scott').
card_number('reality shaping'/'PC2', '6').
card_multiverse_id('reality shaping'/'PC2', '226508').

card_in_set('rivals\' duel', 'PC2').
card_original_type('rivals\' duel'/'PC2', 'Sorcery').
card_original_text('rivals\' duel'/'PC2', 'Choose two target creatures that share no creature types. Those creatures fight each other. (Each deals damage equal to its power to the other.)').
card_image_name('rivals\' duel'/'PC2', 'rivals\' duel').
card_uid('rivals\' duel'/'PC2', 'PC2:Rivals\' Duel:rivals\' duel').
card_rarity('rivals\' duel'/'PC2', 'Uncommon').
card_artist('rivals\' duel'/'PC2', 'Zoltan Boros & Gabor Szikszai').
card_number('rivals\' duel'/'PC2', '51').
card_flavor_text('rivals\' duel'/'PC2', 'They could agree on one thing only: one of them must die.').
card_multiverse_id('rivals\' duel'/'PC2', '271200').

card_in_set('rupture spire', 'PC2').
card_original_type('rupture spire'/'PC2', 'Land').
card_original_text('rupture spire'/'PC2', 'Rupture Spire enters the battlefield tapped.\nWhen Rupture Spire enters the battlefield, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('rupture spire'/'PC2', 'rupture spire').
card_uid('rupture spire'/'PC2', 'PC2:Rupture Spire:rupture spire').
card_rarity('rupture spire'/'PC2', 'Common').
card_artist('rupture spire'/'PC2', 'Jaime Jones').
card_number('rupture spire'/'PC2', '124').
card_multiverse_id('rupture spire'/'PC2', '271142').

card_in_set('sai of the shinobi', 'PC2').
card_original_type('sai of the shinobi'/'PC2', 'Artifact — Equipment').
card_original_text('sai of the shinobi'/'PC2', 'Equipped creature gets +1/+1.\nWhenever a creature enters the battlefield under your control, you may attach Sai of the Shinobi to it.\nEquip {2}').
card_first_print('sai of the shinobi', 'PC2').
card_image_name('sai of the shinobi'/'PC2', 'sai of the shinobi').
card_uid('sai of the shinobi'/'PC2', 'PC2:Sai of the Shinobi:sai of the shinobi').
card_rarity('sai of the shinobi'/'PC2', 'Uncommon').
card_artist('sai of the shinobi'/'PC2', 'Brian Snõddy').
card_number('sai of the shinobi'/'PC2', '113').
card_flavor_text('sai of the shinobi'/'PC2', 'The passing of the sai presages the end of the old clan and the ascent of the new.').
card_multiverse_id('sai of the shinobi'/'PC2', '270731').

card_in_set('sakashima\'s student', 'PC2').
card_original_type('sakashima\'s student'/'PC2', 'Creature — Human Ninja').
card_original_text('sakashima\'s student'/'PC2', 'Ninjutsu {1}{U} ({1}{U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nYou may have Sakashima\'s Student enter the battlefield as a copy of any creature on the battlefield, except it\'s still a Ninja in addition to its other creature types.').
card_first_print('sakashima\'s student', 'PC2').
card_image_name('sakashima\'s student'/'PC2', 'sakashima\'s student').
card_uid('sakashima\'s student'/'PC2', 'PC2:Sakashima\'s Student:sakashima\'s student').
card_rarity('sakashima\'s student'/'PC2', 'Rare').
card_artist('sakashima\'s student'/'PC2', 'Brian Snõddy').
card_number('sakashima\'s student'/'PC2', '24').
card_multiverse_id('sakashima\'s student'/'PC2', '265159').

card_in_set('see beyond', 'PC2').
card_original_type('see beyond'/'PC2', 'Sorcery').
card_original_text('see beyond'/'PC2', 'Draw two cards, then shuffle a card from your hand into your library.').
card_image_name('see beyond'/'PC2', 'see beyond').
card_uid('see beyond'/'PC2', 'PC2:See Beyond:see beyond').
card_rarity('see beyond'/'PC2', 'Common').
card_artist('see beyond'/'PC2', 'Andrew Robinson').
card_number('see beyond'/'PC2', '25').
card_flavor_text('see beyond'/'PC2', 'Ancient lore locked in a mind driven mad is just as safe as when it was locked deep underground.').
card_multiverse_id('see beyond'/'PC2', '271154').

card_in_set('selesnya loft gardens', 'PC2').
card_original_type('selesnya loft gardens'/'PC2', 'Plane — Ravnica').
card_original_text('selesnya loft gardens'/'PC2', 'If an effect would put one or more tokens onto the battlefield, it puts twice that many of those tokens onto the battlefield instead.\nIf an effect would place one or more counters on a permanent, it places twice that many of those counters on that permanent instead.\nWhenever you roll {C}, until end of turn, whenever you tap a land for mana, add one mana to your mana pool of any type that land produced.').
card_first_print('selesnya loft gardens', 'PC2').
card_image_name('selesnya loft gardens'/'PC2', 'selesnya loft gardens').
card_uid('selesnya loft gardens'/'PC2', 'PC2:Selesnya Loft Gardens:selesnya loft gardens').
card_rarity('selesnya loft gardens'/'PC2', 'Common').
card_artist('selesnya loft gardens'/'PC2', 'Martina Pilcerova').
card_number('selesnya loft gardens'/'PC2', '33').
card_multiverse_id('selesnya loft gardens'/'PC2', '226538').

card_in_set('selesnya sanctuary', 'PC2').
card_original_type('selesnya sanctuary'/'PC2', 'Land').
card_original_text('selesnya sanctuary'/'PC2', 'Selesnya Sanctuary enters the battlefield tapped.\nWhen Selesnya Sanctuary enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {G}{W} to your mana pool.').
card_image_name('selesnya sanctuary'/'PC2', 'selesnya sanctuary').
card_uid('selesnya sanctuary'/'PC2', 'PC2:Selesnya Sanctuary:selesnya sanctuary').
card_rarity('selesnya sanctuary'/'PC2', 'Common').
card_artist('selesnya sanctuary'/'PC2', 'John Avon').
card_number('selesnya sanctuary'/'PC2', '125').
card_multiverse_id('selesnya sanctuary'/'PC2', '271212').

card_in_set('shardless agent', 'PC2').
card_original_type('shardless agent'/'PC2', 'Artifact Creature — Human Rogue').
card_original_text('shardless agent'/'PC2', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_first_print('shardless agent', 'PC2').
card_image_name('shardless agent'/'PC2', 'shardless agent').
card_uid('shardless agent'/'PC2', 'PC2:Shardless Agent:shardless agent').
card_rarity('shardless agent'/'PC2', 'Uncommon').
card_artist('shardless agent'/'PC2', 'Izzy').
card_number('shardless agent'/'PC2', '104').
card_multiverse_id('shardless agent'/'PC2', '270733').

card_in_set('shimmering grotto', 'PC2').
card_original_type('shimmering grotto'/'PC2', 'Land').
card_original_text('shimmering grotto'/'PC2', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_image_name('shimmering grotto'/'PC2', 'shimmering grotto').
card_uid('shimmering grotto'/'PC2', 'PC2:Shimmering Grotto:shimmering grotto').
card_rarity('shimmering grotto'/'PC2', 'Common').
card_artist('shimmering grotto'/'PC2', 'Cliff Childs').
card_number('shimmering grotto'/'PC2', '126').
card_flavor_text('shimmering grotto'/'PC2', '\"This is the price of being last in the food chain. We must keep our places of beauty hidden away instead of displayed for all to see.\"\n—Hildin, priest of Avacyn').
card_multiverse_id('shimmering grotto'/'PC2', '271203').

card_in_set('sigil of the empty throne', 'PC2').
card_original_type('sigil of the empty throne'/'PC2', 'Enchantment').
card_original_text('sigil of the empty throne'/'PC2', 'Whenever you cast an enchantment spell, put a 4/4 white Angel creature token with flying onto the battlefield.').
card_image_name('sigil of the empty throne'/'PC2', 'sigil of the empty throne').
card_uid('sigil of the empty throne'/'PC2', 'PC2:Sigil of the Empty Throne:sigil of the empty throne').
card_rarity('sigil of the empty throne'/'PC2', 'Rare').
card_artist('sigil of the empty throne'/'PC2', 'Cyril Van Der Haegen').
card_number('sigil of the empty throne'/'PC2', '11').
card_flavor_text('sigil of the empty throne'/'PC2', 'When Asha left Bant, she ensured that the world would have protection and order in her absence.').
card_multiverse_id('sigil of the empty throne'/'PC2', '275258').

card_in_set('silent-blade oni', 'PC2').
card_original_type('silent-blade oni'/'PC2', 'Creature — Demon Ninja').
card_original_text('silent-blade oni'/'PC2', 'Ninjutsu {4}{U}{B} ({4}{U}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Silent-Blade Oni deals combat damage to a player, look at that player\'s hand. You may cast a nonland card in it without paying that card\'s mana cost.').
card_first_print('silent-blade oni', 'PC2').
card_image_name('silent-blade oni'/'PC2', 'silent-blade oni').
card_uid('silent-blade oni'/'PC2', 'PC2:Silent-Blade Oni:silent-blade oni').
card_rarity('silent-blade oni'/'PC2', 'Rare').
card_artist('silent-blade oni'/'PC2', 'Steve Prescott').
card_number('silent-blade oni'/'PC2', '105').
card_multiverse_id('silent-blade oni'/'PC2', '270727').

card_in_set('silhana ledgewalker', 'PC2').
card_original_type('silhana ledgewalker'/'PC2', 'Creature — Elf Rogue').
card_original_text('silhana ledgewalker'/'PC2', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nSilhana Ledgewalker can\'t be blocked except by creatures with flying.').
card_image_name('silhana ledgewalker'/'PC2', 'silhana ledgewalker').
card_uid('silhana ledgewalker'/'PC2', 'PC2:Silhana Ledgewalker:silhana ledgewalker').
card_rarity('silhana ledgewalker'/'PC2', 'Common').
card_artist('silhana ledgewalker'/'PC2', 'James Wong').
card_number('silhana ledgewalker'/'PC2', '77').
card_flavor_text('silhana ledgewalker'/'PC2', 'Street folk call them \"spire mice,\" but behind the mockery is an unspoken envy of the ledgewalkers\' skill at avoiding harm.').
card_multiverse_id('silhana ledgewalker'/'PC2', '271230').

card_in_set('skarrg, the rage pits', 'PC2').
card_original_type('skarrg, the rage pits'/'PC2', 'Land').
card_original_text('skarrg, the rage pits'/'PC2', '{T}: Add {1} to your mana pool.\n{R}{G}, {T}: Target creature gets +1/+1 and gains trample until end of turn.').
card_image_name('skarrg, the rage pits'/'PC2', 'skarrg, the rage pits').
card_uid('skarrg, the rage pits'/'PC2', 'PC2:Skarrg, the Rage Pits:skarrg, the rage pits').
card_rarity('skarrg, the rage pits'/'PC2', 'Uncommon').
card_artist('skarrg, the rage pits'/'PC2', 'Martina Pilcerova').
card_number('skarrg, the rage pits'/'PC2', '127').
card_flavor_text('skarrg, the rage pits'/'PC2', '\"This palace will be our fire-spit, and roasted prince our victory meal. Send in the torch-pigs!\"\n—Ghut Rak, Gruul guildmage').
card_multiverse_id('skarrg, the rage pits'/'PC2', '271231').

card_in_set('skullsnatcher', 'PC2').
card_original_type('skullsnatcher'/'PC2', 'Creature — Rat Ninja').
card_original_text('skullsnatcher'/'PC2', 'Ninjutsu {B} ({B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Skullsnatcher deals combat damage to a player, exile up to two target cards from that player\'s graveyard.').
card_image_name('skullsnatcher'/'PC2', 'skullsnatcher').
card_uid('skullsnatcher'/'PC2', 'PC2:Skullsnatcher:skullsnatcher').
card_rarity('skullsnatcher'/'PC2', 'Common').
card_artist('skullsnatcher'/'PC2', 'Matt Cavotta').
card_number('skullsnatcher'/'PC2', '36').
card_multiverse_id('skullsnatcher'/'PC2', '271186').

card_in_set('snake umbra', 'PC2').
card_original_type('snake umbra'/'PC2', 'Enchantment — Aura').
card_original_text('snake umbra'/'PC2', 'Enchant creature\nEnchanted creature gets +1/+1 and has \"Whenever this creature deals damage to an opponent, you may draw a card.\"\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_image_name('snake umbra'/'PC2', 'snake umbra').
card_uid('snake umbra'/'PC2', 'PC2:Snake Umbra:snake umbra').
card_rarity('snake umbra'/'PC2', 'Common').
card_artist('snake umbra'/'PC2', 'Christopher Moeller').
card_number('snake umbra'/'PC2', '78').
card_multiverse_id('snake umbra'/'PC2', '271155').

card_in_set('spatial merging', 'PC2').
card_original_type('spatial merging'/'PC2', 'Phenomenon').
card_original_text('spatial merging'/'PC2', 'When you encounter Spatial Merging, reveal cards from the top of your planar deck until you reveal two plane cards. Simultaneously planeswalk to both of them. Put all other cards revealed this way on the bottom of your planar deck in any order.').
card_first_print('spatial merging', 'PC2').
card_image_name('spatial merging'/'PC2', 'spatial merging').
card_uid('spatial merging'/'PC2', 'PC2:Spatial Merging:spatial merging').
card_rarity('spatial merging'/'PC2', 'Common').
card_artist('spatial merging'/'PC2', 'Gabor Szikszai').
card_number('spatial merging'/'PC2', '7').
card_multiverse_id('spatial merging'/'PC2', '226546').

card_in_set('spirit mantle', 'PC2').
card_original_type('spirit mantle'/'PC2', 'Enchantment — Aura').
card_original_text('spirit mantle'/'PC2', 'Enchant creature\nEnchanted creature gets +1/+1 and has protection from creatures.').
card_image_name('spirit mantle'/'PC2', 'spirit mantle').
card_uid('spirit mantle'/'PC2', 'PC2:Spirit Mantle:spirit mantle').
card_rarity('spirit mantle'/'PC2', 'Uncommon').
card_artist('spirit mantle'/'PC2', 'Izzy').
card_number('spirit mantle'/'PC2', '12').
card_flavor_text('spirit mantle'/'PC2', 'The shield of unimpeachable purity is as strong as any wrought on the anvil.').
card_multiverse_id('spirit mantle'/'PC2', '275265').

card_in_set('stairs to infinity', 'PC2').
card_original_type('stairs to infinity'/'PC2', 'Plane — Xerex').
card_original_text('stairs to infinity'/'PC2', 'Players have no maximum hand size.\nWhenever you roll the planar die, draw a card.\nWhenever you roll {C}, reveal the top card of your planar deck. You may put it on the bottom of your planar deck.').
card_first_print('stairs to infinity', 'PC2').
card_image_name('stairs to infinity'/'PC2', 'stairs to infinity').
card_uid('stairs to infinity'/'PC2', 'PC2:Stairs to Infinity:stairs to infinity').
card_rarity('stairs to infinity'/'PC2', 'Rare').
card_artist('stairs to infinity'/'PC2', 'Steven Belledin').
card_number('stairs to infinity'/'PC2', 'P1').
card_multiverse_id('stairs to infinity'/'PC2', '226521').

card_in_set('stensia', 'PC2').
card_original_type('stensia'/'PC2', 'Plane — Innistrad').
card_original_text('stensia'/'PC2', 'Whenever a creature deals damage to one or more players for the first time in a turn, put a +1/+1 counter on it.\nWhenever you roll {C}, each creature you control gains \"{T}: This creature deals 1 damage to target player\" until end of turn.').
card_first_print('stensia', 'PC2').
card_image_name('stensia'/'PC2', 'stensia').
card_uid('stensia'/'PC2', 'PC2:Stensia:stensia').
card_rarity('stensia'/'PC2', 'Common').
card_artist('stensia'/'PC2', 'Vincent Proce').
card_number('stensia'/'PC2', '34').
card_multiverse_id('stensia'/'PC2', '226555').

card_in_set('sunken hope', 'PC2').
card_original_type('sunken hope'/'PC2', 'Enchantment').
card_original_text('sunken hope'/'PC2', 'At the beginning of each player\'s upkeep, that player returns a creature he or she controls to its owner\'s hand.').
card_image_name('sunken hope'/'PC2', 'sunken hope').
card_uid('sunken hope'/'PC2', 'PC2:Sunken Hope:sunken hope').
card_rarity('sunken hope'/'PC2', 'Rare').
card_artist('sunken hope'/'PC2', 'Volkan Baga').
card_number('sunken hope'/'PC2', '26').
card_flavor_text('sunken hope'/'PC2', '\"I don\'t concern myself with my enemy\'s hopes. Hopes follow the tides. They will retreat soon enough.\"\n—Ambassador Laquatus').
card_multiverse_id('sunken hope'/'PC2', '271202').

card_in_set('swamp', 'PC2').
card_original_type('swamp'/'PC2', 'Basic Land — Swamp').
card_original_text('swamp'/'PC2', 'B').
card_image_name('swamp'/'PC2', 'swamp1').
card_uid('swamp'/'PC2', 'PC2:Swamp:swamp1').
card_rarity('swamp'/'PC2', 'Basic Land').
card_artist('swamp'/'PC2', 'John Avon').
card_number('swamp'/'PC2', '142').
card_multiverse_id('swamp'/'PC2', '276472').

card_in_set('swamp', 'PC2').
card_original_type('swamp'/'PC2', 'Basic Land — Swamp').
card_original_text('swamp'/'PC2', 'B').
card_image_name('swamp'/'PC2', 'swamp2').
card_uid('swamp'/'PC2', 'PC2:Swamp:swamp2').
card_rarity('swamp'/'PC2', 'Basic Land').
card_artist('swamp'/'PC2', 'Jim Nelson').
card_number('swamp'/'PC2', '143').
card_multiverse_id('swamp'/'PC2', '276463').

card_in_set('swamp', 'PC2').
card_original_type('swamp'/'PC2', 'Basic Land — Swamp').
card_original_text('swamp'/'PC2', 'B').
card_image_name('swamp'/'PC2', 'swamp3').
card_uid('swamp'/'PC2', 'PC2:Swamp:swamp3').
card_rarity('swamp'/'PC2', 'Basic Land').
card_artist('swamp'/'PC2', 'Jim Nelson').
card_number('swamp'/'PC2', '144').
card_multiverse_id('swamp'/'PC2', '276448').

card_in_set('swamp', 'PC2').
card_original_type('swamp'/'PC2', 'Basic Land — Swamp').
card_original_text('swamp'/'PC2', 'B').
card_image_name('swamp'/'PC2', 'swamp4').
card_uid('swamp'/'PC2', 'PC2:Swamp:swamp4').
card_rarity('swamp'/'PC2', 'Basic Land').
card_artist('swamp'/'PC2', 'Jim Nelson').
card_number('swamp'/'PC2', '145').
card_multiverse_id('swamp'/'PC2', '276455').

card_in_set('swamp', 'PC2').
card_original_type('swamp'/'PC2', 'Basic Land — Swamp').
card_original_text('swamp'/'PC2', 'B').
card_image_name('swamp'/'PC2', 'swamp5').
card_uid('swamp'/'PC2', 'PC2:Swamp:swamp5').
card_rarity('swamp'/'PC2', 'Basic Land').
card_artist('swamp'/'PC2', 'Jim Nelson').
card_number('swamp'/'PC2', '146').
card_multiverse_id('swamp'/'PC2', '276457').

card_in_set('tainted isle', 'PC2').
card_original_type('tainted isle'/'PC2', 'Land').
card_original_text('tainted isle'/'PC2', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Activate this ability only if you control a Swamp.').
card_image_name('tainted isle'/'PC2', 'tainted isle').
card_uid('tainted isle'/'PC2', 'PC2:Tainted Isle:tainted isle').
card_rarity('tainted isle'/'PC2', 'Uncommon').
card_artist('tainted isle'/'PC2', 'Alan Pollack').
card_number('tainted isle'/'PC2', '128').
card_multiverse_id('tainted isle'/'PC2', '271239').

card_in_set('takenuma', 'PC2').
card_original_type('takenuma'/'PC2', 'Plane — Kamigawa').
card_original_text('takenuma'/'PC2', 'Whenever a creature leaves the battlefield, its controller draws a card.\nWhenever you roll {C}, return target creature you control to its owner\'s hand.').
card_first_print('takenuma', 'PC2').
card_image_name('takenuma'/'PC2', 'takenuma').
card_uid('takenuma'/'PC2', 'PC2:Takenuma:takenuma').
card_rarity('takenuma'/'PC2', 'Common').
card_artist('takenuma'/'PC2', 'Cliff Childs').
card_number('takenuma'/'PC2', '35').
card_multiverse_id('takenuma'/'PC2', '226533').

card_in_set('talon gates', 'PC2').
card_original_type('talon gates'/'PC2', 'Plane — Dominaria').
card_original_text('talon gates'/'PC2', 'Any time you could cast a sorcery, you may exile a nonland card from your hand with X time counters on it, where X is its converted mana cost. If the exiled card doesn\'t have suspend, it gains suspend. (At the beginning of its owner\'s upkeep, he or she removes a time counter. When the last is removed, the player casts it without paying its mana cost. If it\'s a creature, it has haste.)\nWhenever you roll {C}, remove two time counters from each suspended card you own.').
card_first_print('talon gates', 'PC2').
card_image_name('talon gates'/'PC2', 'talon gates').
card_uid('talon gates'/'PC2', 'PC2:Talon Gates:talon gates').
card_rarity('talon gates'/'PC2', 'Common').
card_artist('talon gates'/'PC2', 'Chippy').
card_number('talon gates'/'PC2', '36').
card_multiverse_id('talon gates'/'PC2', '226525').

card_in_set('terramorphic expanse', 'PC2').
card_original_type('terramorphic expanse'/'PC2', 'Land').
card_original_text('terramorphic expanse'/'PC2', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'PC2', 'terramorphic expanse').
card_uid('terramorphic expanse'/'PC2', 'PC2:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'PC2', 'Common').
card_artist('terramorphic expanse'/'PC2', 'Dan Scott').
card_number('terramorphic expanse'/'PC2', '129').
card_multiverse_id('terramorphic expanse'/'PC2', '271196').

card_in_set('the zephyr maze', 'PC2').
card_original_type('the zephyr maze'/'PC2', 'Plane — Kyneth').
card_original_text('the zephyr maze'/'PC2', 'Creatures with flying get +2/+0.\nCreatures without flying get -2/-0.\nWhenever you roll {C}, target creature gains flying until end of turn.').
card_first_print('the zephyr maze', 'PC2').
card_image_name('the zephyr maze'/'PC2', 'the zephyr maze').
card_uid('the zephyr maze'/'PC2', 'PC2:The Zephyr Maze:the zephyr maze').
card_rarity('the zephyr maze'/'PC2', 'Common').
card_artist('the zephyr maze'/'PC2', 'rk post').
card_number('the zephyr maze'/'PC2', '40').
card_multiverse_id('the zephyr maze'/'PC2', '226528').

card_in_set('thorn-thrash viashino', 'PC2').
card_original_type('thorn-thrash viashino'/'PC2', 'Creature — Viashino Warrior').
card_original_text('thorn-thrash viashino'/'PC2', 'Devour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\n{G}: Thorn-Thrash Viashino gains trample until end of turn.').
card_image_name('thorn-thrash viashino'/'PC2', 'thorn-thrash viashino').
card_uid('thorn-thrash viashino'/'PC2', 'PC2:Thorn-Thrash Viashino:thorn-thrash viashino').
card_rarity('thorn-thrash viashino'/'PC2', 'Common').
card_artist('thorn-thrash viashino'/'PC2', 'Jon Foster').
card_number('thorn-thrash viashino'/'PC2', '52').
card_multiverse_id('thorn-thrash viashino'/'PC2', '271162').

card_in_set('thran golem', 'PC2').
card_original_type('thran golem'/'PC2', 'Artifact Creature — Golem').
card_original_text('thran golem'/'PC2', 'As long as Thran Golem is enchanted, it gets +2/+2 and has flying, first strike, and trample.').
card_image_name('thran golem'/'PC2', 'thran golem').
card_uid('thran golem'/'PC2', 'PC2:Thran Golem:thran golem').
card_rarity('thran golem'/'PC2', 'Uncommon').
card_artist('thran golem'/'PC2', 'Ron Spears').
card_number('thran golem'/'PC2', '114').
card_flavor_text('thran golem'/'PC2', 'The golems of the ancient Thran still draw power in ways that baffle the artificers of today.').
card_multiverse_id('thran golem'/'PC2', '271173').

card_in_set('three dreams', 'PC2').
card_original_type('three dreams'/'PC2', 'Sorcery').
card_original_text('three dreams'/'PC2', 'Search your library for up to three Aura cards with different names, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('three dreams'/'PC2', 'three dreams').
card_uid('three dreams'/'PC2', 'PC2:Three Dreams:three dreams').
card_rarity('three dreams'/'PC2', 'Rare').
card_artist('three dreams'/'PC2', 'Shishizaru').
card_number('three dreams'/'PC2', '13').
card_flavor_text('three dreams'/'PC2', '\"Choose one to heal, one to harm, and one to grant you the prudence to use them.\"\n—Miotri, auratouched mage').
card_multiverse_id('three dreams'/'PC2', '275260').

card_in_set('throat slitter', 'PC2').
card_original_type('throat slitter'/'PC2', 'Creature — Rat Ninja').
card_original_text('throat slitter'/'PC2', 'Ninjutsu {2}{B} ({2}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Throat Slitter deals combat damage to a player, destroy target nonblack creature that player controls.').
card_image_name('throat slitter'/'PC2', 'throat slitter').
card_uid('throat slitter'/'PC2', 'PC2:Throat Slitter:throat slitter').
card_rarity('throat slitter'/'PC2', 'Uncommon').
card_artist('throat slitter'/'PC2', 'Paolo Parente').
card_number('throat slitter'/'PC2', '37').
card_multiverse_id('throat slitter'/'PC2', '271187').

card_in_set('thromok the insatiable', 'PC2').
card_original_type('thromok the insatiable'/'PC2', 'Legendary Creature — Hellion').
card_original_text('thromok the insatiable'/'PC2', 'Devour X, where X is the number of creatures devoured this way (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with X +1/+1 counters on it for each of those creatures.)').
card_first_print('thromok the insatiable', 'PC2').
card_image_name('thromok the insatiable'/'PC2', 'thromok the insatiable').
card_uid('thromok the insatiable'/'PC2', 'PC2:Thromok the Insatiable:thromok the insatiable').
card_rarity('thromok the insatiable'/'PC2', 'Mythic Rare').
card_artist('thromok the insatiable'/'PC2', 'Terese Nielsen').
card_number('thromok the insatiable'/'PC2', '106').
card_multiverse_id('thromok the insatiable'/'PC2', '265155').

card_in_set('thunder-thrash elder', 'PC2').
card_original_type('thunder-thrash elder'/'PC2', 'Creature — Viashino Warrior').
card_original_text('thunder-thrash elder'/'PC2', 'Devour 3 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with three times that many +1/+1 counters on it.)').
card_image_name('thunder-thrash elder'/'PC2', 'thunder-thrash elder').
card_uid('thunder-thrash elder'/'PC2', 'PC2:Thunder-Thrash Elder:thunder-thrash elder').
card_rarity('thunder-thrash elder'/'PC2', 'Uncommon').
card_artist('thunder-thrash elder'/'PC2', 'Brandon Kitkouski').
card_number('thunder-thrash elder'/'PC2', '53').
card_flavor_text('thunder-thrash elder'/'PC2', 'Viashino thrashes are led by elders who have survived countless challenges.').
card_multiverse_id('thunder-thrash elder'/'PC2', '271163').

card_in_set('time distortion', 'PC2').
card_original_type('time distortion'/'PC2', 'Phenomenon').
card_original_text('time distortion'/'PC2', 'When you encounter Time Distortion, reverse the game\'s turn order. (For example, if play had proceeded clockwise around the table, it now goes counterclockwise. Then planeswalk away from this phenomenon.)').
card_first_print('time distortion', 'PC2').
card_image_name('time distortion'/'PC2', 'time distortion').
card_uid('time distortion'/'PC2', 'PC2:Time Distortion:time distortion').
card_rarity('time distortion'/'PC2', 'Common').
card_artist('time distortion'/'PC2', 'rk post').
card_number('time distortion'/'PC2', '8').
card_multiverse_id('time distortion'/'PC2', '226556').

card_in_set('tormented soul', 'PC2').
card_original_type('tormented soul'/'PC2', 'Creature — Spirit').
card_original_text('tormented soul'/'PC2', 'Tormented Soul can\'t block and is unblockable.').
card_image_name('tormented soul'/'PC2', 'tormented soul').
card_uid('tormented soul'/'PC2', 'PC2:Tormented Soul:tormented soul').
card_rarity('tormented soul'/'PC2', 'Common').
card_artist('tormented soul'/'PC2', 'Karl Kopinski').
card_number('tormented soul'/'PC2', '38').
card_flavor_text('tormented soul'/'PC2', 'Those who raged most bitterly at the world in life are cursed to roam the nether realms in death.').
card_multiverse_id('tormented soul'/'PC2', '271174').

card_in_set('trail of the mage-rings', 'PC2').
card_original_type('trail of the mage-rings'/'PC2', 'Plane — Vryn').
card_original_text('trail of the mage-rings'/'PC2', 'Instant and sorcery spells have rebound. (The spell\'s controller exiles the spell as it resolves if he or she cast it from his or her hand. At the beginning of that player\'s next upkeep, he or she may cast that card from exile without paying its mana cost.)\nWhenever you roll {C}, you may search your library for an instant or sorcery card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('trail of the mage-rings', 'PC2').
card_image_name('trail of the mage-rings'/'PC2', 'trail of the mage-rings').
card_uid('trail of the mage-rings'/'PC2', 'PC2:Trail of the Mage-Rings:trail of the mage-rings').
card_rarity('trail of the mage-rings'/'PC2', 'Common').
card_artist('trail of the mage-rings'/'PC2', 'Vincent Proce').
card_number('trail of the mage-rings'/'PC2', '37').
card_multiverse_id('trail of the mage-rings'/'PC2', '226529').

card_in_set('truga jungle', 'PC2').
card_original_type('truga jungle'/'PC2', 'Plane — Ergamon').
card_original_text('truga jungle'/'PC2', 'All lands have \"{T}: Add one mana of any color to your mana pool.\"\nWhenever you roll {C}, reveal the top three cards of your library. Put all land cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_first_print('truga jungle', 'PC2').
card_image_name('truga jungle'/'PC2', 'truga jungle').
card_uid('truga jungle'/'PC2', 'PC2:Truga Jungle:truga jungle').
card_rarity('truga jungle'/'PC2', 'Common').
card_artist('truga jungle'/'PC2', 'Jim Nelson').
card_number('truga jungle'/'PC2', '38').
card_multiverse_id('truga jungle'/'PC2', '226553').

card_in_set('tukatongue thallid', 'PC2').
card_original_type('tukatongue thallid'/'PC2', 'Creature — Fungus').
card_original_text('tukatongue thallid'/'PC2', 'When Tukatongue Thallid dies, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('tukatongue thallid'/'PC2', 'tukatongue thallid').
card_uid('tukatongue thallid'/'PC2', 'PC2:Tukatongue Thallid:tukatongue thallid').
card_rarity('tukatongue thallid'/'PC2', 'Common').
card_artist('tukatongue thallid'/'PC2', 'Vance Kovacs').
card_number('tukatongue thallid'/'PC2', '79').
card_flavor_text('tukatongue thallid'/'PC2', 'Jund\'s thallids tried to disguise their deliciousness by covering themselves in spines harvested from the tukatongue tree.').
card_multiverse_id('tukatongue thallid'/'PC2', '271143').

card_in_set('vela the night-clad', 'PC2').
card_original_type('vela the night-clad'/'PC2', 'Legendary Creature — Human Wizard').
card_original_text('vela the night-clad'/'PC2', 'Intimidate\nOther creatures you control have intimidate.\nWhenever Vela the Night-Clad or another creature you control leaves the battlefield, each opponent loses 1 life.').
card_first_print('vela the night-clad', 'PC2').
card_image_name('vela the night-clad'/'PC2', 'vela the night-clad').
card_uid('vela the night-clad'/'PC2', 'PC2:Vela the Night-Clad:vela the night-clad').
card_rarity('vela the night-clad'/'PC2', 'Mythic Rare').
card_artist('vela the night-clad'/'PC2', 'Allen Williams').
card_number('vela the night-clad'/'PC2', '107').
card_flavor_text('vela the night-clad'/'PC2', 'Vela snuffs out every trace of Krond\'s reign, leaving pure nightfall in her wake.').
card_multiverse_id('vela the night-clad'/'PC2', '270729').

card_in_set('viridian emissary', 'PC2').
card_original_type('viridian emissary'/'PC2', 'Creature — Elf Scout').
card_original_text('viridian emissary'/'PC2', 'When Viridian Emissary dies, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_image_name('viridian emissary'/'PC2', 'viridian emissary').
card_uid('viridian emissary'/'PC2', 'PC2:Viridian Emissary:viridian emissary').
card_rarity('viridian emissary'/'PC2', 'Common').
card_artist('viridian emissary'/'PC2', 'Matt Stewart').
card_number('viridian emissary'/'PC2', '80').
card_multiverse_id('viridian emissary'/'PC2', '271146').

card_in_set('vitu-ghazi, the city-tree', 'PC2').
card_original_type('vitu-ghazi, the city-tree'/'PC2', 'Land').
card_original_text('vitu-ghazi, the city-tree'/'PC2', '{T}: Add {1} to your mana pool.\n{2}{G}{W}, {T}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('vitu-ghazi, the city-tree'/'PC2', 'vitu-ghazi, the city-tree').
card_uid('vitu-ghazi, the city-tree'/'PC2', 'PC2:Vitu-Ghazi, the City-Tree:vitu-ghazi, the city-tree').
card_rarity('vitu-ghazi, the city-tree'/'PC2', 'Uncommon').
card_artist('vitu-ghazi, the city-tree'/'PC2', 'Martina Pilcerova').
card_number('vitu-ghazi, the city-tree'/'PC2', '130').
card_flavor_text('vitu-ghazi, the city-tree'/'PC2', 'In the autumn, she casts her seeds across the streets below, and come spring, her children rise in service to the Conclave.').
card_multiverse_id('vitu-ghazi, the city-tree'/'PC2', '271213').

card_in_set('vivid creek', 'PC2').
card_original_type('vivid creek'/'PC2', 'Land').
card_original_text('vivid creek'/'PC2', 'Vivid Creek enters the battlefield tapped with two charge counters on it.\n{T}: Add {U} to your mana pool.\n{T}, Remove a charge counter from Vivid Creek: Add one mana of any color to your mana pool.').
card_image_name('vivid creek'/'PC2', 'vivid creek').
card_uid('vivid creek'/'PC2', 'PC2:Vivid Creek:vivid creek').
card_rarity('vivid creek'/'PC2', 'Uncommon').
card_artist('vivid creek'/'PC2', 'Fred Fields').
card_number('vivid creek'/'PC2', '131').
card_multiverse_id('vivid creek'/'PC2', '271204').

card_in_set('walker of secret ways', 'PC2').
card_original_type('walker of secret ways'/'PC2', 'Creature — Human Ninja').
card_original_text('walker of secret ways'/'PC2', 'Ninjutsu {1}{U} ({1}{U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Walker of Secret Ways deals combat damage to a player, look at that player\'s hand.\n{1}{U}: Return target Ninja you control to its owner\'s hand. Activate this ability only during your turn.').
card_image_name('walker of secret ways'/'PC2', 'walker of secret ways').
card_uid('walker of secret ways'/'PC2', 'PC2:Walker of Secret Ways:walker of secret ways').
card_rarity('walker of secret ways'/'PC2', 'Uncommon').
card_artist('walker of secret ways'/'PC2', 'Scott M. Fischer').
card_number('walker of secret ways'/'PC2', '27').
card_multiverse_id('walker of secret ways'/'PC2', '271188').

card_in_set('wall of blossoms', 'PC2').
card_original_type('wall of blossoms'/'PC2', 'Creature — Plant Wall').
card_original_text('wall of blossoms'/'PC2', 'Defender\nWhen Wall of Blossoms enters the battlefield, draw a card.').
card_image_name('wall of blossoms'/'PC2', 'wall of blossoms').
card_uid('wall of blossoms'/'PC2', 'PC2:Wall of Blossoms:wall of blossoms').
card_rarity('wall of blossoms'/'PC2', 'Uncommon').
card_artist('wall of blossoms'/'PC2', 'Heather Hudson').
card_number('wall of blossoms'/'PC2', '81').
card_flavor_text('wall of blossoms'/'PC2', 'Each flower identical, every leaf and petal disturbingly exact.').
card_multiverse_id('wall of blossoms'/'PC2', '275262').

card_in_set('wall of frost', 'PC2').
card_original_type('wall of frost'/'PC2', 'Creature — Wall').
card_original_text('wall of frost'/'PC2', 'Defender\nWhenever Wall of Frost blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('wall of frost'/'PC2', 'wall of frost').
card_uid('wall of frost'/'PC2', 'PC2:Wall of Frost:wall of frost').
card_rarity('wall of frost'/'PC2', 'Uncommon').
card_artist('wall of frost'/'PC2', 'Mike Bierek').
card_number('wall of frost'/'PC2', '28').
card_flavor_text('wall of frost'/'PC2', '\"I welcome it. The pasture is cleared of weeds and the wolves are frozen solid.\"\n—Lumi, goatherd').
card_multiverse_id('wall of frost'/'PC2', '271197').

card_in_set('warstorm surge', 'PC2').
card_original_type('warstorm surge'/'PC2', 'Enchantment').
card_original_text('warstorm surge'/'PC2', 'Whenever a creature enters the battlefield under your control, it deals damage equal to its power to target creature or player.').
card_image_name('warstorm surge'/'PC2', 'warstorm surge').
card_uid('warstorm surge'/'PC2', 'PC2:Warstorm Surge:warstorm surge').
card_rarity('warstorm surge'/'PC2', 'Rare').
card_artist('warstorm surge'/'PC2', 'Raymond Swanland').
card_number('warstorm surge'/'PC2', '54').
card_flavor_text('warstorm surge'/'PC2', '\"Listen to the roar! Feel the thunder! The Immersturm shouts its approval with every bolt of lightning!\"').
card_multiverse_id('warstorm surge'/'PC2', '271226').

card_in_set('whirlpool warrior', 'PC2').
card_original_type('whirlpool warrior'/'PC2', 'Creature — Merfolk Warrior').
card_original_text('whirlpool warrior'/'PC2', 'When Whirlpool Warrior enters the battlefield, shuffle the cards from your hand into your library, then draw that many cards.\n{R}, Sacrifice Whirlpool Warrior: Each player shuffles the cards from his or her hand into his or her library, then draws that many cards.').
card_image_name('whirlpool warrior'/'PC2', 'whirlpool warrior').
card_uid('whirlpool warrior'/'PC2', 'PC2:Whirlpool Warrior:whirlpool warrior').
card_rarity('whirlpool warrior'/'PC2', 'Rare').
card_artist('whirlpool warrior'/'PC2', 'Kev Walker').
card_number('whirlpool warrior'/'PC2', '29').
card_multiverse_id('whirlpool warrior'/'PC2', '271165').

card_in_set('whispersilk cloak', 'PC2').
card_original_type('whispersilk cloak'/'PC2', 'Artifact — Equipment').
card_original_text('whispersilk cloak'/'PC2', 'Equipped creature is unblockable and has shroud.\nEquip {2}').
card_image_name('whispersilk cloak'/'PC2', 'whispersilk cloak').
card_uid('whispersilk cloak'/'PC2', 'PC2:Whispersilk Cloak:whispersilk cloak').
card_rarity('whispersilk cloak'/'PC2', 'Uncommon').
card_artist('whispersilk cloak'/'PC2', 'Daren Bader').
card_number('whispersilk cloak'/'PC2', '115').
card_flavor_text('whispersilk cloak'/'PC2', 'Such cloaks are in high demand both by assassins and by those who fear them.').
card_multiverse_id('whispersilk cloak'/'PC2', '271198').

card_in_set('windriddle palaces', 'PC2').
card_original_type('windriddle palaces'/'PC2', 'Plane — Belenon').
card_original_text('windriddle palaces'/'PC2', 'Players play with the top card of their libraries revealed.\nYou may play the top card of any player\'s library.\nWhenever you roll {C}, each player puts the top card of his or her library into his or her graveyard.').
card_first_print('windriddle palaces', 'PC2').
card_image_name('windriddle palaces'/'PC2', 'windriddle palaces').
card_uid('windriddle palaces'/'PC2', 'PC2:Windriddle Palaces:windriddle palaces').
card_rarity('windriddle palaces'/'PC2', 'Common').
card_artist('windriddle palaces'/'PC2', 'Kekai Kotaki').
card_number('windriddle palaces'/'PC2', '39').
card_multiverse_id('windriddle palaces'/'PC2', '226510').
