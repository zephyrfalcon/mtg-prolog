% Ninth Edition

set('9ED').
set_name('9ED', 'Ninth Edition').
set_release_date('9ED', '2005-07-29').
set_border('9ED', 'white').
set_type('9ED', 'core').

card_in_set('adarkar wastes', '9ED').
card_original_type('adarkar wastes'/'9ED', 'Land').
card_original_text('adarkar wastes'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Adarkar Wastes deals 1 damage to you.').
card_image_name('adarkar wastes'/'9ED', 'adarkar wastes').
card_uid('adarkar wastes'/'9ED', '9ED:Adarkar Wastes:adarkar wastes').
card_rarity('adarkar wastes'/'9ED', 'Rare').
card_artist('adarkar wastes'/'9ED', 'John Avon').
card_number('adarkar wastes'/'9ED', '317').
card_multiverse_id('adarkar wastes'/'9ED', '84635').

card_in_set('air elemental', '9ED').
card_original_type('air elemental'/'9ED', 'Creature — Elemental').
card_original_text('air elemental'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('air elemental'/'9ED', 'air elemental').
card_uid('air elemental'/'9ED', '9ED:Air Elemental:air elemental').
card_rarity('air elemental'/'9ED', 'Uncommon').
card_artist('air elemental'/'9ED', 'Nick Percival').
card_number('air elemental'/'9ED', '58').
card_flavor_text('air elemental'/'9ED', 'Pray that it doesn\'t seek the safety of your lungs.').
card_multiverse_id('air elemental'/'9ED', '82988').

card_in_set('aladdin\'s ring', '9ED').
card_original_type('aladdin\'s ring'/'9ED', 'Artifact').
card_original_text('aladdin\'s ring'/'9ED', '{8}, {T}: Aladdin\'s Ring deals 4 damage to target creature or player.').
card_image_name('aladdin\'s ring'/'9ED', 'aladdin\'s ring').
card_uid('aladdin\'s ring'/'9ED', '9ED:Aladdin\'s Ring:aladdin\'s ring').
card_rarity('aladdin\'s ring'/'9ED', 'Rare').
card_artist('aladdin\'s ring'/'9ED', 'Dave Dorman').
card_number('aladdin\'s ring'/'9ED', '286').
card_flavor_text('aladdin\'s ring'/'9ED', '\"A good lamp will light your way. A good ring will clear it.\"\n—Nervan, royal jeweler').
card_multiverse_id('aladdin\'s ring'/'9ED', '82989').

card_in_set('anaba shaman', '9ED').
card_original_type('anaba shaman'/'9ED', 'Creature — Minotaur Shaman').
card_original_text('anaba shaman'/'9ED', '{R}, {T}: Anaba Shaman deals 1 damage to target creature or player.').
card_image_name('anaba shaman'/'9ED', 'anaba shaman').
card_uid('anaba shaman'/'9ED', '9ED:Anaba Shaman:anaba shaman').
card_rarity('anaba shaman'/'9ED', 'Common').
card_artist('anaba shaman'/'9ED', 'Simon Bisley').
card_number('anaba shaman'/'9ED', '172').
card_flavor_text('anaba shaman'/'9ED', 'Just try taking this bull by the horns.').
card_multiverse_id('anaba shaman'/'9ED', '82991').

card_in_set('anaconda', '9ED').
card_original_type('anaconda'/'9ED', 'Creature — Snake').
card_original_text('anaconda'/'9ED', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('anaconda'/'9ED', 'anaconda').
card_uid('anaconda'/'9ED', '9ED:Anaconda:anaconda').
card_rarity('anaconda'/'9ED', 'Uncommon').
card_artist('anaconda'/'9ED', 'Andrew Robinson').
card_number('anaconda'/'9ED', '229').
card_flavor_text('anaconda'/'9ED', 'If you\'re smaller than the anaconda, it considers you food. If you\'re larger than the anaconda, it considers you a lot of food.').
card_multiverse_id('anaconda'/'9ED', '83276').

card_in_set('anarchist', '9ED').
card_original_type('anarchist'/'9ED', 'Creature — Human Wizard').
card_original_text('anarchist'/'9ED', 'When Anarchist comes into play, you may return target sorcery card from your graveyard to your hand.').
card_image_name('anarchist'/'9ED', 'anarchist').
card_uid('anarchist'/'9ED', '9ED:Anarchist:anarchist').
card_rarity('anarchist'/'9ED', 'Uncommon').
card_artist('anarchist'/'9ED', 'Brom').
card_number('anarchist'/'9ED', '173').
card_flavor_text('anarchist'/'9ED', 'Those who will not follow are doomed to lead.').
card_multiverse_id('anarchist'/'9ED', '84072').

card_in_set('ancient silverback', '9ED').
card_original_type('ancient silverback'/'9ED', 'Creature — Ape').
card_original_text('ancient silverback'/'9ED', '{G}: Regenerate Ancient Silverback. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('ancient silverback'/'9ED', 'ancient silverback').
card_uid('ancient silverback'/'9ED', '9ED:Ancient Silverback:ancient silverback').
card_rarity('ancient silverback'/'9ED', 'Rare').
card_artist('ancient silverback'/'9ED', 'Scott M. Fischer').
card_number('ancient silverback'/'9ED', '230').
card_multiverse_id('ancient silverback'/'9ED', '82950').

card_in_set('angel of mercy', '9ED').
card_original_type('angel of mercy'/'9ED', 'Creature — Angel').
card_original_text('angel of mercy'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Angel of Mercy comes into play, you gain 3 life.').
card_image_name('angel of mercy'/'9ED', 'angel of mercy').
card_uid('angel of mercy'/'9ED', '9ED:Angel of Mercy:angel of mercy').
card_rarity('angel of mercy'/'9ED', 'Uncommon').
card_artist('angel of mercy'/'9ED', 'Melissa A. Benson').
card_number('angel of mercy'/'9ED', '1').
card_multiverse_id('angel of mercy'/'9ED', '82992').

card_in_set('angel\'s feather', '9ED').
card_original_type('angel\'s feather'/'9ED', 'Artifact').
card_original_text('angel\'s feather'/'9ED', 'Whenever a player plays a white spell, you may gain 1 life.').
card_image_name('angel\'s feather'/'9ED', 'angel\'s feather').
card_uid('angel\'s feather'/'9ED', '9ED:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'9ED', 'Uncommon').
card_artist('angel\'s feather'/'9ED', 'Alan Pollack').
card_number('angel\'s feather'/'9ED', '287').
card_flavor_text('angel\'s feather'/'9ED', 'If taken, it cuts the hand that clutches it. If given, it heals the hand that holds it.').
card_multiverse_id('angel\'s feather'/'9ED', '83449').

card_in_set('angelic blessing', '9ED').
card_original_type('angelic blessing'/'9ED', 'Sorcery').
card_original_text('angelic blessing'/'9ED', 'Target creature gets +3/+3 and gains flying until end of turn. (It can\'t be blocked except by creatures with flying.)').
card_image_name('angelic blessing'/'9ED', 'angelic blessing').
card_uid('angelic blessing'/'9ED', '9ED:Angelic Blessing:angelic blessing').
card_rarity('angelic blessing'/'9ED', 'Common').
card_artist('angelic blessing'/'9ED', 'Mark Zug').
card_number('angelic blessing'/'9ED', '2').
card_flavor_text('angelic blessing'/'9ED', 'Only the warrior who can admit mortal weakness will be bolstered by immortal strength.').
card_multiverse_id('angelic blessing'/'9ED', '84583').

card_in_set('annex', '9ED').
card_original_type('annex'/'9ED', 'Enchantment — Aura').
card_original_text('annex'/'9ED', 'Enchant land (Target a land as you play this. This card comes into play attached to that land.)\nYou control enchanted land.').
card_image_name('annex'/'9ED', 'annex').
card_uid('annex'/'9ED', '9ED:Annex:annex').
card_rarity('annex'/'9ED', 'Uncommon').
card_artist('annex'/'9ED', 'John Avon').
card_number('annex'/'9ED', '59').
card_flavor_text('annex'/'9ED', 'You should see how small the map is.').
card_multiverse_id('annex'/'9ED', '84564').

card_in_set('archivist', '9ED').
card_original_type('archivist'/'9ED', 'Creature — Human Wizard').
card_original_text('archivist'/'9ED', '{T}: Draw a card.').
card_image_name('archivist'/'9ED', 'archivist').
card_uid('archivist'/'9ED', '9ED:Archivist:archivist').
card_rarity('archivist'/'9ED', 'Rare').
card_artist('archivist'/'9ED', 'Donato Giancola').
card_number('archivist'/'9ED', '60').
card_flavor_text('archivist'/'9ED', '\"Sit down and read. Educate yourself for the coming conflicts.\"\n—Mary Harris \"Mother\" Jones').
card_multiverse_id('archivist'/'9ED', '82994').

card_in_set('aven cloudchaser', '9ED').
card_original_type('aven cloudchaser'/'9ED', 'Creature — Bird Soldier').
card_original_text('aven cloudchaser'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Aven Cloudchaser comes into play, destroy target enchantment.').
card_image_name('aven cloudchaser'/'9ED', 'aven cloudchaser').
card_uid('aven cloudchaser'/'9ED', '9ED:Aven Cloudchaser:aven cloudchaser').
card_rarity('aven cloudchaser'/'9ED', 'Common').
card_artist('aven cloudchaser'/'9ED', 'Justin Sweet').
card_number('aven cloudchaser'/'9ED', '3').
card_multiverse_id('aven cloudchaser'/'9ED', '84522').

card_in_set('aven fisher', '9ED').
card_original_type('aven fisher'/'9ED', 'Creature — Bird Soldier').
card_original_text('aven fisher'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Aven Fisher is put into a graveyard from play, you may draw a card.').
card_image_name('aven fisher'/'9ED', 'aven fisher').
card_uid('aven fisher'/'9ED', '9ED:Aven Fisher:aven fisher').
card_rarity('aven fisher'/'9ED', 'Common').
card_artist('aven fisher'/'9ED', 'Christopher Moeller').
card_number('aven fisher'/'9ED', '61').
card_flavor_text('aven fisher'/'9ED', 'The same spears that catch their food today will defend their homes tomorrow.').
card_multiverse_id('aven fisher'/'9ED', '82998').

card_in_set('aven flock', '9ED').
card_original_type('aven flock'/'9ED', 'Creature — Bird Soldier').
card_original_text('aven flock'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{W}: Aven Flock gets +0/+1 until end of turn.').
card_image_name('aven flock'/'9ED', 'aven flock').
card_uid('aven flock'/'9ED', '9ED:Aven Flock:aven flock').
card_rarity('aven flock'/'9ED', 'Common').
card_artist('aven flock'/'9ED', 'Greg & Tim Hildebrandt').
card_number('aven flock'/'9ED', '4').
card_flavor_text('aven flock'/'9ED', 'Just as each added feather steadies the wing, so does the flock grow stronger with each new member.').
card_multiverse_id('aven flock'/'9ED', '82999').

card_in_set('aven windreader', '9ED').
card_original_type('aven windreader'/'9ED', 'Creature — Bird Soldier Wizard').
card_original_text('aven windreader'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{1}{U}: Target player reveals the top card of his or her library.').
card_image_name('aven windreader'/'9ED', 'aven windreader').
card_uid('aven windreader'/'9ED', '9ED:Aven Windreader:aven windreader').
card_rarity('aven windreader'/'9ED', 'Common').
card_artist('aven windreader'/'9ED', 'Greg & Tim Hildebrandt').
card_number('aven windreader'/'9ED', '62').
card_flavor_text('aven windreader'/'9ED', '\"The tiniest ripple tells a story ten fathoms deep.\"').
card_multiverse_id('aven windreader'/'9ED', '84071').

card_in_set('azure drake', '9ED').
card_original_type('azure drake'/'9ED', 'Creature — Drake').
card_original_text('azure drake'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('azure drake'/'9ED', 'azure drake').
card_uid('azure drake'/'9ED', '9ED:Azure Drake:azure drake').
card_rarity('azure drake'/'9ED', 'Uncommon').
card_artist('azure drake'/'9ED', 'Janine Johnston').
card_number('azure drake'/'9ED', '63').
card_flavor_text('azure drake'/'9ED', 'Little dreamt could seem so cruel\nAs waiting for the wings outspread,\nThe jagged teeth, the burning eyes,\nAnd dagger-claws that clench to nerves.').
card_multiverse_id('azure drake'/'9ED', '83084').

card_in_set('balduvian barbarians', '9ED').
card_original_type('balduvian barbarians'/'9ED', 'Creature — Human Barbarian').
card_original_text('balduvian barbarians'/'9ED', '').
card_image_name('balduvian barbarians'/'9ED', 'balduvian barbarians').
card_uid('balduvian barbarians'/'9ED', '9ED:Balduvian Barbarians:balduvian barbarians').
card_rarity('balduvian barbarians'/'9ED', 'Common').
card_artist('balduvian barbarians'/'9ED', 'Jim Nelson').
card_number('balduvian barbarians'/'9ED', '174').
card_flavor_text('balduvian barbarians'/'9ED', '\"From the snowy slopes of Kaelor,\nTo the canyons of Bandu,\nWe drink and fight and feast and die\nAs we were born to do.\"\n—Balduvian tavern song').
card_multiverse_id('balduvian barbarians'/'9ED', '83001').

card_in_set('baleful stare', '9ED').
card_original_type('baleful stare'/'9ED', 'Sorcery').
card_original_text('baleful stare'/'9ED', 'Target opponent reveals his or her hand. You draw a card for each Mountain and red card in it.').
card_image_name('baleful stare'/'9ED', 'baleful stare').
card_uid('baleful stare'/'9ED', '9ED:Baleful Stare:baleful stare').
card_rarity('baleful stare'/'9ED', 'Uncommon').
card_artist('baleful stare'/'9ED', 'Randy Gallegos').
card_number('baleful stare'/'9ED', '64').
card_flavor_text('baleful stare'/'9ED', '\"Though there is little to be learned from your dust and stones, I shall wring them dry nevertheless.\"').
card_multiverse_id('baleful stare'/'9ED', '84424').

card_in_set('ballista squad', '9ED').
card_original_type('ballista squad'/'9ED', 'Creature — Human Rebel').
card_original_text('ballista squad'/'9ED', '{X}{W}, {T}: Ballista Squad deals X damage to target attacking or blocking creature.').
card_image_name('ballista squad'/'9ED', 'ballista squad').
card_uid('ballista squad'/'9ED', '9ED:Ballista Squad:ballista squad').
card_rarity('ballista squad'/'9ED', 'Uncommon').
card_artist('ballista squad'/'9ED', 'Matthew D. Wilson').
card_number('ballista squad'/'9ED', '5').
card_flavor_text('ballista squad'/'9ED', 'The perfect antidote for a tightly packed formation.').
card_multiverse_id('ballista squad'/'9ED', '84508').

card_in_set('battle of wits', '9ED').
card_original_type('battle of wits'/'9ED', 'Enchantment').
card_original_text('battle of wits'/'9ED', 'At the beginning of your upkeep, if you have 200 or more cards in your library, you win the game.').
card_image_name('battle of wits'/'9ED', 'battle of wits').
card_uid('battle of wits'/'9ED', '9ED:Battle of Wits:battle of wits').
card_rarity('battle of wits'/'9ED', 'Rare').
card_artist('battle of wits'/'9ED', 'Edward P. Beard, Jr.').
card_number('battle of wits'/'9ED', '65').
card_flavor_text('battle of wits'/'9ED', 'The wizard who reads a thousand books is powerful. The wizard who memorizes a thousand books is insane.').
card_multiverse_id('battle of wits'/'9ED', '83133').

card_in_set('battlefield forge', '9ED').
card_original_type('battlefield forge'/'9ED', 'Land').
card_original_text('battlefield forge'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {W} to your mana pool. Battlefield Forge deals 1 damage to you.').
card_image_name('battlefield forge'/'9ED', 'battlefield forge').
card_uid('battlefield forge'/'9ED', '9ED:Battlefield Forge:battlefield forge').
card_rarity('battlefield forge'/'9ED', 'Rare').
card_artist('battlefield forge'/'9ED', 'Darrell Riche').
card_number('battlefield forge'/'9ED', '318').
card_multiverse_id('battlefield forge'/'9ED', '84443').

card_in_set('beast of burden', '9ED').
card_original_type('beast of burden'/'9ED', 'Artifact Creature — Golem').
card_original_text('beast of burden'/'9ED', 'Beast of Burden\'s power and toughness are each equal to the number of creatures in play.').
card_image_name('beast of burden'/'9ED', 'beast of burden').
card_uid('beast of burden'/'9ED', '9ED:Beast of Burden:beast of burden').
card_rarity('beast of burden'/'9ED', 'Rare').
card_artist('beast of burden'/'9ED', 'Chippy').
card_number('beast of burden'/'9ED', '288').
card_flavor_text('beast of burden'/'9ED', 'Though it was built as an instrument of labor, it wasn\'t long before it became an instrument of war.').
card_multiverse_id('beast of burden'/'9ED', '83002').

card_in_set('biorhythm', '9ED').
card_original_type('biorhythm'/'9ED', 'Sorcery').
card_original_text('biorhythm'/'9ED', 'Each player\'s life total becomes the number of creatures he or she controls.').
card_image_name('biorhythm'/'9ED', 'biorhythm').
card_uid('biorhythm'/'9ED', '9ED:Biorhythm:biorhythm').
card_rarity('biorhythm'/'9ED', 'Rare').
card_artist('biorhythm'/'9ED', 'Ron Spears').
card_number('biorhythm'/'9ED', '231').
card_flavor_text('biorhythm'/'9ED', '\"I have seen life\'s purpose, and now it is my own.\"\n—Kamahl, druid acolyte').
card_multiverse_id('biorhythm'/'9ED', '83531').

card_in_set('blackmail', '9ED').
card_original_type('blackmail'/'9ED', 'Sorcery').
card_original_text('blackmail'/'9ED', 'Target player reveals three cards from his or her hand and you choose one of them. That player discards that card.').
card_image_name('blackmail'/'9ED', 'blackmail').
card_uid('blackmail'/'9ED', '9ED:Blackmail:blackmail').
card_rarity('blackmail'/'9ED', 'Uncommon').
card_artist('blackmail'/'9ED', 'Christopher Moeller').
card_number('blackmail'/'9ED', '115').
card_flavor_text('blackmail'/'9ED', 'In addition to killing peasants, punishing subordinates, and raising an army of nightmares, Braids somehow found time for her favorite hobby: petty extortion.').
card_multiverse_id('blackmail'/'9ED', '83471').

card_in_set('blanchwood armor', '9ED').
card_original_type('blanchwood armor'/'9ED', 'Enchantment — Aura').
card_original_text('blanchwood armor'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +1/+1 for each Forest you control.').
card_image_name('blanchwood armor'/'9ED', 'blanchwood armor').
card_uid('blanchwood armor'/'9ED', '9ED:Blanchwood Armor:blanchwood armor').
card_rarity('blanchwood armor'/'9ED', 'Uncommon').
card_artist('blanchwood armor'/'9ED', 'Paolo Parente').
card_number('blanchwood armor'/'9ED', '232').
card_multiverse_id('blanchwood armor'/'9ED', '83004').

card_in_set('blaze', '9ED').
card_original_type('blaze'/'9ED', 'Sorcery').
card_original_text('blaze'/'9ED', 'Blaze deals X damage to target creature or player.').
card_image_name('blaze'/'9ED', 'blaze').
card_uid('blaze'/'9ED', '9ED:Blaze:blaze').
card_rarity('blaze'/'9ED', 'Uncommon').
card_artist('blaze'/'9ED', 'Alex Horley-Orlandelli').
card_number('blaze'/'9ED', '175').
card_flavor_text('blaze'/'9ED', 'Fire never dies alone.').
card_multiverse_id('blaze'/'9ED', '83005').

card_in_set('blessed orator', '9ED').
card_original_type('blessed orator'/'9ED', 'Creature — Human Cleric').
card_original_text('blessed orator'/'9ED', 'Other creatures you control get +0/+1.').
card_image_name('blessed orator'/'9ED', 'blessed orator').
card_uid('blessed orator'/'9ED', '9ED:Blessed Orator:blessed orator').
card_rarity('blessed orator'/'9ED', 'Uncommon').
card_artist('blessed orator'/'9ED', 'Terese Nielsen').
card_number('blessed orator'/'9ED', '6').
card_flavor_text('blessed orator'/'9ED', '\"He spoke with the tongue of Angels; we fought with the strength of lions.\"\n—Handel, infantry soldier').
card_multiverse_id('blessed orator'/'9ED', '84057').

card_in_set('blinding angel', '9ED').
card_original_type('blinding angel'/'9ED', 'Creature — Angel').
card_original_text('blinding angel'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever Blinding Angel deals combat damage to a player, that player skips his or her next combat phase.').
card_image_name('blinding angel'/'9ED', 'blinding angel').
card_uid('blinding angel'/'9ED', '9ED:Blinding Angel:blinding angel').
card_rarity('blinding angel'/'9ED', 'Rare').
card_artist('blinding angel'/'9ED', 'Todd Lockwood').
card_number('blinding angel'/'9ED', '7').
card_flavor_text('blinding angel'/'9ED', '\"Their eyes will shrivel and blacken before faith\'s true light.\"').
card_multiverse_id('blinding angel'/'9ED', '83007').

card_in_set('blinking spirit', '9ED').
card_original_type('blinking spirit'/'9ED', 'Creature — Spirit').
card_original_text('blinking spirit'/'9ED', '{0}: Return Blinking Spirit to its owner\'s hand.').
card_image_name('blinking spirit'/'9ED', 'blinking spirit').
card_uid('blinking spirit'/'9ED', '9ED:Blinking Spirit:blinking spirit').
card_rarity('blinking spirit'/'9ED', 'Rare').
card_artist('blinking spirit'/'9ED', 'L. A. Williams').
card_number('blinking spirit'/'9ED', '8').
card_flavor_text('blinking spirit'/'9ED', '\"Don\'t look at it! Maybe it\'ll go away!\"\n—Ib Halfheart, goblin tactician').
card_multiverse_id('blinking spirit'/'9ED', '83962').

card_in_set('blood moon', '9ED').
card_original_type('blood moon'/'9ED', 'Enchantment').
card_original_text('blood moon'/'9ED', 'Nonbasic lands are Mountains.').
card_image_name('blood moon'/'9ED', 'blood moon').
card_uid('blood moon'/'9ED', '9ED:Blood Moon:blood moon').
card_rarity('blood moon'/'9ED', 'Rare').
card_artist('blood moon'/'9ED', 'Franz Vohwinkel').
card_number('blood moon'/'9ED', '176').
card_flavor_text('blood moon'/'9ED', 'Heavy light flooded across the landscape, cloaking everything in deep crimson.').
card_multiverse_id('blood moon'/'9ED', '83008').

card_in_set('bloodfire colossus', '9ED').
card_original_type('bloodfire colossus'/'9ED', 'Creature — Giant').
card_original_text('bloodfire colossus'/'9ED', '{R}, Sacrifice Bloodfire Colossus: Bloodfire Colossus deals 6 damage to each creature and each player.').
card_image_name('bloodfire colossus'/'9ED', 'bloodfire colossus').
card_uid('bloodfire colossus'/'9ED', '9ED:Bloodfire Colossus:bloodfire colossus').
card_rarity('bloodfire colossus'/'9ED', 'Rare').
card_artist('bloodfire colossus'/'9ED', 'Greg Staples').
card_number('bloodfire colossus'/'9ED', '177').
card_flavor_text('bloodfire colossus'/'9ED', 'It took all its strength to contain the fire within.').
card_multiverse_id('bloodfire colossus'/'9ED', '84456').

card_in_set('bog imp', '9ED').
card_original_type('bog imp'/'9ED', 'Creature — Imp').
card_original_text('bog imp'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('bog imp'/'9ED', 'bog imp').
card_uid('bog imp'/'9ED', '9ED:Bog Imp:bog imp').
card_rarity('bog imp'/'9ED', 'Common').
card_artist('bog imp'/'9ED', 'Carl Critchlow').
card_number('bog imp'/'9ED', '116').
card_flavor_text('bog imp'/'9ED', 'Think of it as a butcher knife with wings.').
card_multiverse_id('bog imp'/'9ED', '83010').

card_in_set('bog wraith', '9ED').
card_original_type('bog wraith'/'9ED', 'Creature — Wraith').
card_original_text('bog wraith'/'9ED', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('bog wraith'/'9ED', 'bog wraith').
card_uid('bog wraith'/'9ED', '9ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'9ED', 'Uncommon').
card_artist('bog wraith'/'9ED', 'Ted Naifeh').
card_number('bog wraith'/'9ED', '117').
card_flavor_text('bog wraith'/'9ED', 'The bodies of its victims are the only tracks it leaves.').
card_multiverse_id('bog wraith'/'9ED', '83011').

card_in_set('boiling seas', '9ED').
card_original_type('boiling seas'/'9ED', 'Sorcery').
card_original_text('boiling seas'/'9ED', 'Destroy all Islands.').
card_image_name('boiling seas'/'9ED', 'boiling seas').
card_uid('boiling seas'/'9ED', '9ED:Boiling Seas:boiling seas').
card_rarity('boiling seas'/'9ED', 'Uncommon').
card_artist('boiling seas'/'9ED', 'Tom Wänerstrand').
card_number('boiling seas'/'9ED', '178').
card_flavor_text('boiling seas'/'9ED', 'What burns the land, boils the seas.').
card_multiverse_id('boiling seas'/'9ED', '83012').

card_in_set('booby trap', '9ED').
card_original_type('booby trap'/'9ED', 'Artifact').
card_original_text('booby trap'/'9ED', 'As Booby Trap comes into play, name a card other than a basic land card and choose an opponent.\nThe chosen player reveals each card he or she draws.\nWhen the chosen player draws the named card, sacrifice Booby Trap. If you do, Booby Trap deals 10 damage to that player.').
card_image_name('booby trap'/'9ED', 'booby trap').
card_uid('booby trap'/'9ED', '9ED:Booby Trap:booby trap').
card_rarity('booby trap'/'9ED', 'Rare').
card_artist('booby trap'/'9ED', 'Doug Chaffee').
card_number('booby trap'/'9ED', '289').
card_multiverse_id('booby trap'/'9ED', '83142').

card_in_set('boomerang', '9ED').
card_original_type('boomerang'/'9ED', 'Instant').
card_original_text('boomerang'/'9ED', 'Return target permanent to its owner\'s hand.').
card_image_name('boomerang'/'9ED', 'boomerang').
card_uid('boomerang'/'9ED', '9ED:Boomerang:boomerang').
card_rarity('boomerang'/'9ED', 'Common').
card_artist('boomerang'/'9ED', 'Arnie Swekel').
card_number('boomerang'/'9ED', '66').
card_flavor_text('boomerang'/'9ED', '\"Returne from whence ye came. . . .\"\n—Edmund Spenser, The Faerie Queene').
card_multiverse_id('boomerang'/'9ED', '83013').

card_in_set('bottle gnomes', '9ED').
card_original_type('bottle gnomes'/'9ED', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'9ED', 'Sacrifice Bottle Gnomes: You gain 3 life.').
card_image_name('bottle gnomes'/'9ED', 'bottle gnomes').
card_uid('bottle gnomes'/'9ED', '9ED:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'9ED', 'Uncommon').
card_artist('bottle gnomes'/'9ED', 'Ben Thompson').
card_number('bottle gnomes'/'9ED', '290').
card_flavor_text('bottle gnomes'/'9ED', 'Reinforcements . . . or refreshments?').
card_multiverse_id('bottle gnomes'/'9ED', '83386').

card_in_set('brushland', '9ED').
card_original_type('brushland'/'9ED', 'Land').
card_original_text('brushland'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Brushland deals 1 damage to you.').
card_image_name('brushland'/'9ED', 'brushland').
card_uid('brushland'/'9ED', '9ED:Brushland:brushland').
card_rarity('brushland'/'9ED', 'Rare').
card_artist('brushland'/'9ED', 'Scott Bailey').
card_number('brushland'/'9ED', '319').
card_multiverse_id('brushland'/'9ED', '84600').

card_in_set('caves of koilos', '9ED').
card_original_type('caves of koilos'/'9ED', 'Land').
card_original_text('caves of koilos'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Caves of Koilos deals 1 damage to you.').
card_image_name('caves of koilos'/'9ED', 'caves of koilos').
card_uid('caves of koilos'/'9ED', '9ED:Caves of Koilos:caves of koilos').
card_rarity('caves of koilos'/'9ED', 'Rare').
card_artist('caves of koilos'/'9ED', 'Jim Nelson').
card_number('caves of koilos'/'9ED', '320').
card_multiverse_id('caves of koilos'/'9ED', '84467').

card_in_set('chastise', '9ED').
card_original_type('chastise'/'9ED', 'Instant').
card_original_text('chastise'/'9ED', 'Destroy target attacking creature. You gain life equal to its power.').
card_image_name('chastise'/'9ED', 'chastise').
card_uid('chastise'/'9ED', '9ED:Chastise:chastise').
card_rarity('chastise'/'9ED', 'Uncommon').
card_artist('chastise'/'9ED', 'Carl Critchlow').
card_number('chastise'/'9ED', '9').
card_flavor_text('chastise'/'9ED', '\"Why do we pray to the Ancestor? Because She listens.\"\n—Mystic elder').
card_multiverse_id('chastise'/'9ED', '83021').

card_in_set('circle of protection: black', '9ED').
card_original_type('circle of protection: black'/'9ED', 'Enchantment').
card_original_text('circle of protection: black'/'9ED', '{1}: The next time a black source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: black'/'9ED', 'circle of protection black').
card_uid('circle of protection: black'/'9ED', '9ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'9ED', 'Uncommon').
card_artist('circle of protection: black'/'9ED', 'Christopher Rush').
card_number('circle of protection: black'/'9ED', '10').
card_multiverse_id('circle of protection: black'/'9ED', '83024').

card_in_set('circle of protection: red', '9ED').
card_original_type('circle of protection: red'/'9ED', 'Enchantment').
card_original_text('circle of protection: red'/'9ED', '{1}: The next time a red source of your choice would deal damage to you this turn, prevent that damage.').
card_image_name('circle of protection: red'/'9ED', 'circle of protection red').
card_uid('circle of protection: red'/'9ED', '9ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'9ED', 'Uncommon').
card_artist('circle of protection: red'/'9ED', 'Christopher Rush').
card_number('circle of protection: red'/'9ED', '11').
card_multiverse_id('circle of protection: red'/'9ED', '83027').

card_in_set('clone', '9ED').
card_original_type('clone'/'9ED', 'Creature — Shapeshifter').
card_original_text('clone'/'9ED', 'As Clone comes into play, you may choose a creature in play. If you do, Clone comes into play as a copy of that creature.').
card_image_name('clone'/'9ED', 'clone').
card_uid('clone'/'9ED', '9ED:Clone:clone').
card_rarity('clone'/'9ED', 'Rare').
card_artist('clone'/'9ED', 'Kev Walker').
card_number('clone'/'9ED', '67').
card_multiverse_id('clone'/'9ED', '83525').

card_in_set('coat of arms', '9ED').
card_original_type('coat of arms'/'9ED', 'Artifact').
card_original_text('coat of arms'/'9ED', 'Each creature gets +1/+1 for each other creature in play that shares a creature type with it. (For example, if a Goblin Warrior, a Goblin Scout, and a Zombie Goblin are in play, each gets +2/+2.)').
card_image_name('coat of arms'/'9ED', 'coat of arms').
card_uid('coat of arms'/'9ED', '9ED:Coat of Arms:coat of arms').
card_rarity('coat of arms'/'9ED', 'Rare').
card_artist('coat of arms'/'9ED', 'Scott M. Fischer').
card_number('coat of arms'/'9ED', '291').
card_flavor_text('coat of arms'/'9ED', '\"Hup, two, three, four,\nDunno how to count no more.\"').
card_multiverse_id('coat of arms'/'9ED', '83033').

card_in_set('coercion', '9ED').
card_original_type('coercion'/'9ED', 'Sorcery').
card_original_text('coercion'/'9ED', 'Target opponent reveals his or her hand. Choose a card from it. That player discards that card.').
card_image_name('coercion'/'9ED', 'coercion').
card_uid('coercion'/'9ED', '9ED:Coercion:coercion').
card_rarity('coercion'/'9ED', 'Common').
card_artist('coercion'/'9ED', 'DiTerlizzi').
card_number('coercion'/'9ED', '118').
card_flavor_text('coercion'/'9ED', '\"Of all I that I have lost, I miss my mind the most.\"').
card_multiverse_id('coercion'/'9ED', '83034').

card_in_set('confiscate', '9ED').
card_original_type('confiscate'/'9ED', 'Enchantment — Aura').
card_original_text('confiscate'/'9ED', 'Enchant permanent (Target a permanent as you play this. This card comes into play attached to that permanent.)\nYou control enchanted permanent.').
card_image_name('confiscate'/'9ED', 'confiscate').
card_uid('confiscate'/'9ED', '9ED:Confiscate:confiscate').
card_rarity('confiscate'/'9ED', 'Uncommon').
card_artist('confiscate'/'9ED', 'Adam Rex').
card_number('confiscate'/'9ED', '68').
card_multiverse_id('confiscate'/'9ED', '83037').

card_in_set('consume spirit', '9ED').
card_original_type('consume spirit'/'9ED', 'Sorcery').
card_original_text('consume spirit'/'9ED', 'Spend only black mana on X.\nConsume Spirit deals X damage to target creature or player. You gain X life.').
card_image_name('consume spirit'/'9ED', 'consume spirit').
card_uid('consume spirit'/'9ED', '9ED:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'9ED', 'Uncommon').
card_artist('consume spirit'/'9ED', 'Matt Thompson').
card_number('consume spirit'/'9ED', '119').
card_flavor_text('consume spirit'/'9ED', '\"Your blood, your marrow, your spirit—all are mine.\"\n—Mayvar, minion of Geth').
card_multiverse_id('consume spirit'/'9ED', '83426').

card_in_set('contaminated bond', '9ED').
card_original_type('contaminated bond'/'9ED', 'Enchantment — Aura').
card_original_text('contaminated bond'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nWhenever enchanted creature attacks or blocks, its controller loses 3 life.').
card_image_name('contaminated bond'/'9ED', 'contaminated bond').
card_uid('contaminated bond'/'9ED', '9ED:Contaminated Bond:contaminated bond').
card_rarity('contaminated bond'/'9ED', 'Common').
card_artist('contaminated bond'/'9ED', 'Thomas M. Baxa').
card_number('contaminated bond'/'9ED', '120').
card_multiverse_id('contaminated bond'/'9ED', '83518').

card_in_set('coral eel', '9ED').
card_original_type('coral eel'/'9ED', 'Creature — Eel').
card_original_text('coral eel'/'9ED', '').
card_image_name('coral eel'/'9ED', 'coral eel').
card_uid('coral eel'/'9ED', '9ED:Coral Eel:coral eel').
card_rarity('coral eel'/'9ED', 'Common').
card_artist('coral eel'/'9ED', 'Una Fricker').
card_number('coral eel'/'9ED', 'S3').
card_flavor_text('coral eel'/'9ED', 'Some fishers like to eat eels, and some eels like to eat fishers.').
card_multiverse_id('coral eel'/'9ED', '84073').

card_in_set('counsel of the soratami', '9ED').
card_original_type('counsel of the soratami'/'9ED', 'Sorcery').
card_original_text('counsel of the soratami'/'9ED', 'Draw two cards.').
card_image_name('counsel of the soratami'/'9ED', 'counsel of the soratami').
card_uid('counsel of the soratami'/'9ED', '9ED:Counsel of the Soratami:counsel of the soratami').
card_rarity('counsel of the soratami'/'9ED', 'Common').
card_artist('counsel of the soratami'/'9ED', 'Randy Gallegos').
card_number('counsel of the soratami'/'9ED', '69').
card_flavor_text('counsel of the soratami'/'9ED', '\"Wisdom is not the counting of all the drops in a waterfall. Wisdom is learning why the water seeks the earth.\"').
card_multiverse_id('counsel of the soratami'/'9ED', '83390').

card_in_set('cowardice', '9ED').
card_original_type('cowardice'/'9ED', 'Enchantment').
card_original_text('cowardice'/'9ED', 'Whenever a creature becomes the target of a spell or ability, return that creature to its owner\'s hand. (It won\'t be affected by the spell or ability.)').
card_image_name('cowardice'/'9ED', 'cowardice').
card_uid('cowardice'/'9ED', '9ED:Cowardice:cowardice').
card_rarity('cowardice'/'9ED', 'Rare').
card_artist('cowardice'/'9ED', 'Michael Sutfin').
card_number('cowardice'/'9ED', '70').
card_flavor_text('cowardice'/'9ED', '\"Cowards die many times before their deaths;\nThe valiant never taste of death but once.\"\n—William Shakespeare, Julius Caesar').
card_multiverse_id('cowardice'/'9ED', '83039').

card_in_set('crafty pathmage', '9ED').
card_original_type('crafty pathmage'/'9ED', 'Creature — Human Wizard').
card_original_text('crafty pathmage'/'9ED', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_image_name('crafty pathmage'/'9ED', 'crafty pathmage').
card_uid('crafty pathmage'/'9ED', '9ED:Crafty Pathmage:crafty pathmage').
card_rarity('crafty pathmage'/'9ED', 'Common').
card_artist('crafty pathmage'/'9ED', 'Wayne England').
card_number('crafty pathmage'/'9ED', '71').
card_flavor_text('crafty pathmage'/'9ED', 'Follow the pathmage\n—Otarian expression meaning\n\"escape quickly\"').
card_multiverse_id('crafty pathmage'/'9ED', '83165').

card_in_set('craw wurm', '9ED').
card_original_type('craw wurm'/'9ED', 'Creature — Wurm').
card_original_text('craw wurm'/'9ED', '').
card_image_name('craw wurm'/'9ED', 'craw wurm').
card_uid('craw wurm'/'9ED', '9ED:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'9ED', 'Common').
card_artist('craw wurm'/'9ED', 'Richard Sardinha').
card_number('craw wurm'/'9ED', '233').
card_flavor_text('craw wurm'/'9ED', 'The most terrifying thing about the craw wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'9ED', '83040').

card_in_set('creeping mold', '9ED').
card_original_type('creeping mold'/'9ED', 'Sorcery').
card_original_text('creeping mold'/'9ED', 'Destroy target artifact, enchantment, or land.').
card_image_name('creeping mold'/'9ED', 'creeping mold').
card_uid('creeping mold'/'9ED', '9ED:Creeping Mold:creeping mold').
card_rarity('creeping mold'/'9ED', 'Uncommon').
card_artist('creeping mold'/'9ED', 'Rob Alexander').
card_number('creeping mold'/'9ED', '234').
card_flavor_text('creeping mold'/'9ED', 'Mold crept over the walls and into every crevice until the gleaming white stone strained and burst.').
card_multiverse_id('creeping mold'/'9ED', '83156').

card_in_set('crossbow infantry', '9ED').
card_original_type('crossbow infantry'/'9ED', 'Creature — Human Soldier').
card_original_text('crossbow infantry'/'9ED', '{T}: Crossbow Infantry deals 1 damage to target attacking or blocking creature.').
card_image_name('crossbow infantry'/'9ED', 'crossbow infantry').
card_uid('crossbow infantry'/'9ED', '9ED:Crossbow Infantry:crossbow infantry').
card_rarity('crossbow infantry'/'9ED', 'Common').
card_artist('crossbow infantry'/'9ED', 'James Bernardin').
card_number('crossbow infantry'/'9ED', '12').
card_flavor_text('crossbow infantry'/'9ED', '\"He can split a marshfly in two from halfway across the range.\"\n—Onean sergeant').
card_multiverse_id('crossbow infantry'/'9ED', '83042').

card_in_set('cruel edict', '9ED').
card_original_type('cruel edict'/'9ED', 'Sorcery').
card_original_text('cruel edict'/'9ED', 'Target opponent sacrifices a creature.').
card_image_name('cruel edict'/'9ED', 'cruel edict').
card_uid('cruel edict'/'9ED', '9ED:Cruel Edict:cruel edict').
card_rarity('cruel edict'/'9ED', 'Uncommon').
card_artist('cruel edict'/'9ED', 'Michael Sutfin').
card_number('cruel edict'/'9ED', '121').
card_flavor_text('cruel edict'/'9ED', '\"No mockeries now for them; no prayers nor bells, / Nor any voice of mourning save the choirs,— / The shrill, demented choirs of wailing shells.\"\n—Wilfred Owen,\n\"Anthem for Doomed Youth\"').
card_multiverse_id('cruel edict'/'9ED', '84659').

card_in_set('dancing scimitar', '9ED').
card_original_type('dancing scimitar'/'9ED', 'Artifact Creature — Spirit').
card_original_text('dancing scimitar'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('dancing scimitar'/'9ED', 'dancing scimitar').
card_uid('dancing scimitar'/'9ED', '9ED:Dancing Scimitar:dancing scimitar').
card_rarity('dancing scimitar'/'9ED', 'Uncommon').
card_artist('dancing scimitar'/'9ED', 'Ron Spears').
card_number('dancing scimitar'/'9ED', '292').
card_flavor_text('dancing scimitar'/'9ED', 'A blade that has never known sheath, a hilt that has never known hand.').
card_multiverse_id('dancing scimitar'/'9ED', '84114').

card_in_set('daring apprentice', '9ED').
card_original_type('daring apprentice'/'9ED', 'Creature — Human Wizard').
card_original_text('daring apprentice'/'9ED', '{T}, Sacrifice Daring Apprentice: Counter target spell.').
card_image_name('daring apprentice'/'9ED', 'daring apprentice').
card_uid('daring apprentice'/'9ED', '9ED:Daring Apprentice:daring apprentice').
card_rarity('daring apprentice'/'9ED', 'Rare').
card_artist('daring apprentice'/'9ED', 'Dany Orizio').
card_number('daring apprentice'/'9ED', '72').
card_flavor_text('daring apprentice'/'9ED', 'In front of every great wizard is a doomed apprentice.').
card_multiverse_id('daring apprentice'/'9ED', '82942').

card_in_set('dark banishing', '9ED').
card_original_type('dark banishing'/'9ED', 'Instant').
card_original_text('dark banishing'/'9ED', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('dark banishing'/'9ED', 'dark banishing').
card_uid('dark banishing'/'9ED', '9ED:Dark Banishing:dark banishing').
card_rarity('dark banishing'/'9ED', 'Common').
card_artist('dark banishing'/'9ED', 'Dermot Power').
card_number('dark banishing'/'9ED', '122').
card_flavor_text('dark banishing'/'9ED', '\"Ha, banishment? Be merciful, say ‘death,\'\nFor exile hath more terror in his look,\nMuch more than death.\"\n—William Shakespeare, Romeo and Juliet').
card_multiverse_id('dark banishing'/'9ED', '83046').

card_in_set('death pits of rath', '9ED').
card_original_type('death pits of rath'/'9ED', 'Enchantment').
card_original_text('death pits of rath'/'9ED', 'Whenever a creature is dealt damage, destroy it. It can\'t be regenerated.').
card_image_name('death pits of rath'/'9ED', 'death pits of rath').
card_uid('death pits of rath'/'9ED', '9ED:Death Pits of Rath:death pits of rath').
card_rarity('death pits of rath'/'9ED', 'Rare').
card_artist('death pits of rath'/'9ED', 'Greg & Tim Hildebrandt').
card_number('death pits of rath'/'9ED', '123').
card_flavor_text('death pits of rath'/'9ED', '\"Neither could I forget what I had read of these pits—that the sudden extinction of life formed no part of their most horrible plan.\"\n—Edgar Allan Poe,\n\"The Pit and the Pendulum\"').
card_multiverse_id('death pits of rath'/'9ED', '83204').

card_in_set('deathgazer', '9ED').
card_original_type('deathgazer'/'9ED', 'Creature — Lizard').
card_original_text('deathgazer'/'9ED', 'Whenever Deathgazer blocks or becomes blocked by a nonblack creature, destroy that creature at end of combat.').
card_image_name('deathgazer'/'9ED', 'deathgazer').
card_uid('deathgazer'/'9ED', '9ED:Deathgazer:deathgazer').
card_rarity('deathgazer'/'9ED', 'Uncommon').
card_artist('deathgazer'/'9ED', 'Donato Giancola').
card_number('deathgazer'/'9ED', '124').
card_flavor_text('deathgazer'/'9ED', 'Your entire life passes before its eyes.').
card_multiverse_id('deathgazer'/'9ED', '83420').

card_in_set('defense grid', '9ED').
card_original_type('defense grid'/'9ED', 'Artifact').
card_original_text('defense grid'/'9ED', 'During each player\'s turn, each other player\'s spells cost {3} more to play.').
card_image_name('defense grid'/'9ED', 'defense grid').
card_uid('defense grid'/'9ED', '9ED:Defense Grid:defense grid').
card_rarity('defense grid'/'9ED', 'Rare').
card_artist('defense grid'/'9ED', 'Mark Tedin').
card_number('defense grid'/'9ED', '293').
card_flavor_text('defense grid'/'9ED', 'It lights up the sky, beckoning enemies only to repel them.').
card_multiverse_id('defense grid'/'9ED', '83051').

card_in_set('dehydration', '9ED').
card_original_type('dehydration'/'9ED', 'Enchantment — Aura').
card_original_text('dehydration'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('dehydration'/'9ED', 'dehydration').
card_uid('dehydration'/'9ED', '9ED:Dehydration:dehydration').
card_rarity('dehydration'/'9ED', 'Common').
card_artist('dehydration'/'9ED', 'Arnie Swekel').
card_number('dehydration'/'9ED', '73').
card_multiverse_id('dehydration'/'9ED', '83053').

card_in_set('demolish', '9ED').
card_original_type('demolish'/'9ED', 'Sorcery').
card_original_text('demolish'/'9ED', 'Destroy target artifact or land.').
card_image_name('demolish'/'9ED', 'demolish').
card_uid('demolish'/'9ED', '9ED:Demolish:demolish').
card_rarity('demolish'/'9ED', 'Uncommon').
card_artist('demolish'/'9ED', 'Gary Ruddell').
card_number('demolish'/'9ED', '179').
card_flavor_text('demolish'/'9ED', '\"Pound the steel until it fits.\nDoesn\'t work? Bash to bits.\"\n—Dwarven forging song').
card_multiverse_id('demolish'/'9ED', '83054').

card_in_set('demon\'s horn', '9ED').
card_original_type('demon\'s horn'/'9ED', 'Artifact').
card_original_text('demon\'s horn'/'9ED', 'Whenever a player plays a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'9ED', 'demon\'s horn').
card_uid('demon\'s horn'/'9ED', '9ED:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'9ED', 'Uncommon').
card_artist('demon\'s horn'/'9ED', 'Alan Pollack').
card_number('demon\'s horn'/'9ED', '294').
card_flavor_text('demon\'s horn'/'9ED', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'9ED', '83473').

card_in_set('demystify', '9ED').
card_original_type('demystify'/'9ED', 'Instant').
card_original_text('demystify'/'9ED', 'Destroy target enchantment.').
card_image_name('demystify'/'9ED', 'demystify').
card_uid('demystify'/'9ED', '9ED:Demystify:demystify').
card_rarity('demystify'/'9ED', 'Common').
card_artist('demystify'/'9ED', 'Christopher Rush').
card_number('demystify'/'9ED', '13').
card_multiverse_id('demystify'/'9ED', '83055').

card_in_set('diabolic tutor', '9ED').
card_original_type('diabolic tutor'/'9ED', 'Sorcery').
card_original_text('diabolic tutor'/'9ED', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'9ED', 'diabolic tutor').
card_uid('diabolic tutor'/'9ED', '9ED:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'9ED', 'Uncommon').
card_artist('diabolic tutor'/'9ED', 'Rick Farrell').
card_number('diabolic tutor'/'9ED', '125').
card_flavor_text('diabolic tutor'/'9ED', 'The best ideas often come from the worst minds.').
card_multiverse_id('diabolic tutor'/'9ED', '83056').

card_in_set('disrupting scepter', '9ED').
card_original_type('disrupting scepter'/'9ED', 'Artifact').
card_original_text('disrupting scepter'/'9ED', '{3}, {T}: Target player discards a card. Play this ability only during your turn.').
card_image_name('disrupting scepter'/'9ED', 'disrupting scepter').
card_uid('disrupting scepter'/'9ED', '9ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'9ED', 'Rare').
card_artist('disrupting scepter'/'9ED', 'Stuart Griffin').
card_number('disrupting scepter'/'9ED', '295').
card_flavor_text('disrupting scepter'/'9ED', '\"True power is controlling not only the hearts of your subjects, but their minds as well.\"').
card_multiverse_id('disrupting scepter'/'9ED', '83058').

card_in_set('dragon\'s claw', '9ED').
card_original_type('dragon\'s claw'/'9ED', 'Artifact').
card_original_text('dragon\'s claw'/'9ED', 'Whenever a player plays a red spell, you may gain 1 life.').
card_image_name('dragon\'s claw'/'9ED', 'dragon\'s claw').
card_uid('dragon\'s claw'/'9ED', '9ED:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'9ED', 'Uncommon').
card_artist('dragon\'s claw'/'9ED', 'Alan Pollack').
card_number('dragon\'s claw'/'9ED', '296').
card_flavor_text('dragon\'s claw'/'9ED', 'Though no longer attached to the hand, it still holds its adversary in its grasp.').
card_multiverse_id('dragon\'s claw'/'9ED', '83498').

card_in_set('dream prowler', '9ED').
card_original_type('dream prowler'/'9ED', 'Creature — Illusion').
card_original_text('dream prowler'/'9ED', 'Dream Prowler is unblockable as long as it\'s attacking alone.').
card_image_name('dream prowler'/'9ED', 'dream prowler').
card_uid('dream prowler'/'9ED', '9ED:Dream Prowler:dream prowler').
card_rarity('dream prowler'/'9ED', 'Uncommon').
card_artist('dream prowler'/'9ED', 'Matt Cavotta').
card_number('dream prowler'/'9ED', '74').
card_flavor_text('dream prowler'/'9ED', '\"To think that some find sleep a restful state.\"\n—Volrath').
card_multiverse_id('dream prowler'/'9ED', '83258').

card_in_set('drudge skeletons', '9ED').
card_original_type('drudge skeletons'/'9ED', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'9ED', '{B}: Regenerate Drudge Skeletons. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('drudge skeletons'/'9ED', 'drudge skeletons').
card_uid('drudge skeletons'/'9ED', '9ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'9ED', 'Uncommon').
card_artist('drudge skeletons'/'9ED', 'Jim Nelson').
card_number('drudge skeletons'/'9ED', '126').
card_flavor_text('drudge skeletons'/'9ED', '\"The dead make good soldiers. They can\'t disobey orders, never surrender, and don\'t stop fighting when a random body part falls off.\"\n—Nevinyrral, Necromancer\'s Handbook').
card_multiverse_id('drudge skeletons'/'9ED', '83061').

card_in_set('eager cadet', '9ED').
card_original_type('eager cadet'/'9ED', 'Creature — Human Soldier').
card_original_text('eager cadet'/'9ED', '').
card_image_name('eager cadet'/'9ED', 'eager cadet').
card_uid('eager cadet'/'9ED', '9ED:Eager Cadet:eager cadet').
card_rarity('eager cadet'/'9ED', 'Common').
card_artist('eager cadet'/'9ED', 'Scott M. Fischer').
card_number('eager cadet'/'9ED', 'S1').
card_flavor_text('eager cadet'/'9ED', '\"Training? Seeing my crops burnt to cinders was all the ‘training\' I needed.\"').
card_multiverse_id('eager cadet'/'9ED', '83064').

card_in_set('early harvest', '9ED').
card_original_type('early harvest'/'9ED', 'Instant').
card_original_text('early harvest'/'9ED', 'Target player untaps all basic lands he or she controls.').
card_image_name('early harvest'/'9ED', 'early harvest').
card_uid('early harvest'/'9ED', '9ED:Early Harvest:early harvest').
card_rarity('early harvest'/'9ED', 'Rare').
card_artist('early harvest'/'9ED', 'Heather Hudson').
card_number('early harvest'/'9ED', '235').
card_flavor_text('early harvest'/'9ED', '\"Earth\'s increase, foison plenty,\nBarns and garners never empty:\nVines with clust\'ring bunches growing;\nPlants with goodly burden bowing.\"\n—William Shakespeare, The Tempest').
card_multiverse_id('early harvest'/'9ED', '84541').

card_in_set('elvish bard', '9ED').
card_original_type('elvish bard'/'9ED', 'Creature — Elf Shaman').
card_original_text('elvish bard'/'9ED', 'All creatures able to block Elvish Bard do so.').
card_image_name('elvish bard'/'9ED', 'elvish bard').
card_uid('elvish bard'/'9ED', '9ED:Elvish Bard:elvish bard').
card_rarity('elvish bard'/'9ED', 'Uncommon').
card_artist('elvish bard'/'9ED', 'Rob Alexander').
card_number('elvish bard'/'9ED', '236').
card_flavor_text('elvish bard'/'9ED', 'Helplessly drawn in, the bard\'s enemies quickly find there\'s more than song to be found.').
card_multiverse_id('elvish bard'/'9ED', '82960').

card_in_set('elvish berserker', '9ED').
card_original_type('elvish berserker'/'9ED', 'Creature — Elf Berserker').
card_original_text('elvish berserker'/'9ED', 'Whenever Elvish Berserker becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_image_name('elvish berserker'/'9ED', 'elvish berserker').
card_uid('elvish berserker'/'9ED', '9ED:Elvish Berserker:elvish berserker').
card_rarity('elvish berserker'/'9ED', 'Common').
card_artist('elvish berserker'/'9ED', 'Paolo Parente').
card_number('elvish berserker'/'9ED', '237').
card_flavor_text('elvish berserker'/'9ED', 'Their fury scatters enemies like a pile of dry leaves.').
card_multiverse_id('elvish berserker'/'9ED', '84661').

card_in_set('elvish champion', '9ED').
card_original_type('elvish champion'/'9ED', 'Creature — Elf Lord').
card_original_text('elvish champion'/'9ED', 'Other Elves get +1/+1 and have forestwalk. (They\'re unblockable as long as defending player controls a Forest.)').
card_image_name('elvish champion'/'9ED', 'elvish champion').
card_uid('elvish champion'/'9ED', '9ED:Elvish Champion:elvish champion').
card_rarity('elvish champion'/'9ED', 'Rare').
card_artist('elvish champion'/'9ED', 'D. Alexander Gregory').
card_number('elvish champion'/'9ED', '238').
card_flavor_text('elvish champion'/'9ED', '\"For what are leaves but countless blades\nTo fight a countless foe on high.\"\n—Elvish hymn').
card_multiverse_id('elvish champion'/'9ED', '83069').

card_in_set('elvish piper', '9ED').
card_original_type('elvish piper'/'9ED', 'Creature — Elf Shaman').
card_original_text('elvish piper'/'9ED', '{G}, {T}: Put a creature card from your hand into play.').
card_image_name('elvish piper'/'9ED', 'elvish piper').
card_uid('elvish piper'/'9ED', '9ED:Elvish Piper:elvish piper').
card_rarity('elvish piper'/'9ED', 'Rare').
card_artist('elvish piper'/'9ED', 'Rebecca Guay').
card_number('elvish piper'/'9ED', '239').
card_flavor_text('elvish piper'/'9ED', 'From Gaea grew the world, and the world was silent. From Gaea grew the world\'s elves, and the world was silent no more.\n—Elvish teaching').
card_multiverse_id('elvish piper'/'9ED', '83072').

card_in_set('elvish warrior', '9ED').
card_original_type('elvish warrior'/'9ED', 'Creature — Elf Warrior').
card_original_text('elvish warrior'/'9ED', '').
card_image_name('elvish warrior'/'9ED', 'elvish warrior').
card_uid('elvish warrior'/'9ED', '9ED:Elvish Warrior:elvish warrior').
card_rarity('elvish warrior'/'9ED', 'Common').
card_artist('elvish warrior'/'9ED', 'Christopher Moeller').
card_number('elvish warrior'/'9ED', '240').
card_flavor_text('elvish warrior'/'9ED', '\"My tales of war are the stories most asked for around the fires at night, but they\'re the ones I care least to tell.\"').
card_multiverse_id('elvish warrior'/'9ED', '83451').

card_in_set('emperor crocodile', '9ED').
card_original_type('emperor crocodile'/'9ED', 'Creature — Crocodile').
card_original_text('emperor crocodile'/'9ED', 'When you control no other creatures, sacrifice Emperor Crocodile.').
card_image_name('emperor crocodile'/'9ED', 'emperor crocodile').
card_uid('emperor crocodile'/'9ED', '9ED:Emperor Crocodile:emperor crocodile').
card_rarity('emperor crocodile'/'9ED', 'Rare').
card_artist('emperor crocodile'/'9ED', 'Kev Walker').
card_number('emperor crocodile'/'9ED', '241').
card_flavor_text('emperor crocodile'/'9ED', 'The king of Yavimaya\'s waters pays constant attention to his subjects . . . and thrives on their adulation.').
card_multiverse_id('emperor crocodile'/'9ED', '83074').

card_in_set('enfeeblement', '9ED').
card_original_type('enfeeblement'/'9ED', 'Enchantment — Aura').
card_original_text('enfeeblement'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets -2/-2.').
card_image_name('enfeeblement'/'9ED', 'enfeeblement').
card_uid('enfeeblement'/'9ED', '9ED:Enfeeblement:enfeeblement').
card_rarity('enfeeblement'/'9ED', 'Common').
card_artist('enfeeblement'/'9ED', 'John Bolton').
card_number('enfeeblement'/'9ED', '127').
card_flavor_text('enfeeblement'/'9ED', '\"If it is weak, either kill it or ignore it. Anything else honors it.\"\n—Kaervek').
card_multiverse_id('enfeeblement'/'9ED', '82973').

card_in_set('enormous baloth', '9ED').
card_original_type('enormous baloth'/'9ED', 'Creature — Beast').
card_original_text('enormous baloth'/'9ED', '').
card_image_name('enormous baloth'/'9ED', 'enormous baloth').
card_uid('enormous baloth'/'9ED', '9ED:Enormous Baloth:enormous baloth').
card_rarity('enormous baloth'/'9ED', 'Uncommon').
card_artist('enormous baloth'/'9ED', 'Mark Tedin').
card_number('enormous baloth'/'9ED', 'S9').
card_flavor_text('enormous baloth'/'9ED', 'Its diet consists of fruits, plants, small woodland animals, large woodland animals, woodlands, fruit groves, fruit farmers, and small cities.').
card_multiverse_id('enormous baloth'/'9ED', '83075').

card_in_set('enrage', '9ED').
card_original_type('enrage'/'9ED', 'Instant').
card_original_text('enrage'/'9ED', 'Target creature gets +X/+0 until end of turn.').
card_image_name('enrage'/'9ED', 'enrage').
card_uid('enrage'/'9ED', '9ED:Enrage:enrage').
card_rarity('enrage'/'9ED', 'Uncommon').
card_artist('enrage'/'9ED', 'Justin Sweet').
card_number('enrage'/'9ED', '180').
card_flavor_text('enrage'/'9ED', 'The barbarians call it \"touching the soul of the dragon.\"').
card_multiverse_id('enrage'/'9ED', '83076').

card_in_set('evacuation', '9ED').
card_original_type('evacuation'/'9ED', 'Instant').
card_original_text('evacuation'/'9ED', 'Return all creatures to their owners\' hands.').
card_image_name('evacuation'/'9ED', 'evacuation').
card_uid('evacuation'/'9ED', '9ED:Evacuation:evacuation').
card_rarity('evacuation'/'9ED', 'Rare').
card_artist('evacuation'/'9ED', 'Rob Alexander').
card_number('evacuation'/'9ED', '75').
card_flavor_text('evacuation'/'9ED', 'Which is more unsettling: the roar of battle or the silence that follows?').
card_multiverse_id('evacuation'/'9ED', '83078').

card_in_set('execute', '9ED').
card_original_type('execute'/'9ED', 'Instant').
card_original_text('execute'/'9ED', 'Destroy target white creature. It can\'t be regenerated.\nDraw a card.').
card_image_name('execute'/'9ED', 'execute').
card_uid('execute'/'9ED', '9ED:Execute:execute').
card_rarity('execute'/'9ED', 'Uncommon').
card_artist('execute'/'9ED', 'Gary Ruddell').
card_number('execute'/'9ED', '128').
card_flavor_text('execute'/'9ED', '\"Any fool who would die for honor is better off dead.\"\n—Cabal Patriarch').
card_multiverse_id('execute'/'9ED', '83079').

card_in_set('exhaustion', '9ED').
card_original_type('exhaustion'/'9ED', 'Sorcery').
card_original_text('exhaustion'/'9ED', 'Creatures and lands target opponent controls don\'t untap during his or her next untap step.').
card_image_name('exhaustion'/'9ED', 'exhaustion').
card_uid('exhaustion'/'9ED', '9ED:Exhaustion:exhaustion').
card_rarity('exhaustion'/'9ED', 'Uncommon').
card_artist('exhaustion'/'9ED', 'Paolo Parente').
card_number('exhaustion'/'9ED', '76').
card_flavor_text('exhaustion'/'9ED', 'Tired doesn\'t even begin to cover it.').
card_multiverse_id('exhaustion'/'9ED', '84065').

card_in_set('fear', '9ED').
card_original_type('fear'/'9ED', 'Enchantment — Aura').
card_original_text('fear'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('fear'/'9ED', 'fear').
card_uid('fear'/'9ED', '9ED:Fear:fear').
card_rarity('fear'/'9ED', 'Common').
card_artist('fear'/'9ED', 'Adam Rex').
card_number('fear'/'9ED', '129').
card_flavor_text('fear'/'9ED', '\"Don\'t fear the darkness. Fear what it hides.\"\n—Remin, venerable monk').
card_multiverse_id('fear'/'9ED', '83081').

card_in_set('fellwar stone', '9ED').
card_original_type('fellwar stone'/'9ED', 'Artifact').
card_original_text('fellwar stone'/'9ED', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_image_name('fellwar stone'/'9ED', 'fellwar stone').
card_uid('fellwar stone'/'9ED', '9ED:Fellwar Stone:fellwar stone').
card_rarity('fellwar stone'/'9ED', 'Uncommon').
card_artist('fellwar stone'/'9ED', 'John Avon').
card_number('fellwar stone'/'9ED', '297').
card_flavor_text('fellwar stone'/'9ED', '\"What do you have that I cannot obtain?\"\n—Mairsil, the Pretender').
card_multiverse_id('fellwar stone'/'9ED', '83489').

card_in_set('festering goblin', '9ED').
card_original_type('festering goblin'/'9ED', 'Creature — Zombie Goblin').
card_original_text('festering goblin'/'9ED', 'When Festering Goblin is put into a graveyard from play, target creature gets -1/-1 until end of turn.').
card_image_name('festering goblin'/'9ED', 'festering goblin').
card_uid('festering goblin'/'9ED', '9ED:Festering Goblin:festering goblin').
card_rarity('festering goblin'/'9ED', 'Common').
card_artist('festering goblin'/'9ED', 'Thomas M. Baxa').
card_number('festering goblin'/'9ED', '130').
card_flavor_text('festering goblin'/'9ED', 'In life, it was a fetid, disease-ridden thing. In death, not much changed.').
card_multiverse_id('festering goblin'/'9ED', '83157').

card_in_set('final punishment', '9ED').
card_original_type('final punishment'/'9ED', 'Sorcery').
card_original_text('final punishment'/'9ED', 'Target player loses life equal to the damage already dealt to him or her this turn.').
card_image_name('final punishment'/'9ED', 'final punishment').
card_uid('final punishment'/'9ED', '9ED:Final Punishment:final punishment').
card_rarity('final punishment'/'9ED', 'Rare').
card_artist('final punishment'/'9ED', 'Matt Thompson').
card_number('final punishment'/'9ED', '131').
card_flavor_text('final punishment'/'9ED', 'The pain of a lifetime—every scrape, illness, and bruise—condensed into a single moment.').
card_multiverse_id('final punishment'/'9ED', '84066').

card_in_set('firebreathing', '9ED').
card_original_type('firebreathing'/'9ED', 'Enchantment — Aura').
card_original_text('firebreathing'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'9ED', 'firebreathing').
card_uid('firebreathing'/'9ED', '9ED:Firebreathing:firebreathing').
card_rarity('firebreathing'/'9ED', 'Common').
card_artist('firebreathing'/'9ED', 'Mike Kerr').
card_number('firebreathing'/'9ED', '181').
card_multiverse_id('firebreathing'/'9ED', '82983').

card_in_set('fishliver oil', '9ED').
card_original_type('fishliver oil'/'9ED', 'Enchantment — Aura').
card_original_text('fishliver oil'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature has islandwalk. (This creature is unblockable as long as defending player controls an Island.)').
card_image_name('fishliver oil'/'9ED', 'fishliver oil').
card_uid('fishliver oil'/'9ED', '9ED:Fishliver Oil:fishliver oil').
card_rarity('fishliver oil'/'9ED', 'Common').
card_artist('fishliver oil'/'9ED', 'Ralph Horsley').
card_number('fishliver oil'/'9ED', '77').
card_multiverse_id('fishliver oil'/'9ED', '83438').

card_in_set('flame wave', '9ED').
card_original_type('flame wave'/'9ED', 'Sorcery').
card_original_text('flame wave'/'9ED', 'Flame Wave deals 4 damage to target player and each creature he or she controls.').
card_image_name('flame wave'/'9ED', 'flame wave').
card_uid('flame wave'/'9ED', '9ED:Flame Wave:flame wave').
card_rarity('flame wave'/'9ED', 'Uncommon').
card_artist('flame wave'/'9ED', 'Donato Giancola').
card_number('flame wave'/'9ED', '182').
card_flavor_text('flame wave'/'9ED', '\"I hear the roaring of a wave whose waters are red and whose mists are black.\"\n—Oracle en-Vec').
card_multiverse_id('flame wave'/'9ED', '84672').

card_in_set('flashfires', '9ED').
card_original_type('flashfires'/'9ED', 'Sorcery').
card_original_text('flashfires'/'9ED', 'Destroy all Plains.').
card_image_name('flashfires'/'9ED', 'flashfires').
card_uid('flashfires'/'9ED', '9ED:Flashfires:flashfires').
card_rarity('flashfires'/'9ED', 'Uncommon').
card_artist('flashfires'/'9ED', 'Randy Gallegos').
card_number('flashfires'/'9ED', '183').
card_flavor_text('flashfires'/'9ED', 'Dry grass is tinder before the spark.').
card_multiverse_id('flashfires'/'9ED', '83086').

card_in_set('fleeting image', '9ED').
card_original_type('fleeting image'/'9ED', 'Creature — Illusion').
card_original_text('fleeting image'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{1}{U}: Return Fleeting Image to its owner\'s hand.').
card_image_name('fleeting image'/'9ED', 'fleeting image').
card_uid('fleeting image'/'9ED', '9ED:Fleeting Image:fleeting image').
card_rarity('fleeting image'/'9ED', 'Rare').
card_artist('fleeting image'/'9ED', 'Dave Dorman').
card_number('fleeting image'/'9ED', '78').
card_flavor_text('fleeting image'/'9ED', '\"Beware lest you lose the substance by grasping at the shadow.\"\n—Aesop, Fables, trans. Jacobs').
card_multiverse_id('fleeting image'/'9ED', '83087').

card_in_set('flight', '9ED').
card_original_type('flight'/'9ED', 'Enchantment — Aura').
card_original_text('flight'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature has flying. (It can\'t be blocked except by creatures with flying.)').
card_image_name('flight'/'9ED', 'flight').
card_uid('flight'/'9ED', '9ED:Flight:flight').
card_rarity('flight'/'9ED', 'Common').
card_artist('flight'/'9ED', 'Arnie Swekel').
card_number('flight'/'9ED', '79').
card_multiverse_id('flight'/'9ED', '83088').

card_in_set('flowstone crusher', '9ED').
card_original_type('flowstone crusher'/'9ED', 'Creature — Beast').
card_original_text('flowstone crusher'/'9ED', '{R}: Flowstone Crusher gets +1/-1 until end of turn.').
card_image_name('flowstone crusher'/'9ED', 'flowstone crusher').
card_uid('flowstone crusher'/'9ED', '9ED:Flowstone Crusher:flowstone crusher').
card_rarity('flowstone crusher'/'9ED', 'Uncommon').
card_artist('flowstone crusher'/'9ED', 'Ben Thompson').
card_number('flowstone crusher'/'9ED', '184').
card_flavor_text('flowstone crusher'/'9ED', '\"What\'s better than smashing things with a rock? A rock that gets up and smashes things for you.\"\n—Toggo, goblin weaponsmith').
card_multiverse_id('flowstone crusher'/'9ED', '84124').

card_in_set('flowstone shambler', '9ED').
card_original_type('flowstone shambler'/'9ED', 'Creature — Beast').
card_original_text('flowstone shambler'/'9ED', '{R}: Flowstone Shambler gets +1/-1 until end of turn.').
card_image_name('flowstone shambler'/'9ED', 'flowstone shambler').
card_uid('flowstone shambler'/'9ED', '9ED:Flowstone Shambler:flowstone shambler').
card_rarity('flowstone shambler'/'9ED', 'Common').
card_artist('flowstone shambler'/'9ED', 'Jim Nelson').
card_number('flowstone shambler'/'9ED', '185').
card_flavor_text('flowstone shambler'/'9ED', 'Flowstone troops are notoriously difficult to keep in formation.').
card_multiverse_id('flowstone shambler'/'9ED', '84120').

card_in_set('flowstone slide', '9ED').
card_original_type('flowstone slide'/'9ED', 'Sorcery').
card_original_text('flowstone slide'/'9ED', 'All creatures get +X/-X until end of turn.').
card_image_name('flowstone slide'/'9ED', 'flowstone slide').
card_uid('flowstone slide'/'9ED', '9ED:Flowstone Slide:flowstone slide').
card_rarity('flowstone slide'/'9ED', 'Rare').
card_artist('flowstone slide'/'9ED', 'Chippy').
card_number('flowstone slide'/'9ED', '186').
card_flavor_text('flowstone slide'/'9ED', 'It may look like soil, but its nature is malice.').
card_multiverse_id('flowstone slide'/'9ED', '84535').

card_in_set('foot soldiers', '9ED').
card_original_type('foot soldiers'/'9ED', 'Creature — Human Soldier').
card_original_text('foot soldiers'/'9ED', '').
card_image_name('foot soldiers'/'9ED', 'foot soldiers').
card_uid('foot soldiers'/'9ED', '9ED:Foot Soldiers:foot soldiers').
card_rarity('foot soldiers'/'9ED', 'Common').
card_artist('foot soldiers'/'9ED', 'Kev Walker').
card_number('foot soldiers'/'9ED', '14').
card_flavor_text('foot soldiers'/'9ED', 'Infantry deployment is the art of putting your troops in the wrong place at the right time.').
card_multiverse_id('foot soldiers'/'9ED', '84608').

card_in_set('force of nature', '9ED').
card_original_type('force of nature'/'9ED', 'Creature — Elemental').
card_original_text('force of nature'/'9ED', 'Trample (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)\nAt the beginning of your upkeep, Force of Nature deals 8 damage to you unless you pay {G}{G}{G}{G}.').
card_image_name('force of nature'/'9ED', 'force of nature').
card_uid('force of nature'/'9ED', '9ED:Force of Nature:force of nature').
card_rarity('force of nature'/'9ED', 'Rare').
card_artist('force of nature'/'9ED', 'Greg Staples').
card_number('force of nature'/'9ED', '242').
card_multiverse_id('force of nature'/'9ED', '83977').

card_in_set('forest', '9ED').
card_original_type('forest'/'9ED', 'Basic Land — Forest').
card_original_text('forest'/'9ED', 'G').
card_image_name('forest'/'9ED', 'forest1').
card_uid('forest'/'9ED', '9ED:Forest:forest1').
card_rarity('forest'/'9ED', 'Basic Land').
card_artist('forest'/'9ED', 'John Avon').
card_number('forest'/'9ED', '347').
card_multiverse_id('forest'/'9ED', '83092').

card_in_set('forest', '9ED').
card_original_type('forest'/'9ED', 'Basic Land — Forest').
card_original_text('forest'/'9ED', 'G').
card_image_name('forest'/'9ED', 'forest2').
card_uid('forest'/'9ED', '9ED:Forest:forest2').
card_rarity('forest'/'9ED', 'Basic Land').
card_artist('forest'/'9ED', 'John Avon').
card_number('forest'/'9ED', '348').
card_multiverse_id('forest'/'9ED', '83093').

card_in_set('forest', '9ED').
card_original_type('forest'/'9ED', 'Basic Land — Forest').
card_original_text('forest'/'9ED', 'G').
card_image_name('forest'/'9ED', 'forest3').
card_uid('forest'/'9ED', '9ED:Forest:forest3').
card_rarity('forest'/'9ED', 'Basic Land').
card_artist('forest'/'9ED', 'John Avon').
card_number('forest'/'9ED', '349').
card_multiverse_id('forest'/'9ED', '83094').

card_in_set('forest', '9ED').
card_original_type('forest'/'9ED', 'Basic Land — Forest').
card_original_text('forest'/'9ED', 'G').
card_image_name('forest'/'9ED', 'forest4').
card_uid('forest'/'9ED', '9ED:Forest:forest4').
card_rarity('forest'/'9ED', 'Basic Land').
card_artist('forest'/'9ED', 'John Avon').
card_number('forest'/'9ED', '350').
card_multiverse_id('forest'/'9ED', '83095').

card_in_set('form of the dragon', '9ED').
card_original_type('form of the dragon'/'9ED', 'Enchantment').
card_original_text('form of the dragon'/'9ED', 'At the beginning of your upkeep, Form of the Dragon deals 5 damage to target creature or player.\nAt the end of each turn, your life total becomes 5.\nCreatures without flying can\'t attack you.').
card_image_name('form of the dragon'/'9ED', 'form of the dragon').
card_uid('form of the dragon'/'9ED', '9ED:Form of the Dragon:form of the dragon').
card_rarity('form of the dragon'/'9ED', 'Rare').
card_artist('form of the dragon'/'9ED', 'Carl Critchlow').
card_number('form of the dragon'/'9ED', '187').
card_multiverse_id('form of the dragon'/'9ED', '84068').

card_in_set('foul imp', '9ED').
card_original_type('foul imp'/'9ED', 'Creature — Imp').
card_original_text('foul imp'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Foul Imp comes into play, you lose 2 life.').
card_image_name('foul imp'/'9ED', 'foul imp').
card_uid('foul imp'/'9ED', '9ED:Foul Imp:foul imp').
card_rarity('foul imp'/'9ED', 'Common').
card_artist('foul imp'/'9ED', 'Kev Walker').
card_number('foul imp'/'9ED', '132').
card_flavor_text('foul imp'/'9ED', 'The imp, unaware of its own odor, paused to catch its breath . . . and promptly died.').
card_multiverse_id('foul imp'/'9ED', '84682').

card_in_set('fugitive wizard', '9ED').
card_original_type('fugitive wizard'/'9ED', 'Creature — Human Wizard').
card_original_text('fugitive wizard'/'9ED', '').
card_image_name('fugitive wizard'/'9ED', 'fugitive wizard').
card_uid('fugitive wizard'/'9ED', '9ED:Fugitive Wizard:fugitive wizard').
card_rarity('fugitive wizard'/'9ED', 'Common').
card_artist('fugitive wizard'/'9ED', 'Jim Nelson').
card_number('fugitive wizard'/'9ED', '80').
card_flavor_text('fugitive wizard'/'9ED', 'The College of Lat-Nam is often forced to expel students whose experiments grow too risky or too cruel.').
card_multiverse_id('fugitive wizard'/'9ED', '83096').

card_in_set('furnace of rath', '9ED').
card_original_type('furnace of rath'/'9ED', 'Enchantment').
card_original_text('furnace of rath'/'9ED', 'If a source would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_image_name('furnace of rath'/'9ED', 'furnace of rath').
card_uid('furnace of rath'/'9ED', '9ED:Furnace of Rath:furnace of rath').
card_rarity('furnace of rath'/'9ED', 'Rare').
card_artist('furnace of rath'/'9ED', 'John Matson').
card_number('furnace of rath'/'9ED', '188').
card_flavor_text('furnace of rath'/'9ED', 'The furnace awaits the next master who would stoke the fires of apocalypse.').
card_multiverse_id('furnace of rath'/'9ED', '83098').

card_in_set('giant cockroach', '9ED').
card_original_type('giant cockroach'/'9ED', 'Creature — Insect').
card_original_text('giant cockroach'/'9ED', '').
card_image_name('giant cockroach'/'9ED', 'giant cockroach').
card_uid('giant cockroach'/'9ED', '9ED:Giant Cockroach:giant cockroach').
card_rarity('giant cockroach'/'9ED', 'Common').
card_artist('giant cockroach'/'9ED', 'Heather Hudson').
card_number('giant cockroach'/'9ED', '133').
card_flavor_text('giant cockroach'/'9ED', 'Toren had stepped on a lot of bugs during his life, so he couldn\'t help feeling embarrassed when a bug stepped on him.').
card_multiverse_id('giant cockroach'/'9ED', '83102').

card_in_set('giant growth', '9ED').
card_original_type('giant growth'/'9ED', 'Instant').
card_original_text('giant growth'/'9ED', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'9ED', 'giant growth').
card_uid('giant growth'/'9ED', '9ED:Giant Growth:giant growth').
card_rarity('giant growth'/'9ED', 'Common').
card_artist('giant growth'/'9ED', 'Jim Murray').
card_number('giant growth'/'9ED', '243').
card_multiverse_id('giant growth'/'9ED', '83103').

card_in_set('giant octopus', '9ED').
card_original_type('giant octopus'/'9ED', 'Creature — Octopus').
card_original_text('giant octopus'/'9ED', '').
card_image_name('giant octopus'/'9ED', 'giant octopus').
card_uid('giant octopus'/'9ED', '9ED:Giant Octopus:giant octopus').
card_rarity('giant octopus'/'9ED', 'Common').
card_artist('giant octopus'/'9ED', 'Heather Hudson').
card_number('giant octopus'/'9ED', 'S4').
card_flavor_text('giant octopus'/'9ED', '\"Before my eyes was a horrible monster, worthy to figure in the legends of the marvellous. . . . Its eight arms, or rather feet, fixed to its head . . . were twice as long as its body, and were twisted like the furies\' hair.\"\n—Jules Verne, Twenty Thousand Leagues under the Sea, trans. Lewis').
card_multiverse_id('giant octopus'/'9ED', '83104').

card_in_set('giant spider', '9ED').
card_original_type('giant spider'/'9ED', 'Creature — Spider').
card_original_text('giant spider'/'9ED', 'Giant Spider can block as though it had flying.').
card_image_name('giant spider'/'9ED', 'giant spider').
card_uid('giant spider'/'9ED', '9ED:Giant Spider:giant spider').
card_rarity('giant spider'/'9ED', 'Common').
card_artist('giant spider'/'9ED', 'Randy Gallegos').
card_number('giant spider'/'9ED', '244').
card_flavor_text('giant spider'/'9ED', 'Watching the spider\'s web\n—Llanowar expression meaning\n\"focusing on the wrong thing\"').
card_multiverse_id('giant spider'/'9ED', '83105').

card_in_set('gift of estates', '9ED').
card_original_type('gift of estates'/'9ED', 'Sorcery').
card_original_text('gift of estates'/'9ED', 'If an opponent has more lands than you, search your library for up to three Plains cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('gift of estates'/'9ED', 'gift of estates').
card_uid('gift of estates'/'9ED', '9ED:Gift of Estates:gift of estates').
card_rarity('gift of estates'/'9ED', 'Uncommon').
card_artist('gift of estates'/'9ED', 'Hugh Jamieson').
card_number('gift of estates'/'9ED', '15').
card_multiverse_id('gift of estates'/'9ED', '84504').

card_in_set('glorious anthem', '9ED').
card_original_type('glorious anthem'/'9ED', 'Enchantment').
card_original_text('glorious anthem'/'9ED', 'Creatures you control get +1/+1.').
card_image_name('glorious anthem'/'9ED', 'glorious anthem').
card_uid('glorious anthem'/'9ED', '9ED:Glorious Anthem:glorious anthem').
card_rarity('glorious anthem'/'9ED', 'Rare').
card_artist('glorious anthem'/'9ED', 'Kev Walker').
card_number('glorious anthem'/'9ED', '16').
card_flavor_text('glorious anthem'/'9ED', 'Each victory adds a new verse.').
card_multiverse_id('glorious anthem'/'9ED', '83106').

card_in_set('glory seeker', '9ED').
card_original_type('glory seeker'/'9ED', 'Creature — Human Soldier').
card_original_text('glory seeker'/'9ED', '').
card_image_name('glory seeker'/'9ED', 'glory seeker').
card_uid('glory seeker'/'9ED', '9ED:Glory Seeker:glory seeker').
card_rarity('glory seeker'/'9ED', 'Common').
card_artist('glory seeker'/'9ED', 'Dave Dorman').
card_number('glory seeker'/'9ED', '17').
card_flavor_text('glory seeker'/'9ED', 'The turning of the tide always begins with one soldier\'s decision to head back into the fray.').
card_multiverse_id('glory seeker'/'9ED', '83107').

card_in_set('gluttonous zombie', '9ED').
card_original_type('gluttonous zombie'/'9ED', 'Creature — Zombie').
card_original_text('gluttonous zombie'/'9ED', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('gluttonous zombie'/'9ED', 'gluttonous zombie').
card_uid('gluttonous zombie'/'9ED', '9ED:Gluttonous Zombie:gluttonous zombie').
card_rarity('gluttonous zombie'/'9ED', 'Uncommon').
card_artist('gluttonous zombie'/'9ED', 'Thomas M. Baxa').
card_number('gluttonous zombie'/'9ED', '134').
card_flavor_text('gluttonous zombie'/'9ED', 'Greed and gluttony led him to death. Now they are his greatest assets.').
card_multiverse_id('gluttonous zombie'/'9ED', '84064').

card_in_set('goblin balloon brigade', '9ED').
card_original_type('goblin balloon brigade'/'9ED', 'Creature — Goblin Warrior').
card_original_text('goblin balloon brigade'/'9ED', '{R}: Goblin Balloon Brigade gains flying until end of turn. (It can\'t be blocked except by creatures with flying.)').
card_image_name('goblin balloon brigade'/'9ED', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'9ED', '9ED:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'9ED', 'Uncommon').
card_artist('goblin balloon brigade'/'9ED', 'Lars Grant-West').
card_number('goblin balloon brigade'/'9ED', '189').
card_flavor_text('goblin balloon brigade'/'9ED', 'They patched the holes, loaded their sharpest rocks, spanked their children one last time, and lurched off into the sunset.').
card_multiverse_id('goblin balloon brigade'/'9ED', '84540').

card_in_set('goblin brigand', '9ED').
card_original_type('goblin brigand'/'9ED', 'Creature — Goblin Warrior').
card_original_text('goblin brigand'/'9ED', 'Goblin Brigand attacks each turn if able.').
card_image_name('goblin brigand'/'9ED', 'goblin brigand').
card_uid('goblin brigand'/'9ED', '9ED:Goblin Brigand:goblin brigand').
card_rarity('goblin brigand'/'9ED', 'Common').
card_artist('goblin brigand'/'9ED', 'Arnie Swekel').
card_number('goblin brigand'/'9ED', '190').
card_flavor_text('goblin brigand'/'9ED', 'Like a loaded cannon—point and shoot.').
card_multiverse_id('goblin brigand'/'9ED', '83374').

card_in_set('goblin chariot', '9ED').
card_original_type('goblin chariot'/'9ED', 'Creature — Goblin Warrior').
card_original_text('goblin chariot'/'9ED', 'Haste (This creature may attack the turn it comes under your control.)').
card_image_name('goblin chariot'/'9ED', 'goblin chariot').
card_uid('goblin chariot'/'9ED', '9ED:Goblin Chariot:goblin chariot').
card_rarity('goblin chariot'/'9ED', 'Common').
card_artist('goblin chariot'/'9ED', 'John Howe').
card_number('goblin chariot'/'9ED', '191').
card_flavor_text('goblin chariot'/'9ED', 'The hardest part of his training is learning not to eat the boar.').
card_multiverse_id('goblin chariot'/'9ED', '83109').

card_in_set('goblin king', '9ED').
card_original_type('goblin king'/'9ED', 'Creature — Goblin Lord').
card_original_text('goblin king'/'9ED', 'Other Goblins get +1/+1 and have mountainwalk. (They\'re unblockable as long as defending player controls a Mountain.)').
card_image_name('goblin king'/'9ED', 'goblin king').
card_uid('goblin king'/'9ED', '9ED:Goblin King:goblin king').
card_rarity('goblin king'/'9ED', 'Rare').
card_artist('goblin king'/'9ED', 'Ron Spears').
card_number('goblin king'/'9ED', '192').
card_flavor_text('goblin king'/'9ED', 'To be king, Numsgil did in Blog, who did in Unkful, who did in Viddle, who did in Loll, who did in Alrok. . . .').
card_multiverse_id('goblin king'/'9ED', '83111').

card_in_set('goblin mountaineer', '9ED').
card_original_type('goblin mountaineer'/'9ED', 'Creature — Goblin Scout').
card_original_text('goblin mountaineer'/'9ED', 'Mountainwalk (This creature is unblockable as long as defending player controls a Mountain.)').
card_image_name('goblin mountaineer'/'9ED', 'goblin mountaineer').
card_uid('goblin mountaineer'/'9ED', '9ED:Goblin Mountaineer:goblin mountaineer').
card_rarity('goblin mountaineer'/'9ED', 'Common').
card_artist('goblin mountaineer'/'9ED', 'Pete Venters').
card_number('goblin mountaineer'/'9ED', '193').
card_flavor_text('goblin mountaineer'/'9ED', 'The only thing worse than a goblin is a goblin that knows his way around.').
card_multiverse_id('goblin mountaineer'/'9ED', '83018').

card_in_set('goblin piker', '9ED').
card_original_type('goblin piker'/'9ED', 'Creature — Goblin Warrior').
card_original_text('goblin piker'/'9ED', '').
card_image_name('goblin piker'/'9ED', 'goblin piker').
card_uid('goblin piker'/'9ED', '9ED:Goblin Piker:goblin piker').
card_rarity('goblin piker'/'9ED', 'Common').
card_artist('goblin piker'/'9ED', 'DiTerlizzi').
card_number('goblin piker'/'9ED', '194').
card_flavor_text('goblin piker'/'9ED', 'Once he\'d worked out which end of the thing was sharp, he was promoted to guard duty.').
card_multiverse_id('goblin piker'/'9ED', '82957').

card_in_set('goblin raider', '9ED').
card_original_type('goblin raider'/'9ED', 'Creature — Goblin Warrior').
card_original_text('goblin raider'/'9ED', 'Goblin Raider can\'t block.').
card_image_name('goblin raider'/'9ED', 'goblin raider').
card_uid('goblin raider'/'9ED', '9ED:Goblin Raider:goblin raider').
card_rarity('goblin raider'/'9ED', 'Common').
card_artist('goblin raider'/'9ED', 'Greg Staples').
card_number('goblin raider'/'9ED', 'S8').
card_flavor_text('goblin raider'/'9ED', 'He\'s smart for a goblin. He can do two things: hit and run.').
card_multiverse_id('goblin raider'/'9ED', '94910').

card_in_set('goblin sky raider', '9ED').
card_original_type('goblin sky raider'/'9ED', 'Creature — Goblin Warrior').
card_original_text('goblin sky raider'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('goblin sky raider'/'9ED', 'goblin sky raider').
card_uid('goblin sky raider'/'9ED', '9ED:Goblin Sky Raider:goblin sky raider').
card_rarity('goblin sky raider'/'9ED', 'Common').
card_artist('goblin sky raider'/'9ED', 'Daren Bader').
card_number('goblin sky raider'/'9ED', '195').
card_flavor_text('goblin sky raider'/'9ED', 'The goblin word for \"flying\" is more accurately translated as \"falling slowly.\"').
card_multiverse_id('goblin sky raider'/'9ED', '83305').

card_in_set('grave pact', '9ED').
card_original_type('grave pact'/'9ED', 'Enchantment').
card_original_text('grave pact'/'9ED', 'Whenever a creature you control is put into a graveyard from play, each other player sacrifices a creature.').
card_image_name('grave pact'/'9ED', 'grave pact').
card_uid('grave pact'/'9ED', '9ED:Grave Pact:grave pact').
card_rarity('grave pact'/'9ED', 'Rare').
card_artist('grave pact'/'9ED', 'Puddnhead').
card_number('grave pact'/'9ED', '135').
card_flavor_text('grave pact'/'9ED', '\"The bonds of loyalty can tie one to the grave.\"\n—Crovax, ascendant evincar').
card_multiverse_id('grave pact'/'9ED', '83113').

card_in_set('gravedigger', '9ED').
card_original_type('gravedigger'/'9ED', 'Creature — Zombie').
card_original_text('gravedigger'/'9ED', 'When Gravedigger comes into play, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'9ED', 'gravedigger').
card_uid('gravedigger'/'9ED', '9ED:Gravedigger:gravedigger').
card_rarity('gravedigger'/'9ED', 'Common').
card_artist('gravedigger'/'9ED', 'Dermot Power').
card_number('gravedigger'/'9ED', '136').
card_flavor_text('gravedigger'/'9ED', 'A full coffin is like a full coffer—both are attractive to thieves.').
card_multiverse_id('gravedigger'/'9ED', '83114').

card_in_set('greater good', '9ED').
card_original_type('greater good'/'9ED', 'Enchantment').
card_original_text('greater good'/'9ED', 'Sacrifice a creature: Draw cards equal to the sacrificed creature\'s power, then discard three cards.').
card_image_name('greater good'/'9ED', 'greater good').
card_uid('greater good'/'9ED', '9ED:Greater Good:greater good').
card_rarity('greater good'/'9ED', 'Rare').
card_artist('greater good'/'9ED', 'Douglas Shuler').
card_number('greater good'/'9ED', '245').
card_flavor_text('greater good'/'9ED', '\"To examine the causes of life, we must first have recourse to death.\"\n—Mary Shelley, Frankenstein').
card_multiverse_id('greater good'/'9ED', '83035').

card_in_set('grizzly bears', '9ED').
card_original_type('grizzly bears'/'9ED', 'Creature — Bear').
card_original_text('grizzly bears'/'9ED', '').
card_image_name('grizzly bears'/'9ED', 'grizzly bears').
card_uid('grizzly bears'/'9ED', '9ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'9ED', 'Common').
card_artist('grizzly bears'/'9ED', 'D. J. Cleland-Hura').
card_number('grizzly bears'/'9ED', '246').
card_flavor_text('grizzly bears'/'9ED', 'Don\'t try to outrun one of Dominaria\'s grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'9ED', '83115').

card_in_set('groundskeeper', '9ED').
card_original_type('groundskeeper'/'9ED', 'Creature — Human Druid').
card_original_text('groundskeeper'/'9ED', '{1}{G}: Return target basic land card from your graveyard to your hand.').
card_image_name('groundskeeper'/'9ED', 'groundskeeper').
card_uid('groundskeeper'/'9ED', '9ED:Groundskeeper:groundskeeper').
card_rarity('groundskeeper'/'9ED', 'Uncommon').
card_artist('groundskeeper'/'9ED', 'Alan Rabinowitz').
card_number('groundskeeper'/'9ED', '247').
card_flavor_text('groundskeeper'/'9ED', '\"Soldiers speak of defending the soil, but they care only for their nations. I care nothing for their nations and everything for the soil.\"').
card_multiverse_id('groundskeeper'/'9ED', '84127').

card_in_set('guerrilla tactics', '9ED').
card_original_type('guerrilla tactics'/'9ED', 'Instant').
card_original_text('guerrilla tactics'/'9ED', 'Guerrilla Tactics deals 2 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Guerrilla Tactics, Guerrilla Tactics deals 4 damage to target creature or player.').
card_image_name('guerrilla tactics'/'9ED', 'guerrilla tactics').
card_uid('guerrilla tactics'/'9ED', '9ED:Guerrilla Tactics:guerrilla tactics').
card_rarity('guerrilla tactics'/'9ED', 'Uncommon').
card_artist('guerrilla tactics'/'9ED', 'Ron Spencer').
card_number('guerrilla tactics'/'9ED', '196').
card_multiverse_id('guerrilla tactics'/'9ED', '83217').

card_in_set('hell\'s caretaker', '9ED').
card_original_type('hell\'s caretaker'/'9ED', 'Creature — Horror').
card_original_text('hell\'s caretaker'/'9ED', '{T}, Sacrifice a creature: Return target creature card from your graveyard to play. Play this ability only during your upkeep.').
card_image_name('hell\'s caretaker'/'9ED', 'hell\'s caretaker').
card_uid('hell\'s caretaker'/'9ED', '9ED:Hell\'s Caretaker:hell\'s caretaker').
card_rarity('hell\'s caretaker'/'9ED', 'Rare').
card_artist('hell\'s caretaker'/'9ED', 'Greg Staples').
card_number('hell\'s caretaker'/'9ED', '137').
card_multiverse_id('hell\'s caretaker'/'9ED', '84590').

card_in_set('highway robber', '9ED').
card_original_type('highway robber'/'9ED', 'Creature — Human Mercenary').
card_original_text('highway robber'/'9ED', 'When Highway Robber comes into play, you gain 2 life and target opponent loses 2 life.').
card_image_name('highway robber'/'9ED', 'highway robber').
card_uid('highway robber'/'9ED', '9ED:Highway Robber:highway robber').
card_rarity('highway robber'/'9ED', 'Common').
card_artist('highway robber'/'9ED', 'Kev Walker').
card_number('highway robber'/'9ED', '138').
card_flavor_text('highway robber'/'9ED', '\"Tonight, madam, it\'s your money and your life.\"').
card_multiverse_id('highway robber'/'9ED', '84505').

card_in_set('hill giant', '9ED').
card_original_type('hill giant'/'9ED', 'Creature — Giant').
card_original_text('hill giant'/'9ED', '').
card_image_name('hill giant'/'9ED', 'hill giant').
card_uid('hill giant'/'9ED', '9ED:Hill Giant:hill giant').
card_rarity('hill giant'/'9ED', 'Common').
card_artist('hill giant'/'9ED', 'Dany Orizio').
card_number('hill giant'/'9ED', '197').
card_flavor_text('hill giant'/'9ED', 'Fortunately, hill giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'9ED', '83120').

card_in_set('hollow dogs', '9ED').
card_original_type('hollow dogs'/'9ED', 'Creature — Hound').
card_original_text('hollow dogs'/'9ED', 'Whenever Hollow Dogs attacks, it gets +2/+0 until end of turn.').
card_image_name('hollow dogs'/'9ED', 'hollow dogs').
card_uid('hollow dogs'/'9ED', '9ED:Hollow Dogs:hollow dogs').
card_rarity('hollow dogs'/'9ED', 'Common').
card_artist('hollow dogs'/'9ED', 'Jeff Miracola').
card_number('hollow dogs'/'9ED', '139').
card_flavor_text('hollow dogs'/'9ED', 'A hollow dog is never empty. It is filled with thirst for the hunt.').
card_multiverse_id('hollow dogs'/'9ED', '83433').

card_in_set('holy day', '9ED').
card_original_type('holy day'/'9ED', 'Instant').
card_original_text('holy day'/'9ED', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('holy day'/'9ED', 'holy day').
card_uid('holy day'/'9ED', '9ED:Holy Day:holy day').
card_rarity('holy day'/'9ED', 'Common').
card_artist('holy day'/'9ED', 'Pete Venters').
card_number('holy day'/'9ED', '18').
card_flavor_text('holy day'/'9ED', '\"The day of Spirits; my soul\'s calm retreat / Which none disturb!\"\n—Henry Vaughan, \"The Night\"').
card_multiverse_id('holy day'/'9ED', '83121').

card_in_set('holy strength', '9ED').
card_original_type('holy strength'/'9ED', 'Enchantment — Aura').
card_original_text('holy strength'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +1/+2.').
card_image_name('holy strength'/'9ED', 'holy strength').
card_uid('holy strength'/'9ED', '9ED:Holy Strength:holy strength').
card_rarity('holy strength'/'9ED', 'Common').
card_artist('holy strength'/'9ED', 'Scott M. Fischer').
card_number('holy strength'/'9ED', '19').
card_flavor_text('holy strength'/'9ED', 'Such power protects the body with the strength of the soul.').
card_multiverse_id('holy strength'/'9ED', '83122').

card_in_set('honor guard', '9ED').
card_original_type('honor guard'/'9ED', 'Creature — Human Soldier').
card_original_text('honor guard'/'9ED', '{W}: Honor Guard gets +0/+1 until end of turn.').
card_image_name('honor guard'/'9ED', 'honor guard').
card_uid('honor guard'/'9ED', '9ED:Honor Guard:honor guard').
card_rarity('honor guard'/'9ED', 'Common').
card_artist('honor guard'/'9ED', 'Mark Zug').
card_number('honor guard'/'9ED', '20').
card_flavor_text('honor guard'/'9ED', 'The strength of one. The courage of ten.').
card_multiverse_id('honor guard'/'9ED', '83123').

card_in_set('horned turtle', '9ED').
card_original_type('horned turtle'/'9ED', 'Creature — Turtle').
card_original_text('horned turtle'/'9ED', '').
card_image_name('horned turtle'/'9ED', 'horned turtle').
card_uid('horned turtle'/'9ED', '9ED:Horned Turtle:horned turtle').
card_rarity('horned turtle'/'9ED', 'Common').
card_artist('horned turtle'/'9ED', 'DiTerlizzi').
card_number('horned turtle'/'9ED', '81').
card_flavor_text('horned turtle'/'9ED', 'It doesn\'t like having visitors . . . unless it\'s having them for lunch.').
card_multiverse_id('horned turtle'/'9ED', '83125').

card_in_set('horror of horrors', '9ED').
card_original_type('horror of horrors'/'9ED', 'Enchantment').
card_original_text('horror of horrors'/'9ED', 'Sacrifice a Swamp: Regenerate target black creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('horror of horrors'/'9ED', 'horror of horrors').
card_uid('horror of horrors'/'9ED', '9ED:Horror of Horrors:horror of horrors').
card_rarity('horror of horrors'/'9ED', 'Uncommon').
card_artist('horror of horrors'/'9ED', 'Simon Bisley').
card_number('horror of horrors'/'9ED', '140').
card_multiverse_id('horror of horrors'/'9ED', '83353').

card_in_set('howling mine', '9ED').
card_original_type('howling mine'/'9ED', 'Artifact').
card_original_text('howling mine'/'9ED', 'At the beginning of each player\'s draw step, if Howling Mine is untapped, that player draws a card.').
card_image_name('howling mine'/'9ED', 'howling mine').
card_uid('howling mine'/'9ED', '9ED:Howling Mine:howling mine').
card_rarity('howling mine'/'9ED', 'Rare').
card_artist('howling mine'/'9ED', 'Dana Knutson').
card_number('howling mine'/'9ED', '298').
card_flavor_text('howling mine'/'9ED', 'Legend has it that the mine howls out the last words of those who died inside.').
card_multiverse_id('howling mine'/'9ED', '83126').

card_in_set('hunted wumpus', '9ED').
card_original_type('hunted wumpus'/'9ED', 'Creature — Beast').
card_original_text('hunted wumpus'/'9ED', 'When Hunted Wumpus comes into play, each other player may put a creature card from his or her hand into play.').
card_image_name('hunted wumpus'/'9ED', 'hunted wumpus').
card_uid('hunted wumpus'/'9ED', '9ED:Hunted Wumpus:hunted wumpus').
card_rarity('hunted wumpus'/'9ED', 'Uncommon').
card_artist('hunted wumpus'/'9ED', 'Thomas M. Baxa').
card_number('hunted wumpus'/'9ED', '248').
card_flavor_text('hunted wumpus'/'9ED', 'Just one can feed a dozen people for a month.').
card_multiverse_id('hunted wumpus'/'9ED', '83128').

card_in_set('hypnotic specter', '9ED').
card_original_type('hypnotic specter'/'9ED', 'Creature — Specter').
card_original_text('hypnotic specter'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever Hypnotic Specter deals damage to an opponent, that player discards a card at random.').
card_image_name('hypnotic specter'/'9ED', 'hypnotic specter').
card_uid('hypnotic specter'/'9ED', '9ED:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'9ED', 'Rare').
card_artist('hypnotic specter'/'9ED', 'Greg Staples').
card_number('hypnotic specter'/'9ED', '141').
card_multiverse_id('hypnotic specter'/'9ED', '83354').

card_in_set('icy manipulator', '9ED').
card_original_type('icy manipulator'/'9ED', 'Artifact').
card_original_text('icy manipulator'/'9ED', '{1}, {T}: Tap target artifact, creature, or land.').
card_image_name('icy manipulator'/'9ED', 'icy manipulator').
card_uid('icy manipulator'/'9ED', '9ED:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'9ED', 'Uncommon').
card_artist('icy manipulator'/'9ED', 'Matt Cavotta').
card_number('icy manipulator'/'9ED', '299').
card_flavor_text('icy manipulator'/'9ED', 'In fire there is the spark of chaos and destruction, the seed of life. In ice there is perfect tranquility, perfect order, and the silence of death.').
card_multiverse_id('icy manipulator'/'9ED', '83415').

card_in_set('imaginary pet', '9ED').
card_original_type('imaginary pet'/'9ED', 'Creature — Illusion').
card_original_text('imaginary pet'/'9ED', 'At the beginning of your upkeep, if you have a card in hand, return Imaginary Pet to its owner\'s hand.').
card_image_name('imaginary pet'/'9ED', 'imaginary pet').
card_uid('imaginary pet'/'9ED', '9ED:Imaginary Pet:imaginary pet').
card_rarity('imaginary pet'/'9ED', 'Rare').
card_artist('imaginary pet'/'9ED', 'Christopher Rush').
card_number('imaginary pet'/'9ED', '82').
card_flavor_text('imaginary pet'/'9ED', '\"It followed me home. Can I keep it?\"').
card_multiverse_id('imaginary pet'/'9ED', '84612').

card_in_set('index', '9ED').
card_original_type('index'/'9ED', 'Sorcery').
card_original_text('index'/'9ED', 'Look at the top five cards of your library, then put them back in any order.').
card_image_name('index'/'9ED', 'index').
card_uid('index'/'9ED', '9ED:Index:index').
card_rarity('index'/'9ED', 'Common').
card_artist('index'/'9ED', 'Kev Walker').
card_number('index'/'9ED', 'S5').
card_flavor_text('index'/'9ED', '\"Let\'s see . . . Mercadia, mercenary, merfolk . . . you know, I really need a better filing system.\"').
card_multiverse_id('index'/'9ED', '94912').

card_in_set('infantry veteran', '9ED').
card_original_type('infantry veteran'/'9ED', 'Creature — Human Soldier').
card_original_text('infantry veteran'/'9ED', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_image_name('infantry veteran'/'9ED', 'infantry veteran').
card_uid('infantry veteran'/'9ED', '9ED:Infantry Veteran:infantry veteran').
card_rarity('infantry veteran'/'9ED', 'Common').
card_artist('infantry veteran'/'9ED', 'Christopher Rush').
card_number('infantry veteran'/'9ED', '21').
card_flavor_text('infantry veteran'/'9ED', '\"This scar? From a zombie\'s teeth. This scar? From a barbarian\'s axe. That scar? I don\'t talk about that one.\"').
card_multiverse_id('infantry veteran'/'9ED', '82978').

card_in_set('inspirit', '9ED').
card_original_type('inspirit'/'9ED', 'Instant').
card_original_text('inspirit'/'9ED', 'Untap target creature. It gets +2/+4 until end of turn.').
card_image_name('inspirit'/'9ED', 'inspirit').
card_uid('inspirit'/'9ED', '9ED:Inspirit:inspirit').
card_rarity('inspirit'/'9ED', 'Uncommon').
card_artist('inspirit'/'9ED', 'Simon Bisley').
card_number('inspirit'/'9ED', '22').
card_flavor_text('inspirit'/'9ED', '\"The hour of your redemption is here. . . . Rally to me. . . . rise and strike. Strike at every favorable opportunity. For your homes and hearths, strike!\"\n—General Douglas MacArthur,\nto the people of the Philippines').
card_multiverse_id('inspirit'/'9ED', '84552').

card_in_set('island', '9ED').
card_original_type('island'/'9ED', 'Basic Land — Island').
card_original_text('island'/'9ED', 'U').
card_image_name('island'/'9ED', 'island1').
card_uid('island'/'9ED', '9ED:Island:island1').
card_rarity('island'/'9ED', 'Basic Land').
card_artist('island'/'9ED', 'John Avon').
card_number('island'/'9ED', '335').
card_multiverse_id('island'/'9ED', '83136').

card_in_set('island', '9ED').
card_original_type('island'/'9ED', 'Basic Land — Island').
card_original_text('island'/'9ED', 'U').
card_image_name('island'/'9ED', 'island2').
card_uid('island'/'9ED', '9ED:Island:island2').
card_rarity('island'/'9ED', 'Basic Land').
card_artist('island'/'9ED', 'John Avon').
card_number('island'/'9ED', '336').
card_multiverse_id('island'/'9ED', '83137').

card_in_set('island', '9ED').
card_original_type('island'/'9ED', 'Basic Land — Island').
card_original_text('island'/'9ED', 'U').
card_image_name('island'/'9ED', 'island3').
card_uid('island'/'9ED', '9ED:Island:island3').
card_rarity('island'/'9ED', 'Basic Land').
card_artist('island'/'9ED', 'Tony Szczudlo').
card_number('island'/'9ED', '337').
card_multiverse_id('island'/'9ED', '83138').

card_in_set('island', '9ED').
card_original_type('island'/'9ED', 'Basic Land — Island').
card_original_text('island'/'9ED', 'U').
card_image_name('island'/'9ED', 'island4').
card_uid('island'/'9ED', '9ED:Island:island4').
card_rarity('island'/'9ED', 'Basic Land').
card_artist('island'/'9ED', 'Scott Bailey').
card_number('island'/'9ED', '338').
card_multiverse_id('island'/'9ED', '83139').

card_in_set('ivory mask', '9ED').
card_original_type('ivory mask'/'9ED', 'Enchantment').
card_original_text('ivory mask'/'9ED', 'You can\'t be the target of spells or abilities.').
card_image_name('ivory mask'/'9ED', 'ivory mask').
card_uid('ivory mask'/'9ED', '9ED:Ivory Mask:ivory mask').
card_rarity('ivory mask'/'9ED', 'Rare').
card_artist('ivory mask'/'9ED', 'Glen Angus').
card_number('ivory mask'/'9ED', '23').
card_flavor_text('ivory mask'/'9ED', 'Made from its wearer\'s hopes, the mask ensures that those hopes will be fulfilled.').
card_multiverse_id('ivory mask'/'9ED', '83413').

card_in_set('jade statue', '9ED').
card_original_type('jade statue'/'9ED', 'Artifact').
card_original_text('jade statue'/'9ED', '{2}: Jade Statue becomes a 3/6 artifact creature until end of combat. Play this ability only during combat.').
card_image_name('jade statue'/'9ED', 'jade statue').
card_uid('jade statue'/'9ED', '9ED:Jade Statue:jade statue').
card_rarity('jade statue'/'9ED', 'Rare').
card_artist('jade statue'/'9ED', 'Jim Pavelec').
card_number('jade statue'/'9ED', '300').
card_flavor_text('jade statue'/'9ED', '\"The paradox of jade statues is that their victims never really expect attack, but they are never completely surprised either.\"\n—Kabel, Tolarian archivist').
card_multiverse_id('jade statue'/'9ED', '83350').

card_in_set('jester\'s cap', '9ED').
card_original_type('jester\'s cap'/'9ED', 'Artifact').
card_original_text('jester\'s cap'/'9ED', '{2}, {T}, Sacrifice Jester\'s Cap: Search target player\'s library for three cards and remove them from the game. Then that player shuffles his or her library.').
card_image_name('jester\'s cap'/'9ED', 'jester\'s cap').
card_uid('jester\'s cap'/'9ED', '9ED:Jester\'s Cap:jester\'s cap').
card_rarity('jester\'s cap'/'9ED', 'Rare').
card_artist('jester\'s cap'/'9ED', 'D. Alexander Gregory').
card_number('jester\'s cap'/'9ED', '301').
card_flavor_text('jester\'s cap'/'9ED', '\"Know your foes\' strengths as well as their weaknesses.\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('jester\'s cap'/'9ED', '84595').

card_in_set('kami of old stone', '9ED').
card_original_type('kami of old stone'/'9ED', 'Creature — Spirit').
card_original_text('kami of old stone'/'9ED', '').
card_image_name('kami of old stone'/'9ED', 'kami of old stone').
card_uid('kami of old stone'/'9ED', '9ED:Kami of Old Stone:kami of old stone').
card_rarity('kami of old stone'/'9ED', 'Uncommon').
card_artist('kami of old stone'/'9ED', 'Stephen Tappin').
card_number('kami of old stone'/'9ED', '24').
card_flavor_text('kami of old stone'/'9ED', '\"There was a wall here. Now it is dust. A tower rose here. Now it is fallen. An army fought here. Now it is dead. A spirit was here. It is all that remains.\"\n—Snow-Fur, kitsune poet').
card_multiverse_id('kami of old stone'/'9ED', '83461').

card_in_set('karplusan forest', '9ED').
card_original_type('karplusan forest'/'9ED', 'Land').
card_original_text('karplusan forest'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Karplusan Forest deals 1 damage to you.').
card_image_name('karplusan forest'/'9ED', 'karplusan forest').
card_uid('karplusan forest'/'9ED', '9ED:Karplusan Forest:karplusan forest').
card_rarity('karplusan forest'/'9ED', 'Rare').
card_artist('karplusan forest'/'9ED', 'John Avon').
card_number('karplusan forest'/'9ED', '321').
card_multiverse_id('karplusan forest'/'9ED', '84559').

card_in_set('karplusan yeti', '9ED').
card_original_type('karplusan yeti'/'9ED', 'Creature — Yeti').
card_original_text('karplusan yeti'/'9ED', '{T}: Karplusan Yeti deals damage equal to its power to target creature. That creature deals damage equal to its power to Karplusan Yeti.').
card_image_name('karplusan yeti'/'9ED', 'karplusan yeti').
card_uid('karplusan yeti'/'9ED', '9ED:Karplusan Yeti:karplusan yeti').
card_rarity('karplusan yeti'/'9ED', 'Rare').
card_artist('karplusan yeti'/'9ED', 'Wayne England').
card_number('karplusan yeti'/'9ED', '198').
card_multiverse_id('karplusan yeti'/'9ED', '82955').

card_in_set('kavu climber', '9ED').
card_original_type('kavu climber'/'9ED', 'Creature — Kavu').
card_original_text('kavu climber'/'9ED', 'When Kavu Climber comes into play, draw a card.').
card_image_name('kavu climber'/'9ED', 'kavu climber').
card_uid('kavu climber'/'9ED', '9ED:Kavu Climber:kavu climber').
card_rarity('kavu climber'/'9ED', 'Common').
card_artist('kavu climber'/'9ED', 'Rob Alexander').
card_number('kavu climber'/'9ED', '249').
card_flavor_text('kavu climber'/'9ED', '\"There was a crack of branches, a rustle of leaves, then a tremendous roar. Our party had no chance as death descended from above.\"\n—Taseen, elvish bard').
card_multiverse_id('kavu climber'/'9ED', '83175').

card_in_set('king cheetah', '9ED').
card_original_type('king cheetah'/'9ED', 'Creature — Cat').
card_original_text('king cheetah'/'9ED', 'You may play King Cheetah any time you could play an instant.').
card_image_name('king cheetah'/'9ED', 'king cheetah').
card_uid('king cheetah'/'9ED', '9ED:King Cheetah:king cheetah').
card_rarity('king cheetah'/'9ED', 'Uncommon').
card_artist('king cheetah'/'9ED', 'Terese Nielsen').
card_number('king cheetah'/'9ED', '250').
card_flavor_text('king cheetah'/'9ED', 'If you find yourself and a friend being chased by a king cheetah, you have but one chance: Trip your friend.\n—Suq\'Ata wisdom').
card_multiverse_id('king cheetah'/'9ED', '83440').

card_in_set('kird ape', '9ED').
card_original_type('kird ape'/'9ED', 'Creature — Ape').
card_original_text('kird ape'/'9ED', 'Kird Ape gets +1/+2 as long as you control a Forest.').
card_image_name('kird ape'/'9ED', 'kird ape').
card_uid('kird ape'/'9ED', '9ED:Kird Ape:kird ape').
card_rarity('kird ape'/'9ED', 'Uncommon').
card_artist('kird ape'/'9ED', 'Terese Nielsen').
card_number('kird ape'/'9ED', '199').
card_flavor_text('kird ape'/'9ED', 'It puts the \"fur\" in \"fury.\"').
card_multiverse_id('kird ape'/'9ED', '83346').

card_in_set('kraken\'s eye', '9ED').
card_original_type('kraken\'s eye'/'9ED', 'Artifact').
card_original_text('kraken\'s eye'/'9ED', 'Whenever a player plays a blue spell, you may gain 1 life.').
card_image_name('kraken\'s eye'/'9ED', 'kraken\'s eye').
card_uid('kraken\'s eye'/'9ED', '9ED:Kraken\'s Eye:kraken\'s eye').
card_rarity('kraken\'s eye'/'9ED', 'Uncommon').
card_artist('kraken\'s eye'/'9ED', 'Alan Pollack').
card_number('kraken\'s eye'/'9ED', '302').
card_flavor_text('kraken\'s eye'/'9ED', 'Bright as a mirror, dark as the sea.').
card_multiverse_id('kraken\'s eye'/'9ED', '83493').

card_in_set('lava axe', '9ED').
card_original_type('lava axe'/'9ED', 'Sorcery').
card_original_text('lava axe'/'9ED', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'9ED', 'lava axe').
card_uid('lava axe'/'9ED', '9ED:Lava Axe:lava axe').
card_rarity('lava axe'/'9ED', 'Common').
card_artist('lava axe'/'9ED', 'Brian Snõddy').
card_number('lava axe'/'9ED', '200').
card_flavor_text('lava axe'/'9ED', '\"Catch!\"').
card_multiverse_id('lava axe'/'9ED', '83145').

card_in_set('leonin skyhunter', '9ED').
card_original_type('leonin skyhunter'/'9ED', 'Creature — Cat Knight').
card_original_text('leonin skyhunter'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('leonin skyhunter'/'9ED', 'leonin skyhunter').
card_uid('leonin skyhunter'/'9ED', '9ED:Leonin Skyhunter:leonin skyhunter').
card_rarity('leonin skyhunter'/'9ED', 'Uncommon').
card_artist('leonin skyhunter'/'9ED', 'Kev Walker').
card_number('leonin skyhunter'/'9ED', '25').
card_flavor_text('leonin skyhunter'/'9ED', 'The skyhunters were born when the first leonin gazed at the heavens and wished to hunt the birds overhead.').
card_multiverse_id('leonin skyhunter'/'9ED', '83416').

card_in_set('levitation', '9ED').
card_original_type('levitation'/'9ED', 'Enchantment').
card_original_text('levitation'/'9ED', 'Creatures you control have flying. (They can\'t be blocked except by creatures with flying.)').
card_image_name('levitation'/'9ED', 'levitation').
card_uid('levitation'/'9ED', '9ED:Levitation:levitation').
card_rarity('levitation'/'9ED', 'Uncommon').
card_artist('levitation'/'9ED', 'Heather Hudson').
card_number('levitation'/'9ED', '83').
card_flavor_text('levitation'/'9ED', 'Barrin\'s pride in his apprentice was diminished somewhat when he had to get the others back down.').
card_multiverse_id('levitation'/'9ED', '84503').

card_in_set('ley druid', '9ED').
card_original_type('ley druid'/'9ED', 'Creature — Human Druid').
card_original_text('ley druid'/'9ED', '{T}: Untap target land.').
card_image_name('ley druid'/'9ED', 'ley druid').
card_uid('ley druid'/'9ED', '9ED:Ley Druid:ley druid').
card_rarity('ley druid'/'9ED', 'Uncommon').
card_artist('ley druid'/'9ED', 'Edward P. Beard, Jr.').
card_number('ley druid'/'9ED', '251').
card_flavor_text('ley druid'/'9ED', 'After years of training, the druid becomes one with nature, drawing power from the land and returning it when needed.').
card_multiverse_id('ley druid'/'9ED', '83099').

card_in_set('lightning elemental', '9ED').
card_original_type('lightning elemental'/'9ED', 'Creature — Elemental').
card_original_text('lightning elemental'/'9ED', 'Haste (This creature may attack the turn it comes under your control.)').
card_image_name('lightning elemental'/'9ED', 'lightning elemental').
card_uid('lightning elemental'/'9ED', '9ED:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'9ED', 'Common').
card_artist('lightning elemental'/'9ED', 'Kev Walker').
card_number('lightning elemental'/'9ED', '201').
card_flavor_text('lightning elemental'/'9ED', '\"A flash of the lightning, a break of the wave,\nHe passes from life to his rest in the grave.\"\n—William Knox, \"Mortality\"').
card_multiverse_id('lightning elemental'/'9ED', '83150').

card_in_set('llanowar behemoth', '9ED').
card_original_type('llanowar behemoth'/'9ED', 'Creature — Elemental').
card_original_text('llanowar behemoth'/'9ED', 'Tap an untapped creature you control: Llanowar Behemoth gets +1/+1 until end of turn.').
card_image_name('llanowar behemoth'/'9ED', 'llanowar behemoth').
card_uid('llanowar behemoth'/'9ED', '9ED:Llanowar Behemoth:llanowar behemoth').
card_rarity('llanowar behemoth'/'9ED', 'Uncommon').
card_artist('llanowar behemoth'/'9ED', 'Kev Walker').
card_number('llanowar behemoth'/'9ED', '252').
card_flavor_text('llanowar behemoth'/'9ED', '\"Most people can\'t build decent weapons out of stone or steel. Trust the elves to do it with only mud and vines.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('llanowar behemoth'/'9ED', '83152').

card_in_set('llanowar elves', '9ED').
card_original_type('llanowar elves'/'9ED', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'9ED', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'9ED', 'llanowar elves').
card_uid('llanowar elves'/'9ED', '9ED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'9ED', 'Common').
card_artist('llanowar elves'/'9ED', 'Kev Walker').
card_number('llanowar elves'/'9ED', '253').
card_flavor_text('llanowar elves'/'9ED', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'9ED', '83515').

card_in_set('llanowar wastes', '9ED').
card_original_type('llanowar wastes'/'9ED', 'Land').
card_original_text('llanowar wastes'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Llanowar Wastes deals 1 damage to you.').
card_image_name('llanowar wastes'/'9ED', 'llanowar wastes').
card_uid('llanowar wastes'/'9ED', '9ED:Llanowar Wastes:llanowar wastes').
card_rarity('llanowar wastes'/'9ED', 'Rare').
card_artist('llanowar wastes'/'9ED', 'Rob Alexander').
card_number('llanowar wastes'/'9ED', '322').
card_multiverse_id('llanowar wastes'/'9ED', '84440').

card_in_set('looming shade', '9ED').
card_original_type('looming shade'/'9ED', 'Creature — Shade').
card_original_text('looming shade'/'9ED', '{B}: Looming Shade gets +1/+1 until end of turn.').
card_image_name('looming shade'/'9ED', 'looming shade').
card_uid('looming shade'/'9ED', '9ED:Looming Shade:looming shade').
card_rarity('looming shade'/'9ED', 'Common').
card_artist('looming shade'/'9ED', 'Kev Walker').
card_number('looming shade'/'9ED', '142').
card_flavor_text('looming shade'/'9ED', 'On darkness feeding, in nightmares breeding.').
card_multiverse_id('looming shade'/'9ED', '83154').

card_in_set('lord of the undead', '9ED').
card_original_type('lord of the undead'/'9ED', 'Creature — Zombie Lord').
card_original_text('lord of the undead'/'9ED', 'Other Zombies get +1/+1.\n{1}{B}, {T}: Return target Zombie card from your graveyard to your hand.').
card_image_name('lord of the undead'/'9ED', 'lord of the undead').
card_uid('lord of the undead'/'9ED', '9ED:Lord of the Undead:lord of the undead').
card_rarity('lord of the undead'/'9ED', 'Rare').
card_artist('lord of the undead'/'9ED', 'Brom').
card_number('lord of the undead'/'9ED', '143').
card_flavor_text('lord of the undead'/'9ED', '\"I confer with Death itself. What could you possibly do to injure me?\"').
card_multiverse_id('lord of the undead'/'9ED', '83155').

card_in_set('loxodon warhammer', '9ED').
card_original_type('loxodon warhammer'/'9ED', 'Artifact — Equipment').
card_original_text('loxodon warhammer'/'9ED', 'Equipped creature gets +3/+0, has trample, and has \"Whenever this creature deals damage, you gain that much life.\"\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('loxodon warhammer'/'9ED', 'loxodon warhammer').
card_uid('loxodon warhammer'/'9ED', '9ED:Loxodon Warhammer:loxodon warhammer').
card_rarity('loxodon warhammer'/'9ED', 'Rare').
card_artist('loxodon warhammer'/'9ED', 'Jeremy Jarvis').
card_number('loxodon warhammer'/'9ED', '303').
card_multiverse_id('loxodon warhammer'/'9ED', '83435').

card_in_set('lumengrid warden', '9ED').
card_original_type('lumengrid warden'/'9ED', 'Creature — Human Wizard').
card_original_text('lumengrid warden'/'9ED', '').
card_image_name('lumengrid warden'/'9ED', 'lumengrid warden').
card_uid('lumengrid warden'/'9ED', '9ED:Lumengrid Warden:lumengrid warden').
card_rarity('lumengrid warden'/'9ED', 'Common').
card_artist('lumengrid warden'/'9ED', 'Francis Tsai').
card_number('lumengrid warden'/'9ED', '84').
card_flavor_text('lumengrid warden'/'9ED', 'His peers derive knowledge from ancient tomes locked within dark spires. He finds knowledge within the solitude of a lifetime spent watching the Quicksilver Sea.').
card_multiverse_id('lumengrid warden'/'9ED', '82968').

card_in_set('magnivore', '9ED').
card_original_type('magnivore'/'9ED', 'Creature — Lhurgoyf').
card_original_text('magnivore'/'9ED', 'Haste (This creature may attack the turn it comes under your control.)\nMagnivore\'s power and toughness are each equal to the number of sorcery cards in all graveyards.').
card_image_name('magnivore'/'9ED', 'magnivore').
card_uid('magnivore'/'9ED', '9ED:Magnivore:magnivore').
card_rarity('magnivore'/'9ED', 'Rare').
card_artist('magnivore'/'9ED', 'Carl Critchlow').
card_number('magnivore'/'9ED', '202').
card_multiverse_id('magnivore'/'9ED', '83448').

card_in_set('mahamoti djinn', '9ED').
card_original_type('mahamoti djinn'/'9ED', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('mahamoti djinn'/'9ED', 'mahamoti djinn').
card_uid('mahamoti djinn'/'9ED', '9ED:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'9ED', 'Rare').
card_artist('mahamoti djinn'/'9ED', 'Greg Staples').
card_number('mahamoti djinn'/'9ED', '85').
card_flavor_text('mahamoti djinn'/'9ED', 'Of royal blood among the spirits of the air, the Mahamoti djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'9ED', '83158').

card_in_set('mana clash', '9ED').
card_original_type('mana clash'/'9ED', 'Sorcery').
card_original_text('mana clash'/'9ED', 'You and target opponent each flip a coin. Mana Clash deals 1 damage to each player whose coin comes up tails. Repeat this process until both players\' coins come up heads on the same flip.').
card_image_name('mana clash'/'9ED', 'mana clash').
card_uid('mana clash'/'9ED', '9ED:Mana Clash:mana clash').
card_rarity('mana clash'/'9ED', 'Rare').
card_artist('mana clash'/'9ED', 'Ron Spencer').
card_number('mana clash'/'9ED', '203').
card_multiverse_id('mana clash'/'9ED', '83159').

card_in_set('mana leak', '9ED').
card_original_type('mana leak'/'9ED', 'Instant').
card_original_text('mana leak'/'9ED', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'9ED', 'mana leak').
card_uid('mana leak'/'9ED', '9ED:Mana Leak:mana leak').
card_rarity('mana leak'/'9ED', 'Common').
card_artist('mana leak'/'9ED', 'Christopher Rush').
card_number('mana leak'/'9ED', '86').
card_flavor_text('mana leak'/'9ED', 'The fatal flaw in every plan is the assumption that you know more than your enemy.').
card_multiverse_id('mana leak'/'9ED', '83160').

card_in_set('marble titan', '9ED').
card_original_type('marble titan'/'9ED', 'Creature — Giant').
card_original_text('marble titan'/'9ED', 'Creatures with power 3 or greater don\'t untap during their controllers\' untap steps.').
card_image_name('marble titan'/'9ED', 'marble titan').
card_uid('marble titan'/'9ED', '9ED:Marble Titan:marble titan').
card_rarity('marble titan'/'9ED', 'Rare').
card_artist('marble titan'/'9ED', 'Brom').
card_number('marble titan'/'9ED', '26').
card_flavor_text('marble titan'/'9ED', 'It entered the temple with all the pomp and decorum of a rock hitting a goblin\'s skull.').
card_multiverse_id('marble titan'/'9ED', '84533').

card_in_set('maro', '9ED').
card_original_type('maro'/'9ED', 'Creature — Elemental').
card_original_text('maro'/'9ED', 'Maro\'s power and toughness are each equal to the number of cards in your hand.').
card_image_name('maro'/'9ED', 'maro').
card_uid('maro'/'9ED', '9ED:Maro:maro').
card_rarity('maro'/'9ED', 'Rare').
card_artist('maro'/'9ED', 'Stuart Griffin').
card_number('maro'/'9ED', '254').
card_flavor_text('maro'/'9ED', 'No two see the same Maro.').
card_multiverse_id('maro'/'9ED', '83161').

card_in_set('master decoy', '9ED').
card_original_type('master decoy'/'9ED', 'Creature — Human Soldier').
card_original_text('master decoy'/'9ED', '{W}, {T}: Tap target creature.').
card_image_name('master decoy'/'9ED', 'master decoy').
card_uid('master decoy'/'9ED', '9ED:Master Decoy:master decoy').
card_rarity('master decoy'/'9ED', 'Common').
card_artist('master decoy'/'9ED', 'Ben Thompson').
card_number('master decoy'/'9ED', '27').
card_flavor_text('master decoy'/'9ED', 'A skilled decoy can throw your enemies off your trail. A master decoy can survive to do it again.').
card_multiverse_id('master decoy'/'9ED', '83162').

card_in_set('master healer', '9ED').
card_original_type('master healer'/'9ED', 'Creature — Human Cleric').
card_original_text('master healer'/'9ED', '{T}: Prevent the next 4 damage that would be dealt to target creature or player this turn.').
card_image_name('master healer'/'9ED', 'master healer').
card_uid('master healer'/'9ED', '9ED:Master Healer:master healer').
card_rarity('master healer'/'9ED', 'Rare').
card_artist('master healer'/'9ED', 'Adam Rex').
card_number('master healer'/'9ED', '28').
card_flavor_text('master healer'/'9ED', 'Behind his eyes is the pain of every soldier his hands have healed.').
card_multiverse_id('master healer'/'9ED', '83163').

card_in_set('megrim', '9ED').
card_original_type('megrim'/'9ED', 'Enchantment').
card_original_text('megrim'/'9ED', 'Whenever an opponent discards a card, Megrim deals 2 damage to that player.').
card_image_name('megrim'/'9ED', 'megrim').
card_uid('megrim'/'9ED', '9ED:Megrim:megrim').
card_rarity('megrim'/'9ED', 'Uncommon').
card_artist('megrim'/'9ED', 'Nick Percival').
card_number('megrim'/'9ED', '144').
card_flavor_text('megrim'/'9ED', '\"You can run from your pain, but you will tire before it does.\"\n—Gerrard of the Weatherlight').
card_multiverse_id('megrim'/'9ED', '83164').

card_in_set('mending hands', '9ED').
card_original_type('mending hands'/'9ED', 'Instant').
card_original_text('mending hands'/'9ED', 'Prevent the next 4 damage that would be dealt to target creature or player this turn.').
card_image_name('mending hands'/'9ED', 'mending hands').
card_uid('mending hands'/'9ED', '9ED:Mending Hands:mending hands').
card_rarity('mending hands'/'9ED', 'Common').
card_artist('mending hands'/'9ED', 'Douglas Shuler').
card_number('mending hands'/'9ED', '29').
card_flavor_text('mending hands'/'9ED', '\"I can staunch their blood, mend their flesh, and knit their bones. But I cannot restore their hope.\"\n—Tender-Hand, kitsune healer').
card_multiverse_id('mending hands'/'9ED', '84671').

card_in_set('might of oaks', '9ED').
card_original_type('might of oaks'/'9ED', 'Instant').
card_original_text('might of oaks'/'9ED', 'Target creature gets +7/+7 until end of turn.').
card_image_name('might of oaks'/'9ED', 'might of oaks').
card_uid('might of oaks'/'9ED', '9ED:Might of Oaks:might of oaks').
card_rarity('might of oaks'/'9ED', 'Rare').
card_artist('might of oaks'/'9ED', 'Greg Staples').
card_number('might of oaks'/'9ED', '255').
card_flavor_text('might of oaks'/'9ED', '\"Guess where I\'m gonna plant this!\"').
card_multiverse_id('might of oaks'/'9ED', '83167').

card_in_set('millstone', '9ED').
card_original_type('millstone'/'9ED', 'Artifact').
card_original_text('millstone'/'9ED', '{2}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('millstone'/'9ED', 'millstone').
card_uid('millstone'/'9ED', '9ED:Millstone:millstone').
card_rarity('millstone'/'9ED', 'Rare').
card_artist('millstone'/'9ED', 'John Avon').
card_number('millstone'/'9ED', '304').
card_flavor_text('millstone'/'9ED', 'More than one mage has been driven insane by the sound of the millstone relentlessly grinding away.').
card_multiverse_id('millstone'/'9ED', '83168').

card_in_set('mind bend', '9ED').
card_original_type('mind bend'/'9ED', 'Instant').
card_original_text('mind bend'/'9ED', 'Change the text of target permanent by replacing all instances of one color word with another or one basic land type with another. (For example, you may change \"nonblack creature\" to \"nongreen creature\" or \"forestwalk\" to \"plainswalk.\" This effect doesn\'t end at end of turn.)').
card_image_name('mind bend'/'9ED', 'mind bend').
card_uid('mind bend'/'9ED', '9ED:Mind Bend:mind bend').
card_rarity('mind bend'/'9ED', 'Rare').
card_artist('mind bend'/'9ED', 'Tony Szczudlo').
card_number('mind bend'/'9ED', '87').
card_multiverse_id('mind bend'/'9ED', '83169').

card_in_set('mind rot', '9ED').
card_original_type('mind rot'/'9ED', 'Sorcery').
card_original_text('mind rot'/'9ED', 'Target player discards two cards.').
card_image_name('mind rot'/'9ED', 'mind rot').
card_uid('mind rot'/'9ED', '9ED:Mind Rot:mind rot').
card_rarity('mind rot'/'9ED', 'Common').
card_artist('mind rot'/'9ED', 'Steve Luke').
card_number('mind rot'/'9ED', '145').
card_flavor_text('mind rot'/'9ED', '\"The beauty of mental attacks is that your victims never remember them.\"\n—Volrath').
card_multiverse_id('mind rot'/'9ED', '83170').

card_in_set('mindslicer', '9ED').
card_original_type('mindslicer'/'9ED', 'Creature — Horror').
card_original_text('mindslicer'/'9ED', 'When Mindslicer is put into a graveyard from play, each player discards his or her hand.').
card_image_name('mindslicer'/'9ED', 'mindslicer').
card_uid('mindslicer'/'9ED', '9ED:Mindslicer:mindslicer').
card_rarity('mindslicer'/'9ED', 'Rare').
card_artist('mindslicer'/'9ED', 'Kev Walker').
card_number('mindslicer'/'9ED', '146').
card_flavor_text('mindslicer'/'9ED', 'It is the thing that goes bump in the night.').
card_multiverse_id('mindslicer'/'9ED', '84104').

card_in_set('mogg sentry', '9ED').
card_original_type('mogg sentry'/'9ED', 'Creature — Goblin Warrior').
card_original_text('mogg sentry'/'9ED', 'Whenever an opponent plays a spell, Mogg Sentry gets +2/+2 until end of turn.').
card_image_name('mogg sentry'/'9ED', 'mogg sentry').
card_uid('mogg sentry'/'9ED', '9ED:Mogg Sentry:mogg sentry').
card_rarity('mogg sentry'/'9ED', 'Rare').
card_artist('mogg sentry'/'9ED', 'Greg Staples').
card_number('mogg sentry'/'9ED', '204').
card_flavor_text('mogg sentry'/'9ED', 'Nothing makes a mogg madder than having to wake up.').
card_multiverse_id('mogg sentry'/'9ED', '83173').

card_in_set('mortivore', '9ED').
card_original_type('mortivore'/'9ED', 'Creature — Lhurgoyf').
card_original_text('mortivore'/'9ED', 'Mortivore\'s power and toughness are each equal to the number of creature cards in all graveyards.\n{B}: Regenerate Mortivore. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('mortivore'/'9ED', 'mortivore').
card_uid('mortivore'/'9ED', '9ED:Mortivore:mortivore').
card_rarity('mortivore'/'9ED', 'Rare').
card_artist('mortivore'/'9ED', 'Anthony S. Waters').
card_number('mortivore'/'9ED', '147').
card_multiverse_id('mortivore'/'9ED', '83464').

card_in_set('mountain', '9ED').
card_original_type('mountain'/'9ED', 'Basic Land — Mountain').
card_original_text('mountain'/'9ED', 'R').
card_image_name('mountain'/'9ED', 'mountain1').
card_uid('mountain'/'9ED', '9ED:Mountain:mountain1').
card_rarity('mountain'/'9ED', 'Basic Land').
card_artist('mountain'/'9ED', 'John Avon').
card_number('mountain'/'9ED', '343').
card_multiverse_id('mountain'/'9ED', '83176').

card_in_set('mountain', '9ED').
card_original_type('mountain'/'9ED', 'Basic Land — Mountain').
card_original_text('mountain'/'9ED', 'R').
card_image_name('mountain'/'9ED', 'mountain2').
card_uid('mountain'/'9ED', '9ED:Mountain:mountain2').
card_rarity('mountain'/'9ED', 'Basic Land').
card_artist('mountain'/'9ED', 'John Avon').
card_number('mountain'/'9ED', '344').
card_multiverse_id('mountain'/'9ED', '83177').

card_in_set('mountain', '9ED').
card_original_type('mountain'/'9ED', 'Basic Land — Mountain').
card_original_text('mountain'/'9ED', 'R').
card_image_name('mountain'/'9ED', 'mountain3').
card_uid('mountain'/'9ED', '9ED:Mountain:mountain3').
card_rarity('mountain'/'9ED', 'Basic Land').
card_artist('mountain'/'9ED', 'John Avon').
card_number('mountain'/'9ED', '345').
card_multiverse_id('mountain'/'9ED', '83178').

card_in_set('mountain', '9ED').
card_original_type('mountain'/'9ED', 'Basic Land — Mountain').
card_original_text('mountain'/'9ED', 'R').
card_image_name('mountain'/'9ED', 'mountain4').
card_uid('mountain'/'9ED', '9ED:Mountain:mountain4').
card_rarity('mountain'/'9ED', 'Basic Land').
card_artist('mountain'/'9ED', 'Rob Alexander').
card_number('mountain'/'9ED', '346').
card_multiverse_id('mountain'/'9ED', '83179').

card_in_set('nantuko husk', '9ED').
card_original_type('nantuko husk'/'9ED', 'Creature — Zombie Insect').
card_original_text('nantuko husk'/'9ED', 'Sacrifice a creature: Nantuko Husk gets +2/+2 until end of turn.').
card_image_name('nantuko husk'/'9ED', 'nantuko husk').
card_uid('nantuko husk'/'9ED', '9ED:Nantuko Husk:nantuko husk').
card_rarity('nantuko husk'/'9ED', 'Uncommon').
card_artist('nantuko husk'/'9ED', 'Carl Critchlow').
card_number('nantuko husk'/'9ED', '148').
card_flavor_text('nantuko husk'/'9ED', 'The soul sheds light, and death is its shadow. When the light dims, life and death embrace.\n—Nantuko teaching').
card_multiverse_id('nantuko husk'/'9ED', '84663').

card_in_set('natural affinity', '9ED').
card_original_type('natural affinity'/'9ED', 'Instant').
card_original_text('natural affinity'/'9ED', 'Until end of turn, all lands become 2/2 creatures that are still lands.').
card_image_name('natural affinity'/'9ED', 'natural affinity').
card_uid('natural affinity'/'9ED', '9ED:Natural Affinity:natural affinity').
card_rarity('natural affinity'/'9ED', 'Rare').
card_artist('natural affinity'/'9ED', 'Pete Venters').
card_number('natural affinity'/'9ED', '256').
card_flavor_text('natural affinity'/'9ED', 'As long as the dryads are a part of the forest, the forest is a part of the army.').
card_multiverse_id('natural affinity'/'9ED', '83182').

card_in_set('natural spring', '9ED').
card_original_type('natural spring'/'9ED', 'Sorcery').
card_original_text('natural spring'/'9ED', 'Target player gains 8 life.').
card_image_name('natural spring'/'9ED', 'natural spring').
card_uid('natural spring'/'9ED', '9ED:Natural Spring:natural spring').
card_rarity('natural spring'/'9ED', 'Common').
card_artist('natural spring'/'9ED', 'Janine Johnston').
card_number('natural spring'/'9ED', '257').
card_flavor_text('natural spring'/'9ED', 'The dying ask for water because they know it contains life.').
card_multiverse_id('natural spring'/'9ED', '84409').

card_in_set('naturalize', '9ED').
card_original_type('naturalize'/'9ED', 'Instant').
card_original_text('naturalize'/'9ED', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'9ED', 'naturalize').
card_uid('naturalize'/'9ED', '9ED:Naturalize:naturalize').
card_rarity('naturalize'/'9ED', 'Common').
card_artist('naturalize'/'9ED', 'Jeff Miracola').
card_number('naturalize'/'9ED', '258').
card_flavor_text('naturalize'/'9ED', '\"Well, it also makes a very nice flower pot. . . .\"\n—Arcum Dagsson, Soldevi machinist').
card_multiverse_id('naturalize'/'9ED', '83183').

card_in_set('needle storm', '9ED').
card_original_type('needle storm'/'9ED', 'Sorcery').
card_original_text('needle storm'/'9ED', 'Needle Storm deals 4 damage to each creature with flying.').
card_image_name('needle storm'/'9ED', 'needle storm').
card_uid('needle storm'/'9ED', '9ED:Needle Storm:needle storm').
card_rarity('needle storm'/'9ED', 'Uncommon').
card_artist('needle storm'/'9ED', 'Val Mayerik').
card_number('needle storm'/'9ED', '259').
card_flavor_text('needle storm'/'9ED', 'The rain is driven like nails.').
card_multiverse_id('needle storm'/'9ED', '84611').

card_in_set('nekrataal', '9ED').
card_original_type('nekrataal'/'9ED', 'Creature — Human Assassin').
card_original_text('nekrataal'/'9ED', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhen Nekrataal comes into play, destroy target nonartifact, nonblack creature. That creature can\'t be regenerated.').
card_image_name('nekrataal'/'9ED', 'nekrataal').
card_uid('nekrataal'/'9ED', '9ED:Nekrataal:nekrataal').
card_rarity('nekrataal'/'9ED', 'Uncommon').
card_artist('nekrataal'/'9ED', 'Adrian Smith').
card_number('nekrataal'/'9ED', '149').
card_multiverse_id('nekrataal'/'9ED', '83185').

card_in_set('nightmare', '9ED').
card_original_type('nightmare'/'9ED', 'Creature — Nightmare').
card_original_text('nightmare'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('nightmare'/'9ED', 'nightmare').
card_uid('nightmare'/'9ED', '9ED:Nightmare:nightmare').
card_rarity('nightmare'/'9ED', 'Rare').
card_artist('nightmare'/'9ED', 'Carl Critchlow').
card_number('nightmare'/'9ED', '150').
card_flavor_text('nightmare'/'9ED', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'9ED', '83186').

card_in_set('norwood ranger', '9ED').
card_original_type('norwood ranger'/'9ED', 'Creature — Elf Scout').
card_original_text('norwood ranger'/'9ED', '').
card_image_name('norwood ranger'/'9ED', 'norwood ranger').
card_uid('norwood ranger'/'9ED', '9ED:Norwood Ranger:norwood ranger').
card_rarity('norwood ranger'/'9ED', 'Common').
card_artist('norwood ranger'/'9ED', 'Ron Spencer').
card_number('norwood ranger'/'9ED', '260').
card_flavor_text('norwood ranger'/'9ED', 'The song of the forest is in perfect harmony. If a single note is out of place, the elves will find its source.').
card_multiverse_id('norwood ranger'/'9ED', '83188').

card_in_set('ogre taskmaster', '9ED').
card_original_type('ogre taskmaster'/'9ED', 'Creature — Ogre').
card_original_text('ogre taskmaster'/'9ED', 'Ogre Taskmaster can\'t block.').
card_image_name('ogre taskmaster'/'9ED', 'ogre taskmaster').
card_uid('ogre taskmaster'/'9ED', '9ED:Ogre Taskmaster:ogre taskmaster').
card_rarity('ogre taskmaster'/'9ED', 'Uncommon').
card_artist('ogre taskmaster'/'9ED', 'Dany Orizio').
card_number('ogre taskmaster'/'9ED', '205').
card_flavor_text('ogre taskmaster'/'9ED', 'Three little goblins, enjoying their brew. One bumped an ogre, and then there were two.').
card_multiverse_id('ogre taskmaster'/'9ED', '83190').

card_in_set('oracle\'s attendants', '9ED').
card_original_type('oracle\'s attendants'/'9ED', 'Creature — Human Soldier').
card_original_text('oracle\'s attendants'/'9ED', '{T}: All damage that would be dealt to target creature this turn by a source of your choice is dealt to Oracle\'s Attendants instead.').
card_image_name('oracle\'s attendants'/'9ED', 'oracle\'s attendants').
card_uid('oracle\'s attendants'/'9ED', '9ED:Oracle\'s Attendants:oracle\'s attendants').
card_rarity('oracle\'s attendants'/'9ED', 'Rare').
card_artist('oracle\'s attendants'/'9ED', 'Dany Orizio').
card_number('oracle\'s attendants'/'9ED', '30').
card_flavor_text('oracle\'s attendants'/'9ED', 'They follow the prophecy to each new prophet, guarding the future from the present.').
card_multiverse_id('oracle\'s attendants'/'9ED', '83192').

card_in_set('orcish artillery', '9ED').
card_original_type('orcish artillery'/'9ED', 'Creature — Orc Warrior').
card_original_text('orcish artillery'/'9ED', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'9ED', 'orcish artillery').
card_uid('orcish artillery'/'9ED', '9ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'9ED', 'Uncommon').
card_artist('orcish artillery'/'9ED', 'Jeff Miracola').
card_number('orcish artillery'/'9ED', '206').
card_flavor_text('orcish artillery'/'9ED', '\"So they want to kill my men? Well two can play at that game.\"\n—General Khurzog').
card_multiverse_id('orcish artillery'/'9ED', '83193').

card_in_set('order of the sacred bell', '9ED').
card_original_type('order of the sacred bell'/'9ED', 'Creature — Human Monk').
card_original_text('order of the sacred bell'/'9ED', '').
card_image_name('order of the sacred bell'/'9ED', 'order of the sacred bell').
card_uid('order of the sacred bell'/'9ED', '9ED:Order of the Sacred Bell:order of the sacred bell').
card_rarity('order of the sacred bell'/'9ED', 'Common').
card_artist('order of the sacred bell'/'9ED', 'Carl Critchlow').
card_number('order of the sacred bell'/'9ED', '261').
card_flavor_text('order of the sacred bell'/'9ED', '\"My brother, it may now be time to ring the bell and put out the call for aid.\"').
card_multiverse_id('order of the sacred bell'/'9ED', '82943').

card_in_set('ornithopter', '9ED').
card_original_type('ornithopter'/'9ED', 'Artifact Creature — Thopter').
card_original_text('ornithopter'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('ornithopter'/'9ED', 'ornithopter').
card_uid('ornithopter'/'9ED', '9ED:Ornithopter:ornithopter').
card_rarity('ornithopter'/'9ED', 'Uncommon').
card_artist('ornithopter'/'9ED', 'Dana Knutson').
card_number('ornithopter'/'9ED', '305').
card_flavor_text('ornithopter'/'9ED', '\"It has been my honor to improve on the Thran\'s original design. Perhaps history will remember me in some small part for my work.\"\n—Urza, in his apprenticeship').
card_multiverse_id('ornithopter'/'9ED', '95220').

card_in_set('overgrowth', '9ED').
card_original_type('overgrowth'/'9ED', 'Enchantment — Aura').
card_original_text('overgrowth'/'9ED', 'Enchant land (Target a land as you play this. This card comes into play attached to that land.)\nWhenever enchanted land is tapped for mana, its controller adds {G}{G} to his or her mana pool.').
card_image_name('overgrowth'/'9ED', 'overgrowth').
card_uid('overgrowth'/'9ED', '9ED:Overgrowth:overgrowth').
card_rarity('overgrowth'/'9ED', 'Common').
card_artist('overgrowth'/'9ED', 'Christopher Rush').
card_number('overgrowth'/'9ED', '262').
card_multiverse_id('overgrowth'/'9ED', '84685').

card_in_set('pacifism', '9ED').
card_original_type('pacifism'/'9ED', 'Enchantment — Aura').
card_original_text('pacifism'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'9ED', 'pacifism').
card_uid('pacifism'/'9ED', '9ED:Pacifism:pacifism').
card_rarity('pacifism'/'9ED', 'Common').
card_artist('pacifism'/'9ED', 'Robert Bliss').
card_number('pacifism'/'9ED', '31').
card_flavor_text('pacifism'/'9ED', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'9ED', '83195').

card_in_set('paladin en-vec', '9ED').
card_original_type('paladin en-vec'/'9ED', 'Creature — Human Knight').
card_original_text('paladin en-vec'/'9ED', 'First strike (This creature deals combat damage before creatures without first strike.) \nProtection from black, protection from red (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything black or red.)').
card_image_name('paladin en-vec'/'9ED', 'paladin en-vec').
card_uid('paladin en-vec'/'9ED', '9ED:Paladin en-Vec:paladin en-vec').
card_rarity('paladin en-vec'/'9ED', 'Rare').
card_artist('paladin en-vec'/'9ED', 'Simon Bisley').
card_number('paladin en-vec'/'9ED', '32').
card_multiverse_id('paladin en-vec'/'9ED', '82951').

card_in_set('panic attack', '9ED').
card_original_type('panic attack'/'9ED', 'Sorcery').
card_original_text('panic attack'/'9ED', 'Up to three target creatures can\'t block this turn.').
card_image_name('panic attack'/'9ED', 'panic attack').
card_uid('panic attack'/'9ED', '9ED:Panic Attack:panic attack').
card_rarity('panic attack'/'9ED', 'Common').
card_artist('panic attack'/'9ED', 'Mike Ploog').
card_number('panic attack'/'9ED', '207').
card_flavor_text('panic attack'/'9ED', 'Sometimes everyone decides to go for help at the same time.').
card_multiverse_id('panic attack'/'9ED', '83196').

card_in_set('peace of mind', '9ED').
card_original_type('peace of mind'/'9ED', 'Enchantment').
card_original_text('peace of mind'/'9ED', '{W}, Discard a card: You gain 3 life.').
card_image_name('peace of mind'/'9ED', 'peace of mind').
card_uid('peace of mind'/'9ED', '9ED:Peace of Mind:peace of mind').
card_rarity('peace of mind'/'9ED', 'Uncommon').
card_artist('peace of mind'/'9ED', 'Randy Elliott').
card_number('peace of mind'/'9ED', '33').
card_flavor_text('peace of mind'/'9ED', '\"What can a peasant get for free that an emperor can never buy?\"\n—Dal riddle').
card_multiverse_id('peace of mind'/'9ED', '84697').

card_in_set('pegasus charger', '9ED').
card_original_type('pegasus charger'/'9ED', 'Creature — Pegasus').
card_original_text('pegasus charger'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_image_name('pegasus charger'/'9ED', 'pegasus charger').
card_uid('pegasus charger'/'9ED', '9ED:Pegasus Charger:pegasus charger').
card_rarity('pegasus charger'/'9ED', 'Common').
card_artist('pegasus charger'/'9ED', 'Val Mayerik').
card_number('pegasus charger'/'9ED', '34').
card_multiverse_id('pegasus charger'/'9ED', '82967').

card_in_set('persecute', '9ED').
card_original_type('persecute'/'9ED', 'Sorcery').
card_original_text('persecute'/'9ED', 'Choose a color. Target player reveals his or her hand and discards all cards of that color.').
card_image_name('persecute'/'9ED', 'persecute').
card_uid('persecute'/'9ED', '9ED:Persecute:persecute').
card_rarity('persecute'/'9ED', 'Rare').
card_artist('persecute'/'9ED', 'D. Alexander Gregory').
card_number('persecute'/'9ED', '151').
card_flavor_text('persecute'/'9ED', '\"Of all the tyrannies on humane kind\nThe worst is that which persecutes the mind.\"\n—John Dryden,\n\"The Hind and the Panther\"').
card_multiverse_id('persecute'/'9ED', '83199').

card_in_set('phantom warrior', '9ED').
card_original_type('phantom warrior'/'9ED', 'Creature — Illusion').
card_original_text('phantom warrior'/'9ED', 'Phantom Warrior is unblockable.').
card_image_name('phantom warrior'/'9ED', 'phantom warrior').
card_uid('phantom warrior'/'9ED', '9ED:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'9ED', 'Uncommon').
card_artist('phantom warrior'/'9ED', 'Greg Staples').
card_number('phantom warrior'/'9ED', '88').
card_flavor_text('phantom warrior'/'9ED', '\"There are as many pillows of illusion as flakes in a snow-storm. We wake from one dream into another dream.\"\n—Ralph Waldo Emerson, \"Illusions\"').
card_multiverse_id('phantom warrior'/'9ED', '83200').

card_in_set('phyrexian arena', '9ED').
card_original_type('phyrexian arena'/'9ED', 'Enchantment').
card_original_text('phyrexian arena'/'9ED', 'At the beginning of your upkeep, you draw a card and you lose 1 life.').
card_image_name('phyrexian arena'/'9ED', 'phyrexian arena').
card_uid('phyrexian arena'/'9ED', '9ED:Phyrexian Arena:phyrexian arena').
card_rarity('phyrexian arena'/'9ED', 'Rare').
card_artist('phyrexian arena'/'9ED', 'Carl Critchlow').
card_number('phyrexian arena'/'9ED', '152').
card_flavor_text('phyrexian arena'/'9ED', 'A drop of humanity for a sea of power.').
card_multiverse_id('phyrexian arena'/'9ED', '83201').

card_in_set('phyrexian gargantua', '9ED').
card_original_type('phyrexian gargantua'/'9ED', 'Creature — Horror').
card_original_text('phyrexian gargantua'/'9ED', 'When Phyrexian Gargantua comes into play, you draw two cards and you lose 2 life.').
card_image_name('phyrexian gargantua'/'9ED', 'phyrexian gargantua').
card_uid('phyrexian gargantua'/'9ED', '9ED:Phyrexian Gargantua:phyrexian gargantua').
card_rarity('phyrexian gargantua'/'9ED', 'Uncommon').
card_artist('phyrexian gargantua'/'9ED', 'Carl Critchlow').
card_number('phyrexian gargantua'/'9ED', '153').
card_flavor_text('phyrexian gargantua'/'9ED', 'Other Phyrexians have nightmares about the gargantua.').
card_multiverse_id('phyrexian gargantua'/'9ED', '84393').

card_in_set('phyrexian hulk', '9ED').
card_original_type('phyrexian hulk'/'9ED', 'Artifact Creature — Golem').
card_original_text('phyrexian hulk'/'9ED', '').
card_image_name('phyrexian hulk'/'9ED', 'phyrexian hulk').
card_uid('phyrexian hulk'/'9ED', '9ED:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'9ED', 'Uncommon').
card_artist('phyrexian hulk'/'9ED', 'Matthew D. Wilson').
card_number('phyrexian hulk'/'9ED', '306').
card_flavor_text('phyrexian hulk'/'9ED', 'It doesn\'t think. It doesn\'t feel.\nIt doesn\'t laugh or cry.\nAll it does from dusk till dawn\nIs make the soldiers die.\n—Onean children\'s rhyme').
card_multiverse_id('phyrexian hulk'/'9ED', '83203').

card_in_set('plagiarize', '9ED').
card_original_type('plagiarize'/'9ED', 'Instant').
card_original_text('plagiarize'/'9ED', 'Until end of turn, if target player would draw a card, instead that player skips that draw and you draw a card.').
card_image_name('plagiarize'/'9ED', 'plagiarize').
card_uid('plagiarize'/'9ED', '9ED:Plagiarize:plagiarize').
card_rarity('plagiarize'/'9ED', 'Rare').
card_artist('plagiarize'/'9ED', 'Jeremy Jarvis').
card_number('plagiarize'/'9ED', '89').
card_flavor_text('plagiarize'/'9ED', '\"Are you thinking what I\'m thinking?\"').
card_multiverse_id('plagiarize'/'9ED', '84413').

card_in_set('plague beetle', '9ED').
card_original_type('plague beetle'/'9ED', 'Creature — Insect').
card_original_text('plague beetle'/'9ED', 'Swampwalk (This creature is unblockable as long as defending player controls a Swamp.)').
card_image_name('plague beetle'/'9ED', 'plague beetle').
card_uid('plague beetle'/'9ED', '9ED:Plague Beetle:plague beetle').
card_rarity('plague beetle'/'9ED', 'Common').
card_artist('plague beetle'/'9ED', 'Tom Fleming').
card_number('plague beetle'/'9ED', '154').
card_flavor_text('plague beetle'/'9ED', 'No one knows whether they were named for the disease they carry or for the speed at which they multiply.').
card_multiverse_id('plague beetle'/'9ED', '83205').

card_in_set('plague wind', '9ED').
card_original_type('plague wind'/'9ED', 'Sorcery').
card_original_text('plague wind'/'9ED', 'Destroy all creatures you don\'t control. They can\'t be regenerated.').
card_image_name('plague wind'/'9ED', 'plague wind').
card_uid('plague wind'/'9ED', '9ED:Plague Wind:plague wind').
card_rarity('plague wind'/'9ED', 'Rare').
card_artist('plague wind'/'9ED', 'Alan Pollack').
card_number('plague wind'/'9ED', '155').
card_flavor_text('plague wind'/'9ED', '\"The second wind of ascension is Reaver, slaying the unworthy.\"\n—Keld Triumphant').
card_multiverse_id('plague wind'/'9ED', '83206').

card_in_set('plains', '9ED').
card_original_type('plains'/'9ED', 'Basic Land — Plains').
card_original_text('plains'/'9ED', 'W').
card_image_name('plains'/'9ED', 'plains1').
card_uid('plains'/'9ED', '9ED:Plains:plains1').
card_rarity('plains'/'9ED', 'Basic Land').
card_artist('plains'/'9ED', 'John Avon').
card_number('plains'/'9ED', '331').
card_multiverse_id('plains'/'9ED', '83207').

card_in_set('plains', '9ED').
card_original_type('plains'/'9ED', 'Basic Land — Plains').
card_original_text('plains'/'9ED', 'W').
card_image_name('plains'/'9ED', 'plains2').
card_uid('plains'/'9ED', '9ED:Plains:plains2').
card_rarity('plains'/'9ED', 'Basic Land').
card_artist('plains'/'9ED', 'Matthew Mitchell').
card_number('plains'/'9ED', '332').
card_multiverse_id('plains'/'9ED', '83208').

card_in_set('plains', '9ED').
card_original_type('plains'/'9ED', 'Basic Land — Plains').
card_original_text('plains'/'9ED', 'W').
card_image_name('plains'/'9ED', 'plains3').
card_uid('plains'/'9ED', '9ED:Plains:plains3').
card_rarity('plains'/'9ED', 'Basic Land').
card_artist('plains'/'9ED', 'John Avon').
card_number('plains'/'9ED', '333').
card_multiverse_id('plains'/'9ED', '83209').

card_in_set('plains', '9ED').
card_original_type('plains'/'9ED', 'Basic Land — Plains').
card_original_text('plains'/'9ED', 'W').
card_image_name('plains'/'9ED', 'plains4').
card_uid('plains'/'9ED', '9ED:Plains:plains4').
card_rarity('plains'/'9ED', 'Basic Land').
card_artist('plains'/'9ED', 'Fred Fields').
card_number('plains'/'9ED', '334').
card_multiverse_id('plains'/'9ED', '83210').

card_in_set('polymorph', '9ED').
card_original_type('polymorph'/'9ED', 'Sorcery').
card_original_text('polymorph'/'9ED', 'Destroy target creature. It can\'t be regenerated. Its controller reveals cards from the top of his or her library until he or she reveals a creature card. The player puts that card into play and shuffles all other cards revealed this way into his or her library.').
card_image_name('polymorph'/'9ED', 'polymorph').
card_uid('polymorph'/'9ED', '9ED:Polymorph:polymorph').
card_rarity('polymorph'/'9ED', 'Rare').
card_artist('polymorph'/'9ED', 'Robert Bliss').
card_number('polymorph'/'9ED', '90').
card_multiverse_id('polymorph'/'9ED', '84565').

card_in_set('puppeteer', '9ED').
card_original_type('puppeteer'/'9ED', 'Creature — Human Wizard').
card_original_text('puppeteer'/'9ED', '{U}, {T}: Tap or untap target creature.').
card_image_name('puppeteer'/'9ED', 'puppeteer').
card_uid('puppeteer'/'9ED', '9ED:Puppeteer:puppeteer').
card_rarity('puppeteer'/'9ED', 'Uncommon').
card_artist('puppeteer'/'9ED', 'Scott M. Fischer').
card_number('puppeteer'/'9ED', '91').
card_flavor_text('puppeteer'/'9ED', '\"Getting people to do what you want is merely a matter of telling them what they want to hear.\"').
card_multiverse_id('puppeteer'/'9ED', '83215').

card_in_set('pyroclasm', '9ED').
card_original_type('pyroclasm'/'9ED', 'Sorcery').
card_original_text('pyroclasm'/'9ED', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'9ED', 'pyroclasm').
card_uid('pyroclasm'/'9ED', '9ED:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'9ED', 'Uncommon').
card_artist('pyroclasm'/'9ED', 'John Avon').
card_number('pyroclasm'/'9ED', '208').
card_flavor_text('pyroclasm'/'9ED', '\"When the air burns, only death breathes deep.\"\n—Bogardan mage').
card_multiverse_id('pyroclasm'/'9ED', '83216').

card_in_set('quicksand', '9ED').
card_original_type('quicksand'/'9ED', 'Land').
card_original_text('quicksand'/'9ED', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Quicksand: Target attacking creature without flying gets -1/-2 until end of turn.').
card_image_name('quicksand'/'9ED', 'quicksand').
card_uid('quicksand'/'9ED', '9ED:Quicksand:quicksand').
card_rarity('quicksand'/'9ED', 'Uncommon').
card_artist('quicksand'/'9ED', 'Roger Raupp').
card_number('quicksand'/'9ED', '323').
card_multiverse_id('quicksand'/'9ED', '83089').

card_in_set('raging goblin', '9ED').
card_original_type('raging goblin'/'9ED', 'Creature — Goblin Berserker').
card_original_text('raging goblin'/'9ED', 'Haste (This creature may attack the turn it comes under your control.)').
card_image_name('raging goblin'/'9ED', 'raging goblin').
card_uid('raging goblin'/'9ED', '9ED:Raging Goblin:raging goblin').
card_rarity('raging goblin'/'9ED', 'Common').
card_artist('raging goblin'/'9ED', 'Jeff Miracola').
card_number('raging goblin'/'9ED', '209').
card_flavor_text('raging goblin'/'9ED', 'He raged at the world, at his family, at his life. But mostly he just raged.').
card_multiverse_id('raging goblin'/'9ED', '83218').

card_in_set('raise dead', '9ED').
card_original_type('raise dead'/'9ED', 'Sorcery').
card_original_text('raise dead'/'9ED', 'Return target creature card from your graveyard to your hand.').
card_image_name('raise dead'/'9ED', 'raise dead').
card_uid('raise dead'/'9ED', '9ED:Raise Dead:raise dead').
card_rarity('raise dead'/'9ED', 'Common').
card_artist('raise dead'/'9ED', 'Carl Critchlow').
card_number('raise dead'/'9ED', '156').
card_flavor_text('raise dead'/'9ED', 'The earth cannot hold that which magic commands.').
card_multiverse_id('raise dead'/'9ED', '83220').

card_in_set('rampant growth', '9ED').
card_original_type('rampant growth'/'9ED', 'Sorcery').
card_original_text('rampant growth'/'9ED', 'Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_image_name('rampant growth'/'9ED', 'rampant growth').
card_uid('rampant growth'/'9ED', '9ED:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'9ED', 'Common').
card_artist('rampant growth'/'9ED', 'Tom Kyffin').
card_number('rampant growth'/'9ED', '263').
card_flavor_text('rampant growth'/'9ED', 'Nature grows solutions to her problems.').
card_multiverse_id('rampant growth'/'9ED', '83221').

card_in_set('rathi dragon', '9ED').
card_original_type('rathi dragon'/'9ED', 'Creature — Dragon').
card_original_text('rathi dragon'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Rathi Dragon comes into play, sacrifice it unless you sacrifice two Mountains.').
card_image_name('rathi dragon'/'9ED', 'rathi dragon').
card_uid('rathi dragon'/'9ED', '9ED:Rathi Dragon:rathi dragon').
card_rarity('rathi dragon'/'9ED', 'Rare').
card_artist('rathi dragon'/'9ED', 'Christopher Rush').
card_number('rathi dragon'/'9ED', '210').
card_multiverse_id('rathi dragon'/'9ED', '83973').

card_in_set('ravenous rats', '9ED').
card_original_type('ravenous rats'/'9ED', 'Creature — Rat').
card_original_text('ravenous rats'/'9ED', 'When Ravenous Rats comes into play, target opponent discards a card.').
card_image_name('ravenous rats'/'9ED', 'ravenous rats').
card_uid('ravenous rats'/'9ED', '9ED:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'9ED', 'Common').
card_artist('ravenous rats'/'9ED', 'Carl Critchlow').
card_number('ravenous rats'/'9ED', '157').
card_flavor_text('ravenous rats'/'9ED', 'Nothing is sacred to the rats. Everything is simply another meal.').
card_multiverse_id('ravenous rats'/'9ED', '83222').

card_in_set('razortooth rats', '9ED').
card_original_type('razortooth rats'/'9ED', 'Creature — Rat').
card_original_text('razortooth rats'/'9ED', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_image_name('razortooth rats'/'9ED', 'razortooth rats').
card_uid('razortooth rats'/'9ED', '9ED:Razortooth Rats:razortooth rats').
card_rarity('razortooth rats'/'9ED', 'Common').
card_artist('razortooth rats'/'9ED', 'Carl Critchlow').
card_number('razortooth rats'/'9ED', '158').
card_flavor_text('razortooth rats'/'9ED', '\"Men and rats both hunger: we for our playthings; they, for us.\"\n—Crovax, ascendant evincar').
card_multiverse_id('razortooth rats'/'9ED', '83418').

card_in_set('reclaim', '9ED').
card_original_type('reclaim'/'9ED', 'Instant').
card_original_text('reclaim'/'9ED', 'Put target card from your graveyard on top of your library.').
card_image_name('reclaim'/'9ED', 'reclaim').
card_uid('reclaim'/'9ED', '9ED:Reclaim:reclaim').
card_rarity('reclaim'/'9ED', 'Common').
card_artist('reclaim'/'9ED', 'Andrew Robinson').
card_number('reclaim'/'9ED', '264').
card_flavor_text('reclaim'/'9ED', 'The wise pay as much attention to what they throw away as to what they keep.').
card_multiverse_id('reclaim'/'9ED', '83229').

card_in_set('reflexes', '9ED').
card_original_type('reflexes'/'9ED', 'Enchantment — Aura').
card_original_text('reflexes'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature has first strike. (It deals combat damage before creatures without first strike.)').
card_image_name('reflexes'/'9ED', 'reflexes').
card_uid('reflexes'/'9ED', '9ED:Reflexes:reflexes').
card_rarity('reflexes'/'9ED', 'Common').
card_artist('reflexes'/'9ED', 'Donato Giancola').
card_number('reflexes'/'9ED', '211').
card_multiverse_id('reflexes'/'9ED', '83225').

card_in_set('regeneration', '9ED').
card_original_type('regeneration'/'9ED', 'Enchantment — Aura').
card_original_text('regeneration'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\n{G}: Regenerate enchanted creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('regeneration'/'9ED', 'regeneration').
card_uid('regeneration'/'9ED', '9ED:Regeneration:regeneration').
card_rarity('regeneration'/'9ED', 'Uncommon').
card_artist('regeneration'/'9ED', 'Adam Rex').
card_number('regeneration'/'9ED', '265').
card_multiverse_id('regeneration'/'9ED', '83226').

card_in_set('relentless assault', '9ED').
card_original_type('relentless assault'/'9ED', 'Sorcery').
card_original_text('relentless assault'/'9ED', 'Untap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.').
card_image_name('relentless assault'/'9ED', 'relentless assault').
card_uid('relentless assault'/'9ED', '9ED:Relentless Assault:relentless assault').
card_rarity('relentless assault'/'9ED', 'Rare').
card_artist('relentless assault'/'9ED', 'Greg & Tim Hildebrandt').
card_number('relentless assault'/'9ED', '212').
card_flavor_text('relentless assault'/'9ED', '\"Mercy? Mercy is for the playground, not the battleground.\"').
card_multiverse_id('relentless assault'/'9ED', '83227').

card_in_set('reminisce', '9ED').
card_original_type('reminisce'/'9ED', 'Sorcery').
card_original_text('reminisce'/'9ED', 'Target player shuffles his or her graveyard into his or her library.').
card_image_name('reminisce'/'9ED', 'reminisce').
card_uid('reminisce'/'9ED', '9ED:Reminisce:reminisce').
card_rarity('reminisce'/'9ED', 'Uncommon').
card_artist('reminisce'/'9ED', 'Ralph Horsley').
card_number('reminisce'/'9ED', '92').
card_flavor_text('reminisce'/'9ED', 'Leave the door to the past even slightly ajar and it could be blown off its hinges.').
card_multiverse_id('reminisce'/'9ED', '83958').

card_in_set('remove soul', '9ED').
card_original_type('remove soul'/'9ED', 'Instant').
card_original_text('remove soul'/'9ED', 'Counter target creature spell.').
card_image_name('remove soul'/'9ED', 'remove soul').
card_uid('remove soul'/'9ED', '9ED:Remove Soul:remove soul').
card_rarity('remove soul'/'9ED', 'Common').
card_artist('remove soul'/'9ED', 'Adam Rex').
card_number('remove soul'/'9ED', '93').
card_flavor_text('remove soul'/'9ED', 'When your enemies are denied soldiers, they are denied victory.').
card_multiverse_id('remove soul'/'9ED', '83228').

card_in_set('reverse damage', '9ED').
card_original_type('reverse damage'/'9ED', 'Instant').
card_original_text('reverse damage'/'9ED', 'The next time a source of your choice would deal damage to you this turn, prevent that damage. You gain that much life.').
card_image_name('reverse damage'/'9ED', 'reverse damage').
card_uid('reverse damage'/'9ED', '9ED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'9ED', 'Rare').
card_artist('reverse damage'/'9ED', 'Thomas Gianni').
card_number('reverse damage'/'9ED', '35').
card_flavor_text('reverse damage'/'9ED', '\"My enemy\'s hatred is his weakness. My enemy\'s anger is my strength.\"\n—Remin, venerable monk').
card_multiverse_id('reverse damage'/'9ED', '83132').

card_in_set('rewind', '9ED').
card_original_type('rewind'/'9ED', 'Instant').
card_original_text('rewind'/'9ED', 'Counter target spell, then untap up to four lands.').
card_image_name('rewind'/'9ED', 'rewind').
card_uid('rewind'/'9ED', '9ED:Rewind:rewind').
card_rarity('rewind'/'9ED', 'Uncommon').
card_artist('rewind'/'9ED', 'Dermot Power').
card_number('rewind'/'9ED', '94').
card_flavor_text('rewind'/'9ED', '\"Let\'s go over this one more time. . . .\"').
card_multiverse_id('rewind'/'9ED', '83230').

card_in_set('righteousness', '9ED').
card_original_type('righteousness'/'9ED', 'Instant').
card_original_text('righteousness'/'9ED', 'Target blocking creature gets +7/+7 until end of turn.').
card_image_name('righteousness'/'9ED', 'righteousness').
card_uid('righteousness'/'9ED', '9ED:Righteousness:righteousness').
card_rarity('righteousness'/'9ED', 'Rare').
card_artist('righteousness'/'9ED', 'Mike Dringenberg').
card_number('righteousness'/'9ED', '36').
card_flavor_text('righteousness'/'9ED', 'Sometimes the greatest strength is the strength of conviction.').
card_multiverse_id('righteousness'/'9ED', '84598').

card_in_set('river bear', '9ED').
card_original_type('river bear'/'9ED', 'Creature — Bear').
card_original_text('river bear'/'9ED', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)').
card_image_name('river bear'/'9ED', 'river bear').
card_uid('river bear'/'9ED', '9ED:River Bear:river bear').
card_rarity('river bear'/'9ED', 'Uncommon').
card_artist('river bear'/'9ED', 'Una Fricker').
card_number('river bear'/'9ED', '266').
card_flavor_text('river bear'/'9ED', 'The bears had been isolated on an island for hundreds of years, until the island sank and the bears didn\'t.').
card_multiverse_id('river bear'/'9ED', '83022').

card_in_set('rod of ruin', '9ED').
card_original_type('rod of ruin'/'9ED', 'Artifact').
card_original_text('rod of ruin'/'9ED', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'9ED', 'rod of ruin').
card_uid('rod of ruin'/'9ED', '9ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'9ED', 'Uncommon').
card_artist('rod of ruin'/'9ED', 'Eric Deschamps').
card_number('rod of ruin'/'9ED', '307').
card_flavor_text('rod of ruin'/'9ED', 'The rod is a relic from ancient times . . . cruel, vicious, mean-spirited times.').
card_multiverse_id('rod of ruin'/'9ED', '83233').

card_in_set('rogue kavu', '9ED').
card_original_type('rogue kavu'/'9ED', 'Creature — Kavu').
card_original_text('rogue kavu'/'9ED', 'Whenever Rogue Kavu attacks alone, it gets +2/+0 until end of turn.').
card_image_name('rogue kavu'/'9ED', 'rogue kavu').
card_uid('rogue kavu'/'9ED', '9ED:Rogue Kavu:rogue kavu').
card_rarity('rogue kavu'/'9ED', 'Common').
card_artist('rogue kavu'/'9ED', 'Darrell Riche').
card_number('rogue kavu'/'9ED', '213').
card_flavor_text('rogue kavu'/'9ED', 'Every litter of kavu yields one cub that refuses to hunt with the pack.').
card_multiverse_id('rogue kavu'/'9ED', '84478').

card_in_set('rootbreaker wurm', '9ED').
card_original_type('rootbreaker wurm'/'9ED', 'Creature — Wurm').
card_original_text('rootbreaker wurm'/'9ED', 'Trample (If this creature would deal enough combat damage to its blockers to destroy them, you may have it deal the rest of its damage to defending player.)').
card_image_name('rootbreaker wurm'/'9ED', 'rootbreaker wurm').
card_uid('rootbreaker wurm'/'9ED', '9ED:Rootbreaker Wurm:rootbreaker wurm').
card_rarity('rootbreaker wurm'/'9ED', 'Uncommon').
card_artist('rootbreaker wurm'/'9ED', 'Richard Kane Ferguson').
card_number('rootbreaker wurm'/'9ED', '267').
card_multiverse_id('rootbreaker wurm'/'9ED', '83151').

card_in_set('rootwalla', '9ED').
card_original_type('rootwalla'/'9ED', 'Creature — Lizard').
card_original_text('rootwalla'/'9ED', '{1}{G}: Rootwalla gets +2/+2 until end of turn. Play this ability only once each turn.').
card_image_name('rootwalla'/'9ED', 'rootwalla').
card_uid('rootwalla'/'9ED', '9ED:Rootwalla:rootwalla').
card_rarity('rootwalla'/'9ED', 'Common').
card_artist('rootwalla'/'9ED', 'Roger Raupp').
card_number('rootwalla'/'9ED', '268').
card_flavor_text('rootwalla'/'9ED', 'If you try to sneak up on a rootwalla, you\'ll suddenly find yourself dealing with twice the lizard.').
card_multiverse_id('rootwalla'/'9ED', '84530').

card_in_set('royal assassin', '9ED').
card_original_type('royal assassin'/'9ED', 'Creature — Human Assassin').
card_original_text('royal assassin'/'9ED', '{T}: Destroy target tapped creature.').
card_image_name('royal assassin'/'9ED', 'royal assassin').
card_uid('royal assassin'/'9ED', '9ED:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'9ED', 'Rare').
card_artist('royal assassin'/'9ED', 'Mark Zug').
card_number('royal assassin'/'9ED', '159').
card_flavor_text('royal assassin'/'9ED', 'Trained in the arts of stealth, royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'9ED', '83235').

card_in_set('rukh egg', '9ED').
card_original_type('rukh egg'/'9ED', 'Creature — Egg').
card_original_text('rukh egg'/'9ED', 'When Rukh Egg is put into a graveyard from play, put a 4/4 red Bird creature token with flying into play at end of turn.').
card_image_name('rukh egg'/'9ED', 'rukh egg').
card_uid('rukh egg'/'9ED', '9ED:Rukh Egg:rukh egg').
card_rarity('rukh egg'/'9ED', 'Rare').
card_artist('rukh egg'/'9ED', 'Mark Zug').
card_number('rukh egg'/'9ED', '214').
card_flavor_text('rukh egg'/'9ED', 'Called \"stonefeathers\" by many lowlanders, rukhs are thought to descend from a phoenix that sacrificed its flame for a body of stone.').
card_multiverse_id('rukh egg'/'9ED', '83236').

card_in_set('sacred ground', '9ED').
card_original_type('sacred ground'/'9ED', 'Enchantment').
card_original_text('sacred ground'/'9ED', 'Whenever a spell or ability an opponent controls causes a land to be put into your graveyard from play, return that land to play.').
card_image_name('sacred ground'/'9ED', 'sacred ground').
card_uid('sacred ground'/'9ED', '9ED:Sacred Ground:sacred ground').
card_rarity('sacred ground'/'9ED', 'Rare').
card_artist('sacred ground'/'9ED', 'Gary Ruddell').
card_number('sacred ground'/'9ED', '37').
card_flavor_text('sacred ground'/'9ED', 'The knight knelt and prayed, first for the sanctity of his land and second for the strength to repel its invaders.').
card_multiverse_id('sacred ground'/'9ED', '83239').

card_in_set('sacred nectar', '9ED').
card_original_type('sacred nectar'/'9ED', 'Sorcery').
card_original_text('sacred nectar'/'9ED', 'You gain 4 life.').
card_image_name('sacred nectar'/'9ED', 'sacred nectar').
card_uid('sacred nectar'/'9ED', '9ED:Sacred Nectar:sacred nectar').
card_rarity('sacred nectar'/'9ED', 'Common').
card_artist('sacred nectar'/'9ED', 'Janine Johnston').
card_number('sacred nectar'/'9ED', '38').
card_flavor_text('sacred nectar'/'9ED', '\"Over the silver mountains,\nWhere spring the nectar fountains,\n     There will I kiss\n     The bowl of bliss;\nAnd drink my everlasting fill. . . .\"\n—Sir Walter Raleigh, \"The Pilgrimage\"').
card_multiverse_id('sacred nectar'/'9ED', '83240').

card_in_set('sage aven', '9ED').
card_original_type('sage aven'/'9ED', 'Creature — Bird Wizard').
card_original_text('sage aven'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhen Sage Aven comes into play, look at the top four cards of your library, then put them back in any order.').
card_image_name('sage aven'/'9ED', 'sage aven').
card_uid('sage aven'/'9ED', '9ED:Sage Aven:sage aven').
card_rarity('sage aven'/'9ED', 'Common').
card_artist('sage aven'/'9ED', 'Randy Gallegos').
card_number('sage aven'/'9ED', '95').
card_multiverse_id('sage aven'/'9ED', '83985').

card_in_set('samite healer', '9ED').
card_original_type('samite healer'/'9ED', 'Creature — Human Cleric').
card_original_text('samite healer'/'9ED', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_image_name('samite healer'/'9ED', 'samite healer').
card_uid('samite healer'/'9ED', '9ED:Samite Healer:samite healer').
card_rarity('samite healer'/'9ED', 'Common').
card_artist('samite healer'/'9ED', 'Anson Maddocks').
card_number('samite healer'/'9ED', '39').
card_flavor_text('samite healer'/'9ED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'9ED', '83244').

card_in_set('sanctum guardian', '9ED').
card_original_type('sanctum guardian'/'9ED', 'Creature — Human Cleric').
card_original_text('sanctum guardian'/'9ED', 'Sacrifice Sanctum Guardian: The next time a source of your choice would deal damage to target creature or player this turn, prevent that damage.').
card_image_name('sanctum guardian'/'9ED', 'sanctum guardian').
card_uid('sanctum guardian'/'9ED', '9ED:Sanctum Guardian:sanctum guardian').
card_rarity('sanctum guardian'/'9ED', 'Uncommon').
card_artist('sanctum guardian'/'9ED', 'Donato Giancola').
card_number('sanctum guardian'/'9ED', '40').
card_multiverse_id('sanctum guardian'/'9ED', '87346').

card_in_set('sandstone warrior', '9ED').
card_original_type('sandstone warrior'/'9ED', 'Creature — Human Soldier').
card_original_text('sandstone warrior'/'9ED', 'First strike (This creature deals combat damage before creatures without first strike.)\n{R}: Sandstone Warrior gets +1/+0 until end of turn.').
card_image_name('sandstone warrior'/'9ED', 'sandstone warrior').
card_uid('sandstone warrior'/'9ED', '9ED:Sandstone Warrior:sandstone warrior').
card_rarity('sandstone warrior'/'9ED', 'Common').
card_artist('sandstone warrior'/'9ED', 'Lars Grant-West').
card_number('sandstone warrior'/'9ED', '215').
card_multiverse_id('sandstone warrior'/'9ED', '84054').

card_in_set('savannah lions', '9ED').
card_original_type('savannah lions'/'9ED', 'Creature — Cat').
card_original_text('savannah lions'/'9ED', '').
card_image_name('savannah lions'/'9ED', 'savannah lions').
card_uid('savannah lions'/'9ED', '9ED:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'9ED', 'Rare').
card_artist('savannah lions'/'9ED', 'Carl Critchlow').
card_number('savannah lions'/'9ED', '41').
card_flavor_text('savannah lions'/'9ED', 'Even the brave are scared by a lion three times: first by its tracks, again by its roar, and one last time face to face.\n—Somali proverb').
card_multiverse_id('savannah lions'/'9ED', '83246').

card_in_set('scaled wurm', '9ED').
card_original_type('scaled wurm'/'9ED', 'Creature — Wurm').
card_original_text('scaled wurm'/'9ED', '').
card_image_name('scaled wurm'/'9ED', 'scaled wurm').
card_uid('scaled wurm'/'9ED', '9ED:Scaled Wurm:scaled wurm').
card_rarity('scaled wurm'/'9ED', 'Common').
card_artist('scaled wurm'/'9ED', 'Wayne England').
card_number('scaled wurm'/'9ED', '269').
card_flavor_text('scaled wurm'/'9ED', 'The wurms become so big, a single scale can be used as a roof for a human dwelling.').
card_multiverse_id('scaled wurm'/'9ED', '83272').

card_in_set('scathe zombies', '9ED').
card_original_type('scathe zombies'/'9ED', 'Creature — Zombie').
card_original_text('scathe zombies'/'9ED', '').
card_image_name('scathe zombies'/'9ED', 'scathe zombies').
card_uid('scathe zombies'/'9ED', '9ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'9ED', 'Common').
card_artist('scathe zombies'/'9ED', 'Kev Walker').
card_number('scathe zombies'/'9ED', '160').
card_flavor_text('scathe zombies'/'9ED', '\"They groaned, they stirred, they all uprose,\nNor spake, nor moved their eyes;\nIt had been strange, even in a dream,\nTo have seen those dead men rise.\"\n—Samuel Taylor Coleridge,\n\"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'9ED', '83247').

card_in_set('sea monster', '9ED').
card_original_type('sea monster'/'9ED', 'Creature — Serpent').
card_original_text('sea monster'/'9ED', 'Sea Monster can\'t attack unless defending player controls an Island.').
card_image_name('sea monster'/'9ED', 'sea monster').
card_uid('sea monster'/'9ED', '9ED:Sea Monster:sea monster').
card_rarity('sea monster'/'9ED', 'Common').
card_artist('sea monster'/'9ED', 'Daniel Gelon').
card_number('sea monster'/'9ED', '96').
card_flavor_text('sea monster'/'9ED', 'It\'s easy to believe the monster is a myth—until you feel three hundred thousand pounds of myth crashing down on your ship.').
card_multiverse_id('sea monster'/'9ED', '83249').

card_in_set('sea\'s claim', '9ED').
card_original_type('sea\'s claim'/'9ED', 'Enchantment — Aura').
card_original_text('sea\'s claim'/'9ED', 'Enchant land (Target a land as you play this. This card comes into play attached to that land.)\nEnchanted land is an Island.').
card_image_name('sea\'s claim'/'9ED', 'sea\'s claim').
card_uid('sea\'s claim'/'9ED', '9ED:Sea\'s Claim:sea\'s claim').
card_rarity('sea\'s claim'/'9ED', 'Common').
card_artist('sea\'s claim'/'9ED', 'Thomas Gianni').
card_number('sea\'s claim'/'9ED', '97').
card_flavor_text('sea\'s claim'/'9ED', '\"Who\'s the crazy one now!?\"\n—Torgle, mountaintop boatmaker').
card_multiverse_id('sea\'s claim'/'9ED', '83446').

card_in_set('seasoned marshal', '9ED').
card_original_type('seasoned marshal'/'9ED', 'Creature — Human Soldier').
card_original_text('seasoned marshal'/'9ED', 'Whenever Seasoned Marshal attacks, you may tap target creature.').
card_image_name('seasoned marshal'/'9ED', 'seasoned marshal').
card_uid('seasoned marshal'/'9ED', '9ED:Seasoned Marshal:seasoned marshal').
card_rarity('seasoned marshal'/'9ED', 'Uncommon').
card_artist('seasoned marshal'/'9ED', 'Matthew D. Wilson').
card_number('seasoned marshal'/'9ED', '42').
card_flavor_text('seasoned marshal'/'9ED', '\"We are not interested in the possibilities of defeat.\"\n—Queen Victoria').
card_multiverse_id('seasoned marshal'/'9ED', '87347').

card_in_set('seedborn muse', '9ED').
card_original_type('seedborn muse'/'9ED', 'Creature — Spirit').
card_original_text('seedborn muse'/'9ED', 'Untap all permanents you control during each other player\'s untap step.').
card_image_name('seedborn muse'/'9ED', 'seedborn muse').
card_uid('seedborn muse'/'9ED', '9ED:Seedborn Muse:seedborn muse').
card_rarity('seedborn muse'/'9ED', 'Rare').
card_artist('seedborn muse'/'9ED', 'Adam Rex').
card_number('seedborn muse'/'9ED', '270').
card_flavor_text('seedborn muse'/'9ED', '\"Her voice is the wilderness, savage and pure.\"\n—Kamahl, druid acolyte').
card_multiverse_id('seedborn muse'/'9ED', '84098').

card_in_set('seething song', '9ED').
card_original_type('seething song'/'9ED', 'Instant').
card_original_text('seething song'/'9ED', 'Add {R}{R}{R}{R}{R} to your mana pool.').
card_image_name('seething song'/'9ED', 'seething song').
card_uid('seething song'/'9ED', '9ED:Seething Song:seething song').
card_rarity('seething song'/'9ED', 'Common').
card_artist('seething song'/'9ED', 'Martina Pilcerova').
card_number('seething song'/'9ED', '216').
card_flavor_text('seething song'/'9ED', '\"The purest ore is produced from the hottest furnace, and the brightest thunder-bolt is elicited from the darkest storm.\"\n—Charles Colton').
card_multiverse_id('seething song'/'9ED', '83377').

card_in_set('sengir vampire', '9ED').
card_original_type('sengir vampire'/'9ED', 'Creature — Vampire').
card_original_text('sengir vampire'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever a creature dealt damage by Sengir Vampire this turn is put into a graveyard, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'9ED', 'sengir vampire').
card_uid('sengir vampire'/'9ED', '9ED:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'9ED', 'Rare').
card_artist('sengir vampire'/'9ED', 'Kev Walker').
card_number('sengir vampire'/'9ED', '161').
card_flavor_text('sengir vampire'/'9ED', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'9ED', '84688').

card_in_set('serpent warrior', '9ED').
card_original_type('serpent warrior'/'9ED', 'Creature — Snake Warrior').
card_original_text('serpent warrior'/'9ED', 'When Serpent Warrior comes into play, you lose 3 life.').
card_image_name('serpent warrior'/'9ED', 'serpent warrior').
card_uid('serpent warrior'/'9ED', '9ED:Serpent Warrior:serpent warrior').
card_rarity('serpent warrior'/'9ED', 'Common').
card_artist('serpent warrior'/'9ED', 'Ron Spencer').
card_number('serpent warrior'/'9ED', '162').
card_flavor_text('serpent warrior'/'9ED', 'They are incubated by swamp gases and cut their teeth on zombie flesh. Serpent warriors are not to be trifled with.').
card_multiverse_id('serpent warrior'/'9ED', '83364').

card_in_set('serra angel', '9ED').
card_original_type('serra angel'/'9ED', 'Creature — Angel').
card_original_text('serra angel'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'9ED', 'serra angel').
card_uid('serra angel'/'9ED', '9ED:Serra Angel:serra angel').
card_rarity('serra angel'/'9ED', 'Rare').
card_artist('serra angel'/'9ED', 'Greg Staples').
card_number('serra angel'/'9ED', '43').
card_flavor_text('serra angel'/'9ED', 'When she flies above the good, they consider themselves blessed. When she flies above the wicked, they consider themselves dead.').
card_multiverse_id('serra angel'/'9ED', '83254').

card_in_set('serra\'s blessing', '9ED').
card_original_type('serra\'s blessing'/'9ED', 'Enchantment').
card_original_text('serra\'s blessing'/'9ED', 'Creatures you control have vigilance. (Attacking doesn\'t cause them to tap.)').
card_image_name('serra\'s blessing'/'9ED', 'serra\'s blessing').
card_uid('serra\'s blessing'/'9ED', '9ED:Serra\'s Blessing:serra\'s blessing').
card_rarity('serra\'s blessing'/'9ED', 'Uncommon').
card_artist('serra\'s blessing'/'9ED', 'Rebecca Guay').
card_number('serra\'s blessing'/'9ED', '44').
card_flavor_text('serra\'s blessing'/'9ED', '\"Rise, and I will show you how to run without flagging, fight without raging, and fly without wings.\"').
card_multiverse_id('serra\'s blessing'/'9ED', '82964').

card_in_set('shard phoenix', '9ED').
card_original_type('shard phoenix'/'9ED', 'Creature — Phoenix').
card_original_text('shard phoenix'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nSacrifice Shard Phoenix: Shard Phoenix deals 2 damage to each creature without flying.\n{R}{R}{R}: Return Shard Phoenix from your graveyard to your hand. Play this ability only during your upkeep.').
card_image_name('shard phoenix'/'9ED', 'shard phoenix').
card_uid('shard phoenix'/'9ED', '9ED:Shard Phoenix:shard phoenix').
card_rarity('shard phoenix'/'9ED', 'Rare').
card_artist('shard phoenix'/'9ED', 'Paolo Parente').
card_number('shard phoenix'/'9ED', '217').
card_multiverse_id('shard phoenix'/'9ED', '83410').

card_in_set('shatter', '9ED').
card_original_type('shatter'/'9ED', 'Instant').
card_original_text('shatter'/'9ED', 'Destroy target artifact.').
card_image_name('shatter'/'9ED', 'shatter').
card_uid('shatter'/'9ED', '9ED:Shatter:shatter').
card_rarity('shatter'/'9ED', 'Common').
card_artist('shatter'/'9ED', 'Jason Alexander Behnke').
card_number('shatter'/'9ED', '218').
card_multiverse_id('shatter'/'9ED', '83257').

card_in_set('shivan dragon', '9ED').
card_original_type('shivan dragon'/'9ED', 'Creature — Dragon').
card_original_text('shivan dragon'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'9ED', 'shivan dragon').
card_uid('shivan dragon'/'9ED', '9ED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'9ED', 'Rare').
card_artist('shivan dragon'/'9ED', 'Donato Giancola').
card_number('shivan dragon'/'9ED', '219').
card_flavor_text('shivan dragon'/'9ED', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'9ED', '83259').

card_in_set('shivan reef', '9ED').
card_original_type('shivan reef'/'9ED', 'Land').
card_original_text('shivan reef'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Shivan Reef deals 1 damage to you.').
card_image_name('shivan reef'/'9ED', 'shivan reef').
card_uid('shivan reef'/'9ED', '9ED:Shivan Reef:shivan reef').
card_rarity('shivan reef'/'9ED', 'Rare').
card_artist('shivan reef'/'9ED', 'Rob Alexander').
card_number('shivan reef'/'9ED', '324').
card_multiverse_id('shivan reef'/'9ED', '84457').

card_in_set('shock', '9ED').
card_original_type('shock'/'9ED', 'Instant').
card_original_text('shock'/'9ED', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'9ED', 'shock').
card_uid('shock'/'9ED', '9ED:Shock:shock').
card_rarity('shock'/'9ED', 'Common').
card_artist('shock'/'9ED', 'Randy Gallegos').
card_number('shock'/'9ED', '220').
card_flavor_text('shock'/'9ED', '\"Once I saw a goblin shouting at a thunderstorm. He lost the argument pretty quickly.\"\n—Wandering mage').
card_multiverse_id('shock'/'9ED', '83261').

card_in_set('sift', '9ED').
card_original_type('sift'/'9ED', 'Sorcery').
card_original_text('sift'/'9ED', 'Draw three cards, then discard a card.').
card_image_name('sift'/'9ED', 'sift').
card_uid('sift'/'9ED', '9ED:Sift:sift').
card_rarity('sift'/'9ED', 'Common').
card_artist('sift'/'9ED', 'Jeremy Jarvis').
card_number('sift'/'9ED', '98').
card_flavor_text('sift'/'9ED', 'Dwell longest on the thoughts that shine brightest.').
card_multiverse_id('sift'/'9ED', '83307').

card_in_set('silklash spider', '9ED').
card_original_type('silklash spider'/'9ED', 'Creature — Spider').
card_original_text('silklash spider'/'9ED', 'Silklash Spider can block as though it had flying.\n{X}{G}{G}: Silklash Spider deals X damage to each creature with flying.').
card_image_name('silklash spider'/'9ED', 'silklash spider').
card_uid('silklash spider'/'9ED', '9ED:Silklash Spider:silklash spider').
card_rarity('silklash spider'/'9ED', 'Rare').
card_artist('silklash spider'/'9ED', 'Iain McCaig').
card_number('silklash spider'/'9ED', '271').
card_flavor_text('silklash spider'/'9ED', 'The only thing that flies over the Krosan Forest is the wind.').
card_multiverse_id('silklash spider'/'9ED', '83463').

card_in_set('skyhunter prowler', '9ED').
card_original_type('skyhunter prowler'/'9ED', 'Creature — Cat Knight').
card_original_text('skyhunter prowler'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.) \nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('skyhunter prowler'/'9ED', 'skyhunter prowler').
card_uid('skyhunter prowler'/'9ED', '9ED:Skyhunter Prowler:skyhunter prowler').
card_rarity('skyhunter prowler'/'9ED', 'Common').
card_artist('skyhunter prowler'/'9ED', 'Vance Kovacs').
card_number('skyhunter prowler'/'9ED', '45').
card_multiverse_id('skyhunter prowler'/'9ED', '83412').

card_in_set('slate of ancestry', '9ED').
card_original_type('slate of ancestry'/'9ED', 'Artifact').
card_original_text('slate of ancestry'/'9ED', '{4}, {T}, Discard your hand: Draw a card for each creature you control.').
card_image_name('slate of ancestry'/'9ED', 'slate of ancestry').
card_uid('slate of ancestry'/'9ED', '9ED:Slate of Ancestry:slate of ancestry').
card_rarity('slate of ancestry'/'9ED', 'Rare').
card_artist('slate of ancestry'/'9ED', 'Corey D. Macourek').
card_number('slate of ancestry'/'9ED', '308').
card_flavor_text('slate of ancestry'/'9ED', 'The pattern of life can be studied like a book, if you know how to read it.').
card_multiverse_id('slate of ancestry'/'9ED', '83540').

card_in_set('slay', '9ED').
card_original_type('slay'/'9ED', 'Instant').
card_original_text('slay'/'9ED', 'Destroy target green creature. It can\'t be regenerated.\nDraw a card.').
card_image_name('slay'/'9ED', 'slay').
card_uid('slay'/'9ED', '9ED:Slay:slay').
card_rarity('slay'/'9ED', 'Uncommon').
card_artist('slay'/'9ED', 'Ben Thompson').
card_number('slay'/'9ED', '163').
card_flavor_text('slay'/'9ED', 'The elves had the edge in guile, skill, and valor. But in the end, only sheer numbers mattered.').
card_multiverse_id('slay'/'9ED', '83266').

card_in_set('sleight of hand', '9ED').
card_original_type('sleight of hand'/'9ED', 'Sorcery').
card_original_text('sleight of hand'/'9ED', 'Look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.').
card_image_name('sleight of hand'/'9ED', 'sleight of hand').
card_uid('sleight of hand'/'9ED', '9ED:Sleight of Hand:sleight of hand').
card_rarity('sleight of hand'/'9ED', 'Common').
card_artist('sleight of hand'/'9ED', 'Jim Murray').
card_number('sleight of hand'/'9ED', '99').
card_flavor_text('sleight of hand'/'9ED', '\"Gambling with mages never is.\"\n—Remin, venerable monk').
card_multiverse_id('sleight of hand'/'9ED', '83129').

card_in_set('soul feast', '9ED').
card_original_type('soul feast'/'9ED', 'Sorcery').
card_original_text('soul feast'/'9ED', 'Target player loses 4 life and you gain 4 life.').
card_image_name('soul feast'/'9ED', 'soul feast').
card_uid('soul feast'/'9ED', '9ED:Soul Feast:soul feast').
card_rarity('soul feast'/'9ED', 'Uncommon').
card_artist('soul feast'/'9ED', 'Adam Rex').
card_number('soul feast'/'9ED', '164').
card_flavor_text('soul feast'/'9ED', 'You are who you eat.').
card_multiverse_id('soul feast'/'9ED', '83269').

card_in_set('soul warden', '9ED').
card_original_type('soul warden'/'9ED', 'Creature — Human Cleric').
card_original_text('soul warden'/'9ED', 'Whenever another creature comes into play, you gain 1 life.').
card_image_name('soul warden'/'9ED', 'soul warden').
card_uid('soul warden'/'9ED', '9ED:Soul Warden:soul warden').
card_rarity('soul warden'/'9ED', 'Uncommon').
card_artist('soul warden'/'9ED', 'Randy Gallegos').
card_number('soul warden'/'9ED', '46').
card_flavor_text('soul warden'/'9ED', 'Count carefully the souls and see that none are lost.\n—Vec teaching').
card_multiverse_id('soul warden'/'9ED', '84112').

card_in_set('spellbook', '9ED').
card_original_type('spellbook'/'9ED', 'Artifact').
card_original_text('spellbook'/'9ED', 'You have no maximum hand size.').
card_image_name('spellbook'/'9ED', 'spellbook').
card_uid('spellbook'/'9ED', '9ED:Spellbook:spellbook').
card_rarity('spellbook'/'9ED', 'Uncommon').
card_artist('spellbook'/'9ED', 'Andrew Goldhawk').
card_number('spellbook'/'9ED', '309').
card_flavor_text('spellbook'/'9ED', '\"Everything the wise woman learned she wrote in a book, and when the pages were black with ink, she took white ink and began again.\"\n—Karn, silver golem').
card_multiverse_id('spellbook'/'9ED', '83270').

card_in_set('spined wurm', '9ED').
card_original_type('spined wurm'/'9ED', 'Creature — Wurm').
card_original_text('spined wurm'/'9ED', '').
card_image_name('spined wurm'/'9ED', 'spined wurm').
card_uid('spined wurm'/'9ED', '9ED:Spined Wurm:spined wurm').
card_rarity('spined wurm'/'9ED', 'Common').
card_artist('spined wurm'/'9ED', 'Carl Critchlow').
card_number('spined wurm'/'9ED', 'S10').
card_flavor_text('spined wurm'/'9ED', '\"I wouldn\'t stand in front of that wurm, son. \'Course, I wouldn\'t stand behind it neither. In fact, standing anywhere near that wurm\'s not much of a plan. Running, now that\'s a plan\"\n—Wandering mage').
card_multiverse_id('spined wurm'/'9ED', '94914').

card_in_set('spineless thug', '9ED').
card_original_type('spineless thug'/'9ED', 'Creature — Zombie Mercenary').
card_original_text('spineless thug'/'9ED', 'Spineless Thug can\'t block.').
card_image_name('spineless thug'/'9ED', 'spineless thug').
card_uid('spineless thug'/'9ED', '9ED:Spineless Thug:spineless thug').
card_rarity('spineless thug'/'9ED', 'Common').
card_artist('spineless thug'/'9ED', 'Matthew D. Wilson').
card_number('spineless thug'/'9ED', '165').
card_flavor_text('spineless thug'/'9ED', 'What it lacks in backbone, it makes up for in cruelty.').
card_multiverse_id('spineless thug'/'9ED', '83273').

card_in_set('spirit link', '9ED').
card_original_type('spirit link'/'9ED', 'Enchantment — Aura').
card_original_text('spirit link'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nWhenever enchanted creature deals damage, you gain that much life.').
card_image_name('spirit link'/'9ED', 'spirit link').
card_uid('spirit link'/'9ED', '9ED:Spirit Link:spirit link').
card_rarity('spirit link'/'9ED', 'Uncommon').
card_artist('spirit link'/'9ED', 'Kev Walker').
card_number('spirit link'/'9ED', '47').
card_multiverse_id('spirit link'/'9ED', '83274').

card_in_set('stone rain', '9ED').
card_original_type('stone rain'/'9ED', 'Sorcery').
card_original_text('stone rain'/'9ED', 'Destroy target land.').
card_image_name('stone rain'/'9ED', 'stone rain').
card_uid('stone rain'/'9ED', '9ED:Stone Rain:stone rain').
card_rarity('stone rain'/'9ED', 'Common').
card_artist('stone rain'/'9ED', 'John Matson').
card_number('stone rain'/'9ED', '221').
card_multiverse_id('stone rain'/'9ED', '83281').

card_in_set('storage matrix', '9ED').
card_original_type('storage matrix'/'9ED', 'Artifact').
card_original_text('storage matrix'/'9ED', 'As long as Storage Matrix is untapped, each player chooses artifact, creature, or land during his or her untap step. That player can untap only permanents of the chosen type this step.').
card_image_name('storage matrix'/'9ED', 'storage matrix').
card_uid('storage matrix'/'9ED', '9ED:Storage Matrix:storage matrix').
card_rarity('storage matrix'/'9ED', 'Rare').
card_artist('storage matrix'/'9ED', 'Patrick Ho').
card_number('storage matrix'/'9ED', '310').
card_multiverse_id('storage matrix'/'9ED', '84652').

card_in_set('storm crow', '9ED').
card_original_type('storm crow'/'9ED', 'Creature — Bird').
card_original_text('storm crow'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('storm crow'/'9ED', 'storm crow').
card_uid('storm crow'/'9ED', '9ED:Storm Crow:storm crow').
card_rarity('storm crow'/'9ED', 'Common').
card_artist('storm crow'/'9ED', 'John Matson').
card_number('storm crow'/'9ED', '100').
card_flavor_text('storm crow'/'9ED', 'Storm crow descending, winter unending. Storm crow departing, summer is starting.').
card_multiverse_id('storm crow'/'9ED', '83282').

card_in_set('story circle', '9ED').
card_original_type('story circle'/'9ED', 'Enchantment').
card_original_text('story circle'/'9ED', 'As Story Circle comes into play, choose a color.\n{W}: The next time a source of your choice of the chosen color would deal damage to you this turn, prevent that damage.').
card_image_name('story circle'/'9ED', 'story circle').
card_uid('story circle'/'9ED', '9ED:Story Circle:story circle').
card_rarity('story circle'/'9ED', 'Rare').
card_artist('story circle'/'9ED', 'Alan Pollack').
card_number('story circle'/'9ED', '48').
card_multiverse_id('story circle'/'9ED', '83283').

card_in_set('stream of life', '9ED').
card_original_type('stream of life'/'9ED', 'Sorcery').
card_original_text('stream of life'/'9ED', 'Target player gains X life.').
card_image_name('stream of life'/'9ED', 'stream of life').
card_uid('stream of life'/'9ED', '9ED:Stream of Life:stream of life').
card_rarity('stream of life'/'9ED', 'Uncommon').
card_artist('stream of life'/'9ED', 'Andrew Goldhawk').
card_number('stream of life'/'9ED', '272').
card_multiverse_id('stream of life'/'9ED', '83284').

card_in_set('sudden impact', '9ED').
card_original_type('sudden impact'/'9ED', 'Instant').
card_original_text('sudden impact'/'9ED', 'Sudden Impact deals damage equal to the number of cards in target player\'s hand to that player.').
card_image_name('sudden impact'/'9ED', 'sudden impact').
card_uid('sudden impact'/'9ED', '9ED:Sudden Impact:sudden impact').
card_rarity('sudden impact'/'9ED', 'Uncommon').
card_artist('sudden impact'/'9ED', 'Simon Bisley').
card_number('sudden impact'/'9ED', '222').
card_flavor_text('sudden impact'/'9ED', '\"Some say it\'s better to think before you act. While those people are considering all the options, that\'s usually when I kill them.\"\n—Dravus, lava mage').
card_multiverse_id('sudden impact'/'9ED', '83285').

card_in_set('sulfurous springs', '9ED').
card_original_type('sulfurous springs'/'9ED', 'Land').
card_original_text('sulfurous springs'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Sulfurous Springs deals 1 damage to you.').
card_image_name('sulfurous springs'/'9ED', 'sulfurous springs').
card_uid('sulfurous springs'/'9ED', '9ED:Sulfurous Springs:sulfurous springs').
card_rarity('sulfurous springs'/'9ED', 'Rare').
card_artist('sulfurous springs'/'9ED', 'Rob Alexander').
card_number('sulfurous springs'/'9ED', '325').
card_multiverse_id('sulfurous springs'/'9ED', '84550').

card_in_set('summer bloom', '9ED').
card_original_type('summer bloom'/'9ED', 'Sorcery').
card_original_text('summer bloom'/'9ED', 'You may play up to three additional lands this turn.').
card_image_name('summer bloom'/'9ED', 'summer bloom').
card_uid('summer bloom'/'9ED', '9ED:Summer Bloom:summer bloom').
card_rarity('summer bloom'/'9ED', 'Uncommon').
card_artist('summer bloom'/'9ED', 'Nicola Leonard').
card_number('summer bloom'/'9ED', '273').
card_flavor_text('summer bloom'/'9ED', 'Summer sends its kiss with warmth and blooming life.').
card_multiverse_id('summer bloom'/'9ED', '84694').

card_in_set('suntail hawk', '9ED').
card_original_type('suntail hawk'/'9ED', 'Creature — Bird').
card_original_text('suntail hawk'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('suntail hawk'/'9ED', 'suntail hawk').
card_uid('suntail hawk'/'9ED', '9ED:Suntail Hawk:suntail hawk').
card_rarity('suntail hawk'/'9ED', 'Common').
card_artist('suntail hawk'/'9ED', 'Heather Hudson').
card_number('suntail hawk'/'9ED', '49').
card_flavor_text('suntail hawk'/'9ED', 'Its eye the glaring sun, its cry the keening wind.').
card_multiverse_id('suntail hawk'/'9ED', '83286').

card_in_set('swamp', '9ED').
card_original_type('swamp'/'9ED', 'Basic Land — Swamp').
card_original_text('swamp'/'9ED', 'B').
card_image_name('swamp'/'9ED', 'swamp1').
card_uid('swamp'/'9ED', '9ED:Swamp:swamp1').
card_rarity('swamp'/'9ED', 'Basic Land').
card_artist('swamp'/'9ED', 'Bob Eggleton').
card_number('swamp'/'9ED', '339').
card_multiverse_id('swamp'/'9ED', '83288').

card_in_set('swamp', '9ED').
card_original_type('swamp'/'9ED', 'Basic Land — Swamp').
card_original_text('swamp'/'9ED', 'B').
card_image_name('swamp'/'9ED', 'swamp2').
card_uid('swamp'/'9ED', '9ED:Swamp:swamp2').
card_rarity('swamp'/'9ED', 'Basic Land').
card_artist('swamp'/'9ED', 'John Avon').
card_number('swamp'/'9ED', '340').
card_multiverse_id('swamp'/'9ED', '83289').

card_in_set('swamp', '9ED').
card_original_type('swamp'/'9ED', 'Basic Land — Swamp').
card_original_text('swamp'/'9ED', 'B').
card_image_name('swamp'/'9ED', 'swamp3').
card_uid('swamp'/'9ED', '9ED:Swamp:swamp3').
card_rarity('swamp'/'9ED', 'Basic Land').
card_artist('swamp'/'9ED', 'Dan Frazier').
card_number('swamp'/'9ED', '341').
card_multiverse_id('swamp'/'9ED', '83290').

card_in_set('swamp', '9ED').
card_original_type('swamp'/'9ED', 'Basic Land — Swamp').
card_original_text('swamp'/'9ED', 'B').
card_image_name('swamp'/'9ED', 'swamp4').
card_uid('swamp'/'9ED', '9ED:Swamp:swamp4').
card_rarity('swamp'/'9ED', 'Basic Land').
card_artist('swamp'/'9ED', 'Larry Elmore').
card_number('swamp'/'9ED', '342').
card_multiverse_id('swamp'/'9ED', '83291').

card_in_set('swarm of rats', '9ED').
card_original_type('swarm of rats'/'9ED', 'Creature — Rat').
card_original_text('swarm of rats'/'9ED', 'Swarm of Rats\'s power is equal to the number of Rats you control.').
card_image_name('swarm of rats'/'9ED', 'swarm of rats').
card_uid('swarm of rats'/'9ED', '9ED:Swarm of Rats:swarm of rats').
card_rarity('swarm of rats'/'9ED', 'Uncommon').
card_artist('swarm of rats'/'9ED', 'Kev Walker').
card_number('swarm of rats'/'9ED', '166').
card_flavor_text('swarm of rats'/'9ED', '\"Rats, rats, rats! Hundreds, thousands, millions of them, and every one a life.\"\n—Bram Stoker, Dracula').
card_multiverse_id('swarm of rats'/'9ED', '83292').

card_in_set('tanglebloom', '9ED').
card_original_type('tanglebloom'/'9ED', 'Artifact').
card_original_text('tanglebloom'/'9ED', '{1}, {T}: You gain 1 life.').
card_image_name('tanglebloom'/'9ED', 'tanglebloom').
card_uid('tanglebloom'/'9ED', '9ED:Tanglebloom:tanglebloom').
card_rarity('tanglebloom'/'9ED', 'Uncommon').
card_artist('tanglebloom'/'9ED', 'Val Mayerik').
card_number('tanglebloom'/'9ED', '311').
card_flavor_text('tanglebloom'/'9ED', 'The bloom contains no seeds. It\'s meant only to nourish the forest\'s inhabitants.').
card_multiverse_id('tanglebloom'/'9ED', '95221').

card_in_set('teferi\'s puzzle box', '9ED').
card_original_type('teferi\'s puzzle box'/'9ED', 'Artifact').
card_original_text('teferi\'s puzzle box'/'9ED', 'At the beginning of each player\'s draw step, that player puts the cards in his or her hand on the bottom of his or her library in any order, then draws that many cards.').
card_image_name('teferi\'s puzzle box'/'9ED', 'teferi\'s puzzle box').
card_uid('teferi\'s puzzle box'/'9ED', '9ED:Teferi\'s Puzzle Box:teferi\'s puzzle box').
card_rarity('teferi\'s puzzle box'/'9ED', 'Rare').
card_artist('teferi\'s puzzle box'/'9ED', 'Donato Giancola').
card_number('teferi\'s puzzle box'/'9ED', '312').
card_multiverse_id('teferi\'s puzzle box'/'9ED', '83294').

card_in_set('telepathy', '9ED').
card_original_type('telepathy'/'9ED', 'Enchantment').
card_original_text('telepathy'/'9ED', 'Your opponents play with their hands revealed.').
card_image_name('telepathy'/'9ED', 'telepathy').
card_uid('telepathy'/'9ED', '9ED:Telepathy:telepathy').
card_rarity('telepathy'/'9ED', 'Uncommon').
card_artist('telepathy'/'9ED', 'Matthew D. Wilson').
card_number('telepathy'/'9ED', '101').
card_flavor_text('telepathy'/'9ED', '\"Secrets? What secrets?\"').
card_multiverse_id('telepathy'/'9ED', '83295').

card_in_set('tempest of light', '9ED').
card_original_type('tempest of light'/'9ED', 'Instant').
card_original_text('tempest of light'/'9ED', 'Destroy all enchantments.').
card_image_name('tempest of light'/'9ED', 'tempest of light').
card_uid('tempest of light'/'9ED', '9ED:Tempest of Light:tempest of light').
card_rarity('tempest of light'/'9ED', 'Uncommon').
card_artist('tempest of light'/'9ED', 'Wayne England').
card_number('tempest of light'/'9ED', '50').
card_flavor_text('tempest of light'/'9ED', '\"Let everything return to its true nature, so that destiny may take its course.\"').
card_multiverse_id('tempest of light'/'9ED', '82976').

card_in_set('temporal adept', '9ED').
card_original_type('temporal adept'/'9ED', 'Creature — Human Wizard').
card_original_text('temporal adept'/'9ED', '{U}{U}{U}, {T}: Return target permanent to its owner\'s hand.').
card_image_name('temporal adept'/'9ED', 'temporal adept').
card_uid('temporal adept'/'9ED', '9ED:Temporal Adept:temporal adept').
card_rarity('temporal adept'/'9ED', 'Rare').
card_artist('temporal adept'/'9ED', 'Mark Tedin').
card_number('temporal adept'/'9ED', '102').
card_flavor_text('temporal adept'/'9ED', '\"If yesterday was two days ago tomorrow, will the day after tomorrow be today or yesterday?\"\n—Temporal Manipulation 101 final exam,\nTolarian Academy').
card_multiverse_id('temporal adept'/'9ED', '84141').

card_in_set('thieving magpie', '9ED').
card_original_type('thieving magpie'/'9ED', 'Creature — Bird').
card_original_text('thieving magpie'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nWhenever Thieving Magpie deals damage to an opponent, you draw a card.').
card_image_name('thieving magpie'/'9ED', 'thieving magpie').
card_uid('thieving magpie'/'9ED', '9ED:Thieving Magpie:thieving magpie').
card_rarity('thieving magpie'/'9ED', 'Uncommon').
card_artist('thieving magpie'/'9ED', 'Una Fricker').
card_number('thieving magpie'/'9ED', '103').
card_flavor_text('thieving magpie'/'9ED', 'Other birds collect twigs for their nests. Magpies steal jewels for theirs.').
card_multiverse_id('thieving magpie'/'9ED', '83298').

card_in_set('thought courier', '9ED').
card_original_type('thought courier'/'9ED', 'Creature — Human Wizard').
card_original_text('thought courier'/'9ED', '{T}: Draw a card, then discard a card.').
card_image_name('thought courier'/'9ED', 'thought courier').
card_uid('thought courier'/'9ED', '9ED:Thought Courier:thought courier').
card_rarity('thought courier'/'9ED', 'Uncommon').
card_artist('thought courier'/'9ED', 'Stephen Tappin').
card_number('thought courier'/'9ED', '104').
card_flavor_text('thought courier'/'9ED', '\"There\'s an assortment to satisfy all your needs: deep, fleeting, dirty, pure, scientific, philosophical, after, and even fore. For a price.\"').
card_multiverse_id('thought courier'/'9ED', '82980').

card_in_set('thran golem', '9ED').
card_original_type('thran golem'/'9ED', 'Artifact Creature — Golem').
card_original_text('thran golem'/'9ED', 'As long as Thran Golem is enchanted, it gets +2/+2 and has flying, first strike, and trample.').
card_image_name('thran golem'/'9ED', 'thran golem').
card_uid('thran golem'/'9ED', '9ED:Thran Golem:thran golem').
card_rarity('thran golem'/'9ED', 'Rare').
card_artist('thran golem'/'9ED', 'Ron Spears').
card_number('thran golem'/'9ED', '313').
card_flavor_text('thran golem'/'9ED', 'As a beaver\'s teeth grow unchecked without regular use, so does the golem\'s armor grow without regular battles.').
card_multiverse_id('thran golem'/'9ED', '83202').

card_in_set('threaten', '9ED').
card_original_type('threaten'/'9ED', 'Sorcery').
card_original_text('threaten'/'9ED', 'Untap target creature and gain control of it until end of turn. That creature gains haste until end of turn. (It may attack this turn.)').
card_image_name('threaten'/'9ED', 'threaten').
card_uid('threaten'/'9ED', '9ED:Threaten:threaten').
card_rarity('threaten'/'9ED', 'Uncommon').
card_artist('threaten'/'9ED', 'Pete Venters').
card_number('threaten'/'9ED', '223').
card_flavor_text('threaten'/'9ED', 'Goblins\' motivational techniques are crude, but effective.').
card_multiverse_id('threaten'/'9ED', '83472').

card_in_set('thundermare', '9ED').
card_original_type('thundermare'/'9ED', 'Creature — Thundermare').
card_original_text('thundermare'/'9ED', 'Haste (This creature may attack the turn it comes under your control.)\nWhen Thundermare comes into play, tap all other creatures.').
card_image_name('thundermare'/'9ED', 'thundermare').
card_uid('thundermare'/'9ED', '9ED:Thundermare:thundermare').
card_rarity('thundermare'/'9ED', 'Rare').
card_artist('thundermare'/'9ED', 'Bob Eggleton').
card_number('thundermare'/'9ED', '224').
card_flavor_text('thundermare'/'9ED', 'Its hooves strike lightning and the thunder follows quickly after.').
card_multiverse_id('thundermare'/'9ED', '84502').

card_in_set('tidal kraken', '9ED').
card_original_type('tidal kraken'/'9ED', 'Creature — Kraken').
card_original_text('tidal kraken'/'9ED', 'Tidal Kraken is unblockable.').
card_image_name('tidal kraken'/'9ED', 'tidal kraken').
card_uid('tidal kraken'/'9ED', '9ED:Tidal Kraken:tidal kraken').
card_rarity('tidal kraken'/'9ED', 'Rare').
card_artist('tidal kraken'/'9ED', 'Christopher Moeller').
card_number('tidal kraken'/'9ED', '105').
card_flavor_text('tidal kraken'/'9ED', 'To merfolk, pirates are a nuisance. To pirates, merfolk are a threat. To the kraken, they\'re both appetizers.').
card_multiverse_id('tidal kraken'/'9ED', '83301').

card_in_set('tidings', '9ED').
card_original_type('tidings'/'9ED', 'Sorcery').
card_original_text('tidings'/'9ED', 'Draw four cards.').
card_image_name('tidings'/'9ED', 'tidings').
card_uid('tidings'/'9ED', '9ED:Tidings:tidings').
card_rarity('tidings'/'9ED', 'Uncommon').
card_artist('tidings'/'9ED', 'Pete Venters').
card_number('tidings'/'9ED', '106').
card_flavor_text('tidings'/'9ED', '\"Though the letter was many pages long, I could tell all I needed to know from one look at the messenger\'s face.\"\n—Barrin, master wizard').
card_multiverse_id('tidings'/'9ED', '83036').

card_in_set('time ebb', '9ED').
card_original_type('time ebb'/'9ED', 'Sorcery').
card_original_text('time ebb'/'9ED', 'Put target creature on top of its owner\'s library.').
card_image_name('time ebb'/'9ED', 'time ebb').
card_uid('time ebb'/'9ED', '9ED:Time Ebb:time ebb').
card_rarity('time ebb'/'9ED', 'Common').
card_artist('time ebb'/'9ED', 'Thomas M. Baxa').
card_number('time ebb'/'9ED', '107').
card_flavor_text('time ebb'/'9ED', 'Like the tide, time both ebbs and flows.').
card_multiverse_id('time ebb'/'9ED', '84507').

card_in_set('trade routes', '9ED').
card_original_type('trade routes'/'9ED', 'Enchantment').
card_original_text('trade routes'/'9ED', '{1}: Return target land you control to its owner\'s hand.\n{1}, Discard a land card: Draw a card.').
card_image_name('trade routes'/'9ED', 'trade routes').
card_uid('trade routes'/'9ED', '9ED:Trade Routes:trade routes').
card_rarity('trade routes'/'9ED', 'Rare').
card_artist('trade routes'/'9ED', 'Matt Cavotta').
card_number('trade routes'/'9ED', '108').
card_flavor_text('trade routes'/'9ED', 'The wise, the righteous, the mighty—the merchant feeds them all.').
card_multiverse_id('trade routes'/'9ED', '83302').

card_in_set('trained armodon', '9ED').
card_original_type('trained armodon'/'9ED', 'Creature — Elephant').
card_original_text('trained armodon'/'9ED', '').
card_image_name('trained armodon'/'9ED', 'trained armodon').
card_uid('trained armodon'/'9ED', '9ED:Trained Armodon:trained armodon').
card_rarity('trained armodon'/'9ED', 'Common').
card_artist('trained armodon'/'9ED', 'Gary Leach').
card_number('trained armodon'/'9ED', '274').
card_flavor_text('trained armodon'/'9ED', 'Armodons are trained to step on things. Enemy things.').
card_multiverse_id('trained armodon'/'9ED', '83303').

card_in_set('traumatize', '9ED').
card_original_type('traumatize'/'9ED', 'Sorcery').
card_original_text('traumatize'/'9ED', 'Target player puts the top half of his or her library, rounded down, into his or her graveyard.').
card_image_name('traumatize'/'9ED', 'traumatize').
card_uid('traumatize'/'9ED', '9ED:Traumatize:traumatize').
card_rarity('traumatize'/'9ED', 'Rare').
card_artist('traumatize'/'9ED', 'Greg Staples').
card_number('traumatize'/'9ED', '109').
card_flavor_text('traumatize'/'9ED', 'You won\'t know what you\'re missing.').
card_multiverse_id('traumatize'/'9ED', '84119').

card_in_set('treasure trove', '9ED').
card_original_type('treasure trove'/'9ED', 'Enchantment').
card_original_text('treasure trove'/'9ED', '{2}{U}{U}: Draw a card.').
card_image_name('treasure trove'/'9ED', 'treasure trove').
card_uid('treasure trove'/'9ED', '9ED:Treasure Trove:treasure trove').
card_rarity('treasure trove'/'9ED', 'Uncommon').
card_artist('treasure trove'/'9ED', 'Brian Despain').
card_number('treasure trove'/'9ED', '110').
card_flavor_text('treasure trove'/'9ED', '\"Wealth means power; the power to subdue, to crush, to exploit, the power to enslave, to outrage, to degrade.\"\n—Emma Goldman, \"Anarchism\"').
card_multiverse_id('treasure trove'/'9ED', '83304').

card_in_set('tree monkey', '9ED').
card_original_type('tree monkey'/'9ED', 'Creature — Ape').
card_original_text('tree monkey'/'9ED', 'Tree Monkey can block as though it had flying.').
card_image_name('tree monkey'/'9ED', 'tree monkey').
card_uid('tree monkey'/'9ED', '9ED:Tree Monkey:tree monkey').
card_rarity('tree monkey'/'9ED', 'Common').
card_artist('tree monkey'/'9ED', 'John Matson').
card_number('tree monkey'/'9ED', '275').
card_flavor_text('tree monkey'/'9ED', 'The vines and branches of the jungle canopy are all the wings it needs.').
card_multiverse_id('tree monkey'/'9ED', '84665').

card_in_set('treetop bracers', '9ED').
card_original_type('treetop bracers'/'9ED', 'Enchantment — Aura').
card_original_text('treetop bracers'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +1/+1 and can\'t be blocked except by creatures with flying.').
card_image_name('treetop bracers'/'9ED', 'treetop bracers').
card_uid('treetop bracers'/'9ED', '9ED:Treetop Bracers:treetop bracers').
card_rarity('treetop bracers'/'9ED', 'Common').
card_artist('treetop bracers'/'9ED', 'Heather Hudson').
card_number('treetop bracers'/'9ED', '276').
card_flavor_text('treetop bracers'/'9ED', 'Gravity is truly what you make of it.').
card_multiverse_id('treetop bracers'/'9ED', '84053').

card_in_set('underground river', '9ED').
card_original_type('underground river'/'9ED', 'Land').
card_original_text('underground river'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Underground River deals 1 damage to you.').
card_image_name('underground river'/'9ED', 'underground river').
card_uid('underground river'/'9ED', '9ED:Underground River:underground river').
card_rarity('underground river'/'9ED', 'Rare').
card_artist('underground river'/'9ED', 'Andrew Goldhawk').
card_number('underground river'/'9ED', '326').
card_multiverse_id('underground river'/'9ED', '84566').

card_in_set('underworld dreams', '9ED').
card_original_type('underworld dreams'/'9ED', 'Enchantment').
card_original_text('underworld dreams'/'9ED', 'Whenever an opponent draws a card, Underworld Dreams deals 1 damage to him or her.').
card_image_name('underworld dreams'/'9ED', 'underworld dreams').
card_uid('underworld dreams'/'9ED', '9ED:Underworld Dreams:underworld dreams').
card_rarity('underworld dreams'/'9ED', 'Rare').
card_artist('underworld dreams'/'9ED', 'Carl Critchlow').
card_number('underworld dreams'/'9ED', '167').
card_flavor_text('underworld dreams'/'9ED', '\"In the drowsy dark cave of the mind, dreams build their nest with fragments dropped from day\'s caravan.\"\n—Rabindranath Tagore').
card_multiverse_id('underworld dreams'/'9ED', '83309').

card_in_set('unholy strength', '9ED').
card_original_type('unholy strength'/'9ED', 'Enchantment — Aura').
card_original_text('unholy strength'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +2/+1.').
card_image_name('unholy strength'/'9ED', 'unholy strength').
card_uid('unholy strength'/'9ED', '9ED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'9ED', 'Common').
card_artist('unholy strength'/'9ED', 'Puddnhead').
card_number('unholy strength'/'9ED', '168').
card_flavor_text('unholy strength'/'9ED', 'Such power grows the body as it shrinks the soul.').
card_multiverse_id('unholy strength'/'9ED', '83310').

card_in_set('ur-golem\'s eye', '9ED').
card_original_type('ur-golem\'s eye'/'9ED', 'Artifact').
card_original_text('ur-golem\'s eye'/'9ED', '{T}: Add {2} to your mana pool.').
card_image_name('ur-golem\'s eye'/'9ED', 'ur-golem\'s eye').
card_uid('ur-golem\'s eye'/'9ED', '9ED:Ur-Golem\'s Eye:ur-golem\'s eye').
card_rarity('ur-golem\'s eye'/'9ED', 'Uncommon').
card_artist('ur-golem\'s eye'/'9ED', 'Heather Hudson').
card_number('ur-golem\'s eye'/'9ED', '314').
card_flavor_text('ur-golem\'s eye'/'9ED', 'It still stares accusingly at Memnarch, though millennia have passed since he separated the eye from its body.').
card_multiverse_id('ur-golem\'s eye'/'9ED', '83510').

card_in_set('urza\'s mine', '9ED').
card_original_type('urza\'s mine'/'9ED', 'Land — Urza\'s Mine').
card_original_text('urza\'s mine'/'9ED', '{T}: Add {1} to your mana pool. If you control an Urza\'s Power-Plant and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s mine'/'9ED', 'urza\'s mine').
card_uid('urza\'s mine'/'9ED', '9ED:Urza\'s Mine:urza\'s mine').
card_rarity('urza\'s mine'/'9ED', 'Uncommon').
card_artist('urza\'s mine'/'9ED', 'Brian Snõddy').
card_number('urza\'s mine'/'9ED', '327').
card_multiverse_id('urza\'s mine'/'9ED', '83314').

card_in_set('urza\'s power plant', '9ED').
card_original_type('urza\'s power plant'/'9ED', 'Land — Urza\'s Power-Plant').
card_original_text('urza\'s power plant'/'9ED', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Tower, add {2} to your mana pool instead.').
card_image_name('urza\'s power plant'/'9ED', 'urza\'s power plant').
card_uid('urza\'s power plant'/'9ED', '9ED:Urza\'s Power Plant:urza\'s power plant').
card_rarity('urza\'s power plant'/'9ED', 'Uncommon').
card_artist('urza\'s power plant'/'9ED', 'Brian Snõddy').
card_number('urza\'s power plant'/'9ED', '328').
card_multiverse_id('urza\'s power plant'/'9ED', '83315').

card_in_set('urza\'s tower', '9ED').
card_original_type('urza\'s tower'/'9ED', 'Land — Urza\'s Tower').
card_original_text('urza\'s tower'/'9ED', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Power-Plant, add {3} to your mana pool instead.').
card_image_name('urza\'s tower'/'9ED', 'urza\'s tower').
card_uid('urza\'s tower'/'9ED', '9ED:Urza\'s Tower:urza\'s tower').
card_rarity('urza\'s tower'/'9ED', 'Uncommon').
card_artist('urza\'s tower'/'9ED', 'Brian Snõddy').
card_number('urza\'s tower'/'9ED', '329').
card_multiverse_id('urza\'s tower'/'9ED', '83316').

card_in_set('utopia tree', '9ED').
card_original_type('utopia tree'/'9ED', 'Creature — Plant').
card_original_text('utopia tree'/'9ED', '{T}: Add one mana of any color to your mana pool.').
card_image_name('utopia tree'/'9ED', 'utopia tree').
card_uid('utopia tree'/'9ED', '9ED:Utopia Tree:utopia tree').
card_rarity('utopia tree'/'9ED', 'Rare').
card_artist('utopia tree'/'9ED', 'Gary Ruddell').
card_number('utopia tree'/'9ED', '277').
card_flavor_text('utopia tree'/'9ED', 'The fruit of this fabled tree takes on the flavor of whatever food you love most.').
card_multiverse_id('utopia tree'/'9ED', '84434').

card_in_set('venerable monk', '9ED').
card_original_type('venerable monk'/'9ED', 'Creature — Human Monk Cleric').
card_original_text('venerable monk'/'9ED', 'When Venerable Monk comes into play, you gain 2 life.').
card_image_name('venerable monk'/'9ED', 'venerable monk').
card_uid('venerable monk'/'9ED', '9ED:Venerable Monk:venerable monk').
card_rarity('venerable monk'/'9ED', 'Common').
card_artist('venerable monk'/'9ED', 'D. Alexander Gregory').
card_number('venerable monk'/'9ED', '51').
card_flavor_text('venerable monk'/'9ED', 'Age wears the flesh but galvanizes the soul.').
card_multiverse_id('venerable monk'/'9ED', '83441').

card_in_set('vengeance', '9ED').
card_original_type('vengeance'/'9ED', 'Sorcery').
card_original_text('vengeance'/'9ED', 'Destroy target tapped creature.').
card_image_name('vengeance'/'9ED', 'vengeance').
card_uid('vengeance'/'9ED', '9ED:Vengeance:vengeance').
card_rarity('vengeance'/'9ED', 'Uncommon').
card_artist('vengeance'/'9ED', 'Paolo Parente').
card_number('vengeance'/'9ED', 'S2').
card_flavor_text('vengeance'/'9ED', 'Bitter as wormwood, sweet as mulled wine.').
card_multiverse_id('vengeance'/'9ED', '83319').

card_in_set('verdant force', '9ED').
card_original_type('verdant force'/'9ED', 'Creature — Elemental').
card_original_text('verdant force'/'9ED', 'At the beginning of each player\'s upkeep, put a 1/1 green Saproling creature token into play under your control.').
card_image_name('verdant force'/'9ED', 'verdant force').
card_uid('verdant force'/'9ED', '9ED:Verdant Force:verdant force').
card_rarity('verdant force'/'9ED', 'Rare').
card_artist('verdant force'/'9ED', 'DiTerlizzi').
card_number('verdant force'/'9ED', '278').
card_multiverse_id('verdant force'/'9ED', '83384').

card_in_set('verduran enchantress', '9ED').
card_original_type('verduran enchantress'/'9ED', 'Creature — Human Druid').
card_original_text('verduran enchantress'/'9ED', 'Whenever you play an enchantment spell, you may draw a card.').
card_image_name('verduran enchantress'/'9ED', 'verduran enchantress').
card_uid('verduran enchantress'/'9ED', '9ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'9ED', 'Rare').
card_artist('verduran enchantress'/'9ED', 'Rob Alexander').
card_number('verduran enchantress'/'9ED', '279').
card_flavor_text('verduran enchantress'/'9ED', '\"Graceful? Yes. Beautiful? Absolutely. Harmless? Definitely not.\"\n—Fyndhorn elder').
card_multiverse_id('verduran enchantress'/'9ED', '83320').

card_in_set('veteran cavalier', '9ED').
card_original_type('veteran cavalier'/'9ED', 'Creature — Human Knight').
card_original_text('veteran cavalier'/'9ED', 'Vigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('veteran cavalier'/'9ED', 'veteran cavalier').
card_uid('veteran cavalier'/'9ED', '9ED:Veteran Cavalier:veteran cavalier').
card_rarity('veteran cavalier'/'9ED', 'Common').
card_artist('veteran cavalier'/'9ED', 'rk post').
card_number('veteran cavalier'/'9ED', '52').
card_flavor_text('veteran cavalier'/'9ED', 'Spirit is the sword and experience the sharpening stone.\n—Arabian proverb').
card_multiverse_id('veteran cavalier'/'9ED', '84622').

card_in_set('viashino sandstalker', '9ED').
card_original_type('viashino sandstalker'/'9ED', 'Creature — Viashino Warrior').
card_original_text('viashino sandstalker'/'9ED', 'Haste (This creature may attack the turn it comes under your control.)\nAt end of turn, return Viashino Sandstalker to its owner\'s hand. (Return it only if it\'s in play.)').
card_image_name('viashino sandstalker'/'9ED', 'viashino sandstalker').
card_uid('viashino sandstalker'/'9ED', '9ED:Viashino Sandstalker:viashino sandstalker').
card_rarity('viashino sandstalker'/'9ED', 'Uncommon').
card_artist('viashino sandstalker'/'9ED', 'Andrew Robinson').
card_number('viashino sandstalker'/'9ED', '225').
card_flavor_text('viashino sandstalker'/'9ED', '\"Some believe sandstalkers to be illusions; those with scars know better.\"\n—Zhalfirin Guide to the Desert').
card_multiverse_id('viashino sandstalker'/'9ED', '83323').

card_in_set('viridian shaman', '9ED').
card_original_type('viridian shaman'/'9ED', 'Creature — Elf Shaman').
card_original_text('viridian shaman'/'9ED', 'When Viridian Shaman comes into play, destroy target artifact.').
card_image_name('viridian shaman'/'9ED', 'viridian shaman').
card_uid('viridian shaman'/'9ED', '9ED:Viridian Shaman:viridian shaman').
card_rarity('viridian shaman'/'9ED', 'Uncommon').
card_artist('viridian shaman'/'9ED', 'Scott M. Fischer').
card_number('viridian shaman'/'9ED', '280').
card_flavor_text('viridian shaman'/'9ED', 'She stands as a living symbol of the natural world and an enemy of the forces that threaten it.').
card_multiverse_id('viridian shaman'/'9ED', '83073').

card_in_set('vizzerdrix', '9ED').
card_original_type('vizzerdrix'/'9ED', 'Creature — Beast').
card_original_text('vizzerdrix'/'9ED', '').
card_image_name('vizzerdrix'/'9ED', 'vizzerdrix').
card_uid('vizzerdrix'/'9ED', '9ED:Vizzerdrix:vizzerdrix').
card_rarity('vizzerdrix'/'9ED', 'Rare').
card_artist('vizzerdrix'/'9ED', 'Dave Dorman').
card_number('vizzerdrix'/'9ED', 'S7').
card_flavor_text('vizzerdrix'/'9ED', 'A bored wizard once created a vizzerdrix out of a bunny and a piranha. He never made that mistake again.').
card_multiverse_id('vizzerdrix'/'9ED', '94911').

card_in_set('volcanic hammer', '9ED').
card_original_type('volcanic hammer'/'9ED', 'Sorcery').
card_original_text('volcanic hammer'/'9ED', 'Volcanic Hammer deals 3 damage to target creature or player.').
card_image_name('volcanic hammer'/'9ED', 'volcanic hammer').
card_uid('volcanic hammer'/'9ED', '9ED:Volcanic Hammer:volcanic hammer').
card_rarity('volcanic hammer'/'9ED', 'Common').
card_artist('volcanic hammer'/'9ED', 'Ben Thompson').
card_number('volcanic hammer'/'9ED', '226').
card_flavor_text('volcanic hammer'/'9ED', 'Fire finds its form in the heat of the forge.').
card_multiverse_id('volcanic hammer'/'9ED', '83327').

card_in_set('vulshok morningstar', '9ED').
card_original_type('vulshok morningstar'/'9ED', 'Artifact — Equipment').
card_original_text('vulshok morningstar'/'9ED', 'Equipped creature gets +2/+2.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('vulshok morningstar'/'9ED', 'vulshok morningstar').
card_uid('vulshok morningstar'/'9ED', '9ED:Vulshok Morningstar:vulshok morningstar').
card_rarity('vulshok morningstar'/'9ED', 'Uncommon').
card_artist('vulshok morningstar'/'9ED', 'David Martin').
card_number('vulshok morningstar'/'9ED', '315').
card_multiverse_id('vulshok morningstar'/'9ED', '83447').

card_in_set('wanderguard sentry', '9ED').
card_original_type('wanderguard sentry'/'9ED', 'Creature — Drone').
card_original_text('wanderguard sentry'/'9ED', 'When Wanderguard Sentry comes into play, look at target opponent\'s hand.').
card_image_name('wanderguard sentry'/'9ED', 'wanderguard sentry').
card_uid('wanderguard sentry'/'9ED', '9ED:Wanderguard Sentry:wanderguard sentry').
card_rarity('wanderguard sentry'/'9ED', 'Common').
card_artist('wanderguard sentry'/'9ED', 'Luca Zontini').
card_number('wanderguard sentry'/'9ED', '111').
card_flavor_text('wanderguard sentry'/'9ED', 'It allows those with dangerous weapons to pass. But not those with dangerous thoughts.').
card_multiverse_id('wanderguard sentry'/'9ED', '84561').

card_in_set('warrior\'s honor', '9ED').
card_original_type('warrior\'s honor'/'9ED', 'Instant').
card_original_text('warrior\'s honor'/'9ED', 'Creatures you control get +1/+1 until end of turn.').
card_image_name('warrior\'s honor'/'9ED', 'warrior\'s honor').
card_uid('warrior\'s honor'/'9ED', '9ED:Warrior\'s Honor:warrior\'s honor').
card_rarity('warrior\'s honor'/'9ED', 'Common').
card_artist('warrior\'s honor'/'9ED', 'D. Alexander Gregory').
card_number('warrior\'s honor'/'9ED', '53').
card_flavor_text('warrior\'s honor'/'9ED', '\"No person was ever honored for what he received. Honor has been the reward for what he gave.\"\n—Calvin Coolidge').
card_multiverse_id('warrior\'s honor'/'9ED', '83219').

card_in_set('weathered wayfarer', '9ED').
card_original_type('weathered wayfarer'/'9ED', 'Creature — Human Nomad Cleric').
card_original_text('weathered wayfarer'/'9ED', '{W}, {T}: Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library. Play this ability only if an opponent controls more lands than you.').
card_image_name('weathered wayfarer'/'9ED', 'weathered wayfarer').
card_uid('weathered wayfarer'/'9ED', '9ED:Weathered Wayfarer:weathered wayfarer').
card_rarity('weathered wayfarer'/'9ED', 'Rare').
card_artist('weathered wayfarer'/'9ED', 'Greg & Tim Hildebrandt').
card_number('weathered wayfarer'/'9ED', '54').
card_multiverse_id('weathered wayfarer'/'9ED', '83311').

card_in_set('web', '9ED').
card_original_type('web'/'9ED', 'Enchantment — Aura').
card_original_text('web'/'9ED', 'Enchant creature (Target a creature as you play this. This card comes into play attached to that creature.)\nEnchanted creature gets +0/+2 and can block as though it had flying.').
card_image_name('web'/'9ED', 'web').
card_uid('web'/'9ED', '9ED:Web:web').
card_rarity('web'/'9ED', 'Uncommon').
card_artist('web'/'9ED', 'Hugh Jamieson').
card_number('web'/'9ED', '281').
card_multiverse_id('web'/'9ED', '83994').

card_in_set('weird harvest', '9ED').
card_original_type('weird harvest'/'9ED', 'Sorcery').
card_original_text('weird harvest'/'9ED', 'Each player may search his or her library for up to X creature cards, reveal those cards, and put them into his or her hand. Then each player who searched his or her library this way shuffles it.').
card_image_name('weird harvest'/'9ED', 'weird harvest').
card_uid('weird harvest'/'9ED', '9ED:Weird Harvest:weird harvest').
card_rarity('weird harvest'/'9ED', 'Rare').
card_artist('weird harvest'/'9ED', 'Bob Petillo').
card_number('weird harvest'/'9ED', '282').
card_flavor_text('weird harvest'/'9ED', 'Krosa\'s distorted groves bear strange fruit.').
card_multiverse_id('weird harvest'/'9ED', '83974').

card_in_set('whip sergeant', '9ED').
card_original_type('whip sergeant'/'9ED', 'Creature — Human Soldier').
card_original_text('whip sergeant'/'9ED', '{R}: Target creature gains haste until end of turn. (It may attack this turn.)').
card_image_name('whip sergeant'/'9ED', 'whip sergeant').
card_uid('whip sergeant'/'9ED', '9ED:Whip Sergeant:whip sergeant').
card_rarity('whip sergeant'/'9ED', 'Uncommon').
card_artist('whip sergeant'/'9ED', 'Paolo Parente').
card_number('whip sergeant'/'9ED', '227').
card_flavor_text('whip sergeant'/'9ED', 'Keldon soldiers learn to move faster than the whip.').
card_multiverse_id('whip sergeant'/'9ED', '83421').

card_in_set('wildfire', '9ED').
card_original_type('wildfire'/'9ED', 'Sorcery').
card_original_text('wildfire'/'9ED', 'Each player sacrifices four lands. Wildfire deals 4 damage to each creature.').
card_image_name('wildfire'/'9ED', 'wildfire').
card_uid('wildfire'/'9ED', '9ED:Wildfire:wildfire').
card_rarity('wildfire'/'9ED', 'Rare').
card_artist('wildfire'/'9ED', 'Carl Critchlow').
card_number('wildfire'/'9ED', '228').
card_flavor_text('wildfire'/'9ED', 'Fire is always at the top of the food chain, and it has a big appetite.').
card_multiverse_id('wildfire'/'9ED', '83483').

card_in_set('will-o\'-the-wisp', '9ED').
card_original_type('will-o\'-the-wisp'/'9ED', 'Creature — Spirit').
card_original_text('will-o\'-the-wisp'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\n{B}: Regenerate Will-o\'-the-Wisp. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('will-o\'-the-wisp'/'9ED', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'9ED', '9ED:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'9ED', 'Rare').
card_artist('will-o\'-the-wisp'/'9ED', 'Rob Alexander').
card_number('will-o\'-the-wisp'/'9ED', '169').
card_multiverse_id('will-o\'-the-wisp'/'9ED', '83411').

card_in_set('wind drake', '9ED').
card_original_type('wind drake'/'9ED', 'Creature — Drake').
card_original_text('wind drake'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)').
card_image_name('wind drake'/'9ED', 'wind drake').
card_uid('wind drake'/'9ED', '9ED:Wind Drake:wind drake').
card_rarity('wind drake'/'9ED', 'Common').
card_artist('wind drake'/'9ED', 'Tom Wänerstrand').
card_number('wind drake'/'9ED', '112').
card_flavor_text('wind drake'/'9ED', '\"But high she shoots through air and light,\nAbove all low delay,\nWhere nothing earthly bounds her flight,\nNor shadow dims her way.\"\n—Thomas Moore, \"Oh that I had Wings\"').
card_multiverse_id('wind drake'/'9ED', '83334').

card_in_set('withering gaze', '9ED').
card_original_type('withering gaze'/'9ED', 'Sorcery').
card_original_text('withering gaze'/'9ED', 'Target opponent reveals his or her hand. You draw a card for each Forest and green card in it.').
card_image_name('withering gaze'/'9ED', 'withering gaze').
card_uid('withering gaze'/'9ED', '9ED:Withering Gaze:withering gaze').
card_rarity('withering gaze'/'9ED', 'Uncommon').
card_artist('withering gaze'/'9ED', 'Randy Gallegos').
card_number('withering gaze'/'9ED', '113').
card_flavor_text('withering gaze'/'9ED', '\"Nature cares not for the knowledge to be gained from its study. Better to bleed it dry, lest its potential be wasted on lizards and leaves.\"').
card_multiverse_id('withering gaze'/'9ED', '83166').

card_in_set('wood elves', '9ED').
card_original_type('wood elves'/'9ED', 'Creature — Elf Scout').
card_original_text('wood elves'/'9ED', 'When Wood Elves comes into play, search your library for a Forest card and put that card into play. Then shuffle your library.').
card_image_name('wood elves'/'9ED', 'wood elves').
card_uid('wood elves'/'9ED', '9ED:Wood Elves:wood elves').
card_rarity('wood elves'/'9ED', 'Common').
card_artist('wood elves'/'9ED', 'Christopher Moeller').
card_number('wood elves'/'9ED', '283').
card_flavor_text('wood elves'/'9ED', 'Every branch a crossroads, every vine a swift steed.').
card_multiverse_id('wood elves'/'9ED', '83336').

card_in_set('worship', '9ED').
card_original_type('worship'/'9ED', 'Enchantment').
card_original_text('worship'/'9ED', 'If you control a creature, damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_image_name('worship'/'9ED', 'worship').
card_uid('worship'/'9ED', '9ED:Worship:worship').
card_rarity('worship'/'9ED', 'Rare').
card_artist('worship'/'9ED', 'Mark Zug').
card_number('worship'/'9ED', '55').
card_flavor_text('worship'/'9ED', '\"Believe in the ideal, not the idol.\"\n—Serra').
card_multiverse_id('worship'/'9ED', '83338').

card_in_set('wrath of god', '9ED').
card_original_type('wrath of god'/'9ED', 'Sorcery').
card_original_text('wrath of god'/'9ED', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'9ED', 'wrath of god').
card_uid('wrath of god'/'9ED', '9ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'9ED', 'Rare').
card_artist('wrath of god'/'9ED', 'Kev Walker').
card_number('wrath of god'/'9ED', '56').
card_multiverse_id('wrath of god'/'9ED', '83339').

card_in_set('wurm\'s tooth', '9ED').
card_original_type('wurm\'s tooth'/'9ED', 'Artifact').
card_original_text('wurm\'s tooth'/'9ED', 'Whenever a player plays a green spell, you may gain 1 life.').
card_image_name('wurm\'s tooth'/'9ED', 'wurm\'s tooth').
card_uid('wurm\'s tooth'/'9ED', '9ED:Wurm\'s Tooth:wurm\'s tooth').
card_rarity('wurm\'s tooth'/'9ED', 'Uncommon').
card_artist('wurm\'s tooth'/'9ED', 'Alan Pollack').
card_number('wurm\'s tooth'/'9ED', '316').
card_flavor_text('wurm\'s tooth'/'9ED', 'A wurm knows nothing of deception. If it opens its mouth, it plans to eat you.').
card_multiverse_id('wurm\'s tooth'/'9ED', '83470').

card_in_set('yavimaya coast', '9ED').
card_original_type('yavimaya coast'/'9ED', 'Land').
card_original_text('yavimaya coast'/'9ED', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {U} to your mana pool. Yavimaya Coast deals 1 damage to you.').
card_image_name('yavimaya coast'/'9ED', 'yavimaya coast').
card_uid('yavimaya coast'/'9ED', '9ED:Yavimaya Coast:yavimaya coast').
card_rarity('yavimaya coast'/'9ED', 'Rare').
card_artist('yavimaya coast'/'9ED', 'Anthony S. Waters').
card_number('yavimaya coast'/'9ED', '330').
card_multiverse_id('yavimaya coast'/'9ED', '84474').

card_in_set('yavimaya enchantress', '9ED').
card_original_type('yavimaya enchantress'/'9ED', 'Creature — Human Druid').
card_original_text('yavimaya enchantress'/'9ED', 'Yavimaya Enchantress gets +1/+1 for each enchantment in play.').
card_image_name('yavimaya enchantress'/'9ED', 'yavimaya enchantress').
card_uid('yavimaya enchantress'/'9ED', '9ED:Yavimaya Enchantress:yavimaya enchantress').
card_rarity('yavimaya enchantress'/'9ED', 'Uncommon').
card_artist('yavimaya enchantress'/'9ED', 'Matthew D. Wilson').
card_number('yavimaya enchantress'/'9ED', '284').
card_flavor_text('yavimaya enchantress'/'9ED', 'Her roots connect her to the forest\'s wishes.').
card_multiverse_id('yavimaya enchantress'/'9ED', '83341').

card_in_set('yawgmoth demon', '9ED').
card_original_type('yawgmoth demon'/'9ED', 'Creature — Demon').
card_original_text('yawgmoth demon'/'9ED', 'Flying (This creature can\'t be blocked except by creatures with flying.)\nFirst strike (This creature deals combat damage before creatures without first strike.)\nAt the beginning of your upkeep, you may sacrifice an artifact. If you don\'t, tap Yawgmoth Demon and it deals 2 damage to you.').
card_image_name('yawgmoth demon'/'9ED', 'yawgmoth demon').
card_uid('yawgmoth demon'/'9ED', '9ED:Yawgmoth Demon:yawgmoth demon').
card_rarity('yawgmoth demon'/'9ED', 'Rare').
card_artist('yawgmoth demon'/'9ED', 'Pete Venters').
card_number('yawgmoth demon'/'9ED', '170').
card_multiverse_id('yawgmoth demon'/'9ED', '84579').

card_in_set('zealous inquisitor', '9ED').
card_original_type('zealous inquisitor'/'9ED', 'Creature — Human Cleric').
card_original_text('zealous inquisitor'/'9ED', '{1}{W}: The next 1 damage that would be dealt to Zealous Inquisitor this turn is dealt to target creature instead.').
card_image_name('zealous inquisitor'/'9ED', 'zealous inquisitor').
card_uid('zealous inquisitor'/'9ED', '9ED:Zealous Inquisitor:zealous inquisitor').
card_rarity('zealous inquisitor'/'9ED', 'Uncommon').
card_artist('zealous inquisitor'/'9ED', 'Wayne England').
card_number('zealous inquisitor'/'9ED', '57').
card_flavor_text('zealous inquisitor'/'9ED', 'Wine and balm for the confessions of the repentant. Razors and irons for the confessions of the unrepentant.').
card_multiverse_id('zealous inquisitor'/'9ED', '84122').

card_in_set('zodiac monkey', '9ED').
card_original_type('zodiac monkey'/'9ED', 'Creature — Ape').
card_original_text('zodiac monkey'/'9ED', 'Forestwalk (This creature is unblockable as long as defending player controls a Forest.)').
card_image_name('zodiac monkey'/'9ED', 'zodiac monkey').
card_uid('zodiac monkey'/'9ED', '9ED:Zodiac Monkey:zodiac monkey').
card_rarity('zodiac monkey'/'9ED', 'Common').
card_artist('zodiac monkey'/'9ED', 'Richard Sardinha').
card_number('zodiac monkey'/'9ED', '285').
card_flavor_text('zodiac monkey'/'9ED', 'It lives mainly on fruit, leaves, and the occasional nose.').
card_multiverse_id('zodiac monkey'/'9ED', '83237').

card_in_set('zombify', '9ED').
card_original_type('zombify'/'9ED', 'Sorcery').
card_original_text('zombify'/'9ED', 'Return target creature card from your graveyard to play.').
card_image_name('zombify'/'9ED', 'zombify').
card_uid('zombify'/'9ED', '9ED:Zombify:zombify').
card_rarity('zombify'/'9ED', 'Uncommon').
card_artist('zombify'/'9ED', 'Mark Romanoski').
card_number('zombify'/'9ED', '171').
card_flavor_text('zombify'/'9ED', '\"The first birth celebrates life. The second birth mocks it.\"\n—Mystic elder').
card_multiverse_id('zombify'/'9ED', '83342').

card_in_set('zur\'s weirding', '9ED').
card_original_type('zur\'s weirding'/'9ED', 'Enchantment').
card_original_text('zur\'s weirding'/'9ED', 'Players play with their hands revealed.\nIf a player would draw a card, he or she reveals it instead. Then any other player may pay 2 life. If a player does, put that card into its owner\'s graveyard. Otherwise, that player draws the card.').
card_image_name('zur\'s weirding'/'9ED', 'zur\'s weirding').
card_uid('zur\'s weirding'/'9ED', '9ED:Zur\'s Weirding:zur\'s weirding').
card_rarity('zur\'s weirding'/'9ED', 'Rare').
card_artist('zur\'s weirding'/'9ED', 'rk post').
card_number('zur\'s weirding'/'9ED', '114').
card_multiverse_id('zur\'s weirding'/'9ED', '83343').
