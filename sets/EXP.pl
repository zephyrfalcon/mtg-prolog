% Zendikar Expedition

set('EXP').
set_name('EXP', 'Zendikar Expedition').
set_release_date('EXP', '2015-10-02').
set_border('EXP', 'black').
set_type('EXP', 'promo').

card_in_set('arid mesa', 'EXP').
card_original_type('arid mesa'/'EXP', 'Land').
card_original_text('arid mesa'/'EXP', '{T}, Pay 1 life, Sacrifice Arid Mesa: Search your library for a Mountain or Plains card and put it onto the battlefield. Then shuffle your library.').
card_image_name('arid mesa'/'EXP', 'arid mesa').
card_uid('arid mesa'/'EXP', 'EXP:Arid Mesa:arid mesa').
card_rarity('arid mesa'/'EXP', 'Mythic Rare').
card_artist('arid mesa'/'EXP', 'Ryan Yee').
card_number('arid mesa'/'EXP', '24').
card_multiverse_id('arid mesa'/'EXP', '405092').

card_in_set('blood crypt', 'EXP').
card_original_type('blood crypt'/'EXP', 'Land — Swamp Mountain').
card_original_text('blood crypt'/'EXP', 'As Blood Crypt enters the battlefield, you may pay 2 life. If you don\'t, Blood Crypt enters the battlefield tapped.').
card_image_name('blood crypt'/'EXP', 'blood crypt').
card_uid('blood crypt'/'EXP', 'EXP:Blood Crypt:blood crypt').
card_rarity('blood crypt'/'EXP', 'Mythic Rare').
card_artist('blood crypt'/'EXP', 'Min Yum').
card_number('blood crypt'/'EXP', '8').
card_multiverse_id('blood crypt'/'EXP', '405093').

card_in_set('bloodstained mire', 'EXP').
card_original_type('bloodstained mire'/'EXP', 'Land').
card_original_text('bloodstained mire'/'EXP', '{T}, Pay 1 life, Sacrifice Bloodstained Mire: Search your library for a Swamp or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_image_name('bloodstained mire'/'EXP', 'bloodstained mire').
card_uid('bloodstained mire'/'EXP', 'EXP:Bloodstained Mire:bloodstained mire').
card_rarity('bloodstained mire'/'EXP', 'Mythic Rare').
card_artist('bloodstained mire'/'EXP', 'Véronique Meignaud').
card_number('bloodstained mire'/'EXP', '18').
card_multiverse_id('bloodstained mire'/'EXP', '405094').

card_in_set('breeding pool', 'EXP').
card_original_type('breeding pool'/'EXP', 'Land — Forest Island').
card_original_text('breeding pool'/'EXP', 'As Breeding Pool enters the battlefield, you may pay 2 life. If you don\'t, Breeding Pool enters the battlefield tapped.').
card_image_name('breeding pool'/'EXP', 'breeding pool').
card_uid('breeding pool'/'EXP', 'EXP:Breeding Pool:breeding pool').
card_rarity('breeding pool'/'EXP', 'Mythic Rare').
card_artist('breeding pool'/'EXP', 'Noah Bradley').
card_number('breeding pool'/'EXP', '15').
card_multiverse_id('breeding pool'/'EXP', '405095').

card_in_set('canopy vista', 'EXP').
card_original_type('canopy vista'/'EXP', 'Land — Forest Plains').
card_original_text('canopy vista'/'EXP', 'Canopy Vista enters the battlefield tapped unless you control two or more basic lands.').
card_image_name('canopy vista'/'EXP', 'canopy vista').
card_uid('canopy vista'/'EXP', 'EXP:Canopy Vista:canopy vista').
card_rarity('canopy vista'/'EXP', 'Mythic Rare').
card_artist('canopy vista'/'EXP', 'Titus Lunter').
card_number('canopy vista'/'EXP', '5').
card_multiverse_id('canopy vista'/'EXP', '405096').

card_in_set('cinder glade', 'EXP').
card_original_type('cinder glade'/'EXP', 'Land — Mountain Forest').
card_original_text('cinder glade'/'EXP', 'Cinder Glade enters the battlefield tapped unless you control two or more basic lands.').
card_image_name('cinder glade'/'EXP', 'cinder glade').
card_uid('cinder glade'/'EXP', 'EXP:Cinder Glade:cinder glade').
card_rarity('cinder glade'/'EXP', 'Mythic Rare').
card_artist('cinder glade'/'EXP', 'Titus Lunter').
card_number('cinder glade'/'EXP', '4').
card_multiverse_id('cinder glade'/'EXP', '405097').

card_in_set('flooded strand', 'EXP').
card_original_type('flooded strand'/'EXP', 'Land').
card_original_text('flooded strand'/'EXP', '{T}, Pay 1 life, Sacrifice Flooded Strand: Search your library for a Plains or Island card and put it onto the battlefield. Then shuffle your library.').
card_image_name('flooded strand'/'EXP', 'flooded strand').
card_uid('flooded strand'/'EXP', 'EXP:Flooded Strand:flooded strand').
card_rarity('flooded strand'/'EXP', 'Mythic Rare').
card_artist('flooded strand'/'EXP', 'Véronique Meignaud').
card_number('flooded strand'/'EXP', '16').
card_multiverse_id('flooded strand'/'EXP', '405098').

card_in_set('godless shrine', 'EXP').
card_original_type('godless shrine'/'EXP', 'Land — Plains Swamp').
card_original_text('godless shrine'/'EXP', 'As Godless Shrine enters the battlefield, you may pay 2 life. If you don\'t, Godless Shrine enters the battlefield tapped.').
card_image_name('godless shrine'/'EXP', 'godless shrine').
card_uid('godless shrine'/'EXP', 'EXP:Godless Shrine:godless shrine').
card_rarity('godless shrine'/'EXP', 'Mythic Rare').
card_artist('godless shrine'/'EXP', 'Noah Bradley').
card_number('godless shrine'/'EXP', '11').
card_multiverse_id('godless shrine'/'EXP', '405099').

card_in_set('hallowed fountain', 'EXP').
card_original_type('hallowed fountain'/'EXP', 'Land — Plains Island').
card_original_text('hallowed fountain'/'EXP', 'As Hallowed Fountain enters the battlefield, you may pay 2 life. If you don\'t, Hallowed Fountain enters the battlefield tapped.').
card_image_name('hallowed fountain'/'EXP', 'hallowed fountain').
card_uid('hallowed fountain'/'EXP', 'EXP:Hallowed Fountain:hallowed fountain').
card_rarity('hallowed fountain'/'EXP', 'Mythic Rare').
card_artist('hallowed fountain'/'EXP', 'Min Yum').
card_number('hallowed fountain'/'EXP', '6').
card_multiverse_id('hallowed fountain'/'EXP', '405100').

card_in_set('marsh flats', 'EXP').
card_original_type('marsh flats'/'EXP', 'Land').
card_original_text('marsh flats'/'EXP', '{T}, Pay 1 life, Sacrifice Marsh Flats: Search your library for a Plains or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_image_name('marsh flats'/'EXP', 'marsh flats').
card_uid('marsh flats'/'EXP', 'EXP:Marsh Flats:marsh flats').
card_rarity('marsh flats'/'EXP', 'Mythic Rare').
card_artist('marsh flats'/'EXP', 'Ryan Yee').
card_number('marsh flats'/'EXP', '21').
card_multiverse_id('marsh flats'/'EXP', '405101').

card_in_set('misty rainforest', 'EXP').
card_original_type('misty rainforest'/'EXP', 'Land').
card_original_text('misty rainforest'/'EXP', '{T}, Pay 1 life, Sacrifice Misty Rainforest: Search your library for a Forest or Island card and put it onto the battlefield. Then shuffle your library.').
card_image_name('misty rainforest'/'EXP', 'misty rainforest').
card_uid('misty rainforest'/'EXP', 'EXP:Misty Rainforest:misty rainforest').
card_rarity('misty rainforest'/'EXP', 'Mythic Rare').
card_artist('misty rainforest'/'EXP', 'Ryan Yee').
card_number('misty rainforest'/'EXP', '25').
card_multiverse_id('misty rainforest'/'EXP', '405102').

card_in_set('overgrown tomb', 'EXP').
card_original_type('overgrown tomb'/'EXP', 'Land — Swamp Forest').
card_original_text('overgrown tomb'/'EXP', 'As Overgrown Tomb enters the battlefield, you may pay 2 life. If you don\'t, Overgrown Tomb enters the battlefield tapped.').
card_image_name('overgrown tomb'/'EXP', 'overgrown tomb').
card_uid('overgrown tomb'/'EXP', 'EXP:Overgrown Tomb:overgrown tomb').
card_rarity('overgrown tomb'/'EXP', 'Mythic Rare').
card_artist('overgrown tomb'/'EXP', 'Noah Bradley').
card_number('overgrown tomb'/'EXP', '13').
card_multiverse_id('overgrown tomb'/'EXP', '405103').

card_in_set('polluted delta', 'EXP').
card_original_type('polluted delta'/'EXP', 'Land').
card_original_text('polluted delta'/'EXP', '{T}, Pay 1 life, Sacrifice Polluted Delta: Search your library for an Island or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_image_name('polluted delta'/'EXP', 'polluted delta').
card_uid('polluted delta'/'EXP', 'EXP:Polluted Delta:polluted delta').
card_rarity('polluted delta'/'EXP', 'Mythic Rare').
card_artist('polluted delta'/'EXP', 'Véronique Meignaud').
card_number('polluted delta'/'EXP', '17').
card_multiverse_id('polluted delta'/'EXP', '405104').

card_in_set('prairie stream', 'EXP').
card_original_type('prairie stream'/'EXP', 'Land — Plains Island').
card_original_text('prairie stream'/'EXP', 'Prairie Stream enters the battlefield tapped unless you control two or more basic lands.').
card_image_name('prairie stream'/'EXP', 'prairie stream').
card_uid('prairie stream'/'EXP', 'EXP:Prairie Stream:prairie stream').
card_rarity('prairie stream'/'EXP', 'Mythic Rare').
card_artist('prairie stream'/'EXP', 'Titus Lunter').
card_number('prairie stream'/'EXP', '1').
card_multiverse_id('prairie stream'/'EXP', '405105').

card_in_set('sacred foundry', 'EXP').
card_original_type('sacred foundry'/'EXP', 'Land — Mountain Plains').
card_original_text('sacred foundry'/'EXP', 'As Sacred Foundry enters the battlefield, you may pay 2 life. If you don\'t, Sacred Foundry enters the battlefield tapped.').
card_image_name('sacred foundry'/'EXP', 'sacred foundry').
card_uid('sacred foundry'/'EXP', 'EXP:Sacred Foundry:sacred foundry').
card_rarity('sacred foundry'/'EXP', 'Mythic Rare').
card_artist('sacred foundry'/'EXP', 'Noah Bradley').
card_number('sacred foundry'/'EXP', '14').
card_multiverse_id('sacred foundry'/'EXP', '405106').

card_in_set('scalding tarn', 'EXP').
card_original_type('scalding tarn'/'EXP', 'Land').
card_original_text('scalding tarn'/'EXP', '{T}, Pay 1 life, Sacrifice Scalding Tarn: Search your library for an Island or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_image_name('scalding tarn'/'EXP', 'scalding tarn').
card_uid('scalding tarn'/'EXP', 'EXP:Scalding Tarn:scalding tarn').
card_rarity('scalding tarn'/'EXP', 'Mythic Rare').
card_artist('scalding tarn'/'EXP', 'Ryan Yee').
card_number('scalding tarn'/'EXP', '22').
card_multiverse_id('scalding tarn'/'EXP', '405107').

card_in_set('smoldering marsh', 'EXP').
card_original_type('smoldering marsh'/'EXP', 'Land — Swamp Mountain').
card_original_text('smoldering marsh'/'EXP', 'Smoldering Marsh enters the battlefield tapped unless you control two or more basic lands.').
card_image_name('smoldering marsh'/'EXP', 'smoldering marsh').
card_uid('smoldering marsh'/'EXP', 'EXP:Smoldering Marsh:smoldering marsh').
card_rarity('smoldering marsh'/'EXP', 'Mythic Rare').
card_artist('smoldering marsh'/'EXP', 'Titus Lunter').
card_number('smoldering marsh'/'EXP', '3').
card_multiverse_id('smoldering marsh'/'EXP', '405108').

card_in_set('steam vents', 'EXP').
card_original_type('steam vents'/'EXP', 'Land — Island Mountain').
card_original_text('steam vents'/'EXP', 'As Steam Vents enters the battlefield, you may pay 2 life. If you don\'t, Steam Vents enters the battlefield tapped.').
card_image_name('steam vents'/'EXP', 'steam vents').
card_uid('steam vents'/'EXP', 'EXP:Steam Vents:steam vents').
card_rarity('steam vents'/'EXP', 'Mythic Rare').
card_artist('steam vents'/'EXP', 'Noah Bradley').
card_number('steam vents'/'EXP', '12').
card_multiverse_id('steam vents'/'EXP', '405109').

card_in_set('stomping ground', 'EXP').
card_original_type('stomping ground'/'EXP', 'Land — Mountain Forest').
card_original_text('stomping ground'/'EXP', 'As Stomping Ground enters the battlefield, you may pay 2 life. If you don\'t, Stomping Ground enters the battlefield tapped.').
card_image_name('stomping ground'/'EXP', 'stomping ground').
card_uid('stomping ground'/'EXP', 'EXP:Stomping Ground:stomping ground').
card_rarity('stomping ground'/'EXP', 'Mythic Rare').
card_artist('stomping ground'/'EXP', 'Min Yum').
card_number('stomping ground'/'EXP', '9').
card_multiverse_id('stomping ground'/'EXP', '405110').

card_in_set('sunken hollow', 'EXP').
card_original_type('sunken hollow'/'EXP', 'Land — Island Swamp').
card_original_text('sunken hollow'/'EXP', 'Sunken Hollow enters the battlefield tapped unless you control two or more basic lands.').
card_image_name('sunken hollow'/'EXP', 'sunken hollow').
card_uid('sunken hollow'/'EXP', 'EXP:Sunken Hollow:sunken hollow').
card_rarity('sunken hollow'/'EXP', 'Mythic Rare').
card_artist('sunken hollow'/'EXP', 'Titus Lunter').
card_number('sunken hollow'/'EXP', '2').
card_multiverse_id('sunken hollow'/'EXP', '405111').

card_in_set('temple garden', 'EXP').
card_original_type('temple garden'/'EXP', 'Land — Forest Plains').
card_original_text('temple garden'/'EXP', 'As Temple Garden enters the battlefield, you may pay 2 life. If you don\'t, Temple Garden enters the battlefield tapped.').
card_image_name('temple garden'/'EXP', 'temple garden').
card_uid('temple garden'/'EXP', 'EXP:Temple Garden:temple garden').
card_rarity('temple garden'/'EXP', 'Mythic Rare').
card_artist('temple garden'/'EXP', 'Min Yum').
card_number('temple garden'/'EXP', '10').
card_multiverse_id('temple garden'/'EXP', '405112').

card_in_set('verdant catacombs', 'EXP').
card_original_type('verdant catacombs'/'EXP', 'Land').
card_original_text('verdant catacombs'/'EXP', '{T}, Pay 1 life, Sacrifice Verdant Catacombs: Search your library for a Swamp or Forest card and put it onto the battlefield. Then shuffle your library.').
card_image_name('verdant catacombs'/'EXP', 'verdant catacombs').
card_uid('verdant catacombs'/'EXP', 'EXP:Verdant Catacombs:verdant catacombs').
card_rarity('verdant catacombs'/'EXP', 'Mythic Rare').
card_artist('verdant catacombs'/'EXP', 'Ryan Yee').
card_number('verdant catacombs'/'EXP', '23').
card_multiverse_id('verdant catacombs'/'EXP', '405113').

card_in_set('watery grave', 'EXP').
card_original_type('watery grave'/'EXP', 'Land — Island Swamp').
card_original_text('watery grave'/'EXP', 'As Watery Grave enters the battlefield, you may pay 2 life. If you don\'t, Watery Grave enters the battlefield tapped.').
card_image_name('watery grave'/'EXP', 'watery grave').
card_uid('watery grave'/'EXP', 'EXP:Watery Grave:watery grave').
card_rarity('watery grave'/'EXP', 'Mythic Rare').
card_artist('watery grave'/'EXP', 'Min Yum').
card_number('watery grave'/'EXP', '7').
card_multiverse_id('watery grave'/'EXP', '405114').

card_in_set('windswept heath', 'EXP').
card_original_type('windswept heath'/'EXP', 'Land').
card_original_text('windswept heath'/'EXP', '{T}, Pay 1 life, Sacrifice Windswept Heath: Search your library for a Forest or Plains card and put it onto the battlefield. Then shuffle your library.').
card_image_name('windswept heath'/'EXP', 'windswept heath').
card_uid('windswept heath'/'EXP', 'EXP:Windswept Heath:windswept heath').
card_rarity('windswept heath'/'EXP', 'Mythic Rare').
card_artist('windswept heath'/'EXP', 'Véronique Meignaud').
card_number('windswept heath'/'EXP', '20').
card_multiverse_id('windswept heath'/'EXP', '405115').

card_in_set('wooded foothills', 'EXP').
card_original_type('wooded foothills'/'EXP', 'Land').
card_original_text('wooded foothills'/'EXP', '{T}, Pay 1 life, Sacrifice Wooded Foothills: Search your library for a Mountain or Forest card and put it onto the battlefield. Then shuffle your library.').
card_image_name('wooded foothills'/'EXP', 'wooded foothills').
card_uid('wooded foothills'/'EXP', 'EXP:Wooded Foothills:wooded foothills').
card_rarity('wooded foothills'/'EXP', 'Mythic Rare').
card_artist('wooded foothills'/'EXP', 'Véronique Meignaud').
card_number('wooded foothills'/'EXP', '19').
card_multiverse_id('wooded foothills'/'EXP', '405116').
