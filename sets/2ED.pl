% Unlimited Edition

set('2ED').
set_name('2ED', 'Unlimited Edition').
set_release_date('2ED', '1993-12-01').
set_border('2ED', 'white').
set_type('2ED', 'core').

card_in_set('air elemental', '2ED').
card_original_type('air elemental'/'2ED', 'Summon — Elemental').
card_original_text('air elemental'/'2ED', 'Flying').
card_image_name('air elemental'/'2ED', 'air elemental').
card_uid('air elemental'/'2ED', '2ED:Air Elemental:air elemental').
card_rarity('air elemental'/'2ED', 'Uncommon').
card_artist('air elemental'/'2ED', 'Richard Thomas').
card_flavor_text('air elemental'/'2ED', 'These spirits of the air are winsome and wild, and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').
card_multiverse_id('air elemental'/'2ED', '691').

card_in_set('ancestral recall', '2ED').
card_original_type('ancestral recall'/'2ED', 'Instant').
card_original_text('ancestral recall'/'2ED', 'Draw 3 cards or force opponent to draw 3 cards.').
card_image_name('ancestral recall'/'2ED', 'ancestral recall').
card_uid('ancestral recall'/'2ED', '2ED:Ancestral Recall:ancestral recall').
card_rarity('ancestral recall'/'2ED', 'Rare').
card_artist('ancestral recall'/'2ED', 'Mark Poole').
card_multiverse_id('ancestral recall'/'2ED', '692').

card_in_set('animate artifact', '2ED').
card_original_type('animate artifact'/'2ED', 'Enchant Non-Creature Artifact').
card_original_text('animate artifact'/'2ED', 'Target artifact is now a creature with both power and toughness equal to its casting cost; target retains all its original abilities as well. This will destroy artifacts with 0 casting cost.').
card_image_name('animate artifact'/'2ED', 'animate artifact').
card_uid('animate artifact'/'2ED', '2ED:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'2ED', 'Uncommon').
card_artist('animate artifact'/'2ED', 'Douglas Shuler').
card_multiverse_id('animate artifact'/'2ED', '693').

card_in_set('animate dead', '2ED').
card_original_type('animate dead'/'2ED', 'Enchant Dead Creature').
card_original_text('animate dead'/'2ED', 'Any creature in either player\'s graveyard comes into play on your side with -1 to its original power. If this enchantment is removed, or at end of game, target creature is returned to its owner\'s graveyard. Target creature may be killed as normal.').
card_image_name('animate dead'/'2ED', 'animate dead').
card_uid('animate dead'/'2ED', '2ED:Animate Dead:animate dead').
card_rarity('animate dead'/'2ED', 'Uncommon').
card_artist('animate dead'/'2ED', 'Anson Maddocks').
card_multiverse_id('animate dead'/'2ED', '645').

card_in_set('animate wall', '2ED').
card_original_type('animate wall'/'2ED', 'Enchant Wall').
card_original_text('animate wall'/'2ED', 'Target wall can now attack. Target wall\'s power and toughness are unchanged, even if its power is 0.').
card_image_name('animate wall'/'2ED', 'animate wall').
card_uid('animate wall'/'2ED', '2ED:Animate Wall:animate wall').
card_rarity('animate wall'/'2ED', 'Rare').
card_artist('animate wall'/'2ED', 'Dan Frazier').
card_multiverse_id('animate wall'/'2ED', '829').

card_in_set('ankh of mishra', '2ED').
card_original_type('ankh of mishra'/'2ED', 'Continuous Artifact').
card_original_text('ankh of mishra'/'2ED', 'Ankh does 2 damage to anyone who puts a new land into play.').
card_image_name('ankh of mishra'/'2ED', 'ankh of mishra').
card_uid('ankh of mishra'/'2ED', '2ED:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'2ED', 'Rare').
card_artist('ankh of mishra'/'2ED', 'Amy Weber').
card_multiverse_id('ankh of mishra'/'2ED', '598').

card_in_set('armageddon', '2ED').
card_original_type('armageddon'/'2ED', 'Sorcery').
card_original_text('armageddon'/'2ED', 'All lands in play are destroyed.').
card_image_name('armageddon'/'2ED', 'armageddon').
card_uid('armageddon'/'2ED', '2ED:Armageddon:armageddon').
card_rarity('armageddon'/'2ED', 'Rare').
card_artist('armageddon'/'2ED', 'Jesper Myrfors').
card_multiverse_id('armageddon'/'2ED', '830').

card_in_set('aspect of wolf', '2ED').
card_original_type('aspect of wolf'/'2ED', 'Enchant Creature').
card_original_text('aspect of wolf'/'2ED', 'Target creature\'s power and toughness are increased by half the number of forests you have in play, rounding down for power and up for toughness.').
card_image_name('aspect of wolf'/'2ED', 'aspect of wolf').
card_uid('aspect of wolf'/'2ED', '2ED:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'2ED', 'Rare').
card_artist('aspect of wolf'/'2ED', 'Jeff A. Menges').
card_multiverse_id('aspect of wolf'/'2ED', '737').

card_in_set('bad moon', '2ED').
card_original_type('bad moon'/'2ED', 'Enchantment').
card_original_text('bad moon'/'2ED', 'All black creatures in play gain +1/+1.').
card_image_name('bad moon'/'2ED', 'bad moon').
card_uid('bad moon'/'2ED', '2ED:Bad Moon:bad moon').
card_rarity('bad moon'/'2ED', 'Rare').
card_artist('bad moon'/'2ED', 'Jesper Myrfors').
card_multiverse_id('bad moon'/'2ED', '646').

card_in_set('badlands', '2ED').
card_original_type('badlands'/'2ED', 'Land').
card_original_text('badlands'/'2ED', 'Counts as both mountains and swamp and is affected by spells that affect either. Tap to add either {R} or {B} to your mana pool.').
card_image_name('badlands'/'2ED', 'badlands').
card_uid('badlands'/'2ED', '2ED:Badlands:badlands').
card_rarity('badlands'/'2ED', 'Rare').
card_artist('badlands'/'2ED', 'Rob Alexander').
card_multiverse_id('badlands'/'2ED', '878').

card_in_set('balance', '2ED').
card_original_type('balance'/'2ED', 'Sorcery').
card_original_text('balance'/'2ED', 'Whichever player has more lands in play must discard enough lands of his or her choice to equalize the number of lands both players have in play. Cards in hand and creatures in play must be equalized the same way. Creatures lost in this manner may not be regenerated.').
card_image_name('balance'/'2ED', 'balance').
card_uid('balance'/'2ED', '2ED:Balance:balance').
card_rarity('balance'/'2ED', 'Rare').
card_artist('balance'/'2ED', 'Mark Poole').
card_multiverse_id('balance'/'2ED', '831').

card_in_set('basalt monolith', '2ED').
card_original_type('basalt monolith'/'2ED', 'Mono Artifact').
card_original_text('basalt monolith'/'2ED', 'Tap to add 3 colorless mana to your mana pool. Does not untap as normal during untap phase; spend {3} to untap. Tapping this artifact can be played as an interrupt.').
card_image_name('basalt monolith'/'2ED', 'basalt monolith').
card_uid('basalt monolith'/'2ED', '2ED:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'2ED', 'Uncommon').
card_artist('basalt monolith'/'2ED', 'Jesper Myrfors').
card_multiverse_id('basalt monolith'/'2ED', '599').

card_in_set('bayou', '2ED').
card_original_type('bayou'/'2ED', 'Land').
card_original_text('bayou'/'2ED', 'Counts as both swamp and forest and is affected by spells that affect either. Tap to add either {B} or {G} to your mana pool.').
card_image_name('bayou'/'2ED', 'bayou').
card_uid('bayou'/'2ED', '2ED:Bayou:bayou').
card_rarity('bayou'/'2ED', 'Rare').
card_artist('bayou'/'2ED', 'Jesper Myrfors').
card_multiverse_id('bayou'/'2ED', '879').

card_in_set('benalish hero', '2ED').
card_original_type('benalish hero'/'2ED', 'Summon — Hero').
card_original_text('benalish hero'/'2ED', 'Bands').
card_image_name('benalish hero'/'2ED', 'benalish hero').
card_uid('benalish hero'/'2ED', '2ED:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'2ED', 'Common').
card_artist('benalish hero'/'2ED', 'Douglas Shuler').
card_flavor_text('benalish hero'/'2ED', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the hero.').
card_multiverse_id('benalish hero'/'2ED', '832').

card_in_set('berserk', '2ED').
card_original_type('berserk'/'2ED', 'Instant').
card_original_text('berserk'/'2ED', 'Until end of turn, target creature\'s current power doubles and it gains trample ability. If it attacks, target creature is destroyed at end of turn. This spell cannot be cast after current turn\'s attack is completed.').
card_image_name('berserk'/'2ED', 'berserk').
card_uid('berserk'/'2ED', '2ED:Berserk:berserk').
card_rarity('berserk'/'2ED', 'Uncommon').
card_artist('berserk'/'2ED', 'Dan Frazier').
card_multiverse_id('berserk'/'2ED', '738').

card_in_set('birds of paradise', '2ED').
card_original_type('birds of paradise'/'2ED', 'Summon — Mana Birds').
card_original_text('birds of paradise'/'2ED', 'Flying\nTap to add one mana of any color to your mana pool. This tap may be played as an interrupt.').
card_image_name('birds of paradise'/'2ED', 'birds of paradise').
card_uid('birds of paradise'/'2ED', '2ED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'2ED', 'Rare').
card_artist('birds of paradise'/'2ED', 'Mark Poole').
card_multiverse_id('birds of paradise'/'2ED', '739').

card_in_set('black knight', '2ED').
card_original_type('black knight'/'2ED', 'Summon — Knight').
card_original_text('black knight'/'2ED', 'Protection from white, first strike').
card_image_name('black knight'/'2ED', 'black knight').
card_uid('black knight'/'2ED', '2ED:Black Knight:black knight').
card_rarity('black knight'/'2ED', 'Uncommon').
card_artist('black knight'/'2ED', 'Jeff A. Menges').
card_flavor_text('black knight'/'2ED', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').
card_multiverse_id('black knight'/'2ED', '647').

card_in_set('black lotus', '2ED').
card_original_type('black lotus'/'2ED', 'Mono Artifact').
card_original_text('black lotus'/'2ED', 'Adds 3 mana of any single color of your choice to your mana pool, then is discarded. Tapping this artifact can be played as an interrupt.').
card_image_name('black lotus'/'2ED', 'black lotus').
card_uid('black lotus'/'2ED', '2ED:Black Lotus:black lotus').
card_rarity('black lotus'/'2ED', 'Rare').
card_artist('black lotus'/'2ED', 'Christopher Rush').
card_multiverse_id('black lotus'/'2ED', '600').

card_in_set('black vise', '2ED').
card_original_type('black vise'/'2ED', 'Continuous Artifact').
card_original_text('black vise'/'2ED', 'If opponent has more than four cards in hand during upkeep, black vise does 1 damage to opponent for each card in excess of four.').
card_image_name('black vise'/'2ED', 'black vise').
card_uid('black vise'/'2ED', '2ED:Black Vise:black vise').
card_rarity('black vise'/'2ED', 'Uncommon').
card_artist('black vise'/'2ED', 'Richard Thomas').
card_multiverse_id('black vise'/'2ED', '601').

card_in_set('black ward', '2ED').
card_original_type('black ward'/'2ED', 'Enchant Creature').
card_original_text('black ward'/'2ED', 'Target creature gains protection from black.').
card_image_name('black ward'/'2ED', 'black ward').
card_uid('black ward'/'2ED', '2ED:Black Ward:black ward').
card_rarity('black ward'/'2ED', 'Uncommon').
card_artist('black ward'/'2ED', 'Dan Frazier').
card_multiverse_id('black ward'/'2ED', '833').

card_in_set('blaze of glory', '2ED').
card_original_type('blaze of glory'/'2ED', 'Instant').
card_original_text('blaze of glory'/'2ED', 'Target defending creature can and must block all attacking creatures it can legally block. For example, a normal non-flying target defender can and must block all normal non-flying attackers at once, but it cannot block any flying attackers. Controller of target defender may distribute damage among attackers as desired. Play before defense is chosen.').
card_image_name('blaze of glory'/'2ED', 'blaze of glory').
card_uid('blaze of glory'/'2ED', '2ED:Blaze of Glory:blaze of glory').
card_rarity('blaze of glory'/'2ED', 'Rare').
card_artist('blaze of glory'/'2ED', 'Richard Thomas').
card_multiverse_id('blaze of glory'/'2ED', '834').

card_in_set('blessing', '2ED').
card_original_type('blessing'/'2ED', 'Enchant Creature').
card_original_text('blessing'/'2ED', '{W} Target creature gains +1/+1 until end of turn.').
card_image_name('blessing'/'2ED', 'blessing').
card_uid('blessing'/'2ED', '2ED:Blessing:blessing').
card_rarity('blessing'/'2ED', 'Rare').
card_artist('blessing'/'2ED', 'Julie Baroh').
card_multiverse_id('blessing'/'2ED', '835').

card_in_set('blue elemental blast', '2ED').
card_original_type('blue elemental blast'/'2ED', 'Interrupt').
card_original_text('blue elemental blast'/'2ED', 'Counters a red spell being cast or destroys a red card in play.').
card_image_name('blue elemental blast'/'2ED', 'blue elemental blast').
card_uid('blue elemental blast'/'2ED', '2ED:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'2ED', 'Common').
card_artist('blue elemental blast'/'2ED', 'Richard Thomas').
card_multiverse_id('blue elemental blast'/'2ED', '694').

card_in_set('blue ward', '2ED').
card_original_type('blue ward'/'2ED', 'Enchant Creature').
card_original_text('blue ward'/'2ED', 'Target creature gains protection from blue.').
card_image_name('blue ward'/'2ED', 'blue ward').
card_uid('blue ward'/'2ED', '2ED:Blue Ward:blue ward').
card_rarity('blue ward'/'2ED', 'Uncommon').
card_artist('blue ward'/'2ED', 'Dan Frazier').
card_multiverse_id('blue ward'/'2ED', '836').

card_in_set('bog wraith', '2ED').
card_original_type('bog wraith'/'2ED', 'Summon — Wraith').
card_original_text('bog wraith'/'2ED', 'Swampwalk').
card_image_name('bog wraith'/'2ED', 'bog wraith').
card_uid('bog wraith'/'2ED', '2ED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'2ED', 'Uncommon').
card_artist('bog wraith'/'2ED', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'2ED', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').
card_multiverse_id('bog wraith'/'2ED', '648').

card_in_set('braingeyser', '2ED').
card_original_type('braingeyser'/'2ED', 'Sorcery').
card_original_text('braingeyser'/'2ED', 'Draw X cards or force opponent to draw X cards.').
card_image_name('braingeyser'/'2ED', 'braingeyser').
card_uid('braingeyser'/'2ED', '2ED:Braingeyser:braingeyser').
card_rarity('braingeyser'/'2ED', 'Rare').
card_artist('braingeyser'/'2ED', 'Mark Tedin').
card_multiverse_id('braingeyser'/'2ED', '695').

card_in_set('burrowing', '2ED').
card_original_type('burrowing'/'2ED', 'Enchant Creature').
card_original_text('burrowing'/'2ED', 'Target creature gains mountainwalk.').
card_image_name('burrowing'/'2ED', 'burrowing').
card_uid('burrowing'/'2ED', '2ED:Burrowing:burrowing').
card_rarity('burrowing'/'2ED', 'Uncommon').
card_artist('burrowing'/'2ED', 'Mark Poole').
card_multiverse_id('burrowing'/'2ED', '783').

card_in_set('camouflage', '2ED').
card_original_type('camouflage'/'2ED', 'Instant').
card_original_text('camouflage'/'2ED', 'You may rearrange your attacking creatures and place them face down, revealing which is which only after defense is chosen. If this results in impossible blocks, such as non-flying creatures blocking flying creatures, illegal blockers cannot block this turn.').
card_image_name('camouflage'/'2ED', 'camouflage').
card_uid('camouflage'/'2ED', '2ED:Camouflage:camouflage').
card_rarity('camouflage'/'2ED', 'Uncommon').
card_artist('camouflage'/'2ED', 'Jesper Myrfors').
card_multiverse_id('camouflage'/'2ED', '740').

card_in_set('castle', '2ED').
card_original_type('castle'/'2ED', 'Enchantment').
card_original_text('castle'/'2ED', 'Your untapped creatures gain +0/+2. Attacking creatures lose this bonus.').
card_image_name('castle'/'2ED', 'castle').
card_uid('castle'/'2ED', '2ED:Castle:castle').
card_rarity('castle'/'2ED', 'Uncommon').
card_artist('castle'/'2ED', 'Dameon Willich').
card_multiverse_id('castle'/'2ED', '837').

card_in_set('celestial prism', '2ED').
card_original_type('celestial prism'/'2ED', 'Mono Artifact').
card_original_text('celestial prism'/'2ED', '{2}: Provides 1 mana of any color. This use can be played as an interrupt.').
card_image_name('celestial prism'/'2ED', 'celestial prism').
card_uid('celestial prism'/'2ED', '2ED:Celestial Prism:celestial prism').
card_rarity('celestial prism'/'2ED', 'Uncommon').
card_artist('celestial prism'/'2ED', 'Amy Weber').
card_multiverse_id('celestial prism'/'2ED', '602').

card_in_set('channel', '2ED').
card_original_type('channel'/'2ED', 'Sorcery').
card_original_text('channel'/'2ED', 'Until end of turn, you may add colorless mana to your mana pool, at a cost of 1 life each. These additions are played with the speed of an interrupt. Effects that prevent damage may not be used to counter this loss of life.').
card_image_name('channel'/'2ED', 'channel').
card_uid('channel'/'2ED', '2ED:Channel:channel').
card_rarity('channel'/'2ED', 'Uncommon').
card_artist('channel'/'2ED', 'Richard Thomas').
card_multiverse_id('channel'/'2ED', '741').

card_in_set('chaos orb', '2ED').
card_original_type('chaos orb'/'2ED', 'Mono Artifact').
card_original_text('chaos orb'/'2ED', '{1}: Flip Chaos Orb onto the playing area from a height of at least one foot. Chaos Orb must turn completely over at least once or it is discarded with no effect. When Chaos Orb lands, any cards in play that it touches are destroyed, as is Chaos Orb.').
card_image_name('chaos orb'/'2ED', 'chaos orb').
card_uid('chaos orb'/'2ED', '2ED:Chaos Orb:chaos orb').
card_rarity('chaos orb'/'2ED', 'Rare').
card_artist('chaos orb'/'2ED', 'Mark Tedin').
card_multiverse_id('chaos orb'/'2ED', '603').

card_in_set('chaoslace', '2ED').
card_original_type('chaoslace'/'2ED', 'Interrupt').
card_original_text('chaoslace'/'2ED', 'Changes the color of one card either being played or already in play to red. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('chaoslace'/'2ED', 'chaoslace').
card_uid('chaoslace'/'2ED', '2ED:Chaoslace:chaoslace').
card_rarity('chaoslace'/'2ED', 'Rare').
card_artist('chaoslace'/'2ED', 'Dameon Willich').
card_multiverse_id('chaoslace'/'2ED', '784').

card_in_set('circle of protection: black', '2ED').
card_original_type('circle of protection: black'/'2ED', 'Enchantment').
card_original_text('circle of protection: black'/'2ED', '{1}: Prevents all damage against you from one black source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: black'/'2ED', 'circle of protection black').
card_uid('circle of protection: black'/'2ED', '2ED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'2ED', 'Common').
card_artist('circle of protection: black'/'2ED', 'Jesper Myrfors').
card_multiverse_id('circle of protection: black'/'2ED', '838').

card_in_set('circle of protection: blue', '2ED').
card_original_type('circle of protection: blue'/'2ED', 'Enchantment').
card_original_text('circle of protection: blue'/'2ED', '{1}: Prevents all damage against you from one blue source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: blue'/'2ED', 'circle of protection blue').
card_uid('circle of protection: blue'/'2ED', '2ED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'2ED', 'Common').
card_artist('circle of protection: blue'/'2ED', 'Dameon Willich').
card_multiverse_id('circle of protection: blue'/'2ED', '839').

card_in_set('circle of protection: green', '2ED').
card_original_type('circle of protection: green'/'2ED', 'Enchantment').
card_original_text('circle of protection: green'/'2ED', '{1}: Prevents all damage against you from one green source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: green'/'2ED', 'circle of protection green').
card_uid('circle of protection: green'/'2ED', '2ED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'2ED', 'Common').
card_artist('circle of protection: green'/'2ED', 'Sandra Everingham').
card_multiverse_id('circle of protection: green'/'2ED', '840').

card_in_set('circle of protection: red', '2ED').
card_original_type('circle of protection: red'/'2ED', 'Enchantment').
card_original_text('circle of protection: red'/'2ED', '{1}: Prevents all damage against you from one red source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: red'/'2ED', 'circle of protection red').
card_uid('circle of protection: red'/'2ED', '2ED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'2ED', 'Common').
card_artist('circle of protection: red'/'2ED', 'Mark Tedin').
card_multiverse_id('circle of protection: red'/'2ED', '841').

card_in_set('circle of protection: white', '2ED').
card_original_type('circle of protection: white'/'2ED', 'Enchantment').
card_original_text('circle of protection: white'/'2ED', '{1}: Prevents all damage against you from one white source. If a source does damage to you more than once in a turn, you must pay 1 mana each time to prevent the damage.').
card_image_name('circle of protection: white'/'2ED', 'circle of protection white').
card_uid('circle of protection: white'/'2ED', '2ED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'2ED', 'Common').
card_artist('circle of protection: white'/'2ED', 'Douglas Shuler').
card_multiverse_id('circle of protection: white'/'2ED', '842').

card_in_set('clockwork beast', '2ED').
card_original_type('clockwork beast'/'2ED', 'Artifact Creature').
card_original_text('clockwork beast'/'2ED', 'Put seven +1/+0 counters on Beast. After Beast attacks or blocks a creature, discard a counter. During the untap phase, controller may buy back lost counters for 1 mana per counter instead of tapping Beast; this taps Beast if it wasn\'t tapped already.').
card_image_name('clockwork beast'/'2ED', 'clockwork beast').
card_uid('clockwork beast'/'2ED', '2ED:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'2ED', 'Rare').
card_artist('clockwork beast'/'2ED', 'Drew Tucker').
card_multiverse_id('clockwork beast'/'2ED', '604').

card_in_set('clone', '2ED').
card_original_type('clone'/'2ED', 'Summon — Clone').
card_original_text('clone'/'2ED', 'Upon summoning, Clone acquires all characteristics, including color, of any one creature in play on either side; any creature enchantments on original creature are not copied. Clone retains these characteristics even after original creature is destroyed. Clone cannot be played if there are no creatures in play.').
card_image_name('clone'/'2ED', 'clone').
card_uid('clone'/'2ED', '2ED:Clone:clone').
card_rarity('clone'/'2ED', 'Uncommon').
card_artist('clone'/'2ED', 'Julie Baroh').
card_multiverse_id('clone'/'2ED', '696').

card_in_set('cockatrice', '2ED').
card_original_type('cockatrice'/'2ED', 'Summon — Cockatrice').
card_original_text('cockatrice'/'2ED', 'Flying\nAny non-wall creature blocking Cockatrice is destroyed, as is any creature blocked by Cockatrice. Creatures destroyed in this way deal their damage before dying.').
card_image_name('cockatrice'/'2ED', 'cockatrice').
card_uid('cockatrice'/'2ED', '2ED:Cockatrice:cockatrice').
card_rarity('cockatrice'/'2ED', 'Rare').
card_artist('cockatrice'/'2ED', 'Dan Frazier').
card_multiverse_id('cockatrice'/'2ED', '742').

card_in_set('consecrate land', '2ED').
card_original_type('consecrate land'/'2ED', 'Enchant Land').
card_original_text('consecrate land'/'2ED', 'All enchantments on target land are destroyed. Land cannot be destroyed or further enchanted until Consecrate Land has been destroyed.').
card_image_name('consecrate land'/'2ED', 'consecrate land').
card_uid('consecrate land'/'2ED', '2ED:Consecrate Land:consecrate land').
card_rarity('consecrate land'/'2ED', 'Uncommon').
card_artist('consecrate land'/'2ED', 'Jeff A. Menges').
card_multiverse_id('consecrate land'/'2ED', '843').

card_in_set('conservator', '2ED').
card_original_type('conservator'/'2ED', 'Mono Artifact').
card_original_text('conservator'/'2ED', '{3}: Prevent the loss of up to 2 life.').
card_image_name('conservator'/'2ED', 'conservator').
card_uid('conservator'/'2ED', '2ED:Conservator:conservator').
card_rarity('conservator'/'2ED', 'Uncommon').
card_artist('conservator'/'2ED', 'Amy Weber').
card_multiverse_id('conservator'/'2ED', '605').

card_in_set('contract from below', '2ED').
card_original_type('contract from below'/'2ED', 'Sorcery').
card_original_text('contract from below'/'2ED', 'Discard your current hand and draw eight new cards, adding the first drawn to your ante. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('contract from below'/'2ED', 'contract from below').
card_uid('contract from below'/'2ED', '2ED:Contract from Below:contract from below').
card_rarity('contract from below'/'2ED', 'Rare').
card_artist('contract from below'/'2ED', 'Douglas Shuler').
card_multiverse_id('contract from below'/'2ED', '649').

card_in_set('control magic', '2ED').
card_original_type('control magic'/'2ED', 'Enchant Creature').
card_original_text('control magic'/'2ED', 'You control target creature until enchantment is discarded or game ends. You can\'t tap target creature this turn, but if it was already tapped it stays tapped until you can untap it. If destroyed, target creature is put in its owner\'s graveyard.').
card_image_name('control magic'/'2ED', 'control magic').
card_uid('control magic'/'2ED', '2ED:Control Magic:control magic').
card_rarity('control magic'/'2ED', 'Uncommon').
card_artist('control magic'/'2ED', 'Dameon Willich').
card_multiverse_id('control magic'/'2ED', '697').

card_in_set('conversion', '2ED').
card_original_type('conversion'/'2ED', 'Enchantment').
card_original_text('conversion'/'2ED', 'All mountains are considered plains while Conversion is in play. Pay {W}{W} during upkeep or Conversion is discarded.').
card_image_name('conversion'/'2ED', 'conversion').
card_uid('conversion'/'2ED', '2ED:Conversion:conversion').
card_rarity('conversion'/'2ED', 'Uncommon').
card_artist('conversion'/'2ED', 'Jesper Myrfors').
card_multiverse_id('conversion'/'2ED', '844').

card_in_set('copper tablet', '2ED').
card_original_type('copper tablet'/'2ED', 'Continuous Artifact').
card_original_text('copper tablet'/'2ED', 'Copper Tablet does 1 damage to each player during his or her upkeep.').
card_image_name('copper tablet'/'2ED', 'copper tablet').
card_uid('copper tablet'/'2ED', '2ED:Copper Tablet:copper tablet').
card_rarity('copper tablet'/'2ED', 'Uncommon').
card_artist('copper tablet'/'2ED', 'Amy Weber').
card_multiverse_id('copper tablet'/'2ED', '606').

card_in_set('copy artifact', '2ED').
card_original_type('copy artifact'/'2ED', 'Enchantment').
card_original_text('copy artifact'/'2ED', 'Select any artifact in play. This enchantment acts as a duplicate of that artifact; enchantment copy is affected by cards that affect either enchantments or artifacts. Enchantment copy remains even if original artifact is destroyed.').
card_image_name('copy artifact'/'2ED', 'copy artifact').
card_uid('copy artifact'/'2ED', '2ED:Copy Artifact:copy artifact').
card_rarity('copy artifact'/'2ED', 'Rare').
card_artist('copy artifact'/'2ED', 'Amy Weber').
card_multiverse_id('copy artifact'/'2ED', '698').

card_in_set('counterspell', '2ED').
card_original_type('counterspell'/'2ED', 'Interrupt').
card_original_text('counterspell'/'2ED', 'Counters target spell as it is being cast.').
card_image_name('counterspell'/'2ED', 'counterspell').
card_uid('counterspell'/'2ED', '2ED:Counterspell:counterspell').
card_rarity('counterspell'/'2ED', 'Uncommon').
card_artist('counterspell'/'2ED', 'Mark Poole').
card_multiverse_id('counterspell'/'2ED', '699').

card_in_set('craw wurm', '2ED').
card_original_type('craw wurm'/'2ED', 'Summon — Wurm').
card_original_text('craw wurm'/'2ED', '').
card_image_name('craw wurm'/'2ED', 'craw wurm').
card_uid('craw wurm'/'2ED', '2ED:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'2ED', 'Common').
card_artist('craw wurm'/'2ED', 'Daniel Gelon').
card_flavor_text('craw wurm'/'2ED', 'The most terrifying thing about the Craw Wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').
card_multiverse_id('craw wurm'/'2ED', '743').

card_in_set('creature bond', '2ED').
card_original_type('creature bond'/'2ED', 'Enchant Creature').
card_original_text('creature bond'/'2ED', 'If target creature is destroyed, Creature Bond does an amount of damage equal to creature\'s toughness to creature\'s controller.').
card_image_name('creature bond'/'2ED', 'creature bond').
card_uid('creature bond'/'2ED', '2ED:Creature Bond:creature bond').
card_rarity('creature bond'/'2ED', 'Common').
card_artist('creature bond'/'2ED', 'Anson Maddocks').
card_multiverse_id('creature bond'/'2ED', '700').

card_in_set('crusade', '2ED').
card_original_type('crusade'/'2ED', 'Enchantment').
card_original_text('crusade'/'2ED', 'All white creatures gain +1/+1.').
card_image_name('crusade'/'2ED', 'crusade').
card_uid('crusade'/'2ED', '2ED:Crusade:crusade').
card_rarity('crusade'/'2ED', 'Rare').
card_artist('crusade'/'2ED', 'Mark Poole').
card_multiverse_id('crusade'/'2ED', '845').

card_in_set('crystal rod', '2ED').
card_original_type('crystal rod'/'2ED', 'Poly Artifact').
card_original_text('crystal rod'/'2ED', '{1}: Any blue spell cast by any player gives you 1 life.').
card_image_name('crystal rod'/'2ED', 'crystal rod').
card_uid('crystal rod'/'2ED', '2ED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'2ED', 'Uncommon').
card_artist('crystal rod'/'2ED', 'Amy Weber').
card_multiverse_id('crystal rod'/'2ED', '607').

card_in_set('cursed land', '2ED').
card_original_type('cursed land'/'2ED', 'Enchant Land').
card_original_text('cursed land'/'2ED', 'Cursed Land does 1 damage to target land\'s controller during each upkeep.').
card_image_name('cursed land'/'2ED', 'cursed land').
card_uid('cursed land'/'2ED', '2ED:Cursed Land:cursed land').
card_rarity('cursed land'/'2ED', 'Uncommon').
card_artist('cursed land'/'2ED', 'Jesper Myrfors').
card_multiverse_id('cursed land'/'2ED', '650').

card_in_set('cyclopean tomb', '2ED').
card_original_type('cyclopean tomb'/'2ED', 'Mono Artifact').
card_original_text('cyclopean tomb'/'2ED', '{2}: Turn any one non-swamp land into swamp during upkeep. Mark the changed lands with tokens. If Cyclopean Tomb is destroyed, remove one token of your choice each upkeep, returning that land to its original nature.').
card_image_name('cyclopean tomb'/'2ED', 'cyclopean tomb').
card_uid('cyclopean tomb'/'2ED', '2ED:Cyclopean Tomb:cyclopean tomb').
card_rarity('cyclopean tomb'/'2ED', 'Rare').
card_artist('cyclopean tomb'/'2ED', 'Anson Maddocks').
card_multiverse_id('cyclopean tomb'/'2ED', '608').

card_in_set('dark ritual', '2ED').
card_original_type('dark ritual'/'2ED', 'Interrupt').
card_original_text('dark ritual'/'2ED', 'Add 3 black mana to your mana pool.').
card_image_name('dark ritual'/'2ED', 'dark ritual').
card_uid('dark ritual'/'2ED', '2ED:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'2ED', 'Common').
card_artist('dark ritual'/'2ED', 'Sandra Everingham').
card_multiverse_id('dark ritual'/'2ED', '651').

card_in_set('darkpact', '2ED').
card_original_type('darkpact'/'2ED', 'Sorcery').
card_original_text('darkpact'/'2ED', 'Without looking at it first, swap top card of your library with either card of the ante; this swap is permanent. You must have a card in your library to cast this spell. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('darkpact'/'2ED', 'darkpact').
card_uid('darkpact'/'2ED', '2ED:Darkpact:darkpact').
card_rarity('darkpact'/'2ED', 'Rare').
card_artist('darkpact'/'2ED', 'Quinton Hoover').
card_multiverse_id('darkpact'/'2ED', '652').

card_in_set('death ward', '2ED').
card_original_type('death ward'/'2ED', 'Instant').
card_original_text('death ward'/'2ED', 'Regenerates target creature.').
card_image_name('death ward'/'2ED', 'death ward').
card_uid('death ward'/'2ED', '2ED:Death Ward:death ward').
card_rarity('death ward'/'2ED', 'Common').
card_artist('death ward'/'2ED', 'Mark Poole').
card_multiverse_id('death ward'/'2ED', '846').

card_in_set('deathgrip', '2ED').
card_original_type('deathgrip'/'2ED', 'Enchantment').
card_original_text('deathgrip'/'2ED', '{B}{B} Destroy a green spell as it is being cast. This action may be played as an interrupt, and does not affect green cards already in play.').
card_image_name('deathgrip'/'2ED', 'deathgrip').
card_uid('deathgrip'/'2ED', '2ED:Deathgrip:deathgrip').
card_rarity('deathgrip'/'2ED', 'Uncommon').
card_artist('deathgrip'/'2ED', 'Anson Maddocks').
card_multiverse_id('deathgrip'/'2ED', '653').

card_in_set('deathlace', '2ED').
card_original_type('deathlace'/'2ED', 'Interrupt').
card_original_text('deathlace'/'2ED', 'Changes the color of one card either being played or already in play to black. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('deathlace'/'2ED', 'deathlace').
card_uid('deathlace'/'2ED', '2ED:Deathlace:deathlace').
card_rarity('deathlace'/'2ED', 'Rare').
card_artist('deathlace'/'2ED', 'Sandra Everingham').
card_multiverse_id('deathlace'/'2ED', '654').

card_in_set('demonic attorney', '2ED').
card_original_type('demonic attorney'/'2ED', 'Sorcery').
card_original_text('demonic attorney'/'2ED', 'If opponent doesn\'t concede the game immediately, each player must ante an additional card from the top of his or her library. Remove this card from your deck before playing if you are not playing for ante.').
card_image_name('demonic attorney'/'2ED', 'demonic attorney').
card_uid('demonic attorney'/'2ED', '2ED:Demonic Attorney:demonic attorney').
card_rarity('demonic attorney'/'2ED', 'Rare').
card_artist('demonic attorney'/'2ED', 'Daniel Gelon').
card_multiverse_id('demonic attorney'/'2ED', '655').

card_in_set('demonic hordes', '2ED').
card_original_type('demonic hordes'/'2ED', 'Summon — Demons').
card_original_text('demonic hordes'/'2ED', 'Tap to destroy 1 land. Pay {B}{B}{B} during upkeep or the Hordes become tapped and you lose a land of opponent\'s choice.').
card_image_name('demonic hordes'/'2ED', 'demonic hordes').
card_uid('demonic hordes'/'2ED', '2ED:Demonic Hordes:demonic hordes').
card_rarity('demonic hordes'/'2ED', 'Rare').
card_artist('demonic hordes'/'2ED', 'Jesper Myrfors').
card_flavor_text('demonic hordes'/'2ED', 'Created to destroy Dominia, Demons can sometimes be bent to a more focused purpose.').
card_multiverse_id('demonic hordes'/'2ED', '656').

card_in_set('demonic tutor', '2ED').
card_original_type('demonic tutor'/'2ED', 'Sorcery').
card_original_text('demonic tutor'/'2ED', 'You may search your library for one card and take it into your hand. Reshuffle your library afterwards.').
card_image_name('demonic tutor'/'2ED', 'demonic tutor').
card_uid('demonic tutor'/'2ED', '2ED:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'2ED', 'Uncommon').
card_artist('demonic tutor'/'2ED', 'Douglas Shuler').
card_multiverse_id('demonic tutor'/'2ED', '657').

card_in_set('dingus egg', '2ED').
card_original_type('dingus egg'/'2ED', 'Continuous Artifact').
card_original_text('dingus egg'/'2ED', 'Whenever anyone loses a land, Egg does 2 damage to that player for each land lost.').
card_image_name('dingus egg'/'2ED', 'dingus egg').
card_uid('dingus egg'/'2ED', '2ED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'2ED', 'Rare').
card_artist('dingus egg'/'2ED', 'Dan Frazier').
card_multiverse_id('dingus egg'/'2ED', '609').

card_in_set('disenchant', '2ED').
card_original_type('disenchant'/'2ED', 'Instant').
card_original_text('disenchant'/'2ED', 'Target enchantment or artifact must be discarded.').
card_image_name('disenchant'/'2ED', 'disenchant').
card_uid('disenchant'/'2ED', '2ED:Disenchant:disenchant').
card_rarity('disenchant'/'2ED', 'Common').
card_artist('disenchant'/'2ED', 'Amy Weber').
card_multiverse_id('disenchant'/'2ED', '847').

card_in_set('disintegrate', '2ED').
card_original_type('disintegrate'/'2ED', 'Sorcery').
card_original_text('disintegrate'/'2ED', 'Disintegrate does X damage to one target. If target dies this turn, it is removed from game entirely and cannot be regenerated. Return target to its owner\'s deck only when game is over.').
card_image_name('disintegrate'/'2ED', 'disintegrate').
card_uid('disintegrate'/'2ED', '2ED:Disintegrate:disintegrate').
card_rarity('disintegrate'/'2ED', 'Common').
card_artist('disintegrate'/'2ED', 'Anson Maddocks').
card_multiverse_id('disintegrate'/'2ED', '785').

card_in_set('disrupting scepter', '2ED').
card_original_type('disrupting scepter'/'2ED', 'Mono Artifact').
card_original_text('disrupting scepter'/'2ED', '{3}: Opponent must discard one card of his or her choice. Can only be used during your turn.').
card_image_name('disrupting scepter'/'2ED', 'disrupting scepter').
card_uid('disrupting scepter'/'2ED', '2ED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'2ED', 'Rare').
card_artist('disrupting scepter'/'2ED', 'Dan Frazier').
card_multiverse_id('disrupting scepter'/'2ED', '610').

card_in_set('dragon whelp', '2ED').
card_original_type('dragon whelp'/'2ED', 'Summon — Dragon').
card_original_text('dragon whelp'/'2ED', 'Flying\n{R} +1/+0 until end of turn; if more than {R}{R}{R} is spent in this way, Dragon Whelp is destroyed at end of turn.').
card_image_name('dragon whelp'/'2ED', 'dragon whelp').
card_uid('dragon whelp'/'2ED', '2ED:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'2ED', 'Uncommon').
card_artist('dragon whelp'/'2ED', 'Amy Weber').
card_flavor_text('dragon whelp'/'2ED', '\"O to be a dragon . . . of silkworm size or immense . . .\"\n—Marianne Moore, \"O to Be a Dragon\"').
card_multiverse_id('dragon whelp'/'2ED', '786').

card_in_set('drain life', '2ED').
card_original_type('drain life'/'2ED', 'Sorcery').
card_original_text('drain life'/'2ED', 'Drain Life does 1 damage to a single target for each {B} spent in addition to the casting cost. Caster gains 1 life for each damage inflicted. If you drain life from a creature, you cannot gain more life than the creature\'s toughness.').
card_image_name('drain life'/'2ED', 'drain life').
card_uid('drain life'/'2ED', '2ED:Drain Life:drain life').
card_rarity('drain life'/'2ED', 'Common').
card_artist('drain life'/'2ED', 'Douglas Shuler').
card_multiverse_id('drain life'/'2ED', '658').

card_in_set('drain power', '2ED').
card_original_type('drain power'/'2ED', 'Sorcery').
card_original_text('drain power'/'2ED', 'Tap all opponent\'s land, taking all this mana and all mana in opponent\'s mana pool into your mana pool. You can\'t tap fewer than all opponent\'s lands.').
card_image_name('drain power'/'2ED', 'drain power').
card_uid('drain power'/'2ED', '2ED:Drain Power:drain power').
card_rarity('drain power'/'2ED', 'Rare').
card_artist('drain power'/'2ED', 'Douglas Shuler').
card_multiverse_id('drain power'/'2ED', '701').

card_in_set('drudge skeletons', '2ED').
card_original_type('drudge skeletons'/'2ED', 'Summon — Skeletons').
card_original_text('drudge skeletons'/'2ED', '{B} Regenerates').
card_image_name('drudge skeletons'/'2ED', 'drudge skeletons').
card_uid('drudge skeletons'/'2ED', '2ED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'2ED', 'Common').
card_artist('drudge skeletons'/'2ED', 'Sandra Everingham').
card_flavor_text('drudge skeletons'/'2ED', 'Bones scattered around us joined to form misshapen bodies. We struck at them repeatedly—they fell, but soon formed again, with the same mocking look on their faceless skulls.').
card_multiverse_id('drudge skeletons'/'2ED', '659').

card_in_set('dwarven demolition team', '2ED').
card_original_type('dwarven demolition team'/'2ED', 'Summon — Dwarves').
card_original_text('dwarven demolition team'/'2ED', 'Tap to destroy a wall.').
card_image_name('dwarven demolition team'/'2ED', 'dwarven demolition team').
card_uid('dwarven demolition team'/'2ED', '2ED:Dwarven Demolition Team:dwarven demolition team').
card_rarity('dwarven demolition team'/'2ED', 'Uncommon').
card_artist('dwarven demolition team'/'2ED', 'Kev Brockschmidt').
card_flavor_text('dwarven demolition team'/'2ED', 'Foolishly, Najib retreated to his castle at El-Abar; the next morning he was dead. In just one night, the dwarven forces had reduced the mighty walls to mere rubble.').
card_multiverse_id('dwarven demolition team'/'2ED', '787').

card_in_set('dwarven warriors', '2ED').
card_original_type('dwarven warriors'/'2ED', 'Summon — Dwarves').
card_original_text('dwarven warriors'/'2ED', 'Tap to make a creature of power no greater than 2 unblockable until end of turn. Other cards may later be used to increase target creature\'s power beyond 2 after defense is chosen.').
card_image_name('dwarven warriors'/'2ED', 'dwarven warriors').
card_uid('dwarven warriors'/'2ED', '2ED:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'2ED', 'Common').
card_artist('dwarven warriors'/'2ED', 'Douglas Shuler').
card_multiverse_id('dwarven warriors'/'2ED', '788').

card_in_set('earth elemental', '2ED').
card_original_type('earth elemental'/'2ED', 'Summon — Elemental').
card_original_text('earth elemental'/'2ED', '').
card_image_name('earth elemental'/'2ED', 'earth elemental').
card_uid('earth elemental'/'2ED', '2ED:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'2ED', 'Uncommon').
card_artist('earth elemental'/'2ED', 'Dan Frazier').
card_flavor_text('earth elemental'/'2ED', 'Earth Elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').
card_multiverse_id('earth elemental'/'2ED', '789').

card_in_set('earthbind', '2ED').
card_original_type('earthbind'/'2ED', 'Enchant Flying Creature').
card_original_text('earthbind'/'2ED', 'Earthbind does 2 damage to target flying creature, which also loses flying ability.').
card_image_name('earthbind'/'2ED', 'earthbind').
card_uid('earthbind'/'2ED', '2ED:Earthbind:earthbind').
card_rarity('earthbind'/'2ED', 'Common').
card_artist('earthbind'/'2ED', 'Quinton Hoover').
card_multiverse_id('earthbind'/'2ED', '790').

card_in_set('earthquake', '2ED').
card_original_type('earthquake'/'2ED', 'Sorcery').
card_original_text('earthquake'/'2ED', 'Does X damage to each player and each non-flying creature in play.').
card_image_name('earthquake'/'2ED', 'earthquake').
card_uid('earthquake'/'2ED', '2ED:Earthquake:earthquake').
card_rarity('earthquake'/'2ED', 'Rare').
card_artist('earthquake'/'2ED', 'Dan Frazier').
card_multiverse_id('earthquake'/'2ED', '791').

card_in_set('elvish archers', '2ED').
card_original_type('elvish archers'/'2ED', 'Summon — Elves').
card_original_text('elvish archers'/'2ED', 'First strike').
card_image_name('elvish archers'/'2ED', 'elvish archers').
card_uid('elvish archers'/'2ED', '2ED:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'2ED', 'Rare').
card_artist('elvish archers'/'2ED', 'Anson Maddocks').
card_flavor_text('elvish archers'/'2ED', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').
card_multiverse_id('elvish archers'/'2ED', '744').

card_in_set('evil presence', '2ED').
card_original_type('evil presence'/'2ED', 'Enchant Land').
card_original_text('evil presence'/'2ED', 'Target land is now a swamp.').
card_image_name('evil presence'/'2ED', 'evil presence').
card_uid('evil presence'/'2ED', '2ED:Evil Presence:evil presence').
card_rarity('evil presence'/'2ED', 'Uncommon').
card_artist('evil presence'/'2ED', 'Sandra Everingham').
card_multiverse_id('evil presence'/'2ED', '660').

card_in_set('false orders', '2ED').
card_original_type('false orders'/'2ED', 'Instant').
card_original_text('false orders'/'2ED', 'You decide whether and how one defending creature blocks, though you can\'t make a choice the defender couldn\'t legally make. Play after defender has chosen defense but before damage is dealt.').
card_image_name('false orders'/'2ED', 'false orders').
card_uid('false orders'/'2ED', '2ED:False Orders:false orders').
card_rarity('false orders'/'2ED', 'Common').
card_artist('false orders'/'2ED', 'Anson Maddocks').
card_multiverse_id('false orders'/'2ED', '792').

card_in_set('farmstead', '2ED').
card_original_type('farmstead'/'2ED', 'Enchant Land').
card_original_text('farmstead'/'2ED', 'Target land\'s controller gains 1 life each upkeep if {W}{W} is spent. Target land still generates mana as usual.').
card_image_name('farmstead'/'2ED', 'farmstead').
card_uid('farmstead'/'2ED', '2ED:Farmstead:farmstead').
card_rarity('farmstead'/'2ED', 'Rare').
card_artist('farmstead'/'2ED', 'Mark Poole').
card_multiverse_id('farmstead'/'2ED', '848').

card_in_set('fastbond', '2ED').
card_original_type('fastbond'/'2ED', 'Enchantment').
card_original_text('fastbond'/'2ED', 'You may put as many lands into play as you want each turn. Fastbond does 1 damage to you for every land beyond the first that you play in a single turn.').
card_image_name('fastbond'/'2ED', 'fastbond').
card_uid('fastbond'/'2ED', '2ED:Fastbond:fastbond').
card_rarity('fastbond'/'2ED', 'Rare').
card_artist('fastbond'/'2ED', 'Mark Poole').
card_multiverse_id('fastbond'/'2ED', '745').

card_in_set('fear', '2ED').
card_original_type('fear'/'2ED', 'Enchant Creature').
card_original_text('fear'/'2ED', 'Target creature cannot be blocked by any creatures other than artifact creatures and black creatures.').
card_image_name('fear'/'2ED', 'fear').
card_uid('fear'/'2ED', '2ED:Fear:fear').
card_rarity('fear'/'2ED', 'Common').
card_artist('fear'/'2ED', 'Mark Poole').
card_multiverse_id('fear'/'2ED', '661').

card_in_set('feedback', '2ED').
card_original_type('feedback'/'2ED', 'Enchant Enchantment').
card_original_text('feedback'/'2ED', 'Feedback does 1 damage to controller of target enchantment during each upkeep.').
card_image_name('feedback'/'2ED', 'feedback').
card_uid('feedback'/'2ED', '2ED:Feedback:feedback').
card_rarity('feedback'/'2ED', 'Uncommon').
card_artist('feedback'/'2ED', 'Quinton Hoover').
card_multiverse_id('feedback'/'2ED', '702').

card_in_set('fire elemental', '2ED').
card_original_type('fire elemental'/'2ED', 'Summon — Elemental').
card_original_text('fire elemental'/'2ED', '').
card_image_name('fire elemental'/'2ED', 'fire elemental').
card_uid('fire elemental'/'2ED', '2ED:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'2ED', 'Uncommon').
card_artist('fire elemental'/'2ED', 'Melissa A. Benson').
card_flavor_text('fire elemental'/'2ED', 'Fire Elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').
card_multiverse_id('fire elemental'/'2ED', '793').

card_in_set('fireball', '2ED').
card_original_type('fireball'/'2ED', 'Sorcery').
card_original_text('fireball'/'2ED', 'Fireball does X damage total, divided evenly (round down) among any number of targets. Pay 1 extra mana for each target beyond the first.').
card_image_name('fireball'/'2ED', 'fireball').
card_uid('fireball'/'2ED', '2ED:Fireball:fireball').
card_rarity('fireball'/'2ED', 'Common').
card_artist('fireball'/'2ED', 'Mark Tedin').
card_multiverse_id('fireball'/'2ED', '794').

card_in_set('firebreathing', '2ED').
card_original_type('firebreathing'/'2ED', 'Enchant Creature').
card_original_text('firebreathing'/'2ED', '{R} +1/+0').
card_image_name('firebreathing'/'2ED', 'firebreathing').
card_uid('firebreathing'/'2ED', '2ED:Firebreathing:firebreathing').
card_rarity('firebreathing'/'2ED', 'Common').
card_artist('firebreathing'/'2ED', 'Dan Frazier').
card_flavor_text('firebreathing'/'2ED', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson, \"In Memoriam\"').
card_multiverse_id('firebreathing'/'2ED', '795').

card_in_set('flashfires', '2ED').
card_original_type('flashfires'/'2ED', 'Sorcery').
card_original_text('flashfires'/'2ED', 'All plains in play are destroyed.').
card_image_name('flashfires'/'2ED', 'flashfires').
card_uid('flashfires'/'2ED', '2ED:Flashfires:flashfires').
card_rarity('flashfires'/'2ED', 'Uncommon').
card_artist('flashfires'/'2ED', 'Dameon Willich').
card_multiverse_id('flashfires'/'2ED', '796').

card_in_set('flight', '2ED').
card_original_type('flight'/'2ED', 'Enchant Creature').
card_original_text('flight'/'2ED', 'Target creature is now a flying creature.').
card_image_name('flight'/'2ED', 'flight').
card_uid('flight'/'2ED', '2ED:Flight:flight').
card_rarity('flight'/'2ED', 'Common').
card_artist('flight'/'2ED', 'Anson Maddocks').
card_multiverse_id('flight'/'2ED', '703').

card_in_set('fog', '2ED').
card_original_type('fog'/'2ED', 'Instant').
card_original_text('fog'/'2ED', 'Creatures attack and block as normal, but none deal any damage. All attacking creatures are still tapped. Play any time before attack damage is dealt.').
card_image_name('fog'/'2ED', 'fog').
card_uid('fog'/'2ED', '2ED:Fog:fog').
card_rarity('fog'/'2ED', 'Common').
card_artist('fog'/'2ED', 'Jesper Myrfors').
card_multiverse_id('fog'/'2ED', '746').

card_in_set('force of nature', '2ED').
card_original_type('force of nature'/'2ED', 'Summon — Force').
card_original_text('force of nature'/'2ED', 'Trample\nYou must pay {G}{G}{G}{G} during upkeep or Force of Nature does 8 damage to you. You may still attack with Force of Nature even if you failed to pay the upkeep.').
card_image_name('force of nature'/'2ED', 'force of nature').
card_uid('force of nature'/'2ED', '2ED:Force of Nature:force of nature').
card_rarity('force of nature'/'2ED', 'Rare').
card_artist('force of nature'/'2ED', 'Douglas Shuler').
card_multiverse_id('force of nature'/'2ED', '747').

card_in_set('forcefield', '2ED').
card_original_type('forcefield'/'2ED', 'Poly Artifact').
card_original_text('forcefield'/'2ED', '{1}: Lose only 1 life to an unblocked creature.').
card_image_name('forcefield'/'2ED', 'forcefield').
card_uid('forcefield'/'2ED', '2ED:Forcefield:forcefield').
card_rarity('forcefield'/'2ED', 'Rare').
card_artist('forcefield'/'2ED', 'Dan Frazier').
card_multiverse_id('forcefield'/'2ED', '611').

card_in_set('forest', '2ED').
card_original_type('forest'/'2ED', 'Land').
card_original_text('forest'/'2ED', 'Tap to add {G} to your mana pool.').
card_image_name('forest'/'2ED', 'forest1').
card_uid('forest'/'2ED', '2ED:Forest:forest1').
card_rarity('forest'/'2ED', 'Basic Land').
card_artist('forest'/'2ED', 'Christopher Rush').
card_multiverse_id('forest'/'2ED', '890').

card_in_set('forest', '2ED').
card_original_type('forest'/'2ED', 'Land').
card_original_text('forest'/'2ED', 'Tap to add {G} to your mana pool.').
card_image_name('forest'/'2ED', 'forest2').
card_uid('forest'/'2ED', '2ED:Forest:forest2').
card_rarity('forest'/'2ED', 'Basic Land').
card_artist('forest'/'2ED', 'Christopher Rush').
card_multiverse_id('forest'/'2ED', '889').

card_in_set('forest', '2ED').
card_original_type('forest'/'2ED', 'Land').
card_original_text('forest'/'2ED', 'Tap to add {G} to your mana pool.').
card_image_name('forest'/'2ED', 'forest3').
card_uid('forest'/'2ED', '2ED:Forest:forest3').
card_rarity('forest'/'2ED', 'Basic Land').
card_artist('forest'/'2ED', 'Christopher Rush').
card_multiverse_id('forest'/'2ED', '888').

card_in_set('fork', '2ED').
card_original_type('fork'/'2ED', 'Interrupt').
card_original_text('fork'/'2ED', 'Any sorcery or instant spell just cast is doubled. Treat Fork as an exact copy of target spell except that Fork remains red. Copy and original may have different targets.').
card_image_name('fork'/'2ED', 'fork').
card_uid('fork'/'2ED', '2ED:Fork:fork').
card_rarity('fork'/'2ED', 'Rare').
card_artist('fork'/'2ED', 'Amy Weber').
card_multiverse_id('fork'/'2ED', '797').

card_in_set('frozen shade', '2ED').
card_original_type('frozen shade'/'2ED', 'Summon — Shade').
card_original_text('frozen shade'/'2ED', '{B} +1/+1').
card_image_name('frozen shade'/'2ED', 'frozen shade').
card_uid('frozen shade'/'2ED', '2ED:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'2ED', 'Common').
card_artist('frozen shade'/'2ED', 'Douglas Shuler').
card_flavor_text('frozen shade'/'2ED', '\"There are some qualities—some incorporate things,/ That have a double life, which thus is made/ A type of twin entity which springs/ From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').
card_multiverse_id('frozen shade'/'2ED', '662').

card_in_set('fungusaur', '2ED').
card_original_type('fungusaur'/'2ED', 'Summon — Fungusaur').
card_original_text('fungusaur'/'2ED', 'Each time Fungusaur is damaged but not destroyed, put a +1/+1 counter on it.').
card_image_name('fungusaur'/'2ED', 'fungusaur').
card_uid('fungusaur'/'2ED', '2ED:Fungusaur:fungusaur').
card_rarity('fungusaur'/'2ED', 'Rare').
card_artist('fungusaur'/'2ED', 'Daniel Gelon').
card_flavor_text('fungusaur'/'2ED', 'Rather than sheltering her young, the female Fungusaur often injures her own offspring, thereby ensuring their rapid growth.').
card_multiverse_id('fungusaur'/'2ED', '748').

card_in_set('gaea\'s liege', '2ED').
card_original_type('gaea\'s liege'/'2ED', 'Summon — Gaea\'s Liege').
card_original_text('gaea\'s liege'/'2ED', 'When defending, Gaea\'s Liege has power and toughness equal to the number of forests you have in play; when it\'s attacking, they are equal to the number of forests opponent has in play. Tap to turn any one land into a forest until Gaea\'s Liege leaves play. Mark changed lands with counters, removing the counters when Gaea\'s Liege leaves play.').
card_image_name('gaea\'s liege'/'2ED', 'gaea\'s liege').
card_uid('gaea\'s liege'/'2ED', '2ED:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'2ED', 'Rare').
card_artist('gaea\'s liege'/'2ED', 'Dameon Willich').
card_multiverse_id('gaea\'s liege'/'2ED', '749').

card_in_set('gauntlet of might', '2ED').
card_original_type('gauntlet of might'/'2ED', 'Continuous Artifact').
card_original_text('gauntlet of might'/'2ED', 'All red creatures gain +1/+1 and all mountains provide an extra red mana when tapped.').
card_image_name('gauntlet of might'/'2ED', 'gauntlet of might').
card_uid('gauntlet of might'/'2ED', '2ED:Gauntlet of Might:gauntlet of might').
card_rarity('gauntlet of might'/'2ED', 'Rare').
card_artist('gauntlet of might'/'2ED', 'Christopher Rush').
card_multiverse_id('gauntlet of might'/'2ED', '612').

card_in_set('giant growth', '2ED').
card_original_type('giant growth'/'2ED', 'Instant').
card_original_text('giant growth'/'2ED', 'Target creature gains +3/+3 until end of turn.').
card_image_name('giant growth'/'2ED', 'giant growth').
card_uid('giant growth'/'2ED', '2ED:Giant Growth:giant growth').
card_rarity('giant growth'/'2ED', 'Common').
card_artist('giant growth'/'2ED', 'Sandra Everingham').
card_multiverse_id('giant growth'/'2ED', '750').

card_in_set('giant spider', '2ED').
card_original_type('giant spider'/'2ED', 'Summon — Spider').
card_original_text('giant spider'/'2ED', 'Does not fly, but can block flying creatures.').
card_image_name('giant spider'/'2ED', 'giant spider').
card_uid('giant spider'/'2ED', '2ED:Giant Spider:giant spider').
card_rarity('giant spider'/'2ED', 'Common').
card_artist('giant spider'/'2ED', 'Sandra Everingham').
card_flavor_text('giant spider'/'2ED', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').
card_multiverse_id('giant spider'/'2ED', '751').

card_in_set('glasses of urza', '2ED').
card_original_type('glasses of urza'/'2ED', 'Mono Artifact').
card_original_text('glasses of urza'/'2ED', 'You may look at opponent\'s hand.').
card_image_name('glasses of urza'/'2ED', 'glasses of urza').
card_uid('glasses of urza'/'2ED', '2ED:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'2ED', 'Uncommon').
card_artist('glasses of urza'/'2ED', 'Douglas Shuler').
card_multiverse_id('glasses of urza'/'2ED', '613').

card_in_set('gloom', '2ED').
card_original_type('gloom'/'2ED', 'Enchantment').
card_original_text('gloom'/'2ED', 'White spells cost 3 more mana to cast. Circles of Protection cost 3 more mana to use.').
card_image_name('gloom'/'2ED', 'gloom').
card_uid('gloom'/'2ED', '2ED:Gloom:gloom').
card_rarity('gloom'/'2ED', 'Uncommon').
card_artist('gloom'/'2ED', 'Dan Frazier').
card_multiverse_id('gloom'/'2ED', '663').

card_in_set('goblin balloon brigade', '2ED').
card_original_type('goblin balloon brigade'/'2ED', 'Summon — Goblins').
card_original_text('goblin balloon brigade'/'2ED', '{R} Goblins gain flying ability until end of turn. Controller may not choose to make Goblins fly after they have been blocked.').
card_image_name('goblin balloon brigade'/'2ED', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'2ED', '2ED:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'2ED', 'Uncommon').
card_artist('goblin balloon brigade'/'2ED', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'2ED', '\"From up here we can drop rocks and arrows and more rocks!\" \"Uh, yeah boss, but how do we get down?\"').
card_multiverse_id('goblin balloon brigade'/'2ED', '798').

card_in_set('goblin king', '2ED').
card_original_type('goblin king'/'2ED', 'Summon — Goblin King').
card_original_text('goblin king'/'2ED', 'Goblins in play gain mountainwalk and +1/+1 while this card remains in play.').
card_image_name('goblin king'/'2ED', 'goblin king').
card_uid('goblin king'/'2ED', '2ED:Goblin King:goblin king').
card_rarity('goblin king'/'2ED', 'Rare').
card_artist('goblin king'/'2ED', 'Jesper Myrfors').
card_flavor_text('goblin king'/'2ED', 'To become king of the Goblins, one must assassinate the previous king. Thus, only the most foolish seek positions of leadership.').
card_multiverse_id('goblin king'/'2ED', '799').

card_in_set('granite gargoyle', '2ED').
card_original_type('granite gargoyle'/'2ED', 'Summon — Gargoyle').
card_original_text('granite gargoyle'/'2ED', 'Flying; {R} +0/+1 until end of turn.').
card_image_name('granite gargoyle'/'2ED', 'granite gargoyle').
card_uid('granite gargoyle'/'2ED', '2ED:Granite Gargoyle:granite gargoyle').
card_rarity('granite gargoyle'/'2ED', 'Rare').
card_artist('granite gargoyle'/'2ED', 'Christopher Rush').
card_flavor_text('granite gargoyle'/'2ED', '\"While most overworlders fortunately don\'t realize this, Gargoyles can be most delicious, providing you have the appropriate tools to carve them.\"\n—The Underworld Cookbook, by Asmoranomardicadaistinaculdacar').
card_multiverse_id('granite gargoyle'/'2ED', '800').

card_in_set('gray ogre', '2ED').
card_original_type('gray ogre'/'2ED', 'Summon — Ogre').
card_original_text('gray ogre'/'2ED', '').
card_image_name('gray ogre'/'2ED', 'gray ogre').
card_uid('gray ogre'/'2ED', '2ED:Gray Ogre:gray ogre').
card_rarity('gray ogre'/'2ED', 'Common').
card_artist('gray ogre'/'2ED', 'Dan Frazier').
card_flavor_text('gray ogre'/'2ED', 'The Ogre philosopher Gnerdel believed the purpose of life was to live as high on the food chain as possible. She refused to eat vegetarians, and preferred to live entirely on creatures that preyed on sentient beings.').
card_multiverse_id('gray ogre'/'2ED', '801').

card_in_set('green ward', '2ED').
card_original_type('green ward'/'2ED', 'Enchant Creature').
card_original_text('green ward'/'2ED', 'Target creature gains protection from green.').
card_image_name('green ward'/'2ED', 'green ward').
card_uid('green ward'/'2ED', '2ED:Green Ward:green ward').
card_rarity('green ward'/'2ED', 'Uncommon').
card_artist('green ward'/'2ED', 'Dan Frazier').
card_multiverse_id('green ward'/'2ED', '849').

card_in_set('grizzly bears', '2ED').
card_original_type('grizzly bears'/'2ED', 'Summon — Bears').
card_original_text('grizzly bears'/'2ED', '').
card_image_name('grizzly bears'/'2ED', 'grizzly bears').
card_uid('grizzly bears'/'2ED', '2ED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'2ED', 'Common').
card_artist('grizzly bears'/'2ED', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'2ED', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').
card_multiverse_id('grizzly bears'/'2ED', '752').

card_in_set('guardian angel', '2ED').
card_original_type('guardian angel'/'2ED', 'Instant').
card_original_text('guardian angel'/'2ED', 'Prevents X damage from being dealt to any one target. Any further damage to the same target this turn can be canceled by spending 1 mana per point of damage to be canceled.').
card_image_name('guardian angel'/'2ED', 'guardian angel').
card_uid('guardian angel'/'2ED', '2ED:Guardian Angel:guardian angel').
card_rarity('guardian angel'/'2ED', 'Common').
card_artist('guardian angel'/'2ED', 'Anson Maddocks').
card_multiverse_id('guardian angel'/'2ED', '850').

card_in_set('healing salve', '2ED').
card_original_type('healing salve'/'2ED', 'Instant').
card_original_text('healing salve'/'2ED', 'Gain 3 life, or prevent up to 3 damage from being dealt to a single target.').
card_image_name('healing salve'/'2ED', 'healing salve').
card_uid('healing salve'/'2ED', '2ED:Healing Salve:healing salve').
card_rarity('healing salve'/'2ED', 'Common').
card_artist('healing salve'/'2ED', 'Dan Frazier').
card_multiverse_id('healing salve'/'2ED', '851').

card_in_set('helm of chatzuk', '2ED').
card_original_type('helm of chatzuk'/'2ED', 'Mono Artifact').
card_original_text('helm of chatzuk'/'2ED', '{1}: You may give one creature the ability to band until end of turn.').
card_image_name('helm of chatzuk'/'2ED', 'helm of chatzuk').
card_uid('helm of chatzuk'/'2ED', '2ED:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'2ED', 'Rare').
card_artist('helm of chatzuk'/'2ED', 'Mark Tedin').
card_multiverse_id('helm of chatzuk'/'2ED', '614').

card_in_set('hill giant', '2ED').
card_original_type('hill giant'/'2ED', 'Summon — Giant').
card_original_text('hill giant'/'2ED', '').
card_image_name('hill giant'/'2ED', 'hill giant').
card_uid('hill giant'/'2ED', '2ED:Hill Giant:hill giant').
card_rarity('hill giant'/'2ED', 'Common').
card_artist('hill giant'/'2ED', 'Dan Frazier').
card_flavor_text('hill giant'/'2ED', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').
card_multiverse_id('hill giant'/'2ED', '802').

card_in_set('holy armor', '2ED').
card_original_type('holy armor'/'2ED', 'Enchant Creature').
card_original_text('holy armor'/'2ED', 'Target creature gains +0/+2. {W} Target creature gets extra +0/+1 until end of turn.').
card_image_name('holy armor'/'2ED', 'holy armor').
card_uid('holy armor'/'2ED', '2ED:Holy Armor:holy armor').
card_rarity('holy armor'/'2ED', 'Common').
card_artist('holy armor'/'2ED', 'Melissa A. Benson').
card_multiverse_id('holy armor'/'2ED', '852').

card_in_set('holy strength', '2ED').
card_original_type('holy strength'/'2ED', 'Enchant Creature').
card_original_text('holy strength'/'2ED', 'Target creature gains +1/+2.').
card_image_name('holy strength'/'2ED', 'holy strength').
card_uid('holy strength'/'2ED', '2ED:Holy Strength:holy strength').
card_rarity('holy strength'/'2ED', 'Common').
card_artist('holy strength'/'2ED', 'Anson Maddocks').
card_multiverse_id('holy strength'/'2ED', '853').

card_in_set('howl from beyond', '2ED').
card_original_type('howl from beyond'/'2ED', 'Instant').
card_original_text('howl from beyond'/'2ED', 'Target creature gains +X/+0 until end of turn.').
card_image_name('howl from beyond'/'2ED', 'howl from beyond').
card_uid('howl from beyond'/'2ED', '2ED:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'2ED', 'Common').
card_artist('howl from beyond'/'2ED', 'Mark Poole').
card_multiverse_id('howl from beyond'/'2ED', '664').

card_in_set('howling mine', '2ED').
card_original_type('howling mine'/'2ED', 'Continuous Artifact').
card_original_text('howling mine'/'2ED', 'Each player draws one extra card each turn during his or her draw phase.').
card_image_name('howling mine'/'2ED', 'howling mine').
card_uid('howling mine'/'2ED', '2ED:Howling Mine:howling mine').
card_rarity('howling mine'/'2ED', 'Rare').
card_artist('howling mine'/'2ED', 'Mark Poole').
card_multiverse_id('howling mine'/'2ED', '615').

card_in_set('hurloon minotaur', '2ED').
card_original_type('hurloon minotaur'/'2ED', 'Summon — Minotaur').
card_original_text('hurloon minotaur'/'2ED', '').
card_image_name('hurloon minotaur'/'2ED', 'hurloon minotaur').
card_uid('hurloon minotaur'/'2ED', '2ED:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'2ED', 'Common').
card_artist('hurloon minotaur'/'2ED', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'2ED', 'The Minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').
card_multiverse_id('hurloon minotaur'/'2ED', '803').

card_in_set('hurricane', '2ED').
card_original_type('hurricane'/'2ED', 'Sorcery').
card_original_text('hurricane'/'2ED', 'All players and flying creatures suffer X damage.').
card_image_name('hurricane'/'2ED', 'hurricane').
card_uid('hurricane'/'2ED', '2ED:Hurricane:hurricane').
card_rarity('hurricane'/'2ED', 'Uncommon').
card_artist('hurricane'/'2ED', 'Dameon Willich').
card_multiverse_id('hurricane'/'2ED', '753').

card_in_set('hypnotic specter', '2ED').
card_original_type('hypnotic specter'/'2ED', 'Summon — Specter').
card_original_text('hypnotic specter'/'2ED', 'Flying\nAn opponent damaged by Specter must discard a card at random from his or her hand. Ignore this effect if opponent has no cards left in hand.').
card_image_name('hypnotic specter'/'2ED', 'hypnotic specter').
card_uid('hypnotic specter'/'2ED', '2ED:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'2ED', 'Uncommon').
card_artist('hypnotic specter'/'2ED', 'Douglas Shuler').
card_flavor_text('hypnotic specter'/'2ED', '\"...There was no trace/ Of aught on that illumined face...\"\n—Samuel Coleridge, \"Phantom\"').
card_multiverse_id('hypnotic specter'/'2ED', '665').

card_in_set('ice storm', '2ED').
card_original_type('ice storm'/'2ED', 'Sorcery').
card_original_text('ice storm'/'2ED', 'Destroys any one land.').
card_image_name('ice storm'/'2ED', 'ice storm').
card_uid('ice storm'/'2ED', '2ED:Ice Storm:ice storm').
card_rarity('ice storm'/'2ED', 'Uncommon').
card_artist('ice storm'/'2ED', 'Dan Frazier').
card_multiverse_id('ice storm'/'2ED', '754').

card_in_set('icy manipulator', '2ED').
card_original_type('icy manipulator'/'2ED', 'Mono Artifact').
card_original_text('icy manipulator'/'2ED', '{1}: You may tap any land, creature, or artifact in play on either side. No effects are generated by the target card.').
card_image_name('icy manipulator'/'2ED', 'icy manipulator').
card_uid('icy manipulator'/'2ED', '2ED:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'2ED', 'Uncommon').
card_artist('icy manipulator'/'2ED', 'Douglas Shuler').
card_multiverse_id('icy manipulator'/'2ED', '616').

card_in_set('illusionary mask', '2ED').
card_original_type('illusionary mask'/'2ED', 'Poly Artifact').
card_original_text('illusionary mask'/'2ED', '{X} You can summon a creature face down so opponent doesn\'t know what it is. The X cost can be any amount of mana, even 0; it serves to hide the true casting cost of the creature, which you still have to spend. As soon as a face-down creature receives damage, deals damage, or is tapped, you must turn it face up.').
card_image_name('illusionary mask'/'2ED', 'illusionary mask').
card_uid('illusionary mask'/'2ED', '2ED:Illusionary Mask:illusionary mask').
card_rarity('illusionary mask'/'2ED', 'Rare').
card_artist('illusionary mask'/'2ED', 'Amy Weber').
card_multiverse_id('illusionary mask'/'2ED', '617').

card_in_set('instill energy', '2ED').
card_original_type('instill energy'/'2ED', 'Enchant Creature').
card_original_text('instill energy'/'2ED', 'You may untap target creature both during your untap phase and one additional time during your turn. Target creature may attack the turn it comes into play.').
card_image_name('instill energy'/'2ED', 'instill energy').
card_uid('instill energy'/'2ED', '2ED:Instill Energy:instill energy').
card_rarity('instill energy'/'2ED', 'Uncommon').
card_artist('instill energy'/'2ED', 'Dameon Willich').
card_multiverse_id('instill energy'/'2ED', '755').

card_in_set('invisibility', '2ED').
card_original_type('invisibility'/'2ED', 'Enchant Creature').
card_original_text('invisibility'/'2ED', 'Target creature can be blocked only by walls.').
card_image_name('invisibility'/'2ED', 'invisibility').
card_uid('invisibility'/'2ED', '2ED:Invisibility:invisibility').
card_rarity('invisibility'/'2ED', 'Common').
card_artist('invisibility'/'2ED', 'Anson Maddocks').
card_multiverse_id('invisibility'/'2ED', '704').

card_in_set('iron star', '2ED').
card_original_type('iron star'/'2ED', 'Poly Artifact').
card_original_text('iron star'/'2ED', '{1}: Any red spell cast by any player gives you 1 life.').
card_image_name('iron star'/'2ED', 'iron star').
card_uid('iron star'/'2ED', '2ED:Iron Star:iron star').
card_rarity('iron star'/'2ED', 'Uncommon').
card_artist('iron star'/'2ED', 'Dan Frazier').
card_multiverse_id('iron star'/'2ED', '618').

card_in_set('ironclaw orcs', '2ED').
card_original_type('ironclaw orcs'/'2ED', 'Summon — Orcs').
card_original_text('ironclaw orcs'/'2ED', 'Cannot be used to block any creature of power more than 1.').
card_image_name('ironclaw orcs'/'2ED', 'ironclaw orcs').
card_uid('ironclaw orcs'/'2ED', '2ED:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'2ED', 'Common').
card_artist('ironclaw orcs'/'2ED', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'2ED', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').
card_multiverse_id('ironclaw orcs'/'2ED', '804').

card_in_set('ironroot treefolk', '2ED').
card_original_type('ironroot treefolk'/'2ED', 'Summon — Treefolk').
card_original_text('ironroot treefolk'/'2ED', '').
card_image_name('ironroot treefolk'/'2ED', 'ironroot treefolk').
card_uid('ironroot treefolk'/'2ED', '2ED:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'2ED', 'Common').
card_artist('ironroot treefolk'/'2ED', 'Jesper Myrfors').
card_flavor_text('ironroot treefolk'/'2ED', 'The mating habits of Treefolk, particularly the stalwart Ironroot Treefolk, are truly absurd. Molasses comes to mind. It\'s amazing the species can survive at all given such protracted periods of mate selection, conjugation, and gestation.').
card_multiverse_id('ironroot treefolk'/'2ED', '756').

card_in_set('island', '2ED').
card_original_type('island'/'2ED', 'Land').
card_original_text('island'/'2ED', 'Tap to add {U} to your mana pool.').
card_image_name('island'/'2ED', 'island1').
card_uid('island'/'2ED', '2ED:Island:island1').
card_rarity('island'/'2ED', 'Basic Land').
card_artist('island'/'2ED', 'Mark Poole').
card_multiverse_id('island'/'2ED', '896').

card_in_set('island', '2ED').
card_original_type('island'/'2ED', 'Land').
card_original_text('island'/'2ED', 'Tap to add {U} to your mana pool.').
card_image_name('island'/'2ED', 'island2').
card_uid('island'/'2ED', '2ED:Island:island2').
card_rarity('island'/'2ED', 'Basic Land').
card_artist('island'/'2ED', 'Mark Poole').
card_multiverse_id('island'/'2ED', '894').

card_in_set('island', '2ED').
card_original_type('island'/'2ED', 'Land').
card_original_text('island'/'2ED', 'Tap to add {U} to your mana pool.').
card_image_name('island'/'2ED', 'island3').
card_uid('island'/'2ED', '2ED:Island:island3').
card_rarity('island'/'2ED', 'Basic Land').
card_artist('island'/'2ED', 'Mark Poole').
card_multiverse_id('island'/'2ED', '895').

card_in_set('island sanctuary', '2ED').
card_original_type('island sanctuary'/'2ED', 'Enchantment').
card_original_text('island sanctuary'/'2ED', 'You may decline to draw a card from your library during draw phase. In exchange, until start of your next turn the only creatures that may attack you are those that fly or have islandwalk.').
card_image_name('island sanctuary'/'2ED', 'island sanctuary').
card_uid('island sanctuary'/'2ED', '2ED:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'2ED', 'Rare').
card_artist('island sanctuary'/'2ED', 'Mark Poole').
card_multiverse_id('island sanctuary'/'2ED', '854').

card_in_set('ivory cup', '2ED').
card_original_type('ivory cup'/'2ED', 'Poly Artifact').
card_original_text('ivory cup'/'2ED', '{1}: Any white spell cast by any player gives you 1 life.').
card_image_name('ivory cup'/'2ED', 'ivory cup').
card_uid('ivory cup'/'2ED', '2ED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'2ED', 'Uncommon').
card_artist('ivory cup'/'2ED', 'Anson Maddocks').
card_multiverse_id('ivory cup'/'2ED', '619').

card_in_set('jade monolith', '2ED').
card_original_type('jade monolith'/'2ED', 'Poly Artifact').
card_original_text('jade monolith'/'2ED', '{1}: You may take damage done to any creature on yourself instead, but you must take all of it. Source of damage is unchanged.').
card_image_name('jade monolith'/'2ED', 'jade monolith').
card_uid('jade monolith'/'2ED', '2ED:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'2ED', 'Rare').
card_artist('jade monolith'/'2ED', 'Anson Maddocks').
card_multiverse_id('jade monolith'/'2ED', '620').

card_in_set('jade statue', '2ED').
card_original_type('jade statue'/'2ED', 'Artifact').
card_original_text('jade statue'/'2ED', '{2}: Jade Statue becomes a creature for the duration of the current attack exchange. Can be a creature only during an attack or defense.').
card_image_name('jade statue'/'2ED', 'jade statue').
card_uid('jade statue'/'2ED', '2ED:Jade Statue:jade statue').
card_rarity('jade statue'/'2ED', 'Uncommon').
card_artist('jade statue'/'2ED', 'Dan Frazier').
card_flavor_text('jade statue'/'2ED', '\"Some of the other guys dared me to touch it, but I knew it weren\'t no ordinary hunk o\' rock.\" —Norin the Wary').
card_multiverse_id('jade statue'/'2ED', '621').

card_in_set('jayemdae tome', '2ED').
card_original_type('jayemdae tome'/'2ED', 'Mono Artifact').
card_original_text('jayemdae tome'/'2ED', '{4}: You may draw one extra card.').
card_image_name('jayemdae tome'/'2ED', 'jayemdae tome').
card_uid('jayemdae tome'/'2ED', '2ED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'2ED', 'Rare').
card_artist('jayemdae tome'/'2ED', 'Mark Tedin').
card_multiverse_id('jayemdae tome'/'2ED', '622').

card_in_set('juggernaut', '2ED').
card_original_type('juggernaut'/'2ED', 'Artifact Creature').
card_original_text('juggernaut'/'2ED', 'Must attack each turn if possible. Can\'t be blocked by walls.').
card_image_name('juggernaut'/'2ED', 'juggernaut').
card_uid('juggernaut'/'2ED', '2ED:Juggernaut:juggernaut').
card_rarity('juggernaut'/'2ED', 'Uncommon').
card_artist('juggernaut'/'2ED', 'Dan Frazier').
card_flavor_text('juggernaut'/'2ED', 'We had taken refuge in a small cave, thinking the entrance was too narrow for it to follow. To our horror, its gigantic head smashed into the mountainside, ripping itself a new entrance.').
card_multiverse_id('juggernaut'/'2ED', '623').

card_in_set('jump', '2ED').
card_original_type('jump'/'2ED', 'Instant').
card_original_text('jump'/'2ED', 'Target creature is a flying creature until end of turn.').
card_image_name('jump'/'2ED', 'jump').
card_uid('jump'/'2ED', '2ED:Jump:jump').
card_rarity('jump'/'2ED', 'Common').
card_artist('jump'/'2ED', 'Mark Poole').
card_multiverse_id('jump'/'2ED', '705').

card_in_set('karma', '2ED').
card_original_type('karma'/'2ED', 'Enchantment').
card_original_text('karma'/'2ED', 'For each swamp in play, Karma does 1 damage to the swamp owner during the swamp owner\'s upkeep.').
card_image_name('karma'/'2ED', 'karma').
card_uid('karma'/'2ED', '2ED:Karma:karma').
card_rarity('karma'/'2ED', 'Uncommon').
card_artist('karma'/'2ED', 'Richard Thomas').
card_multiverse_id('karma'/'2ED', '855').

card_in_set('keldon warlord', '2ED').
card_original_type('keldon warlord'/'2ED', 'Summon — Lord').
card_original_text('keldon warlord'/'2ED', 'The Xs below are the number of non-wall creatures on your side, including Warlord. Thus if you have 2 other non-wall creatures, Warlord is 3/3. If one of those creatures is killed during the turn, Warlord immediately becomes 2/2.').
card_image_name('keldon warlord'/'2ED', 'keldon warlord').
card_uid('keldon warlord'/'2ED', '2ED:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'2ED', 'Uncommon').
card_artist('keldon warlord'/'2ED', 'Kev Brockschmidt').
card_multiverse_id('keldon warlord'/'2ED', '805').

card_in_set('kormus bell', '2ED').
card_original_type('kormus bell'/'2ED', 'Continuous Artifact').
card_original_text('kormus bell'/'2ED', 'Treat all swamps in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack. Swamps have no color; they are not considered black cards.').
card_image_name('kormus bell'/'2ED', 'kormus bell').
card_uid('kormus bell'/'2ED', '2ED:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'2ED', 'Rare').
card_artist('kormus bell'/'2ED', 'Christopher Rush').
card_multiverse_id('kormus bell'/'2ED', '624').

card_in_set('kudzu', '2ED').
card_original_type('kudzu'/'2ED', 'Enchant Land').
card_original_text('kudzu'/'2ED', 'When target land becomes tapped, it is destroyed. Unless that was the last land in play, Kudzu is not discarded; instead, the player whose land it just destroyed may place it on another land of his or her choice.').
card_image_name('kudzu'/'2ED', 'kudzu').
card_uid('kudzu'/'2ED', '2ED:Kudzu:kudzu').
card_rarity('kudzu'/'2ED', 'Rare').
card_artist('kudzu'/'2ED', 'Mark Poole').
card_multiverse_id('kudzu'/'2ED', '757').

card_in_set('lance', '2ED').
card_original_type('lance'/'2ED', 'Enchant Creature').
card_original_text('lance'/'2ED', 'Target creature gains first strike.').
card_image_name('lance'/'2ED', 'lance').
card_uid('lance'/'2ED', '2ED:Lance:lance').
card_rarity('lance'/'2ED', 'Uncommon').
card_artist('lance'/'2ED', 'Rob Alexander').
card_multiverse_id('lance'/'2ED', '856').

card_in_set('ley druid', '2ED').
card_original_type('ley druid'/'2ED', 'Summon — Cleric').
card_original_text('ley druid'/'2ED', 'Tap Druid to untap a land of your choice. This action can be played as an interrupt.').
card_image_name('ley druid'/'2ED', 'ley druid').
card_uid('ley druid'/'2ED', '2ED:Ley Druid:ley druid').
card_rarity('ley druid'/'2ED', 'Uncommon').
card_artist('ley druid'/'2ED', 'Sandra Everingham').
card_flavor_text('ley druid'/'2ED', 'After years of training, the Druid becomes one with nature, drawing power from the land and returning it when needed.').
card_multiverse_id('ley druid'/'2ED', '758').

card_in_set('library of leng', '2ED').
card_original_type('library of leng'/'2ED', 'Continuous Artifact').
card_original_text('library of leng'/'2ED', 'There is no limit to size of your hand. If a card forces you to discard, you may choose to discard to top of your library rather than to graveyard. If discard is random, you may look at card before deciding where to discard it.').
card_image_name('library of leng'/'2ED', 'library of leng').
card_uid('library of leng'/'2ED', '2ED:Library of Leng:library of leng').
card_rarity('library of leng'/'2ED', 'Uncommon').
card_artist('library of leng'/'2ED', 'Daniel Gelon').
card_multiverse_id('library of leng'/'2ED', '625').

card_in_set('lich', '2ED').
card_original_type('lich'/'2ED', 'Enchantment').
card_original_text('lich'/'2ED', 'You lose all life. If you gain life later in the game, instead draw one card from your library for each life. For each point of damage you suffer, you must destroy one of your cards in play. Creatures destroyed in this way cannot be regenerated. You lose if this enchantment is destroyed or if you suffer a point of damage without sending a card to the graveyard.').
card_image_name('lich'/'2ED', 'lich').
card_uid('lich'/'2ED', '2ED:Lich:lich').
card_rarity('lich'/'2ED', 'Rare').
card_artist('lich'/'2ED', 'Daniel Gelon').
card_multiverse_id('lich'/'2ED', '666').

card_in_set('lifeforce', '2ED').
card_original_type('lifeforce'/'2ED', 'Enchantment').
card_original_text('lifeforce'/'2ED', '{G}{G}: Destroy a black spell as it is being cast. This use may be played as an interrupt, and does not affect black cards already in play.').
card_image_name('lifeforce'/'2ED', 'lifeforce').
card_uid('lifeforce'/'2ED', '2ED:Lifeforce:lifeforce').
card_rarity('lifeforce'/'2ED', 'Uncommon').
card_artist('lifeforce'/'2ED', 'Dameon Willich').
card_multiverse_id('lifeforce'/'2ED', '759').

card_in_set('lifelace', '2ED').
card_original_type('lifelace'/'2ED', 'Interrupt').
card_original_text('lifelace'/'2ED', 'Changes the color of one card either being played or already in play to green. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('lifelace'/'2ED', 'lifelace').
card_uid('lifelace'/'2ED', '2ED:Lifelace:lifelace').
card_rarity('lifelace'/'2ED', 'Rare').
card_artist('lifelace'/'2ED', 'Amy Weber').
card_multiverse_id('lifelace'/'2ED', '760').

card_in_set('lifetap', '2ED').
card_original_type('lifetap'/'2ED', 'Enchantment').
card_original_text('lifetap'/'2ED', 'You gain 1 life each time any forest of opponent\'s becomes tapped.').
card_image_name('lifetap'/'2ED', 'lifetap').
card_uid('lifetap'/'2ED', '2ED:Lifetap:lifetap').
card_rarity('lifetap'/'2ED', 'Uncommon').
card_artist('lifetap'/'2ED', 'Anson Maddocks').
card_multiverse_id('lifetap'/'2ED', '706').

card_in_set('lightning bolt', '2ED').
card_original_type('lightning bolt'/'2ED', 'Instant').
card_original_text('lightning bolt'/'2ED', 'Lightning Bolt does 3 damage to one target.').
card_image_name('lightning bolt'/'2ED', 'lightning bolt').
card_uid('lightning bolt'/'2ED', '2ED:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'2ED', 'Common').
card_artist('lightning bolt'/'2ED', 'Christopher Rush').
card_multiverse_id('lightning bolt'/'2ED', '806').

card_in_set('living artifact', '2ED').
card_original_type('living artifact'/'2ED', 'Enchant Artifact').
card_original_text('living artifact'/'2ED', 'Put a counter on target artifact for each life you lose. During upkeep you may trade one counter for one life, but you can only trade in one counter each turn.').
card_image_name('living artifact'/'2ED', 'living artifact').
card_uid('living artifact'/'2ED', '2ED:Living Artifact:living artifact').
card_rarity('living artifact'/'2ED', 'Rare').
card_artist('living artifact'/'2ED', 'Anson Maddocks').
card_multiverse_id('living artifact'/'2ED', '761').

card_in_set('living lands', '2ED').
card_original_type('living lands'/'2ED', 'Enchantment').
card_original_text('living lands'/'2ED', 'Treat all forests in play as 1/1 creatures. Now they can be enchanted, killed, and so forth, and they can be tapped either for mana or to attack. The living lands have no color; they are not considered green cards.').
card_image_name('living lands'/'2ED', 'living lands').
card_uid('living lands'/'2ED', '2ED:Living Lands:living lands').
card_rarity('living lands'/'2ED', 'Rare').
card_artist('living lands'/'2ED', 'Jesper Myrfors').
card_multiverse_id('living lands'/'2ED', '762').

card_in_set('living wall', '2ED').
card_original_type('living wall'/'2ED', 'Artifact Creature').
card_original_text('living wall'/'2ED', 'Counts as a wall. {1}: Regenerates.').
card_image_name('living wall'/'2ED', 'living wall').
card_uid('living wall'/'2ED', '2ED:Living Wall:living wall').
card_rarity('living wall'/'2ED', 'Uncommon').
card_artist('living wall'/'2ED', 'Anson Maddocks').
card_flavor_text('living wall'/'2ED', 'Some fiendish mage had created a horrifying wall of living flesh, patched together from a jumble of still-recognizable body parts. As we sought to hew our way through it, some unknown power healed the gaping wounds we cut, denying us passage.').
card_multiverse_id('living wall'/'2ED', '626').

card_in_set('llanowar elves', '2ED').
card_original_type('llanowar elves'/'2ED', 'Summon — Elves').
card_original_text('llanowar elves'/'2ED', 'Tap to add 1 green mana to your mana pool. This tap can be played as an interrupt.').
card_image_name('llanowar elves'/'2ED', 'llanowar elves').
card_uid('llanowar elves'/'2ED', '2ED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'2ED', 'Common').
card_artist('llanowar elves'/'2ED', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'2ED', 'Whenever the Llanowar Elves gather the fruits of their forest, they leave one plant of each type untouched, considering that nature\'s portion.').
card_multiverse_id('llanowar elves'/'2ED', '763').

card_in_set('lord of atlantis', '2ED').
card_original_type('lord of atlantis'/'2ED', 'Summon — Lord of Atlantis').
card_original_text('lord of atlantis'/'2ED', 'All Merfolk in play gain islandwalk and +1/+1 while this card is in play.').
card_image_name('lord of atlantis'/'2ED', 'lord of atlantis').
card_uid('lord of atlantis'/'2ED', '2ED:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'2ED', 'Rare').
card_artist('lord of atlantis'/'2ED', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'2ED', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').
card_multiverse_id('lord of atlantis'/'2ED', '707').

card_in_set('lord of the pit', '2ED').
card_original_type('lord of the pit'/'2ED', 'Summon — Demon').
card_original_text('lord of the pit'/'2ED', 'Flying, trample\nYou must sacrifice one of your own creatures during upkeep or Lord of the Pit does 7 damage to you. You may still attack with Lord of the Pit even if you failed to sacrifice a creature.').
card_image_name('lord of the pit'/'2ED', 'lord of the pit').
card_uid('lord of the pit'/'2ED', '2ED:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'2ED', 'Rare').
card_artist('lord of the pit'/'2ED', 'Mark Tedin').
card_multiverse_id('lord of the pit'/'2ED', '667').

card_in_set('lure', '2ED').
card_original_type('lure'/'2ED', 'Enchant Creature').
card_original_text('lure'/'2ED', 'All creatures able to block target creature must do so. If a creature has the ability to block more than one creature, Lure does not prevent this. If there is more than one attacking creature with Lure, defender may choose which of them each defending creature blocks.').
card_image_name('lure'/'2ED', 'lure').
card_uid('lure'/'2ED', '2ED:Lure:lure').
card_rarity('lure'/'2ED', 'Uncommon').
card_artist('lure'/'2ED', 'Anson Maddocks').
card_multiverse_id('lure'/'2ED', '764').

card_in_set('magical hack', '2ED').
card_original_type('magical hack'/'2ED', 'Interrupt').
card_original_text('magical hack'/'2ED', 'Change the text of any card being played or already in play by replacing one basic land type with another. For example, you can change \"swampwalk\" to \"plainswalk.\"').
card_image_name('magical hack'/'2ED', 'magical hack').
card_uid('magical hack'/'2ED', '2ED:Magical Hack:magical hack').
card_rarity('magical hack'/'2ED', 'Rare').
card_artist('magical hack'/'2ED', 'Julie Baroh').
card_multiverse_id('magical hack'/'2ED', '708').

card_in_set('mahamoti djinn', '2ED').
card_original_type('mahamoti djinn'/'2ED', 'Summon — Djinn').
card_original_text('mahamoti djinn'/'2ED', 'Flying').
card_image_name('mahamoti djinn'/'2ED', 'mahamoti djinn').
card_uid('mahamoti djinn'/'2ED', '2ED:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'2ED', 'Rare').
card_artist('mahamoti djinn'/'2ED', 'Dan Frazier').
card_flavor_text('mahamoti djinn'/'2ED', 'Of royal blood among the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').
card_multiverse_id('mahamoti djinn'/'2ED', '709').

card_in_set('mana flare', '2ED').
card_original_type('mana flare'/'2ED', 'Enchantment').
card_original_text('mana flare'/'2ED', 'Whenever either player taps land for mana, each land produces 1 extra mana of the appropriate type.').
card_image_name('mana flare'/'2ED', 'mana flare').
card_uid('mana flare'/'2ED', '2ED:Mana Flare:mana flare').
card_rarity('mana flare'/'2ED', 'Rare').
card_artist('mana flare'/'2ED', 'Christopher Rush').
card_multiverse_id('mana flare'/'2ED', '807').

card_in_set('mana short', '2ED').
card_original_type('mana short'/'2ED', 'Instant').
card_original_text('mana short'/'2ED', 'All opponent\'s lands are tapped, and opponent\'s mana pool is emptied. Opponent takes no damage from unspent mana.').
card_image_name('mana short'/'2ED', 'mana short').
card_uid('mana short'/'2ED', '2ED:Mana Short:mana short').
card_rarity('mana short'/'2ED', 'Rare').
card_artist('mana short'/'2ED', 'Dameon Willich').
card_multiverse_id('mana short'/'2ED', '710').

card_in_set('mana vault', '2ED').
card_original_type('mana vault'/'2ED', 'Mono Artifact').
card_original_text('mana vault'/'2ED', 'Tap to add 3 colorless mana to your mana pool. Mana Vault doesn\'t untap normally during untap phase; to untap it, you must pay 4 mana. If Mana Vault remains tapped during upkeep it does 1 damage to you. Tapping this artifact can be played as an interrupt.').
card_image_name('mana vault'/'2ED', 'mana vault').
card_uid('mana vault'/'2ED', '2ED:Mana Vault:mana vault').
card_rarity('mana vault'/'2ED', 'Rare').
card_artist('mana vault'/'2ED', 'Mark Tedin').
card_multiverse_id('mana vault'/'2ED', '627').

card_in_set('manabarbs', '2ED').
card_original_type('manabarbs'/'2ED', 'Enchantment').
card_original_text('manabarbs'/'2ED', 'Whenever any land is tapped, Manabarbs does 1 damage to the land\'s controller.').
card_image_name('manabarbs'/'2ED', 'manabarbs').
card_uid('manabarbs'/'2ED', '2ED:Manabarbs:manabarbs').
card_rarity('manabarbs'/'2ED', 'Rare').
card_artist('manabarbs'/'2ED', 'Christopher Rush').
card_multiverse_id('manabarbs'/'2ED', '808').

card_in_set('meekstone', '2ED').
card_original_type('meekstone'/'2ED', 'Continuous Artifact').
card_original_text('meekstone'/'2ED', 'Any creature with power greater than 2 may not be untapped as normal during the untap phase.').
card_image_name('meekstone'/'2ED', 'meekstone').
card_uid('meekstone'/'2ED', '2ED:Meekstone:meekstone').
card_rarity('meekstone'/'2ED', 'Rare').
card_artist('meekstone'/'2ED', 'Quinton Hoover').
card_multiverse_id('meekstone'/'2ED', '628').

card_in_set('merfolk of the pearl trident', '2ED').
card_original_type('merfolk of the pearl trident'/'2ED', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'2ED', '').
card_image_name('merfolk of the pearl trident'/'2ED', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'2ED', '2ED:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'2ED', 'Common').
card_artist('merfolk of the pearl trident'/'2ED', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'2ED', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').
card_multiverse_id('merfolk of the pearl trident'/'2ED', '711').

card_in_set('mesa pegasus', '2ED').
card_original_type('mesa pegasus'/'2ED', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'2ED', 'Flying, bands').
card_image_name('mesa pegasus'/'2ED', 'mesa pegasus').
card_uid('mesa pegasus'/'2ED', '2ED:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'2ED', 'Common').
card_artist('mesa pegasus'/'2ED', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'2ED', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').
card_multiverse_id('mesa pegasus'/'2ED', '857').

card_in_set('mind twist', '2ED').
card_original_type('mind twist'/'2ED', 'Sorcery').
card_original_text('mind twist'/'2ED', 'Opponent must discard X cards at random from hand. If opponent doesn\'t have enough cards in hand, entire hand is discarded.').
card_image_name('mind twist'/'2ED', 'mind twist').
card_uid('mind twist'/'2ED', '2ED:Mind Twist:mind twist').
card_rarity('mind twist'/'2ED', 'Rare').
card_artist('mind twist'/'2ED', 'Julie Baroh').
card_multiverse_id('mind twist'/'2ED', '668').

card_in_set('mons\'s goblin raiders', '2ED').
card_original_type('mons\'s goblin raiders'/'2ED', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'2ED', '').
card_image_name('mons\'s goblin raiders'/'2ED', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'2ED', '2ED:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'2ED', 'Common').
card_artist('mons\'s goblin raiders'/'2ED', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'2ED', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his raiders are the thunderhead that leads in the storm.').
card_multiverse_id('mons\'s goblin raiders'/'2ED', '809').

card_in_set('mountain', '2ED').
card_original_type('mountain'/'2ED', 'Land').
card_original_text('mountain'/'2ED', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'2ED', 'mountain1').
card_uid('mountain'/'2ED', '2ED:Mountain:mountain1').
card_rarity('mountain'/'2ED', 'Basic Land').
card_artist('mountain'/'2ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'2ED', '893').

card_in_set('mountain', '2ED').
card_original_type('mountain'/'2ED', 'Land').
card_original_text('mountain'/'2ED', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'2ED', 'mountain2').
card_uid('mountain'/'2ED', '2ED:Mountain:mountain2').
card_rarity('mountain'/'2ED', 'Basic Land').
card_artist('mountain'/'2ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'2ED', '892').

card_in_set('mountain', '2ED').
card_original_type('mountain'/'2ED', 'Land').
card_original_text('mountain'/'2ED', 'Tap to add {R} to your mana pool.').
card_image_name('mountain'/'2ED', 'mountain3').
card_uid('mountain'/'2ED', '2ED:Mountain:mountain3').
card_rarity('mountain'/'2ED', 'Basic Land').
card_artist('mountain'/'2ED', 'Douglas Shuler').
card_multiverse_id('mountain'/'2ED', '891').

card_in_set('mox emerald', '2ED').
card_original_type('mox emerald'/'2ED', 'Mono Artifact').
card_original_text('mox emerald'/'2ED', 'Add 1 green mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox emerald'/'2ED', 'mox emerald').
card_uid('mox emerald'/'2ED', '2ED:Mox Emerald:mox emerald').
card_rarity('mox emerald'/'2ED', 'Rare').
card_artist('mox emerald'/'2ED', 'Dan Frazier').
card_multiverse_id('mox emerald'/'2ED', '629').

card_in_set('mox jet', '2ED').
card_original_type('mox jet'/'2ED', 'Mono Artifact').
card_original_text('mox jet'/'2ED', 'Add 1 black mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox jet'/'2ED', 'mox jet').
card_uid('mox jet'/'2ED', '2ED:Mox Jet:mox jet').
card_rarity('mox jet'/'2ED', 'Rare').
card_artist('mox jet'/'2ED', 'Dan Frazier').
card_multiverse_id('mox jet'/'2ED', '630').

card_in_set('mox pearl', '2ED').
card_original_type('mox pearl'/'2ED', 'Mono Artifact').
card_original_text('mox pearl'/'2ED', 'Add 1 white mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox pearl'/'2ED', 'mox pearl').
card_uid('mox pearl'/'2ED', '2ED:Mox Pearl:mox pearl').
card_rarity('mox pearl'/'2ED', 'Rare').
card_artist('mox pearl'/'2ED', 'Dan Frazier').
card_multiverse_id('mox pearl'/'2ED', '631').

card_in_set('mox ruby', '2ED').
card_original_type('mox ruby'/'2ED', 'Mono Artifact').
card_original_text('mox ruby'/'2ED', 'Add 1 red mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox ruby'/'2ED', 'mox ruby').
card_uid('mox ruby'/'2ED', '2ED:Mox Ruby:mox ruby').
card_rarity('mox ruby'/'2ED', 'Rare').
card_artist('mox ruby'/'2ED', 'Dan Frazier').
card_multiverse_id('mox ruby'/'2ED', '632').

card_in_set('mox sapphire', '2ED').
card_original_type('mox sapphire'/'2ED', 'Mono Artifact').
card_original_text('mox sapphire'/'2ED', 'Add 1 blue mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('mox sapphire'/'2ED', 'mox sapphire').
card_uid('mox sapphire'/'2ED', '2ED:Mox Sapphire:mox sapphire').
card_rarity('mox sapphire'/'2ED', 'Rare').
card_artist('mox sapphire'/'2ED', 'Dan Frazier').
card_multiverse_id('mox sapphire'/'2ED', '633').

card_in_set('natural selection', '2ED').
card_original_type('natural selection'/'2ED', 'Instant').
card_original_text('natural selection'/'2ED', 'Look at top three cards of any player\'s library. You may opt to rearrange those three cards or shuffle the entire library.').
card_image_name('natural selection'/'2ED', 'natural selection').
card_uid('natural selection'/'2ED', '2ED:Natural Selection:natural selection').
card_rarity('natural selection'/'2ED', 'Rare').
card_artist('natural selection'/'2ED', 'Mark Poole').
card_multiverse_id('natural selection'/'2ED', '765').

card_in_set('nether shadow', '2ED').
card_original_type('nether shadow'/'2ED', 'Summon — Shadow').
card_original_text('nether shadow'/'2ED', 'If Shadow is in graveyard with any combination of cards above it that includes at least three creatures, it can be returned to play during upkeep for its normal casting cost. Shadow can attack on same turn summoned or returned to play.').
card_image_name('nether shadow'/'2ED', 'nether shadow').
card_uid('nether shadow'/'2ED', '2ED:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'2ED', 'Rare').
card_artist('nether shadow'/'2ED', 'Christopher Rush').
card_multiverse_id('nether shadow'/'2ED', '669').

card_in_set('nettling imp', '2ED').
card_original_type('nettling imp'/'2ED', 'Summon — Imp').
card_original_text('nettling imp'/'2ED', 'Tap to force a particular one of opponent\'s non-wall creatures to attack. If target creature cannot attack, it is destroyed at end of turn. This tap should be played during opponent\'s turn, before the attack. May not be used on creatures summoned this turn.').
card_image_name('nettling imp'/'2ED', 'nettling imp').
card_uid('nettling imp'/'2ED', '2ED:Nettling Imp:nettling imp').
card_rarity('nettling imp'/'2ED', 'Uncommon').
card_artist('nettling imp'/'2ED', 'Quinton Hoover').
card_multiverse_id('nettling imp'/'2ED', '670').

card_in_set('nevinyrral\'s disk', '2ED').
card_original_type('nevinyrral\'s disk'/'2ED', 'Mono Artifact').
card_original_text('nevinyrral\'s disk'/'2ED', '{1}: Destroys all creatures, enchantments, and artifacts in play. Disk begins tapped but can be untapped as usual. Disk destroys itself when used.').
card_image_name('nevinyrral\'s disk'/'2ED', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'2ED', '2ED:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'2ED', 'Rare').
card_artist('nevinyrral\'s disk'/'2ED', 'Mark Tedin').
card_multiverse_id('nevinyrral\'s disk'/'2ED', '634').

card_in_set('nightmare', '2ED').
card_original_type('nightmare'/'2ED', 'Summon — Nightmare').
card_original_text('nightmare'/'2ED', 'Flying\nNightmare\'s power and toughness both equal the number of swamps its controller has in play.').
card_image_name('nightmare'/'2ED', 'nightmare').
card_uid('nightmare'/'2ED', '2ED:Nightmare:nightmare').
card_rarity('nightmare'/'2ED', 'Rare').
card_artist('nightmare'/'2ED', 'Melissa A. Benson').
card_flavor_text('nightmare'/'2ED', 'The Nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the Nightmare\'s rage and terrifying strength.').
card_multiverse_id('nightmare'/'2ED', '671').

card_in_set('northern paladin', '2ED').
card_original_type('northern paladin'/'2ED', 'Summon — Paladin').
card_original_text('northern paladin'/'2ED', '{W}{W} and tap: Destroys a black card in play. Cannot be used to cancel a black spell as it is being cast.').
card_image_name('northern paladin'/'2ED', 'northern paladin').
card_uid('northern paladin'/'2ED', '2ED:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'2ED', 'Rare').
card_artist('northern paladin'/'2ED', 'Douglas Shuler').
card_flavor_text('northern paladin'/'2ED', '\"Look to the north; there you will find aid and comfort.\"\n—The Book of Tal').
card_multiverse_id('northern paladin'/'2ED', '858').

card_in_set('obsianus golem', '2ED').
card_original_type('obsianus golem'/'2ED', 'Artifact Creature').
card_original_text('obsianus golem'/'2ED', '').
card_image_name('obsianus golem'/'2ED', 'obsianus golem').
card_uid('obsianus golem'/'2ED', '2ED:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'2ED', 'Uncommon').
card_artist('obsianus golem'/'2ED', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'2ED', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone...\"\n—Song of the Artificer').
card_multiverse_id('obsianus golem'/'2ED', '635').

card_in_set('orcish artillery', '2ED').
card_original_type('orcish artillery'/'2ED', 'Summon — Orcs').
card_original_text('orcish artillery'/'2ED', 'Tap to do 2 damage to any target, but you suffer 3 damage as well.').
card_image_name('orcish artillery'/'2ED', 'orcish artillery').
card_uid('orcish artillery'/'2ED', '2ED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'2ED', 'Uncommon').
card_artist('orcish artillery'/'2ED', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'2ED', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').
card_multiverse_id('orcish artillery'/'2ED', '810').

card_in_set('orcish oriflamme', '2ED').
card_original_type('orcish oriflamme'/'2ED', 'Enchantment').
card_original_text('orcish oriflamme'/'2ED', 'When attacking, all of your attacking creatures gain +1/+0.').
card_image_name('orcish oriflamme'/'2ED', 'orcish oriflamme').
card_uid('orcish oriflamme'/'2ED', '2ED:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'2ED', 'Uncommon').
card_artist('orcish oriflamme'/'2ED', 'Dan Frazier').
card_multiverse_id('orcish oriflamme'/'2ED', '811').

card_in_set('paralyze', '2ED').
card_original_type('paralyze'/'2ED', 'Enchant Creature').
card_original_text('paralyze'/'2ED', 'Target creature is not untapped as normal during untap phase unless 4 mana are spent. Tap target creature when Paralyze is cast.').
card_image_name('paralyze'/'2ED', 'paralyze').
card_uid('paralyze'/'2ED', '2ED:Paralyze:paralyze').
card_rarity('paralyze'/'2ED', 'Common').
card_artist('paralyze'/'2ED', 'Anson Maddocks').
card_multiverse_id('paralyze'/'2ED', '672').

card_in_set('pearled unicorn', '2ED').
card_original_type('pearled unicorn'/'2ED', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'2ED', '').
card_image_name('pearled unicorn'/'2ED', 'pearled unicorn').
card_uid('pearled unicorn'/'2ED', '2ED:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'2ED', 'Common').
card_artist('pearled unicorn'/'2ED', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'2ED', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\" —Lewis Carroll').
card_multiverse_id('pearled unicorn'/'2ED', '859').

card_in_set('personal incarnation', '2ED').
card_original_type('personal incarnation'/'2ED', 'Summon — Avatar').
card_original_text('personal incarnation'/'2ED', 'Caster can redirect any or all damage done to Personal Incarnation to self instead. The source of the damage is unchanged. If Personal Incarnation is destroyed, caster loses half his or her remaining life points, rounding up the loss.').
card_image_name('personal incarnation'/'2ED', 'personal incarnation').
card_uid('personal incarnation'/'2ED', '2ED:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'2ED', 'Rare').
card_artist('personal incarnation'/'2ED', 'Kev Brockschmidt').
card_multiverse_id('personal incarnation'/'2ED', '860').

card_in_set('pestilence', '2ED').
card_original_type('pestilence'/'2ED', 'Enchantment').
card_original_text('pestilence'/'2ED', '{B}: Do 1 damage to each creature and to both players. Pestilence must be discarded at end of any turn in which there are no creatures in play at end of turn.').
card_image_name('pestilence'/'2ED', 'pestilence').
card_uid('pestilence'/'2ED', '2ED:Pestilence:pestilence').
card_rarity('pestilence'/'2ED', 'Common').
card_artist('pestilence'/'2ED', 'Jesper Myrfors').
card_multiverse_id('pestilence'/'2ED', '673').

card_in_set('phantasmal forces', '2ED').
card_original_type('phantasmal forces'/'2ED', 'Summon — Phantasm').
card_original_text('phantasmal forces'/'2ED', 'Flying\nController must spend {U} during upkeep to maintain or Phantasmal Forces are destroyed.').
card_image_name('phantasmal forces'/'2ED', 'phantasmal forces').
card_uid('phantasmal forces'/'2ED', '2ED:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'2ED', 'Uncommon').
card_artist('phantasmal forces'/'2ED', 'Mark Poole').
card_flavor_text('phantasmal forces'/'2ED', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').
card_multiverse_id('phantasmal forces'/'2ED', '712').

card_in_set('phantasmal terrain', '2ED').
card_original_type('phantasmal terrain'/'2ED', 'Enchant Land').
card_original_text('phantasmal terrain'/'2ED', 'Target land changes to any basic land type of caster\'s choice. Land type is set when cast and may not be further altered by this enchantment.').
card_image_name('phantasmal terrain'/'2ED', 'phantasmal terrain').
card_uid('phantasmal terrain'/'2ED', '2ED:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'2ED', 'Common').
card_artist('phantasmal terrain'/'2ED', 'Dameon Willich').
card_multiverse_id('phantasmal terrain'/'2ED', '713').

card_in_set('phantom monster', '2ED').
card_original_type('phantom monster'/'2ED', 'Summon — Phantasm').
card_original_text('phantom monster'/'2ED', 'Flying').
card_image_name('phantom monster'/'2ED', 'phantom monster').
card_uid('phantom monster'/'2ED', '2ED:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'2ED', 'Uncommon').
card_artist('phantom monster'/'2ED', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'2ED', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').
card_multiverse_id('phantom monster'/'2ED', '714').

card_in_set('pirate ship', '2ED').
card_original_type('pirate ship'/'2ED', 'Summon — Ship').
card_original_text('pirate ship'/'2ED', 'Tap to do 1 damage to any target. Cannot attack unless opponent has islands in play, though controller may still tap. Pirate Ship is destroyed immediately if at any time controller has no islands in play.').
card_image_name('pirate ship'/'2ED', 'pirate ship').
card_uid('pirate ship'/'2ED', '2ED:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'2ED', 'Rare').
card_artist('pirate ship'/'2ED', 'Tom Wänerstrand').
card_multiverse_id('pirate ship'/'2ED', '715').

card_in_set('plague rats', '2ED').
card_original_type('plague rats'/'2ED', 'Summon — Rats').
card_original_text('plague rats'/'2ED', 'The Xs below are the number of Plague Rats in play, counting both sides. Thus if there are 2 Plague Rats in play, each has power and toughness 2/2.').
card_image_name('plague rats'/'2ED', 'plague rats').
card_uid('plague rats'/'2ED', '2ED:Plague Rats:plague rats').
card_rarity('plague rats'/'2ED', 'Common').
card_artist('plague rats'/'2ED', 'Anson Maddocks').
card_flavor_text('plague rats'/'2ED', '\"Should you a Rat to madness tease\nWhy ev\'n a Rat may plague you...\"\n—Samuel Coleridge, \"Recantation\"').
card_multiverse_id('plague rats'/'2ED', '674').

card_in_set('plains', '2ED').
card_original_type('plains'/'2ED', 'Land').
card_original_text('plains'/'2ED', 'Tap to add {W} to your mana pool.').
card_image_name('plains'/'2ED', 'plains1').
card_uid('plains'/'2ED', '2ED:Plains:plains1').
card_rarity('plains'/'2ED', 'Basic Land').
card_artist('plains'/'2ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'2ED', '897').

card_in_set('plains', '2ED').
card_original_type('plains'/'2ED', 'Land').
card_original_text('plains'/'2ED', 'Tap to add {W} to your mana pool.').
card_image_name('plains'/'2ED', 'plains2').
card_uid('plains'/'2ED', '2ED:Plains:plains2').
card_rarity('plains'/'2ED', 'Basic Land').
card_artist('plains'/'2ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'2ED', '898').

card_in_set('plains', '2ED').
card_original_type('plains'/'2ED', 'Land').
card_original_text('plains'/'2ED', 'Tap to add {W} to your mana pool.').
card_image_name('plains'/'2ED', 'plains3').
card_uid('plains'/'2ED', '2ED:Plains:plains3').
card_rarity('plains'/'2ED', 'Basic Land').
card_artist('plains'/'2ED', 'Jesper Myrfors').
card_multiverse_id('plains'/'2ED', '899').

card_in_set('plateau', '2ED').
card_original_type('plateau'/'2ED', 'Land').
card_original_text('plateau'/'2ED', 'Counts as both mountains and plains and is affected by spells that affect either. Tap to add either {R} or {W} to your mana pool.').
card_image_name('plateau'/'2ED', 'plateau').
card_uid('plateau'/'2ED', '2ED:Plateau:plateau').
card_rarity('plateau'/'2ED', 'Rare').
card_artist('plateau'/'2ED', 'Drew Tucker').
card_multiverse_id('plateau'/'2ED', '880').

card_in_set('power leak', '2ED').
card_original_type('power leak'/'2ED', 'Enchant Enchantment').
card_original_text('power leak'/'2ED', 'Target enchantment costs 2 extra mana each turn during upkeep. If target enchantment\'s controller cannot or will not pay this extra mana, Power Leak does 1 damage to him or her for each unpaid mana.').
card_image_name('power leak'/'2ED', 'power leak').
card_uid('power leak'/'2ED', '2ED:Power Leak:power leak').
card_rarity('power leak'/'2ED', 'Common').
card_artist('power leak'/'2ED', 'Drew Tucker').
card_multiverse_id('power leak'/'2ED', '716').

card_in_set('power sink', '2ED').
card_original_type('power sink'/'2ED', 'Interrupt').
card_original_text('power sink'/'2ED', 'Target spell is countered unless its caster spends X more mana; caster of target spell can\'t choose to let it be countered. If caster of target spell doesn\'t have enough mana, all available mana from lands and mana pool must be paid but target spell will still be countered.').
card_image_name('power sink'/'2ED', 'power sink').
card_uid('power sink'/'2ED', '2ED:Power Sink:power sink').
card_rarity('power sink'/'2ED', 'Common').
card_artist('power sink'/'2ED', 'Richard Thomas').
card_multiverse_id('power sink'/'2ED', '717').

card_in_set('power surge', '2ED').
card_original_type('power surge'/'2ED', 'Enchantment').
card_original_text('power surge'/'2ED', 'Before untapping lands at the start of a turn, each player takes 1 damage for each land he or she controls but did not tap during the previous turn.').
card_image_name('power surge'/'2ED', 'power surge').
card_uid('power surge'/'2ED', '2ED:Power Surge:power surge').
card_rarity('power surge'/'2ED', 'Rare').
card_artist('power surge'/'2ED', 'Douglas Shuler').
card_multiverse_id('power surge'/'2ED', '812').

card_in_set('prodigal sorcerer', '2ED').
card_original_type('prodigal sorcerer'/'2ED', 'Summon — Wizard').
card_original_text('prodigal sorcerer'/'2ED', 'Tap to do 1 damage to any target.').
card_image_name('prodigal sorcerer'/'2ED', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'2ED', '2ED:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'2ED', 'Common').
card_artist('prodigal sorcerer'/'2ED', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'2ED', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').
card_multiverse_id('prodigal sorcerer'/'2ED', '718').

card_in_set('psionic blast', '2ED').
card_original_type('psionic blast'/'2ED', 'Instant').
card_original_text('psionic blast'/'2ED', 'Psionic Blast does 4 damage to any target, but it does 2 damage to you as well.').
card_image_name('psionic blast'/'2ED', 'psionic blast').
card_uid('psionic blast'/'2ED', '2ED:Psionic Blast:psionic blast').
card_rarity('psionic blast'/'2ED', 'Uncommon').
card_artist('psionic blast'/'2ED', 'Douglas Shuler').
card_multiverse_id('psionic blast'/'2ED', '719').

card_in_set('psychic venom', '2ED').
card_original_type('psychic venom'/'2ED', 'Enchant Land').
card_original_text('psychic venom'/'2ED', 'Whenever target land is tapped, Psychic Venom does 2 damage to target land\'s controller.').
card_image_name('psychic venom'/'2ED', 'psychic venom').
card_uid('psychic venom'/'2ED', '2ED:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'2ED', 'Common').
card_artist('psychic venom'/'2ED', 'Brian Snõddy').
card_multiverse_id('psychic venom'/'2ED', '720').

card_in_set('purelace', '2ED').
card_original_type('purelace'/'2ED', 'Interrupt').
card_original_text('purelace'/'2ED', 'Changes the color of one card either being played or already in play to white. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('purelace'/'2ED', 'purelace').
card_uid('purelace'/'2ED', '2ED:Purelace:purelace').
card_rarity('purelace'/'2ED', 'Rare').
card_artist('purelace'/'2ED', 'Sandra Everingham').
card_multiverse_id('purelace'/'2ED', '861').

card_in_set('raging river', '2ED').
card_original_type('raging river'/'2ED', 'Enchantment').
card_original_text('raging river'/'2ED', 'When you attack, non-flying defending creatures must be divided as opponent wishes between the left and right sides of the River. You then choose on which side of the River to place each attacking creature, and attacking creatures can only be blocked by flying creatures or those on the same side of the River.').
card_image_name('raging river'/'2ED', 'raging river').
card_uid('raging river'/'2ED', '2ED:Raging River:raging river').
card_rarity('raging river'/'2ED', 'Rare').
card_artist('raging river'/'2ED', 'Sandra Everingham').
card_multiverse_id('raging river'/'2ED', '813').

card_in_set('raise dead', '2ED').
card_original_type('raise dead'/'2ED', 'Sorcery').
card_original_text('raise dead'/'2ED', 'Return creature from  your graveyard to your hand.').
card_image_name('raise dead'/'2ED', 'raise dead').
card_uid('raise dead'/'2ED', '2ED:Raise Dead:raise dead').
card_rarity('raise dead'/'2ED', 'Common').
card_artist('raise dead'/'2ED', 'Jeff A. Menges').
card_multiverse_id('raise dead'/'2ED', '675').

card_in_set('red elemental blast', '2ED').
card_original_type('red elemental blast'/'2ED', 'Interrupt').
card_original_text('red elemental blast'/'2ED', 'Counters a blue spell being cast or destroys a blue card in play.').
card_image_name('red elemental blast'/'2ED', 'red elemental blast').
card_uid('red elemental blast'/'2ED', '2ED:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'2ED', 'Common').
card_artist('red elemental blast'/'2ED', 'Richard Thomas').
card_multiverse_id('red elemental blast'/'2ED', '814').

card_in_set('red ward', '2ED').
card_original_type('red ward'/'2ED', 'Enchant Creature').
card_original_text('red ward'/'2ED', 'Target creature gains protection from red.').
card_image_name('red ward'/'2ED', 'red ward').
card_uid('red ward'/'2ED', '2ED:Red Ward:red ward').
card_rarity('red ward'/'2ED', 'Uncommon').
card_artist('red ward'/'2ED', 'Dan Frazier').
card_multiverse_id('red ward'/'2ED', '862').

card_in_set('regeneration', '2ED').
card_original_type('regeneration'/'2ED', 'Enchant Creature').
card_original_text('regeneration'/'2ED', '{G} Target creature regenerates.').
card_image_name('regeneration'/'2ED', 'regeneration').
card_uid('regeneration'/'2ED', '2ED:Regeneration:regeneration').
card_rarity('regeneration'/'2ED', 'Common').
card_artist('regeneration'/'2ED', 'Quinton Hoover').
card_multiverse_id('regeneration'/'2ED', '766').

card_in_set('regrowth', '2ED').
card_original_type('regrowth'/'2ED', 'Sorcery').
card_original_text('regrowth'/'2ED', 'Return any card from your graveyard to your hand.').
card_image_name('regrowth'/'2ED', 'regrowth').
card_uid('regrowth'/'2ED', '2ED:Regrowth:regrowth').
card_rarity('regrowth'/'2ED', 'Uncommon').
card_artist('regrowth'/'2ED', 'Dameon Willich').
card_multiverse_id('regrowth'/'2ED', '767').

card_in_set('resurrection', '2ED').
card_original_type('resurrection'/'2ED', 'Sorcery').
card_original_text('resurrection'/'2ED', 'Take a creature from your graveyard and put it directly into play. You can\'t tap it until your next turn.').
card_image_name('resurrection'/'2ED', 'resurrection').
card_uid('resurrection'/'2ED', '2ED:Resurrection:resurrection').
card_rarity('resurrection'/'2ED', 'Uncommon').
card_artist('resurrection'/'2ED', 'Dan Frazier').
card_multiverse_id('resurrection'/'2ED', '863').

card_in_set('reverse damage', '2ED').
card_original_type('reverse damage'/'2ED', 'Instant').
card_original_text('reverse damage'/'2ED', 'All damage you have taken from any one source this turn is added to your life total instead of subtracted from it.').
card_image_name('reverse damage'/'2ED', 'reverse damage').
card_uid('reverse damage'/'2ED', '2ED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'2ED', 'Rare').
card_artist('reverse damage'/'2ED', 'Dameon Willich').
card_multiverse_id('reverse damage'/'2ED', '864').

card_in_set('righteousness', '2ED').
card_original_type('righteousness'/'2ED', 'Instant').
card_original_text('righteousness'/'2ED', 'Target defending creature gains +7/+7 until end of turn.').
card_image_name('righteousness'/'2ED', 'righteousness').
card_uid('righteousness'/'2ED', '2ED:Righteousness:righteousness').
card_rarity('righteousness'/'2ED', 'Rare').
card_artist('righteousness'/'2ED', 'Douglas Shuler').
card_multiverse_id('righteousness'/'2ED', '865').

card_in_set('roc of kher ridges', '2ED').
card_original_type('roc of kher ridges'/'2ED', 'Summon — Roc').
card_original_text('roc of kher ridges'/'2ED', 'Flying').
card_image_name('roc of kher ridges'/'2ED', 'roc of kher ridges').
card_uid('roc of kher ridges'/'2ED', '2ED:Roc of Kher Ridges:roc of kher ridges').
card_rarity('roc of kher ridges'/'2ED', 'Rare').
card_artist('roc of kher ridges'/'2ED', 'Andi Rusu').
card_flavor_text('roc of kher ridges'/'2ED', 'We encountered a valley topped with immense boulders and eerie rock formations. Suddenly one of these boulders toppled from its perch and sprouted gargantuan wings, casting a shadow of darkness and sending us fleeing in terror.').
card_multiverse_id('roc of kher ridges'/'2ED', '815').

card_in_set('rock hydra', '2ED').
card_original_type('rock hydra'/'2ED', 'Summon — Hydra').
card_original_text('rock hydra'/'2ED', 'Put X +1/+1 counters (heads) on Hydra. Each point of damage Hydra suffers destroys one head unless {R} is spent. During upkeep, new heads may be grown for {R}{R}{R} apiece.').
card_image_name('rock hydra'/'2ED', 'rock hydra').
card_uid('rock hydra'/'2ED', '2ED:Rock Hydra:rock hydra').
card_rarity('rock hydra'/'2ED', 'Rare').
card_artist('rock hydra'/'2ED', 'Jeff A. Menges').
card_multiverse_id('rock hydra'/'2ED', '816').

card_in_set('rod of ruin', '2ED').
card_original_type('rod of ruin'/'2ED', 'Mono Artifact').
card_original_text('rod of ruin'/'2ED', '{3}: Rod of Ruin does 1 damage to any target.').
card_image_name('rod of ruin'/'2ED', 'rod of ruin').
card_uid('rod of ruin'/'2ED', '2ED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'2ED', 'Uncommon').
card_artist('rod of ruin'/'2ED', 'Christopher Rush').
card_multiverse_id('rod of ruin'/'2ED', '636').

card_in_set('royal assassin', '2ED').
card_original_type('royal assassin'/'2ED', 'Summon — Assassin').
card_original_text('royal assassin'/'2ED', 'Tap to destroy any tapped creature.').
card_image_name('royal assassin'/'2ED', 'royal assassin').
card_uid('royal assassin'/'2ED', '2ED:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'2ED', 'Rare').
card_artist('royal assassin'/'2ED', 'Tom Wänerstrand').
card_flavor_text('royal assassin'/'2ED', 'Trained in the arts of stealth, the royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').
card_multiverse_id('royal assassin'/'2ED', '676').

card_in_set('sacrifice', '2ED').
card_original_type('sacrifice'/'2ED', 'Interrupt').
card_original_text('sacrifice'/'2ED', 'Destroy one of your creatures without regenerating it, and add to your mana pool a number of black mana equal to creature\'s casting cost.').
card_image_name('sacrifice'/'2ED', 'sacrifice').
card_uid('sacrifice'/'2ED', '2ED:Sacrifice:sacrifice').
card_rarity('sacrifice'/'2ED', 'Uncommon').
card_artist('sacrifice'/'2ED', 'Dan Frazier').
card_multiverse_id('sacrifice'/'2ED', '677').

card_in_set('samite healer', '2ED').
card_original_type('samite healer'/'2ED', 'Summon — Cleric').
card_original_text('samite healer'/'2ED', 'Tap to prevent 1 damage to any target.').
card_image_name('samite healer'/'2ED', 'samite healer').
card_uid('samite healer'/'2ED', '2ED:Samite Healer:samite healer').
card_rarity('samite healer'/'2ED', 'Common').
card_artist('samite healer'/'2ED', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'2ED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').
card_multiverse_id('samite healer'/'2ED', '866').

card_in_set('savannah', '2ED').
card_original_type('savannah'/'2ED', 'Land').
card_original_text('savannah'/'2ED', 'Counts as both plains and forest and is affected by spells that affect either. Tap to add either {W} or {G} to your mana pool.').
card_image_name('savannah'/'2ED', 'savannah').
card_uid('savannah'/'2ED', '2ED:Savannah:savannah').
card_rarity('savannah'/'2ED', 'Rare').
card_artist('savannah'/'2ED', 'Rob Alexander').
card_multiverse_id('savannah'/'2ED', '881').

card_in_set('savannah lions', '2ED').
card_original_type('savannah lions'/'2ED', 'Summon — Lions').
card_original_text('savannah lions'/'2ED', '').
card_image_name('savannah lions'/'2ED', 'savannah lions').
card_uid('savannah lions'/'2ED', '2ED:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'2ED', 'Rare').
card_artist('savannah lions'/'2ED', 'Daniel Gelon').
card_flavor_text('savannah lions'/'2ED', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless flat plains of their homeland.').
card_multiverse_id('savannah lions'/'2ED', '867').

card_in_set('scathe zombies', '2ED').
card_original_type('scathe zombies'/'2ED', 'Summon — Zombies').
card_original_text('scathe zombies'/'2ED', '').
card_image_name('scathe zombies'/'2ED', 'scathe zombies').
card_uid('scathe zombies'/'2ED', '2ED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'2ED', 'Common').
card_artist('scathe zombies'/'2ED', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'2ED', '\"They groaned, they stirred, they all uprose,/ Nor spake, nor moved their eyes;/ It had been strange, even in a dream,/ To have seen those dead men rise.\"/ —Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('scathe zombies'/'2ED', '678').

card_in_set('scavenging ghoul', '2ED').
card_original_type('scavenging ghoul'/'2ED', 'Summon — Ghoul').
card_original_text('scavenging ghoul'/'2ED', 'At the end of each turn, put one counter on the Ghoul for each other creature that was destroyed without regenerating during the turn. If Ghoul dies, you may use a counter to regenerate it; counters remain until used.').
card_image_name('scavenging ghoul'/'2ED', 'scavenging ghoul').
card_uid('scavenging ghoul'/'2ED', '2ED:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'2ED', 'Uncommon').
card_artist('scavenging ghoul'/'2ED', 'Jeff A. Menges').
card_multiverse_id('scavenging ghoul'/'2ED', '679').

card_in_set('scrubland', '2ED').
card_original_type('scrubland'/'2ED', 'Land').
card_original_text('scrubland'/'2ED', 'Counts as both plains and swamp and is affected by spells that affect either. Tap to add either {W} or {B} to your mana pool.').
card_image_name('scrubland'/'2ED', 'scrubland').
card_uid('scrubland'/'2ED', '2ED:Scrubland:scrubland').
card_rarity('scrubland'/'2ED', 'Rare').
card_artist('scrubland'/'2ED', 'Jesper Myrfors').
card_multiverse_id('scrubland'/'2ED', '882').

card_in_set('scryb sprites', '2ED').
card_original_type('scryb sprites'/'2ED', 'Summon — Faeries').
card_original_text('scryb sprites'/'2ED', 'Flying').
card_image_name('scryb sprites'/'2ED', 'scryb sprites').
card_uid('scryb sprites'/'2ED', '2ED:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'2ED', 'Common').
card_artist('scryb sprites'/'2ED', 'Amy Weber').
card_flavor_text('scryb sprites'/'2ED', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').
card_multiverse_id('scryb sprites'/'2ED', '768').

card_in_set('sea serpent', '2ED').
card_original_type('sea serpent'/'2ED', 'Summon — Serpent').
card_original_text('sea serpent'/'2ED', 'Serpent cannot attack unless opponent has islands in play. Serpent is destroyed immediately if at any time controller has no islands in play.').
card_image_name('sea serpent'/'2ED', 'sea serpent').
card_uid('sea serpent'/'2ED', '2ED:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'2ED', 'Common').
card_artist('sea serpent'/'2ED', 'Jeff A. Menges').
card_flavor_text('sea serpent'/'2ED', 'Legend has it that Serpents used to be bigger, but how could that be?').
card_multiverse_id('sea serpent'/'2ED', '721').

card_in_set('sedge troll', '2ED').
card_original_type('sedge troll'/'2ED', 'Summon — Troll').
card_original_text('sedge troll'/'2ED', '{B} Regenerates. Troll gains +1/+1 if controller has swamps in play.').
card_image_name('sedge troll'/'2ED', 'sedge troll').
card_uid('sedge troll'/'2ED', '2ED:Sedge Troll:sedge troll').
card_rarity('sedge troll'/'2ED', 'Rare').
card_artist('sedge troll'/'2ED', 'Dan Frazier').
card_flavor_text('sedge troll'/'2ED', 'The stench in the hovel was overpowering; something loathsome was cooking. Occasionally something surfaced in the thick paste, but my host would casually push it down before I could make out what it was.').
card_multiverse_id('sedge troll'/'2ED', '817').

card_in_set('sengir vampire', '2ED').
card_original_type('sengir vampire'/'2ED', 'Summon — Vampire').
card_original_text('sengir vampire'/'2ED', 'Flying\nVampire gets a +1/+1 counter each time a creature dies during a turn in which Vampire damaged it, unless the dead creature is regenerated.').
card_image_name('sengir vampire'/'2ED', 'sengir vampire').
card_uid('sengir vampire'/'2ED', '2ED:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'2ED', 'Uncommon').
card_artist('sengir vampire'/'2ED', 'Anson Maddocks').
card_multiverse_id('sengir vampire'/'2ED', '680').

card_in_set('serra angel', '2ED').
card_original_type('serra angel'/'2ED', 'Summon — Angel').
card_original_text('serra angel'/'2ED', 'Flying\nDoes not tap when attacking.').
card_image_name('serra angel'/'2ED', 'serra angel').
card_uid('serra angel'/'2ED', '2ED:Serra Angel:serra angel').
card_rarity('serra angel'/'2ED', 'Uncommon').
card_artist('serra angel'/'2ED', 'Douglas Shuler').
card_flavor_text('serra angel'/'2ED', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').
card_multiverse_id('serra angel'/'2ED', '868').

card_in_set('shanodin dryads', '2ED').
card_original_type('shanodin dryads'/'2ED', 'Summon — Nymphs').
card_original_text('shanodin dryads'/'2ED', 'Forestwalk').
card_image_name('shanodin dryads'/'2ED', 'shanodin dryads').
card_uid('shanodin dryads'/'2ED', '2ED:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'2ED', 'Common').
card_artist('shanodin dryads'/'2ED', 'Anson Maddocks').
card_flavor_text('shanodin dryads'/'2ED', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the Dryads of Shanodin Forest are seen only when they wish to be.').
card_multiverse_id('shanodin dryads'/'2ED', '769').

card_in_set('shatter', '2ED').
card_original_type('shatter'/'2ED', 'Instant').
card_original_text('shatter'/'2ED', 'Shatter destroys target artifact.').
card_image_name('shatter'/'2ED', 'shatter').
card_uid('shatter'/'2ED', '2ED:Shatter:shatter').
card_rarity('shatter'/'2ED', 'Common').
card_artist('shatter'/'2ED', 'Amy Weber').
card_multiverse_id('shatter'/'2ED', '818').

card_in_set('shivan dragon', '2ED').
card_original_type('shivan dragon'/'2ED', 'Summon — Dragon').
card_original_text('shivan dragon'/'2ED', 'Flying, {R} +1/+0 until end of turn.').
card_image_name('shivan dragon'/'2ED', 'shivan dragon').
card_uid('shivan dragon'/'2ED', '2ED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'2ED', 'Rare').
card_artist('shivan dragon'/'2ED', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'2ED', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').
card_multiverse_id('shivan dragon'/'2ED', '819').

card_in_set('simulacrum', '2ED').
card_original_type('simulacrum'/'2ED', 'Instant').
card_original_text('simulacrum'/'2ED', 'All damage done to you so far this turn is instead retroactively applied to one of your creatures in play. If this damage kills the creature it can be regenerated; even if there\'s more than enough damage to kill the creature, you don\'t suffer any of it. Further damage this turn is treated normally.').
card_image_name('simulacrum'/'2ED', 'simulacrum').
card_uid('simulacrum'/'2ED', '2ED:Simulacrum:simulacrum').
card_rarity('simulacrum'/'2ED', 'Uncommon').
card_artist('simulacrum'/'2ED', 'Mark Poole').
card_multiverse_id('simulacrum'/'2ED', '681').

card_in_set('sinkhole', '2ED').
card_original_type('sinkhole'/'2ED', 'Sorcery').
card_original_text('sinkhole'/'2ED', 'Destroys any one land.').
card_image_name('sinkhole'/'2ED', 'sinkhole').
card_uid('sinkhole'/'2ED', '2ED:Sinkhole:sinkhole').
card_rarity('sinkhole'/'2ED', 'Common').
card_artist('sinkhole'/'2ED', 'Sandra Everingham').
card_multiverse_id('sinkhole'/'2ED', '682').

card_in_set('siren\'s call', '2ED').
card_original_type('siren\'s call'/'2ED', 'Instant').
card_original_text('siren\'s call'/'2ED', 'All of opponent\'s creatures that can attack must do so. Any non-wall creatures that cannot attack are destroyed at end of turn. Play only during opponent\'s turn, before opponent\'s attack. Creatures summoned this turn are unaffected by Siren\'s Call.').
card_image_name('siren\'s call'/'2ED', 'siren\'s call').
card_uid('siren\'s call'/'2ED', '2ED:Siren\'s Call:siren\'s call').
card_rarity('siren\'s call'/'2ED', 'Uncommon').
card_artist('siren\'s call'/'2ED', 'Anson Maddocks').
card_multiverse_id('siren\'s call'/'2ED', '722').

card_in_set('sleight of mind', '2ED').
card_original_type('sleight of mind'/'2ED', 'Interrupt').
card_original_text('sleight of mind'/'2ED', 'Change the text of any card being played or already in play by replacing one color word with another. For example, you can change \"Counters red spells\" to \"Counters black spells.\" Cannot change mana symbols.').
card_image_name('sleight of mind'/'2ED', 'sleight of mind').
card_uid('sleight of mind'/'2ED', '2ED:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'2ED', 'Rare').
card_artist('sleight of mind'/'2ED', 'Mark Poole').
card_multiverse_id('sleight of mind'/'2ED', '723').

card_in_set('smoke', '2ED').
card_original_type('smoke'/'2ED', 'Enchantment').
card_original_text('smoke'/'2ED', 'Each player can only untap one creature during his or her untap phase.').
card_image_name('smoke'/'2ED', 'smoke').
card_uid('smoke'/'2ED', '2ED:Smoke:smoke').
card_rarity('smoke'/'2ED', 'Rare').
card_artist('smoke'/'2ED', 'Jesper Myrfors').
card_multiverse_id('smoke'/'2ED', '820').

card_in_set('sol ring', '2ED').
card_original_type('sol ring'/'2ED', 'Mono Artifact').
card_original_text('sol ring'/'2ED', 'Add 2 colorless mana to your mana pool. Tapping this artifact can be played as an interrupt.').
card_image_name('sol ring'/'2ED', 'sol ring').
card_uid('sol ring'/'2ED', '2ED:Sol Ring:sol ring').
card_rarity('sol ring'/'2ED', 'Uncommon').
card_artist('sol ring'/'2ED', 'Mark Tedin').
card_multiverse_id('sol ring'/'2ED', '637').

card_in_set('soul net', '2ED').
card_original_type('soul net'/'2ED', 'Poly Artifact').
card_original_text('soul net'/'2ED', '{1}: You gain 1 life every time a creature is destroyed, unless it is then regenerated.').
card_image_name('soul net'/'2ED', 'soul net').
card_uid('soul net'/'2ED', '2ED:Soul Net:soul net').
card_rarity('soul net'/'2ED', 'Uncommon').
card_artist('soul net'/'2ED', 'Dameon Willich').
card_multiverse_id('soul net'/'2ED', '638').

card_in_set('spell blast', '2ED').
card_original_type('spell blast'/'2ED', 'Interrupt').
card_original_text('spell blast'/'2ED', 'Target spell is countered; X is cost of target spell.').
card_image_name('spell blast'/'2ED', 'spell blast').
card_uid('spell blast'/'2ED', '2ED:Spell Blast:spell blast').
card_rarity('spell blast'/'2ED', 'Common').
card_artist('spell blast'/'2ED', 'Brian Snõddy').
card_multiverse_id('spell blast'/'2ED', '724').

card_in_set('stasis', '2ED').
card_original_type('stasis'/'2ED', 'Enchantment').
card_original_text('stasis'/'2ED', 'Players do not get an untap phase. Pay {U} during your upkeep or Stasis is destroyed.').
card_image_name('stasis'/'2ED', 'stasis').
card_uid('stasis'/'2ED', '2ED:Stasis:stasis').
card_rarity('stasis'/'2ED', 'Rare').
card_artist('stasis'/'2ED', 'Fay Jones').
card_multiverse_id('stasis'/'2ED', '725').

card_in_set('steal artifact', '2ED').
card_original_type('steal artifact'/'2ED', 'Enchant Artifact').
card_original_text('steal artifact'/'2ED', 'You control target artifact until enchantment is discarded or game ends. If target artifact was tapped when stolen, it stays tapped until you can untap it. If destroyed, target artifact is put in its owner\'s graveyard.').
card_image_name('steal artifact'/'2ED', 'steal artifact').
card_uid('steal artifact'/'2ED', '2ED:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'2ED', 'Uncommon').
card_artist('steal artifact'/'2ED', 'Amy Weber').
card_multiverse_id('steal artifact'/'2ED', '726').

card_in_set('stone giant', '2ED').
card_original_type('stone giant'/'2ED', 'Summon — Giant').
card_original_text('stone giant'/'2ED', 'Tap to make one of your own creatures a flying creature until end of turn. Target creature, which must have toughness less than Stone Giant\'s power, is destroyed at end of turn.').
card_image_name('stone giant'/'2ED', 'stone giant').
card_uid('stone giant'/'2ED', '2ED:Stone Giant:stone giant').
card_rarity('stone giant'/'2ED', 'Uncommon').
card_artist('stone giant'/'2ED', 'Dameon Willich').
card_flavor_text('stone giant'/'2ED', 'What goes up, must come down.').
card_multiverse_id('stone giant'/'2ED', '821').

card_in_set('stone rain', '2ED').
card_original_type('stone rain'/'2ED', 'Sorcery').
card_original_text('stone rain'/'2ED', 'Destroys any one land.').
card_image_name('stone rain'/'2ED', 'stone rain').
card_uid('stone rain'/'2ED', '2ED:Stone Rain:stone rain').
card_rarity('stone rain'/'2ED', 'Common').
card_artist('stone rain'/'2ED', 'Daniel Gelon').
card_multiverse_id('stone rain'/'2ED', '822').

card_in_set('stream of life', '2ED').
card_original_type('stream of life'/'2ED', 'Sorcery').
card_original_text('stream of life'/'2ED', 'Target player gains X life.').
card_image_name('stream of life'/'2ED', 'stream of life').
card_uid('stream of life'/'2ED', '2ED:Stream of Life:stream of life').
card_rarity('stream of life'/'2ED', 'Common').
card_artist('stream of life'/'2ED', 'Mark Poole').
card_multiverse_id('stream of life'/'2ED', '770').

card_in_set('sunglasses of urza', '2ED').
card_original_type('sunglasses of urza'/'2ED', 'Continuous Artifact').
card_original_text('sunglasses of urza'/'2ED', 'White mana in your mana pool can be used as either white or red mana.').
card_image_name('sunglasses of urza'/'2ED', 'sunglasses of urza').
card_uid('sunglasses of urza'/'2ED', '2ED:Sunglasses of Urza:sunglasses of urza').
card_rarity('sunglasses of urza'/'2ED', 'Rare').
card_artist('sunglasses of urza'/'2ED', 'Dan Frazier').
card_multiverse_id('sunglasses of urza'/'2ED', '639').

card_in_set('swamp', '2ED').
card_original_type('swamp'/'2ED', 'Land').
card_original_text('swamp'/'2ED', 'Tap to add {B} to your mana pool.').
card_image_name('swamp'/'2ED', 'swamp1').
card_uid('swamp'/'2ED', '2ED:Swamp:swamp1').
card_rarity('swamp'/'2ED', 'Basic Land').
card_artist('swamp'/'2ED', 'Dan Frazier').
card_multiverse_id('swamp'/'2ED', '877').

card_in_set('swamp', '2ED').
card_original_type('swamp'/'2ED', 'Land').
card_original_text('swamp'/'2ED', 'Tap to add {B} to your mana pool.').
card_image_name('swamp'/'2ED', 'swamp2').
card_uid('swamp'/'2ED', '2ED:Swamp:swamp2').
card_rarity('swamp'/'2ED', 'Basic Land').
card_artist('swamp'/'2ED', 'Dan Frazier').
card_multiverse_id('swamp'/'2ED', '876').

card_in_set('swamp', '2ED').
card_original_type('swamp'/'2ED', 'Land').
card_original_text('swamp'/'2ED', 'Tap to add {B} to your mana pool.').
card_image_name('swamp'/'2ED', 'swamp3').
card_uid('swamp'/'2ED', '2ED:Swamp:swamp3').
card_rarity('swamp'/'2ED', 'Basic Land').
card_artist('swamp'/'2ED', 'Dan Frazier').
card_multiverse_id('swamp'/'2ED', '875').

card_in_set('swords to plowshares', '2ED').
card_original_type('swords to plowshares'/'2ED', 'Instant').
card_original_text('swords to plowshares'/'2ED', 'Target creature is removed from game entirely; return to owner\'s deck only when game is over. Creature\'s controller gains life points equal to creature\'s power.').
card_image_name('swords to plowshares'/'2ED', 'swords to plowshares').
card_uid('swords to plowshares'/'2ED', '2ED:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'2ED', 'Uncommon').
card_artist('swords to plowshares'/'2ED', 'Jeff A. Menges').
card_multiverse_id('swords to plowshares'/'2ED', '869').

card_in_set('taiga', '2ED').
card_original_type('taiga'/'2ED', 'Land').
card_original_text('taiga'/'2ED', 'Counts as both forest and mountains and is affected by spells that affect either. Tap to add either {G} or {R} to your mana pool.').
card_image_name('taiga'/'2ED', 'taiga').
card_uid('taiga'/'2ED', '2ED:Taiga:taiga').
card_rarity('taiga'/'2ED', 'Rare').
card_artist('taiga'/'2ED', 'Rob Alexander').
card_multiverse_id('taiga'/'2ED', '883').

card_in_set('terror', '2ED').
card_original_type('terror'/'2ED', 'Instant').
card_original_text('terror'/'2ED', 'Destroys target creature without possibility of regeneration. Does not affect black creatures and artifact creatures.').
card_image_name('terror'/'2ED', 'terror').
card_uid('terror'/'2ED', '2ED:Terror:terror').
card_rarity('terror'/'2ED', 'Common').
card_artist('terror'/'2ED', 'Ron Spencer').
card_multiverse_id('terror'/'2ED', '683').

card_in_set('the hive', '2ED').
card_original_type('the hive'/'2ED', 'Mono Artifact').
card_original_text('the hive'/'2ED', '{5}: Creates one Giant Wasp, a 1/1 flying creature. Represent Wasps with tokens, making sure to indicate when each Wasp is tapped. Wasps can\'t attack during the turn created. Treat Wasps like artifact creatures in every way, except that they are removed from the game entirely if they ever leave play. If the Hive is destroyed, the Wasps must still be killed individually.').
card_image_name('the hive'/'2ED', 'the hive').
card_uid('the hive'/'2ED', '2ED:The Hive:the hive').
card_rarity('the hive'/'2ED', 'Rare').
card_artist('the hive'/'2ED', 'Sandra Everingham').
card_multiverse_id('the hive'/'2ED', '640').

card_in_set('thicket basilisk', '2ED').
card_original_type('thicket basilisk'/'2ED', 'Summon — Basilisk').
card_original_text('thicket basilisk'/'2ED', 'Any non-wall creature blocking Basilisk is destroyed, as is any creature blocked by Basilisk. Creatures destroyed this way deal their damage before dying.').
card_image_name('thicket basilisk'/'2ED', 'thicket basilisk').
card_uid('thicket basilisk'/'2ED', '2ED:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'2ED', 'Uncommon').
card_artist('thicket basilisk'/'2ED', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'2ED', 'Moss-covered statues littered the area, a macabre monument to the Basilisk\'s power.').
card_multiverse_id('thicket basilisk'/'2ED', '771').

card_in_set('thoughtlace', '2ED').
card_original_type('thoughtlace'/'2ED', 'Interrupt').
card_original_text('thoughtlace'/'2ED', 'Changes the color of one card either being played or already in play to blue. Cost to cast, tap, maintain, or use a special ability of target card remains entirely unchanged.').
card_image_name('thoughtlace'/'2ED', 'thoughtlace').
card_uid('thoughtlace'/'2ED', '2ED:Thoughtlace:thoughtlace').
card_rarity('thoughtlace'/'2ED', 'Rare').
card_artist('thoughtlace'/'2ED', 'Mark Poole').
card_multiverse_id('thoughtlace'/'2ED', '727').

card_in_set('throne of bone', '2ED').
card_original_type('throne of bone'/'2ED', 'Poly Artifact').
card_original_text('throne of bone'/'2ED', '{1}: Any black spell cast by any player gives you 1 life.').
card_image_name('throne of bone'/'2ED', 'throne of bone').
card_uid('throne of bone'/'2ED', '2ED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'2ED', 'Uncommon').
card_artist('throne of bone'/'2ED', 'Anson Maddocks').
card_multiverse_id('throne of bone'/'2ED', '641').

card_in_set('timber wolves', '2ED').
card_original_type('timber wolves'/'2ED', 'Summon — Wolves').
card_original_text('timber wolves'/'2ED', 'Bands').
card_image_name('timber wolves'/'2ED', 'timber wolves').
card_uid('timber wolves'/'2ED', '2ED:Timber Wolves:timber wolves').
card_rarity('timber wolves'/'2ED', 'Rare').
card_artist('timber wolves'/'2ED', 'Melissa A. Benson').
card_flavor_text('timber wolves'/'2ED', 'Though many think of Wolves as solitary predators, they are actually extremely social animals. During a hunt they often call to each other, which can be quite unsettling for their prey.').
card_multiverse_id('timber wolves'/'2ED', '772').

card_in_set('time vault', '2ED').
card_original_type('time vault'/'2ED', 'Mono Artifact').
card_original_text('time vault'/'2ED', 'Tap to gain an additional turn after the current one. Time Vault doesn\'t untap normally during untap phase; to untap it, you must skip a turn. Time Vault begins tapped.').
card_image_name('time vault'/'2ED', 'time vault').
card_uid('time vault'/'2ED', '2ED:Time Vault:time vault').
card_rarity('time vault'/'2ED', 'Rare').
card_artist('time vault'/'2ED', 'Mark Tedin').
card_multiverse_id('time vault'/'2ED', '642').

card_in_set('time walk', '2ED').
card_original_type('time walk'/'2ED', 'Sorcery').
card_original_text('time walk'/'2ED', 'Take an extra turn after this one.').
card_image_name('time walk'/'2ED', 'time walk').
card_uid('time walk'/'2ED', '2ED:Time Walk:time walk').
card_rarity('time walk'/'2ED', 'Rare').
card_artist('time walk'/'2ED', 'Amy Weber').
card_multiverse_id('time walk'/'2ED', '728').

card_in_set('timetwister', '2ED').
card_original_type('timetwister'/'2ED', 'Sorcery').
card_original_text('timetwister'/'2ED', 'Set Timetwister aside in a new graveyard pile. Shuffle your hand, library, and graveyard together into a new library and draw a new hand of seven cards, leaving all cards in play where they are; opponent must do the same.').
card_image_name('timetwister'/'2ED', 'timetwister').
card_uid('timetwister'/'2ED', '2ED:Timetwister:timetwister').
card_rarity('timetwister'/'2ED', 'Rare').
card_artist('timetwister'/'2ED', 'Mark Tedin').
card_multiverse_id('timetwister'/'2ED', '729').

card_in_set('tranquility', '2ED').
card_original_type('tranquility'/'2ED', 'Sorcery').
card_original_text('tranquility'/'2ED', 'All enchantments in play must be discarded.').
card_image_name('tranquility'/'2ED', 'tranquility').
card_uid('tranquility'/'2ED', '2ED:Tranquility:tranquility').
card_rarity('tranquility'/'2ED', 'Common').
card_artist('tranquility'/'2ED', 'Douglas Shuler').
card_multiverse_id('tranquility'/'2ED', '773').

card_in_set('tropical island', '2ED').
card_original_type('tropical island'/'2ED', 'Land').
card_original_text('tropical island'/'2ED', 'Counts as both forest and islands and is affected by spells that affect either. Tap to add either {G} or {U} to your mana pool.').
card_image_name('tropical island'/'2ED', 'tropical island').
card_uid('tropical island'/'2ED', '2ED:Tropical Island:tropical island').
card_rarity('tropical island'/'2ED', 'Rare').
card_artist('tropical island'/'2ED', 'Jesper Myrfors').
card_multiverse_id('tropical island'/'2ED', '884').

card_in_set('tsunami', '2ED').
card_original_type('tsunami'/'2ED', 'Sorcery').
card_original_text('tsunami'/'2ED', 'All islands in play are destroyed.').
card_image_name('tsunami'/'2ED', 'tsunami').
card_uid('tsunami'/'2ED', '2ED:Tsunami:tsunami').
card_rarity('tsunami'/'2ED', 'Uncommon').
card_artist('tsunami'/'2ED', 'Richard Thomas').
card_multiverse_id('tsunami'/'2ED', '774').

card_in_set('tundra', '2ED').
card_original_type('tundra'/'2ED', 'Land').
card_original_text('tundra'/'2ED', 'Counts as both islands and plains and is affected by spells that affect either. Tap to add either {U} or {W} to your mana pool.').
card_image_name('tundra'/'2ED', 'tundra').
card_uid('tundra'/'2ED', '2ED:Tundra:tundra').
card_rarity('tundra'/'2ED', 'Rare').
card_artist('tundra'/'2ED', 'Jesper Myrfors').
card_multiverse_id('tundra'/'2ED', '885').

card_in_set('tunnel', '2ED').
card_original_type('tunnel'/'2ED', 'Instant').
card_original_text('tunnel'/'2ED', 'Destroys 1 wall. Target wall cannot be regenerated.').
card_image_name('tunnel'/'2ED', 'tunnel').
card_uid('tunnel'/'2ED', '2ED:Tunnel:tunnel').
card_rarity('tunnel'/'2ED', 'Uncommon').
card_artist('tunnel'/'2ED', 'Dan Frazier').
card_multiverse_id('tunnel'/'2ED', '823').

card_in_set('twiddle', '2ED').
card_original_type('twiddle'/'2ED', 'Instant').
card_original_text('twiddle'/'2ED', 'Caster may tap or untap any one land, creature, or artifact in play. No effects are generated by the target card.').
card_image_name('twiddle'/'2ED', 'twiddle').
card_uid('twiddle'/'2ED', '2ED:Twiddle:twiddle').
card_rarity('twiddle'/'2ED', 'Common').
card_artist('twiddle'/'2ED', 'Rob Alexander').
card_multiverse_id('twiddle'/'2ED', '730').

card_in_set('two-headed giant of foriys', '2ED').
card_original_type('two-headed giant of foriys'/'2ED', 'Summon — Giant').
card_original_text('two-headed giant of foriys'/'2ED', 'Trample\nMay block two attacking creatures; divide damage between them however controller likes.').
card_image_name('two-headed giant of foriys'/'2ED', 'two-headed giant of foriys').
card_uid('two-headed giant of foriys'/'2ED', '2ED:Two-Headed Giant of Foriys:two-headed giant of foriys').
card_rarity('two-headed giant of foriys'/'2ED', 'Rare').
card_artist('two-headed giant of foriys'/'2ED', 'Anson Maddocks').
card_flavor_text('two-headed giant of foriys'/'2ED', 'None know if this Giant is the result of aberrant magics, Siamese twins, or a mentalist\'s schizophrenia.').
card_multiverse_id('two-headed giant of foriys'/'2ED', '824').

card_in_set('underground sea', '2ED').
card_original_type('underground sea'/'2ED', 'Land').
card_original_text('underground sea'/'2ED', 'Counts as both swamp and islands and is affected by spells that affect either. Tap to add either {B} or {U} to your mana pool.').
card_image_name('underground sea'/'2ED', 'underground sea').
card_uid('underground sea'/'2ED', '2ED:Underground Sea:underground sea').
card_rarity('underground sea'/'2ED', 'Rare').
card_artist('underground sea'/'2ED', 'Rob Alexander').
card_multiverse_id('underground sea'/'2ED', '886').

card_in_set('unholy strength', '2ED').
card_original_type('unholy strength'/'2ED', 'Enchant Creature').
card_original_text('unholy strength'/'2ED', 'Target creature gains +2/+1.').
card_image_name('unholy strength'/'2ED', 'unholy strength').
card_uid('unholy strength'/'2ED', '2ED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'2ED', 'Common').
card_artist('unholy strength'/'2ED', 'Douglas Shuler').
card_multiverse_id('unholy strength'/'2ED', '684').

card_in_set('unsummon', '2ED').
card_original_type('unsummon'/'2ED', 'Instant').
card_original_text('unsummon'/'2ED', 'Return creature to owner\'s hand; enchantments on creature are discarded. Unsummon cannot be played during the damage-dealing phase of an attack.').
card_image_name('unsummon'/'2ED', 'unsummon').
card_uid('unsummon'/'2ED', '2ED:Unsummon:unsummon').
card_rarity('unsummon'/'2ED', 'Common').
card_artist('unsummon'/'2ED', 'Douglas Shuler').
card_multiverse_id('unsummon'/'2ED', '731').

card_in_set('uthden troll', '2ED').
card_original_type('uthden troll'/'2ED', 'Summon — Troll').
card_original_text('uthden troll'/'2ED', '{R} Regenerates').
card_image_name('uthden troll'/'2ED', 'uthden troll').
card_uid('uthden troll'/'2ED', '2ED:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'2ED', 'Uncommon').
card_artist('uthden troll'/'2ED', 'Douglas Shuler').
card_flavor_text('uthden troll'/'2ED', '\"Oi oi oi, me gotta hurt in \'ere,\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh \'n da\nhurt\'ll disappear.\"\n—Traditional').
card_multiverse_id('uthden troll'/'2ED', '825').

card_in_set('verduran enchantress', '2ED').
card_original_type('verduran enchantress'/'2ED', 'Summon — Enchantress').
card_original_text('verduran enchantress'/'2ED', 'While Enchantress is in play, you may immediately draw a card from your library each time you cast an enchantment.').
card_image_name('verduran enchantress'/'2ED', 'verduran enchantress').
card_uid('verduran enchantress'/'2ED', '2ED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'2ED', 'Rare').
card_artist('verduran enchantress'/'2ED', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'2ED', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').
card_multiverse_id('verduran enchantress'/'2ED', '775').

card_in_set('vesuvan doppelganger', '2ED').
card_original_type('vesuvan doppelganger'/'2ED', 'Summon — Doppelganger').
card_original_text('vesuvan doppelganger'/'2ED', 'Upon summoning, Doppelganger acquires all normal characteristics (except color) of any one creature in play on either side; any enchantments on the original creature are not copied. During controller\'s upkeep, Doppelganger may take on the characteristics of a different creature in play instead. Doppleganger may continue to copy a creature even after that creature leaves play, but if it switches it won\'t be able to switch back.').
card_image_name('vesuvan doppelganger'/'2ED', 'vesuvan doppelganger').
card_uid('vesuvan doppelganger'/'2ED', '2ED:Vesuvan Doppelganger:vesuvan doppelganger').
card_rarity('vesuvan doppelganger'/'2ED', 'Rare').
card_artist('vesuvan doppelganger'/'2ED', 'Quinton Hoover').
card_multiverse_id('vesuvan doppelganger'/'2ED', '732').

card_in_set('veteran bodyguard', '2ED').
card_original_type('veteran bodyguard'/'2ED', 'Summon — Bodyguard').
card_original_text('veteran bodyguard'/'2ED', 'Unless Bodyguard is tapped, any damage done to you by unblocked creatures is done instead to Bodyguard. You may not take this damage yourself, though you can prevent it if possible.').
card_image_name('veteran bodyguard'/'2ED', 'veteran bodyguard').
card_uid('veteran bodyguard'/'2ED', '2ED:Veteran Bodyguard:veteran bodyguard').
card_rarity('veteran bodyguard'/'2ED', 'Rare').
card_artist('veteran bodyguard'/'2ED', 'Douglas Shuler').
card_flavor_text('veteran bodyguard'/'2ED', 'Good bodyguards are hard to find, mainly because they don\'t live long.').
card_multiverse_id('veteran bodyguard'/'2ED', '870').

card_in_set('volcanic eruption', '2ED').
card_original_type('volcanic eruption'/'2ED', 'Sorcery').
card_original_text('volcanic eruption'/'2ED', 'Destroys X mountains of your choice, and does X damage to each player and each creature in play.').
card_image_name('volcanic eruption'/'2ED', 'volcanic eruption').
card_uid('volcanic eruption'/'2ED', '2ED:Volcanic Eruption:volcanic eruption').
card_rarity('volcanic eruption'/'2ED', 'Rare').
card_artist('volcanic eruption'/'2ED', 'Douglas Shuler').
card_multiverse_id('volcanic eruption'/'2ED', '733').

card_in_set('volcanic island', '2ED').
card_original_type('volcanic island'/'2ED', 'Land').
card_original_text('volcanic island'/'2ED', 'Counts as both islands and mountains and is affected by spells that affect either. Tap to add either {U} or {R} to your mana pool.').
card_image_name('volcanic island'/'2ED', 'volcanic island').
card_uid('volcanic island'/'2ED', '2ED:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'2ED', 'Rare').
card_artist('volcanic island'/'2ED', 'Brian Snõddy').
card_multiverse_id('volcanic island'/'2ED', '887').

card_in_set('wall of air', '2ED').
card_original_type('wall of air'/'2ED', 'Summon — Wall').
card_original_text('wall of air'/'2ED', 'Flying').
card_image_name('wall of air'/'2ED', 'wall of air').
card_uid('wall of air'/'2ED', '2ED:Wall of Air:wall of air').
card_rarity('wall of air'/'2ED', 'Uncommon').
card_artist('wall of air'/'2ED', 'Richard Thomas').
card_flavor_text('wall of air'/'2ED', '\"This ‘standing windstorm\' can hold us off indefinitely? Ridiculous!\" Saying nothing, she put a pinch of salt on the table. With a bang she clapped her hands, and the salt disappeared, blown away.').
card_multiverse_id('wall of air'/'2ED', '734').

card_in_set('wall of bone', '2ED').
card_original_type('wall of bone'/'2ED', 'Summon — Wall').
card_original_text('wall of bone'/'2ED', '{B} Regenerates').
card_image_name('wall of bone'/'2ED', 'wall of bone').
card_uid('wall of bone'/'2ED', '2ED:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'2ED', 'Uncommon').
card_artist('wall of bone'/'2ED', 'Anson Maddocks').
card_flavor_text('wall of bone'/'2ED', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').
card_multiverse_id('wall of bone'/'2ED', '685').

card_in_set('wall of brambles', '2ED').
card_original_type('wall of brambles'/'2ED', 'Summon — Wall').
card_original_text('wall of brambles'/'2ED', '{G} Regenerates').
card_image_name('wall of brambles'/'2ED', 'wall of brambles').
card_uid('wall of brambles'/'2ED', '2ED:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'2ED', 'Uncommon').
card_artist('wall of brambles'/'2ED', 'Anson Maddocks').
card_flavor_text('wall of brambles'/'2ED', '\"What else, when chaos draws all forces inward to shape a single leaf.\"\n —Conrad Aiken').
card_multiverse_id('wall of brambles'/'2ED', '776').

card_in_set('wall of fire', '2ED').
card_original_type('wall of fire'/'2ED', 'Summon — Wall').
card_original_text('wall of fire'/'2ED', '{R} +1/+0 until end of turn.').
card_image_name('wall of fire'/'2ED', 'wall of fire').
card_uid('wall of fire'/'2ED', '2ED:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'2ED', 'Uncommon').
card_artist('wall of fire'/'2ED', 'Richard Thomas').
card_flavor_text('wall of fire'/'2ED', 'Conjured from the bowels of hell, the fiery wall forms an impassable barrier, searing the soul of any creature attempting to pass through its terrible bursts of flame.').
card_multiverse_id('wall of fire'/'2ED', '826').

card_in_set('wall of ice', '2ED').
card_original_type('wall of ice'/'2ED', 'Summon — Wall').
card_original_text('wall of ice'/'2ED', '').
card_image_name('wall of ice'/'2ED', 'wall of ice').
card_uid('wall of ice'/'2ED', '2ED:Wall of Ice:wall of ice').
card_rarity('wall of ice'/'2ED', 'Uncommon').
card_artist('wall of ice'/'2ED', 'Richard Thomas').
card_flavor_text('wall of ice'/'2ED', '\"And through the drifts the snowy cliffs/ Did send a dismal sheen:/ Nor shapes of men nor beasts we ken—/ The ice was all between.\"/—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('wall of ice'/'2ED', '777').

card_in_set('wall of stone', '2ED').
card_original_type('wall of stone'/'2ED', 'Summon — Wall').
card_original_text('wall of stone'/'2ED', '').
card_image_name('wall of stone'/'2ED', 'wall of stone').
card_uid('wall of stone'/'2ED', '2ED:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'2ED', 'Uncommon').
card_artist('wall of stone'/'2ED', 'Dan Frazier').
card_flavor_text('wall of stone'/'2ED', 'The Earth herself lends her strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').
card_multiverse_id('wall of stone'/'2ED', '827').

card_in_set('wall of swords', '2ED').
card_original_type('wall of swords'/'2ED', 'Summon — Wall').
card_original_text('wall of swords'/'2ED', 'Flying').
card_image_name('wall of swords'/'2ED', 'wall of swords').
card_uid('wall of swords'/'2ED', '2ED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'2ED', 'Uncommon').
card_artist('wall of swords'/'2ED', 'Mark Tedin').
card_flavor_text('wall of swords'/'2ED', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').
card_multiverse_id('wall of swords'/'2ED', '871').

card_in_set('wall of water', '2ED').
card_original_type('wall of water'/'2ED', 'Summon — Wall').
card_original_text('wall of water'/'2ED', '{U} +1/+0 until end of turn.').
card_image_name('wall of water'/'2ED', 'wall of water').
card_uid('wall of water'/'2ED', '2ED:Wall of Water:wall of water').
card_rarity('wall of water'/'2ED', 'Uncommon').
card_artist('wall of water'/'2ED', 'Richard Thomas').
card_flavor_text('wall of water'/'2ED', 'A deafening roar arose as the fury of an enormous vertical river supplanted our serenity. Eddies turned into whirling geysers, leveling everything in their path.').
card_multiverse_id('wall of water'/'2ED', '735').

card_in_set('wall of wood', '2ED').
card_original_type('wall of wood'/'2ED', 'Summon — Wall').
card_original_text('wall of wood'/'2ED', '').
card_image_name('wall of wood'/'2ED', 'wall of wood').
card_uid('wall of wood'/'2ED', '2ED:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'2ED', 'Common').
card_artist('wall of wood'/'2ED', 'Mark Tedin').
card_flavor_text('wall of wood'/'2ED', 'Everybody knows that to ward off trouble, you knock on wood. But usually it\'s better to make a wall out of the wood and let trouble do the knocking.').
card_multiverse_id('wall of wood'/'2ED', '778').

card_in_set('wanderlust', '2ED').
card_original_type('wanderlust'/'2ED', 'Enchant Creature').
card_original_text('wanderlust'/'2ED', 'Wanderlust does 1 damage to target creature\'s controller during upkeep.').
card_image_name('wanderlust'/'2ED', 'wanderlust').
card_uid('wanderlust'/'2ED', '2ED:Wanderlust:wanderlust').
card_rarity('wanderlust'/'2ED', 'Uncommon').
card_artist('wanderlust'/'2ED', 'Cornelius Brudi').
card_multiverse_id('wanderlust'/'2ED', '779').

card_in_set('war mammoth', '2ED').
card_original_type('war mammoth'/'2ED', 'Summon — Mammoth').
card_original_text('war mammoth'/'2ED', 'Trample').
card_image_name('war mammoth'/'2ED', 'war mammoth').
card_uid('war mammoth'/'2ED', '2ED:War Mammoth:war mammoth').
card_rarity('war mammoth'/'2ED', 'Common').
card_artist('war mammoth'/'2ED', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'2ED', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').
card_multiverse_id('war mammoth'/'2ED', '780').

card_in_set('warp artifact', '2ED').
card_original_type('warp artifact'/'2ED', 'Enchant Artifact').
card_original_text('warp artifact'/'2ED', 'Warp Artifact does 1 damage to target artifact\'s controller at start of each turn.').
card_image_name('warp artifact'/'2ED', 'warp artifact').
card_uid('warp artifact'/'2ED', '2ED:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'2ED', 'Rare').
card_artist('warp artifact'/'2ED', 'Amy Weber').
card_multiverse_id('warp artifact'/'2ED', '686').

card_in_set('water elemental', '2ED').
card_original_type('water elemental'/'2ED', 'Summon — Elemental').
card_original_text('water elemental'/'2ED', '').
card_image_name('water elemental'/'2ED', 'water elemental').
card_uid('water elemental'/'2ED', '2ED:Water Elemental:water elemental').
card_rarity('water elemental'/'2ED', 'Uncommon').
card_artist('water elemental'/'2ED', 'Jeff A. Menges').
card_flavor_text('water elemental'/'2ED', 'Unpredictable as the sea itself, Water Elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').
card_multiverse_id('water elemental'/'2ED', '736').

card_in_set('weakness', '2ED').
card_original_type('weakness'/'2ED', 'Enchant Creature').
card_original_text('weakness'/'2ED', 'Target creature loses -2/-1; if this drops the creature\'s toughness below 1, it is dead.').
card_image_name('weakness'/'2ED', 'weakness').
card_uid('weakness'/'2ED', '2ED:Weakness:weakness').
card_rarity('weakness'/'2ED', 'Common').
card_artist('weakness'/'2ED', 'Douglas Shuler').
card_multiverse_id('weakness'/'2ED', '687').

card_in_set('web', '2ED').
card_original_type('web'/'2ED', 'Enchant Creature').
card_original_text('web'/'2ED', 'Target creature gains +0/+2 and can now block flying creatures, though it does not gain the power to fly.').
card_image_name('web'/'2ED', 'web').
card_uid('web'/'2ED', '2ED:Web:web').
card_rarity('web'/'2ED', 'Rare').
card_artist('web'/'2ED', 'Rob Alexander').
card_multiverse_id('web'/'2ED', '781').

card_in_set('wheel of fortune', '2ED').
card_original_type('wheel of fortune'/'2ED', 'Sorcery').
card_original_text('wheel of fortune'/'2ED', 'Both players must discard their hands and draw seven new cards.').
card_image_name('wheel of fortune'/'2ED', 'wheel of fortune').
card_uid('wheel of fortune'/'2ED', '2ED:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'2ED', 'Rare').
card_artist('wheel of fortune'/'2ED', 'Daniel Gelon').
card_multiverse_id('wheel of fortune'/'2ED', '828').

card_in_set('white knight', '2ED').
card_original_type('white knight'/'2ED', 'Summon — Knight').
card_original_text('white knight'/'2ED', 'Protection from black, first strike').
card_image_name('white knight'/'2ED', 'white knight').
card_uid('white knight'/'2ED', '2ED:White Knight:white knight').
card_rarity('white knight'/'2ED', 'Uncommon').
card_artist('white knight'/'2ED', 'Daniel Gelon').
card_flavor_text('white knight'/'2ED', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').
card_multiverse_id('white knight'/'2ED', '872').

card_in_set('white ward', '2ED').
card_original_type('white ward'/'2ED', 'Enchant Creature').
card_original_text('white ward'/'2ED', 'Target creature gains protection from white.').
card_image_name('white ward'/'2ED', 'white ward').
card_uid('white ward'/'2ED', '2ED:White Ward:white ward').
card_rarity('white ward'/'2ED', 'Uncommon').
card_artist('white ward'/'2ED', 'Dan Frazier').
card_multiverse_id('white ward'/'2ED', '873').

card_in_set('wild growth', '2ED').
card_original_type('wild growth'/'2ED', 'Enchant Land').
card_original_text('wild growth'/'2ED', 'When tapped, target land provides 1 green mana in addition to the mana it normally provides.').
card_image_name('wild growth'/'2ED', 'wild growth').
card_uid('wild growth'/'2ED', '2ED:Wild Growth:wild growth').
card_rarity('wild growth'/'2ED', 'Common').
card_artist('wild growth'/'2ED', 'Mark Poole').
card_multiverse_id('wild growth'/'2ED', '782').

card_in_set('will-o\'-the-wisp', '2ED').
card_original_type('will-o\'-the-wisp'/'2ED', 'Summon — Will-O\'-The-Wisp').
card_original_text('will-o\'-the-wisp'/'2ED', 'Flying; {B} Regenerates').
card_image_name('will-o\'-the-wisp'/'2ED', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'2ED', '2ED:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'2ED', 'Rare').
card_artist('will-o\'-the-wisp'/'2ED', 'Jesper Myrfors').
card_flavor_text('will-o\'-the-wisp'/'2ED', '\"About, about in reel and rout\nThe death-fires danced at night;\nThe water, like a witch\'s oils,\nBurnt green, and blue and white.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').
card_multiverse_id('will-o\'-the-wisp'/'2ED', '688').

card_in_set('winter orb', '2ED').
card_original_type('winter orb'/'2ED', 'Continuous Artifact').
card_original_text('winter orb'/'2ED', 'Players can untap only one land each during untap phase. Creatures and artifacts are untapped as normal.').
card_image_name('winter orb'/'2ED', 'winter orb').
card_uid('winter orb'/'2ED', '2ED:Winter Orb:winter orb').
card_rarity('winter orb'/'2ED', 'Rare').
card_artist('winter orb'/'2ED', 'Mark Tedin').
card_multiverse_id('winter orb'/'2ED', '643').

card_in_set('wooden sphere', '2ED').
card_original_type('wooden sphere'/'2ED', 'Poly Artifact').
card_original_text('wooden sphere'/'2ED', '{1}: Any green spell cast by any player gives you 1 life.').
card_image_name('wooden sphere'/'2ED', 'wooden sphere').
card_uid('wooden sphere'/'2ED', '2ED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'2ED', 'Uncommon').
card_artist('wooden sphere'/'2ED', 'Mark Tedin').
card_multiverse_id('wooden sphere'/'2ED', '644').

card_in_set('word of command', '2ED').
card_original_type('word of command'/'2ED', 'Instant').
card_original_text('word of command'/'2ED', 'You may look at opponent\'s hand and choose any card opponent can legally play using mana from his or her mana pool or lands. Opponent must play this card immediately; you make all the decisions it calls for. This spell may not be countered after you have looked at opponent\'s hand.').
card_image_name('word of command'/'2ED', 'word of command').
card_uid('word of command'/'2ED', '2ED:Word of Command:word of command').
card_rarity('word of command'/'2ED', 'Rare').
card_artist('word of command'/'2ED', 'Jesper Myrfors').
card_multiverse_id('word of command'/'2ED', '689').

card_in_set('wrath of god', '2ED').
card_original_type('wrath of god'/'2ED', 'Sorcery').
card_original_text('wrath of god'/'2ED', 'All creatures in play are destroyed and cannot regenerate.').
card_image_name('wrath of god'/'2ED', 'wrath of god').
card_uid('wrath of god'/'2ED', '2ED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'2ED', 'Rare').
card_artist('wrath of god'/'2ED', 'Quinton Hoover').
card_multiverse_id('wrath of god'/'2ED', '874').

card_in_set('zombie master', '2ED').
card_original_type('zombie master'/'2ED', 'Summon — Lord').
card_original_text('zombie master'/'2ED', 'All zombies in play gain swampwalk and \"{B} Regenerates\" for as long as this card remains in play.').
card_image_name('zombie master'/'2ED', 'zombie master').
card_uid('zombie master'/'2ED', '2ED:Zombie Master:zombie master').
card_rarity('zombie master'/'2ED', 'Rare').
card_artist('zombie master'/'2ED', 'Jeff A. Menges').
card_flavor_text('zombie master'/'2ED', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
card_multiverse_id('zombie master'/'2ED', '690').
