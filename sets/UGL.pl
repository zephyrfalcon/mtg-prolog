% Unglued

set('UGL').
set_name('UGL', 'Unglued').
set_release_date('UGL', '1998-08-11').
set_border('UGL', 'silver').
set_type('UGL', 'un').

card_in_set('ashnod\'s coupon', 'UGL').
card_original_type('ashnod\'s coupon'/'UGL', 'Artifact').
card_original_text('ashnod\'s coupon'/'UGL', '{T}, Sacrifice Ashnod\'s Coupon: Target player gets you target drink.\nErrata: You pay any costs for the drink.').
card_image_name('ashnod\'s coupon'/'UGL', 'ashnod\'s coupon').
card_uid('ashnod\'s coupon'/'UGL', 'UGL:Ashnod\'s Coupon:ashnod\'s coupon').
card_rarity('ashnod\'s coupon'/'UGL', 'Rare').
card_artist('ashnod\'s coupon'/'UGL', 'Edward P. Beard, Jr.').
card_number('ashnod\'s coupon'/'UGL', '69').
card_flavor_text('ashnod\'s coupon'/'UGL', 'Limited time offer. Void where prohibited. Limit one per purchase. Valid only in participating duels. This coupon is nontransferable and invalid if shattered, crumbled, detonated, pillaged, or otherwise disenchanted. Cash value less than 1/20 of a cent. Offer not valid in Quebec, Rhode Island, or where prohibited by law or the DCI.').
card_multiverse_id('ashnod\'s coupon'/'UGL', '9769').

card_in_set('b.f.m. (big furry monster)', 'UGL').
card_original_type('b.f.m. (big furry monster)'/'UGL', 'Summon — The Biggest, Baddest, Nastiest,').
card_original_text('b.f.m. (big furry monster)'/'UGL', 'You must play both B.F.M. cards to put\nleaves play, sacrifice the other.\nB.F.M. can be blocked only by three or').
card_first_print('b.f.m. (big furry monster)', 'UGL').
card_image_name('b.f.m. (big furry monster)'/'UGL', 'b.f.m. 1').
card_uid('b.f.m. (big furry monster)'/'UGL', 'UGL:B.F.M. (Big Furry Monster):b.f.m. 1').
card_rarity('b.f.m. (big furry monster)'/'UGL', 'Rare').
card_artist('b.f.m. (big furry monster)'/'UGL', 'Douglas Shuler').
card_number('b.f.m. (big furry monster)'/'UGL', '28').
card_flavor_text('b.f.m. (big furry monster)'/'UGL', '\"It was big. Really, really big. No, bigger\nNo, more. Look, we\'re talking krakens and').
card_multiverse_id('b.f.m. (big furry monster)'/'UGL', '9780').

card_in_set('b.f.m. (big furry monster)', 'UGL').
card_original_type('b.f.m. (big furry monster)'/'UGL', 'Scariest Creature You\'ll Ever See').
card_original_text('b.f.m. (big furry monster)'/'UGL', 'B.F.M. into play. If either B.F.M. card\nmore creatures.').
card_image_name('b.f.m. (big furry monster)'/'UGL', 'b.f.m. 2').
card_uid('b.f.m. (big furry monster)'/'UGL', 'UGL:B.F.M. (Big Furry Monster):b.f.m. 2').
card_rarity('b.f.m. (big furry monster)'/'UGL', 'Rare').
card_artist('b.f.m. (big furry monster)'/'UGL', 'Douglas Shuler').
card_number('b.f.m. (big furry monster)'/'UGL', '29').
card_flavor_text('b.f.m. (big furry monster)'/'UGL', 'than that. Even bigger. Keep going. More.\ndreadnoughts for jewelry. It was big\"\n—Arna Kennerüd, skyknight').
card_multiverse_id('b.f.m. (big furry monster)'/'UGL', '9844').

card_in_set('blacker lotus', 'UGL').
card_original_type('blacker lotus'/'UGL', 'Artifact').
card_original_text('blacker lotus'/'UGL', '{T}: Tear Blacker Lotus into pieces. Add four mana of any one color to your mana pool. Play this ability as a mana source. Remove the pieces from the game afterwards.').
card_first_print('blacker lotus', 'UGL').
card_image_name('blacker lotus'/'UGL', 'blacker lotus').
card_uid('blacker lotus'/'UGL', 'UGL:Blacker Lotus:blacker lotus').
card_rarity('blacker lotus'/'UGL', 'Rare').
card_artist('blacker lotus'/'UGL', 'Christopher Rush').
card_number('blacker lotus'/'UGL', '70').
card_multiverse_id('blacker lotus'/'UGL', '9764').

card_in_set('bronze calendar', 'UGL').
card_original_type('bronze calendar'/'UGL', 'Artifact').
card_original_text('bronze calendar'/'UGL', 'Your spells cost {1} less to play as long as you speak in a voice other than your normal voice.\nIf you speak in your normal voice, sacrifice Bronze Calendar.').
card_first_print('bronze calendar', 'UGL').
card_image_name('bronze calendar'/'UGL', 'bronze calendar').
card_uid('bronze calendar'/'UGL', 'UGL:Bronze Calendar:bronze calendar').
card_rarity('bronze calendar'/'UGL', 'Uncommon').
card_artist('bronze calendar'/'UGL', 'Melissa A. Benson').
card_number('bronze calendar'/'UGL', '71').
card_flavor_text('bronze calendar'/'UGL', 'Every page holds a month, every date a numeral.').
card_multiverse_id('bronze calendar'/'UGL', '9730').

card_in_set('bureaucracy', 'UGL').
card_original_type('bureaucracy'/'UGL', 'Enchantment').
card_original_text('bureaucracy'/'UGL', 'Pursuant to subsection 3.1(4) of Richard\'s Rules of Order, during the upkeep of each participant in this game of the Magic: The Gathering® trading card game (hereafter known as \"PLAYER\"), that PLAYER performs all actions in the sequence of previously added actions (hereafter known as \"ACTION QUEUE\"), in the order those actions were added, then adds another action to the end of the ACTION QUEUE. All actions must be simple physical or verbal actions that a player can perform while sitting in a chair, without jeopardizing the health and security of said PLAYER.\nIf any PLAYER does not perform all the prescribed actions in the correct order, sacrifice Bureaucracy and said PLAYER discards his or her complement of cards in hand (hereafter known as \"HAND\").').
card_first_print('bureaucracy', 'UGL').
card_image_name('bureaucracy'/'UGL', 'bureaucracy').
card_uid('bureaucracy'/'UGL', 'UGL:Bureaucracy:bureaucracy').
card_rarity('bureaucracy'/'UGL', 'Rare').
card_artist('bureaucracy'/'UGL', 'Mark Zug').
card_number('bureaucracy'/'UGL', '14').
card_multiverse_id('bureaucracy'/'UGL', '9778').

card_in_set('burning cinder fury of crimson chaos fire', 'UGL').
card_original_type('burning cinder fury of crimson chaos fire'/'UGL', 'Enchantment').
card_original_text('burning cinder fury of crimson chaos fire'/'UGL', 'Whenever any player taps a card, that player gives control of that card to an opponent at end of turn.\nIf a player does not tap any nonland cards during his or her turn, Burning Cinder Fury of Crimson Chaos Fire deals 3 damage to that player at end of turn.').
card_first_print('burning cinder fury of crimson chaos fire', 'UGL').
card_image_name('burning cinder fury of crimson chaos fire'/'UGL', 'burning cinder fury of crimson chaos fire').
card_uid('burning cinder fury of crimson chaos fire'/'UGL', 'UGL:Burning Cinder Fury of Crimson Chaos Fire:burning cinder fury of crimson chaos fire').
card_rarity('burning cinder fury of crimson chaos fire'/'UGL', 'Rare').
card_artist('burning cinder fury of crimson chaos fire'/'UGL', 'Richard Kane Ferguson').
card_number('burning cinder fury of crimson chaos fire'/'UGL', '40').
card_multiverse_id('burning cinder fury of crimson chaos fire'/'UGL', '9783').

card_in_set('cardboard carapace', 'UGL').
card_original_type('cardboard carapace'/'UGL', 'Enchant Creature').
card_original_text('cardboard carapace'/'UGL', 'For each other Cardboard Carapace card you have with you, enchanted creature gets +1/+1.\nErrata: This does not count any Cardboard Carapace cards in play that you control or in your graveyard, hand, or library.').
card_first_print('cardboard carapace', 'UGL').
card_image_name('cardboard carapace'/'UGL', 'cardboard carapace').
card_uid('cardboard carapace'/'UGL', 'UGL:Cardboard Carapace:cardboard carapace').
card_rarity('cardboard carapace'/'UGL', 'Rare').
card_artist('cardboard carapace'/'UGL', 'Douglas Shuler').
card_number('cardboard carapace'/'UGL', '54').
card_flavor_text('cardboard carapace'/'UGL', '. . .  that exciting  . . .  about  . . .').
card_multiverse_id('cardboard carapace'/'UGL', '9762').

card_in_set('censorship', 'UGL').
card_original_type('censorship'/'UGL', 'Enchantment').
card_original_text('censorship'/'UGL', 'When Censorship comes into play, choose a CENSORED word.\nWhenever any CENSORED player says the chosen CENSORED word, Censorship deals 2 CENSORED damage to him or her.').
card_first_print('censorship', 'UGL').
card_image_name('censorship'/'UGL', 'censorship').
card_uid('censorship'/'UGL', 'UGL:Censorship:censorship').
card_rarity('censorship'/'UGL', 'Uncommon').
card_artist('censorship'/'UGL', 'Kaja Foglio').
card_number('censorship'/'UGL', '15').
card_flavor_text('censorship'/'UGL', 'Editor\'s note: There were no suitable flavor text submissions for this card.').
card_multiverse_id('censorship'/'UGL', '9747').

card_in_set('chaos confetti', 'UGL').
card_original_type('chaos confetti'/'UGL', 'Artifact').
card_original_text('chaos confetti'/'UGL', '{4}, {T}: Tear Chaos Confetti into pieces. Throw the pieces onto the playing area from a distance of at least five feet. Destroy each card in play that a piece touches. Remove the pieces from the game afterwards.').
card_first_print('chaos confetti', 'UGL').
card_image_name('chaos confetti'/'UGL', 'chaos confetti').
card_uid('chaos confetti'/'UGL', 'UGL:Chaos Confetti:chaos confetti').
card_rarity('chaos confetti'/'UGL', 'Common').
card_artist('chaos confetti'/'UGL', 'Mark Tedin').
card_number('chaos confetti'/'UGL', '72').
card_flavor_text('chaos confetti'/'UGL', 'And you thought that was just an urban legend.').
card_multiverse_id('chaos confetti'/'UGL', '5712').

card_in_set('charm school', 'UGL').
card_original_type('charm school'/'UGL', 'Enchant Player').
card_original_text('charm school'/'UGL', 'When Charm School comes into play, choose a color and balance Charm School on your head.\nPrevent all damage to you of the chosen color.\nIf Charm School falls off your head, sacrifice Charm School.').
card_first_print('charm school', 'UGL').
card_image_name('charm school'/'UGL', 'charm school').
card_uid('charm school'/'UGL', 'UGL:Charm School:charm school').
card_rarity('charm school'/'UGL', 'Uncommon').
card_artist('charm school'/'UGL', 'Kaja Foglio').
card_number('charm school'/'UGL', '1').
card_multiverse_id('charm school'/'UGL', '9741').

card_in_set('checks and balances', 'UGL').
card_original_type('checks and balances'/'UGL', 'Enchantment').
card_original_text('checks and balances'/'UGL', 'Whenever any spell is played, counter that spell if each player, other than the caster and his or her teammates, agrees to choose and discard a card. Those players must discard those cards after agreeing.\nChecks and Balances may be played only in a game with three or more players.').
card_first_print('checks and balances', 'UGL').
card_image_name('checks and balances'/'UGL', 'checks and balances').
card_uid('checks and balances'/'UGL', 'UGL:Checks and Balances:checks and balances').
card_rarity('checks and balances'/'UGL', 'Uncommon').
card_artist('checks and balances'/'UGL', 'David A. Cherry').
card_number('checks and balances'/'UGL', '16').
card_multiverse_id('checks and balances'/'UGL', '9746').

card_in_set('chicken à la king', 'UGL').
card_original_type('chicken à la king'/'UGL', 'Summon — Chicken').
card_original_text('chicken à la king'/'UGL', 'Whenever a 6 is rolled on a six-sided die, put a +1/+1 counter on each Chicken in play. (You may roll dice only when a card instructs you to.)\nTap a Chicken you control: Roll a six-sided die.').
card_first_print('chicken à la king', 'UGL').
card_image_name('chicken à la king'/'UGL', 'chicken a la king').
card_uid('chicken à la king'/'UGL', 'UGL:Chicken à la King:chicken a la king').
card_rarity('chicken à la king'/'UGL', 'Rare').
card_artist('chicken à la king'/'UGL', 'Mark Zug').
card_number('chicken à la king'/'UGL', '17').
card_flavor_text('chicken à la king'/'UGL', 'During the Chicken Revolution, the king managed to keep his head while the others—well, just ran around.').
card_multiverse_id('chicken à la king'/'UGL', '9749').

card_in_set('chicken egg', 'UGL').
card_original_type('chicken egg'/'UGL', 'Summon — Egg').
card_original_text('chicken egg'/'UGL', 'During your upkeep, roll a six-sided die. On a 6, sacrifice Chicken Egg and put a Giant Chicken token into play. Treat this token as a 4/4 red creature that counts as a Chicken.').
card_first_print('chicken egg', 'UGL').
card_image_name('chicken egg'/'UGL', 'chicken egg').
card_uid('chicken egg'/'UGL', 'UGL:Chicken Egg:chicken egg').
card_rarity('chicken egg'/'UGL', 'Common').
card_artist('chicken egg'/'UGL', 'Christopher Rush').
card_number('chicken egg'/'UGL', '41').
card_flavor_text('chicken egg'/'UGL', '\"That\'s a lotta nuggets.\"\n—Jaya Ballard, task mage').
card_multiverse_id('chicken egg'/'UGL', '9667').

card_in_set('clam session', 'UGL').
card_original_type('clam session'/'UGL', 'Summon — Clamfolk').
card_original_text('clam session'/'UGL', 'When Clam Session comes into play, choose a word. \nDuring your upkeep, sing at least six words of a song, one of which must be the chosen word, or sacrifice Clam Session. You cannot repeat a song.').
card_first_print('clam session', 'UGL').
card_image_name('clam session'/'UGL', 'clam session').
card_uid('clam session'/'UGL', 'UGL:Clam Session:clam session').
card_rarity('clam session'/'UGL', 'Common').
card_artist('clam session'/'UGL', 'Randy Elliott').
card_number('clam session'/'UGL', '20').
card_flavor_text('clam session'/'UGL', '\"Duke, Duke, Duke, Duke of Pearl . . . .\"').
card_multiverse_id('clam session'/'UGL', '5824').

card_in_set('clam-i-am', 'UGL').
card_original_type('clam-i-am'/'UGL', 'Summon — Clamfolk').
card_original_text('clam-i-am'/'UGL', 'Whenever you roll a 3 on a six-sided die, you may reroll that die.').
card_first_print('clam-i-am', 'UGL').
card_image_name('clam-i-am'/'UGL', 'clam-i-am').
card_uid('clam-i-am'/'UGL', 'UGL:Clam-I-Am:clam-i-am').
card_rarity('clam-i-am'/'UGL', 'Common').
card_artist('clam-i-am'/'UGL', 'Randy Elliott').
card_number('clam-i-am'/'UGL', '19').
card_flavor_text('clam-i-am'/'UGL', 'The Clams down in Clamville\nAll scootered and skittled—\n\"The three is no more!\"\nThe Clam fiddler fiddled.').
card_multiverse_id('clam-i-am'/'UGL', '5770').

card_in_set('clambassadors', 'UGL').
card_original_type('clambassadors'/'UGL', 'Summon — Clamfolk').
card_original_text('clambassadors'/'UGL', 'If Clambassadors damages any player, choose an artifact, creature, or land you control. That player gains control of that artifact, creature, or land.').
card_first_print('clambassadors', 'UGL').
card_image_name('clambassadors'/'UGL', 'clambassadors').
card_uid('clambassadors'/'UGL', 'UGL:Clambassadors:clambassadors').
card_rarity('clambassadors'/'UGL', 'Common').
card_artist('clambassadors'/'UGL', 'Randy Elliott').
card_number('clambassadors'/'UGL', '18').
card_flavor_text('clambassadors'/'UGL', '\"Sorry we shelled your village—here\'s some gold.\"').
card_multiverse_id('clambassadors'/'UGL', '5763').

card_in_set('clay pigeon', 'UGL').
card_original_type('clay pigeon'/'UGL', 'Artifact Creature').
card_original_text('clay pigeon'/'UGL', 'Flying\n{1}, Throw Clay Pigeon into the air at least two feet above your head while seated, Attempt to catch it with one hand: If you catch Clay Pigeon, prevent all damage to you from any one source and return Clay Pigeon to play, tapped. Otherwise, sacrifice it.').
card_first_print('clay pigeon', 'UGL').
card_image_name('clay pigeon'/'UGL', 'clay pigeon').
card_uid('clay pigeon'/'UGL', 'UGL:Clay Pigeon:clay pigeon').
card_rarity('clay pigeon'/'UGL', 'Uncommon').
card_artist('clay pigeon'/'UGL', 'D. Alexander Gregory').
card_number('clay pigeon'/'UGL', '73').
card_multiverse_id('clay pigeon'/'UGL', '9766').

card_in_set('common courtesy', 'UGL').
card_original_type('common courtesy'/'UGL', 'Enchantment').
card_original_text('common courtesy'/'UGL', 'Counter any spell unless its caster asks your permission to play that spell. If you refuse permission, Sacrifice Common Courtesy and counter the spell.').
card_first_print('common courtesy', 'UGL').
card_image_name('common courtesy'/'UGL', 'common courtesy').
card_uid('common courtesy'/'UGL', 'UGL:Common Courtesy:common courtesy').
card_rarity('common courtesy'/'UGL', 'Uncommon').
card_artist('common courtesy'/'UGL', 'Mike Raabe').
card_number('common courtesy'/'UGL', '21').
card_flavor_text('common courtesy'/'UGL', '\"You didn\'t say the Magic word.\"').
card_multiverse_id('common courtesy'/'UGL', '9748').

card_in_set('deadhead', 'UGL').
card_original_type('deadhead'/'UGL', 'Summon — Zombie').
card_original_text('deadhead'/'UGL', 'Put Deadhead into play. Use this ability only if any opponent loses contact with his or her hand of cards and only if Deadhead is in your graveyard.').
card_first_print('deadhead', 'UGL').
card_image_name('deadhead'/'UGL', 'deadhead').
card_uid('deadhead'/'UGL', 'UGL:Deadhead:deadhead').
card_rarity('deadhead'/'UGL', 'Common').
card_artist('deadhead'/'UGL', 'Daren Bader').
card_number('deadhead'/'UGL', '30').
card_flavor_text('deadhead'/'UGL', '\"I\'m back from the Dead,\" the zombie moaned. \"And they were far out, man.\"').
card_multiverse_id('deadhead'/'UGL', '5839').

card_in_set('denied!', 'UGL').
card_original_type('denied!'/'UGL', 'Interrupt').
card_original_text('denied!'/'UGL', 'Play Denied only as any opponent casts target spell. Name a card, then look at all cards in that player\'s hand. If the named card is in the player\'s hand, counter target spell.').
card_first_print('denied!', 'UGL').
card_image_name('denied!'/'UGL', 'denied!').
card_uid('denied!'/'UGL', 'UGL:Denied!:denied!').
card_rarity('denied!'/'UGL', 'Common').
card_artist('denied!'/'UGL', 'Quinton Hoover').
card_number('denied!'/'UGL', '22').
card_flavor_text('denied!'/'UGL', '\"Don\'t worry about it. It happens to every mage sooner or later.\"').
card_multiverse_id('denied!'/'UGL', '5800').

card_in_set('double cross', 'UGL').
card_original_type('double cross'/'UGL', 'Sorcery').
card_original_text('double cross'/'UGL', 'Choose another player. Look at that player\'s hand and choose one of those cards other than a basic land. He or she discards that card. At the beginning of the next game with the player, look at the player\'s hand and choose one of those cards other than a basic land. He or she discards that card.').
card_first_print('double cross', 'UGL').
card_image_name('double cross'/'UGL', 'double cross').
card_uid('double cross'/'UGL', 'UGL:Double Cross:double cross').
card_rarity('double cross'/'UGL', 'Common').
card_artist('double cross'/'UGL', 'Jeff Laubenstein').
card_number('double cross'/'UGL', '31').
card_flavor_text('double cross'/'UGL', '\"You\'re in for a nasty butt-kickin\'.\"').
card_multiverse_id('double cross'/'UGL', '5846').

card_in_set('double deal', 'UGL').
card_original_type('double deal'/'UGL', 'Sorcery').
card_original_text('double deal'/'UGL', 'Choose another player. Double Deal deals 3 damage to that player now and deals an additional 3 damage to the player at the beginning of the next game with the player.').
card_first_print('double deal', 'UGL').
card_image_name('double deal'/'UGL', 'double deal').
card_uid('double deal'/'UGL', 'UGL:Double Deal:double deal').
card_rarity('double deal'/'UGL', 'Common').
card_artist('double deal'/'UGL', 'Edward P. Beard, Jr.').
card_number('double deal'/'UGL', '42').
card_flavor_text('double deal'/'UGL', '\"I\'m facing defeat, . . .\"').
card_multiverse_id('double deal'/'UGL', '8812').

card_in_set('double dip', 'UGL').
card_original_type('double dip'/'UGL', 'Instant').
card_original_text('double dip'/'UGL', 'Choose another player. Gain 5 life now and an additional 5 life at the beginning of the next game with that player.').
card_first_print('double dip', 'UGL').
card_image_name('double dip'/'UGL', 'double dip').
card_uid('double dip'/'UGL', 'UGL:Double Dip:double dip').
card_rarity('double dip'/'UGL', 'Common').
card_artist('double dip'/'UGL', 'D. Alexander Gregory').
card_number('double dip'/'UGL', '3').
card_flavor_text('double dip'/'UGL', 'In a duel and taking a lickin\' . . .').
card_multiverse_id('double dip'/'UGL', '5726').

card_in_set('double play', 'UGL').
card_original_type('double play'/'UGL', 'Sorcery').
card_original_text('double play'/'UGL', 'Choose another player. Search your library for a basic land and put that land into play. At the beginning of the next game with that player, search your library for an additional basic land and put that land into play. In both cases, shuffle your library afterwards.').
card_first_print('double play', 'UGL').
card_image_name('double play'/'UGL', 'double play').
card_uid('double play'/'UGL', 'UGL:Double Play:double play').
card_rarity('double play'/'UGL', 'Common').
card_artist('double play'/'UGL', 'Claymore J. Flapdoodle').
card_number('double play'/'UGL', '55').
card_flavor_text('double play'/'UGL', 'The wizard exclaimed, \"I\'m no chicken . . . .\"').
card_multiverse_id('double play'/'UGL', '9895').

card_in_set('double take', 'UGL').
card_original_type('double take'/'UGL', 'Instant').
card_original_text('double take'/'UGL', 'Choose another player. Draw two cards now and draw an additional two cards at the beginning of the next game with that player.').
card_first_print('double take', 'UGL').
card_image_name('double take'/'UGL', 'double take').
card_uid('double take'/'UGL', 'UGL:Double Take:double take').
card_rarity('double take'/'UGL', 'Common').
card_artist('double take'/'UGL', 'Claymore J. Flapdoodle').
card_number('double take'/'UGL', '23').
card_flavor_text('double take'/'UGL', '\"But next time we meet, . . .\"').
card_multiverse_id('double take'/'UGL', '9894').

card_in_set('elvish impersonators', 'UGL').
card_original_type('elvish impersonators'/'UGL', 'Summon — Elves').
card_original_text('elvish impersonators'/'UGL', 'When you play Elvish Impersonators, roll two six-sided dice one after the other. Elvish Impersonators comes into play with power equal to the first die roll and toughness equal to the second.').
card_first_print('elvish impersonators', 'UGL').
card_image_name('elvish impersonators'/'UGL', 'elvish impersonators').
card_uid('elvish impersonators'/'UGL', 'UGL:Elvish Impersonators:elvish impersonators').
card_rarity('elvish impersonators'/'UGL', 'Common').
card_artist('elvish impersonators'/'UGL', 'Claymore J. Flapdoodle').
card_number('elvish impersonators'/'UGL', '56').
card_flavor_text('elvish impersonators'/'UGL', '\"Uh-hunh.\"').
card_multiverse_id('elvish impersonators'/'UGL', '9669').

card_in_set('flock of rabid sheep', 'UGL').
card_original_type('flock of rabid sheep'/'UGL', 'Sorcery').
card_original_text('flock of rabid sheep'/'UGL', 'Flip X coins; an opponent calls heads or tails. For each flip you win, put a Rabid Sheep token into play. Treat these tokens as 2/2 green creatures that count as Sheep.').
card_first_print('flock of rabid sheep', 'UGL').
card_image_name('flock of rabid sheep'/'UGL', 'flock of rabid sheep').
card_uid('flock of rabid sheep'/'UGL', 'UGL:Flock of Rabid Sheep:flock of rabid sheep').
card_rarity('flock of rabid sheep'/'UGL', 'Uncommon').
card_artist('flock of rabid sheep'/'UGL', 'Anthony S. Waters').
card_number('flock of rabid sheep'/'UGL', '57').
card_flavor_text('flock of rabid sheep'/'UGL', 'And their bleating was like a wet salmon slapped upon the land—slap! slap! slap!').
card_multiverse_id('flock of rabid sheep'/'UGL', '9759').

card_in_set('forest', 'UGL').
card_original_type('forest'/'UGL', 'Land').
card_original_text('forest'/'UGL', 'G').
card_border('forest'/'UGL', 'black').
card_image_name('forest'/'UGL', 'forest').
card_uid('forest'/'UGL', 'UGL:Forest:forest').
card_rarity('forest'/'UGL', 'Basic Land').
card_artist('forest'/'UGL', 'Terese Nielsen').
card_number('forest'/'UGL', '88').
card_multiverse_id('forest'/'UGL', '9683').

card_in_set('fowl play', 'UGL').
card_original_type('fowl play'/'UGL', 'Enchant Creature').
card_original_text('fowl play'/'UGL', 'Enchanted creature loses all abilities and is a 1/1 creature that counts as a Chicken.').
card_first_print('fowl play', 'UGL').
card_image_name('fowl play'/'UGL', 'fowl play').
card_uid('fowl play'/'UGL', 'UGL:Fowl Play:fowl play').
card_rarity('fowl play'/'UGL', 'Common').
card_artist('fowl play'/'UGL', 'Mark Poole').
card_number('fowl play'/'UGL', '24').
card_flavor_text('fowl play'/'UGL', '\"I feel like chicken tonight!\"').
card_multiverse_id('fowl play'/'UGL', '5822').

card_in_set('free-for-all', 'UGL').
card_original_type('free-for-all'/'UGL', 'Enchantment').
card_original_text('free-for-all'/'UGL', 'When Free-for-All comes into play, set aside all creatures in play, face down.\nDuring each player\'s upkeep, that player chooses a creature card at random from those set aside in this way and puts that creature into play under his or her control.\nIf Free-for-All leaves play, put each creature still set aside this way into its owner\'s graveyard.').
card_first_print('free-for-all', 'UGL').
card_image_name('free-for-all'/'UGL', 'free-for-all').
card_uid('free-for-all'/'UGL', 'UGL:Free-for-All:free-for-all').
card_rarity('free-for-all'/'UGL', 'Rare').
card_artist('free-for-all'/'UGL', 'Claymore J. Flapdoodle').
card_number('free-for-all'/'UGL', '25').
card_multiverse_id('free-for-all'/'UGL', '9775').

card_in_set('free-range chicken', 'UGL').
card_original_type('free-range chicken'/'UGL', 'Summon — Chicken').
card_original_text('free-range chicken'/'UGL', '{1}{G}: Roll two six-sided dice. If both die rolls are the same, Free-Range Chicken gets +X/+X until end of turn, where X is the number rolled on each die. Otherwise, if the total rolled is equal to any other total you have rolled this turn for Free-Range Chicken, sacrifice it. (For example, if you roll two 3s, Free-Range Chicken gets +3/+3. If you roll a total of 6 for Free-Range Chicken later in that turn, sacrifice it.)').
card_first_print('free-range chicken', 'UGL').
card_image_name('free-range chicken'/'UGL', 'free-range chicken').
card_uid('free-range chicken'/'UGL', 'UGL:Free-Range Chicken:free-range chicken').
card_rarity('free-range chicken'/'UGL', 'Common').
card_artist('free-range chicken'/'UGL', 'Mike Raabe').
card_number('free-range chicken'/'UGL', '58').
card_multiverse_id('free-range chicken'/'UGL', '9675').

card_in_set('gerrymandering', 'UGL').
card_original_type('gerrymandering'/'UGL', 'Sorcery').
card_original_text('gerrymandering'/'UGL', 'Remove all lands from play and shuffle them together. Randomly deal to each player one land card for each land he or she had before. Each player puts those lands into play under his or her control, untapped.').
card_first_print('gerrymandering', 'UGL').
card_image_name('gerrymandering'/'UGL', 'gerrymandering').
card_uid('gerrymandering'/'UGL', 'UGL:Gerrymandering:gerrymandering').
card_rarity('gerrymandering'/'UGL', 'Uncommon').
card_artist('gerrymandering'/'UGL', 'Doug Chaffee').
card_number('gerrymandering'/'UGL', '59').
card_multiverse_id('gerrymandering'/'UGL', '9761').

card_in_set('get a life', 'UGL').
card_original_type('get a life'/'UGL', 'Instant').
card_original_text('get a life'/'UGL', 'Target player and each of his or her teammates exchange life totals.').
card_first_print('get a life', 'UGL').
card_image_name('get a life'/'UGL', 'get a life').
card_uid('get a life'/'UGL', 'UGL:Get a Life:get a life').
card_rarity('get a life'/'UGL', 'Uncommon').
card_artist('get a life'/'UGL', 'Melissa A. Benson').
card_number('get a life'/'UGL', '4').
card_flavor_text('get a life'/'UGL', 'Gimme five! (Or whatever you got.)').
card_multiverse_id('get a life'/'UGL', '9743').

card_in_set('ghazbán ogress', 'UGL').
card_original_type('ghazbán ogress'/'UGL', 'Summon — Ogre').
card_original_text('ghazbán ogress'/'UGL', 'When Ghazbán Ogress comes into play, the player who has won the most Magic games that day gains control of it. If more than one player has won the same number of games, you retain control of Ghazbán Ogress.').
card_first_print('ghazbán ogress', 'UGL').
card_image_name('ghazbán ogress'/'UGL', 'ghazban ogress').
card_uid('ghazbán ogress'/'UGL', 'UGL:Ghazbán Ogress:ghazban ogress').
card_rarity('ghazbán ogress'/'UGL', 'Common').
card_artist('ghazbán ogress'/'UGL', 'Mike Raabe').
card_number('ghazbán ogress'/'UGL', '60').
card_multiverse_id('ghazbán ogress'/'UGL', '9668').

card_in_set('giant fan', 'UGL').
card_original_type('giant fan'/'UGL', 'Artifact').
card_original_text('giant fan'/'UGL', '{2}, {T}: Move target counter from one card to another. If the second card\'s rules text refers to any type of counters, the moved counter becomes one of those counters. Otherwise, it becomes a +1/+1 counter.').
card_first_print('giant fan', 'UGL').
card_image_name('giant fan'/'UGL', 'giant fan').
card_uid('giant fan'/'UGL', 'UGL:Giant Fan:giant fan').
card_rarity('giant fan'/'UGL', 'Rare').
card_artist('giant fan'/'UGL', 'Randy Gallegos').
card_number('giant fan'/'UGL', '74').
card_flavor_text('giant fan'/'UGL', 'Only a villain would unleash a giant fan on anyone!').
card_multiverse_id('giant fan'/'UGL', '9767').

card_in_set('goblin bookie', 'UGL').
card_original_type('goblin bookie'/'UGL', 'Summon — Goblin').
card_original_text('goblin bookie'/'UGL', '{R}, {T}: Reflip any coin or reroll any die.').
card_first_print('goblin bookie', 'UGL').
card_image_name('goblin bookie'/'UGL', 'goblin bookie').
card_uid('goblin bookie'/'UGL', 'UGL:Goblin Bookie:goblin bookie').
card_rarity('goblin bookie'/'UGL', 'Common').
card_artist('goblin bookie'/'UGL', 'Claymore J. Flapdoodle').
card_number('goblin bookie'/'UGL', '43').
card_flavor_text('goblin bookie'/'UGL', '\"Glok loved bets! He\'d sit in the bar all night an\' laugh an\' laugh. Hey—he still owes me.\"\n—Squee, goblin casino hand').
card_multiverse_id('goblin bookie'/'UGL', '9665').

card_in_set('goblin bowling team', 'UGL').
card_original_type('goblin bowling team'/'UGL', 'Summon — Goblins').
card_original_text('goblin bowling team'/'UGL', 'Whenever Goblin Bowling Team damages any creature or player, roll a six-sided die. Goblin Bowling Team deals to that creature or player additional damage equal to the die roll.').
card_first_print('goblin bowling team', 'UGL').
card_image_name('goblin bowling team'/'UGL', 'goblin bowling team').
card_uid('goblin bowling team'/'UGL', 'UGL:Goblin Bowling Team:goblin bowling team').
card_rarity('goblin bowling team'/'UGL', 'Common').
card_artist('goblin bowling team'/'UGL', 'Pete Venters').
card_number('goblin bowling team'/'UGL', '44').
card_flavor_text('goblin bowling team'/'UGL', 'Flog was out of his league—this game wasn\'t up his alley, but the team couldn\'t spare him if he split.').
card_multiverse_id('goblin bowling team'/'UGL', '9666').

card_in_set('goblin token card', 'UGL').
card_original_type('goblin token card'/'UGL', '(none)').
card_original_text('goblin token card'/'UGL', '').
card_first_print('goblin token card', 'UGL').
card_image_name('goblin token card'/'UGL', 'goblin').
card_uid('goblin token card'/'UGL', 'UGL:Goblin token card:goblin').
card_rarity('goblin token card'/'UGL', 'Uncommon').
card_artist('goblin token card'/'UGL', 'Pete Venters').
card_number('goblin token card'/'UGL', '92').
card_multiverse_id('goblin token card'/'UGL', '5503').

card_in_set('goblin tutor', 'UGL').
card_original_type('goblin tutor'/'UGL', 'Instant').
card_original_text('goblin tutor'/'UGL', 'Roll a six-sided die for Goblin Tutor. On a 1, Goblin Tutor has no effect. Otherwise, search your library for the indicated card, reveal that card to all players, and put it into your hand. Shuffle your library afterwards.\n2 Any Goblin Tutor\n3 Any enchantment\n4 Any artifact\n5 Any creature\n6 Any sorcery, instant, or interrupt').
card_first_print('goblin tutor', 'UGL').
card_image_name('goblin tutor'/'UGL', 'goblin tutor').
card_uid('goblin tutor'/'UGL', 'UGL:Goblin Tutor:goblin tutor').
card_rarity('goblin tutor'/'UGL', 'Uncommon').
card_artist('goblin tutor'/'UGL', 'Quinton Hoover').
card_number('goblin tutor'/'UGL', '45').
card_multiverse_id('goblin tutor'/'UGL', '9755').

card_in_set('growth spurt', 'UGL').
card_original_type('growth spurt'/'UGL', 'Instant').
card_original_text('growth spurt'/'UGL', 'Roll a six-sided die. Target creature gets +X/+X until end of turn, where X is equal to the die roll.').
card_first_print('growth spurt', 'UGL').
card_image_name('growth spurt'/'UGL', 'growth spurt').
card_uid('growth spurt'/'UGL', 'UGL:Growth Spurt:growth spurt').
card_rarity('growth spurt'/'UGL', 'Common').
card_artist('growth spurt'/'UGL', 'Jeff Laubenstein').
card_number('growth spurt'/'UGL', '61').
card_flavor_text('growth spurt'/'UGL', 'MORE TO LOVE: Friendly, nature-loving, Bunyonesque SEM seeks SEF looking for a huge commitment.\n . . .  seeks atog prince').
card_multiverse_id('growth spurt'/'UGL', '9671').

card_in_set('gus', 'UGL').
card_original_type('gus'/'UGL', 'Summon — Gus').
card_original_text('gus'/'UGL', 'Gus comes into play with one +1/+1 counter on it for each game you have lost to your opponent since you last won a Magic game against him or her.').
card_first_print('gus', 'UGL').
card_image_name('gus'/'UGL', 'gus').
card_uid('gus'/'UGL', 'UGL:Gus:gus').
card_rarity('gus'/'UGL', 'Common').
card_artist('gus'/'UGL', 'DiTerlizzi').
card_number('gus'/'UGL', '62').
card_flavor_text('gus'/'UGL', '\"Now I lay me down to sleep — What are you starin\' at?\"').
card_multiverse_id('gus'/'UGL', '9670').

card_in_set('handcuffs', 'UGL').
card_original_type('handcuffs'/'UGL', 'Enchantment').
card_original_text('handcuffs'/'UGL', 'Target player keeps both hands in contact with each other. If he or she does not, sacrifice Handcuffs and that player sacrifices three cards in play.').
card_first_print('handcuffs', 'UGL').
card_image_name('handcuffs'/'UGL', 'handcuffs').
card_uid('handcuffs'/'UGL', 'UGL:Handcuffs:handcuffs').
card_rarity('handcuffs'/'UGL', 'Uncommon').
card_artist('handcuffs'/'UGL', 'Jeff Laubenstein').
card_number('handcuffs'/'UGL', '32').
card_flavor_text('handcuffs'/'UGL', '\"That was fun! Now me.\"\n—Gwendlyn Di Corci').
card_multiverse_id('handcuffs'/'UGL', '9751').

card_in_set('hungry hungry heifer', 'UGL').
card_original_type('hungry hungry heifer'/'UGL', 'Summon — Cow').
card_original_text('hungry hungry heifer'/'UGL', 'During your upkeep, remove a counter from any card you control or sacrifice Hungry Hungry Heifer.').
card_first_print('hungry hungry heifer', 'UGL').
card_image_name('hungry hungry heifer'/'UGL', 'hungry hungry heifer').
card_uid('hungry hungry heifer'/'UGL', 'UGL:Hungry Hungry Heifer:hungry hungry heifer').
card_rarity('hungry hungry heifer'/'UGL', 'Uncommon').
card_artist('hungry hungry heifer'/'UGL', 'Randy Gallegos').
card_number('hungry hungry heifer'/'UGL', '63').
card_flavor_text('hungry hungry heifer'/'UGL', 'Mooo.').
card_multiverse_id('hungry hungry heifer'/'UGL', '9843').

card_in_set('hurloon wrangler', 'UGL').
card_original_type('hurloon wrangler'/'UGL', 'Summon — Minotaur').
card_original_text('hurloon wrangler'/'UGL', 'Denimwalk (If defending player is wearing any clothing made of denim, this creature is unblockable.)').
card_first_print('hurloon wrangler', 'UGL').
card_image_name('hurloon wrangler'/'UGL', 'hurloon wrangler').
card_uid('hurloon wrangler'/'UGL', 'UGL:Hurloon Wrangler:hurloon wrangler').
card_rarity('hurloon wrangler'/'UGL', 'Common').
card_artist('hurloon wrangler'/'UGL', 'Kaja Foglio').
card_number('hurloon wrangler'/'UGL', '46').
card_flavor_text('hurloon wrangler'/'UGL', '\"Nothing comes between me and my Didgeridoos™.\"').
card_multiverse_id('hurloon wrangler'/'UGL', '8904').

card_in_set('i\'m rubber, you\'re glue', 'UGL').
card_original_type('i\'m rubber, you\'re glue'/'UGL', 'Enchantment').
card_original_text('i\'m rubber, you\'re glue'/'UGL', 'Speak only in rhyming sentences. If you do not, sacrifice I\'m Rubber, You\'re Glue.\nSay \"I\'m rubber, you\'re glue. Everything bounces off me and sticks to you\": Target spell or ability, which targets only you, targets another player of your choice instead. (The new target must be legal.)').
card_first_print('i\'m rubber, you\'re glue', 'UGL').
card_image_name('i\'m rubber, you\'re glue'/'UGL', 'i\'m rubber, you\'re glue').
card_uid('i\'m rubber, you\'re glue'/'UGL', 'UGL:I\'m Rubber, You\'re Glue:i\'m rubber, you\'re glue').
card_rarity('i\'m rubber, you\'re glue'/'UGL', 'Rare').
card_artist('i\'m rubber, you\'re glue'/'UGL', 'Claymore J. Flapdoodle').
card_number('i\'m rubber, you\'re glue'/'UGL', '5').
card_multiverse_id('i\'m rubber, you\'re glue'/'UGL', '9774').

card_in_set('incoming!', 'UGL').
card_original_type('incoming!'/'UGL', 'Sorcery').
card_original_text('incoming!'/'UGL', 'Each player searches his or her library for any number of artifacts, creatures, enchantments, and lands and puts those cards into play. Each player shuffles his or her library afterwards.').
card_first_print('incoming!', 'UGL').
card_image_name('incoming!'/'UGL', 'incoming!').
card_uid('incoming!'/'UGL', 'UGL:Incoming!:incoming!').
card_rarity('incoming!'/'UGL', 'Rare').
card_artist('incoming!'/'UGL', 'Quinton Hoover').
card_number('incoming!'/'UGL', '64').
card_multiverse_id('incoming!'/'UGL', '9788').

card_in_set('infernal spawn of evil', 'UGL').
card_original_type('infernal spawn of evil'/'UGL', 'Summon — Demon Beast').
card_original_text('infernal spawn of evil'/'UGL', 'Flying, first strike\n{1}{B}, Reveal Infernal Spawn of Evil from your hand, Say \"It\'s coming\": Infernal Spawn of Evil deals 1 damage to target opponent. Use this ability only during your upkeep and only once each upkeep.').
card_first_print('infernal spawn of evil', 'UGL').
card_image_name('infernal spawn of evil'/'UGL', 'infernal spawn of evil').
card_uid('infernal spawn of evil'/'UGL', 'UGL:Infernal Spawn of Evil:infernal spawn of evil').
card_rarity('infernal spawn of evil'/'UGL', 'Rare').
card_artist('infernal spawn of evil'/'UGL', 'Ron Spencer').
card_number('infernal spawn of evil'/'UGL', '33').
card_multiverse_id('infernal spawn of evil'/'UGL', '9779').

card_in_set('island', 'UGL').
card_original_type('island'/'UGL', 'Land').
card_original_text('island'/'UGL', 'U').
card_border('island'/'UGL', 'black').
card_image_name('island'/'UGL', 'island').
card_uid('island'/'UGL', 'UGL:Island:island').
card_rarity('island'/'UGL', 'Basic Land').
card_artist('island'/'UGL', 'Daren Bader').
card_number('island'/'UGL', '85').
card_multiverse_id('island'/'UGL', '9677').

card_in_set('jack-in-the-mox', 'UGL').
card_original_type('jack-in-the-mox'/'UGL', 'Artifact').
card_original_text('jack-in-the-mox'/'UGL', '{T}: Roll a six-sided die for Jack-in-the-Mox. On a 1, sacrifice Jack-in-the-Mox and lose 5 life. Otherwise, Jack-in-the-Mox has one of the following effects. Treat this ability as a mana source.\n2 Add {W} to your mana pool.\n3 Add {U} to your mana pool.\n4 Add {B} to your mana pool.\n5 Add {R} to your mana pool.\n6 Add {G} to your mana pool.').
card_first_print('jack-in-the-mox', 'UGL').
card_image_name('jack-in-the-mox'/'UGL', 'jack-in-the-mox').
card_uid('jack-in-the-mox'/'UGL', 'UGL:Jack-in-the-Mox:jack-in-the-mox').
card_rarity('jack-in-the-mox'/'UGL', 'Rare').
card_artist('jack-in-the-mox'/'UGL', 'Dan Frazier').
card_number('jack-in-the-mox'/'UGL', '75').
card_multiverse_id('jack-in-the-mox'/'UGL', '9729').

card_in_set('jalum grifter', 'UGL').
card_original_type('jalum grifter'/'UGL', 'Summon — Legend').
card_original_text('jalum grifter'/'UGL', '{1}{R}, {T}: Put Jalum Grifter and two lands you control face down in front of target opponent after revealing each card to him or her. Then, rearrange the order of the three cards as often as you wish, keeping them on the table at all times. That opponent then chooses one of those cards. If a land is chosen, destroy target card in play. Otherwise, sacrifice Jalum Grifter.').
card_first_print('jalum grifter', 'UGL').
card_image_name('jalum grifter'/'UGL', 'jalum grifter').
card_uid('jalum grifter'/'UGL', 'UGL:Jalum Grifter:jalum grifter').
card_rarity('jalum grifter'/'UGL', 'Rare').
card_artist('jalum grifter'/'UGL', 'Claymore J. Flapdoodle').
card_number('jalum grifter'/'UGL', '47').
card_multiverse_id('jalum grifter'/'UGL', '9782').

card_in_set('jester\'s sombrero', 'UGL').
card_original_type('jester\'s sombrero'/'UGL', 'Artifact').
card_original_text('jester\'s sombrero'/'UGL', '{2}, {T}, Sacrifice Jester\'s Sombrero: Look through target player\'s sideboard and remove any three of those cards from it for the remainder of the match.').
card_first_print('jester\'s sombrero', 'UGL').
card_image_name('jester\'s sombrero'/'UGL', 'jester\'s sombrero').
card_uid('jester\'s sombrero'/'UGL', 'UGL:Jester\'s Sombrero:jester\'s sombrero').
card_rarity('jester\'s sombrero'/'UGL', 'Rare').
card_artist('jester\'s sombrero'/'UGL', 'Dan Frazier').
card_number('jester\'s sombrero'/'UGL', '76').
card_flavor_text('jester\'s sombrero'/'UGL', '\"¡Yo quiero Kormus Bell!\"').
card_multiverse_id('jester\'s sombrero'/'UGL', '9739').

card_in_set('jumbo imp', 'UGL').
card_original_type('jumbo imp'/'UGL', 'Summon — Imp').
card_original_text('jumbo imp'/'UGL', 'Flying\nWhen you play Jumbo Imp, roll a six-sided die. Jumbo Imp comes into play with a number of +1/+1 counters on it equal to the die roll.\nDuring your upkeep, roll a six-sided die and put on Jumbo Imp a number of +1/+1 counters equal to the die roll.\nAt the end of your turn, roll a six-sided die and remove from Jumbo Imp a number of +1/+1 counters equal to the die roll.').
card_first_print('jumbo imp', 'UGL').
card_image_name('jumbo imp'/'UGL', 'jumbo imp').
card_uid('jumbo imp'/'UGL', 'UGL:Jumbo Imp:jumbo imp').
card_rarity('jumbo imp'/'UGL', 'Uncommon').
card_artist('jumbo imp'/'UGL', 'Pete Venters').
card_number('jumbo imp'/'UGL', '34').
card_multiverse_id('jumbo imp'/'UGL', '9750').

card_in_set('knight of the hokey pokey', 'UGL').
card_original_type('knight of the hokey pokey'/'UGL', 'Summon — Knight').
card_original_text('knight of the hokey pokey'/'UGL', 'First strike\n{1}{W}, Do the Hokey Pokey (Stand up, wiggle your butt, raise your hands above your head, and shake them wildly as you rotate 360 degrees): Prevent all damage to Knight of the Hokey Pokey from any one source.').
card_first_print('knight of the hokey pokey', 'UGL').
card_image_name('knight of the hokey pokey'/'UGL', 'knight of the hokey pokey').
card_uid('knight of the hokey pokey'/'UGL', 'UGL:Knight of the Hokey Pokey:knight of the hokey pokey').
card_rarity('knight of the hokey pokey'/'UGL', 'Common').
card_artist('knight of the hokey pokey'/'UGL', 'Kev Walker').
card_number('knight of the hokey pokey'/'UGL', '6').
card_flavor_text('knight of the hokey pokey'/'UGL', 'That\'s what it\'s all about.').
card_multiverse_id('knight of the hokey pokey'/'UGL', '9742').

card_in_set('krazy kow', 'UGL').
card_original_type('krazy kow'/'UGL', 'Summon — Cow').
card_original_text('krazy kow'/'UGL', 'During your upkeep, roll a six-sided die. On a 1, sacrifice Krazy Kow and it deals 3 damage to each creature and player.').
card_first_print('krazy kow', 'UGL').
card_image_name('krazy kow'/'UGL', 'krazy kow').
card_uid('krazy kow'/'UGL', 'UGL:Krazy Kow:krazy kow').
card_rarity('krazy kow'/'UGL', 'Common').
card_artist('krazy kow'/'UGL', 'Ron Spencer').
card_number('krazy kow'/'UGL', '48').
card_flavor_text('krazy kow'/'UGL', '\"I got your milk right here!\"').
card_multiverse_id('krazy kow'/'UGL', '9758').

card_in_set('landfill', 'UGL').
card_original_type('landfill'/'UGL', 'Sorcery').
card_original_text('landfill'/'UGL', 'Choose a land type. Remove from play all lands of that type that you control. Drop those cards, one at a time, onto the playing area from a height of at least one foot. Destroy each card in play that is completely covered by those cards. Then return to play, tapped, all lands dropped in this way.').
card_first_print('landfill', 'UGL').
card_image_name('landfill'/'UGL', 'landfill').
card_uid('landfill'/'UGL', 'UGL:Landfill:landfill').
card_rarity('landfill'/'UGL', 'Rare').
card_artist('landfill'/'UGL', 'Daren Bader').
card_number('landfill'/'UGL', '49').
card_multiverse_id('landfill'/'UGL', '9785').

card_in_set('lexivore', 'UGL').
card_original_type('lexivore'/'UGL', 'Summon — Beast').
card_original_text('lexivore'/'UGL', 'If Lexivore damages any player, destroy target card in play, other than Lexivore, with the most lines of text in its text box. (If more than one card has the most lines of text, you choose which of those cards to destroy.)').
card_first_print('lexivore', 'UGL').
card_image_name('lexivore'/'UGL', 'lexivore').
card_uid('lexivore'/'UGL', 'UGL:Lexivore:lexivore').
card_rarity('lexivore'/'UGL', 'Uncommon').
card_artist('lexivore'/'UGL', 'Daniel Gelon').
card_number('lexivore'/'UGL', '7').
card_flavor_text('lexivore'/'UGL', 'Plucking the chicken\n—Elvish expression meaning \"flinging the monkey\"').
card_multiverse_id('lexivore'/'UGL', '9760').

card_in_set('look at me, i\'m the dci', 'UGL').
card_original_type('look at me, i\'m the dci'/'UGL', 'Sorcery').
card_original_text('look at me, i\'m the dci'/'UGL', 'Ban one card, other than a basic land, for the remainder of the match. (For the remainder of the match, each player removes from the game all copies of that card in play or in any graveyard, hand, library, or sideboard.)').
card_first_print('look at me, i\'m the dci', 'UGL').
card_image_name('look at me, i\'m the dci'/'UGL', 'look at me, i\'m the dci').
card_uid('look at me, i\'m the dci'/'UGL', 'UGL:Look at Me, I\'m the DCI:look at me, i\'m the dci').
card_rarity('look at me, i\'m the dci'/'UGL', 'Rare').
card_artist('look at me, i\'m the dci'/'UGL', 'Mark Rosewater').
card_number('look at me, i\'m the dci'/'UGL', '8').
card_flavor_text('look at me, i\'m the dci'/'UGL', 'Few understand the DCI\'s rigorous decision-making process.').
card_multiverse_id('look at me, i\'m the dci'/'UGL', '9771').

card_in_set('mesa chicken', 'UGL').
card_original_type('mesa chicken'/'UGL', 'Summon — Chicken').
card_original_text('mesa chicken'/'UGL', 'Stand up, Flap your arms, Cluck like a chicken: Mesa Chicken gains flying until end of turn.').
card_first_print('mesa chicken', 'UGL').
card_image_name('mesa chicken'/'UGL', 'mesa chicken').
card_uid('mesa chicken'/'UGL', 'UGL:Mesa Chicken:mesa chicken').
card_rarity('mesa chicken'/'UGL', 'Common').
card_artist('mesa chicken'/'UGL', 'Kev Walker').
card_number('mesa chicken'/'UGL', '9').
card_flavor_text('mesa chicken'/'UGL', '\"Lo! lord of layers proudly comb-crested\nHero to hens father to feathers\nCrowing sun-caller weaver of wattle\nElder to eggs.\"\n—Rooster Saga').
card_multiverse_id('mesa chicken'/'UGL', '5725').

card_in_set('mine, mine, mine!', 'UGL').
card_original_type('mine, mine, mine!'/'UGL', 'Enchantment').
card_original_text('mine, mine, mine!'/'UGL', 'When Mine, Mine, Mine comes into play, each player puts his or her library into his or her hand.\nEach player skips his or her discard phase and does not lose as a result of being unable to draw a card.\nEach player cannot play more than one spell each turn.\nIf Mine, Mine, Mine leaves play, each player shuffles his or her hand and graveyard into his or her library.').
card_first_print('mine, mine, mine!', 'UGL').
card_image_name('mine, mine, mine!'/'UGL', 'mine, mine, mine!').
card_uid('mine, mine, mine!'/'UGL', 'UGL:Mine, Mine, Mine!:mine, mine, mine!').
card_rarity('mine, mine, mine!'/'UGL', 'Rare').
card_artist('mine, mine, mine!'/'UGL', 'Heather Hudson').
card_number('mine, mine, mine!'/'UGL', '65').
card_multiverse_id('mine, mine, mine!'/'UGL', '9787').

card_in_set('mirror mirror', 'UGL').
card_original_type('mirror mirror'/'UGL', 'Artifact').
card_original_text('mirror mirror'/'UGL', 'Mirror Mirror comes into play tapped.\n{7}, {T}, Sacrifice Mirror Mirror: At end of turn, exchange life totals with target player and exchange all cards in play that you control, and all cards in your hand, library, and graveyard, with that player until end of game.').
card_first_print('mirror mirror', 'UGL').
card_image_name('mirror mirror'/'UGL', 'mirror mirror').
card_uid('mirror mirror'/'UGL', 'UGL:Mirror Mirror:mirror mirror').
card_rarity('mirror mirror'/'UGL', 'Rare').
card_artist('mirror mirror'/'UGL', 'Kaja Foglio').
card_number('mirror mirror'/'UGL', '77').
card_multiverse_id('mirror mirror'/'UGL', '9765').

card_in_set('miss demeanor', 'UGL').
card_original_type('miss demeanor'/'UGL', 'Summon — Lady of Proper Etiquette').
card_original_text('miss demeanor'/'UGL', 'Flying, first strike\nDuring each other player\'s turn, compliment that player on his or her game play or sacrifice Miss Demeanor.').
card_first_print('miss demeanor', 'UGL').
card_image_name('miss demeanor'/'UGL', 'miss demeanor').
card_uid('miss demeanor'/'UGL', 'UGL:Miss Demeanor:miss demeanor').
card_rarity('miss demeanor'/'UGL', 'Uncommon').
card_artist('miss demeanor'/'UGL', 'Matthew D. Wilson').
card_number('miss demeanor'/'UGL', '10').
card_flavor_text('miss demeanor'/'UGL', 'Even war can be civil.').
card_multiverse_id('miss demeanor'/'UGL', '9745').

card_in_set('mountain', 'UGL').
card_original_type('mountain'/'UGL', 'Land').
card_original_text('mountain'/'UGL', 'R').
card_border('mountain'/'UGL', 'black').
card_image_name('mountain'/'UGL', 'mountain').
card_uid('mountain'/'UGL', 'UGL:Mountain:mountain').
card_rarity('mountain'/'UGL', 'Basic Land').
card_artist('mountain'/'UGL', 'Tom Wänerstrand').
card_number('mountain'/'UGL', '87').
card_multiverse_id('mountain'/'UGL', '9707').

card_in_set('once more with feeling', 'UGL').
card_original_type('once more with feeling'/'UGL', 'Sorcery').
card_original_text('once more with feeling'/'UGL', 'Remove Once More with Feeling from the game as well as all cards in play and in all graveyards. Each player shuffles his or her hand into her or his library, then draws seven cards. Each player\'s life total is set to 10.\nDCI ruling: This card is restricted. (You cannot play with more than one in a deck.)').
card_first_print('once more with feeling', 'UGL').
card_image_name('once more with feeling'/'UGL', 'once more with feeling').
card_uid('once more with feeling'/'UGL', 'UGL:Once More with Feeling:once more with feeling').
card_rarity('once more with feeling'/'UGL', 'Rare').
card_artist('once more with feeling'/'UGL', 'Terese Nielsen').
card_number('once more with feeling'/'UGL', '11').
card_flavor_text('once more with feeling'/'UGL', 'If yo  . . .  ted.').
card_multiverse_id('once more with feeling'/'UGL', '9772').

card_in_set('organ harvest', 'UGL').
card_original_type('organ harvest'/'UGL', 'Sorcery').
card_original_text('organ harvest'/'UGL', 'You and your teammates may sacrifice any number of creatures. For each creature sacrificed in this way, add {B}{B} to your mana pool.').
card_first_print('organ harvest', 'UGL').
card_image_name('organ harvest'/'UGL', 'organ harvest').
card_uid('organ harvest'/'UGL', 'UGL:Organ Harvest:organ harvest').
card_rarity('organ harvest'/'UGL', 'Common').
card_artist('organ harvest'/'UGL', 'Terese Nielsen').
card_number('organ harvest'/'UGL', '35').
card_flavor_text('organ harvest'/'UGL', '\"Lucy, you\'ve got some spleenin\' to do.\"').
card_multiverse_id('organ harvest'/'UGL', '9752').

card_in_set('ow', 'UGL').
card_original_type('ow'/'UGL', 'Enchantment').
card_original_text('ow'/'UGL', 'Whenever any creature damages a player, for each Ow card in play, that player says \"Ow\" once or Ow deals 1 damage to him or her.').
card_first_print('ow', 'UGL').
card_image_name('ow'/'UGL', 'ow').
card_uid('ow'/'UGL', 'UGL:Ow:ow').
card_rarity('ow'/'UGL', 'Rare').
card_artist('ow'/'UGL', 'Edward P. Beard, Jr.').
card_number('ow'/'UGL', '36').
card_flavor_text('ow'/'UGL', 'Have you ever noticed how some flavor text has no relevance whatsoever to the card it\'s on?').
card_multiverse_id('ow'/'UGL', '9781').

card_in_set('paper tiger', 'UGL').
card_original_type('paper tiger'/'UGL', 'Artifact Creature').
card_original_text('paper tiger'/'UGL', 'Rock Lobsters cannot attack or block.').
card_first_print('paper tiger', 'UGL').
card_image_name('paper tiger'/'UGL', 'paper tiger').
card_uid('paper tiger'/'UGL', 'UGL:Paper Tiger:paper tiger').
card_rarity('paper tiger'/'UGL', 'Common').
card_artist('paper tiger'/'UGL', 'Heather Hudson').
card_number('paper tiger'/'UGL', '78').
card_flavor_text('paper tiger'/'UGL', 'The tiger is always quick to fold.').
card_multiverse_id('paper tiger'/'UGL', '5705').

card_in_set('pegasus token card', 'UGL').
card_original_type('pegasus token card'/'UGL', '(none)').
card_original_text('pegasus token card'/'UGL', '').
card_first_print('pegasus token card', 'UGL').
card_image_name('pegasus token card'/'UGL', 'pegasus').
card_uid('pegasus token card'/'UGL', 'UGL:Pegasus token card:pegasus').
card_rarity('pegasus token card'/'UGL', 'Uncommon').
card_artist('pegasus token card'/'UGL', 'Mark Zug').
card_number('pegasus token card'/'UGL', '89').
card_multiverse_id('pegasus token card'/'UGL', '4979').

card_in_set('plains', 'UGL').
card_original_type('plains'/'UGL', 'Land').
card_original_text('plains'/'UGL', 'W').
card_border('plains'/'UGL', 'black').
card_image_name('plains'/'UGL', 'plains').
card_uid('plains'/'UGL', 'UGL:Plains:plains').
card_rarity('plains'/'UGL', 'Basic Land').
card_artist('plains'/'UGL', 'Christopher Rush').
card_number('plains'/'UGL', '84').
card_multiverse_id('plains'/'UGL', '9680').

card_in_set('poultrygeist', 'UGL').
card_original_type('poultrygeist'/'UGL', 'Summon — Chicken').
card_original_text('poultrygeist'/'UGL', 'Flying\nWhenever a creature is put into any graveyard from play, you may roll a six-sided die. On a 1, sacrifice Poultrygeist. Otherwise, put a +1/+1 counter on Poultrygeist.').
card_first_print('poultrygeist', 'UGL').
card_image_name('poultrygeist'/'UGL', 'poultrygeist').
card_uid('poultrygeist'/'UGL', 'UGL:Poultrygeist:poultrygeist').
card_rarity('poultrygeist'/'UGL', 'Common').
card_artist('poultrygeist'/'UGL', 'Daren Bader').
card_number('poultrygeist'/'UGL', '37').
card_flavor_text('poultrygeist'/'UGL', 'Farmer Brown never ate eggs again.').
card_multiverse_id('poultrygeist'/'UGL', '5864').

card_in_set('prismatic wardrobe', 'UGL').
card_original_type('prismatic wardrobe'/'UGL', 'Sorcery').
card_original_text('prismatic wardrobe'/'UGL', 'Destroy target card that does not share a color with clothing worn by its controller. You cannot choose an artifact or land card.').
card_first_print('prismatic wardrobe', 'UGL').
card_image_name('prismatic wardrobe'/'UGL', 'prismatic wardrobe').
card_uid('prismatic wardrobe'/'UGL', 'UGL:Prismatic Wardrobe:prismatic wardrobe').
card_rarity('prismatic wardrobe'/'UGL', 'Common').
card_artist('prismatic wardrobe'/'UGL', 'D. Alexander Gregory').
card_number('prismatic wardrobe'/'UGL', '12').
card_flavor_text('prismatic wardrobe'/'UGL', '\"Spring in Dominaria means pastels!\"\n—Excerpt from Mage\'s Home Journal').
card_multiverse_id('prismatic wardrobe'/'UGL', '9744').

card_in_set('psychic network', 'UGL').
card_original_type('psychic network'/'UGL', 'Enchantment').
card_original_text('psychic network'/'UGL', 'Each player reveals the top card of his or her library to all other players by continuously holding it against his or her forehead. This does not allow a player to look at his or her own card. (That card still counts as the top card of your library. Whenever you draw a card, draw that one and replace it with the next card of your library.)').
card_first_print('psychic network', 'UGL').
card_image_name('psychic network'/'UGL', 'psychic network').
card_uid('psychic network'/'UGL', 'UGL:Psychic Network:psychic network').
card_rarity('psychic network'/'UGL', 'Rare').
card_artist('psychic network'/'UGL', 'Claymore J. Flapdoodle').
card_number('psychic network'/'UGL', '26').
card_multiverse_id('psychic network'/'UGL', '9777').

card_in_set('ricochet', 'UGL').
card_original_type('ricochet'/'UGL', 'Enchantment').
card_original_text('ricochet'/'UGL', 'Whenever any spell targets a single player, each player rolls a six-sided die. That spell is redirected to the player or players with the lowest die roll. If two or more players tie for the lowest die roll, they reroll until there is no tie.').
card_first_print('ricochet', 'UGL').
card_image_name('ricochet'/'UGL', 'ricochet').
card_uid('ricochet'/'UGL', 'UGL:Ricochet:ricochet').
card_rarity('ricochet'/'UGL', 'Uncommon').
card_artist('ricochet'/'UGL', 'David A. Cherry').
card_number('ricochet'/'UGL', '50').
card_multiverse_id('ricochet'/'UGL', '9756').

card_in_set('rock lobster', 'UGL').
card_original_type('rock lobster'/'UGL', 'Artifact Creature').
card_original_text('rock lobster'/'UGL', 'Scissors Lizards cannot attack or block.').
card_first_print('rock lobster', 'UGL').
card_image_name('rock lobster'/'UGL', 'rock lobster').
card_uid('rock lobster'/'UGL', 'UGL:Rock Lobster:rock lobster').
card_rarity('rock lobster'/'UGL', 'Common').
card_artist('rock lobster'/'UGL', 'Heather Hudson').
card_number('rock lobster'/'UGL', '79').
card_flavor_text('rock lobster'/'UGL', 'Many take the lobster for granite.').
card_multiverse_id('rock lobster'/'UGL', '5648').

card_in_set('scissors lizard', 'UGL').
card_original_type('scissors lizard'/'UGL', 'Artifact Creature').
card_original_text('scissors lizard'/'UGL', 'Paper Tigers cannot attack or block.').
card_first_print('scissors lizard', 'UGL').
card_image_name('scissors lizard'/'UGL', 'scissors lizard').
card_uid('scissors lizard'/'UGL', 'UGL:Scissors Lizard:scissors lizard').
card_rarity('scissors lizard'/'UGL', 'Common').
card_artist('scissors lizard'/'UGL', 'Heather Hudson').
card_number('scissors lizard'/'UGL', '80').
card_flavor_text('scissors lizard'/'UGL', 'Nothing beats the lizard\'s shear power.').
card_multiverse_id('scissors lizard'/'UGL', '5698').

card_in_set('sex appeal', 'UGL').
card_original_type('sex appeal'/'UGL', 'Instant').
card_original_text('sex appeal'/'UGL', 'Prevent up to 3 damage total to any number of creatures and/or players. If there are more players in the room of the opposite sex, prevent up to 3 additional damage total to any number of creatures and/or players.').
card_first_print('sex appeal', 'UGL').
card_image_name('sex appeal'/'UGL', 'sex appeal').
card_uid('sex appeal'/'UGL', 'UGL:Sex Appeal:sex appeal').
card_rarity('sex appeal'/'UGL', 'Common').
card_artist('sex appeal'/'UGL', 'Matthew D. Wilson').
card_number('sex appeal'/'UGL', '13').
card_multiverse_id('sex appeal'/'UGL', '5755').

card_in_set('sheep token card', 'UGL').
card_original_type('sheep token card'/'UGL', '(none)').
card_original_text('sheep token card'/'UGL', '').
card_first_print('sheep token card', 'UGL').
card_image_name('sheep token card'/'UGL', 'sheep').
card_uid('sheep token card'/'UGL', 'UGL:Sheep token card:sheep').
card_rarity('sheep token card'/'UGL', 'Uncommon').
card_artist('sheep token card'/'UGL', 'Kev Walker').
card_number('sheep token card'/'UGL', '93').
card_multiverse_id('sheep token card'/'UGL', '5560').

card_in_set('soldier token card', 'UGL').
card_original_type('soldier token card'/'UGL', '(none)').
card_original_text('soldier token card'/'UGL', '').
card_first_print('soldier token card', 'UGL').
card_image_name('soldier token card'/'UGL', 'soldier').
card_uid('soldier token card'/'UGL', 'UGL:Soldier token card:soldier').
card_rarity('soldier token card'/'UGL', 'Uncommon').
card_artist('soldier token card'/'UGL', 'Daren Bader').
card_number('soldier token card'/'UGL', '90').
card_multiverse_id('soldier token card'/'UGL', '5472').

card_in_set('sorry', 'UGL').
card_original_type('sorry'/'UGL', 'Enchantment').
card_original_text('sorry'/'UGL', 'Before playing any spell, if a copy of that spell card is in any graveyard, the spell\'s caster may say \"Sorry.\" If he or she does not, any other player may counter the spell by saying \"Sorry\" as it is cast.\nIf any player says \"Sorry\" at any other time, Sorry deals 2 damage to that player.').
card_first_print('sorry', 'UGL').
card_image_name('sorry'/'UGL', 'sorry').
card_uid('sorry'/'UGL', 'UGL:Sorry:sorry').
card_rarity('sorry'/'UGL', 'Uncommon').
card_artist('sorry'/'UGL', 'Kaja Foglio').
card_number('sorry'/'UGL', '27').
card_multiverse_id('sorry'/'UGL', '9776').

card_in_set('spark fiend', 'UGL').
card_original_type('spark fiend'/'UGL', 'Summon — Beast').
card_original_text('spark fiend'/'UGL', 'When Spark Fiend comes into play, roll two six-sided dice. On a total of 2, 3, or 12, sacrifice Spark Fiend. On a total of 7 or 11, do not roll dice for Spark Fiend during any of your following upkeep phases. If you roll any other total, note it.\nDuring your upkeep, roll two six-sided dice. On a total of 7, sacrifice Spark Fiend. If you roll the noted total, do not roll dice for Spark Fiend during any of your following upkeep phases. On any other roll, there is no effect.').
card_first_print('spark fiend', 'UGL').
card_image_name('spark fiend'/'UGL', 'spark fiend').
card_uid('spark fiend'/'UGL', 'UGL:Spark Fiend:spark fiend').
card_rarity('spark fiend'/'UGL', 'Rare').
card_artist('spark fiend'/'UGL', 'Pete Venters').
card_number('spark fiend'/'UGL', '51').
card_multiverse_id('spark fiend'/'UGL', '9754').

card_in_set('spatula of the ages', 'UGL').
card_original_type('spatula of the ages'/'UGL', 'Artifact').
card_original_text('spatula of the ages'/'UGL', '{4}, {T}, Sacrifice Spatula of the Ages: Put into play from your hand any card from an Unglued supplement.').
card_first_print('spatula of the ages', 'UGL').
card_image_name('spatula of the ages'/'UGL', 'spatula of the ages').
card_uid('spatula of the ages'/'UGL', 'UGL:Spatula of the Ages:spatula of the ages').
card_rarity('spatula of the ages'/'UGL', 'Uncommon').
card_artist('spatula of the ages'/'UGL', 'Melissa A. Benson').
card_number('spatula of the ages'/'UGL', '81').
card_flavor_text('spatula of the ages'/'UGL', 'At last Urza\'s powers were focused through the incredible artifact.\n\"Who wants pancakes?\" he asked.').
card_multiverse_id('spatula of the ages'/'UGL', '9740').

card_in_set('squirrel farm', 'UGL').
card_original_type('squirrel farm'/'UGL', 'Enchantment').
card_original_text('squirrel farm'/'UGL', '{1}{G}: Choose a card in your hand. Covering the artist\'s name, reveal the card to target player. If that player cannot name the artist, reveal the artist\'s name and put a Squirrel token into play. Treat this token as a 1/1 green creature.').
card_first_print('squirrel farm', 'UGL').
card_image_name('squirrel farm'/'UGL', 'squirrel farm').
card_uid('squirrel farm'/'UGL', 'UGL:Squirrel Farm:squirrel farm').
card_rarity('squirrel farm'/'UGL', 'Rare').
card_artist('squirrel farm'/'UGL', 'Ron Spencer').
card_number('squirrel farm'/'UGL', '66').
card_flavor_text('squirrel farm'/'UGL', '\"And the ignorant shall fall to the squirrels.\"\n—Chip 2:54').
card_multiverse_id('squirrel farm'/'UGL', '9789').

card_in_set('squirrel token card', 'UGL').
card_original_type('squirrel token card'/'UGL', '(none)').
card_original_text('squirrel token card'/'UGL', '').
card_first_print('squirrel token card', 'UGL').
card_image_name('squirrel token card'/'UGL', 'squirrel').
card_uid('squirrel token card'/'UGL', 'UGL:Squirrel token card:squirrel').
card_rarity('squirrel token card'/'UGL', 'Uncommon').
card_artist('squirrel token card'/'UGL', 'Ron Spencer').
card_number('squirrel token card'/'UGL', '94').
card_multiverse_id('squirrel token card'/'UGL', '5607').

card_in_set('strategy, schmategy', 'UGL').
card_original_type('strategy, schmategy'/'UGL', 'Sorcery').
card_original_text('strategy, schmategy'/'UGL', 'Roll a six-sided die for Strategy, Schmategy. On a 1, Strategy, Schmategy has no effect. Otherwise, it has one of the following effects.\n2 Destroy all artifacts.\n3 Destroy all lands.\n4 Strategy, Schmategy deals 3 damage to each creature and player.\n5 Each player discards his or her hand and draws seven cards. \n6 Roll the die two more times.').
card_first_print('strategy, schmategy', 'UGL').
card_image_name('strategy, schmategy'/'UGL', 'strategy, schmategy').
card_uid('strategy, schmategy'/'UGL', 'UGL:Strategy, Schmategy:strategy, schmategy').
card_rarity('strategy, schmategy'/'UGL', 'Rare').
card_artist('strategy, schmategy'/'UGL', 'Daniel Gelon').
card_number('strategy, schmategy'/'UGL', '52').
card_multiverse_id('strategy, schmategy'/'UGL', '9784').

card_in_set('swamp', 'UGL').
card_original_type('swamp'/'UGL', 'Land').
card_original_text('swamp'/'UGL', 'B').
card_border('swamp'/'UGL', 'black').
card_image_name('swamp'/'UGL', 'swamp').
card_uid('swamp'/'UGL', 'UGL:Swamp:swamp').
card_rarity('swamp'/'UGL', 'Basic Land').
card_artist('swamp'/'UGL', 'Mark Zug').
card_number('swamp'/'UGL', '86').
card_multiverse_id('swamp'/'UGL', '9676').

card_in_set('team spirit', 'UGL').
card_original_type('team spirit'/'UGL', 'Instant').
card_original_text('team spirit'/'UGL', 'All creatures controlled by target player and his or her teammates get +1/+1 until end of turn.').
card_first_print('team spirit', 'UGL').
card_image_name('team spirit'/'UGL', 'team spirit').
card_uid('team spirit'/'UGL', 'UGL:Team Spirit:team spirit').
card_rarity('team spirit'/'UGL', 'Common').
card_artist('team spirit'/'UGL', 'Terese Nielsen').
card_number('team spirit'/'UGL', '67').
card_flavor_text('team spirit'/'UGL', 'Due to some late-night partying, the chicken missed the group photo.').
card_multiverse_id('team spirit'/'UGL', '9763').

card_in_set('temp of the damned', 'UGL').
card_original_type('temp of the damned'/'UGL', 'Summon — Zombie').
card_original_text('temp of the damned'/'UGL', 'When you play Temp of the Damned, roll a six-sided die. Temp of the Damned comes into play with a number of funk counters on it equal to the die roll.\nDuring your upkeep, remove a funk counter from Temp of the Damned or sacrifice Temp of the Damned.').
card_first_print('temp of the damned', 'UGL').
card_image_name('temp of the damned'/'UGL', 'temp of the damned').
card_uid('temp of the damned'/'UGL', 'UGL:Temp of the Damned:temp of the damned').
card_rarity('temp of the damned'/'UGL', 'Common').
card_artist('temp of the damned'/'UGL', 'Adam Rex').
card_number('temp of the damned'/'UGL', '38').
card_flavor_text('temp of the damned'/'UGL', '\"Not the graveyard shift again!\"').
card_multiverse_id('temp of the damned'/'UGL', '10717').

card_in_set('the cheese stands alone', 'UGL').
card_original_type('the cheese stands alone'/'UGL', 'Enchantment').
card_original_text('the cheese stands alone'/'UGL', 'If you control no cards in play other than The Cheese Stands Alone and have no cards in your hand, you win the game.').
card_first_print('the cheese stands alone', 'UGL').
card_image_name('the cheese stands alone'/'UGL', 'the cheese stands alone').
card_uid('the cheese stands alone'/'UGL', 'UGL:The Cheese Stands Alone:the cheese stands alone').
card_rarity('the cheese stands alone'/'UGL', 'Rare').
card_artist('the cheese stands alone'/'UGL', 'Randy Elliott').
card_number('the cheese stands alone'/'UGL', '2').
card_flavor_text('the cheese stands alone'/'UGL', 'The meat, on the other hand, has frequent visitors.').
card_multiverse_id('the cheese stands alone'/'UGL', '9773').

card_in_set('the ultimate nightmare of wizards of the coast® customer service', 'UGL').
card_original_type('the ultimate nightmare of wizards of the coast® customer service'/'UGL', 'Sorcery').
card_original_text('the ultimate nightmare of wizards of the coast® customer service'/'UGL', 'The Ultimate Nightmare of Wizards of the Coast® Customer Service deals X damage to each of Y target creatures and Z target players.').
card_first_print('the ultimate nightmare of wizards of the coast® customer service', 'UGL').
card_image_name('the ultimate nightmare of wizards of the coast® customer service'/'UGL', 'the ultimate nightmare of wizards of the coastr customer service').
card_uid('the ultimate nightmare of wizards of the coast® customer service'/'UGL', 'UGL:The Ultimate Nightmare of Wizards of the Coast® Customer Service:the ultimate nightmare of wizards of the coastr customer service').
card_rarity('the ultimate nightmare of wizards of the coast® customer service'/'UGL', 'Uncommon').
card_artist('the ultimate nightmare of wizards of the coast® customer service'/'UGL', 'Melissa A. Benson').
card_number('the ultimate nightmare of wizards of the coast® customer service'/'UGL', '53').
card_flavor_text('the ultimate nightmare of wizards of the coast® customer service'/'UGL', 'Mon.–Fri. 9:00 A.M.–8:00 P.M. Pacific\n(206) 624-0933').
card_multiverse_id('the ultimate nightmare of wizards of the coast® customer service'/'UGL', '9757').

card_in_set('timmy, power gamer', 'UGL').
card_original_type('timmy, power gamer'/'UGL', 'Summon — Legend').
card_original_text('timmy, power gamer'/'UGL', '{4}: Put a creature into play from your hand.').
card_first_print('timmy, power gamer', 'UGL').
card_image_name('timmy, power gamer'/'UGL', 'timmy, power gamer').
card_uid('timmy, power gamer'/'UGL', 'UGL:Timmy, Power Gamer:timmy, power gamer').
card_rarity('timmy, power gamer'/'UGL', 'Rare').
card_artist('timmy, power gamer'/'UGL', 'Edward P. Beard, Jr.').
card_number('timmy, power gamer'/'UGL', '68').
card_flavor_text('timmy, power gamer'/'UGL', '\"Just wait till I get my Leviathan . . . .\"').
card_multiverse_id('timmy, power gamer'/'UGL', '9786').

card_in_set('urza\'s contact lenses', 'UGL').
card_original_type('urza\'s contact lenses'/'UGL', 'Artifact').
card_original_text('urza\'s contact lenses'/'UGL', 'Urza\'s Contact Lenses comes into play tapped and does not untap during its controller\'s untap phase.\nAll players play with their hands face up.\nClap your hands twice: Tap or untap Urza\'s Contact Lenses.').
card_first_print('urza\'s contact lenses', 'UGL').
card_image_name('urza\'s contact lenses'/'UGL', 'urza\'s contact lenses').
card_uid('urza\'s contact lenses'/'UGL', 'UGL:Urza\'s Contact Lenses:urza\'s contact lenses').
card_rarity('urza\'s contact lenses'/'UGL', 'Uncommon').
card_artist('urza\'s contact lenses'/'UGL', 'David A. Cherry').
card_number('urza\'s contact lenses'/'UGL', '82').
card_multiverse_id('urza\'s contact lenses'/'UGL', '9770').

card_in_set('urza\'s science fair project', 'UGL').
card_original_type('urza\'s science fair project'/'UGL', 'Artifact Creature').
card_original_text('urza\'s science fair project'/'UGL', '{2}: Roll a six-sided die for Urza\'s Science Fair Project.\n1 It gets -2/-2 until end of turn.\n2 It deals no combat damage this turn.\n3 Attacking does not cause it to tap this turn.\n4 It gains first strike until end of turn.\n5 It gains flying until end of turn.\n6 It gets +2/+2 until end of turn.').
card_first_print('urza\'s science fair project', 'UGL').
card_image_name('urza\'s science fair project'/'UGL', 'urza\'s science fair project').
card_uid('urza\'s science fair project'/'UGL', 'UGL:Urza\'s Science Fair Project:urza\'s science fair project').
card_rarity('urza\'s science fair project'/'UGL', 'Uncommon').
card_artist('urza\'s science fair project'/'UGL', 'Claymore J. Flapdoodle').
card_number('urza\'s science fair project'/'UGL', '83').
card_multiverse_id('urza\'s science fair project'/'UGL', '9731').

card_in_set('volrath\'s motion sensor', 'UGL').
card_original_type('volrath\'s motion sensor'/'UGL', 'Enchant Player').
card_original_text('volrath\'s motion sensor'/'UGL', 'When Volrath\'s Motion Sensor comes into play, choose target hand controlled by an opponent. Enchanted player balances Volrath\'s Motion Sensor on the back of that hand.\nIf Volrath\'s Motion Sensor falls off the hand, sacrifice Volrath\'s Motion Sensor and that player loses 3 life.').
card_first_print('volrath\'s motion sensor', 'UGL').
card_image_name('volrath\'s motion sensor'/'UGL', 'volrath\'s motion sensor').
card_uid('volrath\'s motion sensor'/'UGL', 'UGL:Volrath\'s Motion Sensor:volrath\'s motion sensor').
card_rarity('volrath\'s motion sensor'/'UGL', 'Uncommon').
card_artist('volrath\'s motion sensor'/'UGL', 'Mark Tedin').
card_number('volrath\'s motion sensor'/'UGL', '39').
card_multiverse_id('volrath\'s motion sensor'/'UGL', '9753').

card_in_set('zombie token card', 'UGL').
card_original_type('zombie token card'/'UGL', '(none)').
card_original_text('zombie token card'/'UGL', '').
card_first_print('zombie token card', 'UGL').
card_image_name('zombie token card'/'UGL', 'zombie').
card_uid('zombie token card'/'UGL', 'UGL:Zombie token card:zombie').
card_rarity('zombie token card'/'UGL', 'Uncommon').
card_artist('zombie token card'/'UGL', 'Christopher Rush').
card_number('zombie token card'/'UGL', '91').
card_multiverse_id('zombie token card'/'UGL', '5601').
