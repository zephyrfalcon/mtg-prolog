% Duel Decks: Heroes vs. Monsters

set('DDL').
set_name('DDL', 'Duel Decks: Heroes vs. Monsters').
set_release_date('DDL', '2013-09-06').
set_border('DDL', 'black').
set_type('DDL', 'duel deck').

card_in_set('anax and cymede', 'DDL').
card_original_type('anax and cymede'/'DDL', 'Legendary Creature — Human Soldier').
card_original_text('anax and cymede'/'DDL', 'First strike, vigilanceHeroic — Whenever you cast a spell that targets Anax and Cymede, creatures you control get +1/+1 and gain trample until end of turn.').
card_first_print('anax and cymede', 'DDL').
card_image_name('anax and cymede'/'DDL', 'anax and cymede').
card_uid('anax and cymede'/'DDL', 'DDL:Anax and Cymede:anax and cymede').
card_rarity('anax and cymede'/'DDL', 'Rare').
card_artist('anax and cymede'/'DDL', 'Willian Murai').
card_number('anax and cymede'/'DDL', '11').
card_flavor_text('anax and cymede'/'DDL', 'Akros\'s greatest heroes are also its royalty.').
card_multiverse_id('anax and cymede'/'DDL', '373396').

card_in_set('armory guard', 'DDL').
card_original_type('armory guard'/'DDL', 'Creature — Giant Soldier').
card_original_text('armory guard'/'DDL', 'Armory Guard has vigilance as long as you control a Gate.').
card_image_name('armory guard'/'DDL', 'armory guard').
card_uid('armory guard'/'DDL', 'DDL:Armory Guard:armory guard').
card_rarity('armory guard'/'DDL', 'Common').
card_artist('armory guard'/'DDL', 'Karl Kopinski').
card_number('armory guard'/'DDL', '12').
card_flavor_text('armory guard'/'DDL', 'The Dimir agents watched from the shadows. \"Eight hours, and I\'ve yet to see him blink,\" Nefara hissed. \"I suggest we find another way in.\"').
card_multiverse_id('armory guard'/'DDL', '373415').

card_in_set('auramancer', 'DDL').
card_original_type('auramancer'/'DDL', 'Creature — Human Wizard').
card_original_text('auramancer'/'DDL', 'When Auramancer enters the battlefield, you may return target enchantment card from your graveyard to your hand.').
card_image_name('auramancer'/'DDL', 'auramancer').
card_uid('auramancer'/'DDL', 'DDL:Auramancer:auramancer').
card_rarity('auramancer'/'DDL', 'Common').
card_artist('auramancer'/'DDL', 'Rebecca Guay').
card_number('auramancer'/'DDL', '9').
card_flavor_text('auramancer'/'DDL', '\"In memories, we can find our deepest reserves of strength.\"').
card_multiverse_id('auramancer'/'DDL', '373402').

card_in_set('battle mastery', 'DDL').
card_original_type('battle mastery'/'DDL', 'Enchantment — Aura').
card_original_text('battle mastery'/'DDL', 'Enchant creatureEnchanted creature has double strike. (It deals both first-strike and regular combat damage.)').
card_image_name('battle mastery'/'DDL', 'battle mastery').
card_uid('battle mastery'/'DDL', 'DDL:Battle Mastery:battle mastery').
card_rarity('battle mastery'/'DDL', 'Uncommon').
card_artist('battle mastery'/'DDL', 'Zoltan Boros & Gabor Szikszai').
card_number('battle mastery'/'DDL', '27').
card_flavor_text('battle mastery'/'DDL', '\"Boom! Boom! Boots the size of oxcarts, then an axe like a falling sun. Elves scattered. Trees scattered. Even the hills ran for the hills!\"\n—Clachan Tales').
card_multiverse_id('battle mastery'/'DDL', '373358').

card_in_set('beast within', 'DDL').
card_original_type('beast within'/'DDL', 'Instant').
card_original_text('beast within'/'DDL', 'Destroy target permanent. Its controller puts a 3/3 green Beast creature token onto the battlefield.').
card_image_name('beast within'/'DDL', 'beast within').
card_uid('beast within'/'DDL', 'DDL:Beast Within:beast within').
card_rarity('beast within'/'DDL', 'Uncommon').
card_artist('beast within'/'DDL', 'Jesper Ejsing').
card_number('beast within'/'DDL', '69').
card_flavor_text('beast within'/'DDL', '\"Monsters dwell in every heart. We only think civilization tames them.\"\n—Euphemia, Nexus watcher').
card_multiverse_id('beast within'/'DDL', '373352').

card_in_set('blood ogre', 'DDL').
card_original_type('blood ogre'/'DDL', 'Creature — Ogre Warrior').
card_original_text('blood ogre'/'DDL', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)First strike').
card_image_name('blood ogre'/'DDL', 'blood ogre').
card_uid('blood ogre'/'DDL', 'DDL:Blood Ogre:blood ogre').
card_rarity('blood ogre'/'DDL', 'Common').
card_artist('blood ogre'/'DDL', 'Christopher Moeller').
card_number('blood ogre'/'DDL', '49').
card_multiverse_id('blood ogre'/'DDL', '373383').

card_in_set('bonds of faith', 'DDL').
card_original_type('bonds of faith'/'DDL', 'Enchantment — Aura').
card_original_text('bonds of faith'/'DDL', 'Enchant creatureEnchanted creature gets +2/+2 as long as it\'s a Human. Otherwise, it can\'t attack or block.').
card_image_name('bonds of faith'/'DDL', 'bonds of faith').
card_uid('bonds of faith'/'DDL', 'DDL:Bonds of Faith:bonds of faith').
card_rarity('bonds of faith'/'DDL', 'Common').
card_artist('bonds of faith'/'DDL', 'Steve Argyle').
card_number('bonds of faith'/'DDL', '24').
card_flavor_text('bonds of faith'/'DDL', '\"What cannot be destroyed will be bound.\"\n—Oath of Avacyn').
card_multiverse_id('bonds of faith'/'DDL', '373354').

card_in_set('boros guildgate', 'DDL').
card_original_type('boros guildgate'/'DDL', 'Land — Gate').
card_original_text('boros guildgate'/'DDL', 'Boros Guildgate enters the battlefield tapped.{T}: Add {R} or {W} to your mana pool.').
card_image_name('boros guildgate'/'DDL', 'boros guildgate').
card_uid('boros guildgate'/'DDL', 'DDL:Boros Guildgate:boros guildgate').
card_rarity('boros guildgate'/'DDL', 'Common').
card_artist('boros guildgate'/'DDL', 'Noah Bradley').
card_number('boros guildgate'/'DDL', '33').
card_flavor_text('boros guildgate'/'DDL', 'Enter with merciful hearts. Exit with battle cries.').
card_multiverse_id('boros guildgate'/'DDL', '373389').

card_in_set('cavalry pegasus', 'DDL').
card_original_type('cavalry pegasus'/'DDL', 'Creature — Pegasus').
card_original_text('cavalry pegasus'/'DDL', 'FlyingWhenever Cavalry Pegasus attacks, each attacking Human gains flying until end of turn.').
card_first_print('cavalry pegasus', 'DDL').
card_image_name('cavalry pegasus'/'DDL', 'cavalry pegasus').
card_uid('cavalry pegasus'/'DDL', 'DDL:Cavalry Pegasus:cavalry pegasus').
card_rarity('cavalry pegasus'/'DDL', 'Common').
card_artist('cavalry pegasus'/'DDL', 'Kev Walker').
card_number('cavalry pegasus'/'DDL', '4').
card_flavor_text('cavalry pegasus'/'DDL', '\"It is hope, hooved and winged.\"\n—Cymede, queen of Akros').
card_multiverse_id('cavalry pegasus'/'DDL', '373353').

card_in_set('condemn', 'DDL').
card_original_type('condemn'/'DDL', 'Instant').
card_original_text('condemn'/'DDL', 'Put target attacking creature on the bottom of its owner\'s library. Its controller gains life equal to its toughness.').
card_image_name('condemn'/'DDL', 'condemn').
card_uid('condemn'/'DDL', 'DDL:Condemn:condemn').
card_rarity('condemn'/'DDL', 'Uncommon').
card_artist('condemn'/'DDL', 'Daren Bader').
card_number('condemn'/'DDL', '17').
card_flavor_text('condemn'/'DDL', '\"No doubt the arbiters would put you away, after all the documents are signed. But I will have justice now.\"\n—Alovnek, Boros guildmage').
card_multiverse_id('condemn'/'DDL', '373407').

card_in_set('conquering manticore', 'DDL').
card_original_type('conquering manticore'/'DDL', 'Creature — Manticore').
card_original_text('conquering manticore'/'DDL', 'FlyingWhen Conquering Manticore enters the battlefield, gain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn.').
card_image_name('conquering manticore'/'DDL', 'conquering manticore').
card_uid('conquering manticore'/'DDL', 'DDL:Conquering Manticore:conquering manticore').
card_rarity('conquering manticore'/'DDL', 'Rare').
card_artist('conquering manticore'/'DDL', 'Chris Rahn').
card_number('conquering manticore'/'DDL', '55').
card_flavor_text('conquering manticore'/'DDL', 'It speaks no commands but is always obeyed.').
card_multiverse_id('conquering manticore'/'DDL', '373366').

card_in_set('crater hellion', 'DDL').
card_original_type('crater hellion'/'DDL', 'Creature — Hellion Beast').
card_original_text('crater hellion'/'DDL', 'Echo {4}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)When Crater Hellion enters the battlefield, it deals 4 damage to each other creature.').
card_image_name('crater hellion'/'DDL', 'crater hellion').
card_uid('crater hellion'/'DDL', 'DDL:Crater Hellion:crater hellion').
card_rarity('crater hellion'/'DDL', 'Rare').
card_artist('crater hellion'/'DDL', 'Daren Bader').
card_number('crater hellion'/'DDL', '56').
card_multiverse_id('crater hellion'/'DDL', '373345').

card_in_set('crowned ceratok', 'DDL').
card_original_type('crowned ceratok'/'DDL', 'Creature — Rhino').
card_original_text('crowned ceratok'/'DDL', 'TrampleEach creature you control with a +1/+1 counter on it has trample.').
card_image_name('crowned ceratok'/'DDL', 'crowned ceratok').
card_uid('crowned ceratok'/'DDL', 'DDL:Crowned Ceratok:crowned ceratok').
card_rarity('crowned ceratok'/'DDL', 'Uncommon').
card_artist('crowned ceratok'/'DDL', 'Steve Prescott').
card_number('crowned ceratok'/'DDL', '51').
card_flavor_text('crowned ceratok'/'DDL', 'The best way to calm a rampaging ceratok is to wait until it dies of natural causes.').
card_multiverse_id('crowned ceratok'/'DDL', '373409').

card_in_set('daily regimen', 'DDL').
card_original_type('daily regimen'/'DDL', 'Enchantment — Aura').
card_original_text('daily regimen'/'DDL', 'Enchant creature{1}{W}: Put a +1/+1 counter on enchanted creature.').
card_image_name('daily regimen'/'DDL', 'daily regimen').
card_uid('daily regimen'/'DDL', 'DDL:Daily Regimen:daily regimen').
card_rarity('daily regimen'/'DDL', 'Uncommon').
card_artist('daily regimen'/'DDL', 'Warren Mahy').
card_number('daily regimen'/'DDL', '18').
card_flavor_text('daily regimen'/'DDL', 'What self-indulgence tears down, discipline builds up again.').
card_multiverse_id('daily regimen'/'DDL', '373360').

card_in_set('dawnstrike paladin', 'DDL').
card_original_type('dawnstrike paladin'/'DDL', 'Creature — Human Knight').
card_original_text('dawnstrike paladin'/'DDL', 'VigilanceLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('dawnstrike paladin'/'DDL', 'dawnstrike paladin').
card_uid('dawnstrike paladin'/'DDL', 'DDL:Dawnstrike Paladin:dawnstrike paladin').
card_rarity('dawnstrike paladin'/'DDL', 'Common').
card_artist('dawnstrike paladin'/'DDL', 'Tyler Jacobson').
card_number('dawnstrike paladin'/'DDL', '14').
card_flavor_text('dawnstrike paladin'/'DDL', 'She crushes darkness beneath her charger\'s hooves.').
card_multiverse_id('dawnstrike paladin'/'DDL', '373371').

card_in_set('deadly recluse', 'DDL').
card_original_type('deadly recluse'/'DDL', 'Creature — Spider').
card_original_text('deadly recluse'/'DDL', 'Reach (This creature can block creatures with flying.)Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('deadly recluse'/'DDL', 'deadly recluse').
card_uid('deadly recluse'/'DDL', 'DDL:Deadly Recluse:deadly recluse').
card_rarity('deadly recluse'/'DDL', 'Common').
card_artist('deadly recluse'/'DDL', 'Warren Mahy').
card_number('deadly recluse'/'DDL', '45').
card_flavor_text('deadly recluse'/'DDL', 'Even dragons fear its silken strands.').
card_multiverse_id('deadly recluse'/'DDL', '373350').

card_in_set('destructive revelry', 'DDL').
card_original_type('destructive revelry'/'DDL', 'Instant').
card_original_text('destructive revelry'/'DDL', 'Destroy target artifact or enchantment. Destructive Revelry deals 2 damage to that permanent\'s controller.').
card_first_print('destructive revelry', 'DDL').
card_image_name('destructive revelry'/'DDL', 'destructive revelry').
card_uid('destructive revelry'/'DDL', 'DDL:Destructive Revelry:destructive revelry').
card_rarity('destructive revelry'/'DDL', 'Uncommon').
card_artist('destructive revelry'/'DDL', 'Kev Walker').
card_number('destructive revelry'/'DDL', '66').
card_flavor_text('destructive revelry'/'DDL', '\"Stoke a fire hot enough and you\'ll never run out of things to burn.\"\n—Xenagos, the Reveler').
card_multiverse_id('destructive revelry'/'DDL', '373351').

card_in_set('deus of calamity', 'DDL').
card_original_type('deus of calamity'/'DDL', 'Creature — Spirit Avatar').
card_original_text('deus of calamity'/'DDL', 'TrampleWhenever Deus of Calamity deals 6 or more damage to an opponent, destroy target land that player controls.').
card_image_name('deus of calamity'/'DDL', 'deus of calamity').
card_uid('deus of calamity'/'DDL', 'DDL:Deus of Calamity:deus of calamity').
card_rarity('deus of calamity'/'DDL', 'Rare').
card_artist('deus of calamity'/'DDL', 'Todd Lockwood').
card_number('deus of calamity'/'DDL', '54').
card_flavor_text('deus of calamity'/'DDL', '\"He bears the marks of ages upon his skin, memories of dreams long dead and best left buried.\"\n—The Seer\'s Parables').
card_multiverse_id('deus of calamity'/'DDL', '373363').

card_in_set('dragon blood', 'DDL').
card_original_type('dragon blood'/'DDL', 'Artifact').
card_original_text('dragon blood'/'DDL', '{3}, {T}: Put a +1/+1 counter on target creature.').
card_image_name('dragon blood'/'DDL', 'dragon blood').
card_uid('dragon blood'/'DDL', 'DDL:Dragon Blood:dragon blood').
card_rarity('dragon blood'/'DDL', 'Uncommon').
card_artist('dragon blood'/'DDL', 'Ron Spencer').
card_number('dragon blood'/'DDL', '67').
card_flavor_text('dragon blood'/'DDL', 'A single drop turns skin to scale and fist to claw.').
card_multiverse_id('dragon blood'/'DDL', '373362').

card_in_set('fencing ace', 'DDL').
card_original_type('fencing ace'/'DDL', 'Creature — Human Soldier').
card_original_text('fencing ace'/'DDL', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_image_name('fencing ace'/'DDL', 'fencing ace').
card_uid('fencing ace'/'DDL', 'DDL:Fencing Ace:fencing ace').
card_rarity('fencing ace'/'DDL', 'Uncommon').
card_artist('fencing ace'/'DDL', 'David Rapoza').
card_number('fencing ace'/'DDL', '5').
card_flavor_text('fencing ace'/'DDL', 'His prowess gives the guildless hope that they can hold out against tyranny.').
card_multiverse_id('fencing ace'/'DDL', '373367').

card_in_set('figure of destiny', 'DDL').
card_original_type('figure of destiny'/'DDL', 'Creature — Kithkin').
card_original_text('figure of destiny'/'DDL', '{R/W}: Figure of Destiny becomes a 2/2 Kithkin Spirit.{R/W}{R/W}{R/W}: If Figure of Destiny is a Spirit, it becomes a 4/4 Kithkin Spirit Warrior.{R/W}{R/W}{R/W}{R/W}{R/W}{R/W}: If Figure of Destiny is a Warrior, it becomes an 8/8 Kithkin Spirit Warrior Avatar with flying and first strike.').
card_image_name('figure of destiny'/'DDL', 'figure of destiny').
card_uid('figure of destiny'/'DDL', 'DDL:Figure of Destiny:figure of destiny').
card_rarity('figure of destiny'/'DDL', 'Rare').
card_artist('figure of destiny'/'DDL', 'Scott M. Fischer').
card_number('figure of destiny'/'DDL', '3').
card_multiverse_id('figure of destiny'/'DDL', '373388').

card_in_set('fires of yavimaya', 'DDL').
card_original_type('fires of yavimaya'/'DDL', 'Enchantment').
card_original_text('fires of yavimaya'/'DDL', 'Creatures you control have haste.Sacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_image_name('fires of yavimaya'/'DDL', 'fires of yavimaya').
card_uid('fires of yavimaya'/'DDL', 'DDL:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'DDL', 'Uncommon').
card_artist('fires of yavimaya'/'DDL', 'Izzy').
card_number('fires of yavimaya'/'DDL', '70').
card_flavor_text('fires of yavimaya'/'DDL', 'Yavimaya lights the quickest path to battle.').
card_multiverse_id('fires of yavimaya'/'DDL', '373397').

card_in_set('forest', 'DDL').
card_original_type('forest'/'DDL', 'Basic Land — Forest').
card_original_text('forest'/'DDL', 'G').
card_image_name('forest'/'DDL', 'forest1').
card_uid('forest'/'DDL', 'DDL:Forest:forest1').
card_rarity('forest'/'DDL', 'Basic Land').
card_artist('forest'/'DDL', 'John Avon').
card_number('forest'/'DDL', '78').
card_multiverse_id('forest'/'DDL', '373349').

card_in_set('forest', 'DDL').
card_original_type('forest'/'DDL', 'Basic Land — Forest').
card_original_text('forest'/'DDL', 'G').
card_image_name('forest'/'DDL', 'forest2').
card_uid('forest'/'DDL', 'DDL:Forest:forest2').
card_rarity('forest'/'DDL', 'Basic Land').
card_artist('forest'/'DDL', 'Chippy').
card_number('forest'/'DDL', '79').
card_multiverse_id('forest'/'DDL', '373375').

card_in_set('forest', 'DDL').
card_original_type('forest'/'DDL', 'Basic Land — Forest').
card_original_text('forest'/'DDL', 'G').
card_image_name('forest'/'DDL', 'forest3').
card_uid('forest'/'DDL', 'DDL:Forest:forest3').
card_rarity('forest'/'DDL', 'Basic Land').
card_artist('forest'/'DDL', 'Jonas De Ro').
card_number('forest'/'DDL', '80').
card_multiverse_id('forest'/'DDL', '373404').

card_in_set('forest', 'DDL').
card_original_type('forest'/'DDL', 'Basic Land — Forest').
card_original_text('forest'/'DDL', 'G').
card_image_name('forest'/'DDL', 'forest4').
card_uid('forest'/'DDL', 'DDL:Forest:forest4').
card_rarity('forest'/'DDL', 'Basic Land').
card_artist('forest'/'DDL', 'Ryan Pancoast').
card_number('forest'/'DDL', '81').
card_multiverse_id('forest'/'DDL', '373374').

card_in_set('freewind equenaut', 'DDL').
card_original_type('freewind equenaut'/'DDL', 'Creature — Human Archer').
card_original_text('freewind equenaut'/'DDL', 'FlyingAs long as Freewind Equenaut is enchanted, it has \"{T}: Freewind Equenaut deals 2 damage to target attacking or blocking creature.\"').
card_image_name('freewind equenaut'/'DDL', 'freewind equenaut').
card_uid('freewind equenaut'/'DDL', 'DDL:Freewind Equenaut:freewind equenaut').
card_rarity('freewind equenaut'/'DDL', 'Common').
card_artist('freewind equenaut'/'DDL', 'Rebecca Guay').
card_number('freewind equenaut'/'DDL', '10').
card_flavor_text('freewind equenaut'/'DDL', 'Confront her, and feel the hooves of her steed. Ignore her, and feel the sting of her arrow.').
card_multiverse_id('freewind equenaut'/'DDL', '373376').

card_in_set('ghor-clan savage', 'DDL').
card_original_type('ghor-clan savage'/'DDL', 'Creature — Centaur Berserker').
card_original_text('ghor-clan savage'/'DDL', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)').
card_image_name('ghor-clan savage'/'DDL', 'ghor-clan savage').
card_uid('ghor-clan savage'/'DDL', 'DDL:Ghor-Clan Savage:ghor-clan savage').
card_rarity('ghor-clan savage'/'DDL', 'Common').
card_artist('ghor-clan savage'/'DDL', 'Greg Staples').
card_number('ghor-clan savage'/'DDL', '53').
card_flavor_text('ghor-clan savage'/'DDL', 'The snap of sinew. The crunch of bone. A cry for mercy. These are the music of the Gruul.').
card_multiverse_id('ghor-clan savage'/'DDL', '373373').

card_in_set('gorehorn minotaurs', 'DDL').
card_original_type('gorehorn minotaurs'/'DDL', 'Creature — Minotaur Warrior').
card_original_text('gorehorn minotaurs'/'DDL', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)').
card_image_name('gorehorn minotaurs'/'DDL', 'gorehorn minotaurs').
card_uid('gorehorn minotaurs'/'DDL', 'DDL:Gorehorn Minotaurs:gorehorn minotaurs').
card_rarity('gorehorn minotaurs'/'DDL', 'Common').
card_artist('gorehorn minotaurs'/'DDL', 'Wayne Reynolds').
card_number('gorehorn minotaurs'/'DDL', '52').
card_flavor_text('gorehorn minotaurs'/'DDL', 'Some of the eleven minotaur clans of Mirtiin are expert crafters and learned philosophers. Others just like to hit stuff.').
card_multiverse_id('gorehorn minotaurs'/'DDL', '373398').

card_in_set('griffin guide', 'DDL').
card_original_type('griffin guide'/'DDL', 'Enchantment — Aura').
card_original_text('griffin guide'/'DDL', 'Enchant creatureEnchanted creature gets +2/+2 and has flying.When enchanted creature dies, put a 2/2 white Griffin creature token with flying onto the battlefield.').
card_image_name('griffin guide'/'DDL', 'griffin guide').
card_uid('griffin guide'/'DDL', 'DDL:Griffin Guide:griffin guide').
card_rarity('griffin guide'/'DDL', 'Uncommon').
card_artist('griffin guide'/'DDL', 'Johann Bodin').
card_number('griffin guide'/'DDL', '28').
card_multiverse_id('griffin guide'/'DDL', '373337').

card_in_set('gustcloak sentinel', 'DDL').
card_original_type('gustcloak sentinel'/'DDL', 'Creature — Human Soldier').
card_original_text('gustcloak sentinel'/'DDL', 'Whenever Gustcloak Sentinel becomes blocked, you may untap it and remove it from combat.').
card_image_name('gustcloak sentinel'/'DDL', 'gustcloak sentinel').
card_uid('gustcloak sentinel'/'DDL', 'DDL:Gustcloak Sentinel:gustcloak sentinel').
card_rarity('gustcloak sentinel'/'DDL', 'Uncommon').
card_artist('gustcloak sentinel'/'DDL', 'Mark Zug').
card_number('gustcloak sentinel'/'DDL', '13').
card_flavor_text('gustcloak sentinel'/'DDL', 'Entire platoons have mysteriously vanished from battle, leaving enemy weapons to slice through empty air.').
card_multiverse_id('gustcloak sentinel'/'DDL', '373364').

card_in_set('kamahl, pit fighter', 'DDL').
card_original_type('kamahl, pit fighter'/'DDL', 'Legendary Creature — Human Barbarian').
card_original_text('kamahl, pit fighter'/'DDL', 'Haste{T}: Kamahl, Pit Fighter deals 3 damage to target creature or player.').
card_image_name('kamahl, pit fighter'/'DDL', 'kamahl, pit fighter').
card_uid('kamahl, pit fighter'/'DDL', 'DDL:Kamahl, Pit Fighter:kamahl, pit fighter').
card_rarity('kamahl, pit fighter'/'DDL', 'Rare').
card_artist('kamahl, pit fighter'/'DDL', 'Kev Walker').
card_number('kamahl, pit fighter'/'DDL', '16').
card_flavor_text('kamahl, pit fighter'/'DDL', 'In times when freedom seems lost, great souls arise to reclaim it.').
card_multiverse_id('kamahl, pit fighter'/'DDL', '373338').

card_in_set('kavu predator', 'DDL').
card_original_type('kavu predator'/'DDL', 'Creature — Kavu').
card_original_text('kavu predator'/'DDL', 'TrampleWhenever an opponent gains life, put that many +1/+1 counters on Kavu Predator.').
card_image_name('kavu predator'/'DDL', 'kavu predator').
card_uid('kavu predator'/'DDL', 'DDL:Kavu Predator:kavu predator').
card_rarity('kavu predator'/'DDL', 'Uncommon').
card_artist('kavu predator'/'DDL', 'Dan Scott').
card_number('kavu predator'/'DDL', '46').
card_flavor_text('kavu predator'/'DDL', 'In a withered world, the scent of healthy prey is enough to drive a predator to frenzy.').
card_multiverse_id('kavu predator'/'DDL', '373377').

card_in_set('kazandu refuge', 'DDL').
card_original_type('kazandu refuge'/'DDL', 'Land').
card_original_text('kazandu refuge'/'DDL', 'Kazandu Refuge enters the battlefield tapped.When Kazandu Refuge enters the battlefield, you gain 1 life.{T}: Add {R} or {G} to your mana pool.').
card_image_name('kazandu refuge'/'DDL', 'kazandu refuge').
card_uid('kazandu refuge'/'DDL', 'DDL:Kazandu Refuge:kazandu refuge').
card_rarity('kazandu refuge'/'DDL', 'Uncommon').
card_artist('kazandu refuge'/'DDL', 'Franz Vohwinkel').
card_number('kazandu refuge'/'DDL', '71').
card_multiverse_id('kazandu refuge'/'DDL', '373412').

card_in_set('krosan tusker', 'DDL').
card_original_type('krosan tusker'/'DDL', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'DDL', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)When you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('krosan tusker'/'DDL', 'krosan tusker').
card_uid('krosan tusker'/'DDL', 'DDL:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'DDL', 'Common').
card_artist('krosan tusker'/'DDL', 'Kev Walker').
card_number('krosan tusker'/'DDL', '59').
card_multiverse_id('krosan tusker'/'DDL', '373394').

card_in_set('llanowar reborn', 'DDL').
card_original_type('llanowar reborn'/'DDL', 'Land').
card_original_text('llanowar reborn'/'DDL', 'Llanowar Reborn enters the battlefield tapped.{T}: Add {G} to your mana pool.Graft 1 (This land enters the battlefield with a +1/+1 counter on it. Whenever a creature enters the battlefield, you may move a +1/+1 counter from this land onto it.)').
card_image_name('llanowar reborn'/'DDL', 'llanowar reborn').
card_uid('llanowar reborn'/'DDL', 'DDL:Llanowar Reborn:llanowar reborn').
card_rarity('llanowar reborn'/'DDL', 'Uncommon').
card_artist('llanowar reborn'/'DDL', 'Philip Straub').
card_number('llanowar reborn'/'DDL', '72').
card_multiverse_id('llanowar reborn'/'DDL', '373368').

card_in_set('magma jet', 'DDL').
card_original_type('magma jet'/'DDL', 'Instant').
card_original_text('magma jet'/'DDL', 'Magma Jet deals 2 damage to target creature or player. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('magma jet'/'DDL', 'magma jet').
card_uid('magma jet'/'DDL', 'DDL:Magma Jet:magma jet').
card_rarity('magma jet'/'DDL', 'Uncommon').
card_artist('magma jet'/'DDL', 'Justin Sweet').
card_number('magma jet'/'DDL', '22').
card_multiverse_id('magma jet'/'DDL', '373400').

card_in_set('miraculous recovery', 'DDL').
card_original_type('miraculous recovery'/'DDL', 'Instant').
card_original_text('miraculous recovery'/'DDL', 'Return target creature card from your graveyard to the battlefield. Put a +1/+1 counter on it.').
card_image_name('miraculous recovery'/'DDL', 'miraculous recovery').
card_uid('miraculous recovery'/'DDL', 'DDL:Miraculous Recovery:miraculous recovery').
card_rarity('miraculous recovery'/'DDL', 'Uncommon').
card_artist('miraculous recovery'/'DDL', 'Mark Winters').
card_number('miraculous recovery'/'DDL', '30').
card_flavor_text('miraculous recovery'/'DDL', '\"I am the rock of the polis. Death only strengthens me.\"').
card_multiverse_id('miraculous recovery'/'DDL', '373370').

card_in_set('moment of heroism', 'DDL').
card_original_type('moment of heroism'/'DDL', 'Instant').
card_original_text('moment of heroism'/'DDL', 'Target creature gets +2/+2 and gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_image_name('moment of heroism'/'DDL', 'moment of heroism').
card_uid('moment of heroism'/'DDL', 'DDL:Moment of Heroism:moment of heroism').
card_rarity('moment of heroism'/'DDL', 'Common').
card_artist('moment of heroism'/'DDL', 'Christopher Moeller').
card_number('moment of heroism'/'DDL', '25').
card_flavor_text('moment of heroism'/'DDL', '\"My faith is stronger than fang, claw, or mindless hunger.\"').
card_multiverse_id('moment of heroism'/'DDL', '373344').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain1').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain1').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'Cliff Childs').
card_number('mountain'/'DDL', '35').
card_multiverse_id('mountain'/'DDL', '373387').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain2').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain2').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'Jonas De Ro').
card_number('mountain'/'DDL', '36').
card_multiverse_id('mountain'/'DDL', '373382').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain3').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain3').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'Karl Kopinski').
card_number('mountain'/'DDL', '37').
card_multiverse_id('mountain'/'DDL', '373346').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain4').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain4').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'Adam Paquette').
card_number('mountain'/'DDL', '38').
card_multiverse_id('mountain'/'DDL', '373411').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain5').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain5').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'John Avon').
card_number('mountain'/'DDL', '74').
card_multiverse_id('mountain'/'DDL', '373392').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain6').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain6').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'D. Alexander Gregory').
card_number('mountain'/'DDL', '75').
card_multiverse_id('mountain'/'DDL', '373359').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain7').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain7').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'Dave Kendall').
card_number('mountain'/'DDL', '76').
card_multiverse_id('mountain'/'DDL', '373403').

card_in_set('mountain', 'DDL').
card_original_type('mountain'/'DDL', 'Basic Land — Mountain').
card_original_text('mountain'/'DDL', 'R').
card_image_name('mountain'/'DDL', 'mountain8').
card_uid('mountain'/'DDL', 'DDL:Mountain:mountain8').
card_rarity('mountain'/'DDL', 'Basic Land').
card_artist('mountain'/'DDL', 'Andreas Rocha').
card_number('mountain'/'DDL', '77').
card_multiverse_id('mountain'/'DDL', '373378').

card_in_set('new benalia', 'DDL').
card_original_type('new benalia'/'DDL', 'Land').
card_original_text('new benalia'/'DDL', 'New Benalia enters the battlefield tapped.When New Benalia enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.){T}: Add {W} to your mana pool.').
card_image_name('new benalia'/'DDL', 'new benalia').
card_uid('new benalia'/'DDL', 'DDL:New Benalia:new benalia').
card_rarity('new benalia'/'DDL', 'Uncommon').
card_artist('new benalia'/'DDL', 'Richard Wright').
card_number('new benalia'/'DDL', '34').
card_multiverse_id('new benalia'/'DDL', '373401').

card_in_set('nobilis of war', 'DDL').
card_original_type('nobilis of war'/'DDL', 'Creature — Spirit Avatar').
card_original_text('nobilis of war'/'DDL', 'FlyingAttacking creatures you control get +2/+0.').
card_image_name('nobilis of war'/'DDL', 'nobilis of war').
card_uid('nobilis of war'/'DDL', 'DDL:Nobilis of War:nobilis of war').
card_rarity('nobilis of war'/'DDL', 'Rare').
card_artist('nobilis of war'/'DDL', 'Christopher Moeller').
card_number('nobilis of war'/'DDL', '15').
card_flavor_text('nobilis of war'/'DDL', '\"A great siege is a banquet to him; a long and terrible battle, the most exquisite delicacy.\"\n—The Seer\'s Parables').
card_multiverse_id('nobilis of war'/'DDL', '373336').

card_in_set('orcish lumberjack', 'DDL').
card_original_type('orcish lumberjack'/'DDL', 'Creature — Orc').
card_original_text('orcish lumberjack'/'DDL', '{T}, Sacrifice a Forest: Add three mana in any combination of {R} and/or {G} to your mana pool.').
card_image_name('orcish lumberjack'/'DDL', 'orcish lumberjack').
card_uid('orcish lumberjack'/'DDL', 'DDL:Orcish Lumberjack:orcish lumberjack').
card_rarity('orcish lumberjack'/'DDL', 'Common').
card_artist('orcish lumberjack'/'DDL', 'Steve Prescott').
card_number('orcish lumberjack'/'DDL', '44').
card_flavor_text('orcish lumberjack'/'DDL', '\"Wood is wasted in the dirt.\"\n—Orcish proverb').
card_multiverse_id('orcish lumberjack'/'DDL', '373399').

card_in_set('ordeal of purphoros', 'DDL').
card_original_type('ordeal of purphoros'/'DDL', 'Enchantment — Aura').
card_original_text('ordeal of purphoros'/'DDL', 'Enchant creatureWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Purphoros.When you sacrifice Ordeal of Purphoros, it deals 3 damage to target creature or player.').
card_first_print('ordeal of purphoros', 'DDL').
card_image_name('ordeal of purphoros'/'DDL', 'ordeal of purphoros').
card_uid('ordeal of purphoros'/'DDL', 'DDL:Ordeal of Purphoros:ordeal of purphoros').
card_rarity('ordeal of purphoros'/'DDL', 'Uncommon').
card_artist('ordeal of purphoros'/'DDL', 'Maciej Kuciara').
card_number('ordeal of purphoros'/'DDL', '23').
card_multiverse_id('ordeal of purphoros'/'DDL', '373406').

card_in_set('pay no heed', 'DDL').
card_original_type('pay no heed'/'DDL', 'Instant').
card_original_text('pay no heed'/'DDL', 'Prevent all damage a source of your choice would deal this turn.').
card_image_name('pay no heed'/'DDL', 'pay no heed').
card_uid('pay no heed'/'DDL', 'DDL:Pay No Heed:pay no heed').
card_rarity('pay no heed'/'DDL', 'Common').
card_artist('pay no heed'/'DDL', 'Adam Rex').
card_number('pay no heed'/'DDL', '19').
card_flavor_text('pay no heed'/'DDL', '\"My course is before me. There is nothing behind me.\"').
card_multiverse_id('pay no heed'/'DDL', '373405').

card_in_set('plains', 'DDL').
card_original_type('plains'/'DDL', 'Basic Land — Plains').
card_original_text('plains'/'DDL', 'W').
card_image_name('plains'/'DDL', 'plains1').
card_uid('plains'/'DDL', 'DDL:Plains:plains1').
card_rarity('plains'/'DDL', 'Basic Land').
card_artist('plains'/'DDL', 'Rob Alexander').
card_number('plains'/'DDL', '39').
card_multiverse_id('plains'/'DDL', '373341').

card_in_set('plains', 'DDL').
card_original_type('plains'/'DDL', 'Basic Land — Plains').
card_original_text('plains'/'DDL', 'W').
card_image_name('plains'/'DDL', 'plains2').
card_uid('plains'/'DDL', 'DDL:Plains:plains2').
card_rarity('plains'/'DDL', 'Basic Land').
card_artist('plains'/'DDL', 'John Avon').
card_number('plains'/'DDL', '40').
card_multiverse_id('plains'/'DDL', '373369').

card_in_set('plains', 'DDL').
card_original_type('plains'/'DDL', 'Basic Land — Plains').
card_original_text('plains'/'DDL', 'W').
card_image_name('plains'/'DDL', 'plains3').
card_uid('plains'/'DDL', 'DDL:Plains:plains3').
card_rarity('plains'/'DDL', 'Basic Land').
card_artist('plains'/'DDL', 'Michael Komarck').
card_number('plains'/'DDL', '41').
card_multiverse_id('plains'/'DDL', '373386').

card_in_set('plains', 'DDL').
card_original_type('plains'/'DDL', 'Basic Land — Plains').
card_original_text('plains'/'DDL', 'W').
card_image_name('plains'/'DDL', 'plains4').
card_uid('plains'/'DDL', 'DDL:Plains:plains4').
card_rarity('plains'/'DDL', 'Basic Land').
card_artist('plains'/'DDL', 'Adam Paquette').
card_number('plains'/'DDL', '42').
card_multiverse_id('plains'/'DDL', '373361').

card_in_set('polukranos, world eater', 'DDL').
card_original_type('polukranos, world eater'/'DDL', 'Legendary Creature — Hydra').
card_original_text('polukranos, world eater'/'DDL', '{X}{X}{G}: Monstrosity X. (If this creature isn\'t monstrous, put X +1/+1 counters on it and it becomes monstrous.)When Polukranos, World Eater becomes monstrous, it deals X damage divided as you choose among any number of target creatures your opponents control. Each of those creatures deals damage equal to its power to Polukranos.').
card_first_print('polukranos, world eater', 'DDL').
card_image_name('polukranos, world eater'/'DDL', 'polukranos, world eater').
card_uid('polukranos, world eater'/'DDL', 'DDL:Polukranos, World Eater:polukranos, world eater').
card_rarity('polukranos, world eater'/'DDL', 'Mythic Rare').
card_artist('polukranos, world eater'/'DDL', 'Karl Kopinski').
card_number('polukranos, world eater'/'DDL', '43').
card_multiverse_id('polukranos, world eater'/'DDL', '373384').

card_in_set('prey upon', 'DDL').
card_original_type('prey upon'/'DDL', 'Sorcery').
card_original_text('prey upon'/'DDL', 'Target creature you control fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_image_name('prey upon'/'DDL', 'prey upon').
card_uid('prey upon'/'DDL', 'DDL:Prey Upon:prey upon').
card_rarity('prey upon'/'DDL', 'Common').
card_artist('prey upon'/'DDL', 'Dave Kendall').
card_number('prey upon'/'DDL', '62').
card_flavor_text('prey upon'/'DDL', '\"You don\'t find many old werewolf hunters.\"\n—Paulin, trapper of Somberwald').
card_multiverse_id('prey upon'/'DDL', '373348').

card_in_set('pyroclasm', 'DDL').
card_original_type('pyroclasm'/'DDL', 'Sorcery').
card_original_text('pyroclasm'/'DDL', 'Pyroclasm deals 2 damage to each creature.').
card_image_name('pyroclasm'/'DDL', 'pyroclasm').
card_uid('pyroclasm'/'DDL', 'DDL:Pyroclasm:pyroclasm').
card_rarity('pyroclasm'/'DDL', 'Uncommon').
card_artist('pyroclasm'/'DDL', 'John Avon').
card_number('pyroclasm'/'DDL', '63').
card_flavor_text('pyroclasm'/'DDL', '\"Who\'d want to ignite things one at a time?\"\n—Chandra Nalaar').
card_multiverse_id('pyroclasm'/'DDL', '373365').

card_in_set('pyrokinesis', 'DDL').
card_original_type('pyrokinesis'/'DDL', 'Instant').
card_original_text('pyrokinesis'/'DDL', 'You may exile a red card from your hand rather than pay Pyrokinesis\'s mana cost.Pyrokinesis deals 4 damage divided as you choose among any number of target creatures.').
card_image_name('pyrokinesis'/'DDL', 'pyrokinesis').
card_uid('pyrokinesis'/'DDL', 'DDL:Pyrokinesis:pyrokinesis').
card_rarity('pyrokinesis'/'DDL', 'Uncommon').
card_artist('pyrokinesis'/'DDL', 'Igor Kieryluk').
card_number('pyrokinesis'/'DDL', '32').
card_multiverse_id('pyrokinesis'/'DDL', '373356').

card_in_set('regrowth', 'DDL').
card_original_type('regrowth'/'DDL', 'Sorcery').
card_original_text('regrowth'/'DDL', 'Return target card from your graveyard to your hand.').
card_image_name('regrowth'/'DDL', 'regrowth').
card_uid('regrowth'/'DDL', 'DDL:Regrowth:regrowth').
card_rarity('regrowth'/'DDL', 'Uncommon').
card_artist('regrowth'/'DDL', 'Dan Scott').
card_number('regrowth'/'DDL', '64').
card_flavor_text('regrowth'/'DDL', 'While the root remains, the tree yet lives.').
card_multiverse_id('regrowth'/'DDL', '373408').

card_in_set('righteousness', 'DDL').
card_original_type('righteousness'/'DDL', 'Instant').
card_original_text('righteousness'/'DDL', 'Target blocking creature gets +7/+7 until end of turn.').
card_image_name('righteousness'/'DDL', 'righteousness').
card_uid('righteousness'/'DDL', 'DDL:Righteousness:righteousness').
card_rarity('righteousness'/'DDL', 'Uncommon').
card_artist('righteousness'/'DDL', 'Wayne England').
card_number('righteousness'/'DDL', '20').
card_flavor_text('righteousness'/'DDL', 'Sometimes the greatest strength is the strength of conviction.').
card_multiverse_id('righteousness'/'DDL', '373381').

card_in_set('satyr hedonist', 'DDL').
card_original_type('satyr hedonist'/'DDL', 'Creature — Satyr').
card_original_text('satyr hedonist'/'DDL', '{R}, Sacrifice Satyr Hedonist: Add {R}{R}{R} to your mana pool.').
card_first_print('satyr hedonist', 'DDL').
card_image_name('satyr hedonist'/'DDL', 'satyr hedonist').
card_uid('satyr hedonist'/'DDL', 'DDL:Satyr Hedonist:satyr hedonist').
card_rarity('satyr hedonist'/'DDL', 'Common').
card_artist('satyr hedonist'/'DDL', 'Chase Stone').
card_number('satyr hedonist'/'DDL', '47').
card_flavor_text('satyr hedonist'/'DDL', '\"Any festival you can walk away from wasn\'t worth attending in the first place.\"').
card_multiverse_id('satyr hedonist'/'DDL', '373340').

card_in_set('shower of sparks', 'DDL').
card_original_type('shower of sparks'/'DDL', 'Instant').
card_original_text('shower of sparks'/'DDL', 'Shower of Sparks deals 1 damage to target creature and 1 damage to target player.').
card_image_name('shower of sparks'/'DDL', 'shower of sparks').
card_uid('shower of sparks'/'DDL', 'DDL:Shower of Sparks:shower of sparks').
card_rarity('shower of sparks'/'DDL', 'Common').
card_artist('shower of sparks'/'DDL', 'Christopher Moeller').
card_number('shower of sparks'/'DDL', '61').
card_flavor_text('shower of sparks'/'DDL', 'The viashino had learned how to operate the rig through trial and error—mostly error.').
card_multiverse_id('shower of sparks'/'DDL', '373355').

card_in_set('skarrg, the rage pits', 'DDL').
card_original_type('skarrg, the rage pits'/'DDL', 'Land').
card_original_text('skarrg, the rage pits'/'DDL', '{T}: Add {1} to your mana pool.{R}{G}, {T}: Target creature gets +1/+1 and gains trample until end of turn.').
card_image_name('skarrg, the rage pits'/'DDL', 'skarrg, the rage pits').
card_uid('skarrg, the rage pits'/'DDL', 'DDL:Skarrg, the Rage Pits:skarrg, the rage pits').
card_rarity('skarrg, the rage pits'/'DDL', 'Uncommon').
card_artist('skarrg, the rage pits'/'DDL', 'Martina Pilcerova').
card_number('skarrg, the rage pits'/'DDL', '73').
card_flavor_text('skarrg, the rage pits'/'DDL', '\"This palace will be our fire-spit, and roasted prince our victory meal. Send in the torch-pigs!\"\n—Ghut Rak, Gruul guildmage').
card_multiverse_id('skarrg, the rage pits'/'DDL', '373357').

card_in_set('skarrgan firebird', 'DDL').
card_original_type('skarrgan firebird'/'DDL', 'Creature — Phoenix').
card_original_text('skarrgan firebird'/'DDL', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)Flying{R}{R}{R}: Return Skarrgan Firebird from your graveyard to your hand. Activate this ability only if an opponent was dealt damage this turn.').
card_image_name('skarrgan firebird'/'DDL', 'skarrgan firebird').
card_uid('skarrgan firebird'/'DDL', 'DDL:Skarrgan Firebird:skarrgan firebird').
card_rarity('skarrgan firebird'/'DDL', 'Rare').
card_artist('skarrgan firebird'/'DDL', 'Kev Walker').
card_number('skarrgan firebird'/'DDL', '57').
card_multiverse_id('skarrgan firebird'/'DDL', '373393').

card_in_set('skarrgan skybreaker', 'DDL').
card_original_type('skarrgan skybreaker'/'DDL', 'Creature — Giant Shaman').
card_original_text('skarrgan skybreaker'/'DDL', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.){1}, Sacrifice Skarrgan Skybreaker: Skarrgan Skybreaker deals damage equal to its power to target creature or player.').
card_image_name('skarrgan skybreaker'/'DDL', 'skarrgan skybreaker').
card_uid('skarrgan skybreaker'/'DDL', 'DDL:Skarrgan Skybreaker:skarrgan skybreaker').
card_rarity('skarrgan skybreaker'/'DDL', 'Uncommon').
card_artist('skarrgan skybreaker'/'DDL', 'Dan Scott').
card_number('skarrgan skybreaker'/'DDL', '60').
card_flavor_text('skarrgan skybreaker'/'DDL', '\"The sky isn\'t falling—it\'s being thrown at us!\"\n—Otak, Tin Street shopkeep').
card_multiverse_id('skarrgan skybreaker'/'DDL', '373413').

card_in_set('smite the monstrous', 'DDL').
card_original_type('smite the monstrous'/'DDL', 'Instant').
card_original_text('smite the monstrous'/'DDL', 'Destroy target creature with power 4 or greater.').
card_image_name('smite the monstrous'/'DDL', 'smite the monstrous').
card_uid('smite the monstrous'/'DDL', 'DDL:Smite the Monstrous:smite the monstrous').
card_rarity('smite the monstrous'/'DDL', 'Common').
card_artist('smite the monstrous'/'DDL', 'Jason Felix').
card_number('smite the monstrous'/'DDL', '29').
card_flavor_text('smite the monstrous'/'DDL', 'Though the old holy wards and roadside shrines have begun to fail, faith in Avacyn still holds true power.').
card_multiverse_id('smite the monstrous'/'DDL', '373372').

card_in_set('somberwald vigilante', 'DDL').
card_original_type('somberwald vigilante'/'DDL', 'Creature — Human Warrior').
card_original_text('somberwald vigilante'/'DDL', 'Whenever Somberwald Vigilante becomes blocked by a creature, Somberwald Vigilante deals 1 damage to that creature.').
card_image_name('somberwald vigilante'/'DDL', 'somberwald vigilante').
card_uid('somberwald vigilante'/'DDL', 'DDL:Somberwald Vigilante:somberwald vigilante').
card_rarity('somberwald vigilante'/'DDL', 'Common').
card_artist('somberwald vigilante'/'DDL', 'John Stanko').
card_number('somberwald vigilante'/'DDL', '2').
card_flavor_text('somberwald vigilante'/'DDL', 'He has nothing left but his resentment.').
card_multiverse_id('somberwald vigilante'/'DDL', '373385').

card_in_set('stand firm', 'DDL').
card_original_type('stand firm'/'DDL', 'Instant').
card_original_text('stand firm'/'DDL', 'Target creature gets +1/+1 until end of turn. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('stand firm'/'DDL', 'stand firm').
card_uid('stand firm'/'DDL', 'DDL:Stand Firm:stand firm').
card_rarity('stand firm'/'DDL', 'Common').
card_artist('stand firm'/'DDL', 'Matt Cavotta').
card_number('stand firm'/'DDL', '21').
card_multiverse_id('stand firm'/'DDL', '373343').

card_in_set('stun sniper', 'DDL').
card_original_type('stun sniper'/'DDL', 'Creature — Human Archer').
card_original_text('stun sniper'/'DDL', '{1}, {T}: Stun Sniper deals 1 damage to target creature. Tap that creature.').
card_image_name('stun sniper'/'DDL', 'stun sniper').
card_uid('stun sniper'/'DDL', 'DDL:Stun Sniper:stun sniper').
card_rarity('stun sniper'/'DDL', 'Uncommon').
card_artist('stun sniper'/'DDL', 'Steve Prescott').
card_number('stun sniper'/'DDL', '7').
card_flavor_text('stun sniper'/'DDL', 'The tips of her bolts are blunt, but her aim is deadly sharp.').
card_multiverse_id('stun sniper'/'DDL', '373391').

card_in_set('sun titan', 'DDL').
card_original_type('sun titan'/'DDL', 'Creature — Giant').
card_original_text('sun titan'/'DDL', 'VigilanceWhenever Sun Titan enters the battlefield or attacks, you may return target permanent card with converted mana cost 3 or less from your graveyard to the battlefield.').
card_image_name('sun titan'/'DDL', 'sun titan').
card_uid('sun titan'/'DDL', 'DDL:Sun Titan:sun titan').
card_rarity('sun titan'/'DDL', 'Mythic Rare').
card_artist('sun titan'/'DDL', 'Karl Kopinski').
card_number('sun titan'/'DDL', '1').
card_flavor_text('sun titan'/'DDL', 'A blazing sun that never sets.').
card_multiverse_id('sun titan'/'DDL', '373379').

card_in_set('terrifying presence', 'DDL').
card_original_type('terrifying presence'/'DDL', 'Instant').
card_original_text('terrifying presence'/'DDL', 'Prevent all combat damage that would be dealt by creatures other than target creature this turn.').
card_image_name('terrifying presence'/'DDL', 'terrifying presence').
card_uid('terrifying presence'/'DDL', 'DDL:Terrifying Presence:terrifying presence').
card_rarity('terrifying presence'/'DDL', 'Common').
card_artist('terrifying presence'/'DDL', 'Jaime Jones').
card_number('terrifying presence'/'DDL', '65').
card_flavor_text('terrifying presence'/'DDL', 'Elmut had slaughtered vampires, slain werewolves, and destroyed hordes of zombies. Sadly, he never got over his fear of spiders.').
card_multiverse_id('terrifying presence'/'DDL', '373380').

card_in_set('thraben valiant', 'DDL').
card_original_type('thraben valiant'/'DDL', 'Creature — Human Soldier').
card_original_text('thraben valiant'/'DDL', 'Vigilance').
card_image_name('thraben valiant'/'DDL', 'thraben valiant').
card_uid('thraben valiant'/'DDL', 'DDL:Thraben Valiant:thraben valiant').
card_rarity('thraben valiant'/'DDL', 'Common').
card_artist('thraben valiant'/'DDL', 'Jason Chan').
card_number('thraben valiant'/'DDL', '6').
card_flavor_text('thraben valiant'/'DDL', '\"Once more into Devil\'s Breach, soldiers. I want another devil tail for my collection.\"').
card_multiverse_id('thraben valiant'/'DDL', '373342').

card_in_set('troll ascetic', 'DDL').
card_original_type('troll ascetic'/'DDL', 'Creature — Troll Shaman').
card_original_text('troll ascetic'/'DDL', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.){1}{G}: Regenerate Troll Ascetic.').
card_image_name('troll ascetic'/'DDL', 'troll ascetic').
card_uid('troll ascetic'/'DDL', 'DDL:Troll Ascetic:troll ascetic').
card_rarity('troll ascetic'/'DDL', 'Rare').
card_artist('troll ascetic'/'DDL', 'Puddnhead').
card_number('troll ascetic'/'DDL', '50').
card_flavor_text('troll ascetic'/'DDL', 'It\'s no coincidence that the oldest trolls are also the angriest.').
card_multiverse_id('troll ascetic'/'DDL', '373416').

card_in_set('truefire paladin', 'DDL').
card_original_type('truefire paladin'/'DDL', 'Creature — Human Knight').
card_original_text('truefire paladin'/'DDL', 'Vigilance{R}{W}: Truefire Paladin gets +2/+0 until end of turn.{R}{W}: Truefire Paladin gains first strike until end of turn.').
card_image_name('truefire paladin'/'DDL', 'truefire paladin').
card_uid('truefire paladin'/'DDL', 'DDL:Truefire Paladin:truefire paladin').
card_rarity('truefire paladin'/'DDL', 'Uncommon').
card_artist('truefire paladin'/'DDL', 'Michael C. Hayes').
card_number('truefire paladin'/'DDL', '8').
card_multiverse_id('truefire paladin'/'DDL', '373395').

card_in_set('undying rage', 'DDL').
card_original_type('undying rage'/'DDL', 'Enchantment — Aura').
card_original_text('undying rage'/'DDL', 'Enchant creatureEnchanted creature gets +2/+2 and can\'t block.When Undying Rage is put into a graveyard from the battlefield, return Undying Rage to its owner\'s hand.').
card_image_name('undying rage'/'DDL', 'undying rage').
card_uid('undying rage'/'DDL', 'DDL:Undying Rage:undying rage').
card_rarity('undying rage'/'DDL', 'Uncommon').
card_artist('undying rage'/'DDL', 'Scott M. Fischer').
card_number('undying rage'/'DDL', '26').
card_multiverse_id('undying rage'/'DDL', '373347').

card_in_set('valley rannet', 'DDL').
card_original_type('valley rannet'/'DDL', 'Creature — Beast').
card_original_text('valley rannet'/'DDL', 'Mountaincycling {2}, forestcycling {2} ({2}, Discard this card: Search your library for a Mountain or Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('valley rannet'/'DDL', 'valley rannet').
card_uid('valley rannet'/'DDL', 'DDL:Valley Rannet:valley rannet').
card_rarity('valley rannet'/'DDL', 'Common').
card_artist('valley rannet'/'DDL', 'Dave Allsop').
card_number('valley rannet'/'DDL', '58').
card_multiverse_id('valley rannet'/'DDL', '373390').

card_in_set('volt charge', 'DDL').
card_original_type('volt charge'/'DDL', 'Instant').
card_original_text('volt charge'/'DDL', 'Volt Charge deals 3 damage to target creature or player. Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_image_name('volt charge'/'DDL', 'volt charge').
card_uid('volt charge'/'DDL', 'DDL:Volt Charge:volt charge').
card_rarity('volt charge'/'DDL', 'Common').
card_artist('volt charge'/'DDL', 'Jana Schirmer & Johannes Voss').
card_number('volt charge'/'DDL', '68').
card_multiverse_id('volt charge'/'DDL', '373414').

card_in_set('winds of rath', 'DDL').
card_original_type('winds of rath'/'DDL', 'Sorcery').
card_original_text('winds of rath'/'DDL', 'Destroy all creatures that aren\'t enchanted. They can\'t be regenerated.').
card_image_name('winds of rath'/'DDL', 'winds of rath').
card_uid('winds of rath'/'DDL', 'DDL:Winds of Rath:winds of rath').
card_rarity('winds of rath'/'DDL', 'Rare').
card_artist('winds of rath'/'DDL', 'Drew Tucker').
card_number('winds of rath'/'DDL', '31').
card_flavor_text('winds of rath'/'DDL', '\"There shall be a vast shout and then a vaster silence.\"\n—Oracle en-Vec').
card_multiverse_id('winds of rath'/'DDL', '373410').

card_in_set('zhur-taa druid', 'DDL').
card_original_type('zhur-taa druid'/'DDL', 'Creature — Human Druid').
card_original_text('zhur-taa druid'/'DDL', '{T}: Add {G} to your mana pool.Whenever you tap Zhur-Taa Druid for mana, it deals 1 damage to each opponent.').
card_image_name('zhur-taa druid'/'DDL', 'zhur-taa druid').
card_uid('zhur-taa druid'/'DDL', 'DDL:Zhur-Taa Druid:zhur-taa druid').
card_rarity('zhur-taa druid'/'DDL', 'Common').
card_artist('zhur-taa druid'/'DDL', 'Mark Winters').
card_number('zhur-taa druid'/'DDL', '48').
card_flavor_text('zhur-taa druid'/'DDL', '\"Only the decadent think magic should be pristine and without cost.\"').
card_multiverse_id('zhur-taa druid'/'DDL', '373339').
