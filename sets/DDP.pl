% Duel Decks: Zendikar vs. Eldrazi

set('DDP').
set_name('DDP', 'Duel Decks: Zendikar vs. Eldrazi').
set_release_date('DDP', '2015-08-28').
set_border('DDP', 'black').
set_type('DDP', 'duel deck').

card_in_set('affa guard hound', 'DDP').
card_original_type('affa guard hound'/'DDP', 'Creature — Hound').
card_original_text('affa guard hound'/'DDP', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Affa Guard Hound enters the battlefield, target creature gets +0/+3 until end of turn.').
card_image_name('affa guard hound'/'DDP', 'affa guard hound').
card_uid('affa guard hound'/'DDP', 'DDP:Affa Guard Hound:affa guard hound').
card_rarity('affa guard hound'/'DDP', 'Uncommon').
card_artist('affa guard hound'/'DDP', 'Ryan Pancoast').
card_number('affa guard hound'/'DDP', '2').
card_flavor_text('affa guard hound'/'DDP', 'Once a welcoming hub for explorers, Affa became a place of guarded tongues and quick defenses.').
card_multiverse_id('affa guard hound'/'DDP', '401644').

card_in_set('akoum refuge', 'DDP').
card_original_type('akoum refuge'/'DDP', 'Land').
card_original_text('akoum refuge'/'DDP', 'Akoum Refuge enters the battlefield tapped.\nWhen Akoum Refuge enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('akoum refuge'/'DDP', 'akoum refuge').
card_uid('akoum refuge'/'DDP', 'DDP:Akoum Refuge:akoum refuge').
card_rarity('akoum refuge'/'DDP', 'Uncommon').
card_artist('akoum refuge'/'DDP', 'Fred Fields').
card_number('akoum refuge'/'DDP', '67').
card_multiverse_id('akoum refuge'/'DDP', '401709').

card_in_set('artisan of kozilek', 'DDP').
card_original_type('artisan of kozilek'/'DDP', 'Creature — Eldrazi').
card_original_text('artisan of kozilek'/'DDP', 'When you cast Artisan of Kozilek, you may return target creature card from your graveyard to the battlefield.\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)').
card_image_name('artisan of kozilek'/'DDP', 'artisan of kozilek').
card_uid('artisan of kozilek'/'DDP', 'DDP:Artisan of Kozilek:artisan of kozilek').
card_rarity('artisan of kozilek'/'DDP', 'Uncommon').
card_artist('artisan of kozilek'/'DDP', 'Jason Felix').
card_number('artisan of kozilek'/'DDP', '42').
card_multiverse_id('artisan of kozilek'/'DDP', '401684').

card_in_set('avenger of zendikar', 'DDP').
card_original_type('avenger of zendikar'/'DDP', 'Creature — Elemental').
card_original_text('avenger of zendikar'/'DDP', 'When Avenger of Zendikar enters the battlefield, put a 0/1 green Plant creature token onto the battlefield for each land you control.\nLandfall — Whenever a land enters the battlefield under your control, you may put a +1/+1 counter on each Plant creature you control.').
card_image_name('avenger of zendikar'/'DDP', 'avenger of zendikar').
card_uid('avenger of zendikar'/'DDP', 'DDP:Avenger of Zendikar:avenger of zendikar').
card_rarity('avenger of zendikar'/'DDP', 'Mythic Rare').
card_artist('avenger of zendikar'/'DDP', 'Aleksi Briclot').
card_number('avenger of zendikar'/'DDP', '1').
card_multiverse_id('avenger of zendikar'/'DDP', '401643').

card_in_set('beastbreaker of bala ged', 'DDP').
card_original_type('beastbreaker of bala ged'/'DDP', 'Creature — Human Warrior').
card_original_text('beastbreaker of bala ged'/'DDP', 'Level up {2}{G} ({2}{G}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n4/4\nLEVEL 4+\n6/6\nTrample').
card_image_name('beastbreaker of bala ged'/'DDP', 'beastbreaker of bala ged').
card_uid('beastbreaker of bala ged'/'DDP', 'DDP:Beastbreaker of Bala Ged:beastbreaker of bala ged').
card_rarity('beastbreaker of bala ged'/'DDP', 'Uncommon').
card_artist('beastbreaker of bala ged'/'DDP', 'Karl Kopinski').
card_number('beastbreaker of bala ged'/'DDP', '10').
card_multiverse_id('beastbreaker of bala ged'/'DDP', '401652').

card_in_set('bloodrite invoker', 'DDP').
card_original_type('bloodrite invoker'/'DDP', 'Creature — Vampire Shaman').
card_original_text('bloodrite invoker'/'DDP', '{8}: Target player loses 3 life and you gain 3 life.').
card_image_name('bloodrite invoker'/'DDP', 'bloodrite invoker').
card_uid('bloodrite invoker'/'DDP', 'DDP:Bloodrite Invoker:bloodrite invoker').
card_rarity('bloodrite invoker'/'DDP', 'Common').
card_artist('bloodrite invoker'/'DDP', 'Svetlin Velinov').
card_number('bloodrite invoker'/'DDP', '45').
card_flavor_text('bloodrite invoker'/'DDP', '\"The brood lineages unfolded across the world, each patterned after one of three progenitors, each a study in mindless consumption.\"\n—The Invokers\' Tales').
card_multiverse_id('bloodrite invoker'/'DDP', '401687').

card_in_set('bloodthrone vampire', 'DDP').
card_original_type('bloodthrone vampire'/'DDP', 'Creature — Vampire').
card_original_text('bloodthrone vampire'/'DDP', 'Sacrifice a creature: Bloodthrone Vampire gets +2/+2 until end of turn.').
card_image_name('bloodthrone vampire'/'DDP', 'bloodthrone vampire').
card_uid('bloodthrone vampire'/'DDP', 'DDP:Bloodthrone Vampire:bloodthrone vampire').
card_rarity('bloodthrone vampire'/'DDP', 'Common').
card_artist('bloodthrone vampire'/'DDP', 'Steve Argyle').
card_number('bloodthrone vampire'/'DDP', '46').
card_flavor_text('bloodthrone vampire'/'DDP', 'Some humans willingly offered up their blood, hoping it would grant the vampire families the strength to stave off the Eldrazi.').
card_multiverse_id('bloodthrone vampire'/'DDP', '401688').

card_in_set('butcher of malakir', 'DDP').
card_original_type('butcher of malakir'/'DDP', 'Creature — Vampire Warrior').
card_original_text('butcher of malakir'/'DDP', 'Flying\nWhenever Butcher of Malakir or another creature you control dies, each opponent sacrifices a creature.').
card_image_name('butcher of malakir'/'DDP', 'butcher of malakir').
card_uid('butcher of malakir'/'DDP', 'DDP:Butcher of Malakir:butcher of malakir').
card_rarity('butcher of malakir'/'DDP', 'Rare').
card_artist('butcher of malakir'/'DDP', 'Jason Chan').
card_number('butcher of malakir'/'DDP', '47').
card_flavor_text('butcher of malakir'/'DDP', 'His verdict is always guilty. His sentence is always death.').
card_multiverse_id('butcher of malakir'/'DDP', '401689').

card_in_set('cadaver imp', 'DDP').
card_original_type('cadaver imp'/'DDP', 'Creature — Imp').
card_original_text('cadaver imp'/'DDP', 'Flying\nWhen Cadaver Imp enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('cadaver imp'/'DDP', 'cadaver imp').
card_uid('cadaver imp'/'DDP', 'DDP:Cadaver Imp:cadaver imp').
card_rarity('cadaver imp'/'DDP', 'Common').
card_artist('cadaver imp'/'DDP', 'Dave Kendall').
card_number('cadaver imp'/'DDP', '48').
card_flavor_text('cadaver imp'/'DDP', 'The mouth must be pried open before rigor mortis sets in. Otherwise the returning soul can find no ingress.').
card_multiverse_id('cadaver imp'/'DDP', '401690').

card_in_set('caravan escort', 'DDP').
card_original_type('caravan escort'/'DDP', 'Creature — Human Knight').
card_original_text('caravan escort'/'DDP', 'Level up {2} ({2}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n2/2\nLEVEL 5+\n5/5\nFirst strike').
card_image_name('caravan escort'/'DDP', 'caravan escort').
card_uid('caravan escort'/'DDP', 'DDP:Caravan Escort:caravan escort').
card_rarity('caravan escort'/'DDP', 'Common').
card_artist('caravan escort'/'DDP', 'Goran Josic').
card_number('caravan escort'/'DDP', '3').
card_multiverse_id('caravan escort'/'DDP', '401645').

card_in_set('consume the meek', 'DDP').
card_original_type('consume the meek'/'DDP', 'Instant').
card_original_text('consume the meek'/'DDP', 'Destroy each creature with converted mana cost 3 or less. They can\'t be regenerated.').
card_image_name('consume the meek'/'DDP', 'consume the meek').
card_uid('consume the meek'/'DDP', 'DDP:Consume the Meek:consume the meek').
card_rarity('consume the meek'/'DDP', 'Rare').
card_artist('consume the meek'/'DDP', 'Richard Wright').
card_number('consume the meek'/'DDP', '49').
card_flavor_text('consume the meek'/'DDP', '\"Others ask why the Eldrazi destroy. I ask why they preserve.\"\n—Ganda, Goma Fada deserter').
card_multiverse_id('consume the meek'/'DDP', '401691').

card_in_set('corpsehatch', 'DDP').
card_original_type('corpsehatch'/'DDP', 'Sorcery').
card_original_text('corpsehatch'/'DDP', 'Destroy target nonblack creature. Put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('corpsehatch'/'DDP', 'corpsehatch').
card_uid('corpsehatch'/'DDP', 'DDP:Corpsehatch:corpsehatch').
card_rarity('corpsehatch'/'DDP', 'Uncommon').
card_artist('corpsehatch'/'DDP', 'Kekai Kotaki').
card_number('corpsehatch'/'DDP', '50').
card_multiverse_id('corpsehatch'/'DDP', '401692').

card_in_set('daggerback basilisk', 'DDP').
card_original_type('daggerback basilisk'/'DDP', 'Creature — Basilisk').
card_original_text('daggerback basilisk'/'DDP', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('daggerback basilisk'/'DDP', 'daggerback basilisk').
card_uid('daggerback basilisk'/'DDP', 'DDP:Daggerback Basilisk:daggerback basilisk').
card_rarity('daggerback basilisk'/'DDP', 'Common').
card_artist('daggerback basilisk'/'DDP', 'Jesper Ejsing').
card_number('daggerback basilisk'/'DDP', '11').
card_flavor_text('daggerback basilisk'/'DDP', '\"Petrifying gaze, deadly fangs, knifelike dorsal spines, venomous saliva . . . Am I missing anything? . . . Toxic bones? Seriously?\"\n—Samila, Murasa Expeditionary House').
card_multiverse_id('daggerback basilisk'/'DDP', '401653').

card_in_set('dominator drone', 'DDP').
card_original_type('dominator drone'/'DDP', 'Creature — Eldrazi Drone').
card_original_text('dominator drone'/'DDP', 'Devoid (This card has no color.)\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)\nWhen Dominator Drone enters the battlefield, if you control another colorless creature, each opponent loses 2 life.').
card_first_print('dominator drone', 'DDP').
card_image_name('dominator drone'/'DDP', 'dominator drone').
card_uid('dominator drone'/'DDP', 'DDP:Dominator Drone:dominator drone').
card_rarity('dominator drone'/'DDP', 'Common').
card_artist('dominator drone'/'DDP', 'James Zapata').
card_number('dominator drone'/'DDP', '51').
card_multiverse_id('dominator drone'/'DDP', '401693').

card_in_set('eldrazi spawn', 'DDP').
card_original_type('eldrazi spawn'/'DDP', 'Token Creature — Eldrazi Spawn').
card_original_text('eldrazi spawn'/'DDP', 'Sacrifice this creature: Add {1} to your mana pool.').
card_first_print('eldrazi spawn', 'DDP').
card_image_name('eldrazi spawn'/'DDP', 'eldrazi spawn1').
card_uid('eldrazi spawn'/'DDP', 'DDP:Eldrazi Spawn:eldrazi spawn1').
card_rarity('eldrazi spawn'/'DDP', 'Common').
card_artist('eldrazi spawn'/'DDP', 'Aleksi Briclot').
card_number('eldrazi spawn'/'DDP', '76').
card_multiverse_id('eldrazi spawn'/'DDP', '401718').

card_in_set('eldrazi spawn', 'DDP').
card_original_type('eldrazi spawn'/'DDP', 'Token Creature — Eldrazi Spawn').
card_original_text('eldrazi spawn'/'DDP', 'Sacrifice this creature: Add {1} to your mana pool.').
card_image_name('eldrazi spawn'/'DDP', 'eldrazi spawn2').
card_uid('eldrazi spawn'/'DDP', 'DDP:Eldrazi Spawn:eldrazi spawn2').
card_rarity('eldrazi spawn'/'DDP', 'Common').
card_artist('eldrazi spawn'/'DDP', 'Véronique Meignaud').
card_number('eldrazi spawn'/'DDP', '77').
card_multiverse_id('eldrazi spawn'/'DDP', '401719').

card_in_set('eldrazi spawn', 'DDP').
card_original_type('eldrazi spawn'/'DDP', 'Token Creature — Eldrazi Spawn').
card_original_text('eldrazi spawn'/'DDP', 'Sacrifice this creature: Add {1} to your mana pool.').
card_image_name('eldrazi spawn'/'DDP', 'eldrazi spawn3').
card_uid('eldrazi spawn'/'DDP', 'DDP:Eldrazi Spawn:eldrazi spawn3').
card_rarity('eldrazi spawn'/'DDP', 'Common').
card_artist('eldrazi spawn'/'DDP', 'Mark Tedin').
card_number('eldrazi spawn'/'DDP', '78').
card_multiverse_id('eldrazi spawn'/'DDP', '401720').

card_in_set('eldrazi temple', 'DDP').
card_original_type('eldrazi temple'/'DDP', 'Land').
card_original_text('eldrazi temple'/'DDP', '{T}: Add {1} to your mana pool.\n{T}: Add {2} to your mana pool. Spend this mana only to cast colorless Eldrazi spells or activate abilities of colorless Eldrazi.').
card_image_name('eldrazi temple'/'DDP', 'eldrazi temple').
card_uid('eldrazi temple'/'DDP', 'DDP:Eldrazi Temple:eldrazi temple').
card_rarity('eldrazi temple'/'DDP', 'Uncommon').
card_artist('eldrazi temple'/'DDP', 'James Paick').
card_number('eldrazi temple'/'DDP', '68').
card_flavor_text('eldrazi temple'/'DDP', 'Each temple is a door to a horrible future.').
card_multiverse_id('eldrazi temple'/'DDP', '401710').

card_in_set('emrakul\'s hatcher', 'DDP').
card_original_type('emrakul\'s hatcher'/'DDP', 'Creature — Eldrazi Drone').
card_original_text('emrakul\'s hatcher'/'DDP', 'When Emrakul\'s Hatcher enters the battlefield, put three 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('emrakul\'s hatcher'/'DDP', 'emrakul\'s hatcher').
card_uid('emrakul\'s hatcher'/'DDP', 'DDP:Emrakul\'s Hatcher:emrakul\'s hatcher').
card_rarity('emrakul\'s hatcher'/'DDP', 'Common').
card_artist('emrakul\'s hatcher'/'DDP', 'Jaime Jones').
card_number('emrakul\'s hatcher'/'DDP', '59').
card_flavor_text('emrakul\'s hatcher'/'DDP', 'Wordlessly it leads its abhorrent brood.').
card_multiverse_id('emrakul\'s hatcher'/'DDP', '401701').

card_in_set('evolving wilds', 'DDP').
card_original_type('evolving wilds'/'DDP', 'Land').
card_original_text('evolving wilds'/'DDP', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'DDP', 'evolving wilds').
card_uid('evolving wilds'/'DDP', 'DDP:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'DDP', 'Common').
card_artist('evolving wilds'/'DDP', 'Steven Belledin').
card_number('evolving wilds'/'DDP', '31').
card_flavor_text('evolving wilds'/'DDP', 'Every world is an organism, able to grow new lands. Some just do it faster than others.').
card_multiverse_id('evolving wilds'/'DDP', '401673').

card_in_set('explorer\'s scope', 'DDP').
card_original_type('explorer\'s scope'/'DDP', 'Artifact — Equipment').
card_original_text('explorer\'s scope'/'DDP', 'Whenever equipped creature attacks, look at the top card of your library. If it\'s a land card, you may put it onto the battlefield tapped.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('explorer\'s scope'/'DDP', 'explorer\'s scope').
card_uid('explorer\'s scope'/'DDP', 'DDP:Explorer\'s Scope:explorer\'s scope').
card_rarity('explorer\'s scope'/'DDP', 'Common').
card_artist('explorer\'s scope'/'DDP', 'Vincent Proce').
card_number('explorer\'s scope'/'DDP', '28').
card_multiverse_id('explorer\'s scope'/'DDP', '401670').

card_in_set('forerunner of slaughter', 'DDP').
card_original_type('forerunner of slaughter'/'DDP', 'Creature — Eldrazi Drone').
card_original_text('forerunner of slaughter'/'DDP', 'Devoid (This card has no color.)\n{1}: Target colorless creature gains haste until end of turn.').
card_first_print('forerunner of slaughter', 'DDP').
card_image_name('forerunner of slaughter'/'DDP', 'forerunner of slaughter').
card_uid('forerunner of slaughter'/'DDP', 'DDP:Forerunner of Slaughter:forerunner of slaughter').
card_rarity('forerunner of slaughter'/'DDP', 'Uncommon').
card_artist('forerunner of slaughter'/'DDP', 'James Zapata').
card_number('forerunner of slaughter'/'DDP', '64').
card_flavor_text('forerunner of slaughter'/'DDP', '\"While we delay, they\'re on the move. They will not wait as we bicker. We are losing this war by moments as our goal slips beyond our reach.\"\n—Gideon Jura').
card_multiverse_id('forerunner of slaughter'/'DDP', '401706').

card_in_set('forest', 'DDP').
card_original_type('forest'/'DDP', 'Basic Land — Forest').
card_original_text('forest'/'DDP', 'G').
card_image_name('forest'/'DDP', 'forest1').
card_uid('forest'/'DDP', 'DDP:Forest:forest1').
card_rarity('forest'/'DDP', 'Basic Land').
card_artist('forest'/'DDP', 'John Avon').
card_number('forest'/'DDP', '38').
card_multiverse_id('forest'/'DDP', '401680').

card_in_set('forest', 'DDP').
card_original_type('forest'/'DDP', 'Basic Land — Forest').
card_original_text('forest'/'DDP', 'G').
card_image_name('forest'/'DDP', 'forest2').
card_uid('forest'/'DDP', 'DDP:Forest:forest2').
card_rarity('forest'/'DDP', 'Basic Land').
card_artist('forest'/'DDP', 'Véronique Meignaud').
card_number('forest'/'DDP', '39').
card_multiverse_id('forest'/'DDP', '401681').

card_in_set('forest', 'DDP').
card_original_type('forest'/'DDP', 'Basic Land — Forest').
card_original_text('forest'/'DDP', 'G').
card_image_name('forest'/'DDP', 'forest3').
card_uid('forest'/'DDP', 'DDP:Forest:forest3').
card_rarity('forest'/'DDP', 'Basic Land').
card_artist('forest'/'DDP', 'Vincent Proce').
card_number('forest'/'DDP', '40').
card_multiverse_id('forest'/'DDP', '401682').

card_in_set('forked bolt', 'DDP').
card_original_type('forked bolt'/'DDP', 'Sorcery').
card_original_text('forked bolt'/'DDP', 'Forked Bolt deals 2 damage divided as you choose among one or two target creatures and/or players.').
card_image_name('forked bolt'/'DDP', 'forked bolt').
card_uid('forked bolt'/'DDP', 'DDP:Forked Bolt:forked bolt').
card_rarity('forked bolt'/'DDP', 'Common').
card_artist('forked bolt'/'DDP', 'Tomasz Jedruszek').
card_number('forked bolt'/'DDP', '60').
card_flavor_text('forked bolt'/'DDP', '\"Play no favorites. Everybody dies.\"\n—Sparkmage saying').
card_multiverse_id('forked bolt'/'DDP', '401702').

card_in_set('frontier guide', 'DDP').
card_original_type('frontier guide'/'DDP', 'Creature — Elf Scout').
card_original_text('frontier guide'/'DDP', '{3}{G}, {T}: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('frontier guide'/'DDP', 'frontier guide').
card_uid('frontier guide'/'DDP', 'DDP:Frontier Guide:frontier guide').
card_rarity('frontier guide'/'DDP', 'Uncommon').
card_artist('frontier guide'/'DDP', 'Wayne Reynolds').
card_number('frontier guide'/'DDP', '12').
card_flavor_text('frontier guide'/'DDP', '\"The elders say Zendikar has no safe, peaceful places. How would they know? They\'ve never looked.\"').
card_multiverse_id('frontier guide'/'DDP', '401654').

card_in_set('graypelt hunter', 'DDP').
card_original_type('graypelt hunter'/'DDP', 'Creature — Human Warrior Ally').
card_original_text('graypelt hunter'/'DDP', 'Trample\nWhenever Graypelt Hunter or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Graypelt Hunter.').
card_image_name('graypelt hunter'/'DDP', 'graypelt hunter').
card_uid('graypelt hunter'/'DDP', 'DDP:Graypelt Hunter:graypelt hunter').
card_rarity('graypelt hunter'/'DDP', 'Common').
card_artist('graypelt hunter'/'DDP', 'Svetlin Velinov').
card_number('graypelt hunter'/'DDP', '13').
card_flavor_text('graypelt hunter'/'DDP', 'He\'ll cleave wood and bone alike to carve a path for his allies.').
card_multiverse_id('graypelt hunter'/'DDP', '401655').

card_in_set('graypelt refuge', 'DDP').
card_original_type('graypelt refuge'/'DDP', 'Land').
card_original_text('graypelt refuge'/'DDP', 'Graypelt Refuge enters the battlefield tapped.\nWhen Graypelt Refuge enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('graypelt refuge'/'DDP', 'graypelt refuge').
card_uid('graypelt refuge'/'DDP', 'DDP:Graypelt Refuge:graypelt refuge').
card_rarity('graypelt refuge'/'DDP', 'Uncommon').
card_artist('graypelt refuge'/'DDP', 'Philip Straub').
card_number('graypelt refuge'/'DDP', '32').
card_multiverse_id('graypelt refuge'/'DDP', '401674').

card_in_set('grazing gladehart', 'DDP').
card_original_type('grazing gladehart'/'DDP', 'Creature — Antelope').
card_original_text('grazing gladehart'/'DDP', 'Landfall — Whenever a land enters the battlefield under your control, you may gain 2 life.').
card_image_name('grazing gladehart'/'DDP', 'grazing gladehart').
card_uid('grazing gladehart'/'DDP', 'DDP:Grazing Gladehart:grazing gladehart').
card_rarity('grazing gladehart'/'DDP', 'Common').
card_artist('grazing gladehart'/'DDP', 'Ryan Pancoast').
card_number('grazing gladehart'/'DDP', '14').
card_flavor_text('grazing gladehart'/'DDP', '\"Don\'t be fooled. If it were as docile as it looks, it would\'ve died off long ago.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('grazing gladehart'/'DDP', '401656').

card_in_set('groundswell', 'DDP').
card_original_type('groundswell'/'DDP', 'Instant').
card_original_text('groundswell'/'DDP', 'Target creature gets +2/+2 until end of turn.\nLandfall — If you had a land enter the battlefield under your control this turn, that creature gets +4/+4 until end of turn instead.').
card_image_name('groundswell'/'DDP', 'groundswell').
card_uid('groundswell'/'DDP', 'DDP:Groundswell:groundswell').
card_rarity('groundswell'/'DDP', 'Common').
card_artist('groundswell'/'DDP', 'Chris Rahn').
card_number('groundswell'/'DDP', '15').
card_flavor_text('groundswell'/'DDP', '\"This world will not be tamed.\"').
card_multiverse_id('groundswell'/'DDP', '401657').

card_in_set('harrow', 'DDP').
card_original_type('harrow'/'DDP', 'Instant').
card_original_text('harrow'/'DDP', 'As an additional cost to cast Harrow, sacrifice a land.\nSearch your library for up to two basic land cards and put them onto the battlefield. Then shuffle your library.').
card_image_name('harrow'/'DDP', 'harrow').
card_uid('harrow'/'DDP', 'DDP:Harrow:harrow').
card_rarity('harrow'/'DDP', 'Common').
card_artist('harrow'/'DDP', 'Izzy').
card_number('harrow'/'DDP', '16').
card_flavor_text('harrow'/'DDP', 'The Roil redraws the world as it passes, leaving only the hedrons unchanged.').
card_multiverse_id('harrow'/'DDP', '401658').

card_in_set('heartstabber mosquito', 'DDP').
card_original_type('heartstabber mosquito'/'DDP', 'Creature — Insect').
card_original_text('heartstabber mosquito'/'DDP', 'Kicker {2}{B} (You may pay an additional {2}{B} as you cast this spell.)\nFlying\nWhen Heartstabber Mosquito enters the battlefield, if it was kicked, destroy target creature.').
card_image_name('heartstabber mosquito'/'DDP', 'heartstabber mosquito').
card_uid('heartstabber mosquito'/'DDP', 'DDP:Heartstabber Mosquito:heartstabber mosquito').
card_rarity('heartstabber mosquito'/'DDP', 'Common').
card_artist('heartstabber mosquito'/'DDP', 'Jason Felix').
card_number('heartstabber mosquito'/'DDP', '52').
card_multiverse_id('heartstabber mosquito'/'DDP', '401694').

card_in_set('hellion', 'DDP').
card_original_type('hellion'/'DDP', 'Token Creature — Hellion').
card_original_text('hellion'/'DDP', '').
card_first_print('hellion', 'DDP').
card_image_name('hellion'/'DDP', 'hellion').
card_uid('hellion'/'DDP', 'DDP:Hellion:hellion').
card_rarity('hellion'/'DDP', 'Common').
card_artist('hellion'/'DDP', 'Anthony Francisco').
card_number('hellion'/'DDP', '79').
card_multiverse_id('hellion'/'DDP', '401721').

card_in_set('hellion eruption', 'DDP').
card_original_type('hellion eruption'/'DDP', 'Sorcery').
card_original_text('hellion eruption'/'DDP', 'Sacrifice all creatures you control, then put that many 4/4 red Hellion creature tokens onto the battlefield.').
card_image_name('hellion eruption'/'DDP', 'hellion eruption').
card_uid('hellion eruption'/'DDP', 'DDP:Hellion Eruption:hellion eruption').
card_rarity('hellion eruption'/'DDP', 'Rare').
card_artist('hellion eruption'/'DDP', 'Anthony Francisco').
card_number('hellion eruption'/'DDP', '61').
card_flavor_text('hellion eruption'/'DDP', 'Just when you thought you\'d be safe, in the middle of an open field with no Eldrazi around for miles.').
card_multiverse_id('hellion eruption'/'DDP', '401703').

card_in_set('induce despair', 'DDP').
card_original_type('induce despair'/'DDP', 'Instant').
card_original_text('induce despair'/'DDP', 'As an additional cost to cast Induce Despair, reveal a creature card from your hand.\nTarget creature gets -X/-X until end of turn, where X is the revealed card\'s converted mana cost.').
card_image_name('induce despair'/'DDP', 'induce despair').
card_uid('induce despair'/'DDP', 'DDP:Induce Despair:induce despair').
card_rarity('induce despair'/'DDP', 'Common').
card_artist('induce despair'/'DDP', 'Igor Kieryluk').
card_number('induce despair'/'DDP', '53').
card_flavor_text('induce despair'/'DDP', 'All the angel saw was her doom.').
card_multiverse_id('induce despair'/'DDP', '401695').

card_in_set('it that betrays', 'DDP').
card_original_type('it that betrays'/'DDP', 'Creature — Eldrazi').
card_original_text('it that betrays'/'DDP', 'Annihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)\nWhenever an opponent sacrifices a nontoken permanent, put that card onto the battlefield under your control.').
card_image_name('it that betrays'/'DDP', 'it that betrays').
card_uid('it that betrays'/'DDP', 'DDP:It That Betrays:it that betrays').
card_rarity('it that betrays'/'DDP', 'Rare').
card_artist('it that betrays'/'DDP', 'Tomasz Jedruszek').
card_number('it that betrays'/'DDP', '43').
card_flavor_text('it that betrays'/'DDP', 'Your pleas for death shall go unheard.').
card_multiverse_id('it that betrays'/'DDP', '401685').

card_in_set('joraga bard', 'DDP').
card_original_type('joraga bard'/'DDP', 'Creature — Elf Rogue Ally').
card_original_text('joraga bard'/'DDP', 'Whenever Joraga Bard or another Ally enters the battlefield under your control, you may have Ally creatures you control gain vigilance until end of turn.').
card_image_name('joraga bard'/'DDP', 'joraga bard').
card_uid('joraga bard'/'DDP', 'DDP:Joraga Bard:joraga bard').
card_rarity('joraga bard'/'DDP', 'Common').
card_artist('joraga bard'/'DDP', 'Volkan Baga').
card_number('joraga bard'/'DDP', '17').
card_flavor_text('joraga bard'/'DDP', 'Adventuring bards recite Chanter\'s Epics, never-ending tales that keep their comrades\' minds alert to danger.').
card_multiverse_id('joraga bard'/'DDP', '401659').

card_in_set('kabira vindicator', 'DDP').
card_original_type('kabira vindicator'/'DDP', 'Creature — Human Knight').
card_original_text('kabira vindicator'/'DDP', 'Level up {2}{W} ({2}{W}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-4\n3/6\nOther creatures you control get +1/+1.\nLEVEL 5+\n4/8\nOther creatures you control get +2/+2.').
card_image_name('kabira vindicator'/'DDP', 'kabira vindicator').
card_uid('kabira vindicator'/'DDP', 'DDP:Kabira Vindicator:kabira vindicator').
card_rarity('kabira vindicator'/'DDP', 'Uncommon').
card_artist('kabira vindicator'/'DDP', 'Steven Belledin').
card_number('kabira vindicator'/'DDP', '4').
card_multiverse_id('kabira vindicator'/'DDP', '401646').

card_in_set('khalni heart expedition', 'DDP').
card_original_type('khalni heart expedition'/'DDP', 'Enchantment').
card_original_text('khalni heart expedition'/'DDP', 'Landfall — Whenever a land enters the battlefield under your control, you may put a quest counter on Khalni Heart Expedition.\nRemove three quest counters from Khalni Heart Expedition and sacrifice it: Search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_image_name('khalni heart expedition'/'DDP', 'khalni heart expedition').
card_uid('khalni heart expedition'/'DDP', 'DDP:Khalni Heart Expedition:khalni heart expedition').
card_rarity('khalni heart expedition'/'DDP', 'Common').
card_artist('khalni heart expedition'/'DDP', 'Jason Chan').
card_number('khalni heart expedition'/'DDP', '18').
card_multiverse_id('khalni heart expedition'/'DDP', '401660').

card_in_set('knight of cliffhaven', 'DDP').
card_original_type('knight of cliffhaven'/'DDP', 'Creature — Kor Knight').
card_original_text('knight of cliffhaven'/'DDP', 'Level up {3} ({3}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n2/3\nFlying\nLEVEL 4+\n4/4\nFlying, vigilance').
card_image_name('knight of cliffhaven'/'DDP', 'knight of cliffhaven').
card_uid('knight of cliffhaven'/'DDP', 'DDP:Knight of Cliffhaven:knight of cliffhaven').
card_rarity('knight of cliffhaven'/'DDP', 'Common').
card_artist('knight of cliffhaven'/'DDP', 'Matt Cavotta').
card_number('knight of cliffhaven'/'DDP', '5').
card_multiverse_id('knight of cliffhaven'/'DDP', '401647').

card_in_set('magmaw', 'DDP').
card_original_type('magmaw'/'DDP', 'Creature — Elemental').
card_original_text('magmaw'/'DDP', '{1}, Sacrifice a nonland permanent: Magmaw deals 1 damage to target creature or player.').
card_image_name('magmaw'/'DDP', 'magmaw').
card_uid('magmaw'/'DDP', 'DDP:Magmaw:magmaw').
card_rarity('magmaw'/'DDP', 'Rare').
card_artist('magmaw'/'DDP', 'Karl Kopinski').
card_number('magmaw'/'DDP', '62').
card_flavor_text('magmaw'/'DDP', '\"The purpose of existence is simple: everything is fuel for the magmaw.\"\n—Jaji, magmaw worshipper').
card_multiverse_id('magmaw'/'DDP', '401704').

card_in_set('makindi griffin', 'DDP').
card_original_type('makindi griffin'/'DDP', 'Creature — Griffin').
card_original_text('makindi griffin'/'DDP', 'Flying').
card_image_name('makindi griffin'/'DDP', 'makindi griffin').
card_uid('makindi griffin'/'DDP', 'DDP:Makindi Griffin:makindi griffin').
card_rarity('makindi griffin'/'DDP', 'Common').
card_artist('makindi griffin'/'DDP', 'Izzy').
card_number('makindi griffin'/'DDP', '6').
card_flavor_text('makindi griffin'/'DDP', 'As the hedrons began to coalesce into colossal Eldrazi superstructures, the griffins were forced to seek new territory lest their aeries be crushed between the massive stone monoliths.').
card_multiverse_id('makindi griffin'/'DDP', '401648').

card_in_set('marsh casualties', 'DDP').
card_original_type('marsh casualties'/'DDP', 'Sorcery').
card_original_text('marsh casualties'/'DDP', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nCreatures target player controls get -1/-1 until end of turn. If Marsh Casualties was kicked, those creatures get -2/-2 until end of turn instead.').
card_image_name('marsh casualties'/'DDP', 'marsh casualties').
card_uid('marsh casualties'/'DDP', 'DDP:Marsh Casualties:marsh casualties').
card_rarity('marsh casualties'/'DDP', 'Uncommon').
card_artist('marsh casualties'/'DDP', 'Scott Chou').
card_number('marsh casualties'/'DDP', '54').
card_multiverse_id('marsh casualties'/'DDP', '401696').

card_in_set('mind stone', 'DDP').
card_original_type('mind stone'/'DDP', 'Artifact').
card_original_text('mind stone'/'DDP', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Mind Stone: Draw a card.').
card_image_name('mind stone'/'DDP', 'mind stone').
card_uid('mind stone'/'DDP', 'DDP:Mind Stone:mind stone').
card_rarity('mind stone'/'DDP', 'Uncommon').
card_artist('mind stone'/'DDP', 'Adam Rex').
card_number('mind stone'/'DDP', '65').
card_flavor_text('mind stone'/'DDP', 'What is mana but possibility, an idea not yet given form?').
card_multiverse_id('mind stone'/'DDP', '401707').

card_in_set('mountain', 'DDP').
card_original_type('mountain'/'DDP', 'Basic Land — Mountain').
card_original_text('mountain'/'DDP', 'R').
card_image_name('mountain'/'DDP', 'mountain1').
card_uid('mountain'/'DDP', 'DDP:Mountain:mountain1').
card_rarity('mountain'/'DDP', 'Basic Land').
card_artist('mountain'/'DDP', 'James Paick').
card_number('mountain'/'DDP', '73').
card_multiverse_id('mountain'/'DDP', '401715').

card_in_set('mountain', 'DDP').
card_original_type('mountain'/'DDP', 'Basic Land — Mountain').
card_original_text('mountain'/'DDP', 'R').
card_image_name('mountain'/'DDP', 'mountain2').
card_uid('mountain'/'DDP', 'DDP:Mountain:mountain2').
card_rarity('mountain'/'DDP', 'Basic Land').
card_artist('mountain'/'DDP', 'James Paick').
card_number('mountain'/'DDP', '74').
card_multiverse_id('mountain'/'DDP', '401716').

card_in_set('mountain', 'DDP').
card_original_type('mountain'/'DDP', 'Basic Land — Mountain').
card_original_text('mountain'/'DDP', 'R').
card_image_name('mountain'/'DDP', 'mountain3').
card_uid('mountain'/'DDP', 'DDP:Mountain:mountain3').
card_rarity('mountain'/'DDP', 'Basic Land').
card_artist('mountain'/'DDP', 'James Paick').
card_number('mountain'/'DDP', '75').
card_multiverse_id('mountain'/'DDP', '401717').

card_in_set('oblivion sower', 'DDP').
card_original_type('oblivion sower'/'DDP', 'Creature — Eldrazi').
card_original_text('oblivion sower'/'DDP', 'When you cast Oblivion Sower, target opponent exiles the top four cards of his or her library, then you may put any number of land cards that player owns from exile onto the battlefield under your control.').
card_first_print('oblivion sower', 'DDP').
card_image_name('oblivion sower'/'DDP', 'oblivion sower').
card_uid('oblivion sower'/'DDP', 'DDP:Oblivion Sower:oblivion sower').
card_rarity('oblivion sower'/'DDP', 'Mythic Rare').
card_artist('oblivion sower'/'DDP', 'Aleksi Briclot').
card_number('oblivion sower'/'DDP', '41').
card_flavor_text('oblivion sower'/'DDP', 'The Eldrazi hunger without limit and consume without pause.').
card_multiverse_id('oblivion sower'/'DDP', '401683').

card_in_set('ondu giant', 'DDP').
card_original_type('ondu giant'/'DDP', 'Creature — Giant Druid').
card_original_text('ondu giant'/'DDP', 'When Ondu Giant enters the battlefield, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_image_name('ondu giant'/'DDP', 'ondu giant').
card_uid('ondu giant'/'DDP', 'DDP:Ondu Giant:ondu giant').
card_rarity('ondu giant'/'DDP', 'Common').
card_artist('ondu giant'/'DDP', 'Igor Kieryluk').
card_number('ondu giant'/'DDP', '19').
card_flavor_text('ondu giant'/'DDP', 'Some druids nurture gardens. Others nurture continents.').
card_multiverse_id('ondu giant'/'DDP', '401661').

card_in_set('oust', 'DDP').
card_original_type('oust'/'DDP', 'Sorcery').
card_original_text('oust'/'DDP', 'Put target creature into its owner\'s library second from the top. Its controller gains 3 life.').
card_image_name('oust'/'DDP', 'oust').
card_uid('oust'/'DDP', 'DDP:Oust:oust').
card_rarity('oust'/'DDP', 'Uncommon').
card_artist('oust'/'DDP', 'Mike Bierek').
card_number('oust'/'DDP', '7').
card_flavor_text('oust'/'DDP', '\"‘Invincible\' is just a word.\"\n—Gideon Jura').
card_multiverse_id('oust'/'DDP', '401649').

card_in_set('pawn of ulamog', 'DDP').
card_original_type('pawn of ulamog'/'DDP', 'Creature — Vampire Shaman').
card_original_text('pawn of ulamog'/'DDP', 'Whenever Pawn of Ulamog or another nontoken creature you control dies, you may put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_image_name('pawn of ulamog'/'DDP', 'pawn of ulamog').
card_uid('pawn of ulamog'/'DDP', 'DDP:Pawn of Ulamog:pawn of ulamog').
card_rarity('pawn of ulamog'/'DDP', 'Uncommon').
card_artist('pawn of ulamog'/'DDP', 'Daarken').
card_number('pawn of ulamog'/'DDP', '55').
card_multiverse_id('pawn of ulamog'/'DDP', '401697').

card_in_set('plains', 'DDP').
card_original_type('plains'/'DDP', 'Basic Land — Plains').
card_original_text('plains'/'DDP', 'W').
card_image_name('plains'/'DDP', 'plains1').
card_uid('plains'/'DDP', 'DDP:Plains:plains1').
card_rarity('plains'/'DDP', 'Basic Land').
card_artist('plains'/'DDP', 'John Avon').
card_number('plains'/'DDP', '35').
card_multiverse_id('plains'/'DDP', '401677').

card_in_set('plains', 'DDP').
card_original_type('plains'/'DDP', 'Basic Land — Plains').
card_original_text('plains'/'DDP', 'W').
card_image_name('plains'/'DDP', 'plains2').
card_uid('plains'/'DDP', 'DDP:Plains:plains2').
card_rarity('plains'/'DDP', 'Basic Land').
card_artist('plains'/'DDP', 'Jung Park').
card_number('plains'/'DDP', '36').
card_multiverse_id('plains'/'DDP', '401678').

card_in_set('plains', 'DDP').
card_original_type('plains'/'DDP', 'Basic Land — Plains').
card_original_text('plains'/'DDP', 'W').
card_image_name('plains'/'DDP', 'plains3').
card_uid('plains'/'DDP', 'DDP:Plains:plains3').
card_rarity('plains'/'DDP', 'Basic Land').
card_artist('plains'/'DDP', 'Vincent Proce').
card_number('plains'/'DDP', '37').
card_multiverse_id('plains'/'DDP', '401679').

card_in_set('plant', 'DDP').
card_original_type('plant'/'DDP', 'Token Creature — Plant').
card_original_text('plant'/'DDP', '').
card_first_print('plant', 'DDP').
card_image_name('plant'/'DDP', 'plant').
card_uid('plant'/'DDP', 'DDP:Plant:plant').
card_rarity('plant'/'DDP', 'Common').
card_artist('plant'/'DDP', 'Daren Bader').
card_number('plant'/'DDP', '80').
card_multiverse_id('plant'/'DDP', '401722').

card_in_set('primal command', 'DDP').
card_original_type('primal command'/'DDP', 'Sorcery').
card_original_text('primal command'/'DDP', 'Choose two —\n• Target player gains 7 life.\n• Put target noncreature permanent on top of its owner\'s library.\n• Target player shuffles his or her graveyard into his or her library.\n• Search your library for a creature card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('primal command'/'DDP', 'primal command').
card_uid('primal command'/'DDP', 'DDP:Primal Command:primal command').
card_rarity('primal command'/'DDP', 'Rare').
card_artist('primal command'/'DDP', 'Magali Villeneuve').
card_number('primal command'/'DDP', '20').
card_multiverse_id('primal command'/'DDP', '401662').

card_in_set('read the bones', 'DDP').
card_original_type('read the bones'/'DDP', 'Sorcery').
card_original_text('read the bones'/'DDP', 'Scry 2, then draw two cards. You lose 2 life. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('read the bones'/'DDP', 'read the bones').
card_uid('read the bones'/'DDP', 'DDP:Read the Bones:read the bones').
card_rarity('read the bones'/'DDP', 'Common').
card_artist('read the bones'/'DDP', 'Lars Grant-West').
card_number('read the bones'/'DDP', '56').
card_flavor_text('read the bones'/'DDP', 'The dead know lessons the living haven\'t learned.').
card_multiverse_id('read the bones'/'DDP', '401698').

card_in_set('repel the darkness', 'DDP').
card_original_type('repel the darkness'/'DDP', 'Instant').
card_original_text('repel the darkness'/'DDP', 'Tap up to two target creatures.\nDraw a card.').
card_image_name('repel the darkness'/'DDP', 'repel the darkness').
card_uid('repel the darkness'/'DDP', 'DDP:Repel the Darkness:repel the darkness').
card_rarity('repel the darkness'/'DDP', 'Common').
card_artist('repel the darkness'/'DDP', 'Scott Chou').
card_number('repel the darkness'/'DDP', '8').
card_flavor_text('repel the darkness'/'DDP', 'A boon to those who cannot see in the dark. A bane to those who live in it.').
card_multiverse_id('repel the darkness'/'DDP', '401650').

card_in_set('retreat to kazandu', 'DDP').
card_original_type('retreat to kazandu'/'DDP', 'Enchantment').
card_original_text('retreat to kazandu'/'DDP', 'Landfall — Whenever a land enters the battlefield under your control, choose one —\n• Put a +1/+1 counter on target creature.\n• You gain 2 life.').
card_first_print('retreat to kazandu', 'DDP').
card_image_name('retreat to kazandu'/'DDP', 'retreat to kazandu').
card_uid('retreat to kazandu'/'DDP', 'DDP:Retreat to Kazandu:retreat to kazandu').
card_rarity('retreat to kazandu'/'DDP', 'Uncommon').
card_artist('retreat to kazandu'/'DDP', 'Kieran Yanner').
card_number('retreat to kazandu'/'DDP', '21').
card_multiverse_id('retreat to kazandu'/'DDP', '401663').

card_in_set('rocky tar pit', 'DDP').
card_original_type('rocky tar pit'/'DDP', 'Land').
card_original_text('rocky tar pit'/'DDP', 'Rocky Tar Pit enters the battlefield tapped.\n{T}, Sacrifice Rocky Tar Pit: Search your library for a Swamp or Mountain card and put it onto the battlefield. Then shuffle your library.').
card_image_name('rocky tar pit'/'DDP', 'rocky tar pit').
card_uid('rocky tar pit'/'DDP', 'DDP:Rocky Tar Pit:rocky tar pit').
card_rarity('rocky tar pit'/'DDP', 'Uncommon').
card_artist('rocky tar pit'/'DDP', 'Jeff Miracola').
card_number('rocky tar pit'/'DDP', '69').
card_multiverse_id('rocky tar pit'/'DDP', '401711').

card_in_set('runed servitor', 'DDP').
card_original_type('runed servitor'/'DDP', 'Artifact Creature — Construct').
card_original_text('runed servitor'/'DDP', 'When Runed Servitor dies, each player draws a card.').
card_image_name('runed servitor'/'DDP', 'runed servitor').
card_uid('runed servitor'/'DDP', 'DDP:Runed Servitor:runed servitor').
card_rarity('runed servitor'/'DDP', 'Uncommon').
card_artist('runed servitor'/'DDP', 'Mike Bierek').
card_number('runed servitor'/'DDP', '66').
card_flavor_text('runed servitor'/'DDP', 'Scholars had puzzled for centuries over the ruins at Tal Terig. Its secrets had always lived within one rune-carved head.').
card_multiverse_id('runed servitor'/'DDP', '401708').

card_in_set('scute mob', 'DDP').
card_original_type('scute mob'/'DDP', 'Creature — Insect').
card_original_text('scute mob'/'DDP', 'At the beginning of your upkeep, if you control five or more lands, put four +1/+1 counters on Scute Mob.').
card_image_name('scute mob'/'DDP', 'scute mob').
card_uid('scute mob'/'DDP', 'DDP:Scute Mob:scute mob').
card_rarity('scute mob'/'DDP', 'Rare').
card_artist('scute mob'/'DDP', 'Zoltan Boros & Gabor Szikszai').
card_number('scute mob'/'DDP', '22').
card_flavor_text('scute mob'/'DDP', '\"Survival rule 781: There are always more scute bugs.\"\n—Zurdi, goblin shortcutter').
card_multiverse_id('scute mob'/'DDP', '401664').

card_in_set('seer\'s sundial', 'DDP').
card_original_type('seer\'s sundial'/'DDP', 'Artifact').
card_original_text('seer\'s sundial'/'DDP', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {2}. If you do, draw a card.').
card_image_name('seer\'s sundial'/'DDP', 'seer\'s sundial').
card_uid('seer\'s sundial'/'DDP', 'DDP:Seer\'s Sundial:seer\'s sundial').
card_rarity('seer\'s sundial'/'DDP', 'Rare').
card_artist('seer\'s sundial'/'DDP', 'Franz Vohwinkel').
card_number('seer\'s sundial'/'DDP', '29').
card_flavor_text('seer\'s sundial'/'DDP', '\"The shadow travels toward the apex. I predict we will soon see the true measure of darkness.\"').
card_multiverse_id('seer\'s sundial'/'DDP', '401671').

card_in_set('sheer drop', 'DDP').
card_original_type('sheer drop'/'DDP', 'Sorcery').
card_original_text('sheer drop'/'DDP', 'Destroy target tapped creature.\nAwaken 3—{5}{W} (If you cast this spell for {5}{W}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_first_print('sheer drop', 'DDP').
card_image_name('sheer drop'/'DDP', 'sheer drop').
card_uid('sheer drop'/'DDP', 'DDP:Sheer Drop:sheer drop').
card_rarity('sheer drop'/'DDP', 'Common').
card_artist('sheer drop'/'DDP', 'Clint Cearley').
card_number('sheer drop'/'DDP', '9').
card_multiverse_id('sheer drop'/'DDP', '401651').

card_in_set('smother', 'DDP').
card_original_type('smother'/'DDP', 'Instant').
card_original_text('smother'/'DDP', 'Destroy target creature with converted mana cost 3 or less. It can\'t be regenerated.').
card_image_name('smother'/'DDP', 'smother').
card_uid('smother'/'DDP', 'DDP:Smother:smother').
card_rarity('smother'/'DDP', 'Uncommon').
card_artist('smother'/'DDP', 'Karl Kopinski').
card_number('smother'/'DDP', '57').
card_flavor_text('smother'/'DDP', '\"Before I hire new recruits, I test how long they can hold their breath. You\'d be surprised how often it comes up.\"\n—Zahr Gada, Halimar expedition leader').
card_multiverse_id('smother'/'DDP', '401699').

card_in_set('stirring wildwood', 'DDP').
card_original_type('stirring wildwood'/'DDP', 'Land').
card_original_text('stirring wildwood'/'DDP', 'Stirring Wildwood enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.\n{1}{G}{W}: Until end of turn, Stirring Wildwood becomes a 3/4 green and white Elemental creature with reach. It\'s still a land. (It can block creatures with flying.)').
card_image_name('stirring wildwood'/'DDP', 'stirring wildwood').
card_uid('stirring wildwood'/'DDP', 'DDP:Stirring Wildwood:stirring wildwood').
card_rarity('stirring wildwood'/'DDP', 'Rare').
card_artist('stirring wildwood'/'DDP', 'Eric Deschamps').
card_number('stirring wildwood'/'DDP', '33').
card_multiverse_id('stirring wildwood'/'DDP', '401675').

card_in_set('stonework puma', 'DDP').
card_original_type('stonework puma'/'DDP', 'Artifact Creature — Cat Ally').
card_original_text('stonework puma'/'DDP', '').
card_image_name('stonework puma'/'DDP', 'stonework puma').
card_uid('stonework puma'/'DDP', 'DDP:Stonework Puma:stonework puma').
card_rarity('stonework puma'/'DDP', 'Common').
card_artist('stonework puma'/'DDP', 'Christopher Moeller').
card_number('stonework puma'/'DDP', '30').
card_flavor_text('stonework puma'/'DDP', '\"We suffer uneasy ground, unstable alliances, and unpredictable magic. Something you can truly trust is worth more than a chest of gold.\"\n—Nikou, Joraga bard').
card_multiverse_id('stonework puma'/'DDP', '401672').

card_in_set('swamp', 'DDP').
card_original_type('swamp'/'DDP', 'Basic Land — Swamp').
card_original_text('swamp'/'DDP', 'B').
card_image_name('swamp'/'DDP', 'swamp1').
card_uid('swamp'/'DDP', 'DDP:Swamp:swamp1').
card_rarity('swamp'/'DDP', 'Basic Land').
card_artist('swamp'/'DDP', 'Véronique Meignaud').
card_number('swamp'/'DDP', '70').
card_multiverse_id('swamp'/'DDP', '401712').

card_in_set('swamp', 'DDP').
card_original_type('swamp'/'DDP', 'Basic Land — Swamp').
card_original_text('swamp'/'DDP', 'B').
card_image_name('swamp'/'DDP', 'swamp2').
card_uid('swamp'/'DDP', 'DDP:Swamp:swamp2').
card_rarity('swamp'/'DDP', 'Basic Land').
card_artist('swamp'/'DDP', 'Véronique Meignaud').
card_number('swamp'/'DDP', '71').
card_multiverse_id('swamp'/'DDP', '401713').

card_in_set('swamp', 'DDP').
card_original_type('swamp'/'DDP', 'Basic Land — Swamp').
card_original_text('swamp'/'DDP', 'B').
card_image_name('swamp'/'DDP', 'swamp3').
card_uid('swamp'/'DDP', 'DDP:Swamp:swamp3').
card_rarity('swamp'/'DDP', 'Basic Land').
card_artist('swamp'/'DDP', 'Véronique Meignaud').
card_number('swamp'/'DDP', '72').
card_multiverse_id('swamp'/'DDP', '401714').

card_in_set('tajuru archer', 'DDP').
card_original_type('tajuru archer'/'DDP', 'Creature — Elf Archer Ally').
card_original_text('tajuru archer'/'DDP', 'Whenever Tajuru Archer or another Ally enters the battlefield under your control, you may have Tajuru Archer deal damage to target creature with flying equal to the number of Allies you control.').
card_image_name('tajuru archer'/'DDP', 'tajuru archer').
card_uid('tajuru archer'/'DDP', 'DDP:Tajuru Archer:tajuru archer').
card_rarity('tajuru archer'/'DDP', 'Uncommon').
card_artist('tajuru archer'/'DDP', 'Chris Rahn').
card_number('tajuru archer'/'DDP', '23').
card_multiverse_id('tajuru archer'/'DDP', '401665').

card_in_set('territorial baloth', 'DDP').
card_original_type('territorial baloth'/'DDP', 'Creature — Beast').
card_original_text('territorial baloth'/'DDP', 'Landfall — Whenever a land enters the battlefield under your control, Territorial Baloth gets +2/+2 until end of turn.').
card_image_name('territorial baloth'/'DDP', 'territorial baloth').
card_uid('territorial baloth'/'DDP', 'DDP:Territorial Baloth:territorial baloth').
card_rarity('territorial baloth'/'DDP', 'Common').
card_artist('territorial baloth'/'DDP', 'Jesper Ejsing').
card_number('territorial baloth'/'DDP', '24').
card_flavor_text('territorial baloth'/'DDP', 'Its territory is defined by wherever it is at the moment.').
card_multiverse_id('territorial baloth'/'DDP', '401666').

card_in_set('torch slinger', 'DDP').
card_original_type('torch slinger'/'DDP', 'Creature — Goblin Shaman').
card_original_text('torch slinger'/'DDP', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nWhen Torch Slinger enters the battlefield, if it was kicked, it deals 2 damage to target creature.').
card_image_name('torch slinger'/'DDP', 'torch slinger').
card_uid('torch slinger'/'DDP', 'DDP:Torch Slinger:torch slinger').
card_rarity('torch slinger'/'DDP', 'Common').
card_artist('torch slinger'/'DDP', 'Andrew Robinson').
card_number('torch slinger'/'DDP', '63').
card_flavor_text('torch slinger'/'DDP', 'Once thrown, twice burned.').
card_multiverse_id('torch slinger'/'DDP', '401705').

card_in_set('turntimber basilisk', 'DDP').
card_original_type('turntimber basilisk'/'DDP', 'Creature — Basilisk').
card_original_text('turntimber basilisk'/'DDP', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nLandfall — Whenever a land enters the battlefield under your control, you may have target creature block Turntimber Basilisk this turn if able.').
card_image_name('turntimber basilisk'/'DDP', 'turntimber basilisk').
card_uid('turntimber basilisk'/'DDP', 'DDP:Turntimber Basilisk:turntimber basilisk').
card_rarity('turntimber basilisk'/'DDP', 'Uncommon').
card_artist('turntimber basilisk'/'DDP', 'Goran Josic').
card_number('turntimber basilisk'/'DDP', '25').
card_multiverse_id('turntimber basilisk'/'DDP', '401667').

card_in_set('turntimber grove', 'DDP').
card_original_type('turntimber grove'/'DDP', 'Land').
card_original_text('turntimber grove'/'DDP', 'Turntimber Grove enters the battlefield tapped.\nWhen Turntimber Grove enters the battlefield, target creature gets +1/+1 until end of turn.\n{T}: Add {G} to your mana pool.').
card_image_name('turntimber grove'/'DDP', 'turntimber grove').
card_uid('turntimber grove'/'DDP', 'DDP:Turntimber Grove:turntimber grove').
card_rarity('turntimber grove'/'DDP', 'Common').
card_artist('turntimber grove'/'DDP', 'Rob Alexander').
card_number('turntimber grove'/'DDP', '34').
card_multiverse_id('turntimber grove'/'DDP', '401676').

card_in_set('ulamog\'s crusher', 'DDP').
card_original_type('ulamog\'s crusher'/'DDP', 'Creature — Eldrazi').
card_original_text('ulamog\'s crusher'/'DDP', 'Annihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)\nUlamog\'s Crusher attacks each turn if able.').
card_image_name('ulamog\'s crusher'/'DDP', 'ulamog\'s crusher').
card_uid('ulamog\'s crusher'/'DDP', 'DDP:Ulamog\'s Crusher:ulamog\'s crusher').
card_rarity('ulamog\'s crusher'/'DDP', 'Common').
card_artist('ulamog\'s crusher'/'DDP', 'Todd Lockwood').
card_number('ulamog\'s crusher'/'DDP', '44').
card_flavor_text('ulamog\'s crusher'/'DDP', '\"Whatever the Eldrazi\'s purpose is, it has nothing to do with something so insignificant as us.\"\n—Nirthu, lone missionary').
card_multiverse_id('ulamog\'s crusher'/'DDP', '401686').

card_in_set('vampire nighthawk', 'DDP').
card_original_type('vampire nighthawk'/'DDP', 'Creature — Vampire Shaman').
card_original_text('vampire nighthawk'/'DDP', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vampire nighthawk'/'DDP', 'vampire nighthawk').
card_uid('vampire nighthawk'/'DDP', 'DDP:Vampire Nighthawk:vampire nighthawk').
card_rarity('vampire nighthawk'/'DDP', 'Uncommon').
card_artist('vampire nighthawk'/'DDP', 'Jason Chan').
card_number('vampire nighthawk'/'DDP', '58').
card_multiverse_id('vampire nighthawk'/'DDP', '401700').

card_in_set('veteran warleader', 'DDP').
card_original_type('veteran warleader'/'DDP', 'Creature — Human Soldier Ally').
card_original_text('veteran warleader'/'DDP', 'Veteran Warleader\'s power and toughness are each equal to the number of creatures you control.\nTap another untapped Ally you control: Veteran Warleader gains your choice of first strike, vigilance, or trample until end of turn.').
card_first_print('veteran warleader', 'DDP').
card_image_name('veteran warleader'/'DDP', 'veteran warleader').
card_uid('veteran warleader'/'DDP', 'DDP:Veteran Warleader:veteran warleader').
card_rarity('veteran warleader'/'DDP', 'Rare').
card_artist('veteran warleader'/'DDP', 'Josu Hernaiz').
card_number('veteran warleader'/'DDP', '27').
card_multiverse_id('veteran warleader'/'DDP', '401669').

card_in_set('wildheart invoker', 'DDP').
card_original_type('wildheart invoker'/'DDP', 'Creature — Elf Shaman').
card_original_text('wildheart invoker'/'DDP', '{8}: Target creature gets +5/+5 and gains trample until end of turn.').
card_image_name('wildheart invoker'/'DDP', 'wildheart invoker').
card_uid('wildheart invoker'/'DDP', 'DDP:Wildheart Invoker:wildheart invoker').
card_rarity('wildheart invoker'/'DDP', 'Common').
card_artist('wildheart invoker'/'DDP', 'Erica Yang').
card_number('wildheart invoker'/'DDP', '26').
card_flavor_text('wildheart invoker'/'DDP', '\"Life as we know it dangles on the brink of extinction. We must show the strength they would steal from us.\"\n—The Invokers\' Tales').
card_multiverse_id('wildheart invoker'/'DDP', '401668').
