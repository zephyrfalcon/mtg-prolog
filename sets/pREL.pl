% Release Events

set('pREL').
set_name('pREL', 'Release Events').
set_release_date('pREL', '2003-07-26').
set_border('pREL', 'black').
set_type('pREL', 'promo').

card_in_set('ass whuppin\'', 'pREL').
card_original_type('ass whuppin\''/'pREL', 'Sorcery').
card_original_text('ass whuppin\''/'pREL', '').
card_first_print('ass whuppin\'', 'pREL').
card_border('ass whuppin\''/'pREL', 'silver').
card_image_name('ass whuppin\''/'pREL', 'ass whuppin\'').
card_uid('ass whuppin\''/'pREL', 'pREL:Ass Whuppin\':ass whuppin\'').
card_rarity('ass whuppin\''/'pREL', 'Special').
card_artist('ass whuppin\''/'pREL', 'Don Hazeltine').
card_number('ass whuppin\''/'pREL', '2').
card_flavor_text('ass whuppin\''/'pREL', 'Asses to ashes,\nDonkeys to dust.').

card_in_set('azorius guildmage', 'pREL').
card_original_type('azorius guildmage'/'pREL', 'Creature — Vedalken Wizard').
card_original_text('azorius guildmage'/'pREL', '').
card_first_print('azorius guildmage', 'pREL').
card_image_name('azorius guildmage'/'pREL', 'azorius guildmage').
card_uid('azorius guildmage'/'pREL', 'pREL:Azorius Guildmage:azorius guildmage').
card_rarity('azorius guildmage'/'pREL', 'Special').
card_artist('azorius guildmage'/'pREL', 'Christopher Moeller').
card_number('azorius guildmage'/'pREL', '9').

card_in_set('budoka pupil', 'pREL').
card_original_type('budoka pupil'/'pREL', 'Creature — Human Monk').
card_original_text('budoka pupil'/'pREL', '').
card_first_print('budoka pupil', 'pREL').
card_image_name('budoka pupil'/'pREL', 'budoka pupil').
card_uid('budoka pupil'/'pREL', 'pREL:Budoka Pupil:budoka pupil').
card_rarity('budoka pupil'/'pREL', 'Special').
card_artist('budoka pupil'/'pREL', 'Shishizaru').
card_number('budoka pupil'/'pREL', '3a').

card_in_set('dimir guildmage', 'pREL').
card_original_type('dimir guildmage'/'pREL', 'Creature — Human Wizard').
card_original_text('dimir guildmage'/'pREL', '').
card_first_print('dimir guildmage', 'pREL').
card_image_name('dimir guildmage'/'pREL', 'dimir guildmage').
card_uid('dimir guildmage'/'pREL', 'pREL:Dimir Guildmage:dimir guildmage').
card_rarity('dimir guildmage'/'pREL', 'Special').
card_artist('dimir guildmage'/'pREL', 'Adam Rex').
card_number('dimir guildmage'/'pREL', '7').

card_in_set('force of nature', 'pREL').
card_original_type('force of nature'/'pREL', 'Creature — Elemental').
card_original_text('force of nature'/'pREL', '').
card_image_name('force of nature'/'pREL', 'force of nature').
card_uid('force of nature'/'pREL', 'pREL:Force of Nature:force of nature').
card_rarity('force of nature'/'pREL', 'Special').
card_artist('force of nature'/'pREL', 'Greg Staples').
card_number('force of nature'/'pREL', '5').

card_in_set('ghost-lit raider', 'pREL').
card_original_type('ghost-lit raider'/'pREL', 'Creature — Spirit').
card_original_text('ghost-lit raider'/'pREL', '').
card_first_print('ghost-lit raider', 'pREL').
card_image_name('ghost-lit raider'/'pREL', 'ghost-lit raider').
card_uid('ghost-lit raider'/'pREL', 'pREL:Ghost-Lit Raider:ghost-lit raider').
card_rarity('ghost-lit raider'/'pREL', 'Special').
card_artist('ghost-lit raider'/'pREL', 'Ittoku').
card_number('ghost-lit raider'/'pREL', '4').

card_in_set('gruul guildmage', 'pREL').
card_original_type('gruul guildmage'/'pREL', 'Creature — Human Shaman').
card_original_text('gruul guildmage'/'pREL', '').
card_first_print('gruul guildmage', 'pREL').
card_image_name('gruul guildmage'/'pREL', 'gruul guildmage').
card_uid('gruul guildmage'/'pREL', 'pREL:Gruul Guildmage:gruul guildmage').
card_rarity('gruul guildmage'/'pREL', 'Special').
card_artist('gruul guildmage'/'pREL', 'Paolo Parente').
card_number('gruul guildmage'/'pREL', '8').

card_in_set('hedge troll', 'pREL').
card_original_type('hedge troll'/'pREL', 'Creature — Troll Cleric').
card_original_text('hedge troll'/'pREL', '').
card_first_print('hedge troll', 'pREL').
card_image_name('hedge troll'/'pREL', 'hedge troll').
card_uid('hedge troll'/'pREL', 'pREL:Hedge Troll:hedge troll').
card_rarity('hedge troll'/'pREL', 'Special').
card_artist('hedge troll'/'pREL', 'Paolo Parente').
card_number('hedge troll'/'pREL', '11').
card_flavor_text('hedge troll'/'pREL', 'His abode was clean and bare, not a morsel in sight. I asked him what he ate, fearing the answer. He smiled and said his faith alone sustained him.').

card_in_set('ichiga, who topples oaks', 'pREL').
card_original_type('ichiga, who topples oaks'/'pREL', 'Legendary Creature — Spirit').
card_original_text('ichiga, who topples oaks'/'pREL', '').
card_first_print('ichiga, who topples oaks', 'pREL').
card_image_name('ichiga, who topples oaks'/'pREL', 'ichiga, who topples oaks').
card_uid('ichiga, who topples oaks'/'pREL', 'pREL:Ichiga, Who Topples Oaks:ichiga, who topples oaks').
card_rarity('ichiga, who topples oaks'/'pREL', 'Special').
card_artist('ichiga, who topples oaks'/'pREL', 'Shishizaru').
card_number('ichiga, who topples oaks'/'pREL', '3b').

card_in_set('rukh egg', 'pREL').
card_original_type('rukh egg'/'pREL', 'Creature — Bird').
card_original_text('rukh egg'/'pREL', '').
card_image_name('rukh egg'/'pREL', 'rukh egg').
card_uid('rukh egg'/'pREL', 'pREL:Rukh Egg:rukh egg').
card_rarity('rukh egg'/'pREL', 'Special').
card_artist('rukh egg'/'pREL', 'Mark Zug').
card_number('rukh egg'/'pREL', '1').
card_flavor_text('rukh egg'/'pREL', 'Called \"stonefeathers\" by many lowlanders, rukhs are thought to descend from a phoenix that sacrificed its flame for a body of stone.').

card_in_set('shivan dragon', 'pREL').
card_original_type('shivan dragon'/'pREL', 'Creature — Dragon').
card_original_text('shivan dragon'/'pREL', '').
card_image_name('shivan dragon'/'pREL', 'shivan dragon').
card_uid('shivan dragon'/'pREL', 'pREL:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'pREL', 'Special').
card_artist('shivan dragon'/'pREL', 'Donato Giancola').
card_number('shivan dragon'/'pREL', '6').
card_flavor_text('shivan dragon'/'pREL', 'The undisputed master of the mountains of Shiv.').

card_in_set('shriekmaw', 'pREL').
card_original_type('shriekmaw'/'pREL', 'Creature — Elemental').
card_original_text('shriekmaw'/'pREL', '').
card_first_print('shriekmaw', 'pREL').
card_image_name('shriekmaw'/'pREL', 'shriekmaw').
card_uid('shriekmaw'/'pREL', 'pREL:Shriekmaw:shriekmaw').
card_rarity('shriekmaw'/'pREL', 'Special').
card_artist('shriekmaw'/'pREL', 'Steve Prescott').
card_number('shriekmaw'/'pREL', '13').

card_in_set('storm entity', 'pREL').
card_original_type('storm entity'/'pREL', 'Creature — Elemental').
card_original_text('storm entity'/'pREL', '').
card_first_print('storm entity', 'pREL').
card_image_name('storm entity'/'pREL', 'storm entity').
card_uid('storm entity'/'pREL', 'pREL:Storm Entity:storm entity').
card_rarity('storm entity'/'pREL', 'Special').
card_artist('storm entity'/'pREL', 'Lucio Parrillo').
card_number('storm entity'/'pREL', '12').
card_flavor_text('storm entity'/'pREL', 'Dweldian magi don\'t enter the Realm of Thrones when they die. Instead their souls are drawn to great works of magic.').

card_in_set('sudden shock', 'pREL').
card_original_type('sudden shock'/'pREL', 'Instant').
card_original_text('sudden shock'/'pREL', '').
card_first_print('sudden shock', 'pREL').
card_image_name('sudden shock'/'pREL', 'sudden shock').
card_uid('sudden shock'/'pREL', 'pREL:Sudden Shock:sudden shock').
card_rarity('sudden shock'/'pREL', 'Special').
card_artist('sudden shock'/'pREL', 'Vance Kovacs').
card_number('sudden shock'/'pREL', '10').
card_flavor_text('sudden shock'/'pREL', 'Arc mages aren\'t known for their patience.').
