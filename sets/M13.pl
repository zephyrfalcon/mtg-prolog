% Magic 2013

set('M13').
set_name('M13', 'Magic 2013').
set_release_date('M13', '2012-07-13').
set_border('M13', 'black').
set_type('M13', 'core').

card_in_set('acidic slime', 'M13').
card_original_type('acidic slime'/'M13', 'Creature — Ooze').
card_original_text('acidic slime'/'M13', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_image_name('acidic slime'/'M13', 'acidic slime').
card_uid('acidic slime'/'M13', 'M13:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'M13', 'Uncommon').
card_artist('acidic slime'/'M13', 'Karl Kopinski').
card_number('acidic slime'/'M13', '159').
card_multiverse_id('acidic slime'/'M13', '265718').

card_in_set('ajani\'s sunstriker', 'M13').
card_original_type('ajani\'s sunstriker'/'M13', 'Creature — Cat Cleric').
card_original_text('ajani\'s sunstriker'/'M13', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('ajani\'s sunstriker', 'M13').
card_image_name('ajani\'s sunstriker'/'M13', 'ajani\'s sunstriker').
card_uid('ajani\'s sunstriker'/'M13', 'M13:Ajani\'s Sunstriker:ajani\'s sunstriker').
card_rarity('ajani\'s sunstriker'/'M13', 'Common').
card_artist('ajani\'s sunstriker'/'M13', 'Matt Stewart').
card_number('ajani\'s sunstriker'/'M13', '2').
card_flavor_text('ajani\'s sunstriker'/'M13', '\"Ajani goes where he is needed most and in his name we keep his lands safe.\"').
card_multiverse_id('ajani\'s sunstriker'/'M13', '259715').

card_in_set('ajani, caller of the pride', 'M13').
card_original_type('ajani, caller of the pride'/'M13', 'Planeswalker — Ajani').
card_original_text('ajani, caller of the pride'/'M13', '+1: Put a +1/+1 counter on up to one target creature.\n-3: Target creature gains flying and double strike until end of turn.\n-8: Put X 2/2 white Cat creature tokens onto the battlefield, where X is your life total.').
card_image_name('ajani, caller of the pride'/'M13', 'ajani, caller of the pride').
card_uid('ajani, caller of the pride'/'M13', 'M13:Ajani, Caller of the Pride:ajani, caller of the pride').
card_rarity('ajani, caller of the pride'/'M13', 'Mythic Rare').
card_artist('ajani, caller of the pride'/'M13', 'D. Alexander Gregory').
card_number('ajani, caller of the pride'/'M13', '1').
card_multiverse_id('ajani, caller of the pride'/'M13', '249695').

card_in_set('akroma\'s memorial', 'M13').
card_original_type('akroma\'s memorial'/'M13', 'Legendary Artifact').
card_original_text('akroma\'s memorial'/'M13', 'Creatures you control have flying, first strike, vigilance, trample, haste, and protection from black and from red.').
card_image_name('akroma\'s memorial'/'M13', 'akroma\'s memorial').
card_uid('akroma\'s memorial'/'M13', 'M13:Akroma\'s Memorial:akroma\'s memorial').
card_rarity('akroma\'s memorial'/'M13', 'Mythic Rare').
card_artist('akroma\'s memorial'/'M13', 'Dan Scott').
card_number('akroma\'s memorial'/'M13', '200').
card_flavor_text('akroma\'s memorial'/'M13', '\"No rest. No mercy. No matter what.\"\n—Memorial inscription').
card_multiverse_id('akroma\'s memorial'/'M13', '279712').

card_in_set('angel\'s mercy', 'M13').
card_original_type('angel\'s mercy'/'M13', 'Instant').
card_original_text('angel\'s mercy'/'M13', 'You gain 7 life.').
card_image_name('angel\'s mercy'/'M13', 'angel\'s mercy').
card_uid('angel\'s mercy'/'M13', 'M13:Angel\'s Mercy:angel\'s mercy').
card_rarity('angel\'s mercy'/'M13', 'Common').
card_artist('angel\'s mercy'/'M13', 'Andrew Robinson').
card_number('angel\'s mercy'/'M13', '3').
card_flavor_text('angel\'s mercy'/'M13', '\"She rose above me, and in that moment Varrik the Bloody died and I was born.\"\n—Varrik the Just').
card_multiverse_id('angel\'s mercy'/'M13', '259714').

card_in_set('angelic benediction', 'M13').
card_original_type('angelic benediction'/'M13', 'Enchantment').
card_original_text('angelic benediction'/'M13', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, you may tap target creature.').
card_image_name('angelic benediction'/'M13', 'angelic benediction').
card_uid('angelic benediction'/'M13', 'M13:Angelic Benediction:angelic benediction').
card_rarity('angelic benediction'/'M13', 'Uncommon').
card_artist('angelic benediction'/'M13', 'Michael Komarck').
card_number('angelic benediction'/'M13', '4').
card_flavor_text('angelic benediction'/'M13', '\"Only the bravest are worthy of our blessing.\"').
card_multiverse_id('angelic benediction'/'M13', '279702').

card_in_set('arbor elf', 'M13').
card_original_type('arbor elf'/'M13', 'Creature — Elf Druid').
card_original_text('arbor elf'/'M13', '{T}: Untap target Forest.').
card_image_name('arbor elf'/'M13', 'arbor elf').
card_uid('arbor elf'/'M13', 'M13:Arbor Elf:arbor elf').
card_rarity('arbor elf'/'M13', 'Common').
card_artist('arbor elf'/'M13', 'rk post').
card_number('arbor elf'/'M13', '160').
card_flavor_text('arbor elf'/'M13', '\"The forest will surround you with its life if you are still and calm.\"').
card_multiverse_id('arbor elf'/'M13', '249840').

card_in_set('archaeomancer', 'M13').
card_original_type('archaeomancer'/'M13', 'Creature — Human Wizard').
card_original_text('archaeomancer'/'M13', 'When Archaeomancer enters the battlefield, return target instant or sorcery card from your graveyard to your hand.').
card_first_print('archaeomancer', 'M13').
card_image_name('archaeomancer'/'M13', 'archaeomancer').
card_uid('archaeomancer'/'M13', 'M13:Archaeomancer:archaeomancer').
card_rarity('archaeomancer'/'M13', 'Common').
card_artist('archaeomancer'/'M13', 'Zoltan Boros').
card_number('archaeomancer'/'M13', '41').
card_flavor_text('archaeomancer'/'M13', '\"Words of power never disappear. They sleep, awaiting those with the will to rouse them.\"').
card_multiverse_id('archaeomancer'/'M13', '265722').

card_in_set('arctic aven', 'M13').
card_original_type('arctic aven'/'M13', 'Creature — Bird Wizard').
card_original_text('arctic aven'/'M13', 'Flying\nArctic Aven gets +1/+1 as long as you control a Plains.\n{W}: Arctic Aven gains lifelink until end of turn. (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('arctic aven', 'M13').
card_image_name('arctic aven'/'M13', 'arctic aven').
card_uid('arctic aven'/'M13', 'M13:Arctic Aven:arctic aven').
card_rarity('arctic aven'/'M13', 'Uncommon').
card_artist('arctic aven'/'M13', 'Igor Kieryluk').
card_number('arctic aven'/'M13', '42').
card_multiverse_id('arctic aven'/'M13', '249710').

card_in_set('arms dealer', 'M13').
card_original_type('arms dealer'/'M13', 'Creature — Goblin Rogue').
card_original_text('arms dealer'/'M13', '{1}{R}, Sacrifice a Goblin: Arms Dealer deals 4 damage to target creature.').
card_image_name('arms dealer'/'M13', 'arms dealer').
card_uid('arms dealer'/'M13', 'M13:Arms Dealer:arms dealer').
card_rarity('arms dealer'/'M13', 'Uncommon').
card_artist('arms dealer'/'M13', 'Wayne Reynolds').
card_number('arms dealer'/'M13', '120').
card_flavor_text('arms dealer'/'M13', '\"Guaranteed to leave your enemies sufficiently stabbed, bitten, cleaved, or exploded.\"\n—Grezkrick\'s Emporium motto').
card_multiverse_id('arms dealer'/'M13', '265719').

card_in_set('attended knight', 'M13').
card_original_type('attended knight'/'M13', 'Creature — Human Knight').
card_original_text('attended knight'/'M13', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhen Attended Knight enters the battlefield, put a 1/1 white Soldier creature token onto the battlefield.').
card_first_print('attended knight', 'M13').
card_image_name('attended knight'/'M13', 'attended knight').
card_uid('attended knight'/'M13', 'M13:Attended Knight:attended knight').
card_rarity('attended knight'/'M13', 'Common').
card_artist('attended knight'/'M13', 'Seb McKinnon').
card_number('attended knight'/'M13', '5').
card_multiverse_id('attended knight'/'M13', '265723').

card_in_set('augur of bolas', 'M13').
card_original_type('augur of bolas'/'M13', 'Creature — Merfolk Wizard').
card_original_text('augur of bolas'/'M13', 'When Augur of Bolas enters the battlefield, look at the top three cards of your library. You may reveal an instant or sorcery card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_first_print('augur of bolas', 'M13').
card_image_name('augur of bolas'/'M13', 'augur of bolas').
card_uid('augur of bolas'/'M13', 'M13:Augur of Bolas:augur of bolas').
card_rarity('augur of bolas'/'M13', 'Uncommon').
card_artist('augur of bolas'/'M13', 'Slawomir Maniak').
card_number('augur of bolas'/'M13', '43').
card_multiverse_id('augur of bolas'/'M13', '249676').

card_in_set('aven squire', 'M13').
card_original_type('aven squire'/'M13', 'Creature — Bird Soldier').
card_original_text('aven squire'/'M13', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_image_name('aven squire'/'M13', 'aven squire').
card_uid('aven squire'/'M13', 'M13:Aven Squire:aven squire').
card_rarity('aven squire'/'M13', 'Common').
card_artist('aven squire'/'M13', 'David Palumbo').
card_number('aven squire'/'M13', '6').
card_flavor_text('aven squire'/'M13', 'When the meek charge into battle, courage becomes infectious.').
card_multiverse_id('aven squire'/'M13', '265712').

card_in_set('battle of wits', 'M13').
card_original_type('battle of wits'/'M13', 'Enchantment').
card_original_text('battle of wits'/'M13', 'At the beginning of your upkeep, if you have 200 or more cards in your library, you win the game.').
card_image_name('battle of wits'/'M13', 'battle of wits').
card_uid('battle of wits'/'M13', 'M13:Battle of Wits:battle of wits').
card_rarity('battle of wits'/'M13', 'Rare').
card_artist('battle of wits'/'M13', 'Jason Chan').
card_number('battle of wits'/'M13', '44').
card_flavor_text('battle of wits'/'M13', 'There is no loftier ambition than the pursuit of knowledge.').
card_multiverse_id('battle of wits'/'M13', '288878').

card_in_set('battleflight eagle', 'M13').
card_original_type('battleflight eagle'/'M13', 'Creature — Bird').
card_original_text('battleflight eagle'/'M13', 'Flying\nWhen Battleflight Eagle enters the battlefield, target creature gets +2/+2 and gains flying until end of turn.').
card_first_print('battleflight eagle', 'M13').
card_image_name('battleflight eagle'/'M13', 'battleflight eagle').
card_uid('battleflight eagle'/'M13', 'M13:Battleflight Eagle:battleflight eagle').
card_rarity('battleflight eagle'/'M13', 'Common').
card_artist('battleflight eagle'/'M13', 'Kev Walker').
card_number('battleflight eagle'/'M13', '7').
card_flavor_text('battleflight eagle'/'M13', 'Eagles make fine mounts, but they\'re too proud to carry anyone for long.').
card_multiverse_id('battleflight eagle'/'M13', '259721').

card_in_set('bladetusk boar', 'M13').
card_original_type('bladetusk boar'/'M13', 'Creature — Boar').
card_original_text('bladetusk boar'/'M13', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_image_name('bladetusk boar'/'M13', 'bladetusk boar').
card_uid('bladetusk boar'/'M13', 'M13:Bladetusk Boar:bladetusk boar').
card_rarity('bladetusk boar'/'M13', 'Common').
card_artist('bladetusk boar'/'M13', 'Paul Bonner').
card_number('bladetusk boar'/'M13', '121').
card_flavor_text('bladetusk boar'/'M13', 'Those who dare stand in its path are either brave or mindless. Or in the case of goblins, both.').
card_multiverse_id('bladetusk boar'/'M13', '279707').

card_in_set('blood reckoning', 'M13').
card_original_type('blood reckoning'/'M13', 'Enchantment').
card_original_text('blood reckoning'/'M13', 'Whenever a creature attacks you or a planeswalker you control, that creature\'s controller loses 1 life.').
card_first_print('blood reckoning', 'M13').
card_image_name('blood reckoning'/'M13', 'blood reckoning').
card_uid('blood reckoning'/'M13', 'M13:Blood Reckoning:blood reckoning').
card_rarity('blood reckoning'/'M13', 'Uncommon').
card_artist('blood reckoning'/'M13', 'Wayne Reynolds').
card_number('blood reckoning'/'M13', '81').
card_flavor_text('blood reckoning'/'M13', '\"There is no greater folly than standing against me.\"\n—Nicol Bolas').
card_multiverse_id('blood reckoning'/'M13', '279991').

card_in_set('bloodhunter bat', 'M13').
card_original_type('bloodhunter bat'/'M13', 'Creature — Bat').
card_original_text('bloodhunter bat'/'M13', 'Flying\nWhen Bloodhunter Bat enters the battlefield, target player loses 2 life and you gain 2 life.').
card_first_print('bloodhunter bat', 'M13').
card_image_name('bloodhunter bat'/'M13', 'bloodhunter bat').
card_uid('bloodhunter bat'/'M13', 'M13:Bloodhunter Bat:bloodhunter bat').
card_rarity('bloodhunter bat'/'M13', 'Common').
card_artist('bloodhunter bat'/'M13', 'Tomasz Jedruszek').
card_number('bloodhunter bat'/'M13', '82').
card_flavor_text('bloodhunter bat'/'M13', 'It returns eager to share the feast of blood and gore with its ghoulish master.').
card_multiverse_id('bloodhunter bat'/'M13', '259705').

card_in_set('bloodthrone vampire', 'M13').
card_original_type('bloodthrone vampire'/'M13', 'Creature — Vampire').
card_original_text('bloodthrone vampire'/'M13', 'Sacrifice a creature: Bloodthrone Vampire gets +2/+2 until end of turn.').
card_image_name('bloodthrone vampire'/'M13', 'bloodthrone vampire').
card_uid('bloodthrone vampire'/'M13', 'M13:Bloodthrone Vampire:bloodthrone vampire').
card_rarity('bloodthrone vampire'/'M13', 'Common').
card_artist('bloodthrone vampire'/'M13', 'Steve Argyle').
card_number('bloodthrone vampire'/'M13', '83').
card_flavor_text('bloodthrone vampire'/'M13', '\"What I need is your blood. What I don\'t need is your permission.\"').
card_multiverse_id('bloodthrone vampire'/'M13', '280298').

card_in_set('bond beetle', 'M13').
card_original_type('bond beetle'/'M13', 'Creature — Insect').
card_original_text('bond beetle'/'M13', 'When Bond Beetle enters the battlefield, put a +1/+1 counter on target creature.').
card_first_print('bond beetle', 'M13').
card_image_name('bond beetle'/'M13', 'bond beetle').
card_uid('bond beetle'/'M13', 'M13:Bond Beetle:bond beetle').
card_rarity('bond beetle'/'M13', 'Common').
card_artist('bond beetle'/'M13', 'John Avon').
card_number('bond beetle'/'M13', '161').
card_flavor_text('bond beetle'/'M13', 'A beetle on your shoulder\n—Elvish expression meaning\n\"a blessing of luck\"').
card_multiverse_id('bond beetle'/'M13', '249696').

card_in_set('boundless realms', 'M13').
card_original_type('boundless realms'/'M13', 'Sorcery').
card_original_text('boundless realms'/'M13', 'Search your library for up to X basic land cards, where X is the number of lands you control, and put them onto the battlefield tapped. Then shuffle your library.').
card_first_print('boundless realms', 'M13').
card_image_name('boundless realms'/'M13', 'boundless realms').
card_uid('boundless realms'/'M13', 'M13:Boundless Realms:boundless realms').
card_rarity('boundless realms'/'M13', 'Rare').
card_artist('boundless realms'/'M13', 'Cliff Childs').
card_number('boundless realms'/'M13', '162').
card_flavor_text('boundless realms'/'M13', '\"Planeswalking cannot be taught. Either you see the doors or you do not.\"\n—Nissa Revane').
card_multiverse_id('boundless realms'/'M13', '249667').

card_in_set('bountiful harvest', 'M13').
card_original_type('bountiful harvest'/'M13', 'Sorcery').
card_original_text('bountiful harvest'/'M13', 'You gain 1 life for each land you control.').
card_image_name('bountiful harvest'/'M13', 'bountiful harvest').
card_uid('bountiful harvest'/'M13', 'M13:Bountiful Harvest:bountiful harvest').
card_rarity('bountiful harvest'/'M13', 'Common').
card_artist('bountiful harvest'/'M13', 'Jason Chan').
card_number('bountiful harvest'/'M13', '163').
card_flavor_text('bountiful harvest'/'M13', '\"When we fail to see the beauty in every tree, we are no better than humans.\"\n—Saelia, elvish scout').
card_multiverse_id('bountiful harvest'/'M13', '278074').

card_in_set('canyon minotaur', 'M13').
card_original_type('canyon minotaur'/'M13', 'Creature — Minotaur Warrior').
card_original_text('canyon minotaur'/'M13', '').
card_image_name('canyon minotaur'/'M13', 'canyon minotaur').
card_uid('canyon minotaur'/'M13', 'M13:Canyon Minotaur:canyon minotaur').
card_rarity('canyon minotaur'/'M13', 'Common').
card_artist('canyon minotaur'/'M13', 'Steve Prescott').
card_number('canyon minotaur'/'M13', '122').
card_flavor_text('canyon minotaur'/'M13', '\"We\'ll scale these cliffs, traverse Brittle Bridge, and then fight our way down the volcanic slopes on the other side.\"\n\"Isn\'t the shortest route through the canyon?\"\n\"Yes.\"\n\"So shouldn\'t we—\"\n\"No.\"').
card_multiverse_id('canyon minotaur'/'M13', '259211').

card_in_set('captain of the watch', 'M13').
card_original_type('captain of the watch'/'M13', 'Creature — Human Soldier').
card_original_text('captain of the watch'/'M13', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nOther Soldier creatures you control get +1/+1 and have vigilance.\nWhen Captain of the Watch enters the battlefield, put three 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('captain of the watch'/'M13', 'captain of the watch').
card_uid('captain of the watch'/'M13', 'M13:Captain of the Watch:captain of the watch').
card_rarity('captain of the watch'/'M13', 'Rare').
card_artist('captain of the watch'/'M13', 'Greg Staples').
card_number('captain of the watch'/'M13', '8').
card_multiverse_id('captain of the watch'/'M13', '259716').

card_in_set('captain\'s call', 'M13').
card_original_type('captain\'s call'/'M13', 'Sorcery').
card_original_text('captain\'s call'/'M13', 'Put three 1/1 white Soldier creature tokens onto the battlefield.').
card_first_print('captain\'s call', 'M13').
card_image_name('captain\'s call'/'M13', 'captain\'s call').
card_uid('captain\'s call'/'M13', 'M13:Captain\'s Call:captain\'s call').
card_rarity('captain\'s call'/'M13', 'Common').
card_artist('captain\'s call'/'M13', 'Greg Staples').
card_number('captain\'s call'/'M13', '9').
card_flavor_text('captain\'s call'/'M13', '\"Who among you has the courage to face certain death? Who will heed my call?\"\n—Rayel Vanger, Firstblade of Thune').
card_multiverse_id('captain\'s call'/'M13', '249689').

card_in_set('cathedral of war', 'M13').
card_original_type('cathedral of war'/'M13', 'Land').
card_original_text('cathedral of war'/'M13', 'Cathedral of War enters the battlefield tapped.\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{T}: Add {1} to your mana pool.').
card_image_name('cathedral of war'/'M13', 'cathedral of war').
card_uid('cathedral of war'/'M13', 'M13:Cathedral of War:cathedral of war').
card_rarity('cathedral of war'/'M13', 'Rare').
card_artist('cathedral of war'/'M13', 'Kekai Kotaki').
card_number('cathedral of war'/'M13', '221').
card_multiverse_id('cathedral of war'/'M13', '259680').

card_in_set('centaur courser', 'M13').
card_original_type('centaur courser'/'M13', 'Creature — Centaur Warrior').
card_original_text('centaur courser'/'M13', '').
card_image_name('centaur courser'/'M13', 'centaur courser').
card_uid('centaur courser'/'M13', 'M13:Centaur Courser:centaur courser').
card_rarity('centaur courser'/'M13', 'Common').
card_artist('centaur courser'/'M13', 'Vance Kovacs').
card_number('centaur courser'/'M13', '164').
card_flavor_text('centaur courser'/'M13', '\"The centaurs are truly free. Never will they be tamed by temptation or controlled by fear. They live in total harmony, a feat not yet achieved by our kind.\"\n—Ramal, sage of Westgate').
card_multiverse_id('centaur courser'/'M13', '279993').

card_in_set('chandra\'s fury', 'M13').
card_original_type('chandra\'s fury'/'M13', 'Instant').
card_original_text('chandra\'s fury'/'M13', 'Chandra\'s Fury deals 4 damage to target player and 1 damage to each creature that player controls.').
card_image_name('chandra\'s fury'/'M13', 'chandra\'s fury').
card_uid('chandra\'s fury'/'M13', 'M13:Chandra\'s Fury:chandra\'s fury').
card_rarity('chandra\'s fury'/'M13', 'Common').
card_artist('chandra\'s fury'/'M13', 'Volkan Baga').
card_number('chandra\'s fury'/'M13', '124').
card_flavor_text('chandra\'s fury'/'M13', '\"I asked if they wanted to do things the easy way. I meant easy for me.\"').
card_multiverse_id('chandra\'s fury'/'M13', '249682').

card_in_set('chandra, the firebrand', 'M13').
card_original_type('chandra, the firebrand'/'M13', 'Planeswalker — Chandra').
card_original_text('chandra, the firebrand'/'M13', '+1: Chandra, the Firebrand deals 1 damage to target creature or player.\n-2: When you cast your next instant or sorcery spell this turn, copy that spell. You may choose new targets for the copy.\n-6: Chandra, the Firebrand deals 6 damage to each of up to six target creatures and/or players.').
card_image_name('chandra, the firebrand'/'M13', 'chandra, the firebrand').
card_uid('chandra, the firebrand'/'M13', 'M13:Chandra, the Firebrand:chandra, the firebrand').
card_rarity('chandra, the firebrand'/'M13', 'Mythic Rare').
card_artist('chandra, the firebrand'/'M13', 'D. Alexander Gregory').
card_number('chandra, the firebrand'/'M13', '123').
card_multiverse_id('chandra, the firebrand'/'M13', '259205').

card_in_set('chronomaton', 'M13').
card_original_type('chronomaton'/'M13', 'Artifact Creature — Golem').
card_original_text('chronomaton'/'M13', '{1}, {T}: Put a +1/+1 counter on Chronomaton.').
card_first_print('chronomaton', 'M13').
card_image_name('chronomaton'/'M13', 'chronomaton').
card_uid('chronomaton'/'M13', 'M13:Chronomaton:chronomaton').
card_rarity('chronomaton'/'M13', 'Uncommon').
card_artist('chronomaton'/'M13', 'Vincent Proce').
card_number('chronomaton'/'M13', '201').
card_flavor_text('chronomaton'/'M13', 'On the third night, the villagers destroyed their clocks. The sounds of whirring gears and chiming metal held only dread for them.').
card_multiverse_id('chronomaton'/'M13', '259698').

card_in_set('cleaver riot', 'M13').
card_original_type('cleaver riot'/'M13', 'Sorcery').
card_original_text('cleaver riot'/'M13', 'Creatures you control gain double strike until end of turn. (They deal both first-strike and regular combat damage.)').
card_first_print('cleaver riot', 'M13').
card_image_name('cleaver riot'/'M13', 'cleaver riot').
card_uid('cleaver riot'/'M13', 'M13:Cleaver Riot:cleaver riot').
card_rarity('cleaver riot'/'M13', 'Uncommon').
card_artist('cleaver riot'/'M13', 'Dave Kendall').
card_number('cleaver riot'/'M13', '125').
card_flavor_text('cleaver riot'/'M13', 'Measure once, cut twice.').
card_multiverse_id('cleaver riot'/'M13', '276207').

card_in_set('clock of omens', 'M13').
card_original_type('clock of omens'/'M13', 'Artifact').
card_original_text('clock of omens'/'M13', 'Tap two untapped artifacts you control: Untap target artifact.').
card_image_name('clock of omens'/'M13', 'clock of omens').
card_uid('clock of omens'/'M13', 'M13:Clock of Omens:clock of omens').
card_rarity('clock of omens'/'M13', 'Uncommon').
card_artist('clock of omens'/'M13', 'Ryan Yee').
card_number('clock of omens'/'M13', '202').
card_flavor_text('clock of omens'/'M13', 'Most clocks measure time, but a few measure everything.').
card_multiverse_id('clock of omens'/'M13', '288877').

card_in_set('clone', 'M13').
card_original_type('clone'/'M13', 'Creature — Shapeshifter').
card_original_text('clone'/'M13', 'You may have Clone enter the battlefield as a copy of any creature on the battlefield.').
card_image_name('clone'/'M13', 'clone').
card_uid('clone'/'M13', 'M13:Clone:clone').
card_rarity('clone'/'M13', 'Rare').
card_artist('clone'/'M13', 'Kev Walker').
card_number('clone'/'M13', '45').
card_flavor_text('clone'/'M13', 'One face, two minds.').
card_multiverse_id('clone'/'M13', '266274').

card_in_set('courtly provocateur', 'M13').
card_original_type('courtly provocateur'/'M13', 'Creature — Human Wizard').
card_original_text('courtly provocateur'/'M13', '{T}: Target creature attacks this turn if able.\n{T}: Target creature blocks this turn if able.').
card_first_print('courtly provocateur', 'M13').
card_image_name('courtly provocateur'/'M13', 'courtly provocateur').
card_uid('courtly provocateur'/'M13', 'M13:Courtly Provocateur:courtly provocateur').
card_rarity('courtly provocateur'/'M13', 'Uncommon').
card_artist('courtly provocateur'/'M13', 'James Ryman').
card_number('courtly provocateur'/'M13', '46').
card_flavor_text('courtly provocateur'/'M13', '\"My one regret is that I have mastered an art that leaves its audience unable to applaud.\"').
card_multiverse_id('courtly provocateur'/'M13', '276475').

card_in_set('cower in fear', 'M13').
card_original_type('cower in fear'/'M13', 'Instant').
card_original_text('cower in fear'/'M13', 'Creatures your opponents control get -1/-1 until end of turn.').
card_first_print('cower in fear', 'M13').
card_image_name('cower in fear'/'M13', 'cower in fear').
card_uid('cower in fear'/'M13', 'M13:Cower in Fear:cower in fear').
card_rarity('cower in fear'/'M13', 'Uncommon').
card_artist('cower in fear'/'M13', 'Nils Hamm').
card_number('cower in fear'/'M13', '84').
card_flavor_text('cower in fear'/'M13', '\"You will fully understand fear when you discover it is the final thing you put your faith in.\"\n—Nicol Bolas').
card_multiverse_id('cower in fear'/'M13', '280232').

card_in_set('craterize', 'M13').
card_original_type('craterize'/'M13', 'Sorcery').
card_original_text('craterize'/'M13', 'Destroy target land.').
card_first_print('craterize', 'M13').
card_image_name('craterize'/'M13', 'craterize').
card_uid('craterize'/'M13', 'M13:Craterize:craterize').
card_rarity('craterize'/'M13', 'Common').
card_artist('craterize'/'M13', 'Eytan Zana').
card_number('craterize'/'M13', '126').
card_flavor_text('craterize'/'M13', '\"‘Utterly\' is my favorite way to destroy something.\"\n—Chandra Nalaar').
card_multiverse_id('craterize'/'M13', '253699').

card_in_set('crimson muckwader', 'M13').
card_original_type('crimson muckwader'/'M13', 'Creature — Lizard').
card_original_text('crimson muckwader'/'M13', 'Crimson Muckwader gets +1/+1 as long as you control a Swamp.\n{2}{B}: Regenerate Crimson Muckwader. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('crimson muckwader', 'M13').
card_image_name('crimson muckwader'/'M13', 'crimson muckwader').
card_uid('crimson muckwader'/'M13', 'M13:Crimson Muckwader:crimson muckwader').
card_rarity('crimson muckwader'/'M13', 'Uncommon').
card_artist('crimson muckwader'/'M13', 'Steven Belledin').
card_number('crimson muckwader'/'M13', '127').
card_multiverse_id('crimson muckwader'/'M13', '253739').

card_in_set('crippling blight', 'M13').
card_original_type('crippling blight'/'M13', 'Enchantment — Aura').
card_original_text('crippling blight'/'M13', 'Enchant creature\nEnchanted creature gets -1/-1 and can\'t block.').
card_first_print('crippling blight', 'M13').
card_image_name('crippling blight'/'M13', 'crippling blight').
card_uid('crippling blight'/'M13', 'M13:Crippling Blight:crippling blight').
card_rarity('crippling blight'/'M13', 'Common').
card_artist('crippling blight'/'M13', 'Lucas Graciano').
card_number('crippling blight'/'M13', '85').
card_flavor_text('crippling blight'/'M13', '\"Still alive? No matter. I\'ll leave you as a warning to others who would oppose me.\"\n—Vish Kal, Blood Arbiter').
card_multiverse_id('crippling blight'/'M13', '253716').

card_in_set('crusader of odric', 'M13').
card_original_type('crusader of odric'/'M13', 'Creature — Human Soldier').
card_original_text('crusader of odric'/'M13', 'Crusader of Odric\'s power and toughness are each equal to the number of creatures you control.').
card_first_print('crusader of odric', 'M13').
card_image_name('crusader of odric'/'M13', 'crusader of odric').
card_uid('crusader of odric'/'M13', 'M13:Crusader of Odric:crusader of odric').
card_rarity('crusader of odric'/'M13', 'Uncommon').
card_artist('crusader of odric'/'M13', 'Michael Komarck').
card_number('crusader of odric'/'M13', '10').
card_flavor_text('crusader of odric'/'M13', '\"We are Odric\'s sword. We strike without fear, for his mind has divined how our foe will be slain.\"').
card_multiverse_id('crusader of odric'/'M13', '253730').

card_in_set('dark favor', 'M13').
card_original_type('dark favor'/'M13', 'Enchantment — Aura').
card_original_text('dark favor'/'M13', 'Enchant creature\nWhen Dark Favor enters the battlefield, you lose 1 life.\nEnchanted creature gets +3/+1.').
card_image_name('dark favor'/'M13', 'dark favor').
card_uid('dark favor'/'M13', 'M13:Dark Favor:dark favor').
card_rarity('dark favor'/'M13', 'Common').
card_artist('dark favor'/'M13', 'Allen Williams').
card_number('dark favor'/'M13', '86').
card_flavor_text('dark favor'/'M13', 'When he began to curse what he held holy, his strength grew unrivaled.').
card_multiverse_id('dark favor'/'M13', '260975').

card_in_set('deadly recluse', 'M13').
card_original_type('deadly recluse'/'M13', 'Creature — Spider').
card_original_text('deadly recluse'/'M13', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('deadly recluse'/'M13', 'deadly recluse').
card_uid('deadly recluse'/'M13', 'M13:Deadly Recluse:deadly recluse').
card_rarity('deadly recluse'/'M13', 'Common').
card_artist('deadly recluse'/'M13', 'Warren Mahy').
card_number('deadly recluse'/'M13', '165').
card_flavor_text('deadly recluse'/'M13', 'Even dragons fear its silken strands.').
card_multiverse_id('deadly recluse'/'M13', '265717').

card_in_set('diabolic revelation', 'M13').
card_original_type('diabolic revelation'/'M13', 'Sorcery').
card_original_text('diabolic revelation'/'M13', 'Search your library for up to X cards and put those cards into your hand. Then shuffle your library.').
card_first_print('diabolic revelation', 'M13').
card_image_name('diabolic revelation'/'M13', 'diabolic revelation').
card_uid('diabolic revelation'/'M13', 'M13:Diabolic Revelation:diabolic revelation').
card_rarity('diabolic revelation'/'M13', 'Rare').
card_artist('diabolic revelation'/'M13', 'Raymond Swanland').
card_number('diabolic revelation'/'M13', '87').
card_flavor_text('diabolic revelation'/'M13', '\"There are some secrets so dark no human mind should ever contain them. Interested?\"\n—Nefarox, Overlord of Grixis').
card_multiverse_id('diabolic revelation'/'M13', '278196').

card_in_set('disciple of bolas', 'M13').
card_original_type('disciple of bolas'/'M13', 'Creature — Human Wizard').
card_original_text('disciple of bolas'/'M13', 'When Disciple of Bolas enters the battlefield, sacrifice another creature. You gain X life and draw X cards, where X is that creature\'s power.').
card_first_print('disciple of bolas', 'M13').
card_image_name('disciple of bolas'/'M13', 'disciple of bolas').
card_uid('disciple of bolas'/'M13', 'M13:Disciple of Bolas:disciple of bolas').
card_rarity('disciple of bolas'/'M13', 'Rare').
card_artist('disciple of bolas'/'M13', 'Slawomir Maniak').
card_number('disciple of bolas'/'M13', '88').
card_flavor_text('disciple of bolas'/'M13', '\"Lord Bolas has shown me how each miserable little life holds untold resources.\"').
card_multiverse_id('disciple of bolas'/'M13', '253694').

card_in_set('disentomb', 'M13').
card_original_type('disentomb'/'M13', 'Sorcery').
card_original_text('disentomb'/'M13', 'Return target creature card from your graveyard to your hand.').
card_image_name('disentomb'/'M13', 'disentomb').
card_uid('disentomb'/'M13', 'M13:Disentomb:disentomb').
card_rarity('disentomb'/'M13', 'Common').
card_artist('disentomb'/'M13', 'Alex Horley-Orlandelli').
card_number('disentomb'/'M13', '89').
card_flavor_text('disentomb'/'M13', '\"I deal so often in rotting corpses that when I come across the freshly dead . . . why, it\'s like my birthday come early.\"\n—Nevinyrral, necromancer').
card_multiverse_id('disentomb'/'M13', '259701').

card_in_set('divination', 'M13').
card_original_type('divination'/'M13', 'Sorcery').
card_original_text('divination'/'M13', 'Draw two cards.').
card_image_name('divination'/'M13', 'divination').
card_uid('divination'/'M13', 'M13:Divination:divination').
card_rarity('divination'/'M13', 'Common').
card_artist('divination'/'M13', 'Howard Lyon').
card_number('divination'/'M13', '47').
card_flavor_text('divination'/'M13', '\"The key to unlocking this puzzle is within you.\"\n—Doriel, mentor of Mistral Isle').
card_multiverse_id('divination'/'M13', '254118').

card_in_set('divine favor', 'M13').
card_original_type('divine favor'/'M13', 'Enchantment — Aura').
card_original_text('divine favor'/'M13', 'Enchant creature\nWhen Divine Favor enters the battlefield, you gain 3 life.\nEnchanted creature gets +1/+3.').
card_image_name('divine favor'/'M13', 'divine favor').
card_uid('divine favor'/'M13', 'M13:Divine Favor:divine favor').
card_rarity('divine favor'/'M13', 'Common').
card_artist('divine favor'/'M13', 'Allen Williams').
card_number('divine favor'/'M13', '11').
card_flavor_text('divine favor'/'M13', 'With an armory of light, even the squire may champion her people.').
card_multiverse_id('divine favor'/'M13', '259709').

card_in_set('divine verdict', 'M13').
card_original_type('divine verdict'/'M13', 'Instant').
card_original_text('divine verdict'/'M13', 'Destroy target attacking or blocking creature.').
card_image_name('divine verdict'/'M13', 'divine verdict').
card_uid('divine verdict'/'M13', 'M13:Divine Verdict:divine verdict').
card_rarity('divine verdict'/'M13', 'Common').
card_artist('divine verdict'/'M13', 'Kev Walker').
card_number('divine verdict'/'M13', '12').
card_flavor_text('divine verdict'/'M13', '\"Guilty.\"').
card_multiverse_id('divine verdict'/'M13', '271367').

card_in_set('door to nothingness', 'M13').
card_original_type('door to nothingness'/'M13', 'Artifact').
card_original_text('door to nothingness'/'M13', 'Door to Nothingness enters the battlefield tapped.\n{W}{W}{U}{U}{B}{B}{R}{R}{G}{G}, {T}, Sacrifice Door to Nothingness: Target player loses the game.').
card_image_name('door to nothingness'/'M13', 'door to nothingness').
card_uid('door to nothingness'/'M13', 'M13:Door to Nothingness:door to nothingness').
card_rarity('door to nothingness'/'M13', 'Rare').
card_artist('door to nothingness'/'M13', 'Svetlin Velinov').
card_number('door to nothingness'/'M13', '203').
card_flavor_text('door to nothingness'/'M13', 'Only a madman could create such a door. Only an imbecile would open it.').
card_multiverse_id('door to nothingness'/'M13', '288992').

card_in_set('downpour', 'M13').
card_original_type('downpour'/'M13', 'Instant').
card_original_text('downpour'/'M13', 'Tap up to three target creatures.').
card_first_print('downpour', 'M13').
card_image_name('downpour'/'M13', 'downpour').
card_uid('downpour'/'M13', 'M13:Downpour:downpour').
card_rarity('downpour'/'M13', 'Common').
card_artist('downpour'/'M13', 'Eytan Zana').
card_number('downpour'/'M13', '48').
card_flavor_text('downpour'/'M13', '\"The sky holds vast oceans. They are at your fingertips if you know how to call them.\"\n—Talrand, sky summoner').
card_multiverse_id('downpour'/'M13', '276208').

card_in_set('dragon hatchling', 'M13').
card_original_type('dragon hatchling'/'M13', 'Creature — Dragon').
card_original_text('dragon hatchling'/'M13', 'Flying\n{R}: Dragon Hatchling gets +1/+0 until end of turn.').
card_first_print('dragon hatchling', 'M13').
card_image_name('dragon hatchling'/'M13', 'dragon hatchling').
card_uid('dragon hatchling'/'M13', 'M13:Dragon Hatchling:dragon hatchling').
card_rarity('dragon hatchling'/'M13', 'Common').
card_artist('dragon hatchling'/'M13', 'David Palumbo').
card_number('dragon hatchling'/'M13', '128').
card_flavor_text('dragon hatchling'/'M13', '\"Those dragons grow fast. For a while they feed on squirrels and goblins and then suddenly you\'re missing a mammoth.\"\n—Hurdek, Mazar mammoth trainer').
card_multiverse_id('dragon hatchling'/'M13', '259201').

card_in_set('dragonskull summit', 'M13').
card_original_type('dragonskull summit'/'M13', 'Land').
card_original_text('dragonskull summit'/'M13', 'Dragonskull Summit enters the battlefield tapped unless you control a Swamp or a Mountain.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('dragonskull summit'/'M13', 'dragonskull summit').
card_uid('dragonskull summit'/'M13', 'M13:Dragonskull Summit:dragonskull summit').
card_rarity('dragonskull summit'/'M13', 'Rare').
card_artist('dragonskull summit'/'M13', 'Jon Foster').
card_number('dragonskull summit'/'M13', '222').
card_multiverse_id('dragonskull summit'/'M13', '249716').

card_in_set('drowned catacomb', 'M13').
card_original_type('drowned catacomb'/'M13', 'Land').
card_original_text('drowned catacomb'/'M13', 'Drowned Catacomb enters the battlefield tapped unless you control an Island or a Swamp.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('drowned catacomb'/'M13', 'drowned catacomb').
card_uid('drowned catacomb'/'M13', 'M13:Drowned Catacomb:drowned catacomb').
card_rarity('drowned catacomb'/'M13', 'Rare').
card_artist('drowned catacomb'/'M13', 'Dave Kendall').
card_number('drowned catacomb'/'M13', '223').
card_multiverse_id('drowned catacomb'/'M13', '249717').

card_in_set('duress', 'M13').
card_original_type('duress'/'M13', 'Sorcery').
card_original_text('duress'/'M13', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'M13', 'duress').
card_uid('duress'/'M13', 'M13:Duress:duress').
card_rarity('duress'/'M13', 'Common').
card_artist('duress'/'M13', 'Steven Belledin').
card_number('duress'/'M13', '90').
card_flavor_text('duress'/'M13', '\"Don\'t worry. I\'m not going to deprive you of all your secrets. Just your most precious one.\"\n—Liliana Vess').
card_multiverse_id('duress'/'M13', '260979').

card_in_set('duskdale wurm', 'M13').
card_original_type('duskdale wurm'/'M13', 'Creature — Wurm').
card_original_text('duskdale wurm'/'M13', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('duskdale wurm'/'M13', 'duskdale wurm').
card_uid('duskdale wurm'/'M13', 'M13:Duskdale Wurm:duskdale wurm').
card_rarity('duskdale wurm'/'M13', 'Uncommon').
card_artist('duskdale wurm'/'M13', 'Dan Dos Santos').
card_number('duskdale wurm'/'M13', '166').
card_multiverse_id('duskdale wurm'/'M13', '276474').

card_in_set('duskmantle prowler', 'M13').
card_original_type('duskmantle prowler'/'M13', 'Creature — Vampire Rogue').
card_original_text('duskmantle prowler'/'M13', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('duskmantle prowler', 'M13').
card_image_name('duskmantle prowler'/'M13', 'duskmantle prowler').
card_uid('duskmantle prowler'/'M13', 'M13:Duskmantle Prowler:duskmantle prowler').
card_rarity('duskmantle prowler'/'M13', 'Uncommon').
card_artist('duskmantle prowler'/'M13', 'Johannes Voss').
card_number('duskmantle prowler'/'M13', '91').
card_multiverse_id('duskmantle prowler'/'M13', '265728').

card_in_set('duty-bound dead', 'M13').
card_original_type('duty-bound dead'/'M13', 'Creature — Skeleton').
card_original_text('duty-bound dead'/'M13', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{3}{B}: Regenerate Duty-Bound Dead. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('duty-bound dead', 'M13').
card_image_name('duty-bound dead'/'M13', 'duty-bound dead').
card_uid('duty-bound dead'/'M13', 'M13:Duty-Bound Dead:duty-bound dead').
card_rarity('duty-bound dead'/'M13', 'Common').
card_artist('duty-bound dead'/'M13', 'Johannes Voss').
card_number('duty-bound dead'/'M13', '92').
card_multiverse_id('duty-bound dead'/'M13', '260988').

card_in_set('elderscale wurm', 'M13').
card_original_type('elderscale wurm'/'M13', 'Creature — Wurm').
card_original_text('elderscale wurm'/'M13', 'Trample\nWhen Elderscale Wurm enters the battlefield, if your life total is less than 7, your life total becomes 7.\nAs long as you have 7 or more life, damage that would reduce your life total to less than 7 reduces it to 7 instead.').
card_first_print('elderscale wurm', 'M13').
card_image_name('elderscale wurm'/'M13', 'elderscale wurm').
card_uid('elderscale wurm'/'M13', 'M13:Elderscale Wurm:elderscale wurm').
card_rarity('elderscale wurm'/'M13', 'Mythic Rare').
card_artist('elderscale wurm'/'M13', 'Richard Wright').
card_number('elderscale wurm'/'M13', '167').
card_multiverse_id('elderscale wurm'/'M13', '249703').

card_in_set('elixir of immortality', 'M13').
card_original_type('elixir of immortality'/'M13', 'Artifact').
card_original_text('elixir of immortality'/'M13', '{2}, {T}: You gain 5 life. Shuffle Elixir of Immortality and your graveyard into their owner\'s library.').
card_image_name('elixir of immortality'/'M13', 'elixir of immortality').
card_uid('elixir of immortality'/'M13', 'M13:Elixir of Immortality:elixir of immortality').
card_rarity('elixir of immortality'/'M13', 'Uncommon').
card_artist('elixir of immortality'/'M13', 'Zoltan Boros & Gabor Szikszai').
card_number('elixir of immortality'/'M13', '204').
card_flavor_text('elixir of immortality'/'M13', '\"Bottled life. Not as tasty as I\'m used to, rather stale, but it has the same effect.\"\n—Baron Sengir').
card_multiverse_id('elixir of immortality'/'M13', '279717').

card_in_set('elvish archdruid', 'M13').
card_original_type('elvish archdruid'/'M13', 'Creature — Elf Druid').
card_original_text('elvish archdruid'/'M13', 'Other Elf creatures you control get +1/+1.\n{T}: Add {G} to your mana pool for each Elf you control.').
card_image_name('elvish archdruid'/'M13', 'elvish archdruid').
card_uid('elvish archdruid'/'M13', 'M13:Elvish Archdruid:elvish archdruid').
card_rarity('elvish archdruid'/'M13', 'Rare').
card_artist('elvish archdruid'/'M13', 'Karl Kopinski').
card_number('elvish archdruid'/'M13', '168').
card_flavor_text('elvish archdruid'/'M13', 'He knows the name of every elf born in the last four centuries. More importantly, they all know his.').
card_multiverse_id('elvish archdruid'/'M13', '253678').

card_in_set('elvish visionary', 'M13').
card_original_type('elvish visionary'/'M13', 'Creature — Elf Shaman').
card_original_text('elvish visionary'/'M13', 'When Elvish Visionary enters the battlefield, draw a card.').
card_image_name('elvish visionary'/'M13', 'elvish visionary').
card_uid('elvish visionary'/'M13', 'M13:Elvish Visionary:elvish visionary').
card_rarity('elvish visionary'/'M13', 'Common').
card_artist('elvish visionary'/'M13', 'D. Alexander Gregory').
card_number('elvish visionary'/'M13', '169').
card_flavor_text('elvish visionary'/'M13', '\"From a tiny sprout, the greatest trees grow and flourish. May the seeds of your mind be equally fruitful.\"').
card_multiverse_id('elvish visionary'/'M13', '278193').

card_in_set('encrust', 'M13').
card_original_type('encrust'/'M13', 'Enchantment — Aura').
card_original_text('encrust'/'M13', 'Enchant artifact or creature\nEnchanted permanent doesn\'t untap during its controller\'s untap step and its activated abilities can\'t be activated.').
card_first_print('encrust', 'M13').
card_image_name('encrust'/'M13', 'encrust').
card_uid('encrust'/'M13', 'M13:Encrust:encrust').
card_rarity('encrust'/'M13', 'Common').
card_artist('encrust'/'M13', 'Jason Felix').
card_number('encrust'/'M13', '49').
card_flavor_text('encrust'/'M13', '\"Barnacles are the sea\'s final defense.\"\n—Garild, merfolk mage').
card_multiverse_id('encrust'/'M13', '278194').

card_in_set('erase', 'M13').
card_original_type('erase'/'M13', 'Instant').
card_original_text('erase'/'M13', 'Exile target enchantment.').
card_image_name('erase'/'M13', 'erase').
card_uid('erase'/'M13', 'M13:Erase:erase').
card_rarity('erase'/'M13', 'Common').
card_artist('erase'/'M13', 'Richard Wright').
card_number('erase'/'M13', '13').
card_flavor_text('erase'/'M13', '\"Don\'t trust your eyes, for mages are skilled in deception. Trust only in your faith. It will wipe everything clean.\"\n—Lila, Erunian priest').
card_multiverse_id('erase'/'M13', '259723').

card_in_set('essence drain', 'M13').
card_original_type('essence drain'/'M13', 'Sorcery').
card_original_text('essence drain'/'M13', 'Essence Drain deals 3 damage to target creature or player and you gain 3 life.').
card_image_name('essence drain'/'M13', 'essence drain').
card_uid('essence drain'/'M13', 'M13:Essence Drain:essence drain').
card_rarity('essence drain'/'M13', 'Common').
card_artist('essence drain'/'M13', 'Jim Nelson').
card_number('essence drain'/'M13', '93').
card_flavor_text('essence drain'/'M13', '\"Life\'s essence is a nectar unlike any I tasted when I lived as a mortal.\"\n—Zul Ashur, lich lord').
card_multiverse_id('essence drain'/'M13', '276300').

card_in_set('essence scatter', 'M13').
card_original_type('essence scatter'/'M13', 'Instant').
card_original_text('essence scatter'/'M13', 'Counter target creature spell.').
card_image_name('essence scatter'/'M13', 'essence scatter').
card_uid('essence scatter'/'M13', 'M13:Essence Scatter:essence scatter').
card_rarity('essence scatter'/'M13', 'Common').
card_artist('essence scatter'/'M13', 'Jon Foster').
card_number('essence scatter'/'M13', '50').
card_flavor_text('essence scatter'/'M13', '\"What you attempt to pull from the Æther, I can spread onto the wind.\"\n—Jace Beleren').
card_multiverse_id('essence scatter'/'M13', '276209').

card_in_set('evolving wilds', 'M13').
card_original_type('evolving wilds'/'M13', 'Land').
card_original_text('evolving wilds'/'M13', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'M13', 'evolving wilds').
card_uid('evolving wilds'/'M13', 'M13:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'M13', 'Common').
card_artist('evolving wilds'/'M13', 'Steven Belledin').
card_number('evolving wilds'/'M13', '224').
card_flavor_text('evolving wilds'/'M13', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'M13', '269666').

card_in_set('faerie invaders', 'M13').
card_original_type('faerie invaders'/'M13', 'Creature — Faerie Rogue').
card_original_text('faerie invaders'/'M13', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_first_print('faerie invaders', 'M13').
card_image_name('faerie invaders'/'M13', 'faerie invaders').
card_uid('faerie invaders'/'M13', 'M13:Faerie Invaders:faerie invaders').
card_rarity('faerie invaders'/'M13', 'Common').
card_artist('faerie invaders'/'M13', 'Ryan Pancoast').
card_number('faerie invaders'/'M13', '51').
card_flavor_text('faerie invaders'/'M13', 'Small enough to penetrate the narrowest crack in a castle wall and numerous enough to hack apart a griffin.').
card_multiverse_id('faerie invaders'/'M13', '279988').

card_in_set('faith\'s reward', 'M13').
card_original_type('faith\'s reward'/'M13', 'Instant').
card_original_text('faith\'s reward'/'M13', 'Return to the battlefield all permanent cards in your graveyard that were put there from the battlefield this turn.').
card_first_print('faith\'s reward', 'M13').
card_image_name('faith\'s reward'/'M13', 'faith\'s reward').
card_uid('faith\'s reward'/'M13', 'M13:Faith\'s Reward:faith\'s reward').
card_rarity('faith\'s reward'/'M13', 'Rare').
card_artist('faith\'s reward'/'M13', 'Raymond Swanland').
card_number('faith\'s reward'/'M13', '14').
card_flavor_text('faith\'s reward'/'M13', '\"I thought the wind was ushering me to my final destination, but it lifted me to my feet and I fought on.\"\n—Idrus, war priest of Thune').
card_multiverse_id('faith\'s reward'/'M13', '259666').

card_in_set('farseek', 'M13').
card_original_type('farseek'/'M13', 'Sorcery').
card_original_text('farseek'/'M13', 'Search your library for a Plains, Island, Swamp, or Mountain card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('farseek'/'M13', 'farseek').
card_uid('farseek'/'M13', 'M13:Farseek:farseek').
card_rarity('farseek'/'M13', 'Common').
card_artist('farseek'/'M13', 'Martina Pilcerova').
card_number('farseek'/'M13', '170').
card_flavor_text('farseek'/'M13', '\"How truly vast this city must be, that I have traveled so far and seen so much, yet never once found the place where the buildings fail.\"').
card_multiverse_id('farseek'/'M13', '277824').

card_in_set('fervor', 'M13').
card_original_type('fervor'/'M13', 'Enchantment').
card_original_text('fervor'/'M13', 'Creatures you control have haste. (They can attack and {T} as soon as they come under your control.)').
card_image_name('fervor'/'M13', 'fervor').
card_uid('fervor'/'M13', 'M13:Fervor:fervor').
card_rarity('fervor'/'M13', 'Rare').
card_artist('fervor'/'M13', 'Wayne England').
card_number('fervor'/'M13', '129').
card_flavor_text('fervor'/'M13', '\"We won\'t sit like lazy gob-slugs waiting for death to come to us. We\'ll bring death, shiny sharp, to our enemies.\"\n—Krenko, mob boss').
card_multiverse_id('fervor'/'M13', '279709').

card_in_set('fire elemental', 'M13').
card_original_type('fire elemental'/'M13', 'Creature — Elemental').
card_original_text('fire elemental'/'M13', '').
card_image_name('fire elemental'/'M13', 'fire elemental').
card_uid('fire elemental'/'M13', 'M13:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'M13', 'Common').
card_artist('fire elemental'/'M13', 'Slawomir Maniak').
card_number('fire elemental'/'M13', '130').
card_flavor_text('fire elemental'/'M13', '\"Fire set free is a fearsome force—destructive, merciless, always hungry. Given direction, it is much worse.\"\n—Jeflin, Vastwood shaman').
card_multiverse_id('fire elemental'/'M13', '259212').

card_in_set('firewing phoenix', 'M13').
card_original_type('firewing phoenix'/'M13', 'Creature — Phoenix').
card_original_text('firewing phoenix'/'M13', 'Flying\n{1}{R}{R}{R}: Return Firewing Phoenix from your graveyard to your hand.').
card_first_print('firewing phoenix', 'M13').
card_image_name('firewing phoenix'/'M13', 'firewing phoenix').
card_uid('firewing phoenix'/'M13', 'M13:Firewing Phoenix:firewing phoenix').
card_rarity('firewing phoenix'/'M13', 'Rare').
card_artist('firewing phoenix'/'M13', 'James Paick').
card_number('firewing phoenix'/'M13', '131').
card_flavor_text('firewing phoenix'/'M13', '\"When a phoenix nests in a volcano, we rejoice, for it means a hundred years of war.\"\n—Golmak, Keldon warlord').
card_multiverse_id('firewing phoenix'/'M13', '259202').

card_in_set('flames of the firebrand', 'M13').
card_original_type('flames of the firebrand'/'M13', 'Sorcery').
card_original_text('flames of the firebrand'/'M13', 'Flames of the Firebrand deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_first_print('flames of the firebrand', 'M13').
card_image_name('flames of the firebrand'/'M13', 'flames of the firebrand').
card_uid('flames of the firebrand'/'M13', 'M13:Flames of the Firebrand:flames of the firebrand').
card_rarity('flames of the firebrand'/'M13', 'Uncommon').
card_artist('flames of the firebrand'/'M13', 'Steve Argyle').
card_number('flames of the firebrand'/'M13', '132').
card_flavor_text('flames of the firebrand'/'M13', '\"You\'re in luck. I brought enough to share.\"\n—Chandra Nalaar').
card_multiverse_id('flames of the firebrand'/'M13', '259219').

card_in_set('flinthoof boar', 'M13').
card_original_type('flinthoof boar'/'M13', 'Creature — Boar').
card_original_text('flinthoof boar'/'M13', 'Flinthoof Boar gets +1/+1 as long as you control a Mountain.\n{R}: Flinthoof Boar gains haste until end of turn. (It can attack and {T} this turn.)').
card_first_print('flinthoof boar', 'M13').
card_image_name('flinthoof boar'/'M13', 'flinthoof boar').
card_uid('flinthoof boar'/'M13', 'M13:Flinthoof Boar:flinthoof boar').
card_rarity('flinthoof boar'/'M13', 'Uncommon').
card_artist('flinthoof boar'/'M13', 'Erica Yang').
card_number('flinthoof boar'/'M13', '171').
card_flavor_text('flinthoof boar'/'M13', 'Through the smoke and flames it hunts, sniffing out the panicked and confused.').
card_multiverse_id('flinthoof boar'/'M13', '249712').

card_in_set('fog', 'M13').
card_original_type('fog'/'M13', 'Instant').
card_original_text('fog'/'M13', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'M13', 'fog').
card_uid('fog'/'M13', 'M13:Fog:fog').
card_rarity('fog'/'M13', 'Common').
card_artist('fog'/'M13', 'Jaime Jones').
card_number('fog'/'M13', '172').
card_flavor_text('fog'/'M13', '\"I fear no army or beast, but only the morning fog. Our assault can survive everything else.\"\n—Lord Hilneth').
card_multiverse_id('fog'/'M13', '253673').

card_in_set('fog bank', 'M13').
card_original_type('fog bank'/'M13', 'Creature — Wall').
card_original_text('fog bank'/'M13', 'Defender (This creature can\'t attack.)\nFlying\nPrevent all combat damage that would be dealt to and dealt by Fog Bank.').
card_image_name('fog bank'/'M13', 'fog bank').
card_uid('fog bank'/'M13', 'M13:Fog Bank:fog bank').
card_rarity('fog bank'/'M13', 'Uncommon').
card_artist('fog bank'/'M13', 'Howard Lyon').
card_number('fog bank'/'M13', '52').
card_flavor_text('fog bank'/'M13', '\"The union of sea and sky makes a perfect cover.\"\n—Talrand, sky summoner').
card_multiverse_id('fog bank'/'M13', '270314').

card_in_set('forest', 'M13').
card_original_type('forest'/'M13', 'Basic Land — Forest').
card_original_text('forest'/'M13', 'G').
card_image_name('forest'/'M13', 'forest1').
card_uid('forest'/'M13', 'M13:Forest:forest1').
card_rarity('forest'/'M13', 'Basic Land').
card_artist('forest'/'M13', 'Volkan Baga').
card_number('forest'/'M13', '246').
card_multiverse_id('forest'/'M13', '249718').

card_in_set('forest', 'M13').
card_original_type('forest'/'M13', 'Basic Land — Forest').
card_original_text('forest'/'M13', 'G').
card_image_name('forest'/'M13', 'forest2').
card_uid('forest'/'M13', 'M13:Forest:forest2').
card_rarity('forest'/'M13', 'Basic Land').
card_artist('forest'/'M13', 'Steven Belledin').
card_number('forest'/'M13', '247').
card_multiverse_id('forest'/'M13', '249719').

card_in_set('forest', 'M13').
card_original_type('forest'/'M13', 'Basic Land — Forest').
card_original_text('forest'/'M13', 'G').
card_image_name('forest'/'M13', 'forest3').
card_uid('forest'/'M13', 'M13:Forest:forest3').
card_rarity('forest'/'M13', 'Basic Land').
card_artist('forest'/'M13', 'Noah Bradley').
card_number('forest'/'M13', '248').
card_multiverse_id('forest'/'M13', '249720').

card_in_set('forest', 'M13').
card_original_type('forest'/'M13', 'Basic Land — Forest').
card_original_text('forest'/'M13', 'G').
card_image_name('forest'/'M13', 'forest4').
card_uid('forest'/'M13', 'M13:Forest:forest4').
card_rarity('forest'/'M13', 'Basic Land').
card_artist('forest'/'M13', 'Jim Nelson').
card_number('forest'/'M13', '249').
card_multiverse_id('forest'/'M13', '249721').

card_in_set('fungal sprouting', 'M13').
card_original_type('fungal sprouting'/'M13', 'Sorcery').
card_original_text('fungal sprouting'/'M13', 'Put X 1/1 green Saproling creature tokens onto the battlefield, where X is the greatest power among creatures you control.').
card_first_print('fungal sprouting', 'M13').
card_image_name('fungal sprouting'/'M13', 'fungal sprouting').
card_uid('fungal sprouting'/'M13', 'M13:Fungal Sprouting:fungal sprouting').
card_rarity('fungal sprouting'/'M13', 'Uncommon').
card_artist('fungal sprouting'/'M13', 'Brad Rigney').
card_number('fungal sprouting'/'M13', '173').
card_flavor_text('fungal sprouting'/'M13', '\"Saprolings need only a host to feed upon. Numbers will take care of the rest.\"\n—Ghave, Guru of Spores').
card_multiverse_id('fungal sprouting'/'M13', '249711').

card_in_set('furnace whelp', 'M13').
card_original_type('furnace whelp'/'M13', 'Creature — Dragon').
card_original_text('furnace whelp'/'M13', 'Flying\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_image_name('furnace whelp'/'M13', 'furnace whelp').
card_uid('furnace whelp'/'M13', 'M13:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'M13', 'Uncommon').
card_artist('furnace whelp'/'M13', 'Matt Cavotta').
card_number('furnace whelp'/'M13', '133').
card_flavor_text('furnace whelp'/'M13', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'M13', '288936').

card_in_set('garruk\'s packleader', 'M13').
card_original_type('garruk\'s packleader'/'M13', 'Creature — Beast').
card_original_text('garruk\'s packleader'/'M13', 'Whenever another creature with power 3 or greater enters the battlefield under your control, you may draw a card.').
card_image_name('garruk\'s packleader'/'M13', 'garruk\'s packleader').
card_uid('garruk\'s packleader'/'M13', 'M13:Garruk\'s Packleader:garruk\'s packleader').
card_rarity('garruk\'s packleader'/'M13', 'Uncommon').
card_artist('garruk\'s packleader'/'M13', 'Nils Hamm').
card_number('garruk\'s packleader'/'M13', '175').
card_flavor_text('garruk\'s packleader'/'M13', '\"He has learned much in his long years. And unlike selfish humans, he\'s willing to share.\"\n—Garruk Wildspeaker').
card_multiverse_id('garruk\'s packleader'/'M13', '253687').

card_in_set('garruk, primal hunter', 'M13').
card_original_type('garruk, primal hunter'/'M13', 'Planeswalker — Garruk').
card_original_text('garruk, primal hunter'/'M13', '+1: Put a 3/3 green Beast creature token onto the battlefield.\n-3: Draw cards equal to the greatest power among creatures you control.\n-6: Put a 6/6 green Wurm creature token onto the battlefield for each land you control.').
card_image_name('garruk, primal hunter'/'M13', 'garruk, primal hunter').
card_uid('garruk, primal hunter'/'M13', 'M13:Garruk, Primal Hunter:garruk, primal hunter').
card_rarity('garruk, primal hunter'/'M13', 'Mythic Rare').
card_artist('garruk, primal hunter'/'M13', 'D. Alexander Gregory').
card_number('garruk, primal hunter'/'M13', '174').
card_multiverse_id('garruk, primal hunter'/'M13', '253669').

card_in_set('gem of becoming', 'M13').
card_original_type('gem of becoming'/'M13', 'Artifact').
card_original_text('gem of becoming'/'M13', '{3}, {T}, Sacrifice Gem of Becoming: Search your library for an Island card, a Swamp card, and a Mountain card. Reveal those cards and put them into your hand. Then shuffle your library.').
card_first_print('gem of becoming', 'M13').
card_image_name('gem of becoming'/'M13', 'gem of becoming').
card_uid('gem of becoming'/'M13', 'M13:Gem of Becoming:gem of becoming').
card_rarity('gem of becoming'/'M13', 'Uncommon').
card_artist('gem of becoming'/'M13', 'Jack Wang').
card_number('gem of becoming'/'M13', '205').
card_multiverse_id('gem of becoming'/'M13', '275727').

card_in_set('giant scorpion', 'M13').
card_original_type('giant scorpion'/'M13', 'Creature — Scorpion').
card_original_text('giant scorpion'/'M13', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('giant scorpion'/'M13', 'giant scorpion').
card_uid('giant scorpion'/'M13', 'M13:Giant Scorpion:giant scorpion').
card_rarity('giant scorpion'/'M13', 'Common').
card_artist('giant scorpion'/'M13', 'Raymond Swanland').
card_number('giant scorpion'/'M13', '94').
card_flavor_text('giant scorpion'/'M13', 'Its sting hurts, but death is strangely painless.').
card_multiverse_id('giant scorpion'/'M13', '260986').

card_in_set('gilded lotus', 'M13').
card_original_type('gilded lotus'/'M13', 'Artifact').
card_original_text('gilded lotus'/'M13', '{T}: Add three mana of any one color to your mana pool.').
card_image_name('gilded lotus'/'M13', 'gilded lotus').
card_uid('gilded lotus'/'M13', 'M13:Gilded Lotus:gilded lotus').
card_rarity('gilded lotus'/'M13', 'Rare').
card_artist('gilded lotus'/'M13', 'Martina Pilcerova').
card_number('gilded lotus'/'M13', '206').
card_flavor_text('gilded lotus'/'M13', 'Over such beauty, wars are fought. With such power, wars are won.').
card_multiverse_id('gilded lotus'/'M13', '249742').

card_in_set('glacial fortress', 'M13').
card_original_type('glacial fortress'/'M13', 'Land').
card_original_text('glacial fortress'/'M13', 'Glacial Fortress enters the battlefield tapped unless you control a Plains or an Island.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('glacial fortress'/'M13', 'glacial fortress').
card_uid('glacial fortress'/'M13', 'M13:Glacial Fortress:glacial fortress').
card_rarity('glacial fortress'/'M13', 'Rare').
card_artist('glacial fortress'/'M13', 'Franz Vohwinkel').
card_number('glacial fortress'/'M13', '225').
card_multiverse_id('glacial fortress'/'M13', '249722').

card_in_set('glorious charge', 'M13').
card_original_type('glorious charge'/'M13', 'Instant').
card_original_text('glorious charge'/'M13', 'Creatures you control get +1/+1 until end of turn.').
card_image_name('glorious charge'/'M13', 'glorious charge').
card_uid('glorious charge'/'M13', 'M13:Glorious Charge:glorious charge').
card_rarity('glorious charge'/'M13', 'Common').
card_artist('glorious charge'/'M13', 'Izzy').
card_number('glorious charge'/'M13', '15').
card_flavor_text('glorious charge'/'M13', '\"One\'s blade is only as sharp as one\'s conviction.\"\n—Ajani Goldmane').
card_multiverse_id('glorious charge'/'M13', '265720').

card_in_set('goblin arsonist', 'M13').
card_original_type('goblin arsonist'/'M13', 'Creature — Goblin Shaman').
card_original_text('goblin arsonist'/'M13', 'When Goblin Arsonist dies, you may have it deal 1 damage to target creature or player.').
card_image_name('goblin arsonist'/'M13', 'goblin arsonist').
card_uid('goblin arsonist'/'M13', 'M13:Goblin Arsonist:goblin arsonist').
card_rarity('goblin arsonist'/'M13', 'Common').
card_artist('goblin arsonist'/'M13', 'Wayne Reynolds').
card_number('goblin arsonist'/'M13', '134').
card_flavor_text('goblin arsonist'/'M13', 'With great power comes great risk of getting yourself killed.').
card_multiverse_id('goblin arsonist'/'M13', '266050').

card_in_set('goblin battle jester', 'M13').
card_original_type('goblin battle jester'/'M13', 'Creature — Goblin').
card_original_text('goblin battle jester'/'M13', 'Whenever you cast a red spell, target creature can\'t block this turn.').
card_first_print('goblin battle jester', 'M13').
card_image_name('goblin battle jester'/'M13', 'goblin battle jester').
card_uid('goblin battle jester'/'M13', 'M13:Goblin Battle Jester:goblin battle jester').
card_rarity('goblin battle jester'/'M13', 'Common').
card_artist('goblin battle jester'/'M13', 'Steve Prescott').
card_number('goblin battle jester'/'M13', '135').
card_flavor_text('goblin battle jester'/'M13', 'Somehow goblins found a tactical advantage by sending a clown to war.').
card_multiverse_id('goblin battle jester'/'M13', '278070').

card_in_set('griffin protector', 'M13').
card_original_type('griffin protector'/'M13', 'Creature — Griffin').
card_original_text('griffin protector'/'M13', 'Flying\nWhenever another creature enters the battlefield under your control, Griffin Protector gets +1/+1 until end of turn.').
card_first_print('griffin protector', 'M13').
card_image_name('griffin protector'/'M13', 'griffin protector').
card_uid('griffin protector'/'M13', 'M13:Griffin Protector:griffin protector').
card_rarity('griffin protector'/'M13', 'Common').
card_artist('griffin protector'/'M13', 'Christopher Moeller').
card_number('griffin protector'/'M13', '16').
card_flavor_text('griffin protector'/'M13', 'The drums of war stir the hearts of all who fight for righteousness.').
card_multiverse_id('griffin protector'/'M13', '259673').

card_in_set('ground seal', 'M13').
card_original_type('ground seal'/'M13', 'Enchantment').
card_original_text('ground seal'/'M13', 'When Ground Seal enters the battlefield, draw a card.\nCards in graveyards can\'t be the targets of spells or abilities.').
card_image_name('ground seal'/'M13', 'ground seal').
card_uid('ground seal'/'M13', 'M13:Ground Seal:ground seal').
card_rarity('ground seal'/'M13', 'Rare').
card_artist('ground seal'/'M13', 'Charles Urbach').
card_number('ground seal'/'M13', '176').
card_flavor_text('ground seal'/'M13', '\"Let all we have buried be buried forever.\"\n—Seton, centaur druid').
card_multiverse_id('ground seal'/'M13', '289156').

card_in_set('guardian lions', 'M13').
card_original_type('guardian lions'/'M13', 'Creature — Cat').
card_original_text('guardian lions'/'M13', 'Vigilance (Attacking doesn\'t cause this creature to tap.)').
card_first_print('guardian lions', 'M13').
card_image_name('guardian lions'/'M13', 'guardian lions').
card_uid('guardian lions'/'M13', 'M13:Guardian Lions:guardian lions').
card_rarity('guardian lions'/'M13', 'Common').
card_artist('guardian lions'/'M13', 'Johannes Voss').
card_number('guardian lions'/'M13', '17').
card_flavor_text('guardian lions'/'M13', 'Approach in peace or not at all.').
card_multiverse_id('guardian lions'/'M13', '279701').

card_in_set('guardians of akrasa', 'M13').
card_original_type('guardians of akrasa'/'M13', 'Creature — Human Soldier').
card_original_text('guardians of akrasa'/'M13', 'Defender (This creature can\'t attack.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_image_name('guardians of akrasa'/'M13', 'guardians of akrasa').
card_uid('guardians of akrasa'/'M13', 'M13:Guardians of Akrasa:guardians of akrasa').
card_rarity('guardians of akrasa'/'M13', 'Common').
card_artist('guardians of akrasa'/'M13', 'Alan Pollack').
card_number('guardians of akrasa'/'M13', '18').
card_flavor_text('guardians of akrasa'/'M13', '\"Only by the bravery of those who put loyalty above glory is our home kept safe.\"\n—Elspeth Tirel').
card_multiverse_id('guardians of akrasa'/'M13', '279861').

card_in_set('hamletback goliath', 'M13').
card_original_type('hamletback goliath'/'M13', 'Creature — Giant Warrior').
card_original_text('hamletback goliath'/'M13', 'Whenever another creature enters the battlefield, you may put X +1/+1 counters on Hamletback Goliath, where X is that creature\'s power.').
card_image_name('hamletback goliath'/'M13', 'hamletback goliath').
card_uid('hamletback goliath'/'M13', 'M13:Hamletback Goliath:hamletback goliath').
card_rarity('hamletback goliath'/'M13', 'Rare').
card_artist('hamletback goliath'/'M13', 'Paolo Parente & Brian Snõddy').
card_number('hamletback goliath'/'M13', '136').
card_flavor_text('hamletback goliath'/'M13', '\"If you live on a giant\'s back, there\'s only one individual you\'ll ever need to fear.\"\n—Gaddock Teeg').
card_multiverse_id('hamletback goliath'/'M13', '279708').

card_in_set('harbor bandit', 'M13').
card_original_type('harbor bandit'/'M13', 'Creature — Human Rogue').
card_original_text('harbor bandit'/'M13', 'Harbor Bandit gets +1/+1 as long as you control an Island.\n{1}{U}: Harbor Bandit is unblockable this turn.').
card_first_print('harbor bandit', 'M13').
card_image_name('harbor bandit'/'M13', 'harbor bandit').
card_uid('harbor bandit'/'M13', 'M13:Harbor Bandit:harbor bandit').
card_rarity('harbor bandit'/'M13', 'Uncommon').
card_artist('harbor bandit'/'M13', 'Jesper Ejsing').
card_number('harbor bandit'/'M13', '95').
card_flavor_text('harbor bandit'/'M13', 'He always drops a little trinket into the bay as thanks to the merfolk who taught him all his best tricks.').
card_multiverse_id('harbor bandit'/'M13', '259660').

card_in_set('harbor serpent', 'M13').
card_original_type('harbor serpent'/'M13', 'Creature — Serpent').
card_original_text('harbor serpent'/'M13', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)\nHarbor Serpent can\'t attack unless there are five or more Islands on the battlefield.').
card_image_name('harbor serpent'/'M13', 'harbor serpent').
card_uid('harbor serpent'/'M13', 'M13:Harbor Serpent:harbor serpent').
card_rarity('harbor serpent'/'M13', 'Common').
card_artist('harbor serpent'/'M13', 'Daarken').
card_number('harbor serpent'/'M13', '53').
card_flavor_text('harbor serpent'/'M13', 'Like most giant monsters villagers worship as gods, it proves a fickle master.').
card_multiverse_id('harbor serpent'/'M13', '254113').

card_in_set('healer of the pride', 'M13').
card_original_type('healer of the pride'/'M13', 'Creature — Cat Cleric').
card_original_text('healer of the pride'/'M13', 'Whenever another creature enters the battlefield under your control, you gain 2 life.').
card_first_print('healer of the pride', 'M13').
card_image_name('healer of the pride'/'M13', 'healer of the pride').
card_uid('healer of the pride'/'M13', 'M13:Healer of the Pride:healer of the pride').
card_rarity('healer of the pride'/'M13', 'Uncommon').
card_artist('healer of the pride'/'M13', 'Christopher Moeller').
card_number('healer of the pride'/'M13', '19').
card_flavor_text('healer of the pride'/'M13', '\"No life is without meaning. No living thing is too small to be strong.\"').
card_multiverse_id('healer of the pride'/'M13', '276206').

card_in_set('hellion crucible', 'M13').
card_original_type('hellion crucible'/'M13', 'Land').
card_original_text('hellion crucible'/'M13', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Put a pressure counter on Hellion Crucible.\n{1}{R}, {T}, Remove two pressure counters from Hellion Crucible and sacrifice it: Put a 4/4 red Hellion creature token with haste onto the battlefield. (It can attack and {T} as soon as it comes under your control.)').
card_first_print('hellion crucible', 'M13').
card_image_name('hellion crucible'/'M13', 'hellion crucible').
card_uid('hellion crucible'/'M13', 'M13:Hellion Crucible:hellion crucible').
card_rarity('hellion crucible'/'M13', 'Rare').
card_artist('hellion crucible'/'M13', 'Trevor Claxton').
card_number('hellion crucible'/'M13', '226').
card_multiverse_id('hellion crucible'/'M13', '253729').

card_in_set('hydrosurge', 'M13').
card_original_type('hydrosurge'/'M13', 'Instant').
card_original_text('hydrosurge'/'M13', 'Target creature gets -5/-0 until end of turn.').
card_first_print('hydrosurge', 'M13').
card_image_name('hydrosurge'/'M13', 'hydrosurge').
card_uid('hydrosurge'/'M13', 'M13:Hydrosurge:hydrosurge').
card_rarity('hydrosurge'/'M13', 'Common').
card_artist('hydrosurge'/'M13', 'Steve Prescott').
card_number('hydrosurge'/'M13', '54').
card_flavor_text('hydrosurge'/'M13', '\"Thirsty?\"\n—Drunvalus, hydromancer').
card_multiverse_id('hydrosurge'/'M13', '270378').

card_in_set('index', 'M13').
card_original_type('index'/'M13', 'Sorcery').
card_original_text('index'/'M13', 'Look at the top five cards of your library, then put them back in any order.').
card_image_name('index'/'M13', 'index').
card_uid('index'/'M13', 'M13:Index:index').
card_rarity('index'/'M13', 'Common').
card_artist('index'/'M13', 'Kev Walker').
card_number('index'/'M13', '55').
card_flavor_text('index'/'M13', '\"What good is having all the right answers if you have them at all the wrong times?\"').
card_multiverse_id('index'/'M13', '289098').

card_in_set('intrepid hero', 'M13').
card_original_type('intrepid hero'/'M13', 'Creature — Human Soldier').
card_original_text('intrepid hero'/'M13', '{T}: Destroy target creature with power 4 or greater.').
card_image_name('intrepid hero'/'M13', 'intrepid hero').
card_uid('intrepid hero'/'M13', 'M13:Intrepid Hero:intrepid hero').
card_rarity('intrepid hero'/'M13', 'Rare').
card_artist('intrepid hero'/'M13', 'Greg Hildebrandt').
card_number('intrepid hero'/'M13', '20').
card_flavor_text('intrepid hero'/'M13', 'A fool knows no fear. A hero shows no fear.').
card_multiverse_id('intrepid hero'/'M13', '280320').

card_in_set('island', 'M13').
card_original_type('island'/'M13', 'Basic Land — Island').
card_original_text('island'/'M13', 'U').
card_image_name('island'/'M13', 'island1').
card_uid('island'/'M13', 'M13:Island:island1').
card_rarity('island'/'M13', 'Basic Land').
card_artist('island'/'M13', 'Rob Alexander').
card_number('island'/'M13', '234').
card_multiverse_id('island'/'M13', '249726').

card_in_set('island', 'M13').
card_original_type('island'/'M13', 'Basic Land — Island').
card_original_text('island'/'M13', 'U').
card_image_name('island'/'M13', 'island2').
card_uid('island'/'M13', 'M13:Island:island2').
card_rarity('island'/'M13', 'Basic Land').
card_artist('island'/'M13', 'Noah Bradley').
card_number('island'/'M13', '235').
card_multiverse_id('island'/'M13', '249725').

card_in_set('island', 'M13').
card_original_type('island'/'M13', 'Basic Land — Island').
card_original_text('island'/'M13', 'U').
card_image_name('island'/'M13', 'island3').
card_uid('island'/'M13', 'M13:Island:island3').
card_rarity('island'/'M13', 'Basic Land').
card_artist('island'/'M13', 'Cliff Childs').
card_number('island'/'M13', '236').
card_multiverse_id('island'/'M13', '249723').

card_in_set('island', 'M13').
card_original_type('island'/'M13', 'Basic Land — Island').
card_original_text('island'/'M13', 'U').
card_image_name('island'/'M13', 'island4').
card_uid('island'/'M13', 'M13:Island:island4').
card_rarity('island'/'M13', 'Basic Land').
card_artist('island'/'M13', 'Peter Mohrbacher').
card_number('island'/'M13', '237').
card_multiverse_id('island'/'M13', '249724').

card_in_set('jace\'s phantasm', 'M13').
card_original_type('jace\'s phantasm'/'M13', 'Creature — Illusion').
card_original_text('jace\'s phantasm'/'M13', 'Flying\nJace\'s Phantasm gets +4/+4 as long as an opponent has ten or more cards in his or her graveyard.').
card_first_print('jace\'s phantasm', 'M13').
card_image_name('jace\'s phantasm'/'M13', 'jace\'s phantasm').
card_uid('jace\'s phantasm'/'M13', 'M13:Jace\'s Phantasm:jace\'s phantasm').
card_rarity('jace\'s phantasm'/'M13', 'Uncommon').
card_artist('jace\'s phantasm'/'M13', 'Johann Bodin').
card_number('jace\'s phantasm'/'M13', '57').
card_flavor_text('jace\'s phantasm'/'M13', 'In the abstract memories of the Iquati, Jace found interesting ideas to improve upon.').
card_multiverse_id('jace\'s phantasm'/'M13', '249672').

card_in_set('jace, memory adept', 'M13').
card_original_type('jace, memory adept'/'M13', 'Planeswalker — Jace').
card_original_text('jace, memory adept'/'M13', '+1: Draw a card. Target player puts the top card of his or her library into his or her graveyard.\n0: Target player puts the top ten cards of his or her library into his or her graveyard.\n-7: Any number of target players each draw twenty cards.').
card_image_name('jace, memory adept'/'M13', 'jace, memory adept').
card_uid('jace, memory adept'/'M13', 'M13:Jace, Memory Adept:jace, memory adept').
card_rarity('jace, memory adept'/'M13', 'Mythic Rare').
card_artist('jace, memory adept'/'M13', 'D. Alexander Gregory').
card_number('jace, memory adept'/'M13', '56').
card_multiverse_id('jace, memory adept'/'M13', '254107').

card_in_set('jayemdae tome', 'M13').
card_original_type('jayemdae tome'/'M13', 'Artifact').
card_original_text('jayemdae tome'/'M13', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'M13', 'jayemdae tome').
card_uid('jayemdae tome'/'M13', 'M13:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'M13', 'Uncommon').
card_artist('jayemdae tome'/'M13', 'Donato Giancola').
card_number('jayemdae tome'/'M13', '207').
card_flavor_text('jayemdae tome'/'M13', 'A true scribe devotes an entire lifetime to the creation of a single volume, a masterpiece as unique as its maker.').
card_multiverse_id('jayemdae tome'/'M13', '275705').

card_in_set('kindled fury', 'M13').
card_original_type('kindled fury'/'M13', 'Instant').
card_original_text('kindled fury'/'M13', 'Target creature gets +1/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_image_name('kindled fury'/'M13', 'kindled fury').
card_uid('kindled fury'/'M13', 'M13:Kindled Fury:kindled fury').
card_rarity('kindled fury'/'M13', 'Common').
card_artist('kindled fury'/'M13', 'Wayne Reynolds').
card_number('kindled fury'/'M13', '137').
card_flavor_text('kindled fury'/'M13', '\"Rage is a dangerous weapon. Your enemies will try to use your anger against you. Use it against them first.\"\n—Ajani Goldmane').
card_multiverse_id('kindled fury'/'M13', '280221').

card_in_set('kitesail', 'M13').
card_original_type('kitesail'/'M13', 'Artifact — Equipment').
card_original_text('kitesail'/'M13', 'Equipped creature gets +1/+0 and has flying.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('kitesail'/'M13', 'kitesail').
card_uid('kitesail'/'M13', 'M13:Kitesail:kitesail').
card_rarity('kitesail'/'M13', 'Uncommon').
card_artist('kitesail'/'M13', 'Cyril Van Der Haegen').
card_number('kitesail'/'M13', '208').
card_flavor_text('kitesail'/'M13', 'Kitesailing is a way of life—and without practice, the end of it.').
card_multiverse_id('kitesail'/'M13', '278075').

card_in_set('knight of glory', 'M13').
card_original_type('knight of glory'/'M13', 'Creature — Human Knight').
card_original_text('knight of glory'/'M13', 'Protection from black (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything black.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('knight of glory', 'M13').
card_image_name('knight of glory'/'M13', 'knight of glory').
card_uid('knight of glory'/'M13', 'M13:Knight of Glory:knight of glory').
card_rarity('knight of glory'/'M13', 'Uncommon').
card_artist('knight of glory'/'M13', 'Peter Mohrbacher').
card_number('knight of glory'/'M13', '21').
card_flavor_text('knight of glory'/'M13', '\"I will uphold the law, and no manner of foe will stop me.\"').
card_multiverse_id('knight of glory'/'M13', '265731').

card_in_set('knight of infamy', 'M13').
card_original_type('knight of infamy'/'M13', 'Creature — Human Knight').
card_original_text('knight of infamy'/'M13', 'Protection from white (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything white.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('knight of infamy', 'M13').
card_image_name('knight of infamy'/'M13', 'knight of infamy').
card_uid('knight of infamy'/'M13', 'M13:Knight of Infamy:knight of infamy').
card_rarity('knight of infamy'/'M13', 'Uncommon').
card_artist('knight of infamy'/'M13', 'Peter Mohrbacher').
card_number('knight of infamy'/'M13', '96').
card_flavor_text('knight of infamy'/'M13', '\"Your laws, like your bones, were made to be broken.\"').
card_multiverse_id('knight of infamy'/'M13', '265735').

card_in_set('kraken hatchling', 'M13').
card_original_type('kraken hatchling'/'M13', 'Creature — Kraken').
card_original_text('kraken hatchling'/'M13', '').
card_image_name('kraken hatchling'/'M13', 'kraken hatchling').
card_uid('kraken hatchling'/'M13', 'M13:Kraken Hatchling:kraken hatchling').
card_rarity('kraken hatchling'/'M13', 'Common').
card_artist('kraken hatchling'/'M13', 'Jason Felix').
card_number('kraken hatchling'/'M13', '58').
card_flavor_text('kraken hatchling'/'M13', 'A spike and a maul are needed to crack their shells, but the taste is worth the effort.').
card_multiverse_id('kraken hatchling'/'M13', '279699').

card_in_set('krenko\'s command', 'M13').
card_original_type('krenko\'s command'/'M13', 'Sorcery').
card_original_text('krenko\'s command'/'M13', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_first_print('krenko\'s command', 'M13').
card_image_name('krenko\'s command'/'M13', 'krenko\'s command').
card_uid('krenko\'s command'/'M13', 'M13:Krenko\'s Command:krenko\'s command').
card_rarity('krenko\'s command'/'M13', 'Common').
card_artist('krenko\'s command'/'M13', 'Karl Kopinski').
card_number('krenko\'s command'/'M13', '139').
card_flavor_text('krenko\'s command'/'M13', 'Goblins are eager to follow orders, especially when those orders involve stealing, hurting, annoying, eating, destroying, or swearing.').
card_multiverse_id('krenko\'s command'/'M13', '259213').

card_in_set('krenko, mob boss', 'M13').
card_original_type('krenko, mob boss'/'M13', 'Legendary Creature — Goblin Warrior').
card_original_text('krenko, mob boss'/'M13', '{T}: Put X 1/1 red Goblin creature tokens onto the battlefield, where X is the number of Goblins you control.').
card_first_print('krenko, mob boss', 'M13').
card_image_name('krenko, mob boss'/'M13', 'krenko, mob boss').
card_uid('krenko, mob boss'/'M13', 'M13:Krenko, Mob Boss:krenko, mob boss').
card_rarity('krenko, mob boss'/'M13', 'Rare').
card_artist('krenko, mob boss'/'M13', 'Karl Kopinski').
card_number('krenko, mob boss'/'M13', '138').
card_flavor_text('krenko, mob boss'/'M13', '\"He displays a perverse charisma fueled by avarice. Highly dangerous. Recommend civil sanctions.\"\n—Agmand Sarv, Azorius hussar').
card_multiverse_id('krenko, mob boss'/'M13', '253712').

card_in_set('liliana of the dark realms', 'M13').
card_original_type('liliana of the dark realms'/'M13', 'Planeswalker — Liliana').
card_original_text('liliana of the dark realms'/'M13', '+1: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.\n-3: Target creature gets +X/+X or -X/-X until end of turn, where X is the number of Swamps you control.\n-6: You get an emblem with \"Swamps you control have ‘{T}: Add {B}{B}{B}{B} to your mana pool.\'\"').
card_image_name('liliana of the dark realms'/'M13', 'liliana of the dark realms').
card_uid('liliana of the dark realms'/'M13', 'M13:Liliana of the Dark Realms:liliana of the dark realms').
card_rarity('liliana of the dark realms'/'M13', 'Mythic Rare').
card_artist('liliana of the dark realms'/'M13', 'D. Alexander Gregory').
card_number('liliana of the dark realms'/'M13', '97').
card_multiverse_id('liliana of the dark realms'/'M13', '259695').

card_in_set('liliana\'s shade', 'M13').
card_original_type('liliana\'s shade'/'M13', 'Creature — Shade').
card_original_text('liliana\'s shade'/'M13', 'When Liliana\'s Shade enters the battlefield, you may search your library for a Swamp card, reveal it, put it into your hand, then shuffle your library.\n{B}: Liliana\'s Shade gets +1/+1 until end of turn.').
card_first_print('liliana\'s shade', 'M13').
card_image_name('liliana\'s shade'/'M13', 'liliana\'s shade').
card_uid('liliana\'s shade'/'M13', 'M13:Liliana\'s Shade:liliana\'s shade').
card_rarity('liliana\'s shade'/'M13', 'Common').
card_artist('liliana\'s shade'/'M13', 'Eric Deschamps').
card_number('liliana\'s shade'/'M13', '98').
card_multiverse_id('liliana\'s shade'/'M13', '260992').

card_in_set('magmaquake', 'M13').
card_original_type('magmaquake'/'M13', 'Instant').
card_original_text('magmaquake'/'M13', 'Magmaquake deals X damage to each creature without flying and each planeswalker.').
card_image_name('magmaquake'/'M13', 'magmaquake').
card_uid('magmaquake'/'M13', 'M13:Magmaquake:magmaquake').
card_rarity('magmaquake'/'M13', 'Rare').
card_artist('magmaquake'/'M13', 'Gabor Szikszai').
card_number('magmaquake'/'M13', '140').
card_flavor_text('magmaquake'/'M13', '\"Where will you run when I punish you with the very ground you flee on?\"\n—Nicol Bolas').
card_multiverse_id('magmaquake'/'M13', '276205').

card_in_set('mark of mutiny', 'M13').
card_original_type('mark of mutiny'/'M13', 'Sorcery').
card_original_text('mark of mutiny'/'M13', 'Gain control of target creature until end of turn. Put a +1/+1 counter on it and untap it. That creature gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('mark of mutiny'/'M13', 'mark of mutiny').
card_uid('mark of mutiny'/'M13', 'M13:Mark of Mutiny:mark of mutiny').
card_rarity('mark of mutiny'/'M13', 'Uncommon').
card_artist('mark of mutiny'/'M13', 'Mike Bierek').
card_number('mark of mutiny'/'M13', '141').
card_flavor_text('mark of mutiny'/'M13', 'The flame of anger is hard to douse once lit.').
card_multiverse_id('mark of mutiny'/'M13', '282537').

card_in_set('mark of the vampire', 'M13').
card_original_type('mark of the vampire'/'M13', 'Enchantment — Aura').
card_original_text('mark of the vampire'/'M13', 'Enchant creature\nEnchanted creature gets +2/+2 and has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_first_print('mark of the vampire', 'M13').
card_image_name('mark of the vampire'/'M13', 'mark of the vampire').
card_uid('mark of the vampire'/'M13', 'M13:Mark of the Vampire:mark of the vampire').
card_rarity('mark of the vampire'/'M13', 'Common').
card_artist('mark of the vampire'/'M13', 'Winona Nelson').
card_number('mark of the vampire'/'M13', '99').
card_flavor_text('mark of the vampire'/'M13', '\"My ‘condition\' is a trial. The weak are consumed by it. The strong transcend it.\"\n—Sorin Markov').
card_multiverse_id('mark of the vampire'/'M13', '253723').

card_in_set('master of the pearl trident', 'M13').
card_original_type('master of the pearl trident'/'M13', 'Creature — Merfolk').
card_original_text('master of the pearl trident'/'M13', 'Other Merfolk creatures you control get +1/+1 and have islandwalk. (They are unblockable as long as defending player controls an Island.)').
card_first_print('master of the pearl trident', 'M13').
card_image_name('master of the pearl trident'/'M13', 'master of the pearl trident').
card_uid('master of the pearl trident'/'M13', 'M13:Master of the Pearl Trident:master of the pearl trident').
card_rarity('master of the pearl trident'/'M13', 'Rare').
card_artist('master of the pearl trident'/'M13', 'Ryan Pancoast').
card_number('master of the pearl trident'/'M13', '59').
card_flavor_text('master of the pearl trident'/'M13', '\"Let the land dwellers know the coast is no longer the border between our realms. A new age of empire has begun.\"').
card_multiverse_id('master of the pearl trident'/'M13', '279901').

card_in_set('merfolk of the pearl trident', 'M13').
card_original_type('merfolk of the pearl trident'/'M13', 'Creature — Merfolk').
card_original_text('merfolk of the pearl trident'/'M13', '').
card_image_name('merfolk of the pearl trident'/'M13', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'M13', 'M13:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'M13', 'Common').
card_artist('merfolk of the pearl trident'/'M13', 'Ray Lago').
card_number('merfolk of the pearl trident'/'M13', '60').
card_flavor_text('merfolk of the pearl trident'/'M13', '\"The merfolk of the Pearl Trident have risen from the waves since the earliest days, at times allies to humans, other times enemies. Now their power is cresting again, and who can say which way the new tide will flow.\"\n—Kalis, Onean scholar').
card_multiverse_id('merfolk of the pearl trident'/'M13', '278211').

card_in_set('mind rot', 'M13').
card_original_type('mind rot'/'M13', 'Sorcery').
card_original_text('mind rot'/'M13', 'Target player discards two cards.').
card_image_name('mind rot'/'M13', 'mind rot').
card_uid('mind rot'/'M13', 'M13:Mind Rot:mind rot').
card_rarity('mind rot'/'M13', 'Common').
card_artist('mind rot'/'M13', 'Steve Luke').
card_number('mind rot'/'M13', '100').
card_flavor_text('mind rot'/'M13', '\"What a pity. You should have written it down.\"\n—Liliana Vess').
card_multiverse_id('mind rot'/'M13', '260980').

card_in_set('mind sculpt', 'M13').
card_original_type('mind sculpt'/'M13', 'Sorcery').
card_original_text('mind sculpt'/'M13', 'Target opponent puts the top seven cards of his or her library into his or her graveyard.').
card_first_print('mind sculpt', 'M13').
card_image_name('mind sculpt'/'M13', 'mind sculpt').
card_uid('mind sculpt'/'M13', 'M13:Mind Sculpt:mind sculpt').
card_rarity('mind sculpt'/'M13', 'Common').
card_artist('mind sculpt'/'M13', 'Michael C. Hayes').
card_number('mind sculpt'/'M13', '61').
card_flavor_text('mind sculpt'/'M13', '\"Your mind was a curious mix of madness and genius. I just took away the genius.\"\n—Jace Beleren').
card_multiverse_id('mind sculpt'/'M13', '266487').

card_in_set('mindclaw shaman', 'M13').
card_original_type('mindclaw shaman'/'M13', 'Creature — Viashino Shaman').
card_original_text('mindclaw shaman'/'M13', 'When Mindclaw Shaman enters the battlefield, target opponent reveals his or her hand. You may cast an instant or sorcery card from it without paying its mana cost.').
card_first_print('mindclaw shaman', 'M13').
card_image_name('mindclaw shaman'/'M13', 'mindclaw shaman').
card_uid('mindclaw shaman'/'M13', 'M13:Mindclaw Shaman:mindclaw shaman').
card_rarity('mindclaw shaman'/'M13', 'Uncommon').
card_artist('mindclaw shaman'/'M13', 'Slawomir Maniak').
card_number('mindclaw shaman'/'M13', '142').
card_flavor_text('mindclaw shaman'/'M13', 'Nicol Bolas gives power to those who will abuse it most.').
card_multiverse_id('mindclaw shaman'/'M13', '249715').

card_in_set('mogg flunkies', 'M13').
card_original_type('mogg flunkies'/'M13', 'Creature — Goblin').
card_original_text('mogg flunkies'/'M13', 'Mogg Flunkies can\'t attack or block alone.').
card_image_name('mogg flunkies'/'M13', 'mogg flunkies').
card_uid('mogg flunkies'/'M13', 'M13:Mogg Flunkies:mogg flunkies').
card_rarity('mogg flunkies'/'M13', 'Common').
card_artist('mogg flunkies'/'M13', 'Brom').
card_number('mogg flunkies'/'M13', '143').
card_flavor_text('mogg flunkies'/'M13', 'They\'ll attack whatever\'s in front of them—as long as you tell them where that is.').
card_multiverse_id('mogg flunkies'/'M13', '249706').

card_in_set('mountain', 'M13').
card_original_type('mountain'/'M13', 'Basic Land — Mountain').
card_original_text('mountain'/'M13', 'R').
card_image_name('mountain'/'M13', 'mountain1').
card_uid('mountain'/'M13', 'M13:Mountain:mountain1').
card_rarity('mountain'/'M13', 'Basic Land').
card_artist('mountain'/'M13', 'Cliff Childs').
card_number('mountain'/'M13', '242').
card_multiverse_id('mountain'/'M13', '249729').

card_in_set('mountain', 'M13').
card_original_type('mountain'/'M13', 'Basic Land — Mountain').
card_original_text('mountain'/'M13', 'R').
card_image_name('mountain'/'M13', 'mountain2').
card_uid('mountain'/'M13', 'M13:Mountain:mountain2').
card_rarity('mountain'/'M13', 'Basic Land').
card_artist('mountain'/'M13', 'Nils Hamm').
card_number('mountain'/'M13', '243').
card_multiverse_id('mountain'/'M13', '249730').

card_in_set('mountain', 'M13').
card_original_type('mountain'/'M13', 'Basic Land — Mountain').
card_original_text('mountain'/'M13', 'R').
card_image_name('mountain'/'M13', 'mountain3').
card_uid('mountain'/'M13', 'M13:Mountain:mountain3').
card_rarity('mountain'/'M13', 'Basic Land').
card_artist('mountain'/'M13', 'Karl Kopinski').
card_number('mountain'/'M13', '244').
card_multiverse_id('mountain'/'M13', '249728').

card_in_set('mountain', 'M13').
card_original_type('mountain'/'M13', 'Basic Land — Mountain').
card_original_text('mountain'/'M13', 'R').
card_image_name('mountain'/'M13', 'mountain4').
card_uid('mountain'/'M13', 'M13:Mountain:mountain4').
card_rarity('mountain'/'M13', 'Basic Land').
card_artist('mountain'/'M13', 'Robh Ruppel').
card_number('mountain'/'M13', '245').
card_multiverse_id('mountain'/'M13', '249727').

card_in_set('murder', 'M13').
card_original_type('murder'/'M13', 'Instant').
card_original_text('murder'/'M13', 'Destroy target creature.').
card_first_print('murder', 'M13').
card_image_name('murder'/'M13', 'murder').
card_uid('murder'/'M13', 'M13:Murder:murder').
card_rarity('murder'/'M13', 'Common').
card_artist('murder'/'M13', 'Allen Williams').
card_number('murder'/'M13', '101').
card_multiverse_id('murder'/'M13', '259677').

card_in_set('mutilate', 'M13').
card_original_type('mutilate'/'M13', 'Sorcery').
card_original_text('mutilate'/'M13', 'All creatures get -1/-1 until end of turn for each Swamp you control.').
card_image_name('mutilate'/'M13', 'mutilate').
card_uid('mutilate'/'M13', 'M13:Mutilate:mutilate').
card_rarity('mutilate'/'M13', 'Rare').
card_artist('mutilate'/'M13', 'Tyler Jacobson').
card_number('mutilate'/'M13', '102').
card_flavor_text('mutilate'/'M13', '\"It\'s only torture if you\'re strong enough to survive. Otherwise, it\'s a simple, gruesome death. I\'m happy with either outcome.\"\n—Liliana Vess').
card_multiverse_id('mutilate'/'M13', '265134').

card_in_set('mwonvuli beast tracker', 'M13').
card_original_type('mwonvuli beast tracker'/'M13', 'Creature — Human Scout').
card_original_text('mwonvuli beast tracker'/'M13', 'When Mwonvuli Beast Tracker enters the battlefield, search your library for a creature card with deathtouch, hexproof, reach, or trample and reveal it. Shuffle your library and put that card on top of it.').
card_image_name('mwonvuli beast tracker'/'M13', 'mwonvuli beast tracker').
card_uid('mwonvuli beast tracker'/'M13', 'M13:Mwonvuli Beast Tracker:mwonvuli beast tracker').
card_rarity('mwonvuli beast tracker'/'M13', 'Uncommon').
card_artist('mwonvuli beast tracker'/'M13', 'Zoltan Boros').
card_number('mwonvuli beast tracker'/'M13', '177').
card_flavor_text('mwonvuli beast tracker'/'M13', '\"The more dangerous, the better.\"').
card_multiverse_id('mwonvuli beast tracker'/'M13', '249699').

card_in_set('naturalize', 'M13').
card_original_type('naturalize'/'M13', 'Instant').
card_original_text('naturalize'/'M13', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'M13', 'naturalize').
card_uid('naturalize'/'M13', 'M13:Naturalize:naturalize').
card_rarity('naturalize'/'M13', 'Common').
card_artist('naturalize'/'M13', 'Scott Chou').
card_number('naturalize'/'M13', '178').
card_flavor_text('naturalize'/'M13', '\"When your cities and trinkets crumble, only nature will remain.\"\n—Garruk Wildspeaker').
card_multiverse_id('naturalize'/'M13', '253672').

card_in_set('nefarox, overlord of grixis', 'M13').
card_original_type('nefarox, overlord of grixis'/'M13', 'Legendary Creature — Demon').
card_original_text('nefarox, overlord of grixis'/'M13', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever Nefarox, Overlord of Grixis attacks alone, defending player sacrifices a creature.').
card_first_print('nefarox, overlord of grixis', 'M13').
card_image_name('nefarox, overlord of grixis'/'M13', 'nefarox, overlord of grixis').
card_uid('nefarox, overlord of grixis'/'M13', 'M13:Nefarox, Overlord of Grixis:nefarox, overlord of grixis').
card_rarity('nefarox, overlord of grixis'/'M13', 'Rare').
card_artist('nefarox, overlord of grixis'/'M13', 'Aleksi Briclot').
card_number('nefarox, overlord of grixis'/'M13', '103').
card_multiverse_id('nefarox, overlord of grixis'/'M13', '259679').

card_in_set('negate', 'M13').
card_original_type('negate'/'M13', 'Instant').
card_original_text('negate'/'M13', 'Counter target noncreature spell.').
card_image_name('negate'/'M13', 'negate').
card_uid('negate'/'M13', 'M13:Negate:negate').
card_rarity('negate'/'M13', 'Common').
card_artist('negate'/'M13', 'Jeremy Jarvis').
card_number('negate'/'M13', '62').
card_flavor_text('negate'/'M13', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'M13', '254114').

card_in_set('nicol bolas, planeswalker', 'M13').
card_original_type('nicol bolas, planeswalker'/'M13', 'Planeswalker — Bolas').
card_original_text('nicol bolas, planeswalker'/'M13', '+3: Destroy target noncreature permanent.\n-2: Gain control of target creature.\n-9: Nicol Bolas, Planeswalker deals 7 damage to target player. That player discards seven cards, then sacrifices seven permanents.').
card_image_name('nicol bolas, planeswalker'/'M13', 'nicol bolas, planeswalker').
card_uid('nicol bolas, planeswalker'/'M13', 'M13:Nicol Bolas, Planeswalker:nicol bolas, planeswalker').
card_rarity('nicol bolas, planeswalker'/'M13', 'Mythic Rare').
card_artist('nicol bolas, planeswalker'/'M13', 'D. Alexander Gregory').
card_number('nicol bolas, planeswalker'/'M13', '199').
card_multiverse_id('nicol bolas, planeswalker'/'M13', '260991').

card_in_set('oblivion ring', 'M13').
card_original_type('oblivion ring'/'M13', 'Enchantment').
card_original_text('oblivion ring'/'M13', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'M13', 'oblivion ring').
card_uid('oblivion ring'/'M13', 'M13:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'M13', 'Uncommon').
card_artist('oblivion ring'/'M13', 'Franz Vohwinkel').
card_number('oblivion ring'/'M13', '22').
card_multiverse_id('oblivion ring'/'M13', '259711').

card_in_set('odric, master tactician', 'M13').
card_original_type('odric, master tactician'/'M13', 'Legendary Creature — Human Soldier').
card_original_text('odric, master tactician'/'M13', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhenever Odric, Master Tactician and at least three other creatures attack, you choose which creatures block this combat and how those creatures block.').
card_first_print('odric, master tactician', 'M13').
card_image_name('odric, master tactician'/'M13', 'odric, master tactician').
card_uid('odric, master tactician'/'M13', 'M13:Odric, Master Tactician:odric, master tactician').
card_rarity('odric, master tactician'/'M13', 'Rare').
card_artist('odric, master tactician'/'M13', 'Michael Komarck').
card_number('odric, master tactician'/'M13', '23').
card_flavor_text('odric, master tactician'/'M13', '\"Fear holds no place in faith\'s battle plan.\"').
card_multiverse_id('odric, master tactician'/'M13', '259670').

card_in_set('omniscience', 'M13').
card_original_type('omniscience'/'M13', 'Enchantment').
card_original_text('omniscience'/'M13', 'You may cast nonland cards from your hand without paying their mana costs.').
card_first_print('omniscience', 'M13').
card_image_name('omniscience'/'M13', 'omniscience').
card_uid('omniscience'/'M13', 'M13:Omniscience:omniscience').
card_rarity('omniscience'/'M13', 'Mythic Rare').
card_artist('omniscience'/'M13', 'Jason Chan').
card_number('omniscience'/'M13', '63').
card_flavor_text('omniscience'/'M13', '\"The things I once imagined would be my greatest achievements were only the first steps toward a future I can only begin to fathom.\"\n—Jace Beleren').
card_multiverse_id('omniscience'/'M13', '288937').

card_in_set('pacifism', 'M13').
card_original_type('pacifism'/'M13', 'Enchantment — Aura').
card_original_text('pacifism'/'M13', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'M13', 'pacifism').
card_uid('pacifism'/'M13', 'M13:Pacifism:pacifism').
card_rarity('pacifism'/'M13', 'Common').
card_artist('pacifism'/'M13', 'Robert Bliss').
card_number('pacifism'/'M13', '24').
card_flavor_text('pacifism'/'M13', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'M13', '259712').

card_in_set('phylactery lich', 'M13').
card_original_type('phylactery lich'/'M13', 'Creature — Zombie').
card_original_text('phylactery lich'/'M13', 'As Phylactery Lich enters the battlefield, put a phylactery counter on an artifact you control.\nPhylactery Lich is indestructible.\nWhen you control no permanents with phylactery counters on them, sacrifice Phylactery Lich.').
card_image_name('phylactery lich'/'M13', 'phylactery lich').
card_uid('phylactery lich'/'M13', 'M13:Phylactery Lich:phylactery lich').
card_rarity('phylactery lich'/'M13', 'Rare').
card_artist('phylactery lich'/'M13', 'Michael Komarck').
card_number('phylactery lich'/'M13', '104').
card_multiverse_id('phylactery lich'/'M13', '271386').

card_in_set('phyrexian hulk', 'M13').
card_original_type('phyrexian hulk'/'M13', 'Artifact Creature — Golem').
card_original_text('phyrexian hulk'/'M13', '').
card_image_name('phyrexian hulk'/'M13', 'phyrexian hulk').
card_uid('phyrexian hulk'/'M13', 'M13:Phyrexian Hulk:phyrexian hulk').
card_rarity('phyrexian hulk'/'M13', 'Uncommon').
card_artist('phyrexian hulk'/'M13', 'Steven Belledin').
card_number('phyrexian hulk'/'M13', '209').
card_flavor_text('phyrexian hulk'/'M13', 'An invasion weapon of ages past, the glistening oil contained the blueprints of countless atrocities.').
card_multiverse_id('phyrexian hulk'/'M13', '279990').

card_in_set('pillarfield ox', 'M13').
card_original_type('pillarfield ox'/'M13', 'Creature — Ox').
card_original_text('pillarfield ox'/'M13', '').
card_image_name('pillarfield ox'/'M13', 'pillarfield ox').
card_uid('pillarfield ox'/'M13', 'M13:Pillarfield Ox:pillarfield ox').
card_rarity('pillarfield ox'/'M13', 'Common').
card_artist('pillarfield ox'/'M13', 'Andrew Robinson').
card_number('pillarfield ox'/'M13', '25').
card_flavor_text('pillarfield ox'/'M13', '\"May starving fleas birth a thousand generations on your stubborn hide, cow!\"\n—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('pillarfield ox'/'M13', '279700').

card_in_set('plains', 'M13').
card_original_type('plains'/'M13', 'Basic Land — Plains').
card_original_text('plains'/'M13', 'W').
card_image_name('plains'/'M13', 'plains1').
card_uid('plains'/'M13', 'M13:Plains:plains1').
card_rarity('plains'/'M13', 'Basic Land').
card_artist('plains'/'M13', 'John Avon').
card_number('plains'/'M13', '230').
card_multiverse_id('plains'/'M13', '249733').

card_in_set('plains', 'M13').
card_original_type('plains'/'M13', 'Basic Land — Plains').
card_original_text('plains'/'M13', 'W').
card_image_name('plains'/'M13', 'plains2').
card_uid('plains'/'M13', 'M13:Plains:plains2').
card_rarity('plains'/'M13', 'Basic Land').
card_artist('plains'/'M13', 'Noah Bradley').
card_number('plains'/'M13', '231').
card_multiverse_id('plains'/'M13', '249732').

card_in_set('plains', 'M13').
card_original_type('plains'/'M13', 'Basic Land — Plains').
card_original_text('plains'/'M13', 'W').
card_image_name('plains'/'M13', 'plains3').
card_uid('plains'/'M13', 'M13:Plains:plains3').
card_rarity('plains'/'M13', 'Basic Land').
card_artist('plains'/'M13', 'Nils Hamm').
card_number('plains'/'M13', '232').
card_multiverse_id('plains'/'M13', '249731').

card_in_set('plains', 'M13').
card_original_type('plains'/'M13', 'Basic Land — Plains').
card_original_text('plains'/'M13', 'W').
card_image_name('plains'/'M13', 'plains4').
card_uid('plains'/'M13', 'M13:Plains:plains4').
card_rarity('plains'/'M13', 'Basic Land').
card_artist('plains'/'M13', 'Charles Urbach').
card_number('plains'/'M13', '233').
card_multiverse_id('plains'/'M13', '249734').

card_in_set('planar cleansing', 'M13').
card_original_type('planar cleansing'/'M13', 'Sorcery').
card_original_text('planar cleansing'/'M13', 'Destroy all nonland permanents.').
card_image_name('planar cleansing'/'M13', 'planar cleansing').
card_uid('planar cleansing'/'M13', 'M13:Planar Cleansing:planar cleansing').
card_rarity('planar cleansing'/'M13', 'Rare').
card_artist('planar cleansing'/'M13', 'Michael Komarck').
card_number('planar cleansing'/'M13', '26').
card_multiverse_id('planar cleansing'/'M13', '275269').

card_in_set('plummet', 'M13').
card_original_type('plummet'/'M13', 'Instant').
card_original_text('plummet'/'M13', 'Destroy target creature with flying.').
card_image_name('plummet'/'M13', 'plummet').
card_uid('plummet'/'M13', 'M13:Plummet:plummet').
card_rarity('plummet'/'M13', 'Common').
card_artist('plummet'/'M13', 'Pete Venters').
card_number('plummet'/'M13', '179').
card_flavor_text('plummet'/'M13', '\"Let nothing own the skies but the wind.\"\n—Dejara, Giltwood druid').
card_multiverse_id('plummet'/'M13', '253675').

card_in_set('predatory rampage', 'M13').
card_original_type('predatory rampage'/'M13', 'Sorcery').
card_original_text('predatory rampage'/'M13', 'Creatures you control get +3/+3 until end of turn. Each creature your opponents control blocks this turn if able.').
card_first_print('predatory rampage', 'M13').
card_image_name('predatory rampage'/'M13', 'predatory rampage').
card_uid('predatory rampage'/'M13', 'M13:Predatory Rampage:predatory rampage').
card_rarity('predatory rampage'/'M13', 'Rare').
card_artist('predatory rampage'/'M13', 'Wayne England').
card_number('predatory rampage'/'M13', '180').
card_flavor_text('predatory rampage'/'M13', '\"It is the fate of the weak to fall.\"\n—Garruk Wildspeaker').
card_multiverse_id('predatory rampage'/'M13', '249714').

card_in_set('prey upon', 'M13').
card_original_type('prey upon'/'M13', 'Sorcery').
card_original_text('prey upon'/'M13', 'Target creature you control fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_image_name('prey upon'/'M13', 'prey upon').
card_uid('prey upon'/'M13', 'M13:Prey Upon:prey upon').
card_rarity('prey upon'/'M13', 'Common').
card_artist('prey upon'/'M13', 'Dave Kendall').
card_number('prey upon'/'M13', '181').
card_flavor_text('prey upon'/'M13', '\"You don\'t find many old werewolf hunters.\"\n—Paulin, trapper of Somberwald').
card_multiverse_id('prey upon'/'M13', '271366').

card_in_set('primal clay', 'M13').
card_original_type('primal clay'/'M13', 'Artifact Creature — Shapeshifter').
card_original_text('primal clay'/'M13', 'As Primal Clay enters the battlefield, it becomes your choice of a 3/3 artifact creature, a 2/2 artifact creature with flying, or a 1/6 Wall artifact creature with defender in addition to its other types. (A creature with defender can\'t attack.)').
card_image_name('primal clay'/'M13', 'primal clay').
card_uid('primal clay'/'M13', 'M13:Primal Clay:primal clay').
card_rarity('primal clay'/'M13', 'Uncommon').
card_artist('primal clay'/'M13', 'Lucas Graciano').
card_number('primal clay'/'M13', '210').
card_multiverse_id('primal clay'/'M13', '268530').

card_in_set('primal huntbeast', 'M13').
card_original_type('primal huntbeast'/'M13', 'Creature — Beast').
card_original_text('primal huntbeast'/'M13', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_first_print('primal huntbeast', 'M13').
card_image_name('primal huntbeast'/'M13', 'primal huntbeast').
card_uid('primal huntbeast'/'M13', 'M13:Primal Huntbeast:primal huntbeast').
card_rarity('primal huntbeast'/'M13', 'Common').
card_artist('primal huntbeast'/'M13', 'Chris Rahn').
card_number('primal huntbeast'/'M13', '182').
card_flavor_text('primal huntbeast'/'M13', '\"To the unskilled, it appears as blurred patches of distorted light. Few have seen its true form.\"\n—Garruk Wildspeaker').
card_multiverse_id('primal huntbeast'/'M13', '249677').

card_in_set('primordial hydra', 'M13').
card_original_type('primordial hydra'/'M13', 'Creature — Hydra').
card_original_text('primordial hydra'/'M13', 'Primordial Hydra enters the battlefield with X +1/+1 counters on it.\nAt the beginning of your upkeep, double the number of +1/+1 counters on Primordial Hydra.\nPrimordial Hydra has trample as long as it has ten or more +1/+1 counters on it.').
card_image_name('primordial hydra'/'M13', 'primordial hydra').
card_uid('primordial hydra'/'M13', 'M13:Primordial Hydra:primordial hydra').
card_rarity('primordial hydra'/'M13', 'Mythic Rare').
card_artist('primordial hydra'/'M13', 'Aleksi Briclot').
card_number('primordial hydra'/'M13', '183').
card_multiverse_id('primordial hydra'/'M13', '253670').

card_in_set('prized elephant', 'M13').
card_original_type('prized elephant'/'M13', 'Creature — Elephant').
card_original_text('prized elephant'/'M13', 'Prized Elephant gets +1/+1 as long as you control a Forest.\n{G}: Prized Elephant gains trample until end of turn. (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('prized elephant', 'M13').
card_image_name('prized elephant'/'M13', 'prized elephant').
card_uid('prized elephant'/'M13', 'M13:Prized Elephant:prized elephant').
card_rarity('prized elephant'/'M13', 'Uncommon').
card_artist('prized elephant'/'M13', 'Ioan Dumitrescu').
card_number('prized elephant'/'M13', '27').
card_multiverse_id('prized elephant'/'M13', '253719').

card_in_set('public execution', 'M13').
card_original_type('public execution'/'M13', 'Instant').
card_original_text('public execution'/'M13', 'Destroy target creature an opponent controls. Each other creature that player controls gets -2/-0 until end of turn.').
card_first_print('public execution', 'M13').
card_image_name('public execution'/'M13', 'public execution').
card_uid('public execution'/'M13', 'M13:Public Execution:public execution').
card_rarity('public execution'/'M13', 'Uncommon').
card_artist('public execution'/'M13', 'Anthony Palumbo').
card_number('public execution'/'M13', '105').
card_flavor_text('public execution'/'M13', 'Though the executioner did not speak, the villagers got the message.').
card_multiverse_id('public execution'/'M13', '253738').

card_in_set('quirion dryad', 'M13').
card_original_type('quirion dryad'/'M13', 'Creature — Dryad').
card_original_text('quirion dryad'/'M13', 'Whenever you cast a white, blue, black, or red spell, put a +1/+1 counter on Quirion Dryad.').
card_image_name('quirion dryad'/'M13', 'quirion dryad').
card_uid('quirion dryad'/'M13', 'M13:Quirion Dryad:quirion dryad').
card_rarity('quirion dryad'/'M13', 'Rare').
card_artist('quirion dryad'/'M13', 'Todd Lockwood').
card_number('quirion dryad'/'M13', '184').
card_flavor_text('quirion dryad'/'M13', '\"Never underestimate the ability of natural forces to adapt to unnatural influences.\"\n—Molimo, maro-sorcerer').
card_multiverse_id('quirion dryad'/'M13', '288764').

card_in_set('rain of blades', 'M13').
card_original_type('rain of blades'/'M13', 'Instant').
card_original_text('rain of blades'/'M13', 'Rain of Blades deals 1 damage to each attacking creature.').
card_image_name('rain of blades'/'M13', 'rain of blades').
card_uid('rain of blades'/'M13', 'M13:Rain of Blades:rain of blades').
card_rarity('rain of blades'/'M13', 'Uncommon').
card_artist('rain of blades'/'M13', 'Rob Alexander').
card_number('rain of blades'/'M13', '28').
card_flavor_text('rain of blades'/'M13', 'Some say they are the weapons of heroes fallen in battle, eager for one last chance at glory.').
card_multiverse_id('rain of blades'/'M13', '279986').

card_in_set('rancor', 'M13').
card_original_type('rancor'/'M13', 'Enchantment — Aura').
card_original_text('rancor'/'M13', 'Enchant creature\nEnchanted creature gets +2/+0 and has trample.\nWhen Rancor is put into a graveyard from the battlefield, return Rancor to its owner\'s hand.').
card_image_name('rancor'/'M13', 'rancor').
card_uid('rancor'/'M13', 'M13:Rancor:rancor').
card_rarity('rancor'/'M13', 'Uncommon').
card_artist('rancor'/'M13', 'Kev Walker').
card_number('rancor'/'M13', '185').
card_multiverse_id('rancor'/'M13', '253686').

card_in_set('ranger\'s path', 'M13').
card_original_type('ranger\'s path'/'M13', 'Sorcery').
card_original_text('ranger\'s path'/'M13', 'Search your library for up to two Forest cards and put them onto the battlefield tapped. Then shuffle your library.').
card_first_print('ranger\'s path', 'M13').
card_image_name('ranger\'s path'/'M13', 'ranger\'s path').
card_uid('ranger\'s path'/'M13', 'M13:Ranger\'s Path:ranger\'s path').
card_rarity('ranger\'s path'/'M13', 'Common').
card_artist('ranger\'s path'/'M13', 'Tomasz Jedruszek').
card_number('ranger\'s path'/'M13', '186').
card_flavor_text('ranger\'s path'/'M13', 'The forest can seem like a dense maze. Those who travel the canopy know otherwise.').
card_multiverse_id('ranger\'s path'/'M13', '275699').

card_in_set('ravenous rats', 'M13').
card_original_type('ravenous rats'/'M13', 'Creature — Rat').
card_original_text('ravenous rats'/'M13', 'When Ravenous Rats enters the battlefield, target opponent discards a card.').
card_image_name('ravenous rats'/'M13', 'ravenous rats').
card_uid('ravenous rats'/'M13', 'M13:Ravenous Rats:ravenous rats').
card_rarity('ravenous rats'/'M13', 'Common').
card_artist('ravenous rats'/'M13', 'Carl Critchlow').
card_number('ravenous rats'/'M13', '106').
card_flavor_text('ravenous rats'/'M13', 'Devouring books is only the beginning. The librarian\'s next.').
card_multiverse_id('ravenous rats'/'M13', '279715').

card_in_set('reckless brute', 'M13').
card_original_type('reckless brute'/'M13', 'Creature — Ogre Warrior').
card_original_text('reckless brute'/'M13', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nReckless Brute attacks each turn if able.').
card_first_print('reckless brute', 'M13').
card_image_name('reckless brute'/'M13', 'reckless brute').
card_uid('reckless brute'/'M13', 'M13:Reckless Brute:reckless brute').
card_rarity('reckless brute'/'M13', 'Common').
card_artist('reckless brute'/'M13', 'Johann Bodin').
card_number('reckless brute'/'M13', '144').
card_flavor_text('reckless brute'/'M13', 'His mind knows no fear, registering only the burning impulse to destroy.').
card_multiverse_id('reckless brute'/'M13', '253715').

card_in_set('redirect', 'M13').
card_original_type('redirect'/'M13', 'Instant').
card_original_text('redirect'/'M13', 'You may choose new targets for target spell.').
card_image_name('redirect'/'M13', 'redirect').
card_uid('redirect'/'M13', 'M13:Redirect:redirect').
card_rarity('redirect'/'M13', 'Rare').
card_artist('redirect'/'M13', 'Izzy').
card_number('redirect'/'M13', '64').
card_flavor_text('redirect'/'M13', '\"It\'s actually quite simple, but since you\'ve only recently begun to walk upright, it may take some time to explain.\"\n—Jace Beleren, to Garruk Wildspeaker').
card_multiverse_id('redirect'/'M13', '290288').

card_in_set('reliquary tower', 'M13').
card_original_type('reliquary tower'/'M13', 'Land').
card_original_text('reliquary tower'/'M13', 'You have no maximum hand size.\n{T}: Add {1} to your mana pool.').
card_image_name('reliquary tower'/'M13', 'reliquary tower').
card_uid('reliquary tower'/'M13', 'M13:Reliquary Tower:reliquary tower').
card_rarity('reliquary tower'/'M13', 'Uncommon').
card_artist('reliquary tower'/'M13', 'Jesper Ejsing').
card_number('reliquary tower'/'M13', '227').
card_flavor_text('reliquary tower'/'M13', 'Once guarded by the Knights of the Reliquary, the tower stands now protected only by its own remoteness, its dusty treasures open to plunder by anyone.').
card_multiverse_id('reliquary tower'/'M13', '288876').

card_in_set('reverberate', 'M13').
card_original_type('reverberate'/'M13', 'Instant').
card_original_text('reverberate'/'M13', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_image_name('reverberate'/'M13', 'reverberate').
card_uid('reverberate'/'M13', 'M13:Reverberate:reverberate').
card_rarity('reverberate'/'M13', 'Rare').
card_artist('reverberate'/'M13', 'jD').
card_number('reverberate'/'M13', '145').
card_flavor_text('reverberate'/'M13', '\"Not bad, but I can think of a better use for that.\"').
card_multiverse_id('reverberate'/'M13', '290289').

card_in_set('revive', 'M13').
card_original_type('revive'/'M13', 'Sorcery').
card_original_text('revive'/'M13', 'Return target green card from your graveyard to your hand.').
card_image_name('revive'/'M13', 'revive').
card_uid('revive'/'M13', 'M13:Revive:revive').
card_rarity('revive'/'M13', 'Uncommon').
card_artist('revive'/'M13', 'Matthew D. Wilson').
card_number('revive'/'M13', '187').
card_flavor_text('revive'/'M13', 'Even what is lost beyond recall returns when coaxed by a dryad\'s hand.').
card_multiverse_id('revive'/'M13', '276223').

card_in_set('rewind', 'M13').
card_original_type('rewind'/'M13', 'Instant').
card_original_text('rewind'/'M13', 'Counter target spell. Untap up to four lands.').
card_image_name('rewind'/'M13', 'rewind').
card_uid('rewind'/'M13', 'M13:Rewind:rewind').
card_rarity('rewind'/'M13', 'Uncommon').
card_artist('rewind'/'M13', 'Dermot Power').
card_number('rewind'/'M13', '65').
card_flavor_text('rewind'/'M13', '\"The best denials are simply that: a beginning and end unchanged, a dream with no memory.\"').
card_multiverse_id('rewind'/'M13', '280222').

card_in_set('rhox faithmender', 'M13').
card_original_type('rhox faithmender'/'M13', 'Creature — Rhino Monk').
card_original_text('rhox faithmender'/'M13', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nIf you would gain life, you gain twice that much life instead.').
card_first_print('rhox faithmender', 'M13').
card_image_name('rhox faithmender'/'M13', 'rhox faithmender').
card_uid('rhox faithmender'/'M13', 'M13:Rhox Faithmender:rhox faithmender').
card_rarity('rhox faithmender'/'M13', 'Rare').
card_artist('rhox faithmender'/'M13', 'Wesley Burt').
card_number('rhox faithmender'/'M13', '29').
card_flavor_text('rhox faithmender'/'M13', 'Rhoxes who live long enough to retire from war lend support with their extensive knowledge of combat injuries.').
card_multiverse_id('rhox faithmender'/'M13', '279987').

card_in_set('ring of evos isle', 'M13').
card_original_type('ring of evos isle'/'M13', 'Artifact — Equipment').
card_original_text('ring of evos isle'/'M13', '{2}: Equipped creature gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s blue.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('ring of evos isle', 'M13').
card_image_name('ring of evos isle'/'M13', 'ring of evos isle').
card_uid('ring of evos isle'/'M13', 'M13:Ring of Evos Isle:ring of evos isle').
card_rarity('ring of evos isle'/'M13', 'Uncommon').
card_artist('ring of evos isle'/'M13', 'Erica Yang').
card_number('ring of evos isle'/'M13', '211').
card_multiverse_id('ring of evos isle'/'M13', '253690').

card_in_set('ring of kalonia', 'M13').
card_original_type('ring of kalonia'/'M13', 'Artifact — Equipment').
card_original_text('ring of kalonia'/'M13', 'Equipped creature has trample. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s green.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('ring of kalonia', 'M13').
card_image_name('ring of kalonia'/'M13', 'ring of kalonia').
card_uid('ring of kalonia'/'M13', 'M13:Ring of Kalonia:ring of kalonia').
card_rarity('ring of kalonia'/'M13', 'Uncommon').
card_artist('ring of kalonia'/'M13', 'Erica Yang').
card_number('ring of kalonia'/'M13', '212').
card_multiverse_id('ring of kalonia'/'M13', '259699').

card_in_set('ring of thune', 'M13').
card_original_type('ring of thune'/'M13', 'Artifact — Equipment').
card_original_text('ring of thune'/'M13', 'Equipped creature has vigilance. (Attacking doesn\'t cause it to tap.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s white.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('ring of thune', 'M13').
card_image_name('ring of thune'/'M13', 'ring of thune').
card_uid('ring of thune'/'M13', 'M13:Ring of Thune:ring of thune').
card_rarity('ring of thune'/'M13', 'Uncommon').
card_artist('ring of thune'/'M13', 'Erica Yang').
card_number('ring of thune'/'M13', '213').
card_multiverse_id('ring of thune'/'M13', '253696').

card_in_set('ring of valkas', 'M13').
card_original_type('ring of valkas'/'M13', 'Artifact — Equipment').
card_original_text('ring of valkas'/'M13', 'Equipped creature has haste. (It can attack and {T} no matter when it came under your control.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s red.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('ring of valkas', 'M13').
card_image_name('ring of valkas'/'M13', 'ring of valkas').
card_uid('ring of valkas'/'M13', 'M13:Ring of Valkas:ring of valkas').
card_rarity('ring of valkas'/'M13', 'Uncommon').
card_artist('ring of valkas'/'M13', 'Erica Yang').
card_number('ring of valkas'/'M13', '214').
card_multiverse_id('ring of valkas'/'M13', '259689').

card_in_set('ring of xathrid', 'M13').
card_original_type('ring of xathrid'/'M13', 'Artifact — Equipment').
card_original_text('ring of xathrid'/'M13', '{2}: Regenerate equipped creature. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)\nAt the beginning of your upkeep, put a +1/+1 counter on equipped creature if it\'s black.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('ring of xathrid', 'M13').
card_image_name('ring of xathrid'/'M13', 'ring of xathrid').
card_uid('ring of xathrid'/'M13', 'M13:Ring of Xathrid:ring of xathrid').
card_rarity('ring of xathrid'/'M13', 'Uncommon').
card_artist('ring of xathrid'/'M13', 'Erica Yang').
card_number('ring of xathrid'/'M13', '215').
card_multiverse_id('ring of xathrid'/'M13', '259692').

card_in_set('rise from the grave', 'M13').
card_original_type('rise from the grave'/'M13', 'Sorcery').
card_original_text('rise from the grave'/'M13', 'Put target creature card from a graveyard onto the battlefield under your control. That creature is a black Zombie in addition to its other colors and types.').
card_image_name('rise from the grave'/'M13', 'rise from the grave').
card_uid('rise from the grave'/'M13', 'M13:Rise from the Grave:rise from the grave').
card_rarity('rise from the grave'/'M13', 'Uncommon').
card_artist('rise from the grave'/'M13', 'Vance Kovacs').
card_number('rise from the grave'/'M13', '107').
card_flavor_text('rise from the grave'/'M13', '\"For my enemies, one death. For my allies, many.\"\n—Liliana Vess').
card_multiverse_id('rise from the grave'/'M13', '260981').

card_in_set('roaring primadox', 'M13').
card_original_type('roaring primadox'/'M13', 'Creature — Beast').
card_original_text('roaring primadox'/'M13', 'At the beginning of your upkeep, return a creature you control to its owner\'s hand.').
card_first_print('roaring primadox', 'M13').
card_image_name('roaring primadox'/'M13', 'roaring primadox').
card_uid('roaring primadox'/'M13', 'M13:Roaring Primadox:roaring primadox').
card_rarity('roaring primadox'/'M13', 'Uncommon').
card_artist('roaring primadox'/'M13', 'James Ryman').
card_number('roaring primadox'/'M13', '188').
card_flavor_text('roaring primadox'/'M13', '\"They\'re easy enough to find. Question is, are you sure you want to find one?\"\n—Juruk, Kalonian tracker').
card_multiverse_id('roaring primadox'/'M13', '249687').

card_in_set('rootbound crag', 'M13').
card_original_type('rootbound crag'/'M13', 'Land').
card_original_text('rootbound crag'/'M13', 'Rootbound Crag enters the battlefield tapped unless you control a Mountain or a Forest.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('rootbound crag'/'M13', 'rootbound crag').
card_uid('rootbound crag'/'M13', 'M13:Rootbound Crag:rootbound crag').
card_rarity('rootbound crag'/'M13', 'Rare').
card_artist('rootbound crag'/'M13', 'Matt Stewart').
card_number('rootbound crag'/'M13', '228').
card_multiverse_id('rootbound crag'/'M13', '249735').

card_in_set('rummaging goblin', 'M13').
card_original_type('rummaging goblin'/'M13', 'Creature — Goblin Rogue').
card_original_text('rummaging goblin'/'M13', '{T}, Discard a card: Draw a card.').
card_first_print('rummaging goblin', 'M13').
card_image_name('rummaging goblin'/'M13', 'rummaging goblin').
card_uid('rummaging goblin'/'M13', 'M13:Rummaging Goblin:rummaging goblin').
card_rarity('rummaging goblin'/'M13', 'Common').
card_artist('rummaging goblin'/'M13', 'Karl Kopinski').
card_number('rummaging goblin'/'M13', '146').
card_flavor_text('rummaging goblin'/'M13', 'To a goblin, value is based on the four S\'s: shiny, stabby, smelly, and super smelly.').
card_multiverse_id('rummaging goblin'/'M13', '249705').

card_in_set('safe passage', 'M13').
card_original_type('safe passage'/'M13', 'Instant').
card_original_text('safe passage'/'M13', 'Prevent all damage that would be dealt to you and creatures you control this turn.').
card_image_name('safe passage'/'M13', 'safe passage').
card_uid('safe passage'/'M13', 'M13:Safe Passage:safe passage').
card_rarity('safe passage'/'M13', 'Common').
card_artist('safe passage'/'M13', 'Christopher Moeller').
card_number('safe passage'/'M13', '30').
card_flavor_text('safe passage'/'M13', 'As she shepherds the faithful, the angel\'s radiance is a shield of salvation.').
card_multiverse_id('safe passage'/'M13', '276224').

card_in_set('sands of delirium', 'M13').
card_original_type('sands of delirium'/'M13', 'Artifact').
card_original_text('sands of delirium'/'M13', '{X}, {T}: Target player puts the top X cards of his or her library into his or her graveyard.').
card_first_print('sands of delirium', 'M13').
card_image_name('sands of delirium'/'M13', 'sands of delirium').
card_uid('sands of delirium'/'M13', 'M13:Sands of Delirium:sands of delirium').
card_rarity('sands of delirium'/'M13', 'Rare').
card_artist('sands of delirium'/'M13', 'Charles Urbach').
card_number('sands of delirium'/'M13', '216').
card_flavor_text('sands of delirium'/'M13', 'It counts down your last few moments of sanity.').
card_multiverse_id('sands of delirium'/'M13', '265133').

card_in_set('scroll thief', 'M13').
card_original_type('scroll thief'/'M13', 'Creature — Merfolk Rogue').
card_original_text('scroll thief'/'M13', 'Whenever Scroll Thief deals combat damage to a player, draw a card.').
card_image_name('scroll thief'/'M13', 'scroll thief').
card_uid('scroll thief'/'M13', 'M13:Scroll Thief:scroll thief').
card_rarity('scroll thief'/'M13', 'Common').
card_artist('scroll thief'/'M13', 'Alex Horley-Orlandelli').
card_number('scroll thief'/'M13', '66').
card_flavor_text('scroll thief'/'M13', 'The arcane academies along the coast were powerless to protect themselves from the brazen raiders of the Kapsho Seas.').
card_multiverse_id('scroll thief'/'M13', '254130').

card_in_set('searing spear', 'M13').
card_original_type('searing spear'/'M13', 'Instant').
card_original_text('searing spear'/'M13', 'Searing Spear deals 3 damage to target creature or player.').
card_image_name('searing spear'/'M13', 'searing spear').
card_uid('searing spear'/'M13', 'M13:Searing Spear:searing spear').
card_rarity('searing spear'/'M13', 'Common').
card_artist('searing spear'/'M13', 'Chris Rahn').
card_number('searing spear'/'M13', '147').
card_flavor_text('searing spear'/'M13', 'Sometimes you die a glorious death with your sword held high. Sometimes you\'re just target practice.').
card_multiverse_id('searing spear'/'M13', '249684').

card_in_set('sentinel spider', 'M13').
card_original_type('sentinel spider'/'M13', 'Creature — Spider').
card_original_text('sentinel spider'/'M13', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nReach (This creature can block creatures with flying.)').
card_first_print('sentinel spider', 'M13').
card_image_name('sentinel spider'/'M13', 'sentinel spider').
card_uid('sentinel spider'/'M13', 'M13:Sentinel Spider:sentinel spider').
card_rarity('sentinel spider'/'M13', 'Common').
card_artist('sentinel spider'/'M13', 'Vincent Proce').
card_number('sentinel spider'/'M13', '189').
card_flavor_text('sentinel spider'/'M13', '\"Your first reaction may be to stand very still and hope she didn\'t see you. Trust me, she did.\"\n—Endril, Kalonian naturalist').
card_multiverse_id('sentinel spider'/'M13', '249681').

card_in_set('serpent\'s gift', 'M13').
card_original_type('serpent\'s gift'/'M13', 'Instant').
card_original_text('serpent\'s gift'/'M13', 'Target creature gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('serpent\'s gift', 'M13').
card_image_name('serpent\'s gift'/'M13', 'serpent\'s gift').
card_uid('serpent\'s gift'/'M13', 'M13:Serpent\'s Gift:serpent\'s gift').
card_rarity('serpent\'s gift'/'M13', 'Common').
card_artist('serpent\'s gift'/'M13', 'Steve Argyle').
card_number('serpent\'s gift'/'M13', '190').
card_flavor_text('serpent\'s gift'/'M13', 'Venom is nature\'s way of redressing the imbalance between weak and mighty.').
card_multiverse_id('serpent\'s gift'/'M13', '249666').

card_in_set('serra angel', 'M13').
card_original_type('serra angel'/'M13', 'Creature — Angel').
card_original_text('serra angel'/'M13', 'Flying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'M13', 'serra angel').
card_uid('serra angel'/'M13', 'M13:Serra Angel:serra angel').
card_rarity('serra angel'/'M13', 'Uncommon').
card_artist('serra angel'/'M13', 'Greg Staples').
card_number('serra angel'/'M13', '31').
card_flavor_text('serra angel'/'M13', 'Follow the light. In its absence, follow her.').
card_multiverse_id('serra angel'/'M13', '270376').

card_in_set('serra avatar', 'M13').
card_original_type('serra avatar'/'M13', 'Creature — Avatar').
card_original_text('serra avatar'/'M13', 'Serra Avatar\'s power and toughness are each equal to your life total.\nWhen Serra Avatar is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('serra avatar'/'M13', 'serra avatar').
card_uid('serra avatar'/'M13', 'M13:Serra Avatar:serra avatar').
card_rarity('serra avatar'/'M13', 'Mythic Rare').
card_artist('serra avatar'/'M13', 'Dermot Power').
card_number('serra avatar'/'M13', '32').
card_flavor_text('serra avatar'/'M13', '\"Serra isn\'t dead. She lives on through me.\"').
card_multiverse_id('serra avatar'/'M13', '259706').

card_in_set('serra avenger', 'M13').
card_original_type('serra avenger'/'M13', 'Creature — Angel').
card_original_text('serra avenger'/'M13', 'You can\'t cast Serra Avenger during your first, second, or third turns of the game.\nFlying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra avenger'/'M13', 'serra avenger').
card_uid('serra avenger'/'M13', 'M13:Serra Avenger:serra avenger').
card_rarity('serra avenger'/'M13', 'Rare').
card_artist('serra avenger'/'M13', 'Scott M. Fischer').
card_number('serra avenger'/'M13', '33').
card_multiverse_id('serra avenger'/'M13', '288762').

card_in_set('servant of nefarox', 'M13').
card_original_type('servant of nefarox'/'M13', 'Creature — Human Cleric').
card_original_text('servant of nefarox'/'M13', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_first_print('servant of nefarox', 'M13').
card_image_name('servant of nefarox'/'M13', 'servant of nefarox').
card_uid('servant of nefarox'/'M13', 'M13:Servant of Nefarox:servant of nefarox').
card_rarity('servant of nefarox'/'M13', 'Common').
card_artist('servant of nefarox'/'M13', 'Igor Kieryluk').
card_number('servant of nefarox'/'M13', '108').
card_flavor_text('servant of nefarox'/'M13', '\"My cause is power, and at the altar of power I will gladly forfeit my soul.\"').
card_multiverse_id('servant of nefarox'/'M13', '265714').

card_in_set('shimian specter', 'M13').
card_original_type('shimian specter'/'M13', 'Creature — Specter').
card_original_text('shimian specter'/'M13', 'Flying\nWhenever Shimian Specter deals combat damage to a player, that player reveals his or her hand. You choose a nonland card from it. Search that player\'s graveyard, hand, and library for all cards with the same name as that card and exile them. Then that player shuffles his or her library.').
card_image_name('shimian specter'/'M13', 'shimian specter').
card_uid('shimian specter'/'M13', 'M13:Shimian Specter:shimian specter').
card_rarity('shimian specter'/'M13', 'Rare').
card_artist('shimian specter'/'M13', 'Anthony S. Waters').
card_number('shimian specter'/'M13', '109').
card_multiverse_id('shimian specter'/'M13', '278072').

card_in_set('show of valor', 'M13').
card_original_type('show of valor'/'M13', 'Instant').
card_original_text('show of valor'/'M13', 'Target creature gets +2/+4 until end of turn.').
card_first_print('show of valor', 'M13').
card_image_name('show of valor'/'M13', 'show of valor').
card_uid('show of valor'/'M13', 'M13:Show of Valor:show of valor').
card_rarity('show of valor'/'M13', 'Common').
card_artist('show of valor'/'M13', 'Anthony Palumbo').
card_number('show of valor'/'M13', '34').
card_flavor_text('show of valor'/'M13', '\"Duty, honor, and valor are either in your heart or they are not. You will never know for certain until you are tested.\"\n—Ajani Goldmane').
card_multiverse_id('show of valor'/'M13', '265727').

card_in_set('sign in blood', 'M13').
card_original_type('sign in blood'/'M13', 'Sorcery').
card_original_text('sign in blood'/'M13', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'M13', 'sign in blood').
card_uid('sign in blood'/'M13', 'M13:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'M13', 'Common').
card_artist('sign in blood'/'M13', 'Howard Lyon').
card_number('sign in blood'/'M13', '110').
card_flavor_text('sign in blood'/'M13', 'Little agonies pave the way to greater power.').
card_multiverse_id('sign in blood'/'M13', '260982').

card_in_set('silklash spider', 'M13').
card_original_type('silklash spider'/'M13', 'Creature — Spider').
card_original_text('silklash spider'/'M13', 'Reach (This creature can block creatures with flying.)\n{X}{G}{G}: Silklash Spider deals X damage to each creature with flying.').
card_image_name('silklash spider'/'M13', 'silklash spider').
card_uid('silklash spider'/'M13', 'M13:Silklash Spider:silklash spider').
card_rarity('silklash spider'/'M13', 'Rare').
card_artist('silklash spider'/'M13', 'Iain McCaig').
card_number('silklash spider'/'M13', '191').
card_flavor_text('silklash spider'/'M13', 'By the time the spider comes to slurp up its dinner, its victims have been partially dissolved by acidic silk.').
card_multiverse_id('silklash spider'/'M13', '292718').

card_in_set('silvercoat lion', 'M13').
card_original_type('silvercoat lion'/'M13', 'Creature — Cat').
card_original_text('silvercoat lion'/'M13', '').
card_image_name('silvercoat lion'/'M13', 'silvercoat lion').
card_uid('silvercoat lion'/'M13', 'M13:Silvercoat Lion:silvercoat lion').
card_rarity('silvercoat lion'/'M13', 'Common').
card_artist('silvercoat lion'/'M13', 'Terese Nielsen').
card_number('silvercoat lion'/'M13', '35').
card_flavor_text('silvercoat lion'/'M13', '\"In the wild, white fur like mine is an aberration. We lack natural camouflage, but the inability to hide encourages other strengths.\"\n—Ajani Goldmane').
card_multiverse_id('silvercoat lion'/'M13', '266132').

card_in_set('sleep', 'M13').
card_original_type('sleep'/'M13', 'Sorcery').
card_original_text('sleep'/'M13', 'Tap all creatures target player controls. Those creatures don\'t untap during that player\'s next untap step.').
card_image_name('sleep'/'M13', 'sleep').
card_uid('sleep'/'M13', 'M13:Sleep:sleep').
card_rarity('sleep'/'M13', 'Uncommon').
card_artist('sleep'/'M13', 'Chris Rahn').
card_number('sleep'/'M13', '67').
card_flavor_text('sleep'/'M13', '\"I give them dreams so wondrous that they hesitate to return to the world of the conscious.\"\n—Garild, merfolk mage').
card_multiverse_id('sleep'/'M13', '254119').

card_in_set('slumbering dragon', 'M13').
card_original_type('slumbering dragon'/'M13', 'Creature — Dragon').
card_original_text('slumbering dragon'/'M13', 'Flying\nSlumbering Dragon can\'t attack or block unless it has five or more +1/+1 counters on it.\nWhenever a creature attacks you or a planeswalker you control, put a +1/+1 counter on Slumbering Dragon.').
card_first_print('slumbering dragon', 'M13').
card_image_name('slumbering dragon'/'M13', 'slumbering dragon').
card_uid('slumbering dragon'/'M13', 'M13:Slumbering Dragon:slumbering dragon').
card_rarity('slumbering dragon'/'M13', 'Rare').
card_artist('slumbering dragon'/'M13', 'Chris Rahn').
card_number('slumbering dragon'/'M13', '148').
card_multiverse_id('slumbering dragon'/'M13', '253693').

card_in_set('smelt', 'M13').
card_original_type('smelt'/'M13', 'Instant').
card_original_text('smelt'/'M13', 'Destroy target artifact.').
card_first_print('smelt', 'M13').
card_image_name('smelt'/'M13', 'smelt').
card_uid('smelt'/'M13', 'M13:Smelt:smelt').
card_rarity('smelt'/'M13', 'Common').
card_artist('smelt'/'M13', 'Zoltan Boros').
card_number('smelt'/'M13', '149').
card_flavor_text('smelt'/'M13', '\"Looks like that wasn\'t darksteel after all.\"\n—Koth of the Hammer').
card_multiverse_id('smelt'/'M13', '249691').

card_in_set('spelltwine', 'M13').
card_original_type('spelltwine'/'M13', 'Sorcery').
card_original_text('spelltwine'/'M13', 'Exile target instant or sorcery card from your graveyard and target instant or sorcery card from an opponent\'s graveyard. Copy those cards. Cast the copies if able without paying their mana costs. Exile Spelltwine.').
card_first_print('spelltwine', 'M13').
card_image_name('spelltwine'/'M13', 'spelltwine').
card_uid('spelltwine'/'M13', 'M13:Spelltwine:spelltwine').
card_rarity('spelltwine'/'M13', 'Rare').
card_artist('spelltwine'/'M13', 'Noah Bradley').
card_number('spelltwine'/'M13', '68').
card_multiverse_id('spelltwine'/'M13', '280223').

card_in_set('sphinx of uthuun', 'M13').
card_original_type('sphinx of uthuun'/'M13', 'Creature — Sphinx').
card_original_text('sphinx of uthuun'/'M13', 'Flying\nWhen Sphinx of Uthuun enters the battlefield, reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_image_name('sphinx of uthuun'/'M13', 'sphinx of uthuun').
card_uid('sphinx of uthuun'/'M13', 'M13:Sphinx of Uthuun:sphinx of uthuun').
card_rarity('sphinx of uthuun'/'M13', 'Rare').
card_artist('sphinx of uthuun'/'M13', 'Kekai Kotaki').
card_number('sphinx of uthuun'/'M13', '69').
card_multiverse_id('sphinx of uthuun'/'M13', '254108').

card_in_set('spiked baloth', 'M13').
card_original_type('spiked baloth'/'M13', 'Creature — Beast').
card_original_text('spiked baloth'/'M13', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('spiked baloth', 'M13').
card_image_name('spiked baloth'/'M13', 'spiked baloth').
card_uid('spiked baloth'/'M13', 'M13:Spiked Baloth:spiked baloth').
card_rarity('spiked baloth'/'M13', 'Common').
card_artist('spiked baloth'/'M13', 'Daarken').
card_number('spiked baloth'/'M13', '192').
card_flavor_text('spiked baloth'/'M13', 'If a baloth isn\'t hungry, it\'s dead.').
card_multiverse_id('spiked baloth'/'M13', '249702').

card_in_set('staff of nin', 'M13').
card_original_type('staff of nin'/'M13', 'Artifact').
card_original_text('staff of nin'/'M13', 'At the beginning of your upkeep, draw a card.\n{T}: Staff of Nin deals 1 damage to target creature or player.').
card_image_name('staff of nin'/'M13', 'staff of nin').
card_uid('staff of nin'/'M13', 'M13:Staff of Nin:staff of nin').
card_rarity('staff of nin'/'M13', 'Rare').
card_artist('staff of nin'/'M13', 'Dan Scott').
card_number('staff of nin'/'M13', '217').
card_flavor_text('staff of nin'/'M13', '\"I have attuned the staff to your screams so that only I may benefit from your pain.\"\n—Volux, disciple of Nin').
card_multiverse_id('staff of nin'/'M13', '249713').

card_in_set('stormtide leviathan', 'M13').
card_original_type('stormtide leviathan'/'M13', 'Creature — Leviathan').
card_original_text('stormtide leviathan'/'M13', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)\nAll lands are Islands in addition to their other types.\nCreatures without flying or islandwalk can\'t attack.').
card_image_name('stormtide leviathan'/'M13', 'stormtide leviathan').
card_uid('stormtide leviathan'/'M13', 'M13:Stormtide Leviathan:stormtide leviathan').
card_rarity('stormtide leviathan'/'M13', 'Rare').
card_artist('stormtide leviathan'/'M13', 'Karl Kopinski').
card_number('stormtide leviathan'/'M13', '70').
card_multiverse_id('stormtide leviathan'/'M13', '278198').

card_in_set('stuffy doll', 'M13').
card_original_type('stuffy doll'/'M13', 'Artifact Creature — Construct').
card_original_text('stuffy doll'/'M13', 'As Stuffy Doll enters the battlefield, choose a player.\nStuffy Doll is indestructible.\nWhenever Stuffy Doll is dealt damage, it deals that much damage to the chosen player.\n{T}: Stuffy Doll deals 1 damage to itself.').
card_image_name('stuffy doll'/'M13', 'stuffy doll').
card_uid('stuffy doll'/'M13', 'M13:Stuffy Doll:stuffy doll').
card_rarity('stuffy doll'/'M13', 'Rare').
card_artist('stuffy doll'/'M13', 'David Rapoza').
card_number('stuffy doll'/'M13', '218').
card_multiverse_id('stuffy doll'/'M13', '279711').

card_in_set('sublime archangel', 'M13').
card_original_type('sublime archangel'/'M13', 'Creature — Angel').
card_original_text('sublime archangel'/'M13', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nOther creatures you control have exalted. (If a creature has multiple instances of exalted, each triggers separately.)').
card_first_print('sublime archangel', 'M13').
card_image_name('sublime archangel'/'M13', 'sublime archangel').
card_uid('sublime archangel'/'M13', 'M13:Sublime Archangel:sublime archangel').
card_rarity('sublime archangel'/'M13', 'Mythic Rare').
card_artist('sublime archangel'/'M13', 'Cynthia Sheppard').
card_number('sublime archangel'/'M13', '36').
card_multiverse_id('sublime archangel'/'M13', '259725').

card_in_set('sunpetal grove', 'M13').
card_original_type('sunpetal grove'/'M13', 'Land').
card_original_text('sunpetal grove'/'M13', 'Sunpetal Grove enters the battlefield tapped unless you control a Forest or a Plains.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('sunpetal grove'/'M13', 'sunpetal grove').
card_uid('sunpetal grove'/'M13', 'M13:Sunpetal Grove:sunpetal grove').
card_rarity('sunpetal grove'/'M13', 'Rare').
card_artist('sunpetal grove'/'M13', 'Jason Chan').
card_number('sunpetal grove'/'M13', '229').
card_multiverse_id('sunpetal grove'/'M13', '249736').

card_in_set('swamp', 'M13').
card_original_type('swamp'/'M13', 'Basic Land — Swamp').
card_original_text('swamp'/'M13', 'B').
card_image_name('swamp'/'M13', 'swamp1').
card_uid('swamp'/'M13', 'M13:Swamp:swamp1').
card_rarity('swamp'/'M13', 'Basic Land').
card_artist('swamp'/'M13', 'Mike Bierek').
card_number('swamp'/'M13', '238').
card_multiverse_id('swamp'/'M13', '249739').

card_in_set('swamp', 'M13').
card_original_type('swamp'/'M13', 'Basic Land — Swamp').
card_original_text('swamp'/'M13', 'B').
card_image_name('swamp'/'M13', 'swamp2').
card_uid('swamp'/'M13', 'M13:Swamp:swamp2').
card_rarity('swamp'/'M13', 'Basic Land').
card_artist('swamp'/'M13', 'Mike Bierek').
card_number('swamp'/'M13', '239').
card_multiverse_id('swamp'/'M13', '249740').

card_in_set('swamp', 'M13').
card_original_type('swamp'/'M13', 'Basic Land — Swamp').
card_original_text('swamp'/'M13', 'B').
card_image_name('swamp'/'M13', 'swamp3').
card_uid('swamp'/'M13', 'M13:Swamp:swamp3').
card_rarity('swamp'/'M13', 'Basic Land').
card_artist('swamp'/'M13', 'Cliff Childs').
card_number('swamp'/'M13', '240').
card_multiverse_id('swamp'/'M13', '249738').

card_in_set('swamp', 'M13').
card_original_type('swamp'/'M13', 'Basic Land — Swamp').
card_original_text('swamp'/'M13', 'B').
card_image_name('swamp'/'M13', 'swamp4').
card_uid('swamp'/'M13', 'M13:Swamp:swamp4').
card_rarity('swamp'/'M13', 'Basic Land').
card_artist('swamp'/'M13', 'Jung Park').
card_number('swamp'/'M13', '241').
card_multiverse_id('swamp'/'M13', '249737').

card_in_set('switcheroo', 'M13').
card_original_type('switcheroo'/'M13', 'Sorcery').
card_original_text('switcheroo'/'M13', 'Exchange control of two target creatures.').
card_first_print('switcheroo', 'M13').
card_image_name('switcheroo'/'M13', 'switcheroo').
card_uid('switcheroo'/'M13', 'M13:Switcheroo:switcheroo').
card_rarity('switcheroo'/'M13', 'Uncommon').
card_artist('switcheroo'/'M13', 'Kev Walker').
card_number('switcheroo'/'M13', '71').
card_flavor_text('switcheroo'/'M13', 'Say hello to your new friend.').
card_multiverse_id('switcheroo'/'M13', '253704').

card_in_set('talrand\'s invocation', 'M13').
card_original_type('talrand\'s invocation'/'M13', 'Sorcery').
card_original_text('talrand\'s invocation'/'M13', 'Put two 2/2 blue Drake creature tokens with flying onto the battlefield.').
card_first_print('talrand\'s invocation', 'M13').
card_image_name('talrand\'s invocation'/'M13', 'talrand\'s invocation').
card_uid('talrand\'s invocation'/'M13', 'M13:Talrand\'s Invocation:talrand\'s invocation').
card_rarity('talrand\'s invocation'/'M13', 'Uncommon').
card_artist('talrand\'s invocation'/'M13', 'Svetlin Velinov').
card_number('talrand\'s invocation'/'M13', '73').
card_flavor_text('talrand\'s invocation'/'M13', 'After Talrand conquered the depths of Shandalar, his ambitions drove him skyward, joined by servants whose drive and curiosity equaled his own.').
card_multiverse_id('talrand\'s invocation'/'M13', '254117').

card_in_set('talrand, sky summoner', 'M13').
card_original_type('talrand, sky summoner'/'M13', 'Legendary Creature — Merfolk Wizard').
card_original_text('talrand, sky summoner'/'M13', 'Whenever you cast an instant or sorcery spell, put a 2/2 blue Drake creature token with flying onto the battlefield.').
card_first_print('talrand, sky summoner', 'M13').
card_image_name('talrand, sky summoner'/'M13', 'talrand, sky summoner').
card_uid('talrand, sky summoner'/'M13', 'M13:Talrand, Sky Summoner:talrand, sky summoner').
card_rarity('talrand, sky summoner'/'M13', 'Rare').
card_artist('talrand, sky summoner'/'M13', 'Svetlin Velinov').
card_number('talrand, sky summoner'/'M13', '72').
card_flavor_text('talrand, sky summoner'/'M13', '\"The seas are vast, but the skies are even more so. Why be content with one kingdom when I can rule them both?\"').
card_multiverse_id('talrand, sky summoner'/'M13', '253701').

card_in_set('thragtusk', 'M13').
card_original_type('thragtusk'/'M13', 'Creature — Beast').
card_original_text('thragtusk'/'M13', 'When Thragtusk enters the battlefield, you gain 5 life.\nWhen Thragtusk leaves the battlefield, put a 3/3 green Beast creature token onto the battlefield.').
card_first_print('thragtusk', 'M13').
card_image_name('thragtusk'/'M13', 'thragtusk').
card_uid('thragtusk'/'M13', 'M13:Thragtusk:thragtusk').
card_rarity('thragtusk'/'M13', 'Rare').
card_artist('thragtusk'/'M13', 'Nils Hamm').
card_number('thragtusk'/'M13', '193').
card_flavor_text('thragtusk'/'M13', '\"Always carry two spears.\"\n—Mokgar, Kalonian hunter').
card_multiverse_id('thragtusk'/'M13', '249685').

card_in_set('thundermaw hellkite', 'M13').
card_original_type('thundermaw hellkite'/'M13', 'Creature — Dragon').
card_original_text('thundermaw hellkite'/'M13', 'Flying\nHaste (This creature can attack and {T} as soon as it comes under your control.)\nWhen Thundermaw Hellkite enters the battlefield, it deals 1 damage to each creature with flying your opponents control. Tap those creatures.').
card_first_print('thundermaw hellkite', 'M13').
card_image_name('thundermaw hellkite'/'M13', 'thundermaw hellkite').
card_uid('thundermaw hellkite'/'M13', 'M13:Thundermaw Hellkite:thundermaw hellkite').
card_rarity('thundermaw hellkite'/'M13', 'Mythic Rare').
card_artist('thundermaw hellkite'/'M13', 'Svetlin Velinov').
card_number('thundermaw hellkite'/'M13', '150').
card_multiverse_id('thundermaw hellkite'/'M13', '253700').

card_in_set('timberpack wolf', 'M13').
card_original_type('timberpack wolf'/'M13', 'Creature — Wolf').
card_original_text('timberpack wolf'/'M13', 'Timberpack Wolf gets +1/+1 for each other creature you control named Timberpack Wolf.').
card_first_print('timberpack wolf', 'M13').
card_image_name('timberpack wolf'/'M13', 'timberpack wolf').
card_uid('timberpack wolf'/'M13', 'M13:Timberpack Wolf:timberpack wolf').
card_rarity('timberpack wolf'/'M13', 'Common').
card_artist('timberpack wolf'/'M13', 'John Avon').
card_number('timberpack wolf'/'M13', '194').
card_flavor_text('timberpack wolf'/'M13', '\"The need for the pack is bred in their bones. Their true strength will not show itself for any other purpose.\"\n—Garruk Wildspeaker').
card_multiverse_id('timberpack wolf'/'M13', '249694').

card_in_set('titanic growth', 'M13').
card_original_type('titanic growth'/'M13', 'Instant').
card_original_text('titanic growth'/'M13', 'Target creature gets +4/+4 until end of turn.').
card_image_name('titanic growth'/'M13', 'titanic growth').
card_uid('titanic growth'/'M13', 'M13:Titanic Growth:titanic growth').
card_rarity('titanic growth'/'M13', 'Common').
card_artist('titanic growth'/'M13', 'Ryan Pancoast').
card_number('titanic growth'/'M13', '195').
card_flavor_text('titanic growth'/'M13', 'The pup looked over the treetops, eyeing the man who just yesterday had kicked her. Suddenly, her hunger was infused with pure delight.').
card_multiverse_id('titanic growth'/'M13', '288934').

card_in_set('torch fiend', 'M13').
card_original_type('torch fiend'/'M13', 'Creature — Devil').
card_original_text('torch fiend'/'M13', '{R}, Sacrifice Torch Fiend: Destroy target artifact.').
card_image_name('torch fiend'/'M13', 'torch fiend').
card_uid('torch fiend'/'M13', 'M13:Torch Fiend:torch fiend').
card_rarity('torch fiend'/'M13', 'Uncommon').
card_artist('torch fiend'/'M13', 'Winona Nelson').
card_number('torch fiend'/'M13', '151').
card_flavor_text('torch fiend'/'M13', 'Devils redecorate every room with fire.').
card_multiverse_id('torch fiend'/'M13', '279989').

card_in_set('tormented soul', 'M13').
card_original_type('tormented soul'/'M13', 'Creature — Spirit').
card_original_text('tormented soul'/'M13', 'Tormented Soul can\'t block and is unblockable.').
card_image_name('tormented soul'/'M13', 'tormented soul').
card_uid('tormented soul'/'M13', 'M13:Tormented Soul:tormented soul').
card_rarity('tormented soul'/'M13', 'Common').
card_artist('tormented soul'/'M13', 'Karl Kopinski').
card_number('tormented soul'/'M13', '111').
card_flavor_text('tormented soul'/'M13', 'Those who raged most bitterly at the world in life are cursed to roam the nether realms in death.').
card_multiverse_id('tormented soul'/'M13', '249668').

card_in_set('tormod\'s crypt', 'M13').
card_original_type('tormod\'s crypt'/'M13', 'Artifact').
card_original_text('tormod\'s crypt'/'M13', '{T}, Sacrifice Tormod\'s Crypt: Exile all cards from target player\'s graveyard.').
card_image_name('tormod\'s crypt'/'M13', 'tormod\'s crypt').
card_uid('tormod\'s crypt'/'M13', 'M13:Tormod\'s Crypt:tormod\'s crypt').
card_rarity('tormod\'s crypt'/'M13', 'Uncommon').
card_artist('tormod\'s crypt'/'M13', 'Lars Grant-West').
card_number('tormod\'s crypt'/'M13', '219').
card_flavor_text('tormod\'s crypt'/'M13', 'Dominaria\'s most extravagant crypt nevertheless holds an empty grave.').
card_multiverse_id('tormod\'s crypt'/'M13', '279713').

card_in_set('touch of the eternal', 'M13').
card_original_type('touch of the eternal'/'M13', 'Enchantment').
card_original_text('touch of the eternal'/'M13', 'At the beginning of your upkeep, count the number of permanents you control. Your life total becomes that number.').
card_first_print('touch of the eternal', 'M13').
card_image_name('touch of the eternal'/'M13', 'touch of the eternal').
card_uid('touch of the eternal'/'M13', 'M13:Touch of the Eternal:touch of the eternal').
card_rarity('touch of the eternal'/'M13', 'Rare').
card_artist('touch of the eternal'/'M13', 'Christopher Moeller').
card_number('touch of the eternal'/'M13', '37').
card_flavor_text('touch of the eternal'/'M13', '\"Time is a vigilant tyrant, but there are those who have broken free of its gaze.\"\n—Laikas, Erunian monk').
card_multiverse_id('touch of the eternal'/'M13', '278197').

card_in_set('trading post', 'M13').
card_original_type('trading post'/'M13', 'Artifact').
card_original_text('trading post'/'M13', '{1}, {T}, Discard a card: You gain 4 life.\n{1}, {T}, Pay 1 life: Put a 0/1 white Goat creature token onto the battlefield.\n{1}, {T}, Sacrifice a creature: Return target artifact card from your graveyard to your hand.\n{1}, {T}, Sacrifice an artifact: Draw a card.').
card_first_print('trading post', 'M13').
card_image_name('trading post'/'M13', 'trading post').
card_uid('trading post'/'M13', 'M13:Trading Post:trading post').
card_rarity('trading post'/'M13', 'Rare').
card_artist('trading post'/'M13', 'Adam Paquette').
card_number('trading post'/'M13', '220').
card_multiverse_id('trading post'/'M13', '253710').

card_in_set('tricks of the trade', 'M13').
card_original_type('tricks of the trade'/'M13', 'Enchantment — Aura').
card_original_text('tricks of the trade'/'M13', 'Enchant creature\nEnchanted creature gets +2/+0 and is unblockable.').
card_first_print('tricks of the trade', 'M13').
card_image_name('tricks of the trade'/'M13', 'tricks of the trade').
card_uid('tricks of the trade'/'M13', 'M13:Tricks of the Trade:tricks of the trade').
card_rarity('tricks of the trade'/'M13', 'Common').
card_artist('tricks of the trade'/'M13', 'Steven Belledin').
card_number('tricks of the trade'/'M13', '74').
card_flavor_text('tricks of the trade'/'M13', '\"Pathetic. You might as well have protected your treasures with a paper box and some string.\"').
card_multiverse_id('tricks of the trade'/'M13', '253735').

card_in_set('trumpet blast', 'M13').
card_original_type('trumpet blast'/'M13', 'Instant').
card_original_text('trumpet blast'/'M13', 'Attacking creatures get +2/+0 until end of turn.').
card_image_name('trumpet blast'/'M13', 'trumpet blast').
card_uid('trumpet blast'/'M13', 'M13:Trumpet Blast:trumpet blast').
card_rarity('trumpet blast'/'M13', 'Common').
card_artist('trumpet blast'/'M13', 'Carl Critchlow').
card_number('trumpet blast'/'M13', '152').
card_flavor_text('trumpet blast'/'M13', 'Keldon warriors don\'t need signals to tell them when to attack. They need signals to tell them when to stop.').
card_multiverse_id('trumpet blast'/'M13', '276606').

card_in_set('turn to slag', 'M13').
card_original_type('turn to slag'/'M13', 'Sorcery').
card_original_text('turn to slag'/'M13', 'Turn to Slag deals 5 damage to target creature. Destroy all Equipment attached to that creature.').
card_image_name('turn to slag'/'M13', 'turn to slag').
card_uid('turn to slag'/'M13', 'M13:Turn to Slag:turn to slag').
card_rarity('turn to slag'/'M13', 'Common').
card_artist('turn to slag'/'M13', 'Zoltan Boros & Gabor Szikszai').
card_number('turn to slag'/'M13', '153').
card_flavor_text('turn to slag'/'M13', '\"If it wasn\'t a blackened, stinking, melted abomination before, it certainly is now.\"\n—Koth of the Hammer').
card_multiverse_id('turn to slag'/'M13', '276212').

card_in_set('unsummon', 'M13').
card_original_type('unsummon'/'M13', 'Instant').
card_original_text('unsummon'/'M13', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'M13', 'unsummon').
card_uid('unsummon'/'M13', 'M13:Unsummon:unsummon').
card_rarity('unsummon'/'M13', 'Common').
card_artist('unsummon'/'M13', 'Izzy').
card_number('unsummon'/'M13', '75').
card_flavor_text('unsummon'/'M13', 'Not to be. That is the answer.').
card_multiverse_id('unsummon'/'M13', '265715').

card_in_set('vampire nighthawk', 'M13').
card_original_type('vampire nighthawk'/'M13', 'Creature — Vampire Shaman').
card_original_text('vampire nighthawk'/'M13', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vampire nighthawk'/'M13', 'vampire nighthawk').
card_uid('vampire nighthawk'/'M13', 'M13:Vampire Nighthawk:vampire nighthawk').
card_rarity('vampire nighthawk'/'M13', 'Uncommon').
card_artist('vampire nighthawk'/'M13', 'Jason Chan').
card_number('vampire nighthawk'/'M13', '112').
card_multiverse_id('vampire nighthawk'/'M13', '260989').

card_in_set('vampire nocturnus', 'M13').
card_original_type('vampire nocturnus'/'M13', 'Creature — Vampire').
card_original_text('vampire nocturnus'/'M13', 'Play with the top card of your library revealed.\nAs long as the top card of your library is black, Vampire Nocturnus and other Vampire creatures you control get +2/+1 and have flying.').
card_image_name('vampire nocturnus'/'M13', 'vampire nocturnus').
card_uid('vampire nocturnus'/'M13', 'M13:Vampire Nocturnus:vampire nocturnus').
card_rarity('vampire nocturnus'/'M13', 'Mythic Rare').
card_artist('vampire nocturnus'/'M13', 'Raymond Swanland').
card_number('vampire nocturnus'/'M13', '113').
card_flavor_text('vampire nocturnus'/'M13', '\"Your life will set with the sun.\"').
card_multiverse_id('vampire nocturnus'/'M13', '275151').

card_in_set('vastwood gorger', 'M13').
card_original_type('vastwood gorger'/'M13', 'Creature — Wurm').
card_original_text('vastwood gorger'/'M13', '').
card_image_name('vastwood gorger'/'M13', 'vastwood gorger').
card_uid('vastwood gorger'/'M13', 'M13:Vastwood Gorger:vastwood gorger').
card_rarity('vastwood gorger'/'M13', 'Common').
card_artist('vastwood gorger'/'M13', 'Kieran Yanner').
card_number('vastwood gorger'/'M13', '196').
card_flavor_text('vastwood gorger'/'M13', '\"A long and difficult incision revealed that all vital organs are housed in the head, save for a long chain of stomachs, forty in all, leading from its throat to the end of the tail.\"\n—Mulak Ffar, Vastwood Biodiversity').
card_multiverse_id('vastwood gorger'/'M13', '279992').

card_in_set('vedalken entrancer', 'M13').
card_original_type('vedalken entrancer'/'M13', 'Creature — Vedalken Wizard').
card_original_text('vedalken entrancer'/'M13', '{U}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('vedalken entrancer'/'M13', 'vedalken entrancer').
card_uid('vedalken entrancer'/'M13', 'M13:Vedalken Entrancer:vedalken entrancer').
card_rarity('vedalken entrancer'/'M13', 'Common').
card_artist('vedalken entrancer'/'M13', 'Dan Scott').
card_number('vedalken entrancer'/'M13', '76').
card_flavor_text('vedalken entrancer'/'M13', '\"Don\'t get any ideas.\"').
card_multiverse_id('vedalken entrancer'/'M13', '254121').

card_in_set('veilborn ghoul', 'M13').
card_original_type('veilborn ghoul'/'M13', 'Creature — Zombie').
card_original_text('veilborn ghoul'/'M13', 'Veilborn Ghoul can\'t block.\nWhenever a Swamp enters the battlefield under your control, you may return Veilborn Ghoul from your graveyard to your hand.').
card_first_print('veilborn ghoul', 'M13').
card_image_name('veilborn ghoul'/'M13', 'veilborn ghoul').
card_uid('veilborn ghoul'/'M13', 'M13:Veilborn Ghoul:veilborn ghoul').
card_rarity('veilborn ghoul'/'M13', 'Uncommon').
card_artist('veilborn ghoul'/'M13', 'Dan Scott').
card_number('veilborn ghoul'/'M13', '114').
card_flavor_text('veilborn ghoul'/'M13', 'Liliana\'s voice carries far, reaching beyond the cold pall of death.').
card_multiverse_id('veilborn ghoul'/'M13', '259676').

card_in_set('vile rebirth', 'M13').
card_original_type('vile rebirth'/'M13', 'Instant').
card_original_text('vile rebirth'/'M13', 'Exile target creature card from a graveyard. Put a 2/2 black Zombie creature token onto the battlefield.').
card_first_print('vile rebirth', 'M13').
card_image_name('vile rebirth'/'M13', 'vile rebirth').
card_uid('vile rebirth'/'M13', 'M13:Vile Rebirth:vile rebirth').
card_rarity('vile rebirth'/'M13', 'Common').
card_artist('vile rebirth'/'M13', 'Erica Yang').
card_number('vile rebirth'/'M13', '115').
card_flavor_text('vile rebirth'/'M13', '\"‘Here lies our beloved Gartu, the gentlest of all men.\' Ooh, I love irony.\"\n—Jadar, ghoulcaller of Nephalia').
card_multiverse_id('vile rebirth'/'M13', '253703').

card_in_set('void stalker', 'M13').
card_original_type('void stalker'/'M13', 'Creature — Elemental').
card_original_text('void stalker'/'M13', '{2}{U}, {T}: Put Void Stalker and target creature on top of their owners\' libraries, then those players shuffle their libraries.').
card_first_print('void stalker', 'M13').
card_image_name('void stalker'/'M13', 'void stalker').
card_uid('void stalker'/'M13', 'M13:Void Stalker:void stalker').
card_rarity('void stalker'/'M13', 'Rare').
card_artist('void stalker'/'M13', 'Marco Nelor').
card_number('void stalker'/'M13', '77').
card_flavor_text('void stalker'/'M13', 'Now you see it, now you\'re gone.').
card_multiverse_id('void stalker'/'M13', '270997').

card_in_set('volcanic geyser', 'M13').
card_original_type('volcanic geyser'/'M13', 'Instant').
card_original_text('volcanic geyser'/'M13', 'Volcanic Geyser deals X damage to target creature or player.').
card_image_name('volcanic geyser'/'M13', 'volcanic geyser').
card_uid('volcanic geyser'/'M13', 'M13:Volcanic Geyser:volcanic geyser').
card_rarity('volcanic geyser'/'M13', 'Uncommon').
card_artist('volcanic geyser'/'M13', 'Clint Cearley').
card_number('volcanic geyser'/'M13', '154').
card_flavor_text('volcanic geyser'/'M13', '\"The mountain\'s fire surges beneath your feet, demanding release. Can you not feel it?\"').
card_multiverse_id('volcanic geyser'/'M13', '276214').

card_in_set('volcanic strength', 'M13').
card_original_type('volcanic strength'/'M13', 'Enchantment — Aura').
card_original_text('volcanic strength'/'M13', 'Enchant creature\nEnchanted creature gets +2/+2 and has mountainwalk. (It\'s unblockable as long as defending player controls a Mountain.)').
card_image_name('volcanic strength'/'M13', 'volcanic strength').
card_uid('volcanic strength'/'M13', 'M13:Volcanic Strength:volcanic strength').
card_rarity('volcanic strength'/'M13', 'Common').
card_artist('volcanic strength'/'M13', 'Izzy').
card_number('volcanic strength'/'M13', '155').
card_flavor_text('volcanic strength'/'M13', 'The mountains bend to no one.').
card_multiverse_id('volcanic strength'/'M13', '259204').

card_in_set('walking corpse', 'M13').
card_original_type('walking corpse'/'M13', 'Creature — Zombie').
card_original_text('walking corpse'/'M13', '').
card_image_name('walking corpse'/'M13', 'walking corpse').
card_uid('walking corpse'/'M13', 'M13:Walking Corpse:walking corpse').
card_rarity('walking corpse'/'M13', 'Common').
card_artist('walking corpse'/'M13', 'Igor Kieryluk').
card_number('walking corpse'/'M13', '116').
card_flavor_text('walking corpse'/'M13', '\"Feeding a normal army is a problem of logistics. With zombies, it is an asset. Feeding is why they fight. Feeding is why they are feared.\"\n—Jadar, ghoulcaller of Nephalia').
card_multiverse_id('walking corpse'/'M13', '260987').

card_in_set('wall of fire', 'M13').
card_original_type('wall of fire'/'M13', 'Creature — Wall').
card_original_text('wall of fire'/'M13', 'Defender (This creature can\'t attack.)\n{R}: Wall of Fire gets +1/+0 until end of turn.').
card_image_name('wall of fire'/'M13', 'wall of fire').
card_uid('wall of fire'/'M13', 'M13:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'M13', 'Common').
card_artist('wall of fire'/'M13', 'Dan Dos Santos').
card_number('wall of fire'/'M13', '156').
card_flavor_text('wall of fire'/'M13', 'Mercy is for those who keep their distance.').
card_multiverse_id('wall of fire'/'M13', '271370').

card_in_set('war falcon', 'M13').
card_original_type('war falcon'/'M13', 'Creature — Bird').
card_original_text('war falcon'/'M13', 'Flying\nWar Falcon can\'t attack unless you control a Knight or a Soldier.').
card_first_print('war falcon', 'M13').
card_image_name('war falcon'/'M13', 'war falcon').
card_uid('war falcon'/'M13', 'M13:War Falcon:war falcon').
card_rarity('war falcon'/'M13', 'Common').
card_artist('war falcon'/'M13', 'Volkan Baga').
card_number('war falcon'/'M13', '38').
card_flavor_text('war falcon'/'M13', 'The falcon is the picture of nobility, maintaining a regal bearing even as it conducts the bloody business of war.').
card_multiverse_id('war falcon'/'M13', '259697').

card_in_set('war priest of thune', 'M13').
card_original_type('war priest of thune'/'M13', 'Creature — Human Cleric').
card_original_text('war priest of thune'/'M13', 'When War Priest of Thune enters the battlefield, you may destroy target enchantment.').
card_image_name('war priest of thune'/'M13', 'war priest of thune').
card_uid('war priest of thune'/'M13', 'M13:War Priest of Thune:war priest of thune').
card_rarity('war priest of thune'/'M13', 'Uncommon').
card_artist('war priest of thune'/'M13', 'Izzy').
card_number('war priest of thune'/'M13', '39').
card_flavor_text('war priest of thune'/'M13', '\"The sacred might of Thune thunders in my soul.\"').
card_multiverse_id('war priest of thune'/'M13', '275698').

card_in_set('warclamp mastiff', 'M13').
card_original_type('warclamp mastiff'/'M13', 'Creature — Hound').
card_original_text('warclamp mastiff'/'M13', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_first_print('warclamp mastiff', 'M13').
card_image_name('warclamp mastiff'/'M13', 'warclamp mastiff').
card_uid('warclamp mastiff'/'M13', 'M13:Warclamp Mastiff:warclamp mastiff').
card_rarity('warclamp mastiff'/'M13', 'Common').
card_artist('warclamp mastiff'/'M13', 'David Palumbo').
card_number('warclamp mastiff'/'M13', '40').
card_flavor_text('warclamp mastiff'/'M13', '\"One bite from that dog will send you to the healer\'s tent for days.\"\n—Watchmaster Finn of An Karras').
card_multiverse_id('warclamp mastiff'/'M13', '280224').

card_in_set('watercourser', 'M13').
card_original_type('watercourser'/'M13', 'Creature — Elemental').
card_original_text('watercourser'/'M13', '{U}: Watercourser gets +1/-1 until end of turn.').
card_first_print('watercourser', 'M13').
card_image_name('watercourser'/'M13', 'watercourser').
card_uid('watercourser'/'M13', 'M13:Watercourser:watercourser').
card_rarity('watercourser'/'M13', 'Common').
card_artist('watercourser'/'M13', 'Mathias Kollros').
card_number('watercourser'/'M13', '78').
card_flavor_text('watercourser'/'M13', '\"Beware an eddy where there should be none or a stretch that flows too fast or too slow.\"\n—Old Fishbones, Martyne river guide').
card_multiverse_id('watercourser'/'M13', '254109').

card_in_set('welkin tern', 'M13').
card_original_type('welkin tern'/'M13', 'Creature — Bird').
card_original_text('welkin tern'/'M13', 'Flying\nWelkin Tern can block only creatures with flying.').
card_image_name('welkin tern'/'M13', 'welkin tern').
card_uid('welkin tern'/'M13', 'M13:Welkin Tern:welkin tern').
card_rarity('welkin tern'/'M13', 'Common').
card_artist('welkin tern'/'M13', 'Austin Hsu').
card_number('welkin tern'/'M13', '79').
card_flavor_text('welkin tern'/'M13', '\"It preys on other birds. And drakelings. And drakes. Pretty much anything that crosses its flight path is fair game.\"\n—Amira, skycaptain of Thune').
card_multiverse_id('welkin tern'/'M13', '276416').

card_in_set('wild guess', 'M13').
card_original_type('wild guess'/'M13', 'Sorcery').
card_original_text('wild guess'/'M13', 'As an additional cost to cast Wild Guess, discard a card.\nDraw two cards.').
card_first_print('wild guess', 'M13').
card_image_name('wild guess'/'M13', 'wild guess').
card_uid('wild guess'/'M13', 'M13:Wild Guess:wild guess').
card_rarity('wild guess'/'M13', 'Common').
card_artist('wild guess'/'M13', 'Lucas Graciano').
card_number('wild guess'/'M13', '157').
card_flavor_text('wild guess'/'M13', 'No guts, no glory.').
card_multiverse_id('wild guess'/'M13', '253695').

card_in_set('wind drake', 'M13').
card_original_type('wind drake'/'M13', 'Creature — Drake').
card_original_text('wind drake'/'M13', 'Flying').
card_image_name('wind drake'/'M13', 'wind drake').
card_uid('wind drake'/'M13', 'M13:Wind Drake:wind drake').
card_rarity('wind drake'/'M13', 'Common').
card_artist('wind drake'/'M13', 'Steve Prescott').
card_number('wind drake'/'M13', '80').
card_flavor_text('wind drake'/'M13', 'A smaller cousin to the dragon, drakes are often seen at the side of powerful wizards who use them as airborne eyes and ears.').
card_multiverse_id('wind drake'/'M13', '279860').

card_in_set('wit\'s end', 'M13').
card_original_type('wit\'s end'/'M13', 'Sorcery').
card_original_text('wit\'s end'/'M13', 'Target player discards his or her hand.').
card_image_name('wit\'s end'/'M13', 'wit\'s end').
card_uid('wit\'s end'/'M13', 'M13:Wit\'s End:wit\'s end').
card_rarity('wit\'s end'/'M13', 'Rare').
card_artist('wit\'s end'/'M13', 'Chris Rahn').
card_number('wit\'s end'/'M13', '117').
card_flavor_text('wit\'s end'/'M13', '\"Your pathetic ideas lie in shambles, Planeswalker. Where is your arrogant pride now?\"\n—Nicol Bolas').
card_multiverse_id('wit\'s end'/'M13', '278076').

card_in_set('worldfire', 'M13').
card_original_type('worldfire'/'M13', 'Sorcery').
card_original_text('worldfire'/'M13', 'Exile all permanents. Exile all cards from all hands and graveyards. Each player\'s life total becomes 1.').
card_first_print('worldfire', 'M13').
card_image_name('worldfire'/'M13', 'worldfire').
card_uid('worldfire'/'M13', 'M13:Worldfire:worldfire').
card_rarity('worldfire'/'M13', 'Mythic Rare').
card_artist('worldfire'/'M13', 'Izzy').
card_number('worldfire'/'M13', '158').
card_flavor_text('worldfire'/'M13', 'Even the smallest spark may set a world ablaze.').
card_multiverse_id('worldfire'/'M13', '278195').

card_in_set('xathrid gorgon', 'M13').
card_original_type('xathrid gorgon'/'M13', 'Creature — Gorgon').
card_original_text('xathrid gorgon'/'M13', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\n{2}{B}, {T}: Put a petrification counter on target creature. It gains defender and becomes a colorless artifact in addition to its other types. Its activated abilities can\'t be activated. (A creature with defender can\'t attack.)').
card_image_name('xathrid gorgon'/'M13', 'xathrid gorgon').
card_uid('xathrid gorgon'/'M13', 'M13:Xathrid Gorgon:xathrid gorgon').
card_rarity('xathrid gorgon'/'M13', 'Rare').
card_artist('xathrid gorgon'/'M13', 'Chase Stone').
card_number('xathrid gorgon'/'M13', '118').
card_multiverse_id('xathrid gorgon'/'M13', '253728').

card_in_set('yeva\'s forcemage', 'M13').
card_original_type('yeva\'s forcemage'/'M13', 'Creature — Elf Shaman').
card_original_text('yeva\'s forcemage'/'M13', 'When Yeva\'s Forcemage enters the battlefield, target creature gets +2/+2 until end of turn.').
card_first_print('yeva\'s forcemage', 'M13').
card_image_name('yeva\'s forcemage'/'M13', 'yeva\'s forcemage').
card_uid('yeva\'s forcemage'/'M13', 'M13:Yeva\'s Forcemage:yeva\'s forcemage').
card_rarity('yeva\'s forcemage'/'M13', 'Common').
card_artist('yeva\'s forcemage'/'M13', 'Eric Deschamps').
card_number('yeva\'s forcemage'/'M13', '198').
card_flavor_text('yeva\'s forcemage'/'M13', '\"Nature can\'t be stopped. It rips and tears at Ravnica\'s tallest buildings to claim its place in the sun.\"').
card_multiverse_id('yeva\'s forcemage'/'M13', '265840').

card_in_set('yeva, nature\'s herald', 'M13').
card_original_type('yeva, nature\'s herald'/'M13', 'Legendary Creature — Elf Shaman').
card_original_text('yeva, nature\'s herald'/'M13', 'Flash (You may cast this spell any time you could cast an instant.)\nYou may cast green creature cards as though they had flash.').
card_first_print('yeva, nature\'s herald', 'M13').
card_image_name('yeva, nature\'s herald'/'M13', 'yeva, nature\'s herald').
card_uid('yeva, nature\'s herald'/'M13', 'M13:Yeva, Nature\'s Herald:yeva, nature\'s herald').
card_rarity('yeva, nature\'s herald'/'M13', 'Rare').
card_artist('yeva, nature\'s herald'/'M13', 'Eric Deschamps').
card_number('yeva, nature\'s herald'/'M13', '197').
card_flavor_text('yeva, nature\'s herald'/'M13', '\"I can call an army to my side in the blink of an eye. Best not blink.\"').
card_multiverse_id('yeva, nature\'s herald'/'M13', '249680').

card_in_set('zombie goliath', 'M13').
card_original_type('zombie goliath'/'M13', 'Creature — Zombie Giant').
card_original_text('zombie goliath'/'M13', '').
card_image_name('zombie goliath'/'M13', 'zombie goliath').
card_uid('zombie goliath'/'M13', 'M13:Zombie Goliath:zombie goliath').
card_rarity('zombie goliath'/'M13', 'Common').
card_artist('zombie goliath'/'M13', 'E. M. Gist').
card_number('zombie goliath'/'M13', '119').
card_flavor_text('zombie goliath'/'M13', '\"Removing the encumbrance of useless brain tissue grants several advantages: a slight increase in mobility, a response of revulsion and horror from the enemy, and, in the case of large specimens, room for passengers.\"\n—Zul Ashur, lich lord').
card_multiverse_id('zombie goliath'/'M13', '276299').
