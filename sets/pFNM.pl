% Friday Night Magic

set('pFNM').
set_name('pFNM', 'Friday Night Magic').
set_release_date('pFNM', '2000-02-01').
set_border('pFNM', 'black').
set_type('pFNM', 'promo').

card_in_set('abzan beastmaster', 'pFNM').
card_original_type('abzan beastmaster'/'pFNM', 'Creature — Hound Shaman').
card_original_text('abzan beastmaster'/'pFNM', '').
card_first_print('abzan beastmaster', 'pFNM').
card_image_name('abzan beastmaster'/'pFNM', 'abzan beastmaster').
card_uid('abzan beastmaster'/'pFNM', 'pFNM:Abzan Beastmaster:abzan beastmaster').
card_rarity('abzan beastmaster'/'pFNM', 'Special').
card_artist('abzan beastmaster'/'pFNM', 'Scott Murphy').
card_number('abzan beastmaster'/'pFNM', '180').
card_flavor_text('abzan beastmaster'/'pFNM', 'His beasts move the great siege towers of the Abzan across the endless sands.').

card_in_set('accumulated knowledge', 'pFNM').
card_original_type('accumulated knowledge'/'pFNM', 'Instant').
card_original_text('accumulated knowledge'/'pFNM', '').
card_first_print('accumulated knowledge', 'pFNM').
card_image_name('accumulated knowledge'/'pFNM', 'accumulated knowledge').
card_uid('accumulated knowledge'/'pFNM', 'pFNM:Accumulated Knowledge:accumulated knowledge').
card_rarity('accumulated knowledge'/'pFNM', 'Special').
card_artist('accumulated knowledge'/'pFNM', 'Randy Gallegos').
card_number('accumulated knowledge'/'pFNM', '51').
card_flavor_text('accumulated knowledge'/'pFNM', '\"I have seen and heard much here. There are secrets within secrets. Let me show you.\"\n—Takara, to Eladamri').

card_in_set('acidic slime', 'pFNM').
card_original_type('acidic slime'/'pFNM', 'Creature — Ooze').
card_original_text('acidic slime'/'pFNM', '').
card_first_print('acidic slime', 'pFNM').
card_image_name('acidic slime'/'pFNM', 'acidic slime').
card_uid('acidic slime'/'pFNM', 'pFNM:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'pFNM', 'Special').
card_artist('acidic slime'/'pFNM', 'Brad Rigney').
card_number('acidic slime'/'pFNM', '145').

card_in_set('albino troll', 'pFNM').
card_original_type('albino troll'/'pFNM', 'Creature — Troll').
card_original_text('albino troll'/'pFNM', '').
card_image_name('albino troll'/'pFNM', 'albino troll').
card_uid('albino troll'/'pFNM', 'pFNM:Albino Troll:albino troll').
card_rarity('albino troll'/'pFNM', 'Special').
card_artist('albino troll'/'pFNM', 'Paolo Parente').
card_number('albino troll'/'pFNM', '20').

card_in_set('anathemancer', 'pFNM').
card_original_type('anathemancer'/'pFNM', 'Creature — Zombie Wizard').
card_original_text('anathemancer'/'pFNM', '').
card_first_print('anathemancer', 'pFNM').
card_image_name('anathemancer'/'pFNM', 'anathemancer').
card_uid('anathemancer'/'pFNM', 'pFNM:Anathemancer:anathemancer').
card_rarity('anathemancer'/'pFNM', 'Special').
card_artist('anathemancer'/'pFNM', 'Mark Hyzer').
card_number('anathemancer'/'pFNM', '122').

card_in_set('ancient grudge', 'pFNM').
card_original_type('ancient grudge'/'pFNM', 'Instant').
card_original_text('ancient grudge'/'pFNM', '').
card_first_print('ancient grudge', 'pFNM').
card_image_name('ancient grudge'/'pFNM', 'ancient grudge').
card_uid('ancient grudge'/'pFNM', 'pFNM:Ancient Grudge:ancient grudge').
card_rarity('ancient grudge'/'pFNM', 'Special').
card_artist('ancient grudge'/'pFNM', 'Wayne England').
card_number('ancient grudge'/'pFNM', '144').
card_flavor_text('ancient grudge'/'pFNM', 'If there\'s anything a werewolf hates, it\'s a collar—especially Avacyn\'s Collar, the symbol of her church.').

card_in_set('ancient ziggurat', 'pFNM').
card_original_type('ancient ziggurat'/'pFNM', 'Land').
card_original_text('ancient ziggurat'/'pFNM', '').
card_first_print('ancient ziggurat', 'pFNM').
card_image_name('ancient ziggurat'/'pFNM', 'ancient ziggurat').
card_uid('ancient ziggurat'/'pFNM', 'pFNM:Ancient Ziggurat:ancient ziggurat').
card_rarity('ancient ziggurat'/'pFNM', 'Special').
card_artist('ancient ziggurat'/'pFNM', 'Randy Gallegos').
card_number('ancient ziggurat'/'pFNM', '118').
card_flavor_text('ancient ziggurat'/'pFNM', 'Leylines branch out from its foundation like roots from a tree. These draw mana from every region and welcome all living things into its verdant chambers.').

card_in_set('armadillo cloak', 'pFNM').
card_original_type('armadillo cloak'/'pFNM', 'Enchantment — Aura').
card_original_text('armadillo cloak'/'pFNM', '').
card_first_print('armadillo cloak', 'pFNM').
card_image_name('armadillo cloak'/'pFNM', 'armadillo cloak').
card_uid('armadillo cloak'/'pFNM', 'pFNM:Armadillo Cloak:armadillo cloak').
card_rarity('armadillo cloak'/'pFNM', 'Special').
card_artist('armadillo cloak'/'pFNM', 'Paolo Parente').
card_number('armadillo cloak'/'pFNM', '69').
card_flavor_text('armadillo cloak'/'pFNM', '\"Don\'t laugh. It works.\"\n—Yavimaya ranger').

card_in_set('arrogant wurm', 'pFNM').
card_original_type('arrogant wurm'/'pFNM', 'Creature — Wurm').
card_original_text('arrogant wurm'/'pFNM', '').
card_first_print('arrogant wurm', 'pFNM').
card_image_name('arrogant wurm'/'pFNM', 'arrogant wurm').
card_uid('arrogant wurm'/'pFNM', 'pFNM:Arrogant Wurm:arrogant wurm').
card_rarity('arrogant wurm'/'pFNM', 'Special').
card_artist('arrogant wurm'/'pFNM', 'John Avon').
card_number('arrogant wurm'/'pFNM', '77').
card_flavor_text('arrogant wurm'/'pFNM', 'It\'s hard to be humble when the whole world is bite size.').

card_in_set('artisan of kozilek', 'pFNM').
card_original_type('artisan of kozilek'/'pFNM', 'Creature — Eldrazi').
card_original_text('artisan of kozilek'/'pFNM', '').
card_first_print('artisan of kozilek', 'pFNM').
card_image_name('artisan of kozilek'/'pFNM', 'artisan of kozilek').
card_uid('artisan of kozilek'/'pFNM', 'pFNM:Artisan of Kozilek:artisan of kozilek').
card_rarity('artisan of kozilek'/'pFNM', 'Special').
card_artist('artisan of kozilek'/'pFNM', 'Nils Hamm').
card_number('artisan of kozilek'/'pFNM', '131').

card_in_set('astral slide', 'pFNM').
card_original_type('astral slide'/'pFNM', 'Enchantment').
card_original_text('astral slide'/'pFNM', '').
card_first_print('astral slide', 'pFNM').
card_image_name('astral slide'/'pFNM', 'astral slide').
card_uid('astral slide'/'pFNM', 'pFNM:Astral Slide:astral slide').
card_rarity('astral slide'/'pFNM', 'Special').
card_artist('astral slide'/'pFNM', 'Ron Spears').
card_number('astral slide'/'pFNM', '76').
card_flavor_text('astral slide'/'pFNM', '\"The hum of the universe is never off-key.\"\n—Mystic elder').

card_in_set('aura of silence', 'pFNM').
card_original_type('aura of silence'/'pFNM', 'Enchantment').
card_original_text('aura of silence'/'pFNM', '').
card_image_name('aura of silence'/'pFNM', 'aura of silence').
card_uid('aura of silence'/'pFNM', 'pFNM:Aura of Silence:aura of silence').
card_rarity('aura of silence'/'pFNM', 'Special').
card_artist('aura of silence'/'pFNM', 'D. Alexander Gregory').
card_number('aura of silence'/'pFNM', '26').

card_in_set('avacyn\'s pilgrim', 'pFNM').
card_original_type('avacyn\'s pilgrim'/'pFNM', 'Creature — Human Monk').
card_original_text('avacyn\'s pilgrim'/'pFNM', '').
card_first_print('avacyn\'s pilgrim', 'pFNM').
card_image_name('avacyn\'s pilgrim'/'pFNM', 'avacyn\'s pilgrim').
card_uid('avacyn\'s pilgrim'/'pFNM', 'pFNM:Avacyn\'s Pilgrim:avacyn\'s pilgrim').
card_rarity('avacyn\'s pilgrim'/'pFNM', 'Special').
card_artist('avacyn\'s pilgrim'/'pFNM', 'Greg Staples').
card_number('avacyn\'s pilgrim'/'pFNM', '147').
card_flavor_text('avacyn\'s pilgrim'/'pFNM', '\"Avacyn\'s protection is everywhere. From the holy church to the sacred glade, all that we see is under her blessed watch.\"').

card_in_set('avalanche riders', 'pFNM').
card_original_type('avalanche riders'/'pFNM', 'Creature — Human Nomad').
card_original_text('avalanche riders'/'pFNM', '').
card_image_name('avalanche riders'/'pFNM', 'avalanche riders').
card_uid('avalanche riders'/'pFNM', 'pFNM:Avalanche Riders:avalanche riders').
card_rarity('avalanche riders'/'pFNM', 'Special').
card_artist('avalanche riders'/'pFNM', 'Edward P. Beard, Jr.').
card_number('avalanche riders'/'pFNM', '52').

card_in_set('banisher priest', 'pFNM').
card_original_type('banisher priest'/'pFNM', 'Creature — Human Cleric').
card_original_text('banisher priest'/'pFNM', '').
card_first_print('banisher priest', 'pFNM').
card_image_name('banisher priest'/'pFNM', 'banisher priest').
card_uid('banisher priest'/'pFNM', 'pFNM:Banisher Priest:banisher priest').
card_rarity('banisher priest'/'pFNM', 'Special').
card_artist('banisher priest'/'pFNM', 'James Zapata').
card_number('banisher priest'/'pFNM', '166').
card_flavor_text('banisher priest'/'pFNM', '\"Oathbreaker, I cast you out!\"').

card_in_set('banishing light', 'pFNM').
card_original_type('banishing light'/'pFNM', 'Enchantment').
card_original_text('banishing light'/'pFNM', '').
card_first_print('banishing light', 'pFNM').
card_image_name('banishing light'/'pFNM', 'banishing light').
card_uid('banishing light'/'pFNM', 'pFNM:Banishing Light:banishing light').
card_rarity('banishing light'/'pFNM', 'Special').
card_artist('banishing light'/'pFNM', 'Wesley Burt').
card_number('banishing light'/'pFNM', '172').

card_in_set('basking rootwalla', 'pFNM').
card_original_type('basking rootwalla'/'pFNM', 'Creature — Lizard').
card_original_text('basking rootwalla'/'pFNM', '').
card_first_print('basking rootwalla', 'pFNM').
card_image_name('basking rootwalla'/'pFNM', 'basking rootwalla').
card_uid('basking rootwalla'/'pFNM', 'pFNM:Basking Rootwalla:basking rootwalla').
card_rarity('basking rootwalla'/'pFNM', 'Special').
card_artist('basking rootwalla'/'pFNM', 'Heather Hudson').
card_number('basking rootwalla'/'pFNM', '83').

card_in_set('bile blight', 'pFNM').
card_original_type('bile blight'/'pFNM', 'Instant').
card_original_text('bile blight'/'pFNM', '').
card_first_print('bile blight', 'pFNM').
card_image_name('bile blight'/'pFNM', 'bile blight').
card_uid('bile blight'/'pFNM', 'pFNM:Bile Blight:bile blight').
card_rarity('bile blight'/'pFNM', 'Special').
card_artist('bile blight'/'pFNM', 'Nils Hamm').
card_number('bile blight'/'pFNM', '171').
card_flavor_text('bile blight'/'pFNM', 'Not an arrow loosed, javelin thrown, nor sword raised. None were needed.').

card_in_set('black knight', 'pFNM').
card_original_type('black knight'/'pFNM', 'Creature — Human Knight').
card_original_text('black knight'/'pFNM', '').
card_image_name('black knight'/'pFNM', 'black knight').
card_uid('black knight'/'pFNM', 'pFNM:Black Knight:black knight').
card_rarity('black knight'/'pFNM', 'Special').
card_artist('black knight'/'pFNM', 'Jeff A. Menges').
card_number('black knight'/'pFNM', '22').
card_flavor_text('black knight'/'pFNM', '\"The chill, to him who breathed it, drew / Down with his blood, till all his heart was cold.\"\n—Alfred, Lord Tennyson, Idyls of the King').

card_in_set('blastoderm', 'pFNM').
card_original_type('blastoderm'/'pFNM', 'Creature — Beast').
card_original_text('blastoderm'/'pFNM', '').
card_first_print('blastoderm', 'pFNM').
card_image_name('blastoderm'/'pFNM', 'blastoderm').
card_uid('blastoderm'/'pFNM', 'pFNM:Blastoderm:blastoderm').
card_rarity('blastoderm'/'pFNM', 'Special').
card_artist('blastoderm'/'pFNM', 'Eric Peterson').
card_number('blastoderm'/'pFNM', '59').

card_in_set('bloodbraid elf', 'pFNM').
card_original_type('bloodbraid elf'/'pFNM', 'Creature — Elf Berserker').
card_original_text('bloodbraid elf'/'pFNM', '').
card_first_print('bloodbraid elf', 'pFNM').
card_image_name('bloodbraid elf'/'pFNM', 'bloodbraid elf').
card_uid('bloodbraid elf'/'pFNM', 'pFNM:Bloodbraid Elf:bloodbraid elf').
card_rarity('bloodbraid elf'/'pFNM', 'Special').
card_artist('bloodbraid elf'/'pFNM', 'Steve Argyle').
card_number('bloodbraid elf'/'pFNM', '119').

card_in_set('bottle gnomes', 'pFNM').
card_original_type('bottle gnomes'/'pFNM', 'Artifact Creature — Gnome').
card_original_text('bottle gnomes'/'pFNM', '').
card_image_name('bottle gnomes'/'pFNM', 'bottle gnomes').
card_uid('bottle gnomes'/'pFNM', 'pFNM:Bottle Gnomes:bottle gnomes').
card_rarity('bottle gnomes'/'pFNM', 'Special').
card_artist('bottle gnomes'/'pFNM', 'Kaja Foglio').
card_number('bottle gnomes'/'pFNM', '32').
card_flavor_text('bottle gnomes'/'pFNM', '\"I am reminded of the fable of the silver egg. Its owner cracks it open to see what jewels it holds, only to discover a simple yellow yolk.\"\n—Karn, silver golem').

card_in_set('brain maggot', 'pFNM').
card_original_type('brain maggot'/'pFNM', 'Enchantment Creature — Insect').
card_original_text('brain maggot'/'pFNM', '').
card_first_print('brain maggot', 'pFNM').
card_image_name('brain maggot'/'pFNM', 'brain maggot').
card_uid('brain maggot'/'pFNM', 'pFNM:Brain Maggot:brain maggot').
card_rarity('brain maggot'/'pFNM', 'Special').
card_artist('brain maggot'/'pFNM', 'Dave Kendall').
card_number('brain maggot'/'pFNM', '174').

card_in_set('brainstorm', 'pFNM').
card_original_type('brainstorm'/'pFNM', 'Instant').
card_original_text('brainstorm'/'pFNM', '').
card_image_name('brainstorm'/'pFNM', 'brainstorm').
card_uid('brainstorm'/'pFNM', 'pFNM:Brainstorm:brainstorm').
card_rarity('brainstorm'/'pFNM', 'Special').
card_artist('brainstorm'/'pFNM', 'DiTerlizzi').
card_number('brainstorm'/'pFNM', '55').
card_flavor_text('brainstorm'/'pFNM', '\"I see more than other because I know where to look\"\n—Saprazzan vizier').

card_in_set('browbeat', 'pFNM').
card_original_type('browbeat'/'pFNM', 'Sorcery').
card_original_text('browbeat'/'pFNM', '').
card_first_print('browbeat', 'pFNM').
card_image_name('browbeat'/'pFNM', 'browbeat').
card_uid('browbeat'/'pFNM', 'pFNM:Browbeat:browbeat').
card_rarity('browbeat'/'pFNM', 'Special').
card_artist('browbeat'/'pFNM', 'Kieran Yanner').
card_number('browbeat'/'pFNM', '113').
card_flavor_text('browbeat'/'pFNM', '\"Even the threat of power has power.\"\n—Jeska, warrior adept').

card_in_set('cabal coffers', 'pFNM').
card_original_type('cabal coffers'/'pFNM', 'Land').
card_original_text('cabal coffers'/'pFNM', '').
card_first_print('cabal coffers', 'pFNM').
card_image_name('cabal coffers'/'pFNM', 'cabal coffers').
card_uid('cabal coffers'/'pFNM', 'pFNM:Cabal Coffers:cabal coffers').
card_rarity('cabal coffers'/'pFNM', 'Special').
card_artist('cabal coffers'/'pFNM', 'Don Hazeltine').
card_number('cabal coffers'/'pFNM', '89').
card_flavor_text('cabal coffers'/'pFNM', 'Deep within the Cabal\'s vault, the Mirari pulsed like a dead sun-and its darkness radiated across Otaria.').

card_in_set('cabal therapy', 'pFNM').
card_original_type('cabal therapy'/'pFNM', 'Sorcery').
card_original_text('cabal therapy'/'pFNM', '').
card_first_print('cabal therapy', 'pFNM').
card_image_name('cabal therapy'/'pFNM', 'cabal therapy').
card_uid('cabal therapy'/'pFNM', 'pFNM:Cabal Therapy:cabal therapy').
card_rarity('cabal therapy'/'pFNM', 'Special').
card_artist('cabal therapy'/'pFNM', 'Ron Spencer').
card_number('cabal therapy'/'pFNM', '60').

card_in_set('call of the conclave', 'pFNM').
card_original_type('call of the conclave'/'pFNM', 'Sorcery').
card_original_text('call of the conclave'/'pFNM', '').
card_first_print('call of the conclave', 'pFNM').
card_image_name('call of the conclave'/'pFNM', 'call of the conclave').
card_uid('call of the conclave'/'pFNM', 'pFNM:Call of the Conclave:call of the conclave').
card_rarity('call of the conclave'/'pFNM', 'Special').
card_artist('call of the conclave'/'pFNM', 'Anthony S. Waters').
card_number('call of the conclave'/'pFNM', '155').
card_flavor_text('call of the conclave'/'pFNM', 'Centaurs are sent to evangelize in Gruul territories where words of war speak louder than prayers of peace.').

card_in_set('capsize', 'pFNM').
card_original_type('capsize'/'pFNM', 'Instant').
card_original_text('capsize'/'pFNM', '').
card_image_name('capsize'/'pFNM', 'capsize').
card_uid('capsize'/'pFNM', 'pFNM:Capsize:capsize').
card_rarity('capsize'/'pFNM', 'Special').
card_artist('capsize'/'pFNM', 'Tom Wänerstrand').
card_number('capsize'/'pFNM', '35').

card_in_set('carnophage', 'pFNM').
card_original_type('carnophage'/'pFNM', 'Creature — Zombie').
card_original_text('carnophage'/'pFNM', '').
card_image_name('carnophage'/'pFNM', 'carnophage').
card_uid('carnophage'/'pFNM', 'pFNM:Carnophage:carnophage').
card_rarity('carnophage'/'pFNM', 'Special').
card_artist('carnophage'/'pFNM', 'Pete Venters').
card_number('carnophage'/'pFNM', '16').
card_flavor_text('carnophage'/'pFNM', '\"And in their blind and unattaining state their miserable lives have sunk so low that they must envy every other fate.\"\n—Dante, Inferno, trans. Ciardi').

card_in_set('carrion feeder', 'pFNM').
card_original_type('carrion feeder'/'pFNM', 'Creature — Zombie').
card_original_text('carrion feeder'/'pFNM', '').
card_first_print('carrion feeder', 'pFNM').
card_image_name('carrion feeder'/'pFNM', 'carrion feeder').
card_uid('carrion feeder'/'pFNM', 'pFNM:Carrion Feeder:carrion feeder').
card_rarity('carrion feeder'/'pFNM', 'Special').
card_artist('carrion feeder'/'pFNM', 'Brian Snõddy').
card_number('carrion feeder'/'pFNM', '49').
card_flavor_text('carrion feeder'/'pFNM', 'Stinking of rot, it leaps between gravestones in search of its next meal.').

card_in_set('chainer\'s edict', 'pFNM').
card_original_type('chainer\'s edict'/'pFNM', 'Sorcery').
card_original_text('chainer\'s edict'/'pFNM', '').
card_first_print('chainer\'s edict', 'pFNM').
card_image_name('chainer\'s edict'/'pFNM', 'chainer\'s edict').
card_uid('chainer\'s edict'/'pFNM', 'pFNM:Chainer\'s Edict:chainer\'s edict').
card_rarity('chainer\'s edict'/'pFNM', 'Special').
card_artist('chainer\'s edict'/'pFNM', 'Ben Thompson').
card_number('chainer\'s edict'/'pFNM', '74').
card_flavor_text('chainer\'s edict'/'pFNM', 'The pits have their own form of mercy.').

card_in_set('circle of protection: red', 'pFNM').
card_original_type('circle of protection: red'/'pFNM', 'Enchantment').
card_original_text('circle of protection: red'/'pFNM', '').
card_image_name('circle of protection: red'/'pFNM', 'circle of protection red').
card_uid('circle of protection: red'/'pFNM', 'pFNM:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'pFNM', 'Special').
card_artist('circle of protection: red'/'pFNM', 'Mark Tedin').
card_number('circle of protection: red'/'pFNM', '63').

card_in_set('circular logic', 'pFNM').
card_original_type('circular logic'/'pFNM', 'Instant').
card_original_text('circular logic'/'pFNM', '').
card_first_print('circular logic', 'pFNM').
card_image_name('circular logic'/'pFNM', 'circular logic').
card_uid('circular logic'/'pFNM', 'pFNM:Circular Logic:circular logic').
card_rarity('circular logic'/'pFNM', 'Special').
card_artist('circular logic'/'pFNM', 'Anthony S. Waters').
card_number('circular logic'/'pFNM', '75').

card_in_set('cloudpost', 'pFNM').
card_original_type('cloudpost'/'pFNM', 'Land — Locus').
card_original_text('cloudpost'/'pFNM', '').
card_first_print('cloudpost', 'pFNM').
card_image_name('cloudpost'/'pFNM', 'cloudpost').
card_uid('cloudpost'/'pFNM', 'pFNM:Cloudpost:cloudpost').
card_rarity('cloudpost'/'pFNM', 'Special').
card_artist('cloudpost'/'pFNM', 'Jim Nelson').
card_number('cloudpost'/'pFNM', '120').
card_flavor_text('cloudpost'/'pFNM', '\"He watches from above. He watches from below. He watches from within.\" -Inscription on Tel-Jilad, the Tree of Tales').

card_in_set('contagion clasp', 'pFNM').
card_original_type('contagion clasp'/'pFNM', 'Artifact').
card_original_text('contagion clasp'/'pFNM', '').
card_first_print('contagion clasp', 'pFNM').
card_image_name('contagion clasp'/'pFNM', 'contagion clasp').
card_uid('contagion clasp'/'pFNM', 'pFNM:Contagion Clasp:contagion clasp').
card_rarity('contagion clasp'/'pFNM', 'Special').
card_artist('contagion clasp'/'pFNM', 'Franz Vohwinkel').
card_number('contagion clasp'/'pFNM', '137').

card_in_set('counterspell', 'pFNM').
card_original_type('counterspell'/'pFNM', 'Instant').
card_original_text('counterspell'/'pFNM', '').
card_image_name('counterspell'/'pFNM', 'counterspell').
card_uid('counterspell'/'pFNM', 'pFNM:Counterspell:counterspell').
card_rarity('counterspell'/'pFNM', 'Special').
card_artist('counterspell'/'pFNM', 'Mark Poole').
card_number('counterspell'/'pFNM', '66').
card_flavor_text('counterspell'/'pFNM', '\"It was probably a lousy spell in the first place.\"\n—Ertai, wizard adept').

card_in_set('crystalline sliver', 'pFNM').
card_original_type('crystalline sliver'/'pFNM', 'Creature — Sliver').
card_original_text('crystalline sliver'/'pFNM', '').
card_image_name('crystalline sliver'/'pFNM', 'crystalline sliver').
card_uid('crystalline sliver'/'pFNM', 'pFNM:Crystalline Sliver:crystalline sliver').
card_rarity('crystalline sliver'/'pFNM', 'Special').
card_artist('crystalline sliver'/'pFNM', 'L. A. Williams').
card_number('crystalline sliver'/'pFNM', '34').
card_flavor_text('crystalline sliver'/'pFNM', 'Bred as living shields, these slivers have proven unruly-they know they cannot be caught.').

card_in_set('cultivate', 'pFNM').
card_original_type('cultivate'/'pFNM', 'Sorcery').
card_original_text('cultivate'/'pFNM', '').
card_first_print('cultivate', 'pFNM').
card_image_name('cultivate'/'pFNM', 'cultivate').
card_uid('cultivate'/'pFNM', 'pFNM:Cultivate:cultivate').
card_rarity('cultivate'/'pFNM', 'Special').
card_artist('cultivate'/'pFNM', 'John Avon').
card_number('cultivate'/'pFNM', '135').
card_flavor_text('cultivate'/'pFNM', 'All seeds share a common bond, calling to each other across infinity.').

card_in_set('death', 'pFNM').
card_original_type('death'/'pFNM', 'Sorcery').
card_original_text('death'/'pFNM', '').
card_first_print('death', 'pFNM').
card_image_name('death'/'pFNM', 'lifedeath').
card_uid('death'/'pFNM', 'pFNM:Death:lifedeath').
card_rarity('death'/'pFNM', 'Special').
card_artist('death'/'pFNM', 'Edward P. Beard, Jr. & Anthony S. Waters').
card_number('death'/'pFNM', '78b').

card_in_set('deep analysis', 'pFNM').
card_original_type('deep analysis'/'pFNM', 'Sorcery').
card_original_text('deep analysis'/'pFNM', '').
card_first_print('deep analysis', 'pFNM').
card_image_name('deep analysis'/'pFNM', 'deep analysis').
card_uid('deep analysis'/'pFNM', 'pFNM:Deep Analysis:deep analysis').
card_rarity('deep analysis'/'pFNM', 'Special').
card_artist('deep analysis'/'pFNM', 'Daren Bader').
card_number('deep analysis'/'pFNM', '81').
card_flavor_text('deep analysis'/'pFNM', '\"The specimen seems to be broken.\"').

card_in_set('desert', 'pFNM').
card_original_type('desert'/'pFNM', 'Land — Desert').
card_original_text('desert'/'pFNM', '').
card_image_name('desert'/'pFNM', 'desert').
card_uid('desert'/'pFNM', 'pFNM:Desert:desert').
card_rarity('desert'/'pFNM', 'Special').
card_artist('desert'/'pFNM', 'Glen Angus').
card_number('desert'/'pFNM', '99').

card_in_set('despise', 'pFNM').
card_original_type('despise'/'pFNM', 'Sorcery').
card_original_text('despise'/'pFNM', '').
card_first_print('despise', 'pFNM').
card_image_name('despise'/'pFNM', 'despise').
card_uid('despise'/'pFNM', 'pFNM:Despise:despise').
card_rarity('despise'/'pFNM', 'Special').
card_artist('despise'/'pFNM', 'Raymond Swanland').
card_number('despise'/'pFNM', '141').
card_flavor_text('despise'/'pFNM', '\"Truth is always a weapon in your enemies\' hands.\"\n—Geth, Lord of the Vault').

card_in_set('dimir charm', 'pFNM').
card_original_type('dimir charm'/'pFNM', 'Instant').
card_original_text('dimir charm'/'pFNM', '').
card_first_print('dimir charm', 'pFNM').
card_image_name('dimir charm'/'pFNM', 'dimir charm').
card_uid('dimir charm'/'pFNM', 'pFNM:Dimir Charm:dimir charm').
card_rarity('dimir charm'/'pFNM', 'Special').
card_artist('dimir charm'/'pFNM', 'Zoltan Boros').
card_number('dimir charm'/'pFNM', '159').
card_flavor_text('dimir charm'/'pFNM', '\"Dangerous to recognize. Deadly not to.\"\n—Lazav').

card_in_set('disdainful stroke', 'pFNM').
card_original_type('disdainful stroke'/'pFNM', 'Instant').
card_original_text('disdainful stroke'/'pFNM', '').
card_first_print('disdainful stroke', 'pFNM').
card_image_name('disdainful stroke'/'pFNM', 'disdainful stroke').
card_uid('disdainful stroke'/'pFNM', 'pFNM:Disdainful Stroke:disdainful stroke').
card_rarity('disdainful stroke'/'pFNM', 'Special').
card_artist('disdainful stroke'/'pFNM', 'Svetlin Velinov').
card_number('disdainful stroke'/'pFNM', '177').
card_flavor_text('disdainful stroke'/'pFNM', '\"You are beneath contempt. Your lineage will be forgotten.\"').

card_in_set('disenchant', 'pFNM').
card_original_type('disenchant'/'pFNM', 'Instant').
card_original_text('disenchant'/'pFNM', '').
card_image_name('disenchant'/'pFNM', 'disenchant').
card_uid('disenchant'/'pFNM', 'pFNM:Disenchant:disenchant').
card_rarity('disenchant'/'pFNM', 'Special').
card_artist('disenchant'/'pFNM', 'Kevin McCann').
card_number('disenchant'/'pFNM', '31').

card_in_set('dismember', 'pFNM').
card_original_type('dismember'/'pFNM', 'Instant').
card_original_text('dismember'/'pFNM', '').
card_first_print('dismember', 'pFNM').
card_image_name('dismember'/'pFNM', 'dismember').
card_uid('dismember'/'pFNM', 'pFNM:Dismember:dismember').
card_rarity('dismember'/'pFNM', 'Special').
card_artist('dismember'/'pFNM', 'Jason Felix').
card_number('dismember'/'pFNM', '143').
card_flavor_text('dismember'/'pFNM', '\"You serve Phyrexia. Your pieces would better serve Phyrexia elsewhere.\"\n—Azax-Azog, the Demon Thane').

card_in_set('dissipate', 'pFNM').
card_original_type('dissipate'/'pFNM', 'Instant').
card_original_text('dissipate'/'pFNM', '').
card_image_name('dissipate'/'pFNM', 'dissipate').
card_uid('dissipate'/'pFNM', 'pFNM:Dissipate:dissipate').
card_rarity('dissipate'/'pFNM', 'Special').
card_artist('dissipate'/'pFNM', 'Richard Kane Ferguson').
card_number('dissipate'/'pFNM', '21').

card_in_set('dissolve', 'pFNM').
card_original_type('dissolve'/'pFNM', 'Instant').
card_original_text('dissolve'/'pFNM', '').
card_first_print('dissolve', 'pFNM').
card_image_name('dissolve'/'pFNM', 'dissolve').
card_uid('dissolve'/'pFNM', 'pFNM:Dissolve:dissolve').
card_rarity('dissolve'/'pFNM', 'Special').
card_artist('dissolve'/'pFNM', 'Jeff Simpson').
card_number('dissolve'/'pFNM', '169').
card_flavor_text('dissolve'/'pFNM', '\"You thought only the gods could stop you?\"').

card_in_set('drain life', 'pFNM').
card_original_type('drain life'/'pFNM', 'Sorcery').
card_original_text('drain life'/'pFNM', '').
card_image_name('drain life'/'pFNM', 'drain life').
card_uid('drain life'/'pFNM', 'pFNM:Drain Life:drain life').
card_rarity('drain life'/'pFNM', 'Special').
card_artist('drain life'/'pFNM', 'Douglas Shuler').
card_number('drain life'/'pFNM', '25').

card_in_set('duress', 'pFNM').
card_original_type('duress'/'pFNM', 'Sorcery').
card_original_text('duress'/'pFNM', '').
card_image_name('duress'/'pFNM', 'duress').
card_uid('duress'/'pFNM', 'pFNM:Duress:duress').
card_rarity('duress'/'pFNM', 'Special').
card_artist('duress'/'pFNM', 'Lawrence Snelly').
card_number('duress'/'pFNM', '65').
card_flavor_text('duress'/'pFNM', '\"We decide who is worthy of our works.\"\n—Gix, Yawgmoth praetor').

card_in_set('elves of deep shadow', 'pFNM').
card_original_type('elves of deep shadow'/'pFNM', 'Creature — Elf Druid').
card_original_text('elves of deep shadow'/'pFNM', '').
card_image_name('elves of deep shadow'/'pFNM', 'elves of deep shadow').
card_uid('elves of deep shadow'/'pFNM', 'pFNM:Elves of Deep Shadow:elves of deep shadow').
card_rarity('elves of deep shadow'/'pFNM', 'Special').
card_artist('elves of deep shadow'/'pFNM', 'Jesper Myrfors').
card_number('elves of deep shadow'/'pFNM', '68').
card_flavor_text('elves of deep shadow'/'pFNM', '\"They are aberrations who have turned on everything we hold sacred. Let them be cast out.\"\n—Ailheen, Speaker of the Council').

card_in_set('elvish mystic', 'pFNM').
card_original_type('elvish mystic'/'pFNM', 'Creature — Elf Druid').
card_original_text('elvish mystic'/'pFNM', '').
card_first_print('elvish mystic', 'pFNM').
card_image_name('elvish mystic'/'pFNM', 'elvish mystic').
card_uid('elvish mystic'/'pFNM', 'pFNM:Elvish Mystic:elvish mystic').
card_rarity('elvish mystic'/'pFNM', 'Special').
card_artist('elvish mystic'/'pFNM', 'Winona Nelson').
card_number('elvish mystic'/'pFNM', '165').
card_flavor_text('elvish mystic'/'pFNM', '\"Life grows everywhere. My kin merely find those places where it grows strongest.\"\n—Nissa Revane').

card_in_set('elvish visionary', 'pFNM').
card_original_type('elvish visionary'/'pFNM', 'Creature — Elf Shaman').
card_original_text('elvish visionary'/'pFNM', '').
card_first_print('elvish visionary', 'pFNM').
card_image_name('elvish visionary'/'pFNM', 'elvish visionary').
card_uid('elvish visionary'/'pFNM', 'pFNM:Elvish Visionary:elvish visionary').
card_rarity('elvish visionary'/'pFNM', 'Special').
card_artist('elvish visionary'/'pFNM', 'Lucio Parrillo').
card_number('elvish visionary'/'pFNM', '121').
card_flavor_text('elvish visionary'/'pFNM', 'Before any major undertaking, a Cylian elf seeks the guidance of a visionary to learn the will of the gargantuan ancients.').

card_in_set('encroaching wastes', 'pFNM').
card_original_type('encroaching wastes'/'pFNM', 'Land').
card_original_text('encroaching wastes'/'pFNM', '').
card_first_print('encroaching wastes', 'pFNM').
card_image_name('encroaching wastes'/'pFNM', 'encroaching wastes').
card_uid('encroaching wastes'/'pFNM', 'pFNM:Encroaching Wastes:encroaching wastes').
card_rarity('encroaching wastes'/'pFNM', 'Special').
card_artist('encroaching wastes'/'pFNM', 'Richard Wright').
card_number('encroaching wastes'/'pFNM', '167').
card_flavor_text('encroaching wastes'/'pFNM', 'Every world is a work in progress, constantly reshaped by time, disasters, and even the powerful magic of Planeswalkers.').

card_in_set('engineered plague', 'pFNM').
card_original_type('engineered plague'/'pFNM', 'Enchantment').
card_original_text('engineered plague'/'pFNM', '').
card_image_name('engineered plague'/'pFNM', 'engineered plague').
card_uid('engineered plague'/'pFNM', 'pFNM:Engineered Plague:engineered plague').
card_rarity('engineered plague'/'pFNM', 'Special').
card_artist('engineered plague'/'pFNM', 'Michael Sutfin').
card_number('engineered plague'/'pFNM', '86').
card_flavor_text('engineered plague'/'pFNM', '\"The admixture of bitterwort in the viral brew has produced most favorable results.\"\n—Phyrexian progress notes').

card_in_set('eternal witness', 'pFNM').
card_original_type('eternal witness'/'pFNM', 'Creature — Human Shaman').
card_original_text('eternal witness'/'pFNM', '').
card_first_print('eternal witness', 'pFNM').
card_image_name('eternal witness'/'pFNM', 'eternal witness').
card_uid('eternal witness'/'pFNM', 'pFNM:Eternal Witness:eternal witness').
card_rarity('eternal witness'/'pFNM', 'Special').
card_artist('eternal witness'/'pFNM', 'Terese Nielsen').
card_number('eternal witness'/'pFNM', '94').
card_flavor_text('eternal witness'/'pFNM', 'Gaea needs no book to record every act.').

card_in_set('everflowing chalice', 'pFNM').
card_original_type('everflowing chalice'/'pFNM', 'Artifact').
card_original_text('everflowing chalice'/'pFNM', '').
card_first_print('everflowing chalice', 'pFNM').
card_image_name('everflowing chalice'/'pFNM', 'everflowing chalice').
card_uid('everflowing chalice'/'pFNM', 'pFNM:Everflowing Chalice:everflowing chalice').
card_rarity('everflowing chalice'/'pFNM', 'Special').
card_artist('everflowing chalice'/'pFNM', 'Charles Urbach').
card_number('everflowing chalice'/'pFNM', '128').

card_in_set('evolving wilds', 'pFNM').
card_original_type('evolving wilds'/'pFNM', 'Land').
card_original_text('evolving wilds'/'pFNM', '').
card_image_name('evolving wilds'/'pFNM', 'evolving wilds').
card_uid('evolving wilds'/'pFNM', 'pFNM:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'pFNM', 'Special').
card_artist('evolving wilds'/'pFNM', 'Sam Burley').
card_number('evolving wilds'/'pFNM', '149').
card_flavor_text('evolving wilds'/'pFNM', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').

card_in_set('experiment one', 'pFNM').
card_original_type('experiment one'/'pFNM', 'Creature — Human Ooze').
card_original_text('experiment one'/'pFNM', '').
card_first_print('experiment one', 'pFNM').
card_image_name('experiment one'/'pFNM', 'experiment one').
card_uid('experiment one'/'pFNM', 'pFNM:Experiment One:experiment one').
card_rarity('experiment one'/'pFNM', 'Special').
card_artist('experiment one'/'pFNM', 'Jack Wang').
card_number('experiment one'/'pFNM', '160').

card_in_set('fact or fiction', 'pFNM').
card_original_type('fact or fiction'/'pFNM', 'Instant').
card_original_text('fact or fiction'/'pFNM', '').
card_first_print('fact or fiction', 'pFNM').
card_image_name('fact or fiction'/'pFNM', 'fact or fiction').
card_uid('fact or fiction'/'pFNM', 'pFNM:Fact or Fiction:fact or fiction').
card_rarity('fact or fiction'/'pFNM', 'Special').
card_artist('fact or fiction'/'pFNM', 'Terese Nielsen').
card_number('fact or fiction'/'pFNM', '61').

card_in_set('fanatic of xenagos', 'pFNM').
card_original_type('fanatic of xenagos'/'pFNM', 'Creature — Centaur Warrior').
card_original_text('fanatic of xenagos'/'pFNM', '').
card_first_print('fanatic of xenagos', 'pFNM').
card_image_name('fanatic of xenagos'/'pFNM', 'fanatic of xenagos').
card_uid('fanatic of xenagos'/'pFNM', 'pFNM:Fanatic of Xenagos:fanatic of xenagos').
card_rarity('fanatic of xenagos'/'pFNM', 'Special').
card_artist('fanatic of xenagos'/'pFNM', 'Zoltan Boros').
card_number('fanatic of xenagos'/'pFNM', '173').

card_in_set('farseek', 'pFNM').
card_original_type('farseek'/'pFNM', 'Sorcery').
card_original_text('farseek'/'pFNM', '').
card_first_print('farseek', 'pFNM').
card_image_name('farseek'/'pFNM', 'farseek').
card_uid('farseek'/'pFNM', 'pFNM:Farseek:farseek').
card_rarity('farseek'/'pFNM', 'Special').
card_artist('farseek'/'pFNM', 'Michael Komarck').
card_number('farseek'/'pFNM', '154').
card_flavor_text('farseek'/'pFNM', '\"How truly vast this city must be, that I have traveled so far and seen so much, yet never once found the place where the buildings fail.\"').

card_in_set('fire', 'pFNM').
card_original_type('fire'/'pFNM', 'Instant').
card_original_text('fire'/'pFNM', '').
card_first_print('fire', 'pFNM').
card_image_name('fire'/'pFNM', 'fireice').
card_uid('fire'/'pFNM', 'pFNM:Fire:fireice').
card_rarity('fire'/'pFNM', 'Special').
card_artist('fire'/'pFNM', 'Franz Vohwinkel & David Martin').
card_number('fire'/'pFNM', '79a').

card_in_set('fireblast', 'pFNM').
card_original_type('fireblast'/'pFNM', 'Instant').
card_original_text('fireblast'/'pFNM', '').
card_image_name('fireblast'/'pFNM', 'fireblast').
card_uid('fireblast'/'pFNM', 'pFNM:Fireblast:fireblast').
card_rarity('fireblast'/'pFNM', 'Special').
card_artist('fireblast'/'pFNM', 'Michael Danza').
card_number('fireblast'/'pFNM', '18').

card_in_set('firebolt', 'pFNM').
card_original_type('firebolt'/'pFNM', 'Sorcery').
card_original_text('firebolt'/'pFNM', '').
card_first_print('firebolt', 'pFNM').
card_image_name('firebolt'/'pFNM', 'firebolt').
card_uid('firebolt'/'pFNM', 'pFNM:Firebolt:firebolt').
card_rarity('firebolt'/'pFNM', 'Special').
card_artist('firebolt'/'pFNM', 'Ron Spencer').
card_number('firebolt'/'pFNM', '80').
card_flavor_text('firebolt'/'pFNM', 'Reach out and torch someone.').

card_in_set('fireslinger', 'pFNM').
card_original_type('fireslinger'/'pFNM', 'Creature — Human Wizard').
card_original_text('fireslinger'/'pFNM', '').
card_image_name('fireslinger'/'pFNM', 'fireslinger').
card_uid('fireslinger'/'pFNM', 'pFNM:Fireslinger:fireslinger').
card_rarity('fireslinger'/'pFNM', 'Special').
card_artist('fireslinger'/'pFNM', 'Jeff Reitz').
card_number('fireslinger'/'pFNM', '24').
card_flavor_text('fireslinger'/'pFNM', '\"One pain is lessened by another\'s anguish.\"\n—William Shakespeare, Romeo and Juliet').

card_in_set('flametongue kavu', 'pFNM').
card_original_type('flametongue kavu'/'pFNM', 'Creature — Kavu').
card_original_text('flametongue kavu'/'pFNM', '').
card_first_print('flametongue kavu', 'pFNM').
card_image_name('flametongue kavu'/'pFNM', 'flametongue kavu').
card_uid('flametongue kavu'/'pFNM', 'pFNM:Flametongue Kavu:flametongue kavu').
card_rarity('flametongue kavu'/'pFNM', 'Special').
card_artist('flametongue kavu'/'pFNM', 'Pete Venters').
card_number('flametongue kavu'/'pFNM', '58').
card_flavor_text('flametongue kavu'/'pFNM', '\"For dim-witted, thick-skulled genetic mutants, they have pretty good aim.\"\n—Sisay ').

card_in_set('forbid', 'pFNM').
card_original_type('forbid'/'pFNM', 'Instant').
card_original_text('forbid'/'pFNM', '').
card_image_name('forbid'/'pFNM', 'forbid').
card_uid('forbid'/'pFNM', 'pFNM:Forbid:forbid').
card_rarity('forbid'/'pFNM', 'Special').
card_artist('forbid'/'pFNM', 'Scott Kirschner').
card_number('forbid'/'pFNM', '27').

card_in_set('forbidden alchemy', 'pFNM').
card_original_type('forbidden alchemy'/'pFNM', 'Instant').
card_original_text('forbidden alchemy'/'pFNM', '').
card_first_print('forbidden alchemy', 'pFNM').
card_image_name('forbidden alchemy'/'pFNM', 'forbidden alchemy').
card_uid('forbidden alchemy'/'pFNM', 'pFNM:Forbidden Alchemy:forbidden alchemy').
card_rarity('forbidden alchemy'/'pFNM', 'Special').
card_artist('forbidden alchemy'/'pFNM', 'Randy Gallegos').
card_number('forbidden alchemy'/'pFNM', '146').

card_in_set('force spike', 'pFNM').
card_original_type('force spike'/'pFNM', 'Instant').
card_original_text('force spike'/'pFNM', '').
card_image_name('force spike'/'pFNM', 'force spike').
card_uid('force spike'/'pFNM', 'pFNM:Force Spike:force spike').
card_rarity('force spike'/'pFNM', 'Special').
card_artist('force spike'/'pFNM', 'Bryon Wackwitz').
card_number('force spike'/'pFNM', '91').

card_in_set('frenzied goblin', 'pFNM').
card_original_type('frenzied goblin'/'pFNM', 'Creature — Goblin Berserker').
card_original_text('frenzied goblin'/'pFNM', '').
card_first_print('frenzied goblin', 'pFNM').
card_image_name('frenzied goblin'/'pFNM', 'frenzied goblin').
card_uid('frenzied goblin'/'pFNM', 'pFNM:Frenzied Goblin:frenzied goblin').
card_rarity('frenzied goblin'/'pFNM', 'Special').
card_artist('frenzied goblin'/'pFNM', 'Kev Walker').
card_number('frenzied goblin'/'pFNM', '176').
card_flavor_text('frenzied goblin'/'pFNM', 'The upside to not thinking about the consequences is that you\'ll always surprise those who do.').

card_in_set('gatekeeper of malakir', 'pFNM').
card_original_type('gatekeeper of malakir'/'pFNM', 'Creature — Vampire Warrior').
card_original_text('gatekeeper of malakir'/'pFNM', '').
card_first_print('gatekeeper of malakir', 'pFNM').
card_image_name('gatekeeper of malakir'/'pFNM', 'gatekeeper of malakir').
card_uid('gatekeeper of malakir'/'pFNM', 'pFNM:Gatekeeper of Malakir:gatekeeper of malakir').
card_rarity('gatekeeper of malakir'/'pFNM', 'Special').
card_artist('gatekeeper of malakir'/'pFNM', 'Jason Felix').
card_number('gatekeeper of malakir'/'pFNM', '126').
card_flavor_text('gatekeeper of malakir'/'pFNM', '\"You may enter the city—once the toll is paid.\"').

card_in_set('gerrard\'s verdict', 'pFNM').
card_original_type('gerrard\'s verdict'/'pFNM', 'Sorcery').
card_original_text('gerrard\'s verdict'/'pFNM', '').
card_first_print('gerrard\'s verdict', 'pFNM').
card_image_name('gerrard\'s verdict'/'pFNM', 'gerrard\'s verdict').
card_uid('gerrard\'s verdict'/'pFNM', 'pFNM:Gerrard\'s Verdict:gerrard\'s verdict').
card_rarity('gerrard\'s verdict'/'pFNM', 'Special').
card_artist('gerrard\'s verdict'/'pFNM', 'Carl Critchlow').
card_number('gerrard\'s verdict'/'pFNM', '82').
card_flavor_text('gerrard\'s verdict'/'pFNM', 'A heart of light and a soul of darkness cannot coexist.').

card_in_set('ghor-clan rampager', 'pFNM').
card_original_type('ghor-clan rampager'/'pFNM', 'Creature — Beast').
card_original_text('ghor-clan rampager'/'pFNM', '').
card_first_print('ghor-clan rampager', 'pFNM').
card_image_name('ghor-clan rampager'/'pFNM', 'ghor-clan rampager').
card_uid('ghor-clan rampager'/'pFNM', 'pFNM:Ghor-Clan Rampager:ghor-clan rampager').
card_rarity('ghor-clan rampager'/'pFNM', 'Special').
card_artist('ghor-clan rampager'/'pFNM', 'Dave Kendall').
card_number('ghor-clan rampager'/'pFNM', '161').
card_flavor_text('ghor-clan rampager'/'pFNM', '\"The Simic come from the cold slow depths. How could they understand the fire in a wild heart?\"\n—Kroshkar, Gruul shaman').

card_in_set('ghostly prison', 'pFNM').
card_original_type('ghostly prison'/'pFNM', 'Enchantment').
card_original_text('ghostly prison'/'pFNM', '').
card_first_print('ghostly prison', 'pFNM').
card_image_name('ghostly prison'/'pFNM', 'ghostly prison').
card_uid('ghostly prison'/'pFNM', 'pFNM:Ghostly Prison:ghostly prison').
card_rarity('ghostly prison'/'pFNM', 'Special').
card_artist('ghostly prison'/'pFNM', 'Wayne England').
card_number('ghostly prison'/'pFNM', '117').
card_flavor_text('ghostly prison'/'pFNM', '\"May the memory of our fallen heroes ensnare the violent hearts of lesser men.\"\n—Great Threshold of Monserkat inscription').

card_in_set('giant growth', 'pFNM').
card_original_type('giant growth'/'pFNM', 'Instant').
card_original_text('giant growth'/'pFNM', '').
card_image_name('giant growth'/'pFNM', 'giant growth').
card_uid('giant growth'/'pFNM', 'pFNM:Giant Growth:giant growth').
card_rarity('giant growth'/'pFNM', 'Special').
card_artist('giant growth'/'pFNM', 'Sandra Everingham').
card_number('giant growth'/'pFNM', '8').

card_in_set('gitaxian probe', 'pFNM').
card_original_type('gitaxian probe'/'pFNM', 'Sorcery').
card_original_text('gitaxian probe'/'pFNM', '').
card_first_print('gitaxian probe', 'pFNM').
card_image_name('gitaxian probe'/'pFNM', 'gitaxian probe').
card_uid('gitaxian probe'/'pFNM', 'pFNM:Gitaxian Probe:gitaxian probe').
card_rarity('gitaxian probe'/'pFNM', 'Special').
card_artist('gitaxian probe'/'pFNM', 'Trevor Claxton').
card_number('gitaxian probe'/'pFNM', '151').
card_flavor_text('gitaxian probe'/'pFNM', '\"My flesh holds no secrets, monster. The spirit of Mirrodin will fight on.\"\n—Vy Covalt, Mirran resistance').

card_in_set('glistener elf', 'pFNM').
card_original_type('glistener elf'/'pFNM', 'Creature — Elf Warrior').
card_original_text('glistener elf'/'pFNM', '').
card_first_print('glistener elf', 'pFNM').
card_image_name('glistener elf'/'pFNM', 'glistener elf').
card_uid('glistener elf'/'pFNM', 'pFNM:Glistener Elf:glistener elf').
card_rarity('glistener elf'/'pFNM', 'Special').
card_artist('glistener elf'/'pFNM', 'Mark Zug').
card_number('glistener elf'/'pFNM', '140').
card_flavor_text('glistener elf'/'pFNM', '\"Beg me for life, and I will fill you with the glory of Phyrexian perfection.\"').

card_in_set('go for the throat', 'pFNM').
card_original_type('go for the throat'/'pFNM', 'Instant').
card_original_text('go for the throat'/'pFNM', '').
card_first_print('go for the throat', 'pFNM').
card_image_name('go for the throat'/'pFNM', 'go for the throat').
card_uid('go for the throat'/'pFNM', 'pFNM:Go for the Throat:go for the throat').
card_rarity('go for the throat'/'pFNM', 'Special').
card_artist('go for the throat'/'pFNM', 'David Rapoza').
card_number('go for the throat'/'pFNM', '138').
card_flavor_text('go for the throat'/'pFNM', 'Having flesh is increasingly a liability on Mirrodin.').

card_in_set('goblin bombardment', 'pFNM').
card_original_type('goblin bombardment'/'pFNM', 'Enchantment').
card_original_text('goblin bombardment'/'pFNM', '').
card_image_name('goblin bombardment'/'pFNM', 'goblin bombardment').
card_uid('goblin bombardment'/'pFNM', 'pFNM:Goblin Bombardment:goblin bombardment').
card_rarity('goblin bombardment'/'pFNM', 'Special').
card_artist('goblin bombardment'/'pFNM', 'Brian Snõddy').
card_number('goblin bombardment'/'pFNM', '37').
card_flavor_text('goblin bombardment'/'pFNM', 'One mogg to aim the catapult, one mogg to steer the rock.').

card_in_set('goblin legionnaire', 'pFNM').
card_original_type('goblin legionnaire'/'pFNM', 'Creature — Goblin Soldier').
card_original_text('goblin legionnaire'/'pFNM', '').
card_first_print('goblin legionnaire', 'pFNM').
card_image_name('goblin legionnaire'/'pFNM', 'goblin legionnaire').
card_uid('goblin legionnaire'/'pFNM', 'pFNM:Goblin Legionnaire:goblin legionnaire').
card_rarity('goblin legionnaire'/'pFNM', 'Special').
card_artist('goblin legionnaire'/'pFNM', 'Mark Romanoski').
card_number('goblin legionnaire'/'pFNM', '85').

card_in_set('goblin ringleader', 'pFNM').
card_original_type('goblin ringleader'/'pFNM', 'Creature — Goblin').
card_original_text('goblin ringleader'/'pFNM', '').
card_first_print('goblin ringleader', 'pFNM').
card_image_name('goblin ringleader'/'pFNM', 'goblin ringleader').
card_uid('goblin ringleader'/'pFNM', 'pFNM:Goblin Ringleader:goblin ringleader').
card_rarity('goblin ringleader'/'pFNM', 'Special').
card_artist('goblin ringleader'/'pFNM', 'Mark Romanoski').
card_number('goblin ringleader'/'pFNM', '87').

card_in_set('goblin warchief', 'pFNM').
card_original_type('goblin warchief'/'pFNM', 'Creature — Goblin Warrior').
card_original_text('goblin warchief'/'pFNM', '').
card_first_print('goblin warchief', 'pFNM').
card_image_name('goblin warchief'/'pFNM', 'goblin warchief').
card_uid('goblin warchief'/'pFNM', 'pFNM:Goblin Warchief:goblin warchief').
card_rarity('goblin warchief'/'pFNM', 'Special').
card_artist('goblin warchief'/'pFNM', 'Tim Hildebrandt').
card_number('goblin warchief'/'pFNM', '72').
card_flavor_text('goblin warchief'/'pFNM', 'They poured from the Skirk Ridge like lava, burning and devouring everything in their path.').

card_in_set('grisly salvage', 'pFNM').
card_original_type('grisly salvage'/'pFNM', 'Instant').
card_original_text('grisly salvage'/'pFNM', '').
card_first_print('grisly salvage', 'pFNM').
card_image_name('grisly salvage'/'pFNM', 'grisly salvage').
card_uid('grisly salvage'/'pFNM', 'pFNM:Grisly Salvage:grisly salvage').
card_rarity('grisly salvage'/'pFNM', 'Special').
card_artist('grisly salvage'/'pFNM', 'Maciej Kuciara').
card_number('grisly salvage'/'pFNM', '162').
card_flavor_text('grisly salvage'/'pFNM', 'To the Golgari, anything buried is treasure.').

card_in_set('hordeling outburst', 'pFNM').
card_original_type('hordeling outburst'/'pFNM', 'Sorcery').
card_original_text('hordeling outburst'/'pFNM', '').
card_first_print('hordeling outburst', 'pFNM').
card_image_name('hordeling outburst'/'pFNM', 'hordeling outburst').
card_uid('hordeling outburst'/'pFNM', 'pFNM:Hordeling Outburst:hordeling outburst').
card_rarity('hordeling outburst'/'pFNM', 'Special').
card_artist('hordeling outburst'/'pFNM', 'Zoltan Boros').
card_number('hordeling outburst'/'pFNM', '178').
card_flavor_text('hordeling outburst'/'pFNM', 'Kolaghan goblins rush into battle to satisfy their desire for violence as well as their desire not to be eaten.').

card_in_set('ice', 'pFNM').
card_original_type('ice'/'pFNM', 'Instant').
card_original_text('ice'/'pFNM', '').
card_first_print('ice', 'pFNM').
card_image_name('ice'/'pFNM', 'fireice').
card_uid('ice'/'pFNM', 'pFNM:Ice:fireice').
card_rarity('ice'/'pFNM', 'Special').
card_artist('ice'/'pFNM', 'Franz Vohwinkel & David Martin').
card_number('ice'/'pFNM', '79b').

card_in_set('icy manipulator', 'pFNM').
card_original_type('icy manipulator'/'pFNM', 'Artifact').
card_original_text('icy manipulator'/'pFNM', '').
card_image_name('icy manipulator'/'pFNM', 'icy manipulator').
card_uid('icy manipulator'/'pFNM', 'pFNM:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'pFNM', 'Special').
card_artist('icy manipulator'/'pFNM', 'Douglas Shuler').
card_number('icy manipulator'/'pFNM', '67').
card_flavor_text('icy manipulator'/'pFNM', 'In fire there is the spark of chaos and destruction, the seed of life. In ice there is perfect tranquility, perfect order, and the silence of death.').

card_in_set('impulse', 'pFNM').
card_original_type('impulse'/'pFNM', 'Instant').
card_original_text('impulse'/'pFNM', '').
card_image_name('impulse'/'pFNM', 'impulse').
card_uid('impulse'/'pFNM', 'pFNM:Impulse:impulse').
card_rarity('impulse'/'pFNM', 'Special').
card_artist('impulse'/'pFNM', 'Bryan Talbot').
card_number('impulse'/'pFNM', '17').

card_in_set('isochron scepter', 'pFNM').
card_original_type('isochron scepter'/'pFNM', 'Artifact').
card_original_text('isochron scepter'/'pFNM', '').
card_first_print('isochron scepter', 'pFNM').
card_image_name('isochron scepter'/'pFNM', 'isochron scepter').
card_uid('isochron scepter'/'pFNM', 'pFNM:Isochron Scepter:isochron scepter').
card_rarity('isochron scepter'/'pFNM', 'Special').
card_artist('isochron scepter'/'pFNM', 'Brian Despain').
card_number('isochron scepter'/'pFNM', '102').

card_in_set('izzet charm', 'pFNM').
card_original_type('izzet charm'/'pFNM', 'Instant').
card_original_text('izzet charm'/'pFNM', '').
card_first_print('izzet charm', 'pFNM').
card_image_name('izzet charm'/'pFNM', 'izzet charm').
card_uid('izzet charm'/'pFNM', 'pFNM:Izzet Charm:izzet charm').
card_rarity('izzet charm'/'pFNM', 'Special').
card_artist('izzet charm'/'pFNM', 'Zoltan Boros').
card_number('izzet charm'/'pFNM', '157').

card_in_set('jace\'s ingenuity', 'pFNM').
card_original_type('jace\'s ingenuity'/'pFNM', 'Instant').
card_original_text('jace\'s ingenuity'/'pFNM', '').
card_first_print('jace\'s ingenuity', 'pFNM').
card_image_name('jace\'s ingenuity'/'pFNM', 'jace\'s ingenuity').
card_uid('jace\'s ingenuity'/'pFNM', 'pFNM:Jace\'s Ingenuity:jace\'s ingenuity').
card_rarity('jace\'s ingenuity'/'pFNM', 'Special').
card_artist('jace\'s ingenuity'/'pFNM', 'Steve Argyle').
card_number('jace\'s ingenuity'/'pFNM', '134').
card_flavor_text('jace\'s ingenuity'/'pFNM', '\"Brute force can sometimes kick down a locked door, but knowledge is a skeleton key.\"').

card_in_set('jackal pup', 'pFNM').
card_original_type('jackal pup'/'pFNM', 'Creature — Hound').
card_original_text('jackal pup'/'pFNM', '').
card_image_name('jackal pup'/'pFNM', 'jackal pup').
card_uid('jackal pup'/'pFNM', 'pFNM:Jackal Pup:jackal pup').
card_rarity('jackal pup'/'pFNM', 'Special').
card_artist('jackal pup'/'pFNM', 'Susan Van Camp').
card_number('jackal pup'/'pFNM', '14').
card_flavor_text('jackal pup'/'pFNM', '\"Cry havoc and let slip the dogs of war.\"\n—William Shakespeare, Julius Caesar').

card_in_set('judge\'s familiar', 'pFNM').
card_original_type('judge\'s familiar'/'pFNM', 'Creature — Bird').
card_original_text('judge\'s familiar'/'pFNM', '').
card_first_print('judge\'s familiar', 'pFNM').
card_image_name('judge\'s familiar'/'pFNM', 'judge\'s familiar').
card_uid('judge\'s familiar'/'pFNM', 'pFNM:Judge\'s Familiar:judge\'s familiar').
card_rarity('judge\'s familiar'/'pFNM', 'Special').
card_artist('judge\'s familiar'/'pFNM', 'Ryan Yee').
card_number('judge\'s familiar'/'pFNM', '156').
card_flavor_text('judge\'s familiar'/'pFNM', '\"It misses nothing and it has no sense of humor.\"\n—Barvisa, courtroom scribe').

card_in_set('juggernaut', 'pFNM').
card_original_type('juggernaut'/'pFNM', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'pFNM', '').
card_image_name('juggernaut'/'pFNM', 'juggernaut').
card_uid('juggernaut'/'pFNM', 'pFNM:Juggernaut:juggernaut').
card_rarity('juggernaut'/'pFNM', 'Special').
card_artist('juggernaut'/'pFNM', 'Dan Frazier').
card_number('juggernaut'/'pFNM', '62').
card_flavor_text('juggernaut'/'pFNM', 'Built with neither a way to steer nor a way to stop, the juggernauts were simply aimed at an enemy\'s best defenses and told to charge.').

card_in_set('kird ape', 'pFNM').
card_original_type('kird ape'/'pFNM', 'Creature — Ape').
card_original_text('kird ape'/'pFNM', '').
card_image_name('kird ape'/'pFNM', 'kird ape').
card_uid('kird ape'/'pFNM', 'pFNM:Kird Ape:kird ape').
card_rarity('kird ape'/'pFNM', 'Special').
card_artist('kird ape'/'pFNM', 'Ken Meyer, Jr.').
card_number('kird ape'/'pFNM', '64').
card_flavor_text('kird ape'/'pFNM', 'It puts the \"fur\" in \"fury.\"').

card_in_set('kitchen finks', 'pFNM').
card_original_type('kitchen finks'/'pFNM', 'Creature — Ouphe').
card_original_text('kitchen finks'/'pFNM', '').
card_first_print('kitchen finks', 'pFNM').
card_image_name('kitchen finks'/'pFNM', 'kitchen finks').
card_uid('kitchen finks'/'pFNM', 'pFNM:Kitchen Finks:kitchen finks').
card_rarity('kitchen finks'/'pFNM', 'Special').
card_artist('kitchen finks'/'pFNM', 'Larry MacDougall').
card_number('kitchen finks'/'pFNM', '106').

card_in_set('krosan grip', 'pFNM').
card_original_type('krosan grip'/'pFNM', 'Instant').
card_original_text('krosan grip'/'pFNM', '').
card_first_print('krosan grip', 'pFNM').
card_image_name('krosan grip'/'pFNM', 'krosan grip').
card_uid('krosan grip'/'pFNM', 'pFNM:Krosan Grip:krosan grip').
card_rarity('krosan grip'/'pFNM', 'Special').
card_artist('krosan grip'/'pFNM', 'Franz Vohwinkel').
card_number('krosan grip'/'pFNM', '123').
card_flavor_text('krosan grip'/'pFNM', '\"Give up these unnatural weapons, these scrolls. Heart and mind and fist are enough.\"\n—Zyd, Kamahlite druid').

card_in_set('krosan tusker', 'pFNM').
card_original_type('krosan tusker'/'pFNM', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'pFNM', '').
card_first_print('krosan tusker', 'pFNM').
card_image_name('krosan tusker'/'pFNM', 'krosan tusker').
card_uid('krosan tusker'/'pFNM', 'pFNM:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'pFNM', 'Special').
card_artist('krosan tusker'/'pFNM', 'Kev Walker').
card_number('krosan tusker'/'pFNM', '42').

card_in_set('krosan warchief', 'pFNM').
card_original_type('krosan warchief'/'pFNM', 'Creature — Beast').
card_original_text('krosan warchief'/'pFNM', '').
card_first_print('krosan warchief', 'pFNM').
card_image_name('krosan warchief'/'pFNM', 'krosan warchief').
card_uid('krosan warchief'/'pFNM', 'pFNM:Krosan Warchief:krosan warchief').
card_rarity('krosan warchief'/'pFNM', 'Special').
card_artist('krosan warchief'/'pFNM', 'Greg Hildebrandt').
card_number('krosan warchief'/'pFNM', '47').
card_flavor_text('krosan warchief'/'pFNM', 'It turns prey into predator.').

card_in_set('life', 'pFNM').
card_original_type('life'/'pFNM', 'Sorcery').
card_original_text('life'/'pFNM', '').
card_first_print('life', 'pFNM').
card_image_name('life'/'pFNM', 'lifedeath').
card_uid('life'/'pFNM', 'pFNM:Life:lifedeath').
card_rarity('life'/'pFNM', 'Special').
card_artist('life'/'pFNM', 'Edward P. Beard, Jr. & Anthony S. Waters').
card_number('life'/'pFNM', '78a').

card_in_set('lightning greaves', 'pFNM').
card_original_type('lightning greaves'/'pFNM', 'Artifact — Equipment').
card_original_text('lightning greaves'/'pFNM', '').
card_first_print('lightning greaves', 'pFNM').
card_image_name('lightning greaves'/'pFNM', 'lightning greaves').
card_uid('lightning greaves'/'pFNM', 'pFNM:Lightning Greaves:lightning greaves').
card_rarity('lightning greaves'/'pFNM', 'Special').
card_artist('lightning greaves'/'pFNM', 'Michael Komarck').
card_number('lightning greaves'/'pFNM', '111').
card_flavor_text('lightning greaves'/'pFNM', 'After lightning struck the cliffs, the ore became iron, the iron became steel, and the steel became greaves. The lightning never left.').

card_in_set('lightning rift', 'pFNM').
card_original_type('lightning rift'/'pFNM', 'Enchantment').
card_original_text('lightning rift'/'pFNM', '').
card_first_print('lightning rift', 'pFNM').
card_image_name('lightning rift'/'pFNM', 'lightning rift').
card_uid('lightning rift'/'pFNM', 'pFNM:Lightning Rift:lightning rift').
card_rarity('lightning rift'/'pFNM', 'Special').
card_artist('lightning rift'/'pFNM', 'Eric Peterson').
card_number('lightning rift'/'pFNM', '48').
card_flavor_text('lightning rift'/'pFNM', 'Options will cost you, but a lack of options will cost you even more.').

card_in_set('lingering souls', 'pFNM').
card_original_type('lingering souls'/'pFNM', 'Sorcery').
card_original_text('lingering souls'/'pFNM', '').
card_first_print('lingering souls', 'pFNM').
card_image_name('lingering souls'/'pFNM', 'lingering souls').
card_uid('lingering souls'/'pFNM', 'pFNM:Lingering Souls:lingering souls').
card_rarity('lingering souls'/'pFNM', 'Special').
card_artist('lingering souls'/'pFNM', 'John Stanko').
card_number('lingering souls'/'pFNM', '148').
card_flavor_text('lingering souls'/'pFNM', 'The murdered inhabitants of Hollowhenge impart to the living the terror they felt in death.').

card_in_set('llanowar elves', 'pFNM').
card_original_type('llanowar elves'/'pFNM', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'pFNM', '').
card_image_name('llanowar elves'/'pFNM', 'llanowar elves').
card_uid('llanowar elves'/'pFNM', 'pFNM:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'pFNM', 'Special').
card_artist('llanowar elves'/'pFNM', 'Anson Maddocks').
card_number('llanowar elves'/'pFNM', '11').

card_in_set('lobotomy', 'pFNM').
card_original_type('lobotomy'/'pFNM', 'Sorcery').
card_original_text('lobotomy'/'pFNM', '').
card_image_name('lobotomy'/'pFNM', 'lobotomy').
card_uid('lobotomy'/'pFNM', 'pFNM:Lobotomy:lobotomy').
card_rarity('lobotomy'/'pFNM', 'Special').
card_artist('lobotomy'/'pFNM', 'Thomas M. Baxa').
card_number('lobotomy'/'pFNM', '71').

card_in_set('longbow archer', 'pFNM').
card_original_type('longbow archer'/'pFNM', 'Creature — Human Soldier Archer').
card_original_text('longbow archer'/'pFNM', '').
card_image_name('longbow archer'/'pFNM', 'longbow archer').
card_uid('longbow archer'/'pFNM', 'pFNM:Longbow Archer:longbow archer').
card_rarity('longbow archer'/'pFNM', 'Special').
card_artist('longbow archer'/'pFNM', 'Eric Peterson').
card_number('longbow archer'/'pFNM', '3').
card_flavor_text('longbow archer'/'pFNM', '\"If it bears wings, I will pin it to the skies over Tefemburu.\"\n—Roya, Zhalfirin archer').

card_in_set('magma jet', 'pFNM').
card_original_type('magma jet'/'pFNM', 'Instant').
card_original_text('magma jet'/'pFNM', '').
card_first_print('magma jet', 'pFNM').
card_image_name('magma jet'/'pFNM', 'magma jet').
card_uid('magma jet'/'pFNM', 'pFNM:Magma Jet:magma jet').
card_rarity('magma jet'/'pFNM', 'Special').
card_artist('magma jet'/'pFNM', 'Darrell Riche').
card_number('magma jet'/'pFNM', '104').

card_in_set('magma spray', 'pFNM').
card_original_type('magma spray'/'pFNM', 'Instant').
card_original_text('magma spray'/'pFNM', '').
card_first_print('magma spray', 'pFNM').
card_image_name('magma spray'/'pFNM', 'magma spray').
card_uid('magma spray'/'pFNM', 'pFNM:Magma Spray:magma spray').
card_rarity('magma spray'/'pFNM', 'Special').
card_artist('magma spray'/'pFNM', 'Mathias Kollros').
card_number('magma spray'/'pFNM', '170').
card_flavor_text('magma spray'/'pFNM', 'A stone hurled at an enemy is deadlier in liquid form.').

card_in_set('merrow reejerey', 'pFNM').
card_original_type('merrow reejerey'/'pFNM', 'Creature — Merfolk Soldier').
card_original_text('merrow reejerey'/'pFNM', '').
card_first_print('merrow reejerey', 'pFNM').
card_image_name('merrow reejerey'/'pFNM', 'merrow reejerey').
card_uid('merrow reejerey'/'pFNM', 'pFNM:Merrow Reejerey:merrow reejerey').
card_rarity('merrow reejerey'/'pFNM', 'Special').
card_artist('merrow reejerey'/'pFNM', 'Thomas M. Baxa').
card_number('merrow reejerey'/'pFNM', '107').
card_flavor_text('merrow reejerey'/'pFNM', 'Steady and silent as the deep current, the reejerey guides the course of the school.').

card_in_set('mind warp', 'pFNM').
card_original_type('mind warp'/'pFNM', 'Sorcery').
card_original_text('mind warp'/'pFNM', '').
card_image_name('mind warp'/'pFNM', 'mind warp').
card_uid('mind warp'/'pFNM', 'pFNM:Mind Warp:mind warp').
card_rarity('mind warp'/'pFNM', 'Special').
card_artist('mind warp'/'pFNM', 'Liz Danforth').
card_number('mind warp'/'pFNM', '5').
card_flavor_text('mind warp'/'pFNM', 'The hardest of wizards can have the softest of minds.').

card_in_set('mogg fanatic', 'pFNM').
card_original_type('mogg fanatic'/'pFNM', 'Creature — Goblin').
card_original_text('mogg fanatic'/'pFNM', '').
card_image_name('mogg fanatic'/'pFNM', 'mogg fanatic').
card_uid('mogg fanatic'/'pFNM', 'pFNM:Mogg Fanatic:mogg fanatic').
card_rarity('mogg fanatic'/'pFNM', 'Special').
card_artist('mogg fanatic'/'pFNM', 'Brom').
card_number('mogg fanatic'/'pFNM', '29').
card_flavor_text('mogg fanatic'/'pFNM', '\"As if his chest had been a mortar, he burst his hot heart\'s shell upon it.\"\n—Herman Melville, Moby Dick').

card_in_set('mother of runes', 'pFNM').
card_original_type('mother of runes'/'pFNM', 'Creature — Human Cleric').
card_original_text('mother of runes'/'pFNM', '').
card_image_name('mother of runes'/'pFNM', 'mother of runes').
card_uid('mother of runes'/'pFNM', 'pFNM:Mother of Runes:mother of runes').
card_rarity('mother of runes'/'pFNM', 'Special').
card_artist('mother of runes'/'pFNM', 'Scott M. Fischer').
card_number('mother of runes'/'pFNM', '54').
card_flavor_text('mother of runes'/'pFNM', '\"My family protects all families.\"').

card_in_set('mulldrifter', 'pFNM').
card_original_type('mulldrifter'/'pFNM', 'Creature — Elemental').
card_original_text('mulldrifter'/'pFNM', '').
card_first_print('mulldrifter', 'pFNM').
card_image_name('mulldrifter'/'pFNM', 'mulldrifter').
card_uid('mulldrifter'/'pFNM', 'pFNM:Mulldrifter:mulldrifter').
card_rarity('mulldrifter'/'pFNM', 'Special').
card_artist('mulldrifter'/'pFNM', 'Wayne England').
card_number('mulldrifter'/'pFNM', '109').

card_in_set('murderous redcap', 'pFNM').
card_original_type('murderous redcap'/'pFNM', 'Creature — Goblin Assassin').
card_original_text('murderous redcap'/'pFNM', '').
card_first_print('murderous redcap', 'pFNM').
card_image_name('murderous redcap'/'pFNM', 'murderous redcap').
card_uid('murderous redcap'/'pFNM', 'pFNM:Murderous Redcap:murderous redcap').
card_rarity('murderous redcap'/'pFNM', 'Special').
card_artist('murderous redcap'/'pFNM', 'Mark Hyzer').
card_number('murderous redcap'/'pFNM', '110').

card_in_set('muscle sliver', 'pFNM').
card_original_type('muscle sliver'/'pFNM', 'Creature — Sliver').
card_original_text('muscle sliver'/'pFNM', '').
card_image_name('muscle sliver'/'pFNM', 'muscle sliver').
card_uid('muscle sliver'/'pFNM', 'pFNM:Muscle Sliver:muscle sliver').
card_rarity('muscle sliver'/'pFNM', 'Special').
card_artist('muscle sliver'/'pFNM', 'Richard Kane Ferguson').
card_number('muscle sliver'/'pFNM', '33').
card_flavor_text('muscle sliver'/'pFNM', 'The air was filled with the cracks and snaps of flesh hardening as the new sliver joined the battle.').

card_in_set('myr enforcer', 'pFNM').
card_original_type('myr enforcer'/'pFNM', 'Artifact Creature — Myr').
card_original_text('myr enforcer'/'pFNM', '').
card_first_print('myr enforcer', 'pFNM').
card_image_name('myr enforcer'/'pFNM', 'myr enforcer').
card_uid('myr enforcer'/'pFNM', 'pFNM:Myr Enforcer:myr enforcer').
card_rarity('myr enforcer'/'pFNM', 'Special').
card_artist('myr enforcer'/'pFNM', 'Jim Murray').
card_number('myr enforcer'/'pFNM', '105').
card_flavor_text('myr enforcer'/'pFNM', 'Most myr monitor other species. Some myr monitor other myr.').

card_in_set('oblivion ring', 'pFNM').
card_original_type('oblivion ring'/'pFNM', 'Enchantment').
card_original_text('oblivion ring'/'pFNM', '').
card_first_print('oblivion ring', 'pFNM').
card_image_name('oblivion ring'/'pFNM', 'oblivion ring').
card_uid('oblivion ring'/'pFNM', 'pFNM:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'pFNM', 'Special').
card_artist('oblivion ring'/'pFNM', 'Chuck Lukacs').
card_number('oblivion ring'/'pFNM', '114').

card_in_set('ophidian', 'pFNM').
card_original_type('ophidian'/'pFNM', 'Creature — Snake').
card_original_text('ophidian'/'pFNM', '').
card_image_name('ophidian'/'pFNM', 'ophidian').
card_uid('ophidian'/'pFNM', 'pFNM:Ophidian:ophidian').
card_rarity('ophidian'/'pFNM', 'Special').
card_artist('ophidian'/'pFNM', 'Cliff Nielsen').
card_number('ophidian'/'pFNM', '13').
card_flavor_text('ophidian'/'pFNM', '\"I will ... tell thee more than thou hast wit to ask.\"\n—Christopher Marlowe, Doctor Faustus').

card_in_set('pendelhaven', 'pFNM').
card_original_type('pendelhaven'/'pFNM', 'Legendary Land').
card_original_text('pendelhaven'/'pFNM', '').
card_image_name('pendelhaven'/'pFNM', 'pendelhaven').
card_uid('pendelhaven'/'pFNM', 'pFNM:Pendelhaven:pendelhaven').
card_rarity('pendelhaven'/'pFNM', 'Special').
card_artist('pendelhaven'/'pFNM', 'Philip Straub').
card_number('pendelhaven'/'pFNM', '96').
card_flavor_text('pendelhaven'/'pFNM', 'The limbs of Pendelhaven extend as far as the meek roam, and reach as high as they strive.').

card_in_set('pillar of flame', 'pFNM').
card_original_type('pillar of flame'/'pFNM', 'Sorcery').
card_original_text('pillar of flame'/'pFNM', '').
card_first_print('pillar of flame', 'pFNM').
card_image_name('pillar of flame'/'pFNM', 'pillar of flame').
card_uid('pillar of flame'/'pFNM', 'pFNM:Pillar of Flame:pillar of flame').
card_rarity('pillar of flame'/'pFNM', 'Special').
card_artist('pillar of flame'/'pFNM', 'Dave Kendall').
card_number('pillar of flame'/'pFNM', '150').
card_flavor_text('pillar of flame'/'pFNM', '\"May the worthy spend an eternity in Blessed Sleep. May the wicked find the peace of oblivion.\"').

card_in_set('priest of titania', 'pFNM').
card_original_type('priest of titania'/'pFNM', 'Creature — Elf Druid').
card_original_text('priest of titania'/'pFNM', '').
card_image_name('priest of titania'/'pFNM', 'priest of titania').
card_uid('priest of titania'/'pFNM', 'pFNM:Priest of Titania:priest of titania').
card_rarity('priest of titania'/'pFNM', 'Special').
card_artist('priest of titania'/'pFNM', 'Rebecca Guay').
card_number('priest of titania'/'pFNM', '36').
card_flavor_text('priest of titania'/'pFNM', 'Titania rewards all who honor the forest by making them a living part of it.').

card_in_set('prodigal sorcerer', 'pFNM').
card_original_type('prodigal sorcerer'/'pFNM', 'Creature — Human Wizard').
card_original_text('prodigal sorcerer'/'pFNM', '').
card_image_name('prodigal sorcerer'/'pFNM', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'pFNM', 'pFNM:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'pFNM', 'Special').
card_artist('prodigal sorcerer'/'pFNM', 'Douglas Shuler').
card_number('prodigal sorcerer'/'pFNM', '9').

card_in_set('qasali pridemage', 'pFNM').
card_original_type('qasali pridemage'/'pFNM', 'Creature — Cat Wizard').
card_original_text('qasali pridemage'/'pFNM', '').
card_first_print('qasali pridemage', 'pFNM').
card_image_name('qasali pridemage'/'pFNM', 'qasali pridemage').
card_uid('qasali pridemage'/'pFNM', 'pFNM:Qasali Pridemage:qasali pridemage').
card_rarity('qasali pridemage'/'pFNM', 'Special').
card_artist('qasali pridemage'/'pFNM', 'Dave Allsop').
card_number('qasali pridemage'/'pFNM', '124').
card_flavor_text('qasali pridemage'/'pFNM', 'An elder in one pride, of the Sigiled caste in another.').

card_in_set('quirion ranger', 'pFNM').
card_original_type('quirion ranger'/'pFNM', 'Creature — Elf').
card_original_text('quirion ranger'/'pFNM', '').
card_image_name('quirion ranger'/'pFNM', 'quirion ranger').
card_uid('quirion ranger'/'pFNM', 'pFNM:Quirion Ranger:quirion ranger').
card_rarity('quirion ranger'/'pFNM', 'Special').
card_artist('quirion ranger'/'pFNM', 'Tom Kyffin').
card_number('quirion ranger'/'pFNM', '15').
card_flavor_text('quirion ranger'/'pFNM', '\"I went to the woods because I wished to live deliberately.\"\n—Henry David Thoreau, Walden').

card_in_set('rakdos cackler', 'pFNM').
card_original_type('rakdos cackler'/'pFNM', 'Creature — Devil').
card_original_text('rakdos cackler'/'pFNM', '').
card_first_print('rakdos cackler', 'pFNM').
card_image_name('rakdos cackler'/'pFNM', 'rakdos cackler').
card_uid('rakdos cackler'/'pFNM', 'pFNM:Rakdos Cackler:rakdos cackler').
card_rarity('rakdos cackler'/'pFNM', 'Special').
card_artist('rakdos cackler'/'pFNM', 'Dave Kendall').
card_number('rakdos cackler'/'pFNM', '158').
card_flavor_text('rakdos cackler'/'pFNM', 'Devil-blades and dripping blood make music Rakdos never tires of hearing.').

card_in_set('rancor', 'pFNM').
card_original_type('rancor'/'pFNM', 'Enchantment — Aura').
card_original_text('rancor'/'pFNM', '').
card_image_name('rancor'/'pFNM', 'rancor').
card_uid('rancor'/'pFNM', 'pFNM:Rancor:rancor').
card_rarity('rancor'/'pFNM', 'Special').
card_artist('rancor'/'pFNM', 'Kev Walker').
card_number('rancor'/'pFNM', '56').
card_flavor_text('rancor'/'pFNM', 'Hatred outlives the hateful.').

card_in_set('reanimate', 'pFNM').
card_original_type('reanimate'/'pFNM', 'Sorcery').
card_original_text('reanimate'/'pFNM', '').
card_image_name('reanimate'/'pFNM', 'reanimate').
card_uid('reanimate'/'pFNM', 'pFNM:Reanimate:reanimate').
card_rarity('reanimate'/'pFNM', 'Special').
card_artist('reanimate'/'pFNM', 'Robert Bliss').
card_number('reanimate'/'pFNM', '53').
card_flavor_text('reanimate'/'pFNM', '\"You will learn to earn death.\"\n—Volrath').

card_in_set('reliquary tower', 'pFNM').
card_original_type('reliquary tower'/'pFNM', 'Land').
card_original_text('reliquary tower'/'pFNM', '').
card_first_print('reliquary tower', 'pFNM').
card_image_name('reliquary tower'/'pFNM', 'reliquary tower').
card_uid('reliquary tower'/'pFNM', 'pFNM:Reliquary Tower:reliquary tower').
card_rarity('reliquary tower'/'pFNM', 'Special').
card_artist('reliquary tower'/'pFNM', 'Drew Baker').
card_number('reliquary tower'/'pFNM', '153').
card_flavor_text('reliquary tower'/'pFNM', 'Once guarded by the Knights of the Reliquary, the tower stands now protected only by its own remoteness, its dusty treasures open to plunder by anyone.').

card_in_set('remand', 'pFNM').
card_original_type('remand'/'pFNM', 'Instant').
card_original_text('remand'/'pFNM', '').
card_first_print('remand', 'pFNM').
card_image_name('remand'/'pFNM', 'remand').
card_uid('remand'/'pFNM', 'pFNM:Remand:remand').
card_rarity('remand'/'pFNM', 'Special').
card_artist('remand'/'pFNM', 'Hideaki Takamura').
card_number('remand'/'pFNM', '92').
card_flavor_text('remand'/'pFNM', 'Well, at least all of that arm-waving and arcane babbling you did was impressive.').

card_in_set('resurrection', 'pFNM').
card_original_type('resurrection'/'pFNM', 'Sorcery').
card_original_text('resurrection'/'pFNM', '').
card_image_name('resurrection'/'pFNM', 'resurrection').
card_uid('resurrection'/'pFNM', 'pFNM:Resurrection:resurrection').
card_rarity('resurrection'/'pFNM', 'Special').
card_artist('resurrection'/'pFNM', 'Michael Komarck').
card_number('resurrection'/'pFNM', '97').
card_flavor_text('resurrection'/'pFNM', '\"Think of this new life not as a gift from the angels but as a command to fight for their cause once more.\" —War blessing of Serra').

card_in_set('rhox war monk', 'pFNM').
card_original_type('rhox war monk'/'pFNM', 'Creature — Rhino Monk').
card_original_text('rhox war monk'/'pFNM', '').
card_first_print('rhox war monk', 'pFNM').
card_image_name('rhox war monk'/'pFNM', 'rhox war monk').
card_uid('rhox war monk'/'pFNM', 'pFNM:Rhox War Monk:rhox war monk').
card_rarity('rhox war monk'/'pFNM', 'Special').
card_artist('rhox war monk'/'pFNM', 'Allen Williams').
card_number('rhox war monk'/'pFNM', '133').
card_flavor_text('rhox war monk'/'pFNM', 'Rhox monks are dedicated to spiritual growth and learning, and most bear the sigils of many students. However, they do not gladly suffer fools or those who disagree with their carefully wrought dogma.').

card_in_set('rift bolt', 'pFNM').
card_original_type('rift bolt'/'pFNM', 'Sorcery').
card_original_text('rift bolt'/'pFNM', '').
card_first_print('rift bolt', 'pFNM').
card_image_name('rift bolt'/'pFNM', 'rift bolt').
card_uid('rift bolt'/'pFNM', 'pFNM:Rift Bolt:rift bolt').
card_rarity('rift bolt'/'pFNM', 'Special').
card_artist('rift bolt'/'pFNM', 'Daniel Ljunggren').
card_number('rift bolt'/'pFNM', '125').

card_in_set('river boa', 'pFNM').
card_original_type('river boa'/'pFNM', 'Creature — Snake').
card_original_text('river boa'/'pFNM', '').
card_image_name('river boa'/'pFNM', 'river boa').
card_uid('river boa'/'pFNM', 'pFNM:River Boa:river boa').
card_rarity('river boa'/'pFNM', 'Special').
card_artist('river boa'/'pFNM', 'Steve White').
card_number('river boa'/'pFNM', '1').
card_flavor_text('river boa'/'pFNM', '\"But no one heard the snake\'s gentle hiss for peace over the elephant\'s trumpeting of war.\"\n—Afari, Tales').

card_in_set('roar of the wurm', 'pFNM').
card_original_type('roar of the wurm'/'pFNM', 'Sorcery').
card_original_text('roar of the wurm'/'pFNM', '').
card_first_print('roar of the wurm', 'pFNM').
card_image_name('roar of the wurm'/'pFNM', 'roar of the wurm').
card_uid('roar of the wurm'/'pFNM', 'pFNM:Roar of the Wurm:roar of the wurm').
card_rarity('roar of the wurm'/'pFNM', 'Special').
card_artist('roar of the wurm'/'pFNM', 'Kev Walker').
card_number('roar of the wurm'/'pFNM', '90').

card_in_set('sakura-tribe elder', 'pFNM').
card_original_type('sakura-tribe elder'/'pFNM', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'pFNM', '').
card_image_name('sakura-tribe elder'/'pFNM', 'sakura-tribe elder').
card_uid('sakura-tribe elder'/'pFNM', 'pFNM:Sakura-Tribe Elder:sakura-tribe elder').
card_rarity('sakura-tribe elder'/'pFNM', 'Special').
card_artist('sakura-tribe elder'/'pFNM', 'Ted Galaday').
card_number('sakura-tribe elder'/'pFNM', '115').
card_flavor_text('sakura-tribe elder'/'pFNM', 'Slain orochi warriors were buried with a tree sapling so they would become part of the forest after death.').

card_in_set('savage lands', 'pFNM').
card_original_type('savage lands'/'pFNM', 'Land').
card_original_text('savage lands'/'pFNM', '').
card_first_print('savage lands', 'pFNM').
card_image_name('savage lands'/'pFNM', 'savage lands').
card_uid('savage lands'/'pFNM', 'pFNM:Savage Lands:savage lands').
card_rarity('savage lands'/'pFNM', 'Special').
card_artist('savage lands'/'pFNM', 'John Avon').
card_number('savage lands'/'pFNM', '139').
card_flavor_text('savage lands'/'pFNM', 'Jund is a world as cruel as those who call it home. Their brutal struggles scar the land even as it carves them in its image, a vicious circle spiraling out of control.').

card_in_set('scragnoth', 'pFNM').
card_original_type('scragnoth'/'pFNM', 'Creature — Beast').
card_original_text('scragnoth'/'pFNM', '').
card_image_name('scragnoth'/'pFNM', 'scragnoth').
card_uid('scragnoth'/'pFNM', 'pFNM:Scragnoth:scragnoth').
card_rarity('scragnoth'/'pFNM', 'Special').
card_artist('scragnoth'/'pFNM', 'Jeff Laubenstein').
card_number('scragnoth'/'pFNM', '38').
card_flavor_text('scragnoth'/'pFNM', 'It possesses no intelligence, only counter-intelligence.').

card_in_set('seal of cleansing', 'pFNM').
card_original_type('seal of cleansing'/'pFNM', 'Enchantment').
card_original_text('seal of cleansing'/'pFNM', '').
card_first_print('seal of cleansing', 'pFNM').
card_image_name('seal of cleansing'/'pFNM', 'seal of cleansing').
card_uid('seal of cleansing'/'pFNM', 'pFNM:Seal of Cleansing:seal of cleansing').
card_rarity('seal of cleansing'/'pFNM', 'Special').
card_artist('seal of cleansing'/'pFNM', 'Christopher Moeller').
card_number('seal of cleansing'/'pFNM', '57').
card_flavor_text('seal of cleansing'/'pFNM', '\"I am the purifier, the light that clears all shadows.\"\n—Seal inscription ').

card_in_set('searing spear', 'pFNM').
card_original_type('searing spear'/'pFNM', 'Instant').
card_original_text('searing spear'/'pFNM', '').
card_first_print('searing spear', 'pFNM').
card_image_name('searing spear'/'pFNM', 'searing spear').
card_uid('searing spear'/'pFNM', 'pFNM:Searing Spear:searing spear').
card_rarity('searing spear'/'pFNM', 'Special').
card_artist('searing spear'/'pFNM', 'Christopher Moeller').
card_number('searing spear'/'pFNM', '152').
card_flavor_text('searing spear'/'pFNM', 'Sometimes you die a glorious death with your sword held high. Sometimes you\'re just target practice.').

card_in_set('serrated arrows', 'pFNM').
card_original_type('serrated arrows'/'pFNM', 'Artifact').
card_original_text('serrated arrows'/'pFNM', '').
card_image_name('serrated arrows'/'pFNM', 'serrated arrows').
card_uid('serrated arrows'/'pFNM', 'pFNM:Serrated Arrows:serrated arrows').
card_rarity('serrated arrows'/'pFNM', 'Special').
card_artist('serrated arrows'/'pFNM', 'John Avon').
card_number('serrated arrows'/'pFNM', '101').

card_in_set('shock', 'pFNM').
card_original_type('shock'/'pFNM', 'Instant').
card_original_text('shock'/'pFNM', '').
card_image_name('shock'/'pFNM', 'shock').
card_uid('shock'/'pFNM', 'pFNM:Shock:shock').
card_rarity('shock'/'pFNM', 'Special').
card_artist('shock'/'pFNM', 'Randy Gallegos').
card_number('shock'/'pFNM', '6').
card_flavor_text('shock'/'pFNM', 'Lightning tethers souls to the world. —Kor saying').

card_in_set('shrapnel blast', 'pFNM').
card_original_type('shrapnel blast'/'pFNM', 'Instant').
card_original_text('shrapnel blast'/'pFNM', '').
card_first_print('shrapnel blast', 'pFNM').
card_image_name('shrapnel blast'/'pFNM', 'shrapnel blast').
card_uid('shrapnel blast'/'pFNM', 'pFNM:Shrapnel Blast:shrapnel blast').
card_rarity('shrapnel blast'/'pFNM', 'Special').
card_artist('shrapnel blast'/'pFNM', 'Hideaki Takamura').
card_number('shrapnel blast'/'pFNM', '103').
card_flavor_text('shrapnel blast'/'pFNM', 'To the Krark Clan, every artifact has a trigger and a barrel.').

card_in_set('silver knight', 'pFNM').
card_original_type('silver knight'/'pFNM', 'Creature — Human Knight').
card_original_text('silver knight'/'pFNM', '').
card_first_print('silver knight', 'pFNM').
card_image_name('silver knight'/'pFNM', 'silver knight').
card_uid('silver knight'/'pFNM', 'pFNM:Silver Knight:silver knight').
card_rarity('silver knight'/'pFNM', 'Special').
card_artist('silver knight'/'pFNM', 'Eric Peterson').
card_number('silver knight'/'pFNM', '46').
card_flavor_text('silver knight'/'pFNM', 'Otaria\'s last defense against the wave of chaos threatening to engulf it.').

card_in_set('sin collector', 'pFNM').
card_original_type('sin collector'/'pFNM', 'Creature — Human Cleric').
card_original_text('sin collector'/'pFNM', '').
card_first_print('sin collector', 'pFNM').
card_image_name('sin collector'/'pFNM', 'sin collector').
card_uid('sin collector'/'pFNM', 'pFNM:Sin Collector:sin collector').
card_rarity('sin collector'/'pFNM', 'Special').
card_artist('sin collector'/'pFNM', 'Izzy').
card_number('sin collector'/'pFNM', '163').
card_flavor_text('sin collector'/'pFNM', '\"That must weigh heavily on your soul. Let me purge it for you.\"').

card_in_set('slice and dice', 'pFNM').
card_original_type('slice and dice'/'pFNM', 'Sorcery').
card_original_text('slice and dice'/'pFNM', '').
card_first_print('slice and dice', 'pFNM').
card_image_name('slice and dice'/'pFNM', 'slice and dice').
card_uid('slice and dice'/'pFNM', 'pFNM:Slice and Dice:slice and dice').
card_rarity('slice and dice'/'pFNM', 'Special').
card_artist('slice and dice'/'pFNM', 'Mark Brill').
card_number('slice and dice'/'pFNM', '45').

card_in_set('smother', 'pFNM').
card_original_type('smother'/'pFNM', 'Instant').
card_original_text('smother'/'pFNM', '').
card_first_print('smother', 'pFNM').
card_image_name('smother'/'pFNM', 'smother').
card_uid('smother'/'pFNM', 'pFNM:Smother:smother').
card_rarity('smother'/'pFNM', 'Special').
card_artist('smother'/'pFNM', 'Carl Critchlow').
card_number('smother'/'pFNM', '39').
card_flavor_text('smother'/'pFNM', '\"I can\'t hear them scream, but at least I don\'t have to listen to them beg.\"\n—Phage the Untouchable').

card_in_set('soltari priest', 'pFNM').
card_original_type('soltari priest'/'pFNM', 'Creature — Soltari Cleric').
card_original_text('soltari priest'/'pFNM', '').
card_image_name('soltari priest'/'pFNM', 'soltari priest').
card_uid('soltari priest'/'pFNM', 'pFNM:Soltari Priest:soltari priest').
card_rarity('soltari priest'/'pFNM', 'Special').
card_artist('soltari priest'/'pFNM', 'Janet Aulisio').
card_number('soltari priest'/'pFNM', '19').
card_flavor_text('soltari priest'/'pFNM', '\"Fire is the test of gold; adversity of strong men.\" - Seneca, \"On Providence,\" trans. Basara').

card_in_set('sparksmith', 'pFNM').
card_original_type('sparksmith'/'pFNM', 'Creature — Goblin').
card_original_text('sparksmith'/'pFNM', '').
card_first_print('sparksmith', 'pFNM').
card_image_name('sparksmith'/'pFNM', 'sparksmith').
card_uid('sparksmith'/'pFNM', 'pFNM:Sparksmith:sparksmith').
card_rarity('sparksmith'/'pFNM', 'Special').
card_artist('sparksmith'/'pFNM', 'Jim Nelson').
card_number('sparksmith'/'pFNM', '41').
card_flavor_text('sparksmith'/'pFNM', '\"If it didn\'t hurt, how would I know it worked?\"').

card_in_set('spellstutter sprite', 'pFNM').
card_original_type('spellstutter sprite'/'pFNM', 'Creature — Faerie Wizard').
card_original_text('spellstutter sprite'/'pFNM', '').
card_first_print('spellstutter sprite', 'pFNM').
card_image_name('spellstutter sprite'/'pFNM', 'spellstutter sprite').
card_uid('spellstutter sprite'/'pFNM', 'pFNM:Spellstutter Sprite:spellstutter sprite').
card_rarity('spellstutter sprite'/'pFNM', 'Special').
card_artist('spellstutter sprite'/'pFNM', 'Christopher Moeller').
card_number('spellstutter sprite'/'pFNM', '129').

card_in_set('spike feeder', 'pFNM').
card_original_type('spike feeder'/'pFNM', 'Creature — Spike').
card_original_text('spike feeder'/'pFNM', '').
card_image_name('spike feeder'/'pFNM', 'spike feeder').
card_uid('spike feeder'/'pFNM', 'pFNM:Spike Feeder:spike feeder').
card_rarity('spike feeder'/'pFNM', 'Special').
card_artist('spike feeder'/'pFNM', 'Heather Hudson').
card_number('spike feeder'/'pFNM', '28').

card_in_set('squadron hawk', 'pFNM').
card_original_type('squadron hawk'/'pFNM', 'Creature — Bird').
card_original_text('squadron hawk'/'pFNM', '').
card_first_print('squadron hawk', 'pFNM').
card_image_name('squadron hawk'/'pFNM', 'squadron hawk').
card_uid('squadron hawk'/'pFNM', 'pFNM:Squadron Hawk:squadron hawk').
card_rarity('squadron hawk'/'pFNM', 'Special').
card_artist('squadron hawk'/'pFNM', 'Martina Pilcerova').
card_number('squadron hawk'/'pFNM', '132').

card_in_set('staunch defenders', 'pFNM').
card_original_type('staunch defenders'/'pFNM', 'Creature — Human Soldier').
card_original_text('staunch defenders'/'pFNM', '').
card_image_name('staunch defenders'/'pFNM', 'staunch defenders').
card_uid('staunch defenders'/'pFNM', 'pFNM:Staunch Defenders:staunch defenders').
card_rarity('staunch defenders'/'pFNM', 'Special').
card_artist('staunch defenders'/'pFNM', 'Mark Poole').
card_number('staunch defenders'/'pFNM', '7').
card_flavor_text('staunch defenders'/'pFNM', '\"Hold your position! Leave doubt for the dying!\"\n—Tahngarth of the Weatherlight').

card_in_set('stoke the flames', 'pFNM').
card_original_type('stoke the flames'/'pFNM', 'Instant').
card_original_text('stoke the flames'/'pFNM', '').
card_first_print('stoke the flames', 'pFNM').
card_image_name('stoke the flames'/'pFNM', 'stoke the flames').
card_uid('stoke the flames'/'pFNM', 'pFNM:Stoke the Flames:stoke the flames').
card_rarity('stoke the flames'/'pFNM', 'Special').
card_artist('stoke the flames'/'pFNM', 'Mathias Kollros').
card_number('stoke the flames'/'pFNM', '175').

card_in_set('stone rain', 'pFNM').
card_original_type('stone rain'/'pFNM', 'Sorcery').
card_original_text('stone rain'/'pFNM', '').
card_image_name('stone rain'/'pFNM', 'stone rain').
card_uid('stone rain'/'pFNM', 'pFNM:Stone Rain:stone rain').
card_rarity('stone rain'/'pFNM', 'Special').
card_artist('stone rain'/'pFNM', 'Daniel Gelon').
card_number('stone rain'/'pFNM', '10').

card_in_set('suspension field', 'pFNM').
card_original_type('suspension field'/'pFNM', 'Enchantment').
card_original_text('suspension field'/'pFNM', '').
card_first_print('suspension field', 'pFNM').
card_image_name('suspension field'/'pFNM', 'suspension field').
card_uid('suspension field'/'pFNM', 'pFNM:Suspension Field:suspension field').
card_rarity('suspension field'/'pFNM', 'Special').
card_artist('suspension field'/'pFNM', 'Seb McKinnon').
card_number('suspension field'/'pFNM', '179').

card_in_set('swords to plowshares', 'pFNM').
card_original_type('swords to plowshares'/'pFNM', 'Instant').
card_original_text('swords to plowshares'/'pFNM', '').
card_image_name('swords to plowshares'/'pFNM', 'swords to plowshares').
card_uid('swords to plowshares'/'pFNM', 'pFNM:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'pFNM', 'Special').
card_artist('swords to plowshares'/'pFNM', 'Jeff A. Menges').
card_number('swords to plowshares'/'pFNM', '12').
card_flavor_text('swords to plowshares'/'pFNM', '\"Peace hath her victories No less renownd than war.\" - John Milton, \"To the Lord General Cromwell\"').

card_in_set('tectonic edge', 'pFNM').
card_original_type('tectonic edge'/'pFNM', 'Land').
card_original_text('tectonic edge'/'pFNM', '').
card_first_print('tectonic edge', 'pFNM').
card_image_name('tectonic edge'/'pFNM', 'tectonic edge').
card_uid('tectonic edge'/'pFNM', 'pFNM:Tectonic Edge:tectonic edge').
card_rarity('tectonic edge'/'pFNM', 'Special').
card_artist('tectonic edge'/'pFNM', 'Eytan Zana').
card_number('tectonic edge'/'pFNM', '142').
card_flavor_text('tectonic edge'/'pFNM', '\"We move because the earth does.\"\n—Bruse Tarl, Goma Fada nomad').

card_in_set('teetering peaks', 'pFNM').
card_original_type('teetering peaks'/'pFNM', 'Land').
card_original_text('teetering peaks'/'pFNM', '').
card_first_print('teetering peaks', 'pFNM').
card_image_name('teetering peaks'/'pFNM', 'teetering peaks').
card_uid('teetering peaks'/'pFNM', 'pFNM:Teetering Peaks:teetering peaks').
card_rarity('teetering peaks'/'pFNM', 'Special').
card_artist('teetering peaks'/'pFNM', 'Kekai Kotaki').
card_number('teetering peaks'/'pFNM', '136').

card_in_set('tendrils of agony', 'pFNM').
card_original_type('tendrils of agony'/'pFNM', 'Sorcery').
card_original_text('tendrils of agony'/'pFNM', '').
card_first_print('tendrils of agony', 'pFNM').
card_image_name('tendrils of agony'/'pFNM', 'tendrils of agony').
card_uid('tendrils of agony'/'pFNM', 'pFNM:Tendrils of Agony:tendrils of agony').
card_rarity('tendrils of agony'/'pFNM', 'Special').
card_artist('tendrils of agony'/'pFNM', 'Volkan Baga').
card_number('tendrils of agony'/'pFNM', '95').

card_in_set('terminate', 'pFNM').
card_original_type('terminate'/'pFNM', 'Instant').
card_original_text('terminate'/'pFNM', '').
card_first_print('terminate', 'pFNM').
card_image_name('terminate'/'pFNM', 'terminate').
card_uid('terminate'/'pFNM', 'pFNM:Terminate:terminate').
card_rarity('terminate'/'pFNM', 'Special').
card_artist('terminate'/'pFNM', 'DiTerlizzi').
card_number('terminate'/'pFNM', '70').
card_flavor_text('terminate'/'pFNM', 'Like his mother before him, Darigaaz gave his life defending Dominaria from the tyranny of self-styled \"gods.\"').

card_in_set('terror', 'pFNM').
card_original_type('terror'/'pFNM', 'Instant').
card_original_text('terror'/'pFNM', '').
card_image_name('terror'/'pFNM', 'terror').
card_uid('terror'/'pFNM', 'pFNM:Terror:terror').
card_rarity('terror'/'pFNM', 'Special').
card_artist('terror'/'pFNM', 'Ron Spencer').
card_number('terror'/'pFNM', '2').

card_in_set('thirst for knowledge', 'pFNM').
card_original_type('thirst for knowledge'/'pFNM', 'Instant').
card_original_text('thirst for knowledge'/'pFNM', '').
card_first_print('thirst for knowledge', 'pFNM').
card_image_name('thirst for knowledge'/'pFNM', 'thirst for knowledge').
card_uid('thirst for knowledge'/'pFNM', 'pFNM:Thirst for Knowledge:thirst for knowledge').
card_rarity('thirst for knowledge'/'pFNM', 'Special').
card_artist('thirst for knowledge'/'pFNM', 'Bud Cook').
card_number('thirst for knowledge'/'pFNM', '100').
card_flavor_text('thirst for knowledge'/'pFNM', 'Raissa transformed herself into a vessel for the Watchword Codex, allowing the arcane lore to flow through her body.').

card_in_set('tidehollow sculler', 'pFNM').
card_original_type('tidehollow sculler'/'pFNM', 'Artifact Creature — Zombie').
card_original_text('tidehollow sculler'/'pFNM', '').
card_first_print('tidehollow sculler', 'pFNM').
card_image_name('tidehollow sculler'/'pFNM', 'tidehollow sculler').
card_uid('tidehollow sculler'/'pFNM', 'pFNM:Tidehollow Sculler:tidehollow sculler').
card_rarity('tidehollow sculler'/'pFNM', 'Special').
card_artist('tidehollow sculler'/'pFNM', 'Carl Critchlow').
card_number('tidehollow sculler'/'pFNM', '116').

card_in_set('tormented hero', 'pFNM').
card_original_type('tormented hero'/'pFNM', 'Creature — Human Warrior').
card_original_text('tormented hero'/'pFNM', '').
card_first_print('tormented hero', 'pFNM').
card_image_name('tormented hero'/'pFNM', 'tormented hero').
card_uid('tormented hero'/'pFNM', 'pFNM:Tormented Hero:tormented hero').
card_rarity('tormented hero'/'pFNM', 'Special').
card_artist('tormented hero'/'pFNM', 'Wesley Burt').
card_number('tormented hero'/'pFNM', '168').

card_in_set('tormod\'s crypt', 'pFNM').
card_original_type('tormod\'s crypt'/'pFNM', 'Artifact').
card_original_text('tormod\'s crypt'/'pFNM', '').
card_image_name('tormod\'s crypt'/'pFNM', 'tormod\'s crypt').
card_uid('tormod\'s crypt'/'pFNM', 'pFNM:Tormod\'s Crypt:tormod\'s crypt').
card_rarity('tormod\'s crypt'/'pFNM', 'Special').
card_artist('tormod\'s crypt'/'pFNM', 'Lars Grant-West').
card_number('tormod\'s crypt'/'pFNM', '93').
card_flavor_text('tormod\'s crypt'/'pFNM', 'Dominaria\'s most extravagant crypt nevertheless holds an empty grave.').

card_in_set('treetop village', 'pFNM').
card_original_type('treetop village'/'pFNM', 'Land').
card_original_text('treetop village'/'pFNM', '').
card_image_name('treetop village'/'pFNM', 'treetop village').
card_uid('treetop village'/'pFNM', 'pFNM:Treetop Village:treetop village').
card_rarity('treetop village'/'pFNM', 'Special').
card_artist('treetop village'/'pFNM', 'Anthony S. Waters').
card_number('treetop village'/'pFNM', '50').

card_in_set('volcanic geyser', 'pFNM').
card_original_type('volcanic geyser'/'pFNM', 'Instant').
card_original_text('volcanic geyser'/'pFNM', '').
card_image_name('volcanic geyser'/'pFNM', 'volcanic geyser').
card_uid('volcanic geyser'/'pFNM', 'pFNM:Volcanic Geyser:volcanic geyser').
card_rarity('volcanic geyser'/'pFNM', 'Special').
card_artist('volcanic geyser'/'pFNM', 'David O\'Connor').
card_number('volcanic geyser'/'pFNM', '4').
card_flavor_text('volcanic geyser'/'pFNM', 'My thunder comes before the lightning; my lightning comes before the clouds; my rain dries all the land it touches. What am I? —Femeref riddle').

card_in_set('wall of blossoms', 'pFNM').
card_original_type('wall of blossoms'/'pFNM', 'Creature — Plant Wall').
card_original_text('wall of blossoms'/'pFNM', '').
card_image_name('wall of blossoms'/'pFNM', 'wall of blossoms').
card_uid('wall of blossoms'/'pFNM', 'pFNM:Wall of Blossoms:wall of blossoms').
card_rarity('wall of blossoms'/'pFNM', 'Special').
card_artist('wall of blossoms'/'pFNM', 'Heather Hudson').
card_number('wall of blossoms'/'pFNM', '23').
card_flavor_text('wall of blossoms'/'pFNM', '\"Everything in Nature contains all the powers of Nature.\" - Ralph Waldo Emerson, \"Compensation\"').

card_in_set('wall of omens', 'pFNM').
card_original_type('wall of omens'/'pFNM', 'Creature — Wall').
card_original_text('wall of omens'/'pFNM', '').
card_first_print('wall of omens', 'pFNM').
card_image_name('wall of omens'/'pFNM', 'wall of omens').
card_uid('wall of omens'/'pFNM', 'pFNM:Wall of Omens:wall of omens').
card_rarity('wall of omens'/'pFNM', 'Special').
card_artist('wall of omens'/'pFNM', 'Howard Lyon').
card_number('wall of omens'/'pFNM', '130').
card_flavor_text('wall of omens'/'pFNM', '\"I search for a vision of Zendikar that does not include the Eldrazi.\"\n—Expedition journal entry').

card_in_set('wall of roots', 'pFNM').
card_original_type('wall of roots'/'pFNM', 'Creature — Plant Wall').
card_original_text('wall of roots'/'pFNM', '').
card_image_name('wall of roots'/'pFNM', 'wall of roots').
card_uid('wall of roots'/'pFNM', 'pFNM:Wall of Roots:wall of roots').
card_rarity('wall of roots'/'pFNM', 'Special').
card_artist('wall of roots'/'pFNM', 'Matt Stewart').
card_number('wall of roots'/'pFNM', '98').
card_flavor_text('wall of roots'/'pFNM', 'Root systems in Mwonvuli twist through currents of mana rather than soil.').

card_in_set('warleader\'s helix', 'pFNM').
card_original_type('warleader\'s helix'/'pFNM', 'Instant').
card_original_text('warleader\'s helix'/'pFNM', '').
card_first_print('warleader\'s helix', 'pFNM').
card_image_name('warleader\'s helix'/'pFNM', 'warleader\'s helix').
card_uid('warleader\'s helix'/'pFNM', 'pFNM:Warleader\'s Helix:warleader\'s helix').
card_rarity('warleader\'s helix'/'pFNM', 'Special').
card_artist('warleader\'s helix'/'pFNM', 'Wesley Burt').
card_number('warleader\'s helix'/'pFNM', '164').
card_flavor_text('warleader\'s helix'/'pFNM', '\"There is no time to remedy our enemies\' ignorance. Blast it out of them.\"\n—Aurelia').

card_in_set('watchwolf', 'pFNM').
card_original_type('watchwolf'/'pFNM', 'Creature — Wolf').
card_original_text('watchwolf'/'pFNM', '').
card_first_print('watchwolf', 'pFNM').
card_image_name('watchwolf'/'pFNM', 'watchwolf').
card_uid('watchwolf'/'pFNM', 'pFNM:Watchwolf:watchwolf').
card_rarity('watchwolf'/'pFNM', 'Special').
card_artist('watchwolf'/'pFNM', 'Dave Kendall').
card_number('watchwolf'/'pFNM', '112').
card_flavor_text('watchwolf'/'pFNM', 'Only in Ravnica do the wolves watch the flock.').

card_in_set('whipcorder', 'pFNM').
card_original_type('whipcorder'/'pFNM', 'Creature — Human Soldier Rebel').
card_original_text('whipcorder'/'pFNM', '').
card_first_print('whipcorder', 'pFNM').
card_image_name('whipcorder'/'pFNM', 'whipcorder').
card_uid('whipcorder'/'pFNM', 'pFNM:Whipcorder:whipcorder').
card_rarity('whipcorder'/'pFNM', 'Special').
card_artist('whipcorder'/'pFNM', 'Ron Spencer').
card_number('whipcorder'/'pFNM', '40').
card_flavor_text('whipcorder'/'pFNM', 'His bolas whirl like galaxies, but it\'s his enemies who see stars.').

card_in_set('white knight', 'pFNM').
card_original_type('white knight'/'pFNM', 'Creature — Human Knight').
card_original_text('white knight'/'pFNM', '').
card_image_name('white knight'/'pFNM', 'white knight').
card_uid('white knight'/'pFNM', 'pFNM:White Knight:white knight').
card_rarity('white knight'/'pFNM', 'Special').
card_artist('white knight'/'pFNM', 'Daniel Gelon').
card_number('white knight'/'pFNM', '30').
card_flavor_text('white knight'/'pFNM', '\"When good men die their goodness does not perish, / But lives though they are gone.\"\n—Euripides, Termenidae, trans. Morgan').

card_in_set('wild mongrel', 'pFNM').
card_original_type('wild mongrel'/'pFNM', 'Creature — Hound').
card_original_text('wild mongrel'/'pFNM', '').
card_first_print('wild mongrel', 'pFNM').
card_image_name('wild mongrel'/'pFNM', 'wild mongrel').
card_uid('wild mongrel'/'pFNM', 'pFNM:Wild Mongrel:wild mongrel').
card_rarity('wild mongrel'/'pFNM', 'Special').
card_artist('wild mongrel'/'pFNM', 'Anthony S. Waters').
card_number('wild mongrel'/'pFNM', '73').
card_flavor_text('wild mongrel'/'pFNM', 'It teaches you to play dead.').

card_in_set('wild nacatl', 'pFNM').
card_original_type('wild nacatl'/'pFNM', 'Creature — Cat Warrior').
card_original_text('wild nacatl'/'pFNM', '').
card_first_print('wild nacatl', 'pFNM').
card_image_name('wild nacatl'/'pFNM', 'wild nacatl').
card_uid('wild nacatl'/'pFNM', 'pFNM:Wild Nacatl:wild nacatl').
card_rarity('wild nacatl'/'pFNM', 'Special').
card_artist('wild nacatl'/'pFNM', 'Kev Walker').
card_number('wild nacatl'/'pFNM', '127').
card_flavor_text('wild nacatl'/'pFNM', '\"The Cloud Nacatl sit and think, a bunch of soft paws. We are the Claws of Marisi, stalking, pouncing, drawing blood.\"').

card_in_set('willbender', 'pFNM').
card_original_type('willbender'/'pFNM', 'Creature — Human Wizard').
card_original_text('willbender'/'pFNM', '').
card_first_print('willbender', 'pFNM').
card_image_name('willbender'/'pFNM', 'willbender').
card_uid('willbender'/'pFNM', 'pFNM:Willbender:willbender').
card_rarity('willbender'/'pFNM', 'Special').
card_artist('willbender'/'pFNM', 'Eric Peterson').
card_number('willbender'/'pFNM', '44').

card_in_set('wing shards', 'pFNM').
card_original_type('wing shards'/'pFNM', 'Instant').
card_original_text('wing shards'/'pFNM', '').
card_first_print('wing shards', 'pFNM').
card_image_name('wing shards'/'pFNM', 'wing shards').
card_uid('wing shards'/'pFNM', 'pFNM:Wing Shards:wing shards').
card_rarity('wing shards'/'pFNM', 'Special').
card_artist('wing shards'/'pFNM', 'Daren Bader').
card_number('wing shards'/'pFNM', '88').

card_in_set('withered wretch', 'pFNM').
card_original_type('withered wretch'/'pFNM', 'Creature — Zombie Cleric').
card_original_text('withered wretch'/'pFNM', '').
card_first_print('withered wretch', 'pFNM').
card_image_name('withered wretch'/'pFNM', 'withered wretch').
card_uid('withered wretch'/'pFNM', 'pFNM:Withered Wretch:withered wretch').
card_rarity('withered wretch'/'pFNM', 'Special').
card_artist('withered wretch'/'pFNM', 'Tim Hildebrandt').
card_number('withered wretch'/'pFNM', '43').
card_flavor_text('withered wretch'/'pFNM', 'Once it consecrated the dead. Now it desecrates them.').

card_in_set('wonder', 'pFNM').
card_original_type('wonder'/'pFNM', 'Creature — Incarnation').
card_original_text('wonder'/'pFNM', '').
card_first_print('wonder', 'pFNM').
card_image_name('wonder'/'pFNM', 'wonder').
card_uid('wonder'/'pFNM', 'pFNM:Wonder:wonder').
card_rarity('wonder'/'pFNM', 'Special').
card_artist('wonder'/'pFNM', 'Rebecca Guay').
card_number('wonder'/'pFNM', '84').
card_flavor_text('wonder'/'pFNM', '\"The awestruck birds gazed at Wonder. Slowly, timidly, they rose into the air.\"\n—Scroll of Beginnings').

card_in_set('wren\'s run vanquisher', 'pFNM').
card_original_type('wren\'s run vanquisher'/'pFNM', 'Creature — Elf Warrior').
card_original_text('wren\'s run vanquisher'/'pFNM', '').
card_first_print('wren\'s run vanquisher', 'pFNM').
card_image_name('wren\'s run vanquisher'/'pFNM', 'wren\'s run vanquisher').
card_uid('wren\'s run vanquisher'/'pFNM', 'pFNM:Wren\'s Run Vanquisher:wren\'s run vanquisher').
card_rarity('wren\'s run vanquisher'/'pFNM', 'Special').
card_artist('wren\'s run vanquisher'/'pFNM', 'Jesper Ejsing').
card_number('wren\'s run vanquisher'/'pFNM', '108').
