% Duel Decks: Elspeth vs. Kiora

set('DDO').
set_name('DDO', 'Duel Decks: Elspeth vs. Kiora').
set_release_date('DDO', '2015-02-27').
set_border('DDO', 'black').
set_type('DDO', 'duel deck').

card_in_set('accumulated knowledge', 'DDO').
card_original_type('accumulated knowledge'/'DDO', 'Instant').
card_original_text('accumulated knowledge'/'DDO', 'Draw a card, then draw cards equal to the number of cards named Accumulated Knowledge in all graveyards.').
card_image_name('accumulated knowledge'/'DDO', 'accumulated knowledge').
card_uid('accumulated knowledge'/'DDO', 'DDO:Accumulated Knowledge:accumulated knowledge').
card_rarity('accumulated knowledge'/'DDO', 'Common').
card_artist('accumulated knowledge'/'DDO', 'David Palumbo').
card_number('accumulated knowledge'/'DDO', '35').
card_flavor_text('accumulated knowledge'/'DDO', '\"Learn not only from the failures of the past, but also from its triumphs.\"\n—Isperia').
card_multiverse_id('accumulated knowledge'/'DDO', '394351').

card_in_set('ætherize', 'DDO').
card_original_type('ætherize'/'DDO', 'Instant').
card_original_text('ætherize'/'DDO', 'Return all attacking creatures to their owner\'s hand.').
card_image_name('ætherize'/'DDO', 'aetherize').
card_uid('ætherize'/'DDO', 'DDO:Ætherize:aetherize').
card_rarity('ætherize'/'DDO', 'Uncommon').
card_artist('ætherize'/'DDO', 'Ryan Barger').
card_number('ætherize'/'DDO', '36').
card_flavor_text('ætherize'/'DDO', '\"You can come back once you\'ve learned some manners—and figured out how to reconstitute your physical forms.\"').
card_multiverse_id('ætherize'/'DDO', '394352').

card_in_set('banisher priest', 'DDO').
card_original_type('banisher priest'/'DDO', 'Creature — Human Cleric').
card_original_text('banisher priest'/'DDO', 'When Banisher Priest enters the battlefield, exile target creature an opponent controls until Banisher Priest leaves the battlefield. (That creature returns under its owner\'s control.)').
card_image_name('banisher priest'/'DDO', 'banisher priest').
card_uid('banisher priest'/'DDO', 'DDO:Banisher Priest:banisher priest').
card_rarity('banisher priest'/'DDO', 'Uncommon').
card_artist('banisher priest'/'DDO', 'Willian Murai').
card_number('banisher priest'/'DDO', '2').
card_flavor_text('banisher priest'/'DDO', '\"Oathbreaker, I cast you out!\"').
card_multiverse_id('banisher priest'/'DDO', '394353').

card_in_set('captain of the watch', 'DDO').
card_original_type('captain of the watch'/'DDO', 'Creature — Human Soldier').
card_original_text('captain of the watch'/'DDO', 'Vigilance\nOther Soldier creatures you control get +1/+1 and have vigilance.\nWhen Captain of the Watch enters the battlefield, put three 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('captain of the watch'/'DDO', 'captain of the watch').
card_uid('captain of the watch'/'DDO', 'DDO:Captain of the Watch:captain of the watch').
card_rarity('captain of the watch'/'DDO', 'Rare').
card_artist('captain of the watch'/'DDO', 'Greg Staples').
card_number('captain of the watch'/'DDO', '3').
card_multiverse_id('captain of the watch'/'DDO', '394354').

card_in_set('celestial flare', 'DDO').
card_original_type('celestial flare'/'DDO', 'Instant').
card_original_text('celestial flare'/'DDO', 'Target player sacrifices an attacking or blocking creature.').
card_image_name('celestial flare'/'DDO', 'celestial flare').
card_uid('celestial flare'/'DDO', 'DDO:Celestial Flare:celestial flare').
card_rarity('celestial flare'/'DDO', 'Common').
card_artist('celestial flare'/'DDO', 'Clint Cearley').
card_number('celestial flare'/'DDO', '4').
card_flavor_text('celestial flare'/'DDO', '\"You were defeated the moment you declared your aggression.\"\n—Gideon Jura').
card_multiverse_id('celestial flare'/'DDO', '394355').

card_in_set('coiling oracle', 'DDO').
card_original_type('coiling oracle'/'DDO', 'Creature — Snake Elf Druid').
card_original_text('coiling oracle'/'DDO', 'When Coiling Oracle enters the battlefield, reveal the top card of your library. If it\'s a land card, put it onto the battlefield. Otherwise, put that card into your hand.').
card_image_name('coiling oracle'/'DDO', 'coiling oracle').
card_uid('coiling oracle'/'DDO', 'DDO:Coiling Oracle:coiling oracle').
card_rarity('coiling oracle'/'DDO', 'Common').
card_artist('coiling oracle'/'DDO', 'Mark Zug').
card_number('coiling oracle'/'DDO', '51').
card_flavor_text('coiling oracle'/'DDO', '\"Nature was slowly heading in the right direction, but we know how to achieve faster results.\"\n—Vorel of the Hull Clade').
card_multiverse_id('coiling oracle'/'DDO', '394356').

card_in_set('court street denizen', 'DDO').
card_original_type('court street denizen'/'DDO', 'Creature — Human Soldier').
card_original_text('court street denizen'/'DDO', 'Whenever another white creature enters the battlefield under your control, tap target creature an opponent controls.').
card_image_name('court street denizen'/'DDO', 'court street denizen').
card_uid('court street denizen'/'DDO', 'DDO:Court Street Denizen:court street denizen').
card_rarity('court street denizen'/'DDO', 'Common').
card_artist('court street denizen'/'DDO', 'Volkan Baga').
card_number('court street denizen'/'DDO', '5').
card_flavor_text('court street denizen'/'DDO', '\"The Boros fight for justice. The Azorius fight for law. I hold the line between, and make sure the people are given both.\"').
card_multiverse_id('court street denizen'/'DDO', '394357').

card_in_set('dauntless onslaught', 'DDO').
card_original_type('dauntless onslaught'/'DDO', 'Instant').
card_original_text('dauntless onslaught'/'DDO', 'Up to two target creatures each get +2/+2 until end of turn.').
card_image_name('dauntless onslaught'/'DDO', 'dauntless onslaught').
card_uid('dauntless onslaught'/'DDO', 'DDO:Dauntless Onslaught:dauntless onslaught').
card_rarity('dauntless onslaught'/'DDO', 'Uncommon').
card_artist('dauntless onslaught'/'DDO', 'Peter Mohrbacher').
card_number('dauntless onslaught'/'DDO', '6').
card_flavor_text('dauntless onslaught'/'DDO', '\"The people of Akros must learn from our leonin adversaries. If we match their staunch ferocity with our superior faith, we cannot fail.\"\n—Cymede, queen of Akros').
card_multiverse_id('dauntless onslaught'/'DDO', '394358').

card_in_set('decree of justice', 'DDO').
card_original_type('decree of justice'/'DDO', 'Sorcery').
card_original_text('decree of justice'/'DDO', 'Put X 4/4 white Angel creature tokens with flying onto the battlefield.\nCycling {2}{W} ({2}{W}, Discard this card: Draw a card.)\nWhen you cycle Decree of Justice, you may pay {X}. If you do, put X 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('decree of justice'/'DDO', 'decree of justice').
card_uid('decree of justice'/'DDO', 'DDO:Decree of Justice:decree of justice').
card_rarity('decree of justice'/'DDO', 'Rare').
card_artist('decree of justice'/'DDO', 'Lius Lasahido').
card_number('decree of justice'/'DDO', '7').
card_multiverse_id('decree of justice'/'DDO', '394359').

card_in_set('dictate of heliod', 'DDO').
card_original_type('dictate of heliod'/'DDO', 'Enchantment').
card_original_text('dictate of heliod'/'DDO', 'Flash\nCreatures you control get +2/+2.').
card_image_name('dictate of heliod'/'DDO', 'dictate of heliod').
card_uid('dictate of heliod'/'DDO', 'DDO:Dictate of Heliod:dictate of heliod').
card_rarity('dictate of heliod'/'DDO', 'Rare').
card_artist('dictate of heliod'/'DDO', 'Terese Nielsen').
card_number('dictate of heliod'/'DDO', '8').
card_flavor_text('dictate of heliod'/'DDO', '\"In our war Heliod gave mortals some favor, yet other times he withheld aid. Are we still no more than game pieces to him?\"\n—Polyxene the Doubter').
card_multiverse_id('dictate of heliod'/'DDO', '394360').

card_in_set('elspeth, sun\'s champion', 'DDO').
card_original_type('elspeth, sun\'s champion'/'DDO', 'Planeswalker — Elspeth').
card_original_text('elspeth, sun\'s champion'/'DDO', '+1: Put three 1/1 white Soldier creature tokens onto the battlefield.\n−3: Destroy all creatures with power 4 or greater.\n−7: You get an emblem with \"Creatures you control get +2/+2 and have flying.\"').
card_image_name('elspeth, sun\'s champion'/'DDO', 'elspeth, sun\'s champion').
card_uid('elspeth, sun\'s champion'/'DDO', 'DDO:Elspeth, Sun\'s Champion:elspeth, sun\'s champion').
card_rarity('elspeth, sun\'s champion'/'DDO', 'Mythic Rare').
card_artist('elspeth, sun\'s champion'/'DDO', 'Tyler Jacobson').
card_number('elspeth, sun\'s champion'/'DDO', '1').
card_multiverse_id('elspeth, sun\'s champion'/'DDO', '394361').

card_in_set('evolving wilds', 'DDO').
card_original_type('evolving wilds'/'DDO', 'Land').
card_original_text('evolving wilds'/'DDO', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'DDO', 'evolving wilds').
card_uid('evolving wilds'/'DDO', 'DDO:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'DDO', 'Common').
card_artist('evolving wilds'/'DDO', 'Steven Belledin').
card_number('evolving wilds'/'DDO', '58').
card_flavor_text('evolving wilds'/'DDO', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'DDO', '394362').

card_in_set('explore', 'DDO').
card_original_type('explore'/'DDO', 'Sorcery').
card_original_text('explore'/'DDO', 'You may play an additional land this turn.\nDraw a card.').
card_image_name('explore'/'DDO', 'explore').
card_uid('explore'/'DDO', 'DDO:Explore:explore').
card_rarity('explore'/'DDO', 'Common').
card_artist('explore'/'DDO', 'Johann Bodin').
card_number('explore'/'DDO', '45').
card_flavor_text('explore'/'DDO', '\"I will never run out of new oceans to explore or new sea-dwellers to embrace as my own.\"\n—Kiora').
card_multiverse_id('explore'/'DDO', '394363').

card_in_set('explosive vegetation', 'DDO').
card_original_type('explosive vegetation'/'DDO', 'Sorcery').
card_original_text('explosive vegetation'/'DDO', 'Search your library for up to two basic land cards and put them onto the battlefield tapped. Then shuffle your library.').
card_image_name('explosive vegetation'/'DDO', 'explosive vegetation').
card_uid('explosive vegetation'/'DDO', 'DDO:Explosive Vegetation:explosive vegetation').
card_rarity('explosive vegetation'/'DDO', 'Uncommon').
card_artist('explosive vegetation'/'DDO', 'John Avon').
card_number('explosive vegetation'/'DDO', '46').
card_flavor_text('explosive vegetation'/'DDO', 'Torching Krosa would be pointless. It grows faster than it burns.').
card_multiverse_id('explosive vegetation'/'DDO', '394364').

card_in_set('forest', 'DDO').
card_original_type('forest'/'DDO', 'Basic Land — Forest').
card_original_text('forest'/'DDO', 'G').
card_image_name('forest'/'DDO', 'forest1').
card_uid('forest'/'DDO', 'DDO:Forest:forest1').
card_rarity('forest'/'DDO', 'Basic Land').
card_artist('forest'/'DDO', 'Steven Belledin').
card_number('forest'/'DDO', '63').
card_multiverse_id('forest'/'DDO', '394366').

card_in_set('forest', 'DDO').
card_original_type('forest'/'DDO', 'Basic Land — Forest').
card_original_text('forest'/'DDO', 'G').
card_image_name('forest'/'DDO', 'forest2').
card_uid('forest'/'DDO', 'DDO:Forest:forest2').
card_rarity('forest'/'DDO', 'Basic Land').
card_artist('forest'/'DDO', 'Adam Paquette').
card_number('forest'/'DDO', '64').
card_multiverse_id('forest'/'DDO', '394365').

card_in_set('forest', 'DDO').
card_original_type('forest'/'DDO', 'Basic Land — Forest').
card_original_text('forest'/'DDO', 'G').
card_image_name('forest'/'DDO', 'forest3').
card_uid('forest'/'DDO', 'DDO:Forest:forest3').
card_rarity('forest'/'DDO', 'Basic Land').
card_artist('forest'/'DDO', 'Raoul Vitale').
card_number('forest'/'DDO', '65').
card_multiverse_id('forest'/'DDO', '394367').

card_in_set('gempalm avenger', 'DDO').
card_original_type('gempalm avenger'/'DDO', 'Creature — Human Soldier').
card_original_text('gempalm avenger'/'DDO', 'Cycling {2}{W} ({2}{W}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Avenger, Soldier creatures get +1/+1 and gain first strike until end of turn.').
card_image_name('gempalm avenger'/'DDO', 'gempalm avenger').
card_uid('gempalm avenger'/'DDO', 'DDO:Gempalm Avenger:gempalm avenger').
card_rarity('gempalm avenger'/'DDO', 'Common').
card_artist('gempalm avenger'/'DDO', 'Tim Hildebrandt').
card_number('gempalm avenger'/'DDO', '9').
card_multiverse_id('gempalm avenger'/'DDO', '394368').

card_in_set('grazing gladehart', 'DDO').
card_original_type('grazing gladehart'/'DDO', 'Creature — Antelope').
card_original_text('grazing gladehart'/'DDO', 'Landfall — Whenever a land enters the battlefield under your control, you may gain 2 life.').
card_image_name('grazing gladehart'/'DDO', 'grazing gladehart').
card_uid('grazing gladehart'/'DDO', 'DDO:Grazing Gladehart:grazing gladehart').
card_rarity('grazing gladehart'/'DDO', 'Common').
card_artist('grazing gladehart'/'DDO', 'Ryan Pancoast').
card_number('grazing gladehart'/'DDO', '47').
card_flavor_text('grazing gladehart'/'DDO', '\"Don\'t be fooled. If it were as docile as it looks, it would\'ve died off long ago.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('grazing gladehart'/'DDO', '394369').

card_in_set('gustcloak harrier', 'DDO').
card_original_type('gustcloak harrier'/'DDO', 'Creature — Bird Soldier').
card_original_text('gustcloak harrier'/'DDO', 'Flying\nWhenever Gustcloak Harrier becomes blocked, you may untap it and remove it from combat.').
card_image_name('gustcloak harrier'/'DDO', 'gustcloak harrier').
card_uid('gustcloak harrier'/'DDO', 'DDO:Gustcloak Harrier:gustcloak harrier').
card_rarity('gustcloak harrier'/'DDO', 'Common').
card_artist('gustcloak harrier'/'DDO', 'Dan Frazier').
card_number('gustcloak harrier'/'DDO', '10').
card_flavor_text('gustcloak harrier'/'DDO', 'Banking steeply, the aven streaked toward the ground—and vanished.').
card_multiverse_id('gustcloak harrier'/'DDO', '394370').

card_in_set('gustcloak savior', 'DDO').
card_original_type('gustcloak savior'/'DDO', 'Creature — Bird Soldier').
card_original_text('gustcloak savior'/'DDO', 'Flying\nWhenever a creature you control becomes blocked, you may untap that creature and remove it from combat.').
card_image_name('gustcloak savior'/'DDO', 'gustcloak savior').
card_uid('gustcloak savior'/'DDO', 'DDO:Gustcloak Savior:gustcloak savior').
card_rarity('gustcloak savior'/'DDO', 'Rare').
card_artist('gustcloak savior'/'DDO', 'Jim Nelson').
card_number('gustcloak savior'/'DDO', '11').
card_flavor_text('gustcloak savior'/'DDO', '\"Our death-arrows flew in high arcs towards the aven. And then . . . nothing.\"\n—Coliseum guard').
card_multiverse_id('gustcloak savior'/'DDO', '394371').

card_in_set('gustcloak sentinel', 'DDO').
card_original_type('gustcloak sentinel'/'DDO', 'Creature — Human Soldier').
card_original_text('gustcloak sentinel'/'DDO', 'Whenever Gustcloak Sentinel becomes blocked, you may untap it and remove it from combat.').
card_image_name('gustcloak sentinel'/'DDO', 'gustcloak sentinel').
card_uid('gustcloak sentinel'/'DDO', 'DDO:Gustcloak Sentinel:gustcloak sentinel').
card_rarity('gustcloak sentinel'/'DDO', 'Uncommon').
card_artist('gustcloak sentinel'/'DDO', 'Mark Zug').
card_number('gustcloak sentinel'/'DDO', '12').
card_flavor_text('gustcloak sentinel'/'DDO', 'Entire platoons have mysteriously vanished from battle, leaving enemy weapons to slice through empty air.').
card_multiverse_id('gustcloak sentinel'/'DDO', '394372').

card_in_set('gustcloak skirmisher', 'DDO').
card_original_type('gustcloak skirmisher'/'DDO', 'Creature — Bird Soldier').
card_original_text('gustcloak skirmisher'/'DDO', 'Flying\nWhenever Gustcloak Skirmisher becomes blocked, you may untap it and remove it from combat.').
card_image_name('gustcloak skirmisher'/'DDO', 'gustcloak skirmisher').
card_uid('gustcloak skirmisher'/'DDO', 'DDO:Gustcloak Skirmisher:gustcloak skirmisher').
card_rarity('gustcloak skirmisher'/'DDO', 'Uncommon').
card_artist('gustcloak skirmisher'/'DDO', 'Dan Frazier').
card_number('gustcloak skirmisher'/'DDO', '13').
card_flavor_text('gustcloak skirmisher'/'DDO', 'They\'re trained in the art of pressing their luck.').
card_multiverse_id('gustcloak skirmisher'/'DDO', '394373').

card_in_set('icatian javelineers', 'DDO').
card_original_type('icatian javelineers'/'DDO', 'Creature — Human Soldier').
card_original_text('icatian javelineers'/'DDO', 'Icatian Javelineers enters the battlefield with a javelin counter on it.\n{T}, Remove a javelin counter from Icatian Javelineers: Icatian Javelineers deals 1 damage to target creature or player.').
card_image_name('icatian javelineers'/'DDO', 'icatian javelineers').
card_uid('icatian javelineers'/'DDO', 'DDO:Icatian Javelineers:icatian javelineers').
card_rarity('icatian javelineers'/'DDO', 'Common').
card_artist('icatian javelineers'/'DDO', 'Michael Phillippi').
card_number('icatian javelineers'/'DDO', '14').
card_multiverse_id('icatian javelineers'/'DDO', '394374').

card_in_set('inkwell leviathan', 'DDO').
card_original_type('inkwell leviathan'/'DDO', 'Artifact Creature — Leviathan').
card_original_text('inkwell leviathan'/'DDO', 'Trample\nIslandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nShroud (This creature can\'t be the target of spells or abilities.)').
card_image_name('inkwell leviathan'/'DDO', 'inkwell leviathan').
card_uid('inkwell leviathan'/'DDO', 'DDO:Inkwell Leviathan:inkwell leviathan').
card_rarity('inkwell leviathan'/'DDO', 'Rare').
card_artist('inkwell leviathan'/'DDO', 'Anthony Francisco').
card_number('inkwell leviathan'/'DDO', '37').
card_flavor_text('inkwell leviathan'/'DDO', '\"Into its maw went the seventh sea, never to be seen again while the world remains.\"\n—Esper fable').
card_multiverse_id('inkwell leviathan'/'DDO', '394375').

card_in_set('island', 'DDO').
card_original_type('island'/'DDO', 'Basic Land — Island').
card_original_text('island'/'DDO', 'U').
card_image_name('island'/'DDO', 'island1').
card_uid('island'/'DDO', 'DDO:Island:island1').
card_rarity('island'/'DDO', 'Basic Land').
card_artist('island'/'DDO', 'Steven Belledin').
card_number('island'/'DDO', '60').
card_multiverse_id('island'/'DDO', '394377').

card_in_set('island', 'DDO').
card_original_type('island'/'DDO', 'Basic Land — Island').
card_original_text('island'/'DDO', 'U').
card_image_name('island'/'DDO', 'island2').
card_uid('island'/'DDO', 'DDO:Island:island2').
card_rarity('island'/'DDO', 'Basic Land').
card_artist('island'/'DDO', 'Adam Paquette').
card_number('island'/'DDO', '61').
card_multiverse_id('island'/'DDO', '394378').

card_in_set('island', 'DDO').
card_original_type('island'/'DDO', 'Basic Land — Island').
card_original_text('island'/'DDO', 'U').
card_image_name('island'/'DDO', 'island3').
card_uid('island'/'DDO', 'DDO:Island:island3').
card_rarity('island'/'DDO', 'Basic Land').
card_artist('island'/'DDO', 'Raoul Vitale').
card_number('island'/'DDO', '62').
card_multiverse_id('island'/'DDO', '394376').

card_in_set('kinsbaile skirmisher', 'DDO').
card_original_type('kinsbaile skirmisher'/'DDO', 'Creature — Kithkin Soldier').
card_original_text('kinsbaile skirmisher'/'DDO', 'When Kinsbaile Skirmisher enters the battlefield, target creature gets +1/+1 until end of turn.').
card_image_name('kinsbaile skirmisher'/'DDO', 'kinsbaile skirmisher').
card_uid('kinsbaile skirmisher'/'DDO', 'DDO:Kinsbaile Skirmisher:kinsbaile skirmisher').
card_rarity('kinsbaile skirmisher'/'DDO', 'Common').
card_artist('kinsbaile skirmisher'/'DDO', 'Thomas Denmark').
card_number('kinsbaile skirmisher'/'DDO', '15').
card_flavor_text('kinsbaile skirmisher'/'DDO', '\"If a boggart even dares breathe near one of my kin, I\'ll know. And I\'ll not be happy.\"').
card_multiverse_id('kinsbaile skirmisher'/'DDO', '394379').

card_in_set('kiora\'s follower', 'DDO').
card_original_type('kiora\'s follower'/'DDO', 'Creature — Merfolk').
card_original_text('kiora\'s follower'/'DDO', '{T}: Untap another target permanent.').
card_image_name('kiora\'s follower'/'DDO', 'kiora\'s follower').
card_uid('kiora\'s follower'/'DDO', 'DDO:Kiora\'s Follower:kiora\'s follower').
card_rarity('kiora\'s follower'/'DDO', 'Uncommon').
card_artist('kiora\'s follower'/'DDO', 'Eric Deschamps').
card_number('kiora\'s follower'/'DDO', '52').
card_flavor_text('kiora\'s follower'/'DDO', '\"She may call herself Kiora but I believe she is Thassa, the embodiment of the sea and empress of the depths.\"').
card_multiverse_id('kiora\'s follower'/'DDO', '394381').

card_in_set('kiora, the crashing wave', 'DDO').
card_original_type('kiora, the crashing wave'/'DDO', 'Planeswalker — Kiora').
card_original_text('kiora, the crashing wave'/'DDO', '+1: Until your next turn, prevent all damage that would be dealt to and dealt by target permanent an opponent controls.\n−1: Draw a card. You may play an additional land this turn.\n−5: You get an emblem with \"At the beginning of your end step, put a 9/9 blue Kraken creature token onto the battlefield.\"').
card_image_name('kiora, the crashing wave'/'DDO', 'kiora, the crashing wave').
card_uid('kiora, the crashing wave'/'DDO', 'DDO:Kiora, the Crashing Wave:kiora, the crashing wave').
card_rarity('kiora, the crashing wave'/'DDO', 'Mythic Rare').
card_artist('kiora, the crashing wave'/'DDO', 'Tyler Jacobson').
card_number('kiora, the crashing wave'/'DDO', '34').
card_multiverse_id('kiora, the crashing wave'/'DDO', '394380').

card_in_set('kor skyfisher', 'DDO').
card_original_type('kor skyfisher'/'DDO', 'Creature — Kor Soldier').
card_original_text('kor skyfisher'/'DDO', 'Flying\nWhen Kor Skyfisher enters the battlefield, return a permanent you control to its owner\'s hand.').
card_image_name('kor skyfisher'/'DDO', 'kor skyfisher').
card_uid('kor skyfisher'/'DDO', 'DDO:Kor Skyfisher:kor skyfisher').
card_rarity('kor skyfisher'/'DDO', 'Common').
card_artist('kor skyfisher'/'DDO', 'Dan Scott').
card_number('kor skyfisher'/'DDO', '16').
card_flavor_text('kor skyfisher'/'DDO', '\"Sometimes I snare the unexpected, but I know its purpose will be revealed in time.\"').
card_multiverse_id('kor skyfisher'/'DDO', '394382').

card_in_set('kraken', 'DDO').
card_original_type('kraken'/'DDO', 'Token Creature — Kraken').
card_original_text('kraken'/'DDO', '').
card_first_print('kraken', 'DDO').
card_image_name('kraken'/'DDO', 'kraken').
card_uid('kraken'/'DDO', 'DDO:Kraken:kraken').
card_rarity('kraken'/'DDO', 'Common').
card_artist('kraken'/'DDO', 'Dan Scott').
card_number('kraken'/'DDO', '67').
card_multiverse_id('kraken'/'DDO', '394383').

card_in_set('lorescale coatl', 'DDO').
card_original_type('lorescale coatl'/'DDO', 'Creature — Snake').
card_original_text('lorescale coatl'/'DDO', 'Whenever you draw a card, you may put a +1/+1 counter on Lorescale Coatl.').
card_image_name('lorescale coatl'/'DDO', 'lorescale coatl').
card_uid('lorescale coatl'/'DDO', 'DDO:Lorescale Coatl:lorescale coatl').
card_rarity('lorescale coatl'/'DDO', 'Uncommon').
card_artist('lorescale coatl'/'DDO', 'Greg Staples').
card_number('lorescale coatl'/'DDO', '53').
card_flavor_text('lorescale coatl'/'DDO', '\"The enlightenment I never found in etherium I have found traced in the coatl\'s scales.\"\n—Ranalus, vedalken heretic').
card_multiverse_id('lorescale coatl'/'DDO', '394384').

card_in_set('loxodon partisan', 'DDO').
card_original_type('loxodon partisan'/'DDO', 'Creature — Elephant Soldier').
card_original_text('loxodon partisan'/'DDO', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)').
card_image_name('loxodon partisan'/'DDO', 'loxodon partisan').
card_uid('loxodon partisan'/'DDO', 'DDO:Loxodon Partisan:loxodon partisan').
card_rarity('loxodon partisan'/'DDO', 'Common').
card_artist('loxodon partisan'/'DDO', 'Matt Stewart').
card_number('loxodon partisan'/'DDO', '17').
card_flavor_text('loxodon partisan'/'DDO', '\"On every plane I\'ve encountered loxodons, I\'ve found stalwart and loyal friends.\"\n—Elspeth').
card_multiverse_id('loxodon partisan'/'DDO', '394385').

card_in_set('man-o\'-war', 'DDO').
card_original_type('man-o\'-war'/'DDO', 'Creature — Jellyfish').
card_original_text('man-o\'-war'/'DDO', 'When Man-o\'-War enters the battlefield, return target creature to its owner\'s hand.').
card_image_name('man-o\'-war'/'DDO', 'man-o\'-war').
card_uid('man-o\'-war'/'DDO', 'DDO:Man-o\'-War:man-o\'-war').
card_rarity('man-o\'-war'/'DDO', 'Common').
card_artist('man-o\'-war'/'DDO', 'Jon J. Muth').
card_number('man-o\'-war'/'DDO', '38').
card_flavor_text('man-o\'-war'/'DDO', '\"Beauty to the eye does not always translate to the touch.\"\n—Naimah, Femeref philosopher').
card_multiverse_id('man-o\'-war'/'DDO', '394386').

card_in_set('mighty leap', 'DDO').
card_original_type('mighty leap'/'DDO', 'Instant').
card_original_text('mighty leap'/'DDO', 'Target creature gets +2/+2 and gains flying until end of turn.').
card_image_name('mighty leap'/'DDO', 'mighty leap').
card_uid('mighty leap'/'DDO', 'DDO:Mighty Leap:mighty leap').
card_rarity('mighty leap'/'DDO', 'Common').
card_artist('mighty leap'/'DDO', 'rk post').
card_number('mighty leap'/'DDO', '18').
card_flavor_text('mighty leap'/'DDO', '\"The southern fortress taken by invaders? Heh, sure . . . when elephants fly.\"\n—Brezard Skeinbow, captain of the guard').
card_multiverse_id('mighty leap'/'DDO', '394387').

card_in_set('mortal\'s ardor', 'DDO').
card_original_type('mortal\'s ardor'/'DDO', 'Instant').
card_original_text('mortal\'s ardor'/'DDO', 'Target creature gets +1/+1 and gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_image_name('mortal\'s ardor'/'DDO', 'mortal\'s ardor').
card_uid('mortal\'s ardor'/'DDO', 'DDO:Mortal\'s Ardor:mortal\'s ardor').
card_rarity('mortal\'s ardor'/'DDO', 'Common').
card_artist('mortal\'s ardor'/'DDO', 'Kev Walker').
card_number('mortal\'s ardor'/'DDO', '19').
card_flavor_text('mortal\'s ardor'/'DDO', '\"Such deeds should be common fare for such as us.\"\n—Anax, king of Akros').
card_multiverse_id('mortal\'s ardor'/'DDO', '394388').

card_in_set('mother of runes', 'DDO').
card_original_type('mother of runes'/'DDO', 'Creature — Human Cleric').
card_original_text('mother of runes'/'DDO', '{T}: Target creature you control gains protection from the color of your choice until end of turn.').
card_image_name('mother of runes'/'DDO', 'mother of runes').
card_uid('mother of runes'/'DDO', 'DDO:Mother of Runes:mother of runes').
card_rarity('mother of runes'/'DDO', 'Uncommon').
card_artist('mother of runes'/'DDO', 'Terese Nielsen').
card_number('mother of runes'/'DDO', '20').
card_flavor_text('mother of runes'/'DDO', 'She will not touch a weapon, yet she is the greatest protector her people have ever known.').
card_multiverse_id('mother of runes'/'DDO', '394389').

card_in_set('nessian asp', 'DDO').
card_original_type('nessian asp'/'DDO', 'Creature — Snake').
card_original_text('nessian asp'/'DDO', 'Reach\n{6}{G}: Monstrosity 4. (If this creature isn\'t monstrous, put four +1/+1 counters on it and it becomes monstrous.)').
card_image_name('nessian asp'/'DDO', 'nessian asp').
card_uid('nessian asp'/'DDO', 'DDO:Nessian Asp:nessian asp').
card_rarity('nessian asp'/'DDO', 'Common').
card_artist('nessian asp'/'DDO', 'Alex Horley-Orlandelli').
card_number('nessian asp'/'DDO', '48').
card_flavor_text('nessian asp'/'DDO', 'It\'s not the two heads you should fear. It\'s the four fangs.').
card_multiverse_id('nessian asp'/'DDO', '394390').

card_in_set('netcaster spider', 'DDO').
card_original_type('netcaster spider'/'DDO', 'Creature — Spider').
card_original_text('netcaster spider'/'DDO', 'Reach (This creature can block creatures with flying.)\nWhenever Netcaster Spider blocks a creature with flying, Netcaster Spider gets +2/+0 until end of turn.').
card_image_name('netcaster spider'/'DDO', 'netcaster spider').
card_uid('netcaster spider'/'DDO', 'DDO:Netcaster Spider:netcaster spider').
card_rarity('netcaster spider'/'DDO', 'Common').
card_artist('netcaster spider'/'DDO', 'Yohann Schepacz').
card_number('netcaster spider'/'DDO', '49').
card_flavor_text('netcaster spider'/'DDO', 'It is an expert at culling individuals who stray too far from the herd.').
card_multiverse_id('netcaster spider'/'DDO', '394391').

card_in_set('nimbus swimmer', 'DDO').
card_original_type('nimbus swimmer'/'DDO', 'Creature — Leviathan').
card_original_text('nimbus swimmer'/'DDO', 'Flying\nNimbus Swimmer enters the battlefield with X +1/+1 counters on it.').
card_image_name('nimbus swimmer'/'DDO', 'nimbus swimmer').
card_uid('nimbus swimmer'/'DDO', 'DDO:Nimbus Swimmer:nimbus swimmer').
card_rarity('nimbus swimmer'/'DDO', 'Uncommon').
card_artist('nimbus swimmer'/'DDO', 'Howard Lyon').
card_number('nimbus swimmer'/'DDO', '54').
card_flavor_text('nimbus swimmer'/'DDO', 'The Simic soon discovered that the sky offered as few constraints on size as the sea.').
card_multiverse_id('nimbus swimmer'/'DDO', '394392').

card_in_set('noble templar', 'DDO').
card_original_type('noble templar'/'DDO', 'Creature — Human Cleric Soldier').
card_original_text('noble templar'/'DDO', 'Vigilance\nPlainscycling {2} ({2}, Discard this card: Search your library for a Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('noble templar'/'DDO', 'noble templar').
card_uid('noble templar'/'DDO', 'DDO:Noble Templar:noble templar').
card_rarity('noble templar'/'DDO', 'Common').
card_artist('noble templar'/'DDO', 'Alex Horley-Orlandelli').
card_number('noble templar'/'DDO', '21').
card_multiverse_id('noble templar'/'DDO', '394393').

card_in_set('omenspeaker', 'DDO').
card_original_type('omenspeaker'/'DDO', 'Creature — Human Wizard').
card_original_text('omenspeaker'/'DDO', 'When Omenspeaker enters the battlefield, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_image_name('omenspeaker'/'DDO', 'omenspeaker').
card_uid('omenspeaker'/'DDO', 'DDO:Omenspeaker:omenspeaker').
card_rarity('omenspeaker'/'DDO', 'Common').
card_artist('omenspeaker'/'DDO', 'Dallas Williams').
card_number('omenspeaker'/'DDO', '39').
card_flavor_text('omenspeaker'/'DDO', 'Her prophecies amaze her even as she speaks them.').
card_multiverse_id('omenspeaker'/'DDO', '394394').

card_in_set('peel from reality', 'DDO').
card_original_type('peel from reality'/'DDO', 'Instant').
card_original_text('peel from reality'/'DDO', 'Return target creature you control and target creature you don\'t control to their owners\' hands.').
card_image_name('peel from reality'/'DDO', 'peel from reality').
card_uid('peel from reality'/'DDO', 'DDO:Peel from Reality:peel from reality').
card_rarity('peel from reality'/'DDO', 'Common').
card_artist('peel from reality'/'DDO', 'Jason Felix').
card_number('peel from reality'/'DDO', '40').
card_flavor_text('peel from reality'/'DDO', '\"Soulless demon, you are bound to me. Now we will both dwell in oblivion.\"').
card_multiverse_id('peel from reality'/'DDO', '394395').

card_in_set('plains', 'DDO').
card_original_type('plains'/'DDO', 'Basic Land — Plains').
card_original_text('plains'/'DDO', 'W').
card_image_name('plains'/'DDO', 'plains1').
card_uid('plains'/'DDO', 'DDO:Plains:plains1').
card_rarity('plains'/'DDO', 'Basic Land').
card_artist('plains'/'DDO', 'Rob Alexander').
card_number('plains'/'DDO', '30').
card_multiverse_id('plains'/'DDO', '394397').

card_in_set('plains', 'DDO').
card_original_type('plains'/'DDO', 'Basic Land — Plains').
card_original_text('plains'/'DDO', 'W').
card_image_name('plains'/'DDO', 'plains2').
card_uid('plains'/'DDO', 'DDO:Plains:plains2').
card_rarity('plains'/'DDO', 'Basic Land').
card_artist('plains'/'DDO', 'Steven Belledin').
card_number('plains'/'DDO', '31').
card_multiverse_id('plains'/'DDO', '394398').

card_in_set('plains', 'DDO').
card_original_type('plains'/'DDO', 'Basic Land — Plains').
card_original_text('plains'/'DDO', 'W').
card_image_name('plains'/'DDO', 'plains3').
card_uid('plains'/'DDO', 'DDO:Plains:plains3').
card_rarity('plains'/'DDO', 'Basic Land').
card_artist('plains'/'DDO', 'Adam Paquette').
card_number('plains'/'DDO', '32').
card_multiverse_id('plains'/'DDO', '394399').

card_in_set('plains', 'DDO').
card_original_type('plains'/'DDO', 'Basic Land — Plains').
card_original_text('plains'/'DDO', 'W').
card_image_name('plains'/'DDO', 'plains4').
card_uid('plains'/'DDO', 'DDO:Plains:plains4').
card_rarity('plains'/'DDO', 'Basic Land').
card_artist('plains'/'DDO', 'Raoul Vitale').
card_number('plains'/'DDO', '33').
card_multiverse_id('plains'/'DDO', '394396').

card_in_set('plasm capture', 'DDO').
card_original_type('plasm capture'/'DDO', 'Instant').
card_original_text('plasm capture'/'DDO', 'Counter target spell. At the beginning of your next precombat main phase, add X mana in any combination of colors to your mana pool, where X is that spell\'s converted mana cost.').
card_image_name('plasm capture'/'DDO', 'plasm capture').
card_uid('plasm capture'/'DDO', 'DDO:Plasm Capture:plasm capture').
card_rarity('plasm capture'/'DDO', 'Rare').
card_artist('plasm capture'/'DDO', 'Chase Stone').
card_number('plasm capture'/'DDO', '55').
card_flavor_text('plasm capture'/'DDO', '\"Everything serves a purpose. Even you.\"\n—Vorel of the Hull Clade').
card_multiverse_id('plasm capture'/'DDO', '394400').

card_in_set('precinct captain', 'DDO').
card_original_type('precinct captain'/'DDO', 'Creature — Human Soldier').
card_original_text('precinct captain'/'DDO', 'First strike\nWhenever Precinct Captain deals combat damage to a player, put a 1/1 white Soldier creature token onto the battlefield.').
card_image_name('precinct captain'/'DDO', 'precinct captain').
card_uid('precinct captain'/'DDO', 'DDO:Precinct Captain:precinct captain').
card_rarity('precinct captain'/'DDO', 'Rare').
card_artist('precinct captain'/'DDO', 'Steve Prescott').
card_number('precinct captain'/'DDO', '22').
card_flavor_text('precinct captain'/'DDO', '\"In troubled times, we all need someone to watch our back.\"').
card_multiverse_id('precinct captain'/'DDO', '394401').

card_in_set('raise the alarm', 'DDO').
card_original_type('raise the alarm'/'DDO', 'Instant').
card_original_text('raise the alarm'/'DDO', 'Put two 1/1 white Soldier creature tokens onto the battlefield.').
card_image_name('raise the alarm'/'DDO', 'raise the alarm').
card_uid('raise the alarm'/'DDO', 'DDO:Raise the Alarm:raise the alarm').
card_rarity('raise the alarm'/'DDO', 'Common').
card_artist('raise the alarm'/'DDO', 'Zoltan Boros').
card_number('raise the alarm'/'DDO', '23').
card_flavor_text('raise the alarm'/'DDO', 'Like blinking or breathing, responding to an alarm is an involuntary reflex.').
card_multiverse_id('raise the alarm'/'DDO', '394402').

card_in_set('scourge of fleets', 'DDO').
card_original_type('scourge of fleets'/'DDO', 'Creature — Kraken').
card_original_text('scourge of fleets'/'DDO', 'When Scourge of Fleets enters the battlefield, return each creature your opponents control with toughness X or less to its owner\'s hand, where X is the number of Islands you control.').
card_image_name('scourge of fleets'/'DDO', 'scourge of fleets').
card_uid('scourge of fleets'/'DDO', 'DDO:Scourge of Fleets:scourge of fleets').
card_rarity('scourge of fleets'/'DDO', 'Rare').
card_artist('scourge of fleets'/'DDO', 'Steven Belledin').
card_number('scourge of fleets'/'DDO', '41').
card_multiverse_id('scourge of fleets'/'DDO', '394403').

card_in_set('sealock monster', 'DDO').
card_original_type('sealock monster'/'DDO', 'Creature — Octopus').
card_original_text('sealock monster'/'DDO', 'Sealock Monster can\'t attack unless defending player controls an Island.\n{5}{U}{U}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Sealock Monster becomes monstrous, target land becomes an Island in addition to its other types.').
card_image_name('sealock monster'/'DDO', 'sealock monster').
card_uid('sealock monster'/'DDO', 'DDO:Sealock Monster:sealock monster').
card_rarity('sealock monster'/'DDO', 'Uncommon').
card_artist('sealock monster'/'DDO', 'Adam Paquette').
card_number('sealock monster'/'DDO', '42').
card_multiverse_id('sealock monster'/'DDO', '394404').

card_in_set('secluded steppe', 'DDO').
card_original_type('secluded steppe'/'DDO', 'Land').
card_original_text('secluded steppe'/'DDO', 'Secluded Steppe enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'DDO', 'secluded steppe').
card_uid('secluded steppe'/'DDO', 'DDO:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'DDO', 'Common').
card_artist('secluded steppe'/'DDO', 'Heather Hudson').
card_number('secluded steppe'/'DDO', '29').
card_multiverse_id('secluded steppe'/'DDO', '394405').

card_in_set('simic sky swallower', 'DDO').
card_original_type('simic sky swallower'/'DDO', 'Creature — Leviathan').
card_original_text('simic sky swallower'/'DDO', 'Flying, trample\nShroud (This creature can\'t be the target of spells or abilities.)').
card_image_name('simic sky swallower'/'DDO', 'simic sky swallower').
card_uid('simic sky swallower'/'DDO', 'DDO:Simic Sky Swallower:simic sky swallower').
card_rarity('simic sky swallower'/'DDO', 'Rare').
card_artist('simic sky swallower'/'DDO', 'rk post').
card_number('simic sky swallower'/'DDO', '56').
card_flavor_text('simic sky swallower'/'DDO', '\"We\'ve bred out the shortcomings of the species\' natural form and replaced them with assets of our own design.\"\n—Momir Vig').
card_multiverse_id('simic sky swallower'/'DDO', '394406').

card_in_set('soldier', 'DDO').
card_original_type('soldier'/'DDO', 'Token Creature — Soldier').
card_original_text('soldier'/'DDO', '').
card_first_print('soldier', 'DDO').
card_image_name('soldier'/'DDO', 'soldier').
card_uid('soldier'/'DDO', 'DDO:Soldier:soldier').
card_rarity('soldier'/'DDO', 'Common').
card_artist('soldier'/'DDO', 'Svetlin Velinov').
card_number('soldier'/'DDO', '66').
card_multiverse_id('soldier'/'DDO', '394407').

card_in_set('soul parry', 'DDO').
card_original_type('soul parry'/'DDO', 'Instant').
card_original_text('soul parry'/'DDO', 'Prevent all damage one or two target creatures would deal this turn.').
card_image_name('soul parry'/'DDO', 'soul parry').
card_uid('soul parry'/'DDO', 'DDO:Soul Parry:soul parry').
card_rarity('soul parry'/'DDO', 'Common').
card_artist('soul parry'/'DDO', 'Igor Kieryluk').
card_number('soul parry'/'DDO', '24').
card_flavor_text('soul parry'/'DDO', '\"I was called to this world of steel, and it will be my steel that answers.\"\n—Elspeth').
card_multiverse_id('soul parry'/'DDO', '394408').

card_in_set('standing troops', 'DDO').
card_original_type('standing troops'/'DDO', 'Creature — Human Soldier').
card_original_text('standing troops'/'DDO', 'Vigilance').
card_image_name('standing troops'/'DDO', 'standing troops').
card_uid('standing troops'/'DDO', 'DDO:Standing Troops:standing troops').
card_rarity('standing troops'/'DDO', 'Common').
card_artist('standing troops'/'DDO', 'Zoltan Boros').
card_number('standing troops'/'DDO', '25').
card_flavor_text('standing troops'/'DDO', '\"Bant sentries were called the Angel Line. They never faltered, and they were ready to die defending their position.\"\n—Elspeth').
card_multiverse_id('standing troops'/'DDO', '394409').

card_in_set('sunlance', 'DDO').
card_original_type('sunlance'/'DDO', 'Sorcery').
card_original_text('sunlance'/'DDO', 'Sunlance deals 3 damage to target nonwhite creature.').
card_image_name('sunlance'/'DDO', 'sunlance').
card_uid('sunlance'/'DDO', 'DDO:Sunlance:sunlance').
card_rarity('sunlance'/'DDO', 'Common').
card_artist('sunlance'/'DDO', 'Volkan Baga').
card_number('sunlance'/'DDO', '26').
card_flavor_text('sunlance'/'DDO', '\"It\'s easy for the innocent to speak of justice. They seldom feel its terrible power.\"\n—Orim, Samite inquisitor').
card_multiverse_id('sunlance'/'DDO', '394410').

card_in_set('surrakar banisher', 'DDO').
card_original_type('surrakar banisher'/'DDO', 'Creature — Surrakar').
card_original_text('surrakar banisher'/'DDO', 'When Surrakar Banisher enters the battlefield, you may return target tapped creature to its owner\'s hand.').
card_image_name('surrakar banisher'/'DDO', 'surrakar banisher').
card_uid('surrakar banisher'/'DDO', 'DDO:Surrakar Banisher:surrakar banisher').
card_rarity('surrakar banisher'/'DDO', 'Common').
card_artist('surrakar banisher'/'DDO', 'Matt Cavotta').
card_number('surrakar banisher'/'DDO', '43').
card_flavor_text('surrakar banisher'/'DDO', 'Surrakar grab things they don\'t want.').
card_multiverse_id('surrakar banisher'/'DDO', '394411').

card_in_set('temple of the false god', 'DDO').
card_original_type('temple of the false god'/'DDO', 'Land').
card_original_text('temple of the false god'/'DDO', '{T}: Add {2} to your mana pool. Activate this ability only if you control five or more lands.').
card_image_name('temple of the false god'/'DDO', 'temple of the false god').
card_uid('temple of the false god'/'DDO', 'DDO:Temple of the False God:temple of the false god').
card_rarity('temple of the false god'/'DDO', 'Uncommon').
card_artist('temple of the false god'/'DDO', 'James Zapata').
card_number('temple of the false god'/'DDO', '59').
card_flavor_text('temple of the false god'/'DDO', '\"When gods become apathetic, the people will worship anyone who answers their pleas.\"\n—Kiora').
card_multiverse_id('temple of the false god'/'DDO', '394412').

card_in_set('time to feed', 'DDO').
card_original_type('time to feed'/'DDO', 'Sorcery').
card_original_text('time to feed'/'DDO', 'Choose target creature an opponent controls. When that creature dies this turn, you gain 3 life. Target creature you control fights that creature. (Each deals damage equal to its power to the other.)').
card_image_name('time to feed'/'DDO', 'time to feed').
card_uid('time to feed'/'DDO', 'DDO:Time to Feed:time to feed').
card_rarity('time to feed'/'DDO', 'Common').
card_artist('time to feed'/'DDO', 'Wayne Reynolds').
card_number('time to feed'/'DDO', '50').
card_multiverse_id('time to feed'/'DDO', '394413').

card_in_set('urban evolution', 'DDO').
card_original_type('urban evolution'/'DDO', 'Sorcery').
card_original_text('urban evolution'/'DDO', 'Draw three cards. You may play an additional land this turn.').
card_image_name('urban evolution'/'DDO', 'urban evolution').
card_uid('urban evolution'/'DDO', 'DDO:Urban Evolution:urban evolution').
card_rarity('urban evolution'/'DDO', 'Uncommon').
card_artist('urban evolution'/'DDO', 'Eytan Zana').
card_number('urban evolution'/'DDO', '57').
card_flavor_text('urban evolution'/'DDO', 'As the Simic released more of their krasis experiments, they required new habitats, always at the expense of the locals.').
card_multiverse_id('urban evolution'/'DDO', '394414').

card_in_set('veteran armorsmith', 'DDO').
card_original_type('veteran armorsmith'/'DDO', 'Creature — Human Soldier').
card_original_text('veteran armorsmith'/'DDO', 'Other Soldier creatures you control get +0/+1.').
card_image_name('veteran armorsmith'/'DDO', 'veteran armorsmith').
card_uid('veteran armorsmith'/'DDO', 'DDO:Veteran Armorsmith:veteran armorsmith').
card_rarity('veteran armorsmith'/'DDO', 'Common').
card_artist('veteran armorsmith'/'DDO', 'Michael Komarck').
card_number('veteran armorsmith'/'DDO', '27').
card_flavor_text('veteran armorsmith'/'DDO', '\"Courage is a proud and gleaming shield. It does not bend or hide from sight. To fight with courage, we must forge in steel.\"').
card_multiverse_id('veteran armorsmith'/'DDO', '394415').

card_in_set('veteran swordsmith', 'DDO').
card_original_type('veteran swordsmith'/'DDO', 'Creature — Human Soldier').
card_original_text('veteran swordsmith'/'DDO', 'Other Soldier creatures you control get +1/+0.').
card_image_name('veteran swordsmith'/'DDO', 'veteran swordsmith').
card_uid('veteran swordsmith'/'DDO', 'DDO:Veteran Swordsmith:veteran swordsmith').
card_rarity('veteran swordsmith'/'DDO', 'Common').
card_artist('veteran swordsmith'/'DDO', 'Michael Komarck').
card_number('veteran swordsmith'/'DDO', '28').
card_flavor_text('veteran swordsmith'/'DDO', '\"Truth is a straight, keen edge. There are no soft angles or rounded edges. To fight for truth, we must forge in steel.\"').
card_multiverse_id('veteran swordsmith'/'DDO', '394416').

card_in_set('whelming wave', 'DDO').
card_original_type('whelming wave'/'DDO', 'Sorcery').
card_original_text('whelming wave'/'DDO', 'Return all creatures to their owners\' hands except for Krakens, Leviathans, Octopuses, and Serpents.').
card_image_name('whelming wave'/'DDO', 'whelming wave').
card_uid('whelming wave'/'DDO', 'DDO:Whelming Wave:whelming wave').
card_rarity('whelming wave'/'DDO', 'Rare').
card_artist('whelming wave'/'DDO', 'Slawomir Maniak').
card_number('whelming wave'/'DDO', '44').
card_flavor_text('whelming wave'/'DDO', '\"I can see why this appeals to Thassa.\"\n—Kiora').
card_multiverse_id('whelming wave'/'DDO', '394417').
