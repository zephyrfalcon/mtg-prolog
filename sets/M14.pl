% Magic 2014 Core Set

set('M14').
set_name('M14', 'Magic 2014 Core Set').
set_release_date('M14', '2013-07-19').
set_border('M14', 'black').
set_type('M14', 'core').

card_in_set('academy raider', 'M14').
card_original_type('academy raider'/'M14', 'Creature — Human Warrior').
card_original_text('academy raider'/'M14', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)Whenever Academy Raider deals combat damage to a player, you may discard a card. If you do, draw a card.').
card_first_print('academy raider', 'M14').
card_image_name('academy raider'/'M14', 'academy raider').
card_uid('academy raider'/'M14', 'M14:Academy Raider:academy raider').
card_rarity('academy raider'/'M14', 'Common').
card_artist('academy raider'/'M14', 'Karl Kopinski').
card_number('academy raider'/'M14', '124').
card_multiverse_id('academy raider'/'M14', '370735').

card_in_set('accorder\'s shield', 'M14').
card_original_type('accorder\'s shield'/'M14', 'Artifact — Equipment').
card_original_text('accorder\'s shield'/'M14', 'Equipped creature gets +0/+3 and has vigilance. (Attacking doesn\'t cause it to tap.)Equip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('accorder\'s shield'/'M14', 'accorder\'s shield').
card_uid('accorder\'s shield'/'M14', 'M14:Accorder\'s Shield:accorder\'s shield').
card_rarity('accorder\'s shield'/'M14', 'Uncommon').
card_artist('accorder\'s shield'/'M14', 'Alan Pollack').
card_number('accorder\'s shield'/'M14', '204').
card_flavor_text('accorder\'s shield'/'M14', 'An Auriok shield is polished to a mirror finish even on the inside, enabling its bearer to watch foes ahead and behind.').
card_multiverse_id('accorder\'s shield'/'M14', '370581').

card_in_set('accursed spirit', 'M14').
card_original_type('accursed spirit'/'M14', 'Creature — Spirit').
card_original_text('accursed spirit'/'M14', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('accursed spirit', 'M14').
card_image_name('accursed spirit'/'M14', 'accursed spirit').
card_uid('accursed spirit'/'M14', 'M14:Accursed Spirit:accursed spirit').
card_rarity('accursed spirit'/'M14', 'Common').
card_artist('accursed spirit'/'M14', 'Kev Walker').
card_number('accursed spirit'/'M14', '83').
card_flavor_text('accursed spirit'/'M14', 'Many have heard the slither of dragging armor and the soft squelch of its voice. But only its victims ever meet its icy gaze.').
card_multiverse_id('accursed spirit'/'M14', '370811').

card_in_set('act of treason', 'M14').
card_original_type('act of treason'/'M14', 'Sorcery').
card_original_text('act of treason'/'M14', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('act of treason'/'M14', 'act of treason').
card_uid('act of treason'/'M14', 'M14:Act of Treason:act of treason').
card_rarity('act of treason'/'M14', 'Common').
card_artist('act of treason'/'M14', 'Eric Deschamps').
card_number('act of treason'/'M14', '125').
card_flavor_text('act of treason'/'M14', '\"Rage courses in every heart, yearning to betray its rational prison.\"\n—Sarkhan Vol').
card_multiverse_id('act of treason'/'M14', '370618').

card_in_set('advocate of the beast', 'M14').
card_original_type('advocate of the beast'/'M14', 'Creature — Elf Shaman').
card_original_text('advocate of the beast'/'M14', 'At the beginning of your end step, put a +1/+1 counter on target Beast creature you control.').
card_first_print('advocate of the beast', 'M14').
card_image_name('advocate of the beast'/'M14', 'advocate of the beast').
card_uid('advocate of the beast'/'M14', 'M14:Advocate of the Beast:advocate of the beast').
card_rarity('advocate of the beast'/'M14', 'Common').
card_artist('advocate of the beast'/'M14', 'Jesper Ejsing').
card_number('advocate of the beast'/'M14', '164').
card_flavor_text('advocate of the beast'/'M14', '\"I am neither their drover nor their leader. To these majestic beasts, I am simply their herdmate and nothing more.\"').
card_multiverse_id('advocate of the beast'/'M14', '370738').

card_in_set('air servant', 'M14').
card_original_type('air servant'/'M14', 'Creature — Elemental').
card_original_text('air servant'/'M14', 'Flying{2}{U}: Tap target creature with flying.').
card_image_name('air servant'/'M14', 'air servant').
card_uid('air servant'/'M14', 'M14:Air Servant:air servant').
card_rarity('air servant'/'M14', 'Uncommon').
card_artist('air servant'/'M14', 'Lars Grant-West').
card_number('air servant'/'M14', '42').
card_flavor_text('air servant'/'M14', '\"Wind is forceful, yet ephemeral. It can knock a dragon out of the sky, yet pass through the smallest crack unhindered.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('air servant'/'M14', '370688').

card_in_set('ajani\'s chosen', 'M14').
card_original_type('ajani\'s chosen'/'M14', 'Creature — Cat Soldier').
card_original_text('ajani\'s chosen'/'M14', 'Whenever an enchantment enters the battlefield under your control, put a 2/2 white Cat creature token onto the battlefield. If that enchantment is an Aura, you may attach it to the token.').
card_first_print('ajani\'s chosen', 'M14').
card_image_name('ajani\'s chosen'/'M14', 'ajani\'s chosen').
card_uid('ajani\'s chosen'/'M14', 'M14:Ajani\'s Chosen:ajani\'s chosen').
card_rarity('ajani\'s chosen'/'M14', 'Rare').
card_artist('ajani\'s chosen'/'M14', 'Wayne Reynolds').
card_number('ajani\'s chosen'/'M14', '2').
card_flavor_text('ajani\'s chosen'/'M14', '\"United, our roar will level mountains.\"').
card_multiverse_id('ajani\'s chosen'/'M14', '370750').

card_in_set('ajani, caller of the pride', 'M14').
card_original_type('ajani, caller of the pride'/'M14', 'Planeswalker — Ajani').
card_original_text('ajani, caller of the pride'/'M14', '+1: Put a +1/+1 counter on up to one target creature.-3: Target creature gains flying and double strike until end of turn.-8: Put X 2/2 white Cat creature tokens onto the battlefield, where X is your life total.').
card_image_name('ajani, caller of the pride'/'M14', 'ajani, caller of the pride').
card_uid('ajani, caller of the pride'/'M14', 'M14:Ajani, Caller of the Pride:ajani, caller of the pride').
card_rarity('ajani, caller of the pride'/'M14', 'Mythic Rare').
card_artist('ajani, caller of the pride'/'M14', 'D. Alexander Gregory').
card_number('ajani, caller of the pride'/'M14', '1').
card_multiverse_id('ajani, caller of the pride'/'M14', '370680').

card_in_set('altar\'s reap', 'M14').
card_original_type('altar\'s reap'/'M14', 'Instant').
card_original_text('altar\'s reap'/'M14', 'As an additional cost to cast Altar\'s Reap, sacrifice a creature.Draw two cards.').
card_image_name('altar\'s reap'/'M14', 'altar\'s reap').
card_uid('altar\'s reap'/'M14', 'M14:Altar\'s Reap:altar\'s reap').
card_rarity('altar\'s reap'/'M14', 'Common').
card_artist('altar\'s reap'/'M14', 'Donato Giancola').
card_number('altar\'s reap'/'M14', '84').
card_flavor_text('altar\'s reap'/'M14', '\"Don\'t worry, your death will be as informative as possible.\"\n—Gorghul, augur of skulls').
card_multiverse_id('altar\'s reap'/'M14', '370677').

card_in_set('angelic accord', 'M14').
card_original_type('angelic accord'/'M14', 'Enchantment').
card_original_text('angelic accord'/'M14', 'At the beginning of each end step, if you gained 4 or more life this turn, put a 4/4 white Angel creature token with flying onto the battlefield.').
card_first_print('angelic accord', 'M14').
card_image_name('angelic accord'/'M14', 'angelic accord').
card_uid('angelic accord'/'M14', 'M14:Angelic Accord:angelic accord').
card_rarity('angelic accord'/'M14', 'Uncommon').
card_artist('angelic accord'/'M14', 'Michael C. Hayes').
card_number('angelic accord'/'M14', '3').
card_flavor_text('angelic accord'/'M14', 'As demons are drawn to suffering, angels are drawn to acts of benevolence.').
card_multiverse_id('angelic accord'/'M14', '370612').

card_in_set('angelic wall', 'M14').
card_original_type('angelic wall'/'M14', 'Creature — Wall').
card_original_text('angelic wall'/'M14', 'Defender (This creature can\'t attack.)Flying').
card_image_name('angelic wall'/'M14', 'angelic wall').
card_uid('angelic wall'/'M14', 'M14:Angelic Wall:angelic wall').
card_rarity('angelic wall'/'M14', 'Common').
card_artist('angelic wall'/'M14', 'Allen Williams').
card_number('angelic wall'/'M14', '4').
card_flavor_text('angelic wall'/'M14', '\"The air stirred as if fanned by angels\' wings, and the enemy was turned aside.\"\n—Tales of Ikarov the Voyager').
card_multiverse_id('angelic wall'/'M14', '370789').

card_in_set('archaeomancer', 'M14').
card_original_type('archaeomancer'/'M14', 'Creature — Human Wizard').
card_original_text('archaeomancer'/'M14', 'When Archaeomancer enters the battlefield, return target instant or sorcery card from your graveyard to your hand.').
card_image_name('archaeomancer'/'M14', 'archaeomancer').
card_uid('archaeomancer'/'M14', 'M14:Archaeomancer:archaeomancer').
card_rarity('archaeomancer'/'M14', 'Common').
card_artist('archaeomancer'/'M14', 'Zoltan Boros').
card_number('archaeomancer'/'M14', '43').
card_flavor_text('archaeomancer'/'M14', '\"Words of power never disappear. They sleep, awaiting those with the will to rouse them.\"').
card_multiverse_id('archaeomancer'/'M14', '370753').

card_in_set('archangel of thune', 'M14').
card_original_type('archangel of thune'/'M14', 'Creature — Angel').
card_original_text('archangel of thune'/'M14', 'FlyingLifelink (Damage dealt by this creature also causes you to gain that much life.)Whenever you gain life, put a +1/+1 counter on each creature you control.').
card_first_print('archangel of thune', 'M14').
card_image_name('archangel of thune'/'M14', 'archangel of thune').
card_uid('archangel of thune'/'M14', 'M14:Archangel of Thune:archangel of thune').
card_rarity('archangel of thune'/'M14', 'Mythic Rare').
card_artist('archangel of thune'/'M14', 'James Ryman').
card_number('archangel of thune'/'M14', '5').
card_flavor_text('archangel of thune'/'M14', 'Even the wicked have nightmares.').
card_multiverse_id('archangel of thune'/'M14', '370627').

card_in_set('armored cancrix', 'M14').
card_original_type('armored cancrix'/'M14', 'Creature — Crab').
card_original_text('armored cancrix'/'M14', '').
card_image_name('armored cancrix'/'M14', 'armored cancrix').
card_uid('armored cancrix'/'M14', 'M14:Armored Cancrix:armored cancrix').
card_rarity('armored cancrix'/'M14', 'Common').
card_artist('armored cancrix'/'M14', 'Tomasz Jedruszek').
card_number('armored cancrix'/'M14', '44').
card_flavor_text('armored cancrix'/'M14', 'Creatures displaced from time still turn up every year, stranded by the temporal disaster that once swept across Dominaria.').
card_multiverse_id('armored cancrix'/'M14', '370632').

card_in_set('artificer\'s hex', 'M14').
card_original_type('artificer\'s hex'/'M14', 'Enchantment — Aura').
card_original_text('artificer\'s hex'/'M14', 'Enchant EquipmentAt the beginning of your upkeep, if enchanted Equipment is attached to a creature, destroy that creature.').
card_first_print('artificer\'s hex', 'M14').
card_image_name('artificer\'s hex'/'M14', 'artificer\'s hex').
card_uid('artificer\'s hex'/'M14', 'M14:Artificer\'s Hex:artificer\'s hex').
card_rarity('artificer\'s hex'/'M14', 'Uncommon').
card_artist('artificer\'s hex'/'M14', 'Franz Vohwinkel').
card_number('artificer\'s hex'/'M14', '85').
card_flavor_text('artificer\'s hex'/'M14', '\"Some treasures aren\'t worth finding, let alone stealing.\"\n—Dack Fayden').
card_multiverse_id('artificer\'s hex'/'M14', '370634').

card_in_set('auramancer', 'M14').
card_original_type('auramancer'/'M14', 'Creature — Human Wizard').
card_original_text('auramancer'/'M14', 'When Auramancer enters the battlefield, you may return target enchantment card from your graveyard to your hand.').
card_image_name('auramancer'/'M14', 'auramancer').
card_uid('auramancer'/'M14', 'M14:Auramancer:auramancer').
card_rarity('auramancer'/'M14', 'Common').
card_artist('auramancer'/'M14', 'Rebecca Guay').
card_number('auramancer'/'M14', '6').
card_flavor_text('auramancer'/'M14', '\"In memories, we can find our deepest reserves of strength.\"').
card_multiverse_id('auramancer'/'M14', '370793').

card_in_set('awaken the ancient', 'M14').
card_original_type('awaken the ancient'/'M14', 'Enchantment — Aura').
card_original_text('awaken the ancient'/'M14', 'Enchant MountainEnchanted Mountain is a 7/7 red Giant creature with haste. It\'s still a land.').
card_first_print('awaken the ancient', 'M14').
card_image_name('awaken the ancient'/'M14', 'awaken the ancient').
card_uid('awaken the ancient'/'M14', 'M14:Awaken the Ancient:awaken the ancient').
card_rarity('awaken the ancient'/'M14', 'Rare').
card_artist('awaken the ancient'/'M14', 'Jaime Jones').
card_number('awaken the ancient'/'M14', '126').
card_flavor_text('awaken the ancient'/'M14', 'Some days you stand to greet the horizon. Other days the horizon stands to greet you.').
card_multiverse_id('awaken the ancient'/'M14', '370613').

card_in_set('banisher priest', 'M14').
card_original_type('banisher priest'/'M14', 'Creature — Human Cleric').
card_original_text('banisher priest'/'M14', 'When Banisher Priest enters the battlefield, exile target creature an opponent controls until Banisher Priest leaves the battlefield. (That creature returns under its owner\'s control.)').
card_image_name('banisher priest'/'M14', 'banisher priest').
card_uid('banisher priest'/'M14', 'M14:Banisher Priest:banisher priest').
card_rarity('banisher priest'/'M14', 'Uncommon').
card_artist('banisher priest'/'M14', 'Willian Murai').
card_number('banisher priest'/'M14', '7').
card_flavor_text('banisher priest'/'M14', '\"Oathbreaker, I cast you out!\"').
card_multiverse_id('banisher priest'/'M14', '370624').

card_in_set('barrage of expendables', 'M14').
card_original_type('barrage of expendables'/'M14', 'Enchantment').
card_original_text('barrage of expendables'/'M14', '{R}, Sacrifice a creature: Barrage of Expendables deals 1 damage to target creature or player.').
card_first_print('barrage of expendables', 'M14').
card_image_name('barrage of expendables'/'M14', 'barrage of expendables').
card_uid('barrage of expendables'/'M14', 'M14:Barrage of Expendables:barrage of expendables').
card_rarity('barrage of expendables'/'M14', 'Uncommon').
card_artist('barrage of expendables'/'M14', 'Trevor Claxton').
card_number('barrage of expendables'/'M14', '127').
card_flavor_text('barrage of expendables'/'M14', 'Goblin generals don\'t distinguish between troops and ammunition.').
card_multiverse_id('barrage of expendables'/'M14', '370822').

card_in_set('battle sliver', 'M14').
card_original_type('battle sliver'/'M14', 'Creature — Sliver').
card_original_text('battle sliver'/'M14', 'Sliver creatures you control get +2/+0.').
card_first_print('battle sliver', 'M14').
card_image_name('battle sliver'/'M14', 'battle sliver').
card_uid('battle sliver'/'M14', 'M14:Battle Sliver:battle sliver').
card_rarity('battle sliver'/'M14', 'Uncommon').
card_artist('battle sliver'/'M14', 'Slawomir Maniak').
card_number('battle sliver'/'M14', '128').
card_flavor_text('battle sliver'/'M14', '\"One emitted a strange series of buzzing clicks and guttural commands, then clawed arms emerged from all of them. Is there no limit to their adaptations?\"\n—Hastric, Thunian scout').
card_multiverse_id('battle sliver'/'M14', '370639').

card_in_set('blessing', 'M14').
card_original_type('blessing'/'M14', 'Enchantment — Aura').
card_original_text('blessing'/'M14', 'Enchant creature{W}: Enchanted creature gets +1/+1 until end of turn.').
card_image_name('blessing'/'M14', 'blessing').
card_uid('blessing'/'M14', 'M14:Blessing:blessing').
card_rarity('blessing'/'M14', 'Uncommon').
card_artist('blessing'/'M14', 'Jason A. Engle').
card_number('blessing'/'M14', '8').
card_flavor_text('blessing'/'M14', '\"Virtue can\'t survive without strength.\"\n—Ardanna of the Angelic Council').
card_multiverse_id('blessing'/'M14', '370819').

card_in_set('blightcaster', 'M14').
card_original_type('blightcaster'/'M14', 'Creature — Human Wizard').
card_original_text('blightcaster'/'M14', 'Whenever you cast an enchantment spell, you may have target creature get -2/-2 until end of turn.').
card_first_print('blightcaster', 'M14').
card_image_name('blightcaster'/'M14', 'blightcaster').
card_uid('blightcaster'/'M14', 'M14:Blightcaster:blightcaster').
card_rarity('blightcaster'/'M14', 'Uncommon').
card_artist('blightcaster'/'M14', 'Winona Nelson').
card_number('blightcaster'/'M14', '86').
card_flavor_text('blightcaster'/'M14', '\"Your flesh is unprepared for my gifts.\"').
card_multiverse_id('blightcaster'/'M14', '370761').

card_in_set('blood bairn', 'M14').
card_original_type('blood bairn'/'M14', 'Creature — Vampire').
card_original_text('blood bairn'/'M14', 'Sacrifice another creature: Blood Bairn gets +2/+2 until end of turn.').
card_first_print('blood bairn', 'M14').
card_image_name('blood bairn'/'M14', 'blood bairn').
card_uid('blood bairn'/'M14', 'M14:Blood Bairn:blood bairn').
card_rarity('blood bairn'/'M14', 'Common').
card_artist('blood bairn'/'M14', 'Ryan Yee').
card_number('blood bairn'/'M14', '87').
card_flavor_text('blood bairn'/'M14', 'The travelers were warned to watch out for children on the road.').
card_multiverse_id('blood bairn'/'M14', '370698').

card_in_set('blur sliver', 'M14').
card_original_type('blur sliver'/'M14', 'Creature — Sliver').
card_original_text('blur sliver'/'M14', 'Sliver creatures you control have haste. (They can attack and {T} as soon as they come under your control.)').
card_first_print('blur sliver', 'M14').
card_image_name('blur sliver'/'M14', 'blur sliver').
card_uid('blur sliver'/'M14', 'M14:Blur Sliver:blur sliver').
card_rarity('blur sliver'/'M14', 'Common').
card_artist('blur sliver'/'M14', 'Daarken').
card_number('blur sliver'/'M14', '129').
card_flavor_text('blur sliver'/'M14', 'They move in a synchronized swarm, turning entire squads into heaps of bloody rags and bones in an instant.').
card_multiverse_id('blur sliver'/'M14', '370593').

card_in_set('bogbrew witch', 'M14').
card_original_type('bogbrew witch'/'M14', 'Creature — Human Wizard').
card_original_text('bogbrew witch'/'M14', '{2}, {T}: Search your library for a card named Festering Newt or Bubbling Cauldron, put it onto the battlefield tapped, then shuffle your library.').
card_first_print('bogbrew witch', 'M14').
card_image_name('bogbrew witch'/'M14', 'bogbrew witch').
card_uid('bogbrew witch'/'M14', 'M14:Bogbrew Witch:bogbrew witch').
card_rarity('bogbrew witch'/'M14', 'Rare').
card_artist('bogbrew witch'/'M14', 'Eric Deschamps').
card_number('bogbrew witch'/'M14', '88').
card_flavor_text('bogbrew witch'/'M14', '\"The hardest thing is finding the right ingredients.\"').
card_multiverse_id('bogbrew witch'/'M14', '370758').

card_in_set('bonescythe sliver', 'M14').
card_original_type('bonescythe sliver'/'M14', 'Creature — Sliver').
card_original_text('bonescythe sliver'/'M14', 'Sliver creatures you control have double strike. (They deal both first-strike and regular combat damage.)').
card_image_name('bonescythe sliver'/'M14', 'bonescythe sliver').
card_uid('bonescythe sliver'/'M14', 'M14:Bonescythe Sliver:bonescythe sliver').
card_rarity('bonescythe sliver'/'M14', 'Rare').
card_artist('bonescythe sliver'/'M14', 'Trevor Claxton').
card_number('bonescythe sliver'/'M14', '9').
card_flavor_text('bonescythe sliver'/'M14', '\"Their appendages are sharper than our swords and quicker than our bows.\"\n—Hastric, Thunian scout').
card_multiverse_id('bonescythe sliver'/'M14', '370801').

card_in_set('bramblecrush', 'M14').
card_original_type('bramblecrush'/'M14', 'Sorcery').
card_original_text('bramblecrush'/'M14', 'Destroy target noncreature permanent.').
card_image_name('bramblecrush'/'M14', 'bramblecrush').
card_uid('bramblecrush'/'M14', 'M14:Bramblecrush:bramblecrush').
card_rarity('bramblecrush'/'M14', 'Uncommon').
card_artist('bramblecrush'/'M14', 'Drew Baker').
card_number('bramblecrush'/'M14', '165').
card_flavor_text('bramblecrush'/'M14', 'Nature abhors a lot of things.').
card_multiverse_id('bramblecrush'/'M14', '370642').

card_in_set('brave the elements', 'M14').
card_original_type('brave the elements'/'M14', 'Instant').
card_original_text('brave the elements'/'M14', 'Choose a color. White creatures you control gain protection from the chosen color until end of turn. (They can\'t be blocked, targeted, dealt damage, or enchanted by anything of that color.)').
card_image_name('brave the elements'/'M14', 'brave the elements').
card_uid('brave the elements'/'M14', 'M14:Brave the Elements:brave the elements').
card_rarity('brave the elements'/'M14', 'Uncommon').
card_artist('brave the elements'/'M14', 'Goran Josic').
card_number('brave the elements'/'M14', '10').
card_multiverse_id('brave the elements'/'M14', '370816').

card_in_set('briarpack alpha', 'M14').
card_original_type('briarpack alpha'/'M14', 'Creature — Wolf').
card_original_text('briarpack alpha'/'M14', 'Flash (You may cast this spell any time you could cast an instant.)When Briarpack Alpha enters the battlefield, target creature gets +2/+2 until end of turn.').
card_image_name('briarpack alpha'/'M14', 'briarpack alpha').
card_uid('briarpack alpha'/'M14', 'M14:Briarpack Alpha:briarpack alpha').
card_rarity('briarpack alpha'/'M14', 'Uncommon').
card_artist('briarpack alpha'/'M14', 'Daarken').
card_number('briarpack alpha'/'M14', '166').
card_flavor_text('briarpack alpha'/'M14', '\"One of my greatest teachers.\"\n—Garruk Wildspeaker').
card_multiverse_id('briarpack alpha'/'M14', '370739').

card_in_set('brindle boar', 'M14').
card_original_type('brindle boar'/'M14', 'Creature — Boar').
card_original_text('brindle boar'/'M14', 'Sacrifice Brindle Boar: You gain 4 life.').
card_image_name('brindle boar'/'M14', 'brindle boar').
card_uid('brindle boar'/'M14', 'M14:Brindle Boar:brindle boar').
card_rarity('brindle boar'/'M14', 'Common').
card_artist('brindle boar'/'M14', 'Dave Allsop').
card_number('brindle boar'/'M14', '167').
card_flavor_text('brindle boar'/'M14', '\"Tell the cooks to prepare the fires. Tonight we feast!\"\n—Tolar Wolfbrother, Krosan tracker').
card_multiverse_id('brindle boar'/'M14', '370778').

card_in_set('bubbling cauldron', 'M14').
card_original_type('bubbling cauldron'/'M14', 'Artifact').
card_original_text('bubbling cauldron'/'M14', '{1}, {T}, Sacrifice a creature: You gain 4 life.{1}, {T}, Sacrifice a creature named Festering Newt: Each opponent loses 4 life. You gain life equal to the life lost this way.').
card_first_print('bubbling cauldron', 'M14').
card_image_name('bubbling cauldron'/'M14', 'bubbling cauldron').
card_uid('bubbling cauldron'/'M14', 'M14:Bubbling Cauldron:bubbling cauldron').
card_rarity('bubbling cauldron'/'M14', 'Uncommon').
card_artist('bubbling cauldron'/'M14', 'Eric Deschamps').
card_number('bubbling cauldron'/'M14', '205').
card_multiverse_id('bubbling cauldron'/'M14', '370661').

card_in_set('burning earth', 'M14').
card_original_type('burning earth'/'M14', 'Enchantment').
card_original_text('burning earth'/'M14', 'Whenever a player taps a nonbasic land for mana, Burning Earth deals 1 damage to that player.').
card_first_print('burning earth', 'M14').
card_image_name('burning earth'/'M14', 'burning earth').
card_uid('burning earth'/'M14', 'M14:Burning Earth:burning earth').
card_rarity('burning earth'/'M14', 'Rare').
card_artist('burning earth'/'M14', 'rk post').
card_number('burning earth'/'M14', '130').
card_flavor_text('burning earth'/'M14', '\"Your world is formed from the same power that wraps my burning hand around your throat.\"\n—Shrazik, lavamancer').
card_multiverse_id('burning earth'/'M14', '370696').

card_in_set('cancel', 'M14').
card_original_type('cancel'/'M14', 'Instant').
card_original_text('cancel'/'M14', 'Counter target spell.').
card_image_name('cancel'/'M14', 'cancel').
card_uid('cancel'/'M14', 'M14:Cancel:cancel').
card_rarity('cancel'/'M14', 'Common').
card_artist('cancel'/'M14', 'David Palumbo').
card_number('cancel'/'M14', '45').
card_multiverse_id('cancel'/'M14', '370755').

card_in_set('canyon minotaur', 'M14').
card_original_type('canyon minotaur'/'M14', 'Creature — Minotaur Warrior').
card_original_text('canyon minotaur'/'M14', '').
card_image_name('canyon minotaur'/'M14', 'canyon minotaur').
card_uid('canyon minotaur'/'M14', 'M14:Canyon Minotaur:canyon minotaur').
card_rarity('canyon minotaur'/'M14', 'Common').
card_artist('canyon minotaur'/'M14', 'Steve Prescott').
card_number('canyon minotaur'/'M14', '131').
card_flavor_text('canyon minotaur'/'M14', '\"We\'ll scale these cliffs, traverse Brittle Bridge, and then fight our way down the volcanic slopes on the other side.\"\n\"Isn\'t the shortest route through the canyon?\"\n\"Yes.\"\n\"So shouldn\'t we—\"\n\"No.\"').
card_multiverse_id('canyon minotaur'/'M14', '370757').

card_in_set('capashen knight', 'M14').
card_original_type('capashen knight'/'M14', 'Creature — Human Knight').
card_original_text('capashen knight'/'M14', 'First strike (This creature deals combat damage before creatures without first strike.){1}{W}: Capashen Knight gets +1/+0 until end of turn.').
card_image_name('capashen knight'/'M14', 'capashen knight').
card_uid('capashen knight'/'M14', 'M14:Capashen Knight:capashen knight').
card_rarity('capashen knight'/'M14', 'Common').
card_artist('capashen knight'/'M14', 'Jasper Sandner').
card_number('capashen knight'/'M14', '11').
card_flavor_text('capashen knight'/'M14', 'He protects Benalia as much with his reputation as with his force of arms.').
card_multiverse_id('capashen knight'/'M14', '370821').

card_in_set('celestial flare', 'M14').
card_original_type('celestial flare'/'M14', 'Instant').
card_original_text('celestial flare'/'M14', 'Target player sacrifices an attacking or blocking creature.').
card_first_print('celestial flare', 'M14').
card_image_name('celestial flare'/'M14', 'celestial flare').
card_uid('celestial flare'/'M14', 'M14:Celestial Flare:celestial flare').
card_rarity('celestial flare'/'M14', 'Common').
card_artist('celestial flare'/'M14', 'Clint Cearley').
card_number('celestial flare'/'M14', '12').
card_flavor_text('celestial flare'/'M14', '\"You were defeated the moment you declared your aggression.\"\n—Gideon Jura').
card_multiverse_id('celestial flare'/'M14', '370666').

card_in_set('chandra\'s outrage', 'M14').
card_original_type('chandra\'s outrage'/'M14', 'Instant').
card_original_text('chandra\'s outrage'/'M14', 'Chandra\'s Outrage deals 4 damage to target creature and 2 damage to that creature\'s controller.').
card_image_name('chandra\'s outrage'/'M14', 'chandra\'s outrage').
card_uid('chandra\'s outrage'/'M14', 'M14:Chandra\'s Outrage:chandra\'s outrage').
card_rarity('chandra\'s outrage'/'M14', 'Common').
card_artist('chandra\'s outrage'/'M14', 'Christopher Moeller').
card_number('chandra\'s outrage'/'M14', '133').
card_flavor_text('chandra\'s outrage'/'M14', 'Chandra never believed in using her \"inside voice.\"').
card_multiverse_id('chandra\'s outrage'/'M14', '370659').

card_in_set('chandra\'s phoenix', 'M14').
card_original_type('chandra\'s phoenix'/'M14', 'Creature — Phoenix').
card_original_text('chandra\'s phoenix'/'M14', 'FlyingHaste (This creature can attack and {T} as soon as it comes under your control.)Whenever an opponent is dealt damage by a red instant or sorcery spell you control or by a red planeswalker you control, return Chandra\'s Phoenix from your graveyard to your hand.').
card_image_name('chandra\'s phoenix'/'M14', 'chandra\'s phoenix').
card_uid('chandra\'s phoenix'/'M14', 'M14:Chandra\'s Phoenix:chandra\'s phoenix').
card_rarity('chandra\'s phoenix'/'M14', 'Rare').
card_artist('chandra\'s phoenix'/'M14', 'Aleksi Briclot').
card_number('chandra\'s phoenix'/'M14', '134').
card_multiverse_id('chandra\'s phoenix'/'M14', '370691').

card_in_set('chandra, pyromaster', 'M14').
card_original_type('chandra, pyromaster'/'M14', 'Planeswalker — Chandra').
card_original_text('chandra, pyromaster'/'M14', '+1: Chandra, Pyromaster deals 1 damage to target player and 1 damage to up to one target creature that player controls. That creature can\'t block this turn.0: Exile the top card of your library. You may play it this turn.-7: Exile the top ten cards of your library. Choose an instant or sorcery card exiled this way and copy it three times. You may cast the copies without paying their mana costs.').
card_image_name('chandra, pyromaster'/'M14', 'chandra, pyromaster').
card_uid('chandra, pyromaster'/'M14', 'M14:Chandra, Pyromaster:chandra, pyromaster').
card_rarity('chandra, pyromaster'/'M14', 'Mythic Rare').
card_artist('chandra, pyromaster'/'M14', 'Winona Nelson').
card_number('chandra, pyromaster'/'M14', '132').
card_multiverse_id('chandra, pyromaster'/'M14', '370637').

card_in_set('charging griffin', 'M14').
card_original_type('charging griffin'/'M14', 'Creature — Griffin').
card_original_text('charging griffin'/'M14', 'FlyingWhenever Charging Griffin attacks, it gets +1/+1 until end of turn.').
card_first_print('charging griffin', 'M14').
card_image_name('charging griffin'/'M14', 'charging griffin').
card_uid('charging griffin'/'M14', 'M14:Charging Griffin:charging griffin').
card_rarity('charging griffin'/'M14', 'Common').
card_artist('charging griffin'/'M14', 'Erica Yang').
card_number('charging griffin'/'M14', '13').
card_flavor_text('charging griffin'/'M14', 'Four claws, two wings, one beak, no fear.').
card_multiverse_id('charging griffin'/'M14', '370768').

card_in_set('child of night', 'M14').
card_original_type('child of night'/'M14', 'Creature — Vampire').
card_original_text('child of night'/'M14', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('child of night'/'M14', 'child of night').
card_uid('child of night'/'M14', 'M14:Child of Night:child of night').
card_rarity('child of night'/'M14', 'Common').
card_artist('child of night'/'M14', 'Ash Wood').
card_number('child of night'/'M14', '89').
card_flavor_text('child of night'/'M14', 'Sins that would be too gruesome in the light of day are made more pleasing in the dark of night.').
card_multiverse_id('child of night'/'M14', '370823').

card_in_set('claustrophobia', 'M14').
card_original_type('claustrophobia'/'M14', 'Enchantment — Aura').
card_original_text('claustrophobia'/'M14', 'Enchant creatureWhen Claustrophobia enters the battlefield, tap enchanted creature.Enchanted creature doesn\'t untap during its controller\'s untap step.').
card_image_name('claustrophobia'/'M14', 'claustrophobia').
card_uid('claustrophobia'/'M14', 'M14:Claustrophobia:claustrophobia').
card_rarity('claustrophobia'/'M14', 'Common').
card_artist('claustrophobia'/'M14', 'Ryan Pancoast').
card_number('claustrophobia'/'M14', '46').
card_flavor_text('claustrophobia'/'M14', 'Six feet of earth muffled his cries.').
card_multiverse_id('claustrophobia'/'M14', '370653').

card_in_set('clone', 'M14').
card_original_type('clone'/'M14', 'Creature — Shapeshifter').
card_original_text('clone'/'M14', 'You may have Clone enter the battlefield as a copy of any creature on the battlefield.').
card_image_name('clone'/'M14', 'clone').
card_uid('clone'/'M14', 'M14:Clone:clone').
card_rarity('clone'/'M14', 'Rare').
card_artist('clone'/'M14', 'Kev Walker').
card_number('clone'/'M14', '47').
card_flavor_text('clone'/'M14', 'He knows your strengths, your weaknesses, and the shape of that unfortunate birthmark on your backside.').
card_multiverse_id('clone'/'M14', '370622').

card_in_set('colossal whale', 'M14').
card_original_type('colossal whale'/'M14', 'Creature — Whale').
card_original_text('colossal whale'/'M14', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)Whenever Colossal Whale attacks, you may exile target creature defending player controls until Colossal Whale leaves the battlefield. (That creature returns under its owner\'s control.)').
card_image_name('colossal whale'/'M14', 'colossal whale').
card_uid('colossal whale'/'M14', 'M14:Colossal Whale:colossal whale').
card_rarity('colossal whale'/'M14', 'Rare').
card_artist('colossal whale'/'M14', 'Adam Paquette').
card_number('colossal whale'/'M14', '48').
card_multiverse_id('colossal whale'/'M14', '370685').

card_in_set('congregate', 'M14').
card_original_type('congregate'/'M14', 'Instant').
card_original_text('congregate'/'M14', 'Target player gains 2 life for each creature on the battlefield.').
card_image_name('congregate'/'M14', 'congregate').
card_uid('congregate'/'M14', 'M14:Congregate:congregate').
card_rarity('congregate'/'M14', 'Uncommon').
card_artist('congregate'/'M14', 'Mark Zug').
card_number('congregate'/'M14', '14').
card_flavor_text('congregate'/'M14', '\"In the gathering there is strength for all who founder, renewal for all who languish, love for all who sing.\"\n—Song of All, canto 642').
card_multiverse_id('congregate'/'M14', '370804').

card_in_set('coral merfolk', 'M14').
card_original_type('coral merfolk'/'M14', 'Creature — Merfolk').
card_original_text('coral merfolk'/'M14', '').
card_image_name('coral merfolk'/'M14', 'coral merfolk').
card_uid('coral merfolk'/'M14', 'M14:Coral Merfolk:coral merfolk').
card_rarity('coral merfolk'/'M14', 'Common').
card_artist('coral merfolk'/'M14', 'rk post').
card_number('coral merfolk'/'M14', '49').
card_flavor_text('coral merfolk'/'M14', 'Early sailors thought they were tricks of the mind or perhaps manatees. As more and more ships were classified as lost to \"manatee attacks,\" the truth began to dawn.').
card_multiverse_id('coral merfolk'/'M14', '370667').

card_in_set('corpse hauler', 'M14').
card_original_type('corpse hauler'/'M14', 'Creature — Human Rogue').
card_original_text('corpse hauler'/'M14', '{2}{B}, Sacrifice Corpse Hauler: Return another target creature card from your graveyard to your hand.').
card_first_print('corpse hauler', 'M14').
card_image_name('corpse hauler'/'M14', 'corpse hauler').
card_uid('corpse hauler'/'M14', 'M14:Corpse Hauler:corpse hauler').
card_rarity('corpse hauler'/'M14', 'Common').
card_artist('corpse hauler'/'M14', 'Jesper Ejsing').
card_number('corpse hauler'/'M14', '90').
card_flavor_text('corpse hauler'/'M14', '\"He expects a reward, but the only piece of silver he\'ll get from me is my blade in his gut.\"\n—Shelac, necromancer').
card_multiverse_id('corpse hauler'/'M14', '370800').

card_in_set('corrupt', 'M14').
card_original_type('corrupt'/'M14', 'Sorcery').
card_original_text('corrupt'/'M14', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_image_name('corrupt'/'M14', 'corrupt').
card_uid('corrupt'/'M14', 'M14:Corrupt:corrupt').
card_rarity('corrupt'/'M14', 'Uncommon').
card_artist('corrupt'/'M14', 'Dave Allsop').
card_number('corrupt'/'M14', '91').
card_flavor_text('corrupt'/'M14', 'One misstep in the swamp and the evil that sleeps beneath will rise to find you.').
card_multiverse_id('corrupt'/'M14', '370630').

card_in_set('cyclops tyrant', 'M14').
card_original_type('cyclops tyrant'/'M14', 'Creature — Cyclops').
card_original_text('cyclops tyrant'/'M14', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)Cyclops Tyrant can\'t block creatures with power 2 or less.').
card_first_print('cyclops tyrant', 'M14').
card_image_name('cyclops tyrant'/'M14', 'cyclops tyrant').
card_uid('cyclops tyrant'/'M14', 'M14:Cyclops Tyrant:cyclops tyrant').
card_rarity('cyclops tyrant'/'M14', 'Common').
card_artist('cyclops tyrant'/'M14', 'Zack Stella').
card_number('cyclops tyrant'/'M14', '135').
card_flavor_text('cyclops tyrant'/'M14', 'A single eye, blinded by rage.').
card_multiverse_id('cyclops tyrant'/'M14', '370585').

card_in_set('dark favor', 'M14').
card_original_type('dark favor'/'M14', 'Enchantment — Aura').
card_original_text('dark favor'/'M14', 'Enchant creatureWhen Dark Favor enters the battlefield, you lose 1 life.Enchanted creature gets +3/+1.').
card_image_name('dark favor'/'M14', 'dark favor').
card_uid('dark favor'/'M14', 'M14:Dark Favor:dark favor').
card_rarity('dark favor'/'M14', 'Common').
card_artist('dark favor'/'M14', 'Allen Williams').
card_number('dark favor'/'M14', '92').
card_flavor_text('dark favor'/'M14', 'When he began to curse what he held holy, his strength grew unrivaled.').
card_multiverse_id('dark favor'/'M14', '370782').

card_in_set('dark prophecy', 'M14').
card_original_type('dark prophecy'/'M14', 'Enchantment').
card_original_text('dark prophecy'/'M14', 'Whenever a creature you control dies, you draw a card and lose 1 life.').
card_first_print('dark prophecy', 'M14').
card_image_name('dark prophecy'/'M14', 'dark prophecy').
card_uid('dark prophecy'/'M14', 'M14:Dark Prophecy:dark prophecy').
card_rarity('dark prophecy'/'M14', 'Rare').
card_artist('dark prophecy'/'M14', 'Scott Chou').
card_number('dark prophecy'/'M14', '93').
card_flavor_text('dark prophecy'/'M14', 'When the bog ran short on small animals, Ekri turned to the surrounding farmlands.').
card_multiverse_id('dark prophecy'/'M14', '370596').

card_in_set('darksteel forge', 'M14').
card_original_type('darksteel forge'/'M14', 'Artifact').
card_original_text('darksteel forge'/'M14', 'Artifacts you control have indestructible. (Effects that say \"destroy\" don\'t destroy them. Artifact creatures with indestructible can\'t be destroyed by damage.)').
card_image_name('darksteel forge'/'M14', 'darksteel forge').
card_uid('darksteel forge'/'M14', 'M14:Darksteel Forge:darksteel forge').
card_rarity('darksteel forge'/'M14', 'Mythic Rare').
card_artist('darksteel forge'/'M14', 'Martina Pilcerova').
card_number('darksteel forge'/'M14', '206').
card_flavor_text('darksteel forge'/'M14', '\"It is as if this world has been expecting our blessings.\"\n—Elesh Norn, Grand Cenobite').
card_multiverse_id('darksteel forge'/'M14', '370734').

card_in_set('darksteel ingot', 'M14').
card_original_type('darksteel ingot'/'M14', 'Artifact').
card_original_text('darksteel ingot'/'M14', 'Indestructible (Effects that say \"destroy\" don\'t destroy this artifact.){T}: Add one mana of any color to your mana pool.').
card_image_name('darksteel ingot'/'M14', 'darksteel ingot').
card_uid('darksteel ingot'/'M14', 'M14:Darksteel Ingot:darksteel ingot').
card_rarity('darksteel ingot'/'M14', 'Uncommon').
card_artist('darksteel ingot'/'M14', 'Martina Pilcerova').
card_number('darksteel ingot'/'M14', '207').
card_flavor_text('darksteel ingot'/'M14', '\"It reflects the purity of Mirrodin-that-was.\"\n—Koth of the Hammer').
card_multiverse_id('darksteel ingot'/'M14', '370675').

card_in_set('dawnstrike paladin', 'M14').
card_original_type('dawnstrike paladin'/'M14', 'Creature — Human Knight').
card_original_text('dawnstrike paladin'/'M14', 'Vigilance (Attacking doesn\'t cause this creature to tap.)Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('dawnstrike paladin', 'M14').
card_image_name('dawnstrike paladin'/'M14', 'dawnstrike paladin').
card_uid('dawnstrike paladin'/'M14', 'M14:Dawnstrike Paladin:dawnstrike paladin').
card_rarity('dawnstrike paladin'/'M14', 'Common').
card_artist('dawnstrike paladin'/'M14', 'Tyler Jacobson').
card_number('dawnstrike paladin'/'M14', '15').
card_flavor_text('dawnstrike paladin'/'M14', 'She crushes darkness beneath her charger\'s hooves.').
card_multiverse_id('dawnstrike paladin'/'M14', '370721').

card_in_set('deadly recluse', 'M14').
card_original_type('deadly recluse'/'M14', 'Creature — Spider').
card_original_text('deadly recluse'/'M14', 'Reach (This creature can block creatures with flying.)Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('deadly recluse'/'M14', 'deadly recluse').
card_uid('deadly recluse'/'M14', 'M14:Deadly Recluse:deadly recluse').
card_rarity('deadly recluse'/'M14', 'Common').
card_artist('deadly recluse'/'M14', 'Warren Mahy').
card_number('deadly recluse'/'M14', '168').
card_flavor_text('deadly recluse'/'M14', 'Even dragons fear its silken strands.').
card_multiverse_id('deadly recluse'/'M14', '370582').

card_in_set('deathgaze cockatrice', 'M14').
card_original_type('deathgaze cockatrice'/'M14', 'Creature — Cockatrice').
card_original_text('deathgaze cockatrice'/'M14', 'FlyingDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('deathgaze cockatrice', 'M14').
card_image_name('deathgaze cockatrice'/'M14', 'deathgaze cockatrice').
card_uid('deathgaze cockatrice'/'M14', 'M14:Deathgaze Cockatrice:deathgaze cockatrice').
card_rarity('deathgaze cockatrice'/'M14', 'Common').
card_artist('deathgaze cockatrice'/'M14', 'Kev Walker').
card_number('deathgaze cockatrice'/'M14', '94').
card_flavor_text('deathgaze cockatrice'/'M14', '\"Sometimes I come across a stone finger or foot and I know I\'m in cockatrice territory.\"\n—Rulak, bog guide').
card_multiverse_id('deathgaze cockatrice'/'M14', '370775').

card_in_set('demolish', 'M14').
card_original_type('demolish'/'M14', 'Sorcery').
card_original_text('demolish'/'M14', 'Destroy target artifact or land.').
card_image_name('demolish'/'M14', 'demolish').
card_uid('demolish'/'M14', 'M14:Demolish:demolish').
card_rarity('demolish'/'M14', 'Common').
card_artist('demolish'/'M14', 'John Avon').
card_number('demolish'/'M14', '136').
card_flavor_text('demolish'/'M14', '\"This is why I hate settlements. They lull you into a false sense of security.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('demolish'/'M14', '370621').

card_in_set('devout invocation', 'M14').
card_original_type('devout invocation'/'M14', 'Sorcery').
card_original_text('devout invocation'/'M14', 'Tap any number of untapped creatures you control. Put a 4/4 white Angel creature token with flying onto the battlefield for each creature tapped this way.').
card_first_print('devout invocation', 'M14').
card_image_name('devout invocation'/'M14', 'devout invocation').
card_uid('devout invocation'/'M14', 'M14:Devout Invocation:devout invocation').
card_rarity('devout invocation'/'M14', 'Mythic Rare').
card_artist('devout invocation'/'M14', 'David Palumbo').
card_number('devout invocation'/'M14', '16').
card_flavor_text('devout invocation'/'M14', 'A prayer to bring demons to their knees.').
card_multiverse_id('devout invocation'/'M14', '370726').

card_in_set('diabolic tutor', 'M14').
card_original_type('diabolic tutor'/'M14', 'Sorcery').
card_original_text('diabolic tutor'/'M14', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'M14', 'diabolic tutor').
card_uid('diabolic tutor'/'M14', 'M14:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'M14', 'Uncommon').
card_artist('diabolic tutor'/'M14', 'Greg Staples').
card_number('diabolic tutor'/'M14', '95').
card_flavor_text('diabolic tutor'/'M14', 'The wise always keep an ear open to the whispers of power.').
card_multiverse_id('diabolic tutor'/'M14', '370732').

card_in_set('dismiss into dream', 'M14').
card_original_type('dismiss into dream'/'M14', 'Enchantment').
card_original_text('dismiss into dream'/'M14', 'Each creature your opponents control is an Illusion in addition to its other types and has \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_first_print('dismiss into dream', 'M14').
card_image_name('dismiss into dream'/'M14', 'dismiss into dream').
card_uid('dismiss into dream'/'M14', 'M14:Dismiss into Dream:dismiss into dream').
card_rarity('dismiss into dream'/'M14', 'Rare').
card_artist('dismiss into dream'/'M14', 'Sam Wolfe Connelly').
card_number('dismiss into dream'/'M14', '50').
card_flavor_text('dismiss into dream'/'M14', '\"You trust in the shadows of a breeze.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('dismiss into dream'/'M14', '370796').

card_in_set('disperse', 'M14').
card_original_type('disperse'/'M14', 'Instant').
card_original_text('disperse'/'M14', 'Return target nonland permanent to its owner\'s hand.').
card_image_name('disperse'/'M14', 'disperse').
card_uid('disperse'/'M14', 'M14:Disperse:disperse').
card_rarity('disperse'/'M14', 'Common').
card_artist('disperse'/'M14', 'Steve Ellis').
card_number('disperse'/'M14', '51').
card_flavor_text('disperse'/'M14', 'Gryffid found his way home and resumed life as best he could, constantly hiding his newfound fear of clouds.').
card_multiverse_id('disperse'/'M14', '370818').

card_in_set('divination', 'M14').
card_original_type('divination'/'M14', 'Sorcery').
card_original_text('divination'/'M14', 'Draw two cards.').
card_image_name('divination'/'M14', 'divination').
card_uid('divination'/'M14', 'M14:Divination:divination').
card_rarity('divination'/'M14', 'Common').
card_artist('divination'/'M14', 'Howard Lyon').
card_number('divination'/'M14', '52').
card_flavor_text('divination'/'M14', '\"The key to unlocking this puzzle is within you.\"\n—Doriel, mentor of Mistral Isle').
card_multiverse_id('divination'/'M14', '370616').

card_in_set('divine favor', 'M14').
card_original_type('divine favor'/'M14', 'Enchantment — Aura').
card_original_text('divine favor'/'M14', 'Enchant creatureWhen Divine Favor enters the battlefield, you gain 3 life.Enchanted creature gets +1/+3.').
card_image_name('divine favor'/'M14', 'divine favor').
card_uid('divine favor'/'M14', 'M14:Divine Favor:divine favor').
card_rarity('divine favor'/'M14', 'Common').
card_artist('divine favor'/'M14', 'Allen Williams').
card_number('divine favor'/'M14', '17').
card_flavor_text('divine favor'/'M14', 'With an armory of light, even the squire may champion her people.').
card_multiverse_id('divine favor'/'M14', '370748').

card_in_set('domestication', 'M14').
card_original_type('domestication'/'M14', 'Enchantment — Aura').
card_original_text('domestication'/'M14', 'Enchant creatureYou control enchanted creature.At the beginning of your end step, if enchanted creature\'s power is 4 or greater, sacrifice Domestication.').
card_image_name('domestication'/'M14', 'domestication').
card_uid('domestication'/'M14', 'M14:Domestication:domestication').
card_rarity('domestication'/'M14', 'Rare').
card_artist('domestication'/'M14', 'Jesper Ejsing').
card_number('domestication'/'M14', '53').
card_multiverse_id('domestication'/'M14', '370783').

card_in_set('doom blade', 'M14').
card_original_type('doom blade'/'M14', 'Instant').
card_original_text('doom blade'/'M14', 'Destroy target nonblack creature.').
card_image_name('doom blade'/'M14', 'doom blade').
card_uid('doom blade'/'M14', 'M14:Doom Blade:doom blade').
card_rarity('doom blade'/'M14', 'Uncommon').
card_artist('doom blade'/'M14', 'Chippy').
card_number('doom blade'/'M14', '96').
card_multiverse_id('doom blade'/'M14', '370609').

card_in_set('door of destinies', 'M14').
card_original_type('door of destinies'/'M14', 'Artifact').
card_original_text('door of destinies'/'M14', 'As Door of Destinies enters the battlefield, choose a creature type.Whenever you cast a spell of the chosen type, put a charge counter on Door of Destinies.Creatures you control of the chosen type get +1/+1 for each charge counter on Door of Destinies.').
card_image_name('door of destinies'/'M14', 'door of destinies').
card_uid('door of destinies'/'M14', 'M14:Door of Destinies:door of destinies').
card_rarity('door of destinies'/'M14', 'Rare').
card_artist('door of destinies'/'M14', 'Larry MacDougall').
card_number('door of destinies'/'M14', '208').
card_multiverse_id('door of destinies'/'M14', '370699').

card_in_set('dragon egg', 'M14').
card_original_type('dragon egg'/'M14', 'Creature — Dragon').
card_original_text('dragon egg'/'M14', 'Defender (This creature can\'t attack.)When Dragon Egg dies, put a 2/2 red Dragon creature token with flying onto the battlefield. It has \"{R}: This creature gets +1/+0 until end of turn.\"').
card_first_print('dragon egg', 'M14').
card_image_name('dragon egg'/'M14', 'dragon egg').
card_uid('dragon egg'/'M14', 'M14:Dragon Egg:dragon egg').
card_rarity('dragon egg'/'M14', 'Uncommon').
card_artist('dragon egg'/'M14', 'Jack Wang').
card_number('dragon egg'/'M14', '137').
card_flavor_text('dragon egg'/'M14', 'Dragon birth lairs are littered with treasure to entice the young from their eggs.').
card_multiverse_id('dragon egg'/'M14', '370660').

card_in_set('dragon hatchling', 'M14').
card_original_type('dragon hatchling'/'M14', 'Creature — Dragon').
card_original_text('dragon hatchling'/'M14', 'Flying{R}: Dragon Hatchling gets +1/+0 until end of turn.').
card_image_name('dragon hatchling'/'M14', 'dragon hatchling').
card_uid('dragon hatchling'/'M14', 'M14:Dragon Hatchling:dragon hatchling').
card_rarity('dragon hatchling'/'M14', 'Common').
card_artist('dragon hatchling'/'M14', 'David Palumbo').
card_number('dragon hatchling'/'M14', '138').
card_flavor_text('dragon hatchling'/'M14', '\"Those dragons grow fast. For a while they feed on squirrels and goblins and then suddenly you\'re missing a mammoth.\"\n—Hurdek, Mazar mammoth trainer').
card_multiverse_id('dragon hatchling'/'M14', '370717').

card_in_set('duress', 'M14').
card_original_type('duress'/'M14', 'Sorcery').
card_original_text('duress'/'M14', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'M14', 'duress').
card_uid('duress'/'M14', 'M14:Duress:duress').
card_rarity('duress'/'M14', 'Common').
card_artist('duress'/'M14', 'Steven Belledin').
card_number('duress'/'M14', '97').
card_flavor_text('duress'/'M14', '\"It hurts more if you think about it.\"\n—Hooks, Cabal torturer').
card_multiverse_id('duress'/'M14', '370577').

card_in_set('elite arcanist', 'M14').
card_original_type('elite arcanist'/'M14', 'Creature — Human Wizard').
card_original_text('elite arcanist'/'M14', 'When Elite Arcanist enters the battlefield, you may exile an instant card from your hand.{X}, {T}: Copy the exiled card. You may cast the copy without paying its mana cost. X is the converted mana cost of the exiled card.').
card_first_print('elite arcanist', 'M14').
card_image_name('elite arcanist'/'M14', 'elite arcanist').
card_uid('elite arcanist'/'M14', 'M14:Elite Arcanist:elite arcanist').
card_rarity('elite arcanist'/'M14', 'Rare').
card_artist('elite arcanist'/'M14', 'James Zapata').
card_number('elite arcanist'/'M14', '54').
card_multiverse_id('elite arcanist'/'M14', '370747').

card_in_set('elixir of immortality', 'M14').
card_original_type('elixir of immortality'/'M14', 'Artifact').
card_original_text('elixir of immortality'/'M14', '{2}, {T}: You gain 5 life. Shuffle Elixir of Immortality and your graveyard into their owner\'s library.').
card_image_name('elixir of immortality'/'M14', 'elixir of immortality').
card_uid('elixir of immortality'/'M14', 'M14:Elixir of Immortality:elixir of immortality').
card_rarity('elixir of immortality'/'M14', 'Uncommon').
card_artist('elixir of immortality'/'M14', 'Zoltan Boros & Gabor Szikszai').
card_number('elixir of immortality'/'M14', '209').
card_flavor_text('elixir of immortality'/'M14', '\"Bottled life. Not as tasty as I\'m used to, rather stale, but it has the same effect.\"\n—Baron Sengir').
card_multiverse_id('elixir of immortality'/'M14', '370681').

card_in_set('elvish mystic', 'M14').
card_original_type('elvish mystic'/'M14', 'Creature — Elf Druid').
card_original_text('elvish mystic'/'M14', '{T}: Add {G} to your mana pool.').
card_image_name('elvish mystic'/'M14', 'elvish mystic').
card_uid('elvish mystic'/'M14', 'M14:Elvish Mystic:elvish mystic').
card_rarity('elvish mystic'/'M14', 'Common').
card_artist('elvish mystic'/'M14', 'Wesley Burt').
card_number('elvish mystic'/'M14', '169').
card_flavor_text('elvish mystic'/'M14', '\"Life grows everywhere. My kin merely find those places where it grows strongest.\"\n—Nissa Revane').
card_multiverse_id('elvish mystic'/'M14', '370744').

card_in_set('encroaching wastes', 'M14').
card_original_type('encroaching wastes'/'M14', 'Land').
card_original_text('encroaching wastes'/'M14', '{T}: Add {1} to your mana pool.{4}, {T}, Sacrifice Encroaching Wastes: Destroy target nonbasic land.').
card_image_name('encroaching wastes'/'M14', 'encroaching wastes').
card_uid('encroaching wastes'/'M14', 'M14:Encroaching Wastes:encroaching wastes').
card_rarity('encroaching wastes'/'M14', 'Uncommon').
card_artist('encroaching wastes'/'M14', 'Noah Bradley').
card_number('encroaching wastes'/'M14', '227').
card_flavor_text('encroaching wastes'/'M14', 'Every world is a work in progress, constantly reshaped by time, disasters, and even the powerful magic of Planeswalkers.').
card_multiverse_id('encroaching wastes'/'M14', '370769').

card_in_set('enlarge', 'M14').
card_original_type('enlarge'/'M14', 'Sorcery').
card_original_text('enlarge'/'M14', 'Target creature gets +7/+7 and gains trample until end of turn. It must be blocked this turn if able. (If a creature with trample would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('enlarge', 'M14').
card_image_name('enlarge'/'M14', 'enlarge').
card_uid('enlarge'/'M14', 'M14:Enlarge:enlarge').
card_rarity('enlarge'/'M14', 'Uncommon').
card_artist('enlarge'/'M14', 'Michael Komarck').
card_number('enlarge'/'M14', '170').
card_multiverse_id('enlarge'/'M14', '370797').

card_in_set('essence scatter', 'M14').
card_original_type('essence scatter'/'M14', 'Instant').
card_original_text('essence scatter'/'M14', 'Counter target creature spell.').
card_image_name('essence scatter'/'M14', 'essence scatter').
card_uid('essence scatter'/'M14', 'M14:Essence Scatter:essence scatter').
card_rarity('essence scatter'/'M14', 'Common').
card_artist('essence scatter'/'M14', 'Jon Foster').
card_number('essence scatter'/'M14', '55').
card_flavor_text('essence scatter'/'M14', '\"What you attempt to pull from the Æther, I can spread onto the wind.\"\n—Jace Beleren').
card_multiverse_id('essence scatter'/'M14', '370694').

card_in_set('festering newt', 'M14').
card_original_type('festering newt'/'M14', 'Creature — Salamander').
card_original_text('festering newt'/'M14', 'When Festering Newt dies, target creature an opponent controls gets -1/-1 until end of turn. That creature gets -4/-4 instead if you control a creature named Bogbrew Witch.').
card_first_print('festering newt', 'M14').
card_image_name('festering newt'/'M14', 'festering newt').
card_uid('festering newt'/'M14', 'M14:Festering Newt:festering newt').
card_rarity('festering newt'/'M14', 'Common').
card_artist('festering newt'/'M14', 'Eric Deschamps').
card_number('festering newt'/'M14', '98').
card_flavor_text('festering newt'/'M14', 'Its back bubbles like a witch\'s cauldron, and it smells just as vile.').
card_multiverse_id('festering newt'/'M14', '370772').

card_in_set('fiendslayer paladin', 'M14').
card_original_type('fiendslayer paladin'/'M14', 'Creature — Human Knight').
card_original_text('fiendslayer paladin'/'M14', 'First strike (This creature deals combat damage before creatures without first strike.) Lifelink (Damage dealt by this creature also causes you to gain that much life.)Fiendslayer Paladin can\'t be the target of black or red spells your opponents control.').
card_first_print('fiendslayer paladin', 'M14').
card_image_name('fiendslayer paladin'/'M14', 'fiendslayer paladin').
card_uid('fiendslayer paladin'/'M14', 'M14:Fiendslayer Paladin:fiendslayer paladin').
card_rarity('fiendslayer paladin'/'M14', 'Rare').
card_artist('fiendslayer paladin'/'M14', 'Wesley Burt').
card_number('fiendslayer paladin'/'M14', '18').
card_multiverse_id('fiendslayer paladin'/'M14', '370786').

card_in_set('fireshrieker', 'M14').
card_original_type('fireshrieker'/'M14', 'Artifact — Equipment').
card_original_text('fireshrieker'/'M14', 'Equipped creature has double strike. (It deals both first-strike and regular combat damage.)Equip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('fireshrieker'/'M14', 'fireshrieker').
card_uid('fireshrieker'/'M14', 'M14:Fireshrieker:fireshrieker').
card_rarity('fireshrieker'/'M14', 'Uncommon').
card_artist('fireshrieker'/'M14', 'Christopher Moeller').
card_number('fireshrieker'/'M14', '210').
card_multiverse_id('fireshrieker'/'M14', '370715').

card_in_set('flames of the firebrand', 'M14').
card_original_type('flames of the firebrand'/'M14', 'Sorcery').
card_original_text('flames of the firebrand'/'M14', 'Flames of the Firebrand deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_image_name('flames of the firebrand'/'M14', 'flames of the firebrand').
card_uid('flames of the firebrand'/'M14', 'M14:Flames of the Firebrand:flames of the firebrand').
card_rarity('flames of the firebrand'/'M14', 'Uncommon').
card_artist('flames of the firebrand'/'M14', 'Steve Argyle').
card_number('flames of the firebrand'/'M14', '139').
card_flavor_text('flames of the firebrand'/'M14', '\"You\'re in luck. I brought enough to share.\"\n—Chandra Nalaar').
card_multiverse_id('flames of the firebrand'/'M14', '370824').

card_in_set('fleshpulper giant', 'M14').
card_original_type('fleshpulper giant'/'M14', 'Creature — Giant').
card_original_text('fleshpulper giant'/'M14', 'When Fleshpulper Giant enters the battlefield, you may destroy target creature with toughness 2 or less.').
card_first_print('fleshpulper giant', 'M14').
card_image_name('fleshpulper giant'/'M14', 'fleshpulper giant').
card_uid('fleshpulper giant'/'M14', 'M14:Fleshpulper Giant:fleshpulper giant').
card_rarity('fleshpulper giant'/'M14', 'Uncommon').
card_artist('fleshpulper giant'/'M14', 'Alex Horley-Orlandelli').
card_number('fleshpulper giant'/'M14', '140').
card_flavor_text('fleshpulper giant'/'M14', 'He doesn\'t hate small folk. He just likes the squishing sound they make.').
card_multiverse_id('fleshpulper giant'/'M14', '370741').

card_in_set('fog', 'M14').
card_original_type('fog'/'M14', 'Instant').
card_original_text('fog'/'M14', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'M14', 'fog').
card_uid('fog'/'M14', 'M14:Fog:fog').
card_rarity('fog'/'M14', 'Common').
card_artist('fog'/'M14', 'Jaime Jones').
card_number('fog'/'M14', '171').
card_flavor_text('fog'/'M14', '\"I fear no army or beast, but only the morning fog. Our assault can survive everything else.\"\n—Lord Hilneth').
card_multiverse_id('fog'/'M14', '370633').

card_in_set('forest', 'M14').
card_original_type('forest'/'M14', 'Basic Land — Forest').
card_original_text('forest'/'M14', 'G').
card_image_name('forest'/'M14', 'forest1').
card_uid('forest'/'M14', 'M14:Forest:forest1').
card_rarity('forest'/'M14', 'Basic Land').
card_artist('forest'/'M14', 'Volkan Baga').
card_number('forest'/'M14', '246').
card_multiverse_id('forest'/'M14', '370771').

card_in_set('forest', 'M14').
card_original_type('forest'/'M14', 'Basic Land — Forest').
card_original_text('forest'/'M14', 'G').
card_image_name('forest'/'M14', 'forest2').
card_uid('forest'/'M14', 'M14:Forest:forest2').
card_rarity('forest'/'M14', 'Basic Land').
card_artist('forest'/'M14', 'Steven Belledin').
card_number('forest'/'M14', '247').
card_multiverse_id('forest'/'M14', '370729').

card_in_set('forest', 'M14').
card_original_type('forest'/'M14', 'Basic Land — Forest').
card_original_text('forest'/'M14', 'G').
card_image_name('forest'/'M14', 'forest3').
card_uid('forest'/'M14', 'M14:Forest:forest3').
card_rarity('forest'/'M14', 'Basic Land').
card_artist('forest'/'M14', 'Jonas De Ro').
card_number('forest'/'M14', '248').
card_multiverse_id('forest'/'M14', '370598').

card_in_set('forest', 'M14').
card_original_type('forest'/'M14', 'Basic Land — Forest').
card_original_text('forest'/'M14', 'G').
card_image_name('forest'/'M14', 'forest4').
card_uid('forest'/'M14', 'M14:Forest:forest4').
card_rarity('forest'/'M14', 'Basic Land').
card_artist('forest'/'M14', 'Andreas Rocha').
card_number('forest'/'M14', '249').
card_multiverse_id('forest'/'M14', '370756').

card_in_set('fortify', 'M14').
card_original_type('fortify'/'M14', 'Instant').
card_original_text('fortify'/'M14', 'Choose one — Creatures you control get +2/+0 until end of turn; or creatures you control get +0/+2 until end of turn.').
card_image_name('fortify'/'M14', 'fortify').
card_uid('fortify'/'M14', 'M14:Fortify:fortify').
card_rarity('fortify'/'M14', 'Common').
card_artist('fortify'/'M14', 'Christopher Moeller').
card_number('fortify'/'M14', '19').
card_flavor_text('fortify'/'M14', '\"Where metal is tainted and wood is scarce, we are best armed by faith.\"\n—Tavalus, acolyte of Korlis').
card_multiverse_id('fortify'/'M14', '370712').

card_in_set('frost breath', 'M14').
card_original_type('frost breath'/'M14', 'Instant').
card_original_text('frost breath'/'M14', 'Tap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_image_name('frost breath'/'M14', 'frost breath').
card_uid('frost breath'/'M14', 'M14:Frost Breath:frost breath').
card_rarity('frost breath'/'M14', 'Common').
card_artist('frost breath'/'M14', 'Mike Bierek').
card_number('frost breath'/'M14', '56').
card_flavor_text('frost breath'/'M14', '\"I\'ll make ice chimes of your bones.\"').
card_multiverse_id('frost breath'/'M14', '370678').

card_in_set('galerider sliver', 'M14').
card_original_type('galerider sliver'/'M14', 'Creature — Sliver').
card_original_text('galerider sliver'/'M14', 'Sliver creatures you control have flying.').
card_first_print('galerider sliver', 'M14').
card_image_name('galerider sliver'/'M14', 'galerider sliver').
card_uid('galerider sliver'/'M14', 'M14:Galerider Sliver:galerider sliver').
card_rarity('galerider sliver'/'M14', 'Rare').
card_artist('galerider sliver'/'M14', 'James Zapata').
card_number('galerider sliver'/'M14', '57').
card_flavor_text('galerider sliver'/'M14', 'Masters of adaptation, galeriders serve multiple purposes useful to the hive. When they\'re not patrolling their territories, their majestic wings serve to circulate cool air through the vast hive chambers.').
card_multiverse_id('galerider sliver'/'M14', '370590').

card_in_set('garruk\'s horde', 'M14').
card_original_type('garruk\'s horde'/'M14', 'Creature — Beast').
card_original_text('garruk\'s horde'/'M14', 'Trample Play with the top card of your library revealed.You may cast the top card of your library if it\'s a creature card. (Do this only any time you could cast that creature card. You still pay the spell\'s costs.)').
card_image_name('garruk\'s horde'/'M14', 'garruk\'s horde').
card_uid('garruk\'s horde'/'M14', 'M14:Garruk\'s Horde:garruk\'s horde').
card_rarity('garruk\'s horde'/'M14', 'Rare').
card_artist('garruk\'s horde'/'M14', 'Steve Prescott').
card_number('garruk\'s horde'/'M14', '173').
card_multiverse_id('garruk\'s horde'/'M14', '370684').

card_in_set('garruk, caller of beasts', 'M14').
card_original_type('garruk, caller of beasts'/'M14', 'Planeswalker — Garruk').
card_original_text('garruk, caller of beasts'/'M14', '+1: Reveal the top five cards of your library. Put all creature cards revealed this way into your hand and the rest on the bottom of your library in any order.-3: You may put a green creature card from your hand onto the battlefield.-7: You get an emblem with \"Whenever you cast a creature spell, you may search your library for a creature card, put it onto the battlefield, then shuffle your library.\"').
card_image_name('garruk, caller of beasts'/'M14', 'garruk, caller of beasts').
card_uid('garruk, caller of beasts'/'M14', 'M14:Garruk, Caller of Beasts:garruk, caller of beasts').
card_rarity('garruk, caller of beasts'/'M14', 'Mythic Rare').
card_artist('garruk, caller of beasts'/'M14', 'Karl Kopinski').
card_number('garruk, caller of beasts'/'M14', '172').
card_multiverse_id('garruk, caller of beasts'/'M14', '370687').

card_in_set('giant growth', 'M14').
card_original_type('giant growth'/'M14', 'Instant').
card_original_text('giant growth'/'M14', 'Target creature gets +3/+3 until end of turn.').
card_image_name('giant growth'/'M14', 'giant growth').
card_uid('giant growth'/'M14', 'M14:Giant Growth:giant growth').
card_rarity('giant growth'/'M14', 'Common').
card_artist('giant growth'/'M14', 'Matt Cavotta').
card_number('giant growth'/'M14', '174').
card_multiverse_id('giant growth'/'M14', '370788').

card_in_set('giant spider', 'M14').
card_original_type('giant spider'/'M14', 'Creature — Spider').
card_original_text('giant spider'/'M14', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant spider'/'M14', 'giant spider').
card_uid('giant spider'/'M14', 'M14:Giant Spider:giant spider').
card_rarity('giant spider'/'M14', 'Common').
card_artist('giant spider'/'M14', 'Randy Gallegos').
card_number('giant spider'/'M14', '175').
card_flavor_text('giant spider'/'M14', '\"It has a quickness about it that seems unnatural for its large size, yet its hunger is about right.\"\n—Endril, Kalonian naturalist').
card_multiverse_id('giant spider'/'M14', '370781').

card_in_set('gladecover scout', 'M14').
card_original_type('gladecover scout'/'M14', 'Creature — Elf Scout').
card_original_text('gladecover scout'/'M14', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_image_name('gladecover scout'/'M14', 'gladecover scout').
card_uid('gladecover scout'/'M14', 'M14:Gladecover Scout:gladecover scout').
card_rarity('gladecover scout'/'M14', 'Common').
card_artist('gladecover scout'/'M14', 'Allen Williams').
card_number('gladecover scout'/'M14', '176').
card_flavor_text('gladecover scout'/'M14', '\"The forest is my cover and I hold it close. In such a tight embrace, there is no room for wickedness.\"').
card_multiverse_id('gladecover scout'/'M14', '370716').

card_in_set('glimpse the future', 'M14').
card_original_type('glimpse the future'/'M14', 'Sorcery').
card_original_text('glimpse the future'/'M14', 'Look at the top three cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_first_print('glimpse the future', 'M14').
card_image_name('glimpse the future'/'M14', 'glimpse the future').
card_uid('glimpse the future'/'M14', 'M14:Glimpse the Future:glimpse the future').
card_rarity('glimpse the future'/'M14', 'Uncommon').
card_artist('glimpse the future'/'M14', 'Andrew Robinson').
card_number('glimpse the future'/'M14', '58').
card_flavor_text('glimpse the future'/'M14', '\"No simple coin toss can solve this riddle. You must think and choose wisely.\"\n—Shai Fusan, archmage').
card_multiverse_id('glimpse the future'/'M14', '370774').

card_in_set('gnawing zombie', 'M14').
card_original_type('gnawing zombie'/'M14', 'Creature — Zombie').
card_original_text('gnawing zombie'/'M14', '{1}{B}, Sacrifice a creature: Target player loses 1 life and you gain 1 life.').
card_first_print('gnawing zombie', 'M14').
card_image_name('gnawing zombie'/'M14', 'gnawing zombie').
card_uid('gnawing zombie'/'M14', 'M14:Gnawing Zombie:gnawing zombie').
card_rarity('gnawing zombie'/'M14', 'Uncommon').
card_artist('gnawing zombie'/'M14', 'Greg Staples').
card_number('gnawing zombie'/'M14', '99').
card_flavor_text('gnawing zombie'/'M14', 'On still nights you can hear its rotted teeth grinding tirelessly on scavenged bones.').
card_multiverse_id('gnawing zombie'/'M14', '370682').

card_in_set('goblin diplomats', 'M14').
card_original_type('goblin diplomats'/'M14', 'Creature — Goblin').
card_original_text('goblin diplomats'/'M14', '{T}: Each creature attacks this turn if able.').
card_image_name('goblin diplomats'/'M14', 'goblin diplomats').
card_uid('goblin diplomats'/'M14', 'M14:Goblin Diplomats:goblin diplomats').
card_rarity('goblin diplomats'/'M14', 'Rare').
card_artist('goblin diplomats'/'M14', 'Izzy').
card_number('goblin diplomats'/'M14', '141').
card_flavor_text('goblin diplomats'/'M14', 'When you need to convey that special kind of message.').
card_multiverse_id('goblin diplomats'/'M14', '370674').

card_in_set('goblin shortcutter', 'M14').
card_original_type('goblin shortcutter'/'M14', 'Creature — Goblin Scout').
card_original_text('goblin shortcutter'/'M14', 'When Goblin Shortcutter enters the battlefield, target creature can\'t block this turn.').
card_image_name('goblin shortcutter'/'M14', 'goblin shortcutter').
card_uid('goblin shortcutter'/'M14', 'M14:Goblin Shortcutter:goblin shortcutter').
card_rarity('goblin shortcutter'/'M14', 'Common').
card_artist('goblin shortcutter'/'M14', 'Jesper Ejsing').
card_number('goblin shortcutter'/'M14', '142').
card_flavor_text('goblin shortcutter'/'M14', '\"I\'m not running away! I\'m figuring out the best routes.\"').
card_multiverse_id('goblin shortcutter'/'M14', '370610').

card_in_set('griffin sentinel', 'M14').
card_original_type('griffin sentinel'/'M14', 'Creature — Griffin').
card_original_text('griffin sentinel'/'M14', 'FlyingVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('griffin sentinel'/'M14', 'griffin sentinel').
card_uid('griffin sentinel'/'M14', 'M14:Griffin Sentinel:griffin sentinel').
card_rarity('griffin sentinel'/'M14', 'Common').
card_artist('griffin sentinel'/'M14', 'Warren Mahy').
card_number('griffin sentinel'/'M14', '20').
card_flavor_text('griffin sentinel'/'M14', 'Once a griffin sentinel adopts a territory as its own, only death can force it to betray its post.').
card_multiverse_id('griffin sentinel'/'M14', '370792').

card_in_set('grim return', 'M14').
card_original_type('grim return'/'M14', 'Instant').
card_original_text('grim return'/'M14', 'Choose target creature card in a graveyard that was put there from the battlefield this turn. Put that card onto the battlefield under your control.').
card_first_print('grim return', 'M14').
card_image_name('grim return'/'M14', 'grim return').
card_uid('grim return'/'M14', 'M14:Grim Return:grim return').
card_rarity('grim return'/'M14', 'Rare').
card_artist('grim return'/'M14', 'Seb McKinnon').
card_number('grim return'/'M14', '100').
card_flavor_text('grim return'/'M14', 'The necromancer\'s touch strips away memory and loyalty, and only strength and malice remain.').
card_multiverse_id('grim return'/'M14', '370776').

card_in_set('groundshaker sliver', 'M14').
card_original_type('groundshaker sliver'/'M14', 'Creature — Sliver').
card_original_text('groundshaker sliver'/'M14', 'Sliver creatures you control have trample. (If a Sliver you control would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('groundshaker sliver', 'M14').
card_image_name('groundshaker sliver'/'M14', 'groundshaker sliver').
card_uid('groundshaker sliver'/'M14', 'M14:Groundshaker Sliver:groundshaker sliver').
card_rarity('groundshaker sliver'/'M14', 'Common').
card_artist('groundshaker sliver'/'M14', 'Chase Stone').
card_number('groundshaker sliver'/'M14', '177').
card_multiverse_id('groundshaker sliver'/'M14', '370626').

card_in_set('guardian of the ages', 'M14').
card_original_type('guardian of the ages'/'M14', 'Artifact Creature — Golem').
card_original_text('guardian of the ages'/'M14', 'Defender (This creature can\'t attack.)When a creature attacks you or a planeswalker you control, if Guardian of the Ages has defender, it loses defender and gains trample.').
card_first_print('guardian of the ages', 'M14').
card_image_name('guardian of the ages'/'M14', 'guardian of the ages').
card_uid('guardian of the ages'/'M14', 'M14:Guardian of the Ages:guardian of the ages').
card_rarity('guardian of the ages'/'M14', 'Rare').
card_artist('guardian of the ages'/'M14', 'Ryan Pancoast').
card_number('guardian of the ages'/'M14', '211').
card_flavor_text('guardian of the ages'/'M14', '\"At my feet, invasions end.\"\n—Statue inscription').
card_multiverse_id('guardian of the ages'/'M14', '370603').

card_in_set('haunted plate mail', 'M14').
card_original_type('haunted plate mail'/'M14', 'Artifact — Equipment').
card_original_text('haunted plate mail'/'M14', 'Equipped creature gets +4/+4.{0}: Until end of turn, Haunted Plate Mail becomes a 4/4 Spirit artifact creature that\'s no longer an Equipment. Activate this ability only if you control no creatures.Equip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('haunted plate mail', 'M14').
card_image_name('haunted plate mail'/'M14', 'haunted plate mail').
card_uid('haunted plate mail'/'M14', 'M14:Haunted Plate Mail:haunted plate mail').
card_rarity('haunted plate mail'/'M14', 'Rare').
card_artist('haunted plate mail'/'M14', 'Izzy').
card_number('haunted plate mail'/'M14', '212').
card_multiverse_id('haunted plate mail'/'M14', '370594').

card_in_set('hive stirrings', 'M14').
card_original_type('hive stirrings'/'M14', 'Sorcery').
card_original_text('hive stirrings'/'M14', 'Put two 1/1 colorless Sliver creature tokens onto the battlefield.').
card_image_name('hive stirrings'/'M14', 'hive stirrings').
card_uid('hive stirrings'/'M14', 'M14:Hive Stirrings:hive stirrings').
card_rarity('hive stirrings'/'M14', 'Common').
card_artist('hive stirrings'/'M14', 'Maciej Kuciara').
card_number('hive stirrings'/'M14', '21').
card_flavor_text('hive stirrings'/'M14', 'Sliver young are sorted into clutches according to their potential and their future role. Human scholars can only guess how those are determined.').
card_multiverse_id('hive stirrings'/'M14', '370817').

card_in_set('howl of the night pack', 'M14').
card_original_type('howl of the night pack'/'M14', 'Sorcery').
card_original_text('howl of the night pack'/'M14', 'Put a 2/2 green Wolf creature token onto the battlefield for each Forest you control.').
card_image_name('howl of the night pack'/'M14', 'howl of the night pack').
card_uid('howl of the night pack'/'M14', 'M14:Howl of the Night Pack:howl of the night pack').
card_rarity('howl of the night pack'/'M14', 'Uncommon').
card_artist('howl of the night pack'/'M14', 'Lars Grant-West').
card_number('howl of the night pack'/'M14', '178').
card_flavor_text('howl of the night pack'/'M14', 'The murderous horrors of Raven\'s Run are legendary, but even that haunted place goes quiet when the night wolves howl.').
card_multiverse_id('howl of the night pack'/'M14', '370718').

card_in_set('hunt the weak', 'M14').
card_original_type('hunt the weak'/'M14', 'Sorcery').
card_original_text('hunt the weak'/'M14', 'Put a +1/+1 counter on target creature you control. Then that creature fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_first_print('hunt the weak', 'M14').
card_image_name('hunt the weak'/'M14', 'hunt the weak').
card_uid('hunt the weak'/'M14', 'M14:Hunt the Weak:hunt the weak').
card_rarity('hunt the weak'/'M14', 'Common').
card_artist('hunt the weak'/'M14', 'Raoul Vitale').
card_number('hunt the weak'/'M14', '179').
card_flavor_text('hunt the weak'/'M14', 'He who hesitates is lunch.').
card_multiverse_id('hunt the weak'/'M14', '370743').

card_in_set('illusionary armor', 'M14').
card_original_type('illusionary armor'/'M14', 'Enchantment — Aura').
card_original_text('illusionary armor'/'M14', 'Enchant creatureEnchanted creature gets +4/+4.When enchanted creature becomes the target of a spell or ability, sacrifice Illusionary Armor.').
card_first_print('illusionary armor', 'M14').
card_image_name('illusionary armor'/'M14', 'illusionary armor').
card_uid('illusionary armor'/'M14', 'M14:Illusionary Armor:illusionary armor').
card_rarity('illusionary armor'/'M14', 'Uncommon').
card_artist('illusionary armor'/'M14', 'Mathias Kollros').
card_number('illusionary armor'/'M14', '59').
card_multiverse_id('illusionary armor'/'M14', '370701').

card_in_set('imposing sovereign', 'M14').
card_original_type('imposing sovereign'/'M14', 'Creature — Human').
card_original_text('imposing sovereign'/'M14', 'Creatures your opponents control enter the battlefield tapped.').
card_first_print('imposing sovereign', 'M14').
card_image_name('imposing sovereign'/'M14', 'imposing sovereign').
card_uid('imposing sovereign'/'M14', 'M14:Imposing Sovereign:imposing sovereign').
card_rarity('imposing sovereign'/'M14', 'Rare').
card_artist('imposing sovereign'/'M14', 'Scott M. Fischer').
card_number('imposing sovereign'/'M14', '22').
card_flavor_text('imposing sovereign'/'M14', 'Some are born to rule. The rest are born to bow before them.').
card_multiverse_id('imposing sovereign'/'M14', '370770').

card_in_set('indestructibility', 'M14').
card_original_type('indestructibility'/'M14', 'Enchantment — Aura').
card_original_text('indestructibility'/'M14', 'Enchant permanentEnchanted permanent has indestructible. (Effects that say \"destroy\" don\'t destroy that permanent. A creature with indestructible can\'t be destroyed by damage.)').
card_image_name('indestructibility'/'M14', 'indestructibility').
card_uid('indestructibility'/'M14', 'M14:Indestructibility:indestructibility').
card_rarity('indestructibility'/'M14', 'Rare').
card_artist('indestructibility'/'M14', 'Darrell Riche').
card_number('indestructibility'/'M14', '23').
card_flavor_text('indestructibility'/'M14', '\"By all means, keep trying.\"').
card_multiverse_id('indestructibility'/'M14', '370673').

card_in_set('into the wilds', 'M14').
card_original_type('into the wilds'/'M14', 'Enchantment').
card_original_text('into the wilds'/'M14', 'At the beginning of your upkeep, look at the top card of your library. If it\'s a land card, you may put it onto the battlefield.').
card_first_print('into the wilds', 'M14').
card_image_name('into the wilds'/'M14', 'into the wilds').
card_uid('into the wilds'/'M14', 'M14:Into the Wilds:into the wilds').
card_rarity('into the wilds'/'M14', 'Rare').
card_artist('into the wilds'/'M14', 'Véronique Meignaud').
card_number('into the wilds'/'M14', '180').
card_flavor_text('into the wilds'/'M14', '\"Wonders hide where the trees grow thickest.\"\n—Mul Daya proverb').
card_multiverse_id('into the wilds'/'M14', '370579').

card_in_set('island', 'M14').
card_original_type('island'/'M14', 'Basic Land — Island').
card_original_text('island'/'M14', 'U').
card_image_name('island'/'M14', 'island1').
card_uid('island'/'M14', 'M14:Island:island1').
card_rarity('island'/'M14', 'Basic Land').
card_artist('island'/'M14', 'Noah Bradley').
card_number('island'/'M14', '234').
card_multiverse_id('island'/'M14', '370773').

card_in_set('island', 'M14').
card_original_type('island'/'M14', 'Basic Land — Island').
card_original_text('island'/'M14', 'U').
card_image_name('island'/'M14', 'island2').
card_uid('island'/'M14', 'M14:Island:island2').
card_rarity('island'/'M14', 'Basic Land').
card_artist('island'/'M14', 'Cliff Childs').
card_number('island'/'M14', '235').
card_multiverse_id('island'/'M14', '370608').

card_in_set('island', 'M14').
card_original_type('island'/'M14', 'Basic Land — Island').
card_original_text('island'/'M14', 'U').
card_image_name('island'/'M14', 'island3').
card_uid('island'/'M14', 'M14:Island:island3').
card_rarity('island'/'M14', 'Basic Land').
card_artist('island'/'M14', 'Jonas De Ro').
card_number('island'/'M14', '236').
card_multiverse_id('island'/'M14', '370611').

card_in_set('island', 'M14').
card_original_type('island'/'M14', 'Basic Land — Island').
card_original_text('island'/'M14', 'U').
card_image_name('island'/'M14', 'island4').
card_uid('island'/'M14', 'M14:Island:island4').
card_rarity('island'/'M14', 'Basic Land').
card_artist('island'/'M14', 'Andreas Rocha').
card_number('island'/'M14', '237').
card_multiverse_id('island'/'M14', '370647').

card_in_set('jace\'s mindseeker', 'M14').
card_original_type('jace\'s mindseeker'/'M14', 'Creature — Fish Illusion').
card_original_text('jace\'s mindseeker'/'M14', 'FlyingWhen Jace\'s Mindseeker enters the battlefield, target opponent puts the top five cards of his or her library into his or her graveyard. You may cast an instant or sorcery card from among them without paying its mana cost.').
card_first_print('jace\'s mindseeker', 'M14').
card_image_name('jace\'s mindseeker'/'M14', 'jace\'s mindseeker').
card_uid('jace\'s mindseeker'/'M14', 'M14:Jace\'s Mindseeker:jace\'s mindseeker').
card_rarity('jace\'s mindseeker'/'M14', 'Rare').
card_artist('jace\'s mindseeker'/'M14', 'Greg Staples').
card_number('jace\'s mindseeker'/'M14', '61').
card_multiverse_id('jace\'s mindseeker'/'M14', '370638').

card_in_set('jace, memory adept', 'M14').
card_original_type('jace, memory adept'/'M14', 'Planeswalker — Jace').
card_original_text('jace, memory adept'/'M14', '+1: Draw a card. Target player puts the top card of his or her library into his or her graveyard.0: Target player puts the top ten cards of his or her library into his or her graveyard.-7: Any number of target players each draw twenty cards.').
card_image_name('jace, memory adept'/'M14', 'jace, memory adept').
card_uid('jace, memory adept'/'M14', 'M14:Jace, Memory Adept:jace, memory adept').
card_rarity('jace, memory adept'/'M14', 'Mythic Rare').
card_artist('jace, memory adept'/'M14', 'D. Alexander Gregory').
card_number('jace, memory adept'/'M14', '60').
card_multiverse_id('jace, memory adept'/'M14', '370728').

card_in_set('kalonian hydra', 'M14').
card_original_type('kalonian hydra'/'M14', 'Creature — Hydra').
card_original_text('kalonian hydra'/'M14', 'TrampleKalonian Hydra enters the battlefield with four +1/+1 counters on it.Whenever Kalonian Hydra attacks, double the number of +1/+1 counters on each creature you control.').
card_first_print('kalonian hydra', 'M14').
card_image_name('kalonian hydra'/'M14', 'kalonian hydra').
card_uid('kalonian hydra'/'M14', 'M14:Kalonian Hydra:kalonian hydra').
card_rarity('kalonian hydra'/'M14', 'Mythic Rare').
card_artist('kalonian hydra'/'M14', 'Chris Rahn').
card_number('kalonian hydra'/'M14', '181').
card_flavor_text('kalonian hydra'/'M14', 'Even baloths fear its feeding time.').
card_multiverse_id('kalonian hydra'/'M14', '370766').

card_in_set('kalonian tusker', 'M14').
card_original_type('kalonian tusker'/'M14', 'Creature — Beast').
card_original_text('kalonian tusker'/'M14', '').
card_first_print('kalonian tusker', 'M14').
card_image_name('kalonian tusker'/'M14', 'kalonian tusker').
card_uid('kalonian tusker'/'M14', 'M14:Kalonian Tusker:kalonian tusker').
card_rarity('kalonian tusker'/'M14', 'Uncommon').
card_artist('kalonian tusker'/'M14', 'Svetlin Velinov').
card_number('kalonian tusker'/'M14', '182').
card_flavor_text('kalonian tusker'/'M14', '\"And all this time I thought we were tracking it.\"\n—Juruk, Kalonian tracker').
card_multiverse_id('kalonian tusker'/'M14', '370700').

card_in_set('lava axe', 'M14').
card_original_type('lava axe'/'M14', 'Sorcery').
card_original_text('lava axe'/'M14', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'M14', 'lava axe').
card_uid('lava axe'/'M14', 'M14:Lava Axe:lava axe').
card_rarity('lava axe'/'M14', 'Common').
card_artist('lava axe'/'M14', 'Brian Snõddy').
card_number('lava axe'/'M14', '143').
card_flavor_text('lava axe'/'M14', 'A strict upgrade over the cinder hatchet.').
card_multiverse_id('lava axe'/'M14', '370595').

card_in_set('lay of the land', 'M14').
card_original_type('lay of the land'/'M14', 'Sorcery').
card_original_text('lay of the land'/'M14', 'Search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('lay of the land'/'M14', 'lay of the land').
card_uid('lay of the land'/'M14', 'M14:Lay of the Land:lay of the land').
card_rarity('lay of the land'/'M14', 'Common').
card_artist('lay of the land'/'M14', 'Chuck Lukacs').
card_number('lay of the land'/'M14', '183').
card_flavor_text('lay of the land'/'M14', 'Victory favors neither the righteous nor the wicked. It favors the prepared.').
card_multiverse_id('lay of the land'/'M14', '370767').

card_in_set('lifebane zombie', 'M14').
card_original_type('lifebane zombie'/'M14', 'Creature — Zombie Warrior').
card_original_text('lifebane zombie'/'M14', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)When Lifebane Zombie enters the battlefield, target opponent reveals his or her hand. You choose a green or white creature card from it and exile that card.').
card_first_print('lifebane zombie', 'M14').
card_image_name('lifebane zombie'/'M14', 'lifebane zombie').
card_uid('lifebane zombie'/'M14', 'M14:Lifebane Zombie:lifebane zombie').
card_rarity('lifebane zombie'/'M14', 'Rare').
card_artist('lifebane zombie'/'M14', 'Min Yum').
card_number('lifebane zombie'/'M14', '101').
card_multiverse_id('lifebane zombie'/'M14', '370723').

card_in_set('lightning talons', 'M14').
card_original_type('lightning talons'/'M14', 'Enchantment — Aura').
card_original_text('lightning talons'/'M14', 'Enchant creatureEnchanted creature gets +3/+0 and has first strike. (It deals combat damage before creatures without first strike.)').
card_image_name('lightning talons'/'M14', 'lightning talons').
card_uid('lightning talons'/'M14', 'M14:Lightning Talons:lightning talons').
card_rarity('lightning talons'/'M14', 'Common').
card_artist('lightning talons'/'M14', 'Johann Bodin').
card_number('lightning talons'/'M14', '144').
card_flavor_text('lightning talons'/'M14', '\"The victim was either clawed to death or struck by lightning. Possibly both.\"\n—Pel Javya, Wojek investigator').
card_multiverse_id('lightning talons'/'M14', '370795').

card_in_set('liliana of the dark realms', 'M14').
card_original_type('liliana of the dark realms'/'M14', 'Planeswalker — Liliana').
card_original_text('liliana of the dark realms'/'M14', '+1: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.-3: Target creature gets +X/+X or -X/-X until end of turn, where X is the number of Swamps you control.-6: You get an emblem with \"Swamps you control have ‘{T}: Add {B}{B}{B}{B} to your mana pool.\'\"').
card_image_name('liliana of the dark realms'/'M14', 'liliana of the dark realms').
card_uid('liliana of the dark realms'/'M14', 'M14:Liliana of the Dark Realms:liliana of the dark realms').
card_rarity('liliana of the dark realms'/'M14', 'Mythic Rare').
card_artist('liliana of the dark realms'/'M14', 'D. Alexander Gregory').
card_number('liliana of the dark realms'/'M14', '102').
card_multiverse_id('liliana of the dark realms'/'M14', '370658').

card_in_set('liliana\'s reaver', 'M14').
card_original_type('liliana\'s reaver'/'M14', 'Creature — Zombie').
card_original_text('liliana\'s reaver'/'M14', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)Whenever Liliana\'s Reaver deals combat damage to a player, that player discards a card and you put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_first_print('liliana\'s reaver', 'M14').
card_image_name('liliana\'s reaver'/'M14', 'liliana\'s reaver').
card_uid('liliana\'s reaver'/'M14', 'M14:Liliana\'s Reaver:liliana\'s reaver').
card_rarity('liliana\'s reaver'/'M14', 'Rare').
card_artist('liliana\'s reaver'/'M14', 'Karl Kopinski').
card_number('liliana\'s reaver'/'M14', '103').
card_multiverse_id('liliana\'s reaver'/'M14', '370740').

card_in_set('liturgy of blood', 'M14').
card_original_type('liturgy of blood'/'M14', 'Sorcery').
card_original_text('liturgy of blood'/'M14', 'Destroy target creature. Add {B}{B}{B} to your mana pool.').
card_first_print('liturgy of blood', 'M14').
card_image_name('liturgy of blood'/'M14', 'liturgy of blood').
card_uid('liturgy of blood'/'M14', 'M14:Liturgy of Blood:liturgy of blood').
card_rarity('liturgy of blood'/'M14', 'Common').
card_artist('liturgy of blood'/'M14', 'Zack Stella').
card_number('liturgy of blood'/'M14', '104').
card_flavor_text('liturgy of blood'/'M14', '\"You harbor such vast potential. It would be such a shame to let you die of old age.\"\n—Zul Ashur, lich lord').
card_multiverse_id('liturgy of blood'/'M14', '370652').

card_in_set('manaweft sliver', 'M14').
card_original_type('manaweft sliver'/'M14', 'Creature — Sliver').
card_original_text('manaweft sliver'/'M14', 'Sliver creatures you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_first_print('manaweft sliver', 'M14').
card_image_name('manaweft sliver'/'M14', 'manaweft sliver').
card_uid('manaweft sliver'/'M14', 'M14:Manaweft Sliver:manaweft sliver').
card_rarity('manaweft sliver'/'M14', 'Uncommon').
card_artist('manaweft sliver'/'M14', 'Trevor Claxton').
card_number('manaweft sliver'/'M14', '184').
card_flavor_text('manaweft sliver'/'M14', '\"I see in their interconnectedness a strange embodiment of the natural order.\"\n—Dionus, elvish archdruid').
card_multiverse_id('manaweft sliver'/'M14', '370599').

card_in_set('marauding maulhorn', 'M14').
card_original_type('marauding maulhorn'/'M14', 'Creature — Beast').
card_original_text('marauding maulhorn'/'M14', 'Marauding Maulhorn attacks each combat if able unless you control a creature named Advocate of the Beast.').
card_first_print('marauding maulhorn', 'M14').
card_image_name('marauding maulhorn'/'M14', 'marauding maulhorn').
card_uid('marauding maulhorn'/'M14', 'M14:Marauding Maulhorn:marauding maulhorn').
card_rarity('marauding maulhorn'/'M14', 'Common').
card_artist('marauding maulhorn'/'M14', 'Jesper Ejsing').
card_number('marauding maulhorn'/'M14', '145').
card_flavor_text('marauding maulhorn'/'M14', '\"It\'s not just you. Everyone makes it angry.\"\n—Ardenoth, elvish nature advocate').
card_multiverse_id('marauding maulhorn'/'M14', '370648').

card_in_set('mark of the vampire', 'M14').
card_original_type('mark of the vampire'/'M14', 'Enchantment — Aura').
card_original_text('mark of the vampire'/'M14', 'Enchant creatureEnchanted creature gets +2/+2 and has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_image_name('mark of the vampire'/'M14', 'mark of the vampire').
card_uid('mark of the vampire'/'M14', 'M14:Mark of the Vampire:mark of the vampire').
card_rarity('mark of the vampire'/'M14', 'Common').
card_artist('mark of the vampire'/'M14', 'Winona Nelson').
card_number('mark of the vampire'/'M14', '105').
card_flavor_text('mark of the vampire'/'M14', '\"My ‘condition\' is a trial. The weak are consumed by it. The strong transcend it.\"\n—Sorin Markov').
card_multiverse_id('mark of the vampire'/'M14', '370787').

card_in_set('master of diversion', 'M14').
card_original_type('master of diversion'/'M14', 'Creature — Human Scout').
card_original_text('master of diversion'/'M14', 'Whenever Master of Diversion attacks, tap target creature defending player controls.').
card_first_print('master of diversion', 'M14').
card_image_name('master of diversion'/'M14', 'master of diversion').
card_uid('master of diversion'/'M14', 'M14:Master of Diversion:master of diversion').
card_rarity('master of diversion'/'M14', 'Common').
card_artist('master of diversion'/'M14', 'Michael Komarck').
card_number('master of diversion'/'M14', '24').
card_flavor_text('master of diversion'/'M14', 'Choose your battles carefully. For example, avoid the big guy.').
card_multiverse_id('master of diversion'/'M14', '370708').

card_in_set('megantic sliver', 'M14').
card_original_type('megantic sliver'/'M14', 'Creature — Sliver').
card_original_text('megantic sliver'/'M14', 'Sliver creatures you control get +3/+3.').
card_image_name('megantic sliver'/'M14', 'megantic sliver').
card_uid('megantic sliver'/'M14', 'M14:Megantic Sliver:megantic sliver').
card_rarity('megantic sliver'/'M14', 'Rare').
card_artist('megantic sliver'/'M14', 'Ryan Barger').
card_number('megantic sliver'/'M14', '185').
card_flavor_text('megantic sliver'/'M14', 'Even the thrums, the lowliest of slivers, become deadly in its presence.').
card_multiverse_id('megantic sliver'/'M14', '370794').

card_in_set('merfolk spy', 'M14').
card_original_type('merfolk spy'/'M14', 'Creature — Merfolk Rogue').
card_original_text('merfolk spy'/'M14', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)Whenever Merfolk Spy deals combat damage to a player, that player reveals a card at random from his or her hand.').
card_image_name('merfolk spy'/'M14', 'merfolk spy').
card_uid('merfolk spy'/'M14', 'M14:Merfolk Spy:merfolk spy').
card_rarity('merfolk spy'/'M14', 'Common').
card_artist('merfolk spy'/'M14', 'Matt Cavotta & Richard Whitters').
card_number('merfolk spy'/'M14', '62').
card_multiverse_id('merfolk spy'/'M14', '370762').

card_in_set('messenger drake', 'M14').
card_original_type('messenger drake'/'M14', 'Creature — Drake').
card_original_text('messenger drake'/'M14', 'FlyingWhen Messenger Drake dies, draw a card.').
card_first_print('messenger drake', 'M14').
card_image_name('messenger drake'/'M14', 'messenger drake').
card_uid('messenger drake'/'M14', 'M14:Messenger Drake:messenger drake').
card_rarity('messenger drake'/'M14', 'Common').
card_artist('messenger drake'/'M14', 'Yeong-Hao Han').
card_number('messenger drake'/'M14', '63').
card_flavor_text('messenger drake'/'M14', 'The more important the message, the larger the messenger.').
card_multiverse_id('messenger drake'/'M14', '370807').

card_in_set('millstone', 'M14').
card_original_type('millstone'/'M14', 'Artifact').
card_original_text('millstone'/'M14', '{2}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('millstone'/'M14', 'millstone').
card_uid('millstone'/'M14', 'M14:Millstone:millstone').
card_rarity('millstone'/'M14', 'Uncommon').
card_artist('millstone'/'M14', 'Yeong-Hao Han').
card_number('millstone'/'M14', '213').
card_flavor_text('millstone'/'M14', 'Minds, like mountains, are never so grand and mighty that they can\'t be reduced to dust.').
card_multiverse_id('millstone'/'M14', '370737').

card_in_set('mind rot', 'M14').
card_original_type('mind rot'/'M14', 'Sorcery').
card_original_text('mind rot'/'M14', 'Target player discards two cards.').
card_image_name('mind rot'/'M14', 'mind rot').
card_uid('mind rot'/'M14', 'M14:Mind Rot:mind rot').
card_rarity('mind rot'/'M14', 'Common').
card_artist('mind rot'/'M14', 'Steve Luke').
card_number('mind rot'/'M14', '106').
card_flavor_text('mind rot'/'M14', '\"What a pity. You should have written it down.\"\n—Liliana Vess').
card_multiverse_id('mind rot'/'M14', '370711').

card_in_set('mindsparker', 'M14').
card_original_type('mindsparker'/'M14', 'Creature — Elemental').
card_original_text('mindsparker'/'M14', 'First strike (This creature deals combat damage before creatures without first strike.)Whenever an opponent casts a white or blue instant or sorcery spell, Mindsparker deals 2 damage to that player.').
card_first_print('mindsparker', 'M14').
card_image_name('mindsparker'/'M14', 'mindsparker').
card_uid('mindsparker'/'M14', 'M14:Mindsparker:mindsparker').
card_rarity('mindsparker'/'M14', 'Rare').
card_artist('mindsparker'/'M14', 'Wayne Reynolds').
card_number('mindsparker'/'M14', '146').
card_multiverse_id('mindsparker'/'M14', '370695').

card_in_set('minotaur abomination', 'M14').
card_original_type('minotaur abomination'/'M14', 'Creature — Zombie Minotaur').
card_original_text('minotaur abomination'/'M14', '').
card_first_print('minotaur abomination', 'M14').
card_image_name('minotaur abomination'/'M14', 'minotaur abomination').
card_uid('minotaur abomination'/'M14', 'M14:Minotaur Abomination:minotaur abomination').
card_rarity('minotaur abomination'/'M14', 'Common').
card_artist('minotaur abomination'/'M14', 'Karl Kopinski').
card_number('minotaur abomination'/'M14', '107').
card_flavor_text('minotaur abomination'/'M14', '\"Look at that. Shuffling, wobbling, entrails placed haphazardly. It\'s shameful. Who would let that kind of work flap about for all to see?\"\n—Lestin, necromancer').
card_multiverse_id('minotaur abomination'/'M14', '370683').

card_in_set('molten birth', 'M14').
card_original_type('molten birth'/'M14', 'Sorcery').
card_original_text('molten birth'/'M14', 'Put two 1/1 red Elemental creature tokens onto the battlefield. Then flip a coin. If you win the flip, return Molten Birth to its owner\'s hand.').
card_first_print('molten birth', 'M14').
card_image_name('molten birth'/'M14', 'molten birth').
card_uid('molten birth'/'M14', 'M14:Molten Birth:molten birth').
card_rarity('molten birth'/'M14', 'Uncommon').
card_artist('molten birth'/'M14', 'Jaime Jones').
card_number('molten birth'/'M14', '147').
card_flavor_text('molten birth'/'M14', '\"Few can coax the creatures within the magma to be cooled and take form.\"\n—Koth of the Hammer').
card_multiverse_id('molten birth'/'M14', '370604').

card_in_set('mountain', 'M14').
card_original_type('mountain'/'M14', 'Basic Land — Mountain').
card_original_text('mountain'/'M14', 'R').
card_image_name('mountain'/'M14', 'mountain1').
card_uid('mountain'/'M14', 'M14:Mountain:mountain1').
card_rarity('mountain'/'M14', 'Basic Land').
card_artist('mountain'/'M14', 'Cliff Childs').
card_number('mountain'/'M14', '242').
card_multiverse_id('mountain'/'M14', '370588').

card_in_set('mountain', 'M14').
card_original_type('mountain'/'M14', 'Basic Land — Mountain').
card_original_text('mountain'/'M14', 'R').
card_image_name('mountain'/'M14', 'mountain2').
card_uid('mountain'/'M14', 'M14:Mountain:mountain2').
card_rarity('mountain'/'M14', 'Basic Land').
card_artist('mountain'/'M14', 'Jonas De Ro').
card_number('mountain'/'M14', '243').
card_multiverse_id('mountain'/'M14', '370591').

card_in_set('mountain', 'M14').
card_original_type('mountain'/'M14', 'Basic Land — Mountain').
card_original_text('mountain'/'M14', 'R').
card_image_name('mountain'/'M14', 'mountain3').
card_uid('mountain'/'M14', 'M14:Mountain:mountain3').
card_rarity('mountain'/'M14', 'Basic Land').
card_artist('mountain'/'M14', 'Karl Kopinski').
card_number('mountain'/'M14', '244').
card_multiverse_id('mountain'/'M14', '370583').

card_in_set('mountain', 'M14').
card_original_type('mountain'/'M14', 'Basic Land — Mountain').
card_original_text('mountain'/'M14', 'R').
card_image_name('mountain'/'M14', 'mountain4').
card_uid('mountain'/'M14', 'M14:Mountain:mountain4').
card_rarity('mountain'/'M14', 'Basic Land').
card_artist('mountain'/'M14', 'Andreas Rocha').
card_number('mountain'/'M14', '245').
card_multiverse_id('mountain'/'M14', '370725').

card_in_set('mutavault', 'M14').
card_original_type('mutavault'/'M14', 'Land').
card_original_text('mutavault'/'M14', '{T}: Add {1} to your mana pool.{1}: Mutavault becomes a 2/2 creature with all creature types until end of turn. It\'s still a land.').
card_image_name('mutavault'/'M14', 'mutavault').
card_uid('mutavault'/'M14', 'M14:Mutavault:mutavault').
card_rarity('mutavault'/'M14', 'Rare').
card_artist('mutavault'/'M14', 'Fred Fields').
card_number('mutavault'/'M14', '228').
card_flavor_text('mutavault'/'M14', 'Some worlds possess a hidden core where life\'s essence constantly surges.').
card_multiverse_id('mutavault'/'M14', '370733').

card_in_set('naturalize', 'M14').
card_original_type('naturalize'/'M14', 'Instant').
card_original_text('naturalize'/'M14', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'M14', 'naturalize').
card_uid('naturalize'/'M14', 'M14:Naturalize:naturalize').
card_rarity('naturalize'/'M14', 'Common').
card_artist('naturalize'/'M14', 'Tim Hildebrandt').
card_number('naturalize'/'M14', '186').
card_flavor_text('naturalize'/'M14', '\"When your cities and trinkets crumble, only nature will remain.\"\n—Garruk Wildspeaker').
card_multiverse_id('naturalize'/'M14', '370802').

card_in_set('negate', 'M14').
card_original_type('negate'/'M14', 'Instant').
card_original_text('negate'/'M14', 'Counter target noncreature spell.').
card_image_name('negate'/'M14', 'negate').
card_uid('negate'/'M14', 'M14:Negate:negate').
card_rarity('negate'/'M14', 'Common').
card_artist('negate'/'M14', 'Jeremy Jarvis').
card_number('negate'/'M14', '64').
card_flavor_text('negate'/'M14', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'M14', '370719').

card_in_set('nephalia seakite', 'M14').
card_original_type('nephalia seakite'/'M14', 'Creature — Bird').
card_original_text('nephalia seakite'/'M14', 'Flash (You may cast this spell any time you could cast an instant.)Flying').
card_image_name('nephalia seakite'/'M14', 'nephalia seakite').
card_uid('nephalia seakite'/'M14', 'M14:Nephalia Seakite:nephalia seakite').
card_rarity('nephalia seakite'/'M14', 'Common').
card_artist('nephalia seakite'/'M14', 'Wayne England').
card_number('nephalia seakite'/'M14', '65').
card_flavor_text('nephalia seakite'/'M14', '\"Keep one eye on the cliff road or you may fall to your death. Keep one eye on the sky or your death may fall on you.\"\n—Manfried Ulmach, Elgaud Master-at-Arms').
card_multiverse_id('nephalia seakite'/'M14', '370760').

card_in_set('nightmare', 'M14').
card_original_type('nightmare'/'M14', 'Creature — Nightmare Horse').
card_original_text('nightmare'/'M14', 'FlyingNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_image_name('nightmare'/'M14', 'nightmare').
card_uid('nightmare'/'M14', 'M14:Nightmare:nightmare').
card_rarity('nightmare'/'M14', 'Rare').
card_artist('nightmare'/'M14', 'Vance Kovacs').
card_number('nightmare'/'M14', '108').
card_flavor_text('nightmare'/'M14', 'The thunder of its hooves beats dreams into despair.').
card_multiverse_id('nightmare'/'M14', '370689').

card_in_set('nightwing shade', 'M14').
card_original_type('nightwing shade'/'M14', 'Creature — Shade').
card_original_text('nightwing shade'/'M14', 'Flying{1}{B}: Nightwing Shade gets +1/+1 until end of turn.').
card_image_name('nightwing shade'/'M14', 'nightwing shade').
card_uid('nightwing shade'/'M14', 'M14:Nightwing Shade:nightwing shade').
card_rarity('nightwing shade'/'M14', 'Common').
card_artist('nightwing shade'/'M14', 'Lucas Graciano').
card_number('nightwing shade'/'M14', '109').
card_flavor_text('nightwing shade'/'M14', '\"There is one hour of the night even we do not watch.\"\n—Sedva, captain of the watch').
card_multiverse_id('nightwing shade'/'M14', '370705').

card_in_set('oath of the ancient wood', 'M14').
card_original_type('oath of the ancient wood'/'M14', 'Enchantment').
card_original_text('oath of the ancient wood'/'M14', 'Whenever Oath of the Ancient Wood or another enchantment enters the battlefield under your control, you may put a +1/+1 counter on target creature.').
card_first_print('oath of the ancient wood', 'M14').
card_image_name('oath of the ancient wood'/'M14', 'oath of the ancient wood').
card_uid('oath of the ancient wood'/'M14', 'M14:Oath of the Ancient Wood:oath of the ancient wood').
card_rarity('oath of the ancient wood'/'M14', 'Rare').
card_artist('oath of the ancient wood'/'M14', 'Dan Scott').
card_number('oath of the ancient wood'/'M14', '187').
card_flavor_text('oath of the ancient wood'/'M14', '\"Some gaze upon the forest and see trees. I see true power.\"').
card_multiverse_id('oath of the ancient wood'/'M14', '370763').

card_in_set('ogre battledriver', 'M14').
card_original_type('ogre battledriver'/'M14', 'Creature — Ogre Warrior').
card_original_text('ogre battledriver'/'M14', 'Whenever another creature enters the battlefield under your control, that creature gets +2/+0 and gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('ogre battledriver'/'M14', 'ogre battledriver').
card_uid('ogre battledriver'/'M14', 'M14:Ogre Battledriver:ogre battledriver').
card_rarity('ogre battledriver'/'M14', 'Rare').
card_artist('ogre battledriver'/'M14', 'Greg Staples').
card_number('ogre battledriver'/'M14', '148').
card_flavor_text('ogre battledriver'/'M14', 'Ogres are driven by passion, rage, and another ogre standing behind them with a whip.').
card_multiverse_id('ogre battledriver'/'M14', '370662').

card_in_set('opportunity', 'M14').
card_original_type('opportunity'/'M14', 'Instant').
card_original_text('opportunity'/'M14', 'Target player draws four cards.').
card_image_name('opportunity'/'M14', 'opportunity').
card_uid('opportunity'/'M14', 'M14:Opportunity:opportunity').
card_rarity('opportunity'/'M14', 'Uncommon').
card_artist('opportunity'/'M14', 'Allen Williams').
card_number('opportunity'/'M14', '66').
card_flavor_text('opportunity'/'M14', '\"Opportunity isn\'t something you wait for. It\'s something you create.\"').
card_multiverse_id('opportunity'/'M14', '370751').

card_in_set('pacifism', 'M14').
card_original_type('pacifism'/'M14', 'Enchantment — Aura').
card_original_text('pacifism'/'M14', 'Enchant creatureEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'M14', 'pacifism').
card_uid('pacifism'/'M14', 'M14:Pacifism:pacifism').
card_rarity('pacifism'/'M14', 'Common').
card_artist('pacifism'/'M14', 'Robert Bliss').
card_number('pacifism'/'M14', '25').
card_flavor_text('pacifism'/'M14', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'M14', '370812').

card_in_set('path of bravery', 'M14').
card_original_type('path of bravery'/'M14', 'Enchantment').
card_original_text('path of bravery'/'M14', 'As long your life total is greater than or equal to your starting life total, creatures you control get +1/+1.Whenever one or more creatures you control attack, you gain life equal to the number of attacking creatures.').
card_first_print('path of bravery', 'M14').
card_image_name('path of bravery'/'M14', 'path of bravery').
card_uid('path of bravery'/'M14', 'M14:Path of Bravery:path of bravery').
card_rarity('path of bravery'/'M14', 'Rare').
card_artist('path of bravery'/'M14', 'Chris Rahn').
card_number('path of bravery'/'M14', '26').
card_multiverse_id('path of bravery'/'M14', '370798').

card_in_set('pay no heed', 'M14').
card_original_type('pay no heed'/'M14', 'Instant').
card_original_text('pay no heed'/'M14', 'Prevent all damage a source of your choice would deal this turn.').
card_image_name('pay no heed'/'M14', 'pay no heed').
card_uid('pay no heed'/'M14', 'M14:Pay No Heed:pay no heed').
card_rarity('pay no heed'/'M14', 'Common').
card_artist('pay no heed'/'M14', 'Adam Rex').
card_number('pay no heed'/'M14', '27').
card_flavor_text('pay no heed'/'M14', '\"Out of this nettle, danger, we pluck this flower, safety.\"\n—William Shakespeare, King Henry IV, Part I').
card_multiverse_id('pay no heed'/'M14', '370742').

card_in_set('phantom warrior', 'M14').
card_original_type('phantom warrior'/'M14', 'Creature — Illusion Warrior').
card_original_text('phantom warrior'/'M14', 'Phantom Warrior can\'t be blocked.').
card_image_name('phantom warrior'/'M14', 'phantom warrior').
card_uid('phantom warrior'/'M14', 'M14:Phantom Warrior:phantom warrior').
card_rarity('phantom warrior'/'M14', 'Uncommon').
card_artist('phantom warrior'/'M14', 'Greg Staples').
card_number('phantom warrior'/'M14', '67').
card_flavor_text('phantom warrior'/'M14', '\"The construction of a defense is not accomplished by adding bricks.\"\n—Jace Beleren').
card_multiverse_id('phantom warrior'/'M14', '370650').

card_in_set('pillarfield ox', 'M14').
card_original_type('pillarfield ox'/'M14', 'Creature — Ox').
card_original_text('pillarfield ox'/'M14', '').
card_image_name('pillarfield ox'/'M14', 'pillarfield ox').
card_uid('pillarfield ox'/'M14', 'M14:Pillarfield Ox:pillarfield ox').
card_rarity('pillarfield ox'/'M14', 'Common').
card_artist('pillarfield ox'/'M14', 'Andrew Robinson').
card_number('pillarfield ox'/'M14', '28').
card_flavor_text('pillarfield ox'/'M14', '\"You old, immobile clod! If I could stuff your doltish head with knowledge, I\'d do it just so someone else could fully comprehend what your stubbornness has cost me!\"\n—Bruse Tarl, Goma Fada nomad').
card_multiverse_id('pillarfield ox'/'M14', '370765').

card_in_set('pitchburn devils', 'M14').
card_original_type('pitchburn devils'/'M14', 'Creature — Devil').
card_original_text('pitchburn devils'/'M14', 'When Pitchburn Devils dies, it deals 3 damage to target creature or player.').
card_image_name('pitchburn devils'/'M14', 'pitchburn devils').
card_uid('pitchburn devils'/'M14', 'M14:Pitchburn Devils:pitchburn devils').
card_rarity('pitchburn devils'/'M14', 'Common').
card_artist('pitchburn devils'/'M14', 'Johann Bodin').
card_number('pitchburn devils'/'M14', '149').
card_flavor_text('pitchburn devils'/'M14', 'The ingenuity of goblins, the depravity of demons, and the smarts of sheep.').
card_multiverse_id('pitchburn devils'/'M14', '370649').

card_in_set('plains', 'M14').
card_original_type('plains'/'M14', 'Basic Land — Plains').
card_original_text('plains'/'M14', 'W').
card_image_name('plains'/'M14', 'plains1').
card_uid('plains'/'M14', 'M14:Plains:plains1').
card_rarity('plains'/'M14', 'Basic Land').
card_artist('plains'/'M14', 'John Avon').
card_number('plains'/'M14', '230').
card_multiverse_id('plains'/'M14', '370615').

card_in_set('plains', 'M14').
card_original_type('plains'/'M14', 'Basic Land — Plains').
card_original_text('plains'/'M14', 'W').
card_image_name('plains'/'M14', 'plains2').
card_uid('plains'/'M14', 'M14:Plains:plains2').
card_rarity('plains'/'M14', 'Basic Land').
card_artist('plains'/'M14', 'Jonas De Ro').
card_number('plains'/'M14', '231').
card_multiverse_id('plains'/'M14', '370754').

card_in_set('plains', 'M14').
card_original_type('plains'/'M14', 'Basic Land — Plains').
card_original_text('plains'/'M14', 'W').
card_image_name('plains'/'M14', 'plains3').
card_uid('plains'/'M14', 'M14:Plains:plains3').
card_rarity('plains'/'M14', 'Basic Land').
card_artist('plains'/'M14', 'Nils Hamm').
card_number('plains'/'M14', '232').
card_multiverse_id('plains'/'M14', '370679').

card_in_set('plains', 'M14').
card_original_type('plains'/'M14', 'Basic Land — Plains').
card_original_text('plains'/'M14', 'W').
card_image_name('plains'/'M14', 'plains4').
card_uid('plains'/'M14', 'M14:Plains:plains4').
card_rarity('plains'/'M14', 'Basic Land').
card_artist('plains'/'M14', 'Andreas Rocha').
card_number('plains'/'M14', '233').
card_multiverse_id('plains'/'M14', '370669').

card_in_set('planar cleansing', 'M14').
card_original_type('planar cleansing'/'M14', 'Sorcery').
card_original_text('planar cleansing'/'M14', 'Destroy all nonland permanents.').
card_image_name('planar cleansing'/'M14', 'planar cleansing').
card_uid('planar cleansing'/'M14', 'M14:Planar Cleansing:planar cleansing').
card_rarity('planar cleansing'/'M14', 'Rare').
card_artist('planar cleansing'/'M14', 'Michael Komarck').
card_number('planar cleansing'/'M14', '29').
card_multiverse_id('planar cleansing'/'M14', '370808').

card_in_set('plummet', 'M14').
card_original_type('plummet'/'M14', 'Instant').
card_original_text('plummet'/'M14', 'Destroy target creature with flying.').
card_image_name('plummet'/'M14', 'plummet').
card_uid('plummet'/'M14', 'M14:Plummet:plummet').
card_rarity('plummet'/'M14', 'Common').
card_artist('plummet'/'M14', 'Pete Venters').
card_number('plummet'/'M14', '188').
card_flavor_text('plummet'/'M14', '\"Let nothing own the skies but the wind.\"\n—Dejara, Giltwood druid').
card_multiverse_id('plummet'/'M14', '370601').

card_in_set('predatory sliver', 'M14').
card_original_type('predatory sliver'/'M14', 'Creature — Sliver').
card_original_text('predatory sliver'/'M14', 'Sliver creatures you control get +1/+1.').
card_first_print('predatory sliver', 'M14').
card_image_name('predatory sliver'/'M14', 'predatory sliver').
card_uid('predatory sliver'/'M14', 'M14:Predatory Sliver:predatory sliver').
card_rarity('predatory sliver'/'M14', 'Common').
card_artist('predatory sliver'/'M14', 'Mathias Kollros').
card_number('predatory sliver'/'M14', '189').
card_flavor_text('predatory sliver'/'M14', 'No matter how much the slivers change, their collective might remains.').
card_multiverse_id('predatory sliver'/'M14', '370745').

card_in_set('primeval bounty', 'M14').
card_original_type('primeval bounty'/'M14', 'Enchantment').
card_original_text('primeval bounty'/'M14', 'Whenever you cast a creature spell, put a 3/3 green Beast creature token onto the battlefield.Whenever you cast a noncreature spell, put three +1/+1 counters on target creature you control.Whenever a land enters the battlefield under your control, you gain 3 life.').
card_first_print('primeval bounty', 'M14').
card_image_name('primeval bounty'/'M14', 'primeval bounty').
card_uid('primeval bounty'/'M14', 'M14:Primeval Bounty:primeval bounty').
card_rarity('primeval bounty'/'M14', 'Mythic Rare').
card_artist('primeval bounty'/'M14', 'Christine Choi').
card_number('primeval bounty'/'M14', '190').
card_multiverse_id('primeval bounty'/'M14', '370656').

card_in_set('pyromancer\'s gauntlet', 'M14').
card_original_type('pyromancer\'s gauntlet'/'M14', 'Artifact').
card_original_text('pyromancer\'s gauntlet'/'M14', 'If a red instant or sorcery spell you control or a red planeswalker you control would deal damage to a permanent or player, it deals that much damage plus 2 to that permanent or player instead.').
card_first_print('pyromancer\'s gauntlet', 'M14').
card_image_name('pyromancer\'s gauntlet'/'M14', 'pyromancer\'s gauntlet').
card_uid('pyromancer\'s gauntlet'/'M14', 'M14:Pyromancer\'s Gauntlet:pyromancer\'s gauntlet').
card_rarity('pyromancer\'s gauntlet'/'M14', 'Rare').
card_artist('pyromancer\'s gauntlet'/'M14', 'Christine Choi').
card_number('pyromancer\'s gauntlet'/'M14', '214').
card_flavor_text('pyromancer\'s gauntlet'/'M14', 'An inferno is just a gesture away.').
card_multiverse_id('pyromancer\'s gauntlet'/'M14', '370686').

card_in_set('quag sickness', 'M14').
card_original_type('quag sickness'/'M14', 'Enchantment — Aura').
card_original_text('quag sickness'/'M14', 'Enchant creatureEnchanted creature gets -1/-1 for each Swamp you control.').
card_image_name('quag sickness'/'M14', 'quag sickness').
card_uid('quag sickness'/'M14', 'M14:Quag Sickness:quag sickness').
card_rarity('quag sickness'/'M14', 'Common').
card_artist('quag sickness'/'M14', 'Martina Pilcerova').
card_number('quag sickness'/'M14', '110').
card_flavor_text('quag sickness'/'M14', 'The dread gases didn\'t kill Farbid. But as he lay in the muck, miserable and helpless, watching ghouls and rats advance on him, he wished they had.').
card_multiverse_id('quag sickness'/'M14', '370714').

card_in_set('quicken', 'M14').
card_original_type('quicken'/'M14', 'Instant').
card_original_text('quicken'/'M14', 'The next sorcery card you cast this turn can be cast as though it had flash. (It can be cast any time you could cast an instant.)Draw a card.').
card_image_name('quicken'/'M14', 'quicken').
card_uid('quicken'/'M14', 'M14:Quicken:quicken').
card_rarity('quicken'/'M14', 'Rare').
card_artist('quicken'/'M14', 'Aleksi Briclot').
card_number('quicken'/'M14', '68').
card_flavor_text('quicken'/'M14', 'A skilled Izzet chronarch can carry out an epic vendetta between the fall of one hourglass grain and the next.').
card_multiverse_id('quicken'/'M14', '370644').

card_in_set('ranger\'s guile', 'M14').
card_original_type('ranger\'s guile'/'M14', 'Instant').
card_original_text('ranger\'s guile'/'M14', 'Target creature you control gets +1/+1 and gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_image_name('ranger\'s guile'/'M14', 'ranger\'s guile').
card_uid('ranger\'s guile'/'M14', 'M14:Ranger\'s Guile:ranger\'s guile').
card_rarity('ranger\'s guile'/'M14', 'Common').
card_artist('ranger\'s guile'/'M14', 'Steve Prescott').
card_number('ranger\'s guile'/'M14', '191').
card_flavor_text('ranger\'s guile'/'M14', '\"You don\'t survive in the wild by standing in plain sight.\"\n—Garruk Wildspeaker').
card_multiverse_id('ranger\'s guile'/'M14', '370803').

card_in_set('ratchet bomb', 'M14').
card_original_type('ratchet bomb'/'M14', 'Artifact').
card_original_text('ratchet bomb'/'M14', '{T}: Put a charge counter on Ratchet Bomb.{T}, Sacrifice Ratchet Bomb: Destroy each nonland permanent with converted mana cost equal to the number of charge counters on Ratchet Bomb.').
card_image_name('ratchet bomb'/'M14', 'ratchet bomb').
card_uid('ratchet bomb'/'M14', 'M14:Ratchet Bomb:ratchet bomb').
card_rarity('ratchet bomb'/'M14', 'Rare').
card_artist('ratchet bomb'/'M14', 'Austin Hsu').
card_number('ratchet bomb'/'M14', '215').
card_multiverse_id('ratchet bomb'/'M14', '370623').

card_in_set('regathan firecat', 'M14').
card_original_type('regathan firecat'/'M14', 'Creature — Elemental Cat').
card_original_text('regathan firecat'/'M14', '').
card_first_print('regathan firecat', 'M14').
card_image_name('regathan firecat'/'M14', 'regathan firecat').
card_uid('regathan firecat'/'M14', 'M14:Regathan Firecat:regathan firecat').
card_rarity('regathan firecat'/'M14', 'Common').
card_artist('regathan firecat'/'M14', 'Eric Velhagen').
card_number('regathan firecat'/'M14', '150').
card_flavor_text('regathan firecat'/'M14', 'It stalks the Regathan highlands, leaving behind melted snow, scorched earth, and the charred corpses of would-be temple robbers.').
card_multiverse_id('regathan firecat'/'M14', '370805').

card_in_set('ring of three wishes', 'M14').
card_original_type('ring of three wishes'/'M14', 'Artifact').
card_original_text('ring of three wishes'/'M14', 'Ring of Three Wishes enters the battlefield with three wish counters on it.{5}, {T}, Remove a wish counter from Ring of Three Wishes: Search your library for a card and put that card into your hand. Then shuffle your library.').
card_first_print('ring of three wishes', 'M14').
card_image_name('ring of three wishes'/'M14', 'ring of three wishes').
card_uid('ring of three wishes'/'M14', 'M14:Ring of Three Wishes:ring of three wishes').
card_rarity('ring of three wishes'/'M14', 'Mythic Rare').
card_artist('ring of three wishes'/'M14', 'Mark Winters').
card_number('ring of three wishes'/'M14', '216').
card_multiverse_id('ring of three wishes'/'M14', '370580').

card_in_set('rise of the dark realms', 'M14').
card_original_type('rise of the dark realms'/'M14', 'Sorcery').
card_original_text('rise of the dark realms'/'M14', 'Put all creature cards from all graveyards onto the battlefield under your control.').
card_first_print('rise of the dark realms', 'M14').
card_image_name('rise of the dark realms'/'M14', 'rise of the dark realms').
card_uid('rise of the dark realms'/'M14', 'M14:Rise of the Dark Realms:rise of the dark realms').
card_rarity('rise of the dark realms'/'M14', 'Mythic Rare').
card_artist('rise of the dark realms'/'M14', 'Michael Komarck').
card_number('rise of the dark realms'/'M14', '111').
card_flavor_text('rise of the dark realms'/'M14', '\"For every living person there are generations of dead. Which realm would you rather rule?\"\n—Liliana Vess').
card_multiverse_id('rise of the dark realms'/'M14', '370636').

card_in_set('rod of ruin', 'M14').
card_original_type('rod of ruin'/'M14', 'Artifact').
card_original_text('rod of ruin'/'M14', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'M14', 'rod of ruin').
card_uid('rod of ruin'/'M14', 'M14:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'M14', 'Uncommon').
card_artist('rod of ruin'/'M14', 'Mark Zug').
card_number('rod of ruin'/'M14', '217').
card_flavor_text('rod of ruin'/'M14', 'The good news is it\'s so simple a goblin could use it. The bad news is it\'s so simple a goblin could use it.').
card_multiverse_id('rod of ruin'/'M14', '370668').

card_in_set('rootwalla', 'M14').
card_original_type('rootwalla'/'M14', 'Creature — Lizard').
card_original_text('rootwalla'/'M14', '{1}{G}: Rootwalla gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_image_name('rootwalla'/'M14', 'rootwalla').
card_uid('rootwalla'/'M14', 'M14:Rootwalla:rootwalla').
card_rarity('rootwalla'/'M14', 'Common').
card_artist('rootwalla'/'M14', 'Roger Raupp').
card_number('rootwalla'/'M14', '192').
card_flavor_text('rootwalla'/'M14', 'They make terrible pets . . . unless you don\'t like your other pets.').
card_multiverse_id('rootwalla'/'M14', '370693').

card_in_set('rumbling baloth', 'M14').
card_original_type('rumbling baloth'/'M14', 'Creature — Beast').
card_original_text('rumbling baloth'/'M14', '').
card_first_print('rumbling baloth', 'M14').
card_image_name('rumbling baloth'/'M14', 'rumbling baloth').
card_uid('rumbling baloth'/'M14', 'M14:Rumbling Baloth:rumbling baloth').
card_rarity('rumbling baloth'/'M14', 'Common').
card_artist('rumbling baloth'/'M14', 'Jesper Ejsing').
card_number('rumbling baloth'/'M14', '193').
card_flavor_text('rumbling baloth'/'M14', 'In the dim light beneath the vast trees of Deepglade, baloths prowl in search of prey. Their guttural calls are more felt than heard, but their attack scream carries for miles.').
card_multiverse_id('rumbling baloth'/'M14', '370764').

card_in_set('sanguine bond', 'M14').
card_original_type('sanguine bond'/'M14', 'Enchantment').
card_original_text('sanguine bond'/'M14', 'Whenever you gain life, target opponent loses that much life.').
card_image_name('sanguine bond'/'M14', 'sanguine bond').
card_uid('sanguine bond'/'M14', 'M14:Sanguine Bond:sanguine bond').
card_rarity('sanguine bond'/'M14', 'Rare').
card_artist('sanguine bond'/'M14', 'Jaime Jones').
card_number('sanguine bond'/'M14', '112').
card_flavor_text('sanguine bond'/'M14', '\"Blood is constant. Every drop I drink, someone must bleed.\"\n—Vradeen, vampire nocturnus').
card_multiverse_id('sanguine bond'/'M14', '370671').

card_in_set('savage summoning', 'M14').
card_original_type('savage summoning'/'M14', 'Instant').
card_original_text('savage summoning'/'M14', 'Savage Summoning can\'t be countered.The next creature card you cast this turn can be cast as though it had flash. That spell can\'t be countered. That creature enters the battlefield with an additional +1/+1 counter on it.').
card_first_print('savage summoning', 'M14').
card_image_name('savage summoning'/'M14', 'savage summoning').
card_uid('savage summoning'/'M14', 'M14:Savage Summoning:savage summoning').
card_rarity('savage summoning'/'M14', 'Rare').
card_artist('savage summoning'/'M14', 'Johann Bodin').
card_number('savage summoning'/'M14', '194').
card_multiverse_id('savage summoning'/'M14', '370710').

card_in_set('scavenging ooze', 'M14').
card_original_type('scavenging ooze'/'M14', 'Creature — Ooze').
card_original_text('scavenging ooze'/'M14', '{G}: Exile target card from a graveyard. If it was a creature card, put a +1/+1 counter on Scavenging Ooze and you gain 1 life.').
card_image_name('scavenging ooze'/'M14', 'scavenging ooze').
card_uid('scavenging ooze'/'M14', 'M14:Scavenging Ooze:scavenging ooze').
card_rarity('scavenging ooze'/'M14', 'Rare').
card_artist('scavenging ooze'/'M14', 'Austin Hsu').
card_number('scavenging ooze'/'M14', '195').
card_flavor_text('scavenging ooze'/'M14', 'In nature, not a single bone or scrap of flesh goes to waste.').
card_multiverse_id('scavenging ooze'/'M14', '370629').

card_in_set('scourge of valkas', 'M14').
card_original_type('scourge of valkas'/'M14', 'Creature — Dragon').
card_original_text('scourge of valkas'/'M14', 'FlyingWhenever Scourge of Valkas or another Dragon enters the battlefield under your control, it deals X damage to target creature or player, where X is the number of Dragons you control.{R}: Scourge of Valkas gets +1/+0 until end of turn.').
card_first_print('scourge of valkas', 'M14').
card_image_name('scourge of valkas'/'M14', 'scourge of valkas').
card_uid('scourge of valkas'/'M14', 'M14:Scourge of Valkas:scourge of valkas').
card_rarity('scourge of valkas'/'M14', 'Mythic Rare').
card_artist('scourge of valkas'/'M14', 'Lucas Graciano').
card_number('scourge of valkas'/'M14', '151').
card_multiverse_id('scourge of valkas'/'M14', '370584').

card_in_set('scroll thief', 'M14').
card_original_type('scroll thief'/'M14', 'Creature — Merfolk Rogue').
card_original_text('scroll thief'/'M14', 'Whenever Scroll Thief deals combat damage to a player, draw a card.').
card_image_name('scroll thief'/'M14', 'scroll thief').
card_uid('scroll thief'/'M14', 'M14:Scroll Thief:scroll thief').
card_rarity('scroll thief'/'M14', 'Common').
card_artist('scroll thief'/'M14', 'Alex Horley-Orlandelli').
card_number('scroll thief'/'M14', '69').
card_flavor_text('scroll thief'/'M14', 'Kapsho merfolk have infiltrated fortresses as remote as the desert citadel in Sunari in their relentless quest for arcane knowledge.').
card_multiverse_id('scroll thief'/'M14', '370651').

card_in_set('seacoast drake', 'M14').
card_original_type('seacoast drake'/'M14', 'Creature — Drake').
card_original_text('seacoast drake'/'M14', 'Flying').
card_first_print('seacoast drake', 'M14').
card_image_name('seacoast drake'/'M14', 'seacoast drake').
card_uid('seacoast drake'/'M14', 'M14:Seacoast Drake:seacoast drake').
card_rarity('seacoast drake'/'M14', 'Common').
card_artist('seacoast drake'/'M14', 'Scott Chou').
card_number('seacoast drake'/'M14', '70').
card_flavor_text('seacoast drake'/'M14', 'Seacoast drakes have been known to follow ships for hundreds of miles, waiting to snap up garbage, bait, and the occasional sailor.').
card_multiverse_id('seacoast drake'/'M14', '370617').

card_in_set('seismic stomp', 'M14').
card_original_type('seismic stomp'/'M14', 'Sorcery').
card_original_text('seismic stomp'/'M14', 'Creatures without flying can\'t block this turn.').
card_first_print('seismic stomp', 'M14').
card_image_name('seismic stomp'/'M14', 'seismic stomp').
card_uid('seismic stomp'/'M14', 'M14:Seismic Stomp:seismic stomp').
card_rarity('seismic stomp'/'M14', 'Common').
card_artist('seismic stomp'/'M14', 'Chase Stone').
card_number('seismic stomp'/'M14', '152').
card_flavor_text('seismic stomp'/'M14', '\"It is not in me to tread lightly.\"\n—Ryda, geomancer').
card_multiverse_id('seismic stomp'/'M14', '370713').

card_in_set('sengir vampire', 'M14').
card_original_type('sengir vampire'/'M14', 'Creature — Vampire').
card_original_text('sengir vampire'/'M14', 'FlyingWhenever a creature dealt damage by Sengir Vampire this turn dies, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'M14', 'sengir vampire').
card_uid('sengir vampire'/'M14', 'M14:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'M14', 'Uncommon').
card_artist('sengir vampire'/'M14', 'Kev Walker').
card_number('sengir vampire'/'M14', '113').
card_flavor_text('sengir vampire'/'M14', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'M14', '370724').

card_in_set('sensory deprivation', 'M14').
card_original_type('sensory deprivation'/'M14', 'Enchantment — Aura').
card_original_text('sensory deprivation'/'M14', 'Enchant creatureEnchanted creature gets -3/-0.').
card_image_name('sensory deprivation'/'M14', 'sensory deprivation').
card_uid('sensory deprivation'/'M14', 'M14:Sensory Deprivation:sensory deprivation').
card_rarity('sensory deprivation'/'M14', 'Common').
card_artist('sensory deprivation'/'M14', 'Steven Belledin').
card_number('sensory deprivation'/'M14', '71').
card_flavor_text('sensory deprivation'/'M14', 'They call it \"stitcher\'s anesthesia,\" a spell to deaden the senses while the mad doctors begin their grisly work.').
card_multiverse_id('sensory deprivation'/'M14', '370780').

card_in_set('sentinel sliver', 'M14').
card_original_type('sentinel sliver'/'M14', 'Creature — Sliver').
card_original_text('sentinel sliver'/'M14', 'Sliver creatures you control have vigilance. (Attacking doesn\'t cause them to tap.)').
card_first_print('sentinel sliver', 'M14').
card_image_name('sentinel sliver'/'M14', 'sentinel sliver').
card_uid('sentinel sliver'/'M14', 'M14:Sentinel Sliver:sentinel sliver').
card_rarity('sentinel sliver'/'M14', 'Common').
card_artist('sentinel sliver'/'M14', 'Maciej Kuciara').
card_number('sentinel sliver'/'M14', '30').
card_flavor_text('sentinel sliver'/'M14', 'Through its watchful gaze, all slivers may see.').
card_multiverse_id('sentinel sliver'/'M14', '370813').

card_in_set('seraph of the sword', 'M14').
card_original_type('seraph of the sword'/'M14', 'Creature — Angel').
card_original_text('seraph of the sword'/'M14', 'FlyingPrevent all combat damage that would be dealt to Seraph of the Sword.').
card_first_print('seraph of the sword', 'M14').
card_image_name('seraph of the sword'/'M14', 'seraph of the sword').
card_uid('seraph of the sword'/'M14', 'M14:Seraph of the Sword:seraph of the sword').
card_rarity('seraph of the sword'/'M14', 'Rare').
card_artist('seraph of the sword'/'M14', 'Jaime Jones').
card_number('seraph of the sword'/'M14', '31').
card_flavor_text('seraph of the sword'/'M14', '\"Only when my sword lies idle shall I rejoice.\"').
card_multiverse_id('seraph of the sword'/'M14', '370620').

card_in_set('serra angel', 'M14').
card_original_type('serra angel'/'M14', 'Creature — Angel').
card_original_text('serra angel'/'M14', 'FlyingVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'M14', 'serra angel').
card_uid('serra angel'/'M14', 'M14:Serra Angel:serra angel').
card_rarity('serra angel'/'M14', 'Uncommon').
card_artist('serra angel'/'M14', 'Greg Staples').
card_number('serra angel'/'M14', '32').
card_flavor_text('serra angel'/'M14', 'Follow the light. In its absence, follow her.').
card_multiverse_id('serra angel'/'M14', '370602').

card_in_set('shadowborn apostle', 'M14').
card_original_type('shadowborn apostle'/'M14', 'Creature — Human Cleric').
card_original_text('shadowborn apostle'/'M14', 'A deck can have any number of cards named Shadowborn Apostle.{B}, Sacrifice six creatures named Shadowborn Apostle: Search your library for a Demon creature card and put it onto the battlefield. Then shuffle your library.').
card_first_print('shadowborn apostle', 'M14').
card_image_name('shadowborn apostle'/'M14', 'shadowborn apostle').
card_uid('shadowborn apostle'/'M14', 'M14:Shadowborn Apostle:shadowborn apostle').
card_rarity('shadowborn apostle'/'M14', 'Common').
card_artist('shadowborn apostle'/'M14', 'Lucas Graciano').
card_number('shadowborn apostle'/'M14', '114').
card_multiverse_id('shadowborn apostle'/'M14', '370746').

card_in_set('shadowborn demon', 'M14').
card_original_type('shadowborn demon'/'M14', 'Creature — Demon').
card_original_text('shadowborn demon'/'M14', 'FlyingWhen Shadowborn Demon enters the battlefield, destroy target non-Demon creature.At the beginning of your upkeep, if there are fewer than six creature cards in your graveyard, sacrifice a creature.').
card_first_print('shadowborn demon', 'M14').
card_image_name('shadowborn demon'/'M14', 'shadowborn demon').
card_uid('shadowborn demon'/'M14', 'M14:Shadowborn Demon:shadowborn demon').
card_rarity('shadowborn demon'/'M14', 'Mythic Rare').
card_artist('shadowborn demon'/'M14', 'Lucas Graciano').
card_number('shadowborn demon'/'M14', '115').
card_multiverse_id('shadowborn demon'/'M14', '370655').

card_in_set('shimmering grotto', 'M14').
card_original_type('shimmering grotto'/'M14', 'Land').
card_original_text('shimmering grotto'/'M14', '{T}: Add {1} to your mana pool.{1}, {T}: Add one mana of any color to your mana pool.').
card_image_name('shimmering grotto'/'M14', 'shimmering grotto').
card_uid('shimmering grotto'/'M14', 'M14:Shimmering Grotto:shimmering grotto').
card_rarity('shimmering grotto'/'M14', 'Uncommon').
card_artist('shimmering grotto'/'M14', 'Cliff Childs').
card_number('shimmering grotto'/'M14', '229').
card_flavor_text('shimmering grotto'/'M14', 'Untold riches await those who forsake the bustling world to search the secret, silent places.').
card_multiverse_id('shimmering grotto'/'M14', '370631').

card_in_set('shiv\'s embrace', 'M14').
card_original_type('shiv\'s embrace'/'M14', 'Enchantment — Aura').
card_original_text('shiv\'s embrace'/'M14', 'Enchant creatureEnchanted creature gets +2/+2 and has flying.{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('shiv\'s embrace'/'M14', 'shiv\'s embrace').
card_uid('shiv\'s embrace'/'M14', 'M14:Shiv\'s Embrace:shiv\'s embrace').
card_rarity('shiv\'s embrace'/'M14', 'Uncommon').
card_artist('shiv\'s embrace'/'M14', 'Dave Kendall').
card_number('shiv\'s embrace'/'M14', '153').
card_flavor_text('shiv\'s embrace'/'M14', 'When you see the world as a dragon sees it, everything looks fit to burn.').
card_multiverse_id('shiv\'s embrace'/'M14', '370707').

card_in_set('shivan dragon', 'M14').
card_original_type('shivan dragon'/'M14', 'Creature — Dragon').
card_original_text('shivan dragon'/'M14', 'Flying{R}: Shivan Dragon gets +1/+0 until end of turn.').
card_image_name('shivan dragon'/'M14', 'shivan dragon').
card_uid('shivan dragon'/'M14', 'M14:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'M14', 'Rare').
card_artist('shivan dragon'/'M14', 'Donato Giancola').
card_number('shivan dragon'/'M14', '154').
card_flavor_text('shivan dragon'/'M14', 'The undisputed master of the mountains of Shiv.').
card_multiverse_id('shivan dragon'/'M14', '370825').

card_in_set('shock', 'M14').
card_original_type('shock'/'M14', 'Instant').
card_original_text('shock'/'M14', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'M14', 'shock').
card_uid('shock'/'M14', 'M14:Shock:shock').
card_rarity('shock'/'M14', 'Common').
card_artist('shock'/'M14', 'Jon Foster').
card_number('shock'/'M14', '155').
card_multiverse_id('shock'/'M14', '370654').

card_in_set('show of valor', 'M14').
card_original_type('show of valor'/'M14', 'Instant').
card_original_text('show of valor'/'M14', 'Target creature gets +2/+4 until end of turn.').
card_image_name('show of valor'/'M14', 'show of valor').
card_uid('show of valor'/'M14', 'M14:Show of Valor:show of valor').
card_rarity('show of valor'/'M14', 'Common').
card_artist('show of valor'/'M14', 'Anthony Palumbo').
card_number('show of valor'/'M14', '33').
card_flavor_text('show of valor'/'M14', 'Cowards watch. Heroes act.').
card_multiverse_id('show of valor'/'M14', '370779').

card_in_set('shrivel', 'M14').
card_original_type('shrivel'/'M14', 'Sorcery').
card_original_text('shrivel'/'M14', 'All creatures get -1/-1 until end of turn.').
card_image_name('shrivel'/'M14', 'shrivel').
card_uid('shrivel'/'M14', 'M14:Shrivel:shrivel').
card_rarity('shrivel'/'M14', 'Common').
card_artist('shrivel'/'M14', 'Jung Park').
card_number('shrivel'/'M14', '116').
card_flavor_text('shrivel'/'M14', '\"Do not weep for the frail. Their time couldn\'t have been that far off.\"\n—Nathrac, plaguesower').
card_multiverse_id('shrivel'/'M14', '370722').

card_in_set('siege mastodon', 'M14').
card_original_type('siege mastodon'/'M14', 'Creature — Elephant').
card_original_text('siege mastodon'/'M14', '').
card_image_name('siege mastodon'/'M14', 'siege mastodon').
card_uid('siege mastodon'/'M14', 'M14:Siege Mastodon:siege mastodon').
card_rarity('siege mastodon'/'M14', 'Common').
card_artist('siege mastodon'/'M14', 'Matt Cavotta').
card_number('siege mastodon'/'M14', '34').
card_flavor_text('siege mastodon'/'M14', '\"The walls of the wicked will fall before us. Ready the siege engines. We proceed to war!\"\n—General Avitora').
card_multiverse_id('siege mastodon'/'M14', '370704').

card_in_set('silence', 'M14').
card_original_type('silence'/'M14', 'Instant').
card_original_text('silence'/'M14', 'Your opponents can\'t cast spells this turn. (Spells cast before this resolves are unaffected.)').
card_image_name('silence'/'M14', 'silence').
card_uid('silence'/'M14', 'M14:Silence:silence').
card_rarity('silence'/'M14', 'Rare').
card_artist('silence'/'M14', 'Wayne Reynolds').
card_number('silence'/'M14', '35').
card_flavor_text('silence'/'M14', '\"Take a quiet moment to reflect on your sins.\"\n—Cleph, Order of Redeemed Souls').
card_multiverse_id('silence'/'M14', '370578').

card_in_set('sliver construct', 'M14').
card_original_type('sliver construct'/'M14', 'Artifact Creature — Sliver Construct').
card_original_text('sliver construct'/'M14', '').
card_first_print('sliver construct', 'M14').
card_image_name('sliver construct'/'M14', 'sliver construct').
card_uid('sliver construct'/'M14', 'M14:Sliver Construct:sliver construct').
card_rarity('sliver construct'/'M14', 'Common').
card_artist('sliver construct'/'M14', 'Mathias Kollros').
card_number('sliver construct'/'M14', '218').
card_flavor_text('sliver construct'/'M14', 'Slivers destroy those who come close to the Skep, the central hive. Shards of torn metal litter the ground as a warning to any artificers inquisitive about the hive\'s inner workings.').
card_multiverse_id('sliver construct'/'M14', '370643').

card_in_set('smelt', 'M14').
card_original_type('smelt'/'M14', 'Instant').
card_original_text('smelt'/'M14', 'Destroy target artifact.').
card_image_name('smelt'/'M14', 'smelt').
card_uid('smelt'/'M14', 'M14:Smelt:smelt').
card_rarity('smelt'/'M14', 'Common').
card_artist('smelt'/'M14', 'Zoltan Boros').
card_number('smelt'/'M14', '156').
card_flavor_text('smelt'/'M14', '\"Looks like that wasn\'t darksteel after all.\"\n—Koth of the Hammer').
card_multiverse_id('smelt'/'M14', '370784').

card_in_set('solemn offering', 'M14').
card_original_type('solemn offering'/'M14', 'Sorcery').
card_original_text('solemn offering'/'M14', 'Destroy target artifact or enchantment. You gain 4 life.').
card_image_name('solemn offering'/'M14', 'solemn offering').
card_uid('solemn offering'/'M14', 'M14:Solemn Offering:solemn offering').
card_rarity('solemn offering'/'M14', 'Common').
card_artist('solemn offering'/'M14', 'Sam Wood').
card_number('solemn offering'/'M14', '36').
card_flavor_text('solemn offering'/'M14', '\"You will be reimbursed for your donation.\"\"The reimbursement is spiritual.\"\n—Temple signs').
card_multiverse_id('solemn offering'/'M14', '370730').

card_in_set('soulmender', 'M14').
card_original_type('soulmender'/'M14', 'Creature — Human Cleric').
card_original_text('soulmender'/'M14', '{T}: You gain 1 life.').
card_first_print('soulmender', 'M14').
card_image_name('soulmender'/'M14', 'soulmender').
card_uid('soulmender'/'M14', 'M14:Soulmender:soulmender').
card_rarity('soulmender'/'M14', 'Common').
card_artist('soulmender'/'M14', 'James Ryman').
card_number('soulmender'/'M14', '37').
card_flavor_text('soulmender'/'M14', '\"Healing is more art than magic. Well, there is still quite a bit of magic.\"').
card_multiverse_id('soulmender'/'M14', '370587').

card_in_set('spell blast', 'M14').
card_original_type('spell blast'/'M14', 'Instant').
card_original_text('spell blast'/'M14', 'Counter target spell with converted mana cost X. (For example, if that spell\'s mana cost is {3}{U}{U}, X is 5.)').
card_image_name('spell blast'/'M14', 'spell blast').
card_uid('spell blast'/'M14', 'M14:Spell Blast:spell blast').
card_rarity('spell blast'/'M14', 'Uncommon').
card_artist('spell blast'/'M14', 'Jaime Jones').
card_number('spell blast'/'M14', '72').
card_flavor_text('spell blast'/'M14', '\"Perhaps you could try throwing a rock? That might be better suited to your intellect.\"').
card_multiverse_id('spell blast'/'M14', '370645').

card_in_set('sporemound', 'M14').
card_original_type('sporemound'/'M14', 'Creature — Fungus').
card_original_text('sporemound'/'M14', 'Whenever a land enters the battlefield under your control, put a 1/1 green Saproling creature token onto the battlefield.').
card_first_print('sporemound', 'M14').
card_image_name('sporemound'/'M14', 'sporemound').
card_uid('sporemound'/'M14', 'M14:Sporemound:sporemound').
card_rarity('sporemound'/'M14', 'Common').
card_artist('sporemound'/'M14', 'Svetlin Velinov').
card_number('sporemound'/'M14', '196').
card_flavor_text('sporemound'/'M14', '\"If you threaten its territory, it produces spores. Actually, no matter what you do, it produces spores.\"\n—Hadi Kasten, Calla Dale naturalist').
card_multiverse_id('sporemound'/'M14', '370605').

card_in_set('staff of the death magus', 'M14').
card_original_type('staff of the death magus'/'M14', 'Artifact').
card_original_text('staff of the death magus'/'M14', 'Whenever you cast a black spell or a Swamp enters the battlefield under your control, you gain 1 life.').
card_first_print('staff of the death magus', 'M14').
card_image_name('staff of the death magus'/'M14', 'staff of the death magus').
card_uid('staff of the death magus'/'M14', 'M14:Staff of the Death Magus:staff of the death magus').
card_rarity('staff of the death magus'/'M14', 'Uncommon').
card_artist('staff of the death magus'/'M14', 'Daniel Ljunggren').
card_number('staff of the death magus'/'M14', '219').
card_flavor_text('staff of the death magus'/'M14', 'A symbol of ambition in ruthless times.').
card_multiverse_id('staff of the death magus'/'M14', '370586').

card_in_set('staff of the flame magus', 'M14').
card_original_type('staff of the flame magus'/'M14', 'Artifact').
card_original_text('staff of the flame magus'/'M14', 'Whenever you cast a red spell or a Mountain enters the battlefield under your control, you gain 1 life.').
card_first_print('staff of the flame magus', 'M14').
card_image_name('staff of the flame magus'/'M14', 'staff of the flame magus').
card_uid('staff of the flame magus'/'M14', 'M14:Staff of the Flame Magus:staff of the flame magus').
card_rarity('staff of the flame magus'/'M14', 'Uncommon').
card_artist('staff of the flame magus'/'M14', 'Daniel Ljunggren').
card_number('staff of the flame magus'/'M14', '220').
card_flavor_text('staff of the flame magus'/'M14', 'A symbol of passion in indifferent times.').
card_multiverse_id('staff of the flame magus'/'M14', '370625').

card_in_set('staff of the mind magus', 'M14').
card_original_type('staff of the mind magus'/'M14', 'Artifact').
card_original_text('staff of the mind magus'/'M14', 'Whenever you cast a blue spell or an Island enters the battlefield under your control, you gain 1 life.').
card_first_print('staff of the mind magus', 'M14').
card_image_name('staff of the mind magus'/'M14', 'staff of the mind magus').
card_uid('staff of the mind magus'/'M14', 'M14:Staff of the Mind Magus:staff of the mind magus').
card_rarity('staff of the mind magus'/'M14', 'Uncommon').
card_artist('staff of the mind magus'/'M14', 'Daniel Ljunggren').
card_number('staff of the mind magus'/'M14', '221').
card_flavor_text('staff of the mind magus'/'M14', 'A symbol of sagacity in bewildering times.').
card_multiverse_id('staff of the mind magus'/'M14', '370676').

card_in_set('staff of the sun magus', 'M14').
card_original_type('staff of the sun magus'/'M14', 'Artifact').
card_original_text('staff of the sun magus'/'M14', 'Whenever you cast a white spell or a Plains enters the battlefield under your control, you gain 1 life.').
card_first_print('staff of the sun magus', 'M14').
card_image_name('staff of the sun magus'/'M14', 'staff of the sun magus').
card_uid('staff of the sun magus'/'M14', 'M14:Staff of the Sun Magus:staff of the sun magus').
card_rarity('staff of the sun magus'/'M14', 'Uncommon').
card_artist('staff of the sun magus'/'M14', 'Daniel Ljunggren').
card_number('staff of the sun magus'/'M14', '222').
card_flavor_text('staff of the sun magus'/'M14', 'A symbol of conviction in uncertain times.').
card_multiverse_id('staff of the sun magus'/'M14', '370635').

card_in_set('staff of the wild magus', 'M14').
card_original_type('staff of the wild magus'/'M14', 'Artifact').
card_original_text('staff of the wild magus'/'M14', 'Whenever you cast a green spell or a Forest enters the battlefield under your control, you gain 1 life.').
card_first_print('staff of the wild magus', 'M14').
card_image_name('staff of the wild magus'/'M14', 'staff of the wild magus').
card_uid('staff of the wild magus'/'M14', 'M14:Staff of the Wild Magus:staff of the wild magus').
card_rarity('staff of the wild magus'/'M14', 'Uncommon').
card_artist('staff of the wild magus'/'M14', 'Daniel Ljunggren').
card_number('staff of the wild magus'/'M14', '223').
card_flavor_text('staff of the wild magus'/'M14', 'A symbol of ferocity in oppressive times.').
card_multiverse_id('staff of the wild magus'/'M14', '370592').

card_in_set('steelform sliver', 'M14').
card_original_type('steelform sliver'/'M14', 'Creature — Sliver').
card_original_text('steelform sliver'/'M14', 'Sliver creatures you control get +0/+1.').
card_first_print('steelform sliver', 'M14').
card_image_name('steelform sliver'/'M14', 'steelform sliver').
card_uid('steelform sliver'/'M14', 'M14:Steelform Sliver:steelform sliver').
card_rarity('steelform sliver'/'M14', 'Uncommon').
card_artist('steelform sliver'/'M14', 'Chase Stone').
card_number('steelform sliver'/'M14', '38').
card_flavor_text('steelform sliver'/'M14', '\"Though the slivers may sometimes resemble us, they are not human. Anyone who fights them must remember this.\"\n—Sarlena, paladin of the Northern Verge').
card_multiverse_id('steelform sliver'/'M14', '370597').

card_in_set('stonehorn chanter', 'M14').
card_original_type('stonehorn chanter'/'M14', 'Creature — Rhino Cleric').
card_original_text('stonehorn chanter'/'M14', '{5}{W}: Stonehorn Chanter gains vigilance and lifelink until end of turn. (Attacking doesn\'t cause it to tap. Damage dealt by it also causes you to gain that much life.)').
card_first_print('stonehorn chanter', 'M14').
card_image_name('stonehorn chanter'/'M14', 'stonehorn chanter').
card_uid('stonehorn chanter'/'M14', 'M14:Stonehorn Chanter:stonehorn chanter').
card_rarity('stonehorn chanter'/'M14', 'Uncommon').
card_artist('stonehorn chanter'/'M14', 'Raymond Swanland').
card_number('stonehorn chanter'/'M14', '39').
card_flavor_text('stonehorn chanter'/'M14', 'With the Stonehorn, piety and power are one.').
card_multiverse_id('stonehorn chanter'/'M14', '370777').

card_in_set('striking sliver', 'M14').
card_original_type('striking sliver'/'M14', 'Creature — Sliver').
card_original_text('striking sliver'/'M14', 'Sliver creatures you control have first strike. (They deal combat damage before creatures without first strike.)').
card_first_print('striking sliver', 'M14').
card_image_name('striking sliver'/'M14', 'striking sliver').
card_uid('striking sliver'/'M14', 'M14:Striking Sliver:striking sliver').
card_rarity('striking sliver'/'M14', 'Common').
card_artist('striking sliver'/'M14', 'Maciej Kuciara').
card_number('striking sliver'/'M14', '157').
card_flavor_text('striking sliver'/'M14', 'You\'re too busy recoiling in fear to realize that it\'s already hit you.').
card_multiverse_id('striking sliver'/'M14', '370589').

card_in_set('strionic resonator', 'M14').
card_original_type('strionic resonator'/'M14', 'Artifact').
card_original_text('strionic resonator'/'M14', '{2}, {T}: Copy target triggered ability you control. You may choose new targets for the copy. (A triggered ability uses the words \"when,\" \"whenever,\" or \"at.\")').
card_first_print('strionic resonator', 'M14').
card_image_name('strionic resonator'/'M14', 'strionic resonator').
card_uid('strionic resonator'/'M14', 'M14:Strionic Resonator:strionic resonator').
card_rarity('strionic resonator'/'M14', 'Rare').
card_artist('strionic resonator'/'M14', 'Noah Bradley').
card_number('strionic resonator'/'M14', '224').
card_flavor_text('strionic resonator'/'M14', 'Its tines cannot be rung by mere physical force. Only magic of great power can start its eldritch resonance.').
card_multiverse_id('strionic resonator'/'M14', '370670').

card_in_set('suntail hawk', 'M14').
card_original_type('suntail hawk'/'M14', 'Creature — Bird').
card_original_text('suntail hawk'/'M14', 'Flying').
card_image_name('suntail hawk'/'M14', 'suntail hawk').
card_uid('suntail hawk'/'M14', 'M14:Suntail Hawk:suntail hawk').
card_rarity('suntail hawk'/'M14', 'Common').
card_artist('suntail hawk'/'M14', 'Heather Hudson').
card_number('suntail hawk'/'M14', '40').
card_flavor_text('suntail hawk'/'M14', '\"They are the finest companions in the realm. Swift, clever, and easily trained to hunt intruders.\"\n—Almira, falconer').
card_multiverse_id('suntail hawk'/'M14', '370720').

card_in_set('swamp', 'M14').
card_original_type('swamp'/'M14', 'Basic Land — Swamp').
card_original_text('swamp'/'M14', 'B').
card_image_name('swamp'/'M14', 'swamp1').
card_uid('swamp'/'M14', 'M14:Swamp:swamp1').
card_rarity('swamp'/'M14', 'Basic Land').
card_artist('swamp'/'M14', 'Cliff Childs').
card_number('swamp'/'M14', '238').
card_multiverse_id('swamp'/'M14', '370727').

card_in_set('swamp', 'M14').
card_original_type('swamp'/'M14', 'Basic Land — Swamp').
card_original_text('swamp'/'M14', 'B').
card_image_name('swamp'/'M14', 'swamp2').
card_uid('swamp'/'M14', 'M14:Swamp:swamp2').
card_rarity('swamp'/'M14', 'Basic Land').
card_artist('swamp'/'M14', 'Jonas De Ro').
card_number('swamp'/'M14', '239').
card_multiverse_id('swamp'/'M14', '370785').

card_in_set('swamp', 'M14').
card_original_type('swamp'/'M14', 'Basic Land — Swamp').
card_original_text('swamp'/'M14', 'B').
card_image_name('swamp'/'M14', 'swamp3').
card_uid('swamp'/'M14', 'M14:Swamp:swamp3').
card_rarity('swamp'/'M14', 'Basic Land').
card_artist('swamp'/'M14', 'Jung Park').
card_number('swamp'/'M14', '240').
card_multiverse_id('swamp'/'M14', '370731').

card_in_set('swamp', 'M14').
card_original_type('swamp'/'M14', 'Basic Land — Swamp').
card_original_text('swamp'/'M14', 'B').
card_image_name('swamp'/'M14', 'swamp4').
card_uid('swamp'/'M14', 'M14:Swamp:swamp4').
card_rarity('swamp'/'M14', 'Basic Land').
card_artist('swamp'/'M14', 'Andreas Rocha').
card_number('swamp'/'M14', '241').
card_multiverse_id('swamp'/'M14', '370703').

card_in_set('syphon sliver', 'M14').
card_original_type('syphon sliver'/'M14', 'Creature — Sliver').
card_original_text('syphon sliver'/'M14', 'Sliver creatures you control have lifelink. (Damage dealt by a Sliver creature you control also causes you to gain that much life.)').
card_first_print('syphon sliver', 'M14').
card_image_name('syphon sliver'/'M14', 'syphon sliver').
card_uid('syphon sliver'/'M14', 'M14:Syphon Sliver:syphon sliver').
card_rarity('syphon sliver'/'M14', 'Rare').
card_artist('syphon sliver'/'M14', 'Tyler Jacobson').
card_number('syphon sliver'/'M14', '117').
card_flavor_text('syphon sliver'/'M14', 'When the hive must feed, every appendage becomes an additional mouth.').
card_multiverse_id('syphon sliver'/'M14', '370752').

card_in_set('tenacious dead', 'M14').
card_original_type('tenacious dead'/'M14', 'Creature — Skeleton Warrior').
card_original_text('tenacious dead'/'M14', 'When Tenacious Dead dies, you may pay {1}{B}. If you do, return it to the battlefield tapped under its owner\'s control.').
card_first_print('tenacious dead', 'M14').
card_image_name('tenacious dead'/'M14', 'tenacious dead').
card_uid('tenacious dead'/'M14', 'M14:Tenacious Dead:tenacious dead').
card_rarity('tenacious dead'/'M14', 'Uncommon').
card_artist('tenacious dead'/'M14', 'John Stanko').
card_number('tenacious dead'/'M14', '118').
card_flavor_text('tenacious dead'/'M14', 'Raising the bones of Hekjek the Mad proved far easier than getting them to lie back down.').
card_multiverse_id('tenacious dead'/'M14', '370606').

card_in_set('thorncaster sliver', 'M14').
card_original_type('thorncaster sliver'/'M14', 'Creature — Sliver').
card_original_text('thorncaster sliver'/'M14', 'Sliver creatures you control have \"Whenever this creature attacks, it deals 1 damage to target creature or player.\"').
card_first_print('thorncaster sliver', 'M14').
card_image_name('thorncaster sliver'/'M14', 'thorncaster sliver').
card_uid('thorncaster sliver'/'M14', 'M14:Thorncaster Sliver:thorncaster sliver').
card_rarity('thorncaster sliver'/'M14', 'Rare').
card_artist('thorncaster sliver'/'M14', 'Trevor Claxton').
card_number('thorncaster sliver'/'M14', '158').
card_flavor_text('thorncaster sliver'/'M14', 'Every inch of a sliver\'s body is a potential claw, appendage, or projectile.').
card_multiverse_id('thorncaster sliver'/'M14', '370820').

card_in_set('thunder strike', 'M14').
card_original_type('thunder strike'/'M14', 'Instant').
card_original_text('thunder strike'/'M14', 'Target creature gets +2/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_image_name('thunder strike'/'M14', 'thunder strike').
card_uid('thunder strike'/'M14', 'M14:Thunder Strike:thunder strike').
card_rarity('thunder strike'/'M14', 'Common').
card_artist('thunder strike'/'M14', 'Wayne Reynolds').
card_number('thunder strike'/'M14', '159').
card_flavor_text('thunder strike'/'M14', 'Lightning reflexes, thunderous might.').
card_multiverse_id('thunder strike'/'M14', '370607').

card_in_set('tidebinder mage', 'M14').
card_original_type('tidebinder mage'/'M14', 'Creature — Merfolk Wizard').
card_original_text('tidebinder mage'/'M14', 'When Tidebinder Mage enters the battlefield, tap target red or green creature an opponent controls. That creature doesn\'t untap during its controller\'s untap step for as long as you control Tidebinder Mage.').
card_first_print('tidebinder mage', 'M14').
card_image_name('tidebinder mage'/'M14', 'tidebinder mage').
card_uid('tidebinder mage'/'M14', 'M14:Tidebinder Mage:tidebinder mage').
card_rarity('tidebinder mage'/'M14', 'Rare').
card_artist('tidebinder mage'/'M14', 'John Severin Brassell').
card_number('tidebinder mage'/'M14', '73').
card_multiverse_id('tidebinder mage'/'M14', '370736').

card_in_set('time ebb', 'M14').
card_original_type('time ebb'/'M14', 'Sorcery').
card_original_text('time ebb'/'M14', 'Put target creature on top of its owner\'s library.').
card_image_name('time ebb'/'M14', 'time ebb').
card_uid('time ebb'/'M14', 'M14:Time Ebb:time ebb').
card_rarity('time ebb'/'M14', 'Common').
card_artist('time ebb'/'M14', 'Alan Rabinowitz').
card_number('time ebb'/'M14', '74').
card_flavor_text('time ebb'/'M14', '\"Yesterday you were just a figment of your master\'s imagination. I liked you better then.\"\n—Shai Fusan, archmage').
card_multiverse_id('time ebb'/'M14', '370641').

card_in_set('tome scour', 'M14').
card_original_type('tome scour'/'M14', 'Sorcery').
card_original_text('tome scour'/'M14', 'Target player puts the top five cards of his or her library into his or her graveyard.').
card_image_name('tome scour'/'M14', 'tome scour').
card_uid('tome scour'/'M14', 'M14:Tome Scour:tome scour').
card_rarity('tome scour'/'M14', 'Common').
card_artist('tome scour'/'M14', 'Steven Belledin').
card_number('tome scour'/'M14', '75').
card_flavor_text('tome scour'/'M14', '\"I hope none of those were important!\"').
card_multiverse_id('tome scour'/'M14', '370706').

card_in_set('trading post', 'M14').
card_original_type('trading post'/'M14', 'Artifact').
card_original_text('trading post'/'M14', '{1}, {T}, Discard a card: You gain 4 life.{1}, {T}, Pay 1 life: Put a 0/1 white Goat creature token onto the battlefield.{1}, {T}, Sacrifice a creature: Return target artifact card from your graveyard to your hand.{1}, {T}, Sacrifice an artifact: Draw a card.').
card_image_name('trading post'/'M14', 'trading post').
card_uid('trading post'/'M14', 'M14:Trading Post:trading post').
card_rarity('trading post'/'M14', 'Rare').
card_artist('trading post'/'M14', 'Adam Paquette').
card_number('trading post'/'M14', '225').
card_multiverse_id('trading post'/'M14', '370646').

card_in_set('trained condor', 'M14').
card_original_type('trained condor'/'M14', 'Creature — Bird').
card_original_text('trained condor'/'M14', 'FlyingWhenever Trained Condor attacks, another target creature you control gains flying until end of turn.').
card_first_print('trained condor', 'M14').
card_image_name('trained condor'/'M14', 'trained condor').
card_uid('trained condor'/'M14', 'M14:Trained Condor:trained condor').
card_rarity('trained condor'/'M14', 'Common').
card_artist('trained condor'/'M14', 'Alex Horley-Orlandelli').
card_number('trained condor'/'M14', '76').
card_flavor_text('trained condor'/'M14', '\"They\'re smart enough to understand that war means lots of carrion.\"\n—Griv, condor rider').
card_multiverse_id('trained condor'/'M14', '370692').

card_in_set('traumatize', 'M14').
card_original_type('traumatize'/'M14', 'Sorcery').
card_original_text('traumatize'/'M14', 'Target player puts the top half of his or her library, rounded down, into his or her graveyard.').
card_image_name('traumatize'/'M14', 'traumatize').
card_uid('traumatize'/'M14', 'M14:Traumatize:traumatize').
card_rarity('traumatize'/'M14', 'Rare').
card_artist('traumatize'/'M14', 'Greg Staples').
card_number('traumatize'/'M14', '77').
card_flavor_text('traumatize'/'M14', 'He was left with just enough memory to understand what he had lost.').
card_multiverse_id('traumatize'/'M14', '370663').

card_in_set('trollhide', 'M14').
card_original_type('trollhide'/'M14', 'Enchantment — Aura').
card_original_text('trollhide'/'M14', 'Enchant creatureEnchanted creature gets +2/+2 and has \"{1}{G}: Regenerate this creature.\" (The next time the creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('trollhide'/'M14', 'trollhide').
card_uid('trollhide'/'M14', 'M14:Trollhide:trollhide').
card_rarity('trollhide'/'M14', 'Common').
card_artist('trollhide'/'M14', 'Steven Belledin').
card_number('trollhide'/'M14', '197').
card_multiverse_id('trollhide'/'M14', '370664').

card_in_set('undead minotaur', 'M14').
card_original_type('undead minotaur'/'M14', 'Creature — Zombie Minotaur').
card_original_text('undead minotaur'/'M14', '').
card_first_print('undead minotaur', 'M14').
card_image_name('undead minotaur'/'M14', 'undead minotaur').
card_uid('undead minotaur'/'M14', 'M14:Undead Minotaur:undead minotaur').
card_rarity('undead minotaur'/'M14', 'Common').
card_artist('undead minotaur'/'M14', 'Karl Kopinski').
card_number('undead minotaur'/'M14', '119').
card_flavor_text('undead minotaur'/'M14', '\"The work that went into creating this magnificent specimen. Horrific, deadly, well-balanced. Not all necromancers do elegant work like this.\"\n—Lestin, necromancer').
card_multiverse_id('undead minotaur'/'M14', '370702').

card_in_set('vampire warlord', 'M14').
card_original_type('vampire warlord'/'M14', 'Creature — Vampire Warrior').
card_original_text('vampire warlord'/'M14', 'Sacrifice another creature: Regenerate Vampire Warlord. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('vampire warlord', 'M14').
card_image_name('vampire warlord'/'M14', 'vampire warlord').
card_uid('vampire warlord'/'M14', 'M14:Vampire Warlord:vampire warlord').
card_rarity('vampire warlord'/'M14', 'Uncommon').
card_artist('vampire warlord'/'M14', 'Wesley Burt').
card_number('vampire warlord'/'M14', '120').
card_flavor_text('vampire warlord'/'M14', '\"How can you serve me? By dying.\"').
card_multiverse_id('vampire warlord'/'M14', '370709').

card_in_set('vastwood hydra', 'M14').
card_original_type('vastwood hydra'/'M14', 'Creature — Hydra').
card_original_text('vastwood hydra'/'M14', 'Vastwood Hydra enters the battlefield with X +1/+1 counters on it.When Vastwood Hydra dies, you may distribute a number of +1/+1 counters equal to the number of +1/+1 counters on Vastwood Hydra among any number of creatures you control.').
card_first_print('vastwood hydra', 'M14').
card_image_name('vastwood hydra'/'M14', 'vastwood hydra').
card_uid('vastwood hydra'/'M14', 'M14:Vastwood Hydra:vastwood hydra').
card_rarity('vastwood hydra'/'M14', 'Rare').
card_artist('vastwood hydra'/'M14', 'Slawomir Maniak').
card_number('vastwood hydra'/'M14', '198').
card_multiverse_id('vastwood hydra'/'M14', '370749').

card_in_set('verdant haven', 'M14').
card_original_type('verdant haven'/'M14', 'Enchantment — Aura').
card_original_text('verdant haven'/'M14', 'Enchant landWhen Verdant Haven enters the battlefield, you gain 2 life.Whenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_image_name('verdant haven'/'M14', 'verdant haven').
card_uid('verdant haven'/'M14', 'M14:Verdant Haven:verdant haven').
card_rarity('verdant haven'/'M14', 'Common').
card_artist('verdant haven'/'M14', 'Daniel Ljunggren').
card_number('verdant haven'/'M14', '199').
card_multiverse_id('verdant haven'/'M14', '370657').

card_in_set('vial of poison', 'M14').
card_original_type('vial of poison'/'M14', 'Artifact').
card_original_text('vial of poison'/'M14', '{1}, Sacrifice Vial of Poison: Target creature gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('vial of poison', 'M14').
card_image_name('vial of poison'/'M14', 'vial of poison').
card_uid('vial of poison'/'M14', 'M14:Vial of Poison:vial of poison').
card_rarity('vial of poison'/'M14', 'Uncommon').
card_artist('vial of poison'/'M14', 'Franz Vohwinkel').
card_number('vial of poison'/'M14', '226').
card_flavor_text('vial of poison'/'M14', 'There are worse ways to die, but not many.').
card_multiverse_id('vial of poison'/'M14', '370640').

card_in_set('vile rebirth', 'M14').
card_original_type('vile rebirth'/'M14', 'Instant').
card_original_text('vile rebirth'/'M14', 'Exile target creature card from a graveyard. Put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('vile rebirth'/'M14', 'vile rebirth').
card_uid('vile rebirth'/'M14', 'M14:Vile Rebirth:vile rebirth').
card_rarity('vile rebirth'/'M14', 'Common').
card_artist('vile rebirth'/'M14', 'Erica Yang').
card_number('vile rebirth'/'M14', '121').
card_flavor_text('vile rebirth'/'M14', 'Grave robbers digging down cautiously listen for anything digging up.').
card_multiverse_id('vile rebirth'/'M14', '370799').

card_in_set('volcanic geyser', 'M14').
card_original_type('volcanic geyser'/'M14', 'Instant').
card_original_text('volcanic geyser'/'M14', 'Volcanic Geyser deals X damage to target creature or player.').
card_image_name('volcanic geyser'/'M14', 'volcanic geyser').
card_uid('volcanic geyser'/'M14', 'M14:Volcanic Geyser:volcanic geyser').
card_rarity('volcanic geyser'/'M14', 'Uncommon').
card_artist('volcanic geyser'/'M14', 'Clint Cearley').
card_number('volcanic geyser'/'M14', '160').
card_flavor_text('volcanic geyser'/'M14', '\"The common folk call it the breaking of the crust or the spilling of fire. Geomancers call it target practice.\"\n—Ryda, geomancer').
card_multiverse_id('volcanic geyser'/'M14', '370614').

card_in_set('voracious wurm', 'M14').
card_original_type('voracious wurm'/'M14', 'Creature — Wurm').
card_original_text('voracious wurm'/'M14', 'Voracious Wurm enters the battlefield with X +1/+1 counters on it, where X is the amount of life you\'ve gained this turn.').
card_first_print('voracious wurm', 'M14').
card_image_name('voracious wurm'/'M14', 'voracious wurm').
card_uid('voracious wurm'/'M14', 'M14:Voracious Wurm:voracious wurm').
card_rarity('voracious wurm'/'M14', 'Uncommon').
card_artist('voracious wurm'/'M14', 'Igor Kieryluk').
card_number('voracious wurm'/'M14', '200').
card_flavor_text('voracious wurm'/'M14', 'Shepherds in Kalonia know it\'s better to let sheep roam than have them be trapped when the wurms are feeding.').
card_multiverse_id('voracious wurm'/'M14', '370814').

card_in_set('wall of frost', 'M14').
card_original_type('wall of frost'/'M14', 'Creature — Wall').
card_original_text('wall of frost'/'M14', 'Defender (This creature can\'t attack.)Whenever Wall of Frost blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_image_name('wall of frost'/'M14', 'wall of frost').
card_uid('wall of frost'/'M14', 'M14:Wall of Frost:wall of frost').
card_rarity('wall of frost'/'M14', 'Uncommon').
card_artist('wall of frost'/'M14', 'Mike Bierek').
card_number('wall of frost'/'M14', '78').
card_flavor_text('wall of frost'/'M14', '\"I have seen countless petty warmongers gaze on it for a time before turning away.\"\n—Sarlena, paladin of the Northern Verge').
card_multiverse_id('wall of frost'/'M14', '370690').

card_in_set('wall of swords', 'M14').
card_original_type('wall of swords'/'M14', 'Creature — Wall').
card_original_text('wall of swords'/'M14', 'Defender (This creature can\'t attack)Flying').
card_image_name('wall of swords'/'M14', 'wall of swords').
card_uid('wall of swords'/'M14', 'M14:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'M14', 'Uncommon').
card_artist('wall of swords'/'M14', 'Zoltan Boros & Gabor Szikszai').
card_number('wall of swords'/'M14', '41').
card_flavor_text('wall of swords'/'M14', '\"When I said we needed more swords to protect the realm, this was not quite what I had in mind.\"\n—Olander, tactician of the Northern Verge').
card_multiverse_id('wall of swords'/'M14', '370697').

card_in_set('warden of evos isle', 'M14').
card_original_type('warden of evos isle'/'M14', 'Creature — Bird Wizard').
card_original_text('warden of evos isle'/'M14', 'FlyingCreature spells with flying you cast cost {1} less to cast.').
card_first_print('warden of evos isle', 'M14').
card_image_name('warden of evos isle'/'M14', 'warden of evos isle').
card_uid('warden of evos isle'/'M14', 'M14:Warden of Evos Isle:warden of evos isle').
card_rarity('warden of evos isle'/'M14', 'Uncommon').
card_artist('warden of evos isle'/'M14', 'Nils Hamm').
card_number('warden of evos isle'/'M14', '79').
card_flavor_text('warden of evos isle'/'M14', 'On Evos Isle, the swift and formidable aven enforce the will of the ruling sphinxes.').
card_multiverse_id('warden of evos isle'/'M14', '370815').

card_in_set('water servant', 'M14').
card_original_type('water servant'/'M14', 'Creature — Elemental').
card_original_text('water servant'/'M14', '{U}: Water Servant gets +1/-1 until end of turn.{U}: Water Servant gets -1/+1 until end of turn.').
card_image_name('water servant'/'M14', 'water servant').
card_uid('water servant'/'M14', 'M14:Water Servant:water servant').
card_rarity('water servant'/'M14', 'Uncommon').
card_artist('water servant'/'M14', 'Igor Kieryluk').
card_number('water servant'/'M14', '80').
card_flavor_text('water servant'/'M14', '\"This creature has innate perceptiveness. It knows when to rise and when to vanish into the tides.\"\n—Jestus Dreya, Of Elements and Eternity').
card_multiverse_id('water servant'/'M14', '370809').

card_in_set('wild guess', 'M14').
card_original_type('wild guess'/'M14', 'Sorcery').
card_original_text('wild guess'/'M14', 'As an additional cost to cast Wild Guess, discard a card.Draw two cards.').
card_image_name('wild guess'/'M14', 'wild guess').
card_uid('wild guess'/'M14', 'M14:Wild Guess:wild guess').
card_rarity('wild guess'/'M14', 'Common').
card_artist('wild guess'/'M14', 'Lucas Graciano').
card_number('wild guess'/'M14', '161').
card_flavor_text('wild guess'/'M14', 'No guts, no glory.').
card_multiverse_id('wild guess'/'M14', '370791').

card_in_set('wild ricochet', 'M14').
card_original_type('wild ricochet'/'M14', 'Instant').
card_original_text('wild ricochet'/'M14', 'You may choose new targets for target instant or sorcery spell. Then copy that spell. You may choose new targets for the copy.').
card_image_name('wild ricochet'/'M14', 'wild ricochet').
card_uid('wild ricochet'/'M14', 'M14:Wild Ricochet:wild ricochet').
card_rarity('wild ricochet'/'M14', 'Rare').
card_artist('wild ricochet'/'M14', 'Dan Scott').
card_number('wild ricochet'/'M14', '162').
card_flavor_text('wild ricochet'/'M14', '\"May your day never match your expectations.\"\n—Scriptus Fleen, flectomancer').
card_multiverse_id('wild ricochet'/'M14', '370790').

card_in_set('windreader sphinx', 'M14').
card_original_type('windreader sphinx'/'M14', 'Creature — Sphinx').
card_original_text('windreader sphinx'/'M14', 'FlyingWhenever a creature with flying attacks, you may draw a card.').
card_first_print('windreader sphinx', 'M14').
card_image_name('windreader sphinx'/'M14', 'windreader sphinx').
card_uid('windreader sphinx'/'M14', 'M14:Windreader Sphinx:windreader sphinx').
card_rarity('windreader sphinx'/'M14', 'Mythic Rare').
card_artist('windreader sphinx'/'M14', 'Min Yum').
card_number('windreader sphinx'/'M14', '81').
card_flavor_text('windreader sphinx'/'M14', '\"Your mind is too filled with foolish concerns to hear the subtle whispers that the breeze brings.\"').
card_multiverse_id('windreader sphinx'/'M14', '370810').

card_in_set('windstorm', 'M14').
card_original_type('windstorm'/'M14', 'Instant').
card_original_text('windstorm'/'M14', 'Windstorm deals X damage to each creature with flying.').
card_image_name('windstorm'/'M14', 'windstorm').
card_uid('windstorm'/'M14', 'M14:Windstorm:windstorm').
card_rarity('windstorm'/'M14', 'Uncommon').
card_artist('windstorm'/'M14', 'Rob Alexander').
card_number('windstorm'/'M14', '201').
card_flavor_text('windstorm'/'M14', '\"We don\'t like interlopers and thieves cluttering our skies.\"\n—Dionus, elvish archdruid').
card_multiverse_id('windstorm'/'M14', '370628').

card_in_set('witchstalker', 'M14').
card_original_type('witchstalker'/'M14', 'Creature — Wolf').
card_original_text('witchstalker'/'M14', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)Whenever an opponent casts a blue or black spell during your turn, put a +1/+1 counter on Witchstalker.').
card_first_print('witchstalker', 'M14').
card_image_name('witchstalker'/'M14', 'witchstalker').
card_uid('witchstalker'/'M14', 'M14:Witchstalker:witchstalker').
card_rarity('witchstalker'/'M14', 'Rare').
card_artist('witchstalker'/'M14', 'Christopher Moeller').
card_number('witchstalker'/'M14', '202').
card_multiverse_id('witchstalker'/'M14', '370806').

card_in_set('woodborn behemoth', 'M14').
card_original_type('woodborn behemoth'/'M14', 'Creature — Elemental').
card_original_text('woodborn behemoth'/'M14', 'As long as you control eight or more lands, Woodborn Behemoth gets +4/+4 and has trample. (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('woodborn behemoth', 'M14').
card_image_name('woodborn behemoth'/'M14', 'woodborn behemoth').
card_uid('woodborn behemoth'/'M14', 'M14:Woodborn Behemoth:woodborn behemoth').
card_rarity('woodborn behemoth'/'M14', 'Uncommon').
card_artist('woodborn behemoth'/'M14', 'Matt Stewart').
card_number('woodborn behemoth'/'M14', '203').
card_multiverse_id('woodborn behemoth'/'M14', '370665').

card_in_set('wring flesh', 'M14').
card_original_type('wring flesh'/'M14', 'Instant').
card_original_text('wring flesh'/'M14', 'Target creature gets -3/-1 until end of turn.').
card_image_name('wring flesh'/'M14', 'wring flesh').
card_uid('wring flesh'/'M14', 'M14:Wring Flesh:wring flesh').
card_rarity('wring flesh'/'M14', 'Common').
card_artist('wring flesh'/'M14', 'Izzy').
card_number('wring flesh'/'M14', '122').
card_flavor_text('wring flesh'/'M14', '\"You shouldn\'t be so attached to something as fragile as flesh.\"\n—Zul Ashur, lich lord').
card_multiverse_id('wring flesh'/'M14', '370759').

card_in_set('xathrid necromancer', 'M14').
card_original_type('xathrid necromancer'/'M14', 'Creature — Human Wizard').
card_original_text('xathrid necromancer'/'M14', 'Whenever Xathrid Necromancer or another Human creature you control dies, put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_image_name('xathrid necromancer'/'M14', 'xathrid necromancer').
card_uid('xathrid necromancer'/'M14', 'M14:Xathrid Necromancer:xathrid necromancer').
card_rarity('xathrid necromancer'/'M14', 'Rare').
card_artist('xathrid necromancer'/'M14', 'Maciej Kuciara').
card_number('xathrid necromancer'/'M14', '123').
card_flavor_text('xathrid necromancer'/'M14', '\"My commands shall echo forever in their dusty skulls.\"').
card_multiverse_id('xathrid necromancer'/'M14', '370619').

card_in_set('young pyromancer', 'M14').
card_original_type('young pyromancer'/'M14', 'Creature — Human Shaman').
card_original_text('young pyromancer'/'M14', 'Whenever you cast an instant or sorcery spell, put a 1/1 red Elemental creature token onto the battlefield.').
card_first_print('young pyromancer', 'M14').
card_image_name('young pyromancer'/'M14', 'young pyromancer').
card_uid('young pyromancer'/'M14', 'M14:Young Pyromancer:young pyromancer').
card_rarity('young pyromancer'/'M14', 'Uncommon').
card_artist('young pyromancer'/'M14', 'Cynthia Sheppard').
card_number('young pyromancer'/'M14', '163').
card_flavor_text('young pyromancer'/'M14', 'Immolation is the sincerest form of flattery.').
card_multiverse_id('young pyromancer'/'M14', '370600').

card_in_set('zephyr charge', 'M14').
card_original_type('zephyr charge'/'M14', 'Enchantment').
card_original_text('zephyr charge'/'M14', '{1}{U}: Target creature gains flying until end of turn.').
card_first_print('zephyr charge', 'M14').
card_image_name('zephyr charge'/'M14', 'zephyr charge').
card_uid('zephyr charge'/'M14', 'M14:Zephyr Charge:zephyr charge').
card_rarity('zephyr charge'/'M14', 'Common').
card_artist('zephyr charge'/'M14', 'Steve Prescott').
card_number('zephyr charge'/'M14', '82').
card_flavor_text('zephyr charge'/'M14', '\"All armies prefer high ground to low and sunny places to dark.\"\n—Sun Tzu, Art of War, trans. Giles').
card_multiverse_id('zephyr charge'/'M14', '370672').
