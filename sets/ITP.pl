% Introductory Two-Player Set

set('ITP').
set_name('ITP', 'Introductory Two-Player Set').
set_release_date('ITP', '1996-12-31').
set_border('ITP', 'white').
set_type('ITP', 'starter').

card_in_set('alabaster potion', 'ITP').
card_original_type('alabaster potion'/'ITP', 'Instant').
card_original_text('alabaster potion'/'ITP', 'Target player gains X life, or prevent X damage to any creature or player.').
card_image_name('alabaster potion'/'ITP', 'alabaster potion').
card_uid('alabaster potion'/'ITP', 'ITP:Alabaster Potion:alabaster potion').
card_rarity('alabaster potion'/'ITP', 'Special').
card_artist('alabaster potion'/'ITP', 'Harold McNeill').
card_flavor_text('alabaster potion'/'ITP', '\"Healing is a matter of time, but it is sometimes also a matter of opportunity.\"\n—D\'Avenant proverb').

card_in_set('battering ram', 'ITP').
card_original_type('battering ram'/'ITP', 'Artifact Creature').
card_original_text('battering ram'/'ITP', 'Banding when attacking\nWhenever a wall blocks Battering Ram, destroy that creature at end of combat.').
card_image_name('battering ram'/'ITP', 'battering ram').
card_uid('battering ram'/'ITP', 'ITP:Battering Ram:battering ram').
card_rarity('battering ram'/'ITP', 'Special').
card_artist('battering ram'/'ITP', 'Jeff A. Menges').
card_flavor_text('battering ram'/'ITP', 'By the time Mishra was defeated, no mage was foolish enough to rely heavily on walls.').

card_in_set('bog imp', 'ITP').
card_original_type('bog imp'/'ITP', 'Summon — Imp').
card_original_text('bog imp'/'ITP', 'Flying').
card_image_name('bog imp'/'ITP', 'bog imp').
card_uid('bog imp'/'ITP', 'ITP:Bog Imp:bog imp').
card_rarity('bog imp'/'ITP', 'Special').
card_artist('bog imp'/'ITP', 'Ron Spencer').
card_flavor_text('bog imp'/'ITP', 'On guard for larger dangers, we underestimated the power and speed of the Imp\'s muck-crusted claws.').

card_in_set('bog wraith', 'ITP').
card_original_type('bog wraith'/'ITP', 'Summon — Wraith').
card_original_text('bog wraith'/'ITP', 'Swampwalk').
card_image_name('bog wraith'/'ITP', 'bog wraith').
card_uid('bog wraith'/'ITP', 'ITP:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'ITP', 'Special').
card_artist('bog wraith'/'ITP', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'ITP', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').

card_in_set('circle of protection: black', 'ITP').
card_original_type('circle of protection: black'/'ITP', 'Enchantment').
card_original_text('circle of protection: black'/'ITP', '{1}: Prevent all damage to you from one black source. Further damage from that source is treated normally.').
card_image_name('circle of protection: black'/'ITP', 'circle of protection black').
card_uid('circle of protection: black'/'ITP', 'ITP:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'ITP', 'Special').
card_artist('circle of protection: black'/'ITP', 'Jesper Myrfors').

card_in_set('circle of protection: red', 'ITP').
card_original_type('circle of protection: red'/'ITP', 'Enchantment').
card_original_text('circle of protection: red'/'ITP', '{1}: Prevent all damage to you from one red source. Further damage from that source is treated normally.').
card_image_name('circle of protection: red'/'ITP', 'circle of protection red').
card_uid('circle of protection: red'/'ITP', 'ITP:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'ITP', 'Special').
card_artist('circle of protection: red'/'ITP', 'Mark Tedin').

card_in_set('clockwork beast', 'ITP').
card_original_type('clockwork beast'/'ITP', 'Artifact Creature').
card_original_text('clockwork beast'/'ITP', 'When Clockwork Beast comes into play, put seven +1/+0 counters on it. At the end of any combat in which Clockwork Beast is assigned to attack or block, remove a counter.\n{X}, {T}: Put X +1/+0 counters on Clockwork Beast. You may have no more than seven of these counters on Clockwork Beast. Use this ability only during your upkeep.').
card_image_name('clockwork beast'/'ITP', 'clockwork beast').
card_uid('clockwork beast'/'ITP', 'ITP:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'ITP', 'Special').
card_artist('clockwork beast'/'ITP', 'Drew Tucker').

card_in_set('cursed land', 'ITP').
card_original_type('cursed land'/'ITP', 'Enchant Land').
card_original_text('cursed land'/'ITP', 'During enchanted land\'s controller\'s upkeep, Cursed Land deals 1 damage to the player.').
card_image_name('cursed land'/'ITP', 'cursed land').
card_uid('cursed land'/'ITP', 'ITP:Cursed Land:cursed land').
card_rarity('cursed land'/'ITP', 'Special').
card_artist('cursed land'/'ITP', 'Jesper Myrfors').

card_in_set('dark ritual', 'ITP').
card_original_type('dark ritual'/'ITP', 'Interrupt').
card_original_text('dark ritual'/'ITP', 'Add {B}{B}{B} to your mana pool.').
card_image_name('dark ritual'/'ITP', 'dark ritual').
card_uid('dark ritual'/'ITP', 'ITP:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'ITP', 'Special').
card_artist('dark ritual'/'ITP', 'Sandra Everingham').

card_in_set('detonate', 'ITP').
card_original_type('detonate'/'ITP', 'Sorcery').
card_original_text('detonate'/'ITP', 'Bury target artifact with casting cost equal to X. Detonate deals X damage to that artifact\'s controller.').
card_image_name('detonate'/'ITP', 'detonate').
card_uid('detonate'/'ITP', 'ITP:Detonate:detonate').
card_rarity('detonate'/'ITP', 'Special').
card_artist('detonate'/'ITP', 'Randy Asplund-Faith').

card_in_set('disintegrate', 'ITP').
card_original_type('disintegrate'/'ITP', 'Sorcery').
card_original_text('disintegrate'/'ITP', 'Disintegrate deals X damage to target creature or player. That creature cannot regenerate until end of turn. If the creature is dealt lethal damage this turn, remove it from the game.').
card_image_name('disintegrate'/'ITP', 'disintegrate').
card_uid('disintegrate'/'ITP', 'ITP:Disintegrate:disintegrate').
card_rarity('disintegrate'/'ITP', 'Special').
card_artist('disintegrate'/'ITP', 'Anson Maddocks').

card_in_set('durkwood boars', 'ITP').
card_original_type('durkwood boars'/'ITP', 'Summon — Boars').
card_original_text('durkwood boars'/'ITP', '').
card_image_name('durkwood boars'/'ITP', 'durkwood boars').
card_uid('durkwood boars'/'ITP', 'ITP:Durkwood Boars:durkwood boars').
card_rarity('durkwood boars'/'ITP', 'Special').
card_artist('durkwood boars'/'ITP', 'Mike Kimble').
card_flavor_text('durkwood boars'/'ITP', '\"And the unclean spirits went out, and entered the swine: and the herd ran violently . . . .\"\n—Mark 5:13').

card_in_set('elven riders', 'ITP').
card_original_type('elven riders'/'ITP', 'Summon — Riders').
card_original_text('elven riders'/'ITP', 'Elven Riders cannot be blocked except by creatures with flying or walls.').
card_image_name('elven riders'/'ITP', 'elven riders').
card_uid('elven riders'/'ITP', 'ITP:Elven Riders:elven riders').
card_rarity('elven riders'/'ITP', 'Special').
card_artist('elven riders'/'ITP', 'Melissa A. Benson').
card_flavor_text('elven riders'/'ITP', '\"Sometimes it is better to be swift of foot than strong of swordarm.\"\n—Elven proverb').

card_in_set('elvish archers', 'ITP').
card_original_type('elvish archers'/'ITP', 'Summon — Elves').
card_original_text('elvish archers'/'ITP', 'First strike').
card_image_name('elvish archers'/'ITP', 'elvish archers').
card_uid('elvish archers'/'ITP', 'ITP:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'ITP', 'Special').
card_artist('elvish archers'/'ITP', 'Anson Maddocks').
card_flavor_text('elvish archers'/'ITP', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').

card_in_set('energy flux', 'ITP').
card_original_type('energy flux'/'ITP', 'Enchantment').
card_original_text('energy flux'/'ITP', 'All artifacts in play gain \"During your upkeep, pay an additional {2} or bury this artifact.\"').
card_image_name('energy flux'/'ITP', 'energy flux').
card_uid('energy flux'/'ITP', 'ITP:Energy Flux:energy flux').
card_rarity('energy flux'/'ITP', 'Special').
card_artist('energy flux'/'ITP', 'Kaja Foglio').

card_in_set('feedback', 'ITP').
card_original_type('feedback'/'ITP', 'Enchant Enchantment').
card_original_text('feedback'/'ITP', 'During enchanted enchantment\'s controller\'s upkeep, Feedback deals 1 damage to that player.').
card_image_name('feedback'/'ITP', 'feedback').
card_uid('feedback'/'ITP', 'ITP:Feedback:feedback').
card_rarity('feedback'/'ITP', 'Special').
card_artist('feedback'/'ITP', 'Quinton Hoover').

card_in_set('fireball', 'ITP').
card_original_type('fireball'/'ITP', 'Sorcery').
card_original_text('fireball'/'ITP', 'Fireball deals X damage divided evenly, round down, among any number of target creatures and/or players. Pay an additional {1} for each target beyond the first.').
card_image_name('fireball'/'ITP', 'fireball').
card_uid('fireball'/'ITP', 'ITP:Fireball:fireball').
card_rarity('fireball'/'ITP', 'Special').
card_artist('fireball'/'ITP', 'Mark Tedin').

card_in_set('forest', 'ITP').
card_original_type('forest'/'ITP', 'Land').
card_original_text('forest'/'ITP', '{T}: Add {G} to your mana pool.').
card_image_name('forest'/'ITP', 'forest1').
card_uid('forest'/'ITP', 'ITP:Forest:forest1').
card_rarity('forest'/'ITP', 'Basic Land').
card_artist('forest'/'ITP', 'Christopher Rush').

card_in_set('glasses of urza', 'ITP').
card_original_type('glasses of urza'/'ITP', 'Artifact').
card_original_text('glasses of urza'/'ITP', '{T}: Look at target player\'s hand.').
card_image_name('glasses of urza'/'ITP', 'glasses of urza').
card_uid('glasses of urza'/'ITP', 'ITP:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'ITP', 'Special').
card_artist('glasses of urza'/'ITP', 'Douglas Shuler').

card_in_set('grizzly bears', 'ITP').
card_original_type('grizzly bears'/'ITP', 'Summon — Bears').
card_original_text('grizzly bears'/'ITP', '').
card_image_name('grizzly bears'/'ITP', 'grizzly bears').
card_uid('grizzly bears'/'ITP', 'ITP:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'ITP', 'Special').
card_artist('grizzly bears'/'ITP', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'ITP', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').

card_in_set('healing salve', 'ITP').
card_original_type('healing salve'/'ITP', 'Instant').
card_original_text('healing salve'/'ITP', 'Target player gains 3 life, or prevent up to 3 damage to any creature or player.').
card_image_name('healing salve'/'ITP', 'healing salve').
card_uid('healing salve'/'ITP', 'ITP:Healing Salve:healing salve').
card_rarity('healing salve'/'ITP', 'Special').
card_artist('healing salve'/'ITP', 'Dan Frazier').

card_in_set('hill giant', 'ITP').
card_original_type('hill giant'/'ITP', 'Summon — Giant').
card_original_text('hill giant'/'ITP', '').
card_image_name('hill giant'/'ITP', 'hill giant').
card_uid('hill giant'/'ITP', 'ITP:Hill Giant:hill giant').
card_rarity('hill giant'/'ITP', 'Special').
card_artist('hill giant'/'ITP', 'Dan Frazier').
card_flavor_text('hill giant'/'ITP', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').

card_in_set('ironclaw orcs', 'ITP').
card_original_type('ironclaw orcs'/'ITP', 'Summon — Orcs').
card_original_text('ironclaw orcs'/'ITP', 'Ironclaw Orcs cannot be assigned to block any creature with power 2 or greater.').
card_image_name('ironclaw orcs'/'ITP', 'ironclaw orcs').
card_uid('ironclaw orcs'/'ITP', 'ITP:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'ITP', 'Special').
card_artist('ironclaw orcs'/'ITP', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'ITP', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').

card_in_set('island', 'ITP').
card_original_type('island'/'ITP', 'Land').
card_original_text('island'/'ITP', '{T}: Add {U} to your mana pool.').
card_image_name('island'/'ITP', 'island1').
card_uid('island'/'ITP', 'ITP:Island:island1').
card_rarity('island'/'ITP', 'Basic Land').
card_artist('island'/'ITP', 'Mark Poole').

card_in_set('jayemdae tome', 'ITP').
card_original_type('jayemdae tome'/'ITP', 'Artifact').
card_original_text('jayemdae tome'/'ITP', '{4}, {T}: Draw a card.').
card_image_name('jayemdae tome'/'ITP', 'jayemdae tome').
card_uid('jayemdae tome'/'ITP', 'ITP:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'ITP', 'Special').
card_artist('jayemdae tome'/'ITP', 'Mark Tedin').

card_in_set('lost soul', 'ITP').
card_original_type('lost soul'/'ITP', 'Summon — Lost Soul').
card_original_text('lost soul'/'ITP', 'Swampwalk').
card_image_name('lost soul'/'ITP', 'lost soul').
card_uid('lost soul'/'ITP', 'ITP:Lost Soul:lost soul').
card_rarity('lost soul'/'ITP', 'Special').
card_artist('lost soul'/'ITP', 'Randy Asplund-Faith').
card_flavor_text('lost soul'/'ITP', 'Her hand gently beckons,\nshe whispers your name—\nBut those who go with her\nare never the same.').

card_in_set('merfolk of the pearl trident', 'ITP').
card_original_type('merfolk of the pearl trident'/'ITP', 'Summon — Merfolk').
card_original_text('merfolk of the pearl trident'/'ITP', '').
card_image_name('merfolk of the pearl trident'/'ITP', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'ITP', 'ITP:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'ITP', 'Special').
card_artist('merfolk of the pearl trident'/'ITP', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'ITP', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').

card_in_set('mesa pegasus', 'ITP').
card_original_type('mesa pegasus'/'ITP', 'Summon — Pegasus').
card_original_text('mesa pegasus'/'ITP', 'Banding, flying').
card_image_name('mesa pegasus'/'ITP', 'mesa pegasus').
card_uid('mesa pegasus'/'ITP', 'ITP:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'ITP', 'Special').
card_artist('mesa pegasus'/'ITP', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'ITP', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').

card_in_set('mons\'s goblin raiders', 'ITP').
card_original_type('mons\'s goblin raiders'/'ITP', 'Summon — Goblins').
card_original_text('mons\'s goblin raiders'/'ITP', '').
card_image_name('mons\'s goblin raiders'/'ITP', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'ITP', 'ITP:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'ITP', 'Special').
card_artist('mons\'s goblin raiders'/'ITP', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'ITP', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his Raiders are the thunderhead that leads in the storm.').

card_in_set('mountain', 'ITP').
card_original_type('mountain'/'ITP', 'Land').
card_original_text('mountain'/'ITP', '{T}: Add {R} to your mana pool.').
card_image_name('mountain'/'ITP', 'mountain1').
card_uid('mountain'/'ITP', 'ITP:Mountain:mountain1').
card_rarity('mountain'/'ITP', 'Basic Land').
card_artist('mountain'/'ITP', 'Douglas Shuler').

card_in_set('murk dwellers', 'ITP').
card_original_type('murk dwellers'/'ITP', 'Summon — Murk Dwellers').
card_original_text('murk dwellers'/'ITP', 'If Murk Dwellers attacks and is not blocked, it gets +2/+0 until end of combat.').
card_image_name('murk dwellers'/'ITP', 'murk dwellers').
card_uid('murk dwellers'/'ITP', 'ITP:Murk Dwellers:murk dwellers').
card_rarity('murk dwellers'/'ITP', 'Special').
card_artist('murk dwellers'/'ITP', 'Drew Tucker').
card_flavor_text('murk dwellers'/'ITP', 'When Raganorn unsealed the catacombs, he found more than the dead and their treasures.').

card_in_set('orcish artillery', 'ITP').
card_original_type('orcish artillery'/'ITP', 'Summon — Orcs').
card_original_text('orcish artillery'/'ITP', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_image_name('orcish artillery'/'ITP', 'orcish artillery').
card_uid('orcish artillery'/'ITP', 'ITP:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'ITP', 'Special').
card_artist('orcish artillery'/'ITP', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'ITP', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').

card_in_set('orcish oriflamme', 'ITP').
card_original_type('orcish oriflamme'/'ITP', 'Enchantment').
card_original_text('orcish oriflamme'/'ITP', 'All attacking creatures you control get +1/+0.').
card_image_name('orcish oriflamme'/'ITP', 'orcish oriflamme').
card_uid('orcish oriflamme'/'ITP', 'ITP:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'ITP', 'Special').
card_artist('orcish oriflamme'/'ITP', 'Dan Frazier').

card_in_set('pearled unicorn', 'ITP').
card_original_type('pearled unicorn'/'ITP', 'Summon — Unicorn').
card_original_text('pearled unicorn'/'ITP', '').
card_image_name('pearled unicorn'/'ITP', 'pearled unicorn').
card_uid('pearled unicorn'/'ITP', 'ITP:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'ITP', 'Special').
card_artist('pearled unicorn'/'ITP', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'ITP', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\" —Lewis Carroll').

card_in_set('phantom monster', 'ITP').
card_original_type('phantom monster'/'ITP', 'Summon — Phantasm').
card_original_text('phantom monster'/'ITP', 'Flying').
card_image_name('phantom monster'/'ITP', 'phantom monster').
card_uid('phantom monster'/'ITP', 'ITP:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'ITP', 'Special').
card_artist('phantom monster'/'ITP', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'ITP', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').

card_in_set('plains', 'ITP').
card_original_type('plains'/'ITP', 'Land').
card_original_text('plains'/'ITP', '{T}: Add {W} to your mana pool.').
card_image_name('plains'/'ITP', 'plains1').
card_uid('plains'/'ITP', 'ITP:Plains:plains1').
card_rarity('plains'/'ITP', 'Basic Land').
card_artist('plains'/'ITP', 'Jesper Myrfors').

card_in_set('power sink', 'ITP').
card_original_type('power sink'/'ITP', 'Interrupt').
card_original_text('power sink'/'ITP', 'Counter target spell unless that spell\'s caster pays an additional {X}. That player draws and pays all available mana from lands and mana pool until {X} is paid; he or she may also draw and pay mana from other sources if desired.').
card_image_name('power sink'/'ITP', 'power sink').
card_uid('power sink'/'ITP', 'ITP:Power Sink:power sink').
card_rarity('power sink'/'ITP', 'Special').
card_artist('power sink'/'ITP', 'Richard Thomas').

card_in_set('pyrotechnics', 'ITP').
card_original_type('pyrotechnics'/'ITP', 'Sorcery').
card_original_text('pyrotechnics'/'ITP', 'Pyrotechnics deals 4 damage divided any way you choose among any number of target creatures and/or players.').
card_image_name('pyrotechnics'/'ITP', 'pyrotechnics').
card_uid('pyrotechnics'/'ITP', 'ITP:Pyrotechnics:pyrotechnics').
card_rarity('pyrotechnics'/'ITP', 'Special').
card_artist('pyrotechnics'/'ITP', 'Anson Maddocks').
card_flavor_text('pyrotechnics'/'ITP', '\"Hi! ni! ya! Behold the man of flint, that\'s me!\nFour lightnings zigzag from me, strike and return.\" —Navajo war chant').

card_in_set('raise dead', 'ITP').
card_original_type('raise dead'/'ITP', 'Sorcery').
card_original_text('raise dead'/'ITP', 'Put target creature card from your graveyard into your hand.').
card_image_name('raise dead'/'ITP', 'raise dead').
card_uid('raise dead'/'ITP', 'ITP:Raise Dead:raise dead').
card_rarity('raise dead'/'ITP', 'Special').
card_artist('raise dead'/'ITP', 'Jeff A. Menges').

card_in_set('reverse damage', 'ITP').
card_original_type('reverse damage'/'ITP', 'Instant').
card_original_text('reverse damage'/'ITP', 'All damage dealt to you frmo one source this turn is retroactively added to your life total instead of subtracted from it. Further damage from that source is treated normally.').
card_image_name('reverse damage'/'ITP', 'reverse damage').
card_uid('reverse damage'/'ITP', 'ITP:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'ITP', 'Special').
card_artist('reverse damage'/'ITP', 'Dameon Willich').

card_in_set('rod of ruin', 'ITP').
card_original_type('rod of ruin'/'ITP', 'Artifact').
card_original_text('rod of ruin'/'ITP', '{3}, {T}: Rod of Ruin deals 1 damage to target creature or player.').
card_image_name('rod of ruin'/'ITP', 'rod of ruin').
card_uid('rod of ruin'/'ITP', 'ITP:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'ITP', 'Special').
card_artist('rod of ruin'/'ITP', 'Christopher Rush').

card_in_set('scathe zombies', 'ITP').
card_original_type('scathe zombies'/'ITP', 'Summon — Zombies').
card_original_text('scathe zombies'/'ITP', '').
card_image_name('scathe zombies'/'ITP', 'scathe zombies').
card_uid('scathe zombies'/'ITP', 'ITP:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'ITP', 'Special').
card_artist('scathe zombies'/'ITP', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'ITP', '\"They groaned, they stirred, they all uprose, Nor spake, nor moved their eyes; It had been strange, even in a dream, To have seen those dead men rise.\" —Samuel Coleridge, \"The Rime of the Ancient Mariner\"').

card_in_set('scryb sprites', 'ITP').
card_original_type('scryb sprites'/'ITP', 'Summon — Faeries').
card_original_text('scryb sprites'/'ITP', 'Flying').
card_image_name('scryb sprites'/'ITP', 'scryb sprites').
card_uid('scryb sprites'/'ITP', 'ITP:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'ITP', 'Special').
card_artist('scryb sprites'/'ITP', 'Amy Weber').
card_flavor_text('scryb sprites'/'ITP', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').

card_in_set('sorceress queen', 'ITP').
card_original_type('sorceress queen'/'ITP', 'Summon — Sorceress').
card_original_text('sorceress queen'/'ITP', '{T}: Target creature other than Sorceress Queen becomes 0/2 until end of turn.').
card_image_name('sorceress queen'/'ITP', 'sorceress queen').
card_uid('sorceress queen'/'ITP', 'ITP:Sorceress Queen:sorceress queen').
card_rarity('sorceress queen'/'ITP', 'Special').
card_artist('sorceress queen'/'ITP', 'Kaja Foglio').

card_in_set('swamp', 'ITP').
card_original_type('swamp'/'ITP', 'Land').
card_original_text('swamp'/'ITP', '{T}: Add {B} to your mana pool.').
card_image_name('swamp'/'ITP', 'swamp1').
card_uid('swamp'/'ITP', 'ITP:Swamp:swamp1').
card_rarity('swamp'/'ITP', 'Basic Land').
card_artist('swamp'/'ITP', 'Dan Frazier').

card_in_set('terror', 'ITP').
card_original_type('terror'/'ITP', 'Instant').
card_original_text('terror'/'ITP', 'Bury target non-black, non-artifact creature.').
card_image_name('terror'/'ITP', 'terror').
card_uid('terror'/'ITP', 'ITP:Terror:terror').
card_rarity('terror'/'ITP', 'Special').
card_artist('terror'/'ITP', 'Ron Spencer').

card_in_set('twiddle', 'ITP').
card_original_type('twiddle'/'ITP', 'Instant').
card_original_text('twiddle'/'ITP', 'Tap or untap target artifact, creature, or land.').
card_image_name('twiddle'/'ITP', 'twiddle').
card_uid('twiddle'/'ITP', 'ITP:Twiddle:twiddle').
card_rarity('twiddle'/'ITP', 'Special').
card_artist('twiddle'/'ITP', 'Rob Alexander').

card_in_set('unsummon', 'ITP').
card_original_type('unsummon'/'ITP', 'Instant').
card_original_text('unsummon'/'ITP', 'Return target creature to owner\'s hand.').
card_image_name('unsummon'/'ITP', 'unsummon').
card_uid('unsummon'/'ITP', 'ITP:Unsummon:unsummon').
card_rarity('unsummon'/'ITP', 'Special').
card_artist('unsummon'/'ITP', 'Douglas Shuler').

card_in_set('untamed wilds', 'ITP').
card_original_type('untamed wilds'/'ITP', 'Sorcery').
card_original_text('untamed wilds'/'ITP', 'Search your library for a basic land card and put it into play. Shuffle your library afterwards.').
card_image_name('untamed wilds'/'ITP', 'untamed wilds').
card_uid('untamed wilds'/'ITP', 'ITP:Untamed Wilds:untamed wilds').
card_rarity('untamed wilds'/'ITP', 'Special').
card_artist('untamed wilds'/'ITP', 'NéNé Thomas').

card_in_set('vampire bats', 'ITP').
card_original_type('vampire bats'/'ITP', 'Summon — Bats').
card_original_text('vampire bats'/'ITP', 'Flying\n{B} +1/+0 until end of turn. You cannot spend more than {B}{B} in this way each turn.').
card_image_name('vampire bats'/'ITP', 'vampire bats').
card_uid('vampire bats'/'ITP', 'ITP:Vampire Bats:vampire bats').
card_rarity('vampire bats'/'ITP', 'Special').
card_artist('vampire bats'/'ITP', 'Anson Maddocks').
card_flavor_text('vampire bats'/'ITP', '\"For something is amiss or out of place\nWhen mice with wings can wear a human face.\"\n—Theodore Roethke, \"The Bat\"').

card_in_set('wall of bone', 'ITP').
card_original_type('wall of bone'/'ITP', 'Summon — Wall').
card_original_text('wall of bone'/'ITP', '{B}: Regenerate').
card_image_name('wall of bone'/'ITP', 'wall of bone').
card_uid('wall of bone'/'ITP', 'ITP:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'ITP', 'Special').
card_artist('wall of bone'/'ITP', 'Anson Maddocks').
card_flavor_text('wall of bone'/'ITP', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').

card_in_set('war mammoth', 'ITP').
card_original_type('war mammoth'/'ITP', 'Summon — Mammoth').
card_original_text('war mammoth'/'ITP', 'Trample').
card_image_name('war mammoth'/'ITP', 'war mammoth').
card_uid('war mammoth'/'ITP', 'ITP:War Mammoth:war mammoth').
card_rarity('war mammoth'/'ITP', 'Special').
card_artist('war mammoth'/'ITP', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'ITP', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').

card_in_set('warp artifact', 'ITP').
card_original_type('warp artifact'/'ITP', 'Enchant Artifact').
card_original_text('warp artifact'/'ITP', 'During enchanted artifact\'s controller\'s upkeep, Warp Artifact deals 1 damage to that player.').
card_image_name('warp artifact'/'ITP', 'warp artifact').
card_uid('warp artifact'/'ITP', 'ITP:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'ITP', 'Special').
card_artist('warp artifact'/'ITP', 'Amy Weber').

card_in_set('weakness', 'ITP').
card_original_type('weakness'/'ITP', 'Enchant Creature').
card_original_text('weakness'/'ITP', 'Enchanted creature gets -2/-1.').
card_image_name('weakness'/'ITP', 'weakness').
card_uid('weakness'/'ITP', 'ITP:Weakness:weakness').
card_rarity('weakness'/'ITP', 'Special').
card_artist('weakness'/'ITP', 'Douglas Shuler').

card_in_set('whirling dervish', 'ITP').
card_original_type('whirling dervish'/'ITP', 'Summon — Dervish').
card_original_text('whirling dervish'/'ITP', 'Protection from black\nAt the end of any turn in which Whirling Dervish damaged any opponent, put a +1/+1 counter on it.').
card_image_name('whirling dervish'/'ITP', 'whirling dervish').
card_uid('whirling dervish'/'ITP', 'ITP:Whirling Dervish:whirling dervish').
card_rarity('whirling dervish'/'ITP', 'Special').
card_artist('whirling dervish'/'ITP', 'Susan Van Camp').

card_in_set('winter blast', 'ITP').
card_original_type('winter blast'/'ITP', 'Sorcery').
card_original_text('winter blast'/'ITP', 'Tap X target creatures. Winter Blast deals 2 damage to each of those creatures with flying.').
card_image_name('winter blast'/'ITP', 'winter blast').
card_uid('winter blast'/'ITP', 'ITP:Winter Blast:winter blast').
card_rarity('winter blast'/'ITP', 'Special').
card_artist('winter blast'/'ITP', 'Kaja Foglio').
card_flavor_text('winter blast'/'ITP', '\"Blow, winds, and crack your cheeks! rage! blow!\" —William Shakespeare, King Lear').

card_in_set('zephyr falcon', 'ITP').
card_original_type('zephyr falcon'/'ITP', 'Summon — Falcon').
card_original_text('zephyr falcon'/'ITP', 'Flying\nAttacking does not cause Zephyr Falcon to tap.').
card_image_name('zephyr falcon'/'ITP', 'zephyr falcon').
card_uid('zephyr falcon'/'ITP', 'ITP:Zephyr Falcon:zephyr falcon').
card_rarity('zephyr falcon'/'ITP', 'Special').
card_artist('zephyr falcon'/'ITP', 'Heather Hudson').
card_flavor_text('zephyr falcon'/'ITP', 'Although greatly prized among falconers, the Zephyr Falcon is capricious and not easily tamed.').
