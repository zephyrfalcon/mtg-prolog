% Archenemy

set('ARC').
set_name('ARC', 'Archenemy').
set_release_date('ARC', '2010-06-18').
set_border('ARC', 'black').
set_type('ARC', 'archenemy').

card_in_set('a display of my dark power', 'ARC').
card_original_type('a display of my dark power'/'ARC', 'Scheme').
card_original_text('a display of my dark power'/'ARC', 'When you set this scheme in motion, until your next turn, whenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_first_print('a display of my dark power', 'ARC').
card_image_name('a display of my dark power'/'ARC', 'a display of my dark power').
card_uid('a display of my dark power'/'ARC', 'ARC:A Display of My Dark Power:a display of my dark power').
card_rarity('a display of my dark power'/'ARC', 'Common').
card_artist('a display of my dark power'/'ARC', 'Jim Nelson').
card_number('a display of my dark power'/'ARC', '8').
card_flavor_text('a display of my dark power'/'ARC', '\"What would you say is my greatest attribute? Is it my gluttonous lust for power?\"').
card_multiverse_id('a display of my dark power'/'ARC', '212578').

card_in_set('æther spellbomb', 'ARC').
card_original_type('æther spellbomb'/'ARC', 'Artifact').
card_original_text('æther spellbomb'/'ARC', '{U}, Sacrifice Æther Spellbomb: Return target creature to its owner\'s hand.\n{1}, Sacrifice Æther Spellbomb: Draw a card.').
card_image_name('æther spellbomb'/'ARC', 'aether spellbomb').
card_uid('æther spellbomb'/'ARC', 'ARC:Æther Spellbomb:aether spellbomb').
card_rarity('æther spellbomb'/'ARC', 'Common').
card_artist('æther spellbomb'/'ARC', 'Jim Nelson').
card_number('æther spellbomb'/'ARC', '102').
card_flavor_text('æther spellbomb'/'ARC', '\"Release that which was never caged.\"\n—Spellbomb inscription').
card_multiverse_id('æther spellbomb'/'ARC', '220525').

card_in_set('agony warp', 'ARC').
card_original_type('agony warp'/'ARC', 'Instant').
card_original_text('agony warp'/'ARC', 'Target creature gets -3/-0 until end of turn.\nTarget creature gets -0/-3 until end of turn.').
card_image_name('agony warp'/'ARC', 'agony warp').
card_uid('agony warp'/'ARC', 'ARC:Agony Warp:agony warp').
card_rarity('agony warp'/'ARC', 'Common').
card_artist('agony warp'/'ARC', 'Dave Allsop').
card_number('agony warp'/'ARC', '76').
card_flavor_text('agony warp'/'ARC', 'Life\'s circle has become inverted in Grixis. The same energy is endlessly recycled and becomes more stagnant with each pass.').
card_multiverse_id('agony warp'/'ARC', '220454').

card_in_set('all in good time', 'ARC').
card_original_type('all in good time'/'ARC', 'Scheme').
card_original_text('all in good time'/'ARC', 'When you set this scheme in motion, take an extra turn after this one. Schemes can\'t be set in motion that turn.').
card_first_print('all in good time', 'ARC').
card_image_name('all in good time'/'ARC', 'all in good time').
card_uid('all in good time'/'ARC', 'ARC:All in Good Time:all in good time').
card_rarity('all in good time'/'ARC', 'Common').
card_artist('all in good time'/'ARC', 'Nic Klein').
card_number('all in good time'/'ARC', '1').
card_flavor_text('all in good time'/'ARC', '\"Take a moment. Ponder the depths of your insignificance.\"').
card_multiverse_id('all in good time'/'ARC', '212648').

card_in_set('all shall smolder in my wake', 'ARC').
card_original_type('all shall smolder in my wake'/'ARC', 'Scheme').
card_original_text('all shall smolder in my wake'/'ARC', 'When you set this scheme in motion, destroy up to one target artifact, up to one target enchantment, and up to one target nonbasic land.').
card_first_print('all shall smolder in my wake', 'ARC').
card_image_name('all shall smolder in my wake'/'ARC', 'all shall smolder in my wake').
card_uid('all shall smolder in my wake'/'ARC', 'ARC:All Shall Smolder in My Wake:all shall smolder in my wake').
card_rarity('all shall smolder in my wake'/'ARC', 'Common').
card_artist('all shall smolder in my wake'/'ARC', 'Jim Nelson').
card_number('all shall smolder in my wake'/'ARC', '2').
card_flavor_text('all shall smolder in my wake'/'ARC', '\"It\'s my goal to find the melting point of every substance.\"').
card_multiverse_id('all shall smolder in my wake'/'ARC', '212617').

card_in_set('approach my molten realm', 'ARC').
card_original_type('approach my molten realm'/'ARC', 'Scheme').
card_original_text('approach my molten realm'/'ARC', 'When you set this scheme in motion, until your next turn, if a source would deal damage, it deals double that damage instead.').
card_first_print('approach my molten realm', 'ARC').
card_image_name('approach my molten realm'/'ARC', 'approach my molten realm').
card_uid('approach my molten realm'/'ARC', 'ARC:Approach My Molten Realm:approach my molten realm').
card_rarity('approach my molten realm'/'ARC', 'Common').
card_artist('approach my molten realm'/'ARC', 'Jim Pavelec').
card_number('approach my molten realm'/'ARC', '3').
card_flavor_text('approach my molten realm'/'ARC', '\"It\'s a dragon-infested lavascape of notorious peril. Make yourselves at home.\"').
card_multiverse_id('approach my molten realm'/'ARC', '212591').

card_in_set('architects of will', 'ARC').
card_original_type('architects of will'/'ARC', 'Artifact Creature — Human Wizard').
card_original_text('architects of will'/'ARC', 'When Architects of Will enters the battlefield, look at the top three cards of target player\'s library, then put them back in any order.\nCycling {U/B} ({U/B}, Discard this card: Draw a card.)').
card_image_name('architects of will'/'ARC', 'architects of will').
card_uid('architects of will'/'ARC', 'ARC:Architects of Will:architects of will').
card_rarity('architects of will'/'ARC', 'Common').
card_artist('architects of will'/'ARC', 'Matt Stewart').
card_number('architects of will'/'ARC', '77').
card_flavor_text('architects of will'/'ARC', 'This secret society of mages manipulates the beliefs and opinions of others.').
card_multiverse_id('architects of will'/'ARC', '220517').

card_in_set('armadillo cloak', 'ARC').
card_original_type('armadillo cloak'/'ARC', 'Enchantment — Aura').
card_original_text('armadillo cloak'/'ARC', 'Enchant creature\nEnchanted creature gets +2/+2 and has trample.\nWhenever enchanted creature deals damage, you gain that much life.').
card_image_name('armadillo cloak'/'ARC', 'armadillo cloak').
card_uid('armadillo cloak'/'ARC', 'ARC:Armadillo Cloak:armadillo cloak').
card_rarity('armadillo cloak'/'ARC', 'Common').
card_artist('armadillo cloak'/'ARC', 'Paolo Parente').
card_number('armadillo cloak'/'ARC', '78').
card_multiverse_id('armadillo cloak'/'ARC', '220498').

card_in_set('artisan of kozilek', 'ARC').
card_original_type('artisan of kozilek'/'ARC', 'Creature — Eldrazi').
card_original_text('artisan of kozilek'/'ARC', 'When you cast Artisan of Kozilek, you may return target creature card from your graveyard to the battlefield.\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)').
card_image_name('artisan of kozilek'/'ARC', 'artisan of kozilek').
card_uid('artisan of kozilek'/'ARC', 'ARC:Artisan of Kozilek:artisan of kozilek').
card_rarity('artisan of kozilek'/'ARC', 'Uncommon').
card_artist('artisan of kozilek'/'ARC', 'Jason Felix').
card_number('artisan of kozilek'/'ARC', '123').
card_multiverse_id('artisan of kozilek'/'ARC', '220553').

card_in_set('avatar of discord', 'ARC').
card_original_type('avatar of discord'/'ARC', 'Creature — Avatar').
card_original_text('avatar of discord'/'ARC', 'Flying\nWhen Avatar of Discord enters the battlefield, sacrifice it unless you discard two cards.').
card_image_name('avatar of discord'/'ARC', 'avatar of discord').
card_uid('avatar of discord'/'ARC', 'ARC:Avatar of Discord:avatar of discord').
card_rarity('avatar of discord'/'ARC', 'Rare').
card_artist('avatar of discord'/'ARC', 'rk post').
card_number('avatar of discord'/'ARC', '79').
card_flavor_text('avatar of discord'/'ARC', 'Such is the power of Rakdos that even his shadow takes on a cruel life of its own.').
card_multiverse_id('avatar of discord'/'ARC', '220539').

card_in_set('avatar of woe', 'ARC').
card_original_type('avatar of woe'/'ARC', 'Creature — Avatar').
card_original_text('avatar of woe'/'ARC', 'If there are ten or more creature cards total in all graveyards, Avatar of Woe costs {6} less to cast.\nFear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{T}: Destroy target creature. It can\'t be regenerated.').
card_image_name('avatar of woe'/'ARC', 'avatar of woe').
card_uid('avatar of woe'/'ARC', 'ARC:Avatar of Woe:avatar of woe').
card_rarity('avatar of woe'/'ARC', 'Rare').
card_artist('avatar of woe'/'ARC', 'rk post').
card_number('avatar of woe'/'ARC', '9').
card_multiverse_id('avatar of woe'/'ARC', '220562').

card_in_set('azorius signet', 'ARC').
card_original_type('azorius signet'/'ARC', 'Artifact').
card_original_text('azorius signet'/'ARC', '{1}, {T}: Add {W}{U} to your mana pool.').
card_image_name('azorius signet'/'ARC', 'azorius signet').
card_uid('azorius signet'/'ARC', 'ARC:Azorius Signet:azorius signet').
card_rarity('azorius signet'/'ARC', 'Common').
card_artist('azorius signet'/'ARC', 'Greg Hildebrandt').
card_number('azorius signet'/'ARC', '103').
card_flavor_text('azorius signet'/'ARC', 'The maze-like design embodies the core of Azorius law—strict structure designed to test wills and stall change.').
card_multiverse_id('azorius signet'/'ARC', '220540').

card_in_set('barren moor', 'ARC').
card_original_type('barren moor'/'ARC', 'Land').
card_original_text('barren moor'/'ARC', 'Barren Moor enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'ARC', 'barren moor').
card_uid('barren moor'/'ARC', 'ARC:Barren Moor:barren moor').
card_rarity('barren moor'/'ARC', 'Common').
card_artist('barren moor'/'ARC', 'Heather Hudson').
card_number('barren moor'/'ARC', '124').
card_multiverse_id('barren moor'/'ARC', '220487').

card_in_set('battering craghorn', 'ARC').
card_original_type('battering craghorn'/'ARC', 'Creature — Goat Beast').
card_original_text('battering craghorn'/'ARC', 'First strike\nMorph {1}{R}{R} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('battering craghorn'/'ARC', 'battering craghorn').
card_uid('battering craghorn'/'ARC', 'ARC:Battering Craghorn:battering craghorn').
card_rarity('battering craghorn'/'ARC', 'Common').
card_artist('battering craghorn'/'ARC', 'Matt Cavotta').
card_number('battering craghorn'/'ARC', '30').
card_flavor_text('battering craghorn'/'ARC', 'Their skeletons can be found all over Skirk Ridge, tangled in each other\'s horns.').
card_multiverse_id('battering craghorn'/'ARC', '220488').

card_in_set('batwing brume', 'ARC').
card_original_type('batwing brume'/'ARC', 'Instant').
card_original_text('batwing brume'/'ARC', 'Prevent all combat damage that would be dealt this turn if {W} was spent to cast Batwing Brume. Each player loses 1 life for each attacking creature he or she controls if {B} was spent to cast Batwing Brume. (Do both if {W}{B} was spent.)').
card_image_name('batwing brume'/'ARC', 'batwing brume').
card_uid('batwing brume'/'ARC', 'ARC:Batwing Brume:batwing brume').
card_rarity('batwing brume'/'ARC', 'Uncommon').
card_artist('batwing brume'/'ARC', 'Richard Kane Ferguson').
card_number('batwing brume'/'ARC', '80').
card_multiverse_id('batwing brume'/'ARC', '220573').

card_in_set('beacon of unrest', 'ARC').
card_original_type('beacon of unrest'/'ARC', 'Sorcery').
card_original_text('beacon of unrest'/'ARC', 'Put target artifact or creature card from a graveyard onto the battlefield under your control. Shuffle Beacon of Unrest into its owner\'s library.').
card_image_name('beacon of unrest'/'ARC', 'beacon of unrest').
card_uid('beacon of unrest'/'ARC', 'ARC:Beacon of Unrest:beacon of unrest').
card_rarity('beacon of unrest'/'ARC', 'Rare').
card_artist('beacon of unrest'/'ARC', 'Alan Pollack').
card_number('beacon of unrest'/'ARC', '10').
card_flavor_text('beacon of unrest'/'ARC', 'A vertical scream pierces the night air and echoes doom through the clouds.').
card_multiverse_id('beacon of unrest'/'ARC', '220482').

card_in_set('behold the power of destruction', 'ARC').
card_original_type('behold the power of destruction'/'ARC', 'Scheme').
card_original_text('behold the power of destruction'/'ARC', 'When you set this scheme in motion, destroy all nonland permanents target opponent controls.').
card_first_print('behold the power of destruction', 'ARC').
card_image_name('behold the power of destruction'/'ARC', 'behold the power of destruction').
card_uid('behold the power of destruction'/'ARC', 'ARC:Behold the Power of Destruction:behold the power of destruction').
card_rarity('behold the power of destruction'/'ARC', 'Common').
card_artist('behold the power of destruction'/'ARC', 'Jesper Ejsing').
card_number('behold the power of destruction'/'ARC', '4').
card_flavor_text('behold the power of destruction'/'ARC', '\"I\'d call that a successful first test. Golem! Rearm the Doom Citadel!\"').
card_multiverse_id('behold the power of destruction'/'ARC', '212585').

card_in_set('bituminous blast', 'ARC').
card_original_type('bituminous blast'/'ARC', 'Instant').
card_original_text('bituminous blast'/'ARC', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nBituminous Blast deals 4 damage to target creature.').
card_image_name('bituminous blast'/'ARC', 'bituminous blast').
card_uid('bituminous blast'/'ARC', 'ARC:Bituminous Blast:bituminous blast').
card_rarity('bituminous blast'/'ARC', 'Uncommon').
card_artist('bituminous blast'/'ARC', 'Raymond Swanland').
card_number('bituminous blast'/'ARC', '81').
card_multiverse_id('bituminous blast'/'ARC', '220518').

card_in_set('bog witch', 'ARC').
card_original_type('bog witch'/'ARC', 'Creature — Human Spellshaper').
card_original_text('bog witch'/'ARC', '{B}, {T}, Discard a card: Add {B}{B}{B} to your mana pool.').
card_image_name('bog witch'/'ARC', 'bog witch').
card_uid('bog witch'/'ARC', 'ARC:Bog Witch:bog witch').
card_rarity('bog witch'/'ARC', 'Common').
card_artist('bog witch'/'ARC', 'Gao Yan').
card_number('bog witch'/'ARC', '11').
card_flavor_text('bog witch'/'ARC', 'The world is the body. The mana is the blood. The witch is the surgeon.').
card_multiverse_id('bog witch'/'ARC', '220556').

card_in_set('branching bolt', 'ARC').
card_original_type('branching bolt'/'ARC', 'Instant').
card_original_text('branching bolt'/'ARC', 'Choose one or both — Branching Bolt deals 3 damage to target creature with flying; and/or Branching Bolt deals 3 damage to target creature without flying.').
card_image_name('branching bolt'/'ARC', 'branching bolt').
card_uid('branching bolt'/'ARC', 'ARC:Branching Bolt:branching bolt').
card_rarity('branching bolt'/'ARC', 'Common').
card_artist('branching bolt'/'ARC', 'Vance Kovacs').
card_number('branching bolt'/'ARC', '82').
card_flavor_text('branching bolt'/'ARC', '\"Lightning lives in everything, in living flesh and growing things. It must be set free.\"\n—Rakka Mar').
card_multiverse_id('branching bolt'/'ARC', '220455').

card_in_set('breath of darigaaz', 'ARC').
card_original_type('breath of darigaaz'/'ARC', 'Sorcery').
card_original_text('breath of darigaaz'/'ARC', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nBreath of Darigaaz deals 1 damage to each creature without flying and each player. If Breath of Darigaaz was kicked, it deals 4 damage to each creature without flying and each player instead.').
card_image_name('breath of darigaaz'/'ARC', 'breath of darigaaz').
card_uid('breath of darigaaz'/'ARC', 'ARC:Breath of Darigaaz:breath of darigaaz').
card_rarity('breath of darigaaz'/'ARC', 'Uncommon').
card_artist('breath of darigaaz'/'ARC', 'Greg & Tim Hildebrandt').
card_number('breath of darigaaz'/'ARC', '31').
card_multiverse_id('breath of darigaaz'/'ARC', '220499').

card_in_set('cemetery reaper', 'ARC').
card_original_type('cemetery reaper'/'ARC', 'Creature — Zombie').
card_original_text('cemetery reaper'/'ARC', 'Other Zombie creatures you control get +1/+1.\n{2}{B}, {T}: Exile target creature card from a graveyard. Put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('cemetery reaper'/'ARC', 'cemetery reaper').
card_uid('cemetery reaper'/'ARC', 'ARC:Cemetery Reaper:cemetery reaper').
card_rarity('cemetery reaper'/'ARC', 'Rare').
card_artist('cemetery reaper'/'ARC', 'Dave Allsop').
card_number('cemetery reaper'/'ARC', '12').
card_flavor_text('cemetery reaper'/'ARC', 'Every grave cradles a recruit.').
card_multiverse_id('cemetery reaper'/'ARC', '220476').

card_in_set('chameleon colossus', 'ARC').
card_original_type('chameleon colossus'/'ARC', 'Creature — Shapeshifter').
card_original_text('chameleon colossus'/'ARC', 'Changeling (This card is every creature type at all times.)\nProtection from black\n{2}{G}{G}: Chameleon Colossus gets +X/+X until end of turn, where X is its power.').
card_image_name('chameleon colossus'/'ARC', 'chameleon colossus').
card_uid('chameleon colossus'/'ARC', 'ARC:Chameleon Colossus:chameleon colossus').
card_rarity('chameleon colossus'/'ARC', 'Rare').
card_artist('chameleon colossus'/'ARC', 'Darrell Riche').
card_number('chameleon colossus'/'ARC', '52').
card_multiverse_id('chameleon colossus'/'ARC', '220451').

card_in_set('chandra\'s outrage', 'ARC').
card_original_type('chandra\'s outrage'/'ARC', 'Instant').
card_original_text('chandra\'s outrage'/'ARC', 'Chandra\'s Outrage deals 4 damage to target creature and 2 damage to that creature\'s controller.').
card_first_print('chandra\'s outrage', 'ARC').
card_image_name('chandra\'s outrage'/'ARC', 'chandra\'s outrage').
card_uid('chandra\'s outrage'/'ARC', 'ARC:Chandra\'s Outrage:chandra\'s outrage').
card_rarity('chandra\'s outrage'/'ARC', 'Common').
card_artist('chandra\'s outrage'/'ARC', 'Christopher Moeller').
card_number('chandra\'s outrage'/'ARC', '32').
card_flavor_text('chandra\'s outrage'/'ARC', '\"Her mind is an incredible mix of emotion and power. Even if I could grasp it, I couldn\'t hold it for long.\"\n—Jace Beleren, on Chandra Nalaar').
card_multiverse_id('chandra\'s outrage'/'ARC', '220513').

card_in_set('choose your champion', 'ARC').
card_original_type('choose your champion'/'ARC', 'Scheme').
card_original_text('choose your champion'/'ARC', 'When you set this scheme in motion, target opponent chooses a player. Until your next turn, only you and the chosen player can cast spells and attack with creatures.').
card_first_print('choose your champion', 'ARC').
card_image_name('choose your champion'/'ARC', 'choose your champion').
card_uid('choose your champion'/'ARC', 'ARC:Choose Your Champion:choose your champion').
card_rarity('choose your champion'/'ARC', 'Common').
card_artist('choose your champion'/'ARC', 'Jim Nelson').
card_number('choose your champion'/'ARC', '5').
card_flavor_text('choose your champion'/'ARC', '\"It seems the fate of the world rests on your shoulders, feeble child.\"').
card_multiverse_id('choose your champion'/'ARC', '212581').

card_in_set('colossal might', 'ARC').
card_original_type('colossal might'/'ARC', 'Instant').
card_original_text('colossal might'/'ARC', 'Target creature gets +4/+2 and gains trample until end of turn.').
card_image_name('colossal might'/'ARC', 'colossal might').
card_uid('colossal might'/'ARC', 'ARC:Colossal Might:colossal might').
card_rarity('colossal might'/'ARC', 'Common').
card_artist('colossal might'/'ARC', 'Justin Sweet').
card_number('colossal might'/'ARC', '83').
card_flavor_text('colossal might'/'ARC', '\"Never corner a barbarian. You become the path of least resistance.\"\n—Sarkhan Vol').
card_multiverse_id('colossal might'/'ARC', '220519').

card_in_set('corpse connoisseur', 'ARC').
card_original_type('corpse connoisseur'/'ARC', 'Creature — Zombie Wizard').
card_original_text('corpse connoisseur'/'ARC', 'When Corpse Connoisseur enters the battlefield, you may search your library for a creature card and put that card into your graveyard. If you do, shuffle your library.\nUnearth {3}{B} ({3}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('corpse connoisseur'/'ARC', 'corpse connoisseur').
card_uid('corpse connoisseur'/'ARC', 'ARC:Corpse Connoisseur:corpse connoisseur').
card_rarity('corpse connoisseur'/'ARC', 'Uncommon').
card_artist('corpse connoisseur'/'ARC', 'Mark Hyzer').
card_number('corpse connoisseur'/'ARC', '13').
card_multiverse_id('corpse connoisseur'/'ARC', '220567').

card_in_set('dance, pathetic marionette', 'ARC').
card_original_type('dance, pathetic marionette'/'ARC', 'Scheme').
card_original_text('dance, pathetic marionette'/'ARC', 'When you set this scheme in motion, each opponent reveals cards from the top of his or her library until he or she reveals a creature card. Choose one of the revealed creature cards and put it onto the battlefield under your control. Put all other cards revealed this way into their owners\' graveyards.').
card_first_print('dance, pathetic marionette', 'ARC').
card_image_name('dance, pathetic marionette'/'ARC', 'dance, pathetic marionette').
card_uid('dance, pathetic marionette'/'ARC', 'ARC:Dance, Pathetic Marionette:dance, pathetic marionette').
card_rarity('dance, pathetic marionette'/'ARC', 'Common').
card_artist('dance, pathetic marionette'/'ARC', 'Steve Prescott').
card_number('dance, pathetic marionette'/'ARC', '6').
card_multiverse_id('dance, pathetic marionette'/'ARC', '212610').

card_in_set('dimir signet', 'ARC').
card_original_type('dimir signet'/'ARC', 'Artifact').
card_original_text('dimir signet'/'ARC', '{1}, {T}: Add {U}{B} to your mana pool.').
card_image_name('dimir signet'/'ARC', 'dimir signet').
card_uid('dimir signet'/'ARC', 'ARC:Dimir Signet:dimir signet').
card_rarity('dimir signet'/'ARC', 'Common').
card_artist('dimir signet'/'ARC', 'Greg Hildebrandt').
card_number('dimir signet'/'ARC', '104').
card_flavor_text('dimir signet'/'ARC', 'An emblem of a secret guild, the Dimir insignia is seen only by its own members—and the doomed.').
card_multiverse_id('dimir signet'/'ARC', '220548').

card_in_set('dragon breath', 'ARC').
card_original_type('dragon breath'/'ARC', 'Enchantment — Aura').
card_original_text('dragon breath'/'ARC', 'Enchant creature\nEnchanted creature has haste.\n{R}: Enchanted creature gets +1/+0 until end of turn.\nWhen a creature with converted mana cost 6 or more enters the battlefield, you may return Dragon Breath from your graveyard to the battlefield attached to that creature.').
card_image_name('dragon breath'/'ARC', 'dragon breath').
card_uid('dragon breath'/'ARC', 'ARC:Dragon Breath:dragon breath').
card_rarity('dragon breath'/'ARC', 'Common').
card_artist('dragon breath'/'ARC', 'Greg Staples').
card_number('dragon breath'/'ARC', '33').
card_multiverse_id('dragon breath'/'ARC', '220473').

card_in_set('dragon fodder', 'ARC').
card_original_type('dragon fodder'/'ARC', 'Sorcery').
card_original_text('dragon fodder'/'ARC', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('dragon fodder'/'ARC', 'dragon fodder').
card_uid('dragon fodder'/'ARC', 'ARC:Dragon Fodder:dragon fodder').
card_rarity('dragon fodder'/'ARC', 'Common').
card_artist('dragon fodder'/'ARC', 'Jaime Jones').
card_number('dragon fodder'/'ARC', '34').
card_flavor_text('dragon fodder'/'ARC', 'Goblins journey to the sacrificial peaks in pairs so that the rare survivor might be able to relate the details of the other\'s grisly demise.').
card_multiverse_id('dragon fodder'/'ARC', '220456').

card_in_set('dragon whelp', 'ARC').
card_original_type('dragon whelp'/'ARC', 'Creature — Dragon').
card_original_text('dragon whelp'/'ARC', 'Flying\n{R}: Dragon Whelp gets +1/+0 until end of turn. If this ability has been activated four or more times this turn, sacrifice Dragon Whelp at the beginning of the next end step.').
card_image_name('dragon whelp'/'ARC', 'dragon whelp').
card_uid('dragon whelp'/'ARC', 'ARC:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'ARC', 'Uncommon').
card_artist('dragon whelp'/'ARC', 'Steven Belledin').
card_number('dragon whelp'/'ARC', '35').
card_multiverse_id('dragon whelp'/'ARC', '220477').

card_in_set('dragonspeaker shaman', 'ARC').
card_original_type('dragonspeaker shaman'/'ARC', 'Creature — Human Barbarian Shaman').
card_original_text('dragonspeaker shaman'/'ARC', 'Dragon spells you cast cost {2} less to cast.').
card_image_name('dragonspeaker shaman'/'ARC', 'dragonspeaker shaman').
card_uid('dragonspeaker shaman'/'ARC', 'ARC:Dragonspeaker Shaman:dragonspeaker shaman').
card_rarity('dragonspeaker shaman'/'ARC', 'Uncommon').
card_artist('dragonspeaker shaman'/'ARC', 'Kev Walker').
card_number('dragonspeaker shaman'/'ARC', '36').
card_flavor_text('dragonspeaker shaman'/'ARC', '\"We speak the dragons\' language of flame and rage. They speak our language of fury and honor. Together we shall weave a tale of destruction without equal.\"').
card_multiverse_id('dragonspeaker shaman'/'ARC', '220474').

card_in_set('dreamstone hedron', 'ARC').
card_original_type('dreamstone hedron'/'ARC', 'Artifact').
card_original_text('dreamstone hedron'/'ARC', '{T}: Add {3} to your mana pool.\n{3}, {T}, Sacrifice Dreamstone Hedron: Draw three cards.').
card_image_name('dreamstone hedron'/'ARC', 'dreamstone hedron').
card_uid('dreamstone hedron'/'ARC', 'ARC:Dreamstone Hedron:dreamstone hedron').
card_rarity('dreamstone hedron'/'ARC', 'Uncommon').
card_artist('dreamstone hedron'/'ARC', 'Eric Deschamps').
card_number('dreamstone hedron'/'ARC', '105').
card_flavor_text('dreamstone hedron'/'ARC', 'Only the Eldrazi mind thinks in the warped paths required to open the hedrons and tap the power within.').
card_multiverse_id('dreamstone hedron'/'ARC', '220554').

card_in_set('dregscape zombie', 'ARC').
card_original_type('dregscape zombie'/'ARC', 'Creature — Zombie').
card_original_text('dregscape zombie'/'ARC', 'Unearth {B} ({B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('dregscape zombie'/'ARC', 'dregscape zombie').
card_uid('dregscape zombie'/'ARC', 'ARC:Dregscape Zombie:dregscape zombie').
card_rarity('dregscape zombie'/'ARC', 'Common').
card_artist('dregscape zombie'/'ARC', 'Lars Grant-West').
card_number('dregscape zombie'/'ARC', '14').
card_flavor_text('dregscape zombie'/'ARC', 'The undead of Grixis are fueled by their hatred of the living.').
card_multiverse_id('dregscape zombie'/'ARC', '220457').

card_in_set('duplicant', 'ARC').
card_original_type('duplicant'/'ARC', 'Artifact Creature — Shapeshifter').
card_original_text('duplicant'/'ARC', 'Imprint — When Duplicant enters the battlefield, you may exile target nontoken creature. \nAs long as the exiled card is a creature card, Duplicant has that card\'s power, toughness, and creature types. It\'s still a Shapeshifter.').
card_image_name('duplicant'/'ARC', 'duplicant').
card_uid('duplicant'/'ARC', 'ARC:Duplicant:duplicant').
card_rarity('duplicant'/'ARC', 'Rare').
card_artist('duplicant'/'ARC', 'Thomas M. Baxa').
card_number('duplicant'/'ARC', '106').
card_multiverse_id('duplicant'/'ARC', '220526').

card_in_set('embrace my diabolical vision', 'ARC').
card_original_type('embrace my diabolical vision'/'ARC', 'Scheme').
card_original_text('embrace my diabolical vision'/'ARC', 'When you set this scheme in motion, each player shuffles his or her hand and graveyard into his or her library. You draw seven cards, then each other player draws four cards.').
card_first_print('embrace my diabolical vision', 'ARC').
card_image_name('embrace my diabolical vision'/'ARC', 'embrace my diabolical vision').
card_uid('embrace my diabolical vision'/'ARC', 'ARC:Embrace My Diabolical Vision:embrace my diabolical vision').
card_rarity('embrace my diabolical vision'/'ARC', 'Common').
card_artist('embrace my diabolical vision'/'ARC', 'Franz Vohwinkel').
card_number('embrace my diabolical vision'/'ARC', '9').
card_flavor_text('embrace my diabolical vision'/'ARC', '\"My vision is for the good of everyone. Mostly me.\"').
card_multiverse_id('embrace my diabolical vision'/'ARC', '212596').

card_in_set('ethersworn shieldmage', 'ARC').
card_original_type('ethersworn shieldmage'/'ARC', 'Artifact Creature — Vedalken Wizard').
card_original_text('ethersworn shieldmage'/'ARC', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Ethersworn Shieldmage enters the battlefield, prevent all damage that would be dealt to artifact creatures this turn.').
card_image_name('ethersworn shieldmage'/'ARC', 'ethersworn shieldmage').
card_uid('ethersworn shieldmage'/'ARC', 'ARC:Ethersworn Shieldmage:ethersworn shieldmage').
card_rarity('ethersworn shieldmage'/'ARC', 'Common').
card_artist('ethersworn shieldmage'/'ARC', 'Daarken').
card_number('ethersworn shieldmage'/'ARC', '84').
card_multiverse_id('ethersworn shieldmage'/'ARC', '220520').

card_in_set('everflowing chalice', 'ARC').
card_original_type('everflowing chalice'/'ARC', 'Artifact').
card_original_text('everflowing chalice'/'ARC', 'Multikicker {2} (You may pay an additional {2} any number of times as you cast this spell.)\nEverflowing Chalice enters the battlefield with a charge counter on it for each time it was kicked.\n{T}: Add {1} to your mana pool for each charge counter on Everflowing Chalice.').
card_image_name('everflowing chalice'/'ARC', 'everflowing chalice').
card_uid('everflowing chalice'/'ARC', 'ARC:Everflowing Chalice:everflowing chalice').
card_rarity('everflowing chalice'/'ARC', 'Uncommon').
card_artist('everflowing chalice'/'ARC', 'Steve Argyle').
card_number('everflowing chalice'/'ARC', '107').
card_multiverse_id('everflowing chalice'/'ARC', '220534').

card_in_set('every hope shall vanish', 'ARC').
card_original_type('every hope shall vanish'/'ARC', 'Scheme').
card_original_text('every hope shall vanish'/'ARC', 'When you set this scheme in motion, each opponent reveals his or her hand. Choose a nonland card from each of those hands. Those players discard those cards.').
card_first_print('every hope shall vanish', 'ARC').
card_image_name('every hope shall vanish'/'ARC', 'every hope shall vanish').
card_uid('every hope shall vanish'/'ARC', 'ARC:Every Hope Shall Vanish:every hope shall vanish').
card_rarity('every hope shall vanish'/'ARC', 'Common').
card_artist('every hope shall vanish'/'ARC', 'John Stanko').
card_number('every hope shall vanish'/'ARC', '10').
card_flavor_text('every hope shall vanish'/'ARC', '\"Oh, I think I will get away with this.\"').
card_multiverse_id('every hope shall vanish'/'ARC', '212593').

card_in_set('every last vestige shall rot', 'ARC').
card_original_type('every last vestige shall rot'/'ARC', 'Scheme').
card_original_text('every last vestige shall rot'/'ARC', 'When you set this scheme in motion, you may pay {X}. If you do, put each nonland permanent target player controls with converted mana cost X or less on the bottom of its owner\'s library.').
card_first_print('every last vestige shall rot', 'ARC').
card_image_name('every last vestige shall rot'/'ARC', 'every last vestige shall rot').
card_uid('every last vestige shall rot'/'ARC', 'ARC:Every Last Vestige Shall Rot:every last vestige shall rot').
card_rarity('every last vestige shall rot'/'ARC', 'Common').
card_artist('every last vestige shall rot'/'ARC', 'Martina Pilcerova').
card_number('every last vestige shall rot'/'ARC', '11').
card_flavor_text('every last vestige shall rot'/'ARC', '\"May bloodberries grow from your remains.\"').
card_multiverse_id('every last vestige shall rot'/'ARC', '212655').

card_in_set('evil comes to fruition', 'ARC').
card_original_type('evil comes to fruition'/'ARC', 'Scheme').
card_original_text('evil comes to fruition'/'ARC', 'When you set this scheme in motion, put seven 0/1 green Plant creature tokens onto the battlefield. If you control ten or more lands, put seven 3/3 green Elemental creature tokens onto the battlefield instead.').
card_first_print('evil comes to fruition', 'ARC').
card_image_name('evil comes to fruition'/'ARC', 'evil comes to fruition').
card_uid('evil comes to fruition'/'ARC', 'ARC:Evil Comes to Fruition:evil comes to fruition').
card_rarity('evil comes to fruition'/'ARC', 'Common').
card_artist('evil comes to fruition'/'ARC', 'Austin Hsu').
card_number('evil comes to fruition'/'ARC', '12').
card_flavor_text('evil comes to fruition'/'ARC', '\"The plants justify the means.\"').
card_multiverse_id('evil comes to fruition'/'ARC', '212599').

card_in_set('extractor demon', 'ARC').
card_original_type('extractor demon'/'ARC', 'Creature — Demon').
card_original_text('extractor demon'/'ARC', 'Flying\nWhenever another creature leaves the battlefield, you may have target player put the top two cards of his or her library into his or her graveyard.\nUnearth {2}{B} ({2}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_image_name('extractor demon'/'ARC', 'extractor demon').
card_uid('extractor demon'/'ARC', 'ARC:Extractor Demon:extractor demon').
card_rarity('extractor demon'/'ARC', 'Rare').
card_artist('extractor demon'/'ARC', 'Carl Critchlow').
card_number('extractor demon'/'ARC', '15').
card_multiverse_id('extractor demon'/'ARC', '220507').

card_in_set('feed the machine', 'ARC').
card_original_type('feed the machine'/'ARC', 'Scheme').
card_original_text('feed the machine'/'ARC', 'When you set this scheme in motion, target opponent chooses self or others. If that player chooses self, he or she sacrifices two creatures. If the player chooses others, each of your other opponents sacrifices a creature.').
card_first_print('feed the machine', 'ARC').
card_image_name('feed the machine'/'ARC', 'feed the machine').
card_uid('feed the machine'/'ARC', 'ARC:Feed the Machine:feed the machine').
card_rarity('feed the machine'/'ARC', 'Common').
card_artist('feed the machine'/'ARC', 'Wayne Reynolds').
card_number('feed the machine'/'ARC', '14').
card_flavor_text('feed the machine'/'ARC', '\"Even you have a purpose. Your blood will oil the gears.\"').
card_multiverse_id('feed the machine'/'ARC', '212652').

card_in_set('feral hydra', 'ARC').
card_original_type('feral hydra'/'ARC', 'Creature — Hydra Beast').
card_original_text('feral hydra'/'ARC', 'Feral Hydra enters the battlefield with X +1/+1 counters on it.\n{3}: Put a +1/+1 counter on Feral Hydra. Any player may activate this ability.').
card_image_name('feral hydra'/'ARC', 'feral hydra').
card_uid('feral hydra'/'ARC', 'ARC:Feral Hydra:feral hydra').
card_rarity('feral hydra'/'ARC', 'Rare').
card_artist('feral hydra'/'ARC', 'Steve Prescott').
card_number('feral hydra'/'ARC', '53').
card_flavor_text('feral hydra'/'ARC', 'It shreds its prey as each head fights for the choicest bits.').
card_multiverse_id('feral hydra'/'ARC', '220684').

card_in_set('fertilid', 'ARC').
card_original_type('fertilid'/'ARC', 'Creature — Elemental').
card_original_text('fertilid'/'ARC', 'Fertilid enters the battlefield with two +1/+1 counters on it.\n{1}{G}, Remove a +1/+1 counter from Fertilid: Target player searches his or her library for a basic land card and puts it onto the battlefield tapped. Then that player shuffles his or her library.').
card_image_name('fertilid'/'ARC', 'fertilid').
card_uid('fertilid'/'ARC', 'ARC:Fertilid:fertilid').
card_rarity('fertilid'/'ARC', 'Common').
card_artist('fertilid'/'ARC', 'Wayne Reynolds').
card_number('fertilid'/'ARC', '54').
card_multiverse_id('fertilid'/'ARC', '220452').

card_in_set('festering goblin', 'ARC').
card_original_type('festering goblin'/'ARC', 'Creature — Zombie Goblin').
card_original_text('festering goblin'/'ARC', 'When Festering Goblin is put into a graveyard from the battlefield, target creature gets -1/-1 until end of turn.').
card_image_name('festering goblin'/'ARC', 'festering goblin').
card_uid('festering goblin'/'ARC', 'ARC:Festering Goblin:festering goblin').
card_rarity('festering goblin'/'ARC', 'Common').
card_artist('festering goblin'/'ARC', 'Thomas M. Baxa').
card_number('festering goblin'/'ARC', '16').
card_flavor_text('festering goblin'/'ARC', 'In life, it was a fetid, disease-ridden thing. In death, not much changed.').
card_multiverse_id('festering goblin'/'ARC', '220489').

card_in_set('fieldmist borderpost', 'ARC').
card_original_type('fieldmist borderpost'/'ARC', 'Artifact').
card_original_text('fieldmist borderpost'/'ARC', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Fieldmist Borderpost\'s mana cost.\nFieldmist Borderpost enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('fieldmist borderpost'/'ARC', 'fieldmist borderpost').
card_uid('fieldmist borderpost'/'ARC', 'ARC:Fieldmist Borderpost:fieldmist borderpost').
card_rarity('fieldmist borderpost'/'ARC', 'Common').
card_artist('fieldmist borderpost'/'ARC', 'Michael Bruinsma').
card_number('fieldmist borderpost'/'ARC', '85').
card_multiverse_id('fieldmist borderpost'/'ARC', '220521').

card_in_set('fierce empath', 'ARC').
card_original_type('fierce empath'/'ARC', 'Creature — Elf').
card_original_text('fierce empath'/'ARC', 'When Fierce Empath enters the battlefield, you may search your library for a creature card with converted mana cost 6 or more, reveal it, put it into your hand, then shuffle your library.').
card_image_name('fierce empath'/'ARC', 'fierce empath').
card_uid('fierce empath'/'ARC', 'ARC:Fierce Empath:fierce empath').
card_rarity('fierce empath'/'ARC', 'Common').
card_artist('fierce empath'/'ARC', 'Alan Pollack').
card_number('fierce empath'/'ARC', '55').
card_multiverse_id('fierce empath'/'ARC', '220475').

card_in_set('fireball', 'ARC').
card_original_type('fireball'/'ARC', 'Sorcery').
card_original_text('fireball'/'ARC', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'ARC', 'fireball').
card_uid('fireball'/'ARC', 'ARC:Fireball:fireball').
card_rarity('fireball'/'ARC', 'Uncommon').
card_artist('fireball'/'ARC', 'Dave Dorman').
card_number('fireball'/'ARC', '37').
card_multiverse_id('fireball'/'ARC', '220478').

card_in_set('fires of yavimaya', 'ARC').
card_original_type('fires of yavimaya'/'ARC', 'Enchantment').
card_original_text('fires of yavimaya'/'ARC', 'Creatures you control have haste.\nSacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_image_name('fires of yavimaya'/'ARC', 'fires of yavimaya').
card_uid('fires of yavimaya'/'ARC', 'ARC:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'ARC', 'Uncommon').
card_artist('fires of yavimaya'/'ARC', 'Val Mayerik').
card_number('fires of yavimaya'/'ARC', '86').
card_multiverse_id('fires of yavimaya'/'ARC', '220500').

card_in_set('flameblast dragon', 'ARC').
card_original_type('flameblast dragon'/'ARC', 'Creature — Dragon').
card_original_text('flameblast dragon'/'ARC', 'Flying\nWhenever Flameblast Dragon attacks, you may pay {X}{R}. If you do, Flameblast Dragon deals X damage to target creature or player.').
card_image_name('flameblast dragon'/'ARC', 'flameblast dragon').
card_uid('flameblast dragon'/'ARC', 'ARC:Flameblast Dragon:flameblast dragon').
card_rarity('flameblast dragon'/'ARC', 'Rare').
card_artist('flameblast dragon'/'ARC', 'Jaime Jones').
card_number('flameblast dragon'/'ARC', '38').
card_flavor_text('flameblast dragon'/'ARC', 'Dragon lords of Jund rule by edicts of flame.').
card_multiverse_id('flameblast dragon'/'ARC', '220458').

card_in_set('fog', 'ARC').
card_original_type('fog'/'ARC', 'Instant').
card_original_text('fog'/'ARC', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'ARC', 'fog').
card_uid('fog'/'ARC', 'ARC:Fog:fog').
card_rarity('fog'/'ARC', 'Common').
card_artist('fog'/'ARC', 'John Avon').
card_number('fog'/'ARC', '56').
card_flavor_text('fog'/'ARC', '\"I fear no army or beast, but only the morning fog. Our assault can survive everything else.\"\n—Lord Hilneth').
card_multiverse_id('fog'/'ARC', '220479').

card_in_set('forest', 'ARC').
card_original_type('forest'/'ARC', 'Basic Land — Forest').
card_original_text('forest'/'ARC', 'G').
card_image_name('forest'/'ARC', 'forest1').
card_uid('forest'/'ARC', 'ARC:Forest:forest1').
card_rarity('forest'/'ARC', 'Basic Land').
card_artist('forest'/'ARC', 'Quinton Hoover').
card_number('forest'/'ARC', '148').
card_multiverse_id('forest'/'ARC', '221297').

card_in_set('forest', 'ARC').
card_original_type('forest'/'ARC', 'Basic Land — Forest').
card_original_text('forest'/'ARC', 'G').
card_image_name('forest'/'ARC', 'forest2').
card_uid('forest'/'ARC', 'ARC:Forest:forest2').
card_rarity('forest'/'ARC', 'Basic Land').
card_artist('forest'/'ARC', 'Quinton Hoover').
card_number('forest'/'ARC', '149').
card_multiverse_id('forest'/'ARC', '221296').

card_in_set('forest', 'ARC').
card_original_type('forest'/'ARC', 'Basic Land — Forest').
card_original_text('forest'/'ARC', 'G').
card_image_name('forest'/'ARC', 'forest3').
card_uid('forest'/'ARC', 'ARC:Forest:forest3').
card_rarity('forest'/'ARC', 'Basic Land').
card_artist('forest'/'ARC', 'Quinton Hoover').
card_number('forest'/'ARC', '150').
card_multiverse_id('forest'/'ARC', '221295').

card_in_set('forgotten ancient', 'ARC').
card_original_type('forgotten ancient'/'ARC', 'Creature — Elemental').
card_original_text('forgotten ancient'/'ARC', 'Whenever a player casts a spell, you may put a +1/+1 counter on Forgotten Ancient.\nAt the beginning of your upkeep, you may move any number of +1/+1 counters from Forgotten Ancient onto other creatures.').
card_image_name('forgotten ancient'/'ARC', 'forgotten ancient').
card_uid('forgotten ancient'/'ARC', 'ARC:Forgotten Ancient:forgotten ancient').
card_rarity('forgotten ancient'/'ARC', 'Rare').
card_artist('forgotten ancient'/'ARC', 'Mark Tedin').
card_number('forgotten ancient'/'ARC', '57').
card_flavor_text('forgotten ancient'/'ARC', 'Its blood is life. Its body is growth.').
card_multiverse_id('forgotten ancient'/'ARC', '220583').

card_in_set('furnace whelp', 'ARC').
card_original_type('furnace whelp'/'ARC', 'Creature — Dragon').
card_original_text('furnace whelp'/'ARC', 'Flying\n{R}: Furnace Whelp gets +1/+0 until end of turn.').
card_image_name('furnace whelp'/'ARC', 'furnace whelp').
card_uid('furnace whelp'/'ARC', 'ARC:Furnace Whelp:furnace whelp').
card_rarity('furnace whelp'/'ARC', 'Uncommon').
card_artist('furnace whelp'/'ARC', 'Matt Cavotta').
card_number('furnace whelp'/'ARC', '39').
card_flavor_text('furnace whelp'/'ARC', 'Baby dragons can\'t figure out humans—if they didn\'t want to be killed, why were they made of meat and treasure?').
card_multiverse_id('furnace whelp'/'ARC', '220483').

card_in_set('gathan raiders', 'ARC').
card_original_type('gathan raiders'/'ARC', 'Creature — Human Warrior').
card_original_text('gathan raiders'/'ARC', 'Hellbent — Gathan Raiders gets +2/+2 if you have no cards in hand.\nMorph—Discard a card. (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('gathan raiders'/'ARC', 'gathan raiders').
card_uid('gathan raiders'/'ARC', 'ARC:Gathan Raiders:gathan raiders').
card_rarity('gathan raiders'/'ARC', 'Common').
card_artist('gathan raiders'/'ARC', 'Paolo Parente').
card_number('gathan raiders'/'ARC', '40').
card_multiverse_id('gathan raiders'/'ARC', '220495').

card_in_set('gleeful sabotage', 'ARC').
card_original_type('gleeful sabotage'/'ARC', 'Sorcery').
card_original_text('gleeful sabotage'/'ARC', 'Destroy target artifact or enchantment.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_image_name('gleeful sabotage'/'ARC', 'gleeful sabotage').
card_uid('gleeful sabotage'/'ARC', 'ARC:Gleeful Sabotage:gleeful sabotage').
card_rarity('gleeful sabotage'/'ARC', 'Common').
card_artist('gleeful sabotage'/'ARC', 'Todd Lockwood').
card_number('gleeful sabotage'/'ARC', '58').
card_multiverse_id('gleeful sabotage'/'ARC', '220585').

card_in_set('graypelt refuge', 'ARC').
card_original_type('graypelt refuge'/'ARC', 'Land').
card_original_text('graypelt refuge'/'ARC', 'Graypelt Refuge enters the battlefield tapped.\nWhen Graypelt Refuge enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('graypelt refuge'/'ARC', 'graypelt refuge').
card_uid('graypelt refuge'/'ARC', 'ARC:Graypelt Refuge:graypelt refuge').
card_rarity('graypelt refuge'/'ARC', 'Uncommon').
card_artist('graypelt refuge'/'ARC', 'Philip Straub').
card_number('graypelt refuge'/'ARC', '125').
card_multiverse_id('graypelt refuge'/'ARC', '220467').

card_in_set('gruul signet', 'ARC').
card_original_type('gruul signet'/'ARC', 'Artifact').
card_original_text('gruul signet'/'ARC', '{1}, {T}: Add {R}{G} to your mana pool.').
card_image_name('gruul signet'/'ARC', 'gruul signet').
card_uid('gruul signet'/'ARC', 'ARC:Gruul Signet:gruul signet').
card_rarity('gruul signet'/'ARC', 'Common').
card_artist('gruul signet'/'ARC', 'Greg Hildebrandt').
card_number('gruul signet'/'ARC', '108').
card_flavor_text('gruul signet'/'ARC', 'Gruul territorial markings need not be legible. The blood, snot, and muck used to smear them are unmistakably Gruul.').
card_multiverse_id('gruul signet'/'ARC', '220504').

card_in_set('harmonize', 'ARC').
card_original_type('harmonize'/'ARC', 'Sorcery').
card_original_text('harmonize'/'ARC', 'Draw three cards.').
card_image_name('harmonize'/'ARC', 'harmonize').
card_uid('harmonize'/'ARC', 'ARC:Harmonize:harmonize').
card_rarity('harmonize'/'ARC', 'Uncommon').
card_artist('harmonize'/'ARC', 'Paul Lee').
card_number('harmonize'/'ARC', '59').
card_flavor_text('harmonize'/'ARC', '\"Words lie. People lie. The land tells the truth.\"').
card_multiverse_id('harmonize'/'ARC', '220584').

card_in_set('hellkite charger', 'ARC').
card_original_type('hellkite charger'/'ARC', 'Creature — Dragon').
card_original_text('hellkite charger'/'ARC', 'Flying, haste\nWhenever Hellkite Charger attacks, you may pay {5}{R}{R}. If you do, untap all attacking creatures and after this phase, there is an additional combat phase.').
card_image_name('hellkite charger'/'ARC', 'hellkite charger').
card_uid('hellkite charger'/'ARC', 'ARC:Hellkite Charger:hellkite charger').
card_rarity('hellkite charger'/'ARC', 'Rare').
card_artist('hellkite charger'/'ARC', 'Jaime Jones').
card_number('hellkite charger'/'ARC', '41').
card_multiverse_id('hellkite charger'/'ARC', '220468').

card_in_set('heroes\' reunion', 'ARC').
card_original_type('heroes\' reunion'/'ARC', 'Instant').
card_original_text('heroes\' reunion'/'ARC', 'Target player gains 7 life.').
card_image_name('heroes\' reunion'/'ARC', 'heroes\' reunion').
card_uid('heroes\' reunion'/'ARC', 'ARC:Heroes\' Reunion:heroes\' reunion').
card_rarity('heroes\' reunion'/'ARC', 'Uncommon').
card_artist('heroes\' reunion'/'ARC', 'Terese Nielsen').
card_number('heroes\' reunion'/'ARC', '87').
card_flavor_text('heroes\' reunion'/'ARC', '\"You helped save my people from a Phyrexian fate. Did you think I wouldn\'t return the favor?\"\n—Eladamri, to Gerrard').
card_multiverse_id('heroes\' reunion'/'ARC', '220501').

card_in_set('hunting moa', 'ARC').
card_original_type('hunting moa'/'ARC', 'Creature — Bird Beast').
card_original_text('hunting moa'/'ARC', 'Echo {2}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Hunting Moa enters the battlefield or is put into a graveyard from the battlefield, put a +1/+1 counter on target creature.').
card_image_name('hunting moa'/'ARC', 'hunting moa').
card_uid('hunting moa'/'ARC', 'ARC:Hunting Moa:hunting moa').
card_rarity('hunting moa'/'ARC', 'Uncommon').
card_artist('hunting moa'/'ARC', 'DiTerlizzi').
card_number('hunting moa'/'ARC', '60').
card_multiverse_id('hunting moa'/'ARC', '220563').

card_in_set('i bask in your silent awe', 'ARC').
card_original_type('i bask in your silent awe'/'ARC', 'Ongoing Scheme').
card_original_text('i bask in your silent awe'/'ARC', '(An ongoing scheme remains face up until it\'s abandoned.)\nEach opponent can\'t cast more than one spell each turn.\nAt the beginning of your upkeep, if no opponent cast a spell since your last turn ended, abandon this scheme.').
card_first_print('i bask in your silent awe', 'ARC').
card_image_name('i bask in your silent awe'/'ARC', 'i bask in your silent awe').
card_uid('i bask in your silent awe'/'ARC', 'ARC:I Bask in Your Silent Awe:i bask in your silent awe').
card_rarity('i bask in your silent awe'/'ARC', 'Common').
card_artist('i bask in your silent awe'/'ARC', 'Wayne Reynolds').
card_number('i bask in your silent awe'/'ARC', '15').
card_multiverse_id('i bask in your silent awe'/'ARC', '212614').

card_in_set('i call on the ancient magics', 'ARC').
card_original_type('i call on the ancient magics'/'ARC', 'Scheme').
card_original_text('i call on the ancient magics'/'ARC', 'When you set this scheme in motion, each other player searches his or her library for a card, reveals it, and puts it into his or her hand. Then you search your library for two cards and put them into your hand. Each player shuffles his or her library.').
card_first_print('i call on the ancient magics', 'ARC').
card_image_name('i call on the ancient magics'/'ARC', 'i call on the ancient magics').
card_uid('i call on the ancient magics'/'ARC', 'ARC:I Call on the Ancient Magics:i call on the ancient magics').
card_rarity('i call on the ancient magics'/'ARC', 'Common').
card_artist('i call on the ancient magics'/'ARC', 'John Matson').
card_number('i call on the ancient magics'/'ARC', '16').
card_multiverse_id('i call on the ancient magics'/'ARC', '212619').

card_in_set('i delight in your convulsions', 'ARC').
card_original_type('i delight in your convulsions'/'ARC', 'Scheme').
card_original_text('i delight in your convulsions'/'ARC', 'When you set this scheme in motion, each opponent loses 3 life. You gain life equal to the life lost this way.').
card_first_print('i delight in your convulsions', 'ARC').
card_image_name('i delight in your convulsions'/'ARC', 'i delight in your convulsions').
card_uid('i delight in your convulsions'/'ARC', 'ARC:I Delight in Your Convulsions:i delight in your convulsions').
card_rarity('i delight in your convulsions'/'ARC', 'Common').
card_artist('i delight in your convulsions'/'ARC', 'Wayne Reynolds').
card_number('i delight in your convulsions'/'ARC', '17').
card_flavor_text('i delight in your convulsions'/'ARC', '\"I can\'t imagine what you\'re feeling right now, but I\'ll try my best.\"').
card_multiverse_id('i delight in your convulsions'/'ARC', '212580').

card_in_set('i know all, i see all', 'ARC').
card_original_type('i know all, i see all'/'ARC', 'Ongoing Scheme').
card_original_text('i know all, i see all'/'ARC', '(An ongoing scheme remains face up until it\'s abandoned.)\nUntap all permanents you control during each opponent\'s untap step.\nAt the beginning of each end step, if three or more cards were put into your graveyard this turn from anywhere, abandon this scheme.').
card_first_print('i know all, i see all', 'ARC').
card_image_name('i know all, i see all'/'ARC', 'i know all, i see all').
card_uid('i know all, i see all'/'ARC', 'ARC:I Know All, I See All:i know all, i see all').
card_rarity('i know all, i see all'/'ARC', 'Common').
card_artist('i know all, i see all'/'ARC', 'Chuck Lukacs').
card_number('i know all, i see all'/'ARC', '18').
card_multiverse_id('i know all, i see all'/'ARC', '212597').

card_in_set('ignite the cloneforge!', 'ARC').
card_original_type('ignite the cloneforge!'/'ARC', 'Scheme').
card_original_text('ignite the cloneforge!'/'ARC', 'When you set this scheme in motion, put a token onto the battlefield that\'s a copy of target permanent an opponent controls.').
card_first_print('ignite the cloneforge!', 'ARC').
card_image_name('ignite the cloneforge!'/'ARC', 'ignite the cloneforge!').
card_uid('ignite the cloneforge!'/'ARC', 'ARC:Ignite the Cloneforge!:ignite the cloneforge!').
card_rarity('ignite the cloneforge!'/'ARC', 'Common').
card_artist('ignite the cloneforge!'/'ARC', 'Ralph Horsley').
card_number('ignite the cloneforge!'/'ARC', '19').
card_flavor_text('ignite the cloneforge!'/'ARC', '\"Heroes innovate. Villains duplicate.\"').
card_multiverse_id('ignite the cloneforge!'/'ARC', '212587').

card_in_set('imperial hellkite', 'ARC').
card_original_type('imperial hellkite'/'ARC', 'Creature — Dragon').
card_original_text('imperial hellkite'/'ARC', 'Flying\nMorph {6}{R}{R} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Imperial Hellkite is turned face up, you may search your library for a Dragon card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_image_name('imperial hellkite'/'ARC', 'imperial hellkite').
card_uid('imperial hellkite'/'ARC', 'ARC:Imperial Hellkite:imperial hellkite').
card_rarity('imperial hellkite'/'ARC', 'Rare').
card_artist('imperial hellkite'/'ARC', 'Matt Cavotta').
card_number('imperial hellkite'/'ARC', '42').
card_multiverse_id('imperial hellkite'/'ARC', '220464').

card_in_set('incremental blight', 'ARC').
card_original_type('incremental blight'/'ARC', 'Sorcery').
card_original_text('incremental blight'/'ARC', 'Put a -1/-1 counter on target creature, two -1/-1 counters on another target creature, and three -1/-1 counters on a third target creature.').
card_image_name('incremental blight'/'ARC', 'incremental blight').
card_uid('incremental blight'/'ARC', 'ARC:Incremental Blight:incremental blight').
card_rarity('incremental blight'/'ARC', 'Uncommon').
card_artist('incremental blight'/'ARC', 'Chuck Lukacs').
card_number('incremental blight'/'ARC', '17').
card_flavor_text('incremental blight'/'ARC', 'Shadowmoor\'s main crops are rot, slime, and despair.').
card_multiverse_id('incremental blight'/'ARC', '220546').

card_in_set('infectious horror', 'ARC').
card_original_type('infectious horror'/'ARC', 'Creature — Zombie Horror').
card_original_text('infectious horror'/'ARC', 'Whenever Infectious Horror attacks, each opponent loses 2 life.').
card_image_name('infectious horror'/'ARC', 'infectious horror').
card_uid('infectious horror'/'ARC', 'ARC:Infectious Horror:infectious horror').
card_rarity('infectious horror'/'ARC', 'Common').
card_artist('infectious horror'/'ARC', 'Pete Venters').
card_number('infectious horror'/'ARC', '18').
card_flavor_text('infectious horror'/'ARC', 'Not once in the history of Grixis has anyone died of old age.').
card_multiverse_id('infectious horror'/'ARC', '220508').

card_in_set('inferno trap', 'ARC').
card_original_type('inferno trap'/'ARC', 'Instant — Trap').
card_original_text('inferno trap'/'ARC', 'If you\'ve been dealt damage by two or more creatures this turn, you may pay {R} rather than pay Inferno Trap\'s mana cost.\nInferno Trap deals 4 damage to target creature.').
card_image_name('inferno trap'/'ARC', 'inferno trap').
card_uid('inferno trap'/'ARC', 'ARC:Inferno Trap:inferno trap').
card_rarity('inferno trap'/'ARC', 'Uncommon').
card_artist('inferno trap'/'ARC', 'Philip Straub').
card_number('inferno trap'/'ARC', '43').
card_flavor_text('inferno trap'/'ARC', '\"Do you smell something burning?\"').
card_multiverse_id('inferno trap'/'ARC', '220469').

card_in_set('infest', 'ARC').
card_original_type('infest'/'ARC', 'Sorcery').
card_original_text('infest'/'ARC', 'All creatures get -2/-2 until end of turn.').
card_image_name('infest'/'ARC', 'infest').
card_uid('infest'/'ARC', 'ARC:Infest:infest').
card_rarity('infest'/'ARC', 'Uncommon').
card_artist('infest'/'ARC', 'Karl Kopinski').
card_number('infest'/'ARC', '19').
card_flavor_text('infest'/'ARC', '\"This is why we don\'t go out in banewasp weather.\"\n—Rannon, Vithian holdout').
card_multiverse_id('infest'/'ARC', '220459').

card_in_set('into the earthen maw', 'ARC').
card_original_type('into the earthen maw'/'ARC', 'Scheme').
card_original_text('into the earthen maw'/'ARC', 'When you set this scheme in motion, exile up to one target creature with flying, up to one target creature without flying, and all cards from up to one target opponent\'s graveyard.').
card_first_print('into the earthen maw', 'ARC').
card_image_name('into the earthen maw'/'ARC', 'into the earthen maw').
card_uid('into the earthen maw'/'ARC', 'ARC:Into the Earthen Maw:into the earthen maw').
card_rarity('into the earthen maw'/'ARC', 'Common').
card_artist('into the earthen maw'/'ARC', 'Paul Bonner').
card_number('into the earthen maw'/'ARC', '20').
card_flavor_text('into the earthen maw'/'ARC', '\"You may even see them again. Full digestion takes three hundred years.\"').
card_multiverse_id('into the earthen maw'/'ARC', '212582').

card_in_set('introductions are in order', 'ARC').
card_original_type('introductions are in order'/'ARC', 'Scheme').
card_original_text('introductions are in order'/'ARC', 'When you set this scheme in motion, choose one — Search your library for a creature card, reveal it, put it into your hand, then shuffle your library; or you may put a creature card from your hand onto the battlefield.').
card_first_print('introductions are in order', 'ARC').
card_image_name('introductions are in order'/'ARC', 'introductions are in order').
card_uid('introductions are in order'/'ARC', 'ARC:Introductions Are in Order:introductions are in order').
card_rarity('introductions are in order'/'ARC', 'Common').
card_artist('introductions are in order'/'ARC', 'Nils Hamm').
card_number('introductions are in order'/'ARC', '21').
card_flavor_text('introductions are in order'/'ARC', '\"Meet my newest minion. Come now, don\'t be shy—shake her pincer.\"').
card_multiverse_id('introductions are in order'/'ARC', '212603').

card_in_set('island', 'ARC').
card_original_type('island'/'ARC', 'Basic Land — Island').
card_original_text('island'/'ARC', 'U').
card_image_name('island'/'ARC', 'island1').
card_uid('island'/'ARC', 'ARC:Island:island1').
card_rarity('island'/'ARC', 'Basic Land').
card_artist('island'/'ARC', 'John Avon').
card_number('island'/'ARC', '139').
card_multiverse_id('island'/'ARC', '221300').

card_in_set('island', 'ARC').
card_original_type('island'/'ARC', 'Basic Land — Island').
card_original_text('island'/'ARC', 'U').
card_image_name('island'/'ARC', 'island2').
card_uid('island'/'ARC', 'ARC:Island:island2').
card_rarity('island'/'ARC', 'Basic Land').
card_artist('island'/'ARC', 'John Avon').
card_number('island'/'ARC', '140').
card_multiverse_id('island'/'ARC', '221302').

card_in_set('island', 'ARC').
card_original_type('island'/'ARC', 'Basic Land — Island').
card_original_text('island'/'ARC', 'U').
card_image_name('island'/'ARC', 'island3').
card_uid('island'/'ARC', 'ARC:Island:island3').
card_rarity('island'/'ARC', 'Basic Land').
card_artist('island'/'ARC', 'John Avon').
card_number('island'/'ARC', '141').
card_multiverse_id('island'/'ARC', '221301').

card_in_set('juggernaut', 'ARC').
card_original_type('juggernaut'/'ARC', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'ARC', 'Juggernaut attacks each turn if able.\nJuggernaut can\'t be blocked by Walls.').
card_image_name('juggernaut'/'ARC', 'juggernaut').
card_uid('juggernaut'/'ARC', 'ARC:Juggernaut:juggernaut').
card_rarity('juggernaut'/'ARC', 'Uncommon').
card_artist('juggernaut'/'ARC', 'Arnie Swekel').
card_number('juggernaut'/'ARC', '109').
card_flavor_text('juggernaut'/'ARC', 'Built with neither a way to steer nor a way to stop, the juggernauts were simply aimed at an enemy\'s best defenses and told to charge.').
card_multiverse_id('juggernaut'/'ARC', '220484').

card_in_set('kaervek the merciless', 'ARC').
card_original_type('kaervek the merciless'/'ARC', 'Legendary Creature — Human Shaman').
card_original_text('kaervek the merciless'/'ARC', 'Whenever an opponent casts a spell, Kaervek the Merciless deals damage to target creature or player equal to that spell\'s converted mana cost.').
card_image_name('kaervek the merciless'/'ARC', 'kaervek the merciless').
card_uid('kaervek the merciless'/'ARC', 'ARC:Kaervek the Merciless:kaervek the merciless').
card_rarity('kaervek the merciless'/'ARC', 'Rare').
card_artist('kaervek the merciless'/'ARC', 'rk post').
card_number('kaervek the merciless'/'ARC', '88').
card_flavor_text('kaervek the merciless'/'ARC', '\"Rats and jackals feast in his swath, but even they will not walk with him.\"\n—Mangara').
card_multiverse_id('kaervek the merciless'/'ARC', '220557').

card_in_set('kamahl, fist of krosa', 'ARC').
card_original_type('kamahl, fist of krosa'/'ARC', 'Legendary Creature — Human Druid').
card_original_text('kamahl, fist of krosa'/'ARC', '{G}: Target land becomes a 1/1 creature until end of turn. It\'s still a land.\n{2}{G}{G}{G}: Creatures you control get +3/+3 and gain trample until end of turn.').
card_image_name('kamahl, fist of krosa'/'ARC', 'kamahl, fist of krosa').
card_uid('kamahl, fist of krosa'/'ARC', 'ARC:Kamahl, Fist of Krosa:kamahl, fist of krosa').
card_rarity('kamahl, fist of krosa'/'ARC', 'Rare').
card_artist('kamahl, fist of krosa'/'ARC', 'Matthew D. Wilson').
card_number('kamahl, fist of krosa'/'ARC', '61').
card_flavor_text('kamahl, fist of krosa'/'ARC', '\"My mind has changed. My strength has not.\"').
card_multiverse_id('kamahl, fist of krosa'/'ARC', '220490').

card_in_set('kazandu refuge', 'ARC').
card_original_type('kazandu refuge'/'ARC', 'Land').
card_original_text('kazandu refuge'/'ARC', 'Kazandu Refuge enters the battlefield tapped.\nWhen Kazandu Refuge enters the battlefield, you gain 1 life.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('kazandu refuge'/'ARC', 'kazandu refuge').
card_uid('kazandu refuge'/'ARC', 'ARC:Kazandu Refuge:kazandu refuge').
card_rarity('kazandu refuge'/'ARC', 'Uncommon').
card_artist('kazandu refuge'/'ARC', 'Franz Vohwinkel').
card_number('kazandu refuge'/'ARC', '126').
card_multiverse_id('kazandu refuge'/'ARC', '220470').

card_in_set('khalni garden', 'ARC').
card_original_type('khalni garden'/'ARC', 'Land').
card_original_text('khalni garden'/'ARC', 'Khalni Garden enters the battlefield tapped.\nWhen Khalni Garden enters the battlefield, put a 0/1 green Plant creature token onto the battlefield.\n{T}: Add {G} to your mana pool.').
card_image_name('khalni garden'/'ARC', 'khalni garden').
card_uid('khalni garden'/'ARC', 'ARC:Khalni Garden:khalni garden').
card_rarity('khalni garden'/'ARC', 'Common').
card_artist('khalni garden'/'ARC', 'Ryan Pancoast').
card_number('khalni garden'/'ARC', '127').
card_multiverse_id('khalni garden'/'ARC', '220535').

card_in_set('kilnmouth dragon', 'ARC').
card_original_type('kilnmouth dragon'/'ARC', 'Creature — Dragon').
card_original_text('kilnmouth dragon'/'ARC', 'Amplify 3 (As this creature enters the battlefield, put three +1/+1 counters on it for each Dragon card you reveal in your hand.)\nFlying\n{T}: Kilnmouth Dragon deals damage equal to the number of +1/+1 counters on it to target creature or player.').
card_image_name('kilnmouth dragon'/'ARC', 'kilnmouth dragon').
card_uid('kilnmouth dragon'/'ARC', 'ARC:Kilnmouth Dragon:kilnmouth dragon').
card_rarity('kilnmouth dragon'/'ARC', 'Rare').
card_artist('kilnmouth dragon'/'ARC', 'Carl Critchlow').
card_number('kilnmouth dragon'/'ARC', '44').
card_multiverse_id('kilnmouth dragon'/'ARC', '220465').

card_in_set('know naught but fire', 'ARC').
card_original_type('know naught but fire'/'ARC', 'Scheme').
card_original_text('know naught but fire'/'ARC', 'When you set this scheme in motion, it deals damage to each opponent equal to the number of cards in that player\'s hand.').
card_first_print('know naught but fire', 'ARC').
card_image_name('know naught but fire'/'ARC', 'know naught but fire').
card_uid('know naught but fire'/'ARC', 'ARC:Know Naught but Fire:know naught but fire').
card_rarity('know naught but fire'/'ARC', 'Common').
card_artist('know naught but fire'/'ARC', 'Val Mayerik').
card_number('know naught but fire'/'ARC', '23').
card_flavor_text('know naught but fire'/'ARC', '\"Cranial explosions for everyone. Still planning to outthink my dragons?\"').
card_multiverse_id('know naught but fire'/'ARC', '212600').

card_in_set('krosan tusker', 'ARC').
card_original_type('krosan tusker'/'ARC', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'ARC', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)\nWhen you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('krosan tusker'/'ARC', 'krosan tusker').
card_uid('krosan tusker'/'ARC', 'ARC:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'ARC', 'Common').
card_artist('krosan tusker'/'ARC', 'Kev Walker').
card_number('krosan tusker'/'ARC', '62').
card_multiverse_id('krosan tusker'/'ARC', '220491').

card_in_set('krosan verge', 'ARC').
card_original_type('krosan verge'/'ARC', 'Land').
card_original_text('krosan verge'/'ARC', 'Krosan Verge enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Krosan Verge: Search your library for a Forest card and a Plains card and put them onto the battlefield tapped. Then shuffle your library.').
card_image_name('krosan verge'/'ARC', 'krosan verge').
card_uid('krosan verge'/'ARC', 'ARC:Krosan Verge:krosan verge').
card_rarity('krosan verge'/'ARC', 'Uncommon').
card_artist('krosan verge'/'ARC', 'Tony Szczudlo').
card_number('krosan verge'/'ARC', '128').
card_multiverse_id('krosan verge'/'ARC', '220544').

card_in_set('leaf gilder', 'ARC').
card_original_type('leaf gilder'/'ARC', 'Creature — Elf Druid').
card_original_text('leaf gilder'/'ARC', '{T}: Add {G} to your mana pool.').
card_image_name('leaf gilder'/'ARC', 'leaf gilder').
card_uid('leaf gilder'/'ARC', 'ARC:Leaf Gilder:leaf gilder').
card_rarity('leaf gilder'/'ARC', 'Common').
card_artist('leaf gilder'/'ARC', 'Quinton Hoover').
card_number('leaf gilder'/'ARC', '63').
card_flavor_text('leaf gilder'/'ARC', 'Eidren, perfect of Lys Alana, ordered hundreds of trees uprooted and rearranged into a pattern he deemed beautiful. Thus the Gilt-Leaf Wood was born.').
card_multiverse_id('leaf gilder'/'ARC', '220568').

card_in_set('leonin abunas', 'ARC').
card_original_type('leonin abunas'/'ARC', 'Creature — Cat Cleric').
card_original_text('leonin abunas'/'ARC', 'Artifacts you control can\'t be the targets of spells or abilities your opponents control.').
card_image_name('leonin abunas'/'ARC', 'leonin abunas').
card_uid('leonin abunas'/'ARC', 'ARC:Leonin Abunas:leonin abunas').
card_rarity('leonin abunas'/'ARC', 'Rare').
card_artist('leonin abunas'/'ARC', 'Darrell Riche').
card_number('leonin abunas'/'ARC', '1').
card_flavor_text('leonin abunas'/'ARC', 'Only leonin clerics who can survive the Razor Fields for one turning of the suns can stand in the Cave of Light.').
card_multiverse_id('leonin abunas'/'ARC', '220527').

card_in_set('lightning greaves', 'ARC').
card_original_type('lightning greaves'/'ARC', 'Artifact — Equipment').
card_original_text('lightning greaves'/'ARC', 'Equipped creature has shroud and haste.\nEquip {0}').
card_image_name('lightning greaves'/'ARC', 'lightning greaves').
card_uid('lightning greaves'/'ARC', 'ARC:Lightning Greaves:lightning greaves').
card_rarity('lightning greaves'/'ARC', 'Uncommon').
card_artist('lightning greaves'/'ARC', 'Jeremy Jarvis').
card_number('lightning greaves'/'ARC', '110').
card_flavor_text('lightning greaves'/'ARC', 'After lightning struck the cliffs, the ore became iron, the iron became steel, and the steel became greaves. The lightning never left.').
card_multiverse_id('lightning greaves'/'ARC', '220528').

card_in_set('llanowar reborn', 'ARC').
card_original_type('llanowar reborn'/'ARC', 'Land').
card_original_text('llanowar reborn'/'ARC', 'Llanowar Reborn enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nGraft 1 (This land enters the battlefield with a +1/+1 counter on it. Whenever a creature enters the battlefield, you may move a +1/+1 counter from this land onto it.)').
card_image_name('llanowar reborn'/'ARC', 'llanowar reborn').
card_uid('llanowar reborn'/'ARC', 'ARC:Llanowar Reborn:llanowar reborn').
card_rarity('llanowar reborn'/'ARC', 'Uncommon').
card_artist('llanowar reborn'/'ARC', 'Philip Straub').
card_number('llanowar reborn'/'ARC', '129').
card_multiverse_id('llanowar reborn'/'ARC', '220496').

card_in_set('lodestone golem', 'ARC').
card_original_type('lodestone golem'/'ARC', 'Artifact Creature — Golem').
card_original_text('lodestone golem'/'ARC', 'Nonartifact spells cost {1} more to cast.').
card_image_name('lodestone golem'/'ARC', 'lodestone golem').
card_uid('lodestone golem'/'ARC', 'ARC:Lodestone Golem:lodestone golem').
card_rarity('lodestone golem'/'ARC', 'Rare').
card_artist('lodestone golem'/'ARC', 'Chris Rahn').
card_number('lodestone golem'/'ARC', '111').
card_flavor_text('lodestone golem'/'ARC', '\"Somehow it warps the Æther. It brings a strange weight, a blockade in the flow of spellcraft.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('lodestone golem'/'ARC', '220536').

card_in_set('look skyward and despair', 'ARC').
card_original_type('look skyward and despair'/'ARC', 'Scheme').
card_original_text('look skyward and despair'/'ARC', 'When you set this scheme in motion, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_first_print('look skyward and despair', 'ARC').
card_image_name('look skyward and despair'/'ARC', 'look skyward and despair').
card_uid('look skyward and despair'/'ARC', 'ARC:Look Skyward and Despair:look skyward and despair').
card_rarity('look skyward and despair'/'ARC', 'Common').
card_artist('look skyward and despair'/'ARC', 'Todd Lockwood').
card_number('look skyward and despair'/'ARC', '24').
card_flavor_text('look skyward and despair'/'ARC', '\"Today I take the reins of destiny itself. Today the Draconic Apocalypse is upon you.\"').
card_multiverse_id('look skyward and despair'/'ARC', '212579').

card_in_set('magister sphinx', 'ARC').
card_original_type('magister sphinx'/'ARC', 'Artifact Creature — Sphinx').
card_original_text('magister sphinx'/'ARC', 'Flying\nWhen Magister Sphinx enters the battlefield, target player\'s life total becomes 10.').
card_image_name('magister sphinx'/'ARC', 'magister sphinx').
card_uid('magister sphinx'/'ARC', 'ARC:Magister Sphinx:magister sphinx').
card_rarity('magister sphinx'/'ARC', 'Rare').
card_artist('magister sphinx'/'ARC', 'Steven Belledin').
card_number('magister sphinx'/'ARC', '89').
card_flavor_text('magister sphinx'/'ARC', '\"These benighted worlds are thick with ignorance. I will educate them. They will listen, or they will die.\"').
card_multiverse_id('magister sphinx'/'ARC', '220509').

card_in_set('makeshift mannequin', 'ARC').
card_original_type('makeshift mannequin'/'ARC', 'Instant').
card_original_text('makeshift mannequin'/'ARC', 'Return target creature card from your graveyard to the battlefield with a mannequin counter on it. For as long as that creature has a mannequin counter on it, it has \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_image_name('makeshift mannequin'/'ARC', 'makeshift mannequin').
card_uid('makeshift mannequin'/'ARC', 'ARC:Makeshift Mannequin:makeshift mannequin').
card_rarity('makeshift mannequin'/'ARC', 'Uncommon').
card_artist('makeshift mannequin'/'ARC', 'Darrell Riche').
card_number('makeshift mannequin'/'ARC', '20').
card_flavor_text('makeshift mannequin'/'ARC', '\"This vulgar mimicry will end now.\"\n—Desmera, perfect of Wren\'s Run').
card_multiverse_id('makeshift mannequin'/'ARC', '220569').

card_in_set('march of the machines', 'ARC').
card_original_type('march of the machines'/'ARC', 'Enchantment').
card_original_text('march of the machines'/'ARC', 'Each noncreature artifact is an artifact creature with power and toughness each equal to its converted mana cost. (Equipment that\'s a creature can\'t equip a creature.)').
card_image_name('march of the machines'/'ARC', 'march of the machines').
card_uid('march of the machines'/'ARC', 'ARC:March of the Machines:march of the machines').
card_rarity('march of the machines'/'ARC', 'Rare').
card_artist('march of the machines'/'ARC', 'Ben Thompson').
card_number('march of the machines'/'ARC', '6').
card_multiverse_id('march of the machines'/'ARC', '220529').

card_in_set('master transmuter', 'ARC').
card_original_type('master transmuter'/'ARC', 'Artifact Creature — Human Artificer').
card_original_text('master transmuter'/'ARC', '{U}, {T}, Return an artifact you control to its owner\'s hand: You may put an artifact card from your hand onto the battlefield.').
card_image_name('master transmuter'/'ARC', 'master transmuter').
card_uid('master transmuter'/'ARC', 'ARC:Master Transmuter:master transmuter').
card_rarity('master transmuter'/'ARC', 'Rare').
card_artist('master transmuter'/'ARC', 'Chippy').
card_number('master transmuter'/'ARC', '7').
card_flavor_text('master transmuter'/'ARC', '\"Wasted potential surrounds us. Lend me that bauble, and let me see what it can be made to be.\"').
card_multiverse_id('master transmuter'/'ARC', '220510').

card_in_set('may civilization collapse', 'ARC').
card_original_type('may civilization collapse'/'ARC', 'Scheme').
card_original_text('may civilization collapse'/'ARC', 'When you set this scheme in motion, target opponent chooses self or others. If that player chooses self, he or she sacrifices two lands. If the player chooses others, each of your other opponents sacrifices a land.').
card_first_print('may civilization collapse', 'ARC').
card_image_name('may civilization collapse'/'ARC', 'may civilization collapse').
card_uid('may civilization collapse'/'ARC', 'ARC:May Civilization Collapse:may civilization collapse').
card_rarity('may civilization collapse'/'ARC', 'Common').
card_artist('may civilization collapse'/'ARC', 'John Matson').
card_number('may civilization collapse'/'ARC', '25').
card_flavor_text('may civilization collapse'/'ARC', '\"This should halt the spread of your insolence.\"').
card_multiverse_id('may civilization collapse'/'ARC', '212606').

card_in_set('memnarch', 'ARC').
card_original_type('memnarch'/'ARC', 'Legendary Artifact Creature — Wizard').
card_original_text('memnarch'/'ARC', '{1}{U}{U}: Target permanent becomes an artifact in addition to its other types.\n{3}{U}: Gain control of target artifact.').
card_image_name('memnarch'/'ARC', 'memnarch').
card_uid('memnarch'/'ARC', 'ARC:Memnarch:memnarch').
card_rarity('memnarch'/'ARC', 'Rare').
card_artist('memnarch'/'ARC', 'Carl Critchlow').
card_number('memnarch'/'ARC', '112').
card_flavor_text('memnarch'/'ARC', 'In the blur between metal and flesh, Memnarch found madness.').
card_multiverse_id('memnarch'/'ARC', '220532').

card_in_set('metallurgeon', 'ARC').
card_original_type('metallurgeon'/'ARC', 'Artifact Creature — Human Artificer').
card_original_text('metallurgeon'/'ARC', '{W}, {T}: Regenerate target artifact.').
card_image_name('metallurgeon'/'ARC', 'metallurgeon').
card_uid('metallurgeon'/'ARC', 'ARC:Metallurgeon:metallurgeon').
card_rarity('metallurgeon'/'ARC', 'Uncommon').
card_artist('metallurgeon'/'ARC', 'Warren Mahy').
card_number('metallurgeon'/'ARC', '2').
card_flavor_text('metallurgeon'/'ARC', '\"By the time I got there, the heart had stopped. Fortunately, I was able to replace it with something better.\"').
card_multiverse_id('metallurgeon'/'ARC', '220460').

card_in_set('mistvein borderpost', 'ARC').
card_original_type('mistvein borderpost'/'ARC', 'Artifact').
card_original_text('mistvein borderpost'/'ARC', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Mistvein Borderpost\'s mana cost.\nMistvein Borderpost enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('mistvein borderpost'/'ARC', 'mistvein borderpost').
card_uid('mistvein borderpost'/'ARC', 'ARC:Mistvein Borderpost:mistvein borderpost').
card_rarity('mistvein borderpost'/'ARC', 'Common').
card_artist('mistvein borderpost'/'ARC', 'Pete Venters').
card_number('mistvein borderpost'/'ARC', '90').
card_multiverse_id('mistvein borderpost'/'ARC', '220522').

card_in_set('molimo, maro-sorcerer', 'ARC').
card_original_type('molimo, maro-sorcerer'/'ARC', 'Legendary Creature — Elemental').
card_original_text('molimo, maro-sorcerer'/'ARC', 'Trample\nMolimo, Maro-Sorcerer\'s power and toughness are each equal to the number of lands you control.').
card_image_name('molimo, maro-sorcerer'/'ARC', 'molimo, maro-sorcerer').
card_uid('molimo, maro-sorcerer'/'ARC', 'ARC:Molimo, Maro-Sorcerer:molimo, maro-sorcerer').
card_rarity('molimo, maro-sorcerer'/'ARC', 'Rare').
card_artist('molimo, maro-sorcerer'/'ARC', 'Mark Zug').
card_number('molimo, maro-sorcerer'/'ARC', '64').
card_flavor_text('molimo, maro-sorcerer'/'ARC', '\"My mind is the spread of the canopy. My heart is the embrace of the roots. I am deathless Llanowar, its fury and its peace.\"').
card_multiverse_id('molimo, maro-sorcerer'/'ARC', '220485').

card_in_set('mortal flesh is weak', 'ARC').
card_original_type('mortal flesh is weak'/'ARC', 'Scheme').
card_original_text('mortal flesh is weak'/'ARC', 'When you set this scheme in motion, each opponent\'s life total becomes the lowest life total among your opponents.').
card_first_print('mortal flesh is weak', 'ARC').
card_image_name('mortal flesh is weak'/'ARC', 'mortal flesh is weak').
card_uid('mortal flesh is weak'/'ARC', 'ARC:Mortal Flesh Is Weak:mortal flesh is weak').
card_rarity('mortal flesh is weak'/'ARC', 'Common').
card_artist('mortal flesh is weak'/'ARC', 'Howard Lyon').
card_number('mortal flesh is weak'/'ARC', '26').
card_flavor_text('mortal flesh is weak'/'ARC', '\"I certainly would have accepted my offer of eternal life. But if you choose death instead, who am I to argue?\"').
card_multiverse_id('mortal flesh is weak'/'ARC', '212608').

card_in_set('mosswort bridge', 'ARC').
card_original_type('mosswort bridge'/'ARC', 'Land').
card_original_text('mosswort bridge'/'ARC', 'Hideaway (This land enters the battlefield tapped. When it does, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library.)\n{T}: Add {G} to your mana pool.\n{G}, {T}: You may play the exiled card without paying its mana cost if creatures you control have total power 10 or greater.').
card_image_name('mosswort bridge'/'ARC', 'mosswort bridge').
card_uid('mosswort bridge'/'ARC', 'ARC:Mosswort Bridge:mosswort bridge').
card_rarity('mosswort bridge'/'ARC', 'Rare').
card_artist('mosswort bridge'/'ARC', 'Jeremy Jarvis').
card_number('mosswort bridge'/'ARC', '130').
card_multiverse_id('mosswort bridge'/'ARC', '220570').

card_in_set('mountain', 'ARC').
card_original_type('mountain'/'ARC', 'Basic Land — Mountain').
card_original_text('mountain'/'ARC', 'R').
card_image_name('mountain'/'ARC', 'mountain1').
card_uid('mountain'/'ARC', 'ARC:Mountain:mountain1').
card_rarity('mountain'/'ARC', 'Basic Land').
card_artist('mountain'/'ARC', 'Rob Alexander').
card_number('mountain'/'ARC', '145').
card_multiverse_id('mountain'/'ARC', '221303').

card_in_set('mountain', 'ARC').
card_original_type('mountain'/'ARC', 'Basic Land — Mountain').
card_original_text('mountain'/'ARC', 'R').
card_image_name('mountain'/'ARC', 'mountain2').
card_uid('mountain'/'ARC', 'ARC:Mountain:mountain2').
card_rarity('mountain'/'ARC', 'Basic Land').
card_artist('mountain'/'ARC', 'Rob Alexander').
card_number('mountain'/'ARC', '146').
card_multiverse_id('mountain'/'ARC', '221305').

card_in_set('mountain', 'ARC').
card_original_type('mountain'/'ARC', 'Basic Land — Mountain').
card_original_text('mountain'/'ARC', 'R').
card_image_name('mountain'/'ARC', 'mountain3').
card_uid('mountain'/'ARC', 'ARC:Mountain:mountain3').
card_rarity('mountain'/'ARC', 'Basic Land').
card_artist('mountain'/'ARC', 'Rob Alexander').
card_number('mountain'/'ARC', '147').
card_multiverse_id('mountain'/'ARC', '221304').

card_in_set('my crushing masterstroke', 'ARC').
card_original_type('my crushing masterstroke'/'ARC', 'Scheme').
card_original_text('my crushing masterstroke'/'ARC', 'When you set this scheme in motion, gain control of all nonland permanents your opponents control until end of turn. Untap those permanents. They gain haste until end of turn. Each of them attacks its owner this turn if able.').
card_first_print('my crushing masterstroke', 'ARC').
card_image_name('my crushing masterstroke'/'ARC', 'my crushing masterstroke').
card_uid('my crushing masterstroke'/'ARC', 'ARC:My Crushing Masterstroke:my crushing masterstroke').
card_rarity('my crushing masterstroke'/'ARC', 'Common').
card_artist('my crushing masterstroke'/'ARC', 'Alex Horley-Orlandelli').
card_number('my crushing masterstroke'/'ARC', '27').
card_multiverse_id('my crushing masterstroke'/'ARC', '212611').

card_in_set('my genius knows no bounds', 'ARC').
card_original_type('my genius knows no bounds'/'ARC', 'Scheme').
card_original_text('my genius knows no bounds'/'ARC', 'When you set this scheme in motion, you may pay {X}. If you do, you gain X life and draw X cards.').
card_first_print('my genius knows no bounds', 'ARC').
card_image_name('my genius knows no bounds'/'ARC', 'my genius knows no bounds').
card_uid('my genius knows no bounds'/'ARC', 'ARC:My Genius Knows No Bounds:my genius knows no bounds').
card_rarity('my genius knows no bounds'/'ARC', 'Common').
card_artist('my genius knows no bounds'/'ARC', 'John Stanko').
card_number('my genius knows no bounds'/'ARC', '28').
card_flavor_text('my genius knows no bounds'/'ARC', '\"I can see . . . everything. The beginning and the end of time. The raw edges of reality. And now I see exactly how to kill you.\"').
card_multiverse_id('my genius knows no bounds'/'ARC', '212661').

card_in_set('my undead horde awakens', 'ARC').
card_original_type('my undead horde awakens'/'ARC', 'Ongoing Scheme').
card_original_text('my undead horde awakens'/'ARC', '(An ongoing scheme remains face up until it\'s abandoned.)\nAt the beginning of your end step, you may put target creature card from an opponent\'s graveyard onto the battlefield under your control.\nWhen a creature put onto the battlefield with this scheme is put into a graveyard, abandon this scheme.').
card_first_print('my undead horde awakens', 'ARC').
card_image_name('my undead horde awakens'/'ARC', 'my undead horde awakens').
card_uid('my undead horde awakens'/'ARC', 'ARC:My Undead Horde Awakens:my undead horde awakens').
card_rarity('my undead horde awakens'/'ARC', 'Common').
card_artist('my undead horde awakens'/'ARC', 'Thomas M. Baxa').
card_number('my undead horde awakens'/'ARC', '29').
card_multiverse_id('my undead horde awakens'/'ARC', '212620').

card_in_set('my wish is your command', 'ARC').
card_original_type('my wish is your command'/'ARC', 'Scheme').
card_original_text('my wish is your command'/'ARC', 'When you set this scheme in motion, each opponent reveals his or her hand. You may choose a noncreature, nonland card revealed this way and cast it without paying its mana cost.').
card_first_print('my wish is your command', 'ARC').
card_image_name('my wish is your command'/'ARC', 'my wish is your command').
card_uid('my wish is your command'/'ARC', 'ARC:My Wish Is Your Command:my wish is your command').
card_rarity('my wish is your command'/'ARC', 'Common').
card_artist('my wish is your command'/'ARC', 'Steve Prescott').
card_number('my wish is your command'/'ARC', '30').
card_flavor_text('my wish is your command'/'ARC', '\"Indulge me. Just one spell. One potentially fatal spell gone horribly awry.\"').
card_multiverse_id('my wish is your command'/'ARC', '212607').

card_in_set('nantuko monastery', 'ARC').
card_original_type('nantuko monastery'/'ARC', 'Land').
card_original_text('nantuko monastery'/'ARC', '{T}: Add {1} to your mana pool.\nThreshold — {G}{W}: Nantuko Monastery becomes a 4/4 green and white Insect Monk creature with first strike until end of turn. It\'s still a land. Activate this ability only if seven or more cards are in your graveyard.').
card_image_name('nantuko monastery'/'ARC', 'nantuko monastery').
card_uid('nantuko monastery'/'ARC', 'ARC:Nantuko Monastery:nantuko monastery').
card_rarity('nantuko monastery'/'ARC', 'Uncommon').
card_artist('nantuko monastery'/'ARC', 'Rob Alexander').
card_number('nantuko monastery'/'ARC', '131').
card_multiverse_id('nantuko monastery'/'ARC', '220545').

card_in_set('nature demands an offering', 'ARC').
card_original_type('nature demands an offering'/'ARC', 'Scheme').
card_original_text('nature demands an offering'/'ARC', 'When you set this scheme in motion, target opponent chooses a creature you don\'t control and puts it on top of its owner\'s library, then repeats this process for an artifact, an enchantment, and a land. Then the owner of each permanent chosen this way shuffles his or her library.').
card_first_print('nature demands an offering', 'ARC').
card_image_name('nature demands an offering'/'ARC', 'nature demands an offering').
card_uid('nature demands an offering'/'ARC', 'ARC:Nature Demands an Offering:nature demands an offering').
card_rarity('nature demands an offering'/'ARC', 'Common').
card_artist('nature demands an offering'/'ARC', 'John Matson').
card_number('nature demands an offering'/'ARC', '31').
card_multiverse_id('nature demands an offering'/'ARC', '212601').

card_in_set('nature shields its own', 'ARC').
card_original_type('nature shields its own'/'ARC', 'Ongoing Scheme').
card_original_text('nature shields its own'/'ARC', '(An ongoing scheme remains face up until it\'s abandoned.)\nWhenever a creature attacks and isn\'t blocked, if you\'re the defending player, put a 0/1 green Plant creature token onto the battlefield blocking that creature.\nWhen four or more creatures attack you, abandon this scheme at end of combat.').
card_first_print('nature shields its own', 'ARC').
card_image_name('nature shields its own'/'ARC', 'nature shields its own').
card_uid('nature shields its own'/'ARC', 'ARC:Nature Shields Its Own:nature shields its own').
card_rarity('nature shields its own'/'ARC', 'Common').
card_artist('nature shields its own'/'ARC', 'John Stanko').
card_number('nature shields its own'/'ARC', '32').
card_multiverse_id('nature shields its own'/'ARC', '212577').

card_in_set('nothing can stop me now', 'ARC').
card_original_type('nothing can stop me now'/'ARC', 'Ongoing Scheme').
card_original_text('nothing can stop me now'/'ARC', '(An ongoing scheme remains face up until it\'s abandoned.)\nIf a source an opponent controls would deal damage to you, prevent 1 of that damage.\nAt the beginning of each end step, if you\'ve been dealt 5 or more damage this turn, abandon this scheme.').
card_first_print('nothing can stop me now', 'ARC').
card_image_name('nothing can stop me now'/'ARC', 'nothing can stop me now').
card_uid('nothing can stop me now'/'ARC', 'ARC:Nothing Can Stop Me Now:nothing can stop me now').
card_rarity('nothing can stop me now'/'ARC', 'Common').
card_artist('nothing can stop me now'/'ARC', 'Jesper Ejsing').
card_number('nothing can stop me now'/'ARC', '33').
card_multiverse_id('nothing can stop me now'/'ARC', '212624').

card_in_set('obelisk of esper', 'ARC').
card_original_type('obelisk of esper'/'ARC', 'Artifact').
card_original_text('obelisk of esper'/'ARC', '{T}: Add {W}, {U}, or {B} to your mana pool.').
card_image_name('obelisk of esper'/'ARC', 'obelisk of esper').
card_uid('obelisk of esper'/'ARC', 'ARC:Obelisk of Esper:obelisk of esper').
card_rarity('obelisk of esper'/'ARC', 'Common').
card_artist('obelisk of esper'/'ARC', 'Francis Tsai').
card_number('obelisk of esper'/'ARC', '113').
card_flavor_text('obelisk of esper'/'ARC', 'It is a monument as austere, unyielding, and inscrutable as Esper itself.').
card_multiverse_id('obelisk of esper'/'ARC', '220461').

card_in_set('oblivion ring', 'ARC').
card_original_type('oblivion ring'/'ARC', 'Enchantment').
card_original_text('oblivion ring'/'ARC', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'ARC', 'oblivion ring').
card_uid('oblivion ring'/'ARC', 'ARC:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'ARC', 'Common').
card_artist('oblivion ring'/'ARC', 'Franz Vohwinkel').
card_number('oblivion ring'/'ARC', '3').
card_multiverse_id('oblivion ring'/'ARC', '220586').

card_in_set('only blood ends your nightmares', 'ARC').
card_original_type('only blood ends your nightmares'/'ARC', 'Scheme').
card_original_text('only blood ends your nightmares'/'ARC', 'When you set this scheme in motion, each opponent sacrifices a creature. Then each opponent who didn\'t sacrifice a creature discards two cards.').
card_first_print('only blood ends your nightmares', 'ARC').
card_image_name('only blood ends your nightmares'/'ARC', 'only blood ends your nightmares').
card_uid('only blood ends your nightmares'/'ARC', 'ARC:Only Blood Ends Your Nightmares:only blood ends your nightmares').
card_rarity('only blood ends your nightmares'/'ARC', 'Common').
card_artist('only blood ends your nightmares'/'ARC', 'Paul Bonner').
card_number('only blood ends your nightmares'/'ARC', '34').
card_flavor_text('only blood ends your nightmares'/'ARC', '\"But nothing will end your regret.\"').
card_multiverse_id('only blood ends your nightmares'/'ARC', '212622').

card_in_set('pale recluse', 'ARC').
card_original_type('pale recluse'/'ARC', 'Creature — Spider').
card_original_text('pale recluse'/'ARC', 'Reach (This creature can block creatures with flying.)\nForestcycling {2}, plainscycling {2} ({2}, Discard this card: Search your library for a Forest or Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('pale recluse'/'ARC', 'pale recluse').
card_uid('pale recluse'/'ARC', 'ARC:Pale Recluse:pale recluse').
card_rarity('pale recluse'/'ARC', 'Common').
card_artist('pale recluse'/'ARC', 'Cyril Van Der Haegen').
card_number('pale recluse'/'ARC', '91').
card_multiverse_id('pale recluse'/'ARC', '220523').

card_in_set('path to exile', 'ARC').
card_original_type('path to exile'/'ARC', 'Instant').
card_original_text('path to exile'/'ARC', 'Exile target creature. Its controller may search his or her library for a basic land card, put that card onto the battlefield tapped, then shuffle his or her library.').
card_image_name('path to exile'/'ARC', 'path to exile').
card_uid('path to exile'/'ARC', 'ARC:Path to Exile:path to exile').
card_rarity('path to exile'/'ARC', 'Uncommon').
card_artist('path to exile'/'ARC', 'Todd Lockwood').
card_number('path to exile'/'ARC', '4').
card_multiverse_id('path to exile'/'ARC', '220511').

card_in_set('plains', 'ARC').
card_original_type('plains'/'ARC', 'Basic Land — Plains').
card_original_text('plains'/'ARC', 'W').
card_image_name('plains'/'ARC', 'plains1').
card_uid('plains'/'ARC', 'ARC:Plains:plains1').
card_rarity('plains'/'ARC', 'Basic Land').
card_artist('plains'/'ARC', 'Fred Fields').
card_number('plains'/'ARC', '137').
card_multiverse_id('plains'/'ARC', '221309').

card_in_set('plains', 'ARC').
card_original_type('plains'/'ARC', 'Basic Land — Plains').
card_original_text('plains'/'ARC', 'W').
card_image_name('plains'/'ARC', 'plains2').
card_uid('plains'/'ARC', 'ARC:Plains:plains2').
card_rarity('plains'/'ARC', 'Basic Land').
card_artist('plains'/'ARC', 'Fred Fields').
card_number('plains'/'ARC', '138').
card_multiverse_id('plains'/'ARC', '221310').

card_in_set('plummet', 'ARC').
card_original_type('plummet'/'ARC', 'Instant').
card_original_text('plummet'/'ARC', 'Destroy target creature with flying.').
card_first_print('plummet', 'ARC').
card_image_name('plummet'/'ARC', 'plummet').
card_uid('plummet'/'ARC', 'ARC:Plummet:plummet').
card_rarity('plummet'/'ARC', 'Common').
card_artist('plummet'/'ARC', 'Pete Venters').
card_number('plummet'/'ARC', '65').
card_flavor_text('plummet'/'ARC', '\"You are the grandest of all,\" said the archdruid to the trees. They became so proud of bark and branch that they suffered no creature to fly overhead or perch upon a bough.').
card_multiverse_id('plummet'/'ARC', '220514').

card_in_set('primal command', 'ARC').
card_original_type('primal command'/'ARC', 'Sorcery').
card_original_text('primal command'/'ARC', 'Choose two — Target player gains 7 life; or put target noncreature permanent on top of its owner\'s library; or target player shuffles his or her graveyard into his or her library; or search your library for a creature card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('primal command'/'ARC', 'primal command').
card_uid('primal command'/'ARC', 'ARC:Primal Command:primal command').
card_rarity('primal command'/'ARC', 'Rare').
card_artist('primal command'/'ARC', 'Wayne England').
card_number('primal command'/'ARC', '66').
card_multiverse_id('primal command'/'ARC', '220571').

card_in_set('rakdos carnarium', 'ARC').
card_original_type('rakdos carnarium'/'ARC', 'Land').
card_original_text('rakdos carnarium'/'ARC', 'Rakdos Carnarium enters the battlefield tapped.\nWhen Rakdos Carnarium enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{R} to your mana pool.').
card_image_name('rakdos carnarium'/'ARC', 'rakdos carnarium').
card_uid('rakdos carnarium'/'ARC', 'ARC:Rakdos Carnarium:rakdos carnarium').
card_rarity('rakdos carnarium'/'ARC', 'Common').
card_artist('rakdos carnarium'/'ARC', 'John Avon').
card_number('rakdos carnarium'/'ARC', '132').
card_multiverse_id('rakdos carnarium'/'ARC', '220541').

card_in_set('rakdos guildmage', 'ARC').
card_original_type('rakdos guildmage'/'ARC', 'Creature — Zombie Shaman').
card_original_text('rakdos guildmage'/'ARC', '{3}{B}, Discard a card: Target creature gets -2/-2 until end of turn.\n{3}{R}: Put a 2/1 red Goblin creature token with haste onto the battlefield. Exile it at the beginning of the next end step.').
card_image_name('rakdos guildmage'/'ARC', 'rakdos guildmage').
card_uid('rakdos guildmage'/'ARC', 'ARC:Rakdos Guildmage:rakdos guildmage').
card_rarity('rakdos guildmage'/'ARC', 'Uncommon').
card_artist('rakdos guildmage'/'ARC', 'Jeremy Jarvis').
card_number('rakdos guildmage'/'ARC', '92').
card_multiverse_id('rakdos guildmage'/'ARC', '220542').

card_in_set('rakdos signet', 'ARC').
card_original_type('rakdos signet'/'ARC', 'Artifact').
card_original_text('rakdos signet'/'ARC', '{1}, {T}: Add {B}{R} to your mana pool.').
card_image_name('rakdos signet'/'ARC', 'rakdos signet').
card_uid('rakdos signet'/'ARC', 'ARC:Rakdos Signet:rakdos signet').
card_rarity('rakdos signet'/'ARC', 'Common').
card_artist('rakdos signet'/'ARC', 'Greg Hildebrandt').
card_number('rakdos signet'/'ARC', '114').
card_flavor_text('rakdos signet'/'ARC', 'Made of bone and boiled in blood, a Rakdos signet is not considered finished until it has been used as a murder weapon.').
card_multiverse_id('rakdos signet'/'ARC', '220543').

card_in_set('rancor', 'ARC').
card_original_type('rancor'/'ARC', 'Enchantment — Aura').
card_original_text('rancor'/'ARC', 'Enchant creature\nEnchanted creature gets +2/+0 and has trample.\nWhen Rancor is put into a graveyard from the battlefield, return Rancor to its owner\'s hand.').
card_image_name('rancor'/'ARC', 'rancor').
card_uid('rancor'/'ARC', 'ARC:Rancor:rancor').
card_rarity('rancor'/'ARC', 'Common').
card_artist('rancor'/'ARC', 'Kev Walker').
card_number('rancor'/'ARC', '67').
card_flavor_text('rancor'/'ARC', 'Hatred outlives the hateful.').
card_multiverse_id('rancor'/'ARC', '220577').

card_in_set('realms befitting my majesty', 'ARC').
card_original_type('realms befitting my majesty'/'ARC', 'Scheme').
card_original_text('realms befitting my majesty'/'ARC', 'When you set this scheme in motion, search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_first_print('realms befitting my majesty', 'ARC').
card_image_name('realms befitting my majesty'/'ARC', 'realms befitting my majesty').
card_uid('realms befitting my majesty'/'ARC', 'ARC:Realms Befitting My Majesty:realms befitting my majesty').
card_rarity('realms befitting my majesty'/'ARC', 'Common').
card_artist('realms befitting my majesty'/'ARC', 'Véronique Meignaud').
card_number('realms befitting my majesty'/'ARC', '36').
card_flavor_text('realms befitting my majesty'/'ARC', '\"Look around you. All you behold is mine to rule. It\'s the natural way.\"').
card_multiverse_id('realms befitting my majesty'/'ARC', '212653').

card_in_set('reanimate', 'ARC').
card_original_type('reanimate'/'ARC', 'Sorcery').
card_original_text('reanimate'/'ARC', 'Put target creature card from a graveyard onto the battlefield under your control. You lose life equal to its converted mana cost.').
card_image_name('reanimate'/'ARC', 'reanimate').
card_uid('reanimate'/'ARC', 'ARC:Reanimate:reanimate').
card_rarity('reanimate'/'ARC', 'Uncommon').
card_artist('reanimate'/'ARC', 'Robert Bliss').
card_number('reanimate'/'ARC', '21').
card_flavor_text('reanimate'/'ARC', '\"You will learn to earn death.\"\n—Volrath').
card_multiverse_id('reanimate'/'ARC', '220576').

card_in_set('reassembling skeleton', 'ARC').
card_original_type('reassembling skeleton'/'ARC', 'Creature — Skeleton Warrior').
card_original_text('reassembling skeleton'/'ARC', '{1}{B}: Return Reassembling Skeleton from your graveyard to the battlefield tapped.').
card_first_print('reassembling skeleton', 'ARC').
card_image_name('reassembling skeleton'/'ARC', 'reassembling skeleton').
card_uid('reassembling skeleton'/'ARC', 'ARC:Reassembling Skeleton:reassembling skeleton').
card_rarity('reassembling skeleton'/'ARC', 'Uncommon').
card_artist('reassembling skeleton'/'ARC', 'Austin Hsu').
card_number('reassembling skeleton'/'ARC', '22').
card_flavor_text('reassembling skeleton'/'ARC', 'Though you may see the same bones, you\'ll never see the same skeleton twice.').
card_multiverse_id('reassembling skeleton'/'ARC', '220515').

card_in_set('roots of all evil', 'ARC').
card_original_type('roots of all evil'/'ARC', 'Scheme').
card_original_text('roots of all evil'/'ARC', 'When you set this scheme in motion, put five 1/1 green Saproling creature tokens onto the battlefield.').
card_first_print('roots of all evil', 'ARC').
card_image_name('roots of all evil'/'ARC', 'roots of all evil').
card_uid('roots of all evil'/'ARC', 'ARC:Roots of All Evil:roots of all evil').
card_rarity('roots of all evil'/'ARC', 'Common').
card_artist('roots of all evil'/'ARC', 'Paul Bonner').
card_number('roots of all evil'/'ARC', '37').
card_flavor_text('roots of all evil'/'ARC', '\"I assure you, these are not the kind that thrive on sunlight and water.\"').
card_multiverse_id('roots of all evil'/'ARC', '212602').

card_in_set('rotted ones, lay siege', 'ARC').
card_original_type('rotted ones, lay siege'/'ARC', 'Scheme').
card_original_text('rotted ones, lay siege'/'ARC', 'When you set this scheme in motion, for each opponent, put a 2/2 black Zombie creature token onto the battlefield that attacks that player each combat if able.').
card_first_print('rotted ones, lay siege', 'ARC').
card_image_name('rotted ones, lay siege'/'ARC', 'rotted ones, lay siege').
card_uid('rotted ones, lay siege'/'ARC', 'ARC:Rotted Ones, Lay Siege:rotted ones, lay siege').
card_rarity('rotted ones, lay siege'/'ARC', 'Common').
card_artist('rotted ones, lay siege'/'ARC', 'Steve Prescott').
card_number('rotted ones, lay siege'/'ARC', '38').
card_flavor_text('rotted ones, lay siege'/'ARC', '\"Don\'t come back until you\'ve got brains in your teeth.\"').
card_multiverse_id('rotted ones, lay siege'/'ARC', '212583').

card_in_set('ryusei, the falling star', 'ARC').
card_original_type('ryusei, the falling star'/'ARC', 'Legendary Creature — Dragon Spirit').
card_original_text('ryusei, the falling star'/'ARC', 'Flying\nWhen Ryusei, the Falling Star is put into a graveyard from the battlefield, it deals 5 damage to each creature without flying.').
card_image_name('ryusei, the falling star'/'ARC', 'ryusei, the falling star').
card_uid('ryusei, the falling star'/'ARC', 'ARC:Ryusei, the Falling Star:ryusei, the falling star').
card_rarity('ryusei, the falling star'/'ARC', 'Rare').
card_artist('ryusei, the falling star'/'ARC', 'Nottsuo').
card_number('ryusei, the falling star'/'ARC', '45').
card_multiverse_id('ryusei, the falling star'/'ARC', '220471').

card_in_set('sakura-tribe elder', 'ARC').
card_original_type('sakura-tribe elder'/'ARC', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'ARC', 'Sacrifice Sakura-Tribe Elder: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.').
card_image_name('sakura-tribe elder'/'ARC', 'sakura-tribe elder').
card_uid('sakura-tribe elder'/'ARC', 'ARC:Sakura-Tribe Elder:sakura-tribe elder').
card_rarity('sakura-tribe elder'/'ARC', 'Common').
card_artist('sakura-tribe elder'/'ARC', 'Carl Critchlow').
card_number('sakura-tribe elder'/'ARC', '68').
card_flavor_text('sakura-tribe elder'/'ARC', 'Slain orochi warriors were buried with a tree sapling so they would become a part of the forest after death.').
card_multiverse_id('sakura-tribe elder'/'ARC', '220582').

card_in_set('sanctum gargoyle', 'ARC').
card_original_type('sanctum gargoyle'/'ARC', 'Artifact Creature — Gargoyle').
card_original_text('sanctum gargoyle'/'ARC', 'Flying\nWhen Sanctum Gargoyle enters the battlefield, you may return target artifact card from your graveyard to your hand.').
card_image_name('sanctum gargoyle'/'ARC', 'sanctum gargoyle').
card_uid('sanctum gargoyle'/'ARC', 'ARC:Sanctum Gargoyle:sanctum gargoyle').
card_rarity('sanctum gargoyle'/'ARC', 'Common').
card_artist('sanctum gargoyle'/'ARC', 'Shelly Wan').
card_number('sanctum gargoyle'/'ARC', '5').
card_flavor_text('sanctum gargoyle'/'ARC', 'As their supplies of etherium dwindled, mechanists sent gargoyles farther and farther afield in search of salvage.').
card_multiverse_id('sanctum gargoyle'/'ARC', '220462').

card_in_set('savage twister', 'ARC').
card_original_type('savage twister'/'ARC', 'Sorcery').
card_original_text('savage twister'/'ARC', 'Savage Twister deals X damage to each creature.').
card_image_name('savage twister'/'ARC', 'savage twister').
card_uid('savage twister'/'ARC', 'ARC:Savage Twister:savage twister').
card_rarity('savage twister'/'ARC', 'Uncommon').
card_artist('savage twister'/'ARC', 'Luca Zontini').
card_number('savage twister'/'ARC', '93').
card_flavor_text('savage twister'/'ARC', '\"Nature is the ultimate mindless destroyer, capable of power and ferocity no army can match, and the Gruul follow its example.\"\n—Trigori, Azorius senator').
card_multiverse_id('savage twister'/'ARC', '220505').

card_in_set('scion of darkness', 'ARC').
card_original_type('scion of darkness'/'ARC', 'Creature — Avatar').
card_original_text('scion of darkness'/'ARC', 'Trample\nWhenever Scion of Darkness deals combat damage to a player, you may put target creature card from that player\'s graveyard onto the battlefield under your control.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_image_name('scion of darkness'/'ARC', 'scion of darkness').
card_uid('scion of darkness'/'ARC', 'ARC:Scion of Darkness:scion of darkness').
card_rarity('scion of darkness'/'ARC', 'Rare').
card_artist('scion of darkness'/'ARC', 'Mark Zug').
card_number('scion of darkness'/'ARC', '23').
card_multiverse_id('scion of darkness'/'ARC', '220588').

card_in_set('secluded steppe', 'ARC').
card_original_type('secluded steppe'/'ARC', 'Land').
card_original_text('secluded steppe'/'ARC', 'Secluded Steppe enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'ARC', 'secluded steppe').
card_uid('secluded steppe'/'ARC', 'ARC:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'ARC', 'Common').
card_artist('secluded steppe'/'ARC', 'Heather Hudson').
card_number('secluded steppe'/'ARC', '133').
card_multiverse_id('secluded steppe'/'ARC', '220492').

card_in_set('seething song', 'ARC').
card_original_type('seething song'/'ARC', 'Instant').
card_original_text('seething song'/'ARC', 'Add {R}{R}{R}{R}{R} to your mana pool.').
card_image_name('seething song'/'ARC', 'seething song').
card_uid('seething song'/'ARC', 'ARC:Seething Song:seething song').
card_rarity('seething song'/'ARC', 'Common').
card_artist('seething song'/'ARC', 'Martina Pilcerova').
card_number('seething song'/'ARC', '46').
card_flavor_text('seething song'/'ARC', 'Vulshok mana rituals echo the day when the red sun burst through Mirrodin\'s surface to take its place in the heavens.').
card_multiverse_id('seething song'/'ARC', '220561').

card_in_set('selesnya guildmage', 'ARC').
card_original_type('selesnya guildmage'/'ARC', 'Creature — Elf Wizard').
card_original_text('selesnya guildmage'/'ARC', '{3}{G}: Put a 1/1 green Saproling creature token onto the battlefield.\n{3}{W}: Creatures you control get +1/+1 until end of turn.').
card_image_name('selesnya guildmage'/'ARC', 'selesnya guildmage').
card_uid('selesnya guildmage'/'ARC', 'ARC:Selesnya Guildmage:selesnya guildmage').
card_rarity('selesnya guildmage'/'ARC', 'Uncommon').
card_artist('selesnya guildmage'/'ARC', 'Mark Zug').
card_number('selesnya guildmage'/'ARC', '94').
card_multiverse_id('selesnya guildmage'/'ARC', '220549').

card_in_set('shinen of life\'s roar', 'ARC').
card_original_type('shinen of life\'s roar'/'ARC', 'Creature — Spirit').
card_original_text('shinen of life\'s roar'/'ARC', 'All creatures able to block Shinen of Life\'s Roar do so.\nChannel — {2}{G}{G}, Discard Shinen of Life\'s Roar: All creatures able to block target creature this turn do so.').
card_image_name('shinen of life\'s roar'/'ARC', 'shinen of life\'s roar').
card_uid('shinen of life\'s roar'/'ARC', 'ARC:Shinen of Life\'s Roar:shinen of life\'s roar').
card_rarity('shinen of life\'s roar'/'ARC', 'Common').
card_artist('shinen of life\'s roar'/'ARC', 'Matt Cavotta').
card_number('shinen of life\'s roar'/'ARC', '69').
card_multiverse_id('shinen of life\'s roar'/'ARC', '220587').

card_in_set('shriekmaw', 'ARC').
card_original_type('shriekmaw'/'ARC', 'Creature — Elemental').
card_original_text('shriekmaw'/'ARC', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen Shriekmaw enters the battlefield, destroy target nonartifact, nonblack creature.\nEvoke {1}{B} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('shriekmaw'/'ARC', 'shriekmaw').
card_uid('shriekmaw'/'ARC', 'ARC:Shriekmaw:shriekmaw').
card_rarity('shriekmaw'/'ARC', 'Uncommon').
card_artist('shriekmaw'/'ARC', 'Steve Prescott').
card_number('shriekmaw'/'ARC', '24').
card_multiverse_id('shriekmaw'/'ARC', '220572').

card_in_set('sign in blood', 'ARC').
card_original_type('sign in blood'/'ARC', 'Sorcery').
card_original_text('sign in blood'/'ARC', 'Target player draws two cards and loses 2 life.').
card_image_name('sign in blood'/'ARC', 'sign in blood').
card_uid('sign in blood'/'ARC', 'ARC:Sign in Blood:sign in blood').
card_rarity('sign in blood'/'ARC', 'Common').
card_artist('sign in blood'/'ARC', 'Howard Lyon').
card_number('sign in blood'/'ARC', '25').
card_flavor_text('sign in blood'/'ARC', '\"You know I accept only one currency here, and yet you have sought me out. Why now do you hesitate?\"\n—Xathrid demon').
card_multiverse_id('sign in blood'/'ARC', '220480').

card_in_set('skirk commando', 'ARC').
card_original_type('skirk commando'/'ARC', 'Creature — Goblin').
card_original_text('skirk commando'/'ARC', 'Whenever Skirk Commando deals combat damage to a player, you may have it deal 2 damage to target creature that player controls.\nMorph {2}{R} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_image_name('skirk commando'/'ARC', 'skirk commando').
card_uid('skirk commando'/'ARC', 'ARC:Skirk Commando:skirk commando').
card_rarity('skirk commando'/'ARC', 'Common').
card_artist('skirk commando'/'ARC', 'Dave Dorman').
card_number('skirk commando'/'ARC', '47').
card_multiverse_id('skirk commando'/'ARC', '220493').

card_in_set('skirk marauder', 'ARC').
card_original_type('skirk marauder'/'ARC', 'Creature — Goblin').
card_original_text('skirk marauder'/'ARC', 'Morph {2}{R} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Skirk Marauder is turned face up, it deals 2 damage to target creature or player.').
card_image_name('skirk marauder'/'ARC', 'skirk marauder').
card_uid('skirk marauder'/'ARC', 'ARC:Skirk Marauder:skirk marauder').
card_rarity('skirk marauder'/'ARC', 'Common').
card_artist('skirk marauder'/'ARC', 'Pete Venters').
card_number('skirk marauder'/'ARC', '48').
card_multiverse_id('skirk marauder'/'ARC', '220466').

card_in_set('skullcage', 'ARC').
card_original_type('skullcage'/'ARC', 'Artifact').
card_original_text('skullcage'/'ARC', 'At the beginning of each opponent\'s upkeep, Skullcage deals 2 damage to that player unless he or she has exactly three or exactly four cards in hand.').
card_image_name('skullcage'/'ARC', 'skullcage').
card_uid('skullcage'/'ARC', 'ARC:Skullcage:skullcage').
card_rarity('skullcage'/'ARC', 'Uncommon').
card_artist('skullcage'/'ARC', 'Ron Spencer').
card_number('skullcage'/'ARC', '115').
card_flavor_text('skullcage'/'ARC', 'Only a focused mind can survive it.').
card_multiverse_id('skullcage'/'ARC', '220537').

card_in_set('sorcerer\'s strongbox', 'ARC').
card_original_type('sorcerer\'s strongbox'/'ARC', 'Artifact').
card_original_text('sorcerer\'s strongbox'/'ARC', '{2}, {T}: Flip a coin. If you win the flip, sacrifice Sorcerer\'s Strongbox and draw three cards.').
card_first_print('sorcerer\'s strongbox', 'ARC').
card_image_name('sorcerer\'s strongbox'/'ARC', 'sorcerer\'s strongbox').
card_uid('sorcerer\'s strongbox'/'ARC', 'ARC:Sorcerer\'s Strongbox:sorcerer\'s strongbox').
card_rarity('sorcerer\'s strongbox'/'ARC', 'Uncommon').
card_artist('sorcerer\'s strongbox'/'ARC', 'Chuck Lukacs').
card_number('sorcerer\'s strongbox'/'ARC', '116').
card_flavor_text('sorcerer\'s strongbox'/'ARC', 'Simun the Quiet filled the chest with his most precious thoughts. But in a fit of paranoia, he locked up the memory of where he hid the key.').
card_multiverse_id('sorcerer\'s strongbox'/'ARC', '222702').

card_in_set('spider umbra', 'ARC').
card_original_type('spider umbra'/'ARC', 'Enchantment — Aura').
card_original_text('spider umbra'/'ARC', 'Enchant creature\nEnchanted creature gets +1/+1 and has reach. (It can block creatures with flying.)\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_image_name('spider umbra'/'ARC', 'spider umbra').
card_uid('spider umbra'/'ARC', 'ARC:Spider Umbra:spider umbra').
card_rarity('spider umbra'/'ARC', 'Common').
card_artist('spider umbra'/'ARC', 'Christopher Moeller').
card_number('spider umbra'/'ARC', '70').
card_multiverse_id('spider umbra'/'ARC', '220555').

card_in_set('spin into myth', 'ARC').
card_original_type('spin into myth'/'ARC', 'Instant').
card_original_text('spin into myth'/'ARC', 'Put target creature on top of its owner\'s library, then fateseal 2. (To fateseal 2, look at the top two cards of an opponent\'s library, then put any number of them on the bottom of that player\'s library and the rest on top in any order.)').
card_image_name('spin into myth'/'ARC', 'spin into myth').
card_uid('spin into myth'/'ARC', 'ARC:Spin into Myth:spin into myth').
card_rarity('spin into myth'/'ARC', 'Uncommon').
card_artist('spin into myth'/'ARC', 'David Day').
card_number('spin into myth'/'ARC', '8').
card_multiverse_id('spin into myth'/'ARC', '220497').

card_in_set('sun droplet', 'ARC').
card_original_type('sun droplet'/'ARC', 'Artifact').
card_original_text('sun droplet'/'ARC', 'Whenever you\'re dealt damage, put that many charge counters on Sun Droplet.\nAt the beginning of each upkeep, you may remove a charge counter from Sun Droplet. If you do, you gain 1 life.').
card_image_name('sun droplet'/'ARC', 'sun droplet').
card_uid('sun droplet'/'ARC', 'ARC:Sun Droplet:sun droplet').
card_rarity('sun droplet'/'ARC', 'Uncommon').
card_artist('sun droplet'/'ARC', 'Greg Hildebrandt').
card_number('sun droplet'/'ARC', '117').
card_multiverse_id('sun droplet'/'ARC', '220530').

card_in_set('sundering titan', 'ARC').
card_original_type('sundering titan'/'ARC', 'Artifact Creature — Golem').
card_original_text('sundering titan'/'ARC', 'When Sundering Titan enters the battlefield or leaves the battlefield, choose a land of each basic land type, then destroy those lands.').
card_image_name('sundering titan'/'ARC', 'sundering titan').
card_uid('sundering titan'/'ARC', 'ARC:Sundering Titan:sundering titan').
card_rarity('sundering titan'/'ARC', 'Rare').
card_artist('sundering titan'/'ARC', 'Jim Murray').
card_number('sundering titan'/'ARC', '118').
card_multiverse_id('sundering titan'/'ARC', '220533').

card_in_set('surrender your thoughts', 'ARC').
card_original_type('surrender your thoughts'/'ARC', 'Scheme').
card_original_text('surrender your thoughts'/'ARC', 'When you set this scheme in motion, target opponent chooses self or others. If that player chooses self, he or she discards four cards. If the player chooses others, each of your other opponents discards two cards.').
card_first_print('surrender your thoughts', 'ARC').
card_image_name('surrender your thoughts'/'ARC', 'surrender your thoughts').
card_uid('surrender your thoughts'/'ARC', 'ARC:Surrender Your Thoughts:surrender your thoughts').
card_rarity('surrender your thoughts'/'ARC', 'Common').
card_artist('surrender your thoughts'/'ARC', 'Howard Lyon').
card_number('surrender your thoughts'/'ARC', '39').
card_flavor_text('surrender your thoughts'/'ARC', '\"Or, if you prefer, just surrender.\"').
card_multiverse_id('surrender your thoughts'/'ARC', '212598').

card_in_set('swamp', 'ARC').
card_original_type('swamp'/'ARC', 'Basic Land — Swamp').
card_original_text('swamp'/'ARC', 'B').
card_image_name('swamp'/'ARC', 'swamp1').
card_uid('swamp'/'ARC', 'ARC:Swamp:swamp1').
card_rarity('swamp'/'ARC', 'Basic Land').
card_artist('swamp'/'ARC', 'Susan Van Camp').
card_number('swamp'/'ARC', '142').
card_multiverse_id('swamp'/'ARC', '221311').

card_in_set('swamp', 'ARC').
card_original_type('swamp'/'ARC', 'Basic Land — Swamp').
card_original_text('swamp'/'ARC', 'B').
card_image_name('swamp'/'ARC', 'swamp2').
card_uid('swamp'/'ARC', 'ARC:Swamp:swamp2').
card_rarity('swamp'/'ARC', 'Basic Land').
card_artist('swamp'/'ARC', 'Susan Van Camp').
card_number('swamp'/'ARC', '143').
card_multiverse_id('swamp'/'ARC', '221312').

card_in_set('swamp', 'ARC').
card_original_type('swamp'/'ARC', 'Basic Land — Swamp').
card_original_text('swamp'/'ARC', 'B').
card_image_name('swamp'/'ARC', 'swamp3').
card_uid('swamp'/'ARC', 'ARC:Swamp:swamp3').
card_rarity('swamp'/'ARC', 'Basic Land').
card_artist('swamp'/'ARC', 'Susan Van Camp').
card_number('swamp'/'ARC', '144').
card_multiverse_id('swamp'/'ARC', '221313').

card_in_set('synod centurion', 'ARC').
card_original_type('synod centurion'/'ARC', 'Artifact Creature — Construct').
card_original_text('synod centurion'/'ARC', 'When you control no other artifacts, sacrifice Synod Centurion.').
card_image_name('synod centurion'/'ARC', 'synod centurion').
card_uid('synod centurion'/'ARC', 'ARC:Synod Centurion:synod centurion').
card_rarity('synod centurion'/'ARC', 'Uncommon').
card_artist('synod centurion'/'ARC', 'Kev Walker').
card_number('synod centurion'/'ARC', '119').
card_flavor_text('synod centurion'/'ARC', '\"We order them to stand, they stand. We order them to wait, they wait. We order them to die, they die.\"\n—Pontifex, elder researcher').
card_multiverse_id('synod centurion'/'ARC', '220538').

card_in_set('synod sanctum', 'ARC').
card_original_type('synod sanctum'/'ARC', 'Artifact').
card_original_text('synod sanctum'/'ARC', '{2}, {T}: Exile target permanent you control.\n{2}, Sacrifice Synod Sanctum: Return all cards exiled with Synod Sanctum to the battlefield under your control.').
card_image_name('synod sanctum'/'ARC', 'synod sanctum').
card_uid('synod sanctum'/'ARC', 'ARC:Synod Sanctum:synod sanctum').
card_rarity('synod sanctum'/'ARC', 'Uncommon').
card_artist('synod sanctum'/'ARC', 'Dana Knutson').
card_number('synod sanctum'/'ARC', '120').
card_multiverse_id('synod sanctum'/'ARC', '220531').

card_in_set('taurean mauler', 'ARC').
card_original_type('taurean mauler'/'ARC', 'Creature — Shapeshifter').
card_original_text('taurean mauler'/'ARC', 'Changeling (This card is every creature type at all times.)\nWhenever an opponent casts a spell, you may put a +1/+1 counter on Taurean Mauler.').
card_image_name('taurean mauler'/'ARC', 'taurean mauler').
card_uid('taurean mauler'/'ARC', 'ARC:Taurean Mauler:taurean mauler').
card_rarity('taurean mauler'/'ARC', 'Rare').
card_artist('taurean mauler'/'ARC', 'Dominick Domingo').
card_number('taurean mauler'/'ARC', '49').
card_flavor_text('taurean mauler'/'ARC', 'The power of a waterfall. The fury of an avalanche. The intellect of a gale-force wind.').
card_multiverse_id('taurean mauler'/'ARC', '220453').

card_in_set('terminate', 'ARC').
card_original_type('terminate'/'ARC', 'Instant').
card_original_text('terminate'/'ARC', 'Destroy target creature. It can\'t be regenerated.').
card_image_name('terminate'/'ARC', 'terminate').
card_uid('terminate'/'ARC', 'ARC:Terminate:terminate').
card_rarity('terminate'/'ARC', 'Common').
card_artist('terminate'/'ARC', 'Wayne Reynolds').
card_number('terminate'/'ARC', '95').
card_flavor_text('terminate'/'ARC', '\"I\'ve seen death before. My mother succumbing to illness, my comrades bleeding on the battlefield . . . But I\'d never seen anything as dreadful as that.\"\n—Taani, berserker of Etlan').
card_multiverse_id('terminate'/'ARC', '220524').

card_in_set('terramorphic expanse', 'ARC').
card_original_type('terramorphic expanse'/'ARC', 'Land').
card_original_text('terramorphic expanse'/'ARC', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'ARC', 'terramorphic expanse').
card_uid('terramorphic expanse'/'ARC', 'ARC:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'ARC', 'Common').
card_artist('terramorphic expanse'/'ARC', 'Dan Scott').
card_number('terramorphic expanse'/'ARC', '134').
card_multiverse_id('terramorphic expanse'/'ARC', '220481').

card_in_set('the dead shall serve', 'ARC').
card_original_type('the dead shall serve'/'ARC', 'Scheme').
card_original_text('the dead shall serve'/'ARC', 'When you set this scheme in motion, for each opponent, put up to one target creature card from that player\'s graveyard onto the battlefield under your control. Each of those creatures attacks its owner each combat if able.').
card_first_print('the dead shall serve', 'ARC').
card_image_name('the dead shall serve'/'ARC', 'the dead shall serve').
card_uid('the dead shall serve'/'ARC', 'ARC:The Dead Shall Serve:the dead shall serve').
card_rarity('the dead shall serve'/'ARC', 'Common').
card_artist('the dead shall serve'/'ARC', 'Jesper Ejsing').
card_number('the dead shall serve'/'ARC', '7').
card_flavor_text('the dead shall serve'/'ARC', '\"Tell your old master I said hello.\"').
card_multiverse_id('the dead shall serve'/'ARC', '212613').

card_in_set('the fate of the flammable', 'ARC').
card_original_type('the fate of the flammable'/'ARC', 'Scheme').
card_original_text('the fate of the flammable'/'ARC', 'When you set this scheme in motion, target opponent chooses self or others. If that player chooses self, this scheme deals 6 damage to him or her. If the player chooses others, this scheme deals 3 damage to each of your other opponents.').
card_first_print('the fate of the flammable', 'ARC').
card_image_name('the fate of the flammable'/'ARC', 'the fate of the flammable').
card_uid('the fate of the flammable'/'ARC', 'ARC:The Fate of the Flammable:the fate of the flammable').
card_rarity('the fate of the flammable'/'ARC', 'Common').
card_artist('the fate of the flammable'/'ARC', 'Paul Bonner').
card_number('the fate of the flammable'/'ARC', '13').
card_multiverse_id('the fate of the flammable'/'ARC', '212576').

card_in_set('the iron guardian stirs', 'ARC').
card_original_type('the iron guardian stirs'/'ARC', 'Scheme').
card_original_text('the iron guardian stirs'/'ARC', 'When you set this scheme in motion, put a 4/6 colorless Golem artifact creature token onto the battlefield.').
card_first_print('the iron guardian stirs', 'ARC').
card_image_name('the iron guardian stirs'/'ARC', 'the iron guardian stirs').
card_uid('the iron guardian stirs'/'ARC', 'ARC:The Iron Guardian Stirs:the iron guardian stirs').
card_rarity('the iron guardian stirs'/'ARC', 'Common').
card_artist('the iron guardian stirs'/'ARC', 'Jesper Ejsing').
card_number('the iron guardian stirs'/'ARC', '22').
card_flavor_text('the iron guardian stirs'/'ARC', '\"It would be disappointing if you were unable to overcome this simplest of defenses. Hilarious, but disappointing.\"').
card_multiverse_id('the iron guardian stirs'/'ARC', '212594').

card_in_set('the pieces are coming together', 'ARC').
card_original_type('the pieces are coming together'/'ARC', 'Scheme').
card_original_text('the pieces are coming together'/'ARC', 'When you set this scheme in motion, draw two cards. Artifact spells you cast this turn cost {2} less to cast.').
card_first_print('the pieces are coming together', 'ARC').
card_image_name('the pieces are coming together'/'ARC', 'the pieces are coming together').
card_uid('the pieces are coming together'/'ARC', 'ARC:The Pieces Are Coming Together:the pieces are coming together').
card_rarity('the pieces are coming together'/'ARC', 'Common').
card_artist('the pieces are coming together'/'ARC', 'Ralph Horsley').
card_number('the pieces are coming together'/'ARC', '35').
card_flavor_text('the pieces are coming together'/'ARC', '\"Cretins build machines. I construct destiny.\"').
card_multiverse_id('the pieces are coming together'/'ARC', '212604').

card_in_set('the very soil shall shake', 'ARC').
card_original_type('the very soil shall shake'/'ARC', 'Ongoing Scheme').
card_original_text('the very soil shall shake'/'ARC', '(An ongoing scheme remains face up until it\'s abandoned.)\nCreatures you control get +2/+2 and have trample.\nWhen a creature you control is put into a graveyard from the battlefield, abandon this scheme.').
card_first_print('the very soil shall shake', 'ARC').
card_image_name('the very soil shall shake'/'ARC', 'the very soil shall shake').
card_uid('the very soil shall shake'/'ARC', 'ARC:The Very Soil Shall Shake:the very soil shall shake').
card_rarity('the very soil shall shake'/'ARC', 'Common').
card_artist('the very soil shall shake'/'ARC', 'Val Mayerik').
card_number('the very soil shall shake'/'ARC', '41').
card_multiverse_id('the very soil shall shake'/'ARC', '212615').

card_in_set('thelonite hermit', 'ARC').
card_original_type('thelonite hermit'/'ARC', 'Creature — Elf Shaman').
card_original_text('thelonite hermit'/'ARC', 'Saproling creatures get +1/+1.\nMorph {3}{G}{G} (You may cast this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Thelonite Hermit is turned face up, put four 1/1 green Saproling creature tokens onto the battlefield.').
card_image_name('thelonite hermit'/'ARC', 'thelonite hermit').
card_uid('thelonite hermit'/'ARC', 'ARC:Thelonite Hermit:thelonite hermit').
card_rarity('thelonite hermit'/'ARC', 'Rare').
card_artist('thelonite hermit'/'ARC', 'Chippy').
card_number('thelonite hermit'/'ARC', '71').
card_multiverse_id('thelonite hermit'/'ARC', '220558').

card_in_set('thran dynamo', 'ARC').
card_original_type('thran dynamo'/'ARC', 'Artifact').
card_original_text('thran dynamo'/'ARC', '{T}: Add {3} to your mana pool.').
card_image_name('thran dynamo'/'ARC', 'thran dynamo').
card_uid('thran dynamo'/'ARC', 'ARC:Thran Dynamo:thran dynamo').
card_rarity('thran dynamo'/'ARC', 'Uncommon').
card_artist('thran dynamo'/'ARC', 'Ron Spears').
card_number('thran dynamo'/'ARC', '121').
card_flavor_text('thran dynamo'/'ARC', 'Urza\'s metathran children were conceived, birthed, and nurtured by an integrated system of machines.').
card_multiverse_id('thran dynamo'/'ARC', '220506').

card_in_set('thunderstaff', 'ARC').
card_original_type('thunderstaff'/'ARC', 'Artifact').
card_original_text('thunderstaff'/'ARC', 'If Thunderstaff is untapped and a creature would deal combat damage to you, prevent 1 of that damage.\n{2}, {T}: Attacking creatures get +1/+0 until end of turn.').
card_image_name('thunderstaff'/'ARC', 'thunderstaff').
card_uid('thunderstaff'/'ARC', 'ARC:Thunderstaff:thunderstaff').
card_rarity('thunderstaff'/'ARC', 'Uncommon').
card_artist('thunderstaff'/'ARC', 'Kaja Foglio').
card_number('thunderstaff'/'ARC', '122').
card_multiverse_id('thunderstaff'/'ARC', '220581').

card_in_set('tooth, claw, and tail', 'ARC').
card_original_type('tooth, claw, and tail'/'ARC', 'Scheme').
card_original_text('tooth, claw, and tail'/'ARC', 'When you set this scheme in motion, destroy up to three target nonland permanents.').
card_first_print('tooth, claw, and tail', 'ARC').
card_image_name('tooth, claw, and tail'/'ARC', 'tooth, claw, and tail').
card_uid('tooth, claw, and tail'/'ARC', 'ARC:Tooth, Claw, and Tail:tooth, claw, and tail').
card_rarity('tooth, claw, and tail'/'ARC', 'Common').
card_artist('tooth, claw, and tail'/'ARC', 'Paul Bonner').
card_number('tooth, claw, and tail'/'ARC', '40').
card_flavor_text('tooth, claw, and tail'/'ARC', '\"Silence your wailing. This is but a taste of the destruction my pets have in store.\"').
card_multiverse_id('tooth, claw, and tail'/'ARC', '212590').

card_in_set('torrent of souls', 'ARC').
card_original_type('torrent of souls'/'ARC', 'Sorcery').
card_original_text('torrent of souls'/'ARC', 'Return up to one target creature card from your graveyard to the battlefield if {B} was spent to cast Torrent of Souls. Creatures target player controls get +2/+0 and gain haste until end of turn if {R} was spent to cast Torrent of Souls. (Do both if {B}{R} was spent.)').
card_image_name('torrent of souls'/'ARC', 'torrent of souls').
card_uid('torrent of souls'/'ARC', 'ARC:Torrent of Souls:torrent of souls').
card_rarity('torrent of souls'/'ARC', 'Uncommon').
card_artist('torrent of souls'/'ARC', 'Ian Edward Ameling').
card_number('torrent of souls'/'ARC', '96').
card_multiverse_id('torrent of souls'/'ARC', '220547').

card_in_set('tranquil thicket', 'ARC').
card_original_type('tranquil thicket'/'ARC', 'Land').
card_original_text('tranquil thicket'/'ARC', 'Tranquil Thicket enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'ARC', 'tranquil thicket').
card_uid('tranquil thicket'/'ARC', 'ARC:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'ARC', 'Common').
card_artist('tranquil thicket'/'ARC', 'Heather Hudson').
card_number('tranquil thicket'/'ARC', '135').
card_multiverse_id('tranquil thicket'/'ARC', '220494').

card_in_set('twisted abomination', 'ARC').
card_original_type('twisted abomination'/'ARC', 'Creature — Zombie Mutant').
card_original_text('twisted abomination'/'ARC', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('twisted abomination'/'ARC', 'twisted abomination').
card_uid('twisted abomination'/'ARC', 'ARC:Twisted Abomination:twisted abomination').
card_rarity('twisted abomination'/'ARC', 'Common').
card_artist('twisted abomination'/'ARC', 'Daren Bader').
card_number('twisted abomination'/'ARC', '26').
card_multiverse_id('twisted abomination'/'ARC', '220564').

card_in_set('two-headed dragon', 'ARC').
card_original_type('two-headed dragon'/'ARC', 'Creature — Dragon').
card_original_text('two-headed dragon'/'ARC', 'Flying\n{1}{R}: Two-Headed Dragon gets +2/+0 until end of turn.\nTwo-Headed Dragon can\'t be blocked except by two or more creatures.\nTwo-Headed Dragon can block an additional creature.').
card_image_name('two-headed dragon'/'ARC', 'two-headed dragon').
card_uid('two-headed dragon'/'ARC', 'ARC:Two-Headed Dragon:two-headed dragon').
card_rarity('two-headed dragon'/'ARC', 'Rare').
card_artist('two-headed dragon'/'ARC', 'Sam Wood').
card_number('two-headed dragon'/'ARC', '50').
card_multiverse_id('two-headed dragon'/'ARC', '220472').

card_in_set('unbender tine', 'ARC').
card_original_type('unbender tine'/'ARC', 'Artifact').
card_original_text('unbender tine'/'ARC', '{T}: Untap another target permanent.').
card_image_name('unbender tine'/'ARC', 'unbender tine').
card_uid('unbender tine'/'ARC', 'ARC:Unbender Tine:unbender tine').
card_rarity('unbender tine'/'ARC', 'Uncommon').
card_artist('unbender tine'/'ARC', 'Greg Hildebrandt').
card_number('unbender tine'/'ARC', '97').
card_flavor_text('unbender tine'/'ARC', 'Creations made in madness may contain great power. The path to that power is as labyrinthine as the creator\'s mind.').
card_multiverse_id('unbender tine'/'ARC', '220552').

card_in_set('unmake', 'ARC').
card_original_type('unmake'/'ARC', 'Instant').
card_original_text('unmake'/'ARC', 'Exile target creature.').
card_image_name('unmake'/'ARC', 'unmake').
card_uid('unmake'/'ARC', 'ARC:Unmake:unmake').
card_rarity('unmake'/'ARC', 'Common').
card_artist('unmake'/'ARC', 'Steven Belledin').
card_number('unmake'/'ARC', '98').
card_flavor_text('unmake'/'ARC', 'A gwyllion\'s favorite trap is the vanity mirror. A bewitched piece of glass traps the looker\'s soul and does away with the body.').
card_multiverse_id('unmake'/'ARC', '220574').

card_in_set('urborg syphon-mage', 'ARC').
card_original_type('urborg syphon-mage'/'ARC', 'Creature — Human Spellshaper').
card_original_text('urborg syphon-mage'/'ARC', '{2}{B}, {T}, Discard a card: Each other player loses 2 life. You gain life equal to the life lost this way.').
card_image_name('urborg syphon-mage'/'ARC', 'urborg syphon-mage').
card_uid('urborg syphon-mage'/'ARC', 'ARC:Urborg Syphon-Mage:urborg syphon-mage').
card_rarity('urborg syphon-mage'/'ARC', 'Common').
card_artist('urborg syphon-mage'/'ARC', 'Greg Staples').
card_number('urborg syphon-mage'/'ARC', '27').
card_flavor_text('urborg syphon-mage'/'ARC', '\"I have become a gourmet of sorts. Each soul has its own distinctive flavor. The art is in inviting the right company to the feast.\"').
card_multiverse_id('urborg syphon-mage'/'ARC', '220559').

card_in_set('vampiric dragon', 'ARC').
card_original_type('vampiric dragon'/'ARC', 'Creature — Vampire Dragon').
card_original_text('vampiric dragon'/'ARC', 'Flying\nWhenever a creature dealt damage by Vampiric Dragon this turn is put into a graveyard, put a +1/+1 counter on Vampiric Dragon.\n{1}{R}: Vampiric Dragon deals 1 damage to target creature.').
card_image_name('vampiric dragon'/'ARC', 'vampiric dragon').
card_uid('vampiric dragon'/'ARC', 'ARC:Vampiric Dragon:vampiric dragon').
card_rarity('vampiric dragon'/'ARC', 'Rare').
card_artist('vampiric dragon'/'ARC', 'Gary Ruddell').
card_number('vampiric dragon'/'ARC', '99').
card_multiverse_id('vampiric dragon'/'ARC', '220578').

card_in_set('verdeloth the ancient', 'ARC').
card_original_type('verdeloth the ancient'/'ARC', 'Legendary Creature — Treefolk').
card_original_text('verdeloth the ancient'/'ARC', 'Kicker {X} (You may pay an additional {X} as you cast this spell.)\nSaproling creatures and other Treefolk creatures get +1/+1.\nWhen Verdeloth the Ancient enters the battlefield, if it was kicked, put X 1/1 green Saproling creature tokens onto the battlefield.').
card_image_name('verdeloth the ancient'/'ARC', 'verdeloth the ancient').
card_uid('verdeloth the ancient'/'ARC', 'ARC:Verdeloth the Ancient:verdeloth the ancient').
card_rarity('verdeloth the ancient'/'ARC', 'Rare').
card_artist('verdeloth the ancient'/'ARC', 'Daren Bader').
card_number('verdeloth the ancient'/'ARC', '72').
card_multiverse_id('verdeloth the ancient'/'ARC', '220565').

card_in_set('vitu-ghazi, the city-tree', 'ARC').
card_original_type('vitu-ghazi, the city-tree'/'ARC', 'Land').
card_original_text('vitu-ghazi, the city-tree'/'ARC', '{T}: Add {1} to your mana pool.\n{2}{G}{W}, {T}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('vitu-ghazi, the city-tree'/'ARC', 'vitu-ghazi, the city-tree').
card_uid('vitu-ghazi, the city-tree'/'ARC', 'ARC:Vitu-Ghazi, the City-Tree:vitu-ghazi, the city-tree').
card_rarity('vitu-ghazi, the city-tree'/'ARC', 'Uncommon').
card_artist('vitu-ghazi, the city-tree'/'ARC', 'Martina Pilcerova').
card_number('vitu-ghazi, the city-tree'/'ARC', '136').
card_flavor_text('vitu-ghazi, the city-tree'/'ARC', 'In the autumn, she casts her seeds across the streets below, and come spring, her children rise in service to the Conclave.').
card_multiverse_id('vitu-ghazi, the city-tree'/'ARC', '220550').

card_in_set('volcanic fallout', 'ARC').
card_original_type('volcanic fallout'/'ARC', 'Instant').
card_original_text('volcanic fallout'/'ARC', 'Volcanic Fallout can\'t be countered.\nVolcanic Fallout deals 2 damage to each creature and each player.').
card_image_name('volcanic fallout'/'ARC', 'volcanic fallout').
card_uid('volcanic fallout'/'ARC', 'ARC:Volcanic Fallout:volcanic fallout').
card_rarity('volcanic fallout'/'ARC', 'Uncommon').
card_artist('volcanic fallout'/'ARC', 'Zoltan Boros & Gabor Szikszai').
card_number('volcanic fallout'/'ARC', '51').
card_flavor_text('volcanic fallout'/'ARC', '\"How can we outrun the sky?\"\n—Hadran, sunseeder of Naya').
card_multiverse_id('volcanic fallout'/'ARC', '220512').

card_in_set('wall of roots', 'ARC').
card_original_type('wall of roots'/'ARC', 'Creature — Plant Wall').
card_original_text('wall of roots'/'ARC', 'Defender\nPut a -0/-1 counter on Wall of Roots: Add {G} to your mana pool. Activate this ability only once each turn.').
card_image_name('wall of roots'/'ARC', 'wall of roots').
card_uid('wall of roots'/'ARC', 'ARC:Wall of Roots:wall of roots').
card_rarity('wall of roots'/'ARC', 'Common').
card_artist('wall of roots'/'ARC', 'John Matson').
card_number('wall of roots'/'ARC', '73').
card_flavor_text('wall of roots'/'ARC', 'Sometimes the wise ones wove their magic into living plants; as the plant grew, so grew the magic.').
card_multiverse_id('wall of roots'/'ARC', '220566').

card_in_set('wane', 'ARC').
card_original_type('wane'/'ARC', 'Instant').
card_original_text('wane'/'ARC', 'Target creature gets +2/+2 until end of turn.\n//\nWane\n{W}\nInstant\nDestroy target enchantment.').
card_image_name('wane'/'ARC', 'waxwane').
card_uid('wane'/'ARC', 'ARC:Wane:waxwane').
card_rarity('wane'/'ARC', 'Uncommon').
card_artist('wane'/'ARC', 'Ben Thompson').
card_number('wane'/'ARC', '101b').
card_multiverse_id('wane'/'ARC', '220502').

card_in_set('watchwolf', 'ARC').
card_original_type('watchwolf'/'ARC', 'Creature — Wolf').
card_original_text('watchwolf'/'ARC', '').
card_image_name('watchwolf'/'ARC', 'watchwolf').
card_uid('watchwolf'/'ARC', 'ARC:Watchwolf:watchwolf').
card_rarity('watchwolf'/'ARC', 'Uncommon').
card_artist('watchwolf'/'ARC', 'Kev Walker').
card_number('watchwolf'/'ARC', '100').
card_flavor_text('watchwolf'/'ARC', 'Only in Ravnica do the wolves watch the flock.').
card_multiverse_id('watchwolf'/'ARC', '220551').

card_in_set('wax', 'ARC').
card_original_type('wax'/'ARC', 'Instant').
card_original_text('wax'/'ARC', 'Target creature gets +2/+2 until end of turn.\n//\nWane\n{W}\nInstant\nDestroy target enchantment.').
card_image_name('wax'/'ARC', 'waxwane').
card_uid('wax'/'ARC', 'ARC:Wax:waxwane').
card_rarity('wax'/'ARC', 'Uncommon').
card_artist('wax'/'ARC', 'Ben Thompson').
card_number('wax'/'ARC', '101a').
card_multiverse_id('wax'/'ARC', '220502').

card_in_set('which of you burns brightest?', 'ARC').
card_original_type('which of you burns brightest?'/'ARC', 'Scheme').
card_original_text('which of you burns brightest?'/'ARC', 'When you set this scheme in motion, you may pay {X}. If you do, this scheme deals X damage to target opponent and each creature he or she controls.').
card_first_print('which of you burns brightest?', 'ARC').
card_image_name('which of you burns brightest?'/'ARC', 'which of you burns brightest').
card_uid('which of you burns brightest?'/'ARC', 'ARC:Which of You Burns Brightest?:which of you burns brightest').
card_rarity('which of you burns brightest?'/'ARC', 'Common').
card_artist('which of you burns brightest?'/'ARC', 'Steve Prescott').
card_number('which of you burns brightest?'/'ARC', '42').
card_flavor_text('which of you burns brightest?'/'ARC', '\"Let that be a lesson to you, your family, and everyone you\'ve ever known.\"').
card_multiverse_id('which of you burns brightest?'/'ARC', '212605').

card_in_set('wickerbough elder', 'ARC').
card_original_type('wickerbough elder'/'ARC', 'Creature — Treefolk Shaman').
card_original_text('wickerbough elder'/'ARC', 'Wickerbough Elder enters the battlefield with a -1/-1 counter on it.\n{G}, Remove a -1/-1 counter from Wickerbough Elder: Destroy target artifact or enchantment.').
card_image_name('wickerbough elder'/'ARC', 'wickerbough elder').
card_uid('wickerbough elder'/'ARC', 'ARC:Wickerbough Elder:wickerbough elder').
card_rarity('wickerbough elder'/'ARC', 'Common').
card_artist('wickerbough elder'/'ARC', 'Jesper Ejsing').
card_number('wickerbough elder'/'ARC', '74').
card_flavor_text('wickerbough elder'/'ARC', '\"Living scarecrows make a mockery of the natural order. Dead ones make fine hats.\"').
card_multiverse_id('wickerbough elder'/'ARC', '220575').

card_in_set('yavimaya dryad', 'ARC').
card_original_type('yavimaya dryad'/'ARC', 'Creature — Dryad').
card_original_text('yavimaya dryad'/'ARC', 'Forestwalk\nWhen Yavimaya Dryad enters the battlefield, you may search your library for a Forest card and put it onto the battlefield tapped under target player\'s control. If you do, shuffle your library.').
card_image_name('yavimaya dryad'/'ARC', 'yavimaya dryad').
card_uid('yavimaya dryad'/'ARC', 'ARC:Yavimaya Dryad:yavimaya dryad').
card_rarity('yavimaya dryad'/'ARC', 'Uncommon').
card_artist('yavimaya dryad'/'ARC', 'Rebecca Guay').
card_number('yavimaya dryad'/'ARC', '75').
card_flavor_text('yavimaya dryad'/'ARC', 'The parched earth still shielded those few souls with the heart to call it home.').
card_multiverse_id('yavimaya dryad'/'ARC', '220560').

card_in_set('your fate is thrice sealed', 'ARC').
card_original_type('your fate is thrice sealed'/'ARC', 'Scheme').
card_original_text('your fate is thrice sealed'/'ARC', 'When you set this scheme in motion, reveal the top three cards of your library. Put all land cards revealed this way onto the battlefield and the rest into your hand.').
card_first_print('your fate is thrice sealed', 'ARC').
card_image_name('your fate is thrice sealed'/'ARC', 'your fate is thrice sealed').
card_uid('your fate is thrice sealed'/'ARC', 'ARC:Your Fate Is Thrice Sealed:your fate is thrice sealed').
card_rarity('your fate is thrice sealed'/'ARC', 'Common').
card_artist('your fate is thrice sealed'/'ARC', 'Steve Prescott').
card_number('your fate is thrice sealed'/'ARC', '43').
card_flavor_text('your fate is thrice sealed'/'ARC', '\"I can\'t abide laziness. Arise, forces of nature, and serve your master!\"').
card_multiverse_id('your fate is thrice sealed'/'ARC', '212618').

card_in_set('your puny minds cannot fathom', 'ARC').
card_original_type('your puny minds cannot fathom'/'ARC', 'Scheme').
card_original_text('your puny minds cannot fathom'/'ARC', 'When you set this scheme in motion, draw four cards. You have no maximum hand size until your next turn.').
card_first_print('your puny minds cannot fathom', 'ARC').
card_image_name('your puny minds cannot fathom'/'ARC', 'your puny minds cannot fathom').
card_uid('your puny minds cannot fathom'/'ARC', 'ARC:Your Puny Minds Cannot Fathom:your puny minds cannot fathom').
card_rarity('your puny minds cannot fathom'/'ARC', 'Common').
card_artist('your puny minds cannot fathom'/'ARC', 'Martina Pilcerova').
card_number('your puny minds cannot fathom'/'ARC', '44').
card_flavor_text('your puny minds cannot fathom'/'ARC', '\"I have seen far beyond this imperfect world, into a tomorrow that doesn\'t include you.\"').
card_multiverse_id('your puny minds cannot fathom'/'ARC', '212588').

card_in_set('your will is not your own', 'ARC').
card_original_type('your will is not your own'/'ARC', 'Scheme').
card_original_text('your will is not your own'/'ARC', 'When you set this scheme in motion, gain control of target creature an opponent controls until end of turn. Untap that creature. It gets +3/+3 and gains haste and trample until end of turn.').
card_first_print('your will is not your own', 'ARC').
card_image_name('your will is not your own'/'ARC', 'your will is not your own').
card_uid('your will is not your own'/'ARC', 'ARC:Your Will Is Not Your Own:your will is not your own').
card_rarity('your will is not your own'/'ARC', 'Common').
card_artist('your will is not your own'/'ARC', 'Alex Horley-Orlandelli').
card_number('your will is not your own'/'ARC', '45').
card_flavor_text('your will is not your own'/'ARC', '\"Fool. I\'ll show you how to use that thing.\"').
card_multiverse_id('your will is not your own'/'ARC', '212609').

card_in_set('zombie infestation', 'ARC').
card_original_type('zombie infestation'/'ARC', 'Enchantment').
card_original_text('zombie infestation'/'ARC', 'Discard two cards: Put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('zombie infestation'/'ARC', 'zombie infestation').
card_uid('zombie infestation'/'ARC', 'ARC:Zombie Infestation:zombie infestation').
card_rarity('zombie infestation'/'ARC', 'Uncommon').
card_artist('zombie infestation'/'ARC', 'Thomas M. Baxa').
card_number('zombie infestation'/'ARC', '28').
card_flavor_text('zombie infestation'/'ARC', 'The nomads\' funeral pyres are more practical than ceremonial.').
card_multiverse_id('zombie infestation'/'ARC', '220579').

card_in_set('zombify', 'ARC').
card_original_type('zombify'/'ARC', 'Sorcery').
card_original_text('zombify'/'ARC', 'Return target creature card from your graveyard to the battlefield.').
card_image_name('zombify'/'ARC', 'zombify').
card_uid('zombify'/'ARC', 'ARC:Zombify:zombify').
card_rarity('zombify'/'ARC', 'Uncommon').
card_artist('zombify'/'ARC', 'Mark Romanoski').
card_number('zombify'/'ARC', '29').
card_flavor_text('zombify'/'ARC', '\"The first birth celebrates life. The second birth mocks it.\"\n—Mystic elder').
card_multiverse_id('zombify'/'ARC', '220580').
