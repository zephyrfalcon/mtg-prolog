% Premium Deck Series: Graveborn

set('PD3').
set_name('PD3', 'Premium Deck Series: Graveborn').
set_release_date('PD3', '2011-11-18').
set_border('PD3', 'black').
set_type('PD3', 'premium deck').

card_in_set('animate dead', 'PD3').
card_original_type('animate dead'/'PD3', 'Enchantment — Aura').
card_original_text('animate dead'/'PD3', 'Enchant creature card in a graveyard\nWhen Animate Dead enters the battlefield, if it\'s on the battlefield, it loses \"enchant creature card in a graveyard\" and gains \"enchant creature put onto the battlefield with Animate Dead.\" Return enchanted creature card to the battlefield under your control and attach Animate Dead to it. When Animate Dead leaves the battlefield, that creature\'s controller sacrifices it.\nEnchanted creature gets -1/-0.').
card_image_name('animate dead'/'PD3', 'animate dead').
card_uid('animate dead'/'PD3', 'PD3:Animate Dead:animate dead').
card_rarity('animate dead'/'PD3', 'Uncommon').
card_artist('animate dead'/'PD3', 'Anthony Jones').
card_number('animate dead'/'PD3', '16').
card_multiverse_id('animate dead'/'PD3', '265167').

card_in_set('avatar of woe', 'PD3').
card_original_type('avatar of woe'/'PD3', 'Creature — Avatar').
card_original_text('avatar of woe'/'PD3', 'If there are ten or more creature cards total in all graveyards, Avatar of Woe costs {6} less to cast.\nFear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{T}: Destroy target creature. It can\'t be regenerated.').
card_image_name('avatar of woe'/'PD3', 'avatar of woe').
card_uid('avatar of woe'/'PD3', 'PD3:Avatar of Woe:avatar of woe').
card_rarity('avatar of woe'/'PD3', 'Rare').
card_artist('avatar of woe'/'PD3', 'rk post').
card_number('avatar of woe'/'PD3', '6').
card_multiverse_id('avatar of woe'/'PD3', '270448').

card_in_set('blazing archon', 'PD3').
card_original_type('blazing archon'/'PD3', 'Creature — Archon').
card_original_text('blazing archon'/'PD3', 'Flying\nCreatures can\'t attack you.').
card_image_name('blazing archon'/'PD3', 'blazing archon').
card_uid('blazing archon'/'PD3', 'PD3:Blazing Archon:blazing archon').
card_rarity('blazing archon'/'PD3', 'Rare').
card_artist('blazing archon'/'PD3', 'Zoltan Boros & Gabor Szikszai').
card_number('blazing archon'/'PD3', '11').
card_flavor_text('blazing archon'/'PD3', '\"Through the haze of battle I saw the glint of sun on golden mane, the sheen of glory clad in mail, and I dropped my sword and wept at the idiocy of war.\"\n—Dravin, Gruul deserter').
card_multiverse_id('blazing archon'/'PD3', '270449').

card_in_set('buried alive', 'PD3').
card_original_type('buried alive'/'PD3', 'Sorcery').
card_original_text('buried alive'/'PD3', 'Search your library for up to three creature cards and put them into your graveyard. Then shuffle your library.').
card_image_name('buried alive'/'PD3', 'buried alive').
card_uid('buried alive'/'PD3', 'PD3:Buried Alive:buried alive').
card_rarity('buried alive'/'PD3', 'Uncommon').
card_artist('buried alive'/'PD3', 'Greg Staples').
card_number('buried alive'/'PD3', '20').
card_flavor_text('buried alive'/'PD3', 'The scrape of shovels and the tumble of cold dirt soon muffled their pleas.').
card_multiverse_id('buried alive'/'PD3', '270455').

card_in_set('cabal therapy', 'PD3').
card_original_type('cabal therapy'/'PD3', 'Sorcery').
card_original_text('cabal therapy'/'PD3', 'Name a nonland card. Target player reveals his or her hand and discards all cards with that name.\nFlashback—Sacrifice a creature. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('cabal therapy'/'PD3', 'cabal therapy').
card_uid('cabal therapy'/'PD3', 'PD3:Cabal Therapy:cabal therapy').
card_rarity('cabal therapy'/'PD3', 'Uncommon').
card_artist('cabal therapy'/'PD3', 'Raymond Swanland').
card_number('cabal therapy'/'PD3', '12').
card_multiverse_id('cabal therapy'/'PD3', '265166').

card_in_set('crosis, the purger', 'PD3').
card_original_type('crosis, the purger'/'PD3', 'Legendary Creature — Dragon').
card_original_text('crosis, the purger'/'PD3', 'Flying\nWhenever Crosis, the Purger deals combat damage to a player, you may pay {2}{B}. If you do, choose a color, then that player reveals his or her hand and discards all cards of that color.').
card_image_name('crosis, the purger'/'PD3', 'crosis, the purger').
card_uid('crosis, the purger'/'PD3', 'PD3:Crosis, the Purger:crosis, the purger').
card_rarity('crosis, the purger'/'PD3', 'Rare').
card_artist('crosis, the purger'/'PD3', 'Chris Rahn').
card_number('crosis, the purger'/'PD3', '5').
card_multiverse_id('crosis, the purger'/'PD3', '270140').

card_in_set('crystal vein', 'PD3').
card_original_type('crystal vein'/'PD3', 'Land').
card_original_text('crystal vein'/'PD3', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Crystal Vein: Add {2} to your mana pool.').
card_image_name('crystal vein'/'PD3', 'crystal vein').
card_uid('crystal vein'/'PD3', 'PD3:Crystal Vein:crystal vein').
card_rarity('crystal vein'/'PD3', 'Uncommon').
card_artist('crystal vein'/'PD3', 'Pat Morrissey').
card_number('crystal vein'/'PD3', '24').
card_multiverse_id('crystal vein'/'PD3', '270466').

card_in_set('diabolic servitude', 'PD3').
card_original_type('diabolic servitude'/'PD3', 'Enchantment').
card_original_text('diabolic servitude'/'PD3', 'When Diabolic Servitude enters the battlefield, return target creature card from your graveyard to the battlefield.\nWhen the creature put onto the battlefield with Diabolic Servitude dies, exile it and return Diabolic Servitude to its owner\'s hand.\nWhen Diabolic Servitude leaves the battlefield, exile the creature put onto the battlefield with Diabolic Servitude.').
card_image_name('diabolic servitude'/'PD3', 'diabolic servitude').
card_uid('diabolic servitude'/'PD3', 'PD3:Diabolic Servitude:diabolic servitude').
card_rarity('diabolic servitude'/'PD3', 'Uncommon').
card_artist('diabolic servitude'/'PD3', 'Scott M. Fischer').
card_number('diabolic servitude'/'PD3', '22').
card_multiverse_id('diabolic servitude'/'PD3', '270461').

card_in_set('dread return', 'PD3').
card_original_type('dread return'/'PD3', 'Sorcery').
card_original_text('dread return'/'PD3', 'Return target creature card from your graveyard to the battlefield.\nFlashback—Sacrifice three creatures. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('dread return'/'PD3', 'dread return').
card_uid('dread return'/'PD3', 'PD3:Dread Return:dread return').
card_rarity('dread return'/'PD3', 'Uncommon').
card_artist('dread return'/'PD3', 'Kev Walker').
card_number('dread return'/'PD3', '23').
card_flavor_text('dread return'/'PD3', 'Those who forget the horrors of the past are doomed to re-meet them.').
card_multiverse_id('dread return'/'PD3', '270463').

card_in_set('duress', 'PD3').
card_original_type('duress'/'PD3', 'Sorcery').
card_original_text('duress'/'PD3', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'PD3', 'duress').
card_uid('duress'/'PD3', 'PD3:Duress:duress').
card_rarity('duress'/'PD3', 'Common').
card_artist('duress'/'PD3', 'Steven Belledin').
card_number('duress'/'PD3', '13').
card_flavor_text('duress'/'PD3', '\"Don\'t worry. I\'m not going to deprive you of all your secrets. Just your most precious one.\"\n—Liliana Vess').
card_multiverse_id('duress'/'PD3', '270465').

card_in_set('ebon stronghold', 'PD3').
card_original_type('ebon stronghold'/'PD3', 'Land').
card_original_text('ebon stronghold'/'PD3', 'Ebon Stronghold enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice Ebon Stronghold: Add {B}{B} to your mana pool.').
card_image_name('ebon stronghold'/'PD3', 'ebon stronghold').
card_uid('ebon stronghold'/'PD3', 'PD3:Ebon Stronghold:ebon stronghold').
card_rarity('ebon stronghold'/'PD3', 'Uncommon').
card_artist('ebon stronghold'/'PD3', 'Liz Danforth').
card_number('ebon stronghold'/'PD3', '25').
card_multiverse_id('ebon stronghold'/'PD3', '270467').

card_in_set('entomb', 'PD3').
card_original_type('entomb'/'PD3', 'Instant').
card_original_text('entomb'/'PD3', 'Search your library for a card and put that card into your graveyard. Then shuffle your library.').
card_image_name('entomb'/'PD3', 'entomb').
card_uid('entomb'/'PD3', 'PD3:Entomb:entomb').
card_rarity('entomb'/'PD3', 'Rare').
card_artist('entomb'/'PD3', 'Ron Spears').
card_number('entomb'/'PD3', '14').
card_flavor_text('entomb'/'PD3', 'A grave is the safest place to store ill-gotten treasures.').
card_multiverse_id('entomb'/'PD3', '270456').

card_in_set('exhume', 'PD3').
card_original_type('exhume'/'PD3', 'Sorcery').
card_original_text('exhume'/'PD3', 'Each player puts a creature card from his or her graveyard onto the battlefield.').
card_image_name('exhume'/'PD3', 'exhume').
card_uid('exhume'/'PD3', 'PD3:Exhume:exhume').
card_rarity('exhume'/'PD3', 'Common').
card_artist('exhume'/'PD3', 'Carl Critchlow').
card_number('exhume'/'PD3', '17').
card_flavor_text('exhume'/'PD3', '\"Death—an outmoded concept. We sleep, and we change.\"\n—Sitrik, birth priest').
card_multiverse_id('exhume'/'PD3', '270462').

card_in_set('faceless butcher', 'PD3').
card_original_type('faceless butcher'/'PD3', 'Creature — Nightmare Horror').
card_original_text('faceless butcher'/'PD3', 'When Faceless Butcher enters the battlefield, exile target creature other than Faceless Butcher.\nWhen Faceless Butcher leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('faceless butcher'/'PD3', 'faceless butcher').
card_uid('faceless butcher'/'PD3', 'PD3:Faceless Butcher:faceless butcher').
card_rarity('faceless butcher'/'PD3', 'Common').
card_artist('faceless butcher'/'PD3', 'Daren Bader').
card_number('faceless butcher'/'PD3', '3').
card_multiverse_id('faceless butcher'/'PD3', '270458').

card_in_set('hidden horror', 'PD3').
card_original_type('hidden horror'/'PD3', 'Creature — Horror').
card_original_text('hidden horror'/'PD3', 'When Hidden Horror enters the battlefield, sacrifice it unless you discard a creature card.').
card_image_name('hidden horror'/'PD3', 'hidden horror').
card_uid('hidden horror'/'PD3', 'PD3:Hidden Horror:hidden horror').
card_rarity('hidden horror'/'PD3', 'Uncommon').
card_artist('hidden horror'/'PD3', 'Brom').
card_number('hidden horror'/'PD3', '2').
card_flavor_text('hidden horror'/'PD3', 'If the presence of evil were obvious, it wouldn\'t be nearly as dangerous.').
card_multiverse_id('hidden horror'/'PD3', '270464').

card_in_set('inkwell leviathan', 'PD3').
card_original_type('inkwell leviathan'/'PD3', 'Artifact Creature — Leviathan').
card_original_text('inkwell leviathan'/'PD3', 'Islandwalk, trample, shroud').
card_image_name('inkwell leviathan'/'PD3', 'inkwell leviathan').
card_uid('inkwell leviathan'/'PD3', 'PD3:Inkwell Leviathan:inkwell leviathan').
card_rarity('inkwell leviathan'/'PD3', 'Rare').
card_artist('inkwell leviathan'/'PD3', 'Anthony Francisco').
card_number('inkwell leviathan'/'PD3', '10').
card_flavor_text('inkwell leviathan'/'PD3', '\"Into its maw went the seventh sea, never to be seen again while the world remains.\"\n—Esper fable').
card_multiverse_id('inkwell leviathan'/'PD3', '270447').

card_in_set('last rites', 'PD3').
card_original_type('last rites'/'PD3', 'Sorcery').
card_original_text('last rites'/'PD3', 'Discard any number of cards. Target player reveals his or her hand, then you choose a nonland card from it for each card discarded this way. That player discards those cards.').
card_image_name('last rites'/'PD3', 'last rites').
card_uid('last rites'/'PD3', 'PD3:Last Rites:last rites').
card_rarity('last rites'/'PD3', 'Common').
card_artist('last rites'/'PD3', 'Bradley Williams').
card_number('last rites'/'PD3', '21').
card_multiverse_id('last rites'/'PD3', '270457').

card_in_set('polluted mire', 'PD3').
card_original_type('polluted mire'/'PD3', 'Land').
card_original_text('polluted mire'/'PD3', 'Polluted Mire enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('polluted mire'/'PD3', 'polluted mire').
card_uid('polluted mire'/'PD3', 'PD3:Polluted Mire:polluted mire').
card_rarity('polluted mire'/'PD3', 'Common').
card_artist('polluted mire'/'PD3', 'Stephen Daniele').
card_number('polluted mire'/'PD3', '26').
card_multiverse_id('polluted mire'/'PD3', '270468').

card_in_set('putrid imp', 'PD3').
card_original_type('putrid imp'/'PD3', 'Creature — Zombie Imp').
card_original_text('putrid imp'/'PD3', 'Discard a card: Putrid Imp gains flying until end of turn.\nThreshold — As long as seven or more cards are in your graveyard, Putrid Imp gets +1/+1 and can\'t block.').
card_image_name('putrid imp'/'PD3', 'putrid imp').
card_uid('putrid imp'/'PD3', 'PD3:Putrid Imp:putrid imp').
card_rarity('putrid imp'/'PD3', 'Common').
card_artist('putrid imp'/'PD3', 'Wayne England').
card_number('putrid imp'/'PD3', '1').
card_multiverse_id('putrid imp'/'PD3', '270459').

card_in_set('reanimate', 'PD3').
card_original_type('reanimate'/'PD3', 'Sorcery').
card_original_text('reanimate'/'PD3', 'Put target creature card from a graveyard onto the battlefield under your control. You lose life equal to its converted mana cost.').
card_image_name('reanimate'/'PD3', 'reanimate').
card_uid('reanimate'/'PD3', 'PD3:Reanimate:reanimate').
card_rarity('reanimate'/'PD3', 'Uncommon').
card_artist('reanimate'/'PD3', 'Robert Bliss').
card_number('reanimate'/'PD3', '15').
card_flavor_text('reanimate'/'PD3', '\"You will learn to earn death.\"\n—Volrath').
card_multiverse_id('reanimate'/'PD3', '270452').

card_in_set('sickening dreams', 'PD3').
card_original_type('sickening dreams'/'PD3', 'Sorcery').
card_original_text('sickening dreams'/'PD3', 'As an additional cost to cast Sickening Dreams, discard X cards.\nSickening Dreams deals X damage to each creature and each player.').
card_image_name('sickening dreams'/'PD3', 'sickening dreams').
card_uid('sickening dreams'/'PD3', 'PD3:Sickening Dreams:sickening dreams').
card_rarity('sickening dreams'/'PD3', 'Uncommon').
card_artist('sickening dreams'/'PD3', 'Scott M. Fischer').
card_number('sickening dreams'/'PD3', '18').
card_flavor_text('sickening dreams'/'PD3', 'The Patriarch dreams of vile plague.').
card_multiverse_id('sickening dreams'/'PD3', '270460').

card_in_set('sphinx of the steel wind', 'PD3').
card_original_type('sphinx of the steel wind'/'PD3', 'Artifact Creature — Sphinx').
card_original_text('sphinx of the steel wind'/'PD3', 'Flying, first strike, vigilance, lifelink, protection from red and from green').
card_image_name('sphinx of the steel wind'/'PD3', 'sphinx of the steel wind').
card_uid('sphinx of the steel wind'/'PD3', 'PD3:Sphinx of the Steel Wind:sphinx of the steel wind').
card_rarity('sphinx of the steel wind'/'PD3', 'Mythic Rare').
card_artist('sphinx of the steel wind'/'PD3', 'Kev Walker').
card_number('sphinx of the steel wind'/'PD3', '9').
card_flavor_text('sphinx of the steel wind'/'PD3', 'No one has properly answered her favorite riddle: \"Why should I spare your life?\"').
card_multiverse_id('sphinx of the steel wind'/'PD3', '270446').

card_in_set('swamp', 'PD3').
card_original_type('swamp'/'PD3', 'Basic Land — Swamp').
card_original_text('swamp'/'PD3', 'B').
card_image_name('swamp'/'PD3', 'swamp1').
card_uid('swamp'/'PD3', 'PD3:Swamp:swamp1').
card_rarity('swamp'/'PD3', 'Basic Land').
card_artist('swamp'/'PD3', 'Aleksi Briclot').
card_number('swamp'/'PD3', '27').
card_multiverse_id('swamp'/'PD3', '270475').

card_in_set('swamp', 'PD3').
card_original_type('swamp'/'PD3', 'Basic Land — Swamp').
card_original_text('swamp'/'PD3', 'B').
card_image_name('swamp'/'PD3', 'swamp2').
card_uid('swamp'/'PD3', 'PD3:Swamp:swamp2').
card_rarity('swamp'/'PD3', 'Basic Land').
card_artist('swamp'/'PD3', 'James Paick').
card_number('swamp'/'PD3', '28').
card_multiverse_id('swamp'/'PD3', '270473').

card_in_set('swamp', 'PD3').
card_original_type('swamp'/'PD3', 'Basic Land — Swamp').
card_original_text('swamp'/'PD3', 'B').
card_image_name('swamp'/'PD3', 'swamp3').
card_uid('swamp'/'PD3', 'PD3:Swamp:swamp3').
card_rarity('swamp'/'PD3', 'Basic Land').
card_artist('swamp'/'PD3', 'Jung Park').
card_number('swamp'/'PD3', '29').
card_multiverse_id('swamp'/'PD3', '270474').

card_in_set('swamp', 'PD3').
card_original_type('swamp'/'PD3', 'Basic Land — Swamp').
card_original_text('swamp'/'PD3', 'B').
card_image_name('swamp'/'PD3', 'swamp4').
card_uid('swamp'/'PD3', 'PD3:Swamp:swamp4').
card_rarity('swamp'/'PD3', 'Basic Land').
card_artist('swamp'/'PD3', 'Alan Pollack').
card_number('swamp'/'PD3', '30').
card_multiverse_id('swamp'/'PD3', '270476').

card_in_set('terastodon', 'PD3').
card_original_type('terastodon'/'PD3', 'Creature — Elephant').
card_original_text('terastodon'/'PD3', 'When Terastodon enters the battlefield, you may destroy up to three target noncreature permanents. For each permanent put into a graveyard this way, its controller puts a 3/3 green Elephant creature token onto the battlefield.').
card_image_name('terastodon'/'PD3', 'terastodon').
card_uid('terastodon'/'PD3', 'PD3:Terastodon:terastodon').
card_rarity('terastodon'/'PD3', 'Rare').
card_artist('terastodon'/'PD3', 'Lars Grant-West').
card_number('terastodon'/'PD3', '7').
card_multiverse_id('terastodon'/'PD3', '270451').

card_in_set('twisted abomination', 'PD3').
card_original_type('twisted abomination'/'PD3', 'Creature — Zombie Mutant').
card_original_text('twisted abomination'/'PD3', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('twisted abomination'/'PD3', 'twisted abomination').
card_uid('twisted abomination'/'PD3', 'PD3:Twisted Abomination:twisted abomination').
card_rarity('twisted abomination'/'PD3', 'Common').
card_artist('twisted abomination'/'PD3', 'Daren Bader').
card_number('twisted abomination'/'PD3', '4').
card_multiverse_id('twisted abomination'/'PD3', '270453').

card_in_set('verdant force', 'PD3').
card_original_type('verdant force'/'PD3', 'Creature — Elemental').
card_original_text('verdant force'/'PD3', 'At the beginning of each upkeep, put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('verdant force'/'PD3', 'verdant force').
card_uid('verdant force'/'PD3', 'PD3:Verdant Force:verdant force').
card_rarity('verdant force'/'PD3', 'Rare').
card_artist('verdant force'/'PD3', 'DiTerlizzi').
card_number('verdant force'/'PD3', '8').
card_flavor_text('verdant force'/'PD3', 'Left to itself, nature overflows any container, overthrows any restriction, and overreaches any boundary.').
card_multiverse_id('verdant force'/'PD3', '270450').

card_in_set('zombie infestation', 'PD3').
card_original_type('zombie infestation'/'PD3', 'Enchantment').
card_original_text('zombie infestation'/'PD3', 'Discard two cards: Put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('zombie infestation'/'PD3', 'zombie infestation').
card_uid('zombie infestation'/'PD3', 'PD3:Zombie Infestation:zombie infestation').
card_rarity('zombie infestation'/'PD3', 'Uncommon').
card_artist('zombie infestation'/'PD3', 'Thomas M. Baxa').
card_number('zombie infestation'/'PD3', '19').
card_flavor_text('zombie infestation'/'PD3', 'The nomads\' funeral pyres are more practical than ceremonial.').
card_multiverse_id('zombie infestation'/'PD3', '270454').
