% Chronicles

set('CHR').
set_name('CHR', 'Chronicles').
set_release_date('CHR', '1995-07-01').
set_border('CHR', 'white').
set_type('CHR', 'reprint').

card_in_set('abu ja\'far', 'CHR').
card_original_type('abu ja\'far'/'CHR', 'Summon — Leper').
card_original_text('abu ja\'far'/'CHR', 'If Abu Ja\'far is put into the graveyard from play during combat, bury all creatures blocking or blocked by Abu Ja\'far.').
card_image_name('abu ja\'far'/'CHR', 'abu ja\'far').
card_uid('abu ja\'far'/'CHR', 'CHR:Abu Ja\'far:abu ja\'far').
card_rarity('abu ja\'far'/'CHR', 'Uncommon').
card_artist('abu ja\'far'/'CHR', 'Ken Meyer, Jr.').
card_multiverse_id('abu ja\'far'/'CHR', '2838').

card_in_set('active volcano', 'CHR').
card_original_type('active volcano'/'CHR', 'Instant').
card_original_text('active volcano'/'CHR', 'Destroy target blue permanent or return target island to owner\'s hand.').
card_image_name('active volcano'/'CHR', 'active volcano').
card_uid('active volcano'/'CHR', 'CHR:Active Volcano:active volcano').
card_rarity('active volcano'/'CHR', 'Common').
card_artist('active volcano'/'CHR', 'Justin Hampton').
card_multiverse_id('active volcano'/'CHR', '2839').

card_in_set('akron legionnaire', 'CHR').
card_original_type('akron legionnaire'/'CHR', 'Summon — Legionnaire').
card_original_text('akron legionnaire'/'CHR', 'Except for Akron Legionnaires, non-artifact creatures you control cannot attack.').
card_image_name('akron legionnaire'/'CHR', 'akron legionnaire').
card_uid('akron legionnaire'/'CHR', 'CHR:Akron Legionnaire:akron legionnaire').
card_rarity('akron legionnaire'/'CHR', 'Rare').
card_artist('akron legionnaire'/'CHR', 'Mark Poole').
card_multiverse_id('akron legionnaire'/'CHR', '2853').

card_in_set('aladdin', 'CHR').
card_original_type('aladdin'/'CHR', 'Summon — Aladdin').
card_original_text('aladdin'/'CHR', '{1}{R}{R}, {T}: Gain control of target artifact. Lose control of target artifact if Aladdin leaves play or if you lose control of Aladdin.').
card_image_name('aladdin'/'CHR', 'aladdin').
card_uid('aladdin'/'CHR', 'CHR:Aladdin:aladdin').
card_rarity('aladdin'/'CHR', 'Uncommon').
card_artist('aladdin'/'CHR', 'Julie Baroh').
card_multiverse_id('aladdin'/'CHR', '2840').

card_in_set('angelic voices', 'CHR').
card_original_type('angelic voices'/'CHR', 'Enchantment').
card_original_text('angelic voices'/'CHR', 'As long as the only creatures you control are white or artifact creatures, all creatures you control get +1/+1.').
card_image_name('angelic voices'/'CHR', 'angelic voices').
card_uid('angelic voices'/'CHR', 'CHR:Angelic Voices:angelic voices').
card_rarity('angelic voices'/'CHR', 'Rare').
card_artist('angelic voices'/'CHR', 'Julie Baroh').
card_multiverse_id('angelic voices'/'CHR', '2854').

card_in_set('arcades sabboth', 'CHR').
card_original_type('arcades sabboth'/'CHR', 'Summon — Elder Dragon Legend').
card_original_text('arcades sabboth'/'CHR', 'Flying\nAs long as they are not attacking, untapped creatures you control get +0/+2. During your upkeep, pay {W}{U}{G} or bury Arcades Sabboth.\n{W} +0/+1 until end of turn').
card_image_name('arcades sabboth'/'CHR', 'arcades sabboth').
card_uid('arcades sabboth'/'CHR', 'CHR:Arcades Sabboth:arcades sabboth').
card_rarity('arcades sabboth'/'CHR', 'Rare').
card_artist('arcades sabboth'/'CHR', 'Edward P. Beard, Jr.').
card_multiverse_id('arcades sabboth'/'CHR', '2866').

card_in_set('arena of the ancients', 'CHR').
card_original_type('arena of the ancients'/'CHR', 'Artifact').
card_original_text('arena of the ancients'/'CHR', 'When Arena of the Ancients comes into play, tap all legends. Legends do not untap during their controllers\' untap phase.').
card_image_name('arena of the ancients'/'CHR', 'arena of the ancients').
card_uid('arena of the ancients'/'CHR', 'CHR:Arena of the Ancients:arena of the ancients').
card_rarity('arena of the ancients'/'CHR', 'Rare').
card_artist('arena of the ancients'/'CHR', 'Tom Wänerstrand').
card_multiverse_id('arena of the ancients'/'CHR', '2775').

card_in_set('argothian pixies', 'CHR').
card_original_type('argothian pixies'/'CHR', 'Summon — Faeries').
card_original_text('argothian pixies'/'CHR', 'Cannot be blocked by artifact creatures. Any damage dealt to Argothian Pixies by artifact creatures is reduced to 0.').
card_image_name('argothian pixies'/'CHR', 'argothian pixies').
card_uid('argothian pixies'/'CHR', 'CHR:Argothian Pixies:argothian pixies').
card_rarity('argothian pixies'/'CHR', 'Common').
card_artist('argothian pixies'/'CHR', 'Amy Weber').
card_flavor_text('argothian pixies'/'CHR', 'After the rape of Argoth Forest during the rule of the artificers, the Pixies of Argoth bent their magic to more practical ends.').
card_multiverse_id('argothian pixies'/'CHR', '2824').

card_in_set('ashnod\'s altar', 'CHR').
card_original_type('ashnod\'s altar'/'CHR', 'Artifact').
card_original_text('ashnod\'s altar'/'CHR', '{0}: Sacrifice a creature to add two colorless mana to your mana pool. Play this ability as an interrupt.').
card_image_name('ashnod\'s altar'/'CHR', 'ashnod\'s altar').
card_uid('ashnod\'s altar'/'CHR', 'CHR:Ashnod\'s Altar:ashnod\'s altar').
card_rarity('ashnod\'s altar'/'CHR', 'Common').
card_artist('ashnod\'s altar'/'CHR', 'Anson Maddocks').
card_multiverse_id('ashnod\'s altar'/'CHR', '2776').

card_in_set('ashnod\'s transmogrant', 'CHR').
card_original_type('ashnod\'s transmogrant'/'CHR', 'Artifact').
card_original_text('ashnod\'s transmogrant'/'CHR', '{T}: Sacrifice Ashnod\'s Transmogrant to put a +1/+1 counter on target non-artifact creature. That creature becomes an artifact creature, although it retains its color.').
card_image_name('ashnod\'s transmogrant'/'CHR', 'ashnod\'s transmogrant').
card_uid('ashnod\'s transmogrant'/'CHR', 'CHR:Ashnod\'s Transmogrant:ashnod\'s transmogrant').
card_rarity('ashnod\'s transmogrant'/'CHR', 'Common').
card_artist('ashnod\'s transmogrant'/'CHR', 'Mark Tedin').
card_flavor_text('ashnod\'s transmogrant'/'CHR', 'Ashnod found few willing to trade their humanity for the power she offered them.').
card_multiverse_id('ashnod\'s transmogrant'/'CHR', '2777').

card_in_set('axelrod gunnarson', 'CHR').
card_original_type('axelrod gunnarson'/'CHR', 'Summon — Legend').
card_original_text('axelrod gunnarson'/'CHR', 'Trample\nWhenever a creature is damaged by Axelrod Gunnarson and put into the graveyard, you gain 1 life and Axelrod deals 1 damage to target player.').
card_image_name('axelrod gunnarson'/'CHR', 'axelrod gunnarson').
card_uid('axelrod gunnarson'/'CHR', 'CHR:Axelrod Gunnarson:axelrod gunnarson').
card_rarity('axelrod gunnarson'/'CHR', 'Rare').
card_artist('axelrod gunnarson'/'CHR', 'Scott Kirschner').
card_multiverse_id('axelrod gunnarson'/'CHR', '2867').

card_in_set('ayesha tanaka', 'CHR').
card_original_type('ayesha tanaka'/'CHR', 'Summon — Legend').
card_original_text('ayesha tanaka'/'CHR', 'Banding\n{T}: Counter target artifact effect requiring an activation cost, unless the artifact\'s controller pays {W}. Play this ability as an interrupt.').
card_image_name('ayesha tanaka'/'CHR', 'ayesha tanaka').
card_uid('ayesha tanaka'/'CHR', 'CHR:Ayesha Tanaka:ayesha tanaka').
card_rarity('ayesha tanaka'/'CHR', 'Rare').
card_artist('ayesha tanaka'/'CHR', 'Bryon Wackwitz').
card_multiverse_id('ayesha tanaka'/'CHR', '2868').

card_in_set('azure drake', 'CHR').
card_original_type('azure drake'/'CHR', 'Summon — Drake').
card_original_text('azure drake'/'CHR', 'Flying').
card_image_name('azure drake'/'CHR', 'azure drake').
card_uid('azure drake'/'CHR', 'CHR:Azure Drake:azure drake').
card_rarity('azure drake'/'CHR', 'Uncommon').
card_artist('azure drake'/'CHR', 'Dan Frazier').
card_flavor_text('azure drake'/'CHR', 'The Azure Drake would be more powerful were it not so easily distracted.').
card_multiverse_id('azure drake'/'CHR', '2796').

card_in_set('banshee', 'CHR').
card_original_type('banshee'/'CHR', 'Summon — Banshee').
card_original_text('banshee'/'CHR', '{X}, {T}: Banshee deals X damage, half (rounded up) to you and half (rounded down) to target creature or player.').
card_image_name('banshee'/'CHR', 'banshee').
card_uid('banshee'/'CHR', 'CHR:Banshee:banshee').
card_rarity('banshee'/'CHR', 'Uncommon').
card_artist('banshee'/'CHR', 'Jesper Myrfors').
card_flavor_text('banshee'/'CHR', 'Some say Banshees are the hounds of Death, baying to herd their prey into the arms of their master.').
card_multiverse_id('banshee'/'CHR', '2797').

card_in_set('barl\'s cage', 'CHR').
card_original_type('barl\'s cage'/'CHR', 'Artifact').
card_original_text('barl\'s cage'/'CHR', '{3}: Target creature does not untap during its controller\'s next untap phase.').
card_image_name('barl\'s cage'/'CHR', 'barl\'s cage').
card_uid('barl\'s cage'/'CHR', 'CHR:Barl\'s Cage:barl\'s cage').
card_rarity('barl\'s cage'/'CHR', 'Rare').
card_artist('barl\'s cage'/'CHR', 'Tom Wänerstrand').
card_flavor_text('barl\'s cage'/'CHR', 'For a dozen years the Cage had held Lord Ith, but as the Pretender Mairsil\'s power weakened, so did the bars.').
card_multiverse_id('barl\'s cage'/'CHR', '2778').

card_in_set('beasts of bogardan', 'CHR').
card_original_type('beasts of bogardan'/'CHR', 'Summon — Beasts').
card_original_text('beasts of bogardan'/'CHR', 'Protection from red\nAs long as any opponent controls any white cards, Beasts of Bogardan gets +1/+1.').
card_image_name('beasts of bogardan'/'CHR', 'beasts of bogardan').
card_uid('beasts of bogardan'/'CHR', 'CHR:Beasts of Bogardan:beasts of bogardan').
card_rarity('beasts of bogardan'/'CHR', 'Uncommon').
card_artist('beasts of bogardan'/'CHR', 'Daniel Gelon').
card_flavor_text('beasts of bogardan'/'CHR', 'Bogardan is a land as volatile as the creatures who live there.').
card_multiverse_id('beasts of bogardan'/'CHR', '2841').

card_in_set('blood moon', 'CHR').
card_original_type('blood moon'/'CHR', 'Enchantment').
card_original_text('blood moon'/'CHR', 'All non-basic lands become mountains.').
card_image_name('blood moon'/'CHR', 'blood moon').
card_uid('blood moon'/'CHR', 'CHR:Blood Moon:blood moon').
card_rarity('blood moon'/'CHR', 'Rare').
card_artist('blood moon'/'CHR', 'Tom Wänerstrand').
card_flavor_text('blood moon'/'CHR', 'Heavy light flooded across the landscape, cloaking everything in deep crimson.').
card_multiverse_id('blood moon'/'CHR', '2842').

card_in_set('blood of the martyr', 'CHR').
card_original_type('blood of the martyr'/'CHR', 'Instant').
card_original_text('blood of the martyr'/'CHR', 'Until end of turn, you may redirect to yourself all damage dealt to any number of creatures. The source of the damage does not change.').
card_image_name('blood of the martyr'/'CHR', 'blood of the martyr').
card_uid('blood of the martyr'/'CHR', 'CHR:Blood of the Martyr:blood of the martyr').
card_rarity('blood of the martyr'/'CHR', 'Uncommon').
card_artist('blood of the martyr'/'CHR', 'Christopher Rush').
card_flavor_text('blood of the martyr'/'CHR', 'The willow knows what the storm does not: that the power to endure harm outlives the power to inflict it.').
card_multiverse_id('blood of the martyr'/'CHR', '2855').

card_in_set('bog rats', 'CHR').
card_original_type('bog rats'/'CHR', 'Summon — Rats').
card_original_text('bog rats'/'CHR', 'Cannot be blocked by walls.').
card_image_name('bog rats'/'CHR', 'bog rats').
card_uid('bog rats'/'CHR', 'CHR:Bog Rats:bog rats').
card_rarity('bog rats'/'CHR', 'Common').
card_artist('bog rats'/'CHR', 'Ron Spencer').
card_flavor_text('bog rats'/'CHR', 'Their stench was vile and strong enough, but not nearly as powerful as their hunger.').
card_multiverse_id('bog rats'/'CHR', '2798').

card_in_set('book of rass', 'CHR').
card_original_type('book of rass'/'CHR', 'Artifact').
card_original_text('book of rass'/'CHR', '{2}: Pay 2 life to draw a card. Effects that prevent or redirect damage cannot be used to counter this loss of life.').
card_image_name('book of rass'/'CHR', 'book of rass').
card_uid('book of rass'/'CHR', 'CHR:Book of Rass:book of rass').
card_rarity('book of rass'/'CHR', 'Rare').
card_artist('book of rass'/'CHR', 'Sandra Everingham').
card_multiverse_id('book of rass'/'CHR', '2779').

card_in_set('boomerang', 'CHR').
card_original_type('boomerang'/'CHR', 'Instant').
card_original_text('boomerang'/'CHR', 'Return target permanent to owner\'s hand.').
card_image_name('boomerang'/'CHR', 'boomerang').
card_uid('boomerang'/'CHR', 'CHR:Boomerang:boomerang').
card_rarity('boomerang'/'CHR', 'Common').
card_artist('boomerang'/'CHR', 'Brian Snõddy').
card_flavor_text('boomerang'/'CHR', '\"O! call back yesterday, bid time return.\" —William Shakespeare, King Richard the Second').
card_multiverse_id('boomerang'/'CHR', '2799').

card_in_set('bronze horse', 'CHR').
card_original_type('bronze horse'/'CHR', 'Artifact Creature').
card_original_text('bronze horse'/'CHR', 'Trample\nAs long as you control any other creatures, damage dealt to Bronze Horse by spells that target it is reduced to 0.').
card_image_name('bronze horse'/'CHR', 'bronze horse').
card_uid('bronze horse'/'CHR', 'CHR:Bronze Horse:bronze horse').
card_rarity('bronze horse'/'CHR', 'Rare').
card_artist('bronze horse'/'CHR', 'Mark Poole').
card_multiverse_id('bronze horse'/'CHR', '2780').

card_in_set('cat warriors', 'CHR').
card_original_type('cat warriors'/'CHR', 'Summon — Cat Warriors').
card_original_text('cat warriors'/'CHR', 'Forestwalk').
card_image_name('cat warriors'/'CHR', 'cat warriors').
card_uid('cat warriors'/'CHR', 'CHR:Cat Warriors:cat warriors').
card_rarity('cat warriors'/'CHR', 'Common').
card_artist('cat warriors'/'CHR', 'Melissa A. Benson').
card_flavor_text('cat warriors'/'CHR', 'These stealthy felines have survived so many battles that some believe they must possess many lives.').
card_multiverse_id('cat warriors'/'CHR', '2825').

card_in_set('chromium', 'CHR').
card_original_type('chromium'/'CHR', 'Summon — Elder Dragon Legend').
card_original_text('chromium'/'CHR', 'Flying, rampage: 2\nDuring your upkeep, pay {W}{U}{B} or bury Chromium.').
card_image_name('chromium'/'CHR', 'chromium').
card_uid('chromium'/'CHR', 'CHR:Chromium:chromium').
card_rarity('chromium'/'CHR', 'Rare').
card_artist('chromium'/'CHR', 'Edward P. Beard, Jr.').
card_multiverse_id('chromium'/'CHR', '2869').

card_in_set('city of brass', 'CHR').
card_original_type('city of brass'/'CHR', 'Land').
card_original_text('city of brass'/'CHR', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_image_name('city of brass'/'CHR', 'city of brass').
card_uid('city of brass'/'CHR', 'CHR:City of Brass:city of brass').
card_rarity('city of brass'/'CHR', 'Rare').
card_artist('city of brass'/'CHR', 'Mark Tedin').
card_multiverse_id('city of brass'/'CHR', '2886').

card_in_set('cocoon', 'CHR').
card_original_type('cocoon'/'CHR', 'Enchant Creature').
card_original_text('cocoon'/'CHR', 'Tap target creature you control and put three change counters on Cocoon. If there are any change counters on Cocoon, that creature does not untap during your untap phase. During your upkeep, remove one change counter. During the upkeep after the one in which the last change counter was removed, put a +1/+1 counter on the creature, the creature gains flying, and bury Cocoon.').
card_image_name('cocoon'/'CHR', 'cocoon').
card_uid('cocoon'/'CHR', 'CHR:Cocoon:cocoon').
card_rarity('cocoon'/'CHR', 'Uncommon').
card_artist('cocoon'/'CHR', 'Mark Tedin').
card_multiverse_id('cocoon'/'CHR', '2826').

card_in_set('concordant crossroads', 'CHR').
card_original_type('concordant crossroads'/'CHR', 'Enchant World').
card_original_text('concordant crossroads'/'CHR', 'Creatures can attack or use abilities that include {T} in the activation cost as soon as they come into play on their controller\'s side.').
card_image_name('concordant crossroads'/'CHR', 'concordant crossroads').
card_uid('concordant crossroads'/'CHR', 'CHR:Concordant Crossroads:concordant crossroads').
card_rarity('concordant crossroads'/'CHR', 'Rare').
card_artist('concordant crossroads'/'CHR', 'Amy Weber').
card_multiverse_id('concordant crossroads'/'CHR', '2827').

card_in_set('craw giant', 'CHR').
card_original_type('craw giant'/'CHR', 'Summon — Giant').
card_original_text('craw giant'/'CHR', 'Trample, rampage: 2').
card_image_name('craw giant'/'CHR', 'craw giant').
card_uid('craw giant'/'CHR', 'CHR:Craw Giant:craw giant').
card_rarity('craw giant'/'CHR', 'Uncommon').
card_artist('craw giant'/'CHR', 'Christopher Rush').
card_flavor_text('craw giant'/'CHR', 'Harthag gave a jolly laugh as he surveyed the army before him. \"Ho ho ho! Midgets! You think you can stand in my way?\"').
card_multiverse_id('craw giant'/'CHR', '2828').

card_in_set('cuombajj witches', 'CHR').
card_original_type('cuombajj witches'/'CHR', 'Summon — Witches').
card_original_text('cuombajj witches'/'CHR', '{T}: Cuombajj Witches deals 1 damage to target creature or player. Cuombajj Witches also deals 1 damage to target creature or player of target opponent\'s choice. Choose your target first.').
card_image_name('cuombajj witches'/'CHR', 'cuombajj witches').
card_uid('cuombajj witches'/'CHR', 'CHR:Cuombajj Witches:cuombajj witches').
card_rarity('cuombajj witches'/'CHR', 'Common').
card_artist('cuombajj witches'/'CHR', 'Kaja Foglio').
card_multiverse_id('cuombajj witches'/'CHR', '2800').

card_in_set('cyclone', 'CHR').
card_original_type('cyclone'/'CHR', 'Enchantment').
card_original_text('cyclone'/'CHR', 'At the beginning of your upkeep, put a wind counter on Cyclone. During your upkeep, pay {G} for each wind counter on Cyclone or bury Cyclone. If you pay, Cyclone deals 1 damage to each player and each creature for each wind counter on it.').
card_image_name('cyclone'/'CHR', 'cyclone').
card_uid('cyclone'/'CHR', 'CHR:Cyclone:cyclone').
card_rarity('cyclone'/'CHR', 'Rare').
card_artist('cyclone'/'CHR', 'Mark Tedin').
card_multiverse_id('cyclone'/'CHR', '2829').

card_in_set('d\'avenant archer', 'CHR').
card_original_type('d\'avenant archer'/'CHR', 'Summon — Archer').
card_original_text('d\'avenant archer'/'CHR', '{T}: D\'Avenant Archer deals 1 damage to target attacking or blocking creature.').
card_image_name('d\'avenant archer'/'CHR', 'd\'avenant archer').
card_uid('d\'avenant archer'/'CHR', 'CHR:D\'Avenant Archer:d\'avenant archer').
card_rarity('d\'avenant archer'/'CHR', 'Common').
card_artist('d\'avenant archer'/'CHR', 'Douglas Shuler').
card_multiverse_id('d\'avenant archer'/'CHR', '2856').

card_in_set('dakkon blackblade', 'CHR').
card_original_type('dakkon blackblade'/'CHR', 'Summon — Legend').
card_original_text('dakkon blackblade'/'CHR', 'Dakkon Blackblade has power and toughness each equal to the number of lands you control.').
card_image_name('dakkon blackblade'/'CHR', 'dakkon blackblade').
card_uid('dakkon blackblade'/'CHR', 'CHR:Dakkon Blackblade:dakkon blackblade').
card_rarity('dakkon blackblade'/'CHR', 'Rare').
card_artist('dakkon blackblade'/'CHR', 'Richard Kane Ferguson').
card_flavor_text('dakkon blackblade'/'CHR', '\"My power is as vast as the plains; my strength is that of mountains. Each wave that crashes upon the shore thunders like blood in my veins.\"\n—Dakkon Blackblade, Memoirs').
card_multiverse_id('dakkon blackblade'/'CHR', '2870').

card_in_set('dance of many', 'CHR').
card_original_type('dance of many'/'CHR', 'Enchantment').
card_original_text('dance of many'/'CHR', 'When Dance of Many comes into play, choose a target summon card in play. Put a token creature into play and treat it as though an exact copy of that summon card were just summoned. If Dance of Many leaves play, remove that token creature from game. If the token creature leaves play, bury Dance of Many.\nDuring your upkeep, pay {U}{U} or bury Dance of Many.').
card_image_name('dance of many'/'CHR', 'dance of many').
card_uid('dance of many'/'CHR', 'CHR:Dance of Many:dance of many').
card_rarity('dance of many'/'CHR', 'Rare').
card_artist('dance of many'/'CHR', 'Sandra Everingham').
card_multiverse_id('dance of many'/'CHR', '2801').

card_in_set('dandân', 'CHR').
card_original_type('dandân'/'CHR', 'Summon — Dandân').
card_original_text('dandân'/'CHR', 'Cannot attack if defending player controls no islands. If at any time you control no islands, bury Dandân.').
card_image_name('dandân'/'CHR', 'dandan').
card_uid('dandân'/'CHR', 'CHR:Dandân:dandan').
card_rarity('dandân'/'CHR', 'Common').
card_artist('dandân'/'CHR', 'Drew Tucker').
card_multiverse_id('dandân'/'CHR', '2802').

card_in_set('divine offering', 'CHR').
card_original_type('divine offering'/'CHR', 'Instant').
card_original_text('divine offering'/'CHR', 'Destroy target artifact. Gain life equal to the artifact\'s casting cost.').
card_image_name('divine offering'/'CHR', 'divine offering').
card_uid('divine offering'/'CHR', 'CHR:Divine Offering:divine offering').
card_rarity('divine offering'/'CHR', 'Common').
card_artist('divine offering'/'CHR', 'Jeff A. Menges').
card_flavor_text('divine offering'/'CHR', 'D\'Haren stared at the twisted lump of metal that had been a prized artifact. The fight was getting ugly.').
card_multiverse_id('divine offering'/'CHR', '2857').

card_in_set('emerald dragonfly', 'CHR').
card_original_type('emerald dragonfly'/'CHR', 'Summon — Dragonfly').
card_original_text('emerald dragonfly'/'CHR', 'Flying\n{G}{G} First strike until end of turn').
card_image_name('emerald dragonfly'/'CHR', 'emerald dragonfly').
card_uid('emerald dragonfly'/'CHR', 'CHR:Emerald Dragonfly:emerald dragonfly').
card_rarity('emerald dragonfly'/'CHR', 'Common').
card_artist('emerald dragonfly'/'CHR', 'Quinton Hoover').
card_flavor_text('emerald dragonfly'/'CHR', '\"Flittering, wheeling,/ darting in to strike, and then/ gone just as you blink.\"\n—\"Dragonfly Haiku,\" poet unkown').
card_multiverse_id('emerald dragonfly'/'CHR', '2830').

card_in_set('enchantment alteration', 'CHR').
card_original_type('enchantment alteration'/'CHR', 'Instant').
card_original_text('enchantment alteration'/'CHR', 'Switch target enchantment from one creature to another or from one land to another; that enchantment\'s new target must be legal. The controller of the enchantment does not change. Treat the enchantment as though it were just cast on the new target.').
card_image_name('enchantment alteration'/'CHR', 'enchantment alteration').
card_uid('enchantment alteration'/'CHR', 'CHR:Enchantment Alteration:enchantment alteration').
card_rarity('enchantment alteration'/'CHR', 'Uncommon').
card_artist('enchantment alteration'/'CHR', 'Brian Snõddy').
card_multiverse_id('enchantment alteration'/'CHR', '2803').

card_in_set('erhnam djinn', 'CHR').
card_original_type('erhnam djinn'/'CHR', 'Summon — Djinn').
card_original_text('erhnam djinn'/'CHR', 'During your upkeep, target non-wall creature an opponent controls gains forestwalk until your next turn. Ignore this effect if there are no legal targets.').
card_image_name('erhnam djinn'/'CHR', 'erhnam djinn').
card_uid('erhnam djinn'/'CHR', 'CHR:Erhnam Djinn:erhnam djinn').
card_rarity('erhnam djinn'/'CHR', 'Uncommon').
card_artist('erhnam djinn'/'CHR', 'Ken Meyer, Jr.').
card_multiverse_id('erhnam djinn'/'CHR', '2831').

card_in_set('fallen angel', 'CHR').
card_original_type('fallen angel'/'CHR', 'Summon — Angel').
card_original_text('fallen angel'/'CHR', 'Flying\n{0}: Sacrifice a creature to give Fallen Angel +2/+1 until end of turn.').
card_image_name('fallen angel'/'CHR', 'fallen angel').
card_uid('fallen angel'/'CHR', 'CHR:Fallen Angel:fallen angel').
card_rarity('fallen angel'/'CHR', 'Uncommon').
card_artist('fallen angel'/'CHR', 'Anson Maddocks').
card_multiverse_id('fallen angel'/'CHR', '2804').

card_in_set('feldon\'s cane', 'CHR').
card_original_type('feldon\'s cane'/'CHR', 'Artifact').
card_original_text('feldon\'s cane'/'CHR', '{T}: Reshuffle your graveyard into your library. Remove Feldon\'s Cane from the game.').
card_image_name('feldon\'s cane'/'CHR', 'feldon\'s cane').
card_uid('feldon\'s cane'/'CHR', 'CHR:Feldon\'s Cane:feldon\'s cane').
card_rarity('feldon\'s cane'/'CHR', 'Common').
card_artist('feldon\'s cane'/'CHR', 'Mark Tedin').
card_flavor_text('feldon\'s cane'/'CHR', 'Feldon found the first of these canes frozen in the Ronom Glacier.').
card_multiverse_id('feldon\'s cane'/'CHR', '2781').

card_in_set('fire drake', 'CHR').
card_original_type('fire drake'/'CHR', 'Summon — Drake').
card_original_text('fire drake'/'CHR', 'Flying\n{R} +1/+0 until end of turn. You cannot spend more than {R} in this way each turn.').
card_image_name('fire drake'/'CHR', 'fire drake').
card_uid('fire drake'/'CHR', 'CHR:Fire Drake:fire drake').
card_rarity('fire drake'/'CHR', 'Uncommon').
card_artist('fire drake'/'CHR', 'Christopher Rush').
card_multiverse_id('fire drake'/'CHR', '2843').

card_in_set('fishliver oil', 'CHR').
card_original_type('fishliver oil'/'CHR', 'Enchant Creature').
card_original_text('fishliver oil'/'CHR', 'Target creature gains islandwalk.').
card_image_name('fishliver oil'/'CHR', 'fishliver oil').
card_uid('fishliver oil'/'CHR', 'CHR:Fishliver Oil:fishliver oil').
card_rarity('fishliver oil'/'CHR', 'Common').
card_artist('fishliver oil'/'CHR', 'Anson Maddocks').
card_flavor_text('fishliver oil'/'CHR', 'Then the maiden bade him cast off his robes and cover his body with fishliver oil, that he might safely follow her into the sea.').
card_multiverse_id('fishliver oil'/'CHR', '2805').

card_in_set('flash flood', 'CHR').
card_original_type('flash flood'/'CHR', 'Instant').
card_original_text('flash flood'/'CHR', 'Destroy target red permanent or return target mountain to owner\'s hand.').
card_image_name('flash flood'/'CHR', 'flash flood').
card_uid('flash flood'/'CHR', 'CHR:Flash Flood:flash flood').
card_rarity('flash flood'/'CHR', 'Common').
card_artist('flash flood'/'CHR', 'Tom Wänerstrand').
card_flavor_text('flash flood'/'CHR', 'Many people say that no power can bring the mountains low. Many people are fools.').
card_multiverse_id('flash flood'/'CHR', '2806').

card_in_set('fountain of youth', 'CHR').
card_original_type('fountain of youth'/'CHR', 'Artifact').
card_original_text('fountain of youth'/'CHR', '{2}, {T}: Gain 1 life.').
card_image_name('fountain of youth'/'CHR', 'fountain of youth').
card_uid('fountain of youth'/'CHR', 'CHR:Fountain of Youth:fountain of youth').
card_rarity('fountain of youth'/'CHR', 'Common').
card_artist('fountain of youth'/'CHR', 'Daniel Gelon').
card_flavor_text('fountain of youth'/'CHR', 'The Fountain had stood in the town square for centuries, but only the pigeons knew its secret.').
card_multiverse_id('fountain of youth'/'CHR', '2782').

card_in_set('gabriel angelfire', 'CHR').
card_original_type('gabriel angelfire'/'CHR', 'Summon — Legend').
card_original_text('gabriel angelfire'/'CHR', 'During your upkeep, Gabriel Angelfire gains one of the following abilities until your next upkeep: flying, first strike, trample, or rampage: 3.').
card_image_name('gabriel angelfire'/'CHR', 'gabriel angelfire').
card_uid('gabriel angelfire'/'CHR', 'CHR:Gabriel Angelfire:gabriel angelfire').
card_rarity('gabriel angelfire'/'CHR', 'Rare').
card_artist('gabriel angelfire'/'CHR', 'Daniel Gelon').
card_multiverse_id('gabriel angelfire'/'CHR', '2871').

card_in_set('gauntlets of chaos', 'CHR').
card_original_type('gauntlets of chaos'/'CHR', 'Artifact').
card_original_text('gauntlets of chaos'/'CHR', '{5}: Sacrifice Gauntlets of Chaos. Choose a target artifact, creature, or land opponent controls and a target permanent you control of the same type. Exchange control of these permanents. Bury any enchantments played on these permanents.').
card_image_name('gauntlets of chaos'/'CHR', 'gauntlets of chaos').
card_uid('gauntlets of chaos'/'CHR', 'CHR:Gauntlets of Chaos:gauntlets of chaos').
card_rarity('gauntlets of chaos'/'CHR', 'Rare').
card_artist('gauntlets of chaos'/'CHR', 'Dan Frazier').
card_multiverse_id('gauntlets of chaos'/'CHR', '2783').

card_in_set('ghazbán ogre', 'CHR').
card_original_type('ghazbán ogre'/'CHR', 'Summon — Ogre').
card_original_text('ghazbán ogre'/'CHR', 'During your upkeep, the player with the most life gains control of Ghazbán Ogre. If the highest life total is shared by more than one player, the player currently controlling Ghazbán Ogre retains control of it.').
card_image_name('ghazbán ogre'/'CHR', 'ghazban ogre').
card_uid('ghazbán ogre'/'CHR', 'CHR:Ghazbán Ogre:ghazban ogre').
card_rarity('ghazbán ogre'/'CHR', 'Common').
card_artist('ghazbán ogre'/'CHR', 'Jesper Myrfors').
card_multiverse_id('ghazbán ogre'/'CHR', '2832').

card_in_set('giant slug', 'CHR').
card_original_type('giant slug'/'CHR', 'Summon — Slug').
card_original_text('giant slug'/'CHR', '{5}: During your next upkeep, choose a basic landwalk ability. Giant Slug gains that landwalk ability until end of turn.').
card_image_name('giant slug'/'CHR', 'giant slug').
card_uid('giant slug'/'CHR', 'CHR:Giant Slug:giant slug').
card_rarity('giant slug'/'CHR', 'Common').
card_artist('giant slug'/'CHR', 'Anson Maddocks').
card_multiverse_id('giant slug'/'CHR', '2807').

card_in_set('goblin artisans', 'CHR').
card_original_type('goblin artisans'/'CHR', 'Summon — Goblins').
card_original_text('goblin artisans'/'CHR', '{T}: Use this ability only when you cast a target artifact spell; play this ability as an interrupt. Flip a coin; target opponent calls heads or tails while coin is in the air. If the flip ends up in your favor, draw a card. Otherwise, counter your artifact spell. More than one Goblin Artisans ability cannot target the same artifact spell.').
card_image_name('goblin artisans'/'CHR', 'goblin artisans').
card_uid('goblin artisans'/'CHR', 'CHR:Goblin Artisans:goblin artisans').
card_rarity('goblin artisans'/'CHR', 'Uncommon').
card_artist('goblin artisans'/'CHR', 'Julie Baroh').
card_multiverse_id('goblin artisans'/'CHR', '2844').

card_in_set('goblin digging team', 'CHR').
card_original_type('goblin digging team'/'CHR', 'Summon — Goblins').
card_original_text('goblin digging team'/'CHR', '{T}: Sacrifice Goblin Digging Team to destroy target wall.').
card_image_name('goblin digging team'/'CHR', 'goblin digging team').
card_uid('goblin digging team'/'CHR', 'CHR:Goblin Digging Team:goblin digging team').
card_rarity('goblin digging team'/'CHR', 'Common').
card_artist('goblin digging team'/'CHR', 'Ron Spencer').
card_flavor_text('goblin digging team'/'CHR', '\"From down here we can make the whole wall collapse!\" \"Uh, yeah, boss, but how do we get out?\"').
card_multiverse_id('goblin digging team'/'CHR', '2845').

card_in_set('goblin shrine', 'CHR').
card_original_type('goblin shrine'/'CHR', 'Enchant Land').
card_original_text('goblin shrine'/'CHR', 'As long as target land is a mountain, all Goblins get +1/+0. If Goblin Shrine leaves play, it deals 1 damage to each Goblin.').
card_image_name('goblin shrine'/'CHR', 'goblin shrine').
card_uid('goblin shrine'/'CHR', 'CHR:Goblin Shrine:goblin shrine').
card_rarity('goblin shrine'/'CHR', 'Common').
card_artist('goblin shrine'/'CHR', 'Ron Spencer').
card_flavor_text('goblin shrine'/'CHR', '\"I knew it weren\'t no ordinary pile of—you know.\" —Norin the Wary').
card_multiverse_id('goblin shrine'/'CHR', '2846').

card_in_set('goblins of the flarg', 'CHR').
card_original_type('goblins of the flarg'/'CHR', 'Summon — Goblins').
card_original_text('goblins of the flarg'/'CHR', 'Mountainwalk\nIf at any time you control any Dwarves, bury Goblins of the Flarg.').
card_image_name('goblins of the flarg'/'CHR', 'goblins of the flarg').
card_uid('goblins of the flarg'/'CHR', 'CHR:Goblins of the Flarg:goblins of the flarg').
card_rarity('goblins of the flarg'/'CHR', 'Common').
card_artist('goblins of the flarg'/'CHR', 'Tom Wänerstrand').
card_multiverse_id('goblins of the flarg'/'CHR', '2847').

card_in_set('hasran ogress', 'CHR').
card_original_type('hasran ogress'/'CHR', 'Summon — Ogre').
card_original_text('hasran ogress'/'CHR', 'If you declare Hasran Ogress as an attacker, pay {2} or Hasran Ogress deals 3 damage to you.').
card_image_name('hasran ogress'/'CHR', 'hasran ogress').
card_uid('hasran ogress'/'CHR', 'CHR:Hasran Ogress:hasran ogress').
card_rarity('hasran ogress'/'CHR', 'Common').
card_artist('hasran ogress'/'CHR', 'Dan Frazier').
card_multiverse_id('hasran ogress'/'CHR', '2808').

card_in_set('hell\'s caretaker', 'CHR').
card_original_type('hell\'s caretaker'/'CHR', 'Summon — Hell\'s Caretaker').
card_original_text('hell\'s caretaker'/'CHR', '{T}: Sacrifice a creature to take target creature from your graveyard and put it directly into play as though it were just summoned. Use this ability only during your upkeep.').
card_image_name('hell\'s caretaker'/'CHR', 'hell\'s caretaker').
card_uid('hell\'s caretaker'/'CHR', 'CHR:Hell\'s Caretaker:hell\'s caretaker').
card_rarity('hell\'s caretaker'/'CHR', 'Rare').
card_artist('hell\'s caretaker'/'CHR', 'Sandra Everingham').
card_flavor_text('hell\'s caretaker'/'CHR', 'You might leave here, Chenndra, should another take your place . . . .').
card_multiverse_id('hell\'s caretaker'/'CHR', '2809').

card_in_set('horn of deafening', 'CHR').
card_original_type('horn of deafening'/'CHR', 'Artifact').
card_original_text('horn of deafening'/'CHR', '{2}, {T}: Target creature deals no damage in combat this turn.').
card_image_name('horn of deafening'/'CHR', 'horn of deafening').
card_uid('horn of deafening'/'CHR', 'CHR:Horn of Deafening:horn of deafening').
card_rarity('horn of deafening'/'CHR', 'Rare').
card_artist('horn of deafening'/'CHR', 'Dan Frazier').
card_flavor_text('horn of deafening'/'CHR', '\"A blast, an echo . . . then silence.\"').
card_multiverse_id('horn of deafening'/'CHR', '2784').

card_in_set('indestructible aura', 'CHR').
card_original_type('indestructible aura'/'CHR', 'Instant').
card_original_text('indestructible aura'/'CHR', 'Any damage dealt to target creature for the rest of the turn is reduced to 0.').
card_image_name('indestructible aura'/'CHR', 'indestructible aura').
card_uid('indestructible aura'/'CHR', 'CHR:Indestructible Aura:indestructible aura').
card_rarity('indestructible aura'/'CHR', 'Common').
card_artist('indestructible aura'/'CHR', 'Mark Poole').
card_flavor_text('indestructible aura'/'CHR', 'Theodar strode the battle lines, snatching swords with his bare hands and casting them aside until all cowered before him.').
card_multiverse_id('indestructible aura'/'CHR', '2858').

card_in_set('ivory guardians', 'CHR').
card_original_type('ivory guardians'/'CHR', 'Summon — Guardians').
card_original_text('ivory guardians'/'CHR', 'Protection from red\nAs long as an opponent controls any red cards, all Guardians get +1/+1.').
card_image_name('ivory guardians'/'CHR', 'ivory guardians').
card_uid('ivory guardians'/'CHR', 'CHR:Ivory Guardians:ivory guardians').
card_rarity('ivory guardians'/'CHR', 'Uncommon').
card_artist('ivory guardians'/'CHR', 'Melissa A. Benson').
card_flavor_text('ivory guardians'/'CHR', 'The elite guard of the Mesa High Priests, the Ivory Guardians were created to protect the innocent and faithful.').
card_multiverse_id('ivory guardians'/'CHR', '2859').

card_in_set('jalum tome', 'CHR').
card_original_type('jalum tome'/'CHR', 'Artifact').
card_original_text('jalum tome'/'CHR', '{2}, {T}: Draw a card; then, choose and discard a card from your hand.').
card_image_name('jalum tome'/'CHR', 'jalum tome').
card_uid('jalum tome'/'CHR', 'CHR:Jalum Tome:jalum tome').
card_rarity('jalum tome'/'CHR', 'Rare').
card_artist('jalum tome'/'CHR', 'Tom Wänerstrand').
card_flavor_text('jalum tome'/'CHR', 'This timeworn relic was responsible for many of Urza\'s victories, though he never fully comprehended its mystic runes.').
card_multiverse_id('jalum tome'/'CHR', '2785').

card_in_set('jeweled bird', 'CHR').
card_original_type('jeweled bird'/'CHR', 'Artifact').
card_original_text('jeweled bird'/'CHR', 'Remove Jeweled Bird from your deck before playing if not playing for ante.\n{T}: Draw a card. Put your contribution to the ante into your graveyard and replace it with Jeweled Bird.').
card_image_name('jeweled bird'/'CHR', 'jeweled bird').
card_uid('jeweled bird'/'CHR', 'CHR:Jeweled Bird:jeweled bird').
card_rarity('jeweled bird'/'CHR', 'Rare').
card_artist('jeweled bird'/'CHR', 'Amy Weber').
card_multiverse_id('jeweled bird'/'CHR', '2786').

card_in_set('johan', 'CHR').
card_original_type('johan'/'CHR', 'Summon — Legend').
card_original_text('johan'/'CHR', 'As long as Johan does not attack and is untapped, attacking does not cause creatures you control to tap.').
card_image_name('johan'/'CHR', 'johan').
card_uid('johan'/'CHR', 'CHR:Johan:johan').
card_rarity('johan'/'CHR', 'Rare').
card_artist('johan'/'CHR', 'Mark Tedin').
card_multiverse_id('johan'/'CHR', '2872').

card_in_set('juxtapose', 'CHR').
card_original_type('juxtapose'/'CHR', 'Sorcery').
card_original_text('juxtapose'/'CHR', 'You and target player each choose one of the creatures you control with the highest casting cost. Exchange control of these creatures. Then do the same for artifacts. If one of the players does not control a creature or artifact, don\'t exchange that type of card.').
card_image_name('juxtapose'/'CHR', 'juxtapose').
card_uid('juxtapose'/'CHR', 'CHR:Juxtapose:juxtapose').
card_rarity('juxtapose'/'CHR', 'Rare').
card_artist('juxtapose'/'CHR', 'Justin Hampton').
card_multiverse_id('juxtapose'/'CHR', '2810').

card_in_set('keepers of the faith', 'CHR').
card_original_type('keepers of the faith'/'CHR', 'Summon — Keepers').
card_original_text('keepers of the faith'/'CHR', '').
card_image_name('keepers of the faith'/'CHR', 'keepers of the faith').
card_uid('keepers of the faith'/'CHR', 'CHR:Keepers of the Faith:keepers of the faith').
card_rarity('keepers of the faith'/'CHR', 'Common').
card_artist('keepers of the faith'/'CHR', 'Daniel Gelon').
card_flavor_text('keepers of the faith'/'CHR', 'And then the Archangel Anthius spoke to them, saying, \"Fear shall be vanquished by the Sword of Faith.\"').
card_multiverse_id('keepers of the faith'/'CHR', '2860').

card_in_set('kei takahashi', 'CHR').
card_original_type('kei takahashi'/'CHR', 'Summon — Legend').
card_original_text('kei takahashi'/'CHR', '{T}: Prevent up to 2 damage to any creature.').
card_image_name('kei takahashi'/'CHR', 'kei takahashi').
card_uid('kei takahashi'/'CHR', 'CHR:Kei Takahashi:kei takahashi').
card_rarity('kei takahashi'/'CHR', 'Uncommon').
card_artist('kei takahashi'/'CHR', 'Scott Kirschner').
card_multiverse_id('kei takahashi'/'CHR', '2873').

card_in_set('land\'s edge', 'CHR').
card_original_type('land\'s edge'/'CHR', 'Enchant World').
card_original_text('land\'s edge'/'CHR', 'Any player may choose and discard a card from his or her hand at any time. If a player discards a land, Land\'s Edge deals 2 damage to target player of that player\'s choice.').
card_image_name('land\'s edge'/'CHR', 'land\'s edge').
card_uid('land\'s edge'/'CHR', 'CHR:Land\'s Edge:land\'s edge').
card_rarity('land\'s edge'/'CHR', 'Rare').
card_artist('land\'s edge'/'CHR', 'Brian Snõddy').
card_multiverse_id('land\'s edge'/'CHR', '2848').

card_in_set('living armor', 'CHR').
card_original_type('living armor'/'CHR', 'Artifact').
card_original_text('living armor'/'CHR', '{T}: Sacrifice Living Armor to put a +0/+X counter on target creature, where X is equal to that creature\'s casting cost.').
card_image_name('living armor'/'CHR', 'living armor').
card_uid('living armor'/'CHR', 'CHR:Living Armor:living armor').
card_rarity('living armor'/'CHR', 'Common').
card_artist('living armor'/'CHR', 'Anson Maddocks').
card_flavor_text('living armor'/'CHR', 'Though it affords excellent protection, few don this armor. The process is uncomfortable and not easily reversed.').
card_multiverse_id('living armor'/'CHR', '2787').

card_in_set('marhault elsdragon', 'CHR').
card_original_type('marhault elsdragon'/'CHR', 'Summon — Legend').
card_original_text('marhault elsdragon'/'CHR', 'Rampage: 1').
card_image_name('marhault elsdragon'/'CHR', 'marhault elsdragon').
card_uid('marhault elsdragon'/'CHR', 'CHR:Marhault Elsdragon:marhault elsdragon').
card_rarity('marhault elsdragon'/'CHR', 'Uncommon').
card_artist('marhault elsdragon'/'CHR', 'Mark Poole').
card_flavor_text('marhault elsdragon'/'CHR', 'Marhault Elsdragon follows a strict philosophy, never letting emotions cloud his thoughts. No chance observer could imagine the rage in his heart.').
card_multiverse_id('marhault elsdragon'/'CHR', '2874').

card_in_set('metamorphosis', 'CHR').
card_original_type('metamorphosis'/'CHR', 'Sorcery').
card_original_text('metamorphosis'/'CHR', 'Sacrifice a creature to add an amount of mana equal to its casting cost plus one to your mana pool. This mana may be of any one color. Use this mana only to cast summon spells.').
card_image_name('metamorphosis'/'CHR', 'metamorphosis').
card_uid('metamorphosis'/'CHR', 'CHR:Metamorphosis:metamorphosis').
card_rarity('metamorphosis'/'CHR', 'Common').
card_artist('metamorphosis'/'CHR', 'Christopher Rush').
card_multiverse_id('metamorphosis'/'CHR', '2833').

card_in_set('mountain yeti', 'CHR').
card_original_type('mountain yeti'/'CHR', 'Summon — Yeti').
card_original_text('mountain yeti'/'CHR', 'Mountainwalk, protection from white').
card_image_name('mountain yeti'/'CHR', 'mountain yeti').
card_uid('mountain yeti'/'CHR', 'CHR:Mountain Yeti:mountain yeti').
card_rarity('mountain yeti'/'CHR', 'Common').
card_artist('mountain yeti'/'CHR', 'Dan Frazier').
card_flavor_text('mountain yeti'/'CHR', 'The Yeti\'s single greatest asset is its unnerving ability to blend in with its surroundings.').
card_multiverse_id('mountain yeti'/'CHR', '2849').

card_in_set('nebuchadnezzar', 'CHR').
card_original_type('nebuchadnezzar'/'CHR', 'Summon — Legend').
card_original_text('nebuchadnezzar'/'CHR', '{X}, {T}: Name a card. Target opponent reveals X cards from his or her hand at random. If that player does not have enough cards in hand, his or her entire hand is revealed. Opponent then discards any of those cards that match the named card. Use this ability only during your turn.').
card_image_name('nebuchadnezzar'/'CHR', 'nebuchadnezzar').
card_uid('nebuchadnezzar'/'CHR', 'CHR:Nebuchadnezzar:nebuchadnezzar').
card_rarity('nebuchadnezzar'/'CHR', 'Rare').
card_artist('nebuchadnezzar'/'CHR', 'Richard Kane Ferguson').
card_multiverse_id('nebuchadnezzar'/'CHR', '2875').

card_in_set('nicol bolas', 'CHR').
card_original_type('nicol bolas'/'CHR', 'Summon — Elder Dragon Legend').
card_original_text('nicol bolas'/'CHR', 'Flying\nWhenever Nicol Bolas damages an opponent, that opponent discards his or her entire hand. Ignore this ability if that opponent has no cards left in hand.\nDuring your upkeep, pay {U}{B}{R} or bury Nicol Bolas.').
card_image_name('nicol bolas'/'CHR', 'nicol bolas').
card_uid('nicol bolas'/'CHR', 'CHR:Nicol Bolas:nicol bolas').
card_rarity('nicol bolas'/'CHR', 'Rare').
card_artist('nicol bolas'/'CHR', 'Edward P. Beard, Jr.').
card_multiverse_id('nicol bolas'/'CHR', '2876').

card_in_set('obelisk of undoing', 'CHR').
card_original_type('obelisk of undoing'/'CHR', 'Artifact').
card_original_text('obelisk of undoing'/'CHR', '{6}, {T}: Return target permanent you control and own to your hand.').
card_image_name('obelisk of undoing'/'CHR', 'obelisk of undoing').
card_uid('obelisk of undoing'/'CHR', 'CHR:Obelisk of Undoing:obelisk of undoing').
card_rarity('obelisk of undoing'/'CHR', 'Rare').
card_artist('obelisk of undoing'/'CHR', 'Tom Wänerstrand').
card_flavor_text('obelisk of undoing'/'CHR', 'The Battle of Tomakul taught Urza not to rely on fickle reinforcements.').
card_multiverse_id('obelisk of undoing'/'CHR', '2788').

card_in_set('palladia-mors', 'CHR').
card_original_type('palladia-mors'/'CHR', 'Summon — Elder Dragon Legend').
card_original_text('palladia-mors'/'CHR', 'Flying, trample\nDuring your upkeep, pay {W}{R}{G} or bury Palladia-Mors.').
card_image_name('palladia-mors'/'CHR', 'palladia-mors').
card_uid('palladia-mors'/'CHR', 'CHR:Palladia-Mors:palladia-mors').
card_rarity('palladia-mors'/'CHR', 'Rare').
card_artist('palladia-mors'/'CHR', 'Edward P. Beard, Jr.').
card_multiverse_id('palladia-mors'/'CHR', '2877').

card_in_set('petra sphinx', 'CHR').
card_original_type('petra sphinx'/'CHR', 'Summon — Sphinx').
card_original_text('petra sphinx'/'CHR', '{T}: Target player names a card and then turns over the top card of his or her library. If it is the card named, put it into that player\'s hand. Otherwise, put it into the player\'s graveyard.').
card_image_name('petra sphinx'/'CHR', 'petra sphinx').
card_uid('petra sphinx'/'CHR', 'CHR:Petra Sphinx:petra sphinx').
card_rarity('petra sphinx'/'CHR', 'Rare').
card_artist('petra sphinx'/'CHR', 'Sandra Everingham').
card_multiverse_id('petra sphinx'/'CHR', '2861').

card_in_set('primordial ooze', 'CHR').
card_original_type('primordial ooze'/'CHR', 'Summon — Ooze').
card_original_text('primordial ooze'/'CHR', 'Must attack each turn if possible.\nAt the beginning of your upkeep, put a +1/+1 counter on Primordial Ooze. At the end of your upkeep, pay X, where X is equal to the number of +1/+1 counters on Primordial Ooze, or Primordial Ooze deals X damage to you. If Primordial Ooze deals damage to you in this way, tap it.').
card_image_name('primordial ooze'/'CHR', 'primordial ooze').
card_uid('primordial ooze'/'CHR', 'CHR:Primordial Ooze:primordial ooze').
card_rarity('primordial ooze'/'CHR', 'Uncommon').
card_artist('primordial ooze'/'CHR', 'Sandra Everingham').
card_multiverse_id('primordial ooze'/'CHR', '2850').

card_in_set('puppet master', 'CHR').
card_original_type('puppet master'/'CHR', 'Enchant Creature').
card_original_text('puppet master'/'CHR', 'If target creature is put into the graveyard, return it to its owner\'s hand.\n{U}{U}{U} Return Puppet Master to its owner\'s hand. Use this ability only when the creature Puppet Master enchants returns to its owner\'s hand.').
card_image_name('puppet master'/'CHR', 'puppet master').
card_uid('puppet master'/'CHR', 'CHR:Puppet Master:puppet master').
card_rarity('puppet master'/'CHR', 'Uncommon').
card_artist('puppet master'/'CHR', 'Sandra Everingham').
card_multiverse_id('puppet master'/'CHR', '2811').

card_in_set('rabid wombat', 'CHR').
card_original_type('rabid wombat'/'CHR', 'Summon — Wombat').
card_original_text('rabid wombat'/'CHR', 'Rabid Wombat gets +2/+2 for each creature enchantment on it. Attacking does not cause Rabid Wombat to tap.').
card_image_name('rabid wombat'/'CHR', 'rabid wombat').
card_uid('rabid wombat'/'CHR', 'CHR:Rabid Wombat:rabid wombat').
card_rarity('rabid wombat'/'CHR', 'Uncommon').
card_artist('rabid wombat'/'CHR', 'Kaja Foglio').
card_multiverse_id('rabid wombat'/'CHR', '2834').

card_in_set('rakalite', 'CHR').
card_original_type('rakalite'/'CHR', 'Artifact').
card_original_text('rakalite'/'CHR', '{2}: Prevent 1 damage to any creature or player. Return Rakalite to owner\'s hand at end of turn.').
card_image_name('rakalite'/'CHR', 'rakalite').
card_uid('rakalite'/'CHR', 'CHR:Rakalite:rakalite').
card_rarity('rakalite'/'CHR', 'Rare').
card_artist('rakalite'/'CHR', 'Christopher Rush').
card_flavor_text('rakalite'/'CHR', 'Urza was the first to understand that the war would not be lost for lack of power, but for lack of troops.').
card_multiverse_id('rakalite'/'CHR', '2789').

card_in_set('recall', 'CHR').
card_original_type('recall'/'CHR', 'Sorcery').
card_original_text('recall'/'CHR', 'Choose and discard X cards from your hand to take X target cards from your graveyard and put them into your hand. Remove Recall from the game.').
card_image_name('recall'/'CHR', 'recall').
card_uid('recall'/'CHR', 'CHR:Recall:recall').
card_rarity('recall'/'CHR', 'Uncommon').
card_artist('recall'/'CHR', 'Brian Snõddy').
card_multiverse_id('recall'/'CHR', '2812').

card_in_set('remove soul', 'CHR').
card_original_type('remove soul'/'CHR', 'Interrupt').
card_original_text('remove soul'/'CHR', 'Counter target summon spell.').
card_image_name('remove soul'/'CHR', 'remove soul').
card_uid('remove soul'/'CHR', 'CHR:Remove Soul:remove soul').
card_rarity('remove soul'/'CHR', 'Common').
card_artist('remove soul'/'CHR', 'Brian Snõddy').
card_flavor_text('remove soul'/'CHR', 'Nethya stiffened suddenly, head cocked as if straining to hear some distant sound, then fell lifeless to the ground.').
card_multiverse_id('remove soul'/'CHR', '2813').

card_in_set('repentant blacksmith', 'CHR').
card_original_type('repentant blacksmith'/'CHR', 'Summon — Smith').
card_original_text('repentant blacksmith'/'CHR', 'Protection from red').
card_image_name('repentant blacksmith'/'CHR', 'repentant blacksmith').
card_uid('repentant blacksmith'/'CHR', 'CHR:Repentant Blacksmith:repentant blacksmith').
card_rarity('repentant blacksmith'/'CHR', 'Common').
card_artist('repentant blacksmith'/'CHR', 'Drew Tucker').
card_flavor_text('repentant blacksmith'/'CHR', '\"For my confession they burned me with fire\nAnd found that I was for endurance made.\"\n—The Arabian Nights, trans. Haddawy').
card_multiverse_id('repentant blacksmith'/'CHR', '2862').

card_in_set('revelation', 'CHR').
card_original_type('revelation'/'CHR', 'Enchant World').
card_original_text('revelation'/'CHR', 'All players play with the cards in their hands face up on the table.').
card_image_name('revelation'/'CHR', 'revelation').
card_uid('revelation'/'CHR', 'CHR:Revelation:revelation').
card_rarity('revelation'/'CHR', 'Rare').
card_artist('revelation'/'CHR', 'Kaja Foglio').
card_flavor_text('revelation'/'CHR', '\"Many are in high place, and of renown: but mysteries are revealed unto the meek.\" —Ecclesiasticus 3:19').
card_multiverse_id('revelation'/'CHR', '2835').

card_in_set('rubinia soulsinger', 'CHR').
card_original_type('rubinia soulsinger'/'CHR', 'Summon — Legend').
card_original_text('rubinia soulsinger'/'CHR', 'You may choose not to untap Rubinia Soulsinger during your untap phase.\n{T}: Gain control of target creature. Lose control of target creature if Rubinia leaves play, if you lose control of Rubinia, or if Rubinia becomes untapped.').
card_image_name('rubinia soulsinger'/'CHR', 'rubinia soulsinger').
card_uid('rubinia soulsinger'/'CHR', 'CHR:Rubinia Soulsinger:rubinia soulsinger').
card_rarity('rubinia soulsinger'/'CHR', 'Rare').
card_artist('rubinia soulsinger'/'CHR', 'Rob Alexander').
card_multiverse_id('rubinia soulsinger'/'CHR', '2878').

card_in_set('runesword', 'CHR').
card_original_type('runesword'/'CHR', 'Artifact').
card_original_text('runesword'/'CHR', '{3}, {T}: Target attacking creature gets +2/+0 until end of turn. Any creature damaged by that creature cannot regenerate this turn; if such a creature receives lethal damage this turn, remove it from the game. If the target leaves play before end of turn, bury Runesword.').
card_image_name('runesword'/'CHR', 'runesword').
card_uid('runesword'/'CHR', 'CHR:Runesword:runesword').
card_rarity('runesword'/'CHR', 'Common').
card_artist('runesword'/'CHR', 'Christopher Rush').
card_multiverse_id('runesword'/'CHR', '2790').

card_in_set('safe haven', 'CHR').
card_original_type('safe haven'/'CHR', 'Land').
card_original_text('safe haven'/'CHR', '{2}, {T}: Remove target creature you control from the game. Play this ability as an interrupt.\n{0}: Sacrifice Safe Haven to return all creatures it has removed from the game directly into play under their owners\' control as though they were just summoned. Use this ability during your upkeep.').
card_image_name('safe haven'/'CHR', 'safe haven').
card_uid('safe haven'/'CHR', 'CHR:Safe Haven:safe haven').
card_rarity('safe haven'/'CHR', 'Rare').
card_artist('safe haven'/'CHR', 'Christopher Rush').
card_multiverse_id('safe haven'/'CHR', '2887').

card_in_set('scavenger folk', 'CHR').
card_original_type('scavenger folk'/'CHR', 'Summon — Scavenger Folk').
card_original_text('scavenger folk'/'CHR', '{G}, {T}: Sacrifice Scavenger Folk to destroy target artifact.').
card_image_name('scavenger folk'/'CHR', 'scavenger folk').
card_uid('scavenger folk'/'CHR', 'CHR:Scavenger Folk:scavenger folk').
card_rarity('scavenger folk'/'CHR', 'Common').
card_artist('scavenger folk'/'CHR', 'Dennis Detwiller').
card_flavor_text('scavenger folk'/'CHR', 'String, weapons, wax, or jewels—it makes no difference. Leave nothing unguarded in Scarwood.').
card_multiverse_id('scavenger folk'/'CHR', '2836').

card_in_set('sentinel', 'CHR').
card_original_type('sentinel'/'CHR', 'Artifact Creature').
card_original_text('sentinel'/'CHR', 'When Sentinel comes into play, its toughness is equal to 1.\n{0}: Change Sentinel\'s toughness to 1 plus the power of target creature blocking or blocked by Sentinel.').
card_image_name('sentinel'/'CHR', 'sentinel').
card_uid('sentinel'/'CHR', 'CHR:Sentinel:sentinel').
card_rarity('sentinel'/'CHR', 'Rare').
card_artist('sentinel'/'CHR', 'Randy Asplund-Faith').
card_multiverse_id('sentinel'/'CHR', '2791').

card_in_set('serpent generator', 'CHR').
card_original_type('serpent generator'/'CHR', 'Artifact').
card_original_text('serpent generator'/'CHR', '{4}, {T}: Put a Poison Snake token into play. Treat this token as a 1/1 artifact creature. Whenever a Poison Snake damages a player, he or she gets a poison counter. When a player has ten or more poison counters, he or she loses the game.').
card_image_name('serpent generator'/'CHR', 'serpent generator').
card_uid('serpent generator'/'CHR', 'CHR:Serpent Generator:serpent generator').
card_rarity('serpent generator'/'CHR', 'Rare').
card_artist('serpent generator'/'CHR', 'Mark Tedin').
card_multiverse_id('serpent generator'/'CHR', '2792').

card_in_set('shield wall', 'CHR').
card_original_type('shield wall'/'CHR', 'Instant').
card_original_text('shield wall'/'CHR', 'All creatures you control get +0/+2 until end of turn.').
card_image_name('shield wall'/'CHR', 'shield wall').
card_uid('shield wall'/'CHR', 'CHR:Shield Wall:shield wall').
card_rarity('shield wall'/'CHR', 'Uncommon').
card_artist('shield wall'/'CHR', 'Douglas Shuler').
card_multiverse_id('shield wall'/'CHR', '2863').

card_in_set('shimian night stalker', 'CHR').
card_original_type('shimian night stalker'/'CHR', 'Summon — Night Stalker').
card_original_text('shimian night stalker'/'CHR', '{B}, {T}: Redirect to Shimian Night Stalker all damage dealt to you by any attacking creature. The source of the damage does not change.').
card_image_name('shimian night stalker'/'CHR', 'shimian night stalker').
card_uid('shimian night stalker'/'CHR', 'CHR:Shimian Night Stalker:shimian night stalker').
card_rarity('shimian night stalker'/'CHR', 'Uncommon').
card_artist('shimian night stalker'/'CHR', 'Jesper Myrfors').
card_flavor_text('shimian night stalker'/'CHR', '\"When churchyards yawn and hell itself breathes out/ Contagion to this world.\"\n—William Shakespeare, Hamlet').
card_multiverse_id('shimian night stalker'/'CHR', '2814').

card_in_set('sivitri scarzam', 'CHR').
card_original_type('sivitri scarzam'/'CHR', 'Summon — Legend').
card_original_text('sivitri scarzam'/'CHR', '').
card_image_name('sivitri scarzam'/'CHR', 'sivitri scarzam').
card_uid('sivitri scarzam'/'CHR', 'CHR:Sivitri Scarzam:sivitri scarzam').
card_rarity('sivitri scarzam'/'CHR', 'Uncommon').
card_artist('sivitri scarzam'/'CHR', 'NéNé Thomas').
card_flavor_text('sivitri scarzam'/'CHR', 'Even the brave have cause to tremble at the sight of Sivitri Scarzam. Who else has tamed Scarzam\'s Dragon?').
card_multiverse_id('sivitri scarzam'/'CHR', '2879').

card_in_set('sol\'kanar the swamp king', 'CHR').
card_original_type('sol\'kanar the swamp king'/'CHR', 'Summon — Legend').
card_original_text('sol\'kanar the swamp king'/'CHR', 'Swampwalk\nWhenever a black spell is successfully cast, gain 1 life.').
card_image_name('sol\'kanar the swamp king'/'CHR', 'sol\'kanar the swamp king').
card_uid('sol\'kanar the swamp king'/'CHR', 'CHR:Sol\'kanar the Swamp King:sol\'kanar the swamp king').
card_rarity('sol\'kanar the swamp king'/'CHR', 'Rare').
card_artist('sol\'kanar the swamp king'/'CHR', 'Richard Kane Ferguson').
card_multiverse_id('sol\'kanar the swamp king'/'CHR', '2880').

card_in_set('stangg', 'CHR').
card_original_type('stangg'/'CHR', 'Summon — Legend').
card_original_text('stangg'/'CHR', 'When Stangg comes into play, put a Stangg Twin token into play. Treat this token as a 3/4 green and red legend. If Stangg leaves play, remove Stangg Twin token from the game. If Stangg Twin token leaves play, bury Stangg.').
card_image_name('stangg'/'CHR', 'stangg').
card_uid('stangg'/'CHR', 'CHR:Stangg:stangg').
card_rarity('stangg'/'CHR', 'Rare').
card_artist('stangg'/'CHR', 'Mark Poole').
card_multiverse_id('stangg'/'CHR', '2881').

card_in_set('storm seeker', 'CHR').
card_original_type('storm seeker'/'CHR', 'Instant').
card_original_text('storm seeker'/'CHR', 'Storm Seeker deals 1 damage to target player for each card in his or her hand.').
card_image_name('storm seeker'/'CHR', 'storm seeker').
card_uid('storm seeker'/'CHR', 'CHR:Storm Seeker:storm seeker').
card_rarity('storm seeker'/'CHR', 'Uncommon').
card_artist('storm seeker'/'CHR', 'Mark Poole').
card_multiverse_id('storm seeker'/'CHR', '2837').

card_in_set('takklemaggot', 'CHR').
card_original_type('takklemaggot'/'CHR', 'Enchant Creature').
card_original_text('takklemaggot'/'CHR', 'During target creature\'s controller\'s upkeep, put a -0/-1 counter on that creature. If the creature is put into the graveyard, its controller chooses a new target creature for Takklemaggot. If there are no legal targets, Takklemaggot becomes an enchantment; during his or her upkeep, Takklemaggot deals 1 damage to the controller of the last creature Takklemaggot enchanted. Control of Takklemaggot does not change when its target changes or when it becomes an enchantment.').
card_image_name('takklemaggot'/'CHR', 'takklemaggot').
card_uid('takklemaggot'/'CHR', 'CHR:Takklemaggot:takklemaggot').
card_rarity('takklemaggot'/'CHR', 'Uncommon').
card_artist('takklemaggot'/'CHR', 'Daniel Gelon').
card_multiverse_id('takklemaggot'/'CHR', '2815').

card_in_set('teleport', 'CHR').
card_original_type('teleport'/'CHR', 'Instant').
card_original_text('teleport'/'CHR', 'Target creature becomes unblockable until end of turn. Cast only after attack is declared and before defense is chosen.').
card_image_name('teleport'/'CHR', 'teleport').
card_uid('teleport'/'CHR', 'CHR:Teleport:teleport').
card_rarity('teleport'/'CHR', 'Rare').
card_artist('teleport'/'CHR', 'Douglas Shuler').
card_multiverse_id('teleport'/'CHR', '2816').

card_in_set('the fallen', 'CHR').
card_original_type('the fallen'/'CHR', 'Summon — Fallen').
card_original_text('the fallen'/'CHR', 'During your upkeep, The Fallen deals 1 damage to each opponent it has previously damaged.').
card_image_name('the fallen'/'CHR', 'the fallen').
card_uid('the fallen'/'CHR', 'CHR:The Fallen:the fallen').
card_rarity('the fallen'/'CHR', 'Uncommon').
card_artist('the fallen'/'CHR', 'Jesper Myrfors').
card_flavor_text('the fallen'/'CHR', 'Magic often masters those who cannot master it.').
card_multiverse_id('the fallen'/'CHR', '2817').

card_in_set('the wretched', 'CHR').
card_original_type('the wretched'/'CHR', 'Summon — Wretched').
card_original_text('the wretched'/'CHR', 'At end of combat, gain control of all creatures blocking The Wretched. Lose control of these creatures if The Wretched leaves play or if you lose control of The Wretched.').
card_image_name('the wretched'/'CHR', 'the wretched').
card_uid('the wretched'/'CHR', 'CHR:The Wretched:the wretched').
card_rarity('the wretched'/'CHR', 'Rare').
card_artist('the wretched'/'CHR', 'Christopher Rush').
card_multiverse_id('the wretched'/'CHR', '2818').

card_in_set('tobias andrion', 'CHR').
card_original_type('tobias andrion'/'CHR', 'Summon — Legend').
card_original_text('tobias andrion'/'CHR', '').
card_image_name('tobias andrion'/'CHR', 'tobias andrion').
card_uid('tobias andrion'/'CHR', 'CHR:Tobias Andrion:tobias andrion').
card_rarity('tobias andrion'/'CHR', 'Uncommon').
card_artist('tobias andrion'/'CHR', 'Andi Rusu').
card_flavor_text('tobias andrion'/'CHR', 'Administrator of the military state of Sheoltun, Tobias Andrion is the military right arm of the empire and the figurehead of its freedom.').
card_multiverse_id('tobias andrion'/'CHR', '2882').

card_in_set('tor wauki', 'CHR').
card_original_type('tor wauki'/'CHR', 'Summon — Legend').
card_original_text('tor wauki'/'CHR', '{T}: Tor Wauki deals 2 damage to target attacking or blocking creature.').
card_image_name('tor wauki'/'CHR', 'tor wauki').
card_uid('tor wauki'/'CHR', 'CHR:Tor Wauki:tor wauki').
card_rarity('tor wauki'/'CHR', 'Uncommon').
card_artist('tor wauki'/'CHR', 'Randy Asplund-Faith').
card_multiverse_id('tor wauki'/'CHR', '2883').

card_in_set('tormod\'s crypt', 'CHR').
card_original_type('tormod\'s crypt'/'CHR', 'Artifact').
card_original_text('tormod\'s crypt'/'CHR', '{T}: Sacrifice Tormod\'s Crypt to remove all cards in target player\'s graveyard from the game.').
card_image_name('tormod\'s crypt'/'CHR', 'tormod\'s crypt').
card_uid('tormod\'s crypt'/'CHR', 'CHR:Tormod\'s Crypt:tormod\'s crypt').
card_rarity('tormod\'s crypt'/'CHR', 'Common').
card_artist('tormod\'s crypt'/'CHR', 'Christopher Rush').
card_flavor_text('tormod\'s crypt'/'CHR', 'The dark opening seemed to breathe the cold, damp air of the dead earth in a steady rhythm.').
card_multiverse_id('tormod\'s crypt'/'CHR', '2793').

card_in_set('transmutation', 'CHR').
card_original_type('transmutation'/'CHR', 'Instant').
card_original_text('transmutation'/'CHR', 'Until end of turn, switch target creature\'s power and toughness. Effects that alter power alter toughness instead, and vice versa.').
card_image_name('transmutation'/'CHR', 'transmutation').
card_uid('transmutation'/'CHR', 'CHR:Transmutation:transmutation').
card_rarity('transmutation'/'CHR', 'Common').
card_artist('transmutation'/'CHR', 'Susan Van Camp').
card_flavor_text('transmutation'/'CHR', '\"You know what I was,/ You see what I am: change me, change me!\"\n—Randall Jarrell, The Woman at the Washington Zoo').
card_multiverse_id('transmutation'/'CHR', '2819').

card_in_set('triassic egg', 'CHR').
card_original_type('triassic egg'/'CHR', 'Artifact').
card_original_text('triassic egg'/'CHR', '{3}, {T}: Put a hatchling counter on Triassic Egg.\n{0}: Sacrifice Triassic Egg to take target creature from your hand or graveyard and put it directly into play as though it were just summoned. Use this ability only when there are at least two hatchling counters on Triassic Egg.').
card_image_name('triassic egg'/'CHR', 'triassic egg').
card_uid('triassic egg'/'CHR', 'CHR:Triassic Egg:triassic egg').
card_rarity('triassic egg'/'CHR', 'Rare').
card_artist('triassic egg'/'CHR', 'Dan Frazier').
card_multiverse_id('triassic egg'/'CHR', '2794').

card_in_set('urza\'s mine', 'CHR').
card_original_type('urza\'s mine'/'CHR', 'Land').
card_original_text('urza\'s mine'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s mine'/'CHR', 'urza\'s mine1').
card_uid('urza\'s mine'/'CHR', 'CHR:Urza\'s Mine:urza\'s mine1').
card_rarity('urza\'s mine'/'CHR', 'Uncommon').
card_artist('urza\'s mine'/'CHR', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'CHR', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'CHR', '2890').

card_in_set('urza\'s mine', 'CHR').
card_original_type('urza\'s mine'/'CHR', 'Land').
card_original_text('urza\'s mine'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s mine'/'CHR', 'urza\'s mine2').
card_uid('urza\'s mine'/'CHR', 'CHR:Urza\'s Mine:urza\'s mine2').
card_rarity('urza\'s mine'/'CHR', 'Uncommon').
card_artist('urza\'s mine'/'CHR', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'CHR', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'CHR', '2888').

card_in_set('urza\'s mine', 'CHR').
card_original_type('urza\'s mine'/'CHR', 'Land').
card_original_text('urza\'s mine'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s mine'/'CHR', 'urza\'s mine3').
card_uid('urza\'s mine'/'CHR', 'CHR:Urza\'s Mine:urza\'s mine3').
card_rarity('urza\'s mine'/'CHR', 'Uncommon').
card_artist('urza\'s mine'/'CHR', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'CHR', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'CHR', '2891').

card_in_set('urza\'s mine', 'CHR').
card_original_type('urza\'s mine'/'CHR', 'Land').
card_original_text('urza\'s mine'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s mine'/'CHR', 'urza\'s mine4').
card_uid('urza\'s mine'/'CHR', 'CHR:Urza\'s Mine:urza\'s mine4').
card_rarity('urza\'s mine'/'CHR', 'Uncommon').
card_artist('urza\'s mine'/'CHR', 'Anson Maddocks').
card_flavor_text('urza\'s mine'/'CHR', 'Mines became common as cities during the days of the artificers.').
card_multiverse_id('urza\'s mine'/'CHR', '2889').

card_in_set('urza\'s power plant', 'CHR').
card_original_type('urza\'s power plant'/'CHR', 'Land').
card_original_text('urza\'s power plant'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s power plant'/'CHR', 'urza\'s power plant1').
card_uid('urza\'s power plant'/'CHR', 'CHR:Urza\'s Power Plant:urza\'s power plant1').
card_rarity('urza\'s power plant'/'CHR', 'Uncommon').
card_artist('urza\'s power plant'/'CHR', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'CHR', 'Artifact construction required immense resources.').
card_multiverse_id('urza\'s power plant'/'CHR', '2894').

card_in_set('urza\'s power plant', 'CHR').
card_original_type('urza\'s power plant'/'CHR', 'Land').
card_original_text('urza\'s power plant'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s power plant'/'CHR', 'urza\'s power plant2').
card_uid('urza\'s power plant'/'CHR', 'CHR:Urza\'s Power Plant:urza\'s power plant2').
card_rarity('urza\'s power plant'/'CHR', 'Uncommon').
card_artist('urza\'s power plant'/'CHR', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'CHR', 'Artifact construction required immense resources.').
card_multiverse_id('urza\'s power plant'/'CHR', '2893').

card_in_set('urza\'s power plant', 'CHR').
card_original_type('urza\'s power plant'/'CHR', 'Land').
card_original_text('urza\'s power plant'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s power plant'/'CHR', 'urza\'s power plant3').
card_uid('urza\'s power plant'/'CHR', 'CHR:Urza\'s Power Plant:urza\'s power plant3').
card_rarity('urza\'s power plant'/'CHR', 'Uncommon').
card_artist('urza\'s power plant'/'CHR', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'CHR', 'Artifact construction required immense resources.').
card_multiverse_id('urza\'s power plant'/'CHR', '2892').

card_in_set('urza\'s power plant', 'CHR').
card_original_type('urza\'s power plant'/'CHR', 'Land').
card_original_text('urza\'s power plant'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add two colorless mana to your mana pool instead of one.').
card_image_name('urza\'s power plant'/'CHR', 'urza\'s power plant4').
card_uid('urza\'s power plant'/'CHR', 'CHR:Urza\'s Power Plant:urza\'s power plant4').
card_rarity('urza\'s power plant'/'CHR', 'Uncommon').
card_artist('urza\'s power plant'/'CHR', 'Mark Tedin').
card_flavor_text('urza\'s power plant'/'CHR', 'Artifact construction required immense resources.').
card_multiverse_id('urza\'s power plant'/'CHR', '2895').

card_in_set('urza\'s tower', 'CHR').
card_original_type('urza\'s tower'/'CHR', 'Land').
card_original_text('urza\'s tower'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add three colorless mana to your mana pool instead of one.').
card_image_name('urza\'s tower'/'CHR', 'urza\'s tower1').
card_uid('urza\'s tower'/'CHR', 'CHR:Urza\'s Tower:urza\'s tower1').
card_rarity('urza\'s tower'/'CHR', 'Uncommon').
card_artist('urza\'s tower'/'CHR', 'Mark Poole').
card_flavor_text('urza\'s tower'/'CHR', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'CHR', '2899').

card_in_set('urza\'s tower', 'CHR').
card_original_type('urza\'s tower'/'CHR', 'Land').
card_original_text('urza\'s tower'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add three colorless mana to your mana pool instead of one.').
card_image_name('urza\'s tower'/'CHR', 'urza\'s tower2').
card_uid('urza\'s tower'/'CHR', 'CHR:Urza\'s Tower:urza\'s tower2').
card_rarity('urza\'s tower'/'CHR', 'Uncommon').
card_artist('urza\'s tower'/'CHR', 'Mark Poole').
card_flavor_text('urza\'s tower'/'CHR', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'CHR', '2898').

card_in_set('urza\'s tower', 'CHR').
card_original_type('urza\'s tower'/'CHR', 'Land').
card_original_text('urza\'s tower'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add three colorless mana to your mana pool instead of one.').
card_image_name('urza\'s tower'/'CHR', 'urza\'s tower3').
card_uid('urza\'s tower'/'CHR', 'CHR:Urza\'s Tower:urza\'s tower3').
card_rarity('urza\'s tower'/'CHR', 'Uncommon').
card_artist('urza\'s tower'/'CHR', 'Mark Poole').
card_flavor_text('urza\'s tower'/'CHR', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'CHR', '2896').

card_in_set('urza\'s tower', 'CHR').
card_original_type('urza\'s tower'/'CHR', 'Land').
card_original_text('urza\'s tower'/'CHR', '{T}: Add one colorless mana to your mana pool. If you control Urza\'s Mine, Urza\'s Tower, and Urza\'s Power Plant, add three colorless mana to your mana pool instead of one.').
card_image_name('urza\'s tower'/'CHR', 'urza\'s tower4').
card_uid('urza\'s tower'/'CHR', 'CHR:Urza\'s Tower:urza\'s tower4').
card_rarity('urza\'s tower'/'CHR', 'Uncommon').
card_artist('urza\'s tower'/'CHR', 'Mark Poole').
card_flavor_text('urza\'s tower'/'CHR', 'Urza always put Tocasia\'s lessons on resource-gathering to effective use.').
card_multiverse_id('urza\'s tower'/'CHR', '2897').

card_in_set('vaevictis asmadi', 'CHR').
card_original_type('vaevictis asmadi'/'CHR', 'Summon — Elder Dragon Legend').
card_original_text('vaevictis asmadi'/'CHR', 'Flying\nDuring your upkeep, pay {B}{R}{G} or bury Vaevictis Asmadi.\n{B} +1/+0 until end of turn\n{R} +1/+0 until end of turn\n{G} +1/+0 until end of turn').
card_image_name('vaevictis asmadi'/'CHR', 'vaevictis asmadi').
card_uid('vaevictis asmadi'/'CHR', 'CHR:Vaevictis Asmadi:vaevictis asmadi').
card_rarity('vaevictis asmadi'/'CHR', 'Rare').
card_artist('vaevictis asmadi'/'CHR', 'Andi Rusu').
card_multiverse_id('vaevictis asmadi'/'CHR', '2884').

card_in_set('voodoo doll', 'CHR').
card_original_type('voodoo doll'/'CHR', 'Artifact').
card_original_text('voodoo doll'/'CHR', 'At the beginning of your upkeep, put one pin counter on Voodoo Doll. If Voodoo Doll is untapped at the end of your turn, it deals X damage to you, where X is equal to the number of pin counters on Voodoo Doll. If Voodoo Doll deals damage to you in this way, destroy it.\n{X}{X}, {T}: Voodoo Doll deals X damage to target creature or player, where X is equal to the number of pin counters on Voodoo Doll.').
card_image_name('voodoo doll'/'CHR', 'voodoo doll').
card_uid('voodoo doll'/'CHR', 'CHR:Voodoo Doll:voodoo doll').
card_rarity('voodoo doll'/'CHR', 'Rare').
card_artist('voodoo doll'/'CHR', 'Sandra Everingham').
card_multiverse_id('voodoo doll'/'CHR', '2795').

card_in_set('wall of heat', 'CHR').
card_original_type('wall of heat'/'CHR', 'Summon — Wall').
card_original_text('wall of heat'/'CHR', '').
card_image_name('wall of heat'/'CHR', 'wall of heat').
card_uid('wall of heat'/'CHR', 'CHR:Wall of Heat:wall of heat').
card_rarity('wall of heat'/'CHR', 'Common').
card_artist('wall of heat'/'CHR', 'Richard Thomas').
card_flavor_text('wall of heat'/'CHR', 'At a distance, we mistook the sound for a waterfall . . . .').
card_multiverse_id('wall of heat'/'CHR', '2851').

card_in_set('wall of opposition', 'CHR').
card_original_type('wall of opposition'/'CHR', 'Summon — Wall').
card_original_text('wall of opposition'/'CHR', '{1}: +1/+0 until end of turn').
card_image_name('wall of opposition'/'CHR', 'wall of opposition').
card_uid('wall of opposition'/'CHR', 'CHR:Wall of Opposition:wall of opposition').
card_rarity('wall of opposition'/'CHR', 'Uncommon').
card_artist('wall of opposition'/'CHR', 'Harold McNeill').
card_flavor_text('wall of opposition'/'CHR', 'Like so many obstacles in life, the Wall of Opposition is but an illusion, held fast by the focus and belief of the one who creates it.').
card_multiverse_id('wall of opposition'/'CHR', '2852').

card_in_set('wall of shadows', 'CHR').
card_original_type('wall of shadows'/'CHR', 'Summon — Wall').
card_original_text('wall of shadows'/'CHR', 'Damage dealt to Wall of Shadows by creatures it blocks is reduced to 0. Wall of Shadows cannot be the target of spells or effects that can target only walls.').
card_image_name('wall of shadows'/'CHR', 'wall of shadows').
card_uid('wall of shadows'/'CHR', 'CHR:Wall of Shadows:wall of shadows').
card_rarity('wall of shadows'/'CHR', 'Common').
card_artist('wall of shadows'/'CHR', 'Pete Venters').
card_multiverse_id('wall of shadows'/'CHR', '2820').

card_in_set('wall of vapor', 'CHR').
card_original_type('wall of vapor'/'CHR', 'Summon — Wall').
card_original_text('wall of vapor'/'CHR', 'Damage dealt to Wall of Vapor by creatures it blocks is reduced to 0.').
card_image_name('wall of vapor'/'CHR', 'wall of vapor').
card_uid('wall of vapor'/'CHR', 'CHR:Wall of Vapor:wall of vapor').
card_rarity('wall of vapor'/'CHR', 'Common').
card_artist('wall of vapor'/'CHR', 'Richard Thomas').
card_flavor_text('wall of vapor'/'CHR', '\"Walls of a castle are made out of stone,/ Walls of a house out of bricks or of wood./ My walls are made out of magic alone,/ Stronger than any that ever have stood.\" —Chrysoberyl Earthsdaughter, Incantations').
card_multiverse_id('wall of vapor'/'CHR', '2821').

card_in_set('wall of wonder', 'CHR').
card_original_type('wall of wonder'/'CHR', 'Summon — Wall').
card_original_text('wall of wonder'/'CHR', '{2}{U}{U} +4/-4 until end of turn; Wall of Wonder can attack this turn.').
card_image_name('wall of wonder'/'CHR', 'wall of wonder').
card_uid('wall of wonder'/'CHR', 'CHR:Wall of Wonder:wall of wonder').
card_rarity('wall of wonder'/'CHR', 'Uncommon').
card_artist('wall of wonder'/'CHR', 'Richard Thomas').
card_flavor_text('wall of wonder'/'CHR', 'So confusing is the Wall\'s appearance that few of its victims even see it move.').
card_multiverse_id('wall of wonder'/'CHR', '2822').

card_in_set('war elephant', 'CHR').
card_original_type('war elephant'/'CHR', 'Summon — Elephant').
card_original_text('war elephant'/'CHR', 'Banding, trample').
card_image_name('war elephant'/'CHR', 'war elephant').
card_uid('war elephant'/'CHR', 'CHR:War Elephant:war elephant').
card_rarity('war elephant'/'CHR', 'Common').
card_artist('war elephant'/'CHR', 'Kristen Bishop').
card_flavor_text('war elephant'/'CHR', '\"When elephants fight it is the grass that suffers.\"\n—Kikuyu proverb').
card_multiverse_id('war elephant'/'CHR', '2864').

card_in_set('witch hunter', 'CHR').
card_original_type('witch hunter'/'CHR', 'Summon — Hunter').
card_original_text('witch hunter'/'CHR', '{T}: Witch Hunter deals 1 damage to target player.\n{1}{W}{W}, {T}: Return target creature any opponent controls to owner\'s hand.').
card_image_name('witch hunter'/'CHR', 'witch hunter').
card_uid('witch hunter'/'CHR', 'CHR:Witch Hunter:witch hunter').
card_rarity('witch hunter'/'CHR', 'Uncommon').
card_artist('witch hunter'/'CHR', 'Jesper Myrfors').
card_multiverse_id('witch hunter'/'CHR', '2865').

card_in_set('xira arien', 'CHR').
card_original_type('xira arien'/'CHR', 'Summon — Legend').
card_original_text('xira arien'/'CHR', 'Flying\n{B}{R}{G}, {T}: Target player draws a card.').
card_image_name('xira arien'/'CHR', 'xira arien').
card_uid('xira arien'/'CHR', 'CHR:Xira Arien:xira arien').
card_rarity('xira arien'/'CHR', 'Rare').
card_artist('xira arien'/'CHR', 'Melissa A. Benson').
card_flavor_text('xira arien'/'CHR', 'A regular guest at the Royal Masquerade, Arien is the envy of the Court. She appears in a new costume every hour.').
card_multiverse_id('xira arien'/'CHR', '2885').

card_in_set('yawgmoth demon', 'CHR').
card_original_type('yawgmoth demon'/'CHR', 'Summon — Demon').
card_original_text('yawgmoth demon'/'CHR', 'Flying, first strike\nDuring your upkeep, sacrifice an artifact or Yawgmoth Demon deals 2 damage to you. If Yawgmoth Demon deals damage to you in this way, tap it.').
card_image_name('yawgmoth demon'/'CHR', 'yawgmoth demon').
card_uid('yawgmoth demon'/'CHR', 'CHR:Yawgmoth Demon:yawgmoth demon').
card_rarity('yawgmoth demon'/'CHR', 'Rare').
card_artist('yawgmoth demon'/'CHR', 'Sandra Everingham').
card_multiverse_id('yawgmoth demon'/'CHR', '2823').
