% Dissension

set('DIS').
set_name('DIS', 'Dissension').
set_release_date('DIS', '2006-05-05').
set_border('DIS', 'black').
set_type('DIS', 'expansion').
set_block('DIS', 'Ravnica').

card_in_set('æthermage\'s touch', 'DIS').
card_original_type('æthermage\'s touch'/'DIS', 'Instant').
card_original_text('æthermage\'s touch'/'DIS', 'Reveal the top four cards of your library. You may put a creature card from among them into play with \"At the end of your turn, return this creature to its owner\'s hand.\" Then put the rest of the cards revealed this way on the bottom of your library in any order.').
card_first_print('æthermage\'s touch', 'DIS').
card_image_name('æthermage\'s touch'/'DIS', 'aethermage\'s touch').
card_uid('æthermage\'s touch'/'DIS', 'DIS:Æthermage\'s Touch:aethermage\'s touch').
card_rarity('æthermage\'s touch'/'DIS', 'Rare').
card_artist('æthermage\'s touch'/'DIS', 'Randy Gallegos').
card_number('æthermage\'s touch'/'DIS', '101').
card_multiverse_id('æthermage\'s touch'/'DIS', '111192').
card_watermark('æthermage\'s touch'/'DIS', 'Azorius').

card_in_set('anthem of rakdos', 'DIS').
card_original_type('anthem of rakdos'/'DIS', 'Enchantment').
card_original_text('anthem of rakdos'/'DIS', 'Whenever a creature you control attacks, it gets +2/+0 until end of turn and Anthem of Rakdos deals 1 damage to you.\nHellbent As long as you have no cards in hand, if a source you control would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_first_print('anthem of rakdos', 'DIS').
card_image_name('anthem of rakdos'/'DIS', 'anthem of rakdos').
card_uid('anthem of rakdos'/'DIS', 'DIS:Anthem of Rakdos:anthem of rakdos').
card_rarity('anthem of rakdos'/'DIS', 'Rare').
card_artist('anthem of rakdos'/'DIS', 'Ralph Horsley').
card_number('anthem of rakdos'/'DIS', '102').
card_multiverse_id('anthem of rakdos'/'DIS', '97110').
card_watermark('anthem of rakdos'/'DIS', 'Rakdos').

card_in_set('aquastrand spider', 'DIS').
card_original_type('aquastrand spider'/'DIS', 'Creature — Spider Mutant').
card_original_text('aquastrand spider'/'DIS', 'Graft 2 (This creature comes into play with two +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{G}: Target creature with a +1/+1 counter on it can block as though it had flying this turn.').
card_first_print('aquastrand spider', 'DIS').
card_image_name('aquastrand spider'/'DIS', 'aquastrand spider').
card_uid('aquastrand spider'/'DIS', 'DIS:Aquastrand Spider:aquastrand spider').
card_rarity('aquastrand spider'/'DIS', 'Common').
card_artist('aquastrand spider'/'DIS', 'Dany Orizio').
card_number('aquastrand spider'/'DIS', '80').
card_multiverse_id('aquastrand spider'/'DIS', '107472').
card_watermark('aquastrand spider'/'DIS', 'Simic').

card_in_set('assault zeppelid', 'DIS').
card_original_type('assault zeppelid'/'DIS', 'Creature — Beast').
card_original_text('assault zeppelid'/'DIS', 'Flying, trample').
card_first_print('assault zeppelid', 'DIS').
card_image_name('assault zeppelid'/'DIS', 'assault zeppelid').
card_uid('assault zeppelid'/'DIS', 'DIS:Assault Zeppelid:assault zeppelid').
card_rarity('assault zeppelid'/'DIS', 'Common').
card_artist('assault zeppelid'/'DIS', 'Jeremy Jarvis').
card_number('assault zeppelid'/'DIS', '103').
card_flavor_text('assault zeppelid'/'DIS', '\"Show them to Razia and then to the Azorius sky marshals. We could create a fine bidding war—enough to fund the final stages of Project Kraj.\"\n—Momir Vig').
card_multiverse_id('assault zeppelid'/'DIS', '97107').
card_watermark('assault zeppelid'/'DIS', 'Simic').

card_in_set('aurora eidolon', 'DIS').
card_original_type('aurora eidolon'/'DIS', 'Creature — Spirit').
card_original_text('aurora eidolon'/'DIS', '{W}, Sacrifice Aurora Eidolon: Prevent the next 3 damage that would be dealt to target creature or player this turn.\nWhenever you play a multicolored spell, you may return Aurora Eidolon from your graveyard to your hand.').
card_first_print('aurora eidolon', 'DIS').
card_image_name('aurora eidolon'/'DIS', 'aurora eidolon').
card_uid('aurora eidolon'/'DIS', 'DIS:Aurora Eidolon:aurora eidolon').
card_rarity('aurora eidolon'/'DIS', 'Common').
card_artist('aurora eidolon'/'DIS', 'Justin Sweet').
card_number('aurora eidolon'/'DIS', '1').
card_multiverse_id('aurora eidolon'/'DIS', '110639').

card_in_set('avatar of discord', 'DIS').
card_original_type('avatar of discord'/'DIS', 'Creature — Avatar').
card_original_text('avatar of discord'/'DIS', '({B/R} can be paid with either {B} or {R}.)\nFlying\nWhen Avatar of Discord comes into play, sacrifice it unless you discard two cards.').
card_image_name('avatar of discord'/'DIS', 'avatar of discord').
card_uid('avatar of discord'/'DIS', 'DIS:Avatar of Discord:avatar of discord').
card_rarity('avatar of discord'/'DIS', 'Rare').
card_artist('avatar of discord'/'DIS', 'rk post').
card_number('avatar of discord'/'DIS', '140').
card_flavor_text('avatar of discord'/'DIS', 'Such is the power of Rakdos that even his shadow takes on a cruel life of its own.').
card_multiverse_id('avatar of discord'/'DIS', '107437').
card_watermark('avatar of discord'/'DIS', 'Rakdos').

card_in_set('azorius æthermage', 'DIS').
card_original_type('azorius æthermage'/'DIS', 'Creature — Human Wizard').
card_original_text('azorius æthermage'/'DIS', 'Whenever a permanent is returned to your hand, you may pay {1}. If you do, draw a card.').
card_first_print('azorius æthermage', 'DIS').
card_image_name('azorius æthermage'/'DIS', 'azorius aethermage').
card_uid('azorius æthermage'/'DIS', 'DIS:Azorius Æthermage:azorius aethermage').
card_rarity('azorius æthermage'/'DIS', 'Uncommon').
card_artist('azorius æthermage'/'DIS', 'Heather Hudson').
card_number('azorius æthermage'/'DIS', '104').
card_flavor_text('azorius æthermage'/'DIS', 'In her single metasphere are bound the records of every Ætheric transaction since the time of Azor.').
card_multiverse_id('azorius æthermage'/'DIS', '111256').
card_watermark('azorius æthermage'/'DIS', 'Azorius').

card_in_set('azorius chancery', 'DIS').
card_original_type('azorius chancery'/'DIS', 'Land').
card_original_text('azorius chancery'/'DIS', 'Azorius Chancery comes into play tapped.\nWhen Azorius Chancery comes into play, return a land you control to its owner\'s hand.\n{T}: Add {W}{U} to your mana pool.').
card_first_print('azorius chancery', 'DIS').
card_image_name('azorius chancery'/'DIS', 'azorius chancery').
card_uid('azorius chancery'/'DIS', 'DIS:Azorius Chancery:azorius chancery').
card_rarity('azorius chancery'/'DIS', 'Common').
card_artist('azorius chancery'/'DIS', 'John Avon').
card_number('azorius chancery'/'DIS', '170').
card_multiverse_id('azorius chancery'/'DIS', '97087').
card_watermark('azorius chancery'/'DIS', 'Azorius').

card_in_set('azorius first-wing', 'DIS').
card_original_type('azorius first-wing'/'DIS', 'Creature — Griffin').
card_original_text('azorius first-wing'/'DIS', 'Flying, protection from enchantments').
card_first_print('azorius first-wing', 'DIS').
card_image_name('azorius first-wing'/'DIS', 'azorius first-wing').
card_uid('azorius first-wing'/'DIS', 'DIS:Azorius First-Wing:azorius first-wing').
card_rarity('azorius first-wing'/'DIS', 'Common').
card_artist('azorius first-wing'/'DIS', 'Alex Horley-Orlandelli').
card_number('azorius first-wing'/'DIS', '105').
card_flavor_text('azorius first-wing'/'DIS', 'With a steady diet of lawbreakers, Azorius griffins inevitably develop a resistance to the contraband magic that taints much of their prey.').
card_multiverse_id('azorius first-wing'/'DIS', '97101').
card_watermark('azorius first-wing'/'DIS', 'Azorius').

card_in_set('azorius guildmage', 'DIS').
card_original_type('azorius guildmage'/'DIS', 'Creature — Vedalken Wizard').
card_original_text('azorius guildmage'/'DIS', '({W/U} can be paid with either {W} or {U}.)\n{2}{W}: Tap target creature.\n{2}{U}: Counter target activated ability. (Mana abilities can\'t be targeted.)').
card_image_name('azorius guildmage'/'DIS', 'azorius guildmage').
card_uid('azorius guildmage'/'DIS', 'DIS:Azorius Guildmage:azorius guildmage').
card_rarity('azorius guildmage'/'DIS', 'Uncommon').
card_artist('azorius guildmage'/'DIS', 'Christopher Moeller').
card_number('azorius guildmage'/'DIS', '141').
card_multiverse_id('azorius guildmage'/'DIS', '97077').
card_watermark('azorius guildmage'/'DIS', 'Azorius').

card_in_set('azorius herald', 'DIS').
card_original_type('azorius herald'/'DIS', 'Creature — Spirit').
card_original_text('azorius herald'/'DIS', 'Azorius Herald is unblockable.\nWhen Azorius Herald comes into play, you gain 4 life.\nWhen Azorius Herald comes into play, sacrifice it unless {U} was spent to play it.').
card_first_print('azorius herald', 'DIS').
card_image_name('azorius herald'/'DIS', 'azorius herald').
card_uid('azorius herald'/'DIS', 'DIS:Azorius Herald:azorius herald').
card_rarity('azorius herald'/'DIS', 'Uncommon').
card_artist('azorius herald'/'DIS', 'Justin Sweet').
card_number('azorius herald'/'DIS', '2').
card_flavor_text('azorius herald'/'DIS', '\"As peace should be—gentle yet unstoppable.\"\n—Augustin IV').
card_multiverse_id('azorius herald'/'DIS', '107577').
card_watermark('azorius herald'/'DIS', 'Azorius').

card_in_set('azorius ploy', 'DIS').
card_original_type('azorius ploy'/'DIS', 'Instant').
card_original_text('azorius ploy'/'DIS', 'Prevent all combat damage target creature would deal this turn.\nPrevent all combat damage that would be dealt to target creature this turn.').
card_first_print('azorius ploy', 'DIS').
card_image_name('azorius ploy'/'DIS', 'azorius ploy').
card_uid('azorius ploy'/'DIS', 'DIS:Azorius Ploy:azorius ploy').
card_rarity('azorius ploy'/'DIS', 'Uncommon').
card_artist('azorius ploy'/'DIS', 'Michael Sutfin').
card_number('azorius ploy'/'DIS', '106').
card_flavor_text('azorius ploy'/'DIS', 'Azorius military tactics, like their legal tactics, capitalize on delay and nullification.').
card_multiverse_id('azorius ploy'/'DIS', '107427').
card_watermark('azorius ploy'/'DIS', 'Azorius').

card_in_set('azorius signet', 'DIS').
card_original_type('azorius signet'/'DIS', 'Artifact').
card_original_text('azorius signet'/'DIS', '{1}, {T}: Add {W}{U} to your mana pool.').
card_first_print('azorius signet', 'DIS').
card_image_name('azorius signet'/'DIS', 'azorius signet').
card_uid('azorius signet'/'DIS', 'DIS:Azorius Signet:azorius signet').
card_rarity('azorius signet'/'DIS', 'Common').
card_artist('azorius signet'/'DIS', 'Greg Hildebrandt').
card_number('azorius signet'/'DIS', '159').
card_flavor_text('azorius signet'/'DIS', 'The maze-like design embodies the core of Azorius law—strict structure designed to test wills and stall change.').
card_multiverse_id('azorius signet'/'DIS', '97085').
card_watermark('azorius signet'/'DIS', 'Azorius').

card_in_set('beacon hawk', 'DIS').
card_original_type('beacon hawk'/'DIS', 'Creature — Bird').
card_original_text('beacon hawk'/'DIS', 'Flying\nWhenever Beacon Hawk deals combat damage to a player, you may untap target creature.\n{W}: Beacon Hawk gets +0/+1 until end of turn.').
card_first_print('beacon hawk', 'DIS').
card_image_name('beacon hawk'/'DIS', 'beacon hawk').
card_uid('beacon hawk'/'DIS', 'DIS:Beacon Hawk:beacon hawk').
card_rarity('beacon hawk'/'DIS', 'Common').
card_artist('beacon hawk'/'DIS', 'William Simpson').
card_number('beacon hawk'/'DIS', '3').
card_multiverse_id('beacon hawk'/'DIS', '111225').

card_in_set('biomantic mastery', 'DIS').
card_original_type('biomantic mastery'/'DIS', 'Sorcery').
card_original_text('biomantic mastery'/'DIS', '({G/U} can be paid with either {G} or {U}.)\nDraw a card for each creature target player controls, then draw a card for each creature another target player controls.').
card_first_print('biomantic mastery', 'DIS').
card_image_name('biomantic mastery'/'DIS', 'biomantic mastery').
card_uid('biomantic mastery'/'DIS', 'DIS:Biomantic Mastery:biomantic mastery').
card_rarity('biomantic mastery'/'DIS', 'Rare').
card_artist('biomantic mastery'/'DIS', 'Dan Scott').
card_number('biomantic mastery'/'DIS', '142').
card_flavor_text('biomantic mastery'/'DIS', '\"Look beyond, to the vascular awareness that all life is a map to greater knowledge.\"\n—Momir Vig, Biomancy, vol. I').
card_multiverse_id('biomantic mastery'/'DIS', '107277').
card_watermark('biomantic mastery'/'DIS', 'Simic').

card_in_set('blessing of the nephilim', 'DIS').
card_original_type('blessing of the nephilim'/'DIS', 'Enchantment — Aura').
card_original_text('blessing of the nephilim'/'DIS', 'Enchant creature\nEnchanted creature gets +1/+1 for each of its colors.').
card_first_print('blessing of the nephilim', 'DIS').
card_image_name('blessing of the nephilim'/'DIS', 'blessing of the nephilim').
card_uid('blessing of the nephilim'/'DIS', 'DIS:Blessing of the Nephilim:blessing of the nephilim').
card_rarity('blessing of the nephilim'/'DIS', 'Uncommon').
card_artist('blessing of the nephilim'/'DIS', 'Greg Hildebrandt').
card_number('blessing of the nephilim'/'DIS', '4').
card_flavor_text('blessing of the nephilim'/'DIS', '\"Before the first stone was laid or the first elf-child born, the power of the nephilim was gathering. Let that power be spread by my hands.\"').
card_multiverse_id('blessing of the nephilim'/'DIS', '107307').

card_in_set('blood crypt', 'DIS').
card_original_type('blood crypt'/'DIS', 'Land — Swamp Mountain').
card_original_text('blood crypt'/'DIS', '({T}: Add {B} or {R} to your mana pool.)\nAs Blood Crypt comes into play, you may pay 2 life. If you don\'t, Blood Crypt comes into play tapped instead.').
card_first_print('blood crypt', 'DIS').
card_image_name('blood crypt'/'DIS', 'blood crypt').
card_uid('blood crypt'/'DIS', 'DIS:Blood Crypt:blood crypt').
card_rarity('blood crypt'/'DIS', 'Rare').
card_artist('blood crypt'/'DIS', 'Rob Alexander').
card_number('blood crypt'/'DIS', '171').
card_multiverse_id('blood crypt'/'DIS', '97102').
card_watermark('blood crypt'/'DIS', 'Rakdos').

card_in_set('bond of agony', 'DIS').
card_original_type('bond of agony'/'DIS', 'Sorcery').
card_original_text('bond of agony'/'DIS', 'As an additional cost to play Bond of Agony, pay X life.\nEach other player loses X life.').
card_first_print('bond of agony', 'DIS').
card_image_name('bond of agony'/'DIS', 'bond of agony').
card_uid('bond of agony'/'DIS', 'DIS:Bond of Agony:bond of agony').
card_rarity('bond of agony'/'DIS', 'Uncommon').
card_artist('bond of agony'/'DIS', 'Luca Zontini').
card_number('bond of agony'/'DIS', '38').
card_flavor_text('bond of agony'/'DIS', 'The Rakdos are unique in designing torture equipment they can operate while \"suffering\" alongside their victims.').
card_multiverse_id('bond of agony'/'DIS', '107306').

card_in_set('bound', 'DIS').
card_original_type('bound'/'DIS', 'Instant').
card_original_text('bound'/'DIS', 'Sacrifice a creature. Return up to X cards from your graveyard to your hand, where X is the number of colors that creature was. Then remove this card from the game.\n//\nDetermined\n{G}{U}\nInstant\nOther spells you control can\'t be countered by spells or abilities this turn.\nDraw a card.').
card_first_print('bound', 'DIS').
card_image_name('bound'/'DIS', 'bounddetermined').
card_uid('bound'/'DIS', 'DIS:Bound:bounddetermined').
card_rarity('bound'/'DIS', 'Rare').
card_artist('bound'/'DIS', 'Jim Nelson').
card_number('bound'/'DIS', '149a').
card_multiverse_id('bound'/'DIS', '107373').
card_watermark('bound'/'DIS', 'Golgari').

card_in_set('brace for impact', 'DIS').
card_original_type('brace for impact'/'DIS', 'Instant').
card_original_text('brace for impact'/'DIS', 'Prevent all damage that would be dealt to target multicolored creature this turn. For each 1 damage prevented this way, put a +1/+1 counter on that creature.').
card_first_print('brace for impact', 'DIS').
card_image_name('brace for impact'/'DIS', 'brace for impact').
card_uid('brace for impact'/'DIS', 'DIS:Brace for Impact:brace for impact').
card_rarity('brace for impact'/'DIS', 'Uncommon').
card_artist('brace for impact'/'DIS', 'Dan Scott').
card_number('brace for impact'/'DIS', '5').
card_flavor_text('brace for impact'/'DIS', 'Flesh can be tempered by spellcraft and forged ever stronger by the hammers of foes.').
card_multiverse_id('brace for impact'/'DIS', '107537').

card_in_set('brain pry', 'DIS').
card_original_type('brain pry'/'DIS', 'Sorcery').
card_original_text('brain pry'/'DIS', 'Name a nonland card. Target player reveals his or her hand. That player discards a card with that name. If he or she can\'t, you draw a card.').
card_first_print('brain pry', 'DIS').
card_image_name('brain pry'/'DIS', 'brain pry').
card_uid('brain pry'/'DIS', 'DIS:Brain Pry:brain pry').
card_rarity('brain pry'/'DIS', 'Uncommon').
card_artist('brain pry'/'DIS', 'Jim Nelson').
card_number('brain pry'/'DIS', '39').
card_flavor_text('brain pry'/'DIS', 'To the Rakdos, the fun is in the shakedown. The loot is usually discarded.').
card_multiverse_id('brain pry'/'DIS', '107340').

card_in_set('breeding pool', 'DIS').
card_original_type('breeding pool'/'DIS', 'Land — Forest Island').
card_original_text('breeding pool'/'DIS', '({T}: Add {G} or {U} to your mana pool.)\nAs Breeding Pool comes into play, you may pay 2 life. If you don\'t, Breeding Pool comes into play tapped instead.').
card_first_print('breeding pool', 'DIS').
card_image_name('breeding pool'/'DIS', 'breeding pool').
card_uid('breeding pool'/'DIS', 'DIS:Breeding Pool:breeding pool').
card_rarity('breeding pool'/'DIS', 'Rare').
card_artist('breeding pool'/'DIS', 'Rob Alexander').
card_number('breeding pool'/'DIS', '172').
card_multiverse_id('breeding pool'/'DIS', '97088').
card_watermark('breeding pool'/'DIS', 'Simic').

card_in_set('bronze bombshell', 'DIS').
card_original_type('bronze bombshell'/'DIS', 'Artifact Creature — Construct').
card_original_text('bronze bombshell'/'DIS', 'When a player other than Bronze Bombshell\'s owner controls it, that player sacrifices it. If the player does, Bronze Bombshell deals 7 damage to him or her.').
card_first_print('bronze bombshell', 'DIS').
card_image_name('bronze bombshell'/'DIS', 'bronze bombshell').
card_uid('bronze bombshell'/'DIS', 'DIS:Bronze Bombshell:bronze bombshell').
card_rarity('bronze bombshell'/'DIS', 'Rare').
card_artist('bronze bombshell'/'DIS', 'Martina Pilcerova').
card_number('bronze bombshell'/'DIS', '160').
card_flavor_text('bronze bombshell'/'DIS', '\"Ooh, shiny! Let\'s pull off the chain and take her with us.\"\n—Ukl, Gruul raider, last words').
card_multiverse_id('bronze bombshell'/'DIS', '107327').

card_in_set('cackling flames', 'DIS').
card_original_type('cackling flames'/'DIS', 'Instant').
card_original_text('cackling flames'/'DIS', 'Cackling Flames deals 3 damage to target creature or player.\nHellbent Cackling Flames deals 5 damage to that creature or player instead if you have no cards in hand.').
card_first_print('cackling flames', 'DIS').
card_image_name('cackling flames'/'DIS', 'cackling flames').
card_uid('cackling flames'/'DIS', 'DIS:Cackling Flames:cackling flames').
card_rarity('cackling flames'/'DIS', 'Common').
card_artist('cackling flames'/'DIS', 'Shishizaru').
card_number('cackling flames'/'DIS', '59').
card_flavor_text('cackling flames'/'DIS', '\"I like a little entertainment with my dinner.\"\n—Rakdos').
card_multiverse_id('cackling flames'/'DIS', '107258').
card_watermark('cackling flames'/'DIS', 'Rakdos').

card_in_set('carom', 'DIS').
card_original_type('carom'/'DIS', 'Instant').
card_original_text('carom'/'DIS', 'The next 1 damage that would be dealt to target creature this turn is dealt to another target creature instead.\nDraw a card.').
card_first_print('carom', 'DIS').
card_image_name('carom'/'DIS', 'carom').
card_uid('carom'/'DIS', 'DIS:Carom:carom').
card_rarity('carom'/'DIS', 'Common').
card_artist('carom'/'DIS', 'Alex Horley-Orlandelli').
card_number('carom'/'DIS', '6').
card_flavor_text('carom'/'DIS', 'Her enemy\'s strength is her own.').
card_multiverse_id('carom'/'DIS', '107310').

card_in_set('celestial ancient', 'DIS').
card_original_type('celestial ancient'/'DIS', 'Creature — Elemental').
card_original_text('celestial ancient'/'DIS', 'Flying\nWhenever you play an enchantment spell, put a +1/+1 counter on each creature you control.').
card_first_print('celestial ancient', 'DIS').
card_image_name('celestial ancient'/'DIS', 'celestial ancient').
card_uid('celestial ancient'/'DIS', 'DIS:Celestial Ancient:celestial ancient').
card_rarity('celestial ancient'/'DIS', 'Rare').
card_artist('celestial ancient'/'DIS', 'Mark Tedin').
card_number('celestial ancient'/'DIS', '7').
card_flavor_text('celestial ancient'/'DIS', '\"We thought the clouds had moved from the night sky. Then the night sky moved, and the horizon grew wings.\"\n—Josuri').
card_multiverse_id('celestial ancient'/'DIS', '107317').

card_in_set('coiling oracle', 'DIS').
card_original_type('coiling oracle'/'DIS', 'Creature — Snake Elf Druid').
card_original_text('coiling oracle'/'DIS', 'When Coiling Oracle comes into play, reveal the top card of your library. If it\'s a land card, put it into play. Otherwise, put that card into your hand.').
card_image_name('coiling oracle'/'DIS', 'coiling oracle').
card_uid('coiling oracle'/'DIS', 'DIS:Coiling Oracle:coiling oracle').
card_rarity('coiling oracle'/'DIS', 'Common').
card_artist('coiling oracle'/'DIS', 'Mark Zug').
card_number('coiling oracle'/'DIS', '107').
card_flavor_text('coiling oracle'/'DIS', 'Snaking remnants of nature directed by a body of thought and progress, the oracles embody all that is Simic.').
card_multiverse_id('coiling oracle'/'DIS', '97106').
card_watermark('coiling oracle'/'DIS', 'Simic').

card_in_set('condemn', 'DIS').
card_original_type('condemn'/'DIS', 'Instant').
card_original_text('condemn'/'DIS', 'Put target attacking creature on the bottom of its owner\'s library. Its controller gains life equal to its toughness.').
card_image_name('condemn'/'DIS', 'condemn').
card_uid('condemn'/'DIS', 'DIS:Condemn:condemn').
card_rarity('condemn'/'DIS', 'Uncommon').
card_artist('condemn'/'DIS', 'Daren Bader').
card_number('condemn'/'DIS', '8').
card_flavor_text('condemn'/'DIS', '\"No doubt the arbiters would put you away, after all the documents are signed. But I will have justice now!\"\n—Alovnek, Boros guildmage').
card_multiverse_id('condemn'/'DIS', '107494').

card_in_set('court hussar', 'DIS').
card_original_type('court hussar'/'DIS', 'Creature — Vedalken Knight').
card_original_text('court hussar'/'DIS', 'Vigilance\nWhen Court Hussar comes into play, look at the top three cards of your library, then put one of them into your hand and the rest on the bottom of your library in any order.\nWhen Court Hussar comes into play, sacrifice it unless {W} was spent to play it.').
card_first_print('court hussar', 'DIS').
card_image_name('court hussar'/'DIS', 'court hussar').
card_uid('court hussar'/'DIS', 'DIS:Court Hussar:court hussar').
card_rarity('court hussar'/'DIS', 'Uncommon').
card_artist('court hussar'/'DIS', 'Ron Spears').
card_number('court hussar'/'DIS', '22').
card_multiverse_id('court hussar'/'DIS', '107265').
card_watermark('court hussar'/'DIS', 'Azorius').

card_in_set('crime', 'DIS').
card_original_type('crime'/'DIS', 'Sorcery').
card_original_text('crime'/'DIS', 'Put target creature or enchantment card in an opponent\'s graveyard into play under your control.\n//\nPunishment\n{X}{B}{G}\nSorcery\nDestroy each artifact, creature, and enchantment with converted mana cost X.').
card_first_print('crime', 'DIS').
card_image_name('crime'/'DIS', 'crimepunishment').
card_uid('crime'/'DIS', 'DIS:Crime:crimepunishment').
card_rarity('crime'/'DIS', 'Rare').
card_artist('crime'/'DIS', 'Randy Gallegos').
card_number('crime'/'DIS', '150a').
card_multiverse_id('crime'/'DIS', '107285').
card_watermark('crime'/'DIS', 'Orzhov').

card_in_set('crypt champion', 'DIS').
card_original_type('crypt champion'/'DIS', 'Creature — Zombie').
card_original_text('crypt champion'/'DIS', 'Double strike\nWhen Crypt Champion comes into play, each player puts a creature card with converted mana cost 3 or less from his or her graveyard into play.\nWhen Crypt Champion comes into play, sacrifice it unless {R} was spent to play it.').
card_first_print('crypt champion', 'DIS').
card_image_name('crypt champion'/'DIS', 'crypt champion').
card_uid('crypt champion'/'DIS', 'DIS:Crypt Champion:crypt champion').
card_rarity('crypt champion'/'DIS', 'Uncommon').
card_artist('crypt champion'/'DIS', 'Pete Venters').
card_number('crypt champion'/'DIS', '40').
card_multiverse_id('crypt champion'/'DIS', '107336').
card_watermark('crypt champion'/'DIS', 'Rakdos').

card_in_set('cytoplast manipulator', 'DIS').
card_original_type('cytoplast manipulator'/'DIS', 'Creature — Human Wizard Mutant').
card_original_text('cytoplast manipulator'/'DIS', 'Graft 2 (This creature comes into play with two +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{U}, {T}: Gain control of target creature with a +1/+1 counter on it as long as Cytoplast Manipulator remains in play.').
card_first_print('cytoplast manipulator', 'DIS').
card_image_name('cytoplast manipulator'/'DIS', 'cytoplast manipulator').
card_uid('cytoplast manipulator'/'DIS', 'DIS:Cytoplast Manipulator:cytoplast manipulator').
card_rarity('cytoplast manipulator'/'DIS', 'Rare').
card_artist('cytoplast manipulator'/'DIS', 'Dan Scott').
card_number('cytoplast manipulator'/'DIS', '23').
card_multiverse_id('cytoplast manipulator'/'DIS', '107353').
card_watermark('cytoplast manipulator'/'DIS', 'Simic').

card_in_set('cytoplast root-kin', 'DIS').
card_original_type('cytoplast root-kin'/'DIS', 'Creature — Elemental Mutant').
card_original_text('cytoplast root-kin'/'DIS', 'Graft 4 (This creature comes into play with four +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\nWhen Cytoplast Root-Kin comes into play, put a +1/+1 counter on each other creature you control that has a +1/+1 counter on it.\n{2}: Move a +1/+1 counter from target creature you control onto Cytoplast Root-Kin.').
card_first_print('cytoplast root-kin', 'DIS').
card_image_name('cytoplast root-kin'/'DIS', 'cytoplast root-kin').
card_uid('cytoplast root-kin'/'DIS', 'DIS:Cytoplast Root-Kin:cytoplast root-kin').
card_rarity('cytoplast root-kin'/'DIS', 'Rare').
card_artist('cytoplast root-kin'/'DIS', 'Thomas M. Baxa').
card_number('cytoplast root-kin'/'DIS', '81').
card_multiverse_id('cytoplast root-kin'/'DIS', '107507').
card_watermark('cytoplast root-kin'/'DIS', 'Simic').

card_in_set('cytoshape', 'DIS').
card_original_type('cytoshape'/'DIS', 'Instant').
card_original_text('cytoshape'/'DIS', 'Choose a nonlegendary creature in play. Target creature becomes a copy of that creature until end of turn.').
card_first_print('cytoshape', 'DIS').
card_image_name('cytoshape'/'DIS', 'cytoshape').
card_uid('cytoshape'/'DIS', 'DIS:Cytoshape:cytoshape').
card_rarity('cytoshape'/'DIS', 'Rare').
card_artist('cytoshape'/'DIS', 'Alan Pollack').
card_number('cytoshape'/'DIS', '108').
card_flavor_text('cytoshape'/'DIS', '\"Though highly effective at reshaping flesh, these specially bred cytoplasts leave the subject reeking of omnibian mucus.\"\n—Simic research notes').
card_multiverse_id('cytoshape'/'DIS', '107346').
card_watermark('cytoshape'/'DIS', 'Simic').

card_in_set('cytospawn shambler', 'DIS').
card_original_type('cytospawn shambler'/'DIS', 'Creature — Elemental Mutant').
card_original_text('cytospawn shambler'/'DIS', 'Graft 6 (This creature comes into play with six +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{G}: Target creature with a +1/+1 counter on it gains trample until end of turn.').
card_first_print('cytospawn shambler', 'DIS').
card_image_name('cytospawn shambler'/'DIS', 'cytospawn shambler').
card_uid('cytospawn shambler'/'DIS', 'DIS:Cytospawn Shambler:cytospawn shambler').
card_rarity('cytospawn shambler'/'DIS', 'Common').
card_artist('cytospawn shambler'/'DIS', 'Anthony S. Waters').
card_number('cytospawn shambler'/'DIS', '82').
card_multiverse_id('cytospawn shambler'/'DIS', '107312').
card_watermark('cytospawn shambler'/'DIS', 'Simic').

card_in_set('delirium skeins', 'DIS').
card_original_type('delirium skeins'/'DIS', 'Sorcery').
card_original_text('delirium skeins'/'DIS', 'Each player discards three cards.').
card_first_print('delirium skeins', 'DIS').
card_image_name('delirium skeins'/'DIS', 'delirium skeins').
card_uid('delirium skeins'/'DIS', 'DIS:Delirium Skeins:delirium skeins').
card_rarity('delirium skeins'/'DIS', 'Common').
card_artist('delirium skeins'/'DIS', 'Aleksi Briclot').
card_number('delirium skeins'/'DIS', '41').
card_flavor_text('delirium skeins'/'DIS', 'There came a rush, a sudden fire in the mind. Then the two saw only the sooty fog of madness, heard only the crackling of their last thoughts burning away.').
card_multiverse_id('delirium skeins'/'DIS', '107435').

card_in_set('demand', 'DIS').
card_original_type('demand'/'DIS', 'Sorcery').
card_original_text('demand'/'DIS', 'Put X 1/1 green Saproling creature tokens into play.\n//\nDemand\n{1}{W}{U}\nSorcery\nSearch your library for a multicolored card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('demand', 'DIS').
card_image_name('demand'/'DIS', 'supplydemand').
card_uid('demand'/'DIS', 'DIS:Demand:supplydemand').
card_rarity('demand'/'DIS', 'Uncommon').
card_artist('demand'/'DIS', 'Daren Bader').
card_number('demand'/'DIS', '157b').
card_multiverse_id('demand'/'DIS', '107464').
card_watermark('demand'/'DIS', 'Azorius').

card_in_set('demon\'s jester', 'DIS').
card_original_type('demon\'s jester'/'DIS', 'Creature — Imp').
card_original_text('demon\'s jester'/'DIS', 'Flying\nHellbent Demon\'s Jester gets +2/+1 as long as you have no cards in hand.').
card_first_print('demon\'s jester', 'DIS').
card_image_name('demon\'s jester'/'DIS', 'demon\'s jester').
card_uid('demon\'s jester'/'DIS', 'DIS:Demon\'s Jester:demon\'s jester').
card_rarity('demon\'s jester'/'DIS', 'Common').
card_artist('demon\'s jester'/'DIS', 'Pete Venters').
card_number('demon\'s jester'/'DIS', '42').
card_flavor_text('demon\'s jester'/'DIS', 'They knock \'em dead, with or without the punch line.').
card_multiverse_id('demon\'s jester'/'DIS', '107599').
card_watermark('demon\'s jester'/'DIS', 'Rakdos').

card_in_set('demonfire', 'DIS').
card_original_type('demonfire'/'DIS', 'Sorcery').
card_original_text('demonfire'/'DIS', 'Demonfire deals X damage to target creature or player. If a creature dealt damage this way would be put into a graveyard this turn, remove it from the game instead.\nHellbent If you have no cards in hand, Demonfire can\'t be countered by spells or abilities and the damage can\'t be prevented.').
card_first_print('demonfire', 'DIS').
card_image_name('demonfire'/'DIS', 'demonfire').
card_uid('demonfire'/'DIS', 'DIS:Demonfire:demonfire').
card_rarity('demonfire'/'DIS', 'Rare').
card_artist('demonfire'/'DIS', 'Greg Staples').
card_number('demonfire'/'DIS', '60').
card_multiverse_id('demonfire'/'DIS', '107252').
card_watermark('demonfire'/'DIS', 'Rakdos').

card_in_set('determined', 'DIS').
card_original_type('determined'/'DIS', 'Instant').
card_original_text('determined'/'DIS', 'Sacrifice a creature. Return up to X cards from your graveyard to your hand, where X is the number of colors that creature was. Then remove this card from the game.\n//\nDetermined\n{G}{U}\nInstant\nOther spells you control can\'t be countered by spells or abilities this turn.\nDraw a card.').
card_first_print('determined', 'DIS').
card_image_name('determined'/'DIS', 'bounddetermined').
card_uid('determined'/'DIS', 'DIS:Determined:bounddetermined').
card_rarity('determined'/'DIS', 'Rare').
card_artist('determined'/'DIS', 'Jim Nelson').
card_number('determined'/'DIS', '149b').
card_multiverse_id('determined'/'DIS', '107373').
card_watermark('determined'/'DIS', 'Simic').

card_in_set('development', 'DIS').
card_original_type('development'/'DIS', 'Instant').
card_original_text('development'/'DIS', 'Choose up to four cards you own from outside the game and shuffle them into your library.\n//\nDevelopment\n{3}{U}{R}\nInstant\nPut a 3/1 red Elemental creature token into play unless an opponent lets you draw a card. Repeat this process two more times.').
card_first_print('development', 'DIS').
card_image_name('development'/'DIS', 'researchdevelopment').
card_uid('development'/'DIS', 'DIS:Development:researchdevelopment').
card_rarity('development'/'DIS', 'Rare').
card_artist('development'/'DIS', 'Greg Staples').
card_number('development'/'DIS', '155b').
card_multiverse_id('development'/'DIS', '107375').
card_watermark('development'/'DIS', 'Izzet').

card_in_set('dovescape', 'DIS').
card_original_type('dovescape'/'DIS', 'Enchantment').
card_original_text('dovescape'/'DIS', '({W/U} can be paid with either {W} or {U}.)\nWhenever a player plays a noncreature spell, counter that spell. That player puts X 1/1 white and blue Bird creature tokens with flying into play, where X is the spell\'s converted mana cost.').
card_first_print('dovescape', 'DIS').
card_image_name('dovescape'/'DIS', 'dovescape').
card_uid('dovescape'/'DIS', 'DIS:Dovescape:dovescape').
card_rarity('dovescape'/'DIS', 'Rare').
card_artist('dovescape'/'DIS', 'Shishizaru').
card_number('dovescape'/'DIS', '143').
card_multiverse_id('dovescape'/'DIS', '107428').
card_watermark('dovescape'/'DIS', 'Azorius').

card_in_set('dread slag', 'DIS').
card_original_type('dread slag'/'DIS', 'Creature — Horror').
card_original_text('dread slag'/'DIS', 'Trample\nDread Slag gets -4/-4 for each card in your hand.').
card_first_print('dread slag', 'DIS').
card_image_name('dread slag'/'DIS', 'dread slag').
card_uid('dread slag'/'DIS', 'DIS:Dread Slag:dread slag').
card_rarity('dread slag'/'DIS', 'Rare').
card_artist('dread slag'/'DIS', 'Anthony S. Waters').
card_number('dread slag'/'DIS', '109').
card_flavor_text('dread slag'/'DIS', 'A thousand phobias sopped from the city and wrung from Rix Maadi as one.').
card_multiverse_id('dread slag'/'DIS', '107322').
card_watermark('dread slag'/'DIS', 'Rakdos').

card_in_set('drekavac', 'DIS').
card_original_type('drekavac'/'DIS', 'Creature — Beast').
card_original_text('drekavac'/'DIS', 'When Drekavac comes into play, sacrifice it unless you discard a noncreature card.').
card_first_print('drekavac', 'DIS').
card_image_name('drekavac'/'DIS', 'drekavac').
card_uid('drekavac'/'DIS', 'DIS:Drekavac:drekavac').
card_rarity('drekavac'/'DIS', 'Uncommon').
card_artist('drekavac'/'DIS', 'Carl Critchlow').
card_number('drekavac'/'DIS', '43').
card_flavor_text('drekavac'/'DIS', 'Like a vulture\'s scalp, the face of a drekavac is oily and hairless. The filth and disease of its carrion diet slip off its blood-slick skin.').
card_multiverse_id('drekavac'/'DIS', '111258').

card_in_set('elemental resonance', 'DIS').
card_original_type('elemental resonance'/'DIS', 'Enchantment — Aura').
card_original_text('elemental resonance'/'DIS', 'Enchant permanent\nAt the beginning of your precombat main phase, add mana equal to enchanted permanent\'s mana cost to your mana pool. (Mana cost includes color. If a mana symbol has multiple colors, choose one.)').
card_first_print('elemental resonance', 'DIS').
card_image_name('elemental resonance'/'DIS', 'elemental resonance').
card_uid('elemental resonance'/'DIS', 'DIS:Elemental Resonance:elemental resonance').
card_rarity('elemental resonance'/'DIS', 'Rare').
card_artist('elemental resonance'/'DIS', 'Mark Tedin').
card_number('elemental resonance'/'DIS', '83').
card_multiverse_id('elemental resonance'/'DIS', '107369').

card_in_set('ends', 'DIS').
card_original_type('ends'/'DIS', 'Instant').
card_original_text('ends'/'DIS', 'Flip a coin. If it comes up heads, counter target instant or sorcery spell. If it comes up tails, copy that spell and you may choose new targets for the copy.\n//\nEnds\n{3}{R}{W}\nInstant\nTarget player sacrifices two attacking creatures.').
card_first_print('ends', 'DIS').
card_image_name('ends'/'DIS', 'oddsends').
card_uid('ends'/'DIS', 'DIS:Ends:oddsends').
card_rarity('ends'/'DIS', 'Rare').
card_artist('ends'/'DIS', 'Michael Sutfin').
card_number('ends'/'DIS', '153b').
card_multiverse_id('ends'/'DIS', '107445').
card_watermark('ends'/'DIS', 'Boros').

card_in_set('enemy of the guildpact', 'DIS').
card_original_type('enemy of the guildpact'/'DIS', 'Creature — Spirit').
card_original_text('enemy of the guildpact'/'DIS', 'Protection from multicolored').
card_first_print('enemy of the guildpact', 'DIS').
card_image_name('enemy of the guildpact'/'DIS', 'enemy of the guildpact').
card_uid('enemy of the guildpact'/'DIS', 'DIS:Enemy of the Guildpact:enemy of the guildpact').
card_rarity('enemy of the guildpact'/'DIS', 'Common').
card_artist('enemy of the guildpact'/'DIS', 'Fred Hooper').
card_number('enemy of the guildpact'/'DIS', '44').
card_flavor_text('enemy of the guildpact'/'DIS', 'Guilds often exterminate those who saw or knew too much. Some vindictive souls retain their knowledge, using it in the afterlife to crush the guilded and the vile bargain that gives them power.').
card_multiverse_id('enemy of the guildpact'/'DIS', '107342').

card_in_set('enigma eidolon', 'DIS').
card_original_type('enigma eidolon'/'DIS', 'Creature — Spirit').
card_original_text('enigma eidolon'/'DIS', '{U}, Sacrifice Enigma Eidolon: Target player puts the top three cards of his or her library into his or her graveyard.\nWhenever you play a multicolored spell, you may return Enigma Eidolon from your graveyard to your hand.').
card_first_print('enigma eidolon', 'DIS').
card_image_name('enigma eidolon'/'DIS', 'enigma eidolon').
card_uid('enigma eidolon'/'DIS', 'DIS:Enigma Eidolon:enigma eidolon').
card_rarity('enigma eidolon'/'DIS', 'Common').
card_artist('enigma eidolon'/'DIS', 'Shishizaru').
card_number('enigma eidolon'/'DIS', '24').
card_multiverse_id('enigma eidolon'/'DIS', '110640').

card_in_set('entropic eidolon', 'DIS').
card_original_type('entropic eidolon'/'DIS', 'Creature — Spirit').
card_original_text('entropic eidolon'/'DIS', '{B}, Sacrifice Entropic Eidolon: Target player loses 1 life and you gain 1 life.\nWhenever you play a multicolored spell, you may return Entropic Eidolon from your graveyard to your hand.').
card_first_print('entropic eidolon', 'DIS').
card_image_name('entropic eidolon'/'DIS', 'entropic eidolon').
card_uid('entropic eidolon'/'DIS', 'DIS:Entropic Eidolon:entropic eidolon').
card_rarity('entropic eidolon'/'DIS', 'Common').
card_artist('entropic eidolon'/'DIS', 'Aleksi Briclot').
card_number('entropic eidolon'/'DIS', '45').
card_multiverse_id('entropic eidolon'/'DIS', '110641').

card_in_set('error', 'DIS').
card_original_type('error'/'DIS', 'Instant').
card_original_text('error'/'DIS', 'Return all creatures blocking or blocked by target creature to their owner\'s hand.\n//\nError\n{U}{B}\nInstant\nCounter target multicolored spell.').
card_first_print('error', 'DIS').
card_image_name('error'/'DIS', 'trialerror').
card_uid('error'/'DIS', 'DIS:Error:trialerror').
card_rarity('error'/'DIS', 'Uncommon').
card_artist('error'/'DIS', 'Ron Spears & Wayne Reynolds').
card_number('error'/'DIS', '158b').
card_multiverse_id('error'/'DIS', '107259').
card_watermark('error'/'DIS', 'Dimir').

card_in_set('evolution vat', 'DIS').
card_original_type('evolution vat'/'DIS', 'Artifact').
card_original_text('evolution vat'/'DIS', '{3}, {T}: Tap target creature and put a +1/+1 counter on it. Until end of turn, that creature gains \"{2}{G}{U}: Double the number of +1/+1 counters on this creature.\"').
card_first_print('evolution vat', 'DIS').
card_image_name('evolution vat'/'DIS', 'evolution vat').
card_uid('evolution vat'/'DIS', 'DIS:Evolution Vat:evolution vat').
card_rarity('evolution vat'/'DIS', 'Rare').
card_artist('evolution vat'/'DIS', 'John Avon').
card_number('evolution vat'/'DIS', '161').
card_flavor_text('evolution vat'/'DIS', 'The vats are self-contained worlds, bubbling and churning with unnatural speed, boiling down eons of change into moments.').
card_multiverse_id('evolution vat'/'DIS', '97093').
card_watermark('evolution vat'/'DIS', 'Simic').

card_in_set('experiment kraj', 'DIS').
card_original_type('experiment kraj'/'DIS', 'Legendary Creature — Ooze Mutant').
card_original_text('experiment kraj'/'DIS', 'Experiment Kraj has all activated abilities of each other creature with a +1/+1 counter on it.\n{T}: Put a +1/+1 counter on target creature.').
card_first_print('experiment kraj', 'DIS').
card_image_name('experiment kraj'/'DIS', 'experiment kraj').
card_uid('experiment kraj'/'DIS', 'DIS:Experiment Kraj:experiment kraj').
card_rarity('experiment kraj'/'DIS', 'Rare').
card_artist('experiment kraj'/'DIS', 'Mark Tedin').
card_number('experiment kraj'/'DIS', '110').
card_flavor_text('experiment kraj'/'DIS', '\"Of course it will grow beyond control—it was designed to choose its own evolution.\"\n—Momir Vig').
card_multiverse_id('experiment kraj'/'DIS', '107385').
card_watermark('experiment kraj'/'DIS', 'Simic').

card_in_set('fall', 'DIS').
card_original_type('fall'/'DIS', 'Sorcery').
card_original_text('fall'/'DIS', 'Return target creature card in a graveyard and target creature in play to their owners\' hands.\n//\nFall\n{B}{R}\nSorcery\nTarget player reveals two cards at random from his or her hand, then discards each nonland card revealed this way.').
card_first_print('fall', 'DIS').
card_image_name('fall'/'DIS', 'risefall').
card_uid('fall'/'DIS', 'DIS:Fall:risefall').
card_rarity('fall'/'DIS', 'Uncommon').
card_artist('fall'/'DIS', 'Pete Venters').
card_number('fall'/'DIS', '156b').
card_multiverse_id('fall'/'DIS', '107423').
card_watermark('fall'/'DIS', 'Rakdos').

card_in_set('fertile imagination', 'DIS').
card_original_type('fertile imagination'/'DIS', 'Sorcery').
card_original_text('fertile imagination'/'DIS', 'Choose a card type. Target opponent reveals his or her hand. Put two 1/1 green Saproling creature tokens into play for each card of the chosen type revealed this way. (The card types are artifact, creature, enchantment, instant, land, and sorcery.)').
card_first_print('fertile imagination', 'DIS').
card_image_name('fertile imagination'/'DIS', 'fertile imagination').
card_uid('fertile imagination'/'DIS', 'DIS:Fertile Imagination:fertile imagination').
card_rarity('fertile imagination'/'DIS', 'Uncommon').
card_artist('fertile imagination'/'DIS', 'Dan Scott').
card_number('fertile imagination'/'DIS', '84').
card_multiverse_id('fertile imagination'/'DIS', '111203').

card_in_set('flame-kin war scout', 'DIS').
card_original_type('flame-kin war scout'/'DIS', 'Creature — Elemental Scout').
card_original_text('flame-kin war scout'/'DIS', 'When another creature comes into play, sacrifice Flame-Kin War Scout. If you do, Flame-Kin War Scout deals 4 damage to that creature.').
card_first_print('flame-kin war scout', 'DIS').
card_image_name('flame-kin war scout'/'DIS', 'flame-kin war scout').
card_uid('flame-kin war scout'/'DIS', 'DIS:Flame-Kin War Scout:flame-kin war scout').
card_rarity('flame-kin war scout'/'DIS', 'Uncommon').
card_artist('flame-kin war scout'/'DIS', 'Fred Hooper').
card_number('flame-kin war scout'/'DIS', '61').
card_flavor_text('flame-kin war scout'/'DIS', '\"The forward flame-kin are popping. We have incoming!\"\n—Svar, Boros signaler').
card_multiverse_id('flame-kin war scout'/'DIS', '107337').

card_in_set('flaring flame-kin', 'DIS').
card_original_type('flaring flame-kin'/'DIS', 'Creature — Elemental Warrior').
card_original_text('flaring flame-kin'/'DIS', 'As long as Flaring Flame-Kin is enchanted, it gets +2/+2, has trample, and has \"{R}: Flaring Flame-Kin gets +1/+0 until end of turn.\"').
card_first_print('flaring flame-kin', 'DIS').
card_image_name('flaring flame-kin'/'DIS', 'flaring flame-kin').
card_uid('flaring flame-kin'/'DIS', 'DIS:Flaring Flame-Kin:flaring flame-kin').
card_rarity('flaring flame-kin'/'DIS', 'Uncommon').
card_artist('flaring flame-kin'/'DIS', 'Brian Hagan').
card_number('flaring flame-kin'/'DIS', '62').
card_flavor_text('flaring flame-kin'/'DIS', 'A flame-kin is always formidable, but feeding its fire with a sorcerous gift grants it the power to devastate armies.').
card_multiverse_id('flaring flame-kin'/'DIS', '111247').

card_in_set('flash foliage', 'DIS').
card_original_type('flash foliage'/'DIS', 'Instant').
card_original_text('flash foliage'/'DIS', 'Put a 1/1 green Saproling creature token into play blocking target creature attacking you.\nDraw a card.').
card_first_print('flash foliage', 'DIS').
card_image_name('flash foliage'/'DIS', 'flash foliage').
card_uid('flash foliage'/'DIS', 'DIS:Flash Foliage:flash foliage').
card_rarity('flash foliage'/'DIS', 'Uncommon').
card_artist('flash foliage'/'DIS', 'Ron Spears').
card_number('flash foliage'/'DIS', '85').
card_flavor_text('flash foliage'/'DIS', '\"This undemanding vegetation takes root in a mote of dust but grows incredibly quickly.\"\n—Simic research notes').
card_multiverse_id('flash foliage'/'DIS', '107264').

card_in_set('freewind equenaut', 'DIS').
card_original_type('freewind equenaut'/'DIS', 'Creature — Human Archer').
card_original_text('freewind equenaut'/'DIS', 'Flying\nAs long as Freewind Equenaut is enchanted, it has \"{T}: Freewind Equenaut deals 2 damage to target attacking or blocking creature.\"').
card_first_print('freewind equenaut', 'DIS').
card_image_name('freewind equenaut'/'DIS', 'freewind equenaut').
card_uid('freewind equenaut'/'DIS', 'DIS:Freewind Equenaut:freewind equenaut').
card_rarity('freewind equenaut'/'DIS', 'Common').
card_artist('freewind equenaut'/'DIS', 'Rebecca Guay').
card_number('freewind equenaut'/'DIS', '9').
card_flavor_text('freewind equenaut'/'DIS', 'Confront her, and feel the hooves of her steed. Ignore her, and feel the sting of her arrow.').
card_multiverse_id('freewind equenaut'/'DIS', '107253').

card_in_set('ghost quarter', 'DIS').
card_original_type('ghost quarter'/'DIS', 'Land').
card_original_text('ghost quarter'/'DIS', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Ghost Quarter: Destroy target land. Its controller may search his or her library for a basic land card, put it into play, then shuffle his or her library.').
card_first_print('ghost quarter', 'DIS').
card_image_name('ghost quarter'/'DIS', 'ghost quarter').
card_uid('ghost quarter'/'DIS', 'DIS:Ghost Quarter:ghost quarter').
card_rarity('ghost quarter'/'DIS', 'Uncommon').
card_artist('ghost quarter'/'DIS', 'Heather Hudson').
card_number('ghost quarter'/'DIS', '173').
card_flavor_text('ghost quarter'/'DIS', 'Where wasted life cries out to be reborn.').
card_multiverse_id('ghost quarter'/'DIS', '107504').

card_in_set('gnat alley creeper', 'DIS').
card_original_type('gnat alley creeper'/'DIS', 'Creature — Human Rogue').
card_original_text('gnat alley creeper'/'DIS', 'Gnat Alley Creeper can\'t be blocked by creatures with flying.').
card_first_print('gnat alley creeper', 'DIS').
card_image_name('gnat alley creeper'/'DIS', 'gnat alley creeper').
card_uid('gnat alley creeper'/'DIS', 'DIS:Gnat Alley Creeper:gnat alley creeper').
card_rarity('gnat alley creeper'/'DIS', 'Uncommon').
card_artist('gnat alley creeper'/'DIS', 'Pete Venters').
card_number('gnat alley creeper'/'DIS', '63').
card_flavor_text('gnat alley creeper'/'DIS', 'Despite its diminutive name, Gnat Alley is the longest street in Ravnica. Mile after twisting mile, it threads its way among the broader, safer thoroughfares like a parasite.').
card_multiverse_id('gnat alley creeper'/'DIS', '107526').

card_in_set('gobhobbler rats', 'DIS').
card_original_type('gobhobbler rats'/'DIS', 'Creature — Rat').
card_original_text('gobhobbler rats'/'DIS', 'Hellbent Gobhobbler Rats gets +1/+0 and has \"{B}: Regenerate Gobhobbler Rats\" as long as you have no cards in hand.').
card_first_print('gobhobbler rats', 'DIS').
card_image_name('gobhobbler rats'/'DIS', 'gobhobbler rats').
card_uid('gobhobbler rats'/'DIS', 'DIS:Gobhobbler Rats:gobhobbler rats').
card_rarity('gobhobbler rats'/'DIS', 'Common').
card_artist('gobhobbler rats'/'DIS', 'Michael Sutfin').
card_number('gobhobbler rats'/'DIS', '111').
card_flavor_text('gobhobbler rats'/'DIS', 'Lyzolda\'s pet rats eat better than the average citizen. In fact, they can usually be found eating the average citizen.').
card_multiverse_id('gobhobbler rats'/'DIS', '107542').
card_watermark('gobhobbler rats'/'DIS', 'Rakdos').

card_in_set('govern the guildless', 'DIS').
card_original_type('govern the guildless'/'DIS', 'Sorcery').
card_original_text('govern the guildless'/'DIS', 'Gain control of target monocolored creature.\nForecast {1}{U}, Reveal Govern the Guildless from your hand: Target creature becomes the color or colors of your choice until end of turn. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('govern the guildless', 'DIS').
card_image_name('govern the guildless'/'DIS', 'govern the guildless').
card_uid('govern the guildless'/'DIS', 'DIS:Govern the Guildless:govern the guildless').
card_rarity('govern the guildless'/'DIS', 'Rare').
card_artist('govern the guildless'/'DIS', 'Alex Horley-Orlandelli').
card_number('govern the guildless'/'DIS', '25').
card_multiverse_id('govern the guildless'/'DIS', '107395').
card_watermark('govern the guildless'/'DIS', 'Azorius').

card_in_set('grand arbiter augustin iv', 'DIS').
card_original_type('grand arbiter augustin iv'/'DIS', 'Legendary Creature — Human Advisor').
card_original_text('grand arbiter augustin iv'/'DIS', 'White spells you play cost {1} less to play.\nBlue spells you play cost {1} less to play.\nSpells your opponents play cost {1} more to play.').
card_first_print('grand arbiter augustin iv', 'DIS').
card_image_name('grand arbiter augustin iv'/'DIS', 'grand arbiter augustin iv').
card_uid('grand arbiter augustin iv'/'DIS', 'DIS:Grand Arbiter Augustin IV:grand arbiter augustin iv').
card_rarity('grand arbiter augustin iv'/'DIS', 'Rare').
card_artist('grand arbiter augustin iv'/'DIS', 'Zoltan Boros & Gabor Szikszai').
card_number('grand arbiter augustin iv'/'DIS', '112').
card_flavor_text('grand arbiter augustin iv'/'DIS', 'The Arbiter is a conduit of justice, a will so disciplined that it dispenses justice without ego or remorse.').
card_multiverse_id('grand arbiter augustin iv'/'DIS', '107329').
card_watermark('grand arbiter augustin iv'/'DIS', 'Azorius').

card_in_set('guardian of the guildpact', 'DIS').
card_original_type('guardian of the guildpact'/'DIS', 'Creature — Spirit').
card_original_text('guardian of the guildpact'/'DIS', 'Protection from monocolored').
card_first_print('guardian of the guildpact', 'DIS').
card_image_name('guardian of the guildpact'/'DIS', 'guardian of the guildpact').
card_uid('guardian of the guildpact'/'DIS', 'DIS:Guardian of the Guildpact:guardian of the guildpact').
card_rarity('guardian of the guildpact'/'DIS', 'Common').
card_artist('guardian of the guildpact'/'DIS', 'Fred Hooper').
card_number('guardian of the guildpact'/'DIS', '10').
card_flavor_text('guardian of the guildpact'/'DIS', 'The magic of the Guildpact gives aegis to the spirits pressed into its service. Upon entering the afterlife, they find new focus and are charged with defending the Guildpact against those who would see it broken.').
card_multiverse_id('guardian of the guildpact'/'DIS', '107583').

card_in_set('haazda exonerator', 'DIS').
card_original_type('haazda exonerator'/'DIS', 'Creature — Human Cleric').
card_original_text('haazda exonerator'/'DIS', '{T}, Sacrifice Haazda Exonerator: Destroy target Aura.').
card_first_print('haazda exonerator', 'DIS').
card_image_name('haazda exonerator'/'DIS', 'haazda exonerator').
card_uid('haazda exonerator'/'DIS', 'DIS:Haazda Exonerator:haazda exonerator').
card_rarity('haazda exonerator'/'DIS', 'Common').
card_artist('haazda exonerator'/'DIS', 'Kev Walker').
card_number('haazda exonerator'/'DIS', '11').
card_flavor_text('haazda exonerator'/'DIS', '\"This aura threatens the sanctity of your soul. Wrenching it free won\'t be easy on either of us. If you\'re ready, we\'ll begin.\"').
card_multiverse_id('haazda exonerator'/'DIS', '107467').

card_in_set('haazda shield mate', 'DIS').
card_original_type('haazda shield mate'/'DIS', 'Creature — Human Soldier').
card_original_text('haazda shield mate'/'DIS', 'At the beginning of your upkeep, sacrifice Haazda Shield Mate unless you pay {W}{W}.\n{W}: The next time a source of your choice would deal damage to you this turn, prevent that damage.').
card_first_print('haazda shield mate', 'DIS').
card_image_name('haazda shield mate'/'DIS', 'haazda shield mate').
card_uid('haazda shield mate'/'DIS', 'DIS:Haazda Shield Mate:haazda shield mate').
card_rarity('haazda shield mate'/'DIS', 'Rare').
card_artist('haazda shield mate'/'DIS', 'Ron Spears').
card_number('haazda shield mate'/'DIS', '12').
card_flavor_text('haazda shield mate'/'DIS', 'The Haazda shield is broad, protecting both the free and the guilded.').
card_multiverse_id('haazda shield mate'/'DIS', '107272').

card_in_set('hallowed fountain', 'DIS').
card_original_type('hallowed fountain'/'DIS', 'Land — Plains Island').
card_original_text('hallowed fountain'/'DIS', '({T}: Add {W} or {U} to your mana pool.)\nAs Hallowed Fountain comes into play, you may pay 2 life. If you don\'t, Hallowed Fountain comes into play tapped instead.').
card_first_print('hallowed fountain', 'DIS').
card_image_name('hallowed fountain'/'DIS', 'hallowed fountain').
card_uid('hallowed fountain'/'DIS', 'DIS:Hallowed Fountain:hallowed fountain').
card_rarity('hallowed fountain'/'DIS', 'Rare').
card_artist('hallowed fountain'/'DIS', 'Rob Alexander').
card_number('hallowed fountain'/'DIS', '174').
card_multiverse_id('hallowed fountain'/'DIS', '97071').
card_watermark('hallowed fountain'/'DIS', 'Azorius').

card_in_set('helium squirter', 'DIS').
card_original_type('helium squirter'/'DIS', 'Creature — Beast Mutant').
card_original_text('helium squirter'/'DIS', 'Graft 3 (This creature comes into play with three +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{1}: Target creature with a +1/+1 counter on it gains flying until end of turn.').
card_first_print('helium squirter', 'DIS').
card_image_name('helium squirter'/'DIS', 'helium squirter').
card_uid('helium squirter'/'DIS', 'DIS:Helium Squirter:helium squirter').
card_rarity('helium squirter'/'DIS', 'Common').
card_artist('helium squirter'/'DIS', 'Hideaki Takamura').
card_number('helium squirter'/'DIS', '26').
card_multiverse_id('helium squirter'/'DIS', '107535').
card_watermark('helium squirter'/'DIS', 'Simic').

card_in_set('hellhole rats', 'DIS').
card_original_type('hellhole rats'/'DIS', 'Creature — Rat').
card_original_text('hellhole rats'/'DIS', 'Haste\nWhen Hellhole Rats comes into play, target player discards a card. Hellhole Rats deals damage to that player equal to that card\'s converted mana cost.').
card_first_print('hellhole rats', 'DIS').
card_image_name('hellhole rats'/'DIS', 'hellhole rats').
card_uid('hellhole rats'/'DIS', 'DIS:Hellhole Rats:hellhole rats').
card_rarity('hellhole rats'/'DIS', 'Uncommon').
card_artist('hellhole rats'/'DIS', 'Kev Walker').
card_number('hellhole rats'/'DIS', '113').
card_flavor_text('hellhole rats'/'DIS', '\"I must speak with Momir Vig about creating a water-spouting watchdog.\"\n—Mathvan, Prahv scrollwarden').
card_multiverse_id('hellhole rats'/'DIS', '97100').
card_watermark('hellhole rats'/'DIS', 'Rakdos').

card_in_set('hide', 'DIS').
card_original_type('hide'/'DIS', 'Instant').
card_original_text('hide'/'DIS', 'Put target artifact or enchantment on the bottom of its owner\'s library.\n//\nSeek\n{W}{B}\nInstant\nSearch target opponent\'s library for a card and remove that card from the game. You gain life equal to its converted mana cost. Then that player shuffles his or her library.').
card_first_print('hide', 'DIS').
card_image_name('hide'/'DIS', 'hideseek').
card_uid('hide'/'DIS', 'DIS:Hide:hideseek').
card_rarity('hide'/'DIS', 'Rare').
card_artist('hide'/'DIS', 'Zoltan Boros & Gabor Szikszai').
card_number('hide'/'DIS', '151a').
card_multiverse_id('hide'/'DIS', '107315').
card_watermark('hide'/'DIS', 'Boros').

card_in_set('hit', 'DIS').
card_original_type('hit'/'DIS', 'Instant').
card_original_text('hit'/'DIS', 'Target player sacrifices an artifact or creature. Hit deals damage to that player equal to that permanent\'s converted mana cost.\n//\nRun\n{3}{R}{G}\nInstant\nAttacking creatures you control get +1/+0 until end of turn for each other attacking creature.').
card_first_print('hit', 'DIS').
card_image_name('hit'/'DIS', 'hitrun').
card_uid('hit'/'DIS', 'DIS:Hit:hitrun').
card_rarity('hit'/'DIS', 'Uncommon').
card_artist('hit'/'DIS', 'Darrell Riche').
card_number('hit'/'DIS', '152a').
card_multiverse_id('hit'/'DIS', '107387').
card_watermark('hit'/'DIS', 'Rakdos').

card_in_set('ignorant bliss', 'DIS').
card_original_type('ignorant bliss'/'DIS', 'Instant').
card_original_text('ignorant bliss'/'DIS', 'Remove all cards in your hand from the game face down. At end of turn, return those cards to your hand, then draw a card.').
card_first_print('ignorant bliss', 'DIS').
card_image_name('ignorant bliss'/'DIS', 'ignorant bliss').
card_uid('ignorant bliss'/'DIS', 'DIS:Ignorant Bliss:ignorant bliss').
card_rarity('ignorant bliss'/'DIS', 'Uncommon').
card_artist('ignorant bliss'/'DIS', 'Jeff Miracola').
card_number('ignorant bliss'/'DIS', '64').
card_flavor_text('ignorant bliss'/'DIS', '\"A quick step beyond oblivion lies a place so full of thoughts that it leaves no room for your own.\"\n—Quyzl, chronarch prodigy').
card_multiverse_id('ignorant bliss'/'DIS', '111281').

card_in_set('indrik stomphowler', 'DIS').
card_original_type('indrik stomphowler'/'DIS', 'Creature — Beast').
card_original_text('indrik stomphowler'/'DIS', 'When Indrik Stomphowler comes into play, destroy target artifact or enchantment.').
card_first_print('indrik stomphowler', 'DIS').
card_image_name('indrik stomphowler'/'DIS', 'indrik stomphowler').
card_uid('indrik stomphowler'/'DIS', 'DIS:Indrik Stomphowler:indrik stomphowler').
card_rarity('indrik stomphowler'/'DIS', 'Uncommon').
card_artist('indrik stomphowler'/'DIS', 'Carl Critchlow').
card_number('indrik stomphowler'/'DIS', '86').
card_flavor_text('indrik stomphowler'/'DIS', '\"An indrik\'s howl has destructive power much subtler than that of its crushing foot. The sound is mundane, but inaudible vibrations scatter and sunder magical contrivances.\"\n—Simic research notes').
card_multiverse_id('indrik stomphowler'/'DIS', '107377').

card_in_set('infernal tutor', 'DIS').
card_original_type('infernal tutor'/'DIS', 'Sorcery').
card_original_text('infernal tutor'/'DIS', 'Reveal a card from your hand. Search your library for a card with the same name as that card, reveal it, put it into your hand, then shuffle your library.\nHellbent If you have no cards in hand, instead search your library for a card, put it into your hand, then shuffle your library.').
card_first_print('infernal tutor', 'DIS').
card_image_name('infernal tutor'/'DIS', 'infernal tutor').
card_uid('infernal tutor'/'DIS', 'DIS:Infernal Tutor:infernal tutor').
card_rarity('infernal tutor'/'DIS', 'Rare').
card_artist('infernal tutor'/'DIS', 'Kev Walker').
card_number('infernal tutor'/'DIS', '46').
card_multiverse_id('infernal tutor'/'DIS', '107308').
card_watermark('infernal tutor'/'DIS', 'Rakdos').

card_in_set('isperia the inscrutable', 'DIS').
card_original_type('isperia the inscrutable'/'DIS', 'Legendary Creature — Sphinx').
card_original_text('isperia the inscrutable'/'DIS', 'Flying\nWhenever Isperia the Inscrutable deals combat damage to a player, name a card. That player reveals his or her hand. If he or she reveals the named card, search your library for a creature card with flying, reveal it, put it into your hand, then shuffle your library.').
card_first_print('isperia the inscrutable', 'DIS').
card_image_name('isperia the inscrutable'/'DIS', 'isperia the inscrutable').
card_uid('isperia the inscrutable'/'DIS', 'DIS:Isperia the Inscrutable:isperia the inscrutable').
card_rarity('isperia the inscrutable'/'DIS', 'Rare').
card_artist('isperia the inscrutable'/'DIS', 'Greg Staples').
card_number('isperia the inscrutable'/'DIS', '114').
card_multiverse_id('isperia the inscrutable'/'DIS', '107448').
card_watermark('isperia the inscrutable'/'DIS', 'Azorius').

card_in_set('jagged poppet', 'DIS').
card_original_type('jagged poppet'/'DIS', 'Creature — Ogre Warrior').
card_original_text('jagged poppet'/'DIS', 'Whenever Jagged Poppet is dealt damage, discard that many cards.\nHellbent Whenever Jagged Poppet deals combat damage to a player, if you have no cards in hand, that player discards cards equal to the damage.').
card_first_print('jagged poppet', 'DIS').
card_image_name('jagged poppet'/'DIS', 'jagged poppet').
card_uid('jagged poppet'/'DIS', 'DIS:Jagged Poppet:jagged poppet').
card_rarity('jagged poppet'/'DIS', 'Uncommon').
card_artist('jagged poppet'/'DIS', 'Jeff Miracola').
card_number('jagged poppet'/'DIS', '115').
card_flavor_text('jagged poppet'/'DIS', 'Few puppets are so willing.').
card_multiverse_id('jagged poppet'/'DIS', '97086').
card_watermark('jagged poppet'/'DIS', 'Rakdos').

card_in_set('kill-suit cultist', 'DIS').
card_original_type('kill-suit cultist'/'DIS', 'Creature — Goblin Berserker').
card_original_text('kill-suit cultist'/'DIS', 'Kill-Suit Cultist attacks each turn if able.\n{B}, Sacrifice Kill-Suit Cultist: The next time damage would be dealt to target creature this turn, destroy that creature instead.').
card_first_print('kill-suit cultist', 'DIS').
card_image_name('kill-suit cultist'/'DIS', 'kill-suit cultist').
card_uid('kill-suit cultist'/'DIS', 'DIS:Kill-Suit Cultist:kill-suit cultist').
card_rarity('kill-suit cultist'/'DIS', 'Common').
card_artist('kill-suit cultist'/'DIS', 'Alex Horley-Orlandelli').
card_number('kill-suit cultist'/'DIS', '65').
card_flavor_text('kill-suit cultist'/'DIS', 'Dressed to kill.').
card_multiverse_id('kill-suit cultist'/'DIS', '107493').
card_watermark('kill-suit cultist'/'DIS', 'Rakdos').

card_in_set('kindle the carnage', 'DIS').
card_original_type('kindle the carnage'/'DIS', 'Sorcery').
card_original_text('kindle the carnage'/'DIS', 'Discard a card at random. If you do, Kindle the Carnage deals damage equal to that card\'s converted mana cost to each creature. You may repeat this process any number of times.').
card_first_print('kindle the carnage', 'DIS').
card_image_name('kindle the carnage'/'DIS', 'kindle the carnage').
card_uid('kindle the carnage'/'DIS', 'DIS:Kindle the Carnage:kindle the carnage').
card_rarity('kindle the carnage'/'DIS', 'Uncommon').
card_artist('kindle the carnage'/'DIS', 'Dany Orizio').
card_number('kindle the carnage'/'DIS', '66').
card_flavor_text('kindle the carnage'/'DIS', '\"Start knockin\' heads, boys, and don\'t stop \'til the ragamuffyn sings!\"').
card_multiverse_id('kindle the carnage'/'DIS', '107446').

card_in_set('leafdrake roost', 'DIS').
card_original_type('leafdrake roost'/'DIS', 'Enchantment — Aura').
card_original_text('leafdrake roost'/'DIS', 'Enchant land\nEnchanted land has \"{G}{U}, {T}: Put a 2/2 green and blue Drake creature token with flying into play.\"').
card_first_print('leafdrake roost', 'DIS').
card_image_name('leafdrake roost'/'DIS', 'leafdrake roost').
card_uid('leafdrake roost'/'DIS', 'DIS:Leafdrake Roost:leafdrake roost').
card_rarity('leafdrake roost'/'DIS', 'Uncommon').
card_artist('leafdrake roost'/'DIS', 'Nick Percival').
card_number('leafdrake roost'/'DIS', '116').
card_flavor_text('leafdrake roost'/'DIS', '\"The best experiments are those whose successes replicate themselves.\"\n—Yolov, Simic bioengineer').
card_multiverse_id('leafdrake roost'/'DIS', '107557').
card_watermark('leafdrake roost'/'DIS', 'Simic').

card_in_set('loaming shaman', 'DIS').
card_original_type('loaming shaman'/'DIS', 'Creature — Centaur Shaman').
card_original_text('loaming shaman'/'DIS', 'When Loaming Shaman comes into play, target player shuffles any number of target cards from his or her graveyard into his or her library.').
card_first_print('loaming shaman', 'DIS').
card_image_name('loaming shaman'/'DIS', 'loaming shaman').
card_uid('loaming shaman'/'DIS', 'DIS:Loaming Shaman:loaming shaman').
card_rarity('loaming shaman'/'DIS', 'Rare').
card_artist('loaming shaman'/'DIS', 'Carl Critchlow').
card_number('loaming shaman'/'DIS', '87').
card_flavor_text('loaming shaman'/'DIS', 'His work determines who is remembered and who feeds the worms.').
card_multiverse_id('loaming shaman'/'DIS', '107568').

card_in_set('lyzolda, the blood witch', 'DIS').
card_original_type('lyzolda, the blood witch'/'DIS', 'Legendary Creature — Human Cleric').
card_original_text('lyzolda, the blood witch'/'DIS', '{2}, Sacrifice a creature: Lyzolda, the Blood Witch deals 2 damage to target creature or player if the sacrificed creature was red. Draw a card if the sacrificed creature was black.').
card_first_print('lyzolda, the blood witch', 'DIS').
card_image_name('lyzolda, the blood witch'/'DIS', 'lyzolda, the blood witch').
card_uid('lyzolda, the blood witch'/'DIS', 'DIS:Lyzolda, the Blood Witch:lyzolda, the blood witch').
card_rarity('lyzolda, the blood witch'/'DIS', 'Rare').
card_artist('lyzolda, the blood witch'/'DIS', 'Jim Nelson').
card_number('lyzolda, the blood witch'/'DIS', '117').
card_flavor_text('lyzolda, the blood witch'/'DIS', 'Sacrificial rites take place before an audience of cheering cultists, each begging to be the next on stage.').
card_multiverse_id('lyzolda, the blood witch'/'DIS', '107363').
card_watermark('lyzolda, the blood witch'/'DIS', 'Rakdos').

card_in_set('macabre waltz', 'DIS').
card_original_type('macabre waltz'/'DIS', 'Sorcery').
card_original_text('macabre waltz'/'DIS', 'Return up to two target creature cards from your graveyard to your hand, then discard a card.').
card_first_print('macabre waltz', 'DIS').
card_image_name('macabre waltz'/'DIS', 'macabre waltz').
card_uid('macabre waltz'/'DIS', 'DIS:Macabre Waltz:macabre waltz').
card_rarity('macabre waltz'/'DIS', 'Common').
card_artist('macabre waltz'/'DIS', 'Jim Murray').
card_number('macabre waltz'/'DIS', '47').
card_flavor_text('macabre waltz'/'DIS', '\"All dead move to the hollow rhythm of necromancy.\"\n—Savra').
card_multiverse_id('macabre waltz'/'DIS', '97116').

card_in_set('magewright\'s stone', 'DIS').
card_original_type('magewright\'s stone'/'DIS', 'Artifact').
card_original_text('magewright\'s stone'/'DIS', '{1}, {T}: Untap target creature that has an activated ability with {T} in its cost.').
card_first_print('magewright\'s stone', 'DIS').
card_image_name('magewright\'s stone'/'DIS', 'magewright\'s stone').
card_uid('magewright\'s stone'/'DIS', 'DIS:Magewright\'s Stone:magewright\'s stone').
card_rarity('magewright\'s stone'/'DIS', 'Uncommon').
card_artist('magewright\'s stone'/'DIS', 'Carl Critchlow').
card_number('magewright\'s stone'/'DIS', '162').
card_flavor_text('magewright\'s stone'/'DIS', 'The stones hark back to an age before civilization, when the living earth blessed all who trod its untamed wilds.').
card_multiverse_id('magewright\'s stone'/'DIS', '111259').

card_in_set('might of the nephilim', 'DIS').
card_original_type('might of the nephilim'/'DIS', 'Instant').
card_original_text('might of the nephilim'/'DIS', 'Target creature gets +2/+2 until end of turn for each of its colors.').
card_first_print('might of the nephilim', 'DIS').
card_image_name('might of the nephilim'/'DIS', 'might of the nephilim').
card_uid('might of the nephilim'/'DIS', 'DIS:Might of the Nephilim:might of the nephilim').
card_rarity('might of the nephilim'/'DIS', 'Uncommon').
card_artist('might of the nephilim'/'DIS', 'Paolo Parente').
card_number('might of the nephilim'/'DIS', '88').
card_flavor_text('might of the nephilim'/'DIS', '\"Send a runner to Tin Street, and tell \'em the delivery has been . . . uh . . . held up.\"\n—Bonmod, caravan rear guard').
card_multiverse_id('might of the nephilim'/'DIS', '107320').

card_in_set('minister of impediments', 'DIS').
card_original_type('minister of impediments'/'DIS', 'Creature — Human Advisor').
card_original_text('minister of impediments'/'DIS', '({W/U} can be paid with either {W} or {U}.)\n{T}: Tap target creature.').
card_first_print('minister of impediments', 'DIS').
card_image_name('minister of impediments'/'DIS', 'minister of impediments').
card_uid('minister of impediments'/'DIS', 'DIS:Minister of Impediments:minister of impediments').
card_rarity('minister of impediments'/'DIS', 'Common').
card_artist('minister of impediments'/'DIS', 'Brian Hagan').
card_number('minister of impediments'/'DIS', '144').
card_flavor_text('minister of impediments'/'DIS', 'When it takes forever to learn all the rules, no time is left for breaking them.').
card_multiverse_id('minister of impediments'/'DIS', '97083').
card_watermark('minister of impediments'/'DIS', 'Azorius').

card_in_set('mistral charger', 'DIS').
card_original_type('mistral charger'/'DIS', 'Creature — Pegasus').
card_original_text('mistral charger'/'DIS', 'Flying').
card_first_print('mistral charger', 'DIS').
card_image_name('mistral charger'/'DIS', 'mistral charger').
card_uid('mistral charger'/'DIS', 'DIS:Mistral Charger:mistral charger').
card_rarity('mistral charger'/'DIS', 'Uncommon').
card_artist('mistral charger'/'DIS', 'Terese Nielsen').
card_number('mistral charger'/'DIS', '13').
card_flavor_text('mistral charger'/'DIS', 'Some sky steeds break from the thundering herd to ride free on the open winds.').
card_multiverse_id('mistral charger'/'DIS', '107260').

card_in_set('momir vig, simic visionary', 'DIS').
card_original_type('momir vig, simic visionary'/'DIS', 'Legendary Creature — Elf Wizard').
card_original_text('momir vig, simic visionary'/'DIS', 'Whenever you play a green creature spell, you may search your library for a creature card and reveal it. If you do, shuffle your library and put that card on top of it.\nWhenever you play a blue creature spell, reveal the top card of your library. If it\'s a creature card, put that card into your hand.').
card_first_print('momir vig, simic visionary', 'DIS').
card_image_name('momir vig, simic visionary'/'DIS', 'momir vig, simic visionary').
card_uid('momir vig, simic visionary'/'DIS', 'DIS:Momir Vig, Simic Visionary:momir vig, simic visionary').
card_rarity('momir vig, simic visionary'/'DIS', 'Rare').
card_artist('momir vig, simic visionary'/'DIS', 'Zoltan Boros & Gabor Szikszai').
card_number('momir vig, simic visionary'/'DIS', '118').
card_multiverse_id('momir vig, simic visionary'/'DIS', '107506').
card_watermark('momir vig, simic visionary'/'DIS', 'Simic').

card_in_set('muse vessel', 'DIS').
card_original_type('muse vessel'/'DIS', 'Artifact').
card_original_text('muse vessel'/'DIS', '{3}, {T}: Target player removes a card in his or her hand from the game. Play this ability only any time you could play a sorcery.\n{1}: Choose a card removed from the game with Muse Vessel. You may play that card this turn.').
card_first_print('muse vessel', 'DIS').
card_image_name('muse vessel'/'DIS', 'muse vessel').
card_uid('muse vessel'/'DIS', 'DIS:Muse Vessel:muse vessel').
card_rarity('muse vessel'/'DIS', 'Rare').
card_artist('muse vessel'/'DIS', 'Christopher Rush').
card_number('muse vessel'/'DIS', '163').
card_multiverse_id('muse vessel'/'DIS', '107553').

card_in_set('nettling curse', 'DIS').
card_original_type('nettling curse'/'DIS', 'Enchantment — Aura').
card_original_text('nettling curse'/'DIS', 'Enchant creature\nWhenever enchanted creature attacks or blocks, its controller loses 3 life.\n{1}{R}: Enchanted creature attacks this turn if able.').
card_first_print('nettling curse', 'DIS').
card_image_name('nettling curse'/'DIS', 'nettling curse').
card_uid('nettling curse'/'DIS', 'DIS:Nettling Curse:nettling curse').
card_rarity('nettling curse'/'DIS', 'Common').
card_artist('nettling curse'/'DIS', 'Jeremy Jarvis & Wayne Reynolds').
card_number('nettling curse'/'DIS', '48').
card_flavor_text('nettling curse'/'DIS', '\"I call them ‘Poke\' and ‘Prod.\'\"\n—Chagrach, Rakdos cursemage').
card_multiverse_id('nettling curse'/'DIS', '107374').
card_watermark('nettling curse'/'DIS', 'Rakdos').

card_in_set('nightcreep', 'DIS').
card_original_type('nightcreep'/'DIS', 'Instant').
card_original_text('nightcreep'/'DIS', 'Until end of turn, all creatures become black and all lands become Swamps.').
card_first_print('nightcreep', 'DIS').
card_image_name('nightcreep'/'DIS', 'nightcreep').
card_uid('nightcreep'/'DIS', 'DIS:Nightcreep:nightcreep').
card_rarity('nightcreep'/'DIS', 'Uncommon').
card_artist('nightcreep'/'DIS', 'Jeff Miracola').
card_number('nightcreep'/'DIS', '49').
card_flavor_text('nightcreep'/'DIS', '\"Please tell me I\'m hallucinating. I\'d rather be crazy than here.\"\n—Gorev Hadszak, Wojek investigator').
card_multiverse_id('nightcreep'/'DIS', '107415').

card_in_set('nihilistic glee', 'DIS').
card_original_type('nihilistic glee'/'DIS', 'Enchantment').
card_original_text('nihilistic glee'/'DIS', '{2}{B}, Discard a card: Target opponent loses 1 life and you gain 1 life.\nHellbent {1}, Pay 2 life: Draw a card. Play this ability only if you have no cards in hand.').
card_first_print('nihilistic glee', 'DIS').
card_image_name('nihilistic glee'/'DIS', 'nihilistic glee').
card_uid('nihilistic glee'/'DIS', 'DIS:Nihilistic Glee:nihilistic glee').
card_rarity('nihilistic glee'/'DIS', 'Rare').
card_artist('nihilistic glee'/'DIS', 'Ron Spears & Wayne Reynolds').
card_number('nihilistic glee'/'DIS', '50').
card_flavor_text('nihilistic glee'/'DIS', '\"All ends in obliteration—love in hatred, life in death, and light in empty darkness.\"').
card_multiverse_id('nihilistic glee'/'DIS', '107517').
card_watermark('nihilistic glee'/'DIS', 'Rakdos').

card_in_set('novijen sages', 'DIS').
card_original_type('novijen sages'/'DIS', 'Creature — Human Advisor Mutant').
card_original_text('novijen sages'/'DIS', 'Graft 4 (This creature comes into play with four +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{1}, Remove two +1/+1 counters from among creatures you control: Draw a card.').
card_first_print('novijen sages', 'DIS').
card_image_name('novijen sages'/'DIS', 'novijen sages').
card_uid('novijen sages'/'DIS', 'DIS:Novijen Sages:novijen sages').
card_rarity('novijen sages'/'DIS', 'Rare').
card_artist('novijen sages'/'DIS', 'Luca Zontini').
card_number('novijen sages'/'DIS', '27').
card_multiverse_id('novijen sages'/'DIS', '97091').
card_watermark('novijen sages'/'DIS', 'Simic').

card_in_set('novijen, heart of progress', 'DIS').
card_original_type('novijen, heart of progress'/'DIS', 'Land').
card_original_text('novijen, heart of progress'/'DIS', '{T}: Add {1} to your mana pool.\n{G}{U}, {T}: Put a +1/+1 counter on each creature that came into play this turn.').
card_first_print('novijen, heart of progress', 'DIS').
card_image_name('novijen, heart of progress'/'DIS', 'novijen, heart of progress').
card_uid('novijen, heart of progress'/'DIS', 'DIS:Novijen, Heart of Progress:novijen, heart of progress').
card_rarity('novijen, heart of progress'/'DIS', 'Uncommon').
card_artist('novijen, heart of progress'/'DIS', 'Martina Pilcerova').
card_number('novijen, heart of progress'/'DIS', '175').
card_flavor_text('novijen, heart of progress'/'DIS', '\"The unnatural pressures of life in this city are best withstood by lifeforms that adapt with unnatural swiftness.\"\n—Momir Vig').
card_multiverse_id('novijen, heart of progress'/'DIS', '97099').
card_watermark('novijen, heart of progress'/'DIS', 'Simic').

card_in_set('ocular halo', 'DIS').
card_original_type('ocular halo'/'DIS', 'Enchantment — Aura').
card_original_text('ocular halo'/'DIS', 'Enchant creature\nEnchanted creature has \"{T}: Draw a card.\"\n{W}: Enchanted creature gains vigilance until end of turn.').
card_first_print('ocular halo', 'DIS').
card_image_name('ocular halo'/'DIS', 'ocular halo').
card_uid('ocular halo'/'DIS', 'DIS:Ocular Halo:ocular halo').
card_rarity('ocular halo'/'DIS', 'Common').
card_artist('ocular halo'/'DIS', 'Ralph Horsley').
card_number('ocular halo'/'DIS', '28').
card_flavor_text('ocular halo'/'DIS', 'While the soldier dreams, the eyes stand watch.').
card_multiverse_id('ocular halo'/'DIS', '107328').
card_watermark('ocular halo'/'DIS', 'Azorius').

card_in_set('odds', 'DIS').
card_original_type('odds'/'DIS', 'Instant').
card_original_text('odds'/'DIS', 'Flip a coin. If it comes up heads, counter target instant or sorcery spell. If it comes up tails, copy that spell and you may choose new targets for the copy.\n//\nEnds\n{3}{R}{W}\nInstant\nTarget player sacrifices two attacking creatures.').
card_first_print('odds', 'DIS').
card_image_name('odds'/'DIS', 'oddsends').
card_uid('odds'/'DIS', 'DIS:Odds:oddsends').
card_rarity('odds'/'DIS', 'Rare').
card_artist('odds'/'DIS', 'Michael Sutfin').
card_number('odds'/'DIS', '153a').
card_multiverse_id('odds'/'DIS', '107445').
card_watermark('odds'/'DIS', 'Izzet').

card_in_set('ogre gatecrasher', 'DIS').
card_original_type('ogre gatecrasher'/'DIS', 'Creature — Ogre Rogue').
card_original_text('ogre gatecrasher'/'DIS', 'When Ogre Gatecrasher comes into play, destroy target creature with defender.').
card_first_print('ogre gatecrasher', 'DIS').
card_image_name('ogre gatecrasher'/'DIS', 'ogre gatecrasher').
card_uid('ogre gatecrasher'/'DIS', 'DIS:Ogre Gatecrasher:ogre gatecrasher').
card_rarity('ogre gatecrasher'/'DIS', 'Common').
card_artist('ogre gatecrasher'/'DIS', 'Daren Bader').
card_number('ogre gatecrasher'/'DIS', '67').
card_flavor_text('ogre gatecrasher'/'DIS', 'A popular game among ogres is to see how many revolutions a head can make before it pops off.').
card_multiverse_id('ogre gatecrasher'/'DIS', '107389').

card_in_set('omnibian', 'DIS').
card_original_type('omnibian'/'DIS', 'Creature — Frog').
card_original_text('omnibian'/'DIS', '{T}: Target creature becomes a 3/3 Frog until end of turn.').
card_first_print('omnibian', 'DIS').
card_image_name('omnibian'/'DIS', 'omnibian').
card_uid('omnibian'/'DIS', 'DIS:Omnibian:omnibian').
card_rarity('omnibian'/'DIS', 'Rare').
card_artist('omnibian'/'DIS', 'Jim Nelson').
card_number('omnibian'/'DIS', '119').
card_flavor_text('omnibian'/'DIS', '\"This creature has hyperevolved chameleonic abilities. It no longer mimics its surroundings, instead forcing them to take on its form.\"\n—Simic research notes').
card_multiverse_id('omnibian'/'DIS', '83737').
card_watermark('omnibian'/'DIS', 'Simic').

card_in_set('overrule', 'DIS').
card_original_type('overrule'/'DIS', 'Instant').
card_original_text('overrule'/'DIS', 'Counter target spell unless its controller pays {X}. You gain X life.').
card_first_print('overrule', 'DIS').
card_image_name('overrule'/'DIS', 'overrule').
card_uid('overrule'/'DIS', 'DIS:Overrule:overrule').
card_rarity('overrule'/'DIS', 'Common').
card_artist('overrule'/'DIS', 'Alan Pollack').
card_number('overrule'/'DIS', '120').
card_flavor_text('overrule'/'DIS', 'With one commanding word, the spell was put down and a fine collected for its casting.').
card_multiverse_id('overrule'/'DIS', '97111').
card_watermark('overrule'/'DIS', 'Azorius').

card_in_set('pain magnification', 'DIS').
card_original_type('pain magnification'/'DIS', 'Enchantment').
card_original_text('pain magnification'/'DIS', 'Whenever an opponent is dealt 3 or more damage by a single source, that player discards a card.').
card_first_print('pain magnification', 'DIS').
card_image_name('pain magnification'/'DIS', 'pain magnification').
card_uid('pain magnification'/'DIS', 'DIS:Pain Magnification:pain magnification').
card_rarity('pain magnification'/'DIS', 'Uncommon').
card_artist('pain magnification'/'DIS', 'Aleksi Briclot').
card_number('pain magnification'/'DIS', '121').
card_flavor_text('pain magnification'/'DIS', 'The Rakdos make sure their victims remember their pain—or at least give up a little bit of their sanity in order to forget.').
card_multiverse_id('pain magnification'/'DIS', '97105').
card_watermark('pain magnification'/'DIS', 'Rakdos').

card_in_set('paladin of prahv', 'DIS').
card_original_type('paladin of prahv'/'DIS', 'Creature — Human Knight').
card_original_text('paladin of prahv'/'DIS', 'Whenever Paladin of Prahv deals damage, you gain that much life.\nForecast {1}{W}, Reveal Paladin of Prahv from your hand: Whenever target creature deals damage this turn, you gain that much life. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('paladin of prahv', 'DIS').
card_image_name('paladin of prahv'/'DIS', 'paladin of prahv').
card_uid('paladin of prahv'/'DIS', 'DIS:Paladin of Prahv:paladin of prahv').
card_rarity('paladin of prahv'/'DIS', 'Uncommon').
card_artist('paladin of prahv'/'DIS', 'William Simpson').
card_number('paladin of prahv'/'DIS', '14').
card_multiverse_id('paladin of prahv'/'DIS', '107433').
card_watermark('paladin of prahv'/'DIS', 'Azorius').

card_in_set('palliation accord', 'DIS').
card_original_type('palliation accord'/'DIS', 'Enchantment').
card_original_text('palliation accord'/'DIS', 'Whenever a creature an opponent controls becomes tapped, put a shield counter on Palliation Accord.\nRemove a shield counter from Palliation Accord: Prevent the next 1 damage that would be dealt to you this turn.').
card_first_print('palliation accord', 'DIS').
card_image_name('palliation accord'/'DIS', 'palliation accord').
card_uid('palliation accord'/'DIS', 'DIS:Palliation Accord:palliation accord').
card_rarity('palliation accord'/'DIS', 'Uncommon').
card_artist('palliation accord'/'DIS', 'William Simpson').
card_number('palliation accord'/'DIS', '122').
card_multiverse_id('palliation accord'/'DIS', '97118').
card_watermark('palliation accord'/'DIS', 'Azorius').

card_in_set('patagia viper', 'DIS').
card_original_type('patagia viper'/'DIS', 'Creature — Snake').
card_original_text('patagia viper'/'DIS', 'Flying\nWhen Patagia Viper comes into play, put two 1/1 green and blue Snake creature tokens into play.\nWhen Patagia Viper comes into play, sacrifice it unless {U} was spent to play it.').
card_first_print('patagia viper', 'DIS').
card_image_name('patagia viper'/'DIS', 'patagia viper').
card_uid('patagia viper'/'DIS', 'DIS:Patagia Viper:patagia viper').
card_rarity('patagia viper'/'DIS', 'Uncommon').
card_artist('patagia viper'/'DIS', 'Christopher Moeller').
card_number('patagia viper'/'DIS', '89').
card_multiverse_id('patagia viper'/'DIS', '107549').
card_watermark('patagia viper'/'DIS', 'Simic').

card_in_set('pillar of the paruns', 'DIS').
card_original_type('pillar of the paruns'/'DIS', 'Land').
card_original_text('pillar of the paruns'/'DIS', '{T}: Add one mana of any color to your mana pool. Spend this mana only to play a multicolored spell.').
card_first_print('pillar of the paruns', 'DIS').
card_image_name('pillar of the paruns'/'DIS', 'pillar of the paruns').
card_uid('pillar of the paruns'/'DIS', 'DIS:Pillar of the Paruns:pillar of the paruns').
card_rarity('pillar of the paruns'/'DIS', 'Rare').
card_artist('pillar of the paruns'/'DIS', 'Dany Orizio').
card_number('pillar of the paruns'/'DIS', '176').
card_flavor_text('pillar of the paruns'/'DIS', 'Built on the very ground where the Ten signed the Guildpact, the tower is a monument to the past and a reminder of who holds power in the present.').
card_multiverse_id('pillar of the paruns'/'DIS', '107279').

card_in_set('plaxcaster frogling', 'DIS').
card_original_type('plaxcaster frogling'/'DIS', 'Creature — Frog Mutant').
card_original_text('plaxcaster frogling'/'DIS', 'Graft 3 (This creature comes into play with three +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{2}: Target creature with a +1/+1 counter on it can\'t be the target of spells or abilities this turn.').
card_first_print('plaxcaster frogling', 'DIS').
card_image_name('plaxcaster frogling'/'DIS', 'plaxcaster frogling').
card_uid('plaxcaster frogling'/'DIS', 'DIS:Plaxcaster Frogling:plaxcaster frogling').
card_rarity('plaxcaster frogling'/'DIS', 'Uncommon').
card_artist('plaxcaster frogling'/'DIS', 'Greg Staples').
card_number('plaxcaster frogling'/'DIS', '123').
card_multiverse_id('plaxcaster frogling'/'DIS', '97104').
card_watermark('plaxcaster frogling'/'DIS', 'Simic').

card_in_set('plaxmanta', 'DIS').
card_original_type('plaxmanta'/'DIS', 'Creature — Beast').
card_original_text('plaxmanta'/'DIS', 'You may play Plaxmanta any time you could play an instant.\nWhen Plaxmanta comes into play, creatures you control can\'t be the targets of spells or abilities this turn.\nWhen Plaxmanta comes into play, sacrifice it unless {G} was spent to play it.').
card_first_print('plaxmanta', 'DIS').
card_image_name('plaxmanta'/'DIS', 'plaxmanta').
card_uid('plaxmanta'/'DIS', 'DIS:Plaxmanta:plaxmanta').
card_rarity('plaxmanta'/'DIS', 'Uncommon').
card_artist('plaxmanta'/'DIS', 'Alan Pollack').
card_number('plaxmanta'/'DIS', '29').
card_multiverse_id('plaxmanta'/'DIS', '107286').
card_watermark('plaxmanta'/'DIS', 'Simic').

card_in_set('plumes of peace', 'DIS').
card_original_type('plumes of peace'/'DIS', 'Enchantment — Aura').
card_original_text('plumes of peace'/'DIS', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nForecast {W}{U}, Reveal Plumes of Peace from your hand: Tap target creature. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('plumes of peace', 'DIS').
card_image_name('plumes of peace'/'DIS', 'plumes of peace').
card_uid('plumes of peace'/'DIS', 'DIS:Plumes of Peace:plumes of peace').
card_rarity('plumes of peace'/'DIS', 'Common').
card_artist('plumes of peace'/'DIS', 'Justin Sweet').
card_number('plumes of peace'/'DIS', '124').
card_multiverse_id('plumes of peace'/'DIS', '107379').
card_watermark('plumes of peace'/'DIS', 'Azorius').

card_in_set('prahv, spires of order', 'DIS').
card_original_type('prahv, spires of order'/'DIS', 'Land').
card_original_text('prahv, spires of order'/'DIS', '{T}: Add {1} to your mana pool.\n{4}{W}{U}, {T}: Prevent all damage a source of your choice would deal this turn.').
card_first_print('prahv, spires of order', 'DIS').
card_image_name('prahv, spires of order'/'DIS', 'prahv, spires of order').
card_uid('prahv, spires of order'/'DIS', 'DIS:Prahv, Spires of Order:prahv, spires of order').
card_rarity('prahv, spires of order'/'DIS', 'Uncommon').
card_artist('prahv, spires of order'/'DIS', 'Martina Pilcerova').
card_number('prahv, spires of order'/'DIS', '177').
card_flavor_text('prahv, spires of order'/'DIS', 'Prahv, where much work is done to make sure nothing is accomplished.').
card_multiverse_id('prahv, spires of order'/'DIS', '97117').
card_watermark('prahv, spires of order'/'DIS', 'Azorius').

card_in_set('pride of the clouds', 'DIS').
card_original_type('pride of the clouds'/'DIS', 'Creature — Elemental Cat').
card_original_text('pride of the clouds'/'DIS', 'Flying\nPride of the Clouds gets +1/+1 for each other creature in play with flying.\nForecast {2}{W}{U}, Reveal Pride of the Clouds from your hand: Put a 1/1 white and blue Bird creature token with flying into play. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('pride of the clouds', 'DIS').
card_image_name('pride of the clouds'/'DIS', 'pride of the clouds').
card_uid('pride of the clouds'/'DIS', 'DIS:Pride of the Clouds:pride of the clouds').
card_rarity('pride of the clouds'/'DIS', 'Rare').
card_artist('pride of the clouds'/'DIS', 'Rebecca Guay').
card_number('pride of the clouds'/'DIS', '125').
card_multiverse_id('pride of the clouds'/'DIS', '97097').
card_watermark('pride of the clouds'/'DIS', 'Azorius').

card_in_set('proclamation of rebirth', 'DIS').
card_original_type('proclamation of rebirth'/'DIS', 'Sorcery').
card_original_text('proclamation of rebirth'/'DIS', 'Return up to three target creature cards with converted mana cost 1 or less from your graveyard to play.\nForecast {5}{W}, Reveal Proclamation of Rebirth from your hand: Return target creature card with converted mana cost 1 or less from your graveyard to play. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('proclamation of rebirth', 'DIS').
card_image_name('proclamation of rebirth'/'DIS', 'proclamation of rebirth').
card_uid('proclamation of rebirth'/'DIS', 'DIS:Proclamation of Rebirth:proclamation of rebirth').
card_rarity('proclamation of rebirth'/'DIS', 'Rare').
card_artist('proclamation of rebirth'/'DIS', 'William Simpson').
card_number('proclamation of rebirth'/'DIS', '15').
card_multiverse_id('proclamation of rebirth'/'DIS', '107341').
card_watermark('proclamation of rebirth'/'DIS', 'Azorius').

card_in_set('proper burial', 'DIS').
card_original_type('proper burial'/'DIS', 'Enchantment').
card_original_text('proper burial'/'DIS', 'Whenever a creature you control is put into a graveyard from play, you gain life equal to that creature\'s toughness.').
card_first_print('proper burial', 'DIS').
card_image_name('proper burial'/'DIS', 'proper burial').
card_uid('proper burial'/'DIS', 'DIS:Proper Burial:proper burial').
card_rarity('proper burial'/'DIS', 'Rare').
card_artist('proper burial'/'DIS', 'Luca Zontini').
card_number('proper burial'/'DIS', '16').
card_flavor_text('proper burial'/'DIS', 'Properly honored dead gain the peace of the grave and freedom from the call of the Ghost Quarter.').
card_multiverse_id('proper burial'/'DIS', '107466').

card_in_set('protean hulk', 'DIS').
card_original_type('protean hulk'/'DIS', 'Creature — Beast').
card_original_text('protean hulk'/'DIS', 'When Protean Hulk is put into a graveyard from play, search your library for any number of creature cards with total converted mana cost 6 or less and put them into play. Then shuffle your library.').
card_first_print('protean hulk', 'DIS').
card_image_name('protean hulk'/'DIS', 'protean hulk').
card_uid('protean hulk'/'DIS', 'DIS:Protean Hulk:protean hulk').
card_rarity('protean hulk'/'DIS', 'Rare').
card_artist('protean hulk'/'DIS', 'Matt Cavotta').
card_number('protean hulk'/'DIS', '90').
card_flavor_text('protean hulk'/'DIS', '\"Meat and eggs. We eat!\"\n—Borborygmos').
card_multiverse_id('protean hulk'/'DIS', '107598').

card_in_set('psychic possession', 'DIS').
card_original_type('psychic possession'/'DIS', 'Enchantment — Aura').
card_original_text('psychic possession'/'DIS', 'Enchant opponent\nSkip your draw step.\nWhenever enchanted opponent draws a card, you may draw a card.').
card_first_print('psychic possession', 'DIS').
card_image_name('psychic possession'/'DIS', 'psychic possession').
card_uid('psychic possession'/'DIS', 'DIS:Psychic Possession:psychic possession').
card_rarity('psychic possession'/'DIS', 'Rare').
card_artist('psychic possession'/'DIS', 'Mark Tedin').
card_number('psychic possession'/'DIS', '30').
card_flavor_text('psychic possession'/'DIS', '\"You will do the thinking for the both of us!\"').
card_multiverse_id('psychic possession'/'DIS', '107254').

card_in_set('psychotic fury', 'DIS').
card_original_type('psychotic fury'/'DIS', 'Instant').
card_original_text('psychotic fury'/'DIS', 'Target multicolored creature gains double strike until end of turn.\nDraw a card.').
card_first_print('psychotic fury', 'DIS').
card_image_name('psychotic fury'/'DIS', 'psychotic fury').
card_uid('psychotic fury'/'DIS', 'DIS:Psychotic Fury:psychotic fury').
card_rarity('psychotic fury'/'DIS', 'Common').
card_artist('psychotic fury'/'DIS', 'Kev Walker').
card_number('psychotic fury'/'DIS', '68').
card_flavor_text('psychotic fury'/'DIS', '\"Rage, my servants. Our ancient power is renewed in blood.\"\n—Rakdos').
card_multiverse_id('psychotic fury'/'DIS', '107338').

card_in_set('punishment', 'DIS').
card_original_type('punishment'/'DIS', 'Sorcery').
card_original_text('punishment'/'DIS', 'Put target creature or enchantment card in an opponent\'s graveyard into play under your control.\n//\nPunishment\n{X}{B}{G}\nSorcery\nDestroy each artifact, creature, and enchantment with converted mana cost X.').
card_first_print('punishment', 'DIS').
card_image_name('punishment'/'DIS', 'crimepunishment').
card_uid('punishment'/'DIS', 'DIS:Punishment:crimepunishment').
card_rarity('punishment'/'DIS', 'Rare').
card_artist('punishment'/'DIS', 'Randy Gallegos').
card_number('punishment'/'DIS', '150b').
card_multiverse_id('punishment'/'DIS', '107285').
card_watermark('punishment'/'DIS', 'Golgari').

card_in_set('pure', 'DIS').
card_original_type('pure'/'DIS', 'Sorcery').
card_original_text('pure'/'DIS', 'Destroy target multicolored permanent.\n//\nSimple\n{1}{G}{W}\nSorcery\nDestroy all Auras and Equipment.').
card_first_print('pure', 'DIS').
card_image_name('pure'/'DIS', 'puresimple').
card_uid('pure'/'DIS', 'DIS:Pure:puresimple').
card_rarity('pure'/'DIS', 'Uncommon').
card_artist('pure'/'DIS', 'Paolo Parente').
card_number('pure'/'DIS', '154a').
card_multiverse_id('pure'/'DIS', '107532').
card_watermark('pure'/'DIS', 'Gruul').

card_in_set('ragamuffyn', 'DIS').
card_original_type('ragamuffyn'/'DIS', 'Creature — Zombie Cleric').
card_original_text('ragamuffyn'/'DIS', 'Hellbent {T}, Sacrifice a creature or land: Draw a card. Play this ability only if you have no cards in hand.').
card_first_print('ragamuffyn', 'DIS').
card_image_name('ragamuffyn'/'DIS', 'ragamuffyn').
card_uid('ragamuffyn'/'DIS', 'DIS:Ragamuffyn:ragamuffyn').
card_rarity('ragamuffyn'/'DIS', 'Uncommon').
card_artist('ragamuffyn'/'DIS', 'rk post').
card_number('ragamuffyn'/'DIS', '51').
card_flavor_text('ragamuffyn'/'DIS', 'They cut the stitches and give her sips of her brew. Then they sew her up again, and she\'s back to stirring.').
card_multiverse_id('ragamuffyn'/'DIS', '107570').
card_watermark('ragamuffyn'/'DIS', 'Rakdos').

card_in_set('rain of gore', 'DIS').
card_original_type('rain of gore'/'DIS', 'Enchantment').
card_original_text('rain of gore'/'DIS', 'If a spell or ability would cause its controller to gain life, that player loses that much life instead.').
card_first_print('rain of gore', 'DIS').
card_image_name('rain of gore'/'DIS', 'rain of gore').
card_uid('rain of gore'/'DIS', 'DIS:Rain of Gore:rain of gore').
card_rarity('rain of gore'/'DIS', 'Rare').
card_artist('rain of gore'/'DIS', 'Fred Hooper').
card_number('rain of gore'/'DIS', '126').
card_flavor_text('rain of gore'/'DIS', '\"Stay indoors, away from what seeps over the thresholds. This is the Demon\'s work, and only ill can come of it.\"\n—Belko, owner of Titan\'s Keg tavern').
card_multiverse_id('rain of gore'/'DIS', '107358').
card_watermark('rain of gore'/'DIS', 'Rakdos').

card_in_set('rakdos augermage', 'DIS').
card_original_type('rakdos augermage'/'DIS', 'Creature — Human Wizard').
card_original_text('rakdos augermage'/'DIS', 'First strike\n{T}: Reveal your hand and discard a card of target opponent\'s choice. Then that player reveals his or her hand and discards a card of your choice. Play this ability only any time you could play a sorcery.').
card_first_print('rakdos augermage', 'DIS').
card_image_name('rakdos augermage'/'DIS', 'rakdos augermage').
card_uid('rakdos augermage'/'DIS', 'DIS:Rakdos Augermage:rakdos augermage').
card_rarity('rakdos augermage'/'DIS', 'Rare').
card_artist('rakdos augermage'/'DIS', 'Zoltan Boros & Gabor Szikszai').
card_number('rakdos augermage'/'DIS', '127').
card_flavor_text('rakdos augermage'/'DIS', '\"Great minds bleed alike.\"').
card_multiverse_id('rakdos augermage'/'DIS', '107543').
card_watermark('rakdos augermage'/'DIS', 'Rakdos').

card_in_set('rakdos carnarium', 'DIS').
card_original_type('rakdos carnarium'/'DIS', 'Land').
card_original_text('rakdos carnarium'/'DIS', 'Rakdos Carnarium comes into play tapped.\nWhen Rakdos Carnarium comes into play, return a land you control to its owner\'s hand.\n{T}: Add {B}{R} to your mana pool.').
card_first_print('rakdos carnarium', 'DIS').
card_image_name('rakdos carnarium'/'DIS', 'rakdos carnarium').
card_uid('rakdos carnarium'/'DIS', 'DIS:Rakdos Carnarium:rakdos carnarium').
card_rarity('rakdos carnarium'/'DIS', 'Common').
card_artist('rakdos carnarium'/'DIS', 'John Avon').
card_number('rakdos carnarium'/'DIS', '178').
card_multiverse_id('rakdos carnarium'/'DIS', '97082').
card_watermark('rakdos carnarium'/'DIS', 'Rakdos').

card_in_set('rakdos guildmage', 'DIS').
card_original_type('rakdos guildmage'/'DIS', 'Creature — Zombie Shaman').
card_original_text('rakdos guildmage'/'DIS', '({B/R} can be paid with either {B} or {R}.)\n{3}{B}, Discard a card: Target creature gets -2/-2 until end of turn.\n{3}{R}: Put a 2/1 red Goblin creature token with haste into play. Remove it from the game at end of turn.').
card_image_name('rakdos guildmage'/'DIS', 'rakdos guildmage').
card_uid('rakdos guildmage'/'DIS', 'DIS:Rakdos Guildmage:rakdos guildmage').
card_rarity('rakdos guildmage'/'DIS', 'Uncommon').
card_artist('rakdos guildmage'/'DIS', 'Jeremy Jarvis').
card_number('rakdos guildmage'/'DIS', '145').
card_multiverse_id('rakdos guildmage'/'DIS', '97108').
card_watermark('rakdos guildmage'/'DIS', 'Rakdos').

card_in_set('rakdos ickspitter', 'DIS').
card_original_type('rakdos ickspitter'/'DIS', 'Creature — Thrull').
card_original_text('rakdos ickspitter'/'DIS', '{T}: Rakdos Ickspitter deals 1 damage to target creature and that creature\'s controller loses 1 life.').
card_first_print('rakdos ickspitter', 'DIS').
card_image_name('rakdos ickspitter'/'DIS', 'rakdos ickspitter').
card_uid('rakdos ickspitter'/'DIS', 'DIS:Rakdos Ickspitter:rakdos ickspitter').
card_rarity('rakdos ickspitter'/'DIS', 'Common').
card_artist('rakdos ickspitter'/'DIS', 'Christopher Rush').
card_number('rakdos ickspitter'/'DIS', '128').
card_flavor_text('rakdos ickspitter'/'DIS', '\"Our thrulls dissolve from the inside out in a matter of days. Shoddy work, but they\'re a scream while they last.\"\n—Uzric, Rakdos guildmage').
card_multiverse_id('rakdos ickspitter'/'DIS', '97074').
card_watermark('rakdos ickspitter'/'DIS', 'Rakdos').

card_in_set('rakdos pit dragon', 'DIS').
card_original_type('rakdos pit dragon'/'DIS', 'Creature — Dragon').
card_original_text('rakdos pit dragon'/'DIS', '{R}{R}: Rakdos Pit Dragon gains flying until end of turn.\n{R}: Rakdos Pit Dragon gets +1/+0 until end of turn.\nHellbent Rakdos Pit Dragon has double strike as long as you have no cards in hand.').
card_first_print('rakdos pit dragon', 'DIS').
card_image_name('rakdos pit dragon'/'DIS', 'rakdos pit dragon').
card_uid('rakdos pit dragon'/'DIS', 'DIS:Rakdos Pit Dragon:rakdos pit dragon').
card_rarity('rakdos pit dragon'/'DIS', 'Rare').
card_artist('rakdos pit dragon'/'DIS', 'Kev Walker').
card_number('rakdos pit dragon'/'DIS', '69').
card_multiverse_id('rakdos pit dragon'/'DIS', '111188').
card_watermark('rakdos pit dragon'/'DIS', 'Rakdos').

card_in_set('rakdos riteknife', 'DIS').
card_original_type('rakdos riteknife'/'DIS', 'Artifact — Equipment').
card_original_text('rakdos riteknife'/'DIS', 'Equipped creature gets +1/+0 for each blood counter on Rakdos Riteknife and has \"{T}, Sacrifice a creature: Put a blood counter on Rakdos Riteknife.\"\n{B}{R}, Sacrifice Rakdos Riteknife: Target player sacrifices a permanent for each blood counter on Rakdos Riteknife.\nEquip {2}').
card_first_print('rakdos riteknife', 'DIS').
card_image_name('rakdos riteknife'/'DIS', 'rakdos riteknife').
card_uid('rakdos riteknife'/'DIS', 'DIS:Rakdos Riteknife:rakdos riteknife').
card_rarity('rakdos riteknife'/'DIS', 'Rare').
card_artist('rakdos riteknife'/'DIS', 'Jim Nelson').
card_number('rakdos riteknife'/'DIS', '164').
card_multiverse_id('rakdos riteknife'/'DIS', '97119').
card_watermark('rakdos riteknife'/'DIS', 'Rakdos').

card_in_set('rakdos signet', 'DIS').
card_original_type('rakdos signet'/'DIS', 'Artifact').
card_original_text('rakdos signet'/'DIS', '{1}, {T}: Add {B}{R} to your mana pool.').
card_first_print('rakdos signet', 'DIS').
card_image_name('rakdos signet'/'DIS', 'rakdos signet').
card_uid('rakdos signet'/'DIS', 'DIS:Rakdos Signet:rakdos signet').
card_rarity('rakdos signet'/'DIS', 'Common').
card_artist('rakdos signet'/'DIS', 'Greg Hildebrandt').
card_number('rakdos signet'/'DIS', '165').
card_flavor_text('rakdos signet'/'DIS', 'Made of bone and boiled in blood, a Rakdos signet is not considered finished until it has been used as a murder weapon.').
card_multiverse_id('rakdos signet'/'DIS', '97098').
card_watermark('rakdos signet'/'DIS', 'Rakdos').

card_in_set('rakdos the defiler', 'DIS').
card_original_type('rakdos the defiler'/'DIS', 'Legendary Creature — Demon').
card_original_text('rakdos the defiler'/'DIS', 'Flying, trample\nWhenever Rakdos the Defiler attacks, sacrifice half the non-Demon permanents you control, rounded up.\nWhenever Rakdos deals combat damage to a player, that player sacrifices half the non-Demon permanents he or she controls, rounded up.').
card_first_print('rakdos the defiler', 'DIS').
card_image_name('rakdos the defiler'/'DIS', 'rakdos the defiler').
card_uid('rakdos the defiler'/'DIS', 'DIS:Rakdos the Defiler:rakdos the defiler').
card_rarity('rakdos the defiler'/'DIS', 'Rare').
card_artist('rakdos the defiler'/'DIS', 'Zoltan Boros & Gabor Szikszai').
card_number('rakdos the defiler'/'DIS', '129').
card_multiverse_id('rakdos the defiler'/'DIS', '107438').
card_watermark('rakdos the defiler'/'DIS', 'Rakdos').

card_in_set('ratcatcher', 'DIS').
card_original_type('ratcatcher'/'DIS', 'Creature — Ogre Rogue').
card_original_text('ratcatcher'/'DIS', 'Fear\nAt the beginning of your upkeep, you may search your library for a Rat card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('ratcatcher', 'DIS').
card_image_name('ratcatcher'/'DIS', 'ratcatcher').
card_uid('ratcatcher'/'DIS', 'DIS:Ratcatcher:ratcatcher').
card_rarity('ratcatcher'/'DIS', 'Rare').
card_artist('ratcatcher'/'DIS', 'Dan Scott').
card_number('ratcatcher'/'DIS', '52').
card_flavor_text('ratcatcher'/'DIS', 'An ogre\'s poisonous stench is like fine Kashkaval cheese to the noses lowest to the ground.').
card_multiverse_id('ratcatcher'/'DIS', '107273').

card_in_set('research', 'DIS').
card_original_type('research'/'DIS', 'Instant').
card_original_text('research'/'DIS', 'Choose up to four cards you own from outside the game and shuffle them into your library.\n//\nDevelopment\n{3}{U}{R}\nInstant\nPut a 3/1 red Elemental creature token into play unless an opponent lets you draw a card. Repeat this process two more times.').
card_first_print('research', 'DIS').
card_image_name('research'/'DIS', 'researchdevelopment').
card_uid('research'/'DIS', 'DIS:Research:researchdevelopment').
card_rarity('research'/'DIS', 'Rare').
card_artist('research'/'DIS', 'Greg Staples').
card_number('research'/'DIS', '155a').
card_multiverse_id('research'/'DIS', '107375').
card_watermark('research'/'DIS', 'Simic').

card_in_set('riot spikes', 'DIS').
card_original_type('riot spikes'/'DIS', 'Enchantment — Aura').
card_original_text('riot spikes'/'DIS', '({B/R} can be paid with either {B} or {R}.)\nEnchant creature\nEnchanted creature gets +2/-1.').
card_first_print('riot spikes', 'DIS').
card_image_name('riot spikes'/'DIS', 'riot spikes').
card_uid('riot spikes'/'DIS', 'DIS:Riot Spikes:riot spikes').
card_rarity('riot spikes'/'DIS', 'Common').
card_artist('riot spikes'/'DIS', 'Christopher Moeller').
card_number('riot spikes'/'DIS', '146').
card_flavor_text('riot spikes'/'DIS', 'Most auramancers would have let the spikes hover just above the skin. Having the spikes rip through the skin from beneath was a touch added by Rakdos himself.').
card_multiverse_id('riot spikes'/'DIS', '97076').
card_watermark('riot spikes'/'DIS', 'Rakdos').

card_in_set('rise', 'DIS').
card_original_type('rise'/'DIS', 'Sorcery').
card_original_text('rise'/'DIS', 'Return target creature card in a graveyard and target creature in play to their owners\' hands.\n//\nFall\n{B}{R}\nSorcery\nTarget player reveals two cards at random from his or her hand, then discards each nonland card revealed this way.').
card_first_print('rise', 'DIS').
card_image_name('rise'/'DIS', 'risefall').
card_uid('rise'/'DIS', 'DIS:Rise:risefall').
card_rarity('rise'/'DIS', 'Uncommon').
card_artist('rise'/'DIS', 'Pete Venters').
card_number('rise'/'DIS', '156a').
card_multiverse_id('rise'/'DIS', '107423').
card_watermark('rise'/'DIS', 'Dimir').

card_in_set('rix maadi, dungeon palace', 'DIS').
card_original_type('rix maadi, dungeon palace'/'DIS', 'Land').
card_original_text('rix maadi, dungeon palace'/'DIS', '{T}: Add {1} to your mana pool.\n{1}{B}{R}, {T}: Each player discards a card. Play this ability only any time you could play a sorcery.').
card_first_print('rix maadi, dungeon palace', 'DIS').
card_image_name('rix maadi, dungeon palace'/'DIS', 'rix maadi, dungeon palace').
card_uid('rix maadi, dungeon palace'/'DIS', 'DIS:Rix Maadi, Dungeon Palace:rix maadi, dungeon palace').
card_rarity('rix maadi, dungeon palace'/'DIS', 'Uncommon').
card_artist('rix maadi, dungeon palace'/'DIS', 'Martina Pilcerova').
card_number('rix maadi, dungeon palace'/'DIS', '179').
card_flavor_text('rix maadi, dungeon palace'/'DIS', 'Even miles away, if you put your ear to the sewer vents, you can hear the screams and the tempestuous laughter.').
card_multiverse_id('rix maadi, dungeon palace'/'DIS', '97072').
card_watermark('rix maadi, dungeon palace'/'DIS', 'Rakdos').

card_in_set('run', 'DIS').
card_original_type('run'/'DIS', 'Instant').
card_original_text('run'/'DIS', 'Target player sacrifices an artifact or creature. Hit deals damage to that player equal to that permanent\'s converted mana cost.\n//\nRun\n{3}{R}{G}\nInstant\nAttacking creatures you control get +1/+0 until end of turn for each other attacking creature.').
card_first_print('run', 'DIS').
card_image_name('run'/'DIS', 'hitrun').
card_uid('run'/'DIS', 'DIS:Run:hitrun').
card_rarity('run'/'DIS', 'Uncommon').
card_artist('run'/'DIS', 'Darrell Riche').
card_number('run'/'DIS', '152b').
card_multiverse_id('run'/'DIS', '107387').
card_watermark('run'/'DIS', 'Gruul').

card_in_set('sandstorm eidolon', 'DIS').
card_original_type('sandstorm eidolon'/'DIS', 'Creature — Spirit').
card_original_text('sandstorm eidolon'/'DIS', '{R}, Sacrifice Sandstorm Eidolon: Target creature can\'t block this turn.\nWhenever you play a multicolored spell, you may return Sandstorm Eidolon from your graveyard to your hand.').
card_first_print('sandstorm eidolon', 'DIS').
card_image_name('sandstorm eidolon'/'DIS', 'sandstorm eidolon').
card_uid('sandstorm eidolon'/'DIS', 'DIS:Sandstorm Eidolon:sandstorm eidolon').
card_rarity('sandstorm eidolon'/'DIS', 'Common').
card_artist('sandstorm eidolon'/'DIS', 'Brian Hagan').
card_number('sandstorm eidolon'/'DIS', '70').
card_multiverse_id('sandstorm eidolon'/'DIS', '110637').

card_in_set('seal of doom', 'DIS').
card_original_type('seal of doom'/'DIS', 'Enchantment').
card_original_text('seal of doom'/'DIS', 'Sacrifice Seal of Doom: Destroy target nonblack creature. It can\'t be regenerated.').
card_image_name('seal of doom'/'DIS', 'seal of doom').
card_uid('seal of doom'/'DIS', 'DIS:Seal of Doom:seal of doom').
card_rarity('seal of doom'/'DIS', 'Common').
card_artist('seal of doom'/'DIS', 'Ralph Horsley').
card_number('seal of doom'/'DIS', '53').
card_flavor_text('seal of doom'/'DIS', '\"A basilisk\'s gaze is effective, but disposing of physical remains is a tiresome task. It\'s far more discrete to dissolve one\'s victim entirely.\"\n—Szadek').
card_multiverse_id('seal of doom'/'DIS', '107534').

card_in_set('seal of fire', 'DIS').
card_original_type('seal of fire'/'DIS', 'Enchantment').
card_original_text('seal of fire'/'DIS', 'Sacrifice Seal of Fire: Seal of Fire deals 2 damage to target creature or player.').
card_image_name('seal of fire'/'DIS', 'seal of fire').
card_uid('seal of fire'/'DIS', 'DIS:Seal of Fire:seal of fire').
card_rarity('seal of fire'/'DIS', 'Common').
card_artist('seal of fire'/'DIS', 'Ralph Horsley').
card_number('seal of fire'/'DIS', '71').
card_flavor_text('seal of fire'/'DIS', '\"If it breathes, it will burn.\"\n—Rakdos pyromancers\' creed').
card_multiverse_id('seal of fire'/'DIS', '107531').

card_in_set('seek', 'DIS').
card_original_type('seek'/'DIS', 'Instant').
card_original_text('seek'/'DIS', 'Put target artifact or enchantment on the bottom of its owner\'s library.\n//\nSeek\n{W}{B}\nInstant\nSearch target opponent\'s library for a card and remove that card from the game. You gain life equal to its converted mana cost. Then that player shuffles his or her library.').
card_first_print('seek', 'DIS').
card_image_name('seek'/'DIS', 'hideseek').
card_uid('seek'/'DIS', 'DIS:Seek:hideseek').
card_rarity('seek'/'DIS', 'Rare').
card_artist('seek'/'DIS', 'Zoltan Boros & Gabor Szikszai').
card_number('seek'/'DIS', '151b').
card_multiverse_id('seek'/'DIS', '107315').
card_watermark('seek'/'DIS', 'Orzhov').

card_in_set('shielding plax', 'DIS').
card_original_type('shielding plax'/'DIS', 'Enchantment — Aura').
card_original_text('shielding plax'/'DIS', '({G/U} can be paid with either {G} or {U}.)\nEnchant creature\nWhen Shielding Plax comes into play, draw a card.\nEnchanted creature can\'t be the target of spells or abilities your opponents control.').
card_first_print('shielding plax', 'DIS').
card_image_name('shielding plax'/'DIS', 'shielding plax').
card_uid('shielding plax'/'DIS', 'DIS:Shielding Plax:shielding plax').
card_rarity('shielding plax'/'DIS', 'Common').
card_artist('shielding plax'/'DIS', 'Brian Hagan').
card_number('shielding plax'/'DIS', '147').
card_multiverse_id('shielding plax'/'DIS', '97084').
card_watermark('shielding plax'/'DIS', 'Simic').

card_in_set('silkwing scout', 'DIS').
card_original_type('silkwing scout'/'DIS', 'Creature — Faerie Scout').
card_original_text('silkwing scout'/'DIS', 'Flying\n{G}, Sacrifice Silkwing Scout: Search your library for a basic land card and put that card into play tapped. Then shuffle your library.').
card_first_print('silkwing scout', 'DIS').
card_image_name('silkwing scout'/'DIS', 'silkwing scout').
card_uid('silkwing scout'/'DIS', 'DIS:Silkwing Scout:silkwing scout').
card_rarity('silkwing scout'/'DIS', 'Common').
card_artist('silkwing scout'/'DIS', 'Rebecca Guay').
card_number('silkwing scout'/'DIS', '31').
card_multiverse_id('silkwing scout'/'DIS', '107313').
card_watermark('silkwing scout'/'DIS', 'Simic').

card_in_set('simic basilisk', 'DIS').
card_original_type('simic basilisk'/'DIS', 'Creature — Basilisk Mutant').
card_original_text('simic basilisk'/'DIS', 'Graft 3 (This creature comes into play with three +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{1}{G}: Until end of turn, target creature with a +1/+1 counter on it gains \"Whenever this creature deals combat damage to a creature, destroy that creature at end of combat.\"').
card_first_print('simic basilisk', 'DIS').
card_image_name('simic basilisk'/'DIS', 'simic basilisk').
card_uid('simic basilisk'/'DIS', 'DIS:Simic Basilisk:simic basilisk').
card_rarity('simic basilisk'/'DIS', 'Uncommon').
card_artist('simic basilisk'/'DIS', 'Luca Zontini').
card_number('simic basilisk'/'DIS', '91').
card_multiverse_id('simic basilisk'/'DIS', '107347').
card_watermark('simic basilisk'/'DIS', 'Simic').

card_in_set('simic growth chamber', 'DIS').
card_original_type('simic growth chamber'/'DIS', 'Land').
card_original_text('simic growth chamber'/'DIS', 'Simic Growth Chamber comes into play tapped.\nWhen Simic Growth Chamber comes into play, return a land you control to its owner\'s hand.\n{T}: Add {G}{U} to your mana pool.').
card_first_print('simic growth chamber', 'DIS').
card_image_name('simic growth chamber'/'DIS', 'simic growth chamber').
card_uid('simic growth chamber'/'DIS', 'DIS:Simic Growth Chamber:simic growth chamber').
card_rarity('simic growth chamber'/'DIS', 'Common').
card_artist('simic growth chamber'/'DIS', 'John Avon').
card_number('simic growth chamber'/'DIS', '180').
card_multiverse_id('simic growth chamber'/'DIS', '97089').
card_watermark('simic growth chamber'/'DIS', 'Simic').

card_in_set('simic guildmage', 'DIS').
card_original_type('simic guildmage'/'DIS', 'Creature — Elf Wizard').
card_original_text('simic guildmage'/'DIS', '({G/U} can be paid with either {G} or {U}.)\n{1}{G}: Move a +1/+1 counter from target creature onto another target creature with the same controller.\n{1}{U}: Attach target Aura enchanting a permanent to another permanent with the same controller.').
card_first_print('simic guildmage', 'DIS').
card_image_name('simic guildmage'/'DIS', 'simic guildmage').
card_uid('simic guildmage'/'DIS', 'DIS:Simic Guildmage:simic guildmage').
card_rarity('simic guildmage'/'DIS', 'Uncommon').
card_artist('simic guildmage'/'DIS', 'Aleksi Briclot').
card_number('simic guildmage'/'DIS', '148').
card_multiverse_id('simic guildmage'/'DIS', '97078').
card_watermark('simic guildmage'/'DIS', 'Simic').

card_in_set('simic initiate', 'DIS').
card_original_type('simic initiate'/'DIS', 'Creature — Human Mutant').
card_original_text('simic initiate'/'DIS', 'Graft 1 (This creature comes into play with a +1/+1 counter on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)').
card_first_print('simic initiate', 'DIS').
card_image_name('simic initiate'/'DIS', 'simic initiate').
card_uid('simic initiate'/'DIS', 'DIS:Simic Initiate:simic initiate').
card_rarity('simic initiate'/'DIS', 'Common').
card_artist('simic initiate'/'DIS', 'Dany Orizio').
card_number('simic initiate'/'DIS', '92').
card_flavor_text('simic initiate'/'DIS', 'Simic initiates begin their training as experimental subjects. Failures are flushed to the undersewers.').
card_multiverse_id('simic initiate'/'DIS', '107441').
card_watermark('simic initiate'/'DIS', 'Simic').

card_in_set('simic ragworm', 'DIS').
card_original_type('simic ragworm'/'DIS', 'Creature — Worm').
card_original_text('simic ragworm'/'DIS', '{U}: Untap Simic Ragworm.').
card_first_print('simic ragworm', 'DIS').
card_image_name('simic ragworm'/'DIS', 'simic ragworm').
card_uid('simic ragworm'/'DIS', 'DIS:Simic Ragworm:simic ragworm').
card_rarity('simic ragworm'/'DIS', 'Common').
card_artist('simic ragworm'/'DIS', 'Nick Percival').
card_number('simic ragworm'/'DIS', '93').
card_flavor_text('simic ragworm'/'DIS', '\"Infused with autochthon blood for size and hellion cells for speed, the ragworm is perfect for clearing the undergardens of both rats and vagrants.\"\n—Yolov, Simic bioengineer').
card_multiverse_id('simic ragworm'/'DIS', '107565').
card_watermark('simic ragworm'/'DIS', 'Simic').

card_in_set('simic signet', 'DIS').
card_original_type('simic signet'/'DIS', 'Artifact').
card_original_text('simic signet'/'DIS', '{1}, {T}: Add {G}{U} to your mana pool.').
card_first_print('simic signet', 'DIS').
card_image_name('simic signet'/'DIS', 'simic signet').
card_uid('simic signet'/'DIS', 'DIS:Simic Signet:simic signet').
card_rarity('simic signet'/'DIS', 'Common').
card_artist('simic signet'/'DIS', 'Greg Hildebrandt').
card_number('simic signet'/'DIS', '166').
card_flavor_text('simic signet'/'DIS', 'For the Simic Combine, its sigil serves not as an emblem of honor but as a trademark. Its familiar image on any biological commodity attests to superb craftsmanship, ingenious innovation, and higher cost.').
card_multiverse_id('simic signet'/'DIS', '97079').
card_watermark('simic signet'/'DIS', 'Simic').

card_in_set('simic sky swallower', 'DIS').
card_original_type('simic sky swallower'/'DIS', 'Creature — Leviathan').
card_original_text('simic sky swallower'/'DIS', 'Flying, trample\nSimic Sky Swallower can\'t be the target of spells or abilities.').
card_first_print('simic sky swallower', 'DIS').
card_image_name('simic sky swallower'/'DIS', 'simic sky swallower').
card_uid('simic sky swallower'/'DIS', 'DIS:Simic Sky Swallower:simic sky swallower').
card_rarity('simic sky swallower'/'DIS', 'Rare').
card_artist('simic sky swallower'/'DIS', 'rk post').
card_number('simic sky swallower'/'DIS', '130').
card_flavor_text('simic sky swallower'/'DIS', '\"We\'ve bred out the shortcomings of the species\' natural form and replaced them with assets of our own design.\"\n—Momir Vig').
card_multiverse_id('simic sky swallower'/'DIS', '111204').
card_watermark('simic sky swallower'/'DIS', 'Simic').

card_in_set('simple', 'DIS').
card_original_type('simple'/'DIS', 'Sorcery').
card_original_text('simple'/'DIS', 'Destroy target multicolored permanent.\n//\nSimple\n{1}{G}{W}\nSorcery\nDestroy all Auras and Equipment.').
card_first_print('simple', 'DIS').
card_image_name('simple'/'DIS', 'puresimple').
card_uid('simple'/'DIS', 'DIS:Simple:puresimple').
card_rarity('simple'/'DIS', 'Uncommon').
card_artist('simple'/'DIS', 'Paolo Parente').
card_number('simple'/'DIS', '154b').
card_multiverse_id('simple'/'DIS', '107532').
card_watermark('simple'/'DIS', 'Selesnya').

card_in_set('skullmead cauldron', 'DIS').
card_original_type('skullmead cauldron'/'DIS', 'Artifact').
card_original_text('skullmead cauldron'/'DIS', '{T}: You gain 1 life.\n{T}, Discard a card: You gain 3 life.').
card_first_print('skullmead cauldron', 'DIS').
card_image_name('skullmead cauldron'/'DIS', 'skullmead cauldron').
card_uid('skullmead cauldron'/'DIS', 'DIS:Skullmead Cauldron:skullmead cauldron').
card_rarity('skullmead cauldron'/'DIS', 'Uncommon').
card_artist('skullmead cauldron'/'DIS', 'Martina Pilcerova').
card_number('skullmead cauldron'/'DIS', '167').
card_flavor_text('skullmead cauldron'/'DIS', 'Once tasted, the flavor of skullmead lingers in the throat, a secret brand of guilt for those who partake in forbidden pleasures.').
card_multiverse_id('skullmead cauldron'/'DIS', '107326').

card_in_set('sky hussar', 'DIS').
card_original_type('sky hussar'/'DIS', 'Creature — Human Knight').
card_original_text('sky hussar'/'DIS', 'Flying\nWhen Sky Hussar comes into play, untap all creatures you control.\nForecast Tap two untapped white and/or blue creatures you control, Reveal Sky Hussar from your hand: Draw a card. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('sky hussar', 'DIS').
card_image_name('sky hussar'/'DIS', 'sky hussar').
card_uid('sky hussar'/'DIS', 'DIS:Sky Hussar:sky hussar').
card_rarity('sky hussar'/'DIS', 'Uncommon').
card_artist('sky hussar'/'DIS', 'Kev Walker').
card_number('sky hussar'/'DIS', '131').
card_multiverse_id('sky hussar'/'DIS', '97109').
card_watermark('sky hussar'/'DIS', 'Azorius').

card_in_set('skyscribing', 'DIS').
card_original_type('skyscribing'/'DIS', 'Sorcery').
card_original_text('skyscribing'/'DIS', 'Each player draws X cards.\nForecast {2}{U}, Reveal Skyscribing from your hand: Each player draws a card. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('skyscribing', 'DIS').
card_image_name('skyscribing'/'DIS', 'skyscribing').
card_uid('skyscribing'/'DIS', 'DIS:Skyscribing:skyscribing').
card_rarity('skyscribing'/'DIS', 'Uncommon').
card_artist('skyscribing'/'DIS', 'Luca Zontini').
card_number('skyscribing'/'DIS', '32').
card_multiverse_id('skyscribing'/'DIS', '107449').
card_watermark('skyscribing'/'DIS', 'Azorius').

card_in_set('slaughterhouse bouncer', 'DIS').
card_original_type('slaughterhouse bouncer'/'DIS', 'Creature — Ogre Warrior').
card_original_text('slaughterhouse bouncer'/'DIS', 'Hellbent When Slaughterhouse Bouncer is put into a graveyard from play, if you have no cards in hand, target creature gets -3/-3 until end of turn.').
card_first_print('slaughterhouse bouncer', 'DIS').
card_image_name('slaughterhouse bouncer'/'DIS', 'slaughterhouse bouncer').
card_uid('slaughterhouse bouncer'/'DIS', 'DIS:Slaughterhouse Bouncer:slaughterhouse bouncer').
card_rarity('slaughterhouse bouncer'/'DIS', 'Common').
card_artist('slaughterhouse bouncer'/'DIS', 'Greg Staples').
card_number('slaughterhouse bouncer'/'DIS', '54').
card_flavor_text('slaughterhouse bouncer'/'DIS', 'A Rakdos party is a flop if anyone lives to talk about it.').
card_multiverse_id('slaughterhouse bouncer'/'DIS', '97073').
card_watermark('slaughterhouse bouncer'/'DIS', 'Rakdos').

card_in_set('slithering shade', 'DIS').
card_original_type('slithering shade'/'DIS', 'Creature — Shade').
card_original_text('slithering shade'/'DIS', 'Defender (This creature can\'t attack.)\n{B}: Slithering Shade gets +1/+1 until end of turn.\nHellbent Slithering Shade can attack as though it didn\'t have defender as long as you have no cards in hand.').
card_first_print('slithering shade', 'DIS').
card_image_name('slithering shade'/'DIS', 'slithering shade').
card_uid('slithering shade'/'DIS', 'DIS:Slithering Shade:slithering shade').
card_rarity('slithering shade'/'DIS', 'Uncommon').
card_artist('slithering shade'/'DIS', 'Daren Bader').
card_number('slithering shade'/'DIS', '55').
card_multiverse_id('slithering shade'/'DIS', '111283').
card_watermark('slithering shade'/'DIS', 'Rakdos').

card_in_set('soulsworn jury', 'DIS').
card_original_type('soulsworn jury'/'DIS', 'Creature — Spirit').
card_original_text('soulsworn jury'/'DIS', 'Defender (This creature can\'t attack.)\n{1}{U}, Sacrifice Soulsworn Jury: Counter target creature spell.').
card_first_print('soulsworn jury', 'DIS').
card_image_name('soulsworn jury'/'DIS', 'soulsworn jury').
card_uid('soulsworn jury'/'DIS', 'DIS:Soulsworn Jury:soulsworn jury').
card_rarity('soulsworn jury'/'DIS', 'Common').
card_artist('soulsworn jury'/'DIS', 'Zoltan Boros & Gabor Szikszai').
card_number('soulsworn jury'/'DIS', '17').
card_flavor_text('soulsworn jury'/'DIS', 'In death, as in life, they protect the Grand Arbiter from exposure to contrary points of view.').
card_multiverse_id('soulsworn jury'/'DIS', '107440').
card_watermark('soulsworn jury'/'DIS', 'Azorius').

card_in_set('spell snare', 'DIS').
card_original_type('spell snare'/'DIS', 'Instant').
card_original_text('spell snare'/'DIS', 'Counter target spell with converted mana cost 2.').
card_first_print('spell snare', 'DIS').
card_image_name('spell snare'/'DIS', 'spell snare').
card_uid('spell snare'/'DIS', 'DIS:Spell Snare:spell snare').
card_rarity('spell snare'/'DIS', 'Uncommon').
card_artist('spell snare'/'DIS', 'Hideaki Takamura').
card_number('spell snare'/'DIS', '33').
card_flavor_text('spell snare'/'DIS', 'Every culture has its unlucky numbers. In a city where you\'re either alone, in a crowd, or being stabbed in the back, two is the worst number of all.').
card_multiverse_id('spell snare'/'DIS', '107281').

card_in_set('sporeback troll', 'DIS').
card_original_type('sporeback troll'/'DIS', 'Creature — Troll Mutant').
card_original_text('sporeback troll'/'DIS', 'Graft 2 (This creature comes into play with two +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{1}{G}: Regenerate target creature with a +1/+1 counter on it.').
card_first_print('sporeback troll', 'DIS').
card_image_name('sporeback troll'/'DIS', 'sporeback troll').
card_uid('sporeback troll'/'DIS', 'DIS:Sporeback Troll:sporeback troll').
card_rarity('sporeback troll'/'DIS', 'Common').
card_artist('sporeback troll'/'DIS', 'Dan Scott').
card_number('sporeback troll'/'DIS', '94').
card_multiverse_id('sporeback troll'/'DIS', '107263').
card_watermark('sporeback troll'/'DIS', 'Simic').

card_in_set('sprouting phytohydra', 'DIS').
card_original_type('sprouting phytohydra'/'DIS', 'Creature — Plant Hydra').
card_original_text('sprouting phytohydra'/'DIS', 'Defender (This creature can\'t attack.)\nWhenever Sprouting Phytohydra is dealt damage, you may put a token into play that\'s a copy of Sprouting Phytohydra.').
card_first_print('sprouting phytohydra', 'DIS').
card_image_name('sprouting phytohydra'/'DIS', 'sprouting phytohydra').
card_uid('sprouting phytohydra'/'DIS', 'DIS:Sprouting Phytohydra:sprouting phytohydra').
card_rarity('sprouting phytohydra'/'DIS', 'Rare').
card_artist('sprouting phytohydra'/'DIS', 'Heather Hudson').
card_number('sprouting phytohydra'/'DIS', '95').
card_flavor_text('sprouting phytohydra'/'DIS', 'A hydra tender\'s problem isn\'t having enough defenses but enough pots.').
card_multiverse_id('sprouting phytohydra'/'DIS', '111220').

card_in_set('squealing devil', 'DIS').
card_original_type('squealing devil'/'DIS', 'Creature — Devil').
card_original_text('squealing devil'/'DIS', 'Fear\nWhen Squealing Devil comes into play, you may pay {X}. If you do, target creature gets +X/+0 until end of turn.\nWhen Squealing Devil comes into play, sacrifice it unless {B} was spent to play it.').
card_first_print('squealing devil', 'DIS').
card_image_name('squealing devil'/'DIS', 'squealing devil').
card_uid('squealing devil'/'DIS', 'DIS:Squealing Devil:squealing devil').
card_rarity('squealing devil'/'DIS', 'Uncommon').
card_artist('squealing devil'/'DIS', 'Matt Cavotta').
card_number('squealing devil'/'DIS', '72').
card_multiverse_id('squealing devil'/'DIS', '107303').
card_watermark('squealing devil'/'DIS', 'Rakdos').

card_in_set('stalking vengeance', 'DIS').
card_original_type('stalking vengeance'/'DIS', 'Creature — Avatar').
card_original_text('stalking vengeance'/'DIS', 'Haste\nWhenever another creature you control is put into a graveyard from play, it deals damage equal to its power to target player.').
card_first_print('stalking vengeance', 'DIS').
card_image_name('stalking vengeance'/'DIS', 'stalking vengeance').
card_uid('stalking vengeance'/'DIS', 'DIS:Stalking Vengeance:stalking vengeance').
card_rarity('stalking vengeance'/'DIS', 'Rare').
card_artist('stalking vengeance'/'DIS', 'Anthony S. Waters').
card_number('stalking vengeance'/'DIS', '73').
card_flavor_text('stalking vengeance'/'DIS', 'Something roams the killing places, sniffing the guilt of the slayers, stalking them on iron paws.').
card_multiverse_id('stalking vengeance'/'DIS', '107301').

card_in_set('steeling stance', 'DIS').
card_original_type('steeling stance'/'DIS', 'Instant').
card_original_text('steeling stance'/'DIS', 'Creatures you control get +1/+1 until end of turn.\nForecast {W}, Reveal Steeling Stance from your hand: Target creature gets +1/+1 until end of turn. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('steeling stance', 'DIS').
card_image_name('steeling stance'/'DIS', 'steeling stance').
card_uid('steeling stance'/'DIS', 'DIS:Steeling Stance:steeling stance').
card_rarity('steeling stance'/'DIS', 'Common').
card_artist('steeling stance'/'DIS', 'Randy Gallegos').
card_number('steeling stance'/'DIS', '18').
card_multiverse_id('steeling stance'/'DIS', '97115').
card_watermark('steeling stance'/'DIS', 'Azorius').

card_in_set('stoic ephemera', 'DIS').
card_original_type('stoic ephemera'/'DIS', 'Creature — Spirit').
card_original_text('stoic ephemera'/'DIS', 'Defender (This creature can\'t attack.)\nFlying\nWhen Stoic Ephemera blocks, sacrifice it at end of combat.').
card_first_print('stoic ephemera', 'DIS').
card_image_name('stoic ephemera'/'DIS', 'stoic ephemera').
card_uid('stoic ephemera'/'DIS', 'DIS:Stoic Ephemera:stoic ephemera').
card_rarity('stoic ephemera'/'DIS', 'Uncommon').
card_artist('stoic ephemera'/'DIS', 'Randy Gallegos').
card_number('stoic ephemera'/'DIS', '19').
card_flavor_text('stoic ephemera'/'DIS', '\"Life is fleeting, and so is death. Only the oath is eternal.\"').
card_multiverse_id('stoic ephemera'/'DIS', '107527').

card_in_set('stomp and howl', 'DIS').
card_original_type('stomp and howl'/'DIS', 'Sorcery').
card_original_text('stomp and howl'/'DIS', 'Destroy target artifact and target enchantment.').
card_first_print('stomp and howl', 'DIS').
card_image_name('stomp and howl'/'DIS', 'stomp and howl').
card_uid('stomp and howl'/'DIS', 'DIS:Stomp and Howl:stomp and howl').
card_rarity('stomp and howl'/'DIS', 'Uncommon').
card_artist('stomp and howl'/'DIS', 'Carl Critchlow').
card_number('stomp and howl'/'DIS', '96').
card_flavor_text('stomp and howl'/'DIS', '\"Mizzium may be impervious to fire, but obviously certain other things can . . . modify its form.\"\n—Ivos Koba, indrik handler').
card_multiverse_id('stomp and howl'/'DIS', '107505').

card_in_set('stormscale anarch', 'DIS').
card_original_type('stormscale anarch'/'DIS', 'Creature — Viashino Shaman').
card_original_text('stormscale anarch'/'DIS', '{2}{R}, Discard a card at random: Stormscale Anarch deals 2 damage to target creature or player. If the discarded card was multicolored, Stormscale Anarch deals 4 damage to that creature or player instead.').
card_first_print('stormscale anarch', 'DIS').
card_image_name('stormscale anarch'/'DIS', 'stormscale anarch').
card_uid('stormscale anarch'/'DIS', 'DIS:Stormscale Anarch:stormscale anarch').
card_rarity('stormscale anarch'/'DIS', 'Rare').
card_artist('stormscale anarch'/'DIS', 'Ralph Horsley').
card_number('stormscale anarch'/'DIS', '74').
card_multiverse_id('stormscale anarch'/'DIS', '107300').

card_in_set('street savvy', 'DIS').
card_original_type('street savvy'/'DIS', 'Enchantment — Aura').
card_original_text('street savvy'/'DIS', 'Enchant creature\nEnchanted creature gets +0/+2 and can block creatures with landwalk abilities as though they didn\'t have those abilities.').
card_first_print('street savvy', 'DIS').
card_image_name('street savvy'/'DIS', 'street savvy').
card_uid('street savvy'/'DIS', 'DIS:Street Savvy:street savvy').
card_rarity('street savvy'/'DIS', 'Common').
card_artist('street savvy'/'DIS', 'Kev Walker').
card_number('street savvy'/'DIS', '97').
card_flavor_text('street savvy'/'DIS', '\"It takes more than swift feet and dark shadows to slip by me unnoticed.\"').
card_multiverse_id('street savvy'/'DIS', '107371').

card_in_set('supply', 'DIS').
card_original_type('supply'/'DIS', 'Sorcery').
card_original_text('supply'/'DIS', 'Put X 1/1 green Saproling creature tokens into play.\n//\nDemand\n{1}{W}{U}\nSorcery\nSearch your library for a multicolored card, reveal it, and put it into your hand. Then shuffle your library.').
card_first_print('supply', 'DIS').
card_image_name('supply'/'DIS', 'supplydemand').
card_uid('supply'/'DIS', 'DIS:Supply:supplydemand').
card_rarity('supply'/'DIS', 'Uncommon').
card_artist('supply'/'DIS', 'Daren Bader').
card_number('supply'/'DIS', '157a').
card_multiverse_id('supply'/'DIS', '107464').
card_watermark('supply'/'DIS', 'Selesnya').

card_in_set('swift silence', 'DIS').
card_original_type('swift silence'/'DIS', 'Instant').
card_original_text('swift silence'/'DIS', 'Counter all other spells. Draw a card for each spell countered this way.').
card_first_print('swift silence', 'DIS').
card_image_name('swift silence'/'DIS', 'swift silence').
card_uid('swift silence'/'DIS', 'DIS:Swift Silence:swift silence').
card_rarity('swift silence'/'DIS', 'Rare').
card_artist('swift silence'/'DIS', 'Greg Staples').
card_number('swift silence'/'DIS', '132').
card_flavor_text('swift silence'/'DIS', '\"This world may not know peace, but in my presence you will know quiet.\"\n—Augustin IV').
card_multiverse_id('swift silence'/'DIS', '107284').
card_watermark('swift silence'/'DIS', 'Azorius').

card_in_set('taste for mayhem', 'DIS').
card_original_type('taste for mayhem'/'DIS', 'Enchantment — Aura').
card_original_text('taste for mayhem'/'DIS', 'Enchant creature\nEnchanted creature gets +2/+0.\nHellbent Enchanted creature gets an additional +2/+0 as long as you have no cards in hand.').
card_first_print('taste for mayhem', 'DIS').
card_image_name('taste for mayhem'/'DIS', 'taste for mayhem').
card_uid('taste for mayhem'/'DIS', 'DIS:Taste for Mayhem:taste for mayhem').
card_rarity('taste for mayhem'/'DIS', 'Common').
card_artist('taste for mayhem'/'DIS', 'Greg Hildebrandt').
card_number('taste for mayhem'/'DIS', '75').
card_flavor_text('taste for mayhem'/'DIS', 'The taste of blood breaks down what little self-control the Rakdos possess.').
card_multiverse_id('taste for mayhem'/'DIS', '107597').
card_watermark('taste for mayhem'/'DIS', 'Rakdos').

card_in_set('thrive', 'DIS').
card_original_type('thrive'/'DIS', 'Sorcery').
card_original_text('thrive'/'DIS', 'Put a +1/+1 counter on each of X target creatures.').
card_image_name('thrive'/'DIS', 'thrive').
card_uid('thrive'/'DIS', 'DIS:Thrive:thrive').
card_rarity('thrive'/'DIS', 'Common').
card_artist('thrive'/'DIS', 'Daren Bader').
card_number('thrive'/'DIS', '98').
card_flavor_text('thrive'/'DIS', 'The most successful of Simic creations is cytoplasm, a living, symbiotic substance that feeds off genetic rhythms and strengthens its host in return.').
card_multiverse_id('thrive'/'DIS', '107390').

card_in_set('tidespout tyrant', 'DIS').
card_original_type('tidespout tyrant'/'DIS', 'Creature — Djinn').
card_original_text('tidespout tyrant'/'DIS', 'Flying\nWhenever you play a spell, return target permanent to its owner\'s hand.').
card_first_print('tidespout tyrant', 'DIS').
card_image_name('tidespout tyrant'/'DIS', 'tidespout tyrant').
card_uid('tidespout tyrant'/'DIS', 'DIS:Tidespout Tyrant:tidespout tyrant').
card_rarity('tidespout tyrant'/'DIS', 'Rare').
card_artist('tidespout tyrant'/'DIS', 'Dany Orizio').
card_number('tidespout tyrant'/'DIS', '34').
card_flavor_text('tidespout tyrant'/'DIS', 'He unmakes the world in his own image.').
card_multiverse_id('tidespout tyrant'/'DIS', '107408').

card_in_set('transguild courier', 'DIS').
card_original_type('transguild courier'/'DIS', 'Artifact Creature — Golem').
card_original_text('transguild courier'/'DIS', 'Transguild Courier is all colors (even if this card isn\'t in play).').
card_first_print('transguild courier', 'DIS').
card_image_name('transguild courier'/'DIS', 'transguild courier').
card_uid('transguild courier'/'DIS', 'DIS:Transguild Courier:transguild courier').
card_rarity('transguild courier'/'DIS', 'Uncommon').
card_artist('transguild courier'/'DIS', 'John Avon').
card_number('transguild courier'/'DIS', '168').
card_flavor_text('transguild courier'/'DIS', 'Reluctant to meet face to face, the leaders of the ten guilds prefer to do official business through a go-between immune to bribes and threats.').
card_multiverse_id('transguild courier'/'DIS', '107362').

card_in_set('trial', 'DIS').
card_original_type('trial'/'DIS', 'Instant').
card_original_text('trial'/'DIS', 'Return all creatures blocking or blocked by target creature to their owner\'s hand.\n//\nError\n{U}{B}\nInstant\nCounter target multicolored spell.').
card_first_print('trial', 'DIS').
card_image_name('trial'/'DIS', 'trialerror').
card_uid('trial'/'DIS', 'DIS:Trial:trialerror').
card_rarity('trial'/'DIS', 'Uncommon').
card_artist('trial'/'DIS', 'Ron Spears & Wayne Reynolds').
card_number('trial'/'DIS', '158a').
card_multiverse_id('trial'/'DIS', '107259').
card_watermark('trial'/'DIS', 'Azorius').

card_in_set('trygon predator', 'DIS').
card_original_type('trygon predator'/'DIS', 'Creature — Beast').
card_original_text('trygon predator'/'DIS', 'Flying\nWhenever Trygon Predator deals combat damage to a player, you may destroy target artifact or enchantment that player controls.').
card_first_print('trygon predator', 'DIS').
card_image_name('trygon predator'/'DIS', 'trygon predator').
card_uid('trygon predator'/'DIS', 'DIS:Trygon Predator:trygon predator').
card_rarity('trygon predator'/'DIS', 'Uncommon').
card_artist('trygon predator'/'DIS', 'Carl Critchlow').
card_number('trygon predator'/'DIS', '133').
card_flavor_text('trygon predator'/'DIS', 'Held aloft by metabolized magic, trygons are ravenous for sources of mystic fuel.').
card_multiverse_id('trygon predator'/'DIS', '97112').
card_watermark('trygon predator'/'DIS', 'Simic').

card_in_set('twinstrike', 'DIS').
card_original_type('twinstrike'/'DIS', 'Instant').
card_original_text('twinstrike'/'DIS', 'Twinstrike deals 2 damage to each of two target creatures.\nHellbent Destroy those creatures instead if you have no cards in hand.').
card_first_print('twinstrike', 'DIS').
card_image_name('twinstrike'/'DIS', 'twinstrike').
card_uid('twinstrike'/'DIS', 'DIS:Twinstrike:twinstrike').
card_rarity('twinstrike'/'DIS', 'Uncommon').
card_artist('twinstrike'/'DIS', 'Christopher Rush').
card_number('twinstrike'/'DIS', '134').
card_flavor_text('twinstrike'/'DIS', 'They\'re dying left and right.').
card_multiverse_id('twinstrike'/'DIS', '97114').
card_watermark('twinstrike'/'DIS', 'Rakdos').

card_in_set('unliving psychopath', 'DIS').
card_original_type('unliving psychopath'/'DIS', 'Creature — Zombie Assassin').
card_original_text('unliving psychopath'/'DIS', '{B}: Unliving Psychopath gets +1/-1 until end of turn.\n{B}, {T}: Destroy target creature with power less than Unliving Psychopath\'s power.').
card_first_print('unliving psychopath', 'DIS').
card_image_name('unliving psychopath'/'DIS', 'unliving psychopath').
card_uid('unliving psychopath'/'DIS', 'DIS:Unliving Psychopath:unliving psychopath').
card_rarity('unliving psychopath'/'DIS', 'Rare').
card_artist('unliving psychopath'/'DIS', 'Greg Staples').
card_number('unliving psychopath'/'DIS', '56').
card_flavor_text('unliving psychopath'/'DIS', '\"The victim bears the marks of the ‘Ktozok Impaler\'. . . but he was executed years ago!\"\n—Gorev Hadszak, Wojek investigator').
card_multiverse_id('unliving psychopath'/'DIS', '107451').

card_in_set('utopia sprawl', 'DIS').
card_original_type('utopia sprawl'/'DIS', 'Enchantment — Aura').
card_original_text('utopia sprawl'/'DIS', 'Enchant Forest\nAs Utopia Sprawl comes into play, choose a color.\nWhenever enchanted Forest is tapped for mana, its controller adds one mana of the chosen color to his or her mana pool.').
card_first_print('utopia sprawl', 'DIS').
card_image_name('utopia sprawl'/'DIS', 'utopia sprawl').
card_uid('utopia sprawl'/'DIS', 'DIS:Utopia Sprawl:utopia sprawl').
card_rarity('utopia sprawl'/'DIS', 'Common').
card_artist('utopia sprawl'/'DIS', 'Ron Spears').
card_number('utopia sprawl'/'DIS', '99').
card_multiverse_id('utopia sprawl'/'DIS', '107569').

card_in_set('utvara scalper', 'DIS').
card_original_type('utvara scalper'/'DIS', 'Creature — Goblin Scout').
card_original_text('utvara scalper'/'DIS', 'Flying\nUtvara Scalper attacks each turn if able.').
card_first_print('utvara scalper', 'DIS').
card_image_name('utvara scalper'/'DIS', 'utvara scalper').
card_uid('utvara scalper'/'DIS', 'DIS:Utvara Scalper:utvara scalper').
card_rarity('utvara scalper'/'DIS', 'Common').
card_artist('utvara scalper'/'DIS', 'Christopher Rush').
card_number('utvara scalper'/'DIS', '76').
card_flavor_text('utvara scalper'/'DIS', 'He wages war wherever the wind carries him.').
card_multiverse_id('utvara scalper'/'DIS', '107276').

card_in_set('valor made real', 'DIS').
card_original_type('valor made real'/'DIS', 'Instant').
card_original_text('valor made real'/'DIS', 'Target creature can block any number of creatures this turn.').
card_first_print('valor made real', 'DIS').
card_image_name('valor made real'/'DIS', 'valor made real').
card_uid('valor made real'/'DIS', 'DIS:Valor Made Real:valor made real').
card_rarity('valor made real'/'DIS', 'Common').
card_artist('valor made real'/'DIS', 'Jeff Miracola').
card_number('valor made real'/'DIS', '20').
card_flavor_text('valor made real'/'DIS', '\"As my father taught, ‘Training will raise your shield to the blow, but courage fills the gaps the shield leaves open.\'\"').
card_multiverse_id('valor made real'/'DIS', '107431').

card_in_set('verdant eidolon', 'DIS').
card_original_type('verdant eidolon'/'DIS', 'Creature — Spirit').
card_original_text('verdant eidolon'/'DIS', '{G}, Sacrifice Verdant Eidolon: Add three mana of any one color to your mana pool.\nWhenever you play a multicolored spell, you may return Verdant Eidolon from your graveyard to your hand.').
card_first_print('verdant eidolon', 'DIS').
card_image_name('verdant eidolon'/'DIS', 'verdant eidolon').
card_uid('verdant eidolon'/'DIS', 'DIS:Verdant Eidolon:verdant eidolon').
card_rarity('verdant eidolon'/'DIS', 'Common').
card_artist('verdant eidolon'/'DIS', 'Tsutomu Kawade').
card_number('verdant eidolon'/'DIS', '100').
card_multiverse_id('verdant eidolon'/'DIS', '110638').

card_in_set('vesper ghoul', 'DIS').
card_original_type('vesper ghoul'/'DIS', 'Creature — Zombie Druid').
card_original_text('vesper ghoul'/'DIS', '{T}, Pay 1 life: Add one mana of any color to your mana pool.').
card_first_print('vesper ghoul', 'DIS').
card_image_name('vesper ghoul'/'DIS', 'vesper ghoul').
card_uid('vesper ghoul'/'DIS', 'DIS:Vesper Ghoul:vesper ghoul').
card_rarity('vesper ghoul'/'DIS', 'Common').
card_artist('vesper ghoul'/'DIS', 'Thomas M. Baxa').
card_number('vesper ghoul'/'DIS', '57').
card_flavor_text('vesper ghoul'/'DIS', '\"Nature\'s exceptions remind us not to think in absolutes. Just as the orchid blossoms on blackened stone, so does the gift of mana occasionally manifest in the undead.\"\n—Bougrat, druid of the Cult of Yore').
card_multiverse_id('vesper ghoul'/'DIS', '107477').

card_in_set('vigean graftmage', 'DIS').
card_original_type('vigean graftmage'/'DIS', 'Creature — Vedalken Wizard Mutant').
card_original_text('vigean graftmage'/'DIS', 'Graft 2 (This creature comes into play with two +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\n{1}{U}: Untap target creature with a +1/+1 counter on it.').
card_first_print('vigean graftmage', 'DIS').
card_image_name('vigean graftmage'/'DIS', 'vigean graftmage').
card_uid('vigean graftmage'/'DIS', 'DIS:Vigean Graftmage:vigean graftmage').
card_rarity('vigean graftmage'/'DIS', 'Uncommon').
card_artist('vigean graftmage'/'DIS', 'Alan Pollack').
card_number('vigean graftmage'/'DIS', '35').
card_multiverse_id('vigean graftmage'/'DIS', '107321').
card_watermark('vigean graftmage'/'DIS', 'Simic').

card_in_set('vigean hydropon', 'DIS').
card_original_type('vigean hydropon'/'DIS', 'Creature — Plant Mutant').
card_original_text('vigean hydropon'/'DIS', 'Graft 5 (This creature comes into play with five +1/+1 counters on it. Whenever another creature comes into play, you may move a +1/+1 counter from this creature onto it.)\nVigean Hydropon can\'t attack or block.').
card_first_print('vigean hydropon', 'DIS').
card_image_name('vigean hydropon'/'DIS', 'vigean hydropon').
card_uid('vigean hydropon'/'DIS', 'DIS:Vigean Hydropon:vigean hydropon').
card_rarity('vigean hydropon'/'DIS', 'Common').
card_artist('vigean hydropon'/'DIS', 'Anthony S. Waters').
card_number('vigean hydropon'/'DIS', '135').
card_flavor_text('vigean hydropon'/'DIS', 'Fruits of magic, roots in science.').
card_multiverse_id('vigean hydropon'/'DIS', '107293').
card_watermark('vigean hydropon'/'DIS', 'Simic').

card_in_set('vigean intuition', 'DIS').
card_original_type('vigean intuition'/'DIS', 'Instant').
card_original_text('vigean intuition'/'DIS', 'Choose a card type, then reveal the top four cards of your library. Put all cards of the chosen type revealed this way into your hand and the rest into your graveyard. (The card types are artifact, creature, enchantment, instant, land, and sorcery.)').
card_first_print('vigean intuition', 'DIS').
card_image_name('vigean intuition'/'DIS', 'vigean intuition').
card_uid('vigean intuition'/'DIS', 'DIS:Vigean Intuition:vigean intuition').
card_rarity('vigean intuition'/'DIS', 'Uncommon').
card_artist('vigean intuition'/'DIS', 'Alex Horley-Orlandelli').
card_number('vigean intuition'/'DIS', '136').
card_multiverse_id('vigean intuition'/'DIS', '97120').
card_watermark('vigean intuition'/'DIS', 'Simic').

card_in_set('vision skeins', 'DIS').
card_original_type('vision skeins'/'DIS', 'Instant').
card_original_text('vision skeins'/'DIS', 'Each player draws two cards.').
card_first_print('vision skeins', 'DIS').
card_image_name('vision skeins'/'DIS', 'vision skeins').
card_uid('vision skeins'/'DIS', 'DIS:Vision Skeins:vision skeins').
card_rarity('vision skeins'/'DIS', 'Common').
card_artist('vision skeins'/'DIS', 'Aleksi Briclot').
card_number('vision skeins'/'DIS', '36').
card_flavor_text('vision skeins'/'DIS', '\"I could see in the other mage\'s eyes that he\'d thought of it too. Then it became a race to exploit the knowledge first.\"').
card_multiverse_id('vision skeins'/'DIS', '107397').

card_in_set('voidslime', 'DIS').
card_original_type('voidslime'/'DIS', 'Instant').
card_original_text('voidslime'/'DIS', 'Counter target spell, activated ability, or triggered ability. (Mana abilities can\'t be targeted.)').
card_image_name('voidslime'/'DIS', 'voidslime').
card_uid('voidslime'/'DIS', 'DIS:Voidslime:voidslime').
card_rarity('voidslime'/'DIS', 'Rare').
card_artist('voidslime'/'DIS', 'Jim Murray').
card_number('voidslime'/'DIS', '137').
card_flavor_text('voidslime'/'DIS', '\"It is technically an ooze, but its lifespan measures only seconds. In that short time, its appetite for magic is extraordinary.\"\n—Simic research notes').
card_multiverse_id('voidslime'/'DIS', '97096').
card_watermark('voidslime'/'DIS', 'Simic').

card_in_set('wakestone gargoyle', 'DIS').
card_original_type('wakestone gargoyle'/'DIS', 'Creature — Gargoyle').
card_original_text('wakestone gargoyle'/'DIS', 'Defender (This creature can\'t attack.)\nFlying\n{1}{W}: Creatures you control with defender can attack this turn as though they didn\'t have defender.').
card_first_print('wakestone gargoyle', 'DIS').
card_image_name('wakestone gargoyle'/'DIS', 'wakestone gargoyle').
card_uid('wakestone gargoyle'/'DIS', 'DIS:Wakestone Gargoyle:wakestone gargoyle').
card_rarity('wakestone gargoyle'/'DIS', 'Rare').
card_artist('wakestone gargoyle'/'DIS', 'Jim Murray').
card_number('wakestone gargoyle'/'DIS', '21').
card_flavor_text('wakestone gargoyle'/'DIS', 'Its pulsating cry shatters bonds of iron, granite, and servitude.').
card_multiverse_id('wakestone gargoyle'/'DIS', '111226').

card_in_set('walking archive', 'DIS').
card_original_type('walking archive'/'DIS', 'Artifact Creature — Golem').
card_original_text('walking archive'/'DIS', 'Defender (This creature can\'t attack.)\nWalking Archive comes into play with a +1/+1 counter on it.\nAt the beginning of each player\'s upkeep, that player draws a card for each +1/+1 counter on Walking Archive.\n{2}{W}{U}: Put a +1/+1 counter on Walking Archive.').
card_first_print('walking archive', 'DIS').
card_image_name('walking archive'/'DIS', 'walking archive').
card_uid('walking archive'/'DIS', 'DIS:Walking Archive:walking archive').
card_rarity('walking archive'/'DIS', 'Rare').
card_artist('walking archive'/'DIS', 'Heather Hudson').
card_number('walking archive'/'DIS', '169').
card_multiverse_id('walking archive'/'DIS', '97092').
card_watermark('walking archive'/'DIS', 'Azorius').

card_in_set('war\'s toll', 'DIS').
card_original_type('war\'s toll'/'DIS', 'Enchantment').
card_original_text('war\'s toll'/'DIS', 'Whenever an opponent taps a land for mana, tap all lands that player controls.\nIf a creature an opponent controls attacks, all creatures that opponent controls attack if able.').
card_first_print('war\'s toll', 'DIS').
card_image_name('war\'s toll'/'DIS', 'war\'s toll').
card_uid('war\'s toll'/'DIS', 'DIS:War\'s Toll:war\'s toll').
card_rarity('war\'s toll'/'DIS', 'Rare').
card_artist('war\'s toll'/'DIS', 'Dana Knutson').
card_number('war\'s toll'/'DIS', '77').
card_flavor_text('war\'s toll'/'DIS', 'Razia, archangel of war, demands total commitment.').
card_multiverse_id('war\'s toll'/'DIS', '107492').

card_in_set('weight of spires', 'DIS').
card_original_type('weight of spires'/'DIS', 'Instant').
card_original_text('weight of spires'/'DIS', 'Weight of Spires deals damage to target creature equal to the number of nonbasic lands that creature\'s controller controls.').
card_first_print('weight of spires', 'DIS').
card_image_name('weight of spires'/'DIS', 'weight of spires').
card_uid('weight of spires'/'DIS', 'DIS:Weight of Spires:weight of spires').
card_rarity('weight of spires'/'DIS', 'Uncommon').
card_artist('weight of spires'/'DIS', 'Michael Sutfin').
card_number('weight of spires'/'DIS', '78').
card_flavor_text('weight of spires'/'DIS', '\"Finally, a good use for an Azorius courthouse.\"\n—Ghut Rak, Gruul guildmage').
card_multiverse_id('weight of spires'/'DIS', '107551').

card_in_set('whiptail moloch', 'DIS').
card_original_type('whiptail moloch'/'DIS', 'Creature — Lizard').
card_original_text('whiptail moloch'/'DIS', 'When Whiptail Moloch comes into play, it deals 3 damage to target creature you control.').
card_first_print('whiptail moloch', 'DIS').
card_image_name('whiptail moloch'/'DIS', 'whiptail moloch').
card_uid('whiptail moloch'/'DIS', 'DIS:Whiptail Moloch:whiptail moloch').
card_rarity('whiptail moloch'/'DIS', 'Common').
card_artist('whiptail moloch'/'DIS', 'Darrell Riche').
card_number('whiptail moloch'/'DIS', '79').
card_flavor_text('whiptail moloch'/'DIS', '\"It wags its tail when it\'s happy. It wags its tail when it\'s angry. Nope, there\'s no good time to be around a whiptail.\"\n—Rugar, Leaguehall Infirmary patient').
card_multiverse_id('whiptail moloch'/'DIS', '97075').

card_in_set('windreaver', 'DIS').
card_original_type('windreaver'/'DIS', 'Creature — Elemental').
card_original_text('windreaver'/'DIS', 'Flying\n{W}: Windreaver gains vigilance until end of turn.\n{W}: Windreaver gets +0/+1 until end of turn.\n{U}: Switch Windreaver\'s power and toughness until end of turn.\n{U}: Return Windreaver to its owner\'s hand.').
card_first_print('windreaver', 'DIS').
card_image_name('windreaver'/'DIS', 'windreaver').
card_uid('windreaver'/'DIS', 'DIS:Windreaver:windreaver').
card_rarity('windreaver'/'DIS', 'Rare').
card_artist('windreaver'/'DIS', 'Aleksi Briclot').
card_number('windreaver'/'DIS', '138').
card_multiverse_id('windreaver'/'DIS', '97094').
card_watermark('windreaver'/'DIS', 'Azorius').

card_in_set('wit\'s end', 'DIS').
card_original_type('wit\'s end'/'DIS', 'Sorcery').
card_original_text('wit\'s end'/'DIS', 'Target player discards his or her hand.').
card_first_print('wit\'s end', 'DIS').
card_image_name('wit\'s end'/'DIS', 'wit\'s end').
card_uid('wit\'s end'/'DIS', 'DIS:Wit\'s End:wit\'s end').
card_rarity('wit\'s end'/'DIS', 'Rare').
card_artist('wit\'s end'/'DIS', 'Kev Walker').
card_number('wit\'s end'/'DIS', '58').
card_flavor_text('wit\'s end'/'DIS', '\"Some images are so powerful that one glance burns them into your mind forever. Come, let us gaze on nothingness.\"\n—Szadek').
card_multiverse_id('wit\'s end'/'DIS', '107562').

card_in_set('wrecking ball', 'DIS').
card_original_type('wrecking ball'/'DIS', 'Instant').
card_original_text('wrecking ball'/'DIS', 'Destroy target creature or land.').
card_first_print('wrecking ball', 'DIS').
card_image_name('wrecking ball'/'DIS', 'wrecking ball').
card_uid('wrecking ball'/'DIS', 'DIS:Wrecking Ball:wrecking ball').
card_rarity('wrecking ball'/'DIS', 'Common').
card_artist('wrecking ball'/'DIS', 'Ron Spears').
card_number('wrecking ball'/'DIS', '139').
card_flavor_text('wrecking ball'/'DIS', 'Rakdos festivals almost leave enough rubble in their wake to hide the bodies.').
card_multiverse_id('wrecking ball'/'DIS', '111202').
card_watermark('wrecking ball'/'DIS', 'Rakdos').

card_in_set('writ of passage', 'DIS').
card_original_type('writ of passage'/'DIS', 'Enchantment — Aura').
card_original_text('writ of passage'/'DIS', 'Enchant creature\nWhenever enchanted creature attacks, if its power is 2 or less, it\'s unblockable this turn.\nForecast {1}{U}, Reveal Writ of Passage from your hand: Target creature with power 2 or less is unblockable this turn. (Play this ability only during your upkeep and only once each turn.)').
card_first_print('writ of passage', 'DIS').
card_image_name('writ of passage'/'DIS', 'writ of passage').
card_uid('writ of passage'/'DIS', 'DIS:Writ of Passage:writ of passage').
card_rarity('writ of passage'/'DIS', 'Common').
card_artist('writ of passage'/'DIS', 'Luca Zontini').
card_number('writ of passage'/'DIS', '37').
card_multiverse_id('writ of passage'/'DIS', '107413').
card_watermark('writ of passage'/'DIS', 'Azorius').
