% Dragons of Tarkir

set('DTK').
set_name('DTK', 'Dragons of Tarkir').
set_release_date('DTK', '2015-03-27').
set_border('DTK', 'black').
set_type('DTK', 'expansion').
set_block('DTK', 'Khans of Tarkir').

card_in_set('acid-spewer dragon', 'DTK').
card_original_type('acid-spewer dragon'/'DTK', 'Creature — Dragon').
card_original_text('acid-spewer dragon'/'DTK', 'Flying, deathtouch\nMegamorph {5}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Acid-Spewer Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_first_print('acid-spewer dragon', 'DTK').
card_image_name('acid-spewer dragon'/'DTK', 'acid-spewer dragon').
card_uid('acid-spewer dragon'/'DTK', 'DTK:Acid-Spewer Dragon:acid-spewer dragon').
card_rarity('acid-spewer dragon'/'DTK', 'Uncommon').
card_artist('acid-spewer dragon'/'DTK', 'James Zapata').
card_number('acid-spewer dragon'/'DTK', '86').
card_multiverse_id('acid-spewer dragon'/'DTK', '394485').
card_watermark('acid-spewer dragon'/'DTK', 'Silumgar').

card_in_set('aerie bowmasters', 'DTK').
card_original_type('aerie bowmasters'/'DTK', 'Creature — Hound Archer').
card_original_text('aerie bowmasters'/'DTK', 'Reach (This creature can block creatures with flying.)\nMegamorph {5}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('aerie bowmasters', 'DTK').
card_image_name('aerie bowmasters'/'DTK', 'aerie bowmasters').
card_uid('aerie bowmasters'/'DTK', 'DTK:Aerie Bowmasters:aerie bowmasters').
card_rarity('aerie bowmasters'/'DTK', 'Common').
card_artist('aerie bowmasters'/'DTK', 'Matt Stewart').
card_number('aerie bowmasters'/'DTK', '170').
card_multiverse_id('aerie bowmasters'/'DTK', '394486').
card_watermark('aerie bowmasters'/'DTK', 'Dromoka').

card_in_set('ainok artillerist', 'DTK').
card_original_type('ainok artillerist'/'DTK', 'Creature — Hound Archer').
card_original_text('ainok artillerist'/'DTK', 'Ainok Artillerist has reach as long as it has a +1/+1 counter on it. (It can block creatures with flying.)').
card_first_print('ainok artillerist', 'DTK').
card_image_name('ainok artillerist'/'DTK', 'ainok artillerist').
card_uid('ainok artillerist'/'DTK', 'DTK:Ainok Artillerist:ainok artillerist').
card_rarity('ainok artillerist'/'DTK', 'Common').
card_artist('ainok artillerist'/'DTK', 'James Paick').
card_number('ainok artillerist'/'DTK', '171').
card_flavor_text('ainok artillerist'/'DTK', '\"In the Tarkir I once knew, the Abzan revered their ancestor trees. Now, as Dromoka, they cut their finest groves to make weapons.\"\n—Sarkhan Vol').
card_multiverse_id('ainok artillerist'/'DTK', '394487').
card_watermark('ainok artillerist'/'DTK', 'Dromoka').

card_in_set('ainok survivalist', 'DTK').
card_original_type('ainok survivalist'/'DTK', 'Creature — Hound Shaman').
card_original_text('ainok survivalist'/'DTK', 'Megamorph {1}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Ainok Survivalist is turned face up, destroy target artifact or enchantment an opponent controls.').
card_first_print('ainok survivalist', 'DTK').
card_image_name('ainok survivalist'/'DTK', 'ainok survivalist').
card_uid('ainok survivalist'/'DTK', 'DTK:Ainok Survivalist:ainok survivalist').
card_rarity('ainok survivalist'/'DTK', 'Uncommon').
card_artist('ainok survivalist'/'DTK', 'Craig J Spearing').
card_number('ainok survivalist'/'DTK', '172').
card_multiverse_id('ainok survivalist'/'DTK', '394488').
card_watermark('ainok survivalist'/'DTK', 'Atarka').

card_in_set('ambuscade shaman', 'DTK').
card_original_type('ambuscade shaman'/'DTK', 'Creature — Orc Shaman').
card_original_text('ambuscade shaman'/'DTK', 'Whenever Ambuscade Shaman or another creature enters the battlefield under your control, that creature gets +2/+2 until end of turn.\nDash {3}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('ambuscade shaman', 'DTK').
card_image_name('ambuscade shaman'/'DTK', 'ambuscade shaman').
card_uid('ambuscade shaman'/'DTK', 'DTK:Ambuscade Shaman:ambuscade shaman').
card_rarity('ambuscade shaman'/'DTK', 'Uncommon').
card_artist('ambuscade shaman'/'DTK', 'Anthony Palumbo').
card_number('ambuscade shaman'/'DTK', '87').
card_multiverse_id('ambuscade shaman'/'DTK', '394489').
card_watermark('ambuscade shaman'/'DTK', 'Kolaghan').

card_in_set('anafenza, kin-tree spirit', 'DTK').
card_original_type('anafenza, kin-tree spirit'/'DTK', 'Legendary Creature — Spirit Soldier').
card_original_text('anafenza, kin-tree spirit'/'DTK', 'Whenever another nontoken creature enters the battlefield under your control, bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('anafenza, kin-tree spirit', 'DTK').
card_image_name('anafenza, kin-tree spirit'/'DTK', 'anafenza, kin-tree spirit').
card_uid('anafenza, kin-tree spirit'/'DTK', 'DTK:Anafenza, Kin-Tree Spirit:anafenza, kin-tree spirit').
card_rarity('anafenza, kin-tree spirit'/'DTK', 'Rare').
card_artist('anafenza, kin-tree spirit'/'DTK', 'Ryan Yee').
card_number('anafenza, kin-tree spirit'/'DTK', '2').
card_flavor_text('anafenza, kin-tree spirit'/'DTK', 'Martyred for worshipping her ancestors, she now walks among them.').
card_multiverse_id('anafenza, kin-tree spirit'/'DTK', '394490').
card_watermark('anafenza, kin-tree spirit'/'DTK', 'Dromoka').

card_in_set('ancestral statue', 'DTK').
card_original_type('ancestral statue'/'DTK', 'Artifact Creature — Golem').
card_original_text('ancestral statue'/'DTK', 'When Ancestral Statue enters the battlefield, return a nonland permanent you control to its owner\'s hand.').
card_first_print('ancestral statue', 'DTK').
card_image_name('ancestral statue'/'DTK', 'ancestral statue').
card_uid('ancestral statue'/'DTK', 'DTK:Ancestral Statue:ancestral statue').
card_rarity('ancestral statue'/'DTK', 'Common').
card_artist('ancestral statue'/'DTK', 'Tomasz Jedruszek').
card_number('ancestral statue'/'DTK', '234').
card_flavor_text('ancestral statue'/'DTK', 'The mage awakened the statue in hopes of learning the lost lore of her clan, but the statue was interested only in war.').
card_multiverse_id('ancestral statue'/'DTK', '394491').

card_in_set('ancient carp', 'DTK').
card_original_type('ancient carp'/'DTK', 'Creature — Fish').
card_original_text('ancient carp'/'DTK', '').
card_first_print('ancient carp', 'DTK').
card_image_name('ancient carp'/'DTK', 'ancient carp').
card_uid('ancient carp'/'DTK', 'DTK:Ancient Carp:ancient carp').
card_rarity('ancient carp'/'DTK', 'Common').
card_artist('ancient carp'/'DTK', 'Christopher Burdett').
card_number('ancient carp'/'DTK', '44').
card_flavor_text('ancient carp'/'DTK', '\"Why eat now what could one day grow into a feast?\"\n—Ojutai, translated from Draconic').
card_multiverse_id('ancient carp'/'DTK', '394492').

card_in_set('anticipate', 'DTK').
card_original_type('anticipate'/'DTK', 'Instant').
card_original_text('anticipate'/'DTK', 'Look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_first_print('anticipate', 'DTK').
card_image_name('anticipate'/'DTK', 'anticipate').
card_uid('anticipate'/'DTK', 'DTK:Anticipate:anticipate').
card_rarity('anticipate'/'DTK', 'Common').
card_artist('anticipate'/'DTK', 'Lake Hurwitz').
card_number('anticipate'/'DTK', '45').
card_flavor_text('anticipate'/'DTK', '\"When I have meditated on all outcomes, my opponent has no recourse.\"').
card_multiverse_id('anticipate'/'DTK', '394493').

card_in_set('arashin foremost', 'DTK').
card_original_type('arashin foremost'/'DTK', 'Creature — Human Warrior').
card_original_text('arashin foremost'/'DTK', 'Double strike\nWhenever Arashin Foremost enters the battlefield or attacks, another target Warrior creature you control gains double strike until end of turn.').
card_first_print('arashin foremost', 'DTK').
card_image_name('arashin foremost'/'DTK', 'arashin foremost').
card_uid('arashin foremost'/'DTK', 'DTK:Arashin Foremost:arashin foremost').
card_rarity('arashin foremost'/'DTK', 'Rare').
card_artist('arashin foremost'/'DTK', 'David Palumbo').
card_number('arashin foremost'/'DTK', '3').
card_flavor_text('arashin foremost'/'DTK', '\"I would gladly give my life if it would inspire my clan to victory.\"').
card_multiverse_id('arashin foremost'/'DTK', '394494').
card_watermark('arashin foremost'/'DTK', 'Dromoka').

card_in_set('arashin sovereign', 'DTK').
card_original_type('arashin sovereign'/'DTK', 'Creature — Dragon').
card_original_text('arashin sovereign'/'DTK', 'Flying\nWhen Arashin Sovereign dies, you may put it on the top or bottom of its owner\'s library.').
card_image_name('arashin sovereign'/'DTK', 'arashin sovereign').
card_uid('arashin sovereign'/'DTK', 'DTK:Arashin Sovereign:arashin sovereign').
card_rarity('arashin sovereign'/'DTK', 'Rare').
card_artist('arashin sovereign'/'DTK', 'Dan Scott').
card_number('arashin sovereign'/'DTK', '212').
card_flavor_text('arashin sovereign'/'DTK', 'Dromoka dragons foster trust among their subjects, while the other clans must spend their time quelling rebellion.').
card_multiverse_id('arashin sovereign'/'DTK', '394495').
card_watermark('arashin sovereign'/'DTK', 'Dromoka').

card_in_set('artful maneuver', 'DTK').
card_original_type('artful maneuver'/'DTK', 'Instant').
card_original_text('artful maneuver'/'DTK', 'Target creature gets +2/+2 until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('artful maneuver', 'DTK').
card_image_name('artful maneuver'/'DTK', 'artful maneuver').
card_uid('artful maneuver'/'DTK', 'DTK:Artful Maneuver:artful maneuver').
card_rarity('artful maneuver'/'DTK', 'Common').
card_artist('artful maneuver'/'DTK', 'Lars Grant-West').
card_number('artful maneuver'/'DTK', '4').
card_multiverse_id('artful maneuver'/'DTK', '394496').
card_watermark('artful maneuver'/'DTK', 'Ojutai').

card_in_set('assault formation', 'DTK').
card_original_type('assault formation'/'DTK', 'Enchantment').
card_original_text('assault formation'/'DTK', 'Each creature you control assigns combat damage equal to its toughness rather than its power.\n{G}: Target creature with defender can attack this turn as though it didn\'t have defender.\n{2}{G}: Creatures you control get +0/+1 until end of turn.').
card_first_print('assault formation', 'DTK').
card_image_name('assault formation'/'DTK', 'assault formation').
card_uid('assault formation'/'DTK', 'DTK:Assault Formation:assault formation').
card_rarity('assault formation'/'DTK', 'Rare').
card_artist('assault formation'/'DTK', 'Kieran Yanner').
card_number('assault formation'/'DTK', '173').
card_multiverse_id('assault formation'/'DTK', '394497').

card_in_set('atarka beastbreaker', 'DTK').
card_original_type('atarka beastbreaker'/'DTK', 'Creature — Human Warrior').
card_original_text('atarka beastbreaker'/'DTK', 'Formidable — {4}{G}: Atarka Beastbreaker gets +4/+4 until end of turn. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('atarka beastbreaker', 'DTK').
card_image_name('atarka beastbreaker'/'DTK', 'atarka beastbreaker').
card_uid('atarka beastbreaker'/'DTK', 'DTK:Atarka Beastbreaker:atarka beastbreaker').
card_rarity('atarka beastbreaker'/'DTK', 'Common').
card_artist('atarka beastbreaker'/'DTK', 'Johannes Voss').
card_number('atarka beastbreaker'/'DTK', '174').
card_flavor_text('atarka beastbreaker'/'DTK', 'He scorns the heavy winter garb of lesser warriors, trusting his anger to keep him warm.').
card_multiverse_id('atarka beastbreaker'/'DTK', '394498').
card_watermark('atarka beastbreaker'/'DTK', 'Atarka').

card_in_set('atarka efreet', 'DTK').
card_original_type('atarka efreet'/'DTK', 'Creature — Efreet Shaman').
card_original_text('atarka efreet'/'DTK', 'Megamorph {2}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Atarka Efreet is turned face up, it deals 1 damage to target creature or player.').
card_first_print('atarka efreet', 'DTK').
card_image_name('atarka efreet'/'DTK', 'atarka efreet').
card_uid('atarka efreet'/'DTK', 'DTK:Atarka Efreet:atarka efreet').
card_rarity('atarka efreet'/'DTK', 'Common').
card_artist('atarka efreet'/'DTK', 'Izzy').
card_number('atarka efreet'/'DTK', '128').
card_multiverse_id('atarka efreet'/'DTK', '394499').
card_watermark('atarka efreet'/'DTK', 'Atarka').

card_in_set('atarka monument', 'DTK').
card_original_type('atarka monument'/'DTK', 'Artifact').
card_original_text('atarka monument'/'DTK', '{T}: Add {R} or {G} to your mana pool.\n{4}{R}{G}: Atarka Monument becomes a 4/4 red and green Dragon artifact creature with flying until end of turn.').
card_first_print('atarka monument', 'DTK').
card_image_name('atarka monument'/'DTK', 'atarka monument').
card_uid('atarka monument'/'DTK', 'DTK:Atarka Monument:atarka monument').
card_rarity('atarka monument'/'DTK', 'Uncommon').
card_artist('atarka monument'/'DTK', 'Daniel Ljunggren').
card_number('atarka monument'/'DTK', '235').
card_flavor_text('atarka monument'/'DTK', 'Atarka is worshipped and fed by her subjects at Ayagor, the Dragon\'s Bowl.').
card_multiverse_id('atarka monument'/'DTK', '394500').
card_watermark('atarka monument'/'DTK', 'Atarka').

card_in_set('atarka pummeler', 'DTK').
card_original_type('atarka pummeler'/'DTK', 'Creature — Ogre Warrior').
card_original_text('atarka pummeler'/'DTK', 'Formidable — {3}{R}{R}: Each creature you control can\'t be blocked this turn except by two or more creatures. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('atarka pummeler', 'DTK').
card_image_name('atarka pummeler'/'DTK', 'atarka pummeler').
card_uid('atarka pummeler'/'DTK', 'DTK:Atarka Pummeler:atarka pummeler').
card_rarity('atarka pummeler'/'DTK', 'Uncommon').
card_artist('atarka pummeler'/'DTK', 'Lucas Graciano').
card_number('atarka pummeler'/'DTK', '129').
card_multiverse_id('atarka pummeler'/'DTK', '394501').
card_watermark('atarka pummeler'/'DTK', 'Atarka').

card_in_set('atarka\'s command', 'DTK').
card_original_type('atarka\'s command'/'DTK', 'Instant').
card_original_text('atarka\'s command'/'DTK', 'Choose two —\n• Your opponents can\'t gain life this turn.\n• Atarka\'s Command deals 3 damage to each opponent.\n• You may put a land card from your hand onto the battlefield.\n• Creatures you control get +1/+1 and gain reach until end of turn.').
card_first_print('atarka\'s command', 'DTK').
card_image_name('atarka\'s command'/'DTK', 'atarka\'s command').
card_uid('atarka\'s command'/'DTK', 'DTK:Atarka\'s Command:atarka\'s command').
card_rarity('atarka\'s command'/'DTK', 'Rare').
card_artist('atarka\'s command'/'DTK', 'Chris Rahn').
card_number('atarka\'s command'/'DTK', '213').
card_multiverse_id('atarka\'s command'/'DTK', '394502').
card_watermark('atarka\'s command'/'DTK', 'Atarka').

card_in_set('avatar of the resolute', 'DTK').
card_original_type('avatar of the resolute'/'DTK', 'Creature — Avatar').
card_original_text('avatar of the resolute'/'DTK', 'Reach, trample\nAvatar of the Resolute enters the battlefield with a +1/+1 counter on it for each other creature you control with a +1/+1 counter on it.').
card_first_print('avatar of the resolute', 'DTK').
card_image_name('avatar of the resolute'/'DTK', 'avatar of the resolute').
card_uid('avatar of the resolute'/'DTK', 'DTK:Avatar of the Resolute:avatar of the resolute').
card_rarity('avatar of the resolute'/'DTK', 'Rare').
card_artist('avatar of the resolute'/'DTK', 'Jeff Simpson').
card_number('avatar of the resolute'/'DTK', '175').
card_flavor_text('avatar of the resolute'/'DTK', 'It exemplifies the ideals of the Dromoka: strength, unity, and honor.').
card_multiverse_id('avatar of the resolute'/'DTK', '394503').
card_watermark('avatar of the resolute'/'DTK', 'Dromoka').

card_in_set('aven sunstriker', 'DTK').
card_original_type('aven sunstriker'/'DTK', 'Creature — Bird Warrior').
card_original_text('aven sunstriker'/'DTK', 'Flying\nDouble strike (This creature deals both first-strike and regular combat damage.)\nMegamorph {4}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('aven sunstriker', 'DTK').
card_image_name('aven sunstriker'/'DTK', 'aven sunstriker').
card_uid('aven sunstriker'/'DTK', 'DTK:Aven Sunstriker:aven sunstriker').
card_rarity('aven sunstriker'/'DTK', 'Uncommon').
card_artist('aven sunstriker'/'DTK', 'John Severin Brassell').
card_number('aven sunstriker'/'DTK', '5').
card_multiverse_id('aven sunstriker'/'DTK', '394504').
card_watermark('aven sunstriker'/'DTK', 'Ojutai').

card_in_set('aven tactician', 'DTK').
card_original_type('aven tactician'/'DTK', 'Creature — Bird Soldier').
card_original_text('aven tactician'/'DTK', 'Flying\nWhen Aven Tactician enters the battlefield, bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('aven tactician', 'DTK').
card_image_name('aven tactician'/'DTK', 'aven tactician').
card_uid('aven tactician'/'DTK', 'DTK:Aven Tactician:aven tactician').
card_rarity('aven tactician'/'DTK', 'Common').
card_artist('aven tactician'/'DTK', 'Christopher Moeller').
card_number('aven tactician'/'DTK', '6').
card_flavor_text('aven tactician'/'DTK', 'The aven are admired, for they are able to fly with the dragons.').
card_multiverse_id('aven tactician'/'DTK', '394505').
card_watermark('aven tactician'/'DTK', 'Dromoka').

card_in_set('battle mastery', 'DTK').
card_original_type('battle mastery'/'DTK', 'Enchantment — Aura').
card_original_text('battle mastery'/'DTK', 'Enchant creature\nEnchanted creature has double strike. (It deals both first-strike and regular combat damage.)').
card_image_name('battle mastery'/'DTK', 'battle mastery').
card_uid('battle mastery'/'DTK', 'DTK:Battle Mastery:battle mastery').
card_rarity('battle mastery'/'DTK', 'Uncommon').
card_artist('battle mastery'/'DTK', 'Viktor Titov').
card_number('battle mastery'/'DTK', '7').
card_flavor_text('battle mastery'/'DTK', '\"There is elegance in all the Ojutai do—even their killing.\"\n—Kirada, Silumgar enforcer').
card_multiverse_id('battle mastery'/'DTK', '394506').

card_in_set('belltoll dragon', 'DTK').
card_original_type('belltoll dragon'/'DTK', 'Creature — Dragon').
card_original_text('belltoll dragon'/'DTK', 'Flying, hexproof\nMegamorph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Belltoll Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_first_print('belltoll dragon', 'DTK').
card_image_name('belltoll dragon'/'DTK', 'belltoll dragon').
card_uid('belltoll dragon'/'DTK', 'DTK:Belltoll Dragon:belltoll dragon').
card_rarity('belltoll dragon'/'DTK', 'Uncommon').
card_artist('belltoll dragon'/'DTK', 'Zack Stella').
card_number('belltoll dragon'/'DTK', '46').
card_multiverse_id('belltoll dragon'/'DTK', '394507').
card_watermark('belltoll dragon'/'DTK', 'Ojutai').

card_in_set('berserkers\' onslaught', 'DTK').
card_original_type('berserkers\' onslaught'/'DTK', 'Enchantment').
card_original_text('berserkers\' onslaught'/'DTK', 'Attacking creatures you control have double strike.').
card_first_print('berserkers\' onslaught', 'DTK').
card_image_name('berserkers\' onslaught'/'DTK', 'berserkers\' onslaught').
card_uid('berserkers\' onslaught'/'DTK', 'DTK:Berserkers\' Onslaught:berserkers\' onslaught').
card_rarity('berserkers\' onslaught'/'DTK', 'Rare').
card_artist('berserkers\' onslaught'/'DTK', 'Zoltan Boros').
card_number('berserkers\' onslaught'/'DTK', '130').
card_flavor_text('berserkers\' onslaught'/'DTK', '\"Atarka\'s people starve while she feasts upon the bounty of their hunts. They must conquer ever more lands just for the sake of their own subsistence.\"\n—Arel, Unseen Whisperer').
card_multiverse_id('berserkers\' onslaught'/'DTK', '394508').

card_in_set('blessed reincarnation', 'DTK').
card_original_type('blessed reincarnation'/'DTK', 'Instant').
card_original_text('blessed reincarnation'/'DTK', 'Exile target creature an opponent controls. That player reveals cards from the top of his or her library until a creature card is revealed. The player puts that card onto the battlefield, then shuffles the rest into his or her library.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('blessed reincarnation', 'DTK').
card_image_name('blessed reincarnation'/'DTK', 'blessed reincarnation').
card_uid('blessed reincarnation'/'DTK', 'DTK:Blessed Reincarnation:blessed reincarnation').
card_rarity('blessed reincarnation'/'DTK', 'Rare').
card_artist('blessed reincarnation'/'DTK', 'Kev Walker').
card_number('blessed reincarnation'/'DTK', '47').
card_multiverse_id('blessed reincarnation'/'DTK', '394509').
card_watermark('blessed reincarnation'/'DTK', 'Ojutai').

card_in_set('blood-chin fanatic', 'DTK').
card_original_type('blood-chin fanatic'/'DTK', 'Creature — Orc Warrior').
card_original_text('blood-chin fanatic'/'DTK', '{1}{B}, Sacrifice another Warrior creature: Target player loses X life and you gain X life, where X is the sacrificed creature\'s power.').
card_first_print('blood-chin fanatic', 'DTK').
card_image_name('blood-chin fanatic'/'DTK', 'blood-chin fanatic').
card_uid('blood-chin fanatic'/'DTK', 'DTK:Blood-Chin Fanatic:blood-chin fanatic').
card_rarity('blood-chin fanatic'/'DTK', 'Rare').
card_artist('blood-chin fanatic'/'DTK', 'David Palumbo').
card_number('blood-chin fanatic'/'DTK', '88').
card_flavor_text('blood-chin fanatic'/'DTK', '\"We are as brutal as our dragonlord. We strike like wild lightning and feast on human flesh.\"').
card_multiverse_id('blood-chin fanatic'/'DTK', '394510').
card_watermark('blood-chin fanatic'/'DTK', 'Kolaghan').

card_in_set('blood-chin rager', 'DTK').
card_original_type('blood-chin rager'/'DTK', 'Creature — Human Warrior').
card_original_text('blood-chin rager'/'DTK', 'Whenever Blood-Chin Rager attacks, each Warrior creature you control can\'t be blocked this turn except by two or more creatures.').
card_first_print('blood-chin rager', 'DTK').
card_image_name('blood-chin rager'/'DTK', 'blood-chin rager').
card_uid('blood-chin rager'/'DTK', 'DTK:Blood-Chin Rager:blood-chin rager').
card_rarity('blood-chin rager'/'DTK', 'Uncommon').
card_artist('blood-chin rager'/'DTK', 'Karl Kopinski').
card_number('blood-chin rager'/'DTK', '89').
card_flavor_text('blood-chin rager'/'DTK', 'Kolaghan blades rarely stay clean for long.').
card_multiverse_id('blood-chin rager'/'DTK', '394511').
card_watermark('blood-chin rager'/'DTK', 'Kolaghan').

card_in_set('boltwing marauder', 'DTK').
card_original_type('boltwing marauder'/'DTK', 'Creature — Dragon').
card_original_text('boltwing marauder'/'DTK', 'Flying\nWhenever another creature enters the battlefield under your control, target creature gets +2/+0 until end of turn.').
card_image_name('boltwing marauder'/'DTK', 'boltwing marauder').
card_uid('boltwing marauder'/'DTK', 'DTK:Boltwing Marauder:boltwing marauder').
card_rarity('boltwing marauder'/'DTK', 'Rare').
card_artist('boltwing marauder'/'DTK', 'Raymond Swanland').
card_number('boltwing marauder'/'DTK', '214').
card_flavor_text('boltwing marauder'/'DTK', 'When battling the Kolaghan, consider yourself lucky if lightning strikes the same place only twice.').
card_multiverse_id('boltwing marauder'/'DTK', '394512').
card_watermark('boltwing marauder'/'DTK', 'Kolaghan').

card_in_set('butcher\'s glee', 'DTK').
card_original_type('butcher\'s glee'/'DTK', 'Instant').
card_original_text('butcher\'s glee'/'DTK', 'Target creature gets +3/+0 and gains lifelink until end of turn. Regenerate it. (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_first_print('butcher\'s glee', 'DTK').
card_image_name('butcher\'s glee'/'DTK', 'butcher\'s glee').
card_uid('butcher\'s glee'/'DTK', 'DTK:Butcher\'s Glee:butcher\'s glee').
card_rarity('butcher\'s glee'/'DTK', 'Common').
card_artist('butcher\'s glee'/'DTK', 'Jesper Ejsing').
card_number('butcher\'s glee'/'DTK', '90').
card_flavor_text('butcher\'s glee'/'DTK', 'The Crave made Kneecleaver think she was bigger than the dragon.').
card_multiverse_id('butcher\'s glee'/'DTK', '394513').

card_in_set('center soul', 'DTK').
card_original_type('center soul'/'DTK', 'Instant').
card_original_text('center soul'/'DTK', 'Target creature you control gains protection from the color of your choice until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('center soul', 'DTK').
card_image_name('center soul'/'DTK', 'center soul').
card_uid('center soul'/'DTK', 'DTK:Center Soul:center soul').
card_rarity('center soul'/'DTK', 'Common').
card_artist('center soul'/'DTK', 'Igor Kieryluk').
card_number('center soul'/'DTK', '8').
card_multiverse_id('center soul'/'DTK', '394514').
card_watermark('center soul'/'DTK', 'Ojutai').

card_in_set('champion of arashin', 'DTK').
card_original_type('champion of arashin'/'DTK', 'Creature — Hound Warrior').
card_original_text('champion of arashin'/'DTK', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('champion of arashin', 'DTK').
card_image_name('champion of arashin'/'DTK', 'champion of arashin').
card_uid('champion of arashin'/'DTK', 'DTK:Champion of Arashin:champion of arashin').
card_rarity('champion of arashin'/'DTK', 'Common').
card_artist('champion of arashin'/'DTK', 'Joseph Meehan').
card_number('champion of arashin'/'DTK', '9').
card_flavor_text('champion of arashin'/'DTK', '\"The blood of Dromoka and the blood of my veins are the same.\"').
card_multiverse_id('champion of arashin'/'DTK', '394515').
card_watermark('champion of arashin'/'DTK', 'Dromoka').

card_in_set('circle of elders', 'DTK').
card_original_type('circle of elders'/'DTK', 'Creature — Human Shaman').
card_original_text('circle of elders'/'DTK', 'Vigilance\nFormidable — {T}: Add {3} to your mana pool. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('circle of elders', 'DTK').
card_image_name('circle of elders'/'DTK', 'circle of elders').
card_uid('circle of elders'/'DTK', 'DTK:Circle of Elders:circle of elders').
card_rarity('circle of elders'/'DTK', 'Uncommon').
card_artist('circle of elders'/'DTK', 'Jakub Kasper').
card_number('circle of elders'/'DTK', '176').
card_flavor_text('circle of elders'/'DTK', 'They whisper of an ancient hero who saved the dragons of their world.').
card_multiverse_id('circle of elders'/'DTK', '394516').
card_watermark('circle of elders'/'DTK', 'Atarka').

card_in_set('clone legion', 'DTK').
card_original_type('clone legion'/'DTK', 'Sorcery').
card_original_text('clone legion'/'DTK', 'For each creature target player controls, put a token onto the battlefield that\'s a copy of that creature.').
card_first_print('clone legion', 'DTK').
card_image_name('clone legion'/'DTK', 'clone legion').
card_uid('clone legion'/'DTK', 'DTK:Clone Legion:clone legion').
card_rarity('clone legion'/'DTK', 'Mythic Rare').
card_artist('clone legion'/'DTK', 'Svetlin Velinov').
card_number('clone legion'/'DTK', '48').
card_flavor_text('clone legion'/'DTK', 'Their lust for battle was so great that they didn\'t care their enemies wore their own faces.').
card_multiverse_id('clone legion'/'DTK', '394517').

card_in_set('coat with venom', 'DTK').
card_original_type('coat with venom'/'DTK', 'Instant').
card_original_text('coat with venom'/'DTK', 'Target creature gets +1/+2 and gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('coat with venom', 'DTK').
card_image_name('coat with venom'/'DTK', 'coat with venom').
card_uid('coat with venom'/'DTK', 'DTK:Coat with Venom:coat with venom').
card_rarity('coat with venom'/'DTK', 'Common').
card_artist('coat with venom'/'DTK', 'Johann Bodin').
card_number('coat with venom'/'DTK', '91').
card_flavor_text('coat with venom'/'DTK', '\"Every Silumgar blade carries the blessing of our dragonlord.\"\n—Xathi the Infallible').
card_multiverse_id('coat with venom'/'DTK', '394518').

card_in_set('collected company', 'DTK').
card_original_type('collected company'/'DTK', 'Instant').
card_original_text('collected company'/'DTK', 'Look at the top six cards of your library. Put up to two creature cards with converted mana cost 3 or less from among them onto the battlefield. Put the rest on the bottom of your library in any order.').
card_first_print('collected company', 'DTK').
card_image_name('collected company'/'DTK', 'collected company').
card_uid('collected company'/'DTK', 'DTK:Collected Company:collected company').
card_rarity('collected company'/'DTK', 'Rare').
card_artist('collected company'/'DTK', 'Franz Vohwinkel').
card_number('collected company'/'DTK', '177').
card_flavor_text('collected company'/'DTK', 'Many can stand where one would fall.').
card_multiverse_id('collected company'/'DTK', '394519').

card_in_set('colossodon yearling', 'DTK').
card_original_type('colossodon yearling'/'DTK', 'Creature — Beast').
card_original_text('colossodon yearling'/'DTK', '').
card_first_print('colossodon yearling', 'DTK').
card_image_name('colossodon yearling'/'DTK', 'colossodon yearling').
card_uid('colossodon yearling'/'DTK', 'DTK:Colossodon Yearling:colossodon yearling').
card_rarity('colossodon yearling'/'DTK', 'Common').
card_artist('colossodon yearling'/'DTK', 'Yeong-Hao Han').
card_number('colossodon yearling'/'DTK', '178').
card_flavor_text('colossodon yearling'/'DTK', 'The colossodon\'s hard outer shell stops many predators, but with a gentle flip from a dragon, it quickly becomes a meal in a bowl.').
card_multiverse_id('colossodon yearling'/'DTK', '394520').

card_in_set('commune with lava', 'DTK').
card_original_type('commune with lava'/'DTK', 'Instant').
card_original_text('commune with lava'/'DTK', 'Exile the top X cards of your library. Until the end of your next turn, you may play those cards.').
card_first_print('commune with lava', 'DTK').
card_image_name('commune with lava'/'DTK', 'commune with lava').
card_uid('commune with lava'/'DTK', 'DTK:Commune with Lava:commune with lava').
card_rarity('commune with lava'/'DTK', 'Rare').
card_artist('commune with lava'/'DTK', 'Ryan Barger').
card_number('commune with lava'/'DTK', '131').
card_flavor_text('commune with lava'/'DTK', 'Atarka conquered Qadat, the Fire Rim, long ago, winning over its efreet with a promise to spread the glory of fire to all the world.').
card_multiverse_id('commune with lava'/'DTK', '394521').

card_in_set('conifer strider', 'DTK').
card_original_type('conifer strider'/'DTK', 'Creature — Elemental').
card_original_text('conifer strider'/'DTK', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_first_print('conifer strider', 'DTK').
card_image_name('conifer strider'/'DTK', 'conifer strider').
card_uid('conifer strider'/'DTK', 'DTK:Conifer Strider:conifer strider').
card_rarity('conifer strider'/'DTK', 'Common').
card_artist('conifer strider'/'DTK', 'YW Tang').
card_number('conifer strider'/'DTK', '179').
card_flavor_text('conifer strider'/'DTK', 'Atarka\'s presence thaws the glaciers of Qal Sisma, forcing its elementals to migrate or adapt.').
card_multiverse_id('conifer strider'/'DTK', '394522').

card_in_set('contradict', 'DTK').
card_original_type('contradict'/'DTK', 'Instant').
card_original_text('contradict'/'DTK', 'Counter target spell.\nDraw a card.').
card_first_print('contradict', 'DTK').
card_image_name('contradict'/'DTK', 'contradict').
card_uid('contradict'/'DTK', 'DTK:Contradict:contradict').
card_rarity('contradict'/'DTK', 'Common').
card_artist('contradict'/'DTK', 'Steve Prescott').
card_number('contradict'/'DTK', '49').
card_flavor_text('contradict'/'DTK', 'Those who question Ojutai may not like the answers they receive.').
card_multiverse_id('contradict'/'DTK', '394523').

card_in_set('corpseweft', 'DTK').
card_original_type('corpseweft'/'DTK', 'Enchantment').
card_original_text('corpseweft'/'DTK', '{1}{B}, Exile one or more creature cards from your graveyard: Put an X/X black Zombie Horror creature token onto the battlefield tapped, where X is twice the number of cards exiled this way.').
card_first_print('corpseweft', 'DTK').
card_image_name('corpseweft'/'DTK', 'corpseweft').
card_uid('corpseweft'/'DTK', 'DTK:Corpseweft:corpseweft').
card_rarity('corpseweft'/'DTK', 'Rare').
card_artist('corpseweft'/'DTK', 'Nils Hamm').
card_number('corpseweft'/'DTK', '92').
card_flavor_text('corpseweft'/'DTK', 'Sidisi hated to limit her options.').
card_multiverse_id('corpseweft'/'DTK', '394524').

card_in_set('crater elemental', 'DTK').
card_original_type('crater elemental'/'DTK', 'Creature — Elemental').
card_original_text('crater elemental'/'DTK', '{R}, {T}, Sacrifice Crater Elemental: Crater Elemental deals 4 damage to target creature.\nFormidable — {2}{R}: Crater Elemental has base power 8 until end of turn. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('crater elemental', 'DTK').
card_image_name('crater elemental'/'DTK', 'crater elemental').
card_uid('crater elemental'/'DTK', 'DTK:Crater Elemental:crater elemental').
card_rarity('crater elemental'/'DTK', 'Rare').
card_artist('crater elemental'/'DTK', 'Svetlin Velinov').
card_number('crater elemental'/'DTK', '132').
card_multiverse_id('crater elemental'/'DTK', '394525').
card_watermark('crater elemental'/'DTK', 'Atarka').

card_in_set('cunning breezedancer', 'DTK').
card_original_type('cunning breezedancer'/'DTK', 'Creature — Dragon').
card_original_text('cunning breezedancer'/'DTK', 'Flying\nWhenever you cast a noncreature spell, Cunning Breezedancer gets +2/+2 until end of turn.').
card_first_print('cunning breezedancer', 'DTK').
card_image_name('cunning breezedancer'/'DTK', 'cunning breezedancer').
card_uid('cunning breezedancer'/'DTK', 'DTK:Cunning Breezedancer:cunning breezedancer').
card_rarity('cunning breezedancer'/'DTK', 'Uncommon').
card_artist('cunning breezedancer'/'DTK', 'Todd Lockwood').
card_number('cunning breezedancer'/'DTK', '215').
card_flavor_text('cunning breezedancer'/'DTK', '\"That which is beautiful in form can also be deadly.\"\n—Ishai, Ojutai dragonspeaker').
card_multiverse_id('cunning breezedancer'/'DTK', '394526').
card_watermark('cunning breezedancer'/'DTK', 'Ojutai').

card_in_set('custodian of the trove', 'DTK').
card_original_type('custodian of the trove'/'DTK', 'Artifact Creature — Golem').
card_original_text('custodian of the trove'/'DTK', 'Defender\nCustodian of the Trove enters the battlefield tapped.').
card_first_print('custodian of the trove', 'DTK').
card_image_name('custodian of the trove'/'DTK', 'custodian of the trove').
card_uid('custodian of the trove'/'DTK', 'DTK:Custodian of the Trove:custodian of the trove').
card_rarity('custodian of the trove'/'DTK', 'Common').
card_artist('custodian of the trove'/'DTK', 'Raoul Vitale').
card_number('custodian of the trove'/'DTK', '236').
card_flavor_text('custodian of the trove'/'DTK', 'Silumgar delights in repurposing the treasures of other clans to serve his own ravenous greed.').
card_multiverse_id('custodian of the trove'/'DTK', '394527').

card_in_set('damnable pact', 'DTK').
card_original_type('damnable pact'/'DTK', 'Sorcery').
card_original_text('damnable pact'/'DTK', 'Target player draws X cards and loses X life.').
card_first_print('damnable pact', 'DTK').
card_image_name('damnable pact'/'DTK', 'damnable pact').
card_uid('damnable pact'/'DTK', 'DTK:Damnable Pact:damnable pact').
card_rarity('damnable pact'/'DTK', 'Rare').
card_artist('damnable pact'/'DTK', 'Zack Stella').
card_number('damnable pact'/'DTK', '93').
card_flavor_text('damnable pact'/'DTK', '\"Silumgar\'s mind is a dark labyrinth, full of grim secrets and subtle traps.\"\n—Siara, the Dragon\'s Mouth').
card_multiverse_id('damnable pact'/'DTK', '394528').

card_in_set('dance of the skywise', 'DTK').
card_original_type('dance of the skywise'/'DTK', 'Instant').
card_original_text('dance of the skywise'/'DTK', 'Until end of turn, target creature you control becomes a blue Dragon Illusion with base power and toughness 4/4, loses all abilities, and gains flying.').
card_first_print('dance of the skywise', 'DTK').
card_image_name('dance of the skywise'/'DTK', 'dance of the skywise').
card_uid('dance of the skywise'/'DTK', 'DTK:Dance of the Skywise:dance of the skywise').
card_rarity('dance of the skywise'/'DTK', 'Uncommon').
card_artist('dance of the skywise'/'DTK', 'Jack Wang').
card_number('dance of the skywise'/'DTK', '50').
card_flavor_text('dance of the skywise'/'DTK', 'The Ojutai believe in the Great Wheel: those who best serve the dragonlord are destined to be reborn as dragons.').
card_multiverse_id('dance of the skywise'/'DTK', '394529').

card_in_set('deadly wanderings', 'DTK').
card_original_type('deadly wanderings'/'DTK', 'Enchantment').
card_original_text('deadly wanderings'/'DTK', 'As long as you control exactly one creature, that creature gets +2/+0 and has deathtouch and lifelink.').
card_first_print('deadly wanderings', 'DTK').
card_image_name('deadly wanderings'/'DTK', 'deadly wanderings').
card_uid('deadly wanderings'/'DTK', 'DTK:Deadly Wanderings:deadly wanderings').
card_rarity('deadly wanderings'/'DTK', 'Uncommon').
card_artist('deadly wanderings'/'DTK', 'David Palumbo').
card_number('deadly wanderings'/'DTK', '94').
card_flavor_text('deadly wanderings'/'DTK', '\"Seclusion is only an option for the strong.\"\n—Sorin Markov').
card_multiverse_id('deadly wanderings'/'DTK', '394530').

card_in_set('death wind', 'DTK').
card_original_type('death wind'/'DTK', 'Instant').
card_original_text('death wind'/'DTK', 'Target creature gets -X/-X until end of turn.').
card_image_name('death wind'/'DTK', 'death wind').
card_uid('death wind'/'DTK', 'DTK:Death Wind:death wind').
card_rarity('death wind'/'DTK', 'Uncommon').
card_artist('death wind'/'DTK', 'Nils Hamm').
card_number('death wind'/'DTK', '95').
card_flavor_text('death wind'/'DTK', '\"I am a dragonslayer for Lord Silumgar. There is no dragon save him whom I fear.\"\n—Xathi the Infallible').
card_multiverse_id('death wind'/'DTK', '394531').

card_in_set('deathbringer regent', 'DTK').
card_original_type('deathbringer regent'/'DTK', 'Creature — Dragon').
card_original_text('deathbringer regent'/'DTK', 'Flying\nWhen Deathbringer Regent enters the battlefield, if you cast it from your hand and there are five or more other creatures on the battlefield, destroy all other creatures.').
card_image_name('deathbringer regent'/'DTK', 'deathbringer regent').
card_uid('deathbringer regent'/'DTK', 'DTK:Deathbringer Regent:deathbringer regent').
card_rarity('deathbringer regent'/'DTK', 'Rare').
card_artist('deathbringer regent'/'DTK', 'Adam Paquette').
card_number('deathbringer regent'/'DTK', '96').
card_multiverse_id('deathbringer regent'/'DTK', '394532').
card_watermark('deathbringer regent'/'DTK', 'Silumgar').

card_in_set('deathmist raptor', 'DTK').
card_original_type('deathmist raptor'/'DTK', 'Creature — Lizard Beast').
card_original_text('deathmist raptor'/'DTK', 'Deathtouch\nWhenever a permanent you control is turned face up, you may return Deathmist Raptor from your graveyard to the battlefield face up or face down.\nMegamorph {4}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('deathmist raptor', 'DTK').
card_image_name('deathmist raptor'/'DTK', 'deathmist raptor').
card_uid('deathmist raptor'/'DTK', 'DTK:Deathmist Raptor:deathmist raptor').
card_rarity('deathmist raptor'/'DTK', 'Mythic Rare').
card_artist('deathmist raptor'/'DTK', 'Filip Burburan').
card_number('deathmist raptor'/'DTK', '180').
card_multiverse_id('deathmist raptor'/'DTK', '394533').

card_in_set('defeat', 'DTK').
card_original_type('defeat'/'DTK', 'Sorcery').
card_original_text('defeat'/'DTK', 'Destroy target creature with power 2 or less.').
card_first_print('defeat', 'DTK').
card_image_name('defeat'/'DTK', 'defeat').
card_uid('defeat'/'DTK', 'DTK:Defeat:defeat').
card_rarity('defeat'/'DTK', 'Common').
card_artist('defeat'/'DTK', 'Dave Kendall').
card_number('defeat'/'DTK', '97').
card_flavor_text('defeat'/'DTK', '\"Wars are decided one life at a time.\"\n—Gvar Barzeel, Kolaghan warrior').
card_multiverse_id('defeat'/'DTK', '394534').

card_in_set('den protector', 'DTK').
card_original_type('den protector'/'DTK', 'Creature — Human Warrior').
card_original_text('den protector'/'DTK', 'Creatures with power less than Den Protector\'s power can\'t block it.\nMegamorph {1}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Den Protector is turned face up, return target card from your graveyard to your hand.').
card_first_print('den protector', 'DTK').
card_image_name('den protector'/'DTK', 'den protector').
card_uid('den protector'/'DTK', 'DTK:Den Protector:den protector').
card_rarity('den protector'/'DTK', 'Rare').
card_artist('den protector'/'DTK', 'Viktor Titov').
card_number('den protector'/'DTK', '181').
card_multiverse_id('den protector'/'DTK', '394535').
card_watermark('den protector'/'DTK', 'Atarka').

card_in_set('descent of the dragons', 'DTK').
card_original_type('descent of the dragons'/'DTK', 'Sorcery').
card_original_text('descent of the dragons'/'DTK', 'Destroy any number of target creatures. For each creature destroyed this way, its controller puts a 4/4 red Dragon creature token with flying onto the battlefield.').
card_first_print('descent of the dragons', 'DTK').
card_image_name('descent of the dragons'/'DTK', 'descent of the dragons').
card_uid('descent of the dragons'/'DTK', 'DTK:Descent of the Dragons:descent of the dragons').
card_rarity('descent of the dragons'/'DTK', 'Mythic Rare').
card_artist('descent of the dragons'/'DTK', 'Steve Prescott').
card_number('descent of the dragons'/'DTK', '133').
card_flavor_text('descent of the dragons'/'DTK', 'Dragons emerge from tempests fully formed and terribly hungry.').
card_multiverse_id('descent of the dragons'/'DTK', '394536').

card_in_set('dirgur nemesis', 'DTK').
card_original_type('dirgur nemesis'/'DTK', 'Creature — Serpent').
card_original_text('dirgur nemesis'/'DTK', 'Defender\nMegamorph {6}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('dirgur nemesis', 'DTK').
card_image_name('dirgur nemesis'/'DTK', 'dirgur nemesis').
card_uid('dirgur nemesis'/'DTK', 'DTK:Dirgur Nemesis:dirgur nemesis').
card_rarity('dirgur nemesis'/'DTK', 'Common').
card_artist('dirgur nemesis'/'DTK', 'Mathias Kollros').
card_number('dirgur nemesis'/'DTK', '51').
card_multiverse_id('dirgur nemesis'/'DTK', '394537').

card_in_set('display of dominance', 'DTK').
card_original_type('display of dominance'/'DTK', 'Instant').
card_original_text('display of dominance'/'DTK', 'Choose one —\n• Destroy target blue or black noncreature permanent.\n• Permanents you control can\'t be the targets of blue or black spells your opponents control this turn.').
card_first_print('display of dominance', 'DTK').
card_image_name('display of dominance'/'DTK', 'display of dominance').
card_uid('display of dominance'/'DTK', 'DTK:Display of Dominance:display of dominance').
card_rarity('display of dominance'/'DTK', 'Uncommon').
card_artist('display of dominance'/'DTK', 'Tomasz Jedruszek').
card_number('display of dominance'/'DTK', '182').
card_multiverse_id('display of dominance'/'DTK', '394538').

card_in_set('draconic roar', 'DTK').
card_original_type('draconic roar'/'DTK', 'Instant').
card_original_text('draconic roar'/'DTK', 'As an additional cost to cast Draconic Roar, you may reveal a Dragon card from your hand.\nDraconic Roar deals 3 damage to target creature. If you revealed a Dragon card or controlled a Dragon as you cast Draconic Roar, Draconic Roar deals 3 damage to that creature\'s controller.').
card_first_print('draconic roar', 'DTK').
card_image_name('draconic roar'/'DTK', 'draconic roar').
card_uid('draconic roar'/'DTK', 'DTK:Draconic Roar:draconic roar').
card_rarity('draconic roar'/'DTK', 'Uncommon').
card_artist('draconic roar'/'DTK', 'Kev Walker').
card_number('draconic roar'/'DTK', '134').
card_multiverse_id('draconic roar'/'DTK', '394539').
card_watermark('draconic roar'/'DTK', 'Atarka').

card_in_set('dragon fodder', 'DTK').
card_original_type('dragon fodder'/'DTK', 'Sorcery').
card_original_text('dragon fodder'/'DTK', 'Put two 1/1 red Goblin creature tokens onto the battlefield.').
card_image_name('dragon fodder'/'DTK', 'dragon fodder').
card_uid('dragon fodder'/'DTK', 'DTK:Dragon Fodder:dragon fodder').
card_rarity('dragon fodder'/'DTK', 'Common').
card_artist('dragon fodder'/'DTK', 'Volkan Baga').
card_number('dragon fodder'/'DTK', '135').
card_flavor_text('dragon fodder'/'DTK', 'Atarka goblins meet their demise as readily as their Temur counterparts did, but usually under big, winged shadows.').
card_multiverse_id('dragon fodder'/'DTK', '394540').

card_in_set('dragon hunter', 'DTK').
card_original_type('dragon hunter'/'DTK', 'Creature — Human Warrior').
card_original_text('dragon hunter'/'DTK', 'Protection from Dragons\nDragon Hunter can block Dragons as though it had reach.').
card_first_print('dragon hunter', 'DTK').
card_image_name('dragon hunter'/'DTK', 'dragon hunter').
card_uid('dragon hunter'/'DTK', 'DTK:Dragon Hunter:dragon hunter').
card_rarity('dragon hunter'/'DTK', 'Uncommon').
card_artist('dragon hunter'/'DTK', 'Johannes Voss').
card_number('dragon hunter'/'DTK', '10').
card_flavor_text('dragon hunter'/'DTK', '\"Dromoka has taught me the secrets of her kind, that I might use them in her service.\"').
card_multiverse_id('dragon hunter'/'DTK', '394541').
card_watermark('dragon hunter'/'DTK', 'Dromoka').

card_in_set('dragon tempest', 'DTK').
card_original_type('dragon tempest'/'DTK', 'Enchantment').
card_original_text('dragon tempest'/'DTK', 'Whenever a creature with flying enters the battlefield under your control, it gains haste until end of turn.\nWhenever a Dragon enters the battlefield under your control, it deals X damage to target creature or player, where X is the number of Dragons you control.').
card_first_print('dragon tempest', 'DTK').
card_image_name('dragon tempest'/'DTK', 'dragon tempest').
card_uid('dragon tempest'/'DTK', 'DTK:Dragon Tempest:dragon tempest').
card_rarity('dragon tempest'/'DTK', 'Rare').
card_artist('dragon tempest'/'DTK', 'Willian Murai').
card_number('dragon tempest'/'DTK', '136').
card_multiverse_id('dragon tempest'/'DTK', '394542').

card_in_set('dragon whisperer', 'DTK').
card_original_type('dragon whisperer'/'DTK', 'Creature — Human Shaman').
card_original_text('dragon whisperer'/'DTK', '{R}: Dragon Whisperer gains flying until end of turn.\n{1}{R}: Dragon Whisperer gets +1/+0 until end of turn.\nFormidable — {4}{R}{R}: Put a 4/4 red Dragon creature token with flying onto the battlefield. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('dragon whisperer', 'DTK').
card_image_name('dragon whisperer'/'DTK', 'dragon whisperer').
card_uid('dragon whisperer'/'DTK', 'DTK:Dragon Whisperer:dragon whisperer').
card_rarity('dragon whisperer'/'DTK', 'Mythic Rare').
card_artist('dragon whisperer'/'DTK', 'Chris Rallis').
card_number('dragon whisperer'/'DTK', '137').
card_multiverse_id('dragon whisperer'/'DTK', '394543').
card_watermark('dragon whisperer'/'DTK', 'Atarka').

card_in_set('dragon\'s eye sentry', 'DTK').
card_original_type('dragon\'s eye sentry'/'DTK', 'Creature — Human Monk').
card_original_text('dragon\'s eye sentry'/'DTK', 'Defender, first strike').
card_first_print('dragon\'s eye sentry', 'DTK').
card_image_name('dragon\'s eye sentry'/'DTK', 'dragon\'s eye sentry').
card_uid('dragon\'s eye sentry'/'DTK', 'DTK:Dragon\'s Eye Sentry:dragon\'s eye sentry').
card_rarity('dragon\'s eye sentry'/'DTK', 'Common').
card_artist('dragon\'s eye sentry'/'DTK', 'Anastasia Ovchinnikova').
card_number('dragon\'s eye sentry'/'DTK', '11').
card_flavor_text('dragon\'s eye sentry'/'DTK', 'Even the humblest guard of Ojutai\'s strongholds ponders the puzzles of the Great Teacher\'s lessons.').
card_multiverse_id('dragon\'s eye sentry'/'DTK', '394544').
card_watermark('dragon\'s eye sentry'/'DTK', 'Ojutai').

card_in_set('dragon-scarred bear', 'DTK').
card_original_type('dragon-scarred bear'/'DTK', 'Creature — Bear').
card_original_text('dragon-scarred bear'/'DTK', 'Formidable — {1}{G}: Regenerate Dragon-Scarred Bear. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('dragon-scarred bear', 'DTK').
card_image_name('dragon-scarred bear'/'DTK', 'dragon-scarred bear').
card_uid('dragon-scarred bear'/'DTK', 'DTK:Dragon-Scarred Bear:dragon-scarred bear').
card_rarity('dragon-scarred bear'/'DTK', 'Common').
card_artist('dragon-scarred bear'/'DTK', 'Lars Grant-West').
card_number('dragon-scarred bear'/'DTK', '183').
card_flavor_text('dragon-scarred bear'/'DTK', 'Bears are a delicacy for the Atarka. The few that remain are the toughest of their species.').
card_multiverse_id('dragon-scarred bear'/'DTK', '394553').
card_watermark('dragon-scarred bear'/'DTK', 'Atarka').

card_in_set('dragonloft idol', 'DTK').
card_original_type('dragonloft idol'/'DTK', 'Artifact Creature — Gargoyle').
card_original_text('dragonloft idol'/'DTK', 'As long as you control a Dragon, Dragonloft Idol gets +1/+1 and has flying and trample.').
card_first_print('dragonloft idol', 'DTK').
card_image_name('dragonloft idol'/'DTK', 'dragonloft idol').
card_uid('dragonloft idol'/'DTK', 'DTK:Dragonloft Idol:dragonloft idol').
card_rarity('dragonloft idol'/'DTK', 'Uncommon').
card_artist('dragonloft idol'/'DTK', 'Jung Park').
card_number('dragonloft idol'/'DTK', '237').
card_flavor_text('dragonloft idol'/'DTK', 'The idols were forged during the time of the Khanfall, when the dragons came to rule Tarkir and its people aligned themselves with the five dragonlords.').
card_multiverse_id('dragonloft idol'/'DTK', '394545').

card_in_set('dragonlord atarka', 'DTK').
card_original_type('dragonlord atarka'/'DTK', 'Legendary Creature — Elder Dragon').
card_original_text('dragonlord atarka'/'DTK', 'Flying, trample\nWhen Dragonlord Atarka enters the battlefield, it deals 5 damage divided as you choose among any number of target creatures and/or planeswalkers your opponents control.').
card_first_print('dragonlord atarka', 'DTK').
card_image_name('dragonlord atarka'/'DTK', 'dragonlord atarka').
card_uid('dragonlord atarka'/'DTK', 'DTK:Dragonlord Atarka:dragonlord atarka').
card_rarity('dragonlord atarka'/'DTK', 'Mythic Rare').
card_artist('dragonlord atarka'/'DTK', 'Karl Kopinski').
card_number('dragonlord atarka'/'DTK', '216').
card_multiverse_id('dragonlord atarka'/'DTK', '394546').
card_watermark('dragonlord atarka'/'DTK', 'Atarka').

card_in_set('dragonlord dromoka', 'DTK').
card_original_type('dragonlord dromoka'/'DTK', 'Legendary Creature — Elder Dragon').
card_original_text('dragonlord dromoka'/'DTK', 'Dragonlord Dromoka can\'t be countered.\nFlying, lifelink\nYour opponents can\'t cast spells during your turn.').
card_first_print('dragonlord dromoka', 'DTK').
card_image_name('dragonlord dromoka'/'DTK', 'dragonlord dromoka').
card_uid('dragonlord dromoka'/'DTK', 'DTK:Dragonlord Dromoka:dragonlord dromoka').
card_rarity('dragonlord dromoka'/'DTK', 'Mythic Rare').
card_artist('dragonlord dromoka'/'DTK', 'Eric Deschamps').
card_number('dragonlord dromoka'/'DTK', '217').
card_flavor_text('dragonlord dromoka'/'DTK', 'Dromoka\'s followers forsake blood ties so that they may join a greater family.').
card_multiverse_id('dragonlord dromoka'/'DTK', '394547').
card_watermark('dragonlord dromoka'/'DTK', 'Dromoka').

card_in_set('dragonlord kolaghan', 'DTK').
card_original_type('dragonlord kolaghan'/'DTK', 'Legendary Creature — Elder Dragon').
card_original_text('dragonlord kolaghan'/'DTK', 'Flying, haste\nOther creatures you control have haste.\nWhenever an opponent casts a creature or planeswalker spell with the same name as a card in his or her graveyard, that player loses 10 life.').
card_first_print('dragonlord kolaghan', 'DTK').
card_image_name('dragonlord kolaghan'/'DTK', 'dragonlord kolaghan').
card_uid('dragonlord kolaghan'/'DTK', 'DTK:Dragonlord Kolaghan:dragonlord kolaghan').
card_rarity('dragonlord kolaghan'/'DTK', 'Mythic Rare').
card_artist('dragonlord kolaghan'/'DTK', 'Jaime Jones').
card_number('dragonlord kolaghan'/'DTK', '218').
card_multiverse_id('dragonlord kolaghan'/'DTK', '394548').
card_watermark('dragonlord kolaghan'/'DTK', 'Kolaghan').

card_in_set('dragonlord ojutai', 'DTK').
card_original_type('dragonlord ojutai'/'DTK', 'Legendary Creature — Elder Dragon').
card_original_text('dragonlord ojutai'/'DTK', 'Flying\nDragonlord Ojutai has hexproof as long as it\'s untapped.\nWhenever Dragonlord Ojutai deals combat damage to a player, look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_first_print('dragonlord ojutai', 'DTK').
card_image_name('dragonlord ojutai'/'DTK', 'dragonlord ojutai').
card_uid('dragonlord ojutai'/'DTK', 'DTK:Dragonlord Ojutai:dragonlord ojutai').
card_rarity('dragonlord ojutai'/'DTK', 'Mythic Rare').
card_artist('dragonlord ojutai'/'DTK', 'Chase Stone').
card_number('dragonlord ojutai'/'DTK', '219').
card_multiverse_id('dragonlord ojutai'/'DTK', '394549').
card_watermark('dragonlord ojutai'/'DTK', 'Ojutai').

card_in_set('dragonlord silumgar', 'DTK').
card_original_type('dragonlord silumgar'/'DTK', 'Legendary Creature — Elder Dragon').
card_original_text('dragonlord silumgar'/'DTK', 'Flying, deathtouch\nWhen Dragonlord Silumgar enters the battlefield, gain control of target creature or planeswalker for as long as you control Dragonlord Silumgar.').
card_first_print('dragonlord silumgar', 'DTK').
card_image_name('dragonlord silumgar'/'DTK', 'dragonlord silumgar').
card_uid('dragonlord silumgar'/'DTK', 'DTK:Dragonlord Silumgar:dragonlord silumgar').
card_rarity('dragonlord silumgar'/'DTK', 'Mythic Rare').
card_artist('dragonlord silumgar'/'DTK', 'Steven Belledin').
card_number('dragonlord silumgar'/'DTK', '220').
card_flavor_text('dragonlord silumgar'/'DTK', 'Silumgar never passes up an opportunity to add to his opulence.').
card_multiverse_id('dragonlord silumgar'/'DTK', '394550').
card_watermark('dragonlord silumgar'/'DTK', 'Silumgar').

card_in_set('dragonlord\'s prerogative', 'DTK').
card_original_type('dragonlord\'s prerogative'/'DTK', 'Instant').
card_original_text('dragonlord\'s prerogative'/'DTK', 'As an additional cost to cast Dragonlord\'s Prerogative, you may reveal a Dragon card from your hand.\nIf you revealed a Dragon card or controlled a Dragon as you cast Dragonlord\'s Prerogative, Dragonlord\'s Prerogative can\'t be countered.\nDraw four cards.').
card_first_print('dragonlord\'s prerogative', 'DTK').
card_image_name('dragonlord\'s prerogative'/'DTK', 'dragonlord\'s prerogative').
card_uid('dragonlord\'s prerogative'/'DTK', 'DTK:Dragonlord\'s Prerogative:dragonlord\'s prerogative').
card_rarity('dragonlord\'s prerogative'/'DTK', 'Rare').
card_artist('dragonlord\'s prerogative'/'DTK', 'Seb McKinnon').
card_number('dragonlord\'s prerogative'/'DTK', '52').
card_multiverse_id('dragonlord\'s prerogative'/'DTK', '394551').

card_in_set('dragonlord\'s servant', 'DTK').
card_original_type('dragonlord\'s servant'/'DTK', 'Creature — Goblin Shaman').
card_original_text('dragonlord\'s servant'/'DTK', 'Dragon spells you cast cost {1} less to cast.').
card_image_name('dragonlord\'s servant'/'DTK', 'dragonlord\'s servant').
card_uid('dragonlord\'s servant'/'DTK', 'DTK:Dragonlord\'s Servant:dragonlord\'s servant').
card_rarity('dragonlord\'s servant'/'DTK', 'Uncommon').
card_artist('dragonlord\'s servant'/'DTK', 'Steve Prescott').
card_number('dragonlord\'s servant'/'DTK', '138').
card_flavor_text('dragonlord\'s servant'/'DTK', 'Atarka serving-goblins coat themselves with grease imbued with noxious herbs, hoping to discourage their ravenous masters from adding them to the meal.').
card_multiverse_id('dragonlord\'s servant'/'DTK', '394552').
card_watermark('dragonlord\'s servant'/'DTK', 'Atarka').

card_in_set('dromoka captain', 'DTK').
card_original_type('dromoka captain'/'DTK', 'Creature — Human Soldier').
card_original_text('dromoka captain'/'DTK', 'First strike\nWhenever Dromoka Captain attacks, bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('dromoka captain', 'DTK').
card_image_name('dromoka captain'/'DTK', 'dromoka captain').
card_uid('dromoka captain'/'DTK', 'DTK:Dromoka Captain:dromoka captain').
card_rarity('dromoka captain'/'DTK', 'Uncommon').
card_artist('dromoka captain'/'DTK', 'Matt Stewart').
card_number('dromoka captain'/'DTK', '12').
card_flavor_text('dromoka captain'/'DTK', '\"We fight not out of fear, but for honor.\"').
card_multiverse_id('dromoka captain'/'DTK', '394554').
card_watermark('dromoka captain'/'DTK', 'Dromoka').

card_in_set('dromoka dunecaster', 'DTK').
card_original_type('dromoka dunecaster'/'DTK', 'Creature — Human Wizard').
card_original_text('dromoka dunecaster'/'DTK', '{1}{W}, {T}: Tap target creature without flying.').
card_first_print('dromoka dunecaster', 'DTK').
card_image_name('dromoka dunecaster'/'DTK', 'dromoka dunecaster').
card_uid('dromoka dunecaster'/'DTK', 'DTK:Dromoka Dunecaster:dromoka dunecaster').
card_rarity('dromoka dunecaster'/'DTK', 'Common').
card_artist('dromoka dunecaster'/'DTK', 'Mark Winters').
card_number('dromoka dunecaster'/'DTK', '13').
card_flavor_text('dromoka dunecaster'/'DTK', '\"The dragonlords rule the tempests of the skies. Here in the wastes, the storms are mine to command.\"').
card_multiverse_id('dromoka dunecaster'/'DTK', '394555').
card_watermark('dromoka dunecaster'/'DTK', 'Dromoka').

card_in_set('dromoka monument', 'DTK').
card_original_type('dromoka monument'/'DTK', 'Artifact').
card_original_text('dromoka monument'/'DTK', '{T}: Add {G} or {W} to your mana pool.\n{4}{G}{W}: Dromoka Monument becomes a 4/4 green and white Dragon artifact creature with flying until end of turn.').
card_first_print('dromoka monument', 'DTK').
card_image_name('dromoka monument'/'DTK', 'dromoka monument').
card_uid('dromoka monument'/'DTK', 'DTK:Dromoka Monument:dromoka monument').
card_rarity('dromoka monument'/'DTK', 'Uncommon').
card_artist('dromoka monument'/'DTK', 'Daniel Ljunggren').
card_number('dromoka monument'/'DTK', '238').
card_flavor_text('dromoka monument'/'DTK', 'Dromoka rules her clan from the Great Aerie atop Arashin, the central city of the Shifting Wastes.').
card_multiverse_id('dromoka monument'/'DTK', '394556').
card_watermark('dromoka monument'/'DTK', 'Dromoka').

card_in_set('dromoka warrior', 'DTK').
card_original_type('dromoka warrior'/'DTK', 'Creature — Human Warrior').
card_original_text('dromoka warrior'/'DTK', '').
card_first_print('dromoka warrior', 'DTK').
card_image_name('dromoka warrior'/'DTK', 'dromoka warrior').
card_uid('dromoka warrior'/'DTK', 'DTK:Dromoka Warrior:dromoka warrior').
card_rarity('dromoka warrior'/'DTK', 'Common').
card_artist('dromoka warrior'/'DTK', 'Zack Stella').
card_number('dromoka warrior'/'DTK', '14').
card_flavor_text('dromoka warrior'/'DTK', 'Dromoka has regard for the humans who serve under her. In return for her protection, they obey with steadfast loyalty, acting as weapons for her and her scalelords against the other clans.').
card_multiverse_id('dromoka warrior'/'DTK', '394557').
card_watermark('dromoka warrior'/'DTK', 'Dromoka').

card_in_set('dromoka\'s command', 'DTK').
card_original_type('dromoka\'s command'/'DTK', 'Instant').
card_original_text('dromoka\'s command'/'DTK', 'Choose two —\n• Prevent all damage target instant or sorcery spell would deal this turn.\n• Target player sacrifices an enchantment.\n• Put a +1/+1 counter on target creature.\n• Target creature you control fights target creature you don\'t control.').
card_first_print('dromoka\'s command', 'DTK').
card_image_name('dromoka\'s command'/'DTK', 'dromoka\'s command').
card_uid('dromoka\'s command'/'DTK', 'DTK:Dromoka\'s Command:dromoka\'s command').
card_rarity('dromoka\'s command'/'DTK', 'Rare').
card_artist('dromoka\'s command'/'DTK', 'James Ryman').
card_number('dromoka\'s command'/'DTK', '221').
card_multiverse_id('dromoka\'s command'/'DTK', '394558').
card_watermark('dromoka\'s command'/'DTK', 'Dromoka').

card_in_set('dromoka\'s gift', 'DTK').
card_original_type('dromoka\'s gift'/'DTK', 'Instant').
card_original_text('dromoka\'s gift'/'DTK', 'Bolster 4. (Choose a creature with the least toughness among creatures you control and put four +1/+1 counters on it.)').
card_first_print('dromoka\'s gift', 'DTK').
card_image_name('dromoka\'s gift'/'DTK', 'dromoka\'s gift').
card_uid('dromoka\'s gift'/'DTK', 'DTK:Dromoka\'s Gift:dromoka\'s gift').
card_rarity('dromoka\'s gift'/'DTK', 'Uncommon').
card_artist('dromoka\'s gift'/'DTK', 'Winona Nelson').
card_number('dromoka\'s gift'/'DTK', '184').
card_flavor_text('dromoka\'s gift'/'DTK', 'The simplest gift from a dragon can be a revered accolade for a human warrior.').
card_multiverse_id('dromoka\'s gift'/'DTK', '394559').
card_watermark('dromoka\'s gift'/'DTK', 'Dromoka').

card_in_set('duress', 'DTK').
card_original_type('duress'/'DTK', 'Sorcery').
card_original_text('duress'/'DTK', 'Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_image_name('duress'/'DTK', 'duress').
card_uid('duress'/'DTK', 'DTK:Duress:duress').
card_rarity('duress'/'DTK', 'Common').
card_artist('duress'/'DTK', 'Jason Rainville').
card_number('duress'/'DTK', '98').
card_flavor_text('duress'/'DTK', 'Sarkhan was eager to take vengeance on Zurgo until he saw how lowly his old foe had become.').
card_multiverse_id('duress'/'DTK', '394560').

card_in_set('dutiful attendant', 'DTK').
card_original_type('dutiful attendant'/'DTK', 'Creature — Human Warrior').
card_original_text('dutiful attendant'/'DTK', 'When Dutiful Attendant dies, return another target creature card from your graveyard to your hand.').
card_first_print('dutiful attendant', 'DTK').
card_image_name('dutiful attendant'/'DTK', 'dutiful attendant').
card_uid('dutiful attendant'/'DTK', 'DTK:Dutiful Attendant:dutiful attendant').
card_rarity('dutiful attendant'/'DTK', 'Common').
card_artist('dutiful attendant'/'DTK', 'Aaron Miller').
card_number('dutiful attendant'/'DTK', '99').
card_flavor_text('dutiful attendant'/'DTK', 'The dragon who eats the last head in the basket is entitled to the servant\'s.').
card_multiverse_id('dutiful attendant'/'DTK', '394561').
card_watermark('dutiful attendant'/'DTK', 'Silumgar').

card_in_set('echoes of the kin tree', 'DTK').
card_original_type('echoes of the kin tree'/'DTK', 'Enchantment').
card_original_text('echoes of the kin tree'/'DTK', '{2}{W}: Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('echoes of the kin tree', 'DTK').
card_image_name('echoes of the kin tree'/'DTK', 'echoes of the kin tree').
card_uid('echoes of the kin tree'/'DTK', 'DTK:Echoes of the Kin Tree:echoes of the kin tree').
card_rarity('echoes of the kin tree'/'DTK', 'Uncommon').
card_artist('echoes of the kin tree'/'DTK', 'Ryan Alexander Lee').
card_number('echoes of the kin tree'/'DTK', '15').
card_flavor_text('echoes of the kin tree'/'DTK', 'Even after Anafenza was executed, many secretly maintained their beliefs in the old ways.').
card_multiverse_id('echoes of the kin tree'/'DTK', '394562').
card_watermark('echoes of the kin tree'/'DTK', 'Dromoka').

card_in_set('elusive spellfist', 'DTK').
card_original_type('elusive spellfist'/'DTK', 'Creature — Human Monk').
card_original_text('elusive spellfist'/'DTK', 'Whenever you cast a noncreature spell, Elusive Spellfist gets +1/+0 until end of turn and can\'t be blocked this turn.').
card_first_print('elusive spellfist', 'DTK').
card_image_name('elusive spellfist'/'DTK', 'elusive spellfist').
card_uid('elusive spellfist'/'DTK', 'DTK:Elusive Spellfist:elusive spellfist').
card_rarity('elusive spellfist'/'DTK', 'Common').
card_artist('elusive spellfist'/'DTK', 'Viktor Titov').
card_number('elusive spellfist'/'DTK', '53').
card_flavor_text('elusive spellfist'/'DTK', '\"Learn to see an obstacle from a new perspective. Underneath, for example.\"\n—Chanyi, Ojutai monk').
card_multiverse_id('elusive spellfist'/'DTK', '394563').
card_watermark('elusive spellfist'/'DTK', 'Ojutai').

card_in_set('encase in ice', 'DTK').
card_original_type('encase in ice'/'DTK', 'Enchantment — Aura').
card_original_text('encase in ice'/'DTK', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant red or green creature\nWhen Encase in Ice enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_first_print('encase in ice', 'DTK').
card_image_name('encase in ice'/'DTK', 'encase in ice').
card_uid('encase in ice'/'DTK', 'DTK:Encase in Ice:encase in ice').
card_rarity('encase in ice'/'DTK', 'Uncommon').
card_artist('encase in ice'/'DTK', 'Mathias Kollros').
card_number('encase in ice'/'DTK', '54').
card_multiverse_id('encase in ice'/'DTK', '394564').

card_in_set('enduring scalelord', 'DTK').
card_original_type('enduring scalelord'/'DTK', 'Creature — Dragon').
card_original_text('enduring scalelord'/'DTK', 'Flying\nWhenever one or more +1/+1 counters are placed on another creature you control, you may put a +1/+1 counter on Enduring Scalelord.').
card_first_print('enduring scalelord', 'DTK').
card_image_name('enduring scalelord'/'DTK', 'enduring scalelord').
card_uid('enduring scalelord'/'DTK', 'DTK:Enduring Scalelord:enduring scalelord').
card_rarity('enduring scalelord'/'DTK', 'Uncommon').
card_artist('enduring scalelord'/'DTK', 'Clint Cearley').
card_number('enduring scalelord'/'DTK', '222').
card_flavor_text('enduring scalelord'/'DTK', 'Only the sun that beats down upon Arashin\'s walls could shine more brightly.').
card_multiverse_id('enduring scalelord'/'DTK', '394565').
card_watermark('enduring scalelord'/'DTK', 'Dromoka').

card_in_set('enduring victory', 'DTK').
card_original_type('enduring victory'/'DTK', 'Instant').
card_original_text('enduring victory'/'DTK', 'Destroy target attacking or blocking creature. Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('enduring victory', 'DTK').
card_image_name('enduring victory'/'DTK', 'enduring victory').
card_uid('enduring victory'/'DTK', 'DTK:Enduring Victory:enduring victory').
card_rarity('enduring victory'/'DTK', 'Common').
card_artist('enduring victory'/'DTK', 'Mike Sass').
card_number('enduring victory'/'DTK', '16').
card_flavor_text('enduring victory'/'DTK', '\"My dragonlord is immortal. Most dragons are not.\"\n—Kadri, Dromoka warrior').
card_multiverse_id('enduring victory'/'DTK', '394566').
card_watermark('enduring victory'/'DTK', 'Dromoka').

card_in_set('epic confrontation', 'DTK').
card_original_type('epic confrontation'/'DTK', 'Sorcery').
card_original_text('epic confrontation'/'DTK', 'Target creature you control gets +1/+2 until end of turn. It fights target creature you don\'t control. (Each deals damage equal to its power to the other.)').
card_first_print('epic confrontation', 'DTK').
card_image_name('epic confrontation'/'DTK', 'epic confrontation').
card_uid('epic confrontation'/'DTK', 'DTK:Epic Confrontation:epic confrontation').
card_rarity('epic confrontation'/'DTK', 'Common').
card_artist('epic confrontation'/'DTK', 'Wayne Reynolds').
card_number('epic confrontation'/'DTK', '185').
card_flavor_text('epic confrontation'/'DTK', 'No matter the timeline, some legends will endure.').
card_multiverse_id('epic confrontation'/'DTK', '394567').

card_in_set('evolving wilds', 'DTK').
card_original_type('evolving wilds'/'DTK', 'Land').
card_original_text('evolving wilds'/'DTK', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'DTK', 'evolving wilds').
card_uid('evolving wilds'/'DTK', 'DTK:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'DTK', 'Common').
card_artist('evolving wilds'/'DTK', 'Andreas Rocha').
card_number('evolving wilds'/'DTK', '248').
card_flavor_text('evolving wilds'/'DTK', 'The land is ever resilient. Should it die, it will be reborn.').
card_multiverse_id('evolving wilds'/'DTK', '394568').

card_in_set('explosive vegetation', 'DTK').
card_original_type('explosive vegetation'/'DTK', 'Sorcery').
card_original_text('explosive vegetation'/'DTK', 'Search your library for up to two basic land cards and put them onto the battlefield tapped. Then shuffle your library.').
card_image_name('explosive vegetation'/'DTK', 'explosive vegetation').
card_uid('explosive vegetation'/'DTK', 'DTK:Explosive Vegetation:explosive vegetation').
card_rarity('explosive vegetation'/'DTK', 'Uncommon').
card_artist('explosive vegetation'/'DTK', 'Florian de Gesincourt').
card_number('explosive vegetation'/'DTK', '186').
card_flavor_text('explosive vegetation'/'DTK', 'Despite the flames and devastation of the dragons, Tarkir continued to thrive.').
card_multiverse_id('explosive vegetation'/'DTK', '394569').

card_in_set('fate forgotten', 'DTK').
card_original_type('fate forgotten'/'DTK', 'Instant').
card_original_text('fate forgotten'/'DTK', 'Exile target artifact or enchantment.').
card_first_print('fate forgotten', 'DTK').
card_image_name('fate forgotten'/'DTK', 'fate forgotten').
card_uid('fate forgotten'/'DTK', 'DTK:Fate Forgotten:fate forgotten').
card_rarity('fate forgotten'/'DTK', 'Common').
card_artist('fate forgotten'/'DTK', 'Cliff Childs').
card_number('fate forgotten'/'DTK', '17').
card_flavor_text('fate forgotten'/'DTK', 'When Sarkhan saved Ugin in Tarkir\'s past, it changed Tarkir\'s future. The Sultai no longer exist, having been supplanted by the dragonlord Silumgar and his clan.').
card_multiverse_id('fate forgotten'/'DTK', '394570').

card_in_set('flatten', 'DTK').
card_original_type('flatten'/'DTK', 'Instant').
card_original_text('flatten'/'DTK', 'Target creature gets -4/-4 until end of turn.').
card_first_print('flatten', 'DTK').
card_image_name('flatten'/'DTK', 'flatten').
card_uid('flatten'/'DTK', 'DTK:Flatten:flatten').
card_rarity('flatten'/'DTK', 'Common').
card_artist('flatten'/'DTK', 'Raoul Vitale').
card_number('flatten'/'DTK', '100').
card_flavor_text('flatten'/'DTK', 'Like their dragonlord, the Kolaghan take no trophies. They find true fulfillment only in the battle itself, in clash of steel and thunder of hooves.').
card_multiverse_id('flatten'/'DTK', '394571').

card_in_set('foe-razer regent', 'DTK').
card_original_type('foe-razer regent'/'DTK', 'Creature — Dragon').
card_original_text('foe-razer regent'/'DTK', 'Flying\nWhen Foe-Razer Regent enters the battlefield, you may have it fight target creature you don\'t control.\nWhenever a creature you control fights, put two +1/+1 counters on it at the beginning of the next end step.').
card_image_name('foe-razer regent'/'DTK', 'foe-razer regent').
card_uid('foe-razer regent'/'DTK', 'DTK:Foe-Razer Regent:foe-razer regent').
card_rarity('foe-razer regent'/'DTK', 'Rare').
card_artist('foe-razer regent'/'DTK', 'Lucas Graciano').
card_number('foe-razer regent'/'DTK', '187').
card_multiverse_id('foe-razer regent'/'DTK', '394572').
card_watermark('foe-razer regent'/'DTK', 'Atarka').

card_in_set('forest', 'DTK').
card_original_type('forest'/'DTK', 'Basic Land — Forest').
card_original_text('forest'/'DTK', 'G').
card_image_name('forest'/'DTK', 'forest1').
card_uid('forest'/'DTK', 'DTK:Forest:forest1').
card_rarity('forest'/'DTK', 'Basic Land').
card_artist('forest'/'DTK', 'Sam Burley').
card_number('forest'/'DTK', '262').
card_multiverse_id('forest'/'DTK', '394575').

card_in_set('forest', 'DTK').
card_original_type('forest'/'DTK', 'Basic Land — Forest').
card_original_text('forest'/'DTK', 'G').
card_image_name('forest'/'DTK', 'forest2').
card_uid('forest'/'DTK', 'DTK:Forest:forest2').
card_rarity('forest'/'DTK', 'Basic Land').
card_artist('forest'/'DTK', 'Titus Lunter').
card_number('forest'/'DTK', '263').
card_multiverse_id('forest'/'DTK', '394573').

card_in_set('forest', 'DTK').
card_original_type('forest'/'DTK', 'Basic Land — Forest').
card_original_text('forest'/'DTK', 'G').
card_image_name('forest'/'DTK', 'forest3').
card_uid('forest'/'DTK', 'DTK:Forest:forest3').
card_rarity('forest'/'DTK', 'Basic Land').
card_artist('forest'/'DTK', 'Titus Lunter').
card_number('forest'/'DTK', '264').
card_multiverse_id('forest'/'DTK', '394574').

card_in_set('foul renewal', 'DTK').
card_original_type('foul renewal'/'DTK', 'Instant').
card_original_text('foul renewal'/'DTK', 'Return target creature card from your graveyard to your hand. Target creature gets -X/-X until end of turn, where X is the toughness of the card returned this way.').
card_first_print('foul renewal', 'DTK').
card_image_name('foul renewal'/'DTK', 'foul renewal').
card_uid('foul renewal'/'DTK', 'DTK:Foul Renewal:foul renewal').
card_rarity('foul renewal'/'DTK', 'Rare').
card_artist('foul renewal'/'DTK', 'Filip Burburan').
card_number('foul renewal'/'DTK', '101').
card_flavor_text('foul renewal'/'DTK', 'The cycle of life and death is rarely pretty.').
card_multiverse_id('foul renewal'/'DTK', '394576').

card_in_set('foul-tongue invocation', 'DTK').
card_original_type('foul-tongue invocation'/'DTK', 'Instant').
card_original_text('foul-tongue invocation'/'DTK', 'As an additional cost to cast Foul-Tongue Invocation, you may reveal a Dragon card from your hand.\nTarget player sacrifices a creature. If you revealed a Dragon card or controlled a Dragon as you cast Foul-Tongue Invocation, you gain 4 life.').
card_first_print('foul-tongue invocation', 'DTK').
card_image_name('foul-tongue invocation'/'DTK', 'foul-tongue invocation').
card_uid('foul-tongue invocation'/'DTK', 'DTK:Foul-Tongue Invocation:foul-tongue invocation').
card_rarity('foul-tongue invocation'/'DTK', 'Uncommon').
card_artist('foul-tongue invocation'/'DTK', 'Daarken').
card_number('foul-tongue invocation'/'DTK', '102').
card_multiverse_id('foul-tongue invocation'/'DTK', '394577').
card_watermark('foul-tongue invocation'/'DTK', 'Kolaghan').

card_in_set('foul-tongue shriek', 'DTK').
card_original_type('foul-tongue shriek'/'DTK', 'Instant').
card_original_text('foul-tongue shriek'/'DTK', 'Target opponent loses 1 life for each attacking creature you control. You gain that much life.').
card_first_print('foul-tongue shriek', 'DTK').
card_image_name('foul-tongue shriek'/'DTK', 'foul-tongue shriek').
card_uid('foul-tongue shriek'/'DTK', 'DTK:Foul-Tongue Shriek:foul-tongue shriek').
card_rarity('foul-tongue shriek'/'DTK', 'Common').
card_artist('foul-tongue shriek'/'DTK', 'Dave Kendall').
card_number('foul-tongue shriek'/'DTK', '103').
card_flavor_text('foul-tongue shriek'/'DTK', 'Foul-Tongue shamans draw their powers from a dark twisting of the Draconic language.').
card_multiverse_id('foul-tongue shriek'/'DTK', '394578').

card_in_set('gate smasher', 'DTK').
card_original_type('gate smasher'/'DTK', 'Artifact — Equipment').
card_original_text('gate smasher'/'DTK', 'Gate Smasher can be attached only to a creature with toughness 4 or greater.\nEquipped creature gets +3/+0 and has trample.\nEquip {3}').
card_first_print('gate smasher', 'DTK').
card_image_name('gate smasher'/'DTK', 'gate smasher').
card_uid('gate smasher'/'DTK', 'DTK:Gate Smasher:gate smasher').
card_rarity('gate smasher'/'DTK', 'Uncommon').
card_artist('gate smasher'/'DTK', 'Filip Burburan').
card_number('gate smasher'/'DTK', '239').
card_multiverse_id('gate smasher'/'DTK', '394579').

card_in_set('glade watcher', 'DTK').
card_original_type('glade watcher'/'DTK', 'Creature — Elemental').
card_original_text('glade watcher'/'DTK', 'Defender\nFormidable — {G}: Glade Watcher can attack this turn as though it didn\'t have defender. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('glade watcher', 'DTK').
card_image_name('glade watcher'/'DTK', 'glade watcher').
card_uid('glade watcher'/'DTK', 'DTK:Glade Watcher:glade watcher').
card_rarity('glade watcher'/'DTK', 'Common').
card_artist('glade watcher'/'DTK', 'Jesper Ejsing').
card_number('glade watcher'/'DTK', '188').
card_multiverse_id('glade watcher'/'DTK', '394580').
card_watermark('glade watcher'/'DTK', 'Atarka').

card_in_set('glaring aegis', 'DTK').
card_original_type('glaring aegis'/'DTK', 'Enchantment — Aura').
card_original_text('glaring aegis'/'DTK', 'Enchant creature\nWhen Glaring Aegis enters the battlefield, tap target creature an opponent controls.\nEnchanted creature gets +1/+3.').
card_first_print('glaring aegis', 'DTK').
card_image_name('glaring aegis'/'DTK', 'glaring aegis').
card_uid('glaring aegis'/'DTK', 'DTK:Glaring Aegis:glaring aegis').
card_rarity('glaring aegis'/'DTK', 'Common').
card_artist('glaring aegis'/'DTK', 'Anthony Palumbo').
card_number('glaring aegis'/'DTK', '18').
card_multiverse_id('glaring aegis'/'DTK', '394581').

card_in_set('gleam of authority', 'DTK').
card_original_type('gleam of authority'/'DTK', 'Enchantment — Aura').
card_original_text('gleam of authority'/'DTK', 'Enchant creature\nEnchanted creature gets +1/+1 for each +1/+1 counter on other creatures you control.\nEnchanted creature has vigilance and \"{W}, {T}: Bolster 1.\" (To bolster 1, choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('gleam of authority', 'DTK').
card_image_name('gleam of authority'/'DTK', 'gleam of authority').
card_uid('gleam of authority'/'DTK', 'DTK:Gleam of Authority:gleam of authority').
card_rarity('gleam of authority'/'DTK', 'Rare').
card_artist('gleam of authority'/'DTK', 'Jakub Kasper').
card_number('gleam of authority'/'DTK', '19').
card_multiverse_id('gleam of authority'/'DTK', '394582').
card_watermark('gleam of authority'/'DTK', 'Dromoka').

card_in_set('glint', 'DTK').
card_original_type('glint'/'DTK', 'Instant').
card_original_text('glint'/'DTK', 'Target creature you control gets +0/+3 and gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_first_print('glint', 'DTK').
card_image_name('glint'/'DTK', 'glint').
card_uid('glint'/'DTK', 'DTK:Glint:glint').
card_rarity('glint'/'DTK', 'Common').
card_artist('glint'/'DTK', 'Igor Kieryluk').
card_number('glint'/'DTK', '55').
card_flavor_text('glint'/'DTK', 'Rakshasa waste no opportunity to display their wealth and power, even in the midst of a sorcerous duel.').
card_multiverse_id('glint'/'DTK', '394583').

card_in_set('graceblade artisan', 'DTK').
card_original_type('graceblade artisan'/'DTK', 'Creature — Human Monk').
card_original_text('graceblade artisan'/'DTK', 'Graceblade Artisan gets +2/+2 for each Aura attached to it.').
card_first_print('graceblade artisan', 'DTK').
card_image_name('graceblade artisan'/'DTK', 'graceblade artisan').
card_uid('graceblade artisan'/'DTK', 'DTK:Graceblade Artisan:graceblade artisan').
card_rarity('graceblade artisan'/'DTK', 'Uncommon').
card_artist('graceblade artisan'/'DTK', 'Magali Villeneuve').
card_number('graceblade artisan'/'DTK', '20').
card_flavor_text('graceblade artisan'/'DTK', '\"Can you catch a snowflake on the edge of your blade? Her sword has snagged an entire blizzard.\"\n—Zogye, Ojutai mystic').
card_multiverse_id('graceblade artisan'/'DTK', '394584').
card_watermark('graceblade artisan'/'DTK', 'Ojutai').

card_in_set('gravepurge', 'DTK').
card_original_type('gravepurge'/'DTK', 'Instant').
card_original_text('gravepurge'/'DTK', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card.').
card_image_name('gravepurge'/'DTK', 'gravepurge').
card_uid('gravepurge'/'DTK', 'DTK:Gravepurge:gravepurge').
card_rarity('gravepurge'/'DTK', 'Common').
card_artist('gravepurge'/'DTK', 'Nils Hamm').
card_number('gravepurge'/'DTK', '104').
card_flavor_text('gravepurge'/'DTK', '\"Lord Silumgar has given you a second chance to please him.\"\n—Siara, the Dragon\'s Mouth').
card_multiverse_id('gravepurge'/'DTK', '394585').

card_in_set('great teacher\'s decree', 'DTK').
card_original_type('great teacher\'s decree'/'DTK', 'Sorcery').
card_original_text('great teacher\'s decree'/'DTK', 'Creatures you control get +2/+1 until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('great teacher\'s decree', 'DTK').
card_image_name('great teacher\'s decree'/'DTK', 'great teacher\'s decree').
card_uid('great teacher\'s decree'/'DTK', 'DTK:Great Teacher\'s Decree:great teacher\'s decree').
card_rarity('great teacher\'s decree'/'DTK', 'Uncommon').
card_artist('great teacher\'s decree'/'DTK', 'Zoltan Boros').
card_number('great teacher\'s decree'/'DTK', '21').
card_multiverse_id('great teacher\'s decree'/'DTK', '394586').
card_watermark('great teacher\'s decree'/'DTK', 'Ojutai').

card_in_set('guardian shield-bearer', 'DTK').
card_original_type('guardian shield-bearer'/'DTK', 'Creature — Human Soldier').
card_original_text('guardian shield-bearer'/'DTK', 'Megamorph {3}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Guardian Shield-Bearer is turned face up, put a +1/+1 counter on another target creature you control.').
card_first_print('guardian shield-bearer', 'DTK').
card_image_name('guardian shield-bearer'/'DTK', 'guardian shield-bearer').
card_uid('guardian shield-bearer'/'DTK', 'DTK:Guardian Shield-Bearer:guardian shield-bearer').
card_rarity('guardian shield-bearer'/'DTK', 'Common').
card_artist('guardian shield-bearer'/'DTK', 'Lindsey Look').
card_number('guardian shield-bearer'/'DTK', '189').
card_multiverse_id('guardian shield-bearer'/'DTK', '394587').
card_watermark('guardian shield-bearer'/'DTK', 'Dromoka').

card_in_set('gudul lurker', 'DTK').
card_original_type('gudul lurker'/'DTK', 'Creature — Salamander').
card_original_text('gudul lurker'/'DTK', 'Gudul Lurker can\'t be blocked.\nMegamorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('gudul lurker', 'DTK').
card_image_name('gudul lurker'/'DTK', 'gudul lurker').
card_uid('gudul lurker'/'DTK', 'DTK:Gudul Lurker:gudul lurker').
card_rarity('gudul lurker'/'DTK', 'Uncommon').
card_artist('gudul lurker'/'DTK', 'Christopher Burdett').
card_number('gudul lurker'/'DTK', '56').
card_flavor_text('gudul lurker'/'DTK', 'The small are mostly ignored by dragons.').
card_multiverse_id('gudul lurker'/'DTK', '394588').

card_in_set('gurmag drowner', 'DTK').
card_original_type('gurmag drowner'/'DTK', 'Creature — Naga Wizard').
card_original_text('gurmag drowner'/'DTK', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Gurmag Drowner exploits a creature, look at the top four cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_first_print('gurmag drowner', 'DTK').
card_image_name('gurmag drowner'/'DTK', 'gurmag drowner').
card_uid('gurmag drowner'/'DTK', 'DTK:Gurmag Drowner:gurmag drowner').
card_rarity('gurmag drowner'/'DTK', 'Common').
card_artist('gurmag drowner'/'DTK', 'Lake Hurwitz').
card_number('gurmag drowner'/'DTK', '57').
card_multiverse_id('gurmag drowner'/'DTK', '394589').
card_watermark('gurmag drowner'/'DTK', 'Silumgar').

card_in_set('hand of silumgar', 'DTK').
card_original_type('hand of silumgar'/'DTK', 'Creature — Human Warrior').
card_original_text('hand of silumgar'/'DTK', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_first_print('hand of silumgar', 'DTK').
card_image_name('hand of silumgar'/'DTK', 'hand of silumgar').
card_uid('hand of silumgar'/'DTK', 'DTK:Hand of Silumgar:hand of silumgar').
card_rarity('hand of silumgar'/'DTK', 'Common').
card_artist('hand of silumgar'/'DTK', 'Lius Lasahido').
card_number('hand of silumgar'/'DTK', '105').
card_flavor_text('hand of silumgar'/'DTK', 'Silumgar trains those whom he favors in his magic, granting them the ability to spread his disdain across the land.').
card_multiverse_id('hand of silumgar'/'DTK', '394590').
card_watermark('hand of silumgar'/'DTK', 'Silumgar').

card_in_set('harbinger of the hunt', 'DTK').
card_original_type('harbinger of the hunt'/'DTK', 'Creature — Dragon').
card_original_text('harbinger of the hunt'/'DTK', 'Flying\n{2}{R}: Harbinger of the Hunt deals 1 damage to each creature without flying.\n{2}{G}: Harbinger of the Hunt deals 1 damage to each other creature with flying.').
card_image_name('harbinger of the hunt'/'DTK', 'harbinger of the hunt').
card_uid('harbinger of the hunt'/'DTK', 'DTK:Harbinger of the Hunt:harbinger of the hunt').
card_rarity('harbinger of the hunt'/'DTK', 'Rare').
card_artist('harbinger of the hunt'/'DTK', 'Aaron Miller').
card_number('harbinger of the hunt'/'DTK', '223').
card_flavor_text('harbinger of the hunt'/'DTK', 'An Atarka dragon\'s exhale cooks what its inhale consumes.').
card_multiverse_id('harbinger of the hunt'/'DTK', '394591').
card_watermark('harbinger of the hunt'/'DTK', 'Atarka').

card_in_set('hardened berserker', 'DTK').
card_original_type('hardened berserker'/'DTK', 'Creature — Human Berserker').
card_original_text('hardened berserker'/'DTK', 'Whenever Hardened Berserker attacks, the next spell you cast this turn costs {1} less to cast.').
card_first_print('hardened berserker', 'DTK').
card_image_name('hardened berserker'/'DTK', 'hardened berserker').
card_uid('hardened berserker'/'DTK', 'DTK:Hardened Berserker:hardened berserker').
card_rarity('hardened berserker'/'DTK', 'Common').
card_artist('hardened berserker'/'DTK', 'Viktor Titov').
card_number('hardened berserker'/'DTK', '139').
card_flavor_text('hardened berserker'/'DTK', '\"Just let him loose and follow the charge.\"\n—Yikaro, Atarka warrior').
card_multiverse_id('hardened berserker'/'DTK', '394592').
card_watermark('hardened berserker'/'DTK', 'Atarka').

card_in_set('haven of the spirit dragon', 'DTK').
card_original_type('haven of the spirit dragon'/'DTK', 'Land').
card_original_text('haven of the spirit dragon'/'DTK', '{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a Dragon creature spell.\n{2}, {T}, Sacrifice Haven of the Spirit Dragon: Return target Dragon creature card or Ugin planeswalker card from your graveyard to your hand.').
card_first_print('haven of the spirit dragon', 'DTK').
card_image_name('haven of the spirit dragon'/'DTK', 'haven of the spirit dragon').
card_uid('haven of the spirit dragon'/'DTK', 'DTK:Haven of the Spirit Dragon:haven of the spirit dragon').
card_rarity('haven of the spirit dragon'/'DTK', 'Rare').
card_artist('haven of the spirit dragon'/'DTK', 'Raymond Swanland').
card_number('haven of the spirit dragon'/'DTK', '249').
card_multiverse_id('haven of the spirit dragon'/'DTK', '394593').

card_in_set('hedonist\'s trove', 'DTK').
card_original_type('hedonist\'s trove'/'DTK', 'Enchantment').
card_original_text('hedonist\'s trove'/'DTK', 'When Hedonist\'s Trove enters the battlefield, exile all cards from target opponent\'s graveyard.\nYou may play land cards exiled with Hedonist\'s Trove.\nYou may cast nonland cards exiled with Hedonist\'s Trove. You can\'t cast more than one spell this way each turn.').
card_first_print('hedonist\'s trove', 'DTK').
card_image_name('hedonist\'s trove'/'DTK', 'hedonist\'s trove').
card_uid('hedonist\'s trove'/'DTK', 'DTK:Hedonist\'s Trove:hedonist\'s trove').
card_rarity('hedonist\'s trove'/'DTK', 'Rare').
card_artist('hedonist\'s trove'/'DTK', 'Peter Mohrbacher').
card_number('hedonist\'s trove'/'DTK', '106').
card_multiverse_id('hedonist\'s trove'/'DTK', '394594').

card_in_set('herald of dromoka', 'DTK').
card_original_type('herald of dromoka'/'DTK', 'Creature — Human Warrior').
card_original_text('herald of dromoka'/'DTK', 'Vigilance\nOther Warrior creatures you control have vigilance.').
card_first_print('herald of dromoka', 'DTK').
card_image_name('herald of dromoka'/'DTK', 'herald of dromoka').
card_uid('herald of dromoka'/'DTK', 'DTK:Herald of Dromoka:herald of dromoka').
card_rarity('herald of dromoka'/'DTK', 'Common').
card_artist('herald of dromoka'/'DTK', 'Zack Stella').
card_number('herald of dromoka'/'DTK', '22').
card_flavor_text('herald of dromoka'/'DTK', 'The trumpeters of Arashin are ever alert in their watch over the Great Aerie.').
card_multiverse_id('herald of dromoka'/'DTK', '394595').
card_watermark('herald of dromoka'/'DTK', 'Dromoka').

card_in_set('herdchaser dragon', 'DTK').
card_original_type('herdchaser dragon'/'DTK', 'Creature — Dragon').
card_original_text('herdchaser dragon'/'DTK', 'Flying, trample\nMegamorph {5}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Herdchaser Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_first_print('herdchaser dragon', 'DTK').
card_image_name('herdchaser dragon'/'DTK', 'herdchaser dragon').
card_uid('herdchaser dragon'/'DTK', 'DTK:Herdchaser Dragon:herdchaser dragon').
card_rarity('herdchaser dragon'/'DTK', 'Uncommon').
card_artist('herdchaser dragon'/'DTK', 'Seb McKinnon').
card_number('herdchaser dragon'/'DTK', '190').
card_multiverse_id('herdchaser dragon'/'DTK', '394596').
card_watermark('herdchaser dragon'/'DTK', 'Atarka').

card_in_set('hidden dragonslayer', 'DTK').
card_original_type('hidden dragonslayer'/'DTK', 'Creature — Human Warrior').
card_original_text('hidden dragonslayer'/'DTK', 'Lifelink\nMegamorph {2}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Hidden Dragonslayer is turned face up, destroy target creature with power 4 or greater an opponent controls.').
card_first_print('hidden dragonslayer', 'DTK').
card_image_name('hidden dragonslayer'/'DTK', 'hidden dragonslayer').
card_uid('hidden dragonslayer'/'DTK', 'DTK:Hidden Dragonslayer:hidden dragonslayer').
card_rarity('hidden dragonslayer'/'DTK', 'Rare').
card_artist('hidden dragonslayer'/'DTK', 'Anastasia Ovchinnikova').
card_number('hidden dragonslayer'/'DTK', '23').
card_multiverse_id('hidden dragonslayer'/'DTK', '394597').
card_watermark('hidden dragonslayer'/'DTK', 'Dromoka').

card_in_set('icefall regent', 'DTK').
card_original_type('icefall regent'/'DTK', 'Creature — Dragon').
card_original_text('icefall regent'/'DTK', 'Flying\nWhen Icefall Regent enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s untap step for as long as you control Icefall Regent. \nSpells your opponents cast that target Icefall Regent cost {2} more to cast.').
card_first_print('icefall regent', 'DTK').
card_image_name('icefall regent'/'DTK', 'icefall regent').
card_uid('icefall regent'/'DTK', 'DTK:Icefall Regent:icefall regent').
card_rarity('icefall regent'/'DTK', 'Rare').
card_artist('icefall regent'/'DTK', 'David Gaillet').
card_number('icefall regent'/'DTK', '58').
card_multiverse_id('icefall regent'/'DTK', '394598').
card_watermark('icefall regent'/'DTK', 'Ojutai').

card_in_set('illusory gains', 'DTK').
card_original_type('illusory gains'/'DTK', 'Enchantment — Aura').
card_original_text('illusory gains'/'DTK', 'Enchant creature\nYou control enchanted creature.\nWhenever a creature enters the battlefield under an opponent\'s control, attach Illusory Gains to that creature.').
card_first_print('illusory gains', 'DTK').
card_image_name('illusory gains'/'DTK', 'illusory gains').
card_uid('illusory gains'/'DTK', 'DTK:Illusory Gains:illusory gains').
card_rarity('illusory gains'/'DTK', 'Rare').
card_artist('illusory gains'/'DTK', 'Kev Walker').
card_number('illusory gains'/'DTK', '59').
card_multiverse_id('illusory gains'/'DTK', '394599').

card_in_set('impact tremors', 'DTK').
card_original_type('impact tremors'/'DTK', 'Enchantment').
card_original_text('impact tremors'/'DTK', 'Whenever a creature enters the battlefield under your control, Impact Tremors deals 1 damage to each opponent.').
card_first_print('impact tremors', 'DTK').
card_image_name('impact tremors'/'DTK', 'impact tremors').
card_uid('impact tremors'/'DTK', 'DTK:Impact Tremors:impact tremors').
card_rarity('impact tremors'/'DTK', 'Common').
card_artist('impact tremors'/'DTK', 'Lake Hurwitz').
card_number('impact tremors'/'DTK', '140').
card_flavor_text('impact tremors'/'DTK', '\"If their ears are to the ground, we will make them bleed!\"\n—Taklai, Kolaghan warrior').
card_multiverse_id('impact tremors'/'DTK', '394600').

card_in_set('inspiring call', 'DTK').
card_original_type('inspiring call'/'DTK', 'Instant').
card_original_text('inspiring call'/'DTK', 'Draw a card for each creature you control with a +1/+1 counter on it. Those creatures gain indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy them.)').
card_first_print('inspiring call', 'DTK').
card_image_name('inspiring call'/'DTK', 'inspiring call').
card_uid('inspiring call'/'DTK', 'DTK:Inspiring Call:inspiring call').
card_rarity('inspiring call'/'DTK', 'Uncommon').
card_artist('inspiring call'/'DTK', 'Dan Scott').
card_number('inspiring call'/'DTK', '191').
card_multiverse_id('inspiring call'/'DTK', '394601').
card_watermark('inspiring call'/'DTK', 'Dromoka').

card_in_set('ire shaman', 'DTK').
card_original_type('ire shaman'/'DTK', 'Creature — Orc Shaman').
card_original_text('ire shaman'/'DTK', 'Ire Shaman can\'t be blocked except by two or more creatures.\nMegamorph {R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Ire Shaman is turned face up, exile the top card of your library. Until end of turn, you may play that card.').
card_first_print('ire shaman', 'DTK').
card_image_name('ire shaman'/'DTK', 'ire shaman').
card_uid('ire shaman'/'DTK', 'DTK:Ire Shaman:ire shaman').
card_rarity('ire shaman'/'DTK', 'Rare').
card_artist('ire shaman'/'DTK', 'Jack Wang').
card_number('ire shaman'/'DTK', '141').
card_multiverse_id('ire shaman'/'DTK', '394602').
card_watermark('ire shaman'/'DTK', 'Kolaghan').

card_in_set('island', 'DTK').
card_original_type('island'/'DTK', 'Basic Land — Island').
card_original_text('island'/'DTK', 'U').
card_image_name('island'/'DTK', 'island1').
card_uid('island'/'DTK', 'DTK:Island:island1').
card_rarity('island'/'DTK', 'Basic Land').
card_artist('island'/'DTK', 'Florian de Gesincourt').
card_number('island'/'DTK', '253').
card_multiverse_id('island'/'DTK', '394605').

card_in_set('island', 'DTK').
card_original_type('island'/'DTK', 'Basic Land — Island').
card_original_text('island'/'DTK', 'U').
card_image_name('island'/'DTK', 'island2').
card_uid('island'/'DTK', 'DTK:Island:island2').
card_rarity('island'/'DTK', 'Basic Land').
card_artist('island'/'DTK', 'Florian de Gesincourt').
card_number('island'/'DTK', '254').
card_multiverse_id('island'/'DTK', '394603').

card_in_set('island', 'DTK').
card_original_type('island'/'DTK', 'Basic Land — Island').
card_original_text('island'/'DTK', 'U').
card_image_name('island'/'DTK', 'island3').
card_uid('island'/'DTK', 'DTK:Island:island3').
card_rarity('island'/'DTK', 'Basic Land').
card_artist('island'/'DTK', 'Adam Paquette').
card_number('island'/'DTK', '255').
card_multiverse_id('island'/'DTK', '394604').

card_in_set('keeper of the lens', 'DTK').
card_original_type('keeper of the lens'/'DTK', 'Artifact Creature — Golem').
card_original_text('keeper of the lens'/'DTK', 'You may look at face-down creatures you don\'t control. (You may do this at any time.)').
card_first_print('keeper of the lens', 'DTK').
card_image_name('keeper of the lens'/'DTK', 'keeper of the lens').
card_uid('keeper of the lens'/'DTK', 'DTK:Keeper of the Lens:keeper of the lens').
card_rarity('keeper of the lens'/'DTK', 'Common').
card_artist('keeper of the lens'/'DTK', 'Ryan Barger').
card_number('keeper of the lens'/'DTK', '240').
card_flavor_text('keeper of the lens'/'DTK', '\"It sees all, but it reveals what it sees only to a chosen few.\"\n—Taigam, Ojutai master').
card_multiverse_id('keeper of the lens'/'DTK', '394606').

card_in_set('kindled fury', 'DTK').
card_original_type('kindled fury'/'DTK', 'Instant').
card_original_text('kindled fury'/'DTK', 'Target creature gets +1/+0 and gains first strike until end of turn.').
card_image_name('kindled fury'/'DTK', 'kindled fury').
card_uid('kindled fury'/'DTK', 'DTK:Kindled Fury:kindled fury').
card_rarity('kindled fury'/'DTK', 'Common').
card_artist('kindled fury'/'DTK', 'Dan Scott').
card_number('kindled fury'/'DTK', '142').
card_flavor_text('kindled fury'/'DTK', '\"It is most honorable to use every part of the animals we kill . . . especially if we use them to annihilate our enemies.\"\n—Surrak, the Hunt Caller').
card_multiverse_id('kindled fury'/'DTK', '394607').

card_in_set('kolaghan aspirant', 'DTK').
card_original_type('kolaghan aspirant'/'DTK', 'Creature — Human Warrior').
card_original_text('kolaghan aspirant'/'DTK', 'Whenever Kolaghan Aspirant becomes blocked by a creature, Kolaghan Aspirant deals 1 damage to that creature.').
card_first_print('kolaghan aspirant', 'DTK').
card_image_name('kolaghan aspirant'/'DTK', 'kolaghan aspirant').
card_uid('kolaghan aspirant'/'DTK', 'DTK:Kolaghan Aspirant:kolaghan aspirant').
card_rarity('kolaghan aspirant'/'DTK', 'Common').
card_artist('kolaghan aspirant'/'DTK', 'Aaron Miller').
card_number('kolaghan aspirant'/'DTK', '143').
card_flavor_text('kolaghan aspirant'/'DTK', 'She answers the call of the Crave, the desire for battle sated only by bloodshed.').
card_multiverse_id('kolaghan aspirant'/'DTK', '394608').
card_watermark('kolaghan aspirant'/'DTK', 'Kolaghan').

card_in_set('kolaghan forerunners', 'DTK').
card_original_type('kolaghan forerunners'/'DTK', 'Creature — Human Berserker').
card_original_text('kolaghan forerunners'/'DTK', 'Trample\nKolaghan Forerunners\'s power is equal to the number of creatures you control.\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('kolaghan forerunners', 'DTK').
card_image_name('kolaghan forerunners'/'DTK', 'kolaghan forerunners').
card_uid('kolaghan forerunners'/'DTK', 'DTK:Kolaghan Forerunners:kolaghan forerunners').
card_rarity('kolaghan forerunners'/'DTK', 'Uncommon').
card_artist('kolaghan forerunners'/'DTK', 'Jason A. Engle').
card_number('kolaghan forerunners'/'DTK', '144').
card_multiverse_id('kolaghan forerunners'/'DTK', '394609').
card_watermark('kolaghan forerunners'/'DTK', 'Kolaghan').

card_in_set('kolaghan monument', 'DTK').
card_original_type('kolaghan monument'/'DTK', 'Artifact').
card_original_text('kolaghan monument'/'DTK', '{T}: Add {B} or {R} to your mana pool.\n{4}{B}{R}: Kolaghan Monument becomes a 4/4 black and red Dragon artifact creature with flying until end of turn.').
card_first_print('kolaghan monument', 'DTK').
card_image_name('kolaghan monument'/'DTK', 'kolaghan monument').
card_uid('kolaghan monument'/'DTK', 'DTK:Kolaghan Monument:kolaghan monument').
card_rarity('kolaghan monument'/'DTK', 'Uncommon').
card_artist('kolaghan monument'/'DTK', 'Daniel Ljunggren').
card_number('kolaghan monument'/'DTK', '241').
card_flavor_text('kolaghan monument'/'DTK', 'Kolaghan has no lasting home, but her disciples leave relics to mark her conquests.').
card_multiverse_id('kolaghan monument'/'DTK', '394610').
card_watermark('kolaghan monument'/'DTK', 'Kolaghan').

card_in_set('kolaghan skirmisher', 'DTK').
card_original_type('kolaghan skirmisher'/'DTK', 'Creature — Human Warrior').
card_original_text('kolaghan skirmisher'/'DTK', 'Dash {2}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('kolaghan skirmisher', 'DTK').
card_image_name('kolaghan skirmisher'/'DTK', 'kolaghan skirmisher').
card_uid('kolaghan skirmisher'/'DTK', 'DTK:Kolaghan Skirmisher:kolaghan skirmisher').
card_rarity('kolaghan skirmisher'/'DTK', 'Common').
card_artist('kolaghan skirmisher'/'DTK', 'Anthony Palumbo').
card_number('kolaghan skirmisher'/'DTK', '107').
card_flavor_text('kolaghan skirmisher'/'DTK', 'Kolaghan\'s army rushes from kill to kill, desperate to avoid the dragon\'s wrath.').
card_multiverse_id('kolaghan skirmisher'/'DTK', '394611').
card_watermark('kolaghan skirmisher'/'DTK', 'Kolaghan').

card_in_set('kolaghan stormsinger', 'DTK').
card_original_type('kolaghan stormsinger'/'DTK', 'Creature — Human Shaman').
card_original_text('kolaghan stormsinger'/'DTK', 'Haste\nMegamorph {R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Kolaghan Stormsinger is turned face up, target creature gains haste until end of turn.').
card_first_print('kolaghan stormsinger', 'DTK').
card_image_name('kolaghan stormsinger'/'DTK', 'kolaghan stormsinger').
card_uid('kolaghan stormsinger'/'DTK', 'DTK:Kolaghan Stormsinger:kolaghan stormsinger').
card_rarity('kolaghan stormsinger'/'DTK', 'Common').
card_artist('kolaghan stormsinger'/'DTK', 'Scott Murphy').
card_number('kolaghan stormsinger'/'DTK', '145').
card_multiverse_id('kolaghan stormsinger'/'DTK', '394612').
card_watermark('kolaghan stormsinger'/'DTK', 'Kolaghan').

card_in_set('kolaghan\'s command', 'DTK').
card_original_type('kolaghan\'s command'/'DTK', 'Instant').
card_original_text('kolaghan\'s command'/'DTK', 'Choose two —\n• Return target creature card from your graveyard to your hand.\n• Target player discards a card.\n• Destroy target artifact.\n• Kolaghan\'s Command deals 2 damage to target creature or player.').
card_first_print('kolaghan\'s command', 'DTK').
card_image_name('kolaghan\'s command'/'DTK', 'kolaghan\'s command').
card_uid('kolaghan\'s command'/'DTK', 'DTK:Kolaghan\'s Command:kolaghan\'s command').
card_rarity('kolaghan\'s command'/'DTK', 'Rare').
card_artist('kolaghan\'s command'/'DTK', 'Daarken').
card_number('kolaghan\'s command'/'DTK', '224').
card_multiverse_id('kolaghan\'s command'/'DTK', '394613').
card_watermark('kolaghan\'s command'/'DTK', 'Kolaghan').

card_in_set('learn from the past', 'DTK').
card_original_type('learn from the past'/'DTK', 'Instant').
card_original_text('learn from the past'/'DTK', 'Target player shuffles his or her graveyard into his or her library.\nDraw a card.').
card_first_print('learn from the past', 'DTK').
card_image_name('learn from the past'/'DTK', 'learn from the past').
card_uid('learn from the past'/'DTK', 'DTK:Learn from the Past:learn from the past').
card_rarity('learn from the past'/'DTK', 'Uncommon').
card_artist('learn from the past'/'DTK', 'Chase Stone').
card_number('learn from the past'/'DTK', '60').
card_flavor_text('learn from the past'/'DTK', '\"I\'ve been told my whole life that the great Ojutai holds all knowledge, but I wonder what our forebears knew.\"\n—Narset').
card_multiverse_id('learn from the past'/'DTK', '394614').

card_in_set('lightning berserker', 'DTK').
card_original_type('lightning berserker'/'DTK', 'Creature — Human Berserker').
card_original_text('lightning berserker'/'DTK', '{R}: Lightning Berserker gets +1/+0 until end of turn.\nDash {R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('lightning berserker', 'DTK').
card_image_name('lightning berserker'/'DTK', 'lightning berserker').
card_uid('lightning berserker'/'DTK', 'DTK:Lightning Berserker:lightning berserker').
card_rarity('lightning berserker'/'DTK', 'Uncommon').
card_artist('lightning berserker'/'DTK', 'Joseph Meehan').
card_number('lightning berserker'/'DTK', '146').
card_multiverse_id('lightning berserker'/'DTK', '394615').
card_watermark('lightning berserker'/'DTK', 'Kolaghan').

card_in_set('lightwalker', 'DTK').
card_original_type('lightwalker'/'DTK', 'Creature — Human Warrior').
card_original_text('lightwalker'/'DTK', 'Lightwalker has flying as long as it has a +1/+1 counter on it.').
card_first_print('lightwalker', 'DTK').
card_image_name('lightwalker'/'DTK', 'lightwalker').
card_uid('lightwalker'/'DTK', 'DTK:Lightwalker:lightwalker').
card_rarity('lightwalker'/'DTK', 'Common').
card_artist('lightwalker'/'DTK', 'Winona Nelson').
card_number('lightwalker'/'DTK', '24').
card_flavor_text('lightwalker'/'DTK', '\"The greatest gift Dromoka gives is the ability to fly without wings.\"\n—Urdnan, Dromoka warrior').
card_multiverse_id('lightwalker'/'DTK', '394616').
card_watermark('lightwalker'/'DTK', 'Dromoka').

card_in_set('living lore', 'DTK').
card_original_type('living lore'/'DTK', 'Creature — Avatar').
card_original_text('living lore'/'DTK', 'As Living Lore enters the battlefield, exile an instant or sorcery card from your graveyard.\nLiving Lore\'s power and toughness are each equal to the exiled card\'s converted mana cost.\nWhenever Living Lore deals combat damage, you may sacrifice it. If you do, you may cast the exiled card without paying its mana cost.').
card_first_print('living lore', 'DTK').
card_image_name('living lore'/'DTK', 'living lore').
card_uid('living lore'/'DTK', 'DTK:Living Lore:living lore').
card_rarity('living lore'/'DTK', 'Rare').
card_artist('living lore'/'DTK', 'Jason Felix').
card_number('living lore'/'DTK', '61').
card_multiverse_id('living lore'/'DTK', '394617').
card_watermark('living lore'/'DTK', 'Ojutai').

card_in_set('lose calm', 'DTK').
card_original_type('lose calm'/'DTK', 'Sorcery').
card_original_text('lose calm'/'DTK', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn and can\'t be blocked this turn except by two or more creatures.').
card_first_print('lose calm', 'DTK').
card_image_name('lose calm'/'DTK', 'lose calm').
card_uid('lose calm'/'DTK', 'DTK:Lose Calm:lose calm').
card_rarity('lose calm'/'DTK', 'Common').
card_artist('lose calm'/'DTK', 'Jason A. Engle').
card_number('lose calm'/'DTK', '147').
card_flavor_text('lose calm'/'DTK', 'A lifetime of discipline forsaken in a moment of rage.').
card_multiverse_id('lose calm'/'DTK', '394618').

card_in_set('lurking arynx', 'DTK').
card_original_type('lurking arynx'/'DTK', 'Creature — Cat Beast').
card_original_text('lurking arynx'/'DTK', 'Formidable — {2}{G}: Target creature blocks Lurking Arynx this turn if able. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('lurking arynx', 'DTK').
card_image_name('lurking arynx'/'DTK', 'lurking arynx').
card_uid('lurking arynx'/'DTK', 'DTK:Lurking Arynx:lurking arynx').
card_rarity('lurking arynx'/'DTK', 'Uncommon').
card_artist('lurking arynx'/'DTK', 'Carl Frank').
card_number('lurking arynx'/'DTK', '192').
card_flavor_text('lurking arynx'/'DTK', 'Once it has your scent, it will hunt you from the Gurmag Swamp to Ayagor.').
card_multiverse_id('lurking arynx'/'DTK', '394619').
card_watermark('lurking arynx'/'DTK', 'Atarka').

card_in_set('magmatic chasm', 'DTK').
card_original_type('magmatic chasm'/'DTK', 'Sorcery').
card_original_text('magmatic chasm'/'DTK', 'Creatures without flying can\'t block this turn.').
card_first_print('magmatic chasm', 'DTK').
card_image_name('magmatic chasm'/'DTK', 'magmatic chasm').
card_uid('magmatic chasm'/'DTK', 'DTK:Magmatic Chasm:magmatic chasm').
card_rarity('magmatic chasm'/'DTK', 'Common').
card_artist('magmatic chasm'/'DTK', 'Volkan Baga').
card_number('magmatic chasm'/'DTK', '148').
card_flavor_text('magmatic chasm'/'DTK', '\"Our shamans assert their mastery over the land, thwarting the lesser clans\' attempts to stand in the way of our dragonlord.\"\n—Allek, Atarka hunter').
card_multiverse_id('magmatic chasm'/'DTK', '394620').

card_in_set('marang river skeleton', 'DTK').
card_original_type('marang river skeleton'/'DTK', 'Creature — Skeleton').
card_original_text('marang river skeleton'/'DTK', '{B}: Regenerate Marang River Skeleton.\nMegamorph {3}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('marang river skeleton', 'DTK').
card_image_name('marang river skeleton'/'DTK', 'marang river skeleton').
card_uid('marang river skeleton'/'DTK', 'DTK:Marang River Skeleton:marang river skeleton').
card_rarity('marang river skeleton'/'DTK', 'Uncommon').
card_artist('marang river skeleton'/'DTK', 'Jack Wang').
card_number('marang river skeleton'/'DTK', '108').
card_flavor_text('marang river skeleton'/'DTK', 'The gurgling of the Marang conceals both footsteps and screams.').
card_multiverse_id('marang river skeleton'/'DTK', '394621').
card_watermark('marang river skeleton'/'DTK', 'Silumgar').

card_in_set('marsh hulk', 'DTK').
card_original_type('marsh hulk'/'DTK', 'Creature — Zombie Ogre').
card_original_text('marsh hulk'/'DTK', 'Megamorph {6}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('marsh hulk', 'DTK').
card_image_name('marsh hulk'/'DTK', 'marsh hulk').
card_uid('marsh hulk'/'DTK', 'DTK:Marsh Hulk:marsh hulk').
card_rarity('marsh hulk'/'DTK', 'Common').
card_artist('marsh hulk'/'DTK', 'Raf Sarmento').
card_number('marsh hulk'/'DTK', '109').
card_flavor_text('marsh hulk'/'DTK', 'Now vengeance is his sole purpose.').
card_multiverse_id('marsh hulk'/'DTK', '394622').

card_in_set('mind rot', 'DTK').
card_original_type('mind rot'/'DTK', 'Sorcery').
card_original_text('mind rot'/'DTK', 'Target player discards two cards.').
card_image_name('mind rot'/'DTK', 'mind rot').
card_uid('mind rot'/'DTK', 'DTK:Mind Rot:mind rot').
card_rarity('mind rot'/'DTK', 'Common').
card_artist('mind rot'/'DTK', 'Clint Cearley').
card_number('mind rot'/'DTK', '110').
card_flavor_text('mind rot'/'DTK', '\"The Ojutai are my favorite victims. They have so much knowledge to lose.\"\n—Asmala, Silumgar sorcerer').
card_multiverse_id('mind rot'/'DTK', '394623').

card_in_set('minister of pain', 'DTK').
card_original_type('minister of pain'/'DTK', 'Creature — Human Shaman').
card_original_text('minister of pain'/'DTK', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Minister of Pain exploits a creature, creatures your opponents control get -1/-1 until end of turn.').
card_first_print('minister of pain', 'DTK').
card_image_name('minister of pain'/'DTK', 'minister of pain').
card_uid('minister of pain'/'DTK', 'DTK:Minister of Pain:minister of pain').
card_rarity('minister of pain'/'DTK', 'Uncommon').
card_artist('minister of pain'/'DTK', 'Izzy').
card_number('minister of pain'/'DTK', '111').
card_flavor_text('minister of pain'/'DTK', 'Draconic words need not be shouted. A whisper will suffice.').
card_multiverse_id('minister of pain'/'DTK', '394624').
card_watermark('minister of pain'/'DTK', 'Silumgar').

card_in_set('mirror mockery', 'DTK').
card_original_type('mirror mockery'/'DTK', 'Enchantment — Aura').
card_original_text('mirror mockery'/'DTK', 'Enchant creature\nWhenever enchanted creature attacks, you may put a token onto the battlefield that\'s a copy of that creature. Exile that token at end of combat.').
card_first_print('mirror mockery', 'DTK').
card_image_name('mirror mockery'/'DTK', 'mirror mockery').
card_uid('mirror mockery'/'DTK', 'DTK:Mirror Mockery:mirror mockery').
card_rarity('mirror mockery'/'DTK', 'Rare').
card_artist('mirror mockery'/'DTK', 'Ryan Alexander Lee').
card_number('mirror mockery'/'DTK', '62').
card_flavor_text('mirror mockery'/'DTK', 'Who you are constantly runs toward who you will be.').
card_multiverse_id('mirror mockery'/'DTK', '394625').

card_in_set('misthoof kirin', 'DTK').
card_original_type('misthoof kirin'/'DTK', 'Creature — Kirin').
card_original_text('misthoof kirin'/'DTK', 'Flying, vigilance\nMegamorph {1}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('misthoof kirin', 'DTK').
card_image_name('misthoof kirin'/'DTK', 'misthoof kirin').
card_uid('misthoof kirin'/'DTK', 'DTK:Misthoof Kirin:misthoof kirin').
card_rarity('misthoof kirin'/'DTK', 'Common').
card_artist('misthoof kirin'/'DTK', 'Ryan Barger').
card_number('misthoof kirin'/'DTK', '25').
card_flavor_text('misthoof kirin'/'DTK', 'It heralded Sarkhan\'s return to Tarkir\'s present.').
card_multiverse_id('misthoof kirin'/'DTK', '394626').

card_in_set('monastery loremaster', 'DTK').
card_original_type('monastery loremaster'/'DTK', 'Creature — Djinn Wizard').
card_original_text('monastery loremaster'/'DTK', 'Megamorph {5}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Monastery Loremaster is turned face up, return target noncreature, nonland card from your graveyard to your hand.').
card_first_print('monastery loremaster', 'DTK').
card_image_name('monastery loremaster'/'DTK', 'monastery loremaster').
card_uid('monastery loremaster'/'DTK', 'DTK:Monastery Loremaster:monastery loremaster').
card_rarity('monastery loremaster'/'DTK', 'Common').
card_artist('monastery loremaster'/'DTK', 'Ryan Alexander Lee').
card_number('monastery loremaster'/'DTK', '63').
card_multiverse_id('monastery loremaster'/'DTK', '394627').
card_watermark('monastery loremaster'/'DTK', 'Ojutai').

card_in_set('mountain', 'DTK').
card_original_type('mountain'/'DTK', 'Basic Land — Mountain').
card_original_text('mountain'/'DTK', 'R').
card_image_name('mountain'/'DTK', 'mountain1').
card_uid('mountain'/'DTK', 'DTK:Mountain:mountain1').
card_rarity('mountain'/'DTK', 'Basic Land').
card_artist('mountain'/'DTK', 'Noah Bradley').
card_number('mountain'/'DTK', '259').
card_multiverse_id('mountain'/'DTK', '394628').

card_in_set('mountain', 'DTK').
card_original_type('mountain'/'DTK', 'Basic Land — Mountain').
card_original_text('mountain'/'DTK', 'R').
card_image_name('mountain'/'DTK', 'mountain2').
card_uid('mountain'/'DTK', 'DTK:Mountain:mountain2').
card_rarity('mountain'/'DTK', 'Basic Land').
card_artist('mountain'/'DTK', 'Noah Bradley').
card_number('mountain'/'DTK', '260').
card_multiverse_id('mountain'/'DTK', '394630').

card_in_set('mountain', 'DTK').
card_original_type('mountain'/'DTK', 'Basic Land — Mountain').
card_original_text('mountain'/'DTK', 'R').
card_image_name('mountain'/'DTK', 'mountain3').
card_uid('mountain'/'DTK', 'DTK:Mountain:mountain3').
card_rarity('mountain'/'DTK', 'Basic Land').
card_artist('mountain'/'DTK', 'Titus Lunter').
card_number('mountain'/'DTK', '261').
card_multiverse_id('mountain'/'DTK', '394629').

card_in_set('mystic meditation', 'DTK').
card_original_type('mystic meditation'/'DTK', 'Sorcery').
card_original_text('mystic meditation'/'DTK', 'Draw three cards. Then discard two cards unless you discard a creature card.').
card_first_print('mystic meditation', 'DTK').
card_image_name('mystic meditation'/'DTK', 'mystic meditation').
card_uid('mystic meditation'/'DTK', 'DTK:Mystic Meditation:mystic meditation').
card_rarity('mystic meditation'/'DTK', 'Common').
card_artist('mystic meditation'/'DTK', 'Howard Lyon').
card_number('mystic meditation'/'DTK', '64').
card_flavor_text('mystic meditation'/'DTK', '\"Still the mind and quiet the heart. Only then will you hear the Multiverse\'s great truths.\"\n—Narset').
card_multiverse_id('mystic meditation'/'DTK', '394631').

card_in_set('myth realized', 'DTK').
card_original_type('myth realized'/'DTK', 'Enchantment').
card_original_text('myth realized'/'DTK', 'Whenever you cast a noncreature spell, put a lore counter on Myth Realized.\n{2}{W}: Put a lore counter on Myth Realized.\n{W}: Until end of turn, Myth Realized becomes a Monk Avatar creature in addition to its other types and gains \"This creature\'s power and toughness are each equal to the number of lore counters on it.\"').
card_first_print('myth realized', 'DTK').
card_image_name('myth realized'/'DTK', 'myth realized').
card_uid('myth realized'/'DTK', 'DTK:Myth Realized:myth realized').
card_rarity('myth realized'/'DTK', 'Rare').
card_artist('myth realized'/'DTK', 'Jason Rainville').
card_number('myth realized'/'DTK', '26').
card_multiverse_id('myth realized'/'DTK', '394632').
card_watermark('myth realized'/'DTK', 'Ojutai').

card_in_set('narset transcendent', 'DTK').
card_original_type('narset transcendent'/'DTK', 'Planeswalker — Narset').
card_original_text('narset transcendent'/'DTK', '+1: Look at the top card of your library. If it\'s a noncreature, nonland card, you may reveal it and put it into your hand.\n−2: When you cast your next instant or sorcery spell from your hand this turn, it gains rebound.\n−9: You get an emblem with \"Your opponents can\'t cast noncreature spells.\"').
card_first_print('narset transcendent', 'DTK').
card_image_name('narset transcendent'/'DTK', 'narset transcendent').
card_uid('narset transcendent'/'DTK', 'DTK:Narset Transcendent:narset transcendent').
card_rarity('narset transcendent'/'DTK', 'Mythic Rare').
card_artist('narset transcendent'/'DTK', 'Magali Villeneuve').
card_number('narset transcendent'/'DTK', '225').
card_multiverse_id('narset transcendent'/'DTK', '394633').

card_in_set('naturalize', 'DTK').
card_original_type('naturalize'/'DTK', 'Instant').
card_original_text('naturalize'/'DTK', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'DTK', 'naturalize').
card_uid('naturalize'/'DTK', 'DTK:Naturalize:naturalize').
card_rarity('naturalize'/'DTK', 'Common').
card_artist('naturalize'/'DTK', 'James Paick').
card_number('naturalize'/'DTK', '193').
card_flavor_text('naturalize'/'DTK', 'The remains of ancient civilizations litter the run-down land.').
card_multiverse_id('naturalize'/'DTK', '394634').

card_in_set('necromaster dragon', 'DTK').
card_original_type('necromaster dragon'/'DTK', 'Creature — Dragon').
card_original_text('necromaster dragon'/'DTK', 'Flying\nWhenever Necromaster Dragon deals combat damage to a player, you may pay {2}. If you do, put a 2/2 black Zombie creature token onto the battlefield and each opponent puts the top two cards of his or her library into his or her graveyard.').
card_image_name('necromaster dragon'/'DTK', 'necromaster dragon').
card_uid('necromaster dragon'/'DTK', 'DTK:Necromaster Dragon:necromaster dragon').
card_rarity('necromaster dragon'/'DTK', 'Rare').
card_artist('necromaster dragon'/'DTK', 'Mark Zug').
card_number('necromaster dragon'/'DTK', '226').
card_multiverse_id('necromaster dragon'/'DTK', '394635').
card_watermark('necromaster dragon'/'DTK', 'Silumgar').

card_in_set('negate', 'DTK').
card_original_type('negate'/'DTK', 'Instant').
card_original_text('negate'/'DTK', 'Counter target noncreature spell.').
card_image_name('negate'/'DTK', 'negate').
card_uid('negate'/'DTK', 'DTK:Negate:negate').
card_rarity('negate'/'DTK', 'Common').
card_artist('negate'/'DTK', 'Willian Murai').
card_number('negate'/'DTK', '65').
card_flavor_text('negate'/'DTK', '\"You cannot be an Ojutai monk. They prize wisdom and skill, and you have neither.\"\n—Siara, the Dragon\'s Mouth').
card_multiverse_id('negate'/'DTK', '394636').

card_in_set('obscuring æther', 'DTK').
card_original_type('obscuring æther'/'DTK', 'Enchantment').
card_original_text('obscuring æther'/'DTK', 'Face-down creature spells you cast cost {1} less to cast.\n{1}{G}: Turn Obscuring Æther face down. (It becomes a 2/2 creature.)').
card_first_print('obscuring æther', 'DTK').
card_image_name('obscuring æther'/'DTK', 'obscuring aether').
card_uid('obscuring æther'/'DTK', 'DTK:Obscuring Æther:obscuring aether').
card_rarity('obscuring æther'/'DTK', 'Rare').
card_artist('obscuring æther'/'DTK', 'Min Yum').
card_number('obscuring æther'/'DTK', '194').
card_multiverse_id('obscuring æther'/'DTK', '394637').

card_in_set('ojutai exemplars', 'DTK').
card_original_type('ojutai exemplars'/'DTK', 'Creature — Human Monk').
card_original_text('ojutai exemplars'/'DTK', 'Whenever you cast a noncreature spell, choose one —\n• Tap target creature.\n• Ojutai Exemplars gains first strike and lifelink until of turn.\n• Exile Ojutai Exemplars, then return it to the battlefield tapped under its owner\'s control.').
card_first_print('ojutai exemplars', 'DTK').
card_image_name('ojutai exemplars'/'DTK', 'ojutai exemplars').
card_uid('ojutai exemplars'/'DTK', 'DTK:Ojutai Exemplars:ojutai exemplars').
card_rarity('ojutai exemplars'/'DTK', 'Mythic Rare').
card_artist('ojutai exemplars'/'DTK', 'Willian Murai').
card_number('ojutai exemplars'/'DTK', '27').
card_multiverse_id('ojutai exemplars'/'DTK', '394638').
card_watermark('ojutai exemplars'/'DTK', 'Ojutai').

card_in_set('ojutai interceptor', 'DTK').
card_original_type('ojutai interceptor'/'DTK', 'Creature — Bird Soldier').
card_original_text('ojutai interceptor'/'DTK', 'Flying\nMegamorph {3}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('ojutai interceptor', 'DTK').
card_image_name('ojutai interceptor'/'DTK', 'ojutai interceptor').
card_uid('ojutai interceptor'/'DTK', 'DTK:Ojutai Interceptor:ojutai interceptor').
card_rarity('ojutai interceptor'/'DTK', 'Common').
card_artist('ojutai interceptor'/'DTK', 'Johann Bodin').
card_number('ojutai interceptor'/'DTK', '66').
card_flavor_text('ojutai interceptor'/'DTK', '\"Though I may soar, I could never fly as high as the dragons.\"').
card_multiverse_id('ojutai interceptor'/'DTK', '394639').
card_watermark('ojutai interceptor'/'DTK', 'Ojutai').

card_in_set('ojutai monument', 'DTK').
card_original_type('ojutai monument'/'DTK', 'Artifact').
card_original_text('ojutai monument'/'DTK', '{T}: Add {W} or {U} to your mana pool.\n{4}{W}{U}: Ojutai Monument becomes a 4/4 white and blue Dragon artifact creature with flying until end of turn.').
card_first_print('ojutai monument', 'DTK').
card_image_name('ojutai monument'/'DTK', 'ojutai monument').
card_uid('ojutai monument'/'DTK', 'DTK:Ojutai Monument:ojutai monument').
card_rarity('ojutai monument'/'DTK', 'Uncommon').
card_artist('ojutai monument'/'DTK', 'Daniel Ljunggren').
card_number('ojutai monument'/'DTK', '242').
card_flavor_text('ojutai monument'/'DTK', 'Ojutai spends time at all his strongholds, but he most favors Cori Stronghold for his meditation.').
card_multiverse_id('ojutai monument'/'DTK', '394640').
card_watermark('ojutai monument'/'DTK', 'Ojutai').

card_in_set('ojutai\'s breath', 'DTK').
card_original_type('ojutai\'s breath'/'DTK', 'Instant').
card_original_text('ojutai\'s breath'/'DTK', 'Tap target creature. It doesn\'t untap during its controller\'s next untap step.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('ojutai\'s breath', 'DTK').
card_image_name('ojutai\'s breath'/'DTK', 'ojutai\'s breath').
card_uid('ojutai\'s breath'/'DTK', 'DTK:Ojutai\'s Breath:ojutai\'s breath').
card_rarity('ojutai\'s breath'/'DTK', 'Common').
card_artist('ojutai\'s breath'/'DTK', 'Kev Walker').
card_number('ojutai\'s breath'/'DTK', '67').
card_multiverse_id('ojutai\'s breath'/'DTK', '394641').
card_watermark('ojutai\'s breath'/'DTK', 'Ojutai').

card_in_set('ojutai\'s command', 'DTK').
card_original_type('ojutai\'s command'/'DTK', 'Instant').
card_original_text('ojutai\'s command'/'DTK', 'Choose two —\n• Return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.\n• You gain 4 life.\n• Counter target creature spell.\n• Draw a card.').
card_image_name('ojutai\'s command'/'DTK', 'ojutai\'s command').
card_uid('ojutai\'s command'/'DTK', 'DTK:Ojutai\'s Command:ojutai\'s command').
card_rarity('ojutai\'s command'/'DTK', 'Rare').
card_artist('ojutai\'s command'/'DTK', 'Willian Murai').
card_number('ojutai\'s command'/'DTK', '227').
card_multiverse_id('ojutai\'s command'/'DTK', '394642').
card_watermark('ojutai\'s command'/'DTK', 'Ojutai').

card_in_set('ojutai\'s summons', 'DTK').
card_original_type('ojutai\'s summons'/'DTK', 'Sorcery').
card_original_text('ojutai\'s summons'/'DTK', 'Put a 2/2 blue Djinn Monk creature token with flying onto the battlefield.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('ojutai\'s summons', 'DTK').
card_image_name('ojutai\'s summons'/'DTK', 'ojutai\'s summons').
card_uid('ojutai\'s summons'/'DTK', 'DTK:Ojutai\'s Summons:ojutai\'s summons').
card_rarity('ojutai\'s summons'/'DTK', 'Common').
card_artist('ojutai\'s summons'/'DTK', 'Jakub Kasper').
card_number('ojutai\'s summons'/'DTK', '68').
card_multiverse_id('ojutai\'s summons'/'DTK', '394643').
card_watermark('ojutai\'s summons'/'DTK', 'Ojutai').

card_in_set('orator of ojutai', 'DTK').
card_original_type('orator of ojutai'/'DTK', 'Creature — Bird Monk').
card_original_text('orator of ojutai'/'DTK', 'As an additional cost to cast Orator of Ojutai, you may reveal a Dragon card from your hand.\nDefender, flying\nWhen Orator of Ojutai enters the battlefield, if you revealed a Dragon card or controlled a Dragon as you cast Orator of Ojutai, draw a card.').
card_first_print('orator of ojutai', 'DTK').
card_image_name('orator of ojutai'/'DTK', 'orator of ojutai').
card_uid('orator of ojutai'/'DTK', 'DTK:Orator of Ojutai:orator of ojutai').
card_rarity('orator of ojutai'/'DTK', 'Uncommon').
card_artist('orator of ojutai'/'DTK', 'Zack Stella').
card_number('orator of ojutai'/'DTK', '28').
card_multiverse_id('orator of ojutai'/'DTK', '394644').
card_watermark('orator of ojutai'/'DTK', 'Ojutai').

card_in_set('pacifism', 'DTK').
card_original_type('pacifism'/'DTK', 'Enchantment — Aura').
card_original_text('pacifism'/'DTK', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'DTK', 'pacifism').
card_uid('pacifism'/'DTK', 'DTK:Pacifism:pacifism').
card_rarity('pacifism'/'DTK', 'Common').
card_artist('pacifism'/'DTK', 'Mark Zug').
card_number('pacifism'/'DTK', '29').
card_flavor_text('pacifism'/'DTK', '\"If I fight, I might step on a butterfly. That would be sad.\"\n—Krowg of Qal Sisma').
card_multiverse_id('pacifism'/'DTK', '394645').

card_in_set('palace familiar', 'DTK').
card_original_type('palace familiar'/'DTK', 'Creature — Bird').
card_original_text('palace familiar'/'DTK', 'Flying\nWhen Palace Familiar dies, draw a card.').
card_first_print('palace familiar', 'DTK').
card_image_name('palace familiar'/'DTK', 'palace familiar').
card_uid('palace familiar'/'DTK', 'DTK:Palace Familiar:palace familiar').
card_rarity('palace familiar'/'DTK', 'Common').
card_artist('palace familiar'/'DTK', 'Kev Walker').
card_number('palace familiar'/'DTK', '69').
card_flavor_text('palace familiar'/'DTK', '\"The most profound secrets lie in the darkest places of the world. It can be prudent to make use of another set of eyes.\"\n—Sidisi, Silumgar vizier').
card_multiverse_id('palace familiar'/'DTK', '394646').
card_watermark('palace familiar'/'DTK', 'Silumgar').

card_in_set('pinion feast', 'DTK').
card_original_type('pinion feast'/'DTK', 'Instant').
card_original_text('pinion feast'/'DTK', 'Destroy target creature with flying. Bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_first_print('pinion feast', 'DTK').
card_image_name('pinion feast'/'DTK', 'pinion feast').
card_uid('pinion feast'/'DTK', 'DTK:Pinion Feast:pinion feast').
card_rarity('pinion feast'/'DTK', 'Common').
card_artist('pinion feast'/'DTK', 'Ryan Barger').
card_number('pinion feast'/'DTK', '195').
card_flavor_text('pinion feast'/'DTK', '\"Even paradise is not without tragedies.\"\n—Sarkhan Vol').
card_multiverse_id('pinion feast'/'DTK', '394647').
card_watermark('pinion feast'/'DTK', 'Dromoka').

card_in_set('pitiless horde', 'DTK').
card_original_type('pitiless horde'/'DTK', 'Creature — Orc Berserker').
card_original_text('pitiless horde'/'DTK', 'At the beginning of your upkeep, you lose 2 life.\nDash {2}{B}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('pitiless horde', 'DTK').
card_image_name('pitiless horde'/'DTK', 'pitiless horde').
card_uid('pitiless horde'/'DTK', 'DTK:Pitiless Horde:pitiless horde').
card_rarity('pitiless horde'/'DTK', 'Rare').
card_artist('pitiless horde'/'DTK', 'Viktor Titov').
card_number('pitiless horde'/'DTK', '112').
card_multiverse_id('pitiless horde'/'DTK', '394648').
card_watermark('pitiless horde'/'DTK', 'Kolaghan').

card_in_set('plains', 'DTK').
card_original_type('plains'/'DTK', 'Basic Land — Plains').
card_original_text('plains'/'DTK', 'W').
card_image_name('plains'/'DTK', 'plains1').
card_uid('plains'/'DTK', 'DTK:Plains:plains1').
card_rarity('plains'/'DTK', 'Basic Land').
card_artist('plains'/'DTK', 'Sam Burley').
card_number('plains'/'DTK', '250').
card_multiverse_id('plains'/'DTK', '394649').

card_in_set('plains', 'DTK').
card_original_type('plains'/'DTK', 'Basic Land — Plains').
card_original_text('plains'/'DTK', 'W').
card_image_name('plains'/'DTK', 'plains2').
card_uid('plains'/'DTK', 'DTK:Plains:plains2').
card_rarity('plains'/'DTK', 'Basic Land').
card_artist('plains'/'DTK', 'Sam Burley').
card_number('plains'/'DTK', '251').
card_multiverse_id('plains'/'DTK', '394650').

card_in_set('plains', 'DTK').
card_original_type('plains'/'DTK', 'Basic Land — Plains').
card_original_text('plains'/'DTK', 'W').
card_image_name('plains'/'DTK', 'plains3').
card_uid('plains'/'DTK', 'DTK:Plains:plains3').
card_rarity('plains'/'DTK', 'Basic Land').
card_artist('plains'/'DTK', 'Florian de Gesincourt').
card_number('plains'/'DTK', '252').
card_multiverse_id('plains'/'DTK', '394651').

card_in_set('press the advantage', 'DTK').
card_original_type('press the advantage'/'DTK', 'Instant').
card_original_text('press the advantage'/'DTK', 'Up to two target creatures each get +2/+2 and gain trample until end of turn.').
card_first_print('press the advantage', 'DTK').
card_image_name('press the advantage'/'DTK', 'press the advantage').
card_uid('press the advantage'/'DTK', 'DTK:Press the Advantage:press the advantage').
card_rarity('press the advantage'/'DTK', 'Uncommon').
card_artist('press the advantage'/'DTK', 'Marco Nelor').
card_number('press the advantage'/'DTK', '196').
card_flavor_text('press the advantage'/'DTK', '\"Show your enemies as much mercy as they would show you.\"\n—Surrak, the Hunt Caller').
card_multiverse_id('press the advantage'/'DTK', '394652').

card_in_set('pristine skywise', 'DTK').
card_original_type('pristine skywise'/'DTK', 'Creature — Dragon').
card_original_text('pristine skywise'/'DTK', 'Flying\nWhenever you cast a noncreature spell, untap Pristine Skywise. It gains protection from the color of your choice until end of turn.').
card_image_name('pristine skywise'/'DTK', 'pristine skywise').
card_uid('pristine skywise'/'DTK', 'DTK:Pristine Skywise:pristine skywise').
card_rarity('pristine skywise'/'DTK', 'Rare').
card_artist('pristine skywise'/'DTK', 'Adam Paquette').
card_number('pristine skywise'/'DTK', '228').
card_flavor_text('pristine skywise'/'DTK', 'The elite of Ojutai\'s brood, the skywise see their enemies as puzzles to be solved.').
card_multiverse_id('pristine skywise'/'DTK', '394653').
card_watermark('pristine skywise'/'DTK', 'Ojutai').

card_in_set('profaner of the dead', 'DTK').
card_original_type('profaner of the dead'/'DTK', 'Creature — Naga Wizard').
card_original_text('profaner of the dead'/'DTK', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Profaner of the Dead exploits a creature, return to their owners\' hands all creatures your opponents control with toughness less than the exploited creature\'s toughness.').
card_first_print('profaner of the dead', 'DTK').
card_image_name('profaner of the dead'/'DTK', 'profaner of the dead').
card_uid('profaner of the dead'/'DTK', 'DTK:Profaner of the Dead:profaner of the dead').
card_rarity('profaner of the dead'/'DTK', 'Rare').
card_artist('profaner of the dead'/'DTK', 'Vincent Proce').
card_number('profaner of the dead'/'DTK', '70').
card_multiverse_id('profaner of the dead'/'DTK', '394654').
card_watermark('profaner of the dead'/'DTK', 'Silumgar').

card_in_set('profound journey', 'DTK').
card_original_type('profound journey'/'DTK', 'Sorcery').
card_original_text('profound journey'/'DTK', 'Return target permanent card from your graveyard to the battlefield.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('profound journey', 'DTK').
card_image_name('profound journey'/'DTK', 'profound journey').
card_uid('profound journey'/'DTK', 'DTK:Profound Journey:profound journey').
card_rarity('profound journey'/'DTK', 'Rare').
card_artist('profound journey'/'DTK', 'Tomasz Jedruszek').
card_number('profound journey'/'DTK', '30').
card_multiverse_id('profound journey'/'DTK', '394655').
card_watermark('profound journey'/'DTK', 'Ojutai').

card_in_set('qal sisma behemoth', 'DTK').
card_original_type('qal sisma behemoth'/'DTK', 'Creature — Ogre Warrior').
card_original_text('qal sisma behemoth'/'DTK', 'Qal Sisma Behemoth can\'t attack or block unless you pay {2}.').
card_first_print('qal sisma behemoth', 'DTK').
card_image_name('qal sisma behemoth'/'DTK', 'qal sisma behemoth').
card_uid('qal sisma behemoth'/'DTK', 'DTK:Qal Sisma Behemoth:qal sisma behemoth').
card_rarity('qal sisma behemoth'/'DTK', 'Uncommon').
card_artist('qal sisma behemoth'/'DTK', 'Evan Shipard').
card_number('qal sisma behemoth'/'DTK', '149').
card_flavor_text('qal sisma behemoth'/'DTK', '\"It hunts grand game for Atarka, and when the time comes, it will become a mighty feast.\"\n—Surrak, the Hunt Caller').
card_multiverse_id('qal sisma behemoth'/'DTK', '394656').
card_watermark('qal sisma behemoth'/'DTK', 'Atarka').

card_in_set('qarsi deceiver', 'DTK').
card_original_type('qarsi deceiver'/'DTK', 'Creature — Naga Wizard').
card_original_text('qarsi deceiver'/'DTK', '{T}: Add {1} to your mana pool. Spend this mana only to cast a face-down creature spell, pay a mana cost to turn a manifested creature face up, or pay a morph cost. (A megamorph cost is a morph cost.)').
card_first_print('qarsi deceiver', 'DTK').
card_image_name('qarsi deceiver'/'DTK', 'qarsi deceiver').
card_uid('qarsi deceiver'/'DTK', 'DTK:Qarsi Deceiver:qarsi deceiver').
card_rarity('qarsi deceiver'/'DTK', 'Uncommon').
card_artist('qarsi deceiver'/'DTK', 'Raymond Swanland').
card_number('qarsi deceiver'/'DTK', '71').
card_multiverse_id('qarsi deceiver'/'DTK', '394657').
card_watermark('qarsi deceiver'/'DTK', 'Silumgar').

card_in_set('qarsi sadist', 'DTK').
card_original_type('qarsi sadist'/'DTK', 'Creature — Human Cleric').
card_original_text('qarsi sadist'/'DTK', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Qarsi Sadist exploits a creature, target opponent loses 2 life and you gain 2 life.').
card_first_print('qarsi sadist', 'DTK').
card_image_name('qarsi sadist'/'DTK', 'qarsi sadist').
card_uid('qarsi sadist'/'DTK', 'DTK:Qarsi Sadist:qarsi sadist').
card_rarity('qarsi sadist'/'DTK', 'Common').
card_artist('qarsi sadist'/'DTK', 'Volkan Baga').
card_number('qarsi sadist'/'DTK', '113').
card_flavor_text('qarsi sadist'/'DTK', 'Dying for the greater good still hurts.').
card_multiverse_id('qarsi sadist'/'DTK', '394658').
card_watermark('qarsi sadist'/'DTK', 'Silumgar').

card_in_set('radiant purge', 'DTK').
card_original_type('radiant purge'/'DTK', 'Instant').
card_original_text('radiant purge'/'DTK', 'Exile target multicolored creature or multicolored enchantment.').
card_first_print('radiant purge', 'DTK').
card_image_name('radiant purge'/'DTK', 'radiant purge').
card_uid('radiant purge'/'DTK', 'DTK:Radiant Purge:radiant purge').
card_rarity('radiant purge'/'DTK', 'Rare').
card_artist('radiant purge'/'DTK', 'Igor Kieryluk').
card_number('radiant purge'/'DTK', '31').
card_flavor_text('radiant purge'/'DTK', 'The Shifting Wastes are Dromoka\'s domain. She will not tolerate intruders.').
card_multiverse_id('radiant purge'/'DTK', '394659').

card_in_set('rakshasa gravecaller', 'DTK').
card_original_type('rakshasa gravecaller'/'DTK', 'Creature — Cat Demon').
card_original_text('rakshasa gravecaller'/'DTK', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Rakshasa Gravecaller exploits a creature, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_first_print('rakshasa gravecaller', 'DTK').
card_image_name('rakshasa gravecaller'/'DTK', 'rakshasa gravecaller').
card_uid('rakshasa gravecaller'/'DTK', 'DTK:Rakshasa Gravecaller:rakshasa gravecaller').
card_rarity('rakshasa gravecaller'/'DTK', 'Uncommon').
card_artist('rakshasa gravecaller'/'DTK', 'Jakub Kasper').
card_number('rakshasa gravecaller'/'DTK', '114').
card_flavor_text('rakshasa gravecaller'/'DTK', 'Debts to rakshasa linger beyond death.').
card_multiverse_id('rakshasa gravecaller'/'DTK', '394660').
card_watermark('rakshasa gravecaller'/'DTK', 'Silumgar').

card_in_set('reckless imp', 'DTK').
card_original_type('reckless imp'/'DTK', 'Creature — Imp').
card_original_text('reckless imp'/'DTK', 'Flying\nReckless Imp can\'t block.\nDash {1}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('reckless imp', 'DTK').
card_image_name('reckless imp'/'DTK', 'reckless imp').
card_uid('reckless imp'/'DTK', 'DTK:Reckless Imp:reckless imp').
card_rarity('reckless imp'/'DTK', 'Common').
card_artist('reckless imp'/'DTK', 'Torstein Nordstrand').
card_number('reckless imp'/'DTK', '115').
card_multiverse_id('reckless imp'/'DTK', '394661').
card_watermark('reckless imp'/'DTK', 'Kolaghan').

card_in_set('reduce in stature', 'DTK').
card_original_type('reduce in stature'/'DTK', 'Enchantment — Aura').
card_original_text('reduce in stature'/'DTK', 'Enchant creature\nEnchanted creature has base power and toughness 0/2.').
card_first_print('reduce in stature', 'DTK').
card_image_name('reduce in stature'/'DTK', 'reduce in stature').
card_uid('reduce in stature'/'DTK', 'DTK:Reduce in Stature:reduce in stature').
card_rarity('reduce in stature'/'DTK', 'Common').
card_artist('reduce in stature'/'DTK', 'Dan Scott').
card_number('reduce in stature'/'DTK', '72').
card_flavor_text('reduce in stature'/'DTK', 'A dragon learns humility only in the moments before its death.').
card_multiverse_id('reduce in stature'/'DTK', '394662').

card_in_set('rending volley', 'DTK').
card_original_type('rending volley'/'DTK', 'Instant').
card_original_text('rending volley'/'DTK', 'Rending Volley can\'t be countered by spells or abilities.\nRending Volley deals 4 damage to target white or blue creature.').
card_first_print('rending volley', 'DTK').
card_image_name('rending volley'/'DTK', 'rending volley').
card_uid('rending volley'/'DTK', 'DTK:Rending Volley:rending volley').
card_rarity('rending volley'/'DTK', 'Uncommon').
card_artist('rending volley'/'DTK', 'Lucas Graciano').
card_number('rending volley'/'DTK', '150').
card_flavor_text('rending volley'/'DTK', 'The sky offers few hiding places.').
card_multiverse_id('rending volley'/'DTK', '394663').

card_in_set('resupply', 'DTK').
card_original_type('resupply'/'DTK', 'Instant').
card_original_text('resupply'/'DTK', 'You gain 6 life.\nDraw a card.').
card_first_print('resupply', 'DTK').
card_image_name('resupply'/'DTK', 'resupply').
card_uid('resupply'/'DTK', 'DTK:Resupply:resupply').
card_rarity('resupply'/'DTK', 'Common').
card_artist('resupply'/'DTK', 'Filip Burburan').
card_number('resupply'/'DTK', '32').
card_flavor_text('resupply'/'DTK', '\"If the scalelords are the brains of Dromoka\'s army, the supply caravans are its beating heart.\"\n—Baihir, Dromoka mage').
card_multiverse_id('resupply'/'DTK', '394664').

card_in_set('revealing wind', 'DTK').
card_original_type('revealing wind'/'DTK', 'Instant').
card_original_text('revealing wind'/'DTK', 'Prevent all combat damage that would be dealt this turn. You may look at each face-down creature that\'s attacking or blocking.').
card_first_print('revealing wind', 'DTK').
card_image_name('revealing wind'/'DTK', 'revealing wind').
card_uid('revealing wind'/'DTK', 'DTK:Revealing Wind:revealing wind').
card_rarity('revealing wind'/'DTK', 'Common').
card_artist('revealing wind'/'DTK', 'Phill Simmer').
card_number('revealing wind'/'DTK', '197').
card_flavor_text('revealing wind'/'DTK', '\"The sands obscure the vision of others, but clarify ours.\"\n—Faiso, Dromoka commander').
card_multiverse_id('revealing wind'/'DTK', '394665').

card_in_set('risen executioner', 'DTK').
card_original_type('risen executioner'/'DTK', 'Creature — Zombie Warrior').
card_original_text('risen executioner'/'DTK', 'Risen Executioner can\'t block.\nOther Zombie creatures you control get +1/+1.\nYou may cast Risen Executioner from your graveyard if you pay {1} more to cast it for each other creature card in your graveyard.').
card_first_print('risen executioner', 'DTK').
card_image_name('risen executioner'/'DTK', 'risen executioner').
card_uid('risen executioner'/'DTK', 'DTK:Risen Executioner:risen executioner').
card_rarity('risen executioner'/'DTK', 'Mythic Rare').
card_artist('risen executioner'/'DTK', 'Craig J Spearing').
card_number('risen executioner'/'DTK', '116').
card_multiverse_id('risen executioner'/'DTK', '394666').
card_watermark('risen executioner'/'DTK', 'Silumgar').

card_in_set('roast', 'DTK').
card_original_type('roast'/'DTK', 'Sorcery').
card_original_text('roast'/'DTK', 'Roast deals 5 damage to target creature without flying.').
card_first_print('roast', 'DTK').
card_image_name('roast'/'DTK', 'roast').
card_uid('roast'/'DTK', 'DTK:Roast:roast').
card_rarity('roast'/'DTK', 'Uncommon').
card_artist('roast'/'DTK', 'Zoltan Boros').
card_number('roast'/'DTK', '151').
card_flavor_text('roast'/'DTK', '\"Intruders in the lands of Atarka have but two choices: be consumed by fire, or be consumed by maw.\"\n—Ulnok, Atarka shaman').
card_multiverse_id('roast'/'DTK', '394667').

card_in_set('ruthless deathfang', 'DTK').
card_original_type('ruthless deathfang'/'DTK', 'Creature — Dragon').
card_original_text('ruthless deathfang'/'DTK', 'Flying\nWhenever you sacrifice a creature, target opponent sacrifices a creature.').
card_first_print('ruthless deathfang', 'DTK').
card_image_name('ruthless deathfang'/'DTK', 'ruthless deathfang').
card_uid('ruthless deathfang'/'DTK', 'DTK:Ruthless Deathfang:ruthless deathfang').
card_rarity('ruthless deathfang'/'DTK', 'Uncommon').
card_artist('ruthless deathfang'/'DTK', 'Filip Burburan').
card_number('ruthless deathfang'/'DTK', '229').
card_flavor_text('ruthless deathfang'/'DTK', '\"Bring forth the dead, their skull-grins and rattle-bones. We will feast upon their wailing ghosts.\"\n—Silumgar, translated from Draconic').
card_multiverse_id('ruthless deathfang'/'DTK', '394668').
card_watermark('ruthless deathfang'/'DTK', 'Silumgar').

card_in_set('sabertooth outrider', 'DTK').
card_original_type('sabertooth outrider'/'DTK', 'Creature — Human Warrior').
card_original_text('sabertooth outrider'/'DTK', 'Trample\nFormidable — Whenever Sabertooth Outrider attacks, if creatures you control have total power 8 or greater, Sabertooth Outrider gains first strike until end of turn.').
card_first_print('sabertooth outrider', 'DTK').
card_image_name('sabertooth outrider'/'DTK', 'sabertooth outrider').
card_uid('sabertooth outrider'/'DTK', 'DTK:Sabertooth Outrider:sabertooth outrider').
card_rarity('sabertooth outrider'/'DTK', 'Common').
card_artist('sabertooth outrider'/'DTK', 'Winona Nelson').
card_number('sabertooth outrider'/'DTK', '152').
card_multiverse_id('sabertooth outrider'/'DTK', '394669').
card_watermark('sabertooth outrider'/'DTK', 'Atarka').

card_in_set('salt road ambushers', 'DTK').
card_original_type('salt road ambushers'/'DTK', 'Creature — Hound Warrior').
card_original_text('salt road ambushers'/'DTK', 'Whenever another permanent you control is turned face up, if it\'s a creature, put two +1/+1 counters on it.\nMegamorph {3}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('salt road ambushers', 'DTK').
card_image_name('salt road ambushers'/'DTK', 'salt road ambushers').
card_uid('salt road ambushers'/'DTK', 'DTK:Salt Road Ambushers:salt road ambushers').
card_rarity('salt road ambushers'/'DTK', 'Uncommon').
card_artist('salt road ambushers'/'DTK', 'Joseph Meehan').
card_number('salt road ambushers'/'DTK', '198').
card_multiverse_id('salt road ambushers'/'DTK', '394670').
card_watermark('salt road ambushers'/'DTK', 'Dromoka').

card_in_set('salt road quartermasters', 'DTK').
card_original_type('salt road quartermasters'/'DTK', 'Creature — Human Soldier').
card_original_text('salt road quartermasters'/'DTK', 'Salt Road Quartermasters enters the battlefield with two +1/+1 counters on it.\n{2}{G}, Remove a +1/+1 counter from Salt Road Quartermasters: Put a +1/+1 counter on target creature.').
card_first_print('salt road quartermasters', 'DTK').
card_image_name('salt road quartermasters'/'DTK', 'salt road quartermasters').
card_uid('salt road quartermasters'/'DTK', 'DTK:Salt Road Quartermasters:salt road quartermasters').
card_rarity('salt road quartermasters'/'DTK', 'Uncommon').
card_artist('salt road quartermasters'/'DTK', 'Anthony Palumbo').
card_number('salt road quartermasters'/'DTK', '199').
card_multiverse_id('salt road quartermasters'/'DTK', '394671').
card_watermark('salt road quartermasters'/'DTK', 'Dromoka').

card_in_set('sandcrafter mage', 'DTK').
card_original_type('sandcrafter mage'/'DTK', 'Creature — Human Wizard').
card_original_text('sandcrafter mage'/'DTK', 'When Sandcrafter Mage enters the battlefield, bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('sandcrafter mage', 'DTK').
card_image_name('sandcrafter mage'/'DTK', 'sandcrafter mage').
card_uid('sandcrafter mage'/'DTK', 'DTK:Sandcrafter Mage:sandcrafter mage').
card_rarity('sandcrafter mage'/'DTK', 'Common').
card_artist('sandcrafter mage'/'DTK', 'Willian Murai').
card_number('sandcrafter mage'/'DTK', '33').
card_flavor_text('sandcrafter mage'/'DTK', 'With heat, sand can form a delicate work of art; with pressure, an impenetrable bulwark.').
card_multiverse_id('sandcrafter mage'/'DTK', '394672').
card_watermark('sandcrafter mage'/'DTK', 'Dromoka').

card_in_set('sandsteppe scavenger', 'DTK').
card_original_type('sandsteppe scavenger'/'DTK', 'Creature — Hound Scout').
card_original_text('sandsteppe scavenger'/'DTK', 'When Sandsteppe Scavenger enters the battlefield, bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_first_print('sandsteppe scavenger', 'DTK').
card_image_name('sandsteppe scavenger'/'DTK', 'sandsteppe scavenger').
card_uid('sandsteppe scavenger'/'DTK', 'DTK:Sandsteppe Scavenger:sandsteppe scavenger').
card_rarity('sandsteppe scavenger'/'DTK', 'Common').
card_artist('sandsteppe scavenger'/'DTK', 'Kieran Yanner').
card_number('sandsteppe scavenger'/'DTK', '200').
card_flavor_text('sandsteppe scavenger'/'DTK', '\"Sad what passes for a dragon among the Silumgar.\"').
card_multiverse_id('sandsteppe scavenger'/'DTK', '394673').
card_watermark('sandsteppe scavenger'/'DTK', 'Dromoka').

card_in_set('sandstorm charger', 'DTK').
card_original_type('sandstorm charger'/'DTK', 'Creature — Beast').
card_original_text('sandstorm charger'/'DTK', 'Megamorph {4}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('sandstorm charger', 'DTK').
card_image_name('sandstorm charger'/'DTK', 'sandstorm charger').
card_uid('sandstorm charger'/'DTK', 'DTK:Sandstorm Charger:sandstorm charger').
card_rarity('sandstorm charger'/'DTK', 'Common').
card_artist('sandstorm charger'/'DTK', 'Dave Kendall').
card_number('sandstorm charger'/'DTK', '34').
card_flavor_text('sandstorm charger'/'DTK', '\"May our foes choke upon its horns!\"\n—Urdnan, Dromoka warrior').
card_multiverse_id('sandstorm charger'/'DTK', '394674').
card_watermark('sandstorm charger'/'DTK', 'Dromoka').

card_in_set('sarkhan unbroken', 'DTK').
card_original_type('sarkhan unbroken'/'DTK', 'Planeswalker — Sarkhan').
card_original_text('sarkhan unbroken'/'DTK', '+1: Draw a card, then add one mana of any color to your mana pool.\n−2: Put a 4/4 red Dragon creature token with flying onto the battlefield.\n−8: Search your library for any number of Dragon creature cards and put them onto the battlefield. Then shuffle your library.').
card_first_print('sarkhan unbroken', 'DTK').
card_image_name('sarkhan unbroken'/'DTK', 'sarkhan unbroken').
card_uid('sarkhan unbroken'/'DTK', 'DTK:Sarkhan Unbroken:sarkhan unbroken').
card_rarity('sarkhan unbroken'/'DTK', 'Mythic Rare').
card_artist('sarkhan unbroken'/'DTK', 'Aleksi Briclot').
card_number('sarkhan unbroken'/'DTK', '230').
card_multiverse_id('sarkhan unbroken'/'DTK', '394675').

card_in_set('sarkhan\'s rage', 'DTK').
card_original_type('sarkhan\'s rage'/'DTK', 'Instant').
card_original_text('sarkhan\'s rage'/'DTK', 'Sarkhan\'s Rage deals 5 damage to target creature or player. If you control no Dragons, Sarkhan\'s Rage deals 2 damage to you.').
card_first_print('sarkhan\'s rage', 'DTK').
card_image_name('sarkhan\'s rage'/'DTK', 'sarkhan\'s rage').
card_uid('sarkhan\'s rage'/'DTK', 'DTK:Sarkhan\'s Rage:sarkhan\'s rage').
card_rarity('sarkhan\'s rage'/'DTK', 'Common').
card_artist('sarkhan\'s rage'/'DTK', 'Chris Rahn').
card_number('sarkhan\'s rage'/'DTK', '153').
card_flavor_text('sarkhan\'s rage'/'DTK', 'The people of Tarkir speak of an ancient legend, of the dragon-man called Sarkhan who was greatest of all khans.').
card_multiverse_id('sarkhan\'s rage'/'DTK', '394676').

card_in_set('sarkhan\'s triumph', 'DTK').
card_original_type('sarkhan\'s triumph'/'DTK', 'Instant').
card_original_text('sarkhan\'s triumph'/'DTK', 'Search your library for a Dragon creature card, reveal it, put it into your hand, then shuffle your library.').
card_first_print('sarkhan\'s triumph', 'DTK').
card_image_name('sarkhan\'s triumph'/'DTK', 'sarkhan\'s triumph').
card_uid('sarkhan\'s triumph'/'DTK', 'DTK:Sarkhan\'s Triumph:sarkhan\'s triumph').
card_rarity('sarkhan\'s triumph'/'DTK', 'Uncommon').
card_artist('sarkhan\'s triumph'/'DTK', 'Chris Rahn').
card_number('sarkhan\'s triumph'/'DTK', '154').
card_flavor_text('sarkhan\'s triumph'/'DTK', 'Sarkhan gazed on the world around him, the dragons sweeping through its skies, and joy kindled like a fire in his soul.').
card_multiverse_id('sarkhan\'s triumph'/'DTK', '394677').

card_in_set('savage ventmaw', 'DTK').
card_original_type('savage ventmaw'/'DTK', 'Creature — Dragon').
card_original_text('savage ventmaw'/'DTK', 'Flying\nWhenever Savage Ventmaw attacks, add {R}{R}{R}{G}{G}{G} to your mana pool. Until end of turn, this mana doesn\'t empty from your mana pool as steps and phases end.').
card_first_print('savage ventmaw', 'DTK').
card_image_name('savage ventmaw'/'DTK', 'savage ventmaw').
card_uid('savage ventmaw'/'DTK', 'DTK:Savage Ventmaw:savage ventmaw').
card_rarity('savage ventmaw'/'DTK', 'Uncommon').
card_artist('savage ventmaw'/'DTK', 'Slawomir Maniak').
card_number('savage ventmaw'/'DTK', '231').
card_multiverse_id('savage ventmaw'/'DTK', '394678').
card_watermark('savage ventmaw'/'DTK', 'Atarka').

card_in_set('scale blessing', 'DTK').
card_original_type('scale blessing'/'DTK', 'Instant').
card_original_text('scale blessing'/'DTK', 'Bolster 1, then put a +1/+1 counter on each creature you control with a +1/+1 counter on it. (To bolster 1, choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_first_print('scale blessing', 'DTK').
card_image_name('scale blessing'/'DTK', 'scale blessing').
card_uid('scale blessing'/'DTK', 'DTK:Scale Blessing:scale blessing').
card_rarity('scale blessing'/'DTK', 'Uncommon').
card_artist('scale blessing'/'DTK', 'Matt Stewart').
card_number('scale blessing'/'DTK', '35').
card_flavor_text('scale blessing'/'DTK', '\"Your bravery honors us all.\"\n—Dromoka, translated from Draconic').
card_multiverse_id('scale blessing'/'DTK', '394679').
card_watermark('scale blessing'/'DTK', 'Dromoka').

card_in_set('scaleguard sentinels', 'DTK').
card_original_type('scaleguard sentinels'/'DTK', 'Creature — Human Soldier').
card_original_text('scaleguard sentinels'/'DTK', 'As an additional cost to cast Scaleguard Sentinels, you may reveal a Dragon card from your hand.\nScaleguard Sentinels enters the battlefield with a +1/+1 counter on it if you revealed a Dragon card or controlled a Dragon as you cast Scaleguard Sentinels.').
card_image_name('scaleguard sentinels'/'DTK', 'scaleguard sentinels').
card_uid('scaleguard sentinels'/'DTK', 'DTK:Scaleguard Sentinels:scaleguard sentinels').
card_rarity('scaleguard sentinels'/'DTK', 'Uncommon').
card_artist('scaleguard sentinels'/'DTK', 'Matt Stewart').
card_number('scaleguard sentinels'/'DTK', '201').
card_multiverse_id('scaleguard sentinels'/'DTK', '394680').
card_watermark('scaleguard sentinels'/'DTK', 'Dromoka').

card_in_set('scion of ugin', 'DTK').
card_original_type('scion of ugin'/'DTK', 'Creature — Dragon Spirit').
card_original_text('scion of ugin'/'DTK', 'Flying').
card_first_print('scion of ugin', 'DTK').
card_image_name('scion of ugin'/'DTK', 'scion of ugin').
card_uid('scion of ugin'/'DTK', 'DTK:Scion of Ugin:scion of ugin').
card_rarity('scion of ugin'/'DTK', 'Uncommon').
card_artist('scion of ugin'/'DTK', 'Cliff Childs').
card_number('scion of ugin'/'DTK', '1').
card_flavor_text('scion of ugin'/'DTK', 'For hundreds of years Ugin slept, encased in the cocoon of stone and magic Sarkhan had created using a shard of a Zendikari hedron. As Ugin lay dormant, his spectral guardians kept vigil.').
card_multiverse_id('scion of ugin'/'DTK', '394681').

card_in_set('screamreach brawler', 'DTK').
card_original_type('screamreach brawler'/'DTK', 'Creature — Orc Berserker').
card_original_text('screamreach brawler'/'DTK', 'Dash {1}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('screamreach brawler', 'DTK').
card_image_name('screamreach brawler'/'DTK', 'screamreach brawler').
card_uid('screamreach brawler'/'DTK', 'DTK:Screamreach Brawler:screamreach brawler').
card_rarity('screamreach brawler'/'DTK', 'Common').
card_artist('screamreach brawler'/'DTK', 'Slawomir Maniak').
card_number('screamreach brawler'/'DTK', '155').
card_flavor_text('screamreach brawler'/'DTK', '\"My dragonlord\'s lightning will dance upon your bones!\"').
card_multiverse_id('screamreach brawler'/'DTK', '394682').
card_watermark('screamreach brawler'/'DTK', 'Kolaghan').

card_in_set('secure the wastes', 'DTK').
card_original_type('secure the wastes'/'DTK', 'Instant').
card_original_text('secure the wastes'/'DTK', 'Put X 1/1 white Warrior creature tokens onto the battlefield.').
card_first_print('secure the wastes', 'DTK').
card_image_name('secure the wastes'/'DTK', 'secure the wastes').
card_uid('secure the wastes'/'DTK', 'DTK:Secure the Wastes:secure the wastes').
card_rarity('secure the wastes'/'DTK', 'Rare').
card_artist('secure the wastes'/'DTK', 'Scott Murphy').
card_number('secure the wastes'/'DTK', '36').
card_flavor_text('secure the wastes'/'DTK', '\"The Shifting Wastes provide our clan eternal protection. It is our duty to return the favor.\"\n—Kadri, Dromoka warrior').
card_multiverse_id('secure the wastes'/'DTK', '394683').

card_in_set('segmented krotiq', 'DTK').
card_original_type('segmented krotiq'/'DTK', 'Creature — Insect').
card_original_text('segmented krotiq'/'DTK', 'Megamorph {6}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('segmented krotiq', 'DTK').
card_image_name('segmented krotiq'/'DTK', 'segmented krotiq').
card_uid('segmented krotiq'/'DTK', 'DTK:Segmented Krotiq:segmented krotiq').
card_rarity('segmented krotiq'/'DTK', 'Common').
card_artist('segmented krotiq'/'DTK', 'Christopher Moeller').
card_number('segmented krotiq'/'DTK', '202').
card_flavor_text('segmented krotiq'/'DTK', 'The list of things a krotiq eats is as long as the krotiq itself.').
card_multiverse_id('segmented krotiq'/'DTK', '394684').

card_in_set('seismic rupture', 'DTK').
card_original_type('seismic rupture'/'DTK', 'Sorcery').
card_original_text('seismic rupture'/'DTK', 'Seismic Rupture deals 2 damage to each creature without flying.').
card_first_print('seismic rupture', 'DTK').
card_image_name('seismic rupture'/'DTK', 'seismic rupture').
card_uid('seismic rupture'/'DTK', 'DTK:Seismic Rupture:seismic rupture').
card_rarity('seismic rupture'/'DTK', 'Uncommon').
card_artist('seismic rupture'/'DTK', 'Jason A. Engle').
card_number('seismic rupture'/'DTK', '156').
card_flavor_text('seismic rupture'/'DTK', 'The shaman opened the earth beneath their feet, trapping the survivors within the crevices of Ayagor for Atarka to devour at her leisure.').
card_multiverse_id('seismic rupture'/'DTK', '394685').

card_in_set('self-inflicted wound', 'DTK').
card_original_type('self-inflicted wound'/'DTK', 'Sorcery').
card_original_text('self-inflicted wound'/'DTK', 'Target opponent sacrifices a green or white creature. If that player does, he or she loses 2 life.').
card_first_print('self-inflicted wound', 'DTK').
card_image_name('self-inflicted wound'/'DTK', 'self-inflicted wound').
card_uid('self-inflicted wound'/'DTK', 'DTK:Self-Inflicted Wound:self-inflicted wound').
card_rarity('self-inflicted wound'/'DTK', 'Uncommon').
card_artist('self-inflicted wound'/'DTK', 'Mathias Kollros').
card_number('self-inflicted wound'/'DTK', '117').
card_flavor_text('self-inflicted wound'/'DTK', '\"Worse than watching the cruelest deed is watching it done by your own hand.\"\n—Baihir, Dromoka mage').
card_multiverse_id('self-inflicted wound'/'DTK', '394686').

card_in_set('servant of the scale', 'DTK').
card_original_type('servant of the scale'/'DTK', 'Creature — Human Soldier').
card_original_text('servant of the scale'/'DTK', 'Servant of the Scale enters the battlefield with a +1/+1 counter on it.\nWhen Servant of the Scale dies, put X +1/+1 counters on target creature you control, where X is the number of +1/+1 counters on Servant of the Scale.').
card_first_print('servant of the scale', 'DTK').
card_image_name('servant of the scale'/'DTK', 'servant of the scale').
card_uid('servant of the scale'/'DTK', 'DTK:Servant of the Scale:servant of the scale').
card_rarity('servant of the scale'/'DTK', 'Common').
card_artist('servant of the scale'/'DTK', 'Winona Nelson').
card_number('servant of the scale'/'DTK', '203').
card_multiverse_id('servant of the scale'/'DTK', '394687').
card_watermark('servant of the scale'/'DTK', 'Dromoka').

card_in_set('shaman of forgotten ways', 'DTK').
card_original_type('shaman of forgotten ways'/'DTK', 'Creature — Human Shaman').
card_original_text('shaman of forgotten ways'/'DTK', '{T}: Add two mana in any combination of colors to your mana pool. Spend this mana only to cast creature spells.\nFormidable — {9}{G}{G}, {T}: Each player\'s life total becomes the number of creatures he or she controls. Activate this ability only if creatures you control have total power 8 or greater.').
card_first_print('shaman of forgotten ways', 'DTK').
card_image_name('shaman of forgotten ways'/'DTK', 'shaman of forgotten ways').
card_uid('shaman of forgotten ways'/'DTK', 'DTK:Shaman of Forgotten Ways:shaman of forgotten ways').
card_rarity('shaman of forgotten ways'/'DTK', 'Mythic Rare').
card_artist('shaman of forgotten ways'/'DTK', 'Tyler Jacobson').
card_number('shaman of forgotten ways'/'DTK', '204').
card_multiverse_id('shaman of forgotten ways'/'DTK', '394688').
card_watermark('shaman of forgotten ways'/'DTK', 'Atarka').

card_in_set('shambling goblin', 'DTK').
card_original_type('shambling goblin'/'DTK', 'Creature — Zombie Goblin').
card_original_text('shambling goblin'/'DTK', 'When Shambling Goblin dies, target creature an opponent controls gets -1/-1 until end of turn.').
card_first_print('shambling goblin', 'DTK').
card_image_name('shambling goblin'/'DTK', 'shambling goblin').
card_uid('shambling goblin'/'DTK', 'DTK:Shambling Goblin:shambling goblin').
card_rarity('shambling goblin'/'DTK', 'Common').
card_artist('shambling goblin'/'DTK', 'Yeong-Hao Han').
card_number('shambling goblin'/'DTK', '118').
card_flavor_text('shambling goblin'/'DTK', '\"The Kolaghan send them at us. We kill and raise them. They fight the next wave the Kolaghan send. It\'s a neat little cycle.\"\n—Asmala, Silumgar sorcerer').
card_multiverse_id('shambling goblin'/'DTK', '394689').
card_watermark('shambling goblin'/'DTK', 'Silumgar').

card_in_set('shape the sands', 'DTK').
card_original_type('shape the sands'/'DTK', 'Instant').
card_original_text('shape the sands'/'DTK', 'Target creature gets +0/+5 and gains reach until end of turn. (It can block creatures with flying.)').
card_first_print('shape the sands', 'DTK').
card_image_name('shape the sands'/'DTK', 'shape the sands').
card_uid('shape the sands'/'DTK', 'DTK:Shape the Sands:shape the sands').
card_rarity('shape the sands'/'DTK', 'Common').
card_artist('shape the sands'/'DTK', 'Ryan Yee').
card_number('shape the sands'/'DTK', '205').
card_flavor_text('shape the sands'/'DTK', '\"Dragons in flight seldom expect company.\"\n—Kadri, Dromoka warrior').
card_multiverse_id('shape the sands'/'DTK', '394690').

card_in_set('sheltered aerie', 'DTK').
card_original_type('sheltered aerie'/'DTK', 'Enchantment — Aura').
card_original_text('sheltered aerie'/'DTK', 'Enchant land\nEnchanted land has \"{T}: Add two mana of any one color to your mana pool.\"').
card_first_print('sheltered aerie', 'DTK').
card_image_name('sheltered aerie'/'DTK', 'sheltered aerie').
card_uid('sheltered aerie'/'DTK', 'DTK:Sheltered Aerie:sheltered aerie').
card_rarity('sheltered aerie'/'DTK', 'Common').
card_artist('sheltered aerie'/'DTK', 'Raoul Vitale').
card_number('sheltered aerie'/'DTK', '206').
card_flavor_text('sheltered aerie'/'DTK', 'Dromoka\'s scalelords patrol the skies over Arashin, offering her people safety from the harsh world.').
card_multiverse_id('sheltered aerie'/'DTK', '394691').

card_in_set('shieldhide dragon', 'DTK').
card_original_type('shieldhide dragon'/'DTK', 'Creature — Dragon').
card_original_text('shieldhide dragon'/'DTK', 'Flying, lifelink\nMegamorph {5}{W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Shieldhide Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_first_print('shieldhide dragon', 'DTK').
card_image_name('shieldhide dragon'/'DTK', 'shieldhide dragon').
card_uid('shieldhide dragon'/'DTK', 'DTK:Shieldhide Dragon:shieldhide dragon').
card_rarity('shieldhide dragon'/'DTK', 'Uncommon').
card_artist('shieldhide dragon'/'DTK', 'Chris Rallis').
card_number('shieldhide dragon'/'DTK', '37').
card_multiverse_id('shieldhide dragon'/'DTK', '394692').
card_watermark('shieldhide dragon'/'DTK', 'Dromoka').

card_in_set('shorecrasher elemental', 'DTK').
card_original_type('shorecrasher elemental'/'DTK', 'Creature — Elemental').
card_original_text('shorecrasher elemental'/'DTK', '{U}: Exile Shorecrasher Elemental, then return it to the battlefield face down under its owner\'s control.\n{1}: Shorecrasher Elemental gets +1/-1 or -1/+1 until end of turn. \nMegamorph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('shorecrasher elemental', 'DTK').
card_image_name('shorecrasher elemental'/'DTK', 'shorecrasher elemental').
card_uid('shorecrasher elemental'/'DTK', 'DTK:Shorecrasher Elemental:shorecrasher elemental').
card_rarity('shorecrasher elemental'/'DTK', 'Mythic Rare').
card_artist('shorecrasher elemental'/'DTK', 'Igor Kieryluk').
card_number('shorecrasher elemental'/'DTK', '73').
card_multiverse_id('shorecrasher elemental'/'DTK', '394693').

card_in_set('sibsig icebreakers', 'DTK').
card_original_type('sibsig icebreakers'/'DTK', 'Creature — Zombie').
card_original_text('sibsig icebreakers'/'DTK', 'When Sibsig Icebreakers enters the battlefield, each player discards a card.').
card_first_print('sibsig icebreakers', 'DTK').
card_image_name('sibsig icebreakers'/'DTK', 'sibsig icebreakers').
card_uid('sibsig icebreakers'/'DTK', 'DTK:Sibsig Icebreakers:sibsig icebreakers').
card_rarity('sibsig icebreakers'/'DTK', 'Common').
card_artist('sibsig icebreakers'/'DTK', 'Zoltan Boros').
card_number('sibsig icebreakers'/'DTK', '119').
card_flavor_text('sibsig icebreakers'/'DTK', '\"I almost envy them. They can\'t get frostbite.\"\n—Kirada, Silumgar enforcer').
card_multiverse_id('sibsig icebreakers'/'DTK', '394694').
card_watermark('sibsig icebreakers'/'DTK', 'Silumgar').

card_in_set('sidisi\'s faithful', 'DTK').
card_original_type('sidisi\'s faithful'/'DTK', 'Creature — Naga Wizard').
card_original_text('sidisi\'s faithful'/'DTK', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Sidisi\'s Faithful exploits a creature, return target creature to its owner\'s hand.').
card_first_print('sidisi\'s faithful', 'DTK').
card_image_name('sidisi\'s faithful'/'DTK', 'sidisi\'s faithful').
card_uid('sidisi\'s faithful'/'DTK', 'DTK:Sidisi\'s Faithful:sidisi\'s faithful').
card_rarity('sidisi\'s faithful'/'DTK', 'Common').
card_artist('sidisi\'s faithful'/'DTK', 'Lius Lasahido').
card_number('sidisi\'s faithful'/'DTK', '74').
card_flavor_text('sidisi\'s faithful'/'DTK', '\"I tire of your prattle, and your face.\"').
card_multiverse_id('sidisi\'s faithful'/'DTK', '394696').
card_watermark('sidisi\'s faithful'/'DTK', 'Silumgar').

card_in_set('sidisi, undead vizier', 'DTK').
card_original_type('sidisi, undead vizier'/'DTK', 'Legendary Creature — Zombie Naga').
card_original_text('sidisi, undead vizier'/'DTK', 'Deathtouch\nExploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Sidisi, Undead Vizier exploits a creature, you may search your library for a card, put it into your hand, then shuffle your library.').
card_first_print('sidisi, undead vizier', 'DTK').
card_image_name('sidisi, undead vizier'/'DTK', 'sidisi, undead vizier').
card_uid('sidisi, undead vizier'/'DTK', 'DTK:Sidisi, Undead Vizier:sidisi, undead vizier').
card_rarity('sidisi, undead vizier'/'DTK', 'Rare').
card_artist('sidisi, undead vizier'/'DTK', 'Min Yum').
card_number('sidisi, undead vizier'/'DTK', '120').
card_multiverse_id('sidisi, undead vizier'/'DTK', '394695').
card_watermark('sidisi, undead vizier'/'DTK', 'Silumgar').

card_in_set('sight beyond sight', 'DTK').
card_original_type('sight beyond sight'/'DTK', 'Sorcery').
card_original_text('sight beyond sight'/'DTK', 'Look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('sight beyond sight', 'DTK').
card_image_name('sight beyond sight'/'DTK', 'sight beyond sight').
card_uid('sight beyond sight'/'DTK', 'DTK:Sight Beyond Sight:sight beyond sight').
card_rarity('sight beyond sight'/'DTK', 'Uncommon').
card_artist('sight beyond sight'/'DTK', 'Anastasia Ovchinnikova').
card_number('sight beyond sight'/'DTK', '75').
card_multiverse_id('sight beyond sight'/'DTK', '394697').
card_watermark('sight beyond sight'/'DTK', 'Ojutai').

card_in_set('sight of the scalelords', 'DTK').
card_original_type('sight of the scalelords'/'DTK', 'Enchantment').
card_original_text('sight of the scalelords'/'DTK', 'At the beginning of combat on your turn, creatures you control with toughness 4 or greater get +2/+2 and gain vigilance until end of turn.').
card_first_print('sight of the scalelords', 'DTK').
card_image_name('sight of the scalelords'/'DTK', 'sight of the scalelords').
card_uid('sight of the scalelords'/'DTK', 'DTK:Sight of the Scalelords:sight of the scalelords').
card_rarity('sight of the scalelords'/'DTK', 'Uncommon').
card_artist('sight of the scalelords'/'DTK', 'Marc Simonetti').
card_number('sight of the scalelords'/'DTK', '207').
card_flavor_text('sight of the scalelords'/'DTK', '\"The Silumgar creep around our borders and infiltrate our aeries. We must remain ever watchful.\"\n—Golran, Dromoka captain').
card_multiverse_id('sight of the scalelords'/'DTK', '394698').

card_in_set('silkwrap', 'DTK').
card_original_type('silkwrap'/'DTK', 'Enchantment').
card_original_text('silkwrap'/'DTK', 'When Silkwrap enters the battlefield, exile target creature with converted mana cost 3 or less an opponent controls until Silkwrap leaves the battlefield. (That creature returns under its owner\'s control.)').
card_first_print('silkwrap', 'DTK').
card_image_name('silkwrap'/'DTK', 'silkwrap').
card_uid('silkwrap'/'DTK', 'DTK:Silkwrap:silkwrap').
card_rarity('silkwrap'/'DTK', 'Uncommon').
card_artist('silkwrap'/'DTK', 'David Gaillet').
card_number('silkwrap'/'DTK', '38').
card_flavor_text('silkwrap'/'DTK', 'Better scarves than scars.').
card_multiverse_id('silkwrap'/'DTK', '394699').

card_in_set('silumgar assassin', 'DTK').
card_original_type('silumgar assassin'/'DTK', 'Creature — Human Assassin').
card_original_text('silumgar assassin'/'DTK', 'Creatures with power greater than Silumgar Assassin\'s power can\'t block it.\nMegamorph {2}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Silumgar Assassin is turned face up, destroy target creature with power 3 or less an opponent controls.').
card_first_print('silumgar assassin', 'DTK').
card_image_name('silumgar assassin'/'DTK', 'silumgar assassin').
card_uid('silumgar assassin'/'DTK', 'DTK:Silumgar Assassin:silumgar assassin').
card_rarity('silumgar assassin'/'DTK', 'Rare').
card_artist('silumgar assassin'/'DTK', 'Yohann Schepacz').
card_number('silumgar assassin'/'DTK', '121').
card_multiverse_id('silumgar assassin'/'DTK', '394700').
card_watermark('silumgar assassin'/'DTK', 'Silumgar').

card_in_set('silumgar butcher', 'DTK').
card_original_type('silumgar butcher'/'DTK', 'Creature — Zombie Djinn').
card_original_text('silumgar butcher'/'DTK', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Silumgar Butcher exploits a creature, target creature gets -3/-3 until end of turn.').
card_first_print('silumgar butcher', 'DTK').
card_image_name('silumgar butcher'/'DTK', 'silumgar butcher').
card_uid('silumgar butcher'/'DTK', 'DTK:Silumgar Butcher:silumgar butcher').
card_rarity('silumgar butcher'/'DTK', 'Common').
card_artist('silumgar butcher'/'DTK', 'Dave Kendall').
card_number('silumgar butcher'/'DTK', '122').
card_flavor_text('silumgar butcher'/'DTK', 'Silumgar takes pride in the diversity of his sibsig.').
card_multiverse_id('silumgar butcher'/'DTK', '394701').
card_watermark('silumgar butcher'/'DTK', 'Silumgar').

card_in_set('silumgar monument', 'DTK').
card_original_type('silumgar monument'/'DTK', 'Artifact').
card_original_text('silumgar monument'/'DTK', '{T}: Add {U} or {B} to your mana pool.\n{4}{U}{B}: Silumgar Monument becomes a 4/4 blue and black Dragon artifact creature with flying until end of turn.').
card_first_print('silumgar monument', 'DTK').
card_image_name('silumgar monument'/'DTK', 'silumgar monument').
card_uid('silumgar monument'/'DTK', 'DTK:Silumgar Monument:silumgar monument').
card_rarity('silumgar monument'/'DTK', 'Uncommon').
card_artist('silumgar monument'/'DTK', 'Daniel Ljunggren').
card_number('silumgar monument'/'DTK', '243').
card_flavor_text('silumgar monument'/'DTK', 'Silumgar dominates his clan from a fortress on the Marang River, where he rests upon piles of treasure.').
card_multiverse_id('silumgar monument'/'DTK', '394702').
card_watermark('silumgar monument'/'DTK', 'Silumgar').

card_in_set('silumgar sorcerer', 'DTK').
card_original_type('silumgar sorcerer'/'DTK', 'Creature — Human Wizard').
card_original_text('silumgar sorcerer'/'DTK', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nExploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Silumgar Sorcerer exploits a creature, counter target creature spell.').
card_first_print('silumgar sorcerer', 'DTK').
card_image_name('silumgar sorcerer'/'DTK', 'silumgar sorcerer').
card_uid('silumgar sorcerer'/'DTK', 'DTK:Silumgar Sorcerer:silumgar sorcerer').
card_rarity('silumgar sorcerer'/'DTK', 'Uncommon').
card_artist('silumgar sorcerer'/'DTK', 'Jeff Simpson').
card_number('silumgar sorcerer'/'DTK', '76').
card_multiverse_id('silumgar sorcerer'/'DTK', '394703').
card_watermark('silumgar sorcerer'/'DTK', 'Silumgar').

card_in_set('silumgar spell-eater', 'DTK').
card_original_type('silumgar spell-eater'/'DTK', 'Creature — Naga Wizard').
card_original_text('silumgar spell-eater'/'DTK', 'Megamorph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Silumgar Spell-Eater is turned face up, counter target spell unless its controller pays {3}.').
card_first_print('silumgar spell-eater', 'DTK').
card_image_name('silumgar spell-eater'/'DTK', 'silumgar spell-eater').
card_uid('silumgar spell-eater'/'DTK', 'DTK:Silumgar Spell-Eater:silumgar spell-eater').
card_rarity('silumgar spell-eater'/'DTK', 'Uncommon').
card_artist('silumgar spell-eater'/'DTK', 'Dave Kendall').
card_number('silumgar spell-eater'/'DTK', '77').
card_multiverse_id('silumgar spell-eater'/'DTK', '394704').
card_watermark('silumgar spell-eater'/'DTK', 'Silumgar').

card_in_set('silumgar\'s command', 'DTK').
card_original_type('silumgar\'s command'/'DTK', 'Instant').
card_original_text('silumgar\'s command'/'DTK', 'Choose two —\n• Counter target noncreature spell.\n• Return target permanent to its owner\'s hand.\n• Target creature gets -3/-3 until end of turn.\n• Destroy target planeswalker.').
card_first_print('silumgar\'s command', 'DTK').
card_image_name('silumgar\'s command'/'DTK', 'silumgar\'s command').
card_uid('silumgar\'s command'/'DTK', 'DTK:Silumgar\'s Command:silumgar\'s command').
card_rarity('silumgar\'s command'/'DTK', 'Rare').
card_artist('silumgar\'s command'/'DTK', 'Nils Hamm').
card_number('silumgar\'s command'/'DTK', '232').
card_multiverse_id('silumgar\'s command'/'DTK', '394705').
card_watermark('silumgar\'s command'/'DTK', 'Silumgar').

card_in_set('silumgar\'s scorn', 'DTK').
card_original_type('silumgar\'s scorn'/'DTK', 'Instant').
card_original_text('silumgar\'s scorn'/'DTK', 'As an additional cost to cast Silumgar\'s Scorn, you may reveal a Dragon card from your hand.\nCounter target spell unless its controller pays {1}. If you revealed a Dragon card or controlled a Dragon as you cast Silumgar\'s Scorn, counter that spell instead.').
card_first_print('silumgar\'s scorn', 'DTK').
card_image_name('silumgar\'s scorn'/'DTK', 'silumgar\'s scorn').
card_uid('silumgar\'s scorn'/'DTK', 'DTK:Silumgar\'s Scorn:silumgar\'s scorn').
card_rarity('silumgar\'s scorn'/'DTK', 'Uncommon').
card_artist('silumgar\'s scorn'/'DTK', 'Ryan Yee').
card_number('silumgar\'s scorn'/'DTK', '78').
card_multiverse_id('silumgar\'s scorn'/'DTK', '394706').
card_watermark('silumgar\'s scorn'/'DTK', 'Silumgar').

card_in_set('skywise teachings', 'DTK').
card_original_type('skywise teachings'/'DTK', 'Enchantment').
card_original_text('skywise teachings'/'DTK', 'Whenever you cast a noncreature spell, you may pay {1}{U}. If you do, put a 2/2 blue Djinn Monk creature token with flying onto the battlefield.').
card_first_print('skywise teachings', 'DTK').
card_image_name('skywise teachings'/'DTK', 'skywise teachings').
card_uid('skywise teachings'/'DTK', 'DTK:Skywise Teachings:skywise teachings').
card_rarity('skywise teachings'/'DTK', 'Uncommon').
card_artist('skywise teachings'/'DTK', 'Filip Burburan').
card_number('skywise teachings'/'DTK', '79').
card_flavor_text('skywise teachings'/'DTK', 'Ojutai\'s words must be translated from Draconic before his students can benefit from their wisdom.').
card_multiverse_id('skywise teachings'/'DTK', '394707').
card_watermark('skywise teachings'/'DTK', 'Ojutai').

card_in_set('spidersilk net', 'DTK').
card_original_type('spidersilk net'/'DTK', 'Artifact — Equipment').
card_original_text('spidersilk net'/'DTK', 'Equipped creature gets +0/+2 and has reach. (It can block creatures with flying.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('spidersilk net'/'DTK', 'spidersilk net').
card_uid('spidersilk net'/'DTK', 'DTK:Spidersilk Net:spidersilk net').
card_rarity('spidersilk net'/'DTK', 'Common').
card_artist('spidersilk net'/'DTK', 'Steve Argyle').
card_number('spidersilk net'/'DTK', '244').
card_flavor_text('spidersilk net'/'DTK', '\"Dragons are our betters, but we will fight them if our dragonlord orders it.\"').
card_multiverse_id('spidersilk net'/'DTK', '394708').

card_in_set('sprinting warbrute', 'DTK').
card_original_type('sprinting warbrute'/'DTK', 'Creature — Ogre Berserker').
card_original_text('sprinting warbrute'/'DTK', 'Sprinting Warbrute attacks each turn if able.\nDash {3}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('sprinting warbrute', 'DTK').
card_image_name('sprinting warbrute'/'DTK', 'sprinting warbrute').
card_uid('sprinting warbrute'/'DTK', 'DTK:Sprinting Warbrute:sprinting warbrute').
card_rarity('sprinting warbrute'/'DTK', 'Common').
card_artist('sprinting warbrute'/'DTK', 'Lake Hurwitz').
card_number('sprinting warbrute'/'DTK', '157').
card_multiverse_id('sprinting warbrute'/'DTK', '394709').
card_watermark('sprinting warbrute'/'DTK', 'Kolaghan').

card_in_set('stampeding elk herd', 'DTK').
card_original_type('stampeding elk herd'/'DTK', 'Creature — Elk').
card_original_text('stampeding elk herd'/'DTK', 'Formidable — Whenever Stampeding Elk Herd attacks, if creatures you control have total power 8 or greater, creatures you control gain trample until end of turn.').
card_first_print('stampeding elk herd', 'DTK').
card_image_name('stampeding elk herd'/'DTK', 'stampeding elk herd').
card_uid('stampeding elk herd'/'DTK', 'DTK:Stampeding Elk Herd:stampeding elk herd').
card_rarity('stampeding elk herd'/'DTK', 'Common').
card_artist('stampeding elk herd'/'DTK', 'Carl Frank').
card_number('stampeding elk herd'/'DTK', '208').
card_flavor_text('stampeding elk herd'/'DTK', 'The Atarka use them not just for food, but also to clear away snow, trees, and enemy forces.').
card_multiverse_id('stampeding elk herd'/'DTK', '394710').
card_watermark('stampeding elk herd'/'DTK', 'Atarka').

card_in_set('stormcrag elemental', 'DTK').
card_original_type('stormcrag elemental'/'DTK', 'Creature — Elemental').
card_original_text('stormcrag elemental'/'DTK', 'Trample\nMegamorph {4}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_first_print('stormcrag elemental', 'DTK').
card_image_name('stormcrag elemental'/'DTK', 'stormcrag elemental').
card_uid('stormcrag elemental'/'DTK', 'DTK:Stormcrag Elemental:stormcrag elemental').
card_rarity('stormcrag elemental'/'DTK', 'Uncommon').
card_artist('stormcrag elemental'/'DTK', 'Ralph Horsley').
card_number('stormcrag elemental'/'DTK', '158').
card_flavor_text('stormcrag elemental'/'DTK', 'The storms of Tarkir awaken more than dragons.').
card_multiverse_id('stormcrag elemental'/'DTK', '394711').

card_in_set('stormrider rig', 'DTK').
card_original_type('stormrider rig'/'DTK', 'Artifact — Equipment').
card_original_text('stormrider rig'/'DTK', 'Equipped creature gets +1/+1.\nWhenever a creature enters the battlefield under your control, you may attach Stormrider Rig to it.\nEquip {2}').
card_first_print('stormrider rig', 'DTK').
card_image_name('stormrider rig'/'DTK', 'stormrider rig').
card_uid('stormrider rig'/'DTK', 'DTK:Stormrider Rig:stormrider rig').
card_rarity('stormrider rig'/'DTK', 'Uncommon').
card_artist('stormrider rig'/'DTK', 'Min Yum').
card_number('stormrider rig'/'DTK', '245').
card_multiverse_id('stormrider rig'/'DTK', '394712').

card_in_set('stormwing dragon', 'DTK').
card_original_type('stormwing dragon'/'DTK', 'Creature — Dragon').
card_original_text('stormwing dragon'/'DTK', 'Flying, first strike\nMegamorph {5}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Stormwing Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_first_print('stormwing dragon', 'DTK').
card_image_name('stormwing dragon'/'DTK', 'stormwing dragon').
card_uid('stormwing dragon'/'DTK', 'DTK:Stormwing Dragon:stormwing dragon').
card_rarity('stormwing dragon'/'DTK', 'Uncommon').
card_artist('stormwing dragon'/'DTK', 'Svetlin Velinov').
card_number('stormwing dragon'/'DTK', '159').
card_multiverse_id('stormwing dragon'/'DTK', '394713').
card_watermark('stormwing dragon'/'DTK', 'Kolaghan').

card_in_set('stratus dancer', 'DTK').
card_original_type('stratus dancer'/'DTK', 'Creature — Djinn Monk').
card_original_text('stratus dancer'/'DTK', 'Flying\nMegamorph {1}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Stratus Dancer is turned face up, counter target instant or sorcery spell.').
card_first_print('stratus dancer', 'DTK').
card_image_name('stratus dancer'/'DTK', 'stratus dancer').
card_uid('stratus dancer'/'DTK', 'DTK:Stratus Dancer:stratus dancer').
card_rarity('stratus dancer'/'DTK', 'Rare').
card_artist('stratus dancer'/'DTK', 'Anastasia Ovchinnikova').
card_number('stratus dancer'/'DTK', '80').
card_multiverse_id('stratus dancer'/'DTK', '394714').
card_watermark('stratus dancer'/'DTK', 'Ojutai').

card_in_set('strongarm monk', 'DTK').
card_original_type('strongarm monk'/'DTK', 'Creature — Human Monk').
card_original_text('strongarm monk'/'DTK', 'Whenever you cast a noncreature spell, creatures you control get +1/+1 until end of turn.').
card_first_print('strongarm monk', 'DTK').
card_image_name('strongarm monk'/'DTK', 'strongarm monk').
card_uid('strongarm monk'/'DTK', 'DTK:Strongarm Monk:strongarm monk').
card_rarity('strongarm monk'/'DTK', 'Uncommon').
card_artist('strongarm monk'/'DTK', 'Viktor Titov').
card_number('strongarm monk'/'DTK', '39').
card_flavor_text('strongarm monk'/'DTK', '\"His companions are wise to follow him, for his foes dare not stand in his way.\"\n—Zhiada, Dirgur protector').
card_multiverse_id('strongarm monk'/'DTK', '394715').
card_watermark('strongarm monk'/'DTK', 'Ojutai').

card_in_set('student of ojutai', 'DTK').
card_original_type('student of ojutai'/'DTK', 'Creature — Human Monk').
card_original_text('student of ojutai'/'DTK', 'Whenever you cast a noncreature spell, you gain 2 life.').
card_first_print('student of ojutai', 'DTK').
card_image_name('student of ojutai'/'DTK', 'student of ojutai').
card_uid('student of ojutai'/'DTK', 'DTK:Student of Ojutai:student of ojutai').
card_rarity('student of ojutai'/'DTK', 'Common').
card_artist('student of ojutai'/'DTK', 'Jason A. Engle').
card_number('student of ojutai'/'DTK', '40').
card_flavor_text('student of ojutai'/'DTK', '\"Human enlightenment is a firefly that sparks in the night. Dragon enlightenment is a beacon that disperses all darkness.\"').
card_multiverse_id('student of ojutai'/'DTK', '394716').
card_watermark('student of ojutai'/'DTK', 'Ojutai').

card_in_set('summit prowler', 'DTK').
card_original_type('summit prowler'/'DTK', 'Creature — Yeti').
card_original_text('summit prowler'/'DTK', '').
card_image_name('summit prowler'/'DTK', 'summit prowler').
card_uid('summit prowler'/'DTK', 'DTK:Summit Prowler:summit prowler').
card_rarity('summit prowler'/'DTK', 'Common').
card_artist('summit prowler'/'DTK', 'Filip Burburan').
card_number('summit prowler'/'DTK', '160').
card_flavor_text('summit prowler'/'DTK', '\"Do you not hunt the yetis of the high peaks, stripling? Their meat is as tender as a bear\'s and their blood as warming as fire. They are prey that will please Atarka.\"\n—Surrak, the Hunt Caller').
card_multiverse_id('summit prowler'/'DTK', '394717').

card_in_set('sunbringer\'s touch', 'DTK').
card_original_type('sunbringer\'s touch'/'DTK', 'Sorcery').
card_original_text('sunbringer\'s touch'/'DTK', 'Bolster X, where X is the number of cards in your hand. Each creature you control with a +1/+1 counter on it gains trample until end of turn. (To bolster X, choose a creature with the least toughness among creatures you control and put X +1/+1 counters on it.)').
card_first_print('sunbringer\'s touch', 'DTK').
card_image_name('sunbringer\'s touch'/'DTK', 'sunbringer\'s touch').
card_uid('sunbringer\'s touch'/'DTK', 'DTK:Sunbringer\'s Touch:sunbringer\'s touch').
card_rarity('sunbringer\'s touch'/'DTK', 'Rare').
card_artist('sunbringer\'s touch'/'DTK', 'Lucas Graciano').
card_number('sunbringer\'s touch'/'DTK', '209').
card_multiverse_id('sunbringer\'s touch'/'DTK', '394718').
card_watermark('sunbringer\'s touch'/'DTK', 'Dromoka').

card_in_set('sunscorch regent', 'DTK').
card_original_type('sunscorch regent'/'DTK', 'Creature — Dragon').
card_original_text('sunscorch regent'/'DTK', 'Flying\nWhenever an opponent casts a spell, put a +1/+1 counter on Sunscorch Regent and you gain 1 life.').
card_first_print('sunscorch regent', 'DTK').
card_image_name('sunscorch regent'/'DTK', 'sunscorch regent').
card_uid('sunscorch regent'/'DTK', 'DTK:Sunscorch Regent:sunscorch regent').
card_rarity('sunscorch regent'/'DTK', 'Rare').
card_artist('sunscorch regent'/'DTK', 'Matt Stewart').
card_number('sunscorch regent'/'DTK', '41').
card_flavor_text('sunscorch regent'/'DTK', '\"We trust in the scalelords, bringers of justice that none can escape.\"\n—Urdnan, Dromoka warrior').
card_multiverse_id('sunscorch regent'/'DTK', '394719').
card_watermark('sunscorch regent'/'DTK', 'Dromoka').

card_in_set('surge of righteousness', 'DTK').
card_original_type('surge of righteousness'/'DTK', 'Instant').
card_original_text('surge of righteousness'/'DTK', 'Destroy target black or red creature that\'s attacking or blocking. You gain 2 life.').
card_first_print('surge of righteousness', 'DTK').
card_image_name('surge of righteousness'/'DTK', 'surge of righteousness').
card_uid('surge of righteousness'/'DTK', 'DTK:Surge of Righteousness:surge of righteousness').
card_rarity('surge of righteousness'/'DTK', 'Uncommon').
card_artist('surge of righteousness'/'DTK', 'Marco Nelor').
card_number('surge of righteousness'/'DTK', '42').
card_flavor_text('surge of righteousness'/'DTK', 'Though she stood alone, she struck with the force of an army.').
card_multiverse_id('surge of righteousness'/'DTK', '394720').

card_in_set('surrak, the hunt caller', 'DTK').
card_original_type('surrak, the hunt caller'/'DTK', 'Legendary Creature — Human Warrior').
card_original_text('surrak, the hunt caller'/'DTK', 'Formidable — At the beginning of combat on your turn, if creatures you control have total power 8 or greater, target creature you control gains haste until end of turn.').
card_first_print('surrak, the hunt caller', 'DTK').
card_image_name('surrak, the hunt caller'/'DTK', 'surrak, the hunt caller').
card_uid('surrak, the hunt caller'/'DTK', 'DTK:Surrak, the Hunt Caller:surrak, the hunt caller').
card_rarity('surrak, the hunt caller'/'DTK', 'Rare').
card_artist('surrak, the hunt caller'/'DTK', 'Wesley Burt').
card_number('surrak, the hunt caller'/'DTK', '210').
card_flavor_text('surrak, the hunt caller'/'DTK', '\"The greatest honor is to feed Atarka.\"').
card_multiverse_id('surrak, the hunt caller'/'DTK', '394721').
card_watermark('surrak, the hunt caller'/'DTK', 'Atarka').

card_in_set('swamp', 'DTK').
card_original_type('swamp'/'DTK', 'Basic Land — Swamp').
card_original_text('swamp'/'DTK', 'B').
card_image_name('swamp'/'DTK', 'swamp1').
card_uid('swamp'/'DTK', 'DTK:Swamp:swamp1').
card_rarity('swamp'/'DTK', 'Basic Land').
card_artist('swamp'/'DTK', 'Noah Bradley').
card_number('swamp'/'DTK', '256').
card_multiverse_id('swamp'/'DTK', '394722').

card_in_set('swamp', 'DTK').
card_original_type('swamp'/'DTK', 'Basic Land — Swamp').
card_original_text('swamp'/'DTK', 'B').
card_image_name('swamp'/'DTK', 'swamp2').
card_uid('swamp'/'DTK', 'DTK:Swamp:swamp2').
card_rarity('swamp'/'DTK', 'Basic Land').
card_artist('swamp'/'DTK', 'Adam Paquette').
card_number('swamp'/'DTK', '257').
card_multiverse_id('swamp'/'DTK', '394723').

card_in_set('swamp', 'DTK').
card_original_type('swamp'/'DTK', 'Basic Land — Swamp').
card_original_text('swamp'/'DTK', 'B').
card_image_name('swamp'/'DTK', 'swamp3').
card_uid('swamp'/'DTK', 'DTK:Swamp:swamp3').
card_rarity('swamp'/'DTK', 'Basic Land').
card_artist('swamp'/'DTK', 'Adam Paquette').
card_number('swamp'/'DTK', '258').
card_multiverse_id('swamp'/'DTK', '394724').

card_in_set('swift warkite', 'DTK').
card_original_type('swift warkite'/'DTK', 'Creature — Dragon').
card_original_text('swift warkite'/'DTK', 'Flying\nWhen Swift Warkite enters the battlefield, you may put a creature card with converted mana cost 3 or less from your hand or graveyard onto the battlefield. That creature gains haste. Return it to your hand at the beginning of the next end step.').
card_first_print('swift warkite', 'DTK').
card_image_name('swift warkite'/'DTK', 'swift warkite').
card_uid('swift warkite'/'DTK', 'DTK:Swift Warkite:swift warkite').
card_rarity('swift warkite'/'DTK', 'Uncommon').
card_artist('swift warkite'/'DTK', 'Izzy').
card_number('swift warkite'/'DTK', '233').
card_multiverse_id('swift warkite'/'DTK', '394725').
card_watermark('swift warkite'/'DTK', 'Kolaghan').

card_in_set('taigam\'s strike', 'DTK').
card_original_type('taigam\'s strike'/'DTK', 'Sorcery').
card_original_text('taigam\'s strike'/'DTK', 'Target creature gets +2/+0 until end of turn and can\'t be blocked this turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('taigam\'s strike', 'DTK').
card_image_name('taigam\'s strike'/'DTK', 'taigam\'s strike').
card_uid('taigam\'s strike'/'DTK', 'DTK:Taigam\'s Strike:taigam\'s strike').
card_rarity('taigam\'s strike'/'DTK', 'Common').
card_artist('taigam\'s strike'/'DTK', 'David Gaillet').
card_number('taigam\'s strike'/'DTK', '81').
card_multiverse_id('taigam\'s strike'/'DTK', '394726').
card_watermark('taigam\'s strike'/'DTK', 'Ojutai').

card_in_set('tail slash', 'DTK').
card_original_type('tail slash'/'DTK', 'Instant').
card_original_text('tail slash'/'DTK', 'Target creature you control deals damage equal to its power to target creature you don\'t control.').
card_first_print('tail slash', 'DTK').
card_image_name('tail slash'/'DTK', 'tail slash').
card_uid('tail slash'/'DTK', 'DTK:Tail Slash:tail slash').
card_rarity('tail slash'/'DTK', 'Common').
card_artist('tail slash'/'DTK', 'Efrem Palacios').
card_number('tail slash'/'DTK', '161').
card_flavor_text('tail slash'/'DTK', '\"Kneel before a dragon and you will be spared when it turns to leave.\"\n—Yikaro, Atarka warrior').
card_multiverse_id('tail slash'/'DTK', '394727').

card_in_set('tapestry of the ages', 'DTK').
card_original_type('tapestry of the ages'/'DTK', 'Artifact').
card_original_text('tapestry of the ages'/'DTK', '{2}, {T}: Draw a card. Activate this ability only if you\'ve cast a noncreature spell this turn.').
card_first_print('tapestry of the ages', 'DTK').
card_image_name('tapestry of the ages'/'DTK', 'tapestry of the ages').
card_uid('tapestry of the ages'/'DTK', 'DTK:Tapestry of the Ages:tapestry of the ages').
card_rarity('tapestry of the ages'/'DTK', 'Uncommon').
card_artist('tapestry of the ages'/'DTK', 'Yeong-Hao Han').
card_number('tapestry of the ages'/'DTK', '246').
card_flavor_text('tapestry of the ages'/'DTK', '\"Abzan, Jeskai, Sultai, Mardu, Temur—names lost to history, yet worthy of further study.\"\n—Narset').
card_multiverse_id('tapestry of the ages'/'DTK', '394728').

card_in_set('territorial roc', 'DTK').
card_original_type('territorial roc'/'DTK', 'Creature — Bird').
card_original_text('territorial roc'/'DTK', 'Flying').
card_first_print('territorial roc', 'DTK').
card_image_name('territorial roc'/'DTK', 'territorial roc').
card_uid('territorial roc'/'DTK', 'DTK:Territorial Roc:territorial roc').
card_rarity('territorial roc'/'DTK', 'Common').
card_artist('territorial roc'/'DTK', 'YW Tang').
card_number('territorial roc'/'DTK', '43').
card_flavor_text('territorial roc'/'DTK', '\"Such lesser creatures must be purged from the sky. What use do they have but to help channel the lightning of our mighty dragonlord?\"\n—Gvar Barzeel, Kolaghan warrior').
card_multiverse_id('territorial roc'/'DTK', '394729').

card_in_set('thunderbreak regent', 'DTK').
card_original_type('thunderbreak regent'/'DTK', 'Creature — Dragon').
card_original_text('thunderbreak regent'/'DTK', 'Flying\nWhenever a Dragon you control becomes the target of a spell or ability an opponent controls, Thunderbreak Regent deals 3 damage to that player.').
card_image_name('thunderbreak regent'/'DTK', 'thunderbreak regent').
card_uid('thunderbreak regent'/'DTK', 'DTK:Thunderbreak Regent:thunderbreak regent').
card_rarity('thunderbreak regent'/'DTK', 'Rare').
card_artist('thunderbreak regent'/'DTK', 'Ryan Pancoast').
card_number('thunderbreak regent'/'DTK', '162').
card_flavor_text('thunderbreak regent'/'DTK', 'Attracting a dragon\'s attention may be the last mistake you make.').
card_multiverse_id('thunderbreak regent'/'DTK', '394730').
card_watermark('thunderbreak regent'/'DTK', 'Kolaghan').

card_in_set('tormenting voice', 'DTK').
card_original_type('tormenting voice'/'DTK', 'Sorcery').
card_original_text('tormenting voice'/'DTK', 'As an additional cost to cast Tormenting Voice, discard a card.\nDraw two cards.').
card_image_name('tormenting voice'/'DTK', 'tormenting voice').
card_uid('tormenting voice'/'DTK', 'DTK:Tormenting Voice:tormenting voice').
card_rarity('tormenting voice'/'DTK', 'Common').
card_artist('tormenting voice'/'DTK', 'Volkan Baga').
card_number('tormenting voice'/'DTK', '163').
card_flavor_text('tormenting voice'/'DTK', 'A mocking laughter echoed in Ugin\'s mind. How many centuries had he slumbered, stricken, while Nicol Bolas moved unchallenged among the planes?').
card_multiverse_id('tormenting voice'/'DTK', '394731').

card_in_set('tread upon', 'DTK').
card_original_type('tread upon'/'DTK', 'Instant').
card_original_text('tread upon'/'DTK', 'Target creature gets +2/+2 and gains trample until end of turn.').
card_first_print('tread upon', 'DTK').
card_image_name('tread upon'/'DTK', 'tread upon').
card_uid('tread upon'/'DTK', 'DTK:Tread Upon:tread upon').
card_rarity('tread upon'/'DTK', 'Common').
card_artist('tread upon'/'DTK', 'Efrem Palacios').
card_number('tread upon'/'DTK', '211').
card_flavor_text('tread upon'/'DTK', '\"Boasting impenetrable defenses only draws the most tenacious of attackers.\"\n—Yikaro, Atarka warrior').
card_multiverse_id('tread upon'/'DTK', '394732').

card_in_set('twin bolt', 'DTK').
card_original_type('twin bolt'/'DTK', 'Instant').
card_original_text('twin bolt'/'DTK', 'Twin Bolt deals 2 damage divided as you choose among one or two target creatures and/or players.').
card_first_print('twin bolt', 'DTK').
card_image_name('twin bolt'/'DTK', 'twin bolt').
card_uid('twin bolt'/'DTK', 'DTK:Twin Bolt:twin bolt').
card_rarity('twin bolt'/'DTK', 'Common').
card_artist('twin bolt'/'DTK', 'Svetlin Velinov').
card_number('twin bolt'/'DTK', '164').
card_flavor_text('twin bolt'/'DTK', 'Kolaghan archers are trained in Dakla, the way of the bow. They utilize their dragonlord\'s lightning to strike their target, no matter how small, how fast, or how far away.').
card_multiverse_id('twin bolt'/'DTK', '394733').

card_in_set('ukud cobra', 'DTK').
card_original_type('ukud cobra'/'DTK', 'Creature — Snake').
card_original_text('ukud cobra'/'DTK', 'Deathtouch').
card_first_print('ukud cobra', 'DTK').
card_image_name('ukud cobra'/'DTK', 'ukud cobra').
card_uid('ukud cobra'/'DTK', 'DTK:Ukud Cobra:ukud cobra').
card_rarity('ukud cobra'/'DTK', 'Uncommon').
card_artist('ukud cobra'/'DTK', 'Johann Bodin').
card_number('ukud cobra'/'DTK', '123').
card_flavor_text('ukud cobra'/'DTK', '\"The Silumgar hide behind the deadly wildlife of their swamps. They\'d rather scheme in their jungle palaces than face us.\"\n—Khibat, Kolaghan warrior').
card_multiverse_id('ukud cobra'/'DTK', '394734').

card_in_set('ultimate price', 'DTK').
card_original_type('ultimate price'/'DTK', 'Instant').
card_original_text('ultimate price'/'DTK', 'Destroy target monocolored creature.').
card_image_name('ultimate price'/'DTK', 'ultimate price').
card_uid('ultimate price'/'DTK', 'DTK:Ultimate Price:ultimate price').
card_rarity('ultimate price'/'DTK', 'Uncommon').
card_artist('ultimate price'/'DTK', 'Jack Wang').
card_number('ultimate price'/'DTK', '124').
card_flavor_text('ultimate price'/'DTK', '\"The realization that one is dying is far more terrifying than death itself.\"\n—Sidisi, Silumgar vizier').
card_multiverse_id('ultimate price'/'DTK', '394735').

card_in_set('updraft elemental', 'DTK').
card_original_type('updraft elemental'/'DTK', 'Creature — Elemental').
card_original_text('updraft elemental'/'DTK', 'Flying').
card_first_print('updraft elemental', 'DTK').
card_image_name('updraft elemental'/'DTK', 'updraft elemental').
card_uid('updraft elemental'/'DTK', 'DTK:Updraft Elemental:updraft elemental').
card_rarity('updraft elemental'/'DTK', 'Common').
card_artist('updraft elemental'/'DTK', 'Raf Sarmento').
card_number('updraft elemental'/'DTK', '82').
card_flavor_text('updraft elemental'/'DTK', '\"It slips through the smallest cracks in the mountain, emerging whole and unfettered. There is nowhere it cannot go, for what can hold back the air itself?\"\n—Chanyi, Ojutai monk').
card_multiverse_id('updraft elemental'/'DTK', '394736').
card_watermark('updraft elemental'/'DTK', 'Ojutai').

card_in_set('vandalize', 'DTK').
card_original_type('vandalize'/'DTK', 'Sorcery').
card_original_text('vandalize'/'DTK', 'Choose one or both —\n• Destroy target artifact.\n• Destroy target land.').
card_first_print('vandalize', 'DTK').
card_image_name('vandalize'/'DTK', 'vandalize').
card_uid('vandalize'/'DTK', 'DTK:Vandalize:vandalize').
card_rarity('vandalize'/'DTK', 'Common').
card_artist('vandalize'/'DTK', 'Ryan Barger').
card_number('vandalize'/'DTK', '165').
card_flavor_text('vandalize'/'DTK', '\"As we have learned from Kolaghan, to ruin is to rule.\"\n—Shensu, Kolaghan rider').
card_multiverse_id('vandalize'/'DTK', '394737').

card_in_set('vial of dragonfire', 'DTK').
card_original_type('vial of dragonfire'/'DTK', 'Artifact').
card_original_text('vial of dragonfire'/'DTK', '{2}, {T}, Sacrifice Vial of Dragonfire: Vial of Dragonfire deals 2 damage to target creature.').
card_first_print('vial of dragonfire', 'DTK').
card_image_name('vial of dragonfire'/'DTK', 'vial of dragonfire').
card_uid('vial of dragonfire'/'DTK', 'DTK:Vial of Dragonfire:vial of dragonfire').
card_rarity('vial of dragonfire'/'DTK', 'Common').
card_artist('vial of dragonfire'/'DTK', 'Franz Vohwinkel').
card_number('vial of dragonfire'/'DTK', '247').
card_flavor_text('vial of dragonfire'/'DTK', 'Designed by an ancient artificer, the vials are strong enough to hold the very breath of a dragon—until it\'s needed.').
card_multiverse_id('vial of dragonfire'/'DTK', '394738').

card_in_set('virulent plague', 'DTK').
card_original_type('virulent plague'/'DTK', 'Enchantment').
card_original_text('virulent plague'/'DTK', 'Creature tokens get -2/-2.').
card_first_print('virulent plague', 'DTK').
card_image_name('virulent plague'/'DTK', 'virulent plague').
card_uid('virulent plague'/'DTK', 'DTK:Virulent Plague:virulent plague').
card_rarity('virulent plague'/'DTK', 'Uncommon').
card_artist('virulent plague'/'DTK', 'Johann Bodin').
card_number('virulent plague'/'DTK', '125').
card_flavor_text('virulent plague'/'DTK', '\"This pestilence robs us of glorious death in battle. We starve to death with full bellies and drown trying to slake our unnatural thirst.\"\n—Kerai Hatesinger, Kolaghan warrior').
card_multiverse_id('virulent plague'/'DTK', '394739').

card_in_set('void squall', 'DTK').
card_original_type('void squall'/'DTK', 'Sorcery').
card_original_text('void squall'/'DTK', 'Return target nonland permanent to its owner\'s hand.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_first_print('void squall', 'DTK').
card_image_name('void squall'/'DTK', 'void squall').
card_uid('void squall'/'DTK', 'DTK:Void Squall:void squall').
card_rarity('void squall'/'DTK', 'Uncommon').
card_artist('void squall'/'DTK', 'James Paick').
card_number('void squall'/'DTK', '83').
card_multiverse_id('void squall'/'DTK', '394740').
card_watermark('void squall'/'DTK', 'Ojutai').

card_in_set('volcanic rush', 'DTK').
card_original_type('volcanic rush'/'DTK', 'Instant').
card_original_text('volcanic rush'/'DTK', 'Attacking creatures get +2/+0 and gain trample until end of turn.').
card_first_print('volcanic rush', 'DTK').
card_image_name('volcanic rush'/'DTK', 'volcanic rush').
card_uid('volcanic rush'/'DTK', 'DTK:Volcanic Rush:volcanic rush').
card_rarity('volcanic rush'/'DTK', 'Common').
card_artist('volcanic rush'/'DTK', 'Ryan Barger').
card_number('volcanic rush'/'DTK', '166').
card_flavor_text('volcanic rush'/'DTK', '\"The bravest warriors take the shortest path to victory, whatever that path may be.\"\n—Sakta, Atarka hunter').
card_multiverse_id('volcanic rush'/'DTK', '394741').

card_in_set('volcanic vision', 'DTK').
card_original_type('volcanic vision'/'DTK', 'Sorcery').
card_original_text('volcanic vision'/'DTK', 'Return target instant or sorcery card from your graveyard to your hand. Volcanic Vision deals damage equal to that card\'s converted mana cost to each creature your opponents control. Exile Volcanic Vision.').
card_first_print('volcanic vision', 'DTK').
card_image_name('volcanic vision'/'DTK', 'volcanic vision').
card_uid('volcanic vision'/'DTK', 'DTK:Volcanic Vision:volcanic vision').
card_rarity('volcanic vision'/'DTK', 'Rare').
card_artist('volcanic vision'/'DTK', 'Noah Bradley').
card_number('volcanic vision'/'DTK', '167').
card_multiverse_id('volcanic vision'/'DTK', '394742').

card_in_set('vulturous aven', 'DTK').
card_original_type('vulturous aven'/'DTK', 'Creature — Bird Shaman').
card_original_text('vulturous aven'/'DTK', 'Flying\nExploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Vulturous Aven exploits a creature, you draw two cards and you lose 2 life.').
card_first_print('vulturous aven', 'DTK').
card_image_name('vulturous aven'/'DTK', 'vulturous aven').
card_uid('vulturous aven'/'DTK', 'DTK:Vulturous Aven:vulturous aven').
card_rarity('vulturous aven'/'DTK', 'Common').
card_artist('vulturous aven'/'DTK', 'Kev Walker').
card_number('vulturous aven'/'DTK', '126').
card_multiverse_id('vulturous aven'/'DTK', '394743').
card_watermark('vulturous aven'/'DTK', 'Silumgar').

card_in_set('wandering tombshell', 'DTK').
card_original_type('wandering tombshell'/'DTK', 'Creature — Zombie Turtle').
card_original_text('wandering tombshell'/'DTK', '').
card_first_print('wandering tombshell', 'DTK').
card_image_name('wandering tombshell'/'DTK', 'wandering tombshell').
card_uid('wandering tombshell'/'DTK', 'DTK:Wandering Tombshell:wandering tombshell').
card_rarity('wandering tombshell'/'DTK', 'Common').
card_artist('wandering tombshell'/'DTK', 'Yeong-Hao Han').
card_number('wandering tombshell'/'DTK', '127').
card_flavor_text('wandering tombshell'/'DTK', 'The crumbling temples on the tortoise\'s back are monuments to the decadence of the ancient Sultai. Though it harkens back to the era of the khans, Silumgar allows it to walk his territory as a warning to those who would oppose him.').
card_multiverse_id('wandering tombshell'/'DTK', '394744').
card_watermark('wandering tombshell'/'DTK', 'Silumgar').

card_in_set('warbringer', 'DTK').
card_original_type('warbringer'/'DTK', 'Creature — Orc Berserker').
card_original_text('warbringer'/'DTK', 'Dash costs you pay cost {2} less (as long as this creature is on the battlefield).\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('warbringer', 'DTK').
card_image_name('warbringer'/'DTK', 'warbringer').
card_uid('warbringer'/'DTK', 'DTK:Warbringer:warbringer').
card_rarity('warbringer'/'DTK', 'Uncommon').
card_artist('warbringer'/'DTK', 'Raymond Swanland').
card_number('warbringer'/'DTK', '168').
card_multiverse_id('warbringer'/'DTK', '394745').
card_watermark('warbringer'/'DTK', 'Kolaghan').

card_in_set('youthful scholar', 'DTK').
card_original_type('youthful scholar'/'DTK', 'Creature — Human Wizard').
card_original_text('youthful scholar'/'DTK', 'When Youthful Scholar dies, draw two cards.').
card_first_print('youthful scholar', 'DTK').
card_image_name('youthful scholar'/'DTK', 'youthful scholar').
card_uid('youthful scholar'/'DTK', 'DTK:Youthful Scholar:youthful scholar').
card_rarity('youthful scholar'/'DTK', 'Uncommon').
card_artist('youthful scholar'/'DTK', 'Cynthia Sheppard').
card_number('youthful scholar'/'DTK', '84').
card_flavor_text('youthful scholar'/'DTK', '\"Too dumb, and you end up a sibsig. Too smart, and you end up a meal. Mediocrity is the key to a long life.\"\n—Mogai, Silumgar noble').
card_multiverse_id('youthful scholar'/'DTK', '394746').
card_watermark('youthful scholar'/'DTK', 'Silumgar').

card_in_set('zephyr scribe', 'DTK').
card_original_type('zephyr scribe'/'DTK', 'Creature — Human Monk').
card_original_text('zephyr scribe'/'DTK', '{U}, {T}: Draw a card, then discard a card.\nWhenever you cast a noncreature spell, untap Zephyr Scribe.').
card_first_print('zephyr scribe', 'DTK').
card_image_name('zephyr scribe'/'DTK', 'zephyr scribe').
card_uid('zephyr scribe'/'DTK', 'DTK:Zephyr Scribe:zephyr scribe').
card_rarity('zephyr scribe'/'DTK', 'Common').
card_artist('zephyr scribe'/'DTK', 'Lius Lasahido').
card_number('zephyr scribe'/'DTK', '85').
card_flavor_text('zephyr scribe'/'DTK', '\"Ojutai\'s rule has allowed Tarkir\'s monks to learn from the truly enlightened.\"\n—Sarkhan Vol').
card_multiverse_id('zephyr scribe'/'DTK', '394747').
card_watermark('zephyr scribe'/'DTK', 'Ojutai').

card_in_set('zurgo bellstriker', 'DTK').
card_original_type('zurgo bellstriker'/'DTK', 'Legendary Creature — Orc Warrior').
card_original_text('zurgo bellstriker'/'DTK', 'Zurgo Bellstriker can\'t block creatures with power 2 or greater.\nDash {1}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_first_print('zurgo bellstriker', 'DTK').
card_image_name('zurgo bellstriker'/'DTK', 'zurgo bellstriker').
card_uid('zurgo bellstriker'/'DTK', 'DTK:Zurgo Bellstriker:zurgo bellstriker').
card_rarity('zurgo bellstriker'/'DTK', 'Rare').
card_artist('zurgo bellstriker'/'DTK', 'Jason Rainville').
card_number('zurgo bellstriker'/'DTK', '169').
card_multiverse_id('zurgo bellstriker'/'DTK', '394748').
card_watermark('zurgo bellstriker'/'DTK', 'Kolaghan').
