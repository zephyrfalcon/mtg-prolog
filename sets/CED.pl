% Collector's Edition

set('CED').
set_name('CED', 'Collector\'s Edition').
set_release_date('CED', '1993-12-01').
set_border('CED', 'black').
set_type('CED', 'reprint').

card_in_set('air elemental', 'CED').
card_original_type('air elemental'/'CED', 'Creature — Elemental').
card_original_text('air elemental'/'CED', '').
card_image_name('air elemental'/'CED', 'air elemental').
card_uid('air elemental'/'CED', 'CED:Air Elemental:air elemental').
card_rarity('air elemental'/'CED', 'Uncommon').
card_artist('air elemental'/'CED', 'Richard Thomas').
card_flavor_text('air elemental'/'CED', 'These spirits of the air are winsome and wild and cannot be truly contained. Only marginally intelligent, they often substitute whimsy for strategy, delighting in mischief and mayhem.').

card_in_set('ancestral recall', 'CED').
card_original_type('ancestral recall'/'CED', 'Instant').
card_original_text('ancestral recall'/'CED', '').
card_image_name('ancestral recall'/'CED', 'ancestral recall').
card_uid('ancestral recall'/'CED', 'CED:Ancestral Recall:ancestral recall').
card_rarity('ancestral recall'/'CED', 'Rare').
card_artist('ancestral recall'/'CED', 'Mark Poole').

card_in_set('animate artifact', 'CED').
card_original_type('animate artifact'/'CED', 'Enchantment — Aura').
card_original_text('animate artifact'/'CED', '').
card_image_name('animate artifact'/'CED', 'animate artifact').
card_uid('animate artifact'/'CED', 'CED:Animate Artifact:animate artifact').
card_rarity('animate artifact'/'CED', 'Uncommon').
card_artist('animate artifact'/'CED', 'Douglas Shuler').

card_in_set('animate dead', 'CED').
card_original_type('animate dead'/'CED', 'Enchantment — Aura').
card_original_text('animate dead'/'CED', '').
card_image_name('animate dead'/'CED', 'animate dead').
card_uid('animate dead'/'CED', 'CED:Animate Dead:animate dead').
card_rarity('animate dead'/'CED', 'Uncommon').
card_artist('animate dead'/'CED', 'Anson Maddocks').

card_in_set('animate wall', 'CED').
card_original_type('animate wall'/'CED', 'Enchantment — Aura').
card_original_text('animate wall'/'CED', '').
card_image_name('animate wall'/'CED', 'animate wall').
card_uid('animate wall'/'CED', 'CED:Animate Wall:animate wall').
card_rarity('animate wall'/'CED', 'Rare').
card_artist('animate wall'/'CED', 'Dan Frazier').

card_in_set('ankh of mishra', 'CED').
card_original_type('ankh of mishra'/'CED', 'Artifact').
card_original_text('ankh of mishra'/'CED', '').
card_image_name('ankh of mishra'/'CED', 'ankh of mishra').
card_uid('ankh of mishra'/'CED', 'CED:Ankh of Mishra:ankh of mishra').
card_rarity('ankh of mishra'/'CED', 'Rare').
card_artist('ankh of mishra'/'CED', 'Amy Weber').

card_in_set('armageddon', 'CED').
card_original_type('armageddon'/'CED', 'Sorcery').
card_original_text('armageddon'/'CED', '').
card_image_name('armageddon'/'CED', 'armageddon').
card_uid('armageddon'/'CED', 'CED:Armageddon:armageddon').
card_rarity('armageddon'/'CED', 'Rare').
card_artist('armageddon'/'CED', 'Jesper Myrfors').

card_in_set('aspect of wolf', 'CED').
card_original_type('aspect of wolf'/'CED', 'Enchantment — Aura').
card_original_text('aspect of wolf'/'CED', '').
card_image_name('aspect of wolf'/'CED', 'aspect of wolf').
card_uid('aspect of wolf'/'CED', 'CED:Aspect of Wolf:aspect of wolf').
card_rarity('aspect of wolf'/'CED', 'Rare').
card_artist('aspect of wolf'/'CED', 'Jeff A. Menges').

card_in_set('bad moon', 'CED').
card_original_type('bad moon'/'CED', 'Enchantment').
card_original_text('bad moon'/'CED', '').
card_image_name('bad moon'/'CED', 'bad moon').
card_uid('bad moon'/'CED', 'CED:Bad Moon:bad moon').
card_rarity('bad moon'/'CED', 'Rare').
card_artist('bad moon'/'CED', 'Jesper Myrfors').

card_in_set('badlands', 'CED').
card_original_type('badlands'/'CED', 'Land — Swamp Mountain').
card_original_text('badlands'/'CED', '').
card_image_name('badlands'/'CED', 'badlands').
card_uid('badlands'/'CED', 'CED:Badlands:badlands').
card_rarity('badlands'/'CED', 'Rare').
card_artist('badlands'/'CED', 'Rob Alexander').

card_in_set('balance', 'CED').
card_original_type('balance'/'CED', 'Sorcery').
card_original_text('balance'/'CED', '').
card_image_name('balance'/'CED', 'balance').
card_uid('balance'/'CED', 'CED:Balance:balance').
card_rarity('balance'/'CED', 'Rare').
card_artist('balance'/'CED', 'Mark Poole').

card_in_set('basalt monolith', 'CED').
card_original_type('basalt monolith'/'CED', 'Artifact').
card_original_text('basalt monolith'/'CED', '').
card_image_name('basalt monolith'/'CED', 'basalt monolith').
card_uid('basalt monolith'/'CED', 'CED:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'CED', 'Uncommon').
card_artist('basalt monolith'/'CED', 'Jesper Myrfors').

card_in_set('bayou', 'CED').
card_original_type('bayou'/'CED', 'Land — Swamp Forest').
card_original_text('bayou'/'CED', '').
card_image_name('bayou'/'CED', 'bayou').
card_uid('bayou'/'CED', 'CED:Bayou:bayou').
card_rarity('bayou'/'CED', 'Rare').
card_artist('bayou'/'CED', 'Jesper Myrfors').

card_in_set('benalish hero', 'CED').
card_original_type('benalish hero'/'CED', 'Creature — Human Soldier').
card_original_text('benalish hero'/'CED', '').
card_image_name('benalish hero'/'CED', 'benalish hero').
card_uid('benalish hero'/'CED', 'CED:Benalish Hero:benalish hero').
card_rarity('benalish hero'/'CED', 'Common').
card_artist('benalish hero'/'CED', 'Douglas Shuler').
card_flavor_text('benalish hero'/'CED', 'Benalia has a complex caste system that changes with the lunar year. No matter what the season, the only caste that cannot be attained by either heredity or money is that of the hero.').

card_in_set('berserk', 'CED').
card_original_type('berserk'/'CED', 'Instant').
card_original_text('berserk'/'CED', '').
card_image_name('berserk'/'CED', 'berserk').
card_uid('berserk'/'CED', 'CED:Berserk:berserk').
card_rarity('berserk'/'CED', 'Uncommon').
card_artist('berserk'/'CED', 'Dan Frazier').

card_in_set('birds of paradise', 'CED').
card_original_type('birds of paradise'/'CED', 'Creature — Bird').
card_original_text('birds of paradise'/'CED', '').
card_image_name('birds of paradise'/'CED', 'birds of paradise').
card_uid('birds of paradise'/'CED', 'CED:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'CED', 'Rare').
card_artist('birds of paradise'/'CED', 'Mark Poole').

card_in_set('black knight', 'CED').
card_original_type('black knight'/'CED', 'Creature — Human Knight').
card_original_text('black knight'/'CED', '').
card_image_name('black knight'/'CED', 'black knight').
card_uid('black knight'/'CED', 'CED:Black Knight:black knight').
card_rarity('black knight'/'CED', 'Uncommon').
card_artist('black knight'/'CED', 'Jeff A. Menges').
card_flavor_text('black knight'/'CED', 'Battle doesn\'t need a purpose; the battle is its own purpose. You don\'t ask why a plague spreads or a field burns. Don\'t ask why I fight.').

card_in_set('black lotus', 'CED').
card_original_type('black lotus'/'CED', 'Artifact').
card_original_text('black lotus'/'CED', '').
card_image_name('black lotus'/'CED', 'black lotus').
card_uid('black lotus'/'CED', 'CED:Black Lotus:black lotus').
card_rarity('black lotus'/'CED', 'Rare').
card_artist('black lotus'/'CED', 'Christopher Rush').

card_in_set('black vise', 'CED').
card_original_type('black vise'/'CED', 'Artifact').
card_original_text('black vise'/'CED', '').
card_image_name('black vise'/'CED', 'black vise').
card_uid('black vise'/'CED', 'CED:Black Vise:black vise').
card_rarity('black vise'/'CED', 'Uncommon').
card_artist('black vise'/'CED', 'Richard Thomas').

card_in_set('black ward', 'CED').
card_original_type('black ward'/'CED', 'Enchantment — Aura').
card_original_text('black ward'/'CED', '').
card_image_name('black ward'/'CED', 'black ward').
card_uid('black ward'/'CED', 'CED:Black Ward:black ward').
card_rarity('black ward'/'CED', 'Uncommon').
card_artist('black ward'/'CED', 'Dan Frazier').

card_in_set('blaze of glory', 'CED').
card_original_type('blaze of glory'/'CED', 'Instant').
card_original_text('blaze of glory'/'CED', '').
card_image_name('blaze of glory'/'CED', 'blaze of glory').
card_uid('blaze of glory'/'CED', 'CED:Blaze of Glory:blaze of glory').
card_rarity('blaze of glory'/'CED', 'Rare').
card_artist('blaze of glory'/'CED', 'Richard Thomas').

card_in_set('blessing', 'CED').
card_original_type('blessing'/'CED', 'Enchantment — Aura').
card_original_text('blessing'/'CED', '').
card_image_name('blessing'/'CED', 'blessing').
card_uid('blessing'/'CED', 'CED:Blessing:blessing').
card_rarity('blessing'/'CED', 'Rare').
card_artist('blessing'/'CED', 'Julie Baroh').

card_in_set('blue elemental blast', 'CED').
card_original_type('blue elemental blast'/'CED', 'Instant').
card_original_text('blue elemental blast'/'CED', '').
card_image_name('blue elemental blast'/'CED', 'blue elemental blast').
card_uid('blue elemental blast'/'CED', 'CED:Blue Elemental Blast:blue elemental blast').
card_rarity('blue elemental blast'/'CED', 'Common').
card_artist('blue elemental blast'/'CED', 'Richard Thomas').

card_in_set('blue ward', 'CED').
card_original_type('blue ward'/'CED', 'Enchantment — Aura').
card_original_text('blue ward'/'CED', '').
card_image_name('blue ward'/'CED', 'blue ward').
card_uid('blue ward'/'CED', 'CED:Blue Ward:blue ward').
card_rarity('blue ward'/'CED', 'Uncommon').
card_artist('blue ward'/'CED', 'Dan Frazier').

card_in_set('bog wraith', 'CED').
card_original_type('bog wraith'/'CED', 'Creature — Wraith').
card_original_text('bog wraith'/'CED', '').
card_image_name('bog wraith'/'CED', 'bog wraith').
card_uid('bog wraith'/'CED', 'CED:Bog Wraith:bog wraith').
card_rarity('bog wraith'/'CED', 'Uncommon').
card_artist('bog wraith'/'CED', 'Jeff A. Menges').
card_flavor_text('bog wraith'/'CED', '\'Twas in the bogs of Cannelbrae\nMy mate did meet an early grave\n\'Twas nothing left for us to save\nIn the peat-filled bogs of Cannelbrae.').

card_in_set('braingeyser', 'CED').
card_original_type('braingeyser'/'CED', 'Sorcery').
card_original_text('braingeyser'/'CED', '').
card_image_name('braingeyser'/'CED', 'braingeyser').
card_uid('braingeyser'/'CED', 'CED:Braingeyser:braingeyser').
card_rarity('braingeyser'/'CED', 'Rare').
card_artist('braingeyser'/'CED', 'Mark Tedin').

card_in_set('burrowing', 'CED').
card_original_type('burrowing'/'CED', 'Enchantment — Aura').
card_original_text('burrowing'/'CED', '').
card_image_name('burrowing'/'CED', 'burrowing').
card_uid('burrowing'/'CED', 'CED:Burrowing:burrowing').
card_rarity('burrowing'/'CED', 'Uncommon').
card_artist('burrowing'/'CED', 'Mark Poole').

card_in_set('camouflage', 'CED').
card_original_type('camouflage'/'CED', 'Instant').
card_original_text('camouflage'/'CED', '').
card_image_name('camouflage'/'CED', 'camouflage').
card_uid('camouflage'/'CED', 'CED:Camouflage:camouflage').
card_rarity('camouflage'/'CED', 'Uncommon').
card_artist('camouflage'/'CED', 'Jesper Myrfors').

card_in_set('castle', 'CED').
card_original_type('castle'/'CED', 'Enchantment').
card_original_text('castle'/'CED', '').
card_image_name('castle'/'CED', 'castle').
card_uid('castle'/'CED', 'CED:Castle:castle').
card_rarity('castle'/'CED', 'Uncommon').
card_artist('castle'/'CED', 'Dameon Willich').

card_in_set('celestial prism', 'CED').
card_original_type('celestial prism'/'CED', 'Artifact').
card_original_text('celestial prism'/'CED', '').
card_image_name('celestial prism'/'CED', 'celestial prism').
card_uid('celestial prism'/'CED', 'CED:Celestial Prism:celestial prism').
card_rarity('celestial prism'/'CED', 'Uncommon').
card_artist('celestial prism'/'CED', 'Amy Weber').

card_in_set('channel', 'CED').
card_original_type('channel'/'CED', 'Sorcery').
card_original_text('channel'/'CED', '').
card_image_name('channel'/'CED', 'channel').
card_uid('channel'/'CED', 'CED:Channel:channel').
card_rarity('channel'/'CED', 'Uncommon').
card_artist('channel'/'CED', 'Richard Thomas').

card_in_set('chaos orb', 'CED').
card_original_type('chaos orb'/'CED', 'Artifact').
card_original_text('chaos orb'/'CED', '').
card_image_name('chaos orb'/'CED', 'chaos orb').
card_uid('chaos orb'/'CED', 'CED:Chaos Orb:chaos orb').
card_rarity('chaos orb'/'CED', 'Rare').
card_artist('chaos orb'/'CED', 'Mark Tedin').

card_in_set('chaoslace', 'CED').
card_original_type('chaoslace'/'CED', 'Instant').
card_original_text('chaoslace'/'CED', '').
card_image_name('chaoslace'/'CED', 'chaoslace').
card_uid('chaoslace'/'CED', 'CED:Chaoslace:chaoslace').
card_rarity('chaoslace'/'CED', 'Rare').
card_artist('chaoslace'/'CED', 'Dameon Willich').

card_in_set('circle of protection: black', 'CED').
card_original_type('circle of protection: black'/'CED', 'Enchantment').
card_original_text('circle of protection: black'/'CED', '').
card_image_name('circle of protection: black'/'CED', 'circle of protection black').
card_uid('circle of protection: black'/'CED', 'CED:Circle of Protection: Black:circle of protection black').
card_rarity('circle of protection: black'/'CED', 'Common').
card_artist('circle of protection: black'/'CED', 'Jesper Myrfors').

card_in_set('circle of protection: blue', 'CED').
card_original_type('circle of protection: blue'/'CED', 'Enchantment').
card_original_text('circle of protection: blue'/'CED', '').
card_image_name('circle of protection: blue'/'CED', 'circle of protection blue').
card_uid('circle of protection: blue'/'CED', 'CED:Circle of Protection: Blue:circle of protection blue').
card_rarity('circle of protection: blue'/'CED', 'Common').
card_artist('circle of protection: blue'/'CED', 'Dameon Willich').

card_in_set('circle of protection: green', 'CED').
card_original_type('circle of protection: green'/'CED', 'Enchantment').
card_original_text('circle of protection: green'/'CED', '').
card_image_name('circle of protection: green'/'CED', 'circle of protection green').
card_uid('circle of protection: green'/'CED', 'CED:Circle of Protection: Green:circle of protection green').
card_rarity('circle of protection: green'/'CED', 'Common').
card_artist('circle of protection: green'/'CED', 'Sandra Everingham').

card_in_set('circle of protection: red', 'CED').
card_original_type('circle of protection: red'/'CED', 'Enchantment').
card_original_text('circle of protection: red'/'CED', '').
card_image_name('circle of protection: red'/'CED', 'circle of protection red').
card_uid('circle of protection: red'/'CED', 'CED:Circle of Protection: Red:circle of protection red').
card_rarity('circle of protection: red'/'CED', 'Common').
card_artist('circle of protection: red'/'CED', 'Mark Tedin').

card_in_set('circle of protection: white', 'CED').
card_original_type('circle of protection: white'/'CED', 'Enchantment').
card_original_text('circle of protection: white'/'CED', '').
card_image_name('circle of protection: white'/'CED', 'circle of protection white').
card_uid('circle of protection: white'/'CED', 'CED:Circle of Protection: White:circle of protection white').
card_rarity('circle of protection: white'/'CED', 'Common').
card_artist('circle of protection: white'/'CED', 'Douglas Shuler').

card_in_set('clockwork beast', 'CED').
card_original_type('clockwork beast'/'CED', 'Artifact Creature — Beast').
card_original_text('clockwork beast'/'CED', '').
card_image_name('clockwork beast'/'CED', 'clockwork beast').
card_uid('clockwork beast'/'CED', 'CED:Clockwork Beast:clockwork beast').
card_rarity('clockwork beast'/'CED', 'Rare').
card_artist('clockwork beast'/'CED', 'Drew Tucker').

card_in_set('clone', 'CED').
card_original_type('clone'/'CED', 'Creature — Shapeshifter').
card_original_text('clone'/'CED', '').
card_image_name('clone'/'CED', 'clone').
card_uid('clone'/'CED', 'CED:Clone:clone').
card_rarity('clone'/'CED', 'Uncommon').
card_artist('clone'/'CED', 'Julie Baroh').

card_in_set('cockatrice', 'CED').
card_original_type('cockatrice'/'CED', 'Creature — Cockatrice').
card_original_text('cockatrice'/'CED', '').
card_image_name('cockatrice'/'CED', 'cockatrice').
card_uid('cockatrice'/'CED', 'CED:Cockatrice:cockatrice').
card_rarity('cockatrice'/'CED', 'Rare').
card_artist('cockatrice'/'CED', 'Dan Frazier').

card_in_set('consecrate land', 'CED').
card_original_type('consecrate land'/'CED', 'Enchantment — Aura').
card_original_text('consecrate land'/'CED', '').
card_image_name('consecrate land'/'CED', 'consecrate land').
card_uid('consecrate land'/'CED', 'CED:Consecrate Land:consecrate land').
card_rarity('consecrate land'/'CED', 'Uncommon').
card_artist('consecrate land'/'CED', 'Jeff A. Menges').

card_in_set('conservator', 'CED').
card_original_type('conservator'/'CED', 'Artifact').
card_original_text('conservator'/'CED', '').
card_image_name('conservator'/'CED', 'conservator').
card_uid('conservator'/'CED', 'CED:Conservator:conservator').
card_rarity('conservator'/'CED', 'Uncommon').
card_artist('conservator'/'CED', 'Amy Weber').

card_in_set('contract from below', 'CED').
card_original_type('contract from below'/'CED', 'Sorcery').
card_original_text('contract from below'/'CED', '').
card_image_name('contract from below'/'CED', 'contract from below').
card_uid('contract from below'/'CED', 'CED:Contract from Below:contract from below').
card_rarity('contract from below'/'CED', 'Rare').
card_artist('contract from below'/'CED', 'Douglas Shuler').

card_in_set('control magic', 'CED').
card_original_type('control magic'/'CED', 'Enchantment — Aura').
card_original_text('control magic'/'CED', '').
card_image_name('control magic'/'CED', 'control magic').
card_uid('control magic'/'CED', 'CED:Control Magic:control magic').
card_rarity('control magic'/'CED', 'Uncommon').
card_artist('control magic'/'CED', 'Dameon Willich').

card_in_set('conversion', 'CED').
card_original_type('conversion'/'CED', 'Enchantment').
card_original_text('conversion'/'CED', '').
card_image_name('conversion'/'CED', 'conversion').
card_uid('conversion'/'CED', 'CED:Conversion:conversion').
card_rarity('conversion'/'CED', 'Uncommon').
card_artist('conversion'/'CED', 'Jesper Myrfors').

card_in_set('copper tablet', 'CED').
card_original_type('copper tablet'/'CED', 'Artifact').
card_original_text('copper tablet'/'CED', '').
card_image_name('copper tablet'/'CED', 'copper tablet').
card_uid('copper tablet'/'CED', 'CED:Copper Tablet:copper tablet').
card_rarity('copper tablet'/'CED', 'Uncommon').
card_artist('copper tablet'/'CED', 'Amy Weber').

card_in_set('copy artifact', 'CED').
card_original_type('copy artifact'/'CED', 'Enchantment').
card_original_text('copy artifact'/'CED', '').
card_image_name('copy artifact'/'CED', 'copy artifact').
card_uid('copy artifact'/'CED', 'CED:Copy Artifact:copy artifact').
card_rarity('copy artifact'/'CED', 'Rare').
card_artist('copy artifact'/'CED', 'Amy Weber').

card_in_set('counterspell', 'CED').
card_original_type('counterspell'/'CED', 'Instant').
card_original_text('counterspell'/'CED', '').
card_image_name('counterspell'/'CED', 'counterspell').
card_uid('counterspell'/'CED', 'CED:Counterspell:counterspell').
card_rarity('counterspell'/'CED', 'Uncommon').
card_artist('counterspell'/'CED', 'Mark Poole').

card_in_set('craw wurm', 'CED').
card_original_type('craw wurm'/'CED', 'Creature — Wurm').
card_original_text('craw wurm'/'CED', '').
card_image_name('craw wurm'/'CED', 'craw wurm').
card_uid('craw wurm'/'CED', 'CED:Craw Wurm:craw wurm').
card_rarity('craw wurm'/'CED', 'Common').
card_artist('craw wurm'/'CED', 'Daniel Gelon').
card_flavor_text('craw wurm'/'CED', 'The most terrifying thing about the Craw Wurm is probably the horrible crashing sound it makes as it speeds through the forest. This noise is so loud it echoes through the trees and seems to come from all directions at once.').

card_in_set('creature bond', 'CED').
card_original_type('creature bond'/'CED', 'Enchantment — Aura').
card_original_text('creature bond'/'CED', '').
card_image_name('creature bond'/'CED', 'creature bond').
card_uid('creature bond'/'CED', 'CED:Creature Bond:creature bond').
card_rarity('creature bond'/'CED', 'Common').
card_artist('creature bond'/'CED', 'Anson Maddocks').

card_in_set('crusade', 'CED').
card_original_type('crusade'/'CED', 'Enchantment').
card_original_text('crusade'/'CED', '').
card_image_name('crusade'/'CED', 'crusade').
card_uid('crusade'/'CED', 'CED:Crusade:crusade').
card_rarity('crusade'/'CED', 'Rare').
card_artist('crusade'/'CED', 'Mark Poole').

card_in_set('crystal rod', 'CED').
card_original_type('crystal rod'/'CED', 'Artifact').
card_original_text('crystal rod'/'CED', '').
card_image_name('crystal rod'/'CED', 'crystal rod').
card_uid('crystal rod'/'CED', 'CED:Crystal Rod:crystal rod').
card_rarity('crystal rod'/'CED', 'Uncommon').
card_artist('crystal rod'/'CED', 'Amy Weber').

card_in_set('cursed land', 'CED').
card_original_type('cursed land'/'CED', 'Enchantment — Aura').
card_original_text('cursed land'/'CED', '').
card_image_name('cursed land'/'CED', 'cursed land').
card_uid('cursed land'/'CED', 'CED:Cursed Land:cursed land').
card_rarity('cursed land'/'CED', 'Uncommon').
card_artist('cursed land'/'CED', 'Jesper Myrfors').

card_in_set('cyclopean tomb', 'CED').
card_original_type('cyclopean tomb'/'CED', 'Artifact').
card_original_text('cyclopean tomb'/'CED', '').
card_image_name('cyclopean tomb'/'CED', 'cyclopean tomb').
card_uid('cyclopean tomb'/'CED', 'CED:Cyclopean Tomb:cyclopean tomb').
card_rarity('cyclopean tomb'/'CED', 'Rare').
card_artist('cyclopean tomb'/'CED', 'Anson Maddocks').

card_in_set('dark ritual', 'CED').
card_original_type('dark ritual'/'CED', 'Instant').
card_original_text('dark ritual'/'CED', '').
card_image_name('dark ritual'/'CED', 'dark ritual').
card_uid('dark ritual'/'CED', 'CED:Dark Ritual:dark ritual').
card_rarity('dark ritual'/'CED', 'Common').
card_artist('dark ritual'/'CED', 'Sandra Everingham').

card_in_set('darkpact', 'CED').
card_original_type('darkpact'/'CED', 'Sorcery').
card_original_text('darkpact'/'CED', '').
card_image_name('darkpact'/'CED', 'darkpact').
card_uid('darkpact'/'CED', 'CED:Darkpact:darkpact').
card_rarity('darkpact'/'CED', 'Rare').
card_artist('darkpact'/'CED', 'Quinton Hoover').

card_in_set('death ward', 'CED').
card_original_type('death ward'/'CED', 'Instant').
card_original_text('death ward'/'CED', '').
card_image_name('death ward'/'CED', 'death ward').
card_uid('death ward'/'CED', 'CED:Death Ward:death ward').
card_rarity('death ward'/'CED', 'Common').
card_artist('death ward'/'CED', 'Mark Poole').

card_in_set('deathgrip', 'CED').
card_original_type('deathgrip'/'CED', 'Enchantment').
card_original_text('deathgrip'/'CED', '').
card_image_name('deathgrip'/'CED', 'deathgrip').
card_uid('deathgrip'/'CED', 'CED:Deathgrip:deathgrip').
card_rarity('deathgrip'/'CED', 'Uncommon').
card_artist('deathgrip'/'CED', 'Anson Maddocks').

card_in_set('deathlace', 'CED').
card_original_type('deathlace'/'CED', 'Instant').
card_original_text('deathlace'/'CED', '').
card_image_name('deathlace'/'CED', 'deathlace').
card_uid('deathlace'/'CED', 'CED:Deathlace:deathlace').
card_rarity('deathlace'/'CED', 'Rare').
card_artist('deathlace'/'CED', 'Sandra Everingham').

card_in_set('demonic attorney', 'CED').
card_original_type('demonic attorney'/'CED', 'Sorcery').
card_original_text('demonic attorney'/'CED', '').
card_image_name('demonic attorney'/'CED', 'demonic attorney').
card_uid('demonic attorney'/'CED', 'CED:Demonic Attorney:demonic attorney').
card_rarity('demonic attorney'/'CED', 'Rare').
card_artist('demonic attorney'/'CED', 'Daniel Gelon').

card_in_set('demonic hordes', 'CED').
card_original_type('demonic hordes'/'CED', 'Creature — Demon').
card_original_text('demonic hordes'/'CED', '').
card_image_name('demonic hordes'/'CED', 'demonic hordes').
card_uid('demonic hordes'/'CED', 'CED:Demonic Hordes:demonic hordes').
card_rarity('demonic hordes'/'CED', 'Rare').
card_artist('demonic hordes'/'CED', 'Jesper Myrfors').
card_flavor_text('demonic hordes'/'CED', 'Created to destroy Dominia, Demons can sometimes be bent to a more focused purpose.').

card_in_set('demonic tutor', 'CED').
card_original_type('demonic tutor'/'CED', 'Sorcery').
card_original_text('demonic tutor'/'CED', '').
card_image_name('demonic tutor'/'CED', 'demonic tutor').
card_uid('demonic tutor'/'CED', 'CED:Demonic Tutor:demonic tutor').
card_rarity('demonic tutor'/'CED', 'Uncommon').
card_artist('demonic tutor'/'CED', 'Douglas Shuler').

card_in_set('dingus egg', 'CED').
card_original_type('dingus egg'/'CED', 'Artifact').
card_original_text('dingus egg'/'CED', '').
card_image_name('dingus egg'/'CED', 'dingus egg').
card_uid('dingus egg'/'CED', 'CED:Dingus Egg:dingus egg').
card_rarity('dingus egg'/'CED', 'Rare').
card_artist('dingus egg'/'CED', 'Dan Frazier').

card_in_set('disenchant', 'CED').
card_original_type('disenchant'/'CED', 'Instant').
card_original_text('disenchant'/'CED', '').
card_image_name('disenchant'/'CED', 'disenchant').
card_uid('disenchant'/'CED', 'CED:Disenchant:disenchant').
card_rarity('disenchant'/'CED', 'Common').
card_artist('disenchant'/'CED', 'Amy Weber').

card_in_set('disintegrate', 'CED').
card_original_type('disintegrate'/'CED', 'Sorcery').
card_original_text('disintegrate'/'CED', '').
card_image_name('disintegrate'/'CED', 'disintegrate').
card_uid('disintegrate'/'CED', 'CED:Disintegrate:disintegrate').
card_rarity('disintegrate'/'CED', 'Common').
card_artist('disintegrate'/'CED', 'Anson Maddocks').

card_in_set('disrupting scepter', 'CED').
card_original_type('disrupting scepter'/'CED', 'Artifact').
card_original_text('disrupting scepter'/'CED', '').
card_image_name('disrupting scepter'/'CED', 'disrupting scepter').
card_uid('disrupting scepter'/'CED', 'CED:Disrupting Scepter:disrupting scepter').
card_rarity('disrupting scepter'/'CED', 'Rare').
card_artist('disrupting scepter'/'CED', 'Dan Frazier').

card_in_set('dragon whelp', 'CED').
card_original_type('dragon whelp'/'CED', 'Creature — Dragon').
card_original_text('dragon whelp'/'CED', '').
card_image_name('dragon whelp'/'CED', 'dragon whelp').
card_uid('dragon whelp'/'CED', 'CED:Dragon Whelp:dragon whelp').
card_rarity('dragon whelp'/'CED', 'Uncommon').
card_artist('dragon whelp'/'CED', 'Amy Weber').
card_flavor_text('dragon whelp'/'CED', '\"O to be a dragon . . . of silkworm size or immense . . .\"\n—Marianne Moore, \"O to Be a Dragon\"').

card_in_set('drain life', 'CED').
card_original_type('drain life'/'CED', 'Sorcery').
card_original_text('drain life'/'CED', '').
card_image_name('drain life'/'CED', 'drain life').
card_uid('drain life'/'CED', 'CED:Drain Life:drain life').
card_rarity('drain life'/'CED', 'Common').
card_artist('drain life'/'CED', 'Douglas Shuler').

card_in_set('drain power', 'CED').
card_original_type('drain power'/'CED', 'Sorcery').
card_original_text('drain power'/'CED', '').
card_image_name('drain power'/'CED', 'drain power').
card_uid('drain power'/'CED', 'CED:Drain Power:drain power').
card_rarity('drain power'/'CED', 'Rare').
card_artist('drain power'/'CED', 'Douglas Shuler').

card_in_set('drudge skeletons', 'CED').
card_original_type('drudge skeletons'/'CED', 'Creature — Skeleton').
card_original_text('drudge skeletons'/'CED', '').
card_image_name('drudge skeletons'/'CED', 'drudge skeletons').
card_uid('drudge skeletons'/'CED', 'CED:Drudge Skeletons:drudge skeletons').
card_rarity('drudge skeletons'/'CED', 'Common').
card_artist('drudge skeletons'/'CED', 'Sandra Everingham').
card_flavor_text('drudge skeletons'/'CED', 'Bones scattered around us joined to form misshapen bodies. We struck at them repeatedly—they fell, but soon formed again, with the same mocking look on their faceless skulls.').

card_in_set('dwarven demolition team', 'CED').
card_original_type('dwarven demolition team'/'CED', 'Creature — Dwarf').
card_original_text('dwarven demolition team'/'CED', '').
card_image_name('dwarven demolition team'/'CED', 'dwarven demolition team').
card_uid('dwarven demolition team'/'CED', 'CED:Dwarven Demolition Team:dwarven demolition team').
card_rarity('dwarven demolition team'/'CED', 'Uncommon').
card_artist('dwarven demolition team'/'CED', 'Kev Brockschmidt').
card_flavor_text('dwarven demolition team'/'CED', 'Foolishly, Najib retreated to his castle at El-Abar; the next morning he was dead. In just one night, the dwarven forces had reduced the mighty walls to mere rubble.').

card_in_set('dwarven warriors', 'CED').
card_original_type('dwarven warriors'/'CED', 'Creature — Dwarf Warrior').
card_original_text('dwarven warriors'/'CED', '').
card_image_name('dwarven warriors'/'CED', 'dwarven warriors').
card_uid('dwarven warriors'/'CED', 'CED:Dwarven Warriors:dwarven warriors').
card_rarity('dwarven warriors'/'CED', 'Common').
card_artist('dwarven warriors'/'CED', 'Douglas Shuler').

card_in_set('earth elemental', 'CED').
card_original_type('earth elemental'/'CED', 'Creature — Elemental').
card_original_text('earth elemental'/'CED', '').
card_image_name('earth elemental'/'CED', 'earth elemental').
card_uid('earth elemental'/'CED', 'CED:Earth Elemental:earth elemental').
card_rarity('earth elemental'/'CED', 'Uncommon').
card_artist('earth elemental'/'CED', 'Dan Frazier').
card_flavor_text('earth elemental'/'CED', 'Earth Elementals have the eternal strength of stone and the endurance of mountains. Primordially connected to the land they inhabit, they take a long-term view of things, scorning the impetuous haste of short-lived mortal creatures.').

card_in_set('earthbind', 'CED').
card_original_type('earthbind'/'CED', 'Enchantment — Aura').
card_original_text('earthbind'/'CED', '').
card_image_name('earthbind'/'CED', 'earthbind').
card_uid('earthbind'/'CED', 'CED:Earthbind:earthbind').
card_rarity('earthbind'/'CED', 'Common').
card_artist('earthbind'/'CED', 'Quinton Hoover').

card_in_set('earthquake', 'CED').
card_original_type('earthquake'/'CED', 'Sorcery').
card_original_text('earthquake'/'CED', '').
card_image_name('earthquake'/'CED', 'earthquake').
card_uid('earthquake'/'CED', 'CED:Earthquake:earthquake').
card_rarity('earthquake'/'CED', 'Rare').
card_artist('earthquake'/'CED', 'Dan Frazier').

card_in_set('elvish archers', 'CED').
card_original_type('elvish archers'/'CED', 'Creature — Elf Archer').
card_original_text('elvish archers'/'CED', '').
card_image_name('elvish archers'/'CED', 'elvish archers').
card_uid('elvish archers'/'CED', 'CED:Elvish Archers:elvish archers').
card_rarity('elvish archers'/'CED', 'Rare').
card_artist('elvish archers'/'CED', 'Anson Maddocks').
card_flavor_text('elvish archers'/'CED', 'I tell you, there was so many arrows flying about you couldn\'t hardly see the sun. So I says to young Angus, \"Well, at least now we\'re fighting in the shade!\"').

card_in_set('evil presence', 'CED').
card_original_type('evil presence'/'CED', 'Enchantment — Aura').
card_original_text('evil presence'/'CED', '').
card_image_name('evil presence'/'CED', 'evil presence').
card_uid('evil presence'/'CED', 'CED:Evil Presence:evil presence').
card_rarity('evil presence'/'CED', 'Uncommon').
card_artist('evil presence'/'CED', 'Sandra Everingham').

card_in_set('false orders', 'CED').
card_original_type('false orders'/'CED', 'Instant').
card_original_text('false orders'/'CED', '').
card_image_name('false orders'/'CED', 'false orders').
card_uid('false orders'/'CED', 'CED:False Orders:false orders').
card_rarity('false orders'/'CED', 'Common').
card_artist('false orders'/'CED', 'Anson Maddocks').

card_in_set('farmstead', 'CED').
card_original_type('farmstead'/'CED', 'Enchantment — Aura').
card_original_text('farmstead'/'CED', '').
card_image_name('farmstead'/'CED', 'farmstead').
card_uid('farmstead'/'CED', 'CED:Farmstead:farmstead').
card_rarity('farmstead'/'CED', 'Rare').
card_artist('farmstead'/'CED', 'Mark Poole').

card_in_set('fastbond', 'CED').
card_original_type('fastbond'/'CED', 'Enchantment').
card_original_text('fastbond'/'CED', '').
card_image_name('fastbond'/'CED', 'fastbond').
card_uid('fastbond'/'CED', 'CED:Fastbond:fastbond').
card_rarity('fastbond'/'CED', 'Rare').
card_artist('fastbond'/'CED', 'Mark Poole').

card_in_set('fear', 'CED').
card_original_type('fear'/'CED', 'Enchantment — Aura').
card_original_text('fear'/'CED', '').
card_image_name('fear'/'CED', 'fear').
card_uid('fear'/'CED', 'CED:Fear:fear').
card_rarity('fear'/'CED', 'Common').
card_artist('fear'/'CED', 'Mark Poole').

card_in_set('feedback', 'CED').
card_original_type('feedback'/'CED', 'Enchantment — Aura').
card_original_text('feedback'/'CED', '').
card_image_name('feedback'/'CED', 'feedback').
card_uid('feedback'/'CED', 'CED:Feedback:feedback').
card_rarity('feedback'/'CED', 'Uncommon').
card_artist('feedback'/'CED', 'Quinton Hoover').

card_in_set('fire elemental', 'CED').
card_original_type('fire elemental'/'CED', 'Creature — Elemental').
card_original_text('fire elemental'/'CED', '').
card_image_name('fire elemental'/'CED', 'fire elemental').
card_uid('fire elemental'/'CED', 'CED:Fire Elemental:fire elemental').
card_rarity('fire elemental'/'CED', 'Uncommon').
card_artist('fire elemental'/'CED', 'Melissa A. Benson').
card_flavor_text('fire elemental'/'CED', 'Fire Elementals are ruthless infernos, annihilating and consuming their foes in a frenzied holocaust. Crackling and blazing, they sear swift, terrible paths, leaving the land charred and scorched in their wake.').

card_in_set('fireball', 'CED').
card_original_type('fireball'/'CED', 'Sorcery').
card_original_text('fireball'/'CED', '').
card_image_name('fireball'/'CED', 'fireball').
card_uid('fireball'/'CED', 'CED:Fireball:fireball').
card_rarity('fireball'/'CED', 'Common').
card_artist('fireball'/'CED', 'Mark Tedin').

card_in_set('firebreathing', 'CED').
card_original_type('firebreathing'/'CED', 'Enchantment — Aura').
card_original_text('firebreathing'/'CED', '').
card_image_name('firebreathing'/'CED', 'firebreathing').
card_uid('firebreathing'/'CED', 'CED:Firebreathing:firebreathing').
card_rarity('firebreathing'/'CED', 'Common').
card_artist('firebreathing'/'CED', 'Dan Frazier').
card_flavor_text('firebreathing'/'CED', '\"And topples round the dreary west\nA looming bastion fringed with fire.\"\n—Alfred, Lord Tennyson, \"In Memoriam\"').

card_in_set('flashfires', 'CED').
card_original_type('flashfires'/'CED', 'Sorcery').
card_original_text('flashfires'/'CED', '').
card_image_name('flashfires'/'CED', 'flashfires').
card_uid('flashfires'/'CED', 'CED:Flashfires:flashfires').
card_rarity('flashfires'/'CED', 'Uncommon').
card_artist('flashfires'/'CED', 'Dameon Willich').

card_in_set('flight', 'CED').
card_original_type('flight'/'CED', 'Enchantment — Aura').
card_original_text('flight'/'CED', '').
card_image_name('flight'/'CED', 'flight').
card_uid('flight'/'CED', 'CED:Flight:flight').
card_rarity('flight'/'CED', 'Common').
card_artist('flight'/'CED', 'Anson Maddocks').

card_in_set('fog', 'CED').
card_original_type('fog'/'CED', 'Instant').
card_original_text('fog'/'CED', '').
card_image_name('fog'/'CED', 'fog').
card_uid('fog'/'CED', 'CED:Fog:fog').
card_rarity('fog'/'CED', 'Common').
card_artist('fog'/'CED', 'Jesper Myrfors').

card_in_set('force of nature', 'CED').
card_original_type('force of nature'/'CED', 'Creature — Elemental').
card_original_text('force of nature'/'CED', '').
card_image_name('force of nature'/'CED', 'force of nature').
card_uid('force of nature'/'CED', 'CED:Force of Nature:force of nature').
card_rarity('force of nature'/'CED', 'Rare').
card_artist('force of nature'/'CED', 'Douglas Shuler').

card_in_set('forcefield', 'CED').
card_original_type('forcefield'/'CED', 'Artifact').
card_original_text('forcefield'/'CED', '').
card_image_name('forcefield'/'CED', 'forcefield').
card_uid('forcefield'/'CED', 'CED:Forcefield:forcefield').
card_rarity('forcefield'/'CED', 'Rare').
card_artist('forcefield'/'CED', 'Dan Frazier').

card_in_set('forest', 'CED').
card_original_type('forest'/'CED', 'Basic Land — Forest').
card_original_text('forest'/'CED', '').
card_image_name('forest'/'CED', 'forest1').
card_uid('forest'/'CED', 'CED:Forest:forest1').
card_rarity('forest'/'CED', 'Basic Land').
card_artist('forest'/'CED', 'Christopher Rush').

card_in_set('forest', 'CED').
card_original_type('forest'/'CED', 'Basic Land — Forest').
card_original_text('forest'/'CED', '').
card_image_name('forest'/'CED', 'forest2').
card_uid('forest'/'CED', 'CED:Forest:forest2').
card_rarity('forest'/'CED', 'Basic Land').
card_artist('forest'/'CED', 'Christopher Rush').

card_in_set('forest', 'CED').
card_original_type('forest'/'CED', 'Basic Land — Forest').
card_original_text('forest'/'CED', '').
card_image_name('forest'/'CED', 'forest3').
card_uid('forest'/'CED', 'CED:Forest:forest3').
card_rarity('forest'/'CED', 'Basic Land').
card_artist('forest'/'CED', 'Christopher Rush').

card_in_set('fork', 'CED').
card_original_type('fork'/'CED', 'Instant').
card_original_text('fork'/'CED', '').
card_image_name('fork'/'CED', 'fork').
card_uid('fork'/'CED', 'CED:Fork:fork').
card_rarity('fork'/'CED', 'Rare').
card_artist('fork'/'CED', 'Amy Weber').

card_in_set('frozen shade', 'CED').
card_original_type('frozen shade'/'CED', 'Creature — Shade').
card_original_text('frozen shade'/'CED', '').
card_image_name('frozen shade'/'CED', 'frozen shade').
card_uid('frozen shade'/'CED', 'CED:Frozen Shade:frozen shade').
card_rarity('frozen shade'/'CED', 'Common').
card_artist('frozen shade'/'CED', 'Douglas Shuler').
card_flavor_text('frozen shade'/'CED', '\"There are some qualities—some incorporate things,/ That have a double life, which thus is made/ A type of twin entity which springs/ From matter and light, evinced in solid and shade.\"\n—Edgar Allan Poe, \"Silence\"').

card_in_set('fungusaur', 'CED').
card_original_type('fungusaur'/'CED', 'Creature — Fungus Lizard').
card_original_text('fungusaur'/'CED', '').
card_image_name('fungusaur'/'CED', 'fungusaur').
card_uid('fungusaur'/'CED', 'CED:Fungusaur:fungusaur').
card_rarity('fungusaur'/'CED', 'Rare').
card_artist('fungusaur'/'CED', 'Daniel Gelon').
card_flavor_text('fungusaur'/'CED', 'Rather than sheltering her young, the female Fungusaur often injures her own offspring, thereby ensuring their rapid growth.').

card_in_set('gaea\'s liege', 'CED').
card_original_type('gaea\'s liege'/'CED', 'Creature — Avatar').
card_original_text('gaea\'s liege'/'CED', '').
card_image_name('gaea\'s liege'/'CED', 'gaea\'s liege').
card_uid('gaea\'s liege'/'CED', 'CED:Gaea\'s Liege:gaea\'s liege').
card_rarity('gaea\'s liege'/'CED', 'Rare').
card_artist('gaea\'s liege'/'CED', 'Dameon Willich').

card_in_set('gauntlet of might', 'CED').
card_original_type('gauntlet of might'/'CED', 'Artifact').
card_original_text('gauntlet of might'/'CED', '').
card_image_name('gauntlet of might'/'CED', 'gauntlet of might').
card_uid('gauntlet of might'/'CED', 'CED:Gauntlet of Might:gauntlet of might').
card_rarity('gauntlet of might'/'CED', 'Rare').
card_artist('gauntlet of might'/'CED', 'Christopher Rush').

card_in_set('giant growth', 'CED').
card_original_type('giant growth'/'CED', 'Instant').
card_original_text('giant growth'/'CED', '').
card_image_name('giant growth'/'CED', 'giant growth').
card_uid('giant growth'/'CED', 'CED:Giant Growth:giant growth').
card_rarity('giant growth'/'CED', 'Common').
card_artist('giant growth'/'CED', 'Sandra Everingham').

card_in_set('giant spider', 'CED').
card_original_type('giant spider'/'CED', 'Creature — Spider').
card_original_text('giant spider'/'CED', '').
card_image_name('giant spider'/'CED', 'giant spider').
card_uid('giant spider'/'CED', 'CED:Giant Spider:giant spider').
card_rarity('giant spider'/'CED', 'Common').
card_artist('giant spider'/'CED', 'Sandra Everingham').
card_flavor_text('giant spider'/'CED', 'While it possesses potent venom, the Giant Spider often chooses not to paralyze its victims. Perhaps the creature enjoys the gentle rocking motion caused by its captives\' struggles to escape its web.').

card_in_set('glasses of urza', 'CED').
card_original_type('glasses of urza'/'CED', 'Artifact').
card_original_text('glasses of urza'/'CED', '').
card_image_name('glasses of urza'/'CED', 'glasses of urza').
card_uid('glasses of urza'/'CED', 'CED:Glasses of Urza:glasses of urza').
card_rarity('glasses of urza'/'CED', 'Uncommon').
card_artist('glasses of urza'/'CED', 'Douglas Shuler').

card_in_set('gloom', 'CED').
card_original_type('gloom'/'CED', 'Enchantment').
card_original_text('gloom'/'CED', '').
card_image_name('gloom'/'CED', 'gloom').
card_uid('gloom'/'CED', 'CED:Gloom:gloom').
card_rarity('gloom'/'CED', 'Uncommon').
card_artist('gloom'/'CED', 'Dan Frazier').

card_in_set('goblin balloon brigade', 'CED').
card_original_type('goblin balloon brigade'/'CED', 'Creature — Goblin Warrior').
card_original_text('goblin balloon brigade'/'CED', '').
card_image_name('goblin balloon brigade'/'CED', 'goblin balloon brigade').
card_uid('goblin balloon brigade'/'CED', 'CED:Goblin Balloon Brigade:goblin balloon brigade').
card_rarity('goblin balloon brigade'/'CED', 'Uncommon').
card_artist('goblin balloon brigade'/'CED', 'Andi Rusu').
card_flavor_text('goblin balloon brigade'/'CED', '\"From up here we can drop rocks and arrows and more rocks!\" \"Uh, yeah boss, but how do we get down?\"').

card_in_set('goblin king', 'CED').
card_original_type('goblin king'/'CED', 'Creature — Goblin').
card_original_text('goblin king'/'CED', '').
card_image_name('goblin king'/'CED', 'goblin king').
card_uid('goblin king'/'CED', 'CED:Goblin King:goblin king').
card_rarity('goblin king'/'CED', 'Rare').
card_artist('goblin king'/'CED', 'Jesper Myrfors').
card_flavor_text('goblin king'/'CED', 'To become king of the Goblins, one must assassinate the previous king. Thus, only the most foolish seek positions of leadership.').

card_in_set('granite gargoyle', 'CED').
card_original_type('granite gargoyle'/'CED', 'Creature — Gargoyle').
card_original_text('granite gargoyle'/'CED', '').
card_image_name('granite gargoyle'/'CED', 'granite gargoyle').
card_uid('granite gargoyle'/'CED', 'CED:Granite Gargoyle:granite gargoyle').
card_rarity('granite gargoyle'/'CED', 'Rare').
card_artist('granite gargoyle'/'CED', 'Christopher Rush').
card_flavor_text('granite gargoyle'/'CED', '\"While most overworlders fortunately don\'t realize this, Gargoyles can be most delicious, providing you have the appropriate tools to carve them.\"\n—The Underworld Cookbook by Asmoranomardicadaistinaculdacar').

card_in_set('gray ogre', 'CED').
card_original_type('gray ogre'/'CED', 'Creature — Ogre').
card_original_text('gray ogre'/'CED', '').
card_image_name('gray ogre'/'CED', 'gray ogre').
card_uid('gray ogre'/'CED', 'CED:Gray Ogre:gray ogre').
card_rarity('gray ogre'/'CED', 'Common').
card_artist('gray ogre'/'CED', 'Dan Frazier').
card_flavor_text('gray ogre'/'CED', 'The Ogre philosopher Gnerdel believed the purpose of life was to live as high on the food chain as possible. She refused to eat vegetarians, and preferred to live entirely on creatures that preyed on sentient beings.').

card_in_set('green ward', 'CED').
card_original_type('green ward'/'CED', 'Enchantment — Aura').
card_original_text('green ward'/'CED', '').
card_image_name('green ward'/'CED', 'green ward').
card_uid('green ward'/'CED', 'CED:Green Ward:green ward').
card_rarity('green ward'/'CED', 'Uncommon').
card_artist('green ward'/'CED', 'Dan Frazier').

card_in_set('grizzly bears', 'CED').
card_original_type('grizzly bears'/'CED', 'Creature — Bear').
card_original_text('grizzly bears'/'CED', '').
card_image_name('grizzly bears'/'CED', 'grizzly bears').
card_uid('grizzly bears'/'CED', 'CED:Grizzly Bears:grizzly bears').
card_rarity('grizzly bears'/'CED', 'Common').
card_artist('grizzly bears'/'CED', 'Jeff A. Menges').
card_flavor_text('grizzly bears'/'CED', 'Don\'t try to outrun one of Dominia\'s Grizzlies; it\'ll catch you, knock you down, and eat you. Of course, you could run up a tree. In that case you\'ll get a nice view before it knocks the tree down and eats you.').

card_in_set('guardian angel', 'CED').
card_original_type('guardian angel'/'CED', 'Instant').
card_original_text('guardian angel'/'CED', '').
card_image_name('guardian angel'/'CED', 'guardian angel').
card_uid('guardian angel'/'CED', 'CED:Guardian Angel:guardian angel').
card_rarity('guardian angel'/'CED', 'Common').
card_artist('guardian angel'/'CED', 'Anson Maddocks').

card_in_set('healing salve', 'CED').
card_original_type('healing salve'/'CED', 'Instant').
card_original_text('healing salve'/'CED', '').
card_image_name('healing salve'/'CED', 'healing salve').
card_uid('healing salve'/'CED', 'CED:Healing Salve:healing salve').
card_rarity('healing salve'/'CED', 'Common').
card_artist('healing salve'/'CED', 'Dan Frazier').

card_in_set('helm of chatzuk', 'CED').
card_original_type('helm of chatzuk'/'CED', 'Artifact').
card_original_text('helm of chatzuk'/'CED', '').
card_image_name('helm of chatzuk'/'CED', 'helm of chatzuk').
card_uid('helm of chatzuk'/'CED', 'CED:Helm of Chatzuk:helm of chatzuk').
card_rarity('helm of chatzuk'/'CED', 'Rare').
card_artist('helm of chatzuk'/'CED', 'Mark Tedin').

card_in_set('hill giant', 'CED').
card_original_type('hill giant'/'CED', 'Creature — Giant').
card_original_text('hill giant'/'CED', '').
card_image_name('hill giant'/'CED', 'hill giant').
card_uid('hill giant'/'CED', 'CED:Hill Giant:hill giant').
card_rarity('hill giant'/'CED', 'Common').
card_artist('hill giant'/'CED', 'Dan Frazier').
card_flavor_text('hill giant'/'CED', 'Fortunately, Hill Giants have large blind spots in which a human can easily hide. Unfortunately, these blind spots are beneath the bottoms of their feet.').

card_in_set('holy armor', 'CED').
card_original_type('holy armor'/'CED', 'Enchantment — Aura').
card_original_text('holy armor'/'CED', '').
card_image_name('holy armor'/'CED', 'holy armor').
card_uid('holy armor'/'CED', 'CED:Holy Armor:holy armor').
card_rarity('holy armor'/'CED', 'Common').
card_artist('holy armor'/'CED', 'Melissa A. Benson').

card_in_set('holy strength', 'CED').
card_original_type('holy strength'/'CED', 'Enchantment — Aura').
card_original_text('holy strength'/'CED', '').
card_image_name('holy strength'/'CED', 'holy strength').
card_uid('holy strength'/'CED', 'CED:Holy Strength:holy strength').
card_rarity('holy strength'/'CED', 'Common').
card_artist('holy strength'/'CED', 'Anson Maddocks').

card_in_set('howl from beyond', 'CED').
card_original_type('howl from beyond'/'CED', 'Instant').
card_original_text('howl from beyond'/'CED', '').
card_image_name('howl from beyond'/'CED', 'howl from beyond').
card_uid('howl from beyond'/'CED', 'CED:Howl from Beyond:howl from beyond').
card_rarity('howl from beyond'/'CED', 'Common').
card_artist('howl from beyond'/'CED', 'Mark Poole').

card_in_set('howling mine', 'CED').
card_original_type('howling mine'/'CED', 'Artifact').
card_original_text('howling mine'/'CED', '').
card_image_name('howling mine'/'CED', 'howling mine').
card_uid('howling mine'/'CED', 'CED:Howling Mine:howling mine').
card_rarity('howling mine'/'CED', 'Rare').
card_artist('howling mine'/'CED', 'Mark Poole').

card_in_set('hurloon minotaur', 'CED').
card_original_type('hurloon minotaur'/'CED', 'Creature — Minotaur').
card_original_text('hurloon minotaur'/'CED', '').
card_image_name('hurloon minotaur'/'CED', 'hurloon minotaur').
card_uid('hurloon minotaur'/'CED', 'CED:Hurloon Minotaur:hurloon minotaur').
card_rarity('hurloon minotaur'/'CED', 'Common').
card_artist('hurloon minotaur'/'CED', 'Anson Maddocks').
card_flavor_text('hurloon minotaur'/'CED', 'The Minotaurs of the Hurloon Mountains are known for their love of battle. They are also known for their hymns to the dead, sung for friend and foe alike. These hymns can last for days, filling the mountain valleys with their low, haunting sounds.').

card_in_set('hurricane', 'CED').
card_original_type('hurricane'/'CED', 'Sorcery').
card_original_text('hurricane'/'CED', '').
card_image_name('hurricane'/'CED', 'hurricane').
card_uid('hurricane'/'CED', 'CED:Hurricane:hurricane').
card_rarity('hurricane'/'CED', 'Uncommon').
card_artist('hurricane'/'CED', 'Dameon Willich').

card_in_set('hypnotic specter', 'CED').
card_original_type('hypnotic specter'/'CED', 'Creature — Specter').
card_original_text('hypnotic specter'/'CED', '').
card_image_name('hypnotic specter'/'CED', 'hypnotic specter').
card_uid('hypnotic specter'/'CED', 'CED:Hypnotic Specter:hypnotic specter').
card_rarity('hypnotic specter'/'CED', 'Uncommon').
card_artist('hypnotic specter'/'CED', 'Douglas Shuler').
card_flavor_text('hypnotic specter'/'CED', '\"...There was no trace/ Of aught on that illumined face...\"\n—Samuel Coleridge, \"Phantom\"').

card_in_set('ice storm', 'CED').
card_original_type('ice storm'/'CED', 'Sorcery').
card_original_text('ice storm'/'CED', '').
card_image_name('ice storm'/'CED', 'ice storm').
card_uid('ice storm'/'CED', 'CED:Ice Storm:ice storm').
card_rarity('ice storm'/'CED', 'Uncommon').
card_artist('ice storm'/'CED', 'Dan Frazier').

card_in_set('icy manipulator', 'CED').
card_original_type('icy manipulator'/'CED', 'Artifact').
card_original_text('icy manipulator'/'CED', '').
card_image_name('icy manipulator'/'CED', 'icy manipulator').
card_uid('icy manipulator'/'CED', 'CED:Icy Manipulator:icy manipulator').
card_rarity('icy manipulator'/'CED', 'Uncommon').
card_artist('icy manipulator'/'CED', 'Douglas Shuler').

card_in_set('illusionary mask', 'CED').
card_original_type('illusionary mask'/'CED', 'Artifact').
card_original_text('illusionary mask'/'CED', '').
card_image_name('illusionary mask'/'CED', 'illusionary mask').
card_uid('illusionary mask'/'CED', 'CED:Illusionary Mask:illusionary mask').
card_rarity('illusionary mask'/'CED', 'Rare').
card_artist('illusionary mask'/'CED', 'Amy Weber').

card_in_set('instill energy', 'CED').
card_original_type('instill energy'/'CED', 'Enchantment — Aura').
card_original_text('instill energy'/'CED', '').
card_image_name('instill energy'/'CED', 'instill energy').
card_uid('instill energy'/'CED', 'CED:Instill Energy:instill energy').
card_rarity('instill energy'/'CED', 'Uncommon').
card_artist('instill energy'/'CED', 'Dameon Willich').

card_in_set('invisibility', 'CED').
card_original_type('invisibility'/'CED', 'Enchantment — Aura').
card_original_text('invisibility'/'CED', '').
card_image_name('invisibility'/'CED', 'invisibility').
card_uid('invisibility'/'CED', 'CED:Invisibility:invisibility').
card_rarity('invisibility'/'CED', 'Common').
card_artist('invisibility'/'CED', 'Anson Maddocks').

card_in_set('iron star', 'CED').
card_original_type('iron star'/'CED', 'Artifact').
card_original_text('iron star'/'CED', '').
card_image_name('iron star'/'CED', 'iron star').
card_uid('iron star'/'CED', 'CED:Iron Star:iron star').
card_rarity('iron star'/'CED', 'Uncommon').
card_artist('iron star'/'CED', 'Dan Frazier').

card_in_set('ironclaw orcs', 'CED').
card_original_type('ironclaw orcs'/'CED', 'Creature — Orc').
card_original_text('ironclaw orcs'/'CED', '').
card_image_name('ironclaw orcs'/'CED', 'ironclaw orcs').
card_uid('ironclaw orcs'/'CED', 'CED:Ironclaw Orcs:ironclaw orcs').
card_rarity('ironclaw orcs'/'CED', 'Common').
card_artist('ironclaw orcs'/'CED', 'Anson Maddocks').
card_flavor_text('ironclaw orcs'/'CED', 'Generations of genetic weeding have given rise to the deviously cowardly Ironclaw clan. To say that Orcs in general are vicious, depraved, and ignoble does not do justice to the Ironclaw.').

card_in_set('ironroot treefolk', 'CED').
card_original_type('ironroot treefolk'/'CED', 'Creature — Treefolk').
card_original_text('ironroot treefolk'/'CED', '').
card_image_name('ironroot treefolk'/'CED', 'ironroot treefolk').
card_uid('ironroot treefolk'/'CED', 'CED:Ironroot Treefolk:ironroot treefolk').
card_rarity('ironroot treefolk'/'CED', 'Common').
card_artist('ironroot treefolk'/'CED', 'Jesper Myrfors').
card_flavor_text('ironroot treefolk'/'CED', 'The mating habits of Treefolk, particularly the stalwart Ironroot Treefolk, are truly absurd. Molasses comes to mind. It\'s amazing the species can survive at all given such protracted periods of mate selection, conjugation, and gestation.').

card_in_set('island', 'CED').
card_original_type('island'/'CED', 'Basic Land — Island').
card_original_text('island'/'CED', '').
card_image_name('island'/'CED', 'island1').
card_uid('island'/'CED', 'CED:Island:island1').
card_rarity('island'/'CED', 'Basic Land').
card_artist('island'/'CED', 'Mark Poole').

card_in_set('island', 'CED').
card_original_type('island'/'CED', 'Basic Land — Island').
card_original_text('island'/'CED', '').
card_image_name('island'/'CED', 'island2').
card_uid('island'/'CED', 'CED:Island:island2').
card_rarity('island'/'CED', 'Basic Land').
card_artist('island'/'CED', 'Mark Poole').

card_in_set('island', 'CED').
card_original_type('island'/'CED', 'Basic Land — Island').
card_original_text('island'/'CED', '').
card_image_name('island'/'CED', 'island3').
card_uid('island'/'CED', 'CED:Island:island3').
card_rarity('island'/'CED', 'Basic Land').
card_artist('island'/'CED', 'Mark Poole').

card_in_set('island sanctuary', 'CED').
card_original_type('island sanctuary'/'CED', 'Enchantment').
card_original_text('island sanctuary'/'CED', '').
card_image_name('island sanctuary'/'CED', 'island sanctuary').
card_uid('island sanctuary'/'CED', 'CED:Island Sanctuary:island sanctuary').
card_rarity('island sanctuary'/'CED', 'Rare').
card_artist('island sanctuary'/'CED', 'Mark Poole').

card_in_set('ivory cup', 'CED').
card_original_type('ivory cup'/'CED', 'Artifact').
card_original_text('ivory cup'/'CED', '').
card_image_name('ivory cup'/'CED', 'ivory cup').
card_uid('ivory cup'/'CED', 'CED:Ivory Cup:ivory cup').
card_rarity('ivory cup'/'CED', 'Uncommon').
card_artist('ivory cup'/'CED', 'Anson Maddocks').

card_in_set('jade monolith', 'CED').
card_original_type('jade monolith'/'CED', 'Artifact').
card_original_text('jade monolith'/'CED', '').
card_image_name('jade monolith'/'CED', 'jade monolith').
card_uid('jade monolith'/'CED', 'CED:Jade Monolith:jade monolith').
card_rarity('jade monolith'/'CED', 'Rare').
card_artist('jade monolith'/'CED', 'Anson Maddocks').

card_in_set('jade statue', 'CED').
card_original_type('jade statue'/'CED', 'Artifact').
card_original_text('jade statue'/'CED', '').
card_image_name('jade statue'/'CED', 'jade statue').
card_uid('jade statue'/'CED', 'CED:Jade Statue:jade statue').
card_rarity('jade statue'/'CED', 'Uncommon').
card_artist('jade statue'/'CED', 'Dan Frazier').
card_flavor_text('jade statue'/'CED', '\"Some of the other guys dared me to touch it, but I knew it weren\'t no ordinary hunk o\' rock.\"\n—Norin the Wary').

card_in_set('jayemdae tome', 'CED').
card_original_type('jayemdae tome'/'CED', 'Artifact').
card_original_text('jayemdae tome'/'CED', '').
card_image_name('jayemdae tome'/'CED', 'jayemdae tome').
card_uid('jayemdae tome'/'CED', 'CED:Jayemdae Tome:jayemdae tome').
card_rarity('jayemdae tome'/'CED', 'Rare').
card_artist('jayemdae tome'/'CED', 'Mark Tedin').

card_in_set('juggernaut', 'CED').
card_original_type('juggernaut'/'CED', 'Artifact Creature — Juggernaut').
card_original_text('juggernaut'/'CED', '').
card_image_name('juggernaut'/'CED', 'juggernaut').
card_uid('juggernaut'/'CED', 'CED:Juggernaut:juggernaut').
card_rarity('juggernaut'/'CED', 'Uncommon').
card_artist('juggernaut'/'CED', 'Dan Frazier').
card_flavor_text('juggernaut'/'CED', 'We had taken refuge in a small cave, thinking the entrance was too narrow for it to follow. To our horror, its gigantic head smashed into the mountainside, ripping itself a new entrance.').

card_in_set('jump', 'CED').
card_original_type('jump'/'CED', 'Instant').
card_original_text('jump'/'CED', '').
card_image_name('jump'/'CED', 'jump').
card_uid('jump'/'CED', 'CED:Jump:jump').
card_rarity('jump'/'CED', 'Common').
card_artist('jump'/'CED', 'Mark Poole').

card_in_set('karma', 'CED').
card_original_type('karma'/'CED', 'Enchantment').
card_original_text('karma'/'CED', '').
card_image_name('karma'/'CED', 'karma').
card_uid('karma'/'CED', 'CED:Karma:karma').
card_rarity('karma'/'CED', 'Uncommon').
card_artist('karma'/'CED', 'Richard Thomas').

card_in_set('keldon warlord', 'CED').
card_original_type('keldon warlord'/'CED', 'Creature — Human Barbarian').
card_original_text('keldon warlord'/'CED', '').
card_image_name('keldon warlord'/'CED', 'keldon warlord').
card_uid('keldon warlord'/'CED', 'CED:Keldon Warlord:keldon warlord').
card_rarity('keldon warlord'/'CED', 'Uncommon').
card_artist('keldon warlord'/'CED', 'Kev Brockschmidt').

card_in_set('kormus bell', 'CED').
card_original_type('kormus bell'/'CED', 'Artifact').
card_original_text('kormus bell'/'CED', '').
card_image_name('kormus bell'/'CED', 'kormus bell').
card_uid('kormus bell'/'CED', 'CED:Kormus Bell:kormus bell').
card_rarity('kormus bell'/'CED', 'Rare').
card_artist('kormus bell'/'CED', 'Christopher Rush').

card_in_set('kudzu', 'CED').
card_original_type('kudzu'/'CED', 'Enchantment — Aura').
card_original_text('kudzu'/'CED', '').
card_image_name('kudzu'/'CED', 'kudzu').
card_uid('kudzu'/'CED', 'CED:Kudzu:kudzu').
card_rarity('kudzu'/'CED', 'Rare').
card_artist('kudzu'/'CED', 'Mark Poole').

card_in_set('lance', 'CED').
card_original_type('lance'/'CED', 'Enchantment — Aura').
card_original_text('lance'/'CED', '').
card_image_name('lance'/'CED', 'lance').
card_uid('lance'/'CED', 'CED:Lance:lance').
card_rarity('lance'/'CED', 'Uncommon').
card_artist('lance'/'CED', 'Rob Alexander').

card_in_set('ley druid', 'CED').
card_original_type('ley druid'/'CED', 'Creature — Human Druid').
card_original_text('ley druid'/'CED', '').
card_image_name('ley druid'/'CED', 'ley druid').
card_uid('ley druid'/'CED', 'CED:Ley Druid:ley druid').
card_rarity('ley druid'/'CED', 'Uncommon').
card_artist('ley druid'/'CED', 'Sandra Everingham').
card_flavor_text('ley druid'/'CED', 'After years of training, the Druid becomes one with nature, drawing power from the land and returning it when needed.').

card_in_set('library of leng', 'CED').
card_original_type('library of leng'/'CED', 'Artifact').
card_original_text('library of leng'/'CED', '').
card_image_name('library of leng'/'CED', 'library of leng').
card_uid('library of leng'/'CED', 'CED:Library of Leng:library of leng').
card_rarity('library of leng'/'CED', 'Uncommon').
card_artist('library of leng'/'CED', 'Daniel Gelon').

card_in_set('lich', 'CED').
card_original_type('lich'/'CED', 'Enchantment').
card_original_text('lich'/'CED', '').
card_image_name('lich'/'CED', 'lich').
card_uid('lich'/'CED', 'CED:Lich:lich').
card_rarity('lich'/'CED', 'Rare').
card_artist('lich'/'CED', 'Daniel Gelon').

card_in_set('lifeforce', 'CED').
card_original_type('lifeforce'/'CED', 'Enchantment').
card_original_text('lifeforce'/'CED', '').
card_image_name('lifeforce'/'CED', 'lifeforce').
card_uid('lifeforce'/'CED', 'CED:Lifeforce:lifeforce').
card_rarity('lifeforce'/'CED', 'Uncommon').
card_artist('lifeforce'/'CED', 'Dameon Willich').

card_in_set('lifelace', 'CED').
card_original_type('lifelace'/'CED', 'Instant').
card_original_text('lifelace'/'CED', '').
card_image_name('lifelace'/'CED', 'lifelace').
card_uid('lifelace'/'CED', 'CED:Lifelace:lifelace').
card_rarity('lifelace'/'CED', 'Rare').
card_artist('lifelace'/'CED', 'Amy Weber').

card_in_set('lifetap', 'CED').
card_original_type('lifetap'/'CED', 'Enchantment').
card_original_text('lifetap'/'CED', '').
card_image_name('lifetap'/'CED', 'lifetap').
card_uid('lifetap'/'CED', 'CED:Lifetap:lifetap').
card_rarity('lifetap'/'CED', 'Uncommon').
card_artist('lifetap'/'CED', 'Anson Maddocks').

card_in_set('lightning bolt', 'CED').
card_original_type('lightning bolt'/'CED', 'Instant').
card_original_text('lightning bolt'/'CED', '').
card_image_name('lightning bolt'/'CED', 'lightning bolt').
card_uid('lightning bolt'/'CED', 'CED:Lightning Bolt:lightning bolt').
card_rarity('lightning bolt'/'CED', 'Common').
card_artist('lightning bolt'/'CED', 'Christopher Rush').

card_in_set('living artifact', 'CED').
card_original_type('living artifact'/'CED', 'Enchantment — Aura').
card_original_text('living artifact'/'CED', '').
card_image_name('living artifact'/'CED', 'living artifact').
card_uid('living artifact'/'CED', 'CED:Living Artifact:living artifact').
card_rarity('living artifact'/'CED', 'Rare').
card_artist('living artifact'/'CED', 'Anson Maddocks').

card_in_set('living lands', 'CED').
card_original_type('living lands'/'CED', 'Enchantment').
card_original_text('living lands'/'CED', '').
card_image_name('living lands'/'CED', 'living lands').
card_uid('living lands'/'CED', 'CED:Living Lands:living lands').
card_rarity('living lands'/'CED', 'Rare').
card_artist('living lands'/'CED', 'Jesper Myrfors').

card_in_set('living wall', 'CED').
card_original_type('living wall'/'CED', 'Artifact Creature — Wall').
card_original_text('living wall'/'CED', '').
card_image_name('living wall'/'CED', 'living wall').
card_uid('living wall'/'CED', 'CED:Living Wall:living wall').
card_rarity('living wall'/'CED', 'Uncommon').
card_artist('living wall'/'CED', 'Anson Maddocks').
card_flavor_text('living wall'/'CED', 'Some fiendish mage had created a horrifying wall of living flesh, patched together from a jumble of still-recognizable body parts. As we sought to hew our way through it, some unknown power healed the gaping wounds we cut, denying us passage.').

card_in_set('llanowar elves', 'CED').
card_original_type('llanowar elves'/'CED', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'CED', '').
card_image_name('llanowar elves'/'CED', 'llanowar elves').
card_uid('llanowar elves'/'CED', 'CED:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'CED', 'Common').
card_artist('llanowar elves'/'CED', 'Anson Maddocks').
card_flavor_text('llanowar elves'/'CED', 'Whenever the Llanowar Elves gather the fruits of their forest, they leave one plant of each type untouched, considering that nature\'s portion.').

card_in_set('lord of atlantis', 'CED').
card_original_type('lord of atlantis'/'CED', 'Creature — Merfolk').
card_original_text('lord of atlantis'/'CED', '').
card_image_name('lord of atlantis'/'CED', 'lord of atlantis').
card_uid('lord of atlantis'/'CED', 'CED:Lord of Atlantis:lord of atlantis').
card_rarity('lord of atlantis'/'CED', 'Rare').
card_artist('lord of atlantis'/'CED', 'Melissa A. Benson').
card_flavor_text('lord of atlantis'/'CED', 'A master of tactics, the Lord of Atlantis makes his people bold in battle merely by arriving to lead them.').

card_in_set('lord of the pit', 'CED').
card_original_type('lord of the pit'/'CED', 'Creature — Demon').
card_original_text('lord of the pit'/'CED', '').
card_image_name('lord of the pit'/'CED', 'lord of the pit').
card_uid('lord of the pit'/'CED', 'CED:Lord of the Pit:lord of the pit').
card_rarity('lord of the pit'/'CED', 'Rare').
card_artist('lord of the pit'/'CED', 'Mark Tedin').

card_in_set('lure', 'CED').
card_original_type('lure'/'CED', 'Enchantment — Aura').
card_original_text('lure'/'CED', '').
card_image_name('lure'/'CED', 'lure').
card_uid('lure'/'CED', 'CED:Lure:lure').
card_rarity('lure'/'CED', 'Uncommon').
card_artist('lure'/'CED', 'Anson Maddocks').

card_in_set('magical hack', 'CED').
card_original_type('magical hack'/'CED', 'Instant').
card_original_text('magical hack'/'CED', '').
card_image_name('magical hack'/'CED', 'magical hack').
card_uid('magical hack'/'CED', 'CED:Magical Hack:magical hack').
card_rarity('magical hack'/'CED', 'Rare').
card_artist('magical hack'/'CED', 'Julie Baroh').

card_in_set('mahamoti djinn', 'CED').
card_original_type('mahamoti djinn'/'CED', 'Creature — Djinn').
card_original_text('mahamoti djinn'/'CED', '').
card_image_name('mahamoti djinn'/'CED', 'mahamoti djinn').
card_uid('mahamoti djinn'/'CED', 'CED:Mahamoti Djinn:mahamoti djinn').
card_rarity('mahamoti djinn'/'CED', 'Rare').
card_artist('mahamoti djinn'/'CED', 'Dan Frazier').
card_flavor_text('mahamoti djinn'/'CED', 'Of royal blood among the spirits of the air, the Mahamoti Djinn rides on the wings of the winds. As dangerous in the gambling hall as he is in battle, he is a master of trickery and misdirection.').

card_in_set('mana flare', 'CED').
card_original_type('mana flare'/'CED', 'Enchantment').
card_original_text('mana flare'/'CED', '').
card_image_name('mana flare'/'CED', 'mana flare').
card_uid('mana flare'/'CED', 'CED:Mana Flare:mana flare').
card_rarity('mana flare'/'CED', 'Rare').
card_artist('mana flare'/'CED', 'Christopher Rush').

card_in_set('mana short', 'CED').
card_original_type('mana short'/'CED', 'Instant').
card_original_text('mana short'/'CED', '').
card_image_name('mana short'/'CED', 'mana short').
card_uid('mana short'/'CED', 'CED:Mana Short:mana short').
card_rarity('mana short'/'CED', 'Rare').
card_artist('mana short'/'CED', 'Dameon Willich').

card_in_set('mana vault', 'CED').
card_original_type('mana vault'/'CED', 'Artifact').
card_original_text('mana vault'/'CED', '').
card_image_name('mana vault'/'CED', 'mana vault').
card_uid('mana vault'/'CED', 'CED:Mana Vault:mana vault').
card_rarity('mana vault'/'CED', 'Rare').
card_artist('mana vault'/'CED', 'Mark Tedin').

card_in_set('manabarbs', 'CED').
card_original_type('manabarbs'/'CED', 'Enchantment').
card_original_text('manabarbs'/'CED', '').
card_image_name('manabarbs'/'CED', 'manabarbs').
card_uid('manabarbs'/'CED', 'CED:Manabarbs:manabarbs').
card_rarity('manabarbs'/'CED', 'Rare').
card_artist('manabarbs'/'CED', 'Christopher Rush').

card_in_set('meekstone', 'CED').
card_original_type('meekstone'/'CED', 'Artifact').
card_original_text('meekstone'/'CED', '').
card_image_name('meekstone'/'CED', 'meekstone').
card_uid('meekstone'/'CED', 'CED:Meekstone:meekstone').
card_rarity('meekstone'/'CED', 'Rare').
card_artist('meekstone'/'CED', 'Quinton Hoover').

card_in_set('merfolk of the pearl trident', 'CED').
card_original_type('merfolk of the pearl trident'/'CED', 'Creature — Merfolk').
card_original_text('merfolk of the pearl trident'/'CED', '').
card_image_name('merfolk of the pearl trident'/'CED', 'merfolk of the pearl trident').
card_uid('merfolk of the pearl trident'/'CED', 'CED:Merfolk of the Pearl Trident:merfolk of the pearl trident').
card_rarity('merfolk of the pearl trident'/'CED', 'Common').
card_artist('merfolk of the pearl trident'/'CED', 'Jeff A. Menges').
card_flavor_text('merfolk of the pearl trident'/'CED', 'Most human scholars believe that Merfolk are the survivors of sunken Atlantis, humans adapted to the water. Merfolk, however, believe that humans sprang forth from Merfolk who adapted themselves in order to explore their last frontier.').

card_in_set('mesa pegasus', 'CED').
card_original_type('mesa pegasus'/'CED', 'Creature — Pegasus').
card_original_text('mesa pegasus'/'CED', '').
card_image_name('mesa pegasus'/'CED', 'mesa pegasus').
card_uid('mesa pegasus'/'CED', 'CED:Mesa Pegasus:mesa pegasus').
card_rarity('mesa pegasus'/'CED', 'Common').
card_artist('mesa pegasus'/'CED', 'Melissa A. Benson').
card_flavor_text('mesa pegasus'/'CED', 'Before a woman marries in the village of Sursi, she must visit the land of the Mesa Pegasus. Legend has it that if the woman is pure of heart and her love is true, a Mesa Pegasus will appear, blessing her family with long life and good fortune.').

card_in_set('mind twist', 'CED').
card_original_type('mind twist'/'CED', 'Sorcery').
card_original_text('mind twist'/'CED', '').
card_image_name('mind twist'/'CED', 'mind twist').
card_uid('mind twist'/'CED', 'CED:Mind Twist:mind twist').
card_rarity('mind twist'/'CED', 'Rare').
card_artist('mind twist'/'CED', 'Julie Baroh').

card_in_set('mons\'s goblin raiders', 'CED').
card_original_type('mons\'s goblin raiders'/'CED', 'Creature — Goblin').
card_original_text('mons\'s goblin raiders'/'CED', '').
card_image_name('mons\'s goblin raiders'/'CED', 'mons\'s goblin raiders').
card_uid('mons\'s goblin raiders'/'CED', 'CED:Mons\'s Goblin Raiders:mons\'s goblin raiders').
card_rarity('mons\'s goblin raiders'/'CED', 'Common').
card_artist('mons\'s goblin raiders'/'CED', 'Jeff A. Menges').
card_flavor_text('mons\'s goblin raiders'/'CED', 'The intricate dynamics of Rundvelt Goblin affairs are often confused with anarchy. The chaos, however, is the chaos of a thundercloud, and direction will sporadically and violently appear. Pashalik Mons and his raiders are the thunderhead that leads in the storm.').

card_in_set('mountain', 'CED').
card_original_type('mountain'/'CED', 'Basic Land — Mountain').
card_original_text('mountain'/'CED', '').
card_image_name('mountain'/'CED', 'mountain1').
card_uid('mountain'/'CED', 'CED:Mountain:mountain1').
card_rarity('mountain'/'CED', 'Basic Land').
card_artist('mountain'/'CED', 'Douglas Shuler').

card_in_set('mountain', 'CED').
card_original_type('mountain'/'CED', 'Basic Land — Mountain').
card_original_text('mountain'/'CED', '').
card_image_name('mountain'/'CED', 'mountain2').
card_uid('mountain'/'CED', 'CED:Mountain:mountain2').
card_rarity('mountain'/'CED', 'Basic Land').
card_artist('mountain'/'CED', 'Douglas Shuler').

card_in_set('mountain', 'CED').
card_original_type('mountain'/'CED', 'Basic Land — Mountain').
card_original_text('mountain'/'CED', '').
card_image_name('mountain'/'CED', 'mountain3').
card_uid('mountain'/'CED', 'CED:Mountain:mountain3').
card_rarity('mountain'/'CED', 'Basic Land').
card_artist('mountain'/'CED', 'Douglas Shuler').

card_in_set('mox emerald', 'CED').
card_original_type('mox emerald'/'CED', 'Artifact').
card_original_text('mox emerald'/'CED', '').
card_image_name('mox emerald'/'CED', 'mox emerald').
card_uid('mox emerald'/'CED', 'CED:Mox Emerald:mox emerald').
card_rarity('mox emerald'/'CED', 'Rare').
card_artist('mox emerald'/'CED', 'Dan Frazier').

card_in_set('mox jet', 'CED').
card_original_type('mox jet'/'CED', 'Artifact').
card_original_text('mox jet'/'CED', '').
card_image_name('mox jet'/'CED', 'mox jet').
card_uid('mox jet'/'CED', 'CED:Mox Jet:mox jet').
card_rarity('mox jet'/'CED', 'Rare').
card_artist('mox jet'/'CED', 'Dan Frazier').

card_in_set('mox pearl', 'CED').
card_original_type('mox pearl'/'CED', 'Artifact').
card_original_text('mox pearl'/'CED', '').
card_image_name('mox pearl'/'CED', 'mox pearl').
card_uid('mox pearl'/'CED', 'CED:Mox Pearl:mox pearl').
card_rarity('mox pearl'/'CED', 'Rare').
card_artist('mox pearl'/'CED', 'Dan Frazier').

card_in_set('mox ruby', 'CED').
card_original_type('mox ruby'/'CED', 'Artifact').
card_original_text('mox ruby'/'CED', '').
card_image_name('mox ruby'/'CED', 'mox ruby').
card_uid('mox ruby'/'CED', 'CED:Mox Ruby:mox ruby').
card_rarity('mox ruby'/'CED', 'Rare').
card_artist('mox ruby'/'CED', 'Dan Frazier').

card_in_set('mox sapphire', 'CED').
card_original_type('mox sapphire'/'CED', 'Artifact').
card_original_text('mox sapphire'/'CED', '').
card_image_name('mox sapphire'/'CED', 'mox sapphire').
card_uid('mox sapphire'/'CED', 'CED:Mox Sapphire:mox sapphire').
card_rarity('mox sapphire'/'CED', 'Rare').
card_artist('mox sapphire'/'CED', 'Dan Frazier').

card_in_set('natural selection', 'CED').
card_original_type('natural selection'/'CED', 'Instant').
card_original_text('natural selection'/'CED', '').
card_image_name('natural selection'/'CED', 'natural selection').
card_uid('natural selection'/'CED', 'CED:Natural Selection:natural selection').
card_rarity('natural selection'/'CED', 'Rare').
card_artist('natural selection'/'CED', 'Mark Poole').

card_in_set('nether shadow', 'CED').
card_original_type('nether shadow'/'CED', 'Creature — Spirit').
card_original_text('nether shadow'/'CED', '').
card_image_name('nether shadow'/'CED', 'nether shadow').
card_uid('nether shadow'/'CED', 'CED:Nether Shadow:nether shadow').
card_rarity('nether shadow'/'CED', 'Rare').
card_artist('nether shadow'/'CED', 'Christopher Rush').

card_in_set('nettling imp', 'CED').
card_original_type('nettling imp'/'CED', 'Creature — Imp').
card_original_text('nettling imp'/'CED', '').
card_image_name('nettling imp'/'CED', 'nettling imp').
card_uid('nettling imp'/'CED', 'CED:Nettling Imp:nettling imp').
card_rarity('nettling imp'/'CED', 'Uncommon').
card_artist('nettling imp'/'CED', 'Quinton Hoover').

card_in_set('nevinyrral\'s disk', 'CED').
card_original_type('nevinyrral\'s disk'/'CED', 'Artifact').
card_original_text('nevinyrral\'s disk'/'CED', '').
card_image_name('nevinyrral\'s disk'/'CED', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'CED', 'CED:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'CED', 'Rare').
card_artist('nevinyrral\'s disk'/'CED', 'Mark Tedin').

card_in_set('nightmare', 'CED').
card_original_type('nightmare'/'CED', 'Creature — Nightmare Horse').
card_original_text('nightmare'/'CED', '').
card_image_name('nightmare'/'CED', 'nightmare').
card_uid('nightmare'/'CED', 'CED:Nightmare:nightmare').
card_rarity('nightmare'/'CED', 'Rare').
card_artist('nightmare'/'CED', 'Melissa A. Benson').
card_flavor_text('nightmare'/'CED', 'The Nightmare arises from its lair in the swamps. As the poisoned land spreads, so does the Nightmare\'s rage and terrifying strength.').

card_in_set('northern paladin', 'CED').
card_original_type('northern paladin'/'CED', 'Creature — Human Knight').
card_original_text('northern paladin'/'CED', '').
card_image_name('northern paladin'/'CED', 'northern paladin').
card_uid('northern paladin'/'CED', 'CED:Northern Paladin:northern paladin').
card_rarity('northern paladin'/'CED', 'Rare').
card_artist('northern paladin'/'CED', 'Douglas Shuler').
card_flavor_text('northern paladin'/'CED', '\"Look to the north; there you will find aid and comfort.\"\n—The Book of Tal').

card_in_set('obsianus golem', 'CED').
card_original_type('obsianus golem'/'CED', 'Artifact Creature — Golem').
card_original_text('obsianus golem'/'CED', '').
card_image_name('obsianus golem'/'CED', 'obsianus golem').
card_uid('obsianus golem'/'CED', 'CED:Obsianus Golem:obsianus golem').
card_rarity('obsianus golem'/'CED', 'Uncommon').
card_artist('obsianus golem'/'CED', 'Jesper Myrfors').
card_flavor_text('obsianus golem'/'CED', '\"The foot stone is connected to the ankle stone, the ankle stone is connected to the leg stone . . .\"\n—Song of the Artificer').

card_in_set('orcish artillery', 'CED').
card_original_type('orcish artillery'/'CED', 'Creature — Orc Warrior').
card_original_text('orcish artillery'/'CED', '').
card_image_name('orcish artillery'/'CED', 'orcish artillery').
card_uid('orcish artillery'/'CED', 'CED:Orcish Artillery:orcish artillery').
card_rarity('orcish artillery'/'CED', 'Uncommon').
card_artist('orcish artillery'/'CED', 'Anson Maddocks').
card_flavor_text('orcish artillery'/'CED', 'In a rare display of ingenuity, the Orcs invented an incredibly destructive weapon. Most Orcish artillerists are those who dared criticize its effectiveness.').

card_in_set('orcish oriflamme', 'CED').
card_original_type('orcish oriflamme'/'CED', 'Enchantment').
card_original_text('orcish oriflamme'/'CED', '').
card_image_name('orcish oriflamme'/'CED', 'orcish oriflamme').
card_uid('orcish oriflamme'/'CED', 'CED:Orcish Oriflamme:orcish oriflamme').
card_rarity('orcish oriflamme'/'CED', 'Uncommon').
card_artist('orcish oriflamme'/'CED', 'Dan Frazier').

card_in_set('paralyze', 'CED').
card_original_type('paralyze'/'CED', 'Enchantment — Aura').
card_original_text('paralyze'/'CED', '').
card_image_name('paralyze'/'CED', 'paralyze').
card_uid('paralyze'/'CED', 'CED:Paralyze:paralyze').
card_rarity('paralyze'/'CED', 'Common').
card_artist('paralyze'/'CED', 'Anson Maddocks').

card_in_set('pearled unicorn', 'CED').
card_original_type('pearled unicorn'/'CED', 'Creature — Unicorn').
card_original_text('pearled unicorn'/'CED', '').
card_image_name('pearled unicorn'/'CED', 'pearled unicorn').
card_uid('pearled unicorn'/'CED', 'CED:Pearled Unicorn:pearled unicorn').
card_rarity('pearled unicorn'/'CED', 'Common').
card_artist('pearled unicorn'/'CED', 'Cornelius Brudi').
card_flavor_text('pearled unicorn'/'CED', '\"‘Do you know, I always thought Unicorns were fabulous monsters, too? I never saw one alive before!\' ‘Well, now that we have seen each other,\' said the Unicorn, ‘if you\'ll believe in me, I\'ll believe in you.\'\"\n—Lewis Carroll').

card_in_set('personal incarnation', 'CED').
card_original_type('personal incarnation'/'CED', 'Creature — Avatar Incarnation').
card_original_text('personal incarnation'/'CED', '').
card_image_name('personal incarnation'/'CED', 'personal incarnation').
card_uid('personal incarnation'/'CED', 'CED:Personal Incarnation:personal incarnation').
card_rarity('personal incarnation'/'CED', 'Rare').
card_artist('personal incarnation'/'CED', 'Kev Brockschmidt').

card_in_set('pestilence', 'CED').
card_original_type('pestilence'/'CED', 'Enchantment').
card_original_text('pestilence'/'CED', '').
card_image_name('pestilence'/'CED', 'pestilence').
card_uid('pestilence'/'CED', 'CED:Pestilence:pestilence').
card_rarity('pestilence'/'CED', 'Common').
card_artist('pestilence'/'CED', 'Jesper Myrfors').

card_in_set('phantasmal forces', 'CED').
card_original_type('phantasmal forces'/'CED', 'Creature — Illusion').
card_original_text('phantasmal forces'/'CED', '').
card_image_name('phantasmal forces'/'CED', 'phantasmal forces').
card_uid('phantasmal forces'/'CED', 'CED:Phantasmal Forces:phantasmal forces').
card_rarity('phantasmal forces'/'CED', 'Uncommon').
card_artist('phantasmal forces'/'CED', 'Mark Poole').
card_flavor_text('phantasmal forces'/'CED', 'These beings embody the essence of true heroes long dead. Summoned from the dreamrealms, they rise to meet their enemies.').

card_in_set('phantasmal terrain', 'CED').
card_original_type('phantasmal terrain'/'CED', 'Enchantment — Aura').
card_original_text('phantasmal terrain'/'CED', '').
card_image_name('phantasmal terrain'/'CED', 'phantasmal terrain').
card_uid('phantasmal terrain'/'CED', 'CED:Phantasmal Terrain:phantasmal terrain').
card_rarity('phantasmal terrain'/'CED', 'Common').
card_artist('phantasmal terrain'/'CED', 'Dameon Willich').

card_in_set('phantom monster', 'CED').
card_original_type('phantom monster'/'CED', 'Creature — Illusion').
card_original_text('phantom monster'/'CED', '').
card_image_name('phantom monster'/'CED', 'phantom monster').
card_uid('phantom monster'/'CED', 'CED:Phantom Monster:phantom monster').
card_rarity('phantom monster'/'CED', 'Uncommon').
card_artist('phantom monster'/'CED', 'Jesper Myrfors').
card_flavor_text('phantom monster'/'CED', '\"While, like a ghastly rapid river,\nThrough the pale door,\nA hideous throng rush out forever,\nAnd laugh—but smile no more.\"\n—Edgar Allan Poe, \"The Haunted Palace\"').

card_in_set('pirate ship', 'CED').
card_original_type('pirate ship'/'CED', 'Creature — Human Pirate').
card_original_text('pirate ship'/'CED', '').
card_image_name('pirate ship'/'CED', 'pirate ship').
card_uid('pirate ship'/'CED', 'CED:Pirate Ship:pirate ship').
card_rarity('pirate ship'/'CED', 'Rare').
card_artist('pirate ship'/'CED', 'Tom Wänerstrand').

card_in_set('plague rats', 'CED').
card_original_type('plague rats'/'CED', 'Creature — Rat').
card_original_text('plague rats'/'CED', '').
card_image_name('plague rats'/'CED', 'plague rats').
card_uid('plague rats'/'CED', 'CED:Plague Rats:plague rats').
card_rarity('plague rats'/'CED', 'Common').
card_artist('plague rats'/'CED', 'Anson Maddocks').
card_flavor_text('plague rats'/'CED', '\"Should you a Rat to madness tease\nWhy ev\'n a Rat may plague you...\"\n—Samuel Coleridge, \"Recantation\"').

card_in_set('plains', 'CED').
card_original_type('plains'/'CED', 'Basic Land — Plains').
card_original_text('plains'/'CED', '').
card_image_name('plains'/'CED', 'plains1').
card_uid('plains'/'CED', 'CED:Plains:plains1').
card_rarity('plains'/'CED', 'Basic Land').
card_artist('plains'/'CED', 'Jesper Myrfors').

card_in_set('plains', 'CED').
card_original_type('plains'/'CED', 'Basic Land — Plains').
card_original_text('plains'/'CED', '').
card_image_name('plains'/'CED', 'plains2').
card_uid('plains'/'CED', 'CED:Plains:plains2').
card_rarity('plains'/'CED', 'Basic Land').
card_artist('plains'/'CED', 'Jesper Myrfors').

card_in_set('plains', 'CED').
card_original_type('plains'/'CED', 'Basic Land — Plains').
card_original_text('plains'/'CED', '').
card_image_name('plains'/'CED', 'plains3').
card_uid('plains'/'CED', 'CED:Plains:plains3').
card_rarity('plains'/'CED', 'Basic Land').
card_artist('plains'/'CED', 'Jesper Myrfors').

card_in_set('plateau', 'CED').
card_original_type('plateau'/'CED', 'Land — Mountain Plains').
card_original_text('plateau'/'CED', '').
card_image_name('plateau'/'CED', 'plateau').
card_uid('plateau'/'CED', 'CED:Plateau:plateau').
card_rarity('plateau'/'CED', 'Rare').
card_artist('plateau'/'CED', 'Drew Tucker').

card_in_set('power leak', 'CED').
card_original_type('power leak'/'CED', 'Enchantment — Aura').
card_original_text('power leak'/'CED', '').
card_image_name('power leak'/'CED', 'power leak').
card_uid('power leak'/'CED', 'CED:Power Leak:power leak').
card_rarity('power leak'/'CED', 'Common').
card_artist('power leak'/'CED', 'Drew Tucker').

card_in_set('power sink', 'CED').
card_original_type('power sink'/'CED', 'Instant').
card_original_text('power sink'/'CED', '').
card_image_name('power sink'/'CED', 'power sink').
card_uid('power sink'/'CED', 'CED:Power Sink:power sink').
card_rarity('power sink'/'CED', 'Common').
card_artist('power sink'/'CED', 'Richard Thomas').

card_in_set('power surge', 'CED').
card_original_type('power surge'/'CED', 'Enchantment').
card_original_text('power surge'/'CED', '').
card_image_name('power surge'/'CED', 'power surge').
card_uid('power surge'/'CED', 'CED:Power Surge:power surge').
card_rarity('power surge'/'CED', 'Rare').
card_artist('power surge'/'CED', 'Douglas Shuler').

card_in_set('prodigal sorcerer', 'CED').
card_original_type('prodigal sorcerer'/'CED', 'Creature — Human Wizard').
card_original_text('prodigal sorcerer'/'CED', '').
card_image_name('prodigal sorcerer'/'CED', 'prodigal sorcerer').
card_uid('prodigal sorcerer'/'CED', 'CED:Prodigal Sorcerer:prodigal sorcerer').
card_rarity('prodigal sorcerer'/'CED', 'Common').
card_artist('prodigal sorcerer'/'CED', 'Douglas Shuler').
card_flavor_text('prodigal sorcerer'/'CED', 'Occasionally a member of the Institute of Arcane Study acquires a taste for worldly pleasures. Seldom do they have trouble finding employment.').

card_in_set('psionic blast', 'CED').
card_original_type('psionic blast'/'CED', 'Instant').
card_original_text('psionic blast'/'CED', '').
card_image_name('psionic blast'/'CED', 'psionic blast').
card_uid('psionic blast'/'CED', 'CED:Psionic Blast:psionic blast').
card_rarity('psionic blast'/'CED', 'Uncommon').
card_artist('psionic blast'/'CED', 'Douglas Shuler').

card_in_set('psychic venom', 'CED').
card_original_type('psychic venom'/'CED', 'Enchantment — Aura').
card_original_text('psychic venom'/'CED', '').
card_image_name('psychic venom'/'CED', 'psychic venom').
card_uid('psychic venom'/'CED', 'CED:Psychic Venom:psychic venom').
card_rarity('psychic venom'/'CED', 'Common').
card_artist('psychic venom'/'CED', 'Brian Snõddy').

card_in_set('purelace', 'CED').
card_original_type('purelace'/'CED', 'Instant').
card_original_text('purelace'/'CED', '').
card_image_name('purelace'/'CED', 'purelace').
card_uid('purelace'/'CED', 'CED:Purelace:purelace').
card_rarity('purelace'/'CED', 'Rare').
card_artist('purelace'/'CED', 'Sandra Everingham').

card_in_set('raging river', 'CED').
card_original_type('raging river'/'CED', 'Enchantment').
card_original_text('raging river'/'CED', '').
card_image_name('raging river'/'CED', 'raging river').
card_uid('raging river'/'CED', 'CED:Raging River:raging river').
card_rarity('raging river'/'CED', 'Rare').
card_artist('raging river'/'CED', 'Sandra Everingham').

card_in_set('raise dead', 'CED').
card_original_type('raise dead'/'CED', 'Sorcery').
card_original_text('raise dead'/'CED', '').
card_image_name('raise dead'/'CED', 'raise dead').
card_uid('raise dead'/'CED', 'CED:Raise Dead:raise dead').
card_rarity('raise dead'/'CED', 'Common').
card_artist('raise dead'/'CED', 'Jeff A. Menges').

card_in_set('red elemental blast', 'CED').
card_original_type('red elemental blast'/'CED', 'Instant').
card_original_text('red elemental blast'/'CED', '').
card_image_name('red elemental blast'/'CED', 'red elemental blast').
card_uid('red elemental blast'/'CED', 'CED:Red Elemental Blast:red elemental blast').
card_rarity('red elemental blast'/'CED', 'Common').
card_artist('red elemental blast'/'CED', 'Richard Thomas').

card_in_set('red ward', 'CED').
card_original_type('red ward'/'CED', 'Enchantment — Aura').
card_original_text('red ward'/'CED', '').
card_image_name('red ward'/'CED', 'red ward').
card_uid('red ward'/'CED', 'CED:Red Ward:red ward').
card_rarity('red ward'/'CED', 'Uncommon').
card_artist('red ward'/'CED', 'Dan Frazier').

card_in_set('regeneration', 'CED').
card_original_type('regeneration'/'CED', 'Enchantment — Aura').
card_original_text('regeneration'/'CED', '').
card_image_name('regeneration'/'CED', 'regeneration').
card_uid('regeneration'/'CED', 'CED:Regeneration:regeneration').
card_rarity('regeneration'/'CED', 'Common').
card_artist('regeneration'/'CED', 'Quinton Hoover').

card_in_set('regrowth', 'CED').
card_original_type('regrowth'/'CED', 'Sorcery').
card_original_text('regrowth'/'CED', '').
card_image_name('regrowth'/'CED', 'regrowth').
card_uid('regrowth'/'CED', 'CED:Regrowth:regrowth').
card_rarity('regrowth'/'CED', 'Uncommon').
card_artist('regrowth'/'CED', 'Dameon Willich').

card_in_set('resurrection', 'CED').
card_original_type('resurrection'/'CED', 'Sorcery').
card_original_text('resurrection'/'CED', '').
card_image_name('resurrection'/'CED', 'resurrection').
card_uid('resurrection'/'CED', 'CED:Resurrection:resurrection').
card_rarity('resurrection'/'CED', 'Uncommon').
card_artist('resurrection'/'CED', 'Dan Frazier').

card_in_set('reverse damage', 'CED').
card_original_type('reverse damage'/'CED', 'Instant').
card_original_text('reverse damage'/'CED', '').
card_image_name('reverse damage'/'CED', 'reverse damage').
card_uid('reverse damage'/'CED', 'CED:Reverse Damage:reverse damage').
card_rarity('reverse damage'/'CED', 'Rare').
card_artist('reverse damage'/'CED', 'Dameon Willich').

card_in_set('righteousness', 'CED').
card_original_type('righteousness'/'CED', 'Instant').
card_original_text('righteousness'/'CED', '').
card_image_name('righteousness'/'CED', 'righteousness').
card_uid('righteousness'/'CED', 'CED:Righteousness:righteousness').
card_rarity('righteousness'/'CED', 'Rare').
card_artist('righteousness'/'CED', 'Douglas Shuler').

card_in_set('roc of kher ridges', 'CED').
card_original_type('roc of kher ridges'/'CED', 'Creature — Bird').
card_original_text('roc of kher ridges'/'CED', '').
card_image_name('roc of kher ridges'/'CED', 'roc of kher ridges').
card_uid('roc of kher ridges'/'CED', 'CED:Roc of Kher Ridges:roc of kher ridges').
card_rarity('roc of kher ridges'/'CED', 'Rare').
card_artist('roc of kher ridges'/'CED', 'Andi Rusu').
card_flavor_text('roc of kher ridges'/'CED', 'We encountered a valley topped with immense boulders and eerie rock formations. Suddenly one of these boulders toppled from its perch and sprouted gargantuan wings, casting a shadow of darkness and sending us fleeing in terror.').

card_in_set('rock hydra', 'CED').
card_original_type('rock hydra'/'CED', 'Creature — Hydra').
card_original_text('rock hydra'/'CED', '').
card_image_name('rock hydra'/'CED', 'rock hydra').
card_uid('rock hydra'/'CED', 'CED:Rock Hydra:rock hydra').
card_rarity('rock hydra'/'CED', 'Rare').
card_artist('rock hydra'/'CED', 'Jeff A. Menges').

card_in_set('rod of ruin', 'CED').
card_original_type('rod of ruin'/'CED', 'Artifact').
card_original_text('rod of ruin'/'CED', '').
card_image_name('rod of ruin'/'CED', 'rod of ruin').
card_uid('rod of ruin'/'CED', 'CED:Rod of Ruin:rod of ruin').
card_rarity('rod of ruin'/'CED', 'Uncommon').
card_artist('rod of ruin'/'CED', 'Christopher Rush').

card_in_set('royal assassin', 'CED').
card_original_type('royal assassin'/'CED', 'Creature — Human Assassin').
card_original_text('royal assassin'/'CED', '').
card_image_name('royal assassin'/'CED', 'royal assassin').
card_uid('royal assassin'/'CED', 'CED:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'CED', 'Rare').
card_artist('royal assassin'/'CED', 'Tom Wänerstrand').
card_flavor_text('royal assassin'/'CED', 'Trained in the arts of stealth, the royal assassins choose their victims carefully, relying on timing and precision rather than brute force.').

card_in_set('sacrifice', 'CED').
card_original_type('sacrifice'/'CED', 'Instant').
card_original_text('sacrifice'/'CED', '').
card_image_name('sacrifice'/'CED', 'sacrifice').
card_uid('sacrifice'/'CED', 'CED:Sacrifice:sacrifice').
card_rarity('sacrifice'/'CED', 'Uncommon').
card_artist('sacrifice'/'CED', 'Dan Frazier').

card_in_set('samite healer', 'CED').
card_original_type('samite healer'/'CED', 'Creature — Human Cleric').
card_original_text('samite healer'/'CED', '').
card_image_name('samite healer'/'CED', 'samite healer').
card_uid('samite healer'/'CED', 'CED:Samite Healer:samite healer').
card_rarity('samite healer'/'CED', 'Common').
card_artist('samite healer'/'CED', 'Tom Wänerstrand').
card_flavor_text('samite healer'/'CED', 'Healers ultimately acquire the divine gifts of spiritual and physical wholeness. The most devout are also granted the ability to pass physical wholeness on to others.').

card_in_set('savannah', 'CED').
card_original_type('savannah'/'CED', 'Land — Forest Plains').
card_original_text('savannah'/'CED', '').
card_image_name('savannah'/'CED', 'savannah').
card_uid('savannah'/'CED', 'CED:Savannah:savannah').
card_rarity('savannah'/'CED', 'Rare').
card_artist('savannah'/'CED', 'Rob Alexander').

card_in_set('savannah lions', 'CED').
card_original_type('savannah lions'/'CED', 'Creature — Cat').
card_original_text('savannah lions'/'CED', '').
card_image_name('savannah lions'/'CED', 'savannah lions').
card_uid('savannah lions'/'CED', 'CED:Savannah Lions:savannah lions').
card_rarity('savannah lions'/'CED', 'Rare').
card_artist('savannah lions'/'CED', 'Daniel Gelon').
card_flavor_text('savannah lions'/'CED', 'The traditional kings of the jungle command a healthy respect in other climates as well. Relying mainly on their stealth and speed, Savannah Lions can take a victim by surprise, even in the endless flat plains of their homeland.').

card_in_set('scathe zombies', 'CED').
card_original_type('scathe zombies'/'CED', 'Creature — Zombie').
card_original_text('scathe zombies'/'CED', '').
card_image_name('scathe zombies'/'CED', 'scathe zombies').
card_uid('scathe zombies'/'CED', 'CED:Scathe Zombies:scathe zombies').
card_rarity('scathe zombies'/'CED', 'Common').
card_artist('scathe zombies'/'CED', 'Jesper Myrfors').
card_flavor_text('scathe zombies'/'CED', '\"They groaned, they stirred, they all uprose,/ Nor spake, nor moved their eyes;/ It had been strange, even in a dream,/\nTo have seen those dead men rise.\"/\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').

card_in_set('scavenging ghoul', 'CED').
card_original_type('scavenging ghoul'/'CED', 'Creature — Zombie').
card_original_text('scavenging ghoul'/'CED', '').
card_image_name('scavenging ghoul'/'CED', 'scavenging ghoul').
card_uid('scavenging ghoul'/'CED', 'CED:Scavenging Ghoul:scavenging ghoul').
card_rarity('scavenging ghoul'/'CED', 'Uncommon').
card_artist('scavenging ghoul'/'CED', 'Jeff A. Menges').

card_in_set('scrubland', 'CED').
card_original_type('scrubland'/'CED', 'Land — Plains Swamp').
card_original_text('scrubland'/'CED', '').
card_image_name('scrubland'/'CED', 'scrubland').
card_uid('scrubland'/'CED', 'CED:Scrubland:scrubland').
card_rarity('scrubland'/'CED', 'Rare').
card_artist('scrubland'/'CED', 'Jesper Myrfors').

card_in_set('scryb sprites', 'CED').
card_original_type('scryb sprites'/'CED', 'Creature — Faerie').
card_original_text('scryb sprites'/'CED', '').
card_image_name('scryb sprites'/'CED', 'scryb sprites').
card_uid('scryb sprites'/'CED', 'CED:Scryb Sprites:scryb sprites').
card_rarity('scryb sprites'/'CED', 'Common').
card_artist('scryb sprites'/'CED', 'Amy Weber').
card_flavor_text('scryb sprites'/'CED', 'The only sound was the gentle clicking of the Faeries\' wings. Then those intruders who were still standing turned and fled. One thing was certain: they didn\'t think the Scryb were very funny anymore.').

card_in_set('sea serpent', 'CED').
card_original_type('sea serpent'/'CED', 'Creature — Serpent').
card_original_text('sea serpent'/'CED', '').
card_image_name('sea serpent'/'CED', 'sea serpent').
card_uid('sea serpent'/'CED', 'CED:Sea Serpent:sea serpent').
card_rarity('sea serpent'/'CED', 'Common').
card_artist('sea serpent'/'CED', 'Jeff A. Menges').
card_flavor_text('sea serpent'/'CED', 'Legend has it that Serpents used to be bigger, but how could that be?').

card_in_set('sedge troll', 'CED').
card_original_type('sedge troll'/'CED', 'Creature — Troll').
card_original_text('sedge troll'/'CED', '').
card_image_name('sedge troll'/'CED', 'sedge troll').
card_uid('sedge troll'/'CED', 'CED:Sedge Troll:sedge troll').
card_rarity('sedge troll'/'CED', 'Rare').
card_artist('sedge troll'/'CED', 'Dan Frazier').
card_flavor_text('sedge troll'/'CED', 'The stench in the hovel was overpowering; something loathsome was cooking. Occasionally something surfaced in the thick paste, but my host would casually push it down before I could make out what it was.').

card_in_set('sengir vampire', 'CED').
card_original_type('sengir vampire'/'CED', 'Creature — Vampire').
card_original_text('sengir vampire'/'CED', '').
card_image_name('sengir vampire'/'CED', 'sengir vampire').
card_uid('sengir vampire'/'CED', 'CED:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'CED', 'Uncommon').
card_artist('sengir vampire'/'CED', 'Anson Maddocks').

card_in_set('serra angel', 'CED').
card_original_type('serra angel'/'CED', 'Creature — Angel').
card_original_text('serra angel'/'CED', '').
card_image_name('serra angel'/'CED', 'serra angel').
card_uid('serra angel'/'CED', 'CED:Serra Angel:serra angel').
card_rarity('serra angel'/'CED', 'Uncommon').
card_artist('serra angel'/'CED', 'Douglas Shuler').
card_flavor_text('serra angel'/'CED', 'Born with wings of light and a sword of faith, this heavenly incarnation embodies both fury and purity.').

card_in_set('shanodin dryads', 'CED').
card_original_type('shanodin dryads'/'CED', 'Creature — Nymph Dryad').
card_original_text('shanodin dryads'/'CED', '').
card_image_name('shanodin dryads'/'CED', 'shanodin dryads').
card_uid('shanodin dryads'/'CED', 'CED:Shanodin Dryads:shanodin dryads').
card_rarity('shanodin dryads'/'CED', 'Common').
card_artist('shanodin dryads'/'CED', 'Anson Maddocks').
card_flavor_text('shanodin dryads'/'CED', 'Moving without sound, swift figures pass through branches and undergrowth completely unhindered. One with the trees around them, the Dryads of Shanodin Forest are seen only when they wish to be.').

card_in_set('shatter', 'CED').
card_original_type('shatter'/'CED', 'Instant').
card_original_text('shatter'/'CED', '').
card_image_name('shatter'/'CED', 'shatter').
card_uid('shatter'/'CED', 'CED:Shatter:shatter').
card_rarity('shatter'/'CED', 'Common').
card_artist('shatter'/'CED', 'Amy Weber').

card_in_set('shivan dragon', 'CED').
card_original_type('shivan dragon'/'CED', 'Creature — Dragon').
card_original_text('shivan dragon'/'CED', '').
card_image_name('shivan dragon'/'CED', 'shivan dragon').
card_uid('shivan dragon'/'CED', 'CED:Shivan Dragon:shivan dragon').
card_rarity('shivan dragon'/'CED', 'Rare').
card_artist('shivan dragon'/'CED', 'Melissa A. Benson').
card_flavor_text('shivan dragon'/'CED', 'While it\'s true most Dragons are cruel, the Shivan Dragon seems to take particular glee in the misery of others, often tormenting its victims much like a cat plays with a mouse before delivering the final blow.').

card_in_set('simulacrum', 'CED').
card_original_type('simulacrum'/'CED', 'Instant').
card_original_text('simulacrum'/'CED', '').
card_image_name('simulacrum'/'CED', 'simulacrum').
card_uid('simulacrum'/'CED', 'CED:Simulacrum:simulacrum').
card_rarity('simulacrum'/'CED', 'Uncommon').
card_artist('simulacrum'/'CED', 'Mark Poole').

card_in_set('sinkhole', 'CED').
card_original_type('sinkhole'/'CED', 'Sorcery').
card_original_text('sinkhole'/'CED', '').
card_image_name('sinkhole'/'CED', 'sinkhole').
card_uid('sinkhole'/'CED', 'CED:Sinkhole:sinkhole').
card_rarity('sinkhole'/'CED', 'Common').
card_artist('sinkhole'/'CED', 'Sandra Everingham').

card_in_set('siren\'s call', 'CED').
card_original_type('siren\'s call'/'CED', 'Instant').
card_original_text('siren\'s call'/'CED', '').
card_image_name('siren\'s call'/'CED', 'siren\'s call').
card_uid('siren\'s call'/'CED', 'CED:Siren\'s Call:siren\'s call').
card_rarity('siren\'s call'/'CED', 'Uncommon').
card_artist('siren\'s call'/'CED', 'Anson Maddocks').

card_in_set('sleight of mind', 'CED').
card_original_type('sleight of mind'/'CED', 'Instant').
card_original_text('sleight of mind'/'CED', '').
card_image_name('sleight of mind'/'CED', 'sleight of mind').
card_uid('sleight of mind'/'CED', 'CED:Sleight of Mind:sleight of mind').
card_rarity('sleight of mind'/'CED', 'Rare').
card_artist('sleight of mind'/'CED', 'Mark Poole').

card_in_set('smoke', 'CED').
card_original_type('smoke'/'CED', 'Enchantment').
card_original_text('smoke'/'CED', '').
card_image_name('smoke'/'CED', 'smoke').
card_uid('smoke'/'CED', 'CED:Smoke:smoke').
card_rarity('smoke'/'CED', 'Rare').
card_artist('smoke'/'CED', 'Jesper Myrfors').

card_in_set('sol ring', 'CED').
card_original_type('sol ring'/'CED', 'Artifact').
card_original_text('sol ring'/'CED', '').
card_image_name('sol ring'/'CED', 'sol ring').
card_uid('sol ring'/'CED', 'CED:Sol Ring:sol ring').
card_rarity('sol ring'/'CED', 'Uncommon').
card_artist('sol ring'/'CED', 'Mark Tedin').

card_in_set('soul net', 'CED').
card_original_type('soul net'/'CED', 'Artifact').
card_original_text('soul net'/'CED', '').
card_image_name('soul net'/'CED', 'soul net').
card_uid('soul net'/'CED', 'CED:Soul Net:soul net').
card_rarity('soul net'/'CED', 'Uncommon').
card_artist('soul net'/'CED', 'Dameon Willich').

card_in_set('spell blast', 'CED').
card_original_type('spell blast'/'CED', 'Instant').
card_original_text('spell blast'/'CED', '').
card_image_name('spell blast'/'CED', 'spell blast').
card_uid('spell blast'/'CED', 'CED:Spell Blast:spell blast').
card_rarity('spell blast'/'CED', 'Common').
card_artist('spell blast'/'CED', 'Brian Snõddy').

card_in_set('stasis', 'CED').
card_original_type('stasis'/'CED', 'Enchantment').
card_original_text('stasis'/'CED', '').
card_image_name('stasis'/'CED', 'stasis').
card_uid('stasis'/'CED', 'CED:Stasis:stasis').
card_rarity('stasis'/'CED', 'Rare').
card_artist('stasis'/'CED', 'Fay Jones').

card_in_set('steal artifact', 'CED').
card_original_type('steal artifact'/'CED', 'Enchantment — Aura').
card_original_text('steal artifact'/'CED', '').
card_image_name('steal artifact'/'CED', 'steal artifact').
card_uid('steal artifact'/'CED', 'CED:Steal Artifact:steal artifact').
card_rarity('steal artifact'/'CED', 'Uncommon').
card_artist('steal artifact'/'CED', 'Amy Weber').

card_in_set('stone giant', 'CED').
card_original_type('stone giant'/'CED', 'Creature — Giant').
card_original_text('stone giant'/'CED', '').
card_image_name('stone giant'/'CED', 'stone giant').
card_uid('stone giant'/'CED', 'CED:Stone Giant:stone giant').
card_rarity('stone giant'/'CED', 'Uncommon').
card_artist('stone giant'/'CED', 'Dameon Willich').
card_flavor_text('stone giant'/'CED', 'What goes up, must come down.').

card_in_set('stone rain', 'CED').
card_original_type('stone rain'/'CED', 'Sorcery').
card_original_text('stone rain'/'CED', '').
card_image_name('stone rain'/'CED', 'stone rain').
card_uid('stone rain'/'CED', 'CED:Stone Rain:stone rain').
card_rarity('stone rain'/'CED', 'Common').
card_artist('stone rain'/'CED', 'Daniel Gelon').

card_in_set('stream of life', 'CED').
card_original_type('stream of life'/'CED', 'Sorcery').
card_original_text('stream of life'/'CED', '').
card_image_name('stream of life'/'CED', 'stream of life').
card_uid('stream of life'/'CED', 'CED:Stream of Life:stream of life').
card_rarity('stream of life'/'CED', 'Common').
card_artist('stream of life'/'CED', 'Mark Poole').

card_in_set('sunglasses of urza', 'CED').
card_original_type('sunglasses of urza'/'CED', 'Artifact').
card_original_text('sunglasses of urza'/'CED', '').
card_image_name('sunglasses of urza'/'CED', 'sunglasses of urza').
card_uid('sunglasses of urza'/'CED', 'CED:Sunglasses of Urza:sunglasses of urza').
card_rarity('sunglasses of urza'/'CED', 'Rare').
card_artist('sunglasses of urza'/'CED', 'Dan Frazier').

card_in_set('swamp', 'CED').
card_original_type('swamp'/'CED', 'Basic Land — Swamp').
card_original_text('swamp'/'CED', '').
card_image_name('swamp'/'CED', 'swamp1').
card_uid('swamp'/'CED', 'CED:Swamp:swamp1').
card_rarity('swamp'/'CED', 'Basic Land').
card_artist('swamp'/'CED', 'Dan Frazier').

card_in_set('swamp', 'CED').
card_original_type('swamp'/'CED', 'Basic Land — Swamp').
card_original_text('swamp'/'CED', '').
card_image_name('swamp'/'CED', 'swamp2').
card_uid('swamp'/'CED', 'CED:Swamp:swamp2').
card_rarity('swamp'/'CED', 'Basic Land').
card_artist('swamp'/'CED', 'Dan Frazier').

card_in_set('swamp', 'CED').
card_original_type('swamp'/'CED', 'Basic Land — Swamp').
card_original_text('swamp'/'CED', '').
card_image_name('swamp'/'CED', 'swamp3').
card_uid('swamp'/'CED', 'CED:Swamp:swamp3').
card_rarity('swamp'/'CED', 'Basic Land').
card_artist('swamp'/'CED', 'Dan Frazier').

card_in_set('swords to plowshares', 'CED').
card_original_type('swords to plowshares'/'CED', 'Instant').
card_original_text('swords to plowshares'/'CED', '').
card_image_name('swords to plowshares'/'CED', 'swords to plowshares').
card_uid('swords to plowshares'/'CED', 'CED:Swords to Plowshares:swords to plowshares').
card_rarity('swords to plowshares'/'CED', 'Uncommon').
card_artist('swords to plowshares'/'CED', 'Jeff A. Menges').

card_in_set('taiga', 'CED').
card_original_type('taiga'/'CED', 'Land — Mountain Forest').
card_original_text('taiga'/'CED', '').
card_image_name('taiga'/'CED', 'taiga').
card_uid('taiga'/'CED', 'CED:Taiga:taiga').
card_rarity('taiga'/'CED', 'Rare').
card_artist('taiga'/'CED', 'Rob Alexander').

card_in_set('terror', 'CED').
card_original_type('terror'/'CED', 'Instant').
card_original_text('terror'/'CED', '').
card_image_name('terror'/'CED', 'terror').
card_uid('terror'/'CED', 'CED:Terror:terror').
card_rarity('terror'/'CED', 'Common').
card_artist('terror'/'CED', 'Ron Spencer').

card_in_set('the hive', 'CED').
card_original_type('the hive'/'CED', 'Artifact').
card_original_text('the hive'/'CED', '').
card_image_name('the hive'/'CED', 'the hive').
card_uid('the hive'/'CED', 'CED:The Hive:the hive').
card_rarity('the hive'/'CED', 'Rare').
card_artist('the hive'/'CED', 'Sandra Everingham').

card_in_set('thicket basilisk', 'CED').
card_original_type('thicket basilisk'/'CED', 'Creature — Basilisk').
card_original_text('thicket basilisk'/'CED', '').
card_image_name('thicket basilisk'/'CED', 'thicket basilisk').
card_uid('thicket basilisk'/'CED', 'CED:Thicket Basilisk:thicket basilisk').
card_rarity('thicket basilisk'/'CED', 'Uncommon').
card_artist('thicket basilisk'/'CED', 'Dan Frazier').
card_flavor_text('thicket basilisk'/'CED', 'Moss-covered statues littered the area, a macabre monument to the Basilisk\'s power.').

card_in_set('thoughtlace', 'CED').
card_original_type('thoughtlace'/'CED', 'Instant').
card_original_text('thoughtlace'/'CED', '').
card_image_name('thoughtlace'/'CED', 'thoughtlace').
card_uid('thoughtlace'/'CED', 'CED:Thoughtlace:thoughtlace').
card_rarity('thoughtlace'/'CED', 'Rare').
card_artist('thoughtlace'/'CED', 'Mark Poole').

card_in_set('throne of bone', 'CED').
card_original_type('throne of bone'/'CED', 'Artifact').
card_original_text('throne of bone'/'CED', '').
card_image_name('throne of bone'/'CED', 'throne of bone').
card_uid('throne of bone'/'CED', 'CED:Throne of Bone:throne of bone').
card_rarity('throne of bone'/'CED', 'Uncommon').
card_artist('throne of bone'/'CED', 'Anson Maddocks').

card_in_set('timber wolves', 'CED').
card_original_type('timber wolves'/'CED', 'Creature — Wolf').
card_original_text('timber wolves'/'CED', '').
card_image_name('timber wolves'/'CED', 'timber wolves').
card_uid('timber wolves'/'CED', 'CED:Timber Wolves:timber wolves').
card_rarity('timber wolves'/'CED', 'Rare').
card_artist('timber wolves'/'CED', 'Melissa A. Benson').
card_flavor_text('timber wolves'/'CED', 'Though many think of Wolves as solitary predators, they are actually extremely social animals. During a hunt they often call to each other, which can be quite unsettling for their prey.').

card_in_set('time vault', 'CED').
card_original_type('time vault'/'CED', 'Artifact').
card_original_text('time vault'/'CED', '').
card_image_name('time vault'/'CED', 'time vault').
card_uid('time vault'/'CED', 'CED:Time Vault:time vault').
card_rarity('time vault'/'CED', 'Rare').
card_artist('time vault'/'CED', 'Mark Tedin').

card_in_set('time walk', 'CED').
card_original_type('time walk'/'CED', 'Sorcery').
card_original_text('time walk'/'CED', '').
card_image_name('time walk'/'CED', 'time walk').
card_uid('time walk'/'CED', 'CED:Time Walk:time walk').
card_rarity('time walk'/'CED', 'Rare').
card_artist('time walk'/'CED', 'Amy Weber').

card_in_set('timetwister', 'CED').
card_original_type('timetwister'/'CED', 'Sorcery').
card_original_text('timetwister'/'CED', '').
card_image_name('timetwister'/'CED', 'timetwister').
card_uid('timetwister'/'CED', 'CED:Timetwister:timetwister').
card_rarity('timetwister'/'CED', 'Rare').
card_artist('timetwister'/'CED', 'Mark Tedin').

card_in_set('tranquility', 'CED').
card_original_type('tranquility'/'CED', 'Sorcery').
card_original_text('tranquility'/'CED', '').
card_image_name('tranquility'/'CED', 'tranquility').
card_uid('tranquility'/'CED', 'CED:Tranquility:tranquility').
card_rarity('tranquility'/'CED', 'Common').
card_artist('tranquility'/'CED', 'Douglas Shuler').

card_in_set('tropical island', 'CED').
card_original_type('tropical island'/'CED', 'Land — Forest Island').
card_original_text('tropical island'/'CED', '').
card_image_name('tropical island'/'CED', 'tropical island').
card_uid('tropical island'/'CED', 'CED:Tropical Island:tropical island').
card_rarity('tropical island'/'CED', 'Rare').
card_artist('tropical island'/'CED', 'Jesper Myrfors').

card_in_set('tsunami', 'CED').
card_original_type('tsunami'/'CED', 'Sorcery').
card_original_text('tsunami'/'CED', '').
card_image_name('tsunami'/'CED', 'tsunami').
card_uid('tsunami'/'CED', 'CED:Tsunami:tsunami').
card_rarity('tsunami'/'CED', 'Uncommon').
card_artist('tsunami'/'CED', 'Richard Thomas').

card_in_set('tundra', 'CED').
card_original_type('tundra'/'CED', 'Land — Plains Island').
card_original_text('tundra'/'CED', '').
card_image_name('tundra'/'CED', 'tundra').
card_uid('tundra'/'CED', 'CED:Tundra:tundra').
card_rarity('tundra'/'CED', 'Rare').
card_artist('tundra'/'CED', 'Jesper Myrfors').

card_in_set('tunnel', 'CED').
card_original_type('tunnel'/'CED', 'Instant').
card_original_text('tunnel'/'CED', '').
card_image_name('tunnel'/'CED', 'tunnel').
card_uid('tunnel'/'CED', 'CED:Tunnel:tunnel').
card_rarity('tunnel'/'CED', 'Uncommon').
card_artist('tunnel'/'CED', 'Dan Frazier').

card_in_set('twiddle', 'CED').
card_original_type('twiddle'/'CED', 'Instant').
card_original_text('twiddle'/'CED', '').
card_image_name('twiddle'/'CED', 'twiddle').
card_uid('twiddle'/'CED', 'CED:Twiddle:twiddle').
card_rarity('twiddle'/'CED', 'Common').
card_artist('twiddle'/'CED', 'Rob Alexander').

card_in_set('two-headed giant of foriys', 'CED').
card_original_type('two-headed giant of foriys'/'CED', 'Creature — Giant').
card_original_text('two-headed giant of foriys'/'CED', '').
card_image_name('two-headed giant of foriys'/'CED', 'two-headed giant of foriys').
card_uid('two-headed giant of foriys'/'CED', 'CED:Two-Headed Giant of Foriys:two-headed giant of foriys').
card_rarity('two-headed giant of foriys'/'CED', 'Rare').
card_artist('two-headed giant of foriys'/'CED', 'Anson Maddocks').
card_flavor_text('two-headed giant of foriys'/'CED', 'None know if this Giant is the result of aberrant magics, Siamese twins, or a mentalist\'s schizophrenia.').

card_in_set('underground sea', 'CED').
card_original_type('underground sea'/'CED', 'Land — Island Swamp').
card_original_text('underground sea'/'CED', '').
card_image_name('underground sea'/'CED', 'underground sea').
card_uid('underground sea'/'CED', 'CED:Underground Sea:underground sea').
card_rarity('underground sea'/'CED', 'Rare').
card_artist('underground sea'/'CED', 'Rob Alexander').

card_in_set('unholy strength', 'CED').
card_original_type('unholy strength'/'CED', 'Enchantment — Aura').
card_original_text('unholy strength'/'CED', '').
card_image_name('unholy strength'/'CED', 'unholy strength').
card_uid('unholy strength'/'CED', 'CED:Unholy Strength:unholy strength').
card_rarity('unholy strength'/'CED', 'Common').
card_artist('unholy strength'/'CED', 'Douglas Shuler').

card_in_set('unsummon', 'CED').
card_original_type('unsummon'/'CED', 'Instant').
card_original_text('unsummon'/'CED', '').
card_image_name('unsummon'/'CED', 'unsummon').
card_uid('unsummon'/'CED', 'CED:Unsummon:unsummon').
card_rarity('unsummon'/'CED', 'Common').
card_artist('unsummon'/'CED', 'Douglas Shuler').

card_in_set('uthden troll', 'CED').
card_original_type('uthden troll'/'CED', 'Creature — Troll').
card_original_text('uthden troll'/'CED', '').
card_image_name('uthden troll'/'CED', 'uthden troll').
card_uid('uthden troll'/'CED', 'CED:Uthden Troll:uthden troll').
card_rarity('uthden troll'/'CED', 'Uncommon').
card_artist('uthden troll'/'CED', 'Douglas Shuler').
card_flavor_text('uthden troll'/'CED', '\"Oi oi oi, me gotta hurt in \'ere\nOi oi oi, me smell a ting is near,\nGonna bosh \'n gonna nosh \'n da\nhurt\'ll disappear.\"\n—Traditional').

card_in_set('verduran enchantress', 'CED').
card_original_type('verduran enchantress'/'CED', 'Creature — Human Druid').
card_original_text('verduran enchantress'/'CED', '').
card_image_name('verduran enchantress'/'CED', 'verduran enchantress').
card_uid('verduran enchantress'/'CED', 'CED:Verduran Enchantress:verduran enchantress').
card_rarity('verduran enchantress'/'CED', 'Rare').
card_artist('verduran enchantress'/'CED', 'Kev Brockschmidt').
card_flavor_text('verduran enchantress'/'CED', 'Some say magic was first practiced by women, who have always felt strong ties to the land.').

card_in_set('vesuvan doppelganger', 'CED').
card_original_type('vesuvan doppelganger'/'CED', 'Creature — Shapeshifter').
card_original_text('vesuvan doppelganger'/'CED', '').
card_image_name('vesuvan doppelganger'/'CED', 'vesuvan doppelganger').
card_uid('vesuvan doppelganger'/'CED', 'CED:Vesuvan Doppelganger:vesuvan doppelganger').
card_rarity('vesuvan doppelganger'/'CED', 'Rare').
card_artist('vesuvan doppelganger'/'CED', 'Quinton Hoover').

card_in_set('veteran bodyguard', 'CED').
card_original_type('veteran bodyguard'/'CED', 'Creature — Human').
card_original_text('veteran bodyguard'/'CED', '').
card_image_name('veteran bodyguard'/'CED', 'veteran bodyguard').
card_uid('veteran bodyguard'/'CED', 'CED:Veteran Bodyguard:veteran bodyguard').
card_rarity('veteran bodyguard'/'CED', 'Rare').
card_artist('veteran bodyguard'/'CED', 'Douglas Shuler').
card_flavor_text('veteran bodyguard'/'CED', 'Good bodyguards are hard to find, mainly because they don\'t live long.').

card_in_set('volcanic eruption', 'CED').
card_original_type('volcanic eruption'/'CED', 'Sorcery').
card_original_text('volcanic eruption'/'CED', '').
card_image_name('volcanic eruption'/'CED', 'volcanic eruption').
card_uid('volcanic eruption'/'CED', 'CED:Volcanic Eruption:volcanic eruption').
card_rarity('volcanic eruption'/'CED', 'Rare').
card_artist('volcanic eruption'/'CED', 'Douglas Shuler').

card_in_set('volcanic island', 'CED').
card_original_type('volcanic island'/'CED', 'Land — Island Mountain').
card_original_text('volcanic island'/'CED', '').
card_image_name('volcanic island'/'CED', 'volcanic island').
card_uid('volcanic island'/'CED', 'CED:Volcanic Island:volcanic island').
card_rarity('volcanic island'/'CED', 'Rare').
card_artist('volcanic island'/'CED', 'Brian Snõddy').

card_in_set('wall of air', 'CED').
card_original_type('wall of air'/'CED', 'Creature — Wall').
card_original_text('wall of air'/'CED', '').
card_image_name('wall of air'/'CED', 'wall of air').
card_uid('wall of air'/'CED', 'CED:Wall of Air:wall of air').
card_rarity('wall of air'/'CED', 'Uncommon').
card_artist('wall of air'/'CED', 'Richard Thomas').
card_flavor_text('wall of air'/'CED', '\"This \'standing windstorm\' can hold us off indefinitely? Ridiculous!\" Saying nothing, she put a pinch of salt on the table. With a bang she clapped her hands, and the salt disappeared, blown away.').

card_in_set('wall of bone', 'CED').
card_original_type('wall of bone'/'CED', 'Creature — Skeleton Wall').
card_original_text('wall of bone'/'CED', '').
card_image_name('wall of bone'/'CED', 'wall of bone').
card_uid('wall of bone'/'CED', 'CED:Wall of Bone:wall of bone').
card_rarity('wall of bone'/'CED', 'Uncommon').
card_artist('wall of bone'/'CED', 'Anson Maddocks').
card_flavor_text('wall of bone'/'CED', 'The Wall of Bone is said to be an aspect of the Great Wall in Hel, where the bones of all sinners wait for Ragnarok, when Hela will call them forth for the final battle.').

card_in_set('wall of brambles', 'CED').
card_original_type('wall of brambles'/'CED', 'Creature — Plant Wall').
card_original_text('wall of brambles'/'CED', '').
card_image_name('wall of brambles'/'CED', 'wall of brambles').
card_uid('wall of brambles'/'CED', 'CED:Wall of Brambles:wall of brambles').
card_rarity('wall of brambles'/'CED', 'Uncommon').
card_artist('wall of brambles'/'CED', 'Anson Maddocks').
card_flavor_text('wall of brambles'/'CED', '\"What else, when chaos draws all forces inward to shape a single leaf.\"\n—Conrad Aiken').

card_in_set('wall of fire', 'CED').
card_original_type('wall of fire'/'CED', 'Creature — Wall').
card_original_text('wall of fire'/'CED', '').
card_image_name('wall of fire'/'CED', 'wall of fire').
card_uid('wall of fire'/'CED', 'CED:Wall of Fire:wall of fire').
card_rarity('wall of fire'/'CED', 'Uncommon').
card_artist('wall of fire'/'CED', 'Richard Thomas').
card_flavor_text('wall of fire'/'CED', 'Conjured from the bowels of hell, the fiery wall forms an impassable barrier, searing the soul of any creature attempting to pass through its terrible bursts of flame.').

card_in_set('wall of ice', 'CED').
card_original_type('wall of ice'/'CED', 'Creature — Wall').
card_original_text('wall of ice'/'CED', '').
card_image_name('wall of ice'/'CED', 'wall of ice').
card_uid('wall of ice'/'CED', 'CED:Wall of Ice:wall of ice').
card_rarity('wall of ice'/'CED', 'Uncommon').
card_artist('wall of ice'/'CED', 'Richard Thomas').
card_flavor_text('wall of ice'/'CED', '\"And through the drifts the snowy cliffs/ Did send a dismal sheen:/ Nor shapes of men nor beasts we ken—/ The ice was all between.\"/\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').

card_in_set('wall of stone', 'CED').
card_original_type('wall of stone'/'CED', 'Creature — Wall').
card_original_text('wall of stone'/'CED', '').
card_image_name('wall of stone'/'CED', 'wall of stone').
card_uid('wall of stone'/'CED', 'CED:Wall of Stone:wall of stone').
card_rarity('wall of stone'/'CED', 'Uncommon').
card_artist('wall of stone'/'CED', 'Dan Frazier').
card_flavor_text('wall of stone'/'CED', 'The Earth herself lends her strength to these walls of living stone, which possess the stability of ancient mountains. These mighty bulwarks thwart ground-based troops, providing welcome relief for weary warriors who defend the land.').

card_in_set('wall of swords', 'CED').
card_original_type('wall of swords'/'CED', 'Creature — Wall').
card_original_text('wall of swords'/'CED', '').
card_image_name('wall of swords'/'CED', 'wall of swords').
card_uid('wall of swords'/'CED', 'CED:Wall of Swords:wall of swords').
card_rarity('wall of swords'/'CED', 'Uncommon').
card_artist('wall of swords'/'CED', 'Mark Tedin').
card_flavor_text('wall of swords'/'CED', 'Just as the evil ones approached to slay Justina, she cast a great spell, imbuing her weapons with her own life force. Thus she fulfilled the prophecy: \"In the death of your savior will you find salvation.\"').

card_in_set('wall of water', 'CED').
card_original_type('wall of water'/'CED', 'Creature — Wall').
card_original_text('wall of water'/'CED', '').
card_image_name('wall of water'/'CED', 'wall of water').
card_uid('wall of water'/'CED', 'CED:Wall of Water:wall of water').
card_rarity('wall of water'/'CED', 'Uncommon').
card_artist('wall of water'/'CED', 'Richard Thomas').
card_flavor_text('wall of water'/'CED', 'A deafening roar arose as the fury of an enormous vertical river supplanted our serenity. Eddies turned into whirling geysers, leveling everything in their path.').

card_in_set('wall of wood', 'CED').
card_original_type('wall of wood'/'CED', 'Creature — Wall').
card_original_text('wall of wood'/'CED', '').
card_image_name('wall of wood'/'CED', 'wall of wood').
card_uid('wall of wood'/'CED', 'CED:Wall of Wood:wall of wood').
card_rarity('wall of wood'/'CED', 'Common').
card_artist('wall of wood'/'CED', 'Mark Tedin').
card_flavor_text('wall of wood'/'CED', 'Everybody knows that to ward off trouble, you knock on wood. But usually it\'s better to make a wall out of the wood and let trouble do the knocking.').

card_in_set('wanderlust', 'CED').
card_original_type('wanderlust'/'CED', 'Enchantment — Aura').
card_original_text('wanderlust'/'CED', '').
card_image_name('wanderlust'/'CED', 'wanderlust').
card_uid('wanderlust'/'CED', 'CED:Wanderlust:wanderlust').
card_rarity('wanderlust'/'CED', 'Uncommon').
card_artist('wanderlust'/'CED', 'Cornelius Brudi').

card_in_set('war mammoth', 'CED').
card_original_type('war mammoth'/'CED', 'Creature — Elephant').
card_original_text('war mammoth'/'CED', '').
card_image_name('war mammoth'/'CED', 'war mammoth').
card_uid('war mammoth'/'CED', 'CED:War Mammoth:war mammoth').
card_rarity('war mammoth'/'CED', 'Common').
card_artist('war mammoth'/'CED', 'Jeff A. Menges').
card_flavor_text('war mammoth'/'CED', 'I didn\'t think Mammoths could ever hold a candle to a well-trained battle horse. Then one day I turned my back on a drunken soldier. His blow never landed; Mi\'cha flung the brute over ten meters.').

card_in_set('warp artifact', 'CED').
card_original_type('warp artifact'/'CED', 'Enchantment — Aura').
card_original_text('warp artifact'/'CED', '').
card_image_name('warp artifact'/'CED', 'warp artifact').
card_uid('warp artifact'/'CED', 'CED:Warp Artifact:warp artifact').
card_rarity('warp artifact'/'CED', 'Rare').
card_artist('warp artifact'/'CED', 'Amy Weber').

card_in_set('water elemental', 'CED').
card_original_type('water elemental'/'CED', 'Creature — Elemental').
card_original_text('water elemental'/'CED', '').
card_image_name('water elemental'/'CED', 'water elemental').
card_uid('water elemental'/'CED', 'CED:Water Elemental:water elemental').
card_rarity('water elemental'/'CED', 'Uncommon').
card_artist('water elemental'/'CED', 'Jeff A. Menges').
card_flavor_text('water elemental'/'CED', 'Unpredictable as the sea itself, Water Elementals shift without warning from tranquility to tempest. Capricious and fickle, they flow restlessly from one shape to another, expressing their moods with their physical forms.').

card_in_set('weakness', 'CED').
card_original_type('weakness'/'CED', 'Enchantment — Aura').
card_original_text('weakness'/'CED', '').
card_image_name('weakness'/'CED', 'weakness').
card_uid('weakness'/'CED', 'CED:Weakness:weakness').
card_rarity('weakness'/'CED', 'Common').
card_artist('weakness'/'CED', 'Douglas Shuler').

card_in_set('web', 'CED').
card_original_type('web'/'CED', 'Enchantment — Aura').
card_original_text('web'/'CED', '').
card_image_name('web'/'CED', 'web').
card_uid('web'/'CED', 'CED:Web:web').
card_rarity('web'/'CED', 'Rare').
card_artist('web'/'CED', 'Rob Alexander').

card_in_set('wheel of fortune', 'CED').
card_original_type('wheel of fortune'/'CED', 'Sorcery').
card_original_text('wheel of fortune'/'CED', '').
card_image_name('wheel of fortune'/'CED', 'wheel of fortune').
card_uid('wheel of fortune'/'CED', 'CED:Wheel of Fortune:wheel of fortune').
card_rarity('wheel of fortune'/'CED', 'Rare').
card_artist('wheel of fortune'/'CED', 'Daniel Gelon').

card_in_set('white knight', 'CED').
card_original_type('white knight'/'CED', 'Creature — Human Knight').
card_original_text('white knight'/'CED', '').
card_image_name('white knight'/'CED', 'white knight').
card_uid('white knight'/'CED', 'CED:White Knight:white knight').
card_rarity('white knight'/'CED', 'Uncommon').
card_artist('white knight'/'CED', 'Daniel Gelon').
card_flavor_text('white knight'/'CED', 'Out of the blackness and stench of the engulfing swamp emerged a shimmering figure. Only the splattered armor and ichor-stained sword hinted at the unfathomable evil the knight had just laid waste.').

card_in_set('white ward', 'CED').
card_original_type('white ward'/'CED', 'Enchantment — Aura').
card_original_text('white ward'/'CED', '').
card_image_name('white ward'/'CED', 'white ward').
card_uid('white ward'/'CED', 'CED:White Ward:white ward').
card_rarity('white ward'/'CED', 'Uncommon').
card_artist('white ward'/'CED', 'Dan Frazier').

card_in_set('wild growth', 'CED').
card_original_type('wild growth'/'CED', 'Enchantment — Aura').
card_original_text('wild growth'/'CED', '').
card_image_name('wild growth'/'CED', 'wild growth').
card_uid('wild growth'/'CED', 'CED:Wild Growth:wild growth').
card_rarity('wild growth'/'CED', 'Common').
card_artist('wild growth'/'CED', 'Mark Poole').

card_in_set('will-o\'-the-wisp', 'CED').
card_original_type('will-o\'-the-wisp'/'CED', 'Creature — Spirit').
card_original_text('will-o\'-the-wisp'/'CED', '').
card_image_name('will-o\'-the-wisp'/'CED', 'will-o\'-the-wisp').
card_uid('will-o\'-the-wisp'/'CED', 'CED:Will-o\'-the-Wisp:will-o\'-the-wisp').
card_rarity('will-o\'-the-wisp'/'CED', 'Rare').
card_artist('will-o\'-the-wisp'/'CED', 'Jesper Myrfors').
card_flavor_text('will-o\'-the-wisp'/'CED', '\"About, about in reel and rout\nThe death-fires danced at night;\nThe water, like a witch\'s oils,\nBurnt green, and blue and white.\"\n—Samuel Coleridge, \"The Rime of the Ancient Mariner\"').

card_in_set('winter orb', 'CED').
card_original_type('winter orb'/'CED', 'Artifact').
card_original_text('winter orb'/'CED', '').
card_image_name('winter orb'/'CED', 'winter orb').
card_uid('winter orb'/'CED', 'CED:Winter Orb:winter orb').
card_rarity('winter orb'/'CED', 'Rare').
card_artist('winter orb'/'CED', 'Mark Tedin').

card_in_set('wooden sphere', 'CED').
card_original_type('wooden sphere'/'CED', 'Artifact').
card_original_text('wooden sphere'/'CED', '').
card_image_name('wooden sphere'/'CED', 'wooden sphere').
card_uid('wooden sphere'/'CED', 'CED:Wooden Sphere:wooden sphere').
card_rarity('wooden sphere'/'CED', 'Uncommon').
card_artist('wooden sphere'/'CED', 'Mark Tedin').

card_in_set('word of command', 'CED').
card_original_type('word of command'/'CED', 'Instant').
card_original_text('word of command'/'CED', '').
card_image_name('word of command'/'CED', 'word of command').
card_uid('word of command'/'CED', 'CED:Word of Command:word of command').
card_rarity('word of command'/'CED', 'Rare').
card_artist('word of command'/'CED', 'Jesper Myrfors').

card_in_set('wrath of god', 'CED').
card_original_type('wrath of god'/'CED', 'Sorcery').
card_original_text('wrath of god'/'CED', '').
card_image_name('wrath of god'/'CED', 'wrath of god').
card_uid('wrath of god'/'CED', 'CED:Wrath of God:wrath of god').
card_rarity('wrath of god'/'CED', 'Rare').
card_artist('wrath of god'/'CED', 'Quinton Hoover').

card_in_set('zombie master', 'CED').
card_original_type('zombie master'/'CED', 'Creature — Zombie').
card_original_text('zombie master'/'CED', '').
card_image_name('zombie master'/'CED', 'zombie master').
card_uid('zombie master'/'CED', 'CED:Zombie Master:zombie master').
card_rarity('zombie master'/'CED', 'Rare').
card_artist('zombie master'/'CED', 'Jeff A. Menges').
card_flavor_text('zombie master'/'CED', 'They say the Zombie Master controlled these foul creatures even before his own death, but now that he is one of them, nothing can make them betray him.').
