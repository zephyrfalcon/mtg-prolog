% Magic 2012

set('M12').
set_name('M12', 'Magic 2012').
set_release_date('M12', '2011-07-15').
set_border('M12', 'black').
set_type('M12', 'core').

card_in_set('acidic slime', 'M12').
card_original_type('acidic slime'/'M12', 'Creature — Ooze').
card_original_text('acidic slime'/'M12', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_image_name('acidic slime'/'M12', 'acidic slime').
card_uid('acidic slime'/'M12', 'M12:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'M12', 'Uncommon').
card_artist('acidic slime'/'M12', 'Karl Kopinski').
card_number('acidic slime'/'M12', '161').
card_multiverse_id('acidic slime'/'M12', '226906').

card_in_set('act of treason', 'M12').
card_original_type('act of treason'/'M12', 'Sorcery').
card_original_text('act of treason'/'M12', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_image_name('act of treason'/'M12', 'act of treason').
card_uid('act of treason'/'M12', 'M12:Act of Treason:act of treason').
card_rarity('act of treason'/'M12', 'Common').
card_artist('act of treason'/'M12', 'Eric Deschamps').
card_number('act of treason'/'M12', '121').
card_flavor_text('act of treason'/'M12', '\"Rage courses in every heart, yearning to betray its rational prison.\"\n—Sarkhan Vol').
card_multiverse_id('act of treason'/'M12', '226589').

card_in_set('adaptive automaton', 'M12').
card_original_type('adaptive automaton'/'M12', 'Artifact Creature — Construct').
card_original_text('adaptive automaton'/'M12', 'As Adaptive Automaton enters the battlefield, choose a creature type.\nAdaptive Automaton is the chosen type in addition to its other types.\nOther creatures you control of the chosen type get +1/+1.').
card_first_print('adaptive automaton', 'M12').
card_image_name('adaptive automaton'/'M12', 'adaptive automaton').
card_uid('adaptive automaton'/'M12', 'M12:Adaptive Automaton:adaptive automaton').
card_rarity('adaptive automaton'/'M12', 'Rare').
card_artist('adaptive automaton'/'M12', 'Igor Kieryluk').
card_number('adaptive automaton'/'M12', '201').
card_flavor_text('adaptive automaton'/'M12', 'Such loyalty can only be made.').
card_multiverse_id('adaptive automaton'/'M12', '220147').

card_in_set('aegis angel', 'M12').
card_original_type('aegis angel'/'M12', 'Creature — Angel').
card_original_text('aegis angel'/'M12', 'Flying\nWhen Aegis Angel enters the battlefield, another target permanent is indestructible for as long as you control Aegis Angel. (Effects that say \"destroy\" don\'t destroy that permanent. An indestructible creature can\'t be destroyed by damage.)').
card_first_print('aegis angel', 'M12').
card_image_name('aegis angel'/'M12', 'aegis angel').
card_uid('aegis angel'/'M12', 'M12:Aegis Angel:aegis angel').
card_rarity('aegis angel'/'M12', 'Rare').
card_artist('aegis angel'/'M12', 'Aleksi Briclot').
card_number('aegis angel'/'M12', '1').
card_multiverse_id('aegis angel'/'M12', '220086').

card_in_set('æther adept', 'M12').
card_original_type('æther adept'/'M12', 'Creature — Human Wizard').
card_original_text('æther adept'/'M12', 'When Æther Adept enters the battlefield, return target creature to its owner\'s hand.').
card_image_name('æther adept'/'M12', 'aether adept').
card_uid('æther adept'/'M12', 'M12:Æther Adept:aether adept').
card_rarity('æther adept'/'M12', 'Common').
card_artist('æther adept'/'M12', 'Eric Deschamps').
card_number('æther adept'/'M12', '41').
card_flavor_text('æther adept'/'M12', '\"The universe is my instrument, and the song I play upon it is one you are forbidden to hear.\"').
card_multiverse_id('æther adept'/'M12', '227222').

card_in_set('alabaster mage', 'M12').
card_original_type('alabaster mage'/'M12', 'Creature — Human Wizard').
card_original_text('alabaster mage'/'M12', '{1}{W}: Target creature you control gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_first_print('alabaster mage', 'M12').
card_image_name('alabaster mage'/'M12', 'alabaster mage').
card_uid('alabaster mage'/'M12', 'M12:Alabaster Mage:alabaster mage').
card_rarity('alabaster mage'/'M12', 'Uncommon').
card_artist('alabaster mage'/'M12', 'Izzy').
card_number('alabaster mage'/'M12', '2').
card_flavor_text('alabaster mage'/'M12', '\"We hold sacred the powers of light and life. Truth and honor are our greatest weapons.\"\n—Alabaster creed').
card_multiverse_id('alabaster mage'/'M12', '220264').

card_in_set('alluring siren', 'M12').
card_original_type('alluring siren'/'M12', 'Creature — Siren').
card_original_text('alluring siren'/'M12', '{T}: Target creature an opponent controls attacks you this turn if able.').
card_image_name('alluring siren'/'M12', 'alluring siren').
card_uid('alluring siren'/'M12', 'M12:Alluring Siren:alluring siren').
card_rarity('alluring siren'/'M12', 'Uncommon').
card_artist('alluring siren'/'M12', 'Chippy').
card_number('alluring siren'/'M12', '42').
card_flavor_text('alluring siren'/'M12', 'She lures victims with promises of flesh, captures them in a net of deceit, and destroys them in the embrace of the sea.').
card_multiverse_id('alluring siren'/'M12', '245188').

card_in_set('amphin cutthroat', 'M12').
card_original_type('amphin cutthroat'/'M12', 'Creature — Salamander Rogue').
card_original_text('amphin cutthroat'/'M12', '').
card_first_print('amphin cutthroat', 'M12').
card_image_name('amphin cutthroat'/'M12', 'amphin cutthroat').
card_uid('amphin cutthroat'/'M12', 'M12:Amphin Cutthroat:amphin cutthroat').
card_rarity('amphin cutthroat'/'M12', 'Common').
card_artist('amphin cutthroat'/'M12', 'Howard Lyon').
card_number('amphin cutthroat'/'M12', '43').
card_flavor_text('amphin cutthroat'/'M12', '\"The amphin have long built their society in secret. While surface dwellers squabbled over trivial borders, they patiently expanded, building their ammonite temple-caves. Now amphin priests eye the shore, and amphin hunters gird for war.\"\n—Gor Muldrak, Cryptohistories').
card_multiverse_id('amphin cutthroat'/'M12', '220218').

card_in_set('angel\'s feather', 'M12').
card_original_type('angel\'s feather'/'M12', 'Artifact').
card_original_text('angel\'s feather'/'M12', 'Whenever a player casts a white spell, you may gain 1 life.').
card_image_name('angel\'s feather'/'M12', 'angel\'s feather').
card_uid('angel\'s feather'/'M12', 'M12:Angel\'s Feather:angel\'s feather').
card_rarity('angel\'s feather'/'M12', 'Uncommon').
card_artist('angel\'s feather'/'M12', 'Alan Pollack').
card_number('angel\'s feather'/'M12', '202').
card_flavor_text('angel\'s feather'/'M12', 'The angel represents divine perfection; even the smallest feather is imbued with beauty and benevolence.').
card_multiverse_id('angel\'s feather'/'M12', '221520').

card_in_set('angel\'s mercy', 'M12').
card_original_type('angel\'s mercy'/'M12', 'Instant').
card_original_text('angel\'s mercy'/'M12', 'You gain 7 life.').
card_image_name('angel\'s mercy'/'M12', 'angel\'s mercy').
card_uid('angel\'s mercy'/'M12', 'M12:Angel\'s Mercy:angel\'s mercy').
card_rarity('angel\'s mercy'/'M12', 'Common').
card_artist('angel\'s mercy'/'M12', 'Andrew Robinson').
card_number('angel\'s mercy'/'M12', '4').
card_flavor_text('angel\'s mercy'/'M12', 'Angels appear before both the blessed and the cursed, though most don\'t know which they are until they feel either the angel\'s kiss or her sword.').
card_multiverse_id('angel\'s mercy'/'M12', '238569').

card_in_set('angelic destiny', 'M12').
card_original_type('angelic destiny'/'M12', 'Enchantment — Aura').
card_original_text('angelic destiny'/'M12', 'Enchant creature\nEnchanted creature gets +4/+4, has flying and first strike, and is an Angel in addition to its other types.\nWhen enchanted creature dies, return Angelic Destiny to its owner\'s hand.').
card_first_print('angelic destiny', 'M12').
card_image_name('angelic destiny'/'M12', 'angelic destiny').
card_uid('angelic destiny'/'M12', 'M12:Angelic Destiny:angelic destiny').
card_rarity('angelic destiny'/'M12', 'Mythic Rare').
card_artist('angelic destiny'/'M12', 'Jana Schirmer & Johannes Voss').
card_number('angelic destiny'/'M12', '3').
card_multiverse_id('angelic destiny'/'M12', '220230').

card_in_set('arachnus spinner', 'M12').
card_original_type('arachnus spinner'/'M12', 'Creature — Spider').
card_original_text('arachnus spinner'/'M12', 'Reach (This creature can block creatures with flying.)\nTap an untapped Spider you control: Search your graveyard and/or library for a card named Arachnus Web and put it onto the battlefield attached to target creature. If you search your library this way, shuffle it.').
card_first_print('arachnus spinner', 'M12').
card_image_name('arachnus spinner'/'M12', 'arachnus spinner').
card_uid('arachnus spinner'/'M12', 'M12:Arachnus Spinner:arachnus spinner').
card_rarity('arachnus spinner'/'M12', 'Rare').
card_artist('arachnus spinner'/'M12', 'Karl Kopinski').
card_number('arachnus spinner'/'M12', '162').
card_multiverse_id('arachnus spinner'/'M12', '220242').

card_in_set('arachnus web', 'M12').
card_original_type('arachnus web'/'M12', 'Enchantment — Aura').
card_original_text('arachnus web'/'M12', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nAt the beginning of the end step, if enchanted creature\'s power is 4 or greater, destroy Arachnus Web.').
card_first_print('arachnus web', 'M12').
card_image_name('arachnus web'/'M12', 'arachnus web').
card_uid('arachnus web'/'M12', 'M12:Arachnus Web:arachnus web').
card_rarity('arachnus web'/'M12', 'Common').
card_artist('arachnus web'/'M12', 'Karl Kopinski').
card_number('arachnus web'/'M12', '163').
card_multiverse_id('arachnus web'/'M12', '220283').

card_in_set('arbalest elite', 'M12').
card_original_type('arbalest elite'/'M12', 'Creature — Human Archer').
card_original_text('arbalest elite'/'M12', '{2}{W}, {T}: Arbalest Elite deals 3 damage to target attacking or blocking creature. Arbalest Elite doesn\'t untap during your next untap step.').
card_first_print('arbalest elite', 'M12').
card_image_name('arbalest elite'/'M12', 'arbalest elite').
card_uid('arbalest elite'/'M12', 'M12:Arbalest Elite:arbalest elite').
card_rarity('arbalest elite'/'M12', 'Uncommon').
card_artist('arbalest elite'/'M12', 'Chris Rahn').
card_number('arbalest elite'/'M12', '5').
card_flavor_text('arbalest elite'/'M12', 'There are few things as satisfying as a good, healthy thunk.').
card_multiverse_id('arbalest elite'/'M12', '220078').

card_in_set('archon of justice', 'M12').
card_original_type('archon of justice'/'M12', 'Creature — Archon').
card_original_text('archon of justice'/'M12', 'Flying\nWhen Archon of Justice dies, exile target permanent.').
card_image_name('archon of justice'/'M12', 'archon of justice').
card_uid('archon of justice'/'M12', 'M12:Archon of Justice:archon of justice').
card_rarity('archon of justice'/'M12', 'Rare').
card_artist('archon of justice'/'M12', 'Jason Chan').
card_number('archon of justice'/'M12', '6').
card_flavor_text('archon of justice'/'M12', 'In dark times, Truth bears a blade.').
card_multiverse_id('archon of justice'/'M12', '244828').

card_in_set('armored warhorse', 'M12').
card_original_type('armored warhorse'/'M12', 'Creature — Horse').
card_original_text('armored warhorse'/'M12', '').
card_first_print('armored warhorse', 'M12').
card_image_name('armored warhorse'/'M12', 'armored warhorse').
card_uid('armored warhorse'/'M12', 'M12:Armored Warhorse:armored warhorse').
card_rarity('armored warhorse'/'M12', 'Common').
card_artist('armored warhorse'/'M12', 'rk post').
card_number('armored warhorse'/'M12', '7').
card_flavor_text('armored warhorse'/'M12', '\"When we of the Northern Verge claim a mount, no peasant\'s nag will do. It must be as strong as our virtue, and must join us of its own will.\"\n—Sarlena, paladin of the Northern Verge').
card_multiverse_id('armored warhorse'/'M12', '222634').

card_in_set('assault griffin', 'M12').
card_original_type('assault griffin'/'M12', 'Creature — Griffin').
card_original_text('assault griffin'/'M12', 'Flying').
card_image_name('assault griffin'/'M12', 'assault griffin').
card_uid('assault griffin'/'M12', 'M12:Assault Griffin:assault griffin').
card_rarity('assault griffin'/'M12', 'Common').
card_artist('assault griffin'/'M12', 'Jesper Ejsing').
card_number('assault griffin'/'M12', '8').
card_flavor_text('assault griffin'/'M12', '\"Griffins do not serve. They give freely to those they trust.\"\n—Amira, skycaptain of Thune').
card_multiverse_id('assault griffin'/'M12', '236449').

card_in_set('auramancer', 'M12').
card_original_type('auramancer'/'M12', 'Creature — Human Wizard').
card_original_text('auramancer'/'M12', 'When Auramancer enters the battlefield, you may return target enchantment card from your graveyard to your hand.').
card_image_name('auramancer'/'M12', 'auramancer').
card_uid('auramancer'/'M12', 'M12:Auramancer:auramancer').
card_rarity('auramancer'/'M12', 'Common').
card_artist('auramancer'/'M12', 'Rebecca Guay').
card_number('auramancer'/'M12', '9').
card_flavor_text('auramancer'/'M12', '\"In memories, we can find our deepest reserves of strength.\"').
card_multiverse_id('auramancer'/'M12', '240308').

card_in_set('autumn\'s veil', 'M12').
card_original_type('autumn\'s veil'/'M12', 'Instant').
card_original_text('autumn\'s veil'/'M12', 'Spells you control can\'t be countered by blue or black spells this turn, and creatures you control can\'t be the targets of blue or black spells this turn.').
card_image_name('autumn\'s veil'/'M12', 'autumn\'s veil').
card_uid('autumn\'s veil'/'M12', 'M12:Autumn\'s Veil:autumn\'s veil').
card_rarity('autumn\'s veil'/'M12', 'Uncommon').
card_artist('autumn\'s veil'/'M12', 'Kekai Kotaki').
card_number('autumn\'s veil'/'M12', '164').
card_flavor_text('autumn\'s veil'/'M12', 'The rustling of leaves and a passing shadow are a dryad\'s only trace.').
card_multiverse_id('autumn\'s veil'/'M12', '241990').

card_in_set('aven fleetwing', 'M12').
card_original_type('aven fleetwing'/'M12', 'Creature — Bird Soldier').
card_original_text('aven fleetwing'/'M12', 'Flying\nHexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_first_print('aven fleetwing', 'M12').
card_image_name('aven fleetwing'/'M12', 'aven fleetwing').
card_uid('aven fleetwing'/'M12', 'M12:Aven Fleetwing:aven fleetwing').
card_rarity('aven fleetwing'/'M12', 'Common').
card_artist('aven fleetwing'/'M12', 'Wayne Reynolds').
card_number('aven fleetwing'/'M12', '44').
card_flavor_text('aven fleetwing'/'M12', '\"It was too fast, even for our veteran archers. They might as well have been blowing it kisses.\"\n—Vulok, elvish archer').
card_multiverse_id('aven fleetwing'/'M12', '220145').

card_in_set('azure mage', 'M12').
card_original_type('azure mage'/'M12', 'Creature — Human Wizard').
card_original_text('azure mage'/'M12', '{3}{U}: Draw a card.').
card_first_print('azure mage', 'M12').
card_image_name('azure mage'/'M12', 'azure mage').
card_uid('azure mage'/'M12', 'M12:Azure Mage:azure mage').
card_rarity('azure mage'/'M12', 'Uncommon').
card_artist('azure mage'/'M12', 'Izzy').
card_number('azure mage'/'M12', '45').
card_flavor_text('azure mage'/'M12', '\"We draw our power from the infinite ocean of the mind, where all manner of things can be conceived.\"\n—Azure creed').
card_multiverse_id('azure mage'/'M12', '220268').

card_in_set('belltower sphinx', 'M12').
card_original_type('belltower sphinx'/'M12', 'Creature — Sphinx').
card_original_text('belltower sphinx'/'M12', 'Flying\nWhenever a source deals damage to Belltower Sphinx, that source\'s controller puts that many cards from the top of his or her library into his or her graveyard.').
card_image_name('belltower sphinx'/'M12', 'belltower sphinx').
card_uid('belltower sphinx'/'M12', 'M12:Belltower Sphinx:belltower sphinx').
card_rarity('belltower sphinx'/'M12', 'Uncommon').
card_artist('belltower sphinx'/'M12', 'Jim Nelson').
card_number('belltower sphinx'/'M12', '46').
card_multiverse_id('belltower sphinx'/'M12', '236457').

card_in_set('benalish veteran', 'M12').
card_original_type('benalish veteran'/'M12', 'Creature — Human Soldier').
card_original_text('benalish veteran'/'M12', 'Whenever Benalish Veteran attacks, it gets +1/+1 until end of turn.').
card_first_print('benalish veteran', 'M12').
card_image_name('benalish veteran'/'M12', 'benalish veteran').
card_uid('benalish veteran'/'M12', 'M12:Benalish Veteran:benalish veteran').
card_rarity('benalish veteran'/'M12', 'Common').
card_artist('benalish veteran'/'M12', 'Steven Belledin').
card_number('benalish veteran'/'M12', '10').
card_flavor_text('benalish veteran'/'M12', '\"One day I will rest and join my comrades in the afterlife, but not while scum like you still sully this world.\"').
card_multiverse_id('benalish veteran'/'M12', '228117').

card_in_set('birds of paradise', 'M12').
card_original_type('birds of paradise'/'M12', 'Creature — Bird').
card_original_text('birds of paradise'/'M12', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_image_name('birds of paradise'/'M12', 'birds of paradise').
card_uid('birds of paradise'/'M12', 'M12:Birds of Paradise:birds of paradise').
card_rarity('birds of paradise'/'M12', 'Rare').
card_artist('birds of paradise'/'M12', 'Marcelo Vignali').
card_number('birds of paradise'/'M12', '165').
card_flavor_text('birds of paradise'/'M12', '\"The gods used their feathers to paint all the colors of the world.\"\n—Yare-Tiva, warden of Gramur forest').
card_multiverse_id('birds of paradise'/'M12', '221896').

card_in_set('blood ogre', 'M12').
card_original_type('blood ogre'/'M12', 'Creature — Ogre Warrior').
card_original_text('blood ogre'/'M12', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_first_print('blood ogre', 'M12').
card_image_name('blood ogre'/'M12', 'blood ogre').
card_uid('blood ogre'/'M12', 'M12:Blood Ogre:blood ogre').
card_rarity('blood ogre'/'M12', 'Common').
card_artist('blood ogre'/'M12', 'Christopher Moeller').
card_number('blood ogre'/'M12', '122').
card_multiverse_id('blood ogre'/'M12', '220227').

card_in_set('blood seeker', 'M12').
card_original_type('blood seeker'/'M12', 'Creature — Vampire Shaman').
card_original_text('blood seeker'/'M12', 'Whenever a creature enters the battlefield under an opponent\'s control, you may have that player lose 1 life.').
card_image_name('blood seeker'/'M12', 'blood seeker').
card_uid('blood seeker'/'M12', 'M12:Blood Seeker:blood seeker').
card_rarity('blood seeker'/'M12', 'Common').
card_artist('blood seeker'/'M12', 'Greg Staples').
card_number('blood seeker'/'M12', '81').
card_flavor_text('blood seeker'/'M12', 'A drop now is all he needs to find you later.').
card_multiverse_id('blood seeker'/'M12', '244252').

card_in_set('bloodlord of vaasgoth', 'M12').
card_original_type('bloodlord of vaasgoth'/'M12', 'Creature — Vampire Warrior').
card_original_text('bloodlord of vaasgoth'/'M12', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)\nFlying\nWhenever you cast a Vampire creature spell, it gains bloodthirst 3.').
card_image_name('bloodlord of vaasgoth'/'M12', 'bloodlord of vaasgoth').
card_uid('bloodlord of vaasgoth'/'M12', 'M12:Bloodlord of Vaasgoth:bloodlord of vaasgoth').
card_rarity('bloodlord of vaasgoth'/'M12', 'Mythic Rare').
card_artist('bloodlord of vaasgoth'/'M12', 'Greg Staples').
card_number('bloodlord of vaasgoth'/'M12', '82').
card_multiverse_id('bloodlord of vaasgoth'/'M12', '220306').

card_in_set('bloodrage vampire', 'M12').
card_original_type('bloodrage vampire'/'M12', 'Creature — Vampire').
card_original_text('bloodrage vampire'/'M12', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)').
card_first_print('bloodrage vampire', 'M12').
card_image_name('bloodrage vampire'/'M12', 'bloodrage vampire').
card_uid('bloodrage vampire'/'M12', 'M12:Bloodrage Vampire:bloodrage vampire').
card_rarity('bloodrage vampire'/'M12', 'Common').
card_artist('bloodrage vampire'/'M12', 'Steve Prescott').
card_number('bloodrage vampire'/'M12', '83').
card_flavor_text('bloodrage vampire'/'M12', 'The most genteel facades hide the most abominable desires.').
card_multiverse_id('bloodrage vampire'/'M12', '220125').

card_in_set('bonebreaker giant', 'M12').
card_original_type('bonebreaker giant'/'M12', 'Creature — Giant').
card_original_text('bonebreaker giant'/'M12', '').
card_first_print('bonebreaker giant', 'M12').
card_image_name('bonebreaker giant'/'M12', 'bonebreaker giant').
card_uid('bonebreaker giant'/'M12', 'M12:Bonebreaker Giant:bonebreaker giant').
card_rarity('bonebreaker giant'/'M12', 'Common').
card_artist('bonebreaker giant'/'M12', 'Kev Walker').
card_number('bonebreaker giant'/'M12', '123').
card_flavor_text('bonebreaker giant'/'M12', 'One thing\'s for sure—his fists are harder than your skull.').
card_multiverse_id('bonebreaker giant'/'M12', '226582').

card_in_set('bountiful harvest', 'M12').
card_original_type('bountiful harvest'/'M12', 'Sorcery').
card_original_text('bountiful harvest'/'M12', 'You gain 1 life for each land you control.').
card_image_name('bountiful harvest'/'M12', 'bountiful harvest').
card_uid('bountiful harvest'/'M12', 'M12:Bountiful Harvest:bountiful harvest').
card_rarity('bountiful harvest'/'M12', 'Common').
card_artist('bountiful harvest'/'M12', 'Jason Chan').
card_number('bountiful harvest'/'M12', '166').
card_flavor_text('bountiful harvest'/'M12', '\"When we fail to see the beauty in every tree, we are no better than humans.\"\n—Saelia, elvish scout').
card_multiverse_id('bountiful harvest'/'M12', '226901').

card_in_set('brindle boar', 'M12').
card_original_type('brindle boar'/'M12', 'Creature — Boar').
card_original_text('brindle boar'/'M12', 'Sacrifice Brindle Boar: You gain 4 life.').
card_image_name('brindle boar'/'M12', 'brindle boar').
card_uid('brindle boar'/'M12', 'M12:Brindle Boar:brindle boar').
card_rarity('brindle boar'/'M12', 'Common').
card_artist('brindle boar'/'M12', 'Dave Allsop').
card_number('brindle boar'/'M12', '167').
card_flavor_text('brindle boar'/'M12', '\"Tell the cooks to prepare the fires. Tonight we feast!\"\n—Tolar Wolfbrother, Krosan tracker').
card_multiverse_id('brindle boar'/'M12', '244824').

card_in_set('brink of disaster', 'M12').
card_original_type('brink of disaster'/'M12', 'Enchantment — Aura').
card_original_text('brink of disaster'/'M12', 'Enchant creature or land\nWhen enchanted permanent becomes tapped, destroy it.').
card_image_name('brink of disaster'/'M12', 'brink of disaster').
card_uid('brink of disaster'/'M12', 'M12:Brink of Disaster:brink of disaster').
card_rarity('brink of disaster'/'M12', 'Common').
card_artist('brink of disaster'/'M12', 'Alex Horley-Orlandelli').
card_number('brink of disaster'/'M12', '84').
card_flavor_text('brink of disaster'/'M12', '\"When you find yourself teetering on the edge of oblivion, mine will be the hands taking pleasure in giving you the final push.\"\n—Liliana Vess').
card_multiverse_id('brink of disaster'/'M12', '244246').

card_in_set('buried ruin', 'M12').
card_original_type('buried ruin'/'M12', 'Land').
card_original_text('buried ruin'/'M12', '{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Buried Ruin: Return target artifact card from your graveyard to your hand.').
card_first_print('buried ruin', 'M12').
card_image_name('buried ruin'/'M12', 'buried ruin').
card_uid('buried ruin'/'M12', 'M12:Buried Ruin:buried ruin').
card_rarity('buried ruin'/'M12', 'Uncommon').
card_artist('buried ruin'/'M12', 'Franz Vohwinkel').
card_number('buried ruin'/'M12', '224').
card_flavor_text('buried ruin'/'M12', 'History has buried its treasures deep.').
card_multiverse_id('buried ruin'/'M12', '220302').

card_in_set('call to the grave', 'M12').
card_original_type('call to the grave'/'M12', 'Enchantment').
card_original_text('call to the grave'/'M12', 'At the beginning of each player\'s upkeep, that player sacrifices a non-Zombie creature.\nAt the beginning of the end step, if no creatures are on the battlefield, sacrifice Call to the Grave.').
card_image_name('call to the grave'/'M12', 'call to the grave').
card_uid('call to the grave'/'M12', 'M12:Call to the Grave:call to the grave').
card_rarity('call to the grave'/'M12', 'Rare').
card_artist('call to the grave'/'M12', 'Daarken').
card_number('call to the grave'/'M12', '85').
card_multiverse_id('call to the grave'/'M12', '236460').

card_in_set('cancel', 'M12').
card_original_type('cancel'/'M12', 'Instant').
card_original_text('cancel'/'M12', 'Counter target spell.').
card_image_name('cancel'/'M12', 'cancel').
card_uid('cancel'/'M12', 'M12:Cancel:cancel').
card_rarity('cancel'/'M12', 'Common').
card_artist('cancel'/'M12', 'David Palumbo').
card_number('cancel'/'M12', '47').
card_multiverse_id('cancel'/'M12', '227225').

card_in_set('carnage wurm', 'M12').
card_original_type('carnage wurm'/'M12', 'Creature — Wurm').
card_original_text('carnage wurm'/'M12', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)\nTrample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_first_print('carnage wurm', 'M12').
card_image_name('carnage wurm'/'M12', 'carnage wurm').
card_uid('carnage wurm'/'M12', 'M12:Carnage Wurm:carnage wurm').
card_rarity('carnage wurm'/'M12', 'Uncommon').
card_artist('carnage wurm'/'M12', 'Dave Kendall').
card_number('carnage wurm'/'M12', '168').
card_multiverse_id('carnage wurm'/'M12', '220220').

card_in_set('celestial purge', 'M12').
card_original_type('celestial purge'/'M12', 'Instant').
card_original_text('celestial purge'/'M12', 'Exile target black or red permanent.').
card_image_name('celestial purge'/'M12', 'celestial purge').
card_uid('celestial purge'/'M12', 'M12:Celestial Purge:celestial purge').
card_rarity('celestial purge'/'M12', 'Uncommon').
card_artist('celestial purge'/'M12', 'David Palumbo').
card_number('celestial purge'/'M12', '11').
card_flavor_text('celestial purge'/'M12', '\"A world without evil? Boring. Give me vile fiends to smite and banish any day.\"\n—Sarlena, paladin of the Northern Verge').
card_multiverse_id('celestial purge'/'M12', '220195').

card_in_set('cemetery reaper', 'M12').
card_original_type('cemetery reaper'/'M12', 'Creature — Zombie').
card_original_text('cemetery reaper'/'M12', 'Other Zombie creatures you control get +1/+1.\n{2}{B}, {T}: Exile target creature card from a graveyard. Put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('cemetery reaper'/'M12', 'cemetery reaper').
card_uid('cemetery reaper'/'M12', 'M12:Cemetery Reaper:cemetery reaper').
card_rarity('cemetery reaper'/'M12', 'Rare').
card_artist('cemetery reaper'/'M12', 'Dave Allsop').
card_number('cemetery reaper'/'M12', '86').
card_flavor_text('cemetery reaper'/'M12', 'Every grave cradles a recruit.').
card_multiverse_id('cemetery reaper'/'M12', '234070').

card_in_set('chandra\'s outrage', 'M12').
card_original_type('chandra\'s outrage'/'M12', 'Instant').
card_original_text('chandra\'s outrage'/'M12', 'Chandra\'s Outrage deals 4 damage to target creature and 2 damage to that creature\'s controller.').
card_image_name('chandra\'s outrage'/'M12', 'chandra\'s outrage').
card_uid('chandra\'s outrage'/'M12', 'M12:Chandra\'s Outrage:chandra\'s outrage').
card_rarity('chandra\'s outrage'/'M12', 'Common').
card_artist('chandra\'s outrage'/'M12', 'Christopher Moeller').
card_number('chandra\'s outrage'/'M12', '125').
card_flavor_text('chandra\'s outrage'/'M12', 'Chandra never believed in using her \"inside voice.\"').
card_multiverse_id('chandra\'s outrage'/'M12', '226585').

card_in_set('chandra\'s phoenix', 'M12').
card_original_type('chandra\'s phoenix'/'M12', 'Creature — Phoenix').
card_original_text('chandra\'s phoenix'/'M12', 'Flying\nHaste (This creature can attack and {T} as soon as it comes under your control.)\nWhenever an opponent is dealt damage by a red instant or sorcery spell you control or by a red planeswalker you control, return Chandra\'s Phoenix from your graveyard to your hand.').
card_image_name('chandra\'s phoenix'/'M12', 'chandra\'s phoenix').
card_uid('chandra\'s phoenix'/'M12', 'M12:Chandra\'s Phoenix:chandra\'s phoenix').
card_rarity('chandra\'s phoenix'/'M12', 'Rare').
card_artist('chandra\'s phoenix'/'M12', 'Aleksi Briclot').
card_number('chandra\'s phoenix'/'M12', '126').
card_multiverse_id('chandra\'s phoenix'/'M12', '220298').

card_in_set('chandra, the firebrand', 'M12').
card_original_type('chandra, the firebrand'/'M12', 'Planeswalker — Chandra').
card_original_text('chandra, the firebrand'/'M12', '+1: Chandra, the Firebrand deals 1 damage to target creature or player.\n-2: When you cast your next instant or sorcery spell this turn, copy that spell. You may choose new targets for the copy.\n-6: Chandra, the Firebrand deals 6 damage to each of up to six target creatures and/or players.').
card_first_print('chandra, the firebrand', 'M12').
card_image_name('chandra, the firebrand'/'M12', 'chandra, the firebrand').
card_uid('chandra, the firebrand'/'M12', 'M12:Chandra, the Firebrand:chandra, the firebrand').
card_rarity('chandra, the firebrand'/'M12', 'Mythic Rare').
card_artist('chandra, the firebrand'/'M12', 'D. Alexander Gregory').
card_number('chandra, the firebrand'/'M12', '124').
card_multiverse_id('chandra, the firebrand'/'M12', '220146').

card_in_set('chasm drake', 'M12').
card_original_type('chasm drake'/'M12', 'Creature — Drake').
card_original_text('chasm drake'/'M12', 'Flying\nWhenever Chasm Drake attacks, target creature you control gains flying until end of turn.').
card_first_print('chasm drake', 'M12').
card_image_name('chasm drake'/'M12', 'chasm drake').
card_uid('chasm drake'/'M12', 'M12:Chasm Drake:chasm drake').
card_rarity('chasm drake'/'M12', 'Common').
card_artist('chasm drake'/'M12', 'Anthony Francisco').
card_number('chasm drake'/'M12', '48').
card_flavor_text('chasm drake'/'M12', '\"Once he\'s got you, hold on. Fall from a drake and you\'ll suffer much worse than a bruise.\"\n—Garl, drake trainer').
card_multiverse_id('chasm drake'/'M12', '220225').

card_in_set('child of night', 'M12').
card_original_type('child of night'/'M12', 'Creature — Vampire').
card_original_text('child of night'/'M12', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('child of night'/'M12', 'child of night').
card_uid('child of night'/'M12', 'M12:Child of Night:child of night').
card_rarity('child of night'/'M12', 'Common').
card_artist('child of night'/'M12', 'Ash Wood').
card_number('child of night'/'M12', '87').
card_flavor_text('child of night'/'M12', 'Sins that would be too gruesome in the light of day are made more pleasing in the dark of night.').
card_multiverse_id('child of night'/'M12', '234560').

card_in_set('circle of flame', 'M12').
card_original_type('circle of flame'/'M12', 'Enchantment').
card_original_text('circle of flame'/'M12', 'Whenever a creature without flying attacks you or a planeswalker you control, Circle of Flame deals 1 damage to that creature.').
card_image_name('circle of flame'/'M12', 'circle of flame').
card_uid('circle of flame'/'M12', 'M12:Circle of Flame:circle of flame').
card_rarity('circle of flame'/'M12', 'Uncommon').
card_artist('circle of flame'/'M12', 'Jaime Jones').
card_number('circle of flame'/'M12', '127').
card_flavor_text('circle of flame'/'M12', '\"Which do you think is a better deterrent: a moat of water or one of fire?\"\n—Chandra Nalaar').
card_multiverse_id('circle of flame'/'M12', '220273').

card_in_set('combust', 'M12').
card_original_type('combust'/'M12', 'Instant').
card_original_text('combust'/'M12', 'Combust can\'t be countered by spells or abilities.\nCombust deals 5 damage to target white or blue creature. The damage can\'t be prevented.').
card_image_name('combust'/'M12', 'combust').
card_uid('combust'/'M12', 'M12:Combust:combust').
card_rarity('combust'/'M12', 'Uncommon').
card_artist('combust'/'M12', 'Jaime Jones').
card_number('combust'/'M12', '128').
card_multiverse_id('combust'/'M12', '226591').

card_in_set('consume spirit', 'M12').
card_original_type('consume spirit'/'M12', 'Sorcery').
card_original_text('consume spirit'/'M12', 'Spend only black mana on X.\nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_image_name('consume spirit'/'M12', 'consume spirit').
card_uid('consume spirit'/'M12', 'M12:Consume Spirit:consume spirit').
card_rarity('consume spirit'/'M12', 'Uncommon').
card_artist('consume spirit'/'M12', 'Justin Sweet').
card_number('consume spirit'/'M12', '88').
card_flavor_text('consume spirit'/'M12', '\"You clerics brag about your strength of spirit, yet I take away the tiniest fragment, and you crumple like a rag doll.\"\n—Sorin Markov').
card_multiverse_id('consume spirit'/'M12', '244249').

card_in_set('coral merfolk', 'M12').
card_original_type('coral merfolk'/'M12', 'Creature — Merfolk').
card_original_text('coral merfolk'/'M12', '').
card_image_name('coral merfolk'/'M12', 'coral merfolk').
card_uid('coral merfolk'/'M12', 'M12:Coral Merfolk:coral merfolk').
card_rarity('coral merfolk'/'M12', 'Common').
card_artist('coral merfolk'/'M12', 'rk post').
card_number('coral merfolk'/'M12', '49').
card_flavor_text('coral merfolk'/'M12', 'Merfolk are best known for their subtle magic. But those who try to fight them in their own territory quickly realize that merfolk are hardly defenseless on the field of battle.').
card_multiverse_id('coral merfolk'/'M12', '227223').

card_in_set('crimson mage', 'M12').
card_original_type('crimson mage'/'M12', 'Creature — Human Shaman').
card_original_text('crimson mage'/'M12', '{R}: Target creature you control gains haste until end of turn. (It can attack and {T} this turn.)').
card_first_print('crimson mage', 'M12').
card_image_name('crimson mage'/'M12', 'crimson mage').
card_uid('crimson mage'/'M12', 'M12:Crimson Mage:crimson mage').
card_rarity('crimson mage'/'M12', 'Uncommon').
card_artist('crimson mage'/'M12', 'Izzy').
card_number('crimson mage'/'M12', '129').
card_flavor_text('crimson mage'/'M12', '\"We wield the fires of rage. War is our blood and destruction our birthright.\"\n—Crimson creed').
card_multiverse_id('crimson mage'/'M12', '220069').

card_in_set('crown of empires', 'M12').
card_original_type('crown of empires'/'M12', 'Artifact').
card_original_text('crown of empires'/'M12', '{3}, {T}: Tap target creature. Gain control of that creature instead if you control artifacts named Scepter of Empires and Throne of Empires.').
card_first_print('crown of empires', 'M12').
card_image_name('crown of empires'/'M12', 'crown of empires').
card_uid('crown of empires'/'M12', 'M12:Crown of Empires:crown of empires').
card_rarity('crown of empires'/'M12', 'Uncommon').
card_artist('crown of empires'/'M12', 'John Avon').
card_number('crown of empires'/'M12', '203').
card_flavor_text('crown of empires'/'M12', '\"With this crown, assert your authority.\"\n—Crown inscription').
card_multiverse_id('crown of empires'/'M12', '220297').

card_in_set('crumbling colossus', 'M12').
card_original_type('crumbling colossus'/'M12', 'Artifact Creature — Golem').
card_original_text('crumbling colossus'/'M12', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nWhen Crumbling Colossus attacks, sacrifice it at end of combat.').
card_first_print('crumbling colossus', 'M12').
card_image_name('crumbling colossus'/'M12', 'crumbling colossus').
card_uid('crumbling colossus'/'M12', 'M12:Crumbling Colossus:crumbling colossus').
card_rarity('crumbling colossus'/'M12', 'Uncommon').
card_artist('crumbling colossus'/'M12', 'Michael C. Hayes').
card_number('crumbling colossus'/'M12', '204').
card_multiverse_id('crumbling colossus'/'M12', '220113').

card_in_set('cudgel troll', 'M12').
card_original_type('cudgel troll'/'M12', 'Creature — Troll').
card_original_text('cudgel troll'/'M12', '{G}: Regenerate Cudgel Troll. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_image_name('cudgel troll'/'M12', 'cudgel troll').
card_uid('cudgel troll'/'M12', 'M12:Cudgel Troll:cudgel troll').
card_rarity('cudgel troll'/'M12', 'Uncommon').
card_artist('cudgel troll'/'M12', 'Jesper Ejsing').
card_number('cudgel troll'/'M12', '169').
card_multiverse_id('cudgel troll'/'M12', '221895').

card_in_set('dark favor', 'M12').
card_original_type('dark favor'/'M12', 'Enchantment — Aura').
card_original_text('dark favor'/'M12', 'Enchant creature\nWhen Dark Favor enters the battlefield, you lose 1 life.\nEnchanted creature gets +3/+1.').
card_first_print('dark favor', 'M12').
card_image_name('dark favor'/'M12', 'dark favor').
card_uid('dark favor'/'M12', 'M12:Dark Favor:dark favor').
card_rarity('dark favor'/'M12', 'Common').
card_artist('dark favor'/'M12', 'Allen Williams').
card_number('dark favor'/'M12', '89').
card_flavor_text('dark favor'/'M12', 'When he began to curse what he held holy, his strength grew unrivaled.').
card_multiverse_id('dark favor'/'M12', '220293').

card_in_set('day of judgment', 'M12').
card_original_type('day of judgment'/'M12', 'Sorcery').
card_original_text('day of judgment'/'M12', 'Destroy all creatures.').
card_image_name('day of judgment'/'M12', 'day of judgment').
card_uid('day of judgment'/'M12', 'M12:Day of Judgment:day of judgment').
card_rarity('day of judgment'/'M12', 'Rare').
card_artist('day of judgment'/'M12', 'Vincent Proce').
card_number('day of judgment'/'M12', '12').
card_multiverse_id('day of judgment'/'M12', '220139').

card_in_set('deathmark', 'M12').
card_original_type('deathmark'/'M12', 'Sorcery').
card_original_text('deathmark'/'M12', 'Destroy target green or white creature.').
card_image_name('deathmark'/'M12', 'deathmark').
card_uid('deathmark'/'M12', 'M12:Deathmark:deathmark').
card_rarity('deathmark'/'M12', 'Uncommon').
card_artist('deathmark'/'M12', 'Steven Belledin').
card_number('deathmark'/'M12', '90').
card_flavor_text('deathmark'/'M12', '\"Let the final vision of doom be etched upon their unblinking eyes.\"\n—Zul Ashur, lich lord').
card_multiverse_id('deathmark'/'M12', '221516').

card_in_set('demon\'s horn', 'M12').
card_original_type('demon\'s horn'/'M12', 'Artifact').
card_original_text('demon\'s horn'/'M12', 'Whenever a player casts a black spell, you may gain 1 life.').
card_image_name('demon\'s horn'/'M12', 'demon\'s horn').
card_uid('demon\'s horn'/'M12', 'M12:Demon\'s Horn:demon\'s horn').
card_rarity('demon\'s horn'/'M12', 'Uncommon').
card_artist('demon\'s horn'/'M12', 'Alan Pollack').
card_number('demon\'s horn'/'M12', '205').
card_flavor_text('demon\'s horn'/'M12', 'Its curve mimics the twists of life and death.').
card_multiverse_id('demon\'s horn'/'M12', '221522').

card_in_set('demystify', 'M12').
card_original_type('demystify'/'M12', 'Instant').
card_original_text('demystify'/'M12', 'Destroy target enchantment.').
card_image_name('demystify'/'M12', 'demystify').
card_uid('demystify'/'M12', 'M12:Demystify:demystify').
card_rarity('demystify'/'M12', 'Common').
card_artist('demystify'/'M12', 'Véronique Meignaud').
card_number('demystify'/'M12', '13').
card_flavor_text('demystify'/'M12', 'Behind every mage is a trail of shattered lies.').
card_multiverse_id('demystify'/'M12', '238328').

card_in_set('devouring swarm', 'M12').
card_original_type('devouring swarm'/'M12', 'Creature — Insect').
card_original_text('devouring swarm'/'M12', 'Flying\nSacrifice a creature: Devouring Swarm gets +1/+1 until end of turn.').
card_first_print('devouring swarm', 'M12').
card_image_name('devouring swarm'/'M12', 'devouring swarm').
card_uid('devouring swarm'/'M12', 'M12:Devouring Swarm:devouring swarm').
card_rarity('devouring swarm'/'M12', 'Common').
card_artist('devouring swarm'/'M12', 'Wayne England').
card_number('devouring swarm'/'M12', '91').
card_flavor_text('devouring swarm'/'M12', 'Their arrival is heralded by the deafening hum of ten thousand wings. Their departure is marked by the silence of the dead.').
card_multiverse_id('devouring swarm'/'M12', '226563').

card_in_set('diabolic tutor', 'M12').
card_original_type('diabolic tutor'/'M12', 'Sorcery').
card_original_text('diabolic tutor'/'M12', 'Search your library for a card and put that card into your hand. Then shuffle your library.').
card_image_name('diabolic tutor'/'M12', 'diabolic tutor').
card_uid('diabolic tutor'/'M12', 'M12:Diabolic Tutor:diabolic tutor').
card_rarity('diabolic tutor'/'M12', 'Uncommon').
card_artist('diabolic tutor'/'M12', 'Greg Staples').
card_number('diabolic tutor'/'M12', '92').
card_flavor_text('diabolic tutor'/'M12', 'The wise always keep an ear open to the whispers of power.').
card_multiverse_id('diabolic tutor'/'M12', '221519').

card_in_set('disentomb', 'M12').
card_original_type('disentomb'/'M12', 'Sorcery').
card_original_text('disentomb'/'M12', 'Return target creature card from your graveyard to your hand.').
card_image_name('disentomb'/'M12', 'disentomb').
card_uid('disentomb'/'M12', 'M12:Disentomb:disentomb').
card_rarity('disentomb'/'M12', 'Common').
card_artist('disentomb'/'M12', 'Alex Horley-Orlandelli').
card_number('disentomb'/'M12', '93').
card_flavor_text('disentomb'/'M12', '\"Stop complaining. You can rest when you\'re dead. Oh—sorry.\"\n—Liliana Vess').
card_multiverse_id('disentomb'/'M12', '244245').

card_in_set('distress', 'M12').
card_original_type('distress'/'M12', 'Sorcery').
card_original_text('distress'/'M12', 'Target player reveals his or her hand. You choose a nonland card from it. That player discards that card.').
card_image_name('distress'/'M12', 'distress').
card_uid('distress'/'M12', 'M12:Distress:distress').
card_rarity('distress'/'M12', 'Common').
card_artist('distress'/'M12', 'Michael C. Hayes').
card_number('distress'/'M12', '94').
card_flavor_text('distress'/'M12', '\"Of course I\'m sure I\'ve gone mad. The little man who crawled out of my eye was quite clear on this.\"').
card_multiverse_id('distress'/'M12', '236908').

card_in_set('divination', 'M12').
card_original_type('divination'/'M12', 'Sorcery').
card_original_text('divination'/'M12', 'Draw two cards.').
card_image_name('divination'/'M12', 'divination').
card_uid('divination'/'M12', 'M12:Divination:divination').
card_rarity('divination'/'M12', 'Common').
card_artist('divination'/'M12', 'Howard Lyon').
card_number('divination'/'M12', '50').
card_flavor_text('divination'/'M12', '\"The key to unlocking this puzzle is within you.\"\n—Doriel, mentor of Mistral Isle').
card_multiverse_id('divination'/'M12', '220194').

card_in_set('divine favor', 'M12').
card_original_type('divine favor'/'M12', 'Enchantment — Aura').
card_original_text('divine favor'/'M12', 'Enchant creature\nWhen Divine Favor enters the battlefield, you gain 3 life.\nEnchanted creature gets +1/+3.').
card_first_print('divine favor', 'M12').
card_image_name('divine favor'/'M12', 'divine favor').
card_uid('divine favor'/'M12', 'M12:Divine Favor:divine favor').
card_rarity('divine favor'/'M12', 'Common').
card_artist('divine favor'/'M12', 'Allen Williams').
card_number('divine favor'/'M12', '14').
card_flavor_text('divine favor'/'M12', 'With an armory of light, even the squire may champion her people.').
card_multiverse_id('divine favor'/'M12', '220219').

card_in_set('djinn of wishes', 'M12').
card_original_type('djinn of wishes'/'M12', 'Creature — Djinn').
card_original_text('djinn of wishes'/'M12', 'Flying\nDjinn of Wishes enters the battlefield with three wish counters on it.\n{2}{U}{U}, Remove a wish counter from Djinn of Wishes: Reveal the top card of your library. You may play that card without paying its mana cost. If you don\'t, exile it.').
card_image_name('djinn of wishes'/'M12', 'djinn of wishes').
card_uid('djinn of wishes'/'M12', 'M12:Djinn of Wishes:djinn of wishes').
card_rarity('djinn of wishes'/'M12', 'Rare').
card_artist('djinn of wishes'/'M12', 'Kev Walker').
card_number('djinn of wishes'/'M12', '51').
card_multiverse_id('djinn of wishes'/'M12', '227305').

card_in_set('doom blade', 'M12').
card_original_type('doom blade'/'M12', 'Instant').
card_original_text('doom blade'/'M12', 'Destroy target nonblack creature.').
card_image_name('doom blade'/'M12', 'doom blade').
card_uid('doom blade'/'M12', 'M12:Doom Blade:doom blade').
card_rarity('doom blade'/'M12', 'Common').
card_artist('doom blade'/'M12', 'Chippy').
card_number('doom blade'/'M12', '95').
card_multiverse_id('doom blade'/'M12', '226560').

card_in_set('doubling chant', 'M12').
card_original_type('doubling chant'/'M12', 'Sorcery').
card_original_text('doubling chant'/'M12', 'For each creature you control, you may search your library for a creature card with the same name as that creature. Put those cards onto the battlefield, then shuffle your library.').
card_first_print('doubling chant', 'M12').
card_image_name('doubling chant'/'M12', 'doubling chant').
card_uid('doubling chant'/'M12', 'M12:Doubling Chant:doubling chant').
card_rarity('doubling chant'/'M12', 'Rare').
card_artist('doubling chant'/'M12', 'Wayne England').
card_number('doubling chant'/'M12', '170').
card_flavor_text('doubling chant'/'M12', 'What\'s scarier than one of every beast in the forest?').
card_multiverse_id('doubling chant'/'M12', '220152').

card_in_set('dragon\'s claw', 'M12').
card_original_type('dragon\'s claw'/'M12', 'Artifact').
card_original_text('dragon\'s claw'/'M12', 'Whenever a player casts a red spell, you may gain 1 life.').
card_image_name('dragon\'s claw'/'M12', 'dragon\'s claw').
card_uid('dragon\'s claw'/'M12', 'M12:Dragon\'s Claw:dragon\'s claw').
card_rarity('dragon\'s claw'/'M12', 'Uncommon').
card_artist('dragon\'s claw'/'M12', 'Alan Pollack').
card_number('dragon\'s claw'/'M12', '206').
card_flavor_text('dragon\'s claw'/'M12', '\"If there is a fire that never ceases to burn, it surely lies in the maw of a dragon.\"\n—Sarkhan Vol').
card_multiverse_id('dragon\'s claw'/'M12', '221523').

card_in_set('dragonskull summit', 'M12').
card_original_type('dragonskull summit'/'M12', 'Land').
card_original_text('dragonskull summit'/'M12', 'Dragonskull Summit enters the battlefield tapped unless you control a Swamp or a Mountain.\n{T}: Add {B} or {R} to your mana pool.').
card_image_name('dragonskull summit'/'M12', 'dragonskull summit').
card_uid('dragonskull summit'/'M12', 'M12:Dragonskull Summit:dragonskull summit').
card_rarity('dragonskull summit'/'M12', 'Rare').
card_artist('dragonskull summit'/'M12', 'Jon Foster').
card_number('dragonskull summit'/'M12', '225').
card_multiverse_id('dragonskull summit'/'M12', '221921').

card_in_set('drifting shade', 'M12').
card_original_type('drifting shade'/'M12', 'Creature — Shade').
card_original_text('drifting shade'/'M12', 'Flying\n{B}: Drifting Shade gets +1/+1 until end of turn.').
card_first_print('drifting shade', 'M12').
card_image_name('drifting shade'/'M12', 'drifting shade').
card_uid('drifting shade'/'M12', 'M12:Drifting Shade:drifting shade').
card_rarity('drifting shade'/'M12', 'Common').
card_artist('drifting shade'/'M12', 'Tomasz Jedruszek').
card_number('drifting shade'/'M12', '96').
card_flavor_text('drifting shade'/'M12', 'It stalks the fens by the light of the full moon, gathering shadows as it floats among them.').
card_multiverse_id('drifting shade'/'M12', '220088').

card_in_set('drowned catacomb', 'M12').
card_original_type('drowned catacomb'/'M12', 'Land').
card_original_text('drowned catacomb'/'M12', 'Drowned Catacomb enters the battlefield tapped unless you control an Island or a Swamp.\n{T}: Add {U} or {B} to your mana pool.').
card_image_name('drowned catacomb'/'M12', 'drowned catacomb').
card_uid('drowned catacomb'/'M12', 'M12:Drowned Catacomb:drowned catacomb').
card_rarity('drowned catacomb'/'M12', 'Rare').
card_artist('drowned catacomb'/'M12', 'Dave Kendall').
card_number('drowned catacomb'/'M12', '226').
card_multiverse_id('drowned catacomb'/'M12', '221920').

card_in_set('druidic satchel', 'M12').
card_original_type('druidic satchel'/'M12', 'Artifact').
card_original_text('druidic satchel'/'M12', '{2}, {T}: Reveal the top card of your library. If it\'s a creature card, put a 1/1 green Saproling creature token onto the battlefield. If it\'s a land card, put that card onto the battlefield under your control. If it\'s a noncreature, nonland card, you gain 2 life.').
card_first_print('druidic satchel', 'M12').
card_image_name('druidic satchel'/'M12', 'druidic satchel').
card_uid('druidic satchel'/'M12', 'M12:Druidic Satchel:druidic satchel').
card_rarity('druidic satchel'/'M12', 'Rare').
card_artist('druidic satchel'/'M12', 'Matt Stewart').
card_number('druidic satchel'/'M12', '207').
card_multiverse_id('druidic satchel'/'M12', '220098').

card_in_set('dungrove elder', 'M12').
card_original_type('dungrove elder'/'M12', 'Creature — Treefolk').
card_original_text('dungrove elder'/'M12', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nDungrove Elder\'s power and toughness are each equal to the number of Forests you control.').
card_image_name('dungrove elder'/'M12', 'dungrove elder').
card_uid('dungrove elder'/'M12', 'M12:Dungrove Elder:dungrove elder').
card_rarity('dungrove elder'/'M12', 'Rare').
card_artist('dungrove elder'/'M12', 'Matt Stewart').
card_number('dungrove elder'/'M12', '171').
card_flavor_text('dungrove elder'/'M12', 'All the trees of the forest are his army, and every leaf his shield.').
card_multiverse_id('dungrove elder'/'M12', '220210').

card_in_set('duskhunter bat', 'M12').
card_original_type('duskhunter bat'/'M12', 'Creature — Bat').
card_original_text('duskhunter bat'/'M12', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nFlying').
card_first_print('duskhunter bat', 'M12').
card_image_name('duskhunter bat'/'M12', 'duskhunter bat').
card_uid('duskhunter bat'/'M12', 'M12:Duskhunter Bat:duskhunter bat').
card_rarity('duskhunter bat'/'M12', 'Common').
card_artist('duskhunter bat'/'M12', 'Jesper Ejsing').
card_number('duskhunter bat'/'M12', '97').
card_flavor_text('duskhunter bat'/'M12', 'The swamps go silent at its approach, but it still hears the heartbeat of the prey within.').
card_multiverse_id('duskhunter bat'/'M12', '220259').

card_in_set('elite vanguard', 'M12').
card_original_type('elite vanguard'/'M12', 'Creature — Human Soldier').
card_original_text('elite vanguard'/'M12', '').
card_image_name('elite vanguard'/'M12', 'elite vanguard').
card_uid('elite vanguard'/'M12', 'M12:Elite Vanguard:elite vanguard').
card_rarity('elite vanguard'/'M12', 'Uncommon').
card_artist('elite vanguard'/'M12', 'Mark Tedin').
card_number('elite vanguard'/'M12', '15').
card_flavor_text('elite vanguard'/'M12', 'The vanguard is skilled at waging war alone. The enemy is often defeated before its reinforcements reach the front.').
card_multiverse_id('elite vanguard'/'M12', '240315').

card_in_set('elixir of immortality', 'M12').
card_original_type('elixir of immortality'/'M12', 'Artifact').
card_original_text('elixir of immortality'/'M12', '{2}, {T}: You gain 5 life. Shuffle Elixir of Immortality and your graveyard into their owner\'s library.').
card_image_name('elixir of immortality'/'M12', 'elixir of immortality').
card_uid('elixir of immortality'/'M12', 'M12:Elixir of Immortality:elixir of immortality').
card_rarity('elixir of immortality'/'M12', 'Uncommon').
card_artist('elixir of immortality'/'M12', 'Zoltan Boros & Gabor Szikszai').
card_number('elixir of immortality'/'M12', '208').
card_flavor_text('elixir of immortality'/'M12', '\"Bottled life. Not as tasty as I\'m used to, rather stale, but it has the same effect.\"\n—Baron Sengir').
card_multiverse_id('elixir of immortality'/'M12', '221525').

card_in_set('elvish archdruid', 'M12').
card_original_type('elvish archdruid'/'M12', 'Creature — Elf Druid').
card_original_text('elvish archdruid'/'M12', 'Other Elf creatures you control get +1/+1.\n{T}: Add {G} to your mana pool for each Elf you control.').
card_image_name('elvish archdruid'/'M12', 'elvish archdruid').
card_uid('elvish archdruid'/'M12', 'M12:Elvish Archdruid:elvish archdruid').
card_rarity('elvish archdruid'/'M12', 'Rare').
card_artist('elvish archdruid'/'M12', 'Karl Kopinski').
card_number('elvish archdruid'/'M12', '172').
card_flavor_text('elvish archdruid'/'M12', 'He knows the name of every elf born in the last four centuries. More importantly, they all know his.').
card_multiverse_id('elvish archdruid'/'M12', '237007').

card_in_set('fiery hellhound', 'M12').
card_original_type('fiery hellhound'/'M12', 'Creature — Elemental Hound').
card_original_text('fiery hellhound'/'M12', '{R}: Fiery Hellhound gets +1/+0 until end of turn.').
card_image_name('fiery hellhound'/'M12', 'fiery hellhound').
card_uid('fiery hellhound'/'M12', 'M12:Fiery Hellhound:fiery hellhound').
card_rarity('fiery hellhound'/'M12', 'Common').
card_artist('fiery hellhound'/'M12', 'Ted Galaday').
card_number('fiery hellhound'/'M12', '130').
card_flavor_text('fiery hellhound'/'M12', 'There\'s nothing more exciting than a game of fetch the still-beating heart.').
card_multiverse_id('fiery hellhound'/'M12', '238331').

card_in_set('fireball', 'M12').
card_original_type('fireball'/'M12', 'Sorcery').
card_original_text('fireball'/'M12', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.\nFireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'M12', 'fireball').
card_uid('fireball'/'M12', 'M12:Fireball:fireball').
card_rarity('fireball'/'M12', 'Uncommon').
card_artist('fireball'/'M12', 'Dave Dorman').
card_number('fireball'/'M12', '131').
card_multiverse_id('fireball'/'M12', '221550').

card_in_set('firebreathing', 'M12').
card_original_type('firebreathing'/'M12', 'Enchantment — Aura').
card_original_text('firebreathing'/'M12', 'Enchant creature\n{R}: Enchanted creature gets +1/+0 until end of turn.').
card_image_name('firebreathing'/'M12', 'firebreathing').
card_uid('firebreathing'/'M12', 'M12:Firebreathing:firebreathing').
card_rarity('firebreathing'/'M12', 'Common').
card_artist('firebreathing'/'M12', 'Aleksi Briclot').
card_number('firebreathing'/'M12', '132').
card_flavor_text('firebreathing'/'M12', 'The mage breathed in life-giving air and breathed out death-bringing fire.').
card_multiverse_id('firebreathing'/'M12', '226588').

card_in_set('flameblast dragon', 'M12').
card_original_type('flameblast dragon'/'M12', 'Creature — Dragon').
card_original_text('flameblast dragon'/'M12', 'Flying\nWhenever Flameblast Dragon attacks, you may pay {X}{R}. If you do, Flameblast Dragon deals X damage to target creature or player.').
card_image_name('flameblast dragon'/'M12', 'flameblast dragon').
card_uid('flameblast dragon'/'M12', 'M12:Flameblast Dragon:flameblast dragon').
card_rarity('flameblast dragon'/'M12', 'Rare').
card_artist('flameblast dragon'/'M12', 'Jaime Jones').
card_number('flameblast dragon'/'M12', '133').
card_flavor_text('flameblast dragon'/'M12', 'He marks his territory with fire and wrath, etching the mountainside with his charring breath.').
card_multiverse_id('flameblast dragon'/'M12', '244720').

card_in_set('flashfreeze', 'M12').
card_original_type('flashfreeze'/'M12', 'Instant').
card_original_text('flashfreeze'/'M12', 'Counter target red or green spell.').
card_image_name('flashfreeze'/'M12', 'flashfreeze').
card_uid('flashfreeze'/'M12', 'M12:Flashfreeze:flashfreeze').
card_rarity('flashfreeze'/'M12', 'Uncommon').
card_artist('flashfreeze'/'M12', 'Brian Despain').
card_number('flashfreeze'/'M12', '52').
card_flavor_text('flashfreeze'/'M12', '\"Is cold merely the absence of heat, or a force unto itself? Would a display of its power help you decide?\"\n—Zubarek, Icewinder Magus').
card_multiverse_id('flashfreeze'/'M12', '220249').

card_in_set('flight', 'M12').
card_original_type('flight'/'M12', 'Enchantment — Aura').
card_original_text('flight'/'M12', 'Enchant creature\nEnchanted creature has flying.').
card_image_name('flight'/'M12', 'flight').
card_uid('flight'/'M12', 'M12:Flight:flight').
card_rarity('flight'/'M12', 'Common').
card_artist('flight'/'M12', 'Mark Zug').
card_number('flight'/'M12', '53').
card_flavor_text('flight'/'M12', '\"Those who claim to know the world in its entirety rarely have the requisite perspective.\"\n—Ulmarus the Wise').
card_multiverse_id('flight'/'M12', '237366').

card_in_set('fling', 'M12').
card_original_type('fling'/'M12', 'Instant').
card_original_text('fling'/'M12', 'As an additional cost to cast Fling, sacrifice a creature.\nFling deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_image_name('fling'/'M12', 'fling').
card_uid('fling'/'M12', 'M12:Fling:fling').
card_rarity('fling'/'M12', 'Common').
card_artist('fling'/'M12', 'Paolo Parente').
card_number('fling'/'M12', '134').
card_flavor_text('fling'/'M12', 'About halfway to the target, Snukk vowed that he had been tricked for the last time.').
card_multiverse_id('fling'/'M12', '241858').

card_in_set('fog', 'M12').
card_original_type('fog'/'M12', 'Instant').
card_original_text('fog'/'M12', 'Prevent all combat damage that would be dealt this turn.').
card_image_name('fog'/'M12', 'fog').
card_uid('fog'/'M12', 'M12:Fog:fog').
card_rarity('fog'/'M12', 'Common').
card_artist('fog'/'M12', 'Jaime Jones').
card_number('fog'/'M12', '173').
card_flavor_text('fog'/'M12', 'It\'s hard enough to find your way through uncharted wilderness when you can see the tip of your nose.').
card_multiverse_id('fog'/'M12', '226900').

card_in_set('forest', 'M12').
card_original_type('forest'/'M12', 'Basic Land — Forest').
card_original_text('forest'/'M12', 'G').
card_image_name('forest'/'M12', 'forest1').
card_uid('forest'/'M12', 'M12:Forest:forest1').
card_rarity('forest'/'M12', 'Basic Land').
card_artist('forest'/'M12', 'Glen Angus').
card_number('forest'/'M12', '246').
card_multiverse_id('forest'/'M12', '244320').

card_in_set('forest', 'M12').
card_original_type('forest'/'M12', 'Basic Land — Forest').
card_original_text('forest'/'M12', 'G').
card_image_name('forest'/'M12', 'forest2').
card_uid('forest'/'M12', 'M12:Forest:forest2').
card_rarity('forest'/'M12', 'Basic Land').
card_artist('forest'/'M12', 'Volkan Baga').
card_number('forest'/'M12', '247').
card_multiverse_id('forest'/'M12', '244319').

card_in_set('forest', 'M12').
card_original_type('forest'/'M12', 'Basic Land — Forest').
card_original_text('forest'/'M12', 'G').
card_image_name('forest'/'M12', 'forest3').
card_uid('forest'/'M12', 'M12:Forest:forest3').
card_rarity('forest'/'M12', 'Basic Land').
card_artist('forest'/'M12', 'Jim Nelson').
card_number('forest'/'M12', '248').
card_multiverse_id('forest'/'M12', '244321').

card_in_set('forest', 'M12').
card_original_type('forest'/'M12', 'Basic Land — Forest').
card_original_text('forest'/'M12', 'G').
card_image_name('forest'/'M12', 'forest4').
card_uid('forest'/'M12', 'M12:Forest:forest4').
card_rarity('forest'/'M12', 'Basic Land').
card_artist('forest'/'M12', 'Ryan Pancoast').
card_number('forest'/'M12', '249').
card_multiverse_id('forest'/'M12', '244322').

card_in_set('frost breath', 'M12').
card_original_type('frost breath'/'M12', 'Instant').
card_original_text('frost breath'/'M12', 'Tap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_first_print('frost breath', 'M12').
card_image_name('frost breath'/'M12', 'frost breath').
card_uid('frost breath'/'M12', 'M12:Frost Breath:frost breath').
card_rarity('frost breath'/'M12', 'Common').
card_artist('frost breath'/'M12', 'Mike Bierek').
card_number('frost breath'/'M12', '54').
card_flavor_text('frost breath'/'M12', 'If you have to be stuck somewhere for a while, it\'s better to be stuck there with a friend.').
card_multiverse_id('frost breath'/'M12', '220284').

card_in_set('frost titan', 'M12').
card_original_type('frost titan'/'M12', 'Creature — Giant').
card_original_text('frost titan'/'M12', 'Whenever Frost Titan becomes the target of a spell or ability an opponent controls, counter that spell or ability unless its controller pays {2}.\nWhenever Frost Titan enters the battlefield or attacks, tap target permanent. It doesn\'t untap during its controller\'s next untap step.').
card_image_name('frost titan'/'M12', 'frost titan').
card_uid('frost titan'/'M12', 'M12:Frost Titan:frost titan').
card_rarity('frost titan'/'M12', 'Mythic Rare').
card_artist('frost titan'/'M12', 'Mike Bierek').
card_number('frost titan'/'M12', '55').
card_multiverse_id('frost titan'/'M12', '235191').

card_in_set('furyborn hellkite', 'M12').
card_original_type('furyborn hellkite'/'M12', 'Creature — Dragon').
card_original_text('furyborn hellkite'/'M12', 'Bloodthirst 6 (If an opponent was dealt damage this turn, this creature enters the battlefield with six +1/+1 counters on it.)\nFlying').
card_first_print('furyborn hellkite', 'M12').
card_image_name('furyborn hellkite'/'M12', 'furyborn hellkite').
card_uid('furyborn hellkite'/'M12', 'M12:Furyborn Hellkite:furyborn hellkite').
card_rarity('furyborn hellkite'/'M12', 'Mythic Rare').
card_artist('furyborn hellkite'/'M12', 'Brad Rigney').
card_number('furyborn hellkite'/'M12', '135').
card_flavor_text('furyborn hellkite'/'M12', '\"Careful. A drop of blood here means death for us all.\"\n—Kalek, mountain guide').
card_multiverse_id('furyborn hellkite'/'M12', '220103').

card_in_set('garruk\'s companion', 'M12').
card_original_type('garruk\'s companion'/'M12', 'Creature — Beast').
card_original_text('garruk\'s companion'/'M12', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('garruk\'s companion'/'M12', 'garruk\'s companion').
card_uid('garruk\'s companion'/'M12', 'M12:Garruk\'s Companion:garruk\'s companion').
card_rarity('garruk\'s companion'/'M12', 'Common').
card_artist('garruk\'s companion'/'M12', 'Efrem Palacios').
card_number('garruk\'s companion'/'M12', '175').
card_multiverse_id('garruk\'s companion'/'M12', '226896').

card_in_set('garruk\'s horde', 'M12').
card_original_type('garruk\'s horde'/'M12', 'Creature — Beast').
card_original_text('garruk\'s horde'/'M12', 'Trample \nPlay with the top card of your library revealed.\nYou may cast the top card of your library if it\'s a creature card. (Do this only any time you could cast that creature card. You still pay the spell\'s costs.)').
card_image_name('garruk\'s horde'/'M12', 'garruk\'s horde').
card_uid('garruk\'s horde'/'M12', 'M12:Garruk\'s Horde:garruk\'s horde').
card_rarity('garruk\'s horde'/'M12', 'Rare').
card_artist('garruk\'s horde'/'M12', 'Steve Prescott').
card_number('garruk\'s horde'/'M12', '176').
card_multiverse_id('garruk\'s horde'/'M12', '220077').

card_in_set('garruk, primal hunter', 'M12').
card_original_type('garruk, primal hunter'/'M12', 'Planeswalker — Garruk').
card_original_text('garruk, primal hunter'/'M12', '+1: Put a 3/3 green Beast creature token onto the battlefield.\n-3: Draw cards equal to the greatest power among creatures you control.\n-6: Put a 6/6 green Wurm creature token onto the battlefield for each land you control.').
card_first_print('garruk, primal hunter', 'M12').
card_image_name('garruk, primal hunter'/'M12', 'garruk, primal hunter').
card_uid('garruk, primal hunter'/'M12', 'M12:Garruk, Primal Hunter:garruk, primal hunter').
card_rarity('garruk, primal hunter'/'M12', 'Mythic Rare').
card_artist('garruk, primal hunter'/'M12', 'D. Alexander Gregory').
card_number('garruk, primal hunter'/'M12', '174').
card_multiverse_id('garruk, primal hunter'/'M12', '220100').

card_in_set('giant spider', 'M12').
card_original_type('giant spider'/'M12', 'Creature — Spider').
card_original_text('giant spider'/'M12', 'Reach (This creature can block creatures with flying.)').
card_image_name('giant spider'/'M12', 'giant spider').
card_uid('giant spider'/'M12', 'M12:Giant Spider:giant spider').
card_rarity('giant spider'/'M12', 'Common').
card_artist('giant spider'/'M12', 'Randy Gallegos').
card_number('giant spider'/'M12', '177').
card_flavor_text('giant spider'/'M12', '\"The wild is always changing, but it does have a few constants.\"\n—Garruk Wildspeaker').
card_multiverse_id('giant spider'/'M12', '233232').

card_in_set('gideon jura', 'M12').
card_original_type('gideon jura'/'M12', 'Planeswalker — Gideon').
card_original_text('gideon jura'/'M12', '+2: During target opponent\'s next turn, creatures that player controls attack Gideon Jura if able.\n-2: Destroy target tapped creature.\n0: Until end of turn, Gideon Jura becomes a 6/6 Human Soldier creature that\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.').
card_image_name('gideon jura'/'M12', 'gideon jura').
card_uid('gideon jura'/'M12', 'M12:Gideon Jura:gideon jura').
card_rarity('gideon jura'/'M12', 'Mythic Rare').
card_artist('gideon jura'/'M12', 'Aleksi Briclot').
card_number('gideon jura'/'M12', '16').
card_multiverse_id('gideon jura'/'M12', '238329').

card_in_set('gideon\'s avenger', 'M12').
card_original_type('gideon\'s avenger'/'M12', 'Creature — Human Soldier').
card_original_text('gideon\'s avenger'/'M12', 'Whenever a creature an opponent controls becomes tapped, put a +1/+1 counter on Gideon\'s Avenger.').
card_first_print('gideon\'s avenger', 'M12').
card_image_name('gideon\'s avenger'/'M12', 'gideon\'s avenger').
card_uid('gideon\'s avenger'/'M12', 'M12:Gideon\'s Avenger:gideon\'s avenger').
card_rarity('gideon\'s avenger'/'M12', 'Rare').
card_artist('gideon\'s avenger'/'M12', 'Randy Gallegos').
card_number('gideon\'s avenger'/'M12', '17').
card_flavor_text('gideon\'s avenger'/'M12', '\"The good don\'t sit and wait for the evil to act. They prepare to defend. They prepare to punish.\"').
card_multiverse_id('gideon\'s avenger'/'M12', '220239').

card_in_set('gideon\'s lawkeeper', 'M12').
card_original_type('gideon\'s lawkeeper'/'M12', 'Creature — Human Soldier').
card_original_text('gideon\'s lawkeeper'/'M12', '{W}, {T}: Tap target creature.').
card_first_print('gideon\'s lawkeeper', 'M12').
card_image_name('gideon\'s lawkeeper'/'M12', 'gideon\'s lawkeeper').
card_uid('gideon\'s lawkeeper'/'M12', 'M12:Gideon\'s Lawkeeper:gideon\'s lawkeeper').
card_rarity('gideon\'s lawkeeper'/'M12', 'Common').
card_artist('gideon\'s lawkeeper'/'M12', 'Steve Prescott').
card_number('gideon\'s lawkeeper'/'M12', '18').
card_flavor_text('gideon\'s lawkeeper'/'M12', '\"The essence of a lawful society is swift deterrence.\"').
card_multiverse_id('gideon\'s lawkeeper'/'M12', '228122').

card_in_set('glacial fortress', 'M12').
card_original_type('glacial fortress'/'M12', 'Land').
card_original_text('glacial fortress'/'M12', 'Glacial Fortress enters the battlefield tapped unless you control a Plains or an Island.\n{T}: Add {W} or {U} to your mana pool.').
card_image_name('glacial fortress'/'M12', 'glacial fortress').
card_uid('glacial fortress'/'M12', 'M12:Glacial Fortress:glacial fortress').
card_rarity('glacial fortress'/'M12', 'Rare').
card_artist('glacial fortress'/'M12', 'Franz Vohwinkel').
card_number('glacial fortress'/'M12', '227').
card_multiverse_id('glacial fortress'/'M12', '221919').

card_in_set('gladecover scout', 'M12').
card_original_type('gladecover scout'/'M12', 'Creature — Elf Scout').
card_original_text('gladecover scout'/'M12', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_first_print('gladecover scout', 'M12').
card_image_name('gladecover scout'/'M12', 'gladecover scout').
card_uid('gladecover scout'/'M12', 'M12:Gladecover Scout:gladecover scout').
card_rarity('gladecover scout'/'M12', 'Common').
card_artist('gladecover scout'/'M12', 'Allen Williams').
card_number('gladecover scout'/'M12', '178').
card_flavor_text('gladecover scout'/'M12', '\"The forest is my cover and I hold it close. In such a tight embrace, there is no room for wickedness.\"').
card_multiverse_id('gladecover scout'/'M12', '220082').

card_in_set('goblin arsonist', 'M12').
card_original_type('goblin arsonist'/'M12', 'Creature — Goblin Shaman').
card_original_text('goblin arsonist'/'M12', 'When Goblin Arsonist dies, you may have it deal 1 damage to target creature or player.').
card_image_name('goblin arsonist'/'M12', 'goblin arsonist').
card_uid('goblin arsonist'/'M12', 'M12:Goblin Arsonist:goblin arsonist').
card_rarity('goblin arsonist'/'M12', 'Common').
card_artist('goblin arsonist'/'M12', 'Wayne Reynolds').
card_number('goblin arsonist'/'M12', '136').
card_flavor_text('goblin arsonist'/'M12', 'With great power comes great risk of getting yourself killed.').
card_multiverse_id('goblin arsonist'/'M12', '242485').

card_in_set('goblin bangchuckers', 'M12').
card_original_type('goblin bangchuckers'/'M12', 'Creature — Goblin Warrior').
card_original_text('goblin bangchuckers'/'M12', '{T}: Flip a coin. If you win the flip, Goblin Bangchuckers deals 2 damage to target creature or player. If you lose the flip, Goblin Bangchuckers deals 2 damage to itself.').
card_first_print('goblin bangchuckers', 'M12').
card_image_name('goblin bangchuckers'/'M12', 'goblin bangchuckers').
card_uid('goblin bangchuckers'/'M12', 'M12:Goblin Bangchuckers:goblin bangchuckers').
card_rarity('goblin bangchuckers'/'M12', 'Uncommon').
card_artist('goblin bangchuckers'/'M12', 'Wayne Reynolds').
card_number('goblin bangchuckers'/'M12', '137').
card_multiverse_id('goblin bangchuckers'/'M12', '229952').

card_in_set('goblin chieftain', 'M12').
card_original_type('goblin chieftain'/'M12', 'Creature — Goblin').
card_original_text('goblin chieftain'/'M12', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nOther Goblin creatures you control get +1/+1 and have haste.').
card_image_name('goblin chieftain'/'M12', 'goblin chieftain').
card_uid('goblin chieftain'/'M12', 'M12:Goblin Chieftain:goblin chieftain').
card_rarity('goblin chieftain'/'M12', 'Rare').
card_artist('goblin chieftain'/'M12', 'Sam Wood').
card_number('goblin chieftain'/'M12', '138').
card_flavor_text('goblin chieftain'/'M12', '\"It\'s time for the ‘Smash, Smash\' song!\"').
card_multiverse_id('goblin chieftain'/'M12', '221201').

card_in_set('goblin fireslinger', 'M12').
card_original_type('goblin fireslinger'/'M12', 'Creature — Goblin Warrior').
card_original_text('goblin fireslinger'/'M12', '{T}: Goblin Fireslinger deals 1 damage to target player.').
card_first_print('goblin fireslinger', 'M12').
card_image_name('goblin fireslinger'/'M12', 'goblin fireslinger').
card_uid('goblin fireslinger'/'M12', 'M12:Goblin Fireslinger:goblin fireslinger').
card_rarity('goblin fireslinger'/'M12', 'Common').
card_artist('goblin fireslinger'/'M12', 'Pete Venters').
card_number('goblin fireslinger'/'M12', '139').
card_flavor_text('goblin fireslinger'/'M12', 'A rock between your eyes hurts. A burning rock between your eyes ruins your whole day.').
card_multiverse_id('goblin fireslinger'/'M12', '228959').

card_in_set('goblin grenade', 'M12').
card_original_type('goblin grenade'/'M12', 'Sorcery').
card_original_text('goblin grenade'/'M12', 'As an additional cost to cast Goblin Grenade, sacrifice a Goblin.\nGoblin Grenade deals 5 damage to target creature or player.').
card_image_name('goblin grenade'/'M12', 'goblin grenade').
card_uid('goblin grenade'/'M12', 'M12:Goblin Grenade:goblin grenade').
card_rarity('goblin grenade'/'M12', 'Uncommon').
card_artist('goblin grenade'/'M12', 'Kev Walker').
card_number('goblin grenade'/'M12', '140').
card_flavor_text('goblin grenade'/'M12', 'Don\'t underestimate the aerodynamic qualities of the common goblin.').
card_multiverse_id('goblin grenade'/'M12', '220435').

card_in_set('goblin piker', 'M12').
card_original_type('goblin piker'/'M12', 'Creature — Goblin Warrior').
card_original_text('goblin piker'/'M12', '').
card_image_name('goblin piker'/'M12', 'goblin piker').
card_uid('goblin piker'/'M12', 'M12:Goblin Piker:goblin piker').
card_rarity('goblin piker'/'M12', 'Common').
card_artist('goblin piker'/'M12', 'DiTerlizzi').
card_number('goblin piker'/'M12', '141').
card_flavor_text('goblin piker'/'M12', 'Once he\'d worked out which end of the thing was sharp, he was promoted to guard duty.').
card_multiverse_id('goblin piker'/'M12', '221199').

card_in_set('goblin tunneler', 'M12').
card_original_type('goblin tunneler'/'M12', 'Creature — Goblin Rogue').
card_original_text('goblin tunneler'/'M12', '{T}: Target creature with power 2 or less is unblockable this turn.').
card_image_name('goblin tunneler'/'M12', 'goblin tunneler').
card_uid('goblin tunneler'/'M12', 'M12:Goblin Tunneler:goblin tunneler').
card_rarity('goblin tunneler'/'M12', 'Common').
card_artist('goblin tunneler'/'M12', 'Jesper Ejsing').
card_number('goblin tunneler'/'M12', '142').
card_flavor_text('goblin tunneler'/'M12', '\"The best way over the mountain is through.\"').
card_multiverse_id('goblin tunneler'/'M12', '245185').

card_in_set('goblin war paint', 'M12').
card_original_type('goblin war paint'/'M12', 'Enchantment — Aura').
card_original_text('goblin war paint'/'M12', 'Enchant creature\nEnchanted creature gets +2/+2 and has haste. (It can attack and {T} no matter when it came under its controller\'s control.)').
card_image_name('goblin war paint'/'M12', 'goblin war paint').
card_uid('goblin war paint'/'M12', 'M12:Goblin War Paint:goblin war paint').
card_rarity('goblin war paint'/'M12', 'Common').
card_artist('goblin war paint'/'M12', 'Austin Hsu').
card_number('goblin war paint'/'M12', '143').
card_flavor_text('goblin war paint'/'M12', '\"The secret to our war paint is hot peppers and stinkbugs. Lots of stinkbugs.\"\n—Skab, goblin secretkeeper').
card_multiverse_id('goblin war paint'/'M12', '236461').

card_in_set('gorehorn minotaurs', 'M12').
card_original_type('gorehorn minotaurs'/'M12', 'Creature — Minotaur Warrior').
card_original_text('gorehorn minotaurs'/'M12', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)').
card_first_print('gorehorn minotaurs', 'M12').
card_image_name('gorehorn minotaurs'/'M12', 'gorehorn minotaurs').
card_uid('gorehorn minotaurs'/'M12', 'M12:Gorehorn Minotaurs:gorehorn minotaurs').
card_rarity('gorehorn minotaurs'/'M12', 'Common').
card_artist('gorehorn minotaurs'/'M12', 'Wayne Reynolds').
card_number('gorehorn minotaurs'/'M12', '144').
card_flavor_text('gorehorn minotaurs'/'M12', 'Some of the eleven minotaur clans of Mirtiin are expert crafters and learned philosophers. Others just like to hit stuff.').
card_multiverse_id('gorehorn minotaurs'/'M12', '220294').

card_in_set('grand abolisher', 'M12').
card_original_type('grand abolisher'/'M12', 'Creature — Human Cleric').
card_original_text('grand abolisher'/'M12', 'During your turn, your opponents can\'t cast spells or activate abilities of artifacts, creatures, or enchantments.').
card_first_print('grand abolisher', 'M12').
card_image_name('grand abolisher'/'M12', 'grand abolisher').
card_uid('grand abolisher'/'M12', 'M12:Grand Abolisher:grand abolisher').
card_rarity('grand abolisher'/'M12', 'Rare').
card_artist('grand abolisher'/'M12', 'Eric Deschamps').
card_number('grand abolisher'/'M12', '19').
card_flavor_text('grand abolisher'/'M12', '\"Your superstitions and mumblings are useless chaff before my righteousness.\"').
card_multiverse_id('grand abolisher'/'M12', '221898').

card_in_set('grave titan', 'M12').
card_original_type('grave titan'/'M12', 'Creature — Giant').
card_original_text('grave titan'/'M12', 'Deathtouch\nWhenever Grave Titan enters the battlefield or attacks, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_image_name('grave titan'/'M12', 'grave titan').
card_uid('grave titan'/'M12', 'M12:Grave Titan:grave titan').
card_rarity('grave titan'/'M12', 'Mythic Rare').
card_artist('grave titan'/'M12', 'Nils Hamm').
card_number('grave titan'/'M12', '98').
card_flavor_text('grave titan'/'M12', 'Death in form and function.').
card_multiverse_id('grave titan'/'M12', '241830').

card_in_set('gravedigger', 'M12').
card_original_type('gravedigger'/'M12', 'Creature — Zombie').
card_original_text('gravedigger'/'M12', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_image_name('gravedigger'/'M12', 'gravedigger').
card_uid('gravedigger'/'M12', 'M12:Gravedigger:gravedigger').
card_rarity('gravedigger'/'M12', 'Common').
card_artist('gravedigger'/'M12', 'Dermot Power').
card_number('gravedigger'/'M12', '99').
card_flavor_text('gravedigger'/'M12', 'His grim ritual ushers the dead to their new life.').
card_multiverse_id('gravedigger'/'M12', '221526').

card_in_set('greater basilisk', 'M12').
card_original_type('greater basilisk'/'M12', 'Creature — Basilisk').
card_original_text('greater basilisk'/'M12', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('greater basilisk'/'M12', 'greater basilisk').
card_uid('greater basilisk'/'M12', 'M12:Greater Basilisk:greater basilisk').
card_rarity('greater basilisk'/'M12', 'Common').
card_artist('greater basilisk'/'M12', 'James Ryman').
card_number('greater basilisk'/'M12', '179').
card_flavor_text('greater basilisk'/'M12', 'Bone, stone . . . both taste the same to a hungry basilisk.').
card_multiverse_id('greater basilisk'/'M12', '233233').

card_in_set('greatsword', 'M12').
card_original_type('greatsword'/'M12', 'Artifact — Equipment').
card_original_text('greatsword'/'M12', 'Equipped creature gets +3/+0.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('greatsword', 'M12').
card_image_name('greatsword'/'M12', 'greatsword').
card_uid('greatsword'/'M12', 'M12:Greatsword:greatsword').
card_rarity('greatsword'/'M12', 'Uncommon').
card_artist('greatsword'/'M12', 'Nic Klein').
card_number('greatsword'/'M12', '209').
card_flavor_text('greatsword'/'M12', 'The only blow that matters is the killing blow.').
card_multiverse_id('greatsword'/'M12', '220296').

card_in_set('griffin rider', 'M12').
card_original_type('griffin rider'/'M12', 'Creature — Human Knight').
card_original_text('griffin rider'/'M12', 'As long as you control a Griffin creature, Griffin Rider gets +3/+3 and has flying.').
card_first_print('griffin rider', 'M12').
card_image_name('griffin rider'/'M12', 'griffin rider').
card_uid('griffin rider'/'M12', 'M12:Griffin Rider:griffin rider').
card_rarity('griffin rider'/'M12', 'Common').
card_artist('griffin rider'/'M12', 'Steve Prescott').
card_number('griffin rider'/'M12', '20').
card_flavor_text('griffin rider'/'M12', 'After fighting alongside the griffin riders, older knights of Thune learned to admire a mount that could rip an enemy to shreds in seconds.').
card_multiverse_id('griffin rider'/'M12', '220282').

card_in_set('griffin sentinel', 'M12').
card_original_type('griffin sentinel'/'M12', 'Creature — Griffin').
card_original_text('griffin sentinel'/'M12', 'Flying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('griffin sentinel'/'M12', 'griffin sentinel').
card_uid('griffin sentinel'/'M12', 'M12:Griffin Sentinel:griffin sentinel').
card_rarity('griffin sentinel'/'M12', 'Common').
card_artist('griffin sentinel'/'M12', 'Warren Mahy').
card_number('griffin sentinel'/'M12', '21').
card_flavor_text('griffin sentinel'/'M12', 'Once a griffin sentinel adopts a territory as its own, only death can force it to betray its post.').
card_multiverse_id('griffin sentinel'/'M12', '222650').

card_in_set('grim lavamancer', 'M12').
card_original_type('grim lavamancer'/'M12', 'Creature — Human Wizard').
card_original_text('grim lavamancer'/'M12', '{R}, {T}, Exile two cards from your graveyard: Grim Lavamancer deals 2 damage to target creature or player.').
card_image_name('grim lavamancer'/'M12', 'grim lavamancer').
card_uid('grim lavamancer'/'M12', 'M12:Grim Lavamancer:grim lavamancer').
card_rarity('grim lavamancer'/'M12', 'Rare').
card_artist('grim lavamancer'/'M12', 'Michael Sutfin').
card_number('grim lavamancer'/'M12', '145').
card_flavor_text('grim lavamancer'/'M12', '\"Fools dig for water, corpses, or gold. The earth\'s real treasure is far deeper.\"').
card_multiverse_id('grim lavamancer'/'M12', '234428').

card_in_set('guardians\' pledge', 'M12').
card_original_type('guardians\' pledge'/'M12', 'Instant').
card_original_text('guardians\' pledge'/'M12', 'White creatures you control get +2/+2 until end of turn.').
card_first_print('guardians\' pledge', 'M12').
card_image_name('guardians\' pledge'/'M12', 'guardians\' pledge').
card_uid('guardians\' pledge'/'M12', 'M12:Guardians\' Pledge:guardians\' pledge').
card_rarity('guardians\' pledge'/'M12', 'Common').
card_artist('guardians\' pledge'/'M12', 'Christopher Moeller').
card_number('guardians\' pledge'/'M12', '22').
card_flavor_text('guardians\' pledge'/'M12', 'A dozen fighting for each other are worth a thousand fighting for themselves.').
card_multiverse_id('guardians\' pledge'/'M12', '220095').

card_in_set('harbor serpent', 'M12').
card_original_type('harbor serpent'/'M12', 'Creature — Serpent').
card_original_text('harbor serpent'/'M12', 'Islandwalk (This creature is unblockable as long as defending player controls an Island.)\nHarbor Serpent can\'t attack unless there are five or more Islands on the battlefield.').
card_image_name('harbor serpent'/'M12', 'harbor serpent').
card_uid('harbor serpent'/'M12', 'M12:Harbor Serpent:harbor serpent').
card_rarity('harbor serpent'/'M12', 'Common').
card_artist('harbor serpent'/'M12', 'Daarken').
card_number('harbor serpent'/'M12', '56').
card_flavor_text('harbor serpent'/'M12', 'Like most giant monsters villagers worship as gods, it proves a fickle master.').
card_multiverse_id('harbor serpent'/'M12', '230756').

card_in_set('hideous visage', 'M12').
card_original_type('hideous visage'/'M12', 'Sorcery').
card_original_text('hideous visage'/'M12', 'Creatures you control gain intimidate until end of turn. (Each of those creatures can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_first_print('hideous visage', 'M12').
card_image_name('hideous visage'/'M12', 'hideous visage').
card_uid('hideous visage'/'M12', 'M12:Hideous Visage:hideous visage').
card_rarity('hideous visage'/'M12', 'Common').
card_artist('hideous visage'/'M12', 'Nils Hamm').
card_number('hideous visage'/'M12', '100').
card_flavor_text('hideous visage'/'M12', '\"Seeing you cower before me is its own reward.\"').
card_multiverse_id('hideous visage'/'M12', '220304').

card_in_set('honor of the pure', 'M12').
card_original_type('honor of the pure'/'M12', 'Enchantment').
card_original_text('honor of the pure'/'M12', 'White creatures you control get +1/+1.').
card_image_name('honor of the pure'/'M12', 'honor of the pure').
card_uid('honor of the pure'/'M12', 'M12:Honor of the Pure:honor of the pure').
card_rarity('honor of the pure'/'M12', 'Rare').
card_artist('honor of the pure'/'M12', 'Greg Staples').
card_number('honor of the pure'/'M12', '23').
card_flavor_text('honor of the pure'/'M12', 'Together the soldiers were like a golden blade, cutting down their enemies and scarring the darkness.').
card_multiverse_id('honor of the pure'/'M12', '233237').

card_in_set('hunter\'s insight', 'M12').
card_original_type('hunter\'s insight'/'M12', 'Instant').
card_original_text('hunter\'s insight'/'M12', 'Choose target creature you control. Whenever that creature deals combat damage to a player or planeswalker this turn, draw that many cards.').
card_first_print('hunter\'s insight', 'M12').
card_image_name('hunter\'s insight'/'M12', 'hunter\'s insight').
card_uid('hunter\'s insight'/'M12', 'M12:Hunter\'s Insight:hunter\'s insight').
card_rarity('hunter\'s insight'/'M12', 'Uncommon').
card_artist('hunter\'s insight'/'M12', 'Terese Nielsen').
card_number('hunter\'s insight'/'M12', '180').
card_flavor_text('hunter\'s insight'/'M12', '\"The predator knows the true way of the world. Life is conflict. Only the strong survive.\"\n—Garruk Wildspeaker').
card_multiverse_id('hunter\'s insight'/'M12', '220291').

card_in_set('ice cage', 'M12').
card_original_type('ice cage'/'M12', 'Enchantment — Aura').
card_original_text('ice cage'/'M12', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nWhen enchanted creature becomes the target of a spell or ability, destroy Ice Cage.').
card_image_name('ice cage'/'M12', 'ice cage').
card_uid('ice cage'/'M12', 'M12:Ice Cage:ice cage').
card_rarity('ice cage'/'M12', 'Common').
card_artist('ice cage'/'M12', 'Mike Bierek').
card_number('ice cage'/'M12', '57').
card_multiverse_id('ice cage'/'M12', '245283').

card_in_set('incinerate', 'M12').
card_original_type('incinerate'/'M12', 'Instant').
card_original_text('incinerate'/'M12', 'Incinerate deals 3 damage to target creature or player. A creature dealt damage this way can\'t be regenerated this turn.').
card_image_name('incinerate'/'M12', 'incinerate').
card_uid('incinerate'/'M12', 'M12:Incinerate:incinerate').
card_rarity('incinerate'/'M12', 'Common').
card_artist('incinerate'/'M12', 'Zoltan Boros & Gabor Szikszai').
card_number('incinerate'/'M12', '146').
card_flavor_text('incinerate'/'M12', '\"Spontaneous combustion is a myth. If you burst into flame, someone wanted you to.\"\n—Chandra Nalaar').
card_multiverse_id('incinerate'/'M12', '234075').

card_in_set('inferno titan', 'M12').
card_original_type('inferno titan'/'M12', 'Creature — Giant').
card_original_text('inferno titan'/'M12', '{R}: Inferno Titan gets +1/+0 until end of turn.\nWhenever Inferno Titan enters the battlefield or attacks, it deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_image_name('inferno titan'/'M12', 'inferno titan').
card_uid('inferno titan'/'M12', 'M12:Inferno Titan:inferno titan').
card_rarity('inferno titan'/'M12', 'Mythic Rare').
card_artist('inferno titan'/'M12', 'Kev Walker').
card_number('inferno titan'/'M12', '147').
card_multiverse_id('inferno titan'/'M12', '235192').

card_in_set('island', 'M12').
card_original_type('island'/'M12', 'Basic Land — Island').
card_original_text('island'/'M12', 'U').
card_image_name('island'/'M12', 'island1').
card_uid('island'/'M12', 'M12:Island:island1').
card_rarity('island'/'M12', 'Basic Land').
card_artist('island'/'M12', 'Rob Alexander').
card_number('island'/'M12', '234').
card_multiverse_id('island'/'M12', '244325').

card_in_set('island', 'M12').
card_original_type('island'/'M12', 'Basic Land — Island').
card_original_text('island'/'M12', 'U').
card_image_name('island'/'M12', 'island2').
card_uid('island'/'M12', 'M12:Island:island2').
card_rarity('island'/'M12', 'Basic Land').
card_artist('island'/'M12', 'Cliff Childs').
card_number('island'/'M12', '235').
card_multiverse_id('island'/'M12', '244323').

card_in_set('island', 'M12').
card_original_type('island'/'M12', 'Basic Land — Island').
card_original_text('island'/'M12', 'U').
card_image_name('island'/'M12', 'island3').
card_uid('island'/'M12', 'M12:Island:island3').
card_rarity('island'/'M12', 'Basic Land').
card_artist('island'/'M12', 'Michael Komarck').
card_number('island'/'M12', '236').
card_multiverse_id('island'/'M12', '244324').

card_in_set('island', 'M12').
card_original_type('island'/'M12', 'Basic Land — Island').
card_original_text('island'/'M12', 'U').
card_image_name('island'/'M12', 'island4').
card_uid('island'/'M12', 'M12:Island:island4').
card_rarity('island'/'M12', 'Basic Land').
card_artist('island'/'M12', 'Peter Mohrbacher').
card_number('island'/'M12', '237').
card_multiverse_id('island'/'M12', '244326').

card_in_set('jace\'s archivist', 'M12').
card_original_type('jace\'s archivist'/'M12', 'Creature — Vedalken Wizard').
card_original_text('jace\'s archivist'/'M12', '{U}, {T}: Each player discards his or her hand, then draws cards equal to the greatest number of cards a player discarded this way.').
card_first_print('jace\'s archivist', 'M12').
card_image_name('jace\'s archivist'/'M12', 'jace\'s archivist').
card_uid('jace\'s archivist'/'M12', 'M12:Jace\'s Archivist:jace\'s archivist').
card_rarity('jace\'s archivist'/'M12', 'Rare').
card_artist('jace\'s archivist'/'M12', 'James Ryman').
card_number('jace\'s archivist'/'M12', '59').
card_flavor_text('jace\'s archivist'/'M12', '\"This is a sacred duty. Master Beleren\'s trust has bought an eternity of my loyalty.\"').
card_multiverse_id('jace\'s archivist'/'M12', '220244').

card_in_set('jace\'s erasure', 'M12').
card_original_type('jace\'s erasure'/'M12', 'Enchantment').
card_original_text('jace\'s erasure'/'M12', 'Whenever you draw a card, you may have target player put the top card of his or her library into his or her graveyard.').
card_image_name('jace\'s erasure'/'M12', 'jace\'s erasure').
card_uid('jace\'s erasure'/'M12', 'M12:Jace\'s Erasure:jace\'s erasure').
card_rarity('jace\'s erasure'/'M12', 'Common').
card_artist('jace\'s erasure'/'M12', 'Jason Chan').
card_number('jace\'s erasure'/'M12', '60').
card_flavor_text('jace\'s erasure'/'M12', '\"You should try to clear your mind of idle thoughts. And if you can\'t, I will.\"').
card_multiverse_id('jace\'s erasure'/'M12', '227227').

card_in_set('jace, memory adept', 'M12').
card_original_type('jace, memory adept'/'M12', 'Planeswalker — Jace').
card_original_text('jace, memory adept'/'M12', '+1: Draw a card. Target player puts the top card of his or her library into his or her graveyard.\n0: Target player puts the top ten cards of his or her library into his or her graveyard.\n-7: Any number of target players each draw twenty cards.').
card_image_name('jace, memory adept'/'M12', 'jace, memory adept').
card_uid('jace, memory adept'/'M12', 'M12:Jace, Memory Adept:jace, memory adept').
card_rarity('jace, memory adept'/'M12', 'Mythic Rare').
card_artist('jace, memory adept'/'M12', 'D. Alexander Gregory').
card_number('jace, memory adept'/'M12', '58').
card_multiverse_id('jace, memory adept'/'M12', '238263').

card_in_set('jade mage', 'M12').
card_original_type('jade mage'/'M12', 'Creature — Human Shaman').
card_original_text('jade mage'/'M12', '{2}{G}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_first_print('jade mage', 'M12').
card_image_name('jade mage'/'M12', 'jade mage').
card_uid('jade mage'/'M12', 'M12:Jade Mage:jade mage').
card_rarity('jade mage'/'M12', 'Uncommon').
card_artist('jade mage'/'M12', 'Izzy').
card_number('jade mage'/'M12', '181').
card_flavor_text('jade mage'/'M12', '\"We are one with the wild things. Life blooms from our fingertips and nature responds to our summons.\"\n—Jade creed').
card_multiverse_id('jade mage'/'M12', '220279').

card_in_set('kite shield', 'M12').
card_original_type('kite shield'/'M12', 'Artifact — Equipment').
card_original_text('kite shield'/'M12', 'Equipped creature gets +0/+3.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('kite shield', 'M12').
card_image_name('kite shield'/'M12', 'kite shield').
card_uid('kite shield'/'M12', 'M12:Kite Shield:kite shield').
card_rarity('kite shield'/'M12', 'Uncommon').
card_artist('kite shield'/'M12', 'Jim Pavelec').
card_number('kite shield'/'M12', '210').
card_flavor_text('kite shield'/'M12', '\"To my sword I owe my glory, but to my shield I owe my life.\"\n—Sarlena, paladin of the Northern Verge').
card_multiverse_id('kite shield'/'M12', '244250').

card_in_set('kraken\'s eye', 'M12').
card_original_type('kraken\'s eye'/'M12', 'Artifact').
card_original_text('kraken\'s eye'/'M12', 'Whenever a player casts a blue spell, you may gain 1 life.').
card_image_name('kraken\'s eye'/'M12', 'kraken\'s eye').
card_uid('kraken\'s eye'/'M12', 'M12:Kraken\'s Eye:kraken\'s eye').
card_rarity('kraken\'s eye'/'M12', 'Uncommon').
card_artist('kraken\'s eye'/'M12', 'Alan Pollack').
card_number('kraken\'s eye'/'M12', '211').
card_flavor_text('kraken\'s eye'/'M12', 'It\'s seen the wonders of entire worlds trapped in the depths of a bottomless ocean.').
card_multiverse_id('kraken\'s eye'/'M12', '221521').

card_in_set('lava axe', 'M12').
card_original_type('lava axe'/'M12', 'Sorcery').
card_original_text('lava axe'/'M12', 'Lava Axe deals 5 damage to target player.').
card_image_name('lava axe'/'M12', 'lava axe').
card_uid('lava axe'/'M12', 'M12:Lava Axe:lava axe').
card_rarity('lava axe'/'M12', 'Common').
card_artist('lava axe'/'M12', 'Brian Snõddy').
card_number('lava axe'/'M12', '148').
card_flavor_text('lava axe'/'M12', 'It can chop down a redwood in a single stroke. Your sternum isn\'t going to fare any better.').
card_multiverse_id('lava axe'/'M12', '221891').

card_in_set('levitation', 'M12').
card_original_type('levitation'/'M12', 'Enchantment').
card_original_text('levitation'/'M12', 'Creatures you control have flying.').
card_image_name('levitation'/'M12', 'levitation').
card_uid('levitation'/'M12', 'M12:Levitation:levitation').
card_rarity('levitation'/'M12', 'Uncommon').
card_artist('levitation'/'M12', 'Jim Murray').
card_number('levitation'/'M12', '61').
card_flavor_text('levitation'/'M12', '\"Why fight over scraps of land when there\'s so much sky waiting to be claimed?\"').
card_multiverse_id('levitation'/'M12', '233728').

card_in_set('lifelink', 'M12').
card_original_type('lifelink'/'M12', 'Enchantment — Aura').
card_original_text('lifelink'/'M12', 'Enchant creature\nEnchanted creature has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_image_name('lifelink'/'M12', 'lifelink').
card_uid('lifelink'/'M12', 'M12:Lifelink:lifelink').
card_rarity('lifelink'/'M12', 'Common').
card_artist('lifelink'/'M12', 'Terese Nielsen').
card_number('lifelink'/'M12', '24').
card_flavor_text('lifelink'/'M12', 'The spoils of war are not measured only in gold and silver.').
card_multiverse_id('lifelink'/'M12', '220182').

card_in_set('lightning elemental', 'M12').
card_original_type('lightning elemental'/'M12', 'Creature — Elemental').
card_original_text('lightning elemental'/'M12', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('lightning elemental'/'M12', 'lightning elemental').
card_uid('lightning elemental'/'M12', 'M12:Lightning Elemental:lightning elemental').
card_rarity('lightning elemental'/'M12', 'Common').
card_artist('lightning elemental'/'M12', 'Kev Walker').
card_number('lightning elemental'/'M12', '149').
card_flavor_text('lightning elemental'/'M12', '\"Oh, it\'s easy enough to kill, but I\'m not going anywhere near it.\"\n—Norin the Wary').
card_multiverse_id('lightning elemental'/'M12', '220240').

card_in_set('llanowar elves', 'M12').
card_original_type('llanowar elves'/'M12', 'Creature — Elf Druid').
card_original_text('llanowar elves'/'M12', '{T}: Add {G} to your mana pool.').
card_image_name('llanowar elves'/'M12', 'llanowar elves').
card_uid('llanowar elves'/'M12', 'M12:Llanowar Elves:llanowar elves').
card_rarity('llanowar elves'/'M12', 'Common').
card_artist('llanowar elves'/'M12', 'Kev Walker').
card_number('llanowar elves'/'M12', '182').
card_flavor_text('llanowar elves'/'M12', 'One bone broken for every twig snapped underfoot.\n—Llanowar penalty for trespassing').
card_multiverse_id('llanowar elves'/'M12', '221892').

card_in_set('lord of the unreal', 'M12').
card_original_type('lord of the unreal'/'M12', 'Creature — Human Wizard').
card_original_text('lord of the unreal'/'M12', 'Illusion creatures you control get +1/+1 and have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_first_print('lord of the unreal', 'M12').
card_image_name('lord of the unreal'/'M12', 'lord of the unreal').
card_uid('lord of the unreal'/'M12', 'M12:Lord of the Unreal:lord of the unreal').
card_rarity('lord of the unreal'/'M12', 'Rare').
card_artist('lord of the unreal'/'M12', 'Jason Chan').
card_number('lord of the unreal'/'M12', '62').
card_flavor_text('lord of the unreal'/'M12', '\"The dream does not end until I say so.\"').
card_multiverse_id('lord of the unreal'/'M12', '220131').

card_in_set('lure', 'M12').
card_original_type('lure'/'M12', 'Enchantment — Aura').
card_original_text('lure'/'M12', 'Enchant creature\nAll creatures able to block enchanted creature do so.').
card_image_name('lure'/'M12', 'lure').
card_uid('lure'/'M12', 'M12:Lure:lure').
card_rarity('lure'/'M12', 'Uncommon').
card_artist('lure'/'M12', 'D. Alexander Gregory').
card_number('lure'/'M12', '183').
card_flavor_text('lure'/'M12', 'It makes the insignificant conspicuous, the repulsive alluring, and the deadly irresistible.').
card_multiverse_id('lure'/'M12', '226905').

card_in_set('lurking crocodile', 'M12').
card_original_type('lurking crocodile'/'M12', 'Creature — Crocodile').
card_original_text('lurking crocodile'/'M12', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nIslandwalk (This creature is unblockable as long as defending player controls an Island.)').
card_first_print('lurking crocodile', 'M12').
card_image_name('lurking crocodile'/'M12', 'lurking crocodile').
card_uid('lurking crocodile'/'M12', 'M12:Lurking Crocodile:lurking crocodile').
card_rarity('lurking crocodile'/'M12', 'Common').
card_artist('lurking crocodile'/'M12', 'Donato Giancola').
card_number('lurking crocodile'/'M12', '184').
card_multiverse_id('lurking crocodile'/'M12', '220199').

card_in_set('mana leak', 'M12').
card_original_type('mana leak'/'M12', 'Instant').
card_original_text('mana leak'/'M12', 'Counter target spell unless its controller pays {3}.').
card_image_name('mana leak'/'M12', 'mana leak').
card_uid('mana leak'/'M12', 'M12:Mana Leak:mana leak').
card_rarity('mana leak'/'M12', 'Common').
card_artist('mana leak'/'M12', 'Howard Lyon').
card_number('mana leak'/'M12', '63').
card_flavor_text('mana leak'/'M12', 'The fatal flaw in every plan is the assumption that you know more than your enemy.').
card_multiverse_id('mana leak'/'M12', '241831').

card_in_set('manabarbs', 'M12').
card_original_type('manabarbs'/'M12', 'Enchantment').
card_original_text('manabarbs'/'M12', 'Whenever a player taps a land for mana, Manabarbs deals 1 damage to that player.').
card_image_name('manabarbs'/'M12', 'manabarbs').
card_uid('manabarbs'/'M12', 'M12:Manabarbs:manabarbs').
card_rarity('manabarbs'/'M12', 'Rare').
card_artist('manabarbs'/'M12', 'Jeff Miracola').
card_number('manabarbs'/'M12', '150').
card_flavor_text('manabarbs'/'M12', '\"I don\'t know why people say a double-edged sword is bad. It\'s a sword. With two edges.\"\n—Kamahl, pit fighter').
card_multiverse_id('manabarbs'/'M12', '235193').

card_in_set('manalith', 'M12').
card_original_type('manalith'/'M12', 'Artifact').
card_original_text('manalith'/'M12', '{T}: Add one mana of any color to your mana pool.').
card_first_print('manalith', 'M12').
card_image_name('manalith'/'M12', 'manalith').
card_uid('manalith'/'M12', 'M12:Manalith:manalith').
card_rarity('manalith'/'M12', 'Common').
card_artist('manalith'/'M12', 'Charles Urbach').
card_number('manalith'/'M12', '212').
card_flavor_text('manalith'/'M12', 'Planeswalkers seek out great monuments throughout the Multiverse, knowing that their builders were unknowingly drawn by the convergence of mana in the area.').
card_multiverse_id('manalith'/'M12', '220162').

card_in_set('manic vandal', 'M12').
card_original_type('manic vandal'/'M12', 'Creature — Human Warrior').
card_original_text('manic vandal'/'M12', 'When Manic Vandal enters the battlefield, destroy target artifact.').
card_image_name('manic vandal'/'M12', 'manic vandal').
card_uid('manic vandal'/'M12', 'M12:Manic Vandal:manic vandal').
card_rarity('manic vandal'/'M12', 'Common').
card_artist('manic vandal'/'M12', 'Christopher Moeller').
card_number('manic vandal'/'M12', '151').
card_flavor_text('manic vandal'/'M12', 'It\'s fun. He doesn\'t need another reason.').
card_multiverse_id('manic vandal'/'M12', '230764').

card_in_set('master thief', 'M12').
card_original_type('master thief'/'M12', 'Creature — Human Rogue').
card_original_text('master thief'/'M12', 'When Master Thief enters the battlefield, gain control of target artifact for as long as you control Master Thief.').
card_first_print('master thief', 'M12').
card_image_name('master thief'/'M12', 'master thief').
card_uid('master thief'/'M12', 'M12:Master Thief:master thief').
card_rarity('master thief'/'M12', 'Uncommon').
card_artist('master thief'/'M12', 'Christopher Moeller').
card_number('master thief'/'M12', '64').
card_flavor_text('master thief'/'M12', 'Possession is ten-tenths of the law.').
card_multiverse_id('master thief'/'M12', '228119').

card_in_set('merfolk looter', 'M12').
card_original_type('merfolk looter'/'M12', 'Creature — Merfolk Rogue').
card_original_text('merfolk looter'/'M12', '{T}: Draw a card, then discard a card.').
card_image_name('merfolk looter'/'M12', 'merfolk looter').
card_uid('merfolk looter'/'M12', 'M12:Merfolk Looter:merfolk looter').
card_rarity('merfolk looter'/'M12', 'Common').
card_artist('merfolk looter'/'M12', 'Austin Hsu').
card_number('merfolk looter'/'M12', '65').
card_flavor_text('merfolk looter'/'M12', 'Merfolk don\'t always know what they\'re looking for, but they\'re certain once they find it.').
card_multiverse_id('merfolk looter'/'M12', '220179').

card_in_set('merfolk mesmerist', 'M12').
card_original_type('merfolk mesmerist'/'M12', 'Creature — Merfolk Wizard').
card_original_text('merfolk mesmerist'/'M12', '{U}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_image_name('merfolk mesmerist'/'M12', 'merfolk mesmerist').
card_uid('merfolk mesmerist'/'M12', 'M12:Merfolk Mesmerist:merfolk mesmerist').
card_rarity('merfolk mesmerist'/'M12', 'Common').
card_artist('merfolk mesmerist'/'M12', 'Jana Schirmer & Johannes Voss').
card_number('merfolk mesmerist'/'M12', '66').
card_flavor_text('merfolk mesmerist'/'M12', '\"Don\'t you think if I saw a half-woman, half-fish with three glowing eyes, I\'d have remembered?\"\n—Drugo, town guard').
card_multiverse_id('merfolk mesmerist'/'M12', '220106').

card_in_set('mesa enchantress', 'M12').
card_original_type('mesa enchantress'/'M12', 'Creature — Human Druid').
card_original_text('mesa enchantress'/'M12', 'Whenever you cast an enchantment spell, you may draw a card.').
card_image_name('mesa enchantress'/'M12', 'mesa enchantress').
card_uid('mesa enchantress'/'M12', 'M12:Mesa Enchantress:mesa enchantress').
card_rarity('mesa enchantress'/'M12', 'Rare').
card_artist('mesa enchantress'/'M12', 'Randy Gallegos').
card_number('mesa enchantress'/'M12', '25').
card_flavor_text('mesa enchantress'/'M12', '\"Scholars seek to understand the way the world is. I wish to shape the world into what it should be.\"').
card_multiverse_id('mesa enchantress'/'M12', '220116').

card_in_set('mighty leap', 'M12').
card_original_type('mighty leap'/'M12', 'Instant').
card_original_text('mighty leap'/'M12', 'Target creature gets +2/+2 and gains flying until end of turn.').
card_image_name('mighty leap'/'M12', 'mighty leap').
card_uid('mighty leap'/'M12', 'M12:Mighty Leap:mighty leap').
card_rarity('mighty leap'/'M12', 'Common').
card_artist('mighty leap'/'M12', 'rk post').
card_number('mighty leap'/'M12', '26').
card_flavor_text('mighty leap'/'M12', '\"The southern fortress taken by invaders? Heh, sure . . . when elephants fly.\"\n—Brezard Skeinbow, captain of the guard').
card_multiverse_id('mighty leap'/'M12', '241989').

card_in_set('mind control', 'M12').
card_original_type('mind control'/'M12', 'Enchantment — Aura').
card_original_text('mind control'/'M12', 'Enchant creature\nYou control enchanted creature.').
card_image_name('mind control'/'M12', 'mind control').
card_uid('mind control'/'M12', 'M12:Mind Control:mind control').
card_rarity('mind control'/'M12', 'Uncommon').
card_artist('mind control'/'M12', 'Ryan Pancoast').
card_number('mind control'/'M12', '67').
card_flavor_text('mind control'/'M12', '\"Every primitive mind has a motive. Easy to find, and eminently exploitable.\"\n—Noyan Dar, Tazeem lullmage').
card_multiverse_id('mind control'/'M12', '238572').

card_in_set('mind rot', 'M12').
card_original_type('mind rot'/'M12', 'Sorcery').
card_original_text('mind rot'/'M12', 'Target player discards two cards.').
card_image_name('mind rot'/'M12', 'mind rot').
card_uid('mind rot'/'M12', 'M12:Mind Rot:mind rot').
card_rarity('mind rot'/'M12', 'Common').
card_artist('mind rot'/'M12', 'Steve Luke').
card_number('mind rot'/'M12', '101').
card_flavor_text('mind rot'/'M12', 'Not every thought is a good one.').
card_multiverse_id('mind rot'/'M12', '234561').

card_in_set('mind unbound', 'M12').
card_original_type('mind unbound'/'M12', 'Enchantment').
card_original_text('mind unbound'/'M12', 'At the beginning of your upkeep, put a lore counter on Mind Unbound, then draw a card for each lore counter on Mind Unbound.').
card_first_print('mind unbound', 'M12').
card_image_name('mind unbound'/'M12', 'mind unbound').
card_uid('mind unbound'/'M12', 'M12:Mind Unbound:mind unbound').
card_rarity('mind unbound'/'M12', 'Rare').
card_artist('mind unbound'/'M12', 'Jason Felix').
card_number('mind unbound'/'M12', '68').
card_flavor_text('mind unbound'/'M12', 'The creations of the hand are confined by reality. The creations of the mind know no such limits.').
card_multiverse_id('mind unbound'/'M12', '236459').

card_in_set('monomania', 'M12').
card_original_type('monomania'/'M12', 'Sorcery').
card_original_text('monomania'/'M12', 'Target player chooses a card in his or her hand and discards the rest.').
card_first_print('monomania', 'M12').
card_image_name('monomania'/'M12', 'monomania').
card_uid('monomania'/'M12', 'M12:Monomania:monomania').
card_rarity('monomania'/'M12', 'Rare').
card_artist('monomania'/'M12', 'James Ryman').
card_number('monomania'/'M12', '102').
card_flavor_text('monomania'/'M12', '\"What do vermin cling to when all hope is lost?\"\n—Sorin Markov').
card_multiverse_id('monomania'/'M12', '220143').

card_in_set('mountain', 'M12').
card_original_type('mountain'/'M12', 'Basic Land — Mountain').
card_original_text('mountain'/'M12', 'R').
card_image_name('mountain'/'M12', 'mountain1').
card_uid('mountain'/'M12', 'M12:Mountain:mountain1').
card_rarity('mountain'/'M12', 'Basic Land').
card_artist('mountain'/'M12', 'Cliff Childs').
card_number('mountain'/'M12', '242').
card_multiverse_id('mountain'/'M12', '244328').

card_in_set('mountain', 'M12').
card_original_type('mountain'/'M12', 'Basic Land — Mountain').
card_original_text('mountain'/'M12', 'R').
card_image_name('mountain'/'M12', 'mountain2').
card_uid('mountain'/'M12', 'M12:Mountain:mountain2').
card_rarity('mountain'/'M12', 'Basic Land').
card_artist('mountain'/'M12', 'Karl Kopinski').
card_number('mountain'/'M12', '243').
card_multiverse_id('mountain'/'M12', '244330').

card_in_set('mountain', 'M12').
card_original_type('mountain'/'M12', 'Basic Land — Mountain').
card_original_text('mountain'/'M12', 'R').
card_image_name('mountain'/'M12', 'mountain3').
card_uid('mountain'/'M12', 'M12:Mountain:mountain3').
card_rarity('mountain'/'M12', 'Basic Land').
card_artist('mountain'/'M12', 'Robh Ruppel').
card_number('mountain'/'M12', '244').
card_multiverse_id('mountain'/'M12', '244329').

card_in_set('mountain', 'M12').
card_original_type('mountain'/'M12', 'Basic Land — Mountain').
card_original_text('mountain'/'M12', 'R').
card_image_name('mountain'/'M12', 'mountain4').
card_uid('mountain'/'M12', 'M12:Mountain:mountain4').
card_rarity('mountain'/'M12', 'Basic Land').
card_artist('mountain'/'M12', 'Sam Wood').
card_number('mountain'/'M12', '245').
card_multiverse_id('mountain'/'M12', '244327').

card_in_set('naturalize', 'M12').
card_original_type('naturalize'/'M12', 'Instant').
card_original_text('naturalize'/'M12', 'Destroy target artifact or enchantment.').
card_image_name('naturalize'/'M12', 'naturalize').
card_uid('naturalize'/'M12', 'M12:Naturalize:naturalize').
card_rarity('naturalize'/'M12', 'Common').
card_artist('naturalize'/'M12', 'Tim Hildebrandt').
card_number('naturalize'/'M12', '185').
card_flavor_text('naturalize'/'M12', '\"When your cities and trinkets crumble, only nature will remain.\"\n—Garruk Wildspeaker').
card_multiverse_id('naturalize'/'M12', '221894').

card_in_set('negate', 'M12').
card_original_type('negate'/'M12', 'Instant').
card_original_text('negate'/'M12', 'Counter target noncreature spell.').
card_image_name('negate'/'M12', 'negate').
card_uid('negate'/'M12', 'M12:Negate:negate').
card_rarity('negate'/'M12', 'Common').
card_artist('negate'/'M12', 'Jeremy Jarvis').
card_number('negate'/'M12', '69').
card_flavor_text('negate'/'M12', 'Masters of the arcane savor a delicious irony. Their study of deep and complex arcana leads to such a simple end: the ability to say merely yes or no.').
card_multiverse_id('negate'/'M12', '230757').

card_in_set('oblivion ring', 'M12').
card_original_type('oblivion ring'/'M12', 'Enchantment').
card_original_text('oblivion ring'/'M12', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('oblivion ring'/'M12', 'oblivion ring').
card_uid('oblivion ring'/'M12', 'M12:Oblivion Ring:oblivion ring').
card_rarity('oblivion ring'/'M12', 'Uncommon').
card_artist('oblivion ring'/'M12', 'Franz Vohwinkel').
card_number('oblivion ring'/'M12', '27').
card_multiverse_id('oblivion ring'/'M12', '234567').

card_in_set('onyx mage', 'M12').
card_original_type('onyx mage'/'M12', 'Creature — Human Wizard').
card_original_text('onyx mage'/'M12', '{1}{B}: Target creature you control gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_first_print('onyx mage', 'M12').
card_image_name('onyx mage'/'M12', 'onyx mage').
card_uid('onyx mage'/'M12', 'M12:Onyx Mage:onyx mage').
card_rarity('onyx mage'/'M12', 'Uncommon').
card_artist('onyx mage'/'M12', 'Izzy').
card_number('onyx mage'/'M12', '103').
card_flavor_text('onyx mage'/'M12', '\"We wield death and darkness, powers that make lesser mages tremble in fear.\"\n—Onyx creed').
card_multiverse_id('onyx mage'/'M12', '220072').

card_in_set('overrun', 'M12').
card_original_type('overrun'/'M12', 'Sorcery').
card_original_text('overrun'/'M12', 'Creatures you control get +3/+3 and gain trample until end of turn. (If a creature you control would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('overrun'/'M12', 'overrun').
card_uid('overrun'/'M12', 'M12:Overrun:overrun').
card_rarity('overrun'/'M12', 'Uncommon').
card_artist('overrun'/'M12', 'Carl Critchlow').
card_number('overrun'/'M12', '186').
card_multiverse_id('overrun'/'M12', '238574').

card_in_set('pacifism', 'M12').
card_original_type('pacifism'/'M12', 'Enchantment — Aura').
card_original_text('pacifism'/'M12', 'Enchant creature\nEnchanted creature can\'t attack or block.').
card_image_name('pacifism'/'M12', 'pacifism').
card_uid('pacifism'/'M12', 'M12:Pacifism:pacifism').
card_rarity('pacifism'/'M12', 'Common').
card_artist('pacifism'/'M12', 'Robert Bliss').
card_number('pacifism'/'M12', '28').
card_flavor_text('pacifism'/'M12', 'For the first time in his life, Grakk felt a little warm and fuzzy inside.').
card_multiverse_id('pacifism'/'M12', '228956').

card_in_set('pentavus', 'M12').
card_original_type('pentavus'/'M12', 'Artifact Creature — Construct').
card_original_text('pentavus'/'M12', 'Pentavus enters the battlefield with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Pentavus: Put a 1/1 colorless Pentavite artifact creature token with flying onto the battlefield.\n{1}, Sacrifice a Pentavite: Put a +1/+1 counter on Pentavus.').
card_image_name('pentavus'/'M12', 'pentavus').
card_uid('pentavus'/'M12', 'M12:Pentavus:pentavus').
card_rarity('pentavus'/'M12', 'Rare').
card_artist('pentavus'/'M12', 'Greg Staples').
card_number('pentavus'/'M12', '213').
card_multiverse_id('pentavus'/'M12', '244251').

card_in_set('peregrine griffin', 'M12').
card_original_type('peregrine griffin'/'M12', 'Creature — Griffin').
card_original_text('peregrine griffin'/'M12', 'Flying\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_first_print('peregrine griffin', 'M12').
card_image_name('peregrine griffin'/'M12', 'peregrine griffin').
card_uid('peregrine griffin'/'M12', 'M12:Peregrine Griffin:peregrine griffin').
card_rarity('peregrine griffin'/'M12', 'Common').
card_artist('peregrine griffin'/'M12', 'Steve Prescott').
card_number('peregrine griffin'/'M12', '29').
card_flavor_text('peregrine griffin'/'M12', 'A griffin is not just an eagle\'s wings and a lion\'s claws. It is also the fiercest of hearts.').
card_multiverse_id('peregrine griffin'/'M12', '238327').

card_in_set('personal sanctuary', 'M12').
card_original_type('personal sanctuary'/'M12', 'Enchantment').
card_original_text('personal sanctuary'/'M12', 'During your turn, prevent all damage that would be dealt to you.').
card_first_print('personal sanctuary', 'M12').
card_image_name('personal sanctuary'/'M12', 'personal sanctuary').
card_uid('personal sanctuary'/'M12', 'M12:Personal Sanctuary:personal sanctuary').
card_rarity('personal sanctuary'/'M12', 'Rare').
card_artist('personal sanctuary'/'M12', 'Howard Lyon').
card_number('personal sanctuary'/'M12', '30').
card_flavor_text('personal sanctuary'/'M12', '\"There is a place deep within my soul where no foe can intrude and no tyrant can conquer.\"\n—Sarlena, paladin of the Northern Verge').
card_multiverse_id('personal sanctuary'/'M12', '220235').

card_in_set('phantasmal bear', 'M12').
card_original_type('phantasmal bear'/'M12', 'Creature — Bear Illusion').
card_original_text('phantasmal bear'/'M12', 'When Phantasmal Bear becomes the target of a spell or ability, sacrifice it.').
card_first_print('phantasmal bear', 'M12').
card_image_name('phantasmal bear'/'M12', 'phantasmal bear').
card_uid('phantasmal bear'/'M12', 'M12:Phantasmal Bear:phantasmal bear').
card_rarity('phantasmal bear'/'M12', 'Common').
card_artist('phantasmal bear'/'M12', 'Ryan Yee').
card_number('phantasmal bear'/'M12', '70').
card_flavor_text('phantasmal bear'/'M12', '\"You know it to be false, but when its claws find you, what you know won\'t matter much at all.\"\n—Pol Jamaar, illusionist').
card_multiverse_id('phantasmal bear'/'M12', '220251').

card_in_set('phantasmal dragon', 'M12').
card_original_type('phantasmal dragon'/'M12', 'Creature — Dragon Illusion').
card_original_text('phantasmal dragon'/'M12', 'Flying\nWhen Phantasmal Dragon becomes the target of a spell or ability, sacrifice it.').
card_first_print('phantasmal dragon', 'M12').
card_image_name('phantasmal dragon'/'M12', 'phantasmal dragon').
card_uid('phantasmal dragon'/'M12', 'M12:Phantasmal Dragon:phantasmal dragon').
card_rarity('phantasmal dragon'/'M12', 'Uncommon').
card_artist('phantasmal dragon'/'M12', 'Wayne Reynolds').
card_number('phantasmal dragon'/'M12', '71').
card_flavor_text('phantasmal dragon'/'M12', 'Its hunger and ire are no less for being wrought of lies and mist.').
card_multiverse_id('phantasmal dragon'/'M12', '220068').

card_in_set('phantasmal image', 'M12').
card_original_type('phantasmal image'/'M12', 'Creature — Illusion').
card_original_text('phantasmal image'/'M12', 'You may have Phantasmal Image enter the battlefield as a copy of any creature on the battlefield, except it\'s an Illusion in addition to its other types and it gains \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_first_print('phantasmal image', 'M12').
card_image_name('phantasmal image'/'M12', 'phantasmal image').
card_uid('phantasmal image'/'M12', 'M12:Phantasmal Image:phantasmal image').
card_rarity('phantasmal image'/'M12', 'Rare').
card_artist('phantasmal image'/'M12', 'Nils Hamm').
card_number('phantasmal image'/'M12', '72').
card_multiverse_id('phantasmal image'/'M12', '220099').

card_in_set('plains', 'M12').
card_original_type('plains'/'M12', 'Basic Land — Plains').
card_original_text('plains'/'M12', 'W').
card_image_name('plains'/'M12', 'plains1').
card_uid('plains'/'M12', 'M12:Plains:plains1').
card_rarity('plains'/'M12', 'Basic Land').
card_artist('plains'/'M12', 'Rob Alexander').
card_number('plains'/'M12', '230').
card_multiverse_id('plains'/'M12', '244333').

card_in_set('plains', 'M12').
card_original_type('plains'/'M12', 'Basic Land — Plains').
card_original_text('plains'/'M12', 'W').
card_image_name('plains'/'M12', 'plains2').
card_uid('plains'/'M12', 'M12:Plains:plains2').
card_rarity('plains'/'M12', 'Basic Land').
card_artist('plains'/'M12', 'D. J. Cleland-Hura').
card_number('plains'/'M12', '231').
card_multiverse_id('plains'/'M12', '244331').

card_in_set('plains', 'M12').
card_original_type('plains'/'M12', 'Basic Land — Plains').
card_original_text('plains'/'M12', 'W').
card_image_name('plains'/'M12', 'plains3').
card_uid('plains'/'M12', 'M12:Plains:plains3').
card_rarity('plains'/'M12', 'Basic Land').
card_artist('plains'/'M12', 'Howard Lyon').
card_number('plains'/'M12', '232').
card_multiverse_id('plains'/'M12', '244334').

card_in_set('plains', 'M12').
card_original_type('plains'/'M12', 'Basic Land — Plains').
card_original_text('plains'/'M12', 'W').
card_image_name('plains'/'M12', 'plains4').
card_uid('plains'/'M12', 'M12:Plains:plains4').
card_rarity('plains'/'M12', 'Basic Land').
card_artist('plains'/'M12', 'Charles Urbach').
card_number('plains'/'M12', '233').
card_multiverse_id('plains'/'M12', '244332').

card_in_set('plummet', 'M12').
card_original_type('plummet'/'M12', 'Instant').
card_original_text('plummet'/'M12', 'Destroy target creature with flying.').
card_image_name('plummet'/'M12', 'plummet').
card_uid('plummet'/'M12', 'M12:Plummet:plummet').
card_rarity('plummet'/'M12', 'Common').
card_artist('plummet'/'M12', 'Pete Venters').
card_number('plummet'/'M12', '187').
card_flavor_text('plummet'/'M12', '\"Let nothing own the skies but the wind.\"\n—Dejara, Giltwood druid').
card_multiverse_id('plummet'/'M12', '233235').

card_in_set('ponder', 'M12').
card_original_type('ponder'/'M12', 'Sorcery').
card_original_text('ponder'/'M12', 'Look at the top three cards of your library, then put them back in any order. You may shuffle your library.\nDraw a card.').
card_image_name('ponder'/'M12', 'ponder').
card_uid('ponder'/'M12', 'M12:Ponder:ponder').
card_rarity('ponder'/'M12', 'Common').
card_artist('ponder'/'M12', 'Dan Scott').
card_number('ponder'/'M12', '73').
card_flavor_text('ponder'/'M12', 'Tomorrow belongs to those who prepare for it today.').
card_multiverse_id('ponder'/'M12', '244313').

card_in_set('pride guardian', 'M12').
card_original_type('pride guardian'/'M12', 'Creature — Cat Monk').
card_original_text('pride guardian'/'M12', 'Defender (This creature can\'t attack.)\nWhenever Pride Guardian blocks, you gain 3 life.').
card_first_print('pride guardian', 'M12').
card_image_name('pride guardian'/'M12', 'pride guardian').
card_uid('pride guardian'/'M12', 'M12:Pride Guardian:pride guardian').
card_rarity('pride guardian'/'M12', 'Common').
card_artist('pride guardian'/'M12', 'Chris Rahn').
card_number('pride guardian'/'M12', '31').
card_flavor_text('pride guardian'/'M12', '\"Any hotheaded cub can raise a sword in anger.\"').
card_multiverse_id('pride guardian'/'M12', '220073').

card_in_set('primeval titan', 'M12').
card_original_type('primeval titan'/'M12', 'Creature — Giant').
card_original_text('primeval titan'/'M12', 'Trample\nWhenever Primeval Titan enters the battlefield or attacks, you may search your library for up to two land cards, put them onto the battlefield tapped, then shuffle your library.').
card_image_name('primeval titan'/'M12', 'primeval titan').
card_uid('primeval titan'/'M12', 'M12:Primeval Titan:primeval titan').
card_rarity('primeval titan'/'M12', 'Mythic Rare').
card_artist('primeval titan'/'M12', 'Aleksi Briclot').
card_number('primeval titan'/'M12', '188').
card_flavor_text('primeval titan'/'M12', 'When nature calls, run.').
card_multiverse_id('primeval titan'/'M12', '241832').

card_in_set('primordial hydra', 'M12').
card_original_type('primordial hydra'/'M12', 'Creature — Hydra').
card_original_text('primordial hydra'/'M12', 'Primordial Hydra enters the battlefield with X +1/+1 counters on it.\nAt the beginning of your upkeep, double the number of +1/+1 counters on Primordial Hydra.\nPrimordial Hydra has trample as long as it has ten or more +1/+1 counters on it.').
card_image_name('primordial hydra'/'M12', 'primordial hydra').
card_uid('primordial hydra'/'M12', 'M12:Primordial Hydra:primordial hydra').
card_rarity('primordial hydra'/'M12', 'Mythic Rare').
card_artist('primordial hydra'/'M12', 'Aleksi Briclot').
card_number('primordial hydra'/'M12', '189').
card_multiverse_id('primordial hydra'/'M12', '220172').

card_in_set('quicksilver amulet', 'M12').
card_original_type('quicksilver amulet'/'M12', 'Artifact').
card_original_text('quicksilver amulet'/'M12', '{4}, {T}: You may put a creature card from your hand onto the battlefield.').
card_image_name('quicksilver amulet'/'M12', 'quicksilver amulet').
card_uid('quicksilver amulet'/'M12', 'M12:Quicksilver Amulet:quicksilver amulet').
card_rarity('quicksilver amulet'/'M12', 'Rare').
card_artist('quicksilver amulet'/'M12', 'Brad Rigney').
card_number('quicksilver amulet'/'M12', '214').
card_flavor_text('quicksilver amulet'/'M12', 'Those who examine the amulet too closely have been known to become part of its ornate carvings.').
card_multiverse_id('quicksilver amulet'/'M12', '237006').

card_in_set('rampant growth', 'M12').
card_original_type('rampant growth'/'M12', 'Sorcery').
card_original_text('rampant growth'/'M12', 'Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('rampant growth'/'M12', 'rampant growth').
card_uid('rampant growth'/'M12', 'M12:Rampant Growth:rampant growth').
card_rarity('rampant growth'/'M12', 'Common').
card_artist('rampant growth'/'M12', 'Steven Belledin').
card_number('rampant growth'/'M12', '190').
card_flavor_text('rampant growth'/'M12', 'Nature grows solutions to its problems.').
card_multiverse_id('rampant growth'/'M12', '233231').

card_in_set('reassembling skeleton', 'M12').
card_original_type('reassembling skeleton'/'M12', 'Creature — Skeleton Warrior').
card_original_text('reassembling skeleton'/'M12', '{1}{B}: Return Reassembling Skeleton from your graveyard to the battlefield tapped.').
card_image_name('reassembling skeleton'/'M12', 'reassembling skeleton').
card_uid('reassembling skeleton'/'M12', 'M12:Reassembling Skeleton:reassembling skeleton').
card_rarity('reassembling skeleton'/'M12', 'Uncommon').
card_artist('reassembling skeleton'/'M12', 'Austin Hsu').
card_number('reassembling skeleton'/'M12', '104').
card_flavor_text('reassembling skeleton'/'M12', '\"They may show up with the wrong thigh bone or mandible, but they always show up.\"\n—Zul Ashur, lich lord').
card_multiverse_id('reassembling skeleton'/'M12', '226565').

card_in_set('reclaim', 'M12').
card_original_type('reclaim'/'M12', 'Instant').
card_original_text('reclaim'/'M12', 'Put target card from your graveyard on top of your library.').
card_image_name('reclaim'/'M12', 'reclaim').
card_uid('reclaim'/'M12', 'M12:Reclaim:reclaim').
card_rarity('reclaim'/'M12', 'Common').
card_artist('reclaim'/'M12', 'Andrew Robinson').
card_number('reclaim'/'M12', '191').
card_flavor_text('reclaim'/'M12', 'The wise pay as much attention to what they throw away as to what they keep.').
card_multiverse_id('reclaim'/'M12', '226903').

card_in_set('redirect', 'M12').
card_original_type('redirect'/'M12', 'Instant').
card_original_text('redirect'/'M12', 'You may choose new targets for target spell.').
card_image_name('redirect'/'M12', 'redirect').
card_uid('redirect'/'M12', 'M12:Redirect:redirect').
card_rarity('redirect'/'M12', 'Rare').
card_artist('redirect'/'M12', 'Izzy').
card_number('redirect'/'M12', '74').
card_flavor_text('redirect'/'M12', '\"It\'s actually quite simple, but since you\'ve only recently begun to walk upright, it may take some time to explain.\"\n—Jace Beleren, to Garruk Wildspeaker').
card_multiverse_id('redirect'/'M12', '221484').

card_in_set('reverberate', 'M12').
card_original_type('reverberate'/'M12', 'Instant').
card_original_text('reverberate'/'M12', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_image_name('reverberate'/'M12', 'reverberate').
card_uid('reverberate'/'M12', 'M12:Reverberate:reverberate').
card_rarity('reverberate'/'M12', 'Rare').
card_artist('reverberate'/'M12', 'jD').
card_number('reverberate'/'M12', '152').
card_flavor_text('reverberate'/'M12', 'For every action, there is a swifter and more violent reaction.').
card_multiverse_id('reverberate'/'M12', '233722').

card_in_set('rites of flourishing', 'M12').
card_original_type('rites of flourishing'/'M12', 'Enchantment').
card_original_text('rites of flourishing'/'M12', 'At the beginning of each player\'s draw step, that player draws an additional card.\nEach player may play an additional land on each of his or her turns.').
card_image_name('rites of flourishing'/'M12', 'rites of flourishing').
card_uid('rites of flourishing'/'M12', 'M12:Rites of Flourishing:rites of flourishing').
card_rarity('rites of flourishing'/'M12', 'Rare').
card_artist('rites of flourishing'/'M12', 'Brandon Kitkouski').
card_number('rites of flourishing'/'M12', '192').
card_flavor_text('rites of flourishing'/'M12', '\"Dance, and bring forth the coil! It is an umbilical to Gaea herself, fattening us with the earth\'s rich bounty.\"').
card_multiverse_id('rites of flourishing'/'M12', '235188').

card_in_set('roc egg', 'M12').
card_original_type('roc egg'/'M12', 'Creature — Bird').
card_original_text('roc egg'/'M12', 'Defender (This creature can\'t attack.)\nWhen Roc Egg dies, put a 3/3 white Bird creature token with flying onto the battlefield.').
card_image_name('roc egg'/'M12', 'roc egg').
card_uid('roc egg'/'M12', 'M12:Roc Egg:roc egg').
card_rarity('roc egg'/'M12', 'Uncommon').
card_artist('roc egg'/'M12', 'Paul Bonner').
card_number('roc egg'/'M12', '32').
card_flavor_text('roc egg'/'M12', 'A thin shell lies between the roc and a world of bite-sized treats.').
card_multiverse_id('roc egg'/'M12', '238570').

card_in_set('rootbound crag', 'M12').
card_original_type('rootbound crag'/'M12', 'Land').
card_original_text('rootbound crag'/'M12', 'Rootbound Crag enters the battlefield tapped unless you control a Mountain or a Forest.\n{T}: Add {R} or {G} to your mana pool.').
card_image_name('rootbound crag'/'M12', 'rootbound crag').
card_uid('rootbound crag'/'M12', 'M12:Rootbound Crag:rootbound crag').
card_rarity('rootbound crag'/'M12', 'Rare').
card_artist('rootbound crag'/'M12', 'Matt Stewart').
card_number('rootbound crag'/'M12', '228').
card_multiverse_id('rootbound crag'/'M12', '221922').

card_in_set('royal assassin', 'M12').
card_original_type('royal assassin'/'M12', 'Creature — Human Assassin').
card_original_text('royal assassin'/'M12', '{T}: Destroy target tapped creature.').
card_image_name('royal assassin'/'M12', 'royal assassin').
card_uid('royal assassin'/'M12', 'M12:Royal Assassin:royal assassin').
card_rarity('royal assassin'/'M12', 'Rare').
card_artist('royal assassin'/'M12', 'Mark Zug').
card_number('royal assassin'/'M12', '105').
card_flavor_text('royal assassin'/'M12', 'An assassin is a king\'s most trusted courier, ensuring his messages are heard by even the most unwilling recipients.').
card_multiverse_id('royal assassin'/'M12', '236464').

card_in_set('rune-scarred demon', 'M12').
card_original_type('rune-scarred demon'/'M12', 'Creature — Demon').
card_original_text('rune-scarred demon'/'M12', 'Flying\nWhen Rune-Scarred Demon enters the battlefield, search your library for a card, put it into your hand, then shuffle your library.').
card_first_print('rune-scarred demon', 'M12').
card_image_name('rune-scarred demon'/'M12', 'rune-scarred demon').
card_uid('rune-scarred demon'/'M12', 'M12:Rune-Scarred Demon:rune-scarred demon').
card_rarity('rune-scarred demon'/'M12', 'Rare').
card_artist('rune-scarred demon'/'M12', 'Michael Komarck').
card_number('rune-scarred demon'/'M12', '106').
card_flavor_text('rune-scarred demon'/'M12', 'The litany of the infernal on his flesh pulses to the beating of his dark heart.').
card_multiverse_id('rune-scarred demon'/'M12', '220130').

card_in_set('runeclaw bear', 'M12').
card_original_type('runeclaw bear'/'M12', 'Creature — Bear').
card_original_text('runeclaw bear'/'M12', '').
card_image_name('runeclaw bear'/'M12', 'runeclaw bear').
card_uid('runeclaw bear'/'M12', 'M12:Runeclaw Bear:runeclaw bear').
card_rarity('runeclaw bear'/'M12', 'Common').
card_artist('runeclaw bear'/'M12', 'Jesper Ejsing').
card_number('runeclaw bear'/'M12', '193').
card_flavor_text('runeclaw bear'/'M12', 'The only honey it wants is the sweet nectar flowing through your veins.').
card_multiverse_id('runeclaw bear'/'M12', '221893').

card_in_set('rusted sentinel', 'M12').
card_original_type('rusted sentinel'/'M12', 'Artifact Creature — Golem').
card_original_text('rusted sentinel'/'M12', 'Rusted Sentinel enters the battlefield tapped.').
card_first_print('rusted sentinel', 'M12').
card_image_name('rusted sentinel'/'M12', 'rusted sentinel').
card_uid('rusted sentinel'/'M12', 'M12:Rusted Sentinel:rusted sentinel').
card_rarity('rusted sentinel'/'M12', 'Uncommon').
card_artist('rusted sentinel'/'M12', 'Jason Felix').
card_number('rusted sentinel'/'M12', '215').
card_flavor_text('rusted sentinel'/'M12', 'It has to deliberate a moment on the best way to break your face.').
card_multiverse_id('rusted sentinel'/'M12', '220241').

card_in_set('sacred wolf', 'M12').
card_original_type('sacred wolf'/'M12', 'Creature — Wolf').
card_original_text('sacred wolf'/'M12', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_image_name('sacred wolf'/'M12', 'sacred wolf').
card_uid('sacred wolf'/'M12', 'M12:Sacred Wolf:sacred wolf').
card_rarity('sacred wolf'/'M12', 'Common').
card_artist('sacred wolf'/'M12', 'Matt Stewart').
card_number('sacred wolf'/'M12', '194').
card_flavor_text('sacred wolf'/'M12', '\"I raised my bow, and the wolf stared at me. Under its gaze, my finger would not release the string.\"\n—Aref the Hunter').
card_multiverse_id('sacred wolf'/'M12', '241859').

card_in_set('scepter of empires', 'M12').
card_original_type('scepter of empires'/'M12', 'Artifact').
card_original_text('scepter of empires'/'M12', '{T}: Scepter of Empires deals 1 damage to target player. It deals 3 damage to that player instead if you control artifacts named Crown of Empires and Throne of Empires.').
card_first_print('scepter of empires', 'M12').
card_image_name('scepter of empires'/'M12', 'scepter of empires').
card_uid('scepter of empires'/'M12', 'M12:Scepter of Empires:scepter of empires').
card_rarity('scepter of empires'/'M12', 'Uncommon').
card_artist('scepter of empires'/'M12', 'John Avon').
card_number('scepter of empires'/'M12', '216').
card_flavor_text('scepter of empires'/'M12', '\"With this scepter, smite your enemies.\"\n—Scepter inscription').
card_multiverse_id('scepter of empires'/'M12', '220209').

card_in_set('scrambleverse', 'M12').
card_original_type('scrambleverse'/'M12', 'Sorcery').
card_original_text('scrambleverse'/'M12', 'For each nonland permanent, choose a player at random. Then each player gains control of each permanent for which he or she was chosen. Untap those permanents.').
card_first_print('scrambleverse', 'M12').
card_image_name('scrambleverse'/'M12', 'scrambleverse').
card_uid('scrambleverse'/'M12', 'M12:Scrambleverse:scrambleverse').
card_rarity('scrambleverse'/'M12', 'Rare').
card_artist('scrambleverse'/'M12', 'Dan Scott').
card_number('scrambleverse'/'M12', '153').
card_flavor_text('scrambleverse'/'M12', 'Sometimes a little chaos is in order.').
card_multiverse_id('scrambleverse'/'M12', '220300').

card_in_set('sengir vampire', 'M12').
card_original_type('sengir vampire'/'M12', 'Creature — Vampire').
card_original_text('sengir vampire'/'M12', 'Flying\nWhenever a creature dealt damage by Sengir Vampire this turn dies, put a +1/+1 counter on Sengir Vampire.').
card_image_name('sengir vampire'/'M12', 'sengir vampire').
card_uid('sengir vampire'/'M12', 'M12:Sengir Vampire:sengir vampire').
card_rarity('sengir vampire'/'M12', 'Uncommon').
card_artist('sengir vampire'/'M12', 'Kev Walker').
card_number('sengir vampire'/'M12', '107').
card_flavor_text('sengir vampire'/'M12', 'Empires rise and fall, but evil is eternal.').
card_multiverse_id('sengir vampire'/'M12', '244253').

card_in_set('serra angel', 'M12').
card_original_type('serra angel'/'M12', 'Creature — Angel').
card_original_text('serra angel'/'M12', 'Flying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_image_name('serra angel'/'M12', 'serra angel').
card_uid('serra angel'/'M12', 'M12:Serra Angel:serra angel').
card_rarity('serra angel'/'M12', 'Uncommon').
card_artist('serra angel'/'M12', 'Greg Staples').
card_number('serra angel'/'M12', '33').
card_flavor_text('serra angel'/'M12', 'Follow the light. In its absence, follow her.').
card_multiverse_id('serra angel'/'M12', '220176').

card_in_set('shock', 'M12').
card_original_type('shock'/'M12', 'Instant').
card_original_text('shock'/'M12', 'Shock deals 2 damage to target creature or player.').
card_image_name('shock'/'M12', 'shock').
card_uid('shock'/'M12', 'M12:Shock:shock').
card_rarity('shock'/'M12', 'Common').
card_artist('shock'/'M12', 'Jon Foster').
card_number('shock'/'M12', '154').
card_flavor_text('shock'/'M12', '\"The beauty of it is they never see it coming. Ever.\"\n—Razzix, sparkmage').
card_multiverse_id('shock'/'M12', '245184').

card_in_set('siege mastodon', 'M12').
card_original_type('siege mastodon'/'M12', 'Creature — Elephant').
card_original_text('siege mastodon'/'M12', '').
card_image_name('siege mastodon'/'M12', 'siege mastodon').
card_uid('siege mastodon'/'M12', 'M12:Siege Mastodon:siege mastodon').
card_rarity('siege mastodon'/'M12', 'Common').
card_artist('siege mastodon'/'M12', 'Matt Cavotta').
card_number('siege mastodon'/'M12', '34').
card_flavor_text('siege mastodon'/'M12', '\"The walls of the wicked will fall before us. Ready the siege engines. We proceed to war!\"\n—General Avitora').
card_multiverse_id('siege mastodon'/'M12', '222635').

card_in_set('skinshifter', 'M12').
card_original_type('skinshifter'/'M12', 'Creature — Human Shaman').
card_original_text('skinshifter'/'M12', '{G}: Choose one — Until end of turn, Skinshifter becomes a 4/4 Rhino and gains trample; or until end of turn, Skinshifter becomes a 2/2 Bird and gains flying; or until end of turn, Skinshifter becomes a 0/8 Plant. Activate this ability only once each turn.').
card_first_print('skinshifter', 'M12').
card_image_name('skinshifter'/'M12', 'skinshifter').
card_uid('skinshifter'/'M12', 'M12:Skinshifter:skinshifter').
card_rarity('skinshifter'/'M12', 'Rare').
card_artist('skinshifter'/'M12', 'Matt Stewart').
card_number('skinshifter'/'M12', '195').
card_multiverse_id('skinshifter'/'M12', '220151').

card_in_set('skywinder drake', 'M12').
card_original_type('skywinder drake'/'M12', 'Creature — Drake').
card_original_text('skywinder drake'/'M12', 'Flying\nSkywinder Drake can block only creatures with flying.').
card_first_print('skywinder drake', 'M12').
card_image_name('skywinder drake'/'M12', 'skywinder drake').
card_uid('skywinder drake'/'M12', 'M12:Skywinder Drake:skywinder drake').
card_rarity('skywinder drake'/'M12', 'Common').
card_artist('skywinder drake'/'M12', 'Dan Scott').
card_number('skywinder drake'/'M12', '75').
card_flavor_text('skywinder drake'/'M12', 'It\'s so light and fragile that it can glide where the air is thinnest.').
card_multiverse_id('skywinder drake'/'M12', '220153').

card_in_set('slaughter cry', 'M12').
card_original_type('slaughter cry'/'M12', 'Instant').
card_original_text('slaughter cry'/'M12', 'Target creature gets +3/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_image_name('slaughter cry'/'M12', 'slaughter cry').
card_uid('slaughter cry'/'M12', 'M12:Slaughter Cry:slaughter cry').
card_rarity('slaughter cry'/'M12', 'Common').
card_artist('slaughter cry'/'M12', 'Matt Cavotta').
card_number('slaughter cry'/'M12', '155').
card_flavor_text('slaughter cry'/'M12', 'It\'s rather embarrassing to end up with your head mounted over his hearth.').
card_multiverse_id('slaughter cry'/'M12', '230762').

card_in_set('smallpox', 'M12').
card_original_type('smallpox'/'M12', 'Sorcery').
card_original_text('smallpox'/'M12', 'Each player loses 1 life, discards a card, sacrifices a creature, then sacrifices a land.').
card_image_name('smallpox'/'M12', 'smallpox').
card_uid('smallpox'/'M12', 'M12:Smallpox:smallpox').
card_rarity('smallpox'/'M12', 'Uncommon').
card_artist('smallpox'/'M12', 'Ryan Pancoast').
card_number('smallpox'/'M12', '108').
card_flavor_text('smallpox'/'M12', 'Take away a few things, and a rebellion may ensue. Take away everything, and the oppression will be accepted as fate.').
card_multiverse_id('smallpox'/'M12', '237010').

card_in_set('solemn simulacrum', 'M12').
card_original_type('solemn simulacrum'/'M12', 'Artifact Creature — Golem').
card_original_text('solemn simulacrum'/'M12', 'When Solemn Simulacrum enters the battlefield, you may search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.\nWhen Solemn Simulacrum dies, you may draw a card.').
card_image_name('solemn simulacrum'/'M12', 'solemn simulacrum').
card_uid('solemn simulacrum'/'M12', 'M12:Solemn Simulacrum:solemn simulacrum').
card_rarity('solemn simulacrum'/'M12', 'Rare').
card_artist('solemn simulacrum'/'M12', 'Dan Scott').
card_number('solemn simulacrum'/'M12', '217').
card_multiverse_id('solemn simulacrum'/'M12', '236907').

card_in_set('sorin markov', 'M12').
card_original_type('sorin markov'/'M12', 'Planeswalker — Sorin').
card_original_text('sorin markov'/'M12', '+2: Sorin Markov deals 2 damage to target creature or player and you gain 2 life.\n-3: Target opponent\'s life total becomes 10.\n-7: You control target player during that player\'s next turn.').
card_image_name('sorin markov'/'M12', 'sorin markov').
card_uid('sorin markov'/'M12', 'M12:Sorin Markov:sorin markov').
card_rarity('sorin markov'/'M12', 'Mythic Rare').
card_artist('sorin markov'/'M12', 'Michael Komarck').
card_number('sorin markov'/'M12', '109').
card_multiverse_id('sorin markov'/'M12', '238330').

card_in_set('sorin\'s thirst', 'M12').
card_original_type('sorin\'s thirst'/'M12', 'Instant').
card_original_text('sorin\'s thirst'/'M12', 'Sorin\'s Thirst deals 2 damage to target creature and you gain 2 life.').
card_first_print('sorin\'s thirst', 'M12').
card_image_name('sorin\'s thirst'/'M12', 'sorin\'s thirst').
card_uid('sorin\'s thirst'/'M12', 'M12:Sorin\'s Thirst:sorin\'s thirst').
card_rarity('sorin\'s thirst'/'M12', 'Common').
card_artist('sorin\'s thirst'/'M12', 'Karl Kopinski').
card_number('sorin\'s thirst'/'M12', '110').
card_flavor_text('sorin\'s thirst'/'M12', '\"All your steel won\'t protect you if your will is weak.\"').
card_multiverse_id('sorin\'s thirst'/'M12', '241994').

card_in_set('sorin\'s vengeance', 'M12').
card_original_type('sorin\'s vengeance'/'M12', 'Sorcery').
card_original_text('sorin\'s vengeance'/'M12', 'Sorin\'s Vengeance deals 10 damage to target player and you gain 10 life.').
card_first_print('sorin\'s vengeance', 'M12').
card_image_name('sorin\'s vengeance'/'M12', 'sorin\'s vengeance').
card_uid('sorin\'s vengeance'/'M12', 'M12:Sorin\'s Vengeance:sorin\'s vengeance').
card_rarity('sorin\'s vengeance'/'M12', 'Rare').
card_artist('sorin\'s vengeance'/'M12', 'Jana Schirmer & Johannes Voss').
card_number('sorin\'s vengeance'/'M12', '111').
card_flavor_text('sorin\'s vengeance'/'M12', '\"Cherish these last moments. Though your miserable life has come to nothing, I have given it a magnificent end.\"').
card_multiverse_id('sorin\'s vengeance'/'M12', '234436').

card_in_set('sphinx of uthuun', 'M12').
card_original_type('sphinx of uthuun'/'M12', 'Creature — Sphinx').
card_original_text('sphinx of uthuun'/'M12', 'Flying\nWhen Sphinx of Uthuun enters the battlefield, reveal the top five cards of your library. An opponent separates those cards into two piles. Put one pile into your hand and the other into your graveyard.').
card_first_print('sphinx of uthuun', 'M12').
card_image_name('sphinx of uthuun'/'M12', 'sphinx of uthuun').
card_uid('sphinx of uthuun'/'M12', 'M12:Sphinx of Uthuun:sphinx of uthuun').
card_rarity('sphinx of uthuun'/'M12', 'Rare').
card_artist('sphinx of uthuun'/'M12', 'Kekai Kotaki').
card_number('sphinx of uthuun'/'M12', '76').
card_multiverse_id('sphinx of uthuun'/'M12', '220090').

card_in_set('spirit mantle', 'M12').
card_original_type('spirit mantle'/'M12', 'Enchantment — Aura').
card_original_text('spirit mantle'/'M12', 'Enchant creature\nEnchanted creature gets +1/+1 and has protection from creatures. (It can\'t be blocked, targeted, or dealt damage by creatures.)').
card_first_print('spirit mantle', 'M12').
card_image_name('spirit mantle'/'M12', 'spirit mantle').
card_uid('spirit mantle'/'M12', 'M12:Spirit Mantle:spirit mantle').
card_rarity('spirit mantle'/'M12', 'Uncommon').
card_artist('spirit mantle'/'M12', 'Izzy').
card_number('spirit mantle'/'M12', '35').
card_flavor_text('spirit mantle'/'M12', 'The shield of unimpeachable purity is as strong as any wrought on the anvil.').
card_multiverse_id('spirit mantle'/'M12', '220154').

card_in_set('stampeding rhino', 'M12').
card_original_type('stampeding rhino'/'M12', 'Creature — Rhino').
card_original_text('stampeding rhino'/'M12', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_image_name('stampeding rhino'/'M12', 'stampeding rhino').
card_uid('stampeding rhino'/'M12', 'M12:Stampeding Rhino:stampeding rhino').
card_rarity('stampeding rhino'/'M12', 'Common').
card_artist('stampeding rhino'/'M12', 'Steven Belledin').
card_number('stampeding rhino'/'M12', '196').
card_flavor_text('stampeding rhino'/'M12', 'The quickest way to get through the jungle is to agitate a rhino and run in its wake.').
card_multiverse_id('stampeding rhino'/'M12', '244825').

card_in_set('stave off', 'M12').
card_original_type('stave off'/'M12', 'Instant').
card_original_text('stave off'/'M12', 'Target creature gains protection from the color of your choice until end of turn. (It can\'t be blocked, targeted, dealt damage, or enchanted by anything of that color.)').
card_first_print('stave off', 'M12').
card_image_name('stave off'/'M12', 'stave off').
card_uid('stave off'/'M12', 'M12:Stave Off:stave off').
card_rarity('stave off'/'M12', 'Common').
card_artist('stave off'/'M12', 'Mark Zug').
card_number('stave off'/'M12', '36').
card_multiverse_id('stave off'/'M12', '220295').

card_in_set('stingerfling spider', 'M12').
card_original_type('stingerfling spider'/'M12', 'Creature — Spider').
card_original_text('stingerfling spider'/'M12', 'Reach (This creature can block creatures with flying.)\nWhen Stingerfling Spider enters the battlefield, you may destroy target creature with flying.').
card_first_print('stingerfling spider', 'M12').
card_image_name('stingerfling spider'/'M12', 'stingerfling spider').
card_uid('stingerfling spider'/'M12', 'M12:Stingerfling Spider:stingerfling spider').
card_rarity('stingerfling spider'/'M12', 'Uncommon').
card_artist('stingerfling spider'/'M12', 'Dave Allsop').
card_number('stingerfling spider'/'M12', '197').
card_flavor_text('stingerfling spider'/'M12', 'The juiciest prey is that which grows lazy, thinking itself beyond the reach of danger.').
card_multiverse_id('stingerfling spider'/'M12', '220075').

card_in_set('stonehorn dignitary', 'M12').
card_original_type('stonehorn dignitary'/'M12', 'Creature — Rhino Soldier').
card_original_text('stonehorn dignitary'/'M12', 'When Stonehorn Dignitary enters the battlefield, target opponent skips his or her next combat phase.').
card_first_print('stonehorn dignitary', 'M12').
card_image_name('stonehorn dignitary'/'M12', 'stonehorn dignitary').
card_uid('stonehorn dignitary'/'M12', 'M12:Stonehorn Dignitary:stonehorn dignitary').
card_rarity('stonehorn dignitary'/'M12', 'Common').
card_artist('stonehorn dignitary'/'M12', 'Dave Kendall').
card_number('stonehorn dignitary'/'M12', '37').
card_flavor_text('stonehorn dignitary'/'M12', 'It did not escape the ambassador\'s notice that the sound of war drums could also marshal the city to attention.').
card_multiverse_id('stonehorn dignitary'/'M12', '220160').

card_in_set('stormblood berserker', 'M12').
card_original_type('stormblood berserker'/'M12', 'Creature — Human Berserker').
card_original_text('stormblood berserker'/'M12', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)\nStormblood Berserker can\'t be blocked except by two or more creatures.').
card_image_name('stormblood berserker'/'M12', 'stormblood berserker').
card_uid('stormblood berserker'/'M12', 'M12:Stormblood Berserker:stormblood berserker').
card_rarity('stormblood berserker'/'M12', 'Uncommon').
card_artist('stormblood berserker'/'M12', 'Min Yum').
card_number('stormblood berserker'/'M12', '156').
card_flavor_text('stormblood berserker'/'M12', 'Blood is holy to those whose god is War.').
card_multiverse_id('stormblood berserker'/'M12', '220277').

card_in_set('stormfront pegasus', 'M12').
card_original_type('stormfront pegasus'/'M12', 'Creature — Pegasus').
card_original_text('stormfront pegasus'/'M12', 'Flying').
card_image_name('stormfront pegasus'/'M12', 'stormfront pegasus').
card_uid('stormfront pegasus'/'M12', 'M12:Stormfront Pegasus:stormfront pegasus').
card_rarity('stormfront pegasus'/'M12', 'Common').
card_artist('stormfront pegasus'/'M12', 'rk post').
card_number('stormfront pegasus'/'M12', '38').
card_flavor_text('stormfront pegasus'/'M12', 'Its hooves touch the earth only when a hero must rise.').
card_multiverse_id('stormfront pegasus'/'M12', '222633').

card_in_set('sun titan', 'M12').
card_original_type('sun titan'/'M12', 'Creature — Giant').
card_original_text('sun titan'/'M12', 'Vigilance\nWhenever Sun Titan enters the battlefield or attacks, you may return target permanent card with converted mana cost 3 or less from your graveyard to the battlefield.').
card_image_name('sun titan'/'M12', 'sun titan').
card_uid('sun titan'/'M12', 'M12:Sun Titan:sun titan').
card_rarity('sun titan'/'M12', 'Mythic Rare').
card_artist('sun titan'/'M12', 'Todd Lockwood').
card_number('sun titan'/'M12', '39').
card_flavor_text('sun titan'/'M12', 'A blazing sun that never sets.').
card_multiverse_id('sun titan'/'M12', '241833').

card_in_set('sundial of the infinite', 'M12').
card_original_type('sundial of the infinite'/'M12', 'Artifact').
card_original_text('sundial of the infinite'/'M12', '{1}, {T}: End the turn. Activate this ability only during your turn. (Exile all spells and abilities on the stack. Discard down to your maximum hand size. Damage wears off, and \"this turn\" and \"until end of turn\" effects end.)').
card_first_print('sundial of the infinite', 'M12').
card_image_name('sundial of the infinite'/'M12', 'sundial of the infinite').
card_uid('sundial of the infinite'/'M12', 'M12:Sundial of the Infinite:sundial of the infinite').
card_rarity('sundial of the infinite'/'M12', 'Rare').
card_artist('sundial of the infinite'/'M12', 'Vincent Proce').
card_number('sundial of the infinite'/'M12', '218').
card_multiverse_id('sundial of the infinite'/'M12', '228118').

card_in_set('sunpetal grove', 'M12').
card_original_type('sunpetal grove'/'M12', 'Land').
card_original_text('sunpetal grove'/'M12', 'Sunpetal Grove enters the battlefield tapped unless you control a Forest or a Plains.\n{T}: Add {G} or {W} to your mana pool.').
card_image_name('sunpetal grove'/'M12', 'sunpetal grove').
card_uid('sunpetal grove'/'M12', 'M12:Sunpetal Grove:sunpetal grove').
card_rarity('sunpetal grove'/'M12', 'Rare').
card_artist('sunpetal grove'/'M12', 'Jason Chan').
card_number('sunpetal grove'/'M12', '229').
card_multiverse_id('sunpetal grove'/'M12', '221923').

card_in_set('sutured ghoul', 'M12').
card_original_type('sutured ghoul'/'M12', 'Creature — Zombie').
card_original_text('sutured ghoul'/'M12', 'Trample\nAs Sutured Ghoul enters the battlefield, exile any number of creature cards from your graveyard.\nSutured Ghoul\'s power is equal to the total power of the exiled cards and its toughness is equal to their total toughness.').
card_image_name('sutured ghoul'/'M12', 'sutured ghoul').
card_uid('sutured ghoul'/'M12', 'M12:Sutured Ghoul:sutured ghoul').
card_rarity('sutured ghoul'/'M12', 'Rare').
card_artist('sutured ghoul'/'M12', 'Carl Critchlow').
card_number('sutured ghoul'/'M12', '112').
card_multiverse_id('sutured ghoul'/'M12', '220157').

card_in_set('swamp', 'M12').
card_original_type('swamp'/'M12', 'Basic Land — Swamp').
card_original_text('swamp'/'M12', 'B').
card_image_name('swamp'/'M12', 'swamp1').
card_uid('swamp'/'M12', 'M12:Swamp:swamp1').
card_rarity('swamp'/'M12', 'Basic Land').
card_artist('swamp'/'M12', 'Cliff Childs').
card_number('swamp'/'M12', '238').
card_multiverse_id('swamp'/'M12', '244338').

card_in_set('swamp', 'M12').
card_original_type('swamp'/'M12', 'Basic Land — Swamp').
card_original_text('swamp'/'M12', 'B').
card_image_name('swamp'/'M12', 'swamp2').
card_uid('swamp'/'M12', 'M12:Swamp:swamp2').
card_rarity('swamp'/'M12', 'Basic Land').
card_artist('swamp'/'M12', 'Chippy').
card_number('swamp'/'M12', '239').
card_multiverse_id('swamp'/'M12', '244337').

card_in_set('swamp', 'M12').
card_original_type('swamp'/'M12', 'Basic Land — Swamp').
card_original_text('swamp'/'M12', 'B').
card_image_name('swamp'/'M12', 'swamp3').
card_uid('swamp'/'M12', 'M12:Swamp:swamp3').
card_rarity('swamp'/'M12', 'Basic Land').
card_artist('swamp'/'M12', 'Jung Park').
card_number('swamp'/'M12', '240').
card_multiverse_id('swamp'/'M12', '244335').

card_in_set('swamp', 'M12').
card_original_type('swamp'/'M12', 'Basic Land — Swamp').
card_original_text('swamp'/'M12', 'B').
card_image_name('swamp'/'M12', 'swamp4').
card_uid('swamp'/'M12', 'M12:Swamp:swamp4').
card_rarity('swamp'/'M12', 'Basic Land').
card_artist('swamp'/'M12', 'Alan Pollack').
card_number('swamp'/'M12', '241').
card_multiverse_id('swamp'/'M12', '244336').

card_in_set('swiftfoot boots', 'M12').
card_original_type('swiftfoot boots'/'M12', 'Artifact — Equipment').
card_original_text('swiftfoot boots'/'M12', 'Equipped creature has hexproof and haste. (It can\'t be the target of spells or abilities your opponents control, and it can attack and {T} as soon as it comes under your control.)\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_first_print('swiftfoot boots', 'M12').
card_image_name('swiftfoot boots'/'M12', 'swiftfoot boots').
card_uid('swiftfoot boots'/'M12', 'M12:Swiftfoot Boots:swiftfoot boots').
card_rarity('swiftfoot boots'/'M12', 'Uncommon').
card_artist('swiftfoot boots'/'M12', 'Svetlin Velinov').
card_number('swiftfoot boots'/'M12', '219').
card_multiverse_id('swiftfoot boots'/'M12', '220261').

card_in_set('taste of blood', 'M12').
card_original_type('taste of blood'/'M12', 'Sorcery').
card_original_text('taste of blood'/'M12', 'Taste of Blood deals 1 damage to target player and you gain 1 life.').
card_first_print('taste of blood', 'M12').
card_image_name('taste of blood'/'M12', 'taste of blood').
card_uid('taste of blood'/'M12', 'M12:Taste of Blood:taste of blood').
card_rarity('taste of blood'/'M12', 'Common').
card_artist('taste of blood'/'M12', 'Howard Lyon').
card_number('taste of blood'/'M12', '113').
card_flavor_text('taste of blood'/'M12', '\"How blessed are some people, whose lives have no fears, no dreads, to whom sleep is a blessing that comes nightly, and brings nothing but sweet dreams.\"\n—Bram Stoker, Dracula').
card_multiverse_id('taste of blood'/'M12', '226561').

card_in_set('tectonic rift', 'M12').
card_original_type('tectonic rift'/'M12', 'Sorcery').
card_original_text('tectonic rift'/'M12', 'Destroy target land. Creatures without flying can\'t block this turn.').
card_first_print('tectonic rift', 'M12').
card_image_name('tectonic rift'/'M12', 'tectonic rift').
card_uid('tectonic rift'/'M12', 'M12:Tectonic Rift:tectonic rift').
card_rarity('tectonic rift'/'M12', 'Uncommon').
card_artist('tectonic rift'/'M12', 'John Avon').
card_number('tectonic rift'/'M12', '157').
card_flavor_text('tectonic rift'/'M12', '\"You will kneel before me, even if I have to split the earth under your feet!\"\n—Ash Kronor, Keldon warlord').
card_multiverse_id('tectonic rift'/'M12', '234568').

card_in_set('thran golem', 'M12').
card_original_type('thran golem'/'M12', 'Artifact Creature — Golem').
card_original_text('thran golem'/'M12', 'As long as Thran Golem is enchanted, it gets +2/+2 and has flying, first strike, and trample.').
card_image_name('thran golem'/'M12', 'thran golem').
card_uid('thran golem'/'M12', 'M12:Thran Golem:thran golem').
card_rarity('thran golem'/'M12', 'Uncommon').
card_artist('thran golem'/'M12', 'Ron Spears').
card_number('thran golem'/'M12', '220').
card_flavor_text('thran golem'/'M12', 'The golems of the ancient Thran still draw power in ways that baffle the artificers of today.').
card_multiverse_id('thran golem'/'M12', '234072').

card_in_set('throne of empires', 'M12').
card_original_type('throne of empires'/'M12', 'Artifact').
card_original_text('throne of empires'/'M12', '{1}, {T}: Put a 1/1 white Soldier creature token onto the battlefield. Put five of those tokens onto the battlefield instead if you control artifacts named Crown of Empires and Scepter of Empires.').
card_first_print('throne of empires', 'M12').
card_image_name('throne of empires'/'M12', 'throne of empires').
card_uid('throne of empires'/'M12', 'M12:Throne of Empires:throne of empires').
card_rarity('throne of empires'/'M12', 'Rare').
card_artist('throne of empires'/'M12', 'John Avon').
card_number('throne of empires'/'M12', '221').
card_flavor_text('throne of empires'/'M12', '\"With this throne, unite your nation.\"\n—Throne inscription').
card_multiverse_id('throne of empires'/'M12', '220232').

card_in_set('time reversal', 'M12').
card_original_type('time reversal'/'M12', 'Sorcery').
card_original_text('time reversal'/'M12', 'Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. Exile Time Reversal.').
card_image_name('time reversal'/'M12', 'time reversal').
card_uid('time reversal'/'M12', 'M12:Time Reversal:time reversal').
card_rarity('time reversal'/'M12', 'Mythic Rare').
card_artist('time reversal'/'M12', 'Howard Lyon').
card_number('time reversal'/'M12', '77').
card_flavor_text('time reversal'/'M12', '\"Any oaf can conquer a kingdom. It takes true power to conquer time.\"\n—Shai Fusan, archmage').
card_multiverse_id('time reversal'/'M12', '245187').

card_in_set('timely reinforcements', 'M12').
card_original_type('timely reinforcements'/'M12', 'Sorcery').
card_original_text('timely reinforcements'/'M12', 'If you have less life than an opponent, you gain 6 life. If you control fewer creatures than an opponent, put three 1/1 white Soldier creature tokens onto the battlefield.').
card_first_print('timely reinforcements', 'M12').
card_image_name('timely reinforcements'/'M12', 'timely reinforcements').
card_uid('timely reinforcements'/'M12', 'M12:Timely Reinforcements:timely reinforcements').
card_rarity('timely reinforcements'/'M12', 'Uncommon').
card_artist('timely reinforcements'/'M12', 'Tomasz Jedruszek').
card_number('timely reinforcements'/'M12', '40').
card_flavor_text('timely reinforcements'/'M12', 'The gods are not always kind, but they do have a soft spot for plucky underdogs.').
card_multiverse_id('timely reinforcements'/'M12', '220074').

card_in_set('titanic growth', 'M12').
card_original_type('titanic growth'/'M12', 'Instant').
card_original_text('titanic growth'/'M12', 'Target creature gets +4/+4 until end of turn.').
card_first_print('titanic growth', 'M12').
card_image_name('titanic growth'/'M12', 'titanic growth').
card_uid('titanic growth'/'M12', 'M12:Titanic Growth:titanic growth').
card_rarity('titanic growth'/'M12', 'Common').
card_artist('titanic growth'/'M12', 'Ryan Pancoast').
card_number('titanic growth'/'M12', '198').
card_flavor_text('titanic growth'/'M12', 'The pup looked over the treetops, eyeing the man who just yesterday had kicked her. Suddenly, her hunger was infused with pure delight.').
card_multiverse_id('titanic growth'/'M12', '221203').

card_in_set('tormented soul', 'M12').
card_original_type('tormented soul'/'M12', 'Creature — Spirit').
card_original_text('tormented soul'/'M12', 'Tormented Soul can\'t block and is unblockable.').
card_image_name('tormented soul'/'M12', 'tormented soul').
card_uid('tormented soul'/'M12', 'M12:Tormented Soul:tormented soul').
card_rarity('tormented soul'/'M12', 'Common').
card_artist('tormented soul'/'M12', 'Karl Kopinski').
card_number('tormented soul'/'M12', '114').
card_flavor_text('tormented soul'/'M12', 'Those who raged most bitterly at the world in life are cursed to roam the nether realms in death.').
card_multiverse_id('tormented soul'/'M12', '220122').

card_in_set('trollhide', 'M12').
card_original_type('trollhide'/'M12', 'Enchantment — Aura').
card_original_text('trollhide'/'M12', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"{1}{G}: Regenerate this creature.\" (The next time the creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_first_print('trollhide', 'M12').
card_image_name('trollhide'/'M12', 'trollhide').
card_uid('trollhide'/'M12', 'M12:Trollhide:trollhide').
card_rarity('trollhide'/'M12', 'Common').
card_artist('trollhide'/'M12', 'Steven Belledin').
card_number('trollhide'/'M12', '199').
card_multiverse_id('trollhide'/'M12', '220190').

card_in_set('turn to frog', 'M12').
card_original_type('turn to frog'/'M12', 'Instant').
card_original_text('turn to frog'/'M12', 'Target creature loses all abilities and becomes a 1/1 blue Frog until end of turn.').
card_first_print('turn to frog', 'M12').
card_image_name('turn to frog'/'M12', 'turn to frog').
card_uid('turn to frog'/'M12', 'M12:Turn to Frog:turn to frog').
card_rarity('turn to frog'/'M12', 'Uncommon').
card_artist('turn to frog'/'M12', 'Warren Mahy').
card_number('turn to frog'/'M12', '78').
card_flavor_text('turn to frog'/'M12', '\"Ribbit.\"').
card_multiverse_id('turn to frog'/'M12', '220265').

card_in_set('unsummon', 'M12').
card_original_type('unsummon'/'M12', 'Instant').
card_original_text('unsummon'/'M12', 'Return target creature to its owner\'s hand.').
card_image_name('unsummon'/'M12', 'unsummon').
card_uid('unsummon'/'M12', 'M12:Unsummon:unsummon').
card_rarity('unsummon'/'M12', 'Common').
card_artist('unsummon'/'M12', 'Izzy').
card_number('unsummon'/'M12', '79').
card_flavor_text('unsummon'/'M12', '\"It\'s nothing personal. I\'d just prefer if you weren\'t.\"\n—Ashurel, voidmage').
card_multiverse_id('unsummon'/'M12', '220301').

card_in_set('vampire outcasts', 'M12').
card_original_type('vampire outcasts'/'M12', 'Creature — Vampire').
card_original_text('vampire outcasts'/'M12', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_first_print('vampire outcasts', 'M12').
card_image_name('vampire outcasts'/'M12', 'vampire outcasts').
card_uid('vampire outcasts'/'M12', 'M12:Vampire Outcasts:vampire outcasts').
card_rarity('vampire outcasts'/'M12', 'Uncommon').
card_artist('vampire outcasts'/'M12', 'Clint Cearley').
card_number('vampire outcasts'/'M12', '115').
card_multiverse_id('vampire outcasts'/'M12', '220127').

card_in_set('vastwood gorger', 'M12').
card_original_type('vastwood gorger'/'M12', 'Creature — Wurm').
card_original_text('vastwood gorger'/'M12', '').
card_image_name('vastwood gorger'/'M12', 'vastwood gorger').
card_uid('vastwood gorger'/'M12', 'M12:Vastwood Gorger:vastwood gorger').
card_rarity('vastwood gorger'/'M12', 'Common').
card_artist('vastwood gorger'/'M12', 'Kieran Yanner').
card_number('vastwood gorger'/'M12', '200').
card_flavor_text('vastwood gorger'/'M12', 'The gorger\'s insect-like eyes pick up the slightest movement in any direction. Its body then whips toward its prey with startling speed, swallowing it in one engulfing gulp.').
card_multiverse_id('vastwood gorger'/'M12', '244827').

card_in_set('vengeful pharaoh', 'M12').
card_original_type('vengeful pharaoh'/'M12', 'Creature — Zombie').
card_original_text('vengeful pharaoh'/'M12', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhenever combat damage is dealt to you or a planeswalker you control, if Vengeful Pharaoh is in your graveyard, destroy target attacking creature, then put Vengeful Pharaoh on top of your library.').
card_first_print('vengeful pharaoh', 'M12').
card_image_name('vengeful pharaoh'/'M12', 'vengeful pharaoh').
card_uid('vengeful pharaoh'/'M12', 'M12:Vengeful Pharaoh:vengeful pharaoh').
card_rarity('vengeful pharaoh'/'M12', 'Rare').
card_artist('vengeful pharaoh'/'M12', 'Igor Kieryluk').
card_number('vengeful pharaoh'/'M12', '116').
card_multiverse_id('vengeful pharaoh'/'M12', '220170').

card_in_set('visions of beyond', 'M12').
card_original_type('visions of beyond'/'M12', 'Instant').
card_original_text('visions of beyond'/'M12', 'Draw a card. If a graveyard has twenty or more cards in it, draw three cards instead.').
card_first_print('visions of beyond', 'M12').
card_image_name('visions of beyond'/'M12', 'visions of beyond').
card_uid('visions of beyond'/'M12', 'M12:Visions of Beyond:visions of beyond').
card_rarity('visions of beyond'/'M12', 'Rare').
card_artist('visions of beyond'/'M12', 'Terese Nielsen').
card_number('visions of beyond'/'M12', '80').
card_flavor_text('visions of beyond'/'M12', '\"The past is the foundation for all that exists in the present. We merely reconstruct what our ancestors have already discovered.\"\n—Sachimir, Sage of Memories').
card_multiverse_id('visions of beyond'/'M12', '220226').

card_in_set('volcanic dragon', 'M12').
card_original_type('volcanic dragon'/'M12', 'Creature — Dragon').
card_original_text('volcanic dragon'/'M12', 'Flying\nHaste (This creature can attack and {T} as soon as it comes under your control.)').
card_image_name('volcanic dragon'/'M12', 'volcanic dragon').
card_uid('volcanic dragon'/'M12', 'M12:Volcanic Dragon:volcanic dragon').
card_rarity('volcanic dragon'/'M12', 'Uncommon').
card_artist('volcanic dragon'/'M12', 'Chris Rahn').
card_number('volcanic dragon'/'M12', '158').
card_flavor_text('volcanic dragon'/'M12', 'Sometimes an eruption in Shiv produces something more dangerous than a mere river of molten rock.').
card_multiverse_id('volcanic dragon'/'M12', '244719').

card_in_set('wall of torches', 'M12').
card_original_type('wall of torches'/'M12', 'Creature — Wall').
card_original_text('wall of torches'/'M12', 'Defender (This creature can\'t attack.)').
card_first_print('wall of torches', 'M12').
card_image_name('wall of torches'/'M12', 'wall of torches').
card_uid('wall of torches'/'M12', 'M12:Wall of Torches:wall of torches').
card_rarity('wall of torches'/'M12', 'Common').
card_artist('wall of torches'/'M12', 'Mike Bierek').
card_number('wall of torches'/'M12', '159').
card_flavor_text('wall of torches'/'M12', 'The foolish try to leap through the unlit spaces. The arrogant believe the fires harmless. The wise find a different door.').
card_multiverse_id('wall of torches'/'M12', '220189').

card_in_set('warpath ghoul', 'M12').
card_original_type('warpath ghoul'/'M12', 'Creature — Zombie').
card_original_text('warpath ghoul'/'M12', '').
card_image_name('warpath ghoul'/'M12', 'warpath ghoul').
card_uid('warpath ghoul'/'M12', 'M12:Warpath Ghoul:warpath ghoul').
card_rarity('warpath ghoul'/'M12', 'Common').
card_artist('warpath ghoul'/'M12', 'rk post').
card_number('warpath ghoul'/'M12', '117').
card_flavor_text('warpath ghoul'/'M12', 'Ghouls are often found skulking on the fringes of great battles, showing themselves only to snatch the wounded and weak, dragging their victims screaming into the shadows.').
card_multiverse_id('warpath ghoul'/'M12', '241995').

card_in_set('warstorm surge', 'M12').
card_original_type('warstorm surge'/'M12', 'Enchantment').
card_original_text('warstorm surge'/'M12', 'Whenever a creature enters the battlefield under your control, it deals damage equal to its power to target creature or player.').
card_first_print('warstorm surge', 'M12').
card_image_name('warstorm surge'/'M12', 'warstorm surge').
card_uid('warstorm surge'/'M12', 'M12:Warstorm Surge:warstorm surge').
card_rarity('warstorm surge'/'M12', 'Rare').
card_artist('warstorm surge'/'M12', 'Raymond Swanland').
card_number('warstorm surge'/'M12', '160').
card_flavor_text('warstorm surge'/'M12', '\"Listen to the roar! Feel the thunder! The Immersturm shouts its approval with every bolt of lightning!\"').
card_multiverse_id('warstorm surge'/'M12', '228796').

card_in_set('worldslayer', 'M12').
card_original_type('worldslayer'/'M12', 'Artifact — Equipment').
card_original_text('worldslayer'/'M12', 'Whenever equipped creature deals combat damage to a player, destroy all permanents other than Worldslayer.\nEquip {5} ({5}: Attach to target creature you control. Equip only as a sorcery.)').
card_image_name('worldslayer'/'M12', 'worldslayer').
card_uid('worldslayer'/'M12', 'M12:Worldslayer:worldslayer').
card_rarity('worldslayer'/'M12', 'Rare').
card_artist('worldslayer'/'M12', 'Greg Staples').
card_number('worldslayer'/'M12', '222').
card_flavor_text('worldslayer'/'M12', '\"If that is what you seek, try looking in craters.\"\n—Fodrus, mastersmith').
card_multiverse_id('worldslayer'/'M12', '227302').

card_in_set('wring flesh', 'M12').
card_original_type('wring flesh'/'M12', 'Instant').
card_original_text('wring flesh'/'M12', 'Target creature gets -3/-1 until end of turn.').
card_first_print('wring flesh', 'M12').
card_image_name('wring flesh'/'M12', 'wring flesh').
card_uid('wring flesh'/'M12', 'M12:Wring Flesh:wring flesh').
card_rarity('wring flesh'/'M12', 'Common').
card_artist('wring flesh'/'M12', 'Izzy').
card_number('wring flesh'/'M12', '118').
card_flavor_text('wring flesh'/'M12', '\"Don\'t blame me. You\'re the one walking around with skin.\"\n—Zul Ashur, lich lord').
card_multiverse_id('wring flesh'/'M12', '230758').

card_in_set('wurm\'s tooth', 'M12').
card_original_type('wurm\'s tooth'/'M12', 'Artifact').
card_original_text('wurm\'s tooth'/'M12', 'Whenever a player casts a green spell, you may gain 1 life.').
card_image_name('wurm\'s tooth'/'M12', 'wurm\'s tooth').
card_uid('wurm\'s tooth'/'M12', 'M12:Wurm\'s Tooth:wurm\'s tooth').
card_rarity('wurm\'s tooth'/'M12', 'Uncommon').
card_artist('wurm\'s tooth'/'M12', 'Alan Pollack').
card_number('wurm\'s tooth'/'M12', '223').
card_flavor_text('wurm\'s tooth'/'M12', 'Jevna Nor lugged it around, bragging of his triumph over the wurm. Luckily, the tavern regulars didn\'t notice it was only a baby tooth.').
card_multiverse_id('wurm\'s tooth'/'M12', '221524').

card_in_set('zombie goliath', 'M12').
card_original_type('zombie goliath'/'M12', 'Creature — Zombie Giant').
card_original_text('zombie goliath'/'M12', '').
card_image_name('zombie goliath'/'M12', 'zombie goliath').
card_uid('zombie goliath'/'M12', 'M12:Zombie Goliath:zombie goliath').
card_rarity('zombie goliath'/'M12', 'Common').
card_artist('zombie goliath'/'M12', 'E. M. Gist').
card_number('zombie goliath'/'M12', '119').
card_flavor_text('zombie goliath'/'M12', '\"Removing the encumbrance of useless brain tissue grants several advantages: a slight increase in mobility, a response of revulsion and horror from the enemy, and, in the case of large specimens, room for passengers.\"\n—Zul Ashur, lich lord').
card_multiverse_id('zombie goliath'/'M12', '226558').

card_in_set('zombie infestation', 'M12').
card_original_type('zombie infestation'/'M12', 'Enchantment').
card_original_text('zombie infestation'/'M12', 'Discard two cards: Put a 2/2 black Zombie creature token onto the battlefield.').
card_image_name('zombie infestation'/'M12', 'zombie infestation').
card_uid('zombie infestation'/'M12', 'M12:Zombie Infestation:zombie infestation').
card_rarity('zombie infestation'/'M12', 'Uncommon').
card_artist('zombie infestation'/'M12', 'Thomas M. Baxa').
card_number('zombie infestation'/'M12', '120').
card_flavor_text('zombie infestation'/'M12', 'The nomads\' funeral pyres are more practical than ceremonial.').
card_multiverse_id('zombie infestation'/'M12', '241860').
