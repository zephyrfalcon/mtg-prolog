% Commander 2013 Edition

set('C13').
set_name('C13', 'Commander 2013 Edition').
set_release_date('C13', '2013-11-01').
set_border('C13', 'black').
set_type('C13', 'commander').

card_in_set('acidic slime', 'C13').
card_original_type('acidic slime'/'C13', 'Creature — Ooze').
card_original_text('acidic slime'/'C13', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)When Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_image_name('acidic slime'/'C13', 'acidic slime').
card_uid('acidic slime'/'C13', 'C13:Acidic Slime:acidic slime').
card_rarity('acidic slime'/'C13', 'Uncommon').
card_artist('acidic slime'/'C13', 'Karl Kopinski').
card_number('acidic slime'/'C13', '134').
card_multiverse_id('acidic slime'/'C13', '376237').

card_in_set('act of authority', 'C13').
card_original_type('act of authority'/'C13', 'Enchantment').
card_original_text('act of authority'/'C13', 'When Act of Authority enters the battlefield, you may exile target artifact or enchantment.At the beginning of your upkeep, you may exile target artifact or enchantment. If you do, its controller gains control of Act of Authority.').
card_first_print('act of authority', 'C13').
card_image_name('act of authority'/'C13', 'act of authority').
card_uid('act of authority'/'C13', 'C13:Act of Authority:act of authority').
card_rarity('act of authority'/'C13', 'Rare').
card_artist('act of authority'/'C13', 'Véronique Meignaud').
card_number('act of authority'/'C13', '1').
card_multiverse_id('act of authority'/'C13', '376238').

card_in_set('aerie mystics', 'C13').
card_original_type('aerie mystics'/'C13', 'Creature — Bird Wizard').
card_original_text('aerie mystics'/'C13', 'Flying{1}{G}{U}: Creatures you control gain shroud until end of turn. (They can\'t be the targets of spells or abilities.)').
card_image_name('aerie mystics'/'C13', 'aerie mystics').
card_uid('aerie mystics'/'C13', 'C13:Aerie Mystics:aerie mystics').
card_rarity('aerie mystics'/'C13', 'Uncommon').
card_artist('aerie mystics'/'C13', 'Mark Zug').
card_number('aerie mystics'/'C13', '2').
card_flavor_text('aerie mystics'/'C13', 'They are cautious with their body language and facial expressions. Any stray movement could betray the positions of the troops they protect and cost many lives.').
card_multiverse_id('aerie mystics'/'C13', '376239').

card_in_set('æthermage\'s touch', 'C13').
card_original_type('æthermage\'s touch'/'C13', 'Instant').
card_original_text('æthermage\'s touch'/'C13', 'Reveal the top four cards of your library. You may put a creature card from among them onto the battlefield. It gains \"At the beginning of your end step, return this creature to its owner\'s hand.\" Then put the rest of the cards revealed this way on the bottom of your library in any order.').
card_image_name('æthermage\'s touch'/'C13', 'aethermage\'s touch').
card_uid('æthermage\'s touch'/'C13', 'C13:Æthermage\'s Touch:aethermage\'s touch').
card_rarity('æthermage\'s touch'/'C13', 'Rare').
card_artist('æthermage\'s touch'/'C13', 'Randy Gallegos').
card_number('æthermage\'s touch'/'C13', '176').
card_multiverse_id('æthermage\'s touch'/'C13', '376240').

card_in_set('ajani\'s pridemate', 'C13').
card_original_type('ajani\'s pridemate'/'C13', 'Creature — Cat Soldier').
card_original_text('ajani\'s pridemate'/'C13', 'Whenever you gain life, you may put a +1/+1 counter on Ajani\'s Pridemate. (For example, if an effect causes you to gain 3 life, you may put one +1/+1 counter on this creature.)').
card_image_name('ajani\'s pridemate'/'C13', 'ajani\'s pridemate').
card_uid('ajani\'s pridemate'/'C13', 'C13:Ajani\'s Pridemate:ajani\'s pridemate').
card_rarity('ajani\'s pridemate'/'C13', 'Uncommon').
card_artist('ajani\'s pridemate'/'C13', 'Svetlin Velinov').
card_number('ajani\'s pridemate'/'C13', '3').
card_flavor_text('ajani\'s pridemate'/'C13', '\"When one of us prospers, the pride prospers.\"\n—Jazal Goldmane').
card_multiverse_id('ajani\'s pridemate'/'C13', '376241').

card_in_set('akoum refuge', 'C13').
card_original_type('akoum refuge'/'C13', 'Land').
card_original_text('akoum refuge'/'C13', 'Akoum Refuge enters the battlefield tapped.When Akoum Refuge enters the battlefield, you gain 1 life.{T}: Add {B} or {R} to your mana pool.').
card_image_name('akoum refuge'/'C13', 'akoum refuge').
card_uid('akoum refuge'/'C13', 'C13:Akoum Refuge:akoum refuge').
card_rarity('akoum refuge'/'C13', 'Uncommon').
card_artist('akoum refuge'/'C13', 'Fred Fields').
card_number('akoum refuge'/'C13', '272').
card_multiverse_id('akoum refuge'/'C13', '376242').

card_in_set('angel of finality', 'C13').
card_original_type('angel of finality'/'C13', 'Creature — Angel').
card_original_text('angel of finality'/'C13', 'Flying When Angel of Finality enters the battlefield, exile all cards from target player\'s graveyard.').
card_first_print('angel of finality', 'C13').
card_image_name('angel of finality'/'C13', 'angel of finality').
card_uid('angel of finality'/'C13', 'C13:Angel of Finality:angel of finality').
card_rarity('angel of finality'/'C13', 'Rare').
card_artist('angel of finality'/'C13', 'Howard Lyon').
card_number('angel of finality'/'C13', '4').
card_flavor_text('angel of finality'/'C13', '\"Better the dead depart utterly than be enslaved.\"').
card_multiverse_id('angel of finality'/'C13', '376243').

card_in_set('annihilate', 'C13').
card_original_type('annihilate'/'C13', 'Instant').
card_original_text('annihilate'/'C13', 'Destroy target nonblack creature. It can\'t be regenerated.Draw a card.').
card_image_name('annihilate'/'C13', 'annihilate').
card_uid('annihilate'/'C13', 'C13:Annihilate:annihilate').
card_rarity('annihilate'/'C13', 'Uncommon').
card_artist('annihilate'/'C13', 'Kev Walker').
card_number('annihilate'/'C13', '68').
card_flavor_text('annihilate'/'C13', '\"Whatever Yawgmoth marks, dies. There is no escape.\"\n—Tsabo Tavoc, Phyrexian general').
card_multiverse_id('annihilate'/'C13', '376244').

card_in_set('arcane denial', 'C13').
card_original_type('arcane denial'/'C13', 'Instant').
card_original_text('arcane denial'/'C13', 'Counter target spell. Its controller may draw up to two cards at the beginning of the next turn\'s upkeep.You draw a card at the beginning of the next turn\'s upkeep.').
card_image_name('arcane denial'/'C13', 'arcane denial').
card_uid('arcane denial'/'C13', 'C13:Arcane Denial:arcane denial').
card_rarity('arcane denial'/'C13', 'Common').
card_artist('arcane denial'/'C13', 'Mark Zug').
card_number('arcane denial'/'C13', '28').
card_multiverse_id('arcane denial'/'C13', '376245').

card_in_set('arcane melee', 'C13').
card_original_type('arcane melee'/'C13', 'Enchantment').
card_original_text('arcane melee'/'C13', 'Instant and sorcery spells cost {2} less to cast.').
card_image_name('arcane melee'/'C13', 'arcane melee').
card_uid('arcane melee'/'C13', 'C13:Arcane Melee:arcane melee').
card_rarity('arcane melee'/'C13', 'Rare').
card_artist('arcane melee'/'C13', 'Jaime Jones').
card_number('arcane melee'/'C13', '29').
card_flavor_text('arcane melee'/'C13', 'Debates between wizards are never purely academic.').
card_multiverse_id('arcane melee'/'C13', '376246').

card_in_set('arcane sanctum', 'C13').
card_original_type('arcane sanctum'/'C13', 'Land').
card_original_text('arcane sanctum'/'C13', 'Arcane Sanctum enters the battlefield tapped.{T}: Add {W}, {U}, or {B} to your mana pool.').
card_image_name('arcane sanctum'/'C13', 'arcane sanctum').
card_uid('arcane sanctum'/'C13', 'C13:Arcane Sanctum:arcane sanctum').
card_rarity('arcane sanctum'/'C13', 'Uncommon').
card_artist('arcane sanctum'/'C13', 'Anthony Francisco').
card_number('arcane sanctum'/'C13', '273').
card_flavor_text('arcane sanctum'/'C13', '\"We must rely on our own knowledge, not on the dogma of the seekers or the mutterings of the sphinxes.\"\n—Tullus of Palandius').
card_multiverse_id('arcane sanctum'/'C13', '376247').

card_in_set('archangel', 'C13').
card_original_type('archangel'/'C13', 'Creature — Angel').
card_original_text('archangel'/'C13', 'Flying, vigilance').
card_image_name('archangel'/'C13', 'archangel').
card_uid('archangel'/'C13', 'C13:Archangel:archangel').
card_rarity('archangel'/'C13', 'Uncommon').
card_artist('archangel'/'C13', 'Quinton Hoover').
card_number('archangel'/'C13', '5').
card_flavor_text('archangel'/'C13', 'The sky rang with the cries of armored seraphs, and the darkness made a tactical retreat.').
card_multiverse_id('archangel'/'C13', '376248').

card_in_set('armillary sphere', 'C13').
card_original_type('armillary sphere'/'C13', 'Artifact').
card_original_text('armillary sphere'/'C13', '{2}, {T}, Sacrifice Armillary Sphere: Search your library for up to two basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_image_name('armillary sphere'/'C13', 'armillary sphere').
card_uid('armillary sphere'/'C13', 'C13:Armillary Sphere:armillary sphere').
card_rarity('armillary sphere'/'C13', 'Common').
card_artist('armillary sphere'/'C13', 'Franz Vohwinkel').
card_number('armillary sphere'/'C13', '235').
card_flavor_text('armillary sphere'/'C13', 'The mysterious purpose of two of the rings had eluded Esper mages—until now.').
card_multiverse_id('armillary sphere'/'C13', '376249').

card_in_set('army of the damned', 'C13').
card_original_type('army of the damned'/'C13', 'Sorcery').
card_original_text('army of the damned'/'C13', 'Put thirteen 2/2 black Zombie creature tokens onto the battlefield tapped.Flashback {7}{B}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('army of the damned'/'C13', 'army of the damned').
card_uid('army of the damned'/'C13', 'C13:Army of the Damned:army of the damned').
card_rarity('army of the damned'/'C13', 'Mythic Rare').
card_artist('army of the damned'/'C13', 'Ryan Pancoast').
card_number('army of the damned'/'C13', '69').
card_flavor_text('army of the damned'/'C13', 'Sometimes death comes knocking. Sometimes it tears down the walls.').
card_multiverse_id('army of the damned'/'C13', '376250').

card_in_set('augur of bolas', 'C13').
card_original_type('augur of bolas'/'C13', 'Creature — Merfolk Wizard').
card_original_text('augur of bolas'/'C13', 'When Augur of Bolas enters the battlefield, look at the top three cards of your library. You may reveal an instant or sorcery card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_image_name('augur of bolas'/'C13', 'augur of bolas').
card_uid('augur of bolas'/'C13', 'C13:Augur of Bolas:augur of bolas').
card_rarity('augur of bolas'/'C13', 'Uncommon').
card_artist('augur of bolas'/'C13', 'Slawomir Maniak').
card_number('augur of bolas'/'C13', '30').
card_multiverse_id('augur of bolas'/'C13', '376251').

card_in_set('augury adept', 'C13').
card_original_type('augury adept'/'C13', 'Creature — Kithkin Wizard').
card_original_text('augury adept'/'C13', 'Whenever Augury Adept deals combat damage to a player, reveal the top card of your library and put that card into your hand. You gain life equal to its converted mana cost.').
card_image_name('augury adept'/'C13', 'augury adept').
card_uid('augury adept'/'C13', 'C13:Augury Adept:augury adept').
card_rarity('augury adept'/'C13', 'Rare').
card_artist('augury adept'/'C13', 'Steve Prescott').
card_number('augury adept'/'C13', '227').
card_flavor_text('augury adept'/'C13', 'Moonstones grant limited foresight but unlimited advantages.').
card_multiverse_id('augury adept'/'C13', '376252').

card_in_set('avenger of zendikar', 'C13').
card_original_type('avenger of zendikar'/'C13', 'Creature — Elemental').
card_original_text('avenger of zendikar'/'C13', 'When Avenger of Zendikar enters the battlefield, put a 0/1 green Plant creature token onto the battlefield for each land you control.Landfall — Whenever a land enters the battlefield under your control, you may put a +1/+1 counter on each Plant creature you control.').
card_image_name('avenger of zendikar'/'C13', 'avenger of zendikar').
card_uid('avenger of zendikar'/'C13', 'C13:Avenger of Zendikar:avenger of zendikar').
card_rarity('avenger of zendikar'/'C13', 'Mythic Rare').
card_artist('avenger of zendikar'/'C13', 'Zoltan Boros & Gabor Szikszai').
card_number('avenger of zendikar'/'C13', '135').
card_multiverse_id('avenger of zendikar'/'C13', '376253').

card_in_set('azami, lady of scrolls', 'C13').
card_original_type('azami, lady of scrolls'/'C13', 'Legendary Creature — Human Wizard').
card_original_text('azami, lady of scrolls'/'C13', 'Tap an untapped Wizard you control: Draw a card.').
card_image_name('azami, lady of scrolls'/'C13', 'azami, lady of scrolls').
card_uid('azami, lady of scrolls'/'C13', 'C13:Azami, Lady of Scrolls:azami, lady of scrolls').
card_rarity('azami, lady of scrolls'/'C13', 'Rare').
card_artist('azami, lady of scrolls'/'C13', 'Ittoku').
card_number('azami, lady of scrolls'/'C13', '31').
card_flavor_text('azami, lady of scrolls'/'C13', '\"Choices belong to those with the luxuries of time and distance. We have neither. I recommend we proceed with the plan to destroy all shrines of the kami.\"\n—Lady Azami, letter to Sensei Hisoka').
card_multiverse_id('azami, lady of scrolls'/'C13', '376254').

card_in_set('azorius chancery', 'C13').
card_original_type('azorius chancery'/'C13', 'Land').
card_original_text('azorius chancery'/'C13', 'Azorius Chancery enters the battlefield tapped.When Azorius Chancery enters the battlefield, return a land you control to its owner\'s hand.{T}: Add {W}{U} to your mana pool.').
card_image_name('azorius chancery'/'C13', 'azorius chancery').
card_uid('azorius chancery'/'C13', 'C13:Azorius Chancery:azorius chancery').
card_rarity('azorius chancery'/'C13', 'Common').
card_artist('azorius chancery'/'C13', 'John Avon').
card_number('azorius chancery'/'C13', '274').
card_multiverse_id('azorius chancery'/'C13', '376255').

card_in_set('azorius guildgate', 'C13').
card_original_type('azorius guildgate'/'C13', 'Land — Gate').
card_original_text('azorius guildgate'/'C13', 'Azorius Guildgate enters the battlefield tapped.{T}: Add {W} or {U} to your mana pool.').
card_image_name('azorius guildgate'/'C13', 'azorius guildgate').
card_uid('azorius guildgate'/'C13', 'C13:Azorius Guildgate:azorius guildgate').
card_rarity('azorius guildgate'/'C13', 'Common').
card_artist('azorius guildgate'/'C13', 'Drew Baker').
card_number('azorius guildgate'/'C13', '275').
card_flavor_text('azorius guildgate'/'C13', 'Enter the Senate, the seat of justice and the foundation of Ravnican society.').
card_multiverse_id('azorius guildgate'/'C13', '376256').

card_in_set('azorius herald', 'C13').
card_original_type('azorius herald'/'C13', 'Creature — Spirit').
card_original_text('azorius herald'/'C13', 'Azorius Herald can\'t be blocked.When Azorius Herald enters the battlefield, you gain 4 life.When Azorius Herald enters the battlefield, sacrifice it unless {U} was spent to cast it.').
card_image_name('azorius herald'/'C13', 'azorius herald').
card_uid('azorius herald'/'C13', 'C13:Azorius Herald:azorius herald').
card_rarity('azorius herald'/'C13', 'Uncommon').
card_artist('azorius herald'/'C13', 'Justin Sweet').
card_number('azorius herald'/'C13', '6').
card_flavor_text('azorius herald'/'C13', '\"As peace should be—gentle yet unstoppable.\"\n—Augustin IV').
card_multiverse_id('azorius herald'/'C13', '376257').

card_in_set('azorius keyrune', 'C13').
card_original_type('azorius keyrune'/'C13', 'Artifact').
card_original_text('azorius keyrune'/'C13', '{T}: Add {W} or {U} to your mana pool.{W}{U}: Azorius Keyrune becomes a 2/2 white and blue Bird artifact creature with flying until end of turn.').
card_image_name('azorius keyrune'/'C13', 'azorius keyrune').
card_uid('azorius keyrune'/'C13', 'C13:Azorius Keyrune:azorius keyrune').
card_rarity('azorius keyrune'/'C13', 'Uncommon').
card_artist('azorius keyrune'/'C13', 'Daniel Ljunggren').
card_number('azorius keyrune'/'C13', '236').
card_flavor_text('azorius keyrune'/'C13', '\"The higher the mind soars, the greater its understanding of the law.\"\n—Isperia').
card_multiverse_id('azorius keyrune'/'C13', '376258').

card_in_set('baleful force', 'C13').
card_original_type('baleful force'/'C13', 'Creature — Elemental').
card_original_text('baleful force'/'C13', 'At the beginning of each upkeep, you draw a card and you lose 1 life.').
card_first_print('baleful force', 'C13').
card_image_name('baleful force'/'C13', 'baleful force').
card_uid('baleful force'/'C13', 'C13:Baleful Force:baleful force').
card_rarity('baleful force'/'C13', 'Rare').
card_artist('baleful force'/'C13', 'Eytan Zana').
card_number('baleful force'/'C13', '70').
card_flavor_text('baleful force'/'C13', '\"As with any malevolent being, the trick is knowing how long you can afford to keep it in your service.\"\n—Liliana Vess').
card_multiverse_id('baleful force'/'C13', '376259').

card_in_set('baleful strix', 'C13').
card_original_type('baleful strix'/'C13', 'Artifact Creature — Bird').
card_original_text('baleful strix'/'C13', 'Flying, deathtouchWhen Baleful Strix enters the battlefield, draw a card.').
card_image_name('baleful strix'/'C13', 'baleful strix').
card_uid('baleful strix'/'C13', 'C13:Baleful Strix:baleful strix').
card_rarity('baleful strix'/'C13', 'Uncommon').
card_artist('baleful strix'/'C13', 'Nils Hamm').
card_number('baleful strix'/'C13', '177').
card_flavor_text('baleful strix'/'C13', 'Its beak rends flesh and bone, exposing the tender marrow of dream.').
card_multiverse_id('baleful strix'/'C13', '376260').

card_in_set('baloth woodcrasher', 'C13').
card_original_type('baloth woodcrasher'/'C13', 'Creature — Beast').
card_original_text('baloth woodcrasher'/'C13', 'Landfall — Whenever a land enters the battlefield under your control, Baloth Woodcrasher gets +4/+4 and gains trample until end of turn.').
card_image_name('baloth woodcrasher'/'C13', 'baloth woodcrasher').
card_uid('baloth woodcrasher'/'C13', 'C13:Baloth Woodcrasher:baloth woodcrasher').
card_rarity('baloth woodcrasher'/'C13', 'Uncommon').
card_artist('baloth woodcrasher'/'C13', 'Zoltan Boros & Gabor Szikszai').
card_number('baloth woodcrasher'/'C13', '136').
card_flavor_text('baloth woodcrasher'/'C13', 'Its insatiable hunger quickly depletes a region of prey. It must migrate from place to place to feed its massive bulk.').
card_multiverse_id('baloth woodcrasher'/'C13', '376261').

card_in_set('bane of progress', 'C13').
card_original_type('bane of progress'/'C13', 'Creature — Elemental').
card_original_text('bane of progress'/'C13', 'When Bane of Progress enters the battlefield, destroy all artifacts and enchantments. Put a +1/+1 counter on Bane of Progress for each permanent destroyed this way.').
card_first_print('bane of progress', 'C13').
card_image_name('bane of progress'/'C13', 'bane of progress').
card_uid('bane of progress'/'C13', 'C13:Bane of Progress:bane of progress').
card_rarity('bane of progress'/'C13', 'Rare').
card_artist('bane of progress'/'C13', 'Lars Grant-West').
card_number('bane of progress'/'C13', '137').
card_flavor_text('bane of progress'/'C13', 'It sees shaped stone and carved wood as mutilations.').
card_multiverse_id('bane of progress'/'C13', '376262').

card_in_set('bant panorama', 'C13').
card_original_type('bant panorama'/'C13', 'Land').
card_original_text('bant panorama'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}, Sacrifice Bant Panorama: Search your library for a basic Forest, Plains, or Island card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('bant panorama'/'C13', 'bant panorama').
card_uid('bant panorama'/'C13', 'C13:Bant Panorama:bant panorama').
card_rarity('bant panorama'/'C13', 'Common').
card_artist('bant panorama'/'C13', 'Donato Giancola').
card_number('bant panorama'/'C13', '276').
card_flavor_text('bant panorama'/'C13', 'Bant\'s hearts are as pure as its air and as bright as its skies.').
card_multiverse_id('bant panorama'/'C13', '376263').

card_in_set('barren moor', 'C13').
card_original_type('barren moor'/'C13', 'Land').
card_original_text('barren moor'/'C13', 'Barren Moor enters the battlefield tapped.{T}: Add {B} to your mana pool.Cycling {B} ({B}, Discard this card: Draw a card.)').
card_image_name('barren moor'/'C13', 'barren moor').
card_uid('barren moor'/'C13', 'C13:Barren Moor:barren moor').
card_rarity('barren moor'/'C13', 'Common').
card_artist('barren moor'/'C13', 'Heather Hudson').
card_number('barren moor'/'C13', '277').
card_multiverse_id('barren moor'/'C13', '376264').

card_in_set('basalt monolith', 'C13').
card_original_type('basalt monolith'/'C13', 'Artifact').
card_original_text('basalt monolith'/'C13', 'Basalt Monolith doesn\'t untap during your untap step.{T}: Add {3} to your mana pool.{3}: Untap Basalt Monolith.').
card_image_name('basalt monolith'/'C13', 'basalt monolith').
card_uid('basalt monolith'/'C13', 'C13:Basalt Monolith:basalt monolith').
card_rarity('basalt monolith'/'C13', 'Uncommon').
card_artist('basalt monolith'/'C13', 'Yeong-Hao Han').
card_number('basalt monolith'/'C13', '237').
card_multiverse_id('basalt monolith'/'C13', '376265').

card_in_set('behemoth sledge', 'C13').
card_original_type('behemoth sledge'/'C13', 'Artifact — Equipment').
card_original_text('behemoth sledge'/'C13', 'Equipped creature gets +2/+2 and has lifelink and trample.Equip {3}').
card_image_name('behemoth sledge'/'C13', 'behemoth sledge').
card_uid('behemoth sledge'/'C13', 'C13:Behemoth Sledge:behemoth sledge').
card_rarity('behemoth sledge'/'C13', 'Uncommon').
card_artist('behemoth sledge'/'C13', 'Steve Prescott').
card_number('behemoth sledge'/'C13', '178').
card_flavor_text('behemoth sledge'/'C13', 'Those who worship the great gargantuans could hardly be expected to fight with a subtle weapon.').
card_multiverse_id('behemoth sledge'/'C13', '376266').

card_in_set('blood rites', 'C13').
card_original_type('blood rites'/'C13', 'Enchantment').
card_original_text('blood rites'/'C13', '{1}{R}, Sacrifice a creature: Blood Rites deals 2 damage to target creature or player.').
card_image_name('blood rites'/'C13', 'blood rites').
card_uid('blood rites'/'C13', 'C13:Blood Rites:blood rites').
card_rarity('blood rites'/'C13', 'Uncommon').
card_artist('blood rites'/'C13', 'Raymond Swanland').
card_number('blood rites'/'C13', '101').
card_flavor_text('blood rites'/'C13', 'In war, the best leaders are open to all tactics.').
card_multiverse_id('blood rites'/'C13', '376267').

card_in_set('blue sun\'s zenith', 'C13').
card_original_type('blue sun\'s zenith'/'C13', 'Instant').
card_original_text('blue sun\'s zenith'/'C13', 'Target player draws X cards. Shuffle Blue Sun\'s Zenith into its owner\'s library.').
card_image_name('blue sun\'s zenith'/'C13', 'blue sun\'s zenith').
card_uid('blue sun\'s zenith'/'C13', 'C13:Blue Sun\'s Zenith:blue sun\'s zenith').
card_rarity('blue sun\'s zenith'/'C13', 'Rare').
card_artist('blue sun\'s zenith'/'C13', 'Izzy').
card_number('blue sun\'s zenith'/'C13', '32').
card_flavor_text('blue sun\'s zenith'/'C13', '\"The Origin Query will wait. We must ensure we survive to return to it.\"\n—Pelyus, vedalken ordinar').
card_multiverse_id('blue sun\'s zenith'/'C13', '376268').

card_in_set('bojuka bog', 'C13').
card_original_type('bojuka bog'/'C13', 'Land').
card_original_text('bojuka bog'/'C13', 'Bojuka Bog enters the battlefield tapped.When Bojuka Bog enters the battlefield, exile all cards from target player\'s graveyard.{T}: Add {B} to your mana pool.').
card_image_name('bojuka bog'/'C13', 'bojuka bog').
card_uid('bojuka bog'/'C13', 'C13:Bojuka Bog:bojuka bog').
card_rarity('bojuka bog'/'C13', 'Common').
card_artist('bojuka bog'/'C13', 'Howard Lyon').
card_number('bojuka bog'/'C13', '278').
card_multiverse_id('bojuka bog'/'C13', '376269').

card_in_set('boros charm', 'C13').
card_original_type('boros charm'/'C13', 'Instant').
card_original_text('boros charm'/'C13', 'Choose one — Boros Charm deals 4 damage to target player; or permanents you control gain indestructible until end of turn; or target creature gains double strike until end of turn.').
card_image_name('boros charm'/'C13', 'boros charm').
card_uid('boros charm'/'C13', 'C13:Boros Charm:boros charm').
card_rarity('boros charm'/'C13', 'Uncommon').
card_artist('boros charm'/'C13', 'Zoltan Boros').
card_number('boros charm'/'C13', '179').
card_flavor_text('boros charm'/'C13', '\"Practice compassion and mercy. But know when they must end.\"\n—Aurelia').
card_multiverse_id('boros charm'/'C13', '376270').

card_in_set('boros garrison', 'C13').
card_original_type('boros garrison'/'C13', 'Land').
card_original_text('boros garrison'/'C13', 'Boros Garrison enters the battlefield tapped.When Boros Garrison enters the battlefield, return a land you control to its owner\'s hand.{T}: Add {R}{W} to your mana pool.').
card_image_name('boros garrison'/'C13', 'boros garrison').
card_uid('boros garrison'/'C13', 'C13:Boros Garrison:boros garrison').
card_rarity('boros garrison'/'C13', 'Common').
card_artist('boros garrison'/'C13', 'John Avon').
card_number('boros garrison'/'C13', '279').
card_multiverse_id('boros garrison'/'C13', '376271').

card_in_set('boros guildgate', 'C13').
card_original_type('boros guildgate'/'C13', 'Land — Gate').
card_original_text('boros guildgate'/'C13', 'Boros Guildgate enters the battlefield tapped.{T}: Add {R} or {W} to your mana pool.').
card_image_name('boros guildgate'/'C13', 'boros guildgate').
card_uid('boros guildgate'/'C13', 'C13:Boros Guildgate:boros guildgate').
card_rarity('boros guildgate'/'C13', 'Common').
card_artist('boros guildgate'/'C13', 'Noah Bradley').
card_number('boros guildgate'/'C13', '280').
card_flavor_text('boros guildgate'/'C13', 'Enter with merciful hearts. Exit with battle cries.').
card_multiverse_id('boros guildgate'/'C13', '376272').

card_in_set('borrowing 100,000 arrows', 'C13').
card_original_type('borrowing 100,000 arrows'/'C13', 'Sorcery').
card_original_text('borrowing 100,000 arrows'/'C13', 'Draw a card for each tapped creature target opponent controls.').
card_image_name('borrowing 100,000 arrows'/'C13', 'borrowing 100,000 arrows').
card_uid('borrowing 100,000 arrows'/'C13', 'C13:Borrowing 100,000 Arrows:borrowing 100,000 arrows').
card_rarity('borrowing 100,000 arrows'/'C13', 'Uncommon').
card_artist('borrowing 100,000 arrows'/'C13', 'Song Shikai').
card_number('borrowing 100,000 arrows'/'C13', '33').
card_flavor_text('borrowing 100,000 arrows'/'C13', 'Kongming and Lu Su tricked Wei troops into shooting over 100,000 arrows at them to later use against the Wei at Red Cliffs.').
card_multiverse_id('borrowing 100,000 arrows'/'C13', '376273').

card_in_set('brilliant plan', 'C13').
card_original_type('brilliant plan'/'C13', 'Sorcery').
card_original_text('brilliant plan'/'C13', 'Draw three cards.').
card_image_name('brilliant plan'/'C13', 'brilliant plan').
card_uid('brilliant plan'/'C13', 'C13:Brilliant Plan:brilliant plan').
card_rarity('brilliant plan'/'C13', 'Uncommon').
card_artist('brilliant plan'/'C13', 'Song Shikai').
card_number('brilliant plan'/'C13', '34').
card_flavor_text('brilliant plan'/'C13', 'At Red Cliffs, Kongming and Zhou Yu each wrote his plan for defeating Wei on the palm of his hand. They laughed as they both revealed the same word, \"Fire.\"').
card_multiverse_id('brilliant plan'/'C13', '376274').

card_in_set('brooding saurian', 'C13').
card_original_type('brooding saurian'/'C13', 'Creature — Lizard').
card_original_text('brooding saurian'/'C13', 'At the beginning of each end step, each player gains control of all nontoken permanents he or she owns.').
card_image_name('brooding saurian'/'C13', 'brooding saurian').
card_uid('brooding saurian'/'C13', 'C13:Brooding Saurian:brooding saurian').
card_rarity('brooding saurian'/'C13', 'Rare').
card_artist('brooding saurian'/'C13', 'rk post').
card_number('brooding saurian'/'C13', '138').
card_flavor_text('brooding saurian'/'C13', '\"I have been trapped in the lizard\'s lair for days. It seems to regard me as its child and will not let me leave.\"\n—Aevar Borg, northern guide, journal entry').
card_multiverse_id('brooding saurian'/'C13', '376275').

card_in_set('capricious efreet', 'C13').
card_original_type('capricious efreet'/'C13', 'Creature — Efreet').
card_original_text('capricious efreet'/'C13', 'At the beginning of your upkeep, choose target nonland permanent you control and up to two target nonland permanents you don\'t control. Destroy one of them at random.').
card_image_name('capricious efreet'/'C13', 'capricious efreet').
card_uid('capricious efreet'/'C13', 'C13:Capricious Efreet:capricious efreet').
card_rarity('capricious efreet'/'C13', 'Rare').
card_artist('capricious efreet'/'C13', 'Justin Sweet').
card_number('capricious efreet'/'C13', '102').
card_flavor_text('capricious efreet'/'C13', '\"You wish for great destruction? It is done, my master.\"').
card_multiverse_id('capricious efreet'/'C13', '376276').

card_in_set('carnage altar', 'C13').
card_original_type('carnage altar'/'C13', 'Artifact').
card_original_text('carnage altar'/'C13', '{3}, Sacrifice a creature: Draw a card.').
card_image_name('carnage altar'/'C13', 'carnage altar').
card_uid('carnage altar'/'C13', 'C13:Carnage Altar:carnage altar').
card_rarity('carnage altar'/'C13', 'Uncommon').
card_artist('carnage altar'/'C13', 'James Paick').
card_number('carnage altar'/'C13', '238').
card_flavor_text('carnage altar'/'C13', '\"In these bloodstains I will find the fingerprints of our oppressors.\"\n—Anowon, the Ruin Sage').
card_multiverse_id('carnage altar'/'C13', '376277').

card_in_set('charmbreaker devils', 'C13').
card_original_type('charmbreaker devils'/'C13', 'Creature — Devil').
card_original_text('charmbreaker devils'/'C13', 'At the beginning of your upkeep, return an instant or sorcery card at random from your graveyard to your hand.Whenever you cast an instant or sorcery spell, Charmbreaker Devils gets +4/+0 until end of turn.').
card_image_name('charmbreaker devils'/'C13', 'charmbreaker devils').
card_uid('charmbreaker devils'/'C13', 'C13:Charmbreaker Devils:charmbreaker devils').
card_rarity('charmbreaker devils'/'C13', 'Rare').
card_artist('charmbreaker devils'/'C13', 'Dan Scott').
card_number('charmbreaker devils'/'C13', '103').
card_multiverse_id('charmbreaker devils'/'C13', '376278').

card_in_set('charnelhoard wurm', 'C13').
card_original_type('charnelhoard wurm'/'C13', 'Creature — Wurm').
card_original_text('charnelhoard wurm'/'C13', 'TrampleWhenever Charnelhoard Wurm deals damage to an opponent, you may return target card from your graveyard to your hand.').
card_image_name('charnelhoard wurm'/'C13', 'charnelhoard wurm').
card_uid('charnelhoard wurm'/'C13', 'C13:Charnelhoard Wurm:charnelhoard wurm').
card_rarity('charnelhoard wurm'/'C13', 'Rare').
card_artist('charnelhoard wurm'/'C13', 'Lars Grant-West').
card_number('charnelhoard wurm'/'C13', '180').
card_flavor_text('charnelhoard wurm'/'C13', 'Jund\'s dragons hoard only sangrite crystals. Its wurms aren\'t so picky.').
card_multiverse_id('charnelhoard wurm'/'C13', '376279').

card_in_set('command tower', 'C13').
card_original_type('command tower'/'C13', 'Land').
card_original_text('command tower'/'C13', '{T}: Add to your mana pool one mana of any color in your commander\'s color identity.').
card_image_name('command tower'/'C13', 'command tower').
card_uid('command tower'/'C13', 'C13:Command Tower:command tower').
card_rarity('command tower'/'C13', 'Common').
card_artist('command tower'/'C13', 'Ryan Yee').
card_number('command tower'/'C13', '281').
card_flavor_text('command tower'/'C13', 'When defeat is near and guidance is scarce, all eyes look in one direction.').
card_multiverse_id('command tower'/'C13', '376280').

card_in_set('conjurer\'s closet', 'C13').
card_original_type('conjurer\'s closet'/'C13', 'Artifact').
card_original_text('conjurer\'s closet'/'C13', 'At the beginning of your end step, you may exile target creature you control, then return that card to the battlefield under your control.').
card_image_name('conjurer\'s closet'/'C13', 'conjurer\'s closet').
card_uid('conjurer\'s closet'/'C13', 'C13:Conjurer\'s Closet:conjurer\'s closet').
card_rarity('conjurer\'s closet'/'C13', 'Rare').
card_artist('conjurer\'s closet'/'C13', 'Jason Felix').
card_number('conjurer\'s closet'/'C13', '239').
card_flavor_text('conjurer\'s closet'/'C13', '\"Tomorrow wears yesterday\'s face.\"\n—Kordel the Cryptic').
card_multiverse_id('conjurer\'s closet'/'C13', '376281').

card_in_set('contested cliffs', 'C13').
card_original_type('contested cliffs'/'C13', 'Land').
card_original_text('contested cliffs'/'C13', '{T}: Add {1} to your mana pool.{R}{G}, {T}: Target Beast creature you control fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_image_name('contested cliffs'/'C13', 'contested cliffs').
card_uid('contested cliffs'/'C13', 'C13:Contested Cliffs:contested cliffs').
card_rarity('contested cliffs'/'C13', 'Rare').
card_artist('contested cliffs'/'C13', 'Anthony S. Waters').
card_number('contested cliffs'/'C13', '282').
card_multiverse_id('contested cliffs'/'C13', '376282').

card_in_set('control magic', 'C13').
card_original_type('control magic'/'C13', 'Enchantment — Aura').
card_original_text('control magic'/'C13', 'Enchant creatureYou control enchanted creature.').
card_image_name('control magic'/'C13', 'control magic').
card_uid('control magic'/'C13', 'C13:Control Magic:control magic').
card_rarity('control magic'/'C13', 'Uncommon').
card_artist('control magic'/'C13', 'Clint Cearley').
card_number('control magic'/'C13', '35').
card_flavor_text('control magic'/'C13', '\"Do as I think, not as I do.\"\n—Jace Beleren').
card_multiverse_id('control magic'/'C13', '376283').

card_in_set('cradle of vitality', 'C13').
card_original_type('cradle of vitality'/'C13', 'Enchantment').
card_original_text('cradle of vitality'/'C13', 'Whenever you gain life, you may pay {1}{W}. If you do, put a +1/+1 counter on target creature for each 1 life you gained.').
card_image_name('cradle of vitality'/'C13', 'cradle of vitality').
card_uid('cradle of vitality'/'C13', 'C13:Cradle of Vitality:cradle of vitality').
card_rarity('cradle of vitality'/'C13', 'Rare').
card_artist('cradle of vitality'/'C13', 'Trevor Hairsine').
card_number('cradle of vitality'/'C13', '7').
card_flavor_text('cradle of vitality'/'C13', 'Naya\'s trees grow tall and sturdy. Their foliage intertwines to form dewcups, rainwater pools where the elves gather to celebrate life.').
card_multiverse_id('cradle of vitality'/'C13', '376284').

card_in_set('crater hellion', 'C13').
card_original_type('crater hellion'/'C13', 'Creature — Hellion Beast').
card_original_text('crater hellion'/'C13', 'Echo {4}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)When Crater Hellion enters the battlefield, it deals 4 damage to each other creature.').
card_image_name('crater hellion'/'C13', 'crater hellion').
card_uid('crater hellion'/'C13', 'C13:Crater Hellion:crater hellion').
card_rarity('crater hellion'/'C13', 'Rare').
card_artist('crater hellion'/'C13', 'Daren Bader').
card_number('crater hellion'/'C13', '104').
card_multiverse_id('crater hellion'/'C13', '376285').

card_in_set('crawlspace', 'C13').
card_original_type('crawlspace'/'C13', 'Artifact').
card_original_text('crawlspace'/'C13', 'No more than two creatures can attack you each combat.').
card_image_name('crawlspace'/'C13', 'crawlspace').
card_uid('crawlspace'/'C13', 'C13:Crawlspace:crawlspace').
card_rarity('crawlspace'/'C13', 'Rare').
card_artist('crawlspace'/'C13', 'Franz Vohwinkel').
card_number('crawlspace'/'C13', '240').
card_flavor_text('crawlspace'/'C13', 'Lord Rondor arrived for battle with sixty war elephants, ten catapults, and two hundred of his finest archers. Then he saw the battlefield . . .').
card_multiverse_id('crawlspace'/'C13', '376286').

card_in_set('crosis\'s charm', 'C13').
card_original_type('crosis\'s charm'/'C13', 'Instant').
card_original_text('crosis\'s charm'/'C13', 'Choose one — Return target permanent to its owner\'s hand; or destroy target nonblack creature, and it can\'t be regenerated; or destroy target artifact.').
card_image_name('crosis\'s charm'/'C13', 'crosis\'s charm').
card_uid('crosis\'s charm'/'C13', 'C13:Crosis\'s Charm:crosis\'s charm').
card_rarity('crosis\'s charm'/'C13', 'Uncommon').
card_artist('crosis\'s charm'/'C13', 'Marco Nelor').
card_number('crosis\'s charm'/'C13', '181').
card_flavor_text('crosis\'s charm'/'C13', 'A pretty bauble of persecution.').
card_multiverse_id('crosis\'s charm'/'C13', '376287').

card_in_set('cruel ultimatum', 'C13').
card_original_type('cruel ultimatum'/'C13', 'Sorcery').
card_original_text('cruel ultimatum'/'C13', 'Target opponent sacrifices a creature, discards three cards, then loses 5 life. You return a creature card from your graveyard to your hand, draw three cards, then gain 5 life.').
card_image_name('cruel ultimatum'/'C13', 'cruel ultimatum').
card_uid('cruel ultimatum'/'C13', 'C13:Cruel Ultimatum:cruel ultimatum').
card_rarity('cruel ultimatum'/'C13', 'Rare').
card_artist('cruel ultimatum'/'C13', 'Todd Lockwood').
card_number('cruel ultimatum'/'C13', '182').
card_flavor_text('cruel ultimatum'/'C13', 'There is always a greater power.').
card_multiverse_id('cruel ultimatum'/'C13', '376288').

card_in_set('crumbling necropolis', 'C13').
card_original_type('crumbling necropolis'/'C13', 'Land').
card_original_text('crumbling necropolis'/'C13', 'Crumbling Necropolis enters the battlefield tapped.{T}: Add {U}, {B}, or {R} to your mana pool.').
card_image_name('crumbling necropolis'/'C13', 'crumbling necropolis').
card_uid('crumbling necropolis'/'C13', 'C13:Crumbling Necropolis:crumbling necropolis').
card_rarity('crumbling necropolis'/'C13', 'Uncommon').
card_artist('crumbling necropolis'/'C13', 'Dave Kendall').
card_number('crumbling necropolis'/'C13', '283').
card_flavor_text('crumbling necropolis'/'C13', '\"They say the ruins of Sedraxis were once a shining capital in Vithia. Now it is a blight, a place to be avoided by the living.\"\n—Olcot, Rider of Joffik').
card_multiverse_id('crumbling necropolis'/'C13', '376289').

card_in_set('cultivate', 'C13').
card_original_type('cultivate'/'C13', 'Sorcery').
card_original_text('cultivate'/'C13', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_image_name('cultivate'/'C13', 'cultivate').
card_uid('cultivate'/'C13', 'C13:Cultivate:cultivate').
card_rarity('cultivate'/'C13', 'Common').
card_artist('cultivate'/'C13', 'Anthony Palumbo').
card_number('cultivate'/'C13', '139').
card_flavor_text('cultivate'/'C13', 'All seeds share a common bond, calling to each other across infinity.').
card_multiverse_id('cultivate'/'C13', '376290').

card_in_set('curse of chaos', 'C13').
card_original_type('curse of chaos'/'C13', 'Enchantment — Aura Curse').
card_original_text('curse of chaos'/'C13', 'Enchant playerWhenever a player attacks enchanted player with one or more creatures, that attacking player may discard a card. If the player does, he or she draws a card.').
card_first_print('curse of chaos', 'C13').
card_image_name('curse of chaos'/'C13', 'curse of chaos').
card_uid('curse of chaos'/'C13', 'C13:Curse of Chaos:curse of chaos').
card_rarity('curse of chaos'/'C13', 'Uncommon').
card_artist('curse of chaos'/'C13', 'Jason A. Engle').
card_number('curse of chaos'/'C13', '105').
card_flavor_text('curse of chaos'/'C13', '\"Let them cry for mercy. The only one who can hear them is me.\"\n—Siglind the Conqueror').
card_multiverse_id('curse of chaos'/'C13', '376291').

card_in_set('curse of inertia', 'C13').
card_original_type('curse of inertia'/'C13', 'Enchantment — Aura Curse').
card_original_text('curse of inertia'/'C13', 'Enchant playerWhenever a player attacks enchanted player with one or more creatures, that attacking player may tap or untap target permanent of his or her choice.').
card_first_print('curse of inertia', 'C13').
card_image_name('curse of inertia'/'C13', 'curse of inertia').
card_uid('curse of inertia'/'C13', 'C13:Curse of Inertia:curse of inertia').
card_rarity('curse of inertia'/'C13', 'Uncommon').
card_artist('curse of inertia'/'C13', 'Jasper Sandner').
card_number('curse of inertia'/'C13', '36').
card_flavor_text('curse of inertia'/'C13', '\"As long as I pull the strings, you will dance.\"\n—Tezzeret').
card_multiverse_id('curse of inertia'/'C13', '376292').

card_in_set('curse of predation', 'C13').
card_original_type('curse of predation'/'C13', 'Enchantment — Aura Curse').
card_original_text('curse of predation'/'C13', 'Enchant playerWhenever a creature attacks enchanted player, put a +1/+1 counter on it.').
card_first_print('curse of predation', 'C13').
card_image_name('curse of predation'/'C13', 'curse of predation').
card_uid('curse of predation'/'C13', 'C13:Curse of Predation:curse of predation').
card_rarity('curse of predation'/'C13', 'Uncommon').
card_artist('curse of predation'/'C13', 'Jack Wang').
card_number('curse of predation'/'C13', '140').
card_flavor_text('curse of predation'/'C13', '\"Hunters feed on the fear of their prey.\"\n—Garruk Wildspeaker').
card_multiverse_id('curse of predation'/'C13', '376293').

card_in_set('curse of shallow graves', 'C13').
card_original_type('curse of shallow graves'/'C13', 'Enchantment — Aura Curse').
card_original_text('curse of shallow graves'/'C13', 'Enchant playerWhenever a player attacks enchanted player with one or more creatures, that attacking player may put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_first_print('curse of shallow graves', 'C13').
card_image_name('curse of shallow graves'/'C13', 'curse of shallow graves').
card_uid('curse of shallow graves'/'C13', 'C13:Curse of Shallow Graves:curse of shallow graves').
card_rarity('curse of shallow graves'/'C13', 'Uncommon').
card_artist('curse of shallow graves'/'C13', 'Karla Ortiz').
card_number('curse of shallow graves'/'C13', '71').
card_multiverse_id('curse of shallow graves'/'C13', '376294').

card_in_set('curse of the forsaken', 'C13').
card_original_type('curse of the forsaken'/'C13', 'Enchantment — Aura Curse').
card_original_text('curse of the forsaken'/'C13', 'Enchant playerWhenever a creature attacks enchanted player, its controller gains 1 life.').
card_first_print('curse of the forsaken', 'C13').
card_image_name('curse of the forsaken'/'C13', 'curse of the forsaken').
card_uid('curse of the forsaken'/'C13', 'C13:Curse of the Forsaken:curse of the forsaken').
card_rarity('curse of the forsaken'/'C13', 'Uncommon').
card_artist('curse of the forsaken'/'C13', 'William Wu').
card_number('curse of the forsaken'/'C13', '8').
card_flavor_text('curse of the forsaken'/'C13', '\"Purge the damned, for they have no place among the righteous!\"\n—Seeta of Thraben').
card_multiverse_id('curse of the forsaken'/'C13', '376295').

card_in_set('darksteel ingot', 'C13').
card_original_type('darksteel ingot'/'C13', 'Artifact').
card_original_text('darksteel ingot'/'C13', 'Indestructible{T}: Add one mana of any color to your mana pool.').
card_image_name('darksteel ingot'/'C13', 'darksteel ingot').
card_uid('darksteel ingot'/'C13', 'C13:Darksteel Ingot:darksteel ingot').
card_rarity('darksteel ingot'/'C13', 'Uncommon').
card_artist('darksteel ingot'/'C13', 'Martina Pilcerova').
card_number('darksteel ingot'/'C13', '241').
card_flavor_text('darksteel ingot'/'C13', '\"It reflects the purity of Mirrodin-that-was.\"\n—Koth of the Hammer').
card_multiverse_id('darksteel ingot'/'C13', '376296').

card_in_set('darksteel mutation', 'C13').
card_original_type('darksteel mutation'/'C13', 'Enchantment — Aura').
card_original_text('darksteel mutation'/'C13', 'Enchant creatureEnchanted creature is a 0/1 Insect artifact creature with indestructible and loses all other abilities.').
card_first_print('darksteel mutation', 'C13').
card_image_name('darksteel mutation'/'C13', 'darksteel mutation').
card_uid('darksteel mutation'/'C13', 'C13:Darksteel Mutation:darksteel mutation').
card_rarity('darksteel mutation'/'C13', 'Uncommon').
card_artist('darksteel mutation'/'C13', 'Daniel Ljunggren').
card_number('darksteel mutation'/'C13', '9').
card_flavor_text('darksteel mutation'/'C13', 'Infinitely powerless.').
card_multiverse_id('darksteel mutation'/'C13', '376297').

card_in_set('deadwood treefolk', 'C13').
card_original_type('deadwood treefolk'/'C13', 'Creature — Treefolk').
card_original_text('deadwood treefolk'/'C13', 'Vanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)When Deadwood Treefolk enters the battlefield or leaves the battlefield, return another target creature card from your graveyard to your hand.').
card_image_name('deadwood treefolk'/'C13', 'deadwood treefolk').
card_uid('deadwood treefolk'/'C13', 'C13:Deadwood Treefolk:deadwood treefolk').
card_rarity('deadwood treefolk'/'C13', 'Uncommon').
card_artist('deadwood treefolk'/'C13', 'Don Hazeltine').
card_number('deadwood treefolk'/'C13', '141').
card_multiverse_id('deadwood treefolk'/'C13', '376298').

card_in_set('death grasp', 'C13').
card_original_type('death grasp'/'C13', 'Sorcery').
card_original_text('death grasp'/'C13', 'Death Grasp deals X damage to target creature or player. You gain X life.').
card_image_name('death grasp'/'C13', 'death grasp').
card_uid('death grasp'/'C13', 'C13:Death Grasp:death grasp').
card_rarity('death grasp'/'C13', 'Rare').
card_artist('death grasp'/'C13', 'Raymond Swanland').
card_number('death grasp'/'C13', '183').
card_flavor_text('death grasp'/'C13', 'Plate mail, the skin of the chest, the rib cage, the heart—all useless against the mage who can reach directly for the soul.').
card_multiverse_id('death grasp'/'C13', '376299').

card_in_set('deathbringer thoctar', 'C13').
card_original_type('deathbringer thoctar'/'C13', 'Creature — Zombie Beast').
card_original_text('deathbringer thoctar'/'C13', 'Whenever another creature dies, you may put a +1/+1 counter on Deathbringer Thoctar.Remove a +1/+1 counter from Deathbringer Thoctar: Deathbringer Thoctar deals 1 damage to target creature or player.').
card_image_name('deathbringer thoctar'/'C13', 'deathbringer thoctar').
card_uid('deathbringer thoctar'/'C13', 'C13:Deathbringer Thoctar:deathbringer thoctar').
card_rarity('deathbringer thoctar'/'C13', 'Rare').
card_artist('deathbringer thoctar'/'C13', 'Karl Kopinski').
card_number('deathbringer thoctar'/'C13', '184').
card_multiverse_id('deathbringer thoctar'/'C13', '376300').

card_in_set('deceiver exarch', 'C13').
card_original_type('deceiver exarch'/'C13', 'Creature — Cleric').
card_original_text('deceiver exarch'/'C13', 'Flash (You may cast this spell any time you could cast an instant.)When Deceiver Exarch enters the battlefield, choose one — Untap target permanent you control; or tap target permanent an opponent controls.').
card_image_name('deceiver exarch'/'C13', 'deceiver exarch').
card_uid('deceiver exarch'/'C13', 'C13:Deceiver Exarch:deceiver exarch').
card_rarity('deceiver exarch'/'C13', 'Uncommon').
card_artist('deceiver exarch'/'C13', 'Izzy').
card_number('deceiver exarch'/'C13', '37').
card_multiverse_id('deceiver exarch'/'C13', '376301').

card_in_set('decree of pain', 'C13').
card_original_type('decree of pain'/'C13', 'Sorcery').
card_original_text('decree of pain'/'C13', 'Destroy all creatures. They can\'t be regenerated. Draw a card for each creature destroyed this way.Cycling {3}{B}{B} ({3}{B}{B}, Discard this card: Draw a card.)When you cycle Decree of Pain, all creatures get -2/-2 until end of turn.').
card_image_name('decree of pain'/'C13', 'decree of pain').
card_uid('decree of pain'/'C13', 'C13:Decree of Pain:decree of pain').
card_rarity('decree of pain'/'C13', 'Rare').
card_artist('decree of pain'/'C13', 'Mathias Kollros').
card_number('decree of pain'/'C13', '72').
card_multiverse_id('decree of pain'/'C13', '376302').

card_in_set('deep analysis', 'C13').
card_original_type('deep analysis'/'C13', 'Sorcery').
card_original_text('deep analysis'/'C13', 'Target player draws two cards.Flashback—{1}{U}, Pay 3 life. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_image_name('deep analysis'/'C13', 'deep analysis').
card_uid('deep analysis'/'C13', 'C13:Deep Analysis:deep analysis').
card_rarity('deep analysis'/'C13', 'Common').
card_artist('deep analysis'/'C13', 'Daren Bader').
card_number('deep analysis'/'C13', '38').
card_flavor_text('deep analysis'/'C13', '\"The specimen seems to be broken.\"').
card_multiverse_id('deep analysis'/'C13', '376303').

card_in_set('deepfire elemental', 'C13').
card_original_type('deepfire elemental'/'C13', 'Creature — Elemental').
card_original_text('deepfire elemental'/'C13', '{X}{X}{1}: Destroy target artifact or creature with converted mana cost X.').
card_image_name('deepfire elemental'/'C13', 'deepfire elemental').
card_uid('deepfire elemental'/'C13', 'C13:Deepfire Elemental:deepfire elemental').
card_rarity('deepfire elemental'/'C13', 'Uncommon').
card_artist('deepfire elemental'/'C13', 'Joel Thomas').
card_number('deepfire elemental'/'C13', '185').
card_flavor_text('deepfire elemental'/'C13', '\"There are deep, deep places in this world that have long burned hot with little regard for the whitening of the earth\'s thin skin.\"\n—Sek\'Kuar, Deathkeeper').
card_multiverse_id('deepfire elemental'/'C13', '376304').

card_in_set('derevi, empyrial tactician', 'C13').
card_original_type('derevi, empyrial tactician'/'C13', 'Legendary Creature — Bird Wizard').
card_original_text('derevi, empyrial tactician'/'C13', 'FlyingWhenever Derevi, Empyrial Tactician enters the battlefield or a creature you control deals combat damage to a player, you may tap or untap target permanent.{1}{G}{W}{U}: Put Derevi onto the battlefield from the command zone.').
card_first_print('derevi, empyrial tactician', 'C13').
card_image_name('derevi, empyrial tactician'/'C13', 'derevi, empyrial tactician').
card_uid('derevi, empyrial tactician'/'C13', 'C13:Derevi, Empyrial Tactician:derevi, empyrial tactician').
card_rarity('derevi, empyrial tactician'/'C13', 'Mythic Rare').
card_artist('derevi, empyrial tactician'/'C13', 'Michael Komarck').
card_number('derevi, empyrial tactician'/'C13', '186').
card_multiverse_id('derevi, empyrial tactician'/'C13', '376305').

card_in_set('dimir guildgate', 'C13').
card_original_type('dimir guildgate'/'C13', 'Land — Gate').
card_original_text('dimir guildgate'/'C13', 'Dimir Guildgate enters the battlefield tapped.{T}: Add {U} or {B} to your mana pool.').
card_image_name('dimir guildgate'/'C13', 'dimir guildgate').
card_uid('dimir guildgate'/'C13', 'C13:Dimir Guildgate:dimir guildgate').
card_rarity('dimir guildgate'/'C13', 'Common').
card_artist('dimir guildgate'/'C13', 'Cliff Childs').
card_number('dimir guildgate'/'C13', '284').
card_flavor_text('dimir guildgate'/'C13', 'Enter and become a mastermind of spies, lies, and deception.').
card_multiverse_id('dimir guildgate'/'C13', '376306').

card_in_set('dirge of dread', 'C13').
card_original_type('dirge of dread'/'C13', 'Sorcery').
card_original_text('dirge of dread'/'C13', 'All creatures gain fear until end of turn. (They can\'t be blocked except by artifact creatures and/or black creatures.)Cycling {1}{B} ({1}{B}, Discard this card: Draw a card.)When you cycle Dirge of Dread, you may have target creature gain fear until end of turn.').
card_image_name('dirge of dread'/'C13', 'dirge of dread').
card_uid('dirge of dread'/'C13', 'C13:Dirge of Dread:dirge of dread').
card_rarity('dirge of dread'/'C13', 'Common').
card_artist('dirge of dread'/'C13', 'Heather Hudson').
card_number('dirge of dread'/'C13', '73').
card_multiverse_id('dirge of dread'/'C13', '376307').

card_in_set('disciple of griselbrand', 'C13').
card_original_type('disciple of griselbrand'/'C13', 'Creature — Human Cleric').
card_original_text('disciple of griselbrand'/'C13', '{1}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.').
card_image_name('disciple of griselbrand'/'C13', 'disciple of griselbrand').
card_uid('disciple of griselbrand'/'C13', 'C13:Disciple of Griselbrand:disciple of griselbrand').
card_rarity('disciple of griselbrand'/'C13', 'Uncommon').
card_artist('disciple of griselbrand'/'C13', 'Clint Cearley').
card_number('disciple of griselbrand'/'C13', '74').
card_flavor_text('disciple of griselbrand'/'C13', 'The demon Griselbrand dared to stand against Avacyn, earning him the loathing of thousands and the admiration of a few.').
card_multiverse_id('disciple of griselbrand'/'C13', '376308').

card_in_set('dismiss', 'C13').
card_original_type('dismiss'/'C13', 'Instant').
card_original_text('dismiss'/'C13', 'Counter target spell.Draw a card.').
card_image_name('dismiss'/'C13', 'dismiss').
card_uid('dismiss'/'C13', 'C13:Dismiss:dismiss').
card_rarity('dismiss'/'C13', 'Uncommon').
card_artist('dismiss'/'C13', 'Donato Giancola').
card_number('dismiss'/'C13', '39').
card_flavor_text('dismiss'/'C13', '\"There is nothing you can do that I cannot simply deny.\"\n—Ertai, wizard adept').
card_multiverse_id('dismiss'/'C13', '376309').

card_in_set('diviner spirit', 'C13').
card_original_type('diviner spirit'/'C13', 'Creature — Spirit').
card_original_text('diviner spirit'/'C13', 'Whenever Diviner Spirit deals combat damage to a player, you and that player each draw that many cards.').
card_first_print('diviner spirit', 'C13').
card_image_name('diviner spirit'/'C13', 'diviner spirit').
card_uid('diviner spirit'/'C13', 'C13:Diviner Spirit:diviner spirit').
card_rarity('diviner spirit'/'C13', 'Uncommon').
card_artist('diviner spirit'/'C13', 'Yeong-Hao Han').
card_number('diviner spirit'/'C13', '40').
card_flavor_text('diviner spirit'/'C13', 'Sentience is its natural habitat.').
card_multiverse_id('diviner spirit'/'C13', '376310').

card_in_set('divinity of pride', 'C13').
card_original_type('divinity of pride'/'C13', 'Creature — Spirit Avatar').
card_original_text('divinity of pride'/'C13', 'Flying, lifelinkDivinity of Pride gets +4/+4 as long as you have 25 or more life.').
card_image_name('divinity of pride'/'C13', 'divinity of pride').
card_uid('divinity of pride'/'C13', 'C13:Divinity of Pride:divinity of pride').
card_rarity('divinity of pride'/'C13', 'Rare').
card_artist('divinity of pride'/'C13', 'Greg Staples').
card_number('divinity of pride'/'C13', '228').
card_flavor_text('divinity of pride'/'C13', '\"She spoke of mystery and portent, of such unfathomable things that my mind screamed for pity.\"\n—The Seer\'s Parables').
card_multiverse_id('divinity of pride'/'C13', '376311').

card_in_set('djinn of infinite deceits', 'C13').
card_original_type('djinn of infinite deceits'/'C13', 'Creature — Djinn').
card_original_text('djinn of infinite deceits'/'C13', 'Flying{T}: Exchange control of two target nonlegendary creatures. You can\'t activate this ability during combat.').
card_first_print('djinn of infinite deceits', 'C13').
card_image_name('djinn of infinite deceits'/'C13', 'djinn of infinite deceits').
card_uid('djinn of infinite deceits'/'C13', 'C13:Djinn of Infinite Deceits:djinn of infinite deceits').
card_rarity('djinn of infinite deceits'/'C13', 'Rare').
card_artist('djinn of infinite deceits'/'C13', 'Robbie Trevino').
card_number('djinn of infinite deceits'/'C13', '41').
card_flavor_text('djinn of infinite deceits'/'C13', 'The winds of change have a mind of their own.').
card_multiverse_id('djinn of infinite deceits'/'C13', '376312').

card_in_set('drifting meadow', 'C13').
card_original_type('drifting meadow'/'C13', 'Land').
card_original_text('drifting meadow'/'C13', 'Drifting Meadow enters the battlefield tapped.{T}: Add {W} to your mana pool.Cycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('drifting meadow'/'C13', 'drifting meadow').
card_uid('drifting meadow'/'C13', 'C13:Drifting Meadow:drifting meadow').
card_rarity('drifting meadow'/'C13', 'Common').
card_artist('drifting meadow'/'C13', 'Jonas De Ro').
card_number('drifting meadow'/'C13', '285').
card_multiverse_id('drifting meadow'/'C13', '376313').

card_in_set('dromar\'s charm', 'C13').
card_original_type('dromar\'s charm'/'C13', 'Instant').
card_original_text('dromar\'s charm'/'C13', 'Choose one — You gain 5 life; or counter target spell; or target creature gets -2/-2 until end of turn.').
card_image_name('dromar\'s charm'/'C13', 'dromar\'s charm').
card_uid('dromar\'s charm'/'C13', 'C13:Dromar\'s Charm:dromar\'s charm').
card_rarity('dromar\'s charm'/'C13', 'Uncommon').
card_artist('dromar\'s charm'/'C13', 'Marco Nelor').
card_number('dromar\'s charm'/'C13', '187').
card_flavor_text('dromar\'s charm'/'C13', '\"Wisdom can be summed up in two rules: Control what you can. Eliminate what you cannot.\"\n—Dromar, the Banisher').
card_multiverse_id('dromar\'s charm'/'C13', '376314').

card_in_set('druidic satchel', 'C13').
card_original_type('druidic satchel'/'C13', 'Artifact').
card_original_text('druidic satchel'/'C13', '{2}, {T}: Reveal the top card of your library. If it\'s a creature card, put a 1/1 green Saproling creature token onto the battlefield. If it\'s a land card, put that card onto the battlefield under your control. If it\'s a noncreature, nonland card, you gain 2 life.').
card_image_name('druidic satchel'/'C13', 'druidic satchel').
card_uid('druidic satchel'/'C13', 'C13:Druidic Satchel:druidic satchel').
card_rarity('druidic satchel'/'C13', 'Rare').
card_artist('druidic satchel'/'C13', 'Matt Stewart').
card_number('druidic satchel'/'C13', '242').
card_multiverse_id('druidic satchel'/'C13', '376315').

card_in_set('drumhunter', 'C13').
card_original_type('drumhunter'/'C13', 'Creature — Human Druid Warrior').
card_original_text('drumhunter'/'C13', 'At the beginning of your end step, if you control a creature with power 5 or greater, you may draw a card.{T}: Add {1} to your mana pool.').
card_image_name('drumhunter'/'C13', 'drumhunter').
card_uid('drumhunter'/'C13', 'C13:Drumhunter:drumhunter').
card_rarity('drumhunter'/'C13', 'Uncommon').
card_artist('drumhunter'/'C13', 'Jim Murray').
card_number('drumhunter'/'C13', '142').
card_flavor_text('drumhunter'/'C13', '\"I feel the vine-rhythm as my friends drive the beast to me. I will not miss my mark.\"').
card_multiverse_id('drumhunter'/'C13', '376316').

card_in_set('dungeon geists', 'C13').
card_original_type('dungeon geists'/'C13', 'Creature — Spirit').
card_original_text('dungeon geists'/'C13', 'FlyingWhen Dungeon Geists enters the battlefield, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s untap step for as long as you control Dungeon Geists.').
card_image_name('dungeon geists'/'C13', 'dungeon geists').
card_uid('dungeon geists'/'C13', 'C13:Dungeon Geists:dungeon geists').
card_rarity('dungeon geists'/'C13', 'Rare').
card_artist('dungeon geists'/'C13', 'Nils Hamm').
card_number('dungeon geists'/'C13', '42').
card_multiverse_id('dungeon geists'/'C13', '376317').

card_in_set('echo mage', 'C13').
card_original_type('echo mage'/'C13', 'Creature — Human Wizard').
card_original_text('echo mage'/'C13', 'Level up {1}{U} ({1}{U}: Put a level counter on this. Level up only as a sorcery.)LEVEL 2-32/4{U}{U}, {T}: Copy target instant or sorcery spell. You may choose new targets for the copy.LEVEL 4+2/5{U}{U}, {T}: Copy target instant or sorcery spell twice. You may choose new targets for the copies.').
card_image_name('echo mage'/'C13', 'echo mage').
card_uid('echo mage'/'C13', 'C13:Echo Mage:echo mage').
card_rarity('echo mage'/'C13', 'Rare').
card_artist('echo mage'/'C13', 'Matt Stewart').
card_number('echo mage'/'C13', '43').
card_multiverse_id('echo mage'/'C13', '376318').

card_in_set('elvish skysweeper', 'C13').
card_original_type('elvish skysweeper'/'C13', 'Creature — Elf Warrior').
card_original_text('elvish skysweeper'/'C13', '{4}{G}, Sacrifice a creature: Destroy target creature with flying.').
card_image_name('elvish skysweeper'/'C13', 'elvish skysweeper').
card_uid('elvish skysweeper'/'C13', 'C13:Elvish Skysweeper:elvish skysweeper').
card_rarity('elvish skysweeper'/'C13', 'Common').
card_artist('elvish skysweeper'/'C13', 'Mark Tedin').
card_number('elvish skysweeper'/'C13', '143').
card_flavor_text('elvish skysweeper'/'C13', 'The spires of Ravnica are no different from the tall trees of other planes. The elves navigate and protect them just the same.').
card_multiverse_id('elvish skysweeper'/'C13', '376319').

card_in_set('endless cockroaches', 'C13').
card_original_type('endless cockroaches'/'C13', 'Creature — Insect').
card_original_text('endless cockroaches'/'C13', 'When Endless Cockroaches dies, return it to its owner\'s hand.').
card_image_name('endless cockroaches'/'C13', 'endless cockroaches').
card_uid('endless cockroaches'/'C13', 'C13:Endless Cockroaches:endless cockroaches').
card_rarity('endless cockroaches'/'C13', 'Rare').
card_artist('endless cockroaches'/'C13', 'Ron Spencer').
card_number('endless cockroaches'/'C13', '75').
card_flavor_text('endless cockroaches'/'C13', 'There is nothing more repulsive than a cockroach—except a cockroach and all its friends.').
card_multiverse_id('endless cockroaches'/'C13', '376320').

card_in_set('endrek sahr, master breeder', 'C13').
card_original_type('endrek sahr, master breeder'/'C13', 'Legendary Creature — Human Wizard').
card_original_text('endrek sahr, master breeder'/'C13', 'Whenever you cast a creature spell, put X 1/1 black Thrull creature tokens onto the battlefield, where X is that spell\'s converted mana cost.When you control seven or more Thrulls, sacrifice Endrek Sahr, Master Breeder.').
card_image_name('endrek sahr, master breeder'/'C13', 'endrek sahr, master breeder').
card_uid('endrek sahr, master breeder'/'C13', 'C13:Endrek Sahr, Master Breeder:endrek sahr, master breeder').
card_rarity('endrek sahr, master breeder'/'C13', 'Rare').
card_artist('endrek sahr, master breeder'/'C13', 'Mark Tedin').
card_number('endrek sahr, master breeder'/'C13', '76').
card_multiverse_id('endrek sahr, master breeder'/'C13', '376321').

card_in_set('esper panorama', 'C13').
card_original_type('esper panorama'/'C13', 'Land').
card_original_text('esper panorama'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}, Sacrifice Esper Panorama: Search your library for a basic Plains, Island, or Swamp card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('esper panorama'/'C13', 'esper panorama').
card_uid('esper panorama'/'C13', 'C13:Esper Panorama:esper panorama').
card_rarity('esper panorama'/'C13', 'Common').
card_artist('esper panorama'/'C13', 'Franz Vohwinkel').
card_number('esper panorama'/'C13', '286').
card_flavor_text('esper panorama'/'C13', 'Esper is an expansive canvas painted by precise, controlling hands.').
card_multiverse_id('esper panorama'/'C13', '376322').

card_in_set('eternal dragon', 'C13').
card_original_type('eternal dragon'/'C13', 'Creature — Dragon Spirit').
card_original_text('eternal dragon'/'C13', 'Flying{3}{W}{W}: Return Eternal Dragon from your graveyard to your hand. Activate this ability only during your upkeep.Plainscycling {2} ({2}, Discard this card: Search your library for a Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('eternal dragon'/'C13', 'eternal dragon').
card_uid('eternal dragon'/'C13', 'C13:Eternal Dragon:eternal dragon').
card_rarity('eternal dragon'/'C13', 'Rare').
card_artist('eternal dragon'/'C13', 'Adam Rex').
card_number('eternal dragon'/'C13', '10').
card_multiverse_id('eternal dragon'/'C13', '376323').

card_in_set('evolving wilds', 'C13').
card_original_type('evolving wilds'/'C13', 'Land').
card_original_text('evolving wilds'/'C13', '{T}, Sacrifice Evolving Wilds: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('evolving wilds'/'C13', 'evolving wilds').
card_uid('evolving wilds'/'C13', 'C13:Evolving Wilds:evolving wilds').
card_rarity('evolving wilds'/'C13', 'Common').
card_artist('evolving wilds'/'C13', 'Steven Belledin').
card_number('evolving wilds'/'C13', '287').
card_flavor_text('evolving wilds'/'C13', 'Without the interfering hands of civilization, nature will always shape itself to its own needs.').
card_multiverse_id('evolving wilds'/'C13', '376324').

card_in_set('eye of doom', 'C13').
card_original_type('eye of doom'/'C13', 'Artifact').
card_original_text('eye of doom'/'C13', 'When Eye of Doom enters the battlefield, each player chooses a nonland permanent and puts a doom counter on it.{2}, {T}, Sacrifice Eye of Doom: Destroy each permanent with a doom counter on it.').
card_first_print('eye of doom', 'C13').
card_image_name('eye of doom'/'C13', 'eye of doom').
card_uid('eye of doom'/'C13', 'C13:Eye of Doom:eye of doom').
card_rarity('eye of doom'/'C13', 'Rare').
card_artist('eye of doom'/'C13', 'Yeong-Hao Han').
card_number('eye of doom'/'C13', '243').
card_multiverse_id('eye of doom'/'C13', '376325').

card_in_set('faerie conclave', 'C13').
card_original_type('faerie conclave'/'C13', 'Land').
card_original_text('faerie conclave'/'C13', 'Faerie Conclave enters the battlefield tapped.{T}: Add {U} to your mana pool.{1}{U}: Faerie Conclave becomes a 2/1 blue Faerie creature with flying until end of turn. It\'s still a land.').
card_image_name('faerie conclave'/'C13', 'faerie conclave').
card_uid('faerie conclave'/'C13', 'C13:Faerie Conclave:faerie conclave').
card_rarity('faerie conclave'/'C13', 'Uncommon').
card_artist('faerie conclave'/'C13', 'Stephan Martiniere').
card_number('faerie conclave'/'C13', '288').
card_multiverse_id('faerie conclave'/'C13', '376326').

card_in_set('famine', 'C13').
card_original_type('famine'/'C13', 'Sorcery').
card_original_text('famine'/'C13', 'Famine deals 3 damage to each creature and each player.').
card_image_name('famine'/'C13', 'famine').
card_uid('famine'/'C13', 'C13:Famine:famine').
card_rarity('famine'/'C13', 'Uncommon').
card_artist('famine'/'C13', 'Karla Ortiz').
card_number('famine'/'C13', '77').
card_flavor_text('famine'/'C13', '\"Better to starve to death than be bored to death.\"\n—Liliana Vess').
card_multiverse_id('famine'/'C13', '376327').

card_in_set('farhaven elf', 'C13').
card_original_type('farhaven elf'/'C13', 'Creature — Elf Druid').
card_original_text('farhaven elf'/'C13', 'When Farhaven Elf enters the battlefield, you may search your library for a basic land card and put it onto the battlefield tapped. If you do, shuffle your library.').
card_image_name('farhaven elf'/'C13', 'farhaven elf').
card_uid('farhaven elf'/'C13', 'C13:Farhaven Elf:farhaven elf').
card_rarity('farhaven elf'/'C13', 'Common').
card_artist('farhaven elf'/'C13', 'Brandon Kitkouski').
card_number('farhaven elf'/'C13', '144').
card_flavor_text('farhaven elf'/'C13', '\"Verdant bloom does exist. It merely hides for its own safety.\"').
card_multiverse_id('farhaven elf'/'C13', '376328').

card_in_set('fecundity', 'C13').
card_original_type('fecundity'/'C13', 'Enchantment').
card_original_text('fecundity'/'C13', 'Whenever a creature dies, that creature\'s controller may draw a card.').
card_image_name('fecundity'/'C13', 'fecundity').
card_uid('fecundity'/'C13', 'C13:Fecundity:fecundity').
card_rarity('fecundity'/'C13', 'Uncommon').
card_artist('fecundity'/'C13', 'Rebecca Guay').
card_number('fecundity'/'C13', '145').
card_flavor_text('fecundity'/'C13', 'Life is eternal. A lifetime is ephemeral.').
card_multiverse_id('fecundity'/'C13', '376329').

card_in_set('fell shepherd', 'C13').
card_original_type('fell shepherd'/'C13', 'Creature — Avatar').
card_original_text('fell shepherd'/'C13', 'Whenever Fell Shepherd deals combat damage to a player, you may return to your hand all creature cards that were put into your graveyard from the battlefield this turn.{B}, Sacrifice another creature: Target creature gets -2/-2 until end of turn.').
card_first_print('fell shepherd', 'C13').
card_image_name('fell shepherd'/'C13', 'fell shepherd').
card_uid('fell shepherd'/'C13', 'C13:Fell Shepherd:fell shepherd').
card_rarity('fell shepherd'/'C13', 'Rare').
card_artist('fell shepherd'/'C13', 'Brad Rigney').
card_number('fell shepherd'/'C13', '78').
card_multiverse_id('fell shepherd'/'C13', '376330').

card_in_set('fiend hunter', 'C13').
card_original_type('fiend hunter'/'C13', 'Creature — Human Cleric').
card_original_text('fiend hunter'/'C13', 'When Fiend Hunter enters the battlefield, you may exile another target creature.When Fiend Hunter leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_image_name('fiend hunter'/'C13', 'fiend hunter').
card_uid('fiend hunter'/'C13', 'C13:Fiend Hunter:fiend hunter').
card_rarity('fiend hunter'/'C13', 'Uncommon').
card_artist('fiend hunter'/'C13', 'Wayne Reynolds').
card_number('fiend hunter'/'C13', '11').
card_multiverse_id('fiend hunter'/'C13', '376331').

card_in_set('fiery justice', 'C13').
card_original_type('fiery justice'/'C13', 'Sorcery').
card_original_text('fiery justice'/'C13', 'Fiery Justice deals 5 damage divided as you choose among any number of target creatures and/or players. Target opponent gains 5 life.').
card_image_name('fiery justice'/'C13', 'fiery justice').
card_uid('fiery justice'/'C13', 'C13:Fiery Justice:fiery justice').
card_rarity('fiery justice'/'C13', 'Rare').
card_artist('fiery justice'/'C13', 'Mathias Kollros').
card_number('fiery justice'/'C13', '188').
card_flavor_text('fiery justice'/'C13', 'When the rebels couldn\'t breach the castle to kill Lalor the Obstinate, they tried a more internal approach to depose the pretender king.').
card_multiverse_id('fiery justice'/'C13', '376332').

card_in_set('filigree angel', 'C13').
card_original_type('filigree angel'/'C13', 'Artifact Creature — Angel').
card_original_text('filigree angel'/'C13', 'FlyingWhen Filigree Angel enters the battlefield, you gain 3 life for each artifact you control.').
card_image_name('filigree angel'/'C13', 'filigree angel').
card_uid('filigree angel'/'C13', 'C13:Filigree Angel:filigree angel').
card_rarity('filigree angel'/'C13', 'Rare').
card_artist('filigree angel'/'C13', 'Richard Whitters').
card_number('filigree angel'/'C13', '189').
card_flavor_text('filigree angel'/'C13', '\"I craved enlightenment, and Crucius\'s etherium opened my eyes. I would share my sight with you, but first you must believe.\"').
card_multiverse_id('filigree angel'/'C13', '376333').

card_in_set('fireball', 'C13').
card_original_type('fireball'/'C13', 'Sorcery').
card_original_text('fireball'/'C13', 'Fireball deals X damage divided evenly, rounded down, among any number of target creatures and/or players.Fireball costs {1} more to cast for each target beyond the first.').
card_image_name('fireball'/'C13', 'fireball').
card_uid('fireball'/'C13', 'C13:Fireball:fireball').
card_rarity('fireball'/'C13', 'Uncommon').
card_artist('fireball'/'C13', 'Dave Dorman').
card_number('fireball'/'C13', '106').
card_multiverse_id('fireball'/'C13', '376334').

card_in_set('fires of yavimaya', 'C13').
card_original_type('fires of yavimaya'/'C13', 'Enchantment').
card_original_text('fires of yavimaya'/'C13', 'Creatures you control have haste.Sacrifice Fires of Yavimaya: Target creature gets +2/+2 until end of turn.').
card_image_name('fires of yavimaya'/'C13', 'fires of yavimaya').
card_uid('fires of yavimaya'/'C13', 'C13:Fires of Yavimaya:fires of yavimaya').
card_rarity('fires of yavimaya'/'C13', 'Uncommon').
card_artist('fires of yavimaya'/'C13', 'Izzy').
card_number('fires of yavimaya'/'C13', '190').
card_flavor_text('fires of yavimaya'/'C13', 'Yavimaya lights the quickest path to battle.').
card_multiverse_id('fires of yavimaya'/'C13', '376335').

card_in_set('fissure vent', 'C13').
card_original_type('fissure vent'/'C13', 'Sorcery').
card_original_text('fissure vent'/'C13', 'Choose one or both — Destroy target artifact; and/or destroy target nonbasic land.').
card_image_name('fissure vent'/'C13', 'fissure vent').
card_uid('fissure vent'/'C13', 'C13:Fissure Vent:fissure vent').
card_rarity('fissure vent'/'C13', 'Common').
card_artist('fissure vent'/'C13', 'Philip Straub').
card_number('fissure vent'/'C13', '107').
card_flavor_text('fissure vent'/'C13', '\"Something down there has an appetite for what we\'re standing on. Let\'s hope it doesn\'t want seconds.\"\n—Samila, Murasa Expeditionary House').
card_multiverse_id('fissure vent'/'C13', '376336').

card_in_set('flickerform', 'C13').
card_original_type('flickerform'/'C13', 'Enchantment — Aura').
card_original_text('flickerform'/'C13', 'Enchant creature{2}{W}{W}: Exile enchanted creature and all Auras attached to it. At the beginning of the next end step, return that card to the battlefield under its owner\'s control. If you do, return the other cards exiled this way to the battlefield under their owners\' control attached to that creature.').
card_image_name('flickerform'/'C13', 'flickerform').
card_uid('flickerform'/'C13', 'C13:Flickerform:flickerform').
card_rarity('flickerform'/'C13', 'Rare').
card_artist('flickerform'/'C13', 'Ron Spears').
card_number('flickerform'/'C13', '12').
card_multiverse_id('flickerform'/'C13', '376337').

card_in_set('flickerwisp', 'C13').
card_original_type('flickerwisp'/'C13', 'Creature — Elemental').
card_original_text('flickerwisp'/'C13', 'FlyingWhen Flickerwisp enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('flickerwisp'/'C13', 'flickerwisp').
card_uid('flickerwisp'/'C13', 'C13:Flickerwisp:flickerwisp').
card_rarity('flickerwisp'/'C13', 'Uncommon').
card_artist('flickerwisp'/'C13', 'Jeremy Enecio').
card_number('flickerwisp'/'C13', '13').
card_flavor_text('flickerwisp'/'C13', 'Its wings disturb more than air.').
card_multiverse_id('flickerwisp'/'C13', '376338').

card_in_set('fog bank', 'C13').
card_original_type('fog bank'/'C13', 'Creature — Wall').
card_original_text('fog bank'/'C13', 'Defender, flyingPrevent all combat damage that would be dealt to and dealt by Fog Bank.').
card_image_name('fog bank'/'C13', 'fog bank').
card_uid('fog bank'/'C13', 'C13:Fog Bank:fog bank').
card_rarity('fog bank'/'C13', 'Uncommon').
card_artist('fog bank'/'C13', 'Howard Lyon').
card_number('fog bank'/'C13', '44').
card_flavor_text('fog bank'/'C13', '\"The union of sea and sky makes a perfect cover.\"\n—Talrand, sky summoner').
card_multiverse_id('fog bank'/'C13', '376339').

card_in_set('forest', 'C13').
card_original_type('forest'/'C13', 'Basic Land — Forest').
card_original_text('forest'/'C13', 'G').
card_image_name('forest'/'C13', 'forest1').
card_uid('forest'/'C13', 'C13:Forest:forest1').
card_rarity('forest'/'C13', 'Basic Land').
card_artist('forest'/'C13', 'Rob Alexander').
card_number('forest'/'C13', '353').
card_multiverse_id('forest'/'C13', '376340').

card_in_set('forest', 'C13').
card_original_type('forest'/'C13', 'Basic Land — Forest').
card_original_text('forest'/'C13', 'G').
card_image_name('forest'/'C13', 'forest2').
card_uid('forest'/'C13', 'C13:Forest:forest2').
card_rarity('forest'/'C13', 'Basic Land').
card_artist('forest'/'C13', 'Jung Park').
card_number('forest'/'C13', '354').
card_multiverse_id('forest'/'C13', '376342').

card_in_set('forest', 'C13').
card_original_type('forest'/'C13', 'Basic Land — Forest').
card_original_text('forest'/'C13', 'G').
card_image_name('forest'/'C13', 'forest3').
card_uid('forest'/'C13', 'C13:Forest:forest3').
card_rarity('forest'/'C13', 'Basic Land').
card_artist('forest'/'C13', 'Jung Park').
card_number('forest'/'C13', '355').
card_multiverse_id('forest'/'C13', '376341').

card_in_set('forest', 'C13').
card_original_type('forest'/'C13', 'Basic Land — Forest').
card_original_text('forest'/'C13', 'G').
card_image_name('forest'/'C13', 'forest4').
card_uid('forest'/'C13', 'C13:Forest:forest4').
card_rarity('forest'/'C13', 'Basic Land').
card_artist('forest'/'C13', 'Eytan Zana').
card_number('forest'/'C13', '356').
card_multiverse_id('forest'/'C13', '376343').

card_in_set('forgotten cave', 'C13').
card_original_type('forgotten cave'/'C13', 'Land').
card_original_text('forgotten cave'/'C13', 'Forgotten Cave enters the battlefield tapped.{T}: Add {R} to your mana pool.Cycling {R} ({R}, Discard this card: Draw a card.)').
card_image_name('forgotten cave'/'C13', 'forgotten cave').
card_uid('forgotten cave'/'C13', 'C13:Forgotten Cave:forgotten cave').
card_rarity('forgotten cave'/'C13', 'Common').
card_artist('forgotten cave'/'C13', 'Tony Szczudlo').
card_number('forgotten cave'/'C13', '289').
card_multiverse_id('forgotten cave'/'C13', '376344').

card_in_set('foster', 'C13').
card_original_type('foster'/'C13', 'Enchantment').
card_original_text('foster'/'C13', 'Whenever a creature you control dies, you may pay {1}. If you do, reveal cards from the top of your library until you reveal a creature card. Put that card into your hand and the rest into your graveyard.').
card_image_name('foster'/'C13', 'foster').
card_uid('foster'/'C13', 'C13:Foster:foster').
card_rarity('foster'/'C13', 'Rare').
card_artist('foster'/'C13', 'Carl Critchlow').
card_number('foster'/'C13', '146').
card_multiverse_id('foster'/'C13', '376345').

card_in_set('from the ashes', 'C13').
card_original_type('from the ashes'/'C13', 'Sorcery').
card_original_text('from the ashes'/'C13', 'Destroy all nonbasic lands. For each land destroyed this way, its controller may search his or her library for a basic land card and put it onto the battlefield. Then each player who searched his or her library this way shuffles it.').
card_first_print('from the ashes', 'C13').
card_image_name('from the ashes'/'C13', 'from the ashes').
card_uid('from the ashes'/'C13', 'C13:From the Ashes:from the ashes').
card_rarity('from the ashes'/'C13', 'Rare').
card_artist('from the ashes'/'C13', 'Karl Kopinski').
card_number('from the ashes'/'C13', '108').
card_multiverse_id('from the ashes'/'C13', '376346').

card_in_set('furnace celebration', 'C13').
card_original_type('furnace celebration'/'C13', 'Enchantment').
card_original_text('furnace celebration'/'C13', 'Whenever you sacrifice another permanent, you may pay {2}. If you do, Furnace Celebration deals 2 damage to target creature or player.').
card_image_name('furnace celebration'/'C13', 'furnace celebration').
card_uid('furnace celebration'/'C13', 'C13:Furnace Celebration:furnace celebration').
card_rarity('furnace celebration'/'C13', 'Uncommon').
card_artist('furnace celebration'/'C13', 'Svetlin Velinov').
card_number('furnace celebration'/'C13', '109').
card_flavor_text('furnace celebration'/'C13', 'Nothing impresses goblins more than something that\'s bound to kill them.').
card_multiverse_id('furnace celebration'/'C13', '376347').

card_in_set('gahiji, honored one', 'C13').
card_original_type('gahiji, honored one'/'C13', 'Legendary Creature — Beast').
card_original_text('gahiji, honored one'/'C13', 'Whenever a creature attacks one of your opponents or a planeswalker an opponent controls, that creature gets +2/+0 until end of turn.').
card_first_print('gahiji, honored one', 'C13').
card_image_name('gahiji, honored one'/'C13', 'gahiji, honored one').
card_uid('gahiji, honored one'/'C13', 'C13:Gahiji, Honored One:gahiji, honored one').
card_rarity('gahiji, honored one'/'C13', 'Mythic Rare').
card_artist('gahiji, honored one'/'C13', 'Brynn Metheney').
card_number('gahiji, honored one'/'C13', '191').
card_flavor_text('gahiji, honored one'/'C13', 'He can stir the gentlest spirit to wild ferocity.').
card_multiverse_id('gahiji, honored one'/'C13', '376348').

card_in_set('goblin bombardment', 'C13').
card_original_type('goblin bombardment'/'C13', 'Enchantment').
card_original_text('goblin bombardment'/'C13', 'Sacrifice a creature: Goblin Bombardment deals 1 damage to target creature or player.').
card_image_name('goblin bombardment'/'C13', 'goblin bombardment').
card_uid('goblin bombardment'/'C13', 'C13:Goblin Bombardment:goblin bombardment').
card_rarity('goblin bombardment'/'C13', 'Uncommon').
card_artist('goblin bombardment'/'C13', 'Brian Snõddy').
card_number('goblin bombardment'/'C13', '110').
card_flavor_text('goblin bombardment'/'C13', 'One mogg to aim the catapult, one mogg to steer the rock.').
card_multiverse_id('goblin bombardment'/'C13', '376349').

card_in_set('goblin sharpshooter', 'C13').
card_original_type('goblin sharpshooter'/'C13', 'Creature — Goblin').
card_original_text('goblin sharpshooter'/'C13', 'Goblin Sharpshooter doesn\'t untap during your untap step.Whenever a creature dies, untap Goblin Sharpshooter.{T}: Goblin Sharpshooter deals 1 damage to target creature or player.').
card_image_name('goblin sharpshooter'/'C13', 'goblin sharpshooter').
card_uid('goblin sharpshooter'/'C13', 'C13:Goblin Sharpshooter:goblin sharpshooter').
card_rarity('goblin sharpshooter'/'C13', 'Rare').
card_artist('goblin sharpshooter'/'C13', 'Wayne Reynolds').
card_number('goblin sharpshooter'/'C13', '111').
card_multiverse_id('goblin sharpshooter'/'C13', '376350').

card_in_set('golgari guildgate', 'C13').
card_original_type('golgari guildgate'/'C13', 'Land — Gate').
card_original_text('golgari guildgate'/'C13', 'Golgari Guildgate enters the battlefield tapped.{T}: Add {B} or {G} to your mana pool.').
card_image_name('golgari guildgate'/'C13', 'golgari guildgate').
card_uid('golgari guildgate'/'C13', 'C13:Golgari Guildgate:golgari guildgate').
card_rarity('golgari guildgate'/'C13', 'Common').
card_artist('golgari guildgate'/'C13', 'Eytan Zana').
card_number('golgari guildgate'/'C13', '290').
card_flavor_text('golgari guildgate'/'C13', 'Enter those who are starving and sick. You are welcome among the Swarm when the rest of Ravnica rejects you.').
card_multiverse_id('golgari guildgate'/'C13', '376351').

card_in_set('golgari guildmage', 'C13').
card_original_type('golgari guildmage'/'C13', 'Creature — Elf Shaman').
card_original_text('golgari guildmage'/'C13', '{4}{B}, Sacrifice a creature: Return target creature card from your graveyard to your hand.{4}{G}: Put a +1/+1 counter on target creature.').
card_image_name('golgari guildmage'/'C13', 'golgari guildmage').
card_uid('golgari guildmage'/'C13', 'C13:Golgari Guildmage:golgari guildmage').
card_rarity('golgari guildmage'/'C13', 'Uncommon').
card_artist('golgari guildmage'/'C13', 'Zoltan Boros & Gabor Szikszai').
card_number('golgari guildmage'/'C13', '229').
card_multiverse_id('golgari guildmage'/'C13', '376352').

card_in_set('golgari rot farm', 'C13').
card_original_type('golgari rot farm'/'C13', 'Land').
card_original_text('golgari rot farm'/'C13', 'Golgari Rot Farm enters the battlefield tapped.When Golgari Rot Farm enters the battlefield, return a land you control to its owner\'s hand.{T}: Add {B}{G} to your mana pool.').
card_image_name('golgari rot farm'/'C13', 'golgari rot farm').
card_uid('golgari rot farm'/'C13', 'C13:Golgari Rot Farm:golgari rot farm').
card_rarity('golgari rot farm'/'C13', 'Common').
card_artist('golgari rot farm'/'C13', 'John Avon').
card_number('golgari rot farm'/'C13', '291').
card_multiverse_id('golgari rot farm'/'C13', '376353').

card_in_set('grazing gladehart', 'C13').
card_original_type('grazing gladehart'/'C13', 'Creature — Antelope').
card_original_text('grazing gladehart'/'C13', 'Landfall — Whenever a land enters the battlefield under your control, you may gain 2 life.').
card_image_name('grazing gladehart'/'C13', 'grazing gladehart').
card_uid('grazing gladehart'/'C13', 'C13:Grazing Gladehart:grazing gladehart').
card_rarity('grazing gladehart'/'C13', 'Common').
card_artist('grazing gladehart'/'C13', 'Ryan Pancoast').
card_number('grazing gladehart'/'C13', '147').
card_flavor_text('grazing gladehart'/'C13', '\"Don\'t be fooled. If it were as docile as it looks, it would\'ve died off long ago.\"\n—Yon Basrel, Oran-Rief survivalist').
card_multiverse_id('grazing gladehart'/'C13', '376354').

card_in_set('greed', 'C13').
card_original_type('greed'/'C13', 'Enchantment').
card_original_text('greed'/'C13', '{B}, Pay 2 life: Draw a card.').
card_image_name('greed'/'C13', 'greed').
card_uid('greed'/'C13', 'C13:Greed:greed').
card_rarity('greed'/'C13', 'Rare').
card_artist('greed'/'C13', 'Izzy').
card_number('greed'/'C13', '79').
card_flavor_text('greed'/'C13', 'What is a sliver of one\'s soul compared to unimaginable wealth?').
card_multiverse_id('greed'/'C13', '376355').

card_in_set('grim backwoods', 'C13').
card_original_type('grim backwoods'/'C13', 'Land').
card_original_text('grim backwoods'/'C13', '{T}: Add {1} to your mana pool.{2}{B}{G}, {T}, Sacrifice a creature: Draw a card.').
card_image_name('grim backwoods'/'C13', 'grim backwoods').
card_uid('grim backwoods'/'C13', 'C13:Grim Backwoods:grim backwoods').
card_rarity('grim backwoods'/'C13', 'Rare').
card_artist('grim backwoods'/'C13', 'Vincent Proce').
card_number('grim backwoods'/'C13', '292').
card_flavor_text('grim backwoods'/'C13', '\"I love what they\'ve done with the place.\"\n—Liliana Vess').
card_multiverse_id('grim backwoods'/'C13', '376356').

card_in_set('grixis charm', 'C13').
card_original_type('grixis charm'/'C13', 'Instant').
card_original_text('grixis charm'/'C13', 'Choose one — Return target permanent to its owner\'s hand; or target creature gets -4/-4 until end of turn; or creatures you control get +2/+0 until end of turn.').
card_image_name('grixis charm'/'C13', 'grixis charm').
card_uid('grixis charm'/'C13', 'C13:Grixis Charm:grixis charm').
card_rarity('grixis charm'/'C13', 'Uncommon').
card_artist('grixis charm'/'C13', 'Lars Grant-West').
card_number('grixis charm'/'C13', '192').
card_flavor_text('grixis charm'/'C13', '\"So many choices. Shall I choose loathing, hate, or malice today?\"\n—Eliza of the Keep').
card_multiverse_id('grixis charm'/'C13', '376357').

card_in_set('grixis panorama', 'C13').
card_original_type('grixis panorama'/'C13', 'Land').
card_original_text('grixis panorama'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}, Sacrifice Grixis Panorama: Search your library for a basic Island, Swamp, or Mountain card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('grixis panorama'/'C13', 'grixis panorama').
card_uid('grixis panorama'/'C13', 'C13:Grixis Panorama:grixis panorama').
card_rarity('grixis panorama'/'C13', 'Common').
card_artist('grixis panorama'/'C13', 'Nils Hamm').
card_number('grixis panorama'/'C13', '293').
card_flavor_text('grixis panorama'/'C13', 'There is no height above Grixis that is free from the stench of death.').
card_multiverse_id('grixis panorama'/'C13', '376358').

card_in_set('gruul guildgate', 'C13').
card_original_type('gruul guildgate'/'C13', 'Land — Gate').
card_original_text('gruul guildgate'/'C13', 'Gruul Guildgate enters the battlefield tapped.{T}: Add {R} or {G} to your mana pool.').
card_image_name('gruul guildgate'/'C13', 'gruul guildgate').
card_uid('gruul guildgate'/'C13', 'C13:Gruul Guildgate:gruul guildgate').
card_rarity('gruul guildgate'/'C13', 'Common').
card_artist('gruul guildgate'/'C13', 'Randy Gallegos').
card_number('gruul guildgate'/'C13', '294').
card_flavor_text('gruul guildgate'/'C13', 'Enter and leave the shackles of society behind.').
card_multiverse_id('gruul guildgate'/'C13', '376359').

card_in_set('guard gomazoa', 'C13').
card_original_type('guard gomazoa'/'C13', 'Creature — Jellyfish').
card_original_text('guard gomazoa'/'C13', 'Defender, flyingPrevent all combat damage that would be dealt to Guard Gomazoa.').
card_image_name('guard gomazoa'/'C13', 'guard gomazoa').
card_uid('guard gomazoa'/'C13', 'C13:Guard Gomazoa:guard gomazoa').
card_rarity('guard gomazoa'/'C13', 'Uncommon').
card_artist('guard gomazoa'/'C13', 'Rob Alexander').
card_number('guard gomazoa'/'C13', '45').
card_flavor_text('guard gomazoa'/'C13', 'It lingers near the outposts of the Makindi Trenches, inadvertently granting another layer of defense.').
card_multiverse_id('guard gomazoa'/'C13', '376360').

card_in_set('guttersnipe', 'C13').
card_original_type('guttersnipe'/'C13', 'Creature — Goblin Shaman').
card_original_text('guttersnipe'/'C13', 'Whenever you cast an instant or sorcery spell, Guttersnipe deals 2 damage to each opponent.').
card_image_name('guttersnipe'/'C13', 'guttersnipe').
card_uid('guttersnipe'/'C13', 'C13:Guttersnipe:guttersnipe').
card_rarity('guttersnipe'/'C13', 'Uncommon').
card_artist('guttersnipe'/'C13', 'Steve Prescott').
card_number('guttersnipe'/'C13', '112').
card_flavor_text('guttersnipe'/'C13', 'He wants to watch Ravnica burn, one soul at a time.').
card_multiverse_id('guttersnipe'/'C13', '376361').

card_in_set('hada spy patrol', 'C13').
card_original_type('hada spy patrol'/'C13', 'Creature — Human Rogue').
card_original_text('hada spy patrol'/'C13', 'Level up {2}{U} ({2}{U}: Put a level counter on this. Level up only as a sorcery.)LEVEL 1-22/2Hada Spy Patrol can\'t be blocked.LEVEL 3+3/3ShroudHada Spy Patrol can\'t be blocked.').
card_image_name('hada spy patrol'/'C13', 'hada spy patrol').
card_uid('hada spy patrol'/'C13', 'C13:Hada Spy Patrol:hada spy patrol').
card_rarity('hada spy patrol'/'C13', 'Uncommon').
card_artist('hada spy patrol'/'C13', 'Zoltan Boros & Gabor Szikszai').
card_number('hada spy patrol'/'C13', '46').
card_multiverse_id('hada spy patrol'/'C13', '376362').

card_in_set('harmonize', 'C13').
card_original_type('harmonize'/'C13', 'Sorcery').
card_original_text('harmonize'/'C13', 'Draw three cards.').
card_image_name('harmonize'/'C13', 'harmonize').
card_uid('harmonize'/'C13', 'C13:Harmonize:harmonize').
card_rarity('harmonize'/'C13', 'Uncommon').
card_artist('harmonize'/'C13', 'Paul Lee').
card_number('harmonize'/'C13', '148').
card_flavor_text('harmonize'/'C13', '\"Words lie. People lie. The land tells the truth.\"').
card_multiverse_id('harmonize'/'C13', '376363').

card_in_set('homeward path', 'C13').
card_original_type('homeward path'/'C13', 'Land').
card_original_text('homeward path'/'C13', '{T}: Add {1} to your mana pool.{T}: Each player gains control of all creatures he or she owns.').
card_image_name('homeward path'/'C13', 'homeward path').
card_uid('homeward path'/'C13', 'C13:Homeward Path:homeward path').
card_rarity('homeward path'/'C13', 'Rare').
card_artist('homeward path'/'C13', 'Tomasz Jedruszek').
card_number('homeward path'/'C13', '295').
card_flavor_text('homeward path'/'C13', '\"Let your heels point you home.\"\n—Ancient blessing').
card_multiverse_id('homeward path'/'C13', '376364').

card_in_set('hooded horror', 'C13').
card_original_type('hooded horror'/'C13', 'Creature — Horror').
card_original_text('hooded horror'/'C13', 'Hooded Horror can\'t be blocked if defending player controls the most creatures or is tied for the most.').
card_first_print('hooded horror', 'C13').
card_image_name('hooded horror'/'C13', 'hooded horror').
card_uid('hooded horror'/'C13', 'C13:Hooded Horror:hooded horror').
card_rarity('hooded horror'/'C13', 'Uncommon').
card_artist('hooded horror'/'C13', 'Allen Williams').
card_number('hooded horror'/'C13', '80').
card_flavor_text('hooded horror'/'C13', 'Insidious fear can be the undoing of the greatest of armies.').
card_multiverse_id('hooded horror'/'C13', '376365').

card_in_set('hua tuo, honored physician', 'C13').
card_original_type('hua tuo, honored physician'/'C13', 'Legendary Creature — Human').
card_original_text('hua tuo, honored physician'/'C13', '{T}: Put target creature card from your graveyard on top of your library. Activate this ability only during your turn, before attackers are declared.').
card_image_name('hua tuo, honored physician'/'C13', 'hua tuo, honored physician').
card_uid('hua tuo, honored physician'/'C13', 'C13:Hua Tuo, Honored Physician:hua tuo, honored physician').
card_rarity('hua tuo, honored physician'/'C13', 'Rare').
card_artist('hua tuo, honored physician'/'C13', 'Gao Jianzhang').
card_number('hua tuo, honored physician'/'C13', '149').
card_multiverse_id('hua tuo, honored physician'/'C13', '376366').

card_in_set('hull breach', 'C13').
card_original_type('hull breach'/'C13', 'Sorcery').
card_original_text('hull breach'/'C13', 'Choose one — Destroy target artifact; or destroy target enchantment; or destroy target artifact and target enchantment.').
card_image_name('hull breach'/'C13', 'hull breach').
card_uid('hull breach'/'C13', 'C13:Hull Breach:hull breach').
card_rarity('hull breach'/'C13', 'Common').
card_artist('hull breach'/'C13', 'Brian Snõddy').
card_number('hull breach'/'C13', '193').
card_flavor_text('hull breach'/'C13', '\"Crovax knows we\'re coming now,\" said a grinning Sisay. \"I just sent the Predator crashing into his stronghold.\"').
card_multiverse_id('hull breach'/'C13', '376367').

card_in_set('hunted troll', 'C13').
card_original_type('hunted troll'/'C13', 'Creature — Troll Warrior').
card_original_text('hunted troll'/'C13', 'When Hunted Troll enters the battlefield, target opponent puts four 1/1 blue Faerie creature tokens with flying onto the battlefield.{G}: Regenerate Hunted Troll.').
card_image_name('hunted troll'/'C13', 'hunted troll').
card_uid('hunted troll'/'C13', 'C13:Hunted Troll:hunted troll').
card_rarity('hunted troll'/'C13', 'Rare').
card_artist('hunted troll'/'C13', 'Greg Staples').
card_number('hunted troll'/'C13', '150').
card_multiverse_id('hunted troll'/'C13', '376368').

card_in_set('illusionist\'s gambit', 'C13').
card_original_type('illusionist\'s gambit'/'C13', 'Instant').
card_original_text('illusionist\'s gambit'/'C13', 'Cast Illusionist\'s Gambit only during the declare blockers step on an opponent\'s turn.Remove all attacking creatures from combat and untap them. After this phase, there is an additional combat phase. Each of those creatures attacks that combat if able. They can\'t attack you or a planeswalker you control that combat.').
card_first_print('illusionist\'s gambit', 'C13').
card_image_name('illusionist\'s gambit'/'C13', 'illusionist\'s gambit').
card_uid('illusionist\'s gambit'/'C13', 'C13:Illusionist\'s Gambit:illusionist\'s gambit').
card_rarity('illusionist\'s gambit'/'C13', 'Rare').
card_artist('illusionist\'s gambit'/'C13', 'Zoltan Boros').
card_number('illusionist\'s gambit'/'C13', '47').
card_multiverse_id('illusionist\'s gambit'/'C13', '376369').

card_in_set('incendiary command', 'C13').
card_original_type('incendiary command'/'C13', 'Sorcery').
card_original_text('incendiary command'/'C13', 'Choose two — Incendiary Command deals 4 damage to target player; or Incendiary Command deals 2 damage to each creature; or destroy target nonbasic land; or each player discards all the cards in his or her hand, then draws that many cards.').
card_image_name('incendiary command'/'C13', 'incendiary command').
card_uid('incendiary command'/'C13', 'C13:Incendiary Command:incendiary command').
card_rarity('incendiary command'/'C13', 'Rare').
card_artist('incendiary command'/'C13', 'Wayne England').
card_number('incendiary command'/'C13', '113').
card_multiverse_id('incendiary command'/'C13', '376370').

card_in_set('inferno titan', 'C13').
card_original_type('inferno titan'/'C13', 'Creature — Giant').
card_original_text('inferno titan'/'C13', '{R}: Inferno Titan gets +1/+0 until end of turn.Whenever Inferno Titan enters the battlefield or attacks, it deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_image_name('inferno titan'/'C13', 'inferno titan').
card_uid('inferno titan'/'C13', 'C13:Inferno Titan:inferno titan').
card_rarity('inferno titan'/'C13', 'Mythic Rare').
card_artist('inferno titan'/'C13', 'Kev Walker').
card_number('inferno titan'/'C13', '114').
card_multiverse_id('inferno titan'/'C13', '376371').

card_in_set('infest', 'C13').
card_original_type('infest'/'C13', 'Sorcery').
card_original_text('infest'/'C13', 'All creatures get -2/-2 until end of turn.').
card_image_name('infest'/'C13', 'infest').
card_uid('infest'/'C13', 'C13:Infest:infest').
card_rarity('infest'/'C13', 'Uncommon').
card_artist('infest'/'C13', 'Karl Kopinski').
card_number('infest'/'C13', '81').
card_flavor_text('infest'/'C13', '\"This is why we don\'t go out in banewasp weather.\"\n—Rannon, Vithian holdout').
card_multiverse_id('infest'/'C13', '376372').

card_in_set('island', 'C13').
card_original_type('island'/'C13', 'Basic Land — Island').
card_original_text('island'/'C13', 'U').
card_image_name('island'/'C13', 'island1').
card_uid('island'/'C13', 'C13:Island:island1').
card_rarity('island'/'C13', 'Basic Land').
card_artist('island'/'C13', 'Noah Bradley').
card_number('island'/'C13', '341').
card_multiverse_id('island'/'C13', '376375').

card_in_set('island', 'C13').
card_original_type('island'/'C13', 'Basic Land — Island').
card_original_text('island'/'C13', 'U').
card_image_name('island'/'C13', 'island2').
card_uid('island'/'C13', 'C13:Island:island2').
card_rarity('island'/'C13', 'Basic Land').
card_artist('island'/'C13', 'Cliff Childs').
card_number('island'/'C13', '342').
card_multiverse_id('island'/'C13', '376373').

card_in_set('island', 'C13').
card_original_type('island'/'C13', 'Basic Land — Island').
card_original_text('island'/'C13', 'U').
card_image_name('island'/'C13', 'island3').
card_uid('island'/'C13', 'C13:Island:island3').
card_rarity('island'/'C13', 'Basic Land').
card_artist('island'/'C13', 'Michael Komarck').
card_number('island'/'C13', '343').
card_multiverse_id('island'/'C13', '376376').

card_in_set('island', 'C13').
card_original_type('island'/'C13', 'Basic Land — Island').
card_original_text('island'/'C13', 'U').
card_image_name('island'/'C13', 'island4').
card_uid('island'/'C13', 'C13:Island:island4').
card_rarity('island'/'C13', 'Basic Land').
card_artist('island'/'C13', 'Jung Park').
card_number('island'/'C13', '344').
card_multiverse_id('island'/'C13', '376374').

card_in_set('izzet boilerworks', 'C13').
card_original_type('izzet boilerworks'/'C13', 'Land').
card_original_text('izzet boilerworks'/'C13', 'Izzet Boilerworks enters the battlefield tapped.When Izzet Boilerworks enters the battlefield, return a land you control to its owner\'s hand.{T}: Add {U}{R} to your mana pool.').
card_image_name('izzet boilerworks'/'C13', 'izzet boilerworks').
card_uid('izzet boilerworks'/'C13', 'C13:Izzet Boilerworks:izzet boilerworks').
card_rarity('izzet boilerworks'/'C13', 'Common').
card_artist('izzet boilerworks'/'C13', 'John Avon').
card_number('izzet boilerworks'/'C13', '296').
card_multiverse_id('izzet boilerworks'/'C13', '376377').

card_in_set('izzet guildgate', 'C13').
card_original_type('izzet guildgate'/'C13', 'Land — Gate').
card_original_text('izzet guildgate'/'C13', 'Izzet Guildgate enters the battlefield tapped.{T}: Add {U} or {R} to your mana pool.').
card_image_name('izzet guildgate'/'C13', 'izzet guildgate').
card_uid('izzet guildgate'/'C13', 'C13:Izzet Guildgate:izzet guildgate').
card_rarity('izzet guildgate'/'C13', 'Common').
card_artist('izzet guildgate'/'C13', 'Noah Bradley').
card_number('izzet guildgate'/'C13', '297').
card_flavor_text('izzet guildgate'/'C13', 'Enter those with the vision to create and the daring to release their creations.').
card_multiverse_id('izzet guildgate'/'C13', '376378').

card_in_set('jace\'s archivist', 'C13').
card_original_type('jace\'s archivist'/'C13', 'Creature — Vedalken Wizard').
card_original_text('jace\'s archivist'/'C13', '{U}, {T}: Each player discards his or her hand, then draws cards equal to the greatest number of cards a player discarded this way.').
card_image_name('jace\'s archivist'/'C13', 'jace\'s archivist').
card_uid('jace\'s archivist'/'C13', 'C13:Jace\'s Archivist:jace\'s archivist').
card_rarity('jace\'s archivist'/'C13', 'Rare').
card_artist('jace\'s archivist'/'C13', 'James Ryman').
card_number('jace\'s archivist'/'C13', '48').
card_flavor_text('jace\'s archivist'/'C13', '\"This is a sacred duty. Master Beleren\'s trust has bought an eternity of my loyalty.\"').
card_multiverse_id('jace\'s archivist'/'C13', '376379').

card_in_set('jade mage', 'C13').
card_original_type('jade mage'/'C13', 'Creature — Human Shaman').
card_original_text('jade mage'/'C13', '{2}{G}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('jade mage'/'C13', 'jade mage').
card_uid('jade mage'/'C13', 'C13:Jade Mage:jade mage').
card_rarity('jade mage'/'C13', 'Uncommon').
card_artist('jade mage'/'C13', 'Izzy').
card_number('jade mage'/'C13', '151').
card_flavor_text('jade mage'/'C13', '\"We are one with the wild things. Life blooms from our fingertips and nature responds to our summons.\"\n—Jade creed').
card_multiverse_id('jade mage'/'C13', '376380').

card_in_set('jar of eyeballs', 'C13').
card_original_type('jar of eyeballs'/'C13', 'Artifact').
card_original_text('jar of eyeballs'/'C13', 'Whenever a creature you control dies, put two eyeball counters on Jar of Eyeballs.{3}, {T}, Remove all eyeball counters from Jar of Eyeballs: Look at the top X cards of your library, where X is the number of eyeball counters removed this way. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_image_name('jar of eyeballs'/'C13', 'jar of eyeballs').
card_uid('jar of eyeballs'/'C13', 'C13:Jar of Eyeballs:jar of eyeballs').
card_rarity('jar of eyeballs'/'C13', 'Rare').
card_artist('jar of eyeballs'/'C13', 'Jaime Jones').
card_number('jar of eyeballs'/'C13', '244').
card_multiverse_id('jar of eyeballs'/'C13', '376381').

card_in_set('jeleva, nephalia\'s scourge', 'C13').
card_original_type('jeleva, nephalia\'s scourge'/'C13', 'Legendary Creature — Vampire Wizard').
card_original_text('jeleva, nephalia\'s scourge'/'C13', 'FlyingWhen Jeleva, Nephalia\'s Scourge enters the battlefield, each player exiles the top X cards of his or her library, where X is the amount of mana spent to cast Jeleva.Whenever Jeleva attacks, you may cast an instant or sorcery card exiled with it without paying its mana cost.').
card_first_print('jeleva, nephalia\'s scourge', 'C13').
card_image_name('jeleva, nephalia\'s scourge'/'C13', 'jeleva, nephalia\'s scourge').
card_uid('jeleva, nephalia\'s scourge'/'C13', 'C13:Jeleva, Nephalia\'s Scourge:jeleva, nephalia\'s scourge').
card_rarity('jeleva, nephalia\'s scourge'/'C13', 'Mythic Rare').
card_artist('jeleva, nephalia\'s scourge'/'C13', 'Cynthia Sheppard').
card_number('jeleva, nephalia\'s scourge'/'C13', '194').
card_multiverse_id('jeleva, nephalia\'s scourge'/'C13', '376382').

card_in_set('jund charm', 'C13').
card_original_type('jund charm'/'C13', 'Instant').
card_original_text('jund charm'/'C13', 'Choose one — Exile all cards from target player\'s graveyard; or Jund Charm deals 2 damage to each creature; or put two +1/+1 counters on target creature.').
card_image_name('jund charm'/'C13', 'jund charm').
card_uid('jund charm'/'C13', 'C13:Jund Charm:jund charm').
card_rarity('jund charm'/'C13', 'Uncommon').
card_artist('jund charm'/'C13', 'Brandon Kitkouski').
card_number('jund charm'/'C13', '195').
card_flavor_text('jund charm'/'C13', 'Jund pulses with raw energy, waiting for shamans to shape it into useful form.').
card_multiverse_id('jund charm'/'C13', '376383').

card_in_set('jund panorama', 'C13').
card_original_type('jund panorama'/'C13', 'Land').
card_original_text('jund panorama'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}, Sacrifice Jund Panorama: Search your library for a basic Swamp, Mountain, or Forest card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('jund panorama'/'C13', 'jund panorama').
card_uid('jund panorama'/'C13', 'C13:Jund Panorama:jund panorama').
card_rarity('jund panorama'/'C13', 'Common').
card_artist('jund panorama'/'C13', 'Jaime Jones').
card_number('jund panorama'/'C13', '298').
card_flavor_text('jund panorama'/'C13', 'The sky is the dragon\'s throne, the seat from which it issues its fiery decrees.').
card_multiverse_id('jund panorama'/'C13', '376384').

card_in_set('jungle shrine', 'C13').
card_original_type('jungle shrine'/'C13', 'Land').
card_original_text('jungle shrine'/'C13', 'Jungle Shrine enters the battlefield tapped.{T}: Add {R}, {G}, or {W} to your mana pool.').
card_image_name('jungle shrine'/'C13', 'jungle shrine').
card_uid('jungle shrine'/'C13', 'C13:Jungle Shrine:jungle shrine').
card_rarity('jungle shrine'/'C13', 'Uncommon').
card_artist('jungle shrine'/'C13', 'Wayne Reynolds').
card_number('jungle shrine'/'C13', '299').
card_flavor_text('jungle shrine'/'C13', 'On Naya, ambition and treachery are scarce, hunted nearly to extinction by the awe owed to terrestrial gods.').
card_multiverse_id('jungle shrine'/'C13', '376385').

card_in_set('jwar isle refuge', 'C13').
card_original_type('jwar isle refuge'/'C13', 'Land').
card_original_text('jwar isle refuge'/'C13', 'Jwar Isle Refuge enters the battlefield tapped.When Jwar Isle Refuge enters the battlefield, you gain 1 life.{T}: Add {U} or {B} to your mana pool.').
card_image_name('jwar isle refuge'/'C13', 'jwar isle refuge').
card_uid('jwar isle refuge'/'C13', 'C13:Jwar Isle Refuge:jwar isle refuge').
card_rarity('jwar isle refuge'/'C13', 'Uncommon').
card_artist('jwar isle refuge'/'C13', 'Cyril Van Der Haegen').
card_number('jwar isle refuge'/'C13', '300').
card_multiverse_id('jwar isle refuge'/'C13', '376386').

card_in_set('karmic guide', 'C13').
card_original_type('karmic guide'/'C13', 'Creature — Angel Spirit').
card_original_text('karmic guide'/'C13', 'Flying, protection from blackEcho {3}{W}{W} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)When Karmic Guide enters the battlefield, return target creature card from your graveyard to the battlefield.').
card_image_name('karmic guide'/'C13', 'karmic guide').
card_uid('karmic guide'/'C13', 'C13:Karmic Guide:karmic guide').
card_rarity('karmic guide'/'C13', 'Rare').
card_artist('karmic guide'/'C13', 'Allen Williams').
card_number('karmic guide'/'C13', '14').
card_multiverse_id('karmic guide'/'C13', '376387').

card_in_set('kazandu refuge', 'C13').
card_original_type('kazandu refuge'/'C13', 'Land').
card_original_text('kazandu refuge'/'C13', 'Kazandu Refuge enters the battlefield tapped.When Kazandu Refuge enters the battlefield, you gain 1 life.{T}: Add {R} or {G} to your mana pool.').
card_image_name('kazandu refuge'/'C13', 'kazandu refuge').
card_uid('kazandu refuge'/'C13', 'C13:Kazandu Refuge:kazandu refuge').
card_rarity('kazandu refuge'/'C13', 'Uncommon').
card_artist('kazandu refuge'/'C13', 'Franz Vohwinkel').
card_number('kazandu refuge'/'C13', '301').
card_multiverse_id('kazandu refuge'/'C13', '376388').

card_in_set('kazandu tuskcaller', 'C13').
card_original_type('kazandu tuskcaller'/'C13', 'Creature — Human Shaman').
card_original_text('kazandu tuskcaller'/'C13', 'Level up {1}{G} ({1}{G}: Put a level counter on this. Level up only as a sorcery.)LEVEL 2-51/1{T}: Put a 3/3 green Elephant creature token onto the battlefield.LEVEL 6+1/1{T}: Put two 3/3 green Elephant creature tokens onto the battlefield.').
card_image_name('kazandu tuskcaller'/'C13', 'kazandu tuskcaller').
card_uid('kazandu tuskcaller'/'C13', 'C13:Kazandu Tuskcaller:kazandu tuskcaller').
card_rarity('kazandu tuskcaller'/'C13', 'Rare').
card_artist('kazandu tuskcaller'/'C13', 'Mike Bierek').
card_number('kazandu tuskcaller'/'C13', '152').
card_multiverse_id('kazandu tuskcaller'/'C13', '376389').

card_in_set('khalni garden', 'C13').
card_original_type('khalni garden'/'C13', 'Land').
card_original_text('khalni garden'/'C13', 'Khalni Garden enters the battlefield tapped.When Khalni Garden enters the battlefield, put a 0/1 green Plant creature token onto the battlefield.{T}: Add {G} to your mana pool.').
card_image_name('khalni garden'/'C13', 'khalni garden').
card_uid('khalni garden'/'C13', 'C13:Khalni Garden:khalni garden').
card_rarity('khalni garden'/'C13', 'Common').
card_artist('khalni garden'/'C13', 'Ryan Pancoast').
card_number('khalni garden'/'C13', '302').
card_multiverse_id('khalni garden'/'C13', '376390').

card_in_set('kher keep', 'C13').
card_original_type('kher keep'/'C13', 'Legendary Land').
card_original_text('kher keep'/'C13', '{T}: Add {1} to your mana pool.{1}{R}, {T}: Put a 0/1 red Kobold creature token named Kobolds of Kher Keep onto the battlefield.').
card_image_name('kher keep'/'C13', 'kher keep').
card_uid('kher keep'/'C13', 'C13:Kher Keep:kher keep').
card_rarity('kher keep'/'C13', 'Rare').
card_artist('kher keep'/'C13', 'Paolo Parente').
card_number('kher keep'/'C13', '303').
card_flavor_text('kher keep'/'C13', '\"They\'re still here?! The cockroach may have finally met its match.\"\n—Teferi').
card_multiverse_id('kher keep'/'C13', '376391').

card_in_set('kirtar\'s wrath', 'C13').
card_original_type('kirtar\'s wrath'/'C13', 'Sorcery').
card_original_text('kirtar\'s wrath'/'C13', 'Destroy all creatures. They can\'t be regenerated.Threshold — If seven or more cards are in your graveyard, instead destroy all creatures, then put two 1/1 white Spirit creature tokens with flying onto the battlefield. Creatures destroyed this way can\'t be regenerated.').
card_image_name('kirtar\'s wrath'/'C13', 'kirtar\'s wrath').
card_uid('kirtar\'s wrath'/'C13', 'C13:Kirtar\'s Wrath:kirtar\'s wrath').
card_rarity('kirtar\'s wrath'/'C13', 'Rare').
card_artist('kirtar\'s wrath'/'C13', 'Kev Walker').
card_number('kirtar\'s wrath'/'C13', '15').
card_multiverse_id('kirtar\'s wrath'/'C13', '376392').

card_in_set('kongming, \"sleeping dragon\"', 'C13').
card_original_type('kongming, \"sleeping dragon\"'/'C13', 'Legendary Creature — Human Advisor').
card_original_text('kongming, \"sleeping dragon\"'/'C13', 'Other creatures you control get +1/+1.').
card_image_name('kongming, \"sleeping dragon\"'/'C13', 'kongming, sleeping dragon').
card_uid('kongming, \"sleeping dragon\"'/'C13', 'C13:Kongming, \"Sleeping Dragon\":kongming, sleeping dragon').
card_rarity('kongming, \"sleeping dragon\"'/'C13', 'Rare').
card_artist('kongming, \"sleeping dragon\"'/'C13', 'Gao Yan').
card_number('kongming, \"sleeping dragon\"'/'C13', '16').
card_flavor_text('kongming, \"sleeping dragon\"'/'C13', '\"Such a lord as this—all virtues\' height—Had never been, nor ever was again.\"').
card_multiverse_id('kongming, \"sleeping dragon\"'/'C13', '376393').

card_in_set('krosan grip', 'C13').
card_original_type('krosan grip'/'C13', 'Instant').
card_original_text('krosan grip'/'C13', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)Destroy target artifact or enchantment.').
card_image_name('krosan grip'/'C13', 'krosan grip').
card_uid('krosan grip'/'C13', 'C13:Krosan Grip:krosan grip').
card_rarity('krosan grip'/'C13', 'Uncommon').
card_artist('krosan grip'/'C13', 'Zoltan Boros & Gabor Szikszai').
card_number('krosan grip'/'C13', '153').
card_flavor_text('krosan grip'/'C13', '\"Give up these unnatural weapons, these scrolls. Heart and mind and fist are enough.\"\n—Zyd, Kamahlite druid').
card_multiverse_id('krosan grip'/'C13', '376394').

card_in_set('krosan tusker', 'C13').
card_original_type('krosan tusker'/'C13', 'Creature — Boar Beast').
card_original_text('krosan tusker'/'C13', 'Cycling {2}{G} ({2}{G}, Discard this card: Draw a card.)When you cycle Krosan Tusker, you may search your library for a basic land card, reveal that card, put it into your hand, then shuffle your library.').
card_image_name('krosan tusker'/'C13', 'krosan tusker').
card_uid('krosan tusker'/'C13', 'C13:Krosan Tusker:krosan tusker').
card_rarity('krosan tusker'/'C13', 'Common').
card_artist('krosan tusker'/'C13', 'Kev Walker').
card_number('krosan tusker'/'C13', '154').
card_multiverse_id('krosan tusker'/'C13', '376395').

card_in_set('krosan warchief', 'C13').
card_original_type('krosan warchief'/'C13', 'Creature — Beast').
card_original_text('krosan warchief'/'C13', 'Beast spells you cast cost {1} less to cast.{1}{G}: Regenerate target Beast.').
card_image_name('krosan warchief'/'C13', 'krosan warchief').
card_uid('krosan warchief'/'C13', 'C13:Krosan Warchief:krosan warchief').
card_rarity('krosan warchief'/'C13', 'Uncommon').
card_artist('krosan warchief'/'C13', 'Greg Hildebrandt').
card_number('krosan warchief'/'C13', '155').
card_flavor_text('krosan warchief'/'C13', 'It turns prey into predator.').
card_multiverse_id('krosan warchief'/'C13', '376396').

card_in_set('leafdrake roost', 'C13').
card_original_type('leafdrake roost'/'C13', 'Enchantment — Aura').
card_original_text('leafdrake roost'/'C13', 'Enchant landEnchanted land has \"{G}{U}, {T}: Put a 2/2 green and blue Drake creature token with flying onto the battlefield.\"').
card_image_name('leafdrake roost'/'C13', 'leafdrake roost').
card_uid('leafdrake roost'/'C13', 'C13:Leafdrake Roost:leafdrake roost').
card_rarity('leafdrake roost'/'C13', 'Uncommon').
card_artist('leafdrake roost'/'C13', 'Nick Percival').
card_number('leafdrake roost'/'C13', '196').
card_flavor_text('leafdrake roost'/'C13', '\"The best experiments are those whose successes replicate themselves.\"\n—Yolov, Simic bioengineer').
card_multiverse_id('leafdrake roost'/'C13', '376397').

card_in_set('leonin bladetrap', 'C13').
card_original_type('leonin bladetrap'/'C13', 'Artifact').
card_original_text('leonin bladetrap'/'C13', 'Flash (You may cast this spell any time you could cast an instant.){2}, Sacrifice Leonin Bladetrap: Leonin Bladetrap deals 2 damage to each attacking creature without flying.').
card_image_name('leonin bladetrap'/'C13', 'leonin bladetrap').
card_uid('leonin bladetrap'/'C13', 'C13:Leonin Bladetrap:leonin bladetrap').
card_rarity('leonin bladetrap'/'C13', 'Uncommon').
card_artist('leonin bladetrap'/'C13', 'Randy Gallegos').
card_number('leonin bladetrap'/'C13', '245').
card_multiverse_id('leonin bladetrap'/'C13', '376398').

card_in_set('lim-dûl\'s vault', 'C13').
card_original_type('lim-dûl\'s vault'/'C13', 'Instant').
card_original_text('lim-dûl\'s vault'/'C13', 'Look at the top five cards of your library. As many times as you choose, you may pay 1 life, put those cards on the bottom of your library in any order, then look at the top five cards of your library. Then shuffle your library and put the last cards you looked at this way on top of it in any order.').
card_image_name('lim-dûl\'s vault'/'C13', 'lim-dul\'s vault').
card_uid('lim-dûl\'s vault'/'C13', 'C13:Lim-Dûl\'s Vault:lim-dul\'s vault').
card_rarity('lim-dûl\'s vault'/'C13', 'Uncommon').
card_artist('lim-dûl\'s vault'/'C13', 'Wayne England').
card_number('lim-dûl\'s vault'/'C13', '197').
card_multiverse_id('lim-dûl\'s vault'/'C13', '376399').

card_in_set('llanowar reborn', 'C13').
card_original_type('llanowar reborn'/'C13', 'Land').
card_original_text('llanowar reborn'/'C13', 'Llanowar Reborn enters the battlefield tapped.{T}: Add {G} to your mana pool.Graft 1 (This land enters the battlefield with a +1/+1 counter on it. Whenever a creature enters the battlefield, you may move a +1/+1 counter from this land onto it.)').
card_image_name('llanowar reborn'/'C13', 'llanowar reborn').
card_uid('llanowar reborn'/'C13', 'C13:Llanowar Reborn:llanowar reborn').
card_rarity('llanowar reborn'/'C13', 'Uncommon').
card_artist('llanowar reborn'/'C13', 'Philip Straub').
card_number('llanowar reborn'/'C13', '304').
card_multiverse_id('llanowar reborn'/'C13', '376400').

card_in_set('lonely sandbar', 'C13').
card_original_type('lonely sandbar'/'C13', 'Land').
card_original_text('lonely sandbar'/'C13', 'Lonely Sandbar enters the battlefield tapped.{T}: Add {U} to your mana pool.Cycling {U} ({U}, Discard this card: Draw a card.)').
card_image_name('lonely sandbar'/'C13', 'lonely sandbar').
card_uid('lonely sandbar'/'C13', 'C13:Lonely Sandbar:lonely sandbar').
card_rarity('lonely sandbar'/'C13', 'Common').
card_artist('lonely sandbar'/'C13', 'Heather Hudson').
card_number('lonely sandbar'/'C13', '305').
card_multiverse_id('lonely sandbar'/'C13', '376401').

card_in_set('lu xun, scholar general', 'C13').
card_original_type('lu xun, scholar general'/'C13', 'Legendary Creature — Human Soldier').
card_original_text('lu xun, scholar general'/'C13', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)Whenever Lu Xun, Scholar General deals damage to an opponent, you may draw a card.').
card_image_name('lu xun, scholar general'/'C13', 'lu xun, scholar general').
card_uid('lu xun, scholar general'/'C13', 'C13:Lu Xun, Scholar General:lu xun, scholar general').
card_rarity('lu xun, scholar general'/'C13', 'Rare').
card_artist('lu xun, scholar general'/'C13', 'Xu Xiaoming').
card_number('lu xun, scholar general'/'C13', '49').
card_multiverse_id('lu xun, scholar general'/'C13', '376402').

card_in_set('magus of the arena', 'C13').
card_original_type('magus of the arena'/'C13', 'Creature — Human Wizard').
card_original_text('magus of the arena'/'C13', '{3}, {T}: Tap target creature you control and target creature of an opponent\'s choice he or she controls. Those creatures fight each other. (Each deals damage equal to its power to the other.)').
card_image_name('magus of the arena'/'C13', 'magus of the arena').
card_uid('magus of the arena'/'C13', 'C13:Magus of the Arena:magus of the arena').
card_rarity('magus of the arena'/'C13', 'Rare').
card_artist('magus of the arena'/'C13', 'Thomas M. Baxa').
card_number('magus of the arena'/'C13', '115').
card_flavor_text('magus of the arena'/'C13', 'The magus still hears the roar of the crowds. They\'re gone, but the will to compete remains.').
card_multiverse_id('magus of the arena'/'C13', '376403').

card_in_set('marath, will of the wild', 'C13').
card_original_type('marath, will of the wild'/'C13', 'Legendary Creature — Elemental Beast').
card_original_text('marath, will of the wild'/'C13', 'Marath, Will of the Wild enters the battlefield with a number of +1/+1 counters on it equal to the amount of mana spent to cast it.{X}, Remove X +1/+1 counters from Marath: Choose one — Put X +1/+1 counters on target creature; or Marath deals X damage to target creature or player; or put an X/X green Elemental creature token onto the battlefield.').
card_first_print('marath, will of the wild', 'C13').
card_image_name('marath, will of the wild'/'C13', 'marath, will of the wild').
card_uid('marath, will of the wild'/'C13', 'C13:Marath, Will of the Wild:marath, will of the wild').
card_rarity('marath, will of the wild'/'C13', 'Mythic Rare').
card_artist('marath, will of the wild'/'C13', 'Tyler Jacobson').
card_number('marath, will of the wild'/'C13', '198').
card_multiverse_id('marath, will of the wild'/'C13', '376404').

card_in_set('marrow bats', 'C13').
card_original_type('marrow bats'/'C13', 'Creature — Bat Skeleton').
card_original_text('marrow bats'/'C13', 'FlyingPay 4 life: Regenerate Marrow Bats.').
card_image_name('marrow bats'/'C13', 'marrow bats').
card_uid('marrow bats'/'C13', 'C13:Marrow Bats:marrow bats').
card_rarity('marrow bats'/'C13', 'Uncommon').
card_artist('marrow bats'/'C13', 'Jason A. Engle').
card_number('marrow bats'/'C13', '82').
card_flavor_text('marrow bats'/'C13', '\"No matter how far we push into Stensia, undeath will always remain in these lands.\"\n—Terhold, archmage of Drunau').
card_multiverse_id('marrow bats'/'C13', '376405').

card_in_set('mass mutiny', 'C13').
card_original_type('mass mutiny'/'C13', 'Sorcery').
card_original_text('mass mutiny'/'C13', 'For each opponent, gain control of up to one target creature that player controls until end of turn. Untap those creatures. They gain haste until end of turn.').
card_image_name('mass mutiny'/'C13', 'mass mutiny').
card_uid('mass mutiny'/'C13', 'C13:Mass Mutiny:mass mutiny').
card_rarity('mass mutiny'/'C13', 'Rare').
card_artist('mass mutiny'/'C13', 'Carl Critchlow').
card_number('mass mutiny'/'C13', '116').
card_flavor_text('mass mutiny'/'C13', '\"What say you, my most trusted advisors? . . . Advisors?\"\n—Edra, merfolk sovereign').
card_multiverse_id('mass mutiny'/'C13', '376406').

card_in_set('mayael the anima', 'C13').
card_original_type('mayael the anima'/'C13', 'Legendary Creature — Elf Shaman').
card_original_text('mayael the anima'/'C13', '{3}{R}{G}{W}, {T}: Look at the top five cards of your library. You may put a creature card with power 5 or greater from among them onto the battlefield. Put the rest on the bottom of your library in any order.').
card_image_name('mayael the anima'/'C13', 'mayael the anima').
card_uid('mayael the anima'/'C13', 'C13:Mayael the Anima:mayael the anima').
card_rarity('mayael the anima'/'C13', 'Mythic Rare').
card_artist('mayael the anima'/'C13', 'Jason Chan').
card_number('mayael the anima'/'C13', '199').
card_flavor_text('mayael the anima'/'C13', 'The sacred Anima\'s eyes are blind to all but the grandest truths.').
card_multiverse_id('mayael the anima'/'C13', '376407').

card_in_set('mirari', 'C13').
card_original_type('mirari'/'C13', 'Legendary Artifact').
card_original_text('mirari'/'C13', 'Whenever you cast an instant or sorcery spell, you may pay {3}. If you do, copy that spell. You may choose new targets for the copy.').
card_image_name('mirari'/'C13', 'mirari').
card_uid('mirari'/'C13', 'C13:Mirari:mirari').
card_rarity('mirari'/'C13', 'Rare').
card_artist('mirari'/'C13', 'Donato Giancola').
card_number('mirari'/'C13', '246').
card_flavor_text('mirari'/'C13', '\"It offers you what you want, not what you need.\"\n—Braids, dementia summoner').
card_multiverse_id('mirari'/'C13', '376408').

card_in_set('mirror entity', 'C13').
card_original_type('mirror entity'/'C13', 'Creature — Shapeshifter').
card_original_text('mirror entity'/'C13', 'Changeling (This card is every creature type at all times.){X}: Creatures you control become X/X and gain all creature types until end of turn.').
card_image_name('mirror entity'/'C13', 'mirror entity').
card_uid('mirror entity'/'C13', 'C13:Mirror Entity:mirror entity').
card_rarity('mirror entity'/'C13', 'Rare').
card_artist('mirror entity'/'C13', 'Zoltan Boros & Gabor Szikszai').
card_number('mirror entity'/'C13', '17').
card_flavor_text('mirror entity'/'C13', 'Unaware of Lorwyn\'s diversity, it sees only itself, reflected a thousand times over.').
card_multiverse_id('mirror entity'/'C13', '376409').

card_in_set('mistmeadow witch', 'C13').
card_original_type('mistmeadow witch'/'C13', 'Creature — Kithkin Wizard').
card_original_text('mistmeadow witch'/'C13', '{2}{W}{U}: Exile target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_image_name('mistmeadow witch'/'C13', 'mistmeadow witch').
card_uid('mistmeadow witch'/'C13', 'C13:Mistmeadow Witch:mistmeadow witch').
card_rarity('mistmeadow witch'/'C13', 'Uncommon').
card_artist('mistmeadow witch'/'C13', 'Greg Staples').
card_number('mistmeadow witch'/'C13', '230').
card_flavor_text('mistmeadow witch'/'C13', 'Olka collected the evening mist for years, studying its secrets. Once she learned its essence, she could vanish with a thought.').
card_multiverse_id('mistmeadow witch'/'C13', '376410').

card_in_set('mnemonic wall', 'C13').
card_original_type('mnemonic wall'/'C13', 'Creature — Wall').
card_original_text('mnemonic wall'/'C13', 'DefenderWhen Mnemonic Wall enters the battlefield, you may return target instant or sorcery card from your graveyard to your hand.').
card_image_name('mnemonic wall'/'C13', 'mnemonic wall').
card_uid('mnemonic wall'/'C13', 'C13:Mnemonic Wall:mnemonic wall').
card_rarity('mnemonic wall'/'C13', 'Common').
card_artist('mnemonic wall'/'C13', 'Vance Kovacs').
card_number('mnemonic wall'/'C13', '50').
card_flavor_text('mnemonic wall'/'C13', '\"I\'d build an entire fortress of them if I could.\"\n—Mzali, Lighthouse archmage').
card_multiverse_id('mnemonic wall'/'C13', '376411').

card_in_set('mold shambler', 'C13').
card_original_type('mold shambler'/'C13', 'Creature — Fungus Beast').
card_original_text('mold shambler'/'C13', 'Kicker {1}{G} (You may pay an additional {1}{G} as you cast this spell.)When Mold Shambler enters the battlefield, if it was kicked, destroy target noncreature permanent.').
card_image_name('mold shambler'/'C13', 'mold shambler').
card_uid('mold shambler'/'C13', 'C13:Mold Shambler:mold shambler').
card_rarity('mold shambler'/'C13', 'Common').
card_artist('mold shambler'/'C13', 'Karl Kopinski').
card_number('mold shambler'/'C13', '156').
card_flavor_text('mold shambler'/'C13', 'When civilization encroaches on nature, Zendikar encroaches back.').
card_multiverse_id('mold shambler'/'C13', '376412').

card_in_set('molten disaster', 'C13').
card_original_type('molten disaster'/'C13', 'Sorcery').
card_original_text('molten disaster'/'C13', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)If Molten Disaster was kicked, it has split second. (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)Molten Disaster deals X damage to each creature without flying and each player.').
card_image_name('molten disaster'/'C13', 'molten disaster').
card_uid('molten disaster'/'C13', 'C13:Molten Disaster:molten disaster').
card_rarity('molten disaster'/'C13', 'Rare').
card_artist('molten disaster'/'C13', 'Ittoku').
card_number('molten disaster'/'C13', '117').
card_multiverse_id('molten disaster'/'C13', '376413').

card_in_set('molten slagheap', 'C13').
card_original_type('molten slagheap'/'C13', 'Land').
card_original_text('molten slagheap'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}: Put a storage counter on Molten Slagheap.{1}, Remove X storage counters from Molten Slagheap: Add X mana in any combination of {B} and/or {R} to your mana pool.').
card_image_name('molten slagheap'/'C13', 'molten slagheap').
card_uid('molten slagheap'/'C13', 'C13:Molten Slagheap:molten slagheap').
card_rarity('molten slagheap'/'C13', 'Uncommon').
card_artist('molten slagheap'/'C13', 'Daren Bader').
card_number('molten slagheap'/'C13', '306').
card_multiverse_id('molten slagheap'/'C13', '376414').

card_in_set('mosswort bridge', 'C13').
card_original_type('mosswort bridge'/'C13', 'Land').
card_original_text('mosswort bridge'/'C13', 'Hideaway (This land enters the battlefield tapped. When it does, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library.){T}: Add {G} to your mana pool.{G}, {T}: You may play the exiled card without paying its mana cost if creatures you control have total power 10 or greater.').
card_image_name('mosswort bridge'/'C13', 'mosswort bridge').
card_uid('mosswort bridge'/'C13', 'C13:Mosswort Bridge:mosswort bridge').
card_rarity('mosswort bridge'/'C13', 'Rare').
card_artist('mosswort bridge'/'C13', 'Jeremy Jarvis').
card_number('mosswort bridge'/'C13', '307').
card_multiverse_id('mosswort bridge'/'C13', '376415').

card_in_set('mountain', 'C13').
card_original_type('mountain'/'C13', 'Basic Land — Mountain').
card_original_text('mountain'/'C13', 'R').
card_image_name('mountain'/'C13', 'mountain1').
card_uid('mountain'/'C13', 'C13:Mountain:mountain1').
card_rarity('mountain'/'C13', 'Basic Land').
card_artist('mountain'/'C13', 'Cliff Childs').
card_number('mountain'/'C13', '349').
card_multiverse_id('mountain'/'C13', '376417').

card_in_set('mountain', 'C13').
card_original_type('mountain'/'C13', 'Basic Land — Mountain').
card_original_text('mountain'/'C13', 'R').
card_image_name('mountain'/'C13', 'mountain2').
card_uid('mountain'/'C13', 'C13:Mountain:mountain2').
card_rarity('mountain'/'C13', 'Basic Land').
card_artist('mountain'/'C13', 'Jonas De Ro').
card_number('mountain'/'C13', '350').
card_multiverse_id('mountain'/'C13', '376419').

card_in_set('mountain', 'C13').
card_original_type('mountain'/'C13', 'Basic Land — Mountain').
card_original_text('mountain'/'C13', 'R').
card_image_name('mountain'/'C13', 'mountain3').
card_uid('mountain'/'C13', 'C13:Mountain:mountain3').
card_rarity('mountain'/'C13', 'Basic Land').
card_artist('mountain'/'C13', 'Craig Mullins').
card_number('mountain'/'C13', '351').
card_multiverse_id('mountain'/'C13', '376418').

card_in_set('mountain', 'C13').
card_original_type('mountain'/'C13', 'Basic Land — Mountain').
card_original_text('mountain'/'C13', 'R').
card_image_name('mountain'/'C13', 'mountain4').
card_uid('mountain'/'C13', 'C13:Mountain:mountain4').
card_rarity('mountain'/'C13', 'Basic Land').
card_artist('mountain'/'C13', 'Andreas Rocha').
card_number('mountain'/'C13', '352').
card_multiverse_id('mountain'/'C13', '376416').

card_in_set('murkfiend liege', 'C13').
card_original_type('murkfiend liege'/'C13', 'Creature — Horror').
card_original_text('murkfiend liege'/'C13', 'Other green creatures you control get +1/+1.Other blue creatures you control get +1/+1.Untap all green and/or blue creatures you control during each other player\'s untap step.').
card_image_name('murkfiend liege'/'C13', 'murkfiend liege').
card_uid('murkfiend liege'/'C13', 'C13:Murkfiend Liege:murkfiend liege').
card_rarity('murkfiend liege'/'C13', 'Rare').
card_artist('murkfiend liege'/'C13', 'Carl Critchlow').
card_number('murkfiend liege'/'C13', '231').
card_multiverse_id('murkfiend liege'/'C13', '376420').

card_in_set('myr battlesphere', 'C13').
card_original_type('myr battlesphere'/'C13', 'Artifact Creature — Myr Construct').
card_original_text('myr battlesphere'/'C13', 'When Myr Battlesphere enters the battlefield, put four 1/1 colorless Myr artifact creature tokens onto the battlefield.Whenever Myr Battlesphere attacks, you may tap X untapped Myr you control. If you do, Myr Battlesphere gets +X/+0 until end of turn and deals X damage to defending player.').
card_image_name('myr battlesphere'/'C13', 'myr battlesphere').
card_uid('myr battlesphere'/'C13', 'C13:Myr Battlesphere:myr battlesphere').
card_rarity('myr battlesphere'/'C13', 'Rare').
card_artist('myr battlesphere'/'C13', 'Franz Vohwinkel').
card_number('myr battlesphere'/'C13', '247').
card_multiverse_id('myr battlesphere'/'C13', '376421').

card_in_set('mystic barrier', 'C13').
card_original_type('mystic barrier'/'C13', 'Enchantment').
card_original_text('mystic barrier'/'C13', 'When Mystic Barrier enters the battlefield or at the beginning of your upkeep, choose left or right.Each player may attack only the opponent seated nearest him or her in the last chosen direction and planeswalkers controlled by that player.').
card_first_print('mystic barrier', 'C13').
card_image_name('mystic barrier'/'C13', 'mystic barrier').
card_uid('mystic barrier'/'C13', 'C13:Mystic Barrier:mystic barrier').
card_rarity('mystic barrier'/'C13', 'Rare').
card_artist('mystic barrier'/'C13', 'Matt Stewart').
card_number('mystic barrier'/'C13', '18').
card_multiverse_id('mystic barrier'/'C13', '376422').

card_in_set('naya charm', 'C13').
card_original_type('naya charm'/'C13', 'Instant').
card_original_text('naya charm'/'C13', 'Choose one — Naya Charm deals 3 damage to target creature; or return target card from a graveyard to its owner\'s hand; or tap all creatures target player controls.').
card_image_name('naya charm'/'C13', 'naya charm').
card_uid('naya charm'/'C13', 'C13:Naya Charm:naya charm').
card_rarity('naya charm'/'C13', 'Uncommon').
card_artist('naya charm'/'C13', 'Jesper Ejsing').
card_number('naya charm'/'C13', '200').
card_flavor_text('naya charm'/'C13', 'Deep in nature\'s core lies a potential unsullied by greed or civilization.').
card_multiverse_id('naya charm'/'C13', '376423').

card_in_set('naya panorama', 'C13').
card_original_type('naya panorama'/'C13', 'Land').
card_original_text('naya panorama'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}, Sacrifice Naya Panorama: Search your library for a basic Mountain, Forest, or Plains card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('naya panorama'/'C13', 'naya panorama').
card_uid('naya panorama'/'C13', 'C13:Naya Panorama:naya panorama').
card_rarity('naya panorama'/'C13', 'Common').
card_artist('naya panorama'/'C13', 'Hideaki Takamura').
card_number('naya panorama'/'C13', '308').
card_flavor_text('naya panorama'/'C13', 'Between the thunderous footfalls of Naya\'s behemoths lie moments of perfect quiet.').
card_multiverse_id('naya panorama'/'C13', '376424').

card_in_set('naya soulbeast', 'C13').
card_original_type('naya soulbeast'/'C13', 'Creature — Beast').
card_original_text('naya soulbeast'/'C13', 'TrampleWhen you cast Naya Soulbeast, each player reveals the top card of his or her library. Naya Soulbeast enters the battlefield with X +1/+1 counters on it, where X is the total converted mana cost of all cards revealed this way.').
card_first_print('naya soulbeast', 'C13').
card_image_name('naya soulbeast'/'C13', 'naya soulbeast').
card_uid('naya soulbeast'/'C13', 'C13:Naya Soulbeast:naya soulbeast').
card_rarity('naya soulbeast'/'C13', 'Rare').
card_artist('naya soulbeast'/'C13', 'Jesper Ejsing').
card_number('naya soulbeast'/'C13', '157').
card_multiverse_id('naya soulbeast'/'C13', '376425').

card_in_set('nekusar, the mindrazer', 'C13').
card_original_type('nekusar, the mindrazer'/'C13', 'Legendary Creature — Zombie Wizard').
card_original_text('nekusar, the mindrazer'/'C13', 'At the beginning of each player\'s draw step, that player draws an additional card.Whenever an opponent draws a card, Nekusar, the Mindrazer deals 1 damage to that player.').
card_image_name('nekusar, the mindrazer'/'C13', 'nekusar, the mindrazer').
card_uid('nekusar, the mindrazer'/'C13', 'C13:Nekusar, the Mindrazer:nekusar, the mindrazer').
card_rarity('nekusar, the mindrazer'/'C13', 'Mythic Rare').
card_artist('nekusar, the mindrazer'/'C13', 'Mark Winters').
card_number('nekusar, the mindrazer'/'C13', '201').
card_flavor_text('nekusar, the mindrazer'/'C13', 'His enemies wondered if the lich king\'s brutal death and unnatural rebirth had been his plan all along.').
card_multiverse_id('nekusar, the mindrazer'/'C13', '376426').

card_in_set('nevinyrral\'s disk', 'C13').
card_original_type('nevinyrral\'s disk'/'C13', 'Artifact').
card_original_text('nevinyrral\'s disk'/'C13', 'Nevinyrral\'s Disk enters the battlefield tapped.{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_image_name('nevinyrral\'s disk'/'C13', 'nevinyrral\'s disk').
card_uid('nevinyrral\'s disk'/'C13', 'C13:Nevinyrral\'s Disk:nevinyrral\'s disk').
card_rarity('nevinyrral\'s disk'/'C13', 'Rare').
card_artist('nevinyrral\'s disk'/'C13', 'Steve Argyle').
card_number('nevinyrral\'s disk'/'C13', '248').
card_multiverse_id('nevinyrral\'s disk'/'C13', '376427').

card_in_set('new benalia', 'C13').
card_original_type('new benalia'/'C13', 'Land').
card_original_text('new benalia'/'C13', 'New Benalia enters the battlefield tapped.When New Benalia enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.){T}: Add {W} to your mana pool.').
card_image_name('new benalia'/'C13', 'new benalia').
card_uid('new benalia'/'C13', 'C13:New Benalia:new benalia').
card_rarity('new benalia'/'C13', 'Uncommon').
card_artist('new benalia'/'C13', 'Richard Wright').
card_number('new benalia'/'C13', '309').
card_multiverse_id('new benalia'/'C13', '376428').

card_in_set('night soil', 'C13').
card_original_type('night soil'/'C13', 'Enchantment').
card_original_text('night soil'/'C13', '{1}, Exile two creature cards from a single graveyard: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('night soil'/'C13', 'night soil').
card_uid('night soil'/'C13', 'C13:Night Soil:night soil').
card_rarity('night soil'/'C13', 'Common').
card_artist('night soil'/'C13', 'Richard Wright').
card_number('night soil'/'C13', '158').
card_flavor_text('night soil'/'C13', 'Some said killing the thallids only encouraged them.').
card_multiverse_id('night soil'/'C13', '376429').

card_in_set('nightscape familiar', 'C13').
card_original_type('nightscape familiar'/'C13', 'Creature — Zombie').
card_original_text('nightscape familiar'/'C13', 'Blue spells and red spells you cast cost {1} less to cast.{1}{B}: Regenerate Nightscape Familiar.').
card_image_name('nightscape familiar'/'C13', 'nightscape familiar').
card_uid('nightscape familiar'/'C13', 'C13:Nightscape Familiar:nightscape familiar').
card_rarity('nightscape familiar'/'C13', 'Common').
card_artist('nightscape familiar'/'C13', 'Jeff Easley').
card_number('nightscape familiar'/'C13', '83').
card_flavor_text('nightscape familiar'/'C13', 'Nightscape masters don\'t stop at raising the spirit of a fallen battlemage. They raise the flesh along with it.').
card_multiverse_id('nightscape familiar'/'C13', '376430').

card_in_set('nihil spellbomb', 'C13').
card_original_type('nihil spellbomb'/'C13', 'Artifact').
card_original_text('nihil spellbomb'/'C13', '{T}, Sacrifice Nihil Spellbomb: Exile all cards from target player\'s graveyard.When Nihil Spellbomb is put into a graveyard from the battlefield, you may pay {B}. If you do, draw a card.').
card_image_name('nihil spellbomb'/'C13', 'nihil spellbomb').
card_uid('nihil spellbomb'/'C13', 'C13:Nihil Spellbomb:nihil spellbomb').
card_rarity('nihil spellbomb'/'C13', 'Common').
card_artist('nihil spellbomb'/'C13', 'Franz Vohwinkel').
card_number('nihil spellbomb'/'C13', '249').
card_multiverse_id('nihil spellbomb'/'C13', '376431').

card_in_set('nivix guildmage', 'C13').
card_original_type('nivix guildmage'/'C13', 'Creature — Human Wizard').
card_original_text('nivix guildmage'/'C13', '{1}{U}{R}: Draw a card, then discard a card.{2}{U}{R}: Copy target instant or sorcery spell you control. You may choose new targets for the copy.').
card_image_name('nivix guildmage'/'C13', 'nivix guildmage').
card_uid('nivix guildmage'/'C13', 'C13:Nivix Guildmage:nivix guildmage').
card_rarity('nivix guildmage'/'C13', 'Uncommon').
card_artist('nivix guildmage'/'C13', 'Scott M. Fischer').
card_number('nivix guildmage'/'C13', '202').
card_flavor_text('nivix guildmage'/'C13', '\"The only action worth taking is one with an unknown outcome.\"').
card_multiverse_id('nivix guildmage'/'C13', '376432').

card_in_set('obelisk of esper', 'C13').
card_original_type('obelisk of esper'/'C13', 'Artifact').
card_original_text('obelisk of esper'/'C13', '{T}: Add {W}, {U}, or {B} to your mana pool.').
card_image_name('obelisk of esper'/'C13', 'obelisk of esper').
card_uid('obelisk of esper'/'C13', 'C13:Obelisk of Esper:obelisk of esper').
card_rarity('obelisk of esper'/'C13', 'Common').
card_artist('obelisk of esper'/'C13', 'Francis Tsai').
card_number('obelisk of esper'/'C13', '250').
card_flavor_text('obelisk of esper'/'C13', 'It is a monument as austere, unyielding, and inscrutable as Esper itself.').
card_multiverse_id('obelisk of esper'/'C13', '376433').

card_in_set('obelisk of grixis', 'C13').
card_original_type('obelisk of grixis'/'C13', 'Artifact').
card_original_text('obelisk of grixis'/'C13', '{T}: Add {U}, {B}, or {R} to your mana pool.').
card_image_name('obelisk of grixis'/'C13', 'obelisk of grixis').
card_uid('obelisk of grixis'/'C13', 'C13:Obelisk of Grixis:obelisk of grixis').
card_rarity('obelisk of grixis'/'C13', 'Common').
card_artist('obelisk of grixis'/'C13', 'Nils Hamm').
card_number('obelisk of grixis'/'C13', '251').
card_flavor_text('obelisk of grixis'/'C13', 'Like most features of Grixis, the obelisks that remain from the time of Alara now exist only for dark exploitation.').
card_multiverse_id('obelisk of grixis'/'C13', '376434').

card_in_set('obelisk of jund', 'C13').
card_original_type('obelisk of jund'/'C13', 'Artifact').
card_original_text('obelisk of jund'/'C13', '{T}: Add {B}, {R}, or {G} to your mana pool.').
card_image_name('obelisk of jund'/'C13', 'obelisk of jund').
card_uid('obelisk of jund'/'C13', 'C13:Obelisk of Jund:obelisk of jund').
card_rarity('obelisk of jund'/'C13', 'Common').
card_artist('obelisk of jund'/'C13', 'Brandon Kitkouski').
card_number('obelisk of jund'/'C13', '252').
card_flavor_text('obelisk of jund'/'C13', 'Volcanic ash-winds batter it, climbing vines overwhelm it, dragonfire roasts it, and yet it still stands as a testament to a forgotten world.').
card_multiverse_id('obelisk of jund'/'C13', '376435').

card_in_set('oloro, ageless ascetic', 'C13').
card_original_type('oloro, ageless ascetic'/'C13', 'Legendary Creature — Giant Soldier').
card_original_text('oloro, ageless ascetic'/'C13', 'At the beginning of your upkeep, you gain 2 life.Whenever you gain life, you may pay {1}. If you do, draw a card and each opponent loses 1 life.At the beginning of your upkeep, if Oloro, Ageless Ascetic is in the command zone, you gain 2 life.').
card_image_name('oloro, ageless ascetic'/'C13', 'oloro, ageless ascetic').
card_uid('oloro, ageless ascetic'/'C13', 'C13:Oloro, Ageless Ascetic:oloro, ageless ascetic').
card_rarity('oloro, ageless ascetic'/'C13', 'Mythic Rare').
card_artist('oloro, ageless ascetic'/'C13', 'Eric Deschamps').
card_number('oloro, ageless ascetic'/'C13', '203').
card_multiverse_id('oloro, ageless ascetic'/'C13', '376436').

card_in_set('one dozen eyes', 'C13').
card_original_type('one dozen eyes'/'C13', 'Sorcery').
card_original_text('one dozen eyes'/'C13', 'Choose one — Put a 5/5 green Beast creature token onto the battlefield; or put five 1/1 green Insect creature tokens onto the battlefield.Entwine {G}{G}{G} (Choose both if you pay the entwine cost.)').
card_image_name('one dozen eyes'/'C13', 'one dozen eyes').
card_uid('one dozen eyes'/'C13', 'C13:One Dozen Eyes:one dozen eyes').
card_rarity('one dozen eyes'/'C13', 'Uncommon').
card_artist('one dozen eyes'/'C13', 'Darrell Riche').
card_number('one dozen eyes'/'C13', '159').
card_multiverse_id('one dozen eyes'/'C13', '376437').

card_in_set('opal palace', 'C13').
card_original_type('opal palace'/'C13', 'Land').
card_original_text('opal palace'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}: Add to your mana pool one mana of any color in your commander\'s color identity. If you spend this mana to cast your commander, it enters the battlefield with a number of +1/+1 counters on it equal to the number of times it\'s been cast from the command zone this game.').
card_first_print('opal palace', 'C13').
card_image_name('opal palace'/'C13', 'opal palace').
card_uid('opal palace'/'C13', 'C13:Opal Palace:opal palace').
card_rarity('opal palace'/'C13', 'Common').
card_artist('opal palace'/'C13', 'Andreas Rocha').
card_number('opal palace'/'C13', '310').
card_multiverse_id('opal palace'/'C13', '376438').

card_in_set('ophiomancer', 'C13').
card_original_type('ophiomancer'/'C13', 'Creature — Human Shaman').
card_original_text('ophiomancer'/'C13', 'At the beginning of each upkeep, if you control no Snakes, put a 1/1 black Snake creature token with deathtouch onto the battlefield.').
card_first_print('ophiomancer', 'C13').
card_image_name('ophiomancer'/'C13', 'ophiomancer').
card_uid('ophiomancer'/'C13', 'C13:Ophiomancer:ophiomancer').
card_rarity('ophiomancer'/'C13', 'Rare').
card_artist('ophiomancer'/'C13', 'John Stanko').
card_number('ophiomancer'/'C13', '84').
card_flavor_text('ophiomancer'/'C13', '\"There are dark, ancient arts that fascinate even me.\"\n—Sorin Markov').
card_multiverse_id('ophiomancer'/'C13', '376439').

card_in_set('opportunity', 'C13').
card_original_type('opportunity'/'C13', 'Instant').
card_original_text('opportunity'/'C13', 'Target player draws four cards.').
card_image_name('opportunity'/'C13', 'opportunity').
card_uid('opportunity'/'C13', 'C13:Opportunity:opportunity').
card_rarity('opportunity'/'C13', 'Uncommon').
card_artist('opportunity'/'C13', 'Allen Williams').
card_number('opportunity'/'C13', '51').
card_flavor_text('opportunity'/'C13', '\"Opportunity isn\'t something you wait for. It\'s something you create.\"').
card_multiverse_id('opportunity'/'C13', '376440').

card_in_set('order of succession', 'C13').
card_original_type('order of succession'/'C13', 'Sorcery').
card_original_text('order of succession'/'C13', 'Choose left or right. Starting with you and proceeding in the chosen direction, each player chooses a creature controlled by the next player in that direction. Each player gains control of the creature he or she chose.').
card_first_print('order of succession', 'C13').
card_image_name('order of succession'/'C13', 'order of succession').
card_uid('order of succession'/'C13', 'C13:Order of Succession:order of succession').
card_rarity('order of succession'/'C13', 'Rare').
card_artist('order of succession'/'C13', 'Magali Villeneuve').
card_number('order of succession'/'C13', '52').
card_flavor_text('order of succession'/'C13', '\"Long live . . . me.\"').
card_multiverse_id('order of succession'/'C13', '376441').

card_in_set('orzhov basilica', 'C13').
card_original_type('orzhov basilica'/'C13', 'Land').
card_original_text('orzhov basilica'/'C13', 'Orzhov Basilica enters the battlefield tapped.When Orzhov Basilica enters the battlefield, return a land you control to its owner\'s hand.{T}: Add {W}{B} to your mana pool.').
card_image_name('orzhov basilica'/'C13', 'orzhov basilica').
card_uid('orzhov basilica'/'C13', 'C13:Orzhov Basilica:orzhov basilica').
card_rarity('orzhov basilica'/'C13', 'Common').
card_artist('orzhov basilica'/'C13', 'John Avon').
card_number('orzhov basilica'/'C13', '311').
card_multiverse_id('orzhov basilica'/'C13', '376442').

card_in_set('orzhov guildgate', 'C13').
card_original_type('orzhov guildgate'/'C13', 'Land — Gate').
card_original_text('orzhov guildgate'/'C13', 'Orzhov Guildgate enters the battlefield tapped.{T}: Add {W} or {B} to your mana pool.').
card_image_name('orzhov guildgate'/'C13', 'orzhov guildgate').
card_uid('orzhov guildgate'/'C13', 'C13:Orzhov Guildgate:orzhov guildgate').
card_rarity('orzhov guildgate'/'C13', 'Common').
card_artist('orzhov guildgate'/'C13', 'John Avon').
card_number('orzhov guildgate'/'C13', '312').
card_flavor_text('orzhov guildgate'/'C13', 'Enter to find wealth, security, and eternal life . . . for just a small price up front.').
card_multiverse_id('orzhov guildgate'/'C13', '376443').

card_in_set('phantom nantuko', 'C13').
card_original_type('phantom nantuko'/'C13', 'Creature — Insect Spirit').
card_original_text('phantom nantuko'/'C13', 'TramplePhantom Nantuko enters the battlefield with two +1/+1 counters on it.If damage would be dealt to Phantom Nantuko, prevent that damage. Remove a +1/+1 counter from Phantom Nantuko.{T}: Put a +1/+1 counter on Phantom Nantuko.').
card_image_name('phantom nantuko'/'C13', 'phantom nantuko').
card_uid('phantom nantuko'/'C13', 'C13:Phantom Nantuko:phantom nantuko').
card_rarity('phantom nantuko'/'C13', 'Rare').
card_artist('phantom nantuko'/'C13', 'Wayne England').
card_number('phantom nantuko'/'C13', '160').
card_multiverse_id('phantom nantuko'/'C13', '376444').

card_in_set('phthisis', 'C13').
card_original_type('phthisis'/'C13', 'Sorcery').
card_original_text('phthisis'/'C13', 'Destroy target creature. Its controller loses life equal to its power plus its toughness.Suspend 5—{1}{B} (Rather than cast this card from your hand, you may pay {1}{B} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_image_name('phthisis'/'C13', 'phthisis').
card_uid('phthisis'/'C13', 'C13:Phthisis:phthisis').
card_rarity('phthisis'/'C13', 'Uncommon').
card_artist('phthisis'/'C13', 'Carl Critchlow').
card_number('phthisis'/'C13', '85').
card_multiverse_id('phthisis'/'C13', '376445').

card_in_set('phyrexian delver', 'C13').
card_original_type('phyrexian delver'/'C13', 'Creature — Zombie').
card_original_text('phyrexian delver'/'C13', 'When Phyrexian Delver enters the battlefield, return target creature card from your graveyard to the battlefield. You lose life equal to that card\'s converted mana cost.').
card_image_name('phyrexian delver'/'C13', 'phyrexian delver').
card_uid('phyrexian delver'/'C13', 'C13:Phyrexian Delver:phyrexian delver').
card_rarity('phyrexian delver'/'C13', 'Rare').
card_artist('phyrexian delver'/'C13', 'Igor Kieryluk').
card_number('phyrexian delver'/'C13', '86').
card_flavor_text('phyrexian delver'/'C13', 'Because the dead have no will to resist.').
card_multiverse_id('phyrexian delver'/'C13', '376446').

card_in_set('phyrexian gargantua', 'C13').
card_original_type('phyrexian gargantua'/'C13', 'Creature — Horror').
card_original_text('phyrexian gargantua'/'C13', 'When Phyrexian Gargantua enters the battlefield, you draw two cards and you lose 2 life.').
card_image_name('phyrexian gargantua'/'C13', 'phyrexian gargantua').
card_uid('phyrexian gargantua'/'C13', 'C13:Phyrexian Gargantua:phyrexian gargantua').
card_rarity('phyrexian gargantua'/'C13', 'Uncommon').
card_artist('phyrexian gargantua'/'C13', 'Igor Kieryluk').
card_number('phyrexian gargantua'/'C13', '87').
card_flavor_text('phyrexian gargantua'/'C13', 'Other Phyrexians have nightmares about the gargantua.').
card_multiverse_id('phyrexian gargantua'/'C13', '376447').

card_in_set('phyrexian reclamation', 'C13').
card_original_type('phyrexian reclamation'/'C13', 'Enchantment').
card_original_text('phyrexian reclamation'/'C13', '{1}{B}, Pay 2 life: Return target creature card from your graveyard to your hand.').
card_image_name('phyrexian reclamation'/'C13', 'phyrexian reclamation').
card_uid('phyrexian reclamation'/'C13', 'C13:Phyrexian Reclamation:phyrexian reclamation').
card_rarity('phyrexian reclamation'/'C13', 'Uncommon').
card_artist('phyrexian reclamation'/'C13', 'rk post').
card_number('phyrexian reclamation'/'C13', '88').
card_flavor_text('phyrexian reclamation'/'C13', 'Death is no excuse to stop working.').
card_multiverse_id('phyrexian reclamation'/'C13', '376448').

card_in_set('pilgrim\'s eye', 'C13').
card_original_type('pilgrim\'s eye'/'C13', 'Artifact Creature — Thopter').
card_original_text('pilgrim\'s eye'/'C13', 'FlyingWhen Pilgrim\'s Eye enters the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_image_name('pilgrim\'s eye'/'C13', 'pilgrim\'s eye').
card_uid('pilgrim\'s eye'/'C13', 'C13:Pilgrim\'s Eye:pilgrim\'s eye').
card_rarity('pilgrim\'s eye'/'C13', 'Common').
card_artist('pilgrim\'s eye'/'C13', 'Dan Scott').
card_number('pilgrim\'s eye'/'C13', '253').
card_flavor_text('pilgrim\'s eye'/'C13', 'The kor send their thopter kites to see if the land is in a welcoming mood.').
card_multiverse_id('pilgrim\'s eye'/'C13', '376449').

card_in_set('plague boiler', 'C13').
card_original_type('plague boiler'/'C13', 'Artifact').
card_original_text('plague boiler'/'C13', 'At the beginning of your upkeep, put a plague counter on Plague Boiler.{1}{B}{G}: Put a plague counter on Plague Boiler or remove a plague counter from it.When Plague Boiler has three or more plague counters on it, sacrifice it. If you do, destroy all nonland permanents.').
card_image_name('plague boiler'/'C13', 'plague boiler').
card_uid('plague boiler'/'C13', 'C13:Plague Boiler:plague boiler').
card_rarity('plague boiler'/'C13', 'Rare').
card_artist('plague boiler'/'C13', 'Mark Tedin').
card_number('plague boiler'/'C13', '254').
card_multiverse_id('plague boiler'/'C13', '376450').

card_in_set('plains', 'C13').
card_original_type('plains'/'C13', 'Basic Land — Plains').
card_original_text('plains'/'C13', 'W').
card_image_name('plains'/'C13', 'plains1').
card_uid('plains'/'C13', 'C13:Plains:plains1').
card_rarity('plains'/'C13', 'Basic Land').
card_artist('plains'/'C13', 'John Avon').
card_number('plains'/'C13', '337').
card_multiverse_id('plains'/'C13', '376452').

card_in_set('plains', 'C13').
card_original_type('plains'/'C13', 'Basic Land — Plains').
card_original_text('plains'/'C13', 'W').
card_image_name('plains'/'C13', 'plains2').
card_uid('plains'/'C13', 'C13:Plains:plains2').
card_rarity('plains'/'C13', 'Basic Land').
card_artist('plains'/'C13', 'Michael Komarck').
card_number('plains'/'C13', '338').
card_multiverse_id('plains'/'C13', '376454').

card_in_set('plains', 'C13').
card_original_type('plains'/'C13', 'Basic Land — Plains').
card_original_text('plains'/'C13', 'W').
card_image_name('plains'/'C13', 'plains3').
card_uid('plains'/'C13', 'C13:Plains:plains3').
card_rarity('plains'/'C13', 'Basic Land').
card_artist('plains'/'C13', 'Jung Park').
card_number('plains'/'C13', '339').
card_multiverse_id('plains'/'C13', '376451').

card_in_set('plains', 'C13').
card_original_type('plains'/'C13', 'Basic Land — Plains').
card_original_text('plains'/'C13', 'W').
card_image_name('plains'/'C13', 'plains4').
card_uid('plains'/'C13', 'C13:Plains:plains4').
card_rarity('plains'/'C13', 'Basic Land').
card_artist('plains'/'C13', 'Andreas Rocha').
card_number('plains'/'C13', '340').
card_multiverse_id('plains'/'C13', '376453').

card_in_set('presence of gond', 'C13').
card_original_type('presence of gond'/'C13', 'Enchantment — Aura').
card_original_text('presence of gond'/'C13', 'Enchant creatureEnchanted creature has \"{T}: Put a 1/1 green Elf Warrior creature token onto the battlefield.\"').
card_image_name('presence of gond'/'C13', 'presence of gond').
card_uid('presence of gond'/'C13', 'C13:Presence of Gond:presence of gond').
card_rarity('presence of gond'/'C13', 'Common').
card_artist('presence of gond'/'C13', 'Brandon Kitkouski').
card_number('presence of gond'/'C13', '161').
card_flavor_text('presence of gond'/'C13', '\"Here lies Gond, hero of Safehold Taldwen. May he ever guide our quest.\"').
card_multiverse_id('presence of gond'/'C13', '376455').

card_in_set('price of knowledge', 'C13').
card_original_type('price of knowledge'/'C13', 'Enchantment').
card_original_text('price of knowledge'/'C13', 'Players have no maximum hand size.At the beginning of each opponent\'s upkeep, Price of Knowledge deals damage to that player equal to the number of cards in that player\'s hand.').
card_first_print('price of knowledge', 'C13').
card_image_name('price of knowledge'/'C13', 'price of knowledge').
card_uid('price of knowledge'/'C13', 'C13:Price of Knowledge:price of knowledge').
card_rarity('price of knowledge'/'C13', 'Rare').
card_artist('price of knowledge'/'C13', 'Dan Scott').
card_number('price of knowledge'/'C13', '89').
card_multiverse_id('price of knowledge'/'C13', '376456').

card_in_set('primal vigor', 'C13').
card_original_type('primal vigor'/'C13', 'Enchantment').
card_original_text('primal vigor'/'C13', 'If one or more tokens would be put onto the battlefield, twice that many of those tokens are put onto the battlefield instead.If one or more +1/+1 counters would be placed on a creature, twice that many +1/+1 counters are placed on that creature instead.').
card_first_print('primal vigor', 'C13').
card_image_name('primal vigor'/'C13', 'primal vigor').
card_uid('primal vigor'/'C13', 'C13:Primal Vigor:primal vigor').
card_rarity('primal vigor'/'C13', 'Rare').
card_artist('primal vigor'/'C13', 'Matt Stewart').
card_number('primal vigor'/'C13', '162').
card_multiverse_id('primal vigor'/'C13', '376457').

card_in_set('pristine talisman', 'C13').
card_original_type('pristine talisman'/'C13', 'Artifact').
card_original_text('pristine talisman'/'C13', '{T}: Add {1} to your mana pool. You gain 1 life.').
card_image_name('pristine talisman'/'C13', 'pristine talisman').
card_uid('pristine talisman'/'C13', 'C13:Pristine Talisman:pristine talisman').
card_rarity('pristine talisman'/'C13', 'Common').
card_artist('pristine talisman'/'C13', 'Matt Cavotta').
card_number('pristine talisman'/'C13', '255').
card_flavor_text('pristine talisman'/'C13', '\"Tools and artisans can be destroyed, but the act of creation is inviolate.\"\n—Elspeth Tirel').
card_multiverse_id('pristine talisman'/'C13', '376458').

card_in_set('propaganda', 'C13').
card_original_type('propaganda'/'C13', 'Enchantment').
card_original_text('propaganda'/'C13', 'Creatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_image_name('propaganda'/'C13', 'propaganda').
card_uid('propaganda'/'C13', 'C13:Propaganda:propaganda').
card_rarity('propaganda'/'C13', 'Uncommon').
card_artist('propaganda'/'C13', 'Clint Cearley').
card_number('propaganda'/'C13', '53').
card_flavor_text('propaganda'/'C13', '\"The cost of your opposition will be extracted from your wits.\"\n—Nekusar, the Mindrazer').
card_multiverse_id('propaganda'/'C13', '376459').

card_in_set('prosperity', 'C13').
card_original_type('prosperity'/'C13', 'Sorcery').
card_original_text('prosperity'/'C13', 'Each player draws X cards.').
card_image_name('prosperity'/'C13', 'prosperity').
card_uid('prosperity'/'C13', 'C13:Prosperity:prosperity').
card_rarity('prosperity'/'C13', 'Uncommon').
card_artist('prosperity'/'C13', 'Nic Klein').
card_number('prosperity'/'C13', '54').
card_flavor_text('prosperity'/'C13', '\"True riches are found in the mazes of the mind.\"\n—Tamiyo, the Moon Sage').
card_multiverse_id('prosperity'/'C13', '376460').

card_in_set('prossh, skyraider of kher', 'C13').
card_original_type('prossh, skyraider of kher'/'C13', 'Legendary Creature — Dragon').
card_original_text('prossh, skyraider of kher'/'C13', 'FlyingWhen you cast Prossh, Skyraider of Kher, put X 0/1 red Kobold creature tokens named Kobolds of Kher Keep onto the battlefield, where X is the amount of mana spent to cast Prossh.Sacrifice another creature: Prossh gets +1/+0 until end of turn.').
card_first_print('prossh, skyraider of kher', 'C13').
card_image_name('prossh, skyraider of kher'/'C13', 'prossh, skyraider of kher').
card_uid('prossh, skyraider of kher'/'C13', 'C13:Prossh, Skyraider of Kher:prossh, skyraider of kher').
card_rarity('prossh, skyraider of kher'/'C13', 'Mythic Rare').
card_artist('prossh, skyraider of kher'/'C13', 'Todd Lockwood').
card_number('prossh, skyraider of kher'/'C13', '204').
card_multiverse_id('prossh, skyraider of kher'/'C13', '376461').

card_in_set('quagmire druid', 'C13').
card_original_type('quagmire druid'/'C13', 'Creature — Zombie Druid').
card_original_text('quagmire druid'/'C13', '{G}, {T}, Sacrifice a creature: Destroy target enchantment.').
card_image_name('quagmire druid'/'C13', 'quagmire druid').
card_uid('quagmire druid'/'C13', 'C13:Quagmire Druid:quagmire druid').
card_rarity('quagmire druid'/'C13', 'Common').
card_artist('quagmire druid'/'C13', 'Jaime Jones').
card_number('quagmire druid'/'C13', '90').
card_flavor_text('quagmire druid'/'C13', 'As the druids had devoted their lives to preserving Dominaria, so did they devote their deaths.').
card_multiverse_id('quagmire druid'/'C13', '376462').

card_in_set('rain of thorns', 'C13').
card_original_type('rain of thorns'/'C13', 'Sorcery').
card_original_text('rain of thorns'/'C13', 'Choose one or more — Destroy target artifact; destroy target enchantment; and/or destroy target land.').
card_image_name('rain of thorns'/'C13', 'rain of thorns').
card_uid('rain of thorns'/'C13', 'C13:Rain of Thorns:rain of thorns').
card_rarity('rain of thorns'/'C13', 'Uncommon').
card_artist('rain of thorns'/'C13', 'Sam Burley').
card_number('rain of thorns'/'C13', '163').
card_flavor_text('rain of thorns'/'C13', 'When the forests became havens for evil, the archmages devised new ways to cleanse the wilds.').
card_multiverse_id('rain of thorns'/'C13', '376463').

card_in_set('rakdos carnarium', 'C13').
card_original_type('rakdos carnarium'/'C13', 'Land').
card_original_text('rakdos carnarium'/'C13', 'Rakdos Carnarium enters the battlefield tapped.When Rakdos Carnarium enters the battlefield, return a land you control to its owner\'s hand.{T}: Add {B}{R} to your mana pool.').
card_image_name('rakdos carnarium'/'C13', 'rakdos carnarium').
card_uid('rakdos carnarium'/'C13', 'C13:Rakdos Carnarium:rakdos carnarium').
card_rarity('rakdos carnarium'/'C13', 'Common').
card_artist('rakdos carnarium'/'C13', 'John Avon').
card_number('rakdos carnarium'/'C13', '313').
card_multiverse_id('rakdos carnarium'/'C13', '376464').

card_in_set('rakdos guildgate', 'C13').
card_original_type('rakdos guildgate'/'C13', 'Land — Gate').
card_original_text('rakdos guildgate'/'C13', 'Rakdos Guildgate enters the battlefield tapped.{T}: Add {B} or {R} to your mana pool.').
card_image_name('rakdos guildgate'/'C13', 'rakdos guildgate').
card_uid('rakdos guildgate'/'C13', 'C13:Rakdos Guildgate:rakdos guildgate').
card_rarity('rakdos guildgate'/'C13', 'Common').
card_artist('rakdos guildgate'/'C13', 'Eytan Zana').
card_number('rakdos guildgate'/'C13', '314').
card_flavor_text('rakdos guildgate'/'C13', 'Enter and indulge your darkest fantasies, for you may never pass this way again.').
card_multiverse_id('rakdos guildgate'/'C13', '376465').

card_in_set('rakeclaw gargantuan', 'C13').
card_original_type('rakeclaw gargantuan'/'C13', 'Creature — Beast').
card_original_text('rakeclaw gargantuan'/'C13', '{1}: Target creature with power 5 or greater gains first strike until end of turn.').
card_image_name('rakeclaw gargantuan'/'C13', 'rakeclaw gargantuan').
card_uid('rakeclaw gargantuan'/'C13', 'C13:Rakeclaw Gargantuan:rakeclaw gargantuan').
card_rarity('rakeclaw gargantuan'/'C13', 'Common').
card_artist('rakeclaw gargantuan'/'C13', 'Jesper Ejsing').
card_number('rakeclaw gargantuan'/'C13', '205').
card_flavor_text('rakeclaw gargantuan'/'C13', 'Naya teems with gargantuans, titanic monsters to whom both nature and civilization defer.').
card_multiverse_id('rakeclaw gargantuan'/'C13', '376466').

card_in_set('rampaging baloths', 'C13').
card_original_type('rampaging baloths'/'C13', 'Creature — Beast').
card_original_text('rampaging baloths'/'C13', 'TrampleLandfall — Whenever a land enters the battlefield under your control, you may put a 4/4 green Beast creature token onto the battlefield.').
card_image_name('rampaging baloths'/'C13', 'rampaging baloths').
card_uid('rampaging baloths'/'C13', 'C13:Rampaging Baloths:rampaging baloths').
card_rarity('rampaging baloths'/'C13', 'Mythic Rare').
card_artist('rampaging baloths'/'C13', 'Steve Prescott').
card_number('rampaging baloths'/'C13', '164').
card_flavor_text('rampaging baloths'/'C13', '\"When the land is angry, so are they.\"\n—Nissa Revane').
card_multiverse_id('rampaging baloths'/'C13', '376467').

card_in_set('raven familiar', 'C13').
card_original_type('raven familiar'/'C13', 'Creature — Bird').
card_original_text('raven familiar'/'C13', 'FlyingEcho {2}{U} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)When Raven Familiar enters the battlefield, look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_image_name('raven familiar'/'C13', 'raven familiar').
card_uid('raven familiar'/'C13', 'C13:Raven Familiar:raven familiar').
card_rarity('raven familiar'/'C13', 'Uncommon').
card_artist('raven familiar'/'C13', 'John Avon').
card_number('raven familiar'/'C13', '55').
card_multiverse_id('raven familiar'/'C13', '376468').

card_in_set('ravenous baloth', 'C13').
card_original_type('ravenous baloth'/'C13', 'Creature — Beast').
card_original_text('ravenous baloth'/'C13', 'Sacrifice a Beast: You gain 4 life.').
card_image_name('ravenous baloth'/'C13', 'ravenous baloth').
card_uid('ravenous baloth'/'C13', 'C13:Ravenous Baloth:ravenous baloth').
card_rarity('ravenous baloth'/'C13', 'Rare').
card_artist('ravenous baloth'/'C13', 'Todd Lockwood').
card_number('ravenous baloth'/'C13', '165').
card_flavor_text('ravenous baloth'/'C13', '\"All we know about the Krosan Forest we have learned from those few who made it out alive.\"\n—Elvish refugee').
card_multiverse_id('ravenous baloth'/'C13', '376469').

card_in_set('razor hippogriff', 'C13').
card_original_type('razor hippogriff'/'C13', 'Creature — Hippogriff').
card_original_text('razor hippogriff'/'C13', 'FlyingWhen Razor Hippogriff enters the battlefield, return target artifact card from your graveyard to your hand. You gain life equal to that card\'s converted mana cost.').
card_image_name('razor hippogriff'/'C13', 'razor hippogriff').
card_uid('razor hippogriff'/'C13', 'C13:Razor Hippogriff:razor hippogriff').
card_rarity('razor hippogriff'/'C13', 'Uncommon').
card_artist('razor hippogriff'/'C13', 'David Rapoza').
card_number('razor hippogriff'/'C13', '19').
card_flavor_text('razor hippogriff'/'C13', 'She incubates her eggs in gold and mana.').
card_multiverse_id('razor hippogriff'/'C13', '376470').

card_in_set('reckless spite', 'C13').
card_original_type('reckless spite'/'C13', 'Instant').
card_original_text('reckless spite'/'C13', 'Destroy two target nonblack creatures. You lose 5 life.').
card_image_name('reckless spite'/'C13', 'reckless spite').
card_uid('reckless spite'/'C13', 'C13:Reckless Spite:reckless spite').
card_rarity('reckless spite'/'C13', 'Uncommon').
card_artist('reckless spite'/'C13', 'Karl Kopinski').
card_number('reckless spite'/'C13', '91').
card_flavor_text('reckless spite'/'C13', '\"The end is justified in being mean. Or something like that.\"').
card_multiverse_id('reckless spite'/'C13', '376471').

card_in_set('reincarnation', 'C13').
card_original_type('reincarnation'/'C13', 'Instant').
card_original_text('reincarnation'/'C13', 'Choose target creature. When that creature dies this turn, return a creature card from its owner\'s graveyard to the battlefield under the control of that creature\'s owner.').
card_image_name('reincarnation'/'C13', 'reincarnation').
card_uid('reincarnation'/'C13', 'C13:Reincarnation:reincarnation').
card_rarity('reincarnation'/'C13', 'Uncommon').
card_artist('reincarnation'/'C13', 'Steve Prescott').
card_number('reincarnation'/'C13', '166').
card_flavor_text('reincarnation'/'C13', '\"You cannot imagine what eyes you may look through tomorrow.\"\n—Aerona, elder druid').
card_multiverse_id('reincarnation'/'C13', '376472').

card_in_set('restore', 'C13').
card_original_type('restore'/'C13', 'Sorcery').
card_original_text('restore'/'C13', 'Put target land card from a graveyard onto the battlefield under your control.').
card_first_print('restore', 'C13').
card_image_name('restore'/'C13', 'restore').
card_uid('restore'/'C13', 'C13:Restore:restore').
card_rarity('restore'/'C13', 'Uncommon').
card_artist('restore'/'C13', 'John Avon').
card_number('restore'/'C13', '167').
card_flavor_text('restore'/'C13', '\"See, the land itself raises an altar to the ascendancy of nature.\"\n—Mayael the Anima').
card_multiverse_id('restore'/'C13', '376473').

card_in_set('roon of the hidden realm', 'C13').
card_original_type('roon of the hidden realm'/'C13', 'Legendary Creature — Rhino Soldier').
card_original_text('roon of the hidden realm'/'C13', 'Vigilance, trample{2}, {T}: Exile another target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_first_print('roon of the hidden realm', 'C13').
card_image_name('roon of the hidden realm'/'C13', 'roon of the hidden realm').
card_uid('roon of the hidden realm'/'C13', 'C13:Roon of the Hidden Realm:roon of the hidden realm').
card_rarity('roon of the hidden realm'/'C13', 'Mythic Rare').
card_artist('roon of the hidden realm'/'C13', 'Steve Prescott').
card_number('roon of the hidden realm'/'C13', '206').
card_flavor_text('roon of the hidden realm'/'C13', '\"Time and space are just new battlefields to be mastered.\"').
card_multiverse_id('roon of the hidden realm'/'C13', '376474').

card_in_set('rough', 'C13').
card_original_type('rough'/'C13', 'Sorcery').
card_original_text('rough'/'C13', 'Rough deals 2 damage to each creature without flying.\n//').
card_image_name('rough'/'C13', 'roughtumble').
card_uid('rough'/'C13', 'C13:Rough:roughtumble').
card_rarity('rough'/'C13', 'Uncommon').
card_artist('rough'/'C13', 'Luca Zontini').
card_number('rough'/'C13', '118a').
card_multiverse_id('rough'/'C13', '376475').

card_in_set('rubinia soulsinger', 'C13').
card_original_type('rubinia soulsinger'/'C13', 'Legendary Creature — Faerie').
card_original_text('rubinia soulsinger'/'C13', 'You may choose not to untap Rubinia Soulsinger during your untap step.{T}: Gain control of target creature for as long as you control Rubinia and Rubinia remains tapped.').
card_image_name('rubinia soulsinger'/'C13', 'rubinia soulsinger').
card_uid('rubinia soulsinger'/'C13', 'C13:Rubinia Soulsinger:rubinia soulsinger').
card_rarity('rubinia soulsinger'/'C13', 'Rare').
card_artist('rubinia soulsinger'/'C13', 'Cynthia Sheppard').
card_number('rubinia soulsinger'/'C13', '207').
card_flavor_text('rubinia soulsinger'/'C13', '\"Take up the quill, and feel your future flow into the runes.\"').
card_multiverse_id('rubinia soulsinger'/'C13', '376476').

card_in_set('rupture spire', 'C13').
card_original_type('rupture spire'/'C13', 'Land').
card_original_text('rupture spire'/'C13', 'Rupture Spire enters the battlefield tapped.When Rupture Spire enters the battlefield, sacrifice it unless you pay {1}.{T}: Add one mana of any color to your mana pool.').
card_image_name('rupture spire'/'C13', 'rupture spire').
card_uid('rupture spire'/'C13', 'C13:Rupture Spire:rupture spire').
card_rarity('rupture spire'/'C13', 'Common').
card_artist('rupture spire'/'C13', 'Jaime Jones').
card_number('rupture spire'/'C13', '315').
card_multiverse_id('rupture spire'/'C13', '376477').

card_in_set('sakura-tribe elder', 'C13').
card_original_type('sakura-tribe elder'/'C13', 'Creature — Snake Shaman').
card_original_text('sakura-tribe elder'/'C13', 'Sacrifice Sakura-Tribe Elder: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.').
card_image_name('sakura-tribe elder'/'C13', 'sakura-tribe elder').
card_uid('sakura-tribe elder'/'C13', 'C13:Sakura-Tribe Elder:sakura-tribe elder').
card_rarity('sakura-tribe elder'/'C13', 'Common').
card_artist('sakura-tribe elder'/'C13', 'Carl Critchlow').
card_number('sakura-tribe elder'/'C13', '168').
card_flavor_text('sakura-tribe elder'/'C13', 'Slain warriors were buried with a tree sapling, so they would become a part of the forest after death.').
card_multiverse_id('sakura-tribe elder'/'C13', '376478').

card_in_set('saltcrusted steppe', 'C13').
card_original_type('saltcrusted steppe'/'C13', 'Land').
card_original_text('saltcrusted steppe'/'C13', '{T}: Add {1} to your mana pool.{1}, {T}: Put a storage counter on Saltcrusted Steppe.{1}, Remove X storage counters from Saltcrusted Steppe: Add X mana in any combination of {G} and/or {W} to your mana pool.').
card_image_name('saltcrusted steppe'/'C13', 'saltcrusted steppe').
card_uid('saltcrusted steppe'/'C13', 'C13:Saltcrusted Steppe:saltcrusted steppe').
card_rarity('saltcrusted steppe'/'C13', 'Uncommon').
card_artist('saltcrusted steppe'/'C13', 'Greg Staples').
card_number('saltcrusted steppe'/'C13', '316').
card_multiverse_id('saltcrusted steppe'/'C13', '376479').

card_in_set('sanguine bond', 'C13').
card_original_type('sanguine bond'/'C13', 'Enchantment').
card_original_text('sanguine bond'/'C13', 'Whenever you gain life, target opponent loses that much life.').
card_image_name('sanguine bond'/'C13', 'sanguine bond').
card_uid('sanguine bond'/'C13', 'C13:Sanguine Bond:sanguine bond').
card_rarity('sanguine bond'/'C13', 'Rare').
card_artist('sanguine bond'/'C13', 'Jaime Jones').
card_number('sanguine bond'/'C13', '92').
card_flavor_text('sanguine bond'/'C13', '\"Blood is constant. Every drop I drink, someone must bleed.\"\n—Vradeen, vampire nocturnus').
card_multiverse_id('sanguine bond'/'C13', '376480').

card_in_set('savage lands', 'C13').
card_original_type('savage lands'/'C13', 'Land').
card_original_text('savage lands'/'C13', 'Savage Lands enters the battlefield tapped.{T}: Add {B}, {R}, or {G} to your mana pool.').
card_image_name('savage lands'/'C13', 'savage lands').
card_uid('savage lands'/'C13', 'C13:Savage Lands:savage lands').
card_rarity('savage lands'/'C13', 'Uncommon').
card_artist('savage lands'/'C13', 'Vance Kovacs').
card_number('savage lands'/'C13', '317').
card_flavor_text('savage lands'/'C13', 'Jund is a world as cruel as those who call it home. Their brutal struggles scar the land even as it carves them in its image, a vicious circle spiraling out of control.').
card_multiverse_id('savage lands'/'C13', '376481').

card_in_set('savage twister', 'C13').
card_original_type('savage twister'/'C13', 'Sorcery').
card_original_text('savage twister'/'C13', 'Savage Twister deals X damage to each creature.').
card_image_name('savage twister'/'C13', 'savage twister').
card_uid('savage twister'/'C13', 'C13:Savage Twister:savage twister').
card_rarity('savage twister'/'C13', 'Uncommon').
card_artist('savage twister'/'C13', 'John Avon').
card_number('savage twister'/'C13', '208').
card_flavor_text('savage twister'/'C13', '\"Nature is the ultimate mindless destroyer, capable of power and ferocity no army can match, and the Gruul follow its example.\"\n—Trigori, Azorius senator').
card_multiverse_id('savage twister'/'C13', '376482').

card_in_set('scarland thrinax', 'C13').
card_original_type('scarland thrinax'/'C13', 'Creature — Lizard').
card_original_text('scarland thrinax'/'C13', 'Sacrifice a creature: Put a +1/+1 counter on Scarland Thrinax.').
card_image_name('scarland thrinax'/'C13', 'scarland thrinax').
card_uid('scarland thrinax'/'C13', 'C13:Scarland Thrinax:scarland thrinax').
card_rarity('scarland thrinax'/'C13', 'Uncommon').
card_artist('scarland thrinax'/'C13', 'Daarken').
card_number('scarland thrinax'/'C13', '209').
card_flavor_text('scarland thrinax'/'C13', '\"There is only one way of life in Jund: feed on the weak until you are cut down by something stronger.\"\n—Jorshu of Clan Nel Toth').
card_multiverse_id('scarland thrinax'/'C13', '376483').

card_in_set('seaside citadel', 'C13').
card_original_type('seaside citadel'/'C13', 'Land').
card_original_text('seaside citadel'/'C13', 'Seaside Citadel enters the battlefield tapped.{T}: Add {G}, {W}, or {U} to your mana pool.').
card_image_name('seaside citadel'/'C13', 'seaside citadel').
card_uid('seaside citadel'/'C13', 'C13:Seaside Citadel:seaside citadel').
card_rarity('seaside citadel'/'C13', 'Uncommon').
card_artist('seaside citadel'/'C13', 'Volkan Baga').
card_number('seaside citadel'/'C13', '318').
card_flavor_text('seaside citadel'/'C13', 'For wisdom\'s sake, it was built high to gaze on all things. For glory\'s sake, it was built high as a testament of power. For strength\'s sake, it was built high to repel all attacks.').
card_multiverse_id('seaside citadel'/'C13', '376484').

card_in_set('secluded steppe', 'C13').
card_original_type('secluded steppe'/'C13', 'Land').
card_original_text('secluded steppe'/'C13', 'Secluded Steppe enters the battlefield tapped.{T}: Add {W} to your mana pool.Cycling {W} ({W}, Discard this card: Draw a card.)').
card_image_name('secluded steppe'/'C13', 'secluded steppe').
card_uid('secluded steppe'/'C13', 'C13:Secluded Steppe:secluded steppe').
card_rarity('secluded steppe'/'C13', 'Common').
card_artist('secluded steppe'/'C13', 'Heather Hudson').
card_number('secluded steppe'/'C13', '319').
card_multiverse_id('secluded steppe'/'C13', '376485').

card_in_set('seer\'s sundial', 'C13').
card_original_type('seer\'s sundial'/'C13', 'Artifact').
card_original_text('seer\'s sundial'/'C13', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {2}. If you do, draw a card.').
card_image_name('seer\'s sundial'/'C13', 'seer\'s sundial').
card_uid('seer\'s sundial'/'C13', 'C13:Seer\'s Sundial:seer\'s sundial').
card_rarity('seer\'s sundial'/'C13', 'Rare').
card_artist('seer\'s sundial'/'C13', 'Franz Vohwinkel').
card_number('seer\'s sundial'/'C13', '256').
card_flavor_text('seer\'s sundial'/'C13', '\"The shadow travels toward the apex. I predict we will soon see the true measure of darkness.\"').
card_multiverse_id('seer\'s sundial'/'C13', '376486').

card_in_set('sejiri refuge', 'C13').
card_original_type('sejiri refuge'/'C13', 'Land').
card_original_text('sejiri refuge'/'C13', 'Sejiri Refuge enters the battlefield tapped.When Sejiri Refuge enters the battlefield, you gain 1 life.{T}: Add {W} or {U} to your mana pool.').
card_image_name('sejiri refuge'/'C13', 'sejiri refuge').
card_uid('sejiri refuge'/'C13', 'C13:Sejiri Refuge:sejiri refuge').
card_rarity('sejiri refuge'/'C13', 'Uncommon').
card_artist('sejiri refuge'/'C13', 'Ryan Pancoast').
card_number('sejiri refuge'/'C13', '320').
card_multiverse_id('sejiri refuge'/'C13', '376487').

card_in_set('sek\'kuar, deathkeeper', 'C13').
card_original_type('sek\'kuar, deathkeeper'/'C13', 'Legendary Creature — Orc Shaman').
card_original_text('sek\'kuar, deathkeeper'/'C13', 'Whenever another nontoken creature you control dies, put a 3/1 black and red Graveborn creature token with haste onto the battlefield.').
card_image_name('sek\'kuar, deathkeeper'/'C13', 'sek\'kuar, deathkeeper').
card_uid('sek\'kuar, deathkeeper'/'C13', 'C13:Sek\'Kuar, Deathkeeper:sek\'kuar, deathkeeper').
card_rarity('sek\'kuar, deathkeeper'/'C13', 'Rare').
card_artist('sek\'kuar, deathkeeper'/'C13', 'Jesper Ejsing').
card_number('sek\'kuar, deathkeeper'/'C13', '210').
card_flavor_text('sek\'kuar, deathkeeper'/'C13', 'The orcs believe Sek\'Kuar still lives in the Karplusan Mountains, gathering his dead army, intent on revenge.').
card_multiverse_id('sek\'kuar, deathkeeper'/'C13', '376488').

card_in_set('selesnya charm', 'C13').
card_original_type('selesnya charm'/'C13', 'Instant').
card_original_text('selesnya charm'/'C13', 'Choose one — Target creature gets +2/+2 and gains trample until end of turn; or exile target creature with power 5 or greater; or put a 2/2 white Knight creature token with vigilance onto the battlefield.').
card_image_name('selesnya charm'/'C13', 'selesnya charm').
card_uid('selesnya charm'/'C13', 'C13:Selesnya Charm:selesnya charm').
card_rarity('selesnya charm'/'C13', 'Uncommon').
card_artist('selesnya charm'/'C13', 'Zoltan Boros').
card_number('selesnya charm'/'C13', '211').
card_multiverse_id('selesnya charm'/'C13', '376489').

card_in_set('selesnya guildgate', 'C13').
card_original_type('selesnya guildgate'/'C13', 'Land — Gate').
card_original_text('selesnya guildgate'/'C13', 'Selesnya Guildgate enters the battlefield tapped.{T}: Add {G} or {W} to your mana pool.').
card_image_name('selesnya guildgate'/'C13', 'selesnya guildgate').
card_uid('selesnya guildgate'/'C13', 'C13:Selesnya Guildgate:selesnya guildgate').
card_rarity('selesnya guildgate'/'C13', 'Common').
card_artist('selesnya guildgate'/'C13', 'Howard Lyon').
card_number('selesnya guildgate'/'C13', '321').
card_flavor_text('selesnya guildgate'/'C13', 'Enter and rejoice! The Conclave stands united, open to one and all.').
card_multiverse_id('selesnya guildgate'/'C13', '376490').

card_in_set('selesnya guildmage', 'C13').
card_original_type('selesnya guildmage'/'C13', 'Creature — Elf Wizard').
card_original_text('selesnya guildmage'/'C13', '{3}{G}: Put a 1/1 green Saproling creature token onto the battlefield.{3}{W}: Creatures you control get +1/+1 until end of turn.').
card_image_name('selesnya guildmage'/'C13', 'selesnya guildmage').
card_uid('selesnya guildmage'/'C13', 'C13:Selesnya Guildmage:selesnya guildmage').
card_rarity('selesnya guildmage'/'C13', 'Uncommon').
card_artist('selesnya guildmage'/'C13', 'Mark Zug').
card_number('selesnya guildmage'/'C13', '232').
card_multiverse_id('selesnya guildmage'/'C13', '376491').

card_in_set('selesnya sanctuary', 'C13').
card_original_type('selesnya sanctuary'/'C13', 'Land').
card_original_text('selesnya sanctuary'/'C13', 'Selesnya Sanctuary enters the battlefield tapped.When Selesnya Sanctuary enters the battlefield, return a land you control to its owner\'s hand.{T}: Add {G}{W} to your mana pool.').
card_image_name('selesnya sanctuary'/'C13', 'selesnya sanctuary').
card_uid('selesnya sanctuary'/'C13', 'C13:Selesnya Sanctuary:selesnya sanctuary').
card_rarity('selesnya sanctuary'/'C13', 'Common').
card_artist('selesnya sanctuary'/'C13', 'John Avon').
card_number('selesnya sanctuary'/'C13', '322').
card_multiverse_id('selesnya sanctuary'/'C13', '376492').

card_in_set('selesnya signet', 'C13').
card_original_type('selesnya signet'/'C13', 'Artifact').
card_original_text('selesnya signet'/'C13', '{1}, {T}: Add {G}{W} to your mana pool.').
card_image_name('selesnya signet'/'C13', 'selesnya signet').
card_uid('selesnya signet'/'C13', 'C13:Selesnya Signet:selesnya signet').
card_rarity('selesnya signet'/'C13', 'Common').
card_artist('selesnya signet'/'C13', 'Raoul Vitale').
card_number('selesnya signet'/'C13', '257').
card_flavor_text('selesnya signet'/'C13', 'The symbol of the Conclave is one of unity, with tree supporting sun and sun feeding tree.').
card_multiverse_id('selesnya signet'/'C13', '376493').

card_in_set('serene master', 'C13').
card_original_type('serene master'/'C13', 'Creature — Human Monk').
card_original_text('serene master'/'C13', 'Whenever Serene Master blocks, exchange its power and the power of target creature it\'s blocking until end of combat.').
card_first_print('serene master', 'C13').
card_image_name('serene master'/'C13', 'serene master').
card_uid('serene master'/'C13', 'C13:Serene Master:serene master').
card_rarity('serene master'/'C13', 'Rare').
card_artist('serene master'/'C13', 'Mark Zug').
card_number('serene master'/'C13', '20').
card_flavor_text('serene master'/'C13', '\"A great warrior studies many different disciplines and takes the best from each.\"\n—Gideon Jura').
card_multiverse_id('serene master'/'C13', '376494').

card_in_set('serra avatar', 'C13').
card_original_type('serra avatar'/'C13', 'Creature — Avatar').
card_original_text('serra avatar'/'C13', 'Serra Avatar\'s power and toughness are each equal to your life total.When Serra Avatar is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_image_name('serra avatar'/'C13', 'serra avatar').
card_uid('serra avatar'/'C13', 'C13:Serra Avatar:serra avatar').
card_rarity('serra avatar'/'C13', 'Mythic Rare').
card_artist('serra avatar'/'C13', 'Dermot Power').
card_number('serra avatar'/'C13', '21').
card_flavor_text('serra avatar'/'C13', '\"Serra isn\'t dead. She lives on through me.\"').
card_multiverse_id('serra avatar'/'C13', '376495').

card_in_set('sharding sphinx', 'C13').
card_original_type('sharding sphinx'/'C13', 'Artifact Creature — Sphinx').
card_original_text('sharding sphinx'/'C13', 'FlyingWhenever an artifact creature you control deals combat damage to a player, you may put a 1/1 blue Thopter artifact creature token with flying onto the battlefield.').
card_image_name('sharding sphinx'/'C13', 'sharding sphinx').
card_uid('sharding sphinx'/'C13', 'C13:Sharding Sphinx:sharding sphinx').
card_rarity('sharding sphinx'/'C13', 'Rare').
card_artist('sharding sphinx'/'C13', 'Michael Bruinsma').
card_number('sharding sphinx'/'C13', '56').
card_flavor_text('sharding sphinx'/'C13', 'Whether mechanical or biological, life finds a way to propagate itself.').
card_multiverse_id('sharding sphinx'/'C13', '376496').

card_in_set('sharuum the hegemon', 'C13').
card_original_type('sharuum the hegemon'/'C13', 'Legendary Artifact Creature — Sphinx').
card_original_text('sharuum the hegemon'/'C13', 'FlyingWhen Sharuum the Hegemon enters the battlefield, you may return target artifact card from your graveyard to the battlefield.').
card_image_name('sharuum the hegemon'/'C13', 'sharuum the hegemon').
card_uid('sharuum the hegemon'/'C13', 'C13:Sharuum the Hegemon:sharuum the hegemon').
card_rarity('sharuum the hegemon'/'C13', 'Mythic Rare').
card_artist('sharuum the hegemon'/'C13', 'Izzy').
card_number('sharuum the hegemon'/'C13', '212').
card_flavor_text('sharuum the hegemon'/'C13', 'To gain audience with the hegemon, one must bring a riddle she has not heard.').
card_multiverse_id('sharuum the hegemon'/'C13', '376497').

card_in_set('shattergang brothers', 'C13').
card_original_type('shattergang brothers'/'C13', 'Legendary Creature — Goblin Artificer').
card_original_text('shattergang brothers'/'C13', '{2}{B}, Sacrifice a creature: Each other player sacrifices a creature.{2}{R}, Sacrifice an artifact: Each other player sacrifices an artifact.{2}{G}, Sacrifice an enchantment: Each other player sacrifices an enchantment.').
card_first_print('shattergang brothers', 'C13').
card_image_name('shattergang brothers'/'C13', 'shattergang brothers').
card_uid('shattergang brothers'/'C13', 'C13:Shattergang Brothers:shattergang brothers').
card_rarity('shattergang brothers'/'C13', 'Mythic Rare').
card_artist('shattergang brothers'/'C13', 'Kev Walker').
card_number('shattergang brothers'/'C13', '213').
card_multiverse_id('shattergang brothers'/'C13', '376498').

card_in_set('silklash spider', 'C13').
card_original_type('silklash spider'/'C13', 'Creature — Spider').
card_original_text('silklash spider'/'C13', 'Reach{X}{G}{G}: Silklash Spider deals X damage to each creature with flying.').
card_image_name('silklash spider'/'C13', 'silklash spider').
card_uid('silklash spider'/'C13', 'C13:Silklash Spider:silklash spider').
card_rarity('silklash spider'/'C13', 'Rare').
card_artist('silklash spider'/'C13', 'Iain McCaig').
card_number('silklash spider'/'C13', '169').
card_flavor_text('silklash spider'/'C13', 'By the time the spider comes to slurp up its dinner, its victims have been partially dissolved by acidic silk.').
card_multiverse_id('silklash spider'/'C13', '376499').

card_in_set('simic guildgate', 'C13').
card_original_type('simic guildgate'/'C13', 'Land — Gate').
card_original_text('simic guildgate'/'C13', 'Simic Guildgate enters the battlefield tapped.{T}: Add {G} or {U} to your mana pool.').
card_image_name('simic guildgate'/'C13', 'simic guildgate').
card_uid('simic guildgate'/'C13', 'C13:Simic Guildgate:simic guildgate').
card_rarity('simic guildgate'/'C13', 'Common').
card_artist('simic guildgate'/'C13', 'Svetlin Velinov').
card_number('simic guildgate'/'C13', '323').
card_flavor_text('simic guildgate'/'C13', 'Enter and comprehend the perfection of orchestrated life.').
card_multiverse_id('simic guildgate'/'C13', '376500').

card_in_set('simic signet', 'C13').
card_original_type('simic signet'/'C13', 'Artifact').
card_original_text('simic signet'/'C13', '{1}, {T}: Add {G}{U} to your mana pool.').
card_image_name('simic signet'/'C13', 'simic signet').
card_uid('simic signet'/'C13', 'C13:Simic Signet:simic signet').
card_rarity('simic signet'/'C13', 'Common').
card_artist('simic signet'/'C13', 'Mike Sass').
card_number('simic signet'/'C13', '258').
card_flavor_text('simic signet'/'C13', 'For the Simic Combine, its sigil serves not as an emblem of honor but as a trademark. Its familiar image on any biological commodity attests to superb craftsmanship, ingenious innovation, and higher cost.').
card_multiverse_id('simic signet'/'C13', '376501').

card_in_set('skyscribing', 'C13').
card_original_type('skyscribing'/'C13', 'Sorcery').
card_original_text('skyscribing'/'C13', 'Each player draws X cards.Forecast — {2}{U}, Reveal Skyscribing from your hand: Each player draws a card. (Activate this ability only during your upkeep and only once each turn.)').
card_image_name('skyscribing'/'C13', 'skyscribing').
card_uid('skyscribing'/'C13', 'C13:Skyscribing:skyscribing').
card_rarity('skyscribing'/'C13', 'Uncommon').
card_artist('skyscribing'/'C13', 'Richard Wright').
card_number('skyscribing'/'C13', '57').
card_flavor_text('skyscribing'/'C13', '\"All clouds should be so organized.\"\n—Isperia').
card_multiverse_id('skyscribing'/'C13', '376502').

card_in_set('skyward eye prophets', 'C13').
card_original_type('skyward eye prophets'/'C13', 'Creature — Human Wizard').
card_original_text('skyward eye prophets'/'C13', 'Vigilance{T}: Reveal the top card of your library. If it\'s a land card, put it onto the battlefield. Otherwise, put it into your hand.').
card_image_name('skyward eye prophets'/'C13', 'skyward eye prophets').
card_uid('skyward eye prophets'/'C13', 'C13:Skyward Eye Prophets:skyward eye prophets').
card_rarity('skyward eye prophets'/'C13', 'Uncommon').
card_artist('skyward eye prophets'/'C13', 'Matt Stewart').
card_number('skyward eye prophets'/'C13', '214').
card_flavor_text('skyward eye prophets'/'C13', 'They lament the doom that is coming to Bant without realizing the part their own leaders have played in it.').
card_multiverse_id('skyward eye prophets'/'C13', '376503').

card_in_set('slice and dice', 'C13').
card_original_type('slice and dice'/'C13', 'Sorcery').
card_original_text('slice and dice'/'C13', 'Slice and Dice deals 4 damage to each creature.Cycling {2}{R} ({2}{R}, Discard this card: Draw a card.)When you cycle Slice and Dice, you may have it deal 1 damage to each creature.').
card_image_name('slice and dice'/'C13', 'slice and dice').
card_uid('slice and dice'/'C13', 'C13:Slice and Dice:slice and dice').
card_rarity('slice and dice'/'C13', 'Uncommon').
card_artist('slice and dice'/'C13', 'Mark Brill').
card_number('slice and dice'/'C13', '119').
card_multiverse_id('slice and dice'/'C13', '376504').

card_in_set('slice in twain', 'C13').
card_original_type('slice in twain'/'C13', 'Instant').
card_original_text('slice in twain'/'C13', 'Destroy target artifact or enchantment.Draw a card.').
card_image_name('slice in twain'/'C13', 'slice in twain').
card_uid('slice in twain'/'C13', 'C13:Slice in Twain:slice in twain').
card_rarity('slice in twain'/'C13', 'Uncommon').
card_artist('slice in twain'/'C13', 'Efrem Palacios').
card_number('slice in twain'/'C13', '170').
card_flavor_text('slice in twain'/'C13', '\"The hypocrisy of these elves is thicker than steel—destroying ‘unnatural metal\' with their own enchanted swords.\"\n—Kara Vrist, Neurok agent').
card_multiverse_id('slice in twain'/'C13', '376505').

card_in_set('slippery karst', 'C13').
card_original_type('slippery karst'/'C13', 'Land').
card_original_text('slippery karst'/'C13', 'Slippery Karst enters the battlefield tapped.{T}: Add {G} to your mana pool.Cycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('slippery karst'/'C13', 'slippery karst').
card_uid('slippery karst'/'C13', 'C13:Slippery Karst:slippery karst').
card_rarity('slippery karst'/'C13', 'Common').
card_artist('slippery karst'/'C13', 'Stephen Daniele').
card_number('slippery karst'/'C13', '324').
card_multiverse_id('slippery karst'/'C13', '376506').

card_in_set('smoldering crater', 'C13').
card_original_type('smoldering crater'/'C13', 'Land').
card_original_text('smoldering crater'/'C13', 'Smoldering Crater enters the battlefield tapped.{T}: Add {R} to your mana pool.Cycling {2} ({2}, Discard this card: Draw a card.)').
card_image_name('smoldering crater'/'C13', 'smoldering crater').
card_uid('smoldering crater'/'C13', 'C13:Smoldering Crater:smoldering crater').
card_rarity('smoldering crater'/'C13', 'Common').
card_artist('smoldering crater'/'C13', 'Mark Tedin').
card_number('smoldering crater'/'C13', '325').
card_multiverse_id('smoldering crater'/'C13', '376507').

card_in_set('sol ring', 'C13').
card_original_type('sol ring'/'C13', 'Artifact').
card_original_text('sol ring'/'C13', '{T}: Add {2} to your mana pool.').
card_image_name('sol ring'/'C13', 'sol ring').
card_uid('sol ring'/'C13', 'C13:Sol Ring:sol ring').
card_rarity('sol ring'/'C13', 'Uncommon').
card_artist('sol ring'/'C13', 'Mike Bierek').
card_number('sol ring'/'C13', '259').
card_flavor_text('sol ring'/'C13', 'Lost to time is the artificer\'s art of trapping light from a distant star in a ring of purest gold.').
card_multiverse_id('sol ring'/'C13', '376508').

card_in_set('soul manipulation', 'C13').
card_original_type('soul manipulation'/'C13', 'Instant').
card_original_text('soul manipulation'/'C13', 'Choose one or both — Counter target creature spell; and/or return target creature card from your graveyard to your hand.').
card_image_name('soul manipulation'/'C13', 'soul manipulation').
card_uid('soul manipulation'/'C13', 'C13:Soul Manipulation:soul manipulation').
card_rarity('soul manipulation'/'C13', 'Common').
card_artist('soul manipulation'/'C13', 'Carl Critchlow').
card_number('soul manipulation'/'C13', '215').
card_flavor_text('soul manipulation'/'C13', '\"Birth and death are both reversible.\"\n—Nicol Bolas').
card_multiverse_id('soul manipulation'/'C13', '376509').

card_in_set('spawning grounds', 'C13').
card_original_type('spawning grounds'/'C13', 'Enchantment — Aura').
card_original_text('spawning grounds'/'C13', 'Enchant landEnchanted land has \"{T}: Put a 5/5 green Beast creature token with trample onto the battlefield.\"').
card_first_print('spawning grounds', 'C13').
card_image_name('spawning grounds'/'C13', 'spawning grounds').
card_uid('spawning grounds'/'C13', 'C13:Spawning Grounds:spawning grounds').
card_rarity('spawning grounds'/'C13', 'Rare').
card_artist('spawning grounds'/'C13', 'Vincent Proce').
card_number('spawning grounds'/'C13', '171').
card_flavor_text('spawning grounds'/'C13', 'It\'s the only water source in miles of desert, but the local tribes avoid it at all costs.').
card_multiverse_id('spawning grounds'/'C13', '376510').

card_in_set('spellbreaker behemoth', 'C13').
card_original_type('spellbreaker behemoth'/'C13', 'Creature — Beast').
card_original_text('spellbreaker behemoth'/'C13', 'Spellbreaker Behemoth can\'t be countered.Creature spells you control with power 5 or greater can\'t be countered.').
card_image_name('spellbreaker behemoth'/'C13', 'spellbreaker behemoth').
card_uid('spellbreaker behemoth'/'C13', 'C13:Spellbreaker Behemoth:spellbreaker behemoth').
card_rarity('spellbreaker behemoth'/'C13', 'Rare').
card_artist('spellbreaker behemoth'/'C13', 'Jason Chan').
card_number('spellbreaker behemoth'/'C13', '216').
card_flavor_text('spellbreaker behemoth'/'C13', '\"Some think its immunity to countermagic evolved because of the constant mage attacks on Naya. I think it just ate a bunch of wizards.\"\n—Broka, drumhunter').
card_multiverse_id('spellbreaker behemoth'/'C13', '376511').

card_in_set('sphinx of the steel wind', 'C13').
card_original_type('sphinx of the steel wind'/'C13', 'Artifact Creature — Sphinx').
card_original_text('sphinx of the steel wind'/'C13', 'Flying, first strike, vigilance, lifelink, protection from red and from green').
card_image_name('sphinx of the steel wind'/'C13', 'sphinx of the steel wind').
card_uid('sphinx of the steel wind'/'C13', 'C13:Sphinx of the Steel Wind:sphinx of the steel wind').
card_rarity('sphinx of the steel wind'/'C13', 'Mythic Rare').
card_artist('sphinx of the steel wind'/'C13', 'Kev Walker').
card_number('sphinx of the steel wind'/'C13', '217').
card_flavor_text('sphinx of the steel wind'/'C13', 'No one has properly answered her favorite riddle: \"Why should I spare your life?\"').
card_multiverse_id('sphinx of the steel wind'/'C13', '376512').

card_in_set('spinal embrace', 'C13').
card_original_type('spinal embrace'/'C13', 'Instant').
card_original_text('spinal embrace'/'C13', 'Cast Spinal Embrace only during combat.Untap target creature you don\'t control and gain control of it. It gains haste until end of turn. At the beginning of the next end step, sacrifice it. If you do, you gain life equal to its toughness.').
card_image_name('spinal embrace'/'C13', 'spinal embrace').
card_uid('spinal embrace'/'C13', 'C13:Spinal Embrace:spinal embrace').
card_rarity('spinal embrace'/'C13', 'Rare').
card_artist('spinal embrace'/'C13', 'Donato Giancola').
card_number('spinal embrace'/'C13', '218').
card_multiverse_id('spinal embrace'/'C13', '376513').

card_in_set('spine of ish sah', 'C13').
card_original_type('spine of ish sah'/'C13', 'Artifact').
card_original_text('spine of ish sah'/'C13', 'When Spine of Ish Sah enters the battlefield, destroy target permanent.When Spine of Ish Sah is put into a graveyard from the battlefield, return Spine of Ish Sah to its owner\'s hand.').
card_image_name('spine of ish sah'/'C13', 'spine of ish sah').
card_uid('spine of ish sah'/'C13', 'C13:Spine of Ish Sah:spine of ish sah').
card_rarity('spine of ish sah'/'C13', 'Rare').
card_artist('spine of ish sah'/'C13', 'Daniel Ljunggren').
card_number('spine of ish sah'/'C13', '260').
card_multiverse_id('spine of ish sah'/'C13', '376514').

card_in_set('spitebellows', 'C13').
card_original_type('spitebellows'/'C13', 'Creature — Elemental').
card_original_text('spitebellows'/'C13', 'When Spitebellows leaves the battlefield, it deals 6 damage to target creature.Evoke {1}{R}{R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('spitebellows'/'C13', 'spitebellows').
card_uid('spitebellows'/'C13', 'C13:Spitebellows:spitebellows').
card_rarity('spitebellows'/'C13', 'Uncommon').
card_artist('spitebellows'/'C13', 'Larry MacDougall').
card_number('spitebellows'/'C13', '120').
card_flavor_text('spitebellows'/'C13', 'Disaster stalks with gaping jaws across unready lands.').
card_multiverse_id('spitebellows'/'C13', '376515').

card_in_set('spiteful visions', 'C13').
card_original_type('spiteful visions'/'C13', 'Enchantment').
card_original_text('spiteful visions'/'C13', 'At the beginning of each player\'s draw step, that player draws an additional card.Whenever a player draws a card, Spiteful Visions deals 1 damage to that player.').
card_image_name('spiteful visions'/'C13', 'spiteful visions').
card_uid('spiteful visions'/'C13', 'C13:Spiteful Visions:spiteful visions').
card_rarity('spiteful visions'/'C13', 'Rare').
card_artist('spiteful visions'/'C13', 'Brandon Kitkouski').
card_number('spiteful visions'/'C13', '233').
card_flavor_text('spiteful visions'/'C13', 'Knowledge brings the sting of disillusionment, but the pain teaches perspective.').
card_multiverse_id('spiteful visions'/'C13', '376516').

card_in_set('spoils of victory', 'C13').
card_original_type('spoils of victory'/'C13', 'Sorcery').
card_original_text('spoils of victory'/'C13', 'Search your library for a Plains, Island, Swamp, Mountain, or Forest card and put that card onto the battlefield. Then shuffle your library.').
card_image_name('spoils of victory'/'C13', 'spoils of victory').
card_uid('spoils of victory'/'C13', 'C13:Spoils of Victory:spoils of victory').
card_rarity('spoils of victory'/'C13', 'Uncommon').
card_artist('spoils of victory'/'C13', 'Sun Nan').
card_number('spoils of victory'/'C13', '172').
card_multiverse_id('spoils of victory'/'C13', '376517').

card_in_set('springjack pasture', 'C13').
card_original_type('springjack pasture'/'C13', 'Land').
card_original_text('springjack pasture'/'C13', '{T}: Add {1} to your mana pool.{4}, {T}: Put a 0/1 white Goat creature token onto the battlefield.{T}, Sacrifice X Goats: Add X mana of any one color to your mana pool. You gain X life.').
card_image_name('springjack pasture'/'C13', 'springjack pasture').
card_uid('springjack pasture'/'C13', 'C13:Springjack Pasture:springjack pasture').
card_rarity('springjack pasture'/'C13', 'Rare').
card_artist('springjack pasture'/'C13', 'Terese Nielsen').
card_number('springjack pasture'/'C13', '326').
card_multiverse_id('springjack pasture'/'C13', '376518').

card_in_set('sprouting thrinax', 'C13').
card_original_type('sprouting thrinax'/'C13', 'Creature — Lizard').
card_original_text('sprouting thrinax'/'C13', 'When Sprouting Thrinax dies, put three 1/1 green Saproling creature tokens onto the battlefield.').
card_image_name('sprouting thrinax'/'C13', 'sprouting thrinax').
card_uid('sprouting thrinax'/'C13', 'C13:Sprouting Thrinax:sprouting thrinax').
card_rarity('sprouting thrinax'/'C13', 'Uncommon').
card_artist('sprouting thrinax'/'C13', 'Jarreau Wimberly').
card_number('sprouting thrinax'/'C13', '219').
card_flavor_text('sprouting thrinax'/'C13', 'The vast network of predation on Jund has actually caused some strange creatures to adapt to being eaten.').
card_multiverse_id('sprouting thrinax'/'C13', '376519').

card_in_set('sprouting vines', 'C13').
card_original_type('sprouting vines'/'C13', 'Instant').
card_original_text('sprouting vines'/'C13', 'Search your library for a basic land card, reveal that card, and put it into your hand. Then shuffle your library.Storm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_image_name('sprouting vines'/'C13', 'sprouting vines').
card_uid('sprouting vines'/'C13', 'C13:Sprouting Vines:sprouting vines').
card_rarity('sprouting vines'/'C13', 'Common').
card_artist('sprouting vines'/'C13', 'John Avon').
card_number('sprouting vines'/'C13', '173').
card_multiverse_id('sprouting vines'/'C13', '376520').

card_in_set('stalking vengeance', 'C13').
card_original_type('stalking vengeance'/'C13', 'Creature — Avatar').
card_original_text('stalking vengeance'/'C13', 'HasteWhenever another creature you control dies, it deals damage equal to its power to target player.').
card_image_name('stalking vengeance'/'C13', 'stalking vengeance').
card_uid('stalking vengeance'/'C13', 'C13:Stalking Vengeance:stalking vengeance').
card_rarity('stalking vengeance'/'C13', 'Rare').
card_artist('stalking vengeance'/'C13', 'Anthony S. Waters').
card_number('stalking vengeance'/'C13', '121').
card_flavor_text('stalking vengeance'/'C13', 'Something roams the killing places, sniffing the guilt of the slayers, stalking them on iron paws.').
card_multiverse_id('stalking vengeance'/'C13', '376521').

card_in_set('starstorm', 'C13').
card_original_type('starstorm'/'C13', 'Instant').
card_original_text('starstorm'/'C13', 'Starstorm deals X damage to each creature.Cycling {3} ({3}, Discard this card: Draw a card.)').
card_image_name('starstorm'/'C13', 'starstorm').
card_uid('starstorm'/'C13', 'C13:Starstorm:starstorm').
card_rarity('starstorm'/'C13', 'Rare').
card_artist('starstorm'/'C13', 'Jonas De Ro').
card_number('starstorm'/'C13', '122').
card_flavor_text('starstorm'/'C13', 'Ever since the disaster, the city\'s survivors viewed the star-strewn sky with dread.').
card_multiverse_id('starstorm'/'C13', '376522').

card_in_set('stonecloaker', 'C13').
card_original_type('stonecloaker'/'C13', 'Creature — Gargoyle').
card_original_text('stonecloaker'/'C13', 'Flash (You may cast this spell any time you could cast an instant.)FlyingWhen Stonecloaker enters the battlefield, return a creature you control to its owner\'s hand.When Stonecloaker enters the battlefield, exile target card from a graveyard.').
card_image_name('stonecloaker'/'C13', 'stonecloaker').
card_uid('stonecloaker'/'C13', 'C13:Stonecloaker:stonecloaker').
card_rarity('stonecloaker'/'C13', 'Uncommon').
card_artist('stonecloaker'/'C13', 'Tomas Giorello').
card_number('stonecloaker'/'C13', '22').
card_multiverse_id('stonecloaker'/'C13', '376523').

card_in_set('stormscape battlemage', 'C13').
card_original_type('stormscape battlemage'/'C13', 'Creature — Metathran Wizard').
card_original_text('stormscape battlemage'/'C13', 'Kicker {W} and/or {2}{B}When Stormscape Battlemage enters the battlefield, if it was kicked with its {W} kicker, you gain 3 life.When Stormscape Battlemage enters the battlefield, if it was kicked with its {2}{B} kicker, destroy target nonblack creature. That creature can\'t be regenerated.').
card_image_name('stormscape battlemage'/'C13', 'stormscape battlemage').
card_uid('stormscape battlemage'/'C13', 'C13:Stormscape Battlemage:stormscape battlemage').
card_rarity('stormscape battlemage'/'C13', 'Uncommon').
card_artist('stormscape battlemage'/'C13', 'Christopher Moeller').
card_number('stormscape battlemage'/'C13', '58').
card_multiverse_id('stormscape battlemage'/'C13', '376524').

card_in_set('strategic planning', 'C13').
card_original_type('strategic planning'/'C13', 'Sorcery').
card_original_text('strategic planning'/'C13', 'Look at the top three cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_image_name('strategic planning'/'C13', 'strategic planning').
card_uid('strategic planning'/'C13', 'C13:Strategic Planning:strategic planning').
card_rarity('strategic planning'/'C13', 'Uncommon').
card_artist('strategic planning'/'C13', 'Zhang Jiazhen').
card_number('strategic planning'/'C13', '59').
card_flavor_text('strategic planning'/'C13', '\"Plans evolved within the tent decide a victory 1,000 li away.\"').
card_multiverse_id('strategic planning'/'C13', '376525').

card_in_set('street spasm', 'C13').
card_original_type('street spasm'/'C13', 'Instant').
card_original_text('street spasm'/'C13', 'Street Spasm deals X damage to target creature without flying you don\'t control.Overload {X}{X}{R}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_image_name('street spasm'/'C13', 'street spasm').
card_uid('street spasm'/'C13', 'C13:Street Spasm:street spasm').
card_rarity('street spasm'/'C13', 'Uncommon').
card_artist('street spasm'/'C13', 'Raymond Swanland').
card_number('street spasm'/'C13', '123').
card_multiverse_id('street spasm'/'C13', '376526').

card_in_set('stronghold assassin', 'C13').
card_original_type('stronghold assassin'/'C13', 'Creature — Zombie Assassin').
card_original_text('stronghold assassin'/'C13', '{T}, Sacrifice a creature: Destroy target nonblack creature.').
card_image_name('stronghold assassin'/'C13', 'stronghold assassin').
card_uid('stronghold assassin'/'C13', 'C13:Stronghold Assassin:stronghold assassin').
card_rarity('stronghold assassin'/'C13', 'Rare').
card_artist('stronghold assassin'/'C13', 'Matthew D. Wilson').
card_number('stronghold assassin'/'C13', '93').
card_flavor_text('stronghold assassin'/'C13', 'The assassin sees only throats and hears only heartbeats.').
card_multiverse_id('stronghold assassin'/'C13', '376527').

card_in_set('sudden demise', 'C13').
card_original_type('sudden demise'/'C13', 'Sorcery').
card_original_text('sudden demise'/'C13', 'Choose a color. Sudden Demise deals X damage to each creature of the chosen color.').
card_first_print('sudden demise', 'C13').
card_image_name('sudden demise'/'C13', 'sudden demise').
card_uid('sudden demise'/'C13', 'C13:Sudden Demise:sudden demise').
card_rarity('sudden demise'/'C13', 'Rare').
card_artist('sudden demise'/'C13', 'Dan Scott').
card_number('sudden demise'/'C13', '124').
card_flavor_text('sudden demise'/'C13', 'In the Skyfang Mountains, one misstep can mean the end of an entire clan.').
card_multiverse_id('sudden demise'/'C13', '376528').

card_in_set('sudden spoiling', 'C13').
card_original_type('sudden spoiling'/'C13', 'Instant').
card_original_text('sudden spoiling'/'C13', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)Creatures target player controls become 0/2 and lose all abilities until end of turn.').
card_image_name('sudden spoiling'/'C13', 'sudden spoiling').
card_uid('sudden spoiling'/'C13', 'C13:Sudden Spoiling:sudden spoiling').
card_rarity('sudden spoiling'/'C13', 'Rare').
card_artist('sudden spoiling'/'C13', 'Alan Pollack').
card_number('sudden spoiling'/'C13', '94').
card_multiverse_id('sudden spoiling'/'C13', '376529').

card_in_set('sun droplet', 'C13').
card_original_type('sun droplet'/'C13', 'Artifact').
card_original_text('sun droplet'/'C13', 'Whenever you\'re dealt damage, put that many charge counters on Sun Droplet.At the beginning of each upkeep, you may remove a charge counter from Sun Droplet. If you do, you gain 1 life.').
card_image_name('sun droplet'/'C13', 'sun droplet').
card_uid('sun droplet'/'C13', 'C13:Sun Droplet:sun droplet').
card_rarity('sun droplet'/'C13', 'Uncommon').
card_artist('sun droplet'/'C13', 'Greg Hildebrandt').
card_number('sun droplet'/'C13', '261').
card_multiverse_id('sun droplet'/'C13', '376530').

card_in_set('surveyor\'s scope', 'C13').
card_original_type('surveyor\'s scope'/'C13', 'Artifact').
card_original_text('surveyor\'s scope'/'C13', '{T}, Exile Surveyor\'s Scope: Search your library for up to X basic land cards, where X is the number of players who control at least two more lands than you. Put those cards onto the battlefield, then shuffle your library.').
card_first_print('surveyor\'s scope', 'C13').
card_image_name('surveyor\'s scope'/'C13', 'surveyor\'s scope').
card_uid('surveyor\'s scope'/'C13', 'C13:Surveyor\'s Scope:surveyor\'s scope').
card_rarity('surveyor\'s scope'/'C13', 'Rare').
card_artist('surveyor\'s scope'/'C13', 'Daniel Ljunggren').
card_number('surveyor\'s scope'/'C13', '262').
card_multiverse_id('surveyor\'s scope'/'C13', '376531').

card_in_set('survival cache', 'C13').
card_original_type('survival cache'/'C13', 'Sorcery').
card_original_text('survival cache'/'C13', 'You gain 2 life. Then if you have more life than an opponent, draw a card.Rebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_image_name('survival cache'/'C13', 'survival cache').
card_uid('survival cache'/'C13', 'C13:Survival Cache:survival cache').
card_rarity('survival cache'/'C13', 'Uncommon').
card_artist('survival cache'/'C13', 'Scott Chou').
card_number('survival cache'/'C13', '23').
card_multiverse_id('survival cache'/'C13', '376532').

card_in_set('swamp', 'C13').
card_original_type('swamp'/'C13', 'Basic Land — Swamp').
card_original_text('swamp'/'C13', 'B').
card_image_name('swamp'/'C13', 'swamp1').
card_uid('swamp'/'C13', 'C13:Swamp:swamp1').
card_rarity('swamp'/'C13', 'Basic Land').
card_artist('swamp'/'C13', 'Mike Bierek').
card_number('swamp'/'C13', '345').
card_multiverse_id('swamp'/'C13', '376536').

card_in_set('swamp', 'C13').
card_original_type('swamp'/'C13', 'Basic Land — Swamp').
card_original_text('swamp'/'C13', 'B').
card_image_name('swamp'/'C13', 'swamp2').
card_uid('swamp'/'C13', 'C13:Swamp:swamp2').
card_rarity('swamp'/'C13', 'Basic Land').
card_artist('swamp'/'C13', 'Jonas De Ro').
card_number('swamp'/'C13', '346').
card_multiverse_id('swamp'/'C13', '376535').

card_in_set('swamp', 'C13').
card_original_type('swamp'/'C13', 'Basic Land — Swamp').
card_original_text('swamp'/'C13', 'B').
card_image_name('swamp'/'C13', 'swamp3').
card_uid('swamp'/'C13', 'C13:Swamp:swamp3').
card_rarity('swamp'/'C13', 'Basic Land').
card_artist('swamp'/'C13', 'Jung Park').
card_number('swamp'/'C13', '347').
card_multiverse_id('swamp'/'C13', '376534').

card_in_set('swamp', 'C13').
card_original_type('swamp'/'C13', 'Basic Land — Swamp').
card_original_text('swamp'/'C13', 'B').
card_image_name('swamp'/'C13', 'swamp4').
card_uid('swamp'/'C13', 'C13:Swamp:swamp4').
card_rarity('swamp'/'C13', 'Basic Land').
card_artist('swamp'/'C13', 'Andreas Rocha').
card_number('swamp'/'C13', '348').
card_multiverse_id('swamp'/'C13', '376533').

card_in_set('swiftfoot boots', 'C13').
card_original_type('swiftfoot boots'/'C13', 'Artifact — Equipment').
card_original_text('swiftfoot boots'/'C13', 'Equipped creature has hexproof and haste.Equip {1}').
card_image_name('swiftfoot boots'/'C13', 'swiftfoot boots').
card_uid('swiftfoot boots'/'C13', 'C13:Swiftfoot Boots:swiftfoot boots').
card_rarity('swiftfoot boots'/'C13', 'Uncommon').
card_artist('swiftfoot boots'/'C13', 'Svetlin Velinov').
card_number('swiftfoot boots'/'C13', '263').
card_multiverse_id('swiftfoot boots'/'C13', '376537').

card_in_set('sword of the paruns', 'C13').
card_original_type('sword of the paruns'/'C13', 'Artifact — Equipment').
card_original_text('sword of the paruns'/'C13', 'As long as equipped creature is tapped, tapped creatures you control get +2/+0.As long as equipped creature is untapped, untapped creatures you control get +0/+2.{3}: You may tap or untap equipped creature.Equip {3}').
card_image_name('sword of the paruns'/'C13', 'sword of the paruns').
card_uid('sword of the paruns'/'C13', 'C13:Sword of the Paruns:sword of the paruns').
card_rarity('sword of the paruns'/'C13', 'Rare').
card_artist('sword of the paruns'/'C13', 'Greg Hildebrandt').
card_number('sword of the paruns'/'C13', '264').
card_multiverse_id('sword of the paruns'/'C13', '376538').

card_in_set('sydri, galvanic genius', 'C13').
card_original_type('sydri, galvanic genius'/'C13', 'Legendary Creature — Human Artificer').
card_original_text('sydri, galvanic genius'/'C13', '{U}: Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn.{W}{B}: Target artifact creature gains deathtouch and lifelink until end of turn.').
card_first_print('sydri, galvanic genius', 'C13').
card_image_name('sydri, galvanic genius'/'C13', 'sydri, galvanic genius').
card_uid('sydri, galvanic genius'/'C13', 'C13:Sydri, Galvanic Genius:sydri, galvanic genius').
card_rarity('sydri, galvanic genius'/'C13', 'Mythic Rare').
card_artist('sydri, galvanic genius'/'C13', 'Terese Nielsen').
card_number('sydri, galvanic genius'/'C13', '220').
card_multiverse_id('sydri, galvanic genius'/'C13', '376539').

card_in_set('temple bell', 'C13').
card_original_type('temple bell'/'C13', 'Artifact').
card_original_text('temple bell'/'C13', '{T}: Each player draws a card.').
card_image_name('temple bell'/'C13', 'temple bell').
card_uid('temple bell'/'C13', 'C13:Temple Bell:temple bell').
card_rarity('temple bell'/'C13', 'Rare').
card_artist('temple bell'/'C13', 'Mark Tedin').
card_number('temple bell'/'C13', '265').
card_flavor_text('temple bell'/'C13', 'Enlightenment is not gained by striking the bell or hearing its toll. It\'s found in the silence that follows.').
card_multiverse_id('temple bell'/'C13', '376540').

card_in_set('temple of the false god', 'C13').
card_original_type('temple of the false god'/'C13', 'Land').
card_original_text('temple of the false god'/'C13', '{T}: Add {2} to your mana pool. Activate this ability only if you control five or more lands.').
card_image_name('temple of the false god'/'C13', 'temple of the false god').
card_uid('temple of the false god'/'C13', 'C13:Temple of the False God:temple of the false god').
card_rarity('temple of the false god'/'C13', 'Uncommon').
card_artist('temple of the false god'/'C13', 'Brian Snõddy').
card_number('temple of the false god'/'C13', '327').
card_flavor_text('temple of the false god'/'C13', 'Those who bring nothing to the temple take nothing away.').
card_multiverse_id('temple of the false god'/'C13', '376541').

card_in_set('tempt with discovery', 'C13').
card_original_type('tempt with discovery'/'C13', 'Sorcery').
card_original_text('tempt with discovery'/'C13', 'Tempting offer — Search your library for a land card and put it onto the battlefield. Each opponent may search his or her library for a land card and put it onto the battlefield. For each opponent who searches a library this way, search your library for a land card and put it onto the battlefield. Then each player who searched a library this way shuffles it.').
card_first_print('tempt with discovery', 'C13').
card_image_name('tempt with discovery'/'C13', 'tempt with discovery').
card_uid('tempt with discovery'/'C13', 'C13:Tempt with Discovery:tempt with discovery').
card_rarity('tempt with discovery'/'C13', 'Rare').
card_artist('tempt with discovery'/'C13', 'William Wu').
card_number('tempt with discovery'/'C13', '174').
card_multiverse_id('tempt with discovery'/'C13', '376542').

card_in_set('tempt with glory', 'C13').
card_original_type('tempt with glory'/'C13', 'Sorcery').
card_original_text('tempt with glory'/'C13', 'Tempting offer — Put a +1/+1 counter on each creature you control. Each opponent may put a +1/+1 counter on each creature he or she controls. For each opponent who does, put a +1/+1 counter on each creature you control.').
card_first_print('tempt with glory', 'C13').
card_image_name('tempt with glory'/'C13', 'tempt with glory').
card_uid('tempt with glory'/'C13', 'C13:Tempt with Glory:tempt with glory').
card_rarity('tempt with glory'/'C13', 'Rare').
card_artist('tempt with glory'/'C13', 'Michael Komarck').
card_number('tempt with glory'/'C13', '24').
card_multiverse_id('tempt with glory'/'C13', '376543').

card_in_set('tempt with immortality', 'C13').
card_original_type('tempt with immortality'/'C13', 'Sorcery').
card_original_text('tempt with immortality'/'C13', 'Tempting offer — Return a creature card from your graveyard to the battlefield. Each opponent may return a creature card from his or her graveyard to the battlefield. For each player who does, return a creature card from your graveyard to the battlefield.').
card_first_print('tempt with immortality', 'C13').
card_image_name('tempt with immortality'/'C13', 'tempt with immortality').
card_uid('tempt with immortality'/'C13', 'C13:Tempt with Immortality:tempt with immortality').
card_rarity('tempt with immortality'/'C13', 'Rare').
card_artist('tempt with immortality'/'C13', 'Philip Straub').
card_number('tempt with immortality'/'C13', '95').
card_multiverse_id('tempt with immortality'/'C13', '376544').

card_in_set('tempt with reflections', 'C13').
card_original_type('tempt with reflections'/'C13', 'Sorcery').
card_original_text('tempt with reflections'/'C13', 'Tempting offer — Choose target creature you control. Put a token onto the battlefield that\'s a copy of that creature. Each opponent may put a token onto the battlefield that\'s a copy of that creature. For each opponent who does, put a token onto the battlefield that\'s a copy of that creature.').
card_first_print('tempt with reflections', 'C13').
card_image_name('tempt with reflections'/'C13', 'tempt with reflections').
card_uid('tempt with reflections'/'C13', 'C13:Tempt with Reflections:tempt with reflections').
card_rarity('tempt with reflections'/'C13', 'Rare').
card_artist('tempt with reflections'/'C13', 'Mike Bierek').
card_number('tempt with reflections'/'C13', '60').
card_multiverse_id('tempt with reflections'/'C13', '376545').

card_in_set('tempt with vengeance', 'C13').
card_original_type('tempt with vengeance'/'C13', 'Sorcery').
card_original_text('tempt with vengeance'/'C13', 'Tempting offer — Put X 1/1 red Elemental creature tokens with haste onto the battlefield. Each opponent may put X 1/1 red Elemental creature tokens with haste onto the battlefield. For each player who does, put X 1/1 red Elemental creature tokens with haste onto the battlefield.').
card_first_print('tempt with vengeance', 'C13').
card_image_name('tempt with vengeance'/'C13', 'tempt with vengeance').
card_uid('tempt with vengeance'/'C13', 'C13:Tempt with Vengeance:tempt with vengeance').
card_rarity('tempt with vengeance'/'C13', 'Rare').
card_artist('tempt with vengeance'/'C13', 'Ryan Barger').
card_number('tempt with vengeance'/'C13', '125').
card_multiverse_id('tempt with vengeance'/'C13', '376546').

card_in_set('terra ravager', 'C13').
card_original_type('terra ravager'/'C13', 'Creature — Elemental Beast').
card_original_text('terra ravager'/'C13', 'Whenever Terra Ravager attacks, it gets +X/+0 until end of turn, where X is the number of lands defending player controls.').
card_first_print('terra ravager', 'C13').
card_image_name('terra ravager'/'C13', 'terra ravager').
card_uid('terra ravager'/'C13', 'C13:Terra Ravager:terra ravager').
card_rarity('terra ravager'/'C13', 'Uncommon').
card_artist('terra ravager'/'C13', 'Ralph Horsley').
card_number('terra ravager'/'C13', '126').
card_flavor_text('terra ravager'/'C13', 'It lives in the mountaintops, feeds at the mountains\' feet, and kills anything it finds on the path between.').
card_multiverse_id('terra ravager'/'C13', '376547').

card_in_set('terramorphic expanse', 'C13').
card_original_type('terramorphic expanse'/'C13', 'Land').
card_original_text('terramorphic expanse'/'C13', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_image_name('terramorphic expanse'/'C13', 'terramorphic expanse').
card_uid('terramorphic expanse'/'C13', 'C13:Terramorphic Expanse:terramorphic expanse').
card_rarity('terramorphic expanse'/'C13', 'Common').
card_artist('terramorphic expanse'/'C13', 'Dan Scott').
card_number('terramorphic expanse'/'C13', '328').
card_multiverse_id('terramorphic expanse'/'C13', '376548').

card_in_set('thopter foundry', 'C13').
card_original_type('thopter foundry'/'C13', 'Artifact').
card_original_text('thopter foundry'/'C13', '{1}, Sacrifice a nontoken artifact: Put a 1/1 blue Thopter artifact creature token with flying onto the battlefield. You gain 1 life.').
card_image_name('thopter foundry'/'C13', 'thopter foundry').
card_uid('thopter foundry'/'C13', 'C13:Thopter Foundry:thopter foundry').
card_rarity('thopter foundry'/'C13', 'Uncommon').
card_artist('thopter foundry'/'C13', 'Ralph Horsley').
card_number('thopter foundry'/'C13', '234').
card_flavor_text('thopter foundry'/'C13', '\"Etherium is limited. Innovation is not.\"\n—Tezzeret').
card_multiverse_id('thopter foundry'/'C13', '376549').

card_in_set('thornwind faeries', 'C13').
card_original_type('thornwind faeries'/'C13', 'Creature — Faerie').
card_original_text('thornwind faeries'/'C13', 'Flying{T}: Thornwind Faeries deals 1 damage to target creature or player.').
card_image_name('thornwind faeries'/'C13', 'thornwind faeries').
card_uid('thornwind faeries'/'C13', 'C13:Thornwind Faeries:thornwind faeries').
card_rarity('thornwind faeries'/'C13', 'Common').
card_artist('thornwind faeries'/'C13', 'Rebecca Guay').
card_number('thornwind faeries'/'C13', '61').
card_flavor_text('thornwind faeries'/'C13', 'Guarding the ship is the Thornwinds\' first concern. Getting along with the locals ranks fourth or fifth at best.').
card_multiverse_id('thornwind faeries'/'C13', '376550').

card_in_set('thousand-year elixir', 'C13').
card_original_type('thousand-year elixir'/'C13', 'Artifact').
card_original_text('thousand-year elixir'/'C13', 'You may activate abilities of creatures you control as though those creatures had haste.{1}, {T}: Untap target creature.').
card_image_name('thousand-year elixir'/'C13', 'thousand-year elixir').
card_uid('thousand-year elixir'/'C13', 'C13:Thousand-Year Elixir:thousand-year elixir').
card_rarity('thousand-year elixir'/'C13', 'Rare').
card_artist('thousand-year elixir'/'C13', 'Richard Sardinha').
card_number('thousand-year elixir'/'C13', '266').
card_flavor_text('thousand-year elixir'/'C13', 'Paradoxically, to tilt the massive jug for a sip, you\'d need the energy of the giant\'s tonic.').
card_multiverse_id('thousand-year elixir'/'C13', '376551').

card_in_set('thraximundar', 'C13').
card_original_type('thraximundar'/'C13', 'Legendary Creature — Zombie Assassin').
card_original_text('thraximundar'/'C13', 'HasteWhenever Thraximundar attacks, defending player sacrifices a creature.Whenever a player sacrifices a creature, you may put a +1/+1 counter on Thraximundar.').
card_image_name('thraximundar'/'C13', 'thraximundar').
card_uid('thraximundar'/'C13', 'C13:Thraximundar:thraximundar').
card_rarity('thraximundar'/'C13', 'Mythic Rare').
card_artist('thraximundar'/'C13', 'Raymond Swanland').
card_number('thraximundar'/'C13', '221').
card_flavor_text('thraximundar'/'C13', 'His name means \"he who paints the earth red.\"').
card_multiverse_id('thraximundar'/'C13', '376552').

card_in_set('thunderstaff', 'C13').
card_original_type('thunderstaff'/'C13', 'Artifact').
card_original_text('thunderstaff'/'C13', 'As long as Thunderstaff is untapped, if a creature would deal combat damage to you, prevent 1 of that damage.{2}, {T}: Attacking creatures get +1/+0 until end of turn.').
card_image_name('thunderstaff'/'C13', 'thunderstaff').
card_uid('thunderstaff'/'C13', 'C13:Thunderstaff:thunderstaff').
card_rarity('thunderstaff'/'C13', 'Uncommon').
card_artist('thunderstaff'/'C13', 'Cliff Childs').
card_number('thunderstaff'/'C13', '267').
card_flavor_text('thunderstaff'/'C13', 'Jort the Thunder-Mad spent his life perfecting a way to harness the power of the heavens.').
card_multiverse_id('thunderstaff'/'C13', '376553').

card_in_set('tidal force', 'C13').
card_original_type('tidal force'/'C13', 'Creature — Elemental').
card_original_text('tidal force'/'C13', 'At the beginning of each upkeep, you may tap or untap target permanent.').
card_first_print('tidal force', 'C13').
card_image_name('tidal force'/'C13', 'tidal force').
card_uid('tidal force'/'C13', 'C13:Tidal Force:tidal force').
card_rarity('tidal force'/'C13', 'Rare').
card_artist('tidal force'/'C13', 'Phill Simmer').
card_number('tidal force'/'C13', '62').
card_flavor_text('tidal force'/'C13', '\"Do not speak the name of the sea, for if it should hear you and rise up, no fortress in the world could hide you.\"\n—Gimara of Halimar').
card_multiverse_id('tidal force'/'C13', '376554').

card_in_set('tidehollow strix', 'C13').
card_original_type('tidehollow strix'/'C13', 'Artifact Creature — Bird').
card_original_text('tidehollow strix'/'C13', 'FlyingDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('tidehollow strix'/'C13', 'tidehollow strix').
card_uid('tidehollow strix'/'C13', 'C13:Tidehollow Strix:tidehollow strix').
card_rarity('tidehollow strix'/'C13', 'Common').
card_artist('tidehollow strix'/'C13', 'Cyril Van Der Haegen').
card_number('tidehollow strix'/'C13', '222').
card_flavor_text('tidehollow strix'/'C13', 'The scullers beneath Esper keep strixes as trained pets, and set them loose when a fare refuses to pay.').
card_multiverse_id('tidehollow strix'/'C13', '376555').

card_in_set('tooth and claw', 'C13').
card_original_type('tooth and claw'/'C13', 'Enchantment').
card_original_text('tooth and claw'/'C13', 'Sacrifice two creatures: Put a 3/1 red Beast creature token named Carnivore onto the battlefield.').
card_image_name('tooth and claw'/'C13', 'tooth and claw').
card_uid('tooth and claw'/'C13', 'C13:Tooth and Claw:tooth and claw').
card_rarity('tooth and claw'/'C13', 'Rare').
card_artist('tooth and claw'/'C13', 'Val Mayerik').
card_number('tooth and claw'/'C13', '127').
card_flavor_text('tooth and claw'/'C13', '\"You deify nature as female—kind and gentle—yet you cringe at what is natural.\"\n—Mirri of the Weatherlight').
card_multiverse_id('tooth and claw'/'C13', '376556').

card_in_set('tower gargoyle', 'C13').
card_original_type('tower gargoyle'/'C13', 'Artifact Creature — Gargoyle').
card_original_text('tower gargoyle'/'C13', 'Flying').
card_image_name('tower gargoyle'/'C13', 'tower gargoyle').
card_uid('tower gargoyle'/'C13', 'C13:Tower Gargoyle:tower gargoyle').
card_rarity('tower gargoyle'/'C13', 'Uncommon').
card_artist('tower gargoyle'/'C13', 'Matt Cavotta').
card_number('tower gargoyle'/'C13', '223').
card_flavor_text('tower gargoyle'/'C13', '\"Esper, like any work of art, can be truly appreciated only from a distance.\"\n—Tezzeret').
card_multiverse_id('tower gargoyle'/'C13', '376557').

card_in_set('tower of fortunes', 'C13').
card_original_type('tower of fortunes'/'C13', 'Artifact').
card_original_text('tower of fortunes'/'C13', '{8}, {T}: Draw four cards.').
card_image_name('tower of fortunes'/'C13', 'tower of fortunes').
card_uid('tower of fortunes'/'C13', 'C13:Tower of Fortunes:tower of fortunes').
card_rarity('tower of fortunes'/'C13', 'Rare').
card_artist('tower of fortunes'/'C13', 'Matt Cavotta').
card_number('tower of fortunes'/'C13', '268').
card_flavor_text('tower of fortunes'/'C13', 'The ur-golem etchings begin by celebrating Mirrodin\'s creator, a golem of almost limitless power. They end by cursing its protector, a being called Memnarch.').
card_multiverse_id('tower of fortunes'/'C13', '376558').

card_in_set('toxic deluge', 'C13').
card_original_type('toxic deluge'/'C13', 'Sorcery').
card_original_text('toxic deluge'/'C13', 'As an additional cost to cast Toxic Deluge, pay X life.All creatures get -X/-X until end of turn.').
card_first_print('toxic deluge', 'C13').
card_image_name('toxic deluge'/'C13', 'toxic deluge').
card_uid('toxic deluge'/'C13', 'C13:Toxic Deluge:toxic deluge').
card_rarity('toxic deluge'/'C13', 'Rare').
card_artist('toxic deluge'/'C13', 'Svetlin Velinov').
card_number('toxic deluge'/'C13', '96').
card_flavor_text('toxic deluge'/'C13', '\"It\'s a difficult task to quarantine a plague that moves with the clouds.\"\n—Esara, healer adept').
card_multiverse_id('toxic deluge'/'C13', '376559').

card_in_set('tranquil thicket', 'C13').
card_original_type('tranquil thicket'/'C13', 'Land').
card_original_text('tranquil thicket'/'C13', 'Tranquil Thicket enters the battlefield tapped.{T}: Add {G} to your mana pool.Cycling {G} ({G}, Discard this card: Draw a card.)').
card_image_name('tranquil thicket'/'C13', 'tranquil thicket').
card_uid('tranquil thicket'/'C13', 'C13:Tranquil Thicket:tranquil thicket').
card_rarity('tranquil thicket'/'C13', 'Common').
card_artist('tranquil thicket'/'C13', 'Heather Hudson').
card_number('tranquil thicket'/'C13', '329').
card_multiverse_id('tranquil thicket'/'C13', '376560').

card_in_set('transguild promenade', 'C13').
card_original_type('transguild promenade'/'C13', 'Land').
card_original_text('transguild promenade'/'C13', 'Transguild Promenade enters the battlefield tapped.When Transguild Promenade enters the battlefield, sacrifice it unless you pay {1}.{T}: Add one mana of any color to your mana pool.').
card_image_name('transguild promenade'/'C13', 'transguild promenade').
card_uid('transguild promenade'/'C13', 'C13:Transguild Promenade:transguild promenade').
card_rarity('transguild promenade'/'C13', 'Common').
card_artist('transguild promenade'/'C13', 'Noah Bradley').
card_number('transguild promenade'/'C13', '330').
card_multiverse_id('transguild promenade'/'C13', '376561').

card_in_set('true-name nemesis', 'C13').
card_original_type('true-name nemesis'/'C13', 'Creature — Merfolk Rogue').
card_original_text('true-name nemesis'/'C13', 'As True-Name Nemesis enters the battlefield, choose a player.True-Name Nemesis has protection from the chosen player. (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything controlled by that player.)').
card_first_print('true-name nemesis', 'C13').
card_image_name('true-name nemesis'/'C13', 'true-name nemesis').
card_uid('true-name nemesis'/'C13', 'C13:True-Name Nemesis:true-name nemesis').
card_rarity('true-name nemesis'/'C13', 'Rare').
card_artist('true-name nemesis'/'C13', 'Zack Stella').
card_number('true-name nemesis'/'C13', '63').
card_multiverse_id('true-name nemesis'/'C13', '376562').

card_in_set('tumble', 'C13').
card_original_type('tumble'/'C13', 'Sorcery').
card_original_text('tumble'/'C13', 'Rough deals 2 damage to each creature without flying.\n//').
card_image_name('tumble'/'C13', 'roughtumble').
card_uid('tumble'/'C13', 'C13:Tumble:roughtumble').
card_rarity('tumble'/'C13', 'Uncommon').
card_artist('tumble'/'C13', 'Luca Zontini').
card_number('tumble'/'C13', '118b').
card_multiverse_id('tumble'/'C13', '376475').

card_in_set('unexpectedly absent', 'C13').
card_original_type('unexpectedly absent'/'C13', 'Instant').
card_original_text('unexpectedly absent'/'C13', 'Put target nonland permanent into its owner\'s library just beneath the top X cards of that library.').
card_first_print('unexpectedly absent', 'C13').
card_image_name('unexpectedly absent'/'C13', 'unexpectedly absent').
card_uid('unexpectedly absent'/'C13', 'C13:Unexpectedly Absent:unexpectedly absent').
card_rarity('unexpectedly absent'/'C13', 'Rare').
card_artist('unexpectedly absent'/'C13', 'Min Yum').
card_number('unexpectedly absent'/'C13', '25').
card_flavor_text('unexpectedly absent'/'C13', 'Once you\'ve been dragged down the currents of time, you\'ll never quite trust your own permanence again.').
card_multiverse_id('unexpectedly absent'/'C13', '376563').

card_in_set('urza\'s factory', 'C13').
card_original_type('urza\'s factory'/'C13', 'Land — Urza\'s').
card_original_text('urza\'s factory'/'C13', '{T}: Add {1} to your mana pool.{7}, {T}: Put a 2/2 colorless Assembly-Worker artifact creature token onto the battlefield.').
card_image_name('urza\'s factory'/'C13', 'urza\'s factory').
card_uid('urza\'s factory'/'C13', 'C13:Urza\'s Factory:urza\'s factory').
card_rarity('urza\'s factory'/'C13', 'Uncommon').
card_artist('urza\'s factory'/'C13', 'Mark Tedin').
card_number('urza\'s factory'/'C13', '331').
card_flavor_text('urza\'s factory'/'C13', '\"Though their ideals are leagues apart, Urza\'s and Mishra\'s creations have a surprising harmony with one another.\"\n—Tocasia, journal entry').
card_multiverse_id('urza\'s factory'/'C13', '376564').

card_in_set('uyo, silent prophet', 'C13').
card_original_type('uyo, silent prophet'/'C13', 'Legendary Creature — Moonfolk Wizard').
card_original_text('uyo, silent prophet'/'C13', 'Flying{2}, Return two lands you control to their owner\'s hand: Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_image_name('uyo, silent prophet'/'C13', 'uyo, silent prophet').
card_uid('uyo, silent prophet'/'C13', 'C13:Uyo, Silent Prophet:uyo, silent prophet').
card_rarity('uyo, silent prophet'/'C13', 'Rare').
card_artist('uyo, silent prophet'/'C13', 'John Bolton').
card_number('uyo, silent prophet'/'C13', '64').
card_multiverse_id('uyo, silent prophet'/'C13', '376565').

card_in_set('valley rannet', 'C13').
card_original_type('valley rannet'/'C13', 'Creature — Beast').
card_original_text('valley rannet'/'C13', 'Mountaincycling {2}, forestcycling {2} ({2}, Discard this card: Search your library for a Mountain or Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_image_name('valley rannet'/'C13', 'valley rannet').
card_uid('valley rannet'/'C13', 'C13:Valley Rannet:valley rannet').
card_rarity('valley rannet'/'C13', 'Common').
card_artist('valley rannet'/'C13', 'Dave Allsop').
card_number('valley rannet'/'C13', '224').
card_multiverse_id('valley rannet'/'C13', '376566').

card_in_set('vampire nighthawk', 'C13').
card_original_type('vampire nighthawk'/'C13', 'Creature — Vampire Shaman').
card_original_text('vampire nighthawk'/'C13', 'FlyingDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_image_name('vampire nighthawk'/'C13', 'vampire nighthawk').
card_uid('vampire nighthawk'/'C13', 'C13:Vampire Nighthawk:vampire nighthawk').
card_rarity('vampire nighthawk'/'C13', 'Uncommon').
card_artist('vampire nighthawk'/'C13', 'Jason Chan').
card_number('vampire nighthawk'/'C13', '97').
card_multiverse_id('vampire nighthawk'/'C13', '376567').

card_in_set('vile requiem', 'C13').
card_original_type('vile requiem'/'C13', 'Enchantment').
card_original_text('vile requiem'/'C13', 'At the beginning of your upkeep, you may put a verse counter on Vile Requiem.{1}{B}, Sacrifice Vile Requiem: Destroy up to X target nonblack creatures, where X is the number of verse counters on Vile Requiem. They can\'t be regenerated.').
card_image_name('vile requiem'/'C13', 'vile requiem').
card_uid('vile requiem'/'C13', 'C13:Vile Requiem:vile requiem').
card_rarity('vile requiem'/'C13', 'Uncommon').
card_artist('vile requiem'/'C13', 'Carl Critchlow').
card_number('vile requiem'/'C13', '98').
card_multiverse_id('vile requiem'/'C13', '376568').

card_in_set('viscera seer', 'C13').
card_original_type('viscera seer'/'C13', 'Creature — Vampire Wizard').
card_original_text('viscera seer'/'C13', 'Sacrifice a creature: Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_image_name('viscera seer'/'C13', 'viscera seer').
card_uid('viscera seer'/'C13', 'C13:Viscera Seer:viscera seer').
card_rarity('viscera seer'/'C13', 'Common').
card_artist('viscera seer'/'C13', 'John Stanko').
card_number('viscera seer'/'C13', '99').
card_flavor_text('viscera seer'/'C13', 'In matters of life and death, he trusts his gut.').
card_multiverse_id('viscera seer'/'C13', '376569').

card_in_set('viseling', 'C13').
card_original_type('viseling'/'C13', 'Artifact Creature — Construct').
card_original_text('viseling'/'C13', 'At the beginning of each opponent\'s upkeep, Viseling deals X damage to that player, where X is the number of cards in his or her hand minus 4.').
card_image_name('viseling'/'C13', 'viseling').
card_uid('viseling'/'C13', 'C13:Viseling:viseling').
card_rarity('viseling'/'C13', 'Uncommon').
card_artist('viseling'/'C13', 'Kev Walker').
card_number('viseling'/'C13', '269').
card_flavor_text('viseling'/'C13', '\"This may hurt a lot.\"').
card_multiverse_id('viseling'/'C13', '376570').

card_in_set('vision skeins', 'C13').
card_original_type('vision skeins'/'C13', 'Instant').
card_original_text('vision skeins'/'C13', 'Each player draws two cards.').
card_image_name('vision skeins'/'C13', 'vision skeins').
card_uid('vision skeins'/'C13', 'C13:Vision Skeins:vision skeins').
card_rarity('vision skeins'/'C13', 'Common').
card_artist('vision skeins'/'C13', 'Aleksi Briclot').
card_number('vision skeins'/'C13', '65').
card_flavor_text('vision skeins'/'C13', '\"I could see in the other mage\'s eyes that he\'d thought of it too. Then it became a race to exploit the knowledge first.\"').
card_multiverse_id('vision skeins'/'C13', '376571').

card_in_set('vitu-ghazi, the city-tree', 'C13').
card_original_type('vitu-ghazi, the city-tree'/'C13', 'Land').
card_original_text('vitu-ghazi, the city-tree'/'C13', '{T}: Add {1} to your mana pool.{2}{G}{W}, {T}: Put a 1/1 green Saproling creature token onto the battlefield.').
card_image_name('vitu-ghazi, the city-tree'/'C13', 'vitu-ghazi, the city-tree').
card_uid('vitu-ghazi, the city-tree'/'C13', 'C13:Vitu-Ghazi, the City-Tree:vitu-ghazi, the city-tree').
card_rarity('vitu-ghazi, the city-tree'/'C13', 'Uncommon').
card_artist('vitu-ghazi, the city-tree'/'C13', 'Martina Pilcerova').
card_number('vitu-ghazi, the city-tree'/'C13', '332').
card_flavor_text('vitu-ghazi, the city-tree'/'C13', 'In the autumn, she casts her seeds across the streets below, and come spring, her children rise in service to the Conclave.').
card_multiverse_id('vitu-ghazi, the city-tree'/'C13', '376572').

card_in_set('vivid crag', 'C13').
card_original_type('vivid crag'/'C13', 'Land').
card_original_text('vivid crag'/'C13', 'Vivid Crag enters the battlefield tapped with two charge counters on it.{T}: Add {R} to your mana pool.{T}, Remove a charge counter from Vivid Crag: Add one mana of any color to your mana pool.').
card_image_name('vivid crag'/'C13', 'vivid crag').
card_uid('vivid crag'/'C13', 'C13:Vivid Crag:vivid crag').
card_rarity('vivid crag'/'C13', 'Uncommon').
card_artist('vivid crag'/'C13', 'Martina Pilcerova').
card_number('vivid crag'/'C13', '333').
card_multiverse_id('vivid crag'/'C13', '376573').

card_in_set('vivid creek', 'C13').
card_original_type('vivid creek'/'C13', 'Land').
card_original_text('vivid creek'/'C13', 'Vivid Creek enters the battlefield tapped with two charge counters on it.{T}: Add {U} to your mana pool.{T}, Remove a charge counter from Vivid Creek: Add one mana of any color to your mana pool.').
card_image_name('vivid creek'/'C13', 'vivid creek').
card_uid('vivid creek'/'C13', 'C13:Vivid Creek:vivid creek').
card_rarity('vivid creek'/'C13', 'Uncommon').
card_artist('vivid creek'/'C13', 'Fred Fields').
card_number('vivid creek'/'C13', '334').
card_multiverse_id('vivid creek'/'C13', '376574').

card_in_set('vivid grove', 'C13').
card_original_type('vivid grove'/'C13', 'Land').
card_original_text('vivid grove'/'C13', 'Vivid Grove enters the battlefield tapped with two charge counters on it.{T}: Add {G} to your mana pool.{T}, Remove a charge counter from Vivid Grove: Add one mana of any color to your mana pool.').
card_image_name('vivid grove'/'C13', 'vivid grove').
card_uid('vivid grove'/'C13', 'C13:Vivid Grove:vivid grove').
card_rarity('vivid grove'/'C13', 'Uncommon').
card_artist('vivid grove'/'C13', 'Howard Lyon').
card_number('vivid grove'/'C13', '335').
card_multiverse_id('vivid grove'/'C13', '376575').

card_in_set('vivid marsh', 'C13').
card_original_type('vivid marsh'/'C13', 'Land').
card_original_text('vivid marsh'/'C13', 'Vivid Marsh enters the battlefield tapped with two charge counters on it.{T}: Add {B} to your mana pool.{T}, Remove a charge counter from Vivid Marsh: Add one mana of any color to your mana pool.').
card_image_name('vivid marsh'/'C13', 'vivid marsh').
card_uid('vivid marsh'/'C13', 'C13:Vivid Marsh:vivid marsh').
card_rarity('vivid marsh'/'C13', 'Uncommon').
card_artist('vivid marsh'/'C13', 'John Avon').
card_number('vivid marsh'/'C13', '336').
card_multiverse_id('vivid marsh'/'C13', '376576').

card_in_set('vizkopa guildmage', 'C13').
card_original_type('vizkopa guildmage'/'C13', 'Creature — Human Wizard').
card_original_text('vizkopa guildmage'/'C13', '{1}{W}{B}: Target creature gains lifelink until end of turn.{1}{W}{B}: Whenever you gain life this turn, each opponent loses that much life.').
card_image_name('vizkopa guildmage'/'C13', 'vizkopa guildmage').
card_uid('vizkopa guildmage'/'C13', 'C13:Vizkopa Guildmage:vizkopa guildmage').
card_rarity('vizkopa guildmage'/'C13', 'Uncommon').
card_artist('vizkopa guildmage'/'C13', 'Tyler Jacobson').
card_number('vizkopa guildmage'/'C13', '225').
card_multiverse_id('vizkopa guildmage'/'C13', '376577').

card_in_set('walker of the grove', 'C13').
card_original_type('walker of the grove'/'C13', 'Creature — Elemental').
card_original_text('walker of the grove'/'C13', 'When Walker of the Grove leaves the battlefield, put a 4/4 green Elemental creature token onto the battlefield.Evoke {4}{G} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_image_name('walker of the grove'/'C13', 'walker of the grove').
card_uid('walker of the grove'/'C13', 'C13:Walker of the Grove:walker of the grove').
card_rarity('walker of the grove'/'C13', 'Uncommon').
card_artist('walker of the grove'/'C13', 'Todd Lockwood').
card_number('walker of the grove'/'C13', '175').
card_multiverse_id('walker of the grove'/'C13', '376578').

card_in_set('wall of reverence', 'C13').
card_original_type('wall of reverence'/'C13', 'Creature — Spirit Wall').
card_original_text('wall of reverence'/'C13', 'Defender, flyingAt the beginning of your end step, you may gain life equal to the power of target creature you control.').
card_image_name('wall of reverence'/'C13', 'wall of reverence').
card_uid('wall of reverence'/'C13', 'C13:Wall of Reverence:wall of reverence').
card_rarity('wall of reverence'/'C13', 'Rare').
card_artist('wall of reverence'/'C13', 'Wayne Reynolds').
card_number('wall of reverence'/'C13', '26').
card_flavor_text('wall of reverence'/'C13', 'The lives of elves are long, but their memories are longer. Even after death, they do not desert their homes.').
card_multiverse_id('wall of reverence'/'C13', '376579').

card_in_set('war cadence', 'C13').
card_original_type('war cadence'/'C13', 'Enchantment').
card_original_text('war cadence'/'C13', '{X}{R}: This turn, creatures can\'t block unless their controller pays {X} for each blocking creature he or she controls.').
card_image_name('war cadence'/'C13', 'war cadence').
card_uid('war cadence'/'C13', 'C13:War Cadence:war cadence').
card_rarity('war cadence'/'C13', 'Uncommon').
card_artist('war cadence'/'C13', 'John Matson').
card_number('war cadence'/'C13', '128').
card_flavor_text('war cadence'/'C13', 'The rhythm isn\'t a call to arms, but a signal that the goblins have made a new enemy.').
card_multiverse_id('war cadence'/'C13', '376580').

card_in_set('warstorm surge', 'C13').
card_original_type('warstorm surge'/'C13', 'Enchantment').
card_original_text('warstorm surge'/'C13', 'Whenever a creature enters the battlefield under your control, it deals damage equal to its power to target creature or player.').
card_image_name('warstorm surge'/'C13', 'warstorm surge').
card_uid('warstorm surge'/'C13', 'C13:Warstorm Surge:warstorm surge').
card_rarity('warstorm surge'/'C13', 'Rare').
card_artist('warstorm surge'/'C13', 'Raymond Swanland').
card_number('warstorm surge'/'C13', '129').
card_flavor_text('warstorm surge'/'C13', '\"Listen to the roar! Feel the thunder! The Immersturm shouts its approval with every bolt of lightning!\"').
card_multiverse_id('warstorm surge'/'C13', '376581').

card_in_set('wash out', 'C13').
card_original_type('wash out'/'C13', 'Sorcery').
card_original_text('wash out'/'C13', 'Return all permanents of the color of your choice to their owners\' hands.').
card_image_name('wash out'/'C13', 'wash out').
card_uid('wash out'/'C13', 'C13:Wash Out:wash out').
card_rarity('wash out'/'C13', 'Uncommon').
card_artist('wash out'/'C13', 'Steven Belledin').
card_number('wash out'/'C13', '66').
card_flavor_text('wash out'/'C13', '\"It\'s not the darkness I fear but dull and numbing shades of gray.\"\n—Sydri, galvanic genius').
card_multiverse_id('wash out'/'C13', '376582').

card_in_set('wayfarer\'s bauble', 'C13').
card_original_type('wayfarer\'s bauble'/'C13', 'Artifact').
card_original_text('wayfarer\'s bauble'/'C13', '{2}, {T}, Sacrifice Wayfarer\'s Bauble: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_image_name('wayfarer\'s bauble'/'C13', 'wayfarer\'s bauble').
card_uid('wayfarer\'s bauble'/'C13', 'C13:Wayfarer\'s Bauble:wayfarer\'s bauble').
card_rarity('wayfarer\'s bauble'/'C13', 'Common').
card_artist('wayfarer\'s bauble'/'C13', 'Darrell Riche').
card_number('wayfarer\'s bauble'/'C13', '270').
card_flavor_text('wayfarer\'s bauble'/'C13', 'It is the forest beyond the horizon, the mountain waiting to be climbed, the new land across the endless sea.').
card_multiverse_id('wayfarer\'s bauble'/'C13', '376583').

card_in_set('well of lost dreams', 'C13').
card_original_type('well of lost dreams'/'C13', 'Artifact').
card_original_text('well of lost dreams'/'C13', 'Whenever you gain life, you may pay {X}, where X is less than or equal to the amount of life you gained. If you do, draw X cards.').
card_image_name('well of lost dreams'/'C13', 'well of lost dreams').
card_uid('well of lost dreams'/'C13', 'C13:Well of Lost Dreams:well of lost dreams').
card_rarity('well of lost dreams'/'C13', 'Rare').
card_artist('well of lost dreams'/'C13', 'Jeff Miracola').
card_number('well of lost dreams'/'C13', '271').
card_flavor_text('well of lost dreams'/'C13', 'Some say the knowledge lost during the Ritual of Rebuking is returned through the well\'s waters.').
card_multiverse_id('well of lost dreams'/'C13', '376584').

card_in_set('where ancients tread', 'C13').
card_original_type('where ancients tread'/'C13', 'Enchantment').
card_original_text('where ancients tread'/'C13', 'Whenever a creature with power 5 or greater enters the battlefield under your control, you may have Where Ancients Tread deal 5 damage to target creature or player.').
card_image_name('where ancients tread'/'C13', 'where ancients tread').
card_uid('where ancients tread'/'C13', 'C13:Where Ancients Tread:where ancients tread').
card_rarity('where ancients tread'/'C13', 'Rare').
card_artist('where ancients tread'/'C13', 'Zoltan Boros & Gabor Szikszai').
card_number('where ancients tread'/'C13', '130').
card_flavor_text('where ancients tread'/'C13', '\"Never subtle nor cryptic is the reckoning of the mighty.\"\n—Mayael the Anima').
card_multiverse_id('where ancients tread'/'C13', '376585').

card_in_set('widespread panic', 'C13').
card_original_type('widespread panic'/'C13', 'Enchantment').
card_original_text('widespread panic'/'C13', 'Whenever a spell or ability causes its controller to shuffle his or her library, that player puts a card from his or her hand on top of his or her library.').
card_first_print('widespread panic', 'C13').
card_image_name('widespread panic'/'C13', 'widespread panic').
card_uid('widespread panic'/'C13', 'C13:Widespread Panic:widespread panic').
card_rarity('widespread panic'/'C13', 'Rare').
card_artist('widespread panic'/'C13', 'Dave Kendall').
card_number('widespread panic'/'C13', '131').
card_flavor_text('widespread panic'/'C13', '\"How thin is the veneer of civilization! A modest measure of panic will wash it off completely.\"\n—Guard Erdrud of An Karras').
card_multiverse_id('widespread panic'/'C13', '376586').

card_in_set('wight of precinct six', 'C13').
card_original_type('wight of precinct six'/'C13', 'Creature — Zombie').
card_original_text('wight of precinct six'/'C13', 'Wight of Precinct Six gets +1/+1 for each creature card in your opponents\' graveyards.').
card_image_name('wight of precinct six'/'C13', 'wight of precinct six').
card_uid('wight of precinct six'/'C13', 'C13:Wight of Precinct Six:wight of precinct six').
card_rarity('wight of precinct six'/'C13', 'Uncommon').
card_artist('wight of precinct six'/'C13', 'Ryan Barger').
card_number('wight of precinct six'/'C13', '100').
card_flavor_text('wight of precinct six'/'C13', 'Even the lost and undead need a protector.').
card_multiverse_id('wight of precinct six'/'C13', '376587').

card_in_set('wild ricochet', 'C13').
card_original_type('wild ricochet'/'C13', 'Instant').
card_original_text('wild ricochet'/'C13', 'You may choose new targets for target instant or sorcery spell. Then copy that spell. You may choose new targets for the copy.').
card_image_name('wild ricochet'/'C13', 'wild ricochet').
card_uid('wild ricochet'/'C13', 'C13:Wild Ricochet:wild ricochet').
card_rarity('wild ricochet'/'C13', 'Rare').
card_artist('wild ricochet'/'C13', 'Dan Scott').
card_number('wild ricochet'/'C13', '132').
card_flavor_text('wild ricochet'/'C13', '\"May your day never match your expectations.\"\n—Scriptus Fleen, flectomancer').
card_multiverse_id('wild ricochet'/'C13', '376588').

card_in_set('winged coatl', 'C13').
card_original_type('winged coatl'/'C13', 'Creature — Snake').
card_original_text('winged coatl'/'C13', 'Flash (You may cast this spell any time you could cast an instant.)FlyingDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_image_name('winged coatl'/'C13', 'winged coatl').
card_uid('winged coatl'/'C13', 'C13:Winged Coatl:winged coatl').
card_rarity('winged coatl'/'C13', 'Common').
card_artist('winged coatl'/'C13', 'Izzy').
card_number('winged coatl'/'C13', '226').
card_flavor_text('winged coatl'/'C13', 'The nacatl called this new species \"vetli,\" their word for poison arrows.').
card_multiverse_id('winged coatl'/'C13', '376589').

card_in_set('witch hunt', 'C13').
card_original_type('witch hunt'/'C13', 'Enchantment').
card_original_text('witch hunt'/'C13', 'Players can\'t gain life.At the beginning of your upkeep, Witch Hunt deals 4 damage to you.At the beginning of your end step, target opponent chosen at random gains control of Witch Hunt.').
card_first_print('witch hunt', 'C13').
card_image_name('witch hunt'/'C13', 'witch hunt').
card_uid('witch hunt'/'C13', 'C13:Witch Hunt:witch hunt').
card_rarity('witch hunt'/'C13', 'Rare').
card_artist('witch hunt'/'C13', 'Karl Kopinski').
card_number('witch hunt'/'C13', '133').
card_flavor_text('witch hunt'/'C13', 'The accusations spread like hungry flames.').
card_multiverse_id('witch hunt'/'C13', '376590').

card_in_set('wonder', 'C13').
card_original_type('wonder'/'C13', 'Creature — Incarnation').
card_original_text('wonder'/'C13', 'FlyingAs long as Wonder is in your graveyard and you control an Island, creatures you control have flying.').
card_image_name('wonder'/'C13', 'wonder').
card_uid('wonder'/'C13', 'C13:Wonder:wonder').
card_rarity('wonder'/'C13', 'Uncommon').
card_artist('wonder'/'C13', 'Rebecca Guay').
card_number('wonder'/'C13', '67').
card_flavor_text('wonder'/'C13', '\"The awestruck birds gazed at Wonder. Slowly, timidly, they rose into the air.\"\n—Scroll of Beginnings').
card_multiverse_id('wonder'/'C13', '376591').

card_in_set('wrath of god', 'C13').
card_original_type('wrath of god'/'C13', 'Sorcery').
card_original_text('wrath of god'/'C13', 'Destroy all creatures. They can\'t be regenerated.').
card_image_name('wrath of god'/'C13', 'wrath of god').
card_uid('wrath of god'/'C13', 'C13:Wrath of God:wrath of god').
card_rarity('wrath of god'/'C13', 'Rare').
card_artist('wrath of god'/'C13', 'Kev Walker').
card_number('wrath of god'/'C13', '27').
card_multiverse_id('wrath of god'/'C13', '376592').
