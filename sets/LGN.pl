% Legions

set('LGN').
set_name('LGN', 'Legions').
set_release_date('LGN', '2003-02-03').
set_border('LGN', 'black').
set_type('LGN', 'expansion').
set_block('LGN', 'Onslaught').

card_in_set('akroma\'s devoted', 'LGN').
card_original_type('akroma\'s devoted'/'LGN', 'Creature — Cleric').
card_original_text('akroma\'s devoted'/'LGN', 'Attacking doesn\'t cause Clerics to tap.').
card_first_print('akroma\'s devoted', 'LGN').
card_image_name('akroma\'s devoted'/'LGN', 'akroma\'s devoted').
card_uid('akroma\'s devoted'/'LGN', 'LGN:Akroma\'s Devoted:akroma\'s devoted').
card_rarity('akroma\'s devoted'/'LGN', 'Uncommon').
card_artist('akroma\'s devoted'/'LGN', 'Dave Dorman').
card_number('akroma\'s devoted'/'LGN', '2').
card_flavor_text('akroma\'s devoted'/'LGN', 'Akroma asked for only one thing from her troops: unwavering, unconditional loyalty.').
card_multiverse_id('akroma\'s devoted'/'LGN', '44747').

card_in_set('akroma, angel of wrath', 'LGN').
card_original_type('akroma, angel of wrath'/'LGN', 'Creature — Angel Legend').
card_original_text('akroma, angel of wrath'/'LGN', 'Flying, first strike, trample, haste, protection from black, protection from red\nAttacking doesn\'t cause Akroma, Angel of Wrath to tap.').
card_first_print('akroma, angel of wrath', 'LGN').
card_image_name('akroma, angel of wrath'/'LGN', 'akroma, angel of wrath').
card_uid('akroma, angel of wrath'/'LGN', 'LGN:Akroma, Angel of Wrath:akroma, angel of wrath').
card_rarity('akroma, angel of wrath'/'LGN', 'Rare').
card_artist('akroma, angel of wrath'/'LGN', 'Ron Spears').
card_number('akroma, angel of wrath'/'LGN', '1').
card_flavor_text('akroma, angel of wrath'/'LGN', '\"No rest. No mercy. No matter what.\"').
card_multiverse_id('akroma, angel of wrath'/'LGN', '42049').

card_in_set('aphetto exterminator', 'LGN').
card_original_type('aphetto exterminator'/'LGN', 'Creature — Wizard').
card_original_text('aphetto exterminator'/'LGN', 'Morph {3}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Aphetto Exterminator is turned face up, target creature gets -3/-3 until end of turn.').
card_first_print('aphetto exterminator', 'LGN').
card_image_name('aphetto exterminator'/'LGN', 'aphetto exterminator').
card_uid('aphetto exterminator'/'LGN', 'LGN:Aphetto Exterminator:aphetto exterminator').
card_rarity('aphetto exterminator'/'LGN', 'Uncommon').
card_artist('aphetto exterminator'/'LGN', 'Scott M. Fischer').
card_number('aphetto exterminator'/'LGN', '59').
card_multiverse_id('aphetto exterminator'/'LGN', '42060').

card_in_set('aven envoy', 'LGN').
card_original_type('aven envoy'/'LGN', 'Creature — Bird Soldier').
card_original_text('aven envoy'/'LGN', 'Flying').
card_first_print('aven envoy', 'LGN').
card_image_name('aven envoy'/'LGN', 'aven envoy').
card_uid('aven envoy'/'LGN', 'LGN:Aven Envoy:aven envoy').
card_rarity('aven envoy'/'LGN', 'Common').
card_artist('aven envoy'/'LGN', 'Alex Horley-Orlandelli').
card_number('aven envoy'/'LGN', '30').
card_flavor_text('aven envoy'/'LGN', 'Their grieving faces tell far more than a list of casualties ever could.').
card_multiverse_id('aven envoy'/'LGN', '44325').

card_in_set('aven redeemer', 'LGN').
card_original_type('aven redeemer'/'LGN', 'Creature — Bird Cleric').
card_original_text('aven redeemer'/'LGN', 'Flying\n{T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_first_print('aven redeemer', 'LGN').
card_image_name('aven redeemer'/'LGN', 'aven redeemer').
card_uid('aven redeemer'/'LGN', 'LGN:Aven Redeemer:aven redeemer').
card_rarity('aven redeemer'/'LGN', 'Common').
card_artist('aven redeemer'/'LGN', 'Tim Hildebrandt').
card_number('aven redeemer'/'LGN', '3').
card_flavor_text('aven redeemer'/'LGN', 'Redeemers rise with the sun, and the spirits of the people rise with them.').
card_multiverse_id('aven redeemer'/'LGN', '43497').

card_in_set('aven warhawk', 'LGN').
card_original_type('aven warhawk'/'LGN', 'Creature — Bird Soldier').
card_original_text('aven warhawk'/'LGN', 'Amplify 1 (As this card comes into play, put a +1/+1 counter on it for each Bird and/or Soldier card you reveal in your hand.)\nFlying').
card_first_print('aven warhawk', 'LGN').
card_image_name('aven warhawk'/'LGN', 'aven warhawk').
card_uid('aven warhawk'/'LGN', 'LGN:Aven Warhawk:aven warhawk').
card_rarity('aven warhawk'/'LGN', 'Uncommon').
card_artist('aven warhawk'/'LGN', 'Glen Angus').
card_number('aven warhawk'/'LGN', '4').
card_multiverse_id('aven warhawk'/'LGN', '45838').

card_in_set('bane of the living', 'LGN').
card_original_type('bane of the living'/'LGN', 'Creature — Insect').
card_original_text('bane of the living'/'LGN', 'Morph {X}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Bane of the Living is turned face up, all creatures get -X/-X until end of turn.').
card_first_print('bane of the living', 'LGN').
card_image_name('bane of the living'/'LGN', 'bane of the living').
card_uid('bane of the living'/'LGN', 'LGN:Bane of the Living:bane of the living').
card_rarity('bane of the living'/'LGN', 'Rare').
card_artist('bane of the living'/'LGN', 'Justin Sweet').
card_number('bane of the living'/'LGN', '60').
card_multiverse_id('bane of the living'/'LGN', '44493').

card_in_set('beacon of destiny', 'LGN').
card_original_type('beacon of destiny'/'LGN', 'Creature — Cleric').
card_original_text('beacon of destiny'/'LGN', '{T}: The next time a source of your choice would deal damage to you this turn, that damage is dealt to Beacon of Destiny instead.').
card_first_print('beacon of destiny', 'LGN').
card_image_name('beacon of destiny'/'LGN', 'beacon of destiny').
card_uid('beacon of destiny'/'LGN', 'LGN:Beacon of Destiny:beacon of destiny').
card_rarity('beacon of destiny'/'LGN', 'Rare').
card_artist('beacon of destiny'/'LGN', 'Tim Hildebrandt').
card_number('beacon of destiny'/'LGN', '5').
card_flavor_text('beacon of destiny'/'LGN', '\"We all borrowed life from the Ancestor to exist in this world. Today, I repay that debt.\"').
card_multiverse_id('beacon of destiny'/'LGN', '44496').

card_in_set('berserk murlodont', 'LGN').
card_original_type('berserk murlodont'/'LGN', 'Creature — Beast').
card_original_text('berserk murlodont'/'LGN', 'Whenever a Beast becomes blocked, it gets +1/+1 until end of turn for each creature blocking it.').
card_first_print('berserk murlodont', 'LGN').
card_image_name('berserk murlodont'/'LGN', 'berserk murlodont').
card_uid('berserk murlodont'/'LGN', 'LGN:Berserk Murlodont:berserk murlodont').
card_rarity('berserk murlodont'/'LGN', 'Common').
card_artist('berserk murlodont'/'LGN', 'Arnie Swekel').
card_number('berserk murlodont'/'LGN', '117').
card_flavor_text('berserk murlodont'/'LGN', '\"The harder we press forward, the harder Krosa pushes us back.\"\n—Aven scout').
card_multiverse_id('berserk murlodont'/'LGN', '44334').

card_in_set('blade sliver', 'LGN').
card_original_type('blade sliver'/'LGN', 'Creature — Sliver').
card_original_text('blade sliver'/'LGN', 'All Slivers get +1/+0.').
card_first_print('blade sliver', 'LGN').
card_image_name('blade sliver'/'LGN', 'blade sliver').
card_uid('blade sliver'/'LGN', 'LGN:Blade Sliver:blade sliver').
card_rarity('blade sliver'/'LGN', 'Uncommon').
card_artist('blade sliver'/'LGN', 'David Martin').
card_number('blade sliver'/'LGN', '88').
card_flavor_text('blade sliver'/'LGN', 'After breaking free from the Riptide Project, the slivers quickly adapted to life on Otaria—much to the dismay of life on Otaria.').
card_multiverse_id('blade sliver'/'LGN', '42016').

card_in_set('blood celebrant', 'LGN').
card_original_type('blood celebrant'/'LGN', 'Creature — Cleric').
card_original_text('blood celebrant'/'LGN', '{B}, Pay 1 life: Add one mana of any color to your mana pool.').
card_first_print('blood celebrant', 'LGN').
card_image_name('blood celebrant'/'LGN', 'blood celebrant').
card_uid('blood celebrant'/'LGN', 'LGN:Blood Celebrant:blood celebrant').
card_rarity('blood celebrant'/'LGN', 'Common').
card_artist('blood celebrant'/'LGN', 'Ben Thompson').
card_number('blood celebrant'/'LGN', '61').
card_flavor_text('blood celebrant'/'LGN', '\"Their blood is the nectar that nourishes the Cabal.\"\n—Phage the Untouchable').
card_multiverse_id('blood celebrant'/'LGN', '44206').

card_in_set('bloodstoke howler', 'LGN').
card_original_type('bloodstoke howler'/'LGN', 'Creature — Beast').
card_original_text('bloodstoke howler'/'LGN', 'Morph {6}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Bloodstoke Howler is turned face up, Beasts you control get +3/+0 until end of turn.').
card_first_print('bloodstoke howler', 'LGN').
card_image_name('bloodstoke howler'/'LGN', 'bloodstoke howler').
card_uid('bloodstoke howler'/'LGN', 'LGN:Bloodstoke Howler:bloodstoke howler').
card_rarity('bloodstoke howler'/'LGN', 'Common').
card_artist('bloodstoke howler'/'LGN', 'Christopher Moeller').
card_number('bloodstoke howler'/'LGN', '89').
card_multiverse_id('bloodstoke howler'/'LGN', '44209').

card_in_set('branchsnap lorian', 'LGN').
card_original_type('branchsnap lorian'/'LGN', 'Creature — Beast').
card_original_text('branchsnap lorian'/'LGN', 'Trample\nMorph {G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('branchsnap lorian', 'LGN').
card_image_name('branchsnap lorian'/'LGN', 'branchsnap lorian').
card_uid('branchsnap lorian'/'LGN', 'LGN:Branchsnap Lorian:branchsnap lorian').
card_rarity('branchsnap lorian'/'LGN', 'Uncommon').
card_artist('branchsnap lorian'/'LGN', 'Heather Hudson').
card_number('branchsnap lorian'/'LGN', '118').
card_flavor_text('branchsnap lorian'/'LGN', 'Lorians treat trees and prey the same way: by ripping them limb from limb.').
card_multiverse_id('branchsnap lorian'/'LGN', '37909').

card_in_set('brontotherium', 'LGN').
card_original_type('brontotherium'/'LGN', 'Creature — Beast').
card_original_text('brontotherium'/'LGN', 'Trample\nProvoke (When this attacks, you may have target creature defending player controls untap and block it if able.)').
card_first_print('brontotherium', 'LGN').
card_image_name('brontotherium'/'LGN', 'brontotherium').
card_uid('brontotherium'/'LGN', 'LGN:Brontotherium:brontotherium').
card_rarity('brontotherium'/'LGN', 'Uncommon').
card_artist('brontotherium'/'LGN', 'Carl Critchlow').
card_number('brontotherium'/'LGN', '119').
card_flavor_text('brontotherium'/'LGN', 'Lucky victims get run over. Unlucky victims get run through.').
card_multiverse_id('brontotherium'/'LGN', '45133').

card_in_set('brood sliver', 'LGN').
card_original_type('brood sliver'/'LGN', 'Creature — Sliver').
card_original_text('brood sliver'/'LGN', 'Whenever a Sliver deals combat damage to a player, its controller may put a 1/1 colorless Sliver creature token into play.').
card_first_print('brood sliver', 'LGN').
card_image_name('brood sliver'/'LGN', 'brood sliver').
card_uid('brood sliver'/'LGN', 'LGN:Brood Sliver:brood sliver').
card_rarity('brood sliver'/'LGN', 'Rare').
card_artist('brood sliver'/'LGN', 'Ron Spears').
card_number('brood sliver'/'LGN', '120').
card_flavor_text('brood sliver'/'LGN', 'Within weeks, more slivers nested in Otaria than ever existed on Rath.').
card_multiverse_id('brood sliver'/'LGN', '39738').

card_in_set('caller of the claw', 'LGN').
card_original_type('caller of the claw'/'LGN', 'Creature — Elf').
card_original_text('caller of the claw'/'LGN', 'You may play Caller of the Claw any time you could play an instant.\nWhen Caller of the Claw comes into play, put a 2/2 green Bear creature token into play for each nontoken creature put into your graveyard from play this turn.').
card_first_print('caller of the claw', 'LGN').
card_image_name('caller of the claw'/'LGN', 'caller of the claw').
card_uid('caller of the claw'/'LGN', 'LGN:Caller of the Claw:caller of the claw').
card_rarity('caller of the claw'/'LGN', 'Rare').
card_artist('caller of the claw'/'LGN', 'Matt Cavotta').
card_number('caller of the claw'/'LGN', '121').
card_multiverse_id('caller of the claw'/'LGN', '42187').

card_in_set('canopy crawler', 'LGN').
card_original_type('canopy crawler'/'LGN', 'Creature — Beast').
card_original_text('canopy crawler'/'LGN', 'Amplify 1 (As this card comes into play, put a +1/+1 counter on it for each Beast card you reveal in your hand.)\n{T}: Target creature gets +1/+1 until end of turn for each +1/+1 counter on Canopy Crawler.').
card_first_print('canopy crawler', 'LGN').
card_image_name('canopy crawler'/'LGN', 'canopy crawler').
card_uid('canopy crawler'/'LGN', 'LGN:Canopy Crawler:canopy crawler').
card_rarity('canopy crawler'/'LGN', 'Uncommon').
card_artist('canopy crawler'/'LGN', 'Anthony S. Waters').
card_number('canopy crawler'/'LGN', '122').
card_multiverse_id('canopy crawler'/'LGN', '45849').

card_in_set('celestial gatekeeper', 'LGN').
card_original_type('celestial gatekeeper'/'LGN', 'Creature — Bird Cleric').
card_original_text('celestial gatekeeper'/'LGN', 'Flying\nWhen Celestial Gatekeeper is put into a graveyard from play, remove it from the game, then return up to two target Bird and/or Cleric cards from your graveyard to play.').
card_first_print('celestial gatekeeper', 'LGN').
card_image_name('celestial gatekeeper'/'LGN', 'celestial gatekeeper').
card_uid('celestial gatekeeper'/'LGN', 'LGN:Celestial Gatekeeper:celestial gatekeeper').
card_rarity('celestial gatekeeper'/'LGN', 'Rare').
card_artist('celestial gatekeeper'/'LGN', 'Christopher Moeller').
card_number('celestial gatekeeper'/'LGN', '6').
card_multiverse_id('celestial gatekeeper'/'LGN', '42271').

card_in_set('cephalid pathmage', 'LGN').
card_original_type('cephalid pathmage'/'LGN', 'Creature — Cephalid Wizard').
card_original_text('cephalid pathmage'/'LGN', 'Cephalid Pathmage is unblockable.\n{T}, Sacrifice Cephalid Pathmage: Target creature is unblockable this turn.').
card_first_print('cephalid pathmage', 'LGN').
card_image_name('cephalid pathmage'/'LGN', 'cephalid pathmage').
card_uid('cephalid pathmage'/'LGN', 'LGN:Cephalid Pathmage:cephalid pathmage').
card_rarity('cephalid pathmage'/'LGN', 'Common').
card_artist('cephalid pathmage'/'LGN', 'Alex Horley-Orlandelli').
card_number('cephalid pathmage'/'LGN', '31').
card_flavor_text('cephalid pathmage'/'LGN', 'Pathmages can open doors that aren\'t even there.').
card_multiverse_id('cephalid pathmage'/'LGN', '29717').

card_in_set('chromeshell crab', 'LGN').
card_original_type('chromeshell crab'/'LGN', 'Creature — Beast').
card_original_text('chromeshell crab'/'LGN', 'Morph {4}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Chromeshell Crab is turned face up, you may exchange control of target creature you control and target creature an opponent controls.').
card_first_print('chromeshell crab', 'LGN').
card_image_name('chromeshell crab'/'LGN', 'chromeshell crab').
card_uid('chromeshell crab'/'LGN', 'LGN:Chromeshell Crab:chromeshell crab').
card_rarity('chromeshell crab'/'LGN', 'Rare').
card_artist('chromeshell crab'/'LGN', 'Ron Spencer').
card_number('chromeshell crab'/'LGN', '32').
card_multiverse_id('chromeshell crab'/'LGN', '44205').

card_in_set('clickslither', 'LGN').
card_original_type('clickslither'/'LGN', 'Creature — Insect').
card_original_text('clickslither'/'LGN', 'Haste\nSacrifice a Goblin: Clickslither gets +2/+2 and gains trample until end of turn.').
card_first_print('clickslither', 'LGN').
card_image_name('clickslither'/'LGN', 'clickslither').
card_uid('clickslither'/'LGN', 'LGN:Clickslither:clickslither').
card_rarity('clickslither'/'LGN', 'Rare').
card_artist('clickslither'/'LGN', 'Kev Walker').
card_number('clickslither'/'LGN', '90').
card_flavor_text('clickslither'/'LGN', 'The least popular goblins get the outer caves.').
card_multiverse_id('clickslither'/'LGN', '42277').

card_in_set('cloudreach cavalry', 'LGN').
card_original_type('cloudreach cavalry'/'LGN', 'Creature — Soldier').
card_original_text('cloudreach cavalry'/'LGN', 'Cloudreach Cavalry gets +2/+2 and has flying as long as you control a Bird.').
card_first_print('cloudreach cavalry', 'LGN').
card_image_name('cloudreach cavalry'/'LGN', 'cloudreach cavalry').
card_uid('cloudreach cavalry'/'LGN', 'LGN:Cloudreach Cavalry:cloudreach cavalry').
card_rarity('cloudreach cavalry'/'LGN', 'Uncommon').
card_artist('cloudreach cavalry'/'LGN', 'Kev Walker').
card_number('cloudreach cavalry'/'LGN', '7').
card_flavor_text('cloudreach cavalry'/'LGN', '\"It\'s easy to lose sight of hope until you gaze out from my vantage point.\"').
card_multiverse_id('cloudreach cavalry'/'LGN', '45839').

card_in_set('corpse harvester', 'LGN').
card_original_type('corpse harvester'/'LGN', 'Creature — Zombie Wizard').
card_original_text('corpse harvester'/'LGN', '{1}{B}, {T}, Sacrifice a creature: Search your library for a Zombie card and a swamp card, reveal them, and put them into your hand. Then shuffle your library.').
card_first_print('corpse harvester', 'LGN').
card_image_name('corpse harvester'/'LGN', 'corpse harvester').
card_uid('corpse harvester'/'LGN', 'LGN:Corpse Harvester:corpse harvester').
card_rarity('corpse harvester'/'LGN', 'Uncommon').
card_artist('corpse harvester'/'LGN', 'Mark Tedin').
card_number('corpse harvester'/'LGN', '62').
card_multiverse_id('corpse harvester'/'LGN', '44279').

card_in_set('covert operative', 'LGN').
card_original_type('covert operative'/'LGN', 'Creature — Wizard').
card_original_text('covert operative'/'LGN', 'Covert Operative is unblockable.').
card_first_print('covert operative', 'LGN').
card_image_name('covert operative'/'LGN', 'covert operative').
card_uid('covert operative'/'LGN', 'LGN:Covert Operative:covert operative').
card_rarity('covert operative'/'LGN', 'Common').
card_artist('covert operative'/'LGN', 'Kev Walker').
card_number('covert operative'/'LGN', '33').
card_flavor_text('covert operative'/'LGN', 'Some spies seek clarity. Others seek transparency.').
card_multiverse_id('covert operative'/'LGN', '30659').

card_in_set('crested craghorn', 'LGN').
card_original_type('crested craghorn'/'LGN', 'Creature — Beast').
card_original_text('crested craghorn'/'LGN', 'Haste\nProvoke (When this attacks, you may have target creature defending player controls untap and block it if able.)').
card_first_print('crested craghorn', 'LGN').
card_image_name('crested craghorn'/'LGN', 'crested craghorn').
card_uid('crested craghorn'/'LGN', 'LGN:Crested Craghorn:crested craghorn').
card_rarity('crested craghorn'/'LGN', 'Common').
card_artist('crested craghorn'/'LGN', 'Matt Cavotta').
card_number('crested craghorn'/'LGN', '91').
card_flavor_text('crested craghorn'/'LGN', '\"Craghorns experience a wide range of emotions: rage, fury, anger . . .\"\n—Foothill guide').
card_multiverse_id('crested craghorn'/'LGN', '45127').

card_in_set('crookclaw elder', 'LGN').
card_original_type('crookclaw elder'/'LGN', 'Creature — Bird Wizard').
card_original_text('crookclaw elder'/'LGN', 'Flying\nTap two untapped Birds you control: Draw a card.\nTap two untapped Wizards you control: Target creature gains flying until end of turn.').
card_first_print('crookclaw elder', 'LGN').
card_image_name('crookclaw elder'/'LGN', 'crookclaw elder').
card_uid('crookclaw elder'/'LGN', 'LGN:Crookclaw Elder:crookclaw elder').
card_rarity('crookclaw elder'/'LGN', 'Uncommon').
card_artist('crookclaw elder'/'LGN', 'Ron Spencer').
card_number('crookclaw elder'/'LGN', '34').
card_multiverse_id('crookclaw elder'/'LGN', '42172').

card_in_set('crypt sliver', 'LGN').
card_original_type('crypt sliver'/'LGN', 'Creature — Sliver').
card_original_text('crypt sliver'/'LGN', 'All Slivers have \"{T}: Regenerate target Sliver.\"').
card_first_print('crypt sliver', 'LGN').
card_image_name('crypt sliver'/'LGN', 'crypt sliver').
card_uid('crypt sliver'/'LGN', 'LGN:Crypt Sliver:crypt sliver').
card_rarity('crypt sliver'/'LGN', 'Common').
card_artist('crypt sliver'/'LGN', 'Edward P. Beard, Jr.').
card_number('crypt sliver'/'LGN', '63').
card_flavor_text('crypt sliver'/'LGN', '\"Death couldn\'t contain the slivers. What made us think we could?\"\n—Riptide Project researcher').
card_multiverse_id('crypt sliver'/'LGN', '42033').

card_in_set('dark supplicant', 'LGN').
card_original_type('dark supplicant'/'LGN', 'Creature — Cleric').
card_original_text('dark supplicant'/'LGN', '{T}, Sacrifice three Clerics: Search your graveyard, hand, and/or library for a card named Scion of Darkness and put it into play. If you search your library this way, shuffle it.').
card_first_print('dark supplicant', 'LGN').
card_image_name('dark supplicant'/'LGN', 'dark supplicant').
card_uid('dark supplicant'/'LGN', 'LGN:Dark Supplicant:dark supplicant').
card_rarity('dark supplicant'/'LGN', 'Uncommon').
card_artist('dark supplicant'/'LGN', 'Mark Zug').
card_number('dark supplicant'/'LGN', '64').
card_flavor_text('dark supplicant'/'LGN', 'Cabal clerics serve two masters: their patriarch and their god.').
card_multiverse_id('dark supplicant'/'LGN', '42077').

card_in_set('daru mender', 'LGN').
card_original_type('daru mender'/'LGN', 'Creature — Cleric').
card_original_text('daru mender'/'LGN', 'Morph {W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Daru Mender is turned face up, regenerate target creature.').
card_first_print('daru mender', 'LGN').
card_image_name('daru mender'/'LGN', 'daru mender').
card_uid('daru mender'/'LGN', 'LGN:Daru Mender:daru mender').
card_rarity('daru mender'/'LGN', 'Uncommon').
card_artist('daru mender'/'LGN', 'Ben Thompson').
card_number('daru mender'/'LGN', '8').
card_multiverse_id('daru mender'/'LGN', '42053').

card_in_set('daru sanctifier', 'LGN').
card_original_type('daru sanctifier'/'LGN', 'Creature — Cleric').
card_original_text('daru sanctifier'/'LGN', 'Morph {1}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Daru Sanctifier is turned face up, destroy target enchantment.').
card_first_print('daru sanctifier', 'LGN').
card_image_name('daru sanctifier'/'LGN', 'daru sanctifier').
card_uid('daru sanctifier'/'LGN', 'LGN:Daru Sanctifier:daru sanctifier').
card_rarity('daru sanctifier'/'LGN', 'Common').
card_artist('daru sanctifier'/'LGN', 'Tony Szczudlo').
card_number('daru sanctifier'/'LGN', '9').
card_multiverse_id('daru sanctifier'/'LGN', '44504').

card_in_set('daru stinger', 'LGN').
card_original_type('daru stinger'/'LGN', 'Creature — Soldier').
card_original_text('daru stinger'/'LGN', 'Amplify 1 (As this card comes into play, put a +1/+1 counter on it for each Soldier card you reveal in your hand.)\n{T}: Daru Stinger deals damage equal to the number of +1/+1 counters on it to target attacking or blocking creature.').
card_first_print('daru stinger', 'LGN').
card_image_name('daru stinger'/'LGN', 'daru stinger').
card_uid('daru stinger'/'LGN', 'LGN:Daru Stinger:daru stinger').
card_rarity('daru stinger'/'LGN', 'Common').
card_artist('daru stinger'/'LGN', 'Greg Staples').
card_number('daru stinger'/'LGN', '10').
card_multiverse_id('daru stinger'/'LGN', '42278').

card_in_set('deathmark prelate', 'LGN').
card_original_type('deathmark prelate'/'LGN', 'Creature — Cleric').
card_original_text('deathmark prelate'/'LGN', '{2}{B}, {T}, Sacrifice a Zombie: Destroy target non-Zombie creature. It can\'t be regenerated. Play this ability only any time you could play a sorcery.').
card_first_print('deathmark prelate', 'LGN').
card_image_name('deathmark prelate'/'LGN', 'deathmark prelate').
card_uid('deathmark prelate'/'LGN', 'LGN:Deathmark Prelate:deathmark prelate').
card_rarity('deathmark prelate'/'LGN', 'Uncommon').
card_artist('deathmark prelate'/'LGN', 'Tony Szczudlo').
card_number('deathmark prelate'/'LGN', '65').
card_flavor_text('deathmark prelate'/'LGN', 'Death is a secret he is willing to share.').
card_multiverse_id('deathmark prelate'/'LGN', '35249').

card_in_set('defender of the order', 'LGN').
card_original_type('defender of the order'/'LGN', 'Creature — Cleric').
card_original_text('defender of the order'/'LGN', 'Morph {W}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Defender of the Order is turned face up, creatures you control get +0/+2 until end of turn.').
card_first_print('defender of the order', 'LGN').
card_image_name('defender of the order'/'LGN', 'defender of the order').
card_uid('defender of the order'/'LGN', 'LGN:Defender of the Order:defender of the order').
card_rarity('defender of the order'/'LGN', 'Rare').
card_artist('defender of the order'/'LGN', 'Darrell Riche').
card_number('defender of the order'/'LGN', '11').
card_multiverse_id('defender of the order'/'LGN', '44329').

card_in_set('defiant elf', 'LGN').
card_original_type('defiant elf'/'LGN', 'Creature — Elf').
card_original_text('defiant elf'/'LGN', 'Trample').
card_first_print('defiant elf', 'LGN').
card_image_name('defiant elf'/'LGN', 'defiant elf').
card_uid('defiant elf'/'LGN', 'LGN:Defiant Elf:defiant elf').
card_rarity('defiant elf'/'LGN', 'Common').
card_artist('defiant elf'/'LGN', 'Pete Venters').
card_number('defiant elf'/'LGN', '123').
card_flavor_text('defiant elf'/'LGN', '\"I lost one home when Yavimaya was destroyed. I will not lose another.\"').
card_multiverse_id('defiant elf'/'LGN', '39707').

card_in_set('deftblade elite', 'LGN').
card_original_type('deftblade elite'/'LGN', 'Creature — Soldier').
card_original_text('deftblade elite'/'LGN', 'Provoke (When this attacks, you may have target creature defending player controls untap and block it if able.)\n{1}{W}: Prevent all combat damage that would be dealt to and dealt by Deftblade Elite this turn.').
card_first_print('deftblade elite', 'LGN').
card_image_name('deftblade elite'/'LGN', 'deftblade elite').
card_uid('deftblade elite'/'LGN', 'LGN:Deftblade Elite:deftblade elite').
card_rarity('deftblade elite'/'LGN', 'Common').
card_artist('deftblade elite'/'LGN', 'Alan Pollack').
card_number('deftblade elite'/'LGN', '12').
card_multiverse_id('deftblade elite'/'LGN', '45132').

card_in_set('dermoplasm', 'LGN').
card_original_type('dermoplasm'/'LGN', 'Creature — Shapeshifter').
card_original_text('dermoplasm'/'LGN', 'Flying\nMorph {2}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Dermoplasm is turned face up, you may put a creature card with morph from your hand into play face up. If you do, return Dermoplasm to its owner\'s hand.').
card_first_print('dermoplasm', 'LGN').
card_image_name('dermoplasm'/'LGN', 'dermoplasm').
card_uid('dermoplasm'/'LGN', 'LGN:Dermoplasm:dermoplasm').
card_rarity('dermoplasm'/'LGN', 'Rare').
card_artist('dermoplasm'/'LGN', 'John Avon').
card_number('dermoplasm'/'LGN', '35').
card_multiverse_id('dermoplasm'/'LGN', '44300').

card_in_set('dreamborn muse', 'LGN').
card_original_type('dreamborn muse'/'LGN', 'Creature — Spirit').
card_original_text('dreamborn muse'/'LGN', 'At the beginning of each player\'s upkeep, that player puts the top X cards from his or her library into his or her graveyard, where X is the number of cards in his or her hand.').
card_first_print('dreamborn muse', 'LGN').
card_image_name('dreamborn muse'/'LGN', 'dreamborn muse').
card_uid('dreamborn muse'/'LGN', 'LGN:Dreamborn Muse:dreamborn muse').
card_rarity('dreamborn muse'/'LGN', 'Rare').
card_artist('dreamborn muse'/'LGN', 'Kev Walker').
card_number('dreamborn muse'/'LGN', '36').
card_flavor_text('dreamborn muse'/'LGN', '\"Her voice is insight, piercing and true.\"\n—Ixidor, reality sculptor').
card_multiverse_id('dreamborn muse'/'LGN', '44840').

card_in_set('drinker of sorrow', 'LGN').
card_original_type('drinker of sorrow'/'LGN', 'Creature — Horror').
card_original_text('drinker of sorrow'/'LGN', 'Drinker of Sorrow can\'t block.\nWhenever Drinker of Sorrow deals combat damage, sacrifice a permanent.').
card_first_print('drinker of sorrow', 'LGN').
card_image_name('drinker of sorrow'/'LGN', 'drinker of sorrow').
card_uid('drinker of sorrow'/'LGN', 'LGN:Drinker of Sorrow:drinker of sorrow').
card_rarity('drinker of sorrow'/'LGN', 'Rare').
card_artist('drinker of sorrow'/'LGN', 'Carl Critchlow').
card_number('drinker of sorrow'/'LGN', '66').
card_flavor_text('drinker of sorrow'/'LGN', 'It strikes at your soul, heedless of your prayers.').
card_multiverse_id('drinker of sorrow'/'LGN', '44492').

card_in_set('dripping dead', 'LGN').
card_original_type('dripping dead'/'LGN', 'Creature — Zombie').
card_original_text('dripping dead'/'LGN', 'Dripping Dead can\'t block.\nWhenever Dripping Dead deals combat damage to a creature, destroy that creature. It can\'t be regenerated.').
card_first_print('dripping dead', 'LGN').
card_image_name('dripping dead'/'LGN', 'dripping dead').
card_uid('dripping dead'/'LGN', 'LGN:Dripping Dead:dripping dead').
card_rarity('dripping dead'/'LGN', 'Common').
card_artist('dripping dead'/'LGN', 'Thomas M. Baxa').
card_number('dripping dead'/'LGN', '67').
card_flavor_text('dripping dead'/'LGN', 'It oozes death from every pore.').
card_multiverse_id('dripping dead'/'LGN', '44316').

card_in_set('earthblighter', 'LGN').
card_original_type('earthblighter'/'LGN', 'Creature — Cleric').
card_original_text('earthblighter'/'LGN', '{2}{B}, {T}, Sacrifice a Goblin: Destroy target land.').
card_first_print('earthblighter', 'LGN').
card_image_name('earthblighter'/'LGN', 'earthblighter').
card_uid('earthblighter'/'LGN', 'LGN:Earthblighter:earthblighter').
card_rarity('earthblighter'/'LGN', 'Uncommon').
card_artist('earthblighter'/'LGN', 'Alex Horley-Orlandelli').
card_number('earthblighter'/'LGN', '68').
card_flavor_text('earthblighter'/'LGN', '\"A single dedicated mind can bring about the greatest destruction. That, or goblins—goblins work too.\"').
card_multiverse_id('earthblighter'/'LGN', '42171').

card_in_set('echo tracer', 'LGN').
card_original_type('echo tracer'/'LGN', 'Creature — Wizard').
card_original_text('echo tracer'/'LGN', 'Morph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Echo Tracer is turned face up, return target creature to its owner\'s hand.').
card_first_print('echo tracer', 'LGN').
card_image_name('echo tracer'/'LGN', 'echo tracer').
card_uid('echo tracer'/'LGN', 'LGN:Echo Tracer:echo tracer').
card_rarity('echo tracer'/'LGN', 'Common').
card_artist('echo tracer'/'LGN', 'Scott M. Fischer').
card_number('echo tracer'/'LGN', '37').
card_multiverse_id('echo tracer'/'LGN', '42059').

card_in_set('elvish soultiller', 'LGN').
card_original_type('elvish soultiller'/'LGN', 'Creature — Elf Mutant').
card_original_text('elvish soultiller'/'LGN', 'When Elvish Soultiller is put into a graveyard from play, choose a creature type. Shuffle all creature cards of that type from your graveyard into your library.').
card_first_print('elvish soultiller', 'LGN').
card_image_name('elvish soultiller'/'LGN', 'elvish soultiller').
card_uid('elvish soultiller'/'LGN', 'LGN:Elvish Soultiller:elvish soultiller').
card_rarity('elvish soultiller'/'LGN', 'Rare').
card_artist('elvish soultiller'/'LGN', 'Ron Spears').
card_number('elvish soultiller'/'LGN', '124').
card_flavor_text('elvish soultiller'/'LGN', 'Mutated elves wondered if this was their final form, or if it was just another step.').
card_multiverse_id('elvish soultiller'/'LGN', '42083').

card_in_set('embalmed brawler', 'LGN').
card_original_type('embalmed brawler'/'LGN', 'Creature — Zombie').
card_original_text('embalmed brawler'/'LGN', 'Amplify 1 (As this card comes into play, put a +1/+1 counter on it for each Zombie card you reveal in your hand.)\nWhenever Embalmed Brawler attacks or blocks, you lose 1 life for each +1/+1 counter on it.').
card_first_print('embalmed brawler', 'LGN').
card_image_name('embalmed brawler'/'LGN', 'embalmed brawler').
card_uid('embalmed brawler'/'LGN', 'LGN:Embalmed Brawler:embalmed brawler').
card_rarity('embalmed brawler'/'LGN', 'Common').
card_artist('embalmed brawler'/'LGN', 'Justin Sweet').
card_number('embalmed brawler'/'LGN', '69').
card_multiverse_id('embalmed brawler'/'LGN', '43724').

card_in_set('enormous baloth', 'LGN').
card_original_type('enormous baloth'/'LGN', 'Creature — Beast').
card_original_text('enormous baloth'/'LGN', '').
card_first_print('enormous baloth', 'LGN').
card_image_name('enormous baloth'/'LGN', 'enormous baloth').
card_uid('enormous baloth'/'LGN', 'LGN:Enormous Baloth:enormous baloth').
card_rarity('enormous baloth'/'LGN', 'Uncommon').
card_artist('enormous baloth'/'LGN', 'Mark Tedin').
card_number('enormous baloth'/'LGN', '125').
card_flavor_text('enormous baloth'/'LGN', 'Its diet consists of fruits, plants, small woodland animals, large woodland animals, woodlands, fruit groves, fruit farmers, and small cities.').
card_multiverse_id('enormous baloth'/'LGN', '44500').

card_in_set('essence sliver', 'LGN').
card_original_type('essence sliver'/'LGN', 'Creature — Sliver').
card_original_text('essence sliver'/'LGN', 'Whenever a Sliver deals damage, its controller gains that much life.').
card_first_print('essence sliver', 'LGN').
card_image_name('essence sliver'/'LGN', 'essence sliver').
card_uid('essence sliver'/'LGN', 'LGN:Essence Sliver:essence sliver').
card_rarity('essence sliver'/'LGN', 'Rare').
card_artist('essence sliver'/'LGN', 'Glen Angus').
card_number('essence sliver'/'LGN', '13').
card_flavor_text('essence sliver'/'LGN', 'The slivers would survive, even at the expense of every other creature on Otaria.').
card_multiverse_id('essence sliver'/'LGN', '42025').

card_in_set('feral throwback', 'LGN').
card_original_type('feral throwback'/'LGN', 'Creature — Beast').
card_original_text('feral throwback'/'LGN', 'Amplify 2 (As this card comes into play, put two +1/+1 counters on it for each Beast card you reveal in your hand.)\nProvoke (When this attacks, you may have target creature defending player controls untap and block it if able.)').
card_image_name('feral throwback'/'LGN', 'feral throwback').
card_uid('feral throwback'/'LGN', 'LGN:Feral Throwback:feral throwback').
card_rarity('feral throwback'/'LGN', 'Rare').
card_artist('feral throwback'/'LGN', 'Carl Critchlow').
card_number('feral throwback'/'LGN', '126').
card_multiverse_id('feral throwback'/'LGN', '34263').

card_in_set('flamewave invoker', 'LGN').
card_original_type('flamewave invoker'/'LGN', 'Creature — Goblin Mutant').
card_original_text('flamewave invoker'/'LGN', '{7}{R}: Flamewave Invoker deals 5 damage to target player.').
card_first_print('flamewave invoker', 'LGN').
card_image_name('flamewave invoker'/'LGN', 'flamewave invoker').
card_uid('flamewave invoker'/'LGN', 'LGN:Flamewave Invoker:flamewave invoker').
card_rarity('flamewave invoker'/'LGN', 'Common').
card_artist('flamewave invoker'/'LGN', 'Dave Dorman').
card_number('flamewave invoker'/'LGN', '92').
card_flavor_text('flamewave invoker'/'LGN', 'The Mirari burns in his heart.').
card_multiverse_id('flamewave invoker'/'LGN', '43523').

card_in_set('frenetic raptor', 'LGN').
card_original_type('frenetic raptor'/'LGN', 'Creature — Beast').
card_original_text('frenetic raptor'/'LGN', 'Beasts can\'t block.').
card_first_print('frenetic raptor', 'LGN').
card_image_name('frenetic raptor'/'LGN', 'frenetic raptor').
card_uid('frenetic raptor'/'LGN', 'LGN:Frenetic Raptor:frenetic raptor').
card_rarity('frenetic raptor'/'LGN', 'Uncommon').
card_artist('frenetic raptor'/'LGN', 'Daren Bader').
card_number('frenetic raptor'/'LGN', '93').
card_flavor_text('frenetic raptor'/'LGN', '\"How do you stop a raptor from charging? No, seriously Help\"\n—Blarg, goblin jester').
card_multiverse_id('frenetic raptor'/'LGN', '42028').

card_in_set('fugitive wizard', 'LGN').
card_original_type('fugitive wizard'/'LGN', 'Creature — Wizard').
card_original_text('fugitive wizard'/'LGN', '').
card_first_print('fugitive wizard', 'LGN').
card_image_name('fugitive wizard'/'LGN', 'fugitive wizard').
card_uid('fugitive wizard'/'LGN', 'LGN:Fugitive Wizard:fugitive wizard').
card_rarity('fugitive wizard'/'LGN', 'Common').
card_artist('fugitive wizard'/'LGN', 'Jim Nelson').
card_number('fugitive wizard'/'LGN', '38').
card_flavor_text('fugitive wizard'/'LGN', 'Many wizards came to the Riptide Project hoping to return home with answers. Most, however, wouldn\'t return home at all.').
card_multiverse_id('fugitive wizard'/'LGN', '42160').

card_in_set('gempalm avenger', 'LGN').
card_original_type('gempalm avenger'/'LGN', 'Creature — Soldier').
card_original_text('gempalm avenger'/'LGN', 'Cycling {2}{W} ({2}{W}, Discard this card from your hand: Draw a card.)\nWhen you cycle Gempalm Avenger, all Soldiers get +1/+1 and gain first strike until end of turn.').
card_first_print('gempalm avenger', 'LGN').
card_image_name('gempalm avenger'/'LGN', 'gempalm avenger').
card_uid('gempalm avenger'/'LGN', 'LGN:Gempalm Avenger:gempalm avenger').
card_rarity('gempalm avenger'/'LGN', 'Common').
card_artist('gempalm avenger'/'LGN', 'Tim Hildebrandt').
card_number('gempalm avenger'/'LGN', '14').
card_multiverse_id('gempalm avenger'/'LGN', '43791').

card_in_set('gempalm incinerator', 'LGN').
card_original_type('gempalm incinerator'/'LGN', 'Creature — Goblin').
card_original_text('gempalm incinerator'/'LGN', 'Cycling {1}{R} ({1}{R}, Discard this card from your hand: Draw a card.)\nWhen you cycle Gempalm Incinerator, you may have it deal X damage to target creature, where X is the number of Goblins in play.').
card_first_print('gempalm incinerator', 'LGN').
card_image_name('gempalm incinerator'/'LGN', 'gempalm incinerator').
card_uid('gempalm incinerator'/'LGN', 'LGN:Gempalm Incinerator:gempalm incinerator').
card_rarity('gempalm incinerator'/'LGN', 'Uncommon').
card_artist('gempalm incinerator'/'LGN', 'Luca Zontini').
card_number('gempalm incinerator'/'LGN', '94').
card_multiverse_id('gempalm incinerator'/'LGN', '42066').

card_in_set('gempalm polluter', 'LGN').
card_original_type('gempalm polluter'/'LGN', 'Creature — Zombie').
card_original_text('gempalm polluter'/'LGN', 'Cycling {B}{B} ({B}{B}, Discard this card from your hand: Draw a card.)\nWhen you cycle Gempalm Polluter, you may have target player lose 1 life for each Zombie in play.').
card_first_print('gempalm polluter', 'LGN').
card_image_name('gempalm polluter'/'LGN', 'gempalm polluter').
card_uid('gempalm polluter'/'LGN', 'LGN:Gempalm Polluter:gempalm polluter').
card_rarity('gempalm polluter'/'LGN', 'Common').
card_artist('gempalm polluter'/'LGN', 'Dany Orizio').
card_number('gempalm polluter'/'LGN', '70').
card_multiverse_id('gempalm polluter'/'LGN', '42065').

card_in_set('gempalm sorcerer', 'LGN').
card_original_type('gempalm sorcerer'/'LGN', 'Creature — Wizard').
card_original_text('gempalm sorcerer'/'LGN', 'Cycling {2}{U} ({2}{U}, Discard this card from your hand: Draw a card.)\nWhen you cycle Gempalm Sorcerer, all Wizards gain flying until end of turn.').
card_first_print('gempalm sorcerer', 'LGN').
card_image_name('gempalm sorcerer'/'LGN', 'gempalm sorcerer').
card_uid('gempalm sorcerer'/'LGN', 'LGN:Gempalm Sorcerer:gempalm sorcerer').
card_rarity('gempalm sorcerer'/'LGN', 'Uncommon').
card_artist('gempalm sorcerer'/'LGN', 'Greg Hildebrandt').
card_number('gempalm sorcerer'/'LGN', '39').
card_multiverse_id('gempalm sorcerer'/'LGN', '42064').

card_in_set('gempalm strider', 'LGN').
card_original_type('gempalm strider'/'LGN', 'Creature — Elf').
card_original_text('gempalm strider'/'LGN', 'Cycling {2}{G}{G} ({2}{G}{G}, Discard this card from your hand: Draw a card.)\nWhen you cycle Gempalm Strider, all Elves get +2/+2 until end of turn.').
card_first_print('gempalm strider', 'LGN').
card_image_name('gempalm strider'/'LGN', 'gempalm strider').
card_uid('gempalm strider'/'LGN', 'LGN:Gempalm Strider:gempalm strider').
card_rarity('gempalm strider'/'LGN', 'Uncommon').
card_artist('gempalm strider'/'LGN', 'Tim Hildebrandt').
card_number('gempalm strider'/'LGN', '127').
card_multiverse_id('gempalm strider'/'LGN', '42067').

card_in_set('ghastly remains', 'LGN').
card_original_type('ghastly remains'/'LGN', 'Creature — Zombie').
card_original_text('ghastly remains'/'LGN', 'Amplify 1 (As this card comes into play, put a +1/+1 counter on it for each Zombie card you reveal in your hand.)\nAt the beginning of your upkeep, if Ghastly Remains is in your graveyard, you may pay {B}{B}{B}. If you do, return Ghastly Remains to your hand.').
card_first_print('ghastly remains', 'LGN').
card_image_name('ghastly remains'/'LGN', 'ghastly remains').
card_uid('ghastly remains'/'LGN', 'LGN:Ghastly Remains:ghastly remains').
card_rarity('ghastly remains'/'LGN', 'Rare').
card_artist('ghastly remains'/'LGN', 'Edward P. Beard, Jr.').
card_number('ghastly remains'/'LGN', '71').
card_multiverse_id('ghastly remains'/'LGN', '42178').

card_in_set('glintwing invoker', 'LGN').
card_original_type('glintwing invoker'/'LGN', 'Creature — Wizard Mutant').
card_original_text('glintwing invoker'/'LGN', '{7}{U}: Glintwing Invoker gets +3/+3 and gains flying until end of turn.').
card_first_print('glintwing invoker', 'LGN').
card_image_name('glintwing invoker'/'LGN', 'glintwing invoker').
card_uid('glintwing invoker'/'LGN', 'LGN:Glintwing Invoker:glintwing invoker').
card_rarity('glintwing invoker'/'LGN', 'Common').
card_artist('glintwing invoker'/'LGN', 'Jim Nelson').
card_number('glintwing invoker'/'LGN', '40').
card_flavor_text('glintwing invoker'/'LGN', 'The Mirari flares in his mind.').
card_multiverse_id('glintwing invoker'/'LGN', '44204').

card_in_set('glowering rogon', 'LGN').
card_original_type('glowering rogon'/'LGN', 'Creature — Beast').
card_original_text('glowering rogon'/'LGN', 'Amplify 1 (As this card comes into play, put a +1/+1 counter on it for each Beast card you reveal in your hand.)').
card_first_print('glowering rogon', 'LGN').
card_image_name('glowering rogon'/'LGN', 'glowering rogon').
card_uid('glowering rogon'/'LGN', 'LGN:Glowering Rogon:glowering rogon').
card_rarity('glowering rogon'/'LGN', 'Common').
card_artist('glowering rogon'/'LGN', 'Kev Walker').
card_number('glowering rogon'/'LGN', '128').
card_flavor_text('glowering rogon'/'LGN', 'A herd of one.').
card_multiverse_id('glowering rogon'/'LGN', '43723').

card_in_set('glowrider', 'LGN').
card_original_type('glowrider'/'LGN', 'Creature — Cleric').
card_original_text('glowrider'/'LGN', 'Noncreature spells cost {1} more to play.').
card_first_print('glowrider', 'LGN').
card_image_name('glowrider'/'LGN', 'glowrider').
card_uid('glowrider'/'LGN', 'LGN:Glowrider:glowrider').
card_rarity('glowrider'/'LGN', 'Rare').
card_artist('glowrider'/'LGN', 'Scott M. Fischer').
card_number('glowrider'/'LGN', '15').
card_flavor_text('glowrider'/'LGN', '\"It is not yet time.\"').
card_multiverse_id('glowrider'/'LGN', '42073').

card_in_set('goblin assassin', 'LGN').
card_original_type('goblin assassin'/'LGN', 'Creature — Goblin Assassin').
card_original_text('goblin assassin'/'LGN', 'Whenever Goblin Assassin or another Goblin comes into play, each player flips a coin. Each player whose coin comes up tails sacrifices a creature.').
card_first_print('goblin assassin', 'LGN').
card_image_name('goblin assassin'/'LGN', 'goblin assassin').
card_uid('goblin assassin'/'LGN', 'LGN:Goblin Assassin:goblin assassin').
card_rarity('goblin assassin'/'LGN', 'Uncommon').
card_artist('goblin assassin'/'LGN', 'Dave Dorman').
card_number('goblin assassin'/'LGN', '95').
card_flavor_text('goblin assassin'/'LGN', 'The more victims he kills, the more likely he is to get the right one.').
card_multiverse_id('goblin assassin'/'LGN', '44656').

card_in_set('goblin clearcutter', 'LGN').
card_original_type('goblin clearcutter'/'LGN', 'Creature — Goblin').
card_original_text('goblin clearcutter'/'LGN', '{T}, Sacrifice a forest: Add three mana in any combination of red and/or green to your mana pool.').
card_first_print('goblin clearcutter', 'LGN').
card_image_name('goblin clearcutter'/'LGN', 'goblin clearcutter').
card_uid('goblin clearcutter'/'LGN', 'LGN:Goblin Clearcutter:goblin clearcutter').
card_rarity('goblin clearcutter'/'LGN', 'Uncommon').
card_artist('goblin clearcutter'/'LGN', 'Eric Peterson').
card_number('goblin clearcutter'/'LGN', '96').
card_flavor_text('goblin clearcutter'/'LGN', '\"Did you know that wood burns even better than rocks?\"\n—Toggo, goblin weaponsmith').
card_multiverse_id('goblin clearcutter'/'LGN', '44317').

card_in_set('goblin dynamo', 'LGN').
card_original_type('goblin dynamo'/'LGN', 'Creature — Goblin Mutant').
card_original_text('goblin dynamo'/'LGN', '{T}: Goblin Dynamo deals 1 damage to target creature or player.\n{X}{R}, {T}, Sacrifice Goblin Dynamo: Goblin Dynamo deals X damage to target creature or player.').
card_first_print('goblin dynamo', 'LGN').
card_image_name('goblin dynamo'/'LGN', 'goblin dynamo').
card_uid('goblin dynamo'/'LGN', 'LGN:Goblin Dynamo:goblin dynamo').
card_rarity('goblin dynamo'/'LGN', 'Uncommon').
card_artist('goblin dynamo'/'LGN', 'Ron Spencer').
card_number('goblin dynamo'/'LGN', '97').
card_multiverse_id('goblin dynamo'/'LGN', '42274').

card_in_set('goblin firebug', 'LGN').
card_original_type('goblin firebug'/'LGN', 'Creature — Goblin').
card_original_text('goblin firebug'/'LGN', 'When Goblin Firebug leaves play, sacrifice a land.').
card_first_print('goblin firebug', 'LGN').
card_image_name('goblin firebug'/'LGN', 'goblin firebug').
card_uid('goblin firebug'/'LGN', 'LGN:Goblin Firebug:goblin firebug').
card_rarity('goblin firebug'/'LGN', 'Common').
card_artist('goblin firebug'/'LGN', 'Christopher Moeller').
card_number('goblin firebug'/'LGN', '98').
card_flavor_text('goblin firebug'/'LGN', 'Most goblins leave behind a wake of destruction. A few goblins take one with them.').
card_multiverse_id('goblin firebug'/'LGN', '44286').

card_in_set('goblin goon', 'LGN').
card_original_type('goblin goon'/'LGN', 'Creature — Goblin Mutant').
card_original_text('goblin goon'/'LGN', 'Goblin Goon can\'t attack unless you control more creatures than defending player.\nGoblin Goon can\'t block unless you control more creatures than attacking player.').
card_first_print('goblin goon', 'LGN').
card_image_name('goblin goon'/'LGN', 'goblin goon').
card_uid('goblin goon'/'LGN', 'LGN:Goblin Goon:goblin goon').
card_rarity('goblin goon'/'LGN', 'Rare').
card_artist('goblin goon'/'LGN', 'Greg Staples').
card_number('goblin goon'/'LGN', '99').
card_flavor_text('goblin goon'/'LGN', 'Giant-sized body. Goblin-sized brain.').
card_multiverse_id('goblin goon'/'LGN', '39888').

card_in_set('goblin grappler', 'LGN').
card_original_type('goblin grappler'/'LGN', 'Creature — Goblin').
card_original_text('goblin grappler'/'LGN', 'Provoke (When this attacks, you may have target creature defending player controls untap and block it if able.)').
card_first_print('goblin grappler', 'LGN').
card_image_name('goblin grappler'/'LGN', 'goblin grappler').
card_uid('goblin grappler'/'LGN', 'LGN:Goblin Grappler:goblin grappler').
card_rarity('goblin grappler'/'LGN', 'Common').
card_artist('goblin grappler'/'LGN', 'Christopher Moeller').
card_number('goblin grappler'/'LGN', '100').
card_flavor_text('goblin grappler'/'LGN', 'Daru soldiers learned it\'s better to have a clean death from a sharp blade than to tangle with a goblin\'s rusted chains.').
card_multiverse_id('goblin grappler'/'LGN', '45125').

card_in_set('goblin lookout', 'LGN').
card_original_type('goblin lookout'/'LGN', 'Creature — Goblin').
card_original_text('goblin lookout'/'LGN', '{T}, Sacrifice a Goblin: All Goblins get +2/+0 until end of turn.').
card_first_print('goblin lookout', 'LGN').
card_image_name('goblin lookout'/'LGN', 'goblin lookout').
card_uid('goblin lookout'/'LGN', 'LGN:Goblin Lookout:goblin lookout').
card_rarity('goblin lookout'/'LGN', 'Common').
card_artist('goblin lookout'/'LGN', 'Jim Nelson').
card_number('goblin lookout'/'LGN', '101').
card_flavor_text('goblin lookout'/'LGN', '\"Throw rocks at \'em! Throw spears at \'em! Throw Furt at \'em!\"').
card_multiverse_id('goblin lookout'/'LGN', '45123').

card_in_set('goblin turncoat', 'LGN').
card_original_type('goblin turncoat'/'LGN', 'Creature — Goblin Mercenary').
card_original_text('goblin turncoat'/'LGN', 'Sacrifice a Goblin: Regenerate Goblin Turncoat.').
card_first_print('goblin turncoat', 'LGN').
card_image_name('goblin turncoat'/'LGN', 'goblin turncoat').
card_uid('goblin turncoat'/'LGN', 'LGN:Goblin Turncoat:goblin turncoat').
card_rarity('goblin turncoat'/'LGN', 'Common').
card_artist('goblin turncoat'/'LGN', 'Jim Nelson').
card_number('goblin turncoat'/'LGN', '72').
card_flavor_text('goblin turncoat'/'LGN', '\"Goblins won\'t betray their own kind for any price. They\'ll do it for a very specific price.\"\n—Phage the Untouchable').
card_multiverse_id('goblin turncoat'/'LGN', '44294').

card_in_set('graveborn muse', 'LGN').
card_original_type('graveborn muse'/'LGN', 'Creature — Zombie Spirit').
card_original_text('graveborn muse'/'LGN', 'At the beginning of your upkeep, you draw X cards and you lose X life, where X is the number of Zombies you control.').
card_first_print('graveborn muse', 'LGN').
card_image_name('graveborn muse'/'LGN', 'graveborn muse').
card_uid('graveborn muse'/'LGN', 'LGN:Graveborn Muse:graveborn muse').
card_rarity('graveborn muse'/'LGN', 'Rare').
card_artist('graveborn muse'/'LGN', 'Kev Walker').
card_number('graveborn muse'/'LGN', '73').
card_flavor_text('graveborn muse'/'LGN', '\"Her voice is damnation, unyielding and certain.\"\n—Phage the Untouchable').
card_multiverse_id('graveborn muse'/'LGN', '44841').

card_in_set('havoc demon', 'LGN').
card_original_type('havoc demon'/'LGN', 'Creature — Demon').
card_original_text('havoc demon'/'LGN', 'Flying\nWhen Havoc Demon is put into a graveyard from play, all creatures get -5/-5 until end of turn.').
card_first_print('havoc demon', 'LGN').
card_image_name('havoc demon'/'LGN', 'havoc demon').
card_uid('havoc demon'/'LGN', 'LGN:Havoc Demon:havoc demon').
card_rarity('havoc demon'/'LGN', 'Rare').
card_artist('havoc demon'/'LGN', 'Thomas M. Baxa').
card_number('havoc demon'/'LGN', '74').
card_flavor_text('havoc demon'/'LGN', 'A cry of pain as it enters this world. A chorus of screams as it leaves.').
card_multiverse_id('havoc demon'/'LGN', '44299').

card_in_set('hollow specter', 'LGN').
card_original_type('hollow specter'/'LGN', 'Creature — Specter').
card_original_text('hollow specter'/'LGN', 'Flying\nWhenever Hollow Specter deals combat damage to a player, you may pay {X}. If you do, that player reveals X cards from his or her hand and you choose one of them. That player discards that card.').
card_first_print('hollow specter', 'LGN').
card_image_name('hollow specter'/'LGN', 'hollow specter').
card_uid('hollow specter'/'LGN', 'LGN:Hollow Specter:hollow specter').
card_rarity('hollow specter'/'LGN', 'Rare').
card_artist('hollow specter'/'LGN', 'rk post').
card_number('hollow specter'/'LGN', '75').
card_multiverse_id('hollow specter'/'LGN', '8829').

card_in_set('hundroog', 'LGN').
card_original_type('hundroog'/'LGN', 'Creature — Beast').
card_original_text('hundroog'/'LGN', 'Cycling {3} ({3}, Discard this card from your hand: Draw a card.)').
card_first_print('hundroog', 'LGN').
card_image_name('hundroog'/'LGN', 'hundroog').
card_uid('hundroog'/'LGN', 'LGN:Hundroog:hundroog').
card_rarity('hundroog'/'LGN', 'Common').
card_artist('hundroog'/'LGN', 'Wayne England').
card_number('hundroog'/'LGN', '129').
card_flavor_text('hundroog'/'LGN', '\"New links are being added to the food chain on a daily basis.\"\n—Riptide Project researcher').
card_multiverse_id('hundroog'/'LGN', '42249').

card_in_set('hunter sliver', 'LGN').
card_original_type('hunter sliver'/'LGN', 'Creature — Sliver').
card_original_text('hunter sliver'/'LGN', 'All Slivers have provoke. (When a Sliver attacks, its controller may have target creature defending player controls untap and block it if able.)').
card_first_print('hunter sliver', 'LGN').
card_image_name('hunter sliver'/'LGN', 'hunter sliver').
card_uid('hunter sliver'/'LGN', 'LGN:Hunter Sliver:hunter sliver').
card_rarity('hunter sliver'/'LGN', 'Common').
card_artist('hunter sliver'/'LGN', 'Kev Walker').
card_number('hunter sliver'/'LGN', '102').
card_flavor_text('hunter sliver'/'LGN', 'Once they get the scent, there is no escape.').
card_multiverse_id('hunter sliver'/'LGN', '44772').

card_in_set('imperial hellkite', 'LGN').
card_original_type('imperial hellkite'/'LGN', 'Creature — Dragon').
card_original_text('imperial hellkite'/'LGN', 'Flying\nMorph {6}{R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Imperial Hellkite is turned face up, you may search your library for a Dragon card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_first_print('imperial hellkite', 'LGN').
card_image_name('imperial hellkite'/'LGN', 'imperial hellkite').
card_uid('imperial hellkite'/'LGN', 'LGN:Imperial Hellkite:imperial hellkite').
card_rarity('imperial hellkite'/'LGN', 'Rare').
card_artist('imperial hellkite'/'LGN', 'Matt Cavotta').
card_number('imperial hellkite'/'LGN', '103').
card_multiverse_id('imperial hellkite'/'LGN', '44623').

card_in_set('infernal caretaker', 'LGN').
card_original_type('infernal caretaker'/'LGN', 'Creature — Cleric').
card_original_text('infernal caretaker'/'LGN', 'Morph {3}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Infernal Caretaker is turned face up, return all Zombie cards from all graveyards to their owners\' hands.').
card_first_print('infernal caretaker', 'LGN').
card_image_name('infernal caretaker'/'LGN', 'infernal caretaker').
card_uid('infernal caretaker'/'LGN', 'LGN:Infernal Caretaker:infernal caretaker').
card_rarity('infernal caretaker'/'LGN', 'Common').
card_artist('infernal caretaker'/'LGN', 'Christopher Moeller').
card_number('infernal caretaker'/'LGN', '76').
card_multiverse_id('infernal caretaker'/'LGN', '44411').

card_in_set('keeneye aven', 'LGN').
card_original_type('keeneye aven'/'LGN', 'Creature — Bird Soldier').
card_original_text('keeneye aven'/'LGN', 'Flying\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('keeneye aven', 'LGN').
card_image_name('keeneye aven'/'LGN', 'keeneye aven').
card_uid('keeneye aven'/'LGN', 'LGN:Keeneye Aven:keeneye aven').
card_rarity('keeneye aven'/'LGN', 'Common').
card_artist('keeneye aven'/'LGN', 'Greg Hildebrandt').
card_number('keeneye aven'/'LGN', '41').
card_flavor_text('keeneye aven'/'LGN', '\"I have no need of a map. The very continent itself guides my way.\"').
card_multiverse_id('keeneye aven'/'LGN', '35335').

card_in_set('keeper of the nine gales', 'LGN').
card_original_type('keeper of the nine gales'/'LGN', 'Creature — Bird Wizard').
card_original_text('keeper of the nine gales'/'LGN', 'Flying\n{T}, Tap two untapped Birds you control: Return target permanent to its owner\'s hand.').
card_first_print('keeper of the nine gales', 'LGN').
card_image_name('keeper of the nine gales'/'LGN', 'keeper of the nine gales').
card_uid('keeper of the nine gales'/'LGN', 'LGN:Keeper of the Nine Gales:keeper of the nine gales').
card_rarity('keeper of the nine gales'/'LGN', 'Rare').
card_artist('keeper of the nine gales'/'LGN', 'Jim Nelson').
card_number('keeper of the nine gales'/'LGN', '42').
card_flavor_text('keeper of the nine gales'/'LGN', '\"You cannot fight the storm.\"').
card_multiverse_id('keeper of the nine gales'/'LGN', '44626').

card_in_set('kilnmouth dragon', 'LGN').
card_original_type('kilnmouth dragon'/'LGN', 'Creature — Dragon').
card_original_text('kilnmouth dragon'/'LGN', 'Amplify 3 (As this card comes into play, put three +1/+1 counters on it for each Dragon card you reveal in your hand.)\nFlying\n{T}: Kilnmouth Dragon deals damage equal to the number of +1/+1 counters on it to target creature or player.').
card_first_print('kilnmouth dragon', 'LGN').
card_image_name('kilnmouth dragon'/'LGN', 'kilnmouth dragon').
card_uid('kilnmouth dragon'/'LGN', 'LGN:Kilnmouth Dragon:kilnmouth dragon').
card_rarity('kilnmouth dragon'/'LGN', 'Rare').
card_artist('kilnmouth dragon'/'LGN', 'Carl Critchlow').
card_number('kilnmouth dragon'/'LGN', '104').
card_multiverse_id('kilnmouth dragon'/'LGN', '39653').

card_in_set('krosan cloudscraper', 'LGN').
card_original_type('krosan cloudscraper'/'LGN', 'Creature — Beast Mutant').
card_original_text('krosan cloudscraper'/'LGN', 'At the beginning of your upkeep, sacrifice Krosan Cloudscraper unless you pay {G}{G}.\nMorph {7}{G}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('krosan cloudscraper', 'LGN').
card_image_name('krosan cloudscraper'/'LGN', 'krosan cloudscraper').
card_uid('krosan cloudscraper'/'LGN', 'LGN:Krosan Cloudscraper:krosan cloudscraper').
card_rarity('krosan cloudscraper'/'LGN', 'Rare').
card_artist('krosan cloudscraper'/'LGN', 'Ron Spears').
card_number('krosan cloudscraper'/'LGN', '130').
card_multiverse_id('krosan cloudscraper'/'LGN', '35166').

card_in_set('krosan vorine', 'LGN').
card_original_type('krosan vorine'/'LGN', 'Creature — Cat Beast').
card_original_text('krosan vorine'/'LGN', 'Provoke (When this attacks, you may have target creature defending player controls untap and block it if able.)\nKrosan Vorine can\'t be blocked by more than one creature.').
card_first_print('krosan vorine', 'LGN').
card_image_name('krosan vorine'/'LGN', 'krosan vorine').
card_uid('krosan vorine'/'LGN', 'LGN:Krosan Vorine:krosan vorine').
card_rarity('krosan vorine'/'LGN', 'Common').
card_artist('krosan vorine'/'LGN', 'Carl Critchlow').
card_number('krosan vorine'/'LGN', '131').
card_multiverse_id('krosan vorine'/'LGN', '45134').

card_in_set('lavaborn muse', 'LGN').
card_original_type('lavaborn muse'/'LGN', 'Creature — Spirit').
card_original_text('lavaborn muse'/'LGN', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Lavaborn Muse deals 3 damage to him or her.').
card_first_print('lavaborn muse', 'LGN').
card_image_name('lavaborn muse'/'LGN', 'lavaborn muse').
card_uid('lavaborn muse'/'LGN', 'LGN:Lavaborn Muse:lavaborn muse').
card_rarity('lavaborn muse'/'LGN', 'Rare').
card_artist('lavaborn muse'/'LGN', 'Brian Snõddy').
card_number('lavaborn muse'/'LGN', '105').
card_flavor_text('lavaborn muse'/'LGN', '\"Her voice is disaster, painful and final.\"\n—Matoc, lavamancer').
card_multiverse_id('lavaborn muse'/'LGN', '44842').

card_in_set('liege of the axe', 'LGN').
card_original_type('liege of the axe'/'LGN', 'Creature — Soldier').
card_original_text('liege of the axe'/'LGN', 'Attacking doesn\'t cause Liege of the Axe to tap.\nMorph {1}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Liege of the Axe is turned face up, untap it.').
card_first_print('liege of the axe', 'LGN').
card_image_name('liege of the axe'/'LGN', 'liege of the axe').
card_uid('liege of the axe'/'LGN', 'LGN:Liege of the Axe:liege of the axe').
card_rarity('liege of the axe'/'LGN', 'Uncommon').
card_artist('liege of the axe'/'LGN', 'Christopher Moeller').
card_number('liege of the axe'/'LGN', '16').
card_multiverse_id('liege of the axe'/'LGN', '39603').

card_in_set('lowland tracker', 'LGN').
card_original_type('lowland tracker'/'LGN', 'Creature — Soldier').
card_original_text('lowland tracker'/'LGN', 'First strike\nProvoke (When this attacks, you may have target creature defending player controls untap and block it if able.)').
card_first_print('lowland tracker', 'LGN').
card_image_name('lowland tracker'/'LGN', 'lowland tracker').
card_uid('lowland tracker'/'LGN', 'LGN:Lowland Tracker:lowland tracker').
card_rarity('lowland tracker'/'LGN', 'Common').
card_artist('lowland tracker'/'LGN', 'Brian Snõddy').
card_number('lowland tracker'/'LGN', '17').
card_flavor_text('lowland tracker'/'LGN', '\"I feed my hatred to the righteous and they join my crusade.\"\n—Akroma, angelic avenger').
card_multiverse_id('lowland tracker'/'LGN', '45131').

card_in_set('macetail hystrodon', 'LGN').
card_original_type('macetail hystrodon'/'LGN', 'Creature — Beast').
card_original_text('macetail hystrodon'/'LGN', 'First strike, haste\nCycling {3} ({3}, Discard this card from your hand: Draw a card.)').
card_first_print('macetail hystrodon', 'LGN').
card_image_name('macetail hystrodon'/'LGN', 'macetail hystrodon').
card_uid('macetail hystrodon'/'LGN', 'LGN:Macetail Hystrodon:macetail hystrodon').
card_rarity('macetail hystrodon'/'LGN', 'Common').
card_artist('macetail hystrodon'/'LGN', 'Daren Bader').
card_number('macetail hystrodon'/'LGN', '106').
card_flavor_text('macetail hystrodon'/'LGN', 'The goblins tracked the hystrodon with much stealth and cunning. Then they were eaten with much pain and yelling.').
card_multiverse_id('macetail hystrodon'/'LGN', '42440').

card_in_set('magma sliver', 'LGN').
card_original_type('magma sliver'/'LGN', 'Creature — Sliver').
card_original_text('magma sliver'/'LGN', 'All Slivers have \"{T}: Target Sliver gets +X/+0 until end of turn, where X is the number of Slivers in play.\"').
card_first_print('magma sliver', 'LGN').
card_image_name('magma sliver'/'LGN', 'magma sliver').
card_uid('magma sliver'/'LGN', 'LGN:Magma Sliver:magma sliver').
card_rarity('magma sliver'/'LGN', 'Rare').
card_artist('magma sliver'/'LGN', 'Wayne England').
card_number('magma sliver'/'LGN', '107').
card_flavor_text('magma sliver'/'LGN', 'As malleable as molten steel, but as dangerous as the finished blade.').
card_multiverse_id('magma sliver'/'LGN', '42041').

card_in_set('master of the veil', 'LGN').
card_original_type('master of the veil'/'LGN', 'Creature — Wizard').
card_original_text('master of the veil'/'LGN', 'Morph {2}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Master of the Veil is turned face up, you may turn target creature with morph face down.').
card_first_print('master of the veil', 'LGN').
card_image_name('master of the veil'/'LGN', 'master of the veil').
card_uid('master of the veil'/'LGN', 'LGN:Master of the Veil:master of the veil').
card_rarity('master of the veil'/'LGN', 'Uncommon').
card_artist('master of the veil'/'LGN', 'Ron Spears').
card_number('master of the veil'/'LGN', '43').
card_multiverse_id('master of the veil'/'LGN', '42068').

card_in_set('merchant of secrets', 'LGN').
card_original_type('merchant of secrets'/'LGN', 'Creature — Wizard').
card_original_text('merchant of secrets'/'LGN', 'When Merchant of Secrets comes into play, draw a card.').
card_first_print('merchant of secrets', 'LGN').
card_image_name('merchant of secrets'/'LGN', 'merchant of secrets').
card_uid('merchant of secrets'/'LGN', 'LGN:Merchant of Secrets:merchant of secrets').
card_rarity('merchant of secrets'/'LGN', 'Common').
card_artist('merchant of secrets'/'LGN', 'Greg Hildebrandt').
card_number('merchant of secrets'/'LGN', '44').
card_flavor_text('merchant of secrets'/'LGN', 'To scrape out a living in Aphetto, wizards are reduced to selling rumors, lies, forgeries, or—if they get desperate enough—the truth.').
card_multiverse_id('merchant of secrets'/'LGN', '44501').

card_in_set('mistform seaswift', 'LGN').
card_original_type('mistform seaswift'/'LGN', 'Creature — Illusion').
card_original_text('mistform seaswift'/'LGN', 'Flying\n{1}: Mistform Seaswift\'s type becomes the creature type of your choice until end of turn.\nMorph {1}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('mistform seaswift', 'LGN').
card_image_name('mistform seaswift'/'LGN', 'mistform seaswift').
card_uid('mistform seaswift'/'LGN', 'LGN:Mistform Seaswift:mistform seaswift').
card_rarity('mistform seaswift'/'LGN', 'Common').
card_artist('mistform seaswift'/'LGN', 'Dany Orizio').
card_number('mistform seaswift'/'LGN', '45').
card_multiverse_id('mistform seaswift'/'LGN', '43721').

card_in_set('mistform sliver', 'LGN').
card_original_type('mistform sliver'/'LGN', 'Creature — Illusion Sliver').
card_original_text('mistform sliver'/'LGN', 'All Slivers have \"{1}: This creature\'s type becomes the creature type of your choice in addition to its other types until end of turn.\"').
card_first_print('mistform sliver', 'LGN').
card_image_name('mistform sliver'/'LGN', 'mistform sliver').
card_uid('mistform sliver'/'LGN', 'LGN:Mistform Sliver:mistform sliver').
card_rarity('mistform sliver'/'LGN', 'Common').
card_artist('mistform sliver'/'LGN', 'Ben Thompson').
card_number('mistform sliver'/'LGN', '46').
card_flavor_text('mistform sliver'/'LGN', 'Taking the form of a junior researcher, the first sliver slipped out of Riptide.').
card_multiverse_id('mistform sliver'/'LGN', '42037').

card_in_set('mistform ultimus', 'LGN').
card_original_type('mistform ultimus'/'LGN', 'Creature — Illusion Legend').
card_original_text('mistform ultimus'/'LGN', 'Mistform Ultimus is every creature type (even if this card isn\'t in play).\nMistform Ultimus may attack as though it weren\'t a Wall.').
card_first_print('mistform ultimus', 'LGN').
card_image_name('mistform ultimus'/'LGN', 'mistform ultimus').
card_uid('mistform ultimus'/'LGN', 'LGN:Mistform Ultimus:mistform ultimus').
card_rarity('mistform ultimus'/'LGN', 'Rare').
card_artist('mistform ultimus'/'LGN', 'Anthony S. Waters').
card_number('mistform ultimus'/'LGN', '47').
card_flavor_text('mistform ultimus'/'LGN', 'It may wear your face, but its mind is its own.').
card_multiverse_id('mistform ultimus'/'LGN', '45840').

card_in_set('mistform wakecaster', 'LGN').
card_original_type('mistform wakecaster'/'LGN', 'Creature — Illusion').
card_original_text('mistform wakecaster'/'LGN', 'Flying\n{1}: Mistform Wakecaster\'s type becomes the creature type of your choice until end of turn.\n{2}{U}{U}, {T}: Choose a creature type. The type of each creature you control becomes that type until end of turn.').
card_first_print('mistform wakecaster', 'LGN').
card_image_name('mistform wakecaster'/'LGN', 'mistform wakecaster').
card_uid('mistform wakecaster'/'LGN', 'LGN:Mistform Wakecaster:mistform wakecaster').
card_rarity('mistform wakecaster'/'LGN', 'Uncommon').
card_artist('mistform wakecaster'/'LGN', 'Glen Angus').
card_number('mistform wakecaster'/'LGN', '48').
card_multiverse_id('mistform wakecaster'/'LGN', '42165').

card_in_set('nantuko vigilante', 'LGN').
card_original_type('nantuko vigilante'/'LGN', 'Creature — Insect Druid Mutant').
card_original_text('nantuko vigilante'/'LGN', 'Morph {1}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Nantuko Vigilante is turned face up, destroy target artifact or enchantment.').
card_first_print('nantuko vigilante', 'LGN').
card_image_name('nantuko vigilante'/'LGN', 'nantuko vigilante').
card_uid('nantuko vigilante'/'LGN', 'LGN:Nantuko Vigilante:nantuko vigilante').
card_rarity('nantuko vigilante'/'LGN', 'Common').
card_artist('nantuko vigilante'/'LGN', 'Alex Horley-Orlandelli').
card_number('nantuko vigilante'/'LGN', '132').
card_multiverse_id('nantuko vigilante'/'LGN', '44210').

card_in_set('needleshot gourna', 'LGN').
card_original_type('needleshot gourna'/'LGN', 'Creature — Beast').
card_original_text('needleshot gourna'/'LGN', 'Needleshot Gourna may block as though it had flying.').
card_first_print('needleshot gourna', 'LGN').
card_image_name('needleshot gourna'/'LGN', 'needleshot gourna').
card_uid('needleshot gourna'/'LGN', 'LGN:Needleshot Gourna:needleshot gourna').
card_rarity('needleshot gourna'/'LGN', 'Common').
card_artist('needleshot gourna'/'LGN', 'Edward P. Beard, Jr.').
card_number('needleshot gourna'/'LGN', '133').
card_flavor_text('needleshot gourna'/'LGN', 'The first aven scout squad returned from Krosa with disturbing stories. The second returned with disturbing casualties.').
card_multiverse_id('needleshot gourna'/'LGN', '42039').

card_in_set('noxious ghoul', 'LGN').
card_original_type('noxious ghoul'/'LGN', 'Creature — Zombie').
card_original_text('noxious ghoul'/'LGN', 'Whenever Noxious Ghoul or another Zombie comes into play, all non-Zombie creatures get -1/-1 until end of turn.').
card_first_print('noxious ghoul', 'LGN').
card_image_name('noxious ghoul'/'LGN', 'noxious ghoul').
card_uid('noxious ghoul'/'LGN', 'LGN:Noxious Ghoul:noxious ghoul').
card_rarity('noxious ghoul'/'LGN', 'Uncommon').
card_artist('noxious ghoul'/'LGN', 'Luca Zontini').
card_number('noxious ghoul'/'LGN', '77').
card_flavor_text('noxious ghoul'/'LGN', 'Plague and death wrapped in one convenient package.').
card_multiverse_id('noxious ghoul'/'LGN', '44276').

card_in_set('patron of the wild', 'LGN').
card_original_type('patron of the wild'/'LGN', 'Creature — Elf').
card_original_text('patron of the wild'/'LGN', 'Morph {2}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Patron of the Wild is turned face up, target creature gets +3/+3 until end of turn.').
card_first_print('patron of the wild', 'LGN').
card_image_name('patron of the wild'/'LGN', 'patron of the wild').
card_uid('patron of the wild'/'LGN', 'LGN:Patron of the Wild:patron of the wild').
card_rarity('patron of the wild'/'LGN', 'Common').
card_artist('patron of the wild'/'LGN', 'Dave Dorman').
card_number('patron of the wild'/'LGN', '134').
card_multiverse_id('patron of the wild'/'LGN', '42057').

card_in_set('phage the untouchable', 'LGN').
card_original_type('phage the untouchable'/'LGN', 'Creature — Minion Legend').
card_original_text('phage the untouchable'/'LGN', 'When Phage the Untouchable comes into play, if you didn\'t play it from your hand, you lose the game.\nWhenever Phage deals combat damage to a creature, destroy that creature. It can\'t be regenerated.\nWhenever Phage deals combat damage to a player, that player loses the game.').
card_first_print('phage the untouchable', 'LGN').
card_image_name('phage the untouchable'/'LGN', 'phage the untouchable').
card_uid('phage the untouchable'/'LGN', 'LGN:Phage the Untouchable:phage the untouchable').
card_rarity('phage the untouchable'/'LGN', 'Rare').
card_artist('phage the untouchable'/'LGN', 'Ron Spears').
card_number('phage the untouchable'/'LGN', '78').
card_multiverse_id('phage the untouchable'/'LGN', '40545').

card_in_set('planar guide', 'LGN').
card_original_type('planar guide'/'LGN', 'Creature — Cleric').
card_original_text('planar guide'/'LGN', '{3}{W}, Remove Planar Guide from the game: Remove all creatures from the game. At end of turn, return those cards to play under their owners\' control.').
card_first_print('planar guide', 'LGN').
card_image_name('planar guide'/'LGN', 'planar guide').
card_uid('planar guide'/'LGN', 'LGN:Planar Guide:planar guide').
card_rarity('planar guide'/'LGN', 'Rare').
card_artist('planar guide'/'LGN', 'Eric Peterson').
card_number('planar guide'/'LGN', '18').
card_flavor_text('planar guide'/'LGN', '\"Every moment has its own savior.\"').
card_multiverse_id('planar guide'/'LGN', '44323').

card_in_set('plated sliver', 'LGN').
card_original_type('plated sliver'/'LGN', 'Creature — Sliver').
card_original_text('plated sliver'/'LGN', 'All Slivers get +0/+1.').
card_first_print('plated sliver', 'LGN').
card_image_name('plated sliver'/'LGN', 'plated sliver').
card_uid('plated sliver'/'LGN', 'LGN:Plated Sliver:plated sliver').
card_rarity('plated sliver'/'LGN', 'Common').
card_artist('plated sliver'/'LGN', 'Greg Staples').
card_number('plated sliver'/'LGN', '19').
card_flavor_text('plated sliver'/'LGN', 'Overcoming extinction has only made the slivers more determined to live.').
card_multiverse_id('plated sliver'/'LGN', '42017').

card_in_set('primal whisperer', 'LGN').
card_original_type('primal whisperer'/'LGN', 'Creature — Elf Soldier').
card_original_text('primal whisperer'/'LGN', 'Primal Whisperer gets +2/+2 for each face-down creature in play.\nMorph {3}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('primal whisperer', 'LGN').
card_image_name('primal whisperer'/'LGN', 'primal whisperer').
card_uid('primal whisperer'/'LGN', 'LGN:Primal Whisperer:primal whisperer').
card_rarity('primal whisperer'/'LGN', 'Rare').
card_artist('primal whisperer'/'LGN', 'Greg Staples').
card_number('primal whisperer'/'LGN', '135').
card_multiverse_id('primal whisperer'/'LGN', '42272').

card_in_set('primoc escapee', 'LGN').
card_original_type('primoc escapee'/'LGN', 'Creature — Bird Beast').
card_original_text('primoc escapee'/'LGN', 'Flying\nCycling {2} ({2}, Discard this card from your hand: Draw a card.)').
card_first_print('primoc escapee', 'LGN').
card_image_name('primoc escapee'/'LGN', 'primoc escapee').
card_uid('primoc escapee'/'LGN', 'LGN:Primoc Escapee:primoc escapee').
card_rarity('primoc escapee'/'LGN', 'Uncommon').
card_artist('primoc escapee'/'LGN', 'Tony Szczudlo').
card_number('primoc escapee'/'LGN', '49').
card_flavor_text('primoc escapee'/'LGN', 'Though a completely artificial species, primocs are a natural fit for the skies of Otaria.').
card_multiverse_id('primoc escapee'/'LGN', '42437').

card_in_set('quick sliver', 'LGN').
card_original_type('quick sliver'/'LGN', 'Creature — Sliver').
card_original_text('quick sliver'/'LGN', 'You may play Quick Sliver any time you could play an instant.\nAny player may play Sliver cards any time he or she could play an instant.').
card_first_print('quick sliver', 'LGN').
card_image_name('quick sliver'/'LGN', 'quick sliver').
card_uid('quick sliver'/'LGN', 'LGN:Quick Sliver:quick sliver').
card_rarity('quick sliver'/'LGN', 'Common').
card_artist('quick sliver'/'LGN', 'John Avon').
card_number('quick sliver'/'LGN', '136').
card_flavor_text('quick sliver'/'LGN', 'The directors of the Riptide Project wanted instant results on the sliver experiments. They got their wish.').
card_multiverse_id('quick sliver'/'LGN', '42029').

card_in_set('ridgetop raptor', 'LGN').
card_original_type('ridgetop raptor'/'LGN', 'Creature — Beast').
card_original_text('ridgetop raptor'/'LGN', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_first_print('ridgetop raptor', 'LGN').
card_image_name('ridgetop raptor'/'LGN', 'ridgetop raptor').
card_uid('ridgetop raptor'/'LGN', 'LGN:Ridgetop Raptor:ridgetop raptor').
card_rarity('ridgetop raptor'/'LGN', 'Uncommon').
card_artist('ridgetop raptor'/'LGN', 'Daren Bader').
card_number('ridgetop raptor'/'LGN', '108').
card_flavor_text('ridgetop raptor'/'LGN', '\"The Skirk Ridge has many wonderful things to discover—like escape routes.\"\n—Foothill guide').
card_multiverse_id('ridgetop raptor'/'LGN', '39566').

card_in_set('riptide director', 'LGN').
card_original_type('riptide director'/'LGN', 'Creature — Wizard').
card_original_text('riptide director'/'LGN', '{2}{U}{U}, {T}: Draw a card for each Wizard you control.').
card_first_print('riptide director', 'LGN').
card_image_name('riptide director'/'LGN', 'riptide director').
card_uid('riptide director'/'LGN', 'LGN:Riptide Director:riptide director').
card_rarity('riptide director'/'LGN', 'Rare').
card_artist('riptide director'/'LGN', 'Scott M. Fischer').
card_number('riptide director'/'LGN', '50').
card_flavor_text('riptide director'/'LGN', 'Those who lead others to wisdom become the wisest themselves.').
card_multiverse_id('riptide director'/'LGN', '39733').

card_in_set('riptide mangler', 'LGN').
card_original_type('riptide mangler'/'LGN', 'Creature — Beast').
card_original_text('riptide mangler'/'LGN', '{1}{U}: Change Riptide Mangler\'s power to target creature\'s power. (It doesn\'t change back at end of turn.)').
card_first_print('riptide mangler', 'LGN').
card_image_name('riptide mangler'/'LGN', 'riptide mangler').
card_uid('riptide mangler'/'LGN', 'LGN:Riptide Mangler:riptide mangler').
card_rarity('riptide mangler'/'LGN', 'Rare').
card_artist('riptide mangler'/'LGN', 'Arnie Swekel').
card_number('riptide mangler'/'LGN', '51').
card_flavor_text('riptide mangler'/'LGN', 'It wants you to be its chum.').
card_multiverse_id('riptide mangler'/'LGN', '39914').

card_in_set('rockshard elemental', 'LGN').
card_original_type('rockshard elemental'/'LGN', 'Creature — Elemental').
card_original_text('rockshard elemental'/'LGN', 'Double strike (This creature deals both first-strike and regular combat damage.)\nMorph {4}{R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('rockshard elemental', 'LGN').
card_image_name('rockshard elemental'/'LGN', 'rockshard elemental').
card_uid('rockshard elemental'/'LGN', 'LGN:Rockshard Elemental:rockshard elemental').
card_rarity('rockshard elemental'/'LGN', 'Rare').
card_artist('rockshard elemental'/'LGN', 'Anthony S. Waters').
card_number('rockshard elemental'/'LGN', '109').
card_multiverse_id('rockshard elemental'/'LGN', '39736').

card_in_set('root sliver', 'LGN').
card_original_type('root sliver'/'LGN', 'Creature — Sliver').
card_original_text('root sliver'/'LGN', 'Root Sliver can\'t be countered.\nSliver spells can\'t be countered.').
card_first_print('root sliver', 'LGN').
card_image_name('root sliver'/'LGN', 'root sliver').
card_uid('root sliver'/'LGN', 'LGN:Root Sliver:root sliver').
card_rarity('root sliver'/'LGN', 'Uncommon').
card_artist('root sliver'/'LGN', 'Matt Thompson').
card_number('root sliver'/'LGN', '137').
card_flavor_text('root sliver'/'LGN', '\"It would take another apocalypse to stop the slivers now.\"\n—Riptide Project researcher').
card_multiverse_id('root sliver'/'LGN', '44271').

card_in_set('scion of darkness', 'LGN').
card_original_type('scion of darkness'/'LGN', 'Creature — Avatar').
card_original_text('scion of darkness'/'LGN', 'Trample\nWhenever Scion of Darkness deals combat damage to a player, you may put target creature card from that player\'s graveyard into play under your control.\nCycling {3} ({3}, Discard this card from your hand: Draw a card.)').
card_first_print('scion of darkness', 'LGN').
card_image_name('scion of darkness'/'LGN', 'scion of darkness').
card_uid('scion of darkness'/'LGN', 'LGN:Scion of Darkness:scion of darkness').
card_rarity('scion of darkness'/'LGN', 'Rare').
card_artist('scion of darkness'/'LGN', 'Mark Zug').
card_number('scion of darkness'/'LGN', '79').
card_multiverse_id('scion of darkness'/'LGN', '42050').

card_in_set('seedborn muse', 'LGN').
card_original_type('seedborn muse'/'LGN', 'Creature — Spirit').
card_original_text('seedborn muse'/'LGN', 'Untap all permanents you control during each other player\'s untap step.').
card_first_print('seedborn muse', 'LGN').
card_image_name('seedborn muse'/'LGN', 'seedborn muse').
card_uid('seedborn muse'/'LGN', 'LGN:Seedborn Muse:seedborn muse').
card_rarity('seedborn muse'/'LGN', 'Rare').
card_artist('seedborn muse'/'LGN', 'Adam Rex').
card_number('seedborn muse'/'LGN', '138').
card_flavor_text('seedborn muse'/'LGN', '\"Her voice is wilderness, savage and pure.\"\n—Kamahl, druid acolyte').
card_multiverse_id('seedborn muse'/'LGN', '43607').

card_in_set('shaleskin plower', 'LGN').
card_original_type('shaleskin plower'/'LGN', 'Creature — Beast').
card_original_text('shaleskin plower'/'LGN', 'Morph {4}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Shaleskin Plower is turned face up, destroy target land.').
card_first_print('shaleskin plower', 'LGN').
card_image_name('shaleskin plower'/'LGN', 'shaleskin plower').
card_uid('shaleskin plower'/'LGN', 'LGN:Shaleskin Plower:shaleskin plower').
card_rarity('shaleskin plower'/'LGN', 'Common').
card_artist('shaleskin plower'/'LGN', 'Daren Bader').
card_number('shaleskin plower'/'LGN', '110').
card_multiverse_id('shaleskin plower'/'LGN', '44282').

card_in_set('shifting sliver', 'LGN').
card_original_type('shifting sliver'/'LGN', 'Creature — Sliver').
card_original_text('shifting sliver'/'LGN', 'Slivers can\'t be blocked except by Slivers.').
card_first_print('shifting sliver', 'LGN').
card_image_name('shifting sliver'/'LGN', 'shifting sliver').
card_uid('shifting sliver'/'LGN', 'LGN:Shifting Sliver:shifting sliver').
card_rarity('shifting sliver'/'LGN', 'Uncommon').
card_artist('shifting sliver'/'LGN', 'Darrell Riche').
card_number('shifting sliver'/'LGN', '52').
card_flavor_text('shifting sliver'/'LGN', 'Once the last few slivers the Riptide Project controlled were dead, there was nothing to keep the island from being completely overrun.').
card_multiverse_id('shifting sliver'/'LGN', '42078').

card_in_set('skinthinner', 'LGN').
card_original_type('skinthinner'/'LGN', 'Creature — Zombie').
card_original_text('skinthinner'/'LGN', 'Morph {3}{B}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Skinthinner is turned face up, destroy target nonblack creature. It can\'t be regenerated.').
card_first_print('skinthinner', 'LGN').
card_image_name('skinthinner'/'LGN', 'skinthinner').
card_uid('skinthinner'/'LGN', 'LGN:Skinthinner:skinthinner').
card_rarity('skinthinner'/'LGN', 'Common').
card_artist('skinthinner'/'LGN', 'Dany Orizio').
card_number('skinthinner'/'LGN', '80').
card_multiverse_id('skinthinner'/'LGN', '42055').

card_in_set('skirk alarmist', 'LGN').
card_original_type('skirk alarmist'/'LGN', 'Creature — Wizard').
card_original_text('skirk alarmist'/'LGN', 'Haste\n{T}: Turn target face-down creature you control face up. At end of turn, sacrifice it.').
card_first_print('skirk alarmist', 'LGN').
card_image_name('skirk alarmist'/'LGN', 'skirk alarmist').
card_uid('skirk alarmist'/'LGN', 'LGN:Skirk Alarmist:skirk alarmist').
card_rarity('skirk alarmist'/'LGN', 'Rare').
card_artist('skirk alarmist'/'LGN', 'Justin Sweet').
card_number('skirk alarmist'/'LGN', '111').
card_flavor_text('skirk alarmist'/'LGN', '\"I treat each day as your last.\"').
card_multiverse_id('skirk alarmist'/'LGN', '44306').

card_in_set('skirk drill sergeant', 'LGN').
card_original_type('skirk drill sergeant'/'LGN', 'Creature — Goblin').
card_original_text('skirk drill sergeant'/'LGN', 'Whenever Skirk Drill Sergeant or another Goblin is put into a graveyard from play, you may pay {2}{R}. If you do, reveal the top card of your library. If it\'s a Goblin card, put it into play. Otherwise, put it into your graveyard.').
card_first_print('skirk drill sergeant', 'LGN').
card_image_name('skirk drill sergeant'/'LGN', 'skirk drill sergeant').
card_uid('skirk drill sergeant'/'LGN', 'LGN:Skirk Drill Sergeant:skirk drill sergeant').
card_rarity('skirk drill sergeant'/'LGN', 'Uncommon').
card_artist('skirk drill sergeant'/'LGN', 'Alex Horley-Orlandelli').
card_number('skirk drill sergeant'/'LGN', '112').
card_flavor_text('skirk drill sergeant'/'LGN', '\"I order you to volunteer.\"').
card_multiverse_id('skirk drill sergeant'/'LGN', '46457').

card_in_set('skirk marauder', 'LGN').
card_original_type('skirk marauder'/'LGN', 'Creature — Goblin').
card_original_text('skirk marauder'/'LGN', 'Morph {2}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Skirk Marauder is turned face up, it deals 2 damage to target creature or player.').
card_image_name('skirk marauder'/'LGN', 'skirk marauder').
card_uid('skirk marauder'/'LGN', 'LGN:Skirk Marauder:skirk marauder').
card_rarity('skirk marauder'/'LGN', 'Common').
card_artist('skirk marauder'/'LGN', 'Pete Venters').
card_number('skirk marauder'/'LGN', '113').
card_multiverse_id('skirk marauder'/'LGN', '42056').

card_in_set('skirk outrider', 'LGN').
card_original_type('skirk outrider'/'LGN', 'Creature — Goblin').
card_original_text('skirk outrider'/'LGN', 'Skirk Outrider gets +2/+2 and has trample as long as you control a Beast.').
card_first_print('skirk outrider', 'LGN').
card_image_name('skirk outrider'/'LGN', 'skirk outrider').
card_uid('skirk outrider'/'LGN', 'LGN:Skirk Outrider:skirk outrider').
card_rarity('skirk outrider'/'LGN', 'Common').
card_artist('skirk outrider'/'LGN', 'Greg Staples').
card_number('skirk outrider'/'LGN', '114').
card_flavor_text('skirk outrider'/'LGN', 'Once the goblins thought about it, they preferred being atop a slateback to being in front of, behind, or underneath one.').
card_multiverse_id('skirk outrider'/'LGN', '42082').

card_in_set('smokespew invoker', 'LGN').
card_original_type('smokespew invoker'/'LGN', 'Creature — Zombie Mutant').
card_original_text('smokespew invoker'/'LGN', '{7}{B}: Target creature gets -3/-3 until end of turn.').
card_first_print('smokespew invoker', 'LGN').
card_image_name('smokespew invoker'/'LGN', 'smokespew invoker').
card_uid('smokespew invoker'/'LGN', 'LGN:Smokespew Invoker:smokespew invoker').
card_rarity('smokespew invoker'/'LGN', 'Common').
card_artist('smokespew invoker'/'LGN', 'Thomas M. Baxa').
card_number('smokespew invoker'/'LGN', '81').
card_flavor_text('smokespew invoker'/'LGN', 'The Mirari festers in its flesh.').
card_multiverse_id('smokespew invoker'/'LGN', '43714').

card_in_set('sootfeather flock', 'LGN').
card_original_type('sootfeather flock'/'LGN', 'Creature — Bird').
card_original_text('sootfeather flock'/'LGN', 'Flying\nMorph {3}{B} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('sootfeather flock', 'LGN').
card_image_name('sootfeather flock'/'LGN', 'sootfeather flock').
card_uid('sootfeather flock'/'LGN', 'LGN:Sootfeather Flock:sootfeather flock').
card_rarity('sootfeather flock'/'LGN', 'Common').
card_artist('sootfeather flock'/'LGN', 'David Martin').
card_number('sootfeather flock'/'LGN', '82').
card_flavor_text('sootfeather flock'/'LGN', 'They pick at the remains of the city\'s corpse.').
card_multiverse_id('sootfeather flock'/'LGN', '44207').

card_in_set('spectral sliver', 'LGN').
card_original_type('spectral sliver'/'LGN', 'Creature — Sliver').
card_original_text('spectral sliver'/'LGN', 'All Slivers have \"{2}: This creature gets +1/+1 until end of turn.\"').
card_first_print('spectral sliver', 'LGN').
card_image_name('spectral sliver'/'LGN', 'spectral sliver').
card_uid('spectral sliver'/'LGN', 'LGN:Spectral Sliver:spectral sliver').
card_rarity('spectral sliver'/'LGN', 'Uncommon').
card_artist('spectral sliver'/'LGN', 'Pete Venters').
card_number('spectral sliver'/'LGN', '83').
card_flavor_text('spectral sliver'/'LGN', '\"Sure, I\'ve seen agents of the Cabal here and there. Well, not here, and certainly not in any of the sliver labs. Oh dear, I\'ve said too much.\"\n—Apprentice researcher').
card_multiverse_id('spectral sliver'/'LGN', '44277').

card_in_set('starlight invoker', 'LGN').
card_original_type('starlight invoker'/'LGN', 'Creature — Cleric Mutant').
card_original_text('starlight invoker'/'LGN', '{7}{W}: You gain 5 life.').
card_first_print('starlight invoker', 'LGN').
card_image_name('starlight invoker'/'LGN', 'starlight invoker').
card_uid('starlight invoker'/'LGN', 'LGN:Starlight Invoker:starlight invoker').
card_rarity('starlight invoker'/'LGN', 'Common').
card_artist('starlight invoker'/'LGN', 'Glen Angus').
card_number('starlight invoker'/'LGN', '20').
card_flavor_text('starlight invoker'/'LGN', 'The Mirari glows in her eyes.').
card_multiverse_id('starlight invoker'/'LGN', '43792').

card_in_set('stoic champion', 'LGN').
card_original_type('stoic champion'/'LGN', 'Creature — Soldier').
card_original_text('stoic champion'/'LGN', 'Whenever a player cycles a card, Stoic Champion gets +2/+2 until end of turn.').
card_first_print('stoic champion', 'LGN').
card_image_name('stoic champion'/'LGN', 'stoic champion').
card_uid('stoic champion'/'LGN', 'LGN:Stoic Champion:stoic champion').
card_rarity('stoic champion'/'LGN', 'Uncommon').
card_artist('stoic champion'/'LGN', 'Greg Hildebrandt').
card_number('stoic champion'/'LGN', '21').
card_flavor_text('stoic champion'/'LGN', 'His outer calm belies his inner fury.').
card_multiverse_id('stoic champion'/'LGN', '42265').

card_in_set('stonewood invoker', 'LGN').
card_original_type('stonewood invoker'/'LGN', 'Creature — Elf Mutant').
card_original_text('stonewood invoker'/'LGN', '{7}{G}: Stonewood Invoker gets +5/+5 until end of turn.').
card_first_print('stonewood invoker', 'LGN').
card_image_name('stonewood invoker'/'LGN', 'stonewood invoker').
card_uid('stonewood invoker'/'LGN', 'LGN:Stonewood Invoker:stonewood invoker').
card_rarity('stonewood invoker'/'LGN', 'Common').
card_artist('stonewood invoker'/'LGN', 'Eric Peterson').
card_number('stonewood invoker'/'LGN', '139').
card_flavor_text('stonewood invoker'/'LGN', 'The Mirari pulses in his veins.').
card_multiverse_id('stonewood invoker'/'LGN', '43534').

card_in_set('sunstrike legionnaire', 'LGN').
card_original_type('sunstrike legionnaire'/'LGN', 'Creature — Soldier').
card_original_text('sunstrike legionnaire'/'LGN', 'Sunstrike Legionnaire doesn\'t untap during your untap step.\nWhenever another creature comes into play, untap Sunstrike Legionnaire.\n{T}: Tap target creature with converted mana cost 3 or less.').
card_first_print('sunstrike legionnaire', 'LGN').
card_image_name('sunstrike legionnaire'/'LGN', 'sunstrike legionnaire').
card_uid('sunstrike legionnaire'/'LGN', 'LGN:Sunstrike Legionnaire:sunstrike legionnaire').
card_rarity('sunstrike legionnaire'/'LGN', 'Rare').
card_artist('sunstrike legionnaire'/'LGN', 'Mark Zug').
card_number('sunstrike legionnaire'/'LGN', '22').
card_multiverse_id('sunstrike legionnaire'/'LGN', '44328').

card_in_set('swooping talon', 'LGN').
card_original_type('swooping talon'/'LGN', 'Creature — Bird Soldier').
card_original_text('swooping talon'/'LGN', 'Flying\n{1}: Swooping Talon loses flying until end of turn.\nProvoke (When this attacks, you may have target creature defending player controls untap and block it if able.)').
card_first_print('swooping talon', 'LGN').
card_image_name('swooping talon'/'LGN', 'swooping talon').
card_uid('swooping talon'/'LGN', 'LGN:Swooping Talon:swooping talon').
card_rarity('swooping talon'/'LGN', 'Uncommon').
card_artist('swooping talon'/'LGN', 'Mark Zug').
card_number('swooping talon'/'LGN', '23').
card_multiverse_id('swooping talon'/'LGN', '45126').

card_in_set('synapse sliver', 'LGN').
card_original_type('synapse sliver'/'LGN', 'Creature — Sliver').
card_original_text('synapse sliver'/'LGN', 'Whenever a Sliver deals combat damage to a player, its controller may draw a card.').
card_first_print('synapse sliver', 'LGN').
card_image_name('synapse sliver'/'LGN', 'synapse sliver').
card_uid('synapse sliver'/'LGN', 'LGN:Synapse Sliver:synapse sliver').
card_rarity('synapse sliver'/'LGN', 'Rare').
card_artist('synapse sliver'/'LGN', 'Thomas M. Baxa').
card_number('synapse sliver'/'LGN', '53').
card_flavor_text('synapse sliver'/'LGN', '\"Species XR17 feeds upon the mental energies of its victims. This explains why the goblins remain unaffected.\"\n—Riptide Project researcher').
card_multiverse_id('synapse sliver'/'LGN', '42026').

card_in_set('timberwatch elf', 'LGN').
card_original_type('timberwatch elf'/'LGN', 'Creature — Elf').
card_original_text('timberwatch elf'/'LGN', '{T}: Target creature gets +X/+X until end of turn, where X is the number of Elves in play.').
card_first_print('timberwatch elf', 'LGN').
card_image_name('timberwatch elf'/'LGN', 'timberwatch elf').
card_uid('timberwatch elf'/'LGN', 'LGN:Timberwatch Elf:timberwatch elf').
card_rarity('timberwatch elf'/'LGN', 'Common').
card_artist('timberwatch elf'/'LGN', 'Dave Dorman').
card_number('timberwatch elf'/'LGN', '140').
card_flavor_text('timberwatch elf'/'LGN', 'Even through the Mirari\'s voice, the elves still hear the call of their kinship.').
card_multiverse_id('timberwatch elf'/'LGN', '45121').

card_in_set('totem speaker', 'LGN').
card_original_type('totem speaker'/'LGN', 'Creature — Elf Druid').
card_original_text('totem speaker'/'LGN', 'Whenever a Beast comes into play, you may gain 3 life.').
card_first_print('totem speaker', 'LGN').
card_image_name('totem speaker'/'LGN', 'totem speaker').
card_uid('totem speaker'/'LGN', 'LGN:Totem Speaker:totem speaker').
card_rarity('totem speaker'/'LGN', 'Uncommon').
card_artist('totem speaker'/'LGN', 'Darrell Riche').
card_number('totem speaker'/'LGN', '141').
card_flavor_text('totem speaker'/'LGN', '\"Elves and beasts are tied together, and I am the knot that binds them.\"').
card_multiverse_id('totem speaker'/'LGN', '45124').

card_in_set('toxin sliver', 'LGN').
card_original_type('toxin sliver'/'LGN', 'Creature — Sliver').
card_original_text('toxin sliver'/'LGN', 'Whenever a Sliver deals combat damage to a creature, destroy that creature. It can\'t be regenerated.').
card_first_print('toxin sliver', 'LGN').
card_image_name('toxin sliver'/'LGN', 'toxin sliver').
card_uid('toxin sliver'/'LGN', 'LGN:Toxin Sliver:toxin sliver').
card_rarity('toxin sliver'/'LGN', 'Rare').
card_artist('toxin sliver'/'LGN', 'Lars Grant-West').
card_number('toxin sliver'/'LGN', '84').
card_flavor_text('toxin sliver'/'LGN', 'It doesn\'t need to use its venom—it just needs you to know it can.').
card_multiverse_id('toxin sliver'/'LGN', '42038').

card_in_set('tribal forcemage', 'LGN').
card_original_type('tribal forcemage'/'LGN', 'Creature — Elf Wizard').
card_original_text('tribal forcemage'/'LGN', 'Morph {1}{G} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Tribal Forcemage is turned face up, creatures of the type of your choice get +2/+2 and gain trample until end of turn.').
card_first_print('tribal forcemage', 'LGN').
card_image_name('tribal forcemage'/'LGN', 'tribal forcemage').
card_uid('tribal forcemage'/'LGN', 'LGN:Tribal Forcemage:tribal forcemage').
card_rarity('tribal forcemage'/'LGN', 'Rare').
card_artist('tribal forcemage'/'LGN', 'Greg Staples').
card_number('tribal forcemage'/'LGN', '142').
card_multiverse_id('tribal forcemage'/'LGN', '42170').

card_in_set('unstable hulk', 'LGN').
card_original_type('unstable hulk'/'LGN', 'Creature — Goblin Mutant').
card_original_text('unstable hulk'/'LGN', 'Morph {3}{R}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Unstable Hulk is turned face up, it gets +6/+6 and gains trample until end of turn. You skip your next turn.').
card_first_print('unstable hulk', 'LGN').
card_image_name('unstable hulk'/'LGN', 'unstable hulk').
card_uid('unstable hulk'/'LGN', 'LGN:Unstable Hulk:unstable hulk').
card_rarity('unstable hulk'/'LGN', 'Rare').
card_artist('unstable hulk'/'LGN', 'Ron Spencer').
card_number('unstable hulk'/'LGN', '115').
card_multiverse_id('unstable hulk'/'LGN', '44405').

card_in_set('vexing beetle', 'LGN').
card_original_type('vexing beetle'/'LGN', 'Creature — Insect').
card_original_text('vexing beetle'/'LGN', 'Vexing Beetle can\'t be countered.\nVexing Beetle gets +3/+3 as long as no opponent controls a creature.').
card_first_print('vexing beetle', 'LGN').
card_image_name('vexing beetle'/'LGN', 'vexing beetle').
card_uid('vexing beetle'/'LGN', 'LGN:Vexing Beetle:vexing beetle').
card_rarity('vexing beetle'/'LGN', 'Rare').
card_artist('vexing beetle'/'LGN', 'Matt Thompson').
card_number('vexing beetle'/'LGN', '143').
card_flavor_text('vexing beetle'/'LGN', 'As the Mirari\'s mutating effects grew out of control, centaurs and druids fled—but insects swarmed closer.').
card_multiverse_id('vexing beetle'/'LGN', '44327').

card_in_set('vile deacon', 'LGN').
card_original_type('vile deacon'/'LGN', 'Creature — Cleric').
card_original_text('vile deacon'/'LGN', 'Whenever Vile Deacon attacks, it gets +X/+X until end of turn, where X is the number of Clerics in play.').
card_first_print('vile deacon', 'LGN').
card_image_name('vile deacon'/'LGN', 'vile deacon').
card_uid('vile deacon'/'LGN', 'LGN:Vile Deacon:vile deacon').
card_rarity('vile deacon'/'LGN', 'Common').
card_artist('vile deacon'/'LGN', 'Matthew D. Wilson').
card_number('vile deacon'/'LGN', '85').
card_flavor_text('vile deacon'/'LGN', '\"The Cabal and the Order really aren\'t that different. After all, we are both empowered by faith.\"').
card_multiverse_id('vile deacon'/'LGN', '45150').

card_in_set('voidmage apprentice', 'LGN').
card_original_type('voidmage apprentice'/'LGN', 'Creature — Wizard').
card_original_text('voidmage apprentice'/'LGN', 'Morph {2}{U}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Voidmage Apprentice is turned face up, counter target spell.').
card_first_print('voidmage apprentice', 'LGN').
card_image_name('voidmage apprentice'/'LGN', 'voidmage apprentice').
card_uid('voidmage apprentice'/'LGN', 'LGN:Voidmage Apprentice:voidmage apprentice').
card_rarity('voidmage apprentice'/'LGN', 'Common').
card_artist('voidmage apprentice'/'LGN', 'Jim Nelson').
card_number('voidmage apprentice'/'LGN', '54').
card_multiverse_id('voidmage apprentice'/'LGN', '42069').

card_in_set('wall of deceit', 'LGN').
card_original_type('wall of deceit'/'LGN', 'Creature — Wall').
card_original_text('wall of deceit'/'LGN', '(Walls can\'t attack.)\n{3}: Turn Wall of Deceit face down.\nMorph {U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_first_print('wall of deceit', 'LGN').
card_image_name('wall of deceit'/'LGN', 'wall of deceit').
card_uid('wall of deceit'/'LGN', 'LGN:Wall of Deceit:wall of deceit').
card_rarity('wall of deceit'/'LGN', 'Uncommon').
card_artist('wall of deceit'/'LGN', 'John Avon').
card_number('wall of deceit'/'LGN', '55').
card_multiverse_id('wall of deceit'/'LGN', '39850').

card_in_set('wall of hope', 'LGN').
card_original_type('wall of hope'/'LGN', 'Creature — Wall').
card_original_text('wall of hope'/'LGN', '(Walls can\'t attack.)\nWhenever Wall of Hope is dealt damage, you gain that much life.').
card_first_print('wall of hope', 'LGN').
card_image_name('wall of hope'/'LGN', 'wall of hope').
card_uid('wall of hope'/'LGN', 'LGN:Wall of Hope:wall of hope').
card_rarity('wall of hope'/'LGN', 'Common').
card_artist('wall of hope'/'LGN', 'David Martin').
card_number('wall of hope'/'LGN', '24').
card_flavor_text('wall of hope'/'LGN', '\"What cage would you rather be inside of than out?\"\n—Daru riddle').
card_multiverse_id('wall of hope'/'LGN', '44324').

card_in_set('warbreak trumpeter', 'LGN').
card_original_type('warbreak trumpeter'/'LGN', 'Creature — Goblin').
card_original_text('warbreak trumpeter'/'LGN', 'Morph {X}{X}{R} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Warbreak Trumpeter is turned face up, put X 1/1 red Goblin creature tokens into play.').
card_first_print('warbreak trumpeter', 'LGN').
card_image_name('warbreak trumpeter'/'LGN', 'warbreak trumpeter').
card_uid('warbreak trumpeter'/'LGN', 'LGN:Warbreak Trumpeter:warbreak trumpeter').
card_rarity('warbreak trumpeter'/'LGN', 'Uncommon').
card_artist('warbreak trumpeter'/'LGN', 'Dany Orizio').
card_number('warbreak trumpeter'/'LGN', '116').
card_multiverse_id('warbreak trumpeter'/'LGN', '45130').

card_in_set('ward sliver', 'LGN').
card_original_type('ward sliver'/'LGN', 'Creature — Sliver').
card_original_text('ward sliver'/'LGN', 'As Ward Sliver comes into play, choose a color.\nAll Slivers have protection from the chosen color.').
card_first_print('ward sliver', 'LGN').
card_image_name('ward sliver'/'LGN', 'ward sliver').
card_uid('ward sliver'/'LGN', 'LGN:Ward Sliver:ward sliver').
card_rarity('ward sliver'/'LGN', 'Uncommon').
card_artist('ward sliver'/'LGN', 'Pete Venters').
card_number('ward sliver'/'LGN', '25').
card_flavor_text('ward sliver'/'LGN', 'The first wave of slivers perished from the Riptide wizards\' magic. The second wave shrugged off their spells like water.').
card_multiverse_id('ward sliver'/'LGN', '44296').

card_in_set('warped researcher', 'LGN').
card_original_type('warped researcher'/'LGN', 'Creature — Wizard Mutant').
card_original_text('warped researcher'/'LGN', 'Whenever a player cycles a card, Warped Researcher gains flying until end of turn and can\'t be the target of spells or abilities this turn.').
card_first_print('warped researcher', 'LGN').
card_image_name('warped researcher'/'LGN', 'warped researcher').
card_uid('warped researcher'/'LGN', 'LGN:Warped Researcher:warped researcher').
card_rarity('warped researcher'/'LGN', 'Uncommon').
card_artist('warped researcher'/'LGN', 'rk post').
card_number('warped researcher'/'LGN', '56').
card_flavor_text('warped researcher'/'LGN', 'New insights yield new senses.').
card_multiverse_id('warped researcher'/'LGN', '44274').

card_in_set('weaver of lies', 'LGN').
card_original_type('weaver of lies'/'LGN', 'Creature — Beast').
card_original_text('weaver of lies'/'LGN', 'Morph {4}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Weaver of Lies is turned face up, turn any number of target creatures with morph other than Weaver of Lies face down.').
card_first_print('weaver of lies', 'LGN').
card_image_name('weaver of lies'/'LGN', 'weaver of lies').
card_uid('weaver of lies'/'LGN', 'LGN:Weaver of Lies:weaver of lies').
card_rarity('weaver of lies'/'LGN', 'Rare').
card_artist('weaver of lies'/'LGN', 'Luca Zontini').
card_number('weaver of lies'/'LGN', '57').
card_multiverse_id('weaver of lies'/'LGN', '42156').

card_in_set('whipgrass entangler', 'LGN').
card_original_type('whipgrass entangler'/'LGN', 'Creature — Cleric').
card_original_text('whipgrass entangler'/'LGN', '{1}{W}: Until end of turn, target creature gains \"This creature can\'t attack or block unless its controller pays {1} for each Cleric in play. (This cost is paid as attackers or blockers are declared.)\"').
card_first_print('whipgrass entangler', 'LGN').
card_image_name('whipgrass entangler'/'LGN', 'whipgrass entangler').
card_uid('whipgrass entangler'/'LGN', 'LGN:Whipgrass Entangler:whipgrass entangler').
card_rarity('whipgrass entangler'/'LGN', 'Common').
card_artist('whipgrass entangler'/'LGN', 'Ben Thompson').
card_number('whipgrass entangler'/'LGN', '26').
card_flavor_text('whipgrass entangler'/'LGN', '\"Now that I have your attention, perhaps I can tell you of the Order.\"').
card_multiverse_id('whipgrass entangler'/'LGN', '45122').

card_in_set('white knight', 'LGN').
card_original_type('white knight'/'LGN', 'Creature — Knight').
card_original_text('white knight'/'LGN', 'First strike, protection from black').
card_image_name('white knight'/'LGN', 'white knight').
card_uid('white knight'/'LGN', 'LGN:White Knight:white knight').
card_rarity('white knight'/'LGN', 'Uncommon').
card_artist('white knight'/'LGN', 'Edward P. Beard, Jr.').
card_number('white knight'/'LGN', '27').
card_flavor_text('white knight'/'LGN', 'In the wretched depths of the Grand Coliseum, his soul shines like a single torch blazing in the night.').
card_multiverse_id('white knight'/'LGN', '44212').

card_in_set('willbender', 'LGN').
card_original_type('willbender'/'LGN', 'Creature — Wizard').
card_original_text('willbender'/'LGN', 'Morph {1}{U} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Willbender is turned face up, change the target of target spell or ability with a single target.').
card_image_name('willbender'/'LGN', 'willbender').
card_uid('willbender'/'LGN', 'LGN:Willbender:willbender').
card_rarity('willbender'/'LGN', 'Uncommon').
card_artist('willbender'/'LGN', 'Eric Peterson').
card_number('willbender'/'LGN', '58').
card_multiverse_id('willbender'/'LGN', '42054').

card_in_set('windborn muse', 'LGN').
card_original_type('windborn muse'/'LGN', 'Creature — Spirit').
card_original_text('windborn muse'/'LGN', 'Flying\nCreatures can\'t attack you unless their controller pays {2} for each creature attacking you. (This cost is paid as attackers are declared.)').
card_first_print('windborn muse', 'LGN').
card_image_name('windborn muse'/'LGN', 'windborn muse').
card_uid('windborn muse'/'LGN', 'LGN:Windborn Muse:windborn muse').
card_rarity('windborn muse'/'LGN', 'Rare').
card_artist('windborn muse'/'LGN', 'Adam Rex').
card_number('windborn muse'/'LGN', '28').
card_flavor_text('windborn muse'/'LGN', '\"Her voice is justice, clear and relentless.\"\n—Akroma, angelic avenger').
card_multiverse_id('windborn muse'/'LGN', '44837').

card_in_set('wingbeat warrior', 'LGN').
card_original_type('wingbeat warrior'/'LGN', 'Creature — Bird Soldier').
card_original_text('wingbeat warrior'/'LGN', 'Flying\nMorph {2}{W} (You may play this face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Wingbeat Warrior is turned face up, target creature gains first strike until end of turn.').
card_first_print('wingbeat warrior', 'LGN').
card_image_name('wingbeat warrior'/'LGN', 'wingbeat warrior').
card_uid('wingbeat warrior'/'LGN', 'LGN:Wingbeat Warrior:wingbeat warrior').
card_rarity('wingbeat warrior'/'LGN', 'Common').
card_artist('wingbeat warrior'/'LGN', 'Kev Walker').
card_number('wingbeat warrior'/'LGN', '29').
card_multiverse_id('wingbeat warrior'/'LGN', '42058').

card_in_set('wirewood channeler', 'LGN').
card_original_type('wirewood channeler'/'LGN', 'Creature — Elf').
card_original_text('wirewood channeler'/'LGN', '{T}: Add X mana of any one color to your mana pool, where X is the number of Elves in play.').
card_first_print('wirewood channeler', 'LGN').
card_image_name('wirewood channeler'/'LGN', 'wirewood channeler').
card_uid('wirewood channeler'/'LGN', 'LGN:Wirewood Channeler:wirewood channeler').
card_rarity('wirewood channeler'/'LGN', 'Uncommon').
card_artist('wirewood channeler'/'LGN', 'Alan Pollack').
card_number('wirewood channeler'/'LGN', '144').
card_flavor_text('wirewood channeler'/'LGN', '\"Your words are meaningless. The rustling of leaves is the only language that makes any sense.\"').
card_multiverse_id('wirewood channeler'/'LGN', '35299').

card_in_set('wirewood hivemaster', 'LGN').
card_original_type('wirewood hivemaster'/'LGN', 'Creature — Elf').
card_original_text('wirewood hivemaster'/'LGN', 'Whenever another nontoken Elf comes into play, you may put a 1/1 green Insect creature token into play.').
card_first_print('wirewood hivemaster', 'LGN').
card_image_name('wirewood hivemaster'/'LGN', 'wirewood hivemaster').
card_uid('wirewood hivemaster'/'LGN', 'LGN:Wirewood Hivemaster:wirewood hivemaster').
card_rarity('wirewood hivemaster'/'LGN', 'Uncommon').
card_artist('wirewood hivemaster'/'LGN', 'Darrell Riche').
card_number('wirewood hivemaster'/'LGN', '145').
card_flavor_text('wirewood hivemaster'/'LGN', '\"Most insects have been drawn to the Mirari. But all that remain in Wirewood are under my care.\"').
card_multiverse_id('wirewood hivemaster'/'LGN', '43794').

card_in_set('withered wretch', 'LGN').
card_original_type('withered wretch'/'LGN', 'Creature — Zombie Cleric').
card_original_text('withered wretch'/'LGN', '{1}: Remove target card in a graveyard from the game.').
card_image_name('withered wretch'/'LGN', 'withered wretch').
card_uid('withered wretch'/'LGN', 'LGN:Withered Wretch:withered wretch').
card_rarity('withered wretch'/'LGN', 'Uncommon').
card_artist('withered wretch'/'LGN', 'Tim Hildebrandt').
card_number('withered wretch'/'LGN', '86').
card_flavor_text('withered wretch'/'LGN', 'Once it consecrated the dead. Now it desecrates them.').
card_multiverse_id('withered wretch'/'LGN', '46510').

card_in_set('zombie brute', 'LGN').
card_original_type('zombie brute'/'LGN', 'Creature — Zombie').
card_original_text('zombie brute'/'LGN', 'Amplify 1 (As this card comes into play, put a +1/+1 counter on it for each Zombie card you reveal in your hand.)\nTrample').
card_first_print('zombie brute', 'LGN').
card_image_name('zombie brute'/'LGN', 'zombie brute').
card_uid('zombie brute'/'LGN', 'LGN:Zombie Brute:zombie brute').
card_rarity('zombie brute'/'LGN', 'Uncommon').
card_artist('zombie brute'/'LGN', 'Greg Hildebrandt').
card_number('zombie brute'/'LGN', '87').
card_flavor_text('zombie brute'/'LGN', 'Measure its determination by the screams of its prey.').
card_multiverse_id('zombie brute'/'LGN', '42270').
