% rules.pl
% Custom predicates to make querying the database easier.

card_has_type(Card, Type) :-
    card_types(Card, Types),
    member(Type, Types).
card_has_subtype(Card, Subtype) :-
    card_subtypes(Card, Subtypes),
    member(Subtype, Subtypes).
card_has_supertype(Card, Supertype) :-
    card_supertypes(Card, Supertypes),
    member(Supertype, Supertypes).

land(Card) :- card_has_type(Card, 'Land').
enchantment(Card) :- card_has_type(Card, 'Enchantment').
creature(Card) :- card_has_type(Card, 'Creature').
instant(Card) :- card_has_type(Card, 'Instant').
artifact(Card) :- card_has_type(Card, 'Artifact').
sorcery(Card) :- card_has_type(Card, 'Sorcery').
planeswalker(Card) :- card_has_type(Card, 'Planeswalker').

artifact_creature(Card) :- artifact(Card), creature(Card).
aura(Card) :- enchantment(Card), card_has_subtype(Card, 'Aura').
tribal(Card) :- card_has_type(Card, 'Tribal').
conspiracy(Card) :- card_has_type(Card, 'Conspiracy').

snow(Card) :- card_has_supertype(Card, 'Snow').
legendary(Card) :- card_has_supertype(Card, 'Legendary').

permanent(C) :-
    land(C);
    enchantment(C);
    creature(C);
    artifact(C);
    planeswalker(C).

% find pre-Sixth-Edition Interrupts
interrupt(C/S) :-
    card_original_type(C/S, 'Interrupt').

card_has_color(Card, Color) :-
    card_colors(Card, Colors),
    member(Color, Colors).

white(Card) :- card_has_color(Card, 'W').
black(Card) :- card_has_color(Card, 'B').
blue(Card) :- card_has_color(Card, 'U').
red(Card) :- card_has_color(Card, 'R').
green(Card) :- card_has_color(Card, 'G').

colorless(Card) :- 
    card_colors(Card, []).

colored(C) :-
    card_colors(C, [_|_]).
    % cards have a color if their color list is non-empty.

monocolor(C) :-
    card_colors(C, [_]).

multicolor(C) :- 
    card_colors(C, Colors),
    length(Colors, L),
    L > 1.

% border color: if a card (in a set) has a specific border color set, then use
% that; otherwise default to the set's border color
% example: Unglued lands have black borders, other cards have silver
border(C/S, Border) :-
    card_border(C/S, Border);
    set_border(S, Border).

% all possible layouts (so far)
% When adding a new set, look for possible new layouts like this:
% ?- card_layouts(Layouts), card_layout(C/S, Layout), \+ member(Layout, Layouts).
card_layouts([split, scheme, normal, flip, leveler, token, 'double-faced', plane,
              phenomenon, vanguard]).

split_card(C) :- card_layout(C, split).
scheme_card(C) :- card_layout(C, scheme).
normal_card(C) :- card_layout(C, normal).
flip_card(C) :- card_layout(C, flip).
leveler_card(C) :- card_layout(C, leveler).
token_card(C) :- card_layout(C, token).
token(C) :- token_card(C).
double_faced_card(C) :- card_layout(C, 'double-faced').
plane_card(C) :- card_layout(C, plane).
phenomenon_card(C) :- card_layout(C, phenomenon).
vanguard_card(C) :- card_layout(C, vanguard).

% rarities

common(C/S) :- card_rarity(C/S, 'Common').
uncommon(C/S) :- card_rarity(C/S, 'Uncommon').
rare(C/S) :- card_rarity(C/S, 'Rare').
mythic(C/S) :- card_rarity(C/S, 'Mythic Rare').
basic_land(C/S) :- card_rarity(C/S, 'Basic Land').
special(C/S) :- card_rarity(C/S, 'Special').

% special kinds of mana

mana_cost_contains(C, Char) :-
    card_mana_cost(C, ManaCost),
    member(Symbol, ManaCost),
    string_contains_char(Symbol, Char).

has_hybrid_mana(C) :-
    mana_cost_contains(C, '/'), \+ mana_cost_contains(C, 'P').
    % phyrexian mana doesn't count as hybrid, right?

has_phyrexian_mana(C) :-
    mana_cost_contains(C, 'P').

% has_snow_mana(C) :- mana_cost_contains(C, 'S'). % doesn't exist

card_text_contains(C, Text) :-
    card_text(C, CardText),
    string_contains_text(CardText, Text).

card_text_contains_tap_symbol(C) :- card_text_contains(C, '{T}').
card_text_contains_snow_mana(C) :- card_text_contains(C, '{S}').
card_text_contains_phyrexian_mana(C) :- card_text_contains(C, 'P}'). % sic
card_text_contains_chaos_mana(C) :- card_text_contains(C, '{C}').
card_text_contains_untap_symbol(C) :- card_text_contains(C, '{Q}').

card_text_contains_hybrid_mana(C) :-
    member(Symbol, ['/W}', '/U}', '/R}', '/G}', '/B}']),
    card_text_contains(C, Symbol).

% sets

core_set(Set) :- set_type(Set, core).
un_set(Set) :- set_type(Set, un). % useful for filtering out bogus cards
promo_set(Set) :- set_type(Set, promo).
expansion_set(Set) :- set_type(Set, expansion).

card_rulings(C, Rulings) :-
    findall(date(Date)/text(Text), card_ruling(C, Date, Text), Rulings).

other_side(Card1, Card2) :-
    card_sides(Card1, Card2);
    card_sides(Card2, Card1).

card_reprint(C/S) :- card_in_set(C, S), \+ card_first_print(C, S).
card_first_print(C/S) :- card_first_print(C, S).  % alias

% ----- helper predicates -----

% will succeed if the string contains the given char. both String and Char can
% be atoms.
string_contains_char(String, Char) :-
    string_code(_, String, Char).

% will succeed if SubString is found inside String.
string_contains_text(String, SubString) :-
    sub_string(String, _, _, _, SubString).

% TODO: string_starts_with, string_ends_with

% rough template for a rule that produces a booster for a set
% XXX should really follow the 'booster' attribute in JSON
booster(Set, Cards) :-
    findall(Card/rare, (mythic(Card/Set); rare(Card/Set)), Rares),
    findall(Card/uncommon, (uncommon(Card/Set)), Uncommons),
    findall(Card/common, (common(Card/Set)), Commons),
    random_member(R, Rares),
    random_member(U1, Uncommons),
    random_member(U2, Uncommons),
    random_member(U3, Uncommons),
    findall(C, (between(1, 11, _), random_member(C, Commons)), Cs),
    append([R, U1, U2, U3], Cs, Cards).

% ----- aggregators -----

% NOTE: this area is highly experimental and will likely change over time,
% until it has become more stable.

% count the number of results produced by a query.
counts(F, C) :-
    F =.. [_|T],
    findall(T, F, X),
    length(X, C).

print_list(List) :-
    print_term(List, [write_options([spacing(next_argument), numbervars(true), 
                                     quoted(true), portray(true)])]).

% show all results, pretty-printed.
all(F) :-
    F =.. [_|T],
    findall(T, F, All),
    print_list(All).

% TODO: can we write operators for sorting, grouping, unique, max, etc?

% findmax and friends are postfix operators, and can only take one parameter
% (as far as I know :-). I chose to make this a list containing the values
% needed to make the aggregate call.

findmax(Query, [Value, Witness, Max]) :-
    aggregate_all(max(Value, Witness), Query, Max).
    % may succeed multiple times
findmax(Query, [Value, Max]) :-
    aggregate_all(max(Value), Query, Max), !.

unique(Query, [Criterion, Results]) :-
    findall(Criterion, Query, RawResults),
    sort(RawResults, Results).
% ^ efficient IFF used properly :)

% main aggregator operator: >>

:- op(1200, yfx, '>>').
'>>'(Query, all) :-
    all(Query), !.
'>>'(Query, findmax(Value, Max)) :-
    findmax(Query, [Value, Max]), !.
'>>'(Query, findmax(Value, Witness, Max)) :-
    findmax(Query, [Value, Witness, Max]).
'>>'(Query, unique(Criterion, Results)) :-
    unique(Query, [Criterion, Results]), !.
'>>'(Query, count(N)) :-
    counts(Query, N).
'>>'(Query, findall(Template, Results)) :-
    findall(Template, Query, Results).

