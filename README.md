### mtg-prolog ###

This is mtg-prolog, a Magic the Gathering card database implemented in
Prolog.

### Requirements ###

- Python 3.x (I'm using 3.4)
- SWI-Prolog 7.x (I'm using 7.2)
- Directory containing JSON files as produced by [mtgjson](http://mtgjson.com/).

### Generating the Prolog files ###

Check out the project, then run `./generate.py <DIR>`, where `<DIR>` is
a directory containing the **individual set files** from mtgjson. See:
[http://mtgjson.com/#individualSets]. (Download `AllSetFiles-x.zip`,
unpack, move the files to a directory of your choice, then point the
aforementioned `generate.py` script to that directory.)

If all goes well, the script should output a number of filenames for
sets, cards, and rulings, and exit without errors.

**NOTE**: As I write this, it is not strictly necessary to generate the
Prolog files, since they're all in the repository. But I might remove
them in the future, if the repo is getting too big.

### Querying the database ###

Assuming your SWI-Prolog executable is called `swipl`, run:

`swipl all.pl`

which loads all the generated files, then presents you with the usual
Prolog prompt. You can enter queries here using a number of custom
predicates. (**XXX Add documentation**)

If loading takes too long, run the `compile` script, which produces an
executable `mtgdb` that should start up faster. (Note that this
executable still requires SWI-Prolog, it is not standalone and not
suitable for distribution.)