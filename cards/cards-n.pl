% Card-specific data

% Found in: HOP
card_name('naar isle', 'Naar Isle').
card_type('naar isle', 'Plane — Wildfire').
card_types('naar isle', ['Plane']).
card_subtypes('naar isle', ['Wildfire']).
card_colors('naar isle', []).
card_text('naar isle', 'At the beginning of your upkeep, put a flame counter on Naar Isle, then Naar Isle deals damage to you equal to the number of flame counters on it.\nWhenever you roll {C}, Naar Isle deals 3 damage to target player.').
card_layout('naar isle', 'plane').

% Found in: CON, DDH
card_name('nacatl hunt-pride', 'Nacatl Hunt-Pride').
card_type('nacatl hunt-pride', 'Creature — Cat Warrior').
card_types('nacatl hunt-pride', ['Creature']).
card_subtypes('nacatl hunt-pride', ['Cat', 'Warrior']).
card_colors('nacatl hunt-pride', ['W']).
card_text('nacatl hunt-pride', 'Vigilance\n{R}, {T}: Target creature can\'t block this turn.\n{G}, {T}: Target creature blocks this turn if able.').
card_mana_cost('nacatl hunt-pride', ['5', 'W']).
card_cmc('nacatl hunt-pride', 6).
card_layout('nacatl hunt-pride', 'normal').
card_power('nacatl hunt-pride', 5).
card_toughness('nacatl hunt-pride', 4).

% Found in: CON
card_name('nacatl outlander', 'Nacatl Outlander').
card_type('nacatl outlander', 'Creature — Cat Scout').
card_types('nacatl outlander', ['Creature']).
card_subtypes('nacatl outlander', ['Cat', 'Scout']).
card_colors('nacatl outlander', ['R', 'G']).
card_text('nacatl outlander', 'Protection from blue').
card_mana_cost('nacatl outlander', ['R', 'G']).
card_cmc('nacatl outlander', 2).
card_layout('nacatl outlander', 'normal').
card_power('nacatl outlander', 2).
card_toughness('nacatl outlander', 2).

% Found in: CON
card_name('nacatl savage', 'Nacatl Savage').
card_type('nacatl savage', 'Creature — Cat Warrior').
card_types('nacatl savage', ['Creature']).
card_subtypes('nacatl savage', ['Cat', 'Warrior']).
card_colors('nacatl savage', ['G']).
card_text('nacatl savage', 'Protection from artifacts').
card_mana_cost('nacatl savage', ['1', 'G']).
card_cmc('nacatl savage', 2).
card_layout('nacatl savage', 'normal').
card_power('nacatl savage', 2).
card_toughness('nacatl savage', 1).

% Found in: FUT
card_name('nacatl war-pride', 'Nacatl War-Pride').
card_type('nacatl war-pride', 'Creature — Cat Warrior').
card_types('nacatl war-pride', ['Creature']).
card_subtypes('nacatl war-pride', ['Cat', 'Warrior']).
card_colors('nacatl war-pride', ['G']).
card_text('nacatl war-pride', 'Nacatl War-Pride must be blocked by exactly one creature if able.\nWhenever Nacatl War-Pride attacks, put X tokens that are copies of Nacatl War-Pride onto the battlefield tapped and attacking, where X is the number of creatures defending player controls. Exile the tokens at the beginning of the next end step.').
card_mana_cost('nacatl war-pride', ['3', 'G', 'G', 'G']).
card_cmc('nacatl war-pride', 6).
card_layout('nacatl war-pride', 'normal').
card_power('nacatl war-pride', 3).
card_toughness('nacatl war-pride', 3).

% Found in: ICE
card_name('nacre talisman', 'Nacre Talisman').
card_type('nacre talisman', 'Artifact').
card_types('nacre talisman', ['Artifact']).
card_subtypes('nacre talisman', []).
card_colors('nacre talisman', []).
card_text('nacre talisman', 'Whenever a player casts a white spell, you may pay {3}. If you do, untap target permanent.').
card_mana_cost('nacre talisman', ['2']).
card_cmc('nacre talisman', 2).
card_layout('nacre talisman', 'normal').

% Found in: 4ED, ARN
card_name('nafs asp', 'Nafs Asp').
card_type('nafs asp', 'Creature — Snake').
card_types('nafs asp', ['Creature']).
card_subtypes('nafs asp', ['Snake']).
card_colors('nafs asp', ['G']).
card_text('nafs asp', 'Whenever Nafs Asp deals damage to a player, that player loses 1 life at the beginning of his or her next draw step unless he or she pays {1} before that draw step.').
card_mana_cost('nafs asp', ['G']).
card_cmc('nafs asp', 1).
card_layout('nafs asp', 'normal').
card_power('nafs asp', 1).
card_toughness('nafs asp', 1).

% Found in: CHK
card_name('nagao, bound by honor', 'Nagao, Bound by Honor').
card_type('nagao, bound by honor', 'Legendary Creature — Human Samurai').
card_types('nagao, bound by honor', ['Creature']).
card_subtypes('nagao, bound by honor', ['Human', 'Samurai']).
card_supertypes('nagao, bound by honor', ['Legendary']).
card_colors('nagao, bound by honor', ['W']).
card_text('nagao, bound by honor', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever Nagao, Bound by Honor attacks, Samurai creatures you control get +1/+1 until end of turn.').
card_mana_cost('nagao, bound by honor', ['3', 'W']).
card_cmc('nagao, bound by honor', 4).
card_layout('nagao, bound by honor', 'normal').
card_power('nagao, bound by honor', 3).
card_toughness('nagao, bound by honor', 3).

% Found in: C14
card_name('nahiri, the lithomancer', 'Nahiri, the Lithomancer').
card_type('nahiri, the lithomancer', 'Planeswalker — Nahiri').
card_types('nahiri, the lithomancer', ['Planeswalker']).
card_subtypes('nahiri, the lithomancer', ['Nahiri']).
card_colors('nahiri, the lithomancer', ['W']).
card_text('nahiri, the lithomancer', '+2: Put a 1/1 white Kor Soldier creature token onto the battlefield. You may attach an Equipment you control to it.\n−2: You may put an Equipment card from your hand or graveyard onto the battlefield.\n−10: Put a colorless Equipment artifact token named Stoneforged Blade onto the battlefield. It has indestructible, \"Equipped creature gets +5/+5 and has double strike,\" and equip {0}.\nNahiri, the Lithomancer can be your commander.').
card_mana_cost('nahiri, the lithomancer', ['3', 'W', 'W']).
card_cmc('nahiri, the lithomancer', 5).
card_layout('nahiri, the lithomancer', 'normal').
card_loyalty('nahiri, the lithomancer', 3).

% Found in: PCY
card_name('nakaya shade', 'Nakaya Shade').
card_type('nakaya shade', 'Creature — Shade').
card_types('nakaya shade', ['Creature']).
card_subtypes('nakaya shade', ['Shade']).
card_colors('nakaya shade', ['B']).
card_text('nakaya shade', '{B}: Nakaya Shade gets +1/+1 until end of turn unless any player pays {2}.').
card_mana_cost('nakaya shade', ['1', 'B']).
card_cmc('nakaya shade', 2).
card_layout('nakaya shade', 'normal').
card_power('nakaya shade', 1).
card_toughness('nakaya shade', 1).

% Found in: ICE, ME4
card_name('naked singularity', 'Naked Singularity').
card_type('naked singularity', 'Artifact').
card_types('naked singularity', ['Artifact']).
card_subtypes('naked singularity', []).
card_colors('naked singularity', []).
card_text('naked singularity', 'Cumulative upkeep {3} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nIf tapped for mana, Plains produce {R}, Islands produce {G}, Swamps produce {W}, Mountains produce {U}, and Forests produce {B} instead of any other type.').
card_mana_cost('naked singularity', ['5']).
card_cmc('naked singularity', 5).
card_layout('naked singularity', 'normal').

% Found in: pDRC, pMEI
card_name('nalathni dragon', 'Nalathni Dragon').
card_type('nalathni dragon', 'Creature — Dragon').
card_types('nalathni dragon', ['Creature']).
card_subtypes('nalathni dragon', ['Dragon']).
card_colors('nalathni dragon', ['R']).
card_text('nalathni dragon', 'Flying; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\n{R}: Nalathni Dragon gets +1/+0 until end of turn. If this ability has been activated four or more times this turn, sacrifice Nalathni Dragon at the beginning of the next end step.').
card_mana_cost('nalathni dragon', ['2', 'R', 'R']).
card_cmc('nalathni dragon', 4).
card_layout('nalathni dragon', 'normal').
card_power('nalathni dragon', 1).
card_toughness('nalathni dragon', 1).

% Found in: UNH
card_name('name dropping', 'Name Dropping').
card_type('name dropping', 'Enchantment').
card_types('name dropping', ['Enchantment']).
card_subtypes('name dropping', []).
card_colors('name dropping', ['G']).
card_text('name dropping', 'Gotcha Whenever an opponent says a word that\'s in the name of a card in your graveyard, you may say \"Gotcha\" If you do, return that card to your hand.').
card_mana_cost('name dropping', ['1', 'G']).
card_cmc('name dropping', 2).
card_layout('name dropping', 'normal').

% Found in: LRW, MM2, pMPR
card_name('nameless inversion', 'Nameless Inversion').
card_type('nameless inversion', 'Tribal Instant — Shapeshifter').
card_types('nameless inversion', ['Tribal', 'Instant']).
card_subtypes('nameless inversion', ['Shapeshifter']).
card_colors('nameless inversion', ['B']).
card_text('nameless inversion', 'Changeling (This card is every creature type.)\nTarget creature gets +3/-3 and loses all creature types until end of turn.').
card_mana_cost('nameless inversion', ['1', 'B']).
card_cmc('nameless inversion', 2).
card_layout('nameless inversion', 'normal').

% Found in: ONS
card_name('nameless one', 'Nameless One').
card_type('nameless one', 'Creature — Wizard Avatar').
card_types('nameless one', ['Creature']).
card_subtypes('nameless one', ['Wizard', 'Avatar']).
card_colors('nameless one', ['U']).
card_text('nameless one', 'Nameless One\'s power and toughness are each equal to the number of Wizards on the battlefield.\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('nameless one', ['3', 'U']).
card_cmc('nameless one', 4).
card_layout('nameless one', 'normal').
card_power('nameless one', '*').
card_toughness('nameless one', '*').

% Found in: DRK
card_name('nameless race', 'Nameless Race').
card_type('nameless race', 'Creature').
card_types('nameless race', ['Creature']).
card_subtypes('nameless race', []).
card_colors('nameless race', ['B']).
card_text('nameless race', 'Trample\nAs Nameless Race enters the battlefield, pay any amount of life. The amount you pay can\'t be more than the total number of white nontoken permanents your opponents control plus the total number of white cards in their graveyards.\nNameless Race\'s power and toughness are each equal to the life paid as it entered the battlefield.').
card_mana_cost('nameless race', ['3', 'B']).
card_cmc('nameless race', 4).
card_layout('nameless race', 'normal').
card_power('nameless race', '*').
card_toughness('nameless race', '*').
card_reserved('nameless race').

% Found in: TOR
card_name('nantuko blightcutter', 'Nantuko Blightcutter').
card_type('nantuko blightcutter', 'Creature — Insect Druid').
card_types('nantuko blightcutter', ['Creature']).
card_subtypes('nantuko blightcutter', ['Insect', 'Druid']).
card_colors('nantuko blightcutter', ['G']).
card_text('nantuko blightcutter', 'Protection from black\nThreshold — Nantuko Blightcutter gets +1/+1 for each black permanent your opponents control as long as seven or more cards are in your graveyard.').
card_mana_cost('nantuko blightcutter', ['2', 'G']).
card_cmc('nantuko blightcutter', 3).
card_layout('nantuko blightcutter', 'normal').
card_power('nantuko blightcutter', 2).
card_toughness('nantuko blightcutter', 2).

% Found in: TOR
card_name('nantuko calmer', 'Nantuko Calmer').
card_type('nantuko calmer', 'Creature — Insect Druid').
card_types('nantuko calmer', ['Creature']).
card_subtypes('nantuko calmer', ['Insect', 'Druid']).
card_colors('nantuko calmer', ['G']).
card_text('nantuko calmer', '{G}, {T}, Sacrifice Nantuko Calmer: Destroy target enchantment.\nThreshold — Nantuko Calmer gets +1/+1 as long as seven or more cards are in your graveyard.').
card_mana_cost('nantuko calmer', ['2', 'G', 'G']).
card_cmc('nantuko calmer', 4).
card_layout('nantuko calmer', 'normal').
card_power('nantuko calmer', 2).
card_toughness('nantuko calmer', 3).

% Found in: TOR
card_name('nantuko cultivator', 'Nantuko Cultivator').
card_type('nantuko cultivator', 'Creature — Insect Druid').
card_types('nantuko cultivator', ['Creature']).
card_subtypes('nantuko cultivator', ['Insect', 'Druid']).
card_colors('nantuko cultivator', ['G']).
card_text('nantuko cultivator', 'When Nantuko Cultivator enters the battlefield, you may discard any number of land cards. Put that many +1/+1 counters on Nantuko Cultivator and draw that many cards.').
card_mana_cost('nantuko cultivator', ['3', 'G']).
card_cmc('nantuko cultivator', 4).
card_layout('nantuko cultivator', 'normal').
card_power('nantuko cultivator', 2).
card_toughness('nantuko cultivator', 2).

% Found in: 8ED, ODY
card_name('nantuko disciple', 'Nantuko Disciple').
card_type('nantuko disciple', 'Creature — Insect Druid').
card_types('nantuko disciple', ['Creature']).
card_subtypes('nantuko disciple', ['Insect', 'Druid']).
card_colors('nantuko disciple', ['G']).
card_text('nantuko disciple', '{G}, {T}: Target creature gets +2/+2 until end of turn.').
card_mana_cost('nantuko disciple', ['3', 'G']).
card_cmc('nantuko disciple', 4).
card_layout('nantuko disciple', 'normal').
card_power('nantuko disciple', 2).
card_toughness('nantuko disciple', 2).

% Found in: ODY
card_name('nantuko elder', 'Nantuko Elder').
card_type('nantuko elder', 'Creature — Insect Druid').
card_types('nantuko elder', ['Creature']).
card_subtypes('nantuko elder', ['Insect', 'Druid']).
card_colors('nantuko elder', ['G']).
card_text('nantuko elder', '{T}: Add {1}{G} to your mana pool.').
card_mana_cost('nantuko elder', ['2', 'G']).
card_cmc('nantuko elder', 3).
card_layout('nantuko elder', 'normal').
card_power('nantuko elder', 1).
card_toughness('nantuko elder', 2).

% Found in: 10E, 9ED, CMD, ONS, ORI
card_name('nantuko husk', 'Nantuko Husk').
card_type('nantuko husk', 'Creature — Zombie Insect').
card_types('nantuko husk', ['Creature']).
card_subtypes('nantuko husk', ['Zombie', 'Insect']).
card_colors('nantuko husk', ['B']).
card_text('nantuko husk', 'Sacrifice a creature: Nantuko Husk gets +2/+2 until end of turn.').
card_mana_cost('nantuko husk', ['2', 'B']).
card_cmc('nantuko husk', 3).
card_layout('nantuko husk', 'normal').
card_power('nantuko husk', 2).
card_toughness('nantuko husk', 2).

% Found in: ODY
card_name('nantuko mentor', 'Nantuko Mentor').
card_type('nantuko mentor', 'Creature — Insect Druid').
card_types('nantuko mentor', ['Creature']).
card_subtypes('nantuko mentor', ['Insect', 'Druid']).
card_colors('nantuko mentor', ['G']).
card_text('nantuko mentor', '{2}{G}, {T}: Target creature gets +X/+X until end of turn, where X is that creature\'s power.').
card_mana_cost('nantuko mentor', ['2', 'G']).
card_cmc('nantuko mentor', 3).
card_layout('nantuko mentor', 'normal').
card_power('nantuko mentor', 1).
card_toughness('nantuko mentor', 1).

% Found in: ARC, JUD
card_name('nantuko monastery', 'Nantuko Monastery').
card_type('nantuko monastery', 'Land').
card_types('nantuko monastery', ['Land']).
card_subtypes('nantuko monastery', []).
card_colors('nantuko monastery', []).
card_text('nantuko monastery', '{T}: Add {1} to your mana pool.\nThreshold — {G}{W}: Nantuko Monastery becomes a 4/4 green and white Insect Monk creature with first strike until end of turn. It\'s still a land. Activate this ability only if seven or more cards are in your graveyard.').
card_layout('nantuko monastery', 'normal').

% Found in: C14, M11, TOR
card_name('nantuko shade', 'Nantuko Shade').
card_type('nantuko shade', 'Creature — Insect Shade').
card_types('nantuko shade', ['Creature']).
card_subtypes('nantuko shade', ['Insect', 'Shade']).
card_colors('nantuko shade', ['B']).
card_text('nantuko shade', '{B}: Nantuko Shade gets +1/+1 until end of turn.').
card_mana_cost('nantuko shade', ['B', 'B']).
card_cmc('nantuko shade', 2).
card_layout('nantuko shade', 'normal').
card_power('nantuko shade', 2).
card_toughness('nantuko shade', 1).

% Found in: MMA, TSP
card_name('nantuko shaman', 'Nantuko Shaman').
card_type('nantuko shaman', 'Creature — Insect Shaman').
card_types('nantuko shaman', ['Creature']).
card_subtypes('nantuko shaman', ['Insect', 'Shaman']).
card_colors('nantuko shaman', ['G']).
card_text('nantuko shaman', 'When Nantuko Shaman enters the battlefield, if you control no tapped lands, draw a card.\nSuspend 1—{2}{G}{G} (Rather than cast this card from your hand, you may pay {2}{G}{G} and exile it with a time counter on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('nantuko shaman', ['2', 'G']).
card_cmc('nantuko shaman', 3).
card_layout('nantuko shaman', 'normal').
card_power('nantuko shaman', 3).
card_toughness('nantuko shaman', 2).

% Found in: ODY
card_name('nantuko shrine', 'Nantuko Shrine').
card_type('nantuko shrine', 'Enchantment').
card_types('nantuko shrine', ['Enchantment']).
card_subtypes('nantuko shrine', []).
card_colors('nantuko shrine', ['G']).
card_text('nantuko shrine', 'Whenever a player casts a spell, that player puts X 1/1 green Squirrel creature tokens onto the battlefield, where X is the number of cards in all graveyards with the same name as that spell.').
card_mana_cost('nantuko shrine', ['1', 'G', 'G']).
card_cmc('nantuko shrine', 3).
card_layout('nantuko shrine', 'normal').

% Found in: JUD
card_name('nantuko tracer', 'Nantuko Tracer').
card_type('nantuko tracer', 'Creature — Insect Druid').
card_types('nantuko tracer', ['Creature']).
card_subtypes('nantuko tracer', ['Insect', 'Druid']).
card_colors('nantuko tracer', ['G']).
card_text('nantuko tracer', 'When Nantuko Tracer enters the battlefield, you may put target card from a graveyard on the bottom of its owner\'s library.').
card_mana_cost('nantuko tracer', ['1', 'G']).
card_cmc('nantuko tracer', 2).
card_layout('nantuko tracer', 'normal').
card_power('nantuko tracer', 2).
card_toughness('nantuko tracer', 1).

% Found in: LGN
card_name('nantuko vigilante', 'Nantuko Vigilante').
card_type('nantuko vigilante', 'Creature — Insect Druid Mutant').
card_types('nantuko vigilante', ['Creature']).
card_subtypes('nantuko vigilante', ['Insect', 'Druid', 'Mutant']).
card_colors('nantuko vigilante', ['G']).
card_text('nantuko vigilante', 'Morph {1}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Nantuko Vigilante is turned face up, destroy target artifact or enchantment.').
card_mana_cost('nantuko vigilante', ['3', 'G']).
card_cmc('nantuko vigilante', 4).
card_layout('nantuko vigilante', 'normal').
card_power('nantuko vigilante', 3).
card_toughness('nantuko vigilante', 2).

% Found in: TOR
card_name('narcissism', 'Narcissism').
card_type('narcissism', 'Enchantment').
card_types('narcissism', ['Enchantment']).
card_subtypes('narcissism', []).
card_colors('narcissism', ['G']).
card_text('narcissism', '{G}, Discard a card: Target creature gets +2/+2 until end of turn.\n{G}, Sacrifice Narcissism: Target creature gets +2/+2 until end of turn.').
card_mana_cost('narcissism', ['2', 'G']).
card_cmc('narcissism', 3).
card_layout('narcissism', 'normal').

% Found in: MM2, ROE
card_name('narcolepsy', 'Narcolepsy').
card_type('narcolepsy', 'Enchantment — Aura').
card_types('narcolepsy', ['Enchantment']).
card_subtypes('narcolepsy', ['Aura']).
card_colors('narcolepsy', ['U']).
card_text('narcolepsy', 'Enchant creature\nAt the beginning of each upkeep, if enchanted creature is untapped, tap it.').
card_mana_cost('narcolepsy', ['1', 'U']).
card_cmc('narcolepsy', 2).
card_layout('narcolepsy', 'normal').

% Found in: FUT, MMA
card_name('narcomoeba', 'Narcomoeba').
card_type('narcomoeba', 'Creature — Illusion').
card_types('narcomoeba', ['Creature']).
card_subtypes('narcomoeba', ['Illusion']).
card_colors('narcomoeba', ['U']).
card_text('narcomoeba', 'Flying\nWhen Narcomoeba is put into your graveyard from your library, you may put it onto the battlefield.').
card_mana_cost('narcomoeba', ['1', 'U']).
card_cmc('narcomoeba', 2).
card_layout('narcomoeba', 'normal').
card_power('narcomoeba', 1).
card_toughness('narcomoeba', 1).

% Found in: DDE, ZEN
card_name('narrow escape', 'Narrow Escape').
card_type('narrow escape', 'Instant').
card_types('narrow escape', ['Instant']).
card_subtypes('narrow escape', []).
card_colors('narrow escape', ['W']).
card_text('narrow escape', 'Return target permanent you control to its owner\'s hand. You gain 4 life.').
card_mana_cost('narrow escape', ['2', 'W']).
card_cmc('narrow escape', 3).
card_layout('narrow escape', 'normal').

% Found in: DTK
card_name('narset transcendent', 'Narset Transcendent').
card_type('narset transcendent', 'Planeswalker — Narset').
card_types('narset transcendent', ['Planeswalker']).
card_subtypes('narset transcendent', ['Narset']).
card_colors('narset transcendent', ['W', 'U']).
card_text('narset transcendent', '+1: Look at the top card of your library. If it\'s a noncreature, nonland card, you may reveal it and put it into your hand.\n−2: When you cast your next instant or sorcery spell from your hand this turn, it gains rebound.\n−9: You get an emblem with \"Your opponents can\'t cast noncreature spells.\"').
card_mana_cost('narset transcendent', ['2', 'W', 'U']).
card_cmc('narset transcendent', 4).
card_layout('narset transcendent', 'normal').
card_loyalty('narset transcendent', 6).

% Found in: KTK, pPRE
card_name('narset, enlightened master', 'Narset, Enlightened Master').
card_type('narset, enlightened master', 'Legendary Creature — Human Monk').
card_types('narset, enlightened master', ['Creature']).
card_subtypes('narset, enlightened master', ['Human', 'Monk']).
card_supertypes('narset, enlightened master', ['Legendary']).
card_colors('narset, enlightened master', ['W', 'U', 'R']).
card_text('narset, enlightened master', 'First strike, hexproof\nWhenever Narset, Enlightened Master attacks, exile the top four cards of your library. Until end of turn, you may cast noncreature cards exiled with Narset this turn without paying their mana costs.').
card_mana_cost('narset, enlightened master', ['3', 'U', 'R', 'W']).
card_cmc('narset, enlightened master', 6).
card_layout('narset, enlightened master', 'normal').
card_power('narset, enlightened master', 3).
card_toughness('narset, enlightened master', 2).

% Found in: AVR
card_name('narstad scrapper', 'Narstad Scrapper').
card_type('narstad scrapper', 'Artifact Creature — Construct').
card_types('narstad scrapper', ['Artifact', 'Creature']).
card_subtypes('narstad scrapper', ['Construct']).
card_colors('narstad scrapper', []).
card_text('narstad scrapper', '{2}: Narstad Scrapper gets +1/+0 until end of turn.').
card_mana_cost('narstad scrapper', ['5']).
card_cmc('narstad scrapper', 5).
card_layout('narstad scrapper', 'normal').
card_power('narstad scrapper', 3).
card_toughness('narstad scrapper', 3).

% Found in: HML, ME2
card_name('narwhal', 'Narwhal').
card_type('narwhal', 'Creature — Whale').
card_types('narwhal', ['Creature']).
card_subtypes('narwhal', ['Whale']).
card_colors('narwhal', ['U']).
card_text('narwhal', 'First strike, protection from red').
card_mana_cost('narwhal', ['2', 'U', 'U']).
card_cmc('narwhal', 4).
card_layout('narwhal', 'normal').
card_power('narwhal', 2).
card_toughness('narwhal', 2).
card_reserved('narwhal').

% Found in: LRW
card_name('nath of the gilt-leaf', 'Nath of the Gilt-Leaf').
card_type('nath of the gilt-leaf', 'Legendary Creature — Elf Warrior').
card_types('nath of the gilt-leaf', ['Creature']).
card_subtypes('nath of the gilt-leaf', ['Elf', 'Warrior']).
card_supertypes('nath of the gilt-leaf', ['Legendary']).
card_colors('nath of the gilt-leaf', ['B', 'G']).
card_text('nath of the gilt-leaf', 'At the beginning of your upkeep, you may have target opponent discard a card at random.\nWhenever an opponent discards a card, you may put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_mana_cost('nath of the gilt-leaf', ['3', 'B', 'G']).
card_cmc('nath of the gilt-leaf', 5).
card_layout('nath of the gilt-leaf', 'normal').
card_power('nath of the gilt-leaf', 4).
card_toughness('nath of the gilt-leaf', 4).

% Found in: LRW
card_name('nath\'s buffoon', 'Nath\'s Buffoon').
card_type('nath\'s buffoon', 'Creature — Goblin Rogue').
card_types('nath\'s buffoon', ['Creature']).
card_subtypes('nath\'s buffoon', ['Goblin', 'Rogue']).
card_colors('nath\'s buffoon', ['B']).
card_text('nath\'s buffoon', 'Protection from Elves').
card_mana_cost('nath\'s buffoon', ['1', 'B']).
card_cmc('nath\'s buffoon', 2).
card_layout('nath\'s buffoon', 'normal').
card_power('nath\'s buffoon', 1).
card_toughness('nath\'s buffoon', 1).

% Found in: LRW
card_name('nath\'s elite', 'Nath\'s Elite').
card_type('nath\'s elite', 'Creature — Elf Warrior').
card_types('nath\'s elite', ['Creature']).
card_subtypes('nath\'s elite', ['Elf', 'Warrior']).
card_colors('nath\'s elite', ['G']).
card_text('nath\'s elite', 'All creatures able to block Nath\'s Elite do so.\nWhen Nath\'s Elite enters the battlefield, clash with an opponent. If you win, put a +1/+1 counter on Nath\'s Elite. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('nath\'s elite', ['4', 'G']).
card_cmc('nath\'s elite', 5).
card_layout('nath\'s elite', 'normal').
card_power('nath\'s elite', 4).
card_toughness('nath\'s elite', 2).

% Found in: 8ED, 9ED, MMQ
card_name('natural affinity', 'Natural Affinity').
card_type('natural affinity', 'Instant').
card_types('natural affinity', ['Instant']).
card_subtypes('natural affinity', []).
card_colors('natural affinity', ['G']).
card_text('natural affinity', 'All lands become 2/2 creatures until end of turn. They\'re still lands.').
card_mana_cost('natural affinity', ['2', 'G']).
card_cmc('natural affinity', 3).
card_layout('natural affinity', 'normal').

% Found in: MIR
card_name('natural balance', 'Natural Balance').
card_type('natural balance', 'Sorcery').
card_types('natural balance', ['Sorcery']).
card_subtypes('natural balance', []).
card_colors('natural balance', ['G']).
card_text('natural balance', 'Each player who controls six or more lands chooses five lands he or she controls and sacrifices the rest. Each player who controls four or fewer lands may search his or her library for up to X basic land cards and put them onto the battlefield, where X is five minus the number of lands he or she controls. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('natural balance', ['2', 'G', 'G']).
card_cmc('natural balance', 4).
card_layout('natural balance', 'normal').
card_reserved('natural balance').

% Found in: BFZ
card_name('natural connection', 'Natural Connection').
card_type('natural connection', 'Instant').
card_types('natural connection', ['Instant']).
card_subtypes('natural connection', []).
card_colors('natural connection', ['G']).
card_text('natural connection', 'Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('natural connection', ['2', 'G']).
card_cmc('natural connection', 3).
card_layout('natural connection', 'normal').

% Found in: PLS
card_name('natural emergence', 'Natural Emergence').
card_type('natural emergence', 'Enchantment').
card_types('natural emergence', ['Enchantment']).
card_subtypes('natural emergence', []).
card_colors('natural emergence', ['R', 'G']).
card_text('natural emergence', 'When Natural Emergence enters the battlefield, return a red or green enchantment you control to its owner\'s hand.\nLands you control are 2/2 creatures with first strike. They\'re still lands.').
card_mana_cost('natural emergence', ['2', 'R', 'G']).
card_cmc('natural emergence', 4).
card_layout('natural emergence', 'normal').

% Found in: AVR
card_name('natural end', 'Natural End').
card_type('natural end', 'Instant').
card_types('natural end', ['Instant']).
card_subtypes('natural end', []).
card_colors('natural end', ['G']).
card_text('natural end', 'Destroy target artifact or enchantment. You gain 3 life.').
card_mana_cost('natural end', ['2', 'G']).
card_cmc('natural end', 3).
card_layout('natural end', 'normal').

% Found in: POR, VIS, pJGP
card_name('natural order', 'Natural Order').
card_type('natural order', 'Sorcery').
card_types('natural order', ['Sorcery']).
card_subtypes('natural order', []).
card_colors('natural order', ['G']).
card_text('natural order', 'As an additional cost to cast Natural Order, sacrifice a green creature.\nSearch your library for a green creature card and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('natural order', ['2', 'G', 'G']).
card_cmc('natural order', 4).
card_layout('natural order', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB
card_name('natural selection', 'Natural Selection').
card_type('natural selection', 'Instant').
card_types('natural selection', ['Instant']).
card_subtypes('natural selection', []).
card_colors('natural selection', ['G']).
card_text('natural selection', 'Look at the top three cards of target player\'s library, then put them back in any order. You may have that player shuffle his or her library.').
card_mana_cost('natural selection', ['G']).
card_cmc('natural selection', 1).
card_layout('natural selection', 'normal').
card_reserved('natural selection').

% Found in: 10E, 9ED, DPA, PO2, POR, S99, TMP
card_name('natural spring', 'Natural Spring').
card_type('natural spring', 'Sorcery').
card_types('natural spring', ['Sorcery']).
card_subtypes('natural spring', []).
card_colors('natural spring', ['G']).
card_text('natural spring', 'Target player gains 8 life.').
card_mana_cost('natural spring', ['3', 'G', 'G']).
card_cmc('natural spring', 5).
card_layout('natural spring', 'normal').

% Found in: 10E, 8ED, 9ED, ALA, DPA, DTK, GTC, ISD, KTK, M10, M11, M12, M13, M14, M15, ONS, ROE
card_name('naturalize', 'Naturalize').
card_type('naturalize', 'Instant').
card_types('naturalize', ['Instant']).
card_subtypes('naturalize', []).
card_colors('naturalize', ['G']).
card_text('naturalize', 'Destroy target artifact or enchantment.').
card_mana_cost('naturalize', ['1', 'G']).
card_cmc('naturalize', 2).
card_layout('naturalize', 'normal').

% Found in: ARC
card_name('nature demands an offering', 'Nature Demands an Offering').
card_type('nature demands an offering', 'Scheme').
card_types('nature demands an offering', ['Scheme']).
card_subtypes('nature demands an offering', []).
card_colors('nature demands an offering', []).
card_text('nature demands an offering', 'When you set this scheme in motion, target opponent chooses a creature you don\'t control and puts it on top of its owner\'s library, then repeats this process for an artifact, an enchantment, and a land. Then the owner of each permanent chosen this way shuffles his or her library.').
card_layout('nature demands an offering', 'scheme').

% Found in: ARC
card_name('nature shields its own', 'Nature Shields Its Own').
card_type('nature shields its own', 'Ongoing Scheme').
card_types('nature shields its own', ['Scheme']).
card_subtypes('nature shields its own', []).
card_supertypes('nature shields its own', ['Ongoing']).
card_colors('nature shields its own', []).
card_text('nature shields its own', '(An ongoing scheme remains face up until it\'s abandoned.)\nWhenever a creature attacks and isn\'t blocked, if you\'re the defending player, put a 0/1 green Plant creature token onto the battlefield blocking that creature.\nWhen four or more creatures attack you, abandon this scheme at end of combat.').
card_layout('nature shields its own', 'scheme').

% Found in: ALL, ME2
card_name('nature\'s blessing', 'Nature\'s Blessing').
card_type('nature\'s blessing', 'Enchantment').
card_types('nature\'s blessing', ['Enchantment']).
card_subtypes('nature\'s blessing', []).
card_colors('nature\'s blessing', ['W', 'G']).
card_text('nature\'s blessing', '{G}{W}, Discard a card: Put a +1/+1 counter on target creature or that creature gains banding, first strike, or trample. (This effect lasts indefinitely. Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding a player controls are blocking or being blocked by a creature, that player divides that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('nature\'s blessing', ['2', 'G', 'W']).
card_cmc('nature\'s blessing', 4).
card_layout('nature\'s blessing', 'normal').

% Found in: ALL
card_name('nature\'s chosen', 'Nature\'s Chosen').
card_type('nature\'s chosen', 'Enchantment — Aura').
card_types('nature\'s chosen', ['Enchantment']).
card_subtypes('nature\'s chosen', ['Aura']).
card_colors('nature\'s chosen', ['G']).
card_text('nature\'s chosen', 'Enchant creature you control\n{0}: Untap enchanted creature. Activate this ability only during your turn and only once each turn.\nTap enchanted creature: Untap target artifact, creature, or land. Activate this ability only if enchanted creature is white and is untapped and only once each turn.').
card_mana_cost('nature\'s chosen', ['G']).
card_cmc('nature\'s chosen', 1).
card_layout('nature\'s chosen', 'normal').

% Found in: CNS, WWK
card_name('nature\'s claim', 'Nature\'s Claim').
card_type('nature\'s claim', 'Instant').
card_types('nature\'s claim', ['Instant']).
card_subtypes('nature\'s claim', []).
card_colors('nature\'s claim', ['G']).
card_text('nature\'s claim', 'Destroy target artifact or enchantment. Its controller gains 4 life.').
card_mana_cost('nature\'s claim', ['G']).
card_cmc('nature\'s claim', 1).
card_layout('nature\'s claim', 'normal').

% Found in: POR, S99
card_name('nature\'s cloak', 'Nature\'s Cloak').
card_type('nature\'s cloak', 'Sorcery').
card_types('nature\'s cloak', ['Sorcery']).
card_subtypes('nature\'s cloak', []).
card_colors('nature\'s cloak', ['G']).
card_text('nature\'s cloak', 'Green creatures you control gain forestwalk until end of turn. (They can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('nature\'s cloak', ['2', 'G']).
card_cmc('nature\'s cloak', 3).
card_layout('nature\'s cloak', 'normal').

% Found in: WTH
card_name('nature\'s kiss', 'Nature\'s Kiss').
card_type('nature\'s kiss', 'Enchantment — Aura').
card_types('nature\'s kiss', ['Enchantment']).
card_subtypes('nature\'s kiss', ['Aura']).
card_colors('nature\'s kiss', ['G']).
card_text('nature\'s kiss', 'Enchant creature\n{1}, Exile the top card of your graveyard: Enchanted creature gets +1/+1 until end of turn.').
card_mana_cost('nature\'s kiss', ['1', 'G']).
card_cmc('nature\'s kiss', 2).
card_layout('nature\'s kiss', 'normal').

% Found in: 5ED, DD3_GVL, DDD, ICE, MED, PO2, POR, S99, VMA
card_name('nature\'s lore', 'Nature\'s Lore').
card_type('nature\'s lore', 'Sorcery').
card_types('nature\'s lore', ['Sorcery']).
card_subtypes('nature\'s lore', []).
card_colors('nature\'s lore', ['G']).
card_text('nature\'s lore', 'Search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('nature\'s lore', ['1', 'G']).
card_cmc('nature\'s lore', 2).
card_layout('nature\'s lore', 'normal').

% Found in: JOU
card_name('nature\'s panoply', 'Nature\'s Panoply').
card_type('nature\'s panoply', 'Instant').
card_types('nature\'s panoply', ['Instant']).
card_subtypes('nature\'s panoply', []).
card_colors('nature\'s panoply', ['G']).
card_text('nature\'s panoply', 'Strive — Nature\'s Panoply costs {2}{G} more to cast for each target beyond the first.\nChoose any number of target creatures. Put a +1/+1 counter on each of them.').
card_mana_cost('nature\'s panoply', ['G']).
card_cmc('nature\'s panoply', 1).
card_layout('nature\'s panoply', 'normal').

% Found in: 6ED, 7ED, WTH
card_name('nature\'s resurgence', 'Nature\'s Resurgence').
card_type('nature\'s resurgence', 'Sorcery').
card_types('nature\'s resurgence', ['Sorcery']).
card_subtypes('nature\'s resurgence', []).
card_colors('nature\'s resurgence', ['G']).
card_text('nature\'s resurgence', 'Each player draws a card for each creature card in his or her graveyard.').
card_mana_cost('nature\'s resurgence', ['2', 'G', 'G']).
card_cmc('nature\'s resurgence', 4).
card_layout('nature\'s resurgence', 'normal').

% Found in: 7ED, TMP
card_name('nature\'s revolt', 'Nature\'s Revolt').
card_type('nature\'s revolt', 'Enchantment').
card_types('nature\'s revolt', ['Enchantment']).
card_subtypes('nature\'s revolt', []).
card_colors('nature\'s revolt', ['G']).
card_text('nature\'s revolt', 'All lands are 2/2 creatures that are still lands.').
card_mana_cost('nature\'s revolt', ['3', 'G', 'G']).
card_cmc('nature\'s revolt', 5).
card_layout('nature\'s revolt', 'normal').

% Found in: POR, VMA
card_name('nature\'s ruin', 'Nature\'s Ruin').
card_type('nature\'s ruin', 'Sorcery').
card_types('nature\'s ruin', ['Sorcery']).
card_subtypes('nature\'s ruin', []).
card_colors('nature\'s ruin', ['B']).
card_text('nature\'s ruin', 'Destroy all green creatures.').
card_mana_cost('nature\'s ruin', ['2', 'B']).
card_cmc('nature\'s ruin', 3).
card_layout('nature\'s ruin', 'normal').

% Found in: DPA, M10, M11
card_name('nature\'s spiral', 'Nature\'s Spiral').
card_type('nature\'s spiral', 'Sorcery').
card_types('nature\'s spiral', ['Sorcery']).
card_subtypes('nature\'s spiral', []).
card_colors('nature\'s spiral', ['G']).
card_text('nature\'s spiral', 'Return target permanent card from your graveyard to your hand. (A permanent card is an artifact, creature, enchantment, land, or planeswalker card.)').
card_mana_cost('nature\'s spiral', ['1', 'G']).
card_cmc('nature\'s spiral', 2).
card_layout('nature\'s spiral', 'normal').

% Found in: CHK
card_name('nature\'s will', 'Nature\'s Will').
card_type('nature\'s will', 'Enchantment').
card_types('nature\'s will', ['Enchantment']).
card_subtypes('nature\'s will', []).
card_colors('nature\'s will', ['G']).
card_text('nature\'s will', 'Whenever one or more creatures you control deal combat damage to a player, tap all lands that player controls and untap all lands you control.').
card_mana_cost('nature\'s will', ['2', 'G', 'G']).
card_cmc('nature\'s will', 4).
card_layout('nature\'s will', 'normal').

% Found in: ALL, ME2
card_name('nature\'s wrath', 'Nature\'s Wrath').
card_type('nature\'s wrath', 'Enchantment').
card_types('nature\'s wrath', ['Enchantment']).
card_subtypes('nature\'s wrath', []).
card_colors('nature\'s wrath', ['G']).
card_text('nature\'s wrath', 'At the beginning of your upkeep, sacrifice Nature\'s Wrath unless you pay {G}.\nWhenever a player puts an Island or blue permanent onto the battlefield, he or she sacrifices an Island or blue permanent.\nWhenever a player puts a Swamp or black permanent onto the battlefield, he or she sacrifices a Swamp or black permanent.').
card_mana_cost('nature\'s wrath', ['4', 'G', 'G']).
card_cmc('nature\'s wrath', 6).
card_layout('nature\'s wrath', 'normal').
card_reserved('nature\'s wrath').

% Found in: pHHO
card_name('naughty', 'Naughty').
card_type('naughty', 'Sorcery').
card_types('naughty', ['Sorcery']).
card_subtypes('naughty', []).
card_colors('naughty', ['B']).
card_text('naughty', 'Search another target player\'s library for a card and put that card into your hand. Then shuffle that player\'s library.').
card_mana_cost('naughty', ['1', 'B', 'B']).
card_cmc('naughty', 3).
card_layout('naughty', 'split').
card_sides('naughty', 'nice').

% Found in: 7ED, 8ED, EXO
card_name('nausea', 'Nausea').
card_type('nausea', 'Sorcery').
card_types('nausea', ['Sorcery']).
card_subtypes('nausea', []).
card_colors('nausea', ['B']).
card_text('nausea', 'All creatures get -1/-1 until end of turn.').
card_mana_cost('nausea', ['1', 'B']).
card_cmc('nausea', 2).
card_layout('nausea', 'normal').

% Found in: GTC
card_name('nav squad commandos', 'Nav Squad Commandos').
card_type('nav squad commandos', 'Creature — Human Soldier').
card_types('nav squad commandos', ['Creature']).
card_subtypes('nav squad commandos', ['Human', 'Soldier']).
card_colors('nav squad commandos', ['W']).
card_text('nav squad commandos', 'Battalion — Whenever Nav Squad Commandos and at least two other creatures attack, Nav Squad Commandos gets +1/+1 until end of turn. Untap it.').
card_mana_cost('nav squad commandos', ['4', 'W']).
card_cmc('nav squad commandos', 5).
card_layout('nav squad commandos', 'normal').
card_power('nav squad commandos', 3).
card_toughness('nav squad commandos', 5).

% Found in: HOP
card_name('naya', 'Naya').
card_type('naya', 'Plane — Alara').
card_types('naya', ['Plane']).
card_subtypes('naya', ['Alara']).
card_colors('naya', []).
card_text('naya', 'You may play any number of lands on each of your turns.\nWhenever you roll {C}, target red, green, or white creature you control gets +1/+1 until end of turn for each land you control.').
card_layout('naya', 'plane').

% Found in: ALA
card_name('naya battlemage', 'Naya Battlemage').
card_type('naya battlemage', 'Creature — Human Shaman').
card_types('naya battlemage', ['Creature']).
card_subtypes('naya battlemage', ['Human', 'Shaman']).
card_colors('naya battlemage', ['G']).
card_text('naya battlemage', '{R}, {T}: Target creature gets +2/+0 until end of turn.\n{W}, {T}: Tap target creature.').
card_mana_cost('naya battlemage', ['2', 'G']).
card_cmc('naya battlemage', 3).
card_layout('naya battlemage', 'normal').
card_power('naya battlemage', 2).
card_toughness('naya battlemage', 2).

% Found in: ALA, C13, DDH
card_name('naya charm', 'Naya Charm').
card_type('naya charm', 'Instant').
card_types('naya charm', ['Instant']).
card_subtypes('naya charm', []).
card_colors('naya charm', ['W', 'R', 'G']).
card_text('naya charm', 'Choose one —\n• Naya Charm deals 3 damage to target creature.\n• Return target card from a graveyard to its owner\'s hand.\n• Tap all creatures target player controls.').
card_mana_cost('naya charm', ['R', 'G', 'W']).
card_cmc('naya charm', 3).
card_layout('naya charm', 'normal').

% Found in: ARB
card_name('naya hushblade', 'Naya Hushblade').
card_type('naya hushblade', 'Creature — Elf Rogue').
card_types('naya hushblade', ['Creature']).
card_subtypes('naya hushblade', ['Elf', 'Rogue']).
card_colors('naya hushblade', ['W', 'R', 'G']).
card_text('naya hushblade', 'As long as you control another multicolored permanent, Naya Hushblade gets +1/+1 and has shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('naya hushblade', ['R/W', 'G']).
card_cmc('naya hushblade', 2).
card_layout('naya hushblade', 'normal').
card_power('naya hushblade', 2).
card_toughness('naya hushblade', 1).

% Found in: ALA, C13
card_name('naya panorama', 'Naya Panorama').
card_type('naya panorama', 'Land').
card_types('naya panorama', ['Land']).
card_subtypes('naya panorama', []).
card_colors('naya panorama', []).
card_text('naya panorama', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Naya Panorama: Search your library for a basic Mountain, Forest, or Plains card and put it onto the battlefield tapped. Then shuffle your library.').
card_layout('naya panorama', 'normal').

% Found in: ARB, pWPN
card_name('naya sojourners', 'Naya Sojourners').
card_type('naya sojourners', 'Creature — Elf Shaman').
card_types('naya sojourners', ['Creature']).
card_subtypes('naya sojourners', ['Elf', 'Shaman']).
card_colors('naya sojourners', ['W', 'R', 'G']).
card_text('naya sojourners', 'When you cycle Naya Sojourners or it dies, you may put a +1/+1 counter on target creature.\nCycling {2}{G} ({2}{G}, Discard this card: Draw a card.)').
card_mana_cost('naya sojourners', ['2', 'R', 'G', 'W']).
card_cmc('naya sojourners', 5).
card_layout('naya sojourners', 'normal').
card_power('naya sojourners', 5).
card_toughness('naya sojourners', 3).

% Found in: C13
card_name('naya soulbeast', 'Naya Soulbeast').
card_type('naya soulbeast', 'Creature — Beast').
card_types('naya soulbeast', ['Creature']).
card_subtypes('naya soulbeast', ['Beast']).
card_colors('naya soulbeast', ['G']).
card_text('naya soulbeast', 'Trample\nWhen you cast Naya Soulbeast, each player reveals the top card of his or her library. Naya Soulbeast enters the battlefield with X +1/+1 counters on it, where X is the total converted mana cost of all cards revealed this way.').
card_mana_cost('naya soulbeast', ['6', 'G', 'G']).
card_cmc('naya soulbeast', 8).
card_layout('naya soulbeast', 'normal').
card_power('naya soulbeast', 0).
card_toughness('naya soulbeast', 0).

% Found in: ROE
card_name('near-death experience', 'Near-Death Experience').
card_type('near-death experience', 'Enchantment').
card_types('near-death experience', ['Enchantment']).
card_subtypes('near-death experience', []).
card_colors('near-death experience', ['W']).
card_text('near-death experience', 'At the beginning of your upkeep, if you have exactly 1 life, you win the game.').
card_mana_cost('near-death experience', ['2', 'W', 'W', 'W']).
card_cmc('near-death experience', 5).
card_layout('near-death experience', 'normal').

% Found in: AVR
card_name('nearheath pilgrim', 'Nearheath Pilgrim').
card_type('nearheath pilgrim', 'Creature — Human Cleric').
card_types('nearheath pilgrim', ['Creature']).
card_subtypes('nearheath pilgrim', ['Human', 'Cleric']).
card_colors('nearheath pilgrim', ['W']).
card_text('nearheath pilgrim', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Nearheath Pilgrim is paired with another creature, both creatures have lifelink.').
card_mana_cost('nearheath pilgrim', ['1', 'W']).
card_cmc('nearheath pilgrim', 2).
card_layout('nearheath pilgrim', 'normal').
card_power('nearheath pilgrim', 2).
card_toughness('nearheath pilgrim', 1).

% Found in: DKA, pWPN
card_name('nearheath stalker', 'Nearheath Stalker').
card_type('nearheath stalker', 'Creature — Vampire Rogue').
card_types('nearheath stalker', ['Creature']).
card_subtypes('nearheath stalker', ['Vampire', 'Rogue']).
card_colors('nearheath stalker', ['R']).
card_text('nearheath stalker', 'Undying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('nearheath stalker', ['4', 'R']).
card_cmc('nearheath stalker', 5).
card_layout('nearheath stalker', 'normal').
card_power('nearheath stalker', 4).
card_toughness('nearheath stalker', 1).

% Found in: CHR, LEG, ME3
card_name('nebuchadnezzar', 'Nebuchadnezzar').
card_type('nebuchadnezzar', 'Legendary Creature — Human Wizard').
card_types('nebuchadnezzar', ['Creature']).
card_subtypes('nebuchadnezzar', ['Human', 'Wizard']).
card_supertypes('nebuchadnezzar', ['Legendary']).
card_colors('nebuchadnezzar', ['U', 'B']).
card_text('nebuchadnezzar', '{X}, {T}: Name a card. Target opponent reveals X cards at random from his or her hand. Then that player discards all cards with that name revealed this way. Activate this ability only during your turn.').
card_mana_cost('nebuchadnezzar', ['3', 'U', 'B']).
card_cmc('nebuchadnezzar', 5).
card_layout('nebuchadnezzar', 'normal').
card_power('nebuchadnezzar', 3).
card_toughness('nebuchadnezzar', 3).

% Found in: LRW
card_name('neck snap', 'Neck Snap').
card_type('neck snap', 'Instant').
card_types('neck snap', ['Instant']).
card_subtypes('neck snap', []).
card_colors('neck snap', ['W']).
card_text('neck snap', 'Destroy target attacking or blocking creature.').
card_mana_cost('neck snap', ['3', 'W']).
card_cmc('neck snap', 4).
card_layout('neck snap', 'normal').

% Found in: APC
card_name('necra disciple', 'Necra Disciple').
card_type('necra disciple', 'Creature — Human Wizard').
card_types('necra disciple', ['Creature']).
card_subtypes('necra disciple', ['Human', 'Wizard']).
card_colors('necra disciple', ['B']).
card_text('necra disciple', '{G}, {T}: Add one mana of any color to your mana pool.\n{W}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('necra disciple', ['B']).
card_cmc('necra disciple', 1).
card_layout('necra disciple', 'normal').
card_power('necra disciple', 1).
card_toughness('necra disciple', 1).

% Found in: APC
card_name('necra sanctuary', 'Necra Sanctuary').
card_type('necra sanctuary', 'Enchantment').
card_types('necra sanctuary', ['Enchantment']).
card_subtypes('necra sanctuary', []).
card_colors('necra sanctuary', ['B']).
card_text('necra sanctuary', 'At the beginning of your upkeep, if you control a green or white permanent, target player loses 1 life. If you control a green permanent and a white permanent, that player loses 3 life instead.').
card_mana_cost('necra sanctuary', ['2', 'B']).
card_cmc('necra sanctuary', 3).
card_layout('necra sanctuary', 'normal').

% Found in: WTH
card_name('necratog', 'Necratog').
card_type('necratog', 'Creature — Atog').
card_types('necratog', ['Creature']).
card_subtypes('necratog', ['Atog']).
card_colors('necratog', ['B']).
card_text('necratog', 'Exile the top creature card of your graveyard: Necratog gets +2/+2 until end of turn.').
card_mana_cost('necratog', ['1', 'B', 'B']).
card_cmc('necratog', 3).
card_layout('necratog', 'normal').
card_power('necratog', 1).
card_toughness('necratog', 2).

% Found in: APC
card_name('necravolver', 'Necravolver').
card_type('necravolver', 'Creature — Volver').
card_types('necravolver', ['Creature']).
card_subtypes('necravolver', ['Volver']).
card_colors('necravolver', ['B']).
card_text('necravolver', 'Kicker {1}{G} and/or {W} (You may pay an additional {1}{G} and/or {W} as you cast this spell.)\nIf Necravolver was kicked with its {1}{G} kicker, it enters the battlefield with two +1/+1 counters on it and with trample.\nIf Necravolver was kicked with its {W} kicker, it enters the battlefield with a +1/+1 counter on it and with \"Whenever Necravolver deals damage, you gain that much life.\"').
card_mana_cost('necravolver', ['2', 'B']).
card_cmc('necravolver', 3).
card_layout('necravolver', 'normal').
card_power('necravolver', 2).
card_toughness('necravolver', 2).

% Found in: 5ED, FEM, ME2
card_name('necrite', 'Necrite').
card_type('necrite', 'Creature — Thrull').
card_types('necrite', ['Creature']).
card_subtypes('necrite', ['Thrull']).
card_colors('necrite', ['B']).
card_text('necrite', 'Whenever Necrite attacks and isn\'t blocked, you may sacrifice it. If you do, destroy target creature defending player controls. It can\'t be regenerated.').
card_mana_cost('necrite', ['1', 'B', 'B']).
card_cmc('necrite', 3).
card_layout('necrite', 'normal').
card_power('necrite', 2).
card_toughness('necrite', 2).

% Found in: UNH
card_name('necro-impotence', 'Necro-Impotence').
card_type('necro-impotence', 'Enchantment').
card_types('necro-impotence', ['Enchantment']).
card_subtypes('necro-impotence', []).
card_colors('necro-impotence', ['B']).
card_text('necro-impotence', 'Skip your untap step.\nAt the beginning of your upkeep, you may pay X life. If you do, untap X permanents.\nPay ½ life: Remove the top card of your library from the game face down. Put that card into your hand at end of turn.').
card_mana_cost('necro-impotence', ['B', 'B', 'B']).
card_cmc('necro-impotence', 3).
card_layout('necro-impotence', 'normal').

% Found in: AVR, BNG, M15
card_name('necrobite', 'Necrobite').
card_type('necrobite', 'Instant').
card_types('necrobite', ['Instant']).
card_subtypes('necrobite', []).
card_colors('necrobite', ['B']).
card_text('necrobite', 'Target creature gains deathtouch until end of turn. Regenerate it. (The next time that creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat. Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_mana_cost('necrobite', ['2', 'B']).
card_cmc('necrobite', 3).
card_layout('necrobite', 'normal').

% Found in: SOM
card_name('necrogen censer', 'Necrogen Censer').
card_type('necrogen censer', 'Artifact').
card_types('necrogen censer', ['Artifact']).
card_subtypes('necrogen censer', []).
card_colors('necrogen censer', []).
card_text('necrogen censer', 'Necrogen Censer enters the battlefield with two charge counters on it.\n{T}, Remove a charge counter from Necrogen Censer: Target player loses 2 life.').
card_mana_cost('necrogen censer', ['3']).
card_cmc('necrogen censer', 3).
card_layout('necrogen censer', 'normal').

% Found in: MRD
card_name('necrogen mists', 'Necrogen Mists').
card_type('necrogen mists', 'Enchantment').
card_types('necrogen mists', ['Enchantment']).
card_subtypes('necrogen mists', []).
card_colors('necrogen mists', ['B']).
card_text('necrogen mists', 'At the beginning of each player\'s upkeep, that player discards a card.').
card_mana_cost('necrogen mists', ['2', 'B']).
card_cmc('necrogen mists', 3).
card_layout('necrogen mists', 'normal').

% Found in: M15, SOM
card_name('necrogen scudder', 'Necrogen Scudder').
card_type('necrogen scudder', 'Creature — Horror').
card_types('necrogen scudder', ['Creature']).
card_subtypes('necrogen scudder', ['Horror']).
card_colors('necrogen scudder', ['B']).
card_text('necrogen scudder', 'Flying\nWhen Necrogen Scudder enters the battlefield, you lose 3 life.').
card_mana_cost('necrogen scudder', ['2', 'B']).
card_cmc('necrogen scudder', 3).
card_layout('necrogen scudder', 'normal').
card_power('necrogen scudder', 3).
card_toughness('necrogen scudder', 3).

% Found in: MRD
card_name('necrogen spellbomb', 'Necrogen Spellbomb').
card_type('necrogen spellbomb', 'Artifact').
card_types('necrogen spellbomb', ['Artifact']).
card_subtypes('necrogen spellbomb', []).
card_colors('necrogen spellbomb', []).
card_text('necrogen spellbomb', '{B}, Sacrifice Necrogen Spellbomb: Target player discards a card.\n{1}, Sacrifice Necrogen Spellbomb: Draw a card.').
card_mana_cost('necrogen spellbomb', ['1']).
card_cmc('necrogen spellbomb', 1).
card_layout('necrogen spellbomb', 'normal').

% Found in: ALA, CMD, MM2
card_name('necrogenesis', 'Necrogenesis').
card_type('necrogenesis', 'Enchantment').
card_types('necrogenesis', ['Enchantment']).
card_subtypes('necrogenesis', []).
card_colors('necrogenesis', ['B', 'G']).
card_text('necrogenesis', '{2}: Exile target creature card from a graveyard. Put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('necrogenesis', ['B', 'G']).
card_cmc('necrogenesis', 2).
card_layout('necrogenesis', 'normal').

% Found in: 7ED, EXO, TPR
card_name('necrologia', 'Necrologia').
card_type('necrologia', 'Instant').
card_types('necrologia', ['Instant']).
card_subtypes('necrologia', []).
card_colors('necrologia', ['B']).
card_text('necrologia', 'Cast Necrologia only during your end step.\nAs an additional cost to cast Necrologia, pay X life.\nDraw X cards.').
card_mana_cost('necrologia', ['3', 'B', 'B']).
card_cmc('necrologia', 5).
card_layout('necrologia', 'normal').

% Found in: M15
card_name('necromancer\'s assistant', 'Necromancer\'s Assistant').
card_type('necromancer\'s assistant', 'Creature — Zombie').
card_types('necromancer\'s assistant', ['Creature']).
card_subtypes('necromancer\'s assistant', ['Zombie']).
card_colors('necromancer\'s assistant', ['B']).
card_text('necromancer\'s assistant', 'When Necromancer\'s Assistant enters the battlefield, put the top three cards of your library into your graveyard.').
card_mana_cost('necromancer\'s assistant', ['2', 'B']).
card_cmc('necromancer\'s assistant', 3).
card_layout('necromancer\'s assistant', 'normal').
card_power('necromancer\'s assistant', 3).
card_toughness('necromancer\'s assistant', 1).

% Found in: ARB
card_name('necromancer\'s covenant', 'Necromancer\'s Covenant').
card_type('necromancer\'s covenant', 'Enchantment').
card_types('necromancer\'s covenant', ['Enchantment']).
card_subtypes('necromancer\'s covenant', []).
card_colors('necromancer\'s covenant', ['W', 'B']).
card_text('necromancer\'s covenant', 'When Necromancer\'s Covenant enters the battlefield, exile all creature cards from target player\'s graveyard, then put a 2/2 black Zombie creature token onto the battlefield for each card exiled this way.\nZombies you control have lifelink.').
card_mana_cost('necromancer\'s covenant', ['3', 'W', 'B', 'B']).
card_cmc('necromancer\'s covenant', 6).
card_layout('necromancer\'s covenant', 'normal').

% Found in: GPT
card_name('necromancer\'s magemark', 'Necromancer\'s Magemark').
card_type('necromancer\'s magemark', 'Enchantment — Aura').
card_types('necromancer\'s magemark', ['Enchantment']).
card_subtypes('necromancer\'s magemark', ['Aura']).
card_colors('necromancer\'s magemark', ['B']).
card_text('necromancer\'s magemark', 'Enchant creature\nCreatures you control that are enchanted get +1/+1.\nIf a creature you control that\'s enchanted would die, return it to its owner\'s hand instead.').
card_mana_cost('necromancer\'s magemark', ['2', 'B']).
card_cmc('necromancer\'s magemark', 3).
card_layout('necromancer\'s magemark', 'normal').

% Found in: M15
card_name('necromancer\'s stockpile', 'Necromancer\'s Stockpile').
card_type('necromancer\'s stockpile', 'Enchantment').
card_types('necromancer\'s stockpile', ['Enchantment']).
card_subtypes('necromancer\'s stockpile', []).
card_colors('necromancer\'s stockpile', ['B']).
card_text('necromancer\'s stockpile', '{1}{B}, Discard a creature card: Draw a card. If the discarded card was a Zombie card, put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_mana_cost('necromancer\'s stockpile', ['1', 'B']).
card_cmc('necromancer\'s stockpile', 2).
card_layout('necromancer\'s stockpile', 'normal').

% Found in: VIS
card_name('necromancy', 'Necromancy').
card_type('necromancy', 'Enchantment').
card_types('necromancy', ['Enchantment']).
card_subtypes('necromancy', []).
card_colors('necromancy', ['B']).
card_text('necromancy', 'You may cast Necromancy as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nWhen Necromancy enters the battlefield, if it\'s on the battlefield, it becomes an Aura with \"enchant creature put onto the battlefield with Necromancy.\" Put target creature card from a graveyard onto the battlefield under your control and attach Necromancy to it. When Necromancy leaves the battlefield, that creature\'s controller sacrifices it.').
card_mana_cost('necromancy', ['2', 'B']).
card_cmc('necromancy', 3).
card_layout('necromancy', 'normal').

% Found in: C14
card_name('necromantic selection', 'Necromantic Selection').
card_type('necromantic selection', 'Sorcery').
card_types('necromantic selection', ['Sorcery']).
card_subtypes('necromantic selection', []).
card_colors('necromantic selection', ['B']).
card_text('necromantic selection', 'Destroy all creatures, then return a creature card put into a graveyard this way to the battlefield under your control. It\'s a black Zombie in addition to its other colors and types. Exile Necromantic Selection.').
card_mana_cost('necromantic selection', ['4', 'B', 'B', 'B']).
card_cmc('necromantic selection', 7).
card_layout('necromantic selection', 'normal').

% Found in: ORI
card_name('necromantic summons', 'Necromantic Summons').
card_type('necromantic summons', 'Sorcery').
card_types('necromantic summons', ['Sorcery']).
card_subtypes('necromantic summons', []).
card_colors('necromantic summons', ['B']).
card_text('necromantic summons', 'Put target creature card from a graveyard onto the battlefield under your control.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, that creature enters the battlefield with two additional +1/+1 counters on it.').
card_mana_cost('necromantic summons', ['4', 'B']).
card_cmc('necromantic summons', 5).
card_layout('necromantic summons', 'normal').

% Found in: CNS, RAV
card_name('necromantic thirst', 'Necromantic Thirst').
card_type('necromantic thirst', 'Enchantment — Aura').
card_types('necromantic thirst', ['Enchantment']).
card_subtypes('necromantic thirst', ['Aura']).
card_colors('necromantic thirst', ['B']).
card_text('necromantic thirst', 'Enchant creature\nWhenever enchanted creature deals combat damage to a player, you may return target creature card from your graveyard to your hand.').
card_mana_cost('necromantic thirst', ['2', 'B', 'B']).
card_cmc('necromantic thirst', 4).
card_layout('necromantic thirst', 'normal').

% Found in: DTK, pMEI
card_name('necromaster dragon', 'Necromaster Dragon').
card_type('necromaster dragon', 'Creature — Dragon').
card_types('necromaster dragon', ['Creature']).
card_subtypes('necromaster dragon', ['Dragon']).
card_colors('necromaster dragon', ['U', 'B']).
card_text('necromaster dragon', 'Flying\nWhenever Necromaster Dragon deals combat damage to a player, you may pay {2}. If you do, put a 2/2 black Zombie creature token onto the battlefield and each opponent puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('necromaster dragon', ['3', 'U', 'B']).
card_cmc('necromaster dragon', 5).
card_layout('necromaster dragon', 'normal').
card_power('necromaster dragon', 4).
card_toughness('necromaster dragon', 4).

% Found in: SOM
card_name('necropede', 'Necropede').
card_type('necropede', 'Artifact Creature — Insect').
card_types('necropede', ['Artifact', 'Creature']).
card_subtypes('necropede', ['Insect']).
card_colors('necropede', []).
card_text('necropede', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Necropede dies, you may put a -1/-1 counter on target creature.').
card_mana_cost('necropede', ['2']).
card_cmc('necropede', 2).
card_layout('necropede', 'normal').
card_power('necropede', 1).
card_toughness('necropede', 1).

% Found in: RAV
card_name('necroplasm', 'Necroplasm').
card_type('necroplasm', 'Creature — Ooze').
card_types('necroplasm', ['Creature']).
card_subtypes('necroplasm', ['Ooze']).
card_colors('necroplasm', ['B']).
card_text('necroplasm', 'At the beginning of your upkeep, put a +1/+1 counter on Necroplasm.\nAt the beginning of your end step, destroy each creature with converted mana cost equal to the number of +1/+1 counters on Necroplasm.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('necroplasm', ['1', 'B', 'B']).
card_cmc('necroplasm', 3).
card_layout('necroplasm', 'normal').
card_power('necroplasm', 1).
card_toughness('necroplasm', 1).

% Found in: DRK
card_name('necropolis', 'Necropolis').
card_type('necropolis', 'Artifact Creature — Wall').
card_types('necropolis', ['Artifact', 'Creature']).
card_subtypes('necropolis', ['Wall']).
card_colors('necropolis', []).
card_text('necropolis', 'Defender (This creature can\'t attack.)\nExile a creature card from your graveyard: Put X +0/+1 counters on Necropolis, where X is the exiled card\'s converted mana cost.').
card_mana_cost('necropolis', ['5']).
card_cmc('necropolis', 5).
card_layout('necropolis', 'normal').
card_power('necropolis', 0).
card_toughness('necropolis', 1).

% Found in: CPK, KTK, pPRE
card_name('necropolis fiend', 'Necropolis Fiend').
card_type('necropolis fiend', 'Creature — Demon').
card_types('necropolis fiend', ['Creature']).
card_subtypes('necropolis fiend', ['Demon']).
card_colors('necropolis fiend', ['B']).
card_text('necropolis fiend', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nFlying\n{X}, {T}, Exile X cards from your graveyard: Target creature gets -X/-X until end of turn.').
card_mana_cost('necropolis fiend', ['7', 'B', 'B']).
card_cmc('necropolis fiend', 9).
card_layout('necropolis fiend', 'normal').
card_power('necropolis fiend', 4).
card_toughness('necropolis fiend', 5).

% Found in: RTR
card_name('necropolis regent', 'Necropolis Regent').
card_type('necropolis regent', 'Creature — Vampire').
card_types('necropolis regent', ['Creature']).
card_subtypes('necropolis regent', ['Vampire']).
card_colors('necropolis regent', ['B']).
card_text('necropolis regent', 'Flying\nWhenever a creature you control deals combat damage to a player, put that many +1/+1 counters on it.').
card_mana_cost('necropolis regent', ['3', 'B', 'B', 'B']).
card_cmc('necropolis regent', 6).
card_layout('necropolis regent', 'normal').
card_power('necropolis regent', 6).
card_toughness('necropolis regent', 5).

% Found in: 5ED, DKM, ICE, ME2, V09, VMA
card_name('necropotence', 'Necropotence').
card_type('necropotence', 'Enchantment').
card_types('necropotence', ['Enchantment']).
card_subtypes('necropotence', []).
card_colors('necropotence', ['B']).
card_text('necropotence', 'Skip your draw step.\nWhenever you discard a card, exile that card from your graveyard.\nPay 1 life: Exile the top card of your library face down. Put that card into your hand at the beginning of your next end step.').
card_mana_cost('necropotence', ['B', 'B', 'B']).
card_cmc('necropotence', 3).
card_layout('necropotence', 'normal').

% Found in: VAN
card_name('necropotence avatar', 'Necropotence Avatar').
card_type('necropotence avatar', 'Vanguard').
card_types('necropotence avatar', ['Vanguard']).
card_subtypes('necropotence avatar', []).
card_colors('necropotence avatar', []).
card_text('necropotence avatar', 'Skip your draw step.\nAt the beginning of your end step, if it\'s not the first turn of the game, put a death counter on Necropotence Avatar. You draw X cards and you lose X life, where X is the number of death counters on Necropotence Avatar.').
card_layout('necropotence avatar', 'vanguard').

% Found in: NPH
card_name('necropouncer', 'Necropouncer').
card_type('necropouncer', 'Artifact — Equipment').
card_types('necropouncer', ['Artifact']).
card_subtypes('necropouncer', ['Equipment']).
card_colors('necropouncer', []).
card_text('necropouncer', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +3/+1 and has haste.\nEquip {2}').
card_mana_cost('necropouncer', ['6']).
card_cmc('necropouncer', 6).
card_layout('necropouncer', 'normal').

% Found in: 6ED, MGB, VIS
card_name('necrosavant', 'Necrosavant').
card_type('necrosavant', 'Creature — Zombie Giant').
card_types('necrosavant', ['Creature']).
card_subtypes('necrosavant', ['Zombie', 'Giant']).
card_colors('necrosavant', ['B']).
card_text('necrosavant', '{3}{B}{B}, Sacrifice a creature: Return Necrosavant from your graveyard to the battlefield. Activate this ability only during your upkeep.').
card_mana_cost('necrosavant', ['3', 'B', 'B', 'B']).
card_cmc('necrosavant', 6).
card_layout('necrosavant', 'normal').
card_power('necrosavant', 5).
card_toughness('necrosavant', 5).

% Found in: EVE, MM2
card_name('necroskitter', 'Necroskitter').
card_type('necroskitter', 'Creature — Elemental').
card_types('necroskitter', ['Creature']).
card_subtypes('necroskitter', ['Elemental']).
card_colors('necroskitter', ['B']).
card_text('necroskitter', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever a creature an opponent controls with a -1/-1 counter on it dies, you may return that card to the battlefield under your control.').
card_mana_cost('necroskitter', ['1', 'B', 'B']).
card_cmc('necroskitter', 3).
card_layout('necroskitter', 'normal').
card_power('necroskitter', 1).
card_toughness('necroskitter', 4).

% Found in: SOM
card_name('necrotic ooze', 'Necrotic Ooze').
card_type('necrotic ooze', 'Creature — Ooze').
card_types('necrotic ooze', ['Creature']).
card_subtypes('necrotic ooze', ['Ooze']).
card_colors('necrotic ooze', ['B']).
card_text('necrotic ooze', 'As long as Necrotic Ooze is on the battlefield, it has all activated abilities of all creature cards in all graveyards.').
card_mana_cost('necrotic ooze', ['2', 'B', 'B']).
card_cmc('necrotic ooze', 4).
card_layout('necrotic ooze', 'normal').
card_power('necrotic ooze', 4).
card_toughness('necrotic ooze', 3).

% Found in: M11
card_name('necrotic plague', 'Necrotic Plague').
card_type('necrotic plague', 'Enchantment — Aura').
card_types('necrotic plague', ['Enchantment']).
card_subtypes('necrotic plague', ['Aura']).
card_colors('necrotic plague', ['B']).
card_text('necrotic plague', 'Enchant creature\nEnchanted creature has \"At the beginning of your upkeep, sacrifice this creature.\"\nWhen enchanted creature dies, its controller chooses target creature one of his or her opponents controls. Return Necrotic Plague from its owner\'s graveyard to the battlefield attached to that creature.').
card_mana_cost('necrotic plague', ['2', 'B', 'B']).
card_cmc('necrotic plague', 4).
card_layout('necrotic plague', 'normal').

% Found in: H09, PLC
card_name('necrotic sliver', 'Necrotic Sliver').
card_type('necrotic sliver', 'Creature — Sliver').
card_types('necrotic sliver', ['Creature']).
card_subtypes('necrotic sliver', ['Sliver']).
card_colors('necrotic sliver', ['W', 'B']).
card_text('necrotic sliver', 'All Slivers have \"{3}, Sacrifice this permanent: Destroy target permanent.\"').
card_mana_cost('necrotic sliver', ['1', 'W', 'B']).
card_cmc('necrotic sliver', 3).
card_layout('necrotic sliver', 'normal').
card_power('necrotic sliver', 2).
card_toughness('necrotic sliver', 2).

% Found in: LRW
card_name('nectar faerie', 'Nectar Faerie').
card_type('nectar faerie', 'Creature — Faerie Wizard').
card_types('nectar faerie', ['Creature']).
card_subtypes('nectar faerie', ['Faerie', 'Wizard']).
card_colors('nectar faerie', ['B']).
card_text('nectar faerie', 'Flying\n{B}, {T}: Target Faerie or Elf gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('nectar faerie', ['1', 'B']).
card_cmc('nectar faerie', 2).
card_layout('nectar faerie', 'normal').
card_power('nectar faerie', 1).
card_toughness('nectar faerie', 1).

% Found in: ODY
card_name('need for speed', 'Need for Speed').
card_type('need for speed', 'Enchantment').
card_types('need for speed', ['Enchantment']).
card_subtypes('need for speed', []).
card_colors('need for speed', ['R']).
card_text('need for speed', 'Sacrifice a land: Target creature gains haste until end of turn.').
card_mana_cost('need for speed', ['R']).
card_cmc('need for speed', 1).
card_layout('need for speed', 'normal').

% Found in: LRW
card_name('needle drop', 'Needle Drop').
card_type('needle drop', 'Instant').
card_types('needle drop', ['Instant']).
card_subtypes('needle drop', []).
card_colors('needle drop', ['R']).
card_text('needle drop', 'Needle Drop deals 1 damage to target creature or player that was dealt damage this turn.\nDraw a card.').
card_mana_cost('needle drop', ['R']).
card_cmc('needle drop', 1).
card_layout('needle drop', 'normal').

% Found in: EVE
card_name('needle specter', 'Needle Specter').
card_type('needle specter', 'Creature — Specter').
card_types('needle specter', ['Creature']).
card_subtypes('needle specter', ['Specter']).
card_colors('needle specter', ['B']).
card_text('needle specter', 'Flying\nWither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever Needle Specter deals combat damage to a player, that player discards that many cards.').
card_mana_cost('needle specter', ['1', 'B', 'B']).
card_cmc('needle specter', 3).
card_layout('needle specter', 'normal').
card_power('needle specter', 1).
card_toughness('needle specter', 1).

% Found in: 9ED, POR, TMP, TPR
card_name('needle storm', 'Needle Storm').
card_type('needle storm', 'Sorcery').
card_types('needle storm', ['Sorcery']).
card_subtypes('needle storm', []).
card_colors('needle storm', ['G']).
card_text('needle storm', 'Needle Storm deals 4 damage to each creature with flying.').
card_mana_cost('needle storm', ['2', 'G']).
card_cmc('needle storm', 3).
card_layout('needle storm', 'normal').

% Found in: ZEN
card_name('needlebite trap', 'Needlebite Trap').
card_type('needlebite trap', 'Instant — Trap').
card_types('needlebite trap', ['Instant']).
card_subtypes('needlebite trap', ['Trap']).
card_colors('needlebite trap', ['B']).
card_text('needlebite trap', 'If an opponent gained life this turn, you may pay {B} rather than pay Needlebite Trap\'s mana cost.\nTarget player loses 5 life and you gain 5 life.').
card_mana_cost('needlebite trap', ['5', 'B', 'B']).
card_cmc('needlebite trap', 7).
card_layout('needlebite trap', 'normal').

% Found in: MRD
card_name('needlebug', 'Needlebug').
card_type('needlebug', 'Artifact Creature — Insect').
card_types('needlebug', ['Artifact', 'Creature']).
card_subtypes('needlebug', ['Insect']).
card_colors('needlebug', []).
card_text('needlebug', 'Flash\nProtection from artifacts').
card_mana_cost('needlebug', ['4']).
card_cmc('needlebug', 4).
card_layout('needlebug', 'normal').
card_power('needlebug', 2).
card_toughness('needlebug', 2).

% Found in: PLC
card_name('needlepeak spider', 'Needlepeak Spider').
card_type('needlepeak spider', 'Creature — Spider').
card_types('needlepeak spider', ['Creature']).
card_subtypes('needlepeak spider', ['Spider']).
card_colors('needlepeak spider', ['R']).
card_text('needlepeak spider', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('needlepeak spider', ['3', 'R']).
card_cmc('needlepeak spider', 4).
card_layout('needlepeak spider', 'normal').
card_power('needlepeak spider', 4).
card_toughness('needlepeak spider', 2).

% Found in: LGN
card_name('needleshot gourna', 'Needleshot Gourna').
card_type('needleshot gourna', 'Creature — Beast').
card_types('needleshot gourna', ['Creature']).
card_subtypes('needleshot gourna', ['Beast']).
card_colors('needleshot gourna', ['G']).
card_text('needleshot gourna', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('needleshot gourna', ['4', 'G', 'G']).
card_cmc('needleshot gourna', 6).
card_layout('needleshot gourna', 'normal').
card_power('needleshot gourna', 3).
card_toughness('needleshot gourna', 6).

% Found in: ODY
card_name('nefarious lich', 'Nefarious Lich').
card_type('nefarious lich', 'Enchantment').
card_types('nefarious lich', ['Enchantment']).
card_subtypes('nefarious lich', []).
card_colors('nefarious lich', ['B']).
card_text('nefarious lich', 'If damage would be dealt to you, exile that many cards from your graveyard instead. If you can\'t, you lose the game.\nIf you would gain life, draw that many cards instead.\nWhen Nefarious Lich leaves the battlefield, you lose the game.').
card_mana_cost('nefarious lich', ['B', 'B', 'B', 'B']).
card_cmc('nefarious lich', 4).
card_layout('nefarious lich', 'normal').

% Found in: M13
card_name('nefarox, overlord of grixis', 'Nefarox, Overlord of Grixis').
card_type('nefarox, overlord of grixis', 'Legendary Creature — Demon').
card_types('nefarox, overlord of grixis', ['Creature']).
card_subtypes('nefarox, overlord of grixis', ['Demon']).
card_supertypes('nefarox, overlord of grixis', ['Legendary']).
card_colors('nefarox, overlord of grixis', ['B']).
card_text('nefarox, overlord of grixis', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever Nefarox, Overlord of Grixis attacks alone, defending player sacrifices a creature.').
card_mana_cost('nefarox, overlord of grixis', ['4', 'B', 'B']).
card_cmc('nefarox, overlord of grixis', 6).
card_layout('nefarox, overlord of grixis', 'normal').
card_power('nefarox, overlord of grixis', 5).
card_toughness('nefarox, overlord of grixis', 5).

% Found in: HOP, SCG
card_name('nefashu', 'Nefashu').
card_type('nefashu', 'Creature — Zombie Mutant').
card_types('nefashu', ['Creature']).
card_subtypes('nefashu', ['Zombie', 'Mutant']).
card_colors('nefashu', ['B']).
card_text('nefashu', 'Whenever Nefashu attacks, up to five target creatures each get -1/-1 until end of turn.').
card_mana_cost('nefashu', ['4', 'B', 'B']).
card_cmc('nefashu', 6).
card_layout('nefashu', 'normal').
card_power('nefashu', 5).
card_toughness('nefashu', 3).

% Found in: DPA, DTK, M10, M11, M12, M13, M14, M15, MOR, ORI, pMPR
card_name('negate', 'Negate').
card_type('negate', 'Instant').
card_types('negate', ['Instant']).
card_subtypes('negate', []).
card_colors('negate', ['U']).
card_text('negate', 'Counter target noncreature spell.').
card_mana_cost('negate', ['1', 'U']).
card_cmc('negate', 2).
card_layout('negate', 'normal').

% Found in: BOK
card_name('neko-te', 'Neko-Te').
card_type('neko-te', 'Artifact — Equipment').
card_types('neko-te', ['Artifact']).
card_subtypes('neko-te', ['Equipment']).
card_colors('neko-te', []).
card_text('neko-te', 'Whenever equipped creature deals damage to a creature, tap that creature. That creature doesn\'t untap during its controller\'s untap step for as long as Neko-Te remains on the battlefield.\nWhenever equipped creature deals damage to a player, that player loses 1 life.\nEquip {2}').
card_mana_cost('neko-te', ['3']).
card_cmc('neko-te', 3).
card_layout('neko-te', 'normal').

% Found in: 10E, 8ED, 9ED, BRB, C14, DDM, VIS
card_name('nekrataal', 'Nekrataal').
card_type('nekrataal', 'Creature — Human Assassin').
card_types('nekrataal', ['Creature']).
card_subtypes('nekrataal', ['Human', 'Assassin']).
card_colors('nekrataal', ['B']).
card_text('nekrataal', 'First strike\nWhen Nekrataal enters the battlefield, destroy target nonartifact, nonblack creature. That creature can\'t be regenerated.').
card_mana_cost('nekrataal', ['2', 'B', 'B']).
card_cmc('nekrataal', 4).
card_layout('nekrataal', 'normal').
card_power('nekrataal', 2).
card_toughness('nekrataal', 1).

% Found in: VAN
card_name('nekrataal avatar', 'Nekrataal Avatar').
card_type('nekrataal avatar', 'Vanguard').
card_types('nekrataal avatar', ['Vanguard']).
card_subtypes('nekrataal avatar', []).
card_colors('nekrataal avatar', []).
card_text('nekrataal avatar', 'Creature spells you cast cost {B} less to cast. This effect reduces only the amount of colored mana you pay.').
card_layout('nekrataal avatar', 'vanguard').

% Found in: C13, pJGP
card_name('nekusar, the mindrazer', 'Nekusar, the Mindrazer').
card_type('nekusar, the mindrazer', 'Legendary Creature — Zombie Wizard').
card_types('nekusar, the mindrazer', ['Creature']).
card_subtypes('nekusar, the mindrazer', ['Zombie', 'Wizard']).
card_supertypes('nekusar, the mindrazer', ['Legendary']).
card_colors('nekusar, the mindrazer', ['U', 'B', 'R']).
card_text('nekusar, the mindrazer', 'At the beginning of each player\'s draw step, that player draws an additional card.\nWhenever an opponent draws a card, Nekusar, the Mindrazer deals 1 damage to that player.').
card_mana_cost('nekusar, the mindrazer', ['2', 'U', 'B', 'R']).
card_cmc('nekusar, the mindrazer', 5).
card_layout('nekusar, the mindrazer', 'normal').
card_power('nekusar, the mindrazer', 2).
card_toughness('nekusar, the mindrazer', 4).

% Found in: ROE
card_name('nema siltlurker', 'Nema Siltlurker').
card_type('nema siltlurker', 'Creature — Lizard').
card_types('nema siltlurker', ['Creature']).
card_subtypes('nema siltlurker', ['Lizard']).
card_colors('nema siltlurker', ['G']).
card_text('nema siltlurker', '').
card_mana_cost('nema siltlurker', ['4', 'G']).
card_cmc('nema siltlurker', 5).
card_layout('nema siltlurker', 'normal').
card_power('nema siltlurker', 3).
card_toughness('nema siltlurker', 5).

% Found in: PLS
card_name('nemata, grove guardian', 'Nemata, Grove Guardian').
card_type('nemata, grove guardian', 'Legendary Creature — Treefolk').
card_types('nemata, grove guardian', ['Creature']).
card_subtypes('nemata, grove guardian', ['Treefolk']).
card_supertypes('nemata, grove guardian', ['Legendary']).
card_colors('nemata, grove guardian', ['G']).
card_text('nemata, grove guardian', '{2}{G}: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Saproling creatures get +1/+1 until end of turn.').
card_mana_cost('nemata, grove guardian', ['4', 'G', 'G']).
card_cmc('nemata, grove guardian', 6).
card_layout('nemata, grove guardian', 'normal').
card_power('nemata, grove guardian', 4).
card_toughness('nemata, grove guardian', 5).

% Found in: DST
card_name('nemesis mask', 'Nemesis Mask').
card_type('nemesis mask', 'Artifact — Equipment').
card_types('nemesis mask', ['Artifact']).
card_subtypes('nemesis mask', ['Equipment']).
card_colors('nemesis mask', []).
card_text('nemesis mask', 'All creatures able to block equipped creature do so.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('nemesis mask', ['3']).
card_cmc('nemesis mask', 3).
card_layout('nemesis mask', 'normal').

% Found in: THS
card_name('nemesis of mortals', 'Nemesis of Mortals').
card_type('nemesis of mortals', 'Creature — Snake').
card_types('nemesis of mortals', ['Creature']).
card_subtypes('nemesis of mortals', ['Snake']).
card_colors('nemesis of mortals', ['G']).
card_text('nemesis of mortals', 'Nemesis of Mortals costs {1} less to cast for each creature card in your graveyard.\n{7}{G}{G}: Monstrosity 5. This ability costs {1} less to activate for each creature card in your graveyard. (If this creature isn\'t monstrous, put five +1/+1 counters on it and it becomes monstrous.)').
card_mana_cost('nemesis of mortals', ['4', 'G', 'G']).
card_cmc('nemesis of mortals', 6).
card_layout('nemesis of mortals', 'normal').
card_power('nemesis of mortals', 5).
card_toughness('nemesis of mortals', 5).

% Found in: ARB
card_name('nemesis of reason', 'Nemesis of Reason').
card_type('nemesis of reason', 'Creature — Leviathan Horror').
card_types('nemesis of reason', ['Creature']).
card_subtypes('nemesis of reason', ['Leviathan', 'Horror']).
card_colors('nemesis of reason', ['U', 'B']).
card_text('nemesis of reason', 'Whenever Nemesis of Reason attacks, defending player puts the top ten cards of his or her library into his or her graveyard.').
card_mana_cost('nemesis of reason', ['3', 'U', 'B']).
card_cmc('nemesis of reason', 5).
card_layout('nemesis of reason', 'normal').
card_power('nemesis of reason', 3).
card_toughness('nemesis of reason', 7).

% Found in: CMD, WWK
card_name('nemesis trap', 'Nemesis Trap').
card_type('nemesis trap', 'Instant — Trap').
card_types('nemesis trap', ['Instant']).
card_subtypes('nemesis trap', ['Trap']).
card_colors('nemesis trap', ['B']).
card_text('nemesis trap', 'If a white creature is attacking, you may pay {B}{B} rather than pay Nemesis Trap\'s mana cost.\nExile target attacking creature. Put a token that\'s a copy of that creature onto the battlefield. Exile it at the beginning of the next end step.').
card_mana_cost('nemesis trap', ['4', 'B', 'B']).
card_cmc('nemesis trap', 6).
card_layout('nemesis trap', 'normal').

% Found in: PC2
card_name('nephalia', 'Nephalia').
card_type('nephalia', 'Plane — Innistrad').
card_types('nephalia', ['Plane']).
card_subtypes('nephalia', ['Innistrad']).
card_colors('nephalia', []).
card_text('nephalia', 'At the beginning of your end step, put the top seven cards of your library into your graveyard. Then return a card at random from your graveyard to your hand.\nWhenever you roll {C}, return target card from your graveyard to your hand.').
card_layout('nephalia', 'plane').

% Found in: ISD
card_name('nephalia drownyard', 'Nephalia Drownyard').
card_type('nephalia drownyard', 'Land').
card_types('nephalia drownyard', ['Land']).
card_subtypes('nephalia drownyard', []).
card_colors('nephalia drownyard', []).
card_text('nephalia drownyard', '{T}: Add {1} to your mana pool.\n{1}{U}{B}, {T}: Target player puts the top three cards of his or her library into his or her graveyard.').
card_layout('nephalia drownyard', 'normal').

% Found in: DKA, M14
card_name('nephalia seakite', 'Nephalia Seakite').
card_type('nephalia seakite', 'Creature — Bird').
card_types('nephalia seakite', ['Creature']).
card_subtypes('nephalia seakite', ['Bird']).
card_colors('nephalia seakite', ['U']).
card_text('nephalia seakite', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_mana_cost('nephalia seakite', ['3', 'U']).
card_cmc('nephalia seakite', 4).
card_layout('nephalia seakite', 'normal').
card_power('nephalia seakite', 2).
card_toughness('nephalia seakite', 3).

% Found in: AVR
card_name('nephalia smuggler', 'Nephalia Smuggler').
card_type('nephalia smuggler', 'Creature — Human Rogue').
card_types('nephalia smuggler', ['Creature']).
card_subtypes('nephalia smuggler', ['Human', 'Rogue']).
card_colors('nephalia smuggler', ['U']).
card_text('nephalia smuggler', '{3}{U}, {T}: Exile another target creature you control, then return that card to the battlefield under your control.').
card_mana_cost('nephalia smuggler', ['U']).
card_cmc('nephalia smuggler', 1).
card_layout('nephalia smuggler', 'normal').
card_power('nephalia smuggler', 1).
card_toughness('nephalia smuggler', 1).

% Found in: DDO, THS
card_name('nessian asp', 'Nessian Asp').
card_type('nessian asp', 'Creature — Snake').
card_types('nessian asp', ['Creature']).
card_subtypes('nessian asp', ['Snake']).
card_colors('nessian asp', ['G']).
card_text('nessian asp', 'Reach\n{6}{G}: Monstrosity 4. (If this creature isn\'t monstrous, put four +1/+1 counters on it and it becomes monstrous.)').
card_mana_cost('nessian asp', ['4', 'G']).
card_cmc('nessian asp', 5).
card_layout('nessian asp', 'normal').
card_power('nessian asp', 4).
card_toughness('nessian asp', 5).

% Found in: FUT, THS
card_name('nessian courser', 'Nessian Courser').
card_type('nessian courser', 'Creature — Centaur Warrior').
card_types('nessian courser', ['Creature']).
card_subtypes('nessian courser', ['Centaur', 'Warrior']).
card_colors('nessian courser', ['G']).
card_text('nessian courser', '').
card_mana_cost('nessian courser', ['2', 'G']).
card_cmc('nessian courser', 3).
card_layout('nessian courser', 'normal').
card_power('nessian courser', 3).
card_toughness('nessian courser', 3).

% Found in: BNG
card_name('nessian demolok', 'Nessian Demolok').
card_type('nessian demolok', 'Creature — Beast').
card_types('nessian demolok', ['Creature']).
card_subtypes('nessian demolok', ['Beast']).
card_colors('nessian demolok', ['G']).
card_text('nessian demolok', 'Tribute 3 (As this creature enters the battlefield, an opponent of your choice may place three +1/+1 counters on it.)\nWhen Nessian Demolok enters the battlefield, if tribute wasn\'t paid, destroy target noncreature permanent.').
card_mana_cost('nessian demolok', ['3', 'G', 'G']).
card_cmc('nessian demolok', 5).
card_layout('nessian demolok', 'normal').
card_power('nessian demolok', 3).
card_toughness('nessian demolok', 3).

% Found in: JOU
card_name('nessian game warden', 'Nessian Game Warden').
card_type('nessian game warden', 'Creature — Beast').
card_types('nessian game warden', ['Creature']).
card_subtypes('nessian game warden', ['Beast']).
card_colors('nessian game warden', ['G']).
card_text('nessian game warden', 'When Nessian Game Warden enters the battlefield, look at the top X cards of your library, where X is the number of Forests you control. You may reveal a creature card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('nessian game warden', ['3', 'G', 'G']).
card_cmc('nessian game warden', 5).
card_layout('nessian game warden', 'normal').
card_power('nessian game warden', 4).
card_toughness('nessian game warden', 5).

% Found in: BNG, pPRE
card_name('nessian wilds ravager', 'Nessian Wilds Ravager').
card_type('nessian wilds ravager', 'Creature — Hydra').
card_types('nessian wilds ravager', ['Creature']).
card_subtypes('nessian wilds ravager', ['Hydra']).
card_colors('nessian wilds ravager', ['G']).
card_text('nessian wilds ravager', 'Tribute 6 (As this creature enters the battlefield, an opponent of your choice may place six +1/+1 counters on it.)\nWhen Nessian Wilds Ravager enters the battlefield, if tribute wasn\'t paid, you may have Nessian Wilds Ravager fight another target creature. (Each deals damage equal to its power to the other.)').
card_mana_cost('nessian wilds ravager', ['4', 'G', 'G']).
card_cmc('nessian wilds ravager', 6).
card_layout('nessian wilds ravager', 'normal').
card_power('nessian wilds ravager', 6).
card_toughness('nessian wilds ravager', 6).

% Found in: MM2, PC2, ROE
card_name('nest invader', 'Nest Invader').
card_type('nest invader', 'Creature — Eldrazi Drone').
card_types('nest invader', ['Creature']).
card_subtypes('nest invader', ['Eldrazi', 'Drone']).
card_colors('nest invader', ['G']).
card_text('nest invader', 'When Nest Invader enters the battlefield, put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('nest invader', ['1', 'G']).
card_cmc('nest invader', 2).
card_layout('nest invader', 'normal').
card_power('nest invader', 2).
card_toughness('nest invader', 2).

% Found in: MBS
card_name('nested ghoul', 'Nested Ghoul').
card_type('nested ghoul', 'Creature — Zombie Warrior').
card_types('nested ghoul', ['Creature']).
card_subtypes('nested ghoul', ['Zombie', 'Warrior']).
card_colors('nested ghoul', ['B']).
card_text('nested ghoul', 'Whenever a source deals damage to Nested Ghoul, put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('nested ghoul', ['3', 'B', 'B']).
card_cmc('nested ghoul', 5).
card_layout('nested ghoul', 'normal').
card_power('nested ghoul', 4).
card_toughness('nested ghoul', 2).

% Found in: NMS
card_name('nesting wurm', 'Nesting Wurm').
card_type('nesting wurm', 'Creature — Wurm').
card_types('nesting wurm', ['Creature']).
card_subtypes('nesting wurm', ['Wurm']).
card_colors('nesting wurm', ['G']).
card_text('nesting wurm', 'Trample\nWhen Nesting Wurm enters the battlefield, you may search your library for up to three cards named Nesting Wurm, reveal them, and put them into your hand. If you do, shuffle your library.').
card_mana_cost('nesting wurm', ['4', 'G', 'G']).
card_cmc('nesting wurm', 6).
card_layout('nesting wurm', 'normal').
card_power('nesting wurm', 4).
card_toughness('nesting wurm', 3).

% Found in: DDO, M15
card_name('netcaster spider', 'Netcaster Spider').
card_type('netcaster spider', 'Creature — Spider').
card_types('netcaster spider', ['Creature']).
card_subtypes('netcaster spider', ['Spider']).
card_colors('netcaster spider', ['G']).
card_text('netcaster spider', 'Reach (This creature can block creatures with flying.)\nWhenever Netcaster Spider blocks a creature with flying, Netcaster Spider gets +2/+0 until end of turn.').
card_mana_cost('netcaster spider', ['2', 'G']).
card_cmc('netcaster spider', 3).
card_layout('netcaster spider', 'normal').
card_power('netcaster spider', 2).
card_toughness('netcaster spider', 3).

% Found in: M11
card_name('nether horror', 'Nether Horror').
card_type('nether horror', 'Creature — Horror').
card_types('nether horror', ['Creature']).
card_subtypes('nether horror', ['Horror']).
card_colors('nether horror', ['B']).
card_text('nether horror', '').
card_mana_cost('nether horror', ['3', 'B']).
card_cmc('nether horror', 4).
card_layout('nether horror', 'normal').
card_power('nether horror', 4).
card_toughness('nether horror', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED
card_name('nether shadow', 'Nether Shadow').
card_type('nether shadow', 'Creature — Spirit').
card_types('nether shadow', ['Creature']).
card_subtypes('nether shadow', ['Spirit']).
card_colors('nether shadow', ['B']).
card_text('nether shadow', 'Haste\nAt the beginning of your upkeep, if Nether Shadow is in your graveyard with three or more creature cards above it, you may put Nether Shadow onto the battlefield.').
card_mana_cost('nether shadow', ['B', 'B']).
card_cmc('nether shadow', 2).
card_layout('nether shadow', 'normal').
card_power('nether shadow', 1).
card_toughness('nether shadow', 1).

% Found in: MMQ
card_name('nether spirit', 'Nether Spirit').
card_type('nether spirit', 'Creature — Spirit').
card_types('nether spirit', ['Creature']).
card_subtypes('nether spirit', ['Spirit']).
card_colors('nether spirit', ['B']).
card_text('nether spirit', 'At the beginning of your upkeep, if Nether Spirit is the only creature card in your graveyard, you may return Nether Spirit to the battlefield.').
card_mana_cost('nether spirit', ['1', 'B', 'B']).
card_cmc('nether spirit', 3).
card_layout('nether spirit', 'normal').
card_power('nether spirit', 2).
card_toughness('nether spirit', 2).

% Found in: TSP
card_name('nether traitor', 'Nether Traitor').
card_type('nether traitor', 'Creature — Spirit').
card_types('nether traitor', ['Creature']).
card_subtypes('nether traitor', ['Spirit']).
card_colors('nether traitor', ['B']).
card_text('nether traitor', 'Haste\nShadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever another creature is put into your graveyard from the battlefield, you may pay {B}. If you do, return Nether Traitor from your graveyard to the battlefield.').
card_mana_cost('nether traitor', ['B', 'B']).
card_cmc('nether traitor', 2).
card_layout('nether traitor', 'normal').
card_power('nether traitor', 1).
card_toughness('nether traitor', 1).

% Found in: LEG, ME3
card_name('nether void', 'Nether Void').
card_type('nether void', 'World Enchantment').
card_types('nether void', ['Enchantment']).
card_subtypes('nether void', []).
card_supertypes('nether void', ['World']).
card_colors('nether void', ['B']).
card_text('nether void', 'Whenever a player casts a spell, counter it unless that player pays {3}.').
card_mana_cost('nether void', ['3', 'B']).
card_cmc('nether void', 4).
card_layout('nether void', 'normal').
card_reserved('nether void').

% Found in: RAV
card_name('netherborn phalanx', 'Netherborn Phalanx').
card_type('netherborn phalanx', 'Creature — Horror').
card_types('netherborn phalanx', ['Creature']).
card_subtypes('netherborn phalanx', ['Horror']).
card_colors('netherborn phalanx', ['B']).
card_text('netherborn phalanx', 'When Netherborn Phalanx enters the battlefield, each opponent loses 1 life for each creature he or she controls.\nTransmute {1}{B}{B} ({1}{B}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('netherborn phalanx', ['5', 'B']).
card_cmc('netherborn phalanx', 6).
card_layout('netherborn phalanx', 'normal').
card_power('netherborn phalanx', 2).
card_toughness('netherborn phalanx', 4).

% Found in: NMS
card_name('netter en-dal', 'Netter en-Dal').
card_type('netter en-dal', 'Creature — Human Spellshaper').
card_types('netter en-dal', ['Creature']).
card_subtypes('netter en-dal', ['Human', 'Spellshaper']).
card_colors('netter en-dal', ['W']).
card_text('netter en-dal', '{W}, {T}, Discard a card: Target creature can\'t attack this turn.').
card_mana_cost('netter en-dal', ['W']).
card_cmc('netter en-dal', 1).
card_layout('netter en-dal', 'normal').
card_power('netter en-dal', 1).
card_toughness('netter en-dal', 1).

% Found in: BFZ
card_name('nettle drone', 'Nettle Drone').
card_type('nettle drone', 'Creature — Eldrazi Drone').
card_types('nettle drone', ['Creature']).
card_subtypes('nettle drone', ['Eldrazi', 'Drone']).
card_colors('nettle drone', []).
card_text('nettle drone', 'Devoid (This card has no color.)\n{T}: Nettle Drone deals 1 damage to each opponent.\nWhenever you cast a colorless spell, untap Nettle Drone.').
card_mana_cost('nettle drone', ['2', 'R']).
card_cmc('nettle drone', 3).
card_layout('nettle drone', 'normal').
card_power('nettle drone', 3).
card_toughness('nettle drone', 1).

% Found in: EVE
card_name('nettle sentinel', 'Nettle Sentinel').
card_type('nettle sentinel', 'Creature — Elf Warrior').
card_types('nettle sentinel', ['Creature']).
card_subtypes('nettle sentinel', ['Elf', 'Warrior']).
card_colors('nettle sentinel', ['G']).
card_text('nettle sentinel', 'Nettle Sentinel doesn\'t untap during your untap step.\nWhenever you cast a green spell, you may untap Nettle Sentinel.').
card_mana_cost('nettle sentinel', ['G']).
card_cmc('nettle sentinel', 1).
card_layout('nettle sentinel', 'normal').
card_power('nettle sentinel', 2).
card_toughness('nettle sentinel', 2).

% Found in: AVR
card_name('nettle swine', 'Nettle Swine').
card_type('nettle swine', 'Creature — Boar').
card_types('nettle swine', ['Creature']).
card_subtypes('nettle swine', ['Boar']).
card_colors('nettle swine', ['G']).
card_text('nettle swine', '').
card_mana_cost('nettle swine', ['3', 'G']).
card_cmc('nettle swine', 4).
card_layout('nettle swine', 'normal').
card_power('nettle swine', 4).
card_toughness('nettle swine', 3).

% Found in: MIR
card_name('nettletooth djinn', 'Nettletooth Djinn').
card_type('nettletooth djinn', 'Creature — Djinn').
card_types('nettletooth djinn', ['Creature']).
card_subtypes('nettletooth djinn', ['Djinn']).
card_colors('nettletooth djinn', ['G']).
card_text('nettletooth djinn', 'At the beginning of your upkeep, Nettletooth Djinn deals 1 damage to you.').
card_mana_cost('nettletooth djinn', ['3', 'G']).
card_cmc('nettletooth djinn', 4).
card_layout('nettletooth djinn', 'normal').
card_power('nettletooth djinn', 4).
card_toughness('nettletooth djinn', 4).

% Found in: LRW
card_name('nettlevine blight', 'Nettlevine Blight').
card_type('nettlevine blight', 'Enchantment — Aura').
card_types('nettlevine blight', ['Enchantment']).
card_subtypes('nettlevine blight', ['Aura']).
card_colors('nettlevine blight', ['B']).
card_text('nettlevine blight', 'Enchant creature or land\nEnchanted permanent has \"At the beginning of your end step, sacrifice this permanent and attach Nettlevine Blight to a creature or land you control.\"').
card_mana_cost('nettlevine blight', ['4', 'B', 'B']).
card_cmc('nettlevine blight', 6).
card_layout('nettlevine blight', 'normal').

% Found in: DIS
card_name('nettling curse', 'Nettling Curse').
card_type('nettling curse', 'Enchantment — Aura').
card_types('nettling curse', ['Enchantment']).
card_subtypes('nettling curse', ['Aura']).
card_colors('nettling curse', ['B']).
card_text('nettling curse', 'Enchant creature\nWhenever enchanted creature attacks or blocks, its controller loses 3 life.\n{1}{R}: Enchanted creature attacks this turn if able.').
card_mana_cost('nettling curse', ['2', 'B']).
card_cmc('nettling curse', 3).
card_layout('nettling curse', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('nettling imp', 'Nettling Imp').
card_type('nettling imp', 'Creature — Imp').
card_types('nettling imp', ['Creature']).
card_subtypes('nettling imp', ['Imp']).
card_colors('nettling imp', ['B']).
card_text('nettling imp', '{T}: Choose target non-Wall creature the active player has controlled continuously since the beginning of the turn. That creature attacks this turn if able. If it doesn\'t, destroy it at the beginning of the next end step. Activate this ability only during an opponent\'s turn, before attackers are declared.').
card_mana_cost('nettling imp', ['2', 'B']).
card_cmc('nettling imp', 3).
card_layout('nettling imp', 'normal').
card_power('nettling imp', 1).
card_toughness('nettling imp', 1).

% Found in: MBS
card_name('neurok commando', 'Neurok Commando').
card_type('neurok commando', 'Creature — Human Rogue').
card_types('neurok commando', ['Creature']).
card_subtypes('neurok commando', ['Human', 'Rogue']).
card_colors('neurok commando', ['U']).
card_text('neurok commando', 'Shroud (This creature can\'t be the target of spells or abilities.)\nWhenever Neurok Commando deals combat damage to a player, you may draw a card.').
card_mana_cost('neurok commando', ['1', 'U', 'U']).
card_cmc('neurok commando', 3).
card_layout('neurok commando', 'normal').
card_power('neurok commando', 2).
card_toughness('neurok commando', 1).

% Found in: MRD
card_name('neurok familiar', 'Neurok Familiar').
card_type('neurok familiar', 'Creature — Bird').
card_types('neurok familiar', ['Creature']).
card_subtypes('neurok familiar', ['Bird']).
card_colors('neurok familiar', ['U']).
card_text('neurok familiar', 'Flying\nWhen Neurok Familiar enters the battlefield, reveal the top card of your library. If it\'s an artifact card, put it into your hand. Otherwise, put it into your graveyard.').
card_mana_cost('neurok familiar', ['1', 'U']).
card_cmc('neurok familiar', 2).
card_layout('neurok familiar', 'normal').
card_power('neurok familiar', 1).
card_toughness('neurok familiar', 1).

% Found in: MRD
card_name('neurok hoversail', 'Neurok Hoversail').
card_type('neurok hoversail', 'Artifact — Equipment').
card_types('neurok hoversail', ['Artifact']).
card_subtypes('neurok hoversail', ['Equipment']).
card_colors('neurok hoversail', []).
card_text('neurok hoversail', 'Equipped creature has flying.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('neurok hoversail', ['1']).
card_cmc('neurok hoversail', 1).
card_layout('neurok hoversail', 'normal').

% Found in: DDI, SOM
card_name('neurok invisimancer', 'Neurok Invisimancer').
card_type('neurok invisimancer', 'Creature — Human Wizard').
card_types('neurok invisimancer', ['Creature']).
card_subtypes('neurok invisimancer', ['Human', 'Wizard']).
card_colors('neurok invisimancer', ['U']).
card_text('neurok invisimancer', 'Neurok Invisimancer can\'t be blocked.\nWhen Neurok Invisimancer enters the battlefield, target creature can\'t be blocked this turn.').
card_mana_cost('neurok invisimancer', ['1', 'U', 'U']).
card_cmc('neurok invisimancer', 3).
card_layout('neurok invisimancer', 'normal').
card_power('neurok invisimancer', 2).
card_toughness('neurok invisimancer', 1).

% Found in: DST
card_name('neurok prodigy', 'Neurok Prodigy').
card_type('neurok prodigy', 'Creature — Human Wizard').
card_types('neurok prodigy', ['Creature']).
card_subtypes('neurok prodigy', ['Human', 'Wizard']).
card_colors('neurok prodigy', ['U']).
card_text('neurok prodigy', 'Flying\nDiscard an artifact card: Return Neurok Prodigy to its owner\'s hand.').
card_mana_cost('neurok prodigy', ['2', 'U']).
card_cmc('neurok prodigy', 3).
card_layout('neurok prodigy', 'normal').
card_power('neurok prodigy', 2).
card_toughness('neurok prodigy', 1).

% Found in: SOM
card_name('neurok replica', 'Neurok Replica').
card_type('neurok replica', 'Artifact Creature — Wizard').
card_types('neurok replica', ['Artifact', 'Creature']).
card_subtypes('neurok replica', ['Wizard']).
card_colors('neurok replica', []).
card_text('neurok replica', '{1}{U}, Sacrifice Neurok Replica: Return target creature to its owner\'s hand.').
card_mana_cost('neurok replica', ['3']).
card_cmc('neurok replica', 3).
card_layout('neurok replica', 'normal').
card_power('neurok replica', 1).
card_toughness('neurok replica', 4).

% Found in: MRD
card_name('neurok spy', 'Neurok Spy').
card_type('neurok spy', 'Creature — Human Rogue').
card_types('neurok spy', ['Creature']).
card_subtypes('neurok spy', ['Human', 'Rogue']).
card_colors('neurok spy', ['U']).
card_text('neurok spy', 'Neurok Spy can\'t be blocked as long as defending player controls an artifact.').
card_mana_cost('neurok spy', ['2', 'U']).
card_cmc('neurok spy', 3).
card_layout('neurok spy', 'normal').
card_power('neurok spy', 2).
card_toughness('neurok spy', 2).

% Found in: 5DN
card_name('neurok stealthsuit', 'Neurok Stealthsuit').
card_type('neurok stealthsuit', 'Artifact — Equipment').
card_types('neurok stealthsuit', ['Artifact']).
card_subtypes('neurok stealthsuit', ['Equipment']).
card_colors('neurok stealthsuit', []).
card_text('neurok stealthsuit', 'Equipped creature has shroud. (It can\'t be the target of spells or abilities.)\n{U}{U}: Attach Neurok Stealthsuit to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('neurok stealthsuit', ['2']).
card_cmc('neurok stealthsuit', 2).
card_layout('neurok stealthsuit', 'normal').

% Found in: DST
card_name('neurok transmuter', 'Neurok Transmuter').
card_type('neurok transmuter', 'Creature — Human Wizard').
card_types('neurok transmuter', ['Creature']).
card_subtypes('neurok transmuter', ['Human', 'Wizard']).
card_colors('neurok transmuter', ['U']).
card_text('neurok transmuter', '{U}: Target creature becomes an artifact in addition to its other types until end of turn.\n{U}: Until end of turn, target artifact creature becomes blue and isn\'t an artifact.').
card_mana_cost('neurok transmuter', ['2', 'U']).
card_cmc('neurok transmuter', 3).
card_layout('neurok transmuter', 'normal').
card_power('neurok transmuter', 2).
card_toughness('neurok transmuter', 2).

% Found in: FRF
card_name('neutralizing blast', 'Neutralizing Blast').
card_type('neutralizing blast', 'Instant').
card_types('neutralizing blast', ['Instant']).
card_subtypes('neutralizing blast', []).
card_colors('neutralizing blast', ['U']).
card_text('neutralizing blast', 'Counter target multicolored spell.').
card_mana_cost('neutralizing blast', ['1', 'U']).
card_cmc('neutralizing blast', 2).
card_layout('neutralizing blast', 'normal').

% Found in: SOK
card_name('neverending torment', 'Neverending Torment').
card_type('neverending torment', 'Sorcery').
card_types('neverending torment', ['Sorcery']).
card_subtypes('neverending torment', []).
card_colors('neverending torment', ['B']).
card_text('neverending torment', 'Search target player\'s library for X cards, where X is the number of cards in your hand, and exile them. Then that player shuffles his or her library.\nEpic (For the rest of the game, you can\'t cast spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability. You may choose a new target for the copy.)').
card_mana_cost('neverending torment', ['4', 'B', 'B']).
card_cmc('neverending torment', 6).
card_layout('neverending torment', 'normal').

% Found in: MOR
card_name('nevermaker', 'Nevermaker').
card_type('nevermaker', 'Creature — Elemental').
card_types('nevermaker', ['Creature']).
card_subtypes('nevermaker', ['Elemental']).
card_colors('nevermaker', ['U']).
card_text('nevermaker', 'Flying\nWhen Nevermaker leaves the battlefield, put target nonland permanent on top of its owner\'s library.\nEvoke {3}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('nevermaker', ['3', 'U']).
card_cmc('nevermaker', 4).
card_layout('nevermaker', 'normal').
card_power('nevermaker', 2).
card_toughness('nevermaker', 3).

% Found in: ISD
card_name('nevermore', 'Nevermore').
card_type('nevermore', 'Enchantment').
card_types('nevermore', ['Enchantment']).
card_subtypes('nevermore', []).
card_colors('nevermore', ['W']).
card_text('nevermore', 'As Nevermore enters the battlefield, name a nonland card.\nThe named card can\'t be cast.').
card_mana_cost('nevermore', ['1', 'W', 'W']).
card_cmc('nevermore', 3).
card_layout('nevermore', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, ATH, C13, C14, CED, CEI, LEA, LEB, MED, V10, VMA
card_name('nevinyrral\'s disk', 'Nevinyrral\'s Disk').
card_type('nevinyrral\'s disk', 'Artifact').
card_types('nevinyrral\'s disk', ['Artifact']).
card_subtypes('nevinyrral\'s disk', []).
card_colors('nevinyrral\'s disk', []).
card_text('nevinyrral\'s disk', 'Nevinyrral\'s Disk enters the battlefield tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_mana_cost('nevinyrral\'s disk', ['4']).
card_cmc('nevinyrral\'s disk', 4).
card_layout('nevinyrral\'s disk', 'normal').

% Found in: C13, DDI, DDL, FUT
card_name('new benalia', 'New Benalia').
card_type('new benalia', 'Land').
card_types('new benalia', ['Land']).
card_subtypes('new benalia', []).
card_colors('new benalia', []).
card_text('new benalia', 'New Benalia enters the battlefield tapped.\nWhen New Benalia enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {W} to your mana pool.').
card_layout('new benalia', 'normal').

% Found in: ODY
card_name('new frontiers', 'New Frontiers').
card_type('new frontiers', 'Sorcery').
card_types('new frontiers', ['Sorcery']).
card_subtypes('new frontiers', []).
card_colors('new frontiers', ['G']).
card_text('new frontiers', 'Each player may search his or her library for up to X basic land cards and put them onto the battlefield tapped. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('new frontiers', ['X', 'G']).
card_cmc('new frontiers', 1).
card_layout('new frontiers', 'normal').

% Found in: RTR
card_name('new prahv guildmage', 'New Prahv Guildmage').
card_type('new prahv guildmage', 'Creature — Human Wizard').
card_types('new prahv guildmage', ['Creature']).
card_subtypes('new prahv guildmage', ['Human', 'Wizard']).
card_colors('new prahv guildmage', ['W', 'U']).
card_text('new prahv guildmage', '{W}{U}: Target creature gains flying until end of turn.\n{3}{W}{U}: Detain target nonland permanent an opponent controls. (Until your next turn, that permanent can\'t attack or block and its activated abilities can\'t be activated.)').
card_mana_cost('new prahv guildmage', ['W', 'U']).
card_cmc('new prahv guildmage', 2).
card_layout('new prahv guildmage', 'normal').
card_power('new prahv guildmage', 2).
card_toughness('new prahv guildmage', 2).

% Found in: CHK
card_name('nezumi bone-reader', 'Nezumi Bone-Reader').
card_type('nezumi bone-reader', 'Creature — Rat Shaman').
card_types('nezumi bone-reader', ['Creature']).
card_subtypes('nezumi bone-reader', ['Rat', 'Shaman']).
card_colors('nezumi bone-reader', ['B']).
card_text('nezumi bone-reader', '{B}, Sacrifice a creature: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('nezumi bone-reader', ['1', 'B']).
card_cmc('nezumi bone-reader', 2).
card_layout('nezumi bone-reader', 'normal').
card_power('nezumi bone-reader', 1).
card_toughness('nezumi bone-reader', 1).

% Found in: CHK
card_name('nezumi cutthroat', 'Nezumi Cutthroat').
card_type('nezumi cutthroat', 'Creature — Rat Warrior').
card_types('nezumi cutthroat', ['Creature']).
card_subtypes('nezumi cutthroat', ['Rat', 'Warrior']).
card_colors('nezumi cutthroat', ['B']).
card_text('nezumi cutthroat', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nNezumi Cutthroat can\'t block.').
card_mana_cost('nezumi cutthroat', ['1', 'B']).
card_cmc('nezumi cutthroat', 2).
card_layout('nezumi cutthroat', 'normal').
card_power('nezumi cutthroat', 2).
card_toughness('nezumi cutthroat', 1).

% Found in: CHK, CMD
card_name('nezumi graverobber', 'Nezumi Graverobber').
card_type('nezumi graverobber', 'Creature — Rat Rogue').
card_types('nezumi graverobber', ['Creature']).
card_subtypes('nezumi graverobber', ['Rat', 'Rogue']).
card_colors('nezumi graverobber', ['B']).
card_text('nezumi graverobber', '{1}{B}: Exile target card from an opponent\'s graveyard. If no cards are in that graveyard, flip Nezumi Graverobber.').
card_mana_cost('nezumi graverobber', ['1', 'B']).
card_cmc('nezumi graverobber', 2).
card_layout('nezumi graverobber', 'flip').
card_power('nezumi graverobber', 2).
card_toughness('nezumi graverobber', 1).
card_sides('nezumi graverobber', 'nighteyes the desecrator').

% Found in: CHK
card_name('nezumi ronin', 'Nezumi Ronin').
card_type('nezumi ronin', 'Creature — Rat Samurai').
card_types('nezumi ronin', ['Creature']).
card_subtypes('nezumi ronin', ['Rat', 'Samurai']).
card_colors('nezumi ronin', ['B']).
card_text('nezumi ronin', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('nezumi ronin', ['2', 'B']).
card_cmc('nezumi ronin', 3).
card_layout('nezumi ronin', 'normal').
card_power('nezumi ronin', 3).
card_toughness('nezumi ronin', 1).

% Found in: BOK
card_name('nezumi shadow-watcher', 'Nezumi Shadow-Watcher').
card_type('nezumi shadow-watcher', 'Creature — Rat Warrior').
card_types('nezumi shadow-watcher', ['Creature']).
card_subtypes('nezumi shadow-watcher', ['Rat', 'Warrior']).
card_colors('nezumi shadow-watcher', ['B']).
card_text('nezumi shadow-watcher', 'Sacrifice Nezumi Shadow-Watcher: Destroy target Ninja.').
card_mana_cost('nezumi shadow-watcher', ['B']).
card_cmc('nezumi shadow-watcher', 1).
card_layout('nezumi shadow-watcher', 'normal').
card_power('nezumi shadow-watcher', 1).
card_toughness('nezumi shadow-watcher', 1).

% Found in: CHK
card_name('nezumi shortfang', 'Nezumi Shortfang').
card_type('nezumi shortfang', 'Creature — Rat Rogue').
card_types('nezumi shortfang', ['Creature']).
card_subtypes('nezumi shortfang', ['Rat', 'Rogue']).
card_colors('nezumi shortfang', ['B']).
card_text('nezumi shortfang', '{1}{B}, {T}: Target opponent discards a card. Then if that player has no cards in hand, flip Nezumi Shortfang.').
card_mana_cost('nezumi shortfang', ['1', 'B']).
card_cmc('nezumi shortfang', 2).
card_layout('nezumi shortfang', 'flip').
card_power('nezumi shortfang', 1).
card_toughness('nezumi shortfang', 1).
card_sides('nezumi shortfang', 'stabwhisker the odious').

% Found in: DRK
card_name('niall silvain', 'Niall Silvain').
card_type('niall silvain', 'Creature — Ouphe').
card_types('niall silvain', ['Creature']).
card_subtypes('niall silvain', ['Ouphe']).
card_colors('niall silvain', ['G']).
card_text('niall silvain', '{G}{G}{G}{G}, {T}: Regenerate target creature.').
card_mana_cost('niall silvain', ['G', 'G', 'G']).
card_cmc('niall silvain', 3).
card_layout('niall silvain', 'normal').
card_power('niall silvain', 2).
card_toughness('niall silvain', 2).
card_reserved('niall silvain').

% Found in: DKA
card_name('niblis of the breath', 'Niblis of the Breath').
card_type('niblis of the breath', 'Creature — Spirit').
card_types('niblis of the breath', ['Creature']).
card_subtypes('niblis of the breath', ['Spirit']).
card_colors('niblis of the breath', ['U']).
card_text('niblis of the breath', 'Flying\n{U}, {T}: You may tap or untap target creature.').
card_mana_cost('niblis of the breath', ['2', 'U']).
card_cmc('niblis of the breath', 3).
card_layout('niblis of the breath', 'normal').
card_power('niblis of the breath', 2).
card_toughness('niblis of the breath', 1).

% Found in: DKA
card_name('niblis of the mist', 'Niblis of the Mist').
card_type('niblis of the mist', 'Creature — Spirit').
card_types('niblis of the mist', ['Creature']).
card_subtypes('niblis of the mist', ['Spirit']).
card_colors('niblis of the mist', ['W']).
card_text('niblis of the mist', 'Flying\nWhen Niblis of the Mist enters the battlefield, you may tap target creature.').
card_mana_cost('niblis of the mist', ['2', 'W']).
card_cmc('niblis of the mist', 3).
card_layout('niblis of the mist', 'normal').
card_power('niblis of the mist', 2).
card_toughness('niblis of the mist', 1).

% Found in: DKA
card_name('niblis of the urn', 'Niblis of the Urn').
card_type('niblis of the urn', 'Creature — Spirit').
card_types('niblis of the urn', ['Creature']).
card_subtypes('niblis of the urn', ['Spirit']).
card_colors('niblis of the urn', ['W']).
card_text('niblis of the urn', 'Flying\nWhenever Niblis of the Urn attacks, you may tap target creature.').
card_mana_cost('niblis of the urn', ['1', 'W']).
card_cmc('niblis of the urn', 2).
card_layout('niblis of the urn', 'normal').
card_power('niblis of the urn', 1).
card_toughness('niblis of the urn', 1).

% Found in: pHHO
card_name('nice', 'Nice').
card_type('nice', 'Sorcery').
card_types('nice', ['Sorcery']).
card_subtypes('nice', []).
card_colors('nice', ['W']).
card_text('nice', 'Search your library for a card and put it into another target player\'s hand. Then shuffle your library.').
card_mana_cost('nice', ['1', 'W', 'W']).
card_cmc('nice', 3).
card_layout('nice', 'split').

% Found in: CHR, DRB, LEG, ME3, TSB
card_name('nicol bolas', 'Nicol Bolas').
card_type('nicol bolas', 'Legendary Creature — Elder Dragon').
card_types('nicol bolas', ['Creature']).
card_subtypes('nicol bolas', ['Elder', 'Dragon']).
card_supertypes('nicol bolas', ['Legendary']).
card_colors('nicol bolas', ['U', 'B', 'R']).
card_text('nicol bolas', 'Flying\nAt the beginning of your upkeep, sacrifice Nicol Bolas unless you pay {U}{B}{R}.\nWhenever Nicol Bolas deals damage to an opponent, that player discards his or her hand.').
card_mana_cost('nicol bolas', ['2', 'U', 'U', 'B', 'B', 'R', 'R']).
card_cmc('nicol bolas', 8).
card_layout('nicol bolas', 'normal').
card_power('nicol bolas', 7).
card_toughness('nicol bolas', 7).

% Found in: CON, DDH, M13
card_name('nicol bolas, planeswalker', 'Nicol Bolas, Planeswalker').
card_type('nicol bolas, planeswalker', 'Planeswalker — Bolas').
card_types('nicol bolas, planeswalker', ['Planeswalker']).
card_subtypes('nicol bolas, planeswalker', ['Bolas']).
card_colors('nicol bolas, planeswalker', ['U', 'B', 'R']).
card_text('nicol bolas, planeswalker', '+3: Destroy target noncreature permanent.\n−2: Gain control of target creature.\n−9: Nicol Bolas, Planeswalker deals 7 damage to target player. That player discards seven cards, then sacrifices seven permanents.').
card_mana_cost('nicol bolas, planeswalker', ['4', 'U', 'B', 'B', 'R']).
card_cmc('nicol bolas, planeswalker', 8).
card_layout('nicol bolas, planeswalker', 'normal').
card_loyalty('nicol bolas, planeswalker', 5).

% Found in: APC
card_name('night', 'Night').
card_type('night', 'Instant').
card_types('night', ['Instant']).
card_subtypes('night', []).
card_colors('night', ['B']).
card_text('night', 'Target creature gets -1/-1 until end of turn.').
card_mana_cost('night', ['B']).
card_cmc('night', 1).
card_layout('night', 'split').
card_sides('night', 'day').

% Found in: CHK
card_name('night dealings', 'Night Dealings').
card_type('night dealings', 'Enchantment').
card_types('night dealings', ['Enchantment']).
card_subtypes('night dealings', []).
card_colors('night dealings', ['B']).
card_text('night dealings', 'Whenever a source you control deals damage to another player, put that many theft counters on Night Dealings.\n{2}{B}{B}, Remove X theft counters from Night Dealings: Search your library for a nonland card with converted mana cost X, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('night dealings', ['2', 'B', 'B']).
card_cmc('night dealings', 4).
card_layout('night dealings', 'normal').

% Found in: CHK
card_name('night of souls\' betrayal', 'Night of Souls\' Betrayal').
card_type('night of souls\' betrayal', 'Legendary Enchantment').
card_types('night of souls\' betrayal', ['Enchantment']).
card_subtypes('night of souls\' betrayal', []).
card_supertypes('night of souls\' betrayal', ['Legendary']).
card_colors('night of souls\' betrayal', ['B']).
card_text('night of souls\' betrayal', 'All creatures get -1/-1.').
card_mana_cost('night of souls\' betrayal', ['2', 'B', 'B']).
card_cmc('night of souls\' betrayal', 4).
card_layout('night of souls\' betrayal', 'normal').

% Found in: ISD
card_name('night revelers', 'Night Revelers').
card_type('night revelers', 'Creature — Vampire').
card_types('night revelers', ['Creature']).
card_subtypes('night revelers', ['Vampire']).
card_colors('night revelers', ['R']).
card_text('night revelers', 'Night Revelers has haste as long as an opponent controls a Human.').
card_mana_cost('night revelers', ['4', 'R']).
card_cmc('night revelers', 5).
card_layout('night revelers', 'normal').
card_power('night revelers', 4).
card_toughness('night revelers', 4).

% Found in: C13, FEM, ME2
card_name('night soil', 'Night Soil').
card_type('night soil', 'Enchantment').
card_types('night soil', ['Enchantment']).
card_subtypes('night soil', []).
card_colors('night soil', ['G']).
card_text('night soil', '{1}, Exile two creature cards from a single graveyard: Put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('night soil', ['G', 'G']).
card_cmc('night soil', 2).
card_layout('night soil', 'normal').

% Found in: ISD
card_name('night terrors', 'Night Terrors').
card_type('night terrors', 'Sorcery').
card_types('night terrors', ['Sorcery']).
card_subtypes('night terrors', []).
card_colors('night terrors', ['B']).
card_text('night terrors', 'Target player reveals his or her hand. You choose a nonland card from it. Exile that card.').
card_mana_cost('night terrors', ['2', 'B']).
card_cmc('night terrors', 3).
card_layout('night terrors', 'normal').

% Found in: 5DN, DDM
card_name('night\'s whisper', 'Night\'s Whisper').
card_type('night\'s whisper', 'Sorcery').
card_types('night\'s whisper', ['Sorcery']).
card_subtypes('night\'s whisper', []).
card_colors('night\'s whisper', ['B']).
card_text('night\'s whisper', 'You draw two cards and you lose 2 life.').
card_mana_cost('night\'s whisper', ['1', 'B']).
card_cmc('night\'s whisper', 2).
card_layout('night\'s whisper', 'normal').

% Found in: ISD
card_name('nightbird\'s clutches', 'Nightbird\'s Clutches').
card_type('nightbird\'s clutches', 'Sorcery').
card_types('nightbird\'s clutches', ['Sorcery']).
card_subtypes('nightbird\'s clutches', []).
card_colors('nightbird\'s clutches', ['R']).
card_text('nightbird\'s clutches', 'Up to two target creatures can\'t block this turn.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('nightbird\'s clutches', ['1', 'R']).
card_cmc('nightbird\'s clutches', 2).
card_layout('nightbird\'s clutches', 'normal').

% Found in: DIS
card_name('nightcreep', 'Nightcreep').
card_type('nightcreep', 'Instant').
card_types('nightcreep', ['Instant']).
card_subtypes('nightcreep', []).
card_colors('nightcreep', ['B']).
card_text('nightcreep', 'Until end of turn, all creatures become black and all lands become Swamps.').
card_mana_cost('nightcreep', ['B', 'B']).
card_cmc('nightcreep', 2).
card_layout('nightcreep', 'normal').

% Found in: CHK, CMD
card_name('nighteyes the desecrator', 'Nighteyes the Desecrator').
card_type('nighteyes the desecrator', 'Legendary Creature — Rat Wizard').
card_types('nighteyes the desecrator', ['Creature']).
card_subtypes('nighteyes the desecrator', ['Rat', 'Wizard']).
card_supertypes('nighteyes the desecrator', ['Legendary']).
card_colors('nighteyes the desecrator', ['B']).
card_text('nighteyes the desecrator', '{4}{B}: Put target creature card from a graveyard onto the battlefield under your control.').
card_mana_cost('nighteyes the desecrator', ['1', 'B']).
card_cmc('nighteyes the desecrator', 2).
card_layout('nighteyes the desecrator', 'flip').
card_power('nighteyes the desecrator', 4).
card_toughness('nighteyes the desecrator', 2).

% Found in: ISD
card_name('nightfall predator', 'Nightfall Predator').
card_type('nightfall predator', 'Creature — Werewolf').
card_types('nightfall predator', ['Creature']).
card_subtypes('nightfall predator', ['Werewolf']).
card_colors('nightfall predator', ['G']).
card_text('nightfall predator', '{R}, {T}: Nightfall Predator fights target creature. (Each deals damage equal to its power to the other.)\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Nightfall Predator.').
card_layout('nightfall predator', 'double-faced').
card_power('nightfall predator', 4).
card_toughness('nightfall predator', 4).

% Found in: M15
card_name('nightfire giant', 'Nightfire Giant').
card_type('nightfire giant', 'Creature — Zombie Giant').
card_types('nightfire giant', ['Creature']).
card_subtypes('nightfire giant', ['Zombie', 'Giant']).
card_colors('nightfire giant', ['B']).
card_text('nightfire giant', 'Nightfire Giant gets +1/+1 as long as you control a Mountain.\n{4}{R}: Nightfire Giant deals 2 damage to target creature or player.').
card_mana_cost('nightfire giant', ['4', 'B']).
card_cmc('nightfire giant', 5).
card_layout('nightfire giant', 'normal').
card_power('nightfire giant', 4).
card_toughness('nightfire giant', 3).

% Found in: RAV
card_name('nightguard patrol', 'Nightguard Patrol').
card_type('nightguard patrol', 'Creature — Human Soldier').
card_types('nightguard patrol', ['Creature']).
card_subtypes('nightguard patrol', ['Human', 'Soldier']).
card_colors('nightguard patrol', ['W']).
card_text('nightguard patrol', 'First strike, vigilance').
card_mana_cost('nightguard patrol', ['2', 'W']).
card_cmc('nightguard patrol', 3).
card_layout('nightguard patrol', 'normal').
card_power('nightguard patrol', 2).
card_toughness('nightguard patrol', 1).

% Found in: ROE
card_name('nighthaze', 'Nighthaze').
card_type('nighthaze', 'Sorcery').
card_types('nighthaze', ['Sorcery']).
card_subtypes('nighthaze', []).
card_colors('nighthaze', ['B']).
card_text('nighthaze', 'Target creature gains swampwalk until end of turn. (It can\'t be blocked as long as defending player controls a Swamp.)\nDraw a card.').
card_mana_cost('nighthaze', ['B']).
card_cmc('nighthaze', 1).
card_layout('nighthaze', 'normal').

% Found in: THS, pMGD
card_name('nighthowler', 'Nighthowler').
card_type('nighthowler', 'Enchantment Creature — Horror').
card_types('nighthowler', ['Enchantment', 'Creature']).
card_subtypes('nighthowler', ['Horror']).
card_colors('nighthowler', ['B']).
card_text('nighthowler', 'Bestow {2}{B}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nNighthowler and enchanted creature each get +X/+X, where X is the number of creature cards in all graveyards.').
card_mana_cost('nighthowler', ['1', 'B', 'B']).
card_cmc('nighthowler', 3).
card_layout('nighthowler', 'normal').
card_power('nighthowler', 0).
card_toughness('nighthowler', 0).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, LEA, LEB, M10, M14, M15, ORI
card_name('nightmare', 'Nightmare').
card_type('nightmare', 'Creature — Nightmare Horse').
card_types('nightmare', ['Creature']).
card_subtypes('nightmare', ['Nightmare', 'Horse']).
card_colors('nightmare', ['B']).
card_text('nightmare', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nNightmare\'s power and toughness are each equal to the number of Swamps you control.').
card_mana_cost('nightmare', ['5', 'B']).
card_cmc('nightmare', 6).
card_layout('nightmare', 'normal').
card_power('nightmare', '*').
card_toughness('nightmare', '*').

% Found in: EVE
card_name('nightmare incursion', 'Nightmare Incursion').
card_type('nightmare incursion', 'Sorcery').
card_types('nightmare incursion', ['Sorcery']).
card_subtypes('nightmare incursion', []).
card_colors('nightmare incursion', ['B']).
card_text('nightmare incursion', 'Search target player\'s library for up to X cards, where X is the number of Swamps you control, and exile them. Then that player shuffles his or her library.').
card_mana_cost('nightmare incursion', ['5', 'B']).
card_cmc('nightmare incursion', 6).
card_layout('nightmare incursion', 'normal').

% Found in: MRD
card_name('nightmare lash', 'Nightmare Lash').
card_type('nightmare lash', 'Artifact — Equipment').
card_types('nightmare lash', ['Artifact']).
card_subtypes('nightmare lash', ['Equipment']).
card_colors('nightmare lash', []).
card_text('nightmare lash', 'Equipped creature gets +1/+1 for each Swamp you control.\nEquip—Pay 3 life. (Pay 3 life: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('nightmare lash', ['4']).
card_cmc('nightmare lash', 4).
card_layout('nightmare lash', 'normal').

% Found in: DDJ, RAV
card_name('nightmare void', 'Nightmare Void').
card_type('nightmare void', 'Sorcery').
card_types('nightmare void', ['Sorcery']).
card_subtypes('nightmare void', []).
card_colors('nightmare void', ['B']).
card_text('nightmare void', 'Target player reveals his or her hand. You choose a card from it. That player discards that card.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('nightmare void', ['3', 'B']).
card_cmc('nightmare void', 4).
card_layout('nightmare void', 'normal').

% Found in: JOU
card_name('nightmarish end', 'Nightmarish End').
card_type('nightmarish end', 'Instant').
card_types('nightmarish end', ['Instant']).
card_subtypes('nightmarish end', []).
card_colors('nightmarish end', ['B']).
card_text('nightmarish end', 'Target creature gets -X/-X until end of turn, where X is the number of cards in your hand.').
card_mana_cost('nightmarish end', ['2', 'B']).
card_cmc('nightmarish end', 3).
card_layout('nightmarish end', 'normal').

% Found in: INV
card_name('nightscape apprentice', 'Nightscape Apprentice').
card_type('nightscape apprentice', 'Creature — Zombie Wizard').
card_types('nightscape apprentice', ['Creature']).
card_subtypes('nightscape apprentice', ['Zombie', 'Wizard']).
card_colors('nightscape apprentice', ['B']).
card_text('nightscape apprentice', '{U}, {T}: Put target creature you control on top of its owner\'s library.\n{R}, {T}: Target creature gains first strike until end of turn.').
card_mana_cost('nightscape apprentice', ['B']).
card_cmc('nightscape apprentice', 1).
card_layout('nightscape apprentice', 'normal').
card_power('nightscape apprentice', 1).
card_toughness('nightscape apprentice', 1).

% Found in: PLS
card_name('nightscape battlemage', 'Nightscape Battlemage').
card_type('nightscape battlemage', 'Creature — Zombie Wizard').
card_types('nightscape battlemage', ['Creature']).
card_subtypes('nightscape battlemage', ['Zombie', 'Wizard']).
card_colors('nightscape battlemage', ['B']).
card_text('nightscape battlemage', 'Kicker {2}{U} and/or {2}{R} (You may pay an additional {2}{U} and/or {2}{R} as you cast this spell.)\nWhen Nightscape Battlemage enters the battlefield, if it was kicked with its {2}{U} kicker, return up to two target nonblack creatures to their owners\' hands.\nWhen Nightscape Battlemage enters the battlefield, if it was kicked with its {2}{R} kicker, destroy target land.').
card_mana_cost('nightscape battlemage', ['2', 'B']).
card_cmc('nightscape battlemage', 3).
card_layout('nightscape battlemage', 'normal').
card_power('nightscape battlemage', 2).
card_toughness('nightscape battlemage', 2).

% Found in: C13, DDH, PLS, VMA
card_name('nightscape familiar', 'Nightscape Familiar').
card_type('nightscape familiar', 'Creature — Zombie').
card_types('nightscape familiar', ['Creature']).
card_subtypes('nightscape familiar', ['Zombie']).
card_colors('nightscape familiar', ['B']).
card_text('nightscape familiar', 'Blue spells and red spells you cast cost {1} less to cast.\n{1}{B}: Regenerate Nightscape Familiar.').
card_mana_cost('nightscape familiar', ['1', 'B']).
card_cmc('nightscape familiar', 2).
card_layout('nightscape familiar', 'normal').
card_power('nightscape familiar', 1).
card_toughness('nightscape familiar', 1).

% Found in: INV
card_name('nightscape master', 'Nightscape Master').
card_type('nightscape master', 'Creature — Zombie Wizard').
card_types('nightscape master', ['Creature']).
card_subtypes('nightscape master', ['Zombie', 'Wizard']).
card_colors('nightscape master', ['B']).
card_text('nightscape master', '{U}{U}, {T}: Return target creature to its owner\'s hand.\n{R}{R}, {T}: Nightscape Master deals 2 damage to target creature.').
card_mana_cost('nightscape master', ['2', 'B', 'B']).
card_cmc('nightscape master', 4).
card_layout('nightscape master', 'normal').
card_power('nightscape master', 2).
card_toughness('nightscape master', 2).

% Found in: TSP
card_name('nightshade assassin', 'Nightshade Assassin').
card_type('nightshade assassin', 'Creature — Human Assassin').
card_types('nightshade assassin', ['Creature']).
card_subtypes('nightshade assassin', ['Human', 'Assassin']).
card_colors('nightshade assassin', ['B']).
card_text('nightshade assassin', 'First strike\nWhen Nightshade Assassin enters the battlefield, you may reveal X black cards in your hand. If you do, target creature gets -X/-X until end of turn.\nMadness {1}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('nightshade assassin', ['2', 'B', 'B']).
card_cmc('nightshade assassin', 4).
card_layout('nightshade assassin', 'normal').
card_power('nightshade assassin', 2).
card_toughness('nightshade assassin', 1).

% Found in: AVR
card_name('nightshade peddler', 'Nightshade Peddler').
card_type('nightshade peddler', 'Creature — Human Druid').
card_types('nightshade peddler', ['Creature']).
card_subtypes('nightshade peddler', ['Human', 'Druid']).
card_colors('nightshade peddler', ['G']).
card_text('nightshade peddler', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Nightshade Peddler is paired with another creature, both creatures have deathtouch.').
card_mana_cost('nightshade peddler', ['1', 'G']).
card_cmc('nightshade peddler', 2).
card_layout('nightshade peddler', 'normal').
card_power('nightshade peddler', 1).
card_toughness('nightshade peddler', 1).

% Found in: MOR
card_name('nightshade schemers', 'Nightshade Schemers').
card_type('nightshade schemers', 'Creature — Faerie Wizard').
card_types('nightshade schemers', ['Creature']).
card_subtypes('nightshade schemers', ['Faerie', 'Wizard']).
card_colors('nightshade schemers', ['B']).
card_text('nightshade schemers', 'Flying\nKinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Nightshade Schemers, you may reveal it. If you do, each opponent loses 2 life.').
card_mana_cost('nightshade schemers', ['4', 'B']).
card_cmc('nightshade schemers', 5).
card_layout('nightshade schemers', 'normal').
card_power('nightshade schemers', 3).
card_toughness('nightshade schemers', 2).

% Found in: UDS
card_name('nightshade seer', 'Nightshade Seer').
card_type('nightshade seer', 'Creature — Human Wizard').
card_types('nightshade seer', ['Creature']).
card_subtypes('nightshade seer', ['Human', 'Wizard']).
card_colors('nightshade seer', ['B']).
card_text('nightshade seer', '{2}{B}, {T}: Reveal any number of black cards in your hand. Target creature gets -X/-X until end of turn, where X is the number of cards revealed this way.').
card_mana_cost('nightshade seer', ['3', 'B']).
card_cmc('nightshade seer', 4).
card_layout('nightshade seer', 'normal').
card_power('nightshade seer', 1).
card_toughness('nightshade seer', 1).

% Found in: LRW
card_name('nightshade stinger', 'Nightshade Stinger').
card_type('nightshade stinger', 'Creature — Faerie Rogue').
card_types('nightshade stinger', ['Creature']).
card_subtypes('nightshade stinger', ['Faerie', 'Rogue']).
card_colors('nightshade stinger', ['B']).
card_text('nightshade stinger', 'Flying\nNightshade Stinger can\'t block.').
card_mana_cost('nightshade stinger', ['B']).
card_cmc('nightshade stinger', 1).
card_layout('nightshade stinger', 'normal').
card_power('nightshade stinger', 1).
card_toughness('nightshade stinger', 1).

% Found in: EVE
card_name('nightsky mimic', 'Nightsky Mimic').
card_type('nightsky mimic', 'Creature — Shapeshifter').
card_types('nightsky mimic', ['Creature']).
card_subtypes('nightsky mimic', ['Shapeshifter']).
card_colors('nightsky mimic', ['W', 'B']).
card_text('nightsky mimic', 'Whenever you cast a spell that\'s both white and black, Nightsky Mimic has base power and toughness 4/4 until end of turn and gains flying until end of turn.').
card_mana_cost('nightsky mimic', ['1', 'W/B']).
card_cmc('nightsky mimic', 2).
card_layout('nightsky mimic', 'normal').
card_power('nightsky mimic', 2).
card_toughness('nightsky mimic', 1).

% Found in: ORI
card_name('nightsnare', 'Nightsnare').
card_type('nightsnare', 'Sorcery').
card_types('nightsnare', ['Sorcery']).
card_subtypes('nightsnare', []).
card_colors('nightsnare', ['B']).
card_text('nightsnare', 'Target opponent reveals his or her hand. You may choose a nonland card from it. If you do, that player discards that card. If you don\'t, that player discards two cards.').
card_mana_cost('nightsnare', ['3', 'B']).
card_cmc('nightsnare', 4).
card_layout('nightsnare', 'normal').

% Found in: SOK
card_name('nightsoil kami', 'Nightsoil Kami').
card_type('nightsoil kami', 'Creature — Spirit').
card_types('nightsoil kami', ['Creature']).
card_subtypes('nightsoil kami', ['Spirit']).
card_colors('nightsoil kami', ['G']).
card_text('nightsoil kami', 'Soulshift 5 (When this creature dies, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_mana_cost('nightsoil kami', ['4', 'G', 'G']).
card_cmc('nightsoil kami', 6).
card_layout('nightsoil kami', 'normal').
card_power('nightsoil kami', 6).
card_toughness('nightsoil kami', 4).

% Found in: PO2
card_name('nightstalker engine', 'Nightstalker Engine').
card_type('nightstalker engine', 'Creature — Nightstalker').
card_types('nightstalker engine', ['Creature']).
card_subtypes('nightstalker engine', ['Nightstalker']).
card_colors('nightstalker engine', ['B']).
card_text('nightstalker engine', 'Nightstalker Engine\'s power is equal to the number of creature cards in your graveyard.').
card_mana_cost('nightstalker engine', ['4', 'B']).
card_cmc('nightstalker engine', 5).
card_layout('nightstalker engine', 'normal').
card_power('nightstalker engine', '*').
card_toughness('nightstalker engine', 3).

% Found in: GTC, pMEI
card_name('nightveil specter', 'Nightveil Specter').
card_type('nightveil specter', 'Creature — Specter').
card_types('nightveil specter', ['Creature']).
card_subtypes('nightveil specter', ['Specter']).
card_colors('nightveil specter', ['U', 'B']).
card_text('nightveil specter', 'Flying\nWhenever Nightveil Specter deals combat damage to a player, that player exiles the top card of his or her library.\nYou may play cards exiled with Nightveil Specter.').
card_mana_cost('nightveil specter', ['U/B', 'U/B', 'U/B']).
card_cmc('nightveil specter', 3).
card_layout('nightveil specter', 'normal').
card_power('nightveil specter', 2).
card_toughness('nightveil specter', 3).

% Found in: MMQ
card_name('nightwind glider', 'Nightwind Glider').
card_type('nightwind glider', 'Creature — Human Rebel').
card_types('nightwind glider', ['Creature']).
card_subtypes('nightwind glider', ['Human', 'Rebel']).
card_colors('nightwind glider', ['W']).
card_text('nightwind glider', 'Flying, protection from black').
card_mana_cost('nightwind glider', ['2', 'W']).
card_cmc('nightwind glider', 3).
card_layout('nightwind glider', 'normal').
card_power('nightwind glider', 2).
card_toughness('nightwind glider', 1).

% Found in: M11, M14
card_name('nightwing shade', 'Nightwing Shade').
card_type('nightwing shade', 'Creature — Shade').
card_types('nightwing shade', ['Creature']).
card_subtypes('nightwing shade', ['Shade']).
card_colors('nightwing shade', ['B']).
card_text('nightwing shade', 'Flying\n{1}{B}: Nightwing Shade gets +1/+1 until end of turn.').
card_mana_cost('nightwing shade', ['4', 'B']).
card_cmc('nightwing shade', 5).
card_layout('nightwing shade', 'normal').
card_power('nightwing shade', 2).
card_toughness('nightwing shade', 2).

% Found in: C13, SOM
card_name('nihil spellbomb', 'Nihil Spellbomb').
card_type('nihil spellbomb', 'Artifact').
card_types('nihil spellbomb', ['Artifact']).
card_subtypes('nihil spellbomb', []).
card_colors('nihil spellbomb', []).
card_text('nihil spellbomb', '{T}, Sacrifice Nihil Spellbomb: Exile all cards from target player\'s graveyard.\nWhen Nihil Spellbomb is put into a graveyard from the battlefield, you may pay {B}. If you do, draw a card.').
card_mana_cost('nihil spellbomb', ['1']).
card_cmc('nihil spellbomb', 1).
card_layout('nihil spellbomb', 'normal').

% Found in: DIS
card_name('nihilistic glee', 'Nihilistic Glee').
card_type('nihilistic glee', 'Enchantment').
card_types('nihilistic glee', ['Enchantment']).
card_subtypes('nihilistic glee', []).
card_colors('nihilistic glee', ['B']).
card_text('nihilistic glee', '{2}{B}, Discard a card: Target opponent loses 1 life and you gain 1 life.\nHellbent — {1}, Pay 2 life: Draw a card. Activate this ability only if you have no cards in hand.').
card_mana_cost('nihilistic glee', ['2', 'B', 'B']).
card_cmc('nihilistic glee', 4).
card_layout('nihilistic glee', 'normal').

% Found in: FUT
card_name('nihilith', 'Nihilith').
card_type('nihilith', 'Creature — Horror').
card_types('nihilith', ['Creature']).
card_subtypes('nihilith', ['Horror']).
card_colors('nihilith', ['B']).
card_text('nihilith', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nSuspend 7—{1}{B} (Rather than cast this card from your hand, you may pay {1}{B} and exile it with seven time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever a card is put into an opponent\'s graveyard from anywhere, if Nihilith is suspended, you may remove a time counter from Nihilith.').
card_mana_cost('nihilith', ['4', 'B', 'B']).
card_cmc('nihilith', 6).
card_layout('nihilith', 'normal').
card_power('nihilith', 4).
card_toughness('nihilith', 4).

% Found in: SOK
card_name('nikko-onna', 'Nikko-Onna').
card_type('nikko-onna', 'Creature — Spirit').
card_types('nikko-onna', ['Creature']).
card_subtypes('nikko-onna', ['Spirit']).
card_colors('nikko-onna', ['W']).
card_text('nikko-onna', 'When Nikko-Onna enters the battlefield, destroy target enchantment.\nWhenever you cast a Spirit or Arcane spell, you may return Nikko-Onna to its owner\'s hand.').
card_mana_cost('nikko-onna', ['2', 'W']).
card_cmc('nikko-onna', 3).
card_layout('nikko-onna', 'normal').
card_power('nikko-onna', 2).
card_toughness('nikko-onna', 2).

% Found in: DST
card_name('nim abomination', 'Nim Abomination').
card_type('nim abomination', 'Creature — Zombie').
card_types('nim abomination', ['Creature']).
card_subtypes('nim abomination', ['Zombie']).
card_colors('nim abomination', ['B']).
card_text('nim abomination', 'At the beginning of your end step, if Nim Abomination is untapped, you lose 3 life.').
card_mana_cost('nim abomination', ['2', 'B']).
card_cmc('nim abomination', 3).
card_layout('nim abomination', 'normal').
card_power('nim abomination', 3).
card_toughness('nim abomination', 4).

% Found in: SOM
card_name('nim deathmantle', 'Nim Deathmantle').
card_type('nim deathmantle', 'Artifact — Equipment').
card_types('nim deathmantle', ['Artifact']).
card_subtypes('nim deathmantle', ['Equipment']).
card_colors('nim deathmantle', []).
card_text('nim deathmantle', 'Equipped creature gets +2/+2, has intimidate, and is a black Zombie. (A creature with intimidate can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever a nontoken creature is put into your graveyard from the battlefield, you may pay {4}. If you do, return that card to the battlefield and attach Nim Deathmantle to it.\nEquip {4}').
card_mana_cost('nim deathmantle', ['2']).
card_cmc('nim deathmantle', 2).
card_layout('nim deathmantle', 'normal').

% Found in: MRD
card_name('nim devourer', 'Nim Devourer').
card_type('nim devourer', 'Creature — Zombie').
card_types('nim devourer', ['Creature']).
card_subtypes('nim devourer', ['Zombie']).
card_colors('nim devourer', ['B']).
card_text('nim devourer', 'Nim Devourer gets +1/+0 for each artifact you control.\n{B}{B}: Return Nim Devourer from your graveyard to the battlefield, then sacrifice a creature. Activate this ability only during your upkeep.').
card_mana_cost('nim devourer', ['3', 'B', 'B']).
card_cmc('nim devourer', 5).
card_layout('nim devourer', 'normal').
card_power('nim devourer', 4).
card_toughness('nim devourer', 1).

% Found in: 5DN
card_name('nim grotesque', 'Nim Grotesque').
card_type('nim grotesque', 'Creature — Zombie').
card_types('nim grotesque', ['Creature']).
card_subtypes('nim grotesque', ['Zombie']).
card_colors('nim grotesque', ['B']).
card_text('nim grotesque', 'Nim Grotesque gets +1/+0 for each artifact you control.').
card_mana_cost('nim grotesque', ['6', 'B']).
card_cmc('nim grotesque', 7).
card_layout('nim grotesque', 'normal').
card_power('nim grotesque', 3).
card_toughness('nim grotesque', 6).

% Found in: MRD
card_name('nim lasher', 'Nim Lasher').
card_type('nim lasher', 'Creature — Zombie').
card_types('nim lasher', ['Creature']).
card_subtypes('nim lasher', ['Zombie']).
card_colors('nim lasher', ['B']).
card_text('nim lasher', 'Nim Lasher gets +1/+0 for each artifact you control.').
card_mana_cost('nim lasher', ['2', 'B']).
card_cmc('nim lasher', 3).
card_layout('nim lasher', 'normal').
card_power('nim lasher', 1).
card_toughness('nim lasher', 1).

% Found in: MRD
card_name('nim replica', 'Nim Replica').
card_type('nim replica', 'Artifact Creature — Zombie').
card_types('nim replica', ['Artifact', 'Creature']).
card_subtypes('nim replica', ['Zombie']).
card_colors('nim replica', []).
card_text('nim replica', '{2}{B}, Sacrifice Nim Replica: Target creature gets -1/-1 until end of turn.').
card_mana_cost('nim replica', ['3']).
card_cmc('nim replica', 3).
card_layout('nim replica', 'normal').
card_power('nim replica', 3).
card_toughness('nim replica', 1).

% Found in: MRD
card_name('nim shambler', 'Nim Shambler').
card_type('nim shambler', 'Creature — Zombie').
card_types('nim shambler', ['Creature']).
card_subtypes('nim shambler', ['Zombie']).
card_colors('nim shambler', ['B']).
card_text('nim shambler', 'Nim Shambler gets +1/+0 for each artifact you control.\nSacrifice a creature: Regenerate Nim Shambler.').
card_mana_cost('nim shambler', ['2', 'B', 'B']).
card_cmc('nim shambler', 4).
card_layout('nim shambler', 'normal').
card_power('nim shambler', 2).
card_toughness('nim shambler', 1).

% Found in: MRD
card_name('nim shrieker', 'Nim Shrieker').
card_type('nim shrieker', 'Creature — Zombie').
card_types('nim shrieker', ['Creature']).
card_subtypes('nim shrieker', ['Zombie']).
card_colors('nim shrieker', ['B']).
card_text('nim shrieker', 'Flying\nNim Shrieker gets +1/+0 for each artifact you control.').
card_mana_cost('nim shrieker', ['3', 'B']).
card_cmc('nim shrieker', 4).
card_layout('nim shrieker', 'normal').
card_power('nim shrieker', 0).
card_toughness('nim shrieker', 1).

% Found in: ZEN
card_name('nimana sell-sword', 'Nimana Sell-Sword').
card_type('nimana sell-sword', 'Creature — Human Warrior Ally').
card_types('nimana sell-sword', ['Creature']).
card_subtypes('nimana sell-sword', ['Human', 'Warrior', 'Ally']).
card_colors('nimana sell-sword', ['B']).
card_text('nimana sell-sword', 'Whenever Nimana Sell-Sword or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Nimana Sell-Sword.').
card_mana_cost('nimana sell-sword', ['3', 'B']).
card_cmc('nimana sell-sword', 4).
card_layout('nimana sell-sword', 'normal').
card_power('nimana sell-sword', 2).
card_toughness('nimana sell-sword', 2).

% Found in: ODY
card_name('nimble mongoose', 'Nimble Mongoose').
card_type('nimble mongoose', 'Creature — Mongoose').
card_types('nimble mongoose', ['Creature']).
card_subtypes('nimble mongoose', ['Mongoose']).
card_colors('nimble mongoose', ['G']).
card_text('nimble mongoose', 'Shroud (This creature can\'t be the target of spells or abilities.)\nThreshold — Nimble Mongoose gets +2/+2 as long as seven or more cards are in your graveyard.').
card_mana_cost('nimble mongoose', ['G']).
card_cmc('nimble mongoose', 1).
card_layout('nimble mongoose', 'normal').
card_power('nimble mongoose', 1).
card_toughness('nimble mongoose', 1).

% Found in: FUT
card_name('nimbus maze', 'Nimbus Maze').
card_type('nimbus maze', 'Land').
card_types('nimbus maze', ['Land']).
card_subtypes('nimbus maze', []).
card_colors('nimbus maze', []).
card_text('nimbus maze', '{T}: Add {1} to your mana pool.\n{T}: Add {W} to your mana pool. Activate this ability only if you control an Island.\n{T}: Add {U} to your mana pool. Activate this ability only if you control a Plains.').
card_layout('nimbus maze', 'normal').

% Found in: THS
card_name('nimbus naiad', 'Nimbus Naiad').
card_type('nimbus naiad', 'Enchantment Creature — Nymph').
card_types('nimbus naiad', ['Enchantment', 'Creature']).
card_subtypes('nimbus naiad', ['Nymph']).
card_colors('nimbus naiad', ['U']).
card_text('nimbus naiad', 'Bestow {4}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying\nEnchanted creature gets +2/+2 and has flying.').
card_mana_cost('nimbus naiad', ['2', 'U']).
card_cmc('nimbus naiad', 3).
card_layout('nimbus naiad', 'normal').
card_power('nimbus naiad', 2).
card_toughness('nimbus naiad', 2).

% Found in: M15
card_name('nimbus of the isles', 'Nimbus of the Isles').
card_type('nimbus of the isles', 'Creature — Elemental').
card_types('nimbus of the isles', ['Creature']).
card_subtypes('nimbus of the isles', ['Elemental']).
card_colors('nimbus of the isles', ['U']).
card_text('nimbus of the isles', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('nimbus of the isles', ['4', 'U']).
card_cmc('nimbus of the isles', 5).
card_layout('nimbus of the isles', 'normal').
card_power('nimbus of the isles', 3).
card_toughness('nimbus of the isles', 3).

% Found in: DDO, GTC
card_name('nimbus swimmer', 'Nimbus Swimmer').
card_type('nimbus swimmer', 'Creature — Leviathan').
card_types('nimbus swimmer', ['Creature']).
card_subtypes('nimbus swimmer', ['Leviathan']).
card_colors('nimbus swimmer', ['U', 'G']).
card_text('nimbus swimmer', 'Flying\nNimbus Swimmer enters the battlefield with X +1/+1 counters on it.').
card_mana_cost('nimbus swimmer', ['X', 'G', 'U']).
card_cmc('nimbus swimmer', 2).
card_layout('nimbus swimmer', 'normal').
card_power('nimbus swimmer', 0).
card_toughness('nimbus swimmer', 0).

% Found in: ZEN
card_name('nimbus wings', 'Nimbus Wings').
card_type('nimbus wings', 'Enchantment — Aura').
card_types('nimbus wings', ['Enchantment']).
card_subtypes('nimbus wings', ['Aura']).
card_colors('nimbus wings', ['W']).
card_text('nimbus wings', 'Enchant creature\nEnchanted creature gets +1/+2 and has flying.').
card_mana_cost('nimbus wings', ['1', 'W']).
card_cmc('nimbus wings', 2).
card_layout('nimbus wings', 'normal').

% Found in: CMD
card_name('nin, the pain artist', 'Nin, the Pain Artist').
card_type('nin, the pain artist', 'Legendary Creature — Vedalken Wizard').
card_types('nin, the pain artist', ['Creature']).
card_subtypes('nin, the pain artist', ['Vedalken', 'Wizard']).
card_supertypes('nin, the pain artist', ['Legendary']).
card_colors('nin, the pain artist', ['U', 'R']).
card_text('nin, the pain artist', '{X}{U}{R}, {T}: Nin, the Pain Artist deals X damage to target creature. That creature\'s controller draws X cards.').
card_mana_cost('nin, the pain artist', ['U', 'R']).
card_cmc('nin, the pain artist', 2).
card_layout('nin, the pain artist', 'normal').
card_power('nin, the pain artist', 1).
card_toughness('nin, the pain artist', 1).

% Found in: CHK
card_name('nine-ringed bo', 'Nine-Ringed Bo').
card_type('nine-ringed bo', 'Artifact').
card_types('nine-ringed bo', ['Artifact']).
card_subtypes('nine-ringed bo', []).
card_colors('nine-ringed bo', []).
card_text('nine-ringed bo', '{T}: Nine-Ringed Bo deals 1 damage to target Spirit creature. If that creature would die this turn, exile it instead.').
card_mana_cost('nine-ringed bo', ['3']).
card_cmc('nine-ringed bo', 3).
card_layout('nine-ringed bo', 'normal').

% Found in: BOK, PC2
card_name('ninja of the deep hours', 'Ninja of the Deep Hours').
card_type('ninja of the deep hours', 'Creature — Human Ninja').
card_types('ninja of the deep hours', ['Creature']).
card_subtypes('ninja of the deep hours', ['Human', 'Ninja']).
card_colors('ninja of the deep hours', ['U']).
card_text('ninja of the deep hours', 'Ninjutsu {1}{U} ({1}{U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Ninja of the Deep Hours deals combat damage to a player, you may draw a card.').
card_mana_cost('ninja of the deep hours', ['3', 'U']).
card_cmc('ninja of the deep hours', 4).
card_layout('ninja of the deep hours', 'normal').
card_power('ninja of the deep hours', 2).
card_toughness('ninja of the deep hours', 2).

% Found in: EVE
card_name('nip gwyllion', 'Nip Gwyllion').
card_type('nip gwyllion', 'Creature — Hag').
card_types('nip gwyllion', ['Creature']).
card_subtypes('nip gwyllion', ['Hag']).
card_colors('nip gwyllion', ['W', 'B']).
card_text('nip gwyllion', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('nip gwyllion', ['W/B']).
card_cmc('nip gwyllion', 1).
card_layout('nip gwyllion', 'normal').
card_power('nip gwyllion', 1).
card_toughness('nip gwyllion', 1).

% Found in: BFZ
card_name('nirkana assassin', 'Nirkana Assassin').
card_type('nirkana assassin', 'Creature — Vampire Assassin Ally').
card_types('nirkana assassin', ['Creature']).
card_subtypes('nirkana assassin', ['Vampire', 'Assassin', 'Ally']).
card_colors('nirkana assassin', ['B']).
card_text('nirkana assassin', 'Whenever you gain life, Nirkana Assassin gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('nirkana assassin', ['2', 'B']).
card_cmc('nirkana assassin', 3).
card_layout('nirkana assassin', 'normal').
card_power('nirkana assassin', 2).
card_toughness('nirkana assassin', 3).

% Found in: ROE
card_name('nirkana cutthroat', 'Nirkana Cutthroat').
card_type('nirkana cutthroat', 'Creature — Vampire Warrior').
card_types('nirkana cutthroat', ['Creature']).
card_subtypes('nirkana cutthroat', ['Vampire', 'Warrior']).
card_colors('nirkana cutthroat', ['B']).
card_text('nirkana cutthroat', 'Level up {2}{B} ({2}{B}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-2\n4/3\nDeathtouch\nLEVEL 3+\n5/4\nFirst strike, deathtouch').
card_mana_cost('nirkana cutthroat', ['2', 'B']).
card_cmc('nirkana cutthroat', 3).
card_layout('nirkana cutthroat', 'leveler').
card_power('nirkana cutthroat', 3).
card_toughness('nirkana cutthroat', 2).

% Found in: ROE
card_name('nirkana revenant', 'Nirkana Revenant').
card_type('nirkana revenant', 'Creature — Vampire Shade').
card_types('nirkana revenant', ['Creature']).
card_subtypes('nirkana revenant', ['Vampire', 'Shade']).
card_colors('nirkana revenant', ['B']).
card_text('nirkana revenant', 'Whenever you tap a Swamp for mana, add {B} to your mana pool (in addition to the mana the land produces).\n{B}: Nirkana Revenant gets +1/+1 until end of turn.').
card_mana_cost('nirkana revenant', ['4', 'B', 'B']).
card_cmc('nirkana revenant', 6).
card_layout('nirkana revenant', 'normal').
card_power('nirkana revenant', 4).
card_toughness('nirkana revenant', 4).

% Found in: ZEN, pMEI
card_name('nissa revane', 'Nissa Revane').
card_type('nissa revane', 'Planeswalker — Nissa').
card_types('nissa revane', ['Planeswalker']).
card_subtypes('nissa revane', ['Nissa']).
card_colors('nissa revane', ['G']).
card_text('nissa revane', '+1: Search your library for a card named Nissa\'s Chosen and put it onto the battlefield. Then shuffle your library.\n+1: You gain 2 life for each Elf you control.\n−7: Search your library for any number of Elf creature cards and put them onto the battlefield. Then shuffle your library.').
card_mana_cost('nissa revane', ['2', 'G', 'G']).
card_cmc('nissa revane', 4).
card_layout('nissa revane', 'normal').
card_loyalty('nissa revane', 2).

% Found in: ZEN, pWPN
card_name('nissa\'s chosen', 'Nissa\'s Chosen').
card_type('nissa\'s chosen', 'Creature — Elf Warrior').
card_types('nissa\'s chosen', ['Creature']).
card_subtypes('nissa\'s chosen', ['Elf', 'Warrior']).
card_colors('nissa\'s chosen', ['G']).
card_text('nissa\'s chosen', 'If Nissa\'s Chosen would die, put it on the bottom of its owner\'s library instead.').
card_mana_cost('nissa\'s chosen', ['G', 'G']).
card_cmc('nissa\'s chosen', 2).
card_layout('nissa\'s chosen', 'normal').
card_power('nissa\'s chosen', 2).
card_toughness('nissa\'s chosen', 3).

% Found in: M15
card_name('nissa\'s expedition', 'Nissa\'s Expedition').
card_type('nissa\'s expedition', 'Sorcery').
card_types('nissa\'s expedition', ['Sorcery']).
card_subtypes('nissa\'s expedition', []).
card_colors('nissa\'s expedition', ['G']).
card_text('nissa\'s expedition', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nSearch your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_mana_cost('nissa\'s expedition', ['4', 'G']).
card_cmc('nissa\'s expedition', 5).
card_layout('nissa\'s expedition', 'normal').

% Found in: ORI
card_name('nissa\'s pilgrimage', 'Nissa\'s Pilgrimage').
card_type('nissa\'s pilgrimage', 'Sorcery').
card_types('nissa\'s pilgrimage', ['Sorcery']).
card_subtypes('nissa\'s pilgrimage', []).
card_colors('nissa\'s pilgrimage', ['G']).
card_text('nissa\'s pilgrimage', 'Search your library for up to two basic Forest cards, reveal those cards, and put one onto the battlefield tapped and the rest into your hand. Then shuffle your library.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, search your library for up to three basic Forest cards instead of two.').
card_mana_cost('nissa\'s pilgrimage', ['2', 'G']).
card_cmc('nissa\'s pilgrimage', 3).
card_layout('nissa\'s pilgrimage', 'normal').

% Found in: BFZ
card_name('nissa\'s renewal', 'Nissa\'s Renewal').
card_type('nissa\'s renewal', 'Sorcery').
card_types('nissa\'s renewal', ['Sorcery']).
card_subtypes('nissa\'s renewal', []).
card_colors('nissa\'s renewal', ['G']).
card_text('nissa\'s renewal', 'Search your library for up to three basic land cards, put them onto the battlefield tapped, then shuffle your library. You gain 7 life.').
card_mana_cost('nissa\'s renewal', ['5', 'G']).
card_cmc('nissa\'s renewal', 6).
card_layout('nissa\'s renewal', 'normal').

% Found in: ORI
card_name('nissa\'s revelation', 'Nissa\'s Revelation').
card_type('nissa\'s revelation', 'Sorcery').
card_types('nissa\'s revelation', ['Sorcery']).
card_subtypes('nissa\'s revelation', []).
card_colors('nissa\'s revelation', ['G']).
card_text('nissa\'s revelation', 'Scry 5, then reveal the top card of your library. If it\'s a creature card, you draw cards equal to its power and you gain life equal to its toughness.').
card_mana_cost('nissa\'s revelation', ['5', 'G', 'G']).
card_cmc('nissa\'s revelation', 7).
card_layout('nissa\'s revelation', 'normal').

% Found in: ORI
card_name('nissa, sage animist', 'Nissa, Sage Animist').
card_type('nissa, sage animist', 'Planeswalker — Nissa').
card_types('nissa, sage animist', ['Planeswalker']).
card_subtypes('nissa, sage animist', ['Nissa']).
card_colors('nissa, sage animist', ['G']).
card_text('nissa, sage animist', '+1: Reveal the top card of your library. If it\'s a land card, put it onto the battlefield. Otherwise, put it into your hand.\n−2: Put a legendary 4/4 green Elemental creature token named Ashaya, the Awoken World onto the battlefield.\n−7: Untap up to six target lands. They become 6/6 Elemental creatures. They\'re still lands.').
card_layout('nissa, sage animist', 'double-faced').
card_loyalty('nissa, sage animist', 3).

% Found in: ORI
card_name('nissa, vastwood seer', 'Nissa, Vastwood Seer').
card_type('nissa, vastwood seer', 'Legendary Creature — Elf Scout').
card_types('nissa, vastwood seer', ['Creature']).
card_subtypes('nissa, vastwood seer', ['Elf', 'Scout']).
card_supertypes('nissa, vastwood seer', ['Legendary']).
card_colors('nissa, vastwood seer', ['G']).
card_text('nissa, vastwood seer', 'When Nissa, Vastwood Seer enters the battlefield, you may search your library for a basic Forest card, reveal it, put it into your hand, then shuffle your library.\nWhenever a land enters the battlefield under your control, if you control seven or more lands, exile Nissa, then return her to the battlefield transformed under her owner\'s control.').
card_mana_cost('nissa, vastwood seer', ['2', 'G']).
card_cmc('nissa, vastwood seer', 3).
card_layout('nissa, vastwood seer', 'double-faced').
card_power('nissa, vastwood seer', 2).
card_toughness('nissa, vastwood seer', 2).
card_sides('nissa, vastwood seer', 'nissa, sage animist').

% Found in: M15, pMEI
card_name('nissa, worldwaker', 'Nissa, Worldwaker').
card_type('nissa, worldwaker', 'Planeswalker — Nissa').
card_types('nissa, worldwaker', ['Planeswalker']).
card_subtypes('nissa, worldwaker', ['Nissa']).
card_colors('nissa, worldwaker', ['G']).
card_text('nissa, worldwaker', '+1: Target land you control becomes a 4/4 Elemental creature with trample. It\'s still a land.\n+1: Untap up to four target Forests.\n−7: Search your library for any number of basic land cards, put them onto the battlefield, then shuffle your library. Those lands become 4/4 Elemental creatures with trample. They\'re still lands.').
card_mana_cost('nissa, worldwaker', ['3', 'G', 'G']).
card_cmc('nissa, worldwaker', 5).
card_layout('nissa, worldwaker', 'normal').
card_loyalty('nissa, worldwaker', 3).

% Found in: RTR
card_name('niv-mizzet, dracogenius', 'Niv-Mizzet, Dracogenius').
card_type('niv-mizzet, dracogenius', 'Legendary Creature — Dragon Wizard').
card_types('niv-mizzet, dracogenius', ['Creature']).
card_subtypes('niv-mizzet, dracogenius', ['Dragon', 'Wizard']).
card_supertypes('niv-mizzet, dracogenius', ['Legendary']).
card_colors('niv-mizzet, dracogenius', ['U', 'R']).
card_text('niv-mizzet, dracogenius', 'Flying\nWhenever Niv-Mizzet, Dracogenius deals damage to a player, you may draw a card.\n{U}{R}: Niv-Mizzet, Dracogenius deals 1 damage to target creature or player.').
card_mana_cost('niv-mizzet, dracogenius', ['2', 'U', 'U', 'R', 'R']).
card_cmc('niv-mizzet, dracogenius', 6).
card_layout('niv-mizzet, dracogenius', 'normal').
card_power('niv-mizzet, dracogenius', 5).
card_toughness('niv-mizzet, dracogenius', 5).

% Found in: DDJ, DRB, GPT, MM2, pCMP
card_name('niv-mizzet, the firemind', 'Niv-Mizzet, the Firemind').
card_type('niv-mizzet, the firemind', 'Legendary Creature — Dragon Wizard').
card_types('niv-mizzet, the firemind', ['Creature']).
card_subtypes('niv-mizzet, the firemind', ['Dragon', 'Wizard']).
card_supertypes('niv-mizzet, the firemind', ['Legendary']).
card_colors('niv-mizzet, the firemind', ['U', 'R']).
card_text('niv-mizzet, the firemind', 'Flying\nWhenever you draw a card, Niv-Mizzet, the Firemind deals 1 damage to target creature or player.\n{T}: Draw a card.').
card_mana_cost('niv-mizzet, the firemind', ['2', 'U', 'U', 'R', 'R']).
card_cmc('niv-mizzet, the firemind', 6).
card_layout('niv-mizzet, the firemind', 'normal').
card_power('niv-mizzet, the firemind', 4).
card_toughness('niv-mizzet, the firemind', 4).

% Found in: SHM
card_name('niveous wisps', 'Niveous Wisps').
card_type('niveous wisps', 'Instant').
card_types('niveous wisps', ['Instant']).
card_subtypes('niveous wisps', []).
card_colors('niveous wisps', ['W']).
card_text('niveous wisps', 'Target creature becomes white until end of turn. Tap that creature.\nDraw a card.').
card_mana_cost('niveous wisps', ['W']).
card_cmc('niveous wisps', 1).
card_layout('niveous wisps', 'normal').

% Found in: ORI
card_name('nivix barrier', 'Nivix Barrier').
card_type('nivix barrier', 'Creature — Illusion Wall').
card_types('nivix barrier', ['Creature']).
card_subtypes('nivix barrier', ['Illusion', 'Wall']).
card_colors('nivix barrier', ['U']).
card_text('nivix barrier', 'Flash (You may cast this spell any time you could cast an instant.)\nDefender (This creature can\'t attack.)\nWhen Nivix Barrier enters the battlefield, target attacking creature gets -4/-0 until end of turn.').
card_mana_cost('nivix barrier', ['3', 'U']).
card_cmc('nivix barrier', 4).
card_layout('nivix barrier', 'normal').
card_power('nivix barrier', 0).
card_toughness('nivix barrier', 4).

% Found in: DGM
card_name('nivix cyclops', 'Nivix Cyclops').
card_type('nivix cyclops', 'Creature — Cyclops').
card_types('nivix cyclops', ['Creature']).
card_subtypes('nivix cyclops', ['Cyclops']).
card_colors('nivix cyclops', ['U', 'R']).
card_text('nivix cyclops', 'Defender\nWhenever you cast an instant or sorcery spell, Nivix Cyclops gets +3/+0 until end of turn and can attack this turn as though it didn\'t have defender.').
card_mana_cost('nivix cyclops', ['1', 'U', 'R']).
card_cmc('nivix cyclops', 3).
card_layout('nivix cyclops', 'normal').
card_power('nivix cyclops', 1).
card_toughness('nivix cyclops', 4).

% Found in: C13, RTR
card_name('nivix guildmage', 'Nivix Guildmage').
card_type('nivix guildmage', 'Creature — Human Wizard').
card_types('nivix guildmage', ['Creature']).
card_subtypes('nivix guildmage', ['Human', 'Wizard']).
card_colors('nivix guildmage', ['U', 'R']).
card_text('nivix guildmage', '{1}{U}{R}: Draw a card, then discard a card.\n{2}{U}{R}: Copy target instant or sorcery spell you control. You may choose new targets for the copy.').
card_mana_cost('nivix guildmage', ['U', 'R']).
card_cmc('nivix guildmage', 2).
card_layout('nivix guildmage', 'normal').
card_power('nivix guildmage', 2).
card_toughness('nivix guildmage', 2).

% Found in: DDJ, GPT
card_name('nivix, aerie of the firemind', 'Nivix, Aerie of the Firemind').
card_type('nivix, aerie of the firemind', 'Land').
card_types('nivix, aerie of the firemind', ['Land']).
card_subtypes('nivix, aerie of the firemind', []).
card_colors('nivix, aerie of the firemind', []).
card_text('nivix, aerie of the firemind', '{T}: Add {1} to your mana pool.\n{2}{U}{R}, {T}: Exile the top card of your library. Until your next turn, you may cast that card if it\'s an instant or sorcery.').
card_layout('nivix, aerie of the firemind', 'normal').

% Found in: RTR
card_name('nivmagus elemental', 'Nivmagus Elemental').
card_type('nivmagus elemental', 'Creature — Elemental').
card_types('nivmagus elemental', ['Creature']).
card_subtypes('nivmagus elemental', ['Elemental']).
card_colors('nivmagus elemental', ['U', 'R']).
card_text('nivmagus elemental', 'Exile an instant or sorcery spell you control: Put two +1/+1 counters on Nivmagus Elemental. (That spell won\'t resolve.)').
card_mana_cost('nivmagus elemental', ['U/R']).
card_cmc('nivmagus elemental', 1).
card_layout('nivmagus elemental', 'normal').
card_power('nivmagus elemental', 1).
card_toughness('nivmagus elemental', 2).

% Found in: FUT
card_name('nix', 'Nix').
card_type('nix', 'Instant').
card_types('nix', ['Instant']).
card_subtypes('nix', []).
card_colors('nix', ['U']).
card_text('nix', 'Counter target spell if no mana was spent to cast it.').
card_mana_cost('nix', ['U']).
card_cmc('nix', 1).
card_layout('nix', 'normal').

% Found in: ULG
card_name('no mercy', 'No Mercy').
card_type('no mercy', 'Enchantment').
card_types('no mercy', ['Enchantment']).
card_subtypes('no mercy', []).
card_colors('no mercy', ['B']).
card_text('no mercy', 'Whenever a creature deals damage to you, destroy it.').
card_mana_cost('no mercy', ['2', 'B', 'B']).
card_cmc('no mercy', 4).
card_layout('no mercy', 'normal').

% Found in: TMP
card_name('no quarter', 'No Quarter').
card_type('no quarter', 'Enchantment').
card_types('no quarter', ['Enchantment']).
card_subtypes('no quarter', []).
card_colors('no quarter', ['R']).
card_text('no quarter', 'Whenever a creature becomes blocked by a creature with lesser power, destroy the blocking creature.\nWhenever a creature blocks a creature with lesser power, destroy the attacking creature.').
card_mana_cost('no quarter', ['3', 'R']).
card_cmc('no quarter', 4).
card_layout('no quarter', 'normal').

% Found in: 10E, USG
card_name('no rest for the wicked', 'No Rest for the Wicked').
card_type('no rest for the wicked', 'Enchantment').
card_types('no rest for the wicked', ['Enchantment']).
card_subtypes('no rest for the wicked', []).
card_colors('no rest for the wicked', ['B']).
card_text('no rest for the wicked', 'Sacrifice No Rest for the Wicked: Return to your hand all creature cards in your graveyard that were put there from the battlefield this turn.').
card_mana_cost('no rest for the wicked', ['1', 'B']).
card_cmc('no rest for the wicked', 2).
card_layout('no rest for the wicked', 'normal').

% Found in: CHK
card_name('no-dachi', 'No-Dachi').
card_type('no-dachi', 'Artifact — Equipment').
card_types('no-dachi', ['Artifact']).
card_subtypes('no-dachi', ['Equipment']).
card_colors('no-dachi', []).
card_text('no-dachi', 'Equipped creature gets +2/+0 and has first strike.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('no-dachi', ['2']).
card_cmc('no-dachi', 2).
card_layout('no-dachi', 'normal').

% Found in: DDL, EVE, MM2
card_name('nobilis of war', 'Nobilis of War').
card_type('nobilis of war', 'Creature — Spirit Avatar').
card_types('nobilis of war', ['Creature']).
card_subtypes('nobilis of war', ['Spirit', 'Avatar']).
card_colors('nobilis of war', ['W', 'R']).
card_text('nobilis of war', 'Flying\nAttacking creatures you control get +2/+0.').
card_mana_cost('nobilis of war', ['R/W', 'R/W', 'R/W', 'R/W', 'R/W']).
card_cmc('nobilis of war', 5).
card_layout('nobilis of war', 'normal').
card_power('nobilis of war', 3).
card_toughness('nobilis of war', 4).

% Found in: WTH
card_name('noble benefactor', 'Noble Benefactor').
card_type('noble benefactor', 'Creature — Human Cleric').
card_types('noble benefactor', ['Creature']).
card_subtypes('noble benefactor', ['Human', 'Cleric']).
card_colors('noble benefactor', ['U']).
card_text('noble benefactor', 'When Noble Benefactor dies, each player may search his or her library for a card and put that card into his or her hand. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('noble benefactor', ['2', 'U']).
card_cmc('noble benefactor', 3).
card_layout('noble benefactor', 'normal').
card_power('noble benefactor', 2).
card_toughness('noble benefactor', 2).

% Found in: MIR
card_name('noble elephant', 'Noble Elephant').
card_type('noble elephant', 'Creature — Elephant').
card_types('noble elephant', ['Creature']).
card_subtypes('noble elephant', ['Elephant']).
card_colors('noble elephant', ['W']).
card_text('noble elephant', 'Trample; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('noble elephant', ['3', 'W']).
card_cmc('noble elephant', 4).
card_layout('noble elephant', 'normal').
card_power('noble elephant', 2).
card_toughness('noble elephant', 2).

% Found in: CON, MM2, pJGP
card_name('noble hierarch', 'Noble Hierarch').
card_type('noble hierarch', 'Creature — Human Druid').
card_types('noble hierarch', ['Creature']).
card_subtypes('noble hierarch', ['Human', 'Druid']).
card_colors('noble hierarch', ['G']).
card_text('noble hierarch', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{T}: Add {G}, {W}, or {U} to your mana pool.').
card_mana_cost('noble hierarch', ['G']).
card_cmc('noble hierarch', 1).
card_layout('noble hierarch', 'normal').
card_power('noble hierarch', 0).
card_toughness('noble hierarch', 1).

% Found in: INV
card_name('noble panther', 'Noble Panther').
card_type('noble panther', 'Creature — Cat').
card_types('noble panther', ['Creature']).
card_subtypes('noble panther', ['Cat']).
card_colors('noble panther', ['W', 'G']).
card_text('noble panther', '{1}: Noble Panther gains first strike until end of turn.').
card_mana_cost('noble panther', ['1', 'G', 'W']).
card_cmc('noble panther', 3).
card_layout('noble panther', 'normal').
card_power('noble panther', 3).
card_toughness('noble panther', 3).

% Found in: 8ED, MMQ
card_name('noble purpose', 'Noble Purpose').
card_type('noble purpose', 'Enchantment').
card_types('noble purpose', ['Enchantment']).
card_subtypes('noble purpose', []).
card_colors('noble purpose', ['W']).
card_text('noble purpose', 'Whenever a creature you control deals combat damage, you gain that much life.').
card_mana_cost('noble purpose', ['3', 'W', 'W']).
card_cmc('noble purpose', 5).
card_layout('noble purpose', 'normal').

% Found in: BNG
card_name('noble quarry', 'Noble Quarry').
card_type('noble quarry', 'Enchantment Creature — Unicorn').
card_types('noble quarry', ['Enchantment', 'Creature']).
card_subtypes('noble quarry', ['Unicorn']).
card_colors('noble quarry', ['G']).
card_text('noble quarry', 'Bestow {5}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nAll creatures able to block Noble Quarry or enchanted creature do so.\nEnchanted creature gets +1/+1.').
card_mana_cost('noble quarry', ['2', 'G']).
card_cmc('noble quarry', 3).
card_layout('noble quarry', 'normal').
card_power('noble quarry', 1).
card_toughness('noble quarry', 1).

% Found in: NMS
card_name('noble stand', 'Noble Stand').
card_type('noble stand', 'Enchantment').
card_types('noble stand', ['Enchantment']).
card_subtypes('noble stand', []).
card_colors('noble stand', ['W']).
card_text('noble stand', 'Whenever a creature you control blocks, you gain 2 life.').
card_mana_cost('noble stand', ['4', 'W']).
card_cmc('noble stand', 5).
card_layout('noble stand', 'normal').

% Found in: ALL
card_name('noble steeds', 'Noble Steeds').
card_type('noble steeds', 'Enchantment').
card_types('noble steeds', ['Enchantment']).
card_subtypes('noble steeds', []).
card_colors('noble steeds', ['W']).
card_text('noble steeds', '{1}{W}: Target creature gains first strike until end of turn.').
card_mana_cost('noble steeds', ['2', 'W']).
card_cmc('noble steeds', 3).
card_layout('noble steeds', 'normal').

% Found in: CNS, DDO, SCG, VMA
card_name('noble templar', 'Noble Templar').
card_type('noble templar', 'Creature — Human Cleric Soldier').
card_types('noble templar', ['Creature']).
card_subtypes('noble templar', ['Human', 'Cleric', 'Soldier']).
card_colors('noble templar', ['W']).
card_text('noble templar', 'Vigilance\nPlainscycling {2} ({2}, Discard this card: Search your library for a Plains card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('noble templar', ['5', 'W']).
card_cmc('noble templar', 6).
card_layout('noble templar', 'normal').
card_power('noble templar', 3).
card_toughness('noble templar', 6).

% Found in: ZEN
card_name('noble vestige', 'Noble Vestige').
card_type('noble vestige', 'Creature — Spirit').
card_types('noble vestige', ['Creature']).
card_subtypes('noble vestige', ['Spirit']).
card_colors('noble vestige', ['W']).
card_text('noble vestige', 'Flying\n{T}: Prevent the next 1 damage that would be dealt to target player this turn.').
card_mana_cost('noble vestige', ['2', 'W']).
card_cmc('noble vestige', 3).
card_layout('noble vestige', 'normal').
card_power('noble vestige', 1).
card_toughness('noble vestige', 2).

% Found in: 7ED, MIR
card_name('nocturnal raid', 'Nocturnal Raid').
card_type('nocturnal raid', 'Instant').
card_types('nocturnal raid', ['Instant']).
card_subtypes('nocturnal raid', []).
card_colors('nocturnal raid', ['B']).
card_text('nocturnal raid', 'Black creatures get +2/+0 until end of turn.').
card_mana_cost('nocturnal raid', ['2', 'B', 'B']).
card_cmc('nocturnal raid', 4).
card_layout('nocturnal raid', 'normal').

% Found in: USG
card_name('noetic scales', 'Noetic Scales').
card_type('noetic scales', 'Artifact').
card_types('noetic scales', ['Artifact']).
card_subtypes('noetic scales', []).
card_colors('noetic scales', []).
card_text('noetic scales', 'At the beginning of each player\'s upkeep, return to its owner\'s hand each creature that player controls with power greater than the number of cards in his or her hand.').
card_mana_cost('noetic scales', ['4']).
card_cmc('noetic scales', 4).
card_layout('noetic scales', 'normal').

% Found in: MOR
card_name('noggin whack', 'Noggin Whack').
card_type('noggin whack', 'Tribal Sorcery — Rogue').
card_types('noggin whack', ['Tribal', 'Sorcery']).
card_subtypes('noggin whack', ['Rogue']).
card_colors('noggin whack', ['B']).
card_text('noggin whack', 'Prowl {1}{B} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nTarget player reveals three cards from his or her hand. You choose two of them. That player discards those cards.').
card_mana_cost('noggin whack', ['2', 'B', 'B']).
card_cmc('noggin whack', 4).
card_layout('noggin whack', 'normal').

% Found in: EVE
card_name('noggle bandit', 'Noggle Bandit').
card_type('noggle bandit', 'Creature — Noggle Rogue').
card_types('noggle bandit', ['Creature']).
card_subtypes('noggle bandit', ['Noggle', 'Rogue']).
card_colors('noggle bandit', ['U', 'R']).
card_text('noggle bandit', 'Noggle Bandit can\'t be blocked except by creatures with defender.').
card_mana_cost('noggle bandit', ['1', 'U/R', 'U/R']).
card_cmc('noggle bandit', 3).
card_layout('noggle bandit', 'normal').
card_power('noggle bandit', 2).
card_toughness('noggle bandit', 2).

% Found in: EVE
card_name('noggle bridgebreaker', 'Noggle Bridgebreaker').
card_type('noggle bridgebreaker', 'Creature — Noggle Rogue').
card_types('noggle bridgebreaker', ['Creature']).
card_subtypes('noggle bridgebreaker', ['Noggle', 'Rogue']).
card_colors('noggle bridgebreaker', ['U', 'R']).
card_text('noggle bridgebreaker', 'When Noggle Bridgebreaker enters the battlefield, return a land you control to its owner\'s hand.').
card_mana_cost('noggle bridgebreaker', ['2', 'U/R', 'U/R']).
card_cmc('noggle bridgebreaker', 4).
card_layout('noggle bridgebreaker', 'normal').
card_power('noggle bridgebreaker', 4).
card_toughness('noggle bridgebreaker', 3).

% Found in: EVE
card_name('noggle hedge-mage', 'Noggle Hedge-Mage').
card_type('noggle hedge-mage', 'Creature — Noggle Wizard').
card_types('noggle hedge-mage', ['Creature']).
card_subtypes('noggle hedge-mage', ['Noggle', 'Wizard']).
card_colors('noggle hedge-mage', ['U', 'R']).
card_text('noggle hedge-mage', 'When Noggle Hedge-Mage enters the battlefield, if you control two or more Islands, you may tap two target permanents.\nWhen Noggle Hedge-Mage enters the battlefield, if you control two or more Mountains, you may have Noggle Hedge-Mage deal 2 damage to target player.').
card_mana_cost('noggle hedge-mage', ['2', 'U/R']).
card_cmc('noggle hedge-mage', 3).
card_layout('noggle hedge-mage', 'normal').
card_power('noggle hedge-mage', 2).
card_toughness('noggle hedge-mage', 2).

% Found in: EVE, PC2
card_name('noggle ransacker', 'Noggle Ransacker').
card_type('noggle ransacker', 'Creature — Noggle Rogue').
card_types('noggle ransacker', ['Creature']).
card_subtypes('noggle ransacker', ['Noggle', 'Rogue']).
card_colors('noggle ransacker', ['U', 'R']).
card_text('noggle ransacker', 'When Noggle Ransacker enters the battlefield, each player draws two cards, then discards a card at random.').
card_mana_cost('noggle ransacker', ['2', 'U/R']).
card_cmc('noggle ransacker', 3).
card_layout('noggle ransacker', 'normal').
card_power('noggle ransacker', 2).
card_toughness('noggle ransacker', 1).

% Found in: ODY
card_name('nomad decoy', 'Nomad Decoy').
card_type('nomad decoy', 'Creature — Human Nomad').
card_types('nomad decoy', ['Creature']).
card_subtypes('nomad decoy', ['Human', 'Nomad']).
card_colors('nomad decoy', ['W']).
card_text('nomad decoy', '{W}, {T}: Tap target creature.\nThreshold — {W}{W}, {T}: Tap two target creatures. Activate this ability only if seven or more cards are in your graveyard.').
card_mana_cost('nomad decoy', ['2', 'W']).
card_cmc('nomad decoy', 3).
card_layout('nomad decoy', 'normal').
card_power('nomad decoy', 1).
card_toughness('nomad decoy', 2).

% Found in: 10E, JUD
card_name('nomad mythmaker', 'Nomad Mythmaker').
card_type('nomad mythmaker', 'Creature — Human Nomad Cleric').
card_types('nomad mythmaker', ['Creature']).
card_subtypes('nomad mythmaker', ['Human', 'Nomad', 'Cleric']).
card_colors('nomad mythmaker', ['W']).
card_text('nomad mythmaker', '{W}, {T}: Put target Aura card from a graveyard onto the battlefield under your control attached to a creature you control.').
card_mana_cost('nomad mythmaker', ['2', 'W']).
card_cmc('nomad mythmaker', 3).
card_layout('nomad mythmaker', 'normal').
card_power('nomad mythmaker', 2).
card_toughness('nomad mythmaker', 2).

% Found in: DDN, KTK
card_name('nomad outpost', 'Nomad Outpost').
card_type('nomad outpost', 'Land').
card_types('nomad outpost', ['Land']).
card_subtypes('nomad outpost', []).
card_colors('nomad outpost', []).
card_text('nomad outpost', 'Nomad Outpost enters the battlefield tapped.\n{T}: Add {R}, {W}, or {B} to your mana pool.').
card_layout('nomad outpost', 'normal').

% Found in: ODY
card_name('nomad stadium', 'Nomad Stadium').
card_type('nomad stadium', 'Land').
card_types('nomad stadium', ['Land']).
card_subtypes('nomad stadium', []).
card_colors('nomad stadium', []).
card_text('nomad stadium', '{T}: Add {W} to your mana pool. Nomad Stadium deals 1 damage to you.\nThreshold — {W}, {T}, Sacrifice Nomad Stadium: You gain 4 life. Activate this ability only if seven or more cards are in your graveyard.').
card_layout('nomad stadium', 'normal').

% Found in: DDE, INV
card_name('nomadic elf', 'Nomadic Elf').
card_type('nomadic elf', 'Creature — Elf Nomad').
card_types('nomadic elf', ['Creature']).
card_subtypes('nomadic elf', ['Elf', 'Nomad']).
card_colors('nomadic elf', ['G']).
card_text('nomadic elf', '{1}{G}: Add one mana of any color to your mana pool.').
card_mana_cost('nomadic elf', ['1', 'G']).
card_cmc('nomadic elf', 2).
card_layout('nomadic elf', 'normal').
card_power('nomadic elf', 2).
card_toughness('nomadic elf', 2).

% Found in: STH, TPR
card_name('nomads en-kor', 'Nomads en-Kor').
card_type('nomads en-kor', 'Creature — Kor Nomad Soldier').
card_types('nomads en-kor', ['Creature']).
card_subtypes('nomads en-kor', ['Kor', 'Nomad', 'Soldier']).
card_colors('nomads en-kor', ['W']).
card_text('nomads en-kor', '{0}: The next 1 damage that would be dealt to Nomads en-Kor this turn is dealt to target creature you control instead.').
card_mana_cost('nomads en-kor', ['W']).
card_cmc('nomads en-kor', 1).
card_layout('nomads en-kor', 'normal').
card_power('nomads en-kor', 1).
card_toughness('nomads en-kor', 1).

% Found in: C14, ROE
card_name('nomads\' assembly', 'Nomads\' Assembly').
card_type('nomads\' assembly', 'Sorcery').
card_types('nomads\' assembly', ['Sorcery']).
card_subtypes('nomads\' assembly', []).
card_colors('nomads\' assembly', ['W']).
card_text('nomads\' assembly', 'Put a 1/1 white Kor Soldier creature token onto the battlefield for each creature you control.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('nomads\' assembly', ['4', 'W', 'W']).
card_cmc('nomads\' assembly', 6).
card_layout('nomads\' assembly', 'normal').

% Found in: TSP
card_name('norin the wary', 'Norin the Wary').
card_type('norin the wary', 'Legendary Creature — Human Warrior').
card_types('norin the wary', ['Creature']).
card_subtypes('norin the wary', ['Human', 'Warrior']).
card_supertypes('norin the wary', ['Legendary']).
card_colors('norin the wary', ['R']).
card_text('norin the wary', 'When a player casts a spell or a creature attacks, exile Norin the Wary. Return it to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('norin the wary', ['R']).
card_cmc('norin the wary', 1).
card_layout('norin the wary', 'normal').
card_power('norin the wary', 2).
card_toughness('norin the wary', 1).

% Found in: NPH
card_name('norn\'s annex', 'Norn\'s Annex').
card_type('norn\'s annex', 'Artifact').
card_types('norn\'s annex', ['Artifact']).
card_subtypes('norn\'s annex', []).
card_colors('norn\'s annex', ['W']).
card_text('norn\'s annex', '({W/P} can be paid with either {W} or 2 life.)\nCreatures can\'t attack you or a planeswalker you control unless their controller pays {W/P} for each of those creatures.').
card_mana_cost('norn\'s annex', ['3', 'W/P', 'W/P']).
card_cmc('norn\'s annex', 5).
card_layout('norn\'s annex', 'normal').

% Found in: PC2
card_name('norn\'s dominion', 'Norn\'s Dominion').
card_type('norn\'s dominion', 'Plane — New Phyrexia').
card_types('norn\'s dominion', ['Plane']).
card_subtypes('norn\'s dominion', ['New Phyrexia']).
card_colors('norn\'s dominion', []).
card_text('norn\'s dominion', 'When you planeswalk away from Norn\'s Dominion, destroy each nonland permanent without a fate counter on it, then remove all fate counters from all permanents.\nWhenever you roll {C}, you may put a fate counter on target permanent.').
card_layout('norn\'s dominion', 'plane').

% Found in: ICE
card_name('norritt', 'Norritt').
card_type('norritt', 'Creature — Imp').
card_types('norritt', ['Creature']).
card_subtypes('norritt', ['Imp']).
card_colors('norritt', ['B']).
card_text('norritt', '{T}: Untap target blue creature.\n{T}: Choose target non-Wall creature the active player has controlled continuously since the beginning of the turn. That creature attacks this turn if able. If it doesn\'t, destroy it at the beginning of the next end step. Activate this ability only before attackers are declared.').
card_mana_cost('norritt', ['3', 'B']).
card_cmc('norritt', 4).
card_layout('norritt', 'normal').
card_power('norritt', 1).
card_toughness('norritt', 1).

% Found in: LEG
card_name('north star', 'North Star').
card_type('north star', 'Artifact').
card_types('north star', ['Artifact']).
card_subtypes('north star', []).
card_colors('north star', []).
card_text('north star', '{4}, {T}: For one spell this turn, you may spend mana as though it were mana of any color to pay that spell\'s mana cost. (Additional costs are still paid normally.)').
card_mana_cost('north star', ['4']).
card_cmc('north star', 4).
card_layout('north star', 'normal').
card_reserved('north star').

% Found in: 2ED, 3ED, 4ED, 7ED, CED, CEI, LEA, LEB
card_name('northern paladin', 'Northern Paladin').
card_type('northern paladin', 'Creature — Human Knight').
card_types('northern paladin', ['Creature']).
card_subtypes('northern paladin', ['Human', 'Knight']).
card_colors('northern paladin', ['W']).
card_text('northern paladin', '{W}{W}, {T}: Destroy target black permanent.').
card_mana_cost('northern paladin', ['2', 'W', 'W']).
card_cmc('northern paladin', 4).
card_layout('northern paladin', 'normal').
card_power('northern paladin', 3).
card_toughness('northern paladin', 3).

% Found in: PO2, S99
card_name('norwood archers', 'Norwood Archers').
card_type('norwood archers', 'Creature — Elf Archer').
card_types('norwood archers', ['Creature']).
card_subtypes('norwood archers', ['Elf', 'Archer']).
card_colors('norwood archers', ['G']).
card_text('norwood archers', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('norwood archers', ['3', 'G']).
card_cmc('norwood archers', 4).
card_layout('norwood archers', 'normal').
card_power('norwood archers', 3).
card_toughness('norwood archers', 3).

% Found in: PO2, VMA
card_name('norwood priestess', 'Norwood Priestess').
card_type('norwood priestess', 'Creature — Elf Druid').
card_types('norwood priestess', ['Creature']).
card_subtypes('norwood priestess', ['Elf', 'Druid']).
card_colors('norwood priestess', ['G']).
card_text('norwood priestess', '{T}: You may put a green creature card from your hand onto the battlefield. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('norwood priestess', ['2', 'G', 'G']).
card_cmc('norwood priestess', 4).
card_layout('norwood priestess', 'normal').
card_power('norwood priestess', 1).
card_toughness('norwood priestess', 1).

% Found in: 8ED, 9ED, PO2, S99
card_name('norwood ranger', 'Norwood Ranger').
card_type('norwood ranger', 'Creature — Elf Scout').
card_types('norwood ranger', ['Creature']).
card_subtypes('norwood ranger', ['Elf', 'Scout']).
card_colors('norwood ranger', ['G']).
card_text('norwood ranger', '').
card_mana_cost('norwood ranger', ['G']).
card_cmc('norwood ranger', 1).
card_layout('norwood ranger', 'normal').
card_power('norwood ranger', 1).
card_toughness('norwood ranger', 2).

% Found in: PO2
card_name('norwood riders', 'Norwood Riders').
card_type('norwood riders', 'Creature — Elf').
card_types('norwood riders', ['Creature']).
card_subtypes('norwood riders', ['Elf']).
card_colors('norwood riders', ['G']).
card_text('norwood riders', 'Norwood Riders can\'t be blocked by more than one creature.').
card_mana_cost('norwood riders', ['3', 'G']).
card_cmc('norwood riders', 4).
card_layout('norwood riders', 'normal').
card_power('norwood riders', 3).
card_toughness('norwood riders', 3).

% Found in: PO2
card_name('norwood warrior', 'Norwood Warrior').
card_type('norwood warrior', 'Creature — Elf Warrior').
card_types('norwood warrior', ['Creature']).
card_subtypes('norwood warrior', ['Elf', 'Warrior']).
card_colors('norwood warrior', ['G']).
card_text('norwood warrior', 'Whenever Norwood Warrior becomes blocked, it gets +1/+1 until end of turn.').
card_mana_cost('norwood warrior', ['2', 'G']).
card_cmc('norwood warrior', 3).
card_layout('norwood warrior', 'normal').
card_power('norwood warrior', 2).
card_toughness('norwood warrior', 2).

% Found in: TOR, VMA
card_name('nostalgic dreams', 'Nostalgic Dreams').
card_type('nostalgic dreams', 'Sorcery').
card_types('nostalgic dreams', ['Sorcery']).
card_subtypes('nostalgic dreams', []).
card_colors('nostalgic dreams', ['G']).
card_text('nostalgic dreams', 'As an additional cost to cast Nostalgic Dreams, discard X cards.\nReturn X target cards from your graveyard to your hand. Exile Nostalgic Dreams.').
card_mana_cost('nostalgic dreams', ['G', 'G']).
card_cmc('nostalgic dreams', 2).
card_layout('nostalgic dreams', 'normal').

% Found in: ONS
card_name('nosy goblin', 'Nosy Goblin').
card_type('nosy goblin', 'Creature — Goblin').
card_types('nosy goblin', ['Creature']).
card_subtypes('nosy goblin', ['Goblin']).
card_colors('nosy goblin', ['R']).
card_text('nosy goblin', '{T}, Sacrifice Nosy Goblin: Destroy target face-down creature.').
card_mana_cost('nosy goblin', ['2', 'R']).
card_cmc('nosy goblin', 3).
card_layout('nosy goblin', 'normal').
card_power('nosy goblin', 2).
card_toughness('nosy goblin', 1).

% Found in: ROE
card_name('not of this world', 'Not of This World').
card_type('not of this world', 'Tribal Instant — Eldrazi').
card_types('not of this world', ['Tribal', 'Instant']).
card_subtypes('not of this world', ['Eldrazi']).
card_colors('not of this world', []).
card_text('not of this world', 'Counter target spell or ability that targets a permanent you control.\nNot of This World costs {7} less to cast if it targets a spell or ability that targets a creature you control with power 7 or greater.').
card_mana_cost('not of this world', ['7']).
card_cmc('not of this world', 7).
card_layout('not of this world', 'normal').

% Found in: ARC
card_name('nothing can stop me now', 'Nothing Can Stop Me Now').
card_type('nothing can stop me now', 'Ongoing Scheme').
card_types('nothing can stop me now', ['Scheme']).
card_subtypes('nothing can stop me now', []).
card_supertypes('nothing can stop me now', ['Ongoing']).
card_colors('nothing can stop me now', []).
card_text('nothing can stop me now', '(An ongoing scheme remains face up until it\'s abandoned.)\nIf a source an opponent controls would deal damage to you, prevent 1 of that damage.\nAt the beginning of each end step, if you\'ve been dealt 5 or more damage this turn, abandon this scheme.').
card_layout('nothing can stop me now', 'scheme').

% Found in: DGM
card_name('notion thief', 'Notion Thief').
card_type('notion thief', 'Creature — Human Rogue').
card_types('notion thief', ['Creature']).
card_subtypes('notion thief', ['Human', 'Rogue']).
card_colors('notion thief', ['U', 'B']).
card_text('notion thief', 'Flash\nIf an opponent would draw a card except the first one he or she draws in each of his or her draw steps, instead that player skips that draw and you draw a card.').
card_mana_cost('notion thief', ['2', 'U', 'B']).
card_cmc('notion thief', 4).
card_layout('notion thief', 'normal').
card_power('notion thief', 3).
card_toughness('notion thief', 1).

% Found in: MMQ
card_name('notorious assassin', 'Notorious Assassin').
card_type('notorious assassin', 'Creature — Human Spellshaper Assassin').
card_types('notorious assassin', ['Creature']).
card_subtypes('notorious assassin', ['Human', 'Spellshaper', 'Assassin']).
card_colors('notorious assassin', ['B']).
card_text('notorious assassin', '{2}{B}, {T}, Discard a card: Destroy target nonblack creature. It can\'t be regenerated.').
card_mana_cost('notorious assassin', ['3', 'B']).
card_cmc('notorious assassin', 4).
card_layout('notorious assassin', 'normal').
card_power('notorious assassin', 2).
card_toughness('notorious assassin', 2).

% Found in: MOR
card_name('notorious throng', 'Notorious Throng').
card_type('notorious throng', 'Tribal Sorcery — Rogue').
card_types('notorious throng', ['Tribal', 'Sorcery']).
card_subtypes('notorious throng', ['Rogue']).
card_colors('notorious throng', ['U']).
card_text('notorious throng', 'Prowl {5}{U} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nPut X 1/1 black Faerie Rogue creature tokens with flying onto the battlefield, where X is the damage dealt to your opponents this turn. If Notorious Throng\'s prowl cost was paid, take an extra turn after this one.').
card_mana_cost('notorious throng', ['3', 'U']).
card_cmc('notorious throng', 4).
card_layout('notorious throng', 'normal').

% Found in: DST
card_name('nourish', 'Nourish').
card_type('nourish', 'Instant').
card_types('nourish', ['Instant']).
card_subtypes('nourish', []).
card_colors('nourish', ['G']).
card_text('nourish', 'You gain 6 life.').
card_mana_cost('nourish', ['G', 'G']).
card_cmc('nourish', 2).
card_layout('nourish', 'normal').

% Found in: BOK
card_name('nourishing shoal', 'Nourishing Shoal').
card_type('nourishing shoal', 'Instant — Arcane').
card_types('nourishing shoal', ['Instant']).
card_subtypes('nourishing shoal', ['Arcane']).
card_colors('nourishing shoal', ['G']).
card_text('nourishing shoal', 'You may exile a green card with converted mana cost X from your hand rather than pay Nourishing Shoal\'s mana cost.\nYou gain X life.').
card_mana_cost('nourishing shoal', ['X', 'G', 'G']).
card_cmc('nourishing shoal', 2).
card_layout('nourishing shoal', 'normal').

% Found in: LRW
card_name('nova chaser', 'Nova Chaser').
card_type('nova chaser', 'Creature — Elemental Warrior').
card_types('nova chaser', ['Creature']).
card_subtypes('nova chaser', ['Elemental', 'Warrior']).
card_colors('nova chaser', ['R']).
card_text('nova chaser', 'Trample\nChampion an Elemental (When this enters the battlefield, sacrifice it unless you exile another Elemental you control. When this leaves the battlefield, that card returns to the battlefield.)').
card_mana_cost('nova chaser', ['3', 'R']).
card_cmc('nova chaser', 4).
card_layout('nova chaser', 'normal').
card_power('nova chaser', 10).
card_toughness('nova chaser', 2).

% Found in: ONS
card_name('nova cleric', 'Nova Cleric').
card_type('nova cleric', 'Creature — Human Cleric').
card_types('nova cleric', ['Creature']).
card_subtypes('nova cleric', ['Human', 'Cleric']).
card_colors('nova cleric', ['W']).
card_text('nova cleric', '{2}{W}, {T}, Sacrifice Nova Cleric: Destroy all enchantments.').
card_mana_cost('nova cleric', ['W']).
card_cmc('nova cleric', 1).
card_layout('nova cleric', 'normal').
card_power('nova cleric', 1).
card_toughness('nova cleric', 2).

% Found in: LEG, ME3
card_name('nova pentacle', 'Nova Pentacle').
card_type('nova pentacle', 'Artifact').
card_types('nova pentacle', ['Artifact']).
card_subtypes('nova pentacle', []).
card_colors('nova pentacle', []).
card_text('nova pentacle', '{3}, {T}: The next time a source of your choice would deal damage to you this turn, that damage is dealt to target creature of an opponent\'s choice instead.').
card_mana_cost('nova pentacle', ['4']).
card_cmc('nova pentacle', 4).
card_layout('nova pentacle', 'normal').
card_reserved('nova pentacle').

% Found in: WWK
card_name('novablast wurm', 'Novablast Wurm').
card_type('novablast wurm', 'Creature — Wurm').
card_types('novablast wurm', ['Creature']).
card_subtypes('novablast wurm', ['Wurm']).
card_colors('novablast wurm', ['W', 'G']).
card_text('novablast wurm', 'Whenever Novablast Wurm attacks, destroy all other creatures.').
card_mana_cost('novablast wurm', ['3', 'G', 'G', 'W', 'W']).
card_cmc('novablast wurm', 7).
card_layout('novablast wurm', 'normal').
card_power('novablast wurm', 7).
card_toughness('novablast wurm', 7).

% Found in: DIS, MM2
card_name('novijen sages', 'Novijen Sages').
card_type('novijen sages', 'Creature — Human Advisor Mutant').
card_types('novijen sages', ['Creature']).
card_subtypes('novijen sages', ['Human', 'Advisor', 'Mutant']).
card_colors('novijen sages', ['U']).
card_text('novijen sages', 'Graft 4 (This creature enters the battlefield with four +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{1}, Remove two +1/+1 counters from among creatures you control: Draw a card.').
card_mana_cost('novijen sages', ['4', 'U', 'U']).
card_cmc('novijen sages', 6).
card_layout('novijen sages', 'normal').
card_power('novijen sages', 0).
card_toughness('novijen sages', 0).

% Found in: DIS
card_name('novijen, heart of progress', 'Novijen, Heart of Progress').
card_type('novijen, heart of progress', 'Land').
card_types('novijen, heart of progress', ['Land']).
card_subtypes('novijen, heart of progress', []).
card_colors('novijen, heart of progress', []).
card_text('novijen, heart of progress', '{T}: Add {1} to your mana pool.\n{G}{U}, {T}: Put a +1/+1 counter on each creature that entered the battlefield this turn.').
card_layout('novijen, heart of progress', 'normal').

% Found in: UNH
card_name('now i know my abc\'s', 'Now I Know My ABC\'s').
card_type('now i know my abc\'s', 'Enchantment').
card_types('now i know my abc\'s', ['Enchantment']).
card_subtypes('now i know my abc\'s', []).
card_colors('now i know my abc\'s', ['U']).
card_text('now i know my abc\'s', 'At the beginning of your upkeep, if you control permanents with names that include all twenty-six letters of the English alphabet, you win the game.').
card_mana_cost('now i know my abc\'s', ['1', 'U', 'U']).
card_cmc('now i know my abc\'s', 3).
card_layout('now i know my abc\'s', 'normal').

% Found in: FRF
card_name('noxious dragon', 'Noxious Dragon').
card_type('noxious dragon', 'Creature — Dragon').
card_types('noxious dragon', ['Creature']).
card_subtypes('noxious dragon', ['Dragon']).
card_colors('noxious dragon', ['B']).
card_text('noxious dragon', 'Flying\nWhen Noxious Dragon dies, you may destroy target creature with converted mana cost 3 or less.').
card_mana_cost('noxious dragon', ['4', 'B', 'B']).
card_cmc('noxious dragon', 6).
card_layout('noxious dragon', 'normal').
card_power('noxious dragon', 4).
card_toughness('noxious dragon', 4).

% Found in: PCY
card_name('noxious field', 'Noxious Field').
card_type('noxious field', 'Enchantment — Aura').
card_types('noxious field', ['Enchantment']).
card_subtypes('noxious field', ['Aura']).
card_colors('noxious field', ['B']).
card_text('noxious field', 'Enchant land\nEnchanted land has \"{T}: This land deals 1 damage to each creature and each player.\"').
card_mana_cost('noxious field', ['1', 'B', 'B']).
card_cmc('noxious field', 3).
card_layout('noxious field', 'normal').

% Found in: HOP, LGN
card_name('noxious ghoul', 'Noxious Ghoul').
card_type('noxious ghoul', 'Creature — Zombie').
card_types('noxious ghoul', ['Creature']).
card_subtypes('noxious ghoul', ['Zombie']).
card_colors('noxious ghoul', ['B']).
card_text('noxious ghoul', 'Whenever Noxious Ghoul or another Zombie enters the battlefield, all non-Zombie creatures get -1/-1 until end of turn.').
card_mana_cost('noxious ghoul', ['3', 'B', 'B']).
card_cmc('noxious ghoul', 5).
card_layout('noxious ghoul', 'normal').
card_power('noxious ghoul', 3).
card_toughness('noxious ghoul', 3).

% Found in: EVE
card_name('noxious hatchling', 'Noxious Hatchling').
card_type('noxious hatchling', 'Creature — Elemental').
card_types('noxious hatchling', ['Creature']).
card_subtypes('noxious hatchling', ['Elemental']).
card_colors('noxious hatchling', ['B', 'G']).
card_text('noxious hatchling', 'Noxious Hatchling enters the battlefield with four -1/-1 counters on it.\nWither (This deals damage to creatures in the form of -1/-1 counters.)\nWhenever you cast a black spell, remove a -1/-1 counter from Noxious Hatchling.\nWhenever you cast a green spell, remove a -1/-1 counter from Noxious Hatchling.').
card_mana_cost('noxious hatchling', ['3', 'B/G']).
card_cmc('noxious hatchling', 4).
card_layout('noxious hatchling', 'normal').
card_power('noxious hatchling', 6).
card_toughness('noxious hatchling', 6).

% Found in: NPH
card_name('noxious revival', 'Noxious Revival').
card_type('noxious revival', 'Instant').
card_types('noxious revival', ['Instant']).
card_subtypes('noxious revival', []).
card_colors('noxious revival', ['G']).
card_text('noxious revival', '({G/P} can be paid with either {G} or 2 life.)\nPut target card from a graveyard on top of its owner\'s library.').
card_mana_cost('noxious revival', ['G/P']).
card_cmc('noxious revival', 1).
card_layout('noxious revival', 'normal').

% Found in: POR
card_name('noxious toad', 'Noxious Toad').
card_type('noxious toad', 'Creature — Frog').
card_types('noxious toad', ['Creature']).
card_subtypes('noxious toad', ['Frog']).
card_colors('noxious toad', ['B']).
card_text('noxious toad', 'When Noxious Toad dies, each opponent discards a card.').
card_mana_cost('noxious toad', ['2', 'B']).
card_cmc('noxious toad', 3).
card_layout('noxious toad', 'normal').
card_power('noxious toad', 1).
card_toughness('noxious toad', 1).

% Found in: PLS
card_name('noxious vapors', 'Noxious Vapors').
card_type('noxious vapors', 'Sorcery').
card_types('noxious vapors', ['Sorcery']).
card_subtypes('noxious vapors', []).
card_colors('noxious vapors', ['B']).
card_text('noxious vapors', 'Each player reveals his or her hand and chooses one card of each color from it, then discards all other nonland cards.').
card_mana_cost('noxious vapors', ['1', 'B', 'B']).
card_cmc('noxious vapors', 3).
card_layout('noxious vapors', 'normal').

% Found in: BFZ
card_name('noyan dar, roil shaper', 'Noyan Dar, Roil Shaper').
card_type('noyan dar, roil shaper', 'Legendary Creature — Merfolk Ally').
card_types('noyan dar, roil shaper', ['Creature']).
card_subtypes('noyan dar, roil shaper', ['Merfolk', 'Ally']).
card_supertypes('noyan dar, roil shaper', ['Legendary']).
card_colors('noyan dar, roil shaper', ['W', 'U']).
card_text('noyan dar, roil shaper', 'Whenever you cast an instant or sorcery spell, you may put three +1/+1 counters on target land you control. If you do, that land becomes a 0/0 Elemental creature with haste that\'s still a land.').
card_mana_cost('noyan dar, roil shaper', ['3', 'W', 'U']).
card_cmc('noyan dar, roil shaper', 5).
card_layout('noyan dar, roil shaper', 'normal').
card_power('noyan dar, roil shaper', 4).
card_toughness('noyan dar, roil shaper', 4).

% Found in: CMD, EVE
card_name('nucklavee', 'Nucklavee').
card_type('nucklavee', 'Creature — Beast').
card_types('nucklavee', ['Creature']).
card_subtypes('nucklavee', ['Beast']).
card_colors('nucklavee', ['U', 'R']).
card_text('nucklavee', 'When Nucklavee enters the battlefield, you may return target red sorcery card from your graveyard to your hand.\nWhen Nucklavee enters the battlefield, you may return target blue instant card from your graveyard to your hand.').
card_mana_cost('nucklavee', ['4', 'U/R', 'U/R']).
card_cmc('nucklavee', 6).
card_layout('nucklavee', 'normal').
card_power('nucklavee', 4).
card_toughness('nucklavee', 4).

% Found in: HOP, MRD
card_name('nuisance engine', 'Nuisance Engine').
card_type('nuisance engine', 'Artifact').
card_types('nuisance engine', ['Artifact']).
card_subtypes('nuisance engine', []).
card_colors('nuisance engine', []).
card_text('nuisance engine', '{2}, {T}: Put a 0/1 colorless Pest artifact creature token onto the battlefield.').
card_mana_cost('nuisance engine', ['3']).
card_cmc('nuisance engine', 3).
card_layout('nuisance engine', 'normal').

% Found in: EXO
card_name('null brooch', 'Null Brooch').
card_type('null brooch', 'Artifact').
card_types('null brooch', ['Artifact']).
card_subtypes('null brooch', []).
card_colors('null brooch', []).
card_text('null brooch', '{2}, {T}, Discard your hand: Counter target noncreature spell.').
card_mana_cost('null brooch', ['4']).
card_cmc('null brooch', 4).
card_layout('null brooch', 'normal').

% Found in: MIR
card_name('null chamber', 'Null Chamber').
card_type('null chamber', 'World Enchantment').
card_types('null chamber', ['Enchantment']).
card_subtypes('null chamber', []).
card_supertypes('null chamber', ['World']).
card_colors('null chamber', ['W']).
card_text('null chamber', 'As Null Chamber enters the battlefield, you and an opponent each name a card other than a basic land card.\nThe named cards can\'t be played.').
card_mana_cost('null chamber', ['3', 'W']).
card_cmc('null chamber', 4).
card_layout('null chamber', 'normal').
card_reserved('null chamber').

% Found in: ROE
card_name('null champion', 'Null Champion').
card_type('null champion', 'Creature — Zombie Warrior').
card_types('null champion', ['Creature']).
card_subtypes('null champion', ['Zombie', 'Warrior']).
card_colors('null champion', ['B']).
card_text('null champion', 'Level up {3} ({3}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-3\n4/2\nLEVEL 4+\n7/3\n{B}: Regenerate Null Champion.').
card_mana_cost('null champion', ['1', 'B']).
card_cmc('null champion', 2).
card_layout('null champion', 'leveler').
card_power('null champion', 1).
card_toughness('null champion', 1).

% Found in: PLC
card_name('null profusion', 'Null Profusion').
card_type('null profusion', 'Enchantment').
card_types('null profusion', ['Enchantment']).
card_subtypes('null profusion', []).
card_colors('null profusion', ['B']).
card_text('null profusion', 'Skip your draw step.\nWhenever you play a card, draw a card.\nYour maximum hand size is two.').
card_mana_cost('null profusion', ['4', 'B', 'B']).
card_cmc('null profusion', 6).
card_layout('null profusion', 'normal').

% Found in: VMA, WTH
card_name('null rod', 'Null Rod').
card_type('null rod', 'Artifact').
card_types('null rod', ['Artifact']).
card_subtypes('null rod', []).
card_colors('null rod', []).
card_text('null rod', 'Activated abilities of artifacts can\'t be activated.').
card_mana_cost('null rod', ['2']).
card_cmc('null rod', 2).
card_layout('null rod', 'normal').
card_reserved('null rod').

% Found in: BNG
card_name('nullify', 'Nullify').
card_type('nullify', 'Instant').
card_types('nullify', ['Instant']).
card_subtypes('nullify', []).
card_colors('nullify', ['U']).
card_text('nullify', 'Counter target creature or Aura spell.').
card_mana_cost('nullify', ['U', 'U']).
card_cmc('nullify', 2).
card_layout('nullify', 'normal').

% Found in: JUD, PC2
card_name('nullmage advocate', 'Nullmage Advocate').
card_type('nullmage advocate', 'Creature — Insect Druid').
card_types('nullmage advocate', ['Creature']).
card_subtypes('nullmage advocate', ['Insect', 'Druid']).
card_colors('nullmage advocate', ['G']).
card_text('nullmage advocate', '{T}: Return two target cards from an opponent\'s graveyard to his or her hand. Destroy target artifact or enchantment.').
card_mana_cost('nullmage advocate', ['2', 'G']).
card_cmc('nullmage advocate', 3).
card_layout('nullmage advocate', 'normal').
card_power('nullmage advocate', 2).
card_toughness('nullmage advocate', 3).

% Found in: RAV
card_name('nullmage shepherd', 'Nullmage Shepherd').
card_type('nullmage shepherd', 'Creature — Elf Shaman').
card_types('nullmage shepherd', ['Creature']).
card_subtypes('nullmage shepherd', ['Elf', 'Shaman']).
card_colors('nullmage shepherd', ['G']).
card_text('nullmage shepherd', 'Tap four untapped creatures you control: Destroy target artifact or enchantment.').
card_mana_cost('nullmage shepherd', ['3', 'G']).
card_cmc('nullmage shepherd', 4).
card_layout('nullmage shepherd', 'normal').
card_power('nullmage shepherd', 2).
card_toughness('nullmage shepherd', 4).

% Found in: RAV
card_name('nullstone gargoyle', 'Nullstone Gargoyle').
card_type('nullstone gargoyle', 'Artifact Creature — Gargoyle').
card_types('nullstone gargoyle', ['Artifact', 'Creature']).
card_subtypes('nullstone gargoyle', ['Gargoyle']).
card_colors('nullstone gargoyle', []).
card_text('nullstone gargoyle', 'Flying\nWhenever the first noncreature spell of a turn is cast, counter that spell.').
card_mana_cost('nullstone gargoyle', ['9']).
card_cmc('nullstone gargoyle', 9).
card_layout('nullstone gargoyle', 'normal').
card_power('nullstone gargoyle', 4).
card_toughness('nullstone gargoyle', 5).

% Found in: ARB
card_name('nulltread gargantuan', 'Nulltread Gargantuan').
card_type('nulltread gargantuan', 'Creature — Beast').
card_types('nulltread gargantuan', ['Creature']).
card_subtypes('nulltread gargantuan', ['Beast']).
card_colors('nulltread gargantuan', ['U', 'G']).
card_text('nulltread gargantuan', 'When Nulltread Gargantuan enters the battlefield, put a creature you control on top of its owner\'s library.').
card_mana_cost('nulltread gargantuan', ['1', 'G', 'U']).
card_cmc('nulltread gargantuan', 3).
card_layout('nulltread gargantuan', 'normal').
card_power('nulltread gargantuan', 5).
card_toughness('nulltread gargantuan', 6).

% Found in: CHK
card_name('numai outcast', 'Numai Outcast').
card_type('numai outcast', 'Creature — Human Samurai').
card_types('numai outcast', ['Creature']).
card_subtypes('numai outcast', ['Human', 'Samurai']).
card_colors('numai outcast', ['B']).
card_text('numai outcast', 'Bushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)\n{B}, Pay 5 life: Regenerate Numai Outcast.').
card_mana_cost('numai outcast', ['3', 'B']).
card_cmc('numai outcast', 4).
card_layout('numai outcast', 'normal').
card_power('numai outcast', 1).
card_toughness('numai outcast', 1).

% Found in: UNH
card_name('number crunch', 'Number Crunch').
card_type('number crunch', 'Instant').
card_types('number crunch', ['Instant']).
card_subtypes('number crunch', []).
card_colors('number crunch', ['U']).
card_text('number crunch', 'Return target permanent to its owner\'s hand.\nGotcha Whenever an opponent says a number, you may say \"Gotcha\" If you do, return Number Crunch from your graveyard to your hand.').
card_mana_cost('number crunch', ['2', 'U']).
card_cmc('number crunch', 3).
card_layout('number crunch', 'normal').

% Found in: NPH
card_name('numbing dose', 'Numbing Dose').
card_type('numbing dose', 'Enchantment — Aura').
card_types('numbing dose', ['Enchantment']).
card_subtypes('numbing dose', ['Aura']).
card_colors('numbing dose', ['U']).
card_text('numbing dose', 'Enchant artifact or creature\nEnchanted permanent doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted permanent\'s controller, that player loses 1 life.').
card_mana_cost('numbing dose', ['3', 'U', 'U']).
card_cmc('numbing dose', 5).
card_layout('numbing dose', 'normal').

% Found in: CMD, PLC
card_name('numot, the devastator', 'Numot, the Devastator').
card_type('numot, the devastator', 'Legendary Creature — Dragon').
card_types('numot, the devastator', ['Creature']).
card_subtypes('numot, the devastator', ['Dragon']).
card_supertypes('numot, the devastator', ['Legendary']).
card_colors('numot, the devastator', ['W', 'U', 'R']).
card_text('numot, the devastator', 'Flying\nWhenever Numot, the Devastator deals combat damage to a player, you may pay {2}{R}. If you do, destroy up to two target lands.').
card_mana_cost('numot, the devastator', ['3', 'R', 'W', 'U']).
card_cmc('numot, the devastator', 6).
card_layout('numot, the devastator', 'normal').
card_power('numot, the devastator', 6).
card_toughness('numot, the devastator', 6).

% Found in: SHM
card_name('nurturer initiate', 'Nurturer Initiate').
card_type('nurturer initiate', 'Creature — Elf Shaman').
card_types('nurturer initiate', ['Creature']).
card_subtypes('nurturer initiate', ['Elf', 'Shaman']).
card_colors('nurturer initiate', ['G']).
card_text('nurturer initiate', 'Whenever a player casts a green spell, you may pay {1}. If you do, target creature gets +1/+1 until end of turn.').
card_mana_cost('nurturer initiate', ['G']).
card_cmc('nurturer initiate', 1).
card_layout('nurturer initiate', 'normal').
card_power('nurturer initiate', 1).
card_toughness('nurturer initiate', 1).

% Found in: TMP
card_name('nurturing licid', 'Nurturing Licid').
card_type('nurturing licid', 'Creature — Licid').
card_types('nurturing licid', ['Creature']).
card_subtypes('nurturing licid', ['Licid']).
card_colors('nurturing licid', ['G']).
card_text('nurturing licid', '{G}, {T}: Nurturing Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {G} to end this effect.\n{G}: Regenerate enchanted creature.').
card_mana_cost('nurturing licid', ['1', 'G']).
card_cmc('nurturing licid', 2).
card_layout('nurturing licid', 'normal').
card_power('nurturing licid', 1).
card_toughness('nurturing licid', 1).

% Found in: ODY
card_name('nut collector', 'Nut Collector').
card_type('nut collector', 'Creature — Human Druid').
card_types('nut collector', ['Creature']).
card_subtypes('nut collector', ['Human', 'Druid']).
card_colors('nut collector', ['G']).
card_text('nut collector', 'At the beginning of your upkeep, you may put a 1/1 green Squirrel creature token onto the battlefield.\nThreshold — Squirrel creatures get +2/+2 as long as seven or more cards are in your graveyard.').
card_mana_cost('nut collector', ['5', 'G']).
card_cmc('nut collector', 6).
card_layout('nut collector', 'normal').
card_power('nut collector', 1).
card_toughness('nut collector', 1).

% Found in: THS
card_name('nykthos, shrine to nyx', 'Nykthos, Shrine to Nyx').
card_type('nykthos, shrine to nyx', 'Legendary Land').
card_types('nykthos, shrine to nyx', ['Land']).
card_subtypes('nykthos, shrine to nyx', []).
card_supertypes('nykthos, shrine to nyx', ['Legendary']).
card_colors('nykthos, shrine to nyx', []).
card_text('nykthos, shrine to nyx', '{T}: Add {1} to your mana pool.\n{2}, {T}: Choose a color. Add to your mana pool an amount of mana of that color equal to your devotion to that color. (Your devotion to a color is the number of mana symbols of that color in the mana costs of permanents you control.)').
card_layout('nykthos, shrine to nyx', 'normal').

% Found in: THS
card_name('nylea\'s disciple', 'Nylea\'s Disciple').
card_type('nylea\'s disciple', 'Creature — Centaur Archer').
card_types('nylea\'s disciple', ['Creature']).
card_subtypes('nylea\'s disciple', ['Centaur', 'Archer']).
card_colors('nylea\'s disciple', ['G']).
card_text('nylea\'s disciple', 'When Nylea\'s Disciple enters the battlefield, you gain life equal to your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_mana_cost('nylea\'s disciple', ['2', 'G', 'G']).
card_cmc('nylea\'s disciple', 4).
card_layout('nylea\'s disciple', 'normal').
card_power('nylea\'s disciple', 3).
card_toughness('nylea\'s disciple', 3).

% Found in: THS
card_name('nylea\'s emissary', 'Nylea\'s Emissary').
card_type('nylea\'s emissary', 'Enchantment Creature — Cat').
card_types('nylea\'s emissary', ['Enchantment', 'Creature']).
card_subtypes('nylea\'s emissary', ['Cat']).
card_colors('nylea\'s emissary', ['G']).
card_text('nylea\'s emissary', 'Bestow {5}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nTrample\nEnchanted creature gets +3/+3 and has trample.').
card_mana_cost('nylea\'s emissary', ['3', 'G']).
card_cmc('nylea\'s emissary', 4).
card_layout('nylea\'s emissary', 'normal').
card_power('nylea\'s emissary', 3).
card_toughness('nylea\'s emissary', 3).

% Found in: THS
card_name('nylea\'s presence', 'Nylea\'s Presence').
card_type('nylea\'s presence', 'Enchantment — Aura').
card_types('nylea\'s presence', ['Enchantment']).
card_subtypes('nylea\'s presence', ['Aura']).
card_colors('nylea\'s presence', ['G']).
card_text('nylea\'s presence', 'Enchant land\nWhen Nylea\'s Presence enters the battlefield, draw a card.\nEnchanted land is every basic land type in addition to its other types.').
card_mana_cost('nylea\'s presence', ['1', 'G']).
card_cmc('nylea\'s presence', 2).
card_layout('nylea\'s presence', 'normal').

% Found in: THS
card_name('nylea, god of the hunt', 'Nylea, God of the Hunt').
card_type('nylea, god of the hunt', 'Legendary Enchantment Creature — God').
card_types('nylea, god of the hunt', ['Enchantment', 'Creature']).
card_subtypes('nylea, god of the hunt', ['God']).
card_supertypes('nylea, god of the hunt', ['Legendary']).
card_colors('nylea, god of the hunt', ['G']).
card_text('nylea, god of the hunt', 'Indestructible\nAs long as your devotion to green is less than five, Nylea isn\'t a creature. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)\nOther creatures you control have trample.\n{3}{G}: Target creature gets +2/+2 until end of turn.').
card_mana_cost('nylea, god of the hunt', ['3', 'G']).
card_cmc('nylea, god of the hunt', 4).
card_layout('nylea, god of the hunt', 'normal').
card_power('nylea, god of the hunt', 6).
card_toughness('nylea, god of the hunt', 6).

% Found in: JOU
card_name('nyx infusion', 'Nyx Infusion').
card_type('nyx infusion', 'Enchantment — Aura').
card_types('nyx infusion', ['Enchantment']).
card_subtypes('nyx infusion', ['Aura']).
card_colors('nyx infusion', ['B']).
card_text('nyx infusion', 'Enchant creature\nEnchanted creature gets +2/+2 as long as it\'s an enchantment. Otherwise, it gets -2/-2.').
card_mana_cost('nyx infusion', ['2', 'B']).
card_cmc('nyx infusion', 3).
card_layout('nyx infusion', 'normal').

% Found in: JOU
card_name('nyx weaver', 'Nyx Weaver').
card_type('nyx weaver', 'Enchantment Creature — Spider').
card_types('nyx weaver', ['Enchantment', 'Creature']).
card_subtypes('nyx weaver', ['Spider']).
card_colors('nyx weaver', ['B', 'G']).
card_text('nyx weaver', 'Reach\nAt the beginning of your upkeep, put the top two cards of your library into your graveyard.\n{1}{B}{G}, Exile Nyx Weaver: Return target card from your graveyard to your hand.').
card_mana_cost('nyx weaver', ['1', 'B', 'G']).
card_cmc('nyx weaver', 3).
card_layout('nyx weaver', 'normal').
card_power('nyx weaver', 2).
card_toughness('nyx weaver', 3).

% Found in: JOU
card_name('nyx-fleece ram', 'Nyx-Fleece Ram').
card_type('nyx-fleece ram', 'Enchantment Creature — Sheep').
card_types('nyx-fleece ram', ['Enchantment', 'Creature']).
card_subtypes('nyx-fleece ram', ['Sheep']).
card_colors('nyx-fleece ram', ['W']).
card_text('nyx-fleece ram', 'At the beginning of your upkeep, you gain 1 life.').
card_mana_cost('nyx-fleece ram', ['1', 'W']).
card_cmc('nyx-fleece ram', 2).
card_layout('nyx-fleece ram', 'normal').
card_power('nyx-fleece ram', 0).
card_toughness('nyx-fleece ram', 5).

% Found in: CON
card_name('nyxathid', 'Nyxathid').
card_type('nyxathid', 'Creature — Elemental').
card_types('nyxathid', ['Creature']).
card_subtypes('nyxathid', ['Elemental']).
card_colors('nyxathid', ['B']).
card_text('nyxathid', 'As Nyxathid enters the battlefield, choose an opponent.\nNyxathid gets -1/-1 for each card in the chosen player\'s hand.').
card_mana_cost('nyxathid', ['1', 'B', 'B']).
card_cmc('nyxathid', 3).
card_layout('nyxathid', 'normal').
card_power('nyxathid', 7).
card_toughness('nyxathid', 7).

% Found in: BNG
card_name('nyxborn eidolon', 'Nyxborn Eidolon').
card_type('nyxborn eidolon', 'Enchantment Creature — Spirit').
card_types('nyxborn eidolon', ['Enchantment', 'Creature']).
card_subtypes('nyxborn eidolon', ['Spirit']).
card_colors('nyxborn eidolon', ['B']).
card_text('nyxborn eidolon', 'Bestow {4}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +2/+1.').
card_mana_cost('nyxborn eidolon', ['1', 'B']).
card_cmc('nyxborn eidolon', 2).
card_layout('nyxborn eidolon', 'normal').
card_power('nyxborn eidolon', 2).
card_toughness('nyxborn eidolon', 1).

% Found in: BNG
card_name('nyxborn rollicker', 'Nyxborn Rollicker').
card_type('nyxborn rollicker', 'Enchantment Creature — Satyr').
card_types('nyxborn rollicker', ['Enchantment', 'Creature']).
card_subtypes('nyxborn rollicker', ['Satyr']).
card_colors('nyxborn rollicker', ['R']).
card_text('nyxborn rollicker', 'Bestow {1}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +1/+1.').
card_mana_cost('nyxborn rollicker', ['R']).
card_cmc('nyxborn rollicker', 1).
card_layout('nyxborn rollicker', 'normal').
card_power('nyxborn rollicker', 1).
card_toughness('nyxborn rollicker', 1).

% Found in: BNG
card_name('nyxborn shieldmate', 'Nyxborn Shieldmate').
card_type('nyxborn shieldmate', 'Enchantment Creature — Human Soldier').
card_types('nyxborn shieldmate', ['Enchantment', 'Creature']).
card_subtypes('nyxborn shieldmate', ['Human', 'Soldier']).
card_colors('nyxborn shieldmate', ['W']).
card_text('nyxborn shieldmate', 'Bestow {2}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +1/+2.').
card_mana_cost('nyxborn shieldmate', ['W']).
card_cmc('nyxborn shieldmate', 1).
card_layout('nyxborn shieldmate', 'normal').
card_power('nyxborn shieldmate', 1).
card_toughness('nyxborn shieldmate', 2).

% Found in: BNG
card_name('nyxborn triton', 'Nyxborn Triton').
card_type('nyxborn triton', 'Enchantment Creature — Merfolk').
card_types('nyxborn triton', ['Enchantment', 'Creature']).
card_subtypes('nyxborn triton', ['Merfolk']).
card_colors('nyxborn triton', ['U']).
card_text('nyxborn triton', 'Bestow {4}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +2/+3.').
card_mana_cost('nyxborn triton', ['2', 'U']).
card_cmc('nyxborn triton', 3).
card_layout('nyxborn triton', 'normal').
card_power('nyxborn triton', 2).
card_toughness('nyxborn triton', 3).

% Found in: BNG
card_name('nyxborn wolf', 'Nyxborn Wolf').
card_type('nyxborn wolf', 'Enchantment Creature — Wolf').
card_types('nyxborn wolf', ['Enchantment', 'Creature']).
card_subtypes('nyxborn wolf', ['Wolf']).
card_colors('nyxborn wolf', ['G']).
card_text('nyxborn wolf', 'Bestow {4}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nEnchanted creature gets +3/+1.').
card_mana_cost('nyxborn wolf', ['2', 'G']).
card_cmc('nyxborn wolf', 3).
card_layout('nyxborn wolf', 'normal').
card_power('nyxborn wolf', 3).
card_toughness('nyxborn wolf', 1).

