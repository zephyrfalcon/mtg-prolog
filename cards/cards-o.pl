% Card-specific data

% Found in: SOK
card_name('o-naginata', 'O-Naginata').
card_type('o-naginata', 'Artifact — Equipment').
card_types('o-naginata', ['Artifact']).
card_subtypes('o-naginata', ['Equipment']).
card_colors('o-naginata', []).
card_text('o-naginata', 'O-Naginata can be attached only to a creature with power 3 or greater.\nEquipped creature gets +3/+0 and has trample.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('o-naginata', ['1']).
card_cmc('o-naginata', 1).
card_layout('o-naginata', 'normal').

% Found in: RTR
card_name('oak street innkeeper', 'Oak Street Innkeeper').
card_type('oak street innkeeper', 'Creature — Elf').
card_types('oak street innkeeper', ['Creature']).
card_subtypes('oak street innkeeper', ['Elf']).
card_colors('oak street innkeeper', ['G']).
card_text('oak street innkeeper', 'As long as it\'s not your turn, tapped creatures you control have hexproof.').
card_mana_cost('oak street innkeeper', ['2', 'G']).
card_cmc('oak street innkeeper', 3).
card_layout('oak street innkeeper', 'normal').
card_power('oak street innkeeper', 1).
card_toughness('oak street innkeeper', 2).

% Found in: LRW
card_name('oaken brawler', 'Oaken Brawler').
card_type('oaken brawler', 'Creature — Treefolk Warrior').
card_types('oaken brawler', ['Creature']).
card_subtypes('oaken brawler', ['Treefolk', 'Warrior']).
card_colors('oaken brawler', ['W']).
card_text('oaken brawler', 'When Oaken Brawler enters the battlefield, clash with an opponent. If you win, put a +1/+1 counter on Oaken Brawler. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('oaken brawler', ['3', 'W']).
card_cmc('oaken brawler', 4).
card_layout('oaken brawler', 'normal').
card_power('oaken brawler', 2).
card_toughness('oaken brawler', 4).

% Found in: M10
card_name('oakenform', 'Oakenform').
card_type('oakenform', 'Enchantment — Aura').
card_types('oakenform', ['Enchantment']).
card_subtypes('oakenform', ['Aura']).
card_colors('oakenform', ['G']).
card_text('oakenform', 'Enchant creature\nEnchanted creature gets +3/+3.').
card_mana_cost('oakenform', ['2', 'G']).
card_cmc('oakenform', 3).
card_layout('oakenform', 'normal').

% Found in: LRW
card_name('oakgnarl warrior', 'Oakgnarl Warrior').
card_type('oakgnarl warrior', 'Creature — Treefolk Warrior').
card_types('oakgnarl warrior', ['Creature']).
card_subtypes('oakgnarl warrior', ['Treefolk', 'Warrior']).
card_colors('oakgnarl warrior', ['G']).
card_text('oakgnarl warrior', 'Vigilance, trample').
card_mana_cost('oakgnarl warrior', ['5', 'G', 'G']).
card_cmc('oakgnarl warrior', 7).
card_layout('oakgnarl warrior', 'normal').
card_power('oakgnarl warrior', 5).
card_toughness('oakgnarl warrior', 7).

% Found in: JOU
card_name('oakheart dryads', 'Oakheart Dryads').
card_type('oakheart dryads', 'Enchantment Creature — Nymph Dryad').
card_types('oakheart dryads', ['Enchantment', 'Creature']).
card_subtypes('oakheart dryads', ['Nymph', 'Dryad']).
card_colors('oakheart dryads', ['G']).
card_text('oakheart dryads', 'Constellation — Whenever Oakheart Dryads or another enchantment enters the battlefield under your control, target creature gets +1/+1 until end of turn.').
card_mana_cost('oakheart dryads', ['2', 'G']).
card_cmc('oakheart dryads', 3).
card_layout('oakheart dryads', 'normal').
card_power('oakheart dryads', 2).
card_toughness('oakheart dryads', 3).

% Found in: 4ED, ARN, ME4
card_name('oasis', 'Oasis').
card_type('oasis', 'Land').
card_types('oasis', ['Land']).
card_subtypes('oasis', []).
card_colors('oasis', []).
card_text('oasis', '{T}: Prevent the next 1 damage that would be dealt to target creature this turn.').
card_layout('oasis', 'normal').

% Found in: EXO, TPR, VMA, pJGP
card_name('oath of druids', 'Oath of Druids').
card_type('oath of druids', 'Enchantment').
card_types('oath of druids', ['Enchantment']).
card_subtypes('oath of druids', []).
card_colors('oath of druids', ['G']).
card_text('oath of druids', 'At the beginning of each player\'s upkeep, that player chooses target player who controls more creatures than he or she does and is his or her opponent. The first player may reveal cards from the top of his or her library until he or she reveals a creature card. If he or she does, that player puts that card onto the battlefield and all other cards revealed this way into his or her graveyard.').
card_mana_cost('oath of druids', ['1', 'G']).
card_cmc('oath of druids', 2).
card_layout('oath of druids', 'normal').

% Found in: EXO
card_name('oath of ghouls', 'Oath of Ghouls').
card_type('oath of ghouls', 'Enchantment').
card_types('oath of ghouls', ['Enchantment']).
card_subtypes('oath of ghouls', []).
card_colors('oath of ghouls', ['B']).
card_text('oath of ghouls', 'At the beginning of each player\'s upkeep, that player chooses target player whose graveyard has fewer creature cards in it than his or her graveyard does and is his or her opponent. The first player may return a creature card from his or her graveyard to his or her hand.').
card_mana_cost('oath of ghouls', ['1', 'B']).
card_cmc('oath of ghouls', 2).
card_layout('oath of ghouls', 'normal').
card_reserved('oath of ghouls').

% Found in: EXO
card_name('oath of lieges', 'Oath of Lieges').
card_type('oath of lieges', 'Enchantment').
card_types('oath of lieges', ['Enchantment']).
card_subtypes('oath of lieges', []).
card_colors('oath of lieges', ['W']).
card_text('oath of lieges', 'At the beginning of each player\'s upkeep, that player chooses target player who controls more lands than he or she does and is his or her opponent. The first player may search his or her library for a basic land card, put that card onto the battlefield, then shuffle his or her library.').
card_mana_cost('oath of lieges', ['1', 'W']).
card_cmc('oath of lieges', 2).
card_layout('oath of lieges', 'normal').

% Found in: ICE
card_name('oath of lim-dûl', 'Oath of Lim-Dûl').
card_type('oath of lim-dûl', 'Enchantment').
card_types('oath of lim-dûl', ['Enchantment']).
card_subtypes('oath of lim-dûl', []).
card_colors('oath of lim-dûl', ['B']).
card_text('oath of lim-dûl', 'Whenever you lose life, for each 1 life you lost, sacrifice a permanent other than Oath of Lim-Dûl unless you discard a card. (Damage dealt to you causes you to lose life.)\n{B}{B}: Draw a card.').
card_mana_cost('oath of lim-dûl', ['3', 'B']).
card_cmc('oath of lim-dûl', 4).
card_layout('oath of lim-dûl', 'normal').

% Found in: EXO
card_name('oath of mages', 'Oath of Mages').
card_type('oath of mages', 'Enchantment').
card_types('oath of mages', ['Enchantment']).
card_subtypes('oath of mages', []).
card_colors('oath of mages', ['R']).
card_text('oath of mages', 'At the beginning of each player\'s upkeep, that player chooses target player who has more life than he or she does and is his or her opponent. The first player may have Oath of Mages deal 1 damage to the second player.').
card_mana_cost('oath of mages', ['1', 'R']).
card_cmc('oath of mages', 2).
card_layout('oath of mages', 'normal').

% Found in: EXO
card_name('oath of scholars', 'Oath of Scholars').
card_type('oath of scholars', 'Enchantment').
card_types('oath of scholars', ['Enchantment']).
card_subtypes('oath of scholars', []).
card_colors('oath of scholars', ['U']).
card_text('oath of scholars', 'At the beginning of each player\'s upkeep, that player chooses target player who has more cards in hand than he or she does and is his or her opponent. The first player may discard his or her hand and draw three cards.').
card_mana_cost('oath of scholars', ['3', 'U']).
card_cmc('oath of scholars', 4).
card_layout('oath of scholars', 'normal').

% Found in: M14
card_name('oath of the ancient wood', 'Oath of the Ancient Wood').
card_type('oath of the ancient wood', 'Enchantment').
card_types('oath of the ancient wood', ['Enchantment']).
card_subtypes('oath of the ancient wood', []).
card_colors('oath of the ancient wood', ['G']).
card_text('oath of the ancient wood', 'Whenever Oath of the Ancient Wood or another enchantment enters the battlefield under your control, you may put a +1/+1 counter on target creature.').
card_mana_cost('oath of the ancient wood', ['2', 'G']).
card_cmc('oath of the ancient wood', 3).
card_layout('oath of the ancient wood', 'normal').

% Found in: CHK
card_name('oathkeeper, takeno\'s daisho', 'Oathkeeper, Takeno\'s Daisho').
card_type('oathkeeper, takeno\'s daisho', 'Legendary Artifact — Equipment').
card_types('oathkeeper, takeno\'s daisho', ['Artifact']).
card_subtypes('oathkeeper, takeno\'s daisho', ['Equipment']).
card_supertypes('oathkeeper, takeno\'s daisho', ['Legendary']).
card_colors('oathkeeper, takeno\'s daisho', []).
card_text('oathkeeper, takeno\'s daisho', 'Equipped creature gets +3/+1.\nWhenever equipped creature dies, return that card to the battlefield under your control if it\'s a Samurai card.\nWhen Oathkeeper, Takeno\'s Daisho is put into a graveyard from the battlefield, exile equipped creature.\nEquip {2}').
card_mana_cost('oathkeeper, takeno\'s daisho', ['3']).
card_cmc('oathkeeper, takeno\'s daisho', 3).
card_layout('oathkeeper, takeno\'s daisho', 'normal').

% Found in: RAV
card_name('oathsworn giant', 'Oathsworn Giant').
card_type('oathsworn giant', 'Creature — Giant Soldier').
card_types('oathsworn giant', ['Creature']).
card_subtypes('oathsworn giant', ['Giant', 'Soldier']).
card_colors('oathsworn giant', ['W']).
card_text('oathsworn giant', 'Vigilance\nOther creatures you control get +0/+2 and have vigilance.').
card_mana_cost('oathsworn giant', ['4', 'W', 'W']).
card_cmc('oathsworn giant', 6).
card_layout('oathsworn giant', 'normal').
card_power('oathsworn giant', 3).
card_toughness('oathsworn giant', 4).

% Found in: C14
card_name('ob nixilis of the black oath', 'Ob Nixilis of the Black Oath').
card_type('ob nixilis of the black oath', 'Planeswalker — Nixilis').
card_types('ob nixilis of the black oath', ['Planeswalker']).
card_subtypes('ob nixilis of the black oath', ['Nixilis']).
card_colors('ob nixilis of the black oath', ['B']).
card_text('ob nixilis of the black oath', '+2: Each opponent loses 1 life. You gain life equal to the life lost this way.\n−2: Put a 5/5 black Demon creature token with flying onto the battlefield. You lose 2 life.\n−8: You get an emblem with \"{1}{B}, Sacrifice a creature: You gain X life and draw X cards, where X is the sacrificed creature\'s power.\"\nOb Nixilis of the Black Oath can be your commander.').
card_mana_cost('ob nixilis of the black oath', ['3', 'B', 'B']).
card_cmc('ob nixilis of the black oath', 5).
card_layout('ob nixilis of the black oath', 'normal').
card_loyalty('ob nixilis of the black oath', 3).

% Found in: BFZ
card_name('ob nixilis reignited', 'Ob Nixilis Reignited').
card_type('ob nixilis reignited', 'Planeswalker — Nixilis').
card_types('ob nixilis reignited', ['Planeswalker']).
card_subtypes('ob nixilis reignited', ['Nixilis']).
card_colors('ob nixilis reignited', ['B']).
card_text('ob nixilis reignited', '+1: You draw a card and you lose 1 life.\n−3: Destroy target creature.\n−8: Target opponent gets an emblem with \"Whenever a player draws a card, you lose 2 life.\"').
card_mana_cost('ob nixilis reignited', ['3', 'B', 'B']).
card_cmc('ob nixilis reignited', 5).
card_layout('ob nixilis reignited', 'normal').
card_loyalty('ob nixilis reignited', 5).

% Found in: ZEN
card_name('ob nixilis, the fallen', 'Ob Nixilis, the Fallen').
card_type('ob nixilis, the fallen', 'Legendary Creature — Demon').
card_types('ob nixilis, the fallen', ['Creature']).
card_subtypes('ob nixilis, the fallen', ['Demon']).
card_supertypes('ob nixilis, the fallen', ['Legendary']).
card_colors('ob nixilis, the fallen', ['B']).
card_text('ob nixilis, the fallen', 'Landfall — Whenever a land enters the battlefield under your control, you may have target player lose 3 life. If you do, put three +1/+1 counters on Ob Nixilis, the Fallen.').
card_mana_cost('ob nixilis, the fallen', ['3', 'B', 'B']).
card_cmc('ob nixilis, the fallen', 5).
card_layout('ob nixilis, the fallen', 'normal').
card_power('ob nixilis, the fallen', 3).
card_toughness('ob nixilis, the fallen', 3).

% Found in: M15
card_name('ob nixilis, unshackled', 'Ob Nixilis, Unshackled').
card_type('ob nixilis, unshackled', 'Legendary Creature — Demon').
card_types('ob nixilis, unshackled', ['Creature']).
card_subtypes('ob nixilis, unshackled', ['Demon']).
card_supertypes('ob nixilis, unshackled', ['Legendary']).
card_colors('ob nixilis, unshackled', ['B']).
card_text('ob nixilis, unshackled', 'Flying, trample\nWhenever an opponent searches his or her library, that player sacrifices a creature and loses 10 life.\nWhenever another creature dies, put a +1/+1 counter on Ob Nixilis, Unshackled.').
card_mana_cost('ob nixilis, unshackled', ['4', 'B', 'B']).
card_cmc('ob nixilis, unshackled', 6).
card_layout('ob nixilis, unshackled', 'normal').
card_power('ob nixilis, unshackled', 4).
card_toughness('ob nixilis, unshackled', 4).

% Found in: CON, pLPA
card_name('obelisk of alara', 'Obelisk of Alara').
card_type('obelisk of alara', 'Artifact').
card_types('obelisk of alara', ['Artifact']).
card_subtypes('obelisk of alara', []).
card_colors('obelisk of alara', []).
card_text('obelisk of alara', '{1}{W}, {T}: You gain 5 life.\n{1}{U}, {T}: Draw a card, then discard a card.\n{1}{B}, {T}: Target creature gets -2/-2 until end of turn.\n{1}{R}, {T}: Obelisk of Alara deals 3 damage to target player.\n{1}{G}, {T}: Target creature gets +4/+4 until end of turn.').
card_mana_cost('obelisk of alara', ['6']).
card_cmc('obelisk of alara', 6).
card_layout('obelisk of alara', 'normal').

% Found in: ALA
card_name('obelisk of bant', 'Obelisk of Bant').
card_type('obelisk of bant', 'Artifact').
card_types('obelisk of bant', ['Artifact']).
card_subtypes('obelisk of bant', []).
card_colors('obelisk of bant', []).
card_text('obelisk of bant', '{T}: Add {G}, {W}, or {U} to your mana pool.').
card_mana_cost('obelisk of bant', ['3']).
card_cmc('obelisk of bant', 3).
card_layout('obelisk of bant', 'normal').

% Found in: ALA, ARC, C13
card_name('obelisk of esper', 'Obelisk of Esper').
card_type('obelisk of esper', 'Artifact').
card_types('obelisk of esper', ['Artifact']).
card_subtypes('obelisk of esper', []).
card_colors('obelisk of esper', []).
card_text('obelisk of esper', '{T}: Add {W}, {U}, or {B} to your mana pool.').
card_mana_cost('obelisk of esper', ['3']).
card_cmc('obelisk of esper', 3).
card_layout('obelisk of esper', 'normal').

% Found in: ALA, C13, DDH
card_name('obelisk of grixis', 'Obelisk of Grixis').
card_type('obelisk of grixis', 'Artifact').
card_types('obelisk of grixis', ['Artifact']).
card_subtypes('obelisk of grixis', []).
card_colors('obelisk of grixis', []).
card_text('obelisk of grixis', '{T}: Add {U}, {B}, or {R} to your mana pool.').
card_mana_cost('obelisk of grixis', ['3']).
card_cmc('obelisk of grixis', 3).
card_layout('obelisk of grixis', 'normal').

% Found in: ALA, C13
card_name('obelisk of jund', 'Obelisk of Jund').
card_type('obelisk of jund', 'Artifact').
card_types('obelisk of jund', ['Artifact']).
card_subtypes('obelisk of jund', []).
card_colors('obelisk of jund', []).
card_text('obelisk of jund', '{T}: Add {B}, {R}, or {G} to your mana pool.').
card_mana_cost('obelisk of jund', ['3']).
card_cmc('obelisk of jund', 3).
card_layout('obelisk of jund', 'normal').

% Found in: ALA
card_name('obelisk of naya', 'Obelisk of Naya').
card_type('obelisk of naya', 'Artifact').
card_types('obelisk of naya', ['Artifact']).
card_subtypes('obelisk of naya', []).
card_colors('obelisk of naya', []).
card_text('obelisk of naya', '{T}: Add {R}, {G}, or {W} to your mana pool.').
card_mana_cost('obelisk of naya', ['3']).
card_cmc('obelisk of naya', 3).
card_layout('obelisk of naya', 'normal').

% Found in: 5ED, ATQ, CHR, ME4
card_name('obelisk of undoing', 'Obelisk of Undoing').
card_type('obelisk of undoing', 'Artifact').
card_types('obelisk of undoing', ['Artifact']).
card_subtypes('obelisk of undoing', []).
card_colors('obelisk of undoing', []).
card_text('obelisk of undoing', '{6}, {T}: Return target permanent you both own and control to your hand.').
card_mana_cost('obelisk of undoing', ['1']).
card_cmc('obelisk of undoing', 1).
card_layout('obelisk of undoing', 'normal').

% Found in: M15
card_name('obelisk of urd', 'Obelisk of Urd').
card_type('obelisk of urd', 'Artifact').
card_types('obelisk of urd', ['Artifact']).
card_subtypes('obelisk of urd', []).
card_colors('obelisk of urd', []).
card_text('obelisk of urd', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nAs Obelisk of Urd enters the battlefield, choose a creature type.\nCreatures you control of the chosen type get +2/+2.').
card_mana_cost('obelisk of urd', ['6']).
card_cmc('obelisk of urd', 6).
card_layout('obelisk of urd', 'normal').

% Found in: C14, CMD, ONS
card_name('oblation', 'Oblation').
card_type('oblation', 'Instant').
card_types('oblation', ['Instant']).
card_subtypes('oblation', []).
card_colors('oblation', ['W']).
card_text('oblation', 'The owner of target nonland permanent shuffles it into his or her library, then draws two cards.').
card_mana_cost('oblation', ['2', 'W']).
card_cmc('oblation', 3).
card_layout('oblation', 'normal').

% Found in: 8ED, INV
card_name('obliterate', 'Obliterate').
card_type('obliterate', 'Sorcery').
card_types('obliterate', ['Sorcery']).
card_subtypes('obliterate', []).
card_colors('obliterate', ['R']).
card_text('obliterate', 'Obliterate can\'t be countered.\nDestroy all artifacts, creatures, and lands. They can\'t be regenerated.').
card_mana_cost('obliterate', ['6', 'R', 'R']).
card_cmc('obliterate', 8).
card_layout('obliterate', 'normal').

% Found in: FUT
card_name('oblivion crown', 'Oblivion Crown').
card_type('oblivion crown', 'Enchantment — Aura').
card_types('oblivion crown', ['Enchantment']).
card_subtypes('oblivion crown', ['Aura']).
card_colors('oblivion crown', ['B']).
card_text('oblivion crown', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature has \"Discard a card: This creature gets +1/+1 until end of turn.\"').
card_mana_cost('oblivion crown', ['1', 'B']).
card_cmc('oblivion crown', 2).
card_layout('oblivion crown', 'normal').

% Found in: ALA, ARC, CMD, DDG, DDI, HOP, LRW, M12, M13, MM2, pFNM
card_name('oblivion ring', 'Oblivion Ring').
card_type('oblivion ring', 'Enchantment').
card_types('oblivion ring', ['Enchantment']).
card_subtypes('oblivion ring', []).
card_colors('oblivion ring', ['W']).
card_text('oblivion ring', 'When Oblivion Ring enters the battlefield, exile another target nonland permanent.\nWhen Oblivion Ring leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('oblivion ring', ['2', 'W']).
card_cmc('oblivion ring', 3).
card_layout('oblivion ring', 'normal').

% Found in: BFZ, DDP
card_name('oblivion sower', 'Oblivion Sower').
card_type('oblivion sower', 'Creature — Eldrazi').
card_types('oblivion sower', ['Creature']).
card_subtypes('oblivion sower', ['Eldrazi']).
card_colors('oblivion sower', []).
card_text('oblivion sower', 'When you cast Oblivion Sower, target opponent exiles the top four cards of his or her library, then you may put any number of land cards that player owns from exile onto the battlefield under your control.').
card_mana_cost('oblivion sower', ['6']).
card_cmc('oblivion sower', 6).
card_layout('oblivion sower', 'normal').
card_power('oblivion sower', 5).
card_toughness('oblivion sower', 8).

% Found in: CMD, MRD
card_name('oblivion stone', 'Oblivion Stone').
card_type('oblivion stone', 'Artifact').
card_types('oblivion stone', ['Artifact']).
card_subtypes('oblivion stone', []).
card_colors('oblivion stone', []).
card_text('oblivion stone', '{4}, {T}: Put a fate counter on target permanent.\n{5}, {T}, Sacrifice Oblivion Stone: Destroy each nonland permanent without a fate counter on it, then remove all fate counters from all permanents.').
card_mana_cost('oblivion stone', ['3']).
card_cmc('oblivion stone', 3).
card_layout('oblivion stone', 'normal').

% Found in: SOK
card_name('oboro breezecaller', 'Oboro Breezecaller').
card_type('oboro breezecaller', 'Creature — Moonfolk Wizard').
card_types('oboro breezecaller', ['Creature']).
card_subtypes('oboro breezecaller', ['Moonfolk', 'Wizard']).
card_colors('oboro breezecaller', ['U']).
card_text('oboro breezecaller', 'Flying\n{2}, Return a land you control to its owner\'s hand: Untap target land.').
card_mana_cost('oboro breezecaller', ['1', 'U']).
card_cmc('oboro breezecaller', 2).
card_layout('oboro breezecaller', 'normal').
card_power('oboro breezecaller', 1).
card_toughness('oboro breezecaller', 1).

% Found in: SOK
card_name('oboro envoy', 'Oboro Envoy').
card_type('oboro envoy', 'Creature — Moonfolk Wizard').
card_types('oboro envoy', ['Creature']).
card_subtypes('oboro envoy', ['Moonfolk', 'Wizard']).
card_colors('oboro envoy', ['U']).
card_text('oboro envoy', 'Flying\n{2}, Return a land you control to its owner\'s hand: Target creature gets -X/-0 until end of turn, where X is the number of cards in your hand.').
card_mana_cost('oboro envoy', ['3', 'U']).
card_cmc('oboro envoy', 4).
card_layout('oboro envoy', 'normal').
card_power('oboro envoy', 1).
card_toughness('oboro envoy', 3).

% Found in: SOK
card_name('oboro, palace in the clouds', 'Oboro, Palace in the Clouds').
card_type('oboro, palace in the clouds', 'Legendary Land').
card_types('oboro, palace in the clouds', ['Land']).
card_subtypes('oboro, palace in the clouds', []).
card_supertypes('oboro, palace in the clouds', ['Legendary']).
card_colors('oboro, palace in the clouds', []).
card_text('oboro, palace in the clouds', '{T}: Add {U} to your mana pool.\n{1}: Return Oboro, Palace in the Clouds to its owner\'s hand.').
card_layout('oboro, palace in the clouds', 'normal').

% Found in: DTK
card_name('obscuring æther', 'Obscuring Æther').
card_type('obscuring æther', 'Enchantment').
card_types('obscuring æther', ['Enchantment']).
card_subtypes('obscuring æther', []).
card_colors('obscuring æther', ['G']).
card_text('obscuring æther', 'Face-down creature spells you cast cost {1} less to cast.\n{1}{G}: Turn Obscuring Æther face down. (It becomes a 2/2 creature.)').
card_mana_cost('obscuring æther', ['G']).
card_cmc('obscuring æther', 1).
card_layout('obscuring æther', 'normal').

% Found in: THS
card_name('observant alseid', 'Observant Alseid').
card_type('observant alseid', 'Enchantment Creature — Nymph').
card_types('observant alseid', ['Enchantment', 'Creature']).
card_subtypes('observant alseid', ['Nymph']).
card_colors('observant alseid', ['W']).
card_text('observant alseid', 'Bestow {4}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nVigilance\nEnchanted creature gets +2/+2 and has vigilance.').
card_mana_cost('observant alseid', ['2', 'W']).
card_cmc('observant alseid', 3).
card_layout('observant alseid', 'normal').
card_power('observant alseid', 2).
card_toughness('observant alseid', 2).

% Found in: TOR, VMA
card_name('obsessive search', 'Obsessive Search').
card_type('obsessive search', 'Instant').
card_types('obsessive search', ['Instant']).
card_subtypes('obsessive search', []).
card_colors('obsessive search', ['U']).
card_text('obsessive search', 'Draw a card.\nMadness {U} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('obsessive search', ['U']).
card_cmc('obsessive search', 1).
card_layout('obsessive search', 'normal').

% Found in: 2ED, 3ED, 4ED, 6ED, CED, CEI, LEA, LEB, ME4, S00
card_name('obsianus golem', 'Obsianus Golem').
card_type('obsianus golem', 'Artifact Creature — Golem').
card_types('obsianus golem', ['Artifact', 'Creature']).
card_subtypes('obsianus golem', ['Golem']).
card_colors('obsianus golem', []).
card_text('obsianus golem', '').
card_mana_cost('obsianus golem', ['6']).
card_cmc('obsianus golem', 6).
card_layout('obsianus golem', 'normal').
card_power('obsianus golem', 4).
card_toughness('obsianus golem', 6).

% Found in: INV
card_name('obsidian acolyte', 'Obsidian Acolyte').
card_type('obsidian acolyte', 'Creature — Human Cleric').
card_types('obsidian acolyte', ['Creature']).
card_subtypes('obsidian acolyte', ['Human', 'Cleric']).
card_colors('obsidian acolyte', ['W']).
card_text('obsidian acolyte', 'Protection from black\n{W}: Target creature gains protection from black until end of turn.').
card_mana_cost('obsidian acolyte', ['1', 'W']).
card_cmc('obsidian acolyte', 2).
card_layout('obsidian acolyte', 'normal').
card_power('obsidian acolyte', 1).
card_toughness('obsidian acolyte', 1).

% Found in: MOR
card_name('obsidian battle-axe', 'Obsidian Battle-Axe').
card_type('obsidian battle-axe', 'Tribal Artifact — Warrior Equipment').
card_types('obsidian battle-axe', ['Tribal', 'Artifact']).
card_subtypes('obsidian battle-axe', ['Warrior', 'Equipment']).
card_colors('obsidian battle-axe', []).
card_text('obsidian battle-axe', 'Equipped creature gets +2/+1 and has haste.\nWhenever a Warrior creature enters the battlefield, you may attach Obsidian Battle-Axe to it.\nEquip {3}').
card_mana_cost('obsidian battle-axe', ['3']).
card_cmc('obsidian battle-axe', 3).
card_layout('obsidian battle-axe', 'normal').

% Found in: ZEN
card_name('obsidian fireheart', 'Obsidian Fireheart').
card_type('obsidian fireheart', 'Creature — Elemental').
card_types('obsidian fireheart', ['Creature']).
card_subtypes('obsidian fireheart', ['Elemental']).
card_colors('obsidian fireheart', ['R']).
card_text('obsidian fireheart', '{1}{R}{R}: Put a blaze counter on target land without a blaze counter on it. For as long as that land has a blaze counter on it, it has \"At the beginning of your upkeep, this land deals 1 damage to you.\" (The land continues to burn after Obsidian Fireheart has left the battlefield.)').
card_mana_cost('obsidian fireheart', ['1', 'R', 'R', 'R']).
card_cmc('obsidian fireheart', 4).
card_layout('obsidian fireheart', 'normal').
card_power('obsidian fireheart', 4).
card_toughness('obsidian fireheart', 4).

% Found in: PO2
card_name('obsidian giant', 'Obsidian Giant').
card_type('obsidian giant', 'Creature — Giant').
card_types('obsidian giant', ['Creature']).
card_subtypes('obsidian giant', ['Giant']).
card_colors('obsidian giant', ['R']).
card_text('obsidian giant', '').
card_mana_cost('obsidian giant', ['4', 'R']).
card_cmc('obsidian giant', 5).
card_layout('obsidian giant', 'normal').
card_power('obsidian giant', 4).
card_toughness('obsidian giant', 4).

% Found in: M11
card_name('obstinate baloth', 'Obstinate Baloth').
card_type('obstinate baloth', 'Creature — Beast').
card_types('obstinate baloth', ['Creature']).
card_subtypes('obstinate baloth', ['Beast']).
card_colors('obstinate baloth', ['G']).
card_text('obstinate baloth', 'When Obstinate Baloth enters the battlefield, you gain 4 life.\nIf a spell or ability an opponent controls causes you to discard Obstinate Baloth, put it onto the battlefield instead of putting it into your graveyard.').
card_mana_cost('obstinate baloth', ['2', 'G', 'G']).
card_cmc('obstinate baloth', 4).
card_layout('obstinate baloth', 'normal').
card_power('obstinate baloth', 4).
card_toughness('obstinate baloth', 4).

% Found in: ODY
card_name('obstinate familiar', 'Obstinate Familiar').
card_type('obstinate familiar', 'Creature — Lizard').
card_types('obstinate familiar', ['Creature']).
card_subtypes('obstinate familiar', ['Lizard']).
card_colors('obstinate familiar', ['R']).
card_text('obstinate familiar', 'If you would draw a card, you may skip that draw instead.').
card_mana_cost('obstinate familiar', ['R']).
card_cmc('obstinate familiar', 1).
card_layout('obstinate familiar', 'normal').
card_power('obstinate familiar', 1).
card_toughness('obstinate familiar', 1).

% Found in: DGM
card_name('obzedat\'s aid', 'Obzedat\'s Aid').
card_type('obzedat\'s aid', 'Sorcery').
card_types('obzedat\'s aid', ['Sorcery']).
card_subtypes('obzedat\'s aid', []).
card_colors('obzedat\'s aid', ['W', 'B']).
card_text('obzedat\'s aid', 'Return target permanent card from your graveyard to the battlefield.').
card_mana_cost('obzedat\'s aid', ['3', 'W', 'B']).
card_cmc('obzedat\'s aid', 5).
card_layout('obzedat\'s aid', 'normal').

% Found in: GTC
card_name('obzedat, ghost council', 'Obzedat, Ghost Council').
card_type('obzedat, ghost council', 'Legendary Creature — Spirit Advisor').
card_types('obzedat, ghost council', ['Creature']).
card_subtypes('obzedat, ghost council', ['Spirit', 'Advisor']).
card_supertypes('obzedat, ghost council', ['Legendary']).
card_colors('obzedat, ghost council', ['W', 'B']).
card_text('obzedat, ghost council', 'When Obzedat, Ghost Council enters the battlefield, target opponent loses 2 life and you gain 2 life.\nAt the beginning of your end step, you may exile Obzedat. If you do, return it to the battlefield under its owner\'s control at the beginning of your next upkeep. It gains haste.').
card_mana_cost('obzedat, ghost council', ['1', 'W', 'W', 'B', 'B']).
card_cmc('obzedat, ghost council', 5).
card_layout('obzedat, ghost council', 'normal').
card_power('obzedat, ghost council', 5).
card_toughness('obzedat, ghost council', 5).

% Found in: DIS
card_name('ocular halo', 'Ocular Halo').
card_type('ocular halo', 'Enchantment — Aura').
card_types('ocular halo', ['Enchantment']).
card_subtypes('ocular halo', ['Aura']).
card_colors('ocular halo', ['U']).
card_text('ocular halo', 'Enchant creature\nEnchanted creature has \"{T}: Draw a card.\"\n{W}: Enchanted creature gains vigilance until end of turn.').
card_mana_cost('ocular halo', ['3', 'U']).
card_cmc('ocular halo', 4).
card_layout('ocular halo', 'normal').

% Found in: MBS
card_name('oculus', 'Oculus').
card_type('oculus', 'Creature — Homunculus').
card_types('oculus', ['Creature']).
card_subtypes('oculus', ['Homunculus']).
card_colors('oculus', ['U']).
card_text('oculus', 'When Oculus dies, you may draw a card.').
card_mana_cost('oculus', ['1', 'U']).
card_cmc('oculus', 2).
card_layout('oculus', 'normal').
card_power('oculus', 1).
card_toughness('oculus', 1).

% Found in: DIS
card_name('odds', 'Odds').
card_type('odds', 'Instant').
card_types('odds', ['Instant']).
card_subtypes('odds', []).
card_colors('odds', ['U', 'R']).
card_text('odds', 'Flip a coin. If it comes up heads, counter target instant or sorcery spell. If it comes up tails, copy that spell and you may choose new targets for the copy.').
card_mana_cost('odds', ['U', 'R']).
card_cmc('odds', 2).
card_layout('odds', 'split').
card_sides('odds', 'ends').

% Found in: EVE
card_name('odious trow', 'Odious Trow').
card_type('odious trow', 'Creature — Troll').
card_types('odious trow', ['Creature']).
card_subtypes('odious trow', ['Troll']).
card_colors('odious trow', ['B', 'G']).
card_text('odious trow', '{1}{B/G}: Regenerate Odious Trow.').
card_mana_cost('odious trow', ['B/G']).
card_cmc('odious trow', 1).
card_layout('odious trow', 'normal').
card_power('odious trow', 1).
card_toughness('odious trow', 1).

% Found in: M13
card_name('odric, master tactician', 'Odric, Master Tactician').
card_type('odric, master tactician', 'Legendary Creature — Human Soldier').
card_types('odric, master tactician', ['Creature']).
card_subtypes('odric, master tactician', ['Human', 'Soldier']).
card_supertypes('odric, master tactician', ['Legendary']).
card_colors('odric, master tactician', ['W']).
card_text('odric, master tactician', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhenever Odric, Master Tactician and at least three other creatures attack, you choose which creatures block this combat and how those creatures block.').
card_mana_cost('odric, master tactician', ['2', 'W', 'W']).
card_cmc('odric, master tactician', 4).
card_layout('odric, master tactician', 'normal').
card_power('odric, master tactician', 3).
card_toughness('odric, master tactician', 4).

% Found in: BNG
card_name('odunos river trawler', 'Odunos River Trawler').
card_type('odunos river trawler', 'Creature — Zombie').
card_types('odunos river trawler', ['Creature']).
card_subtypes('odunos river trawler', ['Zombie']).
card_colors('odunos river trawler', ['B']).
card_text('odunos river trawler', 'When Odunos River Trawler enters the battlefield, return target enchantment creature card from your graveyard to your hand.\n{W}, Sacrifice Odunos River Trawler: Return target enchantment creature card from your graveyard to your hand.').
card_mana_cost('odunos river trawler', ['2', 'B']).
card_cmc('odunos river trawler', 3).
card_layout('odunos river trawler', 'normal').
card_power('odunos river trawler', 2).
card_toughness('odunos river trawler', 2).

% Found in: WTH
card_name('odylic wraith', 'Odylic Wraith').
card_type('odylic wraith', 'Creature — Wraith').
card_types('odylic wraith', ['Creature']).
card_subtypes('odylic wraith', ['Wraith']).
card_colors('odylic wraith', ['B']).
card_text('odylic wraith', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\nWhenever Odylic Wraith deals damage to a player, that player discards a card.').
card_mana_cost('odylic wraith', ['3', 'B']).
card_cmc('odylic wraith', 4).
card_layout('odylic wraith', 'normal').
card_power('odylic wraith', 2).
card_toughness('odylic wraith', 2).

% Found in: NMS
card_name('off balance', 'Off Balance').
card_type('off balance', 'Instant').
card_types('off balance', ['Instant']).
card_subtypes('off balance', []).
card_colors('off balance', ['W']).
card_text('off balance', 'Target creature can\'t attack or block this turn.').
card_mana_cost('off balance', ['W']).
card_cmc('off balance', 1).
card_layout('off balance', 'normal').

% Found in: MOR
card_name('offalsnout', 'Offalsnout').
card_type('offalsnout', 'Creature — Elemental').
card_types('offalsnout', ['Creature']).
card_subtypes('offalsnout', ['Elemental']).
card_colors('offalsnout', ['B']).
card_text('offalsnout', 'Flash\nWhen Offalsnout leaves the battlefield, exile target card from a graveyard.\nEvoke {B} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('offalsnout', ['2', 'B']).
card_cmc('offalsnout', 3).
card_layout('offalsnout', 'normal').
card_power('offalsnout', 2).
card_toughness('offalsnout', 2).

% Found in: ARB
card_name('offering to asha', 'Offering to Asha').
card_type('offering to asha', 'Instant').
card_types('offering to asha', ['Instant']).
card_subtypes('offering to asha', []).
card_colors('offering to asha', ['W', 'U']).
card_text('offering to asha', 'Counter target spell unless its controller pays {4}. You gain 4 life.').
card_mana_cost('offering to asha', ['2', 'W', 'U']).
card_cmc('offering to asha', 4).
card_layout('offering to asha', 'normal').

% Found in: PO2, pMEI
card_name('ogre arsonist', 'Ogre Arsonist').
card_type('ogre arsonist', 'Creature — Ogre').
card_types('ogre arsonist', ['Creature']).
card_subtypes('ogre arsonist', ['Ogre']).
card_colors('ogre arsonist', ['R']).
card_text('ogre arsonist', 'When Ogre Arsonist enters the battlefield, destroy target land.').
card_mana_cost('ogre arsonist', ['4', 'R']).
card_cmc('ogre arsonist', 5).
card_layout('ogre arsonist', 'normal').
card_power('ogre arsonist', 3).
card_toughness('ogre arsonist', 3).

% Found in: DDN, M14, pMEI
card_name('ogre battledriver', 'Ogre Battledriver').
card_type('ogre battledriver', 'Creature — Ogre Warrior').
card_types('ogre battledriver', ['Creature']).
card_subtypes('ogre battledriver', ['Ogre', 'Warrior']).
card_colors('ogre battledriver', ['R']).
card_text('ogre battledriver', 'Whenever another creature enters the battlefield under your control, that creature gets +2/+0 and gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('ogre battledriver', ['2', 'R', 'R']).
card_cmc('ogre battledriver', 4).
card_layout('ogre battledriver', 'normal').
card_power('ogre battledriver', 3).
card_toughness('ogre battledriver', 3).

% Found in: PO2
card_name('ogre berserker', 'Ogre Berserker').
card_type('ogre berserker', 'Creature — Ogre Berserker').
card_types('ogre berserker', ['Creature']).
card_subtypes('ogre berserker', ['Ogre', 'Berserker']).
card_colors('ogre berserker', ['R']).
card_text('ogre berserker', 'Haste').
card_mana_cost('ogre berserker', ['4', 'R']).
card_cmc('ogre berserker', 5).
card_layout('ogre berserker', 'normal').
card_power('ogre berserker', 4).
card_toughness('ogre berserker', 2).

% Found in: VIS
card_name('ogre enforcer', 'Ogre Enforcer').
card_type('ogre enforcer', 'Creature — Ogre').
card_types('ogre enforcer', ['Creature']).
card_subtypes('ogre enforcer', ['Ogre']).
card_colors('ogre enforcer', ['R']).
card_text('ogre enforcer', 'Ogre Enforcer can\'t be destroyed by lethal damage unless lethal damage dealt by a single source is marked on it.').
card_mana_cost('ogre enforcer', ['3', 'R', 'R']).
card_cmc('ogre enforcer', 5).
card_layout('ogre enforcer', 'normal').
card_power('ogre enforcer', 4).
card_toughness('ogre enforcer', 4).
card_reserved('ogre enforcer').

% Found in: DIS
card_name('ogre gatecrasher', 'Ogre Gatecrasher').
card_type('ogre gatecrasher', 'Creature — Ogre Rogue').
card_types('ogre gatecrasher', ['Creature']).
card_subtypes('ogre gatecrasher', ['Ogre', 'Rogue']).
card_colors('ogre gatecrasher', ['R']).
card_text('ogre gatecrasher', 'When Ogre Gatecrasher enters the battlefield, destroy target creature with defender.').
card_mana_cost('ogre gatecrasher', ['3', 'R']).
card_cmc('ogre gatecrasher', 4).
card_layout('ogre gatecrasher', 'normal').
card_power('ogre gatecrasher', 3).
card_toughness('ogre gatecrasher', 3).

% Found in: SOM
card_name('ogre geargrabber', 'Ogre Geargrabber').
card_type('ogre geargrabber', 'Creature — Ogre Warrior').
card_types('ogre geargrabber', ['Creature']).
card_subtypes('ogre geargrabber', ['Ogre', 'Warrior']).
card_colors('ogre geargrabber', ['R']).
card_text('ogre geargrabber', 'Whenever Ogre Geargrabber attacks, gain control of target Equipment an opponent controls until end of turn. Attach it to Ogre Geargrabber. When you lose control of that Equipment, unattach it.').
card_mana_cost('ogre geargrabber', ['4', 'R', 'R']).
card_cmc('ogre geargrabber', 6).
card_layout('ogre geargrabber', 'normal').
card_power('ogre geargrabber', 4).
card_toughness('ogre geargrabber', 4).

% Found in: RTR
card_name('ogre jailbreaker', 'Ogre Jailbreaker').
card_type('ogre jailbreaker', 'Creature — Ogre Rogue').
card_types('ogre jailbreaker', ['Creature']).
card_subtypes('ogre jailbreaker', ['Ogre', 'Rogue']).
card_colors('ogre jailbreaker', ['B']).
card_text('ogre jailbreaker', 'Defender\nOgre Jailbreaker can attack as though it didn\'t have defender as long as you control a Gate.').
card_mana_cost('ogre jailbreaker', ['3', 'B']).
card_cmc('ogre jailbreaker', 4).
card_layout('ogre jailbreaker', 'normal').
card_power('ogre jailbreaker', 4).
card_toughness('ogre jailbreaker', 4).

% Found in: MRD
card_name('ogre leadfoot', 'Ogre Leadfoot').
card_type('ogre leadfoot', 'Creature — Ogre').
card_types('ogre leadfoot', ['Creature']).
card_subtypes('ogre leadfoot', ['Ogre']).
card_colors('ogre leadfoot', ['R']).
card_text('ogre leadfoot', 'Whenever Ogre Leadfoot becomes blocked by an artifact creature, destroy that creature.').
card_mana_cost('ogre leadfoot', ['4', 'R']).
card_cmc('ogre leadfoot', 5).
card_layout('ogre leadfoot', 'normal').
card_power('ogre leadfoot', 3).
card_toughness('ogre leadfoot', 3).

% Found in: BOK
card_name('ogre marauder', 'Ogre Marauder').
card_type('ogre marauder', 'Creature — Ogre Warrior').
card_types('ogre marauder', ['Creature']).
card_subtypes('ogre marauder', ['Ogre', 'Warrior']).
card_colors('ogre marauder', ['B']).
card_text('ogre marauder', 'Whenever Ogre Marauder attacks, it gains \"Ogre Marauder can\'t be blocked\" until end of turn unless defending player sacrifices a creature.').
card_mana_cost('ogre marauder', ['1', 'B', 'B']).
card_cmc('ogre marauder', 3).
card_layout('ogre marauder', 'normal').
card_power('ogre marauder', 3).
card_toughness('ogre marauder', 1).

% Found in: NPH
card_name('ogre menial', 'Ogre Menial').
card_type('ogre menial', 'Creature — Ogre').
card_types('ogre menial', ['Creature']).
card_subtypes('ogre menial', ['Ogre']).
card_colors('ogre menial', ['R']).
card_text('ogre menial', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{R}: Ogre Menial gets +1/+0 until end of turn.').
card_mana_cost('ogre menial', ['3', 'R']).
card_cmc('ogre menial', 4).
card_layout('ogre menial', 'normal').
card_power('ogre menial', 0).
card_toughness('ogre menial', 4).

% Found in: BOK
card_name('ogre recluse', 'Ogre Recluse').
card_type('ogre recluse', 'Creature — Ogre Warrior').
card_types('ogre recluse', ['Creature']).
card_subtypes('ogre recluse', ['Ogre', 'Warrior']).
card_colors('ogre recluse', ['R']).
card_text('ogre recluse', 'Whenever a player casts a spell, tap Ogre Recluse.').
card_mana_cost('ogre recluse', ['3', 'R']).
card_cmc('ogre recluse', 4).
card_layout('ogre recluse', 'normal').
card_power('ogre recluse', 5).
card_toughness('ogre recluse', 4).

% Found in: MBS
card_name('ogre resister', 'Ogre Resister').
card_type('ogre resister', 'Creature — Ogre').
card_types('ogre resister', ['Creature']).
card_subtypes('ogre resister', ['Ogre']).
card_colors('ogre resister', ['R']).
card_text('ogre resister', '').
card_mana_cost('ogre resister', ['2', 'R', 'R']).
card_cmc('ogre resister', 4).
card_layout('ogre resister', 'normal').
card_power('ogre resister', 4).
card_toughness('ogre resister', 3).

% Found in: DDH, DDJ, GPT
card_name('ogre savant', 'Ogre Savant').
card_type('ogre savant', 'Creature — Ogre Wizard').
card_types('ogre savant', ['Creature']).
card_subtypes('ogre savant', ['Ogre', 'Wizard']).
card_colors('ogre savant', ['R']).
card_text('ogre savant', 'When Ogre Savant enters the battlefield, if {U} was spent to cast Ogre Savant, return target creature to its owner\'s hand.').
card_mana_cost('ogre savant', ['4', 'R']).
card_cmc('ogre savant', 5).
card_layout('ogre savant', 'normal').
card_power('ogre savant', 3).
card_toughness('ogre savant', 2).

% Found in: ROE
card_name('ogre sentry', 'Ogre Sentry').
card_type('ogre sentry', 'Creature — Ogre Warrior').
card_types('ogre sentry', ['Creature']).
card_subtypes('ogre sentry', ['Ogre', 'Warrior']).
card_colors('ogre sentry', ['R']).
card_text('ogre sentry', 'Defender').
card_mana_cost('ogre sentry', ['1', 'R']).
card_cmc('ogre sentry', 2).
card_layout('ogre sentry', 'normal').
card_power('ogre sentry', 3).
card_toughness('ogre sentry', 3).

% Found in: EXO, TPR
card_name('ogre shaman', 'Ogre Shaman').
card_type('ogre shaman', 'Creature — Ogre Shaman').
card_types('ogre shaman', ['Creature']).
card_subtypes('ogre shaman', ['Ogre', 'Shaman']).
card_colors('ogre shaman', ['R']).
card_text('ogre shaman', '{2}, Discard a card at random: Ogre Shaman deals 2 damage to target creature or player.').
card_mana_cost('ogre shaman', ['3', 'R', 'R']).
card_cmc('ogre shaman', 5).
card_layout('ogre shaman', 'normal').
card_power('ogre shaman', 3).
card_toughness('ogre shaman', 3).

% Found in: GTC
card_name('ogre slumlord', 'Ogre Slumlord').
card_type('ogre slumlord', 'Creature — Ogre Rogue').
card_types('ogre slumlord', ['Creature']).
card_subtypes('ogre slumlord', ['Ogre', 'Rogue']).
card_colors('ogre slumlord', ['B']).
card_text('ogre slumlord', 'Whenever another nontoken creature dies, you may put a 1/1 black Rat creature token onto the battlefield.\nRats you control have deathtouch.').
card_mana_cost('ogre slumlord', ['3', 'B', 'B']).
card_cmc('ogre slumlord', 5).
card_layout('ogre slumlord', 'normal').
card_power('ogre slumlord', 3).
card_toughness('ogre slumlord', 3).

% Found in: 7ED, 8ED, 9ED, ME4, MMQ, PO2
card_name('ogre taskmaster', 'Ogre Taskmaster').
card_type('ogre taskmaster', 'Creature — Ogre').
card_types('ogre taskmaster', ['Creature']).
card_subtypes('ogre taskmaster', ['Ogre']).
card_colors('ogre taskmaster', ['R']).
card_text('ogre taskmaster', 'Ogre Taskmaster can\'t block.').
card_mana_cost('ogre taskmaster', ['3', 'R']).
card_cmc('ogre taskmaster', 4).
card_layout('ogre taskmaster', 'normal').
card_power('ogre taskmaster', 4).
card_toughness('ogre taskmaster', 3).

% Found in: PO2, S00, S99
card_name('ogre warrior', 'Ogre Warrior').
card_type('ogre warrior', 'Creature — Ogre Warrior').
card_types('ogre warrior', ['Creature']).
card_subtypes('ogre warrior', ['Ogre', 'Warrior']).
card_colors('ogre warrior', ['R']).
card_text('ogre warrior', '').
card_mana_cost('ogre warrior', ['3', 'R']).
card_cmc('ogre warrior', 4).
card_layout('ogre warrior', 'normal').
card_power('ogre warrior', 3).
card_toughness('ogre warrior', 3).

% Found in: ROE
card_name('ogre\'s cleaver', 'Ogre\'s Cleaver').
card_type('ogre\'s cleaver', 'Artifact — Equipment').
card_types('ogre\'s cleaver', ['Artifact']).
card_subtypes('ogre\'s cleaver', ['Equipment']).
card_colors('ogre\'s cleaver', []).
card_text('ogre\'s cleaver', 'Equipped creature gets +5/+0.\nEquip {5}').
card_mana_cost('ogre\'s cleaver', ['2']).
card_cmc('ogre\'s cleaver', 2).
card_layout('ogre\'s cleaver', 'normal').

% Found in: CSP, DDM
card_name('ohran viper', 'Ohran Viper').
card_type('ohran viper', 'Snow Creature — Snake').
card_types('ohran viper', ['Creature']).
card_subtypes('ohran viper', ['Snake']).
card_supertypes('ohran viper', ['Snow']).
card_colors('ohran viper', ['G']).
card_text('ohran viper', 'Whenever Ohran Viper deals combat damage to a creature, destroy that creature at end of combat.\nWhenever Ohran Viper deals combat damage to a player, you may draw a card.').
card_mana_cost('ohran viper', ['1', 'G', 'G']).
card_cmc('ohran viper', 3).
card_layout('ohran viper', 'normal').
card_power('ohran viper', 1).
card_toughness('ohran viper', 3).

% Found in: CSP
card_name('ohran yeti', 'Ohran Yeti').
card_type('ohran yeti', 'Snow Creature — Yeti').
card_types('ohran yeti', ['Creature']).
card_subtypes('ohran yeti', ['Yeti']).
card_supertypes('ohran yeti', ['Snow']).
card_colors('ohran yeti', ['R']).
card_text('ohran yeti', '{2}{S}: Target snow creature gains first strike until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('ohran yeti', ['3', 'R']).
card_cmc('ohran yeti', 4).
card_layout('ohran yeti', 'normal').
card_power('ohran yeti', 3).
card_toughness('ohran yeti', 3).

% Found in: DTK
card_name('ojutai exemplars', 'Ojutai Exemplars').
card_type('ojutai exemplars', 'Creature — Human Monk').
card_types('ojutai exemplars', ['Creature']).
card_subtypes('ojutai exemplars', ['Human', 'Monk']).
card_colors('ojutai exemplars', ['W']).
card_text('ojutai exemplars', 'Whenever you cast a noncreature spell, choose one —\n• Tap target creature.\n• Ojutai Exemplars gains first strike and lifelink until end of turn.\n• Exile Ojutai Exemplars, then return it to the battlefield tapped under its owner\'s control.').
card_mana_cost('ojutai exemplars', ['2', 'W', 'W']).
card_cmc('ojutai exemplars', 4).
card_layout('ojutai exemplars', 'normal').
card_power('ojutai exemplars', 4).
card_toughness('ojutai exemplars', 4).

% Found in: DTK
card_name('ojutai interceptor', 'Ojutai Interceptor').
card_type('ojutai interceptor', 'Creature — Bird Soldier').
card_types('ojutai interceptor', ['Creature']).
card_subtypes('ojutai interceptor', ['Bird', 'Soldier']).
card_colors('ojutai interceptor', ['U']).
card_text('ojutai interceptor', 'Flying\nMegamorph {3}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('ojutai interceptor', ['3', 'U']).
card_cmc('ojutai interceptor', 4).
card_layout('ojutai interceptor', 'normal').
card_power('ojutai interceptor', 3).
card_toughness('ojutai interceptor', 1).

% Found in: DTK
card_name('ojutai monument', 'Ojutai Monument').
card_type('ojutai monument', 'Artifact').
card_types('ojutai monument', ['Artifact']).
card_subtypes('ojutai monument', []).
card_colors('ojutai monument', []).
card_text('ojutai monument', '{T}: Add {W} or {U} to your mana pool.\n{4}{W}{U}: Ojutai Monument becomes a 4/4 white and blue Dragon artifact creature with flying until end of turn.').
card_mana_cost('ojutai monument', ['3']).
card_cmc('ojutai monument', 3).
card_layout('ojutai monument', 'normal').

% Found in: DTK
card_name('ojutai\'s breath', 'Ojutai\'s Breath').
card_type('ojutai\'s breath', 'Instant').
card_types('ojutai\'s breath', ['Instant']).
card_subtypes('ojutai\'s breath', []).
card_colors('ojutai\'s breath', ['U']).
card_text('ojutai\'s breath', 'Tap target creature. It doesn\'t untap during its controller\'s next untap step.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('ojutai\'s breath', ['2', 'U']).
card_cmc('ojutai\'s breath', 3).
card_layout('ojutai\'s breath', 'normal').

% Found in: DTK, pMEI
card_name('ojutai\'s command', 'Ojutai\'s Command').
card_type('ojutai\'s command', 'Instant').
card_types('ojutai\'s command', ['Instant']).
card_subtypes('ojutai\'s command', []).
card_colors('ojutai\'s command', ['W', 'U']).
card_text('ojutai\'s command', 'Choose two —\n• Return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.\n• You gain 4 life.\n• Counter target creature spell.\n• Draw a card.').
card_mana_cost('ojutai\'s command', ['2', 'W', 'U']).
card_cmc('ojutai\'s command', 4).
card_layout('ojutai\'s command', 'normal').

% Found in: DTK
card_name('ojutai\'s summons', 'Ojutai\'s Summons').
card_type('ojutai\'s summons', 'Sorcery').
card_types('ojutai\'s summons', ['Sorcery']).
card_subtypes('ojutai\'s summons', []).
card_colors('ojutai\'s summons', ['U']).
card_text('ojutai\'s summons', 'Put a 2/2 blue Djinn Monk creature token with flying onto the battlefield.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('ojutai\'s summons', ['3', 'U', 'U']).
card_cmc('ojutai\'s summons', 5).
card_layout('ojutai\'s summons', 'normal').

% Found in: FRF
card_name('ojutai, soul of winter', 'Ojutai, Soul of Winter').
card_type('ojutai, soul of winter', 'Legendary Creature — Dragon').
card_types('ojutai, soul of winter', ['Creature']).
card_subtypes('ojutai, soul of winter', ['Dragon']).
card_supertypes('ojutai, soul of winter', ['Legendary']).
card_colors('ojutai, soul of winter', ['W', 'U']).
card_text('ojutai, soul of winter', 'Flying, vigilance\nWhenever a Dragon you control attacks, tap target nonland permanent an opponent controls. That permanent doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('ojutai, soul of winter', ['5', 'W', 'U']).
card_cmc('ojutai, soul of winter', 7).
card_layout('ojutai, soul of winter', 'normal').
card_power('ojutai, soul of winter', 5).
card_toughness('ojutai, soul of winter', 6).

% Found in: BOK, PC2
card_name('okiba-gang shinobi', 'Okiba-Gang Shinobi').
card_type('okiba-gang shinobi', 'Creature — Rat Ninja').
card_types('okiba-gang shinobi', ['Creature']).
card_subtypes('okiba-gang shinobi', ['Rat', 'Ninja']).
card_colors('okiba-gang shinobi', ['B']).
card_text('okiba-gang shinobi', 'Ninjutsu {3}{B} ({3}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Okiba-Gang Shinobi deals combat damage to a player, that player discards two cards.').
card_mana_cost('okiba-gang shinobi', ['3', 'B', 'B']).
card_cmc('okiba-gang shinobi', 5).
card_layout('okiba-gang shinobi', 'normal').
card_power('okiba-gang shinobi', 3).
card_toughness('okiba-gang shinobi', 2).

% Found in: SOK, pARL
card_name('okina nightwatch', 'Okina Nightwatch').
card_type('okina nightwatch', 'Creature — Human Monk').
card_types('okina nightwatch', ['Creature']).
card_subtypes('okina nightwatch', ['Human', 'Monk']).
card_colors('okina nightwatch', ['G']).
card_text('okina nightwatch', 'As long as you have more cards in hand than each opponent, Okina Nightwatch gets +3/+3.').
card_mana_cost('okina nightwatch', ['4', 'G']).
card_cmc('okina nightwatch', 5).
card_layout('okina nightwatch', 'normal').
card_power('okina nightwatch', 4).
card_toughness('okina nightwatch', 3).

% Found in: CHK
card_name('okina, temple to the grandfathers', 'Okina, Temple to the Grandfathers').
card_type('okina, temple to the grandfathers', 'Legendary Land').
card_types('okina, temple to the grandfathers', ['Land']).
card_subtypes('okina, temple to the grandfathers', []).
card_supertypes('okina, temple to the grandfathers', ['Legendary']).
card_colors('okina, temple to the grandfathers', []).
card_text('okina, temple to the grandfathers', '{T}: Add {G} to your mana pool.\n{G}, {T}: Target legendary creature gets +1/+1 until end of turn.').
card_layout('okina, temple to the grandfathers', 'normal').

% Found in: 7ED, 8ED, USG
card_name('okk', 'Okk').
card_type('okk', 'Creature — Goblin').
card_types('okk', ['Creature']).
card_subtypes('okk', ['Goblin']).
card_colors('okk', ['R']).
card_text('okk', 'Okk can\'t attack unless a creature with greater power also attacks.\nOkk can\'t block unless a creature with greater power also blocks.').
card_mana_cost('okk', ['1', 'R']).
card_cmc('okk', 2).
card_layout('okk', 'normal').
card_power('okk', 4).
card_toughness('okk', 4).

% Found in: UNH
card_name('old fogey', 'Old Fogey').
card_type('old fogey', 'Creature — Dinosaur').
card_types('old fogey', ['Creature']).
card_subtypes('old fogey', ['Dinosaur']).
card_colors('old fogey', ['G']).
card_text('old fogey', 'Phasing, cumulative upkeep {1}, echo, fading 3, bands with other Dinosaurs, protection from Homarids, snow-covered plainswalk, flanking, rampage 2').
card_mana_cost('old fogey', ['G', 'G']).
card_cmc('old fogey', 2).
card_layout('old fogey', 'normal').
card_power('old fogey', 7).
card_toughness('old fogey', 7).

% Found in: SHM
card_name('old ghastbark', 'Old Ghastbark').
card_type('old ghastbark', 'Creature — Treefolk Warrior').
card_types('old ghastbark', ['Creature']).
card_subtypes('old ghastbark', ['Treefolk', 'Warrior']).
card_colors('old ghastbark', ['W', 'G']).
card_text('old ghastbark', '').
card_mana_cost('old ghastbark', ['3', 'G/W', 'G/W']).
card_cmc('old ghastbark', 5).
card_layout('old ghastbark', 'normal').
card_power('old ghastbark', 3).
card_toughness('old ghastbark', 6).

% Found in: ARN, ME3
card_name('old man of the sea', 'Old Man of the Sea').
card_type('old man of the sea', 'Creature — Djinn').
card_types('old man of the sea', ['Creature']).
card_subtypes('old man of the sea', ['Djinn']).
card_colors('old man of the sea', ['U']).
card_text('old man of the sea', 'You may choose not to untap Old Man of the Sea during your untap step.\n{T}: Gain control of target creature with power less than or equal to Old Man of the Sea\'s power for as long as Old Man of the Sea remains tapped and that creature\'s power remains less than or equal to Old Man of the Sea\'s power.').
card_mana_cost('old man of the sea', ['1', 'U', 'U']).
card_cmc('old man of the sea', 3).
card_layout('old man of the sea', 'normal').
card_power('old man of the sea', 2).
card_toughness('old man of the sea', 3).
card_reserved('old man of the sea').

% Found in: ISD
card_name('olivia voldaren', 'Olivia Voldaren').
card_type('olivia voldaren', 'Legendary Creature — Vampire').
card_types('olivia voldaren', ['Creature']).
card_subtypes('olivia voldaren', ['Vampire']).
card_supertypes('olivia voldaren', ['Legendary']).
card_colors('olivia voldaren', ['B', 'R']).
card_text('olivia voldaren', 'Flying\n{1}{R}: Olivia Voldaren deals 1 damage to another target creature. That creature becomes a Vampire in addition to its other types. Put a +1/+1 counter on Olivia Voldaren.\n{3}{B}{B}: Gain control of target Vampire for as long as you control Olivia Voldaren.').
card_mana_cost('olivia voldaren', ['2', 'B', 'R']).
card_cmc('olivia voldaren', 4).
card_layout('olivia voldaren', 'normal').
card_power('olivia voldaren', 3).
card_toughness('olivia voldaren', 3).

% Found in: C13, pJGP
card_name('oloro, ageless ascetic', 'Oloro, Ageless Ascetic').
card_type('oloro, ageless ascetic', 'Legendary Creature — Giant Soldier').
card_types('oloro, ageless ascetic', ['Creature']).
card_subtypes('oloro, ageless ascetic', ['Giant', 'Soldier']).
card_supertypes('oloro, ageless ascetic', ['Legendary']).
card_colors('oloro, ageless ascetic', ['W', 'U', 'B']).
card_text('oloro, ageless ascetic', 'At the beginning of your upkeep, you gain 2 life.\nWhenever you gain life, you may pay {1}. If you do, draw a card and each opponent loses 1 life.\nAt the beginning of your upkeep, if Oloro, Ageless Ascetic is in the command zone, you gain 2 life.').
card_mana_cost('oloro, ageless ascetic', ['3', 'W', 'U', 'B']).
card_cmc('oloro, ageless ascetic', 6).
card_layout('oloro, ageless ascetic', 'normal').
card_power('oloro, ageless ascetic', 4).
card_toughness('oloro, ageless ascetic', 5).

% Found in: MRD
card_name('omega myr', 'Omega Myr').
card_type('omega myr', 'Artifact Creature — Myr').
card_types('omega myr', ['Artifact', 'Creature']).
card_subtypes('omega myr', ['Myr']).
card_colors('omega myr', []).
card_text('omega myr', '').
card_mana_cost('omega myr', ['2']).
card_cmc('omega myr', 2).
card_layout('omega myr', 'normal').
card_power('omega myr', 1).
card_toughness('omega myr', 2).

% Found in: POR
card_name('omen', 'Omen').
card_type('omen', 'Sorcery').
card_types('omen', ['Sorcery']).
card_subtypes('omen', []).
card_colors('omen', ['U']).
card_text('omen', 'Look at the top three cards of your library, then put them back in any order. You may shuffle your library.\nDraw a card.').
card_mana_cost('omen', ['1', 'U']).
card_cmc('omen', 2).
card_layout('omen', 'normal').

% Found in: NPH
card_name('omen machine', 'Omen Machine').
card_type('omen machine', 'Artifact').
card_types('omen machine', ['Artifact']).
card_subtypes('omen machine', []).
card_colors('omen machine', []).
card_text('omen machine', 'Players can\'t draw cards.\nAt the beginning of each player\'s draw step, that player exiles the top card of his or her library. If it\'s a land card, the player puts it onto the battlefield. Otherwise, the player casts it without paying its mana cost if able.').
card_mana_cost('omen machine', ['6']).
card_cmc('omen machine', 6).
card_layout('omen machine', 'normal').

% Found in: ALL
card_name('omen of fire', 'Omen of Fire').
card_type('omen of fire', 'Instant').
card_types('omen of fire', ['Instant']).
card_subtypes('omen of fire', []).
card_colors('omen of fire', ['R']).
card_text('omen of fire', 'Return all Islands to their owners\' hands.\nEach player sacrifices a Plains or a white permanent for each white permanent he or she controls.').
card_mana_cost('omen of fire', ['3', 'R', 'R']).
card_cmc('omen of fire', 5).
card_layout('omen of fire', 'normal').
card_reserved('omen of fire').

% Found in: DDO, THS
card_name('omenspeaker', 'Omenspeaker').
card_type('omenspeaker', 'Creature — Human Wizard').
card_types('omenspeaker', ['Creature']).
card_subtypes('omenspeaker', ['Human', 'Wizard']).
card_colors('omenspeaker', ['U']).
card_text('omenspeaker', 'When Omenspeaker enters the battlefield, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('omenspeaker', ['1', 'U']).
card_cmc('omenspeaker', 2).
card_layout('omenspeaker', 'normal').
card_power('omenspeaker', 1).
card_toughness('omenspeaker', 3).

% Found in: V11, WWK
card_name('omnath, locus of mana', 'Omnath, Locus of Mana').
card_type('omnath, locus of mana', 'Legendary Creature — Elemental').
card_types('omnath, locus of mana', ['Creature']).
card_subtypes('omnath, locus of mana', ['Elemental']).
card_supertypes('omnath, locus of mana', ['Legendary']).
card_colors('omnath, locus of mana', ['G']).
card_text('omnath, locus of mana', 'Green mana doesn\'t empty from your mana pool as steps and phases end.\nOmnath, Locus of Mana gets +1/+1 for each green mana in your mana pool.').
card_mana_cost('omnath, locus of mana', ['2', 'G']).
card_cmc('omnath, locus of mana', 3).
card_layout('omnath, locus of mana', 'normal').
card_power('omnath, locus of mana', 1).
card_toughness('omnath, locus of mana', 1).

% Found in: BFZ
card_name('omnath, locus of rage', 'Omnath, Locus of Rage').
card_type('omnath, locus of rage', 'Legendary Creature — Elemental').
card_types('omnath, locus of rage', ['Creature']).
card_subtypes('omnath, locus of rage', ['Elemental']).
card_supertypes('omnath, locus of rage', ['Legendary']).
card_colors('omnath, locus of rage', ['R', 'G']).
card_text('omnath, locus of rage', 'Landfall — Whenever a land enters the battlefield under your control, put a 5/5 red and green Elemental creature token onto the battlefield.\nWhenever Omnath, Locus of Rage or another Elemental you control dies, Omnath deals 3 damage to target creature or player.').
card_mana_cost('omnath, locus of rage', ['3', 'R', 'R', 'G', 'G']).
card_cmc('omnath, locus of rage', 7).
card_layout('omnath, locus of rage', 'normal').
card_power('omnath, locus of rage', 5).
card_toughness('omnath, locus of rage', 5).

% Found in: DIS
card_name('omnibian', 'Omnibian').
card_type('omnibian', 'Creature — Frog').
card_types('omnibian', ['Creature']).
card_subtypes('omnibian', ['Frog']).
card_colors('omnibian', ['U', 'G']).
card_text('omnibian', '{T}: Target creature becomes a Frog with base power and toughness 3/3 until end of turn.').
card_mana_cost('omnibian', ['1', 'G', 'G', 'U']).
card_cmc('omnibian', 4).
card_layout('omnibian', 'normal').
card_power('omnibian', 3).
card_toughness('omnibian', 3).

% Found in: M13
card_name('omniscience', 'Omniscience').
card_type('omniscience', 'Enchantment').
card_types('omniscience', ['Enchantment']).
card_subtypes('omniscience', []).
card_colors('omniscience', ['U']).
card_text('omniscience', 'You may cast nonland cards from your hand without paying their mana costs.').
card_mana_cost('omniscience', ['7', 'U', 'U', 'U']).
card_cmc('omniscience', 10).
card_layout('omniscience', 'normal').

% Found in: PC2
card_name('onakke catacomb', 'Onakke Catacomb').
card_type('onakke catacomb', 'Plane — Shandalar').
card_types('onakke catacomb', ['Plane']).
card_subtypes('onakke catacomb', ['Shandalar']).
card_colors('onakke catacomb', []).
card_text('onakke catacomb', 'All creatures are black and have deathtouch.\nWhenever you roll {C}, creatures you control get +1/+0 and gain first strike until end of turn.').
card_layout('onakke catacomb', 'plane').

% Found in: UGL
card_name('once more with feeling', 'Once More with Feeling').
card_type('once more with feeling', 'Sorcery').
card_types('once more with feeling', ['Sorcery']).
card_subtypes('once more with feeling', []).
card_colors('once more with feeling', ['W']).
card_text('once more with feeling', 'Remove Once More with Feeling from the game as well as all cards in play and in all graveyards. Each player shuffles his or her hand into her or his library, then draws seven cards. Each player\'s life total is set to 10.\nDCI ruling: This card is restricted. (You cannot play with more than one in a deck.)').
card_mana_cost('once more with feeling', ['W', 'W', 'W', 'W']).
card_cmc('once more with feeling', 4).
card_layout('once more with feeling', 'normal').

% Found in: BFZ
card_name('ondu champion', 'Ondu Champion').
card_type('ondu champion', 'Creature — Minotaur Warrior Ally').
card_types('ondu champion', ['Creature']).
card_subtypes('ondu champion', ['Minotaur', 'Warrior', 'Ally']).
card_colors('ondu champion', ['R']).
card_text('ondu champion', 'Rally — Whenever Ondu Champion or another Ally enters the battlefield under your control, creatures you control gain trample until end of turn.').
card_mana_cost('ondu champion', ['2', 'R', 'R']).
card_cmc('ondu champion', 4).
card_layout('ondu champion', 'normal').
card_power('ondu champion', 4).
card_toughness('ondu champion', 3).

% Found in: ZEN
card_name('ondu cleric', 'Ondu Cleric').
card_type('ondu cleric', 'Creature — Kor Cleric Ally').
card_types('ondu cleric', ['Creature']).
card_subtypes('ondu cleric', ['Kor', 'Cleric', 'Ally']).
card_colors('ondu cleric', ['W']).
card_text('ondu cleric', 'Whenever Ondu Cleric or another Ally enters the battlefield under your control, you may gain life equal to the number of Allies you control.').
card_mana_cost('ondu cleric', ['1', 'W']).
card_cmc('ondu cleric', 2).
card_layout('ondu cleric', 'normal').
card_power('ondu cleric', 1).
card_toughness('ondu cleric', 1).

% Found in: DDP, PC2, ROE
card_name('ondu giant', 'Ondu Giant').
card_type('ondu giant', 'Creature — Giant Druid').
card_types('ondu giant', ['Creature']).
card_subtypes('ondu giant', ['Giant', 'Druid']).
card_colors('ondu giant', ['G']).
card_text('ondu giant', 'When Ondu Giant enters the battlefield, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.').
card_mana_cost('ondu giant', ['3', 'G']).
card_cmc('ondu giant', 4).
card_layout('ondu giant', 'normal').
card_power('ondu giant', 2).
card_toughness('ondu giant', 4).

% Found in: BFZ
card_name('ondu greathorn', 'Ondu Greathorn').
card_type('ondu greathorn', 'Creature — Beast').
card_types('ondu greathorn', ['Creature']).
card_subtypes('ondu greathorn', ['Beast']).
card_colors('ondu greathorn', ['W']).
card_text('ondu greathorn', 'First strike\nLandfall — Whenever a land enters the battlefield under your control, Ondu Greathorn gets +2/+2 until end of turn.').
card_mana_cost('ondu greathorn', ['3', 'W']).
card_cmc('ondu greathorn', 4).
card_layout('ondu greathorn', 'normal').
card_power('ondu greathorn', 2).
card_toughness('ondu greathorn', 3).

% Found in: BFZ
card_name('ondu rising', 'Ondu Rising').
card_type('ondu rising', 'Sorcery').
card_types('ondu rising', ['Sorcery']).
card_subtypes('ondu rising', []).
card_colors('ondu rising', ['W']).
card_text('ondu rising', 'Whenever a creature attacks this turn, it gains lifelink until end of turn.\nAwaken 4—{4}{W} (If you cast this spell for {4}{W}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('ondu rising', ['1', 'W']).
card_cmc('ondu rising', 2).
card_layout('ondu rising', 'normal').

% Found in: C13, MRD
card_name('one dozen eyes', 'One Dozen Eyes').
card_type('one dozen eyes', 'Sorcery').
card_types('one dozen eyes', ['Sorcery']).
card_subtypes('one dozen eyes', []).
card_colors('one dozen eyes', ['G']).
card_text('one dozen eyes', 'Choose one —\n• Put a 5/5 green Beast creature token onto the battlefield.\n• Put five 1/1 green Insect creature tokens onto the battlefield.\nEntwine {G}{G}{G} (Choose both if you pay the entwine cost.)').
card_mana_cost('one dozen eyes', ['5', 'G']).
card_cmc('one dozen eyes', 6).
card_layout('one dozen eyes', 'normal').

% Found in: GTC
card_name('one thousand lashes', 'One Thousand Lashes').
card_type('one thousand lashes', 'Enchantment — Aura').
card_types('one thousand lashes', ['Enchantment']).
card_subtypes('one thousand lashes', ['Aura']).
card_colors('one thousand lashes', ['W', 'B']).
card_text('one thousand lashes', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player loses 1 life.').
card_mana_cost('one thousand lashes', ['2', 'W', 'B']).
card_cmc('one thousand lashes', 4).
card_layout('one thousand lashes', 'normal').

% Found in: SCG
card_name('one with nature', 'One with Nature').
card_type('one with nature', 'Enchantment — Aura').
card_types('one with nature', ['Enchantment']).
card_subtypes('one with nature', ['Aura']).
card_colors('one with nature', ['G']).
card_text('one with nature', 'Enchant creature\nWhenever enchanted creature deals combat damage to a player, you may search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library.').
card_mana_cost('one with nature', ['G']).
card_cmc('one with nature', 1).
card_layout('one with nature', 'normal').

% Found in: SOK
card_name('one with nothing', 'One with Nothing').
card_type('one with nothing', 'Instant').
card_types('one with nothing', ['Instant']).
card_subtypes('one with nothing', []).
card_colors('one with nothing', ['B']).
card_text('one with nothing', 'Discard your hand.').
card_mana_cost('one with nothing', ['B']).
card_cmc('one with nothing', 1).
card_layout('one with nothing', 'normal').

% Found in: ISD
card_name('one-eyed scarecrow', 'One-Eyed Scarecrow').
card_type('one-eyed scarecrow', 'Artifact Creature — Scarecrow').
card_types('one-eyed scarecrow', ['Artifact', 'Creature']).
card_subtypes('one-eyed scarecrow', ['Scarecrow']).
card_colors('one-eyed scarecrow', []).
card_text('one-eyed scarecrow', 'Defender\nCreatures with flying your opponents control get -1/-0.').
card_mana_cost('one-eyed scarecrow', ['3']).
card_cmc('one-eyed scarecrow', 3).
card_layout('one-eyed scarecrow', 'normal').
card_power('one-eyed scarecrow', 2).
card_toughness('one-eyed scarecrow', 3).

% Found in: CMD, DDN, SOK
card_name('oni of wild places', 'Oni of Wild Places').
card_type('oni of wild places', 'Creature — Demon Spirit').
card_types('oni of wild places', ['Creature']).
card_subtypes('oni of wild places', ['Demon', 'Spirit']).
card_colors('oni of wild places', ['R']).
card_text('oni of wild places', 'Haste\nAt the beginning of your upkeep, return a red creature you control to its owner\'s hand.').
card_mana_cost('oni of wild places', ['5', 'R']).
card_cmc('oni of wild places', 6).
card_layout('oni of wild places', 'normal').
card_power('oni of wild places', 6).
card_toughness('oni of wild places', 5).

% Found in: VAN
card_name('oni of wild places avatar', 'Oni of Wild Places Avatar').
card_type('oni of wild places avatar', 'Vanguard').
card_types('oni of wild places avatar', ['Vanguard']).
card_subtypes('oni of wild places avatar', []).
card_colors('oni of wild places avatar', []).
card_text('oni of wild places avatar', 'Creatures you control have haste.\nAt the beginning of your upkeep, return a creature you control to its owner\'s hand.').
card_layout('oni of wild places avatar', 'vanguard').

% Found in: CHK, DD3_DVD, DDC
card_name('oni possession', 'Oni Possession').
card_type('oni possession', 'Enchantment — Aura').
card_types('oni possession', ['Enchantment']).
card_subtypes('oni possession', ['Aura']).
card_colors('oni possession', ['B']).
card_text('oni possession', 'Enchant creature\nAt the beginning of your upkeep, sacrifice a creature.\nEnchanted creature gets +3/+3 and has trample.\nEnchanted creature is a Demon Spirit.').
card_mana_cost('oni possession', ['2', 'B']).
card_cmc('oni possession', 3).
card_layout('oni possession', 'normal').

% Found in: ARC
card_name('only blood ends your nightmares', 'Only Blood Ends Your Nightmares').
card_type('only blood ends your nightmares', 'Scheme').
card_types('only blood ends your nightmares', ['Scheme']).
card_subtypes('only blood ends your nightmares', []).
card_colors('only blood ends your nightmares', []).
card_text('only blood ends your nightmares', 'When you set this scheme in motion, each opponent sacrifices a creature. Then each opponent who didn\'t sacrifice a creature discards two cards.').
card_layout('only blood ends your nightmares', 'scheme').

% Found in: EXO
card_name('onslaught', 'Onslaught').
card_type('onslaught', 'Enchantment').
card_types('onslaught', ['Enchantment']).
card_subtypes('onslaught', []).
card_colors('onslaught', ['R']).
card_text('onslaught', 'Whenever you cast a creature spell, tap target creature.').
card_mana_cost('onslaught', ['R']).
card_cmc('onslaught', 1).
card_layout('onslaught', 'normal').

% Found in: 3ED, 4ED, ATQ, ME4, MED
card_name('onulet', 'Onulet').
card_type('onulet', 'Artifact Creature — Construct').
card_types('onulet', ['Artifact', 'Creature']).
card_subtypes('onulet', ['Construct']).
card_colors('onulet', []).
card_text('onulet', 'When Onulet dies, you gain 2 life.').
card_mana_cost('onulet', ['3']).
card_cmc('onulet', 3).
card_layout('onulet', 'normal').
card_power('onulet', 2).
card_toughness('onulet', 2).

% Found in: ALA
card_name('onyx goblet', 'Onyx Goblet').
card_type('onyx goblet', 'Artifact').
card_types('onyx goblet', ['Artifact']).
card_subtypes('onyx goblet', []).
card_colors('onyx goblet', ['B']).
card_text('onyx goblet', '{T}: Target player loses 1 life.').
card_mana_cost('onyx goblet', ['2', 'B']).
card_cmc('onyx goblet', 3).
card_layout('onyx goblet', 'normal').

% Found in: M12
card_name('onyx mage', 'Onyx Mage').
card_type('onyx mage', 'Creature — Human Wizard').
card_types('onyx mage', ['Creature']).
card_subtypes('onyx mage', ['Human', 'Wizard']).
card_colors('onyx mage', ['B']).
card_text('onyx mage', '{1}{B}: Target creature you control gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('onyx mage', ['1', 'B']).
card_cmc('onyx mage', 2).
card_layout('onyx mage', 'normal').
card_power('onyx mage', 2).
card_toughness('onyx mage', 1).

% Found in: ICE
card_name('onyx talisman', 'Onyx Talisman').
card_type('onyx talisman', 'Artifact').
card_types('onyx talisman', ['Artifact']).
card_subtypes('onyx talisman', []).
card_colors('onyx talisman', []).
card_text('onyx talisman', 'Whenever a player casts a black spell, you may pay {3}. If you do, untap target permanent.').
card_mana_cost('onyx talisman', ['2']).
card_cmc('onyx talisman', 2).
card_layout('onyx talisman', 'normal').

% Found in: MOR, pGTW
card_name('oona\'s blackguard', 'Oona\'s Blackguard').
card_type('oona\'s blackguard', 'Creature — Faerie Rogue').
card_types('oona\'s blackguard', ['Creature']).
card_subtypes('oona\'s blackguard', ['Faerie', 'Rogue']).
card_colors('oona\'s blackguard', ['B']).
card_text('oona\'s blackguard', 'Flying\nEach other Rogue creature you control enters the battlefield with an additional +1/+1 counter on it.\nWhenever a creature you control with a +1/+1 counter on it deals combat damage to a player, that player discards a card.').
card_mana_cost('oona\'s blackguard', ['1', 'B']).
card_cmc('oona\'s blackguard', 2).
card_layout('oona\'s blackguard', 'normal').
card_power('oona\'s blackguard', 1).
card_toughness('oona\'s blackguard', 1).

% Found in: SHM
card_name('oona\'s gatewarden', 'Oona\'s Gatewarden').
card_type('oona\'s gatewarden', 'Creature — Faerie Soldier').
card_types('oona\'s gatewarden', ['Creature']).
card_subtypes('oona\'s gatewarden', ['Faerie', 'Soldier']).
card_colors('oona\'s gatewarden', ['U', 'B']).
card_text('oona\'s gatewarden', 'Defender, flying\nWither (This deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('oona\'s gatewarden', ['U/B']).
card_cmc('oona\'s gatewarden', 1).
card_layout('oona\'s gatewarden', 'normal').
card_power('oona\'s gatewarden', 2).
card_toughness('oona\'s gatewarden', 1).

% Found in: EVE
card_name('oona\'s grace', 'Oona\'s Grace').
card_type('oona\'s grace', 'Instant').
card_types('oona\'s grace', ['Instant']).
card_subtypes('oona\'s grace', []).
card_colors('oona\'s grace', ['U']).
card_text('oona\'s grace', 'Target player draws a card.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('oona\'s grace', ['2', 'U']).
card_cmc('oona\'s grace', 3).
card_layout('oona\'s grace', 'normal').

% Found in: LRW
card_name('oona\'s prowler', 'Oona\'s Prowler').
card_type('oona\'s prowler', 'Creature — Faerie Rogue').
card_types('oona\'s prowler', ['Creature']).
card_subtypes('oona\'s prowler', ['Faerie', 'Rogue']).
card_colors('oona\'s prowler', ['B']).
card_text('oona\'s prowler', 'Flying\nDiscard a card: Oona\'s Prowler gets -2/-0 until end of turn. Any player may activate this ability.').
card_mana_cost('oona\'s prowler', ['1', 'B']).
card_cmc('oona\'s prowler', 2).
card_layout('oona\'s prowler', 'normal').
card_power('oona\'s prowler', 3).
card_toughness('oona\'s prowler', 1).

% Found in: MMA, SHM, V11
card_name('oona, queen of the fae', 'Oona, Queen of the Fae').
card_type('oona, queen of the fae', 'Legendary Creature — Faerie Wizard').
card_types('oona, queen of the fae', ['Creature']).
card_subtypes('oona, queen of the fae', ['Faerie', 'Wizard']).
card_supertypes('oona, queen of the fae', ['Legendary']).
card_colors('oona, queen of the fae', ['U', 'B']).
card_text('oona, queen of the fae', 'Flying\n{X}{U/B}: Choose a color. Target opponent exiles the top X cards of his or her library. For each card of the chosen color exiled this way, put a 1/1 blue and black Faerie Rogue creature token with flying onto the battlefield.').
card_mana_cost('oona, queen of the fae', ['3', 'U/B', 'U/B', 'U/B']).
card_cmc('oona, queen of the fae', 6).
card_layout('oona, queen of the fae', 'normal').
card_power('oona, queen of the fae', 5).
card_toughness('oona, queen of the fae', 5).

% Found in: GTC
card_name('ooze flux', 'Ooze Flux').
card_type('ooze flux', 'Enchantment').
card_types('ooze flux', ['Enchantment']).
card_subtypes('ooze flux', []).
card_colors('ooze flux', ['G']).
card_text('ooze flux', '{1}{G}, Remove one or more +1/+1 counters from among creatures you control: Put an X/X green Ooze creature token onto the battlefield, where X is the number of +1/+1 counters removed this way.').
card_mana_cost('ooze flux', ['3', 'G']).
card_cmc('ooze flux', 4).
card_layout('ooze flux', 'normal').

% Found in: ALA
card_name('ooze garden', 'Ooze Garden').
card_type('ooze garden', 'Enchantment').
card_types('ooze garden', ['Enchantment']).
card_subtypes('ooze garden', []).
card_colors('ooze garden', ['G']).
card_text('ooze garden', '{1}{G}, Sacrifice a non-Ooze creature: Put an X/X green Ooze creature token onto the battlefield, where X is the sacrificed creature\'s power. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('ooze garden', ['1', 'G']).
card_cmc('ooze garden', 2).
card_layout('ooze garden', 'normal').

% Found in: USG
card_name('opal acrolith', 'Opal Acrolith').
card_type('opal acrolith', 'Enchantment').
card_types('opal acrolith', ['Enchantment']).
card_subtypes('opal acrolith', []).
card_colors('opal acrolith', ['W']).
card_text('opal acrolith', 'Whenever an opponent casts a creature spell, if Opal Acrolith is an enchantment, Opal Acrolith becomes a 2/4 Soldier creature.\n{0}: Opal Acrolith becomes an enchantment.').
card_mana_cost('opal acrolith', ['2', 'W']).
card_cmc('opal acrolith', 3).
card_layout('opal acrolith', 'normal').

% Found in: USG
card_name('opal archangel', 'Opal Archangel').
card_type('opal archangel', 'Enchantment').
card_types('opal archangel', ['Enchantment']).
card_subtypes('opal archangel', []).
card_colors('opal archangel', ['W']).
card_text('opal archangel', 'When an opponent casts a creature spell, if Opal Archangel is an enchantment, Opal Archangel becomes a 5/5 Angel creature with flying and vigilance.').
card_mana_cost('opal archangel', ['4', 'W']).
card_cmc('opal archangel', 5).
card_layout('opal archangel', 'normal').
card_reserved('opal archangel').

% Found in: ULG
card_name('opal avenger', 'Opal Avenger').
card_type('opal avenger', 'Enchantment').
card_types('opal avenger', ['Enchantment']).
card_subtypes('opal avenger', []).
card_colors('opal avenger', ['W']).
card_text('opal avenger', 'When you have 10 or less life, if Opal Avenger is an enchantment, Opal Avenger becomes a 3/5 Soldier creature.').
card_mana_cost('opal avenger', ['2', 'W']).
card_cmc('opal avenger', 3).
card_layout('opal avenger', 'normal').

% Found in: USG
card_name('opal caryatid', 'Opal Caryatid').
card_type('opal caryatid', 'Enchantment').
card_types('opal caryatid', ['Enchantment']).
card_subtypes('opal caryatid', []).
card_colors('opal caryatid', ['W']).
card_text('opal caryatid', 'When an opponent casts a creature spell, if Opal Caryatid is an enchantment, Opal Caryatid becomes a 2/2 Soldier creature.').
card_mana_cost('opal caryatid', ['W']).
card_cmc('opal caryatid', 1).
card_layout('opal caryatid', 'normal').

% Found in: ULG
card_name('opal champion', 'Opal Champion').
card_type('opal champion', 'Enchantment').
card_types('opal champion', ['Enchantment']).
card_subtypes('opal champion', []).
card_colors('opal champion', ['W']).
card_text('opal champion', 'When an opponent casts a creature spell, if Opal Champion is an enchantment, Opal Champion becomes a 3/3 Knight creature with first strike.').
card_mana_cost('opal champion', ['2', 'W']).
card_cmc('opal champion', 3).
card_layout('opal champion', 'normal').

% Found in: USG
card_name('opal gargoyle', 'Opal Gargoyle').
card_type('opal gargoyle', 'Enchantment').
card_types('opal gargoyle', ['Enchantment']).
card_subtypes('opal gargoyle', []).
card_colors('opal gargoyle', ['W']).
card_text('opal gargoyle', 'When an opponent casts a creature spell, if Opal Gargoyle is an enchantment, Opal Gargoyle becomes a 2/2 Gargoyle creature with flying.').
card_mana_cost('opal gargoyle', ['1', 'W']).
card_cmc('opal gargoyle', 2).
card_layout('opal gargoyle', 'normal').

% Found in: TSP
card_name('opal guardian', 'Opal Guardian').
card_type('opal guardian', 'Enchantment').
card_types('opal guardian', ['Enchantment']).
card_subtypes('opal guardian', []).
card_colors('opal guardian', ['W']).
card_text('opal guardian', 'When an opponent casts a creature spell, if Opal Guardian is an enchantment, Opal Guardian becomes a 3/4 Gargoyle creature with flying and protection from red.').
card_mana_cost('opal guardian', ['W', 'W', 'W']).
card_cmc('opal guardian', 3).
card_layout('opal guardian', 'normal').

% Found in: DGM
card_name('opal lake gatekeepers', 'Opal Lake Gatekeepers').
card_type('opal lake gatekeepers', 'Creature — Vedalken Soldier').
card_types('opal lake gatekeepers', ['Creature']).
card_subtypes('opal lake gatekeepers', ['Vedalken', 'Soldier']).
card_colors('opal lake gatekeepers', ['U']).
card_text('opal lake gatekeepers', 'When Opal Lake Gatekeepers enters the battlefield, if you control two or more Gates, you may draw a card.').
card_mana_cost('opal lake gatekeepers', ['3', 'U']).
card_cmc('opal lake gatekeepers', 4).
card_layout('opal lake gatekeepers', 'normal').
card_power('opal lake gatekeepers', 2).
card_toughness('opal lake gatekeepers', 4).

% Found in: C13
card_name('opal palace', 'Opal Palace').
card_type('opal palace', 'Land').
card_types('opal palace', ['Land']).
card_subtypes('opal palace', []).
card_colors('opal palace', []).
card_text('opal palace', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add to your mana pool one mana of any color in your commander\'s color identity. If you spend this mana to cast your commander, it enters the battlefield with a number of additional +1/+1 counters on it equal to the number of times it\'s been cast from the command zone this game.').
card_layout('opal palace', 'normal').

% Found in: USG
card_name('opal titan', 'Opal Titan').
card_type('opal titan', 'Enchantment').
card_types('opal titan', ['Enchantment']).
card_subtypes('opal titan', []).
card_colors('opal titan', ['W']).
card_text('opal titan', 'When an opponent casts a creature spell, if Opal Titan is an enchantment, Opal Titan becomes a 4/4 Giant creature with protection from each of that spell\'s colors.').
card_mana_cost('opal titan', ['2', 'W', 'W']).
card_cmc('opal titan', 4).
card_layout('opal titan', 'normal').

% Found in: BOK
card_name('opal-eye, konda\'s yojimbo', 'Opal-Eye, Konda\'s Yojimbo').
card_type('opal-eye, konda\'s yojimbo', 'Legendary Creature — Fox Samurai').
card_types('opal-eye, konda\'s yojimbo', ['Creature']).
card_subtypes('opal-eye, konda\'s yojimbo', ['Fox', 'Samurai']).
card_supertypes('opal-eye, konda\'s yojimbo', ['Legendary']).
card_colors('opal-eye, konda\'s yojimbo', ['W']).
card_text('opal-eye, konda\'s yojimbo', 'Defender (This creature can\'t attack.)\nBushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{T}: The next time a source of your choice would deal damage this turn, that damage is dealt to Opal-Eye, Konda\'s Yojimbo instead.\n{1}{W}: Prevent the next 1 damage that would be dealt to Opal-Eye this turn.').
card_mana_cost('opal-eye, konda\'s yojimbo', ['1', 'W', 'W']).
card_cmc('opal-eye, konda\'s yojimbo', 3).
card_layout('opal-eye, konda\'s yojimbo', 'normal').
card_power('opal-eye, konda\'s yojimbo', 1).
card_toughness('opal-eye, konda\'s yojimbo', 4).

% Found in: UDS
card_name('opalescence', 'Opalescence').
card_type('opalescence', 'Enchantment').
card_types('opalescence', ['Enchantment']).
card_subtypes('opalescence', []).
card_colors('opalescence', ['W']).
card_text('opalescence', 'Each other non-Aura enchantment is a creature in addition to its other types and has base power and base toughness each equal to its converted mana cost.').
card_mana_cost('opalescence', ['2', 'W', 'W']).
card_cmc('opalescence', 4).
card_layout('opalescence', 'normal').
card_reserved('opalescence').

% Found in: 5DN
card_name('opaline bracers', 'Opaline Bracers').
card_type('opaline bracers', 'Artifact — Equipment').
card_types('opaline bracers', ['Artifact']).
card_subtypes('opaline bracers', ['Equipment']).
card_colors('opaline bracers', []).
card_text('opaline bracers', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\nEquipped creature gets +X/+X, where X is the number of charge counters on Opaline Bracers.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('opaline bracers', ['4']).
card_cmc('opaline bracers', 4).
card_layout('opaline bracers', 'normal').

% Found in: TSP
card_name('opaline sliver', 'Opaline Sliver').
card_type('opaline sliver', 'Creature — Sliver').
card_types('opaline sliver', ['Creature']).
card_subtypes('opaline sliver', ['Sliver']).
card_colors('opaline sliver', ['W', 'U']).
card_text('opaline sliver', 'All Slivers have \"Whenever this permanent becomes the target of a spell an opponent controls, you may draw a card.\"').
card_mana_cost('opaline sliver', ['1', 'W', 'U']).
card_cmc('opaline sliver', 3).
card_layout('opaline sliver', 'normal').
card_power('opaline sliver', 2).
card_toughness('opaline sliver', 2).

% Found in: THS
card_name('opaline unicorn', 'Opaline Unicorn').
card_type('opaline unicorn', 'Artifact Creature — Unicorn').
card_types('opaline unicorn', ['Artifact', 'Creature']).
card_subtypes('opaline unicorn', ['Unicorn']).
card_colors('opaline unicorn', []).
card_text('opaline unicorn', '{T}: Add one mana of any color to your mana pool.').
card_mana_cost('opaline unicorn', ['3']).
card_cmc('opaline unicorn', 3).
card_layout('opaline unicorn', 'normal').
card_power('opaline unicorn', 1).
card_toughness('opaline unicorn', 2).

% Found in: M10
card_name('open the vaults', 'Open the Vaults').
card_type('open the vaults', 'Sorcery').
card_types('open the vaults', ['Sorcery']).
card_subtypes('open the vaults', []).
card_colors('open the vaults', ['W']).
card_text('open the vaults', 'Return all artifact and enchantment cards from all graveyards to the battlefield under their owners\' control. (Auras with nothing to enchant remain in graveyards.)').
card_mana_cost('open the vaults', ['4', 'W', 'W']).
card_cmc('open the vaults', 6).
card_layout('open the vaults', 'normal').

% Found in: DD2, DD3_JVC, VMA, WTH, pFNM
card_name('ophidian', 'Ophidian').
card_type('ophidian', 'Creature — Snake').
card_types('ophidian', ['Creature']).
card_subtypes('ophidian', ['Snake']).
card_colors('ophidian', ['U']).
card_text('ophidian', 'Whenever Ophidian attacks and isn\'t blocked, you may draw a card. If you do, Ophidian assigns no combat damage this turn.').
card_mana_cost('ophidian', ['2', 'U']).
card_cmc('ophidian', 3).
card_layout('ophidian', 'normal').
card_power('ophidian', 1).
card_toughness('ophidian', 3).

% Found in: TSP
card_name('ophidian eye', 'Ophidian Eye').
card_type('ophidian eye', 'Enchantment — Aura').
card_types('ophidian eye', ['Enchantment']).
card_subtypes('ophidian eye', ['Aura']).
card_colors('ophidian eye', ['U']).
card_text('ophidian eye', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.').
card_mana_cost('ophidian eye', ['2', 'U']).
card_cmc('ophidian eye', 3).
card_layout('ophidian eye', 'normal').

% Found in: C13
card_name('ophiomancer', 'Ophiomancer').
card_type('ophiomancer', 'Creature — Human Shaman').
card_types('ophiomancer', ['Creature']).
card_subtypes('ophiomancer', ['Human', 'Shaman']).
card_colors('ophiomancer', ['B']).
card_text('ophiomancer', 'At the beginning of each upkeep, if you control no Snakes, put a 1/1 black Snake creature token with deathtouch onto the battlefield.').
card_mana_cost('ophiomancer', ['2', 'B']).
card_cmc('ophiomancer', 3).
card_layout('ophiomancer', 'normal').
card_power('ophiomancer', 2).
card_toughness('ophiomancer', 2).

% Found in: TMP
card_name('opportunist', 'Opportunist').
card_type('opportunist', 'Creature — Human Soldier').
card_types('opportunist', ['Creature']).
card_subtypes('opportunist', ['Human', 'Soldier']).
card_colors('opportunist', ['R']).
card_text('opportunist', '{T}: Opportunist deals 1 damage to target creature that was dealt damage this turn.').
card_mana_cost('opportunist', ['2', 'R']).
card_cmc('opportunist', 3).
card_layout('opportunist', 'normal').
card_power('opportunist', 2).
card_toughness('opportunist', 2).

% Found in: 7ED, BRB, C13, M14, ULG
card_name('opportunity', 'Opportunity').
card_type('opportunity', 'Instant').
card_types('opportunity', ['Instant']).
card_subtypes('opportunity', []).
card_colors('opportunity', ['U']).
card_text('opportunity', 'Target player draws four cards.').
card_mana_cost('opportunity', ['4', 'U', 'U']).
card_cmc('opportunity', 6).
card_layout('opportunity', 'normal').

% Found in: 7ED, UDS
card_name('opposition', 'Opposition').
card_type('opposition', 'Enchantment').
card_types('opposition', ['Enchantment']).
card_subtypes('opposition', []).
card_colors('opposition', ['U']).
card_text('opposition', 'Tap an untapped creature you control: Tap target artifact, creature, or land.').
card_mana_cost('opposition', ['2', 'U', 'U']).
card_cmc('opposition', 4).
card_layout('opposition', 'normal').

% Found in: 7ED, USG
card_name('oppression', 'Oppression').
card_type('oppression', 'Enchantment').
card_types('oppression', ['Enchantment']).
card_subtypes('oppression', []).
card_colors('oppression', ['B']).
card_text('oppression', 'Whenever a player casts a spell, that player discards a card.').
card_mana_cost('oppression', ['1', 'B', 'B']).
card_cmc('oppression', 3).
card_layout('oppression', 'normal').

% Found in: JOU, M15
card_name('oppressive rays', 'Oppressive Rays').
card_type('oppressive rays', 'Enchantment — Aura').
card_types('oppressive rays', ['Enchantment']).
card_subtypes('oppressive rays', ['Aura']).
card_colors('oppressive rays', ['W']).
card_text('oppressive rays', 'Enchant creature\nEnchanted creature can\'t attack or block unless its controller pays {3}.\nActivated abilities of enchanted creature cost {3} more to activate.').
card_mana_cost('oppressive rays', ['W']).
card_cmc('oppressive rays', 1).
card_layout('oppressive rays', 'normal').

% Found in: SOK
card_name('oppressive will', 'Oppressive Will').
card_type('oppressive will', 'Instant').
card_types('oppressive will', ['Instant']).
card_subtypes('oppressive will', []).
card_colors('oppressive will', ['U']).
card_text('oppressive will', 'Counter target spell unless its controller pays {1} for each card in your hand.').
card_mana_cost('oppressive will', ['2', 'U']).
card_cmc('oppressive will', 3).
card_layout('oppressive will', 'normal').

% Found in: INV
card_name('opt', 'Opt').
card_type('opt', 'Instant').
card_types('opt', ['Instant']).
card_subtypes('opt', []).
card_colors('opt', ['U']).
card_text('opt', 'Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\nDraw a card.').
card_mana_cost('opt', ['U']).
card_cmc('opt', 1).
card_layout('opt', 'normal').

% Found in: KTK
card_name('opulent palace', 'Opulent Palace').
card_type('opulent palace', 'Land').
card_types('opulent palace', ['Land']).
card_subtypes('opulent palace', []).
card_colors('opulent palace', []).
card_text('opulent palace', 'Opulent Palace enters the battlefield tapped.\n{T}: Add {B}, {G}, or {U} to your mana pool.').
card_layout('opulent palace', 'normal').

% Found in: VAN
card_name('oracle', 'Oracle').
card_type('oracle', 'Vanguard').
card_types('oracle', ['Vanguard']).
card_subtypes('oracle', []).
card_colors('oracle', []).
card_text('oracle', '{0}: Untap target attacking creature you control and remove it from combat.').
card_layout('oracle', 'vanguard').

% Found in: TMP
card_name('oracle en-vec', 'Oracle en-Vec').
card_type('oracle en-vec', 'Creature — Human Wizard').
card_types('oracle en-vec', ['Creature']).
card_subtypes('oracle en-vec', ['Human', 'Wizard']).
card_colors('oracle en-vec', ['W']).
card_text('oracle en-vec', '{T}: Target opponent chooses any number of creatures he or she controls. During that player\'s next turn, the chosen creatures attack if able, and other creatures can\'t attack. At the beginning of that turn\'s end step, destroy each of the chosen creatures that didn\'t attack. Activate this ability only during your turn.').
card_mana_cost('oracle en-vec', ['1', 'W']).
card_cmc('oracle en-vec', 2).
card_layout('oracle en-vec', 'normal').
card_power('oracle en-vec', 1).
card_toughness('oracle en-vec', 1).

% Found in: BNG
card_name('oracle of bones', 'Oracle of Bones').
card_type('oracle of bones', 'Creature — Minotaur Shaman').
card_types('oracle of bones', ['Creature']).
card_subtypes('oracle of bones', ['Minotaur', 'Shaman']).
card_colors('oracle of bones', ['R']).
card_text('oracle of bones', 'Haste\nTribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Oracle of Bones enters the battlefield, if tribute wasn\'t paid, you may cast an instant or sorcery card from your hand without paying its mana cost.').
card_mana_cost('oracle of bones', ['2', 'R', 'R']).
card_cmc('oracle of bones', 4).
card_layout('oracle of bones', 'normal').
card_power('oracle of bones', 3).
card_toughness('oracle of bones', 1).

% Found in: BFZ
card_name('oracle of dust', 'Oracle of Dust').
card_type('oracle of dust', 'Creature — Eldrazi Processor').
card_types('oracle of dust', ['Creature']).
card_subtypes('oracle of dust', ['Eldrazi', 'Processor']).
card_colors('oracle of dust', []).
card_text('oracle of dust', 'Devoid (This card has no color.)\n{2}, Put a card an opponent owns from exile into that player\'s graveyard: Draw a card, then discard a card.').
card_mana_cost('oracle of dust', ['4', 'U']).
card_cmc('oracle of dust', 5).
card_layout('oracle of dust', 'normal').
card_power('oracle of dust', 3).
card_toughness('oracle of dust', 5).

% Found in: ZEN
card_name('oracle of mul daya', 'Oracle of Mul Daya').
card_type('oracle of mul daya', 'Creature — Elf Shaman').
card_types('oracle of mul daya', ['Creature']).
card_subtypes('oracle of mul daya', ['Elf', 'Shaman']).
card_colors('oracle of mul daya', ['G']).
card_text('oracle of mul daya', 'You may play an additional land on each of your turns.\nPlay with the top card of your library revealed.\nYou may play the top card of your library if it\'s a land card.').
card_mana_cost('oracle of mul daya', ['3', 'G']).
card_cmc('oracle of mul daya', 4).
card_layout('oracle of mul daya', 'normal').
card_power('oracle of mul daya', 2).
card_toughness('oracle of mul daya', 2).

% Found in: SHM
card_name('oracle of nectars', 'Oracle of Nectars').
card_type('oracle of nectars', 'Creature — Elf Cleric').
card_types('oracle of nectars', ['Creature']).
card_subtypes('oracle of nectars', ['Elf', 'Cleric']).
card_colors('oracle of nectars', ['W', 'G']).
card_text('oracle of nectars', '{X}, {T}: You gain X life.').
card_mana_cost('oracle of nectars', ['2', 'G/W']).
card_cmc('oracle of nectars', 3).
card_layout('oracle of nectars', 'normal').
card_power('oracle of nectars', 2).
card_toughness('oracle of nectars', 2).

% Found in: 8ED, 9ED, NMS
card_name('oracle\'s attendants', 'Oracle\'s Attendants').
card_type('oracle\'s attendants', 'Creature — Human Soldier').
card_types('oracle\'s attendants', ['Creature']).
card_subtypes('oracle\'s attendants', ['Human', 'Soldier']).
card_colors('oracle\'s attendants', ['W']).
card_text('oracle\'s attendants', '{T}: All damage that would be dealt to target creature this turn by a source of your choice is dealt to Oracle\'s Attendants instead.').
card_mana_cost('oracle\'s attendants', ['3', 'W']).
card_cmc('oracle\'s attendants', 4).
card_layout('oracle\'s attendants', 'normal').
card_power('oracle\'s attendants', 1).
card_toughness('oracle\'s attendants', 5).

% Found in: BNG
card_name('oracle\'s insight', 'Oracle\'s Insight').
card_type('oracle\'s insight', 'Enchantment — Aura').
card_types('oracle\'s insight', ['Enchantment']).
card_subtypes('oracle\'s insight', ['Aura']).
card_colors('oracle\'s insight', ['U']).
card_text('oracle\'s insight', 'Enchant creature\nEnchanted creature has \"{T}: Scry 1, then draw a card.\" (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_mana_cost('oracle\'s insight', ['3', 'U']).
card_cmc('oracle\'s insight', 4).
card_layout('oracle\'s insight', 'normal').

% Found in: BFZ
card_name('oran-rief hydra', 'Oran-Rief Hydra').
card_type('oran-rief hydra', 'Creature — Hydra').
card_types('oran-rief hydra', ['Creature']).
card_subtypes('oran-rief hydra', ['Hydra']).
card_colors('oran-rief hydra', ['G']).
card_text('oran-rief hydra', 'Trample\nLandfall — Whenever a land enters the battlefield under your control, put a +1/+1 counter on Oran-Rief Hydra. If that land is a Forest, put two +1/+1 counters on Oran-Rief Hydra instead.').
card_mana_cost('oran-rief hydra', ['4', 'G', 'G']).
card_cmc('oran-rief hydra', 6).
card_layout('oran-rief hydra', 'normal').
card_power('oran-rief hydra', 5).
card_toughness('oran-rief hydra', 5).

% Found in: BFZ
card_name('oran-rief invoker', 'Oran-Rief Invoker').
card_type('oran-rief invoker', 'Creature — Human Shaman').
card_types('oran-rief invoker', ['Creature']).
card_subtypes('oran-rief invoker', ['Human', 'Shaman']).
card_colors('oran-rief invoker', ['G']).
card_text('oran-rief invoker', '{8}: Oran-Rief Invoker gets +5/+5 and gains trample until end of turn.').
card_mana_cost('oran-rief invoker', ['1', 'G']).
card_cmc('oran-rief invoker', 2).
card_layout('oran-rief invoker', 'normal').
card_power('oran-rief invoker', 2).
card_toughness('oran-rief invoker', 2).

% Found in: DDM, ZEN
card_name('oran-rief recluse', 'Oran-Rief Recluse').
card_type('oran-rief recluse', 'Creature — Spider').
card_types('oran-rief recluse', ['Creature']).
card_subtypes('oran-rief recluse', ['Spider']).
card_colors('oran-rief recluse', ['G']).
card_text('oran-rief recluse', 'Kicker {2}{G} (You may pay an additional {2}{G} as you cast this spell.)\nReach (This creature can block creatures with flying.)\nWhen Oran-Rief Recluse enters the battlefield, if it was kicked, destroy target creature with flying.').
card_mana_cost('oran-rief recluse', ['2', 'G']).
card_cmc('oran-rief recluse', 3).
card_layout('oran-rief recluse', 'normal').
card_power('oran-rief recluse', 1).
card_toughness('oran-rief recluse', 3).

% Found in: ZEN
card_name('oran-rief survivalist', 'Oran-Rief Survivalist').
card_type('oran-rief survivalist', 'Creature — Human Warrior Ally').
card_types('oran-rief survivalist', ['Creature']).
card_subtypes('oran-rief survivalist', ['Human', 'Warrior', 'Ally']).
card_colors('oran-rief survivalist', ['G']).
card_text('oran-rief survivalist', 'Whenever Oran-Rief Survivalist or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Oran-Rief Survivalist.').
card_mana_cost('oran-rief survivalist', ['1', 'G']).
card_cmc('oran-rief survivalist', 2).
card_layout('oran-rief survivalist', 'normal').
card_power('oran-rief survivalist', 1).
card_toughness('oran-rief survivalist', 1).

% Found in: C14, ZEN
card_name('oran-rief, the vastwood', 'Oran-Rief, the Vastwood').
card_type('oran-rief, the vastwood', 'Land').
card_types('oran-rief, the vastwood', ['Land']).
card_subtypes('oran-rief, the vastwood', []).
card_colors('oran-rief, the vastwood', []).
card_text('oran-rief, the vastwood', 'Oran-Rief, the Vastwood enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{T}: Put a +1/+1 counter on each green creature that entered the battlefield this turn.').
card_layout('oran-rief, the vastwood', 'normal').

% Found in: DTK
card_name('orator of ojutai', 'Orator of Ojutai').
card_type('orator of ojutai', 'Creature — Bird Monk').
card_types('orator of ojutai', ['Creature']).
card_subtypes('orator of ojutai', ['Bird', 'Monk']).
card_colors('orator of ojutai', ['W']).
card_text('orator of ojutai', 'As an additional cost to cast Orator of Ojutai, you may reveal a Dragon card from your hand.\nDefender, flying\nWhen Orator of Ojutai enters the battlefield, if you revealed a Dragon card or controlled a Dragon as you cast Orator of Ojutai, draw a card.').
card_mana_cost('orator of ojutai', ['1', 'W']).
card_cmc('orator of ojutai', 2).
card_layout('orator of ojutai', 'normal').
card_power('orator of ojutai', 0).
card_toughness('orator of ojutai', 4).

% Found in: NMS
card_name('oraxid', 'Oraxid').
card_type('oraxid', 'Creature — Crab Beast').
card_types('oraxid', ['Creature']).
card_subtypes('oraxid', ['Crab', 'Beast']).
card_colors('oraxid', ['U']).
card_text('oraxid', 'Protection from red').
card_mana_cost('oraxid', ['3', 'U']).
card_cmc('oraxid', 4).
card_layout('oraxid', 'normal').
card_power('oraxid', 2).
card_toughness('oraxid', 3).

% Found in: BOK
card_name('orb of dreams', 'Orb of Dreams').
card_type('orb of dreams', 'Artifact').
card_types('orb of dreams', ['Artifact']).
card_subtypes('orb of dreams', []).
card_colors('orb of dreams', []).
card_text('orb of dreams', 'Permanents enter the battlefield tapped.').
card_mana_cost('orb of dreams', ['3']).
card_cmc('orb of dreams', 3).
card_layout('orb of dreams', 'normal').

% Found in: ORI
card_name('orbs of warding', 'Orbs of Warding').
card_type('orbs of warding', 'Artifact').
card_types('orbs of warding', ['Artifact']).
card_subtypes('orbs of warding', []).
card_colors('orbs of warding', []).
card_text('orbs of warding', 'You have hexproof. (You can\'t be the target of spells or abilities your opponents control.)\nIf a creature would deal damage to you, prevent 1 of that damage.').
card_mana_cost('orbs of warding', ['5']).
card_cmc('orbs of warding', 5).
card_layout('orbs of warding', 'normal').

% Found in: CHK
card_name('orbweaver kumo', 'Orbweaver Kumo').
card_type('orbweaver kumo', 'Creature — Spirit').
card_types('orbweaver kumo', ['Creature']).
card_subtypes('orbweaver kumo', ['Spirit']).
card_colors('orbweaver kumo', ['G']).
card_text('orbweaver kumo', 'Reach (This creature can block creatures with flying.)\nWhenever you cast a Spirit or Arcane spell, Orbweaver Kumo gains forestwalk until end of turn. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('orbweaver kumo', ['4', 'G', 'G']).
card_cmc('orbweaver kumo', 6).
card_layout('orbweaver kumo', 'normal').
card_power('orbweaver kumo', 3).
card_toughness('orbweaver kumo', 4).

% Found in: DRK, ME2
card_name('orc general', 'Orc General').
card_type('orc general', 'Creature — Orc Warrior').
card_types('orc general', ['Creature']).
card_subtypes('orc general', ['Orc', 'Warrior']).
card_colors('orc general', ['R']).
card_text('orc general', '{T}, Sacrifice another Orc or Goblin: Other Orc creatures get +1/+1 until end of turn.').
card_mana_cost('orc general', ['2', 'R']).
card_cmc('orc general', 3).
card_layout('orc general', 'normal').
card_power('orc general', 2).
card_toughness('orc general', 2).

% Found in: FRF
card_name('orc sureshot', 'Orc Sureshot').
card_type('orc sureshot', 'Creature — Orc Archer').
card_types('orc sureshot', ['Creature']).
card_subtypes('orc sureshot', ['Orc', 'Archer']).
card_colors('orc sureshot', ['B']).
card_text('orc sureshot', 'Whenever another creature enters the battlefield under your control, target creature an opponent controls gets -1/-1 until end of turn.').
card_mana_cost('orc sureshot', ['3', 'B']).
card_cmc('orc sureshot', 4).
card_layout('orc sureshot', 'normal').
card_power('orc sureshot', 4).
card_toughness('orc sureshot', 2).

% Found in: ISD, ORI
card_name('orchard spirit', 'Orchard Spirit').
card_type('orchard spirit', 'Creature — Spirit').
card_types('orchard spirit', ['Creature']).
card_subtypes('orchard spirit', ['Spirit']).
card_colors('orchard spirit', ['G']).
card_text('orchard spirit', 'Orchard Spirit can\'t be blocked except by creatures with flying or reach.').
card_mana_cost('orchard spirit', ['2', 'G']).
card_cmc('orchard spirit', 3).
card_layout('orchard spirit', 'normal').
card_power('orchard spirit', 2).
card_toughness('orchard spirit', 2).

% Found in: MOR
card_name('orchard warden', 'Orchard Warden').
card_type('orchard warden', 'Creature — Treefolk Shaman').
card_types('orchard warden', ['Creature']).
card_subtypes('orchard warden', ['Treefolk', 'Shaman']).
card_colors('orchard warden', ['G']).
card_text('orchard warden', 'Whenever another Treefolk creature enters the battlefield under your control, you may gain life equal to that creature\'s toughness.').
card_mana_cost('orchard warden', ['4', 'G', 'G']).
card_cmc('orchard warden', 6).
card_layout('orchard warden', 'normal').
card_power('orchard warden', 4).
card_toughness('orchard warden', 6).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ITP, LEA, LEB, RQS
card_name('orcish artillery', 'Orcish Artillery').
card_type('orcish artillery', 'Creature — Orc Warrior').
card_types('orcish artillery', ['Creature']).
card_subtypes('orcish artillery', ['Orc', 'Warrior']).
card_colors('orcish artillery', ['R']).
card_text('orcish artillery', '{T}: Orcish Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_mana_cost('orcish artillery', ['1', 'R', 'R']).
card_cmc('orcish artillery', 3).
card_layout('orcish artillery', 'normal').
card_power('orcish artillery', 1).
card_toughness('orcish artillery', 3).

% Found in: CSP
card_name('orcish bloodpainter', 'Orcish Bloodpainter').
card_type('orcish bloodpainter', 'Creature — Orc Shaman').
card_types('orcish bloodpainter', ['Creature']).
card_subtypes('orcish bloodpainter', ['Orc', 'Shaman']).
card_colors('orcish bloodpainter', ['R']).
card_text('orcish bloodpainter', '{T}, Sacrifice a creature: Orcish Bloodpainter deals 1 damage to target creature or player.').
card_mana_cost('orcish bloodpainter', ['2', 'R']).
card_cmc('orcish bloodpainter', 3).
card_layout('orcish bloodpainter', 'normal').
card_power('orcish bloodpainter', 2).
card_toughness('orcish bloodpainter', 1).

% Found in: CNS, DDN, TSP
card_name('orcish cannonade', 'Orcish Cannonade').
card_type('orcish cannonade', 'Instant').
card_types('orcish cannonade', ['Instant']).
card_subtypes('orcish cannonade', []).
card_colors('orcish cannonade', ['R']).
card_text('orcish cannonade', 'Orcish Cannonade deals 2 damage to target creature or player and 3 damage to you.\nDraw a card.').
card_mana_cost('orcish cannonade', ['1', 'R', 'R']).
card_cmc('orcish cannonade', 3).
card_layout('orcish cannonade', 'normal').

% Found in: DKM, ICE, ME2
card_name('orcish cannoneers', 'Orcish Cannoneers').
card_type('orcish cannoneers', 'Creature — Orc Warrior').
card_types('orcish cannoneers', ['Creature']).
card_subtypes('orcish cannoneers', ['Orc', 'Warrior']).
card_colors('orcish cannoneers', ['R']).
card_text('orcish cannoneers', '{T}: Orcish Cannoneers deals 2 damage to target creature or player and 3 damage to you.').
card_mana_cost('orcish cannoneers', ['1', 'R', 'R']).
card_cmc('orcish cannoneers', 3).
card_layout('orcish cannoneers', 'normal').
card_power('orcish cannoneers', 1).
card_toughness('orcish cannoneers', 3).

% Found in: 5ED, FEM, ME2
card_name('orcish captain', 'Orcish Captain').
card_type('orcish captain', 'Creature — Orc Warrior').
card_types('orcish captain', ['Creature']).
card_subtypes('orcish captain', ['Orc', 'Warrior']).
card_colors('orcish captain', ['R']).
card_text('orcish captain', '{1}: Flip a coin. If you win the flip, target Orc creature gets +2/+0 until end of turn. If you lose the flip, it gets -0/-2 until end of turn.').
card_mana_cost('orcish captain', ['R']).
card_cmc('orcish captain', 1).
card_layout('orcish captain', 'normal').
card_power('orcish captain', 1).
card_toughness('orcish captain', 1).

% Found in: 5ED, ICE, ME2
card_name('orcish conscripts', 'Orcish Conscripts').
card_type('orcish conscripts', 'Creature — Orc').
card_types('orcish conscripts', ['Creature']).
card_subtypes('orcish conscripts', ['Orc']).
card_colors('orcish conscripts', ['R']).
card_text('orcish conscripts', 'Orcish Conscripts can\'t attack unless at least two other creatures attack.\nOrcish Conscripts can\'t block unless at least two other creatures block.').
card_mana_cost('orcish conscripts', ['R']).
card_cmc('orcish conscripts', 1).
card_layout('orcish conscripts', 'normal').
card_power('orcish conscripts', 2).
card_toughness('orcish conscripts', 2).

% Found in: 5ED, ICE, ME2
card_name('orcish farmer', 'Orcish Farmer').
card_type('orcish farmer', 'Creature — Orc').
card_types('orcish farmer', ['Creature']).
card_subtypes('orcish farmer', ['Orc']).
card_colors('orcish farmer', ['R']).
card_text('orcish farmer', '{T}: Target land becomes a Swamp until its controller\'s next untap step.').
card_mana_cost('orcish farmer', ['1', 'R', 'R']).
card_cmc('orcish farmer', 3).
card_layout('orcish farmer', 'normal').
card_power('orcish farmer', 2).
card_toughness('orcish farmer', 2).

% Found in: CST, ICE
card_name('orcish healer', 'Orcish Healer').
card_type('orcish healer', 'Creature — Orc Cleric').
card_types('orcish healer', ['Creature']).
card_subtypes('orcish healer', ['Orc', 'Cleric']).
card_colors('orcish healer', ['R']).
card_text('orcish healer', '{R}{R}, {T}: Target creature can\'t be regenerated this turn.\n{B}{B}{R}, {T}: Regenerate target black or green creature.\n{R}{G}{G}, {T}: Regenerate target black or green creature.').
card_mana_cost('orcish healer', ['R', 'R']).
card_cmc('orcish healer', 2).
card_layout('orcish healer', 'normal').
card_power('orcish healer', 1).
card_toughness('orcish healer', 1).

% Found in: ICE, TSB
card_name('orcish librarian', 'Orcish Librarian').
card_type('orcish librarian', 'Creature — Orc').
card_types('orcish librarian', ['Creature']).
card_subtypes('orcish librarian', ['Orc']).
card_colors('orcish librarian', ['R']).
card_text('orcish librarian', '{R}, {T}: Look at the top eight cards of your library. Exile four of them at random, then put the rest on top of your library in any order.').
card_mana_cost('orcish librarian', ['1', 'R']).
card_cmc('orcish librarian', 2).
card_layout('orcish librarian', 'normal').
card_power('orcish librarian', 1).
card_toughness('orcish librarian', 1).

% Found in: CST, DDL, ICE, ME2, VMA
card_name('orcish lumberjack', 'Orcish Lumberjack').
card_type('orcish lumberjack', 'Creature — Orc').
card_types('orcish lumberjack', ['Creature']).
card_subtypes('orcish lumberjack', ['Orc']).
card_colors('orcish lumberjack', ['R']).
card_text('orcish lumberjack', '{T}, Sacrifice a Forest: Add three mana in any combination of {R} and/or {G} to your mana pool.').
card_mana_cost('orcish lumberjack', ['R']).
card_cmc('orcish lumberjack', 1).
card_layout('orcish lumberjack', 'normal').
card_power('orcish lumberjack', 1).
card_toughness('orcish lumberjack', 1).

% Found in: ATQ, ME4, MED
card_name('orcish mechanics', 'Orcish Mechanics').
card_type('orcish mechanics', 'Creature — Orc').
card_types('orcish mechanics', ['Creature']).
card_subtypes('orcish mechanics', ['Orc']).
card_colors('orcish mechanics', ['R']).
card_text('orcish mechanics', '{T}, Sacrifice an artifact: Orcish Mechanics deals 2 damage to target creature or player.').
card_mana_cost('orcish mechanics', ['2', 'R']).
card_cmc('orcish mechanics', 3).
card_layout('orcish mechanics', 'normal').
card_power('orcish mechanics', 1).
card_toughness('orcish mechanics', 1).

% Found in: HML
card_name('orcish mine', 'Orcish Mine').
card_type('orcish mine', 'Enchantment — Aura').
card_types('orcish mine', ['Enchantment']).
card_subtypes('orcish mine', ['Aura']).
card_colors('orcish mine', ['R']).
card_text('orcish mine', 'Enchant land\nOrcish Mine enters the battlefield with three ore counters on it.\nAt the beginning of your upkeep or whenever enchanted land becomes tapped, remove an ore counter from Orcish Mine.\nWhen the last ore counter is removed from Orcish Mine, destroy enchanted land and Orcish Mine deals 2 damage to that land\'s controller.').
card_mana_cost('orcish mine', ['1', 'R', 'R']).
card_cmc('orcish mine', 3).
card_layout('orcish mine', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, ITP, LEA, LEB, RQS, S00
card_name('orcish oriflamme', 'Orcish Oriflamme').
card_type('orcish oriflamme', 'Enchantment').
card_types('orcish oriflamme', ['Enchantment']).
card_subtypes('orcish oriflamme', []).
card_colors('orcish oriflamme', ['R']).
card_text('orcish oriflamme', 'Attacking creatures you control get +1/+0.').
card_mana_cost('orcish oriflamme', ['3', 'R']).
card_cmc('orcish oriflamme', 4).
card_layout('orcish oriflamme', 'normal').

% Found in: UNH
card_name('orcish paratroopers', 'Orcish Paratroopers').
card_type('orcish paratroopers', 'Creature — Orc Paratrooper').
card_types('orcish paratroopers', ['Creature']).
card_subtypes('orcish paratroopers', ['Orc', 'Paratrooper']).
card_colors('orcish paratroopers', ['R']).
card_text('orcish paratroopers', 'When Orcish Paratroopers comes into play, flip it from a height of at least one foot. Sacrifice Orcish Paratroopers unless it lands face up after turning over completely.').
card_mana_cost('orcish paratroopers', ['2', 'R']).
card_cmc('orcish paratroopers', 3).
card_layout('orcish paratroopers', 'normal').
card_power('orcish paratroopers', 4).
card_toughness('orcish paratroopers', 4).

% Found in: WTH
card_name('orcish settlers', 'Orcish Settlers').
card_type('orcish settlers', 'Creature — Orc').
card_types('orcish settlers', ['Creature']).
card_subtypes('orcish settlers', ['Orc']).
card_colors('orcish settlers', ['R']).
card_text('orcish settlers', '{X}{X}{R}, {T}, Sacrifice Orcish Settlers: Destroy X target lands.').
card_mana_cost('orcish settlers', ['1', 'R']).
card_cmc('orcish settlers', 2).
card_layout('orcish settlers', 'normal').
card_power('orcish settlers', 1).
card_toughness('orcish settlers', 1).

% Found in: 8ED, FEM
card_name('orcish spy', 'Orcish Spy').
card_type('orcish spy', 'Creature — Orc Rogue').
card_types('orcish spy', ['Creature']).
card_subtypes('orcish spy', ['Orc', 'Rogue']).
card_colors('orcish spy', ['R']).
card_text('orcish spy', '{T}: Look at the top three cards of target player\'s library.').
card_mana_cost('orcish spy', ['R']).
card_cmc('orcish spy', 1).
card_layout('orcish spy', 'normal').
card_power('orcish spy', 1).
card_toughness('orcish spy', 1).

% Found in: 5ED, ICE, ME2
card_name('orcish squatters', 'Orcish Squatters').
card_type('orcish squatters', 'Creature — Orc').
card_types('orcish squatters', ['Creature']).
card_subtypes('orcish squatters', ['Orc']).
card_colors('orcish squatters', ['R']).
card_text('orcish squatters', 'Whenever Orcish Squatters attacks and isn\'t blocked, you may gain control of target land defending player controls for as long as you control Orcish Squatters. If you do, Orcish Squatters assigns no combat damage this turn.').
card_mana_cost('orcish squatters', ['4', 'R']).
card_cmc('orcish squatters', 5).
card_layout('orcish squatters', 'normal').
card_power('orcish squatters', 2).
card_toughness('orcish squatters', 3).

% Found in: VAN
card_name('orcish squatters avatar', 'Orcish Squatters Avatar').
card_type('orcish squatters avatar', 'Vanguard').
card_types('orcish squatters avatar', ['Vanguard']).
card_subtypes('orcish squatters avatar', []).
card_colors('orcish squatters avatar', []).
card_text('orcish squatters avatar', 'At the beginning of your precombat main phase, add {X} to your mana pool, where X is the number of lands target opponent controls.').
card_layout('orcish squatters avatar', 'vanguard').

% Found in: FEM, ME2
card_name('orcish veteran', 'Orcish Veteran').
card_type('orcish veteran', 'Creature — Orc').
card_types('orcish veteran', ['Creature']).
card_subtypes('orcish veteran', ['Orc']).
card_colors('orcish veteran', ['R']).
card_text('orcish veteran', 'Orcish Veteran can\'t block white creatures with power 2 or greater.\n{R}: Orcish Veteran gains first strike until end of turn.').
card_mana_cost('orcish veteran', ['2', 'R']).
card_cmc('orcish veteran', 3).
card_layout('orcish veteran', 'normal').
card_power('orcish veteran', 2).
card_toughness('orcish veteran', 2).

% Found in: THS
card_name('ordeal of erebos', 'Ordeal of Erebos').
card_type('ordeal of erebos', 'Enchantment — Aura').
card_types('ordeal of erebos', ['Enchantment']).
card_subtypes('ordeal of erebos', ['Aura']).
card_colors('ordeal of erebos', ['B']).
card_text('ordeal of erebos', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Erebos.\nWhen you sacrifice Ordeal of Erebos, target player discards two cards.').
card_mana_cost('ordeal of erebos', ['1', 'B']).
card_cmc('ordeal of erebos', 2).
card_layout('ordeal of erebos', 'normal').

% Found in: THS
card_name('ordeal of heliod', 'Ordeal of Heliod').
card_type('ordeal of heliod', 'Enchantment — Aura').
card_types('ordeal of heliod', ['Enchantment']).
card_subtypes('ordeal of heliod', ['Aura']).
card_colors('ordeal of heliod', ['W']).
card_text('ordeal of heliod', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Heliod.\nWhen you sacrifice Ordeal of Heliod, you gain 10 life.').
card_mana_cost('ordeal of heliod', ['1', 'W']).
card_cmc('ordeal of heliod', 2).
card_layout('ordeal of heliod', 'normal').

% Found in: THS
card_name('ordeal of nylea', 'Ordeal of Nylea').
card_type('ordeal of nylea', 'Enchantment — Aura').
card_types('ordeal of nylea', ['Enchantment']).
card_subtypes('ordeal of nylea', ['Aura']).
card_colors('ordeal of nylea', ['G']).
card_text('ordeal of nylea', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Nylea.\nWhen you sacrifice Ordeal of Nylea, search your library for up to two basic land cards, put them onto the battlefield tapped, then shuffle your library.').
card_mana_cost('ordeal of nylea', ['1', 'G']).
card_cmc('ordeal of nylea', 2).
card_layout('ordeal of nylea', 'normal').

% Found in: DDL, THS
card_name('ordeal of purphoros', 'Ordeal of Purphoros').
card_type('ordeal of purphoros', 'Enchantment — Aura').
card_types('ordeal of purphoros', ['Enchantment']).
card_subtypes('ordeal of purphoros', ['Aura']).
card_colors('ordeal of purphoros', ['R']).
card_text('ordeal of purphoros', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Purphoros.\nWhen you sacrifice Ordeal of Purphoros, it deals 3 damage to target creature or player.').
card_mana_cost('ordeal of purphoros', ['1', 'R']).
card_cmc('ordeal of purphoros', 2).
card_layout('ordeal of purphoros', 'normal').

% Found in: THS
card_name('ordeal of thassa', 'Ordeal of Thassa').
card_type('ordeal of thassa', 'Enchantment — Aura').
card_types('ordeal of thassa', ['Enchantment']).
card_subtypes('ordeal of thassa', ['Aura']).
card_colors('ordeal of thassa', ['U']).
card_text('ordeal of thassa', 'Enchant creature\nWhenever enchanted creature attacks, put a +1/+1 counter on it. Then if it has three or more +1/+1 counters on it, sacrifice Ordeal of Thassa.\nWhen you sacrifice Ordeal of Thassa, draw two cards.').
card_mana_cost('ordeal of thassa', ['1', 'U']).
card_cmc('ordeal of thassa', 2).
card_layout('ordeal of thassa', 'normal').

% Found in: APC, HOP
card_name('order', 'Order').
card_type('order', 'Instant').
card_types('order', ['Instant']).
card_subtypes('order', []).
card_colors('order', ['W']).
card_text('order', 'Exile target attacking creature.').
card_mana_cost('order', ['3', 'W']).
card_cmc('order', 4).
card_layout('order', 'split').
card_sides('order', 'chaos').

% Found in: FEM, MED
card_name('order of leitbur', 'Order of Leitbur').
card_type('order of leitbur', 'Creature — Human Cleric Knight').
card_types('order of leitbur', ['Creature']).
card_subtypes('order of leitbur', ['Human', 'Cleric', 'Knight']).
card_colors('order of leitbur', ['W']).
card_text('order of leitbur', 'Protection from black\n{W}: Order of Leitbur gains first strike until end of turn.\n{W}{W}: Order of Leitbur gets +1/+0 until end of turn.').
card_mana_cost('order of leitbur', ['W', 'W']).
card_cmc('order of leitbur', 2).
card_layout('order of leitbur', 'normal').
card_power('order of leitbur', 2).
card_toughness('order of leitbur', 1).

% Found in: C13
card_name('order of succession', 'Order of Succession').
card_type('order of succession', 'Sorcery').
card_types('order of succession', ['Sorcery']).
card_subtypes('order of succession', []).
card_colors('order of succession', ['U']).
card_text('order of succession', 'Choose left or right. Starting with you and proceeding in the chosen direction, each player chooses a creature controlled by the next player in that direction. Each player gains control of the creature he or she chose.').
card_mana_cost('order of succession', ['3', 'U']).
card_cmc('order of succession', 4).
card_layout('order of succession', 'normal').

% Found in: FEM, MED
card_name('order of the ebon hand', 'Order of the Ebon Hand').
card_type('order of the ebon hand', 'Creature — Cleric Knight').
card_types('order of the ebon hand', ['Creature']).
card_subtypes('order of the ebon hand', ['Cleric', 'Knight']).
card_colors('order of the ebon hand', ['B']).
card_text('order of the ebon hand', 'Protection from white\n{B}: Order of the Ebon Hand gains first strike until end of turn.\n{B}{B}: Order of the Ebon Hand gets +1/+0 until end of turn.').
card_mana_cost('order of the ebon hand', ['B', 'B']).
card_cmc('order of the ebon hand', 2).
card_layout('order of the ebon hand', 'normal').
card_power('order of the ebon hand', 2).
card_toughness('order of the ebon hand', 1).

% Found in: MOR
card_name('order of the golden cricket', 'Order of the Golden Cricket').
card_type('order of the golden cricket', 'Creature — Kithkin Knight').
card_types('order of the golden cricket', ['Creature']).
card_subtypes('order of the golden cricket', ['Kithkin', 'Knight']).
card_colors('order of the golden cricket', ['W']).
card_text('order of the golden cricket', 'Whenever Order of the Golden Cricket attacks, you may pay {W}. If you do, it gains flying until end of turn.').
card_mana_cost('order of the golden cricket', ['1', 'W']).
card_cmc('order of the golden cricket', 2).
card_layout('order of the golden cricket', 'normal').
card_power('order of the golden cricket', 2).
card_toughness('order of the golden cricket', 2).

% Found in: 9ED, CHK
card_name('order of the sacred bell', 'Order of the Sacred Bell').
card_type('order of the sacred bell', 'Creature — Human Monk').
card_types('order of the sacred bell', ['Creature']).
card_subtypes('order of the sacred bell', ['Human', 'Monk']).
card_colors('order of the sacred bell', ['G']).
card_text('order of the sacred bell', '').
card_mana_cost('order of the sacred bell', ['3', 'G']).
card_cmc('order of the sacred bell', 4).
card_layout('order of the sacred bell', 'normal').
card_power('order of the sacred bell', 4).
card_toughness('order of the sacred bell', 3).

% Found in: 5ED, 6ED, ICE, ME2
card_name('order of the sacred torch', 'Order of the Sacred Torch').
card_type('order of the sacred torch', 'Creature — Human Knight').
card_types('order of the sacred torch', ['Creature']).
card_subtypes('order of the sacred torch', ['Human', 'Knight']).
card_colors('order of the sacred torch', ['W']).
card_text('order of the sacred torch', '{T}, Pay 1 life: Counter target black spell.').
card_mana_cost('order of the sacred torch', ['1', 'W', 'W']).
card_cmc('order of the sacred torch', 3).
card_layout('order of the sacred torch', 'normal').
card_power('order of the sacred torch', 2).
card_toughness('order of the sacred torch', 2).

% Found in: GPT
card_name('order of the stars', 'Order of the Stars').
card_type('order of the stars', 'Creature — Human Cleric').
card_types('order of the stars', ['Creature']).
card_subtypes('order of the stars', ['Human', 'Cleric']).
card_colors('order of the stars', ['W']).
card_text('order of the stars', 'Defender (This creature can\'t attack.)\nAs Order of the Stars enters the battlefield, choose a color.\nOrder of the Stars has protection from the chosen color.').
card_mana_cost('order of the stars', ['W']).
card_cmc('order of the stars', 1).
card_layout('order of the stars', 'normal').
card_power('order of the stars', 0).
card_toughness('order of the stars', 1).

% Found in: 5ED, ATH, ICE, ME2
card_name('order of the white shield', 'Order of the White Shield').
card_type('order of the white shield', 'Creature — Human Knight').
card_types('order of the white shield', ['Creature']).
card_subtypes('order of the white shield', ['Human', 'Knight']).
card_colors('order of the white shield', ['W']).
card_text('order of the white shield', 'Protection from black\n{W}: Order of the White Shield gains first strike until end of turn.\n{W}{W}: Order of the White Shield gets +1/+0 until end of turn.').
card_mana_cost('order of the white shield', ['W', 'W']).
card_cmc('order of the white shield', 2).
card_layout('order of the white shield', 'normal').
card_power('order of the white shield', 2).
card_toughness('order of the white shield', 1).

% Found in: SHM
card_name('order of whiteclay', 'Order of Whiteclay').
card_type('order of whiteclay', 'Creature — Kithkin Cleric').
card_types('order of whiteclay', ['Creature']).
card_subtypes('order of whiteclay', ['Kithkin', 'Cleric']).
card_colors('order of whiteclay', ['W']).
card_text('order of whiteclay', '{1}{W}{W}, {Q}: Return target creature card with converted mana cost 3 or less from your graveyard to the battlefield. ({Q} is the untap symbol.)').
card_mana_cost('order of whiteclay', ['1', 'W', 'W']).
card_cmc('order of whiteclay', 3).
card_layout('order of whiteclay', 'normal').
card_power('order of whiteclay', 1).
card_toughness('order of whiteclay', 4).

% Found in: DDE, USG
card_name('order of yawgmoth', 'Order of Yawgmoth').
card_type('order of yawgmoth', 'Creature — Zombie Knight').
card_types('order of yawgmoth', ['Creature']).
card_subtypes('order of yawgmoth', ['Zombie', 'Knight']).
card_colors('order of yawgmoth', ['B']).
card_text('order of yawgmoth', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever Order of Yawgmoth deals damage to a player, that player discards a card.').
card_mana_cost('order of yawgmoth', ['2', 'B', 'B']).
card_cmc('order of yawgmoth', 4).
card_layout('order of yawgmoth', 'normal').
card_power('order of yawgmoth', 2).
card_toughness('order of yawgmoth', 2).

% Found in: INV
card_name('ordered migration', 'Ordered Migration').
card_type('ordered migration', 'Sorcery').
card_types('ordered migration', ['Sorcery']).
card_subtypes('ordered migration', []).
card_colors('ordered migration', ['W', 'U']).
card_text('ordered migration', 'Domain — Put a 1/1 blue Bird creature token with flying onto the battlefield for each basic land type among lands you control.').
card_mana_cost('ordered migration', ['3', 'W', 'U']).
card_cmc('ordered migration', 5).
card_layout('ordered migration', 'normal').

% Found in: RAV
card_name('ordruun commando', 'Ordruun Commando').
card_type('ordruun commando', 'Creature — Minotaur Soldier').
card_types('ordruun commando', ['Creature']).
card_subtypes('ordruun commando', ['Minotaur', 'Soldier']).
card_colors('ordruun commando', ['R']).
card_text('ordruun commando', '{W}: Prevent the next 1 damage that would be dealt to Ordruun Commando this turn.').
card_mana_cost('ordruun commando', ['3', 'R']).
card_cmc('ordruun commando', 4).
card_layout('ordruun commando', 'normal').
card_power('ordruun commando', 4).
card_toughness('ordruun commando', 1).

% Found in: GTC
card_name('ordruun veteran', 'Ordruun Veteran').
card_type('ordruun veteran', 'Creature — Minotaur Soldier').
card_types('ordruun veteran', ['Creature']).
card_subtypes('ordruun veteran', ['Minotaur', 'Soldier']).
card_colors('ordruun veteran', ['W', 'R']).
card_text('ordruun veteran', 'Battalion — Whenever Ordruun Veteran and at least two other creatures attack, Ordruun Veteran gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_mana_cost('ordruun veteran', ['2', 'R', 'W']).
card_cmc('ordruun veteran', 4).
card_layout('ordruun veteran', 'normal').
card_power('ordruun veteran', 3).
card_toughness('ordruun veteran', 1).

% Found in: CHK
card_name('ore gorger', 'Ore Gorger').
card_type('ore gorger', 'Creature — Spirit').
card_types('ore gorger', ['Creature']).
card_subtypes('ore gorger', ['Spirit']).
card_colors('ore gorger', ['R']).
card_text('ore gorger', 'Whenever you cast a Spirit or Arcane spell, you may destroy target nonbasic land.').
card_mana_cost('ore gorger', ['3', 'R', 'R']).
card_cmc('ore gorger', 5).
card_layout('ore gorger', 'normal').
card_power('ore gorger', 3).
card_toughness('ore gorger', 1).

% Found in: BNG
card_name('oreskos sun guide', 'Oreskos Sun Guide').
card_type('oreskos sun guide', 'Creature — Cat Monk').
card_types('oreskos sun guide', ['Creature']).
card_subtypes('oreskos sun guide', ['Cat', 'Monk']).
card_colors('oreskos sun guide', ['W']).
card_text('oreskos sun guide', 'Inspired — Whenever Oreskos Sun Guide becomes untapped, you gain 2 life.').
card_mana_cost('oreskos sun guide', ['1', 'W']).
card_cmc('oreskos sun guide', 2).
card_layout('oreskos sun guide', 'normal').
card_power('oreskos sun guide', 2).
card_toughness('oreskos sun guide', 2).

% Found in: JOU, M15
card_name('oreskos swiftclaw', 'Oreskos Swiftclaw').
card_type('oreskos swiftclaw', 'Creature — Cat Warrior').
card_types('oreskos swiftclaw', ['Creature']).
card_subtypes('oreskos swiftclaw', ['Cat', 'Warrior']).
card_colors('oreskos swiftclaw', ['W']).
card_text('oreskos swiftclaw', '').
card_mana_cost('oreskos swiftclaw', ['1', 'W']).
card_cmc('oreskos swiftclaw', 2).
card_layout('oreskos swiftclaw', 'normal').
card_power('oreskos swiftclaw', 3).
card_toughness('oreskos swiftclaw', 1).

% Found in: TOR
card_name('organ grinder', 'Organ Grinder').
card_type('organ grinder', 'Creature — Zombie').
card_types('organ grinder', ['Creature']).
card_subtypes('organ grinder', ['Zombie']).
card_colors('organ grinder', ['B']).
card_text('organ grinder', '{T}, Exile three cards from your graveyard: Target player loses 3 life.').
card_mana_cost('organ grinder', ['2', 'B']).
card_cmc('organ grinder', 3).
card_layout('organ grinder', 'normal').
card_power('organ grinder', 3).
card_toughness('organ grinder', 1).

% Found in: UGL
card_name('organ harvest', 'Organ Harvest').
card_type('organ harvest', 'Sorcery').
card_types('organ harvest', ['Sorcery']).
card_subtypes('organ harvest', []).
card_colors('organ harvest', ['B']).
card_text('organ harvest', 'You and your teammates may sacrifice any number of creatures. For each creature sacrificed in this way, add {B}{B} to your mana pool.').
card_mana_cost('organ harvest', ['B']).
card_cmc('organ harvest', 1).
card_layout('organ harvest', 'normal').

% Found in: 5ED, FEM, TSB
card_name('orgg', 'Orgg').
card_type('orgg', 'Creature — Orgg').
card_types('orgg', ['Creature']).
card_subtypes('orgg', ['Orgg']).
card_colors('orgg', ['R']).
card_text('orgg', 'Trample\nOrgg can\'t attack if defending player controls an untapped creature with power 3 or greater.\nOrgg can\'t block creatures with power 3 or greater.').
card_mana_cost('orgg', ['3', 'R', 'R']).
card_cmc('orgg', 5).
card_layout('orgg', 'normal').
card_power('orgg', 6).
card_toughness('orgg', 6).

% Found in: SOM
card_name('origin spellbomb', 'Origin Spellbomb').
card_type('origin spellbomb', 'Artifact').
card_types('origin spellbomb', ['Artifact']).
card_subtypes('origin spellbomb', []).
card_colors('origin spellbomb', []).
card_text('origin spellbomb', '{1}, {T}, Sacrifice Origin Spellbomb: Put a 1/1 colorless Myr artifact creature token onto the battlefield.\nWhen Origin Spellbomb is put into a graveyard from the battlefield, you may pay {W}. If you do, draw a card.').
card_mana_cost('origin spellbomb', ['1']).
card_cmc('origin spellbomb', 1).
card_layout('origin spellbomb', 'normal').

% Found in: VAN
card_name('orim', 'Orim').
card_type('orim', 'Vanguard').
card_types('orim', ['Vanguard']).
card_subtypes('orim', []).
card_colors('orim', []).
card_text('orim', 'Creatures you control have reach.').
card_layout('orim', 'vanguard').

% Found in: PLS, pJGP
card_name('orim\'s chant', 'Orim\'s Chant').
card_type('orim\'s chant', 'Instant').
card_types('orim\'s chant', ['Instant']).
card_subtypes('orim\'s chant', []).
card_colors('orim\'s chant', ['W']).
card_text('orim\'s chant', 'Kicker {W} (You may pay an additional {W} as you cast this spell.)\nTarget player can\'t cast spells this turn.\nIf Orim\'s Chant was kicked, creatures can\'t attack this turn.').
card_mana_cost('orim\'s chant', ['W']).
card_cmc('orim\'s chant', 1).
card_layout('orim\'s chant', 'normal').

% Found in: MMQ
card_name('orim\'s cure', 'Orim\'s Cure').
card_type('orim\'s cure', 'Instant').
card_types('orim\'s cure', ['Instant']).
card_subtypes('orim\'s cure', []).
card_colors('orim\'s cure', ['W']).
card_text('orim\'s cure', 'If you control a Plains, you may tap an untapped creature you control rather than pay Orim\'s Cure\'s mana cost.\nPrevent the next 4 damage that would be dealt to target creature or player this turn.').
card_mana_cost('orim\'s cure', ['1', 'W']).
card_cmc('orim\'s cure', 2).
card_layout('orim\'s cure', 'normal').

% Found in: TMP
card_name('orim\'s prayer', 'Orim\'s Prayer').
card_type('orim\'s prayer', 'Enchantment').
card_types('orim\'s prayer', ['Enchantment']).
card_subtypes('orim\'s prayer', []).
card_colors('orim\'s prayer', ['W']).
card_text('orim\'s prayer', 'Whenever one or more creatures attack you, you gain 1 life for each attacking creature.').
card_mana_cost('orim\'s prayer', ['1', 'W', 'W']).
card_cmc('orim\'s prayer', 3).
card_layout('orim\'s prayer', 'normal').

% Found in: APC, CMD, HOP
card_name('orim\'s thunder', 'Orim\'s Thunder').
card_type('orim\'s thunder', 'Instant').
card_types('orim\'s thunder', ['Instant']).
card_subtypes('orim\'s thunder', []).
card_colors('orim\'s thunder', ['W']).
card_text('orim\'s thunder', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nDestroy target artifact or enchantment. If Orim\'s Thunder was kicked, it deals damage equal to that permanent\'s converted mana cost to target creature.').
card_mana_cost('orim\'s thunder', ['2', 'W']).
card_cmc('orim\'s thunder', 3).
card_layout('orim\'s thunder', 'normal').

% Found in: INV
card_name('orim\'s touch', 'Orim\'s Touch').
card_type('orim\'s touch', 'Instant').
card_types('orim\'s touch', ['Instant']).
card_subtypes('orim\'s touch', []).
card_colors('orim\'s touch', ['W']).
card_text('orim\'s touch', 'Kicker {1} (You may pay an additional {1} as you cast this spell.)\nPrevent the next 2 damage that would be dealt to target creature or player this turn. If Orim\'s Touch was kicked, prevent the next 4 damage that would be dealt to that creature or player this turn instead.').
card_mana_cost('orim\'s touch', ['W']).
card_cmc('orim\'s touch', 1).
card_layout('orim\'s touch', 'normal').

% Found in: TMP, TPR
card_name('orim, samite healer', 'Orim, Samite Healer').
card_type('orim, samite healer', 'Legendary Creature — Human Cleric').
card_types('orim, samite healer', ['Creature']).
card_subtypes('orim, samite healer', ['Human', 'Cleric']).
card_supertypes('orim, samite healer', ['Legendary']).
card_colors('orim, samite healer', ['W']).
card_text('orim, samite healer', '{T}: Prevent the next 3 damage that would be dealt to target creature or player this turn.').
card_mana_cost('orim, samite healer', ['1', 'W', 'W']).
card_cmc('orim, samite healer', 3).
card_layout('orim, samite healer', 'normal').
card_power('orim, samite healer', 1).
card_toughness('orim, samite healer', 3).
card_reserved('orim, samite healer').

% Found in: FUT
card_name('oriss, samite guardian', 'Oriss, Samite Guardian').
card_type('oriss, samite guardian', 'Legendary Creature — Human Cleric').
card_types('oriss, samite guardian', ['Creature']).
card_subtypes('oriss, samite guardian', ['Human', 'Cleric']).
card_supertypes('oriss, samite guardian', ['Legendary']).
card_colors('oriss, samite guardian', ['W']).
card_text('oriss, samite guardian', '{T}: Prevent all damage that would be dealt to target creature this turn.\nGrandeur — Discard another card named Oriss, Samite Guardian: Target player can\'t cast spells this turn, and creatures that player controls can\'t attack this turn.').
card_mana_cost('oriss, samite guardian', ['1', 'W', 'W']).
card_cmc('oriss, samite guardian', 3).
card_layout('oriss, samite guardian', 'normal').
card_power('oriss, samite guardian', 1).
card_toughness('oriss, samite guardian', 3).

% Found in: BOK
card_name('ornate kanzashi', 'Ornate Kanzashi').
card_type('ornate kanzashi', 'Artifact').
card_types('ornate kanzashi', ['Artifact']).
card_subtypes('ornate kanzashi', []).
card_colors('ornate kanzashi', []).
card_text('ornate kanzashi', '{2}, {T}: Target opponent exiles the top card of his or her library. You may play that card this turn.').
card_mana_cost('ornate kanzashi', ['5']).
card_cmc('ornate kanzashi', 5).
card_layout('ornate kanzashi', 'normal').

% Found in: BNG
card_name('ornitharch', 'Ornitharch').
card_type('ornitharch', 'Creature — Archon').
card_types('ornitharch', ['Creature']).
card_subtypes('ornitharch', ['Archon']).
card_colors('ornitharch', ['W']).
card_text('ornitharch', 'Flying\nTribute 2 (As this creature enters the battlefield, an opponent of your choice may place two +1/+1 counters on it.)\nWhen Ornitharch enters the battlefield, if tribute wasn\'t paid, put two 1/1 white Bird creature tokens with flying onto the battlefield.').
card_mana_cost('ornitharch', ['3', 'W', 'W']).
card_cmc('ornitharch', 5).
card_layout('ornitharch', 'normal').
card_power('ornitharch', 3).
card_toughness('ornitharch', 3).

% Found in: 10E, 3ED, 4ED, 5ED, 6ED, 9ED, ATQ, M10, M11, M15, MRD
card_name('ornithopter', 'Ornithopter').
card_type('ornithopter', 'Artifact Creature — Thopter').
card_types('ornithopter', ['Artifact', 'Creature']).
card_subtypes('ornithopter', ['Thopter']).
card_colors('ornithopter', []).
card_text('ornithopter', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('ornithopter', ['0']).
card_cmc('ornithopter', 0).
card_layout('ornithopter', 'normal').
card_power('ornithopter', 0).
card_toughness('ornithopter', 2).

% Found in: PC2
card_name('orochi colony', 'Orochi Colony').
card_type('orochi colony', 'Plane — Kamigawa').
card_types('orochi colony', ['Plane']).
card_subtypes('orochi colony', ['Kamigawa']).
card_colors('orochi colony', []).
card_text('orochi colony', 'Whenever a creature you control deals combat damage to a player, you may search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library.\nWhenever you roll {C}, target creature can\'t be blocked this turn.').
card_layout('orochi colony', 'plane').

% Found in: CHK
card_name('orochi eggwatcher', 'Orochi Eggwatcher').
card_type('orochi eggwatcher', 'Creature — Snake Shaman').
card_types('orochi eggwatcher', ['Creature']).
card_subtypes('orochi eggwatcher', ['Snake', 'Shaman']).
card_colors('orochi eggwatcher', ['G']).
card_text('orochi eggwatcher', '{2}{G}, {T}: Put a 1/1 green Snake creature token onto the battlefield. If you control ten or more creatures, flip Orochi Eggwatcher.').
card_mana_cost('orochi eggwatcher', ['2', 'G']).
card_cmc('orochi eggwatcher', 3).
card_layout('orochi eggwatcher', 'flip').
card_power('orochi eggwatcher', 1).
card_toughness('orochi eggwatcher', 1).
card_sides('orochi eggwatcher', 'shidako, broodmistress').

% Found in: CHK
card_name('orochi hatchery', 'Orochi Hatchery').
card_type('orochi hatchery', 'Artifact').
card_types('orochi hatchery', ['Artifact']).
card_subtypes('orochi hatchery', []).
card_colors('orochi hatchery', []).
card_text('orochi hatchery', 'Orochi Hatchery enters the battlefield with X charge counters on it.\n{5}, {T}: Put a 1/1 green Snake creature token onto the battlefield for each charge counter on Orochi Hatchery.').
card_mana_cost('orochi hatchery', ['X', 'X']).
card_cmc('orochi hatchery', 0).
card_layout('orochi hatchery', 'normal').

% Found in: CHK
card_name('orochi leafcaller', 'Orochi Leafcaller').
card_type('orochi leafcaller', 'Creature — Snake Shaman').
card_types('orochi leafcaller', ['Creature']).
card_subtypes('orochi leafcaller', ['Snake', 'Shaman']).
card_colors('orochi leafcaller', ['G']).
card_text('orochi leafcaller', '{G}: Add one mana of any color to your mana pool.').
card_mana_cost('orochi leafcaller', ['G']).
card_cmc('orochi leafcaller', 1).
card_layout('orochi leafcaller', 'normal').
card_power('orochi leafcaller', 1).
card_toughness('orochi leafcaller', 1).

% Found in: CHK
card_name('orochi ranger', 'Orochi Ranger').
card_type('orochi ranger', 'Creature — Snake Warrior').
card_types('orochi ranger', ['Creature']).
card_subtypes('orochi ranger', ['Snake', 'Warrior']).
card_colors('orochi ranger', ['G']).
card_text('orochi ranger', 'Whenever Orochi Ranger deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('orochi ranger', ['1', 'G']).
card_cmc('orochi ranger', 2).
card_layout('orochi ranger', 'normal').
card_power('orochi ranger', 2).
card_toughness('orochi ranger', 1).

% Found in: CHK
card_name('orochi sustainer', 'Orochi Sustainer').
card_type('orochi sustainer', 'Creature — Snake Shaman').
card_types('orochi sustainer', ['Creature']).
card_subtypes('orochi sustainer', ['Snake', 'Shaman']).
card_colors('orochi sustainer', ['G']).
card_text('orochi sustainer', '{T}: Add {G} to your mana pool.').
card_mana_cost('orochi sustainer', ['1', 'G']).
card_cmc('orochi sustainer', 2).
card_layout('orochi sustainer', 'normal').
card_power('orochi sustainer', 1).
card_toughness('orochi sustainer', 2).

% Found in: CMD, PLC, pPRE
card_name('oros, the avenger', 'Oros, the Avenger').
card_type('oros, the avenger', 'Legendary Creature — Dragon').
card_types('oros, the avenger', ['Creature']).
card_subtypes('oros, the avenger', ['Dragon']).
card_supertypes('oros, the avenger', ['Legendary']).
card_colors('oros, the avenger', ['W', 'B', 'R']).
card_text('oros, the avenger', 'Flying\nWhenever Oros, the Avenger deals combat damage to a player, you may pay {2}{W}. If you do, Oros deals 3 damage to each nonwhite creature.').
card_mana_cost('oros, the avenger', ['3', 'W', 'B', 'R']).
card_cmc('oros, the avenger', 6).
card_layout('oros, the avenger', 'normal').
card_power('oros, the avenger', 6).
card_toughness('oros, the avenger', 6).

% Found in: C13, CMD, GPT, MM2
card_name('orzhov basilica', 'Orzhov Basilica').
card_type('orzhov basilica', 'Land').
card_types('orzhov basilica', ['Land']).
card_subtypes('orzhov basilica', []).
card_colors('orzhov basilica', []).
card_text('orzhov basilica', 'Orzhov Basilica enters the battlefield tapped.\nWhen Orzhov Basilica enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {W}{B} to your mana pool.').
card_layout('orzhov basilica', 'normal').

% Found in: GTC
card_name('orzhov charm', 'Orzhov Charm').
card_type('orzhov charm', 'Instant').
card_types('orzhov charm', ['Instant']).
card_subtypes('orzhov charm', []).
card_colors('orzhov charm', ['W', 'B']).
card_text('orzhov charm', 'Choose one —\n• Return target creature you control and all Auras you control attached to it to their owner\'s hand.\n• Destroy target creature and you lose life equal to its toughness.\n• Return target creature card with converted mana cost 1 or less from your graveyard to the battlefield.').
card_mana_cost('orzhov charm', ['W', 'B']).
card_cmc('orzhov charm', 2).
card_layout('orzhov charm', 'normal').

% Found in: DGM
card_name('orzhov cluestone', 'Orzhov Cluestone').
card_type('orzhov cluestone', 'Artifact').
card_types('orzhov cluestone', ['Artifact']).
card_subtypes('orzhov cluestone', []).
card_colors('orzhov cluestone', []).
card_text('orzhov cluestone', '{T}: Add {W} or {B} to your mana pool.\n{W}{B}, {T}, Sacrifice Orzhov Cluestone: Draw a card.').
card_mana_cost('orzhov cluestone', ['3']).
card_cmc('orzhov cluestone', 3).
card_layout('orzhov cluestone', 'normal').

% Found in: GPT
card_name('orzhov euthanist', 'Orzhov Euthanist').
card_type('orzhov euthanist', 'Creature — Human Assassin').
card_types('orzhov euthanist', ['Creature']).
card_subtypes('orzhov euthanist', ['Human', 'Assassin']).
card_colors('orzhov euthanist', ['B']).
card_text('orzhov euthanist', 'Haunt (When this creature dies, exile it haunting target creature.)\nWhen Orzhov Euthanist enters the battlefield or the creature it haunts dies, destroy target creature that was dealt damage this turn.').
card_mana_cost('orzhov euthanist', ['2', 'B']).
card_cmc('orzhov euthanist', 3).
card_layout('orzhov euthanist', 'normal').
card_power('orzhov euthanist', 2).
card_toughness('orzhov euthanist', 2).

% Found in: C13, DGM, GTC
card_name('orzhov guildgate', 'Orzhov Guildgate').
card_type('orzhov guildgate', 'Land — Gate').
card_types('orzhov guildgate', ['Land']).
card_subtypes('orzhov guildgate', ['Gate']).
card_colors('orzhov guildgate', []).
card_text('orzhov guildgate', 'Orzhov Guildgate enters the battlefield tapped.\n{T}: Add {W} or {B} to your mana pool.').
card_layout('orzhov guildgate', 'normal').

% Found in: CMD, GPT
card_name('orzhov guildmage', 'Orzhov Guildmage').
card_type('orzhov guildmage', 'Creature — Human Wizard').
card_types('orzhov guildmage', ['Creature']).
card_subtypes('orzhov guildmage', ['Human', 'Wizard']).
card_colors('orzhov guildmage', ['W', 'B']).
card_text('orzhov guildmage', '{2}{W}: Target player gains 1 life.\n{2}{B}: Each player loses 1 life.').
card_mana_cost('orzhov guildmage', ['W/B', 'W/B']).
card_cmc('orzhov guildmage', 2).
card_layout('orzhov guildmage', 'normal').
card_power('orzhov guildmage', 2).
card_toughness('orzhov guildmage', 2).

% Found in: GTC
card_name('orzhov keyrune', 'Orzhov Keyrune').
card_type('orzhov keyrune', 'Artifact').
card_types('orzhov keyrune', ['Artifact']).
card_subtypes('orzhov keyrune', []).
card_colors('orzhov keyrune', []).
card_text('orzhov keyrune', '{T}: Add {W} or {B} to your mana pool.\n{W}{B}: Orzhov Keyrune becomes a 1/4 white and black Thrull artifact creature with lifelink until end of turn.').
card_mana_cost('orzhov keyrune', ['3']).
card_cmc('orzhov keyrune', 3).
card_layout('orzhov keyrune', 'normal').

% Found in: GPT
card_name('orzhov pontiff', 'Orzhov Pontiff').
card_type('orzhov pontiff', 'Creature — Human Cleric').
card_types('orzhov pontiff', ['Creature']).
card_subtypes('orzhov pontiff', ['Human', 'Cleric']).
card_colors('orzhov pontiff', ['W', 'B']).
card_text('orzhov pontiff', 'Haunt (When this creature dies, exile it haunting target creature.)\nWhen Orzhov Pontiff enters the battlefield or the creature it haunts dies, choose one —\n• Creatures you control get +1/+1 until end of turn.\n• Creatures you don\'t control get -1/-1 until end of turn.').
card_mana_cost('orzhov pontiff', ['1', 'W', 'B']).
card_cmc('orzhov pontiff', 3).
card_layout('orzhov pontiff', 'normal').
card_power('orzhov pontiff', 1).
card_toughness('orzhov pontiff', 1).

% Found in: CMD, GPT
card_name('orzhov signet', 'Orzhov Signet').
card_type('orzhov signet', 'Artifact').
card_types('orzhov signet', ['Artifact']).
card_subtypes('orzhov signet', []).
card_colors('orzhov signet', []).
card_text('orzhov signet', '{1}, {T}: Add {W}{B} to your mana pool.').
card_mana_cost('orzhov signet', ['2']).
card_cmc('orzhov signet', 2).
card_layout('orzhov signet', 'normal').

% Found in: PC2
card_name('orzhova', 'Orzhova').
card_type('orzhova', 'Plane — Ravnica').
card_types('orzhova', ['Plane']).
card_subtypes('orzhova', ['Ravnica']).
card_colors('orzhova', []).
card_text('orzhova', 'When you planeswalk away from Orzhova, each player returns all creature cards from his or her graveyard to the battlefield.\nWhenever you roll {C}, for each opponent, exile up to one target creature card from that player\'s graveyard.').
card_layout('orzhova', 'plane').

% Found in: GPT
card_name('orzhova, the church of deals', 'Orzhova, the Church of Deals').
card_type('orzhova, the church of deals', 'Land').
card_types('orzhova, the church of deals', ['Land']).
card_subtypes('orzhova, the church of deals', []).
card_colors('orzhova, the church of deals', []).
card_text('orzhova, the church of deals', '{T}: Add {1} to your mana pool.\n{3}{W}{B}, {T}: Target player loses 1 life and you gain 1 life.').
card_layout('orzhova, the church of deals', 'normal').

% Found in: 4ED, LEG, ME4
card_name('osai vultures', 'Osai Vultures').
card_type('osai vultures', 'Creature — Bird').
card_types('osai vultures', ['Creature']).
card_subtypes('osai vultures', ['Bird']).
card_colors('osai vultures', ['W']).
card_text('osai vultures', 'Flying\nAt the beginning of each end step, if a creature died this turn, put a carrion counter on Osai Vultures.\nRemove two carrion counters from Osai Vultures: Osai Vultures gets +1/+1 until end of turn.').
card_mana_cost('osai vultures', ['1', 'W']).
card_cmc('osai vultures', 2).
card_layout('osai vultures', 'normal').
card_power('osai vultures', 1).
card_toughness('osai vultures', 1).

% Found in: GPT
card_name('ostiary thrull', 'Ostiary Thrull').
card_type('ostiary thrull', 'Creature — Thrull').
card_types('ostiary thrull', ['Creature']).
card_subtypes('ostiary thrull', ['Thrull']).
card_colors('ostiary thrull', ['B']).
card_text('ostiary thrull', '{W}, {T}: Tap target creature.').
card_mana_cost('ostiary thrull', ['3', 'B']).
card_cmc('ostiary thrull', 4).
card_layout('ostiary thrull', 'normal').
card_power('ostiary thrull', 2).
card_toughness('ostiary thrull', 2).

% Found in: 7ED, ULG
card_name('ostracize', 'Ostracize').
card_type('ostracize', 'Sorcery').
card_types('ostracize', ['Sorcery']).
card_subtypes('ostracize', []).
card_colors('ostracize', ['B']).
card_text('ostracize', 'Target opponent reveals his or her hand. You choose a creature card from it. That player discards that card.').
card_mana_cost('ostracize', ['B']).
card_cmc('ostracize', 1).
card_layout('ostracize', 'normal').

% Found in: HOP
card_name('otaria', 'Otaria').
card_type('otaria', 'Plane — Dominaria').
card_types('otaria', ['Plane']).
card_subtypes('otaria', ['Dominaria']).
card_colors('otaria', []).
card_text('otaria', 'Instant and sorcery cards in graveyards have flashback. The flashback cost is equal to the card\'s mana cost. (Its owner may cast the card from his or her graveyard for its mana cost. Then he or she exiles it.)\nWhenever you roll {C}, take an extra turn after this one.').
card_layout('otaria', 'plane').

% Found in: ODY
card_name('otarian juggernaut', 'Otarian Juggernaut').
card_type('otarian juggernaut', 'Artifact Creature — Juggernaut').
card_types('otarian juggernaut', ['Artifact', 'Creature']).
card_subtypes('otarian juggernaut', ['Juggernaut']).
card_colors('otarian juggernaut', []).
card_text('otarian juggernaut', 'Otarian Juggernaut can\'t be blocked by Walls.\nThreshold — As long as seven or more cards are in your graveyard, Otarian Juggernaut gets +3/+0 and attacks each turn if able.').
card_mana_cost('otarian juggernaut', ['4']).
card_cmc('otarian juggernaut', 4).
card_layout('otarian juggernaut', 'normal').
card_power('otarian juggernaut', 2).
card_toughness('otarian juggernaut', 3).

% Found in: AVR
card_name('otherworld atlas', 'Otherworld Atlas').
card_type('otherworld atlas', 'Artifact').
card_types('otherworld atlas', ['Artifact']).
card_subtypes('otherworld atlas', []).
card_colors('otherworld atlas', []).
card_text('otherworld atlas', '{T}: Put a charge counter on Otherworld Atlas.\n{T}: Each player draws a card for each charge counter on Otherworld Atlas.').
card_mana_cost('otherworld atlas', ['4']).
card_cmc('otherworld atlas', 4).
card_layout('otherworld atlas', 'normal').

% Found in: CHK, DD3_DVD, DDC, MM2, MMA
card_name('otherworldly journey', 'Otherworldly Journey').
card_type('otherworldly journey', 'Instant — Arcane').
card_types('otherworldly journey', ['Instant']).
card_subtypes('otherworldly journey', ['Arcane']).
card_colors('otherworldly journey', ['W']).
card_text('otherworldly journey', 'Exile target creature. At the beginning of the next end step, return that card to the battlefield under its owner\'s control with a +1/+1 counter on it.').
card_mana_cost('otherworldly journey', ['1', 'W']).
card_cmc('otherworldly journey', 2).
card_layout('otherworldly journey', 'normal').

% Found in: ARN, MED
card_name('oubliette', 'Oubliette').
card_type('oubliette', 'Enchantment').
card_types('oubliette', ['Enchantment']).
card_subtypes('oubliette', []).
card_colors('oubliette', ['B']).
card_text('oubliette', 'When Oubliette enters the battlefield, exile target creature and all Auras attached to it. Note the number and kind of counters that were on that creature.\nWhen Oubliette leaves the battlefield, return that exiled card to the battlefield under its owner\'s control tapped with the noted number and kind of counters on it. If you do, return the other exiled cards to the battlefield under their owner\'s control attached to that permanent.').
card_mana_cost('oubliette', ['1', 'B', 'B']).
card_cmc('oubliette', 3).
card_layout('oubliette', 'normal').

% Found in: 5DN
card_name('ouphe vandals', 'Ouphe Vandals').
card_type('ouphe vandals', 'Creature — Ouphe Rogue').
card_types('ouphe vandals', ['Creature']).
card_subtypes('ouphe vandals', ['Ouphe', 'Rogue']).
card_colors('ouphe vandals', ['G']).
card_text('ouphe vandals', '{G}, Sacrifice Ouphe Vandals: Counter target activated ability from an artifact source and destroy that artifact if it\'s on the battlefield. (Mana abilities can\'t be targeted.)').
card_mana_cost('ouphe vandals', ['2', 'G']).
card_cmc('ouphe vandals', 3).
card_layout('ouphe vandals', 'normal').
card_power('ouphe vandals', 2).
card_toughness('ouphe vandals', 2).

% Found in: UNH
card_name('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 'Our Market Research Shows That Players Like Really Long Card Names So We Made this Card to Have the Absolute Longest Card Name Ever Elemental').
card_type('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 'Creature — Elemental').
card_types('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', ['Creature']).
card_subtypes('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', ['Elemental']).
card_colors('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', ['G']).
card_text('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 'Art rampage 2 (Whenever this becomes blocked by a creature, it gets +2/+2 for each creature in the blocker\'s art beyond the first.)').
card_mana_cost('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', ['1', 'G', 'G']).
card_cmc('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 3).
card_layout('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 'normal').
card_power('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 2).
card_toughness('our market research shows that players like really long card names so we made this card to have the absolute longest card name ever elemental', 2).

% Found in: DDP, ROE
card_name('oust', 'Oust').
card_type('oust', 'Sorcery').
card_types('oust', ['Sorcery']).
card_subtypes('oust', []).
card_colors('oust', ['W']).
card_text('oust', 'Put target creature into its owner\'s library second from the top. Its controller gains 3 life.').
card_mana_cost('oust', ['W']).
card_cmc('oust', 1).
card_layout('oust', 'normal').

% Found in: PCY
card_name('outbreak', 'Outbreak').
card_type('outbreak', 'Sorcery').
card_types('outbreak', ['Sorcery']).
card_subtypes('outbreak', []).
card_colors('outbreak', ['B']).
card_text('outbreak', 'You may discard a Swamp card rather than pay Outbreak\'s mana cost.\nChoose a creature type. All creatures of that type get -1/-1 until end of turn.').
card_mana_cost('outbreak', ['3', 'B']).
card_cmc('outbreak', 4).
card_layout('outbreak', 'normal').

% Found in: ORI
card_name('outland colossus', 'Outland Colossus').
card_type('outland colossus', 'Creature — Giant').
card_types('outland colossus', ['Creature']).
card_subtypes('outland colossus', ['Giant']).
card_colors('outland colossus', ['G']).
card_text('outland colossus', 'Renown 6 (When this creature deals combat damage to a player, if it isn\'t renowned, put six +1/+1 counters on it and it becomes renowned.)\nOutland Colossus can\'t be blocked by more than one creature.').
card_mana_cost('outland colossus', ['3', 'G', 'G']).
card_cmc('outland colossus', 5).
card_layout('outland colossus', 'normal').
card_power('outland colossus', 6).
card_toughness('outland colossus', 6).

% Found in: USG
card_name('outmaneuver', 'Outmaneuver').
card_type('outmaneuver', 'Instant').
card_types('outmaneuver', ['Instant']).
card_subtypes('outmaneuver', []).
card_colors('outmaneuver', ['R']).
card_text('outmaneuver', 'X target blocked creatures assign their combat damage this turn as though they weren\'t blocked.').
card_mana_cost('outmaneuver', ['X', 'R']).
card_cmc('outmaneuver', 1).
card_layout('outmaneuver', 'normal').

% Found in: BFZ
card_name('outnumber', 'Outnumber').
card_type('outnumber', 'Instant').
card_types('outnumber', ['Instant']).
card_subtypes('outnumber', []).
card_colors('outnumber', ['R']).
card_text('outnumber', 'Outnumber deals damage to target creature equal to the number of creatures you control.').
card_mana_cost('outnumber', ['R']).
card_cmc('outnumber', 1).
card_layout('outnumber', 'normal').

% Found in: FRF
card_name('outpost siege', 'Outpost Siege').
card_type('outpost siege', 'Enchantment').
card_types('outpost siege', ['Enchantment']).
card_subtypes('outpost siege', []).
card_colors('outpost siege', ['R']).
card_text('outpost siege', 'As Outpost Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of your upkeep, exile the top card of your library. Until end of turn, you may play that card.\n• Dragons — Whenever a creature you control leaves the battlefield, Outpost Siege deals 1 damage to target creature or player.').
card_mana_cost('outpost siege', ['3', 'R']).
card_cmc('outpost siege', 4).
card_layout('outpost siege', 'normal').

% Found in: EVE
card_name('outrage shaman', 'Outrage Shaman').
card_type('outrage shaman', 'Creature — Goblin Shaman').
card_types('outrage shaman', ['Creature']).
card_subtypes('outrage shaman', ['Goblin', 'Shaman']).
card_colors('outrage shaman', ['R']).
card_text('outrage shaman', 'Chroma — When Outrage Shaman enters the battlefield, it deals damage to target creature equal to the number of red mana symbols in the mana costs of permanents you control.').
card_mana_cost('outrage shaman', ['3', 'R', 'R']).
card_cmc('outrage shaman', 5).
card_layout('outrage shaman', 'normal').
card_power('outrage shaman', 2).
card_toughness('outrage shaman', 2).

% Found in: TSP
card_name('outrider en-kor', 'Outrider en-Kor').
card_type('outrider en-kor', 'Creature — Kor Rebel Knight').
card_types('outrider en-kor', ['Creature']).
card_subtypes('outrider en-kor', ['Kor', 'Rebel', 'Knight']).
card_colors('outrider en-kor', ['W']).
card_text('outrider en-kor', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{0}: The next 1 damage that would be dealt to Outrider en-Kor this turn is dealt to target creature you control instead.').
card_mana_cost('outrider en-kor', ['2', 'W']).
card_cmc('outrider en-kor', 3).
card_layout('outrider en-kor', 'normal').
card_power('outrider en-kor', 2).
card_toughness('outrider en-kor', 2).

% Found in: ALA
card_name('outrider of jhess', 'Outrider of Jhess').
card_type('outrider of jhess', 'Creature — Human Knight').
card_types('outrider of jhess', ['Creature']).
card_subtypes('outrider of jhess', ['Human', 'Knight']).
card_colors('outrider of jhess', ['U']).
card_text('outrider of jhess', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('outrider of jhess', ['3', 'U']).
card_cmc('outrider of jhess', 4).
card_layout('outrider of jhess', 'normal').
card_power('outrider of jhess', 2).
card_toughness('outrider of jhess', 2).

% Found in: AVR
card_name('outwit', 'Outwit').
card_type('outwit', 'Instant').
card_types('outwit', ['Instant']).
card_subtypes('outwit', []).
card_colors('outwit', ['U']).
card_text('outwit', 'Counter target spell that targets a player.').
card_mana_cost('outwit', ['U']).
card_cmc('outwit', 1).
card_layout('outwit', 'normal').

% Found in: INV
card_name('overabundance', 'Overabundance').
card_type('overabundance', 'Enchantment').
card_types('overabundance', ['Enchantment']).
card_subtypes('overabundance', []).
card_colors('overabundance', ['R', 'G']).
card_text('overabundance', 'Whenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced, and Overabundance deals 1 damage to him or her.').
card_mana_cost('overabundance', ['1', 'R', 'G']).
card_cmc('overabundance', 3).
card_layout('overabundance', 'normal').

% Found in: EVE, pPRE
card_name('overbeing of myth', 'Overbeing of Myth').
card_type('overbeing of myth', 'Creature — Spirit Avatar').
card_types('overbeing of myth', ['Creature']).
card_subtypes('overbeing of myth', ['Spirit', 'Avatar']).
card_colors('overbeing of myth', ['U', 'G']).
card_text('overbeing of myth', 'Overbeing of Myth\'s power and toughness are each equal to the number of cards in your hand.\nAt the beginning of your draw step, draw an additional card.').
card_mana_cost('overbeing of myth', ['G/U', 'G/U', 'G/U', 'G/U', 'G/U']).
card_cmc('overbeing of myth', 5).
card_layout('overbeing of myth', 'normal').
card_power('overbeing of myth', '*').
card_toughness('overbeing of myth', '*').

% Found in: BOK
card_name('overblaze', 'Overblaze').
card_type('overblaze', 'Instant — Arcane').
card_types('overblaze', ['Instant']).
card_subtypes('overblaze', ['Arcane']).
card_colors('overblaze', ['R']).
card_text('overblaze', 'Each time target permanent would deal damage to a creature or player this turn, it deals double that damage to that creature or player instead.\nSplice onto Arcane {2}{R}{R} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('overblaze', ['3', 'R']).
card_cmc('overblaze', 4).
card_layout('overblaze', 'normal').

% Found in: PCY
card_name('overburden', 'Overburden').
card_type('overburden', 'Enchantment').
card_types('overburden', ['Enchantment']).
card_subtypes('overburden', []).
card_colors('overburden', ['U']).
card_text('overburden', 'Whenever a player puts a nontoken creature onto the battlefield, that player returns a land he or she controls to its owner\'s hand.').
card_mana_cost('overburden', ['1', 'U']).
card_cmc('overburden', 2).
card_layout('overburden', 'normal').

% Found in: DD3_DVD, DDC, ODY
card_name('overeager apprentice', 'Overeager Apprentice').
card_type('overeager apprentice', 'Creature — Human Minion').
card_types('overeager apprentice', ['Creature']).
card_subtypes('overeager apprentice', ['Human', 'Minion']).
card_colors('overeager apprentice', ['B']).
card_text('overeager apprentice', 'Discard a card, Sacrifice Overeager Apprentice: Add {B}{B}{B} to your mana pool.').
card_mana_cost('overeager apprentice', ['2', 'B']).
card_cmc('overeager apprentice', 3).
card_layout('overeager apprentice', 'normal').
card_power('overeager apprentice', 1).
card_toughness('overeager apprentice', 2).

% Found in: ROE
card_name('overgrown battlement', 'Overgrown Battlement').
card_type('overgrown battlement', 'Creature — Wall').
card_types('overgrown battlement', ['Creature']).
card_subtypes('overgrown battlement', ['Wall']).
card_colors('overgrown battlement', ['G']).
card_text('overgrown battlement', 'Defender\n{T}: Add {G} to your mana pool for each creature with defender you control.').
card_mana_cost('overgrown battlement', ['1', 'G']).
card_cmc('overgrown battlement', 2).
card_layout('overgrown battlement', 'normal').
card_power('overgrown battlement', 0).
card_toughness('overgrown battlement', 4).

% Found in: APC
card_name('overgrown estate', 'Overgrown Estate').
card_type('overgrown estate', 'Enchantment').
card_types('overgrown estate', ['Enchantment']).
card_subtypes('overgrown estate', []).
card_colors('overgrown estate', ['W', 'B', 'G']).
card_text('overgrown estate', 'Sacrifice a land: You gain 3 life.').
card_mana_cost('overgrown estate', ['B', 'G', 'W']).
card_cmc('overgrown estate', 3).
card_layout('overgrown estate', 'normal').

% Found in: EXP, RAV, RTR
card_name('overgrown tomb', 'Overgrown Tomb').
card_type('overgrown tomb', 'Land — Swamp Forest').
card_types('overgrown tomb', ['Land']).
card_subtypes('overgrown tomb', ['Swamp', 'Forest']).
card_colors('overgrown tomb', []).
card_text('overgrown tomb', '({T}: Add {B} or {G} to your mana pool.)\nAs Overgrown Tomb enters the battlefield, you may pay 2 life. If you don\'t, Overgrown Tomb enters the battlefield tapped.').
card_layout('overgrown tomb', 'normal').

% Found in: 10E, 9ED, STH
card_name('overgrowth', 'Overgrowth').
card_type('overgrowth', 'Enchantment — Aura').
card_types('overgrowth', ['Enchantment']).
card_subtypes('overgrowth', ['Aura']).
card_colors('overgrowth', ['G']).
card_text('overgrowth', 'Enchant land (Target a land as you cast this. This card enters the battlefield attached to that land.)\nWhenever enchanted land is tapped for mana, its controller adds {G}{G} to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('overgrowth', ['2', 'G']).
card_cmc('overgrowth', 3).
card_layout('overgrowth', 'normal').

% Found in: NMS
card_name('overlaid terrain', 'Overlaid Terrain').
card_type('overlaid terrain', 'Enchantment').
card_types('overlaid terrain', ['Enchantment']).
card_subtypes('overlaid terrain', []).
card_colors('overlaid terrain', ['G']).
card_text('overlaid terrain', 'As Overlaid Terrain enters the battlefield, sacrifice all lands you control.\nLands you control have \"{T}: Add two mana of any one color to your mana pool.\"').
card_mana_cost('overlaid terrain', ['2', 'G', 'G']).
card_cmc('overlaid terrain', 4).
card_layout('overlaid terrain', 'normal').

% Found in: INV
card_name('overload', 'Overload').
card_type('overload', 'Instant').
card_types('overload', ['Instant']).
card_subtypes('overload', []).
card_colors('overload', ['R']).
card_text('overload', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nDestroy target artifact if its converted mana cost is 2 or less. If Overload was kicked, destroy that artifact if its converted mana cost is 5 or less instead.').
card_mana_cost('overload', ['R']).
card_cmc('overload', 1).
card_layout('overload', 'normal').

% Found in: TOR
card_name('overmaster', 'Overmaster').
card_type('overmaster', 'Sorcery').
card_types('overmaster', ['Sorcery']).
card_subtypes('overmaster', []).
card_colors('overmaster', ['R']).
card_text('overmaster', 'The next instant or sorcery spell you cast this turn can\'t be countered by spells or abilities.\nDraw a card.').
card_mana_cost('overmaster', ['R']).
card_cmc('overmaster', 1).
card_layout('overmaster', 'normal').

% Found in: MRD
card_name('override', 'Override').
card_type('override', 'Instant').
card_types('override', ['Instant']).
card_subtypes('override', []).
card_colors('override', ['U']).
card_text('override', 'Counter target spell unless its controller pays {1} for each artifact you control.').
card_mana_cost('override', ['2', 'U']).
card_cmc('override', 3).
card_layout('override', 'normal').

% Found in: DDI, DIS
card_name('overrule', 'Overrule').
card_type('overrule', 'Instant').
card_types('overrule', ['Instant']).
card_subtypes('overrule', []).
card_colors('overrule', ['W', 'U']).
card_text('overrule', 'Counter target spell unless its controller pays {X}. You gain X life.').
card_mana_cost('overrule', ['X', 'W', 'U']).
card_cmc('overrule', 2).
card_layout('overrule', 'normal').

% Found in: 10E, ATH, C14, DD3_GVL, DDD, DPA, M10, M12, ODY, PC2, TMP, TPR
card_name('overrun', 'Overrun').
card_type('overrun', 'Sorcery').
card_types('overrun', ['Sorcery']).
card_subtypes('overrun', []).
card_colors('overrun', ['G']).
card_text('overrun', 'Creatures you control get +3/+3 and gain trample until end of turn.').
card_mana_cost('overrun', ['2', 'G', 'G', 'G']).
card_cmc('overrun', 5).
card_layout('overrun', 'normal').

% Found in: C14
card_name('overseer of the damned', 'Overseer of the Damned').
card_type('overseer of the damned', 'Creature — Demon').
card_types('overseer of the damned', ['Creature']).
card_subtypes('overseer of the damned', ['Demon']).
card_colors('overseer of the damned', ['B']).
card_text('overseer of the damned', 'Flying\nWhen Overseer of the Damned enters the battlefield, you may destroy target creature.\nWhenever a nontoken creature an opponent controls dies, put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_mana_cost('overseer of the damned', ['5', 'B', 'B']).
card_cmc('overseer of the damned', 7).
card_layout('overseer of the damned', 'normal').
card_power('overseer of the damned', 5).
card_toughness('overseer of the damned', 5).

% Found in: ONS
card_name('oversold cemetery', 'Oversold Cemetery').
card_type('oversold cemetery', 'Enchantment').
card_types('oversold cemetery', ['Enchantment']).
card_subtypes('oversold cemetery', []).
card_colors('oversold cemetery', ['B']).
card_text('oversold cemetery', 'At the beginning of your upkeep, if you have four or more creature cards in your graveyard, you may return target creature card from your graveyard to your hand.').
card_mana_cost('oversold cemetery', ['1', 'B']).
card_cmc('oversold cemetery', 2).
card_layout('oversold cemetery', 'normal').

% Found in: SHM
card_name('oversoul of dusk', 'Oversoul of Dusk').
card_type('oversoul of dusk', 'Creature — Spirit Avatar').
card_types('oversoul of dusk', ['Creature']).
card_subtypes('oversoul of dusk', ['Spirit', 'Avatar']).
card_colors('oversoul of dusk', ['W', 'G']).
card_text('oversoul of dusk', 'Protection from blue, from black, and from red').
card_mana_cost('oversoul of dusk', ['G/W', 'G/W', 'G/W', 'G/W', 'G/W']).
card_cmc('oversoul of dusk', 5).
card_layout('oversoul of dusk', 'normal').
card_power('oversoul of dusk', 5).
card_toughness('oversoul of dusk', 5).

% Found in: MMQ, pPRE
card_name('overtaker', 'Overtaker').
card_type('overtaker', 'Creature — Merfolk Spellshaper').
card_types('overtaker', ['Creature']).
card_subtypes('overtaker', ['Merfolk', 'Spellshaper']).
card_colors('overtaker', ['U']).
card_text('overtaker', '{3}{U}, {T}, Discard a card: Untap target creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_mana_cost('overtaker', ['1', 'U']).
card_cmc('overtaker', 2).
card_layout('overtaker', 'normal').
card_power('overtaker', 1).
card_toughness('overtaker', 1).

% Found in: M15, MM2, RAV
card_name('overwhelm', 'Overwhelm').
card_type('overwhelm', 'Sorcery').
card_types('overwhelm', ['Sorcery']).
card_subtypes('overwhelm', []).
card_colors('overwhelm', ['G']).
card_text('overwhelm', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nCreatures you control get +3/+3 until end of turn.').
card_mana_cost('overwhelm', ['5', 'G', 'G']).
card_cmc('overwhelm', 7).
card_layout('overwhelm', 'normal').

% Found in: ME4, PTK, pJGP
card_name('overwhelming forces', 'Overwhelming Forces').
card_type('overwhelming forces', 'Sorcery').
card_types('overwhelming forces', ['Sorcery']).
card_subtypes('overwhelming forces', []).
card_colors('overwhelming forces', ['B']).
card_text('overwhelming forces', 'Destroy all creatures target opponent controls. Draw a card for each creature destroyed this way.').
card_mana_cost('overwhelming forces', ['6', 'B', 'B']).
card_cmc('overwhelming forces', 8).
card_layout('overwhelming forces', 'normal').

% Found in: ONS
card_name('overwhelming instinct', 'Overwhelming Instinct').
card_type('overwhelming instinct', 'Enchantment').
card_types('overwhelming instinct', ['Enchantment']).
card_subtypes('overwhelming instinct', []).
card_colors('overwhelming instinct', ['G']).
card_text('overwhelming instinct', 'Whenever you attack with three or more creatures, draw a card.').
card_mana_cost('overwhelming instinct', ['2', 'G']).
card_cmc('overwhelming instinct', 3).
card_layout('overwhelming instinct', 'normal').

% Found in: DDJ, SOK
card_name('overwhelming intellect', 'Overwhelming Intellect').
card_type('overwhelming intellect', 'Instant').
card_types('overwhelming intellect', ['Instant']).
card_subtypes('overwhelming intellect', []).
card_colors('overwhelming intellect', ['U']).
card_text('overwhelming intellect', 'Counter target creature spell. Draw cards equal to that spell\'s converted mana cost.').
card_mana_cost('overwhelming intellect', ['4', 'U', 'U']).
card_cmc('overwhelming intellect', 6).
card_layout('overwhelming intellect', 'normal').

% Found in: C14, M11, MM2
card_name('overwhelming stampede', 'Overwhelming Stampede').
card_type('overwhelming stampede', 'Sorcery').
card_types('overwhelming stampede', ['Sorcery']).
card_subtypes('overwhelming stampede', []).
card_colors('overwhelming stampede', ['G']).
card_text('overwhelming stampede', 'Until end of turn, creatures you control gain trample and get +X/+X, where X is the greatest power among creatures you control.').
card_mana_cost('overwhelming stampede', ['3', 'G', 'G']).
card_cmc('overwhelming stampede', 5).
card_layout('overwhelming stampede', 'normal').

% Found in: PLC
card_name('ovinize', 'Ovinize').
card_type('ovinize', 'Instant').
card_types('ovinize', ['Instant']).
card_subtypes('ovinize', []).
card_colors('ovinize', ['U']).
card_text('ovinize', 'Until end of turn, target creature loses all abilities and has base power and toughness 0/1.').
card_mana_cost('ovinize', ['1', 'U']).
card_cmc('ovinize', 2).
card_layout('ovinize', 'normal').

% Found in: MGB, TSB, VIS
card_name('ovinomancer', 'Ovinomancer').
card_type('ovinomancer', 'Creature — Human Wizard').
card_types('ovinomancer', ['Creature']).
card_subtypes('ovinomancer', ['Human', 'Wizard']).
card_colors('ovinomancer', ['U']).
card_text('ovinomancer', 'When Ovinomancer enters the battlefield, sacrifice it unless you return three basic lands you control to their owner\'s hand.\n{T}, Return Ovinomancer to its owner\'s hand: Destroy target creature. It can\'t be regenerated. That creature\'s controller puts a 0/1 green Sheep creature token onto the battlefield.').
card_mana_cost('ovinomancer', ['2', 'U']).
card_cmc('ovinomancer', 3).
card_layout('ovinomancer', 'normal').
card_power('ovinomancer', 0).
card_toughness('ovinomancer', 1).

% Found in: UGL
card_name('ow', 'Ow').
card_type('ow', 'Enchantment').
card_types('ow', ['Enchantment']).
card_subtypes('ow', []).
card_colors('ow', ['B']).
card_text('ow', 'Whenever any creature damages a player, for each Ow card in play, that player says \"Ow\" once or Ow deals 1 damage to him or her.').
card_mana_cost('ow', ['B']).
card_cmc('ow', 1).
card_layout('ow', 'normal').

% Found in: ME4, POR, S99, VMA
card_name('owl familiar', 'Owl Familiar').
card_type('owl familiar', 'Creature — Bird').
card_types('owl familiar', ['Creature']).
card_subtypes('owl familiar', ['Bird']).
card_colors('owl familiar', ['U']).
card_text('owl familiar', 'Flying\nWhen Owl Familiar enters the battlefield, draw a card, then discard a card.').
card_mana_cost('owl familiar', ['1', 'U']).
card_cmc('owl familiar', 2).
card_layout('owl familiar', 'normal').
card_power('owl familiar', 1).
card_toughness('owl familiar', 1).

% Found in: SOM
card_name('oxidda daredevil', 'Oxidda Daredevil').
card_type('oxidda daredevil', 'Creature — Goblin Artificer').
card_types('oxidda daredevil', ['Creature']).
card_subtypes('oxidda daredevil', ['Goblin', 'Artificer']).
card_colors('oxidda daredevil', ['R']).
card_text('oxidda daredevil', 'Sacrifice an artifact: Oxidda Daredevil gains haste until end of turn.').
card_mana_cost('oxidda daredevil', ['1', 'R']).
card_cmc('oxidda daredevil', 2).
card_layout('oxidda daredevil', 'normal').
card_power('oxidda daredevil', 2).
card_toughness('oxidda daredevil', 1).

% Found in: DD2, DD3_JVC, DST
card_name('oxidda golem', 'Oxidda Golem').
card_type('oxidda golem', 'Artifact Creature — Golem').
card_types('oxidda golem', ['Artifact', 'Creature']).
card_subtypes('oxidda golem', ['Golem']).
card_colors('oxidda golem', []).
card_text('oxidda golem', 'Affinity for Mountains (This spell costs {1} less to cast for each Mountain you control.)\nHaste').
card_mana_cost('oxidda golem', ['6']).
card_cmc('oxidda golem', 6).
card_layout('oxidda golem', 'normal').
card_power('oxidda golem', 3).
card_toughness('oxidda golem', 2).

% Found in: SOM
card_name('oxidda scrapmelter', 'Oxidda Scrapmelter').
card_type('oxidda scrapmelter', 'Creature — Beast').
card_types('oxidda scrapmelter', ['Creature']).
card_subtypes('oxidda scrapmelter', ['Beast']).
card_colors('oxidda scrapmelter', ['R']).
card_text('oxidda scrapmelter', 'When Oxidda Scrapmelter enters the battlefield, destroy target artifact.').
card_mana_cost('oxidda scrapmelter', ['3', 'R']).
card_cmc('oxidda scrapmelter', 4).
card_layout('oxidda scrapmelter', 'normal').
card_power('oxidda scrapmelter', 3).
card_toughness('oxidda scrapmelter', 3).

% Found in: DST, pMPR
card_name('oxidize', 'Oxidize').
card_type('oxidize', 'Instant').
card_types('oxidize', ['Instant']).
card_subtypes('oxidize', []).
card_colors('oxidize', ['G']).
card_text('oxidize', 'Destroy target artifact. It can\'t be regenerated.').
card_mana_cost('oxidize', ['G']).
card_cmc('oxidize', 1).
card_layout('oxidize', 'normal').

% Found in: BOK
card_name('oyobi, who split the heavens', 'Oyobi, Who Split the Heavens').
card_type('oyobi, who split the heavens', 'Legendary Creature — Spirit').
card_types('oyobi, who split the heavens', ['Creature']).
card_subtypes('oyobi, who split the heavens', ['Spirit']).
card_supertypes('oyobi, who split the heavens', ['Legendary']).
card_colors('oyobi, who split the heavens', ['W']).
card_text('oyobi, who split the heavens', 'Flying\nWhenever you cast a Spirit or Arcane spell, put a 3/3 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('oyobi, who split the heavens', ['6', 'W']).
card_cmc('oyobi, who split the heavens', 7).
card_layout('oyobi, who split the heavens', 'normal').
card_power('oyobi, who split the heavens', 3).
card_toughness('oyobi, who split the heavens', 6).

