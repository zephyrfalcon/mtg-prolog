% Card-specific data

% Found in: MRD
card_name('wail of the nim', 'Wail of the Nim').
card_type('wail of the nim', 'Instant').
card_types('wail of the nim', ['Instant']).
card_subtypes('wail of the nim', []).
card_colors('wail of the nim', ['B']).
card_text('wail of the nim', 'Choose one —\n• Regenerate each creature you control.\n• Wail of the Nim deals 1 damage to each creature and each player.\nEntwine {B} (Choose both if you pay the entwine cost.)').
card_mana_cost('wail of the nim', ['2', 'B']).
card_cmc('wail of the nim', 3).
card_layout('wail of the nim', 'normal').

% Found in: 6ED, MIR
card_name('waiting in the weeds', 'Waiting in the Weeds').
card_type('waiting in the weeds', 'Sorcery').
card_types('waiting in the weeds', ['Sorcery']).
card_subtypes('waiting in the weeds', []).
card_colors('waiting in the weeds', ['G']).
card_text('waiting in the weeds', 'Each player puts a 1/1 green Cat creature token onto the battlefield for each untapped Forest he or she controls.').
card_mana_cost('waiting in the weeds', ['1', 'G', 'G']).
card_cmc('waiting in the weeds', 3).
card_layout('waiting in the weeds', 'normal').

% Found in: UDS
card_name('wake of destruction', 'Wake of Destruction').
card_type('wake of destruction', 'Sorcery').
card_types('wake of destruction', ['Sorcery']).
card_subtypes('wake of destruction', []).
card_colors('wake of destruction', ['R']).
card_text('wake of destruction', 'Destroy target land and all other lands with the same name as that land.').
card_mana_cost('wake of destruction', ['3', 'R', 'R', 'R']).
card_cmc('wake of destruction', 6).
card_layout('wake of destruction', 'normal').

% Found in: VIS
card_name('wake of vultures', 'Wake of Vultures').
card_type('wake of vultures', 'Creature — Bird').
card_types('wake of vultures', ['Creature']).
card_subtypes('wake of vultures', ['Bird']).
card_colors('wake of vultures', ['B']).
card_text('wake of vultures', 'Flying\n{1}{B}, Sacrifice a creature: Regenerate Wake of Vultures.').
card_mana_cost('wake of vultures', ['3', 'B']).
card_cmc('wake of vultures', 4).
card_layout('wake of vultures', 'normal').
card_power('wake of vultures', 3).
card_toughness('wake of vultures', 1).

% Found in: C14
card_name('wake the dead', 'Wake the Dead').
card_type('wake the dead', 'Instant').
card_types('wake the dead', ['Instant']).
card_subtypes('wake the dead', []).
card_colors('wake the dead', ['B']).
card_text('wake the dead', 'Cast Wake the Dead only during combat on an opponent\'s turn.\nReturn X target creature cards from your graveyard to the battlefield. Sacrifice those creatures at the beginning of the next end step.').
card_mana_cost('wake the dead', ['X', 'B', 'B']).
card_cmc('wake the dead', 2).
card_layout('wake the dead', 'normal').

% Found in: DGM
card_name('wake the reflections', 'Wake the Reflections').
card_type('wake the reflections', 'Sorcery').
card_types('wake the reflections', ['Sorcery']).
card_subtypes('wake the reflections', []).
card_colors('wake the reflections', ['W']).
card_text('wake the reflections', 'Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('wake the reflections', ['W']).
card_cmc('wake the reflections', 1).
card_layout('wake the reflections', 'normal').

% Found in: EVE
card_name('wake thrasher', 'Wake Thrasher').
card_type('wake thrasher', 'Creature — Merfolk Soldier').
card_types('wake thrasher', ['Creature']).
card_subtypes('wake thrasher', ['Merfolk', 'Soldier']).
card_colors('wake thrasher', ['U']).
card_text('wake thrasher', 'Whenever a permanent you control becomes untapped, Wake Thrasher gets +1/+1 until end of turn.').
card_mana_cost('wake thrasher', ['2', 'U']).
card_cmc('wake thrasher', 3).
card_layout('wake thrasher', 'normal').
card_power('wake thrasher', 1).
card_toughness('wake thrasher', 1).

% Found in: CNS, DKA
card_name('wakedancer', 'Wakedancer').
card_type('wakedancer', 'Creature — Human Shaman').
card_types('wakedancer', ['Creature']).
card_subtypes('wakedancer', ['Human', 'Shaman']).
card_colors('wakedancer', ['B']).
card_text('wakedancer', 'Morbid — When Wakedancer enters the battlefield, if a creature died this turn, put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('wakedancer', ['2', 'B']).
card_cmc('wakedancer', 3).
card_layout('wakedancer', 'normal').
card_power('wakedancer', 2).
card_toughness('wakedancer', 2).

% Found in: CNS, DIS
card_name('wakestone gargoyle', 'Wakestone Gargoyle').
card_type('wakestone gargoyle', 'Creature — Gargoyle').
card_types('wakestone gargoyle', ['Creature']).
card_subtypes('wakestone gargoyle', ['Gargoyle']).
card_colors('wakestone gargoyle', ['W']).
card_text('wakestone gargoyle', 'Defender, flying\n{1}{W}: Creatures you control with defender can attack this turn as though they didn\'t have defender.').
card_mana_cost('wakestone gargoyle', ['3', 'W']).
card_cmc('wakestone gargoyle', 4).
card_layout('wakestone gargoyle', 'normal').
card_power('wakestone gargoyle', 3).
card_toughness('wakestone gargoyle', 4).

% Found in: CHK, MM2
card_name('waking nightmare', 'Waking Nightmare').
card_type('waking nightmare', 'Sorcery — Arcane').
card_types('waking nightmare', ['Sorcery']).
card_subtypes('waking nightmare', ['Arcane']).
card_colors('waking nightmare', ['B']).
card_text('waking nightmare', 'Target player discards two cards.').
card_mana_cost('waking nightmare', ['2', 'B']).
card_cmc('waking nightmare', 3).
card_layout('waking nightmare', 'normal').

% Found in: TSP
card_name('walk the aeons', 'Walk the Aeons').
card_type('walk the aeons', 'Sorcery').
card_types('walk the aeons', ['Sorcery']).
card_subtypes('walk the aeons', []).
card_colors('walk the aeons', ['U']).
card_text('walk the aeons', 'Buyback—Sacrifice three Islands. (You may sacrifice three Islands in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget player takes an extra turn after this one.').
card_mana_cost('walk the aeons', ['4', 'U', 'U']).
card_cmc('walk the aeons', 6).
card_layout('walk the aeons', 'normal').

% Found in: BOK, PC2
card_name('walker of secret ways', 'Walker of Secret Ways').
card_type('walker of secret ways', 'Creature — Human Ninja').
card_types('walker of secret ways', ['Creature']).
card_subtypes('walker of secret ways', ['Human', 'Ninja']).
card_colors('walker of secret ways', ['U']).
card_text('walker of secret ways', 'Ninjutsu {1}{U} ({1}{U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Walker of Secret Ways deals combat damage to a player, look at that player\'s hand.\n{1}{U}: Return target Ninja you control to its owner\'s hand. Activate this ability only during your turn.').
card_mana_cost('walker of secret ways', ['2', 'U']).
card_cmc('walker of secret ways', 3).
card_layout('walker of secret ways', 'normal').
card_power('walker of secret ways', 1).
card_toughness('walker of secret ways', 2).

% Found in: C13, MMA, MOR
card_name('walker of the grove', 'Walker of the Grove').
card_type('walker of the grove', 'Creature — Elemental').
card_types('walker of the grove', ['Creature']).
card_subtypes('walker of the grove', ['Elemental']).
card_colors('walker of the grove', ['G']).
card_text('walker of the grove', 'When Walker of the Grove leaves the battlefield, put a 4/4 green Elemental creature token onto the battlefield.\nEvoke {4}{G} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('walker of the grove', ['6', 'G', 'G']).
card_cmc('walker of the grove', 8).
card_layout('walker of the grove', 'normal').
card_power('walker of the grove', 7).
card_toughness('walker of the grove', 7).

% Found in: DIS
card_name('walking archive', 'Walking Archive').
card_type('walking archive', 'Artifact Creature — Golem').
card_types('walking archive', ['Artifact', 'Creature']).
card_subtypes('walking archive', ['Golem']).
card_colors('walking archive', []).
card_text('walking archive', 'Defender (This creature can\'t attack.)\nWalking Archive enters the battlefield with a +1/+1 counter on it.\nAt the beginning of each player\'s upkeep, that player draws a card for each +1/+1 counter on Walking Archive.\n{2}{W}{U}: Put a +1/+1 counter on Walking Archive.').
card_mana_cost('walking archive', ['3']).
card_cmc('walking archive', 3).
card_layout('walking archive', 'normal').
card_power('walking archive', 1).
card_toughness('walking archive', 1).

% Found in: WWK
card_name('walking atlas', 'Walking Atlas').
card_type('walking atlas', 'Artifact Creature — Construct').
card_types('walking atlas', ['Artifact', 'Creature']).
card_subtypes('walking atlas', ['Construct']).
card_colors('walking atlas', []).
card_text('walking atlas', '{T}: You may put a land card from your hand onto the battlefield.').
card_mana_cost('walking atlas', ['2']).
card_cmc('walking atlas', 2).
card_layout('walking atlas', 'normal').
card_power('walking atlas', 1).
card_toughness('walking atlas', 1).

% Found in: ISD, M13, M15
card_name('walking corpse', 'Walking Corpse').
card_type('walking corpse', 'Creature — Zombie').
card_types('walking corpse', ['Creature']).
card_subtypes('walking corpse', ['Zombie']).
card_colors('walking corpse', ['B']).
card_text('walking corpse', '').
card_mana_cost('walking corpse', ['1', 'B']).
card_cmc('walking corpse', 2).
card_layout('walking corpse', 'normal').
card_power('walking corpse', 2).
card_toughness('walking corpse', 2).

% Found in: LEG
card_name('walking dead', 'Walking Dead').
card_type('walking dead', 'Creature — Zombie').
card_types('walking dead', ['Creature']).
card_subtypes('walking dead', ['Zombie']).
card_colors('walking dead', ['B']).
card_text('walking dead', '{B}: Regenerate Walking Dead.').
card_mana_cost('walking dead', ['1', 'B']).
card_cmc('walking dead', 2).
card_layout('walking dead', 'normal').
card_power('walking dead', 1).
card_toughness('walking dead', 1).

% Found in: ONS
card_name('walking desecration', 'Walking Desecration').
card_type('walking desecration', 'Creature — Zombie').
card_types('walking desecration', ['Creature']).
card_subtypes('walking desecration', ['Zombie']).
card_colors('walking desecration', ['B']).
card_text('walking desecration', '{B}, {T}: Creatures of the creature type of your choice attack this turn if able.').
card_mana_cost('walking desecration', ['2', 'B']).
card_cmc('walking desecration', 3).
card_layout('walking desecration', 'normal').
card_power('walking desecration', 1).
card_toughness('walking desecration', 1).

% Found in: STH
card_name('walking dream', 'Walking Dream').
card_type('walking dream', 'Creature — Illusion').
card_types('walking dream', ['Creature']).
card_subtypes('walking dream', ['Illusion']).
card_colors('walking dream', ['U']).
card_text('walking dream', 'Walking Dream can\'t be blocked.\nWalking Dream doesn\'t untap during your untap step if an opponent controls two or more creatures.').
card_mana_cost('walking dream', ['3', 'U']).
card_cmc('walking dream', 4).
card_layout('walking dream', 'normal').
card_power('walking dream', 3).
card_toughness('walking dream', 3).

% Found in: ULG
card_name('walking sponge', 'Walking Sponge').
card_type('walking sponge', 'Creature — Sponge').
card_types('walking sponge', ['Creature']).
card_subtypes('walking sponge', ['Sponge']).
card_colors('walking sponge', ['U']).
card_text('walking sponge', '{T}: Target creature loses your choice of flying, first strike, or trample until end of turn.').
card_mana_cost('walking sponge', ['1', 'U']).
card_cmc('walking sponge', 2).
card_layout('walking sponge', 'normal').
card_power('walking sponge', 1).
card_toughness('walking sponge', 1).

% Found in: DKM, ICE, MED
card_name('walking wall', 'Walking Wall').
card_type('walking wall', 'Artifact Creature — Wall').
card_types('walking wall', ['Artifact', 'Creature']).
card_subtypes('walking wall', ['Wall']).
card_colors('walking wall', []).
card_text('walking wall', 'Defender\n{3}: Walking Wall gets +3/-1 until end of turn and can attack this turn as though it didn\'t have defender. Activate this ability only once each turn.').
card_mana_cost('walking wall', ['4']).
card_cmc('walking wall', 4).
card_layout('walking wall', 'normal').
card_power('walking wall', 0).
card_toughness('walking wall', 6).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB
card_name('wall of air', 'Wall of Air').
card_type('wall of air', 'Creature — Wall').
card_types('wall of air', ['Creature']).
card_subtypes('wall of air', ['Wall']).
card_colors('wall of air', ['U']).
card_text('wall of air', 'Defender, flying (This creature can\'t attack, and it can block creatures with flying.)').
card_mana_cost('wall of air', ['1', 'U', 'U']).
card_cmc('wall of air', 3).
card_layout('wall of air', 'normal').
card_power('wall of air', 1).
card_toughness('wall of air', 5).

% Found in: MRD
card_name('wall of blood', 'Wall of Blood').
card_type('wall of blood', 'Creature — Wall').
card_types('wall of blood', ['Creature']).
card_subtypes('wall of blood', ['Wall']).
card_colors('wall of blood', ['B']).
card_text('wall of blood', 'Defender (This creature can\'t attack.)\nPay 1 life: Wall of Blood gets +1/+1 until end of turn.').
card_mana_cost('wall of blood', ['2', 'B']).
card_cmc('wall of blood', 3).
card_layout('wall of blood', 'normal').
card_power('wall of blood', 0).
card_toughness('wall of blood', 2).

% Found in: PC2, STH, TPR, V13, pFNM
card_name('wall of blossoms', 'Wall of Blossoms').
card_type('wall of blossoms', 'Creature — Plant Wall').
card_types('wall of blossoms', ['Creature']).
card_subtypes('wall of blossoms', ['Plant', 'Wall']).
card_colors('wall of blossoms', ['G']).
card_text('wall of blossoms', 'Defender\nWhen Wall of Blossoms enters the battlefield, draw a card.').
card_mana_cost('wall of blossoms', ['1', 'G']).
card_cmc('wall of blossoms', 2).
card_layout('wall of blossoms', 'normal').
card_power('wall of blossoms', 0).
card_toughness('wall of blossoms', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, 7ED, CED, CEI, DD3_GVL, DDD, ITP, LEA, LEB, M10, RQS
card_name('wall of bone', 'Wall of Bone').
card_type('wall of bone', 'Creature — Skeleton Wall').
card_types('wall of bone', ['Creature']).
card_subtypes('wall of bone', ['Skeleton', 'Wall']).
card_colors('wall of bone', ['B']).
card_text('wall of bone', 'Defender (This creature can\'t attack.)\n{B}: Regenerate Wall of Bone. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('wall of bone', ['2', 'B']).
card_cmc('wall of bone', 3).
card_layout('wall of bone', 'normal').
card_power('wall of bone', 1).
card_toughness('wall of bone', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('wall of brambles', 'Wall of Brambles').
card_type('wall of brambles', 'Creature — Plant Wall').
card_types('wall of brambles', ['Creature']).
card_subtypes('wall of brambles', ['Plant', 'Wall']).
card_colors('wall of brambles', ['G']).
card_text('wall of brambles', 'Defender (This creature can\'t attack.)\n{G}: Regenerate Wall of Brambles.').
card_mana_cost('wall of brambles', ['2', 'G']).
card_cmc('wall of brambles', 3).
card_layout('wall of brambles', 'normal').
card_power('wall of brambles', 2).
card_toughness('wall of brambles', 3).

% Found in: LEG
card_name('wall of caltrops', 'Wall of Caltrops').
card_type('wall of caltrops', 'Creature — Wall').
card_types('wall of caltrops', ['Creature']).
card_subtypes('wall of caltrops', ['Wall']).
card_colors('wall of caltrops', ['W']).
card_text('wall of caltrops', 'Defender (This creature can\'t attack.)\nWhenever Wall of Caltrops blocks a creature, if at least one other Wall creature is blocking that creature and no non-Wall creatures are blocking that creature, Wall of Caltrops gains banding until end of turn. (If any creatures with banding you control are blocking a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by.)').
card_mana_cost('wall of caltrops', ['1', 'W']).
card_cmc('wall of caltrops', 2).
card_layout('wall of caltrops', 'normal').
card_power('wall of caltrops', 2).
card_toughness('wall of caltrops', 1).

% Found in: MIR
card_name('wall of corpses', 'Wall of Corpses').
card_type('wall of corpses', 'Creature — Wall').
card_types('wall of corpses', ['Creature']).
card_subtypes('wall of corpses', ['Wall']).
card_colors('wall of corpses', ['B']).
card_text('wall of corpses', 'Defender (This creature can\'t attack.)\n{B}, Sacrifice Wall of Corpses: Destroy target creature Wall of Corpses is blocking.').
card_mana_cost('wall of corpses', ['1', 'B']).
card_cmc('wall of corpses', 2).
card_layout('wall of corpses', 'normal').
card_power('wall of corpses', 0).
card_toughness('wall of corpses', 2).

% Found in: DD2, DD3_JVC, LGN
card_name('wall of deceit', 'Wall of Deceit').
card_type('wall of deceit', 'Creature — Wall').
card_types('wall of deceit', ['Creature']).
card_subtypes('wall of deceit', ['Wall']).
card_colors('wall of deceit', ['U']).
card_text('wall of deceit', 'Defender (This creature can\'t attack.)\n{3}: Turn Wall of Deceit face down.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('wall of deceit', ['1', 'U']).
card_cmc('wall of deceit', 2).
card_layout('wall of deceit', 'normal').
card_power('wall of deceit', 0).
card_toughness('wall of deceit', 5).

% Found in: ARB, CMD, DDI
card_name('wall of denial', 'Wall of Denial').
card_type('wall of denial', 'Creature — Wall').
card_types('wall of denial', ['Creature']).
card_subtypes('wall of denial', ['Wall']).
card_colors('wall of denial', ['W', 'U']).
card_text('wall of denial', 'Defender, flying\nShroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('wall of denial', ['1', 'W', 'U']).
card_cmc('wall of denial', 3).
card_layout('wall of denial', 'normal').
card_power('wall of denial', 0).
card_toughness('wall of denial', 8).

% Found in: TMP, TPR, VMA
card_name('wall of diffusion', 'Wall of Diffusion').
card_type('wall of diffusion', 'Creature — Wall').
card_types('wall of diffusion', ['Creature']).
card_subtypes('wall of diffusion', ['Wall']).
card_colors('wall of diffusion', ['R']).
card_text('wall of diffusion', 'Defender (This creature can\'t attack.)\nWall of Diffusion can block creatures with shadow as though Wall of Diffusion had shadow.').
card_mana_cost('wall of diffusion', ['1', 'R']).
card_cmc('wall of diffusion', 2).
card_layout('wall of diffusion', 'normal').
card_power('wall of diffusion', 0).
card_toughness('wall of diffusion', 5).

% Found in: MMQ
card_name('wall of distortion', 'Wall of Distortion').
card_type('wall of distortion', 'Creature — Wall').
card_types('wall of distortion', ['Creature']).
card_subtypes('wall of distortion', ['Wall']).
card_colors('wall of distortion', ['B']).
card_text('wall of distortion', 'Defender (This creature can\'t attack.)\n{2}{B}, {T}: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('wall of distortion', ['2', 'B', 'B']).
card_cmc('wall of distortion', 4).
card_layout('wall of distortion', 'normal').
card_power('wall of distortion', 1).
card_toughness('wall of distortion', 3).

% Found in: 4ED, LEG
card_name('wall of dust', 'Wall of Dust').
card_type('wall of dust', 'Creature — Wall').
card_types('wall of dust', ['Creature']).
card_subtypes('wall of dust', ['Wall']).
card_colors('wall of dust', ['R']).
card_text('wall of dust', 'Defender (This creature can\'t attack.)\nWhenever Wall of Dust blocks a creature, that creature can\'t attack during its controller\'s next turn.').
card_mana_cost('wall of dust', ['2', 'R']).
card_cmc('wall of dust', 3).
card_layout('wall of dust', 'normal').
card_power('wall of dust', 1).
card_toughness('wall of dust', 4).

% Found in: LEG
card_name('wall of earth', 'Wall of Earth').
card_type('wall of earth', 'Creature — Wall').
card_types('wall of earth', ['Creature']).
card_subtypes('wall of earth', ['Wall']).
card_colors('wall of earth', ['R']).
card_text('wall of earth', 'Defender (This creature can\'t attack.)').
card_mana_cost('wall of earth', ['1', 'R']).
card_cmc('wall of earth', 2).
card_layout('wall of earth', 'normal').
card_power('wall of earth', 0).
card_toughness('wall of earth', 6).

% Found in: M15, STH, TPR
card_name('wall of essence', 'Wall of Essence').
card_type('wall of essence', 'Creature — Wall').
card_types('wall of essence', ['Creature']).
card_subtypes('wall of essence', ['Wall']).
card_colors('wall of essence', ['W']).
card_text('wall of essence', 'Defender (This creature can\'t attack.)\nWhenever Wall of Essence is dealt combat damage, you gain that much life.').
card_mana_cost('wall of essence', ['1', 'W']).
card_cmc('wall of essence', 2).
card_layout('wall of essence', 'normal').
card_power('wall of essence', 0).
card_toughness('wall of essence', 4).

% Found in: M10
card_name('wall of faith', 'Wall of Faith').
card_type('wall of faith', 'Creature — Wall').
card_types('wall of faith', ['Creature']).
card_subtypes('wall of faith', ['Wall']).
card_colors('wall of faith', ['W']).
card_text('wall of faith', 'Defender (This creature can\'t attack.)\n{W}: Wall of Faith gets +0/+1 until end of turn.').
card_mana_cost('wall of faith', ['3', 'W']).
card_cmc('wall of faith', 4).
card_layout('wall of faith', 'normal').
card_power('wall of faith', 0).
card_toughness('wall of faith', 5).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, LEA, LEB, M10, M13, M15
card_name('wall of fire', 'Wall of Fire').
card_type('wall of fire', 'Creature — Wall').
card_types('wall of fire', ['Creature']).
card_subtypes('wall of fire', ['Wall']).
card_colors('wall of fire', ['R']).
card_text('wall of fire', 'Defender (This creature can\'t attack.)\n{R}: Wall of Fire gets +1/+0 until end of turn.').
card_mana_cost('wall of fire', ['1', 'R', 'R']).
card_cmc('wall of fire', 3).
card_layout('wall of fire', 'normal').
card_power('wall of fire', 0).
card_toughness('wall of fire', 5).

% Found in: M10, M11, M14, M15, PC2
card_name('wall of frost', 'Wall of Frost').
card_type('wall of frost', 'Creature — Wall').
card_types('wall of frost', ['Creature']).
card_subtypes('wall of frost', ['Wall']).
card_colors('wall of frost', ['U']).
card_text('wall of frost', 'Defender (This creature can\'t attack.)\nWhenever Wall of Frost blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('wall of frost', ['1', 'U', 'U']).
card_cmc('wall of frost', 3).
card_layout('wall of frost', 'normal').
card_power('wall of frost', 0).
card_toughness('wall of frost', 7).

% Found in: UDS
card_name('wall of glare', 'Wall of Glare').
card_type('wall of glare', 'Creature — Wall').
card_types('wall of glare', ['Creature']).
card_subtypes('wall of glare', ['Wall']).
card_colors('wall of glare', ['W']).
card_text('wall of glare', 'Defender (This creature can\'t attack.)\nWall of Glare can block any number of creatures.').
card_mana_cost('wall of glare', ['1', 'W']).
card_cmc('wall of glare', 2).
card_layout('wall of glare', 'normal').
card_power('wall of glare', 0).
card_toughness('wall of glare', 5).

% Found in: POR
card_name('wall of granite', 'Wall of Granite').
card_type('wall of granite', 'Creature — Wall').
card_types('wall of granite', ['Creature']).
card_subtypes('wall of granite', ['Wall']).
card_colors('wall of granite', ['R']).
card_text('wall of granite', 'Defender (This creature can\'t attack.)').
card_mana_cost('wall of granite', ['2', 'R']).
card_cmc('wall of granite', 3).
card_layout('wall of granite', 'normal').
card_power('wall of granite', 0).
card_toughness('wall of granite', 7).

% Found in: BRB, CHR, LEG
card_name('wall of heat', 'Wall of Heat').
card_type('wall of heat', 'Creature — Wall').
card_types('wall of heat', ['Creature']).
card_subtypes('wall of heat', ['Wall']).
card_colors('wall of heat', ['R']).
card_text('wall of heat', 'Defender (This creature can\'t attack.)').
card_mana_cost('wall of heat', ['2', 'R']).
card_cmc('wall of heat', 3).
card_layout('wall of heat', 'normal').
card_power('wall of heat', 2).
card_toughness('wall of heat', 6).

% Found in: LGN
card_name('wall of hope', 'Wall of Hope').
card_type('wall of hope', 'Creature — Wall').
card_types('wall of hope', ['Creature']).
card_subtypes('wall of hope', ['Wall']).
card_colors('wall of hope', ['W']).
card_text('wall of hope', 'Defender (This creature can\'t attack.)\nWhenever Wall of Hope is dealt damage, you gain that much life.').
card_mana_cost('wall of hope', ['W']).
card_cmc('wall of hope', 1).
card_layout('wall of hope', 'normal').
card_power('wall of hope', 0).
card_toughness('wall of hope', 3).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('wall of ice', 'Wall of Ice').
card_type('wall of ice', 'Creature — Wall').
card_types('wall of ice', ['Creature']).
card_subtypes('wall of ice', ['Wall']).
card_colors('wall of ice', ['G']).
card_text('wall of ice', 'Defender (This creature can\'t attack.)').
card_mana_cost('wall of ice', ['2', 'G']).
card_cmc('wall of ice', 3).
card_layout('wall of ice', 'normal').
card_power('wall of ice', 0).
card_toughness('wall of ice', 7).

% Found in: USG
card_name('wall of junk', 'Wall of Junk').
card_type('wall of junk', 'Artifact Creature — Wall').
card_types('wall of junk', ['Artifact', 'Creature']).
card_subtypes('wall of junk', ['Wall']).
card_colors('wall of junk', []).
card_text('wall of junk', 'Defender (This creature can\'t attack.)\nWhen Wall of Junk blocks, return it to its owner\'s hand at end of combat. (Return it only if it\'s on the battlefield.)').
card_mana_cost('wall of junk', ['2']).
card_cmc('wall of junk', 2).
card_layout('wall of junk', 'normal').
card_power('wall of junk', 0).
card_toughness('wall of junk', 7).

% Found in: HML, ME2
card_name('wall of kelp', 'Wall of Kelp').
card_type('wall of kelp', 'Creature — Plant Wall').
card_types('wall of kelp', ['Creature']).
card_subtypes('wall of kelp', ['Plant', 'Wall']).
card_colors('wall of kelp', ['U']).
card_text('wall of kelp', 'Defender (This creature can\'t attack.)\n{U}{U}, {T}: Put a 0/1 blue Plant Wall creature token with defender named Kelp onto the battlefield.').
card_mana_cost('wall of kelp', ['U', 'U']).
card_cmc('wall of kelp', 2).
card_layout('wall of kelp', 'normal').
card_power('wall of kelp', 0).
card_toughness('wall of kelp', 3).
card_reserved('wall of kelp').

% Found in: ICE
card_name('wall of lava', 'Wall of Lava').
card_type('wall of lava', 'Creature — Wall').
card_types('wall of lava', ['Creature']).
card_subtypes('wall of lava', ['Wall']).
card_colors('wall of lava', ['R']).
card_text('wall of lava', 'Defender (This creature can\'t attack.)\n{R}: Wall of Lava gets +1/+1 until end of turn.').
card_mana_cost('wall of lava', ['1', 'R', 'R']).
card_cmc('wall of lava', 3).
card_layout('wall of lava', 'normal').
card_power('wall of lava', 1).
card_toughness('wall of lava', 3).

% Found in: LEG, ME3
card_name('wall of light', 'Wall of Light').
card_type('wall of light', 'Creature — Wall').
card_types('wall of light', ['Creature']).
card_subtypes('wall of light', ['Wall']).
card_colors('wall of light', ['W']).
card_text('wall of light', 'Defender (This creature can\'t attack.)\nProtection from black').
card_mana_cost('wall of light', ['2', 'W']).
card_cmc('wall of light', 3).
card_layout('wall of light', 'normal').
card_power('wall of light', 1).
card_toughness('wall of light', 5).

% Found in: M15
card_name('wall of limbs', 'Wall of Limbs').
card_type('wall of limbs', 'Creature — Zombie Wall').
card_types('wall of limbs', ['Creature']).
card_subtypes('wall of limbs', ['Zombie', 'Wall']).
card_colors('wall of limbs', ['B']).
card_text('wall of limbs', 'Defender (This creature can\'t attack.)\nWhenever you gain life, put a +1/+1 counter on Wall of Limbs.\n{5}{B}{B}, Sacrifice Wall of Limbs: Target player loses X life, where X is Wall of Limbs\'s power.').
card_mana_cost('wall of limbs', ['2', 'B']).
card_cmc('wall of limbs', 3).
card_layout('wall of limbs', 'normal').
card_power('wall of limbs', 0).
card_toughness('wall of limbs', 3).

% Found in: M15, ONS
card_name('wall of mulch', 'Wall of Mulch').
card_type('wall of mulch', 'Creature — Wall').
card_types('wall of mulch', ['Creature']).
card_subtypes('wall of mulch', ['Wall']).
card_colors('wall of mulch', ['G']).
card_text('wall of mulch', 'Defender (This creature can\'t attack.)\n{G}, Sacrifice a Wall: Draw a card.').
card_mana_cost('wall of mulch', ['1', 'G']).
card_cmc('wall of mulch', 2).
card_layout('wall of mulch', 'normal').
card_power('wall of mulch', 0).
card_toughness('wall of mulch', 4).

% Found in: EXO
card_name('wall of nets', 'Wall of Nets').
card_type('wall of nets', 'Creature — Wall').
card_types('wall of nets', ['Creature']).
card_subtypes('wall of nets', ['Wall']).
card_colors('wall of nets', ['W']).
card_text('wall of nets', 'Defender (This creature can\'t attack.)\nAt end of combat, exile all creatures blocked by Wall of Nets.\nWhen Wall of Nets leaves the battlefield, return all cards exiled with Wall of Nets to the battlefield under their owners\' control.').
card_mana_cost('wall of nets', ['1', 'W', 'W']).
card_cmc('wall of nets', 3).
card_layout('wall of nets', 'normal').
card_power('wall of nets', 0).
card_toughness('wall of nets', 7).

% Found in: CMD, DDK, ROE, pFNM
card_name('wall of omens', 'Wall of Omens').
card_type('wall of omens', 'Creature — Wall').
card_types('wall of omens', ['Creature']).
card_subtypes('wall of omens', ['Wall']).
card_colors('wall of omens', ['W']).
card_text('wall of omens', 'Defender\nWhen Wall of Omens enters the battlefield, draw a card.').
card_mana_cost('wall of omens', ['1', 'W']).
card_cmc('wall of omens', 2).
card_layout('wall of omens', 'normal').
card_power('wall of omens', 0).
card_toughness('wall of omens', 4).

% Found in: CHR, LEG
card_name('wall of opposition', 'Wall of Opposition').
card_type('wall of opposition', 'Creature — Wall').
card_types('wall of opposition', ['Creature']).
card_subtypes('wall of opposition', ['Wall']).
card_colors('wall of opposition', ['R']).
card_text('wall of opposition', 'Defender (This creature can\'t attack.)\n{1}: Wall of Opposition gets +1/+0 until end of turn.').
card_mana_cost('wall of opposition', ['3', 'R', 'R']).
card_cmc('wall of opposition', 5).
card_layout('wall of opposition', 'normal').
card_power('wall of opposition', 0).
card_toughness('wall of opposition', 6).

% Found in: ICE
card_name('wall of pine needles', 'Wall of Pine Needles').
card_type('wall of pine needles', 'Creature — Plant Wall').
card_types('wall of pine needles', ['Creature']).
card_subtypes('wall of pine needles', ['Plant', 'Wall']).
card_colors('wall of pine needles', ['G']).
card_text('wall of pine needles', 'Defender (This creature can\'t attack.)\n{G}: Regenerate Wall of Pine Needles.').
card_mana_cost('wall of pine needles', ['3', 'G']).
card_cmc('wall of pine needles', 4).
card_layout('wall of pine needles', 'normal').
card_power('wall of pine needles', 3).
card_toughness('wall of pine needles', 3).

% Found in: LEG
card_name('wall of putrid flesh', 'Wall of Putrid Flesh').
card_type('wall of putrid flesh', 'Creature — Wall').
card_types('wall of putrid flesh', ['Creature']).
card_subtypes('wall of putrid flesh', ['Wall']).
card_colors('wall of putrid flesh', ['B']).
card_text('wall of putrid flesh', 'Defender (This creature can\'t attack.)\nProtection from white\nPrevent all damage that would be dealt to Wall of Putrid Flesh by enchanted creatures.').
card_mana_cost('wall of putrid flesh', ['2', 'B']).
card_cmc('wall of putrid flesh', 3).
card_layout('wall of putrid flesh', 'normal').
card_power('wall of putrid flesh', 2).
card_toughness('wall of putrid flesh', 4).

% Found in: STH
card_name('wall of razors', 'Wall of Razors').
card_type('wall of razors', 'Creature — Wall').
card_types('wall of razors', ['Creature']).
card_subtypes('wall of razors', ['Wall']).
card_colors('wall of razors', ['R']).
card_text('wall of razors', 'Defender (This creature can\'t attack.)\nFirst strike').
card_mana_cost('wall of razors', ['1', 'R']).
card_cmc('wall of razors', 2).
card_layout('wall of razors', 'normal').
card_power('wall of razors', 4).
card_toughness('wall of razors', 1).

% Found in: MIR
card_name('wall of resistance', 'Wall of Resistance').
card_type('wall of resistance', 'Creature — Wall').
card_types('wall of resistance', ['Creature']).
card_subtypes('wall of resistance', ['Wall']).
card_colors('wall of resistance', ['W']).
card_text('wall of resistance', 'Defender (This creature can\'t attack.)\nFlying\nAt the beginning of each end step, if Wall of Resistance was dealt damage this turn, put a +0/+1 counter on it.').
card_mana_cost('wall of resistance', ['1', 'W']).
card_cmc('wall of resistance', 2).
card_layout('wall of resistance', 'normal').
card_power('wall of resistance', 0).
card_toughness('wall of resistance', 3).

% Found in: C13, CON
card_name('wall of reverence', 'Wall of Reverence').
card_type('wall of reverence', 'Creature — Spirit Wall').
card_types('wall of reverence', ['Creature']).
card_subtypes('wall of reverence', ['Spirit', 'Wall']).
card_colors('wall of reverence', ['W']).
card_text('wall of reverence', 'Defender, flying\nAt the beginning of your end step, you may gain life equal to the power of target creature you control.').
card_mana_cost('wall of reverence', ['3', 'W']).
card_cmc('wall of reverence', 4).
card_layout('wall of reverence', 'normal').
card_power('wall of reverence', 1).
card_toughness('wall of reverence', 6).

% Found in: ARC, MIR, TSB, pFNM
card_name('wall of roots', 'Wall of Roots').
card_type('wall of roots', 'Creature — Plant Wall').
card_types('wall of roots', ['Creature']).
card_subtypes('wall of roots', ['Plant', 'Wall']).
card_colors('wall of roots', ['G']).
card_text('wall of roots', 'Defender\nPut a -0/-1 counter on Wall of Roots: Add {G} to your mana pool. Activate this ability only once each turn.').
card_mana_cost('wall of roots', ['1', 'G']).
card_cmc('wall of roots', 2).
card_layout('wall of roots', 'normal').
card_power('wall of roots', 0).
card_toughness('wall of roots', 5).

% Found in: CHR, LEG
card_name('wall of shadows', 'Wall of Shadows').
card_type('wall of shadows', 'Creature — Wall').
card_types('wall of shadows', ['Creature']).
card_subtypes('wall of shadows', ['Wall']).
card_colors('wall of shadows', ['B']).
card_text('wall of shadows', 'Defender (This creature can\'t attack.)\nPrevent all damage that would be dealt to Wall of Shadows by creatures it\'s blocking.\nWall of Shadows can\'t be the target of spells that can target only Walls or of abilities that can target only Walls.').
card_mana_cost('wall of shadows', ['1', 'B', 'B']).
card_cmc('wall of shadows', 3).
card_layout('wall of shadows', 'normal').
card_power('wall of shadows', 0).
card_toughness('wall of shadows', 1).

% Found in: CSP
card_name('wall of shards', 'Wall of Shards').
card_type('wall of shards', 'Snow Creature — Wall').
card_types('wall of shards', ['Creature']).
card_subtypes('wall of shards', ['Wall']).
card_supertypes('wall of shards', ['Snow']).
card_colors('wall of shards', ['W']).
card_text('wall of shards', 'Defender, flying\nCumulative upkeep—An opponent gains 1 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('wall of shards', ['1', 'W']).
card_cmc('wall of shards', 2).
card_layout('wall of shards', 'normal').
card_power('wall of shards', 1).
card_toughness('wall of shards', 8).

% Found in: ICE
card_name('wall of shields', 'Wall of Shields').
card_type('wall of shields', 'Artifact Creature — Wall').
card_types('wall of shields', ['Artifact', 'Creature']).
card_subtypes('wall of shields', ['Wall']).
card_colors('wall of shields', []).
card_text('wall of shields', 'Defender (This creature can\'t attack.)\nBanding (If any creatures with banding you control are blocking a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by.)').
card_mana_cost('wall of shields', ['3']).
card_cmc('wall of shields', 3).
card_layout('wall of shields', 'normal').
card_power('wall of shields', 0).
card_toughness('wall of shields', 4).

% Found in: STH, TPR
card_name('wall of souls', 'Wall of Souls').
card_type('wall of souls', 'Creature — Wall').
card_types('wall of souls', ['Creature']).
card_subtypes('wall of souls', ['Wall']).
card_colors('wall of souls', ['B']).
card_text('wall of souls', 'Defender (This creature can\'t attack.)\nWhenever Wall of Souls is dealt combat damage, it deals that much damage to target opponent.').
card_mana_cost('wall of souls', ['1', 'B']).
card_cmc('wall of souls', 2).
card_layout('wall of souls', 'normal').
card_power('wall of souls', 0).
card_toughness('wall of souls', 4).

% Found in: 4ED, 5ED, 7ED, 8ED, ATQ, DPA
card_name('wall of spears', 'Wall of Spears').
card_type('wall of spears', 'Artifact Creature — Wall').
card_types('wall of spears', ['Artifact', 'Creature']).
card_subtypes('wall of spears', ['Wall']).
card_colors('wall of spears', []).
card_text('wall of spears', 'Defender (This creature can\'t attack.)\nFirst strike').
card_mana_cost('wall of spears', ['3']).
card_cmc('wall of spears', 3).
card_layout('wall of spears', 'normal').
card_power('wall of spears', 2).
card_toughness('wall of spears', 3).

% Found in: 2ED, 3ED, 4ED, 5ED, 8ED, CED, CEI, LEA, LEB
card_name('wall of stone', 'Wall of Stone').
card_type('wall of stone', 'Creature — Wall').
card_types('wall of stone', ['Creature']).
card_subtypes('wall of stone', ['Wall']).
card_colors('wall of stone', ['R']).
card_text('wall of stone', 'Defender (This creature can\'t attack.)').
card_mana_cost('wall of stone', ['1', 'R', 'R']).
card_cmc('wall of stone', 3).
card_layout('wall of stone', 'normal').
card_power('wall of stone', 0).
card_toughness('wall of stone', 8).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB, M14, POR
card_name('wall of swords', 'Wall of Swords').
card_type('wall of swords', 'Creature — Wall').
card_types('wall of swords', ['Creature']).
card_subtypes('wall of swords', ['Wall']).
card_colors('wall of swords', ['W']).
card_text('wall of swords', 'Defender (This creature can\'t attack.)\nFlying').
card_mana_cost('wall of swords', ['3', 'W']).
card_cmc('wall of swords', 4).
card_layout('wall of swords', 'normal').
card_power('wall of swords', 3).
card_toughness('wall of swords', 5).

% Found in: SOM
card_name('wall of tanglecord', 'Wall of Tanglecord').
card_type('wall of tanglecord', 'Artifact Creature — Wall').
card_types('wall of tanglecord', ['Artifact', 'Creature']).
card_subtypes('wall of tanglecord', ['Wall']).
card_colors('wall of tanglecord', []).
card_text('wall of tanglecord', 'Defender\n{G}: Wall of Tanglecord gains reach until end of turn. (It can block creatures with flying.)').
card_mana_cost('wall of tanglecord', ['2']).
card_cmc('wall of tanglecord', 2).
card_layout('wall of tanglecord', 'normal').
card_power('wall of tanglecord', 0).
card_toughness('wall of tanglecord', 6).

% Found in: STH
card_name('wall of tears', 'Wall of Tears').
card_type('wall of tears', 'Creature — Wall').
card_types('wall of tears', ['Creature']).
card_subtypes('wall of tears', ['Wall']).
card_colors('wall of tears', ['U']).
card_text('wall of tears', 'Defender (This creature can\'t attack.)\nWhenever Wall of Tears blocks a creature, return that creature to its owner\'s hand at end of combat.').
card_mana_cost('wall of tears', ['1', 'U']).
card_cmc('wall of tears', 2).
card_layout('wall of tears', 'normal').
card_power('wall of tears', 0).
card_toughness('wall of tears', 4).

% Found in: LEG
card_name('wall of tombstones', 'Wall of Tombstones').
card_type('wall of tombstones', 'Creature — Wall').
card_types('wall of tombstones', ['Creature']).
card_subtypes('wall of tombstones', ['Wall']).
card_colors('wall of tombstones', ['B']).
card_text('wall of tombstones', 'Defender (This creature can\'t attack.)\nAt the beginning of your upkeep, change Wall of Tombstones\'s base toughness to 1 plus the number of creature cards in your graveyard. (This effect lasts indefinitely.)').
card_mana_cost('wall of tombstones', ['1', 'B']).
card_cmc('wall of tombstones', 2).
card_layout('wall of tombstones', 'normal').
card_power('wall of tombstones', 0).
card_toughness('wall of tombstones', 1).

% Found in: M12
card_name('wall of torches', 'Wall of Torches').
card_type('wall of torches', 'Creature — Wall').
card_types('wall of torches', ['Creature']).
card_subtypes('wall of torches', ['Wall']).
card_colors('wall of torches', ['R']).
card_text('wall of torches', 'Defender (This creature can\'t attack.)').
card_mana_cost('wall of torches', ['1', 'R']).
card_cmc('wall of torches', 2).
card_layout('wall of torches', 'normal').
card_power('wall of torches', 4).
card_toughness('wall of torches', 1).

% Found in: CHR, LEG
card_name('wall of vapor', 'Wall of Vapor').
card_type('wall of vapor', 'Creature — Wall').
card_types('wall of vapor', ['Creature']).
card_subtypes('wall of vapor', ['Wall']).
card_colors('wall of vapor', ['U']).
card_text('wall of vapor', 'Defender (This creature can\'t attack.)\nPrevent all damage that would be dealt to Wall of Vapor by creatures it\'s blocking.').
card_mana_cost('wall of vapor', ['3', 'U']).
card_cmc('wall of vapor', 4).
card_layout('wall of vapor', 'normal').
card_power('wall of vapor', 0).
card_toughness('wall of vapor', 1).

% Found in: M11
card_name('wall of vines', 'Wall of Vines').
card_type('wall of vines', 'Creature — Plant Wall').
card_types('wall of vines', ['Creature']).
card_subtypes('wall of vines', ['Plant', 'Wall']).
card_colors('wall of vines', ['G']).
card_text('wall of vines', 'Defender (This creature can\'t attack.)\nReach (This creature can block creatures with flying.)').
card_mana_cost('wall of vines', ['G']).
card_cmc('wall of vines', 1).
card_layout('wall of vines', 'normal').
card_power('wall of vines', 0).
card_toughness('wall of vines', 3).

% Found in: PCY
card_name('wall of vipers', 'Wall of Vipers').
card_type('wall of vipers', 'Creature — Snake Wall').
card_types('wall of vipers', ['Creature']).
card_subtypes('wall of vipers', ['Snake', 'Wall']).
card_colors('wall of vipers', ['B']).
card_text('wall of vipers', 'Defender (This creature can\'t attack.)\n{3}: Destroy Wall of Vipers and target creature it\'s blocking. Any player may activate this ability.').
card_mana_cost('wall of vipers', ['2', 'B']).
card_cmc('wall of vipers', 3).
card_layout('wall of vipers', 'normal').
card_power('wall of vipers', 2).
card_toughness('wall of vipers', 4).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('wall of water', 'Wall of Water').
card_type('wall of water', 'Creature — Wall').
card_types('wall of water', ['Creature']).
card_subtypes('wall of water', ['Wall']).
card_colors('wall of water', ['U']).
card_text('wall of water', 'Defender (This creature can\'t attack.)\n{U}: Wall of Water gets +1/+0 until end of turn.').
card_mana_cost('wall of water', ['1', 'U', 'U']).
card_cmc('wall of water', 3).
card_layout('wall of water', 'normal').
card_power('wall of water', 0).
card_toughness('wall of water', 5).

% Found in: 7ED, CHR, LEG
card_name('wall of wonder', 'Wall of Wonder').
card_type('wall of wonder', 'Creature — Wall').
card_types('wall of wonder', ['Creature']).
card_subtypes('wall of wonder', ['Wall']).
card_colors('wall of wonder', ['U']).
card_text('wall of wonder', 'Defender (This creature can\'t attack.)\n{2}{U}{U}: Wall of Wonder gets +4/-4 until end of turn and can attack this turn as though it didn\'t have defender.').
card_mana_cost('wall of wonder', ['2', 'U', 'U']).
card_cmc('wall of wonder', 4).
card_layout('wall of wonder', 'normal').
card_power('wall of wonder', 1).
card_toughness('wall of wonder', 5).

% Found in: 10E, 2ED, 3ED, 4ED, CED, CEI, DPA, LEA, LEB
card_name('wall of wood', 'Wall of Wood').
card_type('wall of wood', 'Creature — Wall').
card_types('wall of wood', ['Creature']).
card_subtypes('wall of wood', ['Wall']).
card_colors('wall of wood', ['G']).
card_text('wall of wood', 'Defender (This creature can\'t attack.)').
card_mana_cost('wall of wood', ['G']).
card_cmc('wall of wood', 1).
card_layout('wall of wood', 'normal').
card_power('wall of wood', 0).
card_toughness('wall of wood', 3).

% Found in: INV
card_name('wallop', 'Wallop').
card_type('wallop', 'Sorcery').
card_types('wallop', ['Sorcery']).
card_subtypes('wallop', []).
card_colors('wallop', ['G']).
card_text('wallop', 'Destroy target blue or black creature with flying.').
card_mana_cost('wallop', ['1', 'G']).
card_cmc('wallop', 2).
card_layout('wallop', 'normal').

% Found in: 6ED, VIS
card_name('wand of denial', 'Wand of Denial').
card_type('wand of denial', 'Artifact').
card_types('wand of denial', ['Artifact']).
card_subtypes('wand of denial', []).
card_colors('wand of denial', []).
card_text('wand of denial', '{T}: Look at the top card of target player\'s library. If it\'s a nonland card, you may pay 2 life. If you do, put it into that player\'s graveyard.').
card_mana_cost('wand of denial', ['2']).
card_cmc('wand of denial', 2).
card_layout('wand of denial', 'normal').

% Found in: DRK
card_name('wand of ith', 'Wand of Ith').
card_type('wand of ith', 'Artifact').
card_types('wand of ith', ['Artifact']).
card_subtypes('wand of ith', []).
card_colors('wand of ith', []).
card_text('wand of ith', '{3}, {T}: Target player reveals a card at random from his or her hand. If it\'s a land card, that player discards it unless he or she pays 1 life. If it isn\'t a land card, the player discards it unless he or she pays life equal to its converted mana cost. Activate this ability only during your turn.').
card_mana_cost('wand of ith', ['4']).
card_cmc('wand of ith', 4).
card_layout('wand of ith', 'normal').

% Found in: DST
card_name('wand of the elements', 'Wand of the Elements').
card_type('wand of the elements', 'Artifact').
card_types('wand of the elements', ['Artifact']).
card_subtypes('wand of the elements', []).
card_colors('wand of the elements', []).
card_text('wand of the elements', '{T}, Sacrifice an Island: Put a 2/2 blue Elemental creature token with flying onto the battlefield.\n{T}, Sacrifice a Mountain: Put a 3/3 red Elemental creature token onto the battlefield.').
card_mana_cost('wand of the elements', ['4']).
card_cmc('wand of the elements', 4).
card_layout('wand of the elements', 'normal').

% Found in: SHM
card_name('wanderbrine rootcutters', 'Wanderbrine Rootcutters').
card_type('wanderbrine rootcutters', 'Creature — Merfolk Rogue').
card_types('wanderbrine rootcutters', ['Creature']).
card_subtypes('wanderbrine rootcutters', ['Merfolk', 'Rogue']).
card_colors('wanderbrine rootcutters', ['U', 'B']).
card_text('wanderbrine rootcutters', 'Wanderbrine Rootcutters can\'t be blocked by green creatures.').
card_mana_cost('wanderbrine rootcutters', ['2', 'U/B', 'U/B']).
card_cmc('wanderbrine rootcutters', 4).
card_layout('wanderbrine rootcutters', 'normal').
card_power('wanderbrine rootcutters', 3).
card_toughness('wanderbrine rootcutters', 3).

% Found in: LRW
card_name('wanderer\'s twig', 'Wanderer\'s Twig').
card_type('wanderer\'s twig', 'Artifact').
card_types('wanderer\'s twig', ['Artifact']).
card_subtypes('wanderer\'s twig', []).
card_colors('wanderer\'s twig', []).
card_text('wanderer\'s twig', '{1}, Sacrifice Wanderer\'s Twig: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('wanderer\'s twig', ['1']).
card_cmc('wanderer\'s twig', 1).
card_layout('wanderer\'s twig', 'normal').

% Found in: 9ED, MRD
card_name('wanderguard sentry', 'Wanderguard Sentry').
card_type('wanderguard sentry', 'Creature — Drone').
card_types('wanderguard sentry', ['Creature']).
card_subtypes('wanderguard sentry', ['Drone']).
card_colors('wanderguard sentry', ['U']).
card_text('wanderguard sentry', 'When Wanderguard Sentry enters the battlefield, look at target opponent\'s hand.').
card_mana_cost('wanderguard sentry', ['4', 'U']).
card_cmc('wanderguard sentry', 5).
card_layout('wanderguard sentry', 'normal').
card_power('wanderguard sentry', 3).
card_toughness('wanderguard sentry', 3).

% Found in: FRF
card_name('wandering champion', 'Wandering Champion').
card_type('wandering champion', 'Creature — Human Monk').
card_types('wandering champion', ['Creature']).
card_subtypes('wandering champion', ['Human', 'Monk']).
card_colors('wandering champion', ['W']).
card_text('wandering champion', 'Whenever Wandering Champion deals combat damage to a player, if you control a blue or red permanent, you may discard a card. If you do, draw a card.').
card_mana_cost('wandering champion', ['1', 'W']).
card_cmc('wandering champion', 2).
card_layout('wandering champion', 'normal').
card_power('wandering champion', 3).
card_toughness('wandering champion', 1).

% Found in: NMS
card_name('wandering eye', 'Wandering Eye').
card_type('wandering eye', 'Creature — Illusion').
card_types('wandering eye', ['Creature']).
card_subtypes('wandering eye', ['Illusion']).
card_colors('wandering eye', ['U']).
card_text('wandering eye', 'Flying\nPlayers play with their hands revealed.').
card_mana_cost('wandering eye', ['2', 'U']).
card_cmc('wandering eye', 3).
card_layout('wandering eye', 'normal').
card_power('wandering eye', 1).
card_toughness('wandering eye', 3).

% Found in: CON
card_name('wandering goblins', 'Wandering Goblins').
card_type('wandering goblins', 'Creature — Goblin Warrior').
card_types('wandering goblins', ['Creature']).
card_subtypes('wandering goblins', ['Goblin', 'Warrior']).
card_colors('wandering goblins', ['R']).
card_text('wandering goblins', 'Domain — {3}: Wandering Goblins gets +1/+0 until end of turn for each basic land type among lands you control.').
card_mana_cost('wandering goblins', ['2', 'R']).
card_cmc('wandering goblins', 3).
card_layout('wandering goblins', 'normal').
card_power('wandering goblins', 0).
card_toughness('wandering goblins', 3).

% Found in: MOR
card_name('wandering graybeard', 'Wandering Graybeard').
card_type('wandering graybeard', 'Creature — Giant Wizard').
card_types('wandering graybeard', ['Creature']).
card_subtypes('wandering graybeard', ['Giant', 'Wizard']).
card_colors('wandering graybeard', ['W']).
card_text('wandering graybeard', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Wandering Graybeard, you may reveal it. If you do, you gain 4 life.').
card_mana_cost('wandering graybeard', ['3', 'W', 'W']).
card_cmc('wandering graybeard', 5).
card_layout('wandering graybeard', 'normal').
card_power('wandering graybeard', 4).
card_toughness('wandering graybeard', 4).

% Found in: ALL, ME3
card_name('wandering mage', 'Wandering Mage').
card_type('wandering mage', 'Creature — Human Cleric Wizard').
card_types('wandering mage', ['Creature']).
card_subtypes('wandering mage', ['Human', 'Cleric', 'Wizard']).
card_colors('wandering mage', ['W', 'U', 'B']).
card_text('wandering mage', '{W}, Pay 1 life: Prevent the next 2 damage that would be dealt to target creature this turn.\n{U}: Prevent the next 1 damage that would be dealt to target Cleric or Wizard creature this turn.\n{B}, Put a -1/-1 counter on a creature you control: Prevent the next 2 damage that would be dealt to target player this turn.').
card_mana_cost('wandering mage', ['W', 'U', 'B']).
card_cmc('wandering mage', 3).
card_layout('wandering mage', 'normal').
card_power('wandering mage', 0).
card_toughness('wandering mage', 3).
card_reserved('wandering mage').

% Found in: CHK
card_name('wandering ones', 'Wandering Ones').
card_type('wandering ones', 'Creature — Spirit').
card_types('wandering ones', ['Creature']).
card_subtypes('wandering ones', ['Spirit']).
card_colors('wandering ones', ['U']).
card_text('wandering ones', '').
card_mana_cost('wandering ones', ['U']).
card_cmc('wandering ones', 1).
card_layout('wandering ones', 'normal').
card_power('wandering ones', 1).
card_toughness('wandering ones', 1).

% Found in: INV
card_name('wandering stream', 'Wandering Stream').
card_type('wandering stream', 'Sorcery').
card_types('wandering stream', ['Sorcery']).
card_subtypes('wandering stream', []).
card_colors('wandering stream', ['G']).
card_text('wandering stream', 'Domain — You gain 2 life for each basic land type among lands you control.').
card_mana_cost('wandering stream', ['2', 'G']).
card_cmc('wandering stream', 3).
card_layout('wandering stream', 'normal').

% Found in: DTK
card_name('wandering tombshell', 'Wandering Tombshell').
card_type('wandering tombshell', 'Creature — Zombie Turtle').
card_types('wandering tombshell', ['Creature']).
card_subtypes('wandering tombshell', ['Zombie', 'Turtle']).
card_colors('wandering tombshell', ['B']).
card_text('wandering tombshell', '').
card_mana_cost('wandering tombshell', ['3', 'B']).
card_cmc('wandering tombshell', 4).
card_layout('wandering tombshell', 'normal').
card_power('wandering tombshell', 1).
card_toughness('wandering tombshell', 6).

% Found in: AVR
card_name('wandering wolf', 'Wandering Wolf').
card_type('wandering wolf', 'Creature — Wolf').
card_types('wandering wolf', ['Creature']).
card_subtypes('wandering wolf', ['Wolf']).
card_colors('wandering wolf', ['G']).
card_text('wandering wolf', 'Creatures with power less than Wandering Wolf\'s power can\'t block it.').
card_mana_cost('wandering wolf', ['1', 'G']).
card_cmc('wandering wolf', 2).
card_layout('wandering wolf', 'normal').
card_power('wandering wolf', 2).
card_toughness('wandering wolf', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED
card_name('wanderlust', 'Wanderlust').
card_type('wanderlust', 'Enchantment — Aura').
card_types('wanderlust', ['Enchantment']).
card_subtypes('wanderlust', ['Aura']).
card_colors('wanderlust', ['G']).
card_text('wanderlust', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, Wanderlust deals 1 damage to that player.').
card_mana_cost('wanderlust', ['2', 'G']).
card_cmc('wanderlust', 3).
card_layout('wanderlust', 'normal').

% Found in: LRW
card_name('wanderwine hub', 'Wanderwine Hub').
card_type('wanderwine hub', 'Land').
card_types('wanderwine hub', ['Land']).
card_subtypes('wanderwine hub', []).
card_colors('wanderwine hub', []).
card_text('wanderwine hub', 'As Wanderwine Hub enters the battlefield, you may reveal a Merfolk card from your hand. If you don\'t, Wanderwine Hub enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_layout('wanderwine hub', 'normal').

% Found in: LRW
card_name('wanderwine prophets', 'Wanderwine Prophets').
card_type('wanderwine prophets', 'Creature — Merfolk Wizard').
card_types('wanderwine prophets', ['Creature']).
card_subtypes('wanderwine prophets', ['Merfolk', 'Wizard']).
card_colors('wanderwine prophets', ['U']).
card_text('wanderwine prophets', 'Champion a Merfolk (When this enters the battlefield, sacrifice it unless you exile another Merfolk you control. When this leaves the battlefield, that card returns to the battlefield.)\nWhenever Wanderwine Prophets deals combat damage to a player, you may sacrifice a Merfolk. If you do, take an extra turn after this one.').
card_mana_cost('wanderwine prophets', ['4', 'U', 'U']).
card_cmc('wanderwine prophets', 6).
card_layout('wanderwine prophets', 'normal').
card_power('wanderwine prophets', 4).
card_toughness('wanderwine prophets', 4).

% Found in: ARC, INV
card_name('wane', 'Wane').
card_type('wane', 'Instant').
card_types('wane', ['Instant']).
card_subtypes('wane', []).
card_colors('wane', ['W']).
card_text('wane', 'Destroy target enchantment.').
card_mana_cost('wane', ['W']).
card_cmc('wane', 1).
card_layout('wane', 'split').

% Found in: PLC
card_name('waning wurm', 'Waning Wurm').
card_type('waning wurm', 'Creature — Zombie Wurm').
card_types('waning wurm', ['Creature']).
card_subtypes('waning wurm', ['Zombie', 'Wurm']).
card_colors('waning wurm', ['B']).
card_text('waning wurm', 'Vanishing 2 (This permanent enters the battlefield with two time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)').
card_mana_cost('waning wurm', ['3', 'B']).
card_cmc('waning wurm', 4).
card_layout('waning wurm', 'normal').
card_power('waning wurm', 7).
card_toughness('waning wurm', 6).

% Found in: DRK, TSB
card_name('war barge', 'War Barge').
card_type('war barge', 'Artifact').
card_types('war barge', ['Artifact']).
card_subtypes('war barge', []).
card_colors('war barge', []).
card_text('war barge', '{3}: Target creature gains islandwalk until end of turn. When War Barge leaves the battlefield this turn, destroy that creature. A creature destroyed this way can\'t be regenerated. (A creature with islandwalk can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('war barge', ['4']).
card_cmc('war barge', 4).
card_layout('war barge', 'normal').

% Found in: KTK
card_name('war behemoth', 'War Behemoth').
card_type('war behemoth', 'Creature — Beast').
card_types('war behemoth', ['Creature']).
card_subtypes('war behemoth', ['Beast']).
card_colors('war behemoth', ['W']).
card_text('war behemoth', 'Morph {4}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('war behemoth', ['5', 'W']).
card_cmc('war behemoth', 6).
card_layout('war behemoth', 'normal').
card_power('war behemoth', 3).
card_toughness('war behemoth', 6).

% Found in: C13, MMQ
card_name('war cadence', 'War Cadence').
card_type('war cadence', 'Enchantment').
card_types('war cadence', ['Enchantment']).
card_subtypes('war cadence', []).
card_colors('war cadence', ['R']).
card_text('war cadence', '{X}{R}: This turn, creatures can\'t block unless their controller pays {X} for each blocking creature he or she controls.').
card_mana_cost('war cadence', ['2', 'R']).
card_cmc('war cadence', 3).
card_layout('war cadence', 'normal').

% Found in: ICE
card_name('war chariot', 'War Chariot').
card_type('war chariot', 'Artifact').
card_types('war chariot', ['Artifact']).
card_subtypes('war chariot', []).
card_colors('war chariot', []).
card_text('war chariot', '{3}, {T}: Target creature gains trample until end of turn.').
card_mana_cost('war chariot', ['3']).
card_cmc('war chariot', 3).
card_layout('war chariot', 'normal').

% Found in: USG
card_name('war dance', 'War Dance').
card_type('war dance', 'Enchantment').
card_types('war dance', ['Enchantment']).
card_subtypes('war dance', []).
card_colors('war dance', ['G']).
card_text('war dance', 'At the beginning of your upkeep, you may put a verse counter on War Dance.\nSacrifice War Dance: Target creature gets +X/+X until end of turn, where X is the number of verse counters on War Dance.').
card_mana_cost('war dance', ['G']).
card_cmc('war dance', 1).
card_layout('war dance', 'normal').

% Found in: MRD
card_name('war elemental', 'War Elemental').
card_type('war elemental', 'Creature — Elemental').
card_types('war elemental', ['Creature']).
card_subtypes('war elemental', ['Elemental']).
card_colors('war elemental', ['R']).
card_text('war elemental', 'When War Elemental enters the battlefield, sacrifice it unless an opponent was dealt damage this turn.\nWhenever an opponent is dealt damage, put that many +1/+1 counters on War Elemental.').
card_mana_cost('war elemental', ['R', 'R', 'R']).
card_cmc('war elemental', 3).
card_layout('war elemental', 'normal').
card_power('war elemental', 1).
card_toughness('war elemental', 1).

% Found in: ARN, CHR
card_name('war elephant', 'War Elephant').
card_type('war elephant', 'Creature — Elephant').
card_types('war elephant', ['Creature']).
card_subtypes('war elephant', ['Elephant']).
card_colors('war elephant', ['W']).
card_text('war elephant', 'Trample; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('war elephant', ['3', 'W']).
card_cmc('war elephant', 4).
card_layout('war elephant', 'normal').
card_power('war elephant', 2).
card_toughness('war elephant', 2).

% Found in: M13
card_name('war falcon', 'War Falcon').
card_type('war falcon', 'Creature — Bird').
card_types('war falcon', ['Creature']).
card_subtypes('war falcon', ['Bird']).
card_colors('war falcon', ['W']).
card_text('war falcon', 'Flying\nWar Falcon can\'t attack unless you control a Knight or a Soldier.').
card_mana_cost('war falcon', ['W']).
card_cmc('war falcon', 1).
card_layout('war falcon', 'normal').
card_power('war falcon', 2).
card_toughness('war falcon', 1).

% Found in: FRF
card_name('war flare', 'War Flare').
card_type('war flare', 'Instant').
card_types('war flare', ['Instant']).
card_subtypes('war flare', []).
card_colors('war flare', ['W', 'R']).
card_text('war flare', 'Creatures you control get +2/+1 until end of turn. Untap those creatures.').
card_mana_cost('war flare', ['2', 'R', 'W']).
card_cmc('war flare', 4).
card_layout('war flare', 'normal').

% Found in: ORI
card_name('war horn', 'War Horn').
card_type('war horn', 'Artifact').
card_types('war horn', ['Artifact']).
card_subtypes('war horn', []).
card_colors('war horn', []).
card_text('war horn', 'Attacking creatures you control get +1/+0.').
card_mana_cost('war horn', ['3']).
card_cmc('war horn', 3).
card_layout('war horn', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, ME4, RQS
card_name('war mammoth', 'War Mammoth').
card_type('war mammoth', 'Creature — Elephant').
card_types('war mammoth', ['Creature']).
card_subtypes('war mammoth', ['Elephant']).
card_colors('war mammoth', ['G']).
card_text('war mammoth', 'Trample').
card_mana_cost('war mammoth', ['3', 'G']).
card_cmc('war mammoth', 4).
card_layout('war mammoth', 'normal').
card_power('war mammoth', 3).
card_toughness('war mammoth', 3).

% Found in: ORI
card_name('war oracle', 'War Oracle').
card_type('war oracle', 'Creature — Human Cleric').
card_types('war oracle', ['Creature']).
card_subtypes('war oracle', ['Human', 'Cleric']).
card_colors('war oracle', ['W']).
card_text('war oracle', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_mana_cost('war oracle', ['2', 'W', 'W']).
card_cmc('war oracle', 4).
card_layout('war oracle', 'normal').
card_power('war oracle', 3).
card_toughness('war oracle', 3).

% Found in: M11, M13
card_name('war priest of thune', 'War Priest of Thune').
card_type('war priest of thune', 'Creature — Human Cleric').
card_types('war priest of thune', ['Creature']).
card_subtypes('war priest of thune', ['Human', 'Cleric']).
card_colors('war priest of thune', ['W']).
card_text('war priest of thune', 'When War Priest of Thune enters the battlefield, you may destroy target enchantment.').
card_mana_cost('war priest of thune', ['1', 'W']).
card_cmc('war priest of thune', 2).
card_layout('war priest of thune', 'normal').
card_power('war priest of thune', 2).
card_toughness('war priest of thune', 2).

% Found in: NPH
card_name('war report', 'War Report').
card_type('war report', 'Instant').
card_types('war report', ['Instant']).
card_subtypes('war report', []).
card_colors('war report', ['W']).
card_text('war report', 'You gain life equal to the number of creatures on the battlefield plus the number of artifacts on the battlefield.').
card_mana_cost('war report', ['3', 'W']).
card_cmc('war report', 4).
card_layout('war report', 'normal').

% Found in: MMQ
card_name('war tax', 'War Tax').
card_type('war tax', 'Enchantment').
card_types('war tax', ['Enchantment']).
card_subtypes('war tax', []).
card_colors('war tax', ['U']).
card_text('war tax', '{X}{U}: This turn, creatures can\'t attack unless their controller pays {X} for each attacking creature he or she controls.').
card_mana_cost('war tax', ['2', 'U']).
card_cmc('war tax', 3).
card_layout('war tax', 'normal').

% Found in: DIS
card_name('war\'s toll', 'War\'s Toll').
card_type('war\'s toll', 'Enchantment').
card_types('war\'s toll', ['Enchantment']).
card_subtypes('war\'s toll', []).
card_colors('war\'s toll', ['R']).
card_text('war\'s toll', 'Whenever an opponent taps a land for mana, tap all lands that player controls.\nIf a creature an opponent controls attacks, all creatures that opponent controls attack if able.').
card_mana_cost('war\'s toll', ['3', 'R']).
card_cmc('war\'s toll', 4).
card_layout('war\'s toll', 'normal').

% Found in: KTK
card_name('war-name aspirant', 'War-Name Aspirant').
card_type('war-name aspirant', 'Creature — Human Warrior').
card_types('war-name aspirant', ['Creature']).
card_subtypes('war-name aspirant', ['Human', 'Warrior']).
card_colors('war-name aspirant', ['R']).
card_text('war-name aspirant', 'Raid — War-Name Aspirant enters the battlefield with a +1/+1 counter on it if you attacked with a creature this turn.\nWar-Name Aspirant can\'t be blocked by creatures with power 1 or less.').
card_mana_cost('war-name aspirant', ['1', 'R']).
card_cmc('war-name aspirant', 2).
card_layout('war-name aspirant', 'normal').
card_power('war-name aspirant', 2).
card_toughness('war-name aspirant', 1).

% Found in: MMA, MOR
card_name('war-spike changeling', 'War-Spike Changeling').
card_type('war-spike changeling', 'Creature — Shapeshifter').
card_types('war-spike changeling', ['Creature']).
card_subtypes('war-spike changeling', ['Shapeshifter']).
card_colors('war-spike changeling', ['R']).
card_text('war-spike changeling', 'Changeling (This card is every creature type.)\n{R}: War-Spike Changeling gains first strike until end of turn.').
card_mana_cost('war-spike changeling', ['3', 'R']).
card_cmc('war-spike changeling', 4).
card_layout('war-spike changeling', 'normal').
card_power('war-spike changeling', 3).
card_toughness('war-spike changeling', 3).

% Found in: RAV
card_name('war-torch goblin', 'War-Torch Goblin').
card_type('war-torch goblin', 'Creature — Goblin Warrior').
card_types('war-torch goblin', ['Creature']).
card_subtypes('war-torch goblin', ['Goblin', 'Warrior']).
card_colors('war-torch goblin', ['R']).
card_text('war-torch goblin', '{R}, Sacrifice War-Torch Goblin: War-Torch Goblin deals 2 damage to target blocking creature.').
card_mana_cost('war-torch goblin', ['R']).
card_cmc('war-torch goblin', 1).
card_layout('war-torch goblin', 'normal').
card_power('war-torch goblin', 1).
card_toughness('war-torch goblin', 1).

% Found in: JOU
card_name('war-wing siren', 'War-Wing Siren').
card_type('war-wing siren', 'Creature — Siren Soldier').
card_types('war-wing siren', ['Creature']).
card_subtypes('war-wing siren', ['Siren', 'Soldier']).
card_colors('war-wing siren', ['U']).
card_text('war-wing siren', 'Flying\nHeroic — Whenever you cast a spell that targets War-Wing Siren, put a +1/+1 counter on War-Wing Siren.').
card_mana_cost('war-wing siren', ['2', 'U']).
card_cmc('war-wing siren', 3).
card_layout('war-wing siren', 'normal').
card_power('war-wing siren', 1).
card_toughness('war-wing siren', 3).

% Found in: LGN
card_name('warbreak trumpeter', 'Warbreak Trumpeter').
card_type('warbreak trumpeter', 'Creature — Goblin').
card_types('warbreak trumpeter', ['Creature']).
card_subtypes('warbreak trumpeter', ['Goblin']).
card_colors('warbreak trumpeter', ['R']).
card_text('warbreak trumpeter', 'Morph {X}{X}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Warbreak Trumpeter is turned face up, put X 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('warbreak trumpeter', ['R']).
card_cmc('warbreak trumpeter', 1).
card_layout('warbreak trumpeter', 'normal').
card_power('warbreak trumpeter', 1).
card_toughness('warbreak trumpeter', 1).

% Found in: DTK
card_name('warbringer', 'Warbringer').
card_type('warbringer', 'Creature — Orc Berserker').
card_types('warbringer', ['Creature']).
card_subtypes('warbringer', ['Orc', 'Berserker']).
card_colors('warbringer', ['R']).
card_text('warbringer', 'Dash costs you pay cost {2} less (as long as this creature is on the battlefield).\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('warbringer', ['3', 'R']).
card_cmc('warbringer', 4).
card_layout('warbringer', 'normal').
card_power('warbringer', 3).
card_toughness('warbringer', 3).

% Found in: BNG
card_name('warchanter of mogis', 'Warchanter of Mogis').
card_type('warchanter of mogis', 'Creature — Minotaur Shaman').
card_types('warchanter of mogis', ['Creature']).
card_subtypes('warchanter of mogis', ['Minotaur', 'Shaman']).
card_colors('warchanter of mogis', ['B']).
card_text('warchanter of mogis', 'Inspired — Whenever Warchanter of Mogis becomes untapped, target creature you control gains intimidate until end of turn. (A creature with intimidate can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('warchanter of mogis', ['3', 'B', 'B']).
card_cmc('warchanter of mogis', 5).
card_layout('warchanter of mogis', 'normal').
card_power('warchanter of mogis', 3).
card_toughness('warchanter of mogis', 3).

% Found in: M13
card_name('warclamp mastiff', 'Warclamp Mastiff').
card_type('warclamp mastiff', 'Creature — Hound').
card_types('warclamp mastiff', ['Creature']).
card_subtypes('warclamp mastiff', ['Hound']).
card_colors('warclamp mastiff', ['W']).
card_text('warclamp mastiff', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('warclamp mastiff', ['W']).
card_cmc('warclamp mastiff', 1).
card_layout('warclamp mastiff', 'normal').
card_power('warclamp mastiff', 1).
card_toughness('warclamp mastiff', 1).

% Found in: EVE
card_name('ward of bones', 'Ward of Bones').
card_type('ward of bones', 'Artifact').
card_types('ward of bones', ['Artifact']).
card_subtypes('ward of bones', []).
card_colors('ward of bones', []).
card_text('ward of bones', 'Each opponent who controls more creatures than you can\'t play creature cards. The same is true for artifacts, enchantments, and lands.').
card_mana_cost('ward of bones', ['6']).
card_cmc('ward of bones', 6).
card_layout('ward of bones', 'normal').

% Found in: MIR
card_name('ward of lights', 'Ward of Lights').
card_type('ward of lights', 'Enchantment — Aura').
card_types('ward of lights', ['Enchantment']).
card_subtypes('ward of lights', ['Aura']).
card_colors('ward of lights', ['W']).
card_text('ward of lights', 'You may cast Ward of Lights as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nEnchant creature\nAs Ward of Lights enters the battlefield, choose a color.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Ward of Lights.').
card_mana_cost('ward of lights', ['W', 'W']).
card_cmc('ward of lights', 2).
card_layout('ward of lights', 'normal').

% Found in: BOK
card_name('ward of piety', 'Ward of Piety').
card_type('ward of piety', 'Enchantment — Aura').
card_types('ward of piety', ['Enchantment']).
card_subtypes('ward of piety', ['Aura']).
card_colors('ward of piety', ['W']).
card_text('ward of piety', 'Enchant creature\n{1}{W}: The next 1 damage that would be dealt to enchanted creature this turn is dealt to target creature or player instead.').
card_mana_cost('ward of piety', ['1', 'W']).
card_cmc('ward of piety', 2).
card_layout('ward of piety', 'normal').

% Found in: LGN
card_name('ward sliver', 'Ward Sliver').
card_type('ward sliver', 'Creature — Sliver').
card_types('ward sliver', ['Creature']).
card_subtypes('ward sliver', ['Sliver']).
card_colors('ward sliver', ['W']).
card_text('ward sliver', 'As Ward Sliver enters the battlefield, choose a color.\nAll Slivers have protection from the chosen color.').
card_mana_cost('ward sliver', ['4', 'W']).
card_cmc('ward sliver', 5).
card_layout('ward sliver', 'normal').
card_power('ward sliver', 2).
card_toughness('ward sliver', 2).

% Found in: M14
card_name('warden of evos isle', 'Warden of Evos Isle').
card_type('warden of evos isle', 'Creature — Bird Wizard').
card_types('warden of evos isle', ['Creature']).
card_subtypes('warden of evos isle', ['Bird', 'Wizard']).
card_colors('warden of evos isle', ['U']).
card_text('warden of evos isle', 'Flying\nCreature spells with flying you cast cost {1} less to cast.').
card_mana_cost('warden of evos isle', ['2', 'U']).
card_cmc('warden of evos isle', 3).
card_layout('warden of evos isle', 'normal').
card_power('warden of evos isle', 2).
card_toughness('warden of evos isle', 2).

% Found in: M15
card_name('warden of the beyond', 'Warden of the Beyond').
card_type('warden of the beyond', 'Creature — Human Wizard').
card_types('warden of the beyond', ['Creature']).
card_subtypes('warden of the beyond', ['Human', 'Wizard']).
card_colors('warden of the beyond', ['W']).
card_text('warden of the beyond', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nWarden of the Beyond gets +2/+2 as long as an opponent owns a card in exile.').
card_mana_cost('warden of the beyond', ['2', 'W']).
card_cmc('warden of the beyond', 3).
card_layout('warden of the beyond', 'normal').
card_power('warden of the beyond', 2).
card_toughness('warden of the beyond', 2).

% Found in: KTK
card_name('warden of the eye', 'Warden of the Eye').
card_type('warden of the eye', 'Creature — Djinn Wizard').
card_types('warden of the eye', ['Creature']).
card_subtypes('warden of the eye', ['Djinn', 'Wizard']).
card_colors('warden of the eye', ['W', 'U', 'R']).
card_text('warden of the eye', 'When Warden of the Eye enters the battlefield, return target noncreature, nonland card from your graveyard to your hand.').
card_mana_cost('warden of the eye', ['2', 'U', 'R', 'W']).
card_cmc('warden of the eye', 5).
card_layout('warden of the eye', 'normal').
card_power('warden of the eye', 3).
card_toughness('warden of the eye', 3).

% Found in: FRF
card_name('warden of the first tree', 'Warden of the First Tree').
card_type('warden of the first tree', 'Creature — Human').
card_types('warden of the first tree', ['Creature']).
card_subtypes('warden of the first tree', ['Human']).
card_colors('warden of the first tree', ['G']).
card_text('warden of the first tree', '{1}{W/B}: Warden of the First Tree becomes a Human Warrior with base power and toughness 3/3.\n{2}{W/B}{W/B}: If Warden of the First Tree is a Warrior, it becomes a Human Spirit Warrior with trample and lifelink.\n{3}{W/B}{W/B}{W/B}: If Warden of the First Tree is a Spirit, put five +1/+1 counters on it.').
card_mana_cost('warden of the first tree', ['G']).
card_cmc('warden of the first tree', 1).
card_layout('warden of the first tree', 'normal').
card_power('warden of the first tree', 1).
card_toughness('warden of the first tree', 1).

% Found in: DKA
card_name('warden of the wall', 'Warden of the Wall').
card_type('warden of the wall', 'Artifact').
card_types('warden of the wall', ['Artifact']).
card_subtypes('warden of the wall', []).
card_colors('warden of the wall', []).
card_text('warden of the wall', 'Warden of the Wall enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\nAs long as it\'s not your turn, Warden of the Wall is a 2/3 Gargoyle artifact creature with flying.').
card_mana_cost('warden of the wall', ['3']).
card_cmc('warden of the wall', 3).
card_layout('warden of the wall', 'normal').

% Found in: FRF
card_name('wardscale dragon', 'Wardscale Dragon').
card_type('wardscale dragon', 'Creature — Dragon').
card_types('wardscale dragon', ['Creature']).
card_subtypes('wardscale dragon', ['Dragon']).
card_colors('wardscale dragon', ['W']).
card_text('wardscale dragon', 'Flying\nAs long as Wardscale Dragon is attacking, defending player can\'t cast spells.').
card_mana_cost('wardscale dragon', ['4', 'W', 'W']).
card_cmc('wardscale dragon', 6).
card_layout('wardscale dragon', 'normal').
card_power('wardscale dragon', 4).
card_toughness('wardscale dragon', 4).

% Found in: ARB
card_name('wargate', 'Wargate').
card_type('wargate', 'Sorcery').
card_types('wargate', ['Sorcery']).
card_subtypes('wargate', []).
card_colors('wargate', ['W', 'U', 'G']).
card_text('wargate', 'Search your library for a permanent card with converted mana cost X or less, put it onto the battlefield, then shuffle your library.').
card_mana_cost('wargate', ['X', 'G', 'W', 'U']).
card_cmc('wargate', 3).
card_layout('wargate', 'normal').

% Found in: DGM, pFNM
card_name('warleader\'s helix', 'Warleader\'s Helix').
card_type('warleader\'s helix', 'Instant').
card_types('warleader\'s helix', ['Instant']).
card_subtypes('warleader\'s helix', []).
card_colors('warleader\'s helix', ['W', 'R']).
card_text('warleader\'s helix', 'Warleader\'s Helix deals 4 damage to target creature or player and you gain 4 life.').
card_mana_cost('warleader\'s helix', ['2', 'R', 'W']).
card_cmc('warleader\'s helix', 4).
card_layout('warleader\'s helix', 'normal').

% Found in: M11
card_name('warlord\'s axe', 'Warlord\'s Axe').
card_type('warlord\'s axe', 'Artifact — Equipment').
card_types('warlord\'s axe', ['Artifact']).
card_subtypes('warlord\'s axe', ['Equipment']).
card_colors('warlord\'s axe', []).
card_text('warlord\'s axe', 'Equipped creature gets +3/+1.\nEquip {4} ({4}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('warlord\'s axe', ['3']).
card_cmc('warlord\'s axe', 3).
card_layout('warlord\'s axe', 'normal').

% Found in: GTC
card_name('warmind infantry', 'Warmind Infantry').
card_type('warmind infantry', 'Creature — Elemental Soldier').
card_types('warmind infantry', ['Creature']).
card_subtypes('warmind infantry', ['Elemental', 'Soldier']).
card_colors('warmind infantry', ['R']).
card_text('warmind infantry', 'Battalion — Whenever Warmind Infantry and at least two other creatures attack, Warmind Infantry gets +2/+0 until end of turn.').
card_mana_cost('warmind infantry', ['2', 'R']).
card_cmc('warmind infantry', 3).
card_layout('warmind infantry', 'normal').
card_power('warmind infantry', 2).
card_toughness('warmind infantry', 3).

% Found in: MMQ, pMEI
card_name('warmonger', 'Warmonger').
card_type('warmonger', 'Creature — Minotaur Monger').
card_types('warmonger', ['Creature']).
card_subtypes('warmonger', ['Minotaur', 'Monger']).
card_colors('warmonger', ['R']).
card_text('warmonger', '{2}: Warmonger deals 1 damage to each creature without flying and each player. Any player may activate this ability.').
card_mana_cost('warmonger', ['3', 'R']).
card_cmc('warmonger', 4).
card_layout('warmonger', 'normal').
card_power('warmonger', 3).
card_toughness('warmonger', 3).

% Found in: C14
card_name('warmonger hellkite', 'Warmonger Hellkite').
card_type('warmonger hellkite', 'Creature — Dragon').
card_types('warmonger hellkite', ['Creature']).
card_subtypes('warmonger hellkite', ['Dragon']).
card_colors('warmonger hellkite', ['R']).
card_text('warmonger hellkite', 'Flying\nAll creatures attack each combat if able.\n{1}{R}: Attacking creatures get +1/+0 until end of turn.').
card_mana_cost('warmonger hellkite', ['4', 'R', 'R']).
card_cmc('warmonger hellkite', 6).
card_layout('warmonger hellkite', 'normal').
card_power('warmonger hellkite', 5).
card_toughness('warmonger hellkite', 5).

% Found in: CNS, ROE
card_name('warmonger\'s chariot', 'Warmonger\'s Chariot').
card_type('warmonger\'s chariot', 'Artifact — Equipment').
card_types('warmonger\'s chariot', ['Artifact']).
card_subtypes('warmonger\'s chariot', ['Equipment']).
card_colors('warmonger\'s chariot', []).
card_text('warmonger\'s chariot', 'Equipped creature gets +2/+2.\nAs long as equipped creature has defender, it can attack as though it didn\'t have defender.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('warmonger\'s chariot', ['2']).
card_cmc('warmonger\'s chariot', 2).
card_layout('warmonger\'s chariot', 'normal').

% Found in: 6ED, TMP
card_name('warmth', 'Warmth').
card_type('warmth', 'Enchantment').
card_types('warmth', ['Enchantment']).
card_subtypes('warmth', []).
card_colors('warmth', ['W']).
card_text('warmth', 'Whenever an opponent casts a red spell, you gain 2 life.').
card_mana_cost('warmth', ['1', 'W']).
card_cmc('warmth', 2).
card_layout('warmth', 'normal').

% Found in: ICE, ME2
card_name('warning', 'Warning').
card_type('warning', 'Instant').
card_types('warning', ['Instant']).
card_subtypes('warning', []).
card_colors('warning', ['W']).
card_text('warning', 'Prevent all combat damage that would be dealt by target attacking creature this turn.').
card_mana_cost('warning', ['W']).
card_cmc('warning', 1).
card_layout('warning', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, ME4, RQS
card_name('warp artifact', 'Warp Artifact').
card_type('warp artifact', 'Enchantment — Aura').
card_types('warp artifact', ['Enchantment']).
card_subtypes('warp artifact', ['Aura']).
card_colors('warp artifact', ['B']).
card_text('warp artifact', 'Enchant artifact\nAt the beginning of the upkeep of enchanted artifact\'s controller, Warp Artifact deals 1 damage to that player.').
card_mana_cost('warp artifact', ['B', 'B']).
card_cmc('warp artifact', 2).
card_layout('warp artifact', 'normal').

% Found in: 10E, M10, RAV
card_name('warp world', 'Warp World').
card_type('warp world', 'Sorcery').
card_types('warp world', ['Sorcery']).
card_subtypes('warp world', []).
card_colors('warp world', ['R']).
card_text('warp world', 'Each player shuffles all permanents he or she owns into his or her library, then reveals that many cards from the top of his or her library. Each player puts all artifact, creature, and land cards revealed this way onto the battlefield, then does the same for enchantment cards, then puts all cards revealed this way that weren\'t put onto the battlefield on the bottom of his or her library.').
card_mana_cost('warp world', ['5', 'R', 'R', 'R']).
card_cmc('warp world', 8).
card_layout('warp world', 'normal').

% Found in: MMQ
card_name('warpath', 'Warpath').
card_type('warpath', 'Instant').
card_types('warpath', ['Instant']).
card_subtypes('warpath', []).
card_colors('warpath', ['R']).
card_text('warpath', 'Warpath deals 3 damage to each blocking creature and each blocked creature.').
card_mana_cost('warpath', ['3', 'R']).
card_cmc('warpath', 4).
card_layout('warpath', 'normal').

% Found in: M10, M12
card_name('warpath ghoul', 'Warpath Ghoul').
card_type('warpath ghoul', 'Creature — Zombie').
card_types('warpath ghoul', ['Creature']).
card_subtypes('warpath ghoul', ['Zombie']).
card_colors('warpath ghoul', ['B']).
card_text('warpath ghoul', '').
card_mana_cost('warpath ghoul', ['2', 'B']).
card_cmc('warpath ghoul', 3).
card_layout('warpath ghoul', 'normal').
card_power('warpath ghoul', 3).
card_toughness('warpath ghoul', 2).

% Found in: 8ED, PLS
card_name('warped devotion', 'Warped Devotion').
card_type('warped devotion', 'Enchantment').
card_types('warped devotion', ['Enchantment']).
card_subtypes('warped devotion', []).
card_colors('warped devotion', ['B']).
card_text('warped devotion', 'Whenever a permanent is returned to a player\'s hand, that player discards a card.').
card_mana_cost('warped devotion', ['2', 'B']).
card_cmc('warped devotion', 3).
card_layout('warped devotion', 'normal').

% Found in: DGM
card_name('warped physique', 'Warped Physique').
card_type('warped physique', 'Instant').
card_types('warped physique', ['Instant']).
card_subtypes('warped physique', []).
card_colors('warped physique', ['U', 'B']).
card_text('warped physique', 'Target creature gets +X/-X until end of turn, where X is the number of cards in your hand.').
card_mana_cost('warped physique', ['U', 'B']).
card_cmc('warped physique', 2).
card_layout('warped physique', 'normal').

% Found in: LGN
card_name('warped researcher', 'Warped Researcher').
card_type('warped researcher', 'Creature — Human Wizard Mutant').
card_types('warped researcher', ['Creature']).
card_subtypes('warped researcher', ['Human', 'Wizard', 'Mutant']).
card_colors('warped researcher', ['U']).
card_text('warped researcher', 'Whenever a player cycles a card, Warped Researcher gains flying and shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_mana_cost('warped researcher', ['4', 'U']).
card_cmc('warped researcher', 5).
card_layout('warped researcher', 'normal').
card_power('warped researcher', 3).
card_toughness('warped researcher', 4).

% Found in: MIR
card_name('warping wurm', 'Warping Wurm').
card_type('warping wurm', 'Creature — Wurm').
card_types('warping wurm', ['Creature']).
card_subtypes('warping wurm', ['Wurm']).
card_colors('warping wurm', ['U', 'G']).
card_text('warping wurm', 'Phasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)\nAt the beginning of your upkeep, Warping Wurm phases out unless you pay {2}{G}{U}.\nWhen Warping Wurm phases in, put a +1/+1 counter on it.').
card_mana_cost('warping wurm', ['2', 'G', 'U']).
card_cmc('warping wurm', 4).
card_layout('warping wurm', 'normal').
card_power('warping wurm', 1).
card_toughness('warping wurm', 1).
card_reserved('warping wurm').

% Found in: ZEN
card_name('warren instigator', 'Warren Instigator').
card_type('warren instigator', 'Creature — Goblin Berserker').
card_types('warren instigator', ['Creature']).
card_subtypes('warren instigator', ['Goblin', 'Berserker']).
card_colors('warren instigator', ['R']).
card_text('warren instigator', 'Double strike\nWhenever Warren Instigator deals damage to an opponent, you may put a Goblin creature card from your hand onto the battlefield.').
card_mana_cost('warren instigator', ['R', 'R']).
card_cmc('warren instigator', 2).
card_layout('warren instigator', 'normal').
card_power('warren instigator', 1).
card_toughness('warren instigator', 1).

% Found in: LRW, MMA
card_name('warren pilferers', 'Warren Pilferers').
card_type('warren pilferers', 'Creature — Goblin Rogue').
card_types('warren pilferers', ['Creature']).
card_subtypes('warren pilferers', ['Goblin', 'Rogue']).
card_colors('warren pilferers', ['B']).
card_text('warren pilferers', 'When Warren Pilferers enters the battlefield, return target creature card from your graveyard to your hand. If that card is a Goblin card, Warren Pilferers gains haste until end of turn.').
card_mana_cost('warren pilferers', ['4', 'B']).
card_cmc('warren pilferers', 5).
card_layout('warren pilferers', 'normal').
card_power('warren pilferers', 3).
card_toughness('warren pilferers', 3).

% Found in: MMA, MOR
card_name('warren weirding', 'Warren Weirding').
card_type('warren weirding', 'Tribal Sorcery — Goblin').
card_types('warren weirding', ['Tribal', 'Sorcery']).
card_subtypes('warren weirding', ['Goblin']).
card_colors('warren weirding', ['B']).
card_text('warren weirding', 'Target player sacrifices a creature. If a Goblin is sacrificed this way, that player puts two 1/1 black Goblin Rogue creature tokens onto the battlefield, and those tokens gain haste until end of turn.').
card_mana_cost('warren weirding', ['1', 'B']).
card_cmc('warren weirding', 2).
card_layout('warren weirding', 'normal').

% Found in: LRW
card_name('warren-scourge elf', 'Warren-Scourge Elf').
card_type('warren-scourge elf', 'Creature — Elf Warrior').
card_types('warren-scourge elf', ['Creature']).
card_subtypes('warren-scourge elf', ['Elf', 'Warrior']).
card_colors('warren-scourge elf', ['G']).
card_text('warren-scourge elf', 'Protection from Goblins').
card_mana_cost('warren-scourge elf', ['1', 'G']).
card_cmc('warren-scourge elf', 2).
card_layout('warren-scourge elf', 'normal').
card_power('warren-scourge elf', 1).
card_toughness('warren-scourge elf', 1).

% Found in: STH
card_name('warrior angel', 'Warrior Angel').
card_type('warrior angel', 'Creature — Angel Warrior').
card_types('warrior angel', ['Creature']).
card_subtypes('warrior angel', ['Angel', 'Warrior']).
card_colors('warrior angel', ['W']).
card_text('warrior angel', 'Flying\nWhenever Warrior Angel deals damage, you gain that much life.').
card_mana_cost('warrior angel', ['4', 'W', 'W']).
card_cmc('warrior angel', 6).
card_layout('warrior angel', 'normal').
card_power('warrior angel', 3).
card_toughness('warrior angel', 4).

% Found in: STH, TPR
card_name('warrior en-kor', 'Warrior en-Kor').
card_type('warrior en-kor', 'Creature — Kor Warrior Knight').
card_types('warrior en-kor', ['Creature']).
card_subtypes('warrior en-kor', ['Kor', 'Warrior', 'Knight']).
card_colors('warrior en-kor', ['W']).
card_text('warrior en-kor', '{0}: The next 1 damage that would be dealt to Warrior en-Kor this turn is dealt to target creature you control instead.').
card_mana_cost('warrior en-kor', ['W', 'W']).
card_cmc('warrior en-kor', 2).
card_layout('warrior en-kor', 'normal').
card_power('warrior en-kor', 2).
card_toughness('warrior en-kor', 2).

% Found in: POR
card_name('warrior\'s charge', 'Warrior\'s Charge').
card_type('warrior\'s charge', 'Sorcery').
card_types('warrior\'s charge', ['Sorcery']).
card_subtypes('warrior\'s charge', []).
card_colors('warrior\'s charge', ['W']).
card_text('warrior\'s charge', 'Creatures you control get +1/+1 until end of turn.').
card_mana_cost('warrior\'s charge', ['2', 'W']).
card_cmc('warrior\'s charge', 3).
card_layout('warrior\'s charge', 'normal').

% Found in: 10E, 6ED, 9ED, ATH, VIS
card_name('warrior\'s honor', 'Warrior\'s Honor').
card_type('warrior\'s honor', 'Instant').
card_types('warrior\'s honor', ['Instant']).
card_subtypes('warrior\'s honor', []).
card_colors('warrior\'s honor', ['W']).
card_text('warrior\'s honor', 'Creatures you control get +1/+1 until end of turn.').
card_mana_cost('warrior\'s honor', ['2', 'W']).
card_cmc('warrior\'s honor', 3).
card_layout('warrior\'s honor', 'normal').

% Found in: PTK
card_name('warrior\'s oath', 'Warrior\'s Oath').
card_type('warrior\'s oath', 'Sorcery').
card_types('warrior\'s oath', ['Sorcery']).
card_subtypes('warrior\'s oath', []).
card_colors('warrior\'s oath', ['R']).
card_text('warrior\'s oath', 'Take an extra turn after this one. At the beginning of that turn\'s end step, you lose the game.').
card_mana_cost('warrior\'s oath', ['R', 'R']).
card_cmc('warrior\'s oath', 2).
card_layout('warrior\'s oath', 'normal').

% Found in: PO2, PTK
card_name('warrior\'s stand', 'Warrior\'s Stand').
card_type('warrior\'s stand', 'Instant').
card_types('warrior\'s stand', ['Instant']).
card_subtypes('warrior\'s stand', []).
card_colors('warrior\'s stand', ['W']).
card_text('warrior\'s stand', 'Cast Warrior\'s Stand only during the declare attackers step and only if you\'ve been attacked this step.\nCreatures you control get +2/+2 until end of turn.').
card_mana_cost('warrior\'s stand', ['1', 'W']).
card_cmc('warrior\'s stand', 2).
card_layout('warrior\'s stand', 'normal').

% Found in: THS
card_name('warriors\' lesson', 'Warriors\' Lesson').
card_type('warriors\' lesson', 'Instant').
card_types('warriors\' lesson', ['Instant']).
card_subtypes('warriors\' lesson', []).
card_colors('warriors\' lesson', ['G']).
card_text('warriors\' lesson', 'Until end of turn, up to two target creatures you control each gain \"Whenever this creature deals combat damage to a player, draw a card.\"').
card_mana_cost('warriors\' lesson', ['G']).
card_cmc('warriors\' lesson', 1).
card_layout('warriors\' lesson', 'normal').

% Found in: C13, M12, PC2
card_name('warstorm surge', 'Warstorm Surge').
card_type('warstorm surge', 'Enchantment').
card_types('warstorm surge', ['Enchantment']).
card_subtypes('warstorm surge', []).
card_colors('warstorm surge', ['R']).
card_text('warstorm surge', 'Whenever a creature enters the battlefield under your control, it deals damage equal to its power to target creature or player.').
card_mana_cost('warstorm surge', ['5', 'R']).
card_cmc('warstorm surge', 6).
card_layout('warstorm surge', 'normal').

% Found in: 6ED, VIS
card_name('warthog', 'Warthog').
card_type('warthog', 'Creature — Boar').
card_types('warthog', ['Creature']).
card_subtypes('warthog', ['Boar']).
card_colors('warthog', ['G']).
card_text('warthog', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('warthog', ['1', 'G', 'G']).
card_cmc('warthog', 3).
card_layout('warthog', 'normal').
card_power('warthog', 3).
card_toughness('warthog', 2).

% Found in: C13, INV, pMEI
card_name('wash out', 'Wash Out').
card_type('wash out', 'Sorcery').
card_types('wash out', ['Sorcery']).
card_subtypes('wash out', []).
card_colors('wash out', ['U']).
card_text('wash out', 'Return all permanents of the color of your choice to their owners\' hands.').
card_mana_cost('wash out', ['3', 'U']).
card_cmc('wash out', 4).
card_layout('wash out', 'normal').

% Found in: SHM
card_name('wasp lancer', 'Wasp Lancer').
card_type('wasp lancer', 'Creature — Faerie Soldier').
card_types('wasp lancer', ['Creature']).
card_subtypes('wasp lancer', ['Faerie', 'Soldier']).
card_colors('wasp lancer', ['U', 'B']).
card_text('wasp lancer', 'Flying').
card_mana_cost('wasp lancer', ['U/B', 'U/B', 'U/B']).
card_cmc('wasp lancer', 3).
card_layout('wasp lancer', 'normal').
card_power('wasp lancer', 3).
card_toughness('wasp lancer', 2).

% Found in: TOR
card_name('waste away', 'Waste Away').
card_type('waste away', 'Instant').
card_types('waste away', ['Instant']).
card_subtypes('waste away', []).
card_colors('waste away', ['B']).
card_text('waste away', 'As an additional cost to cast Waste Away, discard a card.\nTarget creature gets -5/-5 until end of turn.').
card_mana_cost('waste away', ['4', 'B']).
card_cmc('waste away', 5).
card_layout('waste away', 'normal').

% Found in: M15
card_name('waste not', 'Waste Not').
card_type('waste not', 'Enchantment').
card_types('waste not', ['Enchantment']).
card_subtypes('waste not', []).
card_colors('waste not', ['B']).
card_text('waste not', 'Whenever an opponent discards a creature card, put a 2/2 black Zombie creature token onto the battlefield.\nWhenever an opponent discards a land card, add {B}{B} to your mana pool.\nWhenever an opponent discards a noncreature, nonland card, draw a card.').
card_mana_cost('waste not', ['1', 'B']).
card_cmc('waste not', 2).
card_layout('waste not', 'normal').

% Found in: TMP, TPR, pJGP, pMPR
card_name('wasteland', 'Wasteland').
card_type('wasteland', 'Land').
card_types('wasteland', ['Land']).
card_subtypes('wasteland', []).
card_colors('wasteland', []).
card_text('wasteland', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Wasteland: Destroy target nonbasic land.').
card_layout('wasteland', 'normal').

% Found in: BFZ
card_name('wasteland strangler', 'Wasteland Strangler').
card_type('wasteland strangler', 'Creature — Eldrazi Processor').
card_types('wasteland strangler', ['Creature']).
card_subtypes('wasteland strangler', ['Eldrazi', 'Processor']).
card_colors('wasteland strangler', []).
card_text('wasteland strangler', 'Devoid (This card has no color.)\nWhen Wasteland Strangler enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, target creature gets -3/-3 until end of turn.').
card_mana_cost('wasteland strangler', ['2', 'B']).
card_cmc('wasteland strangler', 3).
card_layout('wasteland strangler', 'normal').
card_power('wasteland strangler', 3).
card_toughness('wasteland strangler', 2).

% Found in: GTC
card_name('wasteland viper', 'Wasteland Viper').
card_type('wasteland viper', 'Creature — Snake').
card_types('wasteland viper', ['Creature']).
card_subtypes('wasteland viper', ['Snake']).
card_colors('wasteland viper', ['G']).
card_text('wasteland viper', 'Deathtouch\nBloodrush — {G}, Discard Wasteland Viper: Target attacking creature gets +1/+2 and gains deathtouch until end of turn.').
card_mana_cost('wasteland viper', ['G']).
card_cmc('wasteland viper', 1).
card_layout('wasteland viper', 'normal').
card_power('wasteland viper', 1).
card_toughness('wasteland viper', 2).

% Found in: TMP
card_name('watchdog', 'Watchdog').
card_type('watchdog', 'Artifact Creature — Hound').
card_types('watchdog', ['Artifact', 'Creature']).
card_subtypes('watchdog', ['Hound']).
card_colors('watchdog', []).
card_text('watchdog', 'Watchdog blocks each turn if able.\nAs long as Watchdog is untapped, all creatures attacking you get -1/-0.').
card_mana_cost('watchdog', ['3']).
card_cmc('watchdog', 3).
card_layout('watchdog', 'normal').
card_power('watchdog', 1).
card_toughness('watchdog', 2).

% Found in: FRF_UGIN, KTK
card_name('watcher of the roost', 'Watcher of the Roost').
card_type('watcher of the roost', 'Creature — Bird Soldier').
card_types('watcher of the roost', ['Creature']).
card_subtypes('watcher of the roost', ['Bird', 'Soldier']).
card_colors('watcher of the roost', ['W']).
card_text('watcher of the roost', 'Flying\nMorph—Reveal a white card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Watcher of the Roost is turned face up, you gain 2 life.').
card_mana_cost('watcher of the roost', ['2', 'W']).
card_cmc('watcher of the roost', 3).
card_layout('watcher of the roost', 'normal').
card_power('watcher of the roost', 2).
card_toughness('watcher of the roost', 1).

% Found in: TSP
card_name('watcher sliver', 'Watcher Sliver').
card_type('watcher sliver', 'Creature — Sliver').
card_types('watcher sliver', ['Creature']).
card_subtypes('watcher sliver', ['Sliver']).
card_colors('watcher sliver', ['W']).
card_text('watcher sliver', 'All Sliver creatures get +0/+2.').
card_mana_cost('watcher sliver', ['3', 'W']).
card_cmc('watcher sliver', 4).
card_layout('watcher sliver', 'normal').
card_power('watcher sliver', 2).
card_toughness('watcher sliver', 2).

% Found in: SHM
card_name('watchwing scarecrow', 'Watchwing Scarecrow').
card_type('watchwing scarecrow', 'Artifact Creature — Scarecrow').
card_types('watchwing scarecrow', ['Artifact', 'Creature']).
card_subtypes('watchwing scarecrow', ['Scarecrow']).
card_colors('watchwing scarecrow', []).
card_text('watchwing scarecrow', 'Watchwing Scarecrow has vigilance as long as you control a white creature.\nWatchwing Scarecrow has flying as long as you control a blue creature.').
card_mana_cost('watchwing scarecrow', ['4']).
card_cmc('watchwing scarecrow', 4).
card_layout('watchwing scarecrow', 'normal').
card_power('watchwing scarecrow', 2).
card_toughness('watchwing scarecrow', 4).

% Found in: ARC, RAV, pFNM
card_name('watchwolf', 'Watchwolf').
card_type('watchwolf', 'Creature — Wolf').
card_types('watchwolf', ['Creature']).
card_subtypes('watchwolf', ['Wolf']).
card_colors('watchwolf', ['W', 'G']).
card_text('watchwolf', '').
card_mana_cost('watchwolf', ['G', 'W']).
card_cmc('watchwolf', 2).
card_layout('watchwolf', 'normal').
card_power('watchwolf', 3).
card_toughness('watchwolf', 3).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4, S99
card_name('water elemental', 'Water Elemental').
card_type('water elemental', 'Creature — Elemental').
card_types('water elemental', ['Creature']).
card_subtypes('water elemental', ['Elemental']).
card_colors('water elemental', ['U']).
card_text('water elemental', '').
card_mana_cost('water elemental', ['3', 'U', 'U']).
card_cmc('water elemental', 5).
card_layout('water elemental', 'normal').
card_power('water elemental', 5).
card_toughness('water elemental', 4).

% Found in: UNH
card_name('water gun balloon game', 'Water Gun Balloon Game').
card_type('water gun balloon game', 'Artifact').
card_types('water gun balloon game', ['Artifact']).
card_subtypes('water gun balloon game', []).
card_colors('water gun balloon game', []).
card_text('water gun balloon game', 'As Water Gun Balloon Game comes into play, each player puts a pop counter on a 0.\nWhenever a player plays a spell, move that player\'s pop counter up 1.\nWhenever a player\'s pop counter hits 5, that player puts a 5/5 pink Giant Teddy Bear creature token into play and resets all pop counters to 0.').
card_mana_cost('water gun balloon game', ['2']).
card_cmc('water gun balloon game', 2).
card_layout('water gun balloon game', 'normal').

% Found in: M11, M14, MM2
card_name('water servant', 'Water Servant').
card_type('water servant', 'Creature — Elemental').
card_types('water servant', ['Creature']).
card_subtypes('water servant', ['Elemental']).
card_colors('water servant', ['U']).
card_text('water servant', '{U}: Water Servant gets +1/-1 until end of turn.\n{U}: Water Servant gets -1/+1 until end of turn.').
card_mana_cost('water servant', ['2', 'U', 'U']).
card_cmc('water servant', 4).
card_layout('water servant', 'normal').
card_power('water servant', 3).
card_toughness('water servant', 4).

% Found in: DRK
card_name('water wurm', 'Water Wurm').
card_type('water wurm', 'Creature — Wurm').
card_types('water wurm', ['Creature']).
card_subtypes('water wurm', ['Wurm']).
card_colors('water wurm', ['U']).
card_text('water wurm', 'Water Wurm gets +0/+1 as long as an opponent controls an Island.').
card_mana_cost('water wurm', ['U']).
card_cmc('water wurm', 1).
card_layout('water wurm', 'normal').
card_power('water wurm', 1).
card_toughness('water wurm', 1).

% Found in: M13, ORI
card_name('watercourser', 'Watercourser').
card_type('watercourser', 'Creature — Elemental').
card_types('watercourser', ['Creature']).
card_subtypes('watercourser', ['Elemental']).
card_colors('watercourser', ['U']).
card_text('watercourser', '{U}: Watercourser gets +1/-1 until end of turn.').
card_mana_cost('watercourser', ['2', 'U']).
card_cmc('watercourser', 3).
card_layout('watercourser', 'normal').
card_power('watercourser', 2).
card_toughness('watercourser', 3).

% Found in: MMQ, VMA
card_name('waterfront bouncer', 'Waterfront Bouncer').
card_type('waterfront bouncer', 'Creature — Merfolk Spellshaper').
card_types('waterfront bouncer', ['Creature']).
card_subtypes('waterfront bouncer', ['Merfolk', 'Spellshaper']).
card_colors('waterfront bouncer', ['U']).
card_text('waterfront bouncer', '{U}, {T}, Discard a card: Return target creature to its owner\'s hand.').
card_mana_cost('waterfront bouncer', ['1', 'U']).
card_cmc('waterfront bouncer', 2).
card_layout('waterfront bouncer', 'normal').
card_power('waterfront bouncer', 1).
card_toughness('waterfront bouncer', 1).

% Found in: DD2, DD3_JVC, VIS
card_name('waterspout djinn', 'Waterspout Djinn').
card_type('waterspout djinn', 'Creature — Djinn').
card_types('waterspout djinn', ['Creature']).
card_subtypes('waterspout djinn', ['Djinn']).
card_colors('waterspout djinn', ['U']).
card_text('waterspout djinn', 'Flying\nAt the beginning of your upkeep, sacrifice Waterspout Djinn unless you return an untapped Island you control to its owner\'s hand.').
card_mana_cost('waterspout djinn', ['2', 'U', 'U']).
card_cmc('waterspout djinn', 4).
card_layout('waterspout djinn', 'normal').
card_power('waterspout djinn', 4).
card_toughness('waterspout djinn', 4).

% Found in: PLS
card_name('waterspout elemental', 'Waterspout Elemental').
card_type('waterspout elemental', 'Creature — Elemental').
card_types('waterspout elemental', ['Creature']).
card_subtypes('waterspout elemental', ['Elemental']).
card_colors('waterspout elemental', ['U']).
card_text('waterspout elemental', 'Kicker {U} (You may pay an additional {U} as you cast this spell.)\nFlying\nWhen Waterspout Elemental enters the battlefield, if it was kicked, return all other creatures to their owners\' hands and you skip your next turn.').
card_mana_cost('waterspout elemental', ['3', 'U', 'U']).
card_cmc('waterspout elemental', 5).
card_layout('waterspout elemental', 'normal').
card_power('waterspout elemental', 3).
card_toughness('waterspout elemental', 4).

% Found in: MOR
card_name('waterspout weavers', 'Waterspout Weavers').
card_type('waterspout weavers', 'Creature — Merfolk Wizard').
card_types('waterspout weavers', ['Creature']).
card_subtypes('waterspout weavers', ['Merfolk', 'Wizard']).
card_colors('waterspout weavers', ['U']).
card_text('waterspout weavers', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Waterspout Weavers, you may reveal it. If you do, each creature you control gains flying until end of turn.').
card_mana_cost('waterspout weavers', ['3', 'U', 'U']).
card_cmc('waterspout weavers', 5).
card_layout('waterspout weavers', 'normal').
card_power('waterspout weavers', 3).
card_toughness('waterspout weavers', 3).

% Found in: CHK
card_name('waterveil cavern', 'Waterveil Cavern').
card_type('waterveil cavern', 'Land').
card_types('waterveil cavern', ['Land']).
card_subtypes('waterveil cavern', []).
card_colors('waterveil cavern', []).
card_text('waterveil cavern', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Waterveil Cavern doesn\'t untap during your next untap step.').
card_layout('waterveil cavern', 'normal').

% Found in: KTK
card_name('waterwhirl', 'Waterwhirl').
card_type('waterwhirl', 'Instant').
card_types('waterwhirl', ['Instant']).
card_subtypes('waterwhirl', []).
card_colors('waterwhirl', ['U']).
card_text('waterwhirl', 'Return up to two target creatures to their owners\' hands.').
card_mana_cost('waterwhirl', ['4', 'U', 'U']).
card_cmc('waterwhirl', 6).
card_layout('waterwhirl', 'normal').

% Found in: EXP, GTC, RAV
card_name('watery grave', 'Watery Grave').
card_type('watery grave', 'Land — Island Swamp').
card_types('watery grave', ['Land']).
card_subtypes('watery grave', ['Island', 'Swamp']).
card_colors('watery grave', []).
card_text('watery grave', '({T}: Add {U} or {B} to your mana pool.)\nAs Watery Grave enters the battlefield, you may pay 2 life. If you don\'t, Watery Grave enters the battlefield tapped.').
card_layout('watery grave', 'normal').

% Found in: MIR
card_name('wave elemental', 'Wave Elemental').
card_type('wave elemental', 'Creature — Elemental').
card_types('wave elemental', ['Creature']).
card_subtypes('wave elemental', ['Elemental']).
card_colors('wave elemental', ['U']).
card_text('wave elemental', '{U}, {T}, Sacrifice Wave Elemental: Tap up to three target creatures without flying.').
card_mana_cost('wave elemental', ['2', 'U', 'U']).
card_cmc('wave elemental', 4).
card_layout('wave elemental', 'normal').
card_power('wave elemental', 2).
card_toughness('wave elemental', 3).

% Found in: ONS
card_name('wave of indifference', 'Wave of Indifference').
card_type('wave of indifference', 'Sorcery').
card_types('wave of indifference', ['Sorcery']).
card_subtypes('wave of indifference', []).
card_colors('wave of indifference', ['R']).
card_text('wave of indifference', 'X target creatures can\'t block this turn.').
card_mana_cost('wave of indifference', ['X', 'R']).
card_cmc('wave of indifference', 1).
card_layout('wave of indifference', 'normal').

% Found in: MMQ
card_name('wave of reckoning', 'Wave of Reckoning').
card_type('wave of reckoning', 'Sorcery').
card_types('wave of reckoning', ['Sorcery']).
card_subtypes('wave of reckoning', []).
card_colors('wave of reckoning', ['W']).
card_text('wave of reckoning', 'Each creature deals damage to itself equal to its power.').
card_mana_cost('wave of reckoning', ['4', 'W']).
card_cmc('wave of reckoning', 5).
card_layout('wave of reckoning', 'normal').

% Found in: WTH
card_name('wave of terror', 'Wave of Terror').
card_type('wave of terror', 'Enchantment').
card_types('wave of terror', ['Enchantment']).
card_subtypes('wave of terror', []).
card_colors('wave of terror', ['B']).
card_text('wave of terror', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of your draw step, destroy each creature with converted mana cost equal to the number of age counters on Wave of Terror. They can\'t be regenerated.').
card_mana_cost('wave of terror', ['2', 'B']).
card_cmc('wave of terror', 3).
card_layout('wave of terror', 'normal').
card_reserved('wave of terror').

% Found in: C14
card_name('wave of vitriol', 'Wave of Vitriol').
card_type('wave of vitriol', 'Sorcery').
card_types('wave of vitriol', ['Sorcery']).
card_subtypes('wave of vitriol', []).
card_colors('wave of vitriol', ['G']).
card_text('wave of vitriol', 'Each player sacrifices all artifacts, enchantments, and nonbasic lands he or she controls. For each land sacrificed this way, its controller may search his or her library for a basic land card and put it onto the battlefield tapped. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('wave of vitriol', ['5', 'G', 'G']).
card_cmc('wave of vitriol', 7).
card_layout('wave of vitriol', 'normal').

% Found in: BFZ
card_name('wave-wing elemental', 'Wave-Wing Elemental').
card_type('wave-wing elemental', 'Creature — Elemental').
card_types('wave-wing elemental', ['Creature']).
card_subtypes('wave-wing elemental', ['Elemental']).
card_colors('wave-wing elemental', ['U']).
card_text('wave-wing elemental', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, Wave-Wing Elemental gets +2/+2 until end of turn.').
card_mana_cost('wave-wing elemental', ['5', 'U']).
card_cmc('wave-wing elemental', 6).
card_layout('wave-wing elemental', 'normal').
card_power('wave-wing elemental', 3).
card_toughness('wave-wing elemental', 4).

% Found in: THS
card_name('wavecrash triton', 'Wavecrash Triton').
card_type('wavecrash triton', 'Creature — Merfolk Wizard').
card_types('wavecrash triton', ['Creature']).
card_subtypes('wavecrash triton', ['Merfolk', 'Wizard']).
card_colors('wavecrash triton', ['U']).
card_text('wavecrash triton', 'Heroic — Whenever you cast a spell that targets Wavecrash Triton, tap target creature an opponent controls. That creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('wavecrash triton', ['2', 'U']).
card_cmc('wavecrash triton', 3).
card_layout('wavecrash triton', 'normal').
card_power('wavecrash triton', 1).
card_toughness('wavecrash triton', 4).

% Found in: EVE
card_name('waves of aggression', 'Waves of Aggression').
card_type('waves of aggression', 'Sorcery').
card_types('waves of aggression', ['Sorcery']).
card_subtypes('waves of aggression', []).
card_colors('waves of aggression', ['W', 'R']).
card_text('waves of aggression', 'Untap all creatures that attacked this turn. After this main phase, there is an additional combat phase followed by an additional main phase.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('waves of aggression', ['3', 'R/W', 'R/W']).
card_cmc('waves of aggression', 5).
card_layout('waves of aggression', 'normal').

% Found in: ALA
card_name('waveskimmer aven', 'Waveskimmer Aven').
card_type('waveskimmer aven', 'Creature — Bird Soldier').
card_types('waveskimmer aven', ['Creature']).
card_subtypes('waveskimmer aven', ['Bird', 'Soldier']).
card_colors('waveskimmer aven', ['W', 'U', 'G']).
card_text('waveskimmer aven', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('waveskimmer aven', ['2', 'G', 'W', 'U']).
card_cmc('waveskimmer aven', 5).
card_layout('waveskimmer aven', 'normal').
card_power('waveskimmer aven', 2).
card_toughness('waveskimmer aven', 4).

% Found in: ARC, INV
card_name('wax', 'Wax').
card_type('wax', 'Instant').
card_types('wax', ['Instant']).
card_subtypes('wax', []).
card_colors('wax', ['G']).
card_text('wax', 'Target creature gets +2/+2 until end of turn.').
card_mana_cost('wax', ['G']).
card_cmc('wax', 1).
card_layout('wax', 'split').
card_sides('wax', 'wane').

% Found in: BOK, MM2
card_name('waxmane baku', 'Waxmane Baku').
card_type('waxmane baku', 'Creature — Spirit').
card_types('waxmane baku', ['Creature']).
card_subtypes('waxmane baku', ['Spirit']).
card_colors('waxmane baku', ['W']).
card_text('waxmane baku', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Waxmane Baku.\n{1}, Remove X ki counters from Waxmane Baku: Tap X target creatures.').
card_mana_cost('waxmane baku', ['2', 'W']).
card_cmc('waxmane baku', 3).
card_layout('waxmane baku', 'normal').
card_power('waxmane baku', 2).
card_toughness('waxmane baku', 2).

% Found in: GTC
card_name('way of the thief', 'Way of the Thief').
card_type('way of the thief', 'Enchantment — Aura').
card_types('way of the thief', ['Enchantment']).
card_subtypes('way of the thief', ['Aura']).
card_colors('way of the thief', ['U']).
card_text('way of the thief', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature can\'t be blocked as long as you control a Gate.').
card_mana_cost('way of the thief', ['3', 'U']).
card_cmc('way of the thief', 4).
card_layout('way of the thief', 'normal').

% Found in: 5DN, C13, C14, DDI, MM2
card_name('wayfarer\'s bauble', 'Wayfarer\'s Bauble').
card_type('wayfarer\'s bauble', 'Artifact').
card_types('wayfarer\'s bauble', ['Artifact']).
card_subtypes('wayfarer\'s bauble', []).
card_colors('wayfarer\'s bauble', []).
card_text('wayfarer\'s bauble', '{2}, {T}, Sacrifice Wayfarer\'s Bauble: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('wayfarer\'s bauble', ['1']).
card_cmc('wayfarer\'s bauble', 1).
card_layout('wayfarer\'s bauble', 'normal').

% Found in: INV
card_name('wayfaring giant', 'Wayfaring Giant').
card_type('wayfaring giant', 'Creature — Giant').
card_types('wayfaring giant', ['Creature']).
card_subtypes('wayfaring giant', ['Giant']).
card_colors('wayfaring giant', ['W']).
card_text('wayfaring giant', 'Domain — Wayfaring Giant gets +1/+1 for each basic land type among lands you control.').
card_mana_cost('wayfaring giant', ['5', 'W']).
card_cmc('wayfaring giant', 6).
card_layout('wayfaring giant', 'normal').
card_power('wayfaring giant', 1).
card_toughness('wayfaring giant', 3).

% Found in: RTR
card_name('wayfaring temple', 'Wayfaring Temple').
card_type('wayfaring temple', 'Creature — Elemental').
card_types('wayfaring temple', ['Creature']).
card_subtypes('wayfaring temple', ['Elemental']).
card_colors('wayfaring temple', ['W', 'G']).
card_text('wayfaring temple', 'Wayfaring Temple\'s power and toughness are each equal to the number of creatures you control.\nWhenever Wayfaring Temple deals combat damage to a player, populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('wayfaring temple', ['1', 'G', 'W']).
card_cmc('wayfaring temple', 3).
card_layout('wayfaring temple', 'normal').
card_power('wayfaring temple', '*').
card_toughness('wayfaring temple', '*').

% Found in: USG
card_name('waylay', 'Waylay').
card_type('waylay', 'Instant').
card_types('waylay', ['Instant']).
card_subtypes('waylay', []).
card_colors('waylay', ['W']).
card_text('waylay', 'Put three 2/2 white Knight creature tokens onto the battlefield. Exile them at the beginning of the next cleanup step.').
card_mana_cost('waylay', ['2', 'W']).
card_cmc('waylay', 3).
card_layout('waylay', 'normal').

% Found in: ODY
card_name('wayward angel', 'Wayward Angel').
card_type('wayward angel', 'Creature — Angel Horror').
card_types('wayward angel', ['Creature']).
card_subtypes('wayward angel', ['Angel', 'Horror']).
card_colors('wayward angel', ['W']).
card_text('wayward angel', 'Flying, vigilance\nThreshold — As long as seven or more cards are in your graveyard, Wayward Angel gets +3/+3, is black, has trample, and has \"At the beginning of your upkeep, sacrifice a creature.\"').
card_mana_cost('wayward angel', ['4', 'W', 'W']).
card_cmc('wayward angel', 6).
card_layout('wayward angel', 'normal').
card_power('wayward angel', 4).
card_toughness('wayward angel', 4).

% Found in: BTD, EXO, TPR
card_name('wayward soul', 'Wayward Soul').
card_type('wayward soul', 'Creature — Spirit').
card_types('wayward soul', ['Creature']).
card_subtypes('wayward soul', ['Spirit']).
card_colors('wayward soul', ['U']).
card_text('wayward soul', 'Flying\n{U}: Put Wayward Soul on top of its owner\'s library.').
card_mana_cost('wayward soul', ['2', 'U', 'U']).
card_cmc('wayward soul', 4).
card_layout('wayward soul', 'normal').
card_power('wayward soul', 3).
card_toughness('wayward soul', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, BRB, CED, CEI, ITP, LEA, LEB, M10, ME4, RQS
card_name('weakness', 'Weakness').
card_type('weakness', 'Enchantment — Aura').
card_types('weakness', ['Enchantment']).
card_subtypes('weakness', ['Aura']).
card_colors('weakness', ['B']).
card_text('weakness', 'Enchant creature\nEnchanted creature gets -2/-1.').
card_mana_cost('weakness', ['B']).
card_cmc('weakness', 1).
card_layout('weakness', 'normal').

% Found in: ATQ, ME4
card_name('weakstone', 'Weakstone').
card_type('weakstone', 'Artifact').
card_types('weakstone', ['Artifact']).
card_subtypes('weakstone', []).
card_colors('weakstone', []).
card_text('weakstone', 'Attacking creatures get -1/-0.').
card_mana_cost('weakstone', ['4']).
card_cmc('weakstone', 4).
card_layout('weakstone', 'normal').
card_reserved('weakstone').

% Found in: DGM
card_name('weapon surge', 'Weapon Surge').
card_type('weapon surge', 'Instant').
card_types('weapon surge', ['Instant']).
card_subtypes('weapon surge', []).
card_colors('weapon surge', ['R']).
card_text('weapon surge', 'Target creature you control gets +1/+0 and gains first strike until end of turn.\nOverload {1}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('weapon surge', ['R']).
card_cmc('weapon surge', 1).
card_layout('weapon surge', 'normal').

% Found in: DGM
card_name('wear', 'Wear').
card_type('wear', 'Instant').
card_types('wear', ['Instant']).
card_subtypes('wear', []).
card_colors('wear', ['R']).
card_text('wear', 'Destroy target artifact.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('wear', ['1', 'R']).
card_cmc('wear', 2).
card_layout('wear', 'split').
card_sides('wear', 'tear').

% Found in: CHK
card_name('wear away', 'Wear Away').
card_type('wear away', 'Instant — Arcane').
card_types('wear away', ['Instant']).
card_subtypes('wear away', ['Arcane']).
card_colors('wear away', ['G']).
card_text('wear away', 'Destroy target artifact or enchantment.\nSplice onto Arcane {3}{G} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('wear away', ['G', 'G']).
card_cmc('wear away', 2).
card_layout('wear away', 'normal').

% Found in: TSP
card_name('weathered bodyguards', 'Weathered Bodyguards').
card_type('weathered bodyguards', 'Creature — Human Soldier').
card_types('weathered bodyguards', ['Creature']).
card_subtypes('weathered bodyguards', ['Human', 'Soldier']).
card_colors('weathered bodyguards', ['W']).
card_text('weathered bodyguards', 'As long as Weathered Bodyguards is untapped, all combat damage that would be dealt to you by unblocked creatures is dealt to Weathered Bodyguards instead.\nMorph {3}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('weathered bodyguards', ['5', 'W']).
card_cmc('weathered bodyguards', 6).
card_layout('weathered bodyguards', 'normal').
card_power('weathered bodyguards', 2).
card_toughness('weathered bodyguards', 5).

% Found in: 9ED, ONS
card_name('weathered wayfarer', 'Weathered Wayfarer').
card_type('weathered wayfarer', 'Creature — Human Nomad Cleric').
card_types('weathered wayfarer', ['Creature']).
card_subtypes('weathered wayfarer', ['Human', 'Nomad', 'Cleric']).
card_colors('weathered wayfarer', ['W']).
card_text('weathered wayfarer', '{W}, {T}: Search your library for a land card, reveal it, and put it into your hand. Then shuffle your library. Activate this ability only if an opponent controls more lands than you.').
card_mana_cost('weathered wayfarer', ['W']).
card_cmc('weathered wayfarer', 1).
card_layout('weathered wayfarer', 'normal').
card_power('weathered wayfarer', 1).
card_toughness('weathered wayfarer', 1).

% Found in: ULG
card_name('weatherseed elf', 'Weatherseed Elf').
card_type('weatherseed elf', 'Creature — Elf').
card_types('weatherseed elf', ['Creature']).
card_subtypes('weatherseed elf', ['Elf']).
card_colors('weatherseed elf', ['G']).
card_text('weatherseed elf', '{T}: Target creature gains forestwalk until end of turn. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('weatherseed elf', ['G']).
card_cmc('weatherseed elf', 1).
card_layout('weatherseed elf', 'normal').
card_power('weatherseed elf', 1).
card_toughness('weatherseed elf', 1).

% Found in: ULG
card_name('weatherseed faeries', 'Weatherseed Faeries').
card_type('weatherseed faeries', 'Creature — Faerie').
card_types('weatherseed faeries', ['Creature']).
card_subtypes('weatherseed faeries', ['Faerie']).
card_colors('weatherseed faeries', ['U']).
card_text('weatherseed faeries', 'Flying, protection from red').
card_mana_cost('weatherseed faeries', ['2', 'U']).
card_cmc('weatherseed faeries', 3).
card_layout('weatherseed faeries', 'normal').
card_power('weatherseed faeries', 2).
card_toughness('weatherseed faeries', 1).

% Found in: TSP
card_name('weatherseed totem', 'Weatherseed Totem').
card_type('weatherseed totem', 'Artifact').
card_types('weatherseed totem', ['Artifact']).
card_subtypes('weatherseed totem', []).
card_colors('weatherseed totem', []).
card_text('weatherseed totem', '{T}: Add {G} to your mana pool.\n{2}{G}{G}{G}: Weatherseed Totem becomes a 5/3 green Treefolk artifact creature with trample until end of turn.\nWhen Weatherseed Totem is put into a graveyard from the battlefield, if it was a creature, return this card to its owner\'s hand.').
card_mana_cost('weatherseed totem', ['3']).
card_cmc('weatherseed totem', 3).
card_layout('weatherseed totem', 'normal').

% Found in: ULG
card_name('weatherseed treefolk', 'Weatherseed Treefolk').
card_type('weatherseed treefolk', 'Creature — Treefolk').
card_types('weatherseed treefolk', ['Creature']).
card_subtypes('weatherseed treefolk', ['Treefolk']).
card_colors('weatherseed treefolk', ['G']).
card_text('weatherseed treefolk', 'Trample\nWhen Weatherseed Treefolk dies, return it to its owner\'s hand.').
card_mana_cost('weatherseed treefolk', ['2', 'G', 'G', 'G']).
card_cmc('weatherseed treefolk', 5).
card_layout('weatherseed treefolk', 'normal').
card_power('weatherseed treefolk', 5).
card_toughness('weatherseed treefolk', 3).
card_reserved('weatherseed treefolk').

% Found in: KTK, ORI
card_name('weave fate', 'Weave Fate').
card_type('weave fate', 'Instant').
card_types('weave fate', ['Instant']).
card_subtypes('weave fate', []).
card_colors('weave fate', ['U']).
card_text('weave fate', 'Draw two cards.').
card_mana_cost('weave fate', ['3', 'U']).
card_cmc('weave fate', 4).
card_layout('weave fate', 'normal').

% Found in: LGN
card_name('weaver of lies', 'Weaver of Lies').
card_type('weaver of lies', 'Creature — Beast').
card_types('weaver of lies', ['Creature']).
card_subtypes('weaver of lies', ['Beast']).
card_colors('weaver of lies', ['U']).
card_text('weaver of lies', 'Morph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Weaver of Lies is turned face up, turn any number of target creatures with morph abilities other than Weaver of Lies face down.').
card_mana_cost('weaver of lies', ['5', 'U', 'U']).
card_cmc('weaver of lies', 7).
card_layout('weaver of lies', 'normal').
card_power('weaver of lies', 4).
card_toughness('weaver of lies', 4).

% Found in: 2ED, 3ED, 4ED, 9ED, CED, CEI, LEA, LEB
card_name('web', 'Web').
card_type('web', 'Enchantment — Aura').
card_types('web', ['Enchantment']).
card_subtypes('web', ['Aura']).
card_colors('web', ['G']).
card_text('web', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature gets +0/+2 and has reach. (It can block creatures with flying.)').
card_mana_cost('web', ['G']).
card_cmc('web', 1).
card_layout('web', 'normal').

% Found in: JUD
card_name('web of inertia', 'Web of Inertia').
card_type('web of inertia', 'Enchantment').
card_types('web of inertia', ['Enchantment']).
card_subtypes('web of inertia', []).
card_colors('web of inertia', ['U']).
card_text('web of inertia', 'At the beginning of combat on each opponent\'s turn, that player may exile a card from his or her graveyard. If the player doesn\'t, creatures he or she controls can\'t attack you this turn.').
card_mana_cost('web of inertia', ['2', 'U']).
card_cmc('web of inertia', 3).
card_layout('web of inertia', 'normal').

% Found in: DDJ, GPT, pARL
card_name('wee dragonauts', 'Wee Dragonauts').
card_type('wee dragonauts', 'Creature — Faerie Wizard').
card_types('wee dragonauts', ['Creature']).
card_subtypes('wee dragonauts', ['Faerie', 'Wizard']).
card_colors('wee dragonauts', ['U', 'R']).
card_text('wee dragonauts', 'Flying\nWhenever you cast an instant or sorcery spell, Wee Dragonauts gets +2/+0 until end of turn.').
card_mana_cost('wee dragonauts', ['1', 'U', 'R']).
card_cmc('wee dragonauts', 3).
card_layout('wee dragonauts', 'normal').
card_power('wee dragonauts', 1).
card_toughness('wee dragonauts', 3).

% Found in: LRW
card_name('weed strangle', 'Weed Strangle').
card_type('weed strangle', 'Sorcery').
card_types('weed strangle', ['Sorcery']).
card_subtypes('weed strangle', []).
card_colors('weed strangle', ['B']).
card_text('weed strangle', 'Destroy target creature. Clash with an opponent. If you win, you gain life equal to that creature\'s toughness. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('weed strangle', ['3', 'B', 'B']).
card_cmc('weed strangle', 5).
card_layout('weed strangle', 'normal').

% Found in: MOR
card_name('weed-pruner poplar', 'Weed-Pruner Poplar').
card_type('weed-pruner poplar', 'Creature — Treefolk Assassin').
card_types('weed-pruner poplar', ['Creature']).
card_subtypes('weed-pruner poplar', ['Treefolk', 'Assassin']).
card_colors('weed-pruner poplar', ['B']).
card_text('weed-pruner poplar', 'At the beginning of your upkeep, target creature other than Weed-Pruner Poplar gets -1/-1 until end of turn.').
card_mana_cost('weed-pruner poplar', ['4', 'B']).
card_cmc('weed-pruner poplar', 5).
card_layout('weed-pruner poplar', 'normal').
card_power('weed-pruner poplar', 3).
card_toughness('weed-pruner poplar', 3).

% Found in: PTK
card_name('wei ambush force', 'Wei Ambush Force').
card_type('wei ambush force', 'Creature — Human Soldier').
card_types('wei ambush force', ['Creature']).
card_subtypes('wei ambush force', ['Human', 'Soldier']).
card_colors('wei ambush force', ['B']).
card_text('wei ambush force', 'Whenever Wei Ambush Force attacks, it gets +2/+0 until end of turn.').
card_mana_cost('wei ambush force', ['1', 'B']).
card_cmc('wei ambush force', 2).
card_layout('wei ambush force', 'normal').
card_power('wei ambush force', 1).
card_toughness('wei ambush force', 1).

% Found in: PTK
card_name('wei assassins', 'Wei Assassins').
card_type('wei assassins', 'Creature — Human Soldier Assassin').
card_types('wei assassins', ['Creature']).
card_subtypes('wei assassins', ['Human', 'Soldier', 'Assassin']).
card_colors('wei assassins', ['B']).
card_text('wei assassins', 'When Wei Assassins enters the battlefield, target opponent chooses a creature he or she controls. Destroy it.').
card_mana_cost('wei assassins', ['3', 'B', 'B']).
card_cmc('wei assassins', 5).
card_layout('wei assassins', 'normal').
card_power('wei assassins', 3).
card_toughness('wei assassins', 2).

% Found in: ME3, PTK
card_name('wei elite companions', 'Wei Elite Companions').
card_type('wei elite companions', 'Creature — Human Soldier').
card_types('wei elite companions', ['Creature']).
card_subtypes('wei elite companions', ['Human', 'Soldier']).
card_colors('wei elite companions', ['B']).
card_text('wei elite companions', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('wei elite companions', ['4', 'B']).
card_cmc('wei elite companions', 5).
card_layout('wei elite companions', 'normal').
card_power('wei elite companions', 3).
card_toughness('wei elite companions', 3).

% Found in: ME3, PTK
card_name('wei infantry', 'Wei Infantry').
card_type('wei infantry', 'Creature — Human Soldier').
card_types('wei infantry', ['Creature']).
card_subtypes('wei infantry', ['Human', 'Soldier']).
card_colors('wei infantry', ['B']).
card_text('wei infantry', '').
card_mana_cost('wei infantry', ['1', 'B']).
card_cmc('wei infantry', 2).
card_layout('wei infantry', 'normal').
card_power('wei infantry', 2).
card_toughness('wei infantry', 1).

% Found in: ME3, PTK
card_name('wei night raiders', 'Wei Night Raiders').
card_type('wei night raiders', 'Creature — Human Soldier').
card_types('wei night raiders', ['Creature']).
card_subtypes('wei night raiders', ['Human', 'Soldier']).
card_colors('wei night raiders', ['B']).
card_text('wei night raiders', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhenever Wei Night Raiders deals damage to an opponent, that player discards a card.').
card_mana_cost('wei night raiders', ['2', 'B', 'B']).
card_cmc('wei night raiders', 4).
card_layout('wei night raiders', 'normal').
card_power('wei night raiders', 2).
card_toughness('wei night raiders', 2).

% Found in: PTK
card_name('wei scout', 'Wei Scout').
card_type('wei scout', 'Creature — Human Soldier Scout').
card_types('wei scout', ['Creature']).
card_subtypes('wei scout', ['Human', 'Soldier', 'Scout']).
card_colors('wei scout', ['B']).
card_text('wei scout', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('wei scout', ['1', 'B']).
card_cmc('wei scout', 2).
card_layout('wei scout', 'normal').
card_power('wei scout', 1).
card_toughness('wei scout', 1).

% Found in: ME3, PTK
card_name('wei strike force', 'Wei Strike Force').
card_type('wei strike force', 'Creature — Human Soldier').
card_types('wei strike force', ['Creature']).
card_subtypes('wei strike force', ['Human', 'Soldier']).
card_colors('wei strike force', ['B']).
card_text('wei strike force', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('wei strike force', ['2', 'B']).
card_cmc('wei strike force', 3).
card_layout('wei strike force', 'normal').
card_power('wei strike force', 2).
card_toughness('wei strike force', 1).

% Found in: MOR
card_name('weight of conscience', 'Weight of Conscience').
card_type('weight of conscience', 'Enchantment — Aura').
card_types('weight of conscience', ['Enchantment']).
card_subtypes('weight of conscience', ['Aura']).
card_colors('weight of conscience', ['W']).
card_text('weight of conscience', 'Enchant creature\nEnchanted creature can\'t attack.\nTap two untapped creatures you control that share a creature type: Exile enchanted creature.').
card_mana_cost('weight of conscience', ['1', 'W']).
card_cmc('weight of conscience', 2).
card_layout('weight of conscience', 'normal').

% Found in: DIS
card_name('weight of spires', 'Weight of Spires').
card_type('weight of spires', 'Instant').
card_types('weight of spires', ['Instant']).
card_subtypes('weight of spires', []).
card_colors('weight of spires', ['R']).
card_text('weight of spires', 'Weight of Spires deals damage to target creature equal to the number of nonbasic lands that creature\'s controller controls.').
card_mana_cost('weight of spires', ['R']).
card_cmc('weight of spires', 1).
card_layout('weight of spires', 'normal').

% Found in: BNG, ORI
card_name('weight of the underworld', 'Weight of the Underworld').
card_type('weight of the underworld', 'Enchantment — Aura').
card_types('weight of the underworld', ['Enchantment']).
card_subtypes('weight of the underworld', ['Aura']).
card_colors('weight of the underworld', ['B']).
card_text('weight of the underworld', 'Enchant creature\nEnchanted creature gets -3/-2.').
card_mana_cost('weight of the underworld', ['3', 'B']).
card_cmc('weight of the underworld', 4).
card_layout('weight of the underworld', 'normal').

% Found in: 9ED, ONS
card_name('weird harvest', 'Weird Harvest').
card_type('weird harvest', 'Sorcery').
card_types('weird harvest', ['Sorcery']).
card_subtypes('weird harvest', []).
card_colors('weird harvest', ['G']).
card_text('weird harvest', 'Each player may search his or her library for up to X creature cards, reveal those cards, and put them into his or her hand. Then each player who searched his or her library this way shuffles it.').
card_mana_cost('weird harvest', ['X', 'G', 'G']).
card_cmc('weird harvest', 2).
card_layout('weird harvest', 'normal').

% Found in: MOR
card_name('weirding shaman', 'Weirding Shaman').
card_type('weirding shaman', 'Creature — Goblin Shaman').
card_types('weirding shaman', ['Creature']).
card_subtypes('weirding shaman', ['Goblin', 'Shaman']).
card_colors('weirding shaman', ['B']).
card_text('weirding shaman', '{3}{B}, Sacrifice a Goblin: Put two 1/1 black Goblin Rogue creature tokens onto the battlefield.').
card_mana_cost('weirding shaman', ['1', 'B']).
card_cmc('weirding shaman', 2).
card_layout('weirding shaman', 'normal').
card_power('weirding shaman', 2).
card_toughness('weirding shaman', 1).

% Found in: MRD
card_name('welding jar', 'Welding Jar').
card_type('welding jar', 'Artifact').
card_types('welding jar', ['Artifact']).
card_subtypes('welding jar', []).
card_colors('welding jar', []).
card_text('welding jar', 'Sacrifice Welding Jar: Regenerate target artifact.').
card_mana_cost('welding jar', ['0']).
card_cmc('welding jar', 0).
card_layout('welding jar', 'normal').

% Found in: ALA
card_name('welkin guide', 'Welkin Guide').
card_type('welkin guide', 'Creature — Bird Cleric').
card_types('welkin guide', ['Creature']).
card_subtypes('welkin guide', ['Bird', 'Cleric']).
card_colors('welkin guide', ['W']).
card_text('welkin guide', 'Flying\nWhen Welkin Guide enters the battlefield, target creature gets +2/+2 and gains flying until end of turn.').
card_mana_cost('welkin guide', ['4', 'W']).
card_cmc('welkin guide', 5).
card_layout('welkin guide', 'normal').
card_power('welkin guide', 2).
card_toughness('welkin guide', 2).

% Found in: EXO
card_name('welkin hawk', 'Welkin Hawk').
card_type('welkin hawk', 'Creature — Bird').
card_types('welkin hawk', ['Creature']).
card_subtypes('welkin hawk', ['Bird']).
card_colors('welkin hawk', ['W']).
card_text('welkin hawk', 'Flying\nWhen Welkin Hawk dies, you may search your library for a card named Welkin Hawk, reveal that card, put it into your hand, then shuffle your library.').
card_mana_cost('welkin hawk', ['1', 'W']).
card_cmc('welkin hawk', 2).
card_layout('welkin hawk', 'normal').
card_power('welkin hawk', 1).
card_toughness('welkin hawk', 1).

% Found in: M13, M15, ZEN
card_name('welkin tern', 'Welkin Tern').
card_type('welkin tern', 'Creature — Bird').
card_types('welkin tern', ['Creature']).
card_subtypes('welkin tern', ['Bird']).
card_colors('welkin tern', ['U']).
card_text('welkin tern', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWelkin Tern can block only creatures with flying.').
card_mana_cost('welkin tern', ['1', 'U']).
card_cmc('welkin tern', 2).
card_layout('welkin tern', 'normal').
card_power('welkin tern', 2).
card_toughness('welkin tern', 1).

% Found in: DGM
card_name('well', 'Well').
card_type('well', 'Sorcery').
card_types('well', ['Sorcery']).
card_subtypes('well', []).
card_colors('well', ['W']).
card_text('well', 'You gain 2 life for each creature you control.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('well', ['W']).
card_cmc('well', 1).
card_layout('well', 'split').

% Found in: PCY
card_name('well of discovery', 'Well of Discovery').
card_type('well of discovery', 'Artifact').
card_types('well of discovery', ['Artifact']).
card_subtypes('well of discovery', []).
card_colors('well of discovery', []).
card_text('well of discovery', 'At the beginning of your end step, if you control no untapped lands, draw a card.').
card_mana_cost('well of discovery', ['6']).
card_cmc('well of discovery', 6).
card_layout('well of discovery', 'normal').

% Found in: C14
card_name('well of ideas', 'Well of Ideas').
card_type('well of ideas', 'Enchantment').
card_types('well of ideas', ['Enchantment']).
card_subtypes('well of ideas', []).
card_colors('well of ideas', ['U']).
card_text('well of ideas', 'When Well of Ideas enters the battlefield, draw two cards.\nAt the beginning of each other player\'s draw step, that player draws an additional card.\nAt the beginning of your draw step, draw two additional cards.').
card_mana_cost('well of ideas', ['5', 'U']).
card_cmc('well of ideas', 6).
card_layout('well of ideas', 'normal').

% Found in: WTH
card_name('well of knowledge', 'Well of Knowledge').
card_type('well of knowledge', 'Artifact').
card_types('well of knowledge', ['Artifact']).
card_subtypes('well of knowledge', []).
card_colors('well of knowledge', []).
card_text('well of knowledge', '{2}: Draw a card. Any player may activate this ability but only during his or her draw step.').
card_mana_cost('well of knowledge', ['3']).
card_cmc('well of knowledge', 3).
card_layout('well of knowledge', 'normal').
card_reserved('well of knowledge').

% Found in: PCY
card_name('well of life', 'Well of Life').
card_type('well of life', 'Artifact').
card_types('well of life', ['Artifact']).
card_subtypes('well of life', []).
card_colors('well of life', []).
card_text('well of life', 'At the beginning of your end step, if you control no untapped lands, you gain 2 life.').
card_mana_cost('well of life', ['4']).
card_cmc('well of life', 4).
card_layout('well of life', 'normal').

% Found in: C13, DST
card_name('well of lost dreams', 'Well of Lost Dreams').
card_type('well of lost dreams', 'Artifact').
card_types('well of lost dreams', ['Artifact']).
card_subtypes('well of lost dreams', []).
card_colors('well of lost dreams', []).
card_text('well of lost dreams', 'Whenever you gain life, you may pay {X}, where X is less than or equal to the amount of life you gained. If you do, draw X cards.').
card_mana_cost('well of lost dreams', ['4']).
card_cmc('well of lost dreams', 4).
card_layout('well of lost dreams', 'normal').

% Found in: INV
card_name('well-laid plans', 'Well-Laid Plans').
card_type('well-laid plans', 'Enchantment').
card_types('well-laid plans', ['Enchantment']).
card_subtypes('well-laid plans', []).
card_colors('well-laid plans', ['U']).
card_text('well-laid plans', 'Prevent all damage that would be dealt to a creature by another creature if they share a color.').
card_mana_cost('well-laid plans', ['2', 'U']).
card_cmc('well-laid plans', 3).
card_layout('well-laid plans', 'normal').

% Found in: LRW
card_name('wellgabber apothecary', 'Wellgabber Apothecary').
card_type('wellgabber apothecary', 'Creature — Merfolk Cleric').
card_types('wellgabber apothecary', ['Creature']).
card_subtypes('wellgabber apothecary', ['Merfolk', 'Cleric']).
card_colors('wellgabber apothecary', ['W']).
card_text('wellgabber apothecary', '{1}{W}: Prevent all damage that would be dealt to target tapped Merfolk or Kithkin creature this turn.').
card_mana_cost('wellgabber apothecary', ['4', 'W']).
card_cmc('wellgabber apothecary', 5).
card_layout('wellgabber apothecary', 'normal').
card_power('wellgabber apothecary', 2).
card_toughness('wellgabber apothecary', 3).

% Found in: MIR
card_name('wellspring', 'Wellspring').
card_type('wellspring', 'Enchantment — Aura').
card_types('wellspring', ['Enchantment']).
card_subtypes('wellspring', ['Aura']).
card_colors('wellspring', ['W', 'G']).
card_text('wellspring', 'Enchant land\nWhen Wellspring enters the battlefield, gain control of enchanted land until end of turn.\nAt the beginning of your upkeep, untap enchanted land. You gain control of that land until end of turn.').
card_mana_cost('wellspring', ['1', 'G', 'W']).
card_cmc('wellspring', 3).
card_layout('wellspring', 'normal').
card_reserved('wellspring').

% Found in: C14, DD3_EVG, EVG, ONS
card_name('wellwisher', 'Wellwisher').
card_type('wellwisher', 'Creature — Elf').
card_types('wellwisher', ['Creature']).
card_subtypes('wellwisher', ['Elf']).
card_colors('wellwisher', ['G']).
card_text('wellwisher', '{T}: You gain 1 life for each Elf on the battlefield.').
card_mana_cost('wellwisher', ['1', 'G']).
card_cmc('wellwisher', 2).
card_layout('wellwisher', 'normal').
card_power('wellwisher', 1).
card_toughness('wellwisher', 1).

% Found in: ODY
card_name('werebear', 'Werebear').
card_type('werebear', 'Creature — Human Bear Druid').
card_types('werebear', ['Creature']).
card_subtypes('werebear', ['Human', 'Bear', 'Druid']).
card_colors('werebear', ['G']).
card_text('werebear', '{T}: Add {G} to your mana pool.\nThreshold — Werebear gets +3/+3 as long as seven or more cards are in your graveyard.').
card_mana_cost('werebear', ['1', 'G']).
card_cmc('werebear', 2).
card_layout('werebear', 'normal').
card_power('werebear', 1).
card_toughness('werebear', 1).

% Found in: DKA
card_name('werewolf ransacker', 'Werewolf Ransacker').
card_type('werewolf ransacker', 'Creature — Werewolf').
card_types('werewolf ransacker', ['Creature']).
card_subtypes('werewolf ransacker', ['Werewolf']).
card_colors('werewolf ransacker', ['R']).
card_text('werewolf ransacker', 'Whenever this creature transforms into Werewolf Ransacker, you may destroy target artifact. If that artifact is put into a graveyard this way, Werewolf Ransacker deals 3 damage to that artifact\'s controller.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Werewolf Ransacker.').
card_layout('werewolf ransacker', 'double-faced').
card_power('werewolf ransacker', 5).
card_toughness('werewolf ransacker', 4).

% Found in: 7ED, 8ED, USG
card_name('western paladin', 'Western Paladin').
card_type('western paladin', 'Creature — Zombie Knight').
card_types('western paladin', ['Creature']).
card_subtypes('western paladin', ['Zombie', 'Knight']).
card_colors('western paladin', ['B']).
card_text('western paladin', '{B}{B}, {T}: Destroy target white creature.').
card_mana_cost('western paladin', ['2', 'B', 'B']).
card_cmc('western paladin', 4).
card_layout('western paladin', 'normal').
card_power('western paladin', 3).
card_toughness('western paladin', 3).

% Found in: UNH
card_name('wet willie of the damned', 'Wet Willie of the Damned').
card_type('wet willie of the damned', 'Sorcery').
card_types('wet willie of the damned', ['Sorcery']).
card_subtypes('wet willie of the damned', []).
card_colors('wet willie of the damned', ['B']).
card_text('wet willie of the damned', 'Wet Willie of the Damned deals 2½ damage to target creature or player and you gain 2½ life.').
card_mana_cost('wet willie of the damned', ['2', 'B', 'B']).
card_cmc('wet willie of the damned', 4).
card_layout('wet willie of the damned', 'normal').

% Found in: KTK
card_name('wetland sambar', 'Wetland Sambar').
card_type('wetland sambar', 'Creature — Elk').
card_types('wetland sambar', ['Creature']).
card_subtypes('wetland sambar', ['Elk']).
card_colors('wetland sambar', ['U']).
card_text('wetland sambar', '').
card_mana_cost('wetland sambar', ['1', 'U']).
card_cmc('wetland sambar', 2).
card_layout('wetland sambar', 'normal').
card_power('wetland sambar', 2).
card_toughness('wetland sambar', 1).

% Found in: CST, ICE
card_name('whalebone glider', 'Whalebone Glider').
card_type('whalebone glider', 'Artifact').
card_types('whalebone glider', ['Artifact']).
card_subtypes('whalebone glider', []).
card_colors('whalebone glider', []).
card_text('whalebone glider', '{2}, {T}: Target creature with power 3 or less gains flying until end of turn.').
card_mana_cost('whalebone glider', ['2']).
card_cmc('whalebone glider', 2).
card_layout('whalebone glider', 'normal').

% Found in: UNH
card_name('what', 'What').
card_type('what', 'Instant').
card_types('what', ['Instant']).
card_subtypes('what', []).
card_colors('what', ['R']).
card_text('what', 'Destroy target artifact.').
card_mana_cost('what', ['2', 'R']).
card_cmc('what', 3).
card_layout('what', 'split').

% Found in: ONS
card_name('wheel and deal', 'Wheel and Deal').
card_type('wheel and deal', 'Instant').
card_types('wheel and deal', ['Instant']).
card_subtypes('wheel and deal', []).
card_colors('wheel and deal', ['U']).
card_text('wheel and deal', 'Any number of target opponents each discards his or her hand, then draws seven cards.\nDraw a card.').
card_mana_cost('wheel and deal', ['3', 'U']).
card_cmc('wheel and deal', 4).
card_layout('wheel and deal', 'normal').

% Found in: TSP
card_name('wheel of fate', 'Wheel of Fate').
card_type('wheel of fate', 'Sorcery').
card_types('wheel of fate', ['Sorcery']).
card_subtypes('wheel of fate', []).
card_colors('wheel of fate', ['R']).
card_text('wheel of fate', 'Suspend 4—{1}{R} (Rather than cast this card from your hand, pay {1}{R} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\nEach player discards his or her hand, then draws seven cards.').
card_layout('wheel of fate', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4, VMA, pJGP
card_name('wheel of fortune', 'Wheel of Fortune').
card_type('wheel of fortune', 'Sorcery').
card_types('wheel of fortune', ['Sorcery']).
card_subtypes('wheel of fortune', []).
card_colors('wheel of fortune', ['R']).
card_text('wheel of fortune', 'Each player discards his or her hand, then draws seven cards.').
card_mana_cost('wheel of fortune', ['2', 'R']).
card_cmc('wheel of fortune', 3).
card_layout('wheel of fortune', 'normal').
card_reserved('wheel of fortune').

% Found in: SHM
card_name('wheel of sun and moon', 'Wheel of Sun and Moon').
card_type('wheel of sun and moon', 'Enchantment — Aura').
card_types('wheel of sun and moon', ['Enchantment']).
card_subtypes('wheel of sun and moon', ['Aura']).
card_colors('wheel of sun and moon', ['W', 'G']).
card_text('wheel of sun and moon', 'Enchant player\nIf a card would be put into enchanted player\'s graveyard from anywhere, instead that card is revealed and put on the bottom of that player\'s library.').
card_mana_cost('wheel of sun and moon', ['G/W', 'G/W']).
card_cmc('wheel of sun and moon', 2).
card_layout('wheel of sun and moon', 'normal').

% Found in: ULG
card_name('wheel of torture', 'Wheel of Torture').
card_type('wheel of torture', 'Artifact').
card_types('wheel of torture', ['Artifact']).
card_subtypes('wheel of torture', []).
card_colors('wheel of torture', []).
card_text('wheel of torture', 'At the beginning of each opponent\'s upkeep, Wheel of Torture deals X damage to that player, where X is 3 minus the number of cards in his or her hand.').
card_mana_cost('wheel of torture', ['3']).
card_cmc('wheel of torture', 3).
card_layout('wheel of torture', 'normal').

% Found in: BNG, DDO
card_name('whelming wave', 'Whelming Wave').
card_type('whelming wave', 'Sorcery').
card_types('whelming wave', ['Sorcery']).
card_subtypes('whelming wave', []).
card_colors('whelming wave', ['U']).
card_text('whelming wave', 'Return all creatures to their owners\' hands except for Krakens, Leviathans, Octopuses, and Serpents.').
card_mana_cost('whelming wave', ['2', 'U', 'U']).
card_cmc('whelming wave', 4).
card_layout('whelming wave', 'normal').

% Found in: UNH
card_name('when', 'When').
card_type('when', 'Instant').
card_types('when', ['Instant']).
card_subtypes('when', []).
card_colors('when', ['U']).
card_text('when', 'Counter target creature spell.').
card_mana_cost('when', ['2', 'U']).
card_cmc('when', 3).
card_layout('when', 'split').

% Found in: UNH
card_name('when fluffy bunnies attack', 'When Fluffy Bunnies Attack').
card_type('when fluffy bunnies attack', 'Instant').
card_types('when fluffy bunnies attack', ['Instant']).
card_subtypes('when fluffy bunnies attack', []).
card_colors('when fluffy bunnies attack', ['B']).
card_text('when fluffy bunnies attack', 'Target creature gets -X/-X until end of turn, where X is the number of times the letter of your choice appears in that creature\'s name.').
card_mana_cost('when fluffy bunnies attack', ['3', 'B']).
card_cmc('when fluffy bunnies attack', 4).
card_layout('when fluffy bunnies attack', 'normal').

% Found in: UNH
card_name('where', 'Where').
card_type('where', 'Instant').
card_types('where', ['Instant']).
card_subtypes('where', []).
card_colors('where', ['B']).
card_text('where', 'Destroy target land.').
card_mana_cost('where', ['3', 'B']).
card_cmc('where', 4).
card_layout('where', 'split').

% Found in: ALA, C13
card_name('where ancients tread', 'Where Ancients Tread').
card_type('where ancients tread', 'Enchantment').
card_types('where ancients tread', ['Enchantment']).
card_subtypes('where ancients tread', []).
card_colors('where ancients tread', ['R']).
card_text('where ancients tread', 'Whenever a creature with power 5 or greater enters the battlefield under your control, you may have Where Ancients Tread deal 5 damage to target creature or player.').
card_mana_cost('where ancients tread', ['4', 'R']).
card_cmc('where ancients tread', 5).
card_layout('where ancients tread', 'normal').

% Found in: USG
card_name('whetstone', 'Whetstone').
card_type('whetstone', 'Artifact').
card_types('whetstone', ['Artifact']).
card_subtypes('whetstone', []).
card_colors('whetstone', []).
card_text('whetstone', '{3}: Each player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('whetstone', ['3']).
card_cmc('whetstone', 3).
card_layout('whetstone', 'normal').

% Found in: FUT
card_name('whetwheel', 'Whetwheel').
card_type('whetwheel', 'Artifact').
card_types('whetwheel', ['Artifact']).
card_subtypes('whetwheel', []).
card_colors('whetwheel', []).
card_text('whetwheel', '{X}{X}, {T}: Target player puts the top X cards of his or her library into his or her graveyard.\nMorph {3} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('whetwheel', ['4']).
card_cmc('whetwheel', 4).
card_layout('whetwheel', 'normal').

% Found in: ARC
card_name('which of you burns brightest?', 'Which of You Burns Brightest?').
card_type('which of you burns brightest?', 'Scheme').
card_types('which of you burns brightest?', ['Scheme']).
card_subtypes('which of you burns brightest?', []).
card_colors('which of you burns brightest?', []).
card_text('which of you burns brightest?', 'When you set this scheme in motion, you may pay {X}. If you do, this scheme deals X damage to target opponent and each creature he or she controls.').
card_layout('which of you burns brightest?', 'scheme').

% Found in: TMP
card_name('whim of volrath', 'Whim of Volrath').
card_type('whim of volrath', 'Instant').
card_types('whim of volrath', ['Instant']).
card_subtypes('whim of volrath', []).
card_colors('whim of volrath', ['U']).
card_text('whim of volrath', 'Buyback {2} (You may pay an additional {2} as you cast this spell. If you do, put this card into your hand as it resolves.)\nChange the text of target permanent by replacing all instances of one color word with another or one basic land type with another until end of turn. (For example, you may change \"nonred creature\" to \"nongreen creature\" or \"plainswalk\" to \"swampwalk.\")').
card_mana_cost('whim of volrath', ['U']).
card_cmc('whim of volrath', 1).
card_layout('whim of volrath', 'normal').

% Found in: BNG
card_name('whims of the fates', 'Whims of the Fates').
card_type('whims of the fates', 'Sorcery').
card_types('whims of the fates', ['Sorcery']).
card_subtypes('whims of the fates', []).
card_colors('whims of the fates', ['R']).
card_text('whims of the fates', 'Starting with you, each player separates all permanents he or she controls into three piles. Then each player chooses one of his or her piles at random and sacrifices those permanents. (Piles can be empty.)').
card_mana_cost('whims of the fates', ['5', 'R']).
card_cmc('whims of the fates', 6).
card_layout('whims of the fates', 'normal').

% Found in: SHM
card_name('whimwader', 'Whimwader').
card_type('whimwader', 'Creature — Elemental').
card_types('whimwader', ['Creature']).
card_subtypes('whimwader', ['Elemental']).
card_colors('whimwader', ['U']).
card_text('whimwader', 'Whimwader can\'t attack unless defending player controls a blue permanent.').
card_mana_cost('whimwader', ['4', 'U']).
card_cmc('whimwader', 5).
card_layout('whimwader', 'normal').
card_power('whimwader', 6).
card_toughness('whimwader', 4).

% Found in: CPK, THS
card_name('whip of erebos', 'Whip of Erebos').
card_type('whip of erebos', 'Legendary Enchantment Artifact').
card_types('whip of erebos', ['Enchantment', 'Artifact']).
card_subtypes('whip of erebos', []).
card_supertypes('whip of erebos', ['Legendary']).
card_colors('whip of erebos', ['B']).
card_text('whip of erebos', 'Creatures you control have lifelink.\n{2}{B}{B}, {T}: Return target creature card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step. If it would leave the battlefield, exile it instead of putting it anywhere else. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('whip of erebos', ['2', 'B', 'B']).
card_cmc('whip of erebos', 4).
card_layout('whip of erebos', 'normal').

% Found in: 9ED, PCY
card_name('whip sergeant', 'Whip Sergeant').
card_type('whip sergeant', 'Creature — Human Soldier').
card_types('whip sergeant', ['Creature']).
card_subtypes('whip sergeant', ['Human', 'Soldier']).
card_colors('whip sergeant', ['R']).
card_text('whip sergeant', '{R}: Target creature gains haste until end of turn. (It can attack this turn.)').
card_mana_cost('whip sergeant', ['2', 'R']).
card_cmc('whip sergeant', 3).
card_layout('whip sergeant', 'normal').
card_power('whip sergeant', 2).
card_toughness('whip sergeant', 1).

% Found in: INV
card_name('whip silk', 'Whip Silk').
card_type('whip silk', 'Enchantment — Aura').
card_types('whip silk', ['Enchantment']).
card_subtypes('whip silk', ['Aura']).
card_colors('whip silk', ['G']).
card_text('whip silk', 'Enchant creature\nEnchanted creature has reach. (It can block creatures with flying.)\n{G}: Return Whip Silk to its owner\'s hand.').
card_mana_cost('whip silk', ['G']).
card_cmc('whip silk', 1).
card_layout('whip silk', 'normal').

% Found in: ALL
card_name('whip vine', 'Whip Vine').
card_type('whip vine', 'Creature — Plant Wall').
card_types('whip vine', ['Creature']).
card_subtypes('whip vine', ['Plant', 'Wall']).
card_colors('whip vine', ['G']).
card_text('whip vine', 'Defender; reach (This creature can block creatures with flying.)\nYou may choose not to untap Whip Vine during your untap step.\n{T}: Tap target creature with flying blocked by Whip Vine. That creature doesn\'t untap during its controller\'s untap step for as long as Whip Vine remains tapped.').
card_mana_cost('whip vine', ['2', 'G']).
card_cmc('whip vine', 3).
card_layout('whip vine', 'normal').
card_power('whip vine', 1).
card_toughness('whip vine', 4).

% Found in: FUT
card_name('whip-spine drake', 'Whip-Spine Drake').
card_type('whip-spine drake', 'Creature — Drake').
card_types('whip-spine drake', ['Creature']).
card_subtypes('whip-spine drake', ['Drake']).
card_colors('whip-spine drake', ['U']).
card_text('whip-spine drake', 'Flying\nMorph {2}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('whip-spine drake', ['3', 'U', 'U']).
card_cmc('whip-spine drake', 5).
card_layout('whip-spine drake', 'normal').
card_power('whip-spine drake', 3).
card_toughness('whip-spine drake', 3).

% Found in: ONS, pFNM
card_name('whipcorder', 'Whipcorder').
card_type('whipcorder', 'Creature — Human Soldier Rebel').
card_types('whipcorder', ['Creature']).
card_subtypes('whipcorder', ['Human', 'Soldier', 'Rebel']).
card_colors('whipcorder', ['W']).
card_text('whipcorder', '{W}, {T}: Tap target creature.\nMorph {W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('whipcorder', ['W', 'W']).
card_cmc('whipcorder', 2).
card_layout('whipcorder', 'normal').
card_power('whipcorder', 2).
card_toughness('whipcorder', 2).

% Found in: C14, NPH
card_name('whipflare', 'Whipflare').
card_type('whipflare', 'Sorcery').
card_types('whipflare', ['Sorcery']).
card_subtypes('whipflare', []).
card_colors('whipflare', ['R']).
card_text('whipflare', 'Whipflare deals 2 damage to each nonartifact creature.').
card_mana_cost('whipflare', ['1', 'R']).
card_cmc('whipflare', 2).
card_layout('whipflare', 'normal').

% Found in: LGN
card_name('whipgrass entangler', 'Whipgrass Entangler').
card_type('whipgrass entangler', 'Creature — Human Cleric').
card_types('whipgrass entangler', ['Creature']).
card_subtypes('whipgrass entangler', ['Human', 'Cleric']).
card_colors('whipgrass entangler', ['W']).
card_text('whipgrass entangler', '{1}{W}: Until end of turn, target creature gains \"This creature can\'t attack or block unless its controller pays {1} for each Cleric on the battlefield.\"').
card_mana_cost('whipgrass entangler', ['2', 'W']).
card_cmc('whipgrass entangler', 3).
card_layout('whipgrass entangler', 'normal').
card_power('whipgrass entangler', 1).
card_toughness('whipgrass entangler', 3).

% Found in: ODY
card_name('whipkeeper', 'Whipkeeper').
card_type('whipkeeper', 'Creature — Dwarf').
card_types('whipkeeper', ['Creature']).
card_subtypes('whipkeeper', ['Dwarf']).
card_colors('whipkeeper', ['R']).
card_text('whipkeeper', '{T}: Whipkeeper deals damage to target creature equal to the damage already dealt to it this turn.').
card_mana_cost('whipkeeper', ['2', 'R', 'R']).
card_cmc('whipkeeper', 4).
card_layout('whipkeeper', 'normal').
card_power('whipkeeper', 1).
card_toughness('whipkeeper', 1).

% Found in: DDN, HOP, ZEN
card_name('whiplash trap', 'Whiplash Trap').
card_type('whiplash trap', 'Instant — Trap').
card_types('whiplash trap', ['Instant']).
card_subtypes('whiplash trap', ['Trap']).
card_colors('whiplash trap', ['U']).
card_text('whiplash trap', 'If an opponent had two or more creatures enter the battlefield under his or her control this turn, you may pay {U} rather than pay Whiplash Trap\'s mana cost.\nReturn two target creatures to their owners\' hands.').
card_mana_cost('whiplash trap', ['3', 'U', 'U']).
card_cmc('whiplash trap', 5).
card_layout('whiplash trap', 'normal').

% Found in: DRK
card_name('whippoorwill', 'Whippoorwill').
card_type('whippoorwill', 'Creature — Bird').
card_types('whippoorwill', ['Creature']).
card_subtypes('whippoorwill', ['Bird']).
card_colors('whippoorwill', ['G']).
card_text('whippoorwill', '{G}{G}, {T}: Target creature can\'t be regenerated this turn. Damage that would be dealt to that creature this turn can\'t be prevented or dealt instead to another creature or player. When the creature dies this turn, exile the creature.').
card_mana_cost('whippoorwill', ['G']).
card_cmc('whippoorwill', 1).
card_layout('whippoorwill', 'normal').
card_power('whippoorwill', 1).
card_toughness('whippoorwill', 1).

% Found in: PCY
card_name('whipstitched zombie', 'Whipstitched Zombie').
card_type('whipstitched zombie', 'Creature — Zombie').
card_types('whipstitched zombie', ['Creature']).
card_subtypes('whipstitched zombie', ['Zombie']).
card_colors('whipstitched zombie', ['B']).
card_text('whipstitched zombie', 'At the beginning of your upkeep, sacrifice Whipstitched Zombie unless you pay {B}.').
card_mana_cost('whipstitched zombie', ['1', 'B']).
card_cmc('whipstitched zombie', 2).
card_layout('whipstitched zombie', 'normal').
card_power('whipstitched zombie', 2).
card_toughness('whipstitched zombie', 2).

% Found in: DIS
card_name('whiptail moloch', 'Whiptail Moloch').
card_type('whiptail moloch', 'Creature — Lizard').
card_types('whiptail moloch', ['Creature']).
card_subtypes('whiptail moloch', ['Lizard']).
card_colors('whiptail moloch', ['R']).
card_text('whiptail moloch', 'When Whiptail Moloch enters the battlefield, it deals 3 damage to target creature you control.').
card_mana_cost('whiptail moloch', ['4', 'R']).
card_cmc('whiptail moloch', 5).
card_layout('whiptail moloch', 'normal').
card_power('whiptail moloch', 6).
card_toughness('whiptail moloch', 3).

% Found in: ME4, POR, S99
card_name('whiptail wurm', 'Whiptail Wurm').
card_type('whiptail wurm', 'Creature — Wurm').
card_types('whiptail wurm', ['Creature']).
card_subtypes('whiptail wurm', ['Wurm']).
card_colors('whiptail wurm', ['G']).
card_text('whiptail wurm', '').
card_mana_cost('whiptail wurm', ['6', 'G']).
card_cmc('whiptail wurm', 7).
card_layout('whiptail wurm', 'normal').
card_power('whiptail wurm', 8).
card_toughness('whiptail wurm', 5).

% Found in: EXO
card_name('whiptongue frog', 'Whiptongue Frog').
card_type('whiptongue frog', 'Creature — Frog').
card_types('whiptongue frog', ['Creature']).
card_subtypes('whiptongue frog', ['Frog']).
card_colors('whiptongue frog', ['U']).
card_text('whiptongue frog', '{U}: Whiptongue Frog gains flying until end of turn.').
card_mana_cost('whiptongue frog', ['2', 'U']).
card_cmc('whiptongue frog', 3).
card_layout('whiptongue frog', 'normal').
card_power('whiptongue frog', 1).
card_toughness('whiptongue frog', 3).

% Found in: ORI
card_name('whirler rogue', 'Whirler Rogue').
card_type('whirler rogue', 'Creature — Human Rogue Artificer').
card_types('whirler rogue', ['Creature']).
card_subtypes('whirler rogue', ['Human', 'Rogue', 'Artificer']).
card_colors('whirler rogue', ['U']).
card_text('whirler rogue', 'When Whirler Rogue enters the battlefield, put two 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.\nTap two untapped artifacts you control: Target creature can\'t be blocked this turn.').
card_mana_cost('whirler rogue', ['2', 'U', 'U']).
card_cmc('whirler rogue', 4).
card_layout('whirler rogue', 'normal').
card_power('whirler rogue', 2).
card_toughness('whirler rogue', 2).

% Found in: ALL, ME2
card_name('whirling catapult', 'Whirling Catapult').
card_type('whirling catapult', 'Artifact').
card_types('whirling catapult', ['Artifact']).
card_subtypes('whirling catapult', []).
card_colors('whirling catapult', []).
card_text('whirling catapult', '{2}, Exile the top two cards of your library: Whirling Catapult deals 1 damage to each creature with flying and each player.').
card_mana_cost('whirling catapult', ['4']).
card_cmc('whirling catapult', 4).
card_layout('whirling catapult', 'normal').

% Found in: 4ED, 5ED, ITP, LEG, RQS, TSB, pSUS
card_name('whirling dervish', 'Whirling Dervish').
card_type('whirling dervish', 'Creature — Human Monk').
card_types('whirling dervish', ['Creature']).
card_subtypes('whirling dervish', ['Human', 'Monk']).
card_colors('whirling dervish', ['G']).
card_text('whirling dervish', 'Protection from black\nAt the beginning of each end step, if Whirling Dervish dealt damage to an opponent this turn, put a +1/+1 counter on it.').
card_mana_cost('whirling dervish', ['G', 'G']).
card_cmc('whirling dervish', 2).
card_layout('whirling dervish', 'normal').
card_power('whirling dervish', 1).
card_toughness('whirling dervish', 1).

% Found in: APC
card_name('whirlpool drake', 'Whirlpool Drake').
card_type('whirlpool drake', 'Creature — Drake').
card_types('whirlpool drake', ['Creature']).
card_subtypes('whirlpool drake', ['Drake']).
card_colors('whirlpool drake', ['U']).
card_text('whirlpool drake', 'Flying\nWhen Whirlpool Drake enters the battlefield, shuffle the cards from your hand into your library, then draw that many cards.\nWhen Whirlpool Drake dies, shuffle the cards from your hand into your library, then draw that many cards.').
card_mana_cost('whirlpool drake', ['3', 'U']).
card_cmc('whirlpool drake', 4).
card_layout('whirlpool drake', 'normal').
card_power('whirlpool drake', 2).
card_toughness('whirlpool drake', 2).

% Found in: APC
card_name('whirlpool rider', 'Whirlpool Rider').
card_type('whirlpool rider', 'Creature — Merfolk').
card_types('whirlpool rider', ['Creature']).
card_subtypes('whirlpool rider', ['Merfolk']).
card_colors('whirlpool rider', ['U']).
card_text('whirlpool rider', 'When Whirlpool Rider enters the battlefield, shuffle the cards from your hand into your library, then draw that many cards.').
card_mana_cost('whirlpool rider', ['1', 'U']).
card_cmc('whirlpool rider', 2).
card_layout('whirlpool rider', 'normal').
card_power('whirlpool rider', 1).
card_toughness('whirlpool rider', 1).

% Found in: APC, PC2
card_name('whirlpool warrior', 'Whirlpool Warrior').
card_type('whirlpool warrior', 'Creature — Merfolk Warrior').
card_types('whirlpool warrior', ['Creature']).
card_subtypes('whirlpool warrior', ['Merfolk', 'Warrior']).
card_colors('whirlpool warrior', ['U']).
card_text('whirlpool warrior', 'When Whirlpool Warrior enters the battlefield, shuffle the cards from your hand into your library, then draw that many cards.\n{R}, Sacrifice Whirlpool Warrior: Each player shuffles the cards from his or her hand into his or her library, then draws that many cards.').
card_mana_cost('whirlpool warrior', ['2', 'U']).
card_cmc('whirlpool warrior', 3).
card_layout('whirlpool warrior', 'normal').
card_power('whirlpool warrior', 2).
card_toughness('whirlpool warrior', 2).

% Found in: CMD, LRW
card_name('whirlpool whelm', 'Whirlpool Whelm').
card_type('whirlpool whelm', 'Instant').
card_types('whirlpool whelm', ['Instant']).
card_subtypes('whirlpool whelm', []).
card_colors('whirlpool whelm', ['U']).
card_text('whirlpool whelm', 'Clash with an opponent, then return target creature to its owner\'s hand. If you win, you may put that creature on top of its owner\'s library instead. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('whirlpool whelm', ['1', 'U']).
card_cmc('whirlpool whelm', 2).
card_layout('whirlpool whelm', 'normal').

% Found in: C14, S99, USG
card_name('whirlwind', 'Whirlwind').
card_type('whirlwind', 'Sorcery').
card_types('whirlwind', ['Sorcery']).
card_subtypes('whirlwind', []).
card_colors('whirlwind', ['G']).
card_text('whirlwind', 'Destroy all creatures with flying.').
card_mana_cost('whirlwind', ['2', 'G', 'G']).
card_cmc('whirlwind', 4).
card_layout('whirlwind', 'normal').

% Found in: KTK
card_name('whirlwind adept', 'Whirlwind Adept').
card_type('whirlwind adept', 'Creature — Djinn Monk').
card_types('whirlwind adept', ['Creature']).
card_subtypes('whirlwind adept', ['Djinn', 'Monk']).
card_colors('whirlwind adept', ['U']).
card_text('whirlwind adept', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('whirlwind adept', ['4', 'U']).
card_cmc('whirlwind adept', 5).
card_layout('whirlwind adept', 'normal').
card_power('whirlwind adept', 4).
card_toughness('whirlwind adept', 2).

% Found in: FRF
card_name('whisk away', 'Whisk Away').
card_type('whisk away', 'Instant').
card_types('whisk away', ['Instant']).
card_subtypes('whisk away', []).
card_colors('whisk away', ['U']).
card_text('whisk away', 'Put target attacking or blocking creature on top of its owner\'s library.').
card_mana_cost('whisk away', ['2', 'U']).
card_cmc('whisk away', 3).
card_layout('whisk away', 'normal').

% Found in: FRF
card_name('whisperer of the wilds', 'Whisperer of the Wilds').
card_type('whisperer of the wilds', 'Creature — Human Shaman').
card_types('whisperer of the wilds', ['Creature']).
card_subtypes('whisperer of the wilds', ['Human', 'Shaman']).
card_colors('whisperer of the wilds', ['G']).
card_text('whisperer of the wilds', '{T}: Add {G} to your mana pool.\nFerocious — {T}: Add {G}{G} to your mana pool. Activate this ability only if you control a creature with power 4 or greater.').
card_mana_cost('whisperer of the wilds', ['1', 'G']).
card_cmc('whisperer of the wilds', 2).
card_layout('whisperer of the wilds', 'normal').
card_power('whisperer of the wilds', 0).
card_toughness('whisperer of the wilds', 2).

% Found in: CNS
card_name('whispergear sneak', 'Whispergear Sneak').
card_type('whispergear sneak', 'Artifact Creature — Construct').
card_types('whispergear sneak', ['Artifact', 'Creature']).
card_subtypes('whispergear sneak', ['Construct']).
card_colors('whispergear sneak', []).
card_text('whispergear sneak', 'Draft Whispergear Sneak face up.\nDuring the draft, you may turn Whispergear Sneak face down. If you do, look at any unopened booster pack in the draft or any booster pack not being looked at by another player.').
card_mana_cost('whispergear sneak', ['1']).
card_cmc('whispergear sneak', 1).
card_layout('whispergear sneak', 'normal').
card_power('whispergear sneak', 1).
card_toughness('whispergear sneak', 1).

% Found in: GTC
card_name('whispering madness', 'Whispering Madness').
card_type('whispering madness', 'Sorcery').
card_types('whispering madness', ['Sorcery']).
card_subtypes('whispering madness', []).
card_colors('whispering madness', ['U', 'B']).
card_text('whispering madness', 'Each player discards his or her hand, then draws cards equal to the greatest number of cards a player discarded this way.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('whispering madness', ['2', 'U', 'B']).
card_cmc('whispering madness', 4).
card_layout('whispering madness', 'normal').

% Found in: ODY
card_name('whispering shade', 'Whispering Shade').
card_type('whispering shade', 'Creature — Shade').
card_types('whispering shade', ['Creature']).
card_subtypes('whispering shade', ['Shade']).
card_colors('whispering shade', ['B']).
card_text('whispering shade', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\n{B}: Whispering Shade gets +1/+1 until end of turn.').
card_mana_cost('whispering shade', ['3', 'B']).
card_cmc('whispering shade', 4).
card_layout('whispering shade', 'normal').
card_power('whispering shade', 1).
card_toughness('whispering shade', 1).

% Found in: NPH
card_name('whispering specter', 'Whispering Specter').
card_type('whispering specter', 'Creature — Specter').
card_types('whispering specter', ['Creature']).
card_subtypes('whispering specter', ['Specter']).
card_colors('whispering specter', ['B']).
card_text('whispering specter', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever Whispering Specter deals combat damage to a player, you may sacrifice it. If you do, that player discards a card for each poison counter he or she has.').
card_mana_cost('whispering specter', ['1', 'B', 'B']).
card_cmc('whispering specter', 3).
card_layout('whispering specter', 'normal').
card_power('whispering specter', 1).
card_toughness('whispering specter', 1).

% Found in: TMP, TPR, TSB
card_name('whispers of the muse', 'Whispers of the Muse').
card_type('whispers of the muse', 'Instant').
card_types('whispers of the muse', ['Instant']).
card_subtypes('whispers of the muse', []).
card_colors('whispers of the muse', ['U']).
card_text('whispers of the muse', 'Buyback {5} (You may pay an additional {5} as you cast this spell. If you do, put this card into your hand as it resolves.)\nDraw a card.').
card_mana_cost('whispers of the muse', ['U']).
card_cmc('whispers of the muse', 1).
card_layout('whispers of the muse', 'normal').

% Found in: 10E, DDE, DST, M10, M11, PC2
card_name('whispersilk cloak', 'Whispersilk Cloak').
card_type('whispersilk cloak', 'Artifact — Equipment').
card_types('whispersilk cloak', ['Artifact']).
card_subtypes('whispersilk cloak', ['Equipment']).
card_colors('whispersilk cloak', []).
card_text('whispersilk cloak', 'Equipped creature can\'t be blocked and has shroud. (It can\'t be the target of spells or abilities.)\nEquip {2}').
card_mana_cost('whispersilk cloak', ['3']).
card_cmc('whispersilk cloak', 3).
card_layout('whispersilk cloak', 'normal').

% Found in: FRF
card_name('whisperwood elemental', 'Whisperwood Elemental').
card_type('whisperwood elemental', 'Creature — Elemental').
card_types('whisperwood elemental', ['Creature']).
card_subtypes('whisperwood elemental', ['Elemental']).
card_colors('whisperwood elemental', ['G']).
card_text('whisperwood elemental', 'At the beginning of your end step, manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nSacrifice Whisperwood Elemental: Until end of turn, face-up nontoken creatures you control gain \"When this creature dies, manifest the top card of your library.\"').
card_mana_cost('whisperwood elemental', ['3', 'G', 'G']).
card_cmc('whisperwood elemental', 5).
card_layout('whisperwood elemental', 'normal').
card_power('whisperwood elemental', 4).
card_toughness('whisperwood elemental', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, ATH, CED, CEI, DDG, LEA, LEB, LGN, M10, M11, ME4, pFNM
card_name('white knight', 'White Knight').
card_type('white knight', 'Creature — Human Knight').
card_types('white knight', ['Creature']).
card_subtypes('white knight', ['Human', 'Knight']).
card_colors('white knight', ['W']).
card_text('white knight', 'First strike (This creature deals combat damage before creatures without first strike.)\nProtection from black (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything black.)').
card_mana_cost('white knight', ['W', 'W']).
card_cmc('white knight', 2).
card_layout('white knight', 'normal').
card_power('white knight', 2).
card_toughness('white knight', 2).

% Found in: 4ED, LEG
card_name('white mana battery', 'White Mana Battery').
card_type('white mana battery', 'Artifact').
card_types('white mana battery', ['Artifact']).
card_subtypes('white mana battery', []).
card_colors('white mana battery', []).
card_text('white mana battery', '{2}, {T}: Put a charge counter on White Mana Battery.\n{T}, Remove any number of charge counters from White Mana Battery: Add {W} to your mana pool, then add an additional {W} to your mana pool for each charge counter removed this way.').
card_mana_cost('white mana battery', ['4']).
card_cmc('white mana battery', 4).
card_layout('white mana battery', 'normal').

% Found in: ICE
card_name('white scarab', 'White Scarab').
card_type('white scarab', 'Enchantment — Aura').
card_types('white scarab', ['Enchantment']).
card_subtypes('white scarab', ['Aura']).
card_colors('white scarab', ['W']).
card_text('white scarab', 'Enchant creature\nEnchanted creature can\'t be blocked by white creatures.\nEnchanted creature gets +2/+2 as long as an opponent controls a white permanent.').
card_mana_cost('white scarab', ['W']).
card_cmc('white scarab', 1).
card_layout('white scarab', 'normal').

% Found in: CSP
card_name('white shield crusader', 'White Shield Crusader').
card_type('white shield crusader', 'Creature — Human Knight').
card_types('white shield crusader', ['Creature']).
card_subtypes('white shield crusader', ['Human', 'Knight']).
card_colors('white shield crusader', ['W']).
card_text('white shield crusader', 'Protection from black\n{W}: White Shield Crusader gains flying until end of turn.\n{W}{W}: White Shield Crusader gets +1/+0 until end of turn.').
card_mana_cost('white shield crusader', ['W', 'W']).
card_cmc('white shield crusader', 2).
card_layout('white shield crusader', 'normal').
card_power('white shield crusader', 2).
card_toughness('white shield crusader', 1).

% Found in: C14, MBS
card_name('white sun\'s zenith', 'White Sun\'s Zenith').
card_type('white sun\'s zenith', 'Instant').
card_types('white sun\'s zenith', ['Instant']).
card_subtypes('white sun\'s zenith', []).
card_colors('white sun\'s zenith', ['W']).
card_text('white sun\'s zenith', 'Put X 2/2 white Cat creature tokens onto the battlefield. Shuffle White Sun\'s Zenith into its owner\'s library.').
card_mana_cost('white sun\'s zenith', ['X', 'W', 'W', 'W']).
card_cmc('white sun\'s zenith', 3).
card_layout('white sun\'s zenith', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('white ward', 'White Ward').
card_type('white ward', 'Enchantment — Aura').
card_types('white ward', ['Enchantment']).
card_subtypes('white ward', ['Aura']).
card_colors('white ward', ['W']).
card_text('white ward', 'Enchant creature\nEnchanted creature has protection from white. This effect doesn\'t remove White Ward.').
card_mana_cost('white ward', ['W']).
card_cmc('white ward', 1).
card_layout('white ward', 'normal').

% Found in: C14, DDI, PLC
card_name('whitemane lion', 'Whitemane Lion').
card_type('whitemane lion', 'Creature — Cat').
card_types('whitemane lion', ['Creature']).
card_subtypes('whitemane lion', ['Cat']).
card_colors('whitemane lion', ['W']).
card_text('whitemane lion', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Whitemane Lion enters the battlefield, return a creature you control to its owner\'s hand.').
card_mana_cost('whitemane lion', ['1', 'W']).
card_cmc('whitemane lion', 2).
card_layout('whitemane lion', 'normal').
card_power('whitemane lion', 2).
card_toughness('whitemane lion', 2).

% Found in: ICE, ME2
card_name('whiteout', 'Whiteout').
card_type('whiteout', 'Instant').
card_types('whiteout', ['Instant']).
card_subtypes('whiteout', []).
card_colors('whiteout', ['G']).
card_text('whiteout', 'All creatures lose flying until end of turn.\nSacrifice a snow land: Return Whiteout from your graveyard to your hand.').
card_mana_cost('whiteout', ['1', 'G']).
card_cmc('whiteout', 2).
card_layout('whiteout', 'normal').

% Found in: SOM
card_name('whitesun\'s passage', 'Whitesun\'s Passage').
card_type('whitesun\'s passage', 'Instant').
card_types('whitesun\'s passage', ['Instant']).
card_subtypes('whitesun\'s passage', []).
card_colors('whitesun\'s passage', ['W']).
card_text('whitesun\'s passage', 'You gain 5 life.').
card_mana_cost('whitesun\'s passage', ['1', 'W']).
card_cmc('whitesun\'s passage', 2).
card_layout('whitesun\'s passage', 'normal').

% Found in: JOU
card_name('whitewater naiads', 'Whitewater Naiads').
card_type('whitewater naiads', 'Enchantment Creature — Nymph').
card_types('whitewater naiads', ['Enchantment', 'Creature']).
card_subtypes('whitewater naiads', ['Nymph']).
card_colors('whitewater naiads', ['U']).
card_text('whitewater naiads', 'Constellation — Whenever Whitewater Naiads or another enchantment enters the battlefield under your control, target creature can\'t be blocked this turn.').
card_mana_cost('whitewater naiads', ['3', 'U', 'U']).
card_cmc('whitewater naiads', 5).
card_layout('whitewater naiads', 'normal').
card_power('whitewater naiads', 4).
card_toughness('whitewater naiads', 4).

% Found in: UNH
card_name('who', 'Who').
card_type('who', 'Instant').
card_types('who', ['Instant']).
card_subtypes('who', []).
card_colors('who', ['W']).
card_text('who', 'Target player gains X life.').
card_mana_cost('who', ['X', 'W']).
card_cmc('who', 1).
card_layout('who', 'split').
card_sides('who', 'what').

% Found in: UNH
card_name('why', 'Why').
card_type('why', 'Instant').
card_types('why', ['Instant']).
card_subtypes('why', []).
card_colors('why', ['G']).
card_text('why', 'Destroy target enchantment.').
card_mana_cost('why', ['1', 'G']).
card_cmc('why', 2).
card_layout('why', 'split').

% Found in: CHK
card_name('wicked akuba', 'Wicked Akuba').
card_type('wicked akuba', 'Creature — Spirit').
card_types('wicked akuba', ['Creature']).
card_subtypes('wicked akuba', ['Spirit']).
card_colors('wicked akuba', ['B']).
card_text('wicked akuba', '{B}: Target player dealt damage by Wicked Akuba this turn loses 1 life.').
card_mana_cost('wicked akuba', ['B', 'B']).
card_cmc('wicked akuba', 2).
card_layout('wicked akuba', 'normal').
card_power('wicked akuba', 2).
card_toughness('wicked akuba', 2).

% Found in: ME4, POR, S99
card_name('wicked pact', 'Wicked Pact').
card_type('wicked pact', 'Sorcery').
card_types('wicked pact', ['Sorcery']).
card_subtypes('wicked pact', []).
card_colors('wicked pact', ['B']).
card_text('wicked pact', 'Destroy two target nonblack creatures. You lose 5 life.').
card_mana_cost('wicked pact', ['1', 'B', 'B']).
card_cmc('wicked pact', 3).
card_layout('wicked pact', 'normal').

% Found in: MGB, VIS
card_name('wicked reward', 'Wicked Reward').
card_type('wicked reward', 'Instant').
card_types('wicked reward', ['Instant']).
card_subtypes('wicked reward', []).
card_colors('wicked reward', ['B']).
card_text('wicked reward', 'As an additional cost to cast Wicked Reward, sacrifice a creature.\nTarget creature gets +4/+2 until end of turn.').
card_mana_cost('wicked reward', ['1', 'B']).
card_cmc('wicked reward', 2).
card_layout('wicked reward', 'normal').

% Found in: SHM
card_name('wicker warcrawler', 'Wicker Warcrawler').
card_type('wicker warcrawler', 'Artifact Creature — Scarecrow').
card_types('wicker warcrawler', ['Artifact', 'Creature']).
card_subtypes('wicker warcrawler', ['Scarecrow']).
card_colors('wicker warcrawler', []).
card_text('wicker warcrawler', 'Whenever Wicker Warcrawler attacks or blocks, put a -1/-1 counter on it at end of combat.').
card_mana_cost('wicker warcrawler', ['5']).
card_cmc('wicker warcrawler', 5).
card_layout('wicker warcrawler', 'normal').
card_power('wicker warcrawler', 6).
card_toughness('wicker warcrawler', 6).

% Found in: ARC, EVE
card_name('wickerbough elder', 'Wickerbough Elder').
card_type('wickerbough elder', 'Creature — Treefolk Shaman').
card_types('wickerbough elder', ['Creature']).
card_subtypes('wickerbough elder', ['Treefolk', 'Shaman']).
card_colors('wickerbough elder', ['G']).
card_text('wickerbough elder', 'Wickerbough Elder enters the battlefield with a -1/-1 counter on it.\n{G}, Remove a -1/-1 counter from Wickerbough Elder: Destroy target artifact or enchantment.').
card_mana_cost('wickerbough elder', ['3', 'G']).
card_cmc('wickerbough elder', 4).
card_layout('wickerbough elder', 'normal').
card_power('wickerbough elder', 4).
card_toughness('wickerbough elder', 4).

% Found in: C13
card_name('widespread panic', 'Widespread Panic').
card_type('widespread panic', 'Enchantment').
card_types('widespread panic', ['Enchantment']).
card_subtypes('widespread panic', []).
card_colors('widespread panic', ['R']).
card_text('widespread panic', 'Whenever a spell or ability causes its controller to shuffle his or her library, that player puts a card from his or her hand on top of his or her library.').
card_mana_cost('widespread panic', ['2', 'R']).
card_cmc('widespread panic', 3).
card_layout('widespread panic', 'normal').

% Found in: PTK
card_name('wielding the green dragon', 'Wielding the Green Dragon').
card_type('wielding the green dragon', 'Sorcery').
card_types('wielding the green dragon', ['Sorcery']).
card_subtypes('wielding the green dragon', []).
card_colors('wielding the green dragon', ['G']).
card_text('wielding the green dragon', 'Target creature gets +4/+4 until end of turn.').
card_mana_cost('wielding the green dragon', ['1', 'G']).
card_cmc('wielding the green dragon', 2).
card_layout('wielding the green dragon', 'normal').

% Found in: C13, DDM, GTC
card_name('wight of precinct six', 'Wight of Precinct Six').
card_type('wight of precinct six', 'Creature — Zombie').
card_types('wight of precinct six', ['Creature']).
card_subtypes('wight of precinct six', ['Zombie']).
card_colors('wight of precinct six', ['B']).
card_text('wight of precinct six', 'Wight of Precinct Six gets +1/+1 for each creature card in your opponents\' graveyards.').
card_mana_cost('wight of precinct six', ['1', 'B']).
card_cmc('wight of precinct six', 2).
card_layout('wight of precinct six', 'normal').
card_power('wight of precinct six', 1).
card_toughness('wight of precinct six', 1).

% Found in: ICE, ME2
card_name('wiitigo', 'Wiitigo').
card_type('wiitigo', 'Creature — Yeti').
card_types('wiitigo', ['Creature']).
card_subtypes('wiitigo', ['Yeti']).
card_colors('wiitigo', ['G']).
card_text('wiitigo', 'Wiitigo enters the battlefield with six +1/+1 counters on it.\nAt the beginning of your upkeep, put a +1/+1 counter on Wiitigo if it has blocked or been blocked since your last upkeep. Otherwise, remove a +1/+1 counter from it.').
card_mana_cost('wiitigo', ['3', 'G', 'G', 'G']).
card_cmc('wiitigo', 6).
card_layout('wiitigo', 'normal').
card_power('wiitigo', 0).
card_toughness('wiitigo', 0).

% Found in: ALL, ME4
card_name('wild aesthir', 'Wild Aesthir').
card_type('wild aesthir', 'Creature — Bird').
card_types('wild aesthir', ['Creature']).
card_subtypes('wild aesthir', ['Bird']).
card_colors('wild aesthir', ['W']).
card_text('wild aesthir', 'Flying, first strike\n{W}{W}: Wild Aesthir gets +2/+0 until end of turn. Activate this ability only once each turn.').
card_mana_cost('wild aesthir', ['2', 'W']).
card_cmc('wild aesthir', 3).
card_layout('wild aesthir', 'normal').
card_power('wild aesthir', 1).
card_toughness('wild aesthir', 1).

% Found in: RTR
card_name('wild beastmaster', 'Wild Beastmaster').
card_type('wild beastmaster', 'Creature — Human Shaman').
card_types('wild beastmaster', ['Creature']).
card_subtypes('wild beastmaster', ['Human', 'Shaman']).
card_colors('wild beastmaster', ['G']).
card_text('wild beastmaster', 'Whenever Wild Beastmaster attacks, each other creature you control gets +X/+X until end of turn, where X is Wild Beastmaster\'s power.').
card_mana_cost('wild beastmaster', ['2', 'G']).
card_cmc('wild beastmaster', 3).
card_layout('wild beastmaster', 'normal').
card_power('wild beastmaster', 1).
card_toughness('wild beastmaster', 1).

% Found in: GPT
card_name('wild cantor', 'Wild Cantor').
card_type('wild cantor', 'Creature — Human Druid').
card_types('wild cantor', ['Creature']).
card_subtypes('wild cantor', ['Human', 'Druid']).
card_colors('wild cantor', ['R', 'G']).
card_text('wild cantor', '({R/G} can be paid with either {R} or {G}.)\nSacrifice Wild Cantor: Add one mana of any color to your mana pool.').
card_mana_cost('wild cantor', ['R/G']).
card_cmc('wild cantor', 1).
card_layout('wild cantor', 'normal').
card_power('wild cantor', 1).
card_toughness('wild cantor', 1).

% Found in: THS
card_name('wild celebrants', 'Wild Celebrants').
card_type('wild celebrants', 'Creature — Satyr').
card_types('wild celebrants', ['Creature']).
card_subtypes('wild celebrants', ['Satyr']).
card_colors('wild celebrants', ['R']).
card_text('wild celebrants', 'When Wild Celebrants enters the battlefield, you may destroy target artifact.').
card_mana_cost('wild celebrants', ['3', 'R', 'R']).
card_cmc('wild celebrants', 5).
card_layout('wild celebrants', 'normal').
card_power('wild celebrants', 5).
card_toughness('wild celebrants', 3).

% Found in: UDS
card_name('wild colos', 'Wild Colos').
card_type('wild colos', 'Creature — Goat Beast').
card_types('wild colos', ['Creature']).
card_subtypes('wild colos', ['Goat', 'Beast']).
card_colors('wild colos', ['R']).
card_text('wild colos', 'Haste').
card_mana_cost('wild colos', ['2', 'R']).
card_cmc('wild colos', 3).
card_layout('wild colos', 'normal').
card_power('wild colos', 2).
card_toughness('wild colos', 2).

% Found in: AVR
card_name('wild defiance', 'Wild Defiance').
card_type('wild defiance', 'Enchantment').
card_types('wild defiance', ['Enchantment']).
card_subtypes('wild defiance', []).
card_colors('wild defiance', ['G']).
card_text('wild defiance', 'Whenever a creature you control becomes the target of an instant or sorcery spell, that creature gets +3/+3 until end of turn.').
card_mana_cost('wild defiance', ['2', 'G']).
card_cmc('wild defiance', 3).
card_layout('wild defiance', 'normal').

% Found in: USG
card_name('wild dogs', 'Wild Dogs').
card_type('wild dogs', 'Creature — Hound').
card_types('wild dogs', ['Creature']).
card_subtypes('wild dogs', ['Hound']).
card_colors('wild dogs', ['G']).
card_text('wild dogs', 'At the beginning of your upkeep, if a player has more life than each other player, the player with the most life gains control of Wild Dogs.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('wild dogs', ['G']).
card_cmc('wild dogs', 1).
card_layout('wild dogs', 'normal').
card_power('wild dogs', 2).
card_toughness('wild dogs', 1).

% Found in: MIR
card_name('wild elephant', 'Wild Elephant').
card_type('wild elephant', 'Creature — Elephant').
card_types('wild elephant', ['Creature']).
card_subtypes('wild elephant', ['Elephant']).
card_colors('wild elephant', ['G']).
card_text('wild elephant', 'Trample').
card_mana_cost('wild elephant', ['3', 'G']).
card_cmc('wild elephant', 4).
card_layout('wild elephant', 'normal').
card_power('wild elephant', 3).
card_toughness('wild elephant', 3).

% Found in: M11
card_name('wild evocation', 'Wild Evocation').
card_type('wild evocation', 'Enchantment').
card_types('wild evocation', ['Enchantment']).
card_subtypes('wild evocation', []).
card_colors('wild evocation', ['R']).
card_text('wild evocation', 'At the beginning of each player\'s upkeep, that player reveals a card at random from his or her hand. If it\'s a land card, the player puts it onto the battlefield. Otherwise, the player casts it without paying its mana cost if able.').
card_mana_cost('wild evocation', ['5', 'R']).
card_cmc('wild evocation', 6).
card_layout('wild evocation', 'normal').

% Found in: 10E, M11, ME4, PO2, S00, S99
card_name('wild griffin', 'Wild Griffin').
card_type('wild griffin', 'Creature — Griffin').
card_types('wild griffin', ['Creature']).
card_subtypes('wild griffin', ['Griffin']).
card_colors('wild griffin', ['W']).
card_text('wild griffin', 'Flying').
card_mana_cost('wild griffin', ['2', 'W']).
card_cmc('wild griffin', 3).
card_layout('wild griffin', 'normal').
card_power('wild griffin', 2).
card_toughness('wild griffin', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, BTD, CED, CEI, ICE, LEA, LEB
card_name('wild growth', 'Wild Growth').
card_type('wild growth', 'Enchantment — Aura').
card_types('wild growth', ['Enchantment']).
card_subtypes('wild growth', ['Aura']).
card_colors('wild growth', ['G']).
card_text('wild growth', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds {G} to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('wild growth', ['G']).
card_cmc('wild growth', 1).
card_layout('wild growth', 'normal').

% Found in: M13, M14
card_name('wild guess', 'Wild Guess').
card_type('wild guess', 'Sorcery').
card_types('wild guess', ['Sorcery']).
card_subtypes('wild guess', []).
card_colors('wild guess', ['R']).
card_text('wild guess', 'As an additional cost to cast Wild Guess, discard a card.\nDraw two cards.').
card_mana_cost('wild guess', ['R', 'R']).
card_cmc('wild guess', 2).
card_layout('wild guess', 'normal').

% Found in: DKA
card_name('wild hunger', 'Wild Hunger').
card_type('wild hunger', 'Instant').
card_types('wild hunger', ['Instant']).
card_subtypes('wild hunger', []).
card_colors('wild hunger', ['G']).
card_text('wild hunger', 'Target creature gets +3/+1 and gains trample until end of turn.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('wild hunger', ['2', 'G']).
card_cmc('wild hunger', 3).
card_layout('wild hunger', 'normal').

% Found in: ORI
card_name('wild instincts', 'Wild Instincts').
card_type('wild instincts', 'Sorcery').
card_types('wild instincts', ['Sorcery']).
card_subtypes('wild instincts', []).
card_colors('wild instincts', ['G']).
card_text('wild instincts', 'Target creature you control gets +2/+2 until end of turn. It fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_mana_cost('wild instincts', ['3', 'G']).
card_cmc('wild instincts', 4).
card_layout('wild instincts', 'normal').

% Found in: MMQ
card_name('wild jhovall', 'Wild Jhovall').
card_type('wild jhovall', 'Creature — Cat').
card_types('wild jhovall', ['Creature']).
card_subtypes('wild jhovall', ['Cat']).
card_colors('wild jhovall', ['R']).
card_text('wild jhovall', '').
card_mana_cost('wild jhovall', ['3', 'R']).
card_cmc('wild jhovall', 4).
card_layout('wild jhovall', 'normal').
card_power('wild jhovall', 3).
card_toughness('wild jhovall', 3).

% Found in: CON
card_name('wild leotau', 'Wild Leotau').
card_type('wild leotau', 'Creature — Cat').
card_types('wild leotau', ['Creature']).
card_subtypes('wild leotau', ['Cat']).
card_colors('wild leotau', ['G']).
card_text('wild leotau', 'At the beginning of your upkeep, sacrifice Wild Leotau unless you pay {G}.').
card_mana_cost('wild leotau', ['2', 'G', 'G']).
card_cmc('wild leotau', 4).
card_layout('wild leotau', 'normal').
card_power('wild leotau', 5).
card_toughness('wild leotau', 4).

% Found in: NMS
card_name('wild mammoth', 'Wild Mammoth').
card_type('wild mammoth', 'Creature — Elephant').
card_types('wild mammoth', ['Creature']).
card_subtypes('wild mammoth', ['Elephant']).
card_colors('wild mammoth', ['G']).
card_text('wild mammoth', 'At the beginning of your upkeep, if a player controls more creatures than each other player, the player who controls the most creatures gains control of Wild Mammoth.').
card_mana_cost('wild mammoth', ['2', 'G']).
card_cmc('wild mammoth', 3).
card_layout('wild mammoth', 'normal').
card_power('wild mammoth', 3).
card_toughness('wild mammoth', 4).

% Found in: PCY
card_name('wild might', 'Wild Might').
card_type('wild might', 'Instant').
card_types('wild might', ['Instant']).
card_subtypes('wild might', []).
card_colors('wild might', ['G']).
card_text('wild might', 'Target creature gets +1/+1 until end of turn. That creature gets an additional +4/+4 until end of turn unless any player pays {2}.').
card_mana_cost('wild might', ['1', 'G']).
card_cmc('wild might', 2).
card_layout('wild might', 'normal').

% Found in: DD3_GVL, DDD, ODY, VMA, pFNM
card_name('wild mongrel', 'Wild Mongrel').
card_type('wild mongrel', 'Creature — Hound').
card_types('wild mongrel', ['Creature']).
card_subtypes('wild mongrel', ['Hound']).
card_colors('wild mongrel', ['G']).
card_text('wild mongrel', 'Discard a card: Wild Mongrel gets +1/+1 and becomes the color of your choice until end of turn.').
card_mana_cost('wild mongrel', ['1', 'G']).
card_cmc('wild mongrel', 2).
card_layout('wild mongrel', 'normal').
card_power('wild mongrel', 2).
card_toughness('wild mongrel', 2).

% Found in: ALA, DDH, pFNM
card_name('wild nacatl', 'Wild Nacatl').
card_type('wild nacatl', 'Creature — Cat Warrior').
card_types('wild nacatl', ['Creature']).
card_subtypes('wild nacatl', ['Cat', 'Warrior']).
card_colors('wild nacatl', ['G']).
card_text('wild nacatl', 'Wild Nacatl gets +1/+1 as long as you control a Mountain.\nWild Nacatl gets +1/+1 as long as you control a Plains.').
card_mana_cost('wild nacatl', ['G']).
card_cmc('wild nacatl', 1).
card_layout('wild nacatl', 'normal').
card_power('wild nacatl', 1).
card_toughness('wild nacatl', 1).

% Found in: ME4, PO2, S99
card_name('wild ox', 'Wild Ox').
card_type('wild ox', 'Creature — Ox').
card_types('wild ox', ['Creature']).
card_subtypes('wild ox', ['Ox']).
card_colors('wild ox', ['G']).
card_text('wild ox', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('wild ox', ['3', 'G']).
card_cmc('wild ox', 4).
card_layout('wild ox', 'normal').
card_power('wild ox', 3).
card_toughness('wild ox', 3).

% Found in: H09, PLC
card_name('wild pair', 'Wild Pair').
card_type('wild pair', 'Enchantment').
card_types('wild pair', ['Enchantment']).
card_subtypes('wild pair', []).
card_colors('wild pair', ['G']).
card_text('wild pair', 'Whenever a creature enters the battlefield, if you cast it from your hand, you may search your library for a creature card with the same total power and toughness and put it onto the battlefield. If you do, shuffle your library.').
card_mana_cost('wild pair', ['4', 'G', 'G']).
card_cmc('wild pair', 6).
card_layout('wild pair', 'normal').

% Found in: APC
card_name('wild research', 'Wild Research').
card_type('wild research', 'Enchantment').
card_types('wild research', ['Enchantment']).
card_subtypes('wild research', []).
card_colors('wild research', ['R']).
card_text('wild research', '{1}{W}: Search your library for an enchantment card and reveal that card. Put it into your hand, then discard a card at random. Then shuffle your library.\n{1}{U}: Search your library for an instant card and reveal that card. Put it into your hand, then discard a card at random. Then shuffle your library.').
card_mana_cost('wild research', ['2', 'R']).
card_cmc('wild research', 3).
card_layout('wild research', 'normal').

% Found in: C13, CMD, LRW, M14
card_name('wild ricochet', 'Wild Ricochet').
card_type('wild ricochet', 'Instant').
card_types('wild ricochet', ['Instant']).
card_subtypes('wild ricochet', []).
card_colors('wild ricochet', ['R']).
card_text('wild ricochet', 'You may choose new targets for target instant or sorcery spell. Then copy that spell. You may choose new targets for the copy.').
card_mana_cost('wild ricochet', ['2', 'R', 'R']).
card_cmc('wild ricochet', 4).
card_layout('wild ricochet', 'normal').

% Found in: FRF
card_name('wild slash', 'Wild Slash').
card_type('wild slash', 'Instant').
card_types('wild slash', ['Instant']).
card_subtypes('wild slash', []).
card_colors('wild slash', ['R']).
card_text('wild slash', 'Ferocious — If you control a creature with power 4 or greater, damage can\'t be prevented this turn.\nWild Slash deals 2 damage to target creature or player.').
card_mana_cost('wild slash', ['R']).
card_cmc('wild slash', 1).
card_layout('wild slash', 'normal').

% Found in: SHM
card_name('wild swing', 'Wild Swing').
card_type('wild swing', 'Sorcery').
card_types('wild swing', ['Sorcery']).
card_subtypes('wild swing', []).
card_colors('wild swing', ['R']).
card_text('wild swing', 'Choose three target nonenchantment permanents. Destroy one of them at random.').
card_mana_cost('wild swing', ['3', 'R']).
card_cmc('wild swing', 4).
card_layout('wild swing', 'normal').

% Found in: TMP
card_name('wild wurm', 'Wild Wurm').
card_type('wild wurm', 'Creature — Wurm').
card_types('wild wurm', ['Creature']).
card_subtypes('wild wurm', ['Wurm']).
card_colors('wild wurm', ['R']).
card_text('wild wurm', 'When Wild Wurm enters the battlefield, flip a coin. If you lose the flip, return Wild Wurm to its owner\'s hand.').
card_mana_cost('wild wurm', ['3', 'R']).
card_cmc('wild wurm', 4).
card_layout('wild wurm', 'normal').
card_power('wild wurm', 5).
card_toughness('wild wurm', 4).

% Found in: ISD
card_name('wildblood pack', 'Wildblood Pack').
card_type('wildblood pack', 'Creature — Werewolf').
card_types('wildblood pack', ['Creature']).
card_subtypes('wildblood pack', ['Werewolf']).
card_colors('wildblood pack', ['R']).
card_text('wildblood pack', 'Trample\nAttacking creatures you control get +3/+0.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Wildblood Pack.').
card_layout('wildblood pack', 'double-faced').
card_power('wildblood pack', 5).
card_toughness('wildblood pack', 5).

% Found in: FRF, FRF_UGIN
card_name('wildcall', 'Wildcall').
card_type('wildcall', 'Sorcery').
card_types('wildcall', ['Sorcery']).
card_subtypes('wildcall', []).
card_colors('wildcall', ['G']).
card_text('wildcall', 'Manifest the top card of your library, then put X +1/+1 counters on it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('wildcall', ['X', 'G', 'G']).
card_cmc('wildcall', 2).
card_layout('wildcall', 'normal').

% Found in: CSP
card_name('wilderness elemental', 'Wilderness Elemental').
card_type('wilderness elemental', 'Creature — Elemental').
card_types('wilderness elemental', ['Creature']).
card_subtypes('wilderness elemental', ['Elemental']).
card_colors('wilderness elemental', ['R', 'G']).
card_text('wilderness elemental', 'Trample\nWilderness Elemental\'s power is equal to the number of nonbasic lands your opponents control.').
card_mana_cost('wilderness elemental', ['1', 'R', 'G']).
card_cmc('wilderness elemental', 3).
card_layout('wilderness elemental', 'normal').
card_power('wilderness elemental', '*').
card_toughness('wilderness elemental', 3).

% Found in: EVE
card_name('wilderness hypnotist', 'Wilderness Hypnotist').
card_type('wilderness hypnotist', 'Creature — Merfolk Wizard').
card_types('wilderness hypnotist', ['Creature']).
card_subtypes('wilderness hypnotist', ['Merfolk', 'Wizard']).
card_colors('wilderness hypnotist', ['U']).
card_text('wilderness hypnotist', '{T}: Target red or green creature gets -2/-0 until end of turn.').
card_mana_cost('wilderness hypnotist', ['2', 'U', 'U']).
card_cmc('wilderness hypnotist', 4).
card_layout('wilderness hypnotist', 'normal').
card_power('wilderness hypnotist', 1).
card_toughness('wilderness hypnotist', 3).

% Found in: ARB
card_name('wildfield borderpost', 'Wildfield Borderpost').
card_type('wildfield borderpost', 'Artifact').
card_types('wildfield borderpost', ['Artifact']).
card_subtypes('wildfield borderpost', []).
card_colors('wildfield borderpost', ['W', 'G']).
card_text('wildfield borderpost', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Wildfield Borderpost\'s mana cost.\nWildfield Borderpost enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_mana_cost('wildfield borderpost', ['1', 'G', 'W']).
card_cmc('wildfield borderpost', 3).
card_layout('wildfield borderpost', 'normal').

% Found in: 7ED, 9ED, MM2, PO2, USG
card_name('wildfire', 'Wildfire').
card_type('wildfire', 'Sorcery').
card_types('wildfire', ['Sorcery']).
card_subtypes('wildfire', []).
card_colors('wildfire', ['R']).
card_text('wildfire', 'Each player sacrifices four lands. Wildfire deals 4 damage to each creature.').
card_mana_cost('wildfire', ['4', 'R', 'R']).
card_cmc('wildfire', 6).
card_layout('wildfire', 'normal').

% Found in: JOU
card_name('wildfire cerberus', 'Wildfire Cerberus').
card_type('wildfire cerberus', 'Creature — Hound').
card_types('wildfire cerberus', ['Creature']).
card_subtypes('wildfire cerberus', ['Hound']).
card_colors('wildfire cerberus', ['R']).
card_text('wildfire cerberus', '{5}{R}{R}: Monstrosity 1. (If this creature isn\'t monstrous, put a +1/+1 counter on it and it becomes monstrous.)\nWhen Wildfire Cerberus becomes monstrous, it deals 2 damage to each opponent and each creature your opponents control.').
card_mana_cost('wildfire cerberus', ['4', 'R']).
card_cmc('wildfire cerberus', 5).
card_layout('wildfire cerberus', 'normal').
card_power('wildfire cerberus', 4).
card_toughness('wildfire cerberus', 3).

% Found in: BRB, MIR, TSB
card_name('wildfire emissary', 'Wildfire Emissary').
card_type('wildfire emissary', 'Creature — Efreet').
card_types('wildfire emissary', ['Creature']).
card_subtypes('wildfire emissary', ['Efreet']).
card_colors('wildfire emissary', ['R']).
card_text('wildfire emissary', 'Protection from white\n{1}{R}: Wildfire Emissary gets +1/+0 until end of turn.').
card_mana_cost('wildfire emissary', ['3', 'R']).
card_cmc('wildfire emissary', 4).
card_layout('wildfire emissary', 'normal').
card_power('wildfire emissary', 2).
card_toughness('wildfire emissary', 4).

% Found in: DDP, ROE
card_name('wildheart invoker', 'Wildheart Invoker').
card_type('wildheart invoker', 'Creature — Elf Shaman').
card_types('wildheart invoker', ['Creature']).
card_subtypes('wildheart invoker', ['Elf', 'Shaman']).
card_colors('wildheart invoker', ['G']).
card_text('wildheart invoker', '{8}: Target creature gets +5/+5 and gains trample until end of turn.').
card_mana_cost('wildheart invoker', ['2', 'G', 'G']).
card_cmc('wildheart invoker', 4).
card_layout('wildheart invoker', 'normal').
card_power('wildheart invoker', 4).
card_toughness('wildheart invoker', 3).

% Found in: DD3_EVG, EVG, GPT
card_name('wildsize', 'Wildsize').
card_type('wildsize', 'Instant').
card_types('wildsize', ['Instant']).
card_subtypes('wildsize', []).
card_colors('wildsize', ['G']).
card_text('wildsize', 'Target creature gets +2/+2 and gains trample until end of turn.\nDraw a card.').
card_mana_cost('wildsize', ['2', 'G']).
card_cmc('wildsize', 3).
card_layout('wildsize', 'normal').

% Found in: SHM
card_name('wildslayer elves', 'Wildslayer Elves').
card_type('wildslayer elves', 'Creature — Elf Warrior').
card_types('wildslayer elves', ['Creature']).
card_subtypes('wildslayer elves', ['Elf', 'Warrior']).
card_colors('wildslayer elves', ['G']).
card_text('wildslayer elves', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('wildslayer elves', ['3', 'G']).
card_cmc('wildslayer elves', 4).
card_layout('wildslayer elves', 'normal').
card_power('wildslayer elves', 3).
card_toughness('wildslayer elves', 3).

% Found in: AVR
card_name('wildwood geist', 'Wildwood Geist').
card_type('wildwood geist', 'Creature — Spirit').
card_types('wildwood geist', ['Creature']).
card_subtypes('wildwood geist', ['Spirit']).
card_colors('wildwood geist', ['G']).
card_text('wildwood geist', 'Wildwood Geist gets +2/+2 as long as it\'s your turn.').
card_mana_cost('wildwood geist', ['4', 'G']).
card_cmc('wildwood geist', 5).
card_layout('wildwood geist', 'normal').
card_power('wildwood geist', 3).
card_toughness('wildwood geist', 3).

% Found in: GTC
card_name('wildwood rebirth', 'Wildwood Rebirth').
card_type('wildwood rebirth', 'Instant').
card_types('wildwood rebirth', ['Instant']).
card_subtypes('wildwood rebirth', []).
card_colors('wildwood rebirth', ['G']).
card_text('wildwood rebirth', 'Return target creature card from your graveyard to your hand.').
card_mana_cost('wildwood rebirth', ['1', 'G']).
card_cmc('wildwood rebirth', 2).
card_layout('wildwood rebirth', 'normal').

% Found in: FRF
card_name('will of the naga', 'Will of the Naga').
card_type('will of the naga', 'Instant').
card_types('will of the naga', ['Instant']).
card_subtypes('will of the naga', []).
card_colors('will of the naga', ['U']).
card_text('will of the naga', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_mana_cost('will of the naga', ['4', 'U', 'U']).
card_cmc('will of the naga', 6).
card_layout('will of the naga', 'normal').

% Found in: M15
card_name('will-forged golem', 'Will-Forged Golem').
card_type('will-forged golem', 'Artifact Creature — Golem').
card_types('will-forged golem', ['Artifact', 'Creature']).
card_subtypes('will-forged golem', ['Golem']).
card_colors('will-forged golem', []).
card_text('will-forged golem', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)').
card_mana_cost('will-forged golem', ['6']).
card_cmc('will-forged golem', 6).
card_layout('will-forged golem', 'normal').
card_power('will-forged golem', 4).
card_toughness('will-forged golem', 4).

% Found in: 2ED, 3ED, 4ED, 9ED, CED, CEI, LEA, LEB
card_name('will-o\'-the-wisp', 'Will-o\'-the-Wisp').
card_type('will-o\'-the-wisp', 'Creature — Spirit').
card_types('will-o\'-the-wisp', ['Creature']).
card_subtypes('will-o\'-the-wisp', ['Spirit']).
card_colors('will-o\'-the-wisp', ['B']).
card_text('will-o\'-the-wisp', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{B}: Regenerate Will-o\'-the-Wisp. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('will-o\'-the-wisp', ['B']).
card_cmc('will-o\'-the-wisp', 1).
card_layout('will-o\'-the-wisp', 'normal').
card_power('will-o\'-the-wisp', 0).
card_toughness('will-o\'-the-wisp', 1).

% Found in: C14, DD2, DD3_JVC, DDN, LGN, TSB, pFNM
card_name('willbender', 'Willbender').
card_type('willbender', 'Creature — Human Wizard').
card_types('willbender', ['Creature']).
card_subtypes('willbender', ['Human', 'Wizard']).
card_colors('willbender', ['U']).
card_text('willbender', 'Morph {1}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Willbender is turned face up, change the target of target spell or ability with a single target.').
card_mana_cost('willbender', ['1', 'U']).
card_cmc('willbender', 2).
card_layout('willbender', 'normal').
card_power('willbender', 1).
card_toughness('willbender', 2).

% Found in: ORI
card_name('willbreaker', 'Willbreaker').
card_type('willbreaker', 'Creature — Human Wizard').
card_types('willbreaker', ['Creature']).
card_subtypes('willbreaker', ['Human', 'Wizard']).
card_colors('willbreaker', ['U']).
card_text('willbreaker', 'Whenever a creature an opponent controls becomes the target of a spell or ability you control, gain control of that creature for as long as you control Willbreaker.').
card_mana_cost('willbreaker', ['3', 'U', 'U']).
card_cmc('willbreaker', 5).
card_layout('willbreaker', 'normal').
card_power('willbreaker', 2).
card_toughness('willbreaker', 3).

% Found in: DGM
card_name('willing', 'Willing').
card_type('willing', 'Instant').
card_types('willing', ['Instant']).
card_subtypes('willing', []).
card_colors('willing', ['W', 'B']).
card_text('willing', 'Creatures you control gain deathtouch and lifelink until end of turn.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('willing', ['1', 'W', 'B']).
card_cmc('willing', 3).
card_layout('willing', 'split').

% Found in: POR
card_name('willow dryad', 'Willow Dryad').
card_type('willow dryad', 'Creature — Dryad').
card_types('willow dryad', ['Creature']).
card_subtypes('willow dryad', ['Dryad']).
card_colors('willow dryad', ['G']).
card_text('willow dryad', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('willow dryad', ['G']).
card_cmc('willow dryad', 1).
card_layout('willow dryad', 'normal').
card_power('willow dryad', 1).
card_toughness('willow dryad', 1).

% Found in: S00, S99
card_name('willow elf', 'Willow Elf').
card_type('willow elf', 'Creature — Elf').
card_types('willow elf', ['Creature']).
card_subtypes('willow elf', ['Elf']).
card_colors('willow elf', ['G']).
card_text('willow elf', '').
card_mana_cost('willow elf', ['G']).
card_cmc('willow elf', 1).
card_layout('willow elf', 'normal').
card_power('willow elf', 1).
card_toughness('willow elf', 1).

% Found in: HML
card_name('willow faerie', 'Willow Faerie').
card_type('willow faerie', 'Creature — Faerie').
card_types('willow faerie', ['Creature']).
card_subtypes('willow faerie', ['Faerie']).
card_colors('willow faerie', ['G']).
card_text('willow faerie', 'Flying').
card_mana_cost('willow faerie', ['1', 'G']).
card_cmc('willow faerie', 2).
card_layout('willow faerie', 'normal').
card_power('willow faerie', 1).
card_toughness('willow faerie', 2).

% Found in: HML, ME3
card_name('willow priestess', 'Willow Priestess').
card_type('willow priestess', 'Creature — Faerie Druid').
card_types('willow priestess', ['Creature']).
card_subtypes('willow priestess', ['Faerie', 'Druid']).
card_colors('willow priestess', ['G']).
card_text('willow priestess', '{T}: You may put a Faerie permanent card from your hand onto the battlefield.\n{2}{G}: Target green creature gains protection from black until end of turn.').
card_mana_cost('willow priestess', ['2', 'G', 'G']).
card_cmc('willow priestess', 4).
card_layout('willow priestess', 'normal').
card_power('willow priestess', 2).
card_toughness('willow priestess', 2).
card_reserved('willow priestess').

% Found in: LEG, ME3
card_name('willow satyr', 'Willow Satyr').
card_type('willow satyr', 'Creature — Satyr').
card_types('willow satyr', ['Creature']).
card_subtypes('willow satyr', ['Satyr']).
card_colors('willow satyr', ['G']).
card_text('willow satyr', 'You may choose not to untap Willow Satyr during your untap step.\n{T}: Gain control of target legendary creature for as long as you control Willow Satyr and Willow Satyr remains tapped.').
card_mana_cost('willow satyr', ['2', 'G', 'G']).
card_cmc('willow satyr', 4).
card_layout('willow satyr', 'normal').
card_power('willow satyr', 1).
card_toughness('willow satyr', 1).
card_reserved('willow satyr').

% Found in: DDG, SHM, pGTW
card_name('wilt-leaf cavaliers', 'Wilt-Leaf Cavaliers').
card_type('wilt-leaf cavaliers', 'Creature — Elf Knight').
card_types('wilt-leaf cavaliers', ['Creature']).
card_subtypes('wilt-leaf cavaliers', ['Elf', 'Knight']).
card_colors('wilt-leaf cavaliers', ['W', 'G']).
card_text('wilt-leaf cavaliers', 'Vigilance').
card_mana_cost('wilt-leaf cavaliers', ['G/W', 'G/W', 'G/W']).
card_cmc('wilt-leaf cavaliers', 3).
card_layout('wilt-leaf cavaliers', 'normal').
card_power('wilt-leaf cavaliers', 3).
card_toughness('wilt-leaf cavaliers', 4).

% Found in: MM2, SHM
card_name('wilt-leaf liege', 'Wilt-Leaf Liege').
card_type('wilt-leaf liege', 'Creature — Elf Knight').
card_types('wilt-leaf liege', ['Creature']).
card_subtypes('wilt-leaf liege', ['Elf', 'Knight']).
card_colors('wilt-leaf liege', ['W', 'G']).
card_text('wilt-leaf liege', 'Other green creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\nIf a spell or ability an opponent controls causes you to discard Wilt-Leaf Liege, put it onto the battlefield instead of putting it into your graveyard.').
card_mana_cost('wilt-leaf liege', ['1', 'G/W', 'G/W', 'G/W']).
card_cmc('wilt-leaf liege', 4).
card_layout('wilt-leaf liege', 'normal').
card_power('wilt-leaf liege', 4).
card_toughness('wilt-leaf liege', 4).

% Found in: 7ED, CNS, TMP, TPR
card_name('wind dancer', 'Wind Dancer').
card_type('wind dancer', 'Creature — Faerie').
card_types('wind dancer', ['Creature']).
card_subtypes('wind dancer', ['Faerie']).
card_colors('wind dancer', ['U']).
card_text('wind dancer', 'Flying\n{T}: Target creature gains flying until end of turn.').
card_mana_cost('wind dancer', ['1', 'U']).
card_cmc('wind dancer', 2).
card_layout('wind dancer', 'normal').
card_power('wind dancer', 1).
card_toughness('wind dancer', 1).

% Found in: 6ED, 7ED, 8ED, 9ED, BRB, DGM, M10, M13, POR, S00, S99, TMP, TPR
card_name('wind drake', 'Wind Drake').
card_type('wind drake', 'Creature — Drake').
card_types('wind drake', ['Creature']).
card_subtypes('wind drake', ['Drake']).
card_colors('wind drake', ['U']).
card_text('wind drake', 'Flying').
card_mana_cost('wind drake', ['2', 'U']).
card_cmc('wind drake', 3).
card_layout('wind drake', 'normal').
card_power('wind drake', 2).
card_toughness('wind drake', 2).

% Found in: PO2, S99
card_name('wind sail', 'Wind Sail').
card_type('wind sail', 'Sorcery').
card_types('wind sail', ['Sorcery']).
card_subtypes('wind sail', []).
card_colors('wind sail', ['U']).
card_text('wind sail', 'One or two target creatures gain flying until end of turn.').
card_mana_cost('wind sail', ['1', 'U']).
card_cmc('wind sail', 2).
card_layout('wind sail', 'normal').

% Found in: VIS
card_name('wind shear', 'Wind Shear').
card_type('wind shear', 'Instant').
card_types('wind shear', ['Instant']).
card_subtypes('wind shear', []).
card_colors('wind shear', ['G']).
card_text('wind shear', 'Attacking creatures with flying get -2/-2 and lose flying until end of turn.').
card_mana_cost('wind shear', ['2', 'G']).
card_cmc('wind shear', 3).
card_layout('wind shear', 'normal').

% Found in: 5ED, 6ED, ICE, ME2
card_name('wind spirit', 'Wind Spirit').
card_type('wind spirit', 'Creature — Elemental Spirit').
card_types('wind spirit', ['Creature']).
card_subtypes('wind spirit', ['Elemental', 'Spirit']).
card_colors('wind spirit', ['U']).
card_text('wind spirit', 'Flying\nMenace (This creature can\'t be blocked except by two or more creatures.)').
card_mana_cost('wind spirit', ['4', 'U']).
card_cmc('wind spirit', 5).
card_layout('wind spirit', 'normal').
card_power('wind spirit', 3).
card_toughness('wind spirit', 2).

% Found in: WWK
card_name('wind zendikon', 'Wind Zendikon').
card_type('wind zendikon', 'Enchantment — Aura').
card_types('wind zendikon', ['Enchantment']).
card_subtypes('wind zendikon', ['Aura']).
card_colors('wind zendikon', ['U']).
card_text('wind zendikon', 'Enchant land\nEnchanted land is a 2/2 blue Elemental creature with flying. It\'s still a land.\nWhen enchanted land dies, return that card to its owner\'s hand.').
card_mana_cost('wind zendikon', ['U']).
card_cmc('wind zendikon', 1).
card_layout('wind zendikon', 'normal').

% Found in: FRF, KTK
card_name('wind-scarred crag', 'Wind-Scarred Crag').
card_type('wind-scarred crag', 'Land').
card_types('wind-scarred crag', ['Land']).
card_subtypes('wind-scarred crag', []).
card_colors('wind-scarred crag', []).
card_text('wind-scarred crag', 'Wind-Scarred Crag enters the battlefield tapped.\nWhen Wind-Scarred Crag enters the battlefield, you gain 1 life.\n{T}: Add {R} or {W} to your mana pool.').
card_layout('wind-scarred crag', 'normal').

% Found in: 10E, CMD, LGN
card_name('windborn muse', 'Windborn Muse').
card_type('windborn muse', 'Creature — Spirit').
card_types('windborn muse', ['Creature']).
card_subtypes('windborn muse', ['Spirit']).
card_colors('windborn muse', ['W']).
card_text('windborn muse', 'Flying\nCreatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_mana_cost('windborn muse', ['3', 'W']).
card_cmc('windborn muse', 4).
card_layout('windborn muse', 'normal').
card_power('windborn muse', 2).
card_toughness('windborn muse', 3).

% Found in: ZEN
card_name('windborne charge', 'Windborne Charge').
card_type('windborne charge', 'Sorcery').
card_types('windborne charge', ['Sorcery']).
card_subtypes('windborne charge', []).
card_colors('windborne charge', ['W']).
card_text('windborne charge', 'Two target creatures you control each get +2/+2 and gain flying until end of turn.').
card_mana_cost('windborne charge', ['2', 'W', 'W']).
card_cmc('windborne charge', 4).
card_layout('windborne charge', 'normal').

% Found in: LRW, MD1, V12
card_name('windbrisk heights', 'Windbrisk Heights').
card_type('windbrisk heights', 'Land').
card_types('windbrisk heights', ['Land']).
card_subtypes('windbrisk heights', []).
card_colors('windbrisk heights', []).
card_text('windbrisk heights', 'Hideaway (This land enters the battlefield tapped. When it does, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library.)\n{T}: Add {W} to your mana pool.\n{W}, {T}: You may play the exiled card without paying its mana cost if you attacked with three or more creatures this turn.').
card_layout('windbrisk heights', 'normal').

% Found in: SHM
card_name('windbrisk raptor', 'Windbrisk Raptor').
card_type('windbrisk raptor', 'Creature — Bird').
card_types('windbrisk raptor', ['Creature']).
card_subtypes('windbrisk raptor', ['Bird']).
card_colors('windbrisk raptor', ['W']).
card_text('windbrisk raptor', 'Flying\nAttacking creatures you control have lifelink.').
card_mana_cost('windbrisk raptor', ['5', 'W', 'W']).
card_cmc('windbrisk raptor', 7).
card_layout('windbrisk raptor', 'normal').
card_power('windbrisk raptor', 5).
card_toughness('windbrisk raptor', 7).

% Found in: BRB, CMD, USG
card_name('windfall', 'Windfall').
card_type('windfall', 'Sorcery').
card_types('windfall', ['Sorcery']).
card_subtypes('windfall', []).
card_colors('windfall', ['U']).
card_text('windfall', 'Each player discards his or her hand, then draws cards equal to the greatest number of cards a player discarded this way.').
card_mana_cost('windfall', ['2', 'U']).
card_cmc('windfall', 3).
card_layout('windfall', 'normal').

% Found in: WTH
card_name('winding canyons', 'Winding Canyons').
card_type('winding canyons', 'Land').
card_types('winding canyons', ['Land']).
card_subtypes('winding canyons', []).
card_colors('winding canyons', []).
card_text('winding canyons', '{T}: Add {1} to your mana pool.\n{2}, {T}: Until end of turn, you may play creature cards as though they had flash.').
card_layout('winding canyons', 'normal').
card_reserved('winding canyons').

% Found in: USG
card_name('winding wurm', 'Winding Wurm').
card_type('winding wurm', 'Creature — Wurm').
card_types('winding wurm', ['Creature']).
card_subtypes('winding wurm', ['Wurm']).
card_colors('winding wurm', ['G']).
card_text('winding wurm', 'Echo {4}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('winding wurm', ['4', 'G']).
card_cmc('winding wurm', 5).
card_layout('winding wurm', 'normal').
card_power('winding wurm', 6).
card_toughness('winding wurm', 6).

% Found in: M14
card_name('windreader sphinx', 'Windreader Sphinx').
card_type('windreader sphinx', 'Creature — Sphinx').
card_types('windreader sphinx', ['Creature']).
card_subtypes('windreader sphinx', ['Sphinx']).
card_colors('windreader sphinx', ['U']).
card_text('windreader sphinx', 'Flying\nWhenever a creature with flying attacks, you may draw a card.').
card_mana_cost('windreader sphinx', ['5', 'U', 'U']).
card_cmc('windreader sphinx', 7).
card_layout('windreader sphinx', 'normal').
card_power('windreader sphinx', 3).
card_toughness('windreader sphinx', 7).

% Found in: MIR
card_name('windreaper falcon', 'Windreaper Falcon').
card_type('windreaper falcon', 'Creature — Bird').
card_types('windreaper falcon', ['Creature']).
card_subtypes('windreaper falcon', ['Bird']).
card_colors('windreaper falcon', ['R', 'G']).
card_text('windreaper falcon', 'Flying, protection from blue').
card_mana_cost('windreaper falcon', ['1', 'R', 'G']).
card_cmc('windreaper falcon', 3).
card_layout('windreaper falcon', 'normal').
card_power('windreaper falcon', 1).
card_toughness('windreaper falcon', 1).

% Found in: DDI, DIS
card_name('windreaver', 'Windreaver').
card_type('windreaver', 'Creature — Elemental').
card_types('windreaver', ['Creature']).
card_subtypes('windreaver', ['Elemental']).
card_colors('windreaver', ['W', 'U']).
card_text('windreaver', 'Flying\n{W}: Windreaver gains vigilance until end of turn.\n{W}: Windreaver gets +0/+1 until end of turn.\n{U}: Switch Windreaver\'s power and toughness until end of turn.\n{U}: Return Windreaver to its owner\'s hand.').
card_mana_cost('windreaver', ['3', 'W', 'U']).
card_cmc('windreaver', 5).
card_layout('windreaver', 'normal').
card_power('windreaver', 1).
card_toughness('windreaver', 3).

% Found in: PC2
card_name('windriddle palaces', 'Windriddle Palaces').
card_type('windriddle palaces', 'Plane — Belenon').
card_types('windriddle palaces', ['Plane']).
card_subtypes('windriddle palaces', ['Belenon']).
card_colors('windriddle palaces', []).
card_text('windriddle palaces', 'Players play with the top card of their libraries revealed.\nYou may play the top card of any player\'s library.\nWhenever you roll {C}, each player puts the top card of his or her library into his or her graveyard.').
card_layout('windriddle palaces', 'plane').

% Found in: ZEN
card_name('windrider eel', 'Windrider Eel').
card_type('windrider eel', 'Creature — Fish').
card_types('windrider eel', ['Creature']).
card_subtypes('windrider eel', ['Fish']).
card_colors('windrider eel', ['U']).
card_text('windrider eel', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, Windrider Eel gets +2/+2 until end of turn.').
card_mana_cost('windrider eel', ['3', 'U']).
card_cmc('windrider eel', 4).
card_layout('windrider eel', 'normal').
card_power('windrider eel', 2).
card_toughness('windrider eel', 2).

% Found in: BFZ
card_name('windrider patrol', 'Windrider Patrol').
card_type('windrider patrol', 'Creature — Merfolk Wizard').
card_types('windrider patrol', ['Creature']).
card_subtypes('windrider patrol', ['Merfolk', 'Wizard']).
card_colors('windrider patrol', ['U']).
card_text('windrider patrol', 'Flying\nWhenever Windrider Patrol deals combat damage to a player, scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('windrider patrol', ['3', 'U', 'U']).
card_cmc('windrider patrol', 5).
card_layout('windrider patrol', 'normal').
card_power('windrider patrol', 4).
card_toughness('windrider patrol', 3).

% Found in: 4ED, 5ED, LEG, MED, POR
card_name('winds of change', 'Winds of Change').
card_type('winds of change', 'Sorcery').
card_types('winds of change', ['Sorcery']).
card_subtypes('winds of change', []).
card_colors('winds of change', ['R']).
card_text('winds of change', 'Each player shuffles the cards from his or her hand into his or her library, then draws that many cards.').
card_mana_cost('winds of change', ['R']).
card_cmc('winds of change', 1).
card_layout('winds of change', 'normal').

% Found in: FRF
card_name('winds of qal sisma', 'Winds of Qal Sisma').
card_type('winds of qal sisma', 'Instant').
card_types('winds of qal sisma', ['Instant']).
card_subtypes('winds of qal sisma', []).
card_colors('winds of qal sisma', ['G']).
card_text('winds of qal sisma', 'Prevent all combat damage that would be dealt this turn.\nFerocious — If you control a creature with power 4 or greater, instead prevent all combat damage that would be dealt this turn by creatures your opponents control.').
card_mana_cost('winds of qal sisma', ['1', 'G']).
card_cmc('winds of qal sisma', 2).
card_layout('winds of qal sisma', 'normal').

% Found in: DDL, TMP, TPR, VMA
card_name('winds of rath', 'Winds of Rath').
card_type('winds of rath', 'Sorcery').
card_types('winds of rath', ['Sorcery']).
card_subtypes('winds of rath', []).
card_colors('winds of rath', ['W']).
card_text('winds of rath', 'Destroy all creatures that aren\'t enchanted. They can\'t be regenerated.').
card_mana_cost('winds of rath', ['3', 'W', 'W']).
card_cmc('winds of rath', 5).
card_layout('winds of rath', 'normal').

% Found in: PCY
card_name('windscouter', 'Windscouter').
card_type('windscouter', 'Creature — Human Scout').
card_types('windscouter', ['Creature']).
card_subtypes('windscouter', ['Human', 'Scout']).
card_colors('windscouter', ['U']).
card_text('windscouter', 'Flying\nWhen Windscouter attacks or blocks, return it to its owner\'s hand at end of combat. (Return it only if it\'s on the battlefield.)').
card_mana_cost('windscouter', ['3', 'U']).
card_cmc('windscouter', 4).
card_layout('windscouter', 'normal').
card_power('windscouter', 3).
card_toughness('windscouter', 3).

% Found in: pMEI
card_name('windseeker centaur', 'Windseeker Centaur').
card_type('windseeker centaur', 'Creature — Centaur').
card_types('windseeker centaur', ['Creature']).
card_subtypes('windseeker centaur', ['Centaur']).
card_colors('windseeker centaur', ['R']).
card_text('windseeker centaur', 'Vigilance').
card_mana_cost('windseeker centaur', ['1', 'R', 'R']).
card_cmc('windseeker centaur', 3).
card_layout('windseeker centaur', 'normal').
card_power('windseeker centaur', 2).
card_toughness('windseeker centaur', 2).

% Found in: DD3_GVL, DDD, KTK, M10, M14
card_name('windstorm', 'Windstorm').
card_type('windstorm', 'Instant').
card_types('windstorm', ['Instant']).
card_subtypes('windstorm', []).
card_colors('windstorm', ['G']).
card_text('windstorm', 'Windstorm deals X damage to each creature with flying.').
card_mana_cost('windstorm', ['X', 'G']).
card_cmc('windstorm', 1).
card_layout('windstorm', 'normal').

% Found in: EXP, KTK, ONS, pJGP
card_name('windswept heath', 'Windswept Heath').
card_type('windswept heath', 'Land').
card_types('windswept heath', ['Land']).
card_subtypes('windswept heath', []).
card_colors('windswept heath', []).
card_text('windswept heath', '{T}, Pay 1 life, Sacrifice Windswept Heath: Search your library for a Forest or Plains card and put it onto the battlefield. Then shuffle your library.').
card_layout('windswept heath', 'normal').

% Found in: ALA
card_name('windwright mage', 'Windwright Mage').
card_type('windwright mage', 'Artifact Creature — Human Wizard').
card_types('windwright mage', ['Artifact', 'Creature']).
card_subtypes('windwright mage', ['Human', 'Wizard']).
card_colors('windwright mage', ['W', 'U', 'B']).
card_text('windwright mage', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nWindwright Mage has flying as long as an artifact card is in your graveyard.').
card_mana_cost('windwright mage', ['W', 'U', 'B']).
card_cmc('windwright mage', 3).
card_layout('windwright mage', 'normal').
card_power('windwright mage', 2).
card_toughness('windwright mage', 2).

% Found in: SOK
card_name('wine of blood and iron', 'Wine of Blood and Iron').
card_type('wine of blood and iron', 'Artifact').
card_types('wine of blood and iron', ['Artifact']).
card_subtypes('wine of blood and iron', []).
card_colors('wine of blood and iron', []).
card_text('wine of blood and iron', '{4}: Target creature gets +X/+0 until end of turn, where X is its power. Sacrifice Wine of Blood and Iron at the beginning of the next end step.').
card_mana_cost('wine of blood and iron', ['3']).
card_cmc('wine of blood and iron', 3).
card_layout('wine of blood and iron', 'normal').

% Found in: SOM
card_name('wing puncture', 'Wing Puncture').
card_type('wing puncture', 'Instant').
card_types('wing puncture', ['Instant']).
card_subtypes('wing puncture', []).
card_colors('wing puncture', ['G']).
card_text('wing puncture', 'Target creature you control deals damage equal to its power to target creature with flying.').
card_mana_cost('wing puncture', ['G']).
card_cmc('wing puncture', 1).
card_layout('wing puncture', 'normal').

% Found in: C14, SCG, pFNM
card_name('wing shards', 'Wing Shards').
card_type('wing shards', 'Instant').
card_types('wing shards', ['Instant']).
card_subtypes('wing shards', []).
card_colors('wing shards', ['W']).
card_text('wing shards', 'Target player sacrifices an attacking creature.\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_mana_cost('wing shards', ['1', 'W', 'W']).
card_cmc('wing shards', 3).
card_layout('wing shards', 'normal').

% Found in: 7ED, 8ED, ULG
card_name('wing snare', 'Wing Snare').
card_type('wing snare', 'Sorcery').
card_types('wing snare', ['Sorcery']).
card_subtypes('wing snare', []).
card_colors('wing snare', ['G']).
card_text('wing snare', 'Destroy target creature with flying.').
card_mana_cost('wing snare', ['2', 'G']).
card_cmc('wing snare', 3).
card_layout('wing snare', 'normal').

% Found in: NPH
card_name('wing splicer', 'Wing Splicer').
card_type('wing splicer', 'Creature — Human Artificer').
card_types('wing splicer', ['Creature']).
card_subtypes('wing splicer', ['Human', 'Artificer']).
card_colors('wing splicer', ['U']).
card_text('wing splicer', 'When Wing Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\nGolem creatures you control have flying.').
card_mana_cost('wing splicer', ['3', 'U']).
card_cmc('wing splicer', 4).
card_layout('wing splicer', 'normal').
card_power('wing splicer', 1).
card_toughness('wing splicer', 1).

% Found in: PCY
card_name('wing storm', 'Wing Storm').
card_type('wing storm', 'Sorcery').
card_types('wing storm', ['Sorcery']).
card_subtypes('wing storm', []).
card_colors('wing storm', ['G']).
card_text('wing storm', 'Wing Storm deals damage to each player equal to twice the number of creatures with flying that player controls.').
card_mana_cost('wing storm', ['2', 'G']).
card_cmc('wing storm', 3).
card_layout('wing storm', 'normal').

% Found in: LGN
card_name('wingbeat warrior', 'Wingbeat Warrior').
card_type('wingbeat warrior', 'Creature — Bird Soldier Warrior').
card_types('wingbeat warrior', ['Creature']).
card_subtypes('wingbeat warrior', ['Bird', 'Soldier', 'Warrior']).
card_colors('wingbeat warrior', ['W']).
card_text('wingbeat warrior', 'Flying\nMorph {2}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Wingbeat Warrior is turned face up, target creature gains first strike until end of turn.').
card_mana_cost('wingbeat warrior', ['2', 'W']).
card_cmc('wingbeat warrior', 3).
card_layout('wingbeat warrior', 'normal').
card_power('wingbeat warrior', 2).
card_toughness('wingbeat warrior', 1).

% Found in: AVR
card_name('wingcrafter', 'Wingcrafter').
card_type('wingcrafter', 'Creature — Human Wizard').
card_types('wingcrafter', ['Creature']).
card_subtypes('wingcrafter', ['Human', 'Wizard']).
card_colors('wingcrafter', ['U']).
card_text('wingcrafter', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Wingcrafter is paired with another creature, both creatures have flying.').
card_mana_cost('wingcrafter', ['U']).
card_cmc('wingcrafter', 1).
card_layout('wingcrafter', 'normal').
card_power('wingcrafter', 1).
card_toughness('wingcrafter', 1).

% Found in: ARB, C13
card_name('winged coatl', 'Winged Coatl').
card_type('winged coatl', 'Creature — Snake').
card_types('winged coatl', ['Creature']).
card_subtypes('winged coatl', ['Snake']).
card_colors('winged coatl', ['U', 'G']).
card_text('winged coatl', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('winged coatl', ['1', 'G', 'U']).
card_cmc('winged coatl', 3).
card_layout('winged coatl', 'normal').
card_power('winged coatl', 1).
card_toughness('winged coatl', 1).

% Found in: H09, TMP, TPR
card_name('winged sliver', 'Winged Sliver').
card_type('winged sliver', 'Creature — Sliver').
card_types('winged sliver', ['Creature']).
card_subtypes('winged sliver', ['Sliver']).
card_colors('winged sliver', ['U']).
card_text('winged sliver', 'All Sliver creatures have flying.').
card_mana_cost('winged sliver', ['1', 'U']).
card_cmc('winged sliver', 2).
card_layout('winged sliver', 'normal').
card_power('winged sliver', 1).
card_toughness('winged sliver', 1).

% Found in: KTK
card_name('wingmate roc', 'Wingmate Roc').
card_type('wingmate roc', 'Creature — Bird').
card_types('wingmate roc', ['Creature']).
card_subtypes('wingmate roc', ['Bird']).
card_colors('wingmate roc', ['W']).
card_text('wingmate roc', 'Flying\nRaid — When Wingmate Roc enters the battlefield, if you attacked with a creature this turn, put a 3/4 white Bird creature token with flying onto the battlefield.\nWhenever Wingmate Roc attacks, you gain 1 life for each attacking creature.').
card_mana_cost('wingmate roc', ['3', 'W', 'W']).
card_cmc('wingmate roc', 5).
card_layout('wingmate roc', 'normal').
card_power('wingmate roc', 3).
card_toughness('wingmate roc', 4).

% Found in: SHM
card_name('wingrattle scarecrow', 'Wingrattle Scarecrow').
card_type('wingrattle scarecrow', 'Artifact Creature — Scarecrow').
card_types('wingrattle scarecrow', ['Artifact', 'Creature']).
card_subtypes('wingrattle scarecrow', ['Scarecrow']).
card_colors('wingrattle scarecrow', []).
card_text('wingrattle scarecrow', 'Wingrattle Scarecrow has flying as long as you control a blue creature.\nWingrattle Scarecrow has persist as long as you control a black creature. (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('wingrattle scarecrow', ['3']).
card_cmc('wingrattle scarecrow', 3).
card_layout('wingrattle scarecrow', 'normal').
card_power('wingrattle scarecrow', 2).
card_toughness('wingrattle scarecrow', 2).

% Found in: CST, ICE, ME2
card_name('wings of aesthir', 'Wings of Aesthir').
card_type('wings of aesthir', 'Enchantment — Aura').
card_types('wings of aesthir', ['Enchantment']).
card_subtypes('wings of aesthir', ['Aura']).
card_colors('wings of aesthir', ['W', 'U']).
card_text('wings of aesthir', 'Enchant creature\nEnchanted creature gets +1/+0 and has flying and first strike.').
card_mana_cost('wings of aesthir', ['W', 'U']).
card_cmc('wings of aesthir', 2).
card_layout('wings of aesthir', 'normal').

% Found in: INV
card_name('wings of hope', 'Wings of Hope').
card_type('wings of hope', 'Enchantment — Aura').
card_types('wings of hope', ['Enchantment']).
card_subtypes('wings of hope', ['Aura']).
card_colors('wings of hope', ['W', 'U']).
card_text('wings of hope', 'Enchant creature\nEnchanted creature gets +1/+3 and has flying.').
card_mana_cost('wings of hope', ['W', 'U']).
card_cmc('wings of hope', 2).
card_layout('wings of hope', 'normal').

% Found in: LRW, MM2
card_name('wings of velis vel', 'Wings of Velis Vel').
card_type('wings of velis vel', 'Tribal Instant — Shapeshifter').
card_types('wings of velis vel', ['Tribal', 'Instant']).
card_subtypes('wings of velis vel', ['Shapeshifter']).
card_colors('wings of velis vel', ['U']).
card_text('wings of velis vel', 'Changeling (This card is every creature type.)\nUntil end of turn, target creature has base power and toughness 4/4, gains all creature types, and gains flying.').
card_mana_cost('wings of velis vel', ['1', 'U']).
card_cmc('wings of velis vel', 2).
card_layout('wings of velis vel', 'normal').

% Found in: THS
card_name('wingsteed rider', 'Wingsteed Rider').
card_type('wingsteed rider', 'Creature — Human Knight').
card_types('wingsteed rider', ['Creature']).
card_subtypes('wingsteed rider', ['Human', 'Knight']).
card_colors('wingsteed rider', ['W']).
card_text('wingsteed rider', 'Flying\nHeroic — Whenever you cast a spell that targets Wingsteed Rider, put a +1/+1 counter on Wingsteed Rider.').
card_mana_cost('wingsteed rider', ['1', 'W', 'W']).
card_cmc('wingsteed rider', 3).
card_layout('wingsteed rider', 'normal').
card_power('wingsteed rider', 2).
card_toughness('wingsteed rider', 2).

% Found in: INV
card_name('winnow', 'Winnow').
card_type('winnow', 'Instant').
card_types('winnow', ['Instant']).
card_subtypes('winnow', []).
card_colors('winnow', ['W']).
card_text('winnow', 'Destroy target nonland permanent if another permanent with the same name is on the battlefield.\nDraw a card.').
card_mana_cost('winnow', ['1', 'W']).
card_cmc('winnow', 2).
card_layout('winnow', 'normal').

% Found in: MOR
card_name('winnower patrol', 'Winnower Patrol').
card_type('winnower patrol', 'Creature — Elf Warrior').
card_types('winnower patrol', ['Creature']).
card_subtypes('winnower patrol', ['Elf', 'Warrior']).
card_colors('winnower patrol', ['G']).
card_text('winnower patrol', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Winnower Patrol, you may reveal it. If you do, put a +1/+1 counter on Winnower Patrol.').
card_mana_cost('winnower patrol', ['2', 'G']).
card_cmc('winnower patrol', 3).
card_layout('winnower patrol', 'normal').
card_power('winnower patrol', 3).
card_toughness('winnower patrol', 2).

% Found in: 4ED, 5ED, ITP, LEG, MED, RQS
card_name('winter blast', 'Winter Blast').
card_type('winter blast', 'Sorcery').
card_types('winter blast', ['Sorcery']).
card_subtypes('winter blast', []).
card_colors('winter blast', ['G']).
card_text('winter blast', 'Tap X target creatures. Winter Blast deals 2 damage to each of those creatures with flying.').
card_mana_cost('winter blast', ['X', 'G']).
card_cmc('winter blast', 1).
card_layout('winter blast', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED
card_name('winter orb', 'Winter Orb').
card_type('winter orb', 'Artifact').
card_types('winter orb', ['Artifact']).
card_subtypes('winter orb', []).
card_colors('winter orb', []).
card_text('winter orb', 'Players can\'t untap more than one land during their untap steps.').
card_mana_cost('winter orb', ['2']).
card_cmc('winter orb', 2).
card_layout('winter orb', 'normal').

% Found in: HML
card_name('winter sky', 'Winter Sky').
card_type('winter sky', 'Sorcery').
card_types('winter sky', ['Sorcery']).
card_subtypes('winter sky', []).
card_colors('winter sky', ['R']).
card_text('winter sky', 'Flip a coin. If you win the flip, Winter Sky deals 1 damage to each creature and each player. If you lose the flip, each player draws a card.').
card_mana_cost('winter sky', ['R']).
card_cmc('winter sky', 1).
card_layout('winter sky', 'normal').
card_reserved('winter sky').

% Found in: ICE
card_name('winter\'s chill', 'Winter\'s Chill').
card_type('winter\'s chill', 'Instant').
card_types('winter\'s chill', ['Instant']).
card_subtypes('winter\'s chill', []).
card_colors('winter\'s chill', ['U']).
card_text('winter\'s chill', 'Cast Winter\'s Chill only during combat before blockers are declared.\nX can\'t be greater than the number of snow lands you control.\nChoose X target attacking creatures. For each of those creatures, its controller may pay {1} or {2}. If that player doesn\'t, destroy that creature at end of combat. If that player pays only {1}, prevent all combat damage that would be dealt to and dealt by that creature this combat.').
card_mana_cost('winter\'s chill', ['X', 'U']).
card_cmc('winter\'s chill', 1).
card_layout('winter\'s chill', 'normal').
card_reserved('winter\'s chill').

% Found in: POR, TMP
card_name('winter\'s grasp', 'Winter\'s Grasp').
card_type('winter\'s grasp', 'Sorcery').
card_types('winter\'s grasp', ['Sorcery']).
card_subtypes('winter\'s grasp', []).
card_colors('winter\'s grasp', ['G']).
card_text('winter\'s grasp', 'Destroy target land.').
card_mana_cost('winter\'s grasp', ['1', 'G', 'G']).
card_cmc('winter\'s grasp', 3).
card_layout('winter\'s grasp', 'normal').

% Found in: ALL, ME2
card_name('winter\'s night', 'Winter\'s Night').
card_type('winter\'s night', 'World Enchantment').
card_types('winter\'s night', ['Enchantment']).
card_subtypes('winter\'s night', []).
card_supertypes('winter\'s night', ['World']).
card_colors('winter\'s night', ['W', 'R', 'G']).
card_text('winter\'s night', 'Whenever a player taps a snow land for mana, that player adds one mana to his or her mana pool of any type that land produced. That land doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('winter\'s night', ['R', 'G', 'W']).
card_cmc('winter\'s night', 3).
card_layout('winter\'s night', 'normal').
card_reserved('winter\'s night').

% Found in: KTK
card_name('winterflame', 'Winterflame').
card_type('winterflame', 'Instant').
card_types('winterflame', ['Instant']).
card_subtypes('winterflame', []).
card_colors('winterflame', ['U', 'R']).
card_text('winterflame', 'Choose one or both —\n• Tap target creature.\n• Winterflame deals 2 damage to target creature.').
card_mana_cost('winterflame', ['1', 'U', 'R']).
card_cmc('winterflame', 3).
card_layout('winterflame', 'normal').

% Found in: PCY
card_name('wintermoon mesa', 'Wintermoon Mesa').
card_type('wintermoon mesa', 'Land').
card_types('wintermoon mesa', ['Land']).
card_subtypes('wintermoon mesa', []).
card_colors('wintermoon mesa', []).
card_text('wintermoon mesa', 'Wintermoon Mesa enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Wintermoon Mesa: Tap two target lands.').
card_layout('wintermoon mesa', 'normal').

% Found in: TSP
card_name('wipe away', 'Wipe Away').
card_type('wipe away', 'Instant').
card_types('wipe away', ['Instant']).
card_subtypes('wipe away', []).
card_colors('wipe away', ['U']).
card_text('wipe away', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nReturn target permanent to its owner\'s hand.').
card_mana_cost('wipe away', ['1', 'U', 'U']).
card_cmc('wipe away', 3).
card_layout('wipe away', 'normal').

% Found in: SCG
card_name('wipe clean', 'Wipe Clean').
card_type('wipe clean', 'Instant').
card_types('wipe clean', ['Instant']).
card_subtypes('wipe clean', []).
card_colors('wipe clean', ['W']).
card_text('wipe clean', 'Exile target enchantment.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_mana_cost('wipe clean', ['1', 'W']).
card_cmc('wipe clean', 2).
card_layout('wipe clean', 'normal').

% Found in: USG
card_name('wirecat', 'Wirecat').
card_type('wirecat', 'Artifact Creature — Cat').
card_types('wirecat', ['Artifact', 'Creature']).
card_subtypes('wirecat', ['Cat']).
card_colors('wirecat', []).
card_text('wirecat', 'Wirecat can\'t attack or block if an enchantment is on the battlefield.').
card_mana_cost('wirecat', ['4']).
card_cmc('wirecat', 4).
card_layout('wirecat', 'normal').
card_power('wirecat', 4).
card_toughness('wirecat', 3).

% Found in: DST
card_name('wirefly hive', 'Wirefly Hive').
card_type('wirefly hive', 'Artifact').
card_types('wirefly hive', ['Artifact']).
card_subtypes('wirefly hive', []).
card_colors('wirefly hive', []).
card_text('wirefly hive', '{3}, {T}: Flip a coin. If you win the flip, put a 2/2 colorless Insect artifact creature token with flying named Wirefly onto the battlefield. If you lose the flip, destroy all permanents named Wirefly.').
card_mana_cost('wirefly hive', ['3']).
card_cmc('wirefly hive', 3).
card_layout('wirefly hive', 'normal').

% Found in: LGN
card_name('wirewood channeler', 'Wirewood Channeler').
card_type('wirewood channeler', 'Creature — Elf Druid').
card_types('wirewood channeler', ['Creature']).
card_subtypes('wirewood channeler', ['Elf', 'Druid']).
card_colors('wirewood channeler', ['G']).
card_text('wirewood channeler', '{T}: Add X mana of any one color to your mana pool, where X is the number of Elves on the battlefield.').
card_mana_cost('wirewood channeler', ['3', 'G']).
card_cmc('wirewood channeler', 4).
card_layout('wirewood channeler', 'normal').
card_power('wirewood channeler', 2).
card_toughness('wirewood channeler', 2).

% Found in: ONS
card_name('wirewood elf', 'Wirewood Elf').
card_type('wirewood elf', 'Creature — Elf Druid').
card_types('wirewood elf', ['Creature']).
card_subtypes('wirewood elf', ['Elf', 'Druid']).
card_colors('wirewood elf', ['G']).
card_text('wirewood elf', '{T}: Add {G} to your mana pool.').
card_mana_cost('wirewood elf', ['1', 'G']).
card_cmc('wirewood elf', 2).
card_layout('wirewood elf', 'normal').
card_power('wirewood elf', 1).
card_toughness('wirewood elf', 2).

% Found in: SCG
card_name('wirewood guardian', 'Wirewood Guardian').
card_type('wirewood guardian', 'Creature — Elf Mutant').
card_types('wirewood guardian', ['Creature']).
card_subtypes('wirewood guardian', ['Elf', 'Mutant']).
card_colors('wirewood guardian', ['G']).
card_text('wirewood guardian', 'Forestcycling {2} ({2}, Discard this card: Search your library for a Forest card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('wirewood guardian', ['5', 'G', 'G']).
card_cmc('wirewood guardian', 7).
card_layout('wirewood guardian', 'normal').
card_power('wirewood guardian', 6).
card_toughness('wirewood guardian', 6).

% Found in: DD3_EVG, EVG, ONS
card_name('wirewood herald', 'Wirewood Herald').
card_type('wirewood herald', 'Creature — Elf').
card_types('wirewood herald', ['Creature']).
card_subtypes('wirewood herald', ['Elf']).
card_colors('wirewood herald', ['G']).
card_text('wirewood herald', 'When Wirewood Herald dies, you may search your library for an Elf card, reveal that card, put it into your hand, then shuffle your library.').
card_mana_cost('wirewood herald', ['1', 'G']).
card_cmc('wirewood herald', 2).
card_layout('wirewood herald', 'normal').
card_power('wirewood herald', 1).
card_toughness('wirewood herald', 1).

% Found in: LGN
card_name('wirewood hivemaster', 'Wirewood Hivemaster').
card_type('wirewood hivemaster', 'Creature — Elf').
card_types('wirewood hivemaster', ['Creature']).
card_subtypes('wirewood hivemaster', ['Elf']).
card_colors('wirewood hivemaster', ['G']).
card_text('wirewood hivemaster', 'Whenever another nontoken Elf enters the battlefield, you may put a 1/1 green Insect creature token onto the battlefield.').
card_mana_cost('wirewood hivemaster', ['1', 'G']).
card_cmc('wirewood hivemaster', 2).
card_layout('wirewood hivemaster', 'normal').
card_power('wirewood hivemaster', 1).
card_toughness('wirewood hivemaster', 1).

% Found in: DD3_EVG, EVG, ONS
card_name('wirewood lodge', 'Wirewood Lodge').
card_type('wirewood lodge', 'Land').
card_types('wirewood lodge', ['Land']).
card_subtypes('wirewood lodge', []).
card_colors('wirewood lodge', []).
card_text('wirewood lodge', '{T}: Add {1} to your mana pool.\n{G}, {T}: Untap target Elf.').
card_layout('wirewood lodge', 'normal').

% Found in: ONS
card_name('wirewood pride', 'Wirewood Pride').
card_type('wirewood pride', 'Instant').
card_types('wirewood pride', ['Instant']).
card_subtypes('wirewood pride', []).
card_colors('wirewood pride', ['G']).
card_text('wirewood pride', 'Target creature gets +X/+X until end of turn, where X is the number of Elves on the battlefield.').
card_mana_cost('wirewood pride', ['G']).
card_cmc('wirewood pride', 1).
card_layout('wirewood pride', 'normal').

% Found in: DD3_GVL, DDD, ONS
card_name('wirewood savage', 'Wirewood Savage').
card_type('wirewood savage', 'Creature — Elf').
card_types('wirewood savage', ['Creature']).
card_subtypes('wirewood savage', ['Elf']).
card_colors('wirewood savage', ['G']).
card_text('wirewood savage', 'Whenever a Beast enters the battlefield, you may draw a card.').
card_mana_cost('wirewood savage', ['2', 'G']).
card_cmc('wirewood savage', 3).
card_layout('wirewood savage', 'normal').
card_power('wirewood savage', 2).
card_toughness('wirewood savage', 2).

% Found in: DD3_EVG, EVG, SCG
card_name('wirewood symbiote', 'Wirewood Symbiote').
card_type('wirewood symbiote', 'Creature — Insect').
card_types('wirewood symbiote', ['Creature']).
card_subtypes('wirewood symbiote', ['Insect']).
card_colors('wirewood symbiote', ['G']).
card_text('wirewood symbiote', 'Return an Elf you control to its owner\'s hand: Untap target creature. Activate this ability only once each turn.').
card_mana_cost('wirewood symbiote', ['G']).
card_cmc('wirewood symbiote', 1).
card_layout('wirewood symbiote', 'normal').
card_power('wirewood symbiote', 1).
card_toughness('wirewood symbiote', 1).

% Found in: MMQ
card_name('wishmonger', 'Wishmonger').
card_type('wishmonger', 'Creature — Unicorn Monger').
card_types('wishmonger', ['Creature']).
card_subtypes('wishmonger', ['Unicorn', 'Monger']).
card_colors('wishmonger', ['W']).
card_text('wishmonger', '{2}: Target creature gains protection from the color of its controller\'s choice until end of turn. Any player may activate this ability.').
card_mana_cost('wishmonger', ['3', 'W']).
card_cmc('wishmonger', 4).
card_layout('wishmonger', 'normal').
card_power('wishmonger', 3).
card_toughness('wishmonger', 3).

% Found in: LRW
card_name('wispmare', 'Wispmare').
card_type('wispmare', 'Creature — Elemental').
card_types('wispmare', ['Creature']).
card_subtypes('wispmare', ['Elemental']).
card_colors('wispmare', ['W']).
card_text('wispmare', 'Flying\nWhen Wispmare enters the battlefield, destroy target enchantment.\nEvoke {W} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('wispmare', ['2', 'W']).
card_cmc('wispmare', 3).
card_layout('wispmare', 'normal').
card_power('wispmare', 1).
card_toughness('wispmare', 3).

% Found in: EVE
card_name('wistful selkie', 'Wistful Selkie').
card_type('wistful selkie', 'Creature — Merfolk Wizard').
card_types('wistful selkie', ['Creature']).
card_subtypes('wistful selkie', ['Merfolk', 'Wizard']).
card_colors('wistful selkie', ['U', 'G']).
card_text('wistful selkie', 'When Wistful Selkie enters the battlefield, draw a card.').
card_mana_cost('wistful selkie', ['G/U', 'G/U', 'G/U']).
card_cmc('wistful selkie', 3).
card_layout('wistful selkie', 'normal').
card_power('wistful selkie', 2).
card_toughness('wistful selkie', 2).

% Found in: PLC
card_name('wistful thinking', 'Wistful Thinking').
card_type('wistful thinking', 'Sorcery').
card_types('wistful thinking', ['Sorcery']).
card_subtypes('wistful thinking', []).
card_colors('wistful thinking', ['U']).
card_text('wistful thinking', 'Target player draws two cards, then discards four cards.').
card_mana_cost('wistful thinking', ['2', 'U']).
card_cmc('wistful thinking', 3).
card_layout('wistful thinking', 'normal').

% Found in: DIS, M13
card_name('wit\'s end', 'Wit\'s End').
card_type('wit\'s end', 'Sorcery').
card_types('wit\'s end', ['Sorcery']).
card_subtypes('wit\'s end', []).
card_colors('wit\'s end', ['B']).
card_text('wit\'s end', 'Target player discards his or her hand.').
card_mana_cost('wit\'s end', ['5', 'B', 'B']).
card_cmc('wit\'s end', 7).
card_layout('wit\'s end', 'normal').

% Found in: USG
card_name('witch engine', 'Witch Engine').
card_type('witch engine', 'Creature — Horror').
card_types('witch engine', ['Creature']).
card_subtypes('witch engine', ['Horror']).
card_colors('witch engine', ['B']).
card_text('witch engine', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\n{T}: Add {B}{B}{B}{B} to your mana pool. Target opponent gains control of Witch Engine. (Activate this ability only any time you could cast an instant.)').
card_mana_cost('witch engine', ['5', 'B']).
card_cmc('witch engine', 6).
card_layout('witch engine', 'normal').
card_power('witch engine', 4).
card_toughness('witch engine', 4).

% Found in: C13
card_name('witch hunt', 'Witch Hunt').
card_type('witch hunt', 'Enchantment').
card_types('witch hunt', ['Enchantment']).
card_subtypes('witch hunt', []).
card_colors('witch hunt', ['R']).
card_text('witch hunt', 'Players can\'t gain life.\nAt the beginning of your upkeep, Witch Hunt deals 4 damage to you.\nAt the beginning of your end step, target opponent chosen at random gains control of Witch Hunt.').
card_mana_cost('witch hunt', ['4', 'R']).
card_cmc('witch hunt', 5).
card_layout('witch hunt', 'normal').

% Found in: CHR, DRK, TSB
card_name('witch hunter', 'Witch Hunter').
card_type('witch hunter', 'Creature — Human Cleric').
card_types('witch hunter', ['Creature']).
card_subtypes('witch hunter', ['Human', 'Cleric']).
card_colors('witch hunter', ['W']).
card_text('witch hunter', '{T}: Witch Hunter deals 1 damage to target player.\n{1}{W}{W}, {T}: Return target creature an opponent controls to its owner\'s hand.').
card_mana_cost('witch hunter', ['2', 'W', 'W']).
card_cmc('witch hunter', 4).
card_layout('witch hunter', 'normal').
card_power('witch hunter', 1).
card_toughness('witch hunter', 1).

% Found in: M15
card_name('witch\'s familiar', 'Witch\'s Familiar').
card_type('witch\'s familiar', 'Creature — Frog').
card_types('witch\'s familiar', ['Creature']).
card_subtypes('witch\'s familiar', ['Frog']).
card_colors('witch\'s familiar', ['B']).
card_text('witch\'s familiar', '').
card_mana_cost('witch\'s familiar', ['2', 'B']).
card_cmc('witch\'s familiar', 3).
card_layout('witch\'s familiar', 'normal').
card_power('witch\'s familiar', 2).
card_toughness('witch\'s familiar', 3).

% Found in: FUT
card_name('witch\'s mist', 'Witch\'s Mist').
card_type('witch\'s mist', 'Enchantment').
card_types('witch\'s mist', ['Enchantment']).
card_subtypes('witch\'s mist', []).
card_colors('witch\'s mist', ['B']).
card_text('witch\'s mist', '{2}{B}, {T}: Destroy target creature that was dealt damage this turn.').
card_mana_cost('witch\'s mist', ['2', 'B']).
card_cmc('witch\'s mist', 3).
card_layout('witch\'s mist', 'normal').

% Found in: GPT
card_name('witch-maw nephilim', 'Witch-Maw Nephilim').
card_type('witch-maw nephilim', 'Creature — Nephilim').
card_types('witch-maw nephilim', ['Creature']).
card_subtypes('witch-maw nephilim', ['Nephilim']).
card_colors('witch-maw nephilim', ['W', 'U', 'B', 'G']).
card_text('witch-maw nephilim', 'Whenever you cast a spell, you may put two +1/+1 counters on Witch-Maw Nephilim.\nWhenever Witch-Maw Nephilim attacks, it gains trample until end of turn if its power is 10 or greater.').
card_mana_cost('witch-maw nephilim', ['G', 'W', 'U', 'B']).
card_cmc('witch-maw nephilim', 4).
card_layout('witch-maw nephilim', 'normal').
card_power('witch-maw nephilim', 1).
card_toughness('witch-maw nephilim', 1).

% Found in: ISD
card_name('witchbane orb', 'Witchbane Orb').
card_type('witchbane orb', 'Artifact').
card_types('witchbane orb', ['Artifact']).
card_subtypes('witchbane orb', []).
card_colors('witchbane orb', []).
card_text('witchbane orb', 'When Witchbane Orb enters the battlefield, destroy all Curses attached to you.\nYou have hexproof. (You can\'t be the target of spells or abilities your opponents control, including Aura spells.)').
card_mana_cost('witchbane orb', ['4']).
card_cmc('witchbane orb', 4).
card_layout('witchbane orb', 'normal').

% Found in: THS
card_name('witches\' eye', 'Witches\' Eye').
card_type('witches\' eye', 'Artifact — Equipment').
card_types('witches\' eye', ['Artifact']).
card_subtypes('witches\' eye', ['Equipment']).
card_colors('witches\' eye', []).
card_text('witches\' eye', 'Equipped creature has \"{1}, {T}: Scry 1.\" (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)\nEquip {1}').
card_mana_cost('witches\' eye', ['1']).
card_cmc('witches\' eye', 1).
card_layout('witches\' eye', 'normal').

% Found in: M14
card_name('witchstalker', 'Witchstalker').
card_type('witchstalker', 'Creature — Wolf').
card_types('witchstalker', ['Creature']).
card_subtypes('witchstalker', ['Wolf']).
card_colors('witchstalker', ['G']).
card_text('witchstalker', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever an opponent casts a blue or black spell during your turn, put a +1/+1 counter on Witchstalker.').
card_mana_cost('witchstalker', ['1', 'G', 'G']).
card_cmc('witchstalker', 3).
card_layout('witchstalker', 'normal').
card_power('witchstalker', 3).
card_toughness('witchstalker', 3).

% Found in: PCY
card_name('withdraw', 'Withdraw').
card_type('withdraw', 'Instant').
card_types('withdraw', ['Instant']).
card_subtypes('withdraw', []).
card_colors('withdraw', ['U']).
card_text('withdraw', 'Return target creature to its owner\'s hand. Then return another target creature to its owner\'s hand unless its controller pays {1}.').
card_mana_cost('withdraw', ['U', 'U']).
card_cmc('withdraw', 2).
card_layout('withdraw', 'normal').

% Found in: DKA
card_name('withengar unbound', 'Withengar Unbound').
card_type('withengar unbound', 'Legendary Creature — Demon').
card_types('withengar unbound', ['Creature']).
card_subtypes('withengar unbound', ['Demon']).
card_supertypes('withengar unbound', ['Legendary']).
card_colors('withengar unbound', ['B']).
card_text('withengar unbound', 'Flying, intimidate, trample (A creature with intimidate can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever a player loses the game, put thirteen +1/+1 counters on Withengar Unbound.').
card_layout('withengar unbound', 'double-faced').
card_power('withengar unbound', 13).
card_toughness('withengar unbound', 13).

% Found in: HOP, LGN, TSB, pFNM
card_name('withered wretch', 'Withered Wretch').
card_type('withered wretch', 'Creature — Zombie Cleric').
card_types('withered wretch', ['Creature']).
card_subtypes('withered wretch', ['Zombie', 'Cleric']).
card_colors('withered wretch', ['B']).
card_text('withered wretch', '{1}: Exile target card from a graveyard.').
card_mana_cost('withered wretch', ['B', 'B']).
card_cmc('withered wretch', 2).
card_layout('withered wretch', 'normal').
card_power('withered wretch', 2).
card_toughness('withered wretch', 2).

% Found in: MIR
card_name('withering boon', 'Withering Boon').
card_type('withering boon', 'Instant').
card_types('withering boon', ['Instant']).
card_subtypes('withering boon', []).
card_colors('withering boon', ['B']).
card_text('withering boon', 'As an additional cost to cast Withering Boon, pay 3 life.\nCounter target creature spell.').
card_mana_cost('withering boon', ['1', 'B']).
card_cmc('withering boon', 2).
card_layout('withering boon', 'normal').

% Found in: 9ED, POR
card_name('withering gaze', 'Withering Gaze').
card_type('withering gaze', 'Sorcery').
card_types('withering gaze', ['Sorcery']).
card_subtypes('withering gaze', []).
card_colors('withering gaze', ['U']).
card_text('withering gaze', 'Target opponent reveals his or her hand. You draw a card for each Forest and green card in it.').
card_mana_cost('withering gaze', ['2', 'U']).
card_cmc('withering gaze', 3).
card_layout('withering gaze', 'normal').

% Found in: ONS
card_name('withering hex', 'Withering Hex').
card_type('withering hex', 'Enchantment — Aura').
card_types('withering hex', ['Enchantment']).
card_subtypes('withering hex', ['Aura']).
card_colors('withering hex', ['B']).
card_text('withering hex', 'Enchant creature\nWhenever a player cycles a card, put a plague counter on Withering Hex.\nEnchanted creature gets -1/-1 for each plague counter on Withering Hex.').
card_mana_cost('withering hex', ['B']).
card_cmc('withering hex', 1).
card_layout('withering hex', 'normal').

% Found in: ICE, ME2
card_name('withering wisps', 'Withering Wisps').
card_type('withering wisps', 'Enchantment').
card_types('withering wisps', ['Enchantment']).
card_subtypes('withering wisps', []).
card_colors('withering wisps', ['B']).
card_text('withering wisps', 'At the beginning of the end step, if no creatures are on the battlefield, sacrifice Withering Wisps.\n{B}: Withering Wisps deals 1 damage to each creature and each player. Activate this ability no more times each turn than the number of snow Swamps you control.').
card_mana_cost('withering wisps', ['1', 'B', 'B']).
card_cmc('withering wisps', 3).
card_layout('withering wisps', 'normal').

% Found in: SHM
card_name('witherscale wurm', 'Witherscale Wurm').
card_type('witherscale wurm', 'Creature — Wurm').
card_types('witherscale wurm', ['Creature']).
card_subtypes('witherscale wurm', ['Wurm']).
card_colors('witherscale wurm', ['G']).
card_text('witherscale wurm', 'Whenever Witherscale Wurm blocks or becomes blocked by a creature, that creature gains wither until end of turn. (It deals damage to creatures in the form of -1/-1 counters.)\nWhenever Witherscale Wurm deals damage to an opponent, remove all -1/-1 counters from it.').
card_mana_cost('witherscale wurm', ['4', 'G', 'G']).
card_cmc('witherscale wurm', 6).
card_layout('witherscale wurm', 'normal').
card_power('witherscale wurm', 9).
card_toughness('witherscale wurm', 9).

% Found in: GPT
card_name('withstand', 'Withstand').
card_type('withstand', 'Instant').
card_types('withstand', ['Instant']).
card_subtypes('withstand', []).
card_colors('withstand', ['W']).
card_text('withstand', 'Prevent the next 3 damage that would be dealt to target creature or player this turn.\nDraw a card.').
card_mana_cost('withstand', ['2', 'W']).
card_cmc('withstand', 3).
card_layout('withstand', 'normal').

% Found in: SOM
card_name('withstand death', 'Withstand Death').
card_type('withstand death', 'Instant').
card_types('withstand death', ['Instant']).
card_subtypes('withstand death', []).
card_colors('withstand death', ['G']).
card_text('withstand death', 'Target creature gains indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy it. If its toughness is 0 or less, it\'s still put into its owner\'s graveyard.)').
card_mana_cost('withstand death', ['G']).
card_cmc('withstand death', 1).
card_layout('withstand death', 'normal').

% Found in: KTK
card_name('witness of the ages', 'Witness of the Ages').
card_type('witness of the ages', 'Artifact Creature — Golem').
card_types('witness of the ages', ['Artifact', 'Creature']).
card_subtypes('witness of the ages', ['Golem']).
card_colors('witness of the ages', []).
card_text('witness of the ages', 'Morph {5} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('witness of the ages', ['6']).
card_cmc('witness of the ages', 6).
card_layout('witness of the ages', 'normal').
card_power('witness of the ages', 4).
card_toughness('witness of the ages', 4).

% Found in: USG
card_name('wizard mentor', 'Wizard Mentor').
card_type('wizard mentor', 'Creature — Human Wizard').
card_types('wizard mentor', ['Creature']).
card_subtypes('wizard mentor', ['Human', 'Wizard']).
card_colors('wizard mentor', ['U']).
card_text('wizard mentor', '{T}: Return Wizard Mentor and target creature you control to their owner\'s hand.').
card_mana_cost('wizard mentor', ['2', 'U']).
card_cmc('wizard mentor', 3).
card_layout('wizard mentor', 'normal').
card_power('wizard mentor', 2).
card_toughness('wizard mentor', 2).

% Found in: HOP, MRD
card_name('wizard replica', 'Wizard Replica').
card_type('wizard replica', 'Artifact Creature — Wizard').
card_types('wizard replica', ['Artifact', 'Creature']).
card_subtypes('wizard replica', ['Wizard']).
card_colors('wizard replica', []).
card_text('wizard replica', 'Flying\n{U}, Sacrifice Wizard Replica: Counter target spell unless its controller pays {2}.').
card_mana_cost('wizard replica', ['3']).
card_cmc('wizard replica', 3).
card_layout('wizard replica', 'normal').
card_power('wizard replica', 1).
card_toughness('wizard replica', 3).

% Found in: HML
card_name('wizards\' school', 'Wizards\' School').
card_type('wizards\' school', 'Land').
card_types('wizards\' school', ['Land']).
card_subtypes('wizards\' school', []).
card_colors('wizards\' school', []).
card_text('wizards\' school', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add {U} to your mana pool.\n{2}, {T}: Add {W} or {B} to your mana pool.').
card_layout('wizards\' school', 'normal').

% Found in: LRW
card_name('wizened cenn', 'Wizened Cenn').
card_type('wizened cenn', 'Creature — Kithkin Cleric').
card_types('wizened cenn', ['Creature']).
card_subtypes('wizened cenn', ['Kithkin', 'Cleric']).
card_colors('wizened cenn', ['W']).
card_text('wizened cenn', 'Other Kithkin creatures you control get +1/+1.').
card_mana_cost('wizened cenn', ['W', 'W']).
card_cmc('wizened cenn', 2).
card_layout('wizened cenn', 'normal').
card_power('wizened cenn', 2).
card_toughness('wizened cenn', 2).

% Found in: RAV
card_name('wizened snitches', 'Wizened Snitches').
card_type('wizened snitches', 'Creature — Faerie Rogue').
card_types('wizened snitches', ['Creature']).
card_subtypes('wizened snitches', ['Faerie', 'Rogue']).
card_colors('wizened snitches', ['U']).
card_text('wizened snitches', 'Flying\nPlayers play with the top card of their libraries revealed.').
card_mana_cost('wizened snitches', ['3', 'U']).
card_cmc('wizened snitches', 4).
card_layout('wizened snitches', 'normal').
card_power('wizened snitches', 1).
card_toughness('wizened snitches', 3).

% Found in: MRD
card_name('woebearer', 'Woebearer').
card_type('woebearer', 'Creature — Zombie').
card_types('woebearer', ['Creature']).
card_subtypes('woebearer', ['Zombie']).
card_colors('woebearer', ['B']).
card_text('woebearer', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever Woebearer deals combat damage to a player, you may return target creature card from your graveyard to your hand.').
card_mana_cost('woebearer', ['4', 'B']).
card_cmc('woebearer', 5).
card_layout('woebearer', 'normal').
card_power('woebearer', 2).
card_toughness('woebearer', 3).

% Found in: RAV
card_name('woebringer demon', 'Woebringer Demon').
card_type('woebringer demon', 'Creature — Demon').
card_types('woebringer demon', ['Creature']).
card_subtypes('woebringer demon', ['Demon']).
card_colors('woebringer demon', ['B']).
card_text('woebringer demon', 'Flying\nAt the beginning of each player\'s upkeep, that player sacrifices a creature. If the player can\'t, sacrifice Woebringer Demon.').
card_mana_cost('woebringer demon', ['3', 'B', 'B']).
card_cmc('woebringer demon', 5).
card_layout('woebringer demon', 'normal').
card_power('woebringer demon', 4).
card_toughness('woebringer demon', 4).

% Found in: SHM
card_name('woeleecher', 'Woeleecher').
card_type('woeleecher', 'Creature — Elemental').
card_types('woeleecher', ['Creature']).
card_subtypes('woeleecher', ['Elemental']).
card_colors('woeleecher', ['W']).
card_text('woeleecher', '{W}, {T}: Remove a -1/-1 counter from target creature. If you do, you gain 2 life.').
card_mana_cost('woeleecher', ['5', 'W']).
card_cmc('woeleecher', 6).
card_layout('woeleecher', 'normal').
card_power('woeleecher', 3).
card_toughness('woeleecher', 5).

% Found in: RAV
card_name('wojek apothecary', 'Wojek Apothecary').
card_type('wojek apothecary', 'Creature — Human Cleric').
card_types('wojek apothecary', ['Creature']).
card_subtypes('wojek apothecary', ['Human', 'Cleric']).
card_colors('wojek apothecary', ['W']).
card_text('wojek apothecary', 'Radiance — {T}: Prevent the next 1 damage that would be dealt to target creature and each other creature that shares a color with it this turn.').
card_mana_cost('wojek apothecary', ['2', 'W', 'W']).
card_cmc('wojek apothecary', 4).
card_layout('wojek apothecary', 'normal').
card_power('wojek apothecary', 1).
card_toughness('wojek apothecary', 1).

% Found in: RAV
card_name('wojek embermage', 'Wojek Embermage').
card_type('wojek embermage', 'Creature — Human Wizard').
card_types('wojek embermage', ['Creature']).
card_subtypes('wojek embermage', ['Human', 'Wizard']).
card_colors('wojek embermage', ['R']).
card_text('wojek embermage', 'Radiance — {T}: Wojek Embermage deals 1 damage to target creature and each other creature that shares a color with it.').
card_mana_cost('wojek embermage', ['3', 'R']).
card_cmc('wojek embermage', 4).
card_layout('wojek embermage', 'normal').
card_power('wojek embermage', 1).
card_toughness('wojek embermage', 2).

% Found in: GTC
card_name('wojek halberdiers', 'Wojek Halberdiers').
card_type('wojek halberdiers', 'Creature — Human Soldier').
card_types('wojek halberdiers', ['Creature']).
card_subtypes('wojek halberdiers', ['Human', 'Soldier']).
card_colors('wojek halberdiers', ['W', 'R']).
card_text('wojek halberdiers', 'Battalion — Whenever Wojek Halberdiers and at least two other creatures attack, Wojek Halberdiers gains first strike until end of turn.').
card_mana_cost('wojek halberdiers', ['R', 'W']).
card_cmc('wojek halberdiers', 2).
card_layout('wojek halberdiers', 'normal').
card_power('wojek halberdiers', 3).
card_toughness('wojek halberdiers', 2).

% Found in: RAV
card_name('wojek siren', 'Wojek Siren').
card_type('wojek siren', 'Instant').
card_types('wojek siren', ['Instant']).
card_subtypes('wojek siren', []).
card_colors('wojek siren', ['W']).
card_text('wojek siren', 'Radiance — Target creature and each other creature that shares a color with it get +1/+1 until end of turn.').
card_mana_cost('wojek siren', ['W']).
card_cmc('wojek siren', 1).
card_layout('wojek siren', 'normal').

% Found in: ME2, PTK
card_name('wolf pack', 'Wolf Pack').
card_type('wolf pack', 'Creature — Wolf').
card_types('wolf pack', ['Creature']).
card_subtypes('wolf pack', ['Wolf']).
card_colors('wolf pack', ['G']).
card_text('wolf pack', 'You may have Wolf Pack assign its combat damage as though it weren\'t blocked.').
card_mana_cost('wolf pack', ['6', 'G', 'G']).
card_cmc('wolf pack', 8).
card_layout('wolf pack', 'normal').
card_power('wolf pack', 7).
card_toughness('wolf pack', 6).

% Found in: MOR
card_name('wolf-skull shaman', 'Wolf-Skull Shaman').
card_type('wolf-skull shaman', 'Creature — Elf Shaman').
card_types('wolf-skull shaman', ['Creature']).
card_subtypes('wolf-skull shaman', ['Elf', 'Shaman']).
card_colors('wolf-skull shaman', ['G']).
card_text('wolf-skull shaman', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Wolf-Skull Shaman, you may reveal it. If you do, put a 2/2 green Wolf creature token onto the battlefield.').
card_mana_cost('wolf-skull shaman', ['1', 'G']).
card_cmc('wolf-skull shaman', 2).
card_layout('wolf-skull shaman', 'normal').
card_power('wolf-skull shaman', 2).
card_toughness('wolf-skull shaman', 2).

% Found in: DKA
card_name('wolfbitten captive', 'Wolfbitten Captive').
card_type('wolfbitten captive', 'Creature — Human Werewolf').
card_types('wolfbitten captive', ['Creature']).
card_subtypes('wolfbitten captive', ['Human', 'Werewolf']).
card_colors('wolfbitten captive', ['G']).
card_text('wolfbitten captive', '{1}{G}: Wolfbitten Captive gets +2/+2 until end of turn. Activate this ability only once each turn.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Wolfbitten Captive.').
card_mana_cost('wolfbitten captive', ['G']).
card_cmc('wolfbitten captive', 1).
card_layout('wolfbitten captive', 'double-faced').
card_power('wolfbitten captive', 1).
card_toughness('wolfbitten captive', 1).
card_sides('wolfbitten captive', 'krallenhorde killer').

% Found in: C14, CNS, MM2, WWK
card_name('wolfbriar elemental', 'Wolfbriar Elemental').
card_type('wolfbriar elemental', 'Creature — Elemental').
card_types('wolfbriar elemental', ['Creature']).
card_subtypes('wolfbriar elemental', ['Elemental']).
card_colors('wolfbriar elemental', ['G']).
card_text('wolfbriar elemental', 'Multikicker {G} (You may pay an additional {G} any number of times as you cast this spell.)\nWhen Wolfbriar Elemental enters the battlefield, put a 2/2 green Wolf creature token onto the battlefield for each time it was kicked.').
card_mana_cost('wolfbriar elemental', ['2', 'G', 'G']).
card_cmc('wolfbriar elemental', 4).
card_layout('wolfbriar elemental', 'normal').
card_power('wolfbriar elemental', 4).
card_toughness('wolfbriar elemental', 4).

% Found in: C14
card_name('wolfcaller\'s howl', 'Wolfcaller\'s Howl').
card_type('wolfcaller\'s howl', 'Enchantment').
card_types('wolfcaller\'s howl', ['Enchantment']).
card_subtypes('wolfcaller\'s howl', []).
card_colors('wolfcaller\'s howl', ['G']).
card_text('wolfcaller\'s howl', 'At the beginning of your upkeep, put X 2/2 green Wolf creature tokens onto the battlefield, where X is the number of your opponents with four or more cards in hand.').
card_mana_cost('wolfcaller\'s howl', ['3', 'G']).
card_cmc('wolfcaller\'s howl', 4).
card_layout('wolfcaller\'s howl', 'normal').

% Found in: DKA
card_name('wolfhunter\'s quiver', 'Wolfhunter\'s Quiver').
card_type('wolfhunter\'s quiver', 'Artifact — Equipment').
card_types('wolfhunter\'s quiver', ['Artifact']).
card_subtypes('wolfhunter\'s quiver', ['Equipment']).
card_colors('wolfhunter\'s quiver', []).
card_text('wolfhunter\'s quiver', 'Equipped creature has \"{T}: This creature deals 1 damage to target creature or player\" and \"{T}: This creature deals 3 damage to target Werewolf creature.\"\nEquip {5}').
card_mana_cost('wolfhunter\'s quiver', ['1']).
card_cmc('wolfhunter\'s quiver', 1).
card_layout('wolfhunter\'s quiver', 'normal').

% Found in: AVR
card_name('wolfir avenger', 'Wolfir Avenger').
card_type('wolfir avenger', 'Creature — Wolf Warrior').
card_types('wolfir avenger', ['Creature']).
card_subtypes('wolfir avenger', ['Wolf', 'Warrior']).
card_colors('wolfir avenger', ['G']).
card_text('wolfir avenger', 'Flash (You may cast this spell any time you could cast an instant.)\n{1}{G}: Regenerate Wolfir Avenger.').
card_mana_cost('wolfir avenger', ['1', 'G', 'G']).
card_cmc('wolfir avenger', 3).
card_layout('wolfir avenger', 'normal').
card_power('wolfir avenger', 3).
card_toughness('wolfir avenger', 3).

% Found in: AVR
card_name('wolfir silverheart', 'Wolfir Silverheart').
card_type('wolfir silverheart', 'Creature — Wolf Warrior').
card_types('wolfir silverheart', ['Creature']).
card_subtypes('wolfir silverheart', ['Wolf', 'Warrior']).
card_colors('wolfir silverheart', ['G']).
card_text('wolfir silverheart', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Wolfir Silverheart is paired with another creature, each of those creatures gets +4/+4.').
card_mana_cost('wolfir silverheart', ['3', 'G', 'G']).
card_cmc('wolfir silverheart', 5).
card_layout('wolfir silverheart', 'normal').
card_power('wolfir silverheart', 4).
card_toughness('wolfir silverheart', 4).

% Found in: 5ED, LEG
card_name('wolverine pack', 'Wolverine Pack').
card_type('wolverine pack', 'Creature — Wolverine').
card_types('wolverine pack', ['Creature']).
card_subtypes('wolverine pack', ['Wolverine']).
card_colors('wolverine pack', ['G']).
card_text('wolverine pack', 'Rampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('wolverine pack', ['2', 'G', 'G']).
card_cmc('wolverine pack', 4).
card_layout('wolverine pack', 'normal').
card_power('wolverine pack', 2).
card_toughness('wolverine pack', 4).

% Found in: C13, CMD, JUD, pFNM
card_name('wonder', 'Wonder').
card_type('wonder', 'Creature — Incarnation').
card_types('wonder', ['Creature']).
card_subtypes('wonder', ['Incarnation']).
card_colors('wonder', ['U']).
card_text('wonder', 'Flying\nAs long as Wonder is in your graveyard and you control an Island, creatures you control have flying.').
card_mana_cost('wonder', ['3', 'U']).
card_cmc('wonder', 4).
card_layout('wonder', 'normal').
card_power('wonder', 2).
card_toughness('wonder', 2).

% Found in: LEG, ME4
card_name('wood elemental', 'Wood Elemental').
card_type('wood elemental', 'Creature — Elemental').
card_types('wood elemental', ['Creature']).
card_subtypes('wood elemental', ['Elemental']).
card_colors('wood elemental', ['G']).
card_text('wood elemental', 'As Wood Elemental enters the battlefield, sacrifice any number of untapped Forests.\nWood Elemental\'s power and toughness are each equal to the number of Forests sacrificed as it entered the battlefield.').
card_mana_cost('wood elemental', ['3', 'G']).
card_cmc('wood elemental', 4).
card_layout('wood elemental', 'normal').
card_power('wood elemental', '*').
card_toughness('wood elemental', '*').
card_reserved('wood elemental').

% Found in: 7ED, 8ED, 9ED, C14, DD3_EVG, EVG, EXO, POR, S99, pGTW
card_name('wood elves', 'Wood Elves').
card_type('wood elves', 'Creature — Elf Scout').
card_types('wood elves', ['Creature']).
card_subtypes('wood elves', ['Elf', 'Scout']).
card_colors('wood elves', ['G']).
card_text('wood elves', 'When Wood Elves enters the battlefield, search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('wood elves', ['2', 'G']).
card_cmc('wood elves', 3).
card_layout('wood elves', 'normal').
card_power('wood elves', 1).
card_toughness('wood elves', 1).

% Found in: CNS, TMP, TPR
card_name('wood sage', 'Wood Sage').
card_type('wood sage', 'Creature — Human Druid').
card_types('wood sage', ['Creature']).
card_subtypes('wood sage', ['Human', 'Druid']).
card_colors('wood sage', ['U', 'G']).
card_text('wood sage', '{T}: Name a creature card. Reveal the top four cards of your library and put all of them with that name into your hand. Put the rest into your graveyard.').
card_mana_cost('wood sage', ['G', 'U']).
card_cmc('wood sage', 2).
card_layout('wood sage', 'normal').
card_power('wood sage', 1).
card_toughness('wood sage', 1).

% Found in: M14
card_name('woodborn behemoth', 'Woodborn Behemoth').
card_type('woodborn behemoth', 'Creature — Elemental').
card_types('woodborn behemoth', ['Creature']).
card_subtypes('woodborn behemoth', ['Elemental']).
card_colors('woodborn behemoth', ['G']).
card_text('woodborn behemoth', 'As long as you control eight or more lands, Woodborn Behemoth gets +4/+4 and has trample. (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('woodborn behemoth', ['3', 'G', 'G']).
card_cmc('woodborn behemoth', 5).
card_layout('woodborn behemoth', 'normal').
card_power('woodborn behemoth', 4).
card_toughness('woodborn behemoth', 4).

% Found in: SCG
card_name('woodcloaker', 'Woodcloaker').
card_type('woodcloaker', 'Creature — Elf').
card_types('woodcloaker', ['Creature']).
card_subtypes('woodcloaker', ['Elf']).
card_colors('woodcloaker', ['G']).
card_text('woodcloaker', 'Morph {2}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Woodcloaker is turned face up, target creature gains trample until end of turn.').
card_mana_cost('woodcloaker', ['5', 'G']).
card_cmc('woodcloaker', 6).
card_layout('woodcloaker', 'normal').
card_power('woodcloaker', 3).
card_toughness('woodcloaker', 3).

% Found in: SHM
card_name('wooded bastion', 'Wooded Bastion').
card_type('wooded bastion', 'Land').
card_types('wooded bastion', ['Land']).
card_subtypes('wooded bastion', []).
card_colors('wooded bastion', []).
card_text('wooded bastion', '{T}: Add {1} to your mana pool.\n{G/W}, {T}: Add {G}{G}, {G}{W}, or {W}{W} to your mana pool.').
card_layout('wooded bastion', 'normal').

% Found in: EXP, KTK, ONS, pJGP
card_name('wooded foothills', 'Wooded Foothills').
card_type('wooded foothills', 'Land').
card_types('wooded foothills', ['Land']).
card_subtypes('wooded foothills', []).
card_colors('wooded foothills', []).
card_text('wooded foothills', '{T}, Pay 1 life, Sacrifice Wooded Foothills: Search your library for a Mountain or Forest card and put it onto the battlefield. Then shuffle your library.').
card_layout('wooded foothills', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB
card_name('wooden sphere', 'Wooden Sphere').
card_type('wooden sphere', 'Artifact').
card_types('wooden sphere', ['Artifact']).
card_subtypes('wooden sphere', []).
card_colors('wooden sphere', []).
card_text('wooden sphere', 'Whenever a player casts a green spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('wooden sphere', ['1']).
card_cmc('wooden sphere', 1).
card_layout('wooden sphere', 'normal').

% Found in: ISD
card_name('wooden stake', 'Wooden Stake').
card_type('wooden stake', 'Artifact — Equipment').
card_types('wooden stake', ['Artifact']).
card_subtypes('wooden stake', ['Equipment']).
card_colors('wooden stake', []).
card_text('wooden stake', 'Equipped creature gets +1/+0.\nWhenever equipped creature blocks or becomes blocked by a Vampire, destroy that creature. It can\'t be regenerated.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('wooden stake', ['2']).
card_cmc('wooden stake', 2).
card_layout('wooden stake', 'normal').

% Found in: MMA, SHM
card_name('woodfall primus', 'Woodfall Primus').
card_type('woodfall primus', 'Creature — Treefolk Shaman').
card_types('woodfall primus', ['Creature']).
card_subtypes('woodfall primus', ['Treefolk', 'Shaman']).
card_colors('woodfall primus', ['G']).
card_text('woodfall primus', 'Trample\nWhen Woodfall Primus enters the battlefield, destroy target noncreature permanent.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('woodfall primus', ['5', 'G', 'G', 'G']).
card_cmc('woodfall primus', 8).
card_layout('woodfall primus', 'normal').
card_power('woodfall primus', 6).
card_toughness('woodfall primus', 6).

% Found in: ORI
card_name('woodland bellower', 'Woodland Bellower').
card_type('woodland bellower', 'Creature — Beast').
card_types('woodland bellower', ['Creature']).
card_subtypes('woodland bellower', ['Beast']).
card_colors('woodland bellower', ['G']).
card_text('woodland bellower', 'When Woodland Bellower enters the battlefield, you may search your library for a nonlegendary green creature card with converted mana cost 3 or less, put it onto the battlefield, then shuffle your library.').
card_mana_cost('woodland bellower', ['4', 'G', 'G']).
card_cmc('woodland bellower', 6).
card_layout('woodland bellower', 'normal').
card_power('woodland bellower', 6).
card_toughness('woodland bellower', 5).

% Found in: ISD
card_name('woodland cemetery', 'Woodland Cemetery').
card_type('woodland cemetery', 'Land').
card_types('woodland cemetery', ['Land']).
card_subtypes('woodland cemetery', []).
card_colors('woodland cemetery', []).
card_text('woodland cemetery', 'Woodland Cemetery enters the battlefield tapped unless you control a Swamp or a Forest.\n{T}: Add {B} or {G} to your mana pool.').
card_layout('woodland cemetery', 'normal').

% Found in: LRW
card_name('woodland changeling', 'Woodland Changeling').
card_type('woodland changeling', 'Creature — Shapeshifter').
card_types('woodland changeling', ['Creature']).
card_subtypes('woodland changeling', ['Shapeshifter']).
card_colors('woodland changeling', ['G']).
card_text('woodland changeling', 'Changeling (This card is every creature type.)').
card_mana_cost('woodland changeling', ['1', 'G']).
card_cmc('woodland changeling', 2).
card_layout('woodland changeling', 'normal').
card_power('woodland changeling', 2).
card_toughness('woodland changeling', 2).

% Found in: ODY
card_name('woodland druid', 'Woodland Druid').
card_type('woodland druid', 'Creature — Human Druid').
card_types('woodland druid', ['Creature']).
card_subtypes('woodland druid', ['Human', 'Druid']).
card_colors('woodland druid', ['G']).
card_text('woodland druid', '').
card_mana_cost('woodland druid', ['G']).
card_cmc('woodland druid', 1).
card_layout('woodland druid', 'normal').
card_power('woodland druid', 1).
card_toughness('woodland druid', 2).

% Found in: LRW
card_name('woodland guidance', 'Woodland Guidance').
card_type('woodland guidance', 'Sorcery').
card_types('woodland guidance', ['Sorcery']).
card_subtypes('woodland guidance', []).
card_colors('woodland guidance', ['G']).
card_text('woodland guidance', 'Return target card from your graveyard to your hand. Clash with an opponent. If you win, untap all Forests you control. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)\nExile Woodland Guidance.').
card_mana_cost('woodland guidance', ['3', 'G']).
card_cmc('woodland guidance', 4).
card_layout('woodland guidance', 'normal').

% Found in: ISD
card_name('woodland sleuth', 'Woodland Sleuth').
card_type('woodland sleuth', 'Creature — Human Scout').
card_types('woodland sleuth', ['Creature']).
card_subtypes('woodland sleuth', ['Human', 'Scout']).
card_colors('woodland sleuth', ['G']).
card_text('woodland sleuth', 'Morbid — When Woodland Sleuth enters the battlefield, if a creature died this turn, return a creature card at random from your graveyard to your hand.').
card_mana_cost('woodland sleuth', ['3', 'G']).
card_cmc('woodland sleuth', 4).
card_layout('woodland sleuth', 'normal').
card_power('woodland sleuth', 2).
card_toughness('woodland sleuth', 3).

% Found in: BFZ
card_name('woodland wanderer', 'Woodland Wanderer').
card_type('woodland wanderer', 'Creature — Elemental').
card_types('woodland wanderer', ['Creature']).
card_subtypes('woodland wanderer', ['Elemental']).
card_colors('woodland wanderer', ['G']).
card_text('woodland wanderer', 'Vigilance, trample\nConverge — Woodland Wanderer enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.').
card_mana_cost('woodland wanderer', ['3', 'G']).
card_cmc('woodland wanderer', 4).
card_layout('woodland wanderer', 'normal').
card_power('woodland wanderer', 2).
card_toughness('woodland wanderer', 2).

% Found in: DGM
card_name('woodlot crawler', 'Woodlot Crawler').
card_type('woodlot crawler', 'Creature — Insect').
card_types('woodlot crawler', ['Creature']).
card_subtypes('woodlot crawler', ['Insect']).
card_colors('woodlot crawler', ['U', 'B']).
card_text('woodlot crawler', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)\nProtection from green').
card_mana_cost('woodlot crawler', ['U', 'B']).
card_cmc('woodlot crawler', 2).
card_layout('woodlot crawler', 'normal').
card_power('woodlot crawler', 2).
card_toughness('woodlot crawler', 1).

% Found in: EVE
card_name('woodlurker mimic', 'Woodlurker Mimic').
card_type('woodlurker mimic', 'Creature — Shapeshifter').
card_types('woodlurker mimic', ['Creature']).
card_subtypes('woodlurker mimic', ['Shapeshifter']).
card_colors('woodlurker mimic', ['B', 'G']).
card_text('woodlurker mimic', 'Whenever you cast a spell that\'s both black and green, Woodlurker Mimic has base power and toughness 4/5 until end of turn and gains wither until end of turn. (It deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('woodlurker mimic', ['1', 'B/G']).
card_cmc('woodlurker mimic', 2).
card_layout('woodlurker mimic', 'normal').
card_power('woodlurker mimic', 2).
card_toughness('woodlurker mimic', 1).

% Found in: NMS
card_name('woodripper', 'Woodripper').
card_type('woodripper', 'Creature — Beast').
card_types('woodripper', ['Creature']).
card_subtypes('woodripper', ['Beast']).
card_colors('woodripper', ['G']).
card_text('woodripper', 'Fading 3 (This creature enters the battlefield with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\n{1}, Remove a fade counter from Woodripper: Destroy target artifact.').
card_mana_cost('woodripper', ['3', 'G', 'G']).
card_cmc('woodripper', 5).
card_layout('woodripper', 'normal').
card_power('woodripper', 4).
card_toughness('woodripper', 6).

% Found in: CNS
card_name('woodvine elemental', 'Woodvine Elemental').
card_type('woodvine elemental', 'Creature — Elemental').
card_types('woodvine elemental', ['Creature']).
card_subtypes('woodvine elemental', ['Elemental']).
card_colors('woodvine elemental', ['W', 'G']).
card_text('woodvine elemental', 'Trample\nParley — Whenever Woodvine Elemental attacks, each player reveals the top card of his or her library. For each nonland card revealed this way, attacking creatures you control get +1/+1 until end of turn. Then each player draws a card.').
card_mana_cost('woodvine elemental', ['4', 'G', 'W']).
card_cmc('woodvine elemental', 6).
card_layout('woodvine elemental', 'normal').
card_power('woodvine elemental', 4).
card_toughness('woodvine elemental', 4).

% Found in: RAV
card_name('woodwraith corrupter', 'Woodwraith Corrupter').
card_type('woodwraith corrupter', 'Creature — Elemental Horror').
card_types('woodwraith corrupter', ['Creature']).
card_subtypes('woodwraith corrupter', ['Elemental', 'Horror']).
card_colors('woodwraith corrupter', ['B', 'G']).
card_text('woodwraith corrupter', '{1}{B}{G}, {T}: Target Forest becomes a 4/4 black and green Elemental Horror creature. It\'s still a land.').
card_mana_cost('woodwraith corrupter', ['3', 'B', 'B', 'G']).
card_cmc('woodwraith corrupter', 6).
card_layout('woodwraith corrupter', 'normal').
card_power('woodwraith corrupter', 3).
card_toughness('woodwraith corrupter', 6).

% Found in: RAV
card_name('woodwraith strangler', 'Woodwraith Strangler').
card_type('woodwraith strangler', 'Creature — Plant Zombie').
card_types('woodwraith strangler', ['Creature']).
card_subtypes('woodwraith strangler', ['Plant', 'Zombie']).
card_colors('woodwraith strangler', ['B', 'G']).
card_text('woodwraith strangler', 'Exile a creature card from your graveyard: Regenerate Woodwraith Strangler.').
card_mana_cost('woodwraith strangler', ['2', 'B', 'G']).
card_cmc('woodwraith strangler', 4).
card_layout('woodwraith strangler', 'normal').
card_power('woodwraith strangler', 2).
card_toughness('woodwraith strangler', 2).

% Found in: KTK
card_name('woolly loxodon', 'Woolly Loxodon').
card_type('woolly loxodon', 'Creature — Elephant Warrior').
card_types('woolly loxodon', ['Creature']).
card_subtypes('woolly loxodon', ['Elephant', 'Warrior']).
card_colors('woolly loxodon', ['G']).
card_text('woolly loxodon', 'Morph {5}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('woolly loxodon', ['5', 'G', 'G']).
card_cmc('woolly loxodon', 7).
card_layout('woolly loxodon', 'normal').
card_power('woolly loxodon', 6).
card_toughness('woolly loxodon', 7).

% Found in: CST, ICE, ME2
card_name('woolly mammoths', 'Woolly Mammoths').
card_type('woolly mammoths', 'Creature — Elephant').
card_types('woolly mammoths', ['Creature']).
card_subtypes('woolly mammoths', ['Elephant']).
card_colors('woolly mammoths', ['G']).
card_text('woolly mammoths', 'Woolly Mammoths has trample as long as you control a snow land.').
card_mana_cost('woolly mammoths', ['1', 'G', 'G']).
card_cmc('woolly mammoths', 3).
card_layout('woolly mammoths', 'normal').
card_power('woolly mammoths', 3).
card_toughness('woolly mammoths', 2).

% Found in: CSP
card_name('woolly razorback', 'Woolly Razorback').
card_type('woolly razorback', 'Creature — Boar Beast').
card_types('woolly razorback', ['Creature']).
card_subtypes('woolly razorback', ['Boar', 'Beast']).
card_colors('woolly razorback', ['W']).
card_text('woolly razorback', 'Woolly Razorback enters the battlefield with three ice counters on it.\nAs long as Woolly Razorback has an ice counter on it, prevent all combat damage it would deal and it has defender.\nWhenever Woolly Razorback blocks, remove an ice counter from it.').
card_mana_cost('woolly razorback', ['2', 'W', 'W']).
card_cmc('woolly razorback', 4).
card_layout('woolly razorback', 'normal').
card_power('woolly razorback', 7).
card_toughness('woolly razorback', 7).

% Found in: ATH, BTD, DKM, ICE, ME2
card_name('woolly spider', 'Woolly Spider').
card_type('woolly spider', 'Creature — Spider').
card_types('woolly spider', ['Creature']).
card_subtypes('woolly spider', ['Spider']).
card_colors('woolly spider', ['G']).
card_text('woolly spider', 'Reach (This creature can block creatures with flying.)\nWhenever Woolly Spider blocks a creature with flying, Woolly Spider gets +0/+2 until end of turn.').
card_mana_cost('woolly spider', ['1', 'G', 'G']).
card_cmc('woolly spider', 3).
card_layout('woolly spider', 'normal').
card_power('woolly spider', 2).
card_toughness('woolly spider', 3).

% Found in: ALA, DDH, pWPN
card_name('woolly thoctar', 'Woolly Thoctar').
card_type('woolly thoctar', 'Creature — Beast').
card_types('woolly thoctar', ['Creature']).
card_subtypes('woolly thoctar', ['Beast']).
card_colors('woolly thoctar', ['W', 'R', 'G']).
card_text('woolly thoctar', '').
card_mana_cost('woolly thoctar', ['R', 'G', 'W']).
card_cmc('woolly thoctar', 3).
card_layout('woolly thoctar', 'normal').
card_power('woolly thoctar', 5).
card_toughness('woolly thoctar', 4).

% Found in: 4ED, DRK
card_name('word of binding', 'Word of Binding').
card_type('word of binding', 'Sorcery').
card_types('word of binding', ['Sorcery']).
card_subtypes('word of binding', []).
card_colors('word of binding', ['B']).
card_text('word of binding', 'Tap X target creatures.').
card_mana_cost('word of binding', ['X', 'B', 'B']).
card_cmc('word of binding', 2).
card_layout('word of binding', 'normal').

% Found in: 5ED, ICE, MMQ
card_name('word of blasting', 'Word of Blasting').
card_type('word of blasting', 'Instant').
card_types('word of blasting', ['Instant']).
card_subtypes('word of blasting', []).
card_colors('word of blasting', ['R']).
card_text('word of blasting', 'Destroy target Wall. It can\'t be regenerated. Word of Blasting deals damage equal to that Wall\'s converted mana cost to the Wall\'s controller.').
card_mana_cost('word of blasting', ['1', 'R']).
card_cmc('word of blasting', 2).
card_layout('word of blasting', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, ME4
card_name('word of command', 'Word of Command').
card_type('word of command', 'Instant').
card_types('word of command', ['Instant']).
card_subtypes('word of command', []).
card_colors('word of command', ['B']).
card_text('word of command', 'Look at target opponent\'s hand and choose a card from it. You control that player until Word of Command finishes resolving. The player plays that card if able. While doing so, the player can activate mana abilities only if they\'re from lands he or she controls and only if mana they produce is spent to activate other mana abilities of lands he or she controls and/or play that card. If the chosen card is cast as a spell, you control the player while that spell is resolving.').
card_mana_cost('word of command', ['B', 'B']).
card_cmc('word of command', 2).
card_layout('word of command', 'normal').
card_reserved('word of command').

% Found in: C14, TSP
card_name('word of seizing', 'Word of Seizing').
card_type('word of seizing', 'Instant').
card_types('word of seizing', ['Instant']).
card_subtypes('word of seizing', []).
card_colors('word of seizing', ['R']).
card_text('word of seizing', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nUntap target permanent and gain control of it until end of turn. It gains haste until end of turn.').
card_mana_cost('word of seizing', ['3', 'R', 'R']).
card_cmc('word of seizing', 5).
card_layout('word of seizing', 'normal').

% Found in: ICE, MED
card_name('word of undoing', 'Word of Undoing').
card_type('word of undoing', 'Instant').
card_types('word of undoing', ['Instant']).
card_subtypes('word of undoing', []).
card_colors('word of undoing', ['U']).
card_text('word of undoing', 'Return target creature and all white Auras you own attached to it to their owners\' hands.').
card_mana_cost('word of undoing', ['U']).
card_cmc('word of undoing', 1).
card_layout('word of undoing', 'normal').

% Found in: UNH
card_name('wordmail', 'Wordmail').
card_type('wordmail', 'Enchant Creature').
card_types('wordmail', ['Enchant', 'Creature']).
card_subtypes('wordmail', []).
card_colors('wordmail', ['W']).
card_text('wordmail', 'Enchanted creature gets +1/+1 for each word in its name.').
card_mana_cost('wordmail', ['W']).
card_cmc('wordmail', 1).
card_layout('wordmail', 'normal').

% Found in: ONS
card_name('words of war', 'Words of War').
card_type('words of war', 'Enchantment').
card_types('words of war', ['Enchantment']).
card_subtypes('words of war', []).
card_colors('words of war', ['R']).
card_text('words of war', '{1}: The next time you would draw a card this turn, Words of War deals 2 damage to target creature or player instead.').
card_mana_cost('words of war', ['2', 'R']).
card_cmc('words of war', 3).
card_layout('words of war', 'normal').

% Found in: ONS
card_name('words of waste', 'Words of Waste').
card_type('words of waste', 'Enchantment').
card_types('words of waste', ['Enchantment']).
card_subtypes('words of waste', []).
card_colors('words of waste', ['B']).
card_text('words of waste', '{1}: The next time you would draw a card this turn, each opponent discards a card instead.').
card_mana_cost('words of waste', ['2', 'B']).
card_cmc('words of waste', 3).
card_layout('words of waste', 'normal').

% Found in: ONS
card_name('words of wilding', 'Words of Wilding').
card_type('words of wilding', 'Enchantment').
card_types('words of wilding', ['Enchantment']).
card_subtypes('words of wilding', []).
card_colors('words of wilding', ['G']).
card_text('words of wilding', '{1}: The next time you would draw a card this turn, put a 2/2 green Bear creature token onto the battlefield instead.').
card_mana_cost('words of wilding', ['2', 'G']).
card_cmc('words of wilding', 3).
card_layout('words of wilding', 'normal').

% Found in: ONS
card_name('words of wind', 'Words of Wind').
card_type('words of wind', 'Enchantment').
card_types('words of wind', ['Enchantment']).
card_subtypes('words of wind', []).
card_colors('words of wind', ['U']).
card_text('words of wind', '{1}: The next time you would draw a card this turn, each player returns a permanent he or she controls to its owner\'s hand instead.').
card_mana_cost('words of wind', ['2', 'U']).
card_cmc('words of wind', 3).
card_layout('words of wind', 'normal').

% Found in: ODY
card_name('words of wisdom', 'Words of Wisdom').
card_type('words of wisdom', 'Instant').
card_types('words of wisdom', ['Instant']).
card_subtypes('words of wisdom', []).
card_colors('words of wisdom', ['U']).
card_text('words of wisdom', 'You draw two cards, then each other player draws a card.').
card_mana_cost('words of wisdom', ['1', 'U']).
card_cmc('words of wisdom', 2).
card_layout('words of wisdom', 'normal').

% Found in: ONS
card_name('words of worship', 'Words of Worship').
card_type('words of worship', 'Enchantment').
card_types('words of worship', ['Enchantment']).
card_subtypes('words of worship', []).
card_colors('words of worship', ['W']).
card_text('words of worship', '{1}: The next time you would draw a card this turn, you gain 5 life instead.').
card_mana_cost('words of worship', ['2', 'W']).
card_cmc('words of worship', 3).
card_layout('words of worship', 'normal').

% Found in: EXO
card_name('workhorse', 'Workhorse').
card_type('workhorse', 'Artifact Creature — Horse').
card_types('workhorse', ['Artifact', 'Creature']).
card_subtypes('workhorse', ['Horse']).
card_colors('workhorse', []).
card_text('workhorse', 'Workhorse enters the battlefield with four +1/+1 counters on it.\nRemove a +1/+1 counter from Workhorse: Add {1} to your mana pool.').
card_mana_cost('workhorse', ['6']).
card_cmc('workhorse', 6).
card_layout('workhorse', 'normal').
card_power('workhorse', 0).
card_toughness('workhorse', 0).

% Found in: UNH
card_name('working stiff', 'Working Stiff').
card_type('working stiff', 'Creature — Mummy').
card_types('working stiff', ['Creature']).
card_subtypes('working stiff', ['Mummy']).
card_colors('working stiff', ['B']).
card_text('working stiff', 'As Working Stiff comes into play, straighten your arms.\nWhen you bend an elbow, sacrifice Working Stiff.').
card_mana_cost('working stiff', ['1', 'B']).
card_cmc('working stiff', 2).
card_layout('working stiff', 'normal').
card_power('working stiff', 2).
card_toughness('working stiff', 2).

% Found in: ROE
card_name('world at war', 'World at War').
card_type('world at war', 'Sorcery').
card_types('world at war', ['Sorcery']).
card_subtypes('world at war', []).
card_colors('world at war', ['R']).
card_text('world at war', 'After the first postcombat main phase this turn, there\'s an additional combat phase followed by an additional main phase. At the beginning of that combat, untap all creatures that attacked this turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('world at war', ['3', 'R', 'R']).
card_cmc('world at war', 5).
card_layout('world at war', 'normal').

% Found in: ZEN
card_name('world queller', 'World Queller').
card_type('world queller', 'Creature — Avatar').
card_types('world queller', ['Creature']).
card_subtypes('world queller', ['Avatar']).
card_colors('world queller', ['W']).
card_text('world queller', 'At the beginning of your upkeep, you may choose a card type. If you do, each player sacrifices a permanent of that type.').
card_mana_cost('world queller', ['3', 'W', 'W']).
card_cmc('world queller', 5).
card_layout('world queller', 'normal').
card_power('world queller', 4).
card_toughness('world queller', 4).

% Found in: UNH
card_name('world-bottling kit', 'World-Bottling Kit').
card_type('world-bottling kit', 'Artifact').
card_types('world-bottling kit', ['Artifact']).
card_subtypes('world-bottling kit', []).
card_colors('world-bottling kit', []).
card_text('world-bottling kit', '{5}, Sacrifice World-Bottling Kit: Choose a Magic set. Remove from the game all permanents with that set\'s expansion symbol except for basic lands.').
card_mana_cost('world-bottling kit', ['5']).
card_cmc('world-bottling kit', 5).
card_layout('world-bottling kit', 'normal').

% Found in: M13
card_name('worldfire', 'Worldfire').
card_type('worldfire', 'Sorcery').
card_types('worldfire', ['Sorcery']).
card_subtypes('worldfire', []).
card_colors('worldfire', ['R']).
card_text('worldfire', 'Exile all permanents. Exile all cards from all hands and graveyards. Each player\'s life total becomes 1.').
card_mana_cost('worldfire', ['6', 'R', 'R', 'R']).
card_cmc('worldfire', 9).
card_layout('worldfire', 'normal').

% Found in: JUD, VMA
card_name('worldgorger dragon', 'Worldgorger Dragon').
card_type('worldgorger dragon', 'Creature — Nightmare Dragon').
card_types('worldgorger dragon', ['Creature']).
card_subtypes('worldgorger dragon', ['Nightmare', 'Dragon']).
card_colors('worldgorger dragon', ['R']).
card_text('worldgorger dragon', 'Flying, trample\nWhen Worldgorger Dragon enters the battlefield, exile all other permanents you control.\nWhen Worldgorger Dragon leaves the battlefield, return the exiled cards to the battlefield under their owners\' control.').
card_mana_cost('worldgorger dragon', ['3', 'R', 'R', 'R']).
card_cmc('worldgorger dragon', 6).
card_layout('worldgorger dragon', 'normal').
card_power('worldgorger dragon', 7).
card_toughness('worldgorger dragon', 7).

% Found in: CON, MM2
card_name('worldheart phoenix', 'Worldheart Phoenix').
card_type('worldheart phoenix', 'Creature — Phoenix').
card_types('worldheart phoenix', ['Creature']).
card_subtypes('worldheart phoenix', ['Phoenix']).
card_colors('worldheart phoenix', ['R']).
card_text('worldheart phoenix', 'Flying\nYou may cast Worldheart Phoenix from your graveyard by paying {W}{U}{B}{R}{G} rather than paying its mana cost. If you do, it enters the battlefield with two +1/+1 counters on it.').
card_mana_cost('worldheart phoenix', ['3', 'R']).
card_cmc('worldheart phoenix', 4).
card_layout('worldheart phoenix', 'normal').
card_power('worldheart phoenix', 2).
card_toughness('worldheart phoenix', 2).

% Found in: CNS
card_name('worldknit', 'Worldknit').
card_type('worldknit', 'Conspiracy').
card_types('worldknit', ['Conspiracy']).
card_subtypes('worldknit', []).
card_colors('worldknit', []).
card_text('worldknit', '(Start the game with this conspiracy face up in the command zone.)\nAs long as every card in your card pool started the game in your library or in the command zone, lands you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_layout('worldknit', 'normal').

% Found in: CON, INV
card_name('worldly counsel', 'Worldly Counsel').
card_type('worldly counsel', 'Instant').
card_types('worldly counsel', ['Instant']).
card_subtypes('worldly counsel', []).
card_colors('worldly counsel', ['U']).
card_text('worldly counsel', 'Domain — Look at the top X cards of your library, where X is the number of basic land types among lands you control. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('worldly counsel', ['1', 'U']).
card_cmc('worldly counsel', 2).
card_layout('worldly counsel', 'normal').

% Found in: 6ED, MIR
card_name('worldly tutor', 'Worldly Tutor').
card_type('worldly tutor', 'Instant').
card_types('worldly tutor', ['Instant']).
card_subtypes('worldly tutor', []).
card_colors('worldly tutor', ['G']).
card_text('worldly tutor', 'Search your library for a creature card and reveal that card. Shuffle your library, then put the card on top of it.').
card_mana_cost('worldly tutor', ['G']).
card_cmc('worldly tutor', 1).
card_layout('worldly tutor', 'normal').

% Found in: SHM
card_name('worldpurge', 'Worldpurge').
card_type('worldpurge', 'Sorcery').
card_types('worldpurge', ['Sorcery']).
card_subtypes('worldpurge', []).
card_colors('worldpurge', ['W', 'U']).
card_text('worldpurge', 'Return all permanents to their owners\' hands. Each player chooses up to seven cards in his or her hand, then shuffles the rest into his or her library. Empty all mana pools.').
card_mana_cost('worldpurge', ['4', 'W/U', 'W/U', 'W/U', 'W/U']).
card_cmc('worldpurge', 8).
card_layout('worldpurge', 'normal').

% Found in: M12, MRD
card_name('worldslayer', 'Worldslayer').
card_type('worldslayer', 'Artifact — Equipment').
card_types('worldslayer', ['Artifact']).
card_subtypes('worldslayer', ['Equipment']).
card_colors('worldslayer', []).
card_text('worldslayer', 'Whenever equipped creature deals combat damage to a player, destroy all permanents other than Worldslayer.\nEquip {5} ({5}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('worldslayer', ['5']).
card_cmc('worldslayer', 5).
card_layout('worldslayer', 'normal').

% Found in: RTR
card_name('worldspine wurm', 'Worldspine Wurm').
card_type('worldspine wurm', 'Creature — Wurm').
card_types('worldspine wurm', ['Creature']).
card_subtypes('worldspine wurm', ['Wurm']).
card_colors('worldspine wurm', ['G']).
card_text('worldspine wurm', 'Trample\nWhen Worldspine Wurm dies, put three 5/5 green Wurm creature tokens with trample onto the battlefield.\nWhen Worldspine Wurm is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_mana_cost('worldspine wurm', ['8', 'G', 'G', 'G']).
card_cmc('worldspine wurm', 11).
card_layout('worldspine wurm', 'normal').
card_power('worldspine wurm', 15).
card_toughness('worldspine wurm', 15).

% Found in: EVE, MMA
card_name('worm harvest', 'Worm Harvest').
card_type('worm harvest', 'Sorcery').
card_types('worm harvest', ['Sorcery']).
card_subtypes('worm harvest', []).
card_colors('worm harvest', ['B', 'G']).
card_text('worm harvest', 'Put a 1/1 black and green Worm creature token onto the battlefield for each land card in your graveyard.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('worm harvest', ['2', 'B/G', 'B/G', 'B/G']).
card_cmc('worm harvest', 5).
card_layout('worm harvest', 'normal').

% Found in: JUD
card_name('wormfang behemoth', 'Wormfang Behemoth').
card_type('wormfang behemoth', 'Creature — Nightmare Fish Beast').
card_types('wormfang behemoth', ['Creature']).
card_subtypes('wormfang behemoth', ['Nightmare', 'Fish', 'Beast']).
card_colors('wormfang behemoth', ['U']).
card_text('wormfang behemoth', 'When Wormfang Behemoth enters the battlefield, exile all cards from your hand.\nWhen Wormfang Behemoth leaves the battlefield, return the exiled cards to their owner\'s hand.').
card_mana_cost('wormfang behemoth', ['3', 'U', 'U']).
card_cmc('wormfang behemoth', 5).
card_layout('wormfang behemoth', 'normal').
card_power('wormfang behemoth', 5).
card_toughness('wormfang behemoth', 5).

% Found in: JUD
card_name('wormfang crab', 'Wormfang Crab').
card_type('wormfang crab', 'Creature — Nightmare Crab').
card_types('wormfang crab', ['Creature']).
card_subtypes('wormfang crab', ['Nightmare', 'Crab']).
card_colors('wormfang crab', ['U']).
card_text('wormfang crab', 'Wormfang Crab can\'t be blocked.\nWhen Wormfang Crab enters the battlefield, an opponent chooses a permanent you control other than Wormfang Crab and exiles it.\nWhen Wormfang Crab leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('wormfang crab', ['3', 'U']).
card_cmc('wormfang crab', 4).
card_layout('wormfang crab', 'normal').
card_power('wormfang crab', 3).
card_toughness('wormfang crab', 6).

% Found in: JUD
card_name('wormfang drake', 'Wormfang Drake').
card_type('wormfang drake', 'Creature — Nightmare Drake').
card_types('wormfang drake', ['Creature']).
card_subtypes('wormfang drake', ['Nightmare', 'Drake']).
card_colors('wormfang drake', ['U']).
card_text('wormfang drake', 'Flying\nWhen Wormfang Drake enters the battlefield, sacrifice it unless you exile a creature you control other than Wormfang Drake.\nWhen Wormfang Drake leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('wormfang drake', ['2', 'U']).
card_cmc('wormfang drake', 3).
card_layout('wormfang drake', 'normal').
card_power('wormfang drake', 3).
card_toughness('wormfang drake', 4).

% Found in: JUD
card_name('wormfang manta', 'Wormfang Manta').
card_type('wormfang manta', 'Creature — Nightmare Fish Beast').
card_types('wormfang manta', ['Creature']).
card_subtypes('wormfang manta', ['Nightmare', 'Fish', 'Beast']).
card_colors('wormfang manta', ['U']).
card_text('wormfang manta', 'Flying\nWhen Wormfang Manta enters the battlefield, you skip your next turn.\nWhen Wormfang Manta leaves the battlefield, you take an extra turn after this one.').
card_mana_cost('wormfang manta', ['5', 'U', 'U']).
card_cmc('wormfang manta', 7).
card_layout('wormfang manta', 'normal').
card_power('wormfang manta', 6).
card_toughness('wormfang manta', 1).

% Found in: JUD
card_name('wormfang newt', 'Wormfang Newt').
card_type('wormfang newt', 'Creature — Nightmare Salamander Beast').
card_types('wormfang newt', ['Creature']).
card_subtypes('wormfang newt', ['Nightmare', 'Salamander', 'Beast']).
card_colors('wormfang newt', ['U']).
card_text('wormfang newt', 'When Wormfang Newt enters the battlefield, exile a land you control.\nWhen Wormfang Newt leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('wormfang newt', ['1', 'U']).
card_cmc('wormfang newt', 2).
card_layout('wormfang newt', 'normal').
card_power('wormfang newt', 2).
card_toughness('wormfang newt', 2).

% Found in: JUD
card_name('wormfang turtle', 'Wormfang Turtle').
card_type('wormfang turtle', 'Creature — Nightmare Turtle Beast').
card_types('wormfang turtle', ['Creature']).
card_subtypes('wormfang turtle', ['Nightmare', 'Turtle', 'Beast']).
card_colors('wormfang turtle', ['U']).
card_text('wormfang turtle', 'When Wormfang Turtle enters the battlefield, exile a land you control.\nWhen Wormfang Turtle leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('wormfang turtle', ['2', 'U']).
card_cmc('wormfang turtle', 3).
card_layout('wormfang turtle', 'normal').
card_power('wormfang turtle', 2).
card_toughness('wormfang turtle', 4).

% Found in: DRK
card_name('worms of the earth', 'Worms of the Earth').
card_type('worms of the earth', 'Enchantment').
card_types('worms of the earth', ['Enchantment']).
card_subtypes('worms of the earth', []).
card_colors('worms of the earth', ['B']).
card_text('worms of the earth', 'Players can\'t play lands.\nLands can\'t enter the battlefield.\nAt the beginning of each upkeep, any player may sacrifice two lands or have Worms of the Earth deal 5 damage to him or her. If a player does either, destroy Worms of the Earth.').
card_mana_cost('worms of the earth', ['2', 'B', 'B', 'B']).
card_cmc('worms of the earth', 5).
card_layout('worms of the earth', 'normal').
card_reserved('worms of the earth').

% Found in: TSP
card_name('wormwood dryad', 'Wormwood Dryad').
card_type('wormwood dryad', 'Creature — Dryad').
card_types('wormwood dryad', ['Creature']).
card_subtypes('wormwood dryad', ['Dryad']).
card_colors('wormwood dryad', ['G']).
card_text('wormwood dryad', '{G}: Wormwood Dryad gains forestwalk until end of turn and deals 1 damage to you. (It can\'t be blocked as long as defending player controls a Forest.)\n{B}: Wormwood Dryad gains swampwalk until end of turn and deals 1 damage to you. (It can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('wormwood dryad', ['2', 'G']).
card_cmc('wormwood dryad', 3).
card_layout('wormwood dryad', 'normal').
card_power('wormwood dryad', 3).
card_toughness('wormwood dryad', 1).

% Found in: DRK, ME3
card_name('wormwood treefolk', 'Wormwood Treefolk').
card_type('wormwood treefolk', 'Creature — Treefolk').
card_types('wormwood treefolk', ['Creature']).
card_subtypes('wormwood treefolk', ['Treefolk']).
card_colors('wormwood treefolk', ['G']).
card_text('wormwood treefolk', '{G}{G}: Wormwood Treefolk gains forestwalk until end of turn and deals 2 damage to you. (It can\'t be blocked as long as defending player controls a Forest.)\n{B}{B}: Wormwood Treefolk gains swampwalk until end of turn and deals 2 damage to you. (It can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('wormwood treefolk', ['3', 'G', 'G']).
card_cmc('wormwood treefolk', 5).
card_layout('wormwood treefolk', 'normal').
card_power('wormwood treefolk', 4).
card_toughness('wormwood treefolk', 4).
card_reserved('wormwood treefolk').

% Found in: C14, DDE, USG
card_name('worn powerstone', 'Worn Powerstone').
card_type('worn powerstone', 'Artifact').
card_types('worn powerstone', ['Artifact']).
card_subtypes('worn powerstone', []).
card_colors('worn powerstone', []).
card_text('worn powerstone', 'Worn Powerstone enters the battlefield tapped.\n{T}: Add {2} to your mana pool.').
card_mana_cost('worn powerstone', ['3']).
card_cmc('worn powerstone', 3).
card_layout('worn powerstone', 'normal').

% Found in: MMQ
card_name('worry beads', 'Worry Beads').
card_type('worry beads', 'Artifact').
card_types('worry beads', ['Artifact']).
card_subtypes('worry beads', []).
card_colors('worry beads', []).
card_text('worry beads', 'At the beginning of each player\'s upkeep, that player puts the top card of his or her library into his or her graveyard.').
card_mana_cost('worry beads', ['3']).
card_cmc('worry beads', 3).
card_layout('worry beads', 'normal').

% Found in: 7ED, 8ED, 9ED, USG
card_name('worship', 'Worship').
card_type('worship', 'Enchantment').
card_types('worship', ['Enchantment']).
card_subtypes('worship', []).
card_colors('worship', ['W']).
card_text('worship', 'If you control a creature, damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_mana_cost('worship', ['3', 'W']).
card_cmc('worship', 4).
card_layout('worship', 'normal').

% Found in: JOU
card_name('worst fears', 'Worst Fears').
card_type('worst fears', 'Sorcery').
card_types('worst fears', ['Sorcery']).
card_subtypes('worst fears', []).
card_colors('worst fears', ['B']).
card_text('worst fears', 'You control target player during that player\'s next turn. Exile Worst Fears. (You see all cards that player could see and make all decisions for the player.)').
card_mana_cost('worst fears', ['7', 'B']).
card_cmc('worst fears', 8).
card_layout('worst fears', 'normal').

% Found in: LRW
card_name('wort, boggart auntie', 'Wort, Boggart Auntie').
card_type('wort, boggart auntie', 'Legendary Creature — Goblin Shaman').
card_types('wort, boggart auntie', ['Creature']).
card_subtypes('wort, boggart auntie', ['Goblin', 'Shaman']).
card_supertypes('wort, boggart auntie', ['Legendary']).
card_colors('wort, boggart auntie', ['B', 'R']).
card_text('wort, boggart auntie', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nAt the beginning of your upkeep, you may return target Goblin card from your graveyard to your hand.').
card_mana_cost('wort, boggart auntie', ['2', 'B', 'R']).
card_cmc('wort, boggart auntie', 4).
card_layout('wort, boggart auntie', 'normal').
card_power('wort, boggart auntie', 3).
card_toughness('wort, boggart auntie', 3).

% Found in: SHM
card_name('wort, the raidmother', 'Wort, the Raidmother').
card_type('wort, the raidmother', 'Legendary Creature — Goblin Shaman').
card_types('wort, the raidmother', ['Creature']).
card_subtypes('wort, the raidmother', ['Goblin', 'Shaman']).
card_supertypes('wort, the raidmother', ['Legendary']).
card_colors('wort, the raidmother', ['R', 'G']).
card_text('wort, the raidmother', 'When Wort, the Raidmother enters the battlefield, put two 1/1 red and green Goblin Warrior creature tokens onto the battlefield.\nEach red or green instant or sorcery spell you cast has conspire. (As you cast the spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose new targets for the copy.)').
card_mana_cost('wort, the raidmother', ['4', 'R/G', 'R/G']).
card_cmc('wort, the raidmother', 6).
card_layout('wort, the raidmother', 'normal').
card_power('wort, the raidmother', 3).
card_toughness('wort, the raidmother', 3).

% Found in: TMP
card_name('worthy cause', 'Worthy Cause').
card_type('worthy cause', 'Instant').
card_types('worthy cause', ['Instant']).
card_subtypes('worthy cause', []).
card_colors('worthy cause', ['W']).
card_text('worthy cause', 'Buyback {2} (You may pay an additional {2} as you cast this spell. If you do, put this card into your hand as it resolves.)\nAs an additional cost to cast Worthy Cause, sacrifice a creature.\nYou gain life equal to the sacrificed creature\'s toughness.').
card_mana_cost('worthy cause', ['W']).
card_cmc('worthy cause', 1).
card_layout('worthy cause', 'normal').

% Found in: SHM
card_name('wound reflection', 'Wound Reflection').
card_type('wound reflection', 'Enchantment').
card_types('wound reflection', ['Enchantment']).
card_subtypes('wound reflection', []).
card_colors('wound reflection', ['B']).
card_text('wound reflection', 'At the beginning of each end step, each opponent loses life equal to the life he or she lost this turn. (Damage causes loss of life.)').
card_mana_cost('wound reflection', ['5', 'B']).
card_cmc('wound reflection', 6).
card_layout('wound reflection', 'normal').

% Found in: DKA
card_name('wrack with madness', 'Wrack with Madness').
card_type('wrack with madness', 'Sorcery').
card_types('wrack with madness', ['Sorcery']).
card_subtypes('wrack with madness', []).
card_colors('wrack with madness', ['R']).
card_text('wrack with madness', 'Target creature deals damage to itself equal to its power.').
card_mana_cost('wrack with madness', ['3', 'R']).
card_cmc('wrack with madness', 4).
card_layout('wrack with madness', 'normal').

% Found in: CNS, MM2, ROE
card_name('wrap in flames', 'Wrap in Flames').
card_type('wrap in flames', 'Sorcery').
card_types('wrap in flames', ['Sorcery']).
card_subtypes('wrap in flames', []).
card_colors('wrap in flames', ['R']).
card_text('wrap in flames', 'Wrap in Flames deals 1 damage to each of up to three target creatures. Those creatures can\'t block this turn.').
card_mana_cost('wrap in flames', ['3', 'R']).
card_cmc('wrap in flames', 4).
card_layout('wrap in flames', 'normal').

% Found in: CNS, FUT
card_name('wrap in vigor', 'Wrap in Vigor').
card_type('wrap in vigor', 'Instant').
card_types('wrap in vigor', ['Instant']).
card_subtypes('wrap in vigor', []).
card_colors('wrap in vigor', ['G']).
card_text('wrap in vigor', 'Regenerate each creature you control.').
card_mana_cost('wrap in vigor', ['1', 'G']).
card_cmc('wrap in vigor', 2).
card_layout('wrap in vigor', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, BRB, C13, CED, CEI, LEA, LEB, POR, V14, pMPR
card_name('wrath of god', 'Wrath of God').
card_type('wrath of god', 'Sorcery').
card_types('wrath of god', ['Sorcery']).
card_subtypes('wrath of god', []).
card_colors('wrath of god', ['W']).
card_text('wrath of god', 'Destroy all creatures. They can\'t be regenerated.').
card_mana_cost('wrath of god', ['2', 'W', 'W']).
card_cmc('wrath of god', 4).
card_layout('wrath of god', 'normal').

% Found in: 8ED, ICE
card_name('wrath of marit lage', 'Wrath of Marit Lage').
card_type('wrath of marit lage', 'Enchantment').
card_types('wrath of marit lage', ['Enchantment']).
card_subtypes('wrath of marit lage', []).
card_colors('wrath of marit lage', ['U']).
card_text('wrath of marit lage', 'When Wrath of Marit Lage enters the battlefield, tap all red creatures.\nRed creatures don\'t untap during their controllers\' untap steps.').
card_mana_cost('wrath of marit lage', ['3', 'U', 'U']).
card_cmc('wrath of marit lage', 5).
card_layout('wrath of marit lage', 'normal').

% Found in: GPT
card_name('wreak havoc', 'Wreak Havoc').
card_type('wreak havoc', 'Sorcery').
card_types('wreak havoc', ['Sorcery']).
card_subtypes('wreak havoc', []).
card_colors('wreak havoc', ['R', 'G']).
card_text('wreak havoc', 'Wreak Havoc can\'t be countered by spells or abilities.\nDestroy target artifact or land.').
card_mana_cost('wreak havoc', ['2', 'R', 'G']).
card_cmc('wreak havoc', 4).
card_layout('wreak havoc', 'normal').

% Found in: ISD
card_name('wreath of geists', 'Wreath of Geists').
card_type('wreath of geists', 'Enchantment — Aura').
card_types('wreath of geists', ['Enchantment']).
card_subtypes('wreath of geists', ['Aura']).
card_colors('wreath of geists', ['G']).
card_text('wreath of geists', 'Enchant creature\nEnchanted creature gets +X/+X, where X is the number of creature cards in your graveyard.').
card_mana_cost('wreath of geists', ['G']).
card_cmc('wreath of geists', 1).
card_layout('wreath of geists', 'normal').

% Found in: CMD, DIS, MM2
card_name('wrecking ball', 'Wrecking Ball').
card_type('wrecking ball', 'Instant').
card_types('wrecking ball', ['Instant']).
card_subtypes('wrecking ball', []).
card_colors('wrecking ball', ['B', 'R']).
card_text('wrecking ball', 'Destroy target creature or land.').
card_mana_cost('wrecking ball', ['2', 'B', 'R']).
card_cmc('wrecking ball', 4).
card_layout('wrecking ball', 'normal').

% Found in: GTC
card_name('wrecking ogre', 'Wrecking Ogre').
card_type('wrecking ogre', 'Creature — Ogre Warrior').
card_types('wrecking ogre', ['Creature']).
card_subtypes('wrecking ogre', ['Ogre', 'Warrior']).
card_colors('wrecking ogre', ['R']).
card_text('wrecking ogre', 'Double strike\nBloodrush — {3}{R}{R}, Discard Wrecking Ogre: Target attacking creature gets +3/+3 and gains double strike until end of turn.').
card_mana_cost('wrecking ogre', ['4', 'R']).
card_cmc('wrecking ogre', 5).
card_layout('wrecking ogre', 'normal').
card_power('wrecking ogre', 3).
card_toughness('wrecking ogre', 3).

% Found in: C14, LRW, pPRE
card_name('wren\'s run packmaster', 'Wren\'s Run Packmaster').
card_type('wren\'s run packmaster', 'Creature — Elf Warrior').
card_types('wren\'s run packmaster', ['Creature']).
card_subtypes('wren\'s run packmaster', ['Elf', 'Warrior']).
card_colors('wren\'s run packmaster', ['G']).
card_text('wren\'s run packmaster', 'Champion an Elf (When this creature enters the battlefield, sacrifice it unless you exile another Elf you control. When this creature leaves the battlefield, that card returns to the battlefield.)\n{2}{G}: Put a 2/2 green Wolf creature token onto the battlefield.\nWolves you control have deathtouch.').
card_mana_cost('wren\'s run packmaster', ['3', 'G']).
card_cmc('wren\'s run packmaster', 4).
card_layout('wren\'s run packmaster', 'normal').
card_power('wren\'s run packmaster', 5).
card_toughness('wren\'s run packmaster', 5).

% Found in: DD3_EVG, EVG, LRW, pFNM
card_name('wren\'s run vanquisher', 'Wren\'s Run Vanquisher').
card_type('wren\'s run vanquisher', 'Creature — Elf Warrior').
card_types('wren\'s run vanquisher', ['Creature']).
card_subtypes('wren\'s run vanquisher', ['Elf', 'Warrior']).
card_colors('wren\'s run vanquisher', ['G']).
card_text('wren\'s run vanquisher', 'As an additional cost to cast Wren\'s Run Vanquisher, reveal an Elf card from your hand or pay {3}.\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('wren\'s run vanquisher', ['1', 'G']).
card_cmc('wren\'s run vanquisher', 2).
card_layout('wren\'s run vanquisher', 'normal').
card_power('wren\'s run vanquisher', 3).
card_toughness('wren\'s run vanquisher', 3).

% Found in: MRD
card_name('wrench mind', 'Wrench Mind').
card_type('wrench mind', 'Sorcery').
card_types('wrench mind', ['Sorcery']).
card_subtypes('wrench mind', []).
card_colors('wrench mind', ['B']).
card_text('wrench mind', 'Target player discards two cards unless he or she discards an artifact card.').
card_mana_cost('wrench mind', ['B', 'B']).
card_cmc('wrench mind', 2).
card_layout('wrench mind', 'normal').

% Found in: ONS
card_name('wretched anurid', 'Wretched Anurid').
card_type('wretched anurid', 'Creature — Zombie Frog Beast').
card_types('wretched anurid', ['Creature']).
card_subtypes('wretched anurid', ['Zombie', 'Frog', 'Beast']).
card_colors('wretched anurid', ['B']).
card_text('wretched anurid', 'Whenever another creature enters the battlefield, you lose 1 life.').
card_mana_cost('wretched anurid', ['1', 'B']).
card_cmc('wretched anurid', 2).
card_layout('wretched anurid', 'normal').
card_power('wretched anurid', 3).
card_toughness('wretched anurid', 3).

% Found in: CON
card_name('wretched banquet', 'Wretched Banquet').
card_type('wretched banquet', 'Sorcery').
card_types('wretched banquet', ['Sorcery']).
card_subtypes('wretched banquet', []).
card_colors('wretched banquet', ['B']).
card_text('wretched banquet', 'Destroy target creature if it has the least power or is tied for least power among creatures on the battlefield.').
card_mana_cost('wretched banquet', ['B']).
card_cmc('wretched banquet', 1).
card_layout('wretched banquet', 'normal').

% Found in: CMD, WWK
card_name('wrexial, the risen deep', 'Wrexial, the Risen Deep').
card_type('wrexial, the risen deep', 'Legendary Creature — Kraken').
card_types('wrexial, the risen deep', ['Creature']).
card_subtypes('wrexial, the risen deep', ['Kraken']).
card_supertypes('wrexial, the risen deep', ['Legendary']).
card_colors('wrexial, the risen deep', ['U', 'B']).
card_text('wrexial, the risen deep', 'Islandwalk, swampwalk (This creature can\'t be blocked as long as defending player controls an Island or a Swamp.)\nWhenever Wrexial, the Risen Deep deals combat damage to a player, you may cast target instant or sorcery card from that player\'s graveyard without paying its mana cost. If that card would be put into a graveyard this turn, exile it instead.').
card_mana_cost('wrexial, the risen deep', ['3', 'U', 'U', 'B']).
card_cmc('wrexial, the risen deep', 6).
card_layout('wrexial, the risen deep', 'normal').
card_power('wrexial, the risen deep', 5).
card_toughness('wrexial, the risen deep', 8).

% Found in: M12, M14
card_name('wring flesh', 'Wring Flesh').
card_type('wring flesh', 'Instant').
card_types('wring flesh', ['Instant']).
card_subtypes('wring flesh', []).
card_colors('wring flesh', ['B']).
card_text('wring flesh', 'Target creature gets -3/-1 until end of turn.').
card_mana_cost('wring flesh', ['B']).
card_cmc('wring flesh', 1).
card_layout('wring flesh', 'normal').

% Found in: DIS
card_name('writ of passage', 'Writ of Passage').
card_type('writ of passage', 'Enchantment — Aura').
card_types('writ of passage', ['Enchantment']).
card_subtypes('writ of passage', ['Aura']).
card_colors('writ of passage', ['U']).
card_text('writ of passage', 'Enchant creature\nWhenever enchanted creature attacks, if its power is 2 or less, it can\'t be blocked this turn.\nForecast — {1}{U}, Reveal Writ of Passage from your hand: Target creature with power 2 or less can\'t be blocked this turn. (Activate this ability only during your upkeep and only once each turn.)').
card_mana_cost('writ of passage', ['U']).
card_cmc('writ of passage', 1).
card_layout('writ of passage', 'normal').

% Found in: FRF, FRF_UGIN
card_name('write into being', 'Write into Being').
card_type('write into being', 'Sorcery').
card_types('write into being', ['Sorcery']).
card_subtypes('write into being', []).
card_colors('write into being', ['U']).
card_text('write into being', 'Look at the top two cards of your library. Manifest one of those cards, then put the other on the top or bottom of your library. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('write into being', ['2', 'U']).
card_cmc('write into being', 3).
card_layout('write into being', 'normal').

% Found in: PTK
card_name('wu admiral', 'Wu Admiral').
card_type('wu admiral', 'Creature — Human Soldier').
card_types('wu admiral', ['Creature']).
card_subtypes('wu admiral', ['Human', 'Soldier']).
card_colors('wu admiral', ['U']).
card_text('wu admiral', 'Wu Admiral gets +1/+1 as long as an opponent controls an Island.').
card_mana_cost('wu admiral', ['4', 'U']).
card_cmc('wu admiral', 5).
card_layout('wu admiral', 'normal').
card_power('wu admiral', 3).
card_toughness('wu admiral', 3).

% Found in: ME3, PTK
card_name('wu elite cavalry', 'Wu Elite Cavalry').
card_type('wu elite cavalry', 'Creature — Human Soldier').
card_types('wu elite cavalry', ['Creature']).
card_subtypes('wu elite cavalry', ['Human', 'Soldier']).
card_colors('wu elite cavalry', ['U']).
card_text('wu elite cavalry', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('wu elite cavalry', ['3', 'U']).
card_cmc('wu elite cavalry', 4).
card_layout('wu elite cavalry', 'normal').
card_power('wu elite cavalry', 2).
card_toughness('wu elite cavalry', 3).

% Found in: PTK
card_name('wu infantry', 'Wu Infantry').
card_type('wu infantry', 'Creature — Human Soldier').
card_types('wu infantry', ['Creature']).
card_subtypes('wu infantry', ['Human', 'Soldier']).
card_colors('wu infantry', ['U']).
card_text('wu infantry', '').
card_mana_cost('wu infantry', ['1', 'U']).
card_cmc('wu infantry', 2).
card_layout('wu infantry', 'normal').
card_power('wu infantry', 2).
card_toughness('wu infantry', 1).

% Found in: PTK
card_name('wu light cavalry', 'Wu Light Cavalry').
card_type('wu light cavalry', 'Creature — Human Soldier').
card_types('wu light cavalry', ['Creature']).
card_subtypes('wu light cavalry', ['Human', 'Soldier']).
card_colors('wu light cavalry', ['U']).
card_text('wu light cavalry', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('wu light cavalry', ['1', 'U']).
card_cmc('wu light cavalry', 2).
card_layout('wu light cavalry', 'normal').
card_power('wu light cavalry', 1).
card_toughness('wu light cavalry', 2).

% Found in: ME3, PTK
card_name('wu longbowman', 'Wu Longbowman').
card_type('wu longbowman', 'Creature — Human Soldier Archer').
card_types('wu longbowman', ['Creature']).
card_subtypes('wu longbowman', ['Human', 'Soldier', 'Archer']).
card_colors('wu longbowman', ['U']).
card_text('wu longbowman', '{T}: Wu Longbowman deals 1 damage to target creature or player. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('wu longbowman', ['2', 'U']).
card_cmc('wu longbowman', 3).
card_layout('wu longbowman', 'normal').
card_power('wu longbowman', 1).
card_toughness('wu longbowman', 1).

% Found in: PTK
card_name('wu scout', 'Wu Scout').
card_type('wu scout', 'Creature — Human Soldier Scout').
card_types('wu scout', ['Creature']).
card_subtypes('wu scout', ['Human', 'Soldier', 'Scout']).
card_colors('wu scout', ['U']).
card_text('wu scout', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhen Wu Scout enters the battlefield, look at target opponent\'s hand.').
card_mana_cost('wu scout', ['1', 'U']).
card_cmc('wu scout', 2).
card_layout('wu scout', 'normal').
card_power('wu scout', 1).
card_toughness('wu scout', 1).

% Found in: PTK
card_name('wu spy', 'Wu Spy').
card_type('wu spy', 'Creature — Human Soldier Rogue').
card_types('wu spy', ['Creature']).
card_subtypes('wu spy', ['Human', 'Soldier', 'Rogue']).
card_colors('wu spy', ['U']).
card_text('wu spy', 'When Wu Spy enters the battlefield, look at the top two cards of target player\'s library. Put one of them into his or her graveyard.').
card_mana_cost('wu spy', ['1', 'U']).
card_cmc('wu spy', 2).
card_layout('wu spy', 'normal').
card_power('wu spy', 1).
card_toughness('wu spy', 1).

% Found in: ME3, PTK
card_name('wu warship', 'Wu Warship').
card_type('wu warship', 'Creature — Human Soldier').
card_types('wu warship', ['Creature']).
card_subtypes('wu warship', ['Human', 'Soldier']).
card_colors('wu warship', ['U']).
card_text('wu warship', 'Wu Warship can\'t attack unless defending player controls an Island.').
card_mana_cost('wu warship', ['2', 'U']).
card_cmc('wu warship', 3).
card_layout('wu warship', 'normal').
card_power('wu warship', 3).
card_toughness('wu warship', 3).

% Found in: 10E, 9ED, DPA, DST, M10, M11, M12
card_name('wurm\'s tooth', 'Wurm\'s Tooth').
card_type('wurm\'s tooth', 'Artifact').
card_types('wurm\'s tooth', ['Artifact']).
card_subtypes('wurm\'s tooth', []).
card_colors('wurm\'s tooth', []).
card_text('wurm\'s tooth', 'Whenever a player casts a green spell, you may gain 1 life.').
card_mana_cost('wurm\'s tooth', ['2']).
card_cmc('wurm\'s tooth', 2).
card_layout('wurm\'s tooth', 'normal').

% Found in: TSP
card_name('wurmcalling', 'Wurmcalling').
card_type('wurmcalling', 'Sorcery').
card_types('wurmcalling', ['Sorcery']).
card_subtypes('wurmcalling', []).
card_colors('wurmcalling', ['G']).
card_text('wurmcalling', 'Buyback {2}{G} (You may pay an additional {2}{G} as you cast this spell. If you do, put this card into your hand as it resolves.)\nPut an X/X green Wurm creature token onto the battlefield.').
card_mana_cost('wurmcalling', ['X', 'G']).
card_cmc('wurmcalling', 1).
card_layout('wurmcalling', 'normal').

% Found in: C14, SOM, pPRE
card_name('wurmcoil engine', 'Wurmcoil Engine').
card_type('wurmcoil engine', 'Artifact Creature — Wurm').
card_types('wurmcoil engine', ['Artifact', 'Creature']).
card_subtypes('wurmcoil engine', ['Wurm']).
card_colors('wurmcoil engine', []).
card_text('wurmcoil engine', 'Deathtouch, lifelink\nWhen Wurmcoil Engine dies, put a 3/3 colorless Wurm artifact creature token with deathtouch and a 3/3 colorless Wurm artifact creature token with lifelink onto the battlefield.').
card_mana_cost('wurmcoil engine', ['6']).
card_cmc('wurmcoil engine', 6).
card_layout('wurmcoil engine', 'normal').
card_power('wurmcoil engine', 6).
card_toughness('wurmcoil engine', 6).

% Found in: MRD
card_name('wurmskin forger', 'Wurmskin Forger').
card_type('wurmskin forger', 'Creature — Elf Warrior').
card_types('wurmskin forger', ['Creature']).
card_subtypes('wurmskin forger', ['Elf', 'Warrior']).
card_colors('wurmskin forger', ['G']).
card_text('wurmskin forger', 'When Wurmskin Forger enters the battlefield, distribute three +1/+1 counters among one, two, or three target creatures.').
card_mana_cost('wurmskin forger', ['5', 'G', 'G']).
card_cmc('wurmskin forger', 7).
card_layout('wurmskin forger', 'normal').
card_power('wurmskin forger', 2).
card_toughness('wurmskin forger', 2).

% Found in: GPT
card_name('wurmweaver coil', 'Wurmweaver Coil').
card_type('wurmweaver coil', 'Enchantment — Aura').
card_types('wurmweaver coil', ['Enchantment']).
card_subtypes('wurmweaver coil', ['Aura']).
card_colors('wurmweaver coil', ['G']).
card_text('wurmweaver coil', 'Enchant green creature\nEnchanted creature gets +6/+6.\n{G}{G}{G}, Sacrifice Wurmweaver Coil: Put a 6/6 green Wurm creature token onto the battlefield.').
card_mana_cost('wurmweaver coil', ['4', 'G', 'G']).
card_cmc('wurmweaver coil', 6).
card_layout('wurmweaver coil', 'normal').

% Found in: LRW
card_name('wydwen, the biting gale', 'Wydwen, the Biting Gale').
card_type('wydwen, the biting gale', 'Legendary Creature — Faerie Wizard').
card_types('wydwen, the biting gale', ['Creature']).
card_subtypes('wydwen, the biting gale', ['Faerie', 'Wizard']).
card_supertypes('wydwen, the biting gale', ['Legendary']).
card_colors('wydwen, the biting gale', ['U', 'B']).
card_text('wydwen, the biting gale', 'Flash\nFlying\n{U}{B}, Pay 1 life: Return Wydwen, the Biting Gale to its owner\'s hand.').
card_mana_cost('wydwen, the biting gale', ['2', 'U', 'B']).
card_cmc('wydwen, the biting gale', 4).
card_layout('wydwen, the biting gale', 'normal').
card_power('wydwen, the biting gale', 3).
card_toughness('wydwen, the biting gale', 3).

% Found in: 5ED, 6ED, ARN, MED
card_name('wyluli wolf', 'Wyluli Wolf').
card_type('wyluli wolf', 'Creature — Wolf').
card_types('wyluli wolf', ['Creature']).
card_subtypes('wyluli wolf', ['Wolf']).
card_colors('wyluli wolf', ['G']).
card_text('wyluli wolf', '{T}: Target creature gets +1/+1 until end of turn.').
card_mana_cost('wyluli wolf', ['1', 'G']).
card_cmc('wyluli wolf', 2).
card_layout('wyluli wolf', 'normal').
card_power('wyluli wolf', 1).
card_toughness('wyluli wolf', 1).

