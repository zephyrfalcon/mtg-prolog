% Card-specific data

% Found in: PTK
card_name('ma chao, western warrior', 'Ma Chao, Western Warrior').
card_type('ma chao, western warrior', 'Legendary Creature — Human Soldier Warrior').
card_types('ma chao, western warrior', ['Creature']).
card_subtypes('ma chao, western warrior', ['Human', 'Soldier', 'Warrior']).
card_supertypes('ma chao, western warrior', ['Legendary']).
card_colors('ma chao, western warrior', ['R']).
card_text('ma chao, western warrior', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhenever Ma Chao, Western Warrior attacks alone, it can\'t be blocked this combat.').
card_mana_cost('ma chao, western warrior', ['3', 'R', 'R']).
card_cmc('ma chao, western warrior', 5).
card_layout('ma chao, western warrior', 'normal').
card_power('ma chao, western warrior', 3).
card_toughness('ma chao, western warrior', 3).

% Found in: AVR
card_name('maalfeld twins', 'Maalfeld Twins').
card_type('maalfeld twins', 'Creature — Zombie').
card_types('maalfeld twins', ['Creature']).
card_subtypes('maalfeld twins', ['Zombie']).
card_colors('maalfeld twins', ['B']).
card_text('maalfeld twins', 'When Maalfeld Twins dies, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_mana_cost('maalfeld twins', ['5', 'B']).
card_cmc('maalfeld twins', 6).
card_layout('maalfeld twins', 'normal').
card_power('maalfeld twins', 4).
card_toughness('maalfeld twins', 4).

% Found in: DIS, ORI
card_name('macabre waltz', 'Macabre Waltz').
card_type('macabre waltz', 'Sorcery').
card_types('macabre waltz', ['Sorcery']).
card_subtypes('macabre waltz', []).
card_colors('macabre waltz', ['B']).
card_text('macabre waltz', 'Return up to two target creature cards from your graveyard to your hand, then discard a card.').
card_mana_cost('macabre waltz', ['1', 'B']).
card_cmc('macabre waltz', 2).
card_layout('macabre waltz', 'normal').

% Found in: LGN
card_name('macetail hystrodon', 'Macetail Hystrodon').
card_type('macetail hystrodon', 'Creature — Beast').
card_types('macetail hystrodon', ['Creature']).
card_subtypes('macetail hystrodon', ['Beast']).
card_colors('macetail hystrodon', ['R']).
card_text('macetail hystrodon', 'First strike, haste\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_mana_cost('macetail hystrodon', ['6', 'R']).
card_cmc('macetail hystrodon', 7).
card_layout('macetail hystrodon', 'normal').
card_power('macetail hystrodon', 4).
card_toughness('macetail hystrodon', 4).

% Found in: DST
card_name('machinate', 'Machinate').
card_type('machinate', 'Instant').
card_types('machinate', ['Instant']).
card_subtypes('machinate', []).
card_colors('machinate', ['U']).
card_text('machinate', 'Look at the top X cards of your library, where X is the number of artifacts you control. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('machinate', ['1', 'U', 'U']).
card_cmc('machinate', 3).
card_layout('machinate', 'normal').

% Found in: LRW, MMA, pSUS
card_name('mad auntie', 'Mad Auntie').
card_type('mad auntie', 'Creature — Goblin Shaman').
card_types('mad auntie', ['Creature']).
card_subtypes('mad auntie', ['Goblin', 'Shaman']).
card_colors('mad auntie', ['B']).
card_text('mad auntie', 'Other Goblin creatures you control get +1/+1.\n{T}: Regenerate another target Goblin.').
card_mana_cost('mad auntie', ['2', 'B']).
card_cmc('mad auntie', 3).
card_layout('mad auntie', 'normal').
card_power('mad auntie', 2).
card_toughness('mad auntie', 2).

% Found in: ODY
card_name('mad dog', 'Mad Dog').
card_type('mad dog', 'Creature — Hound').
card_types('mad dog', ['Creature']).
card_subtypes('mad dog', ['Hound']).
card_colors('mad dog', ['R']).
card_text('mad dog', 'At the beginning of your end step, if Mad Dog didn\'t attack or come under your control this turn, sacrifice it.').
card_mana_cost('mad dog', ['1', 'R']).
card_cmc('mad dog', 2).
card_layout('mad dog', 'normal').
card_power('mad dog', 2).
card_toughness('mad dog', 2).

% Found in: AVR, DDK
card_name('mad prophet', 'Mad Prophet').
card_type('mad prophet', 'Creature — Human Shaman').
card_types('mad prophet', ['Creature']).
card_subtypes('mad prophet', ['Human', 'Shaman']).
card_colors('mad prophet', ['R']).
card_text('mad prophet', 'Haste\n{T}, Discard a card: Draw a card.').
card_mana_cost('mad prophet', ['3', 'R']).
card_cmc('mad prophet', 4).
card_layout('mad prophet', 'normal').
card_power('mad prophet', 2).
card_toughness('mad prophet', 2).

% Found in: SHM
card_name('madblind mountain', 'Madblind Mountain').
card_type('madblind mountain', 'Land — Mountain').
card_types('madblind mountain', ['Land']).
card_subtypes('madblind mountain', ['Mountain']).
card_colors('madblind mountain', []).
card_text('madblind mountain', '({T}: Add {R} to your mana pool.)\nMadblind Mountain enters the battlefield tapped.\n{R}, {T}: Shuffle your library. Activate this ability only if you control two or more red permanents.').
card_layout('madblind mountain', 'normal').

% Found in: GTC
card_name('madcap skills', 'Madcap Skills').
card_type('madcap skills', 'Enchantment — Aura').
card_types('madcap skills', ['Enchantment']).
card_subtypes('madcap skills', ['Aura']).
card_colors('madcap skills', ['R']).
card_text('madcap skills', 'Enchant creature\nEnchanted creature gets +3/+0 and has menace. (It can\'t be blocked except by two or more creatures.)').
card_mana_cost('madcap skills', ['1', 'R']).
card_cmc('madcap skills', 2).
card_layout('madcap skills', 'normal').

% Found in: TMP
card_name('maddening imp', 'Maddening Imp').
card_type('maddening imp', 'Creature — Imp').
card_types('maddening imp', ['Creature']).
card_subtypes('maddening imp', ['Imp']).
card_colors('maddening imp', ['B']).
card_text('maddening imp', 'Flying\n{T}: Non-Wall creatures the active player controls attack this turn if able. At the beginning of the next end step, destroy each of those creatures that didn\'t attack this turn. Activate this ability only during an opponent\'s turn and only before combat.').
card_mana_cost('maddening imp', ['2', 'B']).
card_cmc('maddening imp', 3).
card_layout('maddening imp', 'normal').
card_power('maddening imp', 1).
card_toughness('maddening imp', 1).

% Found in: ICE
card_name('maddening wind', 'Maddening Wind').
card_type('maddening wind', 'Enchantment — Aura').
card_types('maddening wind', ['Enchantment']).
card_subtypes('maddening wind', ['Aura']).
card_colors('maddening wind', ['G']).
card_text('maddening wind', 'Enchant creature\nCumulative upkeep {G} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of the upkeep of enchanted creature\'s controller, Maddening Wind deals 2 damage to that player.').
card_mana_cost('maddening wind', ['2', 'G']).
card_cmc('maddening wind', 3).
card_layout('maddening wind', 'normal').

% Found in: ARB
card_name('madrush cyclops', 'Madrush Cyclops').
card_type('madrush cyclops', 'Creature — Cyclops Warrior').
card_types('madrush cyclops', ['Creature']).
card_subtypes('madrush cyclops', ['Cyclops', 'Warrior']).
card_colors('madrush cyclops', ['B', 'R', 'G']).
card_text('madrush cyclops', 'Creatures you control have haste.').
card_mana_cost('madrush cyclops', ['1', 'B', 'R', 'G']).
card_cmc('madrush cyclops', 4).
card_layout('madrush cyclops', 'normal').
card_power('madrush cyclops', 3).
card_toughness('madrush cyclops', 4).

% Found in: CON
card_name('maelstrom archangel', 'Maelstrom Archangel').
card_type('maelstrom archangel', 'Creature — Angel').
card_types('maelstrom archangel', ['Creature']).
card_subtypes('maelstrom archangel', ['Angel']).
card_colors('maelstrom archangel', ['W', 'U', 'B', 'R', 'G']).
card_text('maelstrom archangel', 'Flying\nWhenever Maelstrom Archangel deals combat damage to a player, you may cast a nonland card from your hand without paying its mana cost.').
card_mana_cost('maelstrom archangel', ['W', 'U', 'B', 'R', 'G']).
card_cmc('maelstrom archangel', 5).
card_layout('maelstrom archangel', 'normal').
card_power('maelstrom archangel', 5).
card_toughness('maelstrom archangel', 5).

% Found in: VAN
card_name('maelstrom archangel avatar', 'Maelstrom Archangel Avatar').
card_type('maelstrom archangel avatar', 'Vanguard').
card_types('maelstrom archangel avatar', ['Vanguard']).
card_subtypes('maelstrom archangel avatar', []).
card_colors('maelstrom archangel avatar', []).
card_text('maelstrom archangel avatar', 'At end of combat, for each creature you controlled at the time it dealt combat damage to a player this turn, copy a random card with the same mana cost as that creature. You may pay {3}. If you do, choose one of those copies. If a copy of a permanent card is chosen, you may put a token onto the battlefield that\'s a copy of that card. If a copy of an instant or sorcery card is chosen, you may cast the copy without paying its mana cost.').
card_layout('maelstrom archangel avatar', 'vanguard').

% Found in: FUT
card_name('maelstrom djinn', 'Maelstrom Djinn').
card_type('maelstrom djinn', 'Creature — Djinn').
card_types('maelstrom djinn', ['Creature']).
card_subtypes('maelstrom djinn', ['Djinn']).
card_colors('maelstrom djinn', ['U']).
card_text('maelstrom djinn', 'Flying\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Maelstrom Djinn is turned face up, put two time counters on it and it gains vanishing. (At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)').
card_mana_cost('maelstrom djinn', ['7', 'U']).
card_cmc('maelstrom djinn', 8).
card_layout('maelstrom djinn', 'normal').
card_power('maelstrom djinn', 5).
card_toughness('maelstrom djinn', 6).

% Found in: ARB
card_name('maelstrom nexus', 'Maelstrom Nexus').
card_type('maelstrom nexus', 'Enchantment').
card_types('maelstrom nexus', ['Enchantment']).
card_subtypes('maelstrom nexus', []).
card_colors('maelstrom nexus', ['W', 'U', 'B', 'R', 'G']).
card_text('maelstrom nexus', 'The first spell you cast each turn has cascade. (When you cast your first spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_mana_cost('maelstrom nexus', ['W', 'U', 'B', 'R', 'G']).
card_cmc('maelstrom nexus', 5).
card_layout('maelstrom nexus', 'normal').

% Found in: ARB, MMA, pGPX
card_name('maelstrom pulse', 'Maelstrom Pulse').
card_type('maelstrom pulse', 'Sorcery').
card_types('maelstrom pulse', ['Sorcery']).
card_subtypes('maelstrom pulse', []).
card_colors('maelstrom pulse', ['B', 'G']).
card_text('maelstrom pulse', 'Destroy target nonland permanent and all other permanents with the same name as that permanent.').
card_mana_cost('maelstrom pulse', ['1', 'B', 'G']).
card_cmc('maelstrom pulse', 3).
card_layout('maelstrom pulse', 'normal').

% Found in: CM1, PC2
card_name('maelstrom wanderer', 'Maelstrom Wanderer').
card_type('maelstrom wanderer', 'Legendary Creature — Elemental').
card_types('maelstrom wanderer', ['Creature']).
card_subtypes('maelstrom wanderer', ['Elemental']).
card_supertypes('maelstrom wanderer', ['Legendary']).
card_colors('maelstrom wanderer', ['U', 'R', 'G']).
card_text('maelstrom wanderer', 'Creatures you control have haste.\nCascade, cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order. Then do it again.)').
card_mana_cost('maelstrom wanderer', ['5', 'U', 'R', 'G']).
card_cmc('maelstrom wanderer', 8).
card_layout('maelstrom wanderer', 'normal').
card_power('maelstrom wanderer', 7).
card_toughness('maelstrom wanderer', 5).

% Found in: SOK
card_name('maga, traitor to mortals', 'Maga, Traitor to Mortals').
card_type('maga, traitor to mortals', 'Legendary Creature — Human Wizard').
card_types('maga, traitor to mortals', ['Creature']).
card_subtypes('maga, traitor to mortals', ['Human', 'Wizard']).
card_supertypes('maga, traitor to mortals', ['Legendary']).
card_colors('maga, traitor to mortals', ['B']).
card_text('maga, traitor to mortals', 'Maga, Traitor to Mortals enters the battlefield with X +1/+1 counters on it.\nWhen Maga enters the battlefield, target player loses life equal to the number of +1/+1 counters on it.').
card_mana_cost('maga, traitor to mortals', ['X', 'B', 'B', 'B']).
card_cmc('maga, traitor to mortals', 3).
card_layout('maga, traitor to mortals', 'normal').
card_power('maga, traitor to mortals', 0).
card_toughness('maga, traitor to mortals', 0).

% Found in: EXO, TPR
card_name('mage il-vec', 'Mage il-Vec').
card_type('mage il-vec', 'Creature — Human Wizard').
card_types('mage il-vec', ['Creature']).
card_subtypes('mage il-vec', ['Human', 'Wizard']).
card_colors('mage il-vec', ['R']).
card_text('mage il-vec', '{T}, Discard a card at random: Mage il-Vec deals 1 damage to target creature or player.').
card_mana_cost('mage il-vec', ['2', 'R']).
card_cmc('mage il-vec', 3).
card_layout('mage il-vec', 'normal').
card_power('mage il-vec', 2).
card_toughness('mage il-vec', 2).

% Found in: ARB, HOP
card_name('mage slayer', 'Mage Slayer').
card_type('mage slayer', 'Artifact — Equipment').
card_types('mage slayer', ['Artifact']).
card_subtypes('mage slayer', ['Equipment']).
card_colors('mage slayer', ['R', 'G']).
card_text('mage slayer', 'Whenever equipped creature attacks, it deals damage equal to its power to defending player.\nEquip {3}').
card_mana_cost('mage slayer', ['1', 'R', 'G']).
card_cmc('mage slayer', 3).
card_layout('mage slayer', 'normal').

% Found in: ONS
card_name('mage\'s guile', 'Mage\'s Guile').
card_type('mage\'s guile', 'Instant').
card_types('mage\'s guile', ['Instant']).
card_subtypes('mage\'s guile', []).
card_colors('mage\'s guile', ['U']).
card_text('mage\'s guile', 'Target creature gains shroud until end of turn. (It can\'t be the target of spells or abilities.)\nCycling {U} ({U}, Discard this card: Draw a card.)').
card_mana_cost('mage\'s guile', ['1', 'U']).
card_cmc('mage\'s guile', 2).
card_layout('mage\'s guile', 'normal').

% Found in: ORI
card_name('mage-ring bully', 'Mage-Ring Bully').
card_type('mage-ring bully', 'Creature — Human Warrior').
card_types('mage-ring bully', ['Creature']).
card_subtypes('mage-ring bully', ['Human', 'Warrior']).
card_colors('mage-ring bully', ['R']).
card_text('mage-ring bully', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nMage-Ring Bully attacks each turn if able.').
card_mana_cost('mage-ring bully', ['1', 'R']).
card_cmc('mage-ring bully', 2).
card_layout('mage-ring bully', 'normal').
card_power('mage-ring bully', 2).
card_toughness('mage-ring bully', 2).

% Found in: ORI
card_name('mage-ring network', 'Mage-Ring Network').
card_type('mage-ring network', 'Land').
card_types('mage-ring network', ['Land']).
card_subtypes('mage-ring network', []).
card_colors('mage-ring network', []).
card_text('mage-ring network', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Mage-Ring Network.\n{T}, Remove X storage counters from Mage-Ring Network: Add {X} to your mana pool.').
card_layout('mage-ring network', 'normal').

% Found in: ORI
card_name('mage-ring responder', 'Mage-Ring Responder').
card_type('mage-ring responder', 'Artifact Creature — Golem').
card_types('mage-ring responder', ['Artifact', 'Creature']).
card_subtypes('mage-ring responder', ['Golem']).
card_colors('mage-ring responder', []).
card_text('mage-ring responder', 'Mage-Ring Responder doesn\'t untap during your untap step.\n{7}: Untap Mage-Ring Responder.\nWhenever Mage-Ring Responder attacks, it deals 7 damage to target creature defending player controls.').
card_mana_cost('mage-ring responder', ['7']).
card_cmc('mage-ring responder', 7).
card_layout('mage-ring responder', 'normal').
card_power('mage-ring responder', 7).
card_toughness('mage-ring responder', 7).

% Found in: M10
card_name('magebane armor', 'Magebane Armor').
card_type('magebane armor', 'Artifact — Equipment').
card_types('magebane armor', ['Artifact']).
card_subtypes('magebane armor', ['Equipment']).
card_colors('magebane armor', []).
card_text('magebane armor', 'Equipped creature gets +2/+4 and loses flying.\nPrevent all noncombat damage that would be dealt to equipped creature.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('magebane armor', ['3']).
card_cmc('magebane armor', 3).
card_layout('magebane armor', 'normal').

% Found in: ARB
card_name('magefire wings', 'Magefire Wings').
card_type('magefire wings', 'Enchantment — Aura').
card_types('magefire wings', ['Enchantment']).
card_subtypes('magefire wings', ['Aura']).
card_colors('magefire wings', ['U', 'R']).
card_text('magefire wings', 'Enchant creature\nEnchanted creature gets +2/+0 and has flying.').
card_mana_cost('magefire wings', ['U', 'R']).
card_cmc('magefire wings', 2).
card_layout('magefire wings', 'normal').

% Found in: INV
card_name('mages\' contest', 'Mages\' Contest').
card_type('mages\' contest', 'Instant').
card_types('mages\' contest', ['Instant']).
card_subtypes('mages\' contest', []).
card_colors('mages\' contest', ['R']).
card_text('mages\' contest', 'You and target spell\'s controller bid life. You start the bidding with a bid of 1. In turn order, each player may top the high bid. The bidding ends if the high bid stands. The high bidder loses life equal to the high bid. If you win the bidding, counter that spell.').
card_mana_cost('mages\' contest', ['1', 'R', 'R']).
card_cmc('mages\' contest', 3).
card_layout('mages\' contest', 'normal').

% Found in: PCY
card_name('mageta the lion', 'Mageta the Lion').
card_type('mageta the lion', 'Legendary Creature — Human Spellshaper').
card_types('mageta the lion', ['Creature']).
card_subtypes('mageta the lion', ['Human', 'Spellshaper']).
card_supertypes('mageta the lion', ['Legendary']).
card_colors('mageta the lion', ['W']).
card_text('mageta the lion', '{2}{W}{W}, {T}, Discard two cards: Destroy all creatures except for Mageta the Lion. Those creatures can\'t be regenerated.').
card_mana_cost('mageta the lion', ['3', 'W', 'W']).
card_cmc('mageta the lion', 5).
card_layout('mageta the lion', 'normal').
card_power('mageta the lion', 3).
card_toughness('mageta the lion', 3).

% Found in: PCY
card_name('mageta\'s boon', 'Mageta\'s Boon').
card_type('mageta\'s boon', 'Enchantment — Aura').
card_types('mageta\'s boon', ['Enchantment']).
card_subtypes('mageta\'s boon', ['Aura']).
card_colors('mageta\'s boon', ['W']).
card_text('mageta\'s boon', 'Flash\nEnchant creature\nEnchanted creature gets +1/+2.').
card_mana_cost('mageta\'s boon', ['1', 'W']).
card_cmc('mageta\'s boon', 2).
card_layout('mageta\'s boon', 'normal').

% Found in: DIS
card_name('magewright\'s stone', 'Magewright\'s Stone').
card_type('magewright\'s stone', 'Artifact').
card_types('magewright\'s stone', ['Artifact']).
card_subtypes('magewright\'s stone', []).
card_colors('magewright\'s stone', []).
card_text('magewright\'s stone', '{1}, {T}: Untap target creature that has an activated ability with {T} in its cost.').
card_mana_cost('magewright\'s stone', ['2']).
card_cmc('magewright\'s stone', 2).
card_layout('magewright\'s stone', 'normal').

% Found in: 8ED, PLS
card_name('maggot carrier', 'Maggot Carrier').
card_type('maggot carrier', 'Creature — Zombie').
card_types('maggot carrier', ['Creature']).
card_subtypes('maggot carrier', ['Zombie']).
card_colors('maggot carrier', ['B']).
card_text('maggot carrier', 'When Maggot Carrier enters the battlefield, each player loses 1 life.').
card_mana_cost('maggot carrier', ['B']).
card_cmc('maggot carrier', 1).
card_layout('maggot carrier', 'normal').
card_power('maggot carrier', 1).
card_toughness('maggot carrier', 1).

% Found in: MMQ
card_name('maggot therapy', 'Maggot Therapy').
card_type('maggot therapy', 'Enchantment — Aura').
card_types('maggot therapy', ['Enchantment']).
card_subtypes('maggot therapy', ['Aura']).
card_colors('maggot therapy', ['B']).
card_text('maggot therapy', 'Flash\nEnchant creature\nEnchanted creature gets +2/-2.').
card_mana_cost('maggot therapy', ['2', 'B']).
card_cmc('maggot therapy', 3).
card_layout('maggot therapy', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('magical hack', 'Magical Hack').
card_type('magical hack', 'Instant').
card_types('magical hack', ['Instant']).
card_subtypes('magical hack', []).
card_colors('magical hack', ['U']).
card_text('magical hack', 'Change the text of target spell or permanent by replacing all instances of one basic land type with another. (For example, you may change \"swampwalk\" to \"plainswalk.\" This effect lasts indefinitely.)').
card_mana_cost('magical hack', ['U']).
card_cmc('magical hack', 1).
card_layout('magical hack', 'normal').

% Found in: UNH
card_name('magical hacker', 'Magical Hacker').
card_type('magical hacker', 'Creature — Human Gamer').
card_types('magical hacker', ['Creature']).
card_subtypes('magical hacker', ['Human', 'Gamer']).
card_colors('magical hacker', ['U']).
card_text('magical hacker', '{U}: Change the text of target spell or permanent by replacing all instances of + with -, and vice versa, until end of turn.').
card_mana_cost('magical hacker', ['1', 'U']).
card_cmc('magical hacker', 2).
card_layout('magical hacker', 'normal').
card_power('magical hacker', 1).
card_toughness('magical hacker', 2).

% Found in: CNS, VMA, pMEI
card_name('magister of worth', 'Magister of Worth').
card_type('magister of worth', 'Creature — Angel').
card_types('magister of worth', ['Creature']).
card_subtypes('magister of worth', ['Angel']).
card_colors('magister of worth', ['W', 'B']).
card_text('magister of worth', 'Flying\nWill of the council — When Magister of Worth enters the battlefield, starting with you, each player votes for grace or condemnation. If grace gets more votes, each player returns each creature card from his or her graveyard to the battlefield. If condemnation gets more votes or the vote is tied, destroy all creatures other than Magister of Worth.').
card_mana_cost('magister of worth', ['4', 'W', 'B']).
card_cmc('magister of worth', 6).
card_layout('magister of worth', 'normal').
card_power('magister of worth', 4).
card_toughness('magister of worth', 4).

% Found in: ARC, CON
card_name('magister sphinx', 'Magister Sphinx').
card_type('magister sphinx', 'Artifact Creature — Sphinx').
card_types('magister sphinx', ['Artifact', 'Creature']).
card_subtypes('magister sphinx', ['Sphinx']).
card_colors('magister sphinx', ['W', 'U', 'B']).
card_text('magister sphinx', 'Flying\nWhen Magister Sphinx enters the battlefield, target player\'s life total becomes 10.').
card_mana_cost('magister sphinx', ['4', 'W', 'U', 'B']).
card_cmc('magister sphinx', 7).
card_layout('magister sphinx', 'normal').
card_power('magister sphinx', 5).
card_toughness('magister sphinx', 5).

% Found in: MMQ
card_name('magistrate\'s scepter', 'Magistrate\'s Scepter').
card_type('magistrate\'s scepter', 'Artifact').
card_types('magistrate\'s scepter', ['Artifact']).
card_subtypes('magistrate\'s scepter', []).
card_colors('magistrate\'s scepter', []).
card_text('magistrate\'s scepter', '{4}, {T}: Put a charge counter on Magistrate\'s Scepter.\n{T}, Remove three charge counters from Magistrate\'s Scepter: Take an extra turn after this one.').
card_mana_cost('magistrate\'s scepter', ['3']).
card_cmc('magistrate\'s scepter', 3).
card_layout('magistrate\'s scepter', 'normal').

% Found in: MMQ
card_name('magistrate\'s veto', 'Magistrate\'s Veto').
card_type('magistrate\'s veto', 'Enchantment').
card_types('magistrate\'s veto', ['Enchantment']).
card_subtypes('magistrate\'s veto', []).
card_colors('magistrate\'s veto', ['R']).
card_text('magistrate\'s veto', 'White creatures and blue creatures can\'t block.').
card_mana_cost('magistrate\'s veto', ['2', 'R']).
card_cmc('magistrate\'s veto', 3).
card_layout('magistrate\'s veto', 'normal').

% Found in: PLS
card_name('magma burst', 'Magma Burst').
card_type('magma burst', 'Instant').
card_types('magma burst', ['Instant']).
card_subtypes('magma burst', []).
card_colors('magma burst', ['R']).
card_text('magma burst', 'Kicker—Sacrifice two lands. (You may sacrifice two lands in addition to any other costs as you cast this spell.)\nMagma Burst deals 3 damage to target creature or player. If Magma Burst was kicked, it deals 3 damage to another target creature or player.').
card_mana_cost('magma burst', ['3', 'R']).
card_cmc('magma burst', 4).
card_layout('magma burst', 'normal').

% Found in: 5DN, PO2
card_name('magma giant', 'Magma Giant').
card_type('magma giant', 'Creature — Giant').
card_types('magma giant', ['Creature']).
card_subtypes('magma giant', ['Giant']).
card_colors('magma giant', ['R']).
card_text('magma giant', 'When Magma Giant enters the battlefield, it deals 2 damage to each creature and each player.').
card_mana_cost('magma giant', ['5', 'R', 'R']).
card_cmc('magma giant', 7).
card_layout('magma giant', 'normal').
card_power('magma giant', 5).
card_toughness('magma giant', 5).

% Found in: 5DN, DD2, DD3_JVC, DDL, THS, pFNM
card_name('magma jet', 'Magma Jet').
card_type('magma jet', 'Instant').
card_types('magma jet', ['Instant']).
card_subtypes('magma jet', []).
card_colors('magma jet', ['R']).
card_text('magma jet', 'Magma Jet deals 2 damage to target creature or player. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('magma jet', ['1', 'R']).
card_cmc('magma jet', 2).
card_layout('magma jet', 'normal').

% Found in: VIS
card_name('magma mine', 'Magma Mine').
card_type('magma mine', 'Artifact').
card_types('magma mine', ['Artifact']).
card_subtypes('magma mine', []).
card_colors('magma mine', []).
card_text('magma mine', '{4}: Put a pressure counter on Magma Mine.\n{T}, Sacrifice Magma Mine: Magma Mine deals damage equal to the number of pressure counters on it to target creature or player.').
card_mana_cost('magma mine', ['1']).
card_cmc('magma mine', 1).
card_layout('magma mine', 'normal').

% Found in: M10, M11
card_name('magma phoenix', 'Magma Phoenix').
card_type('magma phoenix', 'Creature — Phoenix').
card_types('magma phoenix', ['Creature']).
card_subtypes('magma phoenix', ['Phoenix']).
card_colors('magma phoenix', ['R']).
card_text('magma phoenix', 'Flying\nWhen Magma Phoenix dies, it deals 3 damage to each creature and each player.\n{3}{R}{R}: Return Magma Phoenix from your graveyard to your hand.').
card_mana_cost('magma phoenix', ['3', 'R', 'R']).
card_cmc('magma phoenix', 5).
card_layout('magma phoenix', 'normal').
card_power('magma phoenix', 3).
card_toughness('magma phoenix', 3).

% Found in: ZEN
card_name('magma rift', 'Magma Rift').
card_type('magma rift', 'Sorcery').
card_types('magma rift', ['Sorcery']).
card_subtypes('magma rift', []).
card_colors('magma rift', ['R']).
card_text('magma rift', 'As an additional cost to cast Magma Rift, sacrifice a land.\nMagma Rift deals 5 damage to target creature.').
card_mana_cost('magma rift', ['2', 'R']).
card_cmc('magma rift', 3).
card_layout('magma rift', 'normal').

% Found in: LGN
card_name('magma sliver', 'Magma Sliver').
card_type('magma sliver', 'Creature — Sliver').
card_types('magma sliver', ['Creature']).
card_subtypes('magma sliver', ['Sliver']).
card_colors('magma sliver', ['R']).
card_text('magma sliver', 'All Slivers have \"{T}: Target Sliver creature gets +X/+0 until end of turn, where X is the number of Slivers on the battlefield.\"').
card_mana_cost('magma sliver', ['3', 'R']).
card_cmc('magma sliver', 4).
card_layout('magma sliver', 'normal').
card_power('magma sliver', 3).
card_toughness('magma sliver', 3).

% Found in: ALA, DDJ, JOU, pFNM
card_name('magma spray', 'Magma Spray').
card_type('magma spray', 'Instant').
card_types('magma spray', ['Instant']).
card_subtypes('magma spray', []).
card_colors('magma spray', ['R']).
card_text('magma spray', 'Magma Spray deals 2 damage to target creature. If that creature would die this turn, exile it instead.').
card_mana_cost('magma spray', ['R']).
card_cmc('magma spray', 1).
card_layout('magma spray', 'normal').

% Found in: ODY
card_name('magma vein', 'Magma Vein').
card_type('magma vein', 'Enchantment').
card_types('magma vein', ['Enchantment']).
card_subtypes('magma vein', []).
card_colors('magma vein', ['R']).
card_text('magma vein', '{R}, Sacrifice a land: Magma Vein deals 1 damage to each creature without flying.').
card_mana_cost('magma vein', ['2', 'R']).
card_cmc('magma vein', 3).
card_layout('magma vein', 'normal').

% Found in: C14, M13, pMGD
card_name('magmaquake', 'Magmaquake').
card_type('magmaquake', 'Instant').
card_types('magmaquake', ['Instant']).
card_subtypes('magmaquake', []).
card_colors('magmaquake', ['R']).
card_text('magmaquake', 'Magmaquake deals X damage to each creature without flying and each planeswalker.').
card_mana_cost('magmaquake', ['X', 'R', 'R']).
card_cmc('magmaquake', 2).
card_layout('magmaquake', 'normal').

% Found in: TMP, TPR
card_name('magmasaur', 'Magmasaur').
card_type('magmasaur', 'Creature — Elemental Lizard').
card_types('magmasaur', ['Creature']).
card_subtypes('magmasaur', ['Elemental', 'Lizard']).
card_colors('magmasaur', ['R']).
card_text('magmasaur', 'Magmasaur enters the battlefield with five +1/+1 counters on it.\nAt the beginning of your upkeep, you may remove a +1/+1 counter from Magmasaur. If you don\'t, sacrifice Magmasaur and it deals damage equal to the number of +1/+1 counters on it to each creature without flying and each player.').
card_mana_cost('magmasaur', ['3', 'R', 'R']).
card_cmc('magmasaur', 5).
card_layout('magmasaur', 'normal').
card_power('magmasaur', 0).
card_toughness('magmasaur', 0).

% Found in: DTK
card_name('magmatic chasm', 'Magmatic Chasm').
card_type('magmatic chasm', 'Sorcery').
card_types('magmatic chasm', ['Sorcery']).
card_subtypes('magmatic chasm', []).
card_colors('magmatic chasm', ['R']).
card_text('magmatic chasm', 'Creatures without flying can\'t block this turn.').
card_mana_cost('magmatic chasm', ['1', 'R']).
card_cmc('magmatic chasm', 2).
card_layout('magmatic chasm', 'normal').

% Found in: CSP
card_name('magmatic core', 'Magmatic Core').
card_type('magmatic core', 'Enchantment').
card_types('magmatic core', ['Enchantment']).
card_subtypes('magmatic core', []).
card_colors('magmatic core', ['R']).
card_text('magmatic core', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of your end step, Magmatic Core deals X damage divided as you choose among any number of target creatures, where X is the number of age counters on it.').
card_mana_cost('magmatic core', ['2', 'R', 'R']).
card_cmc('magmatic core', 4).
card_layout('magmatic core', 'normal').

% Found in: CMD
card_name('magmatic force', 'Magmatic Force').
card_type('magmatic force', 'Creature — Elemental').
card_types('magmatic force', ['Creature']).
card_subtypes('magmatic force', ['Elemental']).
card_colors('magmatic force', ['R']).
card_text('magmatic force', 'At the beginning of each upkeep, Magmatic Force deals 3 damage to target creature or player.').
card_mana_cost('magmatic force', ['5', 'R', 'R', 'R']).
card_cmc('magmatic force', 8).
card_layout('magmatic force', 'normal').
card_power('magmatic force', 7).
card_toughness('magmatic force', 7).

% Found in: ORI
card_name('magmatic insight', 'Magmatic Insight').
card_type('magmatic insight', 'Sorcery').
card_types('magmatic insight', ['Sorcery']).
card_subtypes('magmatic insight', []).
card_colors('magmatic insight', ['R']).
card_text('magmatic insight', 'As an additional cost to cast Magmatic Insight, discard a land card.\nDraw two cards.').
card_mana_cost('magmatic insight', ['R']).
card_cmc('magmatic insight', 1).
card_layout('magmatic insight', 'normal').

% Found in: DDP, ROE
card_name('magmaw', 'Magmaw').
card_type('magmaw', 'Creature — Elemental').
card_types('magmaw', ['Creature']).
card_subtypes('magmaw', ['Elemental']).
card_colors('magmaw', ['R']).
card_text('magmaw', '{1}, Sacrifice a nonland permanent: Magmaw deals 1 damage to target creature or player.').
card_mana_cost('magmaw', ['3', 'R', 'R']).
card_cmc('magmaw', 5).
card_layout('magmaw', 'normal').
card_power('magmaw', 4).
card_toughness('magmaw', 4).

% Found in: DST
card_name('magnetic flux', 'Magnetic Flux').
card_type('magnetic flux', 'Instant').
card_types('magnetic flux', ['Instant']).
card_subtypes('magnetic flux', []).
card_colors('magnetic flux', ['U']).
card_text('magnetic flux', 'Artifact creatures you control gain flying until end of turn.').
card_mana_cost('magnetic flux', ['2', 'U']).
card_cmc('magnetic flux', 3).
card_layout('magnetic flux', 'normal').

% Found in: MBS
card_name('magnetic mine', 'Magnetic Mine').
card_type('magnetic mine', 'Artifact').
card_types('magnetic mine', ['Artifact']).
card_subtypes('magnetic mine', []).
card_colors('magnetic mine', []).
card_text('magnetic mine', 'Whenever another artifact is put into a graveyard from the battlefield, Magnetic Mine deals 2 damage to that artifact\'s controller.').
card_mana_cost('magnetic mine', ['4']).
card_cmc('magnetic mine', 4).
card_layout('magnetic mine', 'normal').

% Found in: 3ED, 4ED, ARN
card_name('magnetic mountain', 'Magnetic Mountain').
card_type('magnetic mountain', 'Enchantment').
card_types('magnetic mountain', ['Enchantment']).
card_subtypes('magnetic mountain', []).
card_colors('magnetic mountain', ['R']).
card_text('magnetic mountain', 'Blue creatures don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player may choose any number of tapped blue creatures he or she controls and pay {4} for each creature chosen this way. If the player does, untap those creatures.').
card_mana_cost('magnetic mountain', ['1', 'R', 'R']).
card_cmc('magnetic mountain', 3).
card_layout('magnetic mountain', 'normal').

% Found in: 5DN
card_name('magnetic theft', 'Magnetic Theft').
card_type('magnetic theft', 'Instant').
card_types('magnetic theft', ['Instant']).
card_subtypes('magnetic theft', []).
card_colors('magnetic theft', ['R']).
card_text('magnetic theft', 'Attach target Equipment to target creature. (Control of the Equipment doesn\'t change.)').
card_mana_cost('magnetic theft', ['R']).
card_cmc('magnetic theft', 1).
card_layout('magnetic theft', 'normal').

% Found in: TMP
card_name('magnetic web', 'Magnetic Web').
card_type('magnetic web', 'Artifact').
card_types('magnetic web', ['Artifact']).
card_subtypes('magnetic web', []).
card_colors('magnetic web', []).
card_text('magnetic web', 'If a creature with a magnet counter on it attacks, all creatures with magnet counters on them attack if able.\nWhenever a creature with a magnet counter on it attacks, all creatures with magnet counters on them block that creature this turn if able.\n{1}, {T}: Put a magnet counter on target creature.').
card_mana_cost('magnetic web', ['2']).
card_cmc('magnetic web', 2).
card_layout('magnetic web', 'normal').

% Found in: UDS
card_name('magnify', 'Magnify').
card_type('magnify', 'Instant').
card_types('magnify', ['Instant']).
card_subtypes('magnify', []).
card_colors('magnify', ['G']).
card_text('magnify', 'All creatures get +1/+1 until end of turn.').
card_mana_cost('magnify', ['G']).
card_cmc('magnify', 1).
card_layout('magnify', 'normal').

% Found in: PLS
card_name('magnigoth treefolk', 'Magnigoth Treefolk').
card_type('magnigoth treefolk', 'Creature — Treefolk').
card_types('magnigoth treefolk', ['Creature']).
card_subtypes('magnigoth treefolk', ['Treefolk']).
card_colors('magnigoth treefolk', ['G']).
card_text('magnigoth treefolk', 'Domain — For each basic land type among lands you control, Magnigoth Treefolk has landwalk of that type. (It can\'t be blocked as long as defending player controls a land of that type.)').
card_mana_cost('magnigoth treefolk', ['4', 'G']).
card_cmc('magnigoth treefolk', 5).
card_layout('magnigoth treefolk', 'normal').
card_power('magnigoth treefolk', 2).
card_toughness('magnigoth treefolk', 6).

% Found in: 9ED, ODY
card_name('magnivore', 'Magnivore').
card_type('magnivore', 'Creature — Lhurgoyf').
card_types('magnivore', ['Creature']).
card_subtypes('magnivore', ['Lhurgoyf']).
card_colors('magnivore', ['R']).
card_text('magnivore', 'Haste (This creature can attack the turn it comes under your control.)\nMagnivore\'s power and toughness are each equal to the number of sorcery cards in all graveyards.').
card_mana_cost('magnivore', ['2', 'R', 'R']).
card_cmc('magnivore', 4).
card_layout('magnivore', 'normal').
card_power('magnivore', '*').
card_toughness('magnivore', '*').

% Found in: ZEN
card_name('magosi, the waterveil', 'Magosi, the Waterveil').
card_type('magosi, the waterveil', 'Land').
card_types('magosi, the waterveil', ['Land']).
card_subtypes('magosi, the waterveil', []).
card_colors('magosi, the waterveil', []).
card_text('magosi, the waterveil', 'Magosi, the Waterveil enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\n{U}, {T}: Put an eon counter on Magosi, the Waterveil. Skip your next turn.\n{T}, Remove an eon counter from Magosi, the Waterveil and return it to its owner\'s hand: Take an extra turn after this one.').
card_layout('magosi, the waterveil', 'normal').

% Found in: FUT
card_name('magus of the abyss', 'Magus of the Abyss').
card_type('magus of the abyss', 'Creature — Human Wizard').
card_types('magus of the abyss', ['Creature']).
card_subtypes('magus of the abyss', ['Human', 'Wizard']).
card_colors('magus of the abyss', ['B']).
card_text('magus of the abyss', 'At the beginning of each player\'s upkeep, destroy target nonartifact creature that player controls of his or her choice. It can\'t be regenerated.').
card_mana_cost('magus of the abyss', ['3', 'B']).
card_cmc('magus of the abyss', 4).
card_layout('magus of the abyss', 'normal').
card_power('magus of the abyss', 4).
card_toughness('magus of the abyss', 3).

% Found in: C13, PLC
card_name('magus of the arena', 'Magus of the Arena').
card_type('magus of the arena', 'Creature — Human Wizard').
card_types('magus of the arena', ['Creature']).
card_subtypes('magus of the arena', ['Human', 'Wizard']).
card_colors('magus of the arena', ['R']).
card_text('magus of the arena', '{3}, {T}: Tap target creature you control and target creature of an opponent\'s choice he or she controls. Those creatures fight each other. (Each deals damage equal to its power to the other.)').
card_mana_cost('magus of the arena', ['4', 'R', 'R']).
card_cmc('magus of the arena', 6).
card_layout('magus of the arena', 'normal').
card_power('magus of the arena', 5).
card_toughness('magus of the arena', 5).

% Found in: PLC
card_name('magus of the bazaar', 'Magus of the Bazaar').
card_type('magus of the bazaar', 'Creature — Human Wizard').
card_types('magus of the bazaar', ['Creature']).
card_subtypes('magus of the bazaar', ['Human', 'Wizard']).
card_colors('magus of the bazaar', ['U']).
card_text('magus of the bazaar', '{T}: Draw two cards, then discard three cards.').
card_mana_cost('magus of the bazaar', ['1', 'U']).
card_cmc('magus of the bazaar', 2).
card_layout('magus of the bazaar', 'normal').
card_power('magus of the bazaar', 0).
card_toughness('magus of the bazaar', 1).

% Found in: TSP
card_name('magus of the candelabra', 'Magus of the Candelabra').
card_type('magus of the candelabra', 'Creature — Human Wizard').
card_types('magus of the candelabra', ['Creature']).
card_subtypes('magus of the candelabra', ['Human', 'Wizard']).
card_colors('magus of the candelabra', ['G']).
card_text('magus of the candelabra', '{X}, {T}: Untap X target lands.').
card_mana_cost('magus of the candelabra', ['G']).
card_cmc('magus of the candelabra', 1).
card_layout('magus of the candelabra', 'normal').
card_power('magus of the candelabra', 1).
card_toughness('magus of the candelabra', 2).

% Found in: C14, PLC
card_name('magus of the coffers', 'Magus of the Coffers').
card_type('magus of the coffers', 'Creature — Human Wizard').
card_types('magus of the coffers', ['Creature']).
card_subtypes('magus of the coffers', ['Human', 'Wizard']).
card_colors('magus of the coffers', ['B']).
card_text('magus of the coffers', '{2}, {T}: Add {B} to your mana pool for each Swamp you control.').
card_mana_cost('magus of the coffers', ['4', 'B']).
card_cmc('magus of the coffers', 5).
card_layout('magus of the coffers', 'normal').
card_power('magus of the coffers', 4).
card_toughness('magus of the coffers', 4).

% Found in: TSP
card_name('magus of the disk', 'Magus of the Disk').
card_type('magus of the disk', 'Creature — Human Wizard').
card_types('magus of the disk', ['Creature']).
card_subtypes('magus of the disk', ['Human', 'Wizard']).
card_colors('magus of the disk', ['W']).
card_text('magus of the disk', 'Magus of the Disk enters the battlefield tapped.\n{1}, {T}: Destroy all artifacts, creatures, and enchantments.').
card_mana_cost('magus of the disk', ['2', 'W', 'W']).
card_cmc('magus of the disk', 4).
card_layout('magus of the disk', 'normal').
card_power('magus of the disk', 2).
card_toughness('magus of the disk', 4).

% Found in: FUT
card_name('magus of the future', 'Magus of the Future').
card_type('magus of the future', 'Creature — Human Wizard').
card_types('magus of the future', ['Creature']).
card_subtypes('magus of the future', ['Human', 'Wizard']).
card_colors('magus of the future', ['U']).
card_text('magus of the future', 'Play with the top card of your library revealed.\nYou may play the top card of your library.').
card_mana_cost('magus of the future', ['2', 'U', 'U', 'U']).
card_cmc('magus of the future', 5).
card_layout('magus of the future', 'normal').
card_power('magus of the future', 2).
card_toughness('magus of the future', 3).

% Found in: TSP
card_name('magus of the jar', 'Magus of the Jar').
card_type('magus of the jar', 'Creature — Human Wizard').
card_types('magus of the jar', ['Creature']).
card_subtypes('magus of the jar', ['Human', 'Wizard']).
card_colors('magus of the jar', ['U']).
card_text('magus of the jar', '{T}, Sacrifice Magus of the Jar: Each player exiles all cards from his or her hand face down and draws seven cards. At the beginning of the next end step, each player discards his or her hand and returns to his or her hand each card he or she exiled this way.').
card_mana_cost('magus of the jar', ['3', 'U', 'U']).
card_cmc('magus of the jar', 5).
card_layout('magus of the jar', 'normal').
card_power('magus of the jar', 3).
card_toughness('magus of the jar', 3).

% Found in: PLC
card_name('magus of the library', 'Magus of the Library').
card_type('magus of the library', 'Creature — Human Wizard').
card_types('magus of the library', ['Creature']).
card_subtypes('magus of the library', ['Human', 'Wizard']).
card_colors('magus of the library', ['G']).
card_text('magus of the library', '{T}: Add {1} to your mana pool.\n{T}: Draw a card. Activate this ability only if you have exactly seven cards in hand.').
card_mana_cost('magus of the library', ['G', 'G']).
card_cmc('magus of the library', 2).
card_layout('magus of the library', 'normal').
card_power('magus of the library', 1).
card_toughness('magus of the library', 1).

% Found in: CNS, TSP
card_name('magus of the mirror', 'Magus of the Mirror').
card_type('magus of the mirror', 'Creature — Human Wizard').
card_types('magus of the mirror', ['Creature']).
card_subtypes('magus of the mirror', ['Human', 'Wizard']).
card_colors('magus of the mirror', ['B']).
card_text('magus of the mirror', '{T}, Sacrifice Magus of the Mirror: Exchange life totals with target opponent. Activate this ability only during your upkeep.').
card_mana_cost('magus of the mirror', ['4', 'B', 'B']).
card_cmc('magus of the mirror', 6).
card_layout('magus of the mirror', 'normal').
card_power('magus of the mirror', 4).
card_toughness('magus of the mirror', 2).

% Found in: FUT
card_name('magus of the moat', 'Magus of the Moat').
card_type('magus of the moat', 'Creature — Human Wizard').
card_types('magus of the moat', ['Creature']).
card_subtypes('magus of the moat', ['Human', 'Wizard']).
card_colors('magus of the moat', ['W']).
card_text('magus of the moat', 'Creatures without flying can\'t attack.').
card_mana_cost('magus of the moat', ['2', 'W', 'W']).
card_cmc('magus of the moat', 4).
card_layout('magus of the moat', 'normal').
card_power('magus of the moat', 0).
card_toughness('magus of the moat', 3).

% Found in: FUT
card_name('magus of the moon', 'Magus of the Moon').
card_type('magus of the moon', 'Creature — Human Wizard').
card_types('magus of the moon', ['Creature']).
card_subtypes('magus of the moon', ['Human', 'Wizard']).
card_colors('magus of the moon', ['R']).
card_text('magus of the moon', 'Nonbasic lands are Mountains.').
card_mana_cost('magus of the moon', ['2', 'R']).
card_cmc('magus of the moon', 3).
card_layout('magus of the moon', 'normal').
card_power('magus of the moon', 2).
card_toughness('magus of the moon', 2).

% Found in: TSP
card_name('magus of the scroll', 'Magus of the Scroll').
card_type('magus of the scroll', 'Creature — Human Wizard').
card_types('magus of the scroll', ['Creature']).
card_subtypes('magus of the scroll', ['Human', 'Wizard']).
card_colors('magus of the scroll', ['R']).
card_text('magus of the scroll', '{3}, {T}: Name a card. Reveal a card at random from your hand. If it\'s the named card, Magus of the Scroll deals 2 damage to target creature or player.').
card_mana_cost('magus of the scroll', ['R']).
card_cmc('magus of the scroll', 1).
card_layout('magus of the scroll', 'normal').
card_power('magus of the scroll', 1).
card_toughness('magus of the scroll', 1).

% Found in: PLC
card_name('magus of the tabernacle', 'Magus of the Tabernacle').
card_type('magus of the tabernacle', 'Creature — Human Wizard').
card_types('magus of the tabernacle', ['Creature']).
card_subtypes('magus of the tabernacle', ['Human', 'Wizard']).
card_colors('magus of the tabernacle', ['W']).
card_text('magus of the tabernacle', 'All creatures have \"At the beginning of your upkeep, sacrifice this creature unless you pay {1}.\"').
card_mana_cost('magus of the tabernacle', ['3', 'W']).
card_cmc('magus of the tabernacle', 4).
card_layout('magus of the tabernacle', 'normal').
card_power('magus of the tabernacle', 2).
card_toughness('magus of the tabernacle', 6).

% Found in: 5ED, ICE, ME2
card_name('magus of the unseen', 'Magus of the Unseen').
card_type('magus of the unseen', 'Creature — Human Wizard').
card_types('magus of the unseen', ['Creature']).
card_subtypes('magus of the unseen', ['Human', 'Wizard']).
card_colors('magus of the unseen', ['U']).
card_text('magus of the unseen', '{1}{U}, {T}: Untap target artifact an opponent controls and gain control of it until end of turn. It gains haste until end of turn. When you lose control of the artifact, tap it.').
card_mana_cost('magus of the unseen', ['1', 'U']).
card_cmc('magus of the unseen', 2).
card_layout('magus of the unseen', 'normal').
card_power('magus of the unseen', 1).
card_toughness('magus of the unseen', 1).

% Found in: CMD, FUT
card_name('magus of the vineyard', 'Magus of the Vineyard').
card_type('magus of the vineyard', 'Creature — Human Wizard').
card_types('magus of the vineyard', ['Creature']).
card_subtypes('magus of the vineyard', ['Human', 'Wizard']).
card_colors('magus of the vineyard', ['G']).
card_text('magus of the vineyard', 'At the beginning of each player\'s precombat main phase, add {G}{G} to that player\'s mana pool.').
card_mana_cost('magus of the vineyard', ['G']).
card_cmc('magus of the vineyard', 1).
card_layout('magus of the vineyard', 'normal').
card_power('magus of the vineyard', 1).
card_toughness('magus of the vineyard', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 7ED, 8ED, 9ED, BTD, CED, CEI, DPA, LEA, LEB, M15, ME4, ORI
card_name('mahamoti djinn', 'Mahamoti Djinn').
card_type('mahamoti djinn', 'Creature — Djinn').
card_types('mahamoti djinn', ['Creature']).
card_subtypes('mahamoti djinn', ['Djinn']).
card_colors('mahamoti djinn', ['U']).
card_text('mahamoti djinn', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('mahamoti djinn', ['4', 'U', 'U']).
card_cmc('mahamoti djinn', 6).
card_layout('mahamoti djinn', 'normal').
card_power('mahamoti djinn', 5).
card_toughness('mahamoti djinn', 6).

% Found in: TOR
card_name('major teroh', 'Major Teroh').
card_type('major teroh', 'Legendary Creature — Bird Soldier').
card_types('major teroh', ['Creature']).
card_subtypes('major teroh', ['Bird', 'Soldier']).
card_supertypes('major teroh', ['Legendary']).
card_colors('major teroh', ['W']).
card_text('major teroh', 'Flying\n{3}{W}{W}, Sacrifice Major Teroh: Exile all black creatures.').
card_mana_cost('major teroh', ['3', 'W']).
card_cmc('major teroh', 4).
card_layout('major teroh', 'normal').
card_power('major teroh', 2).
card_toughness('major teroh', 3).

% Found in: ISD
card_name('make a wish', 'Make a Wish').
card_type('make a wish', 'Sorcery').
card_types('make a wish', ['Sorcery']).
card_subtypes('make a wish', []).
card_colors('make a wish', ['G']).
card_text('make a wish', 'Return two cards at random from your graveyard to your hand.').
card_mana_cost('make a wish', ['3', 'G']).
card_cmc('make a wish', 4).
card_layout('make a wish', 'normal').

% Found in: ARC, LRW
card_name('makeshift mannequin', 'Makeshift Mannequin').
card_type('makeshift mannequin', 'Instant').
card_types('makeshift mannequin', ['Instant']).
card_subtypes('makeshift mannequin', []).
card_colors('makeshift mannequin', ['B']).
card_text('makeshift mannequin', 'Return target creature card from your graveyard to the battlefield with a mannequin counter on it. For as long as that creature has a mannequin counter on it, it has \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_mana_cost('makeshift mannequin', ['3', 'B']).
card_cmc('makeshift mannequin', 4).
card_layout('makeshift mannequin', 'normal').

% Found in: ISD
card_name('makeshift mauler', 'Makeshift Mauler').
card_type('makeshift mauler', 'Creature — Zombie Horror').
card_types('makeshift mauler', ['Creature']).
card_subtypes('makeshift mauler', ['Zombie', 'Horror']).
card_colors('makeshift mauler', ['U']).
card_text('makeshift mauler', 'As an additional cost to cast Makeshift Mauler, exile a creature card from your graveyard.').
card_mana_cost('makeshift mauler', ['3', 'U']).
card_cmc('makeshift mauler', 4).
card_layout('makeshift mauler', 'normal').
card_power('makeshift mauler', 4).
card_toughness('makeshift mauler', 5).

% Found in: DDP, ROE
card_name('makindi griffin', 'Makindi Griffin').
card_type('makindi griffin', 'Creature — Griffin').
card_types('makindi griffin', ['Creature']).
card_subtypes('makindi griffin', ['Griffin']).
card_colors('makindi griffin', ['W']).
card_text('makindi griffin', 'Flying').
card_mana_cost('makindi griffin', ['3', 'W']).
card_cmc('makindi griffin', 4).
card_layout('makindi griffin', 'normal').
card_power('makindi griffin', 2).
card_toughness('makindi griffin', 4).

% Found in: BFZ
card_name('makindi patrol', 'Makindi Patrol').
card_type('makindi patrol', 'Creature — Human Knight Ally').
card_types('makindi patrol', ['Creature']).
card_subtypes('makindi patrol', ['Human', 'Knight', 'Ally']).
card_colors('makindi patrol', ['W']).
card_text('makindi patrol', 'Rally — Whenever Makindi Patrol or another Ally enters the battlefield under your control, creatures you control gain vigilance until end of turn.').
card_mana_cost('makindi patrol', ['2', 'W']).
card_cmc('makindi patrol', 3).
card_layout('makindi patrol', 'normal').
card_power('makindi patrol', 2).
card_toughness('makindi patrol', 3).

% Found in: ZEN
card_name('makindi shieldmate', 'Makindi Shieldmate').
card_type('makindi shieldmate', 'Creature — Kor Soldier Ally').
card_types('makindi shieldmate', ['Creature']).
card_subtypes('makindi shieldmate', ['Kor', 'Soldier', 'Ally']).
card_colors('makindi shieldmate', ['W']).
card_text('makindi shieldmate', 'Defender\nWhenever Makindi Shieldmate or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Makindi Shieldmate.').
card_mana_cost('makindi shieldmate', ['2', 'W']).
card_cmc('makindi shieldmate', 3).
card_layout('makindi shieldmate', 'normal').
card_power('makindi shieldmate', 0).
card_toughness('makindi shieldmate', 3).

% Found in: BFZ
card_name('makindi sliderunner', 'Makindi Sliderunner').
card_type('makindi sliderunner', 'Creature — Beast').
card_types('makindi sliderunner', ['Creature']).
card_subtypes('makindi sliderunner', ['Beast']).
card_colors('makindi sliderunner', ['R']).
card_text('makindi sliderunner', 'Trample\nLandfall — Whenever a land enters the battlefield under your control, Makindi Sliderunner gets +1/+1 until end of turn.').
card_mana_cost('makindi sliderunner', ['1', 'R']).
card_cmc('makindi sliderunner', 2).
card_layout('makindi sliderunner', 'normal').
card_power('makindi sliderunner', 2).
card_toughness('makindi sliderunner', 1).

% Found in: PLC
card_name('malach of the dawn', 'Malach of the Dawn').
card_type('malach of the dawn', 'Creature — Angel').
card_types('malach of the dawn', ['Creature']).
card_subtypes('malach of the dawn', ['Angel']).
card_colors('malach of the dawn', ['W']).
card_text('malach of the dawn', 'Flying\n{W}{W}{W}: Regenerate Malach of the Dawn.').
card_mana_cost('malach of the dawn', ['2', 'W', 'W']).
card_cmc('malach of the dawn', 4).
card_layout('malach of the dawn', 'normal').
card_power('malach of the dawn', 2).
card_toughness('malach of the dawn', 4).

% Found in: MRD
card_name('malachite golem', 'Malachite Golem').
card_type('malachite golem', 'Artifact Creature — Golem').
card_types('malachite golem', ['Artifact', 'Creature']).
card_subtypes('malachite golem', ['Golem']).
card_colors('malachite golem', []).
card_text('malachite golem', '{1}{G}: Malachite Golem gains trample until end of turn.').
card_mana_cost('malachite golem', ['6']).
card_cmc('malachite golem', 6).
card_layout('malachite golem', 'normal').
card_power('malachite golem', 5).
card_toughness('malachite golem', 3).

% Found in: ICE
card_name('malachite talisman', 'Malachite Talisman').
card_type('malachite talisman', 'Artifact').
card_types('malachite talisman', ['Artifact']).
card_subtypes('malachite talisman', []).
card_colors('malachite talisman', []).
card_text('malachite talisman', 'Whenever a player casts a green spell, you may pay {3}. If you do, untap target permanent.').
card_mana_cost('malachite talisman', ['2']).
card_cmc('malachite talisman', 2).
card_layout('malachite talisman', 'normal').

% Found in: ZEN
card_name('malakir bloodwitch', 'Malakir Bloodwitch').
card_type('malakir bloodwitch', 'Creature — Vampire Shaman').
card_types('malakir bloodwitch', ['Creature']).
card_subtypes('malakir bloodwitch', ['Vampire', 'Shaman']).
card_colors('malakir bloodwitch', ['B']).
card_text('malakir bloodwitch', 'Flying, protection from white\nWhen Malakir Bloodwitch enters the battlefield, each opponent loses life equal to the number of Vampires you control. You gain life equal to the life lost this way.').
card_mana_cost('malakir bloodwitch', ['3', 'B', 'B']).
card_cmc('malakir bloodwitch', 5).
card_layout('malakir bloodwitch', 'normal').
card_power('malakir bloodwitch', 4).
card_toughness('malakir bloodwitch', 4).

% Found in: ORI
card_name('malakir cullblade', 'Malakir Cullblade').
card_type('malakir cullblade', 'Creature — Vampire Warrior').
card_types('malakir cullblade', ['Creature']).
card_subtypes('malakir cullblade', ['Vampire', 'Warrior']).
card_colors('malakir cullblade', ['B']).
card_text('malakir cullblade', 'Whenever a creature an opponent controls dies, put a +1/+1 counter on Malakir Cullblade.').
card_mana_cost('malakir cullblade', ['1', 'B']).
card_cmc('malakir cullblade', 2).
card_layout('malakir cullblade', 'normal').
card_power('malakir cullblade', 1).
card_toughness('malakir cullblade', 1).

% Found in: BFZ
card_name('malakir familiar', 'Malakir Familiar').
card_type('malakir familiar', 'Creature — Bat').
card_types('malakir familiar', ['Creature']).
card_subtypes('malakir familiar', ['Bat']).
card_colors('malakir familiar', ['B']).
card_text('malakir familiar', 'Flying, deathtouch\nWhenever you gain life, Malakir Familiar gets +1/+1 until end of turn.').
card_mana_cost('malakir familiar', ['2', 'B']).
card_cmc('malakir familiar', 3).
card_layout('malakir familiar', 'normal').
card_power('malakir familiar', 2).
card_toughness('malakir familiar', 1).

% Found in: ODY
card_name('malevolent awakening', 'Malevolent Awakening').
card_type('malevolent awakening', 'Enchantment').
card_types('malevolent awakening', ['Enchantment']).
card_subtypes('malevolent awakening', []).
card_colors('malevolent awakening', ['B']).
card_text('malevolent awakening', '{1}{B}{B}, Sacrifice a creature: Return target creature card from your graveyard to your hand.').
card_mana_cost('malevolent awakening', ['1', 'B', 'B']).
card_cmc('malevolent awakening', 3).
card_layout('malevolent awakening', 'normal').

% Found in: CMD, CON, pPRE
card_name('malfegor', 'Malfegor').
card_type('malfegor', 'Legendary Creature — Demon Dragon').
card_types('malfegor', ['Creature']).
card_subtypes('malfegor', ['Demon', 'Dragon']).
card_supertypes('malfegor', ['Legendary']).
card_colors('malfegor', ['B', 'R']).
card_text('malfegor', 'Flying\nWhen Malfegor enters the battlefield, discard your hand. Each opponent sacrifices a creature for each card discarded this way.').
card_mana_cost('malfegor', ['2', 'B', 'B', 'R', 'R']).
card_cmc('malfegor', 6).
card_layout('malfegor', 'normal').
card_power('malfegor', 6).
card_toughness('malfegor', 6).

% Found in: VAN
card_name('malfegor avatar', 'Malfegor Avatar').
card_type('malfegor avatar', 'Vanguard').
card_types('malfegor avatar', ['Vanguard']).
card_subtypes('malfegor avatar', []).
card_colors('malfegor avatar', []).
card_text('malfegor avatar', 'Whenever a creature enters the battlefield under your control, if it was unearthed, it gets +3/+0.\nWhenever a creature you control is exiled, if it was unearthed, shuffle that card into its owner\'s library.').
card_layout('malfegor avatar', 'vanguard').

% Found in: DDH, INV
card_name('malice', 'Malice').
card_type('malice', 'Instant').
card_types('malice', ['Instant']).
card_subtypes('malice', []).
card_colors('malice', ['B']).
card_text('malice', 'Destroy target nonblack creature. It can\'t be regenerated.').
card_mana_cost('malice', ['3', 'B']).
card_cmc('malice', 4).
card_layout('malice', 'split').

% Found in: PLS
card_name('malicious advice', 'Malicious Advice').
card_type('malicious advice', 'Instant').
card_types('malicious advice', ['Instant']).
card_subtypes('malicious advice', []).
card_colors('malicious advice', ['U', 'B']).
card_text('malicious advice', 'Tap X target artifacts, creatures, and/or lands. You lose X life.').
card_mana_cost('malicious advice', ['X', 'U', 'B']).
card_cmc('malicious advice', 2).
card_layout('malicious advice', 'normal').

% Found in: C14
card_name('malicious affliction', 'Malicious Affliction').
card_type('malicious affliction', 'Instant').
card_types('malicious affliction', ['Instant']).
card_subtypes('malicious affliction', []).
card_colors('malicious affliction', ['B']).
card_text('malicious affliction', 'Morbid — When you cast Malicious Affliction, if a creature died this turn, you may copy Malicious Affliction and may choose a new target for the copy.\nDestroy target nonblack creature.').
card_mana_cost('malicious affliction', ['B', 'B']).
card_cmc('malicious affliction', 2).
card_layout('malicious affliction', 'normal').

% Found in: AVR
card_name('malicious intent', 'Malicious Intent').
card_type('malicious intent', 'Enchantment — Aura').
card_types('malicious intent', ['Enchantment']).
card_subtypes('malicious intent', ['Aura']).
card_colors('malicious intent', ['R']).
card_text('malicious intent', 'Enchant creature\nEnchanted creature has \"{T}: Target creature can\'t block this turn.\"').
card_mana_cost('malicious intent', ['1', 'R']).
card_cmc('malicious intent', 2).
card_layout('malicious intent', 'normal').

% Found in: MIR
card_name('malignant growth', 'Malignant Growth').
card_type('malignant growth', 'Enchantment').
card_types('malignant growth', ['Enchantment']).
card_subtypes('malignant growth', []).
card_colors('malignant growth', ['U', 'G']).
card_text('malignant growth', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of your upkeep, put a growth counter on Malignant Growth.\nAt the beginning of each opponent\'s draw step, that player draws an additional card for each growth counter on Malignant Growth, then Malignant Growth deals damage to the player equal to the number of cards he or she drew this way.').
card_mana_cost('malignant growth', ['3', 'G', 'U']).
card_cmc('malignant growth', 5).
card_layout('malignant growth', 'normal').
card_reserved('malignant growth').

% Found in: AVR
card_name('malignus', 'Malignus').
card_type('malignus', 'Creature — Elemental Spirit').
card_types('malignus', ['Creature']).
card_subtypes('malignus', ['Elemental', 'Spirit']).
card_colors('malignus', ['R']).
card_text('malignus', 'Malignus\'s power and toughness are each equal to half the highest life total among your opponents, rounded up.\nDamage that would be dealt by Malignus can\'t be prevented.').
card_mana_cost('malignus', ['3', 'R', 'R']).
card_cmc('malignus', 5).
card_layout('malignus', 'normal').
card_power('malignus', '*').
card_toughness('malignus', '*').

% Found in: HML
card_name('mammoth harness', 'Mammoth Harness').
card_type('mammoth harness', 'Enchantment — Aura').
card_types('mammoth harness', ['Enchantment']).
card_subtypes('mammoth harness', ['Aura']).
card_colors('mammoth harness', ['G']).
card_text('mammoth harness', 'Enchant creature\nEnchanted creature loses flying.\nWhenever enchanted creature blocks or becomes blocked by a creature, the other creature gains first strike until end of turn.').
card_mana_cost('mammoth harness', ['3', 'G']).
card_cmc('mammoth harness', 4).
card_layout('mammoth harness', 'normal').
card_reserved('mammoth harness').

% Found in: PC2, ROE
card_name('mammoth umbra', 'Mammoth Umbra').
card_type('mammoth umbra', 'Enchantment — Aura').
card_types('mammoth umbra', ['Enchantment']).
card_subtypes('mammoth umbra', ['Aura']).
card_colors('mammoth umbra', ['W']).
card_text('mammoth umbra', 'Enchant creature\nEnchanted creature gets +3/+3 and has vigilance.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('mammoth umbra', ['4', 'W']).
card_cmc('mammoth umbra', 5).
card_layout('mammoth umbra', 'normal').

% Found in: UNH
card_name('man of measure', 'Man of Measure').
card_type('man of measure', 'Creature — Human Knight').
card_types('man of measure', ['Creature']).
card_subtypes('man of measure', ['Human', 'Knight']).
card_colors('man of measure', ['W']).
card_text('man of measure', 'As long as you\'re shorter than an opponent, Man of Measure has first strike and gets +0/+1.\nAs long as you\'re taller than an opponent, Man of Measure gets +1/+0.').
card_mana_cost('man of measure', ['1', 'W', 'W']).
card_cmc('man of measure', 3).
card_layout('man of measure', 'normal').
card_power('man of measure', 2).
card_toughness('man of measure', 2).

% Found in: BRB, DD2, DD3_JVC, DDO, POR, S99, VIS, VMA, pARL
card_name('man-o\'-war', 'Man-o\'-War').
card_type('man-o\'-war', 'Creature — Jellyfish').
card_types('man-o\'-war', ['Creature']).
card_subtypes('man-o\'-war', ['Jellyfish']).
card_colors('man-o\'-war', ['U']).
card_text('man-o\'-war', 'When Man-o\'-War enters the battlefield, return target creature to its owner\'s hand.').
card_mana_cost('man-o\'-war', ['2', 'U']).
card_cmc('man-o\'-war', 3).
card_layout('man-o\'-war', 'normal').
card_power('man-o\'-war', 2).
card_toughness('man-o\'-war', 2).

% Found in: RTR
card_name('mana bloom', 'Mana Bloom').
card_type('mana bloom', 'Enchantment').
card_types('mana bloom', ['Enchantment']).
card_subtypes('mana bloom', []).
card_colors('mana bloom', ['G']).
card_text('mana bloom', 'Mana Bloom enters the battlefield with X charge counters on it.\nRemove a charge counter from Mana Bloom: Add one mana of any color to your mana pool. Activate this ability only once each turn.\nAt the beginning of your upkeep, if Mana Bloom has no charge counters on it, return it to its owner\'s hand.').
card_mana_cost('mana bloom', ['X', 'G']).
card_cmc('mana bloom', 1).
card_layout('mana bloom', 'normal').

% Found in: 7ED, EXO
card_name('mana breach', 'Mana Breach').
card_type('mana breach', 'Enchantment').
card_types('mana breach', ['Enchantment']).
card_subtypes('mana breach', []).
card_colors('mana breach', ['U']).
card_text('mana breach', 'Whenever a player casts a spell, that player returns a land he or she controls to its owner\'s hand.').
card_mana_cost('mana breach', ['2', 'U']).
card_cmc('mana breach', 3).
card_layout('mana breach', 'normal').

% Found in: NMS
card_name('mana cache', 'Mana Cache').
card_type('mana cache', 'Enchantment').
card_types('mana cache', ['Enchantment']).
card_subtypes('mana cache', []).
card_colors('mana cache', ['R']).
card_text('mana cache', 'At the beginning of each player\'s end step, put a charge counter on Mana Cache for each untapped land that player controls.\nRemove a charge counter from Mana Cache: Add {1} to your mana pool. Any player may activate this ability but only during his or her turn before the end step.').
card_mana_cost('mana cache', ['1', 'R', 'R']).
card_cmc('mana cache', 3).
card_layout('mana cache', 'normal').

% Found in: WTH
card_name('mana chains', 'Mana Chains').
card_type('mana chains', 'Enchantment — Aura').
card_types('mana chains', ['Enchantment']).
card_subtypes('mana chains', ['Aura']).
card_colors('mana chains', ['U']).
card_text('mana chains', 'Enchant creature\nEnchanted creature has \"Cumulative upkeep {1}.\" (At the beginning of its controller\'s upkeep, that player puts an age counter on it, then sacrifices it unless he or she pays its upkeep cost for each age counter on it.)').
card_mana_cost('mana chains', ['U']).
card_cmc('mana chains', 1).
card_layout('mana chains', 'normal').

% Found in: 4ED, 5ED, 7ED, 8ED, 9ED, DRK
card_name('mana clash', 'Mana Clash').
card_type('mana clash', 'Sorcery').
card_types('mana clash', ['Sorcery']).
card_subtypes('mana clash', []).
card_colors('mana clash', ['R']).
card_text('mana clash', 'You and target opponent each flip a coin. Mana Clash deals 1 damage to each player whose coin comes up tails. Repeat this process until both players\' coins come up heads on the same flip.').
card_mana_cost('mana clash', ['R']).
card_cmc('mana clash', 1).
card_layout('mana clash', 'normal').

% Found in: JOU
card_name('mana confluence', 'Mana Confluence').
card_type('mana confluence', 'Land').
card_types('mana confluence', ['Land']).
card_subtypes('mana confluence', []).
card_colors('mana confluence', []).
card_text('mana confluence', '{T}, Pay 1 life: Add one mana of any color to your mana pool.').
card_layout('mana confluence', 'normal').

% Found in: ME2, VMA, pJGP, pMEI
card_name('mana crypt', 'Mana Crypt').
card_type('mana crypt', 'Artifact').
card_types('mana crypt', ['Artifact']).
card_subtypes('mana crypt', []).
card_colors('mana crypt', []).
card_text('mana crypt', 'At the beginning of your upkeep, flip a coin. If you lose the flip, Mana Crypt deals 3 damage to you.\n{T}: Add {2} to your mana pool.').
card_mana_cost('mana crypt', ['0']).
card_cmc('mana crypt', 0).
card_layout('mana crypt', 'normal').

% Found in: CON, PLS
card_name('mana cylix', 'Mana Cylix').
card_type('mana cylix', 'Artifact').
card_types('mana cylix', ['Artifact']).
card_subtypes('mana cylix', []).
card_colors('mana cylix', []).
card_text('mana cylix', '{1}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('mana cylix', ['1']).
card_cmc('mana cylix', 1).
card_layout('mana cylix', 'normal').

% Found in: LEG, ME3, VMA
card_name('mana drain', 'Mana Drain').
card_type('mana drain', 'Instant').
card_types('mana drain', ['Instant']).
card_subtypes('mana drain', []).
card_colors('mana drain', ['U']).
card_text('mana drain', 'Counter target spell. At the beginning of your next main phase, add {X} to your mana pool, where X is that spell\'s converted mana cost.').
card_mana_cost('mana drain', ['U', 'U']).
card_cmc('mana drain', 2).
card_layout('mana drain', 'normal').

% Found in: ONS
card_name('mana echoes', 'Mana Echoes').
card_type('mana echoes', 'Enchantment').
card_types('mana echoes', ['Enchantment']).
card_subtypes('mana echoes', []).
card_colors('mana echoes', ['R']).
card_text('mana echoes', 'Whenever a creature enters the battlefield, you may add {X} to your mana pool, where X is the number of creatures you control that share a creature type with it.').
card_mana_cost('mana echoes', ['2', 'R', 'R']).
card_cmc('mana echoes', 4).
card_layout('mana echoes', 'normal').

% Found in: UNH
card_name('mana flair', 'Mana Flair').
card_type('mana flair', 'Instant').
card_types('mana flair', ['Instant']).
card_subtypes('mana flair', []).
card_colors('mana flair', ['R']).
card_text('mana flair', 'Add {R} to your mana pool for each nonland permanent by the artist of your choice.').
card_mana_cost('mana flair', ['1', 'R']).
card_cmc('mana flair', 2).
card_layout('mana flair', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED
card_name('mana flare', 'Mana Flare').
card_type('mana flare', 'Enchantment').
card_types('mana flare', ['Enchantment']).
card_subtypes('mana flare', []).
card_colors('mana flare', ['R']).
card_text('mana flare', 'Whenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_mana_cost('mana flare', ['2', 'R']).
card_cmc('mana flare', 3).
card_layout('mana flare', 'normal').

% Found in: 5DN, CNS
card_name('mana geyser', 'Mana Geyser').
card_type('mana geyser', 'Sorcery').
card_types('mana geyser', ['Sorcery']).
card_subtypes('mana geyser', []).
card_colors('mana geyser', ['R']).
card_text('mana geyser', 'Add {R} to your mana pool for each tapped land your opponents control.').
card_mana_cost('mana geyser', ['3', 'R', 'R']).
card_cmc('mana geyser', 5).
card_layout('mana geyser', 'normal').

% Found in: 8ED, 9ED, BRB, DDN, M11, M12, MM2, STH, TPR, pARL, pMPR
card_name('mana leak', 'Mana Leak').
card_type('mana leak', 'Instant').
card_types('mana leak', ['Instant']).
card_subtypes('mana leak', []).
card_colors('mana leak', ['U']).
card_text('mana leak', 'Counter target spell unless its controller pays {3}.').
card_mana_cost('mana leak', ['1', 'U']).
card_cmc('mana leak', 2).
card_layout('mana leak', 'normal').

% Found in: USG
card_name('mana leech', 'Mana Leech').
card_type('mana leech', 'Creature — Leech').
card_types('mana leech', ['Creature']).
card_subtypes('mana leech', ['Leech']).
card_colors('mana leech', ['B']).
card_text('mana leech', 'You may choose not to untap Mana Leech during your untap step.\n{T}: Tap target land. It doesn\'t untap during its controller\'s untap step for as long as Mana Leech remains tapped.').
card_mana_cost('mana leech', ['2', 'B']).
card_cmc('mana leech', 3).
card_layout('mana leech', 'normal').
card_power('mana leech', 1).
card_toughness('mana leech', 1).

% Found in: LEG, ME4
card_name('mana matrix', 'Mana Matrix').
card_type('mana matrix', 'Artifact').
card_types('mana matrix', ['Artifact']).
card_subtypes('mana matrix', []).
card_colors('mana matrix', []).
card_text('mana matrix', 'Instant and enchantment spells you cast cost up to {2} less to cast.').
card_mana_cost('mana matrix', ['6']).
card_cmc('mana matrix', 6).
card_layout('mana matrix', 'normal').
card_reserved('mana matrix').

% Found in: INV
card_name('mana maze', 'Mana Maze').
card_type('mana maze', 'Enchantment').
card_types('mana maze', ['Enchantment']).
card_subtypes('mana maze', []).
card_colors('mana maze', ['U']).
card_text('mana maze', 'Players can\'t cast spells that share a color with the spell most recently cast this turn.').
card_mana_cost('mana maze', ['1', 'U']).
card_cmc('mana maze', 2).
card_layout('mana maze', 'normal').

% Found in: 6ED, MIR, VMA
card_name('mana prism', 'Mana Prism').
card_type('mana prism', 'Artifact').
card_types('mana prism', ['Artifact']).
card_subtypes('mana prism', []).
card_colors('mana prism', []).
card_text('mana prism', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('mana prism', ['3']).
card_cmc('mana prism', 3).
card_layout('mana prism', 'normal').

% Found in: SHM
card_name('mana reflection', 'Mana Reflection').
card_type('mana reflection', 'Enchantment').
card_types('mana reflection', ['Enchantment']).
card_subtypes('mana reflection', []).
card_colors('mana reflection', ['G']).
card_text('mana reflection', 'If you tap a permanent for mana, it produces twice as much of that mana instead.').
card_mana_cost('mana reflection', ['4', 'G', 'G']).
card_cmc('mana reflection', 6).
card_layout('mana reflection', 'normal').

% Found in: UNH
card_name('mana screw', 'Mana Screw').
card_type('mana screw', 'Artifact').
card_types('mana screw', ['Artifact']).
card_subtypes('mana screw', []).
card_colors('mana screw', []).
card_text('mana screw', '{1}: Flip a coin. If you win the flip, add {2} to your mana pool. Play this ability only any time you could play an instant.').
card_mana_cost('mana screw', ['1']).
card_cmc('mana screw', 1).
card_layout('mana screw', 'normal').

% Found in: CHK
card_name('mana seism', 'Mana Seism').
card_type('mana seism', 'Sorcery').
card_types('mana seism', ['Sorcery']).
card_subtypes('mana seism', []).
card_colors('mana seism', ['R']).
card_text('mana seism', 'Sacrifice any number of lands. Add {1} to your mana pool for each land sacrificed this way.').
card_mana_cost('mana seism', ['1', 'R']).
card_cmc('mana seism', 2).
card_layout('mana seism', 'normal').

% Found in: TMP
card_name('mana severance', 'Mana Severance').
card_type('mana severance', 'Sorcery').
card_types('mana severance', ['Sorcery']).
card_subtypes('mana severance', []).
card_colors('mana severance', ['U']).
card_text('mana severance', 'Search your library for any number of land cards and exile them. Then shuffle your library.').
card_mana_cost('mana severance', ['1', 'U']).
card_cmc('mana severance', 2).
card_layout('mana severance', 'normal').

% Found in: 2ED, 3ED, 4ED, 6ED, 7ED, CED, CEI, LEA, LEB
card_name('mana short', 'Mana Short').
card_type('mana short', 'Instant').
card_types('mana short', ['Instant']).
card_subtypes('mana short', []).
card_colors('mana short', ['U']).
card_text('mana short', 'Tap all lands target player controls and empty his or her mana pool.').
card_mana_cost('mana short', ['2', 'U']).
card_cmc('mana short', 3).
card_layout('mana short', 'normal').

% Found in: TSP
card_name('mana skimmer', 'Mana Skimmer').
card_type('mana skimmer', 'Creature — Leech').
card_types('mana skimmer', ['Creature']).
card_subtypes('mana skimmer', ['Leech']).
card_colors('mana skimmer', ['B']).
card_text('mana skimmer', 'Flying\nWhenever Mana Skimmer deals damage to a player, tap target land that player controls. That land doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('mana skimmer', ['3', 'B']).
card_cmc('mana skimmer', 4).
card_layout('mana skimmer', 'normal').
card_power('mana skimmer', 2).
card_toughness('mana skimmer', 2).

% Found in: PLC, pMPR
card_name('mana tithe', 'Mana Tithe').
card_type('mana tithe', 'Instant').
card_types('mana tithe', ['Instant']).
card_subtypes('mana tithe', []).
card_colors('mana tithe', ['W']).
card_text('mana tithe', 'Counter target spell unless its controller pays {1}.').
card_mana_cost('mana tithe', ['W']).
card_cmc('mana tithe', 1).
card_layout('mana tithe', 'normal').

% Found in: PCY
card_name('mana vapors', 'Mana Vapors').
card_type('mana vapors', 'Sorcery').
card_types('mana vapors', ['Sorcery']).
card_subtypes('mana vapors', []).
card_colors('mana vapors', ['U']).
card_text('mana vapors', 'Lands target player controls don\'t untap during his or her next untap step.').
card_mana_cost('mana vapors', ['1', 'U']).
card_cmc('mana vapors', 2).
card_layout('mana vapors', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4, VMA
card_name('mana vault', 'Mana Vault').
card_type('mana vault', 'Artifact').
card_types('mana vault', ['Artifact']).
card_subtypes('mana vault', []).
card_colors('mana vault', []).
card_text('mana vault', 'Mana Vault doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {4}. If you do, untap Mana Vault.\nAt the beginning of your draw step, if Mana Vault is tapped, it deals 1 damage to you.\n{T}: Add {3} to your mana pool.').
card_mana_cost('mana vault', ['1']).
card_cmc('mana vault', 1).
card_layout('mana vault', 'normal').

% Found in: DRK, ME3
card_name('mana vortex', 'Mana Vortex').
card_type('mana vortex', 'Enchantment').
card_types('mana vortex', ['Enchantment']).
card_subtypes('mana vortex', []).
card_colors('mana vortex', ['U']).
card_text('mana vortex', 'When you cast Mana Vortex, counter it unless you sacrifice a land.\nAt the beginning of each player\'s upkeep, that player sacrifices a land.\nWhen there are no lands on the battlefield, sacrifice Mana Vortex.').
card_mana_cost('mana vortex', ['1', 'U', 'U']).
card_cmc('mana vortex', 3).
card_layout('mana vortex', 'normal').
card_reserved('mana vortex').

% Found in: WTH
card_name('mana web', 'Mana Web').
card_type('mana web', 'Artifact').
card_types('mana web', ['Artifact']).
card_subtypes('mana web', []).
card_colors('mana web', []).
card_text('mana web', 'Whenever a land an opponent controls is tapped for mana, tap all lands that player controls that could produce any type of mana that land could produce.').
card_mana_cost('mana web', ['3']).
card_cmc('mana web', 3).
card_layout('mana web', 'normal').
card_reserved('mana web').

% Found in: CMD
card_name('mana-charged dragon', 'Mana-Charged Dragon').
card_type('mana-charged dragon', 'Creature — Dragon').
card_types('mana-charged dragon', ['Creature']).
card_subtypes('mana-charged dragon', ['Dragon']).
card_colors('mana-charged dragon', ['R']).
card_text('mana-charged dragon', 'Flying, trample\nJoin forces — Whenever Mana-Charged Dragon attacks or blocks, each player starting with you may pay any amount of mana. Mana-Charged Dragon gets +X/+0 until end of turn, where X is the total amount of mana paid this way.').
card_mana_cost('mana-charged dragon', ['4', 'R', 'R']).
card_cmc('mana-charged dragon', 6).
card_layout('mana-charged dragon', 'normal').
card_power('mana-charged dragon', 5).
card_toughness('mana-charged dragon', 5).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, M10, M12
card_name('manabarbs', 'Manabarbs').
card_type('manabarbs', 'Enchantment').
card_types('manabarbs', ['Enchantment']).
card_subtypes('manabarbs', []).
card_colors('manabarbs', ['R']).
card_text('manabarbs', 'Whenever a player taps a land for mana, Manabarbs deals 1 damage to that player.').
card_mana_cost('manabarbs', ['3', 'R']).
card_cmc('manabarbs', 4).
card_layout('manabarbs', 'normal').

% Found in: EXO, TPR
card_name('manabond', 'Manabond').
card_type('manabond', 'Enchantment').
card_types('manabond', ['Enchantment']).
card_subtypes('manabond', []).
card_colors('manabond', ['G']).
card_text('manabond', 'At the beginning of your end step, you may reveal your hand and put all land cards from it onto the battlefield. If you do, discard your hand.').
card_mana_cost('manabond', ['G']).
card_cmc('manabond', 1).
card_layout('manabond', 'normal').

% Found in: APC
card_name('manacles of decay', 'Manacles of Decay').
card_type('manacles of decay', 'Enchantment — Aura').
card_types('manacles of decay', ['Enchantment']).
card_subtypes('manacles of decay', ['Aura']).
card_colors('manacles of decay', ['W']).
card_text('manacles of decay', 'Enchant creature\nEnchanted creature can\'t attack.\n{B}: Enchanted creature gets -1/-1 until end of turn.\n{R}: Enchanted creature can\'t block this turn.').
card_mana_cost('manacles of decay', ['1', 'W']).
card_cmc('manacles of decay', 2).
card_layout('manacles of decay', 'normal').

% Found in: CON
card_name('manaforce mace', 'Manaforce Mace').
card_type('manaforce mace', 'Artifact — Equipment').
card_types('manaforce mace', ['Artifact']).
card_subtypes('manaforce mace', ['Equipment']).
card_colors('manaforce mace', []).
card_text('manaforce mace', 'Domain — Equipped creature gets +1/+1 for each basic land type among lands you control.\nEquip {3}').
card_mana_cost('manaforce mace', ['4']).
card_cmc('manaforce mace', 4).
card_layout('manaforce mace', 'normal').

% Found in: SHM
card_name('manaforge cinder', 'Manaforge Cinder').
card_type('manaforge cinder', 'Creature — Elemental Shaman').
card_types('manaforge cinder', ['Creature']).
card_subtypes('manaforge cinder', ['Elemental', 'Shaman']).
card_colors('manaforge cinder', ['B', 'R']).
card_text('manaforge cinder', '{1}: Add {B} or {R} to your mana pool. Activate this ability no more than three times each turn.').
card_mana_cost('manaforge cinder', ['B/R']).
card_cmc('manaforge cinder', 1).
card_layout('manaforge cinder', 'normal').
card_power('manaforge cinder', 1).
card_toughness('manaforge cinder', 1).

% Found in: ORI
card_name('managorger hydra', 'Managorger Hydra').
card_type('managorger hydra', 'Creature — Hydra').
card_types('managorger hydra', ['Creature']).
card_subtypes('managorger hydra', ['Hydra']).
card_colors('managorger hydra', ['G']).
card_text('managorger hydra', 'Trample (This creature can deal excess combat damage to defending player or planeswalker while attacking.)\nWhenever a player casts a spell, put a +1/+1 counter on Managorger Hydra.').
card_mana_cost('managorger hydra', ['2', 'G']).
card_cmc('managorger hydra', 3).
card_layout('managorger hydra', 'normal').
card_power('managorger hydra', 1).
card_toughness('managorger hydra', 1).

% Found in: TMP
card_name('manakin', 'Manakin').
card_type('manakin', 'Artifact Creature — Construct').
card_types('manakin', ['Artifact', 'Creature']).
card_subtypes('manakin', ['Construct']).
card_colors('manakin', []).
card_text('manakin', '{T}: Add {1} to your mana pool.').
card_mana_cost('manakin', ['2']).
card_cmc('manakin', 2).
card_layout('manakin', 'normal').
card_power('manakin', 1).
card_toughness('manakin', 1).

% Found in: M12
card_name('manalith', 'Manalith').
card_type('manalith', 'Artifact').
card_types('manalith', ['Artifact']).
card_subtypes('manalith', []).
card_colors('manalith', []).
card_text('manalith', '{T}: Add one mana of any color to your mana pool.').
card_mana_cost('manalith', ['3']).
card_cmc('manalith', 3).
card_layout('manalith', 'normal').

% Found in: MMA, SHM
card_name('manamorphose', 'Manamorphose').
card_type('manamorphose', 'Instant').
card_types('manamorphose', ['Instant']).
card_subtypes('manamorphose', []).
card_colors('manamorphose', ['R', 'G']).
card_text('manamorphose', 'Add two mana in any combination of colors to your mana pool.\nDraw a card.').
card_mana_cost('manamorphose', ['1', 'R/G']).
card_cmc('manamorphose', 2).
card_layout('manamorphose', 'normal').

% Found in: ALA
card_name('manaplasm', 'Manaplasm').
card_type('manaplasm', 'Creature — Ooze').
card_types('manaplasm', ['Creature']).
card_subtypes('manaplasm', ['Ooze']).
card_colors('manaplasm', ['G']).
card_text('manaplasm', 'Whenever you cast a spell, Manaplasm gets +X/+X until end of turn, where X is that spell\'s converted mana cost.').
card_mana_cost('manaplasm', ['2', 'G']).
card_cmc('manaplasm', 3).
card_layout('manaplasm', 'normal').
card_power('manaplasm', 1).
card_toughness('manaplasm', 1).

% Found in: M14
card_name('manaweft sliver', 'Manaweft Sliver').
card_type('manaweft sliver', 'Creature — Sliver').
card_types('manaweft sliver', ['Creature']).
card_subtypes('manaweft sliver', ['Sliver']).
card_colors('manaweft sliver', ['G']).
card_text('manaweft sliver', 'Sliver creatures you control have \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('manaweft sliver', ['1', 'G']).
card_cmc('manaweft sliver', 2).
card_layout('manaweft sliver', 'normal').
card_power('manaweft sliver', 1).
card_toughness('manaweft sliver', 1).

% Found in: TSP
card_name('mangara of corondor', 'Mangara of Corondor').
card_type('mangara of corondor', 'Legendary Creature — Human Wizard').
card_types('mangara of corondor', ['Creature']).
card_subtypes('mangara of corondor', ['Human', 'Wizard']).
card_supertypes('mangara of corondor', ['Legendary']).
card_colors('mangara of corondor', ['W']).
card_text('mangara of corondor', '{T}: Exile Mangara of Corondor and target permanent.').
card_mana_cost('mangara of corondor', ['1', 'W', 'W']).
card_cmc('mangara of corondor', 3).
card_layout('mangara of corondor', 'normal').
card_power('mangara of corondor', 1).
card_toughness('mangara of corondor', 1).

% Found in: MIR
card_name('mangara\'s blessing', 'Mangara\'s Blessing').
card_type('mangara\'s blessing', 'Instant').
card_types('mangara\'s blessing', ['Instant']).
card_subtypes('mangara\'s blessing', []).
card_colors('mangara\'s blessing', ['W']).
card_text('mangara\'s blessing', 'You gain 5 life.\nWhen a spell or ability an opponent controls causes you to discard Mangara\'s Blessing, you gain 2 life, and you return Mangara\'s Blessing from your graveyard to your hand at the beginning of the next end step.').
card_mana_cost('mangara\'s blessing', ['2', 'W']).
card_cmc('mangara\'s blessing', 3).
card_layout('mangara\'s blessing', 'normal').

% Found in: MIR
card_name('mangara\'s equity', 'Mangara\'s Equity').
card_type('mangara\'s equity', 'Enchantment').
card_types('mangara\'s equity', ['Enchantment']).
card_subtypes('mangara\'s equity', []).
card_colors('mangara\'s equity', ['W']).
card_text('mangara\'s equity', 'As Mangara\'s Equity enters the battlefield, choose black or red.\nAt the beginning of your upkeep, sacrifice Mangara\'s Equity unless you pay {1}{W}.\nWhenever a creature of the chosen color deals damage to you or a white creature you control, Mangara\'s Equity deals that much damage to that creature.').
card_mana_cost('mangara\'s equity', ['1', 'W', 'W']).
card_cmc('mangara\'s equity', 3).
card_layout('mangara\'s equity', 'normal').

% Found in: MIR
card_name('mangara\'s tome', 'Mangara\'s Tome').
card_type('mangara\'s tome', 'Artifact').
card_types('mangara\'s tome', ['Artifact']).
card_subtypes('mangara\'s tome', []).
card_colors('mangara\'s tome', []).
card_text('mangara\'s tome', 'When Mangara\'s Tome enters the battlefield, search your library for five cards, exile them in a face-down pile, and shuffle that pile. Then shuffle your library.\n{2}: The next time you would draw a card this turn, instead put the top card of the exiled pile into its owner\'s hand.').
card_mana_cost('mangara\'s tome', ['5']).
card_cmc('mangara\'s tome', 5).
card_layout('mangara\'s tome', 'normal').
card_reserved('mangara\'s tome').

% Found in: BRB, CON, EXO, INV, TPR
card_name('maniacal rage', 'Maniacal Rage').
card_type('maniacal rage', 'Enchantment — Aura').
card_types('maniacal rage', ['Enchantment']).
card_subtypes('maniacal rage', ['Aura']).
card_colors('maniacal rage', ['R']).
card_text('maniacal rage', 'Enchant creature\nEnchanted creature gets +2/+2 and can\'t block.').
card_mana_cost('maniacal rage', ['1', 'R']).
card_cmc('maniacal rage', 2).
card_layout('maniacal rage', 'normal').

% Found in: M11, M12
card_name('manic vandal', 'Manic Vandal').
card_type('manic vandal', 'Creature — Human Warrior').
card_types('manic vandal', ['Creature']).
card_subtypes('manic vandal', ['Human', 'Warrior']).
card_colors('manic vandal', ['R']).
card_text('manic vandal', 'When Manic Vandal enters the battlefield, destroy target artifact.').
card_mana_cost('manic vandal', ['2', 'R']).
card_cmc('manic vandal', 3).
card_layout('manic vandal', 'normal').
card_power('manic vandal', 2).
card_toughness('manic vandal', 2).

% Found in: INV
card_name('manipulate fate', 'Manipulate Fate').
card_type('manipulate fate', 'Sorcery').
card_types('manipulate fate', ['Sorcery']).
card_subtypes('manipulate fate', []).
card_colors('manipulate fate', ['U']).
card_text('manipulate fate', 'Search your library for three cards, exile them, then shuffle your library.\nDraw a card.').
card_mana_cost('manipulate fate', ['1', 'U']).
card_cmc('manipulate fate', 2).
card_layout('manipulate fate', 'normal').

% Found in: BOK
card_name('mannichi, the fevered dream', 'Mannichi, the Fevered Dream').
card_type('mannichi, the fevered dream', 'Legendary Creature — Spirit').
card_types('mannichi, the fevered dream', ['Creature']).
card_subtypes('mannichi, the fevered dream', ['Spirit']).
card_supertypes('mannichi, the fevered dream', ['Legendary']).
card_colors('mannichi, the fevered dream', ['R']).
card_text('mannichi, the fevered dream', '{1}{R}: Switch each creature\'s power and toughness until end of turn.').
card_mana_cost('mannichi, the fevered dream', ['2', 'R']).
card_cmc('mannichi, the fevered dream', 3).
card_layout('mannichi, the fevered dream', 'normal').
card_power('mannichi, the fevered dream', 1).
card_toughness('mannichi, the fevered dream', 2).

% Found in: ISD
card_name('manor gargoyle', 'Manor Gargoyle').
card_type('manor gargoyle', 'Artifact Creature — Gargoyle').
card_types('manor gargoyle', ['Artifact', 'Creature']).
card_subtypes('manor gargoyle', ['Gargoyle']).
card_colors('manor gargoyle', []).
card_text('manor gargoyle', 'Defender\nManor Gargoyle has indestructible as long as it has defender.\n{1}: Until end of turn, Manor Gargoyle loses defender and gains flying.').
card_mana_cost('manor gargoyle', ['5']).
card_cmc('manor gargoyle', 5).
card_layout('manor gargoyle', 'normal').
card_power('manor gargoyle', 4).
card_toughness('manor gargoyle', 4).

% Found in: ISD
card_name('manor skeleton', 'Manor Skeleton').
card_type('manor skeleton', 'Creature — Skeleton').
card_types('manor skeleton', ['Creature']).
card_subtypes('manor skeleton', ['Skeleton']).
card_colors('manor skeleton', ['B']).
card_text('manor skeleton', 'Haste\n{1}{B}: Regenerate Manor Skeleton.').
card_mana_cost('manor skeleton', ['1', 'B']).
card_cmc('manor skeleton', 2).
card_layout('manor skeleton', 'normal').
card_power('manor skeleton', 1).
card_toughness('manor skeleton', 1).

% Found in: SOK
card_name('manriki-gusari', 'Manriki-Gusari').
card_type('manriki-gusari', 'Artifact — Equipment').
card_types('manriki-gusari', ['Artifact']).
card_subtypes('manriki-gusari', ['Equipment']).
card_colors('manriki-gusari', []).
card_text('manriki-gusari', 'Equipped creature gets +1/+2 and has \"{T}: Destroy target Equipment.\"\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('manriki-gusari', ['2']).
card_cmc('manriki-gusari', 2).
card_layout('manriki-gusari', 'normal').

% Found in: WTH
card_name('manta ray', 'Manta Ray').
card_type('manta ray', 'Creature — Fish').
card_types('manta ray', ['Creature']).
card_subtypes('manta ray', ['Fish']).
card_colors('manta ray', ['U']).
card_text('manta ray', 'Manta Ray can\'t attack unless defending player controls an Island.\nManta Ray can\'t be blocked except by blue creatures.\nWhen you control no Islands, sacrifice Manta Ray.').
card_mana_cost('manta ray', ['1', 'U', 'U']).
card_cmc('manta ray', 3).
card_layout('manta ray', 'normal').
card_power('manta ray', 3).
card_toughness('manta ray', 3).

% Found in: BRB, TMP
card_name('manta riders', 'Manta Riders').
card_type('manta riders', 'Creature — Merfolk').
card_types('manta riders', ['Creature']).
card_subtypes('manta riders', ['Merfolk']).
card_colors('manta riders', ['U']).
card_text('manta riders', '{U}: Manta Riders gains flying until end of turn.').
card_mana_cost('manta riders', ['U']).
card_cmc('manta riders', 1).
card_layout('manta riders', 'normal').
card_power('manta riders', 1).
card_toughness('manta riders', 1).

% Found in: 10E, UDS
card_name('mantis engine', 'Mantis Engine').
card_type('mantis engine', 'Artifact Creature — Insect').
card_types('mantis engine', ['Artifact', 'Creature']).
card_subtypes('mantis engine', ['Insect']).
card_colors('mantis engine', []).
card_text('mantis engine', '{2}: Mantis Engine gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)\n{2}: Mantis Engine gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_mana_cost('mantis engine', ['5']).
card_cmc('mantis engine', 5).
card_layout('mantis engine', 'normal').
card_power('mantis engine', 3).
card_toughness('mantis engine', 3).

% Found in: KTK
card_name('mantis rider', 'Mantis Rider').
card_type('mantis rider', 'Creature — Human Monk').
card_types('mantis rider', ['Creature']).
card_subtypes('mantis rider', ['Human', 'Monk']).
card_colors('mantis rider', ['W', 'U', 'R']).
card_text('mantis rider', 'Flying, vigilance, haste').
card_mana_cost('mantis rider', ['U', 'R', 'W']).
card_cmc('mantis rider', 3).
card_layout('mantis rider', 'normal').
card_power('mantis rider', 3).
card_toughness('mantis rider', 3).

% Found in: PLC
card_name('mantle of leadership', 'Mantle of Leadership').
card_type('mantle of leadership', 'Enchantment — Aura').
card_types('mantle of leadership', ['Enchantment']).
card_subtypes('mantle of leadership', ['Aura']).
card_colors('mantle of leadership', ['W']).
card_text('mantle of leadership', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nWhenever a creature enters the battlefield, enchanted creature gets +2/+2 until end of turn.').
card_mana_cost('mantle of leadership', ['1', 'W']).
card_cmc('mantle of leadership', 2).
card_layout('mantle of leadership', 'normal').

% Found in: ORI
card_name('mantle of webs', 'Mantle of Webs').
card_type('mantle of webs', 'Enchantment — Aura').
card_types('mantle of webs', ['Enchantment']).
card_subtypes('mantle of webs', ['Aura']).
card_colors('mantle of webs', ['G']).
card_text('mantle of webs', 'Enchant creature\nEnchanted creature gets +1/+3 and has reach. (It can block creatures with flying.)').
card_mana_cost('mantle of webs', ['1', 'G']).
card_cmc('mantle of webs', 2).
card_layout('mantle of webs', 'normal').

% Found in: FRF
card_name('map the wastes', 'Map the Wastes').
card_type('map the wastes', 'Sorcery').
card_types('map the wastes', ['Sorcery']).
card_subtypes('map the wastes', []).
card_colors('map the wastes', ['G']).
card_text('map the wastes', 'Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library. Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('map the wastes', ['2', 'G']).
card_cmc('map the wastes', 3).
card_layout('map the wastes', 'normal').

% Found in: MOR
card_name('maralen of the mornsong', 'Maralen of the Mornsong').
card_type('maralen of the mornsong', 'Legendary Creature — Elf Wizard').
card_types('maralen of the mornsong', ['Creature']).
card_subtypes('maralen of the mornsong', ['Elf', 'Wizard']).
card_supertypes('maralen of the mornsong', ['Legendary']).
card_colors('maralen of the mornsong', ['B']).
card_text('maralen of the mornsong', 'Players can\'t draw cards.\nAt the beginning of each player\'s draw step, that player loses 3 life, searches his or her library for a card, puts it into his or her hand, then shuffles his or her library.').
card_mana_cost('maralen of the mornsong', ['1', 'B', 'B']).
card_cmc('maralen of the mornsong', 3).
card_layout('maralen of the mornsong', 'normal').
card_power('maralen of the mornsong', 2).
card_toughness('maralen of the mornsong', 3).

% Found in: VAN
card_name('maralen of the mornsong avatar', 'Maralen of the Mornsong Avatar').
card_type('maralen of the mornsong avatar', 'Vanguard').
card_types('maralen of the mornsong avatar', ['Vanguard']).
card_subtypes('maralen of the mornsong avatar', []).
card_colors('maralen of the mornsong avatar', []).
card_text('maralen of the mornsong avatar', 'At the beginning of the game, you may pay any amount of life.\nYou can\'t draw cards.\nAt the beginning of your draw step, look at the top X cards of your library, where X is the amount of life paid with Maralen of the Mornsong Avatar. Put one of them into your hand, then shuffle your library.').
card_layout('maralen of the mornsong avatar', 'vanguard').

% Found in: FRF
card_name('marang river prowler', 'Marang River Prowler').
card_type('marang river prowler', 'Creature — Human Rogue').
card_types('marang river prowler', ['Creature']).
card_subtypes('marang river prowler', ['Human', 'Rogue']).
card_colors('marang river prowler', ['U']).
card_text('marang river prowler', 'Marang River Prowler can\'t block and can\'t be blocked.\nYou may cast Marang River Prowler from your graveyard as long as you control a black or green permanent.').
card_mana_cost('marang river prowler', ['2', 'U']).
card_cmc('marang river prowler', 3).
card_layout('marang river prowler', 'normal').
card_power('marang river prowler', 2).
card_toughness('marang river prowler', 1).

% Found in: DTK
card_name('marang river skeleton', 'Marang River Skeleton').
card_type('marang river skeleton', 'Creature — Skeleton').
card_types('marang river skeleton', ['Creature']).
card_subtypes('marang river skeleton', ['Skeleton']).
card_colors('marang river skeleton', ['B']).
card_text('marang river skeleton', '{B}: Regenerate Marang River Skeleton.\nMegamorph {3}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('marang river skeleton', ['1', 'B']).
card_cmc('marang river skeleton', 2).
card_layout('marang river skeleton', 'normal').
card_power('marang river skeleton', 1).
card_toughness('marang river skeleton', 1).

% Found in: C13
card_name('marath, will of the wild', 'Marath, Will of the Wild').
card_type('marath, will of the wild', 'Legendary Creature — Elemental Beast').
card_types('marath, will of the wild', ['Creature']).
card_subtypes('marath, will of the wild', ['Elemental', 'Beast']).
card_supertypes('marath, will of the wild', ['Legendary']).
card_colors('marath, will of the wild', ['W', 'R', 'G']).
card_text('marath, will of the wild', 'Marath, Will of the Wild enters the battlefield with a number of +1/+1 counters on it equal to the amount of mana spent to cast it.\n{X}, Remove X +1/+1 counters from Marath: Choose one —\n• Put X +1/+1 counters on target creature. X can\'t be 0.\n• Marath deals X damage to target creature or player. X can\'t be 0.\n• Put an X/X green Elemental creature token onto the battlefield. X can\'t be 0.').
card_mana_cost('marath, will of the wild', ['R', 'G', 'W']).
card_cmc('marath, will of the wild', 3).
card_layout('marath, will of the wild', 'normal').
card_power('marath, will of the wild', 0).
card_toughness('marath, will of the wild', 0).

% Found in: INV
card_name('marauding knight', 'Marauding Knight').
card_type('marauding knight', 'Creature — Zombie Knight').
card_types('marauding knight', ['Creature']).
card_subtypes('marauding knight', ['Zombie', 'Knight']).
card_colors('marauding knight', ['B']).
card_text('marauding knight', 'Protection from white\nMarauding Knight gets +1/+1 for each Plains your opponents control.').
card_mana_cost('marauding knight', ['2', 'B', 'B']).
card_cmc('marauding knight', 4).
card_layout('marauding knight', 'normal').
card_power('marauding knight', 2).
card_toughness('marauding knight', 2).

% Found in: M14
card_name('marauding maulhorn', 'Marauding Maulhorn').
card_type('marauding maulhorn', 'Creature — Beast').
card_types('marauding maulhorn', ['Creature']).
card_subtypes('marauding maulhorn', ['Beast']).
card_colors('marauding maulhorn', ['R']).
card_text('marauding maulhorn', 'Marauding Maulhorn attacks each combat if able unless you control a creature named Advocate of the Beast.').
card_mana_cost('marauding maulhorn', ['2', 'R', 'R']).
card_cmc('marauding maulhorn', 4).
card_layout('marauding maulhorn', 'normal').
card_power('marauding maulhorn', 5).
card_toughness('marauding maulhorn', 3).

% Found in: VAN
card_name('maraxus', 'Maraxus').
card_type('maraxus', 'Vanguard').
card_types('maraxus', ['Vanguard']).
card_subtypes('maraxus', []).
card_colors('maraxus', []).
card_text('maraxus', 'Creatures you control get +1/+0.').
card_layout('maraxus', 'vanguard').

% Found in: WTH
card_name('maraxus of keld', 'Maraxus of Keld').
card_type('maraxus of keld', 'Legendary Creature — Human Warrior').
card_types('maraxus of keld', ['Creature']).
card_subtypes('maraxus of keld', ['Human', 'Warrior']).
card_supertypes('maraxus of keld', ['Legendary']).
card_colors('maraxus of keld', ['R']).
card_text('maraxus of keld', 'Maraxus of Keld\'s power and toughness are each equal to the number of untapped artifacts, creatures, and lands you control.').
card_mana_cost('maraxus of keld', ['4', 'R', 'R']).
card_cmc('maraxus of keld', 6).
card_layout('maraxus of keld', 'normal').
card_power('maraxus of keld', '*').
card_toughness('maraxus of keld', '*').
card_reserved('maraxus of keld').

% Found in: ALA
card_name('marble chalice', 'Marble Chalice').
card_type('marble chalice', 'Artifact').
card_types('marble chalice', ['Artifact']).
card_subtypes('marble chalice', []).
card_colors('marble chalice', ['W']).
card_text('marble chalice', '{T}: You gain 1 life.').
card_mana_cost('marble chalice', ['2', 'W']).
card_cmc('marble chalice', 3).
card_layout('marble chalice', 'normal').

% Found in: 6ED, 7ED, C14, DD3_DVD, DDC, MIR
card_name('marble diamond', 'Marble Diamond').
card_type('marble diamond', 'Artifact').
card_types('marble diamond', ['Artifact']).
card_subtypes('marble diamond', []).
card_colors('marble diamond', []).
card_text('marble diamond', 'Marble Diamond enters the battlefield tapped.\n{T}: Add {W} to your mana pool.').
card_mana_cost('marble diamond', ['2']).
card_cmc('marble diamond', 2).
card_layout('marble diamond', 'normal').

% Found in: LEG
card_name('marble priest', 'Marble Priest').
card_type('marble priest', 'Artifact Creature — Cleric').
card_types('marble priest', ['Artifact', 'Creature']).
card_subtypes('marble priest', ['Cleric']).
card_colors('marble priest', []).
card_text('marble priest', 'All Walls able to block Marble Priest do so.\nPrevent all combat damage that would be dealt to Marble Priest by Walls.').
card_mana_cost('marble priest', ['5']).
card_cmc('marble priest', 5).
card_layout('marble priest', 'normal').
card_power('marble priest', 3).
card_toughness('marble priest', 3).

% Found in: 9ED, TMP
card_name('marble titan', 'Marble Titan').
card_type('marble titan', 'Creature — Giant').
card_types('marble titan', ['Creature']).
card_subtypes('marble titan', ['Giant']).
card_colors('marble titan', ['W']).
card_text('marble titan', 'Creatures with power 3 or greater don\'t untap during their controllers\' untap steps.').
card_mana_cost('marble titan', ['3', 'W']).
card_cmc('marble titan', 4).
card_layout('marble titan', 'normal').
card_power('marble titan', 3).
card_toughness('marble titan', 3).

% Found in: BFZ
card_name('march from the tomb', 'March from the Tomb').
card_type('march from the tomb', 'Sorcery').
card_types('march from the tomb', ['Sorcery']).
card_subtypes('march from the tomb', []).
card_colors('march from the tomb', ['W', 'B']).
card_text('march from the tomb', 'Return any number of target Ally creature cards with total converted mana cost 8 or less from your graveyard to the battlefield.').
card_mana_cost('march from the tomb', ['3', 'W', 'B']).
card_cmc('march from the tomb', 5).
card_layout('march from the tomb', 'normal').

% Found in: PLS
card_name('march of souls', 'March of Souls').
card_type('march of souls', 'Sorcery').
card_types('march of souls', ['Sorcery']).
card_subtypes('march of souls', []).
card_colors('march of souls', ['W']).
card_text('march of souls', 'Destroy all creatures. They can\'t be regenerated. For each creature destroyed this way, its controller puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('march of souls', ['4', 'W']).
card_cmc('march of souls', 5).
card_layout('march of souls', 'normal').

% Found in: 10E, ARC, MRD
card_name('march of the machines', 'March of the Machines').
card_type('march of the machines', 'Enchantment').
card_types('march of the machines', ['Enchantment']).
card_subtypes('march of the machines', []).
card_colors('march of the machines', ['U']).
card_text('march of the machines', 'Each noncreature artifact is an artifact creature with power and toughness each equal to its converted mana cost. (Equipment that\'s a creature can\'t equip a creature.)').
card_mana_cost('march of the machines', ['3', 'U']).
card_cmc('march of the machines', 4).
card_layout('march of the machines', 'normal').

% Found in: THS
card_name('march of the returned', 'March of the Returned').
card_type('march of the returned', 'Sorcery').
card_types('march of the returned', ['Sorcery']).
card_subtypes('march of the returned', []).
card_colors('march of the returned', ['B']).
card_text('march of the returned', 'Return up to two target creature cards from your graveyard to your hand.').
card_mana_cost('march of the returned', ['3', 'B']).
card_cmc('march of the returned', 4).
card_layout('march of the returned', 'normal').

% Found in: CNS
card_name('marchesa\'s emissary', 'Marchesa\'s Emissary').
card_type('marchesa\'s emissary', 'Creature — Human Rogue').
card_types('marchesa\'s emissary', ['Creature']).
card_subtypes('marchesa\'s emissary', ['Human', 'Rogue']).
card_colors('marchesa\'s emissary', ['U']).
card_text('marchesa\'s emissary', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nDethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_mana_cost('marchesa\'s emissary', ['3', 'U']).
card_cmc('marchesa\'s emissary', 4).
card_layout('marchesa\'s emissary', 'normal').
card_power('marchesa\'s emissary', 2).
card_toughness('marchesa\'s emissary', 2).

% Found in: CNS
card_name('marchesa\'s infiltrator', 'Marchesa\'s Infiltrator').
card_type('marchesa\'s infiltrator', 'Creature — Human Rogue').
card_types('marchesa\'s infiltrator', ['Creature']).
card_subtypes('marchesa\'s infiltrator', ['Human', 'Rogue']).
card_colors('marchesa\'s infiltrator', ['U']).
card_text('marchesa\'s infiltrator', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nWhenever Marchesa\'s Infiltrator deals combat damage to a player, draw a card.').
card_mana_cost('marchesa\'s infiltrator', ['2', 'U']).
card_cmc('marchesa\'s infiltrator', 3).
card_layout('marchesa\'s infiltrator', 'normal').
card_power('marchesa\'s infiltrator', 1).
card_toughness('marchesa\'s infiltrator', 1).

% Found in: CNS
card_name('marchesa\'s smuggler', 'Marchesa\'s Smuggler').
card_type('marchesa\'s smuggler', 'Creature — Human Rogue').
card_types('marchesa\'s smuggler', ['Creature']).
card_subtypes('marchesa\'s smuggler', ['Human', 'Rogue']).
card_colors('marchesa\'s smuggler', ['U', 'R']).
card_text('marchesa\'s smuggler', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\n{1}{U}{R}: Target creature you control gains haste until end of turn and can\'t be blocked this turn.').
card_mana_cost('marchesa\'s smuggler', ['U', 'R']).
card_cmc('marchesa\'s smuggler', 2).
card_layout('marchesa\'s smuggler', 'normal').
card_power('marchesa\'s smuggler', 1).
card_toughness('marchesa\'s smuggler', 1).

% Found in: CNS, VMA
card_name('marchesa, the black rose', 'Marchesa, the Black Rose').
card_type('marchesa, the black rose', 'Legendary Creature — Human Wizard').
card_types('marchesa, the black rose', ['Creature']).
card_subtypes('marchesa, the black rose', ['Human', 'Wizard']).
card_supertypes('marchesa, the black rose', ['Legendary']).
card_colors('marchesa, the black rose', ['U', 'B', 'R']).
card_text('marchesa, the black rose', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nOther creatures you control have dethrone.\nWhenever a creature you control with a +1/+1 counter on it dies, return that card to the battlefield under your control at the beginning of the next end step.').
card_mana_cost('marchesa, the black rose', ['1', 'U', 'B', 'R']).
card_cmc('marchesa, the black rose', 4).
card_layout('marchesa, the black rose', 'normal').
card_power('marchesa, the black rose', 3).
card_toughness('marchesa, the black rose', 3).

% Found in: KTK, pPRE
card_name('mardu ascendancy', 'Mardu Ascendancy').
card_type('mardu ascendancy', 'Enchantment').
card_types('mardu ascendancy', ['Enchantment']).
card_subtypes('mardu ascendancy', []).
card_colors('mardu ascendancy', ['W', 'B', 'R']).
card_text('mardu ascendancy', 'Whenever a nontoken creature you control attacks, put a 1/1 red Goblin creature token onto the battlefield tapped and attacking.\nSacrifice Mardu Ascendancy: Creatures you control get +0/+3 until end of turn.').
card_mana_cost('mardu ascendancy', ['R', 'W', 'B']).
card_cmc('mardu ascendancy', 3).
card_layout('mardu ascendancy', 'normal').

% Found in: KTK
card_name('mardu banner', 'Mardu Banner').
card_type('mardu banner', 'Artifact').
card_types('mardu banner', ['Artifact']).
card_subtypes('mardu banner', []).
card_colors('mardu banner', []).
card_text('mardu banner', '{T}: Add {R}, {W}, or {B} to your mana pool.\n{R}{W}{B}, {T}, Sacrifice Mardu Banner: Draw a card.').
card_mana_cost('mardu banner', ['3']).
card_cmc('mardu banner', 3).
card_layout('mardu banner', 'normal').

% Found in: KTK
card_name('mardu blazebringer', 'Mardu Blazebringer').
card_type('mardu blazebringer', 'Creature — Ogre Warrior').
card_types('mardu blazebringer', ['Creature']).
card_subtypes('mardu blazebringer', ['Ogre', 'Warrior']).
card_colors('mardu blazebringer', ['R']).
card_text('mardu blazebringer', 'When Mardu Blazebringer attacks or blocks, sacrifice it at end of combat.').
card_mana_cost('mardu blazebringer', ['2', 'R']).
card_cmc('mardu blazebringer', 3).
card_layout('mardu blazebringer', 'normal').
card_power('mardu blazebringer', 4).
card_toughness('mardu blazebringer', 4).

% Found in: KTK
card_name('mardu charm', 'Mardu Charm').
card_type('mardu charm', 'Instant').
card_types('mardu charm', ['Instant']).
card_subtypes('mardu charm', []).
card_colors('mardu charm', ['W', 'B', 'R']).
card_text('mardu charm', 'Choose one —\n• Mardu Charm deals 4 damage to target creature.\n• Put two 1/1 white Warrior creature tokens onto the battlefield. They gain first strike until end of turn.\n• Target opponent reveals his or her hand. You choose a noncreature, nonland card from it. That player discards that card.').
card_mana_cost('mardu charm', ['R', 'W', 'B']).
card_cmc('mardu charm', 3).
card_layout('mardu charm', 'normal').

% Found in: KTK
card_name('mardu hateblade', 'Mardu Hateblade').
card_type('mardu hateblade', 'Creature — Human Warrior').
card_types('mardu hateblade', ['Creature']).
card_subtypes('mardu hateblade', ['Human', 'Warrior']).
card_colors('mardu hateblade', ['W']).
card_text('mardu hateblade', '{B}: Mardu Hateblade gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('mardu hateblade', ['W']).
card_cmc('mardu hateblade', 1).
card_layout('mardu hateblade', 'normal').
card_power('mardu hateblade', 1).
card_toughness('mardu hateblade', 1).

% Found in: DDN, KTK
card_name('mardu heart-piercer', 'Mardu Heart-Piercer').
card_type('mardu heart-piercer', 'Creature — Human Archer').
card_types('mardu heart-piercer', ['Creature']).
card_subtypes('mardu heart-piercer', ['Human', 'Archer']).
card_colors('mardu heart-piercer', ['R']).
card_text('mardu heart-piercer', 'Raid — When Mardu Heart-Piercer enters the battlefield, if you attacked with a creature this turn, Mardu Heart-Piercer deals 2 damage to target creature or player.').
card_mana_cost('mardu heart-piercer', ['3', 'R']).
card_cmc('mardu heart-piercer', 4).
card_layout('mardu heart-piercer', 'normal').
card_power('mardu heart-piercer', 2).
card_toughness('mardu heart-piercer', 3).

% Found in: KTK
card_name('mardu hordechief', 'Mardu Hordechief').
card_type('mardu hordechief', 'Creature — Human Warrior').
card_types('mardu hordechief', ['Creature']).
card_subtypes('mardu hordechief', ['Human', 'Warrior']).
card_colors('mardu hordechief', ['W']).
card_text('mardu hordechief', 'Raid — When Mardu Hordechief enters the battlefield, if you attacked with a creature this turn, put a 1/1 white Warrior creature token onto the battlefield.').
card_mana_cost('mardu hordechief', ['2', 'W']).
card_cmc('mardu hordechief', 3).
card_layout('mardu hordechief', 'normal').
card_power('mardu hordechief', 2).
card_toughness('mardu hordechief', 3).

% Found in: KTK
card_name('mardu roughrider', 'Mardu Roughrider').
card_type('mardu roughrider', 'Creature — Orc Warrior').
card_types('mardu roughrider', ['Creature']).
card_subtypes('mardu roughrider', ['Orc', 'Warrior']).
card_colors('mardu roughrider', ['W', 'B', 'R']).
card_text('mardu roughrider', 'Whenever Mardu Roughrider attacks, target creature can\'t block this turn.').
card_mana_cost('mardu roughrider', ['2', 'R', 'W', 'B']).
card_cmc('mardu roughrider', 5).
card_layout('mardu roughrider', 'normal').
card_power('mardu roughrider', 5).
card_toughness('mardu roughrider', 4).

% Found in: FRF
card_name('mardu runemark', 'Mardu Runemark').
card_type('mardu runemark', 'Enchantment — Aura').
card_types('mardu runemark', ['Enchantment']).
card_subtypes('mardu runemark', ['Aura']).
card_colors('mardu runemark', ['R']).
card_text('mardu runemark', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has first strike as long as you control a white or black permanent.').
card_mana_cost('mardu runemark', ['2', 'R']).
card_cmc('mardu runemark', 3).
card_layout('mardu runemark', 'normal').

% Found in: FRF
card_name('mardu scout', 'Mardu Scout').
card_type('mardu scout', 'Creature — Goblin Scout').
card_types('mardu scout', ['Creature']).
card_subtypes('mardu scout', ['Goblin', 'Scout']).
card_colors('mardu scout', ['R']).
card_text('mardu scout', 'Dash {1}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('mardu scout', ['R', 'R']).
card_cmc('mardu scout', 2).
card_layout('mardu scout', 'normal').
card_power('mardu scout', 3).
card_toughness('mardu scout', 1).

% Found in: FRF, pMGD
card_name('mardu shadowspear', 'Mardu Shadowspear').
card_type('mardu shadowspear', 'Creature — Human Warrior').
card_types('mardu shadowspear', ['Creature']).
card_subtypes('mardu shadowspear', ['Human', 'Warrior']).
card_colors('mardu shadowspear', ['B']).
card_text('mardu shadowspear', 'Whenever Mardu Shadowspear attacks, each opponent loses 1 life.\nDash {1}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('mardu shadowspear', ['B']).
card_cmc('mardu shadowspear', 1).
card_layout('mardu shadowspear', 'normal').
card_power('mardu shadowspear', 1).
card_toughness('mardu shadowspear', 1).

% Found in: KTK
card_name('mardu skullhunter', 'Mardu Skullhunter').
card_type('mardu skullhunter', 'Creature — Human Warrior').
card_types('mardu skullhunter', ['Creature']).
card_subtypes('mardu skullhunter', ['Human', 'Warrior']).
card_colors('mardu skullhunter', ['B']).
card_text('mardu skullhunter', 'Mardu Skullhunter enters the battlefield tapped.\nRaid — When Mardu Skullhunter enters the battlefield, if you attacked with a creature this turn, target opponent discards a card.').
card_mana_cost('mardu skullhunter', ['1', 'B']).
card_cmc('mardu skullhunter', 2).
card_layout('mardu skullhunter', 'normal').
card_power('mardu skullhunter', 2).
card_toughness('mardu skullhunter', 1).

% Found in: FRF
card_name('mardu strike leader', 'Mardu Strike Leader').
card_type('mardu strike leader', 'Creature — Human Warrior').
card_types('mardu strike leader', ['Creature']).
card_subtypes('mardu strike leader', ['Human', 'Warrior']).
card_colors('mardu strike leader', ['B']).
card_text('mardu strike leader', 'Whenever Mardu Strike Leader attacks, put a 2/1 black Warrior creature token onto the battlefield.\nDash {3}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('mardu strike leader', ['2', 'B']).
card_cmc('mardu strike leader', 3).
card_layout('mardu strike leader', 'normal').
card_power('mardu strike leader', 3).
card_toughness('mardu strike leader', 2).

% Found in: KTK
card_name('mardu warshrieker', 'Mardu Warshrieker').
card_type('mardu warshrieker', 'Creature — Orc Shaman').
card_types('mardu warshrieker', ['Creature']).
card_subtypes('mardu warshrieker', ['Orc', 'Shaman']).
card_colors('mardu warshrieker', ['R']).
card_text('mardu warshrieker', 'Raid — When Mardu Warshrieker enters the battlefield, if you attacked with a creature this turn, add {R}{W}{B} to your mana pool.').
card_mana_cost('mardu warshrieker', ['3', 'R']).
card_cmc('mardu warshrieker', 4).
card_layout('mardu warshrieker', 'normal').
card_power('mardu warshrieker', 3).
card_toughness('mardu warshrieker', 3).

% Found in: FRF
card_name('mardu woe-reaper', 'Mardu Woe-Reaper').
card_type('mardu woe-reaper', 'Creature — Human Warrior').
card_types('mardu woe-reaper', ['Creature']).
card_subtypes('mardu woe-reaper', ['Human', 'Warrior']).
card_colors('mardu woe-reaper', ['W']).
card_text('mardu woe-reaper', 'Whenever Mardu Woe-Reaper or another Warrior enters the battlefield under your control, you may exile target creature card from a graveyard. If you do, you gain 1 life.').
card_mana_cost('mardu woe-reaper', ['W']).
card_cmc('mardu woe-reaper', 1).
card_layout('mardu woe-reaper', 'normal').
card_power('mardu woe-reaper', 2).
card_toughness('mardu woe-reaper', 1).

% Found in: CHR, LEG, ME3
card_name('marhault elsdragon', 'Marhault Elsdragon').
card_type('marhault elsdragon', 'Legendary Creature — Elf Warrior').
card_types('marhault elsdragon', ['Creature']).
card_subtypes('marhault elsdragon', ['Elf', 'Warrior']).
card_supertypes('marhault elsdragon', ['Legendary']).
card_colors('marhault elsdragon', ['R', 'G']).
card_text('marhault elsdragon', 'Rampage 1 (Whenever this creature becomes blocked, it gets +1/+1 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('marhault elsdragon', ['3', 'R', 'R', 'G']).
card_cmc('marhault elsdragon', 6).
card_layout('marhault elsdragon', 'normal').
card_power('marhault elsdragon', 4).
card_toughness('marhault elsdragon', 6).

% Found in: ARB, DDH, pWPN
card_name('marisi\'s twinclaws', 'Marisi\'s Twinclaws').
card_type('marisi\'s twinclaws', 'Creature — Cat Warrior').
card_types('marisi\'s twinclaws', ['Creature']).
card_subtypes('marisi\'s twinclaws', ['Cat', 'Warrior']).
card_colors('marisi\'s twinclaws', ['W', 'R', 'G']).
card_text('marisi\'s twinclaws', 'Double strike').
card_mana_cost('marisi\'s twinclaws', ['2', 'R/W', 'G']).
card_cmc('marisi\'s twinclaws', 4).
card_layout('marisi\'s twinclaws', 'normal').
card_power('marisi\'s twinclaws', 2).
card_toughness('marisi\'s twinclaws', 4).

% Found in: M11, ORI
card_name('maritime guard', 'Maritime Guard').
card_type('maritime guard', 'Creature — Merfolk Soldier').
card_types('maritime guard', ['Creature']).
card_subtypes('maritime guard', ['Merfolk', 'Soldier']).
card_colors('maritime guard', ['U']).
card_text('maritime guard', '').
card_mana_cost('maritime guard', ['1', 'U']).
card_cmc('maritime guard', 2).
card_layout('maritime guard', 'normal').
card_power('maritime guard', 1).
card_toughness('maritime guard', 3).

% Found in: HML, ME2
card_name('marjhan', 'Marjhan').
card_type('marjhan', 'Creature — Leviathan').
card_types('marjhan', ['Creature']).
card_subtypes('marjhan', ['Leviathan']).
card_colors('marjhan', ['U']).
card_text('marjhan', 'Marjhan doesn\'t untap during your untap step.\n{U}{U}, Sacrifice a creature: Untap Marjhan. Activate this ability only during your upkeep.\nMarjhan can\'t attack unless defending player controls an Island.\n{U}{U}: Marjhan gets -1/-0 until end of turn and deals 1 damage to target attacking creature without flying.\nWhen you control no Islands, sacrifice Marjhan.').
card_mana_cost('marjhan', ['5', 'U', 'U']).
card_cmc('marjhan', 7).
card_layout('marjhan', 'normal').
card_power('marjhan', 8).
card_toughness('marjhan', 8).
card_reserved('marjhan').

% Found in: GTC
card_name('mark for death', 'Mark for Death').
card_type('mark for death', 'Sorcery').
card_types('mark for death', ['Sorcery']).
card_subtypes('mark for death', []).
card_colors('mark for death', ['R']).
card_text('mark for death', 'Target creature an opponent controls blocks this turn if able. Untap that creature. Other creatures that player controls can\'t block this turn.').
card_mana_cost('mark for death', ['3', 'R']).
card_cmc('mark for death', 4).
card_layout('mark for death', 'normal').

% Found in: CON
card_name('mark of asylum', 'Mark of Asylum').
card_type('mark of asylum', 'Enchantment').
card_types('mark of asylum', ['Enchantment']).
card_subtypes('mark of asylum', []).
card_colors('mark of asylum', ['W']).
card_text('mark of asylum', 'Prevent all noncombat damage that would be dealt to creatures you control.').
card_mana_cost('mark of asylum', ['1', 'W']).
card_cmc('mark of asylum', 2).
card_layout('mark of asylum', 'normal').

% Found in: RAV
card_name('mark of eviction', 'Mark of Eviction').
card_type('mark of eviction', 'Enchantment — Aura').
card_types('mark of eviction', ['Enchantment']).
card_subtypes('mark of eviction', ['Aura']).
card_colors('mark of eviction', ['U']).
card_text('mark of eviction', 'Enchant creature\nAt the beginning of your upkeep, return enchanted creature and all Auras attached to that creature to their owners\' hands.').
card_mana_cost('mark of eviction', ['U']).
card_cmc('mark of eviction', 1).
card_layout('mark of eviction', 'normal').

% Found in: UDS
card_name('mark of fury', 'Mark of Fury').
card_type('mark of fury', 'Enchantment — Aura').
card_types('mark of fury', ['Enchantment']).
card_subtypes('mark of fury', ['Aura']).
card_colors('mark of fury', ['R']).
card_text('mark of fury', 'Enchant creature\nEnchanted creature has haste.\nAt the beginning of the end step, return Mark of Fury to its owner\'s hand.').
card_mana_cost('mark of fury', ['R']).
card_cmc('mark of fury', 1).
card_layout('mark of fury', 'normal').

% Found in: M13, PC2, ZEN
card_name('mark of mutiny', 'Mark of Mutiny').
card_type('mark of mutiny', 'Sorcery').
card_types('mark of mutiny', ['Sorcery']).
card_subtypes('mark of mutiny', []).
card_colors('mark of mutiny', ['R']).
card_text('mark of mutiny', 'Gain control of target creature until end of turn. Put a +1/+1 counter on it and untap it. That creature gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('mark of mutiny', ['2', 'R']).
card_cmc('mark of mutiny', 3).
card_layout('mark of mutiny', 'normal').

% Found in: BOK
card_name('mark of sakiko', 'Mark of Sakiko').
card_type('mark of sakiko', 'Enchantment — Aura').
card_types('mark of sakiko', ['Enchantment']).
card_subtypes('mark of sakiko', ['Aura']).
card_colors('mark of sakiko', ['G']).
card_text('mark of sakiko', 'Enchant creature\nEnchanted creature has \"Whenever this creature deals combat damage to a player, add that much {G} to your mana pool. Until end of turn, this mana doesn\'t empty from your mana pool as steps and phases end.\"').
card_mana_cost('mark of sakiko', ['1', 'G']).
card_cmc('mark of sakiko', 2).
card_layout('mark of sakiko', 'normal').

% Found in: BOK
card_name('mark of the oni', 'Mark of the Oni').
card_type('mark of the oni', 'Enchantment — Aura').
card_types('mark of the oni', ['Enchantment']).
card_subtypes('mark of the oni', ['Aura']).
card_colors('mark of the oni', ['B']).
card_text('mark of the oni', 'Enchant creature\nYou control enchanted creature.\nAt the beginning of the end step, if you control no Demons, sacrifice Mark of the Oni.').
card_mana_cost('mark of the oni', ['2', 'B']).
card_cmc('mark of the oni', 3).
card_layout('mark of the oni', 'normal').

% Found in: DDK, M13, M14
card_name('mark of the vampire', 'Mark of the Vampire').
card_type('mark of the vampire', 'Enchantment — Aura').
card_types('mark of the vampire', ['Enchantment']).
card_subtypes('mark of the vampire', ['Aura']).
card_colors('mark of the vampire', ['B']).
card_text('mark of the vampire', 'Enchant creature\nEnchanted creature gets +2/+2 and has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('mark of the vampire', ['3', 'B']).
card_cmc('mark of the vampire', 4).
card_layout('mark of the vampire', 'normal').

% Found in: M15
card_name('marked by honor', 'Marked by Honor').
card_type('marked by honor', 'Enchantment — Aura').
card_types('marked by honor', ['Enchantment']).
card_subtypes('marked by honor', ['Aura']).
card_colors('marked by honor', ['W']).
card_text('marked by honor', 'Enchant creature\nEnchanted creature gets +2/+2 and has vigilance. (Attacking doesn\'t cause it to tap.)').
card_mana_cost('marked by honor', ['3', 'W']).
card_cmc('marked by honor', 4).
card_layout('marked by honor', 'normal').

% Found in: UDS
card_name('marker beetles', 'Marker Beetles').
card_type('marker beetles', 'Creature — Insect').
card_types('marker beetles', ['Creature']).
card_subtypes('marker beetles', ['Insect']).
card_colors('marker beetles', ['G']).
card_text('marker beetles', 'When Marker Beetles dies, target creature gets +1/+1 until end of turn.\n{2}, Sacrifice Marker Beetles: Draw a card.').
card_mana_cost('marker beetles', ['1', 'G', 'G']).
card_cmc('marker beetles', 3).
card_layout('marker beetles', 'normal').
card_power('marker beetles', 2).
card_toughness('marker beetles', 3).

% Found in: JOU
card_name('market festival', 'Market Festival').
card_type('market festival', 'Enchantment — Aura').
card_types('market festival', ['Enchantment']).
card_subtypes('market festival', ['Aura']).
card_colors('market festival', ['G']).
card_text('market festival', 'Enchant land\nWhenever enchanted land is tapped for mana, its controller adds two mana in any combination of colors to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('market festival', ['3', 'G']).
card_cmc('market festival', 4).
card_layout('market festival', 'normal').

% Found in: DKA
card_name('markov blademaster', 'Markov Blademaster').
card_type('markov blademaster', 'Creature — Vampire Warrior').
card_types('markov blademaster', ['Creature']).
card_subtypes('markov blademaster', ['Vampire', 'Warrior']).
card_colors('markov blademaster', ['R']).
card_text('markov blademaster', 'Double strike\nWhenever Markov Blademaster deals combat damage to a player, put a +1/+1 counter on it.').
card_mana_cost('markov blademaster', ['1', 'R', 'R']).
card_cmc('markov blademaster', 3).
card_layout('markov blademaster', 'normal').
card_power('markov blademaster', 1).
card_toughness('markov blademaster', 1).

% Found in: ISD
card_name('markov patrician', 'Markov Patrician').
card_type('markov patrician', 'Creature — Vampire').
card_types('markov patrician', ['Creature']).
card_subtypes('markov patrician', ['Vampire']).
card_colors('markov patrician', ['B']).
card_text('markov patrician', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('markov patrician', ['2', 'B']).
card_cmc('markov patrician', 3).
card_layout('markov patrician', 'normal').
card_power('markov patrician', 3).
card_toughness('markov patrician', 1).

% Found in: DKA
card_name('markov warlord', 'Markov Warlord').
card_type('markov warlord', 'Creature — Vampire Warrior').
card_types('markov warlord', ['Creature']).
card_subtypes('markov warlord', ['Vampire', 'Warrior']).
card_colors('markov warlord', ['R']).
card_text('markov warlord', 'Haste\nWhen Markov Warlord enters the battlefield, up to two target creatures can\'t block this turn.').
card_mana_cost('markov warlord', ['5', 'R']).
card_cmc('markov warlord', 6).
card_layout('markov warlord', 'normal').
card_power('markov warlord', 4).
card_toughness('markov warlord', 4).

% Found in: DKA
card_name('markov\'s servant', 'Markov\'s Servant').
card_type('markov\'s servant', 'Creature — Vampire').
card_types('markov\'s servant', ['Creature']).
card_subtypes('markov\'s servant', ['Vampire']).
card_colors('markov\'s servant', ['B']).
card_text('markov\'s servant', '').
card_layout('markov\'s servant', 'double-faced').
card_power('markov\'s servant', 4).
card_toughness('markov\'s servant', 4).

% Found in: 6ED, 7ED, 8ED, 9ED, MIR
card_name('maro', 'Maro').
card_type('maro', 'Creature — Elemental').
card_types('maro', ['Creature']).
card_subtypes('maro', ['Elemental']).
card_colors('maro', ['G']).
card_text('maro', 'Maro\'s power and toughness are each equal to the number of cards in your hand.').
card_mana_cost('maro', ['2', 'G', 'G']).
card_cmc('maro', 4).
card_layout('maro', 'normal').
card_power('maro', '*').
card_toughness('maro', '*').

% Found in: VAN
card_name('maro avatar', 'Maro Avatar').
card_type('maro avatar', 'Vanguard').
card_types('maro avatar', ['Vanguard']).
card_subtypes('maro avatar', []).
card_colors('maro avatar', []).
card_text('maro avatar', 'Tap an untapped creature you control, Discard a card: Target creature you control gets +X/+X until end of turn, where X is the number of cards in your hand.').
card_layout('maro avatar', 'vanguard').

% Found in: AVR, C13
card_name('marrow bats', 'Marrow Bats').
card_type('marrow bats', 'Creature — Bat Skeleton').
card_types('marrow bats', ['Creature']).
card_subtypes('marrow bats', ['Bat', 'Skeleton']).
card_colors('marrow bats', ['B']).
card_text('marrow bats', 'Flying\nPay 4 life: Regenerate Marrow Bats.').
card_mana_cost('marrow bats', ['4', 'B']).
card_cmc('marrow bats', 5).
card_layout('marrow bats', 'normal').
card_power('marrow bats', 4).
card_toughness('marrow bats', 1).

% Found in: ARB
card_name('marrow chomper', 'Marrow Chomper').
card_type('marrow chomper', 'Creature — Zombie Lizard').
card_types('marrow chomper', ['Creature']).
card_subtypes('marrow chomper', ['Zombie', 'Lizard']).
card_colors('marrow chomper', ['B', 'G']).
card_text('marrow chomper', 'Devour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\nWhen Marrow Chomper enters the battlefield, you gain 2 life for each creature it devoured.').
card_mana_cost('marrow chomper', ['3', 'B', 'G']).
card_cmc('marrow chomper', 5).
card_layout('marrow chomper', 'normal').
card_power('marrow chomper', 3).
card_toughness('marrow chomper', 3).

% Found in: NPH
card_name('marrow shards', 'Marrow Shards').
card_type('marrow shards', 'Instant').
card_types('marrow shards', ['Instant']).
card_subtypes('marrow shards', []).
card_colors('marrow shards', ['W']).
card_text('marrow shards', '({W/P} can be paid with either {W} or 2 life.)\nMarrow Shards deals 1 damage to each attacking creature.').
card_mana_cost('marrow shards', ['W/P']).
card_cmc('marrow shards', 1).
card_layout('marrow shards', 'normal').

% Found in: CHK
card_name('marrow-gnawer', 'Marrow-Gnawer').
card_type('marrow-gnawer', 'Legendary Creature — Rat Rogue').
card_types('marrow-gnawer', ['Creature']).
card_subtypes('marrow-gnawer', ['Rat', 'Rogue']).
card_supertypes('marrow-gnawer', ['Legendary']).
card_colors('marrow-gnawer', ['B']).
card_text('marrow-gnawer', 'Rat creatures have fear. (They can\'t be blocked except by artifact creatures and/or black creatures.)\n{T}, Sacrifice a Rat: Put X 1/1 black Rat creature tokens onto the battlefield, where X is the number of Rats you control.').
card_mana_cost('marrow-gnawer', ['3', 'B', 'B']).
card_cmc('marrow-gnawer', 5).
card_layout('marrow-gnawer', 'normal').
card_power('marrow-gnawer', 2).
card_toughness('marrow-gnawer', 3).

% Found in: PCY
card_name('marsh boa', 'Marsh Boa').
card_type('marsh boa', 'Creature — Snake').
card_types('marsh boa', ['Creature']).
card_subtypes('marsh boa', ['Snake']).
card_colors('marsh boa', ['G']).
card_text('marsh boa', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('marsh boa', ['G']).
card_cmc('marsh boa', 1).
card_layout('marsh boa', 'normal').
card_power('marsh boa', 1).
card_toughness('marsh boa', 1).

% Found in: DDM, DDP, ZEN
card_name('marsh casualties', 'Marsh Casualties').
card_type('marsh casualties', 'Sorcery').
card_types('marsh casualties', ['Sorcery']).
card_subtypes('marsh casualties', []).
card_colors('marsh casualties', ['B']).
card_text('marsh casualties', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nCreatures target player controls get -1/-1 until end of turn. If Marsh Casualties was kicked, those creatures get -2/-2 until end of turn instead.').
card_mana_cost('marsh casualties', ['B', 'B']).
card_cmc('marsh casualties', 2).
card_layout('marsh casualties', 'normal').

% Found in: PLS
card_name('marsh crocodile', 'Marsh Crocodile').
card_type('marsh crocodile', 'Creature — Crocodile').
card_types('marsh crocodile', ['Creature']).
card_subtypes('marsh crocodile', ['Crocodile']).
card_colors('marsh crocodile', ['U', 'B']).
card_text('marsh crocodile', 'When Marsh Crocodile enters the battlefield, return a blue or black creature you control to its owner\'s hand.\nWhen Marsh Crocodile enters the battlefield, each player discards a card.').
card_mana_cost('marsh crocodile', ['2', 'U', 'B']).
card_cmc('marsh crocodile', 4).
card_layout('marsh crocodile', 'normal').
card_power('marsh crocodile', 4).
card_toughness('marsh crocodile', 4).

% Found in: EXP, ZEN
card_name('marsh flats', 'Marsh Flats').
card_type('marsh flats', 'Land').
card_types('marsh flats', ['Land']).
card_subtypes('marsh flats', []).
card_colors('marsh flats', []).
card_text('marsh flats', '{T}, Pay 1 life, Sacrifice Marsh Flats: Search your library for a Plains or Swamp card and put it onto the battlefield. Then shuffle your library.').
card_layout('marsh flats', 'normal').

% Found in: LRW, MMA
card_name('marsh flitter', 'Marsh Flitter').
card_type('marsh flitter', 'Creature — Faerie Rogue').
card_types('marsh flitter', ['Creature']).
card_subtypes('marsh flitter', ['Faerie', 'Rogue']).
card_colors('marsh flitter', ['B']).
card_text('marsh flitter', 'Flying\nWhen Marsh Flitter enters the battlefield, put two 1/1 black Goblin Rogue creature tokens onto the battlefield.\nSacrifice a Goblin: Marsh Flitter has base power and toughness 3/3 until end of turn.').
card_mana_cost('marsh flitter', ['3', 'B']).
card_cmc('marsh flitter', 4).
card_layout('marsh flitter', 'normal').
card_power('marsh flitter', 1).
card_toughness('marsh flitter', 1).

% Found in: 4ED, DRK
card_name('marsh gas', 'Marsh Gas').
card_type('marsh gas', 'Instant').
card_types('marsh gas', ['Instant']).
card_subtypes('marsh gas', []).
card_colors('marsh gas', ['B']).
card_text('marsh gas', 'All creatures get -2/-0 until end of turn.').
card_mana_cost('marsh gas', ['B']).
card_cmc('marsh gas', 1).
card_layout('marsh gas', 'normal').

% Found in: DRK
card_name('marsh goblins', 'Marsh Goblins').
card_type('marsh goblins', 'Creature — Goblin').
card_types('marsh goblins', ['Creature']).
card_subtypes('marsh goblins', ['Goblin']).
card_colors('marsh goblins', ['B', 'R']).
card_text('marsh goblins', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('marsh goblins', ['B', 'R']).
card_cmc('marsh goblins', 2).
card_layout('marsh goblins', 'normal').
card_power('marsh goblins', 1).
card_toughness('marsh goblins', 1).

% Found in: DTK
card_name('marsh hulk', 'Marsh Hulk').
card_type('marsh hulk', 'Creature — Zombie Ogre').
card_types('marsh hulk', ['Creature']).
card_subtypes('marsh hulk', ['Zombie', 'Ogre']).
card_colors('marsh hulk', ['B']).
card_text('marsh hulk', 'Megamorph {6}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('marsh hulk', ['4', 'B', 'B']).
card_cmc('marsh hulk', 6).
card_layout('marsh hulk', 'normal').
card_power('marsh hulk', 4).
card_toughness('marsh hulk', 6).

% Found in: TMP
card_name('marsh lurker', 'Marsh Lurker').
card_type('marsh lurker', 'Creature — Beast').
card_types('marsh lurker', ['Creature']).
card_subtypes('marsh lurker', ['Beast']).
card_colors('marsh lurker', ['B']).
card_text('marsh lurker', 'Sacrifice a Swamp: Marsh Lurker gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('marsh lurker', ['3', 'B']).
card_cmc('marsh lurker', 4).
card_layout('marsh lurker', 'normal').
card_power('marsh lurker', 3).
card_toughness('marsh lurker', 2).

% Found in: WWK
card_name('marsh threader', 'Marsh Threader').
card_type('marsh threader', 'Creature — Kor Scout').
card_types('marsh threader', ['Creature']).
card_subtypes('marsh threader', ['Kor', 'Scout']).
card_colors('marsh threader', ['W']).
card_text('marsh threader', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('marsh threader', ['1', 'W']).
card_cmc('marsh threader', 2).
card_layout('marsh threader', 'normal').
card_power('marsh threader', 2).
card_toughness('marsh threader', 1).

% Found in: 4ED, 5ED, DRK
card_name('marsh viper', 'Marsh Viper').
card_type('marsh viper', 'Creature — Snake').
card_types('marsh viper', ['Creature']).
card_subtypes('marsh viper', ['Snake']).
card_colors('marsh viper', ['G']).
card_text('marsh viper', 'Whenever Marsh Viper deals damage to a player, that player gets two poison counters. (A player with ten or more poison counters loses the game.)').
card_mana_cost('marsh viper', ['3', 'G']).
card_cmc('marsh viper', 4).
card_layout('marsh viper', 'normal').
card_power('marsh viper', 1).
card_toughness('marsh viper', 2).

% Found in: C14, WWK
card_name('marshal\'s anthem', 'Marshal\'s Anthem').
card_type('marshal\'s anthem', 'Enchantment').
card_types('marshal\'s anthem', ['Enchantment']).
card_subtypes('marshal\'s anthem', []).
card_colors('marshal\'s anthem', ['W']).
card_text('marshal\'s anthem', 'Multikicker {1}{W} (You may pay an additional {1}{W} any number of times as you cast this spell.)\nCreatures you control get +1/+1.\nWhen Marshal\'s Anthem enters the battlefield, return up to X target creature cards from your graveyard to the battlefield, where X is the number of times Marshal\'s Anthem was kicked.').
card_mana_cost('marshal\'s anthem', ['2', 'W', 'W']).
card_cmc('marshal\'s anthem', 4).
card_layout('marshal\'s anthem', 'normal').

% Found in: FUT
card_name('marshaling cry', 'Marshaling Cry').
card_type('marshaling cry', 'Sorcery').
card_types('marshaling cry', ['Sorcery']).
card_subtypes('marshaling cry', []).
card_colors('marshaling cry', ['W']).
card_text('marshaling cry', 'Creatures you control get +1/+1 and gain vigilance until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)\nFlashback {3}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('marshaling cry', ['1', 'W', 'W']).
card_cmc('marshaling cry', 3).
card_layout('marshaling cry', 'normal').

% Found in: PTK
card_name('marshaling the troops', 'Marshaling the Troops').
card_type('marshaling the troops', 'Sorcery').
card_types('marshaling the troops', ['Sorcery']).
card_subtypes('marshaling the troops', []).
card_colors('marshaling the troops', ['G']).
card_text('marshaling the troops', 'Tap any number of untapped creatures you control. You gain 4 life for each creature tapped this way.').
card_mana_cost('marshaling the troops', ['1', 'G']).
card_cmc('marshaling the troops', 2).
card_layout('marshaling the troops', 'normal').

% Found in: EVE
card_name('marshdrinker giant', 'Marshdrinker Giant').
card_type('marshdrinker giant', 'Creature — Giant Warrior').
card_types('marshdrinker giant', ['Creature']).
card_subtypes('marshdrinker giant', ['Giant', 'Warrior']).
card_colors('marshdrinker giant', ['G']).
card_text('marshdrinker giant', 'When Marshdrinker Giant enters the battlefield, destroy target Island or Swamp an opponent controls.').
card_mana_cost('marshdrinker giant', ['3', 'G', 'G']).
card_cmc('marshdrinker giant', 5).
card_layout('marshdrinker giant', 'normal').
card_power('marshdrinker giant', 4).
card_toughness('marshdrinker giant', 3).

% Found in: BNG
card_name('marshmist titan', 'Marshmist Titan').
card_type('marshmist titan', 'Creature — Giant').
card_types('marshmist titan', ['Creature']).
card_subtypes('marshmist titan', ['Giant']).
card_colors('marshmist titan', ['B']).
card_text('marshmist titan', 'Marshmist Titan costs {X} less to cast, where X is your devotion to black. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_mana_cost('marshmist titan', ['6', 'B']).
card_cmc('marshmist titan', 7).
card_layout('marshmist titan', 'normal').
card_power('marshmist titan', 4).
card_toughness('marshmist titan', 5).

% Found in: C14, CON, V14
card_name('martial coup', 'Martial Coup').
card_type('martial coup', 'Sorcery').
card_types('martial coup', ['Sorcery']).
card_subtypes('martial coup', []).
card_colors('martial coup', ['W']).
card_text('martial coup', 'Put X 1/1 white Soldier creature tokens onto the battlefield. If X is 5 or more, destroy all other creatures.').
card_mana_cost('martial coup', ['X', 'W', 'W']).
card_cmc('martial coup', 2).
card_layout('martial coup', 'normal').

% Found in: GTC
card_name('martial glory', 'Martial Glory').
card_type('martial glory', 'Instant').
card_types('martial glory', ['Instant']).
card_subtypes('martial glory', []).
card_colors('martial glory', ['W', 'R']).
card_text('martial glory', 'Target creature gets +3/+0 until end of turn.\nTarget creature gets +0/+3 until end of turn.').
card_mana_cost('martial glory', ['R', 'W']).
card_cmc('martial glory', 2).
card_layout('martial glory', 'normal').

% Found in: RTR
card_name('martial law', 'Martial Law').
card_type('martial law', 'Enchantment').
card_types('martial law', ['Enchantment']).
card_subtypes('martial law', []).
card_colors('martial law', ['W']).
card_text('martial law', 'At the beginning of your upkeep, detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)').
card_mana_cost('martial law', ['2', 'W', 'W']).
card_cmc('martial law', 4).
card_layout('martial law', 'normal').

% Found in: CSP
card_name('martyr of ashes', 'Martyr of Ashes').
card_type('martyr of ashes', 'Creature — Human Shaman').
card_types('martyr of ashes', ['Creature']).
card_subtypes('martyr of ashes', ['Human', 'Shaman']).
card_colors('martyr of ashes', ['R']).
card_text('martyr of ashes', '{2}, Reveal X red cards from your hand, Sacrifice Martyr of Ashes: Martyr of Ashes deals X damage to each creature without flying.').
card_mana_cost('martyr of ashes', ['R']).
card_cmc('martyr of ashes', 1).
card_layout('martyr of ashes', 'normal').
card_power('martyr of ashes', 1).
card_toughness('martyr of ashes', 1).

% Found in: CSP
card_name('martyr of bones', 'Martyr of Bones').
card_type('martyr of bones', 'Creature — Human Wizard').
card_types('martyr of bones', ['Creature']).
card_subtypes('martyr of bones', ['Human', 'Wizard']).
card_colors('martyr of bones', ['B']).
card_text('martyr of bones', '{1}, Reveal X black cards from your hand, Sacrifice Martyr of Bones: Exile up to X target cards from a single graveyard.').
card_mana_cost('martyr of bones', ['B']).
card_cmc('martyr of bones', 1).
card_layout('martyr of bones', 'normal').
card_power('martyr of bones', 1).
card_toughness('martyr of bones', 1).

% Found in: CSP, DD2, DD3_JVC
card_name('martyr of frost', 'Martyr of Frost').
card_type('martyr of frost', 'Creature — Human Wizard').
card_types('martyr of frost', ['Creature']).
card_subtypes('martyr of frost', ['Human', 'Wizard']).
card_colors('martyr of frost', ['U']).
card_text('martyr of frost', '{2}, Reveal X blue cards from your hand, Sacrifice Martyr of Frost: Counter target spell unless its controller pays {X}.').
card_mana_cost('martyr of frost', ['U']).
card_cmc('martyr of frost', 1).
card_layout('martyr of frost', 'normal').
card_power('martyr of frost', 1).
card_toughness('martyr of frost', 1).

% Found in: CSP
card_name('martyr of sands', 'Martyr of Sands').
card_type('martyr of sands', 'Creature — Human Cleric').
card_types('martyr of sands', ['Creature']).
card_subtypes('martyr of sands', ['Human', 'Cleric']).
card_colors('martyr of sands', ['W']).
card_text('martyr of sands', '{1}, Reveal X white cards from your hand, Sacrifice Martyr of Sands: You gain three times X life.').
card_mana_cost('martyr of sands', ['W']).
card_cmc('martyr of sands', 1).
card_layout('martyr of sands', 'normal').
card_power('martyr of sands', 1).
card_toughness('martyr of sands', 1).

% Found in: CSP
card_name('martyr of spores', 'Martyr of Spores').
card_type('martyr of spores', 'Creature — Human Shaman').
card_types('martyr of spores', ['Creature']).
card_subtypes('martyr of spores', ['Human', 'Shaman']).
card_colors('martyr of spores', ['G']).
card_text('martyr of spores', '{1}, Reveal X green cards from your hand, Sacrifice Martyr of Spores: Target creature gets +X/+X until end of turn.').
card_mana_cost('martyr of spores', ['G']).
card_cmc('martyr of spores', 1).
card_layout('martyr of spores', 'normal').
card_power('martyr of spores', 1).
card_toughness('martyr of spores', 1).

% Found in: CMD
card_name('martyr\'s bond', 'Martyr\'s Bond').
card_type('martyr\'s bond', 'Enchantment').
card_types('martyr\'s bond', ['Enchantment']).
card_subtypes('martyr\'s bond', []).
card_colors('martyr\'s bond', ['W']).
card_text('martyr\'s bond', 'Whenever Martyr\'s Bond or another nonland permanent you control is put into a graveyard from the battlefield, each opponent sacrifices a permanent that shares a card type with it.').
card_mana_cost('martyr\'s bond', ['4', 'W', 'W']).
card_cmc('martyr\'s bond', 6).
card_layout('martyr\'s bond', 'normal').

% Found in: ULG
card_name('martyr\'s cause', 'Martyr\'s Cause').
card_type('martyr\'s cause', 'Enchantment').
card_types('martyr\'s cause', ['Enchantment']).
card_subtypes('martyr\'s cause', []).
card_colors('martyr\'s cause', ['W']).
card_text('martyr\'s cause', 'Sacrifice a creature: The next time a source of your choice would deal damage to target creature or player this turn, prevent that damage.').
card_mana_cost('martyr\'s cause', ['2', 'W']).
card_cmc('martyr\'s cause', 3).
card_layout('martyr\'s cause', 'normal').

% Found in: DRK, ME4
card_name('martyr\'s cry', 'Martyr\'s Cry').
card_type('martyr\'s cry', 'Sorcery').
card_types('martyr\'s cry', ['Sorcery']).
card_subtypes('martyr\'s cry', []).
card_colors('martyr\'s cry', ['W']).
card_text('martyr\'s cry', 'Exile all white creatures. For each creature exiled this way, its controller draws a card.').
card_mana_cost('martyr\'s cry', ['W', 'W']).
card_cmc('martyr\'s cry', 2).
card_layout('martyr\'s cry', 'normal').
card_reserved('martyr\'s cry').

% Found in: ALL
card_name('martyrdom', 'Martyrdom').
card_type('martyrdom', 'Instant').
card_types('martyrdom', ['Instant']).
card_subtypes('martyrdom', []).
card_colors('martyrdom', ['W']).
card_text('martyrdom', 'Until end of turn, target creature you control gains \"{0}: The next 1 damage that would be dealt to target creature or player this turn is dealt to this creature instead.\" Only you may activate this ability.').
card_mana_cost('martyrdom', ['1', 'W', 'W']).
card_cmc('martyrdom', 3).
card_layout('martyrdom', 'normal').

% Found in: GPT
card_name('martyred rusalka', 'Martyred Rusalka').
card_type('martyred rusalka', 'Creature — Spirit').
card_types('martyred rusalka', ['Creature']).
card_subtypes('martyred rusalka', ['Spirit']).
card_colors('martyred rusalka', ['W']).
card_text('martyred rusalka', '{W}, Sacrifice a creature: Target creature can\'t attack this turn.').
card_mana_cost('martyred rusalka', ['W']).
card_cmc('martyred rusalka', 1).
card_layout('martyred rusalka', 'normal').
card_power('martyred rusalka', 1).
card_toughness('martyred rusalka', 1).

% Found in: ATQ, ME4
card_name('martyrs of korlis', 'Martyrs of Korlis').
card_type('martyrs of korlis', 'Creature — Human').
card_types('martyrs of korlis', ['Creature']).
card_subtypes('martyrs of korlis', ['Human']).
card_colors('martyrs of korlis', ['W']).
card_text('martyrs of korlis', 'As long as Martyrs of Korlis is untapped, all damage that would be dealt to you by artifacts is dealt to Martyrs of Korlis instead.').
card_mana_cost('martyrs of korlis', ['3', 'W', 'W']).
card_cmc('martyrs of korlis', 5).
card_layout('martyrs of korlis', 'normal').
card_power('martyrs of korlis', 1).
card_toughness('martyrs of korlis', 6).
card_reserved('martyrs of korlis').

% Found in: APC
card_name('martyrs\' tomb', 'Martyrs\' Tomb').
card_type('martyrs\' tomb', 'Enchantment').
card_types('martyrs\' tomb', ['Enchantment']).
card_subtypes('martyrs\' tomb', []).
card_colors('martyrs\' tomb', ['W', 'B']).
card_text('martyrs\' tomb', 'Pay 2 life: Prevent the next 1 damage that would be dealt to target creature this turn.').
card_mana_cost('martyrs\' tomb', ['2', 'W', 'B']).
card_cmc('martyrs\' tomb', 4).
card_layout('martyrs\' tomb', 'normal').

% Found in: CHK
card_name('masako the humorless', 'Masako the Humorless').
card_type('masako the humorless', 'Legendary Creature — Human Advisor').
card_types('masako the humorless', ['Creature']).
card_subtypes('masako the humorless', ['Human', 'Advisor']).
card_supertypes('masako the humorless', ['Legendary']).
card_colors('masako the humorless', ['W']).
card_text('masako the humorless', 'Flash\nTapped creatures you control can block as though they were untapped.').
card_mana_cost('masako the humorless', ['2', 'W']).
card_cmc('masako the humorless', 3).
card_layout('masako the humorless', 'normal').
card_power('masako the humorless', 2).
card_toughness('masako the humorless', 1).

% Found in: ISD
card_name('mask of avacyn', 'Mask of Avacyn').
card_type('mask of avacyn', 'Artifact — Equipment').
card_types('mask of avacyn', ['Artifact']).
card_subtypes('mask of avacyn', ['Equipment']).
card_colors('mask of avacyn', []).
card_text('mask of avacyn', 'Equipped creature gets +1/+2 and has hexproof. (It can\'t be the target of spells or abilities your opponents control.)\nEquip {3}').
card_mana_cost('mask of avacyn', ['2']).
card_cmc('mask of avacyn', 2).
card_layout('mask of avacyn', 'normal').

% Found in: APC
card_name('mask of intolerance', 'Mask of Intolerance').
card_type('mask of intolerance', 'Artifact').
card_types('mask of intolerance', ['Artifact']).
card_subtypes('mask of intolerance', []).
card_colors('mask of intolerance', []).
card_text('mask of intolerance', 'At the beginning of each player\'s upkeep, if there are four or more basic land types among lands that player controls, Mask of Intolerance deals 3 damage to him or her.').
card_mana_cost('mask of intolerance', ['2']).
card_cmc('mask of intolerance', 2).
card_layout('mask of intolerance', 'normal').

% Found in: UDS
card_name('mask of law and grace', 'Mask of Law and Grace').
card_type('mask of law and grace', 'Enchantment — Aura').
card_types('mask of law and grace', ['Enchantment']).
card_subtypes('mask of law and grace', ['Aura']).
card_colors('mask of law and grace', ['W']).
card_text('mask of law and grace', 'Enchant creature\nEnchanted creature has protection from black and from red.').
card_mana_cost('mask of law and grace', ['W']).
card_cmc('mask of law and grace', 1).
card_layout('mask of law and grace', 'normal').

% Found in: C14, HOP, MRD
card_name('mask of memory', 'Mask of Memory').
card_type('mask of memory', 'Artifact — Equipment').
card_types('mask of memory', ['Artifact']).
card_subtypes('mask of memory', ['Equipment']).
card_colors('mask of memory', []).
card_text('mask of memory', 'Whenever equipped creature deals combat damage to a player, you may draw two cards. If you do, discard a card.\nEquip {1}').
card_mana_cost('mask of memory', ['2']).
card_cmc('mask of memory', 2).
card_layout('mask of memory', 'normal').

% Found in: ARB
card_name('mask of riddles', 'Mask of Riddles').
card_type('mask of riddles', 'Artifact — Equipment').
card_types('mask of riddles', ['Artifact']).
card_subtypes('mask of riddles', ['Equipment']).
card_colors('mask of riddles', ['U', 'B']).
card_text('mask of riddles', 'Equipped creature has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever equipped creature deals combat damage to a player, you may draw a card.\nEquip {2}').
card_mana_cost('mask of riddles', ['U', 'B']).
card_cmc('mask of riddles', 2).
card_layout('mask of riddles', 'normal').

% Found in: STH
card_name('mask of the mimic', 'Mask of the Mimic').
card_type('mask of the mimic', 'Instant').
card_types('mask of the mimic', ['Instant']).
card_subtypes('mask of the mimic', []).
card_colors('mask of the mimic', ['U']).
card_text('mask of the mimic', 'As an additional cost to cast Mask of the Mimic, sacrifice a creature.\nSearch your library for a card with the same name as target nontoken creature and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('mask of the mimic', ['U']).
card_cmc('mask of the mimic', 1).
card_layout('mask of the mimic', 'normal').

% Found in: C14, LRW, MMA
card_name('masked admirers', 'Masked Admirers').
card_type('masked admirers', 'Creature — Elf Shaman').
card_types('masked admirers', ['Creature']).
card_subtypes('masked admirers', ['Elf', 'Shaman']).
card_colors('masked admirers', ['G']).
card_text('masked admirers', 'When Masked Admirers enters the battlefield, draw a card.\nWhenever you cast a creature spell, you may pay {G}{G}. If you do, return Masked Admirers from your graveyard to your hand.').
card_mana_cost('masked admirers', ['2', 'G', 'G']).
card_cmc('masked admirers', 4).
card_layout('masked admirers', 'normal').
card_power('masked admirers', 3).
card_toughness('masked admirers', 2).

% Found in: JUD
card_name('masked gorgon', 'Masked Gorgon').
card_type('masked gorgon', 'Creature — Gorgon').
card_types('masked gorgon', ['Creature']).
card_subtypes('masked gorgon', ['Gorgon']).
card_colors('masked gorgon', ['B']).
card_text('masked gorgon', 'Green creatures and white creatures have protection from Gorgons.\nThreshold — Masked Gorgon has protection from green and from white as long as seven or more cards are in your graveyard.').
card_mana_cost('masked gorgon', ['4', 'B']).
card_cmc('masked gorgon', 5).
card_layout('masked gorgon', 'normal').
card_power('masked gorgon', 5).
card_toughness('masked gorgon', 5).

% Found in: AVR
card_name('mass appeal', 'Mass Appeal').
card_type('mass appeal', 'Sorcery').
card_types('mass appeal', ['Sorcery']).
card_subtypes('mass appeal', []).
card_colors('mass appeal', ['U']).
card_text('mass appeal', 'Draw a card for each Human you control.').
card_mana_cost('mass appeal', ['2', 'U']).
card_cmc('mass appeal', 3).
card_layout('mass appeal', 'normal').

% Found in: M15, SHM
card_name('mass calcify', 'Mass Calcify').
card_type('mass calcify', 'Sorcery').
card_types('mass calcify', ['Sorcery']).
card_subtypes('mass calcify', []).
card_colors('mass calcify', ['W']).
card_text('mass calcify', 'Destroy all nonwhite creatures.').
card_mana_cost('mass calcify', ['5', 'W', 'W']).
card_cmc('mass calcify', 7).
card_layout('mass calcify', 'normal').

% Found in: MRD
card_name('mass hysteria', 'Mass Hysteria').
card_type('mass hysteria', 'Enchantment').
card_types('mass hysteria', ['Enchantment']).
card_subtypes('mass hysteria', []).
card_colors('mass hysteria', ['R']).
card_text('mass hysteria', 'All creatures have haste.').
card_mana_cost('mass hysteria', ['R']).
card_cmc('mass hysteria', 1).
card_layout('mass hysteria', 'normal').

% Found in: C13, PC2
card_name('mass mutiny', 'Mass Mutiny').
card_type('mass mutiny', 'Sorcery').
card_types('mass mutiny', ['Sorcery']).
card_subtypes('mass mutiny', []).
card_colors('mass mutiny', ['R']).
card_text('mass mutiny', 'For each opponent, gain control of up to one target creature that player controls until end of turn. Untap those creatures. They gain haste until end of turn.').
card_mana_cost('mass mutiny', ['3', 'R', 'R']).
card_cmc('mass mutiny', 5).
card_layout('mass mutiny', 'normal').

% Found in: 10E, FUT
card_name('mass of ghouls', 'Mass of Ghouls').
card_type('mass of ghouls', 'Creature — Zombie Warrior').
card_types('mass of ghouls', ['Creature']).
card_subtypes('mass of ghouls', ['Zombie', 'Warrior']).
card_colors('mass of ghouls', ['B']).
card_text('mass of ghouls', '').
card_mana_cost('mass of ghouls', ['3', 'B', 'B']).
card_cmc('mass of ghouls', 5).
card_layout('mass of ghouls', 'normal').
card_power('mass of ghouls', 5).
card_toughness('mass of ghouls', 3).

% Found in: M11
card_name('mass polymorph', 'Mass Polymorph').
card_type('mass polymorph', 'Sorcery').
card_types('mass polymorph', ['Sorcery']).
card_subtypes('mass polymorph', []).
card_colors('mass polymorph', ['U']).
card_text('mass polymorph', 'Exile all creatures you control, then reveal cards from the top of your library until you reveal that many creature cards. Put all creature cards revealed this way onto the battlefield, then shuffle the rest of the revealed cards into your library.').
card_mana_cost('mass polymorph', ['5', 'U']).
card_cmc('mass polymorph', 6).
card_layout('mass polymorph', 'normal').

% Found in: NMS
card_name('massacre', 'Massacre').
card_type('massacre', 'Sorcery').
card_types('massacre', ['Sorcery']).
card_subtypes('massacre', []).
card_colors('massacre', ['B']).
card_text('massacre', 'If an opponent controls a Plains and you control a Swamp, you may cast Massacre without paying its mana cost.\nAll creatures get -2/-2 until end of turn.').
card_mana_cost('massacre', ['2', 'B', 'B']).
card_cmc('massacre', 4).
card_layout('massacre', 'normal').

% Found in: MBS
card_name('massacre wurm', 'Massacre Wurm').
card_type('massacre wurm', 'Creature — Wurm').
card_types('massacre wurm', ['Creature']).
card_subtypes('massacre wurm', ['Wurm']).
card_colors('massacre wurm', ['B']).
card_text('massacre wurm', 'When Massacre Wurm enters the battlefield, creatures your opponents control get -2/-2 until end of turn.\nWhenever a creature an opponent controls dies, that player loses 2 life.').
card_mana_cost('massacre wurm', ['3', 'B', 'B', 'B']).
card_cmc('massacre wurm', 6).
card_layout('massacre wurm', 'normal').
card_power('massacre wurm', 6).
card_toughness('massacre wurm', 5).

% Found in: GTC
card_name('massive raid', 'Massive Raid').
card_type('massive raid', 'Instant').
card_types('massive raid', ['Instant']).
card_subtypes('massive raid', []).
card_colors('massive raid', ['R']).
card_text('massive raid', 'Massive Raid deals damage to target creature or player equal to the number of creatures you control.').
card_mana_cost('massive raid', ['1', 'R', 'R']).
card_cmc('massive raid', 3).
card_layout('massive raid', 'normal').

% Found in: ODY
card_name('master apothecary', 'Master Apothecary').
card_type('master apothecary', 'Creature — Human Cleric').
card_types('master apothecary', ['Creature']).
card_subtypes('master apothecary', ['Human', 'Cleric']).
card_colors('master apothecary', ['W']).
card_text('master apothecary', 'Tap an untapped Cleric you control: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('master apothecary', ['W', 'W', 'W']).
card_cmc('master apothecary', 3).
card_layout('master apothecary', 'normal').
card_power('master apothecary', 2).
card_toughness('master apothecary', 2).

% Found in: GTC
card_name('master biomancer', 'Master Biomancer').
card_type('master biomancer', 'Creature — Elf Wizard').
card_types('master biomancer', ['Creature']).
card_subtypes('master biomancer', ['Elf', 'Wizard']).
card_colors('master biomancer', ['U', 'G']).
card_text('master biomancer', 'Each other creature you control enters the battlefield with a number of additional +1/+1 counters on it equal to Master Biomancer\'s power and as a Mutant in addition to its other types.').
card_mana_cost('master biomancer', ['2', 'G', 'U']).
card_cmc('master biomancer', 4).
card_layout('master biomancer', 'normal').
card_power('master biomancer', 2).
card_toughness('master biomancer', 4).

% Found in: 8ED, 9ED, BRB, DDN, TMP, TPR
card_name('master decoy', 'Master Decoy').
card_type('master decoy', 'Creature — Human Soldier').
card_types('master decoy', ['Creature']).
card_subtypes('master decoy', ['Human', 'Soldier']).
card_colors('master decoy', ['W']).
card_text('master decoy', '{W}, {T}: Tap target creature.').
card_mana_cost('master decoy', ['1', 'W']).
card_cmc('master decoy', 2).
card_layout('master decoy', 'normal').
card_power('master decoy', 1).
card_toughness('master decoy', 2).

% Found in: 7ED, 8ED, 9ED, UDS
card_name('master healer', 'Master Healer').
card_type('master healer', 'Creature — Human Cleric').
card_types('master healer', ['Creature']).
card_subtypes('master healer', ['Human', 'Cleric']).
card_colors('master healer', ['W']).
card_text('master healer', '{T}: Prevent the next 4 damage that would be dealt to target creature or player this turn.').
card_mana_cost('master healer', ['4', 'W']).
card_cmc('master healer', 5).
card_layout('master healer', 'normal').
card_power('master healer', 1).
card_toughness('master healer', 4).

% Found in: WTH
card_name('master of arms', 'Master of Arms').
card_type('master of arms', 'Creature — Human Soldier').
card_types('master of arms', ['Creature']).
card_subtypes('master of arms', ['Human', 'Soldier']).
card_colors('master of arms', ['W']).
card_text('master of arms', 'First strike\n{1}{W}: Tap target creature blocking Master of Arms.').
card_mana_cost('master of arms', ['2', 'W']).
card_cmc('master of arms', 3).
card_layout('master of arms', 'normal').
card_power('master of arms', 2).
card_toughness('master of arms', 2).

% Found in: DGM
card_name('master of cruelties', 'Master of Cruelties').
card_type('master of cruelties', 'Creature — Demon').
card_types('master of cruelties', ['Creature']).
card_subtypes('master of cruelties', ['Demon']).
card_colors('master of cruelties', ['B', 'R']).
card_text('master of cruelties', 'First strike, deathtouch\nMaster of Cruelties can only attack alone.\nWhenever Master of Cruelties attacks a player and isn\'t blocked, that player\'s life total becomes 1. Master of Cruelties assigns no combat damage this combat.').
card_mana_cost('master of cruelties', ['3', 'B', 'R']).
card_cmc('master of cruelties', 5).
card_layout('master of cruelties', 'normal').
card_power('master of cruelties', 1).
card_toughness('master of cruelties', 4).

% Found in: M14
card_name('master of diversion', 'Master of Diversion').
card_type('master of diversion', 'Creature — Human Scout').
card_types('master of diversion', ['Creature']).
card_subtypes('master of diversion', ['Human', 'Scout']).
card_colors('master of diversion', ['W']).
card_text('master of diversion', 'Whenever Master of Diversion attacks, tap target creature defending player controls.').
card_mana_cost('master of diversion', ['2', 'W']).
card_cmc('master of diversion', 3).
card_layout('master of diversion', 'normal').
card_power('master of diversion', 2).
card_toughness('master of diversion', 2).

% Found in: ALA, DDF, HOP
card_name('master of etherium', 'Master of Etherium').
card_type('master of etherium', 'Artifact Creature — Vedalken Wizard').
card_types('master of etherium', ['Artifact', 'Creature']).
card_subtypes('master of etherium', ['Vedalken', 'Wizard']).
card_colors('master of etherium', ['U']).
card_text('master of etherium', 'Master of Etherium\'s power and toughness are each equal to the number of artifacts you control.\nOther artifact creatures you control get +1/+1.').
card_mana_cost('master of etherium', ['2', 'U']).
card_cmc('master of etherium', 3).
card_layout('master of etherium', 'normal').
card_power('master of etherium', '*').
card_toughness('master of etherium', '*').

% Found in: KTK, pPRE
card_name('master of pearls', 'Master of Pearls').
card_type('master of pearls', 'Creature — Human Monk').
card_types('master of pearls', ['Creature']).
card_subtypes('master of pearls', ['Human', 'Monk']).
card_colors('master of pearls', ['W']).
card_text('master of pearls', 'Morph {3}{W}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Master of Pearls is turned face up, creatures you control get +2/+2 until end of turn.').
card_mana_cost('master of pearls', ['1', 'W']).
card_cmc('master of pearls', 2).
card_layout('master of pearls', 'normal').
card_power('master of pearls', 2).
card_toughness('master of pearls', 2).

% Found in: M15
card_name('master of predicaments', 'Master of Predicaments').
card_type('master of predicaments', 'Creature — Sphinx').
card_types('master of predicaments', ['Creature']).
card_subtypes('master of predicaments', ['Sphinx']).
card_colors('master of predicaments', ['U']).
card_text('master of predicaments', 'Flying\nWhenever Master of Predicaments deals combat damage to a player, choose a card in your hand. That player guesses whether the card\'s converted mana cost is greater than 4. If the player guessed wrong, you may cast the card without paying its mana cost.').
card_mana_cost('master of predicaments', ['3', 'U', 'U']).
card_cmc('master of predicaments', 5).
card_layout('master of predicaments', 'normal').
card_power('master of predicaments', 4).
card_toughness('master of predicaments', 4).

% Found in: JOU
card_name('master of the feast', 'Master of the Feast').
card_type('master of the feast', 'Enchantment Creature — Demon').
card_types('master of the feast', ['Enchantment', 'Creature']).
card_subtypes('master of the feast', ['Demon']).
card_colors('master of the feast', ['B']).
card_text('master of the feast', 'Flying\nAt the beginning of your upkeep, each opponent draws a card.').
card_mana_cost('master of the feast', ['1', 'B', 'B']).
card_cmc('master of the feast', 3).
card_layout('master of the feast', 'normal').
card_power('master of the feast', 5).
card_toughness('master of the feast', 5).

% Found in: LEG
card_name('master of the hunt', 'Master of the Hunt').
card_type('master of the hunt', 'Creature — Human').
card_types('master of the hunt', ['Creature']).
card_subtypes('master of the hunt', ['Human']).
card_colors('master of the hunt', ['G']).
card_text('master of the hunt', '{2}{G}{G}: Put a 1/1 green Wolf creature token named Wolves of the Hunt onto the battlefield. It has \"bands with other creatures named Wolves of the Hunt.\" (Any creatures named Wolves of the Hunt can attack in a band as long as at least one has \"bands with other creatures named Wolves of the Hunt.\" Bands are blocked as a group. If at least two creatures named Wolves of the Hunt you control, one of which has \"bands with other creatures named Wolves of the Hunt,\" are blocking or being blocked by the same creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('master of the hunt', ['2', 'G', 'G']).
card_cmc('master of the hunt', 4).
card_layout('master of the hunt', 'normal').
card_power('master of the hunt', 2).
card_toughness('master of the hunt', 2).
card_reserved('master of the hunt').

% Found in: M13
card_name('master of the pearl trident', 'Master of the Pearl Trident').
card_type('master of the pearl trident', 'Creature — Merfolk').
card_types('master of the pearl trident', ['Creature']).
card_subtypes('master of the pearl trident', ['Merfolk']).
card_colors('master of the pearl trident', ['U']).
card_text('master of the pearl trident', 'Other Merfolk creatures you control get +1/+1 and have islandwalk. (They can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('master of the pearl trident', ['U', 'U']).
card_cmc('master of the pearl trident', 2).
card_layout('master of the pearl trident', 'normal').
card_power('master of the pearl trident', 2).
card_toughness('master of the pearl trident', 2).

% Found in: LGN
card_name('master of the veil', 'Master of the Veil').
card_type('master of the veil', 'Creature — Human Wizard').
card_types('master of the veil', ['Creature']).
card_subtypes('master of the veil', ['Human', 'Wizard']).
card_colors('master of the veil', ['U']).
card_text('master of the veil', 'Morph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Master of the Veil is turned face up, you may turn target creature with a morph ability face down.').
card_mana_cost('master of the veil', ['2', 'U', 'U']).
card_cmc('master of the veil', 4).
card_layout('master of the veil', 'normal').
card_power('master of the veil', 2).
card_toughness('master of the veil', 3).

% Found in: M10
card_name('master of the wild hunt', 'Master of the Wild Hunt').
card_type('master of the wild hunt', 'Creature — Human Shaman').
card_types('master of the wild hunt', ['Creature']).
card_subtypes('master of the wild hunt', ['Human', 'Shaman']).
card_colors('master of the wild hunt', ['G']).
card_text('master of the wild hunt', 'At the beginning of your upkeep, put a 2/2 green Wolf creature token onto the battlefield.\n{T}: Tap all untapped Wolf creatures you control. Each Wolf tapped this way deals damage equal to its power to target creature. That creature deals damage equal to its power divided as its controller chooses among any number of those Wolves.').
card_mana_cost('master of the wild hunt', ['2', 'G', 'G']).
card_cmc('master of the wild hunt', 4).
card_layout('master of the wild hunt', 'normal').
card_power('master of the wild hunt', 3).
card_toughness('master of the wild hunt', 3).

% Found in: VAN
card_name('master of the wild hunt avatar', 'Master of the Wild Hunt Avatar').
card_type('master of the wild hunt avatar', 'Vanguard').
card_types('master of the wild hunt avatar', ['Vanguard']).
card_subtypes('master of the wild hunt avatar', []).
card_colors('master of the wild hunt avatar', []).
card_text('master of the wild hunt avatar', '{2}{G}: Put a green creature token onto the battlefield that\'s a 2/2 Wolf, a 2/3 Antelope with forestwalk, a 3/2 Cat with shroud, or a 4/4 green Rhino with trample, chosen at random.').
card_layout('master of the wild hunt avatar', 'vanguard').

% Found in: THS
card_name('master of waves', 'Master of Waves').
card_type('master of waves', 'Creature — Merfolk Wizard').
card_types('master of waves', ['Creature']).
card_subtypes('master of waves', ['Merfolk', 'Wizard']).
card_colors('master of waves', ['U']).
card_text('master of waves', 'Protection from red\nElemental creatures you control get +1/+1.\nWhen Master of Waves enters the battlefield, put a number of 1/0 blue Elemental creature tokens onto the battlefield equal to your devotion to blue. (Each {U} in the mana costs of permanents you control counts toward your devotion to blue.)').
card_mana_cost('master of waves', ['3', 'U']).
card_cmc('master of waves', 4).
card_layout('master of waves', 'normal').
card_power('master of waves', 2).
card_toughness('master of waves', 1).

% Found in: NPH
card_name('master splicer', 'Master Splicer').
card_type('master splicer', 'Creature — Human Artificer').
card_types('master splicer', ['Creature']).
card_subtypes('master splicer', ['Human', 'Artificer']).
card_colors('master splicer', ['W']).
card_text('master splicer', 'When Master Splicer enters the battlefield, put a 3/3 colorless Golem artifact creature token onto the battlefield.\nGolem creatures you control get +1/+1.').
card_mana_cost('master splicer', ['3', 'W']).
card_cmc('master splicer', 4).
card_layout('master splicer', 'normal').
card_power('master splicer', 1).
card_toughness('master splicer', 1).

% Found in: KTK
card_name('master the way', 'Master the Way').
card_type('master the way', 'Sorcery').
card_types('master the way', ['Sorcery']).
card_subtypes('master the way', []).
card_colors('master the way', ['U', 'R']).
card_text('master the way', 'Draw a card. Master the Way deals damage to target creature or player equal to the number of cards in your hand.').
card_mana_cost('master the way', ['3', 'U', 'R']).
card_cmc('master the way', 5).
card_layout('master the way', 'normal').

% Found in: M12
card_name('master thief', 'Master Thief').
card_type('master thief', 'Creature — Human Rogue').
card_types('master thief', ['Creature']).
card_subtypes('master thief', ['Human', 'Rogue']).
card_colors('master thief', ['U']).
card_text('master thief', 'When Master Thief enters the battlefield, gain control of target artifact for as long as you control Master Thief.').
card_mana_cost('master thief', ['2', 'U', 'U']).
card_cmc('master thief', 4).
card_layout('master thief', 'normal').
card_power('master thief', 2).
card_toughness('master thief', 2).

% Found in: ARC, CON
card_name('master transmuter', 'Master Transmuter').
card_type('master transmuter', 'Artifact Creature — Human Artificer').
card_types('master transmuter', ['Artifact', 'Creature']).
card_subtypes('master transmuter', ['Human', 'Artificer']).
card_colors('master transmuter', ['U']).
card_text('master transmuter', '{U}, {T}, Return an artifact you control to its owner\'s hand: You may put an artifact card from your hand onto the battlefield.').
card_mana_cost('master transmuter', ['3', 'U']).
card_cmc('master transmuter', 4).
card_layout('master transmuter', 'normal').
card_power('master transmuter', 1).
card_toughness('master transmuter', 2).

% Found in: CMD, RAV
card_name('master warcraft', 'Master Warcraft').
card_type('master warcraft', 'Instant').
card_types('master warcraft', ['Instant']).
card_subtypes('master warcraft', []).
card_colors('master warcraft', ['W', 'R']).
card_text('master warcraft', 'Cast Master Warcraft only before attackers are declared.\nYou choose which creatures attack this turn.\nYou choose which creatures block this turn and how those creatures block.').
card_mana_cost('master warcraft', ['2', 'R/W', 'R/W']).
card_cmc('master warcraft', 4).
card_layout('master warcraft', 'normal').

% Found in: MBS, pWPN
card_name('master\'s call', 'Master\'s Call').
card_type('master\'s call', 'Instant').
card_types('master\'s call', ['Instant']).
card_subtypes('master\'s call', []).
card_colors('master\'s call', ['W']).
card_text('master\'s call', 'Put two 1/1 colorless Myr artifact creature tokens onto the battlefield.').
card_mana_cost('master\'s call', ['2', 'W']).
card_cmc('master\'s call', 3).
card_layout('master\'s call', 'normal').

% Found in: C14
card_name('masterwork of ingenuity', 'Masterwork of Ingenuity').
card_type('masterwork of ingenuity', 'Artifact — Equipment').
card_types('masterwork of ingenuity', ['Artifact']).
card_subtypes('masterwork of ingenuity', ['Equipment']).
card_colors('masterwork of ingenuity', []).
card_text('masterwork of ingenuity', 'You may have Masterwork of Ingenuity enter the battlefield as a copy of any Equipment on the battlefield.').
card_mana_cost('masterwork of ingenuity', ['1']).
card_cmc('masterwork of ingenuity', 1).
card_layout('masterwork of ingenuity', 'normal').

% Found in: FRF, FRF_UGIN
card_name('mastery of the unseen', 'Mastery of the Unseen').
card_type('mastery of the unseen', 'Enchantment').
card_types('mastery of the unseen', ['Enchantment']).
card_subtypes('mastery of the unseen', []).
card_colors('mastery of the unseen', ['W']).
card_text('mastery of the unseen', 'Whenever a permanent you control is turned face up, you gain 1 life for each creature you control.\n{3}{W}: Manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('mastery of the unseen', ['1', 'W']).
card_cmc('mastery of the unseen', 2).
card_layout('mastery of the unseen', 'normal').

% Found in: UDS, V10, VMA
card_name('masticore', 'Masticore').
card_type('masticore', 'Artifact Creature — Masticore').
card_types('masticore', ['Artifact', 'Creature']).
card_subtypes('masticore', ['Masticore']).
card_colors('masticore', []).
card_text('masticore', 'At the beginning of your upkeep, sacrifice Masticore unless you discard a card.\n{2}: Masticore deals 1 damage to target creature.\n{2}: Regenerate Masticore.').
card_mana_cost('masticore', ['4']).
card_cmc('masticore', 4).
card_layout('masticore', 'normal').
card_power('masticore', 4).
card_toughness('masticore', 4).
card_reserved('masticore').

% Found in: SOK
card_name('masumaro, first to live', 'Masumaro, First to Live').
card_type('masumaro, first to live', 'Legendary Creature — Spirit').
card_types('masumaro, first to live', ['Creature']).
card_subtypes('masumaro, first to live', ['Spirit']).
card_supertypes('masumaro, first to live', ['Legendary']).
card_colors('masumaro, first to live', ['G']).
card_text('masumaro, first to live', 'Masumaro, First to Live\'s power and toughness are each equal to twice the number of cards in your hand.').
card_mana_cost('masumaro, first to live', ['3', 'G', 'G', 'G']).
card_cmc('masumaro, first to live', 6).
card_layout('masumaro, first to live', 'normal').
card_power('masumaro, first to live', '*').
card_toughness('masumaro, first to live', '*').

% Found in: CON, MM2
card_name('matca rioters', 'Matca Rioters').
card_type('matca rioters', 'Creature — Human Warrior').
card_types('matca rioters', ['Creature']).
card_subtypes('matca rioters', ['Human', 'Warrior']).
card_colors('matca rioters', ['G']).
card_text('matca rioters', 'Domain — Matca Rioters\'s power and toughness are each equal to the number of basic land types among lands you control.').
card_mana_cost('matca rioters', ['2', 'G']).
card_cmc('matca rioters', 3).
card_layout('matca rioters', 'normal').
card_power('matca rioters', '*').
card_toughness('matca rioters', '*').

% Found in: VIS
card_name('matopi golem', 'Matopi Golem').
card_type('matopi golem', 'Artifact Creature — Golem').
card_types('matopi golem', ['Artifact', 'Creature']).
card_subtypes('matopi golem', ['Golem']).
card_colors('matopi golem', []).
card_text('matopi golem', '{1}: Regenerate Matopi Golem. When it regenerates this way, put a -1/-1 counter on it.').
card_mana_cost('matopi golem', ['5']).
card_cmc('matopi golem', 5).
card_layout('matopi golem', 'normal').
card_power('matopi golem', 3).
card_toughness('matopi golem', 3).

% Found in: SOK
card_name('matsu-tribe birdstalker', 'Matsu-Tribe Birdstalker').
card_type('matsu-tribe birdstalker', 'Creature — Snake Warrior Archer').
card_types('matsu-tribe birdstalker', ['Creature']).
card_subtypes('matsu-tribe birdstalker', ['Snake', 'Warrior', 'Archer']).
card_colors('matsu-tribe birdstalker', ['G']).
card_text('matsu-tribe birdstalker', 'Whenever Matsu-Tribe Birdstalker deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.\n{G}: Matsu-Tribe Birdstalker gains reach until end of turn. (It can block creatures with flying.)').
card_mana_cost('matsu-tribe birdstalker', ['2', 'G', 'G']).
card_cmc('matsu-tribe birdstalker', 4).
card_layout('matsu-tribe birdstalker', 'normal').
card_power('matsu-tribe birdstalker', 2).
card_toughness('matsu-tribe birdstalker', 2).

% Found in: CHK
card_name('matsu-tribe decoy', 'Matsu-Tribe Decoy').
card_type('matsu-tribe decoy', 'Creature — Snake Warrior').
card_types('matsu-tribe decoy', ['Creature']).
card_subtypes('matsu-tribe decoy', ['Snake', 'Warrior']).
card_colors('matsu-tribe decoy', ['G']).
card_text('matsu-tribe decoy', '{2}{G}: Target creature blocks Matsu-Tribe Decoy this turn if able.\nWhenever Matsu-Tribe Decoy deals combat damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('matsu-tribe decoy', ['2', 'G']).
card_cmc('matsu-tribe decoy', 3).
card_layout('matsu-tribe decoy', 'normal').
card_power('matsu-tribe decoy', 1).
card_toughness('matsu-tribe decoy', 3).

% Found in: BOK
card_name('matsu-tribe sniper', 'Matsu-Tribe Sniper').
card_type('matsu-tribe sniper', 'Creature — Snake Warrior Archer').
card_types('matsu-tribe sniper', ['Creature']).
card_subtypes('matsu-tribe sniper', ['Snake', 'Warrior', 'Archer']).
card_colors('matsu-tribe sniper', ['G']).
card_text('matsu-tribe sniper', '{T}: Matsu-Tribe Sniper deals 1 damage to target creature with flying.\nWhenever Matsu-Tribe Sniper deals damage to a creature, tap that creature and it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('matsu-tribe sniper', ['1', 'G']).
card_cmc('matsu-tribe sniper', 2).
card_layout('matsu-tribe sniper', 'normal').
card_power('matsu-tribe sniper', 1).
card_toughness('matsu-tribe sniper', 1).

% Found in: NPH, pWPN
card_name('maul splicer', 'Maul Splicer').
card_type('maul splicer', 'Creature — Human Artificer').
card_types('maul splicer', ['Creature']).
card_subtypes('maul splicer', ['Human', 'Artificer']).
card_colors('maul splicer', ['G']).
card_text('maul splicer', 'When Maul Splicer enters the battlefield, put two 3/3 colorless Golem artifact creature tokens onto the battlefield.\nGolem creatures you control have trample.').
card_mana_cost('maul splicer', ['6', 'G']).
card_cmc('maul splicer', 7).
card_layout('maul splicer', 'normal').
card_power('maul splicer', 1).
card_toughness('maul splicer', 1).

% Found in: DDK, ISD
card_name('mausoleum guard', 'Mausoleum Guard').
card_type('mausoleum guard', 'Creature — Human Scout').
card_types('mausoleum guard', ['Creature']).
card_subtypes('mausoleum guard', ['Human', 'Scout']).
card_colors('mausoleum guard', ['W']).
card_text('mausoleum guard', 'When Mausoleum Guard dies, put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_mana_cost('mausoleum guard', ['3', 'W']).
card_cmc('mausoleum guard', 4).
card_layout('mausoleum guard', 'normal').
card_power('mausoleum guard', 2).
card_toughness('mausoleum guard', 2).

% Found in: RAV
card_name('mausoleum turnkey', 'Mausoleum Turnkey').
card_type('mausoleum turnkey', 'Creature — Ogre Rogue').
card_types('mausoleum turnkey', ['Creature']).
card_subtypes('mausoleum turnkey', ['Ogre', 'Rogue']).
card_colors('mausoleum turnkey', ['B']).
card_text('mausoleum turnkey', 'When Mausoleum Turnkey enters the battlefield, return target creature card of an opponent\'s choice from your graveyard to your hand.').
card_mana_cost('mausoleum turnkey', ['3', 'B']).
card_cmc('mausoleum turnkey', 4).
card_layout('mausoleum turnkey', 'normal').
card_power('mausoleum turnkey', 3).
card_toughness('mausoleum turnkey', 2).

% Found in: ISD
card_name('maw of the mire', 'Maw of the Mire').
card_type('maw of the mire', 'Sorcery').
card_types('maw of the mire', ['Sorcery']).
card_subtypes('maw of the mire', []).
card_colors('maw of the mire', ['B']).
card_text('maw of the mire', 'Destroy target land. You gain 4 life.').
card_mana_cost('maw of the mire', ['4', 'B']).
card_cmc('maw of the mire', 5).
card_layout('maw of the mire', 'normal').

% Found in: DGM
card_name('maw of the obzedat', 'Maw of the Obzedat').
card_type('maw of the obzedat', 'Creature — Thrull').
card_types('maw of the obzedat', ['Creature']).
card_subtypes('maw of the obzedat', ['Thrull']).
card_colors('maw of the obzedat', ['W', 'B']).
card_text('maw of the obzedat', 'Sacrifice a creature: Creatures you control get +1/+1 until end of turn.').
card_mana_cost('maw of the obzedat', ['3', 'W', 'B']).
card_cmc('maw of the obzedat', 5).
card_layout('maw of the obzedat', 'normal').
card_power('maw of the obzedat', 3).
card_toughness('maw of the obzedat', 3).

% Found in: 7ED, TMP, TPR
card_name('mawcor', 'Mawcor').
card_type('mawcor', 'Creature — Beast').
card_types('mawcor', ['Creature']).
card_subtypes('mawcor', ['Beast']).
card_colors('mawcor', ['U']).
card_text('mawcor', 'Flying\n{T}: Mawcor deals 1 damage to target creature or player.').
card_mana_cost('mawcor', ['3', 'U', 'U']).
card_cmc('mawcor', 5).
card_layout('mawcor', 'normal').
card_power('mawcor', 3).
card_toughness('mawcor', 3).

% Found in: ARC
card_name('may civilization collapse', 'May Civilization Collapse').
card_type('may civilization collapse', 'Scheme').
card_types('may civilization collapse', ['Scheme']).
card_subtypes('may civilization collapse', []).
card_colors('may civilization collapse', []).
card_text('may civilization collapse', 'When you set this scheme in motion, target opponent chooses self or others. If that player chooses self, he or she sacrifices two lands. If the player chooses others, each of your other opponents sacrifices a land.').
card_layout('may civilization collapse', 'scheme').

% Found in: ALA, C13
card_name('mayael the anima', 'Mayael the Anima').
card_type('mayael the anima', 'Legendary Creature — Elf Shaman').
card_types('mayael the anima', ['Creature']).
card_subtypes('mayael the anima', ['Elf', 'Shaman']).
card_supertypes('mayael the anima', ['Legendary']).
card_colors('mayael the anima', ['W', 'R', 'G']).
card_text('mayael the anima', '{3}{R}{G}{W}, {T}: Look at the top five cards of your library. You may put a creature card with power 5 or greater from among them onto the battlefield. Put the rest on the bottom of your library in any order.').
card_mana_cost('mayael the anima', ['R', 'G', 'W']).
card_cmc('mayael the anima', 3).
card_layout('mayael the anima', 'normal').
card_power('mayael the anima', 2).
card_toughness('mayael the anima', 3).

% Found in: VAN
card_name('mayael the anima avatar', 'Mayael the Anima Avatar').
card_type('mayael the anima avatar', 'Vanguard').
card_types('mayael the anima avatar', ['Vanguard']).
card_subtypes('mayael the anima avatar', []).
card_colors('mayael the anima avatar', []).
card_text('mayael the anima avatar', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a creature card with power 5 or greater, put it into your hand. Otherwise, you may put it on the bottom of your library.').
card_layout('mayael the anima avatar', 'vanguard').

% Found in: ARB
card_name('mayael\'s aria', 'Mayael\'s Aria').
card_type('mayael\'s aria', 'Enchantment').
card_types('mayael\'s aria', ['Enchantment']).
card_subtypes('mayael\'s aria', []).
card_colors('mayael\'s aria', ['W', 'R', 'G']).
card_text('mayael\'s aria', 'At the beginning of your upkeep, put a +1/+1 counter on each creature you control if you control a creature with power 5 or greater. Then you gain 10 life if you control a creature with power 10 or greater. Then you win the game if you control a creature with power 20 or greater.').
card_mana_cost('mayael\'s aria', ['R', 'G', 'W']).
card_cmc('mayael\'s aria', 3).
card_layout('mayael\'s aria', 'normal').

% Found in: ISD, pPRE
card_name('mayor of avabruck', 'Mayor of Avabruck').
card_type('mayor of avabruck', 'Creature — Human Advisor Werewolf').
card_types('mayor of avabruck', ['Creature']).
card_subtypes('mayor of avabruck', ['Human', 'Advisor', 'Werewolf']).
card_colors('mayor of avabruck', ['G']).
card_text('mayor of avabruck', 'Other Human creatures you control get +1/+1.\nAt the beginning of each upkeep, if no spells were cast last turn, transform Mayor of Avabruck.').
card_mana_cost('mayor of avabruck', ['1', 'G']).
card_cmc('mayor of avabruck', 2).
card_layout('mayor of avabruck', 'double-faced').
card_power('mayor of avabruck', 1).
card_toughness('mayor of avabruck', 1).
card_sides('mayor of avabruck', 'howlpack alpha').

% Found in: DGM
card_name('maze abomination', 'Maze Abomination').
card_type('maze abomination', 'Creature — Elemental').
card_types('maze abomination', ['Creature']).
card_subtypes('maze abomination', ['Elemental']).
card_colors('maze abomination', ['B']).
card_text('maze abomination', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nMulticolored creatures you control have deathtouch.').
card_mana_cost('maze abomination', ['5', 'B']).
card_cmc('maze abomination', 6).
card_layout('maze abomination', 'normal').
card_power('maze abomination', 4).
card_toughness('maze abomination', 5).

% Found in: DGM
card_name('maze behemoth', 'Maze Behemoth').
card_type('maze behemoth', 'Creature — Elemental').
card_types('maze behemoth', ['Creature']).
card_subtypes('maze behemoth', ['Elemental']).
card_colors('maze behemoth', ['G']).
card_text('maze behemoth', 'Trample\nMulticolored creatures you control have trample.').
card_mana_cost('maze behemoth', ['5', 'G']).
card_cmc('maze behemoth', 6).
card_layout('maze behemoth', 'normal').
card_power('maze behemoth', 5).
card_toughness('maze behemoth', 4).

% Found in: DGM
card_name('maze glider', 'Maze Glider').
card_type('maze glider', 'Creature — Elemental').
card_types('maze glider', ['Creature']).
card_subtypes('maze glider', ['Elemental']).
card_colors('maze glider', ['U']).
card_text('maze glider', 'Flying\nMulticolored creatures you control have flying.').
card_mana_cost('maze glider', ['5', 'U']).
card_cmc('maze glider', 6).
card_layout('maze glider', 'normal').
card_power('maze glider', 3).
card_toughness('maze glider', 5).

% Found in: DRK, ME4, V12, pJGP
card_name('maze of ith', 'Maze of Ith').
card_type('maze of ith', 'Land').
card_types('maze of ith', ['Land']).
card_subtypes('maze of ith', []).
card_colors('maze of ith', []).
card_text('maze of ith', '{T}: Untap target attacking creature. Prevent all combat damage that would be dealt to and dealt by that creature this turn.').
card_layout('maze of ith', 'normal').

% Found in: TMP, TPR
card_name('maze of shadows', 'Maze of Shadows').
card_type('maze of shadows', 'Land').
card_types('maze of shadows', ['Land']).
card_subtypes('maze of shadows', []).
card_colors('maze of shadows', []).
card_text('maze of shadows', '{T}: Add {1} to your mana pool.\n{T}: Untap target attacking creature with shadow. Prevent all combat damage that would be dealt to and dealt by that creature this turn.').
card_layout('maze of shadows', 'normal').

% Found in: DGM
card_name('maze rusher', 'Maze Rusher').
card_type('maze rusher', 'Creature — Elemental').
card_types('maze rusher', ['Creature']).
card_subtypes('maze rusher', ['Elemental']).
card_colors('maze rusher', ['R']).
card_text('maze rusher', 'Haste\nMulticolored creatures you control have haste.').
card_mana_cost('maze rusher', ['5', 'R']).
card_cmc('maze rusher', 6).
card_layout('maze rusher', 'normal').
card_power('maze rusher', 6).
card_toughness('maze rusher', 3).

% Found in: DGM
card_name('maze sentinel', 'Maze Sentinel').
card_type('maze sentinel', 'Creature — Elemental').
card_types('maze sentinel', ['Creature']).
card_subtypes('maze sentinel', ['Elemental']).
card_colors('maze sentinel', ['W']).
card_text('maze sentinel', 'Vigilance\nMulticolored creatures you control have vigilance.').
card_mana_cost('maze sentinel', ['5', 'W']).
card_cmc('maze sentinel', 6).
card_layout('maze sentinel', 'normal').
card_power('maze sentinel', 3).
card_toughness('maze sentinel', 6).

% Found in: DGM, pPRE
card_name('maze\'s end', 'Maze\'s End').
card_type('maze\'s end', 'Land').
card_types('maze\'s end', ['Land']).
card_subtypes('maze\'s end', []).
card_colors('maze\'s end', []).
card_text('maze\'s end', 'Maze\'s End enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{3}, {T}, Return Maze\'s End to its owner\'s hand: Search your library for a Gate card, put it onto the battlefield, then shuffle your library. If you control ten or more Gates with different names, you win the game.').
card_layout('maze\'s end', 'normal').

% Found in: MMA, MOR
card_name('meadowboon', 'Meadowboon').
card_type('meadowboon', 'Creature — Elemental').
card_types('meadowboon', ['Creature']).
card_subtypes('meadowboon', ['Elemental']).
card_colors('meadowboon', ['W']).
card_text('meadowboon', 'When Meadowboon leaves the battlefield, put a +1/+1 counter on each creature target player controls.\nEvoke {3}{W} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('meadowboon', ['2', 'W', 'W']).
card_cmc('meadowboon', 4).
card_layout('meadowboon', 'normal').
card_power('meadowboon', 3).
card_toughness('meadowboon', 3).

% Found in: KTK
card_name('meandering towershell', 'Meandering Towershell').
card_type('meandering towershell', 'Creature — Turtle').
card_types('meandering towershell', ['Creature']).
card_subtypes('meandering towershell', ['Turtle']).
card_colors('meandering towershell', ['G']).
card_text('meandering towershell', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhenever Meandering Towershell attacks, exile it. Return it to the battlefield under your control tapped and attacking at the beginning of the declare attackers step on your next turn.').
card_mana_cost('meandering towershell', ['3', 'G', 'G']).
card_cmc('meandering towershell', 5).
card_layout('meandering towershell', 'normal').
card_power('meandering towershell', 5).
card_toughness('meandering towershell', 9).

% Found in: SOK
card_name('measure of wickedness', 'Measure of Wickedness').
card_type('measure of wickedness', 'Enchantment').
card_types('measure of wickedness', ['Enchantment']).
card_subtypes('measure of wickedness', []).
card_colors('measure of wickedness', ['B']).
card_text('measure of wickedness', 'At the beginning of your end step, sacrifice Measure of Wickedness and you lose 8 life.\nWhenever another card is put into your graveyard from anywhere, target opponent gains control of Measure of Wickedness.').
card_mana_cost('measure of wickedness', ['3', 'B']).
card_cmc('measure of wickedness', 4).
card_layout('measure of wickedness', 'normal').

% Found in: MIR, ONS
card_name('meddle', 'Meddle').
card_type('meddle', 'Instant').
card_types('meddle', ['Instant']).
card_subtypes('meddle', []).
card_colors('meddle', ['U']).
card_text('meddle', 'If target spell has only one target and that target is a creature, change that spell\'s target to another creature.').
card_mana_cost('meddle', ['1', 'U']).
card_cmc('meddle', 2).
card_layout('meddle', 'normal').

% Found in: UNH
card_name('meddling kids', 'Meddling Kids').
card_type('meddling kids', 'Creature — Human Child').
card_types('meddling kids', ['Creature']).
card_subtypes('meddling kids', ['Human', 'Child']).
card_colors('meddling kids', ['W', 'U']).
card_text('meddling kids', 'As Meddling Kids comes into play, choose a word with four or more letters.\nNonland cards with the chosen word in their text box can\'t be played.').
card_mana_cost('meddling kids', ['2', 'W', 'U']).
card_cmc('meddling kids', 4).
card_layout('meddling kids', 'normal').
card_power('meddling kids', 2).
card_toughness('meddling kids', 3).

% Found in: ARB, PLS, pJGP
card_name('meddling mage', 'Meddling Mage').
card_type('meddling mage', 'Creature — Human Wizard').
card_types('meddling mage', ['Creature']).
card_subtypes('meddling mage', ['Human', 'Wizard']).
card_colors('meddling mage', ['W', 'U']).
card_text('meddling mage', 'As Meddling Mage enters the battlefield, name a nonland card.\nThe named card can\'t be cast.').
card_mana_cost('meddling mage', ['W', 'U']).
card_cmc('meddling mage', 2).
card_layout('meddling mage', 'normal').
card_power('meddling mage', 2).
card_toughness('meddling mage', 2).

% Found in: EXO
card_name('medicine bag', 'Medicine Bag').
card_type('medicine bag', 'Artifact').
card_types('medicine bag', ['Artifact']).
card_subtypes('medicine bag', []).
card_colors('medicine bag', []).
card_text('medicine bag', '{1}, {T}, Discard a card: Regenerate target creature.').
card_mana_cost('medicine bag', ['3']).
card_cmc('medicine bag', 3).
card_layout('medicine bag', 'normal').

% Found in: SHM
card_name('medicine runner', 'Medicine Runner').
card_type('medicine runner', 'Creature — Elf Cleric').
card_types('medicine runner', ['Creature']).
card_subtypes('medicine runner', ['Elf', 'Cleric']).
card_colors('medicine runner', ['W', 'G']).
card_text('medicine runner', 'When Medicine Runner enters the battlefield, you may remove a counter from target permanent.').
card_mana_cost('medicine runner', ['1', 'G/W']).
card_cmc('medicine runner', 2).
card_layout('medicine runner', 'normal').
card_power('medicine runner', 2).
card_toughness('medicine runner', 1).

% Found in: TMP, TPR
card_name('meditate', 'Meditate').
card_type('meditate', 'Instant').
card_types('meditate', ['Instant']).
card_subtypes('meditate', []).
card_colors('meditate', ['U']).
card_text('meditate', 'Draw four cards. You skip your next turn.').
card_mana_cost('meditate', ['2', 'U']).
card_cmc('meditate', 3).
card_layout('meditate', 'normal').
card_reserved('meditate').

% Found in: M15
card_name('meditation puzzle', 'Meditation Puzzle').
card_type('meditation puzzle', 'Instant').
card_types('meditation puzzle', ['Instant']).
card_subtypes('meditation puzzle', []).
card_colors('meditation puzzle', ['W']).
card_text('meditation puzzle', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nYou gain 8 life.').
card_mana_cost('meditation puzzle', ['3', 'W', 'W']).
card_cmc('meditation puzzle', 5).
card_layout('meditation puzzle', 'normal').

% Found in: THS
card_name('medomai the ageless', 'Medomai the Ageless').
card_type('medomai the ageless', 'Legendary Creature — Sphinx').
card_types('medomai the ageless', ['Creature']).
card_subtypes('medomai the ageless', ['Sphinx']).
card_supertypes('medomai the ageless', ['Legendary']).
card_colors('medomai the ageless', ['W', 'U']).
card_text('medomai the ageless', 'Flying\nWhenever Medomai the Ageless deals combat damage to a player, take an extra turn after this one.\nMedomai the Ageless can\'t attack during extra turns.').
card_mana_cost('medomai the ageless', ['4', 'W', 'U']).
card_cmc('medomai the ageless', 6).
card_layout('medomai the ageless', 'normal').
card_power('medomai the ageless', 4).
card_toughness('medomai the ageless', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, LEA, LEB
card_name('meekstone', 'Meekstone').
card_type('meekstone', 'Artifact').
card_types('meekstone', ['Artifact']).
card_subtypes('meekstone', []).
card_colors('meekstone', []).
card_text('meekstone', 'Creatures with power 3 or greater don\'t untap during their controllers\' untap steps.').
card_mana_cost('meekstone', ['1']).
card_cmc('meekstone', 1).
card_layout('meekstone', 'normal').

% Found in: M14, pPRE
card_name('megantic sliver', 'Megantic Sliver').
card_type('megantic sliver', 'Creature — Sliver').
card_types('megantic sliver', ['Creature']).
card_subtypes('megantic sliver', ['Sliver']).
card_colors('megantic sliver', ['G']).
card_text('megantic sliver', 'Sliver creatures you control get +3/+3.').
card_mana_cost('megantic sliver', ['5', 'G']).
card_cmc('megantic sliver', 6).
card_layout('megantic sliver', 'normal').
card_power('megantic sliver', 3).
card_toughness('megantic sliver', 3).

% Found in: MMQ
card_name('megatherium', 'Megatherium').
card_type('megatherium', 'Creature — Beast').
card_types('megatherium', ['Creature']).
card_subtypes('megatherium', ['Beast']).
card_colors('megatherium', ['G']).
card_text('megatherium', 'Trample\nWhen Megatherium enters the battlefield, sacrifice it unless you pay {1} for each card in your hand.').
card_mana_cost('megatherium', ['2', 'G']).
card_cmc('megatherium', 3).
card_layout('megatherium', 'normal').
card_power('megatherium', 4).
card_toughness('megatherium', 4).

% Found in: MRD
card_name('megatog', 'Megatog').
card_type('megatog', 'Creature — Atog').
card_types('megatog', ['Creature']).
card_subtypes('megatog', ['Atog']).
card_colors('megatog', ['R']).
card_text('megatog', 'Sacrifice an artifact: Megatog gets +3/+3 and gains trample until end of turn.').
card_mana_cost('megatog', ['4', 'R', 'R']).
card_cmc('megatog', 6).
card_layout('megatog', 'normal').
card_power('megatog', 3).
card_toughness('megatog', 4).

% Found in: CON
card_name('meglonoth', 'Meglonoth').
card_type('meglonoth', 'Creature — Beast').
card_types('meglonoth', ['Creature']).
card_subtypes('meglonoth', ['Beast']).
card_colors('meglonoth', ['W', 'R', 'G']).
card_text('meglonoth', 'Vigilance, trample\nWhenever Meglonoth blocks a creature, Meglonoth deals damage to that creature\'s controller equal to Meglonoth\'s power.').
card_mana_cost('meglonoth', ['3', 'R', 'G', 'W']).
card_cmc('meglonoth', 6).
card_layout('meglonoth', 'normal').
card_power('meglonoth', 6).
card_toughness('meglonoth', 6).

% Found in: 10E, 7ED, 8ED, 9ED, DPA, M10, STH
card_name('megrim', 'Megrim').
card_type('megrim', 'Enchantment').
card_types('megrim', ['Enchantment']).
card_subtypes('megrim', []).
card_colors('megrim', ['B']).
card_text('megrim', 'Whenever an opponent discards a card, Megrim deals 2 damage to that player.').
card_mana_cost('megrim', ['2', 'B']).
card_cmc('megrim', 3).
card_layout('megrim', 'normal').

% Found in: SOK
card_name('meishin, the mind cage', 'Meishin, the Mind Cage').
card_type('meishin, the mind cage', 'Legendary Enchantment').
card_types('meishin, the mind cage', ['Enchantment']).
card_subtypes('meishin, the mind cage', []).
card_supertypes('meishin, the mind cage', ['Legendary']).
card_colors('meishin, the mind cage', ['U']).
card_text('meishin, the mind cage', 'All creatures get -X/-0, where X is the number of cards in your hand.').
card_mana_cost('meishin, the mind cage', ['4', 'U', 'U', 'U']).
card_cmc('meishin, the mind cage', 7).
card_layout('meishin, the mind cage', 'normal').

% Found in: PLC
card_name('melancholy', 'Melancholy').
card_type('melancholy', 'Enchantment — Aura').
card_types('melancholy', ['Enchantment']).
card_subtypes('melancholy', ['Aura']).
card_colors('melancholy', ['B']).
card_text('melancholy', 'Enchant creature\nWhen Melancholy enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nAt the beginning of your upkeep, sacrifice Melancholy unless you pay {B}.').
card_mana_cost('melancholy', ['2', 'B']).
card_cmc('melancholy', 3).
card_layout('melancholy', 'normal').

% Found in: ICE
card_name('melee', 'Melee').
card_type('melee', 'Instant').
card_types('melee', ['Instant']).
card_subtypes('melee', []).
card_colors('melee', ['R']).
card_text('melee', 'Cast Melee only during your turn and only during combat before blockers are declared.\nYou choose which creatures block this combat and how those creatures block.\nWhenever a creature attacks and isn\'t blocked this combat, untap it and remove it from combat.').
card_mana_cost('melee', ['4', 'R']).
card_cmc('melee', 5).
card_layout('melee', 'normal').

% Found in: DGM, pMGD
card_name('melek, izzet paragon', 'Melek, Izzet Paragon').
card_type('melek, izzet paragon', 'Legendary Creature — Weird Wizard').
card_types('melek, izzet paragon', ['Creature']).
card_subtypes('melek, izzet paragon', ['Weird', 'Wizard']).
card_supertypes('melek, izzet paragon', ['Legendary']).
card_colors('melek, izzet paragon', ['U', 'R']).
card_text('melek, izzet paragon', 'Play with the top card of your library revealed.\nYou may cast the top card of your library if it\'s an instant or sorcery card.\nWhenever you cast an instant or sorcery spell from your library, copy it. You may choose new targets for the copy.').
card_mana_cost('melek, izzet paragon', ['4', 'U', 'R']).
card_cmc('melek, izzet paragon', 6).
card_layout('melek, izzet paragon', 'normal').
card_power('melek, izzet paragon', 2).
card_toughness('melek, izzet paragon', 4).

% Found in: MIR
card_name('melesse spirit', 'Melesse Spirit').
card_type('melesse spirit', 'Creature — Angel Spirit').
card_types('melesse spirit', ['Creature']).
card_subtypes('melesse spirit', ['Angel', 'Spirit']).
card_colors('melesse spirit', ['W']).
card_text('melesse spirit', 'Flying, protection from black').
card_mana_cost('melesse spirit', ['3', 'W', 'W']).
card_cmc('melesse spirit', 5).
card_layout('melesse spirit', 'normal').
card_power('melesse spirit', 3).
card_toughness('melesse spirit', 3).

% Found in: BNG
card_name('meletis astronomer', 'Meletis Astronomer').
card_type('meletis astronomer', 'Creature — Human Wizard').
card_types('meletis astronomer', ['Creature']).
card_subtypes('meletis astronomer', ['Human', 'Wizard']).
card_colors('meletis astronomer', ['U']).
card_text('meletis astronomer', 'Heroic — Whenever you cast a spell that targets Meletis Astronomer, look at the top three cards of your library. You may reveal an enchantment card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('meletis astronomer', ['1', 'U']).
card_cmc('meletis astronomer', 2).
card_layout('meletis astronomer', 'normal').
card_power('meletis astronomer', 1).
card_toughness('meletis astronomer', 3).

% Found in: THS
card_name('meletis charlatan', 'Meletis Charlatan').
card_type('meletis charlatan', 'Creature — Human Wizard').
card_types('meletis charlatan', ['Creature']).
card_subtypes('meletis charlatan', ['Human', 'Wizard']).
card_colors('meletis charlatan', ['U']).
card_text('meletis charlatan', '{2}{U}, {T}: The controller of target instant or sorcery spell copies it. That player may choose new targets for the copy.').
card_mana_cost('meletis charlatan', ['2', 'U']).
card_cmc('meletis charlatan', 3).
card_layout('meletis charlatan', 'normal').
card_power('meletis charlatan', 2).
card_toughness('meletis charlatan', 3).

% Found in: MBS
card_name('melira\'s keepers', 'Melira\'s Keepers').
card_type('melira\'s keepers', 'Creature — Human Warrior').
card_types('melira\'s keepers', ['Creature']).
card_subtypes('melira\'s keepers', ['Human', 'Warrior']).
card_colors('melira\'s keepers', ['G']).
card_text('melira\'s keepers', 'Melira\'s Keepers can\'t have counters placed on it.').
card_mana_cost('melira\'s keepers', ['4', 'G']).
card_cmc('melira\'s keepers', 5).
card_layout('melira\'s keepers', 'normal').
card_power('melira\'s keepers', 4).
card_toughness('melira\'s keepers', 4).

% Found in: NPH
card_name('melira, sylvok outcast', 'Melira, Sylvok Outcast').
card_type('melira, sylvok outcast', 'Legendary Creature — Human Scout').
card_types('melira, sylvok outcast', ['Creature']).
card_subtypes('melira, sylvok outcast', ['Human', 'Scout']).
card_supertypes('melira, sylvok outcast', ['Legendary']).
card_colors('melira, sylvok outcast', ['G']).
card_text('melira, sylvok outcast', 'You can\'t get poison counters.\nCreatures you control can\'t have -1/-1 counters placed on them.\nCreatures your opponents control lose infect.').
card_mana_cost('melira, sylvok outcast', ['1', 'G']).
card_cmc('melira, sylvok outcast', 2).
card_layout('melira, sylvok outcast', 'normal').
card_power('melira, sylvok outcast', 2).
card_toughness('melira, sylvok outcast', 2).

% Found in: CHK, MMA
card_name('meloku the clouded mirror', 'Meloku the Clouded Mirror').
card_type('meloku the clouded mirror', 'Legendary Creature — Moonfolk Wizard').
card_types('meloku the clouded mirror', ['Creature']).
card_subtypes('meloku the clouded mirror', ['Moonfolk', 'Wizard']).
card_supertypes('meloku the clouded mirror', ['Legendary']).
card_colors('meloku the clouded mirror', ['U']).
card_text('meloku the clouded mirror', 'Flying\n{1}, Return a land you control to its owner\'s hand: Put a 1/1 blue Illusion creature token with flying onto the battlefield.').
card_mana_cost('meloku the clouded mirror', ['4', 'U']).
card_cmc('meloku the clouded mirror', 5).
card_layout('meloku the clouded mirror', 'normal').
card_power('meloku the clouded mirror', 2).
card_toughness('meloku the clouded mirror', 4).

% Found in: SOM
card_name('melt terrain', 'Melt Terrain').
card_type('melt terrain', 'Sorcery').
card_types('melt terrain', ['Sorcery']).
card_subtypes('melt terrain', []).
card_colors('melt terrain', ['R']).
card_text('melt terrain', 'Destroy target land. Melt Terrain deals 2 damage to that land\'s controller.').
card_mana_cost('melt terrain', ['2', 'R', 'R']).
card_cmc('melt terrain', 4).
card_layout('melt terrain', 'normal').

% Found in: USG
card_name('meltdown', 'Meltdown').
card_type('meltdown', 'Sorcery').
card_types('meltdown', ['Sorcery']).
card_subtypes('meltdown', []).
card_colors('meltdown', ['R']).
card_text('meltdown', 'Destroy each artifact with converted mana cost X or less.').
card_mana_cost('meltdown', ['X', 'R']).
card_cmc('meltdown', 1).
card_layout('meltdown', 'normal').

% Found in: ICE
card_name('melting', 'Melting').
card_type('melting', 'Enchantment').
card_types('melting', ['Enchantment']).
card_subtypes('melting', []).
card_colors('melting', ['R']).
card_text('melting', 'All lands are no longer snow.').
card_mana_cost('melting', ['3', 'R']).
card_cmc('melting', 4).
card_layout('melting', 'normal').

% Found in: ARC, DST
card_name('memnarch', 'Memnarch').
card_type('memnarch', 'Legendary Artifact Creature — Wizard').
card_types('memnarch', ['Artifact', 'Creature']).
card_subtypes('memnarch', ['Wizard']).
card_supertypes('memnarch', ['Legendary']).
card_colors('memnarch', []).
card_text('memnarch', '{1}{U}{U}: Target permanent becomes an artifact in addition to its other types. (This effect lasts indefinitely.)\n{3}{U}: Gain control of target artifact. (This effect lasts indefinitely.)').
card_mana_cost('memnarch', ['7']).
card_cmc('memnarch', 7).
card_layout('memnarch', 'normal').
card_power('memnarch', 4).
card_toughness('memnarch', 5).

% Found in: SOM, pMGD
card_name('memnite', 'Memnite').
card_type('memnite', 'Artifact Creature — Construct').
card_types('memnite', ['Artifact', 'Creature']).
card_subtypes('memnite', ['Construct']).
card_colors('memnite', []).
card_text('memnite', '').
card_mana_cost('memnite', ['0']).
card_cmc('memnite', 0).
card_layout('memnite', 'normal').
card_power('memnite', 1).
card_toughness('memnite', 1).

% Found in: SOM, pMEI
card_name('memoricide', 'Memoricide').
card_type('memoricide', 'Sorcery').
card_types('memoricide', ['Sorcery']).
card_subtypes('memoricide', []).
card_colors('memoricide', ['B']).
card_text('memoricide', 'Name a nonland card. Search target player\'s graveyard, hand, and library for any number of cards with that name and exile them. Then that player shuffles his or her library.').
card_mana_cost('memoricide', ['3', 'B']).
card_cmc('memoricide', 4).
card_layout('memoricide', 'normal').

% Found in: EXO
card_name('memory crystal', 'Memory Crystal').
card_type('memory crystal', 'Artifact').
card_types('memory crystal', ['Artifact']).
card_subtypes('memory crystal', []).
card_colors('memory crystal', []).
card_text('memory crystal', 'Buyback costs cost {2} less.').
card_mana_cost('memory crystal', ['3']).
card_cmc('memory crystal', 3).
card_layout('memory crystal', 'normal').

% Found in: ALA, CMD
card_name('memory erosion', 'Memory Erosion').
card_type('memory erosion', 'Enchantment').
card_types('memory erosion', ['Enchantment']).
card_subtypes('memory erosion', []).
card_colors('memory erosion', ['U']).
card_text('memory erosion', 'Whenever an opponent casts a spell, that player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('memory erosion', ['1', 'U', 'U']).
card_cmc('memory erosion', 3).
card_layout('memory erosion', 'normal').

% Found in: ULG, V10, VMA
card_name('memory jar', 'Memory Jar').
card_type('memory jar', 'Artifact').
card_types('memory jar', ['Artifact']).
card_subtypes('memory jar', []).
card_colors('memory jar', []).
card_text('memory jar', '{T}, Sacrifice Memory Jar: Each player exiles all cards from his or her hand face down and draws seven cards. At the beginning of the next end step, each player discards his or her hand and returns to his or her hand each card he or she exiled this way.').
card_mana_cost('memory jar', ['5']).
card_cmc('memory jar', 5).
card_layout('memory jar', 'normal').
card_reserved('memory jar').

% Found in: 5ED, 6ED, 7ED, DDM, HML, MIR, pJGP
card_name('memory lapse', 'Memory Lapse').
card_type('memory lapse', 'Instant').
card_types('memory lapse', ['Instant']).
card_subtypes('memory lapse', []).
card_colors('memory lapse', ['U']).
card_text('memory lapse', 'Counter target spell. If that spell is countered this way, put it on top of its owner\'s library instead of into that player\'s graveyard.').
card_mana_cost('memory lapse', ['1', 'U']).
card_cmc('memory lapse', 2).
card_layout('memory lapse', 'normal').

% Found in: SHM
card_name('memory plunder', 'Memory Plunder').
card_type('memory plunder', 'Instant').
card_types('memory plunder', ['Instant']).
card_subtypes('memory plunder', []).
card_colors('memory plunder', ['U', 'B']).
card_text('memory plunder', 'You may cast target instant or sorcery card from an opponent\'s graveyard without paying its mana cost.').
card_mana_cost('memory plunder', ['U/B', 'U/B', 'U/B', 'U/B']).
card_cmc('memory plunder', 4).
card_layout('memory plunder', 'normal').

% Found in: SHM
card_name('memory sluice', 'Memory Sluice').
card_type('memory sluice', 'Sorcery').
card_types('memory sluice', ['Sorcery']).
card_subtypes('memory sluice', []).
card_colors('memory sluice', ['U', 'B']).
card_text('memory sluice', 'Target player puts the top four cards of his or her library into his or her graveyard.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('memory sluice', ['U/B']).
card_cmc('memory sluice', 1).
card_layout('memory sluice', 'normal').

% Found in: ISD
card_name('memory\'s journey', 'Memory\'s Journey').
card_type('memory\'s journey', 'Instant').
card_types('memory\'s journey', ['Instant']).
card_subtypes('memory\'s journey', []).
card_colors('memory\'s journey', ['U']).
card_text('memory\'s journey', 'Target player shuffles up to three target cards from his or her graveyard into his or her library.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('memory\'s journey', ['1', 'U']).
card_cmc('memory\'s journey', 2).
card_layout('memory\'s journey', 'normal').

% Found in: HOP, ONS
card_name('menacing ogre', 'Menacing Ogre').
card_type('menacing ogre', 'Creature — Ogre').
card_types('menacing ogre', ['Creature']).
card_subtypes('menacing ogre', ['Ogre']).
card_colors('menacing ogre', ['R']).
card_text('menacing ogre', 'Trample, haste\nWhen Menacing Ogre enters the battlefield, each player secretly chooses a number. Then those numbers are revealed. Each player with the highest number loses that much life. If you are one of those players, put two +1/+1 counters on Menacing Ogre.').
card_mana_cost('menacing ogre', ['3', 'R', 'R']).
card_cmc('menacing ogre', 5).
card_layout('menacing ogre', 'normal').
card_power('menacing ogre', 3).
card_toughness('menacing ogre', 3).

% Found in: 9ED, BOK
card_name('mending hands', 'Mending Hands').
card_type('mending hands', 'Instant').
card_types('mending hands', ['Instant']).
card_subtypes('mending hands', []).
card_colors('mending hands', ['W']).
card_text('mending hands', 'Prevent the next 4 damage that would be dealt to target creature or player this turn.').
card_mana_cost('mending hands', ['W']).
card_cmc('mending hands', 1).
card_layout('mending hands', 'normal').

% Found in: DGM
card_name('mending touch', 'Mending Touch').
card_type('mending touch', 'Instant').
card_types('mending touch', ['Instant']).
card_subtypes('mending touch', []).
card_colors('mending touch', ['G']).
card_text('mending touch', 'Regenerate target creature.').
card_mana_cost('mending touch', ['G']).
card_cmc('mending touch', 1).
card_layout('mending touch', 'normal').

% Found in: ME3, PTK
card_name('meng huo\'s horde', 'Meng Huo\'s Horde').
card_type('meng huo\'s horde', 'Creature — Human Soldier').
card_types('meng huo\'s horde', ['Creature']).
card_subtypes('meng huo\'s horde', ['Human', 'Soldier']).
card_colors('meng huo\'s horde', ['G']).
card_text('meng huo\'s horde', '').
card_mana_cost('meng huo\'s horde', ['4', 'G']).
card_cmc('meng huo\'s horde', 5).
card_layout('meng huo\'s horde', 'normal').
card_power('meng huo\'s horde', 4).
card_toughness('meng huo\'s horde', 5).

% Found in: ME3, PTK
card_name('meng huo, barbarian king', 'Meng Huo, Barbarian King').
card_type('meng huo, barbarian king', 'Legendary Creature — Human Barbarian Soldier').
card_types('meng huo, barbarian king', ['Creature']).
card_subtypes('meng huo, barbarian king', ['Human', 'Barbarian', 'Soldier']).
card_supertypes('meng huo, barbarian king', ['Legendary']).
card_colors('meng huo, barbarian king', ['G']).
card_text('meng huo, barbarian king', 'Other green creatures you control get +1/+1.').
card_mana_cost('meng huo, barbarian king', ['3', 'G', 'G']).
card_cmc('meng huo, barbarian king', 5).
card_layout('meng huo, barbarian king', 'normal').
card_power('meng huo, barbarian king', 4).
card_toughness('meng huo, barbarian king', 4).

% Found in: AVR
card_name('mental agony', 'Mental Agony').
card_type('mental agony', 'Sorcery').
card_types('mental agony', ['Sorcery']).
card_subtypes('mental agony', []).
card_colors('mental agony', ['B']).
card_text('mental agony', 'Target player discards two cards and loses 2 life.').
card_mana_cost('mental agony', ['3', 'B']).
card_cmc('mental agony', 4).
card_layout('mental agony', 'normal').

% Found in: UDS
card_name('mental discipline', 'Mental Discipline').
card_type('mental discipline', 'Enchantment').
card_types('mental discipline', ['Enchantment']).
card_subtypes('mental discipline', []).
card_colors('mental discipline', ['U']).
card_text('mental discipline', '{1}{U}, Discard a card: Draw a card.').
card_mana_cost('mental discipline', ['1', 'U', 'U']).
card_cmc('mental discipline', 3).
card_layout('mental discipline', 'normal').

% Found in: NPH
card_name('mental misstep', 'Mental Misstep').
card_type('mental misstep', 'Instant').
card_types('mental misstep', ['Instant']).
card_subtypes('mental misstep', []).
card_colors('mental misstep', ['U']).
card_text('mental misstep', '({U/P} can be paid with either {U} or 2 life.)\nCounter target spell with converted mana cost 1.').
card_mana_cost('mental misstep', ['U/P']).
card_cmc('mental misstep', 1).
card_layout('mental misstep', 'normal').

% Found in: JUD
card_name('mental note', 'Mental Note').
card_type('mental note', 'Instant').
card_types('mental note', ['Instant']).
card_subtypes('mental note', []).
card_colors('mental note', ['U']).
card_text('mental note', 'Put the top two cards of your library into your graveyard.\nDraw a card.').
card_mana_cost('mental note', ['U']).
card_cmc('mental note', 1).
card_layout('mental note', 'normal').

% Found in: GTC
card_name('mental vapors', 'Mental Vapors').
card_type('mental vapors', 'Sorcery').
card_types('mental vapors', ['Sorcery']).
card_subtypes('mental vapors', []).
card_colors('mental vapors', ['B']).
card_text('mental vapors', 'Target player discards a card.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('mental vapors', ['3', 'B']).
card_cmc('mental vapors', 4).
card_layout('mental vapors', 'normal').

% Found in: C14, ISD
card_name('mentor of the meek', 'Mentor of the Meek').
card_type('mentor of the meek', 'Creature — Human Soldier').
card_types('mentor of the meek', ['Creature']).
card_subtypes('mentor of the meek', ['Human', 'Soldier']).
card_colors('mentor of the meek', ['W']).
card_text('mentor of the meek', 'Whenever another creature with power 2 or less enters the battlefield under your control, you may pay {1}. If you do, draw a card.').
card_mana_cost('mentor of the meek', ['2', 'W']).
card_cmc('mentor of the meek', 3).
card_layout('mentor of the meek', 'normal').
card_power('mentor of the meek', 2).
card_toughness('mentor of the meek', 2).

% Found in: 5DN
card_name('mephidross vampire', 'Mephidross Vampire').
card_type('mephidross vampire', 'Creature — Vampire').
card_types('mephidross vampire', ['Creature']).
card_subtypes('mephidross vampire', ['Vampire']).
card_colors('mephidross vampire', ['B']).
card_text('mephidross vampire', 'Flying\nEach creature you control is a Vampire in addition to its other creature types and has \"Whenever this creature deals damage to a creature, put a +1/+1 counter on this creature.\"').
card_mana_cost('mephidross vampire', ['4', 'B', 'B']).
card_cmc('mephidross vampire', 6).
card_layout('mephidross vampire', 'normal').
card_power('mephidross vampire', 3).
card_toughness('mephidross vampire', 4).

% Found in: DST
card_name('mephitic ooze', 'Mephitic Ooze').
card_type('mephitic ooze', 'Creature — Ooze').
card_types('mephitic ooze', ['Creature']).
card_subtypes('mephitic ooze', ['Ooze']).
card_colors('mephitic ooze', ['B']).
card_text('mephitic ooze', 'Mephitic Ooze gets +1/+0 for each artifact you control.\nWhenever Mephitic Ooze deals combat damage to a creature, destroy that creature. The creature can\'t be regenerated.').
card_mana_cost('mephitic ooze', ['4', 'B']).
card_cmc('mephitic ooze', 5).
card_layout('mephitic ooze', 'normal').
card_power('mephitic ooze', 0).
card_toughness('mephitic ooze', 5).

% Found in: KTK
card_name('mer-ek nightblade', 'Mer-Ek Nightblade').
card_type('mer-ek nightblade', 'Creature — Orc Assassin').
card_types('mer-ek nightblade', ['Creature']).
card_subtypes('mer-ek nightblade', ['Orc', 'Assassin']).
card_colors('mer-ek nightblade', ['B']).
card_text('mer-ek nightblade', 'Outlast {B} ({B}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has deathtouch.').
card_mana_cost('mer-ek nightblade', ['3', 'B']).
card_cmc('mer-ek nightblade', 4).
card_layout('mer-ek nightblade', 'normal').
card_power('mer-ek nightblade', 2).
card_toughness('mer-ek nightblade', 3).

% Found in: MMQ
card_name('mercadia\'s downfall', 'Mercadia\'s Downfall').
card_type('mercadia\'s downfall', 'Instant').
card_types('mercadia\'s downfall', ['Instant']).
card_subtypes('mercadia\'s downfall', []).
card_colors('mercadia\'s downfall', ['R']).
card_text('mercadia\'s downfall', 'Each attacking creature gets +1/+0 until end of turn for each nonbasic land defending player controls.').
card_mana_cost('mercadia\'s downfall', ['2', 'R']).
card_cmc('mercadia\'s downfall', 3).
card_layout('mercadia\'s downfall', 'normal').

% Found in: MMQ
card_name('mercadian atlas', 'Mercadian Atlas').
card_type('mercadian atlas', 'Artifact').
card_types('mercadian atlas', ['Artifact']).
card_subtypes('mercadian atlas', []).
card_colors('mercadian atlas', []).
card_text('mercadian atlas', 'At the beginning of your end step, if you didn\'t play a land this turn, you may draw a card.').
card_mana_cost('mercadian atlas', ['5']).
card_cmc('mercadian atlas', 5).
card_layout('mercadian atlas', 'normal').

% Found in: MMQ
card_name('mercadian bazaar', 'Mercadian Bazaar').
card_type('mercadian bazaar', 'Land').
card_types('mercadian bazaar', ['Land']).
card_subtypes('mercadian bazaar', []).
card_colors('mercadian bazaar', []).
card_text('mercadian bazaar', 'Mercadian Bazaar enters the battlefield tapped.\n{T}: Put a storage counter on Mercadian Bazaar.\n{T}, Remove any number of storage counters from Mercadian Bazaar: Add {R} to your mana pool for each storage counter removed this way.').
card_layout('mercadian bazaar', 'normal').

% Found in: MMQ
card_name('mercadian lift', 'Mercadian Lift').
card_type('mercadian lift', 'Artifact').
card_types('mercadian lift', ['Artifact']).
card_subtypes('mercadian lift', []).
card_colors('mercadian lift', []).
card_text('mercadian lift', '{1}, {T}: Put a winch counter on Mercadian Lift.\n{T}, Remove X winch counters from Mercadian Lift: You may put a creature card with converted mana cost X from your hand onto the battlefield.').
card_mana_cost('mercadian lift', ['2']).
card_cmc('mercadian lift', 2).
card_layout('mercadian lift', 'normal').

% Found in: ICE
card_name('mercenaries', 'Mercenaries').
card_type('mercenaries', 'Creature — Human Mercenary').
card_types('mercenaries', ['Creature']).
card_subtypes('mercenaries', ['Human', 'Mercenary']).
card_colors('mercenaries', ['W']).
card_text('mercenaries', '{3}: The next time Mercenaries would deal damage to you this turn, prevent that damage. Any player may activate this ability.').
card_mana_cost('mercenaries', ['3', 'W']).
card_cmc('mercenaries', 4).
card_layout('mercenaries', 'normal').
card_power('mercenaries', 3).
card_toughness('mercenaries', 3).
card_reserved('mercenaries').

% Found in: PCY
card_name('mercenary informer', 'Mercenary Informer').
card_type('mercenary informer', 'Creature — Human Rebel Mercenary').
card_types('mercenary informer', ['Creature']).
card_subtypes('mercenary informer', ['Human', 'Rebel', 'Mercenary']).
card_colors('mercenary informer', ['W']).
card_text('mercenary informer', 'Mercenary Informer can\'t be the target of black spells or abilities from black sources.\n{2}{W}: Put target nontoken Mercenary on the bottom of its owner\'s library.').
card_mana_cost('mercenary informer', ['2', 'W']).
card_cmc('mercenary informer', 3).
card_layout('mercenary informer', 'normal').
card_power('mercenary informer', 2).
card_toughness('mercenary informer', 1).

% Found in: POR
card_name('mercenary knight', 'Mercenary Knight').
card_type('mercenary knight', 'Creature — Human Mercenary Knight').
card_types('mercenary knight', ['Creature']).
card_subtypes('mercenary knight', ['Human', 'Mercenary', 'Knight']).
card_colors('mercenary knight', ['B']).
card_text('mercenary knight', 'When Mercenary Knight enters the battlefield, sacrifice it unless you discard a creature card.').
card_mana_cost('mercenary knight', ['2', 'B']).
card_cmc('mercenary knight', 3).
card_layout('mercenary knight', 'normal').
card_power('mercenary knight', 4).
card_toughness('mercenary knight', 4).

% Found in: 8ED, LGN
card_name('merchant of secrets', 'Merchant of Secrets').
card_type('merchant of secrets', 'Creature — Human Wizard').
card_types('merchant of secrets', ['Creature']).
card_subtypes('merchant of secrets', ['Human', 'Wizard']).
card_colors('merchant of secrets', ['U']).
card_text('merchant of secrets', 'When Merchant of Secrets enters the battlefield, draw a card.').
card_mana_cost('merchant of secrets', ['2', 'U']).
card_cmc('merchant of secrets', 3).
card_layout('merchant of secrets', 'normal').
card_power('merchant of secrets', 1).
card_toughness('merchant of secrets', 1).

% Found in: 8ED, HML
card_name('merchant scroll', 'Merchant Scroll').
card_type('merchant scroll', 'Sorcery').
card_types('merchant scroll', ['Sorcery']).
card_subtypes('merchant scroll', []).
card_colors('merchant scroll', ['U']).
card_text('merchant scroll', 'Search your library for a blue instant card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('merchant scroll', ['1', 'U']).
card_cmc('merchant scroll', 2).
card_layout('merchant scroll', 'normal').

% Found in: ARN
card_name('merchant ship', 'Merchant Ship').
card_type('merchant ship', 'Creature — Human').
card_types('merchant ship', ['Creature']).
card_subtypes('merchant ship', ['Human']).
card_colors('merchant ship', ['U']).
card_text('merchant ship', 'Merchant Ship can\'t attack unless defending player controls an Island.\nWhenever Merchant Ship attacks and isn\'t blocked, you gain 2 life.\nWhen you control no Islands, sacrifice Merchant Ship.').
card_mana_cost('merchant ship', ['U']).
card_cmc('merchant ship', 1).
card_layout('merchant ship', 'normal').
card_power('merchant ship', 0).
card_toughness('merchant ship', 2).
card_reserved('merchant ship').

% Found in: GTC
card_name('merciless eviction', 'Merciless Eviction').
card_type('merciless eviction', 'Sorcery').
card_types('merciless eviction', ['Sorcery']).
card_subtypes('merciless eviction', []).
card_colors('merciless eviction', ['W', 'B']).
card_text('merciless eviction', 'Choose one —\n• Exile all artifacts.\n• Exile all creatures.\n• Exile all enchantments.\n• Exile all planeswalkers.').
card_mana_cost('merciless eviction', ['4', 'W', 'B']).
card_cmc('merciless eviction', 6).
card_layout('merciless eviction', 'normal').

% Found in: FRF
card_name('merciless executioner', 'Merciless Executioner').
card_type('merciless executioner', 'Creature — Orc Warrior').
card_types('merciless executioner', ['Creature']).
card_subtypes('merciless executioner', ['Orc', 'Warrior']).
card_colors('merciless executioner', ['B']).
card_text('merciless executioner', 'When Merciless Executioner enters the battlefield, each player sacrifices a creature.').
card_mana_cost('merciless executioner', ['2', 'B']).
card_cmc('merciless executioner', 3).
card_layout('merciless executioner', 'normal').
card_power('merciless executioner', 3).
card_toughness('merciless executioner', 1).

% Found in: ISD
card_name('merciless predator', 'Merciless Predator').
card_type('merciless predator', 'Creature — Werewolf').
card_types('merciless predator', ['Creature']).
card_subtypes('merciless predator', ['Werewolf']).
card_colors('merciless predator', ['R']).
card_text('merciless predator', 'At the beginning of each upkeep, if a player cast two or more spells last turn, transform Merciless Predator.').
card_layout('merciless predator', 'double-faced').
card_power('merciless predator', 3).
card_toughness('merciless predator', 2).

% Found in: RTR
card_name('mercurial chemister', 'Mercurial Chemister').
card_type('mercurial chemister', 'Creature — Human Wizard').
card_types('mercurial chemister', ['Creature']).
card_subtypes('mercurial chemister', ['Human', 'Wizard']).
card_colors('mercurial chemister', ['U', 'R']).
card_text('mercurial chemister', '{U}, {T}: Draw two cards.\n{R}, {T}, Discard a card: Mercurial Chemister deals damage to target creature equal to the discarded card\'s converted mana cost.').
card_mana_cost('mercurial chemister', ['3', 'U', 'R']).
card_cmc('mercurial chemister', 5).
card_layout('mercurial chemister', 'normal').
card_power('mercurial chemister', 2).
card_toughness('mercurial chemister', 3).

% Found in: SCG
card_name('mercurial kite', 'Mercurial Kite').
card_type('mercurial kite', 'Creature — Bird').
card_types('mercurial kite', ['Creature']).
card_subtypes('mercurial kite', ['Bird']).
card_colors('mercurial kite', ['U']).
card_text('mercurial kite', 'Flying\nWhenever Mercurial Kite deals combat damage to a creature, tap that creature. That creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('mercurial kite', ['3', 'U']).
card_cmc('mercurial kite', 4).
card_layout('mercurial kite', 'normal').
card_power('mercurial kite', 2).
card_toughness('mercurial kite', 2).

% Found in: M15, pPRE
card_name('mercurial pretender', 'Mercurial Pretender').
card_type('mercurial pretender', 'Creature — Shapeshifter').
card_types('mercurial pretender', ['Creature']).
card_subtypes('mercurial pretender', ['Shapeshifter']).
card_colors('mercurial pretender', ['U']).
card_text('mercurial pretender', 'You may have Mercurial Pretender enter the battlefield as a copy of any creature you control except it gains \"{2}{U}{U}: Return this creature to its owner\'s hand.\"').
card_mana_cost('mercurial pretender', ['4', 'U']).
card_cmc('mercurial pretender', 5).
card_layout('mercurial pretender', 'normal').
card_power('mercurial pretender', 0).
card_toughness('mercurial pretender', 0).

% Found in: SHM
card_name('mercy killing', 'Mercy Killing').
card_type('mercy killing', 'Instant').
card_types('mercy killing', ['Instant']).
card_subtypes('mercy killing', []).
card_colors('mercy killing', ['W', 'G']).
card_text('mercy killing', 'Target creature\'s controller sacrifices it, then puts X 1/1 green and white Elf Warrior creature tokens onto the battlefield, where X is that creature\'s power.').
card_mana_cost('mercy killing', ['2', 'G/W']).
card_cmc('mercy killing', 3).
card_layout('mercy killing', 'normal').

% Found in: DRK, TSB
card_name('merfolk assassin', 'Merfolk Assassin').
card_type('merfolk assassin', 'Creature — Merfolk Assassin').
card_types('merfolk assassin', ['Creature']).
card_subtypes('merfolk assassin', ['Merfolk', 'Assassin']).
card_colors('merfolk assassin', ['U']).
card_text('merfolk assassin', '{T}: Destroy target creature with islandwalk.').
card_mana_cost('merfolk assassin', ['U', 'U']).
card_cmc('merfolk assassin', 2).
card_layout('merfolk assassin', 'normal').
card_power('merfolk assassin', 1).
card_toughness('merfolk assassin', 2).

% Found in: 10E, 7ED, EXO, M10, M12, TPR
card_name('merfolk looter', 'Merfolk Looter').
card_type('merfolk looter', 'Creature — Merfolk Rogue').
card_types('merfolk looter', ['Creature']).
card_subtypes('merfolk looter', ['Merfolk', 'Rogue']).
card_colors('merfolk looter', ['U']).
card_text('merfolk looter', '{T}: Draw a card, then discard a card.').
card_mana_cost('merfolk looter', ['1', 'U']).
card_cmc('merfolk looter', 2).
card_layout('merfolk looter', 'normal').
card_power('merfolk looter', 1).
card_toughness('merfolk looter', 1).

% Found in: M12, pMEI
card_name('merfolk mesmerist', 'Merfolk Mesmerist').
card_type('merfolk mesmerist', 'Creature — Merfolk Wizard').
card_types('merfolk mesmerist', ['Creature']).
card_subtypes('merfolk mesmerist', ['Merfolk', 'Wizard']).
card_colors('merfolk mesmerist', ['U']).
card_text('merfolk mesmerist', '{U}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('merfolk mesmerist', ['1', 'U']).
card_cmc('merfolk mesmerist', 2).
card_layout('merfolk mesmerist', 'normal').
card_power('merfolk mesmerist', 1).
card_toughness('merfolk mesmerist', 2).

% Found in: ROE
card_name('merfolk observer', 'Merfolk Observer').
card_type('merfolk observer', 'Creature — Merfolk Rogue').
card_types('merfolk observer', ['Creature']).
card_subtypes('merfolk observer', ['Merfolk', 'Rogue']).
card_colors('merfolk observer', ['U']).
card_text('merfolk observer', 'When Merfolk Observer enters the battlefield, look at the top card of target player\'s library.').
card_mana_cost('merfolk observer', ['1', 'U']).
card_cmc('merfolk observer', 2).
card_layout('merfolk observer', 'normal').
card_power('merfolk observer', 2).
card_toughness('merfolk observer', 1).

% Found in: GTC
card_name('merfolk of the depths', 'Merfolk of the Depths').
card_type('merfolk of the depths', 'Creature — Merfolk Soldier').
card_types('merfolk of the depths', ['Creature']).
card_subtypes('merfolk of the depths', ['Merfolk', 'Soldier']).
card_colors('merfolk of the depths', ['U', 'G']).
card_text('merfolk of the depths', 'Flash (You may cast this spell any time you could cast an instant.)').
card_mana_cost('merfolk of the depths', ['4', 'G/U', 'G/U']).
card_cmc('merfolk of the depths', 6).
card_layout('merfolk of the depths', 'normal').
card_power('merfolk of the depths', 4).
card_toughness('merfolk of the depths', 2).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, ITP, LEA, LEB, M13, POR, RQS, S00, S99
card_name('merfolk of the pearl trident', 'Merfolk of the Pearl Trident').
card_type('merfolk of the pearl trident', 'Creature — Merfolk').
card_types('merfolk of the pearl trident', ['Creature']).
card_subtypes('merfolk of the pearl trident', ['Merfolk']).
card_colors('merfolk of the pearl trident', ['U']).
card_text('merfolk of the pearl trident', '').
card_mana_cost('merfolk of the pearl trident', ['U']).
card_cmc('merfolk of the pearl trident', 1).
card_layout('merfolk of the pearl trident', 'normal').
card_power('merfolk of the pearl trident', 1).
card_toughness('merfolk of the pearl trident', 1).

% Found in: MIR
card_name('merfolk raiders', 'Merfolk Raiders').
card_type('merfolk raiders', 'Creature — Merfolk Soldier').
card_types('merfolk raiders', ['Creature']).
card_subtypes('merfolk raiders', ['Merfolk', 'Soldier']).
card_colors('merfolk raiders', ['U']).
card_text('merfolk raiders', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nPhasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)').
card_mana_cost('merfolk raiders', ['1', 'U']).
card_cmc('merfolk raiders', 2).
card_layout('merfolk raiders', 'normal').
card_power('merfolk raiders', 2).
card_toughness('merfolk raiders', 3).

% Found in: ZEN
card_name('merfolk seastalkers', 'Merfolk Seastalkers').
card_type('merfolk seastalkers', 'Creature — Merfolk Scout').
card_types('merfolk seastalkers', ['Creature']).
card_subtypes('merfolk seastalkers', ['Merfolk', 'Scout']).
card_colors('merfolk seastalkers', ['U']).
card_text('merfolk seastalkers', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\n{2}{U}: Tap target creature without flying.').
card_mana_cost('merfolk seastalkers', ['3', 'U']).
card_cmc('merfolk seastalkers', 4).
card_layout('merfolk seastalkers', 'normal').
card_power('merfolk seastalkers', 2).
card_toughness('merfolk seastalkers', 3).

% Found in: MIR
card_name('merfolk seer', 'Merfolk Seer').
card_type('merfolk seer', 'Creature — Merfolk Wizard').
card_types('merfolk seer', ['Creature']).
card_subtypes('merfolk seer', ['Merfolk', 'Wizard']).
card_colors('merfolk seer', ['U']).
card_text('merfolk seer', 'When Merfolk Seer dies, you may pay {1}{U}. If you do, draw a card.').
card_mana_cost('merfolk seer', ['2', 'U']).
card_cmc('merfolk seer', 3).
card_layout('merfolk seer', 'normal').
card_power('merfolk seer', 2).
card_toughness('merfolk seer', 2).

% Found in: ROE
card_name('merfolk skyscout', 'Merfolk Skyscout').
card_type('merfolk skyscout', 'Creature — Merfolk Scout').
card_types('merfolk skyscout', ['Creature']).
card_subtypes('merfolk skyscout', ['Merfolk', 'Scout']).
card_colors('merfolk skyscout', ['U']).
card_text('merfolk skyscout', 'Flying\nWhenever Merfolk Skyscout attacks or blocks, untap target permanent.').
card_mana_cost('merfolk skyscout', ['2', 'U', 'U']).
card_cmc('merfolk skyscout', 4).
card_layout('merfolk skyscout', 'normal').
card_power('merfolk skyscout', 2).
card_toughness('merfolk skyscout', 3).

% Found in: M10, M11
card_name('merfolk sovereign', 'Merfolk Sovereign').
card_type('merfolk sovereign', 'Creature — Merfolk').
card_types('merfolk sovereign', ['Creature']).
card_subtypes('merfolk sovereign', ['Merfolk']).
card_colors('merfolk sovereign', ['U']).
card_text('merfolk sovereign', 'Other Merfolk creatures you control get +1/+1.\n{T}: Target Merfolk creature can\'t be blocked this turn.').
card_mana_cost('merfolk sovereign', ['1', 'U', 'U']).
card_cmc('merfolk sovereign', 3).
card_layout('merfolk sovereign', 'normal').
card_power('merfolk sovereign', 2).
card_toughness('merfolk sovereign', 2).

% Found in: M11, M14
card_name('merfolk spy', 'Merfolk Spy').
card_type('merfolk spy', 'Creature — Merfolk Rogue').
card_types('merfolk spy', ['Creature']).
card_subtypes('merfolk spy', ['Merfolk', 'Rogue']).
card_colors('merfolk spy', ['U']).
card_text('merfolk spy', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhenever Merfolk Spy deals combat damage to a player, that player reveals a card at random from his or her hand.').
card_mana_cost('merfolk spy', ['U']).
card_cmc('merfolk spy', 1).
card_layout('merfolk spy', 'normal').
card_power('merfolk spy', 1).
card_toughness('merfolk spy', 1).

% Found in: PLC
card_name('merfolk thaumaturgist', 'Merfolk Thaumaturgist').
card_type('merfolk thaumaturgist', 'Creature — Merfolk Wizard').
card_types('merfolk thaumaturgist', ['Creature']).
card_subtypes('merfolk thaumaturgist', ['Merfolk', 'Wizard']).
card_colors('merfolk thaumaturgist', ['U']).
card_text('merfolk thaumaturgist', '{T}: Switch target creature\'s power and toughness until end of turn.').
card_mana_cost('merfolk thaumaturgist', ['2', 'U']).
card_cmc('merfolk thaumaturgist', 3).
card_layout('merfolk thaumaturgist', 'normal').
card_power('merfolk thaumaturgist', 1).
card_toughness('merfolk thaumaturgist', 2).

% Found in: WTH
card_name('merfolk traders', 'Merfolk Traders').
card_type('merfolk traders', 'Creature — Merfolk').
card_types('merfolk traders', ['Creature']).
card_subtypes('merfolk traders', ['Merfolk']).
card_colors('merfolk traders', ['U']).
card_text('merfolk traders', 'When Merfolk Traders enters the battlefield, draw a card, then discard a card.').
card_mana_cost('merfolk traders', ['1', 'U']).
card_cmc('merfolk traders', 2).
card_layout('merfolk traders', 'normal').
card_power('merfolk traders', 1).
card_toughness('merfolk traders', 2).

% Found in: DDM, ZEN
card_name('merfolk wayfinder', 'Merfolk Wayfinder').
card_type('merfolk wayfinder', 'Creature — Merfolk Scout').
card_types('merfolk wayfinder', ['Creature']).
card_subtypes('merfolk wayfinder', ['Merfolk', 'Scout']).
card_colors('merfolk wayfinder', ['U']).
card_text('merfolk wayfinder', 'Flying\nWhen Merfolk Wayfinder enters the battlefield, reveal the top three cards of your library. Put all Island cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('merfolk wayfinder', ['2', 'U']).
card_cmc('merfolk wayfinder', 3).
card_layout('merfolk wayfinder', 'normal').
card_power('merfolk wayfinder', 1).
card_toughness('merfolk wayfinder', 2).

% Found in: ICE, TSB
card_name('merieke ri berit', 'Merieke Ri Berit').
card_type('merieke ri berit', 'Legendary Creature — Human').
card_types('merieke ri berit', ['Creature']).
card_subtypes('merieke ri berit', ['Human']).
card_supertypes('merieke ri berit', ['Legendary']).
card_colors('merieke ri berit', ['W', 'U', 'B']).
card_text('merieke ri berit', 'Merieke Ri Berit doesn\'t untap during your untap step.\n{T}: Gain control of target creature for as long as you control Merieke Ri Berit. When Merieke Ri Berit leaves the battlefield or becomes untapped, destroy that creature. It can\'t be regenerated.').
card_mana_cost('merieke ri berit', ['W', 'U', 'B']).
card_cmc('merieke ri berit', 3).
card_layout('merieke ri berit', 'normal').
card_power('merieke ri berit', 1).
card_toughness('merieke ri berit', 1).

% Found in: EVE
card_name('merrow bonegnawer', 'Merrow Bonegnawer').
card_type('merrow bonegnawer', 'Creature — Merfolk Rogue').
card_types('merrow bonegnawer', ['Creature']).
card_subtypes('merrow bonegnawer', ['Merfolk', 'Rogue']).
card_colors('merrow bonegnawer', ['B']).
card_text('merrow bonegnawer', '{T}: Target player exiles a card from his or her graveyard.\nWhenever you cast a black spell, you may untap Merrow Bonegnawer.').
card_mana_cost('merrow bonegnawer', ['B']).
card_cmc('merrow bonegnawer', 1).
card_layout('merrow bonegnawer', 'normal').
card_power('merrow bonegnawer', 1).
card_toughness('merrow bonegnawer', 1).

% Found in: LRW
card_name('merrow commerce', 'Merrow Commerce').
card_type('merrow commerce', 'Tribal Enchantment — Merfolk').
card_types('merrow commerce', ['Tribal', 'Enchantment']).
card_subtypes('merrow commerce', ['Merfolk']).
card_colors('merrow commerce', ['U']).
card_text('merrow commerce', 'At the beginning of your end step, untap all Merfolk you control.').
card_mana_cost('merrow commerce', ['1', 'U']).
card_cmc('merrow commerce', 2).
card_layout('merrow commerce', 'normal').

% Found in: SHM
card_name('merrow grimeblotter', 'Merrow Grimeblotter').
card_type('merrow grimeblotter', 'Creature — Merfolk Wizard').
card_types('merrow grimeblotter', ['Creature']).
card_subtypes('merrow grimeblotter', ['Merfolk', 'Wizard']).
card_colors('merrow grimeblotter', ['U', 'B']).
card_text('merrow grimeblotter', '{1}{U/B}, {Q}: Target creature gets -2/-0 until end of turn. ({Q} is the untap symbol.)').
card_mana_cost('merrow grimeblotter', ['3', 'U/B']).
card_cmc('merrow grimeblotter', 4).
card_layout('merrow grimeblotter', 'normal').
card_power('merrow grimeblotter', 2).
card_toughness('merrow grimeblotter', 2).

% Found in: LRW
card_name('merrow harbinger', 'Merrow Harbinger').
card_type('merrow harbinger', 'Creature — Merfolk Wizard').
card_types('merrow harbinger', ['Creature']).
card_subtypes('merrow harbinger', ['Merfolk', 'Wizard']).
card_colors('merrow harbinger', ['U']).
card_text('merrow harbinger', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhen Merrow Harbinger enters the battlefield, you may search your library for a Merfolk card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('merrow harbinger', ['3', 'U']).
card_cmc('merrow harbinger', 4).
card_layout('merrow harbinger', 'normal').
card_power('merrow harbinger', 2).
card_toughness('merrow harbinger', 3).

% Found in: EVE
card_name('merrow levitator', 'Merrow Levitator').
card_type('merrow levitator', 'Creature — Merfolk Wizard').
card_types('merrow levitator', ['Creature']).
card_subtypes('merrow levitator', ['Merfolk', 'Wizard']).
card_colors('merrow levitator', ['U']).
card_text('merrow levitator', '{T}: Target creature gains flying until end of turn.\nWhenever you cast a blue spell, you may untap Merrow Levitator.').
card_mana_cost('merrow levitator', ['3', 'U']).
card_cmc('merrow levitator', 4).
card_layout('merrow levitator', 'normal').
card_power('merrow levitator', 2).
card_toughness('merrow levitator', 3).

% Found in: LRW, pFNM
card_name('merrow reejerey', 'Merrow Reejerey').
card_type('merrow reejerey', 'Creature — Merfolk Soldier').
card_types('merrow reejerey', ['Creature']).
card_subtypes('merrow reejerey', ['Merfolk', 'Soldier']).
card_colors('merrow reejerey', ['U']).
card_text('merrow reejerey', 'Other Merfolk creatures you control get +1/+1.\nWhenever you cast a Merfolk spell, you may tap or untap target permanent.').
card_mana_cost('merrow reejerey', ['2', 'U']).
card_cmc('merrow reejerey', 3).
card_layout('merrow reejerey', 'normal').
card_power('merrow reejerey', 2).
card_toughness('merrow reejerey', 2).

% Found in: SHM
card_name('merrow wavebreakers', 'Merrow Wavebreakers').
card_type('merrow wavebreakers', 'Creature — Merfolk Soldier').
card_types('merrow wavebreakers', ['Creature']).
card_subtypes('merrow wavebreakers', ['Merfolk', 'Soldier']).
card_colors('merrow wavebreakers', ['U']).
card_text('merrow wavebreakers', '{1}{U}, {Q}: Merrow Wavebreakers gains flying until end of turn. ({Q} is the untap symbol.)').
card_mana_cost('merrow wavebreakers', ['4', 'U']).
card_cmc('merrow wavebreakers', 5).
card_layout('merrow wavebreakers', 'normal').
card_power('merrow wavebreakers', 3).
card_toughness('merrow wavebreakers', 3).

% Found in: MOR
card_name('merrow witsniper', 'Merrow Witsniper').
card_type('merrow witsniper', 'Creature — Merfolk Rogue').
card_types('merrow witsniper', ['Creature']).
card_subtypes('merrow witsniper', ['Merfolk', 'Rogue']).
card_colors('merrow witsniper', ['U']).
card_text('merrow witsniper', 'When Merrow Witsniper enters the battlefield, target player puts the top card of his or her library into his or her graveyard.').
card_mana_cost('merrow witsniper', ['U']).
card_cmc('merrow witsniper', 1).
card_layout('merrow witsniper', 'normal').
card_power('merrow witsniper', 1).
card_toughness('merrow witsniper', 1).

% Found in: FEM
card_name('merseine', 'Merseine').
card_type('merseine', 'Enchantment — Aura').
card_types('merseine', ['Enchantment']).
card_subtypes('merseine', ['Aura']).
card_colors('merseine', ['U']).
card_text('merseine', 'Enchant creature\nMerseine enters the battlefield with three net counters on it.\nEnchanted creature doesn\'t untap during its controller\'s untap step if Merseine has a net counter on it.\nPay enchanted creature\'s mana cost: Remove a net counter from Merseine. Any player may activate this ability, but only if he or she controls the enchanted creature.').
card_mana_cost('merseine', ['2', 'U', 'U']).
card_cmc('merseine', 4).
card_layout('merseine', 'normal').

% Found in: UGL
card_name('mesa chicken', 'Mesa Chicken').
card_type('mesa chicken', 'Creature — Chicken').
card_types('mesa chicken', ['Creature']).
card_subtypes('mesa chicken', ['Chicken']).
card_colors('mesa chicken', ['W']).
card_text('mesa chicken', 'Stand up, Flap your arms, Cluck like a chicken: Mesa Chicken gains flying until end of turn.').
card_mana_cost('mesa chicken', ['W', 'W']).
card_cmc('mesa chicken', 2).
card_layout('mesa chicken', 'normal').
card_power('mesa chicken', 2).
card_toughness('mesa chicken', 2).

% Found in: M10, M12, PLC
card_name('mesa enchantress', 'Mesa Enchantress').
card_type('mesa enchantress', 'Creature — Human Druid').
card_types('mesa enchantress', ['Creature']).
card_subtypes('mesa enchantress', ['Human', 'Druid']).
card_colors('mesa enchantress', ['W']).
card_text('mesa enchantress', 'Whenever you cast an enchantment spell, you may draw a card.').
card_mana_cost('mesa enchantress', ['1', 'W', 'W']).
card_cmc('mesa enchantress', 3).
card_layout('mesa enchantress', 'normal').
card_power('mesa enchantress', 0).
card_toughness('mesa enchantress', 2).

% Found in: 5ED, 6ED, HML
card_name('mesa falcon', 'Mesa Falcon').
card_type('mesa falcon', 'Creature — Bird').
card_types('mesa falcon', ['Creature']).
card_subtypes('mesa falcon', ['Bird']).
card_colors('mesa falcon', ['W']).
card_text('mesa falcon', 'Flying\n{1}{W}: Mesa Falcon gets +0/+1 until end of turn.').
card_mana_cost('mesa falcon', ['1', 'W']).
card_cmc('mesa falcon', 2).
card_layout('mesa falcon', 'normal').
card_power('mesa falcon', 1).
card_toughness('mesa falcon', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, MED, RQS
card_name('mesa pegasus', 'Mesa Pegasus').
card_type('mesa pegasus', 'Creature — Pegasus').
card_types('mesa pegasus', ['Creature']).
card_subtypes('mesa pegasus', ['Pegasus']).
card_colors('mesa pegasus', ['W']).
card_text('mesa pegasus', 'Flying; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('mesa pegasus', ['1', 'W']).
card_cmc('mesa pegasus', 2).
card_layout('mesa pegasus', 'normal').
card_power('mesa pegasus', 1).
card_toughness('mesa pegasus', 1).

% Found in: DDK, TOR, VMA
card_name('mesmeric fiend', 'Mesmeric Fiend').
card_type('mesmeric fiend', 'Creature — Nightmare Horror').
card_types('mesmeric fiend', ['Creature']).
card_subtypes('mesmeric fiend', ['Nightmare', 'Horror']).
card_colors('mesmeric fiend', ['B']).
card_text('mesmeric fiend', 'When Mesmeric Fiend enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card.\nWhen Mesmeric Fiend leaves the battlefield, return the exiled card to its owner\'s hand.').
card_mana_cost('mesmeric fiend', ['1', 'B']).
card_cmc('mesmeric fiend', 2).
card_layout('mesmeric fiend', 'normal').
card_power('mesmeric fiend', 1).
card_toughness('mesmeric fiend', 1).

% Found in: MRD
card_name('mesmeric orb', 'Mesmeric Orb').
card_type('mesmeric orb', 'Artifact').
card_types('mesmeric orb', ['Artifact']).
card_subtypes('mesmeric orb', []).
card_colors('mesmeric orb', []).
card_text('mesmeric orb', 'Whenever a permanent becomes untapped, that permanent\'s controller puts the top card of his or her library into his or her graveyard.').
card_mana_cost('mesmeric orb', ['2']).
card_cmc('mesmeric orb', 2).
card_layout('mesmeric orb', 'normal').

% Found in: FUT
card_name('mesmeric sliver', 'Mesmeric Sliver').
card_type('mesmeric sliver', 'Creature — Sliver').
card_types('mesmeric sliver', ['Creature']).
card_subtypes('mesmeric sliver', ['Sliver']).
card_colors('mesmeric sliver', ['U']).
card_text('mesmeric sliver', 'All Slivers have \"When this permanent enters the battlefield, you may fateseal 1.\" (To fateseal 1, its controller looks at the top card of an opponent\'s library, then he or she may put that card on the bottom of that library.)').
card_mana_cost('mesmeric sliver', ['3', 'U']).
card_cmc('mesmeric sliver', 4).
card_layout('mesmeric sliver', 'normal').
card_power('mesmeric sliver', 2).
card_toughness('mesmeric sliver', 2).

% Found in: ICE, ME2
card_name('mesmeric trance', 'Mesmeric Trance').
card_type('mesmeric trance', 'Enchantment').
card_types('mesmeric trance', ['Enchantment']).
card_subtypes('mesmeric trance', []).
card_colors('mesmeric trance', ['U']).
card_text('mesmeric trance', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\n{U}, Discard a card: Draw a card.').
card_mana_cost('mesmeric trance', ['1', 'U', 'U']).
card_cmc('mesmeric trance', 3).
card_layout('mesmeric trance', 'normal').
card_reserved('mesmeric trance').

% Found in: M14
card_name('messenger drake', 'Messenger Drake').
card_type('messenger drake', 'Creature — Drake').
card_types('messenger drake', ['Creature']).
card_subtypes('messenger drake', ['Drake']).
card_colors('messenger drake', ['U']).
card_text('messenger drake', 'Flying\nWhen Messenger Drake dies, draw a card.').
card_mana_cost('messenger drake', ['3', 'U', 'U']).
card_cmc('messenger drake', 5).
card_layout('messenger drake', 'normal').
card_power('messenger drake', 3).
card_toughness('messenger drake', 3).

% Found in: ARB
card_name('messenger falcons', 'Messenger Falcons').
card_type('messenger falcons', 'Creature — Bird').
card_types('messenger falcons', ['Creature']).
card_subtypes('messenger falcons', ['Bird']).
card_colors('messenger falcons', ['W', 'U', 'G']).
card_text('messenger falcons', 'Flying\nWhen Messenger Falcons enters the battlefield, draw a card.').
card_mana_cost('messenger falcons', ['2', 'G/U', 'W']).
card_cmc('messenger falcons', 4).
card_layout('messenger falcons', 'normal').
card_power('messenger falcons', 2).
card_toughness('messenger falcons', 2).

% Found in: THS
card_name('messenger\'s speed', 'Messenger\'s Speed').
card_type('messenger\'s speed', 'Enchantment — Aura').
card_types('messenger\'s speed', ['Enchantment']).
card_subtypes('messenger\'s speed', ['Aura']).
card_colors('messenger\'s speed', ['R']).
card_text('messenger\'s speed', 'Enchant creature\nEnchanted creature has trample and haste.').
card_mana_cost('messenger\'s speed', ['R']).
card_cmc('messenger\'s speed', 1).
card_layout('messenger\'s speed', 'normal').

% Found in: DST
card_name('metal fatigue', 'Metal Fatigue').
card_type('metal fatigue', 'Instant').
card_types('metal fatigue', ['Instant']).
card_subtypes('metal fatigue', []).
card_colors('metal fatigue', ['W']).
card_text('metal fatigue', 'Tap all artifacts.').
card_mana_cost('metal fatigue', ['2', 'W']).
card_cmc('metal fatigue', 3).
card_layout('metal fatigue', 'normal').

% Found in: MBS
card_name('metallic mastery', 'Metallic Mastery').
card_type('metallic mastery', 'Sorcery').
card_types('metallic mastery', ['Sorcery']).
card_subtypes('metallic mastery', []).
card_colors('metallic mastery', ['R']).
card_text('metallic mastery', 'Gain control of target artifact until end of turn. Untap that artifact. It gains haste until end of turn.').
card_mana_cost('metallic mastery', ['2', 'R']).
card_cmc('metallic mastery', 3).
card_layout('metallic mastery', 'normal').

% Found in: H09, TMP, TPR
card_name('metallic sliver', 'Metallic Sliver').
card_type('metallic sliver', 'Artifact Creature — Sliver').
card_types('metallic sliver', ['Artifact', 'Creature']).
card_subtypes('metallic sliver', ['Sliver']).
card_colors('metallic sliver', []).
card_text('metallic sliver', '').
card_mana_cost('metallic sliver', ['1']).
card_cmc('metallic sliver', 1).
card_layout('metallic sliver', 'normal').
card_power('metallic sliver', 1).
card_toughness('metallic sliver', 1).

% Found in: ALA, ARC
card_name('metallurgeon', 'Metallurgeon').
card_type('metallurgeon', 'Artifact Creature — Human Artificer').
card_types('metallurgeon', ['Artifact', 'Creature']).
card_subtypes('metallurgeon', ['Human', 'Artificer']).
card_colors('metallurgeon', ['W']).
card_text('metallurgeon', '{W}, {T}: Regenerate target artifact.').
card_mana_cost('metallurgeon', ['1', 'W']).
card_cmc('metallurgeon', 2).
card_layout('metallurgeon', 'normal').
card_power('metallurgeon', 1).
card_toughness('metallurgeon', 2).

% Found in: UDS
card_name('metalworker', 'Metalworker').
card_type('metalworker', 'Artifact Creature — Construct').
card_types('metalworker', ['Artifact', 'Creature']).
card_subtypes('metalworker', ['Construct']).
card_colors('metalworker', []).
card_text('metalworker', '{T}: Reveal any number of artifact cards in your hand. Add {2} to your mana pool for each card revealed this way.').
card_mana_cost('metalworker', ['3']).
card_cmc('metalworker', 3).
card_layout('metalworker', 'normal').
card_power('metalworker', 1).
card_toughness('metalworker', 2).
card_reserved('metalworker').

% Found in: ODY
card_name('metamorphic wurm', 'Metamorphic Wurm').
card_type('metamorphic wurm', 'Creature — Elephant Wurm').
card_types('metamorphic wurm', ['Creature']).
card_subtypes('metamorphic wurm', ['Elephant', 'Wurm']).
card_colors('metamorphic wurm', ['G']).
card_text('metamorphic wurm', 'Threshold — Metamorphic Wurm gets +4/+4 as long as seven or more cards are in your graveyard.').
card_mana_cost('metamorphic wurm', ['3', 'G', 'G']).
card_cmc('metamorphic wurm', 5).
card_layout('metamorphic wurm', 'normal').
card_power('metamorphic wurm', 3).
card_toughness('metamorphic wurm', 3).

% Found in: SCG
card_name('metamorphose', 'Metamorphose').
card_type('metamorphose', 'Instant').
card_types('metamorphose', ['Instant']).
card_subtypes('metamorphose', []).
card_colors('metamorphose', ['U']).
card_text('metamorphose', 'Put target permanent an opponent controls on top of its owner\'s library. That opponent may put an artifact, creature, enchantment, or land card from his or her hand onto the battlefield.').
card_mana_cost('metamorphose', ['1', 'U']).
card_cmc('metamorphose', 2).
card_layout('metamorphose', 'normal').

% Found in: ARN, CHR
card_name('metamorphosis', 'Metamorphosis').
card_type('metamorphosis', 'Sorcery').
card_types('metamorphosis', ['Sorcery']).
card_subtypes('metamorphosis', []).
card_colors('metamorphosis', ['G']).
card_text('metamorphosis', 'As an additional cost to cast Metamorphosis, sacrifice a creature.\nAdd X mana of any one color to your mana pool, where X is one plus the sacrificed creature\'s converted mana cost. Spend this mana only to cast creature spells.').
card_mana_cost('metamorphosis', ['G']).
card_cmc('metamorphosis', 1).
card_layout('metamorphosis', 'normal').

% Found in: INV
card_name('metathran aerostat', 'Metathran Aerostat').
card_type('metathran aerostat', 'Creature — Metathran').
card_types('metathran aerostat', ['Creature']).
card_subtypes('metathran aerostat', ['Metathran']).
card_colors('metathran aerostat', ['U']).
card_text('metathran aerostat', 'Flying\n{X}{U}: You may put a creature card with converted mana cost X from your hand onto the battlefield. If you do, return Metathran Aerostat to its owner\'s hand.').
card_mana_cost('metathran aerostat', ['2', 'U', 'U']).
card_cmc('metathran aerostat', 4).
card_layout('metathran aerostat', 'normal').
card_power('metathran aerostat', 2).
card_toughness('metathran aerostat', 2).

% Found in: UDS
card_name('metathran elite', 'Metathran Elite').
card_type('metathran elite', 'Creature — Metathran Soldier').
card_types('metathran elite', ['Creature']).
card_subtypes('metathran elite', ['Metathran', 'Soldier']).
card_colors('metathran elite', ['U']).
card_text('metathran elite', 'Metathran Elite can\'t be blocked as long as it\'s enchanted.').
card_mana_cost('metathran elite', ['1', 'U', 'U']).
card_cmc('metathran elite', 3).
card_layout('metathran elite', 'normal').
card_power('metathran elite', 2).
card_toughness('metathran elite', 3).

% Found in: UDS
card_name('metathran soldier', 'Metathran Soldier').
card_type('metathran soldier', 'Creature — Metathran Soldier').
card_types('metathran soldier', ['Creature']).
card_subtypes('metathran soldier', ['Metathran', 'Soldier']).
card_colors('metathran soldier', ['U']).
card_text('metathran soldier', 'Metathran Soldier can\'t be blocked.').
card_mana_cost('metathran soldier', ['1', 'U']).
card_cmc('metathran soldier', 2).
card_layout('metathran soldier', 'normal').
card_power('metathran soldier', 1).
card_toughness('metathran soldier', 1).

% Found in: INV
card_name('metathran transport', 'Metathran Transport').
card_type('metathran transport', 'Creature — Metathran').
card_types('metathran transport', ['Creature']).
card_subtypes('metathran transport', ['Metathran']).
card_colors('metathran transport', ['U']).
card_text('metathran transport', 'Flying\nMetathran Transport can\'t be blocked by blue creatures.\n{U}: Target creature becomes blue until end of turn.').
card_mana_cost('metathran transport', ['1', 'U', 'U']).
card_cmc('metathran transport', 3).
card_layout('metathran transport', 'normal').
card_power('metathran transport', 1).
card_toughness('metathran transport', 3).

% Found in: INV
card_name('metathran zombie', 'Metathran Zombie').
card_type('metathran zombie', 'Creature — Metathran Zombie').
card_types('metathran zombie', ['Creature']).
card_subtypes('metathran zombie', ['Metathran', 'Zombie']).
card_colors('metathran zombie', ['U']).
card_text('metathran zombie', '{B}: Regenerate Metathran Zombie.').
card_mana_cost('metathran zombie', ['1', 'U']).
card_cmc('metathran zombie', 2).
card_layout('metathran zombie', 'normal').
card_power('metathran zombie', 1).
card_toughness('metathran zombie', 1).

% Found in: PLS
card_name('meteor crater', 'Meteor Crater').
card_type('meteor crater', 'Land').
card_types('meteor crater', ['Land']).
card_subtypes('meteor crater', []).
card_colors('meteor crater', []).
card_text('meteor crater', '{T}: Choose a color of a permanent you control. Add one mana of that color to your mana pool.').
card_layout('meteor crater', 'normal').

% Found in: ICE, ME2
card_name('meteor shower', 'Meteor Shower').
card_type('meteor shower', 'Sorcery').
card_types('meteor shower', ['Sorcery']).
card_subtypes('meteor shower', []).
card_colors('meteor shower', ['R']).
card_text('meteor shower', 'Meteor Shower deals X plus 1 damage divided as you choose among any number of target creatures and/or players.').
card_mana_cost('meteor shower', ['X', 'X', 'R']).
card_cmc('meteor shower', 1).
card_layout('meteor shower', 'normal').

% Found in: INV
card_name('meteor storm', 'Meteor Storm').
card_type('meteor storm', 'Enchantment').
card_types('meteor storm', ['Enchantment']).
card_subtypes('meteor storm', []).
card_colors('meteor storm', ['R', 'G']).
card_text('meteor storm', '{2}{R}{G}, Discard two cards at random: Meteor Storm deals 4 damage to target creature or player.').
card_mana_cost('meteor storm', ['R', 'G']).
card_cmc('meteor storm', 2).
card_layout('meteor storm', 'normal').

% Found in: M15, ORI
card_name('meteorite', 'Meteorite').
card_type('meteorite', 'Artifact').
card_types('meteorite', ['Artifact']).
card_subtypes('meteorite', []).
card_colors('meteorite', []).
card_text('meteorite', 'When Meteorite enters the battlefield, it deals 2 damage to target creature or player.\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('meteorite', ['5']).
card_cmc('meteorite', 5).
card_layout('meteorite', 'normal').

% Found in: USG
card_name('metrognome', 'Metrognome').
card_type('metrognome', 'Artifact').
card_types('metrognome', ['Artifact']).
card_subtypes('metrognome', []).
card_colors('metrognome', []).
card_text('metrognome', 'When a spell or ability an opponent controls causes you to discard Metrognome, put four 1/1 colorless Gnome artifact creature tokens onto the battlefield.\n{4}, {T}: Put a 1/1 colorless Gnome artifact creature token onto the battlefield.').
card_mana_cost('metrognome', ['4']).
card_cmc('metrognome', 4).
card_layout('metrognome', 'normal').

% Found in: GTC
card_name('metropolis sprite', 'Metropolis Sprite').
card_type('metropolis sprite', 'Creature — Faerie Rogue').
card_types('metropolis sprite', ['Creature']).
card_subtypes('metropolis sprite', ['Faerie', 'Rogue']).
card_colors('metropolis sprite', ['U']).
card_text('metropolis sprite', 'Flying\n{U}: Metropolis Sprite gets +1/-1 until end of turn.').
card_mana_cost('metropolis sprite', ['1', 'U']).
card_cmc('metropolis sprite', 2).
card_layout('metropolis sprite', 'normal').
card_power('metropolis sprite', 1).
card_toughness('metropolis sprite', 2).

% Found in: SOK
card_name('michiko konda, truth seeker', 'Michiko Konda, Truth Seeker').
card_type('michiko konda, truth seeker', 'Legendary Creature — Human Advisor').
card_types('michiko konda, truth seeker', ['Creature']).
card_subtypes('michiko konda, truth seeker', ['Human', 'Advisor']).
card_supertypes('michiko konda, truth seeker', ['Legendary']).
card_colors('michiko konda, truth seeker', ['W']).
card_text('michiko konda, truth seeker', 'Whenever a source an opponent controls deals damage to you, that player sacrifices a permanent.').
card_mana_cost('michiko konda, truth seeker', ['3', 'W']).
card_cmc('michiko konda, truth seeker', 4).
card_layout('michiko konda, truth seeker', 'normal').
card_power('michiko konda, truth seeker', 2).
card_toughness('michiko konda, truth seeker', 2).

% Found in: MM2, SHM
card_name('midnight banshee', 'Midnight Banshee').
card_type('midnight banshee', 'Creature — Spirit').
card_types('midnight banshee', ['Creature']).
card_subtypes('midnight banshee', ['Spirit']).
card_colors('midnight banshee', ['B']).
card_text('midnight banshee', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nAt the beginning of your upkeep, put a -1/-1 counter on each nonblack creature.').
card_mana_cost('midnight banshee', ['3', 'B', 'B', 'B']).
card_cmc('midnight banshee', 6).
card_layout('midnight banshee', 'normal').
card_power('midnight banshee', 5).
card_toughness('midnight banshee', 5).

% Found in: PLC
card_name('midnight charm', 'Midnight Charm').
card_type('midnight charm', 'Instant').
card_types('midnight charm', ['Instant']).
card_subtypes('midnight charm', []).
card_colors('midnight charm', ['B']).
card_text('midnight charm', 'Choose one —\n• Midnight Charm deals 1 damage to target creature and you gain 1 life.\n• Target creature gains first strike until end of turn.\n• Tap target creature.').
card_mana_cost('midnight charm', ['B']).
card_cmc('midnight charm', 1).
card_layout('midnight charm', 'normal').

% Found in: CHK
card_name('midnight covenant', 'Midnight Covenant').
card_type('midnight covenant', 'Enchantment — Aura').
card_types('midnight covenant', ['Enchantment']).
card_subtypes('midnight covenant', ['Aura']).
card_colors('midnight covenant', ['B']).
card_text('midnight covenant', 'Enchant creature\nEnchanted creature has \"{B}: This creature gets +1/+1 until end of turn.\"').
card_mana_cost('midnight covenant', ['1', 'B']).
card_cmc('midnight covenant', 2).
card_layout('midnight covenant', 'normal').

% Found in: AVR
card_name('midnight duelist', 'Midnight Duelist').
card_type('midnight duelist', 'Creature — Human Soldier').
card_types('midnight duelist', ['Creature']).
card_subtypes('midnight duelist', ['Human', 'Soldier']).
card_colors('midnight duelist', ['W']).
card_text('midnight duelist', 'Protection from Vampires').
card_mana_cost('midnight duelist', ['W']).
card_cmc('midnight duelist', 1).
card_layout('midnight duelist', 'normal').
card_power('midnight duelist', 1).
card_toughness('midnight duelist', 2).

% Found in: DKA, M15
card_name('midnight guard', 'Midnight Guard').
card_type('midnight guard', 'Creature — Human Soldier').
card_types('midnight guard', ['Creature']).
card_subtypes('midnight guard', ['Human', 'Soldier']).
card_colors('midnight guard', ['W']).
card_text('midnight guard', 'Whenever another creature enters the battlefield, untap Midnight Guard.').
card_mana_cost('midnight guard', ['2', 'W']).
card_cmc('midnight guard', 3).
card_layout('midnight guard', 'normal').
card_power('midnight guard', 2).
card_toughness('midnight guard', 3).

% Found in: C14, ISD
card_name('midnight haunting', 'Midnight Haunting').
card_type('midnight haunting', 'Instant').
card_types('midnight haunting', ['Instant']).
card_subtypes('midnight haunting', []).
card_colors('midnight haunting', ['W']).
card_text('midnight haunting', 'Put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_mana_cost('midnight haunting', ['2', 'W']).
card_cmc('midnight haunting', 3).
card_layout('midnight haunting', 'normal').

% Found in: GTC
card_name('midnight recovery', 'Midnight Recovery').
card_type('midnight recovery', 'Sorcery').
card_types('midnight recovery', ['Sorcery']).
card_subtypes('midnight recovery', []).
card_colors('midnight recovery', ['B']).
card_text('midnight recovery', 'Return target creature card from your graveyard to your hand.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('midnight recovery', ['3', 'B']).
card_cmc('midnight recovery', 4).
card_layout('midnight recovery', 'normal').

% Found in: 10E, MMQ
card_name('midnight ritual', 'Midnight Ritual').
card_type('midnight ritual', 'Sorcery').
card_types('midnight ritual', ['Sorcery']).
card_subtypes('midnight ritual', []).
card_colors('midnight ritual', ['B']).
card_text('midnight ritual', 'Exile X target creature cards from your graveyard. For each creature card exiled this way, put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('midnight ritual', ['X', '2', 'B']).
card_cmc('midnight ritual', 3).
card_layout('midnight ritual', 'normal').

% Found in: USG
card_name('midsummer revel', 'Midsummer Revel').
card_type('midsummer revel', 'Enchantment').
card_types('midsummer revel', ['Enchantment']).
card_subtypes('midsummer revel', []).
card_colors('midsummer revel', ['G']).
card_text('midsummer revel', 'At the beginning of your upkeep, you may put a verse counter on Midsummer Revel.\n{G}, Sacrifice Midsummer Revel: Put X 3/3 green Beast creature tokens onto the battlefield, where X is the number of verse counters on Midsummer Revel.').
card_mana_cost('midsummer revel', ['3', 'G', 'G']).
card_cmc('midsummer revel', 5).
card_layout('midsummer revel', 'normal').

% Found in: AVR
card_name('midvast protector', 'Midvast Protector').
card_type('midvast protector', 'Creature — Human Wizard').
card_types('midvast protector', ['Creature']).
card_subtypes('midvast protector', ['Human', 'Wizard']).
card_colors('midvast protector', ['W']).
card_text('midvast protector', 'When Midvast Protector enters the battlefield, target creature you control gains protection from the color of your choice until end of turn.').
card_mana_cost('midvast protector', ['3', 'W']).
card_cmc('midvast protector', 4).
card_layout('midvast protector', 'normal').
card_power('midvast protector', 2).
card_toughness('midvast protector', 3).

% Found in: M15
card_name('might makes right', 'Might Makes Right').
card_type('might makes right', 'Enchantment').
card_types('might makes right', ['Enchantment']).
card_subtypes('might makes right', []).
card_colors('might makes right', ['R']).
card_text('might makes right', 'At the beginning of combat on your turn, if you control each creature on the battlefield with the greatest power, gain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('might makes right', ['5', 'R']).
card_cmc('might makes right', 6).
card_layout('might makes right', 'normal').

% Found in: CON
card_name('might of alara', 'Might of Alara').
card_type('might of alara', 'Instant').
card_types('might of alara', ['Instant']).
card_subtypes('might of alara', []).
card_colors('might of alara', ['G']).
card_text('might of alara', 'Domain — Target creature gets +1/+1 until end of turn for each basic land type among lands you control.').
card_mana_cost('might of alara', ['G']).
card_cmc('might of alara', 1).
card_layout('might of alara', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, M10, ULG
card_name('might of oaks', 'Might of Oaks').
card_type('might of oaks', 'Instant').
card_types('might of oaks', ['Instant']).
card_subtypes('might of oaks', []).
card_colors('might of oaks', ['G']).
card_text('might of oaks', 'Target creature gets +7/+7 until end of turn.').
card_mana_cost('might of oaks', ['3', 'G']).
card_cmc('might of oaks', 4).
card_layout('might of oaks', 'normal').

% Found in: TSP
card_name('might of old krosa', 'Might of Old Krosa').
card_type('might of old krosa', 'Instant').
card_types('might of old krosa', ['Instant']).
card_subtypes('might of old krosa', []).
card_colors('might of old krosa', ['G']).
card_text('might of old krosa', 'Target creature gets +2/+2 until end of turn. If you cast this spell during your main phase, that creature gets +4/+4 until end of turn instead.').
card_mana_cost('might of old krosa', ['G']).
card_cmc('might of old krosa', 1).
card_layout('might of old krosa', 'normal').

% Found in: ORI, ROE
card_name('might of the masses', 'Might of the Masses').
card_type('might of the masses', 'Instant').
card_types('might of the masses', ['Instant']).
card_subtypes('might of the masses', []).
card_colors('might of the masses', ['G']).
card_text('might of the masses', 'Target creature gets +1/+1 until end of turn for each creature you control.').
card_mana_cost('might of the masses', ['G']).
card_cmc('might of the masses', 1).
card_layout('might of the masses', 'normal').

% Found in: DIS
card_name('might of the nephilim', 'Might of the Nephilim').
card_type('might of the nephilim', 'Instant').
card_types('might of the nephilim', ['Instant']).
card_subtypes('might of the nephilim', []).
card_colors('might of the nephilim', ['G']).
card_text('might of the nephilim', 'Target creature gets +2/+2 until end of turn for each of its colors.').
card_mana_cost('might of the nephilim', ['1', 'G']).
card_cmc('might of the nephilim', 2).
card_layout('might of the nephilim', 'normal').

% Found in: H09, TSP
card_name('might sliver', 'Might Sliver').
card_type('might sliver', 'Creature — Sliver').
card_types('might sliver', ['Creature']).
card_subtypes('might sliver', ['Sliver']).
card_colors('might sliver', ['G']).
card_text('might sliver', 'All Sliver creatures get +2/+2.').
card_mana_cost('might sliver', ['4', 'G']).
card_cmc('might sliver', 5).
card_layout('might sliver', 'normal').
card_power('might sliver', 2).
card_toughness('might sliver', 2).

% Found in: 10E, INV
card_name('might weaver', 'Might Weaver').
card_type('might weaver', 'Creature — Human Wizard').
card_types('might weaver', ['Creature']).
card_subtypes('might weaver', ['Human', 'Wizard']).
card_colors('might weaver', ['G']).
card_text('might weaver', '{2}: Target red or white creature gains trample until end of turn. (If the creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('might weaver', ['1', 'G']).
card_cmc('might weaver', 2).
card_layout('might weaver', 'normal').
card_power('might weaver', 2).
card_toughness('might weaver', 1).

% Found in: ATQ, ME4
card_name('mightstone', 'Mightstone').
card_type('mightstone', 'Artifact').
card_types('mightstone', ['Artifact']).
card_subtypes('mightstone', []).
card_colors('mightstone', []).
card_text('mightstone', 'Attacking creatures get +1/+0.').
card_mana_cost('mightstone', ['4']).
card_cmc('mightstone', 4).
card_layout('mightstone', 'normal').
card_reserved('mightstone').

% Found in: ALA
card_name('mighty emergence', 'Mighty Emergence').
card_type('mighty emergence', 'Enchantment').
card_types('mighty emergence', ['Enchantment']).
card_subtypes('mighty emergence', []).
card_colors('mighty emergence', ['G']).
card_text('mighty emergence', 'Whenever a creature with power 5 or greater enters the battlefield under your control, you may put two +1/+1 counters on it.').
card_mana_cost('mighty emergence', ['2', 'G']).
card_cmc('mighty emergence', 3).
card_layout('mighty emergence', 'normal').

% Found in: DDF, DDG, DDO, M11, M12, MM2, ORI
card_name('mighty leap', 'Mighty Leap').
card_type('mighty leap', 'Instant').
card_types('mighty leap', ['Instant']).
card_subtypes('mighty leap', []).
card_colors('mighty leap', ['W']).
card_text('mighty leap', 'Target creature gets +2/+2 and gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('mighty leap', ['1', 'W']).
card_cmc('mighty leap', 2).
card_layout('mighty leap', 'normal').

% Found in: 3ED, ARN, ME4
card_name('mijae djinn', 'Mijae Djinn').
card_type('mijae djinn', 'Creature — Djinn').
card_types('mijae djinn', ['Creature']).
card_subtypes('mijae djinn', ['Djinn']).
card_colors('mijae djinn', ['R']).
card_text('mijae djinn', 'Whenever Mijae Djinn attacks, flip a coin. If you lose the flip, remove Mijae Djinn from combat and tap it.').
card_mana_cost('mijae djinn', ['R', 'R', 'R']).
card_cmc('mijae djinn', 3).
card_layout('mijae djinn', 'normal').
card_power('mijae djinn', 6).
card_toughness('mijae djinn', 3).

% Found in: ISD, V11
card_name('mikaeus, the lunarch', 'Mikaeus, the Lunarch').
card_type('mikaeus, the lunarch', 'Legendary Creature — Human Cleric').
card_types('mikaeus, the lunarch', ['Creature']).
card_subtypes('mikaeus, the lunarch', ['Human', 'Cleric']).
card_supertypes('mikaeus, the lunarch', ['Legendary']).
card_colors('mikaeus, the lunarch', ['W']).
card_text('mikaeus, the lunarch', 'Mikaeus, the Lunarch enters the battlefield with X +1/+1 counters on it.\n{T}: Put a +1/+1 counter on Mikaeus.\n{T}, Remove a +1/+1 counter from Mikaeus: Put a +1/+1 counter on each other creature you control.').
card_mana_cost('mikaeus, the lunarch', ['X', 'W']).
card_cmc('mikaeus, the lunarch', 1).
card_layout('mikaeus, the lunarch', 'normal').
card_power('mikaeus, the lunarch', 0).
card_toughness('mikaeus, the lunarch', 0).

% Found in: DKA
card_name('mikaeus, the unhallowed', 'Mikaeus, the Unhallowed').
card_type('mikaeus, the unhallowed', 'Legendary Creature — Zombie Cleric').
card_types('mikaeus, the unhallowed', ['Creature']).
card_subtypes('mikaeus, the unhallowed', ['Zombie', 'Cleric']).
card_supertypes('mikaeus, the unhallowed', ['Legendary']).
card_colors('mikaeus, the unhallowed', ['B']).
card_text('mikaeus, the unhallowed', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever a Human deals damage to you, destroy it.\nOther non-Human creatures you control get +1/+1 and have undying. (When a creature with undying dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('mikaeus, the unhallowed', ['3', 'B', 'B', 'B']).
card_cmc('mikaeus, the unhallowed', 6).
card_layout('mikaeus, the unhallowed', 'normal').
card_power('mikaeus, the unhallowed', 5).
card_toughness('mikaeus, the unhallowed', 5).

% Found in: SOK
card_name('mikokoro, center of the sea', 'Mikokoro, Center of the Sea').
card_type('mikokoro, center of the sea', 'Legendary Land').
card_types('mikokoro, center of the sea', ['Land']).
card_subtypes('mikokoro, center of the sea', []).
card_supertypes('mikokoro, center of the sea', ['Legendary']).
card_colors('mikokoro, center of the sea', []).
card_text('mikokoro, center of the sea', '{T}: Add {1} to your mana pool.\n{2}, {T}: Each player draws a card.').
card_layout('mikokoro, center of the sea', 'normal').

% Found in: TOR
card_name('militant monk', 'Militant Monk').
card_type('militant monk', 'Creature — Human Monk Cleric').
card_types('militant monk', ['Creature']).
card_subtypes('militant monk', ['Human', 'Monk', 'Cleric']).
card_colors('militant monk', ['W']).
card_text('militant monk', 'Vigilance\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('militant monk', ['1', 'W', 'W']).
card_cmc('militant monk', 3).
card_layout('militant monk', 'normal').
card_power('militant monk', 2).
card_toughness('militant monk', 1).

% Found in: M15
card_name('military intelligence', 'Military Intelligence').
card_type('military intelligence', 'Enchantment').
card_types('military intelligence', ['Enchantment']).
card_subtypes('military intelligence', []).
card_colors('military intelligence', ['U']).
card_text('military intelligence', 'Whenever you attack with two or more creatures, draw a card.').
card_mana_cost('military intelligence', ['1', 'U']).
card_cmc('military intelligence', 2).
card_layout('military intelligence', 'normal').

% Found in: LRW
card_name('militia\'s pride', 'Militia\'s Pride').
card_type('militia\'s pride', 'Tribal Enchantment — Kithkin').
card_types('militia\'s pride', ['Tribal', 'Enchantment']).
card_subtypes('militia\'s pride', ['Kithkin']).
card_colors('militia\'s pride', ['W']).
card_text('militia\'s pride', 'Whenever a nontoken creature you control attacks, you may pay {W}. If you do, put a 1/1 white Kithkin Soldier creature token onto the battlefield tapped and attacking.').
card_mana_cost('militia\'s pride', ['1', 'W']).
card_cmc('militia\'s pride', 2).
card_layout('militia\'s pride', 'normal').

% Found in: GTC
card_name('millennial gargoyle', 'Millennial Gargoyle').
card_type('millennial gargoyle', 'Artifact Creature — Gargoyle').
card_types('millennial gargoyle', ['Artifact', 'Creature']).
card_subtypes('millennial gargoyle', ['Gargoyle']).
card_colors('millennial gargoyle', []).
card_text('millennial gargoyle', 'Flying').
card_mana_cost('millennial gargoyle', ['4']).
card_cmc('millennial gargoyle', 4).
card_layout('millennial gargoyle', 'normal').
card_power('millennial gargoyle', 2).
card_toughness('millennial gargoyle', 2).

% Found in: ODY
card_name('millikin', 'Millikin').
card_type('millikin', 'Artifact Creature — Construct').
card_types('millikin', ['Artifact', 'Creature']).
card_subtypes('millikin', ['Construct']).
card_colors('millikin', []).
card_text('millikin', '{T}, Put the top card of your library into your graveyard: Add {1} to your mana pool.').
card_mana_cost('millikin', ['2']).
card_cmc('millikin', 2).
card_layout('millikin', 'normal').
card_power('millikin', 0).
card_toughness('millikin', 1).

% Found in: 10E, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ATQ, M14
card_name('millstone', 'Millstone').
card_type('millstone', 'Artifact').
card_types('millstone', ['Artifact']).
card_subtypes('millstone', []).
card_colors('millstone', []).
card_text('millstone', '{2}, {T}: Target player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('millstone', ['2']).
card_cmc('millstone', 2).
card_layout('millstone', 'normal').

% Found in: GPT
card_name('mimeofacture', 'Mimeofacture').
card_type('mimeofacture', 'Sorcery').
card_types('mimeofacture', ['Sorcery']).
card_subtypes('mimeofacture', []).
card_colors('mimeofacture', ['U']).
card_text('mimeofacture', 'Replicate {3}{U} (When you cast this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nChoose target permanent an opponent controls. Search that player\'s library for a card with the same name and put it onto the battlefield under your control. Then that player shuffles his or her library.').
card_mana_cost('mimeofacture', ['3', 'U']).
card_cmc('mimeofacture', 4).
card_layout('mimeofacture', 'normal').

% Found in: SOM
card_name('mimic vat', 'Mimic Vat').
card_type('mimic vat', 'Artifact').
card_types('mimic vat', ['Artifact']).
card_subtypes('mimic vat', []).
card_colors('mimic vat', []).
card_text('mimic vat', 'Imprint — Whenever a nontoken creature dies, you may exile that card. If you do, return each other card exiled with Mimic Vat to its owner\'s graveyard.\n{3}, {T}: Put a token onto the battlefield that\'s a copy of the exiled card. It gains haste. Exile it at the beginning of the next end step.').
card_mana_cost('mimic vat', ['3']).
card_cmc('mimic vat', 3).
card_layout('mimic vat', 'normal').

% Found in: GTC
card_name('miming slime', 'Miming Slime').
card_type('miming slime', 'Sorcery').
card_types('miming slime', ['Sorcery']).
card_subtypes('miming slime', []).
card_colors('miming slime', ['G']).
card_text('miming slime', 'Put an X/X green Ooze creature token onto the battlefield, where X is the greatest power among creatures you control.').
card_mana_cost('miming slime', ['2', 'G']).
card_cmc('miming slime', 3).
card_layout('miming slime', 'normal').

% Found in: HOP
card_name('minamo', 'Minamo').
card_type('minamo', 'Plane — Kamigawa').
card_types('minamo', ['Plane']).
card_subtypes('minamo', ['Kamigawa']).
card_colors('minamo', []).
card_text('minamo', 'Whenever a player casts a spell, that player may draw a card. \nWhenever you roll {C}, each player may return a blue card from his or her graveyard to his or her hand.').
card_layout('minamo', 'plane').

% Found in: CNS, SOK
card_name('minamo scrollkeeper', 'Minamo Scrollkeeper').
card_type('minamo scrollkeeper', 'Creature — Human Wizard').
card_types('minamo scrollkeeper', ['Creature']).
card_subtypes('minamo scrollkeeper', ['Human', 'Wizard']).
card_colors('minamo scrollkeeper', ['U']).
card_text('minamo scrollkeeper', 'Defender\nYour maximum hand size is increased by one.').
card_mana_cost('minamo scrollkeeper', ['1', 'U']).
card_cmc('minamo scrollkeeper', 2).
card_layout('minamo scrollkeeper', 'normal').
card_power('minamo scrollkeeper', 2).
card_toughness('minamo scrollkeeper', 3).

% Found in: BOK, DDI
card_name('minamo sightbender', 'Minamo Sightbender').
card_type('minamo sightbender', 'Creature — Human Wizard').
card_types('minamo sightbender', ['Creature']).
card_subtypes('minamo sightbender', ['Human', 'Wizard']).
card_colors('minamo sightbender', ['U']).
card_text('minamo sightbender', '{X}, {T}: Target creature with power X or less can\'t be blocked this turn.').
card_mana_cost('minamo sightbender', ['1', 'U']).
card_cmc('minamo sightbender', 2).
card_layout('minamo sightbender', 'normal').
card_power('minamo sightbender', 1).
card_toughness('minamo sightbender', 2).

% Found in: BOK
card_name('minamo\'s meddling', 'Minamo\'s Meddling').
card_type('minamo\'s meddling', 'Instant').
card_types('minamo\'s meddling', ['Instant']).
card_subtypes('minamo\'s meddling', []).
card_colors('minamo\'s meddling', ['U']).
card_text('minamo\'s meddling', 'Counter target spell. That spell\'s controller reveals his or her hand, then discards each card with the same name as a card spliced onto that spell.').
card_mana_cost('minamo\'s meddling', ['2', 'U', 'U']).
card_cmc('minamo\'s meddling', 4).
card_layout('minamo\'s meddling', 'normal').

% Found in: CHK
card_name('minamo, school at water\'s edge', 'Minamo, School at Water\'s Edge').
card_type('minamo, school at water\'s edge', 'Legendary Land').
card_types('minamo, school at water\'s edge', ['Land']).
card_subtypes('minamo, school at water\'s edge', []).
card_supertypes('minamo, school at water\'s edge', ['Legendary']).
card_colors('minamo, school at water\'s edge', []).
card_text('minamo, school at water\'s edge', '{T}: Add {U} to your mana pool.\n{U}, {T}: Untap target legendary permanent.').
card_layout('minamo, school at water\'s edge', 'normal').

% Found in: 10E, 8ED, 9ED, MIR
card_name('mind bend', 'Mind Bend').
card_type('mind bend', 'Instant').
card_types('mind bend', ['Instant']).
card_subtypes('mind bend', []).
card_colors('mind bend', ['U']).
card_text('mind bend', 'Change the text of target permanent by replacing all instances of one color word with another or one basic land type with another. (For example, you may change \"nonblack creature\" to \"nongreen creature\" or \"forestwalk\" to \"islandwalk.\" This effect lasts indefinitely.)').
card_mana_cost('mind bend', ['U']).
card_cmc('mind bend', 1).
card_layout('mind bend', 'normal').

% Found in: 4ED, 5ED, DRK
card_name('mind bomb', 'Mind Bomb').
card_type('mind bomb', 'Sorcery').
card_types('mind bomb', ['Sorcery']).
card_subtypes('mind bomb', []).
card_colors('mind bomb', ['U']).
card_text('mind bomb', 'Each player may discard up to three cards. Mind Bomb deals damage to each player equal to 3 minus the number of cards he or she discarded this way.').
card_mana_cost('mind bomb', ['U']).
card_cmc('mind bomb', 1).
card_layout('mind bomb', 'normal').

% Found in: ODY
card_name('mind burst', 'Mind Burst').
card_type('mind burst', 'Sorcery').
card_types('mind burst', ['Sorcery']).
card_subtypes('mind burst', []).
card_colors('mind burst', ['B']).
card_text('mind burst', 'Target player discards X cards, where X is one plus the number of cards named Mind Burst in all graveyards.').
card_mana_cost('mind burst', ['1', 'B']).
card_cmc('mind burst', 2).
card_layout('mind burst', 'normal').

% Found in: DPA, M10, M11, M12, pWPN
card_name('mind control', 'Mind Control').
card_type('mind control', 'Enchantment — Aura').
card_types('mind control', ['Enchantment']).
card_subtypes('mind control', ['Aura']).
card_colors('mind control', ['U']).
card_text('mind control', 'Enchant creature\nYou control enchanted creature.').
card_mana_cost('mind control', ['3', 'U', 'U']).
card_cmc('mind control', 5).
card_layout('mind control', 'normal').

% Found in: APC
card_name('mind extraction', 'Mind Extraction').
card_type('mind extraction', 'Sorcery').
card_types('mind extraction', ['Sorcery']).
card_subtypes('mind extraction', []).
card_colors('mind extraction', ['B']).
card_text('mind extraction', 'As an additional cost to cast Mind Extraction, sacrifice a creature.\nTarget player reveals his or her hand and discards all cards of each of the sacrificed creature\'s colors.').
card_mana_cost('mind extraction', ['2', 'B']).
card_cmc('mind extraction', 3).
card_layout('mind extraction', 'normal').

% Found in: ARB, MMA
card_name('mind funeral', 'Mind Funeral').
card_type('mind funeral', 'Sorcery').
card_types('mind funeral', ['Sorcery']).
card_subtypes('mind funeral', []).
card_colors('mind funeral', ['U', 'B']).
card_text('mind funeral', 'Target opponent reveals cards from the top of his or her library until four land cards are revealed. That player puts all cards revealed this way into his or her graveyard.').
card_mana_cost('mind funeral', ['1', 'U', 'B']).
card_cmc('mind funeral', 3).
card_layout('mind funeral', 'normal').

% Found in: STH
card_name('mind games', 'Mind Games').
card_type('mind games', 'Instant').
card_types('mind games', ['Instant']).
card_subtypes('mind games', []).
card_colors('mind games', ['U']).
card_text('mind games', 'Buyback {2}{U} (You may pay an additional {2}{U} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTap target artifact, creature, or land.').
card_mana_cost('mind games', ['U']).
card_cmc('mind games', 1).
card_layout('mind games', 'normal').

% Found in: GTC
card_name('mind grind', 'Mind Grind').
card_type('mind grind', 'Sorcery').
card_types('mind grind', ['Sorcery']).
card_subtypes('mind grind', []).
card_colors('mind grind', ['U', 'B']).
card_text('mind grind', 'Each opponent reveals cards from the top of his or her library until he or she reveals X land cards, then puts all cards revealed this way into his or her graveyard. X can\'t be 0.').
card_mana_cost('mind grind', ['X', 'U', 'B']).
card_cmc('mind grind', 2).
card_layout('mind grind', 'normal').

% Found in: MIR
card_name('mind harness', 'Mind Harness').
card_type('mind harness', 'Enchantment — Aura').
card_types('mind harness', ['Enchantment']).
card_subtypes('mind harness', ['Aura']).
card_colors('mind harness', ['U']).
card_text('mind harness', 'Enchant red or green creature\nCumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nYou control enchanted creature.').
card_mana_cost('mind harness', ['U']).
card_cmc('mind harness', 1).
card_layout('mind harness', 'normal').

% Found in: POR
card_name('mind knives', 'Mind Knives').
card_type('mind knives', 'Sorcery').
card_types('mind knives', ['Sorcery']).
card_subtypes('mind knives', []).
card_colors('mind knives', ['B']).
card_text('mind knives', 'Target opponent discards a card at random.').
card_mana_cost('mind knives', ['1', 'B']).
card_cmc('mind knives', 2).
card_layout('mind knives', 'normal').

% Found in: EXO
card_name('mind maggots', 'Mind Maggots').
card_type('mind maggots', 'Creature — Insect').
card_types('mind maggots', ['Creature']).
card_subtypes('mind maggots', ['Insect']).
card_colors('mind maggots', ['B']).
card_text('mind maggots', 'When Mind Maggots enters the battlefield, discard any number of creature cards. For each card discarded this way, put two +1/+1 counters on Mind Maggots.').
card_mana_cost('mind maggots', ['3', 'B']).
card_cmc('mind maggots', 4).
card_layout('mind maggots', 'normal').
card_power('mind maggots', 2).
card_toughness('mind maggots', 2).

% Found in: EXO
card_name('mind over matter', 'Mind Over Matter').
card_type('mind over matter', 'Enchantment').
card_types('mind over matter', ['Enchantment']).
card_subtypes('mind over matter', []).
card_colors('mind over matter', ['U']).
card_text('mind over matter', 'Discard a card: You may tap or untap target artifact, creature, or land.').
card_mana_cost('mind over matter', ['2', 'U', 'U', 'U', 'U']).
card_cmc('mind over matter', 6).
card_layout('mind over matter', 'normal').
card_reserved('mind over matter').

% Found in: STH
card_name('mind peel', 'Mind Peel').
card_type('mind peel', 'Sorcery').
card_types('mind peel', ['Sorcery']).
card_subtypes('mind peel', []).
card_colors('mind peel', ['B']).
card_text('mind peel', 'Buyback {2}{B}{B} (You may pay an additional {2}{B}{B} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget player discards a card.').
card_mana_cost('mind peel', ['B']).
card_cmc('mind peel', 1).
card_layout('mind peel', 'normal').

% Found in: BFZ
card_name('mind raker', 'Mind Raker').
card_type('mind raker', 'Creature — Eldrazi Processor').
card_types('mind raker', ['Creature']).
card_subtypes('mind raker', ['Eldrazi', 'Processor']).
card_colors('mind raker', []).
card_text('mind raker', 'Devoid (This card has no color.)\nWhen Mind Raker enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, each opponent discards a card.').
card_mana_cost('mind raker', ['3', 'B']).
card_cmc('mind raker', 4).
card_layout('mind raker', 'normal').
card_power('mind raker', 3).
card_toughness('mind raker', 3).

% Found in: 5ED, ICE
card_name('mind ravel', 'Mind Ravel').
card_type('mind ravel', 'Sorcery').
card_types('mind ravel', ['Sorcery']).
card_subtypes('mind ravel', []).
card_colors('mind ravel', ['B']).
card_text('mind ravel', 'Target player discards a card.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('mind ravel', ['2', 'B']).
card_cmc('mind ravel', 3).
card_layout('mind ravel', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, DPA, DTK, M10, M11, M12, M13, M14, M15, ORI, PO2, POR, RTR, S99
card_name('mind rot', 'Mind Rot').
card_type('mind rot', 'Sorcery').
card_types('mind rot', ['Sorcery']).
card_subtypes('mind rot', []).
card_colors('mind rot', ['B']).
card_text('mind rot', 'Target player discards two cards.').
card_mana_cost('mind rot', ['2', 'B']).
card_cmc('mind rot', 3).
card_layout('mind rot', 'normal').

% Found in: M13, M15
card_name('mind sculpt', 'Mind Sculpt').
card_type('mind sculpt', 'Sorcery').
card_types('mind sculpt', ['Sorcery']).
card_subtypes('mind sculpt', []).
card_colors('mind sculpt', ['U']).
card_text('mind sculpt', 'Target opponent puts the top seven cards of his or her library into his or her graveyard.').
card_mana_cost('mind sculpt', ['1', 'U']).
card_cmc('mind sculpt', 2).
card_layout('mind sculpt', 'normal').

% Found in: DPA, M10, MOR
card_name('mind shatter', 'Mind Shatter').
card_type('mind shatter', 'Sorcery').
card_types('mind shatter', ['Sorcery']).
card_subtypes('mind shatter', []).
card_colors('mind shatter', ['B']).
card_text('mind shatter', 'Target player discards X cards at random.').
card_mana_cost('mind shatter', ['X', 'B', 'B']).
card_cmc('mind shatter', 2).
card_layout('mind shatter', 'normal').

% Found in: 8ED, NMS
card_name('mind slash', 'Mind Slash').
card_type('mind slash', 'Enchantment').
card_types('mind slash', ['Enchantment']).
card_subtypes('mind slash', []).
card_colors('mind slash', ['B']).
card_text('mind slash', '{B}, Sacrifice a creature: Target opponent reveals his or her hand. You choose a card from it. That player discards that card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('mind slash', ['1', 'B', 'B']).
card_cmc('mind slash', 3).
card_layout('mind slash', 'normal').

% Found in: 8ED, TOR, ZEN
card_name('mind sludge', 'Mind Sludge').
card_type('mind sludge', 'Sorcery').
card_types('mind sludge', ['Sorcery']).
card_subtypes('mind sludge', []).
card_colors('mind sludge', ['B']).
card_text('mind sludge', 'Target player discards a card for each Swamp you control.').
card_mana_cost('mind sludge', ['4', 'B']).
card_cmc('mind sludge', 5).
card_layout('mind sludge', 'normal').

% Found in: DPA, M10, MOR
card_name('mind spring', 'Mind Spring').
card_type('mind spring', 'Sorcery').
card_types('mind spring', ['Sorcery']).
card_subtypes('mind spring', []).
card_colors('mind spring', ['U']).
card_text('mind spring', 'Draw X cards.').
card_mana_cost('mind spring', ['X', 'U', 'U']).
card_cmc('mind spring', 2).
card_layout('mind spring', 'normal').

% Found in: 10E, C14, DD2, DD3_JVC, DDP, WTH, pGTW
card_name('mind stone', 'Mind Stone').
card_type('mind stone', 'Artifact').
card_types('mind stone', ['Artifact']).
card_subtypes('mind stone', []).
card_colors('mind stone', []).
card_text('mind stone', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Mind Stone: Draw a card.').
card_mana_cost('mind stone', ['2']).
card_cmc('mind stone', 2).
card_layout('mind stone', 'normal').

% Found in: NMS
card_name('mind swords', 'Mind Swords').
card_type('mind swords', 'Sorcery').
card_types('mind swords', ['Sorcery']).
card_subtypes('mind swords', []).
card_colors('mind swords', ['B']).
card_text('mind swords', 'If you control a Swamp, you may sacrifice a creature rather than pay Mind Swords\'s mana cost.\nEach player exiles two cards from his or her hand.').
card_mana_cost('mind swords', ['1', 'B']).
card_cmc('mind swords', 2).
card_layout('mind swords', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME3
card_name('mind twist', 'Mind Twist').
card_type('mind twist', 'Sorcery').
card_types('mind twist', ['Sorcery']).
card_subtypes('mind twist', []).
card_colors('mind twist', ['B']).
card_text('mind twist', 'Target player discards X cards at random.').
card_mana_cost('mind twist', ['X', 'B']).
card_cmc('mind twist', 1).
card_layout('mind twist', 'normal').

% Found in: M12
card_name('mind unbound', 'Mind Unbound').
card_type('mind unbound', 'Enchantment').
card_types('mind unbound', ['Enchantment']).
card_subtypes('mind unbound', []).
card_colors('mind unbound', ['U']).
card_text('mind unbound', 'At the beginning of your upkeep, put a lore counter on Mind Unbound, then draw a card for each lore counter on Mind Unbound.').
card_mana_cost('mind unbound', ['4', 'U', 'U']).
card_cmc('mind unbound', 6).
card_layout('mind unbound', 'normal').

% Found in: 5ED, 6ED, ICE, pFNM
card_name('mind warp', 'Mind Warp').
card_type('mind warp', 'Sorcery').
card_types('mind warp', ['Sorcery']).
card_subtypes('mind warp', []).
card_colors('mind warp', ['B']).
card_text('mind warp', 'Look at target player\'s hand and choose X cards from it. That player discards those cards.').
card_mana_cost('mind warp', ['X', '3', 'B']).
card_cmc('mind warp', 4).
card_layout('mind warp', 'normal').

% Found in: ICE
card_name('mind whip', 'Mind Whip').
card_type('mind whip', 'Enchantment — Aura').
card_types('mind whip', ['Enchantment']).
card_subtypes('mind whip', ['Aura']).
card_colors('mind whip', ['B']).
card_text('mind whip', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may pay {3}. If he or she doesn\'t, Mind Whip deals 2 damage to that player and you tap that creature.').
card_mana_cost('mind whip', ['2', 'B', 'B']).
card_cmc('mind whip', 4).
card_layout('mind whip', 'normal').

% Found in: SCG, VMA, pJGP
card_name('mind\'s desire', 'Mind\'s Desire').
card_type('mind\'s desire', 'Sorcery').
card_types('mind\'s desire', ['Sorcery']).
card_subtypes('mind\'s desire', []).
card_colors('mind\'s desire', ['U']).
card_text('mind\'s desire', 'Shuffle your library. Then exile the top card of your library. Until end of turn, you may play that card without paying its mana cost. (If it has X in its mana cost, X is 0.)\nStorm (When you cast this spell, copy it for each spell cast before it this turn.)').
card_mana_cost('mind\'s desire', ['4', 'U', 'U']).
card_cmc('mind\'s desire', 6).
card_layout('mind\'s desire', 'normal').

% Found in: CM1, MRD
card_name('mind\'s eye', 'Mind\'s Eye').
card_type('mind\'s eye', 'Artifact').
card_types('mind\'s eye', ['Artifact']).
card_subtypes('mind\'s eye', []).
card_colors('mind\'s eye', []).
card_text('mind\'s eye', 'Whenever an opponent draws a card, you may pay {1}. If you do, draw a card.').
card_mana_cost('mind\'s eye', ['5']).
card_cmc('mind\'s eye', 5).
card_layout('mind\'s eye', 'normal').

% Found in: MIR
card_name('mindbender spores', 'Mindbender Spores').
card_type('mindbender spores', 'Creature — Fungus Wall').
card_types('mindbender spores', ['Creature']).
card_subtypes('mindbender spores', ['Fungus', 'Wall']).
card_colors('mindbender spores', ['G']).
card_text('mindbender spores', 'Defender (This creature can\'t attack.)\nFlying\nWhenever Mindbender Spores blocks a creature, put four fungus counters on that creature. The creature gains \"This creature doesn\'t untap during your untap step if it has a fungus counter on it\" and \"At the beginning of your upkeep, remove a fungus counter from this creature.\"').
card_mana_cost('mindbender spores', ['2', 'G']).
card_cmc('mindbender spores', 3).
card_layout('mindbender spores', 'normal').
card_power('mindbender spores', 0).
card_toughness('mindbender spores', 1).
card_reserved('mindbender spores').

% Found in: CHK
card_name('mindblaze', 'Mindblaze').
card_type('mindblaze', 'Sorcery').
card_types('mindblaze', ['Sorcery']).
card_subtypes('mindblaze', []).
card_colors('mindblaze', ['R']).
card_text('mindblaze', 'Name a nonland card and choose a number greater than 0. Target player reveals his or her library. If that library contains exactly the chosen number of the named card, Mindblaze deals 8 damage to that player. Then that player shuffles his or her library.').
card_mana_cost('mindblaze', ['5', 'R']).
card_cmc('mindblaze', 6).
card_layout('mindblaze', 'normal').

% Found in: ZEN
card_name('mindbreak trap', 'Mindbreak Trap').
card_type('mindbreak trap', 'Instant — Trap').
card_types('mindbreak trap', ['Instant']).
card_subtypes('mindbreak trap', ['Trap']).
card_colors('mindbreak trap', ['U']).
card_text('mindbreak trap', 'If an opponent cast three or more spells this turn, you may pay {0} rather than pay Mindbreak Trap\'s mana cost.\nExile any number of target spells.').
card_mana_cost('mindbreak trap', ['2', 'U', 'U']).
card_cmc('mindbreak trap', 4).
card_layout('mindbreak trap', 'normal').

% Found in: M13
card_name('mindclaw shaman', 'Mindclaw Shaman').
card_type('mindclaw shaman', 'Creature — Viashino Shaman').
card_types('mindclaw shaman', ['Creature']).
card_subtypes('mindclaw shaman', ['Viashino', 'Shaman']).
card_colors('mindclaw shaman', ['R']).
card_text('mindclaw shaman', 'When Mindclaw Shaman enters the battlefield, target opponent reveals his or her hand. You may cast an instant or sorcery card from it without paying its mana cost.').
card_mana_cost('mindclaw shaman', ['4', 'R']).
card_cmc('mindclaw shaman', 5).
card_layout('mindclaw shaman', 'normal').
card_power('mindclaw shaman', 2).
card_toughness('mindclaw shaman', 2).

% Found in: NPH
card_name('mindcrank', 'Mindcrank').
card_type('mindcrank', 'Artifact').
card_types('mindcrank', ['Artifact']).
card_subtypes('mindcrank', []).
card_colors('mindcrank', []).
card_text('mindcrank', 'Whenever an opponent loses life, that player puts that many cards from the top of his or her library into his or her graveyard. (Damage dealt by sources without infect causes loss of life.)').
card_mana_cost('mindcrank', ['2']).
card_cmc('mindcrank', 2).
card_layout('mindcrank', 'normal').

% Found in: NPH
card_name('mindculling', 'Mindculling').
card_type('mindculling', 'Sorcery').
card_types('mindculling', ['Sorcery']).
card_subtypes('mindculling', []).
card_colors('mindculling', ['U']).
card_text('mindculling', 'You draw two cards and target opponent discards two cards.').
card_mana_cost('mindculling', ['5', 'U']).
card_cmc('mindculling', 6).
card_layout('mindculling', 'normal').

% Found in: GTC
card_name('mindeye drake', 'Mindeye Drake').
card_type('mindeye drake', 'Creature — Drake').
card_types('mindeye drake', ['Creature']).
card_subtypes('mindeye drake', ['Drake']).
card_colors('mindeye drake', ['U']).
card_text('mindeye drake', 'Flying\nWhen Mindeye Drake dies, target player puts the top five cards of his or her library into his or her graveyard.').
card_mana_cost('mindeye drake', ['4', 'U']).
card_cmc('mindeye drake', 5).
card_layout('mindeye drake', 'normal').
card_power('mindeye drake', 2).
card_toughness('mindeye drake', 5).

% Found in: TSP
card_name('mindlash sliver', 'Mindlash Sliver').
card_type('mindlash sliver', 'Creature — Sliver').
card_types('mindlash sliver', ['Creature']).
card_subtypes('mindlash sliver', ['Sliver']).
card_colors('mindlash sliver', ['B']).
card_text('mindlash sliver', 'All Slivers have \"{1}, Sacrifice this permanent: Each player discards a card.\"').
card_mana_cost('mindlash sliver', ['B']).
card_cmc('mindlash sliver', 1).
card_layout('mindlash sliver', 'normal').
card_power('mindlash sliver', 1).
card_toughness('mindlash sliver', 1).

% Found in: RAV
card_name('mindleech mass', 'Mindleech Mass').
card_type('mindleech mass', 'Creature — Horror').
card_types('mindleech mass', ['Creature']).
card_subtypes('mindleech mass', ['Horror']).
card_colors('mindleech mass', ['U', 'B']).
card_text('mindleech mass', 'Trample\nWhenever Mindleech Mass deals combat damage to a player, you may look at that player\'s hand. If you do, you may cast a nonland card in it without paying that card\'s mana cost.').
card_mana_cost('mindleech mass', ['5', 'U', 'B', 'B']).
card_cmc('mindleech mass', 8).
card_layout('mindleech mass', 'normal').
card_power('mindleech mass', 6).
card_toughness('mindleech mass', 6).

% Found in: EXO, TPR, TSB
card_name('mindless automaton', 'Mindless Automaton').
card_type('mindless automaton', 'Artifact Creature — Construct').
card_types('mindless automaton', ['Artifact', 'Creature']).
card_subtypes('mindless automaton', ['Construct']).
card_colors('mindless automaton', []).
card_text('mindless automaton', 'Mindless Automaton enters the battlefield with two +1/+1 counters on it.\n{1}, Discard a card: Put a +1/+1 counter on Mindless Automaton.\nRemove two +1/+1 counters from Mindless Automaton: Draw a card.').
card_mana_cost('mindless automaton', ['4']).
card_cmc('mindless automaton', 4).
card_layout('mindless automaton', 'normal').
card_power('mindless automaton', 0).
card_toughness('mindless automaton', 0).

% Found in: ZEN
card_name('mindless null', 'Mindless Null').
card_type('mindless null', 'Creature — Zombie').
card_types('mindless null', ['Creature']).
card_subtypes('mindless null', ['Zombie']).
card_colors('mindless null', ['B']).
card_text('mindless null', 'Mindless Null can\'t block unless you control a Vampire.').
card_mana_cost('mindless null', ['2', 'B']).
card_cmc('mindless null', 3).
card_layout('mindless null', 'normal').
card_power('mindless null', 2).
card_toughness('mindless null', 2).

% Found in: ALA
card_name('mindlock orb', 'Mindlock Orb').
card_type('mindlock orb', 'Artifact').
card_types('mindlock orb', ['Artifact']).
card_subtypes('mindlock orb', []).
card_colors('mindlock orb', ['U']).
card_text('mindlock orb', 'Players can\'t search libraries.').
card_mana_cost('mindlock orb', ['3', 'U']).
card_cmc('mindlock orb', 4).
card_layout('mindlock orb', 'normal').

% Found in: RAV
card_name('mindmoil', 'Mindmoil').
card_type('mindmoil', 'Enchantment').
card_types('mindmoil', ['Enchantment']).
card_subtypes('mindmoil', []).
card_colors('mindmoil', ['R']).
card_text('mindmoil', 'Whenever you cast a spell, put the cards in your hand on the bottom of your library in any order, then draw that many cards.').
card_mana_cost('mindmoil', ['4', 'R']).
card_cmc('mindmoil', 5).
card_layout('mindmoil', 'normal').

% Found in: BNG
card_name('mindreaver', 'Mindreaver').
card_type('mindreaver', 'Creature — Human Wizard').
card_types('mindreaver', ['Creature']).
card_subtypes('mindreaver', ['Human', 'Wizard']).
card_colors('mindreaver', ['U']).
card_text('mindreaver', 'Heroic — Whenever you cast a spell that targets Mindreaver, exile the top three cards of target player\'s library.\n{U}{U}, Sacrifice Mindreaver: Counter target spell with the same name as a card exiled with Mindreaver.').
card_mana_cost('mindreaver', ['U', 'U']).
card_cmc('mindreaver', 2).
card_layout('mindreaver', 'normal').
card_power('mindreaver', 2).
card_toughness('mindreaver', 1).

% Found in: CMD
card_name('minds aglow', 'Minds Aglow').
card_type('minds aglow', 'Sorcery').
card_types('minds aglow', ['Sorcery']).
card_subtypes('minds aglow', []).
card_colors('minds aglow', ['U']).
card_text('minds aglow', 'Join forces — Starting with you, each player may pay any amount of mana. Each player draws X cards, where X is the total amount of mana paid this way.').
card_mana_cost('minds aglow', ['U']).
card_cmc('minds aglow', 1).
card_layout('minds aglow', 'normal').

% Found in: FRF
card_name('mindscour dragon', 'Mindscour Dragon').
card_type('mindscour dragon', 'Creature — Dragon').
card_types('mindscour dragon', ['Creature']).
card_subtypes('mindscour dragon', ['Dragon']).
card_colors('mindscour dragon', ['U']).
card_text('mindscour dragon', 'Flying\nWhenever Mindscour Dragon deals combat damage to an opponent, target player puts the top four cards of his or her library into his or her graveyard.').
card_mana_cost('mindscour dragon', ['4', 'U', 'U']).
card_cmc('mindscour dragon', 6).
card_layout('mindscour dragon', 'normal').
card_power('mindscour dragon', 4).
card_toughness('mindscour dragon', 4).

% Found in: ISD
card_name('mindshrieker', 'Mindshrieker').
card_type('mindshrieker', 'Creature — Spirit Bird').
card_types('mindshrieker', ['Creature']).
card_subtypes('mindshrieker', ['Spirit', 'Bird']).
card_colors('mindshrieker', ['U']).
card_text('mindshrieker', 'Flying\n{2}: Target player puts the top card of his or her library into his or her graveyard. Mindshrieker gets +X/+X until end of turn, where X is that card\'s converted mana cost.').
card_mana_cost('mindshrieker', ['1', 'U']).
card_cmc('mindshrieker', 2).
card_layout('mindshrieker', 'normal').
card_power('mindshrieker', 1).
card_toughness('mindshrieker', 1).

% Found in: MRD, SOM
card_name('mindslaver', 'Mindslaver').
card_type('mindslaver', 'Legendary Artifact').
card_types('mindslaver', ['Artifact']).
card_subtypes('mindslaver', []).
card_supertypes('mindslaver', ['Legendary']).
card_colors('mindslaver', []).
card_text('mindslaver', '{4}, {T}, Sacrifice Mindslaver: You control target player during that player\'s next turn. (You see all cards that player could see and make all decisions for the player.)').
card_mana_cost('mindslaver', ['6']).
card_cmc('mindslaver', 6).
card_layout('mindslaver', 'normal').

% Found in: 9ED, ODY
card_name('mindslicer', 'Mindslicer').
card_type('mindslicer', 'Creature — Horror').
card_types('mindslicer', ['Creature']).
card_subtypes('mindslicer', ['Horror']).
card_colors('mindslicer', ['B']).
card_text('mindslicer', 'When Mindslicer dies, each player discards his or her hand.').
card_mana_cost('mindslicer', ['2', 'B', 'B']).
card_cmc('mindslicer', 4).
card_layout('mindslicer', 'normal').
card_power('mindslicer', 4).
card_toughness('mindslicer', 3).

% Found in: M14
card_name('mindsparker', 'Mindsparker').
card_type('mindsparker', 'Creature — Elemental').
card_types('mindsparker', ['Creature']).
card_subtypes('mindsparker', ['Elemental']).
card_colors('mindsparker', ['R']).
card_text('mindsparker', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhenever an opponent casts a white or blue instant or sorcery spell, Mindsparker deals 2 damage to that player.').
card_mana_cost('mindsparker', ['1', 'R', 'R']).
card_cmc('mindsparker', 3).
card_layout('mindsparker', 'normal').
card_power('mindsparker', 3).
card_toughness('mindsparker', 2).

% Found in: TSP
card_name('mindstab', 'Mindstab').
card_type('mindstab', 'Sorcery').
card_types('mindstab', ['Sorcery']).
card_subtypes('mindstab', []).
card_colors('mindstab', ['B']).
card_text('mindstab', 'Target player discards three cards.\nSuspend 4—{B} (Rather than cast this card from your hand, you may pay {B} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('mindstab', ['5', 'B']).
card_cmc('mindstab', 6).
card_layout('mindstab', 'normal').

% Found in: 5ED, FEM, MED
card_name('mindstab thrull', 'Mindstab Thrull').
card_type('mindstab thrull', 'Creature — Thrull').
card_types('mindstab thrull', ['Creature']).
card_subtypes('mindstab thrull', ['Thrull']).
card_colors('mindstab thrull', ['B']).
card_text('mindstab thrull', 'Whenever Mindstab Thrull attacks and isn\'t blocked, you may sacrifice it. If you do, defending player discards three cards.').
card_mana_cost('mindstab thrull', ['1', 'B', 'B']).
card_cmc('mindstab thrull', 3).
card_layout('mindstab thrull', 'normal').
card_power('mindstab thrull', 2).
card_toughness('mindstab thrull', 2).

% Found in: DGM
card_name('mindstatic', 'Mindstatic').
card_type('mindstatic', 'Instant').
card_types('mindstatic', ['Instant']).
card_subtypes('mindstatic', []).
card_colors('mindstatic', ['U']).
card_text('mindstatic', 'Counter target spell unless its controller pays {6}.').
card_mana_cost('mindstatic', ['3', 'U']).
card_cmc('mindstatic', 4).
card_layout('mindstatic', 'normal').

% Found in: MRD
card_name('mindstorm crown', 'Mindstorm Crown').
card_type('mindstorm crown', 'Artifact').
card_types('mindstorm crown', ['Artifact']).
card_subtypes('mindstorm crown', []).
card_colors('mindstorm crown', []).
card_text('mindstorm crown', 'At the beginning of your upkeep, draw a card if you had no cards in hand at the beginning of this turn. If you had a card in hand, Mindstorm Crown deals 1 damage to you.').
card_mana_cost('mindstorm crown', ['3']).
card_cmc('mindstorm crown', 3).
card_layout('mindstorm crown', 'normal').

% Found in: KTK
card_name('mindswipe', 'Mindswipe').
card_type('mindswipe', 'Instant').
card_types('mindswipe', ['Instant']).
card_subtypes('mindswipe', []).
card_colors('mindswipe', ['U', 'R']).
card_text('mindswipe', 'Counter target spell unless its controller pays {X}. Mindswipe deals X damage to that spell\'s controller.').
card_mana_cost('mindswipe', ['X', 'U', 'R']).
card_cmc('mindswipe', 2).
card_layout('mindswipe', 'normal').

% Found in: STH
card_name('mindwarper', 'Mindwarper').
card_type('mindwarper', 'Creature — Spirit').
card_types('mindwarper', ['Creature']).
card_subtypes('mindwarper', ['Spirit']).
card_colors('mindwarper', ['B']).
card_text('mindwarper', 'Mindwarper enters the battlefield with three +1/+1 counters on it.\n{2}{B}, Remove a +1/+1 counter from Mindwarper: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('mindwarper', ['2', 'B', 'B']).
card_cmc('mindwarper', 4).
card_layout('mindwarper', 'normal').
card_power('mindwarper', 0).
card_toughness('mindwarper', 0).

% Found in: TMP
card_name('mindwhip sliver', 'Mindwhip Sliver').
card_type('mindwhip sliver', 'Creature — Sliver').
card_types('mindwhip sliver', ['Creature']).
card_subtypes('mindwhip sliver', ['Sliver']).
card_colors('mindwhip sliver', ['B']).
card_text('mindwhip sliver', 'All Slivers have \"{2}, Sacrifice this permanent: Target player discards a card at random. Activate this ability only any time you could cast a sorcery.\"').
card_mana_cost('mindwhip sliver', ['2', 'B']).
card_cmc('mindwhip sliver', 3).
card_layout('mindwhip sliver', 'normal').
card_power('mindwhip sliver', 2).
card_toughness('mindwhip sliver', 2).

% Found in: EVE
card_name('mindwrack liege', 'Mindwrack Liege').
card_type('mindwrack liege', 'Creature — Horror').
card_types('mindwrack liege', ['Creature']).
card_subtypes('mindwrack liege', ['Horror']).
card_colors('mindwrack liege', ['U', 'R']).
card_text('mindwrack liege', 'Other blue creatures you control get +1/+1.\nOther red creatures you control get +1/+1.\n{U/R}{U/R}{U/R}{U/R}: You may put a blue or red creature card from your hand onto the battlefield.').
card_mana_cost('mindwrack liege', ['3', 'U/R', 'U/R', 'U/R']).
card_cmc('mindwrack liege', 6).
card_layout('mindwrack liege', 'normal').
card_power('mindwrack liege', 4).
card_toughness('mindwrack liege', 4).

% Found in: PCY
card_name('mine bearer', 'Mine Bearer').
card_type('mine bearer', 'Creature — Human Soldier').
card_types('mine bearer', ['Creature']).
card_subtypes('mine bearer', ['Human', 'Soldier']).
card_colors('mine bearer', ['W']).
card_text('mine bearer', '{T}, Sacrifice Mine Bearer: Destroy target attacking creature.').
card_mana_cost('mine bearer', ['2', 'W']).
card_cmc('mine bearer', 3).
card_layout('mine bearer', 'normal').
card_power('mine bearer', 1).
card_toughness('mine bearer', 1).

% Found in: SHM
card_name('mine excavation', 'Mine Excavation').
card_type('mine excavation', 'Sorcery').
card_types('mine excavation', ['Sorcery']).
card_subtypes('mine excavation', []).
card_colors('mine excavation', ['W']).
card_text('mine excavation', 'Return target artifact or enchantment card from a graveyard to its owner\'s hand.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('mine excavation', ['1', 'W']).
card_cmc('mine excavation', 2).
card_layout('mine excavation', 'normal').

% Found in: ODY
card_name('mine layer', 'Mine Layer').
card_type('mine layer', 'Creature — Dwarf').
card_types('mine layer', ['Creature']).
card_subtypes('mine layer', ['Dwarf']).
card_colors('mine layer', ['R']).
card_text('mine layer', '{1}{R}, {T}: Put a mine counter on target land.\nWhenever a land with a mine counter on it becomes tapped, destroy it.\nWhen Mine Layer leaves the battlefield, remove all mine counters from all lands.').
card_mana_cost('mine layer', ['3', 'R']).
card_cmc('mine layer', 4).
card_layout('mine layer', 'normal').
card_power('mine layer', 1).
card_toughness('mine layer', 1).

% Found in: UGL
card_name('mine, mine, mine!', 'Mine, Mine, Mine!').
card_type('mine, mine, mine!', 'Enchantment').
card_types('mine, mine, mine!', ['Enchantment']).
card_subtypes('mine, mine, mine!', []).
card_colors('mine, mine, mine!', ['G']).
card_text('mine, mine, mine!', 'When Mine, Mine, Mine comes into play, each player puts his or her library into his or her hand.\nEach player skips his or her discard phase and does not lose as a result of being unable to draw a card.\nEach player cannot play more than one spell each turn.\nIf Mine, Mine, Mine leaves play, each player shuffles his or her hand and graveyard into his or her library.').
card_mana_cost('mine, mine, mine!', ['4', 'G', 'G']).
card_cmc('mine, mine, mine!', 6).
card_layout('mine, mine, mine!', 'normal').

% Found in: M15
card_name('miner\'s bane', 'Miner\'s Bane').
card_type('miner\'s bane', 'Creature — Elemental').
card_types('miner\'s bane', ['Creature']).
card_subtypes('miner\'s bane', ['Elemental']).
card_colors('miner\'s bane', ['R']).
card_text('miner\'s bane', '{2}{R}: Miner\'s Bane gets +1/+0 and gains trample until end of turn. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('miner\'s bane', ['4', 'R', 'R']).
card_cmc('miner\'s bane', 6).
card_layout('miner\'s bane', 'normal').
card_power('miner\'s bane', 6).
card_toughness('miner\'s bane', 3).

% Found in: DDE
card_name('minion', 'Minion').
card_type('minion', 'Creature — Minion').
card_types('minion', ['Creature']).
card_subtypes('minion', ['Minion']).
card_colors('minion', []).
card_text('minion', '').
card_layout('minion', 'token').
card_power('minion', '*').
card_toughness('minion', '*').

% Found in: ICE, ME2
card_name('minion of leshrac', 'Minion of Leshrac').
card_type('minion of leshrac', 'Creature — Demon Minion').
card_types('minion of leshrac', ['Creature']).
card_subtypes('minion of leshrac', ['Demon', 'Minion']).
card_colors('minion of leshrac', ['B']).
card_text('minion of leshrac', 'Protection from black\nAt the beginning of your upkeep, Minion of Leshrac deals 5 damage to you unless you sacrifice a creature other than Minion of Leshrac. If Minion of Leshrac deals damage to you this way, tap it.\n{T}: Destroy target creature or land.').
card_mana_cost('minion of leshrac', ['4', 'B', 'B', 'B']).
card_cmc('minion of leshrac', 7).
card_layout('minion of leshrac', 'normal').
card_power('minion of leshrac', 5).
card_toughness('minion of leshrac', 5).

% Found in: ICE, ME4
card_name('minion of tevesh szat', 'Minion of Tevesh Szat').
card_type('minion of tevesh szat', 'Creature — Demon Minion').
card_types('minion of tevesh szat', ['Creature']).
card_subtypes('minion of tevesh szat', ['Demon', 'Minion']).
card_colors('minion of tevesh szat', ['B']).
card_text('minion of tevesh szat', 'At the beginning of your upkeep, Minion of Tevesh Szat deals 2 damage to you unless you pay {B}{B}.\n{T}: Target creature gets +3/-2 until end of turn.').
card_mana_cost('minion of tevesh szat', ['4', 'B', 'B', 'B']).
card_cmc('minion of tevesh szat', 7).
card_layout('minion of tevesh szat', 'normal').
card_power('minion of tevesh szat', 4).
card_toughness('minion of tevesh szat', 4).
card_reserved('minion of tevesh szat').

% Found in: TMP
card_name('minion of the wastes', 'Minion of the Wastes').
card_type('minion of the wastes', 'Creature — Minion').
card_types('minion of the wastes', ['Creature']).
card_subtypes('minion of the wastes', ['Minion']).
card_colors('minion of the wastes', ['B']).
card_text('minion of the wastes', 'Trample\nAs Minion of the Wastes enters the battlefield, pay any amount of life.\nMinion of the Wastes\'s power and toughness are each equal to the life paid as it entered the battlefield.').
card_mana_cost('minion of the wastes', ['3', 'B', 'B', 'B']).
card_cmc('minion of the wastes', 6).
card_layout('minion of the wastes', 'normal').
card_power('minion of the wastes', '*').
card_toughness('minion of the wastes', '*').

% Found in: ALA
card_name('minion reflector', 'Minion Reflector').
card_type('minion reflector', 'Artifact').
card_types('minion reflector', ['Artifact']).
card_subtypes('minion reflector', []).
card_colors('minion reflector', []).
card_text('minion reflector', 'Whenever a nontoken creature enters the battlefield under your control, you may pay {2}. If you do, put a token that\'s a copy of that creature onto the battlefield. That token has haste and \"At the beginning of the end step, sacrifice this permanent.\"').
card_mana_cost('minion reflector', ['5']).
card_cmc('minion reflector', 5).
card_layout('minion reflector', 'normal').

% Found in: FUT
card_name('minions\' murmurs', 'Minions\' Murmurs').
card_type('minions\' murmurs', 'Sorcery').
card_types('minions\' murmurs', ['Sorcery']).
card_subtypes('minions\' murmurs', []).
card_colors('minions\' murmurs', ['B']).
card_text('minions\' murmurs', 'You draw X cards and you lose X life, where X is the number of creatures you control.').
card_mana_cost('minions\' murmurs', ['2', 'B', 'B']).
card_cmc('minions\' murmurs', 4).
card_layout('minions\' murmurs', 'normal').

% Found in: DIS
card_name('minister of impediments', 'Minister of Impediments').
card_type('minister of impediments', 'Creature — Human Advisor').
card_types('minister of impediments', ['Creature']).
card_subtypes('minister of impediments', ['Human', 'Advisor']).
card_colors('minister of impediments', ['W', 'U']).
card_text('minister of impediments', '({W/U} can be paid with either {W} or {U}.)\n{T}: Tap target creature.').
card_mana_cost('minister of impediments', ['2', 'W/U']).
card_cmc('minister of impediments', 3).
card_layout('minister of impediments', 'normal').
card_power('minister of impediments', 1).
card_toughness('minister of impediments', 1).

% Found in: DTK
card_name('minister of pain', 'Minister of Pain').
card_type('minister of pain', 'Creature — Human Shaman').
card_types('minister of pain', ['Creature']).
card_subtypes('minister of pain', ['Human', 'Shaman']).
card_colors('minister of pain', ['B']).
card_text('minister of pain', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Minister of Pain exploits a creature, creatures your opponents control get -1/-1 until end of turn.').
card_mana_cost('minister of pain', ['2', 'B']).
card_cmc('minister of pain', 3).
card_layout('minister of pain', 'normal').
card_power('minister of pain', 2).
card_toughness('minister of pain', 3).

% Found in: M14
card_name('minotaur abomination', 'Minotaur Abomination').
card_type('minotaur abomination', 'Creature — Zombie Minotaur').
card_types('minotaur abomination', ['Creature']).
card_subtypes('minotaur abomination', ['Zombie', 'Minotaur']).
card_colors('minotaur abomination', ['B']).
card_text('minotaur abomination', '').
card_mana_cost('minotaur abomination', ['4', 'B', 'B']).
card_cmc('minotaur abomination', 6).
card_layout('minotaur abomination', 'normal').
card_power('minotaur abomination', 4).
card_toughness('minotaur abomination', 6).

% Found in: RTR
card_name('minotaur aggressor', 'Minotaur Aggressor').
card_type('minotaur aggressor', 'Creature — Minotaur Berserker').
card_types('minotaur aggressor', ['Creature']).
card_subtypes('minotaur aggressor', ['Minotaur', 'Berserker']).
card_colors('minotaur aggressor', ['R']).
card_text('minotaur aggressor', 'First strike, haste').
card_mana_cost('minotaur aggressor', ['6', 'R']).
card_cmc('minotaur aggressor', 7).
card_layout('minotaur aggressor', 'normal').
card_power('minotaur aggressor', 6).
card_toughness('minotaur aggressor', 2).

% Found in: ODY
card_name('minotaur explorer', 'Minotaur Explorer').
card_type('minotaur explorer', 'Creature — Minotaur Scout').
card_types('minotaur explorer', ['Creature']).
card_subtypes('minotaur explorer', ['Minotaur', 'Scout']).
card_colors('minotaur explorer', ['R']).
card_text('minotaur explorer', 'When Minotaur Explorer enters the battlefield, sacrifice it unless you discard a card at random.').
card_mana_cost('minotaur explorer', ['1', 'R']).
card_cmc('minotaur explorer', 2).
card_layout('minotaur explorer', 'normal').
card_power('minotaur explorer', 3).
card_toughness('minotaur explorer', 3).

% Found in: APC
card_name('minotaur illusionist', 'Minotaur Illusionist').
card_type('minotaur illusionist', 'Creature — Minotaur Wizard').
card_types('minotaur illusionist', ['Creature']).
card_subtypes('minotaur illusionist', ['Minotaur', 'Wizard']).
card_colors('minotaur illusionist', ['U', 'R']).
card_text('minotaur illusionist', '{1}{U}: Minotaur Illusionist gains shroud until end of turn. (It can\'t be the target of spells or abilities.)\n{R}, Sacrifice Minotaur Illusionist: Minotaur Illusionist deals damage equal to its power to target creature.').
card_mana_cost('minotaur illusionist', ['3', 'U', 'R']).
card_cmc('minotaur illusionist', 5).
card_layout('minotaur illusionist', 'normal').
card_power('minotaur illusionist', 3).
card_toughness('minotaur illusionist', 4).

% Found in: THS
card_name('minotaur skullcleaver', 'Minotaur Skullcleaver').
card_type('minotaur skullcleaver', 'Creature — Minotaur Berserker').
card_types('minotaur skullcleaver', ['Creature']).
card_subtypes('minotaur skullcleaver', ['Minotaur', 'Berserker']).
card_colors('minotaur skullcleaver', ['R']).
card_text('minotaur skullcleaver', 'Haste\nWhen Minotaur Skullcleaver enters the battlefield, it gets +2/+0 until end of turn.').
card_mana_cost('minotaur skullcleaver', ['2', 'R']).
card_cmc('minotaur skullcleaver', 3).
card_layout('minotaur skullcleaver', 'normal').
card_power('minotaur skullcleaver', 2).
card_toughness('minotaur skullcleaver', 2).

% Found in: APC
card_name('minotaur tactician', 'Minotaur Tactician').
card_type('minotaur tactician', 'Creature — Minotaur').
card_types('minotaur tactician', ['Creature']).
card_subtypes('minotaur tactician', ['Minotaur']).
card_colors('minotaur tactician', ['R']).
card_text('minotaur tactician', 'Haste\nMinotaur Tactician gets +1/+1 as long as you control a white creature.\nMinotaur Tactician gets +1/+1 as long as you control a blue creature.').
card_mana_cost('minotaur tactician', ['3', 'R']).
card_cmc('minotaur tactician', 4).
card_layout('minotaur tactician', 'normal').
card_power('minotaur tactician', 1).
card_toughness('minotaur tactician', 1).

% Found in: POR
card_name('minotaur warrior', 'Minotaur Warrior').
card_type('minotaur warrior', 'Creature — Minotaur Warrior').
card_types('minotaur warrior', ['Creature']).
card_subtypes('minotaur warrior', ['Minotaur', 'Warrior']).
card_colors('minotaur warrior', ['R']).
card_text('minotaur warrior', '').
card_mana_cost('minotaur warrior', ['2', 'R']).
card_cmc('minotaur warrior', 3).
card_layout('minotaur warrior', 'normal').
card_power('minotaur warrior', 2).
card_toughness('minotaur warrior', 3).

% Found in: DRK
card_name('miracle worker', 'Miracle Worker').
card_type('miracle worker', 'Creature — Human Cleric').
card_types('miracle worker', ['Creature']).
card_subtypes('miracle worker', ['Human', 'Cleric']).
card_colors('miracle worker', ['W']).
card_text('miracle worker', '{T}: Destroy target Aura attached to a creature you control.').
card_mana_cost('miracle worker', ['W']).
card_cmc('miracle worker', 1).
card_layout('miracle worker', 'normal').
card_power('miracle worker', 1).
card_toughness('miracle worker', 1).

% Found in: DDL, VIS
card_name('miraculous recovery', 'Miraculous Recovery').
card_type('miraculous recovery', 'Instant').
card_types('miraculous recovery', ['Instant']).
card_subtypes('miraculous recovery', []).
card_colors('miraculous recovery', ['W']).
card_text('miraculous recovery', 'Return target creature card from your graveyard to the battlefield. Put a +1/+1 counter on it.').
card_mana_cost('miraculous recovery', ['4', 'W']).
card_cmc('miraculous recovery', 5).
card_layout('miraculous recovery', 'normal').

% Found in: C13, ODY, TSB, V10
card_name('mirari', 'Mirari').
card_type('mirari', 'Legendary Artifact').
card_types('mirari', ['Artifact']).
card_subtypes('mirari', []).
card_supertypes('mirari', ['Legendary']).
card_colors('mirari', []).
card_text('mirari', 'Whenever you cast an instant or sorcery spell, you may pay {3}. If you do, copy that spell. You may choose new targets for the copy.').
card_mana_cost('mirari', ['5']).
card_cmc('mirari', 5).
card_layout('mirari', 'normal').

% Found in: CM1, CNS, JUD, pPRO
card_name('mirari\'s wake', 'Mirari\'s Wake').
card_type('mirari\'s wake', 'Enchantment').
card_types('mirari\'s wake', ['Enchantment']).
card_subtypes('mirari\'s wake', []).
card_colors('mirari\'s wake', ['W', 'G']).
card_text('mirari\'s wake', 'Creatures you control get +1/+1.\nWhenever you tap a land for mana, add one mana to your mana pool of any type that land produced.').
card_mana_cost('mirari\'s wake', ['3', 'G', 'W']).
card_cmc('mirari\'s wake', 5).
card_layout('mirari\'s wake', 'normal').

% Found in: ZEN
card_name('mire blight', 'Mire Blight').
card_type('mire blight', 'Enchantment — Aura').
card_types('mire blight', ['Enchantment']).
card_subtypes('mire blight', ['Aura']).
card_colors('mire blight', ['B']).
card_text('mire blight', 'Enchant creature\nWhen enchanted creature is dealt damage, destroy it.').
card_mana_cost('mire blight', ['B']).
card_cmc('mire blight', 1).
card_layout('mire blight', 'normal').

% Found in: PLC
card_name('mire boa', 'Mire Boa').
card_type('mire boa', 'Creature — Snake').
card_types('mire boa', ['Creature']).
card_subtypes('mire boa', ['Snake']).
card_colors('mire boa', ['G']).
card_text('mire boa', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\n{G}: Regenerate Mire Boa.').
card_mana_cost('mire boa', ['1', 'G']).
card_cmc('mire boa', 2).
card_layout('mire boa', 'normal').
card_power('mire boa', 2).
card_toughness('mire boa', 1).

% Found in: PLS
card_name('mire kavu', 'Mire Kavu').
card_type('mire kavu', 'Creature — Kavu').
card_types('mire kavu', ['Creature']).
card_subtypes('mire kavu', ['Kavu']).
card_colors('mire kavu', ['R']).
card_text('mire kavu', 'Mire Kavu gets +1/+1 as long as you control a Swamp.').
card_mana_cost('mire kavu', ['3', 'R']).
card_cmc('mire kavu', 4).
card_layout('mire kavu', 'normal').
card_power('mire kavu', 3).
card_toughness('mire kavu', 2).

% Found in: MIR
card_name('mire shade', 'Mire Shade').
card_type('mire shade', 'Creature — Shade').
card_types('mire shade', ['Creature']).
card_subtypes('mire shade', ['Shade']).
card_colors('mire shade', ['B']).
card_text('mire shade', '{B}, Sacrifice a Swamp: Put a +1/+1 counter on Mire Shade. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('mire shade', ['1', 'B']).
card_cmc('mire shade', 2).
card_layout('mire shade', 'normal').
card_power('mire shade', 1).
card_toughness('mire shade', 1).

% Found in: BFZ
card_name('mire\'s malice', 'Mire\'s Malice').
card_type('mire\'s malice', 'Sorcery').
card_types('mire\'s malice', ['Sorcery']).
card_subtypes('mire\'s malice', []).
card_colors('mire\'s malice', ['B']).
card_text('mire\'s malice', 'Target opponent discards two cards.\nAwaken 3—{5}{B} (If you cast this spell for {5}{B}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('mire\'s malice', ['3', 'B']).
card_cmc('mire\'s malice', 4).
card_layout('mire\'s malice', 'normal').

% Found in: WWK
card_name('mire\'s toll', 'Mire\'s Toll').
card_type('mire\'s toll', 'Sorcery').
card_types('mire\'s toll', ['Sorcery']).
card_subtypes('mire\'s toll', []).
card_colors('mire\'s toll', ['B']).
card_text('mire\'s toll', 'Target player reveals a number of cards from his or her hand equal to the number of Swamps you control. You choose one of them. That player discards that card.').
card_mana_cost('mire\'s toll', ['B']).
card_cmc('mire\'s toll', 1).
card_layout('mire\'s toll', 'normal').

% Found in: SOK
card_name('miren, the moaning well', 'Miren, the Moaning Well').
card_type('miren, the moaning well', 'Legendary Land').
card_types('miren, the moaning well', ['Land']).
card_subtypes('miren, the moaning well', []).
card_supertypes('miren, the moaning well', ['Legendary']).
card_colors('miren, the moaning well', []).
card_text('miren, the moaning well', '{T}: Add {1} to your mana pool.\n{3}, {T}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.').
card_layout('miren, the moaning well', 'normal').

% Found in: DGM
card_name('mirko vosk, mind drinker', 'Mirko Vosk, Mind Drinker').
card_type('mirko vosk, mind drinker', 'Legendary Creature — Vampire').
card_types('mirko vosk, mind drinker', ['Creature']).
card_subtypes('mirko vosk, mind drinker', ['Vampire']).
card_supertypes('mirko vosk, mind drinker', ['Legendary']).
card_colors('mirko vosk, mind drinker', ['U', 'B']).
card_text('mirko vosk, mind drinker', 'Flying\nWhenever Mirko Vosk, Mind Drinker deals combat damage to a player, that player reveals cards from the top of his or her library until he or she reveals four land cards, then puts those cards into his or her graveyard.').
card_mana_cost('mirko vosk, mind drinker', ['3', 'U', 'B']).
card_cmc('mirko vosk, mind drinker', 5).
card_layout('mirko vosk, mind drinker', 'normal').
card_power('mirko vosk, mind drinker', 2).
card_toughness('mirko vosk, mind drinker', 4).

% Found in: EXO
card_name('mirozel', 'Mirozel').
card_type('mirozel', 'Creature — Illusion').
card_types('mirozel', ['Creature']).
card_subtypes('mirozel', ['Illusion']).
card_colors('mirozel', ['U']).
card_text('mirozel', 'Flying\nWhen Mirozel becomes the target of a spell or ability, return Mirozel to its owner\'s hand.').
card_mana_cost('mirozel', ['3', 'U']).
card_cmc('mirozel', 4).
card_layout('mirozel', 'normal').
card_power('mirozel', 2).
card_toughness('mirozel', 3).

% Found in: MBS, MM2, pMEI
card_name('mirran crusader', 'Mirran Crusader').
card_type('mirran crusader', 'Creature — Human Knight').
card_types('mirran crusader', ['Creature']).
card_subtypes('mirran crusader', ['Human', 'Knight']).
card_colors('mirran crusader', ['W']).
card_text('mirran crusader', 'Double strike, protection from black and from green').
card_mana_cost('mirran crusader', ['1', 'W', 'W']).
card_cmc('mirran crusader', 3).
card_layout('mirran crusader', 'normal').
card_power('mirran crusader', 2).
card_toughness('mirran crusader', 2).

% Found in: MBS
card_name('mirran mettle', 'Mirran Mettle').
card_type('mirran mettle', 'Instant').
card_types('mirran mettle', ['Instant']).
card_subtypes('mirran mettle', []).
card_colors('mirran mettle', ['G']).
card_text('mirran mettle', 'Target creature gets +2/+2 until end of turn.\nMetalcraft — That creature gets +4/+4 until end of turn instead if you control three or more artifacts.').
card_mana_cost('mirran mettle', ['G']).
card_cmc('mirran mettle', 1).
card_layout('mirran mettle', 'normal').

% Found in: MBS
card_name('mirran spy', 'Mirran Spy').
card_type('mirran spy', 'Creature — Drone').
card_types('mirran spy', ['Creature']).
card_subtypes('mirran spy', ['Drone']).
card_colors('mirran spy', ['U']).
card_text('mirran spy', 'Flying\nWhenever you cast an artifact spell, you may untap target creature.').
card_mana_cost('mirran spy', ['2', 'U']).
card_cmc('mirran spy', 3).
card_layout('mirran spy', 'normal').
card_power('mirran spy', 1).
card_toughness('mirran spy', 3).

% Found in: VAN
card_name('mirri', 'Mirri').
card_type('mirri', 'Vanguard').
card_types('mirri', ['Vanguard']).
card_subtypes('mirri', []).
card_colors('mirri', []).
card_text('mirri', 'If a basic land you control is tapped for mana, it produces mana of a color of your choice instead of any other type.').
card_layout('mirri', 'vanguard').

% Found in: PLC
card_name('mirri the cursed', 'Mirri the Cursed').
card_type('mirri the cursed', 'Legendary Creature — Vampire Cat').
card_types('mirri the cursed', ['Creature']).
card_subtypes('mirri the cursed', ['Vampire', 'Cat']).
card_supertypes('mirri the cursed', ['Legendary']).
card_colors('mirri the cursed', ['B']).
card_text('mirri the cursed', 'Flying, first strike, haste\nWhenever Mirri the Cursed deals combat damage to a creature, put a +1/+1 counter on Mirri the Cursed.').
card_mana_cost('mirri the cursed', ['2', 'B', 'B']).
card_cmc('mirri the cursed', 4).
card_layout('mirri the cursed', 'normal').
card_power('mirri the cursed', 3).
card_toughness('mirri the cursed', 2).

% Found in: VAN
card_name('mirri the cursed avatar', 'Mirri the Cursed Avatar').
card_type('mirri the cursed avatar', 'Vanguard').
card_types('mirri the cursed avatar', ['Vanguard']).
card_subtypes('mirri the cursed avatar', []).
card_colors('mirri the cursed avatar', []).
card_text('mirri the cursed avatar', 'Creatures you control have \"{T}: Another target creature gets -1/-1 until end of turn. Put a +1/+1 counter on this creature.\"').
card_layout('mirri the cursed avatar', 'vanguard').

% Found in: TMP
card_name('mirri\'s guile', 'Mirri\'s Guile').
card_type('mirri\'s guile', 'Enchantment').
card_types('mirri\'s guile', ['Enchantment']).
card_subtypes('mirri\'s guile', []).
card_colors('mirri\'s guile', ['G']).
card_text('mirri\'s guile', 'At the beginning of your upkeep, you may look at the top three cards of your library, then put them back in any order.').
card_mana_cost('mirri\'s guile', ['G']).
card_cmc('mirri\'s guile', 1).
card_layout('mirri\'s guile', 'normal').

% Found in: 10E, ATH, EXO, TPR
card_name('mirri, cat warrior', 'Mirri, Cat Warrior').
card_type('mirri, cat warrior', 'Legendary Creature — Cat Warrior').
card_types('mirri, cat warrior', ['Creature']).
card_subtypes('mirri, cat warrior', ['Cat', 'Warrior']).
card_supertypes('mirri, cat warrior', ['Legendary']).
card_colors('mirri, cat warrior', ['G']).
card_text('mirri, cat warrior', 'First strike, forestwalk, vigilance (This creature deals combat damage before creatures without first strike, it can\'t be blocked as long as defending player controls a Forest, and attacking doesn\'t cause this creature to tap.)').
card_mana_cost('mirri, cat warrior', ['1', 'G', 'G']).
card_cmc('mirri, cat warrior', 3).
card_layout('mirri, cat warrior', 'normal').
card_power('mirri, cat warrior', 2).
card_toughness('mirri, cat warrior', 3).

% Found in: CNS, DST
card_name('mirrodin\'s core', 'Mirrodin\'s Core').
card_type('mirrodin\'s core', 'Land').
card_types('mirrodin\'s core', ['Land']).
card_subtypes('mirrodin\'s core', []).
card_colors('mirrodin\'s core', []).
card_text('mirrodin\'s core', '{T}: Add {1} to your mana pool.\n{T}: Put a charge counter on Mirrodin\'s Core.\n{T}, Remove a charge counter from Mirrodin\'s Core: Add one mana of any color to your mana pool.').
card_layout('mirrodin\'s core', 'normal').

% Found in: C13, LRW, MM2
card_name('mirror entity', 'Mirror Entity').
card_type('mirror entity', 'Creature — Shapeshifter').
card_types('mirror entity', ['Creature']).
card_subtypes('mirror entity', ['Shapeshifter']).
card_colors('mirror entity', ['W']).
card_text('mirror entity', 'Changeling (This card is every creature type.)\n{X}: Until end of turn, creatures you control have base power and toughness X/X and gain all creature types.').
card_mana_cost('mirror entity', ['2', 'W']).
card_cmc('mirror entity', 3).
card_layout('mirror entity', 'normal').
card_power('mirror entity', 1).
card_toughness('mirror entity', 1).

% Found in: VAN
card_name('mirror entity avatar', 'Mirror Entity Avatar').
card_type('mirror entity avatar', 'Vanguard').
card_types('mirror entity avatar', ['Vanguard']).
card_subtypes('mirror entity avatar', []).
card_colors('mirror entity avatar', []).
card_text('mirror entity avatar', '{X}: Choose a creature type. Until end of turn, creatures you control of the chosen type have base power and toughness X/X and gain all creature types.').
card_layout('mirror entity avatar', 'vanguard').

% Found in: BOK
card_name('mirror gallery', 'Mirror Gallery').
card_type('mirror gallery', 'Artifact').
card_types('mirror gallery', ['Artifact']).
card_subtypes('mirror gallery', []).
card_colors('mirror gallery', []).
card_text('mirror gallery', 'The \"legend rule\" doesn\'t apply.').
card_mana_cost('mirror gallery', ['5']).
card_cmc('mirror gallery', 5).
card_layout('mirror gallery', 'normal').

% Found in: MRD
card_name('mirror golem', 'Mirror Golem').
card_type('mirror golem', 'Artifact Creature — Golem').
card_types('mirror golem', ['Artifact', 'Creature']).
card_subtypes('mirror golem', ['Golem']).
card_colors('mirror golem', []).
card_text('mirror golem', 'Imprint — When Mirror Golem enters the battlefield, you may exile target card from a graveyard.\nMirror Golem has protection from each of the exiled card\'s card types. (Artifact, creature, enchantment, instant, land, planeswalker, sorcery, and tribal are card types.)').
card_mana_cost('mirror golem', ['6']).
card_cmc('mirror golem', 6).
card_layout('mirror golem', 'normal').
card_power('mirror golem', 3).
card_toughness('mirror golem', 4).

% Found in: UGL
card_name('mirror mirror', 'Mirror Mirror').
card_type('mirror mirror', 'Artifact').
card_types('mirror mirror', ['Artifact']).
card_subtypes('mirror mirror', []).
card_colors('mirror mirror', []).
card_text('mirror mirror', 'Mirror Mirror comes into play tapped.\n{7}, {T}, Sacrifice Mirror Mirror: At end of turn, exchange life totals with target player and exchange all cards in play that you control, and all cards in your hand, library, and graveyard, with that player until end of game.').
card_mana_cost('mirror mirror', ['7']).
card_cmc('mirror mirror', 7).
card_layout('mirror mirror', 'normal').

% Found in: DTK
card_name('mirror mockery', 'Mirror Mockery').
card_type('mirror mockery', 'Enchantment — Aura').
card_types('mirror mockery', ['Enchantment']).
card_subtypes('mirror mockery', ['Aura']).
card_colors('mirror mockery', ['U']).
card_text('mirror mockery', 'Enchant creature\nWhenever enchanted creature attacks, you may put a token onto the battlefield that\'s a copy of that creature. Exile that token at end of combat.').
card_mana_cost('mirror mockery', ['1', 'U']).
card_cmc('mirror mockery', 2).
card_layout('mirror mockery', 'normal').

% Found in: M10
card_name('mirror of fate', 'Mirror of Fate').
card_type('mirror of fate', 'Artifact').
card_types('mirror of fate', ['Artifact']).
card_subtypes('mirror of fate', []).
card_colors('mirror of fate', []).
card_text('mirror of fate', '{T}, Sacrifice Mirror of Fate: Choose up to seven face-up exiled cards you own. Exile all the cards from your library, then put the chosen cards on top of your library.').
card_mana_cost('mirror of fate', ['5']).
card_cmc('mirror of fate', 5).
card_layout('mirror of fate', 'normal').

% Found in: EVE
card_name('mirror sheen', 'Mirror Sheen').
card_type('mirror sheen', 'Enchantment').
card_types('mirror sheen', ['Enchantment']).
card_subtypes('mirror sheen', []).
card_colors('mirror sheen', ['U', 'R']).
card_text('mirror sheen', '{1}{U/R}{U/R}: Copy target instant or sorcery spell that targets you. You may choose new targets for the copy.').
card_mana_cost('mirror sheen', ['1', 'U/R', 'U/R']).
card_cmc('mirror sheen', 3).
card_layout('mirror sheen', 'normal').

% Found in: PCY
card_name('mirror strike', 'Mirror Strike').
card_type('mirror strike', 'Instant').
card_types('mirror strike', ['Instant']).
card_subtypes('mirror strike', []).
card_colors('mirror strike', ['W']).
card_text('mirror strike', 'All combat damage that would be dealt to you this turn by target unblocked creature is dealt to its controller instead.').
card_mana_cost('mirror strike', ['3', 'W']).
card_cmc('mirror strike', 4).
card_layout('mirror strike', 'normal').

% Found in: LEG, MED
card_name('mirror universe', 'Mirror Universe').
card_type('mirror universe', 'Artifact').
card_types('mirror universe', ['Artifact']).
card_subtypes('mirror universe', []).
card_colors('mirror universe', []).
card_text('mirror universe', '{T}, Sacrifice Mirror Universe: Exchange life totals with target opponent. Activate this ability only during your upkeep.').
card_mana_cost('mirror universe', ['6']).
card_cmc('mirror universe', 6).
card_layout('mirror universe', 'normal').
card_reserved('mirror universe').

% Found in: JUD
card_name('mirror wall', 'Mirror Wall').
card_type('mirror wall', 'Creature — Wall').
card_types('mirror wall', ['Creature']).
card_subtypes('mirror wall', ['Wall']).
card_colors('mirror wall', ['U']).
card_text('mirror wall', 'Defender (This creature can\'t attack.)\n{W}: Mirror Wall can attack this turn as though it didn\'t have defender.').
card_mana_cost('mirror wall', ['3', 'U']).
card_cmc('mirror wall', 4).
card_layout('mirror wall', 'normal').
card_power('mirror wall', 3).
card_toughness('mirror wall', 4).

% Found in: ISD
card_name('mirror-mad phantasm', 'Mirror-Mad Phantasm').
card_type('mirror-mad phantasm', 'Creature — Spirit').
card_types('mirror-mad phantasm', ['Creature']).
card_subtypes('mirror-mad phantasm', ['Spirit']).
card_colors('mirror-mad phantasm', ['U']).
card_text('mirror-mad phantasm', 'Flying\n{1}{U}: Mirror-Mad Phantasm\'s owner shuffles it into his or her library. If that player does, he or she reveals cards from the top of that library until a card named Mirror-Mad Phantasm is revealed. The player puts that card onto the battlefield and all other cards revealed this way into his or her graveyard.').
card_mana_cost('mirror-mad phantasm', ['3', 'U', 'U']).
card_cmc('mirror-mad phantasm', 5).
card_layout('mirror-mad phantasm', 'normal').
card_power('mirror-mad phantasm', 5).
card_toughness('mirror-mad phantasm', 1).

% Found in: CON
card_name('mirror-sigil sergeant', 'Mirror-Sigil Sergeant').
card_type('mirror-sigil sergeant', 'Creature — Rhino Soldier').
card_types('mirror-sigil sergeant', ['Creature']).
card_subtypes('mirror-sigil sergeant', ['Rhino', 'Soldier']).
card_colors('mirror-sigil sergeant', ['W']).
card_text('mirror-sigil sergeant', 'Trample\nAt the beginning of your upkeep, if you control a blue permanent, you may put a token that\'s a copy of Mirror-Sigil Sergeant onto the battlefield.').
card_mana_cost('mirror-sigil sergeant', ['5', 'W']).
card_cmc('mirror-sigil sergeant', 6).
card_layout('mirror-sigil sergeant', 'normal').
card_power('mirror-sigil sergeant', 4).
card_toughness('mirror-sigil sergeant', 4).

% Found in: SHM
card_name('mirrorweave', 'Mirrorweave').
card_type('mirrorweave', 'Instant').
card_types('mirrorweave', ['Instant']).
card_subtypes('mirrorweave', []).
card_colors('mirrorweave', ['W', 'U']).
card_text('mirrorweave', 'Each other creature becomes a copy of target nonlegendary creature until end of turn.').
card_mana_cost('mirrorweave', ['2', 'W/U', 'W/U']).
card_cmc('mirrorweave', 4).
card_layout('mirrorweave', 'normal').

% Found in: PLS
card_name('mirrorwood treefolk', 'Mirrorwood Treefolk').
card_type('mirrorwood treefolk', 'Creature — Treefolk').
card_types('mirrorwood treefolk', ['Creature']).
card_subtypes('mirrorwood treefolk', ['Treefolk']).
card_colors('mirrorwood treefolk', ['G']).
card_text('mirrorwood treefolk', '{2}{R}{W}: The next time damage would be dealt to Mirrorwood Treefolk this turn, that damage is dealt to target creature or player instead.').
card_mana_cost('mirrorwood treefolk', ['3', 'G']).
card_cmc('mirrorwood treefolk', 4).
card_layout('mirrorwood treefolk', 'normal').
card_power('mirrorwood treefolk', 2).
card_toughness('mirrorwood treefolk', 4).

% Found in: MBS
card_name('mirrorworks', 'Mirrorworks').
card_type('mirrorworks', 'Artifact').
card_types('mirrorworks', ['Artifact']).
card_subtypes('mirrorworks', []).
card_colors('mirrorworks', []).
card_text('mirrorworks', 'Whenever another nontoken artifact enters the battlefield under your control, you may pay {2}. If you do, put a token that\'s a copy of that artifact onto the battlefield.').
card_mana_cost('mirrorworks', ['5']).
card_cmc('mirrorworks', 5).
card_layout('mirrorworks', 'normal').

% Found in: ULG
card_name('miscalculation', 'Miscalculation').
card_type('miscalculation', 'Instant').
card_types('miscalculation', ['Instant']).
card_subtypes('miscalculation', []).
card_colors('miscalculation', ['U']).
card_text('miscalculation', 'Counter target spell unless its controller pays {2}.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('miscalculation', ['1', 'U']).
card_cmc('miscalculation', 2).
card_layout('miscalculation', 'normal').

% Found in: BNG
card_name('mischief and mayhem', 'Mischief and Mayhem').
card_type('mischief and mayhem', 'Sorcery').
card_types('mischief and mayhem', ['Sorcery']).
card_subtypes('mischief and mayhem', []).
card_colors('mischief and mayhem', ['G']).
card_text('mischief and mayhem', 'Up to two target creatures each get +4/+4 until end of turn.').
card_mana_cost('mischief and mayhem', ['4', 'G']).
card_cmc('mischief and mayhem', 5).
card_layout('mischief and mayhem', 'normal').

% Found in: 6ED, WTH
card_name('mischievous poltergeist', 'Mischievous Poltergeist').
card_type('mischievous poltergeist', 'Creature — Spirit').
card_types('mischievous poltergeist', ['Creature']).
card_subtypes('mischievous poltergeist', ['Spirit']).
card_colors('mischievous poltergeist', ['B']).
card_text('mischievous poltergeist', 'Flying\nPay 1 life: Regenerate Mischievous Poltergeist.').
card_mana_cost('mischievous poltergeist', ['2', 'B']).
card_cmc('mischievous poltergeist', 3).
card_layout('mischievous poltergeist', 'normal').
card_power('mischievous poltergeist', 1).
card_toughness('mischievous poltergeist', 1).

% Found in: SCG
card_name('mischievous quanar', 'Mischievous Quanar').
card_type('mischievous quanar', 'Creature — Beast').
card_types('mischievous quanar', ['Creature']).
card_subtypes('mischievous quanar', ['Beast']).
card_colors('mischievous quanar', ['U']).
card_text('mischievous quanar', '{3}{U}{U}: Turn Mischievous Quanar face down.\nMorph {1}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Mischievous Quanar is turned face up, copy target instant or sorcery spell. You may choose new targets for that copy.').
card_mana_cost('mischievous quanar', ['4', 'U']).
card_cmc('mischievous quanar', 5).
card_layout('mischievous quanar', 'normal').
card_power('mischievous quanar', 3).
card_toughness('mischievous quanar', 3).

% Found in: CNS, MMQ
card_name('misdirection', 'Misdirection').
card_type('misdirection', 'Instant').
card_types('misdirection', ['Instant']).
card_subtypes('misdirection', []).
card_colors('misdirection', ['U']).
card_text('misdirection', 'You may exile a blue card from your hand rather than pay Misdirection\'s mana cost.\nChange the target of target spell with a single target.').
card_mana_cost('misdirection', ['3', 'U', 'U']).
card_cmc('misdirection', 5).
card_layout('misdirection', 'normal').

% Found in: UNH, pARL
card_name('mise', 'Mise').
card_type('mise', 'Instant').
card_types('mise', ['Instant']).
card_subtypes('mise', []).
card_colors('mise', ['U']).
card_text('mise', 'Name a nonland card, then reveal the top card of your library. If that card is the named card, draw three cards.').
card_mana_cost('mise', ['U']).
card_cmc('mise', 1).
card_layout('mise', 'normal').

% Found in: MIR
card_name('misers\' cage', 'Misers\' Cage').
card_type('misers\' cage', 'Artifact').
card_types('misers\' cage', ['Artifact']).
card_subtypes('misers\' cage', []).
card_colors('misers\' cage', []).
card_text('misers\' cage', 'At the beginning of each opponent\'s upkeep, if that player has five or more cards in hand, Misers\' Cage deals 2 damage to him or her.').
card_mana_cost('misers\' cage', ['3']).
card_cmc('misers\' cage', 3).
card_layout('misers\' cage', 'normal').
card_reserved('misers\' cage').

% Found in: ONS
card_name('misery charm', 'Misery Charm').
card_type('misery charm', 'Instant').
card_types('misery charm', ['Instant']).
card_subtypes('misery charm', []).
card_colors('misery charm', ['B']).
card_text('misery charm', 'Choose one —\n• Destroy target Cleric.\n• Return target Cleric card from your graveyard to your hand.\n• Target player loses 2 life.').
card_mana_cost('misery charm', ['B']).
card_cmc('misery charm', 1).
card_layout('misery charm', 'normal').

% Found in: ALL
card_name('misfortune', 'Misfortune').
card_type('misfortune', 'Sorcery').
card_types('misfortune', ['Sorcery']).
card_subtypes('misfortune', []).
card_colors('misfortune', ['B', 'R', 'G']).
card_text('misfortune', 'An opponent chooses one —\n• You put a +1/+1 counter on each creature you control and gain 4 life.\n• You put a -1/-1 counter on each creature that player controls and Misfortune deals 4 damage to him or her.').
card_mana_cost('misfortune', ['1', 'B', 'R', 'G']).
card_cmc('misfortune', 4).
card_layout('misfortune', 'normal').
card_reserved('misfortune').

% Found in: ME3, PTK
card_name('misfortune\'s gain', 'Misfortune\'s Gain').
card_type('misfortune\'s gain', 'Sorcery').
card_types('misfortune\'s gain', ['Sorcery']).
card_subtypes('misfortune\'s gain', []).
card_colors('misfortune\'s gain', ['W']).
card_text('misfortune\'s gain', 'Destroy target creature. Its owner gains 4 life.').
card_mana_cost('misfortune\'s gain', ['3', 'W']).
card_cmc('misfortune\'s gain', 4).
card_layout('misfortune\'s gain', 'normal').

% Found in: SCG
card_name('misguided rage', 'Misguided Rage').
card_type('misguided rage', 'Sorcery').
card_types('misguided rage', ['Sorcery']).
card_subtypes('misguided rage', []).
card_colors('misguided rage', ['R']).
card_text('misguided rage', 'Target player sacrifices a permanent.').
card_mana_cost('misguided rage', ['2', 'R']).
card_cmc('misguided rage', 3).
card_layout('misguided rage', 'normal').

% Found in: VAN
card_name('mishra', 'Mishra').
card_type('mishra', 'Vanguard').
card_types('mishra', ['Vanguard']).
card_subtypes('mishra', []).
card_colors('mishra', []).
card_text('mishra', 'If a creature you control would deal damage, it deals double that damage instead.').
card_layout('mishra', 'vanguard').

% Found in: CSP
card_name('mishra\'s bauble', 'Mishra\'s Bauble').
card_type('mishra\'s bauble', 'Artifact').
card_types('mishra\'s bauble', ['Artifact']).
card_subtypes('mishra\'s bauble', []).
card_colors('mishra\'s bauble', []).
card_text('mishra\'s bauble', '{T}, Sacrifice Mishra\'s Bauble: Look at the top card of target player\'s library. Draw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('mishra\'s bauble', ['0']).
card_cmc('mishra\'s bauble', 0).
card_layout('mishra\'s bauble', 'normal').

% Found in: 4ED, ATQ, DDF, MED, pJGP
card_name('mishra\'s factory', 'Mishra\'s Factory').
card_type('mishra\'s factory', 'Land').
card_types('mishra\'s factory', ['Land']).
card_subtypes('mishra\'s factory', []).
card_colors('mishra\'s factory', []).
card_text('mishra\'s factory', '{T}: Add {1} to your mana pool.\n{1}: Mishra\'s Factory becomes a 2/2 Assembly-Worker artifact creature until end of turn. It\'s still a land.\n{T}: Target Assembly-Worker creature gets +1/+1 until end of turn.').
card_layout('mishra\'s factory', 'normal').

% Found in: ALL, ME2
card_name('mishra\'s groundbreaker', 'Mishra\'s Groundbreaker').
card_type('mishra\'s groundbreaker', 'Artifact').
card_types('mishra\'s groundbreaker', ['Artifact']).
card_subtypes('mishra\'s groundbreaker', []).
card_colors('mishra\'s groundbreaker', []).
card_text('mishra\'s groundbreaker', '{T}, Sacrifice Mishra\'s Groundbreaker: Target land becomes a 3/3 artifact creature that\'s still a land. (This effect lasts indefinitely.)').
card_mana_cost('mishra\'s groundbreaker', ['4']).
card_cmc('mishra\'s groundbreaker', 4).
card_layout('mishra\'s groundbreaker', 'normal').

% Found in: USG
card_name('mishra\'s helix', 'Mishra\'s Helix').
card_type('mishra\'s helix', 'Artifact').
card_types('mishra\'s helix', ['Artifact']).
card_subtypes('mishra\'s helix', []).
card_colors('mishra\'s helix', []).
card_text('mishra\'s helix', '{X}, {T}: Tap X target lands.').
card_mana_cost('mishra\'s helix', ['5']).
card_cmc('mishra\'s helix', 5).
card_layout('mishra\'s helix', 'normal').

% Found in: pHHO
card_name('mishra\'s toy workshop', 'Mishra\'s Toy Workshop').
card_type('mishra\'s toy workshop', 'Land').
card_types('mishra\'s toy workshop', ['Land']).
card_subtypes('mishra\'s toy workshop', []).
card_colors('mishra\'s toy workshop', []).
card_text('mishra\'s toy workshop', '{T}: Add {3} to your mana pool. Spend this mana only on spells and abilities that put tokens onto the battlefield. Use toys to represent the tokens.').
card_layout('mishra\'s toy workshop', 'normal').

% Found in: 3ED, 4ED, ATQ
card_name('mishra\'s war machine', 'Mishra\'s War Machine').
card_type('mishra\'s war machine', 'Artifact Creature — Juggernaut').
card_types('mishra\'s war machine', ['Artifact', 'Creature']).
card_subtypes('mishra\'s war machine', ['Juggernaut']).
card_colors('mishra\'s war machine', []).
card_text('mishra\'s war machine', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\nAt the beginning of your upkeep, Mishra\'s War Machine deals 3 damage to you unless you discard a card. If Mishra\'s War Machine deals damage to you this way, tap it.').
card_mana_cost('mishra\'s war machine', ['7']).
card_cmc('mishra\'s war machine', 7).
card_layout('mishra\'s war machine', 'normal').
card_power('mishra\'s war machine', 5).
card_toughness('mishra\'s war machine', 5).

% Found in: ATQ, ME4, VMA
card_name('mishra\'s workshop', 'Mishra\'s Workshop').
card_type('mishra\'s workshop', 'Land').
card_types('mishra\'s workshop', ['Land']).
card_subtypes('mishra\'s workshop', []).
card_colors('mishra\'s workshop', []).
card_text('mishra\'s workshop', '{T}: Add {3} to your mana pool. Spend this mana only to cast artifact spells.').
card_layout('mishra\'s workshop', 'normal').
card_reserved('mishra\'s workshop').

% Found in: TSP
card_name('mishra, artificer prodigy', 'Mishra, Artificer Prodigy').
card_type('mishra, artificer prodigy', 'Legendary Creature — Human Artificer').
card_types('mishra, artificer prodigy', ['Creature']).
card_subtypes('mishra, artificer prodigy', ['Human', 'Artificer']).
card_supertypes('mishra, artificer prodigy', ['Legendary']).
card_colors('mishra, artificer prodigy', ['U', 'B', 'R']).
card_text('mishra, artificer prodigy', 'Whenever you cast an artifact spell, you may search your graveyard, hand, and/or library for a card with the same name as that spell and put it onto the battlefield. If you search your library this way, shuffle it.').
card_mana_cost('mishra, artificer prodigy', ['1', 'U', 'B', 'R']).
card_cmc('mishra, artificer prodigy', 4).
card_layout('mishra, artificer prodigy', 'normal').
card_power('mishra, artificer prodigy', 4).
card_toughness('mishra, artificer prodigy', 4).

% Found in: ALL, ME2
card_name('misinformation', 'Misinformation').
card_type('misinformation', 'Instant').
card_types('misinformation', ['Instant']).
card_subtypes('misinformation', []).
card_colors('misinformation', ['B']).
card_text('misinformation', 'Put up to three target cards from an opponent\'s graveyard on top of his or her library in any order.').
card_mana_cost('misinformation', ['B']).
card_cmc('misinformation', 1).
card_layout('misinformation', 'normal').

% Found in: UGL
card_name('miss demeanor', 'Miss Demeanor').
card_type('miss demeanor', 'Creature — Lady of Proper Etiquette').
card_types('miss demeanor', ['Creature']).
card_subtypes('miss demeanor', ['Lady', 'of', 'Proper', 'Etiquette']).
card_colors('miss demeanor', ['W']).
card_text('miss demeanor', 'Flying, first strike\nDuring each other player\'s turn, compliment that player on his or her game play or sacrifice Miss Demeanor.').
card_mana_cost('miss demeanor', ['3', 'W']).
card_cmc('miss demeanor', 4).
card_layout('miss demeanor', 'normal').
card_power('miss demeanor', 3).
card_toughness('miss demeanor', 1).

% Found in: MMQ
card_name('misshapen fiend', 'Misshapen Fiend').
card_type('misshapen fiend', 'Creature — Horror Mercenary').
card_types('misshapen fiend', ['Creature']).
card_subtypes('misshapen fiend', ['Horror', 'Mercenary']).
card_colors('misshapen fiend', ['B']).
card_text('misshapen fiend', 'Flying').
card_mana_cost('misshapen fiend', ['1', 'B']).
card_cmc('misshapen fiend', 2).
card_layout('misshapen fiend', 'normal').
card_power('misshapen fiend', 1).
card_toughness('misshapen fiend', 1).

% Found in: MMQ
card_name('misstep', 'Misstep').
card_type('misstep', 'Sorcery').
card_types('misstep', ['Sorcery']).
card_subtypes('misstep', []).
card_colors('misstep', ['U']).
card_text('misstep', 'Creatures target player controls don\'t untap during that player\'s next untap step.').
card_mana_cost('misstep', ['1', 'U']).
card_cmc('misstep', 2).
card_layout('misstep', 'normal').

% Found in: MIR
card_name('mist dragon', 'Mist Dragon').
card_type('mist dragon', 'Creature — Dragon').
card_types('mist dragon', ['Creature']).
card_subtypes('mist dragon', ['Dragon']).
card_colors('mist dragon', ['U']).
card_text('mist dragon', '{0}: Mist Dragon gains flying. (This effect lasts indefinitely.)\n{0}: Mist Dragon loses flying. (This effect lasts indefinitely.)\n{3}{U}{U}: Mist Dragon phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before you untap during your next untap step.)').
card_mana_cost('mist dragon', ['4', 'U', 'U']).
card_cmc('mist dragon', 6).
card_layout('mist dragon', 'normal').
card_power('mist dragon', 4).
card_toughness('mist dragon', 4).
card_reserved('mist dragon').

% Found in: BFZ
card_name('mist intruder', 'Mist Intruder').
card_type('mist intruder', 'Creature — Eldrazi Drone').
card_types('mist intruder', ['Creature']).
card_subtypes('mist intruder', ['Eldrazi', 'Drone']).
card_colors('mist intruder', []).
card_text('mist intruder', 'Devoid (This card has no color.)\nFlying\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)').
card_mana_cost('mist intruder', ['1', 'U']).
card_cmc('mist intruder', 2).
card_layout('mist intruder', 'normal').
card_power('mist intruder', 1).
card_toughness('mist intruder', 2).

% Found in: M10
card_name('mist leopard', 'Mist Leopard').
card_type('mist leopard', 'Creature — Cat').
card_types('mist leopard', ['Creature']).
card_subtypes('mist leopard', ['Cat']).
card_colors('mist leopard', ['G']).
card_text('mist leopard', 'Shroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('mist leopard', ['3', 'G']).
card_cmc('mist leopard', 4).
card_layout('mist leopard', 'normal').
card_power('mist leopard', 3).
card_toughness('mist leopard', 2).

% Found in: JUD
card_name('mist of stagnation', 'Mist of Stagnation').
card_type('mist of stagnation', 'Enchantment').
card_types('mist of stagnation', ['Enchantment']).
card_subtypes('mist of stagnation', []).
card_colors('mist of stagnation', ['U']).
card_text('mist of stagnation', 'Permanents don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player chooses a permanent for each card in his or her graveyard, then untaps those permanents.').
card_mana_cost('mist of stagnation', ['3', 'U', 'U']).
card_cmc('mist of stagnation', 5).
card_layout('mist of stagnation', 'normal').

% Found in: AVR
card_name('mist raven', 'Mist Raven').
card_type('mist raven', 'Creature — Bird').
card_types('mist raven', ['Creature']).
card_subtypes('mist raven', ['Bird']).
card_colors('mist raven', ['U']).
card_text('mist raven', 'Flying\nWhen Mist Raven enters the battlefield, return target creature to its owner\'s hand.').
card_mana_cost('mist raven', ['2', 'U', 'U']).
card_cmc('mist raven', 4).
card_layout('mist raven', 'normal').
card_power('mist raven', 2).
card_toughness('mist raven', 2).

% Found in: LRW
card_name('mistbind clique', 'Mistbind Clique').
card_type('mistbind clique', 'Creature — Faerie Wizard').
card_types('mistbind clique', ['Creature']).
card_subtypes('mistbind clique', ['Faerie', 'Wizard']).
card_colors('mistbind clique', ['U']).
card_text('mistbind clique', 'Flash\nFlying\nChampion a Faerie (When this enters the battlefield, sacrifice it unless you exile another Faerie you control. When this leaves the battlefield, that card returns to the battlefield.)\nWhen a Faerie is championed with Mistbind Clique, tap all lands target player controls.').
card_mana_cost('mistbind clique', ['3', 'U']).
card_cmc('mistbind clique', 4).
card_layout('mistbind clique', 'normal').
card_power('mistbind clique', 4).
card_toughness('mistbind clique', 4).

% Found in: BOK, PC2
card_name('mistblade shinobi', 'Mistblade Shinobi').
card_type('mistblade shinobi', 'Creature — Human Ninja').
card_types('mistblade shinobi', ['Creature']).
card_subtypes('mistblade shinobi', ['Human', 'Ninja']).
card_colors('mistblade shinobi', ['U']).
card_text('mistblade shinobi', 'Ninjutsu {U} ({U}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Mistblade Shinobi deals combat damage to a player, you may return target creature that player controls to its owner\'s hand.').
card_mana_cost('mistblade shinobi', ['2', 'U']).
card_cmc('mistblade shinobi', 3).
card_layout('mistblade shinobi', 'normal').
card_power('mistblade shinobi', 1).
card_toughness('mistblade shinobi', 1).

% Found in: THS
card_name('mistcutter hydra', 'Mistcutter Hydra').
card_type('mistcutter hydra', 'Creature — Hydra').
card_types('mistcutter hydra', ['Creature']).
card_subtypes('mistcutter hydra', ['Hydra']).
card_colors('mistcutter hydra', ['G']).
card_text('mistcutter hydra', 'Mistcutter Hydra can\'t be countered.\nHaste, protection from blue\nMistcutter Hydra enters the battlefield with X +1/+1 counters on it.').
card_mana_cost('mistcutter hydra', ['X', 'G']).
card_cmc('mistcutter hydra', 1).
card_layout('mistcutter hydra', 'normal').
card_power('mistcutter hydra', 0).
card_toughness('mistcutter hydra', 0).

% Found in: FRF
card_name('mistfire adept', 'Mistfire Adept').
card_type('mistfire adept', 'Creature — Human Monk').
card_types('mistfire adept', ['Creature']).
card_subtypes('mistfire adept', ['Human', 'Monk']).
card_colors('mistfire adept', ['U']).
card_text('mistfire adept', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever you cast a noncreature spell, target creature gains flying until end of turn.').
card_mana_cost('mistfire adept', ['3', 'U']).
card_cmc('mistfire adept', 4).
card_layout('mistfire adept', 'normal').
card_power('mistfire adept', 3).
card_toughness('mistfire adept', 3).

% Found in: KTK
card_name('mistfire weaver', 'Mistfire Weaver').
card_type('mistfire weaver', 'Creature — Djinn Wizard').
card_types('mistfire weaver', ['Creature']).
card_subtypes('mistfire weaver', ['Djinn', 'Wizard']).
card_colors('mistfire weaver', ['U']).
card_text('mistfire weaver', 'Flying\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Mistfire Weaver is turned face up, target creature you control gains hexproof until end of turn.').
card_mana_cost('mistfire weaver', ['3', 'U']).
card_cmc('mistfire weaver', 4).
card_layout('mistfire weaver', 'normal').
card_power('mistfire weaver', 3).
card_toughness('mistfire weaver', 1).

% Found in: CST, ICE
card_name('mistfolk', 'Mistfolk').
card_type('mistfolk', 'Creature — Illusion').
card_types('mistfolk', ['Creature']).
card_subtypes('mistfolk', ['Illusion']).
card_colors('mistfolk', ['U']).
card_text('mistfolk', '{U}: Counter target spell that targets Mistfolk.').
card_mana_cost('mistfolk', ['U', 'U']).
card_cmc('mistfolk', 2).
card_layout('mistfolk', 'normal').
card_power('mistfolk', 1).
card_toughness('mistfolk', 2).

% Found in: ONS
card_name('mistform dreamer', 'Mistform Dreamer').
card_type('mistform dreamer', 'Creature — Illusion').
card_types('mistform dreamer', ['Creature']).
card_subtypes('mistform dreamer', ['Illusion']).
card_colors('mistform dreamer', ['U']).
card_text('mistform dreamer', 'Flying\n{1}: Mistform Dreamer becomes the creature type of your choice until end of turn.').
card_mana_cost('mistform dreamer', ['2', 'U']).
card_cmc('mistform dreamer', 3).
card_layout('mistform dreamer', 'normal').
card_power('mistform dreamer', 2).
card_toughness('mistform dreamer', 1).

% Found in: ONS
card_name('mistform mask', 'Mistform Mask').
card_type('mistform mask', 'Enchantment — Aura').
card_types('mistform mask', ['Enchantment']).
card_subtypes('mistform mask', ['Aura']).
card_colors('mistform mask', ['U']).
card_text('mistform mask', 'Enchant creature\n{1}: Enchanted creature becomes the creature type of your choice until end of turn.').
card_mana_cost('mistform mask', ['1', 'U']).
card_cmc('mistform mask', 2).
card_layout('mistform mask', 'normal').

% Found in: ONS
card_name('mistform mutant', 'Mistform Mutant').
card_type('mistform mutant', 'Creature — Illusion Mutant').
card_types('mistform mutant', ['Creature']).
card_subtypes('mistform mutant', ['Illusion', 'Mutant']).
card_colors('mistform mutant', ['U']).
card_text('mistform mutant', '{1}{U}: Choose a creature type other than Wall. Target creature becomes that type until end of turn.').
card_mana_cost('mistform mutant', ['4', 'U', 'U']).
card_cmc('mistform mutant', 6).
card_layout('mistform mutant', 'normal').
card_power('mistform mutant', 3).
card_toughness('mistform mutant', 4).

% Found in: LGN
card_name('mistform seaswift', 'Mistform Seaswift').
card_type('mistform seaswift', 'Creature — Illusion').
card_types('mistform seaswift', ['Creature']).
card_subtypes('mistform seaswift', ['Illusion']).
card_colors('mistform seaswift', ['U']).
card_text('mistform seaswift', 'Flying\n{1}: Mistform Seaswift becomes the creature type of your choice until end of turn.\nMorph {1}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('mistform seaswift', ['3', 'U']).
card_cmc('mistform seaswift', 4).
card_layout('mistform seaswift', 'normal').
card_power('mistform seaswift', 3).
card_toughness('mistform seaswift', 1).

% Found in: ONS
card_name('mistform shrieker', 'Mistform Shrieker').
card_type('mistform shrieker', 'Creature — Illusion').
card_types('mistform shrieker', ['Creature']).
card_subtypes('mistform shrieker', ['Illusion']).
card_colors('mistform shrieker', ['U']).
card_text('mistform shrieker', 'Flying\n{1}: Mistform Shrieker becomes the creature type of your choice until end of turn.\nMorph {3}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('mistform shrieker', ['3', 'U', 'U']).
card_cmc('mistform shrieker', 5).
card_layout('mistform shrieker', 'normal').
card_power('mistform shrieker', 3).
card_toughness('mistform shrieker', 3).

% Found in: ONS
card_name('mistform skyreaver', 'Mistform Skyreaver').
card_type('mistform skyreaver', 'Creature — Illusion').
card_types('mistform skyreaver', ['Creature']).
card_subtypes('mistform skyreaver', ['Illusion']).
card_colors('mistform skyreaver', ['U']).
card_text('mistform skyreaver', 'Flying\n{1}: Mistform Skyreaver becomes the creature type of your choice until end of turn.').
card_mana_cost('mistform skyreaver', ['5', 'U', 'U']).
card_cmc('mistform skyreaver', 7).
card_layout('mistform skyreaver', 'normal').
card_power('mistform skyreaver', 6).
card_toughness('mistform skyreaver', 6).

% Found in: LGN
card_name('mistform sliver', 'Mistform Sliver').
card_type('mistform sliver', 'Creature — Illusion Sliver').
card_types('mistform sliver', ['Creature']).
card_subtypes('mistform sliver', ['Illusion', 'Sliver']).
card_colors('mistform sliver', ['U']).
card_text('mistform sliver', 'All Slivers have \"{1}: This permanent becomes the creature type of your choice in addition to its other types until end of turn.\"').
card_mana_cost('mistform sliver', ['1', 'U']).
card_cmc('mistform sliver', 2).
card_layout('mistform sliver', 'normal').
card_power('mistform sliver', 1).
card_toughness('mistform sliver', 1).

% Found in: ONS
card_name('mistform stalker', 'Mistform Stalker').
card_type('mistform stalker', 'Creature — Illusion').
card_types('mistform stalker', ['Creature']).
card_subtypes('mistform stalker', ['Illusion']).
card_colors('mistform stalker', ['U']).
card_text('mistform stalker', '{1}: Mistform Stalker becomes the creature type of your choice until end of turn.\n{2}{U}{U}: Mistform Stalker gets +2/+2 and gains flying until end of turn.').
card_mana_cost('mistform stalker', ['1', 'U']).
card_cmc('mistform stalker', 2).
card_layout('mistform stalker', 'normal').
card_power('mistform stalker', 1).
card_toughness('mistform stalker', 1).

% Found in: LGN, TSB
card_name('mistform ultimus', 'Mistform Ultimus').
card_type('mistform ultimus', 'Legendary Creature — Illusion').
card_types('mistform ultimus', ['Creature']).
card_subtypes('mistform ultimus', ['Illusion']).
card_supertypes('mistform ultimus', ['Legendary']).
card_colors('mistform ultimus', ['U']).
card_text('mistform ultimus', 'Mistform Ultimus is every creature type (even if this card isn\'t on the battlefield).').
card_mana_cost('mistform ultimus', ['3', 'U']).
card_cmc('mistform ultimus', 4).
card_layout('mistform ultimus', 'normal').
card_power('mistform ultimus', 3).
card_toughness('mistform ultimus', 3).

% Found in: LGN
card_name('mistform wakecaster', 'Mistform Wakecaster').
card_type('mistform wakecaster', 'Creature — Illusion').
card_types('mistform wakecaster', ['Creature']).
card_subtypes('mistform wakecaster', ['Illusion']).
card_colors('mistform wakecaster', ['U']).
card_text('mistform wakecaster', 'Flying\n{1}: Mistform Wakecaster becomes the creature type of your choice until end of turn.\n{2}{U}{U}, {T}: Choose a creature type. Each creature you control becomes that type until end of turn.').
card_mana_cost('mistform wakecaster', ['4', 'U']).
card_cmc('mistform wakecaster', 5).
card_layout('mistform wakecaster', 'normal').
card_power('mistform wakecaster', 2).
card_toughness('mistform wakecaster', 3).

% Found in: ONS
card_name('mistform wall', 'Mistform Wall').
card_type('mistform wall', 'Creature — Illusion Wall').
card_types('mistform wall', ['Creature']).
card_subtypes('mistform wall', ['Illusion', 'Wall']).
card_colors('mistform wall', ['U']).
card_text('mistform wall', 'Mistform Wall has defender as long as it\'s a Wall.\n{1}: Mistform Wall becomes the creature type of your choice until end of turn.').
card_mana_cost('mistform wall', ['2', 'U']).
card_cmc('mistform wall', 3).
card_layout('mistform wall', 'normal').
card_power('mistform wall', 1).
card_toughness('mistform wall', 4).

% Found in: SCG
card_name('mistform warchief', 'Mistform Warchief').
card_type('mistform warchief', 'Creature — Illusion').
card_types('mistform warchief', ['Creature']).
card_subtypes('mistform warchief', ['Illusion']).
card_colors('mistform warchief', ['U']).
card_text('mistform warchief', 'Creature spells you cast that share a creature type with Mistform Warchief cost {1} less to cast.\n{T}: Mistform Warchief becomes the creature type of your choice until end of turn.').
card_mana_cost('mistform warchief', ['2', 'U']).
card_cmc('mistform warchief', 3).
card_layout('mistform warchief', 'normal').
card_power('mistform warchief', 1).
card_toughness('mistform warchief', 3).

% Found in: AVR
card_name('misthollow griffin', 'Misthollow Griffin').
card_type('misthollow griffin', 'Creature — Griffin').
card_types('misthollow griffin', ['Creature']).
card_subtypes('misthollow griffin', ['Griffin']).
card_colors('misthollow griffin', ['U']).
card_text('misthollow griffin', 'Flying\nYou may cast Misthollow Griffin from exile.').
card_mana_cost('misthollow griffin', ['2', 'U', 'U']).
card_cmc('misthollow griffin', 4).
card_layout('misthollow griffin', 'normal').
card_power('misthollow griffin', 3).
card_toughness('misthollow griffin', 3).

% Found in: DTK
card_name('misthoof kirin', 'Misthoof Kirin').
card_type('misthoof kirin', 'Creature — Kirin').
card_types('misthoof kirin', ['Creature']).
card_subtypes('misthoof kirin', ['Kirin']).
card_colors('misthoof kirin', ['W']).
card_text('misthoof kirin', 'Flying, vigilance\nMegamorph {1}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('misthoof kirin', ['2', 'W']).
card_cmc('misthoof kirin', 3).
card_layout('misthoof kirin', 'normal').
card_power('misthoof kirin', 2).
card_toughness('misthoof kirin', 1).

% Found in: FUT, SHM
card_name('mistmeadow skulk', 'Mistmeadow Skulk').
card_type('mistmeadow skulk', 'Creature — Kithkin Rogue').
card_types('mistmeadow skulk', ['Creature']).
card_subtypes('mistmeadow skulk', ['Kithkin', 'Rogue']).
card_colors('mistmeadow skulk', ['W']).
card_text('mistmeadow skulk', 'Lifelink, protection from converted mana cost 3 or greater').
card_mana_cost('mistmeadow skulk', ['1', 'W']).
card_cmc('mistmeadow skulk', 2).
card_layout('mistmeadow skulk', 'normal').
card_power('mistmeadow skulk', 1).
card_toughness('mistmeadow skulk', 1).

% Found in: C13, DDI, SHM
card_name('mistmeadow witch', 'Mistmeadow Witch').
card_type('mistmeadow witch', 'Creature — Kithkin Wizard').
card_types('mistmeadow witch', ['Creature']).
card_subtypes('mistmeadow witch', ['Kithkin', 'Wizard']).
card_colors('mistmeadow witch', ['W', 'U']).
card_text('mistmeadow witch', '{2}{W}{U}: Exile target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('mistmeadow witch', ['1', 'W/U']).
card_cmc('mistmeadow witch', 2).
card_layout('mistmeadow witch', 'normal').
card_power('mistmeadow witch', 1).
card_toughness('mistmeadow witch', 1).

% Found in: VMA, WTH
card_name('mistmoon griffin', 'Mistmoon Griffin').
card_type('mistmoon griffin', 'Creature — Griffin').
card_types('mistmoon griffin', ['Creature']).
card_subtypes('mistmoon griffin', ['Griffin']).
card_colors('mistmoon griffin', ['W']).
card_text('mistmoon griffin', 'Flying\nWhen Mistmoon Griffin dies, exile Mistmoon Griffin, then return the top creature card of your graveyard to the battlefield.').
card_mana_cost('mistmoon griffin', ['3', 'W']).
card_cmc('mistmoon griffin', 4).
card_layout('mistmoon griffin', 'normal').
card_power('mistmoon griffin', 2).
card_toughness('mistmoon griffin', 2).

% Found in: DIS
card_name('mistral charger', 'Mistral Charger').
card_type('mistral charger', 'Creature — Pegasus').
card_types('mistral charger', ['Creature']).
card_subtypes('mistral charger', ['Pegasus']).
card_colors('mistral charger', ['W']).
card_text('mistral charger', 'Flying').
card_mana_cost('mistral charger', ['1', 'W']).
card_cmc('mistral charger', 2).
card_layout('mistral charger', 'normal').
card_power('mistral charger', 2).
card_toughness('mistral charger', 1).

% Found in: SHM
card_name('mistveil plains', 'Mistveil Plains').
card_type('mistveil plains', 'Land — Plains').
card_types('mistveil plains', ['Land']).
card_subtypes('mistveil plains', ['Plains']).
card_colors('mistveil plains', []).
card_text('mistveil plains', '({T}: Add {W} to your mana pool.)\nMistveil Plains enters the battlefield tapped.\n{W}, {T}: Put target card from your graveyard on the bottom of your library. Activate this ability only if you control two or more white permanents.').
card_layout('mistveil plains', 'normal').

% Found in: ARB, ARC
card_name('mistvein borderpost', 'Mistvein Borderpost').
card_type('mistvein borderpost', 'Artifact').
card_types('mistvein borderpost', ['Artifact']).
card_subtypes('mistvein borderpost', []).
card_colors('mistvein borderpost', ['U', 'B']).
card_text('mistvein borderpost', 'You may pay {1} and return a basic land you control to its owner\'s hand rather than pay Mistvein Borderpost\'s mana cost.\nMistvein Borderpost enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.').
card_mana_cost('mistvein borderpost', ['1', 'U', 'B']).
card_cmc('mistvein borderpost', 3).
card_layout('mistvein borderpost', 'normal').

% Found in: EXP, ZEN
card_name('misty rainforest', 'Misty Rainforest').
card_type('misty rainforest', 'Land').
card_types('misty rainforest', ['Land']).
card_subtypes('misty rainforest', []).
card_colors('misty rainforest', []).
card_text('misty rainforest', '{T}, Pay 1 life, Sacrifice Misty Rainforest: Search your library for a Forest or Island card and put it onto the battlefield. Then shuffle your library.').
card_layout('misty rainforest', 'normal').

% Found in: MBS
card_name('mitotic manipulation', 'Mitotic Manipulation').
card_type('mitotic manipulation', 'Sorcery').
card_types('mitotic manipulation', ['Sorcery']).
card_subtypes('mitotic manipulation', []).
card_colors('mitotic manipulation', ['U']).
card_text('mitotic manipulation', 'Look at the top seven cards of your library. You may put one of those cards onto the battlefield if it has the same name as a permanent. Put the rest on the bottom of your library in any order.').
card_mana_cost('mitotic manipulation', ['1', 'U', 'U']).
card_cmc('mitotic manipulation', 3).
card_layout('mitotic manipulation', 'normal').

% Found in: M11, PC2, pMGD
card_name('mitotic slime', 'Mitotic Slime').
card_type('mitotic slime', 'Creature — Ooze').
card_types('mitotic slime', ['Creature']).
card_subtypes('mitotic slime', ['Ooze']).
card_colors('mitotic slime', ['G']).
card_text('mitotic slime', 'When Mitotic Slime dies, put two 2/2 green Ooze creature tokens onto the battlefield. They have \"When this creature dies, put two 1/1 green Ooze creature tokens onto the battlefield.\"').
card_mana_cost('mitotic slime', ['4', 'G']).
card_cmc('mitotic slime', 5).
card_layout('mitotic slime', 'normal').
card_power('mitotic slime', 4).
card_toughness('mitotic slime', 4).

% Found in: ORI
card_name('mizzium meddler', 'Mizzium Meddler').
card_type('mizzium meddler', 'Creature — Vedalken Wizard').
card_types('mizzium meddler', ['Creature']).
card_subtypes('mizzium meddler', ['Vedalken', 'Wizard']).
card_colors('mizzium meddler', ['U']).
card_text('mizzium meddler', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Mizzium Meddler enters the battlefield, you may change a target of target spell or ability to Mizzium Meddler.').
card_mana_cost('mizzium meddler', ['2', 'U']).
card_cmc('mizzium meddler', 3).
card_layout('mizzium meddler', 'normal').
card_power('mizzium meddler', 1).
card_toughness('mizzium meddler', 4).

% Found in: RTR
card_name('mizzium mortars', 'Mizzium Mortars').
card_type('mizzium mortars', 'Sorcery').
card_types('mizzium mortars', ['Sorcery']).
card_subtypes('mizzium mortars', []).
card_colors('mizzium mortars', ['R']).
card_text('mizzium mortars', 'Mizzium Mortars deals 4 damage to target creature you don\'t control.\nOverload {3}{R}{R}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('mizzium mortars', ['1', 'R']).
card_cmc('mizzium mortars', 2).
card_layout('mizzium mortars', 'normal').

% Found in: RTR
card_name('mizzium skin', 'Mizzium Skin').
card_type('mizzium skin', 'Instant').
card_types('mizzium skin', ['Instant']).
card_subtypes('mizzium skin', []).
card_colors('mizzium skin', ['U']).
card_text('mizzium skin', 'Target creature you control gets +0/+1 and gains hexproof until end of turn.\nOverload {1}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('mizzium skin', ['U']).
card_cmc('mizzium skin', 1).
card_layout('mizzium skin', 'normal').

% Found in: GPT
card_name('mizzium transreliquat', 'Mizzium Transreliquat').
card_type('mizzium transreliquat', 'Artifact').
card_types('mizzium transreliquat', ['Artifact']).
card_subtypes('mizzium transreliquat', []).
card_colors('mizzium transreliquat', []).
card_text('mizzium transreliquat', '{3}: Mizzium Transreliquat becomes a copy of target artifact until end of turn.\n{1}{U}{R}: Mizzium Transreliquat becomes a copy of target artifact and gains this ability.').
card_mana_cost('mizzium transreliquat', ['3']).
card_cmc('mizzium transreliquat', 3).
card_layout('mizzium transreliquat', 'normal').

% Found in: RAV
card_name('mnemonic nexus', 'Mnemonic Nexus').
card_type('mnemonic nexus', 'Instant').
card_types('mnemonic nexus', ['Instant']).
card_subtypes('mnemonic nexus', []).
card_colors('mnemonic nexus', ['U']).
card_text('mnemonic nexus', 'Each player shuffles his or her graveyard into his or her library.').
card_mana_cost('mnemonic nexus', ['3', 'U']).
card_cmc('mnemonic nexus', 4).
card_layout('mnemonic nexus', 'normal').

% Found in: TMP, TPR
card_name('mnemonic sliver', 'Mnemonic Sliver').
card_type('mnemonic sliver', 'Creature — Sliver').
card_types('mnemonic sliver', ['Creature']).
card_subtypes('mnemonic sliver', ['Sliver']).
card_colors('mnemonic sliver', ['U']).
card_text('mnemonic sliver', 'All Slivers have \"{2}, Sacrifice this permanent: Draw a card.\"').
card_mana_cost('mnemonic sliver', ['2', 'U']).
card_cmc('mnemonic sliver', 3).
card_layout('mnemonic sliver', 'normal').
card_power('mnemonic sliver', 2).
card_toughness('mnemonic sliver', 2).

% Found in: C13, ROE, THS
card_name('mnemonic wall', 'Mnemonic Wall').
card_type('mnemonic wall', 'Creature — Wall').
card_types('mnemonic wall', ['Creature']).
card_subtypes('mnemonic wall', ['Wall']).
card_colors('mnemonic wall', ['U']).
card_text('mnemonic wall', 'Defender\nWhen Mnemonic Wall enters the battlefield, you may return target instant or sorcery card from your graveyard to your hand.').
card_mana_cost('mnemonic wall', ['4', 'U']).
card_cmc('mnemonic wall', 5).
card_layout('mnemonic wall', 'normal').
card_power('mnemonic wall', 0).
card_toughness('mnemonic wall', 4).

% Found in: ISD
card_name('moan of the unhallowed', 'Moan of the Unhallowed').
card_type('moan of the unhallowed', 'Sorcery').
card_types('moan of the unhallowed', ['Sorcery']).
card_subtypes('moan of the unhallowed', []).
card_colors('moan of the unhallowed', ['B']).
card_text('moan of the unhallowed', 'Put two 2/2 black Zombie creature tokens onto the battlefield.\nFlashback {5}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('moan of the unhallowed', ['2', 'B', 'B']).
card_cmc('moan of the unhallowed', 4).
card_layout('moan of the unhallowed', 'normal').

% Found in: PO2
card_name('moaning spirit', 'Moaning Spirit').
card_type('moaning spirit', 'Creature — Spirit').
card_types('moaning spirit', ['Creature']).
card_subtypes('moaning spirit', ['Spirit']).
card_colors('moaning spirit', ['B']).
card_text('moaning spirit', 'Flying').
card_mana_cost('moaning spirit', ['2', 'B']).
card_cmc('moaning spirit', 3).
card_layout('moaning spirit', 'normal').
card_power('moaning spirit', 2).
card_toughness('moaning spirit', 1).

% Found in: LEG, MED
card_name('moat', 'Moat').
card_type('moat', 'Enchantment').
card_types('moat', ['Enchantment']).
card_subtypes('moat', []).
card_colors('moat', ['W']).
card_text('moat', 'Creatures without flying can\'t attack.').
card_mana_cost('moat', ['2', 'W', 'W']).
card_cmc('moat', 4).
card_layout('moat', 'normal').
card_reserved('moat').

% Found in: STH
card_name('mob justice', 'Mob Justice').
card_type('mob justice', 'Sorcery').
card_types('mob justice', ['Sorcery']).
card_subtypes('mob justice', []).
card_colors('mob justice', ['R']).
card_text('mob justice', 'Mob Justice deals damage to target player equal to the number of creatures you control.').
card_mana_cost('mob justice', ['1', 'R']).
card_cmc('mob justice', 2).
card_layout('mob justice', 'normal').

% Found in: VIS
card_name('mob mentality', 'Mob Mentality').
card_type('mob mentality', 'Enchantment — Aura').
card_types('mob mentality', ['Enchantment']).
card_subtypes('mob mentality', ['Aura']).
card_colors('mob mentality', ['R']).
card_text('mob mentality', 'Enchant creature\nEnchanted creature has trample.\nWhenever all non-Wall creatures you control attack, enchanted creature gets +X/+0 until end of turn, where X is the number of attacking creatures.').
card_mana_cost('mob mentality', ['R']).
card_cmc('mob mentality', 1).
card_layout('mob mentality', 'normal').

% Found in: FRF
card_name('mob rule', 'Mob Rule').
card_type('mob rule', 'Sorcery').
card_types('mob rule', ['Sorcery']).
card_subtypes('mob rule', []).
card_colors('mob rule', ['R']).
card_text('mob rule', 'Choose one —\n• Gain control of all creatures with power 4 or greater until end of turn. Untap those creatures. They gain haste until end of turn.\n• Gain control of all creatures with power 3 or less until end of turn. Untap those creatures. They gain haste until end of turn.').
card_mana_cost('mob rule', ['4', 'R', 'R']).
card_cmc('mob rule', 6).
card_layout('mob rule', 'normal').

% Found in: USG
card_name('mobile fort', 'Mobile Fort').
card_type('mobile fort', 'Artifact Creature — Wall').
card_types('mobile fort', ['Artifact', 'Creature']).
card_subtypes('mobile fort', ['Wall']).
card_colors('mobile fort', []).
card_text('mobile fort', 'Defender (This creature can\'t attack.)\n{3}: Mobile Fort gets +3/-1 until end of turn and can attack this turn as though it didn\'t have defender. Activate this ability only once each turn.').
card_mana_cost('mobile fort', ['4']).
card_cmc('mobile fort', 4).
card_layout('mobile fort', 'normal').
card_power('mobile fort', 0).
card_toughness('mobile fort', 6).

% Found in: 10E, C14, ONS
card_name('mobilization', 'Mobilization').
card_type('mobilization', 'Enchantment').
card_types('mobilization', ['Enchantment']).
card_subtypes('mobilization', []).
card_colors('mobilization', ['W']).
card_text('mobilization', 'Soldier creatures have vigilance.\n{2}{W}: Put a 1/1 white Soldier creature token onto the battlefield.').
card_mana_cost('mobilization', ['2', 'W']).
card_cmc('mobilization', 3).
card_layout('mobilization', 'normal').

% Found in: POR
card_name('mobilize', 'Mobilize').
card_type('mobilize', 'Sorcery').
card_types('mobilize', ['Sorcery']).
card_subtypes('mobilize', []).
card_colors('mobilize', ['G']).
card_text('mobilize', 'Untap all creatures you control.').
card_mana_cost('mobilize', ['G']).
card_cmc('mobilize', 1).
card_layout('mobilize', 'normal').

% Found in: NMS
card_name('mogg alarm', 'Mogg Alarm').
card_type('mogg alarm', 'Sorcery').
card_types('mogg alarm', ['Sorcery']).
card_subtypes('mogg alarm', []).
card_colors('mogg alarm', ['R']).
card_text('mogg alarm', 'You may sacrifice two Mountains rather than pay Mogg Alarm\'s mana cost.\nPut two 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('mogg alarm', ['1', 'R', 'R']).
card_cmc('mogg alarm', 3).
card_layout('mogg alarm', 'normal').

% Found in: EXO
card_name('mogg assassin', 'Mogg Assassin').
card_type('mogg assassin', 'Creature — Goblin Assassin').
card_types('mogg assassin', ['Creature']).
card_subtypes('mogg assassin', ['Goblin', 'Assassin']).
card_colors('mogg assassin', ['R']).
card_text('mogg assassin', '{T}: You choose target creature an opponent controls, and that opponent chooses target creature. Flip a coin. If you win the flip, destroy the creature you chose. If you lose the flip, destroy the creature your opponent chose.').
card_mana_cost('mogg assassin', ['2', 'R']).
card_cmc('mogg assassin', 3).
card_layout('mogg assassin', 'normal').
card_power('mogg assassin', 2).
card_toughness('mogg assassin', 1).

% Found in: STH
card_name('mogg bombers', 'Mogg Bombers').
card_type('mogg bombers', 'Creature — Goblin').
card_types('mogg bombers', ['Creature']).
card_subtypes('mogg bombers', ['Goblin']).
card_colors('mogg bombers', ['R']).
card_text('mogg bombers', 'When another creature enters the battlefield, sacrifice Mogg Bombers and it deals 3 damage to target player.').
card_mana_cost('mogg bombers', ['3', 'R']).
card_cmc('mogg bombers', 4).
card_layout('mogg bombers', 'normal').
card_power('mogg bombers', 3).
card_toughness('mogg bombers', 4).

% Found in: TMP
card_name('mogg cannon', 'Mogg Cannon').
card_type('mogg cannon', 'Artifact').
card_types('mogg cannon', ['Artifact']).
card_subtypes('mogg cannon', []).
card_colors('mogg cannon', []).
card_text('mogg cannon', '{T}: Target creature you control gets +1/+0 and gains flying until end of turn. Destroy that creature at the beginning of the next end step.').
card_mana_cost('mogg cannon', ['2']).
card_cmc('mogg cannon', 2).
card_layout('mogg cannon', 'normal').

% Found in: TMP, TPR
card_name('mogg conscripts', 'Mogg Conscripts').
card_type('mogg conscripts', 'Creature — Goblin').
card_types('mogg conscripts', ['Creature']).
card_subtypes('mogg conscripts', ['Goblin']).
card_colors('mogg conscripts', ['R']).
card_text('mogg conscripts', 'Mogg Conscripts can\'t attack unless you\'ve cast a creature spell this turn.').
card_mana_cost('mogg conscripts', ['R']).
card_cmc('mogg conscripts', 1).
card_layout('mogg conscripts', 'normal').
card_power('mogg conscripts', 2).
card_toughness('mogg conscripts', 2).

% Found in: 10E, ATH, DD3_EVG, EVG, PD2, TMP, TPR, pFNM, pGTW
card_name('mogg fanatic', 'Mogg Fanatic').
card_type('mogg fanatic', 'Creature — Goblin').
card_types('mogg fanatic', ['Creature']).
card_subtypes('mogg fanatic', ['Goblin']).
card_colors('mogg fanatic', ['R']).
card_text('mogg fanatic', 'Sacrifice Mogg Fanatic: Mogg Fanatic deals 1 damage to target creature or player.').
card_mana_cost('mogg fanatic', ['R']).
card_cmc('mogg fanatic', 1).
card_layout('mogg fanatic', 'normal').
card_power('mogg fanatic', 1).
card_toughness('mogg fanatic', 1).

% Found in: ATH, M13, PD2, STH, TPR
card_name('mogg flunkies', 'Mogg Flunkies').
card_type('mogg flunkies', 'Creature — Goblin').
card_types('mogg flunkies', ['Creature']).
card_subtypes('mogg flunkies', ['Goblin']).
card_colors('mogg flunkies', ['R']).
card_text('mogg flunkies', 'Mogg Flunkies can\'t attack or block alone.').
card_mana_cost('mogg flunkies', ['1', 'R']).
card_cmc('mogg flunkies', 2).
card_layout('mogg flunkies', 'normal').
card_power('mogg flunkies', 3).
card_toughness('mogg flunkies', 3).

% Found in: BRB, TMP, TPR
card_name('mogg hollows', 'Mogg Hollows').
card_type('mogg hollows', 'Land').
card_types('mogg hollows', ['Land']).
card_subtypes('mogg hollows', []).
card_colors('mogg hollows', []).
card_text('mogg hollows', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Mogg Hollows doesn\'t untap during your next untap step.').
card_layout('mogg hollows', 'normal').

% Found in: STH, TPR
card_name('mogg infestation', 'Mogg Infestation').
card_type('mogg infestation', 'Sorcery').
card_types('mogg infestation', ['Sorcery']).
card_subtypes('mogg infestation', []).
card_colors('mogg infestation', ['R']).
card_text('mogg infestation', 'Destroy all creatures target player controls. For each creature that died this way, put two 1/1 red Goblin creature tokens onto the battlefield under that player\'s control.').
card_mana_cost('mogg infestation', ['3', 'R', 'R']).
card_cmc('mogg infestation', 5).
card_layout('mogg infestation', 'normal').

% Found in: PLS
card_name('mogg jailer', 'Mogg Jailer').
card_type('mogg jailer', 'Creature — Goblin').
card_types('mogg jailer', ['Creature']).
card_subtypes('mogg jailer', ['Goblin']).
card_colors('mogg jailer', ['R']).
card_text('mogg jailer', 'Mogg Jailer can\'t attack if defending player controls an untapped creature with power 2 or less.').
card_mana_cost('mogg jailer', ['1', 'R']).
card_cmc('mogg jailer', 2).
card_layout('mogg jailer', 'normal').
card_power('mogg jailer', 2).
card_toughness('mogg jailer', 2).

% Found in: STH, TPR
card_name('mogg maniac', 'Mogg Maniac').
card_type('mogg maniac', 'Creature — Goblin').
card_types('mogg maniac', ['Creature']).
card_subtypes('mogg maniac', ['Goblin']).
card_colors('mogg maniac', ['R']).
card_text('mogg maniac', 'Whenever Mogg Maniac is dealt damage, it deals that much damage to target opponent.').
card_mana_cost('mogg maniac', ['1', 'R']).
card_cmc('mogg maniac', 2).
card_layout('mogg maniac', 'normal').
card_power('mogg maniac', 1).
card_toughness('mogg maniac', 1).

% Found in: ATH, TMP
card_name('mogg raider', 'Mogg Raider').
card_type('mogg raider', 'Creature — Goblin').
card_types('mogg raider', ['Creature']).
card_subtypes('mogg raider', ['Goblin']).
card_colors('mogg raider', ['R']).
card_text('mogg raider', 'Sacrifice a Goblin: Target creature gets +1/+1 until end of turn.').
card_mana_cost('mogg raider', ['R']).
card_cmc('mogg raider', 1).
card_layout('mogg raider', 'normal').
card_power('mogg raider', 1).
card_toughness('mogg raider', 1).

% Found in: NMS
card_name('mogg salvage', 'Mogg Salvage').
card_type('mogg salvage', 'Instant').
card_types('mogg salvage', ['Instant']).
card_subtypes('mogg salvage', []).
card_colors('mogg salvage', ['R']).
card_text('mogg salvage', 'If an opponent controls an Island and you control a Mountain, you may cast Mogg Salvage without paying its mana cost.\nDestroy target artifact.').
card_mana_cost('mogg salvage', ['2', 'R']).
card_cmc('mogg salvage', 3).
card_layout('mogg salvage', 'normal').

% Found in: 8ED, 9ED, PLS
card_name('mogg sentry', 'Mogg Sentry').
card_type('mogg sentry', 'Creature — Goblin Warrior').
card_types('mogg sentry', ['Creature']).
card_subtypes('mogg sentry', ['Goblin', 'Warrior']).
card_colors('mogg sentry', ['R']).
card_text('mogg sentry', 'Whenever an opponent casts a spell, Mogg Sentry gets +2/+2 until end of turn.').
card_mana_cost('mogg sentry', ['R']).
card_cmc('mogg sentry', 1).
card_layout('mogg sentry', 'normal').
card_power('mogg sentry', 1).
card_toughness('mogg sentry', 1).

% Found in: TMP
card_name('mogg squad', 'Mogg Squad').
card_type('mogg squad', 'Creature — Goblin').
card_types('mogg squad', ['Creature']).
card_subtypes('mogg squad', ['Goblin']).
card_colors('mogg squad', ['R']).
card_text('mogg squad', 'Mogg Squad gets -1/-1 for each other creature on the battlefield.').
card_mana_cost('mogg squad', ['1', 'R']).
card_cmc('mogg squad', 2).
card_layout('mogg squad', 'normal').
card_power('mogg squad', 3).
card_toughness('mogg squad', 3).

% Found in: NMS
card_name('mogg toady', 'Mogg Toady').
card_type('mogg toady', 'Creature — Goblin').
card_types('mogg toady', ['Creature']).
card_subtypes('mogg toady', ['Goblin']).
card_colors('mogg toady', ['R']).
card_text('mogg toady', 'Mogg Toady can\'t attack unless you control more creatures than defending player.\nMogg Toady can\'t block unless you control more creatures than attacking player.').
card_mana_cost('mogg toady', ['1', 'R']).
card_cmc('mogg toady', 2).
card_layout('mogg toady', 'normal').
card_power('mogg toady', 2).
card_toughness('mogg toady', 2).

% Found in: DD3_EVG, EVG, MMA, TSP
card_name('mogg war marshal', 'Mogg War Marshal').
card_type('mogg war marshal', 'Creature — Goblin Warrior').
card_types('mogg war marshal', ['Creature']).
card_subtypes('mogg war marshal', ['Goblin', 'Warrior']).
card_colors('mogg war marshal', ['R']).
card_text('mogg war marshal', 'Echo {1}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Mogg War Marshal enters the battlefield or dies, put a 1/1 red Goblin creature token onto the battlefield.').
card_mana_cost('mogg war marshal', ['1', 'R']).
card_cmc('mogg war marshal', 2).
card_layout('mogg war marshal', 'normal').
card_power('mogg war marshal', 1).
card_toughness('mogg war marshal', 1).

% Found in: NMS
card_name('moggcatcher', 'Moggcatcher').
card_type('moggcatcher', 'Creature — Human Mercenary').
card_types('moggcatcher', ['Creature']).
card_subtypes('moggcatcher', ['Human', 'Mercenary']).
card_colors('moggcatcher', ['R']).
card_text('moggcatcher', '{3}, {T}: Search your library for a Goblin permanent card and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('moggcatcher', ['2', 'R', 'R']).
card_cmc('moggcatcher', 4).
card_layout('moggcatcher', 'normal').
card_power('moggcatcher', 2).
card_toughness('moggcatcher', 2).

% Found in: THS
card_name('mogis\'s marauder', 'Mogis\'s Marauder').
card_type('mogis\'s marauder', 'Creature — Human Berserker').
card_types('mogis\'s marauder', ['Creature']).
card_subtypes('mogis\'s marauder', ['Human', 'Berserker']).
card_colors('mogis\'s marauder', ['B']).
card_text('mogis\'s marauder', 'When Mogis\'s Marauder enters the battlefield, up to X target creatures each gain intimidate and haste until end of turn, where X is your devotion to black. (A creature with intimidate can\'t be blocked except by artifact creatures and/or creatures that share a color with it. Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_mana_cost('mogis\'s marauder', ['2', 'B']).
card_cmc('mogis\'s marauder', 3).
card_layout('mogis\'s marauder', 'normal').
card_power('mogis\'s marauder', 2).
card_toughness('mogis\'s marauder', 2).

% Found in: JOU
card_name('mogis\'s warhound', 'Mogis\'s Warhound').
card_type('mogis\'s warhound', 'Enchantment Creature — Hound').
card_types('mogis\'s warhound', ['Enchantment', 'Creature']).
card_subtypes('mogis\'s warhound', ['Hound']).
card_colors('mogis\'s warhound', ['R']).
card_text('mogis\'s warhound', 'Bestow {2}{R} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nMogis\'s Warhound attacks each turn if able.\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_mana_cost('mogis\'s warhound', ['1', 'R']).
card_cmc('mogis\'s warhound', 2).
card_layout('mogis\'s warhound', 'normal').
card_power('mogis\'s warhound', 2).
card_toughness('mogis\'s warhound', 2).

% Found in: BNG
card_name('mogis, god of slaughter', 'Mogis, God of Slaughter').
card_type('mogis, god of slaughter', 'Legendary Enchantment Creature — God').
card_types('mogis, god of slaughter', ['Enchantment', 'Creature']).
card_subtypes('mogis, god of slaughter', ['God']).
card_supertypes('mogis, god of slaughter', ['Legendary']).
card_colors('mogis, god of slaughter', ['B', 'R']).
card_text('mogis, god of slaughter', 'Indestructible\nAs long as your devotion to black and red is less than seven, Mogis isn\'t a creature.\nAt the beginning of each opponent\'s upkeep, Mogis deals 2 damage to that player unless he or she sacrifices a creature.').
card_mana_cost('mogis, god of slaughter', ['2', 'B', 'R']).
card_cmc('mogis, god of slaughter', 4).
card_layout('mogis, god of slaughter', 'normal').
card_power('mogis, god of slaughter', 7).
card_toughness('mogis, god of slaughter', 5).

% Found in: M10
card_name('mold adder', 'Mold Adder').
card_type('mold adder', 'Creature — Fungus Snake').
card_types('mold adder', ['Creature']).
card_subtypes('mold adder', ['Fungus', 'Snake']).
card_colors('mold adder', ['G']).
card_text('mold adder', 'Whenever an opponent casts a blue or black spell, you may put a +1/+1 counter on Mold Adder.').
card_mana_cost('mold adder', ['G']).
card_cmc('mold adder', 1).
card_layout('mold adder', 'normal').
card_power('mold adder', 1).
card_toughness('mold adder', 1).

% Found in: LEG
card_name('mold demon', 'Mold Demon').
card_type('mold demon', 'Creature — Fungus Demon').
card_types('mold demon', ['Creature']).
card_subtypes('mold demon', ['Fungus', 'Demon']).
card_colors('mold demon', ['B']).
card_text('mold demon', 'When Mold Demon enters the battlefield, sacrifice it unless you sacrifice two Swamps.').
card_mana_cost('mold demon', ['5', 'B', 'B']).
card_cmc('mold demon', 7).
card_layout('mold demon', 'normal').
card_power('mold demon', 6).
card_toughness('mold demon', 6).
card_reserved('mold demon').

% Found in: C13, DDM, ZEN
card_name('mold shambler', 'Mold Shambler').
card_type('mold shambler', 'Creature — Fungus Beast').
card_types('mold shambler', ['Creature']).
card_subtypes('mold shambler', ['Fungus', 'Beast']).
card_colors('mold shambler', ['G']).
card_text('mold shambler', 'Kicker {1}{G} (You may pay an additional {1}{G} as you cast this spell.)\nWhen Mold Shambler enters the battlefield, if it was kicked, destroy target noncreature permanent.').
card_mana_cost('mold shambler', ['3', 'G']).
card_cmc('mold shambler', 4).
card_layout('mold shambler', 'normal').
card_power('mold shambler', 3).
card_toughness('mold shambler', 3).

% Found in: TSP
card_name('molder', 'Molder').
card_type('molder', 'Instant').
card_types('molder', ['Instant']).
card_subtypes('molder', []).
card_colors('molder', ['G']).
card_text('molder', 'Destroy target artifact or enchantment with converted mana cost X. It can\'t be regenerated. You gain X life.').
card_mana_cost('molder', ['X', 'G']).
card_cmc('molder', 1).
card_layout('molder', 'normal').

% Found in: SOM
card_name('molder beast', 'Molder Beast').
card_type('molder beast', 'Creature — Beast').
card_types('molder beast', ['Creature']).
card_subtypes('molder beast', ['Beast']).
card_colors('molder beast', ['G']).
card_text('molder beast', 'Trample\nWhenever an artifact is put into a graveyard from the battlefield, Molder Beast gets +2/+0 until end of turn.').
card_mana_cost('molder beast', ['4', 'G']).
card_cmc('molder beast', 5).
card_layout('molder beast', 'normal').
card_power('molder beast', 5).
card_toughness('molder beast', 3).

% Found in: MRD
card_name('molder slug', 'Molder Slug').
card_type('molder slug', 'Creature — Slug Beast').
card_types('molder slug', ['Creature']).
card_subtypes('molder slug', ['Slug', 'Beast']).
card_colors('molder slug', ['G']).
card_text('molder slug', 'At the beginning of each player\'s upkeep, that player sacrifices an artifact.').
card_mana_cost('molder slug', ['3', 'G', 'G']).
card_cmc('molder slug', 5).
card_layout('molder slug', 'normal').
card_power('molder slug', 4).
card_toughness('molder slug', 6).

% Found in: MMA, RAV
card_name('moldervine cloak', 'Moldervine Cloak').
card_type('moldervine cloak', 'Enchantment — Aura').
card_types('moldervine cloak', ['Enchantment']).
card_subtypes('moldervine cloak', ['Aura']).
card_colors('moldervine cloak', ['G']).
card_text('moldervine cloak', 'Enchant creature\nEnchanted creature gets +3/+3.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('moldervine cloak', ['2', 'G']).
card_cmc('moldervine cloak', 3).
card_layout('moldervine cloak', 'normal').

% Found in: ISD
card_name('moldgraf monstrosity', 'Moldgraf Monstrosity').
card_type('moldgraf monstrosity', 'Creature — Insect').
card_types('moldgraf monstrosity', ['Creature']).
card_subtypes('moldgraf monstrosity', ['Insect']).
card_colors('moldgraf monstrosity', ['G']).
card_text('moldgraf monstrosity', 'Trample\nWhen Moldgraf Monstrosity dies, exile it, then return two creature cards at random from your graveyard to the battlefield.').
card_mana_cost('moldgraf monstrosity', ['4', 'G', 'G', 'G']).
card_cmc('moldgraf monstrosity', 7).
card_layout('moldgraf monstrosity', 'normal').
card_power('moldgraf monstrosity', 8).
card_toughness('moldgraf monstrosity', 8).

% Found in: 5ED, ICE
card_name('mole worms', 'Mole Worms').
card_type('mole worms', 'Creature — Worm').
card_types('mole worms', ['Creature']).
card_subtypes('mole worms', ['Worm']).
card_colors('mole worms', ['B']).
card_text('mole worms', 'You may choose not to untap Mole Worms during your untap step.\n{T}: Tap target land. It doesn\'t untap during its controller\'s untap step for as long as Mole Worms remains tapped.').
card_mana_cost('mole worms', ['2', 'B']).
card_cmc('mole worms', 3).
card_layout('mole worms', 'normal').
card_power('mole worms', 1).
card_toughness('mole worms', 1).

% Found in: 10E, ARC, DPA, INV
card_name('molimo, maro-sorcerer', 'Molimo, Maro-Sorcerer').
card_type('molimo, maro-sorcerer', 'Legendary Creature — Elemental').
card_types('molimo, maro-sorcerer', ['Creature']).
card_subtypes('molimo, maro-sorcerer', ['Elemental']).
card_supertypes('molimo, maro-sorcerer', ['Legendary']).
card_colors('molimo, maro-sorcerer', ['G']).
card_text('molimo, maro-sorcerer', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nMolimo, Maro-Sorcerer\'s power and toughness are each equal to the number of lands you control.').
card_mana_cost('molimo, maro-sorcerer', ['4', 'G', 'G', 'G']).
card_cmc('molimo, maro-sorcerer', 7).
card_layout('molimo, maro-sorcerer', 'normal').
card_power('molimo, maro-sorcerer', '*').
card_toughness('molimo, maro-sorcerer', '*').

% Found in: M14
card_name('molten birth', 'Molten Birth').
card_type('molten birth', 'Sorcery').
card_types('molten birth', ['Sorcery']).
card_subtypes('molten birth', []).
card_colors('molten birth', ['R']).
card_text('molten birth', 'Put two 1/1 red Elemental creature tokens onto the battlefield. Then flip a coin. If you win the flip, return Molten Birth to its owner\'s hand.').
card_mana_cost('molten birth', ['1', 'R', 'R']).
card_cmc('molten birth', 3).
card_layout('molten birth', 'normal').

% Found in: C13, FUT, MMA
card_name('molten disaster', 'Molten Disaster').
card_type('molten disaster', 'Sorcery').
card_types('molten disaster', ['Sorcery']).
card_subtypes('molten disaster', []).
card_colors('molten disaster', ['R']).
card_text('molten disaster', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nIf Molten Disaster was kicked, it has split second. (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nMolten Disaster deals X damage to each creature without flying and each player.').
card_mana_cost('molten disaster', ['X', 'R', 'R']).
card_cmc('molten disaster', 2).
card_layout('molten disaster', 'normal').

% Found in: PLC
card_name('molten firebird', 'Molten Firebird').
card_type('molten firebird', 'Creature — Phoenix').
card_types('molten firebird', ['Creature']).
card_subtypes('molten firebird', ['Phoenix']).
card_colors('molten firebird', ['R']).
card_text('molten firebird', 'Flying\nWhen Molten Firebird dies, return it to the battlefield under its owner\'s control at the beginning of the next end step and you skip your next draw step.\n{4}{R}: Exile Molten Firebird.').
card_mana_cost('molten firebird', ['4', 'R']).
card_cmc('molten firebird', 5).
card_layout('molten firebird', 'normal').
card_power('molten firebird', 2).
card_toughness('molten firebird', 2).

% Found in: CON
card_name('molten frame', 'Molten Frame').
card_type('molten frame', 'Instant').
card_types('molten frame', ['Instant']).
card_subtypes('molten frame', []).
card_colors('molten frame', ['R']).
card_text('molten frame', 'Destroy target artifact creature.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('molten frame', ['1', 'R']).
card_cmc('molten frame', 2).
card_layout('molten frame', 'normal').

% Found in: ULG
card_name('molten hydra', 'Molten Hydra').
card_type('molten hydra', 'Creature — Hydra').
card_types('molten hydra', ['Creature']).
card_subtypes('molten hydra', ['Hydra']).
card_colors('molten hydra', ['R']).
card_text('molten hydra', '{1}{R}{R}: Put a +1/+1 counter on Molten Hydra.\n{T}, Remove all +1/+1 counters from Molten Hydra: Molten Hydra deals damage to target creature or player equal to the number of +1/+1 counters removed this way.').
card_mana_cost('molten hydra', ['1', 'R']).
card_cmc('molten hydra', 2).
card_layout('molten hydra', 'normal').
card_power('molten hydra', 1).
card_toughness('molten hydra', 1).

% Found in: ODY
card_name('molten influence', 'Molten Influence').
card_type('molten influence', 'Instant').
card_types('molten influence', ['Instant']).
card_subtypes('molten influence', []).
card_colors('molten influence', ['R']).
card_text('molten influence', 'Counter target instant or sorcery spell unless its controller has Molten Influence deal 4 damage to him or her.').
card_mana_cost('molten influence', ['1', 'R']).
card_cmc('molten influence', 2).
card_layout('molten influence', 'normal').

% Found in: BFZ
card_name('molten nursery', 'Molten Nursery').
card_type('molten nursery', 'Enchantment').
card_types('molten nursery', ['Enchantment']).
card_subtypes('molten nursery', []).
card_colors('molten nursery', []).
card_text('molten nursery', 'Devoid (This card has no color.)\nWhenever you cast a colorless spell, Molten Nursery deals 1 damage to target creature or player.').
card_mana_cost('molten nursery', ['2', 'R']).
card_cmc('molten nursery', 3).
card_layout('molten nursery', 'normal').

% Found in: GTC
card_name('molten primordial', 'Molten Primordial').
card_type('molten primordial', 'Creature — Avatar').
card_types('molten primordial', ['Creature']).
card_subtypes('molten primordial', ['Avatar']).
card_colors('molten primordial', ['R']).
card_text('molten primordial', 'Haste\nWhen Molten Primordial enters the battlefield, for each opponent, gain control of up to one target creature that player controls until end of turn. Untap those creatures. They gain haste until end of turn.').
card_mana_cost('molten primordial', ['5', 'R', 'R']).
card_cmc('molten primordial', 7).
card_layout('molten primordial', 'normal').
card_power('molten primordial', 6).
card_toughness('molten primordial', 4).

% Found in: SOM
card_name('molten psyche', 'Molten Psyche').
card_type('molten psyche', 'Sorcery').
card_types('molten psyche', ['Sorcery']).
card_subtypes('molten psyche', []).
card_colors('molten psyche', ['R']).
card_text('molten psyche', 'Each player shuffles the cards from his or her hand into his or her library, then draws that many cards.\nMetalcraft — If you control three or more artifacts, Molten Psyche deals damage to each opponent equal to the number of cards that player has drawn this turn.').
card_mana_cost('molten psyche', ['1', 'R', 'R']).
card_cmc('molten psyche', 3).
card_layout('molten psyche', 'normal').

% Found in: MRD
card_name('molten rain', 'Molten Rain').
card_type('molten rain', 'Sorcery').
card_types('molten rain', ['Sorcery']).
card_subtypes('molten rain', []).
card_colors('molten rain', ['R']).
card_text('molten rain', 'Destroy target land. If that land was nonbasic, Molten Rain deals 2 damage to the land\'s controller.').
card_mana_cost('molten rain', ['1', 'R', 'R']).
card_cmc('molten rain', 3).
card_layout('molten rain', 'normal').

% Found in: ZEN
card_name('molten ravager', 'Molten Ravager').
card_type('molten ravager', 'Creature — Elemental').
card_types('molten ravager', ['Creature']).
card_subtypes('molten ravager', ['Elemental']).
card_colors('molten ravager', ['R']).
card_text('molten ravager', '{R}: Molten Ravager gets +1/+0 until end of turn.').
card_mana_cost('molten ravager', ['2', 'R']).
card_cmc('molten ravager', 3).
card_layout('molten ravager', 'normal').
card_power('molten ravager', 0).
card_toughness('molten ravager', 4).

% Found in: RAV
card_name('molten sentry', 'Molten Sentry').
card_type('molten sentry', 'Creature — Elemental').
card_types('molten sentry', ['Creature']).
card_subtypes('molten sentry', ['Elemental']).
card_colors('molten sentry', ['R']).
card_text('molten sentry', 'As Molten Sentry enters the battlefield, flip a coin. If the coin comes up heads, Molten Sentry enters the battlefield as a 5/2 creature with haste. If it comes up tails, Molten Sentry enters the battlefield as a 2/5 creature with defender.').
card_mana_cost('molten sentry', ['3', 'R']).
card_cmc('molten sentry', 4).
card_layout('molten sentry', 'normal').
card_power('molten sentry', '*').
card_toughness('molten sentry', '*').

% Found in: C13, CMD, TSP
card_name('molten slagheap', 'Molten Slagheap').
card_type('molten slagheap', 'Land').
card_types('molten slagheap', ['Land']).
card_subtypes('molten slagheap', []).
card_colors('molten slagheap', []).
card_text('molten slagheap', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Molten Slagheap.\n{1}, Remove X storage counters from Molten Slagheap: Add X mana in any combination of {B} and/or {R} to your mana pool.').
card_layout('molten slagheap', 'normal').

% Found in: ORI
card_name('molten vortex', 'Molten Vortex').
card_type('molten vortex', 'Enchantment').
card_types('molten vortex', ['Enchantment']).
card_subtypes('molten vortex', []).
card_colors('molten vortex', ['R']).
card_text('molten vortex', '{R}, Discard a land card: Molten Vortex deals 2 damage to target creature or player.').
card_mana_cost('molten vortex', ['R']).
card_cmc('molten vortex', 1).
card_layout('molten vortex', 'normal').

% Found in: SOM
card_name('molten-tail masticore', 'Molten-Tail Masticore').
card_type('molten-tail masticore', 'Artifact Creature — Masticore').
card_types('molten-tail masticore', ['Artifact', 'Creature']).
card_subtypes('molten-tail masticore', ['Masticore']).
card_colors('molten-tail masticore', []).
card_text('molten-tail masticore', 'At the beginning of your upkeep, sacrifice Molten-Tail Masticore unless you discard a card.\n{4}, Exile a creature card from your graveyard: Molten-Tail Masticore deals 4 damage to target creature or player.\n{2}: Regenerate Molten-Tail Masticore.').
card_mana_cost('molten-tail masticore', ['4']).
card_cmc('molten-tail masticore', 4).
card_layout('molten-tail masticore', 'normal').
card_power('molten-tail masticore', 4).
card_toughness('molten-tail masticore', 4).

% Found in: NPH
card_name('moltensteel dragon', 'Moltensteel Dragon').
card_type('moltensteel dragon', 'Artifact Creature — Dragon').
card_types('moltensteel dragon', ['Artifact', 'Creature']).
card_subtypes('moltensteel dragon', ['Dragon']).
card_colors('moltensteel dragon', ['R']).
card_text('moltensteel dragon', '({R/P} can be paid with either {R} or 2 life.)\nFlying\n{R/P}: Moltensteel Dragon gets +1/+0 until end of turn.').
card_mana_cost('moltensteel dragon', ['4', 'R/P', 'R/P']).
card_cmc('moltensteel dragon', 6).
card_layout('moltensteel dragon', 'normal').
card_power('moltensteel dragon', 4).
card_toughness('moltensteel dragon', 4).

% Found in: MMQ
card_name('molting harpy', 'Molting Harpy').
card_type('molting harpy', 'Creature — Harpy Mercenary').
card_types('molting harpy', ['Creature']).
card_subtypes('molting harpy', ['Harpy', 'Mercenary']).
card_colors('molting harpy', ['B']).
card_text('molting harpy', 'Flying\nAt the beginning of your upkeep, sacrifice Molting Harpy unless you pay {2}.').
card_mana_cost('molting harpy', ['B']).
card_cmc('molting harpy', 1).
card_layout('molting harpy', 'normal').
card_power('molting harpy', 2).
card_toughness('molting harpy', 1).

% Found in: SOK
card_name('molting skin', 'Molting Skin').
card_type('molting skin', 'Enchantment').
card_types('molting skin', ['Enchantment']).
card_subtypes('molting skin', []).
card_colors('molting skin', ['G']).
card_text('molting skin', 'Return Molting Skin to its owner\'s hand: Regenerate target creature.').
card_mana_cost('molting skin', ['2', 'G']).
card_cmc('molting skin', 3).
card_layout('molting skin', 'normal').

% Found in: KTK
card_name('molting snakeskin', 'Molting Snakeskin').
card_type('molting snakeskin', 'Enchantment — Aura').
card_types('molting snakeskin', ['Enchantment']).
card_subtypes('molting snakeskin', ['Aura']).
card_colors('molting snakeskin', ['B']).
card_text('molting snakeskin', 'Enchant creature\nEnchanted creature gets +2/+0 and has \"{2}{B}: Regenerate this creature.\"').
card_mana_cost('molting snakeskin', ['B']).
card_cmc('molting snakeskin', 1).
card_layout('molting snakeskin', 'normal').

% Found in: CNS, DDL, ISD
card_name('moment of heroism', 'Moment of Heroism').
card_type('moment of heroism', 'Instant').
card_types('moment of heroism', ['Instant']).
card_subtypes('moment of heroism', []).
card_colors('moment of heroism', ['W']).
card_text('moment of heroism', 'Target creature gets +2/+2 and gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('moment of heroism', ['1', 'W']).
card_cmc('moment of heroism', 2).
card_layout('moment of heroism', 'normal').

% Found in: MMQ
card_name('moment of silence', 'Moment of Silence').
card_type('moment of silence', 'Instant').
card_types('moment of silence', ['Instant']).
card_subtypes('moment of silence', []).
card_colors('moment of silence', ['W']).
card_text('moment of silence', 'Target player skips his or her next combat phase this turn.').
card_mana_cost('moment of silence', ['W']).
card_cmc('moment of silence', 1).
card_layout('moment of silence', 'normal').

% Found in: ODY
card_name('moment\'s peace', 'Moment\'s Peace').
card_type('moment\'s peace', 'Instant').
card_types('moment\'s peace', ['Instant']).
card_subtypes('moment\'s peace', []).
card_colors('moment\'s peace', ['G']).
card_text('moment\'s peace', 'Prevent all combat damage that would be dealt this turn.\nFlashback {2}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('moment\'s peace', ['1', 'G']).
card_cmc('moment\'s peace', 2).
card_layout('moment\'s peace', 'normal').

% Found in: TSP
card_name('momentary blink', 'Momentary Blink').
card_type('momentary blink', 'Instant').
card_types('momentary blink', ['Instant']).
card_subtypes('momentary blink', []).
card_colors('momentary blink', ['W']).
card_text('momentary blink', 'Exile target creature you control, then return it to the battlefield under its owner\'s control.\nFlashback {3}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('momentary blink', ['1', 'W']).
card_cmc('momentary blink', 2).
card_layout('momentary blink', 'normal').

% Found in: ROE
card_name('momentous fall', 'Momentous Fall').
card_type('momentous fall', 'Instant').
card_types('momentous fall', ['Instant']).
card_subtypes('momentous fall', []).
card_colors('momentous fall', ['G']).
card_text('momentous fall', 'As an additional cost to cast Momentous Fall, sacrifice a creature.\nYou draw cards equal to the sacrificed creature\'s power, then you gain life equal to its toughness.').
card_mana_cost('momentous fall', ['2', 'G', 'G']).
card_cmc('momentous fall', 4).
card_layout('momentous fall', 'normal').

% Found in: UDS
card_name('momentum', 'Momentum').
card_type('momentum', 'Enchantment — Aura').
card_types('momentum', ['Enchantment']).
card_subtypes('momentum', ['Aura']).
card_colors('momentum', ['G']).
card_text('momentum', 'Enchant creature\nAt the beginning of your upkeep, you may put a growth counter on Momentum.\nEnchanted creature gets +1/+1 for each growth counter on Momentum.').
card_mana_cost('momentum', ['2', 'G']).
card_cmc('momentum', 3).
card_layout('momentum', 'normal').

% Found in: DIS
card_name('momir vig, simic visionary', 'Momir Vig, Simic Visionary').
card_type('momir vig, simic visionary', 'Legendary Creature — Elf Wizard').
card_types('momir vig, simic visionary', ['Creature']).
card_subtypes('momir vig, simic visionary', ['Elf', 'Wizard']).
card_supertypes('momir vig, simic visionary', ['Legendary']).
card_colors('momir vig, simic visionary', ['U', 'G']).
card_text('momir vig, simic visionary', 'Whenever you cast a green creature spell, you may search your library for a creature card and reveal it. If you do, shuffle your library and put that card on top of it.\nWhenever you cast a blue creature spell, reveal the top card of your library. If it\'s a creature card, put that card into your hand.').
card_mana_cost('momir vig, simic visionary', ['3', 'G', 'U']).
card_cmc('momir vig, simic visionary', 5).
card_layout('momir vig, simic visionary', 'normal').
card_power('momir vig, simic visionary', 2).
card_toughness('momir vig, simic visionary', 2).

% Found in: VAN
card_name('momir vig, simic visionary avatar', 'Momir Vig, Simic Visionary Avatar').
card_type('momir vig, simic visionary avatar', 'Vanguard').
card_types('momir vig, simic visionary avatar', ['Vanguard']).
card_subtypes('momir vig, simic visionary avatar', []).
card_colors('momir vig, simic visionary avatar', []).
card_text('momir vig, simic visionary avatar', '{X}, Discard a card: Put a token onto the battlefield that\'s a copy of a creature card with converted mana cost X chosen at random. Activate this ability only any time you could cast a sorcery and only once each turn.').
card_layout('momir vig, simic visionary avatar', 'vanguard').

% Found in: KTK
card_name('monastery flock', 'Monastery Flock').
card_type('monastery flock', 'Creature — Bird').
card_types('monastery flock', ['Creature']).
card_subtypes('monastery flock', ['Bird']).
card_colors('monastery flock', ['U']).
card_text('monastery flock', 'Defender, flying\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('monastery flock', ['2', 'U']).
card_cmc('monastery flock', 3).
card_layout('monastery flock', 'normal').
card_power('monastery flock', 0).
card_toughness('monastery flock', 5).

% Found in: DTK
card_name('monastery loremaster', 'Monastery Loremaster').
card_type('monastery loremaster', 'Creature — Djinn Wizard').
card_types('monastery loremaster', ['Creature']).
card_subtypes('monastery loremaster', ['Djinn', 'Wizard']).
card_colors('monastery loremaster', ['U']).
card_text('monastery loremaster', 'Megamorph {5}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Monastery Loremaster is turned face up, return target noncreature, nonland card from your graveyard to your hand.').
card_mana_cost('monastery loremaster', ['3', 'U']).
card_cmc('monastery loremaster', 4).
card_layout('monastery loremaster', 'normal').
card_power('monastery loremaster', 3).
card_toughness('monastery loremaster', 2).

% Found in: FRF
card_name('monastery mentor', 'Monastery Mentor').
card_type('monastery mentor', 'Creature — Human Monk').
card_types('monastery mentor', ['Creature']).
card_subtypes('monastery mentor', ['Human', 'Monk']).
card_colors('monastery mentor', ['W']).
card_text('monastery mentor', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhenever you cast a noncreature spell, put a 1/1 white Monk creature token with prowess onto the battlefield.').
card_mana_cost('monastery mentor', ['2', 'W']).
card_cmc('monastery mentor', 3).
card_layout('monastery mentor', 'normal').
card_power('monastery mentor', 2).
card_toughness('monastery mentor', 2).

% Found in: FRF
card_name('monastery siege', 'Monastery Siege').
card_type('monastery siege', 'Enchantment').
card_types('monastery siege', ['Enchantment']).
card_subtypes('monastery siege', []).
card_colors('monastery siege', ['U']).
card_text('monastery siege', 'As Monastery Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of your draw step, draw an additional card, then discard a card.\n• Dragons — Spells your opponents cast that target you or a permanent you control cost {2} more to cast.').
card_mana_cost('monastery siege', ['2', 'U']).
card_cmc('monastery siege', 3).
card_layout('monastery siege', 'normal').

% Found in: KTK
card_name('monastery swiftspear', 'Monastery Swiftspear').
card_type('monastery swiftspear', 'Creature — Human Monk').
card_types('monastery swiftspear', ['Creature']).
card_subtypes('monastery swiftspear', ['Human', 'Monk']).
card_colors('monastery swiftspear', ['R']).
card_text('monastery swiftspear', 'Haste\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('monastery swiftspear', ['R']).
card_cmc('monastery swiftspear', 1).
card_layout('monastery swiftspear', 'normal').
card_power('monastery swiftspear', 1).
card_toughness('monastery swiftspear', 2).

% Found in: DKA, pLPA
card_name('mondronen shaman', 'Mondronen Shaman').
card_type('mondronen shaman', 'Creature — Human Werewolf Shaman').
card_types('mondronen shaman', ['Creature']).
card_subtypes('mondronen shaman', ['Human', 'Werewolf', 'Shaman']).
card_colors('mondronen shaman', ['R']).
card_text('mondronen shaman', 'At the beginning of each upkeep, if no spells were cast last turn, transform Mondronen Shaman.').
card_mana_cost('mondronen shaman', ['3', 'R']).
card_cmc('mondronen shaman', 4).
card_layout('mondronen shaman', 'double-faced').
card_power('mondronen shaman', 3).
card_toughness('mondronen shaman', 2).
card_sides('mondronen shaman', 'tovolar\'s magehunter').

% Found in: TMP
card_name('mongrel pack', 'Mongrel Pack').
card_type('mongrel pack', 'Creature — Hound').
card_types('mongrel pack', ['Creature']).
card_subtypes('mongrel pack', ['Hound']).
card_colors('mongrel pack', ['G']).
card_text('mongrel pack', 'When Mongrel Pack dies during combat, put four 1/1 green Hound creature tokens onto the battlefield.').
card_mana_cost('mongrel pack', ['3', 'G']).
card_cmc('mongrel pack', 4).
card_layout('mongrel pack', 'normal').
card_power('mongrel pack', 4).
card_toughness('mongrel pack', 1).

% Found in: UNH
card_name('moniker mage', 'Moniker Mage').
card_type('moniker mage', 'Creature — Human Wizard').
card_types('moniker mage', ['Creature']).
card_subtypes('moniker mage', ['Human', 'Wizard']).
card_colors('moniker mage', ['U']).
card_text('moniker mage', '{U}, Say your middle name: Moniker Mage can\'t be the target of spells or abilities this turn.\n{U}, Say an opponent\'s middle name: Moniker Mage gains flying until end of turn.').
card_mana_cost('moniker mage', ['2', 'U']).
card_cmc('moniker mage', 3).
card_layout('moniker mage', 'normal').
card_power('moniker mage', 2).
card_toughness('moniker mage', 2).

% Found in: USG
card_name('monk idealist', 'Monk Idealist').
card_type('monk idealist', 'Creature — Human Monk Cleric').
card_types('monk idealist', ['Creature']).
card_subtypes('monk idealist', ['Human', 'Monk', 'Cleric']).
card_colors('monk idealist', ['W']).
card_text('monk idealist', 'When Monk Idealist enters the battlefield, return target enchantment card from your graveyard to your hand.').
card_mana_cost('monk idealist', ['2', 'W']).
card_cmc('monk idealist', 3).
card_layout('monk idealist', 'normal').
card_power('monk idealist', 2).
card_toughness('monk idealist', 2).

% Found in: CMD, USG
card_name('monk realist', 'Monk Realist').
card_type('monk realist', 'Creature — Human Monk Cleric').
card_types('monk realist', ['Creature']).
card_subtypes('monk realist', ['Human', 'Monk', 'Cleric']).
card_colors('monk realist', ['W']).
card_text('monk realist', 'When Monk Realist enters the battlefield, destroy target enchantment.').
card_mana_cost('monk realist', ['1', 'W']).
card_cmc('monk realist', 2).
card_layout('monk realist', 'normal').
card_power('monk realist', 1).
card_toughness('monk realist', 1).

% Found in: MMQ
card_name('monkey cage', 'Monkey Cage').
card_type('monkey cage', 'Artifact').
card_types('monkey cage', ['Artifact']).
card_subtypes('monkey cage', []).
card_colors('monkey cage', []).
card_text('monkey cage', 'When a creature enters the battlefield, sacrifice Monkey Cage and put X 2/2 green Ape creature tokens onto the battlefield, where X is that creature\'s converted mana cost.').
card_mana_cost('monkey cage', ['5']).
card_cmc('monkey cage', 5).
card_layout('monkey cage', 'normal').

% Found in: UNH
card_name('monkey monkey monkey', 'Monkey Monkey Monkey').
card_type('monkey monkey monkey', 'Creature — Ape').
card_types('monkey monkey monkey', ['Creature']).
card_subtypes('monkey monkey monkey', ['Ape']).
card_colors('monkey monkey monkey', ['G']).
card_text('monkey monkey monkey', 'As Monkey Monkey Monkey comes into play, choose a letter.\nMonkey Monkey Monkey gets +1/+1 for each nonland permanent whose name begins with the chosen letter.').
card_mana_cost('monkey monkey monkey', ['3', 'G']).
card_cmc('monkey monkey monkey', 4).
card_layout('monkey monkey monkey', 'normal').
card_power('monkey monkey monkey', 1).
card_toughness('monkey monkey monkey', 1).

% Found in: M12
card_name('monomania', 'Monomania').
card_type('monomania', 'Sorcery').
card_types('monomania', ['Sorcery']).
card_subtypes('monomania', []).
card_colors('monomania', ['B']).
card_text('monomania', 'Target player chooses a card in his or her hand and discards the rest.').
card_mana_cost('monomania', ['3', 'B', 'B']).
card_cmc('monomania', 5).
card_layout('monomania', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, RQS, S00, S99
card_name('mons\'s goblin raiders', 'Mons\'s Goblin Raiders').
card_type('mons\'s goblin raiders', 'Creature — Goblin').
card_types('mons\'s goblin raiders', ['Creature']).
card_subtypes('mons\'s goblin raiders', ['Goblin']).
card_colors('mons\'s goblin raiders', ['R']).
card_text('mons\'s goblin raiders', '').
card_mana_cost('mons\'s goblin raiders', ['R']).
card_cmc('mons\'s goblin raiders', 1).
card_layout('mons\'s goblin raiders', 'normal').
card_power('mons\'s goblin raiders', 1).
card_toughness('mons\'s goblin raiders', 1).

% Found in: UNH
card_name('mons\'s goblin waiters', 'Mons\'s Goblin Waiters').
card_type('mons\'s goblin waiters', 'Creature — Goblin Waiter').
card_types('mons\'s goblin waiters', ['Creature']).
card_subtypes('mons\'s goblin waiters', ['Goblin', 'Waiter']).
card_colors('mons\'s goblin waiters', ['R']).
card_text('mons\'s goblin waiters', 'Sacrifice a creature or land: Add {hr} to your mana pool.').
card_mana_cost('mons\'s goblin waiters', ['R']).
card_cmc('mons\'s goblin waiters', 1).
card_layout('mons\'s goblin waiters', 'normal').
card_power('mons\'s goblin waiters', 1).
card_toughness('mons\'s goblin waiters', 1).

% Found in: ICE
card_name('monsoon', 'Monsoon').
card_type('monsoon', 'Enchantment').
card_types('monsoon', ['Enchantment']).
card_subtypes('monsoon', []).
card_colors('monsoon', ['R', 'G']).
card_text('monsoon', 'At the beginning of each player\'s end step, tap all untapped Islands that player controls and Monsoon deals X damage to the player, where X is the number of Islands tapped this way.').
card_mana_cost('monsoon', ['2', 'R', 'G']).
card_cmc('monsoon', 4).
card_layout('monsoon', 'normal').

% Found in: EVE
card_name('monstrify', 'Monstrify').
card_type('monstrify', 'Sorcery').
card_types('monstrify', ['Sorcery']).
card_subtypes('monstrify', []).
card_colors('monstrify', ['G']).
card_text('monstrify', 'Target creature gets +4/+4 until end of turn.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('monstrify', ['3', 'G']).
card_cmc('monstrify', 4).
card_layout('monstrify', 'normal').

% Found in: ARB
card_name('monstrous carabid', 'Monstrous Carabid').
card_type('monstrous carabid', 'Creature — Insect').
card_types('monstrous carabid', ['Creature']).
card_subtypes('monstrous carabid', ['Insect']).
card_colors('monstrous carabid', ['B', 'R']).
card_text('monstrous carabid', 'Monstrous Carabid attacks each turn if able.\nCycling {B/R} ({B/R}, Discard this card: Draw a card.)').
card_mana_cost('monstrous carabid', ['3', 'B', 'R']).
card_cmc('monstrous carabid', 5).
card_layout('monstrous carabid', 'normal').
card_power('monstrous carabid', 4).
card_toughness('monstrous carabid', 4).

% Found in: 7ED, 8ED, PO2, POR, S00, S99
card_name('monstrous growth', 'Monstrous Growth').
card_type('monstrous growth', 'Sorcery').
card_types('monstrous growth', ['Sorcery']).
card_subtypes('monstrous growth', []).
card_colors('monstrous growth', ['G']).
card_text('monstrous growth', 'Target creature gets +4/+4 until end of turn.').
card_mana_cost('monstrous growth', ['1', 'G']).
card_cmc('monstrous growth', 2).
card_layout('monstrous growth', 'normal').

% Found in: EXO, pPRE
card_name('monstrous hound', 'Monstrous Hound').
card_type('monstrous hound', 'Creature — Hound').
card_types('monstrous hound', ['Creature']).
card_subtypes('monstrous hound', ['Hound']).
card_colors('monstrous hound', ['R']).
card_text('monstrous hound', 'Monstrous Hound can\'t attack unless you control more lands than defending player.\nMonstrous Hound can\'t block unless you control more lands than attacking player.').
card_mana_cost('monstrous hound', ['3', 'R']).
card_cmc('monstrous hound', 4).
card_layout('monstrous hound', 'normal').
card_power('monstrous hound', 4).
card_toughness('monstrous hound', 4).

% Found in: ISD
card_name('moon heron', 'Moon Heron').
card_type('moon heron', 'Creature — Spirit Bird').
card_types('moon heron', ['Creature']).
card_subtypes('moon heron', ['Spirit', 'Bird']).
card_colors('moon heron', ['U']).
card_text('moon heron', 'Flying').
card_mana_cost('moon heron', ['3', 'U']).
card_cmc('moon heron', 4).
card_layout('moon heron', 'normal').
card_power('moon heron', 3).
card_toughness('moon heron', 2).

% Found in: POR, S00, S99
card_name('moon sprite', 'Moon Sprite').
card_type('moon sprite', 'Creature — Faerie').
card_types('moon sprite', ['Creature']).
card_subtypes('moon sprite', ['Faerie']).
card_colors('moon sprite', ['G']).
card_text('moon sprite', 'Flying').
card_mana_cost('moon sprite', ['1', 'G']).
card_cmc('moon sprite', 2).
card_layout('moon sprite', 'normal').
card_power('moon sprite', 1).
card_toughness('moon sprite', 1).

% Found in: SOK
card_name('moonbow illusionist', 'Moonbow Illusionist').
card_type('moonbow illusionist', 'Creature — Moonfolk Wizard').
card_types('moonbow illusionist', ['Creature']).
card_subtypes('moonbow illusionist', ['Moonfolk', 'Wizard']).
card_colors('moonbow illusionist', ['U']).
card_text('moonbow illusionist', 'Flying\n{2}, Return a land you control to its owner\'s hand: Target land becomes the basic land type of your choice until end of turn.').
card_mana_cost('moonbow illusionist', ['2', 'U']).
card_cmc('moonbow illusionist', 3).
card_layout('moonbow illusionist', 'normal').
card_power('moonbow illusionist', 2).
card_toughness('moonbow illusionist', 1).

% Found in: MOR
card_name('moonglove changeling', 'Moonglove Changeling').
card_type('moonglove changeling', 'Creature — Shapeshifter').
card_types('moonglove changeling', ['Creature']).
card_subtypes('moonglove changeling', ['Shapeshifter']).
card_colors('moonglove changeling', ['B']).
card_text('moonglove changeling', 'Changeling (This card is every creature type.)\n{B}: Moonglove Changeling gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy that creature.)').
card_mana_cost('moonglove changeling', ['2', 'B']).
card_cmc('moonglove changeling', 3).
card_layout('moonglove changeling', 'normal').
card_power('moonglove changeling', 2).
card_toughness('moonglove changeling', 2).

% Found in: DD3_EVG, DDF, EVG, LRW
card_name('moonglove extract', 'Moonglove Extract').
card_type('moonglove extract', 'Artifact').
card_types('moonglove extract', ['Artifact']).
card_subtypes('moonglove extract', []).
card_colors('moonglove extract', []).
card_text('moonglove extract', 'Sacrifice Moonglove Extract: Moonglove Extract deals 2 damage to target creature or player.').
card_mana_cost('moonglove extract', ['3']).
card_cmc('moonglove extract', 3).
card_layout('moonglove extract', 'normal').

% Found in: DPA, LRW
card_name('moonglove winnower', 'Moonglove Winnower').
card_type('moonglove winnower', 'Creature — Elf Rogue').
card_types('moonglove winnower', ['Creature']).
card_subtypes('moonglove winnower', ['Elf', 'Rogue']).
card_colors('moonglove winnower', ['B']).
card_text('moonglove winnower', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('moonglove winnower', ['3', 'B']).
card_cmc('moonglove winnower', 4).
card_layout('moonglove winnower', 'normal').
card_power('moonglove winnower', 2).
card_toughness('moonglove winnower', 3).

% Found in: EVE
card_name('moonhold', 'Moonhold').
card_type('moonhold', 'Instant').
card_types('moonhold', ['Instant']).
card_subtypes('moonhold', []).
card_colors('moonhold', ['W', 'R']).
card_text('moonhold', 'Target player can\'t play land cards this turn if {R} was spent to cast Moonhold and can\'t play creature cards this turn if {W} was spent to cast it. (Do both if {R}{W} was spent.)').
card_mana_cost('moonhold', ['2', 'R/W']).
card_cmc('moonhold', 3).
card_layout('moonhold', 'normal').

% Found in: TSP
card_name('moonlace', 'Moonlace').
card_type('moonlace', 'Instant').
card_types('moonlace', ['Instant']).
card_subtypes('moonlace', []).
card_colors('moonlace', ['U']).
card_text('moonlace', 'Target spell or permanent becomes colorless.').
card_mana_cost('moonlace', ['U']).
card_cmc('moonlace', 1).
card_layout('moonlace', 'normal').

% Found in: RAV
card_name('moonlight bargain', 'Moonlight Bargain').
card_type('moonlight bargain', 'Instant').
card_types('moonlight bargain', ['Instant']).
card_subtypes('moonlight bargain', []).
card_colors('moonlight bargain', ['B']).
card_text('moonlight bargain', 'Look at the top five cards of your library. For each card, put that card into your graveyard unless you pay 2 life. Then put the rest into your hand.').
card_mana_cost('moonlight bargain', ['3', 'B', 'B']).
card_cmc('moonlight bargain', 5).
card_layout('moonlight bargain', 'normal').

% Found in: AVR
card_name('moonlight geist', 'Moonlight Geist').
card_type('moonlight geist', 'Creature — Spirit').
card_types('moonlight geist', ['Creature']).
card_subtypes('moonlight geist', ['Spirit']).
card_colors('moonlight geist', ['W']).
card_text('moonlight geist', 'Flying\n{3}{W}: Prevent all combat damage that would be dealt to and dealt by Moonlight Geist this turn.').
card_mana_cost('moonlight geist', ['2', 'W']).
card_cmc('moonlight geist', 3).
card_layout('moonlight geist', 'normal').
card_power('moonlight geist', 2).
card_toughness('moonlight geist', 1).

% Found in: BOK, MM2
card_name('moonlit strider', 'Moonlit Strider').
card_type('moonlit strider', 'Creature — Spirit').
card_types('moonlit strider', ['Creature']).
card_subtypes('moonlit strider', ['Spirit']).
card_colors('moonlit strider', ['W']).
card_text('moonlit strider', 'Sacrifice Moonlit Strider: Target creature you control gains protection from the color of your choice until end of turn.\nSoulshift 3 (When this creature dies, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_mana_cost('moonlit strider', ['3', 'W']).
card_cmc('moonlit strider', 4).
card_layout('moonlit strider', 'normal').
card_power('moonlit strider', 1).
card_toughness('moonlit strider', 4).

% Found in: MMQ
card_name('moonlit wake', 'Moonlit Wake').
card_type('moonlit wake', 'Enchantment').
card_types('moonlit wake', ['Enchantment']).
card_subtypes('moonlit wake', []).
card_colors('moonlit wake', ['W']).
card_text('moonlit wake', 'Whenever a creature dies, you gain 1 life.').
card_mana_cost('moonlit wake', ['2', 'W']).
card_cmc('moonlit wake', 3).
card_layout('moonlit wake', 'normal').

% Found in: ISD
card_name('moonmist', 'Moonmist').
card_type('moonmist', 'Instant').
card_types('moonmist', ['Instant']).
card_subtypes('moonmist', []).
card_colors('moonmist', ['G']).
card_text('moonmist', 'Transform all Humans. Prevent all combat damage that would be dealt this turn by creatures other than Werewolves and Wolves. (Only double-faced cards can be transformed.)').
card_mana_cost('moonmist', ['1', 'G']).
card_cmc('moonmist', 2).
card_layout('moonmist', 'normal').

% Found in: SHM
card_name('moonring island', 'Moonring Island').
card_type('moonring island', 'Land — Island').
card_types('moonring island', ['Land']).
card_subtypes('moonring island', ['Island']).
card_colors('moonring island', []).
card_text('moonring island', '({T}: Add {U} to your mana pool.)\nMoonring Island enters the battlefield tapped.\n{U}, {T}: Look at the top card of target player\'s library. Activate this ability only if you control two or more blue permanents.').
card_layout('moonring island', 'normal').

% Found in: CHK
card_name('moonring mirror', 'Moonring Mirror').
card_type('moonring mirror', 'Artifact').
card_types('moonring mirror', ['Artifact']).
card_subtypes('moonring mirror', []).
card_colors('moonring mirror', []).
card_text('moonring mirror', 'Whenever you draw a card, exile the top card of your library face down.\nAt the beginning of your upkeep, you may exile all cards from your hand face down. If you do, put all other cards you own exiled with Moonring Mirror into your hand.').
card_mana_cost('moonring mirror', ['5']).
card_cmc('moonring mirror', 5).
card_layout('moonring mirror', 'normal').

% Found in: DKA
card_name('moonscarred werewolf', 'Moonscarred Werewolf').
card_type('moonscarred werewolf', 'Creature — Werewolf').
card_types('moonscarred werewolf', ['Creature']).
card_subtypes('moonscarred werewolf', ['Werewolf']).
card_colors('moonscarred werewolf', ['G']).
card_text('moonscarred werewolf', 'Vigilance\n{T}: Add {G}{G} to your mana pool.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Moonscarred Werewolf.').
card_layout('moonscarred werewolf', 'double-faced').
card_power('moonscarred werewolf', 2).
card_toughness('moonscarred werewolf', 2).

% Found in: AVR, C14, pPRE
card_name('moonsilver spear', 'Moonsilver Spear').
card_type('moonsilver spear', 'Artifact — Equipment').
card_types('moonsilver spear', ['Artifact']).
card_subtypes('moonsilver spear', ['Equipment']).
card_colors('moonsilver spear', []).
card_text('moonsilver spear', 'Equipped creature has first strike.\nWhenever equipped creature attacks, put a 4/4 white Angel creature token with flying onto the battlefield.\nEquip {4}').
card_mana_cost('moonsilver spear', ['4']).
card_cmc('moonsilver spear', 4).
card_layout('moonsilver spear', 'normal').

% Found in: DKA
card_name('moonveil dragon', 'Moonveil Dragon').
card_type('moonveil dragon', 'Creature — Dragon').
card_types('moonveil dragon', ['Creature']).
card_subtypes('moonveil dragon', ['Dragon']).
card_colors('moonveil dragon', ['R']).
card_text('moonveil dragon', 'Flying\n{R}: Each creature you control gets +1/+0 until end of turn.').
card_mana_cost('moonveil dragon', ['3', 'R', 'R', 'R']).
card_cmc('moonveil dragon', 6).
card_layout('moonveil dragon', 'normal').
card_power('moonveil dragon', 5).
card_toughness('moonveil dragon', 5).

% Found in: SOK
card_name('moonwing moth', 'Moonwing Moth').
card_type('moonwing moth', 'Creature — Insect').
card_types('moonwing moth', ['Creature']).
card_subtypes('moonwing moth', ['Insect']).
card_colors('moonwing moth', ['W']).
card_text('moonwing moth', 'Flying\n{W}: Moonwing Moth gets +0/+1 until end of turn.').
card_mana_cost('moonwing moth', ['1', 'W', 'W']).
card_cmc('moonwing moth', 3).
card_layout('moonwing moth', 'normal').
card_power('moonwing moth', 2).
card_toughness('moonwing moth', 1).

% Found in: ICE
card_name('moor fiend', 'Moor Fiend').
card_type('moor fiend', 'Creature — Horror').
card_types('moor fiend', ['Creature']).
card_subtypes('moor fiend', ['Horror']).
card_colors('moor fiend', ['B']).
card_text('moor fiend', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('moor fiend', ['3', 'B']).
card_cmc('moor fiend', 4).
card_layout('moor fiend', 'normal').
card_power('moor fiend', 3).
card_toughness('moor fiend', 3).

% Found in: ARN, TSB
card_name('moorish cavalry', 'Moorish Cavalry').
card_type('moorish cavalry', 'Creature — Human Knight').
card_types('moorish cavalry', ['Creature']).
card_subtypes('moorish cavalry', ['Human', 'Knight']).
card_colors('moorish cavalry', ['W']).
card_text('moorish cavalry', 'Trample').
card_mana_cost('moorish cavalry', ['2', 'W', 'W']).
card_cmc('moorish cavalry', 4).
card_layout('moorish cavalry', 'normal').
card_power('moorish cavalry', 3).
card_toughness('moorish cavalry', 3).

% Found in: ISD
card_name('moorland haunt', 'Moorland Haunt').
card_type('moorland haunt', 'Land').
card_types('moorland haunt', ['Land']).
card_subtypes('moorland haunt', []).
card_colors('moorland haunt', []).
card_text('moorland haunt', '{T}: Add {1} to your mana pool.\n{W}{U}, {T}, Exile a creature card from your graveyard: Put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_layout('moorland haunt', 'normal').

% Found in: AVR
card_name('moorland inquisitor', 'Moorland Inquisitor').
card_type('moorland inquisitor', 'Creature — Human Soldier').
card_types('moorland inquisitor', ['Creature']).
card_subtypes('moorland inquisitor', ['Human', 'Soldier']).
card_colors('moorland inquisitor', ['W']).
card_text('moorland inquisitor', '{2}{W}: Moorland Inquisitor gains first strike until end of turn.').
card_mana_cost('moorland inquisitor', ['1', 'W']).
card_cmc('moorland inquisitor', 2).
card_layout('moorland inquisitor', 'normal').
card_power('moorland inquisitor', 2).
card_toughness('moorland inquisitor', 2).

% Found in: 4ED, DRK
card_name('morale', 'Morale').
card_type('morale', 'Instant').
card_types('morale', ['Instant']).
card_subtypes('morale', []).
card_colors('morale', ['W']).
card_text('morale', 'Attacking creatures get +1/+1 until end of turn.').
card_mana_cost('morale', ['1', 'W', 'W']).
card_cmc('morale', 3).
card_layout('morale', 'normal').

% Found in: JUD
card_name('morality shift', 'Morality Shift').
card_type('morality shift', 'Sorcery').
card_types('morality shift', ['Sorcery']).
card_subtypes('morality shift', []).
card_colors('morality shift', ['B']).
card_text('morality shift', 'Exchange your graveyard and library. Then shuffle your library.').
card_mana_cost('morality shift', ['5', 'B', 'B']).
card_cmc('morality shift', 7).
card_layout('morality shift', 'normal').

% Found in: GPT
card_name('moratorium stone', 'Moratorium Stone').
card_type('moratorium stone', 'Artifact').
card_types('moratorium stone', ['Artifact']).
card_subtypes('moratorium stone', []).
card_colors('moratorium stone', []).
card_text('moratorium stone', '{2}, {T}: Exile target card from a graveyard.\n{2}{W}{B}, {T}, Sacrifice Moratorium Stone: Exile target nonland card from a graveyard, all other cards from graveyards with the same name as that card, and all permanents with that name.').
card_mana_cost('moratorium stone', ['1']).
card_cmc('moratorium stone', 1).
card_layout('moratorium stone', 'normal').

% Found in: ARB
card_name('morbid bloom', 'Morbid Bloom').
card_type('morbid bloom', 'Sorcery').
card_types('morbid bloom', ['Sorcery']).
card_subtypes('morbid bloom', []).
card_colors('morbid bloom', ['B', 'G']).
card_text('morbid bloom', 'Exile target creature card from a graveyard, then put X 1/1 green Saproling creature tokens onto the battlefield, where X is the exiled card\'s toughness.').
card_mana_cost('morbid bloom', ['4', 'B', 'G']).
card_cmc('morbid bloom', 6).
card_layout('morbid bloom', 'normal').

% Found in: ODY
card_name('morbid hunger', 'Morbid Hunger').
card_type('morbid hunger', 'Sorcery').
card_types('morbid hunger', ['Sorcery']).
card_subtypes('morbid hunger', []).
card_colors('morbid hunger', ['B']).
card_text('morbid hunger', 'Morbid Hunger deals 3 damage to target creature or player. You gain 3 life.\nFlashback {7}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('morbid hunger', ['4', 'B', 'B']).
card_cmc('morbid hunger', 6).
card_layout('morbid hunger', 'normal').

% Found in: MBS
card_name('morbid plunder', 'Morbid Plunder').
card_type('morbid plunder', 'Sorcery').
card_types('morbid plunder', ['Sorcery']).
card_subtypes('morbid plunder', []).
card_colors('morbid plunder', ['B']).
card_text('morbid plunder', 'Return up to two target creature cards from your graveyard to your hand.').
card_mana_cost('morbid plunder', ['1', 'B', 'B']).
card_cmc('morbid plunder', 3).
card_layout('morbid plunder', 'normal').

% Found in: DDG, WWK
card_name('mordant dragon', 'Mordant Dragon').
card_type('mordant dragon', 'Creature — Dragon').
card_types('mordant dragon', ['Creature']).
card_subtypes('mordant dragon', ['Dragon']).
card_colors('mordant dragon', ['R']).
card_text('mordant dragon', 'Flying\n{1}{R}: Mordant Dragon gets +1/+0 until end of turn.\nWhenever Mordant Dragon deals combat damage to a player, you may have it deal that much damage to target creature that player controls.').
card_mana_cost('mordant dragon', ['3', 'R', 'R', 'R']).
card_cmc('mordant dragon', 6).
card_layout('mordant dragon', 'normal').
card_power('mordant dragon', 5).
card_toughness('mordant dragon', 5).

% Found in: DGM
card_name('morgue burst', 'Morgue Burst').
card_type('morgue burst', 'Sorcery').
card_types('morgue burst', ['Sorcery']).
card_subtypes('morgue burst', []).
card_colors('morgue burst', ['B', 'R']).
card_text('morgue burst', 'Return target creature card from your graveyard to your hand. Morgue Burst deals damage to target creature or player equal to the power of the card returned this way.').
card_mana_cost('morgue burst', ['4', 'B', 'R']).
card_cmc('morgue burst', 6).
card_layout('morgue burst', 'normal').

% Found in: ODY
card_name('morgue theft', 'Morgue Theft').
card_type('morgue theft', 'Sorcery').
card_types('morgue theft', ['Sorcery']).
card_subtypes('morgue theft', []).
card_colors('morgue theft', ['B']).
card_text('morgue theft', 'Return target creature card from your graveyard to your hand.\nFlashback {4}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('morgue theft', ['1', 'B']).
card_cmc('morgue theft', 2).
card_layout('morgue theft', 'normal').

% Found in: STH
card_name('morgue thrull', 'Morgue Thrull').
card_type('morgue thrull', 'Creature — Thrull').
card_types('morgue thrull', ['Creature']).
card_subtypes('morgue thrull', ['Thrull']).
card_colors('morgue thrull', ['B']).
card_text('morgue thrull', 'Sacrifice Morgue Thrull: Put the top three cards of your library into your graveyard.').
card_mana_cost('morgue thrull', ['2', 'B']).
card_cmc('morgue thrull', 3).
card_layout('morgue thrull', 'normal').
card_power('morgue thrull', 2).
card_toughness('morgue thrull', 2).

% Found in: DDH, PLS
card_name('morgue toad', 'Morgue Toad').
card_type('morgue toad', 'Creature — Frog').
card_types('morgue toad', ['Creature']).
card_subtypes('morgue toad', ['Frog']).
card_colors('morgue toad', ['B']).
card_text('morgue toad', 'Sacrifice Morgue Toad: Add {U}{R} to your mana pool.').
card_mana_cost('morgue toad', ['2', 'B']).
card_cmc('morgue toad', 3).
card_layout('morgue toad', 'normal').
card_power('morgue toad', 2).
card_toughness('morgue toad', 2).

% Found in: WTH
card_name('morinfen', 'Morinfen').
card_type('morinfen', 'Legendary Creature — Horror').
card_types('morinfen', ['Creature']).
card_subtypes('morinfen', ['Horror']).
card_supertypes('morinfen', ['Legendary']).
card_colors('morinfen', ['B']).
card_text('morinfen', 'Flying\nCumulative upkeep—Pay 1 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('morinfen', ['3', 'B', 'B']).
card_cmc('morinfen', 5).
card_layout('morinfen', 'normal').
card_power('morinfen', 5).
card_toughness('morinfen', 4).
card_reserved('morinfen').

% Found in: VAN
card_name('morinfen avatar', 'Morinfen Avatar').
card_type('morinfen avatar', 'Vanguard').
card_types('morinfen avatar', ['Vanguard']).
card_subtypes('morinfen avatar', []).
card_colors('morinfen avatar', []).
card_text('morinfen avatar', 'At the beginning of your upkeep, you lose 1 life for each permanent you control.').
card_layout('morinfen avatar', 'vanguard').

% Found in: SOM
card_name('moriok reaver', 'Moriok Reaver').
card_type('moriok reaver', 'Creature — Human Warrior').
card_types('moriok reaver', ['Creature']).
card_subtypes('moriok reaver', ['Human', 'Warrior']).
card_colors('moriok reaver', ['B']).
card_text('moriok reaver', '').
card_mana_cost('moriok reaver', ['2', 'B']).
card_cmc('moriok reaver', 3).
card_layout('moriok reaver', 'normal').
card_power('moriok reaver', 3).
card_toughness('moriok reaver', 2).

% Found in: SOM
card_name('moriok replica', 'Moriok Replica').
card_type('moriok replica', 'Artifact Creature — Warrior').
card_types('moriok replica', ['Artifact', 'Creature']).
card_subtypes('moriok replica', ['Warrior']).
card_colors('moriok replica', []).
card_text('moriok replica', '{1}{B}, Sacrifice Moriok Replica: You draw two cards and you lose 2 life.').
card_mana_cost('moriok replica', ['3']).
card_cmc('moriok replica', 3).
card_layout('moriok replica', 'normal').
card_power('moriok replica', 2).
card_toughness('moriok replica', 2).

% Found in: 5DN
card_name('moriok rigger', 'Moriok Rigger').
card_type('moriok rigger', 'Creature — Human Rogue Rigger').
card_types('moriok rigger', ['Creature']).
card_subtypes('moriok rigger', ['Human', 'Rogue', 'Rigger']).
card_colors('moriok rigger', ['B']).
card_text('moriok rigger', 'Whenever an artifact is put into a graveyard from the battlefield, you may put a +1/+1 counter on Moriok Rigger.').
card_mana_cost('moriok rigger', ['2', 'B']).
card_cmc('moriok rigger', 3).
card_layout('moriok rigger', 'normal').
card_power('moriok rigger', 2).
card_toughness('moriok rigger', 2).

% Found in: MRD
card_name('moriok scavenger', 'Moriok Scavenger').
card_type('moriok scavenger', 'Creature — Human Rogue').
card_types('moriok scavenger', ['Creature']).
card_subtypes('moriok scavenger', ['Human', 'Rogue']).
card_colors('moriok scavenger', ['B']).
card_text('moriok scavenger', 'When Moriok Scavenger enters the battlefield, you may return target artifact creature card from your graveyard to your hand.').
card_mana_cost('moriok scavenger', ['3', 'B']).
card_cmc('moriok scavenger', 4).
card_layout('moriok scavenger', 'normal').
card_power('moriok scavenger', 2).
card_toughness('moriok scavenger', 3).

% Found in: C14, CNS, ISD
card_name('morkrut banshee', 'Morkrut Banshee').
card_type('morkrut banshee', 'Creature — Spirit').
card_types('morkrut banshee', ['Creature']).
card_subtypes('morkrut banshee', ['Spirit']).
card_colors('morkrut banshee', ['B']).
card_text('morkrut banshee', 'Morbid — When Morkrut Banshee enters the battlefield, if a creature died this turn, target creature gets -4/-4 until end of turn.').
card_mana_cost('morkrut banshee', ['3', 'B', 'B']).
card_cmc('morkrut banshee', 5).
card_layout('morkrut banshee', 'normal').
card_power('morkrut banshee', 4).
card_toughness('morkrut banshee', 4).

% Found in: TOR
card_name('morningtide', 'Morningtide').
card_type('morningtide', 'Sorcery').
card_types('morningtide', ['Sorcery']).
card_subtypes('morningtide', []).
card_colors('morningtide', ['W']).
card_text('morningtide', 'Exile all cards from all graveyards.').
card_mana_cost('morningtide', ['1', 'W']).
card_cmc('morningtide', 2).
card_layout('morningtide', 'normal').

% Found in: DDH, RAV
card_name('moroii', 'Moroii').
card_type('moroii', 'Creature — Vampire').
card_types('moroii', ['Creature']).
card_subtypes('moroii', ['Vampire']).
card_colors('moroii', ['U', 'B']).
card_text('moroii', 'Flying\nAt the beginning of your upkeep, you lose 1 life.').
card_mana_cost('moroii', ['2', 'U', 'B']).
card_cmc('moroii', 4).
card_layout('moroii', 'normal').
card_power('moroii', 4).
card_toughness('moroii', 4).

% Found in: PC2
card_name('morphic tide', 'Morphic Tide').
card_type('morphic tide', 'Phenomenon').
card_types('morphic tide', ['Phenomenon']).
card_subtypes('morphic tide', []).
card_colors('morphic tide', []).
card_text('morphic tide', 'When you encounter Morphic Tide, each player shuffles all permanents he or she owns into his or her library, then reveals that many cards from the top of his or her library. Each player puts all artifact, creature, land, and planeswalker cards revealed this way onto the battlefield, then does the same for enchantment cards, then puts all cards revealed this way that weren\'t put onto the battlefield on the bottom of his or her library in any order. (Then planeswalk away from this phenomenon.)').
card_layout('morphic tide', 'phenomenon').

% Found in: USG, VMA, pJGP
card_name('morphling', 'Morphling').
card_type('morphling', 'Creature — Shapeshifter').
card_types('morphling', ['Creature']).
card_subtypes('morphling', ['Shapeshifter']).
card_colors('morphling', ['U']).
card_text('morphling', '{U}: Untap Morphling.\n{U}: Morphling gains flying until end of turn.\n{U}: Morphling gains shroud until end of turn. (It can\'t be the target of spells or abilities.)\n{1}: Morphling gets +1/-1 until end of turn.\n{1}: Morphling gets -1/+1 until end of turn.').
card_mana_cost('morphling', ['3', 'U', 'U']).
card_cmc('morphling', 5).
card_layout('morphling', 'normal').
card_power('morphling', 3).
card_toughness('morphling', 3).
card_reserved('morphling').

% Found in: MOR
card_name('morsel theft', 'Morsel Theft').
card_type('morsel theft', 'Tribal Sorcery — Rogue').
card_types('morsel theft', ['Tribal', 'Sorcery']).
card_subtypes('morsel theft', ['Rogue']).
card_colors('morsel theft', ['B']).
card_text('morsel theft', 'Prowl {1}{B} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nTarget player loses 3 life and you gain 3 life. If Morsel Theft\'s prowl cost was paid, draw a card.').
card_mana_cost('morsel theft', ['2', 'B', 'B']).
card_cmc('morsel theft', 4).
card_layout('morsel theft', 'normal').

% Found in: SHM
card_name('morselhoarder', 'Morselhoarder').
card_type('morselhoarder', 'Creature — Elemental').
card_types('morselhoarder', ['Creature']).
card_subtypes('morselhoarder', ['Elemental']).
card_colors('morselhoarder', ['R', 'G']).
card_text('morselhoarder', 'Morselhoarder enters the battlefield with two -1/-1 counters on it.\nRemove a -1/-1 counter from Morselhoarder: Add one mana of any color to your mana pool.').
card_mana_cost('morselhoarder', ['4', 'R/G', 'R/G']).
card_cmc('morselhoarder', 6).
card_layout('morselhoarder', 'normal').
card_power('morselhoarder', 6).
card_toughness('morselhoarder', 4).

% Found in: 10E, TOR
card_name('mortal combat', 'Mortal Combat').
card_type('mortal combat', 'Enchantment').
card_types('mortal combat', ['Enchantment']).
card_subtypes('mortal combat', []).
card_colors('mortal combat', ['B']).
card_text('mortal combat', 'At the beginning of your upkeep, if twenty or more creature cards are in your graveyard, you win the game.').
card_mana_cost('mortal combat', ['2', 'B', 'B']).
card_cmc('mortal combat', 4).
card_layout('mortal combat', 'normal').

% Found in: ARC
card_name('mortal flesh is weak', 'Mortal Flesh Is Weak').
card_type('mortal flesh is weak', 'Scheme').
card_types('mortal flesh is weak', ['Scheme']).
card_subtypes('mortal flesh is weak', []).
card_colors('mortal flesh is weak', []).
card_text('mortal flesh is weak', 'When you set this scheme in motion, each opponent\'s life total becomes the lowest life total among your opponents.').
card_layout('mortal flesh is weak', 'scheme').

% Found in: JOU
card_name('mortal obstinacy', 'Mortal Obstinacy').
card_type('mortal obstinacy', 'Enchantment — Aura').
card_types('mortal obstinacy', ['Enchantment']).
card_subtypes('mortal obstinacy', ['Aura']).
card_colors('mortal obstinacy', ['W']).
card_text('mortal obstinacy', 'Enchant creature you control\nEnchanted creature gets +1/+1.\nWhenever enchanted creature deals combat damage to a player, you may sacrifice Mortal Obstinacy. If you do, destroy target enchantment.').
card_mana_cost('mortal obstinacy', ['W']).
card_cmc('mortal obstinacy', 1).
card_layout('mortal obstinacy', 'normal').

% Found in: VIS
card_name('mortal wound', 'Mortal Wound').
card_type('mortal wound', 'Enchantment — Aura').
card_types('mortal wound', ['Enchantment']).
card_subtypes('mortal wound', ['Aura']).
card_colors('mortal wound', ['G']).
card_text('mortal wound', 'Enchant creature\nWhen enchanted creature is dealt damage, destroy it.').
card_mana_cost('mortal wound', ['G']).
card_cmc('mortal wound', 1).
card_layout('mortal wound', 'normal').

% Found in: BNG, DDO
card_name('mortal\'s ardor', 'Mortal\'s Ardor').
card_type('mortal\'s ardor', 'Instant').
card_types('mortal\'s ardor', ['Instant']).
card_subtypes('mortal\'s ardor', []).
card_colors('mortal\'s ardor', ['W']).
card_text('mortal\'s ardor', 'Target creature gets +1/+1 and gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('mortal\'s ardor', ['W']).
card_cmc('mortal\'s ardor', 1).
card_layout('mortal\'s ardor', 'normal').

% Found in: BNG
card_name('mortal\'s resolve', 'Mortal\'s Resolve').
card_type('mortal\'s resolve', 'Instant').
card_types('mortal\'s resolve', ['Instant']).
card_subtypes('mortal\'s resolve', []).
card_colors('mortal\'s resolve', ['G']).
card_text('mortal\'s resolve', 'Target creature gets +1/+1 and gains indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy it.)').
card_mana_cost('mortal\'s resolve', ['1', 'G']).
card_cmc('mortal\'s resolve', 2).
card_layout('mortal\'s resolve', 'normal').

% Found in: MBS, MM2
card_name('mortarpod', 'Mortarpod').
card_type('mortarpod', 'Artifact — Equipment').
card_types('mortarpod', ['Artifact']).
card_subtypes('mortarpod', ['Equipment']).
card_colors('mortarpod', []).
card_text('mortarpod', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +0/+1 and has \"Sacrifice this creature: This creature deals 1 damage to target creature or player.\"\nEquip {2}').
card_mana_cost('mortarpod', ['2']).
card_cmc('mortarpod', 2).
card_layout('mortarpod', 'normal').

% Found in: ROE
card_name('mortician beetle', 'Mortician Beetle').
card_type('mortician beetle', 'Creature — Insect').
card_types('mortician beetle', ['Creature']).
card_subtypes('mortician beetle', ['Insect']).
card_colors('mortician beetle', ['B']).
card_text('mortician beetle', 'Whenever a player sacrifices a creature, you may put a +1/+1 counter on Mortician Beetle.').
card_mana_cost('mortician beetle', ['B']).
card_cmc('mortician beetle', 1).
card_layout('mortician beetle', 'normal').
card_power('mortician beetle', 1).
card_toughness('mortician beetle', 1).

% Found in: CMD, CNS, DDK, GPT, pMPR
card_name('mortify', 'Mortify').
card_type('mortify', 'Instant').
card_types('mortify', ['Instant']).
card_subtypes('mortify', []).
card_colors('mortify', ['W', 'B']).
card_text('mortify', 'Destroy target creature or enchantment.').
card_mana_cost('mortify', ['1', 'W', 'B']).
card_cmc('mortify', 3).
card_layout('mortify', 'normal').

% Found in: RAV
card_name('mortipede', 'Mortipede').
card_type('mortipede', 'Creature — Insect').
card_types('mortipede', ['Creature']).
card_subtypes('mortipede', ['Insect']).
card_colors('mortipede', ['B']).
card_text('mortipede', '{2}{G}: All creatures able to block Mortipede this turn do so.').
card_mana_cost('mortipede', ['3', 'B']).
card_cmc('mortipede', 4).
card_layout('mortipede', 'normal').
card_power('mortipede', 4).
card_toughness('mortipede', 1).

% Found in: TOR
card_name('mortiphobia', 'Mortiphobia').
card_type('mortiphobia', 'Enchantment').
card_types('mortiphobia', ['Enchantment']).
card_subtypes('mortiphobia', []).
card_colors('mortiphobia', ['B']).
card_text('mortiphobia', '{1}{B}, Discard a card: Exile target card from a graveyard.\n{1}{B}, Sacrifice Mortiphobia: Exile target card from a graveyard.').
card_mana_cost('mortiphobia', ['1', 'B', 'B']).
card_cmc('mortiphobia', 3).
card_layout('mortiphobia', 'normal').

% Found in: NPH
card_name('mortis dogs', 'Mortis Dogs').
card_type('mortis dogs', 'Creature — Hound').
card_types('mortis dogs', ['Creature']).
card_subtypes('mortis dogs', ['Hound']).
card_colors('mortis dogs', ['B']).
card_text('mortis dogs', 'Whenever Mortis Dogs attacks, it gets +2/+0 until end of turn.\nWhen Mortis Dogs dies, target player loses life equal to its power.').
card_mana_cost('mortis dogs', ['3', 'B']).
card_cmc('mortis dogs', 4).
card_layout('mortis dogs', 'normal').
card_power('mortis dogs', 2).
card_toughness('mortis dogs', 2).

% Found in: 10E, 9ED, CMD, DPA, ODY
card_name('mortivore', 'Mortivore').
card_type('mortivore', 'Creature — Lhurgoyf').
card_types('mortivore', ['Creature']).
card_subtypes('mortivore', ['Lhurgoyf']).
card_colors('mortivore', ['B']).
card_text('mortivore', 'Mortivore\'s power and toughness are each equal to the number of creature cards in all graveyards.\n{B}: Regenerate Mortivore.').
card_mana_cost('mortivore', ['2', 'B', 'B']).
card_cmc('mortivore', 4).
card_layout('mortivore', 'normal').
card_power('mortivore', '*').
card_toughness('mortivore', '*').

% Found in: STH
card_name('mortuary', 'Mortuary').
card_type('mortuary', 'Enchantment').
card_types('mortuary', ['Enchantment']).
card_subtypes('mortuary', []).
card_colors('mortuary', ['B']).
card_text('mortuary', 'Whenever a creature is put into your graveyard from the battlefield, put that card on top of your library.').
card_mana_cost('mortuary', ['3', 'B']).
card_cmc('mortuary', 4).
card_layout('mortuary', 'normal').

% Found in: BFZ
card_name('mortuary mire', 'Mortuary Mire').
card_type('mortuary mire', 'Land').
card_types('mortuary mire', ['Land']).
card_subtypes('mortuary mire', []).
card_colors('mortuary mire', []).
card_text('mortuary mire', 'Mortuary Mire enters the battlefield tapped.\nWhen Mortuary Mire enters the battlefield, you may put target creature card from your graveyard on top of your library.\n{T}: Add {B} to your mana pool.').
card_layout('mortuary mire', 'normal').

% Found in: GTC
card_name('mortus strider', 'Mortus Strider').
card_type('mortus strider', 'Creature — Skeleton').
card_types('mortus strider', ['Creature']).
card_subtypes('mortus strider', ['Skeleton']).
card_colors('mortus strider', ['U', 'B']).
card_text('mortus strider', 'When Mortus Strider dies, return it to its owner\'s hand.').
card_mana_cost('mortus strider', ['1', 'U', 'B']).
card_cmc('mortus strider', 3).
card_layout('mortus strider', 'normal').
card_power('mortus strider', 1).
card_toughness('mortus strider', 1).

% Found in: DDF, MOR
card_name('mosquito guard', 'Mosquito Guard').
card_type('mosquito guard', 'Creature — Kithkin Soldier').
card_types('mosquito guard', ['Creature']).
card_subtypes('mosquito guard', ['Kithkin', 'Soldier']).
card_colors('mosquito guard', ['W']).
card_text('mosquito guard', 'First strike\nReinforce 1—{1}{W} ({1}{W}, Discard this card: Put a +1/+1 counter on target creature.)').
card_mana_cost('mosquito guard', ['W']).
card_cmc('mosquito guard', 1).
card_layout('mosquito guard', 'normal').
card_power('mosquito guard', 1).
card_toughness('mosquito guard', 1).

% Found in: 6ED, 7ED, C14, MIR
card_name('moss diamond', 'Moss Diamond').
card_type('moss diamond', 'Artifact').
card_types('moss diamond', ['Artifact']).
card_subtypes('moss diamond', []).
card_colors('moss diamond', []).
card_text('moss diamond', 'Moss Diamond enters the battlefield tapped.\n{T}: Add {G} to your mana pool.').
card_mana_cost('moss diamond', ['2']).
card_cmc('moss diamond', 2).
card_layout('moss diamond', 'normal').

% Found in: CHK
card_name('moss kami', 'Moss Kami').
card_type('moss kami', 'Creature — Spirit').
card_types('moss kami', ['Creature']).
card_subtypes('moss kami', ['Spirit']).
card_colors('moss kami', ['G']).
card_text('moss kami', 'Trample').
card_mana_cost('moss kami', ['5', 'G']).
card_cmc('moss kami', 6).
card_layout('moss kami', 'normal').
card_power('moss kami', 5).
card_toughness('moss kami', 5).

% Found in: 8ED, LEG
card_name('moss monster', 'Moss Monster').
card_type('moss monster', 'Creature — Elemental').
card_types('moss monster', ['Creature']).
card_subtypes('moss monster', ['Elemental']).
card_colors('moss monster', ['G']).
card_text('moss monster', '').
card_mana_cost('moss monster', ['3', 'G', 'G']).
card_cmc('moss monster', 5).
card_layout('moss monster', 'normal').
card_power('moss monster', 3).
card_toughness('moss monster', 6).

% Found in: SHM
card_name('mossbridge troll', 'Mossbridge Troll').
card_type('mossbridge troll', 'Creature — Troll').
card_types('mossbridge troll', ['Creature']).
card_subtypes('mossbridge troll', ['Troll']).
card_colors('mossbridge troll', ['G']).
card_text('mossbridge troll', 'If Mossbridge Troll would be destroyed, regenerate it.\nTap any number of untapped creatures you control other than Mossbridge Troll with total power 10 or greater: Mossbridge Troll gets +20/+20 until end of turn.').
card_mana_cost('mossbridge troll', ['5', 'G', 'G']).
card_cmc('mossbridge troll', 7).
card_layout('mossbridge troll', 'normal').
card_power('mossbridge troll', 5).
card_toughness('mossbridge troll', 5).

% Found in: NMS
card_name('mossdog', 'Mossdog').
card_type('mossdog', 'Creature — Plant Hound').
card_types('mossdog', ['Creature']).
card_subtypes('mossdog', ['Plant', 'Hound']).
card_colors('mossdog', ['G']).
card_text('mossdog', 'Whenever Mossdog becomes the target of a spell or ability an opponent controls, put a +1/+1 counter on Mossdog.').
card_mana_cost('mossdog', ['G']).
card_cmc('mossdog', 1).
card_layout('mossdog', 'normal').
card_power('mossdog', 1).
card_toughness('mossdog', 1).

% Found in: ODY
card_name('mossfire egg', 'Mossfire Egg').
card_type('mossfire egg', 'Artifact').
card_types('mossfire egg', ['Artifact']).
card_subtypes('mossfire egg', []).
card_colors('mossfire egg', []).
card_text('mossfire egg', '{2}, {T}, Sacrifice Mossfire Egg: Add {R}{G} to your mana pool. Draw a card.').
card_mana_cost('mossfire egg', ['1']).
card_cmc('mossfire egg', 1).
card_layout('mossfire egg', 'normal').

% Found in: ODY
card_name('mossfire valley', 'Mossfire Valley').
card_type('mossfire valley', 'Land').
card_types('mossfire valley', ['Land']).
card_subtypes('mossfire valley', []).
card_colors('mossfire valley', []).
card_text('mossfire valley', '{1}, {T}: Add {R}{G} to your mana pool.').
card_layout('mossfire valley', 'normal').

% Found in: ALA
card_name('mosstodon', 'Mosstodon').
card_type('mosstodon', 'Creature — Plant Elephant').
card_types('mosstodon', ['Creature']).
card_subtypes('mosstodon', ['Plant', 'Elephant']).
card_colors('mosstodon', ['G']).
card_text('mosstodon', '{1}: Target creature with power 5 or greater gains trample until end of turn.').
card_mana_cost('mosstodon', ['4', 'G']).
card_cmc('mosstodon', 5).
card_layout('mosstodon', 'normal').
card_power('mosstodon', 5).
card_toughness('mosstodon', 3).

% Found in: ARC, C13, LRW
card_name('mosswort bridge', 'Mosswort Bridge').
card_type('mosswort bridge', 'Land').
card_types('mosswort bridge', ['Land']).
card_subtypes('mosswort bridge', []).
card_colors('mosswort bridge', []).
card_text('mosswort bridge', 'Hideaway (This land enters the battlefield tapped. When it does, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library.)\n{T}: Add {G} to your mana pool.\n{G}, {T}: You may play the exiled card without paying its mana cost if creatures you control have total power 10 or greater.').
card_layout('mosswort bridge', 'normal').

% Found in: MMA, MOR
card_name('mothdust changeling', 'Mothdust Changeling').
card_type('mothdust changeling', 'Creature — Shapeshifter').
card_types('mothdust changeling', ['Creature']).
card_subtypes('mothdust changeling', ['Shapeshifter']).
card_colors('mothdust changeling', ['U']).
card_text('mothdust changeling', 'Changeling (This card is every creature type.)\nTap an untapped creature you control: Mothdust Changeling gains flying until end of turn.').
card_mana_cost('mothdust changeling', ['U']).
card_cmc('mothdust changeling', 1).
card_layout('mothdust changeling', 'normal').
card_power('mothdust changeling', 1).
card_toughness('mothdust changeling', 1).

% Found in: UNH
card_name('mother of goons', 'Mother of Goons').
card_type('mother of goons', 'Creature — Human Cleric').
card_types('mother of goons', ['Creature']).
card_subtypes('mother of goons', ['Human', 'Cleric']).
card_colors('mother of goons', ['B']).
card_text('mother of goons', 'Whenever a creature an opponent controls is put into a graveyard from play, sacrifice Mother of Goons unless you insult that creature.').
card_mana_cost('mother of goons', ['2', 'B']).
card_cmc('mother of goons', 3).
card_layout('mother of goons', 'normal').
card_power('mother of goons', 3).
card_toughness('mother of goons', 2).

% Found in: CMD, DDO, ULG, pFNM
card_name('mother of runes', 'Mother of Runes').
card_type('mother of runes', 'Creature — Human Cleric').
card_types('mother of runes', ['Creature']).
card_subtypes('mother of runes', ['Human', 'Cleric']).
card_colors('mother of runes', ['W']).
card_text('mother of runes', '{T}: Target creature you control gains protection from the color of your choice until end of turn.').
card_mana_cost('mother of runes', ['W']).
card_cmc('mother of runes', 1).
card_layout('mother of runes', 'normal').
card_power('mother of runes', 1).
card_toughness('mother of runes', 1).

% Found in: CHK
card_name('mothrider samurai', 'Mothrider Samurai').
card_type('mothrider samurai', 'Creature — Human Samurai').
card_types('mothrider samurai', ['Creature']).
card_subtypes('mothrider samurai', ['Human', 'Samurai']).
card_colors('mothrider samurai', ['W']).
card_text('mothrider samurai', 'Flying\nBushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('mothrider samurai', ['3', 'W']).
card_cmc('mothrider samurai', 4).
card_layout('mothrider samurai', 'normal').
card_power('mothrider samurai', 2).
card_toughness('mothrider samurai', 2).

% Found in: PC2
card_name('mount keralia', 'Mount Keralia').
card_type('mount keralia', 'Plane — Regatha').
card_types('mount keralia', ['Plane']).
card_subtypes('mount keralia', ['Regatha']).
card_colors('mount keralia', []).
card_text('mount keralia', 'At the beginning of your end step, put a pressure counter on Mount Keralia.\nWhen you planeswalk away from Mount Keralia, it deals damage equal to the number of pressure counters on it to each creature and each planeswalker.\nWhenever you roll {C}, prevent all damage that planes named Mount Keralia would deal this game to permanents you control.').
card_layout('mount keralia', 'plane').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ALA, ARC, ARN, ATH, AVR, BFZ, BRB, BTD, C13, C14, CED, CEI, CHK, CMD, CST, DD2, DD3_EVG, DD3_JVC, DDE, DDG, DDH, DDI, DDJ, DDK, DDL, DDN, DDP, DKM, DPA, DTK, EVG, FRF, H09, HOP, ICE, INV, ISD, ITP, KTK, LEA, LEB, LRW, M10, M11, M12, M13, M14, M15, MBS, ME3, MED, MIR, MMQ, MRD, NPH, ODY, ONS, ORI, PC2, PD2, PO2, POR, PTK, RAV, ROE, RQS, RTR, S00, S99, SHM, SOM, THS, TMP, TPR, TSP, UGL, UNH, USG, ZEN, pALP, pARL, pELP, pGRU, pJGP
card_name('mountain', 'Mountain').
card_type('mountain', 'Basic Land — Mountain').
card_types('mountain', ['Land']).
card_subtypes('mountain', ['Mountain']).
card_supertypes('mountain', ['Basic']).
card_colors('mountain', []).
card_text('mountain', '').
card_layout('mountain', 'normal').

% Found in: PTK
card_name('mountain bandit', 'Mountain Bandit').
card_type('mountain bandit', 'Creature — Human Soldier Rogue').
card_types('mountain bandit', ['Creature']).
card_subtypes('mountain bandit', ['Human', 'Soldier', 'Rogue']).
card_colors('mountain bandit', ['R']).
card_text('mountain bandit', 'Haste').
card_mana_cost('mountain bandit', ['R']).
card_cmc('mountain bandit', 1).
card_layout('mountain bandit', 'normal').
card_power('mountain bandit', 1).
card_toughness('mountain bandit', 1).

% Found in: 5ED, 6ED, ICE, POR
card_name('mountain goat', 'Mountain Goat').
card_type('mountain goat', 'Creature — Goat').
card_types('mountain goat', ['Creature']).
card_subtypes('mountain goat', ['Goat']).
card_colors('mountain goat', ['R']).
card_text('mountain goat', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('mountain goat', ['R']).
card_cmc('mountain goat', 1).
card_layout('mountain goat', 'normal').
card_power('mountain goat', 1).
card_toughness('mountain goat', 1).

% Found in: LEG
card_name('mountain stronghold', 'Mountain Stronghold').
card_type('mountain stronghold', 'Land').
card_types('mountain stronghold', ['Land']).
card_subtypes('mountain stronghold', []).
card_colors('mountain stronghold', []).
card_text('mountain stronghold', 'Red legendary creatures you control have \"bands with other legendary creatures.\" (Any legendary creatures can attack in a band as long as at least one has \"bands with other legendary creatures.\" Bands are blocked as a group. If at least two legendary creatures you control, one of which has \"bands with other legendary creatures,\" are blocking or being blocked by the same creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_layout('mountain stronghold', 'normal').

% Found in: ICE
card_name('mountain titan', 'Mountain Titan').
card_type('mountain titan', 'Creature — Giant').
card_types('mountain titan', ['Creature']).
card_subtypes('mountain titan', ['Giant']).
card_colors('mountain titan', ['B', 'R']).
card_text('mountain titan', '{1}{R}{R}: Until end of turn, whenever you cast a black spell, put a +1/+1 counter on Mountain Titan.').
card_mana_cost('mountain titan', ['2', 'B', 'R']).
card_cmc('mountain titan', 4).
card_layout('mountain titan', 'normal').
card_power('mountain titan', 2).
card_toughness('mountain titan', 2).
card_reserved('mountain titan').

% Found in: MIR, VMA
card_name('mountain valley', 'Mountain Valley').
card_type('mountain valley', 'Land').
card_types('mountain valley', ['Land']).
card_subtypes('mountain valley', []).
card_colors('mountain valley', []).
card_text('mountain valley', 'Mountain Valley enters the battlefield tapped.\n{T}, Sacrifice Mountain Valley: Search your library for a Mountain or Forest card and put it onto the battlefield. Then shuffle your library.').
card_layout('mountain valley', 'normal').

% Found in: CHR, LEG, MED
card_name('mountain yeti', 'Mountain Yeti').
card_type('mountain yeti', 'Creature — Yeti').
card_types('mountain yeti', ['Creature']).
card_subtypes('mountain yeti', ['Yeti']).
card_colors('mountain yeti', ['R']).
card_text('mountain yeti', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)\nProtection from white').
card_mana_cost('mountain yeti', ['2', 'R', 'R']).
card_cmc('mountain yeti', 4).
card_layout('mountain yeti', 'normal').
card_power('mountain yeti', 3).
card_toughness('mountain yeti', 3).

% Found in: TMP, TPR
card_name('mounted archers', 'Mounted Archers').
card_type('mounted archers', 'Creature — Human Soldier Archer').
card_types('mounted archers', ['Creature']).
card_subtypes('mounted archers', ['Human', 'Soldier', 'Archer']).
card_colors('mounted archers', ['W']).
card_text('mounted archers', 'Reach (This creature can block creatures with flying.)\n{W}: Mounted Archers can block an additional creature this turn.').
card_mana_cost('mounted archers', ['3', 'W']).
card_cmc('mounted archers', 4).
card_layout('mounted archers', 'normal').
card_power('mounted archers', 2).
card_toughness('mounted archers', 3).

% Found in: MRD
card_name('mourner\'s shield', 'Mourner\'s Shield').
card_type('mourner\'s shield', 'Artifact').
card_types('mourner\'s shield', ['Artifact']).
card_subtypes('mourner\'s shield', []).
card_colors('mourner\'s shield', []).
card_text('mourner\'s shield', 'Imprint — When Mourner\'s Shield enters the battlefield, you may exile target card from a graveyard.\n{2}, {T}: Prevent all damage that would be dealt this turn by a source of your choice that shares a color with the exiled card.').
card_mana_cost('mourner\'s shield', ['4']).
card_cmc('mourner\'s shield', 4).
card_layout('mourner\'s shield', 'normal').

% Found in: APC
card_name('mournful zombie', 'Mournful Zombie').
card_type('mournful zombie', 'Creature — Zombie').
card_types('mournful zombie', ['Creature']).
card_subtypes('mournful zombie', ['Zombie']).
card_colors('mournful zombie', ['B']).
card_text('mournful zombie', '{W}, {T}: Target player gains 1 life.').
card_mana_cost('mournful zombie', ['2', 'B']).
card_cmc('mournful zombie', 3).
card_layout('mournful zombie', 'normal').
card_power('mournful zombie', 2).
card_toughness('mournful zombie', 1).

% Found in: INV
card_name('mourning', 'Mourning').
card_type('mourning', 'Enchantment — Aura').
card_types('mourning', ['Enchantment']).
card_subtypes('mourning', ['Aura']).
card_colors('mourning', ['B']).
card_text('mourning', 'Enchant creature\nEnchanted creature gets -2/-0.\n{B}: Return Mourning to its owner\'s hand.').
card_mana_cost('mourning', ['1', 'B']).
card_cmc('mourning', 2).
card_layout('mourning', 'normal').

% Found in: GPT
card_name('mourning thrull', 'Mourning Thrull').
card_type('mourning thrull', 'Creature — Thrull').
card_types('mourning thrull', ['Creature']).
card_subtypes('mourning thrull', ['Thrull']).
card_colors('mourning thrull', ['W', 'B']).
card_text('mourning thrull', '({W/B} can be paid with either {W} or {B}.)\nFlying\nWhenever Mourning Thrull deals damage, you gain that much life.').
card_mana_cost('mourning thrull', ['1', 'W/B']).
card_cmc('mourning thrull', 2).
card_layout('mourning thrull', 'normal').
card_power('mourning thrull', 1).
card_toughness('mourning thrull', 1).

% Found in: LRW
card_name('mournwhelk', 'Mournwhelk').
card_type('mournwhelk', 'Creature — Elemental').
card_types('mournwhelk', ['Creature']).
card_subtypes('mournwhelk', ['Elemental']).
card_colors('mournwhelk', ['B']).
card_text('mournwhelk', 'When Mournwhelk enters the battlefield, target player discards two cards.\nEvoke {3}{B} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('mournwhelk', ['6', 'B']).
card_cmc('mournwhelk', 7).
card_layout('mournwhelk', 'normal').
card_power('mournwhelk', 3).
card_toughness('mournwhelk', 3).

% Found in: CSP
card_name('mouth of ronom', 'Mouth of Ronom').
card_type('mouth of ronom', 'Snow Land').
card_types('mouth of ronom', ['Land']).
card_subtypes('mouth of ronom', []).
card_supertypes('mouth of ronom', ['Snow']).
card_colors('mouth of ronom', []).
card_text('mouth of ronom', '{T}: Add {1} to your mana pool.\n{4}{S}, {T}, Sacrifice Mouth of Ronom: Mouth of Ronom deals 4 damage to target creature. ({S} can be paid with one mana from a snow permanent.)').
card_layout('mouth of ronom', 'normal').

% Found in: UNH
card_name('mouth to mouth', 'Mouth to Mouth').
card_type('mouth to mouth', 'Sorcery').
card_types('mouth to mouth', ['Sorcery']).
card_subtypes('mouth to mouth', []).
card_colors('mouth to mouth', ['U']).
card_text('mouth to mouth', 'You and target opponent have a breath-holding contest. If you win, you gain control of target creature that player controls.').
card_mana_cost('mouth to mouth', ['3', 'U']).
card_cmc('mouth to mouth', 4).
card_layout('mouth to mouth', 'normal').

% Found in: STH, TPR, V10
card_name('mox diamond', 'Mox Diamond').
card_type('mox diamond', 'Artifact').
card_types('mox diamond', ['Artifact']).
card_subtypes('mox diamond', []).
card_colors('mox diamond', []).
card_text('mox diamond', 'If Mox Diamond would enter the battlefield, you may discard a land card instead. If you do, put Mox Diamond onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('mox diamond', ['0']).
card_cmc('mox diamond', 0).
card_layout('mox diamond', 'normal').
card_reserved('mox diamond').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('mox emerald', 'Mox Emerald').
card_type('mox emerald', 'Artifact').
card_types('mox emerald', ['Artifact']).
card_subtypes('mox emerald', []).
card_colors('mox emerald', []).
card_text('mox emerald', '{T}: Add {G} to your mana pool.').
card_mana_cost('mox emerald', ['0']).
card_cmc('mox emerald', 0).
card_layout('mox emerald', 'normal').
card_reserved('mox emerald').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('mox jet', 'Mox Jet').
card_type('mox jet', 'Artifact').
card_types('mox jet', ['Artifact']).
card_subtypes('mox jet', []).
card_colors('mox jet', []).
card_text('mox jet', '{T}: Add {B} to your mana pool.').
card_mana_cost('mox jet', ['0']).
card_cmc('mox jet', 0).
card_layout('mox jet', 'normal').
card_reserved('mox jet').

% Found in: UNH
card_name('mox lotus', 'Mox Lotus').
card_type('mox lotus', 'Artifact').
card_types('mox lotus', ['Artifact']).
card_subtypes('mox lotus', []).
card_colors('mox lotus', []).
card_text('mox lotus', '{T}: Add {∞} to your mana pool.\n{100}: Add one mana of any color to your mana pool.\nYou don\'t lose life due to mana burn.').
card_mana_cost('mox lotus', ['15']).
card_cmc('mox lotus', 15).
card_layout('mox lotus', 'normal').

% Found in: MM2, SOM
card_name('mox opal', 'Mox Opal').
card_type('mox opal', 'Legendary Artifact').
card_types('mox opal', ['Artifact']).
card_subtypes('mox opal', []).
card_supertypes('mox opal', ['Legendary']).
card_colors('mox opal', []).
card_text('mox opal', 'Metalcraft — {T}: Add one mana of any color to your mana pool. Activate this ability only if you control three or more artifacts.').
card_mana_cost('mox opal', ['0']).
card_cmc('mox opal', 0).
card_layout('mox opal', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('mox pearl', 'Mox Pearl').
card_type('mox pearl', 'Artifact').
card_types('mox pearl', ['Artifact']).
card_subtypes('mox pearl', []).
card_colors('mox pearl', []).
card_text('mox pearl', '{T}: Add {W} to your mana pool.').
card_mana_cost('mox pearl', ['0']).
card_cmc('mox pearl', 0).
card_layout('mox pearl', 'normal').
card_reserved('mox pearl').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('mox ruby', 'Mox Ruby').
card_type('mox ruby', 'Artifact').
card_types('mox ruby', ['Artifact']).
card_subtypes('mox ruby', []).
card_colors('mox ruby', []).
card_text('mox ruby', '{T}: Add {R} to your mana pool.').
card_mana_cost('mox ruby', ['0']).
card_cmc('mox ruby', 0).
card_layout('mox ruby', 'normal').
card_reserved('mox ruby').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('mox sapphire', 'Mox Sapphire').
card_type('mox sapphire', 'Artifact').
card_types('mox sapphire', ['Artifact']).
card_subtypes('mox sapphire', []).
card_colors('mox sapphire', []).
card_text('mox sapphire', '{T}: Add {U} to your mana pool.').
card_mana_cost('mox sapphire', ['0']).
card_cmc('mox sapphire', 0).
card_layout('mox sapphire', 'normal').
card_reserved('mox sapphire').

% Found in: MIR
card_name('mtenda griffin', 'Mtenda Griffin').
card_type('mtenda griffin', 'Creature — Griffin').
card_types('mtenda griffin', ['Creature']).
card_subtypes('mtenda griffin', ['Griffin']).
card_colors('mtenda griffin', ['W']).
card_text('mtenda griffin', 'Flying\n{W}, {T}: Return Mtenda Griffin to its owner\'s hand and return target Griffin card from your graveyard to your hand. Activate this ability only during your upkeep.').
card_mana_cost('mtenda griffin', ['3', 'W']).
card_cmc('mtenda griffin', 4).
card_layout('mtenda griffin', 'normal').
card_power('mtenda griffin', 2).
card_toughness('mtenda griffin', 2).

% Found in: MIR
card_name('mtenda herder', 'Mtenda Herder').
card_type('mtenda herder', 'Creature — Human Scout').
card_types('mtenda herder', ['Creature']).
card_subtypes('mtenda herder', ['Human', 'Scout']).
card_colors('mtenda herder', ['W']).
card_text('mtenda herder', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_mana_cost('mtenda herder', ['W']).
card_cmc('mtenda herder', 1).
card_layout('mtenda herder', 'normal').
card_power('mtenda herder', 1).
card_toughness('mtenda herder', 1).

% Found in: MIR
card_name('mtenda lion', 'Mtenda Lion').
card_type('mtenda lion', 'Creature — Cat').
card_types('mtenda lion', ['Creature']).
card_subtypes('mtenda lion', ['Cat']).
card_colors('mtenda lion', ['G']).
card_text('mtenda lion', 'Whenever Mtenda Lion attacks, defending player may pay {U}. If that player does, prevent all combat damage that would be dealt by Mtenda Lion this turn.').
card_mana_cost('mtenda lion', ['G']).
card_cmc('mtenda lion', 1).
card_layout('mtenda lion', 'normal').
card_power('mtenda lion', 2).
card_toughness('mtenda lion', 1).

% Found in: PLC
card_name('muck drubb', 'Muck Drubb').
card_type('muck drubb', 'Creature — Beast').
card_types('muck drubb', ['Creature']).
card_subtypes('muck drubb', ['Beast']).
card_colors('muck drubb', ['B']).
card_text('muck drubb', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Muck Drubb enters the battlefield, change the target of target spell that targets only a single creature to Muck Drubb.\nMadness {2}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('muck drubb', ['3', 'B', 'B']).
card_cmc('muck drubb', 5).
card_layout('muck drubb', 'normal').
card_power('muck drubb', 3).
card_toughness('muck drubb', 3).

% Found in: PO2, POR, S99
card_name('muck rats', 'Muck Rats').
card_type('muck rats', 'Creature — Rat').
card_types('muck rats', ['Creature']).
card_subtypes('muck rats', ['Rat']).
card_colors('muck rats', ['B']).
card_text('muck rats', '').
card_mana_cost('muck rats', ['B']).
card_cmc('muck rats', 1).
card_layout('muck rats', 'normal').
card_power('muck rats', 1).
card_toughness('muck rats', 1).

% Found in: SHM
card_name('mudbrawler cohort', 'Mudbrawler Cohort').
card_type('mudbrawler cohort', 'Creature — Goblin Warrior').
card_types('mudbrawler cohort', ['Creature']).
card_subtypes('mudbrawler cohort', ['Goblin', 'Warrior']).
card_colors('mudbrawler cohort', ['R']).
card_text('mudbrawler cohort', 'Haste\nMudbrawler Cohort gets +1/+1 as long as you control another red creature.').
card_mana_cost('mudbrawler cohort', ['1', 'R']).
card_cmc('mudbrawler cohort', 2).
card_layout('mudbrawler cohort', 'normal').
card_power('mudbrawler cohort', 1).
card_toughness('mudbrawler cohort', 1).

% Found in: SHM
card_name('mudbrawler raiders', 'Mudbrawler Raiders').
card_type('mudbrawler raiders', 'Creature — Goblin Warrior').
card_types('mudbrawler raiders', ['Creature']).
card_subtypes('mudbrawler raiders', ['Goblin', 'Warrior']).
card_colors('mudbrawler raiders', ['R', 'G']).
card_text('mudbrawler raiders', 'Mudbrawler Raiders can\'t be blocked by blue creatures.').
card_mana_cost('mudbrawler raiders', ['2', 'R/G', 'R/G']).
card_cmc('mudbrawler raiders', 4).
card_layout('mudbrawler raiders', 'normal').
card_power('mudbrawler raiders', 3).
card_toughness('mudbrawler raiders', 3).

% Found in: MOR
card_name('mudbutton clanger', 'Mudbutton Clanger').
card_type('mudbutton clanger', 'Creature — Goblin Warrior').
card_types('mudbutton clanger', ['Creature']).
card_subtypes('mudbutton clanger', ['Goblin', 'Warrior']).
card_colors('mudbutton clanger', ['R']).
card_text('mudbutton clanger', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Mudbutton Clanger, you may reveal it. If you do, Mudbutton Clanger gets +1/+1 until end of turn.').
card_mana_cost('mudbutton clanger', ['R']).
card_cmc('mudbutton clanger', 1).
card_layout('mudbutton clanger', 'normal').
card_power('mudbutton clanger', 1).
card_toughness('mudbutton clanger', 1).

% Found in: DD3_EVG, DDG, EVG, LRW, PC2
card_name('mudbutton torchrunner', 'Mudbutton Torchrunner').
card_type('mudbutton torchrunner', 'Creature — Goblin Warrior').
card_types('mudbutton torchrunner', ['Creature']).
card_subtypes('mudbutton torchrunner', ['Goblin', 'Warrior']).
card_colors('mudbutton torchrunner', ['R']).
card_text('mudbutton torchrunner', 'When Mudbutton Torchrunner dies, it deals 3 damage to target creature or player.').
card_mana_cost('mudbutton torchrunner', ['2', 'R']).
card_cmc('mudbutton torchrunner', 3).
card_layout('mudbutton torchrunner', 'normal').
card_power('mudbutton torchrunner', 1).
card_toughness('mudbutton torchrunner', 1).

% Found in: RAV
card_name('muddle the mixture', 'Muddle the Mixture').
card_type('muddle the mixture', 'Instant').
card_types('muddle the mixture', ['Instant']).
card_subtypes('muddle the mixture', []).
card_colors('muddle the mixture', ['U']).
card_text('muddle the mixture', 'Counter target instant or sorcery spell.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('muddle the mixture', ['U', 'U']).
card_cmc('muddle the mixture', 2).
card_layout('muddle the mixture', 'normal').

% Found in: ODY
card_name('mudhole', 'Mudhole').
card_type('mudhole', 'Instant').
card_types('mudhole', ['Instant']).
card_subtypes('mudhole', []).
card_colors('mudhole', ['R']).
card_text('mudhole', 'Target player exiles all land cards from his or her graveyard.').
card_mana_cost('mudhole', ['2', 'R']).
card_cmc('mudhole', 3).
card_layout('mudhole', 'normal').

% Found in: ICE, ME2
card_name('mudslide', 'Mudslide').
card_type('mudslide', 'Enchantment').
card_types('mudslide', ['Enchantment']).
card_subtypes('mudslide', []).
card_colors('mudslide', ['R']).
card_text('mudslide', 'Creatures without flying don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player may choose any number of tapped creatures without flying he or she controls and pay {2} for each creature chosen this way. If the player does, untap those creatures.').
card_mana_cost('mudslide', ['2', 'R']).
card_cmc('mudslide', 3).
card_layout('mudslide', 'normal').
card_reserved('mudslide').

% Found in: GTC
card_name('mugging', 'Mugging').
card_type('mugging', 'Sorcery').
card_types('mugging', ['Sorcery']).
card_subtypes('mugging', []).
card_colors('mugging', ['R']).
card_text('mugging', 'Mugging deals 2 damage to target creature. That creature can\'t block this turn.').
card_mana_cost('mugging', ['R']).
card_cmc('mugging', 1).
card_layout('mugging', 'normal').

% Found in: ROE
card_name('mul daya channelers', 'Mul Daya Channelers').
card_type('mul daya channelers', 'Creature — Elf Druid Shaman').
card_types('mul daya channelers', ['Creature']).
card_subtypes('mul daya channelers', ['Elf', 'Druid', 'Shaman']).
card_colors('mul daya channelers', ['G']).
card_text('mul daya channelers', 'Play with the top card of your library revealed.\nAs long as the top card of your library is a creature card, Mul Daya Channelers gets +3/+3.\nAs long as the top card of your library is a land card, Mul Daya Channelers has \"{T}: Add two mana of any one color to your mana pool.\"').
card_mana_cost('mul daya channelers', ['1', 'G', 'G']).
card_cmc('mul daya channelers', 3).
card_layout('mul daya channelers', 'normal').
card_power('mul daya channelers', 2).
card_toughness('mul daya channelers', 2).

% Found in: ISD, STH, TPR
card_name('mulch', 'Mulch').
card_type('mulch', 'Sorcery').
card_types('mulch', ['Sorcery']).
card_subtypes('mulch', []).
card_colors('mulch', ['G']).
card_text('mulch', 'Reveal the top four cards of your library. Put all land cards revealed this way into your hand and the rest into your graveyard.').
card_mana_cost('mulch', ['1', 'G']).
card_cmc('mulch', 2).
card_layout('mulch', 'normal').

% Found in: C14, CMD, DD2, DD3_JVC, LRW, MM2, MMA, pFNM
card_name('mulldrifter', 'Mulldrifter').
card_type('mulldrifter', 'Creature — Elemental').
card_types('mulldrifter', ['Creature']).
card_subtypes('mulldrifter', ['Elemental']).
card_colors('mulldrifter', ['U']).
card_text('mulldrifter', 'Flying\nWhen Mulldrifter enters the battlefield, draw two cards.\nEvoke {2}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('mulldrifter', ['4', 'U']).
card_cmc('mulldrifter', 5).
card_layout('mulldrifter', 'normal').
card_power('mulldrifter', 2).
card_toughness('mulldrifter', 2).

% Found in: VAN
card_name('multani', 'Multani').
card_type('multani', 'Vanguard').
card_types('multani', ['Vanguard']).
card_subtypes('multani', []).
card_colors('multani', []).
card_text('multani', 'Creatures you control get +X/+0, where X is the number of cards in your hand.').
card_layout('multani', 'vanguard').

% Found in: ULG
card_name('multani\'s acolyte', 'Multani\'s Acolyte').
card_type('multani\'s acolyte', 'Creature — Elf').
card_types('multani\'s acolyte', ['Creature']).
card_subtypes('multani\'s acolyte', ['Elf']).
card_colors('multani\'s acolyte', ['G']).
card_text('multani\'s acolyte', 'Echo {G}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Multani\'s Acolyte enters the battlefield, draw a card.').
card_mana_cost('multani\'s acolyte', ['G', 'G']).
card_cmc('multani\'s acolyte', 2).
card_layout('multani\'s acolyte', 'normal').
card_power('multani\'s acolyte', 2).
card_toughness('multani\'s acolyte', 1).

% Found in: UDS
card_name('multani\'s decree', 'Multani\'s Decree').
card_type('multani\'s decree', 'Sorcery').
card_types('multani\'s decree', ['Sorcery']).
card_subtypes('multani\'s decree', []).
card_colors('multani\'s decree', ['G']).
card_text('multani\'s decree', 'Destroy all enchantments. You gain 2 life for each enchantment destroyed this way.').
card_mana_cost('multani\'s decree', ['3', 'G']).
card_cmc('multani\'s decree', 4).
card_layout('multani\'s decree', 'normal').

% Found in: PLS
card_name('multani\'s harmony', 'Multani\'s Harmony').
card_type('multani\'s harmony', 'Enchantment — Aura').
card_types('multani\'s harmony', ['Enchantment']).
card_subtypes('multani\'s harmony', ['Aura']).
card_colors('multani\'s harmony', ['G']).
card_text('multani\'s harmony', 'Enchant creature\nEnchanted creature has \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('multani\'s harmony', ['G']).
card_cmc('multani\'s harmony', 1).
card_layout('multani\'s harmony', 'normal').

% Found in: ULG
card_name('multani\'s presence', 'Multani\'s Presence').
card_type('multani\'s presence', 'Enchantment').
card_types('multani\'s presence', ['Enchantment']).
card_subtypes('multani\'s presence', []).
card_colors('multani\'s presence', ['G']).
card_text('multani\'s presence', 'Whenever a spell you\'ve cast is countered, draw a card.').
card_mana_cost('multani\'s presence', ['G']).
card_cmc('multani\'s presence', 1).
card_layout('multani\'s presence', 'normal').

% Found in: ULG
card_name('multani, maro-sorcerer', 'Multani, Maro-Sorcerer').
card_type('multani, maro-sorcerer', 'Legendary Creature — Elemental').
card_types('multani, maro-sorcerer', ['Creature']).
card_subtypes('multani, maro-sorcerer', ['Elemental']).
card_supertypes('multani, maro-sorcerer', ['Legendary']).
card_colors('multani, maro-sorcerer', ['G']).
card_text('multani, maro-sorcerer', 'Shroud (This creature can\'t be the target of spells or abilities.)\nMultani, Maro-Sorcerer\'s power and toughness are each equal to the total number of cards in all players\' hands.').
card_mana_cost('multani, maro-sorcerer', ['4', 'G', 'G']).
card_cmc('multani, maro-sorcerer', 6).
card_layout('multani, maro-sorcerer', 'normal').
card_power('multani, maro-sorcerer', '*').
card_toughness('multani, maro-sorcerer', '*').
card_reserved('multani, maro-sorcerer').

% Found in: BFZ
card_name('munda, ambush leader', 'Munda, Ambush Leader').
card_type('munda, ambush leader', 'Legendary Creature — Kor Ally').
card_types('munda, ambush leader', ['Creature']).
card_subtypes('munda, ambush leader', ['Kor', 'Ally']).
card_supertypes('munda, ambush leader', ['Legendary']).
card_colors('munda, ambush leader', ['W', 'R']).
card_text('munda, ambush leader', 'Haste\nRally — Whenever Munda, Ambush Leader or another Ally enters the battlefield under your control, you may look at the top four cards of your library. If you do, reveal any number of Ally cards from among them, then put those cards on top of your library in any order and the rest on the bottom in any order.').
card_mana_cost('munda, ambush leader', ['2', 'R', 'W']).
card_cmc('munda, ambush leader', 4).
card_layout('munda, ambush leader', 'normal').
card_power('munda, ambush leader', 3).
card_toughness('munda, ambush leader', 4).

% Found in: VIS
card_name('mundungu', 'Mundungu').
card_type('mundungu', 'Creature — Human Wizard').
card_types('mundungu', ['Creature']).
card_subtypes('mundungu', ['Human', 'Wizard']).
card_colors('mundungu', ['U', 'B']).
card_text('mundungu', '{T}: Counter target spell unless its controller pays {1} and 1 life.').
card_mana_cost('mundungu', ['1', 'U', 'B']).
card_cmc('mundungu', 3).
card_layout('mundungu', 'normal').
card_power('mundungu', 1).
card_toughness('mundungu', 1).

% Found in: PCY
card_name('mungha wurm', 'Mungha Wurm').
card_type('mungha wurm', 'Creature — Wurm').
card_types('mungha wurm', ['Creature']).
card_subtypes('mungha wurm', ['Wurm']).
card_colors('mungha wurm', ['G']).
card_text('mungha wurm', 'You can\'t untap more than one land during your untap step.').
card_mana_cost('mungha wurm', ['2', 'G', 'G']).
card_cmc('mungha wurm', 4).
card_layout('mungha wurm', 'normal').
card_power('mungha wurm', 6).
card_toughness('mungha wurm', 5).

% Found in: FUT
card_name('muraganda petroglyphs', 'Muraganda Petroglyphs').
card_type('muraganda petroglyphs', 'Enchantment').
card_types('muraganda petroglyphs', ['Enchantment']).
card_subtypes('muraganda petroglyphs', []).
card_colors('muraganda petroglyphs', ['G']).
card_text('muraganda petroglyphs', 'Creatures with no abilities get +2/+2.').
card_mana_cost('muraganda petroglyphs', ['3', 'G']).
card_cmc('muraganda petroglyphs', 4).
card_layout('muraganda petroglyphs', 'normal').

% Found in: HOP
card_name('murasa', 'Murasa').
card_type('murasa', 'Plane — Zendikar').
card_types('murasa', ['Plane']).
card_subtypes('murasa', ['Zendikar']).
card_colors('murasa', []).
card_text('murasa', 'Whenever a nontoken creature enters the battlefield, its controller may search his or her library for a basic land card, put it onto the battlefield tapped, then shuffle his or her library.\nWhenever you roll {C}, target land becomes a 4/4 creature that\'s still a land.').
card_layout('murasa', 'plane').

% Found in: ZEN
card_name('murasa pyromancer', 'Murasa Pyromancer').
card_type('murasa pyromancer', 'Creature — Human Shaman Ally').
card_types('murasa pyromancer', ['Creature']).
card_subtypes('murasa pyromancer', ['Human', 'Shaman', 'Ally']).
card_colors('murasa pyromancer', ['R']).
card_text('murasa pyromancer', 'Whenever Murasa Pyromancer or another Ally enters the battlefield under your control, you may have Murasa Pyromancer deal damage to target creature equal to the number of Allies you control.').
card_mana_cost('murasa pyromancer', ['4', 'R', 'R']).
card_cmc('murasa pyromancer', 6).
card_layout('murasa pyromancer', 'normal').
card_power('murasa pyromancer', 3).
card_toughness('murasa pyromancer', 2).

% Found in: BFZ
card_name('murasa ranger', 'Murasa Ranger').
card_type('murasa ranger', 'Creature — Human Warrior').
card_types('murasa ranger', ['Creature']).
card_subtypes('murasa ranger', ['Human', 'Warrior']).
card_colors('murasa ranger', ['G']).
card_text('murasa ranger', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {3}{G}. If you do, put two +1/+1 counters on Murasa Ranger.').
card_mana_cost('murasa ranger', ['3', 'G']).
card_cmc('murasa ranger', 4).
card_layout('murasa ranger', 'normal').
card_power('murasa ranger', 3).
card_toughness('murasa ranger', 3).

% Found in: M13
card_name('murder', 'Murder').
card_type('murder', 'Instant').
card_types('murder', ['Instant']).
card_subtypes('murder', []).
card_colors('murder', ['B']).
card_text('murder', 'Destroy target creature.').
card_mana_cost('murder', ['1', 'B', 'B']).
card_cmc('murder', 3).
card_layout('murder', 'normal').

% Found in: GTC, ORI
card_name('murder investigation', 'Murder Investigation').
card_type('murder investigation', 'Enchantment — Aura').
card_types('murder investigation', ['Enchantment']).
card_subtypes('murder investigation', ['Aura']).
card_colors('murder investigation', ['W']).
card_text('murder investigation', 'Enchant creature you control\nWhen enchanted creature dies, put X 1/1 white Soldier creature tokens onto the battlefield, where X is its power.').
card_mana_cost('murder investigation', ['1', 'W']).
card_cmc('murder investigation', 2).
card_layout('murder investigation', 'normal').

% Found in: ISD
card_name('murder of crows', 'Murder of Crows').
card_type('murder of crows', 'Creature — Bird').
card_types('murder of crows', ['Creature']).
card_subtypes('murder of crows', ['Bird']).
card_colors('murder of crows', ['U']).
card_text('murder of crows', 'Flying\nWhenever another creature dies, you may draw a card. If you do, discard a card.').
card_mana_cost('murder of crows', ['3', 'U', 'U']).
card_cmc('murder of crows', 5).
card_layout('murder of crows', 'normal').
card_power('murder of crows', 4).
card_toughness('murder of crows', 4).

% Found in: 8ED, NMS
card_name('murderous betrayal', 'Murderous Betrayal').
card_type('murderous betrayal', 'Enchantment').
card_types('murderous betrayal', ['Enchantment']).
card_subtypes('murderous betrayal', []).
card_colors('murderous betrayal', ['B']).
card_text('murderous betrayal', '{B}{B}, Pay half your life, rounded up: Destroy target nonblack creature. It can\'t be regenerated.').
card_mana_cost('murderous betrayal', ['B', 'B', 'B']).
card_cmc('murderous betrayal', 3).
card_layout('murderous betrayal', 'normal').

% Found in: KTK
card_name('murderous cut', 'Murderous Cut').
card_type('murderous cut', 'Instant').
card_types('murderous cut', ['Instant']).
card_subtypes('murderous cut', []).
card_colors('murderous cut', ['B']).
card_text('murderous cut', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nDestroy target creature.').
card_mana_cost('murderous cut', ['4', 'B']).
card_cmc('murderous cut', 5).
card_layout('murderous cut', 'normal').

% Found in: MMA, SHM, pFNM
card_name('murderous redcap', 'Murderous Redcap').
card_type('murderous redcap', 'Creature — Goblin Assassin').
card_types('murderous redcap', ['Creature']).
card_subtypes('murderous redcap', ['Goblin', 'Assassin']).
card_colors('murderous redcap', ['B', 'R']).
card_text('murderous redcap', 'When Murderous Redcap enters the battlefield, it deals damage equal to its power to target creature or player.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('murderous redcap', ['2', 'B/R', 'B/R']).
card_cmc('murderous redcap', 4).
card_layout('murderous redcap', 'normal').
card_power('murderous redcap', 2).
card_toughness('murderous redcap', 2).

% Found in: VAN
card_name('murderous redcap avatar', 'Murderous Redcap Avatar').
card_type('murderous redcap avatar', 'Vanguard').
card_types('murderous redcap avatar', ['Vanguard']).
card_subtypes('murderous redcap avatar', []).
card_colors('murderous redcap avatar', []).
card_text('murderous redcap avatar', 'Whenever a creature enters the battlefield under your control with a counter on it, you may have it deal damage equal to its power to target creature or player.').
card_layout('murderous redcap avatar', 'vanguard').

% Found in: DST
card_name('murderous spoils', 'Murderous Spoils').
card_type('murderous spoils', 'Instant').
card_types('murderous spoils', ['Instant']).
card_subtypes('murderous spoils', []).
card_colors('murderous spoils', ['B']).
card_text('murderous spoils', 'Destroy target nonblack creature. It can\'t be regenerated. You gain control of all Equipment that was attached to it. (This effect lasts indefinitely.)').
card_mana_cost('murderous spoils', ['5', 'B']).
card_cmc('murderous spoils', 6).
card_layout('murderous spoils', 'normal').

% Found in: 4ED, 5ED, DRK, ITP, RQS
card_name('murk dwellers', 'Murk Dwellers').
card_type('murk dwellers', 'Creature — Zombie').
card_types('murk dwellers', ['Creature']).
card_subtypes('murk dwellers', ['Zombie']).
card_colors('murk dwellers', ['B']).
card_text('murk dwellers', 'Whenever Murk Dwellers attacks and isn\'t blocked, it gets +2/+0 until end of combat.').
card_mana_cost('murk dwellers', ['3', 'B']).
card_cmc('murk dwellers', 4).
card_layout('murk dwellers', 'normal').
card_power('murk dwellers', 2).
card_toughness('murk dwellers', 2).

% Found in: BFZ
card_name('murk strider', 'Murk Strider').
card_type('murk strider', 'Creature — Eldrazi Processor').
card_types('murk strider', ['Creature']).
card_subtypes('murk strider', ['Eldrazi', 'Processor']).
card_colors('murk strider', []).
card_text('murk strider', 'Devoid (This card has no color.)\nWhen Murk Strider enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, return target creature to its owner\'s hand.').
card_mana_cost('murk strider', ['3', 'U']).
card_cmc('murk strider', 4).
card_layout('murk strider', 'normal').
card_power('murk strider', 3).
card_toughness('murk strider', 2).

% Found in: C13, EVE
card_name('murkfiend liege', 'Murkfiend Liege').
card_type('murkfiend liege', 'Creature — Horror').
card_types('murkfiend liege', ['Creature']).
card_subtypes('murkfiend liege', ['Horror']).
card_colors('murkfiend liege', ['U', 'G']).
card_text('murkfiend liege', 'Other green creatures you control get +1/+1.\nOther blue creatures you control get +1/+1.\nUntap all green and/or blue creatures you control during each other player\'s untap step.').
card_mana_cost('murkfiend liege', ['2', 'G/U', 'G/U', 'G/U']).
card_cmc('murkfiend liege', 5).
card_layout('murkfiend liege', 'normal').
card_power('murkfiend liege', 4).
card_toughness('murkfiend liege', 4).

% Found in: MOR, V12
card_name('murmuring bosk', 'Murmuring Bosk').
card_type('murmuring bosk', 'Land — Forest').
card_types('murmuring bosk', ['Land']).
card_subtypes('murmuring bosk', ['Forest']).
card_colors('murmuring bosk', []).
card_text('murmuring bosk', '({T}: Add {G} to your mana pool.)\nAs Murmuring Bosk enters the battlefield, you may reveal a Treefolk card from your hand. If you don\'t, Murmuring Bosk enters the battlefield tapped.\n{T}: Add {W} or {B} to your mana pool. Murmuring Bosk deals 1 damage to you.').
card_layout('murmuring bosk', 'normal').

% Found in: DGM
card_name('murmuring phantasm', 'Murmuring Phantasm').
card_type('murmuring phantasm', 'Creature — Spirit').
card_types('murmuring phantasm', ['Creature']).
card_subtypes('murmuring phantasm', ['Spirit']).
card_colors('murmuring phantasm', ['U']).
card_text('murmuring phantasm', 'Defender').
card_mana_cost('murmuring phantasm', ['1', 'U']).
card_cmc('murmuring phantasm', 2).
card_layout('murmuring phantasm', 'normal').
card_power('murmuring phantasm', 0).
card_toughness('murmuring phantasm', 5).

% Found in: CMD, SOK
card_name('murmurs from beyond', 'Murmurs from Beyond').
card_type('murmurs from beyond', 'Instant — Arcane').
card_types('murmurs from beyond', ['Instant']).
card_subtypes('murmurs from beyond', ['Arcane']).
card_colors('murmurs from beyond', ['U']).
card_text('murmurs from beyond', 'Reveal the top three cards of your library. An opponent chooses one of them. Put that card into your graveyard and the rest into your hand.').
card_mana_cost('murmurs from beyond', ['2', 'U']).
card_cmc('murmurs from beyond', 3).
card_layout('murmurs from beyond', 'normal').

% Found in: ODY
card_name('muscle burst', 'Muscle Burst').
card_type('muscle burst', 'Instant').
card_types('muscle burst', ['Instant']).
card_subtypes('muscle burst', []).
card_colors('muscle burst', ['G']).
card_text('muscle burst', 'Target creature gets +X/+X until end of turn, where X is 3 plus the number of cards named Muscle Burst in all graveyards.').
card_mana_cost('muscle burst', ['1', 'G']).
card_cmc('muscle burst', 2).
card_layout('muscle burst', 'normal').

% Found in: H09, TMP, TPR, pFNM
card_name('muscle sliver', 'Muscle Sliver').
card_type('muscle sliver', 'Creature — Sliver').
card_types('muscle sliver', ['Creature']).
card_subtypes('muscle sliver', ['Sliver']).
card_colors('muscle sliver', ['G']).
card_text('muscle sliver', 'All Sliver creatures get +1/+1.').
card_mana_cost('muscle sliver', ['1', 'G']).
card_cmc('muscle sliver', 2).
card_layout('muscle sliver', 'normal').
card_power('muscle sliver', 1).
card_toughness('muscle sliver', 1).

% Found in: DIS
card_name('muse vessel', 'Muse Vessel').
card_type('muse vessel', 'Artifact').
card_types('muse vessel', ['Artifact']).
card_subtypes('muse vessel', []).
card_colors('muse vessel', []).
card_text('muse vessel', '{3}, {T}: Target player exiles a card from his or her hand. Activate this ability only any time you could cast a sorcery.\n{1}: Choose a card exiled with Muse Vessel. You may play that card this turn.').
card_mana_cost('muse vessel', ['4']).
card_cmc('muse vessel', 4).
card_layout('muse vessel', 'normal').

% Found in: ICE, ME2
card_name('musician', 'Musician').
card_type('musician', 'Creature — Human Wizard').
card_types('musician', ['Creature']).
card_subtypes('musician', ['Human', 'Wizard']).
card_colors('musician', ['U']).
card_text('musician', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\n{T}: Put a music counter on target creature. If it doesn\'t have \"At the beginning of your upkeep, destroy this creature unless you pay {1} for each music counter on it,\" it gains that ability.').
card_mana_cost('musician', ['2', 'U']).
card_cmc('musician', 3).
card_layout('musician', 'normal').
card_power('musician', 1).
card_toughness('musician', 3).
card_reserved('musician').

% Found in: MM2, NPH
card_name('mutagenic growth', 'Mutagenic Growth').
card_type('mutagenic growth', 'Instant').
card_types('mutagenic growth', ['Instant']).
card_subtypes('mutagenic growth', []).
card_colors('mutagenic growth', ['G']).
card_text('mutagenic growth', '({G/P} can be paid with either {G} or 2 life.)\nTarget creature gets +2/+2 until end of turn.').
card_mana_cost('mutagenic growth', ['G/P']).
card_cmc('mutagenic growth', 1).
card_layout('mutagenic growth', 'normal').

% Found in: DGM
card_name('mutant\'s prey', 'Mutant\'s Prey').
card_type('mutant\'s prey', 'Instant').
card_types('mutant\'s prey', ['Instant']).
card_subtypes('mutant\'s prey', []).
card_colors('mutant\'s prey', ['G']).
card_text('mutant\'s prey', 'Target creature you control with a +1/+1 counter on it fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_mana_cost('mutant\'s prey', ['G']).
card_cmc('mutant\'s prey', 1).
card_layout('mutant\'s prey', 'normal').

% Found in: M14, MOR, pCMP
card_name('mutavault', 'Mutavault').
card_type('mutavault', 'Land').
card_types('mutavault', ['Land']).
card_subtypes('mutavault', []).
card_colors('mutavault', []).
card_text('mutavault', '{T}: Add {1} to your mana pool.\n{1}: Mutavault becomes a 2/2 creature with all creature types until end of turn. It\'s still a land.').
card_layout('mutavault', 'normal').

% Found in: C14, DD3_GVL, DDD, M13, TOR
card_name('mutilate', 'Mutilate').
card_type('mutilate', 'Sorcery').
card_types('mutilate', ['Sorcery']).
card_subtypes('mutilate', []).
card_colors('mutilate', ['B']).
card_text('mutilate', 'All creatures get -1/-1 until end of turn for each Swamp you control.').
card_mana_cost('mutilate', ['2', 'B', 'B']).
card_cmc('mutilate', 4).
card_layout('mutilate', 'normal').

% Found in: PC2
card_name('mutual epiphany', 'Mutual Epiphany').
card_type('mutual epiphany', 'Phenomenon').
card_types('mutual epiphany', ['Phenomenon']).
card_subtypes('mutual epiphany', []).
card_colors('mutual epiphany', []).
card_text('mutual epiphany', 'When you encounter Mutual Epiphany, each player draws four cards. (Then planeswalk away from this phenomenon.)').
card_layout('mutual epiphany', 'phenomenon').

% Found in: CNS
card_name('muzzio\'s preparations', 'Muzzio\'s Preparations').
card_type('muzzio\'s preparations', 'Conspiracy').
card_types('muzzio\'s preparations', ['Conspiracy']).
card_subtypes('muzzio\'s preparations', []).
card_colors('muzzio\'s preparations', []).
card_text('muzzio\'s preparations', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nEach creature you control with the chosen name enters the battlefield with an additional +1/+1 counter on it.').
card_layout('muzzio\'s preparations', 'normal').

% Found in: CNS, VMA
card_name('muzzio, visionary architect', 'Muzzio, Visionary Architect').
card_type('muzzio, visionary architect', 'Legendary Creature — Human Artificer').
card_types('muzzio, visionary architect', ['Creature']).
card_subtypes('muzzio, visionary architect', ['Human', 'Artificer']).
card_supertypes('muzzio, visionary architect', ['Legendary']).
card_colors('muzzio, visionary architect', ['U']).
card_text('muzzio, visionary architect', '{3}{U}, {T}: Look at the top X cards of your library, where X is the highest converted mana cost among artifacts you control. You may reveal an artifact card from among them and put it onto the battlefield. Put the rest on the bottom of your library in any order.').
card_mana_cost('muzzio, visionary architect', ['1', 'U', 'U']).
card_cmc('muzzio, visionary architect', 3).
card_layout('muzzio, visionary architect', 'normal').
card_power('muzzio, visionary architect', 1).
card_toughness('muzzio, visionary architect', 3).

% Found in: MMQ
card_name('muzzle', 'Muzzle').
card_type('muzzle', 'Enchantment — Aura').
card_types('muzzle', ['Enchantment']).
card_subtypes('muzzle', ['Aura']).
card_colors('muzzle', ['W']).
card_text('muzzle', 'Enchant creature\nPrevent all damage that would be dealt by enchanted creature.').
card_mana_cost('muzzle', ['1', 'W']).
card_cmc('muzzle', 2).
card_layout('muzzle', 'normal').

% Found in: TSP
card_name('mwonvuli acid-moss', 'Mwonvuli Acid-Moss').
card_type('mwonvuli acid-moss', 'Sorcery').
card_types('mwonvuli acid-moss', ['Sorcery']).
card_subtypes('mwonvuli acid-moss', []).
card_colors('mwonvuli acid-moss', ['G']).
card_text('mwonvuli acid-moss', 'Destroy target land. Search your library for a Forest card and put that card onto the battlefield tapped. Then shuffle your library.').
card_mana_cost('mwonvuli acid-moss', ['2', 'G', 'G']).
card_cmc('mwonvuli acid-moss', 4).
card_layout('mwonvuli acid-moss', 'normal').

% Found in: M13, pMGD
card_name('mwonvuli beast tracker', 'Mwonvuli Beast Tracker').
card_type('mwonvuli beast tracker', 'Creature — Human Scout').
card_types('mwonvuli beast tracker', ['Creature']).
card_subtypes('mwonvuli beast tracker', ['Human', 'Scout']).
card_colors('mwonvuli beast tracker', ['G']).
card_text('mwonvuli beast tracker', 'When Mwonvuli Beast Tracker enters the battlefield, search your library for a creature card with deathtouch, hexproof, reach, or trample and reveal it. Shuffle your library and put that card on top of it.').
card_mana_cost('mwonvuli beast tracker', ['1', 'G', 'G']).
card_cmc('mwonvuli beast tracker', 3).
card_layout('mwonvuli beast tracker', 'normal').
card_power('mwonvuli beast tracker', 2).
card_toughness('mwonvuli beast tracker', 1).

% Found in: WTH
card_name('mwonvuli ooze', 'Mwonvuli Ooze').
card_type('mwonvuli ooze', 'Creature — Ooze').
card_types('mwonvuli ooze', ['Creature']).
card_subtypes('mwonvuli ooze', ['Ooze']).
card_colors('mwonvuli ooze', ['G']).
card_text('mwonvuli ooze', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay {2} for each age counter on it.)\nMwonvuli Ooze\'s power and toughness are each equal to 1 plus twice the number of age counters on it.').
card_mana_cost('mwonvuli ooze', ['G']).
card_cmc('mwonvuli ooze', 1).
card_layout('mwonvuli ooze', 'normal').
card_power('mwonvuli ooze', '1+*').
card_toughness('mwonvuli ooze', '1+*').
card_reserved('mwonvuli ooze').

% Found in: ARC
card_name('my crushing masterstroke', 'My Crushing Masterstroke').
card_type('my crushing masterstroke', 'Scheme').
card_types('my crushing masterstroke', ['Scheme']).
card_subtypes('my crushing masterstroke', []).
card_colors('my crushing masterstroke', []).
card_text('my crushing masterstroke', 'When you set this scheme in motion, gain control of all nonland permanents your opponents control until end of turn. Untap those permanents. They gain haste until end of turn. Each of them attacks its owner this turn if able.').
card_layout('my crushing masterstroke', 'scheme').

% Found in: UNH
card_name('my first tome', 'My First Tome').
card_type('my first tome', 'Artifact').
card_types('my first tome', ['Artifact']).
card_subtypes('my first tome', []).
card_colors('my first tome', []).
card_text('my first tome', '{1}, {T}: Say the flavor text on a card in your hand. Target opponent guesses that card\'s name. You may reveal that card. If you do and your opponent guessed wrong, draw a card.').
card_mana_cost('my first tome', ['3']).
card_cmc('my first tome', 3).
card_layout('my first tome', 'normal').

% Found in: ARC
card_name('my genius knows no bounds', 'My Genius Knows No Bounds').
card_type('my genius knows no bounds', 'Scheme').
card_types('my genius knows no bounds', ['Scheme']).
card_subtypes('my genius knows no bounds', []).
card_colors('my genius knows no bounds', []).
card_text('my genius knows no bounds', 'When you set this scheme in motion, you may pay {X}. If you do, you gain X life and draw X cards.').
card_layout('my genius knows no bounds', 'scheme').

% Found in: ARC
card_name('my undead horde awakens', 'My Undead Horde Awakens').
card_type('my undead horde awakens', 'Ongoing Scheme').
card_types('my undead horde awakens', ['Scheme']).
card_subtypes('my undead horde awakens', []).
card_supertypes('my undead horde awakens', ['Ongoing']).
card_colors('my undead horde awakens', []).
card_text('my undead horde awakens', '(An ongoing scheme remains face up until it\'s abandoned.)\nAt the beginning of your end step, you may put target creature card from an opponent\'s graveyard onto the battlefield under your control.\nWhen a creature put onto the battlefield with this scheme dies, abandon this scheme.').
card_layout('my undead horde awakens', 'scheme').

% Found in: ARC
card_name('my wish is your command', 'My Wish Is Your Command').
card_type('my wish is your command', 'Scheme').
card_types('my wish is your command', ['Scheme']).
card_subtypes('my wish is your command', []).
card_colors('my wish is your command', []).
card_text('my wish is your command', 'When you set this scheme in motion, each opponent reveals his or her hand. You may choose a noncreature, nonland card revealed this way and cast it without paying its mana cost.').
card_layout('my wish is your command', 'scheme').

% Found in: ARB, pWPN
card_name('mycoid shepherd', 'Mycoid Shepherd').
card_type('mycoid shepherd', 'Creature — Fungus').
card_types('mycoid shepherd', ['Creature']).
card_subtypes('mycoid shepherd', ['Fungus']).
card_colors('mycoid shepherd', ['W', 'G']).
card_text('mycoid shepherd', 'Whenever Mycoid Shepherd or another creature you control with power 5 or greater dies, you may gain 5 life.').
card_mana_cost('mycoid shepherd', ['1', 'G', 'G', 'W']).
card_cmc('mycoid shepherd', 4).
card_layout('mycoid shepherd', 'normal').
card_power('mycoid shepherd', 5).
card_toughness('mycoid shepherd', 4).

% Found in: PLC
card_name('mycologist', 'Mycologist').
card_type('mycologist', 'Creature — Human Druid').
card_types('mycologist', ['Creature']).
card_subtypes('mycologist', ['Human', 'Druid']).
card_colors('mycologist', ['W']).
card_text('mycologist', 'At the beginning of your upkeep, put a spore counter on Mycologist.\nRemove three spore counters from Mycologist: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: You gain 2 life.').
card_mana_cost('mycologist', ['1', 'W']).
card_cmc('mycologist', 2).
card_layout('mycologist', 'normal').
card_power('mycologist', 0).
card_toughness('mycologist', 2).

% Found in: ALA, PC2
card_name('mycoloth', 'Mycoloth').
card_type('mycoloth', 'Creature — Fungus').
card_types('mycoloth', ['Creature']).
card_subtypes('mycoloth', ['Fungus']).
card_colors('mycoloth', ['G']).
card_text('mycoloth', 'Devour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\nAt the beginning of your upkeep, put a 1/1 green Saproling creature token onto the battlefield for each +1/+1 counter on Mycoloth.').
card_mana_cost('mycoloth', ['3', 'G', 'G']).
card_cmc('mycoloth', 5).
card_layout('mycoloth', 'normal').
card_power('mycoloth', 4).
card_toughness('mycoloth', 4).

% Found in: NPH
card_name('mycosynth fiend', 'Mycosynth Fiend').
card_type('mycosynth fiend', 'Creature — Horror').
card_types('mycosynth fiend', ['Creature']).
card_subtypes('mycosynth fiend', ['Horror']).
card_colors('mycosynth fiend', ['G']).
card_text('mycosynth fiend', 'Mycosynth Fiend gets +1/+1 for each poison counter your opponents have.').
card_mana_cost('mycosynth fiend', ['2', 'G']).
card_cmc('mycosynth fiend', 3).
card_layout('mycosynth fiend', 'normal').
card_power('mycosynth fiend', 2).
card_toughness('mycosynth fiend', 2).

% Found in: 5DN
card_name('mycosynth golem', 'Mycosynth Golem').
card_type('mycosynth golem', 'Artifact Creature — Golem').
card_types('mycosynth golem', ['Artifact', 'Creature']).
card_subtypes('mycosynth golem', ['Golem']).
card_colors('mycosynth golem', []).
card_text('mycosynth golem', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nArtifact creature spells you cast have affinity for artifacts. (They cost {1} less to cast for each artifact you control.)').
card_mana_cost('mycosynth golem', ['11']).
card_cmc('mycosynth golem', 11).
card_layout('mycosynth golem', 'normal').
card_power('mycosynth golem', 4).
card_toughness('mycosynth golem', 5).

% Found in: DST
card_name('mycosynth lattice', 'Mycosynth Lattice').
card_type('mycosynth lattice', 'Artifact').
card_types('mycosynth lattice', ['Artifact']).
card_subtypes('mycosynth lattice', []).
card_colors('mycosynth lattice', []).
card_text('mycosynth lattice', 'All permanents are artifacts in addition to their other types.\nAll cards that aren\'t on the battlefield, spells, and permanents are colorless.\nPlayers may spend mana as though it were mana of any color.').
card_mana_cost('mycosynth lattice', ['6']).
card_cmc('mycosynth lattice', 6).
card_layout('mycosynth lattice', 'normal').

% Found in: C14, NPH
card_name('mycosynth wellspring', 'Mycosynth Wellspring').
card_type('mycosynth wellspring', 'Artifact').
card_types('mycosynth wellspring', ['Artifact']).
card_subtypes('mycosynth wellspring', []).
card_colors('mycosynth wellspring', []).
card_text('mycosynth wellspring', 'When Mycosynth Wellspring enters the battlefield or is put into a graveyard from the battlefield, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('mycosynth wellspring', ['2']).
card_cmc('mycosynth wellspring', 2).
card_layout('mycosynth wellspring', 'normal').

% Found in: CHK
card_name('myojin of cleansing fire', 'Myojin of Cleansing Fire').
card_type('myojin of cleansing fire', 'Legendary Creature — Spirit').
card_types('myojin of cleansing fire', ['Creature']).
card_subtypes('myojin of cleansing fire', ['Spirit']).
card_supertypes('myojin of cleansing fire', ['Legendary']).
card_colors('myojin of cleansing fire', ['W']).
card_text('myojin of cleansing fire', 'Myojin of Cleansing Fire enters the battlefield with a divinity counter on it if you cast it from your hand.\nMyojin of Cleansing Fire has indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Cleansing Fire: Destroy all other creatures.').
card_mana_cost('myojin of cleansing fire', ['5', 'W', 'W', 'W']).
card_cmc('myojin of cleansing fire', 8).
card_layout('myojin of cleansing fire', 'normal').
card_power('myojin of cleansing fire', 4).
card_toughness('myojin of cleansing fire', 6).

% Found in: CHK
card_name('myojin of infinite rage', 'Myojin of Infinite Rage').
card_type('myojin of infinite rage', 'Legendary Creature — Spirit').
card_types('myojin of infinite rage', ['Creature']).
card_subtypes('myojin of infinite rage', ['Spirit']).
card_supertypes('myojin of infinite rage', ['Legendary']).
card_colors('myojin of infinite rage', ['R']).
card_text('myojin of infinite rage', 'Myojin of Infinite Rage enters the battlefield with a divinity counter on it if you cast it from your hand.\nMyojin of Infinite Rage has indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Infinite Rage: Destroy all lands.').
card_mana_cost('myojin of infinite rage', ['7', 'R', 'R', 'R']).
card_cmc('myojin of infinite rage', 10).
card_layout('myojin of infinite rage', 'normal').
card_power('myojin of infinite rage', 7).
card_toughness('myojin of infinite rage', 4).

% Found in: CHK
card_name('myojin of life\'s web', 'Myojin of Life\'s Web').
card_type('myojin of life\'s web', 'Legendary Creature — Spirit').
card_types('myojin of life\'s web', ['Creature']).
card_subtypes('myojin of life\'s web', ['Spirit']).
card_supertypes('myojin of life\'s web', ['Legendary']).
card_colors('myojin of life\'s web', ['G']).
card_text('myojin of life\'s web', 'Myojin of Life\'s Web enters the battlefield with a divinity counter on it if you cast it from your hand.\nMyojin of Life\'s Web has indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Life\'s Web: Put any number of creature cards from your hand onto the battlefield.').
card_mana_cost('myojin of life\'s web', ['6', 'G', 'G', 'G']).
card_cmc('myojin of life\'s web', 9).
card_layout('myojin of life\'s web', 'normal').
card_power('myojin of life\'s web', 8).
card_toughness('myojin of life\'s web', 8).

% Found in: CHK
card_name('myojin of night\'s reach', 'Myojin of Night\'s Reach').
card_type('myojin of night\'s reach', 'Legendary Creature — Spirit').
card_types('myojin of night\'s reach', ['Creature']).
card_subtypes('myojin of night\'s reach', ['Spirit']).
card_supertypes('myojin of night\'s reach', ['Legendary']).
card_colors('myojin of night\'s reach', ['B']).
card_text('myojin of night\'s reach', 'Myojin of Night\'s Reach enters the battlefield with a divinity counter on it if you cast it from your hand.\nMyojin of Night\'s Reach has indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Night\'s Reach: Each opponent discards his or her hand.').
card_mana_cost('myojin of night\'s reach', ['5', 'B', 'B', 'B']).
card_cmc('myojin of night\'s reach', 8).
card_layout('myojin of night\'s reach', 'normal').
card_power('myojin of night\'s reach', 5).
card_toughness('myojin of night\'s reach', 2).

% Found in: CHK
card_name('myojin of seeing winds', 'Myojin of Seeing Winds').
card_type('myojin of seeing winds', 'Legendary Creature — Spirit').
card_types('myojin of seeing winds', ['Creature']).
card_subtypes('myojin of seeing winds', ['Spirit']).
card_supertypes('myojin of seeing winds', ['Legendary']).
card_colors('myojin of seeing winds', ['U']).
card_text('myojin of seeing winds', 'Myojin of Seeing Winds enters the battlefield with a divinity counter on it if you cast it from your hand.\nMyojin of Seeing Winds has indestructible as long as it has a divinity counter on it.\nRemove a divinity counter from Myojin of Seeing Winds: Draw a card for each permanent you control.').
card_mana_cost('myojin of seeing winds', ['7', 'U', 'U', 'U']).
card_cmc('myojin of seeing winds', 10).
card_layout('myojin of seeing winds', 'normal').
card_power('myojin of seeing winds', 3).
card_toughness('myojin of seeing winds', 3).

% Found in: MRD
card_name('myr adapter', 'Myr Adapter').
card_type('myr adapter', 'Artifact Creature — Myr').
card_types('myr adapter', ['Artifact', 'Creature']).
card_subtypes('myr adapter', ['Myr']).
card_colors('myr adapter', []).
card_text('myr adapter', 'Myr Adapter gets +1/+1 for each Equipment attached to it.').
card_mana_cost('myr adapter', ['3']).
card_cmc('myr adapter', 3).
card_layout('myr adapter', 'normal').
card_power('myr adapter', 1).
card_toughness('myr adapter', 1).

% Found in: C13, C14, SOM
card_name('myr battlesphere', 'Myr Battlesphere').
card_type('myr battlesphere', 'Artifact Creature — Myr Construct').
card_types('myr battlesphere', ['Artifact', 'Creature']).
card_subtypes('myr battlesphere', ['Myr', 'Construct']).
card_colors('myr battlesphere', []).
card_text('myr battlesphere', 'When Myr Battlesphere enters the battlefield, put four 1/1 colorless Myr artifact creature tokens onto the battlefield.\nWhenever Myr Battlesphere attacks, you may tap X untapped Myr you control. If you do, Myr Battlesphere gets +X/+0 until end of turn and deals X damage to defending player.').
card_mana_cost('myr battlesphere', ['7']).
card_cmc('myr battlesphere', 7).
card_layout('myr battlesphere', 'normal').
card_power('myr battlesphere', 4).
card_toughness('myr battlesphere', 7).

% Found in: HOP, MM2, MMA, MRD, pFNM
card_name('myr enforcer', 'Myr Enforcer').
card_type('myr enforcer', 'Artifact Creature — Myr').
card_types('myr enforcer', ['Artifact', 'Creature']).
card_subtypes('myr enforcer', ['Myr']).
card_colors('myr enforcer', []).
card_text('myr enforcer', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)').
card_mana_cost('myr enforcer', ['7']).
card_cmc('myr enforcer', 7).
card_layout('myr enforcer', 'normal').
card_power('myr enforcer', 4).
card_toughness('myr enforcer', 4).

% Found in: SOM
card_name('myr galvanizer', 'Myr Galvanizer').
card_type('myr galvanizer', 'Artifact Creature — Myr').
card_types('myr galvanizer', ['Artifact', 'Creature']).
card_subtypes('myr galvanizer', ['Myr']).
card_colors('myr galvanizer', []).
card_text('myr galvanizer', 'Other Myr creatures you control get +1/+1.\n{1}, {T}: Untap each other Myr you control.').
card_mana_cost('myr galvanizer', ['3']).
card_cmc('myr galvanizer', 3).
card_layout('myr galvanizer', 'normal').
card_power('myr galvanizer', 2).
card_toughness('myr galvanizer', 2).

% Found in: MRD
card_name('myr incubator', 'Myr Incubator').
card_type('myr incubator', 'Artifact').
card_types('myr incubator', ['Artifact']).
card_subtypes('myr incubator', []).
card_colors('myr incubator', []).
card_text('myr incubator', '{6}, {T}, Sacrifice Myr Incubator: Search your library for any number of artifact cards, exile them, then put that many 1/1 colorless Myr artifact creature tokens onto the battlefield. Then shuffle your library.').
card_mana_cost('myr incubator', ['6']).
card_cmc('myr incubator', 6).
card_layout('myr incubator', 'normal').

% Found in: DST
card_name('myr landshaper', 'Myr Landshaper').
card_type('myr landshaper', 'Artifact Creature — Myr').
card_types('myr landshaper', ['Artifact', 'Creature']).
card_subtypes('myr landshaper', ['Myr']).
card_colors('myr landshaper', []).
card_text('myr landshaper', '{T}: Target land becomes an artifact in addition to its other types until end of turn.').
card_mana_cost('myr landshaper', ['3']).
card_cmc('myr landshaper', 3).
card_layout('myr landshaper', 'normal').
card_power('myr landshaper', 1).
card_toughness('myr landshaper', 1).

% Found in: DST
card_name('myr matrix', 'Myr Matrix').
card_type('myr matrix', 'Artifact').
card_types('myr matrix', ['Artifact']).
card_subtypes('myr matrix', []).
card_colors('myr matrix', []).
card_text('myr matrix', 'Indestructible (Effects that say \"destroy\" don\'t destroy this artifact.)\nMyr creatures get +1/+1.\n{5}: Put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_mana_cost('myr matrix', ['5']).
card_cmc('myr matrix', 5).
card_layout('myr matrix', 'normal').

% Found in: MRD
card_name('myr mindservant', 'Myr Mindservant').
card_type('myr mindservant', 'Artifact Creature — Myr').
card_types('myr mindservant', ['Artifact', 'Creature']).
card_subtypes('myr mindservant', ['Myr']).
card_colors('myr mindservant', []).
card_text('myr mindservant', '{2}, {T}: Shuffle your library.').
card_mana_cost('myr mindservant', ['1']).
card_cmc('myr mindservant', 1).
card_layout('myr mindservant', 'normal').
card_power('myr mindservant', 1).
card_toughness('myr mindservant', 1).

% Found in: DST
card_name('myr moonvessel', 'Myr Moonvessel').
card_type('myr moonvessel', 'Artifact Creature — Myr').
card_types('myr moonvessel', ['Artifact', 'Creature']).
card_subtypes('myr moonvessel', ['Myr']).
card_colors('myr moonvessel', []).
card_text('myr moonvessel', 'When Myr Moonvessel dies, add {1} to your mana pool.').
card_mana_cost('myr moonvessel', ['1']).
card_cmc('myr moonvessel', 1).
card_layout('myr moonvessel', 'normal').
card_power('myr moonvessel', 1).
card_toughness('myr moonvessel', 1).

% Found in: SOM
card_name('myr propagator', 'Myr Propagator').
card_type('myr propagator', 'Artifact Creature — Myr').
card_types('myr propagator', ['Artifact', 'Creature']).
card_subtypes('myr propagator', ['Myr']).
card_colors('myr propagator', []).
card_text('myr propagator', '{3}, {T}: Put a token that\'s a copy of Myr Propagator onto the battlefield.').
card_mana_cost('myr propagator', ['3']).
card_cmc('myr propagator', 3).
card_layout('myr propagator', 'normal').
card_power('myr propagator', 1).
card_toughness('myr propagator', 1).

% Found in: MRD
card_name('myr prototype', 'Myr Prototype').
card_type('myr prototype', 'Artifact Creature — Myr').
card_types('myr prototype', ['Artifact', 'Creature']).
card_subtypes('myr prototype', ['Myr']).
card_colors('myr prototype', []).
card_text('myr prototype', 'At the beginning of your upkeep, put a +1/+1 counter on Myr Prototype.\nMyr Prototype can\'t attack or block unless you pay {1} for each +1/+1 counter on it.').
card_mana_cost('myr prototype', ['5']).
card_cmc('myr prototype', 5).
card_layout('myr prototype', 'normal').
card_power('myr prototype', 2).
card_toughness('myr prototype', 2).

% Found in: 5DN
card_name('myr quadropod', 'Myr Quadropod').
card_type('myr quadropod', 'Artifact Creature — Myr').
card_types('myr quadropod', ['Artifact', 'Creature']).
card_subtypes('myr quadropod', ['Myr']).
card_colors('myr quadropod', []).
card_text('myr quadropod', '{3}: Switch Myr Quadropod\'s power and toughness until end of turn.').
card_mana_cost('myr quadropod', ['4']).
card_cmc('myr quadropod', 4).
card_layout('myr quadropod', 'normal').
card_power('myr quadropod', 1).
card_toughness('myr quadropod', 4).

% Found in: SOM
card_name('myr reservoir', 'Myr Reservoir').
card_type('myr reservoir', 'Artifact').
card_types('myr reservoir', ['Artifact']).
card_subtypes('myr reservoir', []).
card_colors('myr reservoir', []).
card_text('myr reservoir', '{T}: Add {2} to your mana pool. Spend this mana only to cast Myr spells or activate abilities of Myr.\n{3}, {T}: Return target Myr card from your graveyard to your hand.').
card_mana_cost('myr reservoir', ['3']).
card_cmc('myr reservoir', 3).
card_layout('myr reservoir', 'normal').

% Found in: C14, MMA, MRD
card_name('myr retriever', 'Myr Retriever').
card_type('myr retriever', 'Artifact Creature — Myr').
card_types('myr retriever', ['Artifact', 'Creature']).
card_subtypes('myr retriever', ['Myr']).
card_colors('myr retriever', []).
card_text('myr retriever', 'When Myr Retriever dies, return another target artifact card from your graveyard to your hand.').
card_mana_cost('myr retriever', ['2']).
card_cmc('myr retriever', 2).
card_layout('myr retriever', 'normal').
card_power('myr retriever', 1).
card_toughness('myr retriever', 1).

% Found in: 5DN
card_name('myr servitor', 'Myr Servitor').
card_type('myr servitor', 'Artifact Creature — Myr').
card_types('myr servitor', ['Artifact', 'Creature']).
card_subtypes('myr servitor', ['Myr']).
card_colors('myr servitor', []).
card_text('myr servitor', 'At the beginning of your upkeep, if Myr Servitor is on the battlefield, each player returns all cards named Myr Servitor from his or her graveyard to the battlefield.').
card_mana_cost('myr servitor', ['1']).
card_cmc('myr servitor', 1).
card_layout('myr servitor', 'normal').
card_power('myr servitor', 1).
card_toughness('myr servitor', 1).

% Found in: C14, MBS
card_name('myr sire', 'Myr Sire').
card_type('myr sire', 'Artifact Creature — Myr').
card_types('myr sire', ['Artifact', 'Creature']).
card_subtypes('myr sire', ['Myr']).
card_colors('myr sire', []).
card_text('myr sire', 'When Myr Sire dies, put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_mana_cost('myr sire', ['2']).
card_cmc('myr sire', 2).
card_layout('myr sire', 'normal').
card_power('myr sire', 1).
card_toughness('myr sire', 1).

% Found in: NPH, pMGD
card_name('myr superion', 'Myr Superion').
card_type('myr superion', 'Artifact Creature — Myr').
card_types('myr superion', ['Artifact', 'Creature']).
card_subtypes('myr superion', ['Myr']).
card_colors('myr superion', []).
card_text('myr superion', 'Spend only mana produced by creatures to cast Myr Superion.').
card_mana_cost('myr superion', ['2']).
card_cmc('myr superion', 2).
card_layout('myr superion', 'normal').
card_power('myr superion', 5).
card_toughness('myr superion', 6).

% Found in: MBS
card_name('myr turbine', 'Myr Turbine').
card_type('myr turbine', 'Artifact').
card_types('myr turbine', ['Artifact']).
card_subtypes('myr turbine', []).
card_colors('myr turbine', []).
card_text('myr turbine', '{T}: Put a 1/1 colorless Myr artifact creature token onto the battlefield.\n{T}, Tap five untapped Myr you control: Search your library for a Myr creature card, put it onto the battlefield, then shuffle your library.').
card_mana_cost('myr turbine', ['5']).
card_cmc('myr turbine', 5).
card_layout('myr turbine', 'normal').

% Found in: MBS
card_name('myr welder', 'Myr Welder').
card_type('myr welder', 'Artifact Creature — Myr').
card_types('myr welder', ['Artifact', 'Creature']).
card_subtypes('myr welder', ['Myr']).
card_colors('myr welder', []).
card_text('myr welder', 'Imprint — {T}: Exile target artifact card from a graveyard.\nMyr Welder has all activated abilities of all cards exiled with it.').
card_mana_cost('myr welder', ['3']).
card_cmc('myr welder', 3).
card_layout('myr welder', 'normal').
card_power('myr welder', 1).
card_toughness('myr welder', 4).

% Found in: C14
card_name('myriad landscape', 'Myriad Landscape').
card_type('myriad landscape', 'Land').
card_types('myriad landscape', ['Land']).
card_subtypes('myriad landscape', []).
card_colors('myriad landscape', []).
card_text('myriad landscape', 'Myriad Landscape enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Myriad Landscape: Search your library for up to two basic land cards that share a land type, put them onto the battlefield tapped, then shuffle your library.').
card_layout('myriad landscape', 'normal').

% Found in: MM2, SOM
card_name('myrsmith', 'Myrsmith').
card_type('myrsmith', 'Creature — Human Artificer').
card_types('myrsmith', ['Creature']).
card_subtypes('myrsmith', ['Human', 'Artificer']).
card_colors('myrsmith', ['W']).
card_text('myrsmith', 'Whenever you cast an artifact spell, you may pay {1}. If you do, put a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_mana_cost('myrsmith', ['1', 'W']).
card_cmc('myrsmith', 2).
card_layout('myrsmith', 'normal').
card_power('myrsmith', 2).
card_toughness('myrsmith', 1).

% Found in: WWK
card_name('mysteries of the deep', 'Mysteries of the Deep').
card_type('mysteries of the deep', 'Instant').
card_types('mysteries of the deep', ['Instant']).
card_subtypes('mysteries of the deep', []).
card_colors('mysteries of the deep', ['U']).
card_text('mysteries of the deep', 'Draw two cards.\nLandfall — If you had a land enter the battlefield under your control this turn, draw three cards instead.').
card_mana_cost('mysteries of the deep', ['4', 'U']).
card_cmc('mysteries of the deep', 5).
card_layout('mysteries of the deep', 'normal').

% Found in: C13
card_name('mystic barrier', 'Mystic Barrier').
card_type('mystic barrier', 'Enchantment').
card_types('mystic barrier', ['Enchantment']).
card_subtypes('mystic barrier', []).
card_colors('mystic barrier', ['W']).
card_text('mystic barrier', 'When Mystic Barrier enters the battlefield or at the beginning of your upkeep, choose left or right.\nEach player may attack only the opponent seated nearest him or her in the last chosen direction and planeswalkers controlled by that player.').
card_mana_cost('mystic barrier', ['4', 'W']).
card_cmc('mystic barrier', 5).
card_layout('mystic barrier', 'normal').

% Found in: 6ED, ALL
card_name('mystic compass', 'Mystic Compass').
card_type('mystic compass', 'Artifact').
card_types('mystic compass', ['Artifact']).
card_subtypes('mystic compass', []).
card_colors('mystic compass', []).
card_text('mystic compass', '{1}, {T}: Target land becomes the basic land type of your choice until end of turn.').
card_mana_cost('mystic compass', ['2']).
card_cmc('mystic compass', 2).
card_layout('mystic compass', 'normal').

% Found in: ODY
card_name('mystic crusader', 'Mystic Crusader').
card_type('mystic crusader', 'Creature — Human Nomad Mystic').
card_types('mystic crusader', ['Creature']).
card_subtypes('mystic crusader', ['Human', 'Nomad', 'Mystic']).
card_colors('mystic crusader', ['W']).
card_text('mystic crusader', 'Protection from black and from red\nThreshold — As long as seven or more cards are in your graveyard, Mystic Crusader gets +1/+1 and has flying.').
card_mana_cost('mystic crusader', ['1', 'W', 'W']).
card_cmc('mystic crusader', 3).
card_layout('mystic crusader', 'normal').
card_power('mystic crusader', 2).
card_toughness('mystic crusader', 1).

% Found in: HML, ME4
card_name('mystic decree', 'Mystic Decree').
card_type('mystic decree', 'World Enchantment').
card_types('mystic decree', ['Enchantment']).
card_subtypes('mystic decree', []).
card_supertypes('mystic decree', ['World']).
card_colors('mystic decree', ['U']).
card_text('mystic decree', 'All creatures lose flying and islandwalk.').
card_mana_cost('mystic decree', ['2', 'U', 'U']).
card_cmc('mystic decree', 4).
card_layout('mystic decree', 'normal').
card_reserved('mystic decree').

% Found in: PO2, POR, PTK
card_name('mystic denial', 'Mystic Denial').
card_type('mystic denial', 'Instant').
card_types('mystic denial', ['Instant']).
card_subtypes('mystic denial', []).
card_colors('mystic denial', ['U']).
card_text('mystic denial', 'Counter target creature or sorcery spell.').
card_mana_cost('mystic denial', ['1', 'U', 'U']).
card_cmc('mystic denial', 3).
card_layout('mystic denial', 'normal').

% Found in: ODY, TSB
card_name('mystic enforcer', 'Mystic Enforcer').
card_type('mystic enforcer', 'Creature — Human Nomad Mystic').
card_types('mystic enforcer', ['Creature']).
card_subtypes('mystic enforcer', ['Human', 'Nomad', 'Mystic']).
card_colors('mystic enforcer', ['W', 'G']).
card_text('mystic enforcer', 'Protection from black\nThreshold — As long as seven or more cards are in your graveyard, Mystic Enforcer gets +3/+3 and has flying.').
card_mana_cost('mystic enforcer', ['2', 'G', 'W']).
card_cmc('mystic enforcer', 4).
card_layout('mystic enforcer', 'normal').
card_power('mystic enforcer', 3).
card_toughness('mystic enforcer', 3).

% Found in: TOR
card_name('mystic familiar', 'Mystic Familiar').
card_type('mystic familiar', 'Creature — Bird').
card_types('mystic familiar', ['Creature']).
card_subtypes('mystic familiar', ['Bird']).
card_colors('mystic familiar', ['W']).
card_text('mystic familiar', 'Flying\nThreshold — As long as seven or more cards are in your graveyard, Mystic Familiar gets +1/+1 and has protection from black.').
card_mana_cost('mystic familiar', ['1', 'W']).
card_cmc('mystic familiar', 2).
card_layout('mystic familiar', 'normal').
card_power('mystic familiar', 1).
card_toughness('mystic familiar', 2).

% Found in: SHM
card_name('mystic gate', 'Mystic Gate').
card_type('mystic gate', 'Land').
card_types('mystic gate', ['Land']).
card_subtypes('mystic gate', []).
card_colors('mystic gate', []).
card_text('mystic gate', '{T}: Add {1} to your mana pool.\n{W/U}, {T}: Add {W}{W}, {W}{U}, or {U}{U} to your mana pool.').
card_layout('mystic gate', 'normal').

% Found in: GTC
card_name('mystic genesis', 'Mystic Genesis').
card_type('mystic genesis', 'Instant').
card_types('mystic genesis', ['Instant']).
card_subtypes('mystic genesis', []).
card_colors('mystic genesis', ['U', 'G']).
card_text('mystic genesis', 'Counter target spell. Put an X/X green Ooze creature token onto the battlefield, where X is that spell\'s converted mana cost.').
card_mana_cost('mystic genesis', ['2', 'G', 'U', 'U']).
card_cmc('mystic genesis', 5).
card_layout('mystic genesis', 'normal').

% Found in: DTK
card_name('mystic meditation', 'Mystic Meditation').
card_type('mystic meditation', 'Sorcery').
card_types('mystic meditation', ['Sorcery']).
card_subtypes('mystic meditation', []).
card_colors('mystic meditation', ['U']).
card_text('mystic meditation', 'Draw three cards. Then discard two cards unless you discard a creature card.').
card_mana_cost('mystic meditation', ['3', 'U']).
card_cmc('mystic meditation', 4).
card_layout('mystic meditation', 'normal').

% Found in: CSP
card_name('mystic melting', 'Mystic Melting').
card_type('mystic melting', 'Instant').
card_types('mystic melting', ['Instant']).
card_subtypes('mystic melting', []).
card_colors('mystic melting', ['G']).
card_text('mystic melting', 'Destroy target artifact or enchantment.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('mystic melting', ['3', 'G']).
card_cmc('mystic melting', 4).
card_layout('mystic melting', 'normal').

% Found in: ICE
card_name('mystic might', 'Mystic Might').
card_type('mystic might', 'Enchantment — Aura').
card_types('mystic might', ['Enchantment']).
card_subtypes('mystic might', ['Aura']).
card_colors('mystic might', ['U']).
card_text('mystic might', 'Enchant land you control\nCumulative upkeep {1}{U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nEnchanted land has \"{T}: Target creature gets +2/+2 until end of turn.\"').
card_mana_cost('mystic might', ['U']).
card_cmc('mystic might', 1).
card_layout('mystic might', 'normal').
card_reserved('mystic might').

% Found in: DDN, KTK
card_name('mystic monastery', 'Mystic Monastery').
card_type('mystic monastery', 'Land').
card_types('mystic monastery', ['Land']).
card_subtypes('mystic monastery', []).
card_colors('mystic monastery', []).
card_text('mystic monastery', 'Mystic Monastery enters the battlefield tapped.\n{T}: Add {U}, {R}, or {W} to your mana pool.').
card_layout('mystic monastery', 'normal').

% Found in: FRF_UGIN, KTK
card_name('mystic of the hidden way', 'Mystic of the Hidden Way').
card_type('mystic of the hidden way', 'Creature — Human Monk').
card_types('mystic of the hidden way', ['Creature']).
card_subtypes('mystic of the hidden way', ['Human', 'Monk']).
card_colors('mystic of the hidden way', ['U']).
card_text('mystic of the hidden way', 'Mystic of the Hidden Way can\'t be blocked.\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('mystic of the hidden way', ['4', 'U']).
card_cmc('mystic of the hidden way', 5).
card_layout('mystic of the hidden way', 'normal').
card_power('mystic of the hidden way', 3).
card_toughness('mystic of the hidden way', 2).

% Found in: ODY
card_name('mystic penitent', 'Mystic Penitent').
card_type('mystic penitent', 'Creature — Human Nomad Mystic').
card_types('mystic penitent', ['Creature']).
card_subtypes('mystic penitent', ['Human', 'Nomad', 'Mystic']).
card_colors('mystic penitent', ['W']).
card_text('mystic penitent', 'Vigilance\nThreshold — As long as seven or more cards are in your graveyard, Mystic Penitent gets +1/+1 and has flying.').
card_mana_cost('mystic penitent', ['W']).
card_cmc('mystic penitent', 1).
card_layout('mystic penitent', 'normal').
card_power('mystic penitent', 1).
card_toughness('mystic penitent', 1).

% Found in: ICE, MED
card_name('mystic remora', 'Mystic Remora').
card_type('mystic remora', 'Enchantment').
card_types('mystic remora', ['Enchantment']).
card_subtypes('mystic remora', []).
card_colors('mystic remora', ['U']).
card_text('mystic remora', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhenever an opponent casts a noncreature spell, you may draw a card unless that player pays {4}.').
card_mana_cost('mystic remora', ['U']).
card_cmc('mystic remora', 1).
card_layout('mystic remora', 'normal').

% Found in: CHK
card_name('mystic restraints', 'Mystic Restraints').
card_type('mystic restraints', 'Enchantment — Aura').
card_types('mystic restraints', ['Enchantment']).
card_subtypes('mystic restraints', ['Aura']).
card_colors('mystic restraints', ['U']).
card_text('mystic restraints', 'Flash\nEnchant creature\nWhen Mystic Restraints enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('mystic restraints', ['2', 'U', 'U']).
card_cmc('mystic restraints', 4).
card_layout('mystic restraints', 'normal').

% Found in: DKA
card_name('mystic retrieval', 'Mystic Retrieval').
card_type('mystic retrieval', 'Sorcery').
card_types('mystic retrieval', ['Sorcery']).
card_subtypes('mystic retrieval', []).
card_colors('mystic retrieval', ['U']).
card_text('mystic retrieval', 'Return target instant or sorcery card from your graveyard to your hand.\nFlashback {2}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('mystic retrieval', ['3', 'U']).
card_cmc('mystic retrieval', 4).
card_layout('mystic retrieval', 'normal').

% Found in: APC, MM2, TSB
card_name('mystic snake', 'Mystic Snake').
card_type('mystic snake', 'Creature — Snake').
card_types('mystic snake', ['Creature']).
card_subtypes('mystic snake', ['Snake']).
card_colors('mystic snake', ['U', 'G']).
card_text('mystic snake', 'Flash (You may cast this spell any time you could cast an instant.)\nWhen Mystic Snake enters the battlefield, counter target spell.').
card_mana_cost('mystic snake', ['1', 'G', 'U', 'U']).
card_cmc('mystic snake', 4).
card_layout('mystic snake', 'normal').
card_power('mystic snake', 2).
card_toughness('mystic snake', 2).

% Found in: FUT
card_name('mystic speculation', 'Mystic Speculation').
card_type('mystic speculation', 'Sorcery').
card_types('mystic speculation', ['Sorcery']).
card_subtypes('mystic speculation', []).
card_colors('mystic speculation', ['U']).
card_text('mystic speculation', 'Buyback {2} (You may pay an additional {2} as you cast this spell. If you do, put this card into your hand as it resolves.)\nScry 3. (Look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('mystic speculation', ['U']).
card_cmc('mystic speculation', 1).
card_layout('mystic speculation', 'normal').

% Found in: VIS
card_name('mystic veil', 'Mystic Veil').
card_type('mystic veil', 'Enchantment — Aura').
card_types('mystic veil', ['Enchantment']).
card_subtypes('mystic veil', ['Aura']).
card_colors('mystic veil', ['U']).
card_text('mystic veil', 'You may cast Mystic Veil as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nEnchant creature\nEnchanted creature has shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('mystic veil', ['1', 'U']).
card_cmc('mystic veil', 2).
card_layout('mystic veil', 'normal').

% Found in: ODY
card_name('mystic visionary', 'Mystic Visionary').
card_type('mystic visionary', 'Creature — Human Nomad Mystic').
card_types('mystic visionary', ['Creature']).
card_subtypes('mystic visionary', ['Human', 'Nomad', 'Mystic']).
card_colors('mystic visionary', ['W']).
card_text('mystic visionary', 'Threshold — Mystic Visionary has flying as long as seven or more cards are in your graveyard.').
card_mana_cost('mystic visionary', ['1', 'W']).
card_cmc('mystic visionary', 2).
card_layout('mystic visionary', 'normal').
card_power('mystic visionary', 2).
card_toughness('mystic visionary', 1).

% Found in: ODY, VMA
card_name('mystic zealot', 'Mystic Zealot').
card_type('mystic zealot', 'Creature — Human Nomad Mystic').
card_types('mystic zealot', ['Creature']).
card_subtypes('mystic zealot', ['Human', 'Nomad', 'Mystic']).
card_colors('mystic zealot', ['W']).
card_text('mystic zealot', 'Threshold — As long as seven or more cards are in your graveyard, Mystic Zealot gets +1/+1 and has flying.').
card_mana_cost('mystic zealot', ['3', 'W']).
card_cmc('mystic zealot', 4).
card_layout('mystic zealot', 'normal').
card_power('mystic zealot', 2).
card_toughness('mystic zealot', 4).

% Found in: TSP
card_name('mystical teachings', 'Mystical Teachings').
card_type('mystical teachings', 'Instant').
card_types('mystical teachings', ['Instant']).
card_subtypes('mystical teachings', []).
card_colors('mystical teachings', ['U']).
card_text('mystical teachings', 'Search your library for an instant card or a card with flash, reveal it, and put it into your hand. Then shuffle your library.\nFlashback {5}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('mystical teachings', ['3', 'U']).
card_cmc('mystical teachings', 4).
card_layout('mystical teachings', 'normal').

% Found in: 6ED, MIR, V09
card_name('mystical tutor', 'Mystical Tutor').
card_type('mystical tutor', 'Instant').
card_types('mystical tutor', ['Instant']).
card_subtypes('mystical tutor', []).
card_colors('mystical tutor', ['U']).
card_text('mystical tutor', 'Search your library for an instant or sorcery card and reveal that card. Shuffle your library, then put the card on top of it.').
card_mana_cost('mystical tutor', ['U']).
card_cmc('mystical tutor', 1).
card_layout('mystical tutor', 'normal').

% Found in: M11
card_name('mystifying maze', 'Mystifying Maze').
card_type('mystifying maze', 'Land').
card_types('mystifying maze', ['Land']).
card_subtypes('mystifying maze', []).
card_colors('mystifying maze', []).
card_text('mystifying maze', '{T}: Add {1} to your mana pool.\n{4}, {T}: Exile target attacking creature an opponent controls. At the beginning of the next end step, return it to the battlefield tapped under its owner\'s control.').
card_layout('mystifying maze', 'normal').

% Found in: DTK
card_name('myth realized', 'Myth Realized').
card_type('myth realized', 'Enchantment').
card_types('myth realized', ['Enchantment']).
card_subtypes('myth realized', []).
card_colors('myth realized', ['W']).
card_text('myth realized', 'Whenever you cast a noncreature spell, put a lore counter on Myth Realized.\n{2}{W}: Put a lore counter on Myth Realized.\n{W}: Until end of turn, Myth Realized becomes a Monk Avatar creature in addition to its other types and gains \"This creature\'s power and toughness are each equal to the number of lore counters on it.\"').
card_mana_cost('myth realized', ['W']).
card_cmc('myth realized', 1).
card_layout('myth realized', 'normal').

% Found in: ONS
card_name('mythic proportions', 'Mythic Proportions').
card_type('mythic proportions', 'Enchantment — Aura').
card_types('mythic proportions', ['Enchantment']).
card_subtypes('mythic proportions', ['Aura']).
card_colors('mythic proportions', ['G']).
card_text('mythic proportions', 'Enchant creature\nEnchanted creature gets +8/+8 and has trample.').
card_mana_cost('mythic proportions', ['4', 'G', 'G', 'G']).
card_cmc('mythic proportions', 7).
card_layout('mythic proportions', 'normal').

% Found in: ICE, MED
card_name('márton stromgald', 'Márton Stromgald').
card_type('márton stromgald', 'Legendary Creature — Human Knight').
card_types('márton stromgald', ['Creature']).
card_subtypes('márton stromgald', ['Human', 'Knight']).
card_supertypes('márton stromgald', ['Legendary']).
card_colors('márton stromgald', ['R']).
card_text('márton stromgald', 'Whenever Márton Stromgald attacks, other attacking creatures get +1/+1 until end of turn for each attacking creature other than Márton Stromgald.\nWhenever Márton Stromgald blocks, other blocking creatures get +1/+1 until end of turn for each blocking creature other than Márton Stromgald.').
card_mana_cost('márton stromgald', ['2', 'R', 'R']).
card_cmc('márton stromgald', 4).
card_layout('márton stromgald', 'normal').
card_power('márton stromgald', 1).
card_toughness('márton stromgald', 1).
card_reserved('márton stromgald').

