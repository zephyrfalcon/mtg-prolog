% Card-specific data

% Found in: STH, TPR
card_name('lab rats', 'Lab Rats').
card_type('lab rats', 'Sorcery').
card_types('lab rats', ['Sorcery']).
card_subtypes('lab rats', []).
card_colors('lab rats', ['B']).
card_text('lab rats', 'Buyback {4} (You may pay an additional {4} as you cast this spell. If you do, put this card into your hand as it resolves.)\nPut a 1/1 black Rat creature token onto the battlefield.').
card_mana_cost('lab rats', ['B']).
card_cmc('lab rats', 1).
card_layout('lab rats', 'normal').

% Found in: ISD
card_name('laboratory maniac', 'Laboratory Maniac').
card_type('laboratory maniac', 'Creature — Human Wizard').
card_types('laboratory maniac', ['Creature']).
card_subtypes('laboratory maniac', ['Human', 'Wizard']).
card_colors('laboratory maniac', ['U']).
card_text('laboratory maniac', 'If you would draw a card while your library has no cards in it, you win the game instead.').
card_mana_cost('laboratory maniac', ['2', 'U']).
card_cmc('laboratory maniac', 3).
card_layout('laboratory maniac', 'normal').
card_power('laboratory maniac', 2).
card_toughness('laboratory maniac', 2).

% Found in: THS
card_name('labyrinth champion', 'Labyrinth Champion').
card_type('labyrinth champion', 'Creature — Human Warrior').
card_types('labyrinth champion', ['Creature']).
card_subtypes('labyrinth champion', ['Human', 'Warrior']).
card_colors('labyrinth champion', ['R']).
card_text('labyrinth champion', 'Heroic — Whenever you cast a spell that targets Labyrinth Champion, Labyrinth Champion deals 2 damage to target creature or player.').
card_mana_cost('labyrinth champion', ['3', 'R']).
card_cmc('labyrinth champion', 4).
card_layout('labyrinth champion', 'normal').
card_power('labyrinth champion', 2).
card_toughness('labyrinth champion', 2).

% Found in: 5ED, HML, ME3
card_name('labyrinth minotaur', 'Labyrinth Minotaur').
card_type('labyrinth minotaur', 'Creature — Minotaur').
card_types('labyrinth minotaur', ['Creature']).
card_subtypes('labyrinth minotaur', ['Minotaur']).
card_colors('labyrinth minotaur', ['U']).
card_text('labyrinth minotaur', 'Whenever Labyrinth Minotaur blocks a creature, that creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('labyrinth minotaur', ['3', 'U']).
card_cmc('labyrinth minotaur', 4).
card_layout('labyrinth minotaur', 'normal').
card_power('labyrinth minotaur', 1).
card_toughness('labyrinth minotaur', 4).

% Found in: NMS
card_name('laccolith grunt', 'Laccolith Grunt').
card_type('laccolith grunt', 'Creature — Beast').
card_types('laccolith grunt', ['Creature']).
card_subtypes('laccolith grunt', ['Beast']).
card_colors('laccolith grunt', ['R']).
card_text('laccolith grunt', 'Whenever Laccolith Grunt becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Grunt assigns no combat damage this turn.').
card_mana_cost('laccolith grunt', ['2', 'R']).
card_cmc('laccolith grunt', 3).
card_layout('laccolith grunt', 'normal').
card_power('laccolith grunt', 2).
card_toughness('laccolith grunt', 2).

% Found in: NMS
card_name('laccolith rig', 'Laccolith Rig').
card_type('laccolith rig', 'Enchantment — Aura').
card_types('laccolith rig', ['Enchantment']).
card_subtypes('laccolith rig', ['Aura']).
card_colors('laccolith rig', ['R']).
card_text('laccolith rig', 'Enchant creature\nWhenever enchanted creature becomes blocked, you may have it deal damage equal to its power to target creature. If you do, the first creature assigns no combat damage this turn.').
card_mana_cost('laccolith rig', ['R']).
card_cmc('laccolith rig', 1).
card_layout('laccolith rig', 'normal').

% Found in: NMS
card_name('laccolith titan', 'Laccolith Titan').
card_type('laccolith titan', 'Creature — Beast').
card_types('laccolith titan', ['Creature']).
card_subtypes('laccolith titan', ['Beast']).
card_colors('laccolith titan', ['R']).
card_text('laccolith titan', 'Whenever Laccolith Titan becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Titan assigns no combat damage this turn.').
card_mana_cost('laccolith titan', ['5', 'R', 'R']).
card_cmc('laccolith titan', 7).
card_layout('laccolith titan', 'normal').
card_power('laccolith titan', 6).
card_toughness('laccolith titan', 6).

% Found in: NMS
card_name('laccolith warrior', 'Laccolith Warrior').
card_type('laccolith warrior', 'Creature — Beast Warrior').
card_types('laccolith warrior', ['Creature']).
card_subtypes('laccolith warrior', ['Beast', 'Warrior']).
card_colors('laccolith warrior', ['R']).
card_text('laccolith warrior', 'Whenever Laccolith Warrior becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Warrior assigns no combat damage this turn.').
card_mana_cost('laccolith warrior', ['2', 'R', 'R']).
card_cmc('laccolith warrior', 4).
card_layout('laccolith warrior', 'normal').
card_power('laccolith warrior', 3).
card_toughness('laccolith warrior', 3).

% Found in: NMS
card_name('laccolith whelp', 'Laccolith Whelp').
card_type('laccolith whelp', 'Creature — Beast').
card_types('laccolith whelp', ['Creature']).
card_subtypes('laccolith whelp', ['Beast']).
card_colors('laccolith whelp', ['R']).
card_text('laccolith whelp', 'Whenever Laccolith Whelp becomes blocked, you may have it deal damage equal to its power to target creature. If you do, Laccolith Whelp assigns no combat damage this turn.').
card_mana_cost('laccolith whelp', ['R']).
card_cmc('laccolith whelp', 1).
card_layout('laccolith whelp', 'normal').
card_power('laccolith whelp', 1).
card_toughness('laccolith whelp', 1).

% Found in: LRW
card_name('lace with moonglove', 'Lace with Moonglove').
card_type('lace with moonglove', 'Instant').
card_types('lace with moonglove', ['Instant']).
card_subtypes('lace with moonglove', []).
card_colors('lace with moonglove', ['G']).
card_text('lace with moonglove', 'Target creature gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy that creature.)\nDraw a card.').
card_mana_cost('lace with moonglove', ['2', 'G']).
card_cmc('lace with moonglove', 3).
card_layout('lace with moonglove', 'normal').

% Found in: UNH
card_name('ladies\' knight', 'Ladies\' Knight').
card_type('ladies\' knight', 'Creature — Human Knight').
card_types('ladies\' knight', ['Creature']).
card_subtypes('ladies\' knight', ['Human', 'Knight']).
card_colors('ladies\' knight', ['W']).
card_text('ladies\' knight', 'Flying\nSpells that players wearing at least one item of women\'s clothing play cost {1} less to play. (Women\'s clothing is designed to be worn exclusively by women.)').
card_mana_cost('ladies\' knight', ['3', 'W']).
card_cmc('ladies\' knight', 4).
card_layout('ladies\' knight', 'normal').
card_power('ladies\' knight', 2).
card_toughness('ladies\' knight', 2).

% Found in: LEG, ME3
card_name('lady caleria', 'Lady Caleria').
card_type('lady caleria', 'Legendary Creature — Human Archer').
card_types('lady caleria', ['Creature']).
card_subtypes('lady caleria', ['Human', 'Archer']).
card_supertypes('lady caleria', ['Legendary']).
card_colors('lady caleria', ['W', 'G']).
card_text('lady caleria', '{T}: Lady Caleria deals 3 damage to target attacking or blocking creature.').
card_mana_cost('lady caleria', ['3', 'G', 'G', 'W', 'W']).
card_cmc('lady caleria', 7).
card_layout('lady caleria', 'normal').
card_power('lady caleria', 3).
card_toughness('lady caleria', 6).
card_reserved('lady caleria').

% Found in: LEG, ME3
card_name('lady evangela', 'Lady Evangela').
card_type('lady evangela', 'Legendary Creature — Human Cleric').
card_types('lady evangela', ['Creature']).
card_subtypes('lady evangela', ['Human', 'Cleric']).
card_supertypes('lady evangela', ['Legendary']).
card_colors('lady evangela', ['W', 'U', 'B']).
card_text('lady evangela', '{W}{B}, {T}: Prevent all combat damage that would be dealt by target creature this turn.').
card_mana_cost('lady evangela', ['W', 'U', 'B']).
card_cmc('lady evangela', 3).
card_layout('lady evangela', 'normal').
card_power('lady evangela', 1).
card_toughness('lady evangela', 2).
card_reserved('lady evangela').

% Found in: ATH, LEG, ME3
card_name('lady orca', 'Lady Orca').
card_type('lady orca', 'Legendary Creature — Demon').
card_types('lady orca', ['Creature']).
card_subtypes('lady orca', ['Demon']).
card_supertypes('lady orca', ['Legendary']).
card_colors('lady orca', ['B', 'R']).
card_text('lady orca', '').
card_mana_cost('lady orca', ['5', 'B', 'R']).
card_cmc('lady orca', 7).
card_layout('lady orca', 'normal').
card_power('lady orca', 7).
card_toughness('lady orca', 4).

% Found in: PTK
card_name('lady sun', 'Lady Sun').
card_type('lady sun', 'Legendary Creature — Human Advisor').
card_types('lady sun', ['Creature']).
card_subtypes('lady sun', ['Human', 'Advisor']).
card_supertypes('lady sun', ['Legendary']).
card_colors('lady sun', ['U']).
card_text('lady sun', '{T}: Return Lady Sun and another target creature to their owners\' hands. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('lady sun', ['1', 'U', 'U']).
card_cmc('lady sun', 3).
card_layout('lady sun', 'normal').
card_power('lady sun', 1).
card_toughness('lady sun', 1).

% Found in: PTK
card_name('lady zhurong, warrior queen', 'Lady Zhurong, Warrior Queen').
card_type('lady zhurong, warrior queen', 'Legendary Creature — Human Soldier Warrior').
card_types('lady zhurong, warrior queen', ['Creature']).
card_subtypes('lady zhurong, warrior queen', ['Human', 'Soldier', 'Warrior']).
card_supertypes('lady zhurong, warrior queen', ['Legendary']).
card_colors('lady zhurong, warrior queen', ['G']).
card_text('lady zhurong, warrior queen', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('lady zhurong, warrior queen', ['4', 'G']).
card_cmc('lady zhurong, warrior queen', 5).
card_layout('lady zhurong, warrior queen', 'normal').
card_power('lady zhurong, warrior queen', 4).
card_toughness('lady zhurong, warrior queen', 3).

% Found in: ROE
card_name('lagac lizard', 'Lagac Lizard').
card_type('lagac lizard', 'Creature — Lizard').
card_types('lagac lizard', ['Creature']).
card_subtypes('lagac lizard', ['Lizard']).
card_colors('lagac lizard', ['R']).
card_text('lagac lizard', '').
card_mana_cost('lagac lizard', ['3', 'R']).
card_cmc('lagac lizard', 4).
card_layout('lagac lizard', 'normal').
card_power('lagac lizard', 3).
card_toughness('lagac lizard', 3).

% Found in: THS
card_name('lagonna-band elder', 'Lagonna-Band Elder').
card_type('lagonna-band elder', 'Creature — Centaur Advisor').
card_types('lagonna-band elder', ['Creature']).
card_subtypes('lagonna-band elder', ['Centaur', 'Advisor']).
card_colors('lagonna-band elder', ['W']).
card_text('lagonna-band elder', 'When Lagonna-Band Elder enters the battlefield, if you control an enchantment, you gain 3 life.').
card_mana_cost('lagonna-band elder', ['2', 'W']).
card_cmc('lagonna-band elder', 3).
card_layout('lagonna-band elder', 'normal').
card_power('lagonna-band elder', 3).
card_toughness('lagonna-band elder', 2).

% Found in: JOU
card_name('lagonna-band trailblazer', 'Lagonna-Band Trailblazer').
card_type('lagonna-band trailblazer', 'Creature — Centaur Scout').
card_types('lagonna-band trailblazer', ['Creature']).
card_subtypes('lagonna-band trailblazer', ['Centaur', 'Scout']).
card_colors('lagonna-band trailblazer', ['W']).
card_text('lagonna-band trailblazer', 'Heroic — Whenever you cast a spell that targets Lagonna-Band Trailblazer, put a +1/+1 counter on Lagonna-Band Trailblazer.').
card_mana_cost('lagonna-band trailblazer', ['W']).
card_cmc('lagonna-band trailblazer', 1).
card_layout('lagonna-band trailblazer', 'normal').
card_power('lagonna-band trailblazer', 0).
card_toughness('lagonna-band trailblazer', 4).

% Found in: AVR
card_name('lair delve', 'Lair Delve').
card_type('lair delve', 'Sorcery').
card_types('lair delve', ['Sorcery']).
card_subtypes('lair delve', []).
card_colors('lair delve', ['G']).
card_text('lair delve', 'Reveal the top two cards of your library. Put all creature and land cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('lair delve', ['2', 'G']).
card_cmc('lair delve', 3).
card_layout('lair delve', 'normal').

% Found in: PC2
card_name('lair of the ashen idol', 'Lair of the Ashen Idol').
card_type('lair of the ashen idol', 'Plane — Azgol').
card_types('lair of the ashen idol', ['Plane']).
card_subtypes('lair of the ashen idol', ['Azgol']).
card_colors('lair of the ashen idol', []).
card_text('lair of the ashen idol', 'At the beginning of your upkeep, sacrifice a creature. If you can\'t, planeswalk.\nWhenever you roll {C}, any number of target players each put a 2/2 black Zombie creature token onto the battlefield.').
card_layout('lair of the ashen idol', 'plane').

% Found in: LRW
card_name('lairwatch giant', 'Lairwatch Giant').
card_type('lairwatch giant', 'Creature — Giant Warrior').
card_types('lairwatch giant', ['Creature']).
card_subtypes('lairwatch giant', ['Giant', 'Warrior']).
card_colors('lairwatch giant', ['W']).
card_text('lairwatch giant', 'Lairwatch Giant can block an additional creature.\nWhenever Lairwatch Giant blocks two or more creatures, it gains first strike until end of turn.').
card_mana_cost('lairwatch giant', ['5', 'W']).
card_cmc('lairwatch giant', 6).
card_layout('lairwatch giant', 'normal').
card_power('lairwatch giant', 5).
card_toughness('lairwatch giant', 3).

% Found in: ALL, MED, VMA
card_name('lake of the dead', 'Lake of the Dead').
card_type('lake of the dead', 'Land').
card_types('lake of the dead', ['Land']).
card_subtypes('lake of the dead', []).
card_colors('lake of the dead', []).
card_text('lake of the dead', 'If Lake of the Dead would enter the battlefield, sacrifice a Swamp instead. If you do, put Lake of the Dead onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add {B} to your mana pool.\n{T}, Sacrifice a Swamp: Add {B}{B}{B}{B} to your mana pool.').
card_layout('lake of the dead', 'normal').
card_reserved('lake of the dead').

% Found in: DKA
card_name('lambholt elder', 'Lambholt Elder').
card_type('lambholt elder', 'Creature — Human Werewolf').
card_types('lambholt elder', ['Creature']).
card_subtypes('lambholt elder', ['Human', 'Werewolf']).
card_colors('lambholt elder', ['G']).
card_text('lambholt elder', 'At the beginning of each upkeep, if no spells were cast last turn, transform Lambholt Elder.').
card_mana_cost('lambholt elder', ['2', 'G']).
card_cmc('lambholt elder', 3).
card_layout('lambholt elder', 'double-faced').
card_power('lambholt elder', 1).
card_toughness('lambholt elder', 2).
card_sides('lambholt elder', 'silverpelt werewolf').

% Found in: LRW
card_name('lammastide weave', 'Lammastide Weave').
card_type('lammastide weave', 'Instant').
card_types('lammastide weave', ['Instant']).
card_subtypes('lammastide weave', []).
card_colors('lammastide weave', ['G']).
card_text('lammastide weave', 'Name a card, then target player puts the top card of his or her library into his or her graveyard. If that card is the named card, you gain life equal to its converted mana cost.\nDraw a card.').
card_mana_cost('lammastide weave', ['1', 'G']).
card_cmc('lammastide weave', 2).
card_layout('lammastide weave', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('lance', 'Lance').
card_type('lance', 'Enchantment — Aura').
card_types('lance', ['Enchantment']).
card_subtypes('lance', ['Aura']).
card_colors('lance', ['W']).
card_text('lance', 'Enchant creature\nEnchanted creature has first strike.').
card_mana_cost('lance', ['W']).
card_cmc('lance', 1).
card_layout('lance', 'normal').

% Found in: STH
card_name('lancers en-kor', 'Lancers en-Kor').
card_type('lancers en-kor', 'Creature — Kor Soldier').
card_types('lancers en-kor', ['Creature']).
card_subtypes('lancers en-kor', ['Kor', 'Soldier']).
card_colors('lancers en-kor', ['W']).
card_text('lancers en-kor', 'Trample\n{0}: The next 1 damage that would be dealt to Lancers en-Kor this turn is dealt to target creature you control instead.').
card_mana_cost('lancers en-kor', ['3', 'W', 'W']).
card_cmc('lancers en-kor', 5).
card_layout('lancers en-kor', 'normal').
card_power('lancers en-kor', 3).
card_toughness('lancers en-kor', 3).

% Found in: UNH
card_name('land aid \'04', 'Land Aid \'04').
card_type('land aid \'04', 'Sorcery').
card_types('land aid \'04', ['Sorcery']).
card_subtypes('land aid \'04', []).
card_colors('land aid \'04', ['G']).
card_text('land aid \'04', 'Search your library for a basic land card, put that card into play tapped, then shuffle your library. If you sang a song the whole time you were searching and shuffling, you may untap that land.').
card_mana_cost('land aid \'04', ['G', 'G']).
card_cmc('land aid \'04', 2).
card_layout('land aid \'04', 'normal').

% Found in: ICE
card_name('land cap', 'Land Cap').
card_type('land cap', 'Land').
card_types('land cap', ['Land']).
card_subtypes('land cap', []).
card_colors('land cap', []).
card_text('land cap', 'Land Cap doesn\'t untap during your untap step if it has a depletion counter on it.\nAt the beginning of your upkeep, remove a depletion counter from Land Cap.\n{T}: Add {W} or {U} to your mana pool. Put a depletion counter on Land Cap.').
card_layout('land cap', 'normal').
card_reserved('land cap').

% Found in: LEG, ME3
card_name('land equilibrium', 'Land Equilibrium').
card_type('land equilibrium', 'Enchantment').
card_types('land equilibrium', ['Enchantment']).
card_subtypes('land equilibrium', []).
card_colors('land equilibrium', ['U']).
card_text('land equilibrium', 'If an opponent who controls at least as many lands as you do would put a land onto the battlefield, that player instead puts that land onto the battlefield then sacrifices a land.').
card_mana_cost('land equilibrium', ['2', 'U', 'U']).
card_cmc('land equilibrium', 4).
card_layout('land equilibrium', 'normal').
card_reserved('land equilibrium').

% Found in: MMQ
card_name('land grant', 'Land Grant').
card_type('land grant', 'Sorcery').
card_types('land grant', ['Sorcery']).
card_subtypes('land grant', []).
card_colors('land grant', ['G']).
card_text('land grant', 'If you have no land cards in hand, you may reveal your hand rather than pay Land Grant\'s mana cost.\nSearch your library for a Forest card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('land grant', ['1', 'G']).
card_cmc('land grant', 2).
card_layout('land grant', 'normal').

% Found in: 4ED, DRK
card_name('land leeches', 'Land Leeches').
card_type('land leeches', 'Creature — Leech').
card_types('land leeches', ['Creature']).
card_subtypes('land leeches', ['Leech']).
card_colors('land leeches', ['G']).
card_text('land leeches', 'First strike').
card_mana_cost('land leeches', ['1', 'G', 'G']).
card_cmc('land leeches', 3).
card_layout('land leeches', 'normal').
card_power('land leeches', 2).
card_toughness('land leeches', 2).

% Found in: 4ED, BRB, LEG, ME3, pJGP
card_name('land tax', 'Land Tax').
card_type('land tax', 'Enchantment').
card_types('land tax', ['Enchantment']).
card_subtypes('land tax', []).
card_colors('land tax', ['W']).
card_text('land tax', 'At the beginning of your upkeep, if an opponent controls more lands than you, you may search your library for up to three basic land cards, reveal them, and put them into your hand. If you do, shuffle your library.').
card_mana_cost('land tax', ['W']).
card_cmc('land tax', 1).
card_layout('land tax', 'normal').

% Found in: CHR, LEG
card_name('land\'s edge', 'Land\'s Edge').
card_type('land\'s edge', 'World Enchantment').
card_types('land\'s edge', ['Enchantment']).
card_subtypes('land\'s edge', []).
card_supertypes('land\'s edge', ['World']).
card_colors('land\'s edge', ['R']).
card_text('land\'s edge', 'Discard a card: If the discarded card was a land card, Land\'s Edge deals 2 damage to target player. Any player may activate this ability.').
card_mana_cost('land\'s edge', ['1', 'R', 'R']).
card_cmc('land\'s edge', 3).
card_layout('land\'s edge', 'normal').

% Found in: ZEN
card_name('landbind ritual', 'Landbind Ritual').
card_type('landbind ritual', 'Sorcery').
card_types('landbind ritual', ['Sorcery']).
card_subtypes('landbind ritual', []).
card_colors('landbind ritual', ['W']).
card_text('landbind ritual', 'You gain 2 life for each Plains you control.').
card_mana_cost('landbind ritual', ['3', 'W', 'W']).
card_cmc('landbind ritual', 5).
card_layout('landbind ritual', 'normal').

% Found in: UGL
card_name('landfill', 'Landfill').
card_type('landfill', 'Sorcery').
card_types('landfill', ['Sorcery']).
card_subtypes('landfill', []).
card_colors('landfill', ['R']).
card_text('landfill', 'Choose a land type. Remove from play all lands of that type that you control. Drop those cards, one at a time, onto the playing area from a height of at least one foot. Destroy each card in play that is completely covered by those cards. Then return to play, tapped, all lands dropped in this way.').
card_mana_cost('landfill', ['4', 'R']).
card_cmc('landfill', 5).
card_layout('landfill', 'normal').

% Found in: UDS
card_name('landslide', 'Landslide').
card_type('landslide', 'Sorcery').
card_types('landslide', ['Sorcery']).
card_subtypes('landslide', []).
card_colors('landslide', ['R']).
card_text('landslide', 'Sacrifice any number of Mountains. Landslide deals that much damage to target player.').
card_mana_cost('landslide', ['R']).
card_cmc('landslide', 1).
card_layout('landslide', 'normal').

% Found in: ORI
card_name('languish', 'Languish').
card_type('languish', 'Sorcery').
card_types('languish', ['Sorcery']).
card_subtypes('languish', []).
card_colors('languish', ['B']).
card_text('languish', 'All creatures get -4/-4 until end of turn.').
card_mana_cost('languish', ['2', 'B', 'B']).
card_cmc('languish', 4).
card_layout('languish', 'normal').

% Found in: CHK
card_name('lantern kami', 'Lantern Kami').
card_type('lantern kami', 'Creature — Spirit').
card_types('lantern kami', ['Creature']).
card_subtypes('lantern kami', ['Spirit']).
card_colors('lantern kami', ['W']).
card_text('lantern kami', 'Flying').
card_mana_cost('lantern kami', ['W']).
card_cmc('lantern kami', 1).
card_layout('lantern kami', 'normal').
card_power('lantern kami', 1).
card_toughness('lantern kami', 1).

% Found in: 5DN
card_name('lantern of insight', 'Lantern of Insight').
card_type('lantern of insight', 'Artifact').
card_types('lantern of insight', ['Artifact']).
card_subtypes('lantern of insight', []).
card_colors('lantern of insight', []).
card_text('lantern of insight', 'Players play with the top card of their libraries revealed.\n{T}, Sacrifice Lantern of Insight: Target player shuffles his or her library.').
card_mana_cost('lantern of insight', ['1']).
card_cmc('lantern of insight', 1).
card_layout('lantern of insight', 'normal').

% Found in: BFZ
card_name('lantern scout', 'Lantern Scout').
card_type('lantern scout', 'Creature — Human Scout Ally').
card_types('lantern scout', ['Creature']).
card_subtypes('lantern scout', ['Human', 'Scout', 'Ally']).
card_colors('lantern scout', ['W']).
card_text('lantern scout', 'Rally — Whenever Lantern Scout or another Ally enters the battlefield under your control, creatures you control gain lifelink until end of turn.').
card_mana_cost('lantern scout', ['2', 'W']).
card_cmc('lantern scout', 3).
card_layout('lantern scout', 'normal').
card_power('lantern scout', 3).
card_toughness('lantern scout', 2).

% Found in: ISD
card_name('lantern spirit', 'Lantern Spirit').
card_type('lantern spirit', 'Creature — Spirit').
card_types('lantern spirit', ['Creature']).
card_subtypes('lantern spirit', ['Spirit']).
card_colors('lantern spirit', ['U']).
card_text('lantern spirit', 'Flying\n{U}: Return Lantern Spirit to its owner\'s hand.').
card_mana_cost('lantern spirit', ['2', 'U']).
card_cmc('lantern spirit', 3).
card_layout('lantern spirit', 'normal').
card_power('lantern spirit', 2).
card_toughness('lantern spirit', 1).

% Found in: CHK
card_name('lantern-lit graveyard', 'Lantern-Lit Graveyard').
card_type('lantern-lit graveyard', 'Land').
card_types('lantern-lit graveyard', ['Land']).
card_subtypes('lantern-lit graveyard', []).
card_colors('lantern-lit graveyard', []).
card_text('lantern-lit graveyard', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Lantern-Lit Graveyard doesn\'t untap during your next untap step.').
card_layout('lantern-lit graveyard', 'normal').

% Found in: ICE
card_name('lapis lazuli talisman', 'Lapis Lazuli Talisman').
card_type('lapis lazuli talisman', 'Artifact').
card_types('lapis lazuli talisman', ['Artifact']).
card_subtypes('lapis lazuli talisman', []).
card_colors('lapis lazuli talisman', []).
card_text('lapis lazuli talisman', 'Whenever a player casts a blue spell, you may pay {3}. If you do, untap target permanent.').
card_mana_cost('lapis lazuli talisman', ['2']).
card_cmc('lapis lazuli talisman', 2).
card_layout('lapis lazuli talisman', 'normal').

% Found in: CON
card_name('lapse of certainty', 'Lapse of Certainty').
card_type('lapse of certainty', 'Instant').
card_types('lapse of certainty', ['Instant']).
card_subtypes('lapse of certainty', []).
card_colors('lapse of certainty', ['W']).
card_text('lapse of certainty', 'Counter target spell. If that spell is countered this way, put it on top of its owner\'s library instead of into that player\'s graveyard.').
card_mana_cost('lapse of certainty', ['2', 'W']).
card_cmc('lapse of certainty', 3).
card_layout('lapse of certainty', 'normal').

% Found in: TOR, VMA, pPRE
card_name('laquatus\'s champion', 'Laquatus\'s Champion').
card_type('laquatus\'s champion', 'Creature — Nightmare Horror').
card_types('laquatus\'s champion', ['Creature']).
card_subtypes('laquatus\'s champion', ['Nightmare', 'Horror']).
card_colors('laquatus\'s champion', ['B']).
card_text('laquatus\'s champion', 'When Laquatus\'s Champion enters the battlefield, target player loses 6 life.\nWhen Laquatus\'s Champion leaves the battlefield, that player gains 6 life.\n{B}: Regenerate Laquatus\'s Champion.').
card_mana_cost('laquatus\'s champion', ['4', 'B', 'B']).
card_cmc('laquatus\'s champion', 6).
card_layout('laquatus\'s champion', 'normal').
card_power('laquatus\'s champion', 6).
card_toughness('laquatus\'s champion', 3).

% Found in: ODY
card_name('laquatus\'s creativity', 'Laquatus\'s Creativity').
card_type('laquatus\'s creativity', 'Sorcery').
card_types('laquatus\'s creativity', ['Sorcery']).
card_subtypes('laquatus\'s creativity', []).
card_colors('laquatus\'s creativity', ['U']).
card_text('laquatus\'s creativity', 'Target player draws cards equal to the number of cards in his or her hand, then discards that many cards.').
card_mana_cost('laquatus\'s creativity', ['4', 'U']).
card_cmc('laquatus\'s creativity', 5).
card_layout('laquatus\'s creativity', 'normal').

% Found in: JUD
card_name('laquatus\'s disdain', 'Laquatus\'s Disdain').
card_type('laquatus\'s disdain', 'Instant').
card_types('laquatus\'s disdain', ['Instant']).
card_subtypes('laquatus\'s disdain', []).
card_colors('laquatus\'s disdain', ['U']).
card_text('laquatus\'s disdain', 'Counter target spell cast from a graveyard.\nDraw a card.').
card_mana_cost('laquatus\'s disdain', ['1', 'U']).
card_cmc('laquatus\'s disdain', 2).
card_layout('laquatus\'s disdain', 'normal').

% Found in: 8ED, MMQ
card_name('larceny', 'Larceny').
card_type('larceny', 'Enchantment').
card_types('larceny', ['Enchantment']).
card_subtypes('larceny', []).
card_colors('larceny', ['B']).
card_text('larceny', 'Whenever a creature you control deals combat damage to a player, that player discards a card.').
card_mana_cost('larceny', ['3', 'B', 'B']).
card_cmc('larceny', 5).
card_layout('larceny', 'normal').

% Found in: THS
card_name('lash of the whip', 'Lash of the Whip').
card_type('lash of the whip', 'Instant').
card_types('lash of the whip', ['Instant']).
card_subtypes('lash of the whip', []).
card_colors('lash of the whip', ['B']).
card_text('lash of the whip', 'Target creature gets -4/-4 until end of turn.').
card_mana_cost('lash of the whip', ['4', 'B']).
card_cmc('lash of the whip', 5).
card_layout('lash of the whip', 'normal').

% Found in: CMD, LRW
card_name('lash out', 'Lash Out').
card_type('lash out', 'Instant').
card_types('lash out', ['Instant']).
card_subtypes('lash out', []).
card_colors('lash out', ['R']).
card_text('lash out', 'Lash Out deals 3 damage to target creature. Clash with an opponent. If you win, Lash Out deals 3 damage to that creature\'s controller. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('lash out', ['1', 'R']).
card_cmc('lash out', 2).
card_layout('lash out', 'normal').

% Found in: NMS
card_name('lashknife', 'Lashknife').
card_type('lashknife', 'Enchantment — Aura').
card_types('lashknife', ['Enchantment']).
card_subtypes('lashknife', ['Aura']).
card_colors('lashknife', ['W']).
card_text('lashknife', 'If you control a Plains, you may tap an untapped creature you control rather than pay Lashknife\'s mana cost.\nEnchant creature\nEnchanted creature has first strike.').
card_mana_cost('lashknife', ['1', 'W']).
card_cmc('lashknife', 2).
card_layout('lashknife', 'normal').

% Found in: PLS
card_name('lashknife barrier', 'Lashknife Barrier').
card_type('lashknife barrier', 'Enchantment').
card_types('lashknife barrier', ['Enchantment']).
card_subtypes('lashknife barrier', []).
card_colors('lashknife barrier', ['W']).
card_text('lashknife barrier', 'When Lashknife Barrier enters the battlefield, draw a card.\nIf a source would deal damage to a creature you control, it deals that much damage minus 1 to that creature instead.').
card_mana_cost('lashknife barrier', ['2', 'W']).
card_cmc('lashknife barrier', 3).
card_layout('lashknife barrier', 'normal').

% Found in: C14, NPH
card_name('lashwrithe', 'Lashwrithe').
card_type('lashwrithe', 'Artifact — Equipment').
card_types('lashwrithe', ['Artifact']).
card_subtypes('lashwrithe', ['Equipment']).
card_colors('lashwrithe', []).
card_text('lashwrithe', 'Living weapon (When this Equipment enters the battlefield, put a 0/0 black Germ creature token onto the battlefield, then attach this to it.)\nEquipped creature gets +1/+1 for each Swamp you control.\nEquip {B/P}{B/P} ({B/P} can be paid with either {B} or 2 life.)').
card_mana_cost('lashwrithe', ['4']).
card_cmc('lashwrithe', 4).
card_layout('lashwrithe', 'normal').

% Found in: MMQ, SHM, THS
card_name('last breath', 'Last Breath').
card_type('last breath', 'Instant').
card_types('last breath', ['Instant']).
card_subtypes('last breath', []).
card_colors('last breath', ['W']).
card_text('last breath', 'Exile target creature with power 2 or less. Its controller gains 4 life.').
card_mana_cost('last breath', ['1', 'W']).
card_cmc('last breath', 2).
card_layout('last breath', 'normal').

% Found in: APC
card_name('last caress', 'Last Caress').
card_type('last caress', 'Sorcery').
card_types('last caress', ['Sorcery']).
card_subtypes('last caress', []).
card_colors('last caress', ['B']).
card_text('last caress', 'Target player loses 1 life and you gain 1 life.\nDraw a card.').
card_mana_cost('last caress', ['2', 'B']).
card_cmc('last caress', 3).
card_layout('last caress', 'normal').

% Found in: ME4, POR, S99
card_name('last chance', 'Last Chance').
card_type('last chance', 'Sorcery').
card_types('last chance', ['Sorcery']).
card_subtypes('last chance', []).
card_colors('last chance', ['R']).
card_text('last chance', 'Take an extra turn after this one. At the beginning of that turn\'s end step, you lose the game.').
card_mana_cost('last chance', ['R', 'R']).
card_cmc('last chance', 2).
card_layout('last chance', 'normal').

% Found in: RAV
card_name('last gasp', 'Last Gasp').
card_type('last gasp', 'Instant').
card_types('last gasp', ['Instant']).
card_subtypes('last gasp', []).
card_colors('last gasp', ['B']).
card_text('last gasp', 'Target creature gets -3/-3 until end of turn.').
card_mana_cost('last gasp', ['1', 'B']).
card_cmc('last gasp', 2).
card_layout('last gasp', 'normal').

% Found in: DDM, ROE
card_name('last kiss', 'Last Kiss').
card_type('last kiss', 'Instant').
card_types('last kiss', ['Instant']).
card_subtypes('last kiss', []).
card_colors('last kiss', ['B']).
card_text('last kiss', 'Last Kiss deals 2 damage to target creature and you gain 2 life.').
card_mana_cost('last kiss', ['2', 'B']).
card_cmc('last kiss', 3).
card_layout('last kiss', 'normal').

% Found in: TOR
card_name('last laugh', 'Last Laugh').
card_type('last laugh', 'Enchantment').
card_types('last laugh', ['Enchantment']).
card_subtypes('last laugh', []).
card_colors('last laugh', ['B']).
card_text('last laugh', 'Whenever a permanent other than Last Laugh is put into a graveyard from the battlefield, Last Laugh deals 1 damage to each creature and each player.\nWhen no creatures are on the battlefield, sacrifice Last Laugh.').
card_mana_cost('last laugh', ['2', 'B', 'B']).
card_cmc('last laugh', 4).
card_layout('last laugh', 'normal').

% Found in: ODY, PD3
card_name('last rites', 'Last Rites').
card_type('last rites', 'Sorcery').
card_types('last rites', ['Sorcery']).
card_subtypes('last rites', []).
card_colors('last rites', ['B']).
card_text('last rites', 'Discard any number of cards. Target player reveals his or her hand, then you choose a nonland card from it for each card discarded this way. That player discards those cards.').
card_mana_cost('last rites', ['2', 'B']).
card_cmc('last rites', 3).
card_layout('last rites', 'normal').

% Found in: APC, PC2
card_name('last stand', 'Last Stand').
card_type('last stand', 'Sorcery').
card_types('last stand', ['Sorcery']).
card_subtypes('last stand', []).
card_colors('last stand', ['W', 'U', 'B', 'R', 'G']).
card_text('last stand', 'Target opponent loses 2 life for each Swamp you control. Last Stand deals damage equal to the number of Mountains you control to target creature. Put a 1/1 green Saproling creature token onto the battlefield for each Forest you control. You gain 2 life for each Plains you control. Draw a card for each Island you control, then discard that many cards.').
card_mana_cost('last stand', ['W', 'U', 'B', 'R', 'G']).
card_cmc('last stand', 5).
card_layout('last stand', 'normal').

% Found in: GTC
card_name('last thoughts', 'Last Thoughts').
card_type('last thoughts', 'Sorcery').
card_types('last thoughts', ['Sorcery']).
card_subtypes('last thoughts', []).
card_colors('last thoughts', ['U']).
card_text('last thoughts', 'Draw a card.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('last thoughts', ['3', 'U']).
card_cmc('last thoughts', 4).
card_layout('last thoughts', 'normal').

% Found in: DST
card_name('last word', 'Last Word').
card_type('last word', 'Instant').
card_types('last word', ['Instant']).
card_subtypes('last word', []).
card_colors('last word', ['U']).
card_text('last word', 'Last Word can\'t be countered by spells or abilities.\nCounter target spell.').
card_mana_cost('last word', ['2', 'U', 'U']).
card_cmc('last word', 4).
card_layout('last word', 'normal').

% Found in: ULG
card_name('last-ditch effort', 'Last-Ditch Effort').
card_type('last-ditch effort', 'Instant').
card_types('last-ditch effort', ['Instant']).
card_subtypes('last-ditch effort', []).
card_colors('last-ditch effort', ['R']).
card_text('last-ditch effort', 'Sacrifice any number of creatures. Last-Ditch Effort deals that much damage to target creature or player.').
card_mana_cost('last-ditch effort', ['R']).
card_cmc('last-ditch effort', 1).
card_layout('last-ditch effort', 'normal').

% Found in: ALL, CST, ME2
card_name('lat-nam\'s legacy', 'Lat-Nam\'s Legacy').
card_type('lat-nam\'s legacy', 'Instant').
card_types('lat-nam\'s legacy', ['Instant']).
card_subtypes('lat-nam\'s legacy', []).
card_colors('lat-nam\'s legacy', ['U']).
card_text('lat-nam\'s legacy', 'Shuffle a card from your hand into your library. If you do, draw two cards at the beginning of the next turn\'s upkeep.').
card_mana_cost('lat-nam\'s legacy', ['1', 'U']).
card_cmc('lat-nam\'s legacy', 2).
card_layout('lat-nam\'s legacy', 'normal').

% Found in: AVR, pMGD
card_name('latch seeker', 'Latch Seeker').
card_type('latch seeker', 'Creature — Spirit').
card_types('latch seeker', ['Creature']).
card_subtypes('latch seeker', ['Spirit']).
card_colors('latch seeker', ['U']).
card_text('latch seeker', 'Latch Seeker can\'t be blocked.').
card_mana_cost('latch seeker', ['1', 'U', 'U']).
card_cmc('latch seeker', 3).
card_layout('latch seeker', 'normal').
card_power('latch seeker', 3).
card_toughness('latch seeker', 1).

% Found in: MMA, MOR
card_name('latchkey faerie', 'Latchkey Faerie').
card_type('latchkey faerie', 'Creature — Faerie Rogue').
card_types('latchkey faerie', ['Creature']).
card_subtypes('latchkey faerie', ['Faerie', 'Rogue']).
card_colors('latchkey faerie', ['U']).
card_text('latchkey faerie', 'Flying\nProwl {2}{U} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Faerie or Rogue.)\nWhen Latchkey Faerie enters the battlefield, if its prowl cost was paid, draw a card.').
card_mana_cost('latchkey faerie', ['3', 'U']).
card_cmc('latchkey faerie', 4).
card_layout('latchkey faerie', 'normal').
card_power('latchkey faerie', 3).
card_toughness('latchkey faerie', 1).

% Found in: PCY
card_name('latulla\'s orders', 'Latulla\'s Orders').
card_type('latulla\'s orders', 'Enchantment — Aura').
card_types('latulla\'s orders', ['Enchantment']).
card_subtypes('latulla\'s orders', ['Aura']).
card_colors('latulla\'s orders', ['R']).
card_text('latulla\'s orders', 'Flash\nEnchant creature\nWhenever enchanted creature deals combat damage to defending player, you may destroy target artifact that player controls.').
card_mana_cost('latulla\'s orders', ['1', 'R']).
card_cmc('latulla\'s orders', 2).
card_layout('latulla\'s orders', 'normal').

% Found in: PCY
card_name('latulla, keldon overseer', 'Latulla, Keldon Overseer').
card_type('latulla, keldon overseer', 'Legendary Creature — Human Spellshaper').
card_types('latulla, keldon overseer', ['Creature']).
card_subtypes('latulla, keldon overseer', ['Human', 'Spellshaper']).
card_supertypes('latulla, keldon overseer', ['Legendary']).
card_colors('latulla, keldon overseer', ['R']).
card_text('latulla, keldon overseer', '{X}{R}, {T}, Discard two cards: Latulla, Keldon Overseer deals X damage to target creature or player.').
card_mana_cost('latulla, keldon overseer', ['3', 'R', 'R']).
card_cmc('latulla, keldon overseer', 5).
card_layout('latulla, keldon overseer', 'normal').
card_power('latulla, keldon overseer', 3).
card_toughness('latulla, keldon overseer', 3).

% Found in: UNH
card_name('laughing hyena', 'Laughing Hyena').
card_type('laughing hyena', 'Creature — Hyena').
card_types('laughing hyena', ['Creature']).
card_subtypes('laughing hyena', ['Hyena']).
card_colors('laughing hyena', ['G']).
card_text('laughing hyena', 'Gotcha Whenever an opponent laughs, you may say \"Gotcha\" If you do, return Laughing Hyena from your graveyard to your hand.').
card_mana_cost('laughing hyena', ['1', 'G']).
card_cmc('laughing hyena', 2).
card_layout('laughing hyena', 'normal').
card_power('laughing hyena', 2).
card_toughness('laughing hyena', 2).

% Found in: USG
card_name('launch', 'Launch').
card_type('launch', 'Enchantment — Aura').
card_types('launch', ['Enchantment']).
card_subtypes('launch', ['Aura']).
card_colors('launch', ['U']).
card_text('launch', 'Enchant creature\nEnchanted creature has flying.\nWhen Launch is put into a graveyard from the battlefield, return Launch to its owner\'s hand.').
card_mana_cost('launch', ['1', 'U']).
card_cmc('launch', 2).
card_layout('launch', 'normal').

% Found in: RTR
card_name('launch party', 'Launch Party').
card_type('launch party', 'Instant').
card_types('launch party', ['Instant']).
card_subtypes('launch party', []).
card_colors('launch party', ['B']).
card_text('launch party', 'As an additional cost to cast Launch Party, sacrifice a creature.\nDestroy target creature. Its controller loses 2 life.').
card_mana_cost('launch party', ['3', 'B']).
card_cmc('launch party', 4).
card_layout('launch party', 'normal').

% Found in: JOU
card_name('launch the fleet', 'Launch the Fleet').
card_type('launch the fleet', 'Sorcery').
card_types('launch the fleet', ['Sorcery']).
card_subtypes('launch the fleet', []).
card_colors('launch the fleet', ['W']).
card_text('launch the fleet', 'Strive — Launch the Fleet costs {1} more to cast for each target beyond the first.\nUntil end of turn, any number of target creatures each gain \"Whenever this creature attacks, put a 1/1 white Soldier creature token onto the battlefield tapped and attacking.\"').
card_mana_cost('launch the fleet', ['W']).
card_cmc('launch the fleet', 1).
card_layout('launch the fleet', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, BTD, M10, M11, M12, M14, M15, PO2, POR, S00, S99, ULG, pGTW
card_name('lava axe', 'Lava Axe').
card_type('lava axe', 'Sorcery').
card_types('lava axe', ['Sorcery']).
card_subtypes('lava axe', []).
card_colors('lava axe', ['R']).
card_text('lava axe', 'Lava Axe deals 5 damage to target player.').
card_mana_cost('lava axe', ['4', 'R']).
card_cmc('lava axe', 5).
card_layout('lava axe', 'normal').

% Found in: ODY
card_name('lava blister', 'Lava Blister').
card_type('lava blister', 'Sorcery').
card_types('lava blister', ['Sorcery']).
card_subtypes('lava blister', []).
card_colors('lava blister', ['R']).
card_text('lava blister', 'Destroy target nonbasic land unless its controller has Lava Blister deal 6 damage to him or her.').
card_mana_cost('lava blister', ['1', 'R']).
card_cmc('lava blister', 2).
card_layout('lava blister', 'normal').

% Found in: DKM, ICE, ME2
card_name('lava burst', 'Lava Burst').
card_type('lava burst', 'Sorcery').
card_types('lava burst', ['Sorcery']).
card_subtypes('lava burst', []).
card_colors('lava burst', ['R']).
card_text('lava burst', 'Lava Burst deals X damage to target creature or player. If Lava Burst would deal damage to a creature, that damage can\'t be prevented or dealt instead to another creature or player.').
card_mana_cost('lava burst', ['X', 'R']).
card_cmc('lava burst', 1).
card_layout('lava burst', 'normal').

% Found in: JUD
card_name('lava dart', 'Lava Dart').
card_type('lava dart', 'Instant').
card_types('lava dart', ['Instant']).
card_subtypes('lava dart', []).
card_colors('lava dart', ['R']).
card_text('lava dart', 'Lava Dart deals 1 damage to target creature or player.\nFlashback—Sacrifice a Mountain. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('lava dart', ['R']).
card_cmc('lava dart', 1).
card_layout('lava dart', 'normal').

% Found in: ME4, POR
card_name('lava flow', 'Lava Flow').
card_type('lava flow', 'Sorcery').
card_types('lava flow', ['Sorcery']).
card_subtypes('lava flow', []).
card_colors('lava flow', ['R']).
card_text('lava flow', 'Destroy target creature or land.').
card_mana_cost('lava flow', ['3', 'R', 'R']).
card_cmc('lava flow', 5).
card_layout('lava flow', 'normal').

% Found in: 8ED, WTH
card_name('lava hounds', 'Lava Hounds').
card_type('lava hounds', 'Creature — Hound').
card_types('lava hounds', ['Creature']).
card_subtypes('lava hounds', ['Hound']).
card_colors('lava hounds', ['R']).
card_text('lava hounds', 'Haste\nWhen Lava Hounds enters the battlefield, it deals 4 damage to you.').
card_mana_cost('lava hounds', ['2', 'R', 'R']).
card_cmc('lava hounds', 4).
card_layout('lava hounds', 'normal').
card_power('lava hounds', 4).
card_toughness('lava hounds', 4).

% Found in: MMQ
card_name('lava runner', 'Lava Runner').
card_type('lava runner', 'Creature — Lizard').
card_types('lava runner', ['Creature']).
card_subtypes('lava runner', ['Lizard']).
card_colors('lava runner', ['R']).
card_text('lava runner', 'Haste\nWhenever Lava Runner becomes the target of a spell or ability, that spell or ability\'s controller sacrifices a land.').
card_mana_cost('lava runner', ['1', 'R', 'R']).
card_cmc('lava runner', 3).
card_layout('lava runner', 'normal').
card_power('lava runner', 2).
card_toughness('lava runner', 2).

% Found in: CHK, MMA
card_name('lava spike', 'Lava Spike').
card_type('lava spike', 'Sorcery — Arcane').
card_types('lava spike', ['Sorcery']).
card_subtypes('lava spike', ['Arcane']).
card_colors('lava spike', ['R']).
card_text('lava spike', 'Lava Spike deals 3 damage to target player.').
card_mana_cost('lava spike', ['R']).
card_cmc('lava spike', 1).
card_layout('lava spike', 'normal').

% Found in: WTH
card_name('lava storm', 'Lava Storm').
card_type('lava storm', 'Instant').
card_types('lava storm', ['Instant']).
card_subtypes('lava storm', []).
card_colors('lava storm', ['R']).
card_text('lava storm', 'Lava Storm deals 2 damage to each attacking creature or Lava Storm deals 2 damage to each blocking creature.').
card_mana_cost('lava storm', ['3', 'R', 'R']).
card_cmc('lava storm', 5).
card_layout('lava storm', 'normal').

% Found in: ICE
card_name('lava tubes', 'Lava Tubes').
card_type('lava tubes', 'Land').
card_types('lava tubes', ['Land']).
card_subtypes('lava tubes', []).
card_colors('lava tubes', []).
card_text('lava tubes', 'Lava Tubes doesn\'t untap during your untap step if it has a depletion counter on it.\nAt the beginning of your upkeep, remove a depletion counter from Lava Tubes.\n{T}: Add {B} or {R} to your mana pool. Put a depletion counter on Lava Tubes.').
card_layout('lava tubes', 'normal').
card_reserved('lava tubes').

% Found in: PLS
card_name('lava zombie', 'Lava Zombie').
card_type('lava zombie', 'Creature — Zombie').
card_types('lava zombie', ['Creature']).
card_subtypes('lava zombie', ['Zombie']).
card_colors('lava zombie', ['B', 'R']).
card_text('lava zombie', 'When Lava Zombie enters the battlefield, return a black or red creature you control to its owner\'s hand.\n{2}: Lava Zombie gets +1/+0 until end of turn.').
card_mana_cost('lava zombie', ['1', 'B', 'R']).
card_cmc('lava zombie', 3).
card_layout('lava zombie', 'normal').
card_power('lava zombie', 4).
card_toughness('lava zombie', 3).

% Found in: ZEN
card_name('lavaball trap', 'Lavaball Trap').
card_type('lavaball trap', 'Instant — Trap').
card_types('lavaball trap', ['Instant']).
card_subtypes('lavaball trap', ['Trap']).
card_colors('lavaball trap', ['R']).
card_text('lavaball trap', 'If an opponent had two or more lands enter the battlefield under his or her control this turn, you may pay {3}{R}{R} rather than pay Lavaball Trap\'s mana cost.\nDestroy two target lands. Lavaball Trap deals 4 damage to each creature.').
card_mana_cost('lavaball trap', ['6', 'R', 'R']).
card_cmc('lavaball trap', 8).
card_layout('lavaball trap', 'normal').

% Found in: 10E, DDK, LGN
card_name('lavaborn muse', 'Lavaborn Muse').
card_type('lavaborn muse', 'Creature — Spirit').
card_types('lavaborn muse', ['Creature']).
card_subtypes('lavaborn muse', ['Spirit']).
card_colors('lavaborn muse', ['R']).
card_text('lavaborn muse', 'At the beginning of each opponent\'s upkeep, if that player has two or fewer cards in hand, Lavaborn Muse deals 3 damage to him or her.').
card_mana_cost('lavaborn muse', ['3', 'R']).
card_cmc('lavaborn muse', 4).
card_layout('lavaborn muse', 'normal').
card_power('lavaborn muse', 3).
card_toughness('lavaborn muse', 3).

% Found in: WWK
card_name('lavaclaw reaches', 'Lavaclaw Reaches').
card_type('lavaclaw reaches', 'Land').
card_types('lavaclaw reaches', ['Land']).
card_subtypes('lavaclaw reaches', []).
card_colors('lavaclaw reaches', []).
card_text('lavaclaw reaches', 'Lavaclaw Reaches enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.\n{1}{B}{R}: Until end of turn, Lavaclaw Reaches becomes a 2/2 black and red Elemental creature with \"{X}: This creature gets +X/+0 until end of turn.\" It\'s still a land.').
card_layout('lavaclaw reaches', 'normal').

% Found in: PLC
card_name('lavacore elemental', 'Lavacore Elemental').
card_type('lavacore elemental', 'Creature — Elemental').
card_types('lavacore elemental', ['Creature']).
card_subtypes('lavacore elemental', ['Elemental']).
card_colors('lavacore elemental', ['R']).
card_text('lavacore elemental', 'Vanishing 1 (This permanent enters the battlefield with a time counter on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhenever a creature you control deals combat damage to a player, put a time counter on Lavacore Elemental.').
card_mana_cost('lavacore elemental', ['2', 'R']).
card_cmc('lavacore elemental', 3).
card_layout('lavacore elemental', 'normal').
card_power('lavacore elemental', 5).
card_toughness('lavacore elemental', 3).

% Found in: ROE
card_name('lavafume invoker', 'Lavafume Invoker').
card_type('lavafume invoker', 'Creature — Goblin Shaman').
card_types('lavafume invoker', ['Creature']).
card_subtypes('lavafume invoker', ['Goblin', 'Shaman']).
card_colors('lavafume invoker', ['R']).
card_text('lavafume invoker', '{8}: Creatures you control get +3/+0 until end of turn.').
card_mana_cost('lavafume invoker', ['2', 'R']).
card_cmc('lavafume invoker', 3).
card_layout('lavafume invoker', 'normal').
card_power('lavafume invoker', 2).
card_toughness('lavafume invoker', 2).

% Found in: ARB
card_name('lavalanche', 'Lavalanche').
card_type('lavalanche', 'Sorcery').
card_types('lavalanche', ['Sorcery']).
card_subtypes('lavalanche', []).
card_colors('lavalanche', ['B', 'R', 'G']).
card_text('lavalanche', 'Lavalanche deals X damage to target player and each creature he or she controls.').
card_mana_cost('lavalanche', ['X', 'B', 'R', 'G']).
card_cmc('lavalanche', 3).
card_layout('lavalanche', 'normal').

% Found in: ONS
card_name('lavamancer\'s skill', 'Lavamancer\'s Skill').
card_type('lavamancer\'s skill', 'Enchantment — Aura').
card_types('lavamancer\'s skill', ['Enchantment']).
card_subtypes('lavamancer\'s skill', ['Aura']).
card_colors('lavamancer\'s skill', ['R']).
card_text('lavamancer\'s skill', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target creature.\"\nAs long as enchanted creature is a Wizard, it has \"{T}: This creature deals 2 damage to target creature.\"').
card_mana_cost('lavamancer\'s skill', ['1', 'R']).
card_cmc('lavamancer\'s skill', 2).
card_layout('lavamancer\'s skill', 'normal').

% Found in: BFZ
card_name('lavastep raider', 'Lavastep Raider').
card_type('lavastep raider', 'Creature — Goblin Warrior').
card_types('lavastep raider', ['Creature']).
card_subtypes('lavastep raider', ['Goblin', 'Warrior']).
card_colors('lavastep raider', ['R']).
card_text('lavastep raider', '{2}{R}: Lavastep Raider gets +2/+0 until end of turn.').
card_mana_cost('lavastep raider', ['R']).
card_cmc('lavastep raider', 1).
card_layout('lavastep raider', 'normal').
card_power('lavastep raider', 1).
card_toughness('lavastep raider', 2).

% Found in: DGM
card_name('lavinia of the tenth', 'Lavinia of the Tenth').
card_type('lavinia of the tenth', 'Legendary Creature — Human Soldier').
card_types('lavinia of the tenth', ['Creature']).
card_subtypes('lavinia of the tenth', ['Human', 'Soldier']).
card_supertypes('lavinia of the tenth', ['Legendary']).
card_colors('lavinia of the tenth', ['W', 'U']).
card_text('lavinia of the tenth', 'Protection from red\nWhen Lavinia of the Tenth enters the battlefield, detain each nonland permanent your opponents control with converted mana cost 4 or less. (Until your next turn, those permanents can\'t attack or block and their activated abilities can\'t be activated.)').
card_mana_cost('lavinia of the tenth', ['3', 'W', 'U']).
card_cmc('lavinia of the tenth', 5).
card_layout('lavinia of the tenth', 'normal').
card_power('lavinia of the tenth', 4).
card_toughness('lavinia of the tenth', 4).

% Found in: NMS
card_name('lawbringer', 'Lawbringer').
card_type('lawbringer', 'Creature — Kor Rebel').
card_types('lawbringer', ['Creature']).
card_subtypes('lawbringer', ['Kor', 'Rebel']).
card_colors('lawbringer', ['W']).
card_text('lawbringer', '{T}, Sacrifice Lawbringer: Exile target red creature.').
card_mana_cost('lawbringer', ['2', 'W']).
card_cmc('lawbringer', 3).
card_layout('lawbringer', 'normal').
card_power('lawbringer', 2).
card_toughness('lawbringer', 2).

% Found in: ROE
card_name('lay bare', 'Lay Bare').
card_type('lay bare', 'Instant').
card_types('lay bare', ['Instant']).
card_subtypes('lay bare', []).
card_colors('lay bare', ['U']).
card_text('lay bare', 'Counter target spell. Look at its controller\'s hand.').
card_mana_cost('lay bare', ['2', 'U', 'U']).
card_cmc('lay bare', 4).
card_layout('lay bare', 'normal').

% Found in: APC, M14
card_name('lay of the land', 'Lay of the Land').
card_type('lay of the land', 'Sorcery').
card_types('lay of the land', ['Sorcery']).
card_subtypes('lay of the land', []).
card_colors('lay of the land', ['G']).
card_text('lay of the land', 'Search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('lay of the land', ['G']).
card_cmc('lay of the land', 1).
card_layout('lay of the land', 'normal').

% Found in: ONS, USG
card_name('lay waste', 'Lay Waste').
card_type('lay waste', 'Sorcery').
card_types('lay waste', ['Sorcery']).
card_subtypes('lay waste', []).
card_colors('lay waste', ['R']).
card_text('lay waste', 'Destroy target land.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('lay waste', ['3', 'R']).
card_cmc('lay waste', 4).
card_layout('lay waste', 'normal').

% Found in: GTC
card_name('lazav, dimir mastermind', 'Lazav, Dimir Mastermind').
card_type('lazav, dimir mastermind', 'Legendary Creature — Shapeshifter').
card_types('lazav, dimir mastermind', ['Creature']).
card_subtypes('lazav, dimir mastermind', ['Shapeshifter']).
card_supertypes('lazav, dimir mastermind', ['Legendary']).
card_colors('lazav, dimir mastermind', ['U', 'B']).
card_text('lazav, dimir mastermind', 'Hexproof\nWhenever a creature card is put into an opponent\'s graveyard from anywhere, you may have Lazav, Dimir Mastermind become a copy of that card except its name is still Lazav, Dimir Mastermind, it\'s legendary in addition to its other types, and it gains hexproof and this ability.').
card_mana_cost('lazav, dimir mastermind', ['U', 'U', 'B', 'B']).
card_cmc('lazav, dimir mastermind', 4).
card_layout('lazav, dimir mastermind', 'normal').
card_power('lazav, dimir mastermind', 3).
card_toughness('lazav, dimir mastermind', 3).

% Found in: JUD
card_name('lead astray', 'Lead Astray').
card_type('lead astray', 'Instant').
card_types('lead astray', ['Instant']).
card_subtypes('lead astray', []).
card_colors('lead astray', ['W']).
card_text('lead astray', 'Tap up to two target creatures.').
card_mana_cost('lead astray', ['1', 'W']).
card_cmc('lead astray', 2).
card_layout('lead astray', 'normal').

% Found in: 6ED, MIR
card_name('lead golem', 'Lead Golem').
card_type('lead golem', 'Artifact Creature — Golem').
card_types('lead golem', ['Artifact', 'Creature']).
card_subtypes('lead golem', ['Golem']).
card_colors('lead golem', []).
card_text('lead golem', 'Whenever Lead Golem attacks, it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('lead golem', ['5']).
card_cmc('lead golem', 5).
card_layout('lead golem', 'normal').
card_power('lead golem', 3).
card_toughness('lead golem', 5).

% Found in: CNS, DDH, MBS
card_name('lead the stampede', 'Lead the Stampede').
card_type('lead the stampede', 'Sorcery').
card_types('lead the stampede', ['Sorcery']).
card_subtypes('lead the stampede', []).
card_colors('lead the stampede', ['G']).
card_text('lead the stampede', 'Look at the top five cards of your library. You may reveal any number of creature cards from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('lead the stampede', ['2', 'G']).
card_cmc('lead the stampede', 3).
card_layout('lead the stampede', 'normal').

% Found in: VIS
card_name('lead-belly chimera', 'Lead-Belly Chimera').
card_type('lead-belly chimera', 'Artifact Creature — Chimera').
card_types('lead-belly chimera', ['Artifact', 'Creature']).
card_subtypes('lead-belly chimera', ['Chimera']).
card_colors('lead-belly chimera', []).
card_text('lead-belly chimera', 'Trample\nSacrifice Lead-Belly Chimera: Put a +2/+2 counter on target Chimera creature. It gains trample. (This effect lasts indefinitely.)').
card_mana_cost('lead-belly chimera', ['4']).
card_cmc('lead-belly chimera', 4).
card_layout('lead-belly chimera', 'normal').
card_power('lead-belly chimera', 2).
card_toughness('lead-belly chimera', 2).

% Found in: FUT
card_name('leaden fists', 'Leaden Fists').
card_type('leaden fists', 'Enchantment — Aura').
card_types('leaden fists', ['Enchantment']).
card_subtypes('leaden fists', ['Aura']).
card_colors('leaden fists', ['U']).
card_text('leaden fists', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +3/+3 and doesn\'t untap during its controller\'s untap step.').
card_mana_cost('leaden fists', ['2', 'U']).
card_cmc('leaden fists', 3).
card_layout('leaden fists', 'normal').

% Found in: HOP, MRD, SOM
card_name('leaden myr', 'Leaden Myr').
card_type('leaden myr', 'Artifact Creature — Myr').
card_types('leaden myr', ['Artifact', 'Creature']).
card_subtypes('leaden myr', ['Myr']).
card_colors('leaden myr', []).
card_text('leaden myr', '{T}: Add {B} to your mana pool.').
card_mana_cost('leaden myr', ['2']).
card_cmc('leaden myr', 2).
card_layout('leaden myr', 'normal').
card_power('leaden myr', 1).
card_toughness('leaden myr', 1).

% Found in: ROE
card_name('leaf arrow', 'Leaf Arrow').
card_type('leaf arrow', 'Instant').
card_types('leaf arrow', ['Instant']).
card_subtypes('leaf arrow', []).
card_colors('leaf arrow', ['G']).
card_text('leaf arrow', 'Leaf Arrow deals 3 damage to target creature with flying.').
card_mana_cost('leaf arrow', ['G']).
card_cmc('leaf arrow', 1).
card_layout('leaf arrow', 'normal').

% Found in: ODY
card_name('leaf dancer', 'Leaf Dancer').
card_type('leaf dancer', 'Creature — Centaur').
card_types('leaf dancer', ['Creature']).
card_subtypes('leaf dancer', ['Centaur']).
card_colors('leaf dancer', ['G']).
card_text('leaf dancer', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('leaf dancer', ['1', 'G', 'G']).
card_cmc('leaf dancer', 3).
card_layout('leaf dancer', 'normal').
card_power('leaf dancer', 2).
card_toughness('leaf dancer', 2).

% Found in: ARC, LRW, ORI
card_name('leaf gilder', 'Leaf Gilder').
card_type('leaf gilder', 'Creature — Elf Druid').
card_types('leaf gilder', ['Creature']).
card_subtypes('leaf gilder', ['Elf', 'Druid']).
card_colors('leaf gilder', ['G']).
card_text('leaf gilder', '{T}: Add {G} to your mana pool.').
card_mana_cost('leaf gilder', ['1', 'G']).
card_cmc('leaf gilder', 2).
card_layout('leaf gilder', 'normal').
card_power('leaf gilder', 2).
card_toughness('leaf gilder', 1).

% Found in: MOR
card_name('leaf-crowned elder', 'Leaf-Crowned Elder').
card_type('leaf-crowned elder', 'Creature — Treefolk Shaman').
card_types('leaf-crowned elder', ['Creature']).
card_subtypes('leaf-crowned elder', ['Treefolk', 'Shaman']).
card_colors('leaf-crowned elder', ['G']).
card_text('leaf-crowned elder', 'Kinship — At the beginning of your upkeep, you may look at the top card of your library. If it shares a creature type with Leaf-Crowned Elder, you may reveal it. If you do, you may play that card without paying its mana cost.').
card_mana_cost('leaf-crowned elder', ['2', 'G', 'G']).
card_cmc('leaf-crowned elder', 4).
card_layout('leaf-crowned elder', 'normal').
card_power('leaf-crowned elder', 3).
card_toughness('leaf-crowned elder', 5).

% Found in: THS
card_name('leafcrown dryad', 'Leafcrown Dryad').
card_type('leafcrown dryad', 'Enchantment Creature — Nymph Dryad').
card_types('leafcrown dryad', ['Enchantment', 'Creature']).
card_subtypes('leafcrown dryad', ['Nymph', 'Dryad']).
card_colors('leafcrown dryad', ['G']).
card_text('leafcrown dryad', 'Bestow {3}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nReach\nEnchanted creature gets +2/+2 and has reach.').
card_mana_cost('leafcrown dryad', ['1', 'G']).
card_cmc('leafcrown dryad', 2).
card_layout('leafcrown dryad', 'normal').
card_power('leafcrown dryad', 2).
card_toughness('leafcrown dryad', 2).

% Found in: C13, DIS
card_name('leafdrake roost', 'Leafdrake Roost').
card_type('leafdrake roost', 'Enchantment — Aura').
card_types('leafdrake roost', ['Enchantment']).
card_subtypes('leafdrake roost', ['Aura']).
card_colors('leafdrake roost', ['U', 'G']).
card_text('leafdrake roost', 'Enchant land\nEnchanted land has \"{G}{U}, {T}: Put a 2/2 green and blue Drake creature token with flying onto the battlefield.\"').
card_mana_cost('leafdrake roost', ['3', 'G', 'U']).
card_cmc('leafdrake roost', 5).
card_layout('leafdrake roost', 'normal').

% Found in: STH
card_name('leap', 'Leap').
card_type('leap', 'Instant').
card_types('leap', ['Instant']).
card_subtypes('leap', []).
card_colors('leap', ['U']).
card_text('leap', 'Target creature gains flying until end of turn.\nDraw a card.').
card_mana_cost('leap', ['U']).
card_cmc('leap', 1).
card_layout('leap', 'normal').

% Found in: AVR
card_name('leap of faith', 'Leap of Faith').
card_type('leap of faith', 'Instant').
card_types('leap of faith', ['Instant']).
card_subtypes('leap of faith', []).
card_colors('leap of faith', ['W']).
card_text('leap of faith', 'Target creature gains flying until end of turn. Prevent all damage that would be dealt to that creature this turn.').
card_mana_cost('leap of faith', ['2', 'W']).
card_cmc('leap of faith', 3).
card_layout('leap of faith', 'normal').

% Found in: GPT
card_name('leap of flame', 'Leap of Flame').
card_type('leap of flame', 'Instant').
card_types('leap of flame', ['Instant']).
card_subtypes('leap of flame', []).
card_colors('leap of flame', ['U', 'R']).
card_text('leap of flame', 'Replicate {U}{R} (When you cast this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nTarget creature gets +1/+0 and gains flying and first strike until end of turn.').
card_mana_cost('leap of flame', ['U', 'R']).
card_cmc('leap of flame', 2).
card_layout('leap of flame', 'normal').

% Found in: HML, ME2
card_name('leaping lizard', 'Leaping Lizard').
card_type('leaping lizard', 'Creature — Lizard').
card_types('leaping lizard', ['Creature']).
card_subtypes('leaping lizard', ['Lizard']).
card_colors('leaping lizard', ['G']).
card_text('leaping lizard', '{1}{G}: Leaping Lizard gets -0/-1 and gains flying until end of turn.').
card_mana_cost('leaping lizard', ['1', 'G', 'G']).
card_cmc('leaping lizard', 3).
card_layout('leaping lizard', 'normal').
card_power('leaping lizard', 2).
card_toughness('leaping lizard', 3).

% Found in: KTK
card_name('leaping master', 'Leaping Master').
card_type('leaping master', 'Creature — Human Monk').
card_types('leaping master', ['Creature']).
card_subtypes('leaping master', ['Human', 'Monk']).
card_colors('leaping master', ['R']).
card_text('leaping master', '{2}{W}: Leaping Master gains flying until end of turn.').
card_mana_cost('leaping master', ['1', 'R']).
card_cmc('leaping master', 2).
card_layout('leaping master', 'normal').
card_power('leaping master', 2).
card_toughness('leaping master', 1).

% Found in: DTK
card_name('learn from the past', 'Learn from the Past').
card_type('learn from the past', 'Instant').
card_types('learn from the past', ['Instant']).
card_subtypes('learn from the past', []).
card_colors('learn from the past', ['U']).
card_text('learn from the past', 'Target player shuffles his or her graveyard into his or her library.\nDraw a card.').
card_mana_cost('learn from the past', ['3', 'U']).
card_cmc('learn from the past', 4).
card_layout('learn from the past', 'normal').

% Found in: RAV
card_name('leashling', 'Leashling').
card_type('leashling', 'Artifact Creature — Hound').
card_types('leashling', ['Artifact', 'Creature']).
card_subtypes('leashling', ['Hound']).
card_colors('leashling', []).
card_text('leashling', 'Put a card from your hand on top of your library: Return Leashling to its owner\'s hand.').
card_mana_cost('leashling', ['6']).
card_cmc('leashling', 6).
card_layout('leashling', 'normal').
card_power('leashling', 3).
card_toughness('leashling', 3).

% Found in: WWK, pWPN
card_name('leatherback baloth', 'Leatherback Baloth').
card_type('leatherback baloth', 'Creature — Beast').
card_types('leatherback baloth', ['Creature']).
card_subtypes('leatherback baloth', ['Beast']).
card_colors('leatherback baloth', ['G']).
card_text('leatherback baloth', '').
card_mana_cost('leatherback baloth', ['G', 'G', 'G']).
card_cmc('leatherback baloth', 3).
card_layout('leatherback baloth', 'normal').
card_power('leatherback baloth', 4).
card_toughness('leatherback baloth', 5).

% Found in: RAV
card_name('leave no trace', 'Leave No Trace').
card_type('leave no trace', 'Instant').
card_types('leave no trace', ['Instant']).
card_subtypes('leave no trace', []).
card_colors('leave no trace', ['W']).
card_text('leave no trace', 'Radiance — Destroy target enchantment and each other enchantment that shares a color with it.').
card_mana_cost('leave no trace', ['1', 'W']).
card_cmc('leave no trace', 2).
card_layout('leave no trace', 'normal').

% Found in: SHM
card_name('leech bonder', 'Leech Bonder').
card_type('leech bonder', 'Creature — Merfolk Soldier').
card_types('leech bonder', ['Creature']).
card_subtypes('leech bonder', ['Merfolk', 'Soldier']).
card_colors('leech bonder', ['U']).
card_text('leech bonder', 'Leech Bonder enters the battlefield with two -1/-1 counters on it.\n{U}, {Q}: Move a counter from target creature onto a second target creature. ({Q} is the untap symbol.)').
card_mana_cost('leech bonder', ['2', 'U']).
card_cmc('leech bonder', 3).
card_layout('leech bonder', 'normal').
card_power('leech bonder', 3).
card_toughness('leech bonder', 3).

% Found in: HML, ME4
card_name('leeches', 'Leeches').
card_type('leeches', 'Sorcery').
card_types('leeches', ['Sorcery']).
card_subtypes('leeches', []).
card_colors('leeches', ['W']).
card_text('leeches', 'Target player loses all poison counters. Leeches deals that much damage to that player.').
card_mana_cost('leeches', ['1', 'W', 'W']).
card_cmc('leeches', 3).
card_layout('leeches', 'normal').
card_reserved('leeches').

% Found in: NPH
card_name('leeching bite', 'Leeching Bite').
card_type('leeching bite', 'Instant').
card_types('leeching bite', ['Instant']).
card_subtypes('leeching bite', []).
card_colors('leeching bite', ['G']).
card_text('leeching bite', 'Target creature gets +1/+1 until end of turn. Another target creature gets -1/-1 until end of turn.').
card_mana_cost('leeching bite', ['1', 'G']).
card_cmc('leeching bite', 2).
card_layout('leeching bite', 'normal').

% Found in: TMP
card_name('leeching licid', 'Leeching Licid').
card_type('leeching licid', 'Creature — Licid').
card_types('leeching licid', ['Creature']).
card_subtypes('leeching licid', ['Licid']).
card_colors('leeching licid', ['B']).
card_text('leeching licid', '{B}, {T}: Leeching Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {B} to end this effect.\nAt the beginning of the upkeep of enchanted creature\'s controller, Leeching Licid deals 1 damage to that player.').
card_mana_cost('leeching licid', ['1', 'B']).
card_cmc('leeching licid', 2).
card_layout('leeching licid', 'normal').
card_power('leeching licid', 1).
card_toughness('leeching licid', 1).

% Found in: M15
card_name('leeching sliver', 'Leeching Sliver').
card_type('leeching sliver', 'Creature — Sliver').
card_types('leeching sliver', ['Creature']).
card_subtypes('leeching sliver', ['Sliver']).
card_colors('leeching sliver', ['B']).
card_text('leeching sliver', 'Whenever a Sliver you control attacks, defending player loses 1 life.').
card_mana_cost('leeching sliver', ['1', 'B']).
card_cmc('leeching sliver', 2).
card_layout('leeching sliver', 'normal').
card_power('leeching sliver', 1).
card_toughness('leeching sliver', 1).

% Found in: HOP, SHM
card_name('leechridden swamp', 'Leechridden Swamp').
card_type('leechridden swamp', 'Land — Swamp').
card_types('leechridden swamp', ['Land']).
card_subtypes('leechridden swamp', ['Swamp']).
card_colors('leechridden swamp', []).
card_text('leechridden swamp', '({T}: Add {B} to your mana pool.)\nLeechridden Swamp enters the battlefield tapped.\n{B}, {T}: Each opponent loses 1 life. Activate this ability only if you control two or more black permanents.').
card_layout('leechridden swamp', 'normal').

% Found in: EVE
card_name('leering emblem', 'Leering Emblem').
card_type('leering emblem', 'Artifact — Equipment').
card_types('leering emblem', ['Artifact']).
card_subtypes('leering emblem', ['Equipment']).
card_colors('leering emblem', []).
card_text('leering emblem', 'Whenever you cast a spell, equipped creature gets +2/+2 until end of turn.\nEquip {2}').
card_mana_cost('leering emblem', ['2']).
card_cmc('leering emblem', 2).
card_layout('leering emblem', 'normal').

% Found in: MIR
card_name('leering gargoyle', 'Leering Gargoyle').
card_type('leering gargoyle', 'Creature — Gargoyle').
card_types('leering gargoyle', ['Creature']).
card_subtypes('leering gargoyle', ['Gargoyle']).
card_colors('leering gargoyle', ['W', 'U']).
card_text('leering gargoyle', 'Flying\n{T}: Leering Gargoyle gets -2/+2 and loses flying until end of turn.').
card_mana_cost('leering gargoyle', ['1', 'W', 'U']).
card_cmc('leering gargoyle', 3).
card_layout('leering gargoyle', 'normal').
card_power('leering gargoyle', 2).
card_toughness('leering gargoyle', 2).
card_reserved('leering gargoyle').

% Found in: ONS
card_name('leery fogbeast', 'Leery Fogbeast').
card_type('leery fogbeast', 'Creature — Beast').
card_types('leery fogbeast', ['Creature']).
card_subtypes('leery fogbeast', ['Beast']).
card_colors('leery fogbeast', ['G']).
card_text('leery fogbeast', 'Whenever Leery Fogbeast becomes blocked, prevent all combat damage that would be dealt this turn.').
card_mana_cost('leery fogbeast', ['2', 'G']).
card_cmc('leery fogbeast', 3).
card_layout('leery fogbeast', 'normal').
card_power('leery fogbeast', 4).
card_toughness('leery fogbeast', 2).

% Found in: 10E, APC
card_name('legacy weapon', 'Legacy Weapon').
card_type('legacy weapon', 'Legendary Artifact').
card_types('legacy weapon', ['Artifact']).
card_subtypes('legacy weapon', []).
card_supertypes('legacy weapon', ['Legendary']).
card_colors('legacy weapon', []).
card_text('legacy weapon', '{W}{U}{B}{R}{G}: Exile target permanent.\nIf Legacy Weapon would be put into a graveyard from anywhere, reveal Legacy Weapon and shuffle it into its owner\'s library instead.').
card_mana_cost('legacy weapon', ['7']).
card_cmc('legacy weapon', 7).
card_layout('legacy weapon', 'normal').

% Found in: TMP, TPR
card_name('legacy\'s allure', 'Legacy\'s Allure').
card_type('legacy\'s allure', 'Enchantment').
card_types('legacy\'s allure', ['Enchantment']).
card_subtypes('legacy\'s allure', []).
card_colors('legacy\'s allure', ['U']).
card_text('legacy\'s allure', 'At the beginning of your upkeep, you may put a treasure counter on Legacy\'s Allure.\nSacrifice Legacy\'s Allure: Gain control of target creature with power less than or equal to the number of treasure counters on Legacy\'s Allure. (This effect lasts indefinitely.)').
card_mana_cost('legacy\'s allure', ['U', 'U']).
card_cmc('legacy\'s allure', 2).
card_layout('legacy\'s allure', 'normal').

% Found in: TMP, TPR
card_name('legerdemain', 'Legerdemain').
card_type('legerdemain', 'Sorcery').
card_types('legerdemain', ['Sorcery']).
card_subtypes('legerdemain', []).
card_colors('legerdemain', ['U']).
card_text('legerdemain', 'Exchange control of target artifact or creature and another target permanent that shares one of those types with it. (This effect lasts indefinitely.)').
card_mana_cost('legerdemain', ['2', 'U', 'U']).
card_cmc('legerdemain', 4).
card_layout('legerdemain', 'normal').

% Found in: GTC
card_name('legion loyalist', 'Legion Loyalist').
card_type('legion loyalist', 'Creature — Goblin Soldier').
card_types('legion loyalist', ['Creature']).
card_subtypes('legion loyalist', ['Goblin', 'Soldier']).
card_colors('legion loyalist', ['R']).
card_text('legion loyalist', 'Haste\nBattalion — Whenever Legion Loyalist and at least two other creatures attack, creatures you control gain first strike and trample until end of turn and can\'t be blocked by creature tokens this turn.').
card_mana_cost('legion loyalist', ['R']).
card_cmc('legion loyalist', 1).
card_layout('legion loyalist', 'normal').
card_power('legion loyalist', 1).
card_toughness('legion loyalist', 1).

% Found in: DGM
card_name('legion\'s initiative', 'Legion\'s Initiative').
card_type('legion\'s initiative', 'Enchantment').
card_types('legion\'s initiative', ['Enchantment']).
card_subtypes('legion\'s initiative', []).
card_colors('legion\'s initiative', ['W', 'R']).
card_text('legion\'s initiative', 'Red creatures you control get +1/+0.\nWhite creatures you control get +0/+1.\n{R}{W}, Exile Legion\'s Initiative: Exile all creatures you control. At the beginning of the next combat, return those cards to the battlefield under their owner\'s control and those creatures gain haste until end of turn.').
card_mana_cost('legion\'s initiative', ['R', 'W']).
card_cmc('legion\'s initiative', 2).
card_layout('legion\'s initiative', 'normal').

% Found in: CST, ICE
card_name('legions of lim-dûl', 'Legions of Lim-Dûl').
card_type('legions of lim-dûl', 'Creature — Zombie').
card_types('legions of lim-dûl', ['Creature']).
card_subtypes('legions of lim-dûl', ['Zombie']).
card_colors('legions of lim-dûl', ['B']).
card_text('legions of lim-dûl', 'Snow swampwalk (This creature can\'t be blocked as long as defending player controls a snow Swamp.)').
card_mana_cost('legions of lim-dûl', ['1', 'B', 'B']).
card_cmc('legions of lim-dûl', 3).
card_layout('legions of lim-dûl', 'normal').
card_power('legions of lim-dûl', 2).
card_toughness('legions of lim-dûl', 3).

% Found in: KTK
card_name('lens of clarity', 'Lens of Clarity').
card_type('lens of clarity', 'Artifact').
card_types('lens of clarity', ['Artifact']).
card_subtypes('lens of clarity', []).
card_colors('lens of clarity', []).
card_text('lens of clarity', 'You may look at the top card of your library and at face-down creatures you don\'t control. (You may do this at any time.)').
card_mana_cost('lens of clarity', ['1']).
card_cmc('lens of clarity', 1).
card_layout('lens of clarity', 'normal').

% Found in: ARC, MRD
card_name('leonin abunas', 'Leonin Abunas').
card_type('leonin abunas', 'Creature — Cat Cleric').
card_types('leonin abunas', ['Creature']).
card_subtypes('leonin abunas', ['Cat', 'Cleric']).
card_colors('leonin abunas', ['W']).
card_text('leonin abunas', 'Artifacts you control have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_mana_cost('leonin abunas', ['3', 'W']).
card_cmc('leonin abunas', 4).
card_layout('leonin abunas', 'normal').
card_power('leonin abunas', 2).
card_toughness('leonin abunas', 5).

% Found in: SOM
card_name('leonin arbiter', 'Leonin Arbiter').
card_type('leonin arbiter', 'Creature — Cat Cleric').
card_types('leonin arbiter', ['Creature']).
card_subtypes('leonin arbiter', ['Cat', 'Cleric']).
card_colors('leonin arbiter', ['W']).
card_text('leonin arbiter', 'Players can\'t search libraries. Any player may pay {2} for that player to ignore this effect until end of turn.').
card_mana_cost('leonin arbiter', ['1', 'W']).
card_cmc('leonin arbiter', 2).
card_layout('leonin arbiter', 'normal').
card_power('leonin arbiter', 2).
card_toughness('leonin arbiter', 2).

% Found in: ARB
card_name('leonin armorguard', 'Leonin Armorguard').
card_type('leonin armorguard', 'Creature — Cat Soldier').
card_types('leonin armorguard', ['Creature']).
card_subtypes('leonin armorguard', ['Cat', 'Soldier']).
card_colors('leonin armorguard', ['W', 'G']).
card_text('leonin armorguard', 'When Leonin Armorguard enters the battlefield, creatures you control get +1/+1 until end of turn.').
card_mana_cost('leonin armorguard', ['2', 'G', 'W']).
card_cmc('leonin armorguard', 4).
card_layout('leonin armorguard', 'normal').
card_power('leonin armorguard', 3).
card_toughness('leonin armorguard', 3).

% Found in: DST
card_name('leonin battlemage', 'Leonin Battlemage').
card_type('leonin battlemage', 'Creature — Cat Wizard').
card_types('leonin battlemage', ['Creature']).
card_subtypes('leonin battlemage', ['Cat', 'Wizard']).
card_colors('leonin battlemage', ['W']).
card_text('leonin battlemage', '{T}: Target creature gets +1/+1 until end of turn.\nWhenever you cast a spell, you may untap Leonin Battlemage.').
card_mana_cost('leonin battlemage', ['3', 'W']).
card_cmc('leonin battlemage', 4).
card_layout('leonin battlemage', 'normal').
card_power('leonin battlemage', 2).
card_toughness('leonin battlemage', 3).

% Found in: C13, MRD
card_name('leonin bladetrap', 'Leonin Bladetrap').
card_type('leonin bladetrap', 'Artifact').
card_types('leonin bladetrap', ['Artifact']).
card_subtypes('leonin bladetrap', []).
card_colors('leonin bladetrap', []).
card_text('leonin bladetrap', 'Flash (You may cast this spell any time you could cast an instant.)\n{2}, Sacrifice Leonin Bladetrap: Leonin Bladetrap deals 2 damage to each attacking creature without flying.').
card_mana_cost('leonin bladetrap', ['3']).
card_cmc('leonin bladetrap', 3).
card_layout('leonin bladetrap', 'normal').

% Found in: DST
card_name('leonin bola', 'Leonin Bola').
card_type('leonin bola', 'Artifact — Equipment').
card_types('leonin bola', ['Artifact']).
card_subtypes('leonin bola', ['Equipment']).
card_colors('leonin bola', []).
card_text('leonin bola', 'Equipped creature has \"{T}, Unattach Leonin Bola: Tap target creature.\"\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('leonin bola', ['1']).
card_cmc('leonin bola', 1).
card_layout('leonin bola', 'normal').

% Found in: MRD
card_name('leonin den-guard', 'Leonin Den-Guard').
card_type('leonin den-guard', 'Creature — Cat Soldier').
card_types('leonin den-guard', ['Creature']).
card_subtypes('leonin den-guard', ['Cat', 'Soldier']).
card_colors('leonin den-guard', ['W']).
card_text('leonin den-guard', 'As long as Leonin Den-Guard is equipped, it gets +1/+1 and has vigilance.').
card_mana_cost('leonin den-guard', ['1', 'W']).
card_cmc('leonin den-guard', 2).
card_layout('leonin den-guard', 'normal').
card_power('leonin den-guard', 1).
card_toughness('leonin den-guard', 3).

% Found in: MRD
card_name('leonin elder', 'Leonin Elder').
card_type('leonin elder', 'Creature — Cat Cleric').
card_types('leonin elder', ['Creature']).
card_subtypes('leonin elder', ['Cat', 'Cleric']).
card_colors('leonin elder', ['W']).
card_text('leonin elder', 'Whenever an artifact enters the battlefield, you may gain 1 life.').
card_mana_cost('leonin elder', ['W']).
card_cmc('leonin elder', 1).
card_layout('leonin elder', 'normal').
card_power('leonin elder', 1).
card_toughness('leonin elder', 1).

% Found in: JOU
card_name('leonin iconoclast', 'Leonin Iconoclast').
card_type('leonin iconoclast', 'Creature — Cat Monk').
card_types('leonin iconoclast', ['Creature']).
card_subtypes('leonin iconoclast', ['Cat', 'Monk']).
card_colors('leonin iconoclast', ['W']).
card_text('leonin iconoclast', 'Heroic — Whenever you cast a spell that targets Leonin Iconoclast, destroy target enchantment creature an opponent controls.').
card_mana_cost('leonin iconoclast', ['3', 'W']).
card_cmc('leonin iconoclast', 4).
card_layout('leonin iconoclast', 'normal').
card_power('leonin iconoclast', 3).
card_toughness('leonin iconoclast', 2).

% Found in: MBS
card_name('leonin relic-warder', 'Leonin Relic-Warder').
card_type('leonin relic-warder', 'Creature — Cat Cleric').
card_types('leonin relic-warder', ['Creature']).
card_subtypes('leonin relic-warder', ['Cat', 'Cleric']).
card_colors('leonin relic-warder', ['W']).
card_text('leonin relic-warder', 'When Leonin Relic-Warder enters the battlefield, you may exile target artifact or enchantment.\nWhen Leonin Relic-Warder leaves the battlefield, return the exiled card to the battlefield under its owner\'s control.').
card_mana_cost('leonin relic-warder', ['W', 'W']).
card_cmc('leonin relic-warder', 2).
card_layout('leonin relic-warder', 'normal').
card_power('leonin relic-warder', 2).
card_toughness('leonin relic-warder', 2).

% Found in: 10E, MRD
card_name('leonin scimitar', 'Leonin Scimitar').
card_type('leonin scimitar', 'Artifact — Equipment').
card_types('leonin scimitar', ['Artifact']).
card_subtypes('leonin scimitar', ['Equipment']).
card_colors('leonin scimitar', []).
card_text('leonin scimitar', 'Equipped creature gets +1/+1.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('leonin scimitar', ['1']).
card_cmc('leonin scimitar', 1).
card_layout('leonin scimitar', 'normal').

% Found in: DST
card_name('leonin shikari', 'Leonin Shikari').
card_type('leonin shikari', 'Creature — Cat Soldier').
card_types('leonin shikari', ['Creature']).
card_subtypes('leonin shikari', ['Cat', 'Soldier']).
card_colors('leonin shikari', ['W']).
card_text('leonin shikari', 'You may activate equip abilities any time you could cast an instant.').
card_mana_cost('leonin shikari', ['1', 'W']).
card_cmc('leonin shikari', 2).
card_layout('leonin shikari', 'normal').
card_power('leonin shikari', 2).
card_toughness('leonin shikari', 2).

% Found in: 9ED, DDG, MBS, MRD
card_name('leonin skyhunter', 'Leonin Skyhunter').
card_type('leonin skyhunter', 'Creature — Cat Knight').
card_types('leonin skyhunter', ['Creature']).
card_subtypes('leonin skyhunter', ['Cat', 'Knight']).
card_colors('leonin skyhunter', ['W']).
card_text('leonin skyhunter', 'Flying').
card_mana_cost('leonin skyhunter', ['W', 'W']).
card_cmc('leonin skyhunter', 2).
card_layout('leonin skyhunter', 'normal').
card_power('leonin skyhunter', 2).
card_toughness('leonin skyhunter', 2).

% Found in: DDN, THS
card_name('leonin snarecaster', 'Leonin Snarecaster').
card_type('leonin snarecaster', 'Creature — Cat Soldier').
card_types('leonin snarecaster', ['Creature']).
card_subtypes('leonin snarecaster', ['Cat', 'Soldier']).
card_colors('leonin snarecaster', ['W']).
card_text('leonin snarecaster', 'When Leonin Snarecaster enters the battlefield, you may tap target creature.').
card_mana_cost('leonin snarecaster', ['1', 'W']).
card_cmc('leonin snarecaster', 2).
card_layout('leonin snarecaster', 'normal').
card_power('leonin snarecaster', 2).
card_toughness('leonin snarecaster', 1).

% Found in: 5DN
card_name('leonin squire', 'Leonin Squire').
card_type('leonin squire', 'Creature — Cat Soldier').
card_types('leonin squire', ['Creature']).
card_subtypes('leonin squire', ['Cat', 'Soldier']).
card_colors('leonin squire', ['W']).
card_text('leonin squire', 'When Leonin Squire enters the battlefield, return target artifact card with converted mana cost 1 or less from your graveyard to your hand.').
card_mana_cost('leonin squire', ['1', 'W']).
card_cmc('leonin squire', 2).
card_layout('leonin squire', 'normal').
card_power('leonin squire', 2).
card_toughness('leonin squire', 2).

% Found in: MRD
card_name('leonin sun standard', 'Leonin Sun Standard').
card_type('leonin sun standard', 'Artifact').
card_types('leonin sun standard', ['Artifact']).
card_subtypes('leonin sun standard', []).
card_colors('leonin sun standard', []).
card_text('leonin sun standard', '{1}{W}: Creatures you control get +1/+1 until end of turn.').
card_mana_cost('leonin sun standard', ['2']).
card_cmc('leonin sun standard', 2).
card_layout('leonin sun standard', 'normal').

% Found in: 5ED, 6ED, 7ED, ICE
card_name('leshrac\'s rite', 'Leshrac\'s Rite').
card_type('leshrac\'s rite', 'Enchantment — Aura').
card_types('leshrac\'s rite', ['Enchantment']).
card_subtypes('leshrac\'s rite', ['Aura']).
card_colors('leshrac\'s rite', ['B']).
card_text('leshrac\'s rite', 'Enchant creature\nEnchanted creature has swampwalk. (It can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('leshrac\'s rite', ['B']).
card_cmc('leshrac\'s rite', 1).
card_layout('leshrac\'s rite', 'normal').

% Found in: ICE
card_name('leshrac\'s sigil', 'Leshrac\'s Sigil').
card_type('leshrac\'s sigil', 'Enchantment').
card_types('leshrac\'s sigil', ['Enchantment']).
card_subtypes('leshrac\'s sigil', []).
card_colors('leshrac\'s sigil', ['B']).
card_text('leshrac\'s sigil', 'Whenever an opponent casts a green spell, you may pay {B}{B}. If you do, look at that player\'s hand and choose a card from it. The player discards that card.\n{B}{B}: Return Leshrac\'s Sigil to its owner\'s hand.').
card_mana_cost('leshrac\'s sigil', ['B', 'B']).
card_cmc('leshrac\'s sigil', 2).
card_layout('leshrac\'s sigil', 'normal').

% Found in: 8ED, PCY
card_name('lesser gargadon', 'Lesser Gargadon').
card_type('lesser gargadon', 'Creature — Beast').
card_types('lesser gargadon', ['Creature']).
card_subtypes('lesser gargadon', ['Beast']).
card_colors('lesser gargadon', ['R']).
card_text('lesser gargadon', 'Whenever Lesser Gargadon attacks or blocks, sacrifice a land.').
card_mana_cost('lesser gargadon', ['2', 'R', 'R']).
card_cmc('lesser gargadon', 4).
card_layout('lesser gargadon', 'normal').
card_power('lesser gargadon', 6).
card_toughness('lesser gargadon', 4).

% Found in: LEG, ME3
card_name('lesser werewolf', 'Lesser Werewolf').
card_type('lesser werewolf', 'Creature — Werewolf').
card_types('lesser werewolf', ['Creature']).
card_subtypes('lesser werewolf', ['Werewolf']).
card_colors('lesser werewolf', ['B']).
card_text('lesser werewolf', '{B}: If Lesser Werewolf\'s power is 1 or more, it gets -1/-0 until end of turn and put a -0/-1 counter on target creature blocking or blocked by Lesser Werewolf. Activate this ability only during the declare blockers step.').
card_mana_cost('lesser werewolf', ['3', 'B']).
card_cmc('lesser werewolf', 4).
card_layout('lesser werewolf', 'normal').
card_power('lesser werewolf', 2).
card_toughness('lesser werewolf', 4).

% Found in: SCG
card_name('lethal vapors', 'Lethal Vapors').
card_type('lethal vapors', 'Enchantment').
card_types('lethal vapors', ['Enchantment']).
card_subtypes('lethal vapors', []).
card_colors('lethal vapors', ['B']).
card_text('lethal vapors', 'Whenever a creature enters the battlefield, destroy it.\n{0}: Destroy Lethal Vapors. You skip your next turn. Any player may activate this ability.').
card_mana_cost('lethal vapors', ['2', 'B', 'B']).
card_cmc('lethal vapors', 4).
card_layout('lethal vapors', 'normal').

% Found in: ZEN
card_name('lethargy trap', 'Lethargy Trap').
card_type('lethargy trap', 'Instant — Trap').
card_types('lethargy trap', ['Instant']).
card_subtypes('lethargy trap', ['Trap']).
card_colors('lethargy trap', ['U']).
card_text('lethargy trap', 'If three or more creatures are attacking, you may pay {U} rather than pay Lethargy Trap\'s mana cost.\nAttacking creatures get -3/-0 until end of turn.').
card_mana_cost('lethargy trap', ['3', 'U']).
card_cmc('lethargy trap', 4).
card_layout('lethargy trap', 'normal').

% Found in: HOP
card_name('lethe lake', 'Lethe Lake').
card_type('lethe lake', 'Plane — Arkhos').
card_types('lethe lake', ['Plane']).
card_subtypes('lethe lake', ['Arkhos']).
card_colors('lethe lake', []).
card_text('lethe lake', 'At the beginning of your upkeep, put the top ten cards of your library into your graveyard.\nWhenever you roll {C}, target player puts the top ten cards of his or her library into his or her graveyard.').
card_layout('lethe lake', 'plane').

% Found in: UNH
card_name('letter bomb', 'Letter Bomb').
card_type('letter bomb', 'Artifact').
card_types('letter bomb', ['Artifact']).
card_subtypes('letter bomb', []).
card_colors('letter bomb', []).
card_text('letter bomb', 'When Letter Bomb comes into play, sign it and shuffle it into target player\'s library. That player reveals each card he or she draws until Letter Bomb is drawn. When that player draws Letter Bomb, it deals 19½ damage to him or her.').
card_mana_cost('letter bomb', ['6']).
card_cmc('letter bomb', 6).
card_layout('letter bomb', 'normal').

% Found in: MRD
card_name('leveler', 'Leveler').
card_type('leveler', 'Artifact Creature — Juggernaut').
card_types('leveler', ['Artifact', 'Creature']).
card_subtypes('leveler', ['Juggernaut']).
card_colors('leveler', []).
card_text('leveler', 'When Leveler enters the battlefield, exile all cards from your library.').
card_mana_cost('leveler', ['5']).
card_cmc('leveler', 5).
card_layout('leveler', 'normal').
card_power('leveler', 10).
card_toughness('leveler', 10).

% Found in: 4ED, 5ED, BTD, DRK, TSB
card_name('leviathan', 'Leviathan').
card_type('leviathan', 'Creature — Leviathan').
card_types('leviathan', ['Creature']).
card_subtypes('leviathan', ['Leviathan']).
card_colors('leviathan', ['U']).
card_text('leviathan', 'Trample\nLeviathan enters the battlefield tapped and doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may sacrifice two Islands. If you do, untap Leviathan.\nLeviathan can\'t attack unless you sacrifice two Islands. (This cost is paid as attackers are declared.)').
card_mana_cost('leviathan', ['5', 'U', 'U', 'U', 'U']).
card_cmc('leviathan', 9).
card_layout('leviathan', 'normal').
card_power('leviathan', 10).
card_toughness('leviathan', 10).

% Found in: 7ED, 9ED, M10, M12, ULG
card_name('levitation', 'Levitation').
card_type('levitation', 'Enchantment').
card_types('levitation', ['Enchantment']).
card_subtypes('levitation', []).
card_colors('levitation', ['U']).
card_text('levitation', 'Creatures you control have flying.').
card_mana_cost('levitation', ['2', 'U', 'U']).
card_cmc('levitation', 4).
card_layout('levitation', 'normal').

% Found in: UGL
card_name('lexivore', 'Lexivore').
card_type('lexivore', 'Creature — Beast').
card_types('lexivore', ['Creature']).
card_subtypes('lexivore', ['Beast']).
card_colors('lexivore', ['W']).
card_text('lexivore', 'If Lexivore damages any player, destroy target card in play, other than Lexivore, with the most lines of text in its text box. (If more than one card has the most lines of text, you choose which of those cards to destroy.)').
card_mana_cost('lexivore', ['3', 'W']).
card_cmc('lexivore', 4).
card_layout('lexivore', 'normal').
card_power('lexivore', 2).
card_toughness('lexivore', 3).

% Found in: 2ED, 3ED, 4ED, 5ED, 9ED, CED, CEI, LEA, LEB
card_name('ley druid', 'Ley Druid').
card_type('ley druid', 'Creature — Human Druid').
card_types('ley druid', ['Creature']).
card_subtypes('ley druid', ['Human', 'Druid']).
card_colors('ley druid', ['G']).
card_text('ley druid', '{T}: Untap target land.').
card_mana_cost('ley druid', ['2', 'G']).
card_cmc('ley druid', 3).
card_layout('ley druid', 'normal').
card_power('ley druid', 1).
card_toughness('ley druid', 1).

% Found in: MMQ
card_name('ley line', 'Ley Line').
card_type('ley line', 'Enchantment').
card_types('ley line', ['Enchantment']).
card_subtypes('ley line', []).
card_colors('ley line', ['G']).
card_text('ley line', 'At the beginning of each player\'s upkeep, that player may put a +1/+1 counter on target creature of his or her choice.').
card_mana_cost('ley line', ['3', 'G']).
card_cmc('ley line', 4).
card_layout('ley line', 'normal').

% Found in: M11
card_name('leyline of anticipation', 'Leyline of Anticipation').
card_type('leyline of anticipation', 'Enchantment').
card_types('leyline of anticipation', ['Enchantment']).
card_subtypes('leyline of anticipation', []).
card_colors('leyline of anticipation', ['U']).
card_text('leyline of anticipation', 'If Leyline of Anticipation is in your opening hand, you may begin the game with it on the battlefield.\nYou may cast nonland cards as though they had flash. (You may cast them any time you could cast an instant.)').
card_mana_cost('leyline of anticipation', ['2', 'U', 'U']).
card_cmc('leyline of anticipation', 4).
card_layout('leyline of anticipation', 'normal').

% Found in: GPT
card_name('leyline of lifeforce', 'Leyline of Lifeforce').
card_type('leyline of lifeforce', 'Enchantment').
card_types('leyline of lifeforce', ['Enchantment']).
card_subtypes('leyline of lifeforce', []).
card_colors('leyline of lifeforce', ['G']).
card_text('leyline of lifeforce', 'If Leyline of Lifeforce is in your opening hand, you may begin the game with it on the battlefield.\nCreature spells can\'t be countered.').
card_mana_cost('leyline of lifeforce', ['2', 'G', 'G']).
card_cmc('leyline of lifeforce', 4).
card_layout('leyline of lifeforce', 'normal').

% Found in: GPT
card_name('leyline of lightning', 'Leyline of Lightning').
card_type('leyline of lightning', 'Enchantment').
card_types('leyline of lightning', ['Enchantment']).
card_subtypes('leyline of lightning', []).
card_colors('leyline of lightning', ['R']).
card_text('leyline of lightning', 'If Leyline of Lightning is in your opening hand, you may begin the game with it on the battlefield.\nWhenever you cast a spell, you may pay {1}. If you do, Leyline of Lightning deals 1 damage to target player.').
card_mana_cost('leyline of lightning', ['2', 'R', 'R']).
card_cmc('leyline of lightning', 4).
card_layout('leyline of lightning', 'normal').

% Found in: M11
card_name('leyline of punishment', 'Leyline of Punishment').
card_type('leyline of punishment', 'Enchantment').
card_types('leyline of punishment', ['Enchantment']).
card_subtypes('leyline of punishment', []).
card_colors('leyline of punishment', ['R']).
card_text('leyline of punishment', 'If Leyline of Punishment is in your opening hand, you may begin the game with it on the battlefield.\nPlayers can\'t gain life.\nDamage can\'t be prevented.').
card_mana_cost('leyline of punishment', ['2', 'R', 'R']).
card_cmc('leyline of punishment', 4).
card_layout('leyline of punishment', 'normal').

% Found in: M11, MM2
card_name('leyline of sanctity', 'Leyline of Sanctity').
card_type('leyline of sanctity', 'Enchantment').
card_types('leyline of sanctity', ['Enchantment']).
card_subtypes('leyline of sanctity', []).
card_colors('leyline of sanctity', ['W']).
card_text('leyline of sanctity', 'If Leyline of Sanctity is in your opening hand, you may begin the game with it on the battlefield.\nYou have hexproof. (You can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('leyline of sanctity', ['2', 'W', 'W']).
card_cmc('leyline of sanctity', 4).
card_layout('leyline of sanctity', 'normal').

% Found in: GPT
card_name('leyline of singularity', 'Leyline of Singularity').
card_type('leyline of singularity', 'Enchantment').
card_types('leyline of singularity', ['Enchantment']).
card_subtypes('leyline of singularity', []).
card_colors('leyline of singularity', ['U']).
card_text('leyline of singularity', 'If Leyline of Singularity is in your opening hand, you may begin the game with it on the battlefield.\nAll nonland permanents are legendary.').
card_mana_cost('leyline of singularity', ['2', 'U', 'U']).
card_cmc('leyline of singularity', 4).
card_layout('leyline of singularity', 'normal').

% Found in: GPT
card_name('leyline of the meek', 'Leyline of the Meek').
card_type('leyline of the meek', 'Enchantment').
card_types('leyline of the meek', ['Enchantment']).
card_subtypes('leyline of the meek', []).
card_colors('leyline of the meek', ['W']).
card_text('leyline of the meek', 'If Leyline of the Meek is in your opening hand, you may begin the game with it on the battlefield.\nCreature tokens get +1/+1.').
card_mana_cost('leyline of the meek', ['2', 'W', 'W']).
card_cmc('leyline of the meek', 4).
card_layout('leyline of the meek', 'normal').

% Found in: GPT, M11
card_name('leyline of the void', 'Leyline of the Void').
card_type('leyline of the void', 'Enchantment').
card_types('leyline of the void', ['Enchantment']).
card_subtypes('leyline of the void', []).
card_colors('leyline of the void', ['B']).
card_text('leyline of the void', 'If Leyline of the Void is in your opening hand, you may begin the game with it on the battlefield.\nIf a card would be put into an opponent\'s graveyard from anywhere, exile it instead.').
card_mana_cost('leyline of the void', ['2', 'B', 'B']).
card_cmc('leyline of the void', 4).
card_layout('leyline of the void', 'normal').

% Found in: M11
card_name('leyline of vitality', 'Leyline of Vitality').
card_type('leyline of vitality', 'Enchantment').
card_types('leyline of vitality', ['Enchantment']).
card_subtypes('leyline of vitality', []).
card_colors('leyline of vitality', ['G']).
card_text('leyline of vitality', 'If Leyline of Vitality is in your opening hand, you may begin the game with it on the battlefield.\nCreatures you control get +0/+1.\nWhenever a creature enters the battlefield under your control, you may gain 1 life.').
card_mana_cost('leyline of vitality', ['2', 'G', 'G']).
card_cmc('leyline of vitality', 4).
card_layout('leyline of vitality', 'normal').

% Found in: DDM, GTC
card_name('leyline phantom', 'Leyline Phantom').
card_type('leyline phantom', 'Creature — Illusion').
card_types('leyline phantom', ['Creature']).
card_subtypes('leyline phantom', ['Illusion']).
card_colors('leyline phantom', ['U']).
card_text('leyline phantom', 'When Leyline Phantom deals combat damage, return it to its owner\'s hand. (Return it only if it survived combat.)').
card_mana_cost('leyline phantom', ['4', 'U']).
card_cmc('leyline phantom', 5).
card_layout('leyline phantom', 'normal').
card_power('leyline phantom', 5).
card_toughness('leyline phantom', 5).

% Found in: 5ED, 8ED, BRB, CMD, DKM, ICE
card_name('lhurgoyf', 'Lhurgoyf').
card_type('lhurgoyf', 'Creature — Lhurgoyf').
card_types('lhurgoyf', ['Creature']).
card_subtypes('lhurgoyf', ['Lhurgoyf']).
card_colors('lhurgoyf', ['G']).
card_text('lhurgoyf', 'Lhurgoyf\'s power is equal to the number of creature cards in all graveyards and its toughness is equal to that number plus 1.').
card_mana_cost('lhurgoyf', ['2', 'G', 'G']).
card_cmc('lhurgoyf', 4).
card_layout('lhurgoyf', 'normal').
card_power('lhurgoyf', '*').
card_toughness('lhurgoyf', '1+*').

% Found in: MMQ
card_name('liability', 'Liability').
card_type('liability', 'Enchantment').
card_types('liability', ['Enchantment']).
card_subtypes('liability', []).
card_colors('liability', ['B']).
card_text('liability', 'Whenever a nontoken permanent is put into a player\'s graveyard from the battlefield, that player loses 1 life.').
card_mana_cost('liability', ['1', 'B', 'B']).
card_cmc('liability', 3).
card_layout('liability', 'normal').

% Found in: MRD
card_name('liar\'s pendulum', 'Liar\'s Pendulum').
card_type('liar\'s pendulum', 'Artifact').
card_types('liar\'s pendulum', ['Artifact']).
card_subtypes('liar\'s pendulum', []).
card_colors('liar\'s pendulum', []).
card_text('liar\'s pendulum', '{2}, {T}: Name a card. Target opponent guesses whether a card with that name is in your hand. You may reveal your hand. If you do and your opponent guessed wrong, draw a card.').
card_mana_cost('liar\'s pendulum', ['1']).
card_cmc('liar\'s pendulum', 1).
card_layout('liar\'s pendulum', 'normal').

% Found in: INV
card_name('liberate', 'Liberate').
card_type('liberate', 'Instant').
card_types('liberate', ['Instant']).
card_subtypes('liberate', []).
card_colors('liberate', ['W']).
card_text('liberate', 'Exile target creature you control. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('liberate', ['1', 'W']).
card_cmc('liberate', 2).
card_layout('liberate', 'normal').

% Found in: JUD
card_name('liberated dwarf', 'Liberated Dwarf').
card_type('liberated dwarf', 'Creature — Dwarf').
card_types('liberated dwarf', ['Creature']).
card_subtypes('liberated dwarf', ['Dwarf']).
card_colors('liberated dwarf', ['R']).
card_text('liberated dwarf', '{R}, Sacrifice Liberated Dwarf: Target green creature gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('liberated dwarf', ['R']).
card_cmc('liberated dwarf', 1).
card_layout('liberated dwarf', 'normal').
card_power('liberated dwarf', 1).
card_toughness('liberated dwarf', 1).

% Found in: ARN, ME4, VMA
card_name('library of alexandria', 'Library of Alexandria').
card_type('library of alexandria', 'Land').
card_types('library of alexandria', ['Land']).
card_subtypes('library of alexandria', []).
card_colors('library of alexandria', []).
card_text('library of alexandria', '{T}: Add {1} to your mana pool.\n{T}: Draw a card. Activate this ability only if you have exactly seven cards in hand.').
card_layout('library of alexandria', 'normal').
card_reserved('library of alexandria').

% Found in: 6ED, ALL
card_name('library of lat-nam', 'Library of Lat-Nam').
card_type('library of lat-nam', 'Sorcery').
card_types('library of lat-nam', ['Sorcery']).
card_subtypes('library of lat-nam', []).
card_colors('library of lat-nam', ['U']).
card_text('library of lat-nam', 'An opponent chooses one —\n• You draw three cards at the beginning of the next turn\'s upkeep.\n• You search your library for a card, put that card into your hand, then shuffle your library.').
card_mana_cost('library of lat-nam', ['4', 'U']).
card_cmc('library of lat-nam', 5).
card_layout('library of lat-nam', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('library of leng', 'Library of Leng').
card_type('library of leng', 'Artifact').
card_types('library of leng', ['Artifact']).
card_subtypes('library of leng', []).
card_colors('library of leng', []).
card_text('library of leng', 'You have no maximum hand size.\nIf an effect causes you to discard a card, discard it, but you may put it on top of your library instead of into your graveyard.').
card_mana_cost('library of leng', ['1']).
card_cmc('library of leng', 1).
card_layout('library of leng', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, ME4
card_name('lich', 'Lich').
card_type('lich', 'Enchantment').
card_types('lich', ['Enchantment']).
card_subtypes('lich', []).
card_colors('lich', ['B']).
card_text('lich', 'As Lich enters the battlefield, you lose life equal to your life total.\nYou don\'t lose the game for having 0 or less life.\nIf you would gain life, draw that many cards instead.\nWhenever you\'re dealt damage, sacrifice that many nontoken permanents. If you can\'t, you lose the game.\nWhen Lich is put into a graveyard from the battlefield, you lose the game.').
card_mana_cost('lich', ['B', 'B', 'B', 'B']).
card_cmc('lich', 4).
card_layout('lich', 'normal').
card_reserved('lich').

% Found in: ARB
card_name('lich lord of unx', 'Lich Lord of Unx').
card_type('lich lord of unx', 'Creature — Zombie Wizard').
card_types('lich lord of unx', ['Creature']).
card_subtypes('lich lord of unx', ['Zombie', 'Wizard']).
card_colors('lich lord of unx', ['U', 'B']).
card_text('lich lord of unx', '{U}{B}, {T}: Put a 1/1 blue and black Zombie Wizard creature token onto the battlefield.\n{U}{U}{B}{B}: Target player loses X life and puts the top X cards of his or her library into his or her graveyard, where X is the number of Zombies you control.').
card_mana_cost('lich lord of unx', ['1', 'U', 'B']).
card_cmc('lich lord of unx', 3).
card_layout('lich lord of unx', 'normal').
card_power('lich lord of unx', 2).
card_toughness('lich lord of unx', 2).

% Found in: ALA
card_name('lich\'s mirror', 'Lich\'s Mirror').
card_type('lich\'s mirror', 'Artifact').
card_types('lich\'s mirror', ['Artifact']).
card_subtypes('lich\'s mirror', []).
card_colors('lich\'s mirror', []).
card_text('lich\'s mirror', 'If you would lose the game, instead shuffle your hand, your graveyard, and all permanents you own into your library, then draw seven cards and your life total becomes 20.').
card_mana_cost('lich\'s mirror', ['5']).
card_cmc('lich\'s mirror', 5).
card_layout('lich\'s mirror', 'normal').

% Found in: DST
card_name('lich\'s tomb', 'Lich\'s Tomb').
card_type('lich\'s tomb', 'Artifact').
card_types('lich\'s tomb', ['Artifact']).
card_subtypes('lich\'s tomb', []).
card_colors('lich\'s tomb', []).
card_text('lich\'s tomb', 'You don\'t lose the game for having 0 or less life.\nWhenever you lose life, sacrifice a permanent for each 1 life you lost. (Damage causes loss of life.)').
card_mana_cost('lich\'s tomb', ['4']).
card_cmc('lich\'s tomb', 4).
card_layout('lich\'s tomb', 'normal').

% Found in: VIS
card_name('lichenthrope', 'Lichenthrope').
card_type('lichenthrope', 'Creature — Plant Fungus').
card_types('lichenthrope', ['Creature']).
card_subtypes('lichenthrope', ['Plant', 'Fungus']).
card_colors('lichenthrope', ['G']).
card_text('lichenthrope', 'If damage would be dealt to Lichenthrope, put that many -1/-1 counters on it instead.\nAt the beginning of your upkeep, remove a -1/-1 counter from Lichenthrope.').
card_mana_cost('lichenthrope', ['3', 'G', 'G']).
card_cmc('lichenthrope', 5).
card_layout('lichenthrope', 'normal').
card_power('lichenthrope', 5).
card_toughness('lichenthrope', 5).
card_reserved('lichenthrope').

% Found in: LGN
card_name('liege of the axe', 'Liege of the Axe').
card_type('liege of the axe', 'Creature — Human Soldier').
card_types('liege of the axe', ['Creature']).
card_subtypes('liege of the axe', ['Human', 'Soldier']).
card_colors('liege of the axe', ['W']).
card_text('liege of the axe', 'Vigilance\nMorph {1}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Liege of the Axe is turned face up, untap it.').
card_mana_cost('liege of the axe', ['3', 'W']).
card_cmc('liege of the axe', 4).
card_layout('liege of the axe', 'normal').
card_power('liege of the axe', 2).
card_toughness('liege of the axe', 3).

% Found in: WTH
card_name('liege of the hollows', 'Liege of the Hollows').
card_type('liege of the hollows', 'Creature — Spirit').
card_types('liege of the hollows', ['Creature']).
card_subtypes('liege of the hollows', ['Spirit']).
card_colors('liege of the hollows', ['G']).
card_text('liege of the hollows', 'When Liege of the Hollows dies, each player may pay any amount of mana. Then each player who paid mana this way puts that many 1/1 green Squirrel creature tokens onto the battlefield.').
card_mana_cost('liege of the hollows', ['2', 'G', 'G']).
card_cmc('liege of the hollows', 4).
card_layout('liege of the hollows', 'normal').
card_power('liege of the hollows', 3).
card_toughness('liege of the hollows', 4).
card_reserved('liege of the hollows').

% Found in: TSP
card_name('liege of the pit', 'Liege of the Pit').
card_type('liege of the pit', 'Creature — Demon').
card_types('liege of the pit', ['Creature']).
card_subtypes('liege of the pit', ['Demon']).
card_colors('liege of the pit', ['B']).
card_text('liege of the pit', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Liege of the Pit. If you can\'t, Liege of the Pit deals 7 damage to you.\nMorph {B}{B}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('liege of the pit', ['5', 'B', 'B', 'B']).
card_cmc('liege of the pit', 8).
card_layout('liege of the pit', 'normal').
card_power('liege of the pit', 7).
card_toughness('liege of the pit', 7).

% Found in: SOM
card_name('liege of the tangle', 'Liege of the Tangle').
card_type('liege of the tangle', 'Creature — Elemental').
card_types('liege of the tangle', ['Creature']).
card_subtypes('liege of the tangle', ['Elemental']).
card_colors('liege of the tangle', ['G']).
card_text('liege of the tangle', 'Trample\nWhenever Liege of the Tangle deals combat damage to a player, you may choose any number of target lands you control and put an awakening counter on each of them. Each of those lands is an 8/8 green Elemental creature for as long as it has an awakening counter on it. They\'re still lands.').
card_mana_cost('liege of the tangle', ['6', 'G', 'G']).
card_cmc('liege of the tangle', 8).
card_layout('liege of the tangle', 'normal').
card_power('liege of the tangle', 8).
card_toughness('liege of the tangle', 8).

% Found in: ODY
card_name('lieutenant kirtar', 'Lieutenant Kirtar').
card_type('lieutenant kirtar', 'Legendary Creature — Bird Soldier').
card_types('lieutenant kirtar', ['Creature']).
card_subtypes('lieutenant kirtar', ['Bird', 'Soldier']).
card_supertypes('lieutenant kirtar', ['Legendary']).
card_colors('lieutenant kirtar', ['W']).
card_text('lieutenant kirtar', 'Flying\n{1}{W}, Sacrifice Lieutenant Kirtar: Exile target attacking creature.').
card_mana_cost('lieutenant kirtar', ['1', 'W', 'W']).
card_cmc('lieutenant kirtar', 3).
card_layout('lieutenant kirtar', 'normal').
card_power('lieutenant kirtar', 2).
card_toughness('lieutenant kirtar', 2).

% Found in: APC, DDJ, pFNM
card_name('life', 'Life').
card_type('life', 'Sorcery').
card_types('life', ['Sorcery']).
card_subtypes('life', []).
card_colors('life', ['G']).
card_text('life', 'All lands you control become 1/1 creatures until end of turn. They\'re still lands.').
card_mana_cost('life', ['G']).
card_cmc('life', 1).
card_layout('life', 'split').
card_sides('life', 'death').

% Found in: PLC
card_name('life and limb', 'Life and Limb').
card_type('life and limb', 'Enchantment').
card_types('life and limb', ['Enchantment']).
card_subtypes('life and limb', []).
card_colors('life and limb', ['G']).
card_text('life and limb', 'All Forests and all Saprolings are 1/1 green Saproling creatures and Forest lands in addition to their other types.').
card_mana_cost('life and limb', ['3', 'G']).
card_cmc('life and limb', 4).
card_layout('life and limb', 'normal').

% Found in: ODY
card_name('life burst', 'Life Burst').
card_type('life burst', 'Instant').
card_types('life burst', ['Instant']).
card_subtypes('life burst', []).
card_colors('life burst', ['W']).
card_text('life burst', 'Target player gains 4 life, then gains 4 life for each card named Life Burst in each graveyard.').
card_mana_cost('life burst', ['1', 'W']).
card_cmc('life burst', 2).
card_layout('life burst', 'normal').

% Found in: LEG, ME3
card_name('life chisel', 'Life Chisel').
card_type('life chisel', 'Artifact').
card_types('life chisel', ['Artifact']).
card_subtypes('life chisel', []).
card_colors('life chisel', []).
card_text('life chisel', 'Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness. Activate this ability only during your upkeep.').
card_mana_cost('life chisel', ['4']).
card_cmc('life chisel', 4).
card_layout('life chisel', 'normal').

% Found in: DDJ, MMA, RAV
card_name('life from the loam', 'Life from the Loam').
card_type('life from the loam', 'Sorcery').
card_types('life from the loam', ['Sorcery']).
card_subtypes('life from the loam', []).
card_colors('life from the loam', ['G']).
card_text('life from the loam', 'Return up to three target land cards from your graveyard to your hand.\nDredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('life from the loam', ['1', 'G']).
card_cmc('life from the loam', 2).
card_layout('life from the loam', 'normal').

% Found in: LEG
card_name('life matrix', 'Life Matrix').
card_type('life matrix', 'Artifact').
card_types('life matrix', ['Artifact']).
card_subtypes('life matrix', []).
card_colors('life matrix', []).
card_text('life matrix', '{4}, {T}: Put a matrix counter on target creature and that creature gains \"Remove a matrix counter from this creature: Regenerate this creature.\" Activate this ability only during your upkeep.').
card_mana_cost('life matrix', ['4']).
card_cmc('life matrix', 4).
card_layout('life matrix', 'normal').
card_reserved('life matrix').

% Found in: NPH
card_name('life\'s finale', 'Life\'s Finale').
card_type('life\'s finale', 'Sorcery').
card_types('life\'s finale', ['Sorcery']).
card_subtypes('life\'s finale', []).
card_colors('life\'s finale', ['B']).
card_text('life\'s finale', 'Destroy all creatures, then search target opponent\'s library for up to three creature cards and put them into his or her graveyard. Then that player shuffles his or her library.').
card_mana_cost('life\'s finale', ['4', 'B', 'B']).
card_cmc('life\'s finale', 6).
card_layout('life\'s finale', 'normal').

% Found in: M15
card_name('life\'s legacy', 'Life\'s Legacy').
card_type('life\'s legacy', 'Sorcery').
card_types('life\'s legacy', ['Sorcery']).
card_subtypes('life\'s legacy', []).
card_colors('life\'s legacy', ['G']).
card_text('life\'s legacy', 'As an additional cost to cast Life\'s Legacy, sacrifice a creature.\nDraw cards equal to the sacrificed creature\'s power.').
card_mana_cost('life\'s legacy', ['1', 'G']).
card_cmc('life\'s legacy', 2).
card_layout('life\'s legacy', 'normal').

% Found in: M14
card_name('lifebane zombie', 'Lifebane Zombie').
card_type('lifebane zombie', 'Creature — Zombie Warrior').
card_types('lifebane zombie', ['Creature']).
card_subtypes('lifebane zombie', ['Zombie', 'Warrior']).
card_colors('lifebane zombie', ['B']).
card_text('lifebane zombie', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhen Lifebane Zombie enters the battlefield, target opponent reveals his or her hand. You choose a green or white creature card from it and exile that card.').
card_mana_cost('lifebane zombie', ['1', 'B', 'B']).
card_cmc('lifebane zombie', 3).
card_layout('lifebane zombie', 'normal').
card_power('lifebane zombie', 3).
card_toughness('lifebane zombie', 1).

% Found in: LEG
card_name('lifeblood', 'Lifeblood').
card_type('lifeblood', 'Enchantment').
card_types('lifeblood', ['Enchantment']).
card_subtypes('lifeblood', []).
card_colors('lifeblood', ['W']).
card_text('lifeblood', 'Whenever a Mountain an opponent controls becomes tapped, you gain 1 life.').
card_mana_cost('lifeblood', ['2', 'W', 'W']).
card_cmc('lifeblood', 4).
card_layout('lifeblood', 'normal').
card_reserved('lifeblood').

% Found in: C14
card_name('lifeblood hydra', 'Lifeblood Hydra').
card_type('lifeblood hydra', 'Creature — Hydra').
card_types('lifeblood hydra', ['Creature']).
card_subtypes('lifeblood hydra', ['Hydra']).
card_colors('lifeblood hydra', ['G']).
card_text('lifeblood hydra', 'Trample\nLifeblood Hydra enters the battlefield with X +1/+1 counters on it.\nWhen Lifeblood Hydra dies, you gain life and draw cards equal to its power.').
card_mana_cost('lifeblood hydra', ['X', 'G', 'G', 'G']).
card_cmc('lifeblood hydra', 3).
card_layout('lifeblood hydra', 'normal').
card_power('lifeblood hydra', 0).
card_toughness('lifeblood hydra', 0).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('lifeforce', 'Lifeforce').
card_type('lifeforce', 'Enchantment').
card_types('lifeforce', ['Enchantment']).
card_subtypes('lifeforce', []).
card_colors('lifeforce', ['G']).
card_text('lifeforce', '{G}{G}: Counter target black spell.').
card_mana_cost('lifeforce', ['G', 'G']).
card_cmc('lifeforce', 2).
card_layout('lifeforce', 'normal').

% Found in: BOK
card_name('lifegift', 'Lifegift').
card_type('lifegift', 'Enchantment').
card_types('lifegift', ['Enchantment']).
card_subtypes('lifegift', []).
card_colors('lifegift', ['G']).
card_text('lifegift', 'Whenever a land enters the battlefield, you may gain 1 life.').
card_mana_cost('lifegift', ['2', 'G']).
card_cmc('lifegift', 3).
card_layout('lifegift', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('lifelace', 'Lifelace').
card_type('lifelace', 'Instant').
card_types('lifelace', ['Instant']).
card_subtypes('lifelace', []).
card_colors('lifelace', ['G']).
card_text('lifelace', 'Target spell or permanent becomes green. (Mana symbols on that permanent remain unchanged.)').
card_mana_cost('lifelace', ['G']).
card_cmc('lifelace', 1).
card_layout('lifelace', 'normal').

% Found in: USG
card_name('lifeline', 'Lifeline').
card_type('lifeline', 'Artifact').
card_types('lifeline', ['Artifact']).
card_subtypes('lifeline', []).
card_colors('lifeline', []).
card_text('lifeline', 'Whenever a creature dies, if another creature is on the battlefield, return the first card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('lifeline', ['5']).
card_cmc('lifeline', 5).
card_layout('lifeline', 'normal').
card_reserved('lifeline').

% Found in: M10, M12
card_name('lifelink', 'Lifelink').
card_type('lifelink', 'Enchantment — Aura').
card_types('lifelink', ['Enchantment']).
card_subtypes('lifelink', ['Aura']).
card_colors('lifelink', ['W']).
card_text('lifelink', 'Enchant creature\nEnchanted creature has lifelink. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('lifelink', ['W']).
card_cmc('lifelink', 1).
card_layout('lifelink', 'normal').

% Found in: SOM
card_name('lifesmith', 'Lifesmith').
card_type('lifesmith', 'Creature — Human Artificer').
card_types('lifesmith', ['Creature']).
card_subtypes('lifesmith', ['Human', 'Artificer']).
card_colors('lifesmith', ['G']).
card_text('lifesmith', 'Whenever you cast an artifact spell, you may pay {1}. If you do, you gain 3 life.').
card_mana_cost('lifesmith', ['1', 'G']).
card_cmc('lifesmith', 2).
card_layout('lifesmith', 'normal').
card_power('lifesmith', 2).
card_toughness('lifesmith', 1).

% Found in: MRD
card_name('lifespark spellbomb', 'Lifespark Spellbomb').
card_type('lifespark spellbomb', 'Artifact').
card_types('lifespark spellbomb', ['Artifact']).
card_subtypes('lifespark spellbomb', []).
card_colors('lifespark spellbomb', []).
card_text('lifespark spellbomb', '{G}, Sacrifice Lifespark Spellbomb: Until end of turn, target land becomes a 3/3 creature that\'s still a land.\n{1}, Sacrifice Lifespark Spellbomb: Draw a card.').
card_mana_cost('lifespark spellbomb', ['1']).
card_cmc('lifespark spellbomb', 1).
card_layout('lifespark spellbomb', 'normal').

% Found in: BOK
card_name('lifespinner', 'Lifespinner').
card_type('lifespinner', 'Creature — Spirit').
card_types('lifespinner', ['Creature']).
card_subtypes('lifespinner', ['Spirit']).
card_colors('lifespinner', ['G']).
card_text('lifespinner', '{T}, Sacrifice three Spirits: Search your library for a legendary Spirit permanent card and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('lifespinner', ['3', 'G']).
card_cmc('lifespinner', 4).
card_layout('lifespinner', 'normal').
card_power('lifespinner', 3).
card_toughness('lifespinner', 3).

% Found in: BFZ
card_name('lifespring druid', 'Lifespring Druid').
card_type('lifespring druid', 'Creature — Elf Druid').
card_types('lifespring druid', ['Creature']).
card_subtypes('lifespring druid', ['Elf', 'Druid']).
card_colors('lifespring druid', ['G']).
card_text('lifespring druid', '{T}: Add one mana of any color to your mana pool.').
card_mana_cost('lifespring druid', ['2', 'G']).
card_cmc('lifespring druid', 3).
card_layout('lifespring druid', 'normal').
card_power('lifespring druid', 2).
card_toughness('lifespring druid', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('lifetap', 'Lifetap').
card_type('lifetap', 'Enchantment').
card_types('lifetap', ['Enchantment']).
card_subtypes('lifetap', []).
card_colors('lifetap', ['U']).
card_text('lifetap', 'Whenever a Forest an opponent controls becomes tapped, you gain 1 life.').
card_mana_cost('lifetap', ['U', 'U']).
card_cmc('lifetap', 2).
card_layout('lifetap', 'normal').

% Found in: CHK
card_name('lifted by clouds', 'Lifted by Clouds').
card_type('lifted by clouds', 'Instant — Arcane').
card_types('lifted by clouds', ['Instant']).
card_subtypes('lifted by clouds', ['Arcane']).
card_colors('lifted by clouds', ['U']).
card_text('lifted by clouds', 'Target creature gains flying until end of turn.\nSplice onto Arcane {1}{U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('lifted by clouds', ['2', 'U']).
card_cmc('lifted by clouds', 3).
card_layout('lifted by clouds', 'normal').

% Found in: EVE
card_name('light from within', 'Light from Within').
card_type('light from within', 'Enchantment').
card_types('light from within', ['Enchantment']).
card_subtypes('light from within', []).
card_colors('light from within', ['W']).
card_text('light from within', 'Chroma — Each creature you control gets +1/+1 for each white mana symbol in its mana cost.').
card_mana_cost('light from within', ['2', 'W', 'W']).
card_cmc('light from within', 4).
card_layout('light from within', 'normal').

% Found in: 6ED, TMP
card_name('light of day', 'Light of Day').
card_type('light of day', 'Enchantment').
card_types('light of day', ['Enchantment']).
card_subtypes('light of day', []).
card_colors('light of day', ['W']).
card_text('light of day', 'Black creatures can\'t attack or block.').
card_mana_cost('light of day', ['3', 'W']).
card_cmc('light of day', 4).
card_layout('light of day', 'normal').

% Found in: RAV
card_name('light of sanction', 'Light of Sanction').
card_type('light of sanction', 'Enchantment').
card_types('light of sanction', ['Enchantment']).
card_subtypes('light of sanction', []).
card_colors('light of sanction', ['W']).
card_text('light of sanction', 'Prevent all damage that would be dealt to creatures you control by sources you control.').
card_mana_cost('light of sanction', ['1', 'W', 'W']).
card_cmc('light of sanction', 3).
card_layout('light of sanction', 'normal').

% Found in: NMS
card_name('lightbringer', 'Lightbringer').
card_type('lightbringer', 'Creature — Kor Rebel').
card_types('lightbringer', ['Creature']).
card_subtypes('lightbringer', ['Kor', 'Rebel']).
card_colors('lightbringer', ['W']).
card_text('lightbringer', '{T}, Sacrifice Lightbringer: Exile target black creature.').
card_mana_cost('lightbringer', ['2', 'W']).
card_cmc('lightbringer', 3).
card_layout('lightbringer', 'normal').
card_power('lightbringer', 2).
card_toughness('lightbringer', 2).

% Found in: FRF
card_name('lightform', 'Lightform').
card_type('lightform', 'Enchantment').
card_types('lightform', ['Enchantment']).
card_subtypes('lightform', []).
card_colors('lightform', ['W']).
card_text('lightform', 'When Lightform enters the battlefield, it becomes an Aura with enchant creature. Manifest the top card of your library and attach Lightform to it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nEnchanted creature has flying and lifelink.').
card_mana_cost('lightform', ['1', 'W', 'W']).
card_cmc('lightform', 3).
card_layout('lightform', 'normal').

% Found in: ROE
card_name('lighthouse chronologist', 'Lighthouse Chronologist').
card_type('lighthouse chronologist', 'Creature — Human Wizard').
card_types('lighthouse chronologist', ['Creature']).
card_subtypes('lighthouse chronologist', ['Human', 'Wizard']).
card_colors('lighthouse chronologist', ['U']).
card_text('lighthouse chronologist', 'Level up {U} ({U}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 4-6\n2/4\nLEVEL 7+\n3/5\nAt the beginning of each end step, if it\'s not your turn, take an extra turn after this one.').
card_mana_cost('lighthouse chronologist', ['1', 'U']).
card_cmc('lighthouse chronologist', 2).
card_layout('lighthouse chronologist', 'leveler').
card_power('lighthouse chronologist', 1).
card_toughness('lighthouse chronologist', 3).

% Found in: CMD, WWK
card_name('lightkeeper of emeria', 'Lightkeeper of Emeria').
card_type('lightkeeper of emeria', 'Creature — Angel').
card_types('lightkeeper of emeria', ['Creature']).
card_subtypes('lightkeeper of emeria', ['Angel']).
card_colors('lightkeeper of emeria', ['W']).
card_text('lightkeeper of emeria', 'Multikicker {W} (You may pay an additional {W} any number of times as you cast this spell.)\nFlying\nWhen Lightkeeper of Emeria enters the battlefield, you gain 2 life for each time it was kicked.').
card_mana_cost('lightkeeper of emeria', ['3', 'W']).
card_cmc('lightkeeper of emeria', 4).
card_layout('lightkeeper of emeria', 'normal').
card_power('lightkeeper of emeria', 2).
card_toughness('lightkeeper of emeria', 4).

% Found in: ROE
card_name('lightmine field', 'Lightmine Field').
card_type('lightmine field', 'Enchantment').
card_types('lightmine field', ['Enchantment']).
card_subtypes('lightmine field', []).
card_colors('lightmine field', ['W']).
card_text('lightmine field', 'Whenever one or more creatures attack, Lightmine Field deals damage to each of those creatures equal to the number of attacking creatures.').
card_mana_cost('lightmine field', ['2', 'W', 'W']).
card_cmc('lightmine field', 4).
card_layout('lightmine field', 'normal').

% Found in: APC, DDN, TSB, V15
card_name('lightning angel', 'Lightning Angel').
card_type('lightning angel', 'Creature — Angel').
card_types('lightning angel', ['Creature']).
card_subtypes('lightning angel', ['Angel']).
card_colors('lightning angel', ['W', 'U', 'R']).
card_text('lightning angel', 'Flying, vigilance, haste').
card_mana_cost('lightning angel', ['1', 'R', 'W', 'U']).
card_cmc('lightning angel', 4).
card_layout('lightning angel', 'normal').
card_power('lightning angel', 3).
card_toughness('lightning angel', 4).

% Found in: TSP
card_name('lightning axe', 'Lightning Axe').
card_type('lightning axe', 'Instant').
card_types('lightning axe', ['Instant']).
card_subtypes('lightning axe', []).
card_colors('lightning axe', ['R']).
card_text('lightning axe', 'As an additional cost to cast Lightning Axe, discard a card or pay {5}.\nLightning Axe deals 5 damage to target creature.').
card_mana_cost('lightning axe', ['R']).
card_cmc('lightning axe', 1).
card_layout('lightning axe', 'normal').

% Found in: DTK
card_name('lightning berserker', 'Lightning Berserker').
card_type('lightning berserker', 'Creature — Human Berserker').
card_types('lightning berserker', ['Creature']).
card_subtypes('lightning berserker', ['Human', 'Berserker']).
card_colors('lightning berserker', ['R']).
card_text('lightning berserker', '{R}: Lightning Berserker gets +1/+0 until end of turn.\nDash {R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('lightning berserker', ['R']).
card_cmc('lightning berserker', 1).
card_layout('lightning berserker', 'normal').
card_power('lightning berserker', 1).
card_toughness('lightning berserker', 1).

% Found in: 6ED, 7ED, 8ED, TMP, TPR
card_name('lightning blast', 'Lightning Blast').
card_type('lightning blast', 'Instant').
card_types('lightning blast', ['Instant']).
card_subtypes('lightning blast', []).
card_colors('lightning blast', ['R']).
card_text('lightning blast', 'Lightning Blast deals 4 damage to target creature or player.').
card_mana_cost('lightning blast', ['3', 'R']).
card_cmc('lightning blast', 4).
card_layout('lightning blast', 'normal').

% Found in: ICE, ME3
card_name('lightning blow', 'Lightning Blow').
card_type('lightning blow', 'Instant').
card_types('lightning blow', ['Instant']).
card_subtypes('lightning blow', []).
card_colors('lightning blow', ['W']).
card_text('lightning blow', 'Target creature gains first strike until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('lightning blow', ['1', 'W']).
card_cmc('lightning blow', 2).
card_layout('lightning blow', 'normal').
card_reserved('lightning blow').

% Found in: 2ED, 3ED, 4ED, ATH, BTD, CED, CEI, LEA, LEB, M10, M11, MED, MM2, PD2, pJGP, pMPR
card_name('lightning bolt', 'Lightning Bolt').
card_type('lightning bolt', 'Instant').
card_types('lightning bolt', ['Instant']).
card_subtypes('lightning bolt', []).
card_colors('lightning bolt', ['R']).
card_text('lightning bolt', 'Lightning Bolt deals 3 damage to target creature or player.').
card_mana_cost('lightning bolt', ['R']).
card_cmc('lightning bolt', 1).
card_layout('lightning bolt', 'normal').

% Found in: VIS
card_name('lightning cloud', 'Lightning Cloud').
card_type('lightning cloud', 'Enchantment').
card_types('lightning cloud', ['Enchantment']).
card_subtypes('lightning cloud', []).
card_colors('lightning cloud', ['R']).
card_text('lightning cloud', 'Whenever a player casts a red spell, you may pay {R}. If you do, Lightning Cloud deals 1 damage to target creature or player.').
card_mana_cost('lightning cloud', ['3', 'R']).
card_cmc('lightning cloud', 4).
card_layout('lightning cloud', 'normal').
card_reserved('lightning cloud').

% Found in: MRD
card_name('lightning coils', 'Lightning Coils').
card_type('lightning coils', 'Artifact').
card_types('lightning coils', ['Artifact']).
card_subtypes('lightning coils', []).
card_colors('lightning coils', []).
card_text('lightning coils', 'Whenever a nontoken creature you control dies, put a charge counter on Lightning Coils.\nAt the beginning of your upkeep, if Lightning Coils has five or more charge counters on it, remove all of them from it and put that many 3/1 red Elemental creature tokens with haste onto the battlefield. Exile them at the beginning of the next end step.').
card_mana_cost('lightning coils', ['3']).
card_cmc('lightning coils', 3).
card_layout('lightning coils', 'normal').

% Found in: MOR
card_name('lightning crafter', 'Lightning Crafter').
card_type('lightning crafter', 'Creature — Goblin Shaman').
card_types('lightning crafter', ['Creature']).
card_subtypes('lightning crafter', ['Goblin', 'Shaman']).
card_colors('lightning crafter', ['R']).
card_text('lightning crafter', 'Champion a Goblin or Shaman (When this enters the battlefield, sacrifice it unless you exile another Goblin or Shaman you control. When this leaves the battlefield, that card returns to the battlefield.)\n{T}: Lightning Crafter deals 3 damage to target creature or player.').
card_mana_cost('lightning crafter', ['3', 'R']).
card_cmc('lightning crafter', 4).
card_layout('lightning crafter', 'normal').
card_power('lightning crafter', 3).
card_toughness('lightning crafter', 3).

% Found in: INV
card_name('lightning dart', 'Lightning Dart').
card_type('lightning dart', 'Instant').
card_types('lightning dart', ['Instant']).
card_subtypes('lightning dart', []).
card_colors('lightning dart', ['R']).
card_text('lightning dart', 'Lightning Dart deals 1 damage to target creature. If that creature is white or blue, Lightning Dart deals 4 damage to it instead.').
card_mana_cost('lightning dart', ['1', 'R']).
card_cmc('lightning dart', 2).
card_layout('lightning dart', 'normal').

% Found in: JOU
card_name('lightning diadem', 'Lightning Diadem').
card_type('lightning diadem', 'Enchantment — Aura').
card_types('lightning diadem', ['Enchantment']).
card_subtypes('lightning diadem', ['Aura']).
card_colors('lightning diadem', ['R']).
card_text('lightning diadem', 'Enchant creature\nWhen Lightning Diadem enters the battlefield, it deals 2 damage to target creature or player.\nEnchanted creature gets +2/+2.').
card_mana_cost('lightning diadem', ['5', 'R']).
card_cmc('lightning diadem', 6).
card_layout('lightning diadem', 'normal').

% Found in: USG, VMA, pPRE
card_name('lightning dragon', 'Lightning Dragon').
card_type('lightning dragon', 'Creature — Dragon').
card_types('lightning dragon', ['Creature']).
card_subtypes('lightning dragon', ['Dragon']).
card_colors('lightning dragon', ['R']).
card_text('lightning dragon', 'Flying\nEcho {2}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{R}: Lightning Dragon gets +1/+0 until end of turn.').
card_mana_cost('lightning dragon', ['2', 'R', 'R']).
card_cmc('lightning dragon', 4).
card_layout('lightning dragon', 'normal').
card_power('lightning dragon', 4).
card_toughness('lightning dragon', 4).
card_reserved('lightning dragon').

% Found in: 10E, 7ED, 8ED, 9ED, BRB, DPA, M10, M12, TMP
card_name('lightning elemental', 'Lightning Elemental').
card_type('lightning elemental', 'Creature — Elemental').
card_types('lightning elemental', ['Creature']).
card_subtypes('lightning elemental', ['Elemental']).
card_colors('lightning elemental', ['R']).
card_text('lightning elemental', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_mana_cost('lightning elemental', ['3', 'R']).
card_cmc('lightning elemental', 4).
card_layout('lightning elemental', 'normal').
card_power('lightning elemental', 4).
card_toughness('lightning elemental', 1).

% Found in: ARC, CMD, DDE, MRD, pFNM
card_name('lightning greaves', 'Lightning Greaves').
card_type('lightning greaves', 'Artifact — Equipment').
card_types('lightning greaves', ['Artifact']).
card_subtypes('lightning greaves', ['Equipment']).
card_colors('lightning greaves', []).
card_text('lightning greaves', 'Equipped creature has haste and shroud. (It can\'t be the target of spells or abilities.)\nEquip {0}').
card_mana_cost('lightning greaves', ['2']).
card_cmc('lightning greaves', 2).
card_layout('lightning greaves', 'normal').

% Found in: DDH, DDN, HOP, MMA, RAV, pMPR
card_name('lightning helix', 'Lightning Helix').
card_type('lightning helix', 'Instant').
card_types('lightning helix', ['Instant']).
card_subtypes('lightning helix', []).
card_colors('lightning helix', ['W', 'R']).
card_text('lightning helix', 'Lightning Helix deals 3 damage to target creature or player and you gain 3 life.').
card_mana_cost('lightning helix', ['R', 'W']).
card_cmc('lightning helix', 2).
card_layout('lightning helix', 'normal').

% Found in: MMQ, pMEI
card_name('lightning hounds', 'Lightning Hounds').
card_type('lightning hounds', 'Creature — Hound').
card_types('lightning hounds', ['Creature']).
card_subtypes('lightning hounds', ['Hound']).
card_colors('lightning hounds', ['R']).
card_text('lightning hounds', 'First strike').
card_mana_cost('lightning hounds', ['2', 'R', 'R']).
card_cmc('lightning hounds', 4).
card_layout('lightning hounds', 'normal').
card_power('lightning hounds', 3).
card_toughness('lightning hounds', 2).

% Found in: ORI
card_name('lightning javelin', 'Lightning Javelin').
card_type('lightning javelin', 'Sorcery').
card_types('lightning javelin', ['Sorcery']).
card_subtypes('lightning javelin', []).
card_colors('lightning javelin', ['R']).
card_text('lightning javelin', 'Lightning Javelin deals 3 damage to target creature or player. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('lightning javelin', ['3', 'R']).
card_cmc('lightning javelin', 4).
card_layout('lightning javelin', 'normal').

% Found in: AVR
card_name('lightning mauler', 'Lightning Mauler').
card_type('lightning mauler', 'Creature — Human Berserker').
card_types('lightning mauler', ['Creature']).
card_subtypes('lightning mauler', ['Human', 'Berserker']).
card_colors('lightning mauler', ['R']).
card_text('lightning mauler', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Lightning Mauler is paired with another creature, both creatures have haste.').
card_mana_cost('lightning mauler', ['1', 'R']).
card_cmc('lightning mauler', 2).
card_layout('lightning mauler', 'normal').
card_power('lightning mauler', 2).
card_toughness('lightning mauler', 1).

% Found in: AVR
card_name('lightning prowess', 'Lightning Prowess').
card_type('lightning prowess', 'Enchantment — Aura').
card_types('lightning prowess', ['Enchantment']).
card_subtypes('lightning prowess', ['Aura']).
card_colors('lightning prowess', ['R']).
card_text('lightning prowess', 'Enchant creature\nEnchanted creature has haste and \"{T}: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('lightning prowess', ['2', 'R']).
card_cmc('lightning prowess', 3).
card_layout('lightning prowess', 'normal').

% Found in: ARB
card_name('lightning reaver', 'Lightning Reaver').
card_type('lightning reaver', 'Creature — Zombie Beast').
card_types('lightning reaver', ['Creature']).
card_subtypes('lightning reaver', ['Zombie', 'Beast']).
card_colors('lightning reaver', ['B', 'R']).
card_text('lightning reaver', 'Haste; fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever Lightning Reaver deals combat damage to a player, put a charge counter on it.\nAt the beginning of your end step, Lightning Reaver deals damage equal to the number of charge counters on it to each opponent.').
card_mana_cost('lightning reaver', ['3', 'B', 'R']).
card_cmc('lightning reaver', 5).
card_layout('lightning reaver', 'normal').
card_power('lightning reaver', 3).
card_toughness('lightning reaver', 3).

% Found in: MIR
card_name('lightning reflexes', 'Lightning Reflexes').
card_type('lightning reflexes', 'Enchantment — Aura').
card_types('lightning reflexes', ['Enchantment']).
card_subtypes('lightning reflexes', ['Aura']).
card_colors('lightning reflexes', ['R']).
card_text('lightning reflexes', 'You may cast Lightning Reflexes as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nEnchant creature\nEnchanted creature gets +1/+0 and has first strike.').
card_mana_cost('lightning reflexes', ['1', 'R']).
card_cmc('lightning reflexes', 2).
card_layout('lightning reflexes', 'normal').

% Found in: ONS, VMA, pFNM
card_name('lightning rift', 'Lightning Rift').
card_type('lightning rift', 'Enchantment').
card_types('lightning rift', ['Enchantment']).
card_subtypes('lightning rift', []).
card_colors('lightning rift', ['R']).
card_text('lightning rift', 'Whenever a player cycles a card, you may pay {1}. If you do, Lightning Rift deals 2 damage to target creature or player.').
card_mana_cost('lightning rift', ['1', 'R']).
card_cmc('lightning rift', 2).
card_layout('lightning rift', 'normal').

% Found in: CSP
card_name('lightning serpent', 'Lightning Serpent').
card_type('lightning serpent', 'Creature — Elemental Serpent').
card_types('lightning serpent', ['Creature']).
card_subtypes('lightning serpent', ['Elemental', 'Serpent']).
card_colors('lightning serpent', ['R']).
card_text('lightning serpent', 'Trample, haste\nLightning Serpent enters the battlefield with X +1/+0 counters on it.\nAt the beginning of the end step, sacrifice Lightning Serpent.').
card_mana_cost('lightning serpent', ['X', 'R']).
card_cmc('lightning serpent', 1).
card_layout('lightning serpent', 'normal').
card_power('lightning serpent', 2).
card_toughness('lightning serpent', 1).

% Found in: FRF
card_name('lightning shrieker', 'Lightning Shrieker').
card_type('lightning shrieker', 'Creature — Dragon').
card_types('lightning shrieker', ['Creature']).
card_subtypes('lightning shrieker', ['Dragon']).
card_colors('lightning shrieker', ['R']).
card_text('lightning shrieker', 'Flying, trample, haste\nAt the beginning of the end step, Lightning Shrieker\'s owner shuffles it into his or her library.').
card_mana_cost('lightning shrieker', ['4', 'R']).
card_cmc('lightning shrieker', 5).
card_layout('lightning shrieker', 'normal').
card_power('lightning shrieker', 5).
card_toughness('lightning shrieker', 5).

% Found in: CSP
card_name('lightning storm', 'Lightning Storm').
card_type('lightning storm', 'Instant').
card_types('lightning storm', ['Instant']).
card_subtypes('lightning storm', []).
card_colors('lightning storm', ['R']).
card_text('lightning storm', 'Lightning Storm deals X damage to target creature or player, where X is 3 plus the number of charge counters on it.\nDiscard a land card: Put two charge counters on Lightning Storm. You may choose a new target for it. Any player may activate this ability but only if Lightning Storm is on the stack.').
card_mana_cost('lightning storm', ['1', 'R', 'R']).
card_cmc('lightning storm', 3).
card_layout('lightning storm', 'normal').

% Found in: M15, THS
card_name('lightning strike', 'Lightning Strike').
card_type('lightning strike', 'Instant').
card_types('lightning strike', ['Instant']).
card_subtypes('lightning strike', []).
card_colors('lightning strike', ['R']).
card_text('lightning strike', 'Lightning Strike deals 3 damage to target creature or player.').
card_mana_cost('lightning strike', ['1', 'R']).
card_cmc('lightning strike', 2).
card_layout('lightning strike', 'normal').

% Found in: JUD
card_name('lightning surge', 'Lightning Surge').
card_type('lightning surge', 'Sorcery').
card_types('lightning surge', ['Sorcery']).
card_subtypes('lightning surge', []).
card_colors('lightning surge', ['R']).
card_text('lightning surge', 'Lightning Surge deals 4 damage to target creature or player.\nThreshold — If seven or more cards are in your graveyard, instead Lightning Surge deals 6 damage to that creature or player and the damage can\'t be prevented.\nFlashback {5}{R}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('lightning surge', ['3', 'R', 'R']).
card_cmc('lightning surge', 5).
card_layout('lightning surge', 'normal').

% Found in: ALA, M14
card_name('lightning talons', 'Lightning Talons').
card_type('lightning talons', 'Enchantment — Aura').
card_types('lightning talons', ['Enchantment']).
card_subtypes('lightning talons', ['Aura']).
card_colors('lightning talons', ['R']).
card_text('lightning talons', 'Enchant creature\nEnchanted creature gets +3/+0 and has first strike. (It deals combat damage before creatures without first strike.)').
card_mana_cost('lightning talons', ['2', 'R']).
card_cmc('lightning talons', 3).
card_layout('lightning talons', 'normal').

% Found in: BNG
card_name('lightning volley', 'Lightning Volley').
card_type('lightning volley', 'Instant').
card_types('lightning volley', ['Instant']).
card_subtypes('lightning volley', []).
card_colors('lightning volley', ['R']).
card_text('lightning volley', 'Until end of turn, creatures you control gain \"{T}: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('lightning volley', ['3', 'R']).
card_cmc('lightning volley', 4).
card_layout('lightning volley', 'normal').

% Found in: DTK
card_name('lightwalker', 'Lightwalker').
card_type('lightwalker', 'Creature — Human Warrior').
card_types('lightwalker', ['Creature']).
card_subtypes('lightwalker', ['Human', 'Warrior']).
card_colors('lightwalker', ['W']).
card_text('lightwalker', 'Lightwalker has flying as long as it has a +1/+1 counter on it.').
card_mana_cost('lightwalker', ['1', 'W']).
card_cmc('lightwalker', 2).
card_layout('lightwalker', 'normal').
card_power('lightwalker', 2).
card_toughness('lightwalker', 1).

% Found in: M10
card_name('lightwielder paladin', 'Lightwielder Paladin').
card_type('lightwielder paladin', 'Creature — Human Knight').
card_types('lightwielder paladin', ['Creature']).
card_subtypes('lightwielder paladin', ['Human', 'Knight']).
card_colors('lightwielder paladin', ['W']).
card_text('lightwielder paladin', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhenever Lightwielder Paladin deals combat damage to a player, you may exile target black or red permanent that player controls.').
card_mana_cost('lightwielder paladin', ['3', 'W', 'W']).
card_cmc('lightwielder paladin', 5).
card_layout('lightwielder paladin', 'normal').
card_power('lightwielder paladin', 4).
card_toughness('lightwielder paladin', 4).

% Found in: DD3_GVL, DDD, LRW
card_name('lignify', 'Lignify').
card_type('lignify', 'Tribal Enchantment — Treefolk Aura').
card_types('lignify', ['Tribal', 'Enchantment']).
card_subtypes('lignify', ['Treefolk', 'Aura']).
card_colors('lignify', ['G']).
card_text('lignify', 'Enchant creature\nEnchanted creature is a Treefolk with base power and toughness 0/4 and loses all abilities.').
card_mana_cost('lignify', ['1', 'G']).
card_cmc('lignify', 2).
card_layout('lignify', 'normal').

% Found in: M13, M14, pMEI
card_name('liliana of the dark realms', 'Liliana of the Dark Realms').
card_type('liliana of the dark realms', 'Planeswalker — Liliana').
card_types('liliana of the dark realms', ['Planeswalker']).
card_subtypes('liliana of the dark realms', ['Liliana']).
card_colors('liliana of the dark realms', ['B']).
card_text('liliana of the dark realms', '+1: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.\n−3: Target creature gets +X/+X or -X/-X until end of turn, where X is the number of Swamps you control.\n−6: You get an emblem with \"Swamps you control have ‘{T}: Add {B}{B}{B}{B} to your mana pool.\'\"').
card_mana_cost('liliana of the dark realms', ['2', 'B', 'B']).
card_cmc('liliana of the dark realms', 4).
card_layout('liliana of the dark realms', 'normal').
card_loyalty('liliana of the dark realms', 3).

% Found in: ISD
card_name('liliana of the veil', 'Liliana of the Veil').
card_type('liliana of the veil', 'Planeswalker — Liliana').
card_types('liliana of the veil', ['Planeswalker']).
card_subtypes('liliana of the veil', ['Liliana']).
card_colors('liliana of the veil', ['B']).
card_text('liliana of the veil', '+1: Each player discards a card.\n−2: Target player sacrifices a creature.\n−6: Separate all permanents target player controls into two piles. That player sacrifices all permanents in the pile of his or her choice.').
card_mana_cost('liliana of the veil', ['1', 'B', 'B']).
card_cmc('liliana of the veil', 3).
card_layout('liliana of the veil', 'normal').
card_loyalty('liliana of the veil', 3).

% Found in: DD3_GVL, DDD, LRW, M10, M11, M15, pMEI
card_name('liliana vess', 'Liliana Vess').
card_type('liliana vess', 'Planeswalker — Liliana').
card_types('liliana vess', ['Planeswalker']).
card_subtypes('liliana vess', ['Liliana']).
card_colors('liliana vess', ['B']).
card_text('liliana vess', '+1: Target player discards a card.\n−2: Search your library for a card, then shuffle your library and put that card on top of it.\n−8: Put all creature cards from all graveyards onto the battlefield under your control.').
card_mana_cost('liliana vess', ['3', 'B', 'B']).
card_cmc('liliana vess', 5).
card_layout('liliana vess', 'normal').
card_loyalty('liliana vess', 5).

% Found in: M11
card_name('liliana\'s caress', 'Liliana\'s Caress').
card_type('liliana\'s caress', 'Enchantment').
card_types('liliana\'s caress', ['Enchantment']).
card_subtypes('liliana\'s caress', []).
card_colors('liliana\'s caress', ['B']).
card_text('liliana\'s caress', 'Whenever an opponent discards a card, that player loses 2 life.').
card_mana_cost('liliana\'s caress', ['1', 'B']).
card_cmc('liliana\'s caress', 2).
card_layout('liliana\'s caress', 'normal').

% Found in: C14, M14
card_name('liliana\'s reaver', 'Liliana\'s Reaver').
card_type('liliana\'s reaver', 'Creature — Zombie').
card_types('liliana\'s reaver', ['Creature']).
card_subtypes('liliana\'s reaver', ['Zombie']).
card_colors('liliana\'s reaver', ['B']).
card_text('liliana\'s reaver', 'Deathtouch\nWhenever Liliana\'s Reaver deals combat damage to a player, that player discards a card and you put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_mana_cost('liliana\'s reaver', ['2', 'B', 'B']).
card_cmc('liliana\'s reaver', 4).
card_layout('liliana\'s reaver', 'normal').
card_power('liliana\'s reaver', 4).
card_toughness('liliana\'s reaver', 3).

% Found in: M13
card_name('liliana\'s shade', 'Liliana\'s Shade').
card_type('liliana\'s shade', 'Creature — Shade').
card_types('liliana\'s shade', ['Creature']).
card_subtypes('liliana\'s shade', ['Shade']).
card_colors('liliana\'s shade', ['B']).
card_text('liliana\'s shade', 'When Liliana\'s Shade enters the battlefield, you may search your library for a Swamp card, reveal it, put it into your hand, then shuffle your library.\n{B}: Liliana\'s Shade gets +1/+1 until end of turn.').
card_mana_cost('liliana\'s shade', ['2', 'B', 'B']).
card_cmc('liliana\'s shade', 4).
card_layout('liliana\'s shade', 'normal').
card_power('liliana\'s shade', 1).
card_toughness('liliana\'s shade', 1).

% Found in: CNS, M11, PC2, pMGD
card_name('liliana\'s specter', 'Liliana\'s Specter').
card_type('liliana\'s specter', 'Creature — Specter').
card_types('liliana\'s specter', ['Creature']).
card_subtypes('liliana\'s specter', ['Specter']).
card_colors('liliana\'s specter', ['B']).
card_text('liliana\'s specter', 'Flying\nWhen Liliana\'s Specter enters the battlefield, each opponent discards a card.').
card_mana_cost('liliana\'s specter', ['1', 'B', 'B']).
card_cmc('liliana\'s specter', 3).
card_layout('liliana\'s specter', 'normal').
card_power('liliana\'s specter', 2).
card_toughness('liliana\'s specter', 1).

% Found in: ORI
card_name('liliana, defiant necromancer', 'Liliana, Defiant Necromancer').
card_type('liliana, defiant necromancer', 'Planeswalker — Liliana').
card_types('liliana, defiant necromancer', ['Planeswalker']).
card_subtypes('liliana, defiant necromancer', ['Liliana']).
card_colors('liliana, defiant necromancer', ['B']).
card_text('liliana, defiant necromancer', '+2: Each player discards a card.\n−X: Return target nonlegendary creature card with converted mana cost X from your graveyard to the battlefield.\n−8: You get an emblem with \"Whenever a creature dies, return it to the battlefield under your control at the beginning of the next end step.\"').
card_layout('liliana, defiant necromancer', 'double-faced').
card_loyalty('liliana, defiant necromancer', 3).

% Found in: ORI
card_name('liliana, heretical healer', 'Liliana, Heretical Healer').
card_type('liliana, heretical healer', 'Legendary Creature — Human Cleric').
card_types('liliana, heretical healer', ['Creature']).
card_subtypes('liliana, heretical healer', ['Human', 'Cleric']).
card_supertypes('liliana, heretical healer', ['Legendary']).
card_colors('liliana, heretical healer', ['B']).
card_text('liliana, heretical healer', 'Lifelink\nWhenever another nontoken creature you control dies, exile Liliana, Heretical Healer, then return her to the battlefield transformed under her owner\'s control. If you do, put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('liliana, heretical healer', ['1', 'B', 'B']).
card_cmc('liliana, heretical healer', 3).
card_layout('liliana, heretical healer', 'double-faced').
card_power('liliana, heretical healer', 2).
card_toughness('liliana, heretical healer', 3).
card_sides('liliana, heretical healer', 'liliana, defiant necromancer').

% Found in: USG
card_name('lilting refrain', 'Lilting Refrain').
card_type('lilting refrain', 'Enchantment').
card_types('lilting refrain', ['Enchantment']).
card_subtypes('lilting refrain', []).
card_colors('lilting refrain', ['U']).
card_text('lilting refrain', 'At the beginning of your upkeep, you may put a verse counter on Lilting Refrain.\nSacrifice Lilting Refrain: Counter target spell unless its controller pays {X}, where X is the number of verse counters on Lilting Refrain.').
card_mana_cost('lilting refrain', ['1', 'U']).
card_cmc('lilting refrain', 2).
card_layout('lilting refrain', 'normal').

% Found in: TSP
card_name('lim-dûl the necromancer', 'Lim-Dûl the Necromancer').
card_type('lim-dûl the necromancer', 'Legendary Creature — Human Wizard').
card_types('lim-dûl the necromancer', ['Creature']).
card_subtypes('lim-dûl the necromancer', ['Human', 'Wizard']).
card_supertypes('lim-dûl the necromancer', ['Legendary']).
card_colors('lim-dûl the necromancer', ['B']).
card_text('lim-dûl the necromancer', 'Whenever a creature an opponent controls dies, you may pay {1}{B}. If you do, return that card to the battlefield under your control. If it\'s a creature, it\'s a Zombie in addition to its other creature types.\n{1}{B}: Regenerate target Zombie.').
card_mana_cost('lim-dûl the necromancer', ['5', 'B', 'B']).
card_cmc('lim-dûl the necromancer', 7).
card_layout('lim-dûl the necromancer', 'normal').
card_power('lim-dûl the necromancer', 4).
card_toughness('lim-dûl the necromancer', 4).

% Found in: ICE, ME4
card_name('lim-dûl\'s cohort', 'Lim-Dûl\'s Cohort').
card_type('lim-dûl\'s cohort', 'Creature — Zombie').
card_types('lim-dûl\'s cohort', ['Creature']).
card_subtypes('lim-dûl\'s cohort', ['Zombie']).
card_colors('lim-dûl\'s cohort', ['B']).
card_text('lim-dûl\'s cohort', 'Whenever Lim-Dûl\'s Cohort blocks or becomes blocked by a creature, that creature can\'t be regenerated this turn.').
card_mana_cost('lim-dûl\'s cohort', ['1', 'B', 'B']).
card_cmc('lim-dûl\'s cohort', 3).
card_layout('lim-dûl\'s cohort', 'normal').
card_power('lim-dûl\'s cohort', 2).
card_toughness('lim-dûl\'s cohort', 3).

% Found in: ICE
card_name('lim-dûl\'s hex', 'Lim-Dûl\'s Hex').
card_type('lim-dûl\'s hex', 'Enchantment').
card_types('lim-dûl\'s hex', ['Enchantment']).
card_subtypes('lim-dûl\'s hex', []).
card_colors('lim-dûl\'s hex', ['B']).
card_text('lim-dûl\'s hex', 'At the beginning of your upkeep, for each player, Lim-Dûl\'s Hex deals 1 damage to that player unless he or she pays {B} or {3}.').
card_mana_cost('lim-dûl\'s hex', ['1', 'B']).
card_cmc('lim-dûl\'s hex', 2).
card_layout('lim-dûl\'s hex', 'normal').

% Found in: ALL, DKM, ME2
card_name('lim-dûl\'s high guard', 'Lim-Dûl\'s High Guard').
card_type('lim-dûl\'s high guard', 'Creature — Skeleton').
card_types('lim-dûl\'s high guard', ['Creature']).
card_subtypes('lim-dûl\'s high guard', ['Skeleton']).
card_colors('lim-dûl\'s high guard', ['B']).
card_text('lim-dûl\'s high guard', 'First strike\n{1}{B}: Regenerate Lim-Dûl\'s High Guard.').
card_mana_cost('lim-dûl\'s high guard', ['1', 'B', 'B']).
card_cmc('lim-dûl\'s high guard', 3).
card_layout('lim-dûl\'s high guard', 'normal').
card_power('lim-dûl\'s high guard', 2).
card_toughness('lim-dûl\'s high guard', 1).

% Found in: ALL
card_name('lim-dûl\'s paladin', 'Lim-Dûl\'s Paladin').
card_type('lim-dûl\'s paladin', 'Creature — Human Knight').
card_types('lim-dûl\'s paladin', ['Creature']).
card_subtypes('lim-dûl\'s paladin', ['Human', 'Knight']).
card_colors('lim-dûl\'s paladin', ['B', 'R']).
card_text('lim-dûl\'s paladin', 'Trample\nAt the beginning of your upkeep, you may discard a card. If you don\'t, sacrifice Lim-Dûl\'s Paladin and draw a card.\nWhenever Lim-Dûl\'s Paladin becomes blocked, it gets +6/+3 until end of turn.\nWhenever Lim-Dûl\'s Paladin attacks and isn\'t blocked, it assigns no combat damage to defending player this turn and that player loses 4 life.').
card_mana_cost('lim-dûl\'s paladin', ['2', 'B', 'R']).
card_cmc('lim-dûl\'s paladin', 4).
card_layout('lim-dûl\'s paladin', 'normal').
card_power('lim-dûl\'s paladin', 0).
card_toughness('lim-dûl\'s paladin', 3).

% Found in: ALL, C13, MED
card_name('lim-dûl\'s vault', 'Lim-Dûl\'s Vault').
card_type('lim-dûl\'s vault', 'Instant').
card_types('lim-dûl\'s vault', ['Instant']).
card_subtypes('lim-dûl\'s vault', []).
card_colors('lim-dûl\'s vault', ['U', 'B']).
card_text('lim-dûl\'s vault', 'Look at the top five cards of your library. As many times as you choose, you may pay 1 life, put those cards on the bottom of your library in any order, then look at the top five cards of your library. Then shuffle your library and put the last cards you looked at this way on top of it in any order.').
card_mana_cost('lim-dûl\'s vault', ['U', 'B']).
card_cmc('lim-dûl\'s vault', 2).
card_layout('lim-dûl\'s vault', 'normal').

% Found in: ODY
card_name('limestone golem', 'Limestone Golem').
card_type('limestone golem', 'Artifact Creature — Golem').
card_types('limestone golem', ['Artifact', 'Creature']).
card_subtypes('limestone golem', ['Golem']).
card_colors('limestone golem', []).
card_text('limestone golem', '{2}, Sacrifice Limestone Golem: Target player draws a card.').
card_mana_cost('limestone golem', ['6']).
card_cmc('limestone golem', 6).
card_layout('limestone golem', 'normal').
card_power('limestone golem', 3).
card_toughness('limestone golem', 4).

% Found in: EXO
card_name('limited resources', 'Limited Resources').
card_type('limited resources', 'Enchantment').
card_types('limited resources', ['Enchantment']).
card_subtypes('limited resources', []).
card_colors('limited resources', ['W']).
card_text('limited resources', 'When Limited Resources enters the battlefield, each player chooses five lands he or she controls and sacrifices the rest.\nPlayers can\'t play lands as long as ten or more lands are on the battlefield.').
card_mana_cost('limited resources', ['W']).
card_cmc('limited resources', 1).
card_layout('limited resources', 'normal').

% Found in: NMS
card_name('lin sivvi, defiant hero', 'Lin Sivvi, Defiant Hero').
card_type('lin sivvi, defiant hero', 'Legendary Creature — Human Rebel').
card_types('lin sivvi, defiant hero', ['Creature']).
card_subtypes('lin sivvi, defiant hero', ['Human', 'Rebel']).
card_supertypes('lin sivvi, defiant hero', ['Legendary']).
card_colors('lin sivvi, defiant hero', ['W']).
card_text('lin sivvi, defiant hero', '{X}, {T}: Search your library for a Rebel permanent card with converted mana cost X or less and put it onto the battlefield. Then shuffle your library.\n{3}: Put target Rebel card from your graveyard on the bottom of your library.').
card_mana_cost('lin sivvi, defiant hero', ['1', 'W', 'W']).
card_cmc('lin sivvi, defiant hero', 3).
card_layout('lin sivvi, defiant hero', 'normal').
card_power('lin sivvi, defiant hero', 1).
card_toughness('lin sivvi, defiant hero', 3).

% Found in: FUT
card_name('linessa, zephyr mage', 'Linessa, Zephyr Mage').
card_type('linessa, zephyr mage', 'Legendary Creature — Human Wizard').
card_types('linessa, zephyr mage', ['Creature']).
card_subtypes('linessa, zephyr mage', ['Human', 'Wizard']).
card_supertypes('linessa, zephyr mage', ['Legendary']).
card_colors('linessa, zephyr mage', ['U']).
card_text('linessa, zephyr mage', '{X}{U}{U}, {T}: Return target creature with converted mana cost X to its owner\'s hand.\nGrandeur — Discard another card named Linessa, Zephyr Mage: Target player returns a creature he or she controls to its owner\'s hand, then repeats this process for an artifact, an enchantment, and a land.').
card_mana_cost('linessa, zephyr mage', ['3', 'U']).
card_cmc('linessa, zephyr mage', 4).
card_layout('linessa, zephyr mage', 'normal').
card_power('linessa, zephyr mage', 3).
card_toughness('linessa, zephyr mage', 3).

% Found in: SCG
card_name('lingering death', 'Lingering Death').
card_type('lingering death', 'Enchantment — Aura').
card_types('lingering death', ['Enchantment']).
card_subtypes('lingering death', ['Aura']).
card_colors('lingering death', ['B']).
card_text('lingering death', 'Enchant creature\nAt the beginning of the end step of enchanted creature\'s controller, that player sacrifices that creature.').
card_mana_cost('lingering death', ['1', 'B']).
card_cmc('lingering death', 2).
card_layout('lingering death', 'normal').

% Found in: USG
card_name('lingering mirage', 'Lingering Mirage').
card_type('lingering mirage', 'Enchantment — Aura').
card_types('lingering mirage', ['Enchantment']).
card_subtypes('lingering mirage', ['Aura']).
card_colors('lingering mirage', ['U']).
card_text('lingering mirage', 'Enchant land\nEnchanted land is an Island.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('lingering mirage', ['1', 'U']).
card_cmc('lingering mirage', 2).
card_layout('lingering mirage', 'normal').

% Found in: DDK, DKA, MD1, pFNM
card_name('lingering souls', 'Lingering Souls').
card_type('lingering souls', 'Sorcery').
card_types('lingering souls', ['Sorcery']).
card_subtypes('lingering souls', []).
card_colors('lingering souls', ['W']).
card_text('lingering souls', 'Put two 1/1 white Spirit creature tokens with flying onto the battlefield.\nFlashback {1}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('lingering souls', ['2', 'W']).
card_cmc('lingering souls', 3).
card_layout('lingering souls', 'normal').

% Found in: EVE
card_name('lingering tormentor', 'Lingering Tormentor').
card_type('lingering tormentor', 'Creature — Spirit').
card_types('lingering tormentor', ['Creature']).
card_subtypes('lingering tormentor', ['Spirit']).
card_colors('lingering tormentor', ['B']).
card_text('lingering tormentor', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('lingering tormentor', ['3', 'B']).
card_cmc('lingering tormentor', 4).
card_layout('lingering tormentor', 'normal').
card_power('lingering tormentor', 2).
card_toughness('lingering tormentor', 2).

% Found in: ROE
card_name('linvala, keeper of silence', 'Linvala, Keeper of Silence').
card_type('linvala, keeper of silence', 'Legendary Creature — Angel').
card_types('linvala, keeper of silence', ['Creature']).
card_subtypes('linvala, keeper of silence', ['Angel']).
card_supertypes('linvala, keeper of silence', ['Legendary']).
card_colors('linvala, keeper of silence', ['W']).
card_text('linvala, keeper of silence', 'Flying\nActivated abilities of creatures your opponents control can\'t be activated.').
card_mana_cost('linvala, keeper of silence', ['2', 'W', 'W']).
card_cmc('linvala, keeper of silence', 4).
card_layout('linvala, keeper of silence', 'normal').
card_power('linvala, keeper of silence', 3).
card_toughness('linvala, keeper of silence', 4).

% Found in: MIR, VMA
card_name('lion\'s eye diamond', 'Lion\'s Eye Diamond').
card_type('lion\'s eye diamond', 'Artifact').
card_types('lion\'s eye diamond', ['Artifact']).
card_subtypes('lion\'s eye diamond', []).
card_colors('lion\'s eye diamond', []).
card_text('lion\'s eye diamond', 'Sacrifice Lion\'s Eye Diamond, Discard your hand: Add three mana of any one color to your mana pool. Activate this ability only any time you could cast an instant.').
card_mana_cost('lion\'s eye diamond', ['0']).
card_cmc('lion\'s eye diamond', 0).
card_layout('lion\'s eye diamond', 'normal').
card_reserved('lion\'s eye diamond').

% Found in: DDG, GPT
card_name('lionheart maverick', 'Lionheart Maverick').
card_type('lionheart maverick', 'Creature — Human Knight').
card_types('lionheart maverick', ['Creature']).
card_subtypes('lionheart maverick', ['Human', 'Knight']).
card_colors('lionheart maverick', ['W']).
card_text('lionheart maverick', 'Vigilance\n{4}{W}: Lionheart Maverick gets +1/+2 until end of turn.').
card_mana_cost('lionheart maverick', ['W']).
card_cmc('lionheart maverick', 1).
card_layout('lionheart maverick', 'normal').
card_power('lionheart maverick', 1).
card_toughness('lionheart maverick', 1).

% Found in: ODY
card_name('liquid fire', 'Liquid Fire').
card_type('liquid fire', 'Sorcery').
card_types('liquid fire', ['Sorcery']).
card_subtypes('liquid fire', []).
card_colors('liquid fire', ['R']).
card_text('liquid fire', 'As an additional cost to cast Liquid Fire, choose a number between 0 and 5.\nLiquid Fire deals X damage to target creature and 5 minus X damage to that creature\'s controller, where X is the chosen number.').
card_mana_cost('liquid fire', ['4', 'R', 'R']).
card_cmc('liquid fire', 6).
card_layout('liquid fire', 'normal').

% Found in: TOR
card_name('liquify', 'Liquify').
card_type('liquify', 'Instant').
card_types('liquify', ['Instant']).
card_subtypes('liquify', []).
card_colors('liquify', ['U']).
card_text('liquify', 'Counter target spell with converted mana cost 3 or less. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_mana_cost('liquify', ['2', 'U']).
card_cmc('liquify', 3).
card_layout('liquify', 'normal').

% Found in: C14, SOM
card_name('liquimetal coating', 'Liquimetal Coating').
card_type('liquimetal coating', 'Artifact').
card_types('liquimetal coating', ['Artifact']).
card_subtypes('liquimetal coating', []).
card_colors('liquimetal coating', []).
card_text('liquimetal coating', '{T}: Target permanent becomes an artifact in addition to its other types until end of turn.').
card_mana_cost('liquimetal coating', ['2']).
card_cmc('liquimetal coating', 2).
card_layout('liquimetal coating', 'normal').

% Found in: ODY
card_name('lithatog', 'Lithatog').
card_type('lithatog', 'Creature — Atog').
card_types('lithatog', ['Creature']).
card_subtypes('lithatog', ['Atog']).
card_colors('lithatog', ['R', 'G']).
card_text('lithatog', 'Sacrifice an artifact: Lithatog gets +1/+1 until end of turn.\nSacrifice a land: Lithatog gets +1/+1 until end of turn.').
card_mana_cost('lithatog', ['1', 'R', 'G']).
card_cmc('lithatog', 3).
card_layout('lithatog', 'normal').
card_power('lithatog', 1).
card_toughness('lithatog', 2).

% Found in: BFZ
card_name('lithomancer\'s focus', 'Lithomancer\'s Focus').
card_type('lithomancer\'s focus', 'Instant').
card_types('lithomancer\'s focus', ['Instant']).
card_subtypes('lithomancer\'s focus', []).
card_colors('lithomancer\'s focus', ['W']).
card_text('lithomancer\'s focus', 'Target creature gets +2/+2 until end of turn. Prevent all damage that would be dealt to that creature this turn by colorless sources.').
card_mana_cost('lithomancer\'s focus', ['W']).
card_cmc('lithomancer\'s focus', 1).
card_layout('lithomancer\'s focus', 'normal').

% Found in: DDI, MMQ
card_name('lithophage', 'Lithophage').
card_type('lithophage', 'Creature — Insect').
card_types('lithophage', ['Creature']).
card_subtypes('lithophage', ['Insect']).
card_colors('lithophage', ['R']).
card_text('lithophage', 'At the beginning of your upkeep, sacrifice Lithophage unless you sacrifice a Mountain.').
card_mana_cost('lithophage', ['3', 'R', 'R']).
card_cmc('lithophage', 5).
card_layout('lithophage', 'normal').
card_power('lithophage', 7).
card_toughness('lithophage', 7).

% Found in: UNH
card_name('little girl', 'Little Girl').
card_type('little girl', 'Creature — Human Child').
card_types('little girl', ['Creature']).
card_subtypes('little girl', ['Human', 'Child']).
card_colors('little girl', []).
card_text('little girl', '').
card_mana_cost('little girl', ['hw']).
card_cmc('little girl', 0.5).
card_layout('little girl', 'normal').
card_power('little girl', 0.5).
card_toughness('little girl', 0.5).

% Found in: M14
card_name('liturgy of blood', 'Liturgy of Blood').
card_type('liturgy of blood', 'Sorcery').
card_types('liturgy of blood', ['Sorcery']).
card_subtypes('liturgy of blood', []).
card_colors('liturgy of blood', ['B']).
card_text('liturgy of blood', 'Destroy target creature. Add {B}{B}{B} to your mana pool.').
card_mana_cost('liturgy of blood', ['3', 'B', 'B']).
card_cmc('liturgy of blood', 5).
card_layout('liturgy of blood', 'normal').

% Found in: ME3, PTK
card_name('liu bei, lord of shu', 'Liu Bei, Lord of Shu').
card_type('liu bei, lord of shu', 'Legendary Creature — Human Soldier').
card_types('liu bei, lord of shu', ['Creature']).
card_subtypes('liu bei, lord of shu', ['Human', 'Soldier']).
card_supertypes('liu bei, lord of shu', ['Legendary']).
card_colors('liu bei, lord of shu', ['W']).
card_text('liu bei, lord of shu', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nLiu Bei, Lord of Shu gets +2/+2 as long as you control a permanent named Guan Yu, Sainted Warrior or a permanent named Zhang Fei, Fierce Warrior.').
card_mana_cost('liu bei, lord of shu', ['3', 'W', 'W']).
card_cmc('liu bei, lord of shu', 5).
card_layout('liu bei, lord of shu', 'normal').
card_power('liu bei, lord of shu', 2).
card_toughness('liu bei, lord of shu', 4).

% Found in: SOM
card_name('livewire lash', 'Livewire Lash').
card_type('livewire lash', 'Artifact — Equipment').
card_types('livewire lash', ['Artifact']).
card_subtypes('livewire lash', ['Equipment']).
card_colors('livewire lash', []).
card_text('livewire lash', 'Equipped creature gets +2/+0 and has \"Whenever this creature becomes the target of a spell, this creature deals 2 damage to target creature or player.\"\nEquip {2}').
card_mana_cost('livewire lash', ['2']).
card_cmc('livewire lash', 2).
card_layout('livewire lash', 'normal').

% Found in: APC
card_name('living airship', 'Living Airship').
card_type('living airship', 'Creature — Metathran').
card_types('living airship', ['Creature']).
card_subtypes('living airship', ['Metathran']).
card_colors('living airship', ['U']).
card_text('living airship', 'Flying\n{2}{G}: Regenerate Living Airship.').
card_mana_cost('living airship', ['3', 'U']).
card_cmc('living airship', 4).
card_layout('living airship', 'normal').
card_power('living airship', 2).
card_toughness('living airship', 3).

% Found in: CHR, DRK
card_name('living armor', 'Living Armor').
card_type('living armor', 'Artifact').
card_types('living armor', ['Artifact']).
card_subtypes('living armor', []).
card_colors('living armor', []).
card_text('living armor', '{T}, Sacrifice Living Armor: Put X +0/+1 counters on target creature, where X is that creature\'s converted mana cost.').
card_mana_cost('living armor', ['4']).
card_cmc('living armor', 4).
card_layout('living armor', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('living artifact', 'Living Artifact').
card_type('living artifact', 'Enchantment — Aura').
card_types('living artifact', ['Enchantment']).
card_subtypes('living artifact', ['Aura']).
card_colors('living artifact', ['G']).
card_text('living artifact', 'Enchant artifact\nWhenever you\'re dealt damage, put that many vitality counters on Living Artifact.\nAt the beginning of your upkeep, you may remove a vitality counter from Living Artifact. If you do, you gain 1 life.').
card_mana_cost('living artifact', ['G']).
card_cmc('living artifact', 1).
card_layout('living artifact', 'normal').

% Found in: BRB, CMD, DDE, TMP, TPR, V14, VMA, pJGP
card_name('living death', 'Living Death').
card_type('living death', 'Sorcery').
card_types('living death', ['Sorcery']).
card_subtypes('living death', []).
card_colors('living death', ['B']).
card_text('living death', 'Each player exiles all creature cards from his or her graveyard, then sacrifices all creatures he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_mana_cost('living death', ['3', 'B', 'B']).
card_cmc('living death', 5).
card_layout('living death', 'normal').

% Found in: ROE
card_name('living destiny', 'Living Destiny').
card_type('living destiny', 'Instant').
card_types('living destiny', ['Instant']).
card_subtypes('living destiny', []).
card_colors('living destiny', ['G']).
card_text('living destiny', 'As an additional cost to cast Living Destiny, reveal a creature card from your hand.\nYou gain life equal to the revealed card\'s converted mana cost.').
card_mana_cost('living destiny', ['3', 'G']).
card_cmc('living destiny', 4).
card_layout('living destiny', 'normal').

% Found in: TSP
card_name('living end', 'Living End').
card_type('living end', 'Sorcery').
card_types('living end', ['Sorcery']).
card_subtypes('living end', []).
card_colors('living end', ['B']).
card_text('living end', 'Suspend 3—{2}{B}{B} (Rather than cast this card from your hand, pay {2}{B}{B} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\nEach player exiles all creature cards from his or her graveyard, then sacrifices all creatures he or she controls, then puts all cards he or she exiled this way onto the battlefield.').
card_layout('living end', 'normal').

% Found in: HOP, MRD
card_name('living hive', 'Living Hive').
card_type('living hive', 'Creature — Elemental Insect').
card_types('living hive', ['Creature']).
card_subtypes('living hive', ['Elemental', 'Insect']).
card_colors('living hive', ['G']).
card_text('living hive', 'Trample\nWhenever Living Hive deals combat damage to a player, put that many 1/1 green Insect creature tokens onto the battlefield.').
card_mana_cost('living hive', ['6', 'G', 'G']).
card_cmc('living hive', 8).
card_layout('living hive', 'normal').
card_power('living hive', 6).
card_toughness('living hive', 6).

% Found in: GPT
card_name('living inferno', 'Living Inferno').
card_type('living inferno', 'Creature — Elemental').
card_types('living inferno', ['Creature']).
card_subtypes('living inferno', ['Elemental']).
card_colors('living inferno', ['R']).
card_text('living inferno', '{T}: Living Inferno deals damage equal to its power divided as you choose among any number of target creatures. Each of those creatures deals damage equal to its power to Living Inferno.').
card_mana_cost('living inferno', ['6', 'R', 'R']).
card_cmc('living inferno', 8).
card_layout('living inferno', 'normal').
card_power('living inferno', 8).
card_toughness('living inferno', 5).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, ME4
card_name('living lands', 'Living Lands').
card_type('living lands', 'Enchantment').
card_types('living lands', ['Enchantment']).
card_subtypes('living lands', []).
card_colors('living lands', ['G']).
card_text('living lands', 'All Forests are 1/1 creatures that are still lands.').
card_mana_cost('living lands', ['3', 'G']).
card_cmc('living lands', 4).
card_layout('living lands', 'normal').

% Found in: DTK
card_name('living lore', 'Living Lore').
card_type('living lore', 'Creature — Avatar').
card_types('living lore', ['Creature']).
card_subtypes('living lore', ['Avatar']).
card_colors('living lore', ['U']).
card_text('living lore', 'As Living Lore enters the battlefield, exile an instant or sorcery card from your graveyard.\nLiving Lore\'s power and toughness are each equal to the exiled card\'s converted mana cost.\nWhenever Living Lore deals combat damage, you may sacrifice it. If you do, you may cast the exiled card without paying its mana cost.').
card_mana_cost('living lore', ['3', 'U']).
card_cmc('living lore', 4).
card_layout('living lore', 'normal').
card_power('living lore', '*').
card_toughness('living lore', '*').

% Found in: LEG, ME3
card_name('living plane', 'Living Plane').
card_type('living plane', 'World Enchantment').
card_types('living plane', ['Enchantment']).
card_subtypes('living plane', []).
card_supertypes('living plane', ['World']).
card_colors('living plane', ['G']).
card_text('living plane', 'All lands are 1/1 creatures that are still lands.').
card_mana_cost('living plane', ['2', 'G', 'G']).
card_cmc('living plane', 4).
card_layout('living plane', 'normal').
card_reserved('living plane').

% Found in: 8ED, PCY
card_name('living terrain', 'Living Terrain').
card_type('living terrain', 'Enchantment — Aura').
card_types('living terrain', ['Enchantment']).
card_subtypes('living terrain', ['Aura']).
card_colors('living terrain', ['G']).
card_text('living terrain', 'Enchant land\nEnchanted land is a 5/6 green Treefolk creature that\'s still a land.').
card_mana_cost('living terrain', ['2', 'G', 'G']).
card_cmc('living terrain', 4).
card_layout('living terrain', 'normal').

% Found in: M15
card_name('living totem', 'Living Totem').
card_type('living totem', 'Creature — Plant Elemental').
card_types('living totem', ['Creature']).
card_subtypes('living totem', ['Plant', 'Elemental']).
card_colors('living totem', ['G']).
card_text('living totem', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nWhen Living Totem enters the battlefield, you may put a +1/+1 counter on another target creature.').
card_mana_cost('living totem', ['3', 'G']).
card_cmc('living totem', 4).
card_layout('living totem', 'normal').
card_power('living totem', 2).
card_toughness('living totem', 3).

% Found in: ZEN
card_name('living tsunami', 'Living Tsunami').
card_type('living tsunami', 'Creature — Elemental').
card_types('living tsunami', ['Creature']).
card_subtypes('living tsunami', ['Elemental']).
card_colors('living tsunami', ['U']).
card_text('living tsunami', 'Flying\nAt the beginning of your upkeep, sacrifice Living Tsunami unless you return a land you control to its owner\'s hand.').
card_mana_cost('living tsunami', ['2', 'U', 'U']).
card_cmc('living tsunami', 4).
card_layout('living tsunami', 'normal').
card_power('living tsunami', 4).
card_toughness('living tsunami', 4).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('living wall', 'Living Wall').
card_type('living wall', 'Artifact Creature — Wall').
card_types('living wall', ['Artifact', 'Creature']).
card_subtypes('living wall', ['Wall']).
card_colors('living wall', []).
card_text('living wall', 'Defender (This creature can\'t attack.)\n{1}: Regenerate Living Wall.').
card_mana_cost('living wall', ['4']).
card_cmc('living wall', 4).
card_layout('living wall', 'normal').
card_power('living wall', 0).
card_toughness('living wall', 6).

% Found in: JUD, pJGP
card_name('living wish', 'Living Wish').
card_type('living wish', 'Sorcery').
card_types('living wish', ['Sorcery']).
card_subtypes('living wish', []).
card_colors('living wish', ['G']).
card_text('living wish', 'You may choose a creature or land card you own from outside the game, reveal that card, and put it into your hand. Exile Living Wish.').
card_mana_cost('living wish', ['1', 'G']).
card_cmc('living wish', 2).
card_layout('living wish', 'normal').

% Found in: LEG, ME3
card_name('livonya silone', 'Livonya Silone').
card_type('livonya silone', 'Legendary Creature — Human Warrior').
card_types('livonya silone', ['Creature']).
card_subtypes('livonya silone', ['Human', 'Warrior']).
card_supertypes('livonya silone', ['Legendary']).
card_colors('livonya silone', ['R', 'G']).
card_text('livonya silone', 'First strike; legendary landwalk (This creature can\'t be blocked as long as defending player controls a legendary land.)').
card_mana_cost('livonya silone', ['2', 'R', 'R', 'G', 'G']).
card_cmc('livonya silone', 6).
card_layout('livonya silone', 'normal').
card_power('livonya silone', 4).
card_toughness('livonya silone', 4).
card_reserved('livonya silone').

% Found in: CNS, POR
card_name('lizard warrior', 'Lizard Warrior').
card_type('lizard warrior', 'Creature — Lizard Warrior').
card_types('lizard warrior', ['Creature']).
card_subtypes('lizard warrior', ['Lizard', 'Warrior']).
card_colors('lizard warrior', ['R']).
card_text('lizard warrior', '').
card_mana_cost('lizard warrior', ['3', 'R']).
card_cmc('lizard warrior', 4).
card_layout('lizard warrior', 'normal').
card_power('lizard warrior', 4).
card_toughness('lizard warrior', 2).

% Found in: HOP
card_name('llanowar', 'Llanowar').
card_type('llanowar', 'Plane — Dominaria').
card_types('llanowar', ['Plane']).
card_subtypes('llanowar', ['Dominaria']).
card_colors('llanowar', []).
card_text('llanowar', 'All creatures have \"{T}: Add {G}{G} to your mana pool.\"\nWhenever you roll {C}, untap all creatures you control.').
card_layout('llanowar', 'plane').

% Found in: FUT
card_name('llanowar augur', 'Llanowar Augur').
card_type('llanowar augur', 'Creature — Elf Shaman').
card_types('llanowar augur', ['Creature']).
card_subtypes('llanowar augur', ['Elf', 'Shaman']).
card_colors('llanowar augur', ['G']).
card_text('llanowar augur', 'Sacrifice Llanowar Augur: Target creature gets +3/+3 and gains trample until end of turn. Activate this ability only during your upkeep.').
card_mana_cost('llanowar augur', ['G']).
card_cmc('llanowar augur', 1).
card_layout('llanowar augur', 'normal').
card_power('llanowar augur', 0).
card_toughness('llanowar augur', 3).

% Found in: 8ED, 9ED, WTH
card_name('llanowar behemoth', 'Llanowar Behemoth').
card_type('llanowar behemoth', 'Creature — Elemental').
card_types('llanowar behemoth', ['Creature']).
card_subtypes('llanowar behemoth', ['Elemental']).
card_colors('llanowar behemoth', ['G']).
card_text('llanowar behemoth', 'Tap an untapped creature you control: Llanowar Behemoth gets +1/+1 until end of turn.').
card_mana_cost('llanowar behemoth', ['3', 'G', 'G']).
card_cmc('llanowar behemoth', 5).
card_layout('llanowar behemoth', 'normal').
card_power('llanowar behemoth', 4).
card_toughness('llanowar behemoth', 4).

% Found in: INV
card_name('llanowar cavalry', 'Llanowar Cavalry').
card_type('llanowar cavalry', 'Creature — Human Soldier').
card_types('llanowar cavalry', ['Creature']).
card_subtypes('llanowar cavalry', ['Human', 'Soldier']).
card_colors('llanowar cavalry', ['G']).
card_text('llanowar cavalry', '{W}: Llanowar Cavalry gains vigilance until end of turn.').
card_mana_cost('llanowar cavalry', ['2', 'G']).
card_cmc('llanowar cavalry', 3).
card_layout('llanowar cavalry', 'normal').
card_power('llanowar cavalry', 1).
card_toughness('llanowar cavalry', 4).

% Found in: APC
card_name('llanowar dead', 'Llanowar Dead').
card_type('llanowar dead', 'Creature — Zombie Elf').
card_types('llanowar dead', ['Creature']).
card_subtypes('llanowar dead', ['Zombie', 'Elf']).
card_colors('llanowar dead', ['B', 'G']).
card_text('llanowar dead', '{T}: Add {B} to your mana pool.').
card_mana_cost('llanowar dead', ['B', 'G']).
card_cmc('llanowar dead', 2).
card_layout('llanowar dead', 'normal').
card_power('llanowar dead', 2).
card_toughness('llanowar dead', 2).

% Found in: WTH
card_name('llanowar druid', 'Llanowar Druid').
card_type('llanowar druid', 'Creature — Elf Druid').
card_types('llanowar druid', ['Creature']).
card_subtypes('llanowar druid', ['Elf', 'Druid']).
card_colors('llanowar druid', ['G']).
card_text('llanowar druid', '{T}, Sacrifice Llanowar Druid: Untap all Forests.').
card_mana_cost('llanowar druid', ['1', 'G']).
card_cmc('llanowar druid', 2).
card_layout('llanowar druid', 'normal').
card_power('llanowar druid', 1).
card_toughness('llanowar druid', 2).

% Found in: INV
card_name('llanowar elite', 'Llanowar Elite').
card_type('llanowar elite', 'Creature — Elf').
card_types('llanowar elite', ['Creature']).
card_subtypes('llanowar elite', ['Elf']).
card_colors('llanowar elite', ['G']).
card_text('llanowar elite', 'Kicker {8} (You may pay an additional {8} as you cast this spell.)\nTrample\nIf Llanowar Elite was kicked, it enters the battlefield with five +1/+1 counters on it.').
card_mana_cost('llanowar elite', ['G']).
card_cmc('llanowar elite', 1).
card_layout('llanowar elite', 'normal').
card_power('llanowar elite', 1).
card_toughness('llanowar elite', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 9ED, ATH, BRB, BTD, C14, CED, CEI, DD3_EVG, EVG, LEA, LEB, M10, M11, M12, S00, pFNM, pGTW
card_name('llanowar elves', 'Llanowar Elves').
card_type('llanowar elves', 'Creature — Elf Druid').
card_types('llanowar elves', ['Creature']).
card_subtypes('llanowar elves', ['Elf', 'Druid']).
card_colors('llanowar elves', ['G']).
card_text('llanowar elves', '{T}: Add {G} to your mana pool.').
card_mana_cost('llanowar elves', ['G']).
card_cmc('llanowar elves', 1).
card_layout('llanowar elves', 'normal').
card_power('llanowar elves', 1).
card_toughness('llanowar elves', 1).

% Found in: FUT, ORI
card_name('llanowar empath', 'Llanowar Empath').
card_type('llanowar empath', 'Creature — Elf Shaman').
card_types('llanowar empath', ['Creature']).
card_subtypes('llanowar empath', ['Elf', 'Shaman']).
card_colors('llanowar empath', ['G']).
card_text('llanowar empath', 'When Llanowar Empath enters the battlefield, scry 2, then reveal the top card of your library. If it\'s a creature card, put it into your hand. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('llanowar empath', ['3', 'G']).
card_cmc('llanowar empath', 4).
card_layout('llanowar empath', 'normal').
card_power('llanowar empath', 2).
card_toughness('llanowar empath', 2).

% Found in: INV
card_name('llanowar knight', 'Llanowar Knight').
card_type('llanowar knight', 'Creature — Elf Knight').
card_types('llanowar knight', ['Creature']).
card_subtypes('llanowar knight', ['Elf', 'Knight']).
card_colors('llanowar knight', ['W', 'G']).
card_text('llanowar knight', 'Protection from black').
card_mana_cost('llanowar knight', ['G', 'W']).
card_cmc('llanowar knight', 2).
card_layout('llanowar knight', 'normal').
card_power('llanowar knight', 2).
card_toughness('llanowar knight', 2).

% Found in: FUT
card_name('llanowar mentor', 'Llanowar Mentor').
card_type('llanowar mentor', 'Creature — Elf Spellshaper').
card_types('llanowar mentor', ['Creature']).
card_subtypes('llanowar mentor', ['Elf', 'Spellshaper']).
card_colors('llanowar mentor', ['G']).
card_text('llanowar mentor', '{G}, {T}, Discard a card: Put a 1/1 green Elf Druid creature token named Llanowar Elves onto the battlefield. It has \"{T}: Add {G} to your mana pool.\"').
card_mana_cost('llanowar mentor', ['G']).
card_cmc('llanowar mentor', 1).
card_layout('llanowar mentor', 'normal').
card_power('llanowar mentor', 1).
card_toughness('llanowar mentor', 1).

% Found in: ARC, C13, DDL, FUT
card_name('llanowar reborn', 'Llanowar Reborn').
card_type('llanowar reborn', 'Land').
card_types('llanowar reborn', ['Land']).
card_subtypes('llanowar reborn', []).
card_colors('llanowar reborn', []).
card_text('llanowar reborn', 'Llanowar Reborn enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nGraft 1 (This land enters the battlefield with a +1/+1 counter on it. Whenever a creature enters the battlefield, you may move a +1/+1 counter from this land onto it.)').
card_layout('llanowar reborn', 'normal').

% Found in: 10E, WTH
card_name('llanowar sentinel', 'Llanowar Sentinel').
card_type('llanowar sentinel', 'Creature — Elf').
card_types('llanowar sentinel', ['Creature']).
card_subtypes('llanowar sentinel', ['Elf']).
card_colors('llanowar sentinel', ['G']).
card_text('llanowar sentinel', 'When Llanowar Sentinel enters the battlefield, you may pay {1}{G}. If you do, search your library for a card named Llanowar Sentinel and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('llanowar sentinel', ['2', 'G']).
card_cmc('llanowar sentinel', 3).
card_layout('llanowar sentinel', 'normal').
card_power('llanowar sentinel', 2).
card_toughness('llanowar sentinel', 3).

% Found in: INV
card_name('llanowar vanguard', 'Llanowar Vanguard').
card_type('llanowar vanguard', 'Creature — Dryad').
card_types('llanowar vanguard', ['Creature']).
card_subtypes('llanowar vanguard', ['Dryad']).
card_colors('llanowar vanguard', ['G']).
card_text('llanowar vanguard', '{T}: Llanowar Vanguard gets +0/+4 until end of turn.').
card_mana_cost('llanowar vanguard', ['2', 'G']).
card_cmc('llanowar vanguard', 3).
card_layout('llanowar vanguard', 'normal').
card_power('llanowar vanguard', 1).
card_toughness('llanowar vanguard', 1).

% Found in: 10E, 9ED, APC, M15, ORI
card_name('llanowar wastes', 'Llanowar Wastes').
card_type('llanowar wastes', 'Land').
card_types('llanowar wastes', ['Land']).
card_subtypes('llanowar wastes', []).
card_colors('llanowar wastes', []).
card_text('llanowar wastes', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Llanowar Wastes deals 1 damage to you.').
card_layout('llanowar wastes', 'normal').

% Found in: TOR
card_name('llawan, cephalid empress', 'Llawan, Cephalid Empress').
card_type('llawan, cephalid empress', 'Legendary Creature — Cephalid').
card_types('llawan, cephalid empress', ['Creature']).
card_subtypes('llawan, cephalid empress', ['Cephalid']).
card_supertypes('llawan, cephalid empress', ['Legendary']).
card_colors('llawan, cephalid empress', ['U']).
card_text('llawan, cephalid empress', 'When Llawan, Cephalid Empress enters the battlefield, return all blue creatures your opponents control to their owners\' hands.\nYour opponents can\'t cast blue creature spells.').
card_mana_cost('llawan, cephalid empress', ['3', 'U']).
card_cmc('llawan, cephalid empress', 4).
card_layout('llawan, cephalid empress', 'normal').
card_power('llawan, cephalid empress', 2).
card_toughness('llawan, cephalid empress', 3).

% Found in: INV
card_name('loafing giant', 'Loafing Giant').
card_type('loafing giant', 'Creature — Giant').
card_types('loafing giant', ['Creature']).
card_subtypes('loafing giant', ['Giant']).
card_colors('loafing giant', ['R']).
card_text('loafing giant', 'Whenever Loafing Giant attacks or blocks, put the top card of your library into your graveyard. If that card is a land card, prevent all combat damage Loafing Giant would deal this turn.').
card_mana_cost('loafing giant', ['4', 'R']).
card_cmc('loafing giant', 5).
card_layout('loafing giant', 'normal').
card_power('loafing giant', 4).
card_toughness('loafing giant', 6).

% Found in: BOK
card_name('loam dweller', 'Loam Dweller').
card_type('loam dweller', 'Creature — Spirit').
card_types('loam dweller', ['Creature']).
card_subtypes('loam dweller', ['Spirit']).
card_colors('loam dweller', ['G']).
card_text('loam dweller', 'Whenever you cast a Spirit or Arcane spell, you may put a land card from your hand onto the battlefield tapped.').
card_mana_cost('loam dweller', ['1', 'G']).
card_cmc('loam dweller', 2).
card_layout('loam dweller', 'normal').
card_power('loam dweller', 2).
card_toughness('loam dweller', 2).

% Found in: DDH, WWK
card_name('loam lion', 'Loam Lion').
card_type('loam lion', 'Creature — Cat').
card_types('loam lion', ['Creature']).
card_subtypes('loam lion', ['Cat']).
card_colors('loam lion', ['W']).
card_text('loam lion', 'Loam Lion gets +1/+2 as long as you control a Forest.').
card_mana_cost('loam lion', ['W']).
card_cmc('loam lion', 1).
card_layout('loam lion', 'normal').
card_power('loam lion', 1).
card_toughness('loam lion', 1).

% Found in: SHM
card_name('loamdragger giant', 'Loamdragger Giant').
card_type('loamdragger giant', 'Creature — Giant Warrior').
card_types('loamdragger giant', ['Creature']).
card_subtypes('loamdragger giant', ['Giant', 'Warrior']).
card_colors('loamdragger giant', ['R', 'G']).
card_text('loamdragger giant', '').
card_mana_cost('loamdragger giant', ['4', 'R/G', 'R/G', 'R/G']).
card_cmc('loamdragger giant', 7).
card_layout('loamdragger giant', 'normal').
card_power('loamdragger giant', 7).
card_toughness('loamdragger giant', 6).

% Found in: DIS
card_name('loaming shaman', 'Loaming Shaman').
card_type('loaming shaman', 'Creature — Centaur Shaman').
card_types('loaming shaman', ['Creature']).
card_subtypes('loaming shaman', ['Centaur', 'Shaman']).
card_colors('loaming shaman', ['G']).
card_text('loaming shaman', 'When Loaming Shaman enters the battlefield, target player shuffles any number of target cards from his or her graveyard into his or her library.').
card_mana_cost('loaming shaman', ['2', 'G']).
card_cmc('loaming shaman', 3).
card_layout('loaming shaman', 'normal').
card_power('loaming shaman', 3).
card_toughness('loaming shaman', 2).

% Found in: THS
card_name('loathsome catoblepas', 'Loathsome Catoblepas').
card_type('loathsome catoblepas', 'Creature — Beast').
card_types('loathsome catoblepas', ['Creature']).
card_subtypes('loathsome catoblepas', ['Beast']).
card_colors('loathsome catoblepas', ['B']).
card_text('loathsome catoblepas', '{2}{G}: Loathsome Catoblepas must be blocked this turn if able.\nWhen Loathsome Catoblepas dies, target creature an opponent controls gets -3/-3 until end of turn.').
card_mana_cost('loathsome catoblepas', ['5', 'B']).
card_cmc('loathsome catoblepas', 6).
card_layout('loathsome catoblepas', 'normal').
card_power('loathsome catoblepas', 3).
card_toughness('loathsome catoblepas', 3).

% Found in: RTR
card_name('lobber crew', 'Lobber Crew').
card_type('lobber crew', 'Creature — Goblin Warrior').
card_types('lobber crew', ['Creature']).
card_subtypes('lobber crew', ['Goblin', 'Warrior']).
card_colors('lobber crew', ['R']).
card_text('lobber crew', 'Defender\n{T}: Lobber Crew deals 1 damage to each opponent.\nWhenever you cast a multicolored spell, untap Lobber Crew.').
card_mana_cost('lobber crew', ['2', 'R']).
card_cmc('lobber crew', 3).
card_layout('lobber crew', 'normal').
card_power('lobber crew', 0).
card_toughness('lobber crew', 4).

% Found in: INV, TMP, pFNM
card_name('lobotomy', 'Lobotomy').
card_type('lobotomy', 'Sorcery').
card_types('lobotomy', ['Sorcery']).
card_subtypes('lobotomy', []).
card_colors('lobotomy', ['U', 'B']).
card_text('lobotomy', 'Target player reveals his or her hand, then you choose a card other than a basic land card from it. Search that player\'s graveyard, hand, and library for all cards with the same name as the chosen card and exile them. Then that player shuffles his or her library.').
card_mana_cost('lobotomy', ['2', 'U', 'B']).
card_cmc('lobotomy', 4).
card_layout('lobotomy', 'normal').

% Found in: SHM
card_name('loch korrigan', 'Loch Korrigan').
card_type('loch korrigan', 'Creature — Spirit').
card_types('loch korrigan', ['Creature']).
card_subtypes('loch korrigan', ['Spirit']).
card_colors('loch korrigan', ['B']).
card_text('loch korrigan', '{U/B}: Loch Korrigan gets +1/+1 until end of turn.').
card_mana_cost('loch korrigan', ['3', 'B']).
card_cmc('loch korrigan', 4).
card_layout('loch korrigan', 'normal').
card_power('loch korrigan', 1).
card_toughness('loch korrigan', 1).

% Found in: TSP
card_name('locket of yesterdays', 'Locket of Yesterdays').
card_type('locket of yesterdays', 'Artifact').
card_types('locket of yesterdays', ['Artifact']).
card_subtypes('locket of yesterdays', []).
card_colors('locket of yesterdays', []).
card_text('locket of yesterdays', 'Spells you cast cost {1} less to cast for each card with the same name as that spell in your graveyard.').
card_mana_cost('locket of yesterdays', ['1']).
card_cmc('locket of yesterdays', 1).
card_layout('locket of yesterdays', 'normal').

% Found in: SHM
card_name('lockjaw snapper', 'Lockjaw Snapper').
card_type('lockjaw snapper', 'Artifact Creature — Scarecrow').
card_types('lockjaw snapper', ['Artifact', 'Creature']).
card_subtypes('lockjaw snapper', ['Scarecrow']).
card_colors('lockjaw snapper', []).
card_text('lockjaw snapper', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\nWhen Lockjaw Snapper dies, put a -1/-1 counter on each creature with a -1/-1 counter on it.').
card_mana_cost('lockjaw snapper', ['4']).
card_cmc('lockjaw snapper', 4).
card_layout('lockjaw snapper', 'normal').
card_power('lockjaw snapper', 2).
card_toughness('lockjaw snapper', 2).

% Found in: SOK
card_name('locust miser', 'Locust Miser').
card_type('locust miser', 'Creature — Rat Shaman').
card_types('locust miser', ['Creature']).
card_subtypes('locust miser', ['Rat', 'Shaman']).
card_colors('locust miser', ['B']).
card_text('locust miser', 'Each opponent\'s maximum hand size is reduced by two.').
card_mana_cost('locust miser', ['2', 'B', 'B']).
card_cmc('locust miser', 4).
card_layout('locust miser', 'normal').
card_power('locust miser', 2).
card_toughness('locust miser', 2).

% Found in: MIR
card_name('locust swarm', 'Locust Swarm').
card_type('locust swarm', 'Creature — Insect').
card_types('locust swarm', ['Creature']).
card_subtypes('locust swarm', ['Insect']).
card_colors('locust swarm', ['G']).
card_text('locust swarm', 'Flying\n{G}: Regenerate Locust Swarm.\n{G}: Untap Locust Swarm. Activate this ability only once each turn.').
card_mana_cost('locust swarm', ['3', 'G']).
card_cmc('locust swarm', 4).
card_layout('locust swarm', 'normal').
card_power('locust swarm', 1).
card_toughness('locust swarm', 1).

% Found in: ALL, ME2
card_name('lodestone bauble', 'Lodestone Bauble').
card_type('lodestone bauble', 'Artifact').
card_types('lodestone bauble', ['Artifact']).
card_subtypes('lodestone bauble', []).
card_colors('lodestone bauble', []).
card_text('lodestone bauble', '{1}, {T}, Sacrifice Lodestone Bauble: Put up to four target basic land cards from a player\'s graveyard on top of his or her library in any order. That player draws a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('lodestone bauble', ['0']).
card_cmc('lodestone bauble', 0).
card_layout('lodestone bauble', 'normal').
card_reserved('lodestone bauble').

% Found in: ARC, MM2, WWK
card_name('lodestone golem', 'Lodestone Golem').
card_type('lodestone golem', 'Artifact Creature — Golem').
card_types('lodestone golem', ['Artifact', 'Creature']).
card_subtypes('lodestone golem', ['Golem']).
card_colors('lodestone golem', []).
card_text('lodestone golem', 'Nonartifact spells cost {1} more to cast.').
card_mana_cost('lodestone golem', ['4']).
card_cmc('lodestone golem', 4).
card_layout('lodestone golem', 'normal').
card_power('lodestone golem', 5).
card_toughness('lodestone golem', 3).

% Found in: HOP, MM2, MRD
card_name('lodestone myr', 'Lodestone Myr').
card_type('lodestone myr', 'Artifact Creature — Myr').
card_types('lodestone myr', ['Artifact', 'Creature']).
card_subtypes('lodestone myr', ['Myr']).
card_colors('lodestone myr', []).
card_text('lodestone myr', 'Trample\nTap an untapped artifact you control: Lodestone Myr gets +1/+1 until end of turn.').
card_mana_cost('lodestone myr', ['4']).
card_cmc('lodestone myr', 4).
card_layout('lodestone myr', 'normal').
card_power('lodestone myr', 2).
card_toughness('lodestone myr', 2).

% Found in: FUT, MMA
card_name('logic knot', 'Logic Knot').
card_type('logic knot', 'Instant').
card_types('logic knot', ['Instant']).
card_subtypes('logic knot', []).
card_colors('logic knot', ['U']).
card_text('logic knot', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nCounter target spell unless its controller pays {X}.').
card_mana_cost('logic knot', ['X', 'U', 'U']).
card_cmc('logic knot', 2).
card_layout('logic knot', 'normal').

% Found in: DDN, ROE
card_name('lone missionary', 'Lone Missionary').
card_type('lone missionary', 'Creature — Kor Monk').
card_types('lone missionary', ['Creature']).
card_subtypes('lone missionary', ['Kor', 'Monk']).
card_colors('lone missionary', ['W']).
card_text('lone missionary', 'When Lone Missionary enters the battlefield, you gain 4 life.').
card_mana_cost('lone missionary', ['1', 'W']).
card_cmc('lone missionary', 2).
card_layout('lone missionary', 'normal').
card_power('lone missionary', 2).
card_toughness('lone missionary', 1).

% Found in: AVR
card_name('lone revenant', 'Lone Revenant').
card_type('lone revenant', 'Creature — Spirit').
card_types('lone revenant', ['Creature']).
card_subtypes('lone revenant', ['Spirit']).
card_colors('lone revenant', ['U']).
card_text('lone revenant', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever Lone Revenant deals combat damage to a player, if you control no other creatures, look at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('lone revenant', ['3', 'U', 'U']).
card_cmc('lone revenant', 5).
card_layout('lone revenant', 'normal').
card_power('lone revenant', 4).
card_toughness('lone revenant', 4).

% Found in: 7ED, 8ED, PO2, PTK, S99, ULG
card_name('lone wolf', 'Lone Wolf').
card_type('lone wolf', 'Creature — Wolf').
card_types('lone wolf', ['Creature']).
card_subtypes('lone wolf', ['Wolf']).
card_colors('lone wolf', ['G']).
card_text('lone wolf', 'You may have Lone Wolf assign its combat damage as though it weren\'t blocked.').
card_mana_cost('lone wolf', ['2', 'G']).
card_cmc('lone wolf', 3).
card_layout('lone wolf', 'normal').
card_power('lone wolf', 2).
card_toughness('lone wolf', 2).

% Found in: C13, C14, CMD, DDJ, ONS, VMA
card_name('lonely sandbar', 'Lonely Sandbar').
card_type('lonely sandbar', 'Land').
card_types('lonely sandbar', ['Land']).
card_subtypes('lonely sandbar', []).
card_colors('lonely sandbar', []).
card_text('lonely sandbar', 'Lonely Sandbar enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nCycling {U} ({U}, Discard this card: Draw a card.)').
card_layout('lonely sandbar', 'normal').

% Found in: CHK, MM2
card_name('long-forgotten gohei', 'Long-Forgotten Gohei').
card_type('long-forgotten gohei', 'Artifact').
card_types('long-forgotten gohei', ['Artifact']).
card_subtypes('long-forgotten gohei', []).
card_colors('long-forgotten gohei', []).
card_text('long-forgotten gohei', 'Arcane spells you cast cost {1} less to cast.\nSpirit creatures you control get +1/+1.').
card_mana_cost('long-forgotten gohei', ['3']).
card_cmc('long-forgotten gohei', 3).
card_layout('long-forgotten gohei', 'normal').

% Found in: SCG
card_name('long-term plans', 'Long-Term Plans').
card_type('long-term plans', 'Instant').
card_types('long-term plans', ['Instant']).
card_subtypes('long-term plans', []).
card_colors('long-term plans', ['U']).
card_text('long-term plans', 'Search your library for a card, shuffle your library, then put that card third from the top.').
card_mana_cost('long-term plans', ['2', 'U']).
card_cmc('long-term plans', 3).
card_layout('long-term plans', 'normal').

% Found in: 6ED, 7ED, VIS, pFNM
card_name('longbow archer', 'Longbow Archer').
card_type('longbow archer', 'Creature — Human Soldier Archer').
card_types('longbow archer', ['Creature']).
card_subtypes('longbow archer', ['Human', 'Soldier', 'Archer']).
card_colors('longbow archer', ['W']).
card_text('longbow archer', 'First strike; reach (This creature can block creatures with flying.)').
card_mana_cost('longbow archer', ['W', 'W']).
card_cmc('longbow archer', 2).
card_layout('longbow archer', 'normal').
card_power('longbow archer', 2).
card_toughness('longbow archer', 2).

% Found in: TOR
card_name('longhorn firebeast', 'Longhorn Firebeast').
card_type('longhorn firebeast', 'Creature — Elemental Ox Beast').
card_types('longhorn firebeast', ['Creature']).
card_subtypes('longhorn firebeast', ['Elemental', 'Ox', 'Beast']).
card_colors('longhorn firebeast', ['R']).
card_text('longhorn firebeast', 'When Longhorn Firebeast enters the battlefield, any opponent may have it deal 5 damage to him or her. If a player does, sacrifice Longhorn Firebeast.').
card_mana_cost('longhorn firebeast', ['2', 'R']).
card_cmc('longhorn firebeast', 3).
card_layout('longhorn firebeast', 'normal').
card_power('longhorn firebeast', 3).
card_toughness('longhorn firebeast', 2).

% Found in: KTK
card_name('longshot squad', 'Longshot Squad').
card_type('longshot squad', 'Creature — Hound Archer').
card_types('longshot squad', ['Creature']).
card_subtypes('longshot squad', ['Hound', 'Archer']).
card_colors('longshot squad', ['G']).
card_text('longshot squad', 'Outlast {1}{G} ({1}{G}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has reach. (A creature with reach can block creatures with flying.)').
card_mana_cost('longshot squad', ['3', 'G']).
card_cmc('longshot squad', 4).
card_layout('longshot squad', 'normal').
card_power('longshot squad', 3).
card_toughness('longshot squad', 3).

% Found in: UNH
card_name('look at me, i\'m r&d', 'Look at Me, I\'m R&D').
card_type('look at me, i\'m r&d', 'Enchantment').
card_types('look at me, i\'m r&d', ['Enchantment']).
card_subtypes('look at me, i\'m r&d', []).
card_colors('look at me, i\'m r&d', ['W']).
card_text('look at me, i\'m r&d', 'As Look at Me, I\'m R&D comes into play, choose a number and a second number one higher or one lower than that number.\nAll instances of the first chosen number on permanents, spells, and cards in any zone are the second chosen number.').
card_mana_cost('look at me, i\'m r&d', ['2', 'W']).
card_cmc('look at me, i\'m r&d', 3).
card_layout('look at me, i\'m r&d', 'normal').

% Found in: UGL
card_name('look at me, i\'m the dci', 'Look at Me, I\'m the DCI').
card_type('look at me, i\'m the dci', 'Sorcery').
card_types('look at me, i\'m the dci', ['Sorcery']).
card_subtypes('look at me, i\'m the dci', []).
card_colors('look at me, i\'m the dci', ['W']).
card_text('look at me, i\'m the dci', 'Ban one card, other than a basic land, for the remainder of the match. (For the remainder of the match, each player removes from the game all copies of that card in play or in any graveyard, hand, library, or sideboard.)').
card_mana_cost('look at me, i\'m the dci', ['5', 'W', 'W']).
card_cmc('look at me, i\'m the dci', 7).
card_layout('look at me, i\'m the dci', 'normal').

% Found in: ARC
card_name('look skyward and despair', 'Look Skyward and Despair').
card_type('look skyward and despair', 'Scheme').
card_types('look skyward and despair', ['Scheme']).
card_subtypes('look skyward and despair', []).
card_colors('look skyward and despair', []).
card_text('look skyward and despair', 'When you set this scheme in motion, put a 5/5 red Dragon creature token with flying onto the battlefield.').
card_layout('look skyward and despair', 'scheme').

% Found in: MRD
card_name('looming hoverguard', 'Looming Hoverguard').
card_type('looming hoverguard', 'Creature — Drone').
card_types('looming hoverguard', ['Creature']).
card_subtypes('looming hoverguard', ['Drone']).
card_colors('looming hoverguard', ['U']).
card_text('looming hoverguard', 'Flying\nWhen Looming Hoverguard enters the battlefield, put target artifact on top of its owner\'s library.').
card_mana_cost('looming hoverguard', ['4', 'U', 'U']).
card_cmc('looming hoverguard', 6).
card_layout('looming hoverguard', 'normal').
card_power('looming hoverguard', 3).
card_toughness('looming hoverguard', 3).

% Found in: 10E, 7ED, 8ED, 9ED, M10, USG
card_name('looming shade', 'Looming Shade').
card_type('looming shade', 'Creature — Shade').
card_types('looming shade', ['Creature']).
card_subtypes('looming shade', ['Shade']).
card_colors('looming shade', ['B']).
card_text('looming shade', '{B}: Looming Shade gets +1/+1 until end of turn.').
card_mana_cost('looming shade', ['2', 'B']).
card_cmc('looming shade', 3).
card_layout('looming shade', 'normal').
card_power('looming shade', 1).
card_toughness('looming shade', 1).

% Found in: BFZ
card_name('looming spires', 'Looming Spires').
card_type('looming spires', 'Land').
card_types('looming spires', ['Land']).
card_subtypes('looming spires', []).
card_colors('looming spires', []).
card_text('looming spires', 'Looming Spires enters the battlefield tapped.\nWhen Looming Spires enters the battlefield, target creature gets +1/+1 and gains first strike until end of turn.\n{T}: Add {R} to your mana pool.').
card_layout('looming spires', 'normal').

% Found in: UNH
card_name('loose lips', 'Loose Lips').
card_type('loose lips', 'Enchant Creature').
card_types('loose lips', ['Enchant', 'Creature']).
card_subtypes('loose lips', []).
card_colors('loose lips', ['U']).
card_text('loose lips', 'As Loose Lips comes into play, choose a sentence with eight or fewer words.\nEnchanted creature has flying.\nWhenever enchanted creature deals damage to an opponent, you draw two cards unless that player says the chosen sentence.').
card_mana_cost('loose lips', ['U']).
card_cmc('loose lips', 1).
card_layout('loose lips', 'normal').

% Found in: TSP
card_name('looter il-kor', 'Looter il-Kor').
card_type('looter il-kor', 'Creature — Kor Rogue').
card_types('looter il-kor', ['Creature']).
card_subtypes('looter il-kor', ['Kor', 'Rogue']).
card_colors('looter il-kor', ['U']).
card_text('looter il-kor', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Looter il-Kor deals damage to an opponent, draw a card, then discard a card.').
card_mana_cost('looter il-kor', ['1', 'U']).
card_cmc('looter il-kor', 2).
card_layout('looter il-kor', 'normal').
card_power('looter il-kor', 1).
card_toughness('looter il-kor', 1).

% Found in: LEG
card_name('lord magnus', 'Lord Magnus').
card_type('lord magnus', 'Legendary Creature — Human Druid').
card_types('lord magnus', ['Creature']).
card_subtypes('lord magnus', ['Human', 'Druid']).
card_supertypes('lord magnus', ['Legendary']).
card_colors('lord magnus', ['W', 'G']).
card_text('lord magnus', 'First strike\nCreatures with plainswalk can be blocked as though they didn\'t have plainswalk.\nCreatures with forestwalk can be blocked as though they didn\'t have forestwalk.').
card_mana_cost('lord magnus', ['3', 'G', 'W', 'W']).
card_cmc('lord magnus', 6).
card_layout('lord magnus', 'normal').
card_power('lord magnus', 4).
card_toughness('lord magnus', 3).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, LEA, LEB, TSB, pSUS
card_name('lord of atlantis', 'Lord of Atlantis').
card_type('lord of atlantis', 'Creature — Merfolk').
card_types('lord of atlantis', ['Creature']).
card_subtypes('lord of atlantis', ['Merfolk']).
card_colors('lord of atlantis', ['U']).
card_text('lord of atlantis', 'Other Merfolk creatures get +1/+1 and have islandwalk. (They can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('lord of atlantis', ['U', 'U']).
card_cmc('lord of atlantis', 2).
card_layout('lord of atlantis', 'normal').
card_power('lord of atlantis', 2).
card_toughness('lord of atlantis', 2).

% Found in: ARB
card_name('lord of extinction', 'Lord of Extinction').
card_type('lord of extinction', 'Creature — Elemental').
card_types('lord of extinction', ['Creature']).
card_subtypes('lord of extinction', ['Elemental']).
card_colors('lord of extinction', ['B', 'G']).
card_text('lord of extinction', 'Lord of Extinction\'s power and toughness are each equal to the number of cards in all graveyards.').
card_mana_cost('lord of extinction', ['3', 'B', 'G']).
card_cmc('lord of extinction', 5).
card_layout('lord of extinction', 'normal').
card_power('lord of extinction', '*').
card_toughness('lord of extinction', '*').

% Found in: ISD
card_name('lord of lineage', 'Lord of Lineage').
card_type('lord of lineage', 'Creature — Vampire').
card_types('lord of lineage', ['Creature']).
card_subtypes('lord of lineage', ['Vampire']).
card_colors('lord of lineage', ['B']).
card_text('lord of lineage', 'Flying\nOther Vampire creatures you control get +2/+2.\n{T}: Put a 2/2 black Vampire creature token with flying onto the battlefield.').
card_layout('lord of lineage', 'double-faced').
card_power('lord of lineage', 5).
card_toughness('lord of lineage', 5).

% Found in: ROE, pLPA
card_name('lord of shatterskull pass', 'Lord of Shatterskull Pass').
card_type('lord of shatterskull pass', 'Creature — Minotaur Shaman').
card_types('lord of shatterskull pass', ['Creature']).
card_subtypes('lord of shatterskull pass', ['Minotaur', 'Shaman']).
card_colors('lord of shatterskull pass', ['R']).
card_text('lord of shatterskull pass', 'Level up {1}{R} ({1}{R}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-5\n6/6\nLEVEL 6+\n6/6\nWhenever Lord of Shatterskull Pass attacks, it deals 6 damage to each creature defending player controls.').
card_mana_cost('lord of shatterskull pass', ['3', 'R']).
card_cmc('lord of shatterskull pass', 4).
card_layout('lord of shatterskull pass', 'leveler').
card_power('lord of shatterskull pass', 3).
card_toughness('lord of shatterskull pass', 3).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, CED, CEI, DD3_DVD, DDC, LEA, LEB
card_name('lord of the pit', 'Lord of the Pit').
card_type('lord of the pit', 'Creature — Demon').
card_types('lord of the pit', ['Creature']).
card_subtypes('lord of the pit', ['Demon']).
card_colors('lord of the pit', ['B']).
card_text('lord of the pit', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a creature other than Lord of the Pit. If you can\'t, Lord of the Pit deals 7 damage to you.').
card_mana_cost('lord of the pit', ['4', 'B', 'B', 'B']).
card_cmc('lord of the pit', 7).
card_layout('lord of the pit', 'normal').
card_power('lord of the pit', 7).
card_toughness('lord of the pit', 7).

% Found in: 10E, 8ED, 9ED, PLS
card_name('lord of the undead', 'Lord of the Undead').
card_type('lord of the undead', 'Creature — Zombie').
card_types('lord of the undead', ['Creature']).
card_subtypes('lord of the undead', ['Zombie']).
card_colors('lord of the undead', ['B']).
card_text('lord of the undead', 'Other Zombie creatures get +1/+1.\n{1}{B}, {T}: Return target Zombie card from your graveyard to your hand.').
card_mana_cost('lord of the undead', ['1', 'B', 'B']).
card_cmc('lord of the undead', 3).
card_layout('lord of the undead', 'normal').
card_power('lord of the undead', 2).
card_toughness('lord of the undead', 2).

% Found in: M12
card_name('lord of the unreal', 'Lord of the Unreal').
card_type('lord of the unreal', 'Creature — Human Wizard').
card_types('lord of the unreal', ['Creature']).
card_subtypes('lord of the unreal', ['Human', 'Wizard']).
card_colors('lord of the unreal', ['U']).
card_text('lord of the unreal', 'Illusion creatures you control get +1/+1 and have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)').
card_mana_cost('lord of the unreal', ['U', 'U']).
card_cmc('lord of the unreal', 2).
card_layout('lord of the unreal', 'normal').
card_power('lord of the unreal', 2).
card_toughness('lord of the unreal', 2).

% Found in: GTC
card_name('lord of the void', 'Lord of the Void').
card_type('lord of the void', 'Creature — Demon').
card_types('lord of the void', ['Creature']).
card_subtypes('lord of the void', ['Demon']).
card_colors('lord of the void', ['B']).
card_text('lord of the void', 'Flying\nWhenever Lord of the Void deals combat damage to a player, exile the top seven cards of that player\'s library, then put a creature card from among them onto the battlefield under your control.').
card_mana_cost('lord of the void', ['4', 'B', 'B', 'B']).
card_cmc('lord of the void', 7).
card_layout('lord of the void', 'normal').
card_power('lord of the void', 7).
card_toughness('lord of the void', 7).

% Found in: ALL, MED
card_name('lord of tresserhorn', 'Lord of Tresserhorn').
card_type('lord of tresserhorn', 'Legendary Creature — Zombie').
card_types('lord of tresserhorn', ['Creature']).
card_subtypes('lord of tresserhorn', ['Zombie']).
card_supertypes('lord of tresserhorn', ['Legendary']).
card_colors('lord of tresserhorn', ['U', 'B', 'R']).
card_text('lord of tresserhorn', 'When Lord of Tresserhorn enters the battlefield, you lose 2 life, you sacrifice two creatures, and target opponent draws two cards.\n{B}: Regenerate Lord of Tresserhorn.').
card_mana_cost('lord of tresserhorn', ['1', 'U', 'B', 'R']).
card_cmc('lord of tresserhorn', 4).
card_layout('lord of tresserhorn', 'normal').
card_power('lord of tresserhorn', 10).
card_toughness('lord of tresserhorn', 4).
card_reserved('lord of tresserhorn').

% Found in: RAV
card_name('lore broker', 'Lore Broker').
card_type('lore broker', 'Creature — Human Rogue').
card_types('lore broker', ['Creature']).
card_subtypes('lore broker', ['Human', 'Rogue']).
card_colors('lore broker', ['U']).
card_text('lore broker', '{T}: Each player draws a card, then discards a card.').
card_mana_cost('lore broker', ['1', 'U']).
card_cmc('lore broker', 2).
card_layout('lore broker', 'normal').
card_power('lore broker', 1).
card_toughness('lore broker', 2).

% Found in: CNS
card_name('lore seeker', 'Lore Seeker').
card_type('lore seeker', 'Artifact Creature — Construct').
card_types('lore seeker', ['Artifact', 'Creature']).
card_subtypes('lore seeker', ['Construct']).
card_colors('lore seeker', []).
card_text('lore seeker', 'Reveal Lore Seeker as you draft it. After you draft Lore Seeker, you may add a booster pack to the draft. (Your next pick is from that booster pack. Pass it to the next player and it\'s drafted this draft round.)').
card_mana_cost('lore seeker', ['2']).
card_cmc('lore seeker', 2).
card_layout('lore seeker', 'normal').
card_power('lore seeker', 2).
card_toughness('lore seeker', 2).

% Found in: ARB, DDO, MM2
card_name('lorescale coatl', 'Lorescale Coatl').
card_type('lorescale coatl', 'Creature — Snake').
card_types('lorescale coatl', ['Creature']).
card_subtypes('lorescale coatl', ['Snake']).
card_colors('lorescale coatl', ['U', 'G']).
card_text('lorescale coatl', 'Whenever you draw a card, you may put a +1/+1 counter on Lorescale Coatl.').
card_mana_cost('lorescale coatl', ['1', 'G', 'U']).
card_cmc('lorescale coatl', 3).
card_layout('lorescale coatl', 'normal').
card_power('lorescale coatl', 2).
card_toughness('lorescale coatl', 2).

% Found in: C14
card_name('loreseeker\'s stone', 'Loreseeker\'s Stone').
card_type('loreseeker\'s stone', 'Artifact').
card_types('loreseeker\'s stone', ['Artifact']).
card_subtypes('loreseeker\'s stone', []).
card_colors('loreseeker\'s stone', []).
card_text('loreseeker\'s stone', '{3}, {T}: Draw three cards. This ability costs {1} more to activate for each card in your hand.').
card_mana_cost('loreseeker\'s stone', ['6']).
card_cmc('loreseeker\'s stone', 6).
card_layout('loreseeker\'s stone', 'normal').

% Found in: C14, ZEN
card_name('lorthos, the tidemaker', 'Lorthos, the Tidemaker').
card_type('lorthos, the tidemaker', 'Legendary Creature — Octopus').
card_types('lorthos, the tidemaker', ['Creature']).
card_subtypes('lorthos, the tidemaker', ['Octopus']).
card_supertypes('lorthos, the tidemaker', ['Legendary']).
card_colors('lorthos, the tidemaker', ['U']).
card_text('lorthos, the tidemaker', 'Whenever Lorthos, the Tidemaker attacks, you may pay {8}. If you do, tap up to eight target permanents. Those permanents don\'t untap during their controllers\' next untap steps.').
card_mana_cost('lorthos, the tidemaker', ['5', 'U', 'U', 'U']).
card_cmc('lorthos, the tidemaker', 8).
card_layout('lorthos, the tidemaker', 'normal').
card_power('lorthos, the tidemaker', 8).
card_toughness('lorthos, the tidemaker', 8).

% Found in: DTK
card_name('lose calm', 'Lose Calm').
card_type('lose calm', 'Sorcery').
card_types('lose calm', ['Sorcery']).
card_subtypes('lose calm', []).
card_colors('lose calm', ['R']).
card_text('lose calm', 'Gain control of target creature until end of turn. Untap that creature. It gains haste and menace until end of turn. (A creature with menace can\'t be blocked except by two or more creatures.)').
card_mana_cost('lose calm', ['3', 'R']).
card_cmc('lose calm', 4).
card_layout('lose calm', 'normal').

% Found in: 5DN
card_name('lose hope', 'Lose Hope').
card_type('lose hope', 'Instant').
card_types('lose hope', ['Instant']).
card_subtypes('lose hope', []).
card_colors('lose hope', ['B']).
card_text('lose hope', 'Target creature gets -1/-1 until end of turn. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('lose hope', ['B']).
card_cmc('lose hope', 1).
card_layout('lose hope', 'normal').

% Found in: DGM
card_name('loss', 'Loss').
card_type('loss', 'Instant').
card_types('loss', ['Instant']).
card_subtypes('loss', []).
card_colors('loss', ['B']).
card_text('loss', 'Creatures your opponents control get -1/-1 until end of turn.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('loss', ['2', 'B']).
card_cmc('loss', 3).
card_layout('loss', 'split').

% Found in: FUT
card_name('lost auramancers', 'Lost Auramancers').
card_type('lost auramancers', 'Creature — Human Wizard').
card_types('lost auramancers', ['Creature']).
card_subtypes('lost auramancers', ['Human', 'Wizard']).
card_colors('lost auramancers', ['W']).
card_text('lost auramancers', 'Vanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Lost Auramancers dies, if it had no time counters on it, you may search your library for an enchantment card and put it onto the battlefield. If you do, shuffle your library.').
card_mana_cost('lost auramancers', ['2', 'W', 'W']).
card_cmc('lost auramancers', 4).
card_layout('lost auramancers', 'normal').
card_power('lost auramancers', 3).
card_toughness('lost auramancers', 3).

% Found in: FUT
card_name('lost hours', 'Lost Hours').
card_type('lost hours', 'Sorcery').
card_types('lost hours', ['Sorcery']).
card_subtypes('lost hours', []).
card_colors('lost hours', ['B']).
card_text('lost hours', 'Target player reveals his or her hand. You choose a nonland card from it. That player puts that card into his or her library third from the top.').
card_mana_cost('lost hours', ['1', 'B']).
card_cmc('lost hours', 2).
card_layout('lost hours', 'normal').

% Found in: THS
card_name('lost in a labyrinth', 'Lost in a Labyrinth').
card_type('lost in a labyrinth', 'Instant').
card_types('lost in a labyrinth', ['Instant']).
card_subtypes('lost in a labyrinth', []).
card_colors('lost in a labyrinth', ['U']).
card_text('lost in a labyrinth', 'Target creature gets -3/-0 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('lost in a labyrinth', ['U']).
card_cmc('lost in a labyrinth', 1).
card_layout('lost in a labyrinth', 'normal').

% Found in: ISD
card_name('lost in the mist', 'Lost in the Mist').
card_type('lost in the mist', 'Instant').
card_types('lost in the mist', ['Instant']).
card_subtypes('lost in the mist', []).
card_colors('lost in the mist', ['U']).
card_text('lost in the mist', 'Counter target spell. Return target permanent to its owner\'s hand.').
card_mana_cost('lost in the mist', ['3', 'U', 'U']).
card_cmc('lost in the mist', 5).
card_layout('lost in the mist', 'normal').

% Found in: DKA
card_name('lost in the woods', 'Lost in the Woods').
card_type('lost in the woods', 'Enchantment').
card_types('lost in the woods', ['Enchantment']).
card_subtypes('lost in the woods', []).
card_colors('lost in the woods', ['G']).
card_text('lost in the woods', 'Whenever a creature attacks you or a planeswalker you control, reveal the top card of your library. If it\'s a Forest card, remove that creature from combat. Then put the revealed card on the bottom of your library.').
card_mana_cost('lost in the woods', ['3', 'G', 'G']).
card_cmc('lost in the woods', 5).
card_layout('lost in the woods', 'normal').

% Found in: JUD
card_name('lost in thought', 'Lost in Thought').
card_type('lost in thought', 'Enchantment — Aura').
card_types('lost in thought', ['Enchantment']).
card_subtypes('lost in thought', ['Aura']).
card_colors('lost in thought', ['U']).
card_text('lost in thought', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated. Its controller may exile three cards from his or her graveyard for that player to ignore this effect until end of turn.').
card_mana_cost('lost in thought', ['1', 'U']).
card_cmc('lost in thought', 2).
card_layout('lost in thought', 'normal').

% Found in: NPH
card_name('lost leonin', 'Lost Leonin').
card_type('lost leonin', 'Creature — Cat Soldier').
card_types('lost leonin', ['Creature']).
card_subtypes('lost leonin', ['Cat', 'Soldier']).
card_colors('lost leonin', ['W']).
card_text('lost leonin', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('lost leonin', ['1', 'W']).
card_cmc('lost leonin', 2).
card_layout('lost leonin', 'normal').
card_power('lost leonin', 2).
card_toughness('lost leonin', 1).

% Found in: ICE, ME2
card_name('lost order of jarkeld', 'Lost Order of Jarkeld').
card_type('lost order of jarkeld', 'Creature — Human Knight').
card_types('lost order of jarkeld', ['Creature']).
card_subtypes('lost order of jarkeld', ['Human', 'Knight']).
card_colors('lost order of jarkeld', ['W']).
card_text('lost order of jarkeld', 'As Lost Order of Jarkeld enters the battlefield, choose an opponent.\nLost Order of Jarkeld\'s power and toughness are each equal to 1 plus the number of creatures the chosen player controls.').
card_mana_cost('lost order of jarkeld', ['2', 'W', 'W']).
card_cmc('lost order of jarkeld', 4).
card_layout('lost order of jarkeld', 'normal').
card_power('lost order of jarkeld', '1+*').
card_toughness('lost order of jarkeld', '1+*').

% Found in: 4ED, 5ED, 6ED, ITP, LEG, RQS
card_name('lost soul', 'Lost Soul').
card_type('lost soul', 'Creature — Spirit Minion').
card_types('lost soul', ['Creature']).
card_subtypes('lost soul', ['Spirit', 'Minion']).
card_colors('lost soul', ['B']).
card_text('lost soul', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('lost soul', ['1', 'B', 'B']).
card_cmc('lost soul', 3).
card_layout('lost soul', 'normal').
card_power('lost soul', 2).
card_toughness('lost soul', 1).

% Found in: RTR
card_name('lotleth troll', 'Lotleth Troll').
card_type('lotleth troll', 'Creature — Zombie Troll').
card_types('lotleth troll', ['Creature']).
card_subtypes('lotleth troll', ['Zombie', 'Troll']).
card_colors('lotleth troll', ['B', 'G']).
card_text('lotleth troll', 'Trample\nDiscard a creature card: Put a +1/+1 counter on Lotleth Troll.\n{B}: Regenerate Lotleth Troll.').
card_mana_cost('lotleth troll', ['B', 'G']).
card_cmc('lotleth troll', 2).
card_layout('lotleth troll', 'normal').
card_power('lotleth troll', 2).
card_toughness('lotleth troll', 1).

% Found in: MMA, TSP, pPRE
card_name('lotus bloom', 'Lotus Bloom').
card_type('lotus bloom', 'Artifact').
card_types('lotus bloom', ['Artifact']).
card_subtypes('lotus bloom', []).
card_colors('lotus bloom', []).
card_text('lotus bloom', 'Suspend 3—{0} (Rather than cast this card from your hand, pay {0} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\n{T}, Sacrifice Lotus Bloom: Add three mana of any one color to your mana pool.').
card_layout('lotus bloom', 'normal').

% Found in: USG
card_name('lotus blossom', 'Lotus Blossom').
card_type('lotus blossom', 'Artifact').
card_types('lotus blossom', ['Artifact']).
card_subtypes('lotus blossom', []).
card_colors('lotus blossom', []).
card_text('lotus blossom', 'At the beginning of your upkeep, you may put a petal counter on Lotus Blossom.\n{T}, Sacrifice Lotus Blossom: Add X mana of any one color to your mana pool, where X is the number of petal counters on Lotus Blossom.').
card_mana_cost('lotus blossom', ['2']).
card_cmc('lotus blossom', 2).
card_layout('lotus blossom', 'normal').

% Found in: ZEN, pGPX
card_name('lotus cobra', 'Lotus Cobra').
card_type('lotus cobra', 'Creature — Snake').
card_types('lotus cobra', ['Creature']).
card_subtypes('lotus cobra', ['Snake']).
card_colors('lotus cobra', ['G']).
card_text('lotus cobra', 'Landfall — Whenever a land enters the battlefield under your control, you may add one mana of any color to your mana pool.').
card_mana_cost('lotus cobra', ['1', 'G']).
card_cmc('lotus cobra', 2).
card_layout('lotus cobra', 'normal').
card_power('lotus cobra', 2).
card_toughness('lotus cobra', 1).

% Found in: INV
card_name('lotus guardian', 'Lotus Guardian').
card_type('lotus guardian', 'Artifact Creature — Dragon').
card_types('lotus guardian', ['Artifact', 'Creature']).
card_subtypes('lotus guardian', ['Dragon']).
card_colors('lotus guardian', []).
card_text('lotus guardian', 'Flying\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('lotus guardian', ['7']).
card_cmc('lotus guardian', 7).
card_layout('lotus guardian', 'normal').
card_power('lotus guardian', 4).
card_toughness('lotus guardian', 4).

% Found in: FRF
card_name('lotus path djinn', 'Lotus Path Djinn').
card_type('lotus path djinn', 'Creature — Djinn Monk').
card_types('lotus path djinn', ['Creature']).
card_subtypes('lotus path djinn', ['Djinn', 'Monk']).
card_colors('lotus path djinn', ['U']).
card_text('lotus path djinn', 'Flying\nProwess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)').
card_mana_cost('lotus path djinn', ['3', 'U']).
card_cmc('lotus path djinn', 4).
card_layout('lotus path djinn', 'normal').
card_power('lotus path djinn', 2).
card_toughness('lotus path djinn', 3).

% Found in: TMP, TPR, V09
card_name('lotus petal', 'Lotus Petal').
card_type('lotus petal', 'Artifact').
card_types('lotus petal', ['Artifact']).
card_subtypes('lotus petal', []).
card_colors('lotus petal', []).
card_text('lotus petal', '{T}, Sacrifice Lotus Petal: Add one mana of any color to your mana pool.').
card_mana_cost('lotus petal', ['0']).
card_cmc('lotus petal', 0).
card_layout('lotus petal', 'normal').

% Found in: WTH
card_name('lotus vale', 'Lotus Vale').
card_type('lotus vale', 'Land').
card_types('lotus vale', ['Land']).
card_subtypes('lotus vale', []).
card_colors('lotus vale', []).
card_text('lotus vale', 'If Lotus Vale would enter the battlefield, sacrifice two untapped lands instead. If you do, put Lotus Vale onto the battlefield. If you don\'t, put it into its owner\'s graveyard.\n{T}: Add three mana of any one color to your mana pool.').
card_layout('lotus vale', 'normal').
card_reserved('lotus vale').

% Found in: FRF
card_name('lotus-eye mystics', 'Lotus-Eye Mystics').
card_type('lotus-eye mystics', 'Creature — Human Monk').
card_types('lotus-eye mystics', ['Creature']).
card_subtypes('lotus-eye mystics', ['Human', 'Monk']).
card_colors('lotus-eye mystics', ['W']).
card_text('lotus-eye mystics', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhen Lotus-Eye Mystics enters the battlefield, return target enchantment card from your graveyard to your hand.').
card_mana_cost('lotus-eye mystics', ['3', 'W']).
card_cmc('lotus-eye mystics', 4).
card_layout('lotus-eye mystics', 'normal').
card_power('lotus-eye mystics', 3).
card_toughness('lotus-eye mystics', 2).

% Found in: CSP
card_name('lovisa coldeyes', 'Lovisa Coldeyes').
card_type('lovisa coldeyes', 'Legendary Creature — Human').
card_types('lovisa coldeyes', ['Creature']).
card_subtypes('lovisa coldeyes', ['Human']).
card_supertypes('lovisa coldeyes', ['Legendary']).
card_colors('lovisa coldeyes', ['R']).
card_text('lovisa coldeyes', 'Each creature that\'s a Barbarian, a Warrior, or a Berserker gets +2/+2 and has haste.').
card_mana_cost('lovisa coldeyes', ['3', 'R', 'R']).
card_cmc('lovisa coldeyes', 5).
card_layout('lovisa coldeyes', 'normal').
card_power('lovisa coldeyes', 3).
card_toughness('lovisa coldeyes', 3).

% Found in: STH, TPR
card_name('lowland basilisk', 'Lowland Basilisk').
card_type('lowland basilisk', 'Creature — Basilisk').
card_types('lowland basilisk', ['Creature']).
card_subtypes('lowland basilisk', ['Basilisk']).
card_colors('lowland basilisk', ['G']).
card_text('lowland basilisk', 'Whenever Lowland Basilisk deals damage to a creature, destroy that creature at end of combat.').
card_mana_cost('lowland basilisk', ['2', 'G']).
card_cmc('lowland basilisk', 3).
card_layout('lowland basilisk', 'normal').
card_power('lowland basilisk', 1).
card_toughness('lowland basilisk', 3).

% Found in: BTD, TMP, TPR
card_name('lowland giant', 'Lowland Giant').
card_type('lowland giant', 'Creature — Giant').
card_types('lowland giant', ['Creature']).
card_subtypes('lowland giant', ['Giant']).
card_colors('lowland giant', ['R']).
card_text('lowland giant', '').
card_mana_cost('lowland giant', ['2', 'R', 'R']).
card_cmc('lowland giant', 4).
card_layout('lowland giant', 'normal').
card_power('lowland giant', 4).
card_toughness('lowland giant', 3).

% Found in: LRW
card_name('lowland oaf', 'Lowland Oaf').
card_type('lowland oaf', 'Creature — Giant Warrior').
card_types('lowland oaf', ['Creature']).
card_subtypes('lowland oaf', ['Giant', 'Warrior']).
card_colors('lowland oaf', ['R']).
card_text('lowland oaf', '{T}: Target Goblin creature you control gets +1/+0 and gains flying until end of turn. Sacrifice that creature at the beginning of the next end step.').
card_mana_cost('lowland oaf', ['3', 'R']).
card_cmc('lowland oaf', 4).
card_layout('lowland oaf', 'normal').
card_power('lowland oaf', 3).
card_toughness('lowland oaf', 3).

% Found in: LGN
card_name('lowland tracker', 'Lowland Tracker').
card_type('lowland tracker', 'Creature — Human Soldier').
card_types('lowland tracker', ['Creature']).
card_subtypes('lowland tracker', ['Human', 'Soldier']).
card_colors('lowland tracker', ['W']).
card_text('lowland tracker', 'First strike\nProvoke (Whenever this creature attacks, you may have target creature defending player controls untap and block it if able.)').
card_mana_cost('lowland tracker', ['4', 'W']).
card_cmc('lowland tracker', 5).
card_layout('lowland tracker', 'normal').
card_power('lowland tracker', 2).
card_toughness('lowland tracker', 2).

% Found in: 5DN
card_name('loxodon anchorite', 'Loxodon Anchorite').
card_type('loxodon anchorite', 'Creature — Elephant Cleric').
card_types('loxodon anchorite', ['Creature']).
card_subtypes('loxodon anchorite', ['Elephant', 'Cleric']).
card_colors('loxodon anchorite', ['W']).
card_text('loxodon anchorite', '{T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('loxodon anchorite', ['2', 'W', 'W']).
card_cmc('loxodon anchorite', 4).
card_layout('loxodon anchorite', 'normal').
card_power('loxodon anchorite', 2).
card_toughness('loxodon anchorite', 3).

% Found in: NPH
card_name('loxodon convert', 'Loxodon Convert').
card_type('loxodon convert', 'Creature — Elephant Soldier').
card_types('loxodon convert', ['Creature']).
card_subtypes('loxodon convert', ['Elephant', 'Soldier']).
card_colors('loxodon convert', ['W']).
card_text('loxodon convert', '').
card_mana_cost('loxodon convert', ['3', 'W']).
card_cmc('loxodon convert', 4).
card_layout('loxodon convert', 'normal').
card_power('loxodon convert', 4).
card_toughness('loxodon convert', 2).

% Found in: RAV
card_name('loxodon gatekeeper', 'Loxodon Gatekeeper').
card_type('loxodon gatekeeper', 'Creature — Elephant Soldier').
card_types('loxodon gatekeeper', ['Creature']).
card_subtypes('loxodon gatekeeper', ['Elephant', 'Soldier']).
card_colors('loxodon gatekeeper', ['W']).
card_text('loxodon gatekeeper', 'Artifacts, creatures, and lands your opponents control enter the battlefield tapped.').
card_mana_cost('loxodon gatekeeper', ['2', 'W', 'W']).
card_cmc('loxodon gatekeeper', 4).
card_layout('loxodon gatekeeper', 'normal').
card_power('loxodon gatekeeper', 2).
card_toughness('loxodon gatekeeper', 3).

% Found in: DDH, RAV
card_name('loxodon hierarch', 'Loxodon Hierarch').
card_type('loxodon hierarch', 'Creature — Elephant Cleric').
card_types('loxodon hierarch', ['Creature']).
card_subtypes('loxodon hierarch', ['Elephant', 'Cleric']).
card_colors('loxodon hierarch', ['W', 'G']).
card_text('loxodon hierarch', 'When Loxodon Hierarch enters the battlefield, you gain 4 life.\n{G}{W}, Sacrifice Loxodon Hierarch: Regenerate each creature you control.').
card_mana_cost('loxodon hierarch', ['2', 'G', 'W']).
card_cmc('loxodon hierarch', 4).
card_layout('loxodon hierarch', 'normal').
card_power('loxodon hierarch', 4).
card_toughness('loxodon hierarch', 4).

% Found in: VAN
card_name('loxodon hierarch avatar', 'Loxodon Hierarch Avatar').
card_type('loxodon hierarch avatar', 'Vanguard').
card_types('loxodon hierarch avatar', ['Vanguard']).
card_subtypes('loxodon hierarch avatar', []).
card_colors('loxodon hierarch avatar', []).
card_text('loxodon hierarch avatar', 'Sacrifice a permanent: Regenerate target creature you control.').
card_layout('loxodon hierarch avatar', 'vanguard').

% Found in: MRD
card_name('loxodon mender', 'Loxodon Mender').
card_type('loxodon mender', 'Creature — Elephant Cleric').
card_types('loxodon mender', ['Creature']).
card_subtypes('loxodon mender', ['Elephant', 'Cleric']).
card_colors('loxodon mender', ['W']).
card_text('loxodon mender', '{W}, {T}: Regenerate target artifact.').
card_mana_cost('loxodon mender', ['5', 'W']).
card_cmc('loxodon mender', 6).
card_layout('loxodon mender', 'normal').
card_power('loxodon mender', 3).
card_toughness('loxodon mender', 3).

% Found in: 10E, DST
card_name('loxodon mystic', 'Loxodon Mystic').
card_type('loxodon mystic', 'Creature — Elephant Cleric').
card_types('loxodon mystic', ['Creature']).
card_subtypes('loxodon mystic', ['Elephant', 'Cleric']).
card_colors('loxodon mystic', ['W']).
card_text('loxodon mystic', '{W}, {T}: Tap target creature.').
card_mana_cost('loxodon mystic', ['3', 'W', 'W']).
card_cmc('loxodon mystic', 5).
card_layout('loxodon mystic', 'normal').
card_power('loxodon mystic', 3).
card_toughness('loxodon mystic', 3).

% Found in: DDO, MBS
card_name('loxodon partisan', 'Loxodon Partisan').
card_type('loxodon partisan', 'Creature — Elephant Soldier').
card_types('loxodon partisan', ['Creature']).
card_subtypes('loxodon partisan', ['Elephant', 'Soldier']).
card_colors('loxodon partisan', ['W']).
card_text('loxodon partisan', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)').
card_mana_cost('loxodon partisan', ['4', 'W']).
card_cmc('loxodon partisan', 5).
card_layout('loxodon partisan', 'normal').
card_power('loxodon partisan', 3).
card_toughness('loxodon partisan', 4).

% Found in: MRD
card_name('loxodon peacekeeper', 'Loxodon Peacekeeper').
card_type('loxodon peacekeeper', 'Creature — Elephant Soldier').
card_types('loxodon peacekeeper', ['Creature']).
card_subtypes('loxodon peacekeeper', ['Elephant', 'Soldier']).
card_colors('loxodon peacekeeper', ['W']).
card_text('loxodon peacekeeper', 'At the beginning of your upkeep, the player with the lowest life total gains control of Loxodon Peacekeeper. If two or more players are tied for lowest life total, you choose one of them, and that player gains control of Loxodon Peacekeeper.').
card_mana_cost('loxodon peacekeeper', ['1', 'W']).
card_cmc('loxodon peacekeeper', 2).
card_layout('loxodon peacekeeper', 'normal').
card_power('loxodon peacekeeper', 4).
card_toughness('loxodon peacekeeper', 4).

% Found in: MRD
card_name('loxodon punisher', 'Loxodon Punisher').
card_type('loxodon punisher', 'Creature — Elephant Soldier').
card_types('loxodon punisher', ['Creature']).
card_subtypes('loxodon punisher', ['Elephant', 'Soldier']).
card_colors('loxodon punisher', ['W']).
card_text('loxodon punisher', 'Loxodon Punisher gets +2/+2 for each Equipment attached to it.').
card_mana_cost('loxodon punisher', ['3', 'W']).
card_cmc('loxodon punisher', 4).
card_layout('loxodon punisher', 'normal').
card_power('loxodon punisher', 2).
card_toughness('loxodon punisher', 2).

% Found in: RTR
card_name('loxodon smiter', 'Loxodon Smiter').
card_type('loxodon smiter', 'Creature — Elephant Soldier').
card_types('loxodon smiter', ['Creature']).
card_subtypes('loxodon smiter', ['Elephant', 'Soldier']).
card_colors('loxodon smiter', ['W', 'G']).
card_text('loxodon smiter', 'Loxodon Smiter can\'t be countered.\nIf a spell or ability an opponent controls causes you to discard Loxodon Smiter, put it onto the battlefield instead of putting it into your graveyard.').
card_mana_cost('loxodon smiter', ['1', 'G', 'W']).
card_cmc('loxodon smiter', 3).
card_layout('loxodon smiter', 'normal').
card_power('loxodon smiter', 4).
card_toughness('loxodon smiter', 4).

% Found in: 5DN
card_name('loxodon stalwart', 'Loxodon Stalwart').
card_type('loxodon stalwart', 'Creature — Elephant Soldier').
card_types('loxodon stalwart', ['Creature']).
card_subtypes('loxodon stalwart', ['Elephant', 'Soldier']).
card_colors('loxodon stalwart', ['W']).
card_text('loxodon stalwart', 'Vigilance\n{W}: Loxodon Stalwart gets +0/+1 until end of turn.').
card_mana_cost('loxodon stalwart', ['3', 'W', 'W']).
card_cmc('loxodon stalwart', 5).
card_layout('loxodon stalwart', 'normal').
card_power('loxodon stalwart', 3).
card_toughness('loxodon stalwart', 3).

% Found in: 10E, 9ED, C14, DDG, DPA, HOP, MRD
card_name('loxodon warhammer', 'Loxodon Warhammer').
card_type('loxodon warhammer', 'Artifact — Equipment').
card_types('loxodon warhammer', ['Artifact']).
card_subtypes('loxodon warhammer', ['Equipment']).
card_colors('loxodon warhammer', []).
card_text('loxodon warhammer', 'Equipped creature gets +3/+0 and has trample and lifelink.\nEquip {3}').
card_mana_cost('loxodon warhammer', ['3']).
card_cmc('loxodon warhammer', 3).
card_layout('loxodon warhammer', 'normal').

% Found in: SOM
card_name('loxodon wayfarer', 'Loxodon Wayfarer').
card_type('loxodon wayfarer', 'Creature — Elephant Monk').
card_types('loxodon wayfarer', ['Creature']).
card_subtypes('loxodon wayfarer', ['Elephant', 'Monk']).
card_colors('loxodon wayfarer', ['W']).
card_text('loxodon wayfarer', '').
card_mana_cost('loxodon wayfarer', ['2', 'W']).
card_cmc('loxodon wayfarer', 3).
card_layout('loxodon wayfarer', 'normal').
card_power('loxodon wayfarer', 1).
card_toughness('loxodon wayfarer', 5).

% Found in: DKA
card_name('loyal cathar', 'Loyal Cathar').
card_type('loyal cathar', 'Creature — Human Soldier').
card_types('loyal cathar', ['Creature']).
card_subtypes('loyal cathar', ['Human', 'Soldier']).
card_colors('loyal cathar', ['W']).
card_text('loyal cathar', 'Vigilance\nWhen Loyal Cathar dies, return it to the battlefield transformed under your control at the beginning of the next end step.').
card_mana_cost('loyal cathar', ['W', 'W']).
card_cmc('loyal cathar', 2).
card_layout('loyal cathar', 'double-faced').
card_power('loyal cathar', 2).
card_toughness('loyal cathar', 2).
card_sides('loyal cathar', 'unhallowed cathar').

% Found in: EVE
card_name('loyal gyrfalcon', 'Loyal Gyrfalcon').
card_type('loyal gyrfalcon', 'Creature — Bird').
card_types('loyal gyrfalcon', ['Creature']).
card_subtypes('loyal gyrfalcon', ['Bird']).
card_colors('loyal gyrfalcon', ['W']).
card_text('loyal gyrfalcon', 'Defender, flying\nWhenever you cast a white spell, Loyal Gyrfalcon loses defender until end of turn.').
card_mana_cost('loyal gyrfalcon', ['3', 'W']).
card_cmc('loyal gyrfalcon', 4).
card_layout('loyal gyrfalcon', 'normal').
card_power('loyal gyrfalcon', 3).
card_toughness('loyal gyrfalcon', 3).

% Found in: BNG
card_name('loyal pegasus', 'Loyal Pegasus').
card_type('loyal pegasus', 'Creature — Pegasus').
card_types('loyal pegasus', ['Creature']).
card_subtypes('loyal pegasus', ['Pegasus']).
card_colors('loyal pegasus', ['W']).
card_text('loyal pegasus', 'Flying\nLoyal Pegasus can\'t attack or block alone.').
card_mana_cost('loyal pegasus', ['W']).
card_cmc('loyal pegasus', 1).
card_layout('loyal pegasus', 'normal').
card_power('loyal pegasus', 2).
card_toughness('loyal pegasus', 1).

% Found in: CM1, ME3, PTK
card_name('loyal retainers', 'Loyal Retainers').
card_type('loyal retainers', 'Creature — Human Advisor').
card_types('loyal retainers', ['Creature']).
card_subtypes('loyal retainers', ['Human', 'Advisor']).
card_colors('loyal retainers', ['W']).
card_text('loyal retainers', 'Sacrifice Loyal Retainers: Return target legendary creature card from your graveyard to the battlefield. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('loyal retainers', ['2', 'W']).
card_cmc('loyal retainers', 3).
card_layout('loyal retainers', 'normal').
card_power('loyal retainers', 1).
card_toughness('loyal retainers', 1).

% Found in: 10E, DDF, S99
card_name('loyal sentry', 'Loyal Sentry').
card_type('loyal sentry', 'Creature — Human Soldier').
card_types('loyal sentry', ['Creature']).
card_subtypes('loyal sentry', ['Human', 'Soldier']).
card_colors('loyal sentry', ['W']).
card_text('loyal sentry', 'When Loyal Sentry blocks a creature, destroy that creature and Loyal Sentry.').
card_mana_cost('loyal sentry', ['W']).
card_cmc('loyal sentry', 1).
card_layout('loyal sentry', 'normal').
card_power('loyal sentry', 1).
card_toughness('loyal sentry', 1).

% Found in: ME3, PTK, pPRE
card_name('lu bu, master-at-arms', 'Lu Bu, Master-at-Arms').
card_type('lu bu, master-at-arms', 'Legendary Creature — Human Soldier Warrior').
card_types('lu bu, master-at-arms', ['Creature']).
card_subtypes('lu bu, master-at-arms', ['Human', 'Soldier', 'Warrior']).
card_supertypes('lu bu, master-at-arms', ['Legendary']).
card_colors('lu bu, master-at-arms', ['R']).
card_text('lu bu, master-at-arms', 'Haste; horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('lu bu, master-at-arms', ['5', 'R']).
card_cmc('lu bu, master-at-arms', 6).
card_layout('lu bu, master-at-arms', 'normal').
card_power('lu bu, master-at-arms', 4).
card_toughness('lu bu, master-at-arms', 3).

% Found in: ME3, PTK
card_name('lu meng, wu general', 'Lu Meng, Wu General').
card_type('lu meng, wu general', 'Legendary Creature — Human Soldier').
card_types('lu meng, wu general', ['Creature']).
card_subtypes('lu meng, wu general', ['Human', 'Soldier']).
card_supertypes('lu meng, wu general', ['Legendary']).
card_colors('lu meng, wu general', ['U']).
card_text('lu meng, wu general', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)').
card_mana_cost('lu meng, wu general', ['3', 'U', 'U']).
card_cmc('lu meng, wu general', 5).
card_layout('lu meng, wu general', 'normal').
card_power('lu meng, wu general', 4).
card_toughness('lu meng, wu general', 4).

% Found in: PTK
card_name('lu su, wu advisor', 'Lu Su, Wu Advisor').
card_type('lu su, wu advisor', 'Legendary Creature — Human Advisor').
card_types('lu su, wu advisor', ['Creature']).
card_subtypes('lu su, wu advisor', ['Human', 'Advisor']).
card_supertypes('lu su, wu advisor', ['Legendary']).
card_colors('lu su, wu advisor', ['U']).
card_text('lu su, wu advisor', '{T}: Draw a card. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('lu su, wu advisor', ['3', 'U', 'U']).
card_cmc('lu su, wu advisor', 5).
card_layout('lu su, wu advisor', 'normal').
card_power('lu su, wu advisor', 1).
card_toughness('lu su, wu advisor', 2).

% Found in: C13, ME3, PTK
card_name('lu xun, scholar general', 'Lu Xun, Scholar General').
card_type('lu xun, scholar general', 'Legendary Creature — Human Soldier').
card_types('lu xun, scholar general', ['Creature']).
card_subtypes('lu xun, scholar general', ['Human', 'Soldier']).
card_supertypes('lu xun, scholar general', ['Legendary']).
card_colors('lu xun, scholar general', ['U']).
card_text('lu xun, scholar general', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhenever Lu Xun, Scholar General deals damage to an opponent, you may draw a card.').
card_mana_cost('lu xun, scholar general', ['2', 'U', 'U']).
card_cmc('lu xun, scholar general', 4).
card_layout('lu xun, scholar general', 'normal').
card_power('lu xun, scholar general', 1).
card_toughness('lu xun, scholar general', 3).

% Found in: FUT
card_name('lucent liminid', 'Lucent Liminid').
card_type('lucent liminid', 'Enchantment Creature — Elemental').
card_types('lucent liminid', ['Enchantment', 'Creature']).
card_subtypes('lucent liminid', ['Elemental']).
card_colors('lucent liminid', ['W']).
card_text('lucent liminid', 'Flying').
card_mana_cost('lucent liminid', ['3', 'W', 'W']).
card_cmc('lucent liminid', 5).
card_layout('lucent liminid', 'normal').
card_power('lucent liminid', 3).
card_toughness('lucent liminid', 3).

% Found in: ISD, pLPA
card_name('ludevic\'s abomination', 'Ludevic\'s Abomination').
card_type('ludevic\'s abomination', 'Creature — Lizard Horror').
card_types('ludevic\'s abomination', ['Creature']).
card_subtypes('ludevic\'s abomination', ['Lizard', 'Horror']).
card_colors('ludevic\'s abomination', ['U']).
card_text('ludevic\'s abomination', 'Trample').
card_layout('ludevic\'s abomination', 'double-faced').
card_power('ludevic\'s abomination', 13).
card_toughness('ludevic\'s abomination', 13).

% Found in: ISD, pLPA
card_name('ludevic\'s test subject', 'Ludevic\'s Test Subject').
card_type('ludevic\'s test subject', 'Creature — Lizard').
card_types('ludevic\'s test subject', ['Creature']).
card_subtypes('ludevic\'s test subject', ['Lizard']).
card_colors('ludevic\'s test subject', ['U']).
card_text('ludevic\'s test subject', 'Defender\n{1}{U}: Put a hatchling counter on Ludevic\'s Test Subject. Then if there are five or more hatchling counters on it, remove all of them and transform it.').
card_mana_cost('ludevic\'s test subject', ['1', 'U']).
card_cmc('ludevic\'s test subject', 2).
card_layout('ludevic\'s test subject', 'double-faced').
card_power('ludevic\'s test subject', 0).
card_toughness('ludevic\'s test subject', 3).
card_sides('ludevic\'s test subject', 'ludevic\'s abomination').

% Found in: USG
card_name('lull', 'Lull').
card_type('lull', 'Instant').
card_types('lull', ['Instant']).
card_subtypes('lull', []).
card_colors('lull', ['G']).
card_text('lull', 'Prevent all combat damage that would be dealt this turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('lull', ['1', 'G']).
card_cmc('lull', 2).
card_layout('lull', 'normal').

% Found in: ZEN
card_name('lullmage mentor', 'Lullmage Mentor').
card_type('lullmage mentor', 'Creature — Merfolk Wizard').
card_types('lullmage mentor', ['Creature']).
card_subtypes('lullmage mentor', ['Merfolk', 'Wizard']).
card_colors('lullmage mentor', ['U']).
card_text('lullmage mentor', 'Whenever a spell or ability you control counters a spell, you may put a 1/1 blue Merfolk creature token onto the battlefield.\nTap seven untapped Merfolk you control: Counter target spell.').
card_mana_cost('lullmage mentor', ['1', 'U', 'U']).
card_cmc('lullmage mentor', 3).
card_layout('lullmage mentor', 'normal').
card_power('lullmage mentor', 2).
card_toughness('lullmage mentor', 2).

% Found in: BFZ
card_name('lumbering falls', 'Lumbering Falls').
card_type('lumbering falls', 'Land').
card_types('lumbering falls', ['Land']).
card_subtypes('lumbering falls', []).
card_colors('lumbering falls', []).
card_text('lumbering falls', 'Lumbering Falls enters the battlefield tapped.\n{T}: Add {G} or {U} to your mana pool.\n{2}{G}{U}: Lumbering Falls becomes a 3/3 green and blue Elemental creature with hexproof until end of turn. It\'s still a land.').
card_layout('lumbering falls', 'normal').

% Found in: MMQ
card_name('lumbering satyr', 'Lumbering Satyr').
card_type('lumbering satyr', 'Creature — Satyr Beast').
card_types('lumbering satyr', ['Creature']).
card_subtypes('lumbering satyr', ['Satyr', 'Beast']).
card_colors('lumbering satyr', ['G']).
card_text('lumbering satyr', 'All creatures have forestwalk. (They can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('lumbering satyr', ['2', 'G', 'G']).
card_cmc('lumbering satyr', 4).
card_layout('lumbering satyr', 'normal').
card_power('lumbering satyr', 5).
card_toughness('lumbering satyr', 4).

% Found in: ISD, PC2
card_name('lumberknot', 'Lumberknot').
card_type('lumberknot', 'Creature — Treefolk').
card_types('lumberknot', ['Creature']).
card_subtypes('lumberknot', ['Treefolk']).
card_colors('lumberknot', ['G']).
card_text('lumberknot', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever a creature dies, put a +1/+1 counter on Lumberknot.').
card_mana_cost('lumberknot', ['2', 'G', 'G']).
card_cmc('lumberknot', 4).
card_layout('lumberknot', 'normal').
card_power('lumberknot', 1).
card_toughness('lumberknot', 1).

% Found in: MRD
card_name('lumengrid augur', 'Lumengrid Augur').
card_type('lumengrid augur', 'Creature — Vedalken Wizard').
card_types('lumengrid augur', ['Creature']).
card_subtypes('lumengrid augur', ['Vedalken', 'Wizard']).
card_colors('lumengrid augur', ['U']).
card_text('lumengrid augur', '{1}, {T}: Target player draws a card, then discards a card. If that player discards an artifact card this way, untap Lumengrid Augur.').
card_mana_cost('lumengrid augur', ['3', 'U']).
card_cmc('lumengrid augur', 4).
card_layout('lumengrid augur', 'normal').
card_power('lumengrid augur', 2).
card_toughness('lumengrid augur', 2).

% Found in: SOM
card_name('lumengrid drake', 'Lumengrid Drake').
card_type('lumengrid drake', 'Creature — Drake').
card_types('lumengrid drake', ['Creature']).
card_subtypes('lumengrid drake', ['Drake']).
card_colors('lumengrid drake', ['U']).
card_text('lumengrid drake', 'Flying\nMetalcraft — When Lumengrid Drake enters the battlefield, if you control three or more artifacts, return target creature to its owner\'s hand.').
card_mana_cost('lumengrid drake', ['3', 'U']).
card_cmc('lumengrid drake', 4).
card_layout('lumengrid drake', 'normal').
card_power('lumengrid drake', 2).
card_toughness('lumengrid drake', 2).

% Found in: MBS
card_name('lumengrid gargoyle', 'Lumengrid Gargoyle').
card_type('lumengrid gargoyle', 'Artifact Creature — Gargoyle').
card_types('lumengrid gargoyle', ['Artifact', 'Creature']).
card_subtypes('lumengrid gargoyle', ['Gargoyle']).
card_colors('lumengrid gargoyle', []).
card_text('lumengrid gargoyle', 'Flying').
card_mana_cost('lumengrid gargoyle', ['6']).
card_cmc('lumengrid gargoyle', 6).
card_layout('lumengrid gargoyle', 'normal').
card_power('lumengrid gargoyle', 4).
card_toughness('lumengrid gargoyle', 4).

% Found in: MRD
card_name('lumengrid sentinel', 'Lumengrid Sentinel').
card_type('lumengrid sentinel', 'Creature — Human Wizard').
card_types('lumengrid sentinel', ['Creature']).
card_subtypes('lumengrid sentinel', ['Human', 'Wizard']).
card_colors('lumengrid sentinel', ['U']).
card_text('lumengrid sentinel', 'Flying\nWhenever an artifact enters the battlefield under your control, you may tap target permanent.').
card_mana_cost('lumengrid sentinel', ['2', 'U']).
card_cmc('lumengrid sentinel', 3).
card_layout('lumengrid sentinel', 'normal').
card_power('lumengrid sentinel', 1).
card_toughness('lumengrid sentinel', 2).

% Found in: 10E, 9ED, MRD
card_name('lumengrid warden', 'Lumengrid Warden').
card_type('lumengrid warden', 'Creature — Human Wizard').
card_types('lumengrid warden', ['Creature']).
card_subtypes('lumengrid warden', ['Human', 'Wizard']).
card_colors('lumengrid warden', ['U']).
card_text('lumengrid warden', '').
card_mana_cost('lumengrid warden', ['1', 'U']).
card_cmc('lumengrid warden', 2).
card_layout('lumengrid warden', 'normal').
card_power('lumengrid warden', 1).
card_toughness('lumengrid warden', 3).

% Found in: ZEN
card_name('luminarch ascension', 'Luminarch Ascension').
card_type('luminarch ascension', 'Enchantment').
card_types('luminarch ascension', ['Enchantment']).
card_subtypes('luminarch ascension', []).
card_colors('luminarch ascension', ['W']).
card_text('luminarch ascension', 'At the beginning of each opponent\'s end step, if you didn\'t lose life this turn, you may put a quest counter on Luminarch Ascension. (Damage causes loss of life.)\n{1}{W}: Put a 4/4 white Angel creature token with flying onto the battlefield. Activate this ability only if Luminarch Ascension has four or more quest counters on it.').
card_mana_cost('luminarch ascension', ['1', 'W']).
card_cmc('luminarch ascension', 2).
card_layout('luminarch ascension', 'normal').

% Found in: GTC
card_name('luminate primordial', 'Luminate Primordial').
card_type('luminate primordial', 'Creature — Avatar').
card_types('luminate primordial', ['Creature']).
card_subtypes('luminate primordial', ['Avatar']).
card_colors('luminate primordial', ['W']).
card_text('luminate primordial', 'Vigilance\nWhen Luminate Primordial enters the battlefield, for each opponent, exile up to one target creature that player controls and that player gains life equal to its power.').
card_mana_cost('luminate primordial', ['5', 'W', 'W']).
card_cmc('luminate primordial', 7).
card_layout('luminate primordial', 'normal').
card_power('luminate primordial', 4).
card_toughness('luminate primordial', 7).

% Found in: 10E, CSP
card_name('luminesce', 'Luminesce').
card_type('luminesce', 'Instant').
card_types('luminesce', ['Instant']).
card_subtypes('luminesce', []).
card_colors('luminesce', ['W']).
card_text('luminesce', 'Prevent all damage that black sources and red sources would deal this turn.').
card_mana_cost('luminesce', ['W']).
card_cmc('luminesce', 1).
card_layout('luminesce', 'normal').

% Found in: MOR
card_name('luminescent rain', 'Luminescent Rain').
card_type('luminescent rain', 'Instant').
card_types('luminescent rain', ['Instant']).
card_subtypes('luminescent rain', []).
card_colors('luminescent rain', ['G']).
card_text('luminescent rain', 'Choose a creature type. You gain 2 life for each permanent you control of that type.').
card_mana_cost('luminescent rain', ['2', 'G']).
card_cmc('luminescent rain', 3).
card_layout('luminescent rain', 'normal').

% Found in: DD3_DVD, DDC, MRD
card_name('luminous angel', 'Luminous Angel').
card_type('luminous angel', 'Creature — Angel').
card_types('luminous angel', ['Creature']).
card_subtypes('luminous angel', ['Angel']).
card_colors('luminous angel', ['W']).
card_text('luminous angel', 'Flying\nAt the beginning of your upkeep, you may put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('luminous angel', ['4', 'W', 'W', 'W']).
card_cmc('luminous angel', 7).
card_layout('luminous angel', 'normal').
card_power('luminous angel', 4).
card_toughness('luminous angel', 4).

% Found in: ODY
card_name('luminous guardian', 'Luminous Guardian').
card_type('luminous guardian', 'Creature — Human Nomad').
card_types('luminous guardian', ['Creature']).
card_subtypes('luminous guardian', ['Human', 'Nomad']).
card_colors('luminous guardian', ['W']).
card_text('luminous guardian', '{W}: Luminous Guardian gets +0/+1 until end of turn.\n{2}: Luminous Guardian can block an additional creature this turn.').
card_mana_cost('luminous guardian', ['3', 'W']).
card_cmc('luminous guardian', 4).
card_layout('luminous guardian', 'normal').
card_power('luminous guardian', 1).
card_toughness('luminous guardian', 4).

% Found in: ROE
card_name('luminous wake', 'Luminous Wake').
card_type('luminous wake', 'Enchantment — Aura').
card_types('luminous wake', ['Enchantment']).
card_subtypes('luminous wake', ['Aura']).
card_colors('luminous wake', ['W']).
card_text('luminous wake', 'Enchant creature\nWhenever enchanted creature attacks or blocks, you gain 4 life.').
card_mana_cost('luminous wake', ['2', 'W']).
card_cmc('luminous wake', 3).
card_layout('luminous wake', 'normal').

% Found in: FUT
card_name('lumithread field', 'Lumithread Field').
card_type('lumithread field', 'Enchantment').
card_types('lumithread field', ['Enchantment']).
card_subtypes('lumithread field', []).
card_colors('lumithread field', ['W']).
card_text('lumithread field', 'Creatures you control get +0/+1.\nMorph {1}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('lumithread field', ['1', 'W']).
card_cmc('lumithread field', 2).
card_layout('lumithread field', 'normal').

% Found in: 5DN
card_name('lunar avenger', 'Lunar Avenger').
card_type('lunar avenger', 'Artifact Creature — Golem').
card_types('lunar avenger', ['Artifact', 'Creature']).
card_subtypes('lunar avenger', ['Golem']).
card_colors('lunar avenger', []).
card_text('lunar avenger', 'Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.)\nRemove a +1/+1 counter from Lunar Avenger: Lunar Avenger gains your choice of flying, first strike, or haste until end of turn.').
card_mana_cost('lunar avenger', ['7']).
card_cmc('lunar avenger', 7).
card_layout('lunar avenger', 'normal').
card_power('lunar avenger', 2).
card_toughness('lunar avenger', 2).

% Found in: AVR
card_name('lunar mystic', 'Lunar Mystic').
card_type('lunar mystic', 'Creature — Human Wizard').
card_types('lunar mystic', ['Creature']).
card_subtypes('lunar mystic', ['Human', 'Wizard']).
card_colors('lunar mystic', ['U']).
card_text('lunar mystic', 'Whenever you cast an instant spell, you may pay {1}. If you do, draw a card.').
card_mana_cost('lunar mystic', ['2', 'U', 'U']).
card_cmc('lunar mystic', 4).
card_layout('lunar mystic', 'normal').
card_power('lunar mystic', 2).
card_toughness('lunar mystic', 2).

% Found in: MMQ
card_name('lunge', 'Lunge').
card_type('lunge', 'Instant').
card_types('lunge', ['Instant']).
card_subtypes('lunge', []).
card_colors('lunge', ['R']).
card_text('lunge', 'Lunge deals 2 damage to target creature and 2 damage to target player.').
card_mana_cost('lunge', ['2', 'R']).
card_cmc('lunge', 3).
card_layout('lunge', 'normal').

% Found in: MOR
card_name('lunk errant', 'Lunk Errant').
card_type('lunk errant', 'Creature — Giant Warrior').
card_types('lunk errant', ['Creature']).
card_subtypes('lunk errant', ['Giant', 'Warrior']).
card_colors('lunk errant', ['R']).
card_text('lunk errant', 'Whenever Lunk Errant attacks alone, it gets +1/+1 and gains trample until end of turn.').
card_mana_cost('lunk errant', ['5', 'R']).
card_cmc('lunk errant', 6).
card_layout('lunk errant', 'normal').
card_power('lunk errant', 4).
card_toughness('lunk errant', 4).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, CHK, ICE, LEA, LEB, M12, MMQ
card_name('lure', 'Lure').
card_type('lure', 'Enchantment — Aura').
card_types('lure', ['Enchantment']).
card_subtypes('lure', ['Aura']).
card_colors('lure', ['G']).
card_text('lure', 'Enchant creature\nAll creatures able to block enchanted creature do so.').
card_mana_cost('lure', ['1', 'G', 'G']).
card_cmc('lure', 3).
card_layout('lure', 'normal').

% Found in: MIR
card_name('lure of prey', 'Lure of Prey').
card_type('lure of prey', 'Instant').
card_types('lure of prey', ['Instant']).
card_subtypes('lure of prey', []).
card_colors('lure of prey', ['G']).
card_text('lure of prey', 'Cast Lure of Prey only if an opponent cast a creature spell this turn.\nYou may put a green creature card from your hand onto the battlefield.').
card_mana_cost('lure of prey', ['2', 'G', 'G']).
card_cmc('lure of prey', 4).
card_layout('lure of prey', 'normal').
card_reserved('lure of prey').

% Found in: SHM
card_name('lurebound scarecrow', 'Lurebound Scarecrow').
card_type('lurebound scarecrow', 'Artifact Creature — Scarecrow').
card_types('lurebound scarecrow', ['Artifact', 'Creature']).
card_subtypes('lurebound scarecrow', ['Scarecrow']).
card_colors('lurebound scarecrow', []).
card_text('lurebound scarecrow', 'As Lurebound Scarecrow enters the battlefield, choose a color.\nWhen you control no permanents of the chosen color, sacrifice Lurebound Scarecrow.').
card_mana_cost('lurebound scarecrow', ['3']).
card_cmc('lurebound scarecrow', 3).
card_layout('lurebound scarecrow', 'normal').
card_power('lurebound scarecrow', 4).
card_toughness('lurebound scarecrow', 4).

% Found in: DRK
card_name('lurker', 'Lurker').
card_type('lurker', 'Creature — Beast').
card_types('lurker', ['Creature']).
card_subtypes('lurker', ['Beast']).
card_colors('lurker', ['G']).
card_text('lurker', 'Lurker can\'t be the target of spells unless it attacked or blocked this turn.').
card_mana_cost('lurker', ['2', 'G']).
card_cmc('lurker', 3).
card_layout('lurker', 'normal').
card_power('lurker', 2).
card_toughness('lurker', 3).
card_reserved('lurker').

% Found in: DTK
card_name('lurking arynx', 'Lurking Arynx').
card_type('lurking arynx', 'Creature — Cat Beast').
card_types('lurking arynx', ['Creature']).
card_subtypes('lurking arynx', ['Cat', 'Beast']).
card_colors('lurking arynx', ['G']).
card_text('lurking arynx', 'Formidable — {2}{G}: Target creature blocks Lurking Arynx this turn if able. Activate this ability only if creatures you control have total power 8 or greater.').
card_mana_cost('lurking arynx', ['4', 'G']).
card_cmc('lurking arynx', 5).
card_layout('lurking arynx', 'normal').
card_power('lurking arynx', 3).
card_toughness('lurking arynx', 5).

% Found in: CNS
card_name('lurking automaton', 'Lurking Automaton').
card_type('lurking automaton', 'Artifact Creature — Construct').
card_types('lurking automaton', ['Artifact', 'Creature']).
card_subtypes('lurking automaton', ['Construct']).
card_colors('lurking automaton', []).
card_text('lurking automaton', 'Reveal Lurking Automaton as you draft it and note how many cards you\'ve drafted this draft round, including Lurking Automaton.\nLurking Automaton enters the battlefield with X +1/+1 counters on it, where X is the highest number you noted for cards named Lurking Automaton.').
card_mana_cost('lurking automaton', ['5']).
card_cmc('lurking automaton', 5).
card_layout('lurking automaton', 'normal').
card_power('lurking automaton', 0).
card_toughness('lurking automaton', 0).

% Found in: M12
card_name('lurking crocodile', 'Lurking Crocodile').
card_type('lurking crocodile', 'Creature — Crocodile').
card_types('lurking crocodile', ['Creature']).
card_subtypes('lurking crocodile', ['Crocodile']).
card_colors('lurking crocodile', ['G']).
card_text('lurking crocodile', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nIslandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('lurking crocodile', ['2', 'G']).
card_cmc('lurking crocodile', 3).
card_layout('lurking crocodile', 'normal').
card_power('lurking crocodile', 2).
card_toughness('lurking crocodile', 2).

% Found in: USG, VMA
card_name('lurking evil', 'Lurking Evil').
card_type('lurking evil', 'Enchantment').
card_types('lurking evil', ['Enchantment']).
card_subtypes('lurking evil', []).
card_colors('lurking evil', ['B']).
card_text('lurking evil', 'Pay half your life, rounded up: Lurking Evil becomes a 4/4 Horror creature with flying.').
card_mana_cost('lurking evil', ['B', 'B', 'B']).
card_cmc('lurking evil', 3).
card_layout('lurking evil', 'normal').

% Found in: RAV
card_name('lurking informant', 'Lurking Informant').
card_type('lurking informant', 'Creature — Human Rogue').
card_types('lurking informant', ['Creature']).
card_subtypes('lurking informant', ['Human', 'Rogue']).
card_colors('lurking informant', ['U', 'B']).
card_text('lurking informant', '({U/B} can be paid with either {U} or {B}.)\n{2}, {T}: Look at the top card of target player\'s library. You may put that card into that player\'s graveyard.').
card_mana_cost('lurking informant', ['1', 'U/B']).
card_cmc('lurking informant', 2).
card_layout('lurking informant', 'normal').
card_power('lurking informant', 1).
card_toughness('lurking informant', 2).

% Found in: UDS
card_name('lurking jackals', 'Lurking Jackals').
card_type('lurking jackals', 'Enchantment').
card_types('lurking jackals', ['Enchantment']).
card_subtypes('lurking jackals', []).
card_colors('lurking jackals', ['B']).
card_text('lurking jackals', 'When an opponent has 10 or less life, if Lurking Jackals is an enchantment, it becomes a 3/2 Hound creature.').
card_mana_cost('lurking jackals', ['B']).
card_cmc('lurking jackals', 1).
card_layout('lurking jackals', 'normal').

% Found in: PO2
card_name('lurking nightstalker', 'Lurking Nightstalker').
card_type('lurking nightstalker', 'Creature — Nightstalker').
card_types('lurking nightstalker', ['Creature']).
card_subtypes('lurking nightstalker', ['Nightstalker']).
card_colors('lurking nightstalker', ['B']).
card_text('lurking nightstalker', 'Whenever Lurking Nightstalker attacks, it gets +2/+0 until end of turn.').
card_mana_cost('lurking nightstalker', ['B', 'B']).
card_cmc('lurking nightstalker', 2).
card_layout('lurking nightstalker', 'normal').
card_power('lurking nightstalker', 1).
card_toughness('lurking nightstalker', 1).

% Found in: M10
card_name('lurking predators', 'Lurking Predators').
card_type('lurking predators', 'Enchantment').
card_types('lurking predators', ['Enchantment']).
card_subtypes('lurking predators', []).
card_colors('lurking predators', ['G']).
card_text('lurking predators', 'Whenever an opponent casts a spell, reveal the top card of your library. If it\'s a creature card, put it onto the battlefield. Otherwise, you may put that card on the bottom of your library.').
card_mana_cost('lurking predators', ['4', 'G', 'G']).
card_cmc('lurking predators', 6).
card_layout('lurking predators', 'normal').

% Found in: ULG
card_name('lurking skirge', 'Lurking Skirge').
card_type('lurking skirge', 'Enchantment').
card_types('lurking skirge', ['Enchantment']).
card_subtypes('lurking skirge', []).
card_colors('lurking skirge', ['B']).
card_text('lurking skirge', 'When a creature is put into an opponent\'s graveyard from the battlefield, if Lurking Skirge is an enchantment, Lurking Skirge becomes a 3/2 Imp creature with flying.').
card_mana_cost('lurking skirge', ['1', 'B']).
card_cmc('lurking skirge', 2).
card_layout('lurking skirge', 'normal').

% Found in: ALA
card_name('lush growth', 'Lush Growth').
card_type('lush growth', 'Enchantment — Aura').
card_types('lush growth', ['Enchantment']).
card_subtypes('lush growth', ['Aura']).
card_colors('lush growth', ['G']).
card_text('lush growth', 'Enchant land\nEnchanted land is a Mountain, Forest, and Plains.').
card_mana_cost('lush growth', ['G']).
card_cmc('lush growth', 1).
card_layout('lush growth', 'normal').

% Found in: ROE
card_name('lust for war', 'Lust for War').
card_type('lust for war', 'Enchantment — Aura').
card_types('lust for war', ['Enchantment']).
card_subtypes('lust for war', ['Aura']).
card_colors('lust for war', ['R']).
card_text('lust for war', 'Enchant creature\nWhenever enchanted creature becomes tapped, Lust for War deals 3 damage to that creature\'s controller.\nEnchanted creature attacks each turn if able.').
card_mana_cost('lust for war', ['2', 'R']).
card_cmc('lust for war', 3).
card_layout('lust for war', 'normal').

% Found in: SOM
card_name('lux cannon', 'Lux Cannon').
card_type('lux cannon', 'Artifact').
card_types('lux cannon', ['Artifact']).
card_subtypes('lux cannon', []).
card_colors('lux cannon', []).
card_text('lux cannon', '{T}: Put a charge counter on Lux Cannon.\n{T}, Remove three charge counters from Lux Cannon: Destroy target permanent.').
card_mana_cost('lux cannon', ['4']).
card_cmc('lux cannon', 4).
card_layout('lux cannon', 'normal').

% Found in: DGM
card_name('lyev decree', 'Lyev Decree').
card_type('lyev decree', 'Sorcery').
card_types('lyev decree', ['Sorcery']).
card_subtypes('lyev decree', []).
card_colors('lyev decree', ['W']).
card_text('lyev decree', 'Detain up to two target creatures your opponents control. (Until your next turn, those creatures can\'t attack or block and their activated abilities can\'t be activated.)').
card_mana_cost('lyev decree', ['1', 'W']).
card_cmc('lyev decree', 2).
card_layout('lyev decree', 'normal').

% Found in: RTR
card_name('lyev skyknight', 'Lyev Skyknight').
card_type('lyev skyknight', 'Creature — Human Knight').
card_types('lyev skyknight', ['Creature']).
card_subtypes('lyev skyknight', ['Human', 'Knight']).
card_colors('lyev skyknight', ['W', 'U']).
card_text('lyev skyknight', 'Flying\nWhen Lyev Skyknight enters the battlefield, detain target nonland permanent an opponent controls. (Until your next turn, that permanent can\'t attack or block and its activated abilities can\'t be activated.)').
card_mana_cost('lyev skyknight', ['1', 'W', 'U']).
card_cmc('lyev skyknight', 3).
card_layout('lyev skyknight', 'normal').
card_power('lyev skyknight', 3).
card_toughness('lyev skyknight', 1).

% Found in: FUT
card_name('lymph sliver', 'Lymph Sliver').
card_type('lymph sliver', 'Creature — Sliver').
card_types('lymph sliver', ['Creature']).
card_subtypes('lymph sliver', ['Sliver']).
card_colors('lymph sliver', ['W']).
card_text('lymph sliver', 'All Sliver creatures have absorb 1. (If a source would deal damage to a Sliver, prevent 1 of that damage.)').
card_mana_cost('lymph sliver', ['4', 'W']).
card_cmc('lymph sliver', 5).
card_layout('lymph sliver', 'normal').
card_power('lymph sliver', 3).
card_toughness('lymph sliver', 3).

% Found in: VAN
card_name('lyna', 'Lyna').
card_type('lyna', 'Vanguard').
card_types('lyna', ['Vanguard']).
card_subtypes('lyna', []).
card_colors('lyna', []).
card_text('lyna', 'Creatures you control have shadow. (They can block and be blocked only by creatures with shadow.)').
card_layout('lyna', 'vanguard').

% Found in: PO2, S99
card_name('lynx', 'Lynx').
card_type('lynx', 'Creature — Cat').
card_types('lynx', ['Creature']).
card_subtypes('lynx', ['Cat']).
card_colors('lynx', ['G']).
card_text('lynx', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('lynx', ['1', 'G']).
card_cmc('lynx', 2).
card_layout('lynx', 'normal').
card_power('lynx', 2).
card_toughness('lynx', 1).

% Found in: MOR
card_name('lys alana bowmaster', 'Lys Alana Bowmaster').
card_type('lys alana bowmaster', 'Creature — Elf Archer').
card_types('lys alana bowmaster', ['Creature']).
card_subtypes('lys alana bowmaster', ['Elf', 'Archer']).
card_colors('lys alana bowmaster', ['G']).
card_text('lys alana bowmaster', 'Reach (This creature can block creatures with flying.)\nWhenever you cast an Elf spell, you may have Lys Alana Bowmaster deal 2 damage to target creature with flying.').
card_mana_cost('lys alana bowmaster', ['2', 'G']).
card_cmc('lys alana bowmaster', 3).
card_layout('lys alana bowmaster', 'normal').
card_power('lys alana bowmaster', 2).
card_toughness('lys alana bowmaster', 2).

% Found in: C14, DD3_EVG, DPA, EVG, LRW
card_name('lys alana huntmaster', 'Lys Alana Huntmaster').
card_type('lys alana huntmaster', 'Creature — Elf Warrior').
card_types('lys alana huntmaster', ['Creature']).
card_subtypes('lys alana huntmaster', ['Elf', 'Warrior']).
card_colors('lys alana huntmaster', ['G']).
card_text('lys alana huntmaster', 'Whenever you cast an Elf spell, you may put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_mana_cost('lys alana huntmaster', ['2', 'G', 'G']).
card_cmc('lys alana huntmaster', 4).
card_layout('lys alana huntmaster', 'normal').
card_power('lys alana huntmaster', 3).
card_toughness('lys alana huntmaster', 3).

% Found in: LRW
card_name('lys alana scarblade', 'Lys Alana Scarblade').
card_type('lys alana scarblade', 'Creature — Elf Assassin').
card_types('lys alana scarblade', ['Creature']).
card_subtypes('lys alana scarblade', ['Elf', 'Assassin']).
card_colors('lys alana scarblade', ['B']).
card_text('lys alana scarblade', '{T}, Discard an Elf card: Target creature gets -X/-X until end of turn, where X is the number of Elves you control.').
card_mana_cost('lys alana scarblade', ['2', 'B']).
card_cmc('lys alana scarblade', 3).
card_layout('lys alana scarblade', 'normal').
card_power('lys alana scarblade', 1).
card_toughness('lys alana scarblade', 1).

% Found in: DIS
card_name('lyzolda, the blood witch', 'Lyzolda, the Blood Witch').
card_type('lyzolda, the blood witch', 'Legendary Creature — Human Cleric').
card_types('lyzolda, the blood witch', ['Creature']).
card_subtypes('lyzolda, the blood witch', ['Human', 'Cleric']).
card_supertypes('lyzolda, the blood witch', ['Legendary']).
card_colors('lyzolda, the blood witch', ['B', 'R']).
card_text('lyzolda, the blood witch', '{2}, Sacrifice a creature: Lyzolda, the Blood Witch deals 2 damage to target creature or player if the sacrificed creature was red. Draw a card if the sacrificed creature was black.').
card_mana_cost('lyzolda, the blood witch', ['1', 'B', 'R']).
card_cmc('lyzolda, the blood witch', 3).
card_layout('lyzolda, the blood witch', 'normal').
card_power('lyzolda, the blood witch', 3).
card_toughness('lyzolda, the blood witch', 1).

% Found in: VAN
card_name('lyzolda, the blood witch avatar', 'Lyzolda, the Blood Witch Avatar').
card_type('lyzolda, the blood witch avatar', 'Vanguard').
card_types('lyzolda, the blood witch avatar', ['Vanguard']).
card_subtypes('lyzolda, the blood witch avatar', []).
card_colors('lyzolda, the blood witch avatar', []).
card_text('lyzolda, the blood witch avatar', 'Hellbent — As long as you have no cards in hand, if a source you control would deal damage to a creature or player, it deals double that damage to that creature or player instead.\nHellbent — At the beginning of your end step, if you have no cards in hand, each of your opponents discards a card.').
card_layout('lyzolda, the blood witch avatar', 'vanguard').

