% Card-specific data

% Found in: ARC
card_name('a display of my dark power', 'A Display of My Dark Power').
card_type('a display of my dark power', 'Scheme').
card_types('a display of my dark power', ['Scheme']).
card_subtypes('a display of my dark power', []).
card_colors('a display of my dark power', []).
card_text('a display of my dark power', 'When you set this scheme in motion, until your next turn, whenever a player taps a land for mana, that player adds one mana to his or her mana pool of any type that land produced.').
card_layout('a display of my dark power', 'scheme').

% Found in: TMP
card_name('abandon hope', 'Abandon Hope').
card_type('abandon hope', 'Sorcery').
card_types('abandon hope', ['Sorcery']).
card_subtypes('abandon hope', []).
card_colors('abandon hope', ['B']).
card_text('abandon hope', 'As an additional cost to cast Abandon Hope, discard X cards.\nLook at target opponent\'s hand and choose X cards from it. That player discards those cards.').
card_mana_cost('abandon hope', ['X', '1', 'B']).
card_cmc('abandon hope', 2).
card_layout('abandon hope', 'normal').

% Found in: ODY
card_name('abandoned outpost', 'Abandoned Outpost').
card_type('abandoned outpost', 'Land').
card_types('abandoned outpost', ['Land']).
card_subtypes('abandoned outpost', []).
card_colors('abandoned outpost', []).
card_text('abandoned outpost', 'Abandoned Outpost enters the battlefield tapped.\n{T}: Add {W} to your mana pool.\n{T}, Sacrifice Abandoned Outpost: Add one mana of any color to your mana pool.').
card_layout('abandoned outpost', 'normal').

% Found in: ISD
card_name('abattoir ghoul', 'Abattoir Ghoul').
card_type('abattoir ghoul', 'Creature — Zombie').
card_types('abattoir ghoul', ['Creature']).
card_subtypes('abattoir ghoul', ['Zombie']).
card_colors('abattoir ghoul', ['B']).
card_text('abattoir ghoul', 'First strike\nWhenever a creature dealt damage by Abattoir Ghoul this turn dies, you gain life equal to that creature\'s toughness.').
card_mana_cost('abattoir ghoul', ['3', 'B']).
card_cmc('abattoir ghoul', 4).
card_layout('abattoir ghoul', 'normal').
card_power('abattoir ghoul', 3).
card_toughness('abattoir ghoul', 2).

% Found in: 5ED, HML, ME2
card_name('abbey gargoyles', 'Abbey Gargoyles').
card_type('abbey gargoyles', 'Creature — Gargoyle').
card_types('abbey gargoyles', ['Creature']).
card_subtypes('abbey gargoyles', ['Gargoyle']).
card_colors('abbey gargoyles', ['W']).
card_text('abbey gargoyles', 'Flying, protection from red').
card_mana_cost('abbey gargoyles', ['2', 'W', 'W', 'W']).
card_cmc('abbey gargoyles', 5).
card_layout('abbey gargoyles', 'normal').
card_power('abbey gargoyles', 3).
card_toughness('abbey gargoyles', 4).

% Found in: ISD
card_name('abbey griffin', 'Abbey Griffin').
card_type('abbey griffin', 'Creature — Griffin').
card_types('abbey griffin', ['Creature']).
card_subtypes('abbey griffin', ['Griffin']).
card_colors('abbey griffin', ['W']).
card_text('abbey griffin', 'Flying, vigilance').
card_mana_cost('abbey griffin', ['3', 'W']).
card_cmc('abbey griffin', 4).
card_layout('abbey griffin', 'normal').
card_power('abbey griffin', 2).
card_toughness('abbey griffin', 2).

% Found in: HML
card_name('abbey matron', 'Abbey Matron').
card_type('abbey matron', 'Creature — Human Cleric').
card_types('abbey matron', ['Creature']).
card_subtypes('abbey matron', ['Human', 'Cleric']).
card_colors('abbey matron', ['W']).
card_text('abbey matron', '{W}, {T}: Abbey Matron gets +0/+3 until end of turn.').
card_mana_cost('abbey matron', ['2', 'W']).
card_cmc('abbey matron', 3).
card_layout('abbey matron', 'normal').
card_power('abbey matron', 1).
card_toughness('abbey matron', 3).

% Found in: ORI
card_name('abbot of keral keep', 'Abbot of Keral Keep').
card_type('abbot of keral keep', 'Creature — Human Monk').
card_types('abbot of keral keep', ['Creature']).
card_subtypes('abbot of keral keep', ['Human', 'Monk']).
card_colors('abbot of keral keep', ['R']).
card_text('abbot of keral keep', 'Prowess (Whenever you cast a noncreature spell, this creature gets +1/+1 until end of turn.)\nWhen Abbot of Keral Keep enters the battlefield, exile the top card of your library. Until end of turn, you may play that card.').
card_mana_cost('abbot of keral keep', ['1', 'R']).
card_cmc('abbot of keral keep', 2).
card_layout('abbot of keral keep', 'normal').
card_power('abbot of keral keep', 2).
card_toughness('abbot of keral keep', 1).

% Found in: 6ED, WTH
card_name('abduction', 'Abduction').
card_type('abduction', 'Enchantment — Aura').
card_types('abduction', ['Enchantment']).
card_subtypes('abduction', ['Aura']).
card_colors('abduction', ['U']).
card_text('abduction', 'Enchant creature\nWhen Abduction enters the battlefield, untap enchanted creature.\nYou control enchanted creature.\nWhen enchanted creature dies, return that card to the battlefield under its owner\'s control.').
card_mana_cost('abduction', ['2', 'U', 'U']).
card_cmc('abduction', 4).
card_layout('abduction', 'normal').

% Found in: WTH
card_name('abeyance', 'Abeyance').
card_type('abeyance', 'Instant').
card_types('abeyance', ['Instant']).
card_subtypes('abeyance', []).
card_colors('abeyance', ['W']).
card_text('abeyance', 'Until end of turn, target player can\'t cast instant or sorcery spells, and that player can\'t activate abilities that aren\'t mana abilities.\nDraw a card.').
card_mana_cost('abeyance', ['1', 'W']).
card_cmc('abeyance', 2).
card_layout('abeyance', 'normal').
card_reserved('abeyance').

% Found in: THS, pPRE
card_name('abhorrent overlord', 'Abhorrent Overlord').
card_type('abhorrent overlord', 'Creature — Demon').
card_types('abhorrent overlord', ['Creature']).
card_subtypes('abhorrent overlord', ['Demon']).
card_colors('abhorrent overlord', ['B']).
card_text('abhorrent overlord', 'Flying\nWhen Abhorrent Overlord enters the battlefield, put a number of 1/1 black Harpy creature tokens with flying onto the battlefield equal to your devotion to black. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)\nAt the beginning of your upkeep, sacrifice a creature.').
card_mana_cost('abhorrent overlord', ['5', 'B', 'B']).
card_cmc('abhorrent overlord', 7).
card_layout('abhorrent overlord', 'normal').
card_power('abhorrent overlord', 6).
card_toughness('abhorrent overlord', 6).

% Found in: WTH
card_name('abjure', 'Abjure').
card_type('abjure', 'Instant').
card_types('abjure', ['Instant']).
card_subtypes('abjure', []).
card_colors('abjure', ['U']).
card_text('abjure', 'As an additional cost to cast Abjure, sacrifice a blue permanent.\nCounter target spell.').
card_mana_cost('abjure', ['U']).
card_cmc('abjure', 1).
card_layout('abjure', 'normal').

% Found in: DDF, PCY
card_name('abolish', 'Abolish').
card_type('abolish', 'Instant').
card_types('abolish', ['Instant']).
card_subtypes('abolish', []).
card_colors('abolish', ['W']).
card_text('abolish', 'You may discard a Plains card rather than pay Abolish\'s mana cost.\nDestroy target artifact or enchantment.').
card_mana_cost('abolish', ['1', 'W', 'W']).
card_cmc('abolish', 3).
card_layout('abolish', 'normal').

% Found in: 4ED, LEG
card_name('abomination', 'Abomination').
card_type('abomination', 'Creature — Horror').
card_types('abomination', ['Creature']).
card_subtypes('abomination', ['Horror']).
card_colors('abomination', ['B']).
card_text('abomination', 'Whenever Abomination blocks or becomes blocked by a green or white creature, destroy that creature at end of combat.').
card_mana_cost('abomination', ['3', 'B', 'B']).
card_cmc('abomination', 5).
card_layout('abomination', 'normal').
card_power('abomination', 2).
card_toughness('abomination', 6).

% Found in: KTK
card_name('abomination of gudul', 'Abomination of Gudul').
card_type('abomination of gudul', 'Creature — Horror').
card_types('abomination of gudul', ['Creature']).
card_subtypes('abomination of gudul', ['Horror']).
card_colors('abomination of gudul', ['U', 'B', 'G']).
card_text('abomination of gudul', 'Flying\nWhenever Abomination of Gudul deals combat damage to a player, you may draw a card. If you do, discard a card.\nMorph {2}{B}{G}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('abomination of gudul', ['3', 'B', 'G', 'U']).
card_cmc('abomination of gudul', 6).
card_layout('abomination of gudul', 'normal').
card_power('abomination of gudul', 3).
card_toughness('abomination of gudul', 4).

% Found in: WTH
card_name('aboroth', 'Aboroth').
card_type('aboroth', 'Creature — Elemental').
card_types('aboroth', ['Creature']).
card_subtypes('aboroth', ['Elemental']).
card_colors('aboroth', ['G']).
card_text('aboroth', 'Cumulative upkeep—Put a -1/-1 counter on Aboroth. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('aboroth', ['4', 'G', 'G']).
card_cmc('aboroth', 6).
card_layout('aboroth', 'normal').
card_power('aboroth', 9).
card_toughness('aboroth', 9).
card_reserved('aboroth').

% Found in: ODY
card_name('aboshan\'s desire', 'Aboshan\'s Desire').
card_type('aboshan\'s desire', 'Enchantment — Aura').
card_types('aboshan\'s desire', ['Enchantment']).
card_subtypes('aboshan\'s desire', ['Aura']).
card_colors('aboshan\'s desire', ['U']).
card_text('aboshan\'s desire', 'Enchant creature\nEnchanted creature has flying.\nThreshold — Enchanted creature has shroud as long as seven or more cards are in your graveyard. (It can\'t be the target of spells or abilities.)').
card_mana_cost('aboshan\'s desire', ['U']).
card_cmc('aboshan\'s desire', 1).
card_layout('aboshan\'s desire', 'normal').

% Found in: ODY
card_name('aboshan, cephalid emperor', 'Aboshan, Cephalid Emperor').
card_type('aboshan, cephalid emperor', 'Legendary Creature — Cephalid').
card_types('aboshan, cephalid emperor', ['Creature']).
card_subtypes('aboshan, cephalid emperor', ['Cephalid']).
card_supertypes('aboshan, cephalid emperor', ['Legendary']).
card_colors('aboshan, cephalid emperor', ['U']).
card_text('aboshan, cephalid emperor', 'Tap an untapped Cephalid you control: Tap target permanent.\n{U}{U}{U}: Tap all creatures without flying.').
card_mana_cost('aboshan, cephalid emperor', ['4', 'U', 'U']).
card_cmc('aboshan, cephalid emperor', 6).
card_layout('aboshan, cephalid emperor', 'normal').
card_power('aboshan, cephalid emperor', 3).
card_toughness('aboshan, cephalid emperor', 3).

% Found in: ULG
card_name('about face', 'About Face').
card_type('about face', 'Instant').
card_types('about face', ['Instant']).
card_subtypes('about face', []).
card_colors('about face', ['R']).
card_text('about face', 'Switch target creature\'s power and toughness until end of turn.').
card_mana_cost('about face', ['R']).
card_cmc('about face', 1).
card_layout('about face', 'normal').

% Found in: RTR
card_name('abrupt decay', 'Abrupt Decay').
card_type('abrupt decay', 'Instant').
card_types('abrupt decay', ['Instant']).
card_subtypes('abrupt decay', []).
card_colors('abrupt decay', ['B', 'G']).
card_text('abrupt decay', 'Abrupt Decay can\'t be countered by spells or abilities.\nDestroy target nonland permanent with converted mana cost 3 or less.').
card_mana_cost('abrupt decay', ['B', 'G']).
card_cmc('abrupt decay', 2).
card_layout('abrupt decay', 'normal').

% Found in: USG
card_name('absolute grace', 'Absolute Grace').
card_type('absolute grace', 'Enchantment').
card_types('absolute grace', ['Enchantment']).
card_subtypes('absolute grace', []).
card_colors('absolute grace', ['W']).
card_text('absolute grace', 'All creatures have protection from black.').
card_mana_cost('absolute grace', ['1', 'W']).
card_cmc('absolute grace', 2).
card_layout('absolute grace', 'normal').

% Found in: USG
card_name('absolute law', 'Absolute Law').
card_type('absolute law', 'Enchantment').
card_types('absolute law', ['Enchantment']).
card_subtypes('absolute law', []).
card_colors('absolute law', ['W']).
card_text('absolute law', 'All creatures have protection from red.').
card_mana_cost('absolute law', ['1', 'W']).
card_cmc('absolute law', 2).
card_layout('absolute law', 'normal').

% Found in: GPT
card_name('absolver thrull', 'Absolver Thrull').
card_type('absolver thrull', 'Creature — Thrull Cleric').
card_types('absolver thrull', ['Creature']).
card_subtypes('absolver thrull', ['Thrull', 'Cleric']).
card_colors('absolver thrull', ['W']).
card_text('absolver thrull', 'Haunt (When this creature dies, exile it haunting target creature.)\nWhen Absolver Thrull enters the battlefield or the creature it haunts dies, destroy target enchantment.').
card_mana_cost('absolver thrull', ['3', 'W']).
card_cmc('absolver thrull', 4).
card_layout('absolver thrull', 'normal').
card_power('absolver thrull', 2).
card_toughness('absolver thrull', 3).

% Found in: INV
card_name('absorb', 'Absorb').
card_type('absorb', 'Instant').
card_types('absorb', ['Instant']).
card_subtypes('absorb', []).
card_colors('absorb', ['W', 'U']).
card_text('absorb', 'Counter target spell. You gain 3 life.').
card_mana_cost('absorb', ['W', 'U', 'U']).
card_cmc('absorb', 3).
card_layout('absorb', 'normal').

% Found in: CON, DDK, MMA
card_name('absorb vis', 'Absorb Vis').
card_type('absorb vis', 'Sorcery').
card_types('absorb vis', ['Sorcery']).
card_subtypes('absorb vis', []).
card_colors('absorb vis', ['B']).
card_text('absorb vis', 'Target player loses 4 life and you gain 4 life.\nBasic landcycling {1}{B} ({1}{B}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('absorb vis', ['6', 'B']).
card_cmc('absorb vis', 7).
card_layout('absorb vis', 'normal').

% Found in: ARN, CHR
card_name('abu ja\'far', 'Abu Ja\'far').
card_type('abu ja\'far', 'Creature — Human').
card_types('abu ja\'far', ['Creature']).
card_subtypes('abu ja\'far', ['Human']).
card_colors('abu ja\'far', ['W']).
card_text('abu ja\'far', 'When Abu Ja\'far dies, destroy all creatures blocking or blocked by it. They can\'t be regenerated.').
card_mana_cost('abu ja\'far', ['W']).
card_cmc('abu ja\'far', 1).
card_layout('abu ja\'far', 'normal').
card_power('abu ja\'far', 0).
card_toughness('abu ja\'far', 1).

% Found in: SOM
card_name('abuna acolyte', 'Abuna Acolyte').
card_type('abuna acolyte', 'Creature — Cat Cleric').
card_types('abuna acolyte', ['Creature']).
card_subtypes('abuna acolyte', ['Cat', 'Cleric']).
card_colors('abuna acolyte', ['W']).
card_text('abuna acolyte', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.\n{T}: Prevent the next 2 damage that would be dealt to target artifact creature this turn.').
card_mana_cost('abuna acolyte', ['1', 'W']).
card_cmc('abuna acolyte', 2).
card_layout('abuna acolyte', 'normal').
card_power('abuna acolyte', 1).
card_toughness('abuna acolyte', 1).

% Found in: 5DN
card_name('abuna\'s chant', 'Abuna\'s Chant').
card_type('abuna\'s chant', 'Instant').
card_types('abuna\'s chant', ['Instant']).
card_subtypes('abuna\'s chant', []).
card_colors('abuna\'s chant', ['W']).
card_text('abuna\'s chant', 'Choose one —\n• You gain 5 life.\n• Prevent the next 5 damage that would be dealt to target creature this turn.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_mana_cost('abuna\'s chant', ['3', 'W']).
card_cmc('abuna\'s chant', 4).
card_layout('abuna\'s chant', 'normal').

% Found in: 10E, USG
card_name('abundance', 'Abundance').
card_type('abundance', 'Enchantment').
card_types('abundance', ['Enchantment']).
card_subtypes('abundance', []).
card_colors('abundance', ['G']).
card_text('abundance', 'If you would draw a card, you may instead choose land or nonland and reveal cards from the top of your library until you reveal a card of the chosen kind. Put that card into your hand and put all other cards revealed this way on the bottom of your library in any order.').
card_mana_cost('abundance', ['2', 'G', 'G']).
card_cmc('abundance', 4).
card_layout('abundance', 'normal').

% Found in: AVR
card_name('abundant growth', 'Abundant Growth').
card_type('abundant growth', 'Enchantment — Aura').
card_types('abundant growth', ['Enchantment']).
card_subtypes('abundant growth', ['Aura']).
card_colors('abundant growth', ['G']).
card_text('abundant growth', 'Enchant land\nWhen Abundant Growth enters the battlefield, draw a card.\nEnchanted land has \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('abundant growth', ['G']).
card_cmc('abundant growth', 1).
card_layout('abundant growth', 'normal').

% Found in: DD3_DVD, DDC, WTH
card_name('abyssal gatekeeper', 'Abyssal Gatekeeper').
card_type('abyssal gatekeeper', 'Creature — Horror').
card_types('abyssal gatekeeper', ['Creature']).
card_subtypes('abyssal gatekeeper', ['Horror']).
card_colors('abyssal gatekeeper', ['B']).
card_text('abyssal gatekeeper', 'When Abyssal Gatekeeper dies, each player sacrifices a creature.').
card_mana_cost('abyssal gatekeeper', ['1', 'B']).
card_cmc('abyssal gatekeeper', 2).
card_layout('abyssal gatekeeper', 'normal').
card_power('abyssal gatekeeper', 1).
card_toughness('abyssal gatekeeper', 1).

% Found in: 7ED, S99, USG
card_name('abyssal horror', 'Abyssal Horror').
card_type('abyssal horror', 'Creature — Horror').
card_types('abyssal horror', ['Creature']).
card_subtypes('abyssal horror', ['Horror']).
card_colors('abyssal horror', ['B']).
card_text('abyssal horror', 'Flying\nWhen Abyssal Horror enters the battlefield, target player discards two cards.').
card_mana_cost('abyssal horror', ['4', 'B', 'B']).
card_cmc('abyssal horror', 6).
card_layout('abyssal horror', 'normal').
card_power('abyssal horror', 2).
card_toughness('abyssal horror', 2).

% Found in: 6ED, MIR
card_name('abyssal hunter', 'Abyssal Hunter').
card_type('abyssal hunter', 'Creature — Human Assassin').
card_types('abyssal hunter', ['Creature']).
card_subtypes('abyssal hunter', ['Human', 'Assassin']).
card_colors('abyssal hunter', ['B']).
card_text('abyssal hunter', '{B}, {T}: Tap target creature. Abyssal Hunter deals damage equal to Abyssal Hunter\'s power to that creature.').
card_mana_cost('abyssal hunter', ['3', 'B']).
card_cmc('abyssal hunter', 4).
card_layout('abyssal hunter', 'normal').
card_power('abyssal hunter', 1).
card_toughness('abyssal hunter', 1).

% Found in: PO2
card_name('abyssal nightstalker', 'Abyssal Nightstalker').
card_type('abyssal nightstalker', 'Creature — Nightstalker').
card_types('abyssal nightstalker', ['Creature']).
card_subtypes('abyssal nightstalker', ['Nightstalker']).
card_colors('abyssal nightstalker', ['B']).
card_text('abyssal nightstalker', 'Whenever Abyssal Nightstalker attacks and isn\'t blocked, defending player discards a card.').
card_mana_cost('abyssal nightstalker', ['3', 'B']).
card_cmc('abyssal nightstalker', 4).
card_layout('abyssal nightstalker', 'normal').
card_power('abyssal nightstalker', 2).
card_toughness('abyssal nightstalker', 2).

% Found in: GPT
card_name('abyssal nocturnus', 'Abyssal Nocturnus').
card_type('abyssal nocturnus', 'Creature — Horror').
card_types('abyssal nocturnus', ['Creature']).
card_subtypes('abyssal nocturnus', ['Horror']).
card_colors('abyssal nocturnus', ['B']).
card_text('abyssal nocturnus', 'Whenever an opponent discards a card, Abyssal Nocturnus gets +2/+2 and gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('abyssal nocturnus', ['1', 'B', 'B']).
card_cmc('abyssal nocturnus', 3).
card_layout('abyssal nocturnus', 'normal').
card_power('abyssal nocturnus', 2).
card_toughness('abyssal nocturnus', 2).

% Found in: C14, WWK
card_name('abyssal persecutor', 'Abyssal Persecutor').
card_type('abyssal persecutor', 'Creature — Demon').
card_types('abyssal persecutor', ['Creature']).
card_subtypes('abyssal persecutor', ['Demon']).
card_colors('abyssal persecutor', ['B']).
card_text('abyssal persecutor', 'Flying, trample\nYou can\'t win the game and your opponents can\'t lose the game.').
card_mana_cost('abyssal persecutor', ['2', 'B', 'B']).
card_cmc('abyssal persecutor', 4).
card_layout('abyssal persecutor', 'normal').
card_power('abyssal persecutor', 6).
card_toughness('abyssal persecutor', 6).

% Found in: 5ED, 6ED, 7ED, 8ED, BRB, DD3_DVD, DDC, DKM, DPA, ICE
card_name('abyssal specter', 'Abyssal Specter').
card_type('abyssal specter', 'Creature — Specter').
card_types('abyssal specter', ['Creature']).
card_subtypes('abyssal specter', ['Specter']).
card_colors('abyssal specter', ['B']).
card_text('abyssal specter', 'Flying\nWhenever Abyssal Specter deals damage to a player, that player discards a card.').
card_mana_cost('abyssal specter', ['2', 'B', 'B']).
card_cmc('abyssal specter', 4).
card_layout('abyssal specter', 'normal').
card_power('abyssal specter', 2).
card_toughness('abyssal specter', 3).

% Found in: FRF
card_name('abzan advantage', 'Abzan Advantage').
card_type('abzan advantage', 'Instant').
card_types('abzan advantage', ['Instant']).
card_subtypes('abzan advantage', []).
card_colors('abzan advantage', ['W']).
card_text('abzan advantage', 'Target player sacrifices an enchantment. Bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('abzan advantage', ['1', 'W']).
card_cmc('abzan advantage', 2).
card_layout('abzan advantage', 'normal').

% Found in: KTK, pPRE
card_name('abzan ascendancy', 'Abzan Ascendancy').
card_type('abzan ascendancy', 'Enchantment').
card_types('abzan ascendancy', ['Enchantment']).
card_subtypes('abzan ascendancy', []).
card_colors('abzan ascendancy', ['W', 'B', 'G']).
card_text('abzan ascendancy', 'When Abzan Ascendancy enters the battlefield, put a +1/+1 counter on each creature you control.\nWhenever a nontoken creature you control dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('abzan ascendancy', ['W', 'B', 'G']).
card_cmc('abzan ascendancy', 3).
card_layout('abzan ascendancy', 'normal').

% Found in: KTK
card_name('abzan banner', 'Abzan Banner').
card_type('abzan banner', 'Artifact').
card_types('abzan banner', ['Artifact']).
card_subtypes('abzan banner', []).
card_colors('abzan banner', []).
card_text('abzan banner', '{T}: Add {W}, {B}, or {G} to your mana pool.\n{W}{B}{G}, {T}, Sacrifice Abzan Banner: Draw a card.').
card_mana_cost('abzan banner', ['3']).
card_cmc('abzan banner', 3).
card_layout('abzan banner', 'normal').

% Found in: KTK
card_name('abzan battle priest', 'Abzan Battle Priest').
card_type('abzan battle priest', 'Creature — Human Cleric').
card_types('abzan battle priest', ['Creature']).
card_subtypes('abzan battle priest', ['Human', 'Cleric']).
card_colors('abzan battle priest', ['W']).
card_text('abzan battle priest', 'Outlast {W} ({W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has lifelink.').
card_mana_cost('abzan battle priest', ['3', 'W']).
card_cmc('abzan battle priest', 4).
card_layout('abzan battle priest', 'normal').
card_power('abzan battle priest', 3).
card_toughness('abzan battle priest', 2).

% Found in: FRF, pFNM
card_name('abzan beastmaster', 'Abzan Beastmaster').
card_type('abzan beastmaster', 'Creature — Hound Shaman').
card_types('abzan beastmaster', ['Creature']).
card_subtypes('abzan beastmaster', ['Hound', 'Shaman']).
card_colors('abzan beastmaster', ['G']).
card_text('abzan beastmaster', 'At the beginning of your upkeep, draw a card if you control the creature with the greatest toughness or tied for the greatest toughness.').
card_mana_cost('abzan beastmaster', ['2', 'G']).
card_cmc('abzan beastmaster', 3).
card_layout('abzan beastmaster', 'normal').
card_power('abzan beastmaster', 2).
card_toughness('abzan beastmaster', 1).

% Found in: KTK
card_name('abzan charm', 'Abzan Charm').
card_type('abzan charm', 'Instant').
card_types('abzan charm', ['Instant']).
card_subtypes('abzan charm', []).
card_colors('abzan charm', ['W', 'B', 'G']).
card_text('abzan charm', 'Choose one —\n• Exile target creature with power 3 or greater.\n• You draw two cards and you lose 2 life.\n• Distribute two +1/+1 counters among one or two target creatures.').
card_mana_cost('abzan charm', ['W', 'B', 'G']).
card_cmc('abzan charm', 3).
card_layout('abzan charm', 'normal').

% Found in: KTK
card_name('abzan falconer', 'Abzan Falconer').
card_type('abzan falconer', 'Creature — Human Soldier').
card_types('abzan falconer', ['Creature']).
card_subtypes('abzan falconer', ['Human', 'Soldier']).
card_colors('abzan falconer', ['W']).
card_text('abzan falconer', 'Outlast {W} ({W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has flying.').
card_mana_cost('abzan falconer', ['2', 'W']).
card_cmc('abzan falconer', 3).
card_layout('abzan falconer', 'normal').
card_power('abzan falconer', 2).
card_toughness('abzan falconer', 3).

% Found in: KTK
card_name('abzan guide', 'Abzan Guide').
card_type('abzan guide', 'Creature — Human Warrior').
card_types('abzan guide', ['Creature']).
card_subtypes('abzan guide', ['Human', 'Warrior']).
card_colors('abzan guide', ['W', 'B', 'G']).
card_text('abzan guide', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nMorph {2}{W}{B}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('abzan guide', ['3', 'W', 'B', 'G']).
card_cmc('abzan guide', 6).
card_layout('abzan guide', 'normal').
card_power('abzan guide', 4).
card_toughness('abzan guide', 4).

% Found in: FRF
card_name('abzan kin-guard', 'Abzan Kin-Guard').
card_type('abzan kin-guard', 'Creature — Human Warrior').
card_types('abzan kin-guard', ['Creature']).
card_subtypes('abzan kin-guard', ['Human', 'Warrior']).
card_colors('abzan kin-guard', ['G']).
card_text('abzan kin-guard', 'Abzan Kin-Guard has lifelink as long as you control a white or black permanent.').
card_mana_cost('abzan kin-guard', ['3', 'G']).
card_cmc('abzan kin-guard', 4).
card_layout('abzan kin-guard', 'normal').
card_power('abzan kin-guard', 3).
card_toughness('abzan kin-guard', 3).

% Found in: FRF
card_name('abzan runemark', 'Abzan Runemark').
card_type('abzan runemark', 'Enchantment — Aura').
card_types('abzan runemark', ['Enchantment']).
card_subtypes('abzan runemark', ['Aura']).
card_colors('abzan runemark', ['W']).
card_text('abzan runemark', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has vigilance as long as you control a black or green permanent.').
card_mana_cost('abzan runemark', ['2', 'W']).
card_cmc('abzan runemark', 3).
card_layout('abzan runemark', 'normal').

% Found in: FRF
card_name('abzan skycaptain', 'Abzan Skycaptain').
card_type('abzan skycaptain', 'Creature — Bird Soldier').
card_types('abzan skycaptain', ['Creature']).
card_subtypes('abzan skycaptain', ['Bird', 'Soldier']).
card_colors('abzan skycaptain', ['W']).
card_text('abzan skycaptain', 'Flying\nWhen Abzan Skycaptain dies, bolster 2. (Choose a creature with the least toughness among creatures you control and put two +1/+1 counters on it.)').
card_mana_cost('abzan skycaptain', ['3', 'W']).
card_cmc('abzan skycaptain', 4).
card_layout('abzan skycaptain', 'normal').
card_power('abzan skycaptain', 2).
card_toughness('abzan skycaptain', 2).

% Found in: HOP
card_name('academy at tolaria west', 'Academy at Tolaria West').
card_type('academy at tolaria west', 'Plane — Dominaria').
card_types('academy at tolaria west', ['Plane']).
card_subtypes('academy at tolaria west', ['Dominaria']).
card_colors('academy at tolaria west', []).
card_text('academy at tolaria west', 'At the beginning of your end step, if you have no cards in hand, draw seven cards.\nWhenever you roll {C}, discard your hand.').
card_layout('academy at tolaria west', 'plane').

% Found in: CNS, VMA
card_name('academy elite', 'Academy Elite').
card_type('academy elite', 'Creature — Human Wizard').
card_types('academy elite', ['Creature']).
card_subtypes('academy elite', ['Human', 'Wizard']).
card_colors('academy elite', ['U']).
card_text('academy elite', 'Academy Elite enters the battlefield with X +1/+1 counters on it, where X is the number of instant and sorcery cards in all graveyards.\n{2}{U}, Remove a +1/+1 counter from Academy Elite: Draw a card, then discard a card.').
card_mana_cost('academy elite', ['3', 'U']).
card_cmc('academy elite', 4).
card_layout('academy elite', 'normal').
card_power('academy elite', 0).
card_toughness('academy elite', 0).

% Found in: M14
card_name('academy raider', 'Academy Raider').
card_type('academy raider', 'Creature — Human Warrior').
card_types('academy raider', ['Creature']).
card_subtypes('academy raider', ['Human', 'Warrior']).
card_colors('academy raider', ['R']).
card_text('academy raider', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nWhenever Academy Raider deals combat damage to a player, you may discard a card. If you do, draw a card.').
card_mana_cost('academy raider', ['2', 'R']).
card_cmc('academy raider', 3).
card_layout('academy raider', 'normal').
card_power('academy raider', 1).
card_toughness('academy raider', 1).

% Found in: UDS
card_name('academy rector', 'Academy Rector').
card_type('academy rector', 'Creature — Human Cleric').
card_types('academy rector', ['Creature']).
card_subtypes('academy rector', ['Human', 'Cleric']).
card_colors('academy rector', ['W']).
card_text('academy rector', 'When Academy Rector dies, you may exile it. If you do, search your library for an enchantment card, put that card onto the battlefield, then shuffle your library.').
card_mana_cost('academy rector', ['3', 'W']).
card_cmc('academy rector', 4).
card_layout('academy rector', 'normal').
card_power('academy rector', 1).
card_toughness('academy rector', 2).
card_reserved('academy rector').

% Found in: 10E, USG
card_name('academy researchers', 'Academy Researchers').
card_type('academy researchers', 'Creature — Human Wizard').
card_types('academy researchers', ['Creature']).
card_subtypes('academy researchers', ['Human', 'Wizard']).
card_colors('academy researchers', ['U']).
card_text('academy researchers', 'When Academy Researchers enters the battlefield, you may put an Aura card from your hand onto the battlefield attached to Academy Researchers.').
card_mana_cost('academy researchers', ['1', 'U', 'U']).
card_cmc('academy researchers', 3).
card_layout('academy researchers', 'normal').
card_power('academy researchers', 2).
card_toughness('academy researchers', 2).

% Found in: MMA, TSP
card_name('academy ruins', 'Academy Ruins').
card_type('academy ruins', 'Legendary Land').
card_types('academy ruins', ['Land']).
card_subtypes('academy ruins', []).
card_supertypes('academy ruins', ['Legendary']).
card_colors('academy ruins', []).
card_text('academy ruins', '{T}: Add {1} to your mana pool.\n{1}{U}, {T}: Put target artifact card from your graveyard on top of your library.').
card_layout('academy ruins', 'normal').

% Found in: TOR
card_name('accelerate', 'Accelerate').
card_type('accelerate', 'Instant').
card_types('accelerate', ['Instant']).
card_subtypes('accelerate', []).
card_colors('accelerate', ['R']).
card_text('accelerate', 'Target creature gains haste until end of turn.\nDraw a card.').
card_mana_cost('accelerate', ['1', 'R']).
card_cmc('accelerate', 2).
card_layout('accelerate', 'normal').

% Found in: SCG
card_name('accelerated mutation', 'Accelerated Mutation').
card_type('accelerated mutation', 'Instant').
card_types('accelerated mutation', ['Instant']).
card_subtypes('accelerated mutation', []).
card_colors('accelerated mutation', ['G']).
card_text('accelerated mutation', 'Target creature gets +X/+X until end of turn, where X is the highest converted mana cost among permanents you control.').
card_mana_cost('accelerated mutation', ['3', 'G', 'G']).
card_cmc('accelerated mutation', 5).
card_layout('accelerated mutation', 'normal').

% Found in: ODY
card_name('acceptable losses', 'Acceptable Losses').
card_type('acceptable losses', 'Sorcery').
card_types('acceptable losses', ['Sorcery']).
card_subtypes('acceptable losses', []).
card_colors('acceptable losses', ['R']).
card_text('acceptable losses', 'As an additional cost to cast Acceptable Losses, discard a card at random.\nAcceptable Losses deals 5 damage to target creature.').
card_mana_cost('acceptable losses', ['3', 'R']).
card_cmc('acceptable losses', 4).
card_layout('acceptable losses', 'normal').

% Found in: MBS
card_name('accorder paladin', 'Accorder Paladin').
card_type('accorder paladin', 'Creature — Human Knight').
card_types('accorder paladin', ['Creature']).
card_subtypes('accorder paladin', ['Human', 'Knight']).
card_colors('accorder paladin', ['W']).
card_text('accorder paladin', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)').
card_mana_cost('accorder paladin', ['1', 'W']).
card_cmc('accorder paladin', 2).
card_layout('accorder paladin', 'normal').
card_power('accorder paladin', 3).
card_toughness('accorder paladin', 1).

% Found in: M14, SOM
card_name('accorder\'s shield', 'Accorder\'s Shield').
card_type('accorder\'s shield', 'Artifact — Equipment').
card_types('accorder\'s shield', ['Artifact']).
card_subtypes('accorder\'s shield', ['Equipment']).
card_colors('accorder\'s shield', []).
card_text('accorder\'s shield', 'Equipped creature gets +0/+3 and has vigilance. (Attacking doesn\'t cause it to tap.)\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('accorder\'s shield', ['0']).
card_cmc('accorder\'s shield', 0).
card_layout('accorder\'s shield', 'normal').

% Found in: DDO, NMS, pFNM
card_name('accumulated knowledge', 'Accumulated Knowledge').
card_type('accumulated knowledge', 'Instant').
card_types('accumulated knowledge', ['Instant']).
card_subtypes('accumulated knowledge', []).
card_colors('accumulated knowledge', ['U']).
card_text('accumulated knowledge', 'Draw a card, then draw cards equal to the number of cards named Accumulated Knowledge in all graveyards.').
card_mana_cost('accumulated knowledge', ['1', 'U']).
card_cmc('accumulated knowledge', 2).
card_layout('accumulated knowledge', 'normal').

% Found in: ONS
card_name('accursed centaur', 'Accursed Centaur').
card_type('accursed centaur', 'Creature — Zombie Centaur').
card_types('accursed centaur', ['Creature']).
card_subtypes('accursed centaur', ['Zombie', 'Centaur']).
card_colors('accursed centaur', ['B']).
card_text('accursed centaur', 'When Accursed Centaur enters the battlefield, sacrifice a creature.').
card_mana_cost('accursed centaur', ['B']).
card_cmc('accursed centaur', 1).
card_layout('accursed centaur', 'normal').
card_power('accursed centaur', 2).
card_toughness('accursed centaur', 2).

% Found in: M14, M15
card_name('accursed spirit', 'Accursed Spirit').
card_type('accursed spirit', 'Creature — Spirit').
card_types('accursed spirit', ['Creature']).
card_subtypes('accursed spirit', ['Spirit']).
card_colors('accursed spirit', ['B']).
card_text('accursed spirit', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('accursed spirit', ['3', 'B']).
card_cmc('accursed spirit', 4).
card_layout('accursed spirit', 'normal').
card_power('accursed spirit', 3).
card_toughness('accursed spirit', 2).

% Found in: LEG, ME4
card_name('acid rain', 'Acid Rain').
card_type('acid rain', 'Sorcery').
card_types('acid rain', ['Sorcery']).
card_subtypes('acid rain', []).
card_colors('acid rain', ['U']).
card_text('acid rain', 'Destroy all Forests.').
card_mana_cost('acid rain', ['3', 'U']).
card_cmc('acid rain', 4).
card_layout('acid rain', 'normal').
card_reserved('acid rain').

% Found in: SOM
card_name('acid web spider', 'Acid Web Spider').
card_type('acid web spider', 'Creature — Spider').
card_types('acid web spider', ['Creature']).
card_subtypes('acid web spider', ['Spider']).
card_colors('acid web spider', ['G']).
card_text('acid web spider', 'Reach\nWhen Acid Web Spider enters the battlefield, you may destroy target Equipment.').
card_mana_cost('acid web spider', ['3', 'G', 'G']).
card_cmc('acid web spider', 5).
card_layout('acid web spider', 'normal').
card_power('acid web spider', 3).
card_toughness('acid web spider', 5).

% Found in: DTK
card_name('acid-spewer dragon', 'Acid-Spewer Dragon').
card_type('acid-spewer dragon', 'Creature — Dragon').
card_types('acid-spewer dragon', ['Creature']).
card_subtypes('acid-spewer dragon', ['Dragon']).
card_colors('acid-spewer dragon', ['B']).
card_text('acid-spewer dragon', 'Flying, deathtouch\nMegamorph {5}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Acid-Spewer Dragon is turned face up, put a +1/+1 counter on each other Dragon creature you control.').
card_mana_cost('acid-spewer dragon', ['5', 'B']).
card_cmc('acid-spewer dragon', 6).
card_layout('acid-spewer dragon', 'normal').
card_power('acid-spewer dragon', 3).
card_toughness('acid-spewer dragon', 3).

% Found in: MIR
card_name('acidic dagger', 'Acidic Dagger').
card_type('acidic dagger', 'Artifact').
card_types('acidic dagger', ['Artifact']).
card_subtypes('acidic dagger', []).
card_colors('acidic dagger', []).
card_text('acidic dagger', '{4}, {T}: Whenever target creature deals combat damage to a non-Wall creature this turn, destroy that non-Wall creature. When the targeted creature leaves the battlefield this turn, sacrifice Acidic Dagger. Activate this ability only before blockers are declared.').
card_mana_cost('acidic dagger', ['4']).
card_cmc('acidic dagger', 4).
card_layout('acidic dagger', 'normal').
card_reserved('acidic dagger').

% Found in: C13, CMD, DDM, M10, M11, M12, M13, pFNM
card_name('acidic slime', 'Acidic Slime').
card_type('acidic slime', 'Creature — Ooze').
card_types('acidic slime', ['Creature']).
card_subtypes('acidic slime', ['Ooze']).
card_colors('acidic slime', ['G']).
card_text('acidic slime', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhen Acidic Slime enters the battlefield, destroy target artifact, enchantment, or land.').
card_mana_cost('acidic slime', ['3', 'G', 'G']).
card_cmc('acidic slime', 5).
card_layout('acidic slime', 'normal').
card_power('acidic slime', 2).
card_toughness('acidic slime', 2).

% Found in: H09, STH, TPR
card_name('acidic sliver', 'Acidic Sliver').
card_type('acidic sliver', 'Creature — Sliver').
card_types('acidic sliver', ['Creature']).
card_subtypes('acidic sliver', ['Sliver']).
card_colors('acidic sliver', ['B', 'R']).
card_text('acidic sliver', 'All Slivers have \"{2}, Sacrifice this permanent: This permanent deals 2 damage to target creature or player.\"').
card_mana_cost('acidic sliver', ['B', 'R']).
card_cmc('acidic sliver', 2).
card_layout('acidic sliver', 'normal').
card_power('acidic sliver', 2).
card_toughness('acidic sliver', 2).

% Found in: USG
card_name('acidic soil', 'Acidic Soil').
card_type('acidic soil', 'Sorcery').
card_types('acidic soil', ['Sorcery']).
card_subtypes('acidic soil', []).
card_colors('acidic soil', ['R']).
card_text('acidic soil', 'Acidic Soil deals damage to each player equal to the number of lands he or she controls.').
card_mana_cost('acidic soil', ['2', 'R']).
card_cmc('acidic soil', 3).
card_layout('acidic soil', 'normal').

% Found in: ORI
card_name('acolyte of the inferno', 'Acolyte of the Inferno').
card_type('acolyte of the inferno', 'Creature — Human Monk').
card_types('acolyte of the inferno', ['Creature']).
card_subtypes('acolyte of the inferno', ['Human', 'Monk']).
card_colors('acolyte of the inferno', ['R']).
card_text('acolyte of the inferno', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)\nWhenever Acolyte of the Inferno becomes blocked by a creature, it deals 2 damage to that creature.').
card_mana_cost('acolyte of the inferno', ['2', 'R']).
card_cmc('acolyte of the inferno', 3).
card_layout('acolyte of the inferno', 'normal').
card_power('acolyte of the inferno', 3).
card_toughness('acolyte of the inferno', 1).

% Found in: M10
card_name('acolyte of xathrid', 'Acolyte of Xathrid').
card_type('acolyte of xathrid', 'Creature — Human Cleric').
card_types('acolyte of xathrid', ['Creature']).
card_subtypes('acolyte of xathrid', ['Human', 'Cleric']).
card_colors('acolyte of xathrid', ['B']).
card_text('acolyte of xathrid', '{1}{B}, {T}: Target player loses 1 life.').
card_mana_cost('acolyte of xathrid', ['B']).
card_cmc('acolyte of xathrid', 1).
card_layout('acolyte of xathrid', 'normal').
card_power('acolyte of xathrid', 0).
card_toughness('acolyte of xathrid', 1).

% Found in: BNG
card_name('acolyte\'s reward', 'Acolyte\'s Reward').
card_type('acolyte\'s reward', 'Instant').
card_types('acolyte\'s reward', ['Instant']).
card_subtypes('acolyte\'s reward', []).
card_colors('acolyte\'s reward', ['W']).
card_text('acolyte\'s reward', 'Prevent the next X damage that would be dealt to target creature this turn, where X is your devotion to white. If damage is prevented this way, Acolyte\'s Reward deals that much damage to target creature or player. (Each {W} in the mana costs of permanents you control counts toward your devotion to white.)').
card_mana_cost('acolyte\'s reward', ['1', 'W']).
card_cmc('acolyte\'s reward', 2).
card_layout('acolyte\'s reward', 'normal').

% Found in: CMD
card_name('acorn catapult', 'Acorn Catapult').
card_type('acorn catapult', 'Artifact').
card_types('acorn catapult', ['Artifact']).
card_subtypes('acorn catapult', []).
card_colors('acorn catapult', []).
card_text('acorn catapult', '{1}, {T}: Acorn Catapult deals 1 damage to target creature or player. That creature\'s controller or that player puts a 1/1 green Squirrel creature token onto the battlefield.').
card_mana_cost('acorn catapult', ['4']).
card_cmc('acorn catapult', 4).
card_layout('acorn catapult', 'normal').

% Found in: TOR
card_name('acorn harvest', 'Acorn Harvest').
card_type('acorn harvest', 'Sorcery').
card_types('acorn harvest', ['Sorcery']).
card_subtypes('acorn harvest', []).
card_colors('acorn harvest', ['G']).
card_text('acorn harvest', 'Put two 1/1 green Squirrel creature tokens onto the battlefield.\nFlashback—{1}{G}, Pay 3 life. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('acorn harvest', ['3', 'G']).
card_cmc('acorn harvest', 4).
card_layout('acorn harvest', 'normal').

% Found in: 5DN, pMEI
card_name('acquire', 'Acquire').
card_type('acquire', 'Sorcery').
card_types('acquire', ['Sorcery']).
card_subtypes('acquire', []).
card_colors('acquire', ['U']).
card_text('acquire', 'Search target opponent\'s library for an artifact card and put that card onto the battlefield under your control. Then that player shuffles his or her library.').
card_mana_cost('acquire', ['3', 'U', 'U']).
card_cmc('acquire', 5).
card_layout('acquire', 'normal').

% Found in: USG
card_name('acridian', 'Acridian').
card_type('acridian', 'Creature — Insect').
card_types('acridian', ['Creature']).
card_subtypes('acridian', ['Insect']).
card_colors('acridian', ['G']).
card_text('acridian', 'Echo {1}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('acridian', ['1', 'G']).
card_cmc('acridian', 2).
card_layout('acridian', 'normal').
card_power('acridian', 2).
card_toughness('acridian', 4).

% Found in: NPH
card_name('act of aggression', 'Act of Aggression').
card_type('act of aggression', 'Instant').
card_types('act of aggression', ['Instant']).
card_subtypes('act of aggression', []).
card_colors('act of aggression', ['R']).
card_text('act of aggression', '({R/P} can be paid with either {R} or 2 life.)\nGain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn.').
card_mana_cost('act of aggression', ['3', 'R/P', 'R/P']).
card_cmc('act of aggression', 5).
card_layout('act of aggression', 'normal').

% Found in: C13
card_name('act of authority', 'Act of Authority').
card_type('act of authority', 'Enchantment').
card_types('act of authority', ['Enchantment']).
card_subtypes('act of authority', []).
card_colors('act of authority', ['W']).
card_text('act of authority', 'When Act of Authority enters the battlefield, you may exile target artifact or enchantment.\nAt the beginning of your upkeep, you may exile target artifact or enchantment. If you do, its controller gains control of Act of Authority.').
card_mana_cost('act of authority', ['1', 'W', 'W']).
card_cmc('act of authority', 3).
card_layout('act of authority', 'normal').

% Found in: DDN, DPA, GTC, KTK, M10, M11, M12, M14, ORI
card_name('act of treason', 'Act of Treason').
card_type('act of treason', 'Sorcery').
card_types('act of treason', ['Sorcery']).
card_subtypes('act of treason', []).
card_colors('act of treason', ['R']).
card_text('act of treason', 'Gain control of target creature until end of turn. Untap that creature. It gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('act of treason', ['2', 'R']).
card_cmc('act of treason', 3).
card_layout('act of treason', 'normal').

% Found in: M15
card_name('act on impulse', 'Act on Impulse').
card_type('act on impulse', 'Sorcery').
card_types('act on impulse', ['Sorcery']).
card_subtypes('act on impulse', []).
card_colors('act on impulse', ['R']).
card_text('act on impulse', 'Exile the top three cards of your library. Until end of turn, you may play cards exiled this way. (If you cast a spell this way, you still pay its costs. You can play a land this way only if you have an available land play remaining.)').
card_mana_cost('act on impulse', ['2', 'R']).
card_cmc('act on impulse', 3).
card_layout('act on impulse', 'normal').

% Found in: CHR, LEG, ME3
card_name('active volcano', 'Active Volcano').
card_type('active volcano', 'Instant').
card_types('active volcano', ['Instant']).
card_subtypes('active volcano', []).
card_colors('active volcano', ['R']).
card_text('active volcano', 'Choose one —\n• Destroy target blue permanent.\n• Return target Island to its owner\'s hand.').
card_mana_cost('active volcano', ['R']).
card_cmc('active volcano', 1).
card_layout('active volcano', 'normal').

% Found in: ALA
card_name('ad nauseam', 'Ad Nauseam').
card_type('ad nauseam', 'Instant').
card_types('ad nauseam', ['Instant']).
card_subtypes('ad nauseam', []).
card_colors('ad nauseam', ['B']).
card_text('ad nauseam', 'Reveal the top card of your library and put that card into your hand. You lose life equal to its converted mana cost. You may repeat this process any number of times.').
card_mana_cost('ad nauseam', ['3', 'B', 'B']).
card_cmc('ad nauseam', 5).
card_layout('ad nauseam', 'normal').

% Found in: SOK
card_name('adamaro, first to desire', 'Adamaro, First to Desire').
card_type('adamaro, first to desire', 'Legendary Creature — Spirit').
card_types('adamaro, first to desire', ['Creature']).
card_subtypes('adamaro, first to desire', ['Spirit']).
card_supertypes('adamaro, first to desire', ['Legendary']).
card_colors('adamaro, first to desire', ['R']).
card_text('adamaro, first to desire', 'Adamaro, First to Desire\'s power and toughness are each equal to the number of cards in the hand of the opponent with the most cards in hand.').
card_mana_cost('adamaro, first to desire', ['1', 'R', 'R']).
card_cmc('adamaro, first to desire', 3).
card_layout('adamaro, first to desire', 'normal').
card_power('adamaro, first to desire', '*').
card_toughness('adamaro, first to desire', '*').

% Found in: M12
card_name('adaptive automaton', 'Adaptive Automaton').
card_type('adaptive automaton', 'Artifact Creature — Construct').
card_types('adaptive automaton', ['Artifact', 'Creature']).
card_subtypes('adaptive automaton', ['Construct']).
card_colors('adaptive automaton', []).
card_text('adaptive automaton', 'As Adaptive Automaton enters the battlefield, choose a creature type.\nAdaptive Automaton is the chosen type in addition to its other types.\nOther creatures you control of the chosen type get +1/+1.').
card_mana_cost('adaptive automaton', ['3']).
card_cmc('adaptive automaton', 3).
card_layout('adaptive automaton', 'normal').
card_power('adaptive automaton', 2).
card_toughness('adaptive automaton', 2).

% Found in: GTC
card_name('adaptive snapjaw', 'Adaptive Snapjaw').
card_type('adaptive snapjaw', 'Creature — Lizard Beast').
card_types('adaptive snapjaw', ['Creature']).
card_subtypes('adaptive snapjaw', ['Lizard', 'Beast']).
card_colors('adaptive snapjaw', ['G']).
card_text('adaptive snapjaw', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_mana_cost('adaptive snapjaw', ['4', 'G']).
card_cmc('adaptive snapjaw', 5).
card_layout('adaptive snapjaw', 'normal').
card_power('adaptive snapjaw', 6).
card_toughness('adaptive snapjaw', 2).

% Found in: ICE, ME2
card_name('adarkar sentinel', 'Adarkar Sentinel').
card_type('adarkar sentinel', 'Artifact Creature — Soldier').
card_types('adarkar sentinel', ['Artifact', 'Creature']).
card_subtypes('adarkar sentinel', ['Soldier']).
card_colors('adarkar sentinel', []).
card_text('adarkar sentinel', '{1}: Adarkar Sentinel gets +0/+1 until end of turn.').
card_mana_cost('adarkar sentinel', ['5']).
card_cmc('adarkar sentinel', 5).
card_layout('adarkar sentinel', 'normal').
card_power('adarkar sentinel', 3).
card_toughness('adarkar sentinel', 3).

% Found in: ICE
card_name('adarkar unicorn', 'Adarkar Unicorn').
card_type('adarkar unicorn', 'Creature — Unicorn').
card_types('adarkar unicorn', ['Creature']).
card_subtypes('adarkar unicorn', ['Unicorn']).
card_colors('adarkar unicorn', ['W']).
card_text('adarkar unicorn', '{T}: Add {U} or {1}{U} to your mana pool. Spend this mana only to pay cumulative upkeep costs.').
card_mana_cost('adarkar unicorn', ['1', 'W', 'W']).
card_cmc('adarkar unicorn', 3).
card_layout('adarkar unicorn', 'normal').
card_power('adarkar unicorn', 2).
card_toughness('adarkar unicorn', 2).

% Found in: C14, CSP, MMA
card_name('adarkar valkyrie', 'Adarkar Valkyrie').
card_type('adarkar valkyrie', 'Snow Creature — Angel').
card_types('adarkar valkyrie', ['Creature']).
card_subtypes('adarkar valkyrie', ['Angel']).
card_supertypes('adarkar valkyrie', ['Snow']).
card_colors('adarkar valkyrie', ['W']).
card_text('adarkar valkyrie', 'Flying, vigilance\n{T}: When target creature other than Adarkar Valkyrie dies this turn, return that card to the battlefield under your control.').
card_mana_cost('adarkar valkyrie', ['4', 'W', 'W']).
card_cmc('adarkar valkyrie', 6).
card_layout('adarkar valkyrie', 'normal').
card_power('adarkar valkyrie', 4).
card_toughness('adarkar valkyrie', 5).

% Found in: 10E, 5ED, 6ED, 7ED, 9ED, ICE
card_name('adarkar wastes', 'Adarkar Wastes').
card_type('adarkar wastes', 'Land').
card_types('adarkar wastes', ['Land']).
card_subtypes('adarkar wastes', []).
card_colors('adarkar wastes', []).
card_text('adarkar wastes', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Adarkar Wastes deals 1 damage to you.').
card_layout('adarkar wastes', 'normal').

% Found in: CSP
card_name('adarkar windform', 'Adarkar Windform').
card_type('adarkar windform', 'Snow Creature — Illusion').
card_types('adarkar windform', ['Creature']).
card_subtypes('adarkar windform', ['Illusion']).
card_supertypes('adarkar windform', ['Snow']).
card_colors('adarkar windform', ['U']).
card_text('adarkar windform', 'Flying\n{1}{S}: Target creature loses flying until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('adarkar windform', ['4', 'U']).
card_cmc('adarkar windform', 5).
card_layout('adarkar windform', 'normal').
card_power('adarkar windform', 3).
card_toughness('adarkar windform', 3).

% Found in: LRW
card_name('adder-staff boggart', 'Adder-Staff Boggart').
card_type('adder-staff boggart', 'Creature — Goblin Warrior').
card_types('adder-staff boggart', ['Creature']).
card_subtypes('adder-staff boggart', ['Goblin', 'Warrior']).
card_colors('adder-staff boggart', ['R']).
card_text('adder-staff boggart', 'When Adder-Staff Boggart enters the battlefield, clash with an opponent. If you win, put a +1/+1 counter on Adder-Staff Boggart. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('adder-staff boggart', ['1', 'R']).
card_cmc('adder-staff boggart', 2).
card_layout('adder-staff boggart', 'normal').
card_power('adder-staff boggart', 2).
card_toughness('adder-staff boggart', 1).

% Found in: INV, VMA
card_name('addle', 'Addle').
card_type('addle', 'Sorcery').
card_types('addle', ['Sorcery']).
card_subtypes('addle', []).
card_colors('addle', ['B']).
card_text('addle', 'Choose a color. Target player reveals his or her hand and you choose a card of that color from it. That player discards that card.').
card_mana_cost('addle', ['1', 'B']).
card_cmc('addle', 2).
card_layout('addle', 'normal').

% Found in: WWK
card_name('admonition angel', 'Admonition Angel').
card_type('admonition angel', 'Creature — Angel').
card_types('admonition angel', ['Creature']).
card_subtypes('admonition angel', ['Angel']).
card_colors('admonition angel', ['W']).
card_text('admonition angel', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, you may exile target nonland permanent other than Admonition Angel.\nWhen Admonition Angel leaves the battlefield, return all cards exiled with it to the battlefield under their owners\' control.').
card_mana_cost('admonition angel', ['3', 'W', 'W', 'W']).
card_cmc('admonition angel', 6).
card_layout('admonition angel', 'normal').
card_power('admonition angel', 6).
card_toughness('admonition angel', 6).

% Found in: LEG, MED
card_name('adun oakenshield', 'Adun Oakenshield').
card_type('adun oakenshield', 'Legendary Creature — Human Knight').
card_types('adun oakenshield', ['Creature']).
card_subtypes('adun oakenshield', ['Human', 'Knight']).
card_supertypes('adun oakenshield', ['Legendary']).
card_colors('adun oakenshield', ['B', 'R', 'G']).
card_text('adun oakenshield', '{B}{R}{G}, {T}: Return target creature card from your graveyard to your hand.').
card_mana_cost('adun oakenshield', ['B', 'R', 'G']).
card_cmc('adun oakenshield', 3).
card_layout('adun oakenshield', 'normal').
card_power('adun oakenshield', 1).
card_toughness('adun oakenshield', 2).
card_reserved('adun oakenshield').

% Found in: BRB, TMP
card_name('advance scout', 'Advance Scout').
card_type('advance scout', 'Creature — Human Soldier Scout').
card_types('advance scout', ['Creature']).
card_subtypes('advance scout', ['Human', 'Soldier', 'Scout']).
card_colors('advance scout', ['W']).
card_text('advance scout', 'First strike\n{W}: Target creature gains first strike until end of turn.').
card_mana_cost('advance scout', ['1', 'W']).
card_cmc('advance scout', 2).
card_layout('advance scout', 'normal').
card_power('advance scout', 1).
card_toughness('advance scout', 1).

% Found in: 5DN
card_name('advanced hoverguard', 'Advanced Hoverguard').
card_type('advanced hoverguard', 'Creature — Drone').
card_types('advanced hoverguard', ['Creature']).
card_subtypes('advanced hoverguard', ['Drone']).
card_colors('advanced hoverguard', ['U']).
card_text('advanced hoverguard', 'Flying\n{U}: Advanced Hoverguard gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_mana_cost('advanced hoverguard', ['3', 'U']).
card_cmc('advanced hoverguard', 4).
card_layout('advanced hoverguard', 'normal').
card_power('advanced hoverguard', 2).
card_toughness('advanced hoverguard', 2).

% Found in: CNS
card_name('advantageous proclamation', 'Advantageous Proclamation').
card_type('advantageous proclamation', 'Conspiracy').
card_types('advantageous proclamation', ['Conspiracy']).
card_subtypes('advantageous proclamation', []).
card_colors('advantageous proclamation', []).
card_text('advantageous proclamation', '(Start the game with this conspiracy face up in the command zone.)\nYour minimum deck size is reduced by five.').
card_layout('advantageous proclamation', 'normal').

% Found in: DGM
card_name('advent of the wurm', 'Advent of the Wurm').
card_type('advent of the wurm', 'Instant').
card_types('advent of the wurm', ['Instant']).
card_subtypes('advent of the wurm', []).
card_colors('advent of the wurm', ['W', 'G']).
card_text('advent of the wurm', 'Put a 5/5 green Wurm creature token with trample onto the battlefield.').
card_mana_cost('advent of the wurm', ['1', 'G', 'G', 'W']).
card_cmc('advent of the wurm', 4).
card_layout('advent of the wurm', 'normal').

% Found in: LEG
card_name('adventurers\' guildhouse', 'Adventurers\' Guildhouse').
card_type('adventurers\' guildhouse', 'Land').
card_types('adventurers\' guildhouse', ['Land']).
card_subtypes('adventurers\' guildhouse', []).
card_colors('adventurers\' guildhouse', []).
card_text('adventurers\' guildhouse', 'Green legendary creatures you control have \"bands with other legendary creatures.\" (Any legendary creatures can attack in a band as long as at least one has \"bands with other legendary creatures.\" Bands are blocked as a group. If at least two legendary creatures you control, one of which has \"bands with other legendary creatures,\" are blocking or being blocked by the same creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_layout('adventurers\' guildhouse', 'normal').

% Found in: ZEN
card_name('adventuring gear', 'Adventuring Gear').
card_type('adventuring gear', 'Artifact — Equipment').
card_types('adventuring gear', ['Artifact']).
card_subtypes('adventuring gear', ['Equipment']).
card_colors('adventuring gear', []).
card_text('adventuring gear', 'Landfall — Whenever a land enters the battlefield under your control, equipped creature gets +2/+2 until end of turn.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('adventuring gear', ['1']).
card_cmc('adventuring gear', 1).
card_layout('adventuring gear', 'normal').

% Found in: BFZ
card_name('adverse conditions', 'Adverse Conditions').
card_type('adverse conditions', 'Instant').
card_types('adverse conditions', ['Instant']).
card_subtypes('adverse conditions', []).
card_colors('adverse conditions', []).
card_text('adverse conditions', 'Devoid (This card has no color.)\nTap up to two target creatures. Those creatures don\'t untap during their controller\'s next untap step. Put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('adverse conditions', ['3', 'U']).
card_cmc('adverse conditions', 4).
card_layout('adverse conditions', 'normal').

% Found in: SHM
card_name('advice from the fae', 'Advice from the Fae').
card_type('advice from the fae', 'Sorcery').
card_types('advice from the fae', ['Sorcery']).
card_subtypes('advice from the fae', []).
card_colors('advice from the fae', ['U']).
card_text('advice from the fae', '({2/U} can be paid with any two mana or with {U}. This card\'s converted mana cost is 6.)\nLook at the top five cards of your library. If you control more creatures than each other player, put two of those cards into your hand. Otherwise, put one of them into your hand. Then put the rest on the bottom of your library in any order.').
card_mana_cost('advice from the fae', ['2/U', '2/U', '2/U']).
card_cmc('advice from the fae', 6).
card_layout('advice from the fae', 'normal').

% Found in: M14
card_name('advocate of the beast', 'Advocate of the Beast').
card_type('advocate of the beast', 'Creature — Elf Shaman').
card_types('advocate of the beast', ['Creature']).
card_subtypes('advocate of the beast', ['Elf', 'Shaman']).
card_colors('advocate of the beast', ['G']).
card_text('advocate of the beast', 'At the beginning of your end step, put a +1/+1 counter on target Beast creature you control.').
card_mana_cost('advocate of the beast', ['2', 'G']).
card_cmc('advocate of the beast', 3).
card_layout('advocate of the beast', 'normal').
card_power('advocate of the beast', 2).
card_toughness('advocate of the beast', 3).

% Found in: M12, M15, ORI
card_name('aegis angel', 'Aegis Angel').
card_type('aegis angel', 'Creature — Angel').
card_types('aegis angel', ['Creature']).
card_subtypes('aegis angel', ['Angel']).
card_colors('aegis angel', ['W']).
card_text('aegis angel', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Aegis Angel enters the battlefield, another target permanent gains indestructible for as long as you control Aegis Angel. (Effects that say \"destroy\" don\'t destroy it. A creature with indestructible can\'t be destroyed by damage.)').
card_mana_cost('aegis angel', ['4', 'W', 'W']).
card_cmc('aegis angel', 6).
card_layout('aegis angel', 'normal').
card_power('aegis angel', 5).
card_toughness('aegis angel', 5).

% Found in: ODY
card_name('aegis of honor', 'Aegis of Honor').
card_type('aegis of honor', 'Enchantment').
card_types('aegis of honor', ['Enchantment']).
card_subtypes('aegis of honor', []).
card_colors('aegis of honor', ['W']).
card_text('aegis of honor', '{1}: The next time an instant or sorcery spell would deal damage to you this turn, that spell deals that damage to its controller instead.').
card_mana_cost('aegis of honor', ['W']).
card_cmc('aegis of honor', 1).
card_layout('aegis of honor', 'normal').

% Found in: JOU
card_name('aegis of the gods', 'Aegis of the Gods').
card_type('aegis of the gods', 'Enchantment Creature — Human Soldier').
card_types('aegis of the gods', ['Enchantment', 'Creature']).
card_subtypes('aegis of the gods', ['Human', 'Soldier']).
card_colors('aegis of the gods', ['W']).
card_text('aegis of the gods', 'You have hexproof. (You can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('aegis of the gods', ['1', 'W']).
card_cmc('aegis of the gods', 2).
card_layout('aegis of the gods', 'normal').
card_power('aegis of the gods', 2).
card_toughness('aegis of the gods', 1).

% Found in: ICE
card_name('aegis of the meek', 'Aegis of the Meek').
card_type('aegis of the meek', 'Artifact').
card_types('aegis of the meek', ['Artifact']).
card_subtypes('aegis of the meek', []).
card_colors('aegis of the meek', []).
card_text('aegis of the meek', '{1}, {T}: Target 1/1 creature gets +1/+2 until end of turn.').
card_mana_cost('aegis of the meek', ['3']).
card_cmc('aegis of the meek', 3).
card_layout('aegis of the meek', 'normal').
card_reserved('aegis of the meek').

% Found in: FEM, ME2
card_name('aeolipile', 'Aeolipile').
card_type('aeolipile', 'Artifact').
card_types('aeolipile', ['Artifact']).
card_subtypes('aeolipile', []).
card_colors('aeolipile', []).
card_text('aeolipile', '{1}, {T}, Sacrifice Aeolipile: Aeolipile deals 2 damage to target creature or player.').
card_mana_cost('aeolipile', ['2']).
card_cmc('aeolipile', 2).
card_layout('aeolipile', 'normal').
card_reserved('aeolipile').

% Found in: DDM, PLC
card_name('aeon chronicler', 'Aeon Chronicler').
card_type('aeon chronicler', 'Creature — Avatar').
card_types('aeon chronicler', ['Creature']).
card_subtypes('aeon chronicler', ['Avatar']).
card_colors('aeon chronicler', ['U']).
card_text('aeon chronicler', 'Aeon Chronicler\'s power and toughness are each equal to the number of cards in your hand.\nSuspend X—{X}{3}{U}. X can\'t be 0.  (Rather than cast this card from your hand, you may pay {X}{3}{U} and exile it with X time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)\nWhenever a time counter is removed from Aeon Chronicler while it\'s exiled, draw a card.').
card_mana_cost('aeon chronicler', ['3', 'U', 'U']).
card_cmc('aeon chronicler', 5).
card_layout('aeon chronicler', 'normal').
card_power('aeon chronicler', '*').
card_toughness('aeon chronicler', '*').

% Found in: MMQ
card_name('aerial caravan', 'Aerial Caravan').
card_type('aerial caravan', 'Creature — Human Soldier').
card_types('aerial caravan', ['Creature']).
card_subtypes('aerial caravan', ['Human', 'Soldier']).
card_colors('aerial caravan', ['U']).
card_text('aerial caravan', 'Flying\n{1}{U}{U}: Exile the top card of your library. Until end of turn, you may play that card. (Reveal the card as you exile it.)').
card_mana_cost('aerial caravan', ['4', 'U', 'U']).
card_cmc('aerial caravan', 6).
card_layout('aerial caravan', 'normal').
card_power('aerial caravan', 4).
card_toughness('aerial caravan', 3).

% Found in: JOU
card_name('aerial formation', 'Aerial Formation').
card_type('aerial formation', 'Instant').
card_types('aerial formation', ['Instant']).
card_subtypes('aerial formation', []).
card_colors('aerial formation', ['U']).
card_text('aerial formation', 'Strive — Aerial Formation costs {2}{U} more to cast for each target beyond the first.\nAny number of target creatures each get +1/+1 and gain flying until end of turn.').
card_mana_cost('aerial formation', ['U']).
card_cmc('aerial formation', 1).
card_layout('aerial formation', 'normal').

% Found in: GTC
card_name('aerial maneuver', 'Aerial Maneuver').
card_type('aerial maneuver', 'Instant').
card_types('aerial maneuver', ['Instant']).
card_subtypes('aerial maneuver', []).
card_colors('aerial maneuver', ['W']).
card_text('aerial maneuver', 'Target creature gets +1/+1 and gains flying and first strike until end of turn.').
card_mana_cost('aerial maneuver', ['1', 'W']).
card_cmc('aerial maneuver', 2).
card_layout('aerial maneuver', 'normal').

% Found in: RTR
card_name('aerial predation', 'Aerial Predation').
card_type('aerial predation', 'Instant').
card_types('aerial predation', ['Instant']).
card_subtypes('aerial predation', []).
card_colors('aerial predation', ['G']).
card_text('aerial predation', 'Destroy target creature with flying. You gain 2 life.').
card_mana_cost('aerial predation', ['2', 'G']).
card_cmc('aerial predation', 3).
card_layout('aerial predation', 'normal').

% Found in: ORI
card_name('aerial volley', 'Aerial Volley').
card_type('aerial volley', 'Instant').
card_types('aerial volley', ['Instant']).
card_subtypes('aerial volley', []).
card_colors('aerial volley', ['G']).
card_text('aerial volley', 'Aerial Volley deals 3 damage divided as you choose among one, two, or three target creatures with flying.').
card_mana_cost('aerial volley', ['G']).
card_cmc('aerial volley', 1).
card_layout('aerial volley', 'normal').

% Found in: DTK
card_name('aerie bowmasters', 'Aerie Bowmasters').
card_type('aerie bowmasters', 'Creature — Hound Archer').
card_types('aerie bowmasters', ['Creature']).
card_subtypes('aerie bowmasters', ['Hound', 'Archer']).
card_colors('aerie bowmasters', ['G']).
card_text('aerie bowmasters', 'Reach (This creature can block creatures with flying.)\nMegamorph {5}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('aerie bowmasters', ['2', 'G', 'G']).
card_cmc('aerie bowmasters', 4).
card_layout('aerie bowmasters', 'normal').
card_power('aerie bowmasters', 3).
card_toughness('aerie bowmasters', 4).

% Found in: C13, CON
card_name('aerie mystics', 'Aerie Mystics').
card_type('aerie mystics', 'Creature — Bird Wizard').
card_types('aerie mystics', ['Creature']).
card_subtypes('aerie mystics', ['Bird', 'Wizard']).
card_colors('aerie mystics', ['W']).
card_text('aerie mystics', 'Flying\n{1}{G}{U}: Creatures you control gain shroud until end of turn. (They can\'t be the targets of spells or abilities.)').
card_mana_cost('aerie mystics', ['4', 'W']).
card_cmc('aerie mystics', 5).
card_layout('aerie mystics', 'normal').
card_power('aerie mystics', 3).
card_toughness('aerie mystics', 3).

% Found in: EVE
card_name('aerie ouphes', 'Aerie Ouphes').
card_type('aerie ouphes', 'Creature — Ouphe').
card_types('aerie ouphes', ['Creature']).
card_subtypes('aerie ouphes', ['Ouphe']).
card_colors('aerie ouphes', ['G']).
card_text('aerie ouphes', 'Sacrifice Aerie Ouphes: Aerie Ouphes deals damage equal to its power to target creature with flying.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('aerie ouphes', ['4', 'G']).
card_cmc('aerie ouphes', 5).
card_layout('aerie ouphes', 'normal').
card_power('aerie ouphes', 3).
card_toughness('aerie ouphes', 3).

% Found in: BNG
card_name('aerie worshippers', 'Aerie Worshippers').
card_type('aerie worshippers', 'Creature — Human Cleric').
card_types('aerie worshippers', ['Creature']).
card_subtypes('aerie worshippers', ['Human', 'Cleric']).
card_colors('aerie worshippers', ['U']).
card_text('aerie worshippers', 'Inspired — Whenever Aerie Worshippers becomes untapped, you may pay {2}{U}. If you do, put a 2/2 blue Bird enchantment creature token with flying onto the battlefield.').
card_mana_cost('aerie worshippers', ['3', 'U']).
card_cmc('aerie worshippers', 4).
card_layout('aerie worshippers', 'normal').
card_power('aerie worshippers', 2).
card_toughness('aerie worshippers', 4).

% Found in: M15, pMEI
card_name('aeronaut tinkerer', 'Aeronaut Tinkerer').
card_type('aeronaut tinkerer', 'Creature — Human Artificer').
card_types('aeronaut tinkerer', ['Creature']).
card_subtypes('aeronaut tinkerer', ['Human', 'Artificer']).
card_colors('aeronaut tinkerer', ['U']).
card_text('aeronaut tinkerer', 'Aeronaut Tinkerer has flying as long as you control an artifact. (It can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('aeronaut tinkerer', ['2', 'U']).
card_cmc('aeronaut tinkerer', 3).
card_layout('aeronaut tinkerer', 'normal').
card_power('aeronaut tinkerer', 2).
card_toughness('aeronaut tinkerer', 3).

% Found in: UNH
card_name('aesthetic consultation', 'Aesthetic Consultation').
card_type('aesthetic consultation', 'Instant').
card_types('aesthetic consultation', ['Instant']).
card_subtypes('aesthetic consultation', []).
card_colors('aesthetic consultation', ['B']).
card_text('aesthetic consultation', 'Name an artist. Remove the top six cards of your library from the game, then reveal cards from the top of your library until you reveal a card by the named artist. Put that card in your hand, then remove all the other cards revealed this way from the game.').
card_mana_cost('aesthetic consultation', ['B']).
card_cmc('aesthetic consultation', 1).
card_layout('aesthetic consultation', 'normal').

% Found in: ALL, ATH, ME4
card_name('aesthir glider', 'Aesthir Glider').
card_type('aesthir glider', 'Artifact Creature — Bird').
card_types('aesthir glider', ['Artifact', 'Creature']).
card_subtypes('aesthir glider', ['Bird']).
card_colors('aesthir glider', []).
card_text('aesthir glider', 'Flying\nAesthir Glider can\'t block.').
card_mana_cost('aesthir glider', ['3']).
card_cmc('aesthir glider', 3).
card_layout('aesthir glider', 'normal').
card_power('aesthir glider', 2).
card_toughness('aesthir glider', 1).

% Found in: DDP, ROE
card_name('affa guard hound', 'Affa Guard Hound').
card_type('affa guard hound', 'Creature — Hound').
card_types('affa guard hound', ['Creature']).
card_subtypes('affa guard hound', ['Hound']).
card_colors('affa guard hound', ['W']).
card_text('affa guard hound', 'Flash\nWhen Affa Guard Hound enters the battlefield, target creature gets +0/+3 until end of turn.').
card_mana_cost('affa guard hound', ['2', 'W']).
card_cmc('affa guard hound', 3).
card_layout('affa guard hound', 'normal').
card_power('affa guard hound', 2).
card_toughness('affa guard hound', 2).

% Found in: 10E, ODY
card_name('afflict', 'Afflict').
card_type('afflict', 'Instant').
card_types('afflict', ['Instant']).
card_subtypes('afflict', []).
card_colors('afflict', ['B']).
card_text('afflict', 'Target creature gets -1/-1 until end of turn.\nDraw a card.').
card_mana_cost('afflict', ['2', 'B']).
card_cmc('afflict', 3).
card_layout('afflict', 'normal').

% Found in: DKA
card_name('afflicted deserter', 'Afflicted Deserter').
card_type('afflicted deserter', 'Creature — Human Werewolf').
card_types('afflicted deserter', ['Creature']).
card_subtypes('afflicted deserter', ['Human', 'Werewolf']).
card_colors('afflicted deserter', ['R']).
card_text('afflicted deserter', 'At the beginning of each upkeep, if no spells were cast last turn, transform Afflicted Deserter.').
card_mana_cost('afflicted deserter', ['3', 'R']).
card_cmc('afflicted deserter', 4).
card_layout('afflicted deserter', 'double-faced').
card_power('afflicted deserter', 3).
card_toughness('afflicted deserter', 2).
card_sides('afflicted deserter', 'werewolf ransacker').

% Found in: MIR
card_name('afiya grove', 'Afiya Grove').
card_type('afiya grove', 'Enchantment').
card_types('afiya grove', ['Enchantment']).
card_subtypes('afiya grove', []).
card_colors('afiya grove', ['G']).
card_text('afiya grove', 'Afiya Grove enters the battlefield with three +1/+1 counters on it.\nAt the beginning of your upkeep, move a +1/+1 counter from Afiya Grove onto target creature.\nWhen Afiya Grove has no +1/+1 counters on it, sacrifice it.').
card_mana_cost('afiya grove', ['1', 'G']).
card_cmc('afiya grove', 2).
card_layout('afiya grove', 'normal').
card_reserved('afiya grove').

% Found in: C14, CMD, MIR, MMQ, VMA
card_name('afterlife', 'Afterlife').
card_type('afterlife', 'Instant').
card_types('afterlife', ['Instant']).
card_subtypes('afterlife', []).
card_colors('afterlife', ['W']).
card_text('afterlife', 'Destroy target creature. It can\'t be regenerated. Its controller puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('afterlife', ['2', 'W']).
card_cmc('afterlife', 3).
card_layout('afterlife', 'normal').

% Found in: TMP, TPR, VMA
card_name('aftershock', 'Aftershock').
card_type('aftershock', 'Sorcery').
card_types('aftershock', ['Sorcery']).
card_subtypes('aftershock', []).
card_colors('aftershock', ['R']).
card_text('aftershock', 'Destroy target artifact, creature, or land. Aftershock deals 3 damage to you.').
card_mana_cost('aftershock', ['2', 'R', 'R']).
card_cmc('aftershock', 4).
card_layout('aftershock', 'normal').

% Found in: WWK
card_name('agadeem occultist', 'Agadeem Occultist').
card_type('agadeem occultist', 'Creature — Human Shaman Ally').
card_types('agadeem occultist', ['Creature']).
card_subtypes('agadeem occultist', ['Human', 'Shaman', 'Ally']).
card_colors('agadeem occultist', ['B']).
card_text('agadeem occultist', '{T}: Put target creature card from an opponent\'s graveyard onto the battlefield under your control if its converted mana cost is less than or equal to the number of Allies you control.').
card_mana_cost('agadeem occultist', ['2', 'B']).
card_cmc('agadeem occultist', 3).
card_layout('agadeem occultist', 'normal').
card_power('agadeem occultist', 0).
card_toughness('agadeem occultist', 2).

% Found in: DDH, DST
card_name('ageless entity', 'Ageless Entity').
card_type('ageless entity', 'Creature — Elemental').
card_types('ageless entity', ['Creature']).
card_subtypes('ageless entity', ['Elemental']).
card_colors('ageless entity', ['G']).
card_text('ageless entity', 'Whenever you gain life, put that many +1/+1 counters on Ageless Entity.').
card_mana_cost('ageless entity', ['3', 'G', 'G']).
card_cmc('ageless entity', 5).
card_layout('ageless entity', 'normal').
card_power('ageless entity', 4).
card_toughness('ageless entity', 4).

% Found in: SCG
card_name('ageless sentinels', 'Ageless Sentinels').
card_type('ageless sentinels', 'Creature — Wall').
card_types('ageless sentinels', ['Creature']).
card_subtypes('ageless sentinels', ['Wall']).
card_colors('ageless sentinels', ['W']).
card_text('ageless sentinels', 'Defender (This creature can\'t attack.)\nFlying\nWhen Ageless Sentinels blocks, it becomes a Bird Giant, and it loses defender. (It\'s no longer a Wall. This effect lasts indefinitely.)').
card_mana_cost('ageless sentinels', ['3', 'W']).
card_cmc('ageless sentinels', 4).
card_layout('ageless sentinels', 'normal').
card_power('ageless sentinels', 4).
card_toughness('ageless sentinels', 4).

% Found in: CNS
card_name('agent of acquisitions', 'Agent of Acquisitions').
card_type('agent of acquisitions', 'Artifact Creature — Construct').
card_types('agent of acquisitions', ['Artifact', 'Creature']).
card_subtypes('agent of acquisitions', ['Construct']).
card_colors('agent of acquisitions', []).
card_text('agent of acquisitions', 'Draft Agent of Acquisitions face up.\nInstead of drafting a card from a booster pack, you may draft each card in that booster pack, one at a time. If you do, turn Agent of Acquisitions face down and you can\'t draft cards for the rest of this draft round. (You may look at booster packs passed to you.)').
card_mana_cost('agent of acquisitions', ['2']).
card_cmc('agent of acquisitions', 2).
card_layout('agent of acquisitions', 'normal').
card_power('agent of acquisitions', 2).
card_toughness('agent of acquisitions', 1).

% Found in: JOU
card_name('agent of erebos', 'Agent of Erebos').
card_type('agent of erebos', 'Enchantment Creature — Zombie').
card_types('agent of erebos', ['Enchantment', 'Creature']).
card_subtypes('agent of erebos', ['Zombie']).
card_colors('agent of erebos', ['B']).
card_text('agent of erebos', 'Constellation — Whenever Agent of Erebos or another enchantment enters the battlefield under your control, exile all cards from target player\'s graveyard.').
card_mana_cost('agent of erebos', ['3', 'B']).
card_cmc('agent of erebos', 4).
card_layout('agent of erebos', 'normal').
card_power('agent of erebos', 2).
card_toughness('agent of erebos', 2).

% Found in: THS
card_name('agent of horizons', 'Agent of Horizons').
card_type('agent of horizons', 'Creature — Human Rogue').
card_types('agent of horizons', ['Creature']).
card_subtypes('agent of horizons', ['Human', 'Rogue']).
card_colors('agent of horizons', ['G']).
card_text('agent of horizons', '{2}{U}: Agent of Horizons can\'t be blocked this turn.').
card_mana_cost('agent of horizons', ['2', 'G']).
card_cmc('agent of horizons', 3).
card_layout('agent of horizons', 'normal').
card_power('agent of horizons', 3).
card_toughness('agent of horizons', 2).

% Found in: GPT
card_name('agent of masks', 'Agent of Masks').
card_type('agent of masks', 'Creature — Human Advisor').
card_types('agent of masks', ['Creature']).
card_subtypes('agent of masks', ['Human', 'Advisor']).
card_colors('agent of masks', ['W', 'B']).
card_text('agent of masks', 'At the beginning of your upkeep, each opponent loses 1 life. You gain life equal to the life lost this way.').
card_mana_cost('agent of masks', ['3', 'W', 'B']).
card_cmc('agent of masks', 5).
card_layout('agent of masks', 'normal').
card_power('agent of masks', 2).
card_toughness('agent of masks', 3).

% Found in: PCY
card_name('agent of shauku', 'Agent of Shauku').
card_type('agent of shauku', 'Creature — Human Mercenary').
card_types('agent of shauku', ['Creature']).
card_subtypes('agent of shauku', ['Human', 'Mercenary']).
card_colors('agent of shauku', ['B']).
card_text('agent of shauku', '{1}{B}, Sacrifice a land: Target creature gets +2/+0 until end of turn.').
card_mana_cost('agent of shauku', ['1', 'B']).
card_cmc('agent of shauku', 2).
card_layout('agent of shauku', 'normal').
card_power('agent of shauku', 1).
card_toughness('agent of shauku', 1).

% Found in: ALL
card_name('agent of stromgald', 'Agent of Stromgald').
card_type('agent of stromgald', 'Creature — Human Knight').
card_types('agent of stromgald', ['Creature']).
card_subtypes('agent of stromgald', ['Human', 'Knight']).
card_colors('agent of stromgald', ['R']).
card_text('agent of stromgald', '{R}: Add {B} to your mana pool.').
card_mana_cost('agent of stromgald', ['R']).
card_cmc('agent of stromgald', 1).
card_layout('agent of stromgald', 'normal').
card_power('agent of stromgald', 1).
card_toughness('agent of stromgald', 1).

% Found in: THS
card_name('agent of the fates', 'Agent of the Fates').
card_type('agent of the fates', 'Creature — Human Assassin').
card_types('agent of the fates', ['Creature']).
card_subtypes('agent of the fates', ['Human', 'Assassin']).
card_colors('agent of the fates', ['B']).
card_text('agent of the fates', 'Deathtouch\nHeroic — Whenever you cast a spell that targets Agent of the Fates, each opponent sacrifices a creature.').
card_mana_cost('agent of the fates', ['1', 'B', 'B']).
card_cmc('agent of the fates', 3).
card_layout('agent of the fates', 'normal').
card_power('agent of the fates', 3).
card_toughness('agent of the fates', 2).

% Found in: AVR
card_name('aggravate', 'Aggravate').
card_type('aggravate', 'Instant').
card_types('aggravate', ['Instant']).
card_subtypes('aggravate', []).
card_colors('aggravate', ['R']).
card_text('aggravate', 'Aggravate deals 1 damage to each creature target player controls. Each creature dealt damage this way attacks this turn if able.').
card_mana_cost('aggravate', ['3', 'R', 'R']).
card_cmc('aggravate', 5).
card_layout('aggravate', 'normal').

% Found in: ONS
card_name('aggravated assault', 'Aggravated Assault').
card_type('aggravated assault', 'Enchantment').
card_types('aggravated assault', ['Enchantment']).
card_subtypes('aggravated assault', []).
card_colors('aggravated assault', ['R']).
card_text('aggravated assault', '{3}{R}{R}: Untap all creatures you control. After this main phase, there is an additional combat phase followed by an additional main phase. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('aggravated assault', ['2', 'R']).
card_cmc('aggravated assault', 3).
card_layout('aggravated assault', 'normal').

% Found in: ICE
card_name('aggression', 'Aggression').
card_type('aggression', 'Enchantment — Aura').
card_types('aggression', ['Enchantment']).
card_subtypes('aggression', ['Aura']).
card_colors('aggression', ['R']).
card_text('aggression', 'Enchant non-Wall creature\nEnchanted creature has first strike and trample.\nAt the beginning of the end step of enchanted creature\'s controller, destroy that creature if it didn\'t attack this turn.').
card_mana_cost('aggression', ['2', 'R']).
card_cmc('aggression', 3).
card_layout('aggression', 'normal').

% Found in: M15
card_name('aggressive mining', 'Aggressive Mining').
card_type('aggressive mining', 'Enchantment').
card_types('aggressive mining', ['Enchantment']).
card_subtypes('aggressive mining', []).
card_colors('aggressive mining', ['R']).
card_text('aggressive mining', 'You can\'t play lands.\nSacrifice a land: Draw two cards. Activate this ability only once each turn.').
card_mana_cost('aggressive mining', ['3', 'R']).
card_cmc('aggressive mining', 4).
card_layout('aggressive mining', 'normal').

% Found in: 10E, INV
card_name('aggressive urge', 'Aggressive Urge').
card_type('aggressive urge', 'Instant').
card_types('aggressive urge', ['Instant']).
card_subtypes('aggressive urge', []).
card_colors('aggressive urge', ['G']).
card_text('aggressive urge', 'Target creature gets +1/+1 until end of turn.\nDraw a card.').
card_mana_cost('aggressive urge', ['1', 'G']).
card_cmc('aggressive urge', 2).
card_layout('aggressive urge', 'normal').

% Found in: MIR
card_name('agility', 'Agility').
card_type('agility', 'Enchantment — Aura').
card_types('agility', ['Enchantment']).
card_subtypes('agility', ['Aura']).
card_colors('agility', ['R']).
card_text('agility', 'Enchant creature\nEnchanted creature gets +1/+1 and has flanking. (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)').
card_mana_cost('agility', ['1', 'R']).
card_cmc('agility', 2).
card_layout('agility', 'normal').

% Found in: DDH, INV
card_name('agonizing demise', 'Agonizing Demise').
card_type('agonizing demise', 'Instant').
card_types('agonizing demise', ['Instant']).
card_subtypes('agonizing demise', []).
card_colors('agonizing demise', ['B']).
card_text('agonizing demise', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nDestroy target nonblack creature. It can\'t be regenerated. If Agonizing Demise was kicked, it deals damage equal to that creature\'s power to the creature\'s controller.').
card_mana_cost('agonizing demise', ['3', 'B']).
card_cmc('agonizing demise', 4).
card_layout('agonizing demise', 'normal').

% Found in: 10E, 6ED, 7ED, WTH
card_name('agonizing memories', 'Agonizing Memories').
card_type('agonizing memories', 'Sorcery').
card_types('agonizing memories', ['Sorcery']).
card_subtypes('agonizing memories', []).
card_colors('agonizing memories', ['B']).
card_text('agonizing memories', 'Look at target player\'s hand and choose two cards from it. Put them on top of that player\'s library in any order.').
card_mana_cost('agonizing memories', ['2', 'B', 'B']).
card_cmc('agonizing memories', 4).
card_layout('agonizing memories', 'normal').

% Found in: ALA, ARC, MM2
card_name('agony warp', 'Agony Warp').
card_type('agony warp', 'Instant').
card_types('agony warp', ['Instant']).
card_subtypes('agony warp', []).
card_colors('agony warp', ['U', 'B']).
card_text('agony warp', 'Target creature gets -3/-0 until end of turn.\nTarget creature gets -0/-3 until end of turn.').
card_mana_cost('agony warp', ['U', 'B']).
card_cmc('agony warp', 2).
card_layout('agony warp', 'normal').

% Found in: DDM, GTC
card_name('agoraphobia', 'Agoraphobia').
card_type('agoraphobia', 'Enchantment — Aura').
card_types('agoraphobia', ['Enchantment']).
card_subtypes('agoraphobia', ['Aura']).
card_colors('agoraphobia', ['U']).
card_text('agoraphobia', 'Enchant creature\nEnchanted creature gets -5/-0.\n{2}{U}: Return Agoraphobia to its owner\'s hand.').
card_mana_cost('agoraphobia', ['1', 'U']).
card_cmc('agoraphobia', 2).
card_layout('agoraphobia', 'normal').

% Found in: RAV
card_name('agrus kos, wojek veteran', 'Agrus Kos, Wojek Veteran').
card_type('agrus kos, wojek veteran', 'Legendary Creature — Human Soldier').
card_types('agrus kos, wojek veteran', ['Creature']).
card_subtypes('agrus kos, wojek veteran', ['Human', 'Soldier']).
card_supertypes('agrus kos, wojek veteran', ['Legendary']).
card_colors('agrus kos, wojek veteran', ['W', 'R']).
card_text('agrus kos, wojek veteran', 'Whenever Agrus Kos, Wojek Veteran attacks, attacking red creatures get +2/+0 and attacking white creatures get +0/+2 until end of turn.').
card_mana_cost('agrus kos, wojek veteran', ['3', 'R', 'W']).
card_cmc('agrus kos, wojek veteran', 5).
card_layout('agrus kos, wojek veteran', 'normal').
card_power('agrus kos, wojek veteran', 3).
card_toughness('agrus kos, wojek veteran', 3).

% Found in: HOP
card_name('agyrem', 'Agyrem').
card_type('agyrem', 'Plane — Ravnica').
card_types('agyrem', ['Plane']).
card_subtypes('agyrem', ['Ravnica']).
card_colors('agyrem', []).
card_text('agyrem', 'Whenever a white creature dies, return it to the battlefield under its owner\'s control at the beginning of the next end step.\nWhenever a nonwhite creature dies, return it to its owner\'s hand at the beginning of the next end step.\nWhenever you roll {C}, creatures can\'t attack you until a player planeswalks.').
card_layout('agyrem', 'plane').

% Found in: DTK
card_name('ainok artillerist', 'Ainok Artillerist').
card_type('ainok artillerist', 'Creature — Hound Archer').
card_types('ainok artillerist', ['Creature']).
card_subtypes('ainok artillerist', ['Hound', 'Archer']).
card_colors('ainok artillerist', ['G']).
card_text('ainok artillerist', 'Ainok Artillerist has reach as long as it has a +1/+1 counter on it. (It can block creatures with flying.)').
card_mana_cost('ainok artillerist', ['2', 'G']).
card_cmc('ainok artillerist', 3).
card_layout('ainok artillerist', 'normal').
card_power('ainok artillerist', 4).
card_toughness('ainok artillerist', 1).

% Found in: KTK
card_name('ainok bond-kin', 'Ainok Bond-Kin').
card_type('ainok bond-kin', 'Creature — Hound Soldier').
card_types('ainok bond-kin', ['Creature']).
card_subtypes('ainok bond-kin', ['Hound', 'Soldier']).
card_colors('ainok bond-kin', ['W']).
card_text('ainok bond-kin', 'Outlast {1}{W} ({1}{W}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has first strike.').
card_mana_cost('ainok bond-kin', ['1', 'W']).
card_cmc('ainok bond-kin', 2).
card_layout('ainok bond-kin', 'normal').
card_power('ainok bond-kin', 2).
card_toughness('ainok bond-kin', 1).

% Found in: FRF
card_name('ainok guide', 'Ainok Guide').
card_type('ainok guide', 'Creature — Hound Scout').
card_types('ainok guide', ['Creature']).
card_subtypes('ainok guide', ['Hound', 'Scout']).
card_colors('ainok guide', ['G']).
card_text('ainok guide', 'When Ainok Guide enters the battlefield, choose one —\n• Put a +1/+1 counter on Ainok Guide.\n• Search your library for a basic land card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('ainok guide', ['1', 'G']).
card_cmc('ainok guide', 2).
card_layout('ainok guide', 'normal').
card_power('ainok guide', 1).
card_toughness('ainok guide', 1).

% Found in: DTK
card_name('ainok survivalist', 'Ainok Survivalist').
card_type('ainok survivalist', 'Creature — Hound Shaman').
card_types('ainok survivalist', ['Creature']).
card_subtypes('ainok survivalist', ['Hound', 'Shaman']).
card_colors('ainok survivalist', ['G']).
card_text('ainok survivalist', 'Megamorph {1}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Ainok Survivalist is turned face up, destroy target artifact or enchantment an opponent controls.').
card_mana_cost('ainok survivalist', ['1', 'G']).
card_cmc('ainok survivalist', 2).
card_layout('ainok survivalist', 'normal').
card_power('ainok survivalist', 2).
card_toughness('ainok survivalist', 1).

% Found in: FRF_UGIN, KTK
card_name('ainok tracker', 'Ainok Tracker').
card_type('ainok tracker', 'Creature — Hound Scout').
card_types('ainok tracker', ['Creature']).
card_subtypes('ainok tracker', ['Hound', 'Scout']).
card_colors('ainok tracker', ['R']).
card_text('ainok tracker', 'First strike\nMorph {4}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('ainok tracker', ['5', 'R']).
card_cmc('ainok tracker', 6).
card_layout('ainok tracker', 'normal').
card_power('ainok tracker', 3).
card_toughness('ainok tracker', 3).

% Found in: NMS
card_name('air bladder', 'Air Bladder').
card_type('air bladder', 'Enchantment — Aura').
card_types('air bladder', ['Enchantment']).
card_subtypes('air bladder', ['Aura']).
card_colors('air bladder', ['U']).
card_text('air bladder', 'Enchant creature\nEnchanted creature has flying.\nEnchanted creature can block only creatures with flying.').
card_mana_cost('air bladder', ['U']).
card_cmc('air bladder', 1).
card_layout('air bladder', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, BRB, BTD, CED, CEI, DD2, DD3_JVC, DPA, LEA, LEB, M10, ME4, PO2, S99
card_name('air elemental', 'Air Elemental').
card_type('air elemental', 'Creature — Elemental').
card_types('air elemental', ['Creature']).
card_subtypes('air elemental', ['Elemental']).
card_colors('air elemental', ['U']).
card_text('air elemental', 'Flying').
card_mana_cost('air elemental', ['3', 'U', 'U']).
card_cmc('air elemental', 5).
card_layout('air elemental', 'normal').
card_power('air elemental', 4).
card_toughness('air elemental', 4).

% Found in: CNS, M11, M14, MM2
card_name('air servant', 'Air Servant').
card_type('air servant', 'Creature — Elemental').
card_types('air servant', ['Creature']).
card_subtypes('air servant', ['Elemental']).
card_colors('air servant', ['U']).
card_text('air servant', 'Flying\n{2}{U}: Tap target creature with flying.').
card_mana_cost('air servant', ['4', 'U']).
card_cmc('air servant', 5).
card_layout('air servant', 'normal').
card_power('air servant', 4).
card_toughness('air servant', 3).

% Found in: ONS
card_name('airborne aid', 'Airborne Aid').
card_type('airborne aid', 'Sorcery').
card_types('airborne aid', ['Sorcery']).
card_subtypes('airborne aid', []).
card_colors('airborne aid', ['U']).
card_text('airborne aid', 'Draw a card for each Bird on the battlefield.').
card_mana_cost('airborne aid', ['3', 'U']).
card_cmc('airborne aid', 4).
card_layout('airborne aid', 'normal').

% Found in: ONS
card_name('airdrop condor', 'Airdrop Condor').
card_type('airdrop condor', 'Creature — Bird').
card_types('airdrop condor', ['Creature']).
card_subtypes('airdrop condor', ['Bird']).
card_colors('airdrop condor', ['R']).
card_text('airdrop condor', 'Flying\n{1}{R}, Sacrifice a Goblin creature: Airdrop Condor deals damage equal to the sacrificed creature\'s power to target creature or player.').
card_mana_cost('airdrop condor', ['4', 'R']).
card_cmc('airdrop condor', 5).
card_layout('airdrop condor', 'normal').
card_power('airdrop condor', 2).
card_toughness('airdrop condor', 2).

% Found in: LEG
card_name('aisling leprechaun', 'Aisling Leprechaun').
card_type('aisling leprechaun', 'Creature — Faerie').
card_types('aisling leprechaun', ['Creature']).
card_subtypes('aisling leprechaun', ['Faerie']).
card_colors('aisling leprechaun', ['G']).
card_text('aisling leprechaun', 'Whenever Aisling Leprechaun blocks or becomes blocked by a creature, that creature becomes green. (This effect lasts indefinitely.)').
card_mana_cost('aisling leprechaun', ['G']).
card_cmc('aisling leprechaun', 1).
card_layout('aisling leprechaun', 'normal').
card_power('aisling leprechaun', 1).
card_toughness('aisling leprechaun', 1).

% Found in: LRW, M10, M11, pPRO
card_name('ajani goldmane', 'Ajani Goldmane').
card_type('ajani goldmane', 'Planeswalker — Ajani').
card_types('ajani goldmane', ['Planeswalker']).
card_subtypes('ajani goldmane', ['Ajani']).
card_colors('ajani goldmane', ['W']).
card_text('ajani goldmane', '+1: You gain 2 life.\n−1: Put a +1/+1 counter on each creature you control. Those creatures gain vigilance until end of turn.\n−6: Put a white Avatar creature token onto the battlefield. It has \"This creature\'s power and toughness are each equal to your life total.\"').
card_mana_cost('ajani goldmane', ['2', 'W', 'W']).
card_cmc('ajani goldmane', 4).
card_layout('ajani goldmane', 'normal').
card_loyalty('ajani goldmane', 4).

% Found in: M15, pMEI
card_name('ajani steadfast', 'Ajani Steadfast').
card_type('ajani steadfast', 'Planeswalker — Ajani').
card_types('ajani steadfast', ['Planeswalker']).
card_subtypes('ajani steadfast', ['Ajani']).
card_colors('ajani steadfast', ['W']).
card_text('ajani steadfast', '+1: Until end of turn, up to one target creature gets +1/+1 and gains first strike, vigilance, and lifelink.\n−2: Put a +1/+1 counter on each creature you control and a loyalty counter on each other planeswalker you control.\n−7: You get an emblem with \"If a source would deal damage to you or a planeswalker you control, prevent all but 1 of that damage.\"').
card_mana_cost('ajani steadfast', ['3', 'W']).
card_cmc('ajani steadfast', 4).
card_layout('ajani steadfast', 'normal').
card_loyalty('ajani steadfast', 4).

% Found in: ALA, DDH, pLPA, pPRE
card_name('ajani vengeant', 'Ajani Vengeant').
card_type('ajani vengeant', 'Planeswalker — Ajani').
card_types('ajani vengeant', ['Planeswalker']).
card_subtypes('ajani vengeant', ['Ajani']).
card_colors('ajani vengeant', ['W', 'R']).
card_text('ajani vengeant', '+1: Target permanent doesn\'t untap during its controller\'s next untap step.\n−2: Ajani Vengeant deals 3 damage to target creature or player and you gain 3 life.\n−7: Destroy all lands target player controls.').
card_mana_cost('ajani vengeant', ['2', 'R', 'W']).
card_cmc('ajani vengeant', 4).
card_layout('ajani vengeant', 'normal').
card_loyalty('ajani vengeant', 3).

% Found in: M14
card_name('ajani\'s chosen', 'Ajani\'s Chosen').
card_type('ajani\'s chosen', 'Creature — Cat Soldier').
card_types('ajani\'s chosen', ['Creature']).
card_subtypes('ajani\'s chosen', ['Cat', 'Soldier']).
card_colors('ajani\'s chosen', ['W']).
card_text('ajani\'s chosen', 'Whenever an enchantment enters the battlefield under your control, put a 2/2 white Cat creature token onto the battlefield. If that enchantment is an Aura, you may attach it to the token.').
card_mana_cost('ajani\'s chosen', ['2', 'W', 'W']).
card_cmc('ajani\'s chosen', 4).
card_layout('ajani\'s chosen', 'normal').
card_power('ajani\'s chosen', 3).
card_toughness('ajani\'s chosen', 3).

% Found in: DDH, M11
card_name('ajani\'s mantra', 'Ajani\'s Mantra').
card_type('ajani\'s mantra', 'Enchantment').
card_types('ajani\'s mantra', ['Enchantment']).
card_subtypes('ajani\'s mantra', []).
card_colors('ajani\'s mantra', ['W']).
card_text('ajani\'s mantra', 'At the beginning of your upkeep, you may gain 1 life.').
card_mana_cost('ajani\'s mantra', ['1', 'W']).
card_cmc('ajani\'s mantra', 2).
card_layout('ajani\'s mantra', 'normal').

% Found in: JOU
card_name('ajani\'s presence', 'Ajani\'s Presence').
card_type('ajani\'s presence', 'Instant').
card_types('ajani\'s presence', ['Instant']).
card_subtypes('ajani\'s presence', []).
card_colors('ajani\'s presence', ['W']).
card_text('ajani\'s presence', 'Strive — Ajani\'s Presence costs {2}{W} more to cast for each target beyond the first.\nAny number of target creatures each get +1/+1 and gain indestructible until end of turn. (Damage and effects that say \"destroy\" don\'t destroy them.)').
card_mana_cost('ajani\'s presence', ['W']).
card_cmc('ajani\'s presence', 1).
card_layout('ajani\'s presence', 'normal').

% Found in: C13, DDH, M11, M15
card_name('ajani\'s pridemate', 'Ajani\'s Pridemate').
card_type('ajani\'s pridemate', 'Creature — Cat Soldier').
card_types('ajani\'s pridemate', ['Creature']).
card_subtypes('ajani\'s pridemate', ['Cat', 'Soldier']).
card_colors('ajani\'s pridemate', ['W']).
card_text('ajani\'s pridemate', 'Whenever you gain life, you may put a +1/+1 counter on Ajani\'s Pridemate.').
card_mana_cost('ajani\'s pridemate', ['1', 'W']).
card_cmc('ajani\'s pridemate', 2).
card_layout('ajani\'s pridemate', 'normal').
card_power('ajani\'s pridemate', 2).
card_toughness('ajani\'s pridemate', 2).

% Found in: CNS, M13
card_name('ajani\'s sunstriker', 'Ajani\'s Sunstriker').
card_type('ajani\'s sunstriker', 'Creature — Cat Cleric').
card_types('ajani\'s sunstriker', ['Creature']).
card_subtypes('ajani\'s sunstriker', ['Cat', 'Cleric']).
card_colors('ajani\'s sunstriker', ['W']).
card_text('ajani\'s sunstriker', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('ajani\'s sunstriker', ['W', 'W']).
card_cmc('ajani\'s sunstriker', 2).
card_layout('ajani\'s sunstriker', 'normal').
card_power('ajani\'s sunstriker', 2).
card_toughness('ajani\'s sunstriker', 2).

% Found in: M13, M14, pMEI
card_name('ajani, caller of the pride', 'Ajani, Caller of the Pride').
card_type('ajani, caller of the pride', 'Planeswalker — Ajani').
card_types('ajani, caller of the pride', ['Planeswalker']).
card_subtypes('ajani, caller of the pride', ['Ajani']).
card_colors('ajani, caller of the pride', ['W']).
card_text('ajani, caller of the pride', '+1: Put a +1/+1 counter on up to one target creature.\n−3: Target creature gains flying and double strike until end of turn.\n−8: Put X 2/2 white Cat creature tokens onto the battlefield, where X is your life total.').
card_mana_cost('ajani, caller of the pride', ['1', 'W', 'W']).
card_cmc('ajani, caller of the pride', 3).
card_layout('ajani, caller of the pride', 'normal').
card_loyalty('ajani, caller of the pride', 4).

% Found in: JOU
card_name('ajani, mentor of heroes', 'Ajani, Mentor of Heroes').
card_type('ajani, mentor of heroes', 'Planeswalker — Ajani').
card_types('ajani, mentor of heroes', ['Planeswalker']).
card_subtypes('ajani, mentor of heroes', ['Ajani']).
card_colors('ajani, mentor of heroes', ['W', 'G']).
card_text('ajani, mentor of heroes', '+1: Distribute three +1/+1 counters among one, two, or three target creatures you control.\n+1: Look at the top four cards of your library. You may reveal an Aura, creature, or planeswalker card from among them and put it into your hand. Put the rest on the bottom of your library in any order.\n−8: You gain 100 life.').
card_mana_cost('ajani, mentor of heroes', ['3', 'G', 'W']).
card_cmc('ajani, mentor of heroes', 5).
card_layout('ajani, mentor of heroes', 'normal').
card_loyalty('ajani, mentor of heroes', 4).

% Found in: CHK
card_name('akki avalanchers', 'Akki Avalanchers').
card_type('akki avalanchers', 'Creature — Goblin Warrior').
card_types('akki avalanchers', ['Creature']).
card_subtypes('akki avalanchers', ['Goblin', 'Warrior']).
card_colors('akki avalanchers', ['R']).
card_text('akki avalanchers', 'Sacrifice a land: Akki Avalanchers gets +2/+0 until end of turn. Activate this ability only once each turn.').
card_mana_cost('akki avalanchers', ['R']).
card_cmc('akki avalanchers', 1).
card_layout('akki avalanchers', 'normal').
card_power('akki avalanchers', 1).
card_toughness('akki avalanchers', 1).

% Found in: BOK
card_name('akki blizzard-herder', 'Akki Blizzard-Herder').
card_type('akki blizzard-herder', 'Creature — Goblin Shaman').
card_types('akki blizzard-herder', ['Creature']).
card_subtypes('akki blizzard-herder', ['Goblin', 'Shaman']).
card_colors('akki blizzard-herder', ['R']).
card_text('akki blizzard-herder', 'When Akki Blizzard-Herder dies, each player sacrifices a land.').
card_mana_cost('akki blizzard-herder', ['1', 'R']).
card_cmc('akki blizzard-herder', 2).
card_layout('akki blizzard-herder', 'normal').
card_power('akki blizzard-herder', 1).
card_toughness('akki blizzard-herder', 1).

% Found in: CHK, DD3_EVG, EVG
card_name('akki coalflinger', 'Akki Coalflinger').
card_type('akki coalflinger', 'Creature — Goblin Shaman').
card_types('akki coalflinger', ['Creature']).
card_subtypes('akki coalflinger', ['Goblin', 'Shaman']).
card_colors('akki coalflinger', ['R']).
card_text('akki coalflinger', 'First strike\n{R}, {T}: Attacking creatures gain first strike until end of turn.').
card_mana_cost('akki coalflinger', ['1', 'R', 'R']).
card_cmc('akki coalflinger', 3).
card_layout('akki coalflinger', 'normal').
card_power('akki coalflinger', 2).
card_toughness('akki coalflinger', 2).

% Found in: SOK
card_name('akki drillmaster', 'Akki Drillmaster').
card_type('akki drillmaster', 'Creature — Goblin Shaman').
card_types('akki drillmaster', ['Creature']).
card_subtypes('akki drillmaster', ['Goblin', 'Shaman']).
card_colors('akki drillmaster', ['R']).
card_text('akki drillmaster', '{T}: Target creature gains haste until end of turn.').
card_mana_cost('akki drillmaster', ['2', 'R']).
card_cmc('akki drillmaster', 3).
card_layout('akki drillmaster', 'normal').
card_power('akki drillmaster', 2).
card_toughness('akki drillmaster', 2).

% Found in: CHK
card_name('akki lavarunner', 'Akki Lavarunner').
card_type('akki lavarunner', 'Creature — Goblin Warrior').
card_types('akki lavarunner', ['Creature']).
card_subtypes('akki lavarunner', ['Goblin', 'Warrior']).
card_colors('akki lavarunner', ['R']).
card_text('akki lavarunner', 'Haste\nWhenever Akki Lavarunner deals damage to an opponent, flip it.').
card_mana_cost('akki lavarunner', ['3', 'R']).
card_cmc('akki lavarunner', 4).
card_layout('akki lavarunner', 'flip').
card_power('akki lavarunner', 1).
card_toughness('akki lavarunner', 1).
card_sides('akki lavarunner', 'tok-tok, volcano born').

% Found in: BOK
card_name('akki raider', 'Akki Raider').
card_type('akki raider', 'Creature — Goblin Warrior').
card_types('akki raider', ['Creature']).
card_subtypes('akki raider', ['Goblin', 'Warrior']).
card_colors('akki raider', ['R']).
card_text('akki raider', 'Whenever a land is put into a graveyard from the battlefield, Akki Raider gets +1/+0 until end of turn.').
card_mana_cost('akki raider', ['1', 'R']).
card_cmc('akki raider', 2).
card_layout('akki raider', 'normal').
card_power('akki raider', 2).
card_toughness('akki raider', 1).

% Found in: CHK
card_name('akki rockspeaker', 'Akki Rockspeaker').
card_type('akki rockspeaker', 'Creature — Goblin Shaman').
card_types('akki rockspeaker', ['Creature']).
card_subtypes('akki rockspeaker', ['Goblin', 'Shaman']).
card_colors('akki rockspeaker', ['R']).
card_text('akki rockspeaker', 'When Akki Rockspeaker enters the battlefield, add {R} to your mana pool.').
card_mana_cost('akki rockspeaker', ['1', 'R']).
card_cmc('akki rockspeaker', 2).
card_layout('akki rockspeaker', 'normal').
card_power('akki rockspeaker', 1).
card_toughness('akki rockspeaker', 1).

% Found in: SOK
card_name('akki underling', 'Akki Underling').
card_type('akki underling', 'Creature — Goblin Warrior').
card_types('akki underling', ['Creature']).
card_subtypes('akki underling', ['Goblin', 'Warrior']).
card_colors('akki underling', ['R']).
card_text('akki underling', 'As long as you have seven or more cards in hand, Akki Underling gets +2/+1 and has first strike.').
card_mana_cost('akki underling', ['1', 'R']).
card_cmc('akki underling', 2).
card_layout('akki underling', 'normal').
card_power('akki underling', 2).
card_toughness('akki underling', 1).

% Found in: CHK
card_name('akki underminer', 'Akki Underminer').
card_type('akki underminer', 'Creature — Goblin Rogue Shaman').
card_types('akki underminer', ['Creature']).
card_subtypes('akki underminer', ['Goblin', 'Rogue', 'Shaman']).
card_colors('akki underminer', ['R']).
card_text('akki underminer', 'Whenever Akki Underminer deals combat damage to a player, that player sacrifices a permanent.').
card_mana_cost('akki underminer', ['3', 'R']).
card_cmc('akki underminer', 4).
card_layout('akki underminer', 'normal').
card_power('akki underminer', 1).
card_toughness('akki underminer', 1).

% Found in: PC2
card_name('akoum', 'Akoum').
card_type('akoum', 'Plane — Zendikar').
card_types('akoum', ['Plane']).
card_subtypes('akoum', ['Zendikar']).
card_colors('akoum', []).
card_text('akoum', 'Players may cast enchantment cards as though they had flash.\nWhenever you roll {C}, destroy target creature that isn\'t enchanted.').
card_layout('akoum', 'plane').

% Found in: WWK
card_name('akoum battlesinger', 'Akoum Battlesinger').
card_type('akoum battlesinger', 'Creature — Human Berserker Ally').
card_types('akoum battlesinger', ['Creature']).
card_subtypes('akoum battlesinger', ['Human', 'Berserker', 'Ally']).
card_colors('akoum battlesinger', ['R']).
card_text('akoum battlesinger', 'Haste\nWhenever Akoum Battlesinger or another Ally enters the battlefield under your control, you may have Ally creatures you control get +1/+0 until end of turn.').
card_mana_cost('akoum battlesinger', ['1', 'R']).
card_cmc('akoum battlesinger', 2).
card_layout('akoum battlesinger', 'normal').
card_power('akoum battlesinger', 1).
card_toughness('akoum battlesinger', 1).

% Found in: ROE
card_name('akoum boulderfoot', 'Akoum Boulderfoot').
card_type('akoum boulderfoot', 'Creature — Giant Warrior').
card_types('akoum boulderfoot', ['Creature']).
card_subtypes('akoum boulderfoot', ['Giant', 'Warrior']).
card_colors('akoum boulderfoot', ['R']).
card_text('akoum boulderfoot', 'When Akoum Boulderfoot enters the battlefield, it deals 1 damage to target creature or player.').
card_mana_cost('akoum boulderfoot', ['4', 'R', 'R']).
card_cmc('akoum boulderfoot', 6).
card_layout('akoum boulderfoot', 'normal').
card_power('akoum boulderfoot', 4).
card_toughness('akoum boulderfoot', 5).

% Found in: BFZ
card_name('akoum firebird', 'Akoum Firebird').
card_type('akoum firebird', 'Creature — Phoenix').
card_types('akoum firebird', ['Creature']).
card_subtypes('akoum firebird', ['Phoenix']).
card_colors('akoum firebird', ['R']).
card_text('akoum firebird', 'Flying, haste\nAkoum Firebird attacks each turn if able.\nLandfall — Whenever a land enters the battlefield under your control, you may pay {4}{R}{R}. If you do, return Akoum Firebird from your graveyard to the battlefield.').
card_mana_cost('akoum firebird', ['2', 'R', 'R']).
card_cmc('akoum firebird', 4).
card_layout('akoum firebird', 'normal').
card_power('akoum firebird', 3).
card_toughness('akoum firebird', 3).

% Found in: BFZ
card_name('akoum hellkite', 'Akoum Hellkite').
card_type('akoum hellkite', 'Creature — Dragon').
card_types('akoum hellkite', ['Creature']).
card_subtypes('akoum hellkite', ['Dragon']).
card_colors('akoum hellkite', ['R']).
card_text('akoum hellkite', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, Akoum Hellkite deals 1 damage to target creature or player. If that land is a Mountain, Akoum Hellkite deals 2 damage to that creature or player instead.').
card_mana_cost('akoum hellkite', ['4', 'R', 'R']).
card_cmc('akoum hellkite', 6).
card_layout('akoum hellkite', 'normal').
card_power('akoum hellkite', 4).
card_toughness('akoum hellkite', 4).

% Found in: C13, CMD, DDK, DDP, ZEN
card_name('akoum refuge', 'Akoum Refuge').
card_type('akoum refuge', 'Land').
card_types('akoum refuge', ['Land']).
card_subtypes('akoum refuge', []).
card_colors('akoum refuge', []).
card_text('akoum refuge', 'Akoum Refuge enters the battlefield tapped.\nWhen Akoum Refuge enters the battlefield, you gain 1 life.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('akoum refuge', 'normal').

% Found in: BFZ
card_name('akoum stonewaker', 'Akoum Stonewaker').
card_type('akoum stonewaker', 'Creature — Human Shaman').
card_types('akoum stonewaker', ['Creature']).
card_subtypes('akoum stonewaker', ['Human', 'Shaman']).
card_colors('akoum stonewaker', ['R']).
card_text('akoum stonewaker', 'Landfall — Whenever a land enters the battlefield under your control, you may pay {2}{R}. If you do, put a 3/1 red Elemental creature token with trample and haste onto the battlefield. Exile that token at the beginning of the next end step.').
card_mana_cost('akoum stonewaker', ['1', 'R']).
card_cmc('akoum stonewaker', 2).
card_layout('akoum stonewaker', 'normal').
card_power('akoum stonewaker', 2).
card_toughness('akoum stonewaker', 1).

% Found in: ALA
card_name('akrasan squire', 'Akrasan Squire').
card_type('akrasan squire', 'Creature — Human Soldier').
card_types('akrasan squire', ['Creature']).
card_subtypes('akrasan squire', ['Human', 'Soldier']).
card_colors('akrasan squire', ['W']).
card_text('akrasan squire', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('akrasan squire', ['W']).
card_cmc('akrasan squire', 1).
card_layout('akrasan squire', 'normal').
card_power('akrasan squire', 1).
card_toughness('akrasan squire', 1).

% Found in: BNG
card_name('akroan conscriptor', 'Akroan Conscriptor').
card_type('akroan conscriptor', 'Creature — Human Shaman').
card_types('akroan conscriptor', ['Creature']).
card_subtypes('akroan conscriptor', ['Human', 'Shaman']).
card_colors('akroan conscriptor', ['R']).
card_text('akroan conscriptor', 'Heroic — Whenever you cast a spell that targets Akroan Conscriptor, gain control of another target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_mana_cost('akroan conscriptor', ['4', 'R']).
card_cmc('akroan conscriptor', 5).
card_layout('akroan conscriptor', 'normal').
card_power('akroan conscriptor', 3).
card_toughness('akroan conscriptor', 2).

% Found in: THS
card_name('akroan crusader', 'Akroan Crusader').
card_type('akroan crusader', 'Creature — Human Soldier').
card_types('akroan crusader', ['Creature']).
card_subtypes('akroan crusader', ['Human', 'Soldier']).
card_colors('akroan crusader', ['R']).
card_text('akroan crusader', 'Heroic — Whenever you cast a spell that targets Akroan Crusader, put a 1/1 red Soldier creature token with haste onto the battlefield.').
card_mana_cost('akroan crusader', ['R']).
card_cmc('akroan crusader', 1).
card_layout('akroan crusader', 'normal').
card_power('akroan crusader', 1).
card_toughness('akroan crusader', 1).

% Found in: THS
card_name('akroan hoplite', 'Akroan Hoplite').
card_type('akroan hoplite', 'Creature — Human Soldier').
card_types('akroan hoplite', ['Creature']).
card_subtypes('akroan hoplite', ['Human', 'Soldier']).
card_colors('akroan hoplite', ['W', 'R']).
card_text('akroan hoplite', 'Whenever Akroan Hoplite attacks, it gets +X/+0 until end of turn, where X is the number of attacking creatures you control.').
card_mana_cost('akroan hoplite', ['R', 'W']).
card_cmc('akroan hoplite', 2).
card_layout('akroan hoplite', 'normal').
card_power('akroan hoplite', 1).
card_toughness('akroan hoplite', 2).

% Found in: THS
card_name('akroan horse', 'Akroan Horse').
card_type('akroan horse', 'Artifact Creature — Horse').
card_types('akroan horse', ['Artifact', 'Creature']).
card_subtypes('akroan horse', ['Horse']).
card_colors('akroan horse', []).
card_text('akroan horse', 'Defender\nWhen Akroan Horse enters the battlefield, an opponent gains control of it.\nAt the beginning of your upkeep, each opponent puts a 1/1 white Soldier creature token onto the battlefield.').
card_mana_cost('akroan horse', ['4']).
card_cmc('akroan horse', 4).
card_layout('akroan horse', 'normal').
card_power('akroan horse', 0).
card_toughness('akroan horse', 4).

% Found in: ORI
card_name('akroan jailer', 'Akroan Jailer').
card_type('akroan jailer', 'Creature — Human Soldier').
card_types('akroan jailer', ['Creature']).
card_subtypes('akroan jailer', ['Human', 'Soldier']).
card_colors('akroan jailer', ['W']).
card_text('akroan jailer', '{2}{W}, {T}: Tap target creature.').
card_mana_cost('akroan jailer', ['W']).
card_cmc('akroan jailer', 1).
card_layout('akroan jailer', 'normal').
card_power('akroan jailer', 1).
card_toughness('akroan jailer', 1).

% Found in: JOU
card_name('akroan line breaker', 'Akroan Line Breaker').
card_type('akroan line breaker', 'Creature — Human Warrior').
card_types('akroan line breaker', ['Creature']).
card_subtypes('akroan line breaker', ['Human', 'Warrior']).
card_colors('akroan line breaker', ['R']).
card_text('akroan line breaker', 'Heroic — Whenever you cast a spell that targets Akroan Line Breaker, Akroan Line Breaker gets +2/+0 and gains intimidate until end of turn. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('akroan line breaker', ['2', 'R']).
card_cmc('akroan line breaker', 3).
card_layout('akroan line breaker', 'normal').
card_power('akroan line breaker', 2).
card_toughness('akroan line breaker', 1).

% Found in: JOU
card_name('akroan mastiff', 'Akroan Mastiff').
card_type('akroan mastiff', 'Creature — Hound').
card_types('akroan mastiff', ['Creature']).
card_subtypes('akroan mastiff', ['Hound']).
card_colors('akroan mastiff', ['W']).
card_text('akroan mastiff', '{W}, {T}: Tap target creature.').
card_mana_cost('akroan mastiff', ['3', 'W']).
card_cmc('akroan mastiff', 4).
card_layout('akroan mastiff', 'normal').
card_power('akroan mastiff', 2).
card_toughness('akroan mastiff', 2).

% Found in: BNG
card_name('akroan phalanx', 'Akroan Phalanx').
card_type('akroan phalanx', 'Creature — Human Soldier').
card_types('akroan phalanx', ['Creature']).
card_subtypes('akroan phalanx', ['Human', 'Soldier']).
card_colors('akroan phalanx', ['W']).
card_text('akroan phalanx', 'Vigilance\n{2}{R}: Creatures you control get +1/+0 until end of turn.').
card_mana_cost('akroan phalanx', ['3', 'W']).
card_cmc('akroan phalanx', 4).
card_layout('akroan phalanx', 'normal').
card_power('akroan phalanx', 3).
card_toughness('akroan phalanx', 3).

% Found in: ORI
card_name('akroan sergeant', 'Akroan Sergeant').
card_type('akroan sergeant', 'Creature — Human Soldier').
card_types('akroan sergeant', ['Creature']).
card_subtypes('akroan sergeant', ['Human', 'Soldier']).
card_colors('akroan sergeant', ['R']).
card_text('akroan sergeant', 'First strike (This creature deals combat damage before creatures without first strike.)\nRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_mana_cost('akroan sergeant', ['2', 'R']).
card_cmc('akroan sergeant', 3).
card_layout('akroan sergeant', 'normal').
card_power('akroan sergeant', 2).
card_toughness('akroan sergeant', 2).

% Found in: BNG
card_name('akroan skyguard', 'Akroan Skyguard').
card_type('akroan skyguard', 'Creature — Human Soldier').
card_types('akroan skyguard', ['Creature']).
card_subtypes('akroan skyguard', ['Human', 'Soldier']).
card_colors('akroan skyguard', ['W']).
card_text('akroan skyguard', 'Flying\nHeroic — Whenever you cast a spell that targets Akroan Skyguard, put a +1/+1 counter on Akroan Skyguard.').
card_mana_cost('akroan skyguard', ['1', 'W']).
card_cmc('akroan skyguard', 2).
card_layout('akroan skyguard', 'normal').
card_power('akroan skyguard', 1).
card_toughness('akroan skyguard', 1).

% Found in: ONS, VMA
card_name('akroma\'s blessing', 'Akroma\'s Blessing').
card_type('akroma\'s blessing', 'Instant').
card_types('akroma\'s blessing', ['Instant']).
card_subtypes('akroma\'s blessing', []).
card_colors('akroma\'s blessing', ['W']).
card_text('akroma\'s blessing', 'Choose a color. Creatures you control gain protection from the chosen color until end of turn.\nCycling {W} ({W}, Discard this card: Draw a card.)').
card_mana_cost('akroma\'s blessing', ['2', 'W']).
card_cmc('akroma\'s blessing', 3).
card_layout('akroma\'s blessing', 'normal').

% Found in: LGN
card_name('akroma\'s devoted', 'Akroma\'s Devoted').
card_type('akroma\'s devoted', 'Creature — Human Cleric').
card_types('akroma\'s devoted', ['Creature']).
card_subtypes('akroma\'s devoted', ['Human', 'Cleric']).
card_colors('akroma\'s devoted', ['W']).
card_text('akroma\'s devoted', 'Cleric creatures have vigilance.').
card_mana_cost('akroma\'s devoted', ['3', 'W']).
card_cmc('akroma\'s devoted', 4).
card_layout('akroma\'s devoted', 'normal').
card_power('akroma\'s devoted', 2).
card_toughness('akroma\'s devoted', 4).

% Found in: FUT, M13
card_name('akroma\'s memorial', 'Akroma\'s Memorial').
card_type('akroma\'s memorial', 'Legendary Artifact').
card_types('akroma\'s memorial', ['Artifact']).
card_subtypes('akroma\'s memorial', []).
card_supertypes('akroma\'s memorial', ['Legendary']).
card_colors('akroma\'s memorial', []).
card_text('akroma\'s memorial', 'Creatures you control have flying, first strike, vigilance, trample, haste, and protection from black and from red.').
card_mana_cost('akroma\'s memorial', ['7']).
card_cmc('akroma\'s memorial', 7).
card_layout('akroma\'s memorial', 'normal').

% Found in: CMD, HOP, ONS, V13
card_name('akroma\'s vengeance', 'Akroma\'s Vengeance').
card_type('akroma\'s vengeance', 'Sorcery').
card_types('akroma\'s vengeance', ['Sorcery']).
card_subtypes('akroma\'s vengeance', []).
card_colors('akroma\'s vengeance', ['W']).
card_text('akroma\'s vengeance', 'Destroy all artifacts, creatures, and enchantments.\nCycling {3} ({3}, Discard this card: Draw a card.)').
card_mana_cost('akroma\'s vengeance', ['4', 'W', 'W']).
card_cmc('akroma\'s vengeance', 6).
card_layout('akroma\'s vengeance', 'normal').

% Found in: CMD, PLC, V15
card_name('akroma, angel of fury', 'Akroma, Angel of Fury').
card_type('akroma, angel of fury', 'Legendary Creature — Angel').
card_types('akroma, angel of fury', ['Creature']).
card_subtypes('akroma, angel of fury', ['Angel']).
card_supertypes('akroma, angel of fury', ['Legendary']).
card_colors('akroma, angel of fury', ['R']).
card_text('akroma, angel of fury', 'Akroma, Angel of Fury can\'t be countered.\nFlying, trample, protection from white and from blue\n{R}: Akroma, Angel of Fury gets +1/+0 until end of turn.\nMorph {3}{R}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('akroma, angel of fury', ['5', 'R', 'R', 'R']).
card_cmc('akroma, angel of fury', 8).
card_layout('akroma, angel of fury', 'normal').
card_power('akroma, angel of fury', 6).
card_toughness('akroma, angel of fury', 6).

% Found in: DD3_DVD, DDC, LGN, TSB, V15
card_name('akroma, angel of wrath', 'Akroma, Angel of Wrath').
card_type('akroma, angel of wrath', 'Legendary Creature — Angel').
card_types('akroma, angel of wrath', ['Creature']).
card_subtypes('akroma, angel of wrath', ['Angel']).
card_supertypes('akroma, angel of wrath', ['Legendary']).
card_colors('akroma, angel of wrath', ['W']).
card_text('akroma, angel of wrath', 'Flying, first strike, vigilance, trample, haste, protection from black and from red').
card_mana_cost('akroma, angel of wrath', ['5', 'W', 'W', 'W']).
card_cmc('akroma, angel of wrath', 8).
card_layout('akroma, angel of wrath', 'normal').
card_power('akroma, angel of wrath', 6).
card_toughness('akroma, angel of wrath', 6).

% Found in: VAN
card_name('akroma, angel of wrath avatar', 'Akroma, Angel of Wrath Avatar').
card_type('akroma, angel of wrath avatar', 'Vanguard').
card_types('akroma, angel of wrath avatar', ['Vanguard']).
card_subtypes('akroma, angel of wrath avatar', []).
card_colors('akroma, angel of wrath avatar', []).
card_text('akroma, angel of wrath avatar', 'Whenever a creature enters the battlefield under your control, it gains two abilities chosen at random from flying, first strike, trample, haste, protection from black, protection from red, and vigilance.').
card_layout('akroma, angel of wrath avatar', 'vanguard').

% Found in: 5ED, CHR, LEG, ME3
card_name('akron legionnaire', 'Akron Legionnaire').
card_type('akron legionnaire', 'Creature — Giant Soldier').
card_types('akron legionnaire', ['Creature']).
card_subtypes('akron legionnaire', ['Giant', 'Soldier']).
card_colors('akron legionnaire', ['W']).
card_text('akron legionnaire', 'Except for creatures named Akron Legionnaire and artifact creatures, creatures you control can\'t attack.').
card_mana_cost('akron legionnaire', ['6', 'W', 'W']).
card_cmc('akron legionnaire', 8).
card_layout('akron legionnaire', 'normal').
card_power('akron legionnaire', 8).
card_toughness('akron legionnaire', 4).

% Found in: VIS
card_name('aku djinn', 'Aku Djinn').
card_type('aku djinn', 'Creature — Djinn').
card_types('aku djinn', ['Creature']).
card_subtypes('aku djinn', ['Djinn']).
card_colors('aku djinn', ['B']).
card_text('aku djinn', 'Trample\nAt the beginning of your upkeep, put a +1/+1 counter on each creature each opponent controls.').
card_mana_cost('aku djinn', ['3', 'B', 'B']).
card_cmc('aku djinn', 5).
card_layout('aku djinn', 'normal').
card_power('aku djinn', 5).
card_toughness('aku djinn', 6).
card_reserved('aku djinn').

% Found in: SOK
card_name('akuta, born of ash', 'Akuta, Born of Ash').
card_type('akuta, born of ash', 'Legendary Creature — Spirit').
card_types('akuta, born of ash', ['Creature']).
card_subtypes('akuta, born of ash', ['Spirit']).
card_supertypes('akuta, born of ash', ['Legendary']).
card_colors('akuta, born of ash', ['B']).
card_text('akuta, born of ash', 'Haste\nAt the beginning of your upkeep, if you have more cards in hand than each opponent, you may sacrifice a Swamp. If you do, return Akuta, Born of Ash from your graveyard to the battlefield.').
card_mana_cost('akuta, born of ash', ['2', 'B', 'B']).
card_cmc('akuta, born of ash', 4).
card_layout('akuta, born of ash', 'normal').
card_power('akuta, born of ash', 3).
card_toughness('akuta, born of ash', 2).

% Found in: LEG, ME4
card_name('al-abara\'s carpet', 'Al-abara\'s Carpet').
card_type('al-abara\'s carpet', 'Artifact').
card_types('al-abara\'s carpet', ['Artifact']).
card_subtypes('al-abara\'s carpet', []).
card_colors('al-abara\'s carpet', []).
card_text('al-abara\'s carpet', '{5}, {T}: Prevent all damage that would be dealt to you this turn by attacking creatures without flying.').
card_mana_cost('al-abara\'s carpet', ['5']).
card_cmc('al-abara\'s carpet', 5).
card_layout('al-abara\'s carpet', 'normal').
card_reserved('al-abara\'s carpet').

% Found in: POR, WTH
card_name('alabaster dragon', 'Alabaster Dragon').
card_type('alabaster dragon', 'Creature — Dragon').
card_types('alabaster dragon', ['Creature']).
card_subtypes('alabaster dragon', ['Dragon']).
card_colors('alabaster dragon', ['W']).
card_text('alabaster dragon', 'Flying\nWhen Alabaster Dragon dies, shuffle it into its owner\'s library.').
card_mana_cost('alabaster dragon', ['4', 'W', 'W']).
card_cmc('alabaster dragon', 6).
card_layout('alabaster dragon', 'normal').
card_power('alabaster dragon', 4).
card_toughness('alabaster dragon', 4).

% Found in: KTK
card_name('alabaster kirin', 'Alabaster Kirin').
card_type('alabaster kirin', 'Creature — Kirin').
card_types('alabaster kirin', ['Creature']).
card_subtypes('alabaster kirin', ['Kirin']).
card_colors('alabaster kirin', ['W']).
card_text('alabaster kirin', 'Flying, vigilance').
card_mana_cost('alabaster kirin', ['3', 'W']).
card_cmc('alabaster kirin', 4).
card_layout('alabaster kirin', 'normal').
card_power('alabaster kirin', 2).
card_toughness('alabaster kirin', 3).

% Found in: INV
card_name('alabaster leech', 'Alabaster Leech').
card_type('alabaster leech', 'Creature — Leech').
card_types('alabaster leech', ['Creature']).
card_subtypes('alabaster leech', ['Leech']).
card_colors('alabaster leech', ['W']).
card_text('alabaster leech', 'White spells you cast cost {W} more to cast.').
card_mana_cost('alabaster leech', ['W']).
card_cmc('alabaster leech', 1).
card_layout('alabaster leech', 'normal').
card_power('alabaster leech', 1).
card_toughness('alabaster leech', 3).

% Found in: M12
card_name('alabaster mage', 'Alabaster Mage').
card_type('alabaster mage', 'Creature — Human Wizard').
card_types('alabaster mage', ['Creature']).
card_subtypes('alabaster mage', ['Human', 'Wizard']).
card_colors('alabaster mage', ['W']).
card_text('alabaster mage', '{1}{W}: Target creature you control gains lifelink until end of turn. (Damage dealt by the creature also causes its controller to gain that much life.)').
card_mana_cost('alabaster mage', ['1', 'W']).
card_cmc('alabaster mage', 2).
card_layout('alabaster mage', 'normal').
card_power('alabaster mage', 2).
card_toughness('alabaster mage', 1).

% Found in: 4ED, 5ED, ITP, LEG, ME3, RQS
card_name('alabaster potion', 'Alabaster Potion').
card_type('alabaster potion', 'Instant').
card_types('alabaster potion', ['Instant']).
card_subtypes('alabaster potion', []).
card_colors('alabaster potion', ['W']).
card_text('alabaster potion', 'Choose one —\n• Target player gains X life.\n• Prevent the next X damage that would be dealt to target creature or player this turn.').
card_mana_cost('alabaster potion', ['X', 'W', 'W']).
card_cmc('alabaster potion', 2).
card_layout('alabaster potion', 'normal').

% Found in: MMQ
card_name('alabaster wall', 'Alabaster Wall').
card_type('alabaster wall', 'Creature — Wall').
card_types('alabaster wall', ['Creature']).
card_subtypes('alabaster wall', ['Wall']).
card_colors('alabaster wall', ['W']).
card_text('alabaster wall', 'Defender (This creature can\'t attack.)\n{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('alabaster wall', ['2', 'W']).
card_cmc('alabaster wall', 3).
card_layout('alabaster wall', 'normal').
card_power('alabaster wall', 0).
card_toughness('alabaster wall', 4).

% Found in: DDG, PO2
card_name('alaborn cavalier', 'Alaborn Cavalier').
card_type('alaborn cavalier', 'Creature — Human Knight').
card_types('alaborn cavalier', ['Creature']).
card_subtypes('alaborn cavalier', ['Human', 'Knight']).
card_colors('alaborn cavalier', ['W']).
card_text('alaborn cavalier', 'Whenever Alaborn Cavalier attacks, you may tap target creature.').
card_mana_cost('alaborn cavalier', ['2', 'W', 'W']).
card_cmc('alaborn cavalier', 4).
card_layout('alaborn cavalier', 'normal').
card_power('alaborn cavalier', 2).
card_toughness('alaborn cavalier', 2).

% Found in: PO2
card_name('alaborn grenadier', 'Alaborn Grenadier').
card_type('alaborn grenadier', 'Creature — Human Soldier').
card_types('alaborn grenadier', ['Creature']).
card_subtypes('alaborn grenadier', ['Human', 'Soldier']).
card_colors('alaborn grenadier', ['W']).
card_text('alaborn grenadier', 'Vigilance').
card_mana_cost('alaborn grenadier', ['W', 'W']).
card_cmc('alaborn grenadier', 2).
card_layout('alaborn grenadier', 'normal').
card_power('alaborn grenadier', 2).
card_toughness('alaborn grenadier', 2).

% Found in: ME4, PO2
card_name('alaborn musketeer', 'Alaborn Musketeer').
card_type('alaborn musketeer', 'Creature — Human Soldier').
card_types('alaborn musketeer', ['Creature']).
card_subtypes('alaborn musketeer', ['Human', 'Soldier']).
card_colors('alaborn musketeer', ['W']).
card_text('alaborn musketeer', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('alaborn musketeer', ['1', 'W']).
card_cmc('alaborn musketeer', 2).
card_layout('alaborn musketeer', 'normal').
card_power('alaborn musketeer', 2).
card_toughness('alaborn musketeer', 1).

% Found in: ME4, PO2
card_name('alaborn trooper', 'Alaborn Trooper').
card_type('alaborn trooper', 'Creature — Human Soldier').
card_types('alaborn trooper', ['Creature']).
card_subtypes('alaborn trooper', ['Human', 'Soldier']).
card_colors('alaborn trooper', ['W']).
card_text('alaborn trooper', '').
card_mana_cost('alaborn trooper', ['2', 'W']).
card_cmc('alaborn trooper', 3).
card_layout('alaborn trooper', 'normal').
card_power('alaborn trooper', 2).
card_toughness('alaborn trooper', 3).

% Found in: PO2
card_name('alaborn veteran', 'Alaborn Veteran').
card_type('alaborn veteran', 'Creature — Human Knight').
card_types('alaborn veteran', ['Creature']).
card_subtypes('alaborn veteran', ['Human', 'Knight']).
card_colors('alaborn veteran', ['W']).
card_text('alaborn veteran', '{T}: Target creature gets +2/+2 until end of turn. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('alaborn veteran', ['2', 'W']).
card_cmc('alaborn veteran', 3).
card_layout('alaborn veteran', 'normal').
card_power('alaborn veteran', 2).
card_toughness('alaborn veteran', 2).

% Found in: PO2
card_name('alaborn zealot', 'Alaborn Zealot').
card_type('alaborn zealot', 'Creature — Human Soldier').
card_types('alaborn zealot', ['Creature']).
card_subtypes('alaborn zealot', ['Human', 'Soldier']).
card_colors('alaborn zealot', ['W']).
card_text('alaborn zealot', 'When Alaborn Zealot blocks a creature, destroy that creature and Alaborn Zealot.').
card_mana_cost('alaborn zealot', ['W']).
card_cmc('alaborn zealot', 1).
card_layout('alaborn zealot', 'normal').
card_power('alaborn zealot', 1).
card_toughness('alaborn zealot', 1).

% Found in: ARN, CHR, ME4
card_name('aladdin', 'Aladdin').
card_type('aladdin', 'Creature — Human Rogue').
card_types('aladdin', ['Creature']).
card_subtypes('aladdin', ['Human', 'Rogue']).
card_colors('aladdin', ['R']).
card_text('aladdin', '{1}{R}{R}, {T}: Gain control of target artifact for as long as you control Aladdin.').
card_mana_cost('aladdin', ['2', 'R', 'R']).
card_cmc('aladdin', 4).
card_layout('aladdin', 'normal').
card_power('aladdin', 1).
card_toughness('aladdin', 1).

% Found in: 3ED, 4ED, ARN
card_name('aladdin\'s lamp', 'Aladdin\'s Lamp').
card_type('aladdin\'s lamp', 'Artifact').
card_types('aladdin\'s lamp', ['Artifact']).
card_subtypes('aladdin\'s lamp', []).
card_colors('aladdin\'s lamp', []).
card_text('aladdin\'s lamp', '{X}, {T}: The next time you would draw a card this turn, instead look at the top X cards of your library, put all but one of them on the bottom of your library in a random order, then draw a card. X can\'t be 0.').
card_mana_cost('aladdin\'s lamp', ['10']).
card_cmc('aladdin\'s lamp', 10).
card_layout('aladdin\'s lamp', 'normal').

% Found in: 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ARN
card_name('aladdin\'s ring', 'Aladdin\'s Ring').
card_type('aladdin\'s ring', 'Artifact').
card_types('aladdin\'s ring', ['Artifact']).
card_subtypes('aladdin\'s ring', []).
card_colors('aladdin\'s ring', []).
card_text('aladdin\'s ring', '{8}, {T}: Aladdin\'s Ring deals 4 damage to target creature or player.').
card_mana_cost('aladdin\'s ring', ['8']).
card_cmc('aladdin\'s ring', 8).
card_layout('aladdin\'s ring', 'normal').

% Found in: MIR
card_name('alarum', 'Alarum').
card_type('alarum', 'Instant').
card_types('alarum', ['Instant']).
card_subtypes('alarum', []).
card_colors('alarum', ['W']).
card_text('alarum', 'Untap target nonattacking creature. It gets +1/+3 until end of turn.').
card_mana_cost('alarum', ['1', 'W']).
card_cmc('alarum', 2).
card_layout('alarum', 'normal').

% Found in: DD3_GVL, DDD, USG, pFNM
card_name('albino troll', 'Albino Troll').
card_type('albino troll', 'Creature — Troll').
card_types('albino troll', ['Creature']).
card_subtypes('albino troll', ['Troll']).
card_colors('albino troll', ['G']).
card_text('albino troll', 'Echo {1}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{1}{G}: Regenerate Albino Troll.').
card_mana_cost('albino troll', ['1', 'G']).
card_cmc('albino troll', 2).
card_layout('albino troll', 'normal').
card_power('albino troll', 3).
card_toughness('albino troll', 3).

% Found in: AVR
card_name('alchemist\'s apprentice', 'Alchemist\'s Apprentice').
card_type('alchemist\'s apprentice', 'Creature — Human Wizard').
card_types('alchemist\'s apprentice', ['Creature']).
card_subtypes('alchemist\'s apprentice', ['Human', 'Wizard']).
card_colors('alchemist\'s apprentice', ['U']).
card_text('alchemist\'s apprentice', 'Sacrifice Alchemist\'s Apprentice: Draw a card.').
card_mana_cost('alchemist\'s apprentice', ['1', 'U']).
card_cmc('alchemist\'s apprentice', 2).
card_layout('alchemist\'s apprentice', 'normal').
card_power('alchemist\'s apprentice', 1).
card_toughness('alchemist\'s apprentice', 1).

% Found in: AVR
card_name('alchemist\'s refuge', 'Alchemist\'s Refuge').
card_type('alchemist\'s refuge', 'Land').
card_types('alchemist\'s refuge', ['Land']).
card_subtypes('alchemist\'s refuge', []).
card_colors('alchemist\'s refuge', []).
card_text('alchemist\'s refuge', '{T}: Add {1} to your mana pool.\n{G}{U}, {T}: You may cast nonland cards this turn as though they had flash.').
card_layout('alchemist\'s refuge', 'normal').

% Found in: ORI
card_name('alchemist\'s vial', 'Alchemist\'s Vial').
card_type('alchemist\'s vial', 'Artifact').
card_types('alchemist\'s vial', ['Artifact']).
card_subtypes('alchemist\'s vial', []).
card_colors('alchemist\'s vial', []).
card_text('alchemist\'s vial', 'When Alchemist\'s Vial enters the battlefield, draw a card.\n{1}, {T}, Sacrifice Alchemist\'s Vial: Target creature can\'t attack or block this turn.').
card_mana_cost('alchemist\'s vial', ['2']).
card_cmc('alchemist\'s vial', 2).
card_layout('alchemist\'s vial', 'normal').

% Found in: LEG, ME4
card_name('alchor\'s tomb', 'Alchor\'s Tomb').
card_type('alchor\'s tomb', 'Artifact').
card_types('alchor\'s tomb', ['Artifact']).
card_subtypes('alchor\'s tomb', []).
card_colors('alchor\'s tomb', []).
card_text('alchor\'s tomb', '{2}, {T}: Target permanent you control becomes the color of your choice. (This effect lasts indefinitely.)').
card_mana_cost('alchor\'s tomb', ['4']).
card_cmc('alchor\'s tomb', 4).
card_layout('alchor\'s tomb', 'normal').
card_reserved('alchor\'s tomb').

% Found in: MIR
card_name('aleatory', 'Aleatory').
card_type('aleatory', 'Instant').
card_types('aleatory', ['Instant']).
card_subtypes('aleatory', []).
card_colors('aleatory', ['R']).
card_text('aleatory', 'Cast Aleatory only during combat after blockers are declared.\nFlip a coin. If you win the flip, target creature gets +1/+1 until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('aleatory', ['1', 'R']).
card_cmc('aleatory', 2).
card_layout('aleatory', 'normal').

% Found in: PTK
card_name('alert shu infantry', 'Alert Shu Infantry').
card_type('alert shu infantry', 'Creature — Human Soldier').
card_types('alert shu infantry', ['Creature']).
card_subtypes('alert shu infantry', ['Human', 'Soldier']).
card_colors('alert shu infantry', ['W']).
card_text('alert shu infantry', 'Vigilance').
card_mana_cost('alert shu infantry', ['2', 'W']).
card_cmc('alert shu infantry', 3).
card_layout('alert shu infantry', 'normal').
card_power('alert shu infantry', 2).
card_toughness('alert shu infantry', 2).

% Found in: FRF
card_name('alesha\'s vanguard', 'Alesha\'s Vanguard').
card_type('alesha\'s vanguard', 'Creature — Orc Warrior').
card_types('alesha\'s vanguard', ['Creature']).
card_subtypes('alesha\'s vanguard', ['Orc', 'Warrior']).
card_colors('alesha\'s vanguard', ['B']).
card_text('alesha\'s vanguard', 'Dash {2}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('alesha\'s vanguard', ['3', 'B']).
card_cmc('alesha\'s vanguard', 4).
card_layout('alesha\'s vanguard', 'normal').
card_power('alesha\'s vanguard', 3).
card_toughness('alesha\'s vanguard', 3).

% Found in: FRF
card_name('alesha, who smiles at death', 'Alesha, Who Smiles at Death').
card_type('alesha, who smiles at death', 'Legendary Creature — Human Warrior').
card_types('alesha, who smiles at death', ['Creature']).
card_subtypes('alesha, who smiles at death', ['Human', 'Warrior']).
card_supertypes('alesha, who smiles at death', ['Legendary']).
card_colors('alesha, who smiles at death', ['R']).
card_text('alesha, who smiles at death', 'First strike\nWhenever Alesha, Who Smiles at Death attacks, you may pay {W/B}{W/B}. If you do, return target creature card with power 2 or less from your graveyard to the battlefield tapped and attacking.').
card_mana_cost('alesha, who smiles at death', ['2', 'R']).
card_cmc('alesha, who smiles at death', 3).
card_layout('alesha, who smiles at death', 'normal').
card_power('alesha, who smiles at death', 3).
card_toughness('alesha, who smiles at death', 2).

% Found in: PCY
card_name('alexi\'s cloak', 'Alexi\'s Cloak').
card_type('alexi\'s cloak', 'Enchantment — Aura').
card_types('alexi\'s cloak', ['Enchantment']).
card_subtypes('alexi\'s cloak', ['Aura']).
card_colors('alexi\'s cloak', ['U']).
card_text('alexi\'s cloak', 'Flash\nEnchant creature\nEnchanted creature has shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('alexi\'s cloak', ['1', 'U']).
card_cmc('alexi\'s cloak', 2).
card_layout('alexi\'s cloak', 'normal').

% Found in: PCY
card_name('alexi, zephyr mage', 'Alexi, Zephyr Mage').
card_type('alexi, zephyr mage', 'Legendary Creature — Human Spellshaper').
card_types('alexi, zephyr mage', ['Creature']).
card_subtypes('alexi, zephyr mage', ['Human', 'Spellshaper']).
card_supertypes('alexi, zephyr mage', ['Legendary']).
card_colors('alexi, zephyr mage', ['U']).
card_text('alexi, zephyr mage', '{X}{U}, {T}, Discard two cards: Return X target creatures to their owners\' hands.').
card_mana_cost('alexi, zephyr mage', ['3', 'U', 'U']).
card_cmc('alexi, zephyr mage', 5).
card_layout('alexi, zephyr mage', 'normal').
card_power('alexi, zephyr mage', 3).
card_toughness('alexi, zephyr mage', 3).

% Found in: ALA, MM2
card_name('algae gharial', 'Algae Gharial').
card_type('algae gharial', 'Creature — Crocodile').
card_types('algae gharial', ['Creature']).
card_subtypes('algae gharial', ['Crocodile']).
card_colors('algae gharial', ['G']).
card_text('algae gharial', 'Shroud (This creature can\'t be the target of spells or abilities.)\nWhenever another creature dies, you may put a +1/+1 counter on Algae Gharial.').
card_mana_cost('algae gharial', ['3', 'G']).
card_cmc('algae gharial', 4).
card_layout('algae gharial', 'normal').
card_power('algae gharial', 1).
card_toughness('algae gharial', 1).

% Found in: ORI
card_name('alhammarret\'s archive', 'Alhammarret\'s Archive').
card_type('alhammarret\'s archive', 'Legendary Artifact').
card_types('alhammarret\'s archive', ['Artifact']).
card_subtypes('alhammarret\'s archive', []).
card_supertypes('alhammarret\'s archive', ['Legendary']).
card_colors('alhammarret\'s archive', []).
card_text('alhammarret\'s archive', 'If you would gain life, you gain twice that much life instead.\nIf you would draw a card except the first one you draw in each of your draw steps, draw two cards instead.').
card_mana_cost('alhammarret\'s archive', ['5']).
card_cmc('alhammarret\'s archive', 5).
card_layout('alhammarret\'s archive', 'normal').

% Found in: ORI
card_name('alhammarret, high arbiter', 'Alhammarret, High Arbiter').
card_type('alhammarret, high arbiter', 'Legendary Creature — Sphinx').
card_types('alhammarret, high arbiter', ['Creature']).
card_subtypes('alhammarret, high arbiter', ['Sphinx']).
card_supertypes('alhammarret, high arbiter', ['Legendary']).
card_colors('alhammarret, high arbiter', ['U']).
card_text('alhammarret, high arbiter', 'Flying\nAs Alhammarret, High Arbiter enters the battlefield, each opponent reveals his or her hand. You choose the name of a nonland card revealed this way.\nYour opponents can\'t cast spells with the chosen name (as long as this creature is on the battlefield).').
card_mana_cost('alhammarret, high arbiter', ['5', 'U', 'U']).
card_cmc('alhammarret, high arbiter', 7).
card_layout('alhammarret, high arbiter', 'normal').
card_power('alhammarret, high arbiter', 5).
card_toughness('alhammarret, high arbiter', 5).

% Found in: 4ED, ARN
card_name('ali baba', 'Ali Baba').
card_type('ali baba', 'Creature — Human Rogue').
card_types('ali baba', ['Creature']).
card_subtypes('ali baba', ['Human', 'Rogue']).
card_colors('ali baba', ['R']).
card_text('ali baba', '{R}: Tap target Wall.').
card_mana_cost('ali baba', ['R']).
card_cmc('ali baba', 1).
card_layout('ali baba', 'normal').
card_power('ali baba', 1).
card_toughness('ali baba', 1).

% Found in: ARN, ME4
card_name('ali from cairo', 'Ali from Cairo').
card_type('ali from cairo', 'Creature — Human').
card_types('ali from cairo', ['Creature']).
card_subtypes('ali from cairo', ['Human']).
card_colors('ali from cairo', ['R']).
card_text('ali from cairo', 'Damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_mana_cost('ali from cairo', ['2', 'R', 'R']).
card_cmc('ali from cairo', 4).
card_layout('ali from cairo', 'normal').
card_power('ali from cairo', 0).
card_toughness('ali from cairo', 1).
card_reserved('ali from cairo').

% Found in: HML
card_name('aliban\'s tower', 'Aliban\'s Tower').
card_type('aliban\'s tower', 'Instant').
card_types('aliban\'s tower', ['Instant']).
card_subtypes('aliban\'s tower', []).
card_colors('aliban\'s tower', ['R']).
card_text('aliban\'s tower', 'Target blocking creature gets +3/+1 until end of turn.').
card_mana_cost('aliban\'s tower', ['1', 'R']).
card_cmc('aliban\'s tower', 2).
card_layout('aliban\'s tower', 'normal').

% Found in: BFZ
card_name('aligned hedron network', 'Aligned Hedron Network').
card_type('aligned hedron network', 'Artifact').
card_types('aligned hedron network', ['Artifact']).
card_subtypes('aligned hedron network', []).
card_colors('aligned hedron network', []).
card_text('aligned hedron network', 'When Aligned Hedron Network enters the battlefield, exile all creatures with power 5 or greater until Aligned Hedron Network leaves the battlefield. (Those creatures return under their owners\' control.)').
card_mana_cost('aligned hedron network', ['4']).
card_cmc('aligned hedron network', 4).
card_layout('aligned hedron network', 'normal').

% Found in: DGM
card_name('alive', 'Alive').
card_type('alive', 'Sorcery').
card_types('alive', ['Sorcery']).
card_subtypes('alive', []).
card_colors('alive', ['G']).
card_text('alive', 'Put a 3/3 green Centaur creature token onto the battlefield.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('alive', ['3', 'G']).
card_cmc('alive', 4).
card_layout('alive', 'split').
card_sides('alive', 'well').

% Found in: LEG, ME3
card_name('all hallow\'s eve', 'All Hallow\'s Eve').
card_type('all hallow\'s eve', 'Sorcery').
card_types('all hallow\'s eve', ['Sorcery']).
card_subtypes('all hallow\'s eve', []).
card_colors('all hallow\'s eve', ['B']).
card_text('all hallow\'s eve', 'Exile All Hallow\'s Eve with two scream counters on it.\nAt the beginning of your upkeep, if All Hallow\'s Eve is exiled with a scream counter on it, remove a scream counter from it. If there are no more scream counters on it, put it into your graveyard and each player returns all creature cards from his or her graveyard to the battlefield.').
card_mana_cost('all hallow\'s eve', ['2', 'B', 'B']).
card_cmc('all hallow\'s eve', 4).
card_layout('all hallow\'s eve', 'normal').
card_reserved('all hallow\'s eve').

% Found in: ARC
card_name('all in good time', 'All in Good Time').
card_type('all in good time', 'Scheme').
card_types('all in good time', ['Scheme']).
card_subtypes('all in good time', []).
card_colors('all in good time', []).
card_text('all in good time', 'When you set this scheme in motion, take an extra turn after this one. Schemes can\'t be set in motion that turn.').
card_layout('all in good time', 'scheme').

% Found in: MM2, ROE, pGPX
card_name('all is dust', 'All Is Dust').
card_type('all is dust', 'Tribal Sorcery — Eldrazi').
card_types('all is dust', ['Tribal', 'Sorcery']).
card_subtypes('all is dust', ['Eldrazi']).
card_colors('all is dust', []).
card_text('all is dust', 'Each player sacrifices all colored permanents he or she controls.').
card_mana_cost('all is dust', ['7']).
card_cmc('all is dust', 7).
card_layout('all is dust', 'normal').

% Found in: ARC
card_name('all shall smolder in my wake', 'All Shall Smolder in My Wake').
card_type('all shall smolder in my wake', 'Scheme').
card_types('all shall smolder in my wake', ['Scheme']).
card_subtypes('all shall smolder in my wake', []).
card_colors('all shall smolder in my wake', []).
card_text('all shall smolder in my wake', 'When you set this scheme in motion, destroy up to one target artifact, up to one target enchantment, and up to one target nonbasic land.').
card_layout('all shall smolder in my wake', 'scheme').

% Found in: 5DN, MM2
card_name('all suns\' dawn', 'All Suns\' Dawn').
card_type('all suns\' dawn', 'Sorcery').
card_types('all suns\' dawn', ['Sorcery']).
card_subtypes('all suns\' dawn', []).
card_colors('all suns\' dawn', ['G']).
card_text('all suns\' dawn', 'For each color, return up to one target card of that color from your graveyard to your hand. Exile All Suns\' Dawn.').
card_mana_cost('all suns\' dawn', ['4', 'G']).
card_cmc('all suns\' dawn', 5).
card_layout('all suns\' dawn', 'normal').

% Found in: EXO
card_name('allay', 'Allay').
card_type('allay', 'Instant').
card_types('allay', ['Instant']).
card_subtypes('allay', []).
card_colors('allay', ['W']).
card_text('allay', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nDestroy target enchantment.').
card_mana_cost('allay', ['1', 'W']).
card_cmc('allay', 2).
card_layout('allay', 'normal').

% Found in: MMQ
card_name('alley grifters', 'Alley Grifters').
card_type('alley grifters', 'Creature — Human Mercenary').
card_types('alley grifters', ['Creature']).
card_subtypes('alley grifters', ['Human', 'Mercenary']).
card_colors('alley grifters', ['B']).
card_text('alley grifters', 'Whenever Alley Grifters becomes blocked, defending player discards a card.').
card_mana_cost('alley grifters', ['1', 'B', 'B']).
card_cmc('alley grifters', 3).
card_layout('alley grifters', 'normal').
card_power('alley grifters', 2).
card_toughness('alley grifters', 2).

% Found in: CMD
card_name('alliance of arms', 'Alliance of Arms').
card_type('alliance of arms', 'Sorcery').
card_types('alliance of arms', ['Sorcery']).
card_subtypes('alliance of arms', []).
card_colors('alliance of arms', ['W']).
card_text('alliance of arms', 'Join forces — Starting with you, each player may pay any amount of mana. Each player puts X 1/1 white Soldier creature tokens onto the battlefield, where X is the total amount of mana paid this way.').
card_mana_cost('alliance of arms', ['W']).
card_cmc('alliance of arms', 1).
card_layout('alliance of arms', 'normal').

% Found in: DDE, PLS
card_name('allied strategies', 'Allied Strategies').
card_type('allied strategies', 'Sorcery').
card_types('allied strategies', ['Sorcery']).
card_subtypes('allied strategies', []).
card_colors('allied strategies', ['U']).
card_text('allied strategies', 'Domain — Target player draws a card for each basic land type among lands he or she controls.').
card_mana_cost('allied strategies', ['4', 'U']).
card_cmc('allied strategies', 5).
card_layout('allied strategies', 'normal').

% Found in: CSP, DD3_EVG, EVG, pPRE
card_name('allosaurus rider', 'Allosaurus Rider').
card_type('allosaurus rider', 'Creature — Elf Warrior').
card_types('allosaurus rider', ['Creature']).
card_subtypes('allosaurus rider', ['Elf', 'Warrior']).
card_colors('allosaurus rider', ['G']).
card_text('allosaurus rider', 'You may exile two green cards from your hand rather than pay Allosaurus Rider\'s mana cost.\nAllosaurus Rider\'s power and toughness are each equal to 1 plus the number of lands you control.').
card_mana_cost('allosaurus rider', ['5', 'G', 'G']).
card_cmc('allosaurus rider', 7).
card_layout('allosaurus rider', 'normal').
card_power('allosaurus rider', '1+*').
card_toughness('allosaurus rider', '1+*').

% Found in: INV
card_name('alloy golem', 'Alloy Golem').
card_type('alloy golem', 'Artifact Creature — Golem').
card_types('alloy golem', ['Artifact', 'Creature']).
card_subtypes('alloy golem', ['Golem']).
card_colors('alloy golem', []).
card_text('alloy golem', 'As Alloy Golem enters the battlefield, choose a color.\nAlloy Golem is the chosen color. (It\'s still an artifact.)').
card_mana_cost('alloy golem', ['6']).
card_cmc('alloy golem', 6).
card_layout('alloy golem', 'normal').
card_power('alloy golem', 4).
card_toughness('alloy golem', 4).

% Found in: MM2, NPH
card_name('alloy myr', 'Alloy Myr').
card_type('alloy myr', 'Artifact Creature — Myr').
card_types('alloy myr', ['Artifact', 'Creature']).
card_subtypes('alloy myr', ['Myr']).
card_colors('alloy myr', []).
card_text('alloy myr', '{T}: Add one mana of any color to your mana pool.').
card_mana_cost('alloy myr', ['3']).
card_cmc('alloy myr', 3).
card_layout('alloy myr', 'normal').
card_power('alloy myr', 2).
card_toughness('alloy myr', 2).

% Found in: ME4, PO2, POR, S99
card_name('alluring scent', 'Alluring Scent').
card_type('alluring scent', 'Sorcery').
card_types('alluring scent', ['Sorcery']).
card_subtypes('alluring scent', []).
card_colors('alluring scent', ['G']).
card_text('alluring scent', 'All creatures able to block target creature this turn do so.').
card_mana_cost('alluring scent', ['1', 'G', 'G']).
card_cmc('alluring scent', 3).
card_layout('alluring scent', 'normal').

% Found in: M10, M11, M12
card_name('alluring siren', 'Alluring Siren').
card_type('alluring siren', 'Creature — Siren').
card_types('alluring siren', ['Creature']).
card_subtypes('alluring siren', ['Siren']).
card_colors('alluring siren', ['U']).
card_text('alluring siren', '{T}: Target creature an opponent controls attacks you this turn if able.').
card_mana_cost('alluring siren', ['1', 'U']).
card_cmc('alluring siren', 2).
card_layout('alluring siren', 'normal').
card_power('alluring siren', 1).
card_toughness('alluring siren', 1).

% Found in: BFZ
card_name('ally encampment', 'Ally Encampment').
card_type('ally encampment', 'Land').
card_types('ally encampment', ['Land']).
card_subtypes('ally encampment', []).
card_colors('ally encampment', []).
card_text('ally encampment', '{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast an Ally spell.\n{1}, {T}, Sacrifice Ally Encampment: Return target Ally you control to its owner\'s hand.').
card_layout('ally encampment', 'normal').

% Found in: WTH
card_name('alms', 'Alms').
card_type('alms', 'Enchantment').
card_types('alms', ['Enchantment']).
card_subtypes('alms', []).
card_colors('alms', ['W']).
card_text('alms', '{1}, Exile the top card of your graveyard: Prevent the next 1 damage that would be dealt to target creature this turn.').
card_mana_cost('alms', ['W']).
card_cmc('alms', 1).
card_layout('alms', 'normal').

% Found in: GTC
card_name('alms beast', 'Alms Beast').
card_type('alms beast', 'Creature — Beast').
card_types('alms beast', ['Creature']).
card_subtypes('alms beast', ['Beast']).
card_colors('alms beast', ['W', 'B']).
card_text('alms beast', 'Creatures blocking or blocked by Alms Beast have lifelink.').
card_mana_cost('alms beast', ['2', 'W', 'B']).
card_cmc('alms beast', 4).
card_layout('alms beast', 'normal').
card_power('alms beast', 6).
card_toughness('alms beast', 6).

% Found in: GTC
card_name('alpha authority', 'Alpha Authority').
card_type('alpha authority', 'Enchantment — Aura').
card_types('alpha authority', ['Enchantment']).
card_subtypes('alpha authority', ['Aura']).
card_colors('alpha authority', ['G']).
card_text('alpha authority', 'Enchant creature\nEnchanted creature has hexproof and can\'t be blocked by more than one creature.').
card_mana_cost('alpha authority', ['1', 'G']).
card_cmc('alpha authority', 2).
card_layout('alpha authority', 'normal').

% Found in: DKA
card_name('alpha brawl', 'Alpha Brawl').
card_type('alpha brawl', 'Sorcery').
card_types('alpha brawl', ['Sorcery']).
card_subtypes('alpha brawl', []).
card_colors('alpha brawl', ['R']).
card_text('alpha brawl', 'Target creature an opponent controls deals damage equal to its power to each other creature that player controls, then each of those creatures deals damage equal to its power to that creature.').
card_mana_cost('alpha brawl', ['6', 'R', 'R']).
card_cmc('alpha brawl', 8).
card_layout('alpha brawl', 'normal').

% Found in: PLS
card_name('alpha kavu', 'Alpha Kavu').
card_type('alpha kavu', 'Creature — Kavu').
card_types('alpha kavu', ['Creature']).
card_subtypes('alpha kavu', ['Kavu']).
card_colors('alpha kavu', ['G']).
card_text('alpha kavu', '{1}{G}: Target Kavu creature gets -1/+1 until end of turn.').
card_mana_cost('alpha kavu', ['2', 'G']).
card_cmc('alpha kavu', 3).
card_layout('alpha kavu', 'normal').
card_power('alpha kavu', 2).
card_toughness('alpha kavu', 2).

% Found in: MRD
card_name('alpha myr', 'Alpha Myr').
card_type('alpha myr', 'Artifact Creature — Myr').
card_types('alpha myr', ['Artifact', 'Creature']).
card_subtypes('alpha myr', ['Myr']).
card_colors('alpha myr', []).
card_text('alpha myr', '').
card_mana_cost('alpha myr', ['2']).
card_cmc('alpha myr', 2).
card_layout('alpha myr', 'normal').
card_power('alpha myr', 2).
card_toughness('alpha myr', 1).

% Found in: SCG
card_name('alpha status', 'Alpha Status').
card_type('alpha status', 'Enchantment — Aura').
card_types('alpha status', ['Enchantment']).
card_subtypes('alpha status', ['Aura']).
card_colors('alpha status', ['G']).
card_text('alpha status', 'Enchant creature\nEnchanted creature gets +2/+2 for each other creature on the battlefield that shares a creature type with it.').
card_mana_cost('alpha status', ['2', 'G']).
card_cmc('alpha status', 3).
card_layout('alpha status', 'normal').

% Found in: SOM
card_name('alpha tyrranax', 'Alpha Tyrranax').
card_type('alpha tyrranax', 'Creature — Beast').
card_types('alpha tyrranax', ['Creature']).
card_subtypes('alpha tyrranax', ['Beast']).
card_colors('alpha tyrranax', ['G']).
card_text('alpha tyrranax', '').
card_mana_cost('alpha tyrranax', ['4', 'G', 'G']).
card_cmc('alpha tyrranax', 6).
card_layout('alpha tyrranax', 'normal').
card_power('alpha tyrranax', 6).
card_toughness('alpha tyrranax', 5).

% Found in: KTK
card_name('alpine grizzly', 'Alpine Grizzly').
card_type('alpine grizzly', 'Creature — Bear').
card_types('alpine grizzly', ['Creature']).
card_subtypes('alpine grizzly', ['Bear']).
card_colors('alpine grizzly', ['G']).
card_text('alpine grizzly', '').
card_mana_cost('alpine grizzly', ['2', 'G']).
card_cmc('alpine grizzly', 3).
card_layout('alpine grizzly', 'normal').
card_power('alpine grizzly', 4).
card_toughness('alpine grizzly', 2).

% Found in: M15
card_name('altac bloodseeker', 'Altac Bloodseeker').
card_type('altac bloodseeker', 'Creature — Human Berserker').
card_types('altac bloodseeker', ['Creature']).
card_subtypes('altac bloodseeker', ['Human', 'Berserker']).
card_colors('altac bloodseeker', ['R']).
card_text('altac bloodseeker', 'Whenever a creature an opponent controls dies, Altac Bloodseeker gets +2/+0 and gains first strike and haste until end of turn. (It deals combat damage before creatures without first strike, and it can attack and {T} as soon as it comes under your control.)').
card_mana_cost('altac bloodseeker', ['1', 'R']).
card_cmc('altac bloodseeker', 2).
card_layout('altac bloodseeker', 'normal').
card_power('altac bloodseeker', 2).
card_toughness('altac bloodseeker', 1).

% Found in: EVE
card_name('altar golem', 'Altar Golem').
card_type('altar golem', 'Artifact Creature — Golem').
card_types('altar golem', ['Artifact', 'Creature']).
card_subtypes('altar golem', ['Golem']).
card_colors('altar golem', []).
card_text('altar golem', 'Trample\nAltar Golem\'s power and toughness are each equal to the number of creatures on the battlefield.\nAltar Golem doesn\'t untap during your untap step.\nTap five untapped creatures you control: Untap Altar Golem.').
card_mana_cost('altar golem', ['7']).
card_cmc('altar golem', 7).
card_layout('altar golem', 'normal').
card_power('altar golem', '*').
card_toughness('altar golem', '*').

% Found in: ICE
card_name('altar of bone', 'Altar of Bone').
card_type('altar of bone', 'Sorcery').
card_types('altar of bone', ['Sorcery']).
card_subtypes('altar of bone', []).
card_colors('altar of bone', ['W', 'G']).
card_text('altar of bone', 'As an additional cost to cast Altar of Bone, sacrifice a creature.\nSearch your library for a creature card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('altar of bone', ['G', 'W']).
card_cmc('altar of bone', 2).
card_layout('altar of bone', 'normal').
card_reserved('altar of bone').

% Found in: CNS, TMP
card_name('altar of dementia', 'Altar of Dementia').
card_type('altar of dementia', 'Artifact').
card_types('altar of dementia', ['Artifact']).
card_subtypes('altar of dementia', []).
card_colors('altar of dementia', []).
card_text('altar of dementia', 'Sacrifice a creature: Target player puts a number of cards equal to the sacrificed creature\'s power from the top of his or her library into his or her graveyard.').
card_mana_cost('altar of dementia', ['2']).
card_cmc('altar of dementia', 2).
card_layout('altar of dementia', 'normal').

% Found in: MRD
card_name('altar of shadows', 'Altar of Shadows').
card_type('altar of shadows', 'Artifact').
card_types('altar of shadows', ['Artifact']).
card_subtypes('altar of shadows', []).
card_colors('altar of shadows', []).
card_text('altar of shadows', 'At the beginning of your precombat main phase, add {B} to your mana pool for each charge counter on Altar of Shadows.\n{7}, {T}: Destroy target creature. Then put a charge counter on Altar of Shadows.').
card_mana_cost('altar of shadows', ['7']).
card_cmc('altar of shadows', 7).
card_layout('altar of shadows', 'normal').

% Found in: FRF_UGIN, KTK
card_name('altar of the brood', 'Altar of the Brood').
card_type('altar of the brood', 'Artifact').
card_types('altar of the brood', ['Artifact']).
card_subtypes('altar of the brood', []).
card_colors('altar of the brood', []).
card_text('altar of the brood', 'Whenever another permanent enters the battlefield under your control, each opponent puts the top card of his or her library into his or her graveyard.').
card_mana_cost('altar of the brood', ['1']).
card_cmc('altar of the brood', 1).
card_layout('altar of the brood', 'normal').

% Found in: DKA
card_name('altar of the lost', 'Altar of the Lost').
card_type('altar of the lost', 'Artifact').
card_types('altar of the lost', ['Artifact']).
card_subtypes('altar of the lost', []).
card_colors('altar of the lost', []).
card_text('altar of the lost', 'Altar of the Lost enters the battlefield tapped.\n{T}: Add two mana in any combination of colors to your mana pool. Spend this mana only to cast spells with flashback from a graveyard.').
card_mana_cost('altar of the lost', ['3']).
card_cmc('altar of the lost', 3).
card_layout('altar of the lost', 'normal').

% Found in: MRD
card_name('altar\'s light', 'Altar\'s Light').
card_type('altar\'s light', 'Instant').
card_types('altar\'s light', ['Instant']).
card_subtypes('altar\'s light', []).
card_colors('altar\'s light', ['W']).
card_text('altar\'s light', 'Exile target artifact or enchantment.').
card_mana_cost('altar\'s light', ['2', 'W', 'W']).
card_cmc('altar\'s light', 4).
card_layout('altar\'s light', 'normal').

% Found in: BFZ, CNS, ISD, M14
card_name('altar\'s reap', 'Altar\'s Reap').
card_type('altar\'s reap', 'Instant').
card_types('altar\'s reap', ['Instant']).
card_subtypes('altar\'s reap', []).
card_colors('altar\'s reap', ['B']).
card_text('altar\'s reap', 'As an additional cost to cast Altar\'s Reap, sacrifice a creature.\nDraw two cards.').
card_mana_cost('altar\'s reap', ['1', 'B']).
card_cmc('altar\'s reap', 2).
card_layout('altar\'s reap', 'normal').

% Found in: TOR
card_name('alter reality', 'Alter Reality').
card_type('alter reality', 'Instant').
card_types('alter reality', ['Instant']).
card_subtypes('alter reality', []).
card_colors('alter reality', ['U']).
card_text('alter reality', 'Change the text of target spell or permanent by replacing all instances of one color word with another. (This effect lasts indefinitely.)\nFlashback {1}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('alter reality', ['1', 'U']).
card_cmc('alter reality', 2).
card_layout('alter reality', 'normal').

% Found in: TMP, TPR
card_name('aluren', 'Aluren').
card_type('aluren', 'Enchantment').
card_types('aluren', ['Enchantment']).
card_subtypes('aluren', []).
card_colors('aluren', ['G']).
card_text('aluren', 'Any player may play creature cards with converted mana cost 3 or less without paying their mana cost and as though they had flash.').
card_mana_cost('aluren', ['2', 'G', 'G']).
card_cmc('aluren', 4).
card_layout('aluren', 'normal').
card_reserved('aluren').

% Found in: AVR
card_name('amass the components', 'Amass the Components').
card_type('amass the components', 'Sorcery').
card_types('amass the components', ['Sorcery']).
card_subtypes('amass the components', []).
card_colors('amass the components', ['U']).
card_text('amass the components', 'Draw three cards, then put a card from your hand on the bottom of your library.').
card_mana_cost('amass the components', ['3', 'U']).
card_cmc('amass the components', 4).
card_layout('amass the components', 'normal').

% Found in: 10E, TOR
card_name('ambassador laquatus', 'Ambassador Laquatus').
card_type('ambassador laquatus', 'Legendary Creature — Merfolk Wizard').
card_types('ambassador laquatus', ['Creature']).
card_subtypes('ambassador laquatus', ['Merfolk', 'Wizard']).
card_supertypes('ambassador laquatus', ['Legendary']).
card_colors('ambassador laquatus', ['U']).
card_text('ambassador laquatus', '{3}: Target player puts the top three cards of his or her library into his or her graveyard.').
card_mana_cost('ambassador laquatus', ['1', 'U', 'U']).
card_cmc('ambassador laquatus', 3).
card_layout('ambassador laquatus', 'normal').
card_power('ambassador laquatus', 1).
card_toughness('ambassador laquatus', 3).

% Found in: MOR
card_name('ambassador oak', 'Ambassador Oak').
card_type('ambassador oak', 'Creature — Treefolk Warrior').
card_types('ambassador oak', ['Creature']).
card_subtypes('ambassador oak', ['Treefolk', 'Warrior']).
card_colors('ambassador oak', ['G']).
card_text('ambassador oak', 'When Ambassador Oak enters the battlefield, put a 1/1 green Elf Warrior creature token onto the battlefield.').
card_mana_cost('ambassador oak', ['3', 'G']).
card_cmc('ambassador oak', 4).
card_layout('ambassador oak', 'normal').
card_power('ambassador oak', 3).
card_toughness('ambassador oak', 3).

% Found in: 6ED, MIR
card_name('amber prison', 'Amber Prison').
card_type('amber prison', 'Artifact').
card_types('amber prison', ['Artifact']).
card_subtypes('amber prison', []).
card_colors('amber prison', []).
card_text('amber prison', 'You may choose not to untap Amber Prison during your untap step.\n{4}, {T}: Tap target artifact, creature, or land. That permanent doesn\'t untap during its controller\'s untap step for as long as Amber Prison remains tapped.').
card_mana_cost('amber prison', ['4']).
card_cmc('amber prison', 4).
card_layout('amber prison', 'normal').

% Found in: UNH
card_name('ambiguity', 'Ambiguity').
card_type('ambiguity', 'Enchantment').
card_types('ambiguity', ['Enchantment']).
card_subtypes('ambiguity', []).
card_colors('ambiguity', ['U']).
card_text('ambiguity', 'Whenever a player plays a spell that counters a spell that has been played or a player plays a spell that comes into play with counters, that player may counter the next spell played or put an additional counter on a permanent that has already been played, but not countered.').
card_mana_cost('ambiguity', ['2', 'U', 'U']).
card_cmc('ambiguity', 4).
card_layout('ambiguity', 'normal').

% Found in: 8ED, PTK
card_name('ambition\'s cost', 'Ambition\'s Cost').
card_type('ambition\'s cost', 'Sorcery').
card_types('ambition\'s cost', ['Sorcery']).
card_subtypes('ambition\'s cost', []).
card_colors('ambition\'s cost', ['B']).
card_text('ambition\'s cost', 'You draw three cards and you lose 3 life.').
card_mana_cost('ambition\'s cost', ['3', 'B']).
card_cmc('ambition\'s cost', 4).
card_layout('ambition\'s cost', 'normal').

% Found in: DTK
card_name('ambuscade shaman', 'Ambuscade Shaman').
card_type('ambuscade shaman', 'Creature — Orc Shaman').
card_types('ambuscade shaman', ['Creature']).
card_subtypes('ambuscade shaman', ['Orc', 'Shaman']).
card_colors('ambuscade shaman', ['B']).
card_text('ambuscade shaman', 'Whenever Ambuscade Shaman or another creature enters the battlefield under your control, that creature gets +2/+2 until end of turn.\nDash {3}{B} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('ambuscade shaman', ['2', 'B']).
card_cmc('ambuscade shaman', 3).
card_layout('ambuscade shaman', 'normal').
card_power('ambuscade shaman', 2).
card_toughness('ambuscade shaman', 2).

% Found in: HML
card_name('ambush', 'Ambush').
card_type('ambush', 'Instant').
card_types('ambush', ['Instant']).
card_subtypes('ambush', []).
card_colors('ambush', ['R']).
card_text('ambush', 'Blocking creatures gain first strike until end of turn.').
card_mana_cost('ambush', ['3', 'R']).
card_cmc('ambush', 4).
card_layout('ambush', 'normal').

% Found in: DD3_EVG, EVG, SCG
card_name('ambush commander', 'Ambush Commander').
card_type('ambush commander', 'Creature — Elf').
card_types('ambush commander', ['Creature']).
card_subtypes('ambush commander', ['Elf']).
card_colors('ambush commander', ['G']).
card_text('ambush commander', 'Forests you control are 1/1 green Elf creatures that are still lands.\n{1}{G}, Sacrifice an Elf: Target creature gets +3/+3 until end of turn.').
card_mana_cost('ambush commander', ['3', 'G', 'G']).
card_cmc('ambush commander', 5).
card_layout('ambush commander', 'normal').
card_power('ambush commander', 2).
card_toughness('ambush commander', 2).

% Found in: FRF
card_name('ambush krotiq', 'Ambush Krotiq').
card_type('ambush krotiq', 'Creature — Insect').
card_types('ambush krotiq', ['Creature']).
card_subtypes('ambush krotiq', ['Insect']).
card_colors('ambush krotiq', ['G']).
card_text('ambush krotiq', 'Trample\nWhen Ambush Krotiq enters the battlefield, return another creature you control to its owner\'s hand.').
card_mana_cost('ambush krotiq', ['5', 'G']).
card_cmc('ambush krotiq', 6).
card_layout('ambush krotiq', 'normal').
card_power('ambush krotiq', 5).
card_toughness('ambush krotiq', 5).

% Found in: 5ED, HML, ME2
card_name('ambush party', 'Ambush Party').
card_type('ambush party', 'Creature — Human Rogue').
card_types('ambush party', ['Creature']).
card_subtypes('ambush party', ['Human', 'Rogue']).
card_colors('ambush party', ['R']).
card_text('ambush party', 'First strike, haste').
card_mana_cost('ambush party', ['4', 'R']).
card_cmc('ambush party', 5).
card_layout('ambush party', 'normal').
card_power('ambush party', 3).
card_toughness('ambush party', 1).

% Found in: ISD
card_name('ambush viper', 'Ambush Viper').
card_type('ambush viper', 'Creature — Snake').
card_types('ambush viper', ['Creature']).
card_subtypes('ambush viper', ['Snake']).
card_colors('ambush viper', ['G']).
card_text('ambush viper', 'Flash (You may cast this spell any time you could cast an instant.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('ambush viper', ['1', 'G']).
card_cmc('ambush viper', 2).
card_layout('ambush viper', 'normal').
card_power('ambush viper', 2).
card_toughness('ambush viper', 1).

% Found in: DRK, MED
card_name('amnesia', 'Amnesia').
card_type('amnesia', 'Sorcery').
card_types('amnesia', ['Sorcery']).
card_subtypes('amnesia', []).
card_colors('amnesia', ['U']).
card_text('amnesia', 'Target player reveals his or her hand and discards all nonland cards.').
card_mana_cost('amnesia', ['3', 'U', 'U', 'U']).
card_cmc('amnesia', 6).
card_layout('amnesia', 'normal').

% Found in: H09, LRW
card_name('amoeboid changeling', 'Amoeboid Changeling').
card_type('amoeboid changeling', 'Creature — Shapeshifter').
card_types('amoeboid changeling', ['Creature']).
card_subtypes('amoeboid changeling', ['Shapeshifter']).
card_colors('amoeboid changeling', ['U']).
card_text('amoeboid changeling', 'Changeling (This card is every creature type.)\n{T}: Target creature gains all creature types until end of turn.\n{T}: Target creature loses all creature types until end of turn.').
card_mana_cost('amoeboid changeling', ['1', 'U']).
card_cmc('amoeboid changeling', 2).
card_layout('amoeboid changeling', 'normal').
card_power('amoeboid changeling', 1).
card_toughness('amoeboid changeling', 1).

% Found in: STH
card_name('amok', 'Amok').
card_type('amok', 'Enchantment').
card_types('amok', ['Enchantment']).
card_subtypes('amok', []).
card_colors('amok', ['R']).
card_text('amok', '{1}, Discard a card at random: Put a +1/+1 counter on target creature.').
card_mana_cost('amok', ['1', 'R']).
card_cmc('amok', 2).
card_layout('amok', 'normal').

% Found in: PLS
card_name('amphibious kavu', 'Amphibious Kavu').
card_type('amphibious kavu', 'Creature — Kavu').
card_types('amphibious kavu', ['Creature']).
card_subtypes('amphibious kavu', ['Kavu']).
card_colors('amphibious kavu', ['G']).
card_text('amphibious kavu', 'Whenever Amphibious Kavu blocks or becomes blocked by one or more blue and/or black creatures, Amphibious Kavu gets +3/+3 until end of turn.').
card_mana_cost('amphibious kavu', ['2', 'G']).
card_cmc('amphibious kavu', 3).
card_layout('amphibious kavu', 'normal').
card_power('amphibious kavu', 2).
card_toughness('amphibious kavu', 2).

% Found in: M12
card_name('amphin cutthroat', 'Amphin Cutthroat').
card_type('amphin cutthroat', 'Creature — Salamander Rogue').
card_types('amphin cutthroat', ['Creature']).
card_subtypes('amphin cutthroat', ['Salamander', 'Rogue']).
card_colors('amphin cutthroat', ['U']).
card_text('amphin cutthroat', '').
card_mana_cost('amphin cutthroat', ['3', 'U']).
card_cmc('amphin cutthroat', 4).
card_layout('amphin cutthroat', 'normal').
card_power('amphin cutthroat', 2).
card_toughness('amphin cutthroat', 4).

% Found in: M15
card_name('amphin pathmage', 'Amphin Pathmage').
card_type('amphin pathmage', 'Creature — Salamander Wizard').
card_types('amphin pathmage', ['Creature']).
card_subtypes('amphin pathmage', ['Salamander', 'Wizard']).
card_colors('amphin pathmage', ['U']).
card_text('amphin pathmage', '{2}{U}: Target creature can\'t be blocked this turn.').
card_mana_cost('amphin pathmage', ['3', 'U']).
card_cmc('amphin pathmage', 4).
card_layout('amphin pathmage', 'normal').
card_power('amphin pathmage', 3).
card_toughness('amphin pathmage', 2).

% Found in: ORI
card_name('ampryn tactician', 'Ampryn Tactician').
card_type('ampryn tactician', 'Creature — Human Soldier').
card_types('ampryn tactician', ['Creature']).
card_subtypes('ampryn tactician', ['Human', 'Soldier']).
card_colors('ampryn tactician', ['W']).
card_text('ampryn tactician', 'When Ampryn Tactician enters the battlefield, creatures you control get +1/+1 until end of turn.').
card_mana_cost('ampryn tactician', ['2', 'W', 'W']).
card_cmc('ampryn tactician', 4).
card_layout('ampryn tactician', 'normal').
card_power('ampryn tactician', 3).
card_toughness('ampryn tactician', 3).

% Found in: 4ED, LEG, ME3
card_name('amrou kithkin', 'Amrou Kithkin').
card_type('amrou kithkin', 'Creature — Kithkin').
card_types('amrou kithkin', ['Creature']).
card_subtypes('amrou kithkin', ['Kithkin']).
card_colors('amrou kithkin', ['W']).
card_text('amrou kithkin', 'Amrou Kithkin can\'t be blocked by creatures with power 3 or greater.').
card_mana_cost('amrou kithkin', ['W', 'W']).
card_cmc('amrou kithkin', 2).
card_layout('amrou kithkin', 'normal').
card_power('amrou kithkin', 1).
card_toughness('amrou kithkin', 1).

% Found in: MMA, TSP
card_name('amrou scout', 'Amrou Scout').
card_type('amrou scout', 'Creature — Kithkin Rebel Scout').
card_types('amrou scout', ['Creature']).
card_subtypes('amrou scout', ['Kithkin', 'Rebel', 'Scout']).
card_colors('amrou scout', ['W']).
card_text('amrou scout', '{4}, {T}: Search your library for a Rebel permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('amrou scout', ['1', 'W']).
card_cmc('amrou scout', 2).
card_layout('amrou scout', 'normal').
card_power('amrou scout', 2).
card_toughness('amrou scout', 1).

% Found in: MMA, TSP
card_name('amrou seekers', 'Amrou Seekers').
card_type('amrou seekers', 'Creature — Kithkin Rebel').
card_types('amrou seekers', ['Creature']).
card_subtypes('amrou seekers', ['Kithkin', 'Rebel']).
card_colors('amrou seekers', ['W']).
card_text('amrou seekers', 'Amrou Seekers can\'t be blocked except by artifact creatures and/or white creatures.').
card_mana_cost('amrou seekers', ['2', 'W']).
card_cmc('amrou seekers', 3).
card_layout('amrou seekers', 'normal').
card_power('amrou seekers', 2).
card_toughness('amrou seekers', 2).

% Found in: ODY
card_name('amugaba', 'Amugaba').
card_type('amugaba', 'Creature — Illusion').
card_types('amugaba', ['Creature']).
card_subtypes('amugaba', ['Illusion']).
card_colors('amugaba', ['U']).
card_text('amugaba', 'Flying\n{2}{U}, Discard a card: Return Amugaba to its owner\'s hand.').
card_mana_cost('amugaba', ['5', 'U', 'U']).
card_cmc('amugaba', 7).
card_layout('amugaba', 'normal').
card_power('amugaba', 6).
card_toughness('amugaba', 6).

% Found in: 4ED, 5ED, ATQ, ME4
card_name('amulet of kroog', 'Amulet of Kroog').
card_type('amulet of kroog', 'Artifact').
card_types('amulet of kroog', ['Artifact']).
card_subtypes('amulet of kroog', []).
card_colors('amulet of kroog', []).
card_text('amulet of kroog', '{2}, {T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('amulet of kroog', ['2']).
card_cmc('amulet of kroog', 2).
card_layout('amulet of kroog', 'normal').

% Found in: ICE
card_name('amulet of quoz', 'Amulet of Quoz').
card_type('amulet of quoz', 'Artifact').
card_types('amulet of quoz', ['Artifact']).
card_subtypes('amulet of quoz', []).
card_colors('amulet of quoz', []).
card_text('amulet of quoz', 'Remove Amulet of Quoz from your deck before playing if you\'re not playing for ante.\n{T}, Sacrifice Amulet of Quoz: Target opponent may ante the top card of his or her library. If he or she doesn\'t, you flip a coin. If you win the flip, that player loses the game. If you lose the flip, you lose the game. Activate this ability only during your upkeep.').
card_mana_cost('amulet of quoz', ['6']).
card_cmc('amulet of quoz', 6).
card_layout('amulet of quoz', 'normal').
card_reserved('amulet of quoz').

% Found in: MIR
card_name('amulet of unmaking', 'Amulet of Unmaking').
card_type('amulet of unmaking', 'Artifact').
card_types('amulet of unmaking', ['Artifact']).
card_subtypes('amulet of unmaking', []).
card_colors('amulet of unmaking', []).
card_text('amulet of unmaking', '{5}, {T}, Exile Amulet of Unmaking: Exile target artifact, creature, or land. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('amulet of unmaking', ['5']).
card_cmc('amulet of unmaking', 5).
card_layout('amulet of unmaking', 'normal').
card_reserved('amulet of unmaking').

% Found in: WWK
card_name('amulet of vigor', 'Amulet of Vigor').
card_type('amulet of vigor', 'Artifact').
card_types('amulet of vigor', ['Artifact']).
card_subtypes('amulet of vigor', []).
card_colors('amulet of vigor', []).
card_text('amulet of vigor', 'Whenever a permanent enters the battlefield tapped and under your control, untap it.').
card_mana_cost('amulet of vigor', ['1']).
card_cmc('amulet of vigor', 1).
card_layout('amulet of vigor', 'normal').

% Found in: 5ED, HML
card_name('an-havva constable', 'An-Havva Constable').
card_type('an-havva constable', 'Creature — Human').
card_types('an-havva constable', ['Creature']).
card_subtypes('an-havva constable', ['Human']).
card_colors('an-havva constable', ['G']).
card_text('an-havva constable', 'An-Havva Constable\'s toughness is equal to 1 plus the number of green creatures on the battlefield.').
card_mana_cost('an-havva constable', ['1', 'G', 'G']).
card_cmc('an-havva constable', 3).
card_layout('an-havva constable', 'normal').
card_power('an-havva constable', 2).
card_toughness('an-havva constable', '1+*').

% Found in: HML
card_name('an-havva inn', 'An-Havva Inn').
card_type('an-havva inn', 'Sorcery').
card_types('an-havva inn', ['Sorcery']).
card_subtypes('an-havva inn', []).
card_colors('an-havva inn', ['G']).
card_text('an-havva inn', 'You gain X plus 1 life, where X is the number of green creatures on the battlefield.').
card_mana_cost('an-havva inn', ['1', 'G', 'G']).
card_cmc('an-havva inn', 3).
card_layout('an-havva inn', 'normal').

% Found in: HML
card_name('an-havva township', 'An-Havva Township').
card_type('an-havva township', 'Land').
card_types('an-havva township', ['Land']).
card_subtypes('an-havva township', []).
card_colors('an-havva township', []).
card_text('an-havva township', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add {G} to your mana pool.\n{2}, {T}: Add {R} or {W} to your mana pool.').
card_layout('an-havva township', 'normal').

% Found in: HML, ME2
card_name('an-zerrin ruins', 'An-Zerrin Ruins').
card_type('an-zerrin ruins', 'Enchantment').
card_types('an-zerrin ruins', ['Enchantment']).
card_subtypes('an-zerrin ruins', []).
card_colors('an-zerrin ruins', ['R']).
card_text('an-zerrin ruins', 'As An-Zerrin Ruins enters the battlefield, choose a creature type.\nCreatures of the chosen type don\'t untap during their controllers\' untap steps.').
card_mana_cost('an-zerrin ruins', ['2', 'R', 'R']).
card_cmc('an-zerrin ruins', 4).
card_layout('an-zerrin ruins', 'normal').
card_reserved('an-zerrin ruins').

% Found in: PLC
card_name('ana battlemage', 'Ana Battlemage').
card_type('ana battlemage', 'Creature — Human Wizard').
card_types('ana battlemage', ['Creature']).
card_subtypes('ana battlemage', ['Human', 'Wizard']).
card_colors('ana battlemage', ['G']).
card_text('ana battlemage', 'Kicker {2}{U} and/or {1}{B} (You may pay an additional {2}{U} and/or {1}{B} as you cast this spell.)\nWhen Ana Battlemage enters the battlefield, if it was kicked with its {2}{U} kicker, target player discards three cards.\nWhen Ana Battlemage enters the battlefield, if it was kicked with its {1}{B} kicker, tap target untapped creature and that creature deals damage equal to its power to its controller.').
card_mana_cost('ana battlemage', ['2', 'G']).
card_cmc('ana battlemage', 3).
card_layout('ana battlemage', 'normal').
card_power('ana battlemage', 2).
card_toughness('ana battlemage', 2).

% Found in: APC
card_name('ana disciple', 'Ana Disciple').
card_type('ana disciple', 'Creature — Human Wizard').
card_types('ana disciple', ['Creature']).
card_subtypes('ana disciple', ['Human', 'Wizard']).
card_colors('ana disciple', ['G']).
card_text('ana disciple', '{U}, {T}: Target creature gains flying until end of turn.\n{B}, {T}: Target creature gets -2/-0 until end of turn.').
card_mana_cost('ana disciple', ['G']).
card_cmc('ana disciple', 1).
card_layout('ana disciple', 'normal').
card_power('ana disciple', 1).
card_toughness('ana disciple', 1).

% Found in: APC
card_name('ana sanctuary', 'Ana Sanctuary').
card_type('ana sanctuary', 'Enchantment').
card_types('ana sanctuary', ['Enchantment']).
card_subtypes('ana sanctuary', []).
card_colors('ana sanctuary', ['G']).
card_text('ana sanctuary', 'At the beginning of your upkeep, if you control a blue or black permanent, target creature gets +1/+1 until end of turn. If you control a blue permanent and a black permanent, that creature gets +5/+5 until end of turn instead.').
card_mana_cost('ana sanctuary', ['2', 'G']).
card_cmc('ana sanctuary', 3).
card_layout('ana sanctuary', 'normal').

% Found in: HML, ME3
card_name('anaba ancestor', 'Anaba Ancestor').
card_type('anaba ancestor', 'Creature — Minotaur Spirit').
card_types('anaba ancestor', ['Creature']).
card_subtypes('anaba ancestor', ['Minotaur', 'Spirit']).
card_colors('anaba ancestor', ['R']).
card_text('anaba ancestor', '{T}: Another target Minotaur creature gets +1/+1 until end of turn.').
card_mana_cost('anaba ancestor', ['1', 'R']).
card_cmc('anaba ancestor', 2).
card_layout('anaba ancestor', 'normal').
card_power('anaba ancestor', 1).
card_toughness('anaba ancestor', 1).
card_reserved('anaba ancestor').

% Found in: 10E, 6ED, HML
card_name('anaba bodyguard', 'Anaba Bodyguard').
card_type('anaba bodyguard', 'Creature — Minotaur').
card_types('anaba bodyguard', ['Creature']).
card_subtypes('anaba bodyguard', ['Minotaur']).
card_colors('anaba bodyguard', ['R']).
card_text('anaba bodyguard', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('anaba bodyguard', ['3', 'R']).
card_cmc('anaba bodyguard', 4).
card_layout('anaba bodyguard', 'normal').
card_power('anaba bodyguard', 2).
card_toughness('anaba bodyguard', 3).

% Found in: 6ED, 8ED, 9ED, HML
card_name('anaba shaman', 'Anaba Shaman').
card_type('anaba shaman', 'Creature — Minotaur Shaman').
card_types('anaba shaman', ['Creature']).
card_subtypes('anaba shaman', ['Minotaur', 'Shaman']).
card_colors('anaba shaman', ['R']).
card_text('anaba shaman', '{R}, {T}: Anaba Shaman deals 1 damage to target creature or player.').
card_mana_cost('anaba shaman', ['3', 'R']).
card_cmc('anaba shaman', 4).
card_layout('anaba shaman', 'normal').
card_power('anaba shaman', 2).
card_toughness('anaba shaman', 2).

% Found in: HML, ME3
card_name('anaba spirit crafter', 'Anaba Spirit Crafter').
card_type('anaba spirit crafter', 'Creature — Minotaur Shaman').
card_types('anaba spirit crafter', ['Creature']).
card_subtypes('anaba spirit crafter', ['Minotaur', 'Shaman']).
card_colors('anaba spirit crafter', ['R']).
card_text('anaba spirit crafter', 'Minotaur creatures get +1/+0.').
card_mana_cost('anaba spirit crafter', ['2', 'R', 'R']).
card_cmc('anaba spirit crafter', 4).
card_layout('anaba spirit crafter', 'normal').
card_power('anaba spirit crafter', 1).
card_toughness('anaba spirit crafter', 3).
card_reserved('anaba spirit crafter').

% Found in: 7ED, 9ED, POR, USG
card_name('anaconda', 'Anaconda').
card_type('anaconda', 'Creature — Snake').
card_types('anaconda', ['Creature']).
card_subtypes('anaconda', ['Snake']).
card_colors('anaconda', ['G']).
card_text('anaconda', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('anaconda', ['3', 'G']).
card_cmc('anaconda', 4).
card_layout('anaconda', 'normal').
card_power('anaconda', 3).
card_toughness('anaconda', 3).

% Found in: DTK
card_name('anafenza, kin-tree spirit', 'Anafenza, Kin-Tree Spirit').
card_type('anafenza, kin-tree spirit', 'Legendary Creature — Spirit Soldier').
card_types('anafenza, kin-tree spirit', ['Creature']).
card_subtypes('anafenza, kin-tree spirit', ['Spirit', 'Soldier']).
card_supertypes('anafenza, kin-tree spirit', ['Legendary']).
card_colors('anafenza, kin-tree spirit', ['W']).
card_text('anafenza, kin-tree spirit', 'Whenever another nontoken creature enters the battlefield under your control, bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('anafenza, kin-tree spirit', ['W', 'W']).
card_cmc('anafenza, kin-tree spirit', 2).
card_layout('anafenza, kin-tree spirit', 'normal').
card_power('anafenza, kin-tree spirit', 2).
card_toughness('anafenza, kin-tree spirit', 2).

% Found in: KTK, pPRE
card_name('anafenza, the foremost', 'Anafenza, the Foremost').
card_type('anafenza, the foremost', 'Legendary Creature — Human Soldier').
card_types('anafenza, the foremost', ['Creature']).
card_subtypes('anafenza, the foremost', ['Human', 'Soldier']).
card_supertypes('anafenza, the foremost', ['Legendary']).
card_colors('anafenza, the foremost', ['W', 'B', 'G']).
card_text('anafenza, the foremost', 'Whenever Anafenza, the Foremost attacks, put a +1/+1 counter on another target tapped creature you control.\nIf a creature card would be put into an opponent\'s graveyard from anywhere, exile it instead.').
card_mana_cost('anafenza, the foremost', ['W', 'B', 'G']).
card_cmc('anafenza, the foremost', 3).
card_layout('anafenza, the foremost', 'normal').
card_power('anafenza, the foremost', 4).
card_toughness('anafenza, the foremost', 4).

% Found in: 9ED, EXO, ODY, TPR
card_name('anarchist', 'Anarchist').
card_type('anarchist', 'Creature — Human Wizard').
card_types('anarchist', ['Creature']).
card_subtypes('anarchist', ['Human', 'Wizard']).
card_colors('anarchist', ['R']).
card_text('anarchist', 'When Anarchist enters the battlefield, you may return target sorcery card from your graveyard to your hand.').
card_mana_cost('anarchist', ['4', 'R']).
card_cmc('anarchist', 5).
card_layout('anarchist', 'normal').
card_power('anarchist', 2).
card_toughness('anarchist', 2).

% Found in: ICE, ME2
card_name('anarchy', 'Anarchy').
card_type('anarchy', 'Sorcery').
card_types('anarchy', ['Sorcery']).
card_subtypes('anarchy', []).
card_colors('anarchy', ['R']).
card_text('anarchy', 'Destroy all white permanents.').
card_mana_cost('anarchy', ['2', 'R', 'R']).
card_cmc('anarchy', 4).
card_layout('anarchy', 'normal').

% Found in: ARB, pFNM
card_name('anathemancer', 'Anathemancer').
card_type('anathemancer', 'Creature — Zombie Wizard').
card_types('anathemancer', ['Creature']).
card_subtypes('anathemancer', ['Zombie', 'Wizard']).
card_colors('anathemancer', ['B', 'R']).
card_text('anathemancer', 'When Anathemancer enters the battlefield, it deals damage to target player equal to the number of nonbasic lands that player controls.\nUnearth {5}{B}{R} ({5}{B}{R}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('anathemancer', ['1', 'B', 'R']).
card_cmc('anathemancer', 3).
card_layout('anathemancer', 'normal').
card_power('anathemancer', 2).
card_toughness('anathemancer', 2).

% Found in: APC
card_name('anavolver', 'Anavolver').
card_type('anavolver', 'Creature — Volver').
card_types('anavolver', ['Creature']).
card_subtypes('anavolver', ['Volver']).
card_colors('anavolver', ['G']).
card_text('anavolver', 'Kicker {1}{U} and/or {B} (You may pay an additional {1}{U} and/or {B} as you cast this spell.)\nIf Anavolver was kicked with its {1}{U} kicker, it enters the battlefield with two +1/+1 counters on it and with flying.\nIf Anavolver was kicked with its {B} kicker, it enters the battlefield with a +1/+1 counter on it and with \"Pay 3 life: Regenerate Anavolver.\"').
card_mana_cost('anavolver', ['3', 'G']).
card_cmc('anavolver', 4).
card_layout('anavolver', 'normal').
card_power('anavolver', 3).
card_toughness('anavolver', 3).

% Found in: DDL, THS
card_name('anax and cymede', 'Anax and Cymede').
card_type('anax and cymede', 'Legendary Creature — Human Soldier').
card_types('anax and cymede', ['Creature']).
card_subtypes('anax and cymede', ['Human', 'Soldier']).
card_supertypes('anax and cymede', ['Legendary']).
card_colors('anax and cymede', ['W', 'R']).
card_text('anax and cymede', 'First strike, vigilance\nHeroic — Whenever you cast a spell that targets Anax and Cymede, creatures you control get +1/+1 and gain trample until end of turn.').
card_mana_cost('anax and cymede', ['1', 'R', 'W']).
card_cmc('anax and cymede', 3).
card_layout('anax and cymede', 'normal').
card_power('anax and cymede', 3).
card_toughness('anax and cymede', 2).

% Found in: 10E, JUD
card_name('ancestor\'s chosen', 'Ancestor\'s Chosen').
card_type('ancestor\'s chosen', 'Creature — Human Cleric').
card_types('ancestor\'s chosen', ['Creature']).
card_subtypes('ancestor\'s chosen', ['Human', 'Cleric']).
card_colors('ancestor\'s chosen', ['W']).
card_text('ancestor\'s chosen', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhen Ancestor\'s Chosen enters the battlefield, you gain 1 life for each card in your graveyard.').
card_mana_cost('ancestor\'s chosen', ['5', 'W', 'W']).
card_cmc('ancestor\'s chosen', 7).
card_layout('ancestor\'s chosen', 'normal').
card_power('ancestor\'s chosen', 4).
card_toughness('ancestor\'s chosen', 4).

% Found in: ONS
card_name('ancestor\'s prophet', 'Ancestor\'s Prophet').
card_type('ancestor\'s prophet', 'Creature — Human Cleric').
card_types('ancestor\'s prophet', ['Creature']).
card_subtypes('ancestor\'s prophet', ['Human', 'Cleric']).
card_colors('ancestor\'s prophet', ['W']).
card_text('ancestor\'s prophet', 'Tap five untapped Clerics you control: You gain 10 life.').
card_mana_cost('ancestor\'s prophet', ['4', 'W']).
card_cmc('ancestor\'s prophet', 5).
card_layout('ancestor\'s prophet', 'normal').
card_power('ancestor\'s prophet', 1).
card_toughness('ancestor\'s prophet', 5).

% Found in: WTH
card_name('ancestral knowledge', 'Ancestral Knowledge').
card_type('ancestral knowledge', 'Enchantment').
card_types('ancestral knowledge', ['Enchantment']).
card_subtypes('ancestral knowledge', []).
card_colors('ancestral knowledge', ['U']).
card_text('ancestral knowledge', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Ancestral Knowledge enters the battlefield, look at the top ten cards of your library, then exile any number of them and put the rest back on top of your library in any order.\nWhen Ancestral Knowledge leaves the battlefield, shuffle your library.').
card_mana_cost('ancestral knowledge', ['1', 'U']).
card_cmc('ancestral knowledge', 2).
card_layout('ancestral knowledge', 'normal').
card_reserved('ancestral knowledge').

% Found in: MMQ
card_name('ancestral mask', 'Ancestral Mask').
card_type('ancestral mask', 'Enchantment — Aura').
card_types('ancestral mask', ['Enchantment']).
card_subtypes('ancestral mask', ['Aura']).
card_colors('ancestral mask', ['G']).
card_text('ancestral mask', 'Enchant creature\nEnchanted creature gets +2/+2 for each other enchantment on the battlefield.').
card_mana_cost('ancestral mask', ['2', 'G']).
card_cmc('ancestral mask', 3).
card_layout('ancestral mask', 'normal').

% Found in: 6ED, 7ED, MIR, POR
card_name('ancestral memories', 'Ancestral Memories').
card_type('ancestral memories', 'Sorcery').
card_types('ancestral memories', ['Sorcery']).
card_subtypes('ancestral memories', []).
card_colors('ancestral memories', ['U']).
card_text('ancestral memories', 'Look at the top seven cards of your library. Put two of them into your hand and the rest into your graveyard.').
card_mana_cost('ancestral memories', ['2', 'U', 'U', 'U']).
card_cmc('ancestral memories', 5).
card_layout('ancestral memories', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('ancestral recall', 'Ancestral Recall').
card_type('ancestral recall', 'Instant').
card_types('ancestral recall', ['Instant']).
card_subtypes('ancestral recall', []).
card_colors('ancestral recall', ['U']).
card_text('ancestral recall', 'Target player draws three cards.').
card_mana_cost('ancestral recall', ['U']).
card_cmc('ancestral recall', 1).
card_layout('ancestral recall', 'normal').
card_reserved('ancestral recall').

% Found in: DTK
card_name('ancestral statue', 'Ancestral Statue').
card_type('ancestral statue', 'Artifact Creature — Golem').
card_types('ancestral statue', ['Artifact', 'Creature']).
card_subtypes('ancestral statue', ['Golem']).
card_colors('ancestral statue', []).
card_text('ancestral statue', 'When Ancestral Statue enters the battlefield, return a nonland permanent you control to its owner\'s hand.').
card_mana_cost('ancestral statue', ['4']).
card_cmc('ancestral statue', 4).
card_layout('ancestral statue', 'normal').
card_power('ancestral statue', 3).
card_toughness('ancestral statue', 4).

% Found in: ODY
card_name('ancestral tribute', 'Ancestral Tribute').
card_type('ancestral tribute', 'Sorcery').
card_types('ancestral tribute', ['Sorcery']).
card_subtypes('ancestral tribute', []).
card_colors('ancestral tribute', ['W']).
card_text('ancestral tribute', 'You gain 2 life for each card in your graveyard.\nFlashback {9}{W}{W}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('ancestral tribute', ['5', 'W', 'W']).
card_cmc('ancestral tribute', 7).
card_layout('ancestral tribute', 'normal').

% Found in: FRF
card_name('ancestral vengeance', 'Ancestral Vengeance').
card_type('ancestral vengeance', 'Enchantment — Aura').
card_types('ancestral vengeance', ['Enchantment']).
card_subtypes('ancestral vengeance', ['Aura']).
card_colors('ancestral vengeance', ['B']).
card_text('ancestral vengeance', 'Enchant creature\nWhen Ancestral Vengeance enters the battlefield, put a +1/+1 counter on target creature you control.\nEnchanted creature gets -1/-1.').
card_mana_cost('ancestral vengeance', ['B', 'B']).
card_cmc('ancestral vengeance', 2).
card_layout('ancestral vengeance', 'normal').

% Found in: DD2, DD3_JVC, TSP
card_name('ancestral vision', 'Ancestral Vision').
card_type('ancestral vision', 'Sorcery').
card_types('ancestral vision', ['Sorcery']).
card_subtypes('ancestral vision', []).
card_colors('ancestral vision', ['U']).
card_text('ancestral vision', 'Suspend 4—{U} (Rather than cast this card from your hand, pay {U} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\nTarget player draws three cards.').
card_layout('ancestral vision', 'normal').

% Found in: ORI
card_name('anchor to the æther', 'Anchor to the Æther').
card_type('anchor to the æther', 'Sorcery').
card_types('anchor to the æther', ['Sorcery']).
card_subtypes('anchor to the æther', []).
card_colors('anchor to the æther', ['U']).
card_text('anchor to the æther', 'Put target creature on top of its owner\'s library. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('anchor to the æther', ['2', 'U']).
card_cmc('anchor to the æther', 3).
card_layout('anchor to the æther', 'normal').

% Found in: LRW
card_name('ancient amphitheater', 'Ancient Amphitheater').
card_type('ancient amphitheater', 'Land').
card_types('ancient amphitheater', ['Land']).
card_subtypes('ancient amphitheater', []).
card_colors('ancient amphitheater', []).
card_text('ancient amphitheater', 'As Ancient Amphitheater enters the battlefield, you may reveal a Giant card from your hand. If you don\'t, Ancient Amphitheater enters the battlefield tapped.\n{T}: Add {R} or {W} to your mana pool.').
card_layout('ancient amphitheater', 'normal').

% Found in: DTK
card_name('ancient carp', 'Ancient Carp').
card_type('ancient carp', 'Creature — Fish').
card_types('ancient carp', ['Creature']).
card_subtypes('ancient carp', ['Fish']).
card_colors('ancient carp', ['U']).
card_text('ancient carp', '').
card_mana_cost('ancient carp', ['4', 'U']).
card_cmc('ancient carp', 5).
card_layout('ancient carp', 'normal').
card_power('ancient carp', 2).
card_toughness('ancient carp', 5).

% Found in: DDK, PO2, S99
card_name('ancient craving', 'Ancient Craving').
card_type('ancient craving', 'Sorcery').
card_types('ancient craving', ['Sorcery']).
card_subtypes('ancient craving', []).
card_colors('ancient craving', ['B']).
card_text('ancient craving', 'You draw three cards and you lose 3 life.').
card_mana_cost('ancient craving', ['3', 'B']).
card_cmc('ancient craving', 4).
card_layout('ancient craving', 'normal').

% Found in: HOP, MRD
card_name('ancient den', 'Ancient Den').
card_type('ancient den', 'Artifact Land').
card_types('ancient den', ['Artifact', 'Land']).
card_subtypes('ancient den', []).
card_colors('ancient den', []).
card_text('ancient den', '(Ancient Den isn\'t a spell.)\n{T}: Add {W} to your mana pool.').
card_layout('ancient den', 'normal').

% Found in: ISD, TSP, pFNM
card_name('ancient grudge', 'Ancient Grudge').
card_type('ancient grudge', 'Instant').
card_types('ancient grudge', ['Instant']).
card_subtypes('ancient grudge', []).
card_colors('ancient grudge', ['R']).
card_text('ancient grudge', 'Destroy target artifact.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('ancient grudge', ['1', 'R']).
card_cmc('ancient grudge', 2).
card_layout('ancient grudge', 'normal').

% Found in: M11, pLPA
card_name('ancient hellkite', 'Ancient Hellkite').
card_type('ancient hellkite', 'Creature — Dragon').
card_types('ancient hellkite', ['Creature']).
card_subtypes('ancient hellkite', ['Dragon']).
card_colors('ancient hellkite', ['R']).
card_text('ancient hellkite', 'Flying\n{R}: Ancient Hellkite deals 1 damage to target creature defending player controls. Activate this ability only if Ancient Hellkite is attacking.').
card_mana_cost('ancient hellkite', ['4', 'R', 'R', 'R']).
card_cmc('ancient hellkite', 7).
card_layout('ancient hellkite', 'normal').
card_power('ancient hellkite', 6).
card_toughness('ancient hellkite', 6).

% Found in: NMS
card_name('ancient hydra', 'Ancient Hydra').
card_type('ancient hydra', 'Creature — Hydra').
card_types('ancient hydra', ['Creature']).
card_subtypes('ancient hydra', ['Hydra']).
card_colors('ancient hydra', ['R']).
card_text('ancient hydra', 'Fading 5 (This creature enters the battlefield with five fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\n{1}, Remove a fade counter from Ancient Hydra: Ancient Hydra deals 1 damage to target creature or player.').
card_mana_cost('ancient hydra', ['4', 'R']).
card_cmc('ancient hydra', 5).
card_layout('ancient hydra', 'normal').
card_power('ancient hydra', 5).
card_toughness('ancient hydra', 1).

% Found in: INV
card_name('ancient kavu', 'Ancient Kavu').
card_type('ancient kavu', 'Creature — Kavu').
card_types('ancient kavu', ['Creature']).
card_subtypes('ancient kavu', ['Kavu']).
card_colors('ancient kavu', ['R']).
card_text('ancient kavu', '{2}: Ancient Kavu becomes colorless until end of turn.').
card_mana_cost('ancient kavu', ['3', 'R']).
card_cmc('ancient kavu', 4).
card_layout('ancient kavu', 'normal').
card_power('ancient kavu', 3).
card_toughness('ancient kavu', 3).

% Found in: SCG
card_name('ancient ooze', 'Ancient Ooze').
card_type('ancient ooze', 'Creature — Ooze').
card_types('ancient ooze', ['Creature']).
card_subtypes('ancient ooze', ['Ooze']).
card_colors('ancient ooze', ['G']).
card_text('ancient ooze', 'Ancient Ooze\'s power and toughness are each equal to the total converted mana cost of other creatures you control.').
card_mana_cost('ancient ooze', ['5', 'G', 'G']).
card_cmc('ancient ooze', 7).
card_layout('ancient ooze', 'normal').
card_power('ancient ooze', '*').
card_toughness('ancient ooze', '*').

% Found in: TMP
card_name('ancient runes', 'Ancient Runes').
card_type('ancient runes', 'Enchantment').
card_types('ancient runes', ['Enchantment']).
card_subtypes('ancient runes', []).
card_colors('ancient runes', ['R']).
card_text('ancient runes', 'At the beginning of each player\'s upkeep, Ancient Runes deals damage to that player equal to the number of artifacts he or she controls.').
card_mana_cost('ancient runes', ['2', 'R']).
card_cmc('ancient runes', 3).
card_layout('ancient runes', 'normal').

% Found in: 7ED, 9ED, M15, UDS
card_name('ancient silverback', 'Ancient Silverback').
card_type('ancient silverback', 'Creature — Ape').
card_types('ancient silverback', ['Creature']).
card_subtypes('ancient silverback', ['Ape']).
card_colors('ancient silverback', ['G']).
card_text('ancient silverback', '{G}: Regenerate Ancient Silverback. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('ancient silverback', ['4', 'G', 'G']).
card_cmc('ancient silverback', 6).
card_layout('ancient silverback', 'normal').
card_power('ancient silverback', 6).
card_toughness('ancient silverback', 5).

% Found in: PLS
card_name('ancient spider', 'Ancient Spider').
card_type('ancient spider', 'Creature — Spider').
card_types('ancient spider', ['Creature']).
card_subtypes('ancient spider', ['Spider']).
card_colors('ancient spider', ['W', 'G']).
card_text('ancient spider', 'First strike; reach (This creature can block creatures with flying.)').
card_mana_cost('ancient spider', ['2', 'G', 'W']).
card_cmc('ancient spider', 4).
card_layout('ancient spider', 'normal').
card_power('ancient spider', 2).
card_toughness('ancient spider', 5).

% Found in: INV
card_name('ancient spring', 'Ancient Spring').
card_type('ancient spring', 'Land').
card_types('ancient spring', ['Land']).
card_subtypes('ancient spring', []).
card_colors('ancient spring', []).
card_text('ancient spring', 'Ancient Spring enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\n{T}, Sacrifice Ancient Spring: Add {W}{B} to your mana pool.').
card_layout('ancient spring', 'normal').

% Found in: ROE
card_name('ancient stirrings', 'Ancient Stirrings').
card_type('ancient stirrings', 'Sorcery').
card_types('ancient stirrings', ['Sorcery']).
card_subtypes('ancient stirrings', []).
card_colors('ancient stirrings', ['G']).
card_text('ancient stirrings', 'Look at the top five cards of your library. You may reveal a colorless card from among them and put it into your hand. Then put the rest on the bottom of your library in any order. (Cards with no colored mana in their mana costs are colorless. Lands are also colorless.)').
card_mana_cost('ancient stirrings', ['G']).
card_cmc('ancient stirrings', 1).
card_layout('ancient stirrings', 'normal').

% Found in: TMP, V12, VMA
card_name('ancient tomb', 'Ancient Tomb').
card_type('ancient tomb', 'Land').
card_types('ancient tomb', ['Land']).
card_subtypes('ancient tomb', []).
card_colors('ancient tomb', []).
card_text('ancient tomb', '{T}: Add {2} to your mana pool. Ancient Tomb deals 2 damage to you.').
card_layout('ancient tomb', 'normal').

% Found in: CON, H09, pFNM
card_name('ancient ziggurat', 'Ancient Ziggurat').
card_type('ancient ziggurat', 'Land').
card_types('ancient ziggurat', ['Land']).
card_subtypes('ancient ziggurat', []).
card_colors('ancient ziggurat', []).
card_text('ancient ziggurat', '{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a creature spell.').
card_layout('ancient ziggurat', 'normal').

% Found in: INV
card_name('andradite leech', 'Andradite Leech').
card_type('andradite leech', 'Creature — Leech').
card_types('andradite leech', ['Creature']).
card_subtypes('andradite leech', ['Leech']).
card_colors('andradite leech', ['B']).
card_text('andradite leech', 'Black spells you cast cost {B} more to cast.\n{B}: Andradite Leech gets +1/+1 until end of turn.').
card_mana_cost('andradite leech', ['2', 'B']).
card_cmc('andradite leech', 3).
card_layout('andradite leech', 'normal').
card_power('andradite leech', 2).
card_toughness('andradite leech', 2).

% Found in: CMD, GPT
card_name('angel of despair', 'Angel of Despair').
card_type('angel of despair', 'Creature — Angel').
card_types('angel of despair', ['Creature']).
card_subtypes('angel of despair', ['Angel']).
card_colors('angel of despair', ['W', 'B']).
card_text('angel of despair', 'Flying\nWhen Angel of Despair enters the battlefield, destroy target permanent.').
card_mana_cost('angel of despair', ['3', 'W', 'W', 'B', 'B']).
card_cmc('angel of despair', 7).
card_layout('angel of despair', 'normal').
card_power('angel of despair', 5).
card_toughness('angel of despair', 5).

% Found in: C13
card_name('angel of finality', 'Angel of Finality').
card_type('angel of finality', 'Creature — Angel').
card_types('angel of finality', ['Creature']).
card_subtypes('angel of finality', ['Angel']).
card_colors('angel of finality', ['W']).
card_text('angel of finality', 'Flying\nWhen Angel of Finality enters the battlefield, exile all cards from target player\'s graveyard.').
card_mana_cost('angel of finality', ['3', 'W']).
card_cmc('angel of finality', 4).
card_layout('angel of finality', 'normal').
card_power('angel of finality', 3).
card_toughness('angel of finality', 4).

% Found in: ISD
card_name('angel of flight alabaster', 'Angel of Flight Alabaster').
card_type('angel of flight alabaster', 'Creature — Angel').
card_types('angel of flight alabaster', ['Creature']).
card_subtypes('angel of flight alabaster', ['Angel']).
card_colors('angel of flight alabaster', ['W']).
card_text('angel of flight alabaster', 'Flying\nAt the beginning of your upkeep, return target Spirit card from your graveyard to your hand.').
card_mana_cost('angel of flight alabaster', ['4', 'W']).
card_cmc('angel of flight alabaster', 5).
card_layout('angel of flight alabaster', 'normal').
card_power('angel of flight alabaster', 4).
card_toughness('angel of flight alabaster', 4).

% Found in: ME2, PO2
card_name('angel of fury', 'Angel of Fury').
card_type('angel of fury', 'Creature — Angel').
card_types('angel of fury', ['Creature']).
card_subtypes('angel of fury', ['Angel']).
card_colors('angel of fury', ['W']).
card_text('angel of fury', 'Flying\nWhen Angel of Fury dies, you may shuffle it into its owner\'s library.').
card_mana_cost('angel of fury', ['4', 'W', 'W']).
card_cmc('angel of fury', 6).
card_layout('angel of fury', 'normal').
card_power('angel of fury', 3).
card_toughness('angel of fury', 5).

% Found in: AVR, pMEI
card_name('angel of glory\'s rise', 'Angel of Glory\'s Rise').
card_type('angel of glory\'s rise', 'Creature — Angel').
card_types('angel of glory\'s rise', ['Creature']).
card_subtypes('angel of glory\'s rise', ['Angel']).
card_colors('angel of glory\'s rise', ['W']).
card_text('angel of glory\'s rise', 'Flying\nWhen Angel of Glory\'s Rise enters the battlefield, exile all Zombies, then return all Human creature cards from your graveyard to the battlefield.').
card_mana_cost('angel of glory\'s rise', ['5', 'W', 'W']).
card_cmc('angel of glory\'s rise', 7).
card_layout('angel of glory\'s rise', 'normal').
card_power('angel of glory\'s rise', 4).
card_toughness('angel of glory\'s rise', 6).

% Found in: AVR
card_name('angel of jubilation', 'Angel of Jubilation').
card_type('angel of jubilation', 'Creature — Angel').
card_types('angel of jubilation', ['Creature']).
card_subtypes('angel of jubilation', ['Angel']).
card_colors('angel of jubilation', ['W']).
card_text('angel of jubilation', 'Flying\nOther nonblack creatures you control get +1/+1.\nPlayers can\'t pay life or sacrifice creatures to cast spells or activate abilities.').
card_mana_cost('angel of jubilation', ['1', 'W', 'W', 'W']).
card_cmc('angel of jubilation', 4).
card_layout('angel of jubilation', 'normal').
card_power('angel of jubilation', 3).
card_toughness('angel of jubilation', 3).

% Found in: ME2, S99
card_name('angel of light', 'Angel of Light').
card_type('angel of light', 'Creature — Angel').
card_types('angel of light', ['Creature']).
card_subtypes('angel of light', ['Angel']).
card_colors('angel of light', ['W']).
card_text('angel of light', 'Flying, vigilance').
card_mana_cost('angel of light', ['4', 'W']).
card_cmc('angel of light', 5).
card_layout('angel of light', 'normal').
card_power('angel of light', 3).
card_toughness('angel of light', 3).

% Found in: 10E, 8ED, 9ED, DD3_DVD, DDC, INV, PO2, S99
card_name('angel of mercy', 'Angel of Mercy').
card_type('angel of mercy', 'Creature — Angel').
card_types('angel of mercy', ['Creature']).
card_subtypes('angel of mercy', ['Angel']).
card_colors('angel of mercy', ['W']).
card_text('angel of mercy', 'Flying\nWhen Angel of Mercy enters the battlefield, you gain 3 life.').
card_mana_cost('angel of mercy', ['4', 'W']).
card_cmc('angel of mercy', 5).
card_layout('angel of mercy', 'normal').
card_power('angel of mercy', 3).
card_toughness('angel of mercy', 3).

% Found in: BFZ
card_name('angel of renewal', 'Angel of Renewal').
card_type('angel of renewal', 'Creature — Angel Ally').
card_types('angel of renewal', ['Creature']).
card_subtypes('angel of renewal', ['Angel', 'Ally']).
card_colors('angel of renewal', ['W']).
card_text('angel of renewal', 'Flying\nWhen Angel of Renewal enters the battlefield, you gain 1 life for each creature you control.').
card_mana_cost('angel of renewal', ['5', 'W']).
card_cmc('angel of renewal', 6).
card_layout('angel of renewal', 'normal').
card_power('angel of renewal', 4).
card_toughness('angel of renewal', 4).

% Found in: TOR
card_name('angel of retribution', 'Angel of Retribution').
card_type('angel of retribution', 'Creature — Angel').
card_types('angel of retribution', ['Creature']).
card_subtypes('angel of retribution', ['Angel']).
card_colors('angel of retribution', ['W']).
card_text('angel of retribution', 'Flying, first strike').
card_mana_cost('angel of retribution', ['6', 'W']).
card_cmc('angel of retribution', 7).
card_layout('angel of retribution', 'normal').
card_power('angel of retribution', 5).
card_toughness('angel of retribution', 5).

% Found in: DDF, FUT
card_name('angel of salvation', 'Angel of Salvation').
card_type('angel of salvation', 'Creature — Angel').
card_types('angel of salvation', ['Creature']).
card_subtypes('angel of salvation', ['Angel']).
card_colors('angel of salvation', ['W']).
card_text('angel of salvation', 'Flash; convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nFlying\nWhen Angel of Salvation enters the battlefield, prevent the next 5 damage that would be dealt this turn to any number of target creatures and/or players, divided as you choose.').
card_mana_cost('angel of salvation', ['6', 'W', 'W']).
card_cmc('angel of salvation', 8).
card_layout('angel of salvation', 'normal').
card_power('angel of salvation', 5).
card_toughness('angel of salvation', 5).

% Found in: RTR
card_name('angel of serenity', 'Angel of Serenity').
card_type('angel of serenity', 'Creature — Angel').
card_types('angel of serenity', ['Creature']).
card_subtypes('angel of serenity', ['Angel']).
card_colors('angel of serenity', ['W']).
card_text('angel of serenity', 'Flying\nWhen Angel of Serenity enters the battlefield, you may exile up to three other target creatures from the battlefield and/or creature cards from graveyards.\nWhen Angel of Serenity leaves the battlefield, return the exiled cards to their owners\' hands.').
card_mana_cost('angel of serenity', ['4', 'W', 'W', 'W']).
card_cmc('angel of serenity', 7).
card_layout('angel of serenity', 'normal').
card_power('angel of serenity', 5).
card_toughness('angel of serenity', 6).

% Found in: C14
card_name('angel of the dire hour', 'Angel of the Dire Hour').
card_type('angel of the dire hour', 'Creature — Angel').
card_types('angel of the dire hour', ['Creature']).
card_subtypes('angel of the dire hour', ['Angel']).
card_colors('angel of the dire hour', ['W']).
card_text('angel of the dire hour', 'Flash\nFlying\nWhen Angel of the Dire Hour enters the battlefield, if you cast it from your hand, exile all attacking creatures.').
card_mana_cost('angel of the dire hour', ['5', 'W', 'W']).
card_cmc('angel of the dire hour', 7).
card_layout('angel of the dire hour', 'normal').
card_power('angel of the dire hour', 5).
card_toughness('angel of the dire hour', 4).

% Found in: 10E, 9ED, DD3_DVD, DDC, DST, M10, M11, M12
card_name('angel\'s feather', 'Angel\'s Feather').
card_type('angel\'s feather', 'Artifact').
card_types('angel\'s feather', ['Artifact']).
card_subtypes('angel\'s feather', []).
card_colors('angel\'s feather', []).
card_text('angel\'s feather', 'Whenever a player casts a white spell, you may gain 1 life.').
card_mana_cost('angel\'s feather', ['2']).
card_cmc('angel\'s feather', 2).
card_layout('angel\'s feather', 'normal').

% Found in: MMA, TSP
card_name('angel\'s grace', 'Angel\'s Grace').
card_type('angel\'s grace', 'Instant').
card_types('angel\'s grace', ['Instant']).
card_subtypes('angel\'s grace', []).
card_colors('angel\'s grace', ['W']).
card_text('angel\'s grace', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nYou can\'t lose the game this turn and your opponents can\'t win the game this turn. Until end of turn, damage that would reduce your life total to less than 1 reduces it to 1 instead.').
card_mana_cost('angel\'s grace', ['W']).
card_cmc('angel\'s grace', 1).
card_layout('angel\'s grace', 'normal').

% Found in: ALA
card_name('angel\'s herald', 'Angel\'s Herald').
card_type('angel\'s herald', 'Creature — Human Cleric').
card_types('angel\'s herald', ['Creature']).
card_subtypes('angel\'s herald', ['Human', 'Cleric']).
card_colors('angel\'s herald', ['W']).
card_text('angel\'s herald', '{2}{W}, {T}, Sacrifice a green creature, a white creature, and a blue creature: Search your library for a card named Empyrial Archangel and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('angel\'s herald', ['W']).
card_cmc('angel\'s herald', 1).
card_layout('angel\'s herald', 'normal').
card_power('angel\'s herald', 1).
card_toughness('angel\'s herald', 1).

% Found in: AVR, M10, M12, M13
card_name('angel\'s mercy', 'Angel\'s Mercy').
card_type('angel\'s mercy', 'Instant').
card_types('angel\'s mercy', ['Instant']).
card_subtypes('angel\'s mercy', []).
card_colors('angel\'s mercy', ['W']).
card_text('angel\'s mercy', 'You gain 7 life.').
card_mana_cost('angel\'s mercy', ['2', 'W', 'W']).
card_cmc('angel\'s mercy', 4).
card_layout('angel\'s mercy', 'normal').

% Found in: AVR, ORI
card_name('angel\'s tomb', 'Angel\'s Tomb').
card_type('angel\'s tomb', 'Artifact').
card_types('angel\'s tomb', ['Artifact']).
card_subtypes('angel\'s tomb', []).
card_colors('angel\'s tomb', []).
card_text('angel\'s tomb', 'Whenever a creature enters the battlefield under your control, you may have Angel\'s Tomb become a 3/3 white Angel artifact creature with flying until end of turn.').
card_mana_cost('angel\'s tomb', ['3']).
card_cmc('angel\'s tomb', 3).
card_layout('angel\'s tomb', 'normal').

% Found in: ULG
card_name('angel\'s trumpet', 'Angel\'s Trumpet').
card_type('angel\'s trumpet', 'Artifact').
card_types('angel\'s trumpet', ['Artifact']).
card_subtypes('angel\'s trumpet', []).
card_colors('angel\'s trumpet', []).
card_text('angel\'s trumpet', 'All creatures have vigilance.\nAt the beginning of each player\'s end step, tap all untapped creatures that player controls that didn\'t attack this turn. Angel\'s Trumpet deals damage to the player equal to the number of creatures tapped this way.').
card_mana_cost('angel\'s trumpet', ['3']).
card_cmc('angel\'s trumpet', 3).
card_layout('angel\'s trumpet', 'normal').

% Found in: APC
card_name('angelfire crusader', 'Angelfire Crusader').
card_type('angelfire crusader', 'Creature — Human Soldier Knight').
card_types('angelfire crusader', ['Creature']).
card_subtypes('angelfire crusader', ['Human', 'Soldier', 'Knight']).
card_colors('angelfire crusader', ['W']).
card_text('angelfire crusader', '{R}: Angelfire Crusader gets +1/+0 until end of turn.').
card_mana_cost('angelfire crusader', ['3', 'W']).
card_cmc('angelfire crusader', 4).
card_layout('angelfire crusader', 'normal').
card_power('angelfire crusader', 2).
card_toughness('angelfire crusader', 3).

% Found in: ROE
card_name('angelheart vial', 'Angelheart Vial').
card_type('angelheart vial', 'Artifact').
card_types('angelheart vial', ['Artifact']).
card_subtypes('angelheart vial', []).
card_colors('angelheart vial', []).
card_text('angelheart vial', 'Whenever you\'re dealt damage, you may put that many charge counters on Angelheart Vial.\n{2}, {T}, Remove four charge counters from Angelheart Vial: You gain 2 life and draw a card.').
card_mana_cost('angelheart vial', ['5']).
card_cmc('angelheart vial', 5).
card_layout('angelheart vial', 'normal').

% Found in: M14
card_name('angelic accord', 'Angelic Accord').
card_type('angelic accord', 'Enchantment').
card_types('angelic accord', ['Enchantment']).
card_subtypes('angelic accord', []).
card_colors('angelic accord', ['W']).
card_text('angelic accord', 'At the beginning of each end step, if you gained 4 or more life this turn, put a 4/4 white Angel creature token with flying onto the battlefield.').
card_mana_cost('angelic accord', ['3', 'W']).
card_cmc('angelic accord', 4).
card_layout('angelic accord', 'normal').

% Found in: CMD, M11
card_name('angelic arbiter', 'Angelic Arbiter').
card_type('angelic arbiter', 'Creature — Angel').
card_types('angelic arbiter', ['Creature']).
card_subtypes('angelic arbiter', ['Angel']).
card_colors('angelic arbiter', ['W']).
card_text('angelic arbiter', 'Flying\nEach opponent who cast a spell this turn can\'t attack with creatures.\nEach opponent who attacked with a creature this turn can\'t cast spells.').
card_mana_cost('angelic arbiter', ['5', 'W', 'W']).
card_cmc('angelic arbiter', 7).
card_layout('angelic arbiter', 'normal').
card_power('angelic arbiter', 5).
card_toughness('angelic arbiter', 6).

% Found in: AVR
card_name('angelic armaments', 'Angelic Armaments').
card_type('angelic armaments', 'Artifact — Equipment').
card_types('angelic armaments', ['Artifact']).
card_subtypes('angelic armaments', ['Equipment']).
card_colors('angelic armaments', []).
card_text('angelic armaments', 'Equipped creature gets +2/+2, has flying, and is a white Angel in addition to its other colors and types.\nEquip {4}').
card_mana_cost('angelic armaments', ['3']).
card_cmc('angelic armaments', 3).
card_layout('angelic armaments', 'normal').

% Found in: ALA, DD3_DVD, DDC, M13
card_name('angelic benediction', 'Angelic Benediction').
card_type('angelic benediction', 'Enchantment').
card_types('angelic benediction', ['Enchantment']).
card_subtypes('angelic benediction', []).
card_colors('angelic benediction', ['W']).
card_text('angelic benediction', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nWhenever a creature you control attacks alone, you may tap target creature.').
card_mana_cost('angelic benediction', ['3', 'W']).
card_cmc('angelic benediction', 4).
card_layout('angelic benediction', 'normal').

% Found in: 10E, 9ED, EXO, PO2, POR, S00, S99, TPR
card_name('angelic blessing', 'Angelic Blessing').
card_type('angelic blessing', 'Sorcery').
card_types('angelic blessing', ['Sorcery']).
card_subtypes('angelic blessing', []).
card_colors('angelic blessing', ['W']).
card_text('angelic blessing', 'Target creature gets +3/+3 and gains flying until end of turn. (It can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('angelic blessing', ['2', 'W']).
card_cmc('angelic blessing', 3).
card_layout('angelic blessing', 'normal').

% Found in: BFZ
card_name('angelic captain', 'Angelic Captain').
card_type('angelic captain', 'Creature — Angel Ally').
card_types('angelic captain', ['Creature']).
card_subtypes('angelic captain', ['Angel', 'Ally']).
card_colors('angelic captain', ['W', 'R']).
card_text('angelic captain', 'Flying\nWhenever Angelic Captain attacks, it gets +1/+1 until end of turn for each other attacking Ally.').
card_mana_cost('angelic captain', ['3', 'R', 'W']).
card_cmc('angelic captain', 5).
card_layout('angelic captain', 'normal').
card_power('angelic captain', 4).
card_toughness('angelic captain', 3).

% Found in: 10E, USG
card_name('angelic chorus', 'Angelic Chorus').
card_type('angelic chorus', 'Enchantment').
card_types('angelic chorus', ['Enchantment']).
card_subtypes('angelic chorus', []).
card_colors('angelic chorus', ['W']).
card_text('angelic chorus', 'Whenever a creature enters the battlefield under your control, you gain life equal to its toughness.').
card_mana_cost('angelic chorus', ['3', 'W', 'W']).
card_cmc('angelic chorus', 5).
card_layout('angelic chorus', 'normal').

% Found in: ULG
card_name('angelic curator', 'Angelic Curator').
card_type('angelic curator', 'Creature — Angel Spirit').
card_types('angelic curator', ['Creature']).
card_subtypes('angelic curator', ['Angel', 'Spirit']).
card_colors('angelic curator', ['W']).
card_text('angelic curator', 'Flying, protection from artifacts').
card_mana_cost('angelic curator', ['1', 'W']).
card_cmc('angelic curator', 2).
card_layout('angelic curator', 'normal').
card_power('angelic curator', 1).
card_toughness('angelic curator', 1).

% Found in: M12
card_name('angelic destiny', 'Angelic Destiny').
card_type('angelic destiny', 'Enchantment — Aura').
card_types('angelic destiny', ['Enchantment']).
card_subtypes('angelic destiny', ['Aura']).
card_colors('angelic destiny', ['W']).
card_text('angelic destiny', 'Enchant creature\nEnchanted creature gets +4/+4, has flying and first strike, and is an Angel in addition to its other types.\nWhen enchanted creature dies, return Angelic Destiny to its owner\'s hand.').
card_mana_cost('angelic destiny', ['2', 'W', 'W']).
card_cmc('angelic destiny', 4).
card_layout('angelic destiny', 'normal').

% Found in: GTC
card_name('angelic edict', 'Angelic Edict').
card_type('angelic edict', 'Sorcery').
card_types('angelic edict', ['Sorcery']).
card_subtypes('angelic edict', []).
card_colors('angelic edict', ['W']).
card_text('angelic edict', 'Exile target creature or enchantment.').
card_mana_cost('angelic edict', ['4', 'W']).
card_cmc('angelic edict', 5).
card_layout('angelic edict', 'normal').

% Found in: NMS
card_name('angelic favor', 'Angelic Favor').
card_type('angelic favor', 'Instant').
card_types('angelic favor', ['Instant']).
card_subtypes('angelic favor', []).
card_colors('angelic favor', ['W']).
card_text('angelic favor', 'If you control a Plains, you may tap an untapped creature you control rather than pay Angelic Favor\'s mana cost.\nCast Angelic Favor only during combat.\nPut a 4/4 white Angel creature token with flying onto the battlefield. Exile it at the beginning of the next end step.').
card_mana_cost('angelic favor', ['3', 'W']).
card_cmc('angelic favor', 4).
card_layout('angelic favor', 'normal').

% Found in: C14
card_name('angelic field marshal', 'Angelic Field Marshal').
card_type('angelic field marshal', 'Creature — Angel').
card_types('angelic field marshal', ['Creature']).
card_subtypes('angelic field marshal', ['Angel']).
card_colors('angelic field marshal', ['W']).
card_text('angelic field marshal', 'Flying\nLieutenant — As long as you control your commander, Angelic Field Marshal gets +2/+2 and creatures you control have vigilance.').
card_mana_cost('angelic field marshal', ['2', 'W', 'W']).
card_cmc('angelic field marshal', 4).
card_layout('angelic field marshal', 'normal').
card_power('angelic field marshal', 3).
card_toughness('angelic field marshal', 3).

% Found in: BFZ
card_name('angelic gift', 'Angelic Gift').
card_type('angelic gift', 'Enchantment — Aura').
card_types('angelic gift', ['Enchantment']).
card_subtypes('angelic gift', ['Aura']).
card_colors('angelic gift', ['W']).
card_text('angelic gift', 'Enchant creature\nWhen Angelic Gift enters the battlefield, draw a card.\nEnchanted creature has flying.').
card_mana_cost('angelic gift', ['1', 'W']).
card_cmc('angelic gift', 2).
card_layout('angelic gift', 'normal').

% Found in: ISD
card_name('angelic overseer', 'Angelic Overseer').
card_type('angelic overseer', 'Creature — Angel').
card_types('angelic overseer', ['Creature']).
card_subtypes('angelic overseer', ['Angel']).
card_colors('angelic overseer', ['W']).
card_text('angelic overseer', 'Flying\nAs long as you control a Human, Angelic Overseer has hexproof and indestructible.').
card_mana_cost('angelic overseer', ['3', 'W', 'W']).
card_cmc('angelic overseer', 5).
card_layout('angelic overseer', 'normal').
card_power('angelic overseer', 5).
card_toughness('angelic overseer', 3).

% Found in: 7ED, 8ED, BRB, DD3_DVD, DDC, USG
card_name('angelic page', 'Angelic Page').
card_type('angelic page', 'Creature — Angel Spirit').
card_types('angelic page', ['Creature']).
card_subtypes('angelic page', ['Angel', 'Spirit']).
card_colors('angelic page', ['W']).
card_text('angelic page', 'Flying\n{T}: Target attacking or blocking creature gets +1/+1 until end of turn.').
card_mana_cost('angelic page', ['1', 'W']).
card_cmc('angelic page', 2).
card_layout('angelic page', 'normal').
card_power('angelic page', 1).
card_toughness('angelic page', 1).

% Found in: DD3_DVD, DDC, TMP, TPR
card_name('angelic protector', 'Angelic Protector').
card_type('angelic protector', 'Creature — Angel').
card_types('angelic protector', ['Creature']).
card_subtypes('angelic protector', ['Angel']).
card_colors('angelic protector', ['W']).
card_text('angelic protector', 'Flying\nWhenever Angelic Protector becomes the target of a spell or ability, Angelic Protector gets +0/+3 until end of turn.').
card_mana_cost('angelic protector', ['3', 'W']).
card_cmc('angelic protector', 4).
card_layout('angelic protector', 'normal').
card_power('angelic protector', 2).
card_toughness('angelic protector', 2).

% Found in: WTH
card_name('angelic renewal', 'Angelic Renewal').
card_type('angelic renewal', 'Enchantment').
card_types('angelic renewal', ['Enchantment']).
card_subtypes('angelic renewal', []).
card_colors('angelic renewal', ['W']).
card_text('angelic renewal', 'Whenever a creature is put into your graveyard from the battlefield, you may sacrifice Angelic Renewal. If you do, return that card to the battlefield.').
card_mana_cost('angelic renewal', ['1', 'W']).
card_cmc('angelic renewal', 2).
card_layout('angelic renewal', 'normal').

% Found in: DDI, INV
card_name('angelic shield', 'Angelic Shield').
card_type('angelic shield', 'Enchantment').
card_types('angelic shield', ['Enchantment']).
card_subtypes('angelic shield', []).
card_colors('angelic shield', ['W', 'U']).
card_text('angelic shield', 'Creatures you control get +0/+1.\nSacrifice Angelic Shield: Return target creature to its owner\'s hand.').
card_mana_cost('angelic shield', ['W', 'U']).
card_cmc('angelic shield', 2).
card_layout('angelic shield', 'normal').

% Found in: GTC, pMEI
card_name('angelic skirmisher', 'Angelic Skirmisher').
card_type('angelic skirmisher', 'Creature — Angel').
card_types('angelic skirmisher', ['Creature']).
card_subtypes('angelic skirmisher', ['Angel']).
card_colors('angelic skirmisher', ['W']).
card_text('angelic skirmisher', 'Flying\nAt the beginning of each combat, choose first strike, vigilance, or lifelink. Creatures you control gain that ability until end of turn.').
card_mana_cost('angelic skirmisher', ['4', 'W', 'W']).
card_cmc('angelic skirmisher', 6).
card_layout('angelic skirmisher', 'normal').
card_power('angelic skirmisher', 4).
card_toughness('angelic skirmisher', 4).

% Found in: CHR, LEG, ME4
card_name('angelic voices', 'Angelic Voices').
card_type('angelic voices', 'Enchantment').
card_types('angelic voices', ['Enchantment']).
card_subtypes('angelic voices', []).
card_colors('angelic voices', ['W']).
card_text('angelic voices', 'Creatures you control get +1/+1 as long as you control no nonartifact, nonwhite creatures.').
card_mana_cost('angelic voices', ['2', 'W', 'W']).
card_cmc('angelic voices', 4).
card_layout('angelic voices', 'normal').

% Found in: 10E, AVR, M14, ODY, PO2
card_name('angelic wall', 'Angelic Wall').
card_type('angelic wall', 'Creature — Wall').
card_types('angelic wall', ['Creature']).
card_subtypes('angelic wall', ['Wall']).
card_colors('angelic wall', ['W']).
card_text('angelic wall', 'Defender (This creature can\'t attack.)\nFlying').
card_mana_cost('angelic wall', ['1', 'W']).
card_cmc('angelic wall', 2).
card_layout('angelic wall', 'normal').
card_power('angelic wall', 0).
card_toughness('angelic wall', 4).

% Found in: ALA, DD3_DVD, DDC
card_name('angelsong', 'Angelsong').
card_type('angelsong', 'Instant').
card_types('angelsong', ['Instant']).
card_subtypes('angelsong', []).
card_colors('angelsong', ['W']).
card_text('angelsong', 'Prevent all combat damage that would be dealt this turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('angelsong', ['1', 'W']).
card_cmc('angelsong', 2).
card_layout('angelsong', 'normal').

% Found in: CMD, DDI, JUD
card_name('anger', 'Anger').
card_type('anger', 'Creature — Incarnation').
card_types('anger', ['Creature']).
card_subtypes('anger', ['Incarnation']).
card_colors('anger', ['R']).
card_text('anger', 'Haste\nAs long as Anger is in your graveyard and you control a Mountain, creatures you control have haste.').
card_mana_cost('anger', ['3', 'R']).
card_cmc('anger', 4).
card_layout('anger', 'normal').
card_power('anger', 2).
card_toughness('anger', 2).

% Found in: THS
card_name('anger of the gods', 'Anger of the Gods').
card_type('anger of the gods', 'Sorcery').
card_types('anger of the gods', ['Sorcery']).
card_subtypes('anger of the gods', []).
card_colors('anger of the gods', ['R']).
card_text('anger of the gods', 'Anger of the Gods deals 3 damage to each creature. If a creature dealt damage this way would die this turn, exile it instead.').
card_mana_cost('anger of the gods', ['1', 'R', 'R']).
card_cmc('anger of the gods', 3).
card_layout('anger of the gods', 'normal').

% Found in: 4ED, 5ED, DRK, MED
card_name('angry mob', 'Angry Mob').
card_type('angry mob', 'Creature — Human').
card_types('angry mob', ['Creature']).
card_subtypes('angry mob', ['Human']).
card_colors('angry mob', ['W']).
card_text('angry mob', 'Trample\nAs long as it\'s your turn, Angry Mob\'s power and toughness are each equal to 2 plus the number of Swamps your opponents control. As long as it\'s not your turn, Angry Mob\'s power and toughness are each 2.').
card_mana_cost('angry mob', ['2', 'W', 'W']).
card_cmc('angry mob', 4).
card_layout('angry mob', 'normal').
card_power('angry mob', '2+*').
card_toughness('angry mob', '2+*').

% Found in: LEG, ME3
card_name('angus mackenzie', 'Angus Mackenzie').
card_type('angus mackenzie', 'Legendary Creature — Human Cleric').
card_types('angus mackenzie', ['Creature']).
card_subtypes('angus mackenzie', ['Human', 'Cleric']).
card_supertypes('angus mackenzie', ['Legendary']).
card_colors('angus mackenzie', ['W', 'U', 'G']).
card_text('angus mackenzie', '{G}{W}{U}, {T}: Prevent all combat damage that would be dealt this turn. Activate this ability only before the combat damage step.').
card_mana_cost('angus mackenzie', ['G', 'W', 'U']).
card_cmc('angus mackenzie', 3).
card_layout('angus mackenzie', 'normal').
card_power('angus mackenzie', 2).
card_toughness('angus mackenzie', 2).
card_reserved('angus mackenzie').

% Found in: ODY
card_name('animal boneyard', 'Animal Boneyard').
card_type('animal boneyard', 'Enchantment — Aura').
card_types('animal boneyard', ['Enchantment']).
card_subtypes('animal boneyard', ['Aura']).
card_colors('animal boneyard', ['W']).
card_text('animal boneyard', 'Enchant land\nEnchanted land has \"{T}, Sacrifice a creature: You gain life equal to the sacrificed creature\'s toughness.\"').
card_mana_cost('animal boneyard', ['2', 'W']).
card_cmc('animal boneyard', 3).
card_layout('animal boneyard', 'normal').

% Found in: ONS
card_name('animal magnetism', 'Animal Magnetism').
card_type('animal magnetism', 'Sorcery').
card_types('animal magnetism', ['Sorcery']).
card_subtypes('animal magnetism', []).
card_colors('animal magnetism', ['G']).
card_text('animal magnetism', 'Reveal the top five cards of your library. An opponent chooses a creature card from among them. Put that card onto the battlefield and the rest into your graveyard.').
card_mana_cost('animal magnetism', ['4', 'G']).
card_cmc('animal magnetism', 5).
card_layout('animal magnetism', 'normal').

% Found in: CMD
card_name('animar, soul of elements', 'Animar, Soul of Elements').
card_type('animar, soul of elements', 'Legendary Creature — Elemental').
card_types('animar, soul of elements', ['Creature']).
card_subtypes('animar, soul of elements', ['Elemental']).
card_supertypes('animar, soul of elements', ['Legendary']).
card_colors('animar, soul of elements', ['U', 'R', 'G']).
card_text('animar, soul of elements', 'Protection from white and from black\nWhenever you cast a creature spell, put a +1/+1 counter on Animar, Soul of Elements.\nCreature spells you cast cost {1} less to cast for each +1/+1 counter on Animar.').
card_mana_cost('animar, soul of elements', ['U', 'R', 'G']).
card_cmc('animar, soul of elements', 3).
card_layout('animar, soul of elements', 'normal').
card_power('animar, soul of elements', 1).
card_toughness('animar, soul of elements', 1).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4
card_name('animate artifact', 'Animate Artifact').
card_type('animate artifact', 'Enchantment — Aura').
card_types('animate artifact', ['Enchantment']).
card_subtypes('animate artifact', ['Aura']).
card_colors('animate artifact', ['U']).
card_text('animate artifact', 'Enchant artifact\nAs long as enchanted artifact isn\'t a creature, it\'s an artifact creature with power and toughness each equal to its converted mana cost.').
card_mana_cost('animate artifact', ['3', 'U']).
card_cmc('animate artifact', 4).
card_layout('animate artifact', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, MED, PD3, VMA
card_name('animate dead', 'Animate Dead').
card_type('animate dead', 'Enchantment — Aura').
card_types('animate dead', ['Enchantment']).
card_subtypes('animate dead', ['Aura']).
card_colors('animate dead', ['B']).
card_text('animate dead', 'Enchant creature card in a graveyard\nWhen Animate Dead enters the battlefield, if it\'s on the battlefield, it loses \"enchant creature card in a graveyard\" and gains \"enchant creature put onto the battlefield with Animate Dead.\" Return enchanted creature card to the battlefield under your control and attach Animate Dead to it. When Animate Dead leaves the battlefield, that creature\'s controller sacrifices it.\nEnchanted creature gets -1/-0.').
card_mana_cost('animate dead', ['1', 'B']).
card_cmc('animate dead', 2).
card_layout('animate dead', 'normal').

% Found in: NMS
card_name('animate land', 'Animate Land').
card_type('animate land', 'Instant').
card_types('animate land', ['Instant']).
card_subtypes('animate land', []).
card_colors('animate land', ['G']).
card_text('animate land', 'Until end of turn, target land becomes a 3/3 creature that\'s still a land.').
card_mana_cost('animate land', ['G']).
card_cmc('animate land', 1).
card_layout('animate land', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, MED
card_name('animate wall', 'Animate Wall').
card_type('animate wall', 'Enchantment — Aura').
card_types('animate wall', ['Enchantment']).
card_subtypes('animate wall', ['Aura']).
card_colors('animate wall', ['W']).
card_text('animate wall', 'Enchant Wall\nEnchanted Wall can attack as though it didn\'t have defender.').
card_mana_cost('animate wall', ['W']).
card_cmc('animate wall', 1).
card_layout('animate wall', 'normal').

% Found in: ORI
card_name('animist\'s awakening', 'Animist\'s Awakening').
card_type('animist\'s awakening', 'Sorcery').
card_types('animist\'s awakening', ['Sorcery']).
card_subtypes('animist\'s awakening', []).
card_colors('animist\'s awakening', ['G']).
card_text('animist\'s awakening', 'Reveal the top X cards of your library. Put all land cards from among them onto the battlefield tapped and the rest on the bottom of your library in a random order.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, untap those lands.').
card_mana_cost('animist\'s awakening', ['X', 'G']).
card_cmc('animist\'s awakening', 1).
card_layout('animist\'s awakening', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, MED, VMA
card_name('ankh of mishra', 'Ankh of Mishra').
card_type('ankh of mishra', 'Artifact').
card_types('ankh of mishra', ['Artifact']).
card_subtypes('ankh of mishra', []).
card_colors('ankh of mishra', []).
card_text('ankh of mishra', 'Whenever a land enters the battlefield, Ankh of Mishra deals 2 damage to that land\'s controller.').
card_mana_cost('ankh of mishra', ['2']).
card_cmc('ankh of mishra', 2).
card_layout('ankh of mishra', 'normal').

% Found in: KTK, pMEI, pPRE
card_name('ankle shanker', 'Ankle Shanker').
card_type('ankle shanker', 'Creature — Goblin Berserker').
card_types('ankle shanker', ['Creature']).
card_subtypes('ankle shanker', ['Goblin', 'Berserker']).
card_colors('ankle shanker', ['W', 'B', 'R']).
card_text('ankle shanker', 'Haste\nWhenever Ankle Shanker attacks, creatures you control gain first strike and deathtouch until end of turn.').
card_mana_cost('ankle shanker', ['2', 'R', 'W', 'B']).
card_cmc('ankle shanker', 5).
card_layout('ankle shanker', 'normal').
card_power('ankle shanker', 2).
card_toughness('ankle shanker', 2).

% Found in: 9ED, ONS
card_name('annex', 'Annex').
card_type('annex', 'Enchantment — Aura').
card_types('annex', ['Enchantment']).
card_subtypes('annex', ['Aura']).
card_colors('annex', ['U']).
card_text('annex', 'Enchant land (Target a land as you cast this. This card enters the battlefield attached to that land.)\nYou control enchanted land.').
card_mana_cost('annex', ['2', 'U', 'U']).
card_cmc('annex', 4).
card_layout('annex', 'normal').

% Found in: C13, C14, INV
card_name('annihilate', 'Annihilate').
card_type('annihilate', 'Instant').
card_types('annihilate', ['Instant']).
card_subtypes('annihilate', []).
card_colors('annihilate', ['B']).
card_text('annihilate', 'Destroy target nonblack creature. It can\'t be regenerated.\nDraw a card.').
card_mana_cost('annihilate', ['3', 'B', 'B']).
card_cmc('annihilate', 5).
card_layout('annihilate', 'normal').

% Found in: RTR
card_name('annihilating fire', 'Annihilating Fire').
card_type('annihilating fire', 'Instant').
card_types('annihilating fire', ['Instant']).
card_subtypes('annihilating fire', []).
card_colors('annihilating fire', ['R']).
card_text('annihilating fire', 'Annihilating Fire deals 3 damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.').
card_mana_cost('annihilating fire', ['1', 'R', 'R']).
card_cmc('annihilating fire', 3).
card_layout('annihilating fire', 'normal').

% Found in: MRD, THS, USG
card_name('annul', 'Annul').
card_type('annul', 'Instant').
card_types('annul', ['Instant']).
card_subtypes('annul', []).
card_colors('annul', ['U']).
card_text('annul', 'Counter target artifact or enchantment spell.').
card_mana_cost('annul', ['U']).
card_cmc('annul', 1).
card_layout('annul', 'normal').

% Found in: 5DN
card_name('anodet lurker', 'Anodet Lurker').
card_type('anodet lurker', 'Artifact Creature — Construct').
card_types('anodet lurker', ['Artifact', 'Creature']).
card_subtypes('anodet lurker', ['Construct']).
card_colors('anodet lurker', []).
card_text('anodet lurker', 'When Anodet Lurker dies, you gain 3 life.').
card_mana_cost('anodet lurker', ['5']).
card_cmc('anodet lurker', 5).
card_layout('anodet lurker', 'normal').
card_power('anodet lurker', 3).
card_toughness('anodet lurker', 3).

% Found in: TMP, TPR
card_name('anoint', 'Anoint').
card_type('anoint', 'Instant').
card_types('anoint', ['Instant']).
card_subtypes('anoint', []).
card_colors('anoint', ['W']).
card_text('anoint', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nPrevent the next 3 damage that would be dealt to target creature this turn.').
card_mana_cost('anoint', ['W']).
card_cmc('anoint', 1).
card_layout('anoint', 'normal').

% Found in: ORI
card_name('anointer of champions', 'Anointer of Champions').
card_type('anointer of champions', 'Creature — Human Cleric').
card_types('anointer of champions', ['Creature']).
card_subtypes('anointer of champions', ['Human', 'Cleric']).
card_colors('anointer of champions', ['W']).
card_text('anointer of champions', '{T}: Target attacking creature gets +1/+1 until end of turn.').
card_mana_cost('anointer of champions', ['W']).
card_cmc('anointer of champions', 1).
card_layout('anointer of champions', 'normal').
card_power('anointer of champions', 1).
card_toughness('anointer of champions', 1).

% Found in: WWK
card_name('anowon, the ruin sage', 'Anowon, the Ruin Sage').
card_type('anowon, the ruin sage', 'Legendary Creature — Vampire Shaman').
card_types('anowon, the ruin sage', ['Creature']).
card_subtypes('anowon, the ruin sage', ['Vampire', 'Shaman']).
card_supertypes('anowon, the ruin sage', ['Legendary']).
card_colors('anowon, the ruin sage', ['B']).
card_text('anowon, the ruin sage', 'At the beginning of your upkeep, each player sacrifices a non-Vampire creature.').
card_mana_cost('anowon, the ruin sage', ['3', 'B', 'B']).
card_cmc('anowon, the ruin sage', 5).
card_layout('anowon, the ruin sage', 'normal').
card_power('anowon, the ruin sage', 4).
card_toughness('anowon, the ruin sage', 3).

% Found in: M10, MM2, pLPA
card_name('ant queen', 'Ant Queen').
card_type('ant queen', 'Creature — Insect').
card_types('ant queen', ['Creature']).
card_subtypes('ant queen', ['Insect']).
card_colors('ant queen', ['G']).
card_text('ant queen', '{1}{G}: Put a 1/1 green Insect creature token onto the battlefield.').
card_mana_cost('ant queen', ['3', 'G', 'G']).
card_cmc('ant queen', 5).
card_layout('ant queen', 'normal').
card_power('ant queen', 5).
card_toughness('ant queen', 5).

% Found in: USG
card_name('antagonism', 'Antagonism').
card_type('antagonism', 'Enchantment').
card_types('antagonism', ['Enchantment']).
card_subtypes('antagonism', []).
card_colors('antagonism', ['R']).
card_text('antagonism', 'At the beginning of each player\'s end step, Antagonism deals 2 damage to that player unless one of his or her opponents was dealt damage this turn.').
card_mana_cost('antagonism', ['3', 'R']).
card_cmc('antagonism', 4).
card_layout('antagonism', 'normal').

% Found in: DIS
card_name('anthem of rakdos', 'Anthem of Rakdos').
card_type('anthem of rakdos', 'Enchantment').
card_types('anthem of rakdos', ['Enchantment']).
card_subtypes('anthem of rakdos', []).
card_colors('anthem of rakdos', ['B', 'R']).
card_text('anthem of rakdos', 'Whenever a creature you control attacks, it gets +2/+0 until end of turn and Anthem of Rakdos deals 1 damage to you.\nHellbent — As long as you have no cards in hand, if a source you control would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_mana_cost('anthem of rakdos', ['2', 'B', 'R', 'R']).
card_cmc('anthem of rakdos', 5).
card_layout('anthem of rakdos', 'normal').

% Found in: THS, pPRE
card_name('anthousa, setessan hero', 'Anthousa, Setessan Hero').
card_type('anthousa, setessan hero', 'Legendary Creature — Human Warrior').
card_types('anthousa, setessan hero', ['Creature']).
card_subtypes('anthousa, setessan hero', ['Human', 'Warrior']).
card_supertypes('anthousa, setessan hero', ['Legendary']).
card_colors('anthousa, setessan hero', ['G']).
card_text('anthousa, setessan hero', 'Heroic — Whenever you cast a spell that targets Anthousa, Setessan Hero, up to three target lands you control each become 2/2 Warrior creatures until end of turn. They\'re still lands.').
card_mana_cost('anthousa, setessan hero', ['3', 'G', 'G']).
card_cmc('anthousa, setessan hero', 5).
card_layout('anthousa, setessan hero', 'normal').
card_power('anthousa, setessan hero', 4).
card_toughness('anthousa, setessan hero', 5).

% Found in: ULG
card_name('anthroplasm', 'Anthroplasm').
card_type('anthroplasm', 'Creature — Shapeshifter').
card_types('anthroplasm', ['Creature']).
card_subtypes('anthroplasm', ['Shapeshifter']).
card_colors('anthroplasm', ['U']).
card_text('anthroplasm', 'Anthroplasm enters the battlefield with two +1/+1 counters on it.\n{X}, {T}: Remove all +1/+1 counters from Anthroplasm and put X +1/+1 counters on it.').
card_mana_cost('anthroplasm', ['2', 'U', 'U']).
card_cmc('anthroplasm', 4).
card_layout('anthroplasm', 'normal').
card_power('anthroplasm', 0).
card_toughness('anthroplasm', 0).

% Found in: 5ED, LEG
card_name('anti-magic aura', 'Anti-Magic Aura').
card_type('anti-magic aura', 'Enchantment — Aura').
card_types('anti-magic aura', ['Enchantment']).
card_subtypes('anti-magic aura', ['Aura']).
card_colors('anti-magic aura', ['U']).
card_text('anti-magic aura', 'Enchant creature\nEnchanted creature can\'t be the target of spells and can\'t be enchanted by other Auras.').
card_mana_cost('anti-magic aura', ['2', 'U']).
card_cmc('anti-magic aura', 3).
card_layout('anti-magic aura', 'normal').

% Found in: BFZ, DTK
card_name('anticipate', 'Anticipate').
card_type('anticipate', 'Instant').
card_types('anticipate', ['Instant']).
card_subtypes('anticipate', []).
card_colors('anticipate', ['U']).
card_text('anticipate', 'Look at the top three cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('anticipate', ['1', 'U']).
card_cmc('anticipate', 2).
card_layout('anticipate', 'normal').

% Found in: EVE
card_name('antler skulkin', 'Antler Skulkin').
card_type('antler skulkin', 'Artifact Creature — Scarecrow').
card_types('antler skulkin', ['Artifact', 'Creature']).
card_subtypes('antler skulkin', ['Scarecrow']).
card_colors('antler skulkin', []).
card_text('antler skulkin', '{2}: Target white creature gains persist until end of turn. (When it dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('antler skulkin', ['5']).
card_cmc('antler skulkin', 5).
card_layout('antler skulkin', 'normal').
card_power('antler skulkin', 3).
card_toughness('antler skulkin', 3).

% Found in: JUD
card_name('anurid barkripper', 'Anurid Barkripper').
card_type('anurid barkripper', 'Creature — Frog Beast').
card_types('anurid barkripper', ['Creature']).
card_subtypes('anurid barkripper', ['Frog', 'Beast']).
card_colors('anurid barkripper', ['G']).
card_text('anurid barkripper', 'Threshold — Anurid Barkripper gets +2/+2 as long as seven or more cards are in your graveyard.').
card_mana_cost('anurid barkripper', ['1', 'G', 'G']).
card_cmc('anurid barkripper', 3).
card_layout('anurid barkripper', 'normal').
card_power('anurid barkripper', 2).
card_toughness('anurid barkripper', 2).

% Found in: JUD
card_name('anurid brushhopper', 'Anurid Brushhopper').
card_type('anurid brushhopper', 'Creature — Frog Beast').
card_types('anurid brushhopper', ['Creature']).
card_subtypes('anurid brushhopper', ['Frog', 'Beast']).
card_colors('anurid brushhopper', ['W', 'G']).
card_text('anurid brushhopper', 'Discard two cards: Exile Anurid Brushhopper. Return it to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('anurid brushhopper', ['1', 'G', 'W']).
card_cmc('anurid brushhopper', 3).
card_layout('anurid brushhopper', 'normal').
card_power('anurid brushhopper', 3).
card_toughness('anurid brushhopper', 4).

% Found in: ONS
card_name('anurid murkdiver', 'Anurid Murkdiver').
card_type('anurid murkdiver', 'Creature — Zombie Frog Beast').
card_types('anurid murkdiver', ['Creature']).
card_subtypes('anurid murkdiver', ['Zombie', 'Frog', 'Beast']).
card_colors('anurid murkdiver', ['B']).
card_text('anurid murkdiver', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)').
card_mana_cost('anurid murkdiver', ['4', 'B', 'B']).
card_cmc('anurid murkdiver', 6).
card_layout('anurid murkdiver', 'normal').
card_power('anurid murkdiver', 4).
card_toughness('anurid murkdiver', 3).

% Found in: TOR
card_name('anurid scavenger', 'Anurid Scavenger').
card_type('anurid scavenger', 'Creature — Frog Beast').
card_types('anurid scavenger', ['Creature']).
card_subtypes('anurid scavenger', ['Frog', 'Beast']).
card_colors('anurid scavenger', ['G']).
card_text('anurid scavenger', 'Protection from black\nAt the beginning of your upkeep, sacrifice Anurid Scavenger unless you put a card from your graveyard on the bottom of your library.').
card_mana_cost('anurid scavenger', ['2', 'G']).
card_cmc('anurid scavenger', 3).
card_layout('anurid scavenger', 'normal').
card_power('anurid scavenger', 3).
card_toughness('anurid scavenger', 3).

% Found in: JUD
card_name('anurid swarmsnapper', 'Anurid Swarmsnapper').
card_type('anurid swarmsnapper', 'Creature — Frog Beast').
card_types('anurid swarmsnapper', ['Creature']).
card_subtypes('anurid swarmsnapper', ['Frog', 'Beast']).
card_colors('anurid swarmsnapper', ['G']).
card_text('anurid swarmsnapper', 'Reach (This creature can block creatures with flying.)\n{1}{G}: Anurid Swarmsnapper can block an additional creature this turn.').
card_mana_cost('anurid swarmsnapper', ['2', 'G']).
card_cmc('anurid swarmsnapper', 3).
card_layout('anurid swarmsnapper', 'normal').
card_power('anurid swarmsnapper', 1).
card_toughness('anurid swarmsnapper', 4).

% Found in: VIS
card_name('anvil of bogardan', 'Anvil of Bogardan').
card_type('anvil of bogardan', 'Artifact').
card_types('anvil of bogardan', ['Artifact']).
card_subtypes('anvil of bogardan', []).
card_colors('anvil of bogardan', []).
card_text('anvil of bogardan', 'Players have no maximum hand size.\nAt the beginning of each player\'s draw step, that player draws an additional card, then discards a card.').
card_mana_cost('anvil of bogardan', ['2']).
card_cmc('anvil of bogardan', 2).
card_layout('anvil of bogardan', 'normal').
card_reserved('anvil of bogardan').

% Found in: THS
card_name('anvilwrought raptor', 'Anvilwrought Raptor').
card_type('anvilwrought raptor', 'Artifact Creature — Bird').
card_types('anvilwrought raptor', ['Artifact', 'Creature']).
card_subtypes('anvilwrought raptor', ['Bird']).
card_colors('anvilwrought raptor', []).
card_text('anvilwrought raptor', 'Flying, first strike').
card_mana_cost('anvilwrought raptor', ['4']).
card_cmc('anvilwrought raptor', 4).
card_layout('anvilwrought raptor', 'normal').
card_power('anvilwrought raptor', 2).
card_toughness('anvilwrought raptor', 1).

% Found in: WTH
card_name('apathy', 'Apathy').
card_type('apathy', 'Enchantment — Aura').
card_types('apathy', ['Enchantment']).
card_subtypes('apathy', ['Aura']).
card_colors('apathy', ['U']).
card_text('apathy', 'Enchant creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nAt the beginning of the upkeep of enchanted creature\'s controller, that player may discard a card at random. If he or she does, untap that creature.').
card_mana_cost('apathy', ['U']).
card_cmc('apathy', 1).
card_layout('apathy', 'normal').

% Found in: TMP
card_name('apes of rath', 'Apes of Rath').
card_type('apes of rath', 'Creature — Ape').
card_types('apes of rath', ['Creature']).
card_subtypes('apes of rath', ['Ape']).
card_colors('apes of rath', ['G']).
card_text('apes of rath', 'Whenever Apes of Rath attacks, it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('apes of rath', ['2', 'G', 'G']).
card_cmc('apes of rath', 4).
card_layout('apes of rath', 'normal').
card_power('apes of rath', 5).
card_toughness('apes of rath', 4).

% Found in: CNS, WWK
card_name('apex hawks', 'Apex Hawks').
card_type('apex hawks', 'Creature — Bird').
card_types('apex hawks', ['Creature']).
card_subtypes('apex hawks', ['Bird']).
card_colors('apex hawks', ['W']).
card_text('apex hawks', 'Multikicker {1}{W} (You may pay an additional {1}{W} any number of times as you cast this spell.)\nFlying\nApex Hawks enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_mana_cost('apex hawks', ['2', 'W']).
card_cmc('apex hawks', 3).
card_layout('apex hawks', 'normal').
card_power('apex hawks', 2).
card_toughness('apex hawks', 2).

% Found in: ONS
card_name('aphetto alchemist', 'Aphetto Alchemist').
card_type('aphetto alchemist', 'Creature — Human Wizard').
card_types('aphetto alchemist', ['Creature']).
card_subtypes('aphetto alchemist', ['Human', 'Wizard']).
card_colors('aphetto alchemist', ['U']).
card_text('aphetto alchemist', '{T}: Untap target artifact or creature.\nMorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('aphetto alchemist', ['1', 'U']).
card_cmc('aphetto alchemist', 2).
card_layout('aphetto alchemist', 'normal').
card_power('aphetto alchemist', 1).
card_toughness('aphetto alchemist', 2).

% Found in: H09, ONS
card_name('aphetto dredging', 'Aphetto Dredging').
card_type('aphetto dredging', 'Sorcery').
card_types('aphetto dredging', ['Sorcery']).
card_subtypes('aphetto dredging', []).
card_colors('aphetto dredging', ['B']).
card_text('aphetto dredging', 'Return up to three target creature cards of the creature type of your choice from your graveyard to your hand.').
card_mana_cost('aphetto dredging', ['3', 'B']).
card_cmc('aphetto dredging', 4).
card_layout('aphetto dredging', 'normal').

% Found in: LGN
card_name('aphetto exterminator', 'Aphetto Exterminator').
card_type('aphetto exterminator', 'Creature — Human Wizard').
card_types('aphetto exterminator', ['Creature']).
card_subtypes('aphetto exterminator', ['Human', 'Wizard']).
card_colors('aphetto exterminator', ['B']).
card_text('aphetto exterminator', 'Morph {3}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Aphetto Exterminator is turned face up, target creature gets -3/-3 until end of turn.').
card_mana_cost('aphetto exterminator', ['2', 'B']).
card_cmc('aphetto exterminator', 3).
card_layout('aphetto exterminator', 'normal').
card_power('aphetto exterminator', 3).
card_toughness('aphetto exterminator', 1).

% Found in: ONS
card_name('aphetto grifter', 'Aphetto Grifter').
card_type('aphetto grifter', 'Creature — Human Wizard').
card_types('aphetto grifter', ['Creature']).
card_subtypes('aphetto grifter', ['Human', 'Wizard']).
card_colors('aphetto grifter', ['U']).
card_text('aphetto grifter', 'Tap two untapped Wizards you control: Tap target permanent.').
card_mana_cost('aphetto grifter', ['2', 'U']).
card_cmc('aphetto grifter', 3).
card_layout('aphetto grifter', 'normal').
card_power('aphetto grifter', 1).
card_toughness('aphetto grifter', 1).

% Found in: SCG
card_name('aphetto runecaster', 'Aphetto Runecaster').
card_type('aphetto runecaster', 'Creature — Human Wizard').
card_types('aphetto runecaster', ['Creature']).
card_subtypes('aphetto runecaster', ['Human', 'Wizard']).
card_colors('aphetto runecaster', ['U']).
card_text('aphetto runecaster', 'Whenever a permanent is turned face up, you may draw a card.').
card_mana_cost('aphetto runecaster', ['3', 'U']).
card_cmc('aphetto runecaster', 4).
card_layout('aphetto runecaster', 'normal').
card_power('aphetto runecaster', 2).
card_toughness('aphetto runecaster', 3).

% Found in: ONS
card_name('aphetto vulture', 'Aphetto Vulture').
card_type('aphetto vulture', 'Creature — Zombie Bird').
card_types('aphetto vulture', ['Creature']).
card_subtypes('aphetto vulture', ['Zombie', 'Bird']).
card_colors('aphetto vulture', ['B']).
card_text('aphetto vulture', 'Flying\nWhen Aphetto Vulture dies, you may put target Zombie card from your graveyard on top of your library.').
card_mana_cost('aphetto vulture', ['4', 'B', 'B']).
card_cmc('aphetto vulture', 6).
card_layout('aphetto vulture', 'normal').
card_power('aphetto vulture', 3).
card_toughness('aphetto vulture', 2).

% Found in: SHM
card_name('aphotic wisps', 'Aphotic Wisps').
card_type('aphotic wisps', 'Instant').
card_types('aphotic wisps', ['Instant']).
card_subtypes('aphotic wisps', []).
card_colors('aphotic wisps', ['B']).
card_text('aphotic wisps', 'Target creature becomes black and gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)\nDraw a card.').
card_mana_cost('aphotic wisps', ['B']).
card_cmc('aphotic wisps', 1).
card_layout('aphotic wisps', 'normal').

% Found in: TMP
card_name('apocalypse', 'Apocalypse').
card_type('apocalypse', 'Sorcery').
card_types('apocalypse', ['Sorcery']).
card_subtypes('apocalypse', []).
card_colors('apocalypse', ['R']).
card_text('apocalypse', 'Exile all permanents. You discard your hand.').
card_mana_cost('apocalypse', ['2', 'R', 'R', 'R']).
card_cmc('apocalypse', 5).
card_layout('apocalypse', 'normal').
card_reserved('apocalypse').

% Found in: HML
card_name('apocalypse chime', 'Apocalypse Chime').
card_type('apocalypse chime', 'Artifact').
card_types('apocalypse chime', ['Artifact']).
card_subtypes('apocalypse chime', []).
card_colors('apocalypse chime', []).
card_text('apocalypse chime', '{2}, {T}, Sacrifice Apocalypse Chime: Destroy all nontoken permanents originally printed in the Homelands expansion. They can\'t be regenerated.').
card_mana_cost('apocalypse chime', ['2']).
card_cmc('apocalypse chime', 2).
card_layout('apocalypse chime', 'normal').
card_reserved('apocalypse chime').

% Found in: CON, MM2
card_name('apocalypse hydra', 'Apocalypse Hydra').
card_type('apocalypse hydra', 'Creature — Hydra').
card_types('apocalypse hydra', ['Creature']).
card_subtypes('apocalypse hydra', ['Hydra']).
card_colors('apocalypse hydra', ['R', 'G']).
card_text('apocalypse hydra', 'Apocalypse Hydra enters the battlefield with X +1/+1 counters on it. If X is 5 or more, it enters the battlefield with an additional X +1/+1 counters on it.\n{1}{R}, Remove a +1/+1 counter from Apocalypse Hydra: Apocalypse Hydra deals 1 damage to target creature or player.').
card_mana_cost('apocalypse hydra', ['X', 'R', 'G']).
card_cmc('apocalypse hydra', 2).
card_layout('apocalypse hydra', 'normal').
card_power('apocalypse hydra', 0).
card_toughness('apocalypse hydra', 0).

% Found in: MM2, NPH
card_name('apostle\'s blessing', 'Apostle\'s Blessing').
card_type('apostle\'s blessing', 'Instant').
card_types('apostle\'s blessing', ['Instant']).
card_subtypes('apostle\'s blessing', []).
card_colors('apostle\'s blessing', ['W']).
card_text('apostle\'s blessing', '({W/P} can be paid with either {W} or 2 life.)\nTarget artifact or creature you control gains protection from artifacts or from the color of your choice until end of turn.').
card_mana_cost('apostle\'s blessing', ['1', 'W/P']).
card_cmc('apostle\'s blessing', 2).
card_layout('apostle\'s blessing', 'normal').

% Found in: SHM
card_name('apothecary initiate', 'Apothecary Initiate').
card_type('apothecary initiate', 'Creature — Kithkin Cleric').
card_types('apothecary initiate', ['Creature']).
card_subtypes('apothecary initiate', ['Kithkin', 'Cleric']).
card_colors('apothecary initiate', ['W']).
card_text('apothecary initiate', 'Whenever a player casts a white spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('apothecary initiate', ['W']).
card_cmc('apothecary initiate', 1).
card_layout('apothecary initiate', 'normal').
card_power('apothecary initiate', 1).
card_toughness('apothecary initiate', 1).

% Found in: AVR
card_name('appetite for brains', 'Appetite for Brains').
card_type('appetite for brains', 'Sorcery').
card_types('appetite for brains', ['Sorcery']).
card_subtypes('appetite for brains', []).
card_colors('appetite for brains', ['B']).
card_text('appetite for brains', 'Target opponent reveals his or her hand. You choose a card from it with converted mana cost 4 or greater and exile that card.').
card_mana_cost('appetite for brains', ['B']).
card_cmc('appetite for brains', 1).
card_layout('appetite for brains', 'normal').

% Found in: UDS
card_name('apprentice necromancer', 'Apprentice Necromancer').
card_type('apprentice necromancer', 'Creature — Zombie Wizard').
card_types('apprentice necromancer', ['Creature']).
card_subtypes('apprentice necromancer', ['Zombie', 'Wizard']).
card_colors('apprentice necromancer', ['B']).
card_text('apprentice necromancer', '{B}, {T}, Sacrifice Apprentice Necromancer: Return target creature card from your graveyard to the battlefield. That creature gains haste. At the beginning of the next end step, sacrifice it.').
card_mana_cost('apprentice necromancer', ['1', 'B']).
card_cmc('apprentice necromancer', 2).
card_layout('apprentice necromancer', 'normal').
card_power('apprentice necromancer', 1).
card_toughness('apprentice necromancer', 1).

% Found in: PO2
card_name('apprentice sorcerer', 'Apprentice Sorcerer').
card_type('apprentice sorcerer', 'Creature — Human Wizard').
card_types('apprentice sorcerer', ['Creature']).
card_subtypes('apprentice sorcerer', ['Human', 'Wizard']).
card_colors('apprentice sorcerer', ['U']).
card_text('apprentice sorcerer', '{T}: Apprentice Sorcerer deals 1 damage to target creature or player. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('apprentice sorcerer', ['2', 'U']).
card_cmc('apprentice sorcerer', 3).
card_layout('apprentice sorcerer', 'normal').
card_power('apprentice sorcerer', 1).
card_toughness('apprentice sorcerer', 1).

% Found in: 4ED, DRK, MED
card_name('apprentice wizard', 'Apprentice Wizard').
card_type('apprentice wizard', 'Creature — Human Wizard').
card_types('apprentice wizard', ['Creature']).
card_subtypes('apprentice wizard', ['Human', 'Wizard']).
card_colors('apprentice wizard', ['U']).
card_text('apprentice wizard', '{U}, {T}: Add {3} to your mana pool.').
card_mana_cost('apprentice wizard', ['1', 'U', 'U']).
card_cmc('apprentice wizard', 3).
card_layout('apprentice wizard', 'normal').
card_power('apprentice wizard', 0).
card_toughness('apprentice wizard', 1).

% Found in: ARC
card_name('approach my molten realm', 'Approach My Molten Realm').
card_type('approach my molten realm', 'Scheme').
card_types('approach my molten realm', ['Scheme']).
card_subtypes('approach my molten realm', []).
card_colors('approach my molten realm', []).
card_text('approach my molten realm', 'When you set this scheme in motion, until your next turn, if a source would deal damage, it deals double that damage instead.').
card_layout('approach my molten realm', 'scheme').

% Found in: TOR, VMA
card_name('aquamoeba', 'Aquamoeba').
card_type('aquamoeba', 'Creature — Elemental Beast').
card_types('aquamoeba', ['Creature']).
card_subtypes('aquamoeba', ['Elemental', 'Beast']).
card_colors('aquamoeba', ['U']).
card_text('aquamoeba', 'Discard a card: Switch Aquamoeba\'s power and toughness until end of turn.').
card_mana_cost('aquamoeba', ['1', 'U']).
card_cmc('aquamoeba', 2).
card_layout('aquamoeba', 'normal').
card_power('aquamoeba', 1).
card_toughness('aquamoeba', 3).

% Found in: DDN, PLC
card_name('aquamorph entity', 'Aquamorph Entity').
card_type('aquamorph entity', 'Creature — Shapeshifter').
card_types('aquamorph entity', ['Creature']).
card_subtypes('aquamorph entity', ['Shapeshifter']).
card_colors('aquamorph entity', ['U']).
card_text('aquamorph entity', 'As Aquamorph Entity enters the battlefield or is turned face up, it becomes your choice of 5/1 or 1/5.\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('aquamorph entity', ['2', 'U', 'U']).
card_cmc('aquamorph entity', 4).
card_layout('aquamorph entity', 'normal').
card_power('aquamorph entity', '*').
card_toughness('aquamorph entity', '*').

% Found in: CMD, DIS, MM2
card_name('aquastrand spider', 'Aquastrand Spider').
card_type('aquastrand spider', 'Creature — Spider Mutant').
card_types('aquastrand spider', ['Creature']).
card_subtypes('aquastrand spider', ['Spider', 'Mutant']).
card_colors('aquastrand spider', ['G']).
card_text('aquastrand spider', 'Graft 2 (This creature enters the battlefield with two +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{G}: Target creature with a +1/+1 counter on it gains reach until end of turn. (It can block creatures with flying.)').
card_mana_cost('aquastrand spider', ['1', 'G']).
card_cmc('aquastrand spider', 2).
card_layout('aquastrand spider', 'normal').
card_power('aquastrand spider', 0).
card_toughness('aquastrand spider', 0).

% Found in: THS
card_name('aqueous form', 'Aqueous Form').
card_type('aqueous form', 'Enchantment — Aura').
card_types('aqueous form', ['Enchantment']).
card_subtypes('aqueous form', ['Aura']).
card_colors('aqueous form', ['U']).
card_text('aqueous form', 'Enchant creature\nEnchanted creature can\'t be blocked.\nWhenever enchanted creature attacks, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('aqueous form', ['U']).
card_cmc('aqueous form', 1).
card_layout('aqueous form', 'normal').

% Found in: LRW
card_name('aquitect\'s will', 'Aquitect\'s Will').
card_type('aquitect\'s will', 'Tribal Sorcery — Merfolk').
card_types('aquitect\'s will', ['Tribal', 'Sorcery']).
card_subtypes('aquitect\'s will', ['Merfolk']).
card_colors('aquitect\'s will', ['U']).
card_text('aquitect\'s will', 'Put a flood counter on target land. That land is an Island in addition to its other types for as long as it has a flood counter on it. If you control a Merfolk, draw a card.').
card_mana_cost('aquitect\'s will', ['U']).
card_cmc('aquitect\'s will', 1).
card_layout('aquitect\'s will', 'normal').

% Found in: RTR
card_name('aquus steed', 'Aquus Steed').
card_type('aquus steed', 'Creature — Beast').
card_types('aquus steed', ['Creature']).
card_subtypes('aquus steed', ['Beast']).
card_colors('aquus steed', ['U']).
card_text('aquus steed', '{2}{U}, {T}: Target creature gets -2/-0 until end of turn.').
card_mana_cost('aquus steed', ['3', 'U']).
card_cmc('aquus steed', 4).
card_layout('aquus steed', 'normal').
card_power('aquus steed', 1).
card_toughness('aquus steed', 3).

% Found in: SOK
card_name('araba mothrider', 'Araba Mothrider').
card_type('araba mothrider', 'Creature — Human Samurai').
card_types('araba mothrider', ['Creature']).
card_subtypes('araba mothrider', ['Human', 'Samurai']).
card_colors('araba mothrider', ['W']).
card_text('araba mothrider', 'Flying\nBushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)').
card_mana_cost('araba mothrider', ['1', 'W']).
card_cmc('araba mothrider', 2).
card_layout('araba mothrider', 'normal').
card_power('araba mothrider', 1).
card_toughness('araba mothrider', 1).

% Found in: 5DN
card_name('arachnoid', 'Arachnoid').
card_type('arachnoid', 'Artifact Creature — Spider').
card_types('arachnoid', ['Artifact', 'Creature']).
card_subtypes('arachnoid', ['Spider']).
card_colors('arachnoid', []).
card_text('arachnoid', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('arachnoid', ['6']).
card_cmc('arachnoid', 6).
card_layout('arachnoid', 'normal').
card_power('arachnoid', 2).
card_toughness('arachnoid', 6).

% Found in: M12
card_name('arachnus spinner', 'Arachnus Spinner').
card_type('arachnus spinner', 'Creature — Spider').
card_types('arachnus spinner', ['Creature']).
card_subtypes('arachnus spinner', ['Spider']).
card_colors('arachnus spinner', ['G']).
card_text('arachnus spinner', 'Reach (This creature can block creatures with flying.)\nTap an untapped Spider you control: Search your graveyard and/or library for a card named Arachnus Web and put it onto the battlefield attached to target creature. If you search your library this way, shuffle it.').
card_mana_cost('arachnus spinner', ['5', 'G']).
card_cmc('arachnus spinner', 6).
card_layout('arachnus spinner', 'normal').
card_power('arachnus spinner', 5).
card_toughness('arachnus spinner', 7).

% Found in: M12
card_name('arachnus web', 'Arachnus Web').
card_type('arachnus web', 'Enchantment — Aura').
card_types('arachnus web', ['Enchantment']).
card_subtypes('arachnus web', ['Aura']).
card_colors('arachnus web', ['G']).
card_text('arachnus web', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.\nAt the beginning of the end step, if enchanted creature\'s power is 4 or greater, destroy Arachnus Web.').
card_mana_cost('arachnus web', ['2', 'G']).
card_cmc('arachnus web', 3).
card_layout('arachnus web', 'normal').

% Found in: SOK
card_name('arashi, the sky asunder', 'Arashi, the Sky Asunder').
card_type('arashi, the sky asunder', 'Legendary Creature — Spirit').
card_types('arashi, the sky asunder', ['Creature']).
card_subtypes('arashi, the sky asunder', ['Spirit']).
card_supertypes('arashi, the sky asunder', ['Legendary']).
card_colors('arashi, the sky asunder', ['G']).
card_text('arashi, the sky asunder', '{X}{G}, {T}: Arashi, the Sky Asunder deals X damage to target creature with flying.\nChannel — {X}{G}{G}, Discard Arashi: Arashi deals X damage to each creature with flying.').
card_mana_cost('arashi, the sky asunder', ['3', 'G', 'G']).
card_cmc('arashi, the sky asunder', 5).
card_layout('arashi, the sky asunder', 'normal').
card_power('arashi, the sky asunder', 5).
card_toughness('arashi, the sky asunder', 5).

% Found in: FRF
card_name('arashin cleric', 'Arashin Cleric').
card_type('arashin cleric', 'Creature — Human Cleric').
card_types('arashin cleric', ['Creature']).
card_subtypes('arashin cleric', ['Human', 'Cleric']).
card_colors('arashin cleric', ['W']).
card_text('arashin cleric', 'When Arashin Cleric enters the battlefield, you gain 3 life.').
card_mana_cost('arashin cleric', ['1', 'W']).
card_cmc('arashin cleric', 2).
card_layout('arashin cleric', 'normal').
card_power('arashin cleric', 1).
card_toughness('arashin cleric', 3).

% Found in: DTK
card_name('arashin foremost', 'Arashin Foremost').
card_type('arashin foremost', 'Creature — Human Warrior').
card_types('arashin foremost', ['Creature']).
card_subtypes('arashin foremost', ['Human', 'Warrior']).
card_colors('arashin foremost', ['W']).
card_text('arashin foremost', 'Double strike\nWhenever Arashin Foremost enters the battlefield or attacks, another target Warrior creature you control gains double strike until end of turn.').
card_mana_cost('arashin foremost', ['1', 'W', 'W']).
card_cmc('arashin foremost', 3).
card_layout('arashin foremost', 'normal').
card_power('arashin foremost', 2).
card_toughness('arashin foremost', 2).

% Found in: DTK, pMEI
card_name('arashin sovereign', 'Arashin Sovereign').
card_type('arashin sovereign', 'Creature — Dragon').
card_types('arashin sovereign', ['Creature']).
card_subtypes('arashin sovereign', ['Dragon']).
card_colors('arashin sovereign', ['W', 'G']).
card_text('arashin sovereign', 'Flying\nWhen Arashin Sovereign dies, you may put it on the top or bottom of its owner\'s library.').
card_mana_cost('arashin sovereign', ['5', 'G', 'W']).
card_cmc('arashin sovereign', 7).
card_layout('arashin sovereign', 'normal').
card_power('arashin sovereign', 6).
card_toughness('arashin sovereign', 6).

% Found in: FRF, FRF_UGIN
card_name('arashin war beast', 'Arashin War Beast').
card_type('arashin war beast', 'Creature — Beast').
card_types('arashin war beast', ['Creature']).
card_subtypes('arashin war beast', ['Beast']).
card_colors('arashin war beast', ['G']).
card_text('arashin war beast', 'Whenever Arashin War Beast deals combat damage to one or more blocking creatures, manifest the top card of your library. (Put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('arashin war beast', ['5', 'G', 'G']).
card_cmc('arashin war beast', 7).
card_layout('arashin war beast', 'normal').
card_power('arashin war beast', 6).
card_toughness('arashin war beast', 6).

% Found in: M12
card_name('arbalest elite', 'Arbalest Elite').
card_type('arbalest elite', 'Creature — Human Archer').
card_types('arbalest elite', ['Creature']).
card_subtypes('arbalest elite', ['Human', 'Archer']).
card_colors('arbalest elite', ['W']).
card_text('arbalest elite', '{2}{W}, {T}: Arbalest Elite deals 3 damage to target attacking or blocking creature. Arbalest Elite doesn\'t untap during your next untap step.').
card_mana_cost('arbalest elite', ['2', 'W', 'W']).
card_cmc('arbalest elite', 4).
card_layout('arbalest elite', 'normal').
card_power('arbalest elite', 2).
card_toughness('arbalest elite', 3).

% Found in: CMD, LRW
card_name('arbiter of knollridge', 'Arbiter of Knollridge').
card_type('arbiter of knollridge', 'Creature — Giant Wizard').
card_types('arbiter of knollridge', ['Creature']).
card_subtypes('arbiter of knollridge', ['Giant', 'Wizard']).
card_colors('arbiter of knollridge', ['W']).
card_text('arbiter of knollridge', 'Vigilance\nWhen Arbiter of Knollridge enters the battlefield, each player\'s life total becomes the highest life total among all players.').
card_mana_cost('arbiter of knollridge', ['6', 'W']).
card_cmc('arbiter of knollridge', 7).
card_layout('arbiter of knollridge', 'normal').
card_power('arbiter of knollridge', 5).
card_toughness('arbiter of knollridge', 5).

% Found in: BNG, pPRE
card_name('arbiter of the ideal', 'Arbiter of the Ideal').
card_type('arbiter of the ideal', 'Creature — Sphinx').
card_types('arbiter of the ideal', ['Creature']).
card_subtypes('arbiter of the ideal', ['Sphinx']).
card_colors('arbiter of the ideal', ['U']).
card_text('arbiter of the ideal', 'Flying\nInspired — Whenever Arbiter of the Ideal becomes untapped, reveal the top card of your library. If it\'s an artifact, creature, or land card, you may put it onto the battlefield with a manifestation counter on it. That permanent is an enchantment in addition to its other types.').
card_mana_cost('arbiter of the ideal', ['4', 'U', 'U']).
card_cmc('arbiter of the ideal', 6).
card_layout('arbiter of the ideal', 'normal').
card_power('arbiter of the ideal', 4).
card_toughness('arbiter of the ideal', 5).

% Found in: THS
card_name('arbor colossus', 'Arbor Colossus').
card_type('arbor colossus', 'Creature — Giant').
card_types('arbor colossus', ['Creature']).
card_subtypes('arbor colossus', ['Giant']).
card_colors('arbor colossus', ['G']).
card_text('arbor colossus', 'Reach\n{3}{G}{G}{G}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)\nWhen Arbor Colossus becomes monstrous, destroy target creature with flying an opponent controls.').
card_mana_cost('arbor colossus', ['2', 'G', 'G', 'G']).
card_cmc('arbor colossus', 5).
card_layout('arbor colossus', 'normal').
card_power('arbor colossus', 6).
card_toughness('arbor colossus', 6).

% Found in: M13, WWK
card_name('arbor elf', 'Arbor Elf').
card_type('arbor elf', 'Creature — Elf Druid').
card_types('arbor elf', ['Creature']).
card_subtypes('arbor elf', ['Elf', 'Druid']).
card_colors('arbor elf', ['G']).
card_text('arbor elf', '{T}: Untap target Forest.').
card_mana_cost('arbor elf', ['G']).
card_cmc('arbor elf', 1).
card_layout('arbor elf', 'normal').
card_power('arbor elf', 1).
card_toughness('arbor elf', 1).

% Found in: LEG, ME3
card_name('arboria', 'Arboria').
card_type('arboria', 'World Enchantment').
card_types('arboria', ['Enchantment']).
card_subtypes('arboria', []).
card_supertypes('arboria', ['World']).
card_colors('arboria', ['G']).
card_text('arboria', 'Creatures can\'t attack a player unless that player cast a spell or put a nontoken permanent onto the battlefield during his or her last turn.').
card_mana_cost('arboria', ['2', 'G', 'G']).
card_cmc('arboria', 4).
card_layout('arboria', 'normal').

% Found in: FUT
card_name('arc blade', 'Arc Blade').
card_type('arc blade', 'Sorcery').
card_types('arc blade', ['Sorcery']).
card_subtypes('arc blade', []).
card_colors('arc blade', ['R']).
card_text('arc blade', 'Arc Blade deals 2 damage to target creature or player. Exile Arc Blade with three time counters on it.\nSuspend 3—{2}{R} (Rather than cast this card from your hand, you may pay {2}{R} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('arc blade', ['3', 'R', 'R']).
card_cmc('arc blade', 5).
card_layout('arc blade', 'normal').

% Found in: BRB, FRF_UGIN, HOP, KTK, USG, pARL
card_name('arc lightning', 'Arc Lightning').
card_type('arc lightning', 'Sorcery').
card_types('arc lightning', ['Sorcery']).
card_subtypes('arc lightning', []).
card_colors('arc lightning', ['R']).
card_text('arc lightning', 'Arc Lightning deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_mana_cost('arc lightning', ['2', 'R']).
card_cmc('arc lightning', 3).
card_layout('arc lightning', 'normal').

% Found in: NMS
card_name('arc mage', 'Arc Mage').
card_type('arc mage', 'Creature — Human Spellshaper').
card_types('arc mage', ['Creature']).
card_subtypes('arc mage', ['Human', 'Spellshaper']).
card_colors('arc mage', ['R']).
card_text('arc mage', '{2}{R}, {T}, Discard a card: Arc Mage deals 2 damage divided as you choose among one or two target creatures and/or players.').
card_mana_cost('arc mage', ['2', 'R']).
card_cmc('arc mage', 3).
card_layout('arc mage', 'normal').
card_power('arc mage', 2).
card_toughness('arc mage', 2).

% Found in: M11
card_name('arc runner', 'Arc Runner').
card_type('arc runner', 'Creature — Elemental Ox').
card_types('arc runner', ['Creature']).
card_subtypes('arc runner', ['Elemental', 'Ox']).
card_colors('arc runner', ['R']).
card_text('arc runner', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nAt the beginning of the end step, sacrifice Arc Runner.').
card_mana_cost('arc runner', ['2', 'R']).
card_cmc('arc runner', 3).
card_layout('arc runner', 'normal').
card_power('arc runner', 5).
card_toughness('arc runner', 1).

% Found in: DDN, PC2, SOM
card_name('arc trail', 'Arc Trail').
card_type('arc trail', 'Sorcery').
card_types('arc trail', ['Sorcery']).
card_subtypes('arc trail', []).
card_colors('arc trail', ['R']).
card_text('arc trail', 'Arc Trail deals 2 damage to target creature or player and 1 damage to another target creature or player.').
card_mana_cost('arc trail', ['1', 'R']).
card_cmc('arc trail', 2).
card_layout('arc trail', 'normal').

% Found in: MRD
card_name('arc-slogger', 'Arc-Slogger').
card_type('arc-slogger', 'Creature — Beast').
card_types('arc-slogger', ['Creature']).
card_subtypes('arc-slogger', ['Beast']).
card_colors('arc-slogger', ['R']).
card_text('arc-slogger', '{R}, Exile the top ten cards of your library: Arc-Slogger deals 2 damage to target creature or player.').
card_mana_cost('arc-slogger', ['3', 'R', 'R']).
card_cmc('arc-slogger', 5).
card_layout('arc-slogger', 'normal').
card_power('arc-slogger', 4).
card_toughness('arc-slogger', 5).

% Found in: CHR, LEG, ME3
card_name('arcades sabboth', 'Arcades Sabboth').
card_type('arcades sabboth', 'Legendary Creature — Elder Dragon').
card_types('arcades sabboth', ['Creature']).
card_subtypes('arcades sabboth', ['Elder', 'Dragon']).
card_supertypes('arcades sabboth', ['Legendary']).
card_colors('arcades sabboth', ['W', 'U', 'G']).
card_text('arcades sabboth', 'Flying\nAt the beginning of your upkeep, sacrifice Arcades Sabboth unless you pay {G}{W}{U}.\nEach untapped creature you control gets +0/+2 as long as it\'s not attacking.\n{W}: Arcades Sabboth gets +0/+1 until end of turn.').
card_mana_cost('arcades sabboth', ['2', 'G', 'G', 'W', 'W', 'U', 'U']).
card_cmc('arcades sabboth', 8).
card_layout('arcades sabboth', 'normal').
card_power('arcades sabboth', 7).
card_toughness('arcades sabboth', 7).

% Found in: ALL, C13, MED
card_name('arcane denial', 'Arcane Denial').
card_type('arcane denial', 'Instant').
card_types('arcane denial', ['Instant']).
card_subtypes('arcane denial', []).
card_colors('arcane denial', ['U']).
card_text('arcane denial', 'Counter target spell. Its controller may draw up to two cards at the beginning of the next turn\'s upkeep.\nYou draw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('arcane denial', ['1', 'U']).
card_cmc('arcane denial', 2).
card_layout('arcane denial', 'normal').

% Found in: 7ED, USG
card_name('arcane laboratory', 'Arcane Laboratory').
card_type('arcane laboratory', 'Enchantment').
card_types('arcane laboratory', ['Enchantment']).
card_subtypes('arcane laboratory', []).
card_colors('arcane laboratory', ['U']).
card_text('arcane laboratory', 'Each player can\'t cast more than one spell each turn.').
card_mana_cost('arcane laboratory', ['2', 'U']).
card_cmc('arcane laboratory', 3).
card_layout('arcane laboratory', 'normal').

% Found in: C14
card_name('arcane lighthouse', 'Arcane Lighthouse').
card_type('arcane lighthouse', 'Land').
card_types('arcane lighthouse', ['Land']).
card_subtypes('arcane lighthouse', []).
card_colors('arcane lighthouse', []).
card_text('arcane lighthouse', '{T}: Add {1} to your mana pool.\n{1}, {T}: Until end of turn, creatures your opponents control lose hexproof and shroud and can\'t have hexproof or shroud.').
card_layout('arcane lighthouse', 'normal').

% Found in: AVR, C13
card_name('arcane melee', 'Arcane Melee').
card_type('arcane melee', 'Enchantment').
card_types('arcane melee', ['Enchantment']).
card_subtypes('arcane melee', []).
card_colors('arcane melee', ['U']).
card_text('arcane melee', 'Instant and sorcery spells cost {2} less to cast.').
card_mana_cost('arcane melee', ['4', 'U']).
card_cmc('arcane melee', 5).
card_layout('arcane melee', 'normal').

% Found in: ALA, C13
card_name('arcane sanctum', 'Arcane Sanctum').
card_type('arcane sanctum', 'Land').
card_types('arcane sanctum', ['Land']).
card_subtypes('arcane sanctum', []).
card_colors('arcane sanctum', []).
card_text('arcane sanctum', 'Arcane Sanctum enters the battlefield tapped.\n{T}: Add {W}, {U}, or {B} to your mana pool.').
card_layout('arcane sanctum', 'normal').

% Found in: DST
card_name('arcane spyglass', 'Arcane Spyglass').
card_type('arcane spyglass', 'Artifact').
card_types('arcane spyglass', ['Artifact']).
card_subtypes('arcane spyglass', []).
card_colors('arcane spyglass', []).
card_text('arcane spyglass', '{2}, {T}, Sacrifice a land: Draw a card and put a charge counter on Arcane Spyglass.\nRemove three charge counters from Arcane Spyglass: Draw a card.').
card_mana_cost('arcane spyglass', ['4']).
card_cmc('arcane spyglass', 4).
card_layout('arcane spyglass', 'normal').

% Found in: 10E, JUD
card_name('arcane teachings', 'Arcane Teachings').
card_type('arcane teachings', 'Enchantment — Aura').
card_types('arcane teachings', ['Enchantment']).
card_subtypes('arcane teachings', ['Aura']).
card_colors('arcane teachings', ['R']).
card_text('arcane teachings', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature gets +2/+2 and has \"{T}: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('arcane teachings', ['2', 'R']).
card_cmc('arcane teachings', 3).
card_layout('arcane teachings', 'normal').

% Found in: 10E, DDN, ONS
card_name('arcanis the omnipotent', 'Arcanis the Omnipotent').
card_type('arcanis the omnipotent', 'Legendary Creature — Wizard').
card_types('arcanis the omnipotent', ['Creature']).
card_subtypes('arcanis the omnipotent', ['Wizard']).
card_supertypes('arcanis the omnipotent', ['Legendary']).
card_colors('arcanis the omnipotent', ['U']).
card_text('arcanis the omnipotent', '{T}: Draw three cards.\n{2}{U}{U}: Return Arcanis the Omnipotent to its owner\'s hand.').
card_mana_cost('arcanis the omnipotent', ['3', 'U', 'U', 'U']).
card_cmc('arcanis the omnipotent', 6).
card_layout('arcanis the omnipotent', 'normal').
card_power('arcanis the omnipotent', 3).
card_toughness('arcanis the omnipotent', 4).

% Found in: VAN
card_name('arcanis, the omnipotent avatar', 'Arcanis, the Omnipotent Avatar').
card_type('arcanis, the omnipotent avatar', 'Vanguard').
card_types('arcanis, the omnipotent avatar', ['Vanguard']).
card_subtypes('arcanis, the omnipotent avatar', []).
card_colors('arcanis, the omnipotent avatar', []).
card_text('arcanis, the omnipotent avatar', '{X}, Return a creature you control with converted mana cost X to its owner\'s hand: Draw a number of cards chosen at random between 0 and X. X can\'t be 0.').
card_layout('arcanis, the omnipotent avatar', 'vanguard').

% Found in: FUT
card_name('arcanum wings', 'Arcanum Wings').
card_type('arcanum wings', 'Enchantment — Aura').
card_types('arcanum wings', ['Enchantment']).
card_subtypes('arcanum wings', ['Aura']).
card_colors('arcanum wings', ['U']).
card_text('arcanum wings', 'Enchant creature\nEnchanted creature has flying.\nAura swap {2}{U} ({2}{U}: Exchange this Aura with an Aura card in your hand.)').
card_mana_cost('arcanum wings', ['1', 'U']).
card_cmc('arcanum wings', 2).
card_layout('arcanum wings', 'normal').

% Found in: FRF
card_name('arcbond', 'Arcbond').
card_type('arcbond', 'Instant').
card_types('arcbond', ['Instant']).
card_subtypes('arcbond', []).
card_colors('arcbond', ['R']).
card_text('arcbond', 'Choose target creature. Whenever that creature is dealt damage this turn, it deals that much damage to each other creature and each player.').
card_mana_cost('arcbond', ['2', 'R']).
card_cmc('arcbond', 3).
card_layout('arcbond', 'normal').

% Found in: DST
card_name('arcbound bruiser', 'Arcbound Bruiser').
card_type('arcbound bruiser', 'Artifact Creature — Golem').
card_types('arcbound bruiser', ['Artifact', 'Creature']).
card_subtypes('arcbound bruiser', ['Golem']).
card_colors('arcbound bruiser', []).
card_text('arcbound bruiser', 'Modular 3 (This enters the battlefield with three +1/+1 counters on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound bruiser', ['5']).
card_cmc('arcbound bruiser', 5).
card_layout('arcbound bruiser', 'normal').
card_power('arcbound bruiser', 0).
card_toughness('arcbound bruiser', 0).

% Found in: DST, HOP
card_name('arcbound crusher', 'Arcbound Crusher').
card_type('arcbound crusher', 'Artifact Creature — Juggernaut').
card_types('arcbound crusher', ['Artifact', 'Creature']).
card_subtypes('arcbound crusher', ['Juggernaut']).
card_colors('arcbound crusher', []).
card_text('arcbound crusher', 'Trample\nWhenever another artifact enters the battlefield, put a +1/+1 counter on Arcbound Crusher.\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound crusher', ['4']).
card_cmc('arcbound crusher', 4).
card_layout('arcbound crusher', 'normal').
card_power('arcbound crusher', 0).
card_toughness('arcbound crusher', 0).

% Found in: DST
card_name('arcbound fiend', 'Arcbound Fiend').
card_type('arcbound fiend', 'Artifact Creature — Horror').
card_types('arcbound fiend', ['Artifact', 'Creature']).
card_subtypes('arcbound fiend', ['Horror']).
card_colors('arcbound fiend', []).
card_text('arcbound fiend', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nAt the beginning of your upkeep, you may move a +1/+1 counter from target creature onto Arcbound Fiend.\nModular 3 (This enters the battlefield with three +1/+1 counters on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound fiend', ['6']).
card_cmc('arcbound fiend', 6).
card_layout('arcbound fiend', 'normal').
card_power('arcbound fiend', 0).
card_toughness('arcbound fiend', 0).

% Found in: DST
card_name('arcbound hybrid', 'Arcbound Hybrid').
card_type('arcbound hybrid', 'Artifact Creature — Beast').
card_types('arcbound hybrid', ['Artifact', 'Creature']).
card_subtypes('arcbound hybrid', ['Beast']).
card_colors('arcbound hybrid', []).
card_text('arcbound hybrid', 'Haste\nModular 2 (This enters the battlefield with two +1/+1 counters on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound hybrid', ['4']).
card_cmc('arcbound hybrid', 4).
card_layout('arcbound hybrid', 'normal').
card_power('arcbound hybrid', 0).
card_toughness('arcbound hybrid', 0).

% Found in: DST
card_name('arcbound lancer', 'Arcbound Lancer').
card_type('arcbound lancer', 'Artifact Creature — Beast').
card_types('arcbound lancer', ['Artifact', 'Creature']).
card_subtypes('arcbound lancer', ['Beast']).
card_colors('arcbound lancer', []).
card_text('arcbound lancer', 'First strike\nModular 4 (This enters the battlefield with four +1/+1 counters on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound lancer', ['7']).
card_cmc('arcbound lancer', 7).
card_layout('arcbound lancer', 'normal').
card_power('arcbound lancer', 0).
card_toughness('arcbound lancer', 0).

% Found in: DST
card_name('arcbound overseer', 'Arcbound Overseer').
card_type('arcbound overseer', 'Artifact Creature — Golem').
card_types('arcbound overseer', ['Artifact', 'Creature']).
card_subtypes('arcbound overseer', ['Golem']).
card_colors('arcbound overseer', []).
card_text('arcbound overseer', 'At the beginning of your upkeep, put a +1/+1 counter on each creature with modular you control.\nModular 6 (This enters the battlefield with six +1/+1 counters on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound overseer', ['8']).
card_cmc('arcbound overseer', 8).
card_layout('arcbound overseer', 'normal').
card_power('arcbound overseer', 0).
card_toughness('arcbound overseer', 0).

% Found in: VAN
card_name('arcbound overseer avatar', 'Arcbound Overseer Avatar').
card_type('arcbound overseer avatar', 'Vanguard').
card_types('arcbound overseer avatar', ['Vanguard']).
card_subtypes('arcbound overseer avatar', []).
card_colors('arcbound overseer avatar', []).
card_text('arcbound overseer avatar', 'At the beginning of your upkeep, you may put a +1/+1 counter on target creature you control.\nAt the beginning of your upkeep, you may put a charge counter on target permanent you control.').
card_layout('arcbound overseer avatar', 'vanguard').

% Found in: DST, MMA
card_name('arcbound ravager', 'Arcbound Ravager').
card_type('arcbound ravager', 'Artifact Creature — Beast').
card_types('arcbound ravager', ['Artifact', 'Creature']).
card_subtypes('arcbound ravager', ['Beast']).
card_colors('arcbound ravager', []).
card_text('arcbound ravager', 'Sacrifice an artifact: Put a +1/+1 counter on Arcbound Ravager.\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound ravager', ['2']).
card_cmc('arcbound ravager', 2).
card_layout('arcbound ravager', 'normal').
card_power('arcbound ravager', 0).
card_toughness('arcbound ravager', 0).

% Found in: DST
card_name('arcbound reclaimer', 'Arcbound Reclaimer').
card_type('arcbound reclaimer', 'Artifact Creature — Golem').
card_types('arcbound reclaimer', ['Artifact', 'Creature']).
card_subtypes('arcbound reclaimer', ['Golem']).
card_colors('arcbound reclaimer', []).
card_text('arcbound reclaimer', 'Remove a +1/+1 counter from Arcbound Reclaimer: Put target artifact card from your graveyard on top of your library.\nModular 2 (This enters the battlefield with two +1/+1 counters on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound reclaimer', ['4']).
card_cmc('arcbound reclaimer', 4).
card_layout('arcbound reclaimer', 'normal').
card_power('arcbound reclaimer', 0).
card_toughness('arcbound reclaimer', 0).

% Found in: DST, HOP
card_name('arcbound slith', 'Arcbound Slith').
card_type('arcbound slith', 'Artifact Creature — Slith').
card_types('arcbound slith', ['Artifact', 'Creature']).
card_subtypes('arcbound slith', ['Slith']).
card_colors('arcbound slith', []).
card_text('arcbound slith', 'Whenever Arcbound Slith deals combat damage to a player, put a +1/+1 counter on it.\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound slith', ['2']).
card_cmc('arcbound slith', 2).
card_layout('arcbound slith', 'normal').
card_power('arcbound slith', 0).
card_toughness('arcbound slith', 0).

% Found in: DST, MMA
card_name('arcbound stinger', 'Arcbound Stinger').
card_type('arcbound stinger', 'Artifact Creature — Insect').
card_types('arcbound stinger', ['Artifact', 'Creature']).
card_subtypes('arcbound stinger', ['Insect']).
card_colors('arcbound stinger', []).
card_text('arcbound stinger', 'Flying\nModular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound stinger', ['2']).
card_cmc('arcbound stinger', 2).
card_layout('arcbound stinger', 'normal').
card_power('arcbound stinger', 0).
card_toughness('arcbound stinger', 0).

% Found in: 5DN, MMA
card_name('arcbound wanderer', 'Arcbound Wanderer').
card_type('arcbound wanderer', 'Artifact Creature — Golem').
card_types('arcbound wanderer', ['Artifact', 'Creature']).
card_subtypes('arcbound wanderer', ['Golem']).
card_colors('arcbound wanderer', []).
card_text('arcbound wanderer', 'Modular—Sunburst (This enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound wanderer', ['6']).
card_cmc('arcbound wanderer', 6).
card_layout('arcbound wanderer', 'normal').
card_power('arcbound wanderer', 0).
card_toughness('arcbound wanderer', 0).

% Found in: DDF, DST, MMA
card_name('arcbound worker', 'Arcbound Worker').
card_type('arcbound worker', 'Artifact Creature — Construct').
card_types('arcbound worker', ['Artifact', 'Creature']).
card_subtypes('arcbound worker', ['Construct']).
card_colors('arcbound worker', []).
card_text('arcbound worker', 'Modular 1 (This enters the battlefield with a +1/+1 counter on it. When it dies, you may put its +1/+1 counters on target artifact creature.)').
card_mana_cost('arcbound worker', ['1']).
card_cmc('arcbound worker', 1).
card_layout('arcbound worker', 'normal').
card_power('arcbound worker', 0).
card_toughness('arcbound worker', 0).

% Found in: INV
card_name('archaeological dig', 'Archaeological Dig').
card_type('archaeological dig', 'Land').
card_types('archaeological dig', ['Land']).
card_subtypes('archaeological dig', []).
card_colors('archaeological dig', []).
card_text('archaeological dig', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Archaeological Dig: Add one mana of any color to your mana pool.').
card_layout('archaeological dig', 'normal').

% Found in: DDM, M13, M14
card_name('archaeomancer', 'Archaeomancer').
card_type('archaeomancer', 'Creature — Human Wizard').
card_types('archaeomancer', ['Creature']).
card_subtypes('archaeomancer', ['Human', 'Wizard']).
card_colors('archaeomancer', ['U']).
card_text('archaeomancer', 'When Archaeomancer enters the battlefield, return target instant or sorcery card from your graveyard to your hand.').
card_mana_cost('archaeomancer', ['2', 'U', 'U']).
card_cmc('archaeomancer', 4).
card_layout('archaeomancer', 'normal').
card_power('archaeomancer', 1).
card_toughness('archaeomancer', 2).

% Found in: 6ED, AVR, C13, PO2, POR, S99, VIS
card_name('archangel', 'Archangel').
card_type('archangel', 'Creature — Angel').
card_types('archangel', ['Creature']).
card_subtypes('archangel', ['Angel']).
card_colors('archangel', ['W']).
card_text('archangel', 'Flying, vigilance').
card_mana_cost('archangel', ['5', 'W', 'W']).
card_cmc('archangel', 7).
card_layout('archangel', 'normal').
card_power('archangel', 5).
card_toughness('archangel', 5).

% Found in: CMD, V15
card_name('archangel of strife', 'Archangel of Strife').
card_type('archangel of strife', 'Creature — Angel').
card_types('archangel of strife', ['Creature']).
card_subtypes('archangel of strife', ['Angel']).
card_colors('archangel of strife', ['W']).
card_text('archangel of strife', 'Flying\nAs Archangel of Strife enters the battlefield, each player chooses war or peace.\nCreatures controlled by players who chose war get +3/+0.\nCreatures controlled by players who chose peace get +0/+3.').
card_mana_cost('archangel of strife', ['5', 'W', 'W']).
card_cmc('archangel of strife', 7).
card_layout('archangel of strife', 'normal').
card_power('archangel of strife', 6).
card_toughness('archangel of strife', 6).

% Found in: M14
card_name('archangel of thune', 'Archangel of Thune').
card_type('archangel of thune', 'Creature — Angel').
card_types('archangel of thune', ['Creature']).
card_subtypes('archangel of thune', ['Angel']).
card_colors('archangel of thune', ['W']).
card_text('archangel of thune', 'Flying\nLifelink (Damage dealt by this creature also causes you to gain that much life.)\nWhenever you gain life, put a +1/+1 counter on each creature you control.').
card_mana_cost('archangel of thune', ['3', 'W', 'W']).
card_cmc('archangel of thune', 5).
card_layout('archangel of thune', 'normal').
card_power('archangel of thune', 3).
card_toughness('archangel of thune', 4).

% Found in: ORI
card_name('archangel of tithes', 'Archangel of Tithes').
card_type('archangel of tithes', 'Creature — Angel').
card_types('archangel of tithes', ['Creature']).
card_subtypes('archangel of tithes', ['Angel']).
card_colors('archangel of tithes', ['W']).
card_text('archangel of tithes', 'Flying\nAs long as Archangel of Tithes is untapped, creatures can\'t attack you or a planeswalker you control unless their controller pays {1} for each of those creatures.\nAs long as Archangel of Tithes is attacking, creatures can\'t block unless their controller pays {1} for each of those creatures.').
card_mana_cost('archangel of tithes', ['1', 'W', 'W', 'W']).
card_cmc('archangel of tithes', 4).
card_layout('archangel of tithes', 'normal').
card_power('archangel of tithes', 3).
card_toughness('archangel of tithes', 5).

% Found in: DKA
card_name('archangel\'s light', 'Archangel\'s Light').
card_type('archangel\'s light', 'Sorcery').
card_types('archangel\'s light', ['Sorcery']).
card_subtypes('archangel\'s light', []).
card_colors('archangel\'s light', ['W']).
card_text('archangel\'s light', 'You gain 2 life for each card in your graveyard, then shuffle your graveyard into your library.').
card_mana_cost('archangel\'s light', ['7', 'W']).
card_cmc('archangel\'s light', 8).
card_layout('archangel\'s light', 'normal').

% Found in: DKA, pPRE
card_name('archdemon of greed', 'Archdemon of Greed').
card_type('archdemon of greed', 'Creature — Demon').
card_types('archdemon of greed', ['Creature']).
card_subtypes('archdemon of greed', ['Demon']).
card_colors('archdemon of greed', ['B']).
card_text('archdemon of greed', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a Human. If you can\'t, tap Archdemon of Greed and it deals 9 damage to you.').
card_layout('archdemon of greed', 'double-faced').
card_power('archdemon of greed', 9).
card_toughness('archdemon of greed', 9).

% Found in: ALA
card_name('archdemon of unx', 'Archdemon of Unx').
card_type('archdemon of unx', 'Creature — Demon').
card_types('archdemon of unx', ['Creature']).
card_subtypes('archdemon of unx', ['Demon']).
card_colors('archdemon of unx', ['B']).
card_text('archdemon of unx', 'Flying, trample\nAt the beginning of your upkeep, sacrifice a non-Zombie creature, then put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('archdemon of unx', ['5', 'B', 'B']).
card_cmc('archdemon of unx', 7).
card_layout('archdemon of unx', 'normal').
card_power('archdemon of unx', 6).
card_toughness('archdemon of unx', 6).

% Found in: FRF
card_name('archers of qarsi', 'Archers of Qarsi').
card_type('archers of qarsi', 'Creature — Naga Archer').
card_types('archers of qarsi', ['Creature']).
card_subtypes('archers of qarsi', ['Naga', 'Archer']).
card_colors('archers of qarsi', ['G']).
card_text('archers of qarsi', 'Defender\nReach (This creature can block creatures with flying.)').
card_mana_cost('archers of qarsi', ['3', 'G']).
card_cmc('archers of qarsi', 4).
card_layout('archers of qarsi', 'normal').
card_power('archers of qarsi', 5).
card_toughness('archers of qarsi', 2).

% Found in: KTK
card_name('archers\' parapet', 'Archers\' Parapet').
card_type('archers\' parapet', 'Creature — Wall').
card_types('archers\' parapet', ['Creature']).
card_subtypes('archers\' parapet', ['Wall']).
card_colors('archers\' parapet', ['G']).
card_text('archers\' parapet', 'Defender\n{1}{B}, {T}: Each opponent loses 1 life.').
card_mana_cost('archers\' parapet', ['1', 'G']).
card_cmc('archers\' parapet', 2).
card_layout('archers\' parapet', 'normal').
card_power('archers\' parapet', 0).
card_toughness('archers\' parapet', 5).

% Found in: UDS
card_name('archery training', 'Archery Training').
card_type('archery training', 'Enchantment — Aura').
card_types('archery training', ['Enchantment']).
card_subtypes('archery training', ['Aura']).
card_colors('archery training', ['W']).
card_text('archery training', 'Enchant creature\nAt the beginning of your upkeep, you may put an arrow counter on Archery Training.\nEnchanted creature has \"{T}: This creature deals X damage to target attacking or blocking creature, where X is the number of arrow counters on Archery Training.\"').
card_mana_cost('archery training', ['W']).
card_cmc('archery training', 1).
card_layout('archery training', 'normal').

% Found in: BNG
card_name('archetype of aggression', 'Archetype of Aggression').
card_type('archetype of aggression', 'Enchantment Creature — Human Warrior').
card_types('archetype of aggression', ['Enchantment', 'Creature']).
card_subtypes('archetype of aggression', ['Human', 'Warrior']).
card_colors('archetype of aggression', ['R']).
card_text('archetype of aggression', 'Creatures you control have trample.\nCreatures your opponents control lose trample and can\'t have or gain trample.').
card_mana_cost('archetype of aggression', ['1', 'R', 'R']).
card_cmc('archetype of aggression', 3).
card_layout('archetype of aggression', 'normal').
card_power('archetype of aggression', 3).
card_toughness('archetype of aggression', 2).

% Found in: BNG
card_name('archetype of courage', 'Archetype of Courage').
card_type('archetype of courage', 'Enchantment Creature — Human Soldier').
card_types('archetype of courage', ['Enchantment', 'Creature']).
card_subtypes('archetype of courage', ['Human', 'Soldier']).
card_colors('archetype of courage', ['W']).
card_text('archetype of courage', 'Creatures you control have first strike.\nCreatures your opponents control lose first strike and can\'t have or gain first strike.').
card_mana_cost('archetype of courage', ['1', 'W', 'W']).
card_cmc('archetype of courage', 3).
card_layout('archetype of courage', 'normal').
card_power('archetype of courage', 2).
card_toughness('archetype of courage', 2).

% Found in: BNG
card_name('archetype of endurance', 'Archetype of Endurance').
card_type('archetype of endurance', 'Enchantment Creature — Boar').
card_types('archetype of endurance', ['Enchantment', 'Creature']).
card_subtypes('archetype of endurance', ['Boar']).
card_colors('archetype of endurance', ['G']).
card_text('archetype of endurance', 'Creatures you control have hexproof.\nCreatures your opponents control lose hexproof and can\'t have or gain hexproof.').
card_mana_cost('archetype of endurance', ['6', 'G', 'G']).
card_cmc('archetype of endurance', 8).
card_layout('archetype of endurance', 'normal').
card_power('archetype of endurance', 6).
card_toughness('archetype of endurance', 5).

% Found in: BNG
card_name('archetype of finality', 'Archetype of Finality').
card_type('archetype of finality', 'Enchantment Creature — Gorgon').
card_types('archetype of finality', ['Enchantment', 'Creature']).
card_subtypes('archetype of finality', ['Gorgon']).
card_colors('archetype of finality', ['B']).
card_text('archetype of finality', 'Creatures you control have deathtouch.\nCreatures your opponents control lose deathtouch and can\'t have or gain deathtouch.').
card_mana_cost('archetype of finality', ['4', 'B', 'B']).
card_cmc('archetype of finality', 6).
card_layout('archetype of finality', 'normal').
card_power('archetype of finality', 2).
card_toughness('archetype of finality', 3).

% Found in: BNG
card_name('archetype of imagination', 'Archetype of Imagination').
card_type('archetype of imagination', 'Enchantment Creature — Human Wizard').
card_types('archetype of imagination', ['Enchantment', 'Creature']).
card_subtypes('archetype of imagination', ['Human', 'Wizard']).
card_colors('archetype of imagination', ['U']).
card_text('archetype of imagination', 'Creatures you control have flying.\nCreatures your opponents control lose flying and can\'t have or gain flying.').
card_mana_cost('archetype of imagination', ['4', 'U', 'U']).
card_cmc('archetype of imagination', 6).
card_layout('archetype of imagination', 'normal').
card_power('archetype of imagination', 3).
card_toughness('archetype of imagination', 2).

% Found in: FRF, pMEI
card_name('archfiend of depravity', 'Archfiend of Depravity').
card_type('archfiend of depravity', 'Creature — Demon').
card_types('archfiend of depravity', ['Creature']).
card_subtypes('archfiend of depravity', ['Demon']).
card_colors('archfiend of depravity', ['B']).
card_text('archfiend of depravity', 'Flying\nAt the beginning of each opponent\'s end step, that player chooses up to two creatures he or she controls, then sacrifices the rest.').
card_mana_cost('archfiend of depravity', ['3', 'B', 'B']).
card_cmc('archfiend of depravity', 5).
card_layout('archfiend of depravity', 'normal').
card_power('archfiend of depravity', 5).
card_toughness('archfiend of depravity', 4).

% Found in: ARB, ARC
card_name('architects of will', 'Architects of Will').
card_type('architects of will', 'Artifact Creature — Human Wizard').
card_types('architects of will', ['Artifact', 'Creature']).
card_subtypes('architects of will', ['Human', 'Wizard']).
card_colors('architects of will', ['U', 'B']).
card_text('architects of will', 'When Architects of Will enters the battlefield, look at the top three cards of target player\'s library, then put them back in any order.\nCycling {U/B} ({U/B}, Discard this card: Draw a card.)').
card_mana_cost('architects of will', ['2', 'U', 'B']).
card_cmc('architects of will', 4).
card_layout('architects of will', 'normal').
card_power('architects of will', 3).
card_toughness('architects of will', 3).

% Found in: ZEN
card_name('archive trap', 'Archive Trap').
card_type('archive trap', 'Instant — Trap').
card_types('archive trap', ['Instant']).
card_subtypes('archive trap', ['Trap']).
card_colors('archive trap', ['U']).
card_text('archive trap', 'If an opponent searched his or her library this turn, you may pay {0} rather than pay Archive Trap\'s mana cost.\nTarget opponent puts the top thirteen cards of his or her library into his or her graveyard.').
card_mana_cost('archive trap', ['3', 'U', 'U']).
card_cmc('archive trap', 5).
card_layout('archive trap', 'normal').

% Found in: 7ED, 8ED, 9ED, ULG
card_name('archivist', 'Archivist').
card_type('archivist', 'Creature — Human Wizard').
card_types('archivist', ['Creature']).
card_subtypes('archivist', ['Human', 'Wizard']).
card_colors('archivist', ['U']).
card_text('archivist', '{T}: Draw a card.').
card_mana_cost('archivist', ['2', 'U', 'U']).
card_cmc('archivist', 4).
card_layout('archivist', 'normal').
card_power('archivist', 1).
card_toughness('archivist', 1).

% Found in: ZEN
card_name('archmage ascension', 'Archmage Ascension').
card_type('archmage ascension', 'Enchantment').
card_types('archmage ascension', ['Enchantment']).
card_subtypes('archmage ascension', []).
card_colors('archmage ascension', ['U']).
card_text('archmage ascension', 'At the beginning of each end step, if you drew two or more cards this turn, you may put a quest counter on Archmage Ascension.\nAs long as Archmage Ascension has six or more quest counters on it, if you would draw a card, you may instead search your library for a card, put that card into your hand, then shuffle your library.').
card_mana_cost('archmage ascension', ['2', 'U']).
card_cmc('archmage ascension', 3).
card_layout('archmage ascension', 'normal').

% Found in: EVE, M12
card_name('archon of justice', 'Archon of Justice').
card_type('archon of justice', 'Creature — Archon').
card_types('archon of justice', ['Creature']).
card_subtypes('archon of justice', ['Archon']).
card_colors('archon of justice', ['W']).
card_text('archon of justice', 'Flying\nWhen Archon of Justice dies, exile target permanent.').
card_mana_cost('archon of justice', ['3', 'W', 'W']).
card_cmc('archon of justice', 5).
card_layout('archon of justice', 'normal').
card_power('archon of justice', 4).
card_toughness('archon of justice', 4).

% Found in: WWK
card_name('archon of redemption', 'Archon of Redemption').
card_type('archon of redemption', 'Creature — Archon').
card_types('archon of redemption', ['Creature']).
card_subtypes('archon of redemption', ['Archon']).
card_colors('archon of redemption', ['W']).
card_text('archon of redemption', 'Flying\nWhenever Archon of Redemption or another creature with flying enters the battlefield under your control, you may gain life equal to that creature\'s power.').
card_mana_cost('archon of redemption', ['3', 'W', 'W']).
card_cmc('archon of redemption', 5).
card_layout('archon of redemption', 'normal').
card_power('archon of redemption', 3).
card_toughness('archon of redemption', 4).

% Found in: RTR, pPRE
card_name('archon of the triumvirate', 'Archon of the Triumvirate').
card_type('archon of the triumvirate', 'Creature — Archon').
card_types('archon of the triumvirate', ['Creature']).
card_subtypes('archon of the triumvirate', ['Archon']).
card_colors('archon of the triumvirate', ['W', 'U']).
card_text('archon of the triumvirate', 'Flying\nWhenever Archon of the Triumvirate attacks, detain up to two target nonland permanents your opponents control. (Until your next turn, those permanents can\'t attack or block and their activated abilities can\'t be activated.)').
card_mana_cost('archon of the triumvirate', ['5', 'W', 'U']).
card_cmc('archon of the triumvirate', 7).
card_layout('archon of the triumvirate', 'normal').
card_power('archon of the triumvirate', 4).
card_toughness('archon of the triumvirate', 5).

% Found in: RTR
card_name('archweaver', 'Archweaver').
card_type('archweaver', 'Creature — Spider').
card_types('archweaver', ['Creature']).
card_subtypes('archweaver', ['Spider']).
card_colors('archweaver', ['G']).
card_text('archweaver', 'Reach, trample').
card_mana_cost('archweaver', ['5', 'G', 'G']).
card_cmc('archweaver', 7).
card_layout('archweaver', 'normal').
card_power('archweaver', 5).
card_toughness('archweaver', 5).

% Found in: AVR
card_name('archwing dragon', 'Archwing Dragon').
card_type('archwing dragon', 'Creature — Dragon').
card_types('archwing dragon', ['Creature']).
card_subtypes('archwing dragon', ['Dragon']).
card_colors('archwing dragon', ['R']).
card_text('archwing dragon', 'Flying, haste\nAt the beginning of the end step, return Archwing Dragon to its owner\'s hand.').
card_mana_cost('archwing dragon', ['2', 'R', 'R']).
card_cmc('archwing dragon', 4).
card_layout('archwing dragon', 'normal').
card_power('archwing dragon', 4).
card_toughness('archwing dragon', 4).

% Found in: M13
card_name('arctic aven', 'Arctic Aven').
card_type('arctic aven', 'Creature — Bird Wizard').
card_types('arctic aven', ['Creature']).
card_subtypes('arctic aven', ['Bird', 'Wizard']).
card_colors('arctic aven', ['U']).
card_text('arctic aven', 'Flying\nArctic Aven gets +1/+1 as long as you control a Plains.\n{W}: Arctic Aven gains lifelink until end of turn. (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('arctic aven', ['2', 'U']).
card_cmc('arctic aven', 3).
card_layout('arctic aven', 'normal').
card_power('arctic aven', 2).
card_toughness('arctic aven', 1).

% Found in: CSP
card_name('arctic flats', 'Arctic Flats').
card_type('arctic flats', 'Snow Land').
card_types('arctic flats', ['Land']).
card_subtypes('arctic flats', []).
card_supertypes('arctic flats', ['Snow']).
card_colors('arctic flats', []).
card_text('arctic flats', 'Arctic Flats enters the battlefield tapped.\n{T}: Add {G} or {W} to your mana pool.').
card_layout('arctic flats', 'normal').

% Found in: ICE
card_name('arctic foxes', 'Arctic Foxes').
card_type('arctic foxes', 'Creature — Fox').
card_types('arctic foxes', ['Creature']).
card_subtypes('arctic foxes', ['Fox']).
card_colors('arctic foxes', ['W']).
card_text('arctic foxes', 'Arctic Foxes can\'t be blocked by creatures with power 2 or greater as long as defending player controls a snow land.').
card_mana_cost('arctic foxes', ['1', 'W']).
card_cmc('arctic foxes', 2).
card_layout('arctic foxes', 'normal').
card_power('arctic foxes', 1).
card_toughness('arctic foxes', 1).

% Found in: PLS
card_name('arctic merfolk', 'Arctic Merfolk').
card_type('arctic merfolk', 'Creature — Merfolk').
card_types('arctic merfolk', ['Creature']).
card_subtypes('arctic merfolk', ['Merfolk']).
card_colors('arctic merfolk', ['U']).
card_text('arctic merfolk', 'Kicker—Return a creature you control to its owner\'s hand. (You may return a creature you control to its owner\'s hand in addition to any other costs as you cast this spell.)\nIf Arctic Merfolk was kicked, it enters the battlefield with a +1/+1 counter on it.').
card_mana_cost('arctic merfolk', ['1', 'U']).
card_cmc('arctic merfolk', 2).
card_layout('arctic merfolk', 'normal').
card_power('arctic merfolk', 1).
card_toughness('arctic merfolk', 1).

% Found in: CSP
card_name('arctic nishoba', 'Arctic Nishoba').
card_type('arctic nishoba', 'Creature — Cat Warrior').
card_types('arctic nishoba', ['Creature']).
card_subtypes('arctic nishoba', ['Cat', 'Warrior']).
card_colors('arctic nishoba', ['G']).
card_text('arctic nishoba', 'Trample\nCumulative upkeep {G} or {W} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Arctic Nishoba dies, you gain 2 life for each age counter on it.').
card_mana_cost('arctic nishoba', ['5', 'G']).
card_cmc('arctic nishoba', 6).
card_layout('arctic nishoba', 'normal').
card_power('arctic nishoba', 6).
card_toughness('arctic nishoba', 6).

% Found in: WTH
card_name('arctic wolves', 'Arctic Wolves').
card_type('arctic wolves', 'Creature — Wolf').
card_types('arctic wolves', ['Creature']).
card_subtypes('arctic wolves', ['Wolf']).
card_colors('arctic wolves', ['G']).
card_text('arctic wolves', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Arctic Wolves enters the battlefield, draw a card.').
card_mana_cost('arctic wolves', ['3', 'G', 'G']).
card_cmc('arctic wolves', 5).
card_layout('arctic wolves', 'normal').
card_power('arctic wolves', 4).
card_toughness('arctic wolves', 5).

% Found in: CSP
card_name('arcum dagsson', 'Arcum Dagsson').
card_type('arcum dagsson', 'Legendary Creature — Human Artificer').
card_types('arcum dagsson', ['Creature']).
card_subtypes('arcum dagsson', ['Human', 'Artificer']).
card_supertypes('arcum dagsson', ['Legendary']).
card_colors('arcum dagsson', ['U']).
card_text('arcum dagsson', '{T}: Target artifact creature\'s controller sacrifices it. That player may search his or her library for a noncreature artifact card, put it onto the battlefield, then shuffle his or her library.').
card_mana_cost('arcum dagsson', ['3', 'U']).
card_cmc('arcum dagsson', 4).
card_layout('arcum dagsson', 'normal').
card_power('arcum dagsson', 2).
card_toughness('arcum dagsson', 2).

% Found in: ICE
card_name('arcum\'s sleigh', 'Arcum\'s Sleigh').
card_type('arcum\'s sleigh', 'Artifact').
card_types('arcum\'s sleigh', ['Artifact']).
card_subtypes('arcum\'s sleigh', []).
card_colors('arcum\'s sleigh', []).
card_text('arcum\'s sleigh', '{2}, {T}: Target creature gains vigilance until end of turn. Activate this ability only during combat and only if defending player controls a snow land.').
card_mana_cost('arcum\'s sleigh', ['1']).
card_cmc('arcum\'s sleigh', 1).
card_layout('arcum\'s sleigh', 'normal').

% Found in: CST, ICE
card_name('arcum\'s weathervane', 'Arcum\'s Weathervane').
card_type('arcum\'s weathervane', 'Artifact').
card_types('arcum\'s weathervane', ['Artifact']).
card_subtypes('arcum\'s weathervane', []).
card_colors('arcum\'s weathervane', []).
card_text('arcum\'s weathervane', '{2}, {T}: Target snow land is no longer snow.\n{2}, {T}: Target nonsnow basic land becomes snow.').
card_mana_cost('arcum\'s weathervane', ['2']).
card_cmc('arcum\'s weathervane', 2).
card_layout('arcum\'s weathervane', 'normal').

% Found in: ICE
card_name('arcum\'s whistle', 'Arcum\'s Whistle').
card_type('arcum\'s whistle', 'Artifact').
card_types('arcum\'s whistle', ['Artifact']).
card_subtypes('arcum\'s whistle', []).
card_colors('arcum\'s whistle', []).
card_text('arcum\'s whistle', '{3}, {T}: Choose target non-Wall creature the active player has controlled continuously since the beginning of the turn. That player may pay {X}, where X is that creature\'s converted mana cost. If he or she doesn\'t, the creature attacks this turn if able, and at the beginning of the next end step, destroy it if it didn\'t attack. Activate this ability only before attackers are declared.').
card_mana_cost('arcum\'s whistle', ['3']).
card_cmc('arcum\'s whistle', 3).
card_layout('arcum\'s whistle', 'normal').

% Found in: 6ED, 7ED, 8ED, POR, S99, WTH
card_name('ardent militia', 'Ardent Militia').
card_type('ardent militia', 'Creature — Human Soldier').
card_types('ardent militia', ['Creature']).
card_subtypes('ardent militia', ['Human', 'Soldier']).
card_colors('ardent militia', ['W']).
card_text('ardent militia', 'Vigilance').
card_mana_cost('ardent militia', ['4', 'W']).
card_cmc('ardent militia', 5).
card_layout('ardent militia', 'normal').
card_power('ardent militia', 2).
card_toughness('ardent militia', 5).

% Found in: ARB
card_name('ardent plea', 'Ardent Plea').
card_type('ardent plea', 'Enchantment').
card_types('ardent plea', ['Enchantment']).
card_subtypes('ardent plea', []).
card_colors('ardent plea', ['W', 'U']).
card_text('ardent plea', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\nCascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)').
card_mana_cost('ardent plea', ['1', 'W', 'U']).
card_cmc('ardent plea', 3).
card_layout('ardent plea', 'normal').

% Found in: MBS
card_name('ardent recruit', 'Ardent Recruit').
card_type('ardent recruit', 'Creature — Human Soldier').
card_types('ardent recruit', ['Creature']).
card_subtypes('ardent recruit', ['Human', 'Soldier']).
card_colors('ardent recruit', ['W']).
card_text('ardent recruit', 'Metalcraft — Ardent Recruit gets +2/+2 as long as you control three or more artifacts.').
card_mana_cost('ardent recruit', ['W']).
card_cmc('ardent recruit', 1).
card_layout('ardent recruit', 'normal').
card_power('ardent recruit', 1).
card_toughness('ardent recruit', 1).

% Found in: INV
card_name('ardent soldier', 'Ardent Soldier').
card_type('ardent soldier', 'Creature — Human Soldier').
card_types('ardent soldier', ['Creature']).
card_subtypes('ardent soldier', ['Human', 'Soldier']).
card_colors('ardent soldier', ['W']).
card_text('ardent soldier', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nVigilance\nIf Ardent Soldier was kicked, it enters the battlefield with a +1/+1 counter on it.').
card_mana_cost('ardent soldier', ['1', 'W']).
card_cmc('ardent soldier', 2).
card_layout('ardent soldier', 'normal').
card_power('ardent soldier', 1).
card_toughness('ardent soldier', 2).

% Found in: TSB, pMEI
card_name('arena', 'Arena').
card_type('arena', 'Land').
card_types('arena', ['Land']).
card_subtypes('arena', []).
card_colors('arena', []).
card_text('arena', '{3}, {T}: Tap target creature you control and target creature of an opponent\'s choice he or she controls. Those creatures fight each other. (Each deals damage equal to its power to the other.)').
card_layout('arena', 'normal').

% Found in: THS
card_name('arena athlete', 'Arena Athlete').
card_type('arena athlete', 'Creature — Human').
card_types('arena athlete', ['Creature']).
card_subtypes('arena athlete', ['Human']).
card_colors('arena athlete', ['R']).
card_text('arena athlete', 'Heroic — Whenever you cast a spell that targets Arena Athlete, target creature an opponent controls can\'t block this turn.').
card_mana_cost('arena athlete', ['1', 'R']).
card_cmc('arena athlete', 2).
card_layout('arena athlete', 'normal').
card_power('arena athlete', 2).
card_toughness('arena athlete', 1).

% Found in: CHR, LEG, ME3
card_name('arena of the ancients', 'Arena of the Ancients').
card_type('arena of the ancients', 'Artifact').
card_types('arena of the ancients', ['Artifact']).
card_subtypes('arena of the ancients', []).
card_colors('arena of the ancients', []).
card_text('arena of the ancients', 'Legendary creatures don\'t untap during their controllers\' untap steps.\nWhen Arena of the Ancients enters the battlefield, tap all legendary creatures.').
card_mana_cost('arena of the ancients', ['3']).
card_cmc('arena of the ancients', 3).
card_layout('arena of the ancients', 'normal').

% Found in: 5ED, ICE
card_name('arenson\'s aura', 'Arenson\'s Aura').
card_type('arenson\'s aura', 'Enchantment').
card_types('arenson\'s aura', ['Enchantment']).
card_subtypes('arenson\'s aura', []).
card_colors('arenson\'s aura', ['W']).
card_text('arenson\'s aura', '{W}, Sacrifice an enchantment: Destroy target enchantment.\n{3}{U}{U}: Counter target enchantment spell.').
card_mana_cost('arenson\'s aura', ['2', 'W']).
card_cmc('arenson\'s aura', 3).
card_layout('arenson\'s aura', 'normal').

% Found in: PC2
card_name('aretopolis', 'Aretopolis').
card_type('aretopolis', 'Plane — Kephalai').
card_types('aretopolis', ['Plane']).
card_subtypes('aretopolis', ['Kephalai']).
card_colors('aretopolis', []).
card_text('aretopolis', 'When you planeswalk to Aretopolis or at the beginning of your upkeep, put a scroll counter on Aretopolis, then you gain life equal to the number of scroll counters on it.\nWhen Aretopolis has ten or more scroll counters on it, planeswalk.\nWhenever you roll {C}, put a scroll counter on Aretopolis, then draw cards equal to the number of scroll counters on it.').
card_layout('aretopolis', 'plane').

% Found in: NPH
card_name('argent mutation', 'Argent Mutation').
card_type('argent mutation', 'Instant').
card_types('argent mutation', ['Instant']).
card_subtypes('argent mutation', []).
card_colors('argent mutation', ['U']).
card_text('argent mutation', 'Target permanent becomes an artifact in addition to its other types until end of turn.\nDraw a card.').
card_mana_cost('argent mutation', ['2', 'U']).
card_cmc('argent mutation', 3).
card_layout('argent mutation', 'normal').

% Found in: MM2, SOM
card_name('argent sphinx', 'Argent Sphinx').
card_type('argent sphinx', 'Creature — Sphinx').
card_types('argent sphinx', ['Creature']).
card_subtypes('argent sphinx', ['Sphinx']).
card_colors('argent sphinx', ['U']).
card_text('argent sphinx', 'Flying\nMetalcraft — {U}: Exile Argent Sphinx. Return it to the battlefield under your control at the beginning of the next end step. Activate this ability only if you control three or more artifacts.').
card_mana_cost('argent sphinx', ['2', 'U', 'U']).
card_cmc('argent sphinx', 4).
card_layout('argent sphinx', 'normal').
card_power('argent sphinx', 4).
card_toughness('argent sphinx', 3).

% Found in: C14, SOM
card_name('argentum armor', 'Argentum Armor').
card_type('argentum armor', 'Artifact — Equipment').
card_types('argentum armor', ['Artifact']).
card_subtypes('argentum armor', ['Equipment']).
card_colors('argentum armor', []).
card_text('argentum armor', 'Equipped creature gets +6/+6.\nWhenever equipped creature attacks, destroy target permanent.\nEquip {6}').
card_mana_cost('argentum armor', ['6']).
card_cmc('argentum armor', 6).
card_layout('argentum armor', 'normal').

% Found in: ATQ, MED
card_name('argivian archaeologist', 'Argivian Archaeologist').
card_type('argivian archaeologist', 'Creature — Human Artificer').
card_types('argivian archaeologist', ['Creature']).
card_subtypes('argivian archaeologist', ['Human', 'Artificer']).
card_colors('argivian archaeologist', ['W']).
card_text('argivian archaeologist', '{W}{W}, {T}: Return target artifact card from your graveyard to your hand.').
card_mana_cost('argivian archaeologist', ['1', 'W', 'W']).
card_cmc('argivian archaeologist', 3).
card_layout('argivian archaeologist', 'normal').
card_power('argivian archaeologist', 1).
card_toughness('argivian archaeologist', 1).
card_reserved('argivian archaeologist').

% Found in: ATQ, ME4
card_name('argivian blacksmith', 'Argivian Blacksmith').
card_type('argivian blacksmith', 'Creature — Human Artificer').
card_types('argivian blacksmith', ['Creature']).
card_subtypes('argivian blacksmith', ['Human', 'Artificer']).
card_colors('argivian blacksmith', ['W']).
card_text('argivian blacksmith', '{T}: Prevent the next 2 damage that would be dealt to target artifact creature this turn.').
card_mana_cost('argivian blacksmith', ['1', 'W', 'W']).
card_cmc('argivian blacksmith', 3).
card_layout('argivian blacksmith', 'normal').
card_power('argivian blacksmith', 2).
card_toughness('argivian blacksmith', 2).

% Found in: WTH
card_name('argivian find', 'Argivian Find').
card_type('argivian find', 'Instant').
card_types('argivian find', ['Instant']).
card_subtypes('argivian find', []).
card_colors('argivian find', ['W']).
card_text('argivian find', 'Return target artifact or enchantment card from your graveyard to your hand.').
card_mana_cost('argivian find', ['W']).
card_cmc('argivian find', 1).
card_layout('argivian find', 'normal').

% Found in: DDF, WTH
card_name('argivian restoration', 'Argivian Restoration').
card_type('argivian restoration', 'Sorcery').
card_types('argivian restoration', ['Sorcery']).
card_subtypes('argivian restoration', []).
card_colors('argivian restoration', ['U']).
card_text('argivian restoration', 'Return target artifact card from your graveyard to the battlefield.').
card_mana_cost('argivian restoration', ['2', 'U', 'U']).
card_cmc('argivian restoration', 4).
card_layout('argivian restoration', 'normal').

% Found in: BRB, USG
card_name('argothian elder', 'Argothian Elder').
card_type('argothian elder', 'Creature — Elf Druid').
card_types('argothian elder', ['Creature']).
card_subtypes('argothian elder', ['Elf', 'Druid']).
card_colors('argothian elder', ['G']).
card_text('argothian elder', '{T}: Untap two target lands.').
card_mana_cost('argothian elder', ['3', 'G']).
card_cmc('argothian elder', 4).
card_layout('argothian elder', 'normal').
card_power('argothian elder', 2).
card_toughness('argothian elder', 2).

% Found in: USG, pJGP
card_name('argothian enchantress', 'Argothian Enchantress').
card_type('argothian enchantress', 'Creature — Human Druid').
card_types('argothian enchantress', ['Creature']).
card_subtypes('argothian enchantress', ['Human', 'Druid']).
card_colors('argothian enchantress', ['G']).
card_text('argothian enchantress', 'Shroud (This creature can\'t be the target of spells or abilities.)\nWhenever you cast an enchantment spell, draw a card.').
card_mana_cost('argothian enchantress', ['1', 'G']).
card_cmc('argothian enchantress', 2).
card_layout('argothian enchantress', 'normal').
card_power('argothian enchantress', 0).
card_toughness('argothian enchantress', 1).

% Found in: ATQ, CHR, ME4
card_name('argothian pixies', 'Argothian Pixies').
card_type('argothian pixies', 'Creature — Faerie').
card_types('argothian pixies', ['Creature']).
card_subtypes('argothian pixies', ['Faerie']).
card_colors('argothian pixies', ['G']).
card_text('argothian pixies', 'Argothian Pixies can\'t be blocked by artifact creatures.\nPrevent all damage that would be dealt to Argothian Pixies by artifact creatures.').
card_mana_cost('argothian pixies', ['1', 'G']).
card_cmc('argothian pixies', 2).
card_layout('argothian pixies', 'normal').
card_power('argothian pixies', 2).
card_toughness('argothian pixies', 1).

% Found in: USG
card_name('argothian swine', 'Argothian Swine').
card_type('argothian swine', 'Creature — Boar').
card_types('argothian swine', ['Creature']).
card_subtypes('argothian swine', ['Boar']).
card_colors('argothian swine', ['G']).
card_text('argothian swine', 'Trample').
card_mana_cost('argothian swine', ['3', 'G']).
card_cmc('argothian swine', 4).
card_layout('argothian swine', 'normal').
card_power('argothian swine', 3).
card_toughness('argothian swine', 3).

% Found in: ATQ, ME4
card_name('argothian treefolk', 'Argothian Treefolk').
card_type('argothian treefolk', 'Creature — Treefolk').
card_types('argothian treefolk', ['Creature']).
card_subtypes('argothian treefolk', ['Treefolk']).
card_colors('argothian treefolk', ['G']).
card_text('argothian treefolk', 'Prevent all damage that would be dealt to Argothian Treefolk by artifact sources.').
card_mana_cost('argothian treefolk', ['3', 'G', 'G']).
card_cmc('argothian treefolk', 5).
card_layout('argothian treefolk', 'normal').
card_power('argothian treefolk', 3).
card_toughness('argothian treefolk', 5).

% Found in: USG
card_name('argothian wurm', 'Argothian Wurm').
card_type('argothian wurm', 'Creature — Wurm').
card_types('argothian wurm', ['Creature']).
card_subtypes('argothian wurm', ['Wurm']).
card_colors('argothian wurm', ['G']).
card_text('argothian wurm', 'Trample\nWhen Argothian Wurm enters the battlefield, any player may sacrifice a land. If a player does, put Argothian Wurm on top of its owner\'s library.').
card_mana_cost('argothian wurm', ['3', 'G']).
card_cmc('argothian wurm', 4).
card_layout('argothian wurm', 'normal').
card_power('argothian wurm', 6).
card_toughness('argothian wurm', 6).
card_reserved('argothian wurm').

% Found in: EXP, ZEN
card_name('arid mesa', 'Arid Mesa').
card_type('arid mesa', 'Land').
card_types('arid mesa', ['Land']).
card_subtypes('arid mesa', []).
card_colors('arid mesa', []).
card_text('arid mesa', '{T}, Pay 1 life, Sacrifice Arid Mesa: Search your library for a Mountain or Plains card and put it onto the battlefield. Then shuffle your library.').
card_layout('arid mesa', 'normal').

% Found in: SCG
card_name('ark of blight', 'Ark of Blight').
card_type('ark of blight', 'Artifact').
card_types('ark of blight', ['Artifact']).
card_subtypes('ark of blight', []).
card_colors('ark of blight', []).
card_text('ark of blight', '{3}, {T}, Sacrifice Ark of Blight: Destroy target land.').
card_mana_cost('ark of blight', ['2']).
card_cmc('ark of blight', 2).
card_layout('ark of blight', 'normal').

% Found in: NPH
card_name('arm with æther', 'Arm with Æther').
card_type('arm with æther', 'Sorcery').
card_types('arm with æther', ['Sorcery']).
card_subtypes('arm with æther', []).
card_colors('arm with æther', ['U']).
card_text('arm with æther', 'Until end of turn, creatures you control gain \"Whenever this creature deals damage to an opponent, you may return target creature that player controls to its owner\'s hand.\"').
card_mana_cost('arm with æther', ['2', 'U']).
card_cmc('arm with æther', 3).
card_layout('arm with æther', 'normal').

% Found in: RTR
card_name('armada wurm', 'Armada Wurm').
card_type('armada wurm', 'Creature — Wurm').
card_types('armada wurm', ['Creature']).
card_subtypes('armada wurm', ['Wurm']).
card_colors('armada wurm', ['W', 'G']).
card_text('armada wurm', 'Trample\nWhen Armada Wurm enters the battlefield, put a 5/5 green Wurm creature token with trample onto the battlefield.').
card_mana_cost('armada wurm', ['2', 'G', 'G', 'W', 'W']).
card_cmc('armada wurm', 6).
card_layout('armada wurm', 'normal').
card_power('armada wurm', 5).
card_toughness('armada wurm', 5).

% Found in: ARC, DDE, INV, VMA, pFNM
card_name('armadillo cloak', 'Armadillo Cloak').
card_type('armadillo cloak', 'Enchantment — Aura').
card_types('armadillo cloak', ['Enchantment']).
card_subtypes('armadillo cloak', ['Aura']).
card_colors('armadillo cloak', ['W', 'G']).
card_text('armadillo cloak', 'Enchant creature\nEnchanted creature gets +2/+2 and has trample.\nWhenever enchanted creature deals damage, you gain that much life.').
card_mana_cost('armadillo cloak', ['1', 'G', 'W']).
card_cmc('armadillo cloak', 3).
card_layout('armadillo cloak', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, ATH, CED, CEI, LEA, LEB, ME4, MED, PO2, POR, S99, V14, VMA, pJGP
card_name('armageddon', 'Armageddon').
card_type('armageddon', 'Sorcery').
card_types('armageddon', ['Sorcery']).
card_subtypes('armageddon', []).
card_colors('armageddon', ['W']).
card_text('armageddon', 'Destroy all lands.').
card_mana_cost('armageddon', ['3', 'W']).
card_cmc('armageddon', 4).
card_layout('armageddon', 'normal').

% Found in: 3ED, 4ED, ATQ, ME4
card_name('armageddon clock', 'Armageddon Clock').
card_type('armageddon clock', 'Artifact').
card_types('armageddon clock', ['Artifact']).
card_subtypes('armageddon clock', []).
card_colors('armageddon clock', []).
card_text('armageddon clock', 'At the beginning of your upkeep, put a doom counter on Armageddon Clock.\nAt the beginning of your draw step, Armageddon Clock deals damage equal to the number of doom counters on it to each player.\n{4}: Remove a doom counter from Armageddon Clock. Any player may activate this ability but only during any upkeep step.').
card_mana_cost('armageddon clock', ['6']).
card_cmc('armageddon clock', 6).
card_layout('armageddon clock', 'normal').

% Found in: KTK
card_name('armament corps', 'Armament Corps').
card_type('armament corps', 'Creature — Human Soldier').
card_types('armament corps', ['Creature']).
card_subtypes('armament corps', ['Human', 'Soldier']).
card_colors('armament corps', ['W', 'B', 'G']).
card_text('armament corps', 'When Armament Corps enters the battlefield, distribute two +1/+1 counters among one or two target creatures you control.').
card_mana_cost('armament corps', ['2', 'W', 'B', 'G']).
card_cmc('armament corps', 5).
card_layout('armament corps', 'normal').
card_power('armament corps', 4).
card_toughness('armament corps', 4).

% Found in: ZEN
card_name('armament master', 'Armament Master').
card_type('armament master', 'Creature — Kor Soldier').
card_types('armament master', ['Creature']).
card_subtypes('armament master', ['Kor', 'Soldier']).
card_colors('armament master', ['W']).
card_text('armament master', 'Other Kor creatures you control get +2/+2 for each Equipment attached to Armament Master.').
card_mana_cost('armament master', ['W', 'W']).
card_cmc('armament master', 2).
card_layout('armament master', 'normal').
card_power('armament master', 2).
card_toughness('armament master', 2).

% Found in: JOU
card_name('armament of nyx', 'Armament of Nyx').
card_type('armament of nyx', 'Enchantment — Aura').
card_types('armament of nyx', ['Enchantment']).
card_subtypes('armament of nyx', ['Aura']).
card_colors('armament of nyx', ['W']).
card_text('armament of nyx', 'Enchant creature\nEnchanted creature has double strike as long as it\'s an enchantment. Otherwise, prevent all damage that would be dealt by enchanted creature. (A creature with double strike deals both first-strike and regular combat damage.)').
card_mana_cost('armament of nyx', ['2', 'W']).
card_cmc('armament of nyx', 3).
card_layout('armament of nyx', 'normal').

% Found in: DGM
card_name('armed', 'Armed').
card_type('armed', 'Sorcery').
card_types('armed', ['Sorcery']).
card_subtypes('armed', []).
card_colors('armed', ['R']).
card_text('armed', 'Target creature gets +1/+1 and gains double strike until end of turn.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('armed', ['1', 'R']).
card_cmc('armed', 2).
card_layout('armed', 'split').
card_sides('armed', 'dangerous').

% Found in: 5DN
card_name('armed response', 'Armed Response').
card_type('armed response', 'Instant').
card_types('armed response', ['Instant']).
card_subtypes('armed response', []).
card_colors('armed response', ['W']).
card_text('armed response', 'Armed Response deals damage to target attacking creature equal to the number of Equipment you control.').
card_mana_cost('armed response', ['2', 'W']).
card_cmc('armed response', 3).
card_layout('armed response', 'normal').

% Found in: C13, CMD, CON, DDG, DDI, PC2
card_name('armillary sphere', 'Armillary Sphere').
card_type('armillary sphere', 'Artifact').
card_types('armillary sphere', ['Artifact']).
card_subtypes('armillary sphere', []).
card_colors('armillary sphere', []).
card_text('armillary sphere', '{2}, {T}, Sacrifice Armillary Sphere: Search your library for up to two basic land cards, reveal them, and put them into your hand. Then shuffle your library.').
card_mana_cost('armillary sphere', ['2']).
card_cmc('armillary sphere', 2).
card_layout('armillary sphere', 'normal').

% Found in: C14, MMQ
card_name('armistice', 'Armistice').
card_type('armistice', 'Enchantment').
card_types('armistice', ['Enchantment']).
card_subtypes('armistice', []).
card_colors('armistice', ['W']).
card_text('armistice', '{3}{W}{W}: You draw a card and target opponent gains 3 life.').
card_mana_cost('armistice', ['2', 'W']).
card_cmc('armistice', 3).
card_layout('armistice', 'normal').

% Found in: 5ED, ICE, ME2
card_name('armor of faith', 'Armor of Faith').
card_type('armor of faith', 'Enchantment — Aura').
card_types('armor of faith', ['Enchantment']).
card_subtypes('armor of faith', ['Aura']).
card_colors('armor of faith', ['W']).
card_text('armor of faith', 'Enchant creature\nEnchanted creature gets +1/+1.\n{W}: Enchanted creature gets +0/+1 until end of turn.').
card_mana_cost('armor of faith', ['W']).
card_cmc('armor of faith', 1).
card_layout('armor of faith', 'normal').

% Found in: MIR, VMA
card_name('armor of thorns', 'Armor of Thorns').
card_type('armor of thorns', 'Enchantment — Aura').
card_types('armor of thorns', ['Enchantment']).
card_subtypes('armor of thorns', ['Aura']).
card_colors('armor of thorns', ['G']).
card_text('armor of thorns', 'You may cast Armor of Thorns as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nEnchant nonblack creature\nEnchanted creature gets +2/+2.').
card_mana_cost('armor of thorns', ['1', 'G']).
card_cmc('armor of thorns', 2).
card_layout('armor of thorns', 'normal').

% Found in: H09, TMP, TPR
card_name('armor sliver', 'Armor Sliver').
card_type('armor sliver', 'Creature — Sliver').
card_types('armor sliver', ['Creature']).
card_subtypes('armor sliver', ['Sliver']).
card_colors('armor sliver', ['W']).
card_text('armor sliver', 'All Sliver creatures have \"{2}: This creature gets +0/+1 until end of turn.\"').
card_mana_cost('armor sliver', ['2', 'W']).
card_cmc('armor sliver', 3).
card_layout('armor sliver', 'normal').
card_power('armor sliver', 2).
card_toughness('armor sliver', 2).

% Found in: FEM, ME2
card_name('armor thrull', 'Armor Thrull').
card_type('armor thrull', 'Creature — Thrull').
card_types('armor thrull', ['Creature']).
card_subtypes('armor thrull', ['Thrull']).
card_colors('armor thrull', ['B']).
card_text('armor thrull', '{T}, Sacrifice Armor Thrull: Put a +1/+2 counter on target creature.').
card_mana_cost('armor thrull', ['2', 'B']).
card_cmc('armor thrull', 3).
card_layout('armor thrull', 'normal').
card_power('armor thrull', 1).
card_toughness('armor thrull', 3).

% Found in: M10, M11, SHM
card_name('armored ascension', 'Armored Ascension').
card_type('armored ascension', 'Enchantment — Aura').
card_types('armored ascension', ['Enchantment']).
card_subtypes('armored ascension', ['Aura']).
card_colors('armored ascension', ['W']).
card_text('armored ascension', 'Enchant creature\nEnchanted creature gets +1/+1 for each Plains you control and has flying.').
card_mana_cost('armored ascension', ['3', 'W']).
card_cmc('armored ascension', 4).
card_layout('armored ascension', 'normal').

% Found in: M11, M14
card_name('armored cancrix', 'Armored Cancrix').
card_type('armored cancrix', 'Creature — Crab').
card_types('armored cancrix', ['Creature']).
card_subtypes('armored cancrix', ['Crab']).
card_colors('armored cancrix', ['U']).
card_text('armored cancrix', '').
card_mana_cost('armored cancrix', ['4', 'U']).
card_cmc('armored cancrix', 5).
card_layout('armored cancrix', 'normal').
card_power('armored cancrix', 2).
card_toughness('armored cancrix', 5).

% Found in: PO2
card_name('armored galleon', 'Armored Galleon').
card_type('armored galleon', 'Creature — Human Pirate').
card_types('armored galleon', ['Creature']).
card_subtypes('armored galleon', ['Human', 'Pirate']).
card_colors('armored galleon', ['U']).
card_text('armored galleon', 'Armored Galleon can\'t attack unless defending player controls an Island.').
card_mana_cost('armored galleon', ['4', 'U']).
card_cmc('armored galleon', 5).
card_layout('armored galleon', 'normal').
card_power('armored galleon', 5).
card_toughness('armored galleon', 4).

% Found in: ME2, PC2, PO2
card_name('armored griffin', 'Armored Griffin').
card_type('armored griffin', 'Creature — Griffin').
card_types('armored griffin', ['Creature']).
card_subtypes('armored griffin', ['Griffin']).
card_colors('armored griffin', ['W']).
card_text('armored griffin', 'Flying, vigilance').
card_mana_cost('armored griffin', ['3', 'W']).
card_cmc('armored griffin', 4).
card_layout('armored griffin', 'normal').
card_power('armored griffin', 2).
card_toughness('armored griffin', 3).

% Found in: INV
card_name('armored guardian', 'Armored Guardian').
card_type('armored guardian', 'Creature — Cat Soldier').
card_types('armored guardian', ['Creature']).
card_subtypes('armored guardian', ['Cat', 'Soldier']).
card_colors('armored guardian', ['W', 'U']).
card_text('armored guardian', '{1}{W}{W}: Target creature you control gains protection from the color of your choice until end of turn.\n{1}{U}{U}: Armored Guardian gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_mana_cost('armored guardian', ['3', 'W', 'U']).
card_cmc('armored guardian', 5).
card_layout('armored guardian', 'normal').
card_power('armored guardian', 2).
card_toughness('armored guardian', 5).

% Found in: 6ED, ATH, BRB, POR, S00, TMP, TPR, pPOD
card_name('armored pegasus', 'Armored Pegasus').
card_type('armored pegasus', 'Creature — Pegasus').
card_types('armored pegasus', ['Creature']).
card_subtypes('armored pegasus', ['Pegasus']).
card_colors('armored pegasus', ['W']).
card_text('armored pegasus', 'Flying').
card_mana_cost('armored pegasus', ['1', 'W']).
card_cmc('armored pegasus', 2).
card_layout('armored pegasus', 'normal').
card_power('armored pegasus', 1).
card_toughness('armored pegasus', 2).

% Found in: ISD
card_name('armored skaab', 'Armored Skaab').
card_type('armored skaab', 'Creature — Zombie Warrior').
card_types('armored skaab', ['Creature']).
card_subtypes('armored skaab', ['Zombie', 'Warrior']).
card_colors('armored skaab', ['U']).
card_text('armored skaab', 'When Armored Skaab enters the battlefield, put the top four cards of your library into your graveyard.').
card_mana_cost('armored skaab', ['2', 'U']).
card_cmc('armored skaab', 3).
card_layout('armored skaab', 'normal').
card_power('armored skaab', 1).
card_toughness('armored skaab', 4).

% Found in: GTC
card_name('armored transport', 'Armored Transport').
card_type('armored transport', 'Artifact Creature — Construct').
card_types('armored transport', ['Artifact', 'Creature']).
card_subtypes('armored transport', ['Construct']).
card_colors('armored transport', []).
card_text('armored transport', 'Prevent all combat damage that would be dealt to Armored Transport by creatures blocking it.').
card_mana_cost('armored transport', ['3']).
card_cmc('armored transport', 3).
card_layout('armored transport', 'normal').
card_power('armored transport', 2).
card_toughness('armored transport', 1).

% Found in: M12
card_name('armored warhorse', 'Armored Warhorse').
card_type('armored warhorse', 'Creature — Horse').
card_types('armored warhorse', ['Creature']).
card_subtypes('armored warhorse', ['Horse']).
card_colors('armored warhorse', ['W']).
card_text('armored warhorse', '').
card_mana_cost('armored warhorse', ['W', 'W']).
card_cmc('armored warhorse', 2).
card_layout('armored warhorse', 'normal').
card_power('armored warhorse', 2).
card_toughness('armored warhorse', 3).

% Found in: DGM
card_name('armored wolf-rider', 'Armored Wolf-Rider').
card_type('armored wolf-rider', 'Creature — Elf Knight').
card_types('armored wolf-rider', ['Creature']).
card_subtypes('armored wolf-rider', ['Elf', 'Knight']).
card_colors('armored wolf-rider', ['W', 'G']).
card_text('armored wolf-rider', '').
card_mana_cost('armored wolf-rider', ['3', 'G', 'W']).
card_cmc('armored wolf-rider', 5).
card_layout('armored wolf-rider', 'normal').
card_power('armored wolf-rider', 4).
card_toughness('armored wolf-rider', 6).

% Found in: MIR
card_name('armorer guildmage', 'Armorer Guildmage').
card_type('armorer guildmage', 'Creature — Human Wizard').
card_types('armorer guildmage', ['Creature']).
card_subtypes('armorer guildmage', ['Human', 'Wizard']).
card_colors('armorer guildmage', ['R']).
card_text('armorer guildmage', '{B}, {T}: Target creature gets +1/+0 until end of turn.\n{G}, {T}: Target creature gets +0/+1 until end of turn.').
card_mana_cost('armorer guildmage', ['R']).
card_cmc('armorer guildmage', 1).
card_layout('armorer guildmage', 'normal').
card_power('armorer guildmage', 1).
card_toughness('armorer guildmage', 1).

% Found in: DDL, RTR
card_name('armory guard', 'Armory Guard').
card_type('armory guard', 'Creature — Giant Soldier').
card_types('armory guard', ['Creature']).
card_subtypes('armory guard', ['Giant', 'Soldier']).
card_colors('armory guard', ['W']).
card_text('armory guard', 'Armory Guard has vigilance as long as you control a Gate.').
card_mana_cost('armory guard', ['3', 'W']).
card_cmc('armory guard', 4).
card_layout('armory guard', 'normal').
card_power('armory guard', 2).
card_toughness('armory guard', 5).

% Found in: JOU
card_name('armory of iroas', 'Armory of Iroas').
card_type('armory of iroas', 'Artifact — Equipment').
card_types('armory of iroas', ['Artifact']).
card_subtypes('armory of iroas', ['Equipment']).
card_colors('armory of iroas', []).
card_text('armory of iroas', 'Whenever equipped creature attacks, put a +1/+1 counter on it.\nEquip {2}').
card_mana_cost('armory of iroas', ['2']).
card_cmc('armory of iroas', 2).
card_layout('armory of iroas', 'normal').

% Found in: M13, MMQ
card_name('arms dealer', 'Arms Dealer').
card_type('arms dealer', 'Creature — Goblin Rogue').
card_types('arms dealer', ['Creature']).
card_subtypes('arms dealer', ['Goblin', 'Rogue']).
card_colors('arms dealer', ['R']).
card_text('arms dealer', '{1}{R}, Sacrifice a Goblin: Arms Dealer deals 4 damage to target creature.').
card_mana_cost('arms dealer', ['2', 'R']).
card_cmc('arms dealer', 3).
card_layout('arms dealer', 'normal').
card_power('arms dealer', 1).
card_toughness('arms dealer', 1).

% Found in: VIS
card_name('army ants', 'Army Ants').
card_type('army ants', 'Creature — Insect').
card_types('army ants', ['Creature']).
card_subtypes('army ants', ['Insect']).
card_colors('army ants', ['B', 'R']).
card_text('army ants', '{T}, Sacrifice a land: Destroy target land.').
card_mana_cost('army ants', ['1', 'B', 'R']).
card_cmc('army ants', 3).
card_layout('army ants', 'normal').
card_power('army ants', 1).
card_toughness('army ants', 1).

% Found in: ARN
card_name('army of allah', 'Army of Allah').
card_type('army of allah', 'Instant').
card_types('army of allah', ['Instant']).
card_subtypes('army of allah', []).
card_colors('army of allah', ['W']).
card_text('army of allah', 'Attacking creatures get +2/+0 until end of turn.').
card_mana_cost('army of allah', ['1', 'W', 'W']).
card_cmc('army of allah', 3).
card_layout('army of allah', 'normal').

% Found in: C13, ISD
card_name('army of the damned', 'Army of the Damned').
card_type('army of the damned', 'Sorcery').
card_types('army of the damned', ['Sorcery']).
card_subtypes('army of the damned', []).
card_colors('army of the damned', ['B']).
card_text('army of the damned', 'Put thirteen 2/2 black Zombie creature tokens onto the battlefield tapped.\nFlashback {7}{B}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('army of the damned', ['5', 'B', 'B', 'B']).
card_cmc('army of the damned', 8).
card_layout('army of the damned', 'normal').

% Found in: ICE
card_name('arnjlot\'s ascent', 'Arnjlot\'s Ascent').
card_type('arnjlot\'s ascent', 'Enchantment').
card_types('arnjlot\'s ascent', ['Enchantment']).
card_subtypes('arnjlot\'s ascent', []).
card_colors('arnjlot\'s ascent', ['U']).
card_text('arnjlot\'s ascent', 'Cumulative upkeep {U} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\n{1}: Target creature gains flying until end of turn.').
card_mana_cost('arnjlot\'s ascent', ['1', 'U', 'U']).
card_cmc('arnjlot\'s ascent', 3).
card_layout('arnjlot\'s ascent', 'normal').

% Found in: MM2, MMQ, MRD, RTR, SOM, pMEI
card_name('arrest', 'Arrest').
card_type('arrest', 'Enchantment — Aura').
card_types('arrest', ['Enchantment']).
card_subtypes('arrest', ['Aura']).
card_colors('arrest', ['W']).
card_text('arrest', 'Enchant creature\nEnchanted creature can\'t attack or block, and its activated abilities can\'t be activated.').
card_mana_cost('arrest', ['2', 'W']).
card_cmc('arrest', 3).
card_layout('arrest', 'normal').

% Found in: ROE
card_name('arrogant bloodlord', 'Arrogant Bloodlord').
card_type('arrogant bloodlord', 'Creature — Vampire Knight').
card_types('arrogant bloodlord', ['Creature']).
card_subtypes('arrogant bloodlord', ['Vampire', 'Knight']).
card_colors('arrogant bloodlord', ['B']).
card_text('arrogant bloodlord', 'Whenever Arrogant Bloodlord blocks or becomes blocked by a creature with power 1 or less, destroy Arrogant Bloodlord at end of combat.').
card_mana_cost('arrogant bloodlord', ['1', 'B', 'B']).
card_cmc('arrogant bloodlord', 3).
card_layout('arrogant bloodlord', 'normal').
card_power('arrogant bloodlord', 4).
card_toughness('arrogant bloodlord', 4).

% Found in: POR
card_name('arrogant vampire', 'Arrogant Vampire').
card_type('arrogant vampire', 'Creature — Vampire').
card_types('arrogant vampire', ['Creature']).
card_subtypes('arrogant vampire', ['Vampire']).
card_colors('arrogant vampire', ['B']).
card_text('arrogant vampire', 'Flying').
card_mana_cost('arrogant vampire', ['3', 'B', 'B']).
card_cmc('arrogant vampire', 5).
card_layout('arrogant vampire', 'normal').
card_power('arrogant vampire', 4).
card_toughness('arrogant vampire', 3).

% Found in: TOR, VMA, pFNM
card_name('arrogant wurm', 'Arrogant Wurm').
card_type('arrogant wurm', 'Creature — Wurm').
card_types('arrogant wurm', ['Creature']).
card_subtypes('arrogant wurm', ['Wurm']).
card_colors('arrogant wurm', ['G']).
card_text('arrogant wurm', 'Trample\nMadness {2}{G} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('arrogant wurm', ['3', 'G', 'G']).
card_cmc('arrogant wurm', 5).
card_layout('arrogant wurm', 'normal').
card_power('arrogant wurm', 4).
card_toughness('arrogant wurm', 4).

% Found in: KTK
card_name('arrow storm', 'Arrow Storm').
card_type('arrow storm', 'Sorcery').
card_types('arrow storm', ['Sorcery']).
card_subtypes('arrow storm', []).
card_colors('arrow storm', ['R']).
card_text('arrow storm', 'Arrow Storm deals 4 damage to target creature or player.\nRaid — If you attacked with a creature this turn, instead Arrow Storm deals 5 damage to that creature or player and the damage can\'t be prevented.').
card_mana_cost('arrow storm', ['3', 'R', 'R']).
card_cmc('arrow storm', 5).
card_layout('arrow storm', 'normal').

% Found in: DDN, ZEN
card_name('arrow volley trap', 'Arrow Volley Trap').
card_type('arrow volley trap', 'Instant — Trap').
card_types('arrow volley trap', ['Instant']).
card_subtypes('arrow volley trap', ['Trap']).
card_colors('arrow volley trap', ['W']).
card_text('arrow volley trap', 'If four or more creatures are attacking, you may pay {1}{W} rather than pay Arrow Volley Trap\'s mana cost.\nArrow Volley Trap deals 5 damage divided as you choose among any number of target attacking creatures.').
card_mana_cost('arrow volley trap', ['3', 'W', 'W']).
card_cmc('arrow volley trap', 5).
card_layout('arrow volley trap', 'normal').

% Found in: GTC
card_name('arrows of justice', 'Arrows of Justice').
card_type('arrows of justice', 'Instant').
card_types('arrows of justice', ['Instant']).
card_subtypes('arrows of justice', []).
card_colors('arrows of justice', ['W', 'R']).
card_text('arrows of justice', 'Arrows of Justice deals 4 damage to target attacking or blocking creature.').
card_mana_cost('arrows of justice', ['2', 'R/W']).
card_cmc('arrows of justice', 3).
card_layout('arrows of justice', 'normal').

% Found in: ARB, HOP
card_name('arsenal thresher', 'Arsenal Thresher').
card_type('arsenal thresher', 'Artifact Creature — Construct').
card_types('arsenal thresher', ['Artifact', 'Creature']).
card_subtypes('arsenal thresher', ['Construct']).
card_colors('arsenal thresher', ['W', 'U', 'B']).
card_text('arsenal thresher', 'As Arsenal Thresher enters the battlefield, you may reveal any number of other artifact cards from your hand. Arsenal Thresher enters the battlefield with a +1/+1 counter on it for each card revealed this way.').
card_mana_cost('arsenal thresher', ['2', 'W/B', 'U']).
card_cmc('arsenal thresher', 4).
card_layout('arsenal thresher', 'normal').
card_power('arsenal thresher', 2).
card_toughness('arsenal thresher', 2).

% Found in: DKA
card_name('artful dodge', 'Artful Dodge').
card_type('artful dodge', 'Sorcery').
card_types('artful dodge', ['Sorcery']).
card_subtypes('artful dodge', []).
card_colors('artful dodge', ['U']).
card_text('artful dodge', 'Target creature can\'t be blocked this turn.\nFlashback {U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('artful dodge', ['U']).
card_cmc('artful dodge', 1).
card_layout('artful dodge', 'normal').

% Found in: UNH
card_name('artful looter', 'Artful Looter').
card_type('artful looter', 'Creature — Human Wizard').
card_types('artful looter', ['Creature']).
card_subtypes('artful looter', ['Human', 'Wizard']).
card_colors('artful looter', ['U']).
card_text('artful looter', '{T}: Draw a card, then discard a card.\nWhenever a permanent comes into play that shares an artist with another permanent you control, untap Artful Looter.').
card_mana_cost('artful looter', ['2', 'U']).
card_cmc('artful looter', 3).
card_layout('artful looter', 'normal').
card_power('artful looter', 1).
card_toughness('artful looter', 2).

% Found in: DTK
card_name('artful maneuver', 'Artful Maneuver').
card_type('artful maneuver', 'Instant').
card_types('artful maneuver', ['Instant']).
card_subtypes('artful maneuver', []).
card_colors('artful maneuver', ['W']).
card_text('artful maneuver', 'Target creature gets +2/+2 until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('artful maneuver', ['1', 'W']).
card_cmc('artful maneuver', 2).
card_layout('artful maneuver', 'normal').

% Found in: ATQ, ME4, MED
card_name('artifact blast', 'Artifact Blast').
card_type('artifact blast', 'Instant').
card_types('artifact blast', ['Instant']).
card_subtypes('artifact blast', []).
card_colors('artifact blast', ['R']).
card_text('artifact blast', 'Counter target artifact spell.').
card_mana_cost('artifact blast', ['R']).
card_cmc('artifact blast', 1).
card_layout('artifact blast', 'normal').

% Found in: INV
card_name('artifact mutation', 'Artifact Mutation').
card_type('artifact mutation', 'Instant').
card_types('artifact mutation', ['Instant']).
card_subtypes('artifact mutation', []).
card_colors('artifact mutation', ['R', 'G']).
card_text('artifact mutation', 'Destroy target artifact. It can\'t be regenerated. Put X 1/1 green Saproling creature tokens onto the battlefield, where X is that artifact\'s converted mana cost.').
card_mana_cost('artifact mutation', ['R', 'G']).
card_cmc('artifact mutation', 2).
card_layout('artifact mutation', 'normal').

% Found in: ATQ
card_name('artifact possession', 'Artifact Possession').
card_type('artifact possession', 'Enchantment — Aura').
card_types('artifact possession', ['Enchantment']).
card_subtypes('artifact possession', ['Aura']).
card_colors('artifact possession', ['B']).
card_text('artifact possession', 'Enchant artifact\nWhenever enchanted artifact becomes tapped or a player activates an ability of enchanted artifact without {T} in its activation cost, Artifact Possession deals 2 damage to that artifact\'s controller.').
card_mana_cost('artifact possession', ['2', 'B']).
card_cmc('artifact possession', 3).
card_layout('artifact possession', 'normal').

% Found in: ATQ
card_name('artifact ward', 'Artifact Ward').
card_type('artifact ward', 'Enchantment — Aura').
card_types('artifact ward', ['Enchantment']).
card_subtypes('artifact ward', ['Aura']).
card_colors('artifact ward', ['W']).
card_text('artifact ward', 'Enchant creature\nEnchanted creature can\'t be blocked by artifact creatures.\nPrevent all damage that would be dealt to enchanted creature by artifact sources.\nEnchanted creature can\'t be the target of abilities from artifact sources.').
card_mana_cost('artifact ward', ['W']).
card_cmc('artifact ward', 1).
card_layout('artifact ward', 'normal').

% Found in: ORI
card_name('artificer\'s epiphany', 'Artificer\'s Epiphany').
card_type('artificer\'s epiphany', 'Instant').
card_types('artificer\'s epiphany', ['Instant']).
card_subtypes('artificer\'s epiphany', []).
card_colors('artificer\'s epiphany', ['U']).
card_text('artificer\'s epiphany', 'Draw two cards. If you control no artifacts, discard a card.').
card_mana_cost('artificer\'s epiphany', ['2', 'U']).
card_cmc('artificer\'s epiphany', 3).
card_layout('artificer\'s epiphany', 'normal').

% Found in: M14
card_name('artificer\'s hex', 'Artificer\'s Hex').
card_type('artificer\'s hex', 'Enchantment — Aura').
card_types('artificer\'s hex', ['Enchantment']).
card_subtypes('artificer\'s hex', ['Aura']).
card_colors('artificer\'s hex', ['B']).
card_text('artificer\'s hex', 'Enchant Equipment\nAt the beginning of your upkeep, if enchanted Equipment is attached to a creature, destroy that creature.').
card_mana_cost('artificer\'s hex', ['B']).
card_cmc('artificer\'s hex', 1).
card_layout('artificer\'s hex', 'normal').

% Found in: 5DN
card_name('artificer\'s intuition', 'Artificer\'s Intuition').
card_type('artificer\'s intuition', 'Enchantment').
card_types('artificer\'s intuition', ['Enchantment']).
card_subtypes('artificer\'s intuition', []).
card_colors('artificer\'s intuition', ['U']).
card_text('artificer\'s intuition', '{U}, Discard an artifact card: Search your library for an artifact card with converted mana cost 1 or less, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('artificer\'s intuition', ['1', 'U']).
card_cmc('artificer\'s intuition', 2).
card_layout('artificer\'s intuition', 'normal').

% Found in: ONS
card_name('artificial evolution', 'Artificial Evolution').
card_type('artificial evolution', 'Instant').
card_types('artificial evolution', ['Instant']).
card_subtypes('artificial evolution', []).
card_colors('artificial evolution', ['U']).
card_text('artificial evolution', 'Change the text of target spell or permanent by replacing all instances of one creature type with another. The new creature type can\'t be Wall. (This effect lasts indefinitely.)').
card_mana_cost('artificial evolution', ['U']).
card_cmc('artificial evolution', 1).
card_layout('artificial evolution', 'normal').

% Found in: NPH
card_name('artillerize', 'Artillerize').
card_type('artillerize', 'Instant').
card_types('artillerize', ['Instant']).
card_subtypes('artillerize', []).
card_colors('artillerize', ['R']).
card_text('artillerize', 'As an additional cost to cast Artillerize, sacrifice an artifact or creature.\nArtillerize deals 5 damage to target creature or player.').
card_mana_cost('artillerize', ['3', 'R']).
card_cmc('artillerize', 4).
card_layout('artillerize', 'normal').

% Found in: THS
card_name('artisan of forms', 'Artisan of Forms').
card_type('artisan of forms', 'Creature — Human Wizard').
card_types('artisan of forms', ['Creature']).
card_subtypes('artisan of forms', ['Human', 'Wizard']).
card_colors('artisan of forms', ['U']).
card_text('artisan of forms', 'Heroic — Whenever you cast a spell that targets Artisan of Forms, you may have Artisan of Forms become a copy of target creature and gain this ability.').
card_mana_cost('artisan of forms', ['1', 'U']).
card_cmc('artisan of forms', 2).
card_layout('artisan of forms', 'normal').
card_power('artisan of forms', 1).
card_toughness('artisan of forms', 1).

% Found in: ARC, C14, CMD, DDP, MM2, ROE, pFNM
card_name('artisan of kozilek', 'Artisan of Kozilek').
card_type('artisan of kozilek', 'Creature — Eldrazi').
card_types('artisan of kozilek', ['Creature']).
card_subtypes('artisan of kozilek', ['Eldrazi']).
card_colors('artisan of kozilek', []).
card_text('artisan of kozilek', 'When you cast Artisan of Kozilek, you may return target creature card from your graveyard to the battlefield.\nAnnihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)').
card_mana_cost('artisan of kozilek', ['9']).
card_cmc('artisan of kozilek', 9).
card_layout('artisan of kozilek', 'normal').
card_power('artisan of kozilek', 10).
card_toughness('artisan of kozilek', 9).

% Found in: THS
card_name('artisan\'s sorrow', 'Artisan\'s Sorrow').
card_type('artisan\'s sorrow', 'Instant').
card_types('artisan\'s sorrow', ['Instant']).
card_subtypes('artisan\'s sorrow', []).
card_colors('artisan\'s sorrow', ['G']).
card_text('artisan\'s sorrow', 'Destroy target artifact or enchantment. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('artisan\'s sorrow', ['3', 'G']).
card_cmc('artisan\'s sorrow', 4).
card_layout('artisan\'s sorrow', 'normal').

% Found in: 10E, DPA, HOP, NMS
card_name('ascendant evincar', 'Ascendant Evincar').
card_type('ascendant evincar', 'Legendary Creature — Vampire').
card_types('ascendant evincar', ['Creature']).
card_subtypes('ascendant evincar', ['Vampire']).
card_supertypes('ascendant evincar', ['Legendary']).
card_colors('ascendant evincar', ['B']).
card_text('ascendant evincar', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nOther black creatures get +1/+1.\nNonblack creatures get -1/-1.').
card_mana_cost('ascendant evincar', ['4', 'B', 'B']).
card_cmc('ascendant evincar', 6).
card_layout('ascendant evincar', 'normal').
card_power('ascendant evincar', 3).
card_toughness('ascendant evincar', 3).

% Found in: DGM
card_name('ascended lawmage', 'Ascended Lawmage').
card_type('ascended lawmage', 'Creature — Vedalken Wizard').
card_types('ascended lawmage', ['Creature']).
card_subtypes('ascended lawmage', ['Vedalken', 'Wizard']).
card_colors('ascended lawmage', ['W', 'U']).
card_text('ascended lawmage', 'Flying, hexproof').
card_mana_cost('ascended lawmage', ['2', 'W', 'U']).
card_cmc('ascended lawmage', 4).
card_layout('ascended lawmage', 'normal').
card_power('ascended lawmage', 3).
card_toughness('ascended lawmage', 2).

% Found in: ONS
card_name('ascending aven', 'Ascending Aven').
card_type('ascending aven', 'Creature — Bird Soldier').
card_types('ascending aven', ['Creature']).
card_subtypes('ascending aven', ['Bird', 'Soldier']).
card_colors('ascending aven', ['U']).
card_text('ascending aven', 'Flying\nAscending Aven can block only creatures with flying.\nMorph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('ascending aven', ['2', 'U', 'U']).
card_cmc('ascending aven', 4).
card_layout('ascending aven', 'normal').
card_power('ascending aven', 3).
card_toughness('ascending aven', 2).

% Found in: SOM
card_name('asceticism', 'Asceticism').
card_type('asceticism', 'Enchantment').
card_types('asceticism', ['Enchantment']).
card_subtypes('asceticism', []).
card_colors('asceticism', ['G']).
card_text('asceticism', 'Creatures you control have hexproof. (They can\'t be the targets of spells or abilities your opponents control.)\n{1}{G}: Regenerate target creature.').
card_mana_cost('asceticism', ['3', 'G', 'G']).
card_cmc('asceticism', 5).
card_layout('asceticism', 'normal').

% Found in: RTR
card_name('ash zealot', 'Ash Zealot').
card_type('ash zealot', 'Creature — Human Warrior').
card_types('ash zealot', ['Creature']).
card_subtypes('ash zealot', ['Human', 'Warrior']).
card_colors('ash zealot', ['R']).
card_text('ash zealot', 'First strike, haste\nWhenever a player casts a spell from a graveyard, Ash Zealot deals 3 damage to that player.').
card_mana_cost('ash zealot', ['R', 'R']).
card_cmc('ash zealot', 2).
card_layout('ash zealot', 'normal').
card_power('ash zealot', 2).
card_toughness('ash zealot', 2).

% Found in: CON
card_name('asha\'s favor', 'Asha\'s Favor').
card_type('asha\'s favor', 'Enchantment — Aura').
card_types('asha\'s favor', ['Enchantment']).
card_subtypes('asha\'s favor', ['Aura']).
card_colors('asha\'s favor', ['W']).
card_text('asha\'s favor', 'Enchant creature\nEnchanted creature has flying, first strike, and vigilance.').
card_mana_cost('asha\'s favor', ['2', 'W']).
card_cmc('asha\'s favor', 3).
card_layout('asha\'s favor', 'normal').

% Found in: KTK
card_name('ashcloud phoenix', 'Ashcloud Phoenix').
card_type('ashcloud phoenix', 'Creature — Phoenix').
card_types('ashcloud phoenix', ['Creature']).
card_subtypes('ashcloud phoenix', ['Phoenix']).
card_colors('ashcloud phoenix', ['R']).
card_text('ashcloud phoenix', 'Flying\nWhen Ashcloud Phoenix dies, return it to the battlefield face down under your control.\nMorph {4}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Ashcloud Phoenix is turned face up, it deals 2 damage to each player.').
card_mana_cost('ashcloud phoenix', ['2', 'R', 'R']).
card_cmc('ashcloud phoenix', 4).
card_layout('ashcloud phoenix', 'normal').
card_power('ashcloud phoenix', 4).
card_toughness('ashcloud phoenix', 1).

% Found in: TSP
card_name('ashcoat bear', 'Ashcoat Bear').
card_type('ashcoat bear', 'Creature — Bear').
card_types('ashcoat bear', ['Creature']).
card_subtypes('ashcoat bear', ['Bear']).
card_colors('ashcoat bear', ['G']).
card_text('ashcoat bear', 'Flash (You may cast this spell any time you could cast an instant.)').
card_mana_cost('ashcoat bear', ['1', 'G']).
card_cmc('ashcoat bear', 2).
card_layout('ashcoat bear', 'normal').
card_power('ashcoat bear', 2).
card_toughness('ashcoat bear', 2).

% Found in: ODY
card_name('ashen firebeast', 'Ashen Firebeast').
card_type('ashen firebeast', 'Creature — Elemental Beast').
card_types('ashen firebeast', ['Creature']).
card_subtypes('ashen firebeast', ['Elemental', 'Beast']).
card_colors('ashen firebeast', ['R']).
card_text('ashen firebeast', '{1}{R}: Ashen Firebeast deals 1 damage to each creature without flying.').
card_mana_cost('ashen firebeast', ['6', 'R', 'R']).
card_cmc('ashen firebeast', 8).
card_layout('ashen firebeast', 'normal').
card_power('ashen firebeast', 6).
card_toughness('ashen firebeast', 6).

% Found in: CST, ICE, ME2
card_name('ashen ghoul', 'Ashen Ghoul').
card_type('ashen ghoul', 'Creature — Zombie').
card_types('ashen ghoul', ['Creature']).
card_subtypes('ashen ghoul', ['Zombie']).
card_colors('ashen ghoul', ['B']).
card_text('ashen ghoul', 'Haste\n{B}: Return Ashen Ghoul from your graveyard to the battlefield. Activate this ability only during your upkeep and only if three or more creature cards are above Ashen Ghoul.').
card_mana_cost('ashen ghoul', ['3', 'B']).
card_cmc('ashen ghoul', 4).
card_layout('ashen ghoul', 'normal').
card_power('ashen ghoul', 3).
card_toughness('ashen ghoul', 1).

% Found in: BOK
card_name('ashen monstrosity', 'Ashen Monstrosity').
card_type('ashen monstrosity', 'Creature — Spirit').
card_types('ashen monstrosity', ['Creature']).
card_subtypes('ashen monstrosity', ['Spirit']).
card_colors('ashen monstrosity', ['R']).
card_text('ashen monstrosity', 'Haste\nAshen Monstrosity attacks each turn if able.').
card_mana_cost('ashen monstrosity', ['5', 'R', 'R']).
card_cmc('ashen monstrosity', 7).
card_layout('ashen monstrosity', 'normal').
card_power('ashen monstrosity', 7).
card_toughness('ashen monstrosity', 4).

% Found in: 6ED, MIR
card_name('ashen powder', 'Ashen Powder').
card_type('ashen powder', 'Sorcery').
card_types('ashen powder', ['Sorcery']).
card_subtypes('ashen powder', []).
card_colors('ashen powder', ['B']).
card_text('ashen powder', 'Put target creature card from an opponent\'s graveyard onto the battlefield under your control.').
card_mana_cost('ashen powder', ['2', 'B', 'B']).
card_cmc('ashen powder', 4).
card_layout('ashen powder', 'normal').

% Found in: THS
card_name('ashen rider', 'Ashen Rider').
card_type('ashen rider', 'Creature — Archon').
card_types('ashen rider', ['Creature']).
card_subtypes('ashen rider', ['Archon']).
card_colors('ashen rider', ['W', 'B']).
card_text('ashen rider', 'Flying\nWhen Ashen Rider enters the battlefield or dies, exile target permanent.').
card_mana_cost('ashen rider', ['4', 'W', 'W', 'B', 'B']).
card_cmc('ashen rider', 8).
card_layout('ashen rider', 'normal').
card_power('ashen rider', 5).
card_toughness('ashen rider', 5).

% Found in: CHK
card_name('ashen-skin zubera', 'Ashen-Skin Zubera').
card_type('ashen-skin zubera', 'Creature — Zubera Spirit').
card_types('ashen-skin zubera', ['Creature']).
card_subtypes('ashen-skin zubera', ['Zubera', 'Spirit']).
card_colors('ashen-skin zubera', ['B']).
card_text('ashen-skin zubera', 'When Ashen-Skin Zubera dies, target opponent discards a card for each Zubera that died this turn.').
card_mana_cost('ashen-skin zubera', ['1', 'B']).
card_cmc('ashen-skin zubera', 2).
card_layout('ashen-skin zubera', 'normal').
card_power('ashen-skin zubera', 1).
card_toughness('ashen-skin zubera', 2).

% Found in: SHM
card_name('ashenmoor cohort', 'Ashenmoor Cohort').
card_type('ashenmoor cohort', 'Creature — Elemental Warrior').
card_types('ashenmoor cohort', ['Creature']).
card_subtypes('ashenmoor cohort', ['Elemental', 'Warrior']).
card_colors('ashenmoor cohort', ['B']).
card_text('ashenmoor cohort', 'Ashenmoor Cohort gets +1/+1 as long as you control another black creature.').
card_mana_cost('ashenmoor cohort', ['5', 'B']).
card_cmc('ashenmoor cohort', 6).
card_layout('ashenmoor cohort', 'normal').
card_power('ashenmoor cohort', 4).
card_toughness('ashenmoor cohort', 3).

% Found in: MM2, SHM
card_name('ashenmoor gouger', 'Ashenmoor Gouger').
card_type('ashenmoor gouger', 'Creature — Elemental Warrior').
card_types('ashenmoor gouger', ['Creature']).
card_subtypes('ashenmoor gouger', ['Elemental', 'Warrior']).
card_colors('ashenmoor gouger', ['B', 'R']).
card_text('ashenmoor gouger', 'Ashenmoor Gouger can\'t block.').
card_mana_cost('ashenmoor gouger', ['B/R', 'B/R', 'B/R']).
card_cmc('ashenmoor gouger', 3).
card_layout('ashenmoor gouger', 'normal').
card_power('ashenmoor gouger', 4).
card_toughness('ashenmoor gouger', 4).

% Found in: SHM
card_name('ashenmoor liege', 'Ashenmoor Liege').
card_type('ashenmoor liege', 'Creature — Elemental Knight').
card_types('ashenmoor liege', ['Creature']).
card_subtypes('ashenmoor liege', ['Elemental', 'Knight']).
card_colors('ashenmoor liege', ['B', 'R']).
card_text('ashenmoor liege', 'Other black creatures you control get +1/+1.\nOther red creatures you control get +1/+1.\nWhenever Ashenmoor Liege becomes the target of a spell or ability an opponent controls, that player loses 4 life.').
card_mana_cost('ashenmoor liege', ['1', 'B/R', 'B/R', 'B/R']).
card_cmc('ashenmoor liege', 4).
card_layout('ashenmoor liege', 'normal').
card_power('ashenmoor liege', 4).
card_toughness('ashenmoor liege', 1).

% Found in: SOK
card_name('ashes of the fallen', 'Ashes of the Fallen').
card_type('ashes of the fallen', 'Artifact').
card_types('ashes of the fallen', ['Artifact']).
card_subtypes('ashes of the fallen', []).
card_colors('ashes of the fallen', []).
card_text('ashes of the fallen', 'As Ashes of the Fallen enters the battlefield, choose a creature type.\nEach creature card in your graveyard has the chosen creature type in addition to its other types.').
card_mana_cost('ashes of the fallen', ['2']).
card_cmc('ashes of the fallen', 2).
card_layout('ashes of the fallen', 'normal').

% Found in: 4ED, 5ED, DRK, ME3
card_name('ashes to ashes', 'Ashes to Ashes').
card_type('ashes to ashes', 'Sorcery').
card_types('ashes to ashes', ['Sorcery']).
card_subtypes('ashes to ashes', []).
card_colors('ashes to ashes', ['B']).
card_text('ashes to ashes', 'Exile two target nonartifact creatures. Ashes to Ashes deals 5 damage to you.').
card_mana_cost('ashes to ashes', ['1', 'B', 'B']).
card_cmc('ashes to ashes', 3).
card_layout('ashes to ashes', 'normal').

% Found in: BNG
card_name('ashiok\'s adept', 'Ashiok\'s Adept').
card_type('ashiok\'s adept', 'Creature — Human Wizard').
card_types('ashiok\'s adept', ['Creature']).
card_subtypes('ashiok\'s adept', ['Human', 'Wizard']).
card_colors('ashiok\'s adept', ['B']).
card_text('ashiok\'s adept', 'Heroic — Whenever you cast a spell that targets Ashiok\'s Adept, each opponent discards a card.').
card_mana_cost('ashiok\'s adept', ['2', 'B']).
card_cmc('ashiok\'s adept', 3).
card_layout('ashiok\'s adept', 'normal').
card_power('ashiok\'s adept', 1).
card_toughness('ashiok\'s adept', 3).

% Found in: THS
card_name('ashiok, nightmare weaver', 'Ashiok, Nightmare Weaver').
card_type('ashiok, nightmare weaver', 'Planeswalker — Ashiok').
card_types('ashiok, nightmare weaver', ['Planeswalker']).
card_subtypes('ashiok, nightmare weaver', ['Ashiok']).
card_colors('ashiok, nightmare weaver', ['U', 'B']).
card_text('ashiok, nightmare weaver', '+2: Exile the top three cards of target opponent\'s library.\n−X: Put a creature card with converted mana cost X exiled with Ashiok, Nightmare Weaver onto the battlefield under your control. That creature is a Nightmare in addition to its other types.\n−10: Exile all cards from all opponents\' hands and graveyards.').
card_mana_cost('ashiok, nightmare weaver', ['1', 'U', 'B']).
card_cmc('ashiok, nightmare weaver', 3).
card_layout('ashiok, nightmare weaver', 'normal').
card_loyalty('ashiok, nightmare weaver', 3).

% Found in: LRW
card_name('ashling the pilgrim', 'Ashling the Pilgrim').
card_type('ashling the pilgrim', 'Legendary Creature — Elemental Shaman').
card_types('ashling the pilgrim', ['Creature']).
card_subtypes('ashling the pilgrim', ['Elemental', 'Shaman']).
card_supertypes('ashling the pilgrim', ['Legendary']).
card_colors('ashling the pilgrim', ['R']).
card_text('ashling the pilgrim', '{1}{R}: Put a +1/+1 counter on Ashling the Pilgrim. If this is the third time this ability has resolved this turn, remove all +1/+1 counters from Ashling the Pilgrim, and it deals that much damage to each creature and each player.').
card_mana_cost('ashling the pilgrim', ['1', 'R']).
card_cmc('ashling the pilgrim', 2).
card_layout('ashling the pilgrim', 'normal').
card_power('ashling the pilgrim', 1).
card_toughness('ashling the pilgrim', 1).

% Found in: VAN
card_name('ashling the pilgrim avatar', 'Ashling the Pilgrim Avatar').
card_type('ashling the pilgrim avatar', 'Vanguard').
card_types('ashling the pilgrim avatar', ['Vanguard']).
card_subtypes('ashling the pilgrim avatar', []).
card_colors('ashling the pilgrim avatar', []).
card_text('ashling the pilgrim avatar', '{2}: Ashling the Pilgrim Avatar deals 1 damage to each creature and each player.').
card_layout('ashling the pilgrim avatar', 'vanguard').

% Found in: LRW
card_name('ashling\'s prerogative', 'Ashling\'s Prerogative').
card_type('ashling\'s prerogative', 'Enchantment').
card_types('ashling\'s prerogative', ['Enchantment']).
card_subtypes('ashling\'s prerogative', []).
card_colors('ashling\'s prerogative', ['R']).
card_text('ashling\'s prerogative', 'As Ashling\'s Prerogative enters the battlefield, choose odd or even. (Zero is even.)\nEach creature with converted mana cost of the chosen value has haste.\nEach creature without converted mana cost of the chosen value enters the battlefield tapped.').
card_mana_cost('ashling\'s prerogative', ['1', 'R']).
card_cmc('ashling\'s prerogative', 2).
card_layout('ashling\'s prerogative', 'normal').

% Found in: EVE
card_name('ashling, the extinguisher', 'Ashling, the Extinguisher').
card_type('ashling, the extinguisher', 'Legendary Creature — Elemental Shaman').
card_types('ashling, the extinguisher', ['Creature']).
card_subtypes('ashling, the extinguisher', ['Elemental', 'Shaman']).
card_supertypes('ashling, the extinguisher', ['Legendary']).
card_colors('ashling, the extinguisher', ['B']).
card_text('ashling, the extinguisher', 'Whenever Ashling, the Extinguisher deals combat damage to a player, choose target creature that player controls. He or she sacrifices that creature.').
card_mana_cost('ashling, the extinguisher', ['2', 'B', 'B']).
card_cmc('ashling, the extinguisher', 4).
card_layout('ashling, the extinguisher', 'normal').
card_power('ashling, the extinguisher', 4).
card_toughness('ashling, the extinguisher', 4).

% Found in: VAN
card_name('ashling, the extinguisher avatar', 'Ashling, the Extinguisher Avatar').
card_type('ashling, the extinguisher avatar', 'Vanguard').
card_types('ashling, the extinguisher avatar', ['Vanguard']).
card_subtypes('ashling, the extinguisher avatar', []).
card_colors('ashling, the extinguisher avatar', []).
card_text('ashling, the extinguisher avatar', '{5}: Destroy all nonland permanents. Activate this ability only once and only during your turn.').
card_layout('ashling, the extinguisher avatar', 'vanguard').

% Found in: DDK, ISD
card_name('ashmouth hound', 'Ashmouth Hound').
card_type('ashmouth hound', 'Creature — Elemental Hound').
card_types('ashmouth hound', ['Creature']).
card_subtypes('ashmouth hound', ['Elemental', 'Hound']).
card_colors('ashmouth hound', ['R']).
card_text('ashmouth hound', 'Whenever Ashmouth Hound blocks or becomes blocked by a creature, Ashmouth Hound deals 1 damage to that creature.').
card_mana_cost('ashmouth hound', ['1', 'R']).
card_cmc('ashmouth hound', 2).
card_layout('ashmouth hound', 'normal').
card_power('ashmouth hound', 2).
card_toughness('ashmouth hound', 1).

% Found in: VAN
card_name('ashnod', 'Ashnod').
card_type('ashnod', 'Vanguard').
card_types('ashnod', ['Vanguard']).
card_subtypes('ashnod', []).
card_colors('ashnod', []).
card_text('ashnod', 'Whenever a creature deals damage to you, destroy it.').
card_layout('ashnod', 'vanguard').

% Found in: 5ED, 6ED, ATQ, CHR, ME4
card_name('ashnod\'s altar', 'Ashnod\'s Altar').
card_type('ashnod\'s altar', 'Artifact').
card_types('ashnod\'s altar', ['Artifact']).
card_subtypes('ashnod\'s altar', []).
card_colors('ashnod\'s altar', []).
card_text('ashnod\'s altar', 'Sacrifice a creature: Add {2} to your mana pool.').
card_mana_cost('ashnod\'s altar', ['3']).
card_cmc('ashnod\'s altar', 3).
card_layout('ashnod\'s altar', 'normal').

% Found in: 4ED, ATQ
card_name('ashnod\'s battle gear', 'Ashnod\'s Battle Gear').
card_type('ashnod\'s battle gear', 'Artifact').
card_types('ashnod\'s battle gear', ['Artifact']).
card_subtypes('ashnod\'s battle gear', []).
card_colors('ashnod\'s battle gear', []).
card_text('ashnod\'s battle gear', 'You may choose not to untap Ashnod\'s Battle Gear during your untap step.\n{2}, {T}: Target creature you control gets +2/-2 for as long as Ashnod\'s Battle Gear remains tapped.').
card_mana_cost('ashnod\'s battle gear', ['2']).
card_cmc('ashnod\'s battle gear', 2).
card_layout('ashnod\'s battle gear', 'normal').

% Found in: UGL, pARL
card_name('ashnod\'s coupon', 'Ashnod\'s Coupon').
card_type('ashnod\'s coupon', 'Artifact').
card_types('ashnod\'s coupon', ['Artifact']).
card_subtypes('ashnod\'s coupon', []).
card_colors('ashnod\'s coupon', []).
card_text('ashnod\'s coupon', '{T}, Sacrifice Ashnod\'s Coupon: Target player gets you target drink.\nErrata: You pay any costs for the drink.').
card_mana_cost('ashnod\'s coupon', ['0']).
card_cmc('ashnod\'s coupon', 0).
card_layout('ashnod\'s coupon', 'normal').

% Found in: ALL, ME2
card_name('ashnod\'s cylix', 'Ashnod\'s Cylix').
card_type('ashnod\'s cylix', 'Artifact').
card_types('ashnod\'s cylix', ['Artifact']).
card_subtypes('ashnod\'s cylix', []).
card_colors('ashnod\'s cylix', []).
card_text('ashnod\'s cylix', '{3}, {T}: Target player looks at the top three cards of his or her library, puts one of them back on top of his or her library, then exiles the rest.').
card_mana_cost('ashnod\'s cylix', ['2']).
card_cmc('ashnod\'s cylix', 2).
card_layout('ashnod\'s cylix', 'normal').
card_reserved('ashnod\'s cylix').

% Found in: 5ED, ATQ, CHR, MED
card_name('ashnod\'s transmogrant', 'Ashnod\'s Transmogrant').
card_type('ashnod\'s transmogrant', 'Artifact').
card_types('ashnod\'s transmogrant', ['Artifact']).
card_subtypes('ashnod\'s transmogrant', []).
card_colors('ashnod\'s transmogrant', []).
card_text('ashnod\'s transmogrant', '{T}, Sacrifice Ashnod\'s Transmogrant: Put a +1/+1 counter on target nonartifact creature. That creature becomes an artifact in addition to its other types.').
card_mana_cost('ashnod\'s transmogrant', ['1']).
card_cmc('ashnod\'s transmogrant', 1).
card_layout('ashnod\'s transmogrant', 'normal').

% Found in: MIR
card_name('asmira, holy avenger', 'Asmira, Holy Avenger').
card_type('asmira, holy avenger', 'Legendary Creature — Human Cleric').
card_types('asmira, holy avenger', ['Creature']).
card_subtypes('asmira, holy avenger', ['Human', 'Cleric']).
card_supertypes('asmira, holy avenger', ['Legendary']).
card_colors('asmira, holy avenger', ['W', 'G']).
card_text('asmira, holy avenger', 'Flying\nAt the beginning of each end step, put a +1/+1 counter on Asmira, Holy Avenger for each creature put into your graveyard from the battlefield this turn.').
card_mana_cost('asmira, holy avenger', ['2', 'G', 'W']).
card_cmc('asmira, holy avenger', 4).
card_layout('asmira, holy avenger', 'normal').
card_power('asmira, holy avenger', 2).
card_toughness('asmira, holy avenger', 3).
card_reserved('asmira, holy avenger').

% Found in: JOU
card_name('aspect of gorgon', 'Aspect of Gorgon').
card_type('aspect of gorgon', 'Enchantment — Aura').
card_types('aspect of gorgon', ['Enchantment']).
card_subtypes('aspect of gorgon', ['Aura']).
card_colors('aspect of gorgon', ['B']).
card_text('aspect of gorgon', 'Enchant creature\nEnchanted creature gets +1/+3 and has deathtouch. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('aspect of gorgon', ['2', 'B']).
card_cmc('aspect of gorgon', 3).
card_layout('aspect of gorgon', 'normal').

% Found in: BNG
card_name('aspect of hydra', 'Aspect of Hydra').
card_type('aspect of hydra', 'Instant').
card_types('aspect of hydra', ['Instant']).
card_subtypes('aspect of hydra', []).
card_colors('aspect of hydra', ['G']).
card_text('aspect of hydra', 'Target creature gets +X/+X until end of turn, where X is your devotion to green. (Each {G} in the mana costs of permanents you control counts toward your devotion to green.)').
card_mana_cost('aspect of hydra', ['G']).
card_cmc('aspect of hydra', 1).
card_layout('aspect of hydra', 'normal').

% Found in: TSP
card_name('aspect of mongoose', 'Aspect of Mongoose').
card_type('aspect of mongoose', 'Enchantment — Aura').
card_types('aspect of mongoose', ['Enchantment']).
card_subtypes('aspect of mongoose', ['Aura']).
card_colors('aspect of mongoose', ['G']).
card_text('aspect of mongoose', 'Enchant creature\nEnchanted creature has shroud. (It can\'t be the target of spells or abilities.)\nWhen Aspect of Mongoose is put into a graveyard from the battlefield, return Aspect of Mongoose to its owner\'s hand.').
card_mana_cost('aspect of mongoose', ['1', 'G']).
card_cmc('aspect of mongoose', 2).
card_layout('aspect of mongoose', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB
card_name('aspect of wolf', 'Aspect of Wolf').
card_type('aspect of wolf', 'Enchantment — Aura').
card_types('aspect of wolf', ['Enchantment']).
card_subtypes('aspect of wolf', ['Aura']).
card_colors('aspect of wolf', ['G']).
card_text('aspect of wolf', 'Enchant creature\nEnchanted creature gets +X/+Y, where X is half the number of Forests you control, rounded down, and Y is half the number of Forests you control, rounded up.').
card_mana_cost('aspect of wolf', ['1', 'G']).
card_cmc('aspect of wolf', 2).
card_layout('aspect of wolf', 'normal').

% Found in: THS
card_name('asphodel wanderer', 'Asphodel Wanderer').
card_type('asphodel wanderer', 'Creature — Skeleton Soldier').
card_types('asphodel wanderer', ['Creature']).
card_subtypes('asphodel wanderer', ['Skeleton', 'Soldier']).
card_colors('asphodel wanderer', ['B']).
card_text('asphodel wanderer', '{2}{B}: Regenerate Asphodel Wanderer.').
card_mana_cost('asphodel wanderer', ['B']).
card_cmc('asphodel wanderer', 1).
card_layout('asphodel wanderer', 'normal').
card_power('asphodel wanderer', 1).
card_toughness('asphodel wanderer', 1).

% Found in: BNG
card_name('asphyxiate', 'Asphyxiate').
card_type('asphyxiate', 'Sorcery').
card_types('asphyxiate', ['Sorcery']).
card_subtypes('asphyxiate', []).
card_colors('asphyxiate', ['B']).
card_text('asphyxiate', 'Destroy target untapped creature.').
card_mana_cost('asphyxiate', ['1', 'B', 'B']).
card_cmc('asphyxiate', 3).
card_layout('asphyxiate', 'normal').

% Found in: ORI
card_name('aspiring aeronaut', 'Aspiring Aeronaut').
card_type('aspiring aeronaut', 'Creature — Human Artificer').
card_types('aspiring aeronaut', ['Creature']).
card_subtypes('aspiring aeronaut', ['Human', 'Artificer']).
card_colors('aspiring aeronaut', ['U']).
card_text('aspiring aeronaut', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Aspiring Aeronaut enters the battlefield, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield.').
card_mana_cost('aspiring aeronaut', ['3', 'U']).
card_cmc('aspiring aeronaut', 4).
card_layout('aspiring aeronaut', 'normal').
card_power('aspiring aeronaut', 1).
card_toughness('aspiring aeronaut', 2).

% Found in: UNH, pREL
card_name('ass whuppin\'', 'Ass Whuppin\'').
card_type('ass whuppin\'', 'Sorcery').
card_types('ass whuppin\'', ['Sorcery']).
card_subtypes('ass whuppin\'', []).
card_colors('ass whuppin\'', ['W', 'B']).
card_text('ass whuppin\'', 'Destroy target silver-bordered permanent in any game you can see from your seat.').
card_mana_cost('ass whuppin\'', ['1', 'W', 'B']).
card_cmc('ass whuppin\'', 3).
card_layout('ass whuppin\'', 'normal').

% Found in: POR
card_name('assassin\'s blade', 'Assassin\'s Blade').
card_type('assassin\'s blade', 'Instant').
card_types('assassin\'s blade', ['Instant']).
card_subtypes('assassin\'s blade', []).
card_colors('assassin\'s blade', ['B']).
card_text('assassin\'s blade', 'Cast Assassin\'s Blade only during the declare attackers step and only if you\'ve been attacked this step.\nDestroy target nonblack attacking creature.').
card_mana_cost('assassin\'s blade', ['1', 'B']).
card_cmc('assassin\'s blade', 2).
card_layout('assassin\'s blade', 'normal').

% Found in: RTR
card_name('assassin\'s strike', 'Assassin\'s Strike').
card_type('assassin\'s strike', 'Sorcery').
card_types('assassin\'s strike', ['Sorcery']).
card_subtypes('assassin\'s strike', []).
card_colors('assassin\'s strike', ['B']).
card_text('assassin\'s strike', 'Destroy target creature. Its controller discards a card.').
card_mana_cost('assassin\'s strike', ['4', 'B', 'B']).
card_cmc('assassin\'s strike', 6).
card_layout('assassin\'s strike', 'normal').

% Found in: 10E, CNS, M10, M11, PC2, TSP
card_name('assassinate', 'Assassinate').
card_type('assassinate', 'Sorcery').
card_types('assassinate', ['Sorcery']).
card_subtypes('assassinate', []).
card_colors('assassinate', ['B']).
card_text('assassinate', 'Destroy target tapped creature.').
card_mana_cost('assassinate', ['2', 'B']).
card_cmc('assassinate', 3).
card_layout('assassinate', 'normal').

% Found in: HOP, INV, TSB
card_name('assault', 'Assault').
card_type('assault', 'Sorcery').
card_types('assault', ['Sorcery']).
card_subtypes('assault', []).
card_colors('assault', ['R']).
card_text('assault', 'Assault deals 2 damage to target creature or player.').
card_mana_cost('assault', ['R']).
card_cmc('assault', 1).
card_layout('assault', 'split').
card_sides('assault', 'battery').

% Found in: DTK
card_name('assault formation', 'Assault Formation').
card_type('assault formation', 'Enchantment').
card_types('assault formation', ['Enchantment']).
card_subtypes('assault formation', []).
card_colors('assault formation', ['G']).
card_text('assault formation', 'Each creature you control assigns combat damage equal to its toughness rather than its power.\n{G}: Target creature with defender can attack this turn as though it didn\'t have defender.\n{2}{G}: Creatures you control get +0/+1 until end of turn.').
card_mana_cost('assault formation', ['1', 'G']).
card_cmc('assault formation', 2).
card_layout('assault formation', 'normal').

% Found in: GTC, M11, M12
card_name('assault griffin', 'Assault Griffin').
card_type('assault griffin', 'Creature — Griffin').
card_types('assault griffin', ['Creature']).
card_subtypes('assault griffin', ['Griffin']).
card_colors('assault griffin', ['W']).
card_text('assault griffin', 'Flying').
card_mana_cost('assault griffin', ['3', 'W']).
card_cmc('assault griffin', 4).
card_layout('assault griffin', 'normal').
card_power('assault griffin', 3).
card_toughness('assault griffin', 2).

% Found in: SOM
card_name('assault strobe', 'Assault Strobe').
card_type('assault strobe', 'Sorcery').
card_types('assault strobe', ['Sorcery']).
card_subtypes('assault strobe', []).
card_colors('assault strobe', ['R']).
card_text('assault strobe', 'Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)').
card_mana_cost('assault strobe', ['R']).
card_cmc('assault strobe', 1).
card_layout('assault strobe', 'normal').

% Found in: C14
card_name('assault suit', 'Assault Suit').
card_type('assault suit', 'Artifact — Equipment').
card_types('assault suit', ['Artifact']).
card_subtypes('assault suit', ['Equipment']).
card_colors('assault suit', []).
card_text('assault suit', 'Equipped creature gets +2/+2, has haste, can\'t attack you or a planeswalker you control, and can\'t be sacrificed.\nAt the beginning of each opponent\'s upkeep, you may have that player gain control of equipped creature until end of turn. If you do, untap it.\nEquip {3}').
card_mana_cost('assault suit', ['4']).
card_cmc('assault suit', 4).
card_layout('assault suit', 'normal').

% Found in: DIS
card_name('assault zeppelid', 'Assault Zeppelid').
card_type('assault zeppelid', 'Creature — Beast').
card_types('assault zeppelid', ['Creature']).
card_subtypes('assault zeppelid', ['Beast']).
card_colors('assault zeppelid', ['U', 'G']).
card_text('assault zeppelid', 'Flying, trample').
card_mana_cost('assault zeppelid', ['2', 'G', 'U']).
card_cmc('assault zeppelid', 4).
card_layout('assault zeppelid', 'normal').
card_power('assault zeppelid', 3).
card_toughness('assault zeppelid', 3).

% Found in: GTC
card_name('assemble the legion', 'Assemble the Legion').
card_type('assemble the legion', 'Enchantment').
card_types('assemble the legion', ['Enchantment']).
card_subtypes('assemble the legion', []).
card_colors('assemble the legion', ['W', 'R']).
card_text('assemble the legion', 'At the beginning of your upkeep, put a muster counter on Assemble the Legion. Then put a 1/1 red and white Soldier creature token with haste onto the battlefield for each muster counter on Assemble the Legion.').
card_mana_cost('assemble the legion', ['3', 'R', 'W']).
card_cmc('assemble the legion', 5).
card_layout('assemble the legion', 'normal').

% Found in: MMQ
card_name('assembly hall', 'Assembly Hall').
card_type('assembly hall', 'Artifact').
card_types('assembly hall', ['Artifact']).
card_subtypes('assembly hall', []).
card_colors('assembly hall', []).
card_text('assembly hall', '{4}, {T}: Reveal a creature card in your hand. Search your library for a card with the same name as that card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('assembly hall', ['5']).
card_cmc('assembly hall', 5).
card_layout('assembly hall', 'normal').

% Found in: DDF, TSP
card_name('assembly-worker', 'Assembly-Worker').
card_type('assembly-worker', 'Artifact Creature — Assembly-Worker').
card_types('assembly-worker', ['Artifact', 'Creature']).
card_subtypes('assembly-worker', ['Assembly-Worker']).
card_colors('assembly-worker', []).
card_text('assembly-worker', '{T}: Target Assembly-Worker creature gets +1/+1 until end of turn.').
card_mana_cost('assembly-worker', ['3']).
card_cmc('assembly-worker', 3).
card_layout('assembly-worker', 'normal').
card_power('assembly-worker', 2).
card_toughness('assembly-worker', 2).

% Found in: MRD
card_name('assert authority', 'Assert Authority').
card_type('assert authority', 'Instant').
card_types('assert authority', ['Instant']).
card_subtypes('assert authority', []).
card_colors('assert authority', ['U']).
card_text('assert authority', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nCounter target spell. If that spell is countered this way, exile it instead of putting it into its owner\'s graveyard.').
card_mana_cost('assert authority', ['5', 'U', 'U']).
card_cmc('assert authority', 7).
card_layout('assert authority', 'normal').

% Found in: UNH
card_name('assquatch', 'Assquatch').
card_type('assquatch', 'Creature — Donkey Lord').
card_types('assquatch', ['Creature']).
card_subtypes('assquatch', ['Donkey', 'Lord']).
card_colors('assquatch', ['R']).
card_text('assquatch', 'Each other Donkey gets +1½/+1½.\nWhenever another Donkey comes into play, untap target creature and gain control of it until end of turn. That creature gains haste until end of turn.').
card_mana_cost('assquatch', ['4', 'R']).
card_cmc('assquatch', 5).
card_layout('assquatch', 'normal').
card_power('assquatch', 3.5).
card_toughness('assquatch', 3.5).

% Found in: PC2
card_name('astral arena', 'Astral Arena').
card_type('astral arena', 'Plane — Kolbahan').
card_types('astral arena', ['Plane']).
card_subtypes('astral arena', ['Kolbahan']).
card_colors('astral arena', []).
card_text('astral arena', 'No more than one creature can attack each combat.\nNo more than one creature can block each combat.\nWhenever you roll {C}, Astral Arena deals 2 damage to each creature.').
card_layout('astral arena', 'plane').

% Found in: BNG
card_name('astral cornucopia', 'Astral Cornucopia').
card_type('astral cornucopia', 'Artifact').
card_types('astral cornucopia', ['Artifact']).
card_subtypes('astral cornucopia', []).
card_colors('astral cornucopia', []).
card_text('astral cornucopia', 'Astral Cornucopia enters the battlefield with X charge counters on it.\n{T}: Choose a color. Add one mana of that color to your mana pool for each charge counter on Astral Cornucopia.').
card_mana_cost('astral cornucopia', ['X', 'X', 'X']).
card_cmc('astral cornucopia', 0).
card_layout('astral cornucopia', 'normal').

% Found in: ONS, VMA, pFNM
card_name('astral slide', 'Astral Slide').
card_type('astral slide', 'Enchantment').
card_types('astral slide', ['Enchantment']).
card_subtypes('astral slide', []).
card_colors('astral slide', ['W']).
card_text('astral slide', 'Whenever a player cycles a card, you may exile target creature. If you do, return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('astral slide', ['2', 'W']).
card_cmc('astral slide', 3).
card_layout('astral slide', 'normal').

% Found in: SCG
card_name('astral steel', 'Astral Steel').
card_type('astral steel', 'Instant').
card_types('astral steel', ['Instant']).
card_subtypes('astral steel', []).
card_colors('astral steel', ['W']).
card_text('astral steel', 'Target creature gets +1/+2 until end of turn.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('astral steel', ['2', 'W']).
card_cmc('astral steel', 3).
card_layout('astral steel', 'normal').

% Found in: ALL, ME3
card_name('astrolabe', 'Astrolabe').
card_type('astrolabe', 'Artifact').
card_types('astrolabe', ['Artifact']).
card_subtypes('astrolabe', []).
card_colors('astrolabe', []).
card_text('astrolabe', '{1}, {T}, Sacrifice Astrolabe: Add two mana of any one color to your mana pool. Draw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('astrolabe', ['3']).
card_cmc('astrolabe', 3).
card_layout('astrolabe', 'normal').

% Found in: INV
card_name('atalya, samite master', 'Atalya, Samite Master').
card_type('atalya, samite master', 'Legendary Creature — Human Cleric').
card_types('atalya, samite master', ['Creature']).
card_subtypes('atalya, samite master', ['Human', 'Cleric']).
card_supertypes('atalya, samite master', ['Legendary']).
card_colors('atalya, samite master', ['W']).
card_text('atalya, samite master', '{X}, {T}: Choose one —\n• Prevent the next X damage that would be dealt to target creature this turn. Spend only white mana on X.\n• You gain X life. Spend only white mana on X.').
card_mana_cost('atalya, samite master', ['3', 'W', 'W']).
card_cmc('atalya, samite master', 5).
card_layout('atalya, samite master', 'normal').
card_power('atalya, samite master', 2).
card_toughness('atalya, samite master', 3).

% Found in: DTK
card_name('atarka beastbreaker', 'Atarka Beastbreaker').
card_type('atarka beastbreaker', 'Creature — Human Warrior').
card_types('atarka beastbreaker', ['Creature']).
card_subtypes('atarka beastbreaker', ['Human', 'Warrior']).
card_colors('atarka beastbreaker', ['G']).
card_text('atarka beastbreaker', 'Formidable — {4}{G}: Atarka Beastbreaker gets +4/+4 until end of turn. Activate this ability only if creatures you control have total power 8 or greater.').
card_mana_cost('atarka beastbreaker', ['1', 'G']).
card_cmc('atarka beastbreaker', 2).
card_layout('atarka beastbreaker', 'normal').
card_power('atarka beastbreaker', 2).
card_toughness('atarka beastbreaker', 2).

% Found in: DTK
card_name('atarka efreet', 'Atarka Efreet').
card_type('atarka efreet', 'Creature — Efreet Shaman').
card_types('atarka efreet', ['Creature']).
card_subtypes('atarka efreet', ['Efreet', 'Shaman']).
card_colors('atarka efreet', ['R']).
card_text('atarka efreet', 'Megamorph {2}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Atarka Efreet is turned face up, it deals 1 damage to target creature or player.').
card_mana_cost('atarka efreet', ['3', 'R']).
card_cmc('atarka efreet', 4).
card_layout('atarka efreet', 'normal').
card_power('atarka efreet', 5).
card_toughness('atarka efreet', 1).

% Found in: DTK
card_name('atarka monument', 'Atarka Monument').
card_type('atarka monument', 'Artifact').
card_types('atarka monument', ['Artifact']).
card_subtypes('atarka monument', []).
card_colors('atarka monument', []).
card_text('atarka monument', '{T}: Add {R} or {G} to your mana pool.\n{4}{R}{G}: Atarka Monument becomes a 4/4 red and green Dragon artifact creature with flying until end of turn.').
card_mana_cost('atarka monument', ['3']).
card_cmc('atarka monument', 3).
card_layout('atarka monument', 'normal').

% Found in: DTK
card_name('atarka pummeler', 'Atarka Pummeler').
card_type('atarka pummeler', 'Creature — Ogre Warrior').
card_types('atarka pummeler', ['Creature']).
card_subtypes('atarka pummeler', ['Ogre', 'Warrior']).
card_colors('atarka pummeler', ['R']).
card_text('atarka pummeler', 'Formidable — {3}{R}{R}: Creatures you control gain menace until end of turn. Activate this ability only if creatures you control have total power 8 or greater. (They can\'t be blocked except by two or more creatures.)').
card_mana_cost('atarka pummeler', ['4', 'R']).
card_cmc('atarka pummeler', 5).
card_layout('atarka pummeler', 'normal').
card_power('atarka pummeler', 4).
card_toughness('atarka pummeler', 5).

% Found in: DTK
card_name('atarka\'s command', 'Atarka\'s Command').
card_type('atarka\'s command', 'Instant').
card_types('atarka\'s command', ['Instant']).
card_subtypes('atarka\'s command', []).
card_colors('atarka\'s command', ['R', 'G']).
card_text('atarka\'s command', 'Choose two —\n• Your opponents can\'t gain life this turn.\n• Atarka\'s Command deals 3 damage to each opponent.\n• You may put a land card from your hand onto the battlefield.\n• Creatures you control get +1/+1 and gain reach until end of turn.').
card_mana_cost('atarka\'s command', ['R', 'G']).
card_cmc('atarka\'s command', 2).
card_layout('atarka\'s command', 'normal').

% Found in: FRF
card_name('atarka, world render', 'Atarka, World Render').
card_type('atarka, world render', 'Legendary Creature — Dragon').
card_types('atarka, world render', ['Creature']).
card_subtypes('atarka, world render', ['Dragon']).
card_supertypes('atarka, world render', ['Legendary']).
card_colors('atarka, world render', ['R', 'G']).
card_text('atarka, world render', 'Flying, trample\nWhenever a Dragon you control attacks, it gains double strike until end of turn.').
card_mana_cost('atarka, world render', ['5', 'R', 'G']).
card_cmc('atarka, world render', 7).
card_layout('atarka, world render', 'normal').
card_power('atarka, world render', 6).
card_toughness('atarka, world render', 4).

% Found in: JOU
card_name('athreos, god of passage', 'Athreos, God of Passage').
card_type('athreos, god of passage', 'Legendary Enchantment Creature — God').
card_types('athreos, god of passage', ['Enchantment', 'Creature']).
card_subtypes('athreos, god of passage', ['God']).
card_supertypes('athreos, god of passage', ['Legendary']).
card_colors('athreos, god of passage', ['W', 'B']).
card_text('athreos, god of passage', 'Indestructible\nAs long as your devotion to white and black is less than seven, Athreos isn\'t a creature.\nWhenever another creature you own dies, return it to your hand unless target opponent pays 3 life.').
card_mana_cost('athreos, god of passage', ['1', 'W', 'B']).
card_cmc('athreos, god of passage', 3).
card_layout('athreos, god of passage', 'normal').
card_power('athreos, god of passage', 5).
card_toughness('athreos, god of passage', 4).

% Found in: UNH
card_name('atinlay igpay', 'Atinlay Igpay').
card_type('atinlay igpay', 'Eaturecray — Igpay').
card_types('atinlay igpay', ['Eaturecray']).
card_subtypes('atinlay igpay', ['Igpay']).
card_colors('atinlay igpay', ['W']).
card_text('atinlay igpay', 'Oubleday ikestray\nEneverwhay Atinlay Igpay\'s ontrollercay eaksspay ay onnay-Igpay-Atinlay ordway, acrificesay Atinlay Igpay.').
card_mana_cost('atinlay igpay', ['5', 'W']).
card_cmc('atinlay igpay', 6).
card_layout('atinlay igpay', 'normal').
card_power('atinlay igpay', 3).
card_toughness('atinlay igpay', 3).

% Found in: 3ED, 5ED, ATQ, ME4, MRD
card_name('atog', 'Atog').
card_type('atog', 'Creature — Atog').
card_types('atog', ['Creature']).
card_subtypes('atog', ['Atog']).
card_colors('atog', ['R']).
card_text('atog', 'Sacrifice an artifact: Atog gets +2/+2 until end of turn.').
card_mana_cost('atog', ['1', 'R']).
card_cmc('atog', 2).
card_layout('atog', 'normal').
card_power('atog', 1).
card_toughness('atog', 2).

% Found in: ODY
card_name('atogatog', 'Atogatog').
card_type('atogatog', 'Legendary Creature — Atog').
card_types('atogatog', ['Creature']).
card_subtypes('atogatog', ['Atog']).
card_supertypes('atogatog', ['Legendary']).
card_colors('atogatog', ['W', 'U', 'B', 'R', 'G']).
card_text('atogatog', 'Sacrifice an Atog creature: Atogatog gets +X/+X until end of turn, where X is the sacrificed creature\'s power.').
card_mana_cost('atogatog', ['W', 'U', 'B', 'R', 'G']).
card_cmc('atogatog', 5).
card_layout('atogatog', 'normal').
card_power('atogatog', 5).
card_toughness('atogatog', 5).

% Found in: M13
card_name('attended knight', 'Attended Knight').
card_type('attended knight', 'Creature — Human Knight').
card_types('attended knight', ['Creature']).
card_subtypes('attended knight', ['Human', 'Knight']).
card_colors('attended knight', ['W']).
card_text('attended knight', 'First strike (This creature deals combat damage before creatures without first strike.)\nWhen Attended Knight enters the battlefield, put a 1/1 white Soldier creature token onto the battlefield.').
card_mana_cost('attended knight', ['2', 'W']).
card_cmc('attended knight', 3).
card_layout('attended knight', 'normal').
card_power('attended knight', 2).
card_toughness('attended knight', 2).

% Found in: CMD, UDS
card_name('attrition', 'Attrition').
card_type('attrition', 'Enchantment').
card_types('attrition', ['Enchantment']).
card_subtypes('attrition', []).
card_colors('attrition', ['B']).
card_text('attrition', '{B}, Sacrifice a creature: Destroy target nonblack creature.').
card_mana_cost('attrition', ['1', 'B', 'B']).
card_cmc('attrition', 3).
card_layout('attrition', 'normal').

% Found in: USG
card_name('attunement', 'Attunement').
card_type('attunement', 'Enchantment').
card_types('attunement', ['Enchantment']).
card_subtypes('attunement', []).
card_colors('attunement', ['U']).
card_text('attunement', 'Return Attunement to its owner\'s hand: Draw three cards, then discard four cards.').
card_mana_cost('attunement', ['2', 'U']).
card_cmc('attunement', 3).
card_layout('attunement', 'normal').

% Found in: RTR
card_name('auger spree', 'Auger Spree').
card_type('auger spree', 'Instant').
card_types('auger spree', ['Instant']).
card_subtypes('auger spree', []).
card_colors('auger spree', ['B', 'R']).
card_text('auger spree', 'Target creature gets +4/-4 until end of turn.').
card_mana_cost('auger spree', ['1', 'B', 'R']).
card_cmc('auger spree', 3).
card_layout('auger spree', 'normal').

% Found in: FUT
card_name('augur il-vec', 'Augur il-Vec').
card_type('augur il-vec', 'Creature — Human Cleric').
card_types('augur il-vec', ['Creature']).
card_subtypes('augur il-vec', ['Human', 'Cleric']).
card_colors('augur il-vec', ['W']).
card_text('augur il-vec', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nSacrifice Augur il-Vec: You gain 4 life. Activate this ability only during your upkeep.').
card_mana_cost('augur il-vec', ['1', 'W']).
card_cmc('augur il-vec', 2).
card_layout('augur il-vec', 'normal').
card_power('augur il-vec', 1).
card_toughness('augur il-vec', 3).

% Found in: C13, M13
card_name('augur of bolas', 'Augur of Bolas').
card_type('augur of bolas', 'Creature — Merfolk Wizard').
card_types('augur of bolas', ['Creature']).
card_subtypes('augur of bolas', ['Merfolk', 'Wizard']).
card_colors('augur of bolas', ['U']).
card_text('augur of bolas', 'When Augur of Bolas enters the battlefield, look at the top three cards of your library. You may reveal an instant or sorcery card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('augur of bolas', ['1', 'U']).
card_cmc('augur of bolas', 2).
card_layout('augur of bolas', 'normal').
card_power('augur of bolas', 1).
card_toughness('augur of bolas', 3).

% Found in: FUT
card_name('augur of skulls', 'Augur of Skulls').
card_type('augur of skulls', 'Creature — Skeleton Wizard').
card_types('augur of skulls', ['Creature']).
card_subtypes('augur of skulls', ['Skeleton', 'Wizard']).
card_colors('augur of skulls', ['B']).
card_text('augur of skulls', '{1}{B}: Regenerate Augur of Skulls.\nSacrifice Augur of Skulls: Target player discards two cards. Activate this ability only during your upkeep.').
card_mana_cost('augur of skulls', ['1', 'B']).
card_cmc('augur of skulls', 2).
card_layout('augur of skulls', 'normal').
card_power('augur of skulls', 1).
card_toughness('augur of skulls', 1).

% Found in: C13, SHM
card_name('augury adept', 'Augury Adept').
card_type('augury adept', 'Creature — Kithkin Wizard').
card_types('augury adept', ['Creature']).
card_subtypes('augury adept', ['Kithkin', 'Wizard']).
card_colors('augury adept', ['W', 'U']).
card_text('augury adept', 'Whenever Augury Adept deals combat damage to a player, reveal the top card of your library and put that card into your hand. You gain life equal to its converted mana cost.').
card_mana_cost('augury adept', ['1', 'W/U', 'W/U']).
card_cmc('augury adept', 3).
card_layout('augury adept', 'normal').
card_power('augury adept', 2).
card_toughness('augury adept', 2).

% Found in: DDI, M11, PC2
card_name('augury owl', 'Augury Owl').
card_type('augury owl', 'Creature — Bird').
card_types('augury owl', ['Creature']).
card_subtypes('augury owl', ['Bird']).
card_colors('augury owl', ['U']).
card_text('augury owl', 'Flying\nWhen Augury Owl enters the battlefield, scry 3. (Look at the top three cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('augury owl', ['1', 'U']).
card_cmc('augury owl', 2).
card_layout('augury owl', 'normal').
card_power('augury owl', 1).
card_toughness('augury owl', 1).

% Found in: LRW
card_name('auntie\'s hovel', 'Auntie\'s Hovel').
card_type('auntie\'s hovel', 'Land').
card_types('auntie\'s hovel', ['Land']).
card_subtypes('auntie\'s hovel', []).
card_colors('auntie\'s hovel', []).
card_text('auntie\'s hovel', 'As Auntie\'s Hovel enters the battlefield, you may reveal a Goblin card from your hand. If you don\'t, Auntie\'s Hovel enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('auntie\'s hovel', 'normal').

% Found in: MMA, MOR
card_name('auntie\'s snitch', 'Auntie\'s Snitch').
card_type('auntie\'s snitch', 'Creature — Goblin Rogue').
card_types('auntie\'s snitch', ['Creature']).
card_subtypes('auntie\'s snitch', ['Goblin', 'Rogue']).
card_colors('auntie\'s snitch', ['B']).
card_text('auntie\'s snitch', 'Auntie\'s Snitch can\'t block.\nProwl {1}{B} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Goblin or Rogue.)\nWhenever a Goblin or Rogue you control deals combat damage to a player, if Auntie\'s Snitch is in your graveyard, you may return Auntie\'s Snitch to your hand.').
card_mana_cost('auntie\'s snitch', ['2', 'B']).
card_cmc('auntie\'s snitch', 3).
card_layout('auntie\'s snitch', 'normal').
card_power('auntie\'s snitch', 3).
card_toughness('auntie\'s snitch', 1).

% Found in: BOK
card_name('aura barbs', 'Aura Barbs').
card_type('aura barbs', 'Instant — Arcane').
card_types('aura barbs', ['Instant']).
card_subtypes('aura barbs', ['Arcane']).
card_colors('aura barbs', ['R']).
card_text('aura barbs', 'Each enchantment deals 2 damage to its controller, then each Aura attached to a creature deals 2 damage to the creature it\'s attached to.').
card_mana_cost('aura barbs', ['2', 'R']).
card_cmc('aura barbs', 3).
card_layout('aura barbs', 'normal').

% Found in: PLS
card_name('aura blast', 'Aura Blast').
card_type('aura blast', 'Instant').
card_types('aura blast', ['Instant']).
card_subtypes('aura blast', []).
card_colors('aura blast', ['W']).
card_text('aura blast', 'Destroy target enchantment.\nDraw a card.').
card_mana_cost('aura blast', ['1', 'W']).
card_cmc('aura blast', 2).
card_layout('aura blast', 'normal').

% Found in: ONS
card_name('aura extraction', 'Aura Extraction').
card_type('aura extraction', 'Instant').
card_types('aura extraction', ['Instant']).
card_subtypes('aura extraction', []).
card_colors('aura extraction', ['W']).
card_text('aura extraction', 'Put target enchantment on top of its owner\'s library.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('aura extraction', ['1', 'W']).
card_cmc('aura extraction', 2).
card_layout('aura extraction', 'normal').

% Found in: ROE
card_name('aura finesse', 'Aura Finesse').
card_type('aura finesse', 'Instant').
card_types('aura finesse', ['Instant']).
card_subtypes('aura finesse', []).
card_colors('aura finesse', ['U']).
card_text('aura finesse', 'Attach target Aura you control to target creature.\nDraw a card.').
card_mana_cost('aura finesse', ['U']).
card_cmc('aura finesse', 1).
card_layout('aura finesse', 'normal').

% Found in: ULG
card_name('aura flux', 'Aura Flux').
card_type('aura flux', 'Enchantment').
card_types('aura flux', ['Enchantment']).
card_subtypes('aura flux', []).
card_colors('aura flux', ['U']).
card_text('aura flux', 'Other enchantments have \"At the beginning of your upkeep, sacrifice this enchantment unless you pay {2}.\"').
card_mana_cost('aura flux', ['2', 'U']).
card_cmc('aura flux', 3).
card_layout('aura flux', 'normal').

% Found in: PCY
card_name('aura fracture', 'Aura Fracture').
card_type('aura fracture', 'Enchantment').
card_types('aura fracture', ['Enchantment']).
card_subtypes('aura fracture', []).
card_colors('aura fracture', ['W']).
card_text('aura fracture', 'Sacrifice a land: Destroy target enchantment.').
card_mana_cost('aura fracture', ['2', 'W']).
card_cmc('aura fracture', 3).
card_layout('aura fracture', 'normal').

% Found in: PC2, ROE
card_name('aura gnarlid', 'Aura Gnarlid').
card_type('aura gnarlid', 'Creature — Beast').
card_types('aura gnarlid', ['Creature']).
card_subtypes('aura gnarlid', ['Beast']).
card_colors('aura gnarlid', ['G']).
card_text('aura gnarlid', 'Creatures with power less than Aura Gnarlid\'s power can\'t block it.\nAura Gnarlid gets +1/+1 for each Aura on the battlefield.').
card_mana_cost('aura gnarlid', ['2', 'G']).
card_cmc('aura gnarlid', 3).
card_layout('aura gnarlid', 'normal').
card_power('aura gnarlid', 2).
card_toughness('aura gnarlid', 2).

% Found in: 10E, ODY
card_name('aura graft', 'Aura Graft').
card_type('aura graft', 'Instant').
card_types('aura graft', ['Instant']).
card_subtypes('aura graft', []).
card_colors('aura graft', ['U']).
card_text('aura graft', 'Gain control of target Aura that\'s attached to a permanent. Attach it to another permanent it can enchant.').
card_mana_cost('aura graft', ['1', 'U']).
card_cmc('aura graft', 2).
card_layout('aura graft', 'normal').

% Found in: INV
card_name('aura mutation', 'Aura Mutation').
card_type('aura mutation', 'Instant').
card_types('aura mutation', ['Instant']).
card_subtypes('aura mutation', []).
card_colors('aura mutation', ['W', 'G']).
card_text('aura mutation', 'Destroy target enchantment. Put X 1/1 green Saproling creature tokens onto the battlefield, where X is that enchantment\'s converted mana cost.').
card_mana_cost('aura mutation', ['G', 'W']).
card_cmc('aura mutation', 2).
card_layout('aura mutation', 'normal').

% Found in: CHK
card_name('aura of dominion', 'Aura of Dominion').
card_type('aura of dominion', 'Enchantment — Aura').
card_types('aura of dominion', ['Enchantment']).
card_subtypes('aura of dominion', ['Aura']).
card_colors('aura of dominion', ['U']).
card_text('aura of dominion', 'Enchant creature\n{1}, Tap an untapped creature you control: Untap enchanted creature.').
card_mana_cost('aura of dominion', ['U', 'U']).
card_cmc('aura of dominion', 2).
card_layout('aura of dominion', 'normal').

% Found in: 10E, WTH, pFNM
card_name('aura of silence', 'Aura of Silence').
card_type('aura of silence', 'Enchantment').
card_types('aura of silence', ['Enchantment']).
card_subtypes('aura of silence', []).
card_colors('aura of silence', ['W']).
card_text('aura of silence', 'Artifact and enchantment spells your opponents cast cost {2} more to cast.\nSacrifice Aura of Silence: Destroy target artifact or enchantment.').
card_mana_cost('aura of silence', ['1', 'W', 'W']).
card_cmc('aura of silence', 3).
card_layout('aura of silence', 'normal').

% Found in: CMD, INV
card_name('aura shards', 'Aura Shards').
card_type('aura shards', 'Enchantment').
card_types('aura shards', ['Enchantment']).
card_subtypes('aura shards', []).
card_colors('aura shards', ['W', 'G']).
card_text('aura shards', 'Whenever a creature enters the battlefield under your control, you may destroy target artifact or enchantment.').
card_mana_cost('aura shards', ['1', 'G', 'W']).
card_cmc('aura shards', 3).
card_layout('aura shards', 'normal').

% Found in: UDS
card_name('aura thief', 'Aura Thief').
card_type('aura thief', 'Creature — Illusion').
card_types('aura thief', ['Creature']).
card_subtypes('aura thief', ['Illusion']).
card_colors('aura thief', ['U']).
card_text('aura thief', 'Flying\nWhen Aura Thief dies, you gain control of all enchantments. (You don\'t get to move Auras.)').
card_mana_cost('aura thief', ['3', 'U']).
card_cmc('aura thief', 4).
card_layout('aura thief', 'normal').
card_power('aura thief', 2).
card_toughness('aura thief', 2).

% Found in: DDL, M12, M14, ODY, ORI, PC2, pWPN
card_name('auramancer', 'Auramancer').
card_type('auramancer', 'Creature — Human Wizard').
card_types('auramancer', ['Creature']).
card_subtypes('auramancer', ['Human', 'Wizard']).
card_colors('auramancer', ['W']).
card_text('auramancer', 'When Auramancer enters the battlefield, you may return target enchantment card from your graveyard to your hand.').
card_mana_cost('auramancer', ['2', 'W']).
card_cmc('auramancer', 3).
card_layout('auramancer', 'normal').
card_power('auramancer', 2).
card_toughness('auramancer', 2).

% Found in: PLC
card_name('auramancer\'s guise', 'Auramancer\'s Guise').
card_type('auramancer\'s guise', 'Enchantment — Aura').
card_types('auramancer\'s guise', ['Enchantment']).
card_subtypes('auramancer\'s guise', ['Aura']).
card_colors('auramancer\'s guise', ['U']).
card_text('auramancer\'s guise', 'Enchant creature\nEnchanted creature gets +2/+2 for each Aura attached to it and has vigilance.').
card_mana_cost('auramancer\'s guise', ['2', 'U', 'U']).
card_cmc('auramancer\'s guise', 4).
card_layout('auramancer\'s guise', 'normal').

% Found in: TMP, TSB
card_name('auratog', 'Auratog').
card_type('auratog', 'Creature — Atog').
card_types('auratog', ['Creature']).
card_subtypes('auratog', ['Atog']).
card_colors('auratog', ['W']).
card_text('auratog', 'Sacrifice an enchantment: Auratog gets +2/+2 until end of turn.').
card_mana_cost('auratog', ['1', 'W']).
card_cmc('auratog', 2).
card_layout('auratog', 'normal').
card_power('auratog', 1).
card_toughness('auratog', 2).

% Found in: PC2, RAV
card_name('auratouched mage', 'Auratouched Mage').
card_type('auratouched mage', 'Creature — Human Wizard').
card_types('auratouched mage', ['Creature']).
card_subtypes('auratouched mage', ['Human', 'Wizard']).
card_colors('auratouched mage', ['W']).
card_text('auratouched mage', 'When Auratouched Mage enters the battlefield, search your library for an Aura card that could enchant it. If Auratouched Mage is still on the battlefield, put that Aura card onto the battlefield attached to it. Otherwise, reveal the Aura card and put it into your hand. Then shuffle your library.').
card_mana_cost('auratouched mage', ['5', 'W']).
card_cmc('auratouched mage', 6).
card_layout('auratouched mage', 'normal').
card_power('auratouched mage', 3).
card_toughness('auratouched mage', 3).

% Found in: GTC
card_name('aurelia\'s fury', 'Aurelia\'s Fury').
card_type('aurelia\'s fury', 'Instant').
card_types('aurelia\'s fury', ['Instant']).
card_subtypes('aurelia\'s fury', []).
card_colors('aurelia\'s fury', ['W', 'R']).
card_text('aurelia\'s fury', 'Aurelia\'s Fury deals X damage divided as you choose among any number of target creatures and/or players. Tap each creature dealt damage this way. Players dealt damage this way can\'t cast noncreature spells this turn.').
card_mana_cost('aurelia\'s fury', ['X', 'R', 'W']).
card_cmc('aurelia\'s fury', 2).
card_layout('aurelia\'s fury', 'normal').

% Found in: GTC, V15
card_name('aurelia, the warleader', 'Aurelia, the Warleader').
card_type('aurelia, the warleader', 'Legendary Creature — Angel').
card_types('aurelia, the warleader', ['Creature']).
card_subtypes('aurelia, the warleader', ['Angel']).
card_supertypes('aurelia, the warleader', ['Legendary']).
card_colors('aurelia, the warleader', ['W', 'R']).
card_text('aurelia, the warleader', 'Flying, vigilance, haste\nWhenever Aurelia, the Warleader attacks for the first time each turn, untap all creatures you control. After this phase, there is an additional combat phase.').
card_mana_cost('aurelia, the warleader', ['2', 'R', 'R', 'W', 'W']).
card_cmc('aurelia, the warleader', 6).
card_layout('aurelia, the warleader', 'normal').
card_power('aurelia, the warleader', 3).
card_toughness('aurelia, the warleader', 4).

% Found in: ONS
card_name('aurification', 'Aurification').
card_type('aurification', 'Enchantment').
card_types('aurification', ['Enchantment']).
card_subtypes('aurification', []).
card_colors('aurification', ['W']).
card_text('aurification', 'Whenever a creature deals damage to you, put a gold counter on it.\nEach creature with a gold counter on it is a Wall in addition to its other creature types and has defender. (Those creatures can\'t attack.)\nWhen Aurification leaves the battlefield, remove all gold counters from all creatures.').
card_mana_cost('aurification', ['2', 'W', 'W']).
card_cmc('aurification', 4).
card_layout('aurification', 'normal').

% Found in: MRD
card_name('auriok bladewarden', 'Auriok Bladewarden').
card_type('auriok bladewarden', 'Creature — Human Soldier').
card_types('auriok bladewarden', ['Creature']).
card_subtypes('auriok bladewarden', ['Human', 'Soldier']).
card_colors('auriok bladewarden', ['W']).
card_text('auriok bladewarden', '{T}: Target creature gets +X/+X until end of turn, where X is Auriok Bladewarden\'s power.').
card_mana_cost('auriok bladewarden', ['1', 'W']).
card_cmc('auriok bladewarden', 2).
card_layout('auriok bladewarden', 'normal').
card_power('auriok bladewarden', 1).
card_toughness('auriok bladewarden', 1).

% Found in: 5DN
card_name('auriok champion', 'Auriok Champion').
card_type('auriok champion', 'Creature — Human Cleric').
card_types('auriok champion', ['Creature']).
card_subtypes('auriok champion', ['Human', 'Cleric']).
card_colors('auriok champion', ['W']).
card_text('auriok champion', 'Protection from black and from red\nWhenever another creature enters the battlefield, you may gain 1 life.').
card_mana_cost('auriok champion', ['W', 'W']).
card_cmc('auriok champion', 2).
card_layout('auriok champion', 'normal').
card_power('auriok champion', 1).
card_toughness('auriok champion', 1).

% Found in: SOM
card_name('auriok edgewright', 'Auriok Edgewright').
card_type('auriok edgewright', 'Creature — Human Soldier').
card_types('auriok edgewright', ['Creature']).
card_subtypes('auriok edgewright', ['Human', 'Soldier']).
card_colors('auriok edgewright', ['W']).
card_text('auriok edgewright', 'Metalcraft — Auriok Edgewright has double strike as long as you control three or more artifacts.').
card_mana_cost('auriok edgewright', ['W', 'W']).
card_cmc('auriok edgewright', 2).
card_layout('auriok edgewright', 'normal').
card_power('auriok edgewright', 2).
card_toughness('auriok edgewright', 2).

% Found in: DST
card_name('auriok glaivemaster', 'Auriok Glaivemaster').
card_type('auriok glaivemaster', 'Creature — Human Soldier').
card_types('auriok glaivemaster', ['Creature']).
card_subtypes('auriok glaivemaster', ['Human', 'Soldier']).
card_colors('auriok glaivemaster', ['W']).
card_text('auriok glaivemaster', 'As long as Auriok Glaivemaster is equipped, it gets +1/+1 and has first strike.').
card_mana_cost('auriok glaivemaster', ['W']).
card_cmc('auriok glaivemaster', 1).
card_layout('auriok glaivemaster', 'normal').
card_power('auriok glaivemaster', 1).
card_toughness('auriok glaivemaster', 1).

% Found in: SOM
card_name('auriok replica', 'Auriok Replica').
card_type('auriok replica', 'Artifact Creature — Cleric').
card_types('auriok replica', ['Artifact', 'Creature']).
card_subtypes('auriok replica', ['Cleric']).
card_colors('auriok replica', []).
card_text('auriok replica', '{W}, Sacrifice Auriok Replica: Prevent all damage a source of your choice would deal to you this turn.').
card_mana_cost('auriok replica', ['3']).
card_cmc('auriok replica', 3).
card_layout('auriok replica', 'normal').
card_power('auriok replica', 2).
card_toughness('auriok replica', 2).

% Found in: 5DN, MMA
card_name('auriok salvagers', 'Auriok Salvagers').
card_type('auriok salvagers', 'Creature — Human Soldier').
card_types('auriok salvagers', ['Creature']).
card_subtypes('auriok salvagers', ['Human', 'Soldier']).
card_colors('auriok salvagers', ['W']).
card_text('auriok salvagers', '{1}{W}: Return target artifact card with converted mana cost 1 or less from your graveyard to your hand.').
card_mana_cost('auriok salvagers', ['3', 'W']).
card_cmc('auriok salvagers', 4).
card_layout('auriok salvagers', 'normal').
card_power('auriok salvagers', 2).
card_toughness('auriok salvagers', 4).

% Found in: DST
card_name('auriok siege sled', 'Auriok Siege Sled').
card_type('auriok siege sled', 'Artifact Creature — Juggernaut').
card_types('auriok siege sled', ['Artifact', 'Creature']).
card_subtypes('auriok siege sled', ['Juggernaut']).
card_colors('auriok siege sled', []).
card_text('auriok siege sled', '{1}: Target artifact creature blocks Auriok Siege Sled this turn if able.\n{1}: Target artifact creature can\'t block Auriok Siege Sled this turn.').
card_mana_cost('auriok siege sled', ['6']).
card_cmc('auriok siege sled', 6).
card_layout('auriok siege sled', 'normal').
card_power('auriok siege sled', 3).
card_toughness('auriok siege sled', 5).

% Found in: MRD
card_name('auriok steelshaper', 'Auriok Steelshaper').
card_type('auriok steelshaper', 'Creature — Human Soldier').
card_types('auriok steelshaper', ['Creature']).
card_subtypes('auriok steelshaper', ['Human', 'Soldier']).
card_colors('auriok steelshaper', ['W']).
card_text('auriok steelshaper', 'Equip costs you pay cost {1} less.\nAs long as Auriok Steelshaper is equipped, each creature you control that\'s a Soldier or a Knight gets +1/+1.').
card_mana_cost('auriok steelshaper', ['1', 'W']).
card_cmc('auriok steelshaper', 2).
card_layout('auriok steelshaper', 'normal').
card_power('auriok steelshaper', 1).
card_toughness('auriok steelshaper', 1).

% Found in: SOM
card_name('auriok sunchaser', 'Auriok Sunchaser').
card_type('auriok sunchaser', 'Creature — Human Soldier').
card_types('auriok sunchaser', ['Creature']).
card_subtypes('auriok sunchaser', ['Human', 'Soldier']).
card_colors('auriok sunchaser', ['W']).
card_text('auriok sunchaser', 'Metalcraft — As long as you control three or more artifacts, Auriok Sunchaser gets +2/+2 and has flying.').
card_mana_cost('auriok sunchaser', ['1', 'W']).
card_cmc('auriok sunchaser', 2).
card_layout('auriok sunchaser', 'normal').
card_power('auriok sunchaser', 1).
card_toughness('auriok sunchaser', 1).

% Found in: NPH
card_name('auriok survivors', 'Auriok Survivors').
card_type('auriok survivors', 'Creature — Human Soldier').
card_types('auriok survivors', ['Creature']).
card_subtypes('auriok survivors', ['Human', 'Soldier']).
card_colors('auriok survivors', ['W']).
card_text('auriok survivors', 'When Auriok Survivors enters the battlefield, you may return target Equipment card from your graveyard to the battlefield. If you do, you may attach it to Auriok Survivors.').
card_mana_cost('auriok survivors', ['5', 'W']).
card_cmc('auriok survivors', 6).
card_layout('auriok survivors', 'normal').
card_power('auriok survivors', 4).
card_toughness('auriok survivors', 6).

% Found in: MRD
card_name('auriok transfixer', 'Auriok Transfixer').
card_type('auriok transfixer', 'Creature — Human Scout').
card_types('auriok transfixer', ['Creature']).
card_subtypes('auriok transfixer', ['Human', 'Scout']).
card_colors('auriok transfixer', ['W']).
card_text('auriok transfixer', '{W}, {T}: Tap target artifact.').
card_mana_cost('auriok transfixer', ['W']).
card_cmc('auriok transfixer', 1).
card_layout('auriok transfixer', 'normal').
card_power('auriok transfixer', 1).
card_toughness('auriok transfixer', 1).

% Found in: 5DN
card_name('auriok windwalker', 'Auriok Windwalker').
card_type('auriok windwalker', 'Creature — Human Wizard').
card_types('auriok windwalker', ['Creature']).
card_subtypes('auriok windwalker', ['Human', 'Wizard']).
card_colors('auriok windwalker', ['W']).
card_text('auriok windwalker', 'Flying\n{T}: Attach target Equipment you control to target creature you control.').
card_mana_cost('auriok windwalker', ['3', 'W']).
card_cmc('auriok windwalker', 4).
card_layout('auriok windwalker', 'normal').
card_power('auriok windwalker', 2).
card_toughness('auriok windwalker', 3).

% Found in: 5ED, CST, ICE, ME2
card_name('aurochs', 'Aurochs').
card_type('aurochs', 'Creature — Aurochs').
card_types('aurochs', ['Creature']).
card_subtypes('aurochs', ['Aurochs']).
card_colors('aurochs', ['G']).
card_text('aurochs', 'Trample\nWhenever Aurochs attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.').
card_mana_cost('aurochs', ['3', 'G']).
card_cmc('aurochs', 4).
card_layout('aurochs', 'normal').
card_power('aurochs', 2).
card_toughness('aurochs', 3).

% Found in: CSP
card_name('aurochs herd', 'Aurochs Herd').
card_type('aurochs herd', 'Creature — Aurochs').
card_types('aurochs herd', ['Creature']).
card_subtypes('aurochs herd', ['Aurochs']).
card_colors('aurochs herd', ['G']).
card_text('aurochs herd', 'Trample\nWhen Aurochs Herd enters the battlefield, you may search your library for an Aurochs card, reveal it, and put it into your hand. If you do, shuffle your library.\nWhenever Aurochs Herd attacks, it gets +1/+0 until end of turn for each other attacking Aurochs.').
card_mana_cost('aurochs herd', ['5', 'G']).
card_cmc('aurochs herd', 6).
card_layout('aurochs herd', 'normal').
card_power('aurochs herd', 4).
card_toughness('aurochs herd', 4).

% Found in: DIS
card_name('aurora eidolon', 'Aurora Eidolon').
card_type('aurora eidolon', 'Creature — Spirit').
card_types('aurora eidolon', ['Creature']).
card_subtypes('aurora eidolon', ['Spirit']).
card_colors('aurora eidolon', ['W']).
card_text('aurora eidolon', '{W}, Sacrifice Aurora Eidolon: Prevent the next 3 damage that would be dealt to target creature or player this turn.\nWhenever you cast a multicolored spell, you may return Aurora Eidolon from your graveyard to your hand.').
card_mana_cost('aurora eidolon', ['3', 'W']).
card_cmc('aurora eidolon', 4).
card_layout('aurora eidolon', 'normal').
card_power('aurora eidolon', 2).
card_toughness('aurora eidolon', 2).

% Found in: PLS
card_name('aurora griffin', 'Aurora Griffin').
card_type('aurora griffin', 'Creature — Griffin').
card_types('aurora griffin', ['Creature']).
card_subtypes('aurora griffin', ['Griffin']).
card_colors('aurora griffin', ['W']).
card_text('aurora griffin', 'Flying\n{W}: Target permanent becomes white until end of turn.').
card_mana_cost('aurora griffin', ['3', 'W']).
card_cmc('aurora griffin', 4).
card_layout('aurora griffin', 'normal').
card_power('aurora griffin', 2).
card_toughness('aurora griffin', 2).

% Found in: MIR
card_name('auspicious ancestor', 'Auspicious Ancestor').
card_type('auspicious ancestor', 'Creature — Human Cleric').
card_types('auspicious ancestor', ['Creature']).
card_subtypes('auspicious ancestor', ['Human', 'Cleric']).
card_colors('auspicious ancestor', ['W']).
card_text('auspicious ancestor', 'When Auspicious Ancestor dies, you gain 3 life.\nWhenever a player casts a white spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('auspicious ancestor', ['3', 'W']).
card_cmc('auspicious ancestor', 4).
card_layout('auspicious ancestor', 'normal').
card_power('auspicious ancestor', 2).
card_toughness('auspicious ancestor', 3).
card_reserved('auspicious ancestor').

% Found in: CMD, LRW
card_name('austere command', 'Austere Command').
card_type('austere command', 'Sorcery').
card_types('austere command', ['Sorcery']).
card_subtypes('austere command', []).
card_colors('austere command', ['W']).
card_text('austere command', 'Choose two —\n• Destroy all artifacts.\n• Destroy all enchantments.\n• Destroy all creatures with converted mana cost 3 or less.\n• Destroy all creatures with converted mana cost 4 or greater.').
card_mana_cost('austere command', ['4', 'W', 'W']).
card_cmc('austere command', 6).
card_layout('austere command', 'normal').

% Found in: RAV
card_name('autochthon wurm', 'Autochthon Wurm').
card_type('autochthon wurm', 'Creature — Wurm').
card_types('autochthon wurm', ['Creature']).
card_subtypes('autochthon wurm', ['Wurm']).
card_colors('autochthon wurm', ['W', 'G']).
card_text('autochthon wurm', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTrample').
card_mana_cost('autochthon wurm', ['10', 'G', 'G', 'G', 'W', 'W']).
card_cmc('autochthon wurm', 15).
card_layout('autochthon wurm', 'normal').
card_power('autochthon wurm', 9).
card_toughness('autochthon wurm', 14).

% Found in: HML, MED
card_name('autumn willow', 'Autumn Willow').
card_type('autumn willow', 'Legendary Creature — Avatar').
card_types('autumn willow', ['Creature']).
card_subtypes('autumn willow', ['Avatar']).
card_supertypes('autumn willow', ['Legendary']).
card_colors('autumn willow', ['G']).
card_text('autumn willow', 'Shroud (This creature can\'t be the target of spells or abilities.)\n{G}: Until end of turn, Autumn Willow can be the target of spells and abilities controlled by target player as though it didn\'t have shroud.').
card_mana_cost('autumn willow', ['4', 'G', 'G']).
card_cmc('autumn willow', 6).
card_layout('autumn willow', 'normal').
card_power('autumn willow', 4).
card_toughness('autumn willow', 4).
card_reserved('autumn willow').

% Found in: M11, M12
card_name('autumn\'s veil', 'Autumn\'s Veil').
card_type('autumn\'s veil', 'Instant').
card_types('autumn\'s veil', ['Instant']).
card_subtypes('autumn\'s veil', []).
card_colors('autumn\'s veil', ['G']).
card_text('autumn\'s veil', 'Spells you control can\'t be countered by blue or black spells this turn, and creatures you control can\'t be the targets of blue or black spells this turn.').
card_mana_cost('autumn\'s veil', ['G']).
card_cmc('autumn\'s veil', 1).
card_layout('autumn\'s veil', 'normal').

% Found in: CHK
card_name('autumn-tail, kitsune sage', 'Autumn-Tail, Kitsune Sage').
card_type('autumn-tail, kitsune sage', 'Legendary Creature — Fox Wizard').
card_types('autumn-tail, kitsune sage', ['Creature']).
card_subtypes('autumn-tail, kitsune sage', ['Fox', 'Wizard']).
card_supertypes('autumn-tail, kitsune sage', ['Legendary']).
card_colors('autumn-tail, kitsune sage', ['W']).
card_text('autumn-tail, kitsune sage', '{1}: Attach target Aura attached to a creature to another creature.').
card_mana_cost('autumn-tail, kitsune sage', ['3', 'W']).
card_cmc('autumn-tail, kitsune sage', 4).
card_layout('autumn-tail, kitsune sage', 'flip').
card_power('autumn-tail, kitsune sage', 4).
card_toughness('autumn-tail, kitsune sage', 5).

% Found in: DKA
card_name('avacyn\'s collar', 'Avacyn\'s Collar').
card_type('avacyn\'s collar', 'Artifact — Equipment').
card_types('avacyn\'s collar', ['Artifact']).
card_subtypes('avacyn\'s collar', ['Equipment']).
card_colors('avacyn\'s collar', []).
card_text('avacyn\'s collar', 'Equipped creature gets +1/+0 and has vigilance.\nWhenever equipped creature dies, if it was a Human, put a 1/1 white Spirit creature token with flying onto the battlefield.\nEquip {2}').
card_mana_cost('avacyn\'s collar', ['1']).
card_cmc('avacyn\'s collar', 1).
card_layout('avacyn\'s collar', 'normal').

% Found in: ISD, pFNM
card_name('avacyn\'s pilgrim', 'Avacyn\'s Pilgrim').
card_type('avacyn\'s pilgrim', 'Creature — Human Monk').
card_types('avacyn\'s pilgrim', ['Creature']).
card_subtypes('avacyn\'s pilgrim', ['Human', 'Monk']).
card_colors('avacyn\'s pilgrim', ['G']).
card_text('avacyn\'s pilgrim', '{T}: Add {W} to your mana pool.').
card_mana_cost('avacyn\'s pilgrim', ['G']).
card_cmc('avacyn\'s pilgrim', 1).
card_layout('avacyn\'s pilgrim', 'normal').
card_power('avacyn\'s pilgrim', 1).
card_toughness('avacyn\'s pilgrim', 1).

% Found in: AVR, V15
card_name('avacyn, angel of hope', 'Avacyn, Angel of Hope').
card_type('avacyn, angel of hope', 'Legendary Creature — Angel').
card_types('avacyn, angel of hope', ['Creature']).
card_subtypes('avacyn, angel of hope', ['Angel']).
card_supertypes('avacyn, angel of hope', ['Legendary']).
card_colors('avacyn, angel of hope', ['W']).
card_text('avacyn, angel of hope', 'Flying, vigilance, indestructible\nOther permanents you control have indestructible.').
card_mana_cost('avacyn, angel of hope', ['5', 'W', 'W', 'W']).
card_cmc('avacyn, angel of hope', 8).
card_layout('avacyn, angel of hope', 'normal').
card_power('avacyn, angel of hope', 8).
card_toughness('avacyn, angel of hope', 8).

% Found in: M15
card_name('avacyn, guardian angel', 'Avacyn, Guardian Angel').
card_type('avacyn, guardian angel', 'Legendary Creature — Angel').
card_types('avacyn, guardian angel', ['Creature']).
card_subtypes('avacyn, guardian angel', ['Angel']).
card_supertypes('avacyn, guardian angel', ['Legendary']).
card_colors('avacyn, guardian angel', ['W']).
card_text('avacyn, guardian angel', 'Flying, vigilance\n{1}{W}: Prevent all damage that would be dealt to another target creature this turn by sources of the color of your choice.\n{5}{W}{W}: Prevent all damage that would be dealt to target player this turn by sources of the color of your choice.').
card_mana_cost('avacyn, guardian angel', ['2', 'W', 'W', 'W']).
card_cmc('avacyn, guardian angel', 5).
card_layout('avacyn, guardian angel', 'normal').
card_power('avacyn, guardian angel', 5).
card_toughness('avacyn, guardian angel', 4).

% Found in: ISD
card_name('avacynian priest', 'Avacynian Priest').
card_type('avacynian priest', 'Creature — Human Cleric').
card_types('avacynian priest', ['Creature']).
card_subtypes('avacynian priest', ['Human', 'Cleric']).
card_colors('avacynian priest', ['W']).
card_text('avacynian priest', '{1}, {T}: Tap target non-Human creature.').
card_mana_cost('avacynian priest', ['1', 'W']).
card_cmc('avacynian priest', 2).
card_layout('avacynian priest', 'normal').
card_power('avacynian priest', 1).
card_toughness('avacynian priest', 2).

% Found in: ICE
card_name('avalanche', 'Avalanche').
card_type('avalanche', 'Sorcery').
card_types('avalanche', ['Sorcery']).
card_subtypes('avalanche', []).
card_colors('avalanche', ['R']).
card_text('avalanche', 'Destroy X target snow lands.').
card_mana_cost('avalanche', ['X', '2', 'R', 'R']).
card_cmc('avalanche', 4).
card_layout('avalanche', 'normal').

% Found in: TSB, ULG, pFNM
card_name('avalanche riders', 'Avalanche Riders').
card_type('avalanche riders', 'Creature — Human Nomad').
card_types('avalanche riders', ['Creature']).
card_subtypes('avalanche riders', ['Human', 'Nomad']).
card_colors('avalanche riders', ['R']).
card_text('avalanche riders', 'Haste\nEcho {3}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Avalanche Riders enters the battlefield, destroy target land.').
card_mana_cost('avalanche riders', ['3', 'R']).
card_cmc('avalanche riders', 4).
card_layout('avalanche riders', 'normal').
card_power('avalanche riders', 2).
card_toughness('avalanche riders', 2).

% Found in: KTK, pMEI, pPRE
card_name('avalanche tusker', 'Avalanche Tusker').
card_type('avalanche tusker', 'Creature — Elephant Warrior').
card_types('avalanche tusker', ['Creature']).
card_subtypes('avalanche tusker', ['Elephant', 'Warrior']).
card_colors('avalanche tusker', ['U', 'R', 'G']).
card_text('avalanche tusker', 'Whenever Avalanche Tusker attacks, target creature defending player controls blocks it this combat if able.').
card_mana_cost('avalanche tusker', ['2', 'G', 'U', 'R']).
card_cmc('avalanche tusker', 5).
card_layout('avalanche tusker', 'normal').
card_power('avalanche tusker', 6).
card_toughness('avalanche tusker', 4).

% Found in: ONS
card_name('avarax', 'Avarax').
card_type('avarax', 'Creature — Beast').
card_types('avarax', ['Creature']).
card_subtypes('avarax', ['Beast']).
card_colors('avarax', ['R']).
card_text('avarax', 'Haste\nWhen Avarax enters the battlefield, you may search your library for a card named Avarax, reveal it, and put it into your hand. If you do, shuffle your library.\n{1}{R}: Avarax gets +1/+0 until end of turn.').
card_mana_cost('avarax', ['3', 'R', 'R']).
card_cmc('avarax', 5).
card_layout('avarax', 'normal').
card_power('avarax', 3).
card_toughness('avarax', 3).

% Found in: M15
card_name('avarice amulet', 'Avarice Amulet').
card_type('avarice amulet', 'Artifact — Equipment').
card_types('avarice amulet', ['Artifact']).
card_subtypes('avarice amulet', ['Equipment']).
card_colors('avarice amulet', []).
card_text('avarice amulet', 'Equipped creature gets +2/+0 and has vigilance and \"At the beginning of your upkeep, draw a card.\"\nWhen equipped creature dies, target opponent gains control of Avarice Amulet.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('avarice amulet', ['4']).
card_cmc('avarice amulet', 4).
card_layout('avarice amulet', 'normal').

% Found in: 5DN
card_name('avarice totem', 'Avarice Totem').
card_type('avarice totem', 'Artifact').
card_types('avarice totem', ['Artifact']).
card_subtypes('avarice totem', []).
card_colors('avarice totem', []).
card_text('avarice totem', '{5}: Exchange control of Avarice Totem and target nonland permanent.').
card_mana_cost('avarice totem', ['1']).
card_cmc('avarice totem', 1).
card_layout('avarice totem', 'normal').

% Found in: ORI
card_name('avaricious dragon', 'Avaricious Dragon').
card_type('avaricious dragon', 'Creature — Dragon').
card_types('avaricious dragon', ['Creature']).
card_subtypes('avaricious dragon', ['Dragon']).
card_colors('avaricious dragon', ['R']).
card_text('avaricious dragon', 'Flying\nAt the beginning of your draw step, draw an additional card.\nAt the beginning of your end step, discard your hand.').
card_mana_cost('avaricious dragon', ['2', 'R', 'R']).
card_cmc('avaricious dragon', 4).
card_layout('avaricious dragon', 'normal').
card_power('avaricious dragon', 4).
card_toughness('avaricious dragon', 4).

% Found in: ARC, DIS, pPRE
card_name('avatar of discord', 'Avatar of Discord').
card_type('avatar of discord', 'Creature — Avatar').
card_types('avatar of discord', ['Creature']).
card_subtypes('avatar of discord', ['Avatar']).
card_colors('avatar of discord', ['B', 'R']).
card_text('avatar of discord', '({B/R} can be paid with either {B} or {R}.)\nFlying\nWhen Avatar of Discord enters the battlefield, sacrifice it unless you discard two cards.').
card_mana_cost('avatar of discord', ['B/R', 'B/R', 'B/R']).
card_cmc('avatar of discord', 3).
card_layout('avatar of discord', 'normal').
card_power('avatar of discord', 5).
card_toughness('avatar of discord', 3).

% Found in: CMD, PCY
card_name('avatar of fury', 'Avatar of Fury').
card_type('avatar of fury', 'Creature — Avatar').
card_types('avatar of fury', ['Creature']).
card_subtypes('avatar of fury', ['Avatar']).
card_colors('avatar of fury', ['R']).
card_text('avatar of fury', 'If an opponent controls seven or more lands, Avatar of Fury costs {6} less to cast.\nFlying\n{R}: Avatar of Fury gets +1/+0 until end of turn.').
card_mana_cost('avatar of fury', ['6', 'R', 'R']).
card_cmc('avatar of fury', 8).
card_layout('avatar of fury', 'normal').
card_power('avatar of fury', 6).
card_toughness('avatar of fury', 6).

% Found in: 8ED, PCY, pPRE
card_name('avatar of hope', 'Avatar of Hope').
card_type('avatar of hope', 'Creature — Avatar').
card_types('avatar of hope', ['Creature']).
card_subtypes('avatar of hope', ['Avatar']).
card_colors('avatar of hope', ['W']).
card_text('avatar of hope', 'If you have 3 or less life, Avatar of Hope costs {6} less to cast.\nFlying\nAvatar of Hope can block any number of creatures.').
card_mana_cost('avatar of hope', ['6', 'W', 'W']).
card_cmc('avatar of hope', 8).
card_layout('avatar of hope', 'normal').
card_power('avatar of hope', 4).
card_toughness('avatar of hope', 9).

% Found in: UNH
card_name('avatar of me', 'Avatar of Me').
card_type('avatar of me', 'Creature — Avatar').
card_types('avatar of me', ['Creature']).
card_subtypes('avatar of me', ['Avatar']).
card_colors('avatar of me', ['U']).
card_text('avatar of me', 'Avatar of Me costs {1} more to play for each ten years you\'ve been alive.\nAvatar of Me\'s power is equal to your height in feet and its toughness is equal to your American shoe size. Round to the nearest ½.\nAvatar of Me\'s color is the color of your eyes.').
card_mana_cost('avatar of me', ['2', 'U', 'U']).
card_cmc('avatar of me', 4).
card_layout('avatar of me', 'normal').
card_power('avatar of me', '*').
card_toughness('avatar of me', '*').

% Found in: 10E, PCY
card_name('avatar of might', 'Avatar of Might').
card_type('avatar of might', 'Creature — Avatar').
card_types('avatar of might', ['Creature']).
card_subtypes('avatar of might', ['Avatar']).
card_colors('avatar of might', ['G']).
card_text('avatar of might', 'If an opponent controls at least four more creatures than you, Avatar of Might costs {6} less to cast.\nTrample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('avatar of might', ['6', 'G', 'G']).
card_cmc('avatar of might', 8).
card_layout('avatar of might', 'normal').
card_power('avatar of might', 8).
card_toughness('avatar of might', 8).

% Found in: CMD
card_name('avatar of slaughter', 'Avatar of Slaughter').
card_type('avatar of slaughter', 'Creature — Avatar').
card_types('avatar of slaughter', ['Creature']).
card_subtypes('avatar of slaughter', ['Avatar']).
card_colors('avatar of slaughter', ['R']).
card_text('avatar of slaughter', 'All creatures have double strike and attack each turn if able.').
card_mana_cost('avatar of slaughter', ['6', 'R', 'R']).
card_cmc('avatar of slaughter', 8).
card_layout('avatar of slaughter', 'normal').
card_power('avatar of slaughter', 8).
card_toughness('avatar of slaughter', 8).

% Found in: DTK
card_name('avatar of the resolute', 'Avatar of the Resolute').
card_type('avatar of the resolute', 'Creature — Avatar').
card_types('avatar of the resolute', ['Creature']).
card_subtypes('avatar of the resolute', ['Avatar']).
card_colors('avatar of the resolute', ['G']).
card_text('avatar of the resolute', 'Reach, trample\nAvatar of the Resolute enters the battlefield with a +1/+1 counter on it for each other creature you control with a +1/+1 counter on it.').
card_mana_cost('avatar of the resolute', ['G', 'G']).
card_cmc('avatar of the resolute', 2).
card_layout('avatar of the resolute', 'normal').
card_power('avatar of the resolute', 3).
card_toughness('avatar of the resolute', 2).

% Found in: PCY
card_name('avatar of will', 'Avatar of Will').
card_type('avatar of will', 'Creature — Avatar').
card_types('avatar of will', ['Creature']).
card_subtypes('avatar of will', ['Avatar']).
card_colors('avatar of will', ['U']).
card_text('avatar of will', 'If an opponent has no cards in hand, Avatar of Will costs {6} less to cast.\nFlying').
card_mana_cost('avatar of will', ['6', 'U', 'U']).
card_cmc('avatar of will', 8).
card_layout('avatar of will', 'normal').
card_power('avatar of will', 5).
card_toughness('avatar of will', 6).

% Found in: ARC, CMD, PCY, PD3, TSB, pPRO
card_name('avatar of woe', 'Avatar of Woe').
card_type('avatar of woe', 'Creature — Avatar').
card_types('avatar of woe', ['Creature']).
card_subtypes('avatar of woe', ['Avatar']).
card_colors('avatar of woe', ['B']).
card_text('avatar of woe', 'If there are ten or more creature cards total in all graveyards, Avatar of Woe costs {6} less to cast.\nFear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{T}: Destroy target creature. It can\'t be regenerated.').
card_mana_cost('avatar of woe', ['6', 'B', 'B']).
card_cmc('avatar of woe', 8).
card_layout('avatar of woe', 'normal').
card_power('avatar of woe', 6).
card_toughness('avatar of woe', 5).

% Found in: ODY
card_name('aven archer', 'Aven Archer').
card_type('aven archer', 'Creature — Bird Soldier Archer').
card_types('aven archer', ['Creature']).
card_subtypes('aven archer', ['Bird', 'Soldier', 'Archer']).
card_colors('aven archer', ['W']).
card_text('aven archer', 'Flying\n{2}{W}, {T}: Aven Archer deals 2 damage to target attacking or blocking creature.').
card_mana_cost('aven archer', ['3', 'W', 'W']).
card_cmc('aven archer', 5).
card_layout('aven archer', 'normal').
card_power('aven archer', 2).
card_toughness('aven archer', 2).

% Found in: FUT
card_name('aven augur', 'Aven Augur').
card_type('aven augur', 'Creature — Bird Wizard').
card_types('aven augur', ['Creature']).
card_subtypes('aven augur', ['Bird', 'Wizard']).
card_colors('aven augur', ['U']).
card_text('aven augur', 'Flying\nSacrifice Aven Augur: Return up to two target creatures to their owners\' hands. Activate this ability only during your upkeep.').
card_mana_cost('aven augur', ['3', 'U']).
card_cmc('aven augur', 4).
card_layout('aven augur', 'normal').
card_power('aven augur', 2).
card_toughness('aven augur', 2).

% Found in: ORI
card_name('aven battle priest', 'Aven Battle Priest').
card_type('aven battle priest', 'Creature — Bird Cleric').
card_types('aven battle priest', ['Creature']).
card_subtypes('aven battle priest', ['Bird', 'Cleric']).
card_colors('aven battle priest', ['W']).
card_text('aven battle priest', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Aven Battle Priest enters the battlefield, you gain 3 life.').
card_mana_cost('aven battle priest', ['5', 'W']).
card_cmc('aven battle priest', 6).
card_layout('aven battle priest', 'normal').
card_power('aven battle priest', 3).
card_toughness('aven battle priest', 3).

% Found in: ONS
card_name('aven brigadier', 'Aven Brigadier').
card_type('aven brigadier', 'Creature — Bird Soldier').
card_types('aven brigadier', ['Creature']).
card_subtypes('aven brigadier', ['Bird', 'Soldier']).
card_colors('aven brigadier', ['W']).
card_text('aven brigadier', 'Flying\nOther Bird creatures get +1/+1.\nOther Soldier creatures get +1/+1.').
card_mana_cost('aven brigadier', ['3', 'W', 'W', 'W']).
card_cmc('aven brigadier', 6).
card_layout('aven brigadier', 'normal').
card_power('aven brigadier', 3).
card_toughness('aven brigadier', 5).

% Found in: 10E, 8ED, 9ED, ODY
card_name('aven cloudchaser', 'Aven Cloudchaser').
card_type('aven cloudchaser', 'Creature — Bird Soldier').
card_types('aven cloudchaser', ['Creature']).
card_subtypes('aven cloudchaser', ['Bird', 'Soldier']).
card_colors('aven cloudchaser', ['W']).
card_text('aven cloudchaser', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Aven Cloudchaser enters the battlefield, destroy target enchantment.').
card_mana_cost('aven cloudchaser', ['3', 'W']).
card_cmc('aven cloudchaser', 4).
card_layout('aven cloudchaser', 'normal').
card_power('aven cloudchaser', 2).
card_toughness('aven cloudchaser', 2).

% Found in: LGN
card_name('aven envoy', 'Aven Envoy').
card_type('aven envoy', 'Creature — Bird Soldier').
card_types('aven envoy', ['Creature']).
card_subtypes('aven envoy', ['Bird', 'Soldier']).
card_colors('aven envoy', ['U']).
card_text('aven envoy', 'Flying').
card_mana_cost('aven envoy', ['U']).
card_cmc('aven envoy', 1).
card_layout('aven envoy', 'normal').
card_power('aven envoy', 0).
card_toughness('aven envoy', 2).

% Found in: SCG
card_name('aven farseer', 'Aven Farseer').
card_type('aven farseer', 'Creature — Bird Soldier').
card_types('aven farseer', ['Creature']).
card_subtypes('aven farseer', ['Bird', 'Soldier']).
card_colors('aven farseer', ['W']).
card_text('aven farseer', 'Flying\nWhenever a permanent is turned face up, put a +1/+1 counter on Aven Farseer.').
card_mana_cost('aven farseer', ['1', 'W']).
card_cmc('aven farseer', 2).
card_layout('aven farseer', 'normal').
card_power('aven farseer', 1).
card_toughness('aven farseer', 1).

% Found in: ONS
card_name('aven fateshaper', 'Aven Fateshaper').
card_type('aven fateshaper', 'Creature — Bird Wizard').
card_types('aven fateshaper', ['Creature']).
card_subtypes('aven fateshaper', ['Bird', 'Wizard']).
card_colors('aven fateshaper', ['U']).
card_text('aven fateshaper', 'Flying\nWhen Aven Fateshaper enters the battlefield, look at the top four cards of your library, then put them back in any order.\n{4}{U}: Look at the top four cards of your library, then put them back in any order.').
card_mana_cost('aven fateshaper', ['6', 'U']).
card_cmc('aven fateshaper', 7).
card_layout('aven fateshaper', 'normal').
card_power('aven fateshaper', 4).
card_toughness('aven fateshaper', 5).

% Found in: 10E, 8ED, 9ED, ODY
card_name('aven fisher', 'Aven Fisher').
card_type('aven fisher', 'Creature — Bird Soldier').
card_types('aven fisher', ['Creature']).
card_subtypes('aven fisher', ['Bird', 'Soldier']).
card_colors('aven fisher', ['U']).
card_text('aven fisher', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhen Aven Fisher dies, you may draw a card.').
card_mana_cost('aven fisher', ['3', 'U']).
card_cmc('aven fisher', 4).
card_layout('aven fisher', 'normal').
card_power('aven fisher', 2).
card_toughness('aven fisher', 2).

% Found in: M12
card_name('aven fleetwing', 'Aven Fleetwing').
card_type('aven fleetwing', 'Creature — Bird Soldier').
card_types('aven fleetwing', ['Creature']).
card_subtypes('aven fleetwing', ['Bird', 'Soldier']).
card_colors('aven fleetwing', ['U']).
card_text('aven fleetwing', 'Flying\nHexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('aven fleetwing', ['3', 'U']).
card_cmc('aven fleetwing', 4).
card_layout('aven fleetwing', 'normal').
card_power('aven fleetwing', 2).
card_toughness('aven fleetwing', 2).

% Found in: 8ED, 9ED, ODY
card_name('aven flock', 'Aven Flock').
card_type('aven flock', 'Creature — Bird Soldier').
card_types('aven flock', ['Creature']).
card_subtypes('aven flock', ['Bird', 'Soldier']).
card_colors('aven flock', ['W']).
card_text('aven flock', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{W}: Aven Flock gets +0/+1 until end of turn.').
card_mana_cost('aven flock', ['4', 'W']).
card_cmc('aven flock', 5).
card_layout('aven flock', 'normal').
card_power('aven flock', 2).
card_toughness('aven flock', 3).

% Found in: JUD
card_name('aven fogbringer', 'Aven Fogbringer').
card_type('aven fogbringer', 'Creature — Bird Wizard').
card_types('aven fogbringer', ['Creature']).
card_subtypes('aven fogbringer', ['Bird', 'Wizard']).
card_colors('aven fogbringer', ['U']).
card_text('aven fogbringer', 'Flying\nWhen Aven Fogbringer enters the battlefield, return target land to its owner\'s hand.').
card_mana_cost('aven fogbringer', ['3', 'U']).
card_cmc('aven fogbringer', 4).
card_layout('aven fogbringer', 'normal').
card_power('aven fogbringer', 2).
card_toughness('aven fogbringer', 1).

% Found in: SCG
card_name('aven liberator', 'Aven Liberator').
card_type('aven liberator', 'Creature — Bird Soldier').
card_types('aven liberator', ['Creature']).
card_subtypes('aven liberator', ['Bird', 'Soldier']).
card_colors('aven liberator', ['W']).
card_text('aven liberator', 'Flying\nMorph {3}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Aven Liberator is turned face up, target creature you control gains protection from the color of your choice until end of turn.').
card_mana_cost('aven liberator', ['2', 'W', 'W']).
card_cmc('aven liberator', 4).
card_layout('aven liberator', 'normal').
card_power('aven liberator', 2).
card_toughness('aven liberator', 3).

% Found in: ARB
card_name('aven mimeomancer', 'Aven Mimeomancer').
card_type('aven mimeomancer', 'Creature — Bird Wizard').
card_types('aven mimeomancer', ['Creature']).
card_subtypes('aven mimeomancer', ['Bird', 'Wizard']).
card_colors('aven mimeomancer', ['W', 'U']).
card_text('aven mimeomancer', 'Flying\nAt the beginning of your upkeep, you may put a feather counter on target creature. If you do, that creature has base power and toughness 3/1 and has flying for as long as it has a feather counter on it.').
card_mana_cost('aven mimeomancer', ['1', 'W', 'U']).
card_cmc('aven mimeomancer', 3).
card_layout('aven mimeomancer', 'normal').
card_power('aven mimeomancer', 3).
card_toughness('aven mimeomancer', 1).

% Found in: FUT
card_name('aven mindcensor', 'Aven Mindcensor').
card_type('aven mindcensor', 'Creature — Bird Wizard').
card_types('aven mindcensor', ['Creature']).
card_subtypes('aven mindcensor', ['Bird', 'Wizard']).
card_colors('aven mindcensor', ['W']).
card_text('aven mindcensor', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nIf an opponent would search a library, that player searches the top four cards of that library instead.').
card_mana_cost('aven mindcensor', ['2', 'W']).
card_cmc('aven mindcensor', 3).
card_layout('aven mindcensor', 'normal').
card_power('aven mindcensor', 2).
card_toughness('aven mindcensor', 1).

% Found in: LGN
card_name('aven redeemer', 'Aven Redeemer').
card_type('aven redeemer', 'Creature — Bird Cleric').
card_types('aven redeemer', ['Creature']).
card_subtypes('aven redeemer', ['Bird', 'Cleric']).
card_colors('aven redeemer', ['W']).
card_text('aven redeemer', 'Flying\n{T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('aven redeemer', ['3', 'W']).
card_cmc('aven redeemer', 4).
card_layout('aven redeemer', 'normal').
card_power('aven redeemer', 2).
card_toughness('aven redeemer', 2).

% Found in: PLC
card_name('aven riftwatcher', 'Aven Riftwatcher').
card_type('aven riftwatcher', 'Creature — Bird Rebel Soldier').
card_types('aven riftwatcher', ['Creature']).
card_subtypes('aven riftwatcher', ['Bird', 'Rebel', 'Soldier']).
card_colors('aven riftwatcher', ['W']).
card_text('aven riftwatcher', 'Flying\nVanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Aven Riftwatcher enters the battlefield or leaves the battlefield, you gain 2 life.').
card_mana_cost('aven riftwatcher', ['2', 'W']).
card_cmc('aven riftwatcher', 3).
card_layout('aven riftwatcher', 'normal').
card_power('aven riftwatcher', 2).
card_toughness('aven riftwatcher', 3).

% Found in: ODY
card_name('aven shrine', 'Aven Shrine').
card_type('aven shrine', 'Enchantment').
card_types('aven shrine', ['Enchantment']).
card_subtypes('aven shrine', []).
card_colors('aven shrine', ['W']).
card_text('aven shrine', 'Whenever a player casts a spell, that player gains X life, where X is the number of cards in all graveyards with the same name as that spell.').
card_mana_cost('aven shrine', ['1', 'W', 'W']).
card_cmc('aven shrine', 3).
card_layout('aven shrine', 'normal').

% Found in: FRF
card_name('aven skirmisher', 'Aven Skirmisher').
card_type('aven skirmisher', 'Creature — Bird Warrior').
card_types('aven skirmisher', ['Creature']).
card_subtypes('aven skirmisher', ['Bird', 'Warrior']).
card_colors('aven skirmisher', ['W']).
card_text('aven skirmisher', 'Flying').
card_mana_cost('aven skirmisher', ['W']).
card_cmc('aven skirmisher', 1).
card_layout('aven skirmisher', 'normal').
card_power('aven skirmisher', 1).
card_toughness('aven skirmisher', 1).

% Found in: ODY
card_name('aven smokeweaver', 'Aven Smokeweaver').
card_type('aven smokeweaver', 'Creature — Bird Soldier').
card_types('aven smokeweaver', ['Creature']).
card_subtypes('aven smokeweaver', ['Bird', 'Soldier']).
card_colors('aven smokeweaver', ['U']).
card_text('aven smokeweaver', 'Flying, protection from red').
card_mana_cost('aven smokeweaver', ['2', 'U', 'U']).
card_cmc('aven smokeweaver', 4).
card_layout('aven smokeweaver', 'normal').
card_power('aven smokeweaver', 2).
card_toughness('aven smokeweaver', 3).

% Found in: ONS
card_name('aven soulgazer', 'Aven Soulgazer').
card_type('aven soulgazer', 'Creature — Bird Cleric').
card_types('aven soulgazer', ['Creature']).
card_subtypes('aven soulgazer', ['Bird', 'Cleric']).
card_colors('aven soulgazer', ['W']).
card_text('aven soulgazer', 'Flying\n{2}{W}: Look at target face-down creature.').
card_mana_cost('aven soulgazer', ['3', 'W', 'W']).
card_cmc('aven soulgazer', 5).
card_layout('aven soulgazer', 'normal').
card_power('aven soulgazer', 3).
card_toughness('aven soulgazer', 3).

% Found in: CON, M13
card_name('aven squire', 'Aven Squire').
card_type('aven squire', 'Creature — Bird Soldier').
card_types('aven squire', ['Creature']).
card_subtypes('aven squire', ['Bird', 'Soldier']).
card_colors('aven squire', ['W']).
card_text('aven squire', 'Flying\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('aven squire', ['1', 'W']).
card_cmc('aven squire', 2).
card_layout('aven squire', 'normal').
card_power('aven squire', 1).
card_toughness('aven squire', 1).

% Found in: DTK
card_name('aven sunstriker', 'Aven Sunstriker').
card_type('aven sunstriker', 'Creature — Bird Warrior').
card_types('aven sunstriker', ['Creature']).
card_subtypes('aven sunstriker', ['Bird', 'Warrior']).
card_colors('aven sunstriker', ['W']).
card_text('aven sunstriker', 'Flying\nDouble strike (This creature deals both first-strike and regular combat damage.)\nMegamorph {4}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('aven sunstriker', ['1', 'W', 'W']).
card_cmc('aven sunstriker', 3).
card_layout('aven sunstriker', 'normal').
card_power('aven sunstriker', 1).
card_toughness('aven sunstriker', 1).

% Found in: FRF
card_name('aven surveyor', 'Aven Surveyor').
card_type('aven surveyor', 'Creature — Bird Scout').
card_types('aven surveyor', ['Creature']).
card_subtypes('aven surveyor', ['Bird', 'Scout']).
card_colors('aven surveyor', ['U']).
card_text('aven surveyor', 'Flying\nWhen Aven Surveyor enters the battlefield, choose one —\n• Put a +1/+1 counter on Aven Surveyor.\n• Return target creature to its owner\'s hand.').
card_mana_cost('aven surveyor', ['3', 'U', 'U']).
card_cmc('aven surveyor', 5).
card_layout('aven surveyor', 'normal').
card_power('aven surveyor', 2).
card_toughness('aven surveyor', 2).

% Found in: DTK
card_name('aven tactician', 'Aven Tactician').
card_type('aven tactician', 'Creature — Bird Soldier').
card_types('aven tactician', ['Creature']).
card_subtypes('aven tactician', ['Bird', 'Soldier']).
card_colors('aven tactician', ['W']).
card_text('aven tactician', 'Flying\nWhen Aven Tactician enters the battlefield, bolster 1. (Choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('aven tactician', ['4', 'W']).
card_cmc('aven tactician', 5).
card_layout('aven tactician', 'normal').
card_power('aven tactician', 2).
card_toughness('aven tactician', 3).

% Found in: CON
card_name('aven trailblazer', 'Aven Trailblazer').
card_type('aven trailblazer', 'Creature — Bird Soldier').
card_types('aven trailblazer', ['Creature']).
card_subtypes('aven trailblazer', ['Bird', 'Soldier']).
card_colors('aven trailblazer', ['W']).
card_text('aven trailblazer', 'Flying\nDomain — Aven Trailblazer\'s toughness is equal to the number of basic land types among lands you control.').
card_mana_cost('aven trailblazer', ['2', 'W']).
card_cmc('aven trailblazer', 3).
card_layout('aven trailblazer', 'normal').
card_power('aven trailblazer', 2).
card_toughness('aven trailblazer', '*').

% Found in: TOR
card_name('aven trooper', 'Aven Trooper').
card_type('aven trooper', 'Creature — Bird Soldier').
card_types('aven trooper', ['Creature']).
card_subtypes('aven trooper', ['Bird', 'Soldier']).
card_colors('aven trooper', ['W']).
card_text('aven trooper', 'Flying\n{2}{W}, Discard a card: Aven Trooper gets +1/+2 until end of turn.').
card_mana_cost('aven trooper', ['3', 'W']).
card_cmc('aven trooper', 4).
card_layout('aven trooper', 'normal').
card_power('aven trooper', 1).
card_toughness('aven trooper', 1).

% Found in: JUD
card_name('aven warcraft', 'Aven Warcraft').
card_type('aven warcraft', 'Instant').
card_types('aven warcraft', ['Instant']).
card_subtypes('aven warcraft', []).
card_colors('aven warcraft', ['W']).
card_text('aven warcraft', 'Creatures you control get +0/+2 until end of turn.\nThreshold — If seven or more cards are in your graveyard, choose a color. Creatures you control also gain protection from the chosen color until end of turn.').
card_mana_cost('aven warcraft', ['2', 'W']).
card_cmc('aven warcraft', 3).
card_layout('aven warcraft', 'normal').

% Found in: LGN
card_name('aven warhawk', 'Aven Warhawk').
card_type('aven warhawk', 'Creature — Bird Soldier').
card_types('aven warhawk', ['Creature']).
card_subtypes('aven warhawk', ['Bird', 'Soldier']).
card_colors('aven warhawk', ['W']).
card_text('aven warhawk', 'Amplify 1 (As this creature enters the battlefield, put a +1/+1 counter on it for each Bird and/or Soldier card you reveal in your hand.)\nFlying').
card_mana_cost('aven warhawk', ['4', 'W']).
card_cmc('aven warhawk', 5).
card_layout('aven warhawk', 'normal').
card_power('aven warhawk', 2).
card_toughness('aven warhawk', 2).

% Found in: 10E, 9ED, ODY
card_name('aven windreader', 'Aven Windreader').
card_type('aven windreader', 'Creature — Bird Soldier Wizard').
card_types('aven windreader', ['Creature']).
card_subtypes('aven windreader', ['Bird', 'Soldier', 'Wizard']).
card_colors('aven windreader', ['U']).
card_text('aven windreader', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\n{1}{U}: Target player reveals the top card of his or her library.').
card_mana_cost('aven windreader', ['3', 'U', 'U']).
card_cmc('aven windreader', 5).
card_layout('aven windreader', 'normal').
card_power('aven windreader', 3).
card_toughness('aven windreader', 3).

% Found in: NMS
card_name('avenger en-dal', 'Avenger en-Dal').
card_type('avenger en-dal', 'Creature — Human Spellshaper').
card_types('avenger en-dal', ['Creature']).
card_subtypes('avenger en-dal', ['Human', 'Spellshaper']).
card_colors('avenger en-dal', ['W']).
card_text('avenger en-dal', '{2}{W}, {T}, Discard a card: Exile target attacking creature. Its controller gains life equal to its toughness.').
card_mana_cost('avenger en-dal', ['1', 'W']).
card_cmc('avenger en-dal', 2).
card_layout('avenger en-dal', 'normal').
card_power('avenger en-dal', 1).
card_toughness('avenger en-dal', 1).

% Found in: C13, DDP, WWK
card_name('avenger of zendikar', 'Avenger of Zendikar').
card_type('avenger of zendikar', 'Creature — Elemental').
card_types('avenger of zendikar', ['Creature']).
card_subtypes('avenger of zendikar', ['Elemental']).
card_colors('avenger of zendikar', ['G']).
card_text('avenger of zendikar', 'When Avenger of Zendikar enters the battlefield, put a 0/1 green Plant creature token onto the battlefield for each land you control.\nLandfall — Whenever a land enters the battlefield under your control, you may put a +1/+1 counter on each Plant creature you control.').
card_mana_cost('avenger of zendikar', ['5', 'G', 'G']).
card_cmc('avenger of zendikar', 7).
card_layout('avenger of zendikar', 'normal').
card_power('avenger of zendikar', 5).
card_toughness('avenger of zendikar', 5).

% Found in: TMP, TPR
card_name('avenging angel', 'Avenging Angel').
card_type('avenging angel', 'Creature — Angel').
card_types('avenging angel', ['Creature']).
card_subtypes('avenging angel', ['Angel']).
card_colors('avenging angel', ['W']).
card_text('avenging angel', 'Flying\nWhen Avenging Angel dies, you may put it on top of its owner\'s library.').
card_mana_cost('avenging angel', ['3', 'W', 'W']).
card_cmc('avenging angel', 5).
card_layout('avenging angel', 'normal').
card_power('avenging angel', 3).
card_toughness('avenging angel', 3).
card_reserved('avenging angel').

% Found in: RTR
card_name('avenging arrow', 'Avenging Arrow').
card_type('avenging arrow', 'Instant').
card_types('avenging arrow', ['Instant']).
card_subtypes('avenging arrow', []).
card_colors('avenging arrow', ['W']).
card_text('avenging arrow', 'Destroy target creature that dealt damage this turn.').
card_mana_cost('avenging arrow', ['2', 'W']).
card_cmc('avenging arrow', 3).
card_layout('avenging arrow', 'normal').

% Found in: EXO
card_name('avenging druid', 'Avenging Druid').
card_type('avenging druid', 'Creature — Human Druid').
card_types('avenging druid', ['Creature']).
card_subtypes('avenging druid', ['Human', 'Druid']).
card_colors('avenging druid', ['G']).
card_text('avenging druid', 'Whenever Avenging Druid deals damage to an opponent, you may reveal cards from the top of your library until you reveal a land card. If you do, put that card onto the battlefield and put all other cards revealed this way into your graveyard.').
card_mana_cost('avenging druid', ['2', 'G']).
card_cmc('avenging druid', 3).
card_layout('avenging druid', 'normal').
card_power('avenging druid', 1).
card_toughness('avenging druid', 3).

% Found in: LRW, MMA
card_name('avian changeling', 'Avian Changeling').
card_type('avian changeling', 'Creature — Shapeshifter').
card_types('avian changeling', ['Creature']).
card_subtypes('avian changeling', ['Shapeshifter']).
card_colors('avian changeling', ['W']).
card_text('avian changeling', 'Changeling (This card is every creature type.)\nFlying').
card_mana_cost('avian changeling', ['2', 'W']).
card_cmc('avian changeling', 3).
card_layout('avian changeling', 'normal').
card_power('avian changeling', 2).
card_toughness('avian changeling', 2).

% Found in: WTH
card_name('avizoa', 'Avizoa').
card_type('avizoa', 'Creature — Jellyfish').
card_types('avizoa', ['Creature']).
card_subtypes('avizoa', ['Jellyfish']).
card_colors('avizoa', ['U']).
card_text('avizoa', 'Flying\n{0}: Avizoa gets +2/+2 until end of turn. You skip your next untap step. Activate this ability only once each turn.').
card_mana_cost('avizoa', ['3', 'U']).
card_cmc('avizoa', 4).
card_layout('avizoa', 'normal').
card_power('avizoa', 2).
card_toughness('avizoa', 2).
card_reserved('avizoa').

% Found in: LEG, TSB
card_name('avoid fate', 'Avoid Fate').
card_type('avoid fate', 'Instant').
card_types('avoid fate', ['Instant']).
card_subtypes('avoid fate', []).
card_colors('avoid fate', ['G']).
card_text('avoid fate', 'Counter target instant or Aura spell that targets a permanent you control.').
card_mana_cost('avoid fate', ['G']).
card_cmc('avoid fate', 1).
card_layout('avoid fate', 'normal').

% Found in: M14
card_name('awaken the ancient', 'Awaken the Ancient').
card_type('awaken the ancient', 'Enchantment — Aura').
card_types('awaken the ancient', ['Enchantment']).
card_subtypes('awaken the ancient', ['Aura']).
card_colors('awaken the ancient', ['R']).
card_text('awaken the ancient', 'Enchant Mountain\nEnchanted Mountain is a 7/7 red Giant creature with haste. It\'s still a land.').
card_mana_cost('awaken the ancient', ['1', 'R', 'R', 'R']).
card_cmc('awaken the ancient', 4).
card_layout('awaken the ancient', 'normal').

% Found in: KTK
card_name('awaken the bear', 'Awaken the Bear').
card_type('awaken the bear', 'Instant').
card_types('awaken the bear', ['Instant']).
card_subtypes('awaken the bear', []).
card_colors('awaken the bear', ['G']).
card_text('awaken the bear', 'Target creature gets +3/+3 and gains trample until end of turn.').
card_mana_cost('awaken the bear', ['2', 'G']).
card_cmc('awaken the bear', 3).
card_layout('awaken the bear', 'normal').

% Found in: M10, M11
card_name('awakener druid', 'Awakener Druid').
card_type('awakener druid', 'Creature — Human Druid').
card_types('awakener druid', ['Creature']).
card_subtypes('awakener druid', ['Human', 'Druid']).
card_colors('awakener druid', ['G']).
card_text('awakener druid', 'When Awakener Druid enters the battlefield, target Forest becomes a 4/5 green Treefolk creature for as long as Awakener Druid remains on the battlefield. It\'s still a land.').
card_mana_cost('awakener druid', ['2', 'G']).
card_cmc('awakener druid', 3).
card_layout('awakener druid', 'normal').
card_power('awakener druid', 1).
card_toughness('awakener druid', 1).

% Found in: STH
card_name('awakening', 'Awakening').
card_type('awakening', 'Enchantment').
card_types('awakening', ['Enchantment']).
card_subtypes('awakening', []).
card_colors('awakening', ['G']).
card_text('awakening', 'At the beginning of each upkeep, untap all creatures and lands.').
card_mana_cost('awakening', ['2', 'G', 'G']).
card_cmc('awakening', 4).
card_layout('awakening', 'normal').

% Found in: CMD, PC2, ROE
card_name('awakening zone', 'Awakening Zone').
card_type('awakening zone', 'Enchantment').
card_types('awakening zone', ['Enchantment']).
card_subtypes('awakening zone', []).
card_colors('awakening zone', ['G']).
card_text('awakening zone', 'At the beginning of your upkeep, you may put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('awakening zone', ['2', 'G']).
card_cmc('awakening zone', 3).
card_layout('awakening zone', 'normal').

% Found in: DGM
card_name('away', 'Away').
card_type('away', 'Instant').
card_types('away', ['Instant']).
card_subtypes('away', []).
card_colors('away', ['B']).
card_text('away', 'Target player sacrifices a creature.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('away', ['2', 'B']).
card_cmc('away', 3).
card_layout('away', 'split').

% Found in: DGM
card_name('awe for the guilds', 'Awe for the Guilds').
card_type('awe for the guilds', 'Sorcery').
card_types('awe for the guilds', ['Sorcery']).
card_subtypes('awe for the guilds', []).
card_colors('awe for the guilds', ['R']).
card_text('awe for the guilds', 'Monocolored creatures can\'t block this turn.').
card_mana_cost('awe for the guilds', ['2', 'R']).
card_cmc('awe for the guilds', 3).
card_layout('awe for the guilds', 'normal').

% Found in: MRD
card_name('awe strike', 'Awe Strike').
card_type('awe strike', 'Instant').
card_types('awe strike', ['Instant']).
card_subtypes('awe strike', []).
card_colors('awe strike', ['W']).
card_text('awe strike', 'The next time target creature would deal damage this turn, prevent that damage. You gain life equal to the damage prevented this way.').
card_mana_cost('awe strike', ['W']).
card_cmc('awe strike', 1).
card_layout('awe strike', 'normal').

% Found in: ALL
card_name('awesome presence', 'Awesome Presence').
card_type('awesome presence', 'Enchantment — Aura').
card_types('awesome presence', ['Enchantment']).
card_subtypes('awesome presence', ['Aura']).
card_colors('awesome presence', ['U']).
card_text('awesome presence', 'Enchant creature\nEnchanted creature can\'t be blocked unless defending player pays {3} for each creature he or she controls that\'s blocking it.').
card_mana_cost('awesome presence', ['U']).
card_cmc('awesome presence', 1).
card_layout('awesome presence', 'normal').

% Found in: UNH
card_name('awol', 'AWOL').
card_type('awol', 'Instant').
card_types('awol', ['Instant']).
card_subtypes('awol', []).
card_colors('awol', ['W']).
card_text('awol', 'Remove target attacking creature from the game. Then remove it from the removed-from-game zone and put it into the absolutely-removed-from-the-freaking-game-forever zone.').
card_mana_cost('awol', ['2', 'W']).
card_cmc('awol', 3).
card_layout('awol', 'normal').

% Found in: RTR
card_name('axebane guardian', 'Axebane Guardian').
card_type('axebane guardian', 'Creature — Human Druid').
card_types('axebane guardian', ['Creature']).
card_subtypes('axebane guardian', ['Human', 'Druid']).
card_colors('axebane guardian', ['G']).
card_text('axebane guardian', 'Defender\n{T}: Add X mana in any combination of colors to your mana pool, where X is the number of creatures with defender you control.').
card_mana_cost('axebane guardian', ['2', 'G']).
card_cmc('axebane guardian', 3).
card_layout('axebane guardian', 'normal').
card_power('axebane guardian', 0).
card_toughness('axebane guardian', 3).

% Found in: RTR
card_name('axebane stag', 'Axebane Stag').
card_type('axebane stag', 'Creature — Elk').
card_types('axebane stag', ['Creature']).
card_subtypes('axebane stag', ['Elk']).
card_colors('axebane stag', ['G']).
card_text('axebane stag', '').
card_mana_cost('axebane stag', ['6', 'G']).
card_cmc('axebane stag', 7).
card_layout('axebane stag', 'normal').
card_power('axebane stag', 6).
card_toughness('axebane stag', 7).

% Found in: LRW
card_name('axegrinder giant', 'Axegrinder Giant').
card_type('axegrinder giant', 'Creature — Giant Warrior').
card_types('axegrinder giant', ['Creature']).
card_subtypes('axegrinder giant', ['Giant', 'Warrior']).
card_colors('axegrinder giant', ['R']).
card_text('axegrinder giant', '').
card_mana_cost('axegrinder giant', ['4', 'R', 'R']).
card_cmc('axegrinder giant', 6).
card_layout('axegrinder giant', 'normal').
card_power('axegrinder giant', 6).
card_toughness('axegrinder giant', 4).

% Found in: CHR, LEG, ME3
card_name('axelrod gunnarson', 'Axelrod Gunnarson').
card_type('axelrod gunnarson', 'Legendary Creature — Giant').
card_types('axelrod gunnarson', ['Creature']).
card_subtypes('axelrod gunnarson', ['Giant']).
card_supertypes('axelrod gunnarson', ['Legendary']).
card_colors('axelrod gunnarson', ['B', 'R']).
card_text('axelrod gunnarson', 'Trample\nWhenever a creature dealt damage by Axelrod Gunnarson this turn dies, you gain 1 life and Axelrod deals 1 damage to target player.').
card_mana_cost('axelrod gunnarson', ['4', 'B', 'B', 'R', 'R']).
card_cmc('axelrod gunnarson', 8).
card_layout('axelrod gunnarson', 'normal').
card_power('axelrod gunnarson', 5).
card_toughness('axelrod gunnarson', 5).

% Found in: CHR, LEG
card_name('ayesha tanaka', 'Ayesha Tanaka').
card_type('ayesha tanaka', 'Legendary Creature — Human Artificer').
card_types('ayesha tanaka', ['Creature']).
card_subtypes('ayesha tanaka', ['Human', 'Artificer']).
card_supertypes('ayesha tanaka', ['Legendary']).
card_colors('ayesha tanaka', ['W', 'U']).
card_text('ayesha tanaka', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\n{T}: Counter target activated ability from an artifact source unless that ability\'s controller pays {W}. (Mana abilities can\'t be targeted.)').
card_mana_cost('ayesha tanaka', ['W', 'W', 'U', 'U']).
card_cmc('ayesha tanaka', 4).
card_layout('ayesha tanaka', 'normal').
card_power('ayesha tanaka', 2).
card_toughness('ayesha tanaka', 2).

% Found in: HML
card_name('aysen abbey', 'Aysen Abbey').
card_type('aysen abbey', 'Land').
card_types('aysen abbey', ['Land']).
card_subtypes('aysen abbey', []).
card_colors('aysen abbey', []).
card_text('aysen abbey', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add {W} to your mana pool.\n{2}, {T}: Add {G} or {U} to your mana pool.').
card_layout('aysen abbey', 'normal').

% Found in: 5ED, HML, ME2
card_name('aysen bureaucrats', 'Aysen Bureaucrats').
card_type('aysen bureaucrats', 'Creature — Human Advisor').
card_types('aysen bureaucrats', ['Creature']).
card_subtypes('aysen bureaucrats', ['Human', 'Advisor']).
card_colors('aysen bureaucrats', ['W']).
card_text('aysen bureaucrats', '{T}: Tap target creature with power 2 or less.').
card_mana_cost('aysen bureaucrats', ['1', 'W']).
card_cmc('aysen bureaucrats', 2).
card_layout('aysen bureaucrats', 'normal').
card_power('aysen bureaucrats', 1).
card_toughness('aysen bureaucrats', 1).

% Found in: HML, ME2
card_name('aysen crusader', 'Aysen Crusader').
card_type('aysen crusader', 'Creature — Human Knight').
card_types('aysen crusader', ['Creature']).
card_subtypes('aysen crusader', ['Human', 'Knight']).
card_colors('aysen crusader', ['W']).
card_text('aysen crusader', 'Aysen Crusader\'s power and toughness are each equal to 2 plus the number of Soldiers and Warriors you control.').
card_mana_cost('aysen crusader', ['2', 'W', 'W']).
card_cmc('aysen crusader', 4).
card_layout('aysen crusader', 'normal').
card_power('aysen crusader', '2+*').
card_toughness('aysen crusader', '2+*').
card_reserved('aysen crusader').

% Found in: HML
card_name('aysen highway', 'Aysen Highway').
card_type('aysen highway', 'Enchantment').
card_types('aysen highway', ['Enchantment']).
card_subtypes('aysen highway', []).
card_colors('aysen highway', ['W']).
card_text('aysen highway', 'White creatures have plainswalk. (They can\'t be blocked as long as defending player controls a Plains.)').
card_mana_cost('aysen highway', ['3', 'W', 'W', 'W']).
card_cmc('aysen highway', 6).
card_layout('aysen highway', 'normal').
card_reserved('aysen highway').

% Found in: SOK
card_name('ayumi, the last visitor', 'Ayumi, the Last Visitor').
card_type('ayumi, the last visitor', 'Legendary Creature — Spirit').
card_types('ayumi, the last visitor', ['Creature']).
card_subtypes('ayumi, the last visitor', ['Spirit']).
card_supertypes('ayumi, the last visitor', ['Legendary']).
card_colors('ayumi, the last visitor', ['G']).
card_text('ayumi, the last visitor', 'Legendary landwalk (This creature can\'t be blocked as long as defending player controls a legendary land.)').
card_mana_cost('ayumi, the last visitor', ['3', 'G', 'G']).
card_cmc('ayumi, the last visitor', 5).
card_layout('ayumi, the last visitor', 'normal').
card_power('ayumi, the last visitor', 7).
card_toughness('ayumi, the last visitor', 3).

% Found in: C13, CHK
card_name('azami, lady of scrolls', 'Azami, Lady of Scrolls').
card_type('azami, lady of scrolls', 'Legendary Creature — Human Wizard').
card_types('azami, lady of scrolls', ['Creature']).
card_subtypes('azami, lady of scrolls', ['Human', 'Wizard']).
card_supertypes('azami, lady of scrolls', ['Legendary']).
card_colors('azami, lady of scrolls', ['U']).
card_text('azami, lady of scrolls', 'Tap an untapped Wizard you control: Draw a card.').
card_mana_cost('azami, lady of scrolls', ['2', 'U', 'U', 'U']).
card_cmc('azami, lady of scrolls', 5).
card_layout('azami, lady of scrolls', 'normal').
card_power('azami, lady of scrolls', 0).
card_toughness('azami, lady of scrolls', 2).

% Found in: BOK
card_name('azamuki, treachery incarnate', 'Azamuki, Treachery Incarnate').
card_type('azamuki, treachery incarnate', 'Legendary Creature — Spirit').
card_types('azamuki, treachery incarnate', ['Creature']).
card_subtypes('azamuki, treachery incarnate', ['Spirit']).
card_supertypes('azamuki, treachery incarnate', ['Legendary']).
card_colors('azamuki, treachery incarnate', ['R']).
card_text('azamuki, treachery incarnate', 'Remove a ki counter from Azamuki, Treachery Incarnate: Gain control of target creature until end of turn.').
card_mana_cost('azamuki, treachery incarnate', ['1', 'R', 'R']).
card_cmc('azamuki, treachery incarnate', 3).
card_layout('azamuki, treachery incarnate', 'flip').
card_power('azamuki, treachery incarnate', 5).
card_toughness('azamuki, treachery incarnate', 2).

% Found in: MIR
card_name('azimaet drake', 'Azimaet Drake').
card_type('azimaet drake', 'Creature — Drake').
card_types('azimaet drake', ['Creature']).
card_subtypes('azimaet drake', ['Drake']).
card_colors('azimaet drake', ['U']).
card_text('azimaet drake', 'Flying\n{U}: Azimaet Drake gets +1/+0 until end of turn. Activate this ability only once each turn.').
card_mana_cost('azimaet drake', ['2', 'U']).
card_cmc('azimaet drake', 3).
card_layout('azimaet drake', 'normal').
card_power('azimaet drake', 1).
card_toughness('azimaet drake', 3).

% Found in: RTR
card_name('azor\'s elocutors', 'Azor\'s Elocutors').
card_type('azor\'s elocutors', 'Creature — Human Advisor').
card_types('azor\'s elocutors', ['Creature']).
card_subtypes('azor\'s elocutors', ['Human', 'Advisor']).
card_colors('azor\'s elocutors', ['W', 'U']).
card_text('azor\'s elocutors', 'At the beginning of your upkeep, put a filibuster counter on Azor\'s Elocutors. Then if Azor\'s Elocutors has five or more filibuster counters on it, you win the game.\nWhenever a source deals damage to you, remove a filibuster counter from Azor\'s Elocutors.').
card_mana_cost('azor\'s elocutors', ['3', 'W/U', 'W/U']).
card_cmc('azor\'s elocutors', 5).
card_layout('azor\'s elocutors', 'normal').
card_power('azor\'s elocutors', 3).
card_toughness('azor\'s elocutors', 5).

% Found in: RTR
card_name('azorius arrester', 'Azorius Arrester').
card_type('azorius arrester', 'Creature — Human Soldier').
card_types('azorius arrester', ['Creature']).
card_subtypes('azorius arrester', ['Human', 'Soldier']).
card_colors('azorius arrester', ['W']).
card_text('azorius arrester', 'When Azorius Arrester enters the battlefield, detain target creature an opponent controls. (Until your next turn, that creature can\'t attack or block and its activated abilities can\'t be activated.)').
card_mana_cost('azorius arrester', ['1', 'W']).
card_cmc('azorius arrester', 2).
card_layout('azorius arrester', 'normal').
card_power('azorius arrester', 2).
card_toughness('azorius arrester', 1).

% Found in: C13, CMD, DDI, DIS, MM2
card_name('azorius chancery', 'Azorius Chancery').
card_type('azorius chancery', 'Land').
card_types('azorius chancery', ['Land']).
card_subtypes('azorius chancery', []).
card_colors('azorius chancery', []).
card_text('azorius chancery', 'Azorius Chancery enters the battlefield tapped.\nWhen Azorius Chancery enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {W}{U} to your mana pool.').
card_layout('azorius chancery', 'normal').

% Found in: RTR
card_name('azorius charm', 'Azorius Charm').
card_type('azorius charm', 'Instant').
card_types('azorius charm', ['Instant']).
card_subtypes('azorius charm', []).
card_colors('azorius charm', ['W', 'U']).
card_text('azorius charm', 'Choose one —\n• Creatures you control gain lifelink until end of turn.\n• Draw a card.\n• Put target attacking or blocking creature on top of its owner\'s library.').
card_mana_cost('azorius charm', ['W', 'U']).
card_cmc('azorius charm', 2).
card_layout('azorius charm', 'normal').

% Found in: DGM
card_name('azorius cluestone', 'Azorius Cluestone').
card_type('azorius cluestone', 'Artifact').
card_types('azorius cluestone', ['Artifact']).
card_subtypes('azorius cluestone', []).
card_colors('azorius cluestone', []).
card_text('azorius cluestone', '{T}: Add {W} or {U} to your mana pool.\n{W}{U}, {T}, Sacrifice Azorius Cluestone: Draw a card.').
card_mana_cost('azorius cluestone', ['3']).
card_cmc('azorius cluestone', 3).
card_layout('azorius cluestone', 'normal').

% Found in: DIS
card_name('azorius first-wing', 'Azorius First-Wing').
card_type('azorius first-wing', 'Creature — Griffin').
card_types('azorius first-wing', ['Creature']).
card_subtypes('azorius first-wing', ['Griffin']).
card_colors('azorius first-wing', ['W', 'U']).
card_text('azorius first-wing', 'Flying, protection from enchantments').
card_mana_cost('azorius first-wing', ['W', 'U']).
card_cmc('azorius first-wing', 2).
card_layout('azorius first-wing', 'normal').
card_power('azorius first-wing', 2).
card_toughness('azorius first-wing', 2).

% Found in: C13, DGM, RTR
card_name('azorius guildgate', 'Azorius Guildgate').
card_type('azorius guildgate', 'Land — Gate').
card_types('azorius guildgate', ['Land']).
card_subtypes('azorius guildgate', ['Gate']).
card_colors('azorius guildgate', []).
card_text('azorius guildgate', 'Azorius Guildgate enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_layout('azorius guildgate', 'normal').

% Found in: CMD, DIS, pREL
card_name('azorius guildmage', 'Azorius Guildmage').
card_type('azorius guildmage', 'Creature — Vedalken Wizard').
card_types('azorius guildmage', ['Creature']).
card_subtypes('azorius guildmage', ['Vedalken', 'Wizard']).
card_colors('azorius guildmage', ['W', 'U']).
card_text('azorius guildmage', '{2}{W}: Tap target creature.\n{2}{U}: Counter target activated ability. (Mana abilities can\'t be targeted.)').
card_mana_cost('azorius guildmage', ['W/U', 'W/U']).
card_cmc('azorius guildmage', 2).
card_layout('azorius guildmage', 'normal').
card_power('azorius guildmage', 2).
card_toughness('azorius guildmage', 2).

% Found in: C13, DIS
card_name('azorius herald', 'Azorius Herald').
card_type('azorius herald', 'Creature — Spirit').
card_types('azorius herald', ['Creature']).
card_subtypes('azorius herald', ['Spirit']).
card_colors('azorius herald', ['W']).
card_text('azorius herald', 'Azorius Herald can\'t be blocked.\nWhen Azorius Herald enters the battlefield, you gain 4 life.\nWhen Azorius Herald enters the battlefield, sacrifice it unless {U} was spent to cast it.').
card_mana_cost('azorius herald', ['2', 'W']).
card_cmc('azorius herald', 3).
card_layout('azorius herald', 'normal').
card_power('azorius herald', 2).
card_toughness('azorius herald', 1).

% Found in: RTR
card_name('azorius justiciar', 'Azorius Justiciar').
card_type('azorius justiciar', 'Creature — Human Wizard').
card_types('azorius justiciar', ['Creature']).
card_subtypes('azorius justiciar', ['Human', 'Wizard']).
card_colors('azorius justiciar', ['W']).
card_text('azorius justiciar', 'When Azorius Justiciar enters the battlefield, detain up to two target creatures your opponents control. (Until your next turn, those creatures can\'t attack or block and their activated abilities can\'t be activated.)').
card_mana_cost('azorius justiciar', ['2', 'W', 'W']).
card_cmc('azorius justiciar', 4).
card_layout('azorius justiciar', 'normal').
card_power('azorius justiciar', 2).
card_toughness('azorius justiciar', 2).

% Found in: C13, RTR
card_name('azorius keyrune', 'Azorius Keyrune').
card_type('azorius keyrune', 'Artifact').
card_types('azorius keyrune', ['Artifact']).
card_subtypes('azorius keyrune', []).
card_colors('azorius keyrune', []).
card_text('azorius keyrune', '{T}: Add {W} or {U} to your mana pool.\n{W}{U}: Azorius Keyrune becomes a 2/2 white and blue Bird artifact creature with flying until end of turn.').
card_mana_cost('azorius keyrune', ['3']).
card_cmc('azorius keyrune', 3).
card_layout('azorius keyrune', 'normal').

% Found in: DIS
card_name('azorius ploy', 'Azorius Ploy').
card_type('azorius ploy', 'Instant').
card_types('azorius ploy', ['Instant']).
card_subtypes('azorius ploy', []).
card_colors('azorius ploy', ['W', 'U']).
card_text('azorius ploy', 'Prevent all combat damage target creature would deal this turn.\nPrevent all combat damage that would be dealt to target creature this turn.').
card_mana_cost('azorius ploy', ['1', 'W', 'W', 'U']).
card_cmc('azorius ploy', 4).
card_layout('azorius ploy', 'normal').

% Found in: ARC, DIS
card_name('azorius signet', 'Azorius Signet').
card_type('azorius signet', 'Artifact').
card_types('azorius signet', ['Artifact']).
card_subtypes('azorius signet', []).
card_colors('azorius signet', []).
card_text('azorius signet', '{1}, {T}: Add {W}{U} to your mana pool.').
card_mana_cost('azorius signet', ['2']).
card_cmc('azorius signet', 2).
card_layout('azorius signet', 'normal').

% Found in: DIS
card_name('azorius æthermage', 'Azorius Æthermage').
card_type('azorius æthermage', 'Creature — Human Wizard').
card_types('azorius æthermage', ['Creature']).
card_subtypes('azorius æthermage', ['Human', 'Wizard']).
card_colors('azorius æthermage', ['W', 'U']).
card_text('azorius æthermage', 'Whenever a permanent is returned to your hand, you may pay {1}. If you do, draw a card.').
card_mana_cost('azorius æthermage', ['1', 'W', 'U']).
card_cmc('azorius æthermage', 3).
card_layout('azorius æthermage', 'normal').
card_power('azorius æthermage', 1).
card_toughness('azorius æthermage', 1).

% Found in: 5ED, 9ED, BRB, CHR, LEG, M11
card_name('azure drake', 'Azure Drake').
card_type('azure drake', 'Creature — Drake').
card_types('azure drake', ['Creature']).
card_subtypes('azure drake', ['Drake']).
card_colors('azure drake', ['U']).
card_text('azure drake', 'Flying').
card_mana_cost('azure drake', ['3', 'U']).
card_cmc('azure drake', 4).
card_layout('azure drake', 'normal').
card_power('azure drake', 2).
card_toughness('azure drake', 4).

% Found in: C14, M12
card_name('azure mage', 'Azure Mage').
card_type('azure mage', 'Creature — Human Wizard').
card_types('azure mage', ['Creature']).
card_subtypes('azure mage', ['Human', 'Wizard']).
card_colors('azure mage', ['U']).
card_text('azure mage', '{3}{U}: Draw a card.').
card_mana_cost('azure mage', ['1', 'U']).
card_cmc('azure mage', 2).
card_layout('azure mage', 'normal').
card_power('azure mage', 2).
card_toughness('azure mage', 1).

% Found in: CHK
card_name('azusa, lost but seeking', 'Azusa, Lost but Seeking').
card_type('azusa, lost but seeking', 'Legendary Creature — Human Monk').
card_types('azusa, lost but seeking', ['Creature']).
card_subtypes('azusa, lost but seeking', ['Human', 'Monk']).
card_supertypes('azusa, lost but seeking', ['Legendary']).
card_colors('azusa, lost but seeking', ['G']).
card_text('azusa, lost but seeking', 'You may play two additional lands on each of your turns.').
card_mana_cost('azusa, lost but seeking', ['2', 'G']).
card_cmc('azusa, lost but seeking', 3).
card_layout('azusa, lost but seeking', 'normal').
card_power('azusa, lost but seeking', 1).
card_toughness('azusa, lost but seeking', 2).

