% Card-specific data

% Found in: CHR, LEG, ME3
card_name('gabriel angelfire', 'Gabriel Angelfire').
card_type('gabriel angelfire', 'Legendary Creature — Angel').
card_types('gabriel angelfire', ['Creature']).
card_subtypes('gabriel angelfire', ['Angel']).
card_supertypes('gabriel angelfire', ['Legendary']).
card_colors('gabriel angelfire', ['W', 'G']).
card_text('gabriel angelfire', 'At the beginning of your upkeep, choose flying, first strike, trample, or rampage 3. Gabriel Angelfire gains that ability until your next upkeep. (Whenever a creature with rampage 3 becomes blocked, it gets +3/+3 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('gabriel angelfire', ['3', 'G', 'G', 'W', 'W']).
card_cmc('gabriel angelfire', 7).
card_layout('gabriel angelfire', 'normal').
card_power('gabriel angelfire', 4).
card_toughness('gabriel angelfire', 4).

% Found in: LRW
card_name('gaddock teeg', 'Gaddock Teeg').
card_type('gaddock teeg', 'Legendary Creature — Kithkin Advisor').
card_types('gaddock teeg', ['Creature']).
card_subtypes('gaddock teeg', ['Kithkin', 'Advisor']).
card_supertypes('gaddock teeg', ['Legendary']).
card_colors('gaddock teeg', ['W', 'G']).
card_text('gaddock teeg', 'Noncreature spells with converted mana cost 4 or greater can\'t be cast.\nNoncreature spells with {X} in their mana costs can\'t be cast.').
card_mana_cost('gaddock teeg', ['G', 'W']).
card_cmc('gaddock teeg', 2).
card_layout('gaddock teeg', 'normal').
card_power('gaddock teeg', 2).
card_toughness('gaddock teeg', 2).

% Found in: PLC
card_name('gaea\'s anthem', 'Gaea\'s Anthem').
card_type('gaea\'s anthem', 'Enchantment').
card_types('gaea\'s anthem', ['Enchantment']).
card_subtypes('gaea\'s anthem', []).
card_colors('gaea\'s anthem', ['G']).
card_text('gaea\'s anthem', 'Creatures you control get +1/+1.').
card_mana_cost('gaea\'s anthem', ['1', 'G', 'G']).
card_cmc('gaea\'s anthem', 3).
card_layout('gaea\'s anthem', 'normal').

% Found in: ATQ, ME4
card_name('gaea\'s avenger', 'Gaea\'s Avenger').
card_type('gaea\'s avenger', 'Creature — Treefolk').
card_types('gaea\'s avenger', ['Creature']).
card_subtypes('gaea\'s avenger', ['Treefolk']).
card_colors('gaea\'s avenger', ['G']).
card_text('gaea\'s avenger', 'Gaea\'s Avenger\'s power and toughness are each equal to 1 plus the number of artifacts your opponents control.').
card_mana_cost('gaea\'s avenger', ['1', 'G', 'G']).
card_cmc('gaea\'s avenger', 3).
card_layout('gaea\'s avenger', 'normal').
card_power('gaea\'s avenger', '1+*').
card_toughness('gaea\'s avenger', '1+*').
card_reserved('gaea\'s avenger').

% Found in: APC
card_name('gaea\'s balance', 'Gaea\'s Balance').
card_type('gaea\'s balance', 'Sorcery').
card_types('gaea\'s balance', ['Sorcery']).
card_subtypes('gaea\'s balance', []).
card_colors('gaea\'s balance', ['G']).
card_text('gaea\'s balance', 'As an additional cost to cast Gaea\'s Balance, sacrifice five lands.\nSearch your library for a land card of each basic land type and put them onto the battlefield. Then shuffle your library.').
card_mana_cost('gaea\'s balance', ['3', 'G']).
card_cmc('gaea\'s balance', 4).
card_layout('gaea\'s balance', 'normal').

% Found in: TSB, WTH, pARL
card_name('gaea\'s blessing', 'Gaea\'s Blessing').
card_type('gaea\'s blessing', 'Sorcery').
card_types('gaea\'s blessing', ['Sorcery']).
card_subtypes('gaea\'s blessing', []).
card_colors('gaea\'s blessing', ['G']).
card_text('gaea\'s blessing', 'Target player shuffles up to three target cards from his or her graveyard into his or her library.\nDraw a card.\nWhen Gaea\'s Blessing is put into your graveyard from your library, shuffle your graveyard into your library.').
card_mana_cost('gaea\'s blessing', ['1', 'G']).
card_cmc('gaea\'s blessing', 2).
card_layout('gaea\'s blessing', 'normal').

% Found in: USG
card_name('gaea\'s bounty', 'Gaea\'s Bounty').
card_type('gaea\'s bounty', 'Sorcery').
card_types('gaea\'s bounty', ['Sorcery']).
card_subtypes('gaea\'s bounty', []).
card_colors('gaea\'s bounty', ['G']).
card_text('gaea\'s bounty', 'Search your library for up to two Forest cards, reveal those cards, and put them into your hand. Then shuffle your library.').
card_mana_cost('gaea\'s bounty', ['2', 'G']).
card_cmc('gaea\'s bounty', 3).
card_layout('gaea\'s bounty', 'normal').

% Found in: USG, pJGP
card_name('gaea\'s cradle', 'Gaea\'s Cradle').
card_type('gaea\'s cradle', 'Legendary Land').
card_types('gaea\'s cradle', ['Land']).
card_subtypes('gaea\'s cradle', []).
card_supertypes('gaea\'s cradle', ['Legendary']).
card_colors('gaea\'s cradle', []).
card_text('gaea\'s cradle', '{T}: Add {G} to your mana pool for each creature you control.').
card_layout('gaea\'s cradle', 'normal').
card_reserved('gaea\'s cradle').

% Found in: USG, VMA
card_name('gaea\'s embrace', 'Gaea\'s Embrace').
card_type('gaea\'s embrace', 'Enchantment — Aura').
card_types('gaea\'s embrace', ['Enchantment']).
card_subtypes('gaea\'s embrace', ['Aura']).
card_colors('gaea\'s embrace', ['G']).
card_text('gaea\'s embrace', 'Enchant creature\nEnchanted creature gets +3/+3 and has trample.\n{G}: Regenerate enchanted creature.').
card_mana_cost('gaea\'s embrace', ['2', 'G', 'G']).
card_cmc('gaea\'s embrace', 4).
card_layout('gaea\'s embrace', 'normal').

% Found in: 10E, 8ED, DPA, PLS
card_name('gaea\'s herald', 'Gaea\'s Herald').
card_type('gaea\'s herald', 'Creature — Elf').
card_types('gaea\'s herald', ['Creature']).
card_subtypes('gaea\'s herald', ['Elf']).
card_colors('gaea\'s herald', ['G']).
card_text('gaea\'s herald', 'Creature spells can\'t be countered.').
card_mana_cost('gaea\'s herald', ['1', 'G']).
card_cmc('gaea\'s herald', 2).
card_layout('gaea\'s herald', 'normal').
card_power('gaea\'s herald', 1).
card_toughness('gaea\'s herald', 1).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, TSB
card_name('gaea\'s liege', 'Gaea\'s Liege').
card_type('gaea\'s liege', 'Creature — Avatar').
card_types('gaea\'s liege', ['Creature']).
card_subtypes('gaea\'s liege', ['Avatar']).
card_colors('gaea\'s liege', ['G']).
card_text('gaea\'s liege', 'As long as Gaea\'s Liege isn\'t attacking, its power and toughness are each equal to the number of Forests you control. As long as Gaea\'s Liege is attacking, its power and toughness are each equal to the number of Forests defending player controls.\n{T}: Target land becomes a Forest until Gaea\'s Liege leaves the battlefield.').
card_mana_cost('gaea\'s liege', ['3', 'G', 'G', 'G']).
card_cmc('gaea\'s liege', 6).
card_layout('gaea\'s liege', 'normal').
card_power('gaea\'s liege', '*').
card_toughness('gaea\'s liege', '*').

% Found in: PLS
card_name('gaea\'s might', 'Gaea\'s Might').
card_type('gaea\'s might', 'Instant').
card_types('gaea\'s might', ['Instant']).
card_subtypes('gaea\'s might', []).
card_colors('gaea\'s might', ['G']).
card_text('gaea\'s might', 'Domain — Target creature gets +1/+1 until end of turn for each basic land type among lands you control.').
card_mana_cost('gaea\'s might', ['G']).
card_cmc('gaea\'s might', 1).
card_layout('gaea\'s might', 'normal').

% Found in: M11, ORI
card_name('gaea\'s revenge', 'Gaea\'s Revenge').
card_type('gaea\'s revenge', 'Creature — Elemental').
card_types('gaea\'s revenge', ['Creature']).
card_subtypes('gaea\'s revenge', ['Elemental']).
card_colors('gaea\'s revenge', ['G']).
card_text('gaea\'s revenge', 'Gaea\'s Revenge can\'t be countered.\nHaste\nGaea\'s Revenge can\'t be the target of nongreen spells or abilities from nongreen sources.').
card_mana_cost('gaea\'s revenge', ['5', 'G', 'G']).
card_cmc('gaea\'s revenge', 7).
card_layout('gaea\'s revenge', 'normal').
card_power('gaea\'s revenge', 8).
card_toughness('gaea\'s revenge', 5).

% Found in: APC
card_name('gaea\'s skyfolk', 'Gaea\'s Skyfolk').
card_type('gaea\'s skyfolk', 'Creature — Elf Merfolk').
card_types('gaea\'s skyfolk', ['Creature']).
card_subtypes('gaea\'s skyfolk', ['Elf', 'Merfolk']).
card_colors('gaea\'s skyfolk', ['U', 'G']).
card_text('gaea\'s skyfolk', 'Flying').
card_mana_cost('gaea\'s skyfolk', ['G', 'U']).
card_cmc('gaea\'s skyfolk', 2).
card_layout('gaea\'s skyfolk', 'normal').
card_power('gaea\'s skyfolk', 2).
card_toughness('gaea\'s skyfolk', 2).

% Found in: DRK, ME3
card_name('gaea\'s touch', 'Gaea\'s Touch').
card_type('gaea\'s touch', 'Enchantment').
card_types('gaea\'s touch', ['Enchantment']).
card_subtypes('gaea\'s touch', []).
card_colors('gaea\'s touch', ['G']).
card_text('gaea\'s touch', '{0}: You may put a basic Forest card from your hand onto the battlefield. Activate this ability only any time you could cast a sorcery and only once each turn.\nSacrifice Gaea\'s Touch: Add {G}{G} to your mana pool.').
card_mana_cost('gaea\'s touch', ['G', 'G']).
card_cmc('gaea\'s touch', 2).
card_layout('gaea\'s touch', 'normal').

% Found in: C13
card_name('gahiji, honored one', 'Gahiji, Honored One').
card_type('gahiji, honored one', 'Legendary Creature — Beast').
card_types('gahiji, honored one', ['Creature']).
card_subtypes('gahiji, honored one', ['Beast']).
card_supertypes('gahiji, honored one', ['Legendary']).
card_colors('gahiji, honored one', ['W', 'R', 'G']).
card_text('gahiji, honored one', 'Whenever a creature attacks one of your opponents or a planeswalker an opponent controls, that creature gets +2/+0 until end of turn.').
card_mana_cost('gahiji, honored one', ['2', 'R', 'G', 'W']).
card_cmc('gahiji, honored one', 5).
card_layout('gahiji, honored one', 'normal').
card_power('gahiji, honored one', 4).
card_toughness('gahiji, honored one', 4).

% Found in: PLS, THS
card_name('gainsay', 'Gainsay').
card_type('gainsay', 'Instant').
card_types('gainsay', ['Instant']).
card_subtypes('gainsay', []).
card_colors('gainsay', ['U']).
card_text('gainsay', 'Counter target blue spell.').
card_mana_cost('gainsay', ['1', 'U']).
card_cmc('gainsay', 2).
card_layout('gainsay', 'normal').

% Found in: CHK
card_name('gale force', 'Gale Force').
card_type('gale force', 'Sorcery').
card_types('gale force', ['Sorcery']).
card_subtypes('gale force', []).
card_colors('gale force', ['G']).
card_text('gale force', 'Gale Force deals 5 damage to each creature with flying.').
card_mana_cost('gale force', ['4', 'G']).
card_cmc('gale force', 5).
card_layout('gale force', 'normal').

% Found in: DDI, LRW
card_name('galepowder mage', 'Galepowder Mage').
card_type('galepowder mage', 'Creature — Kithkin Wizard').
card_types('galepowder mage', ['Creature']).
card_subtypes('galepowder mage', ['Kithkin', 'Wizard']).
card_colors('galepowder mage', ['W']).
card_text('galepowder mage', 'Flying\nWhenever Galepowder Mage attacks, exile another target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('galepowder mage', ['3', 'W']).
card_cmc('galepowder mage', 4).
card_layout('galepowder mage', 'normal').
card_power('galepowder mage', 3).
card_toughness('galepowder mage', 3).

% Found in: M14
card_name('galerider sliver', 'Galerider Sliver').
card_type('galerider sliver', 'Creature — Sliver').
card_types('galerider sliver', ['Creature']).
card_subtypes('galerider sliver', ['Sliver']).
card_colors('galerider sliver', ['U']).
card_text('galerider sliver', 'Sliver creatures you control have flying.').
card_mana_cost('galerider sliver', ['U']).
card_cmc('galerider sliver', 1).
card_layout('galerider sliver', 'normal').
card_power('galerider sliver', 1).
card_toughness('galerider sliver', 1).

% Found in: INV
card_name('galina\'s knight', 'Galina\'s Knight').
card_type('galina\'s knight', 'Creature — Merfolk Knight').
card_types('galina\'s knight', ['Creature']).
card_subtypes('galina\'s knight', ['Merfolk', 'Knight']).
card_colors('galina\'s knight', ['W', 'U']).
card_text('galina\'s knight', 'Protection from red').
card_mana_cost('galina\'s knight', ['W', 'U']).
card_cmc('galina\'s knight', 2).
card_layout('galina\'s knight', 'normal').
card_power('galina\'s knight', 2).
card_toughness('galina\'s knight', 2).

% Found in: ODY, TMP, TPR
card_name('gallantry', 'Gallantry').
card_type('gallantry', 'Instant').
card_types('gallantry', ['Instant']).
card_subtypes('gallantry', []).
card_colors('gallantry', ['W']).
card_text('gallantry', 'Target blocking creature gets +4/+4 until end of turn.\nDraw a card.').
card_mana_cost('gallantry', ['1', 'W']).
card_cmc('gallantry', 2).
card_layout('gallantry', 'normal').

% Found in: WTH
card_name('gallowbraid', 'Gallowbraid').
card_type('gallowbraid', 'Legendary Creature — Horror').
card_types('gallowbraid', ['Creature']).
card_subtypes('gallowbraid', ['Horror']).
card_supertypes('gallowbraid', ['Legendary']).
card_colors('gallowbraid', ['B']).
card_text('gallowbraid', 'Trample\nCumulative upkeep—Pay 1 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('gallowbraid', ['3', 'B', 'B']).
card_cmc('gallowbraid', 5).
card_layout('gallowbraid', 'normal').
card_power('gallowbraid', 5).
card_toughness('gallowbraid', 5).
card_reserved('gallowbraid').

% Found in: AVR
card_name('gallows at willow hill', 'Gallows at Willow Hill').
card_type('gallows at willow hill', 'Artifact').
card_types('gallows at willow hill', ['Artifact']).
card_subtypes('gallows at willow hill', []).
card_colors('gallows at willow hill', []).
card_text('gallows at willow hill', '{3}, {T}, Tap three untapped Humans you control: Destroy target creature. Its controller puts a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('gallows at willow hill', ['3']).
card_cmc('gallows at willow hill', 3).
card_layout('gallows at willow hill', 'normal').

% Found in: ISD
card_name('gallows warden', 'Gallows Warden').
card_type('gallows warden', 'Creature — Spirit').
card_types('gallows warden', ['Creature']).
card_subtypes('gallows warden', ['Spirit']).
card_colors('gallows warden', ['W']).
card_text('gallows warden', 'Flying\nOther Spirit creatures you control get +0/+1.').
card_mana_cost('gallows warden', ['4', 'W']).
card_cmc('gallows warden', 5).
card_layout('gallows warden', 'normal').
card_power('gallows warden', 3).
card_toughness('gallows warden', 3).

% Found in: AVR
card_name('galvanic alchemist', 'Galvanic Alchemist').
card_type('galvanic alchemist', 'Creature — Human Wizard').
card_types('galvanic alchemist', ['Creature']).
card_subtypes('galvanic alchemist', ['Human', 'Wizard']).
card_colors('galvanic alchemist', ['U']).
card_text('galvanic alchemist', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Galvanic Alchemist is paired with another creature, each of those creatures has \"{2}{U}: Untap this creature.\"').
card_mana_cost('galvanic alchemist', ['2', 'U']).
card_cmc('galvanic alchemist', 3).
card_layout('galvanic alchemist', 'normal').
card_power('galvanic alchemist', 1).
card_toughness('galvanic alchemist', 4).

% Found in: RAV
card_name('galvanic arc', 'Galvanic Arc').
card_type('galvanic arc', 'Enchantment — Aura').
card_types('galvanic arc', ['Enchantment']).
card_subtypes('galvanic arc', ['Aura']).
card_colors('galvanic arc', ['R']).
card_text('galvanic arc', 'Enchant creature\nWhen Galvanic Arc enters the battlefield, it deals 3 damage to target creature or player.\nEnchanted creature has first strike.').
card_mana_cost('galvanic arc', ['2', 'R']).
card_cmc('galvanic arc', 3).
card_layout('galvanic arc', 'normal').

% Found in: SOM
card_name('galvanic blast', 'Galvanic Blast').
card_type('galvanic blast', 'Instant').
card_types('galvanic blast', ['Instant']).
card_subtypes('galvanic blast', []).
card_colors('galvanic blast', ['R']).
card_text('galvanic blast', 'Galvanic Blast deals 2 damage to target creature or player.\nMetalcraft — Galvanic Blast deals 4 damage to that creature or player instead if you control three or more artifacts.').
card_mana_cost('galvanic blast', ['R']).
card_cmc('galvanic blast', 1).
card_layout('galvanic blast', 'normal').

% Found in: CNS, ISD
card_name('galvanic juggernaut', 'Galvanic Juggernaut').
card_type('galvanic juggernaut', 'Artifact Creature — Juggernaut').
card_types('galvanic juggernaut', ['Artifact', 'Creature']).
card_subtypes('galvanic juggernaut', ['Juggernaut']).
card_colors('galvanic juggernaut', []).
card_text('galvanic juggernaut', 'Galvanic Juggernaut attacks each turn if able.\nGalvanic Juggernaut doesn\'t untap during your untap step.\nWhenever another creature dies, untap Galvanic Juggernaut.').
card_mana_cost('galvanic juggernaut', ['4']).
card_cmc('galvanic juggernaut', 4).
card_layout('galvanic juggernaut', 'normal').
card_power('galvanic juggernaut', 5).
card_toughness('galvanic juggernaut', 5).

% Found in: MRD
card_name('galvanic key', 'Galvanic Key').
card_type('galvanic key', 'Artifact').
card_types('galvanic key', ['Artifact']).
card_subtypes('galvanic key', []).
card_colors('galvanic key', []).
card_text('galvanic key', 'Flash\n{3}, {T}: Untap target artifact.').
card_mana_cost('galvanic key', ['2']).
card_cmc('galvanic key', 2).
card_layout('galvanic key', 'normal').

% Found in: DDJ, MBS
card_name('galvanoth', 'Galvanoth').
card_type('galvanoth', 'Creature — Beast').
card_types('galvanoth', ['Creature']).
card_subtypes('galvanoth', ['Beast']).
card_colors('galvanoth', ['R']).
card_text('galvanoth', 'At the beginning of your upkeep, you may look at the top card of your library. If it\'s an instant or sorcery card, you may cast it without paying its mana cost.').
card_mana_cost('galvanoth', ['3', 'R', 'R']).
card_cmc('galvanoth', 5).
card_layout('galvanoth', 'normal').
card_power('galvanoth', 3).
card_toughness('galvanoth', 3).

% Found in: USG, VMA
card_name('gamble', 'Gamble').
card_type('gamble', 'Sorcery').
card_types('gamble', ['Sorcery']).
card_subtypes('gamble', []).
card_colors('gamble', ['R']).
card_text('gamble', 'Search your library for a card, put that card into your hand, discard a card at random, then shuffle your library.').
card_mana_cost('gamble', ['R']).
card_cmc('gamble', 1).
card_layout('gamble', 'normal').

% Found in: 5ED, ICE
card_name('game of chaos', 'Game of Chaos').
card_type('game of chaos', 'Sorcery').
card_types('game of chaos', ['Sorcery']).
card_subtypes('game of chaos', []).
card_colors('game of chaos', ['R']).
card_text('game of chaos', 'Flip a coin. If you win the flip, you gain 1 life and target opponent loses 1 life, and you decide whether to flip again. If you lose the flip, you lose 1 life and that opponent gains 1 life, and that player decides whether to flip again. Double the life stakes with each flip.').
card_mana_cost('game of chaos', ['R', 'R', 'R']).
card_cmc('game of chaos', 3).
card_layout('game of chaos', 'normal').

% Found in: MMQ
card_name('game preserve', 'Game Preserve').
card_type('game preserve', 'Enchantment').
card_types('game preserve', ['Enchantment']).
card_subtypes('game preserve', []).
card_colors('game preserve', ['G']).
card_text('game preserve', 'At the beginning of your upkeep, each player reveals the top card of his or her library. If all cards revealed this way are creature cards, put those cards onto the battlefield under their owners\' control.').
card_mana_cost('game preserve', ['2', 'G']).
card_cmc('game preserve', 3).
card_layout('game preserve', 'normal').

% Found in: MOR
card_name('game-trail changeling', 'Game-Trail Changeling').
card_type('game-trail changeling', 'Creature — Shapeshifter').
card_types('game-trail changeling', ['Creature']).
card_subtypes('game-trail changeling', ['Shapeshifter']).
card_colors('game-trail changeling', ['G']).
card_text('game-trail changeling', 'Changeling (This card is every creature type.)\nTrample').
card_mana_cost('game-trail changeling', ['3', 'G', 'G']).
card_cmc('game-trail changeling', 5).
card_layout('game-trail changeling', 'normal').
card_power('game-trail changeling', 4).
card_toughness('game-trail changeling', 4).

% Found in: CNS, UDS
card_name('gamekeeper', 'Gamekeeper').
card_type('gamekeeper', 'Creature — Elf').
card_types('gamekeeper', ['Creature']).
card_subtypes('gamekeeper', ['Elf']).
card_colors('gamekeeper', ['G']).
card_text('gamekeeper', 'When Gamekeeper dies, you may exile it. If you do, reveal cards from the top of your library until you reveal a creature card. Put that card onto the battlefield and put all other cards revealed this way into your graveyard.').
card_mana_cost('gamekeeper', ['3', 'G']).
card_cmc('gamekeeper', 4).
card_layout('gamekeeper', 'normal').
card_power('gamekeeper', 2).
card_toughness('gamekeeper', 2).

% Found in: AVR, DDK
card_name('gang of devils', 'Gang of Devils').
card_type('gang of devils', 'Creature — Devil').
card_types('gang of devils', ['Creature']).
card_subtypes('gang of devils', ['Devil']).
card_colors('gang of devils', ['R']).
card_text('gang of devils', 'When Gang of Devils dies, it deals 3 damage divided as you choose among one, two, or three target creatures and/or players.').
card_mana_cost('gang of devils', ['5', 'R']).
card_cmc('gang of devils', 6).
card_layout('gang of devils', 'normal').
card_power('gang of devils', 3).
card_toughness('gang of devils', 3).

% Found in: 7ED, ULG
card_name('gang of elk', 'Gang of Elk').
card_type('gang of elk', 'Creature — Elk Beast').
card_types('gang of elk', ['Creature']).
card_subtypes('gang of elk', ['Elk', 'Beast']).
card_colors('gang of elk', ['G']).
card_text('gang of elk', 'Whenever Gang of Elk becomes blocked, it gets +2/+2 until end of turn for each creature blocking it.').
card_mana_cost('gang of elk', ['5', 'G']).
card_cmc('gang of elk', 6).
card_layout('gang of elk', 'normal').
card_power('gang of elk', 5).
card_toughness('gang of elk', 4).

% Found in: ONS
card_name('gangrenous goliath', 'Gangrenous Goliath').
card_type('gangrenous goliath', 'Creature — Zombie Giant').
card_types('gangrenous goliath', ['Creature']).
card_subtypes('gangrenous goliath', ['Zombie', 'Giant']).
card_colors('gangrenous goliath', ['B']).
card_text('gangrenous goliath', 'Tap three untapped Clerics you control: Return Gangrenous Goliath from your graveyard to your hand.').
card_mana_cost('gangrenous goliath', ['3', 'B', 'B']).
card_cmc('gangrenous goliath', 5).
card_layout('gangrenous goliath', 'normal').
card_power('gangrenous goliath', 4).
card_toughness('gangrenous goliath', 4).

% Found in: CST, ICE, ME2
card_name('gangrenous zombies', 'Gangrenous Zombies').
card_type('gangrenous zombies', 'Creature — Zombie').
card_types('gangrenous zombies', ['Creature']).
card_subtypes('gangrenous zombies', ['Zombie']).
card_colors('gangrenous zombies', ['B']).
card_text('gangrenous zombies', '{T}, Sacrifice Gangrenous Zombies: Gangrenous Zombies deals 1 damage to each creature and each player. If you control a snow Swamp, Gangrenous Zombies deals 2 damage to each creature and each player instead.').
card_mana_cost('gangrenous zombies', ['1', 'B', 'B']).
card_cmc('gangrenous zombies', 3).
card_layout('gangrenous zombies', 'normal').
card_power('gangrenous zombies', 2).
card_toughness('gangrenous zombies', 2).

% Found in: ALL, MED
card_name('gargantuan gorilla', 'Gargantuan Gorilla').
card_type('gargantuan gorilla', 'Creature — Ape').
card_types('gargantuan gorilla', ['Creature']).
card_subtypes('gargantuan gorilla', ['Ape']).
card_colors('gargantuan gorilla', ['G']).
card_text('gargantuan gorilla', 'At the beginning of your upkeep, you may sacrifice a Forest. If you sacrifice a snow Forest this way, Gargantuan Gorilla gains trample until end of turn. If you don\'t sacrifice a Forest, sacrifice Gargantuan Gorilla and it deals 7 damage to you.\n{T}: Gargantuan Gorilla deals damage equal to its power to another target creature. That creature deals damage equal to its power to Gargantuan Gorilla.').
card_mana_cost('gargantuan gorilla', ['4', 'G', 'G', 'G']).
card_cmc('gargantuan gorilla', 7).
card_layout('gargantuan gorilla', 'normal').
card_power('gargantuan gorilla', 7).
card_toughness('gargantuan gorilla', 7).
card_reserved('gargantuan gorilla').

% Found in: C14, M10
card_name('gargoyle castle', 'Gargoyle Castle').
card_type('gargoyle castle', 'Land').
card_types('gargoyle castle', ['Land']).
card_subtypes('gargoyle castle', []).
card_colors('gargoyle castle', []).
card_text('gargoyle castle', '{T}: Add {1} to your mana pool.\n{5}, {T}, Sacrifice Gargoyle Castle: Put a 3/4 colorless Gargoyle artifact creature token with flying onto the battlefield.').
card_layout('gargoyle castle', 'normal').

% Found in: M11, M15
card_name('gargoyle sentinel', 'Gargoyle Sentinel').
card_type('gargoyle sentinel', 'Artifact Creature — Gargoyle').
card_types('gargoyle sentinel', ['Artifact', 'Creature']).
card_subtypes('gargoyle sentinel', ['Gargoyle']).
card_colors('gargoyle sentinel', []).
card_text('gargoyle sentinel', 'Defender (This creature can\'t attack.)\n{3}: Until end of turn, Gargoyle Sentinel loses defender and gains flying.').
card_mana_cost('gargoyle sentinel', ['3']).
card_cmc('gargoyle sentinel', 3).
card_layout('gargoyle sentinel', 'normal').
card_power('gargoyle sentinel', 3).
card_toughness('gargoyle sentinel', 3).

% Found in: ISD
card_name('garruk relentless', 'Garruk Relentless').
card_type('garruk relentless', 'Planeswalker — Garruk').
card_types('garruk relentless', ['Planeswalker']).
card_subtypes('garruk relentless', ['Garruk']).
card_colors('garruk relentless', ['G']).
card_text('garruk relentless', 'When Garruk Relentless has two or fewer loyalty counters on him, transform him.\n0: Garruk Relentless deals 3 damage to target creature. That creature deals damage equal to its power to him.\n0: Put a 2/2 green Wolf creature token onto the battlefield.').
card_mana_cost('garruk relentless', ['3', 'G']).
card_cmc('garruk relentless', 4).
card_layout('garruk relentless', 'double-faced').
card_sides('garruk relentless', 'garruk, the veil-cursed').
card_loyalty('garruk relentless', 3).

% Found in: CMD, DD3_GVL, DDD, LRW, M10, M11, pMEI
card_name('garruk wildspeaker', 'Garruk Wildspeaker').
card_type('garruk wildspeaker', 'Planeswalker — Garruk').
card_types('garruk wildspeaker', ['Planeswalker']).
card_subtypes('garruk wildspeaker', ['Garruk']).
card_colors('garruk wildspeaker', ['G']).
card_text('garruk wildspeaker', '+1: Untap two target lands.\n−1: Put a 3/3 green Beast creature token onto the battlefield.\n−4: Creatures you control get +3/+3 and gain trample until end of turn.').
card_mana_cost('garruk wildspeaker', ['2', 'G', 'G']).
card_cmc('garruk wildspeaker', 4).
card_layout('garruk wildspeaker', 'normal').
card_loyalty('garruk wildspeaker', 3).

% Found in: M11, M12
card_name('garruk\'s companion', 'Garruk\'s Companion').
card_type('garruk\'s companion', 'Creature — Beast').
card_types('garruk\'s companion', ['Creature']).
card_subtypes('garruk\'s companion', ['Beast']).
card_colors('garruk\'s companion', ['G']).
card_text('garruk\'s companion', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('garruk\'s companion', ['G', 'G']).
card_cmc('garruk\'s companion', 2).
card_layout('garruk\'s companion', 'normal').
card_power('garruk\'s companion', 3).
card_toughness('garruk\'s companion', 2).

% Found in: M12, M14, pLPA
card_name('garruk\'s horde', 'Garruk\'s Horde').
card_type('garruk\'s horde', 'Creature — Beast').
card_types('garruk\'s horde', ['Creature']).
card_subtypes('garruk\'s horde', ['Beast']).
card_colors('garruk\'s horde', ['G']).
card_text('garruk\'s horde', 'Trample\nPlay with the top card of your library revealed.\nYou may cast the top card of your library if it\'s a creature card. (Do this only any time you could cast that creature card. You still pay the spell\'s costs.)').
card_mana_cost('garruk\'s horde', ['5', 'G', 'G']).
card_cmc('garruk\'s horde', 7).
card_layout('garruk\'s horde', 'normal').
card_power('garruk\'s horde', 7).
card_toughness('garruk\'s horde', 7).

% Found in: M11, M13, M15
card_name('garruk\'s packleader', 'Garruk\'s Packleader').
card_type('garruk\'s packleader', 'Creature — Beast').
card_types('garruk\'s packleader', ['Creature']).
card_subtypes('garruk\'s packleader', ['Beast']).
card_colors('garruk\'s packleader', ['G']).
card_text('garruk\'s packleader', 'Whenever another creature with power 3 or greater enters the battlefield under your control, you may draw a card.').
card_mana_cost('garruk\'s packleader', ['4', 'G']).
card_cmc('garruk\'s packleader', 5).
card_layout('garruk\'s packleader', 'normal').
card_power('garruk\'s packleader', 4).
card_toughness('garruk\'s packleader', 4).

% Found in: M15, pMEI
card_name('garruk, apex predator', 'Garruk, Apex Predator').
card_type('garruk, apex predator', 'Planeswalker — Garruk').
card_types('garruk, apex predator', ['Planeswalker']).
card_subtypes('garruk, apex predator', ['Garruk']).
card_colors('garruk, apex predator', ['B', 'G']).
card_text('garruk, apex predator', '+1: Destroy another target planeswalker.\n+1: Put a 3/3 black Beast creature token with deathtouch onto the battlefield.\n−3: Destroy target creature. You gain life equal to its toughness.\n−8: Target opponent gets an emblem with \"Whenever a creature attacks you, it gets +5/+5 and gains trample until end of turn.\"').
card_mana_cost('garruk, apex predator', ['5', 'B', 'G']).
card_cmc('garruk, apex predator', 7).
card_layout('garruk, apex predator', 'normal').
card_loyalty('garruk, apex predator', 5).

% Found in: M14, pMEI
card_name('garruk, caller of beasts', 'Garruk, Caller of Beasts').
card_type('garruk, caller of beasts', 'Planeswalker — Garruk').
card_types('garruk, caller of beasts', ['Planeswalker']).
card_subtypes('garruk, caller of beasts', ['Garruk']).
card_colors('garruk, caller of beasts', ['G']).
card_text('garruk, caller of beasts', '+1: Reveal the top five cards of your library. Put all creature cards revealed this way into your hand and the rest on the bottom of your library in any order.\n−3: You may put a green creature card from your hand onto the battlefield.\n−7: You get an emblem with \"Whenever you cast a creature spell, you may search your library for a creature card, put it onto the battlefield, then shuffle your library.\"').
card_mana_cost('garruk, caller of beasts', ['4', 'G', 'G']).
card_cmc('garruk, caller of beasts', 6).
card_layout('garruk, caller of beasts', 'normal').
card_loyalty('garruk, caller of beasts', 4).

% Found in: M12, M13
card_name('garruk, primal hunter', 'Garruk, Primal Hunter').
card_type('garruk, primal hunter', 'Planeswalker — Garruk').
card_types('garruk, primal hunter', ['Planeswalker']).
card_subtypes('garruk, primal hunter', ['Garruk']).
card_colors('garruk, primal hunter', ['G']).
card_text('garruk, primal hunter', '+1: Put a 3/3 green Beast creature token onto the battlefield.\n−3: Draw cards equal to the greatest power among creatures you control.\n−6: Put a 6/6 green Wurm creature token onto the battlefield for each land you control.').
card_mana_cost('garruk, primal hunter', ['2', 'G', 'G', 'G']).
card_cmc('garruk, primal hunter', 5).
card_layout('garruk, primal hunter', 'normal').
card_loyalty('garruk, primal hunter', 3).

% Found in: ISD
card_name('garruk, the veil-cursed', 'Garruk, the Veil-Cursed').
card_type('garruk, the veil-cursed', 'Planeswalker — Garruk').
card_types('garruk, the veil-cursed', ['Planeswalker']).
card_subtypes('garruk, the veil-cursed', ['Garruk']).
card_colors('garruk, the veil-cursed', ['B', 'G']).
card_text('garruk, the veil-cursed', '+1: Put a 1/1 black Wolf creature token with deathtouch onto the battlefield.\n−1: Sacrifice a creature. If you do, search your library for a creature card, reveal it, put it into your hand, then shuffle your library.\n−3: Creatures you control gain trample and get +X/+X until end of turn, where X is the number of creature cards in your graveyard.').
card_layout('garruk, the veil-cursed', 'double-faced').

% Found in: CSP
card_name('garza zol, plague queen', 'Garza Zol, Plague Queen').
card_type('garza zol, plague queen', 'Legendary Creature — Vampire').
card_types('garza zol, plague queen', ['Creature']).
card_subtypes('garza zol, plague queen', ['Vampire']).
card_supertypes('garza zol, plague queen', ['Legendary']).
card_colors('garza zol, plague queen', ['U', 'B', 'R']).
card_text('garza zol, plague queen', 'Flying, haste\nWhenever a creature dealt damage by Garza Zol, Plague Queen this turn dies, put a +1/+1 counter on Garza Zol.\nWhenever Garza Zol deals combat damage to a player, you may draw a card.').
card_mana_cost('garza zol, plague queen', ['4', 'U', 'B', 'R']).
card_cmc('garza zol, plague queen', 7).
card_layout('garza zol, plague queen', 'normal').
card_power('garza zol, plague queen', 5).
card_toughness('garza zol, plague queen', 5).

% Found in: CSP
card_name('garza\'s assassin', 'Garza\'s Assassin').
card_type('garza\'s assassin', 'Creature — Human Assassin').
card_types('garza\'s assassin', ['Creature']).
card_subtypes('garza\'s assassin', ['Human', 'Assassin']).
card_colors('garza\'s assassin', ['B']).
card_text('garza\'s assassin', 'Sacrifice Garza\'s Assassin: Destroy target nonblack creature.\nRecover—Pay half your life, rounded up. (When another creature is put into your graveyard from the battlefield, you may pay half your life, rounded up. If you do, return this card from your graveyard to your hand. Otherwise, exile this card.)').
card_mana_cost('garza\'s assassin', ['B', 'B', 'B']).
card_cmc('garza\'s assassin', 3).
card_layout('garza\'s assassin', 'normal').
card_power('garza\'s assassin', 2).
card_toughness('garza\'s assassin', 2).

% Found in: 4ED, 5ED, 6ED, BTD, LEG, TMP, TPR
card_name('gaseous form', 'Gaseous Form').
card_type('gaseous form', 'Enchantment — Aura').
card_types('gaseous form', ['Enchantment']).
card_subtypes('gaseous form', ['Aura']).
card_colors('gaseous form', ['U']).
card_text('gaseous form', 'Enchant creature\nPrevent all combat damage that would be dealt to and dealt by enchanted creature.').
card_mana_cost('gaseous form', ['2', 'U']).
card_cmc('gaseous form', 3).
card_layout('gaseous form', 'normal').

% Found in: RAV
card_name('gate hound', 'Gate Hound').
card_type('gate hound', 'Creature — Hound').
card_types('gate hound', ['Creature']).
card_subtypes('gate hound', ['Hound']).
card_colors('gate hound', ['W']).
card_text('gate hound', 'Creatures you control have vigilance as long as Gate Hound is enchanted.').
card_mana_cost('gate hound', ['2', 'W']).
card_cmc('gate hound', 3).
card_layout('gate hound', 'normal').
card_power('gate hound', 1).
card_toughness('gate hound', 1).

% Found in: DTK
card_name('gate smasher', 'Gate Smasher').
card_type('gate smasher', 'Artifact — Equipment').
card_types('gate smasher', ['Artifact']).
card_subtypes('gate smasher', ['Equipment']).
card_colors('gate smasher', []).
card_text('gate smasher', 'Gate Smasher can be attached only to a creature with toughness 4 or greater.\nEquipped creature gets +3/+0 and has trample.\nEquip {3}').
card_mana_cost('gate smasher', ['3']).
card_cmc('gate smasher', 3).
card_layout('gate smasher', 'normal').

% Found in: ATQ, ME4
card_name('gate to phyrexia', 'Gate to Phyrexia').
card_type('gate to phyrexia', 'Enchantment').
card_types('gate to phyrexia', ['Enchantment']).
card_subtypes('gate to phyrexia', []).
card_colors('gate to phyrexia', ['B']).
card_text('gate to phyrexia', 'Sacrifice a creature: Destroy target artifact. Activate this ability only during your upkeep and only once each turn.').
card_mana_cost('gate to phyrexia', ['B', 'B']).
card_cmc('gate to phyrexia', 2).
card_layout('gate to phyrexia', 'normal').
card_reserved('gate to phyrexia').

% Found in: MRD
card_name('gate to the æther', 'Gate to the Æther').
card_type('gate to the æther', 'Artifact').
card_types('gate to the æther', ['Artifact']).
card_subtypes('gate to the æther', []).
card_colors('gate to the æther', []).
card_text('gate to the æther', 'At the beginning of each player\'s upkeep, that player reveals the top card of his or her library. If it\'s an artifact, creature, enchantment, or land card, the player may put it onto the battlefield.').
card_mana_cost('gate to the æther', ['6']).
card_cmc('gate to the æther', 6).
card_layout('gate to the æther', 'normal').

% Found in: DDM, RTR
card_name('gatecreeper vine', 'Gatecreeper Vine').
card_type('gatecreeper vine', 'Creature — Plant').
card_types('gatecreeper vine', ['Creature']).
card_subtypes('gatecreeper vine', ['Plant']).
card_colors('gatecreeper vine', ['G']).
card_text('gatecreeper vine', 'Defender\nWhen Gatecreeper Vine enters the battlefield, you may search your library for a basic land card or a Gate card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('gatecreeper vine', ['1', 'G']).
card_cmc('gatecreeper vine', 2).
card_layout('gatecreeper vine', 'normal').
card_power('gatecreeper vine', 0).
card_toughness('gatecreeper vine', 2).

% Found in: DDK, ZEN, pFNM
card_name('gatekeeper of malakir', 'Gatekeeper of Malakir').
card_type('gatekeeper of malakir', 'Creature — Vampire Warrior').
card_types('gatekeeper of malakir', ['Creature']).
card_subtypes('gatekeeper of malakir', ['Vampire', 'Warrior']).
card_colors('gatekeeper of malakir', ['B']).
card_text('gatekeeper of malakir', 'Kicker {B} (You may pay an additional {B} as you cast this spell.)\nWhen Gatekeeper of Malakir enters the battlefield, if it was kicked, target player sacrifices a creature.').
card_mana_cost('gatekeeper of malakir', ['B', 'B']).
card_cmc('gatekeeper of malakir', 2).
card_layout('gatekeeper of malakir', 'normal').
card_power('gatekeeper of malakir', 2).
card_toughness('gatekeeper of malakir', 2).

% Found in: GTC
card_name('gateway shade', 'Gateway Shade').
card_type('gateway shade', 'Creature — Shade').
card_types('gateway shade', ['Creature']).
card_subtypes('gateway shade', ['Shade']).
card_colors('gateway shade', ['B']).
card_text('gateway shade', '{B}: Gateway Shade gets +1/+1 until end of turn.\nTap an untapped Gate you control: Gateway Shade gets +2/+2 until end of turn.').
card_mana_cost('gateway shade', ['2', 'B']).
card_cmc('gateway shade', 3).
card_layout('gateway shade', 'normal').
card_power('gateway shade', 1).
card_toughness('gateway shade', 1).

% Found in: ARC, FUT
card_name('gathan raiders', 'Gathan Raiders').
card_type('gathan raiders', 'Creature — Human Warrior').
card_types('gathan raiders', ['Creature']).
card_subtypes('gathan raiders', ['Human', 'Warrior']).
card_colors('gathan raiders', ['R']).
card_text('gathan raiders', 'Hellbent — Gathan Raiders gets +2/+2 as long as you have no cards in hand.\nMorph—Discard a card. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('gathan raiders', ['3', 'R', 'R']).
card_cmc('gathan raiders', 5).
card_layout('gathan raiders', 'normal').
card_power('gathan raiders', 3).
card_toughness('gathan raiders', 3).

% Found in: M15, RAV
card_name('gather courage', 'Gather Courage').
card_type('gather courage', 'Instant').
card_types('gather courage', ['Instant']).
card_subtypes('gather courage', []).
card_colors('gather courage', ['G']).
card_text('gather courage', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTarget creature gets +2/+2 until end of turn.').
card_mana_cost('gather courage', ['G']).
card_cmc('gather courage', 1).
card_layout('gather courage', 'normal').

% Found in: ALA
card_name('gather specimens', 'Gather Specimens').
card_type('gather specimens', 'Instant').
card_types('gather specimens', ['Instant']).
card_subtypes('gather specimens', []).
card_colors('gather specimens', ['U']).
card_text('gather specimens', 'If a creature would enter the battlefield under an opponent\'s control this turn, it enters the battlefield under your control instead.').
card_mana_cost('gather specimens', ['3', 'U', 'U', 'U']).
card_cmc('gather specimens', 6).
card_layout('gather specimens', 'normal').

% Found in: ORI
card_name('gather the pack', 'Gather the Pack').
card_type('gather the pack', 'Sorcery').
card_types('gather the pack', ['Sorcery']).
card_subtypes('gather the pack', []).
card_colors('gather the pack', ['G']).
card_text('gather the pack', 'Reveal the top five cards of your library. You may put a creature card from among them into your hand. Put the rest into your graveyard.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, put up to two creature cards from among the revealed cards into your hand instead of one.').
card_mana_cost('gather the pack', ['1', 'G']).
card_cmc('gather the pack', 2).
card_layout('gather the pack', 'normal').

% Found in: DKA, pWPN
card_name('gather the townsfolk', 'Gather the Townsfolk').
card_type('gather the townsfolk', 'Sorcery').
card_types('gather the townsfolk', ['Sorcery']).
card_subtypes('gather the townsfolk', []).
card_colors('gather the townsfolk', ['W']).
card_text('gather the townsfolk', 'Put two 1/1 white Human creature tokens onto the battlefield.\nFateful hour — If you have 5 or less life, put five of those tokens onto the battlefield instead.').
card_mana_cost('gather the townsfolk', ['1', 'W']).
card_cmc('gather the townsfolk', 2).
card_layout('gather the townsfolk', 'normal').

% Found in: GPT
card_name('gatherer of graces', 'Gatherer of Graces').
card_type('gatherer of graces', 'Creature — Human Druid').
card_types('gatherer of graces', ['Creature']).
card_subtypes('gatherer of graces', ['Human', 'Druid']).
card_colors('gatherer of graces', ['G']).
card_text('gatherer of graces', 'Gatherer of Graces gets +1/+1 for each Aura attached to it.\nSacrifice an Aura: Regenerate Gatherer of Graces.').
card_mana_cost('gatherer of graces', ['1', 'G']).
card_cmc('gatherer of graces', 2).
card_layout('gatherer of graces', 'normal').
card_power('gatherer of graces', 1).
card_toughness('gatherer of graces', 2).

% Found in: ISD
card_name('gatstaf howler', 'Gatstaf Howler').
card_type('gatstaf howler', 'Creature — Werewolf').
card_types('gatstaf howler', ['Creature']).
card_subtypes('gatstaf howler', ['Werewolf']).
card_colors('gatstaf howler', ['G']).
card_text('gatstaf howler', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Gatstaf Howler.').
card_layout('gatstaf howler', 'double-faced').
card_power('gatstaf howler', 3).
card_toughness('gatstaf howler', 3).

% Found in: ISD
card_name('gatstaf shepherd', 'Gatstaf Shepherd').
card_type('gatstaf shepherd', 'Creature — Human Werewolf').
card_types('gatstaf shepherd', ['Creature']).
card_subtypes('gatstaf shepherd', ['Human', 'Werewolf']).
card_colors('gatstaf shepherd', ['G']).
card_text('gatstaf shepherd', 'At the beginning of each upkeep, if no spells were cast last turn, transform Gatstaf Shepherd.').
card_mana_cost('gatstaf shepherd', ['1', 'G']).
card_cmc('gatstaf shepherd', 2).
card_layout('gatstaf shepherd', 'double-faced').
card_power('gatstaf shepherd', 2).
card_toughness('gatstaf shepherd', 2).
card_sides('gatstaf shepherd', 'gatstaf howler').

% Found in: 2ED, CED, CEI, LEA, LEB, ME4
card_name('gauntlet of might', 'Gauntlet of Might').
card_type('gauntlet of might', 'Artifact').
card_types('gauntlet of might', ['Artifact']).
card_subtypes('gauntlet of might', []).
card_colors('gauntlet of might', []).
card_text('gauntlet of might', 'Red creatures get +1/+1.\nWhenever a Mountain is tapped for mana, its controller adds {R} to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('gauntlet of might', ['4']).
card_cmc('gauntlet of might', 4).
card_layout('gauntlet of might', 'normal').
card_reserved('gauntlet of might').

% Found in: TSP
card_name('gauntlet of power', 'Gauntlet of Power').
card_type('gauntlet of power', 'Artifact').
card_types('gauntlet of power', ['Artifact']).
card_subtypes('gauntlet of power', []).
card_colors('gauntlet of power', []).
card_text('gauntlet of power', 'As Gauntlet of Power enters the battlefield, choose a color.\nCreatures of the chosen color get +1/+1.\nWhenever a basic land is tapped for mana of the chosen color, its controller adds one mana of that color to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('gauntlet of power', ['5']).
card_cmc('gauntlet of power', 5).
card_layout('gauntlet of power', 'normal').

% Found in: 5ED, CHR, LEG, ME3
card_name('gauntlets of chaos', 'Gauntlets of Chaos').
card_type('gauntlets of chaos', 'Artifact').
card_types('gauntlets of chaos', ['Artifact']).
card_subtypes('gauntlets of chaos', []).
card_colors('gauntlets of chaos', []).
card_text('gauntlets of chaos', '{5}, Sacrifice Gauntlets of Chaos: Exchange control of target artifact, creature, or land you control and target permanent an opponent controls that shares one of those types with it. If those permanents are exchanged this way, destroy all Auras attached to them.').
card_mana_cost('gauntlets of chaos', ['5']).
card_cmc('gauntlets of chaos', 5).
card_layout('gauntlets of chaos', 'normal').

% Found in: PC2
card_name('gavony', 'Gavony').
card_type('gavony', 'Plane — Innistrad').
card_types('gavony', ['Plane']).
card_subtypes('gavony', ['Innistrad']).
card_colors('gavony', []).
card_text('gavony', 'All creatures have vigilance.\nWhenever you roll {C}, creatures you control gain indestructible until end of turn.').
card_layout('gavony', 'plane').

% Found in: DKA
card_name('gavony ironwright', 'Gavony Ironwright').
card_type('gavony ironwright', 'Creature — Human Soldier').
card_types('gavony ironwright', ['Creature']).
card_subtypes('gavony ironwright', ['Human', 'Soldier']).
card_colors('gavony ironwright', ['W']).
card_text('gavony ironwright', 'Fateful hour — As long as you have 5 or less life, other creatures you control get +1/+4.').
card_mana_cost('gavony ironwright', ['2', 'W']).
card_cmc('gavony ironwright', 3).
card_layout('gavony ironwright', 'normal').
card_power('gavony ironwright', 1).
card_toughness('gavony ironwright', 4).

% Found in: ISD
card_name('gavony township', 'Gavony Township').
card_type('gavony township', 'Land').
card_types('gavony township', ['Land']).
card_subtypes('gavony township', []).
card_colors('gavony township', []).
card_text('gavony township', '{T}: Add {1} to your mana pool.\n{2}{G}{W}, {T}: Put a +1/+1 counter on each creature you control.').
card_layout('gavony township', 'normal').

% Found in: SOK
card_name('gaze of adamaro', 'Gaze of Adamaro').
card_type('gaze of adamaro', 'Instant — Arcane').
card_types('gaze of adamaro', ['Instant']).
card_subtypes('gaze of adamaro', ['Arcane']).
card_colors('gaze of adamaro', ['R']).
card_text('gaze of adamaro', 'Gaze of Adamaro deals damage to target player equal to the number of cards in that player\'s hand.').
card_mana_cost('gaze of adamaro', ['2', 'R', 'R']).
card_cmc('gaze of adamaro', 4).
card_layout('gaze of adamaro', 'normal').

% Found in: DGM, pMEI
card_name('gaze of granite', 'Gaze of Granite').
card_type('gaze of granite', 'Sorcery').
card_types('gaze of granite', ['Sorcery']).
card_subtypes('gaze of granite', []).
card_colors('gaze of granite', ['B', 'G']).
card_text('gaze of granite', 'Destroy each nonland permanent with converted mana cost X or less.').
card_mana_cost('gaze of granite', ['X', 'B', 'B', 'G']).
card_cmc('gaze of granite', 3).
card_layout('gaze of granite', 'normal').

% Found in: TSP
card_name('gaze of justice', 'Gaze of Justice').
card_type('gaze of justice', 'Sorcery').
card_types('gaze of justice', ['Sorcery']).
card_subtypes('gaze of justice', []).
card_colors('gaze of justice', ['W']).
card_text('gaze of justice', 'As an additional cost to cast Gaze of Justice, tap three untapped white creatures you control.\nExile target creature.\nFlashback {5}{W} (You may cast this card from your graveyard for its flashback cost and any additional costs. Then exile it.)').
card_mana_cost('gaze of justice', ['W']).
card_cmc('gaze of justice', 1).
card_layout('gaze of justice', 'normal').

% Found in: ICE
card_name('gaze of pain', 'Gaze of Pain').
card_type('gaze of pain', 'Sorcery').
card_types('gaze of pain', ['Sorcery']).
card_subtypes('gaze of pain', []).
card_colors('gaze of pain', ['B']).
card_text('gaze of pain', 'Until end of turn, whenever a creature you control attacks and isn\'t blocked, you may choose to have it deal damage equal to its power to a target creature. If you do, it assigns no combat damage this turn.').
card_mana_cost('gaze of pain', ['1', 'B']).
card_cmc('gaze of pain', 2).
card_layout('gaze of pain', 'normal').

% Found in: RAV
card_name('gaze of the gorgon', 'Gaze of the Gorgon').
card_type('gaze of the gorgon', 'Instant').
card_types('gaze of the gorgon', ['Instant']).
card_subtypes('gaze of the gorgon', []).
card_colors('gaze of the gorgon', ['B', 'G']).
card_text('gaze of the gorgon', '({B/G} can be paid with either {B} or {G}.)\nRegenerate target creature. At this turn\'s next end of combat, destroy all creatures that blocked or were blocked by it this turn.').
card_mana_cost('gaze of the gorgon', ['3', 'B/G']).
card_cmc('gaze of the gorgon', 4).
card_layout('gaze of the gorgon', 'normal').

% Found in: ISD, pWCQ
card_name('geist of saint traft', 'Geist of Saint Traft').
card_type('geist of saint traft', 'Legendary Creature — Spirit Cleric').
card_types('geist of saint traft', ['Creature']).
card_subtypes('geist of saint traft', ['Spirit', 'Cleric']).
card_supertypes('geist of saint traft', ['Legendary']).
card_colors('geist of saint traft', ['W', 'U']).
card_text('geist of saint traft', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nWhenever Geist of Saint Traft attacks, put a 4/4 white Angel creature token with flying onto the battlefield tapped and attacking. Exile that token at end of combat.').
card_mana_cost('geist of saint traft', ['1', 'W', 'U']).
card_cmc('geist of saint traft', 3).
card_layout('geist of saint traft', 'normal').
card_power('geist of saint traft', 2).
card_toughness('geist of saint traft', 2).

% Found in: M15
card_name('geist of the moors', 'Geist of the Moors').
card_type('geist of the moors', 'Creature — Spirit').
card_types('geist of the moors', ['Creature']).
card_subtypes('geist of the moors', ['Spirit']).
card_colors('geist of the moors', ['W']).
card_text('geist of the moors', 'Flying').
card_mana_cost('geist of the moors', ['1', 'W', 'W']).
card_cmc('geist of the moors', 3).
card_layout('geist of the moors', 'normal').
card_power('geist of the moors', 3).
card_toughness('geist of the moors', 1).

% Found in: AVR
card_name('geist snatch', 'Geist Snatch').
card_type('geist snatch', 'Instant').
card_types('geist snatch', ['Instant']).
card_subtypes('geist snatch', []).
card_colors('geist snatch', ['U']).
card_text('geist snatch', 'Counter target creature spell. Put a 1/1 blue Spirit creature token with flying onto the battlefield.').
card_mana_cost('geist snatch', ['2', 'U', 'U']).
card_cmc('geist snatch', 4).
card_layout('geist snatch', 'normal').

% Found in: AVR
card_name('geist trappers', 'Geist Trappers').
card_type('geist trappers', 'Creature — Human Warrior').
card_types('geist trappers', ['Creature']).
card_subtypes('geist trappers', ['Human', 'Warrior']).
card_colors('geist trappers', ['G']).
card_text('geist trappers', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Geist Trappers is paired with another creature, both creatures have reach.').
card_mana_cost('geist trappers', ['4', 'G']).
card_cmc('geist trappers', 5).
card_layout('geist trappers', 'normal').
card_power('geist trappers', 3).
card_toughness('geist trappers', 5).

% Found in: C14, ISD
card_name('geist-honored monk', 'Geist-Honored Monk').
card_type('geist-honored monk', 'Creature — Human Monk').
card_types('geist-honored monk', ['Creature']).
card_subtypes('geist-honored monk', ['Human', 'Monk']).
card_colors('geist-honored monk', ['W']).
card_text('geist-honored monk', 'Vigilance\nGeist-Honored Monk\'s power and toughness are each equal to the number of creatures you control.\nWhen Geist-Honored Monk enters the battlefield, put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_mana_cost('geist-honored monk', ['3', 'W', 'W']).
card_cmc('geist-honored monk', 5).
card_layout('geist-honored monk', 'normal').
card_power('geist-honored monk', '*').
card_toughness('geist-honored monk', '*').

% Found in: ISD
card_name('geistcatcher\'s rig', 'Geistcatcher\'s Rig').
card_type('geistcatcher\'s rig', 'Artifact Creature — Construct').
card_types('geistcatcher\'s rig', ['Artifact', 'Creature']).
card_subtypes('geistcatcher\'s rig', ['Construct']).
card_colors('geistcatcher\'s rig', []).
card_text('geistcatcher\'s rig', 'When Geistcatcher\'s Rig enters the battlefield, you may have it deal 4 damage to target creature with flying.').
card_mana_cost('geistcatcher\'s rig', ['6']).
card_cmc('geistcatcher\'s rig', 6).
card_layout('geistcatcher\'s rig', 'normal').
card_power('geistcatcher\'s rig', 4).
card_toughness('geistcatcher\'s rig', 5).

% Found in: DDK, ISD
card_name('geistflame', 'Geistflame').
card_type('geistflame', 'Instant').
card_types('geistflame', ['Instant']).
card_subtypes('geistflame', []).
card_colors('geistflame', ['R']).
card_text('geistflame', 'Geistflame deals 1 damage to target creature or player.\nFlashback {3}{R} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('geistflame', ['R']).
card_cmc('geistflame', 1).
card_layout('geistflame', 'normal').

% Found in: ROE
card_name('gelatinous genesis', 'Gelatinous Genesis').
card_type('gelatinous genesis', 'Sorcery').
card_types('gelatinous genesis', ['Sorcery']).
card_subtypes('gelatinous genesis', []).
card_colors('gelatinous genesis', ['G']).
card_text('gelatinous genesis', 'Put X X/X green Ooze creature tokens onto the battlefield.').
card_mana_cost('gelatinous genesis', ['X', 'X', 'G']).
card_cmc('gelatinous genesis', 1).
card_layout('gelatinous genesis', 'normal').

% Found in: DDJ, GPT
card_name('gelectrode', 'Gelectrode').
card_type('gelectrode', 'Creature — Weird').
card_types('gelectrode', ['Creature']).
card_subtypes('gelectrode', ['Weird']).
card_colors('gelectrode', ['U', 'R']).
card_text('gelectrode', '{T}: Gelectrode deals 1 damage to target creature or player.\nWhenever you cast an instant or sorcery spell, you may untap Gelectrode.').
card_mana_cost('gelectrode', ['1', 'U', 'R']).
card_cmc('gelectrode', 3).
card_layout('gelectrode', 'normal').
card_power('gelectrode', 0).
card_toughness('gelectrode', 1).

% Found in: CSP
card_name('gelid shackles', 'Gelid Shackles').
card_type('gelid shackles', 'Snow Enchantment — Aura').
card_types('gelid shackles', ['Enchantment']).
card_subtypes('gelid shackles', ['Aura']).
card_supertypes('gelid shackles', ['Snow']).
card_colors('gelid shackles', ['W']).
card_text('gelid shackles', 'Enchant creature\nEnchanted creature can\'t block, and its activated abilities can\'t be activated.\n{S}: Enchanted creature gains defender until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('gelid shackles', ['W']).
card_cmc('gelid shackles', 1).
card_layout('gelid shackles', 'normal').

% Found in: M13
card_name('gem of becoming', 'Gem of Becoming').
card_type('gem of becoming', 'Artifact').
card_types('gem of becoming', ['Artifact']).
card_subtypes('gem of becoming', []).
card_colors('gem of becoming', []).
card_text('gem of becoming', '{3}, {T}, Sacrifice Gem of Becoming: Search your library for an Island card, a Swamp card, and a Mountain card. Reveal those cards and put them into your hand. Then shuffle your library.').
card_mana_cost('gem of becoming', ['3']).
card_cmc('gem of becoming', 3).
card_layout('gem of becoming', 'normal').

% Found in: H09, TSP
card_name('gemhide sliver', 'Gemhide Sliver').
card_type('gemhide sliver', 'Creature — Sliver').
card_types('gemhide sliver', ['Creature']).
card_subtypes('gemhide sliver', ['Sliver']).
card_colors('gemhide sliver', ['G']).
card_text('gemhide sliver', 'All Slivers have \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('gemhide sliver', ['1', 'G']).
card_cmc('gemhide sliver', 2).
card_layout('gemhide sliver', 'normal').
card_power('gemhide sliver', 1).
card_toughness('gemhide sliver', 1).

% Found in: DST
card_name('gemini engine', 'Gemini Engine').
card_type('gemini engine', 'Artifact Creature — Construct').
card_types('gemini engine', ['Artifact', 'Creature']).
card_subtypes('gemini engine', ['Construct']).
card_colors('gemini engine', []).
card_text('gemini engine', 'Whenever Gemini Engine attacks, put a colorless Construct artifact creature token named Twin onto the battlefield attacking. Its power is equal to Gemini Engine\'s power and its toughness is equal to Gemini Engine\'s toughness. Sacrifice the token at end of combat.').
card_mana_cost('gemini engine', ['6']).
card_cmc('gemini engine', 6).
card_layout('gemini engine', 'normal').
card_power('gemini engine', 3).
card_toughness('gemini engine', 4).

% Found in: DDO, LGN
card_name('gempalm avenger', 'Gempalm Avenger').
card_type('gempalm avenger', 'Creature — Human Soldier').
card_types('gempalm avenger', ['Creature']).
card_subtypes('gempalm avenger', ['Human', 'Soldier']).
card_colors('gempalm avenger', ['W']).
card_text('gempalm avenger', 'Cycling {2}{W} ({2}{W}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Avenger, Soldier creatures get +1/+1 and gain first strike until end of turn.').
card_mana_cost('gempalm avenger', ['5', 'W']).
card_cmc('gempalm avenger', 6).
card_layout('gempalm avenger', 'normal').
card_power('gempalm avenger', 3).
card_toughness('gempalm avenger', 5).

% Found in: DD3_EVG, EVG, LGN
card_name('gempalm incinerator', 'Gempalm Incinerator').
card_type('gempalm incinerator', 'Creature — Goblin').
card_types('gempalm incinerator', ['Creature']).
card_subtypes('gempalm incinerator', ['Goblin']).
card_colors('gempalm incinerator', ['R']).
card_text('gempalm incinerator', 'Cycling {1}{R} ({1}{R}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Incinerator, you may have it deal X damage to target creature, where X is the number of Goblins on the battlefield.').
card_mana_cost('gempalm incinerator', ['2', 'R']).
card_cmc('gempalm incinerator', 3).
card_layout('gempalm incinerator', 'normal').
card_power('gempalm incinerator', 2).
card_toughness('gempalm incinerator', 1).

% Found in: LGN
card_name('gempalm polluter', 'Gempalm Polluter').
card_type('gempalm polluter', 'Creature — Zombie').
card_types('gempalm polluter', ['Creature']).
card_subtypes('gempalm polluter', ['Zombie']).
card_colors('gempalm polluter', ['B']).
card_text('gempalm polluter', 'Cycling {B}{B} ({B}{B}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Polluter, you may have target player lose life equal to the number of Zombies on the battlefield.').
card_mana_cost('gempalm polluter', ['5', 'B']).
card_cmc('gempalm polluter', 6).
card_layout('gempalm polluter', 'normal').
card_power('gempalm polluter', 4).
card_toughness('gempalm polluter', 3).

% Found in: LGN
card_name('gempalm sorcerer', 'Gempalm Sorcerer').
card_type('gempalm sorcerer', 'Creature — Human Wizard').
card_types('gempalm sorcerer', ['Creature']).
card_subtypes('gempalm sorcerer', ['Human', 'Wizard']).
card_colors('gempalm sorcerer', ['U']).
card_text('gempalm sorcerer', 'Cycling {2}{U} ({2}{U}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Sorcerer, Wizard creatures gain flying until end of turn.').
card_mana_cost('gempalm sorcerer', ['2', 'U']).
card_cmc('gempalm sorcerer', 3).
card_layout('gempalm sorcerer', 'normal').
card_power('gempalm sorcerer', 2).
card_toughness('gempalm sorcerer', 2).

% Found in: DD3_EVG, EVG, LGN
card_name('gempalm strider', 'Gempalm Strider').
card_type('gempalm strider', 'Creature — Elf').
card_types('gempalm strider', ['Creature']).
card_subtypes('gempalm strider', ['Elf']).
card_colors('gempalm strider', ['G']).
card_text('gempalm strider', 'Cycling {2}{G}{G} ({2}{G}{G}, Discard this card: Draw a card.)\nWhen you cycle Gempalm Strider, Elf creatures get +2/+2 until end of turn.').
card_mana_cost('gempalm strider', ['1', 'G']).
card_cmc('gempalm strider', 2).
card_layout('gempalm strider', 'normal').
card_power('gempalm strider', 2).
card_toughness('gempalm strider', 2).

% Found in: 5DN
card_name('gemstone array', 'Gemstone Array').
card_type('gemstone array', 'Artifact').
card_types('gemstone array', ['Artifact']).
card_subtypes('gemstone array', []).
card_colors('gemstone array', []).
card_text('gemstone array', '{2}: Put a charge counter on Gemstone Array.\nRemove a charge counter from Gemstone Array: Add one mana of any color to your mana pool.').
card_mana_cost('gemstone array', ['4']).
card_cmc('gemstone array', 4).
card_layout('gemstone array', 'normal').

% Found in: TSP
card_name('gemstone caverns', 'Gemstone Caverns').
card_type('gemstone caverns', 'Legendary Land').
card_types('gemstone caverns', ['Land']).
card_subtypes('gemstone caverns', []).
card_supertypes('gemstone caverns', ['Legendary']).
card_colors('gemstone caverns', []).
card_text('gemstone caverns', 'If Gemstone Caverns is in your opening hand and you\'re not playing first, you may begin the game with Gemstone Caverns on the battlefield with a luck counter on it. If you do, exile a card from your hand.\n{T}: Add {1} to your mana pool. If Gemstone Caverns has a luck counter on it, instead add one mana of any color to your mana pool.').
card_layout('gemstone caverns', 'normal').

% Found in: TSB, WTH, pJGP
card_name('gemstone mine', 'Gemstone Mine').
card_type('gemstone mine', 'Land').
card_types('gemstone mine', ['Land']).
card_subtypes('gemstone mine', []).
card_colors('gemstone mine', []).
card_text('gemstone mine', 'Gemstone Mine enters the battlefield with three mining counters on it.\n{T}, Remove a mining counter from Gemstone Mine: Add one mana of any color to your mana pool. If there are no mining counters on Gemstone Mine, sacrifice it.').
card_layout('gemstone mine', 'normal').

% Found in: ICE
card_name('general jarkeld', 'General Jarkeld').
card_type('general jarkeld', 'Legendary Creature — Human Soldier').
card_types('general jarkeld', ['Creature']).
card_subtypes('general jarkeld', ['Human', 'Soldier']).
card_supertypes('general jarkeld', ['Legendary']).
card_colors('general jarkeld', ['W']).
card_text('general jarkeld', '{T}: Switch the blocking creatures of two target attacking creatures. Activate this ability only during the declare blockers step.').
card_mana_cost('general jarkeld', ['3', 'W']).
card_cmc('general jarkeld', 4).
card_layout('general jarkeld', 'normal').
card_power('general jarkeld', 1).
card_toughness('general jarkeld', 2).
card_reserved('general jarkeld').

% Found in: CHK
card_name('general\'s kabuto', 'General\'s Kabuto').
card_type('general\'s kabuto', 'Artifact — Equipment').
card_types('general\'s kabuto', ['Artifact']).
card_subtypes('general\'s kabuto', ['Equipment']).
card_colors('general\'s kabuto', []).
card_text('general\'s kabuto', 'Equipped creature has shroud. (It can\'t be the target of spells or abilities.)\nPrevent all combat damage that would be dealt to equipped creature.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('general\'s kabuto', ['4']).
card_cmc('general\'s kabuto', 4).
card_layout('general\'s kabuto', 'normal').

% Found in: MMQ
card_name('general\'s regalia', 'General\'s Regalia').
card_type('general\'s regalia', 'Artifact').
card_types('general\'s regalia', ['Artifact']).
card_subtypes('general\'s regalia', []).
card_colors('general\'s regalia', []).
card_text('general\'s regalia', '{3}: The next time a source of your choice would deal damage to you this turn, that damage is dealt to target creature you control instead.').
card_mana_cost('general\'s regalia', ['3']).
card_cmc('general\'s regalia', 3).
card_layout('general\'s regalia', 'normal').

% Found in: M15
card_name('generator servant', 'Generator Servant').
card_type('generator servant', 'Creature — Elemental').
card_types('generator servant', ['Creature']).
card_subtypes('generator servant', ['Elemental']).
card_colors('generator servant', ['R']).
card_text('generator servant', '{T}, Sacrifice Generator Servant: Add {2} to your mana pool. If that mana is spent on a creature spell, it gains haste until end of turn. (That creature can attack and {T} as soon as it comes under your control.)').
card_mana_cost('generator servant', ['1', 'R']).
card_cmc('generator servant', 2).
card_layout('generator servant', 'normal').
card_power('generator servant', 2).
card_toughness('generator servant', 1).

% Found in: JUD, VMA, pJGP
card_name('genesis', 'Genesis').
card_type('genesis', 'Creature — Incarnation').
card_types('genesis', ['Creature']).
card_subtypes('genesis', ['Incarnation']).
card_colors('genesis', ['G']).
card_text('genesis', 'At the beginning of your upkeep, if Genesis is in your graveyard, you may pay {2}{G}. If you do, return target creature card from your graveyard to your hand.').
card_mana_cost('genesis', ['4', 'G']).
card_cmc('genesis', 5).
card_layout('genesis', 'normal').
card_power('genesis', 4).
card_toughness('genesis', 4).

% Found in: DST
card_name('genesis chamber', 'Genesis Chamber').
card_type('genesis chamber', 'Artifact').
card_types('genesis chamber', ['Artifact']).
card_subtypes('genesis chamber', []).
card_colors('genesis chamber', []).
card_text('genesis chamber', 'Whenever a nontoken creature enters the battlefield, if Genesis Chamber is untapped, that creature\'s controller puts a 1/1 colorless Myr artifact creature token onto the battlefield.').
card_mana_cost('genesis chamber', ['2']).
card_cmc('genesis chamber', 2).
card_layout('genesis chamber', 'normal').

% Found in: M15
card_name('genesis hydra', 'Genesis Hydra').
card_type('genesis hydra', 'Creature — Plant Hydra').
card_types('genesis hydra', ['Creature']).
card_subtypes('genesis hydra', ['Plant', 'Hydra']).
card_colors('genesis hydra', ['G']).
card_text('genesis hydra', 'When you cast Genesis Hydra, reveal the top X cards of your library. You may put a nonland permanent card with converted mana cost X or less from among them onto the battlefield. Then shuffle the rest into your library.\nGenesis Hydra enters the battlefield with X +1/+1 counters on it.').
card_mana_cost('genesis hydra', ['X', 'G', 'G']).
card_cmc('genesis hydra', 2).
card_layout('genesis hydra', 'normal').
card_power('genesis hydra', 0).
card_toughness('genesis hydra', 0).

% Found in: SOM
card_name('genesis wave', 'Genesis Wave').
card_type('genesis wave', 'Sorcery').
card_types('genesis wave', ['Sorcery']).
card_subtypes('genesis wave', []).
card_colors('genesis wave', ['G']).
card_text('genesis wave', 'Reveal the top X cards of your library. You may put any number of permanent cards with converted mana cost X or less from among them onto the battlefield. Then put all cards revealed this way that weren\'t put onto the battlefield into your graveyard.').
card_mana_cost('genesis wave', ['X', 'G', 'G', 'G']).
card_cmc('genesis wave', 3).
card_layout('genesis wave', 'normal').

% Found in: BOK, DD3_GVL, DDD
card_name('genju of the cedars', 'Genju of the Cedars').
card_type('genju of the cedars', 'Enchantment — Aura').
card_types('genju of the cedars', ['Enchantment']).
card_subtypes('genju of the cedars', ['Aura']).
card_colors('genju of the cedars', ['G']).
card_text('genju of the cedars', 'Enchant Forest\n{2}: Enchanted Forest becomes a 4/4 green Spirit creature until end of turn. It\'s still a land.\nWhen enchanted Forest is put into a graveyard, you may return Genju of the Cedars from your graveyard to your hand.').
card_mana_cost('genju of the cedars', ['G']).
card_cmc('genju of the cedars', 1).
card_layout('genju of the cedars', 'normal').

% Found in: BOK
card_name('genju of the falls', 'Genju of the Falls').
card_type('genju of the falls', 'Enchantment — Aura').
card_types('genju of the falls', ['Enchantment']).
card_subtypes('genju of the falls', ['Aura']).
card_colors('genju of the falls', ['U']).
card_text('genju of the falls', 'Enchant Island\n{2}: Enchanted Island becomes a 3/2 blue Spirit creature with flying until end of turn. It\'s still a land.\nWhen enchanted Island is put into a graveyard, you may return Genju of the Falls from your graveyard to your hand.').
card_mana_cost('genju of the falls', ['U']).
card_cmc('genju of the falls', 1).
card_layout('genju of the falls', 'normal').

% Found in: BOK, DD3_GVL, DDD
card_name('genju of the fens', 'Genju of the Fens').
card_type('genju of the fens', 'Enchantment — Aura').
card_types('genju of the fens', ['Enchantment']).
card_subtypes('genju of the fens', ['Aura']).
card_colors('genju of the fens', ['B']).
card_text('genju of the fens', 'Enchant Swamp\n{2}: Until end of turn, enchanted Swamp becomes a 2/2 black Spirit creature with \"{B}: This creature gets +1/+1 until end of turn.\" It\'s still a land.\nWhen enchanted Swamp is put into a graveyard, you may return Genju of the Fens from your graveyard to your hand.').
card_mana_cost('genju of the fens', ['B']).
card_cmc('genju of the fens', 1).
card_layout('genju of the fens', 'normal').

% Found in: BOK
card_name('genju of the fields', 'Genju of the Fields').
card_type('genju of the fields', 'Enchantment — Aura').
card_types('genju of the fields', ['Enchantment']).
card_subtypes('genju of the fields', ['Aura']).
card_colors('genju of the fields', ['W']).
card_text('genju of the fields', 'Enchant Plains\n{2}: Until end of turn, enchanted Plains becomes a 2/5 white Spirit creature with \"Whenever this creature deals damage, its controller gains that much life.\" It\'s still a land.\nWhen enchanted Plains is put into a graveyard, you may return Genju of the Fields from your graveyard to your hand.').
card_mana_cost('genju of the fields', ['W']).
card_cmc('genju of the fields', 1).
card_layout('genju of the fields', 'normal').

% Found in: BOK
card_name('genju of the realm', 'Genju of the Realm').
card_type('genju of the realm', 'Legendary Enchantment — Aura').
card_types('genju of the realm', ['Enchantment']).
card_subtypes('genju of the realm', ['Aura']).
card_supertypes('genju of the realm', ['Legendary']).
card_colors('genju of the realm', ['W', 'U', 'B', 'R', 'G']).
card_text('genju of the realm', 'Enchant land\n{2}: Enchanted land becomes a legendary 8/12 Spirit creature with trample until end of turn. It\'s still a land.\nWhen enchanted land is put into a graveyard, you may return Genju of the Realm from your graveyard to your hand.').
card_mana_cost('genju of the realm', ['W', 'U', 'B', 'R', 'G']).
card_cmc('genju of the realm', 5).
card_layout('genju of the realm', 'normal').

% Found in: BOK, pARL
card_name('genju of the spires', 'Genju of the Spires').
card_type('genju of the spires', 'Enchantment — Aura').
card_types('genju of the spires', ['Enchantment']).
card_subtypes('genju of the spires', ['Aura']).
card_colors('genju of the spires', ['R']).
card_text('genju of the spires', 'Enchant Mountain\n{2}: Enchanted Mountain becomes a 6/1 red Spirit creature until end of turn. It\'s still a land.\nWhen enchanted Mountain is put into a graveyard, you may return Genju of the Spires from your graveyard to your hand.').
card_mana_cost('genju of the spires', ['R']).
card_cmc('genju of the spires', 1).
card_layout('genju of the spires', 'normal').

% Found in: NPH
card_name('geosurge', 'Geosurge').
card_type('geosurge', 'Sorcery').
card_types('geosurge', ['Sorcery']).
card_subtypes('geosurge', []).
card_colors('geosurge', ['R']).
card_text('geosurge', 'Add {R}{R}{R}{R}{R}{R}{R} to your mana pool. Spend this mana only to cast artifact or creature spells.').
card_mana_cost('geosurge', ['R', 'R', 'R', 'R']).
card_cmc('geosurge', 4).
card_layout('geosurge', 'normal').

% Found in: INV
card_name('geothermal crevice', 'Geothermal Crevice').
card_type('geothermal crevice', 'Land').
card_types('geothermal crevice', ['Land']).
card_subtypes('geothermal crevice', []).
card_colors('geothermal crevice', []).
card_text('geothermal crevice', 'Geothermal Crevice enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{T}, Sacrifice Geothermal Crevice: Add {B}{G} to your mana pool.').
card_layout('geothermal crevice', 'normal').

% Found in: DKA
card_name('geralf\'s messenger', 'Geralf\'s Messenger').
card_type('geralf\'s messenger', 'Creature — Zombie').
card_types('geralf\'s messenger', ['Creature']).
card_subtypes('geralf\'s messenger', ['Zombie']).
card_colors('geralf\'s messenger', ['B']).
card_text('geralf\'s messenger', 'Geralf\'s Messenger enters the battlefield tapped.\nWhen Geralf\'s Messenger enters the battlefield, target opponent loses 2 life.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('geralf\'s messenger', ['B', 'B', 'B']).
card_cmc('geralf\'s messenger', 3).
card_layout('geralf\'s messenger', 'normal').
card_power('geralf\'s messenger', 3).
card_toughness('geralf\'s messenger', 2).

% Found in: DKA
card_name('geralf\'s mindcrusher', 'Geralf\'s Mindcrusher').
card_type('geralf\'s mindcrusher', 'Creature — Zombie Horror').
card_types('geralf\'s mindcrusher', ['Creature']).
card_subtypes('geralf\'s mindcrusher', ['Zombie', 'Horror']).
card_colors('geralf\'s mindcrusher', ['U']).
card_text('geralf\'s mindcrusher', 'When Geralf\'s Mindcrusher enters the battlefield, target player puts the top five cards of his or her library into his or her graveyard.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('geralf\'s mindcrusher', ['4', 'U', 'U']).
card_cmc('geralf\'s mindcrusher', 6).
card_layout('geralf\'s mindcrusher', 'normal').
card_power('geralf\'s mindcrusher', 5).
card_toughness('geralf\'s mindcrusher', 5).

% Found in: VAN
card_name('gerrard', 'Gerrard').
card_type('gerrard', 'Vanguard').
card_types('gerrard', ['Vanguard']).
card_subtypes('gerrard', []).
card_colors('gerrard', []).
card_text('gerrard', 'At the beginning of your draw step, draw an additional card.').
card_layout('gerrard', 'vanguard').

% Found in: APC, DDE
card_name('gerrard capashen', 'Gerrard Capashen').
card_type('gerrard capashen', 'Legendary Creature — Human Soldier').
card_types('gerrard capashen', ['Creature']).
card_subtypes('gerrard capashen', ['Human', 'Soldier']).
card_supertypes('gerrard capashen', ['Legendary']).
card_colors('gerrard capashen', ['W']).
card_text('gerrard capashen', 'At the beginning of your upkeep, you gain 1 life for each card in target opponent\'s hand.\n{3}{W}: Tap target creature. Activate this ability only if Gerrard Capashen is attacking.').
card_mana_cost('gerrard capashen', ['3', 'W', 'W']).
card_cmc('gerrard capashen', 5).
card_layout('gerrard capashen', 'normal').
card_power('gerrard capashen', 3).
card_toughness('gerrard capashen', 4).

% Found in: TMP, TPR, VMA
card_name('gerrard\'s battle cry', 'Gerrard\'s Battle Cry').
card_type('gerrard\'s battle cry', 'Enchantment').
card_types('gerrard\'s battle cry', ['Enchantment']).
card_subtypes('gerrard\'s battle cry', []).
card_colors('gerrard\'s battle cry', ['W']).
card_text('gerrard\'s battle cry', '{2}{W}: Creatures you control get +1/+1 until end of turn.').
card_mana_cost('gerrard\'s battle cry', ['W']).
card_cmc('gerrard\'s battle cry', 1).
card_layout('gerrard\'s battle cry', 'normal').

% Found in: DDE, PLS
card_name('gerrard\'s command', 'Gerrard\'s Command').
card_type('gerrard\'s command', 'Instant').
card_types('gerrard\'s command', ['Instant']).
card_subtypes('gerrard\'s command', []).
card_colors('gerrard\'s command', ['W', 'G']).
card_text('gerrard\'s command', 'Untap target creature. It gets +3/+3 until end of turn.').
card_mana_cost('gerrard\'s command', ['G', 'W']).
card_cmc('gerrard\'s command', 2).
card_layout('gerrard\'s command', 'normal').

% Found in: MMQ
card_name('gerrard\'s irregulars', 'Gerrard\'s Irregulars').
card_type('gerrard\'s irregulars', 'Creature — Human Soldier').
card_types('gerrard\'s irregulars', ['Creature']).
card_subtypes('gerrard\'s irregulars', ['Human', 'Soldier']).
card_colors('gerrard\'s irregulars', ['R']).
card_text('gerrard\'s irregulars', 'Trample, haste').
card_mana_cost('gerrard\'s irregulars', ['4', 'R']).
card_cmc('gerrard\'s irregulars', 5).
card_layout('gerrard\'s irregulars', 'normal').
card_power('gerrard\'s irregulars', 4).
card_toughness('gerrard\'s irregulars', 2).

% Found in: APC, pFNM
card_name('gerrard\'s verdict', 'Gerrard\'s Verdict').
card_type('gerrard\'s verdict', 'Sorcery').
card_types('gerrard\'s verdict', ['Sorcery']).
card_subtypes('gerrard\'s verdict', []).
card_colors('gerrard\'s verdict', ['W', 'B']).
card_text('gerrard\'s verdict', 'Target player discards two cards. You gain 3 life for each land card discarded this way.').
card_mana_cost('gerrard\'s verdict', ['W', 'B']).
card_cmc('gerrard\'s verdict', 2).
card_layout('gerrard\'s verdict', 'normal').

% Found in: 7ED, S99, WTH
card_name('gerrard\'s wisdom', 'Gerrard\'s Wisdom').
card_type('gerrard\'s wisdom', 'Sorcery').
card_types('gerrard\'s wisdom', ['Sorcery']).
card_subtypes('gerrard\'s wisdom', []).
card_colors('gerrard\'s wisdom', ['W']).
card_text('gerrard\'s wisdom', 'You gain 2 life for each card in your hand.').
card_mana_cost('gerrard\'s wisdom', ['2', 'W', 'W']).
card_cmc('gerrard\'s wisdom', 4).
card_layout('gerrard\'s wisdom', 'normal').

% Found in: UGL
card_name('gerrymandering', 'Gerrymandering').
card_type('gerrymandering', 'Sorcery').
card_types('gerrymandering', ['Sorcery']).
card_subtypes('gerrymandering', []).
card_colors('gerrymandering', ['G']).
card_text('gerrymandering', 'Remove all lands from play and shuffle them together. Randomly deal to each player one land card for each land he or she had before. Each player puts those lands into play under his or her control, untapped.').
card_mana_cost('gerrymandering', ['2', 'G']).
card_cmc('gerrymandering', 3).
card_layout('gerrymandering', 'normal').

% Found in: UGL
card_name('get a life', 'Get a Life').
card_type('get a life', 'Instant').
card_types('get a life', ['Instant']).
card_subtypes('get a life', []).
card_colors('get a life', ['W']).
card_text('get a life', 'Target player and each of his or her teammates exchange life totals.').
card_mana_cost('get a life', ['W']).
card_cmc('get a life', 1).
card_layout('get a life', 'normal').

% Found in: DST
card_name('geth\'s grimoire', 'Geth\'s Grimoire').
card_type('geth\'s grimoire', 'Artifact').
card_types('geth\'s grimoire', ['Artifact']).
card_subtypes('geth\'s grimoire', []).
card_colors('geth\'s grimoire', []).
card_text('geth\'s grimoire', 'Whenever an opponent discards a card, you may draw a card.').
card_mana_cost('geth\'s grimoire', ['4']).
card_cmc('geth\'s grimoire', 4).
card_layout('geth\'s grimoire', 'normal').

% Found in: NPH
card_name('geth\'s verdict', 'Geth\'s Verdict').
card_type('geth\'s verdict', 'Instant').
card_types('geth\'s verdict', ['Instant']).
card_subtypes('geth\'s verdict', []).
card_colors('geth\'s verdict', ['B']).
card_text('geth\'s verdict', 'Target player sacrifices a creature and loses 1 life.').
card_mana_cost('geth\'s verdict', ['B', 'B']).
card_cmc('geth\'s verdict', 2).
card_layout('geth\'s verdict', 'normal').

% Found in: SOM
card_name('geth, lord of the vault', 'Geth, Lord of the Vault').
card_type('geth, lord of the vault', 'Legendary Creature — Zombie').
card_types('geth, lord of the vault', ['Creature']).
card_subtypes('geth, lord of the vault', ['Zombie']).
card_supertypes('geth, lord of the vault', ['Legendary']).
card_colors('geth, lord of the vault', ['B']).
card_text('geth, lord of the vault', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\n{X}{B}: Put target artifact or creature card with converted mana cost X from an opponent\'s graveyard onto the battlefield under your control tapped. Then that player puts the top X cards of his or her library into his or her graveyard.').
card_mana_cost('geth, lord of the vault', ['4', 'B', 'B']).
card_cmc('geth, lord of the vault', 6).
card_layout('geth, lord of the vault', 'normal').
card_power('geth, lord of the vault', 5).
card_toughness('geth, lord of the vault', 5).

% Found in: DDI, ZEN
card_name('geyser glider', 'Geyser Glider').
card_type('geyser glider', 'Creature — Elemental Beast').
card_types('geyser glider', ['Creature']).
card_subtypes('geyser glider', ['Elemental', 'Beast']).
card_colors('geyser glider', ['R']).
card_text('geyser glider', 'Landfall — Whenever a land enters the battlefield under your control, Geyser Glider gains flying until end of turn.').
card_mana_cost('geyser glider', ['3', 'R', 'R']).
card_cmc('geyser glider', 5).
card_layout('geyser glider', 'normal').
card_power('geyser glider', 4).
card_toughness('geyser glider', 4).

% Found in: BFZ
card_name('geyserfield stalker', 'Geyserfield Stalker').
card_type('geyserfield stalker', 'Creature — Elemental').
card_types('geyserfield stalker', ['Creature']).
card_subtypes('geyserfield stalker', ['Elemental']).
card_colors('geyserfield stalker', ['B']).
card_text('geyserfield stalker', 'Menace (This creature can\'t be blocked except by two or more creatures.)\nLandfall — Whenever a land enters the battlefield under your control, Geyserfield Stalker gets +2/+2 until end of turn.').
card_mana_cost('geyserfield stalker', ['4', 'B']).
card_cmc('geyserfield stalker', 5).
card_layout('geyserfield stalker', 'normal').
card_power('geyserfield stalker', 3).
card_toughness('geyserfield stalker', 2).

% Found in: SOM
card_name('ghalma\'s warden', 'Ghalma\'s Warden').
card_type('ghalma\'s warden', 'Creature — Elephant Soldier').
card_types('ghalma\'s warden', ['Creature']).
card_subtypes('ghalma\'s warden', ['Elephant', 'Soldier']).
card_colors('ghalma\'s warden', ['W']).
card_text('ghalma\'s warden', 'Metalcraft — Ghalma\'s Warden gets +2/+2 as long as you control three or more artifacts.').
card_mana_cost('ghalma\'s warden', ['3', 'W']).
card_cmc('ghalma\'s warden', 4).
card_layout('ghalma\'s warden', 'normal').
card_power('ghalma\'s warden', 2).
card_toughness('ghalma\'s warden', 4).

% Found in: SHM
card_name('ghastlord of fugue', 'Ghastlord of Fugue').
card_type('ghastlord of fugue', 'Creature — Spirit Avatar').
card_types('ghastlord of fugue', ['Creature']).
card_subtypes('ghastlord of fugue', ['Spirit', 'Avatar']).
card_colors('ghastlord of fugue', ['U', 'B']).
card_text('ghastlord of fugue', 'Ghastlord of Fugue can\'t be blocked.\nWhenever Ghastlord of Fugue deals combat damage to a player, that player reveals his or her hand. You choose a card from it. That player exiles that card.').
card_mana_cost('ghastlord of fugue', ['U/B', 'U/B', 'U/B', 'U/B', 'U/B']).
card_cmc('ghastlord of fugue', 5).
card_layout('ghastlord of fugue', 'normal').
card_power('ghastlord of fugue', 4).
card_toughness('ghastlord of fugue', 4).

% Found in: FRF
card_name('ghastly conscription', 'Ghastly Conscription').
card_type('ghastly conscription', 'Sorcery').
card_types('ghastly conscription', ['Sorcery']).
card_subtypes('ghastly conscription', []).
card_colors('ghastly conscription', ['B']).
card_text('ghastly conscription', 'Exile all creature cards from target player\'s graveyard in a face-down pile, shuffle that pile, then manifest those cards. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('ghastly conscription', ['5', 'B', 'B']).
card_cmc('ghastly conscription', 7).
card_layout('ghastly conscription', 'normal').

% Found in: ODY
card_name('ghastly demise', 'Ghastly Demise').
card_type('ghastly demise', 'Instant').
card_types('ghastly demise', ['Instant']).
card_subtypes('ghastly demise', []).
card_colors('ghastly demise', ['B']).
card_text('ghastly demise', 'Destroy target nonblack creature if its toughness is less than or equal to the number of cards in your graveyard.').
card_mana_cost('ghastly demise', ['B']).
card_cmc('ghastly demise', 1).
card_layout('ghastly demise', 'normal').

% Found in: SHM
card_name('ghastly discovery', 'Ghastly Discovery').
card_type('ghastly discovery', 'Sorcery').
card_types('ghastly discovery', ['Sorcery']).
card_subtypes('ghastly discovery', []).
card_colors('ghastly discovery', ['U']).
card_text('ghastly discovery', 'Draw two cards, then discard a card.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it.)').
card_mana_cost('ghastly discovery', ['2', 'U']).
card_cmc('ghastly discovery', 3).
card_layout('ghastly discovery', 'normal').

% Found in: DKA
card_name('ghastly haunting', 'Ghastly Haunting').
card_type('ghastly haunting', 'Enchantment — Aura').
card_types('ghastly haunting', ['Enchantment']).
card_subtypes('ghastly haunting', ['Aura']).
card_colors('ghastly haunting', ['U']).
card_text('ghastly haunting', 'Enchant creature\nYou control enchanted creature.').
card_layout('ghastly haunting', 'double-faced').

% Found in: LGN
card_name('ghastly remains', 'Ghastly Remains').
card_type('ghastly remains', 'Creature — Zombie').
card_types('ghastly remains', ['Creature']).
card_subtypes('ghastly remains', ['Zombie']).
card_colors('ghastly remains', ['B']).
card_text('ghastly remains', 'Amplify 1 (As this creature enters the battlefield, put a +1/+1 counter on it for each Zombie card you reveal in your hand.)\nAt the beginning of your upkeep, if Ghastly Remains is in your graveyard, you may pay {B}{B}{B}. If you do, return Ghastly Remains to your hand.').
card_mana_cost('ghastly remains', ['B', 'B', 'B']).
card_cmc('ghastly remains', 3).
card_layout('ghastly remains', 'normal').
card_power('ghastly remains', 0).
card_toughness('ghastly remains', 0).

% Found in: CMD
card_name('ghave, guru of spores', 'Ghave, Guru of Spores').
card_type('ghave, guru of spores', 'Legendary Creature — Fungus Shaman').
card_types('ghave, guru of spores', ['Creature']).
card_subtypes('ghave, guru of spores', ['Fungus', 'Shaman']).
card_supertypes('ghave, guru of spores', ['Legendary']).
card_colors('ghave, guru of spores', ['W', 'B', 'G']).
card_text('ghave, guru of spores', 'Ghave, Guru of Spores enters the battlefield with five +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from a creature you control: Put a 1/1 green Saproling creature token onto the battlefield.\n{1}, Sacrifice a creature: Put a +1/+1 counter on target creature.').
card_mana_cost('ghave, guru of spores', ['2', 'B', 'G', 'W']).
card_cmc('ghave, guru of spores', 5).
card_layout('ghave, guru of spores', 'normal').
card_power('ghave, guru of spores', 0).
card_toughness('ghave, guru of spores', 0).

% Found in: 5ED, ARN, CHR, MED
card_name('ghazbán ogre', 'Ghazbán Ogre').
card_type('ghazbán ogre', 'Creature — Ogre').
card_types('ghazbán ogre', ['Creature']).
card_subtypes('ghazbán ogre', ['Ogre']).
card_colors('ghazbán ogre', ['G']).
card_text('ghazbán ogre', 'At the beginning of your upkeep, if a player has more life than each other player, the player with the most life gains control of Ghazbán Ogre.').
card_mana_cost('ghazbán ogre', ['G']).
card_cmc('ghazbán ogre', 1).
card_layout('ghazbán ogre', 'normal').
card_power('ghazbán ogre', 2).
card_toughness('ghazbán ogre', 2).

% Found in: UGL
card_name('ghazbán ogress', 'Ghazbán Ogress').
card_type('ghazbán ogress', 'Creature — Ogre').
card_types('ghazbán ogress', ['Creature']).
card_subtypes('ghazbán ogress', ['Ogre']).
card_colors('ghazbán ogress', ['G']).
card_text('ghazbán ogress', 'When Ghazbán Ogress comes into play, the player who has won the most Magic games that day gains control of it. If more than one player has won the same number of games, you retain control of Ghazbán Ogress.').
card_mana_cost('ghazbán ogress', ['G']).
card_cmc('ghazbán ogress', 1).
card_layout('ghazbán ogress', 'normal').
card_power('ghazbán ogress', 2).
card_toughness('ghazbán ogress', 2).

% Found in: ORI
card_name('ghirapur gearcrafter', 'Ghirapur Gearcrafter').
card_type('ghirapur gearcrafter', 'Creature — Human Artificer').
card_types('ghirapur gearcrafter', ['Creature']).
card_subtypes('ghirapur gearcrafter', ['Human', 'Artificer']).
card_colors('ghirapur gearcrafter', ['R']).
card_text('ghirapur gearcrafter', 'When Ghirapur Gearcrafter enters the battlefield, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield. (A creature with flying can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('ghirapur gearcrafter', ['2', 'R']).
card_cmc('ghirapur gearcrafter', 3).
card_layout('ghirapur gearcrafter', 'normal').
card_power('ghirapur gearcrafter', 2).
card_toughness('ghirapur gearcrafter', 1).

% Found in: ORI
card_name('ghirapur æther grid', 'Ghirapur Æther Grid').
card_type('ghirapur æther grid', 'Enchantment').
card_types('ghirapur æther grid', ['Enchantment']).
card_subtypes('ghirapur æther grid', []).
card_colors('ghirapur æther grid', ['R']).
card_text('ghirapur æther grid', 'Tap two untapped artifacts you control: Ghirapur Æther Grid deals 1 damage to target creature or player.').
card_mana_cost('ghirapur æther grid', ['2', 'R']).
card_cmc('ghirapur æther grid', 3).
card_layout('ghirapur æther grid', 'normal').

% Found in: 10E, DDN, PD2, ULG
card_name('ghitu encampment', 'Ghitu Encampment').
card_type('ghitu encampment', 'Land').
card_types('ghitu encampment', ['Land']).
card_subtypes('ghitu encampment', []).
card_colors('ghitu encampment', []).
card_text('ghitu encampment', 'Ghitu Encampment enters the battlefield tapped.\n{T}: Add {R} to your mana pool.\n{1}{R}: Ghitu Encampment becomes a 2/1 red Warrior creature with first strike until end of turn. It\'s still a land. (It deals combat damage before creatures without first strike.)').
card_layout('ghitu encampment', 'normal').

% Found in: INV
card_name('ghitu fire', 'Ghitu Fire').
card_type('ghitu fire', 'Sorcery').
card_types('ghitu fire', ['Sorcery']).
card_subtypes('ghitu fire', []).
card_colors('ghitu fire', ['R']).
card_text('ghitu fire', 'You may cast Ghitu Fire as though it had flash if you pay {2} more to cast it. (You may cast it any time you could cast an instant.)\nGhitu Fire deals X damage to target creature or player.').
card_mana_cost('ghitu fire', ['X', 'R']).
card_cmc('ghitu fire', 1).
card_layout('ghitu fire', 'normal').

% Found in: 7ED, ULG
card_name('ghitu fire-eater', 'Ghitu Fire-Eater').
card_type('ghitu fire-eater', 'Creature — Human Nomad').
card_types('ghitu fire-eater', ['Creature']).
card_subtypes('ghitu fire-eater', ['Human', 'Nomad']).
card_colors('ghitu fire-eater', ['R']).
card_text('ghitu fire-eater', '{T}, Sacrifice Ghitu Fire-Eater: Ghitu Fire-Eater deals damage equal to its power to target creature or player.').
card_mana_cost('ghitu fire-eater', ['2', 'R']).
card_cmc('ghitu fire-eater', 3).
card_layout('ghitu fire-eater', 'normal').
card_power('ghitu fire-eater', 2).
card_toughness('ghitu fire-eater', 2).

% Found in: TSP
card_name('ghitu firebreathing', 'Ghitu Firebreathing').
card_type('ghitu firebreathing', 'Enchantment — Aura').
card_types('ghitu firebreathing', ['Enchantment']).
card_subtypes('ghitu firebreathing', ['Aura']).
card_colors('ghitu firebreathing', ['R']).
card_text('ghitu firebreathing', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\n{R}: Enchanted creature gets +1/+0 until end of turn.\n{R}: Return Ghitu Firebreathing to its owner\'s hand.').
card_mana_cost('ghitu firebreathing', ['1', 'R']).
card_cmc('ghitu firebreathing', 2).
card_layout('ghitu firebreathing', 'normal').

% Found in: ULG
card_name('ghitu slinger', 'Ghitu Slinger').
card_type('ghitu slinger', 'Creature — Human Nomad').
card_types('ghitu slinger', ['Creature']).
card_subtypes('ghitu slinger', ['Human', 'Nomad']).
card_colors('ghitu slinger', ['R']).
card_text('ghitu slinger', 'Echo {2}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Ghitu Slinger enters the battlefield, it deals 2 damage to target creature or player.').
card_mana_cost('ghitu slinger', ['2', 'R']).
card_cmc('ghitu slinger', 3).
card_layout('ghitu slinger', 'normal').
card_power('ghitu slinger', 2).
card_toughness('ghitu slinger', 2).

% Found in: ULG
card_name('ghitu war cry', 'Ghitu War Cry').
card_type('ghitu war cry', 'Enchantment').
card_types('ghitu war cry', ['Enchantment']).
card_subtypes('ghitu war cry', []).
card_colors('ghitu war cry', ['R']).
card_text('ghitu war cry', '{R}: Target creature gets +1/+0 until end of turn.').
card_mana_cost('ghitu war cry', ['2', 'R']).
card_cmc('ghitu war cry', 3).
card_layout('ghitu war cry', 'normal').

% Found in: GPT
card_name('ghor-clan bloodscale', 'Ghor-Clan Bloodscale').
card_type('ghor-clan bloodscale', 'Creature — Viashino Warrior').
card_types('ghor-clan bloodscale', ['Creature']).
card_subtypes('ghor-clan bloodscale', ['Viashino', 'Warrior']).
card_colors('ghor-clan bloodscale', ['R']).
card_text('ghor-clan bloodscale', 'First strike\n{3}{G}: Ghor-Clan Bloodscale gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_mana_cost('ghor-clan bloodscale', ['3', 'R']).
card_cmc('ghor-clan bloodscale', 4).
card_layout('ghor-clan bloodscale', 'normal').
card_power('ghor-clan bloodscale', 2).
card_toughness('ghor-clan bloodscale', 1).

% Found in: GTC, pFNM
card_name('ghor-clan rampager', 'Ghor-Clan Rampager').
card_type('ghor-clan rampager', 'Creature — Beast').
card_types('ghor-clan rampager', ['Creature']).
card_subtypes('ghor-clan rampager', ['Beast']).
card_colors('ghor-clan rampager', ['R', 'G']).
card_text('ghor-clan rampager', 'Trample\nBloodrush — {R}{G}, Discard Ghor-Clan Rampager: Target attacking creature gets +4/+4 and gains trample until end of turn.').
card_mana_cost('ghor-clan rampager', ['2', 'R', 'G']).
card_cmc('ghor-clan rampager', 4).
card_layout('ghor-clan rampager', 'normal').
card_power('ghor-clan rampager', 4).
card_toughness('ghor-clan rampager', 4).

% Found in: DDL, GPT
card_name('ghor-clan savage', 'Ghor-Clan Savage').
card_type('ghor-clan savage', 'Creature — Centaur Berserker').
card_types('ghor-clan savage', ['Creature']).
card_subtypes('ghor-clan savage', ['Centaur', 'Berserker']).
card_colors('ghor-clan savage', ['G']).
card_text('ghor-clan savage', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)').
card_mana_cost('ghor-clan savage', ['3', 'G', 'G']).
card_cmc('ghor-clan savage', 5).
card_layout('ghor-clan savage', 'normal').
card_power('ghor-clan savage', 2).
card_toughness('ghor-clan savage', 3).

% Found in: GPT, MM2
card_name('ghost council of orzhova', 'Ghost Council of Orzhova').
card_type('ghost council of orzhova', 'Legendary Creature — Spirit').
card_types('ghost council of orzhova', ['Creature']).
card_subtypes('ghost council of orzhova', ['Spirit']).
card_supertypes('ghost council of orzhova', ['Legendary']).
card_colors('ghost council of orzhova', ['W', 'B']).
card_text('ghost council of orzhova', 'When Ghost Council of Orzhova enters the battlefield, target opponent loses 1 life and you gain 1 life.\n{1}, Sacrifice a creature: Exile Ghost Council of Orzhova. Return it to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('ghost council of orzhova', ['W', 'W', 'B', 'B']).
card_cmc('ghost council of orzhova', 4).
card_layout('ghost council of orzhova', 'normal').
card_power('ghost council of orzhova', 4).
card_toughness('ghost council of orzhova', 4).

% Found in: HML
card_name('ghost hounds', 'Ghost Hounds').
card_type('ghost hounds', 'Creature — Hound Spirit').
card_types('ghost hounds', ['Creature']).
card_subtypes('ghost hounds', ['Hound', 'Spirit']).
card_colors('ghost hounds', ['B']).
card_text('ghost hounds', 'Vigilance\nWhenever Ghost Hounds blocks or becomes blocked by a white creature, Ghost Hounds gains first strike until end of turn.').
card_mana_cost('ghost hounds', ['1', 'B']).
card_cmc('ghost hounds', 2).
card_layout('ghost hounds', 'normal').
card_power('ghost hounds', 1).
card_toughness('ghost hounds', 1).

% Found in: C14, DIS, ISD, MD1
card_name('ghost quarter', 'Ghost Quarter').
card_type('ghost quarter', 'Land').
card_types('ghost quarter', ['Land']).
card_subtypes('ghost quarter', []).
card_colors('ghost quarter', []).
card_text('ghost quarter', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Ghost Quarter: Destroy target land. Its controller may search his or her library for a basic land card, put it onto the battlefield, then shuffle his or her library.').
card_layout('ghost quarter', 'normal').

% Found in: 4ED, DRK, TSB
card_name('ghost ship', 'Ghost Ship').
card_type('ghost ship', 'Creature — Spirit').
card_types('ghost ship', ['Creature']).
card_subtypes('ghost ship', ['Spirit']).
card_colors('ghost ship', ['U']).
card_text('ghost ship', 'Flying\n{U}{U}{U}: Regenerate Ghost Ship.').
card_mana_cost('ghost ship', ['2', 'U', 'U']).
card_cmc('ghost ship', 4).
card_layout('ghost ship', 'normal').
card_power('ghost ship', 2).
card_toughness('ghost ship', 4).

% Found in: PLC
card_name('ghost tactician', 'Ghost Tactician').
card_type('ghost tactician', 'Creature — Spirit Spellshaper').
card_types('ghost tactician', ['Creature']).
card_subtypes('ghost tactician', ['Spirit', 'Spellshaper']).
card_colors('ghost tactician', ['W']).
card_text('ghost tactician', '{W}, {T}, Discard a card: Creatures you control get +1/+0 until end of turn.').
card_mana_cost('ghost tactician', ['4', 'W']).
card_cmc('ghost tactician', 5).
card_layout('ghost tactician', 'normal').
card_power('ghost tactician', 2).
card_toughness('ghost tactician', 5).

% Found in: TMP
card_name('ghost town', 'Ghost Town').
card_type('ghost town', 'Land').
card_types('ghost town', ['Land']).
card_subtypes('ghost town', []).
card_colors('ghost town', []).
card_text('ghost town', '{T}: Add {1} to your mana pool.\n{0}: Return Ghost Town to its owner\'s hand. Activate this ability only if it\'s not your turn.').
card_layout('ghost town', 'normal').

% Found in: 10E, GPT
card_name('ghost warden', 'Ghost Warden').
card_type('ghost warden', 'Creature — Spirit').
card_types('ghost warden', ['Creature']).
card_subtypes('ghost warden', ['Spirit']).
card_colors('ghost warden', ['W']).
card_text('ghost warden', '{T}: Target creature gets +1/+1 until end of turn.').
card_mana_cost('ghost warden', ['1', 'W']).
card_cmc('ghost warden', 2).
card_layout('ghost warden', 'normal').
card_power('ghost warden', 1).
card_toughness('ghost warden', 1).

% Found in: SOK
card_name('ghost-lit nourisher', 'Ghost-Lit Nourisher').
card_type('ghost-lit nourisher', 'Creature — Spirit').
card_types('ghost-lit nourisher', ['Creature']).
card_subtypes('ghost-lit nourisher', ['Spirit']).
card_colors('ghost-lit nourisher', ['G']).
card_text('ghost-lit nourisher', '{2}{G}, {T}: Target creature gets +2/+2 until end of turn.\nChannel — {3}{G}, Discard Ghost-Lit Nourisher: Target creature gets +4/+4 until end of turn.').
card_mana_cost('ghost-lit nourisher', ['2', 'G']).
card_cmc('ghost-lit nourisher', 3).
card_layout('ghost-lit nourisher', 'normal').
card_power('ghost-lit nourisher', 2).
card_toughness('ghost-lit nourisher', 1).

% Found in: SOK, pREL
card_name('ghost-lit raider', 'Ghost-Lit Raider').
card_type('ghost-lit raider', 'Creature — Spirit').
card_types('ghost-lit raider', ['Creature']).
card_subtypes('ghost-lit raider', ['Spirit']).
card_colors('ghost-lit raider', ['R']).
card_text('ghost-lit raider', '{2}{R}, {T}: Ghost-Lit Raider deals 2 damage to target creature.\nChannel — {3}{R}, Discard Ghost-Lit Raider: Ghost-Lit Raider deals 4 damage to target creature.').
card_mana_cost('ghost-lit raider', ['2', 'R']).
card_cmc('ghost-lit raider', 3).
card_layout('ghost-lit raider', 'normal').
card_power('ghost-lit raider', 2).
card_toughness('ghost-lit raider', 1).

% Found in: SOK
card_name('ghost-lit redeemer', 'Ghost-Lit Redeemer').
card_type('ghost-lit redeemer', 'Creature — Spirit').
card_types('ghost-lit redeemer', ['Creature']).
card_subtypes('ghost-lit redeemer', ['Spirit']).
card_colors('ghost-lit redeemer', ['W']).
card_text('ghost-lit redeemer', '{W}, {T}: You gain 2 life.\nChannel — {1}{W}, Discard Ghost-Lit Redeemer: You gain 4 life.').
card_mana_cost('ghost-lit redeemer', ['W']).
card_cmc('ghost-lit redeemer', 1).
card_layout('ghost-lit redeemer', 'normal').
card_power('ghost-lit redeemer', 1).
card_toughness('ghost-lit redeemer', 1).

% Found in: DD3_GVL, DDD, SOK
card_name('ghost-lit stalker', 'Ghost-Lit Stalker').
card_type('ghost-lit stalker', 'Creature — Spirit').
card_types('ghost-lit stalker', ['Creature']).
card_subtypes('ghost-lit stalker', ['Spirit']).
card_colors('ghost-lit stalker', ['B']).
card_text('ghost-lit stalker', '{4}{B}, {T}: Target player discards two cards. Activate this ability only any time you could cast a sorcery.\nChannel — {5}{B}{B}, Discard Ghost-Lit Stalker: Target player discards four cards. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('ghost-lit stalker', ['B']).
card_cmc('ghost-lit stalker', 1).
card_layout('ghost-lit stalker', 'normal').
card_power('ghost-lit stalker', 1).
card_toughness('ghost-lit stalker', 1).

% Found in: SOK
card_name('ghost-lit warder', 'Ghost-Lit Warder').
card_type('ghost-lit warder', 'Creature — Spirit').
card_types('ghost-lit warder', ['Creature']).
card_subtypes('ghost-lit warder', ['Spirit']).
card_colors('ghost-lit warder', ['U']).
card_text('ghost-lit warder', '{3}{U}, {T}: Counter target spell unless its controller pays {2}.\nChannel — {3}{U}, Discard Ghost-Lit Warder: Counter target spell unless its controller pays {4}.').
card_mana_cost('ghost-lit warder', ['1', 'U']).
card_cmc('ghost-lit warder', 2).
card_layout('ghost-lit warder', 'normal').
card_power('ghost-lit warder', 1).
card_toughness('ghost-lit warder', 1).

% Found in: BNG
card_name('ghostblade eidolon', 'Ghostblade Eidolon').
card_type('ghostblade eidolon', 'Enchantment Creature — Spirit').
card_types('ghostblade eidolon', ['Enchantment', 'Creature']).
card_subtypes('ghostblade eidolon', ['Spirit']).
card_colors('ghostblade eidolon', ['W']).
card_text('ghostblade eidolon', 'Bestow {5}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nDouble strike (This creature deals both first-strike and regular combat damage.)\nEnchanted creature gets +1/+1 and has double strike.').
card_mana_cost('ghostblade eidolon', ['2', 'W']).
card_cmc('ghostblade eidolon', 3).
card_layout('ghostblade eidolon', 'normal').
card_power('ghostblade eidolon', 1).
card_toughness('ghostblade eidolon', 1).

% Found in: DDG, FUT
card_name('ghostfire', 'Ghostfire').
card_type('ghostfire', 'Instant').
card_types('ghostfire', ['Instant']).
card_subtypes('ghostfire', []).
card_colors('ghostfire', []).
card_text('ghostfire', 'Ghostfire is colorless.\nGhostfire deals 3 damage to target creature or player.').
card_mana_cost('ghostfire', ['2', 'R']).
card_cmc('ghostfire', 3).
card_layout('ghostfire', 'normal').

% Found in: FRF_UGIN, KTK
card_name('ghostfire blade', 'Ghostfire Blade').
card_type('ghostfire blade', 'Artifact — Equipment').
card_types('ghostfire blade', ['Artifact']).
card_subtypes('ghostfire blade', ['Equipment']).
card_colors('ghostfire blade', []).
card_text('ghostfire blade', 'Equipped creature gets +2/+2.\nEquip {3}\nGhostfire Blade\'s equip ability costs {2} less to activate if it targets a colorless creature.').
card_mana_cost('ghostfire blade', ['1']).
card_cmc('ghostfire blade', 1).
card_layout('ghostfire blade', 'normal').

% Found in: TSP
card_name('ghostflame sliver', 'Ghostflame Sliver').
card_type('ghostflame sliver', 'Creature — Sliver').
card_types('ghostflame sliver', ['Creature']).
card_subtypes('ghostflame sliver', ['Sliver']).
card_colors('ghostflame sliver', ['B', 'R']).
card_text('ghostflame sliver', 'All Slivers are colorless.').
card_mana_cost('ghostflame sliver', ['B', 'R']).
card_cmc('ghostflame sliver', 2).
card_layout('ghostflame sliver', 'normal').
card_power('ghostflame sliver', 2).
card_toughness('ghostflame sliver', 2).

% Found in: AVR
card_name('ghostform', 'Ghostform').
card_type('ghostform', 'Sorcery').
card_types('ghostform', ['Sorcery']).
card_subtypes('ghostform', []).
card_colors('ghostform', ['U']).
card_text('ghostform', 'Up to two target creatures can\'t be blocked this turn.').
card_mana_cost('ghostform', ['1', 'U']).
card_cmc('ghostform', 2).
card_layout('ghostform', 'normal').

% Found in: ONS
card_name('ghosthelm courier', 'Ghosthelm Courier').
card_type('ghosthelm courier', 'Creature — Human Wizard').
card_types('ghosthelm courier', ['Creature']).
card_subtypes('ghosthelm courier', ['Human', 'Wizard']).
card_colors('ghosthelm courier', ['U']).
card_text('ghosthelm courier', 'You may choose not to untap Ghosthelm Courier during your untap step.\n{2}{U}, {T}: Target Wizard creature gets +2/+2 and has shroud for as long as Ghosthelm Courier remains tapped. (It can\'t be the target of spells or abilities.)').
card_mana_cost('ghosthelm courier', ['2', 'U']).
card_cmc('ghosthelm courier', 3).
card_layout('ghosthelm courier', 'normal').
card_power('ghosthelm courier', 2).
card_toughness('ghosthelm courier', 1).

% Found in: LRW, MM2
card_name('ghostly changeling', 'Ghostly Changeling').
card_type('ghostly changeling', 'Creature — Shapeshifter').
card_types('ghostly changeling', ['Creature']).
card_subtypes('ghostly changeling', ['Shapeshifter']).
card_colors('ghostly changeling', ['B']).
card_text('ghostly changeling', 'Changeling (This card is every creature type.)\n{1}{B}: Ghostly Changeling gets +1/+1 until end of turn.').
card_mana_cost('ghostly changeling', ['2', 'B']).
card_cmc('ghostly changeling', 3).
card_layout('ghostly changeling', 'normal').
card_power('ghostly changeling', 2).
card_toughness('ghostly changeling', 2).

% Found in: ICE
card_name('ghostly flame', 'Ghostly Flame').
card_type('ghostly flame', 'Enchantment').
card_types('ghostly flame', ['Enchantment']).
card_subtypes('ghostly flame', []).
card_colors('ghostly flame', ['B', 'R']).
card_text('ghostly flame', 'Black and/or red permanents and spells are colorless sources of damage.').
card_mana_cost('ghostly flame', ['B', 'R']).
card_cmc('ghostly flame', 2).
card_layout('ghostly flame', 'normal').

% Found in: AVR
card_name('ghostly flicker', 'Ghostly Flicker').
card_type('ghostly flicker', 'Instant').
card_types('ghostly flicker', ['Instant']).
card_subtypes('ghostly flicker', []).
card_colors('ghostly flicker', ['U']).
card_text('ghostly flicker', 'Exile two target artifacts, creatures, and/or lands you control, then return those cards to the battlefield under your control.').
card_mana_cost('ghostly flicker', ['2', 'U']).
card_cmc('ghostly flicker', 3).
card_layout('ghostly flicker', 'normal').

% Found in: ISD
card_name('ghostly possession', 'Ghostly Possession').
card_type('ghostly possession', 'Enchantment — Aura').
card_types('ghostly possession', ['Enchantment']).
card_subtypes('ghostly possession', ['Aura']).
card_colors('ghostly possession', ['W']).
card_text('ghostly possession', 'Enchant creature\nEnchanted creature has flying.\nPrevent all combat damage that would be dealt to and dealt by enchanted creature.').
card_mana_cost('ghostly possession', ['2', 'W']).
card_cmc('ghostly possession', 3).
card_layout('ghostly possession', 'normal').

% Found in: CHK, CMD, PC2, pFNM
card_name('ghostly prison', 'Ghostly Prison').
card_type('ghostly prison', 'Enchantment').
card_types('ghostly prison', ['Enchantment']).
card_subtypes('ghostly prison', []).
card_colors('ghostly prison', ['W']).
card_text('ghostly prison', 'Creatures can\'t attack you unless their controller pays {2} for each creature he or she controls that\'s attacking you.').
card_mana_cost('ghostly prison', ['2', 'W']).
card_cmc('ghostly prison', 3).
card_layout('ghostly prison', 'normal').

% Found in: BFZ
card_name('ghostly sentinel', 'Ghostly Sentinel').
card_type('ghostly sentinel', 'Creature — Kor Spirit').
card_types('ghostly sentinel', ['Creature']).
card_subtypes('ghostly sentinel', ['Kor', 'Spirit']).
card_colors('ghostly sentinel', ['W']).
card_text('ghostly sentinel', 'Flying, vigilance').
card_mana_cost('ghostly sentinel', ['4', 'W']).
card_cmc('ghostly sentinel', 5).
card_layout('ghostly sentinel', 'normal').
card_power('ghostly sentinel', 3).
card_toughness('ghostly sentinel', 3).

% Found in: AVR
card_name('ghostly touch', 'Ghostly Touch').
card_type('ghostly touch', 'Enchantment — Aura').
card_types('ghostly touch', ['Enchantment']).
card_subtypes('ghostly touch', ['Aura']).
card_colors('ghostly touch', ['U']).
card_text('ghostly touch', 'Enchant creature\nEnchanted creature has \"Whenever this creature attacks, you may tap or untap target permanent.\"').
card_mana_cost('ghostly touch', ['1', 'U']).
card_cmc('ghostly touch', 2).
card_layout('ghostly touch', 'normal').

% Found in: ME3, PTK
card_name('ghostly visit', 'Ghostly Visit').
card_type('ghostly visit', 'Sorcery').
card_types('ghostly visit', ['Sorcery']).
card_subtypes('ghostly visit', []).
card_colors('ghostly visit', ['B']).
card_text('ghostly visit', 'Destroy target nonblack creature.').
card_mana_cost('ghostly visit', ['2', 'B']).
card_cmc('ghostly visit', 3).
card_layout('ghostly visit', 'normal').

% Found in: TOR
card_name('ghostly wings', 'Ghostly Wings').
card_type('ghostly wings', 'Enchantment — Aura').
card_types('ghostly wings', ['Enchantment']).
card_subtypes('ghostly wings', ['Aura']).
card_colors('ghostly wings', ['U']).
card_text('ghostly wings', 'Enchant creature\nEnchanted creature gets +1/+1 and has flying.\nDiscard a card: Return enchanted creature to its owner\'s hand.').
card_mana_cost('ghostly wings', ['1', 'U']).
card_cmc('ghostly wings', 2).
card_layout('ghostly wings', 'normal').

% Found in: LEG, ME3
card_name('ghosts of the damned', 'Ghosts of the Damned').
card_type('ghosts of the damned', 'Creature — Spirit').
card_types('ghosts of the damned', ['Creature']).
card_subtypes('ghosts of the damned', ['Spirit']).
card_colors('ghosts of the damned', ['B']).
card_text('ghosts of the damned', '{T}: Target creature gets -1/-0 until end of turn.').
card_mana_cost('ghosts of the damned', ['1', 'B', 'B']).
card_cmc('ghosts of the damned', 3).
card_layout('ghosts of the damned', 'normal').
card_power('ghosts of the damned', 0).
card_toughness('ghosts of the damned', 2).

% Found in: RAV
card_name('ghosts of the innocent', 'Ghosts of the Innocent').
card_type('ghosts of the innocent', 'Creature — Spirit').
card_types('ghosts of the innocent', ['Creature']).
card_subtypes('ghosts of the innocent', ['Spirit']).
card_colors('ghosts of the innocent', ['W']).
card_text('ghosts of the innocent', 'If a source would deal damage to a creature or player, it deals half that damage, rounded down, to that creature or player instead.').
card_mana_cost('ghosts of the innocent', ['5', 'W', 'W']).
card_cmc('ghosts of the innocent', 7).
card_layout('ghosts of the innocent', 'normal').
card_power('ghosts of the innocent', 4).
card_toughness('ghosts of the innocent', 5).

% Found in: GPT
card_name('ghostway', 'Ghostway').
card_type('ghostway', 'Instant').
card_types('ghostway', ['Instant']).
card_subtypes('ghostway', []).
card_colors('ghostway', ['W']).
card_text('ghostway', 'Exile each creature you control. Return those cards to the battlefield under their owner\'s control at the beginning of the next end step.').
card_mana_cost('ghostway', ['2', 'W']).
card_cmc('ghostway', 3).
card_layout('ghostway', 'normal').

% Found in: DDJ, MMQ
card_name('ghoul\'s feast', 'Ghoul\'s Feast').
card_type('ghoul\'s feast', 'Instant').
card_types('ghoul\'s feast', ['Instant']).
card_subtypes('ghoul\'s feast', []).
card_colors('ghoul\'s feast', ['B']).
card_text('ghoul\'s feast', 'Target creature gets +X/+0 until end of turn, where X is the number of creature cards in your graveyard.').
card_mana_cost('ghoul\'s feast', ['1', 'B']).
card_cmc('ghoul\'s feast', 2).
card_layout('ghoul\'s feast', 'normal').

% Found in: C14
card_name('ghoulcaller gisa', 'Ghoulcaller Gisa').
card_type('ghoulcaller gisa', 'Legendary Creature — Human Wizard').
card_types('ghoulcaller gisa', ['Creature']).
card_subtypes('ghoulcaller gisa', ['Human', 'Wizard']).
card_supertypes('ghoulcaller gisa', ['Legendary']).
card_colors('ghoulcaller gisa', ['B']).
card_text('ghoulcaller gisa', '{B}, {T}, Sacrifice another creature: Put X 2/2 black Zombie creature tokens onto the battlefield, where X is the sacrificed creature\'s power.').
card_mana_cost('ghoulcaller gisa', ['3', 'B', 'B']).
card_cmc('ghoulcaller gisa', 5).
card_layout('ghoulcaller gisa', 'normal').
card_power('ghoulcaller gisa', 3).
card_toughness('ghoulcaller gisa', 4).

% Found in: ISD
card_name('ghoulcaller\'s bell', 'Ghoulcaller\'s Bell').
card_type('ghoulcaller\'s bell', 'Artifact').
card_types('ghoulcaller\'s bell', ['Artifact']).
card_subtypes('ghoulcaller\'s bell', []).
card_colors('ghoulcaller\'s bell', []).
card_text('ghoulcaller\'s bell', '{T}: Each player puts the top card of his or her library into his or her graveyard.').
card_mana_cost('ghoulcaller\'s bell', ['1']).
card_cmc('ghoulcaller\'s bell', 1).
card_layout('ghoulcaller\'s bell', 'normal').

% Found in: ISD
card_name('ghoulcaller\'s chant', 'Ghoulcaller\'s Chant').
card_type('ghoulcaller\'s chant', 'Sorcery').
card_types('ghoulcaller\'s chant', ['Sorcery']).
card_subtypes('ghoulcaller\'s chant', []).
card_colors('ghoulcaller\'s chant', ['B']).
card_text('ghoulcaller\'s chant', 'Choose one —\n• Return target creature card from your graveyard to your hand.\n• Return two target Zombie cards from your graveyard to your hand.').
card_mana_cost('ghoulcaller\'s chant', ['B']).
card_cmc('ghoulcaller\'s chant', 1).
card_layout('ghoulcaller\'s chant', 'normal').

% Found in: AVR
card_name('ghoulflesh', 'Ghoulflesh').
card_type('ghoulflesh', 'Enchantment — Aura').
card_types('ghoulflesh', ['Enchantment']).
card_subtypes('ghoulflesh', ['Aura']).
card_colors('ghoulflesh', ['B']).
card_text('ghoulflesh', 'Enchant creature\nEnchanted creature gets -1/-1 and is a black Zombie in addition to its other colors and types.').
card_mana_cost('ghoulflesh', ['B']).
card_cmc('ghoulflesh', 1).
card_layout('ghoulflesh', 'normal').

% Found in: ISD
card_name('ghoulraiser', 'Ghoulraiser').
card_type('ghoulraiser', 'Creature — Zombie').
card_types('ghoulraiser', ['Creature']).
card_subtypes('ghoulraiser', ['Zombie']).
card_colors('ghoulraiser', ['B']).
card_text('ghoulraiser', 'When Ghoulraiser enters the battlefield, return a Zombie card at random from your graveyard to your hand.').
card_mana_cost('ghoulraiser', ['1', 'B', 'B']).
card_cmc('ghoulraiser', 3).
card_layout('ghoulraiser', 'normal').
card_power('ghoulraiser', 2).
card_toughness('ghoulraiser', 2).

% Found in: DKA
card_name('ghoultree', 'Ghoultree').
card_type('ghoultree', 'Creature — Zombie Treefolk').
card_types('ghoultree', ['Creature']).
card_subtypes('ghoultree', ['Zombie', 'Treefolk']).
card_colors('ghoultree', ['G']).
card_text('ghoultree', 'Ghoultree costs {1} less to cast for each creature card in your graveyard.').
card_mana_cost('ghoultree', ['7', 'G']).
card_cmc('ghoultree', 8).
card_layout('ghoultree', 'normal').
card_power('ghoultree', 10).
card_toughness('ghoultree', 10).

% Found in: GTC
card_name('giant adephage', 'Giant Adephage').
card_type('giant adephage', 'Creature — Insect').
card_types('giant adephage', ['Creature']).
card_subtypes('giant adephage', ['Insect']).
card_colors('giant adephage', ['G']).
card_text('giant adephage', 'Trample\nWhenever Giant Adephage deals combat damage to a player, put a token onto the battlefield that\'s a copy of Giant Adephage.').
card_mana_cost('giant adephage', ['5', 'G', 'G']).
card_cmc('giant adephage', 7).
card_layout('giant adephage', 'normal').
card_power('giant adephage', 7).
card_toughness('giant adephage', 7).

% Found in: HML
card_name('giant albatross', 'Giant Albatross').
card_type('giant albatross', 'Creature — Bird').
card_types('giant albatross', ['Creature']).
card_subtypes('giant albatross', ['Bird']).
card_colors('giant albatross', ['U']).
card_text('giant albatross', 'Flying\nWhen Giant Albatross dies, you may pay {1}{U}. If you do, for each creature that dealt damage to Giant Albatross this turn, destroy that creature unless its controller pays 2 life. A creature destroyed this way can\'t be regenerated.').
card_mana_cost('giant albatross', ['1', 'U']).
card_cmc('giant albatross', 2).
card_layout('giant albatross', 'normal').
card_power('giant albatross', 1).
card_toughness('giant albatross', 1).

% Found in: ARB
card_name('giant ambush beetle', 'Giant Ambush Beetle').
card_type('giant ambush beetle', 'Creature — Insect').
card_types('giant ambush beetle', ['Creature']).
card_subtypes('giant ambush beetle', ['Insect']).
card_colors('giant ambush beetle', ['B', 'R', 'G']).
card_text('giant ambush beetle', 'Haste\nWhen Giant Ambush Beetle enters the battlefield, you may have target creature block it this turn if able.').
card_mana_cost('giant ambush beetle', ['3', 'B/G', 'R']).
card_cmc('giant ambush beetle', 5).
card_layout('giant ambush beetle', 'normal').
card_power('giant ambush beetle', 4).
card_toughness('giant ambush beetle', 3).

% Found in: 8ED, pMEI
card_name('giant badger', 'Giant Badger').
card_type('giant badger', 'Creature — Badger').
card_types('giant badger', ['Creature']).
card_subtypes('giant badger', ['Badger']).
card_colors('giant badger', ['G']).
card_text('giant badger', 'Whenever Giant Badger blocks, it gets +2/+2 until end of turn.').
card_mana_cost('giant badger', ['1', 'G', 'G']).
card_cmc('giant badger', 3).
card_layout('giant badger', 'normal').
card_power('giant badger', 2).
card_toughness('giant badger', 2).

% Found in: MMQ, VIS
card_name('giant caterpillar', 'Giant Caterpillar').
card_type('giant caterpillar', 'Creature — Insect').
card_types('giant caterpillar', ['Creature']).
card_subtypes('giant caterpillar', ['Insect']).
card_colors('giant caterpillar', ['G']).
card_text('giant caterpillar', '{G}, Sacrifice Giant Caterpillar: Put a 1/1 green Insect creature token with flying named Butterfly onto the battlefield at the beginning of the next end step.').
card_mana_cost('giant caterpillar', ['3', 'G']).
card_cmc('giant caterpillar', 4).
card_layout('giant caterpillar', 'normal').
card_power('giant caterpillar', 3).
card_toughness('giant caterpillar', 3).

% Found in: 7ED, 8ED, 9ED, ULG
card_name('giant cockroach', 'Giant Cockroach').
card_type('giant cockroach', 'Creature — Insect').
card_types('giant cockroach', ['Creature']).
card_subtypes('giant cockroach', ['Insect']).
card_colors('giant cockroach', ['B']).
card_text('giant cockroach', '').
card_mana_cost('giant cockroach', ['3', 'B']).
card_cmc('giant cockroach', 4).
card_layout('giant cockroach', 'normal').
card_power('giant cockroach', 4).
card_toughness('giant cockroach', 2).

% Found in: BTD, TMP
card_name('giant crab', 'Giant Crab').
card_type('giant crab', 'Creature — Crab').
card_types('giant crab', ['Creature']).
card_subtypes('giant crab', ['Crab']).
card_colors('giant crab', ['U']).
card_text('giant crab', '{U}: Giant Crab gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_mana_cost('giant crab', ['4', 'U']).
card_cmc('giant crab', 5).
card_layout('giant crab', 'normal').
card_power('giant crab', 3).
card_toughness('giant crab', 3).

% Found in: MMA, PLC
card_name('giant dustwasp', 'Giant Dustwasp').
card_type('giant dustwasp', 'Creature — Insect').
card_types('giant dustwasp', ['Creature']).
card_subtypes('giant dustwasp', ['Insect']).
card_colors('giant dustwasp', ['G']).
card_text('giant dustwasp', 'Flying\nSuspend 4—{1}{G} (Rather than cast this card from your hand, you may pay {1}{G} and exile it with four time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('giant dustwasp', ['3', 'G', 'G']).
card_cmc('giant dustwasp', 5).
card_layout('giant dustwasp', 'normal').
card_power('giant dustwasp', 3).
card_toughness('giant dustwasp', 3).

% Found in: UGL
card_name('giant fan', 'Giant Fan').
card_type('giant fan', 'Artifact').
card_types('giant fan', ['Artifact']).
card_subtypes('giant fan', []).
card_colors('giant fan', []).
card_text('giant fan', '{2}, {T}: Move target counter from one card to another. If the second card\'s rules text refers to any type of counters, the moved counter becomes one of those counters. Otherwise, it becomes a +1/+1 counter.').
card_mana_cost('giant fan', ['4']).
card_cmc('giant fan', 4).
card_layout('giant fan', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ATH, BRB, BTD, CED, CEI, DD3_EVG, DD3_GVL, DDD, DKM, DPA, EVG, ICE, LEA, LEB, M10, M11, M14, ME2, ME3, ME4, RTR, S00, pFNM, pMPR, pSUS
card_name('giant growth', 'Giant Growth').
card_type('giant growth', 'Instant').
card_types('giant growth', ['Instant']).
card_subtypes('giant growth', []).
card_colors('giant growth', ['G']).
card_text('giant growth', 'Target creature gets +3/+3 until end of turn.').
card_mana_cost('giant growth', ['G']).
card_cmc('giant growth', 1).
card_layout('giant growth', 'normal').

% Found in: LRW
card_name('giant harbinger', 'Giant Harbinger').
card_type('giant harbinger', 'Creature — Giant Shaman').
card_types('giant harbinger', ['Creature']).
card_subtypes('giant harbinger', ['Giant', 'Shaman']).
card_colors('giant harbinger', ['R']).
card_text('giant harbinger', 'When Giant Harbinger enters the battlefield, you may search your library for a Giant card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('giant harbinger', ['4', 'R']).
card_cmc('giant harbinger', 5).
card_layout('giant harbinger', 'normal').
card_power('giant harbinger', 3).
card_toughness('giant harbinger', 4).

% Found in: BFZ, MIR, VMA
card_name('giant mantis', 'Giant Mantis').
card_type('giant mantis', 'Creature — Insect').
card_types('giant mantis', ['Creature']).
card_subtypes('giant mantis', ['Insect']).
card_colors('giant mantis', ['G']).
card_text('giant mantis', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('giant mantis', ['3', 'G']).
card_cmc('giant mantis', 4).
card_layout('giant mantis', 'normal').
card_power('giant mantis', 2).
card_toughness('giant mantis', 4).

% Found in: 7ED, 8ED, 9ED, POR, S00, S99
card_name('giant octopus', 'Giant Octopus').
card_type('giant octopus', 'Creature — Octopus').
card_types('giant octopus', ['Creature']).
card_subtypes('giant octopus', ['Octopus']).
card_colors('giant octopus', ['U']).
card_text('giant octopus', '').
card_mana_cost('giant octopus', ['3', 'U']).
card_cmc('giant octopus', 4).
card_layout('giant octopus', 'normal').
card_power('giant octopus', 3).
card_toughness('giant octopus', 3).

% Found in: HML, TSB
card_name('giant oyster', 'Giant Oyster').
card_type('giant oyster', 'Creature — Oyster').
card_types('giant oyster', ['Creature']).
card_subtypes('giant oyster', ['Oyster']).
card_colors('giant oyster', ['U']).
card_text('giant oyster', 'You may choose not to untap Giant Oyster during your untap step.\n{T}: For as long as Giant Oyster remains tapped, target tapped creature doesn\'t untap during its controller\'s untap step, and at the beginning of each of your draw steps, put a -1/-1 counter on that creature. When Giant Oyster leaves the battlefield or becomes untapped, remove all -1/-1 counters from the creature.').
card_mana_cost('giant oyster', ['2', 'U', 'U']).
card_cmc('giant oyster', 4).
card_layout('giant oyster', 'normal').
card_power('giant oyster', 0).
card_toughness('giant oyster', 3).

% Found in: M13, ZEN
card_name('giant scorpion', 'Giant Scorpion').
card_type('giant scorpion', 'Creature — Scorpion').
card_types('giant scorpion', ['Creature']).
card_subtypes('giant scorpion', ['Scorpion']).
card_colors('giant scorpion', ['B']).
card_text('giant scorpion', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('giant scorpion', ['2', 'B']).
card_cmc('giant scorpion', 3).
card_layout('giant scorpion', 'normal').
card_power('giant scorpion', 1).
card_toughness('giant scorpion', 3).

% Found in: DRK
card_name('giant shark', 'Giant Shark').
card_type('giant shark', 'Creature — Fish').
card_types('giant shark', ['Creature']).
card_subtypes('giant shark', ['Fish']).
card_colors('giant shark', ['U']).
card_text('giant shark', 'Giant Shark can\'t attack unless defending player controls an Island.\nWhenever Giant Shark blocks or becomes blocked by a creature that has been dealt damage this turn, Giant Shark gets +2/+0 and gains trample until end of turn.\nWhen you control no Islands, sacrifice Giant Shark.').
card_mana_cost('giant shark', ['5', 'U']).
card_cmc('giant shark', 6).
card_layout('giant shark', 'normal').
card_power('giant shark', 4).
card_toughness('giant shark', 4).

% Found in: CHR, LEG
card_name('giant slug', 'Giant Slug').
card_type('giant slug', 'Creature — Slug').
card_types('giant slug', ['Creature']).
card_subtypes('giant slug', ['Slug']).
card_colors('giant slug', ['B']).
card_text('giant slug', '{5}: At the beginning of your next upkeep, choose a basic land type. Giant Slug gains landwalk of the chosen type until the end of that turn. (It can\'t be blocked as long as defending player controls a land of that type.)').
card_mana_cost('giant slug', ['1', 'B']).
card_cmc('giant slug', 2).
card_layout('giant slug', 'normal').
card_power('giant slug', 1).
card_toughness('giant slug', 1).

% Found in: GPT
card_name('giant solifuge', 'Giant Solifuge').
card_type('giant solifuge', 'Creature — Insect').
card_types('giant solifuge', ['Creature']).
card_subtypes('giant solifuge', ['Insect']).
card_colors('giant solifuge', ['R', 'G']).
card_text('giant solifuge', '({R/G} can be paid with either {R} or {G}.)\nTrample; haste; shroud (This creature can\'t be the target of spells or abilities.)').
card_mana_cost('giant solifuge', ['2', 'R/G', 'R/G']).
card_cmc('giant solifuge', 4).
card_layout('giant solifuge', 'normal').
card_power('giant solifuge', 4).
card_toughness('giant solifuge', 1).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ATH, CED, CEI, DPA, LEA, LEB, M10, M11, M12, M14, POR
card_name('giant spider', 'Giant Spider').
card_type('giant spider', 'Creature — Spider').
card_types('giant spider', ['Creature']).
card_subtypes('giant spider', ['Spider']).
card_colors('giant spider', ['G']).
card_text('giant spider', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('giant spider', ['3', 'G']).
card_cmc('giant spider', 4).
card_layout('giant spider', 'normal').
card_power('giant spider', 2).
card_toughness('giant spider', 4).

% Found in: 4ED, 5ED, 6ED, LEG, TMP, VMA
card_name('giant strength', 'Giant Strength').
card_type('giant strength', 'Enchantment — Aura').
card_types('giant strength', ['Enchantment']).
card_subtypes('giant strength', ['Aura']).
card_colors('giant strength', ['R']).
card_text('giant strength', 'Enchant creature\nEnchanted creature gets +2/+2.').
card_mana_cost('giant strength', ['R', 'R']).
card_cmc('giant strength', 2).
card_layout('giant strength', 'normal').

% Found in: 4ED, ARN, ME4, MED
card_name('giant tortoise', 'Giant Tortoise').
card_type('giant tortoise', 'Creature — Turtle').
card_types('giant tortoise', ['Creature']).
card_subtypes('giant tortoise', ['Turtle']).
card_colors('giant tortoise', ['U']).
card_text('giant tortoise', 'Giant Tortoise gets +0/+3 as long as it\'s untapped.').
card_mana_cost('giant tortoise', ['1', 'U']).
card_cmc('giant tortoise', 2).
card_layout('giant tortoise', 'normal').
card_power('giant tortoise', 1).
card_toughness('giant tortoise', 1).

% Found in: CST, DKM, ICE, ME2
card_name('giant trap door spider', 'Giant Trap Door Spider').
card_type('giant trap door spider', 'Creature — Spider').
card_types('giant trap door spider', ['Creature']).
card_subtypes('giant trap door spider', ['Spider']).
card_colors('giant trap door spider', ['R', 'G']).
card_text('giant trap door spider', '{1}{R}{G}, {T}: Exile Giant Trap Door Spider and target creature without flying that\'s attacking you.').
card_mana_cost('giant trap door spider', ['1', 'R', 'G']).
card_cmc('giant trap door spider', 3).
card_layout('giant trap door spider', 'normal').
card_power('giant trap door spider', 2).
card_toughness('giant trap door spider', 3).

% Found in: LEG
card_name('giant turtle', 'Giant Turtle').
card_type('giant turtle', 'Creature — Turtle').
card_types('giant turtle', ['Creature']).
card_subtypes('giant turtle', ['Turtle']).
card_colors('giant turtle', ['G']).
card_text('giant turtle', 'Giant Turtle can\'t attack if it attacked during your last turn.').
card_mana_cost('giant turtle', ['1', 'G', 'G']).
card_cmc('giant turtle', 3).
card_layout('giant turtle', 'normal').
card_power('giant turtle', 2).
card_toughness('giant turtle', 4).

% Found in: JUD
card_name('giant warthog', 'Giant Warthog').
card_type('giant warthog', 'Creature — Boar Beast').
card_types('giant warthog', ['Creature']).
card_subtypes('giant warthog', ['Boar', 'Beast']).
card_colors('giant warthog', ['G']).
card_text('giant warthog', 'Trample').
card_mana_cost('giant warthog', ['5', 'G']).
card_cmc('giant warthog', 6).
card_layout('giant warthog', 'normal').
card_power('giant warthog', 5).
card_toughness('giant warthog', 5).

% Found in: LRW
card_name('giant\'s ire', 'Giant\'s Ire').
card_type('giant\'s ire', 'Tribal Sorcery — Giant').
card_types('giant\'s ire', ['Tribal', 'Sorcery']).
card_subtypes('giant\'s ire', ['Giant']).
card_colors('giant\'s ire', ['R']).
card_text('giant\'s ire', 'Giant\'s Ire deals 4 damage to target player. If you control a Giant, draw a card.').
card_mana_cost('giant\'s ire', ['3', 'R']).
card_cmc('giant\'s ire', 4).
card_layout('giant\'s ire', 'normal').

% Found in: SHM
card_name('giantbaiting', 'Giantbaiting').
card_type('giantbaiting', 'Sorcery').
card_types('giantbaiting', ['Sorcery']).
card_subtypes('giantbaiting', []).
card_colors('giantbaiting', ['R', 'G']).
card_text('giantbaiting', 'Put a 4/4 red and green Giant Warrior creature token with haste onto the battlefield. Exile it at the beginning of the next end step.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it.)').
card_mana_cost('giantbaiting', ['2', 'R/G']).
card_cmc('giantbaiting', 3).
card_layout('giantbaiting', 'normal').

% Found in: FUT
card_name('gibbering descent', 'Gibbering Descent').
card_type('gibbering descent', 'Enchantment').
card_types('gibbering descent', ['Enchantment']).
card_subtypes('gibbering descent', []).
card_colors('gibbering descent', ['B']).
card_text('gibbering descent', 'At the beginning of each player\'s upkeep, that player loses 1 life and discards a card.\nHellbent — Skip your upkeep step if you have no cards in hand.\nMadness {2}{B}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('gibbering descent', ['4', 'B', 'B']).
card_cmc('gibbering descent', 6).
card_layout('gibbering descent', 'normal').

% Found in: MIR
card_name('gibbering hyenas', 'Gibbering Hyenas').
card_type('gibbering hyenas', 'Creature — Hyena').
card_types('gibbering hyenas', ['Creature']).
card_subtypes('gibbering hyenas', ['Hyena']).
card_colors('gibbering hyenas', ['G']).
card_text('gibbering hyenas', 'Gibbering Hyenas can\'t block black creatures.').
card_mana_cost('gibbering hyenas', ['2', 'G']).
card_cmc('gibbering hyenas', 3).
card_layout('gibbering hyenas', 'normal').
card_power('gibbering hyenas', 3).
card_toughness('gibbering hyenas', 2).

% Found in: CHK
card_name('gibbering kami', 'Gibbering Kami').
card_type('gibbering kami', 'Creature — Spirit').
card_types('gibbering kami', ['Creature']).
card_subtypes('gibbering kami', ['Spirit']).
card_colors('gibbering kami', ['B']).
card_text('gibbering kami', 'Flying\nSoulshift 3 (When this creature dies, you may return target Spirit card with converted mana cost 3 or less from your graveyard to your hand.)').
card_mana_cost('gibbering kami', ['3', 'B']).
card_cmc('gibbering kami', 4).
card_layout('gibbering kami', 'normal').
card_power('gibbering kami', 2).
card_toughness('gibbering kami', 2).

% Found in: M12, ROE
card_name('gideon jura', 'Gideon Jura').
card_type('gideon jura', 'Planeswalker — Gideon').
card_types('gideon jura', ['Planeswalker']).
card_subtypes('gideon jura', ['Gideon']).
card_colors('gideon jura', ['W']).
card_text('gideon jura', '+2: During target opponent\'s next turn, creatures that player controls attack Gideon Jura if able.\n−2: Destroy target tapped creature.\n0: Until end of turn, Gideon Jura becomes a 6/6 Human Soldier creature that\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.').
card_mana_cost('gideon jura', ['3', 'W', 'W']).
card_cmc('gideon jura', 5).
card_layout('gideon jura', 'normal').
card_loyalty('gideon jura', 6).

% Found in: M12
card_name('gideon\'s avenger', 'Gideon\'s Avenger').
card_type('gideon\'s avenger', 'Creature — Human Soldier').
card_types('gideon\'s avenger', ['Creature']).
card_subtypes('gideon\'s avenger', ['Human', 'Soldier']).
card_colors('gideon\'s avenger', ['W']).
card_text('gideon\'s avenger', 'Whenever a creature an opponent controls becomes tapped, put a +1/+1 counter on Gideon\'s Avenger.').
card_mana_cost('gideon\'s avenger', ['1', 'W', 'W']).
card_cmc('gideon\'s avenger', 3).
card_layout('gideon\'s avenger', 'normal').
card_power('gideon\'s avenger', 2).
card_toughness('gideon\'s avenger', 2).

% Found in: M12
card_name('gideon\'s lawkeeper', 'Gideon\'s Lawkeeper').
card_type('gideon\'s lawkeeper', 'Creature — Human Soldier').
card_types('gideon\'s lawkeeper', ['Creature']).
card_subtypes('gideon\'s lawkeeper', ['Human', 'Soldier']).
card_colors('gideon\'s lawkeeper', ['W']).
card_text('gideon\'s lawkeeper', '{W}, {T}: Tap target creature.').
card_mana_cost('gideon\'s lawkeeper', ['W']).
card_cmc('gideon\'s lawkeeper', 1).
card_layout('gideon\'s lawkeeper', 'normal').
card_power('gideon\'s lawkeeper', 1).
card_toughness('gideon\'s lawkeeper', 1).

% Found in: ORI
card_name('gideon\'s phalanx', 'Gideon\'s Phalanx').
card_type('gideon\'s phalanx', 'Instant').
card_types('gideon\'s phalanx', ['Instant']).
card_subtypes('gideon\'s phalanx', []).
card_colors('gideon\'s phalanx', ['W']).
card_text('gideon\'s phalanx', 'Put four 2/2 white Knight creature tokens with vigilance onto the battlefield.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, creatures you control gain indestructible until end of turn.').
card_mana_cost('gideon\'s phalanx', ['5', 'W', 'W']).
card_cmc('gideon\'s phalanx', 7).
card_layout('gideon\'s phalanx', 'normal').

% Found in: BFZ
card_name('gideon\'s reproach', 'Gideon\'s Reproach').
card_type('gideon\'s reproach', 'Instant').
card_types('gideon\'s reproach', ['Instant']).
card_subtypes('gideon\'s reproach', []).
card_colors('gideon\'s reproach', ['W']).
card_text('gideon\'s reproach', 'Gideon\'s Reproach deals 4 damage to target attacking or blocking creature.').
card_mana_cost('gideon\'s reproach', ['1', 'W']).
card_cmc('gideon\'s reproach', 2).
card_layout('gideon\'s reproach', 'normal').

% Found in: BFZ
card_name('gideon, ally of zendikar', 'Gideon, Ally of Zendikar').
card_type('gideon, ally of zendikar', 'Planeswalker — Gideon').
card_types('gideon, ally of zendikar', ['Planeswalker']).
card_subtypes('gideon, ally of zendikar', ['Gideon']).
card_colors('gideon, ally of zendikar', ['W']).
card_text('gideon, ally of zendikar', '+1: Until end of turn, Gideon, Ally of Zendikar becomes a 5/5 Human Soldier Ally creature with indestructible that\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.\n0: Put a 2/2 white Knight Ally creature token onto the battlefield.\n−4: You get an emblem with \"Creatures you control get +1/+1.\"').
card_mana_cost('gideon, ally of zendikar', ['2', 'W', 'W']).
card_cmc('gideon, ally of zendikar', 4).
card_layout('gideon, ally of zendikar', 'normal').
card_loyalty('gideon, ally of zendikar', 4).

% Found in: ORI
card_name('gideon, battle-forged', 'Gideon, Battle-Forged').
card_type('gideon, battle-forged', 'Planeswalker — Gideon').
card_types('gideon, battle-forged', ['Planeswalker']).
card_subtypes('gideon, battle-forged', ['Gideon']).
card_colors('gideon, battle-forged', ['W']).
card_text('gideon, battle-forged', '+2: Up to one target creature an opponent controls attacks Gideon, Battle-Forged during its controller\'s next turn if able.\n+1: Until your next turn, target creature gains indestructible. Untap that creature.\n0: Until end of turn, Gideon, Battle-Forged becomes a 4/4 Human Soldier creature with indestructible that\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.').
card_layout('gideon, battle-forged', 'double-faced').
card_loyalty('gideon, battle-forged', 3).

% Found in: GTC
card_name('gideon, champion of justice', 'Gideon, Champion of Justice').
card_type('gideon, champion of justice', 'Planeswalker — Gideon').
card_types('gideon, champion of justice', ['Planeswalker']).
card_subtypes('gideon, champion of justice', ['Gideon']).
card_colors('gideon, champion of justice', ['W']).
card_text('gideon, champion of justice', '+1: Put a loyalty counter on Gideon, Champion of Justice for each creature target opponent controls.\n0: Until end of turn, Gideon, Champion of Justice becomes a Human Soldier creature with power and toughness each equal to the number of loyalty counters on him and gains indestructible. He\'s still a planeswalker. Prevent all damage that would be dealt to him this turn.\n−15: Exile all other permanents.').
card_mana_cost('gideon, champion of justice', ['2', 'W', 'W']).
card_cmc('gideon, champion of justice', 4).
card_layout('gideon, champion of justice', 'normal').
card_loyalty('gideon, champion of justice', 4).

% Found in: 9ED, C14, POR
card_name('gift of estates', 'Gift of Estates').
card_type('gift of estates', 'Sorcery').
card_types('gift of estates', ['Sorcery']).
card_subtypes('gift of estates', []).
card_colors('gift of estates', ['W']).
card_text('gift of estates', 'If an opponent controls more lands than you, search your library for up to three Plains cards, reveal them, and put them into your hand. Then shuffle your library.').
card_mana_cost('gift of estates', ['1', 'W']).
card_cmc('gift of estates', 2).
card_layout('gift of estates', 'normal').

% Found in: FUT
card_name('gift of granite', 'Gift of Granite').
card_type('gift of granite', 'Enchantment — Aura').
card_types('gift of granite', ['Enchantment']).
card_subtypes('gift of granite', ['Aura']).
card_colors('gift of granite', ['W']).
card_text('gift of granite', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +0/+2.').
card_mana_cost('gift of granite', ['W']).
card_cmc('gift of granite', 1).
card_layout('gift of granite', 'normal').

% Found in: THS
card_name('gift of immortality', 'Gift of Immortality').
card_type('gift of immortality', 'Enchantment — Aura').
card_types('gift of immortality', ['Enchantment']).
card_subtypes('gift of immortality', ['Aura']).
card_colors('gift of immortality', ['W']).
card_text('gift of immortality', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under its owner\'s control. Return Gift of Immortality to the battlefield attached to that creature at the beginning of the next end step.').
card_mana_cost('gift of immortality', ['2', 'W']).
card_cmc('gift of immortality', 3).
card_layout('gift of immortality', 'normal').

% Found in: GTC
card_name('gift of orzhova', 'Gift of Orzhova').
card_type('gift of orzhova', 'Enchantment — Aura').
card_types('gift of orzhova', ['Enchantment']).
card_subtypes('gift of orzhova', ['Aura']).
card_colors('gift of orzhova', ['W', 'B']).
card_text('gift of orzhova', 'Enchant creature\nEnchanted creature gets +1/+1 and has flying and lifelink.').
card_mana_cost('gift of orzhova', ['1', 'W/B', 'W/B']).
card_cmc('gift of orzhova', 3).
card_layout('gift of orzhova', 'normal').

% Found in: EVE
card_name('gift of the deity', 'Gift of the Deity').
card_type('gift of the deity', 'Enchantment — Aura').
card_types('gift of the deity', ['Enchantment']).
card_subtypes('gift of the deity', ['Aura']).
card_colors('gift of the deity', ['B', 'G']).
card_text('gift of the deity', 'Enchant creature\nAs long as enchanted creature is black, it gets +1/+1 and has deathtouch. (Any amount of damage it deals to a creature is enough to destroy that creature.)\nAs long as enchanted creature is green, it gets +1/+1 and all creatures able to block it do so.').
card_mana_cost('gift of the deity', ['4', 'B/G']).
card_cmc('gift of the deity', 5).
card_layout('gift of the deity', 'normal').

% Found in: ALA
card_name('gift of the gargantuan', 'Gift of the Gargantuan').
card_type('gift of the gargantuan', 'Sorcery').
card_types('gift of the gargantuan', ['Sorcery']).
card_subtypes('gift of the gargantuan', []).
card_colors('gift of the gargantuan', ['G']).
card_text('gift of the gargantuan', 'Look at the top four cards of your library. You may reveal a creature card and/or a land card from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('gift of the gargantuan', ['2', 'G']).
card_cmc('gift of the gargantuan', 3).
card_layout('gift of the gargantuan', 'normal').

% Found in: ALL
card_name('gift of the woods', 'Gift of the Woods').
card_type('gift of the woods', 'Enchantment — Aura').
card_types('gift of the woods', ['Enchantment']).
card_subtypes('gift of the woods', ['Aura']).
card_colors('gift of the woods', ['G']).
card_text('gift of the woods', 'Enchant creature\nWhenever enchanted creature blocks or becomes blocked, it gets +0/+3 until end of turn and you gain 1 life.').
card_mana_cost('gift of the woods', ['G']).
card_cmc('gift of the woods', 1).
card_layout('gift of the woods', 'normal').

% Found in: pHHO
card_name('gifts given', 'Gifts Given').
card_type('gifts given', 'Instant').
card_types('gifts given', ['Instant']).
card_subtypes('gifts given', []).
card_colors('gifts given', ['U']).
card_text('gifts given', 'Search target opponent\'s library for four cards with different names and reveal them. That player chooses two of those cards. Put the chosen cards into the player\'s graveyard and the rest into your hand. Then that player shuffles his or her library.').
card_mana_cost('gifts given', ['3', 'U']).
card_cmc('gifts given', 4).
card_layout('gifts given', 'normal').

% Found in: CHK, MMA, V09
card_name('gifts ungiven', 'Gifts Ungiven').
card_type('gifts ungiven', 'Instant').
card_types('gifts ungiven', ['Instant']).
card_subtypes('gifts ungiven', []).
card_colors('gifts ungiven', ['U']).
card_text('gifts ungiven', 'Search your library for up to four cards with different names and reveal them. Target opponent chooses two of those cards. Put the chosen cards into your graveyard and the rest into your hand. Then shuffle your library.').
card_mana_cost('gifts ungiven', ['3', 'U']).
card_cmc('gifts ungiven', 4).
card_layout('gifts ungiven', 'normal').

% Found in: GPT
card_name('gigadrowse', 'Gigadrowse').
card_type('gigadrowse', 'Instant').
card_types('gigadrowse', ['Instant']).
card_subtypes('gigadrowse', []).
card_colors('gigadrowse', ['U']).
card_text('gigadrowse', 'Replicate {U} (When you cast this spell, copy it for each time you paid its replicate cost. You may choose new targets for the copies.)\nTap target permanent.').
card_mana_cost('gigadrowse', ['U']).
card_cmc('gigadrowse', 1).
card_layout('gigadrowse', 'normal').

% Found in: ZEN
card_name('gigantiform', 'Gigantiform').
card_type('gigantiform', 'Enchantment — Aura').
card_types('gigantiform', ['Enchantment']).
card_subtypes('gigantiform', ['Aura']).
card_colors('gigantiform', ['G']).
card_text('gigantiform', 'Kicker {4}\nEnchant creature\nEnchanted creature has base power and toughness 8/8 and has trample.\nWhen Gigantiform enters the battlefield, if it was kicked, you may search your library for a card named Gigantiform, put it onto the battlefield, then shuffle your library.').
card_mana_cost('gigantiform', ['3', 'G', 'G']).
card_cmc('gigantiform', 5).
card_layout('gigantiform', 'normal').

% Found in: ROE
card_name('gigantomancer', 'Gigantomancer').
card_type('gigantomancer', 'Creature — Human Shaman').
card_types('gigantomancer', ['Creature']).
card_subtypes('gigantomancer', ['Human', 'Shaman']).
card_colors('gigantomancer', ['G']).
card_text('gigantomancer', '{1}: Target creature you control has base power and toughness 7/7 until end of turn.').
card_mana_cost('gigantomancer', ['7', 'G']).
card_cmc('gigantomancer', 8).
card_layout('gigantomancer', 'normal').
card_power('gigantomancer', 1).
card_toughness('gigantomancer', 1).

% Found in: ONS, VMA
card_name('gigapede', 'Gigapede').
card_type('gigapede', 'Creature — Insect').
card_types('gigapede', ['Creature']).
card_subtypes('gigapede', ['Insect']).
card_colors('gigapede', ['G']).
card_text('gigapede', 'Shroud (This creature can\'t be the target of spells or abilities.)\nAt the beginning of your upkeep, if Gigapede is in your graveyard, you may discard a card. If you do, return Gigapede to your hand.').
card_mana_cost('gigapede', ['3', 'G', 'G']).
card_cmc('gigapede', 5).
card_layout('gigapede', 'normal').
card_power('gigapede', 6).
card_toughness('gigapede', 1).

% Found in: BNG
card_name('gild', 'Gild').
card_type('gild', 'Sorcery').
card_types('gild', ['Sorcery']).
card_subtypes('gild', []).
card_colors('gild', ['B']).
card_text('gild', 'Exile target creature. Put a colorless artifact token named Gold onto the battlefield. It has \"Sacrifice this artifact: Add one mana of any color to your mana pool.\"').
card_mana_cost('gild', ['3', 'B']).
card_cmc('gild', 4).
card_layout('gild', 'normal').

% Found in: USG
card_name('gilded drake', 'Gilded Drake').
card_type('gilded drake', 'Creature — Drake').
card_types('gilded drake', ['Creature']).
card_subtypes('gilded drake', ['Drake']).
card_colors('gilded drake', ['U']).
card_text('gilded drake', 'Flying\nWhen Gilded Drake enters the battlefield, exchange control of Gilded Drake and up to one target creature an opponent controls. If you don\'t make an exchange, sacrifice Gilded Drake. This ability can\'t be countered except by spells and abilities. (This effect lasts indefinitely.)').
card_mana_cost('gilded drake', ['1', 'U']).
card_cmc('gilded drake', 2).
card_layout('gilded drake', 'normal').
card_power('gilded drake', 3).
card_toughness('gilded drake', 3).
card_reserved('gilded drake').

% Found in: SCG, VMA
card_name('gilded light', 'Gilded Light').
card_type('gilded light', 'Instant').
card_types('gilded light', ['Instant']).
card_subtypes('gilded light', []).
card_colors('gilded light', ['W']).
card_text('gilded light', 'You gain shroud until end of turn. (You can\'t be the target of spells or abilities.)\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('gilded light', ['1', 'W']).
card_cmc('gilded light', 2).
card_layout('gilded light', 'normal').

% Found in: M13, MRD, V13
card_name('gilded lotus', 'Gilded Lotus').
card_type('gilded lotus', 'Artifact').
card_types('gilded lotus', ['Artifact']).
card_subtypes('gilded lotus', []).
card_colors('gilded lotus', []).
card_text('gilded lotus', '{T}: Add three mana of any one color to your mana pool.').
card_mana_cost('gilded lotus', ['5']).
card_cmc('gilded lotus', 5).
card_layout('gilded lotus', 'normal').

% Found in: EVE
card_name('gilder bairn', 'Gilder Bairn').
card_type('gilder bairn', 'Creature — Ouphe').
card_types('gilder bairn', ['Creature']).
card_subtypes('gilder bairn', ['Ouphe']).
card_colors('gilder bairn', ['U', 'G']).
card_text('gilder bairn', '{2}{G/U}, {Q}: For each counter on target permanent, put another of those counters on that permanent. ({Q} is the untap symbol.)').
card_mana_cost('gilder bairn', ['1', 'G/U', 'G/U']).
card_cmc('gilder bairn', 3).
card_layout('gilder bairn', 'normal').
card_power('gilder bairn', 1).
card_toughness('gilder bairn', 3).

% Found in: LRW
card_name('gilt-leaf ambush', 'Gilt-Leaf Ambush').
card_type('gilt-leaf ambush', 'Tribal Instant — Elf').
card_types('gilt-leaf ambush', ['Tribal', 'Instant']).
card_subtypes('gilt-leaf ambush', ['Elf']).
card_colors('gilt-leaf ambush', ['G']).
card_text('gilt-leaf ambush', 'Put two 1/1 green Elf Warrior creature tokens onto the battlefield. Clash with an opponent. If you win, those creatures gain deathtouch until end of turn. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost. Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_mana_cost('gilt-leaf ambush', ['2', 'G']).
card_cmc('gilt-leaf ambush', 3).
card_layout('gilt-leaf ambush', 'normal').

% Found in: MOR
card_name('gilt-leaf archdruid', 'Gilt-Leaf Archdruid').
card_type('gilt-leaf archdruid', 'Creature — Elf Druid').
card_types('gilt-leaf archdruid', ['Creature']).
card_subtypes('gilt-leaf archdruid', ['Elf', 'Druid']).
card_colors('gilt-leaf archdruid', ['G']).
card_text('gilt-leaf archdruid', 'Whenever you cast a Druid spell, you may draw a card.\nTap seven untapped Druids you control: Gain control of all lands target player controls.').
card_mana_cost('gilt-leaf archdruid', ['3', 'G', 'G']).
card_cmc('gilt-leaf archdruid', 5).
card_layout('gilt-leaf archdruid', 'normal').
card_power('gilt-leaf archdruid', 3).
card_toughness('gilt-leaf archdruid', 3).

% Found in: LRW
card_name('gilt-leaf palace', 'Gilt-Leaf Palace').
card_type('gilt-leaf palace', 'Land').
card_types('gilt-leaf palace', ['Land']).
card_subtypes('gilt-leaf palace', []).
card_colors('gilt-leaf palace', []).
card_text('gilt-leaf palace', 'As Gilt-Leaf Palace enters the battlefield, you may reveal an Elf card from your hand. If you don\'t, Gilt-Leaf Palace enters the battlefield tapped.\n{T}: Add {B} or {G} to your mana pool.').
card_layout('gilt-leaf palace', 'normal').

% Found in: LRW
card_name('gilt-leaf seer', 'Gilt-Leaf Seer').
card_type('gilt-leaf seer', 'Creature — Elf Shaman').
card_types('gilt-leaf seer', ['Creature']).
card_subtypes('gilt-leaf seer', ['Elf', 'Shaman']).
card_colors('gilt-leaf seer', ['G']).
card_text('gilt-leaf seer', '{G}, {T}: Look at the top two cards of your library, then put them back in any order.').
card_mana_cost('gilt-leaf seer', ['2', 'G']).
card_cmc('gilt-leaf seer', 3).
card_layout('gilt-leaf seer', 'normal').
card_power('gilt-leaf seer', 2).
card_toughness('gilt-leaf seer', 2).

% Found in: ORI
card_name('gilt-leaf winnower', 'Gilt-Leaf Winnower').
card_type('gilt-leaf winnower', 'Creature — Elf Warrior').
card_types('gilt-leaf winnower', ['Creature']).
card_subtypes('gilt-leaf winnower', ['Elf', 'Warrior']).
card_colors('gilt-leaf winnower', ['B']).
card_text('gilt-leaf winnower', 'Menace (This creature can\'t be blocked except by two or more creatures.)\nWhen Gilt-Leaf Winnower enters the battlefield, you may destroy target non-Elf creature whose power and toughness aren\'t equal.').
card_mana_cost('gilt-leaf winnower', ['3', 'B', 'B']).
card_cmc('gilt-leaf winnower', 5).
card_layout('gilt-leaf winnower', 'normal').
card_power('gilt-leaf winnower', 4).
card_toughness('gilt-leaf winnower', 3).

% Found in: CON
card_name('giltspire avenger', 'Giltspire Avenger').
card_type('giltspire avenger', 'Creature — Human Soldier').
card_types('giltspire avenger', ['Creature']).
card_subtypes('giltspire avenger', ['Human', 'Soldier']).
card_colors('giltspire avenger', ['W', 'U', 'G']).
card_text('giltspire avenger', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{T}: Destroy target creature that dealt damage to you this turn.').
card_mana_cost('giltspire avenger', ['G', 'W', 'U']).
card_cmc('giltspire avenger', 3).
card_layout('giltspire avenger', 'normal').
card_power('giltspire avenger', 2).
card_toughness('giltspire avenger', 2).

% Found in: AVR
card_name('gisela, blade of goldnight', 'Gisela, Blade of Goldnight').
card_type('gisela, blade of goldnight', 'Legendary Creature — Angel').
card_types('gisela, blade of goldnight', ['Creature']).
card_subtypes('gisela, blade of goldnight', ['Angel']).
card_supertypes('gisela, blade of goldnight', ['Legendary']).
card_colors('gisela, blade of goldnight', ['W', 'R']).
card_text('gisela, blade of goldnight', 'Flying, first strike\nIf a source would deal damage to an opponent or a permanent an opponent controls, that source deals double that damage to that player or permanent instead.\nIf a source would deal damage to you or a permanent you control, prevent half that damage, rounded up.').
card_mana_cost('gisela, blade of goldnight', ['4', 'R', 'W', 'W']).
card_cmc('gisela, blade of goldnight', 7).
card_layout('gisela, blade of goldnight', 'normal').
card_power('gisela, blade of goldnight', 5).
card_toughness('gisela, blade of goldnight', 5).

% Found in: NPH, pFNM
card_name('gitaxian probe', 'Gitaxian Probe').
card_type('gitaxian probe', 'Sorcery').
card_types('gitaxian probe', ['Sorcery']).
card_subtypes('gitaxian probe', []).
card_colors('gitaxian probe', ['U']).
card_text('gitaxian probe', '({U/P} can be paid with either {U} or 2 life.)\nLook at target player\'s hand.\nDraw a card.').
card_mana_cost('gitaxian probe', ['U/P']).
card_cmc('gitaxian probe', 1).
card_layout('gitaxian probe', 'normal').

% Found in: DGM
card_name('give', 'Give').
card_type('give', 'Sorcery').
card_types('give', ['Sorcery']).
card_subtypes('give', []).
card_colors('give', ['G']).
card_text('give', 'Put three +1/+1 counters on target creature.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('give', ['2', 'G']).
card_cmc('give', 3).
card_layout('give', 'split').
card_sides('give', 'take').

% Found in: VAN
card_name('gix', 'Gix').
card_type('gix', 'Vanguard').
card_types('gix', ['Vanguard']).
card_subtypes('gix', []).
card_colors('gix', []).
card_text('gix', '{3}: Return target creature card from your graveyard to your hand.').
card_layout('gix', 'vanguard').

% Found in: ICE, ME2, V12
card_name('glacial chasm', 'Glacial Chasm').
card_type('glacial chasm', 'Land').
card_types('glacial chasm', ['Land']).
card_subtypes('glacial chasm', []).
card_colors('glacial chasm', []).
card_text('glacial chasm', 'Cumulative upkeep—Pay 2 life. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen Glacial Chasm enters the battlefield, sacrifice a land.\nCreatures you control can\'t attack.\nPrevent all damage that would be dealt to you.').
card_layout('glacial chasm', 'normal').

% Found in: M15
card_name('glacial crasher', 'Glacial Crasher').
card_type('glacial crasher', 'Creature — Elemental').
card_types('glacial crasher', ['Creature']).
card_subtypes('glacial crasher', ['Elemental']).
card_colors('glacial crasher', ['U']).
card_text('glacial crasher', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nGlacial Crasher can\'t attack unless there is a Mountain on the battlefield.').
card_mana_cost('glacial crasher', ['4', 'U', 'U']).
card_cmc('glacial crasher', 6).
card_layout('glacial crasher', 'normal').
card_power('glacial crasher', 5).
card_toughness('glacial crasher', 5).

% Found in: ICE, ME2
card_name('glacial crevasses', 'Glacial Crevasses').
card_type('glacial crevasses', 'Enchantment').
card_types('glacial crevasses', ['Enchantment']).
card_subtypes('glacial crevasses', []).
card_colors('glacial crevasses', ['R']).
card_text('glacial crevasses', 'Sacrifice a snow Mountain: Prevent all combat damage that would be dealt this turn.').
card_mana_cost('glacial crevasses', ['2', 'R']).
card_cmc('glacial crevasses', 3).
card_layout('glacial crevasses', 'normal').
card_reserved('glacial crevasses').

% Found in: M10, M11, M12, M13
card_name('glacial fortress', 'Glacial Fortress').
card_type('glacial fortress', 'Land').
card_types('glacial fortress', ['Land']).
card_subtypes('glacial fortress', []).
card_colors('glacial fortress', []).
card_text('glacial fortress', 'Glacial Fortress enters the battlefield tapped unless you control a Plains or an Island.\n{T}: Add {W} or {U} to your mana pool.').
card_layout('glacial fortress', 'normal').

% Found in: CSP
card_name('glacial plating', 'Glacial Plating').
card_type('glacial plating', 'Snow Enchantment — Aura').
card_types('glacial plating', ['Enchantment']).
card_subtypes('glacial plating', ['Aura']).
card_supertypes('glacial plating', ['Snow']).
card_colors('glacial plating', ['W']).
card_text('glacial plating', 'Enchant creature\nCumulative upkeep {S} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it. {S} can be paid with one mana from a snow permanent.)\nEnchanted creature gets +3/+3 for each age counter on Glacial Plating.').
card_mana_cost('glacial plating', ['2', 'W', 'W']).
card_cmc('glacial plating', 4).
card_layout('glacial plating', 'normal').

% Found in: CHK, MMA, pARL
card_name('glacial ray', 'Glacial Ray').
card_type('glacial ray', 'Instant — Arcane').
card_types('glacial ray', ['Instant']).
card_subtypes('glacial ray', ['Arcane']).
card_colors('glacial ray', ['R']).
card_text('glacial ray', 'Glacial Ray deals 2 damage to target creature or player.\nSplice onto Arcane {1}{R} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('glacial ray', ['1', 'R']).
card_cmc('glacial ray', 2).
card_layout('glacial ray', 'normal').

% Found in: KTK
card_name('glacial stalker', 'Glacial Stalker').
card_type('glacial stalker', 'Creature — Elemental').
card_types('glacial stalker', ['Creature']).
card_subtypes('glacial stalker', ['Elemental']).
card_colors('glacial stalker', ['U']).
card_text('glacial stalker', 'Morph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('glacial stalker', ['5', 'U']).
card_cmc('glacial stalker', 6).
card_layout('glacial stalker', 'normal').
card_power('glacial stalker', 4).
card_toughness('glacial stalker', 5).

% Found in: 5ED, 6ED, 7ED, ICE
card_name('glacial wall', 'Glacial Wall').
card_type('glacial wall', 'Creature — Wall').
card_types('glacial wall', ['Creature']).
card_subtypes('glacial wall', ['Wall']).
card_colors('glacial wall', ['U']).
card_text('glacial wall', 'Defender (This creature can\'t attack.)').
card_mana_cost('glacial wall', ['2', 'U']).
card_cmc('glacial wall', 3).
card_layout('glacial wall', 'normal').
card_power('glacial wall', 0).
card_toughness('glacial wall', 7).

% Found in: ICE
card_name('glaciers', 'Glaciers').
card_type('glaciers', 'Enchantment').
card_types('glaciers', ['Enchantment']).
card_subtypes('glaciers', []).
card_colors('glaciers', ['W', 'U']).
card_text('glaciers', 'At the beginning of your upkeep, sacrifice Glaciers unless you pay {W}{U}.\nAll Mountains are Plains.').
card_mana_cost('glaciers', ['2', 'W', 'U']).
card_cmc('glaciers', 4).
card_layout('glaciers', 'normal').

% Found in: APC
card_name('glade gnarr', 'Glade Gnarr').
card_type('glade gnarr', 'Creature — Beast').
card_types('glade gnarr', ['Creature']).
card_subtypes('glade gnarr', ['Beast']).
card_colors('glade gnarr', ['G']).
card_text('glade gnarr', 'Whenever a player casts a blue spell, Glade Gnarr gets +2/+2 until end of turn.').
card_mana_cost('glade gnarr', ['5', 'G']).
card_cmc('glade gnarr', 6).
card_layout('glade gnarr', 'normal').
card_power('glade gnarr', 4).
card_toughness('glade gnarr', 4).

% Found in: DTK
card_name('glade watcher', 'Glade Watcher').
card_type('glade watcher', 'Creature — Elemental').
card_types('glade watcher', ['Creature']).
card_subtypes('glade watcher', ['Elemental']).
card_colors('glade watcher', ['G']).
card_text('glade watcher', 'Defender\nFormidable — {G}: Glade Watcher can attack this turn as though it didn\'t have defender. Activate this ability only if creatures you control have total power 8 or greater.').
card_mana_cost('glade watcher', ['1', 'G']).
card_cmc('glade watcher', 2).
card_layout('glade watcher', 'normal').
card_power('glade watcher', 3).
card_toughness('glade watcher', 3).

% Found in: M12, M14
card_name('gladecover scout', 'Gladecover Scout').
card_type('gladecover scout', 'Creature — Elf Scout').
card_types('gladecover scout', ['Creature']).
card_subtypes('gladecover scout', ['Elf', 'Scout']).
card_colors('gladecover scout', ['G']).
card_text('gladecover scout', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('gladecover scout', ['G']).
card_cmc('gladecover scout', 1).
card_layout('gladecover scout', 'normal').
card_power('gladecover scout', 1).
card_toughness('gladecover scout', 1).

% Found in: SHM
card_name('glamer spinners', 'Glamer Spinners').
card_type('glamer spinners', 'Creature — Faerie Wizard').
card_types('glamer spinners', ['Creature']).
card_subtypes('glamer spinners', ['Faerie', 'Wizard']).
card_colors('glamer spinners', ['W', 'U']).
card_text('glamer spinners', 'Flash\nFlying\nWhen Glamer Spinners enters the battlefield, attach all Auras enchanting target permanent to another permanent with the same controller.').
card_mana_cost('glamer spinners', ['4', 'W/U']).
card_cmc('glamer spinners', 5).
card_layout('glamer spinners', 'normal').
card_power('glamer spinners', 2).
card_toughness('glamer spinners', 4).

% Found in: EVE
card_name('glamerdye', 'Glamerdye').
card_type('glamerdye', 'Instant').
card_types('glamerdye', ['Instant']).
card_subtypes('glamerdye', []).
card_colors('glamerdye', ['U']).
card_text('glamerdye', 'Change the text of target spell or permanent by replacing all instances of one color word with another.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('glamerdye', ['1', 'U']).
card_cmc('glamerdye', 2).
card_layout('glamerdye', 'normal').

% Found in: THS
card_name('glare of heresy', 'Glare of Heresy').
card_type('glare of heresy', 'Sorcery').
card_types('glare of heresy', ['Sorcery']).
card_subtypes('glare of heresy', []).
card_colors('glare of heresy', ['W']).
card_text('glare of heresy', 'Exile target white permanent.').
card_mana_cost('glare of heresy', ['1', 'W']).
card_cmc('glare of heresy', 2).
card_layout('glare of heresy', 'normal').

% Found in: RAV
card_name('glare of subdual', 'Glare of Subdual').
card_type('glare of subdual', 'Enchantment').
card_types('glare of subdual', ['Enchantment']).
card_subtypes('glare of subdual', []).
card_colors('glare of subdual', ['W', 'G']).
card_text('glare of subdual', 'Tap an untapped creature you control: Tap target artifact or creature.').
card_mana_cost('glare of subdual', ['2', 'G', 'W']).
card_cmc('glare of subdual', 4).
card_layout('glare of subdual', 'normal').

% Found in: ONS
card_name('glarecaster', 'Glarecaster').
card_type('glarecaster', 'Creature — Bird Cleric').
card_types('glarecaster', ['Creature']).
card_subtypes('glarecaster', ['Bird', 'Cleric']).
card_colors('glarecaster', ['W']).
card_text('glarecaster', 'Flying\n{5}{W}: The next time damage would be dealt to Glarecaster and/or you this turn, that damage is dealt to target creature or player instead.').
card_mana_cost('glarecaster', ['4', 'W', 'W']).
card_cmc('glarecaster', 6).
card_layout('glarecaster', 'normal').
card_power('glarecaster', 3).
card_toughness('glarecaster', 3).

% Found in: LRW
card_name('glarewielder', 'Glarewielder').
card_type('glarewielder', 'Creature — Elemental Shaman').
card_types('glarewielder', ['Creature']).
card_subtypes('glarewielder', ['Elemental', 'Shaman']).
card_colors('glarewielder', ['R']).
card_text('glarewielder', 'Haste\nWhen Glarewielder enters the battlefield, up to two target creatures can\'t block this turn.\nEvoke {1}{R} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('glarewielder', ['4', 'R']).
card_cmc('glarewielder', 5).
card_layout('glarewielder', 'normal').
card_power('glarewielder', 3).
card_toughness('glarewielder', 1).

% Found in: DTK
card_name('glaring aegis', 'Glaring Aegis').
card_type('glaring aegis', 'Enchantment — Aura').
card_types('glaring aegis', ['Enchantment']).
card_subtypes('glaring aegis', ['Aura']).
card_colors('glaring aegis', ['W']).
card_text('glaring aegis', 'Enchant creature\nWhen Glaring Aegis enters the battlefield, tap target creature an opponent controls.\nEnchanted creature gets +1/+3.').
card_mana_cost('glaring aegis', ['W']).
card_cmc('glaring aegis', 1).
card_layout('glaring aegis', 'normal').

% Found in: GTC
card_name('glaring spotlight', 'Glaring Spotlight').
card_type('glaring spotlight', 'Artifact').
card_types('glaring spotlight', ['Artifact']).
card_subtypes('glaring spotlight', []).
card_colors('glaring spotlight', []).
card_text('glaring spotlight', 'Creatures your opponents control with hexproof can be the targets of spells and abilities you control as though they didn\'t have hexproof.\n{3}, Sacrifice Glaring Spotlight: Creatures you control gain hexproof until end of turn and can\'t be blocked this turn.').
card_mana_cost('glaring spotlight', ['1']).
card_cmc('glaring spotlight', 1).
card_layout('glaring spotlight', 'normal').

% Found in: TSP
card_name('glass asp', 'Glass Asp').
card_type('glass asp', 'Creature — Snake').
card_types('glass asp', ['Creature']).
card_subtypes('glass asp', ['Snake']).
card_colors('glass asp', ['G']).
card_text('glass asp', 'Whenever Glass Asp deals combat damage to a player, that player loses 2 life at the beginning of his or her next draw step unless he or she pays {2} before that step.').
card_mana_cost('glass asp', ['1', 'G', 'G']).
card_cmc('glass asp', 3).
card_layout('glass asp', 'normal').
card_power('glass asp', 2).
card_toughness('glass asp', 1).

% Found in: RAV
card_name('glass golem', 'Glass Golem').
card_type('glass golem', 'Artifact Creature — Golem').
card_types('glass golem', ['Artifact', 'Creature']).
card_subtypes('glass golem', ['Golem']).
card_colors('glass golem', []).
card_text('glass golem', '').
card_mana_cost('glass golem', ['5']).
card_cmc('glass golem', 5).
card_layout('glass golem', 'normal').
card_power('glass golem', 6).
card_toughness('glass golem', 2).

% Found in: ARB, MM2
card_name('glassdust hulk', 'Glassdust Hulk').
card_type('glassdust hulk', 'Artifact Creature — Golem').
card_types('glassdust hulk', ['Artifact', 'Creature']).
card_subtypes('glassdust hulk', ['Golem']).
card_colors('glassdust hulk', ['W', 'U']).
card_text('glassdust hulk', 'Whenever another artifact enters the battlefield under your control, Glassdust Hulk gets +1/+1 until end of turn and can\'t be blocked this turn.\nCycling {W/U} ({W/U}, Discard this card: Draw a card.)').
card_mana_cost('glassdust hulk', ['3', 'W', 'U']).
card_cmc('glassdust hulk', 5).
card_layout('glassdust hulk', 'normal').
card_power('glassdust hulk', 3).
card_toughness('glassdust hulk', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, ITP, LEA, LEB, ME4, RQS
card_name('glasses of urza', 'Glasses of Urza').
card_type('glasses of urza', 'Artifact').
card_types('glasses of urza', ['Artifact']).
card_subtypes('glasses of urza', []).
card_colors('glasses of urza', []).
card_text('glasses of urza', '{T}: Look at target player\'s hand.').
card_mana_cost('glasses of urza', ['1']).
card_cmc('glasses of urza', 1).
card_layout('glasses of urza', 'normal').

% Found in: ALA
card_name('glaze fiend', 'Glaze Fiend').
card_type('glaze fiend', 'Artifact Creature — Illusion').
card_types('glaze fiend', ['Artifact', 'Creature']).
card_subtypes('glaze fiend', ['Illusion']).
card_colors('glaze fiend', ['B']).
card_text('glaze fiend', 'Flying\nWhenever another artifact enters the battlefield under your control, Glaze Fiend gets +2/+2 until end of turn.').
card_mana_cost('glaze fiend', ['1', 'B']).
card_cmc('glaze fiend', 2).
card_layout('glaze fiend', 'normal').
card_power('glaze fiend', 0).
card_toughness('glaze fiend', 1).

% Found in: DTK
card_name('gleam of authority', 'Gleam of Authority').
card_type('gleam of authority', 'Enchantment — Aura').
card_types('gleam of authority', ['Enchantment']).
card_subtypes('gleam of authority', ['Aura']).
card_colors('gleam of authority', ['W']).
card_text('gleam of authority', 'Enchant creature\nEnchanted creature gets +1/+1 for each +1/+1 counter on other creatures you control.\nEnchanted creature has vigilance and \"{W}, {T}: Bolster 1.\" (To bolster 1, choose a creature with the least toughness among creatures you control and put a +1/+1 counter on it.)').
card_mana_cost('gleam of authority', ['1', 'W']).
card_cmc('gleam of authority', 2).
card_layout('gleam of authority', 'normal').

% Found in: DGM
card_name('gleam of battle', 'Gleam of Battle').
card_type('gleam of battle', 'Enchantment').
card_types('gleam of battle', ['Enchantment']).
card_subtypes('gleam of battle', []).
card_colors('gleam of battle', ['W', 'R']).
card_text('gleam of battle', 'Whenever a creature you control attacks, put a +1/+1 counter on it.').
card_mana_cost('gleam of battle', ['4', 'R', 'W']).
card_cmc('gleam of battle', 6).
card_layout('gleam of battle', 'normal').

% Found in: CON, MMA
card_name('gleam of resistance', 'Gleam of Resistance').
card_type('gleam of resistance', 'Instant').
card_types('gleam of resistance', ['Instant']).
card_subtypes('gleam of resistance', []).
card_colors('gleam of resistance', ['W']).
card_text('gleam of resistance', 'Creatures you control get +1/+2 until end of turn. Untap those creatures.\nBasic landcycling {1}{W} ({1}{W}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('gleam of resistance', ['4', 'W']).
card_cmc('gleam of resistance', 5).
card_layout('gleam of resistance', 'normal').

% Found in: DDJ, RAV, pPRE
card_name('gleancrawler', 'Gleancrawler').
card_type('gleancrawler', 'Creature — Insect Horror').
card_types('gleancrawler', ['Creature']).
card_subtypes('gleancrawler', ['Insect', 'Horror']).
card_colors('gleancrawler', ['B', 'G']).
card_text('gleancrawler', '({B/G} can be paid with either {B} or {G}.)\nTrample\nAt the beginning of your end step, return to your hand all creature cards in your graveyard that were put there from the battlefield this turn.').
card_mana_cost('gleancrawler', ['3', 'B/G', 'B/G', 'B/G']).
card_cmc('gleancrawler', 6).
card_layout('gleancrawler', 'normal').
card_power('gleancrawler', 6).
card_toughness('gleancrawler', 6).

% Found in: ARC, SHM
card_name('gleeful sabotage', 'Gleeful Sabotage').
card_type('gleeful sabotage', 'Sorcery').
card_types('gleeful sabotage', ['Sorcery']).
card_subtypes('gleeful sabotage', []).
card_colors('gleeful sabotage', ['G']).
card_text('gleeful sabotage', 'Destroy target artifact or enchantment.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('gleeful sabotage', ['1', 'G']).
card_cmc('gleeful sabotage', 2).
card_layout('gleeful sabotage', 'normal').

% Found in: UNH
card_name('gleemax', 'Gleemax').
card_type('gleemax', 'Legendary Artifact').
card_types('gleemax', ['Artifact']).
card_subtypes('gleemax', []).
card_supertypes('gleemax', ['Legendary']).
card_colors('gleemax', []).
card_text('gleemax', 'You choose all targets for all spells and abilities.').
card_mana_cost('gleemax', ['1000000']).
card_cmc('gleemax', 1000000).
card_layout('gleemax', 'normal').

% Found in: PC2
card_name('glen elendra', 'Glen Elendra').
card_type('glen elendra', 'Plane — Lorwyn').
card_types('glen elendra', ['Plane']).
card_subtypes('glen elendra', ['Lorwyn']).
card_colors('glen elendra', []).
card_text('glen elendra', 'At end of combat, you may exchange control of target creature you control that dealt combat damage to a player this combat and target creature that player controls.\nWhenever you roll {C}, gain control of target creature you own.').
card_layout('glen elendra', 'plane').

% Found in: EVE, MMA
card_name('glen elendra archmage', 'Glen Elendra Archmage').
card_type('glen elendra archmage', 'Creature — Faerie Wizard').
card_types('glen elendra archmage', ['Creature']).
card_subtypes('glen elendra archmage', ['Faerie', 'Wizard']).
card_colors('glen elendra archmage', ['U']).
card_text('glen elendra archmage', 'Flying\n{U}, Sacrifice Glen Elendra Archmage: Counter target noncreature spell.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('glen elendra archmage', ['3', 'U']).
card_cmc('glen elendra archmage', 4).
card_layout('glen elendra archmage', 'normal').
card_power('glen elendra archmage', 2).
card_toughness('glen elendra archmage', 2).

% Found in: PC2, SHM
card_name('glen elendra liege', 'Glen Elendra Liege').
card_type('glen elendra liege', 'Creature — Faerie Knight').
card_types('glen elendra liege', ['Creature']).
card_subtypes('glen elendra liege', ['Faerie', 'Knight']).
card_colors('glen elendra liege', ['U', 'B']).
card_text('glen elendra liege', 'Flying\nOther blue creatures you control get +1/+1.\nOther black creatures you control get +1/+1.').
card_mana_cost('glen elendra liege', ['1', 'U/B', 'U/B', 'U/B']).
card_cmc('glen elendra liege', 4).
card_layout('glen elendra liege', 'normal').
card_power('glen elendra liege', 2).
card_toughness('glen elendra liege', 3).

% Found in: LRW
card_name('glen elendra pranksters', 'Glen Elendra Pranksters').
card_type('glen elendra pranksters', 'Creature — Faerie Wizard').
card_types('glen elendra pranksters', ['Creature']).
card_subtypes('glen elendra pranksters', ['Faerie', 'Wizard']).
card_colors('glen elendra pranksters', ['U']).
card_text('glen elendra pranksters', 'Flying\nWhenever you cast a spell during an opponent\'s turn, you may return target creature you control to its owner\'s hand.').
card_mana_cost('glen elendra pranksters', ['3', 'U']).
card_cmc('glen elendra pranksters', 4).
card_layout('glen elendra pranksters', 'normal').
card_power('glen elendra pranksters', 1).
card_toughness('glen elendra pranksters', 3).

% Found in: STH
card_name('gliding licid', 'Gliding Licid').
card_type('gliding licid', 'Creature — Licid').
card_types('gliding licid', ['Creature']).
card_subtypes('gliding licid', ['Licid']).
card_colors('gliding licid', ['U']).
card_text('gliding licid', '{U}, {T}: Gliding Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {U} to end this effect.\nEnchanted creature has flying.').
card_mana_cost('gliding licid', ['2', 'U']).
card_cmc('gliding licid', 3).
card_layout('gliding licid', 'normal').
card_power('gliding licid', 2).
card_toughness('gliding licid', 2).

% Found in: LRW
card_name('glimmerdust nap', 'Glimmerdust Nap').
card_type('glimmerdust nap', 'Enchantment — Aura').
card_types('glimmerdust nap', ['Enchantment']).
card_subtypes('glimmerdust nap', ['Aura']).
card_colors('glimmerdust nap', ['U']).
card_text('glimmerdust nap', 'Enchant tapped creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('glimmerdust nap', ['2', 'U']).
card_cmc('glimmerdust nap', 3).
card_layout('glimmerdust nap', 'normal').

% Found in: INV
card_name('glimmering angel', 'Glimmering Angel').
card_type('glimmering angel', 'Creature — Angel').
card_types('glimmering angel', ['Creature']).
card_subtypes('glimmering angel', ['Angel']).
card_colors('glimmering angel', ['W']).
card_text('glimmering angel', 'Flying\n{U}: Glimmering Angel gains shroud until end of turn. (It can\'t be the target of spells or abilities.)').
card_mana_cost('glimmering angel', ['3', 'W']).
card_cmc('glimmering angel', 4).
card_layout('glimmering angel', 'normal').
card_power('glimmering angel', 2).
card_toughness('glimmering angel', 2).

% Found in: CNS, SOM
card_name('glimmerpoint stag', 'Glimmerpoint Stag').
card_type('glimmerpoint stag', 'Creature — Elk').
card_types('glimmerpoint stag', ['Creature']).
card_subtypes('glimmerpoint stag', ['Elk']).
card_colors('glimmerpoint stag', ['W']).
card_text('glimmerpoint stag', 'Vigilance\nWhen Glimmerpoint Stag enters the battlefield, exile another target permanent. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('glimmerpoint stag', ['2', 'W', 'W']).
card_cmc('glimmerpoint stag', 4).
card_layout('glimmerpoint stag', 'normal').
card_power('glimmerpoint stag', 3).
card_toughness('glimmerpoint stag', 3).

% Found in: SOM
card_name('glimmerpost', 'Glimmerpost').
card_type('glimmerpost', 'Land — Locus').
card_types('glimmerpost', ['Land']).
card_subtypes('glimmerpost', ['Locus']).
card_colors('glimmerpost', []).
card_text('glimmerpost', 'When Glimmerpost enters the battlefield, you gain 1 life for each Locus on the battlefield.\n{T}: Add {1} to your mana pool.').
card_layout('glimmerpost', 'normal').

% Found in: MMA, MRD
card_name('glimmervoid', 'Glimmervoid').
card_type('glimmervoid', 'Land').
card_types('glimmervoid', ['Land']).
card_subtypes('glimmervoid', []).
card_colors('glimmervoid', []).
card_text('glimmervoid', 'At the beginning of the end step, if you control no artifacts, sacrifice Glimmervoid.\n{T}: Add one mana of any color to your mana pool.').
card_layout('glimmervoid', 'normal').

% Found in: HOP
card_name('glimmervoid basin', 'Glimmervoid Basin').
card_type('glimmervoid basin', 'Plane — Mirrodin').
card_types('glimmervoid basin', ['Plane']).
card_subtypes('glimmervoid basin', ['Mirrodin']).
card_colors('glimmervoid basin', []).
card_text('glimmervoid basin', 'Whenever a player casts an instant or sorcery spell with a single target, he or she copies that spell for each other spell, permanent, card not on the battlefield, and/or player the spell could target. Each copy targets a different one of them.\nWhenever you roll {C}, choose target creature. Each player except that creature\'s controller puts a token that\'s a copy of that creature onto the battlefield.').
card_layout('glimmervoid basin', 'plane').

% Found in: CHK
card_name('glimpse of nature', 'Glimpse of Nature').
card_type('glimpse of nature', 'Sorcery').
card_types('glimpse of nature', ['Sorcery']).
card_subtypes('glimpse of nature', []).
card_colors('glimpse of nature', ['G']).
card_text('glimpse of nature', 'Whenever you cast a creature spell this turn, draw a card.').
card_mana_cost('glimpse of nature', ['G']).
card_cmc('glimpse of nature', 1).
card_layout('glimpse of nature', 'normal').

% Found in: M14
card_name('glimpse the future', 'Glimpse the Future').
card_type('glimpse the future', 'Sorcery').
card_types('glimpse the future', ['Sorcery']).
card_subtypes('glimpse the future', []).
card_colors('glimpse the future', ['U']).
card_text('glimpse the future', 'Look at the top three cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_mana_cost('glimpse the future', ['2', 'U']).
card_cmc('glimpse the future', 3).
card_layout('glimpse the future', 'normal').

% Found in: BNG
card_name('glimpse the sun god', 'Glimpse the Sun God').
card_type('glimpse the sun god', 'Instant').
card_types('glimpse the sun god', ['Instant']).
card_subtypes('glimpse the sun god', []).
card_colors('glimpse the sun god', ['W']).
card_text('glimpse the sun god', 'Tap X target creatures. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('glimpse the sun god', ['X', 'W']).
card_cmc('glimpse the sun god', 1).
card_layout('glimpse the sun god', 'normal').

% Found in: RAV
card_name('glimpse the unthinkable', 'Glimpse the Unthinkable').
card_type('glimpse the unthinkable', 'Sorcery').
card_types('glimpse the unthinkable', ['Sorcery']).
card_subtypes('glimpse the unthinkable', []).
card_colors('glimpse the unthinkable', ['U', 'B']).
card_text('glimpse the unthinkable', 'Target player puts the top ten cards of his or her library into his or her graveyard.').
card_mana_cost('glimpse the unthinkable', ['U', 'B']).
card_cmc('glimpse the unthinkable', 2).
card_layout('glimpse the unthinkable', 'normal').

% Found in: DTK
card_name('glint', 'Glint').
card_type('glint', 'Instant').
card_types('glint', ['Instant']).
card_subtypes('glint', []).
card_colors('glint', ['U']).
card_text('glint', 'Target creature you control gets +0/+3 and gains hexproof until end of turn. (It can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('glint', ['1', 'U']).
card_cmc('glint', 2).
card_layout('glint', 'normal').

% Found in: SOM
card_name('glint hawk', 'Glint Hawk').
card_type('glint hawk', 'Creature — Bird').
card_types('glint hawk', ['Creature']).
card_subtypes('glint hawk', ['Bird']).
card_colors('glint hawk', ['W']).
card_text('glint hawk', 'Flying\nWhen Glint Hawk enters the battlefield, sacrifice it unless you return an artifact you control to its owner\'s hand.').
card_mana_cost('glint hawk', ['W']).
card_cmc('glint hawk', 1).
card_layout('glint hawk', 'normal').
card_power('glint hawk', 2).
card_toughness('glint hawk', 2).

% Found in: MM2, SOM
card_name('glint hawk idol', 'Glint Hawk Idol').
card_type('glint hawk idol', 'Artifact').
card_types('glint hawk idol', ['Artifact']).
card_subtypes('glint hawk idol', []).
card_colors('glint hawk idol', []).
card_text('glint hawk idol', 'Whenever another artifact enters the battlefield under your control, you may have Glint Hawk Idol become a 2/2 Bird artifact creature with flying until end of turn.\n{W}: Glint Hawk Idol becomes a 2/2 Bird artifact creature with flying until end of turn.').
card_mana_cost('glint hawk idol', ['2']).
card_cmc('glint hawk idol', 2).
card_layout('glint hawk idol', 'normal').

% Found in: GPT
card_name('glint-eye nephilim', 'Glint-Eye Nephilim').
card_type('glint-eye nephilim', 'Creature — Nephilim').
card_types('glint-eye nephilim', ['Creature']).
card_subtypes('glint-eye nephilim', ['Nephilim']).
card_colors('glint-eye nephilim', ['U', 'B', 'R', 'G']).
card_text('glint-eye nephilim', 'Whenever Glint-Eye Nephilim deals combat damage to a player, draw that many cards.\n{1}, Discard a card: Glint-Eye Nephilim gets +1/+1 until end of turn.').
card_mana_cost('glint-eye nephilim', ['U', 'B', 'R', 'G']).
card_cmc('glint-eye nephilim', 4).
card_layout('glint-eye nephilim', 'normal').
card_power('glint-eye nephilim', 2).
card_toughness('glint-eye nephilim', 2).

% Found in: LGN
card_name('glintwing invoker', 'Glintwing Invoker').
card_type('glintwing invoker', 'Creature — Human Wizard Mutant').
card_types('glintwing invoker', ['Creature']).
card_subtypes('glintwing invoker', ['Human', 'Wizard', 'Mutant']).
card_colors('glintwing invoker', ['U']).
card_text('glintwing invoker', '{7}{U}: Glintwing Invoker gets +3/+3 and gains flying until end of turn.').
card_mana_cost('glintwing invoker', ['4', 'U']).
card_cmc('glintwing invoker', 5).
card_layout('glintwing invoker', 'normal').
card_power('glintwing invoker', 3).
card_toughness('glintwing invoker', 3).

% Found in: MRD
card_name('glissa sunseeker', 'Glissa Sunseeker').
card_type('glissa sunseeker', 'Legendary Creature — Elf').
card_types('glissa sunseeker', ['Creature']).
card_subtypes('glissa sunseeker', ['Elf']).
card_supertypes('glissa sunseeker', ['Legendary']).
card_colors('glissa sunseeker', ['G']).
card_text('glissa sunseeker', 'First strike\n{T}: Destroy target artifact if its converted mana cost is equal to the amount of mana in your mana pool.').
card_mana_cost('glissa sunseeker', ['2', 'G', 'G']).
card_cmc('glissa sunseeker', 4).
card_layout('glissa sunseeker', 'normal').
card_power('glissa sunseeker', 3).
card_toughness('glissa sunseeker', 2).

% Found in: MBS
card_name('glissa\'s courier', 'Glissa\'s Courier').
card_type('glissa\'s courier', 'Creature — Horror').
card_types('glissa\'s courier', ['Creature']).
card_subtypes('glissa\'s courier', ['Horror']).
card_colors('glissa\'s courier', ['G']).
card_text('glissa\'s courier', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('glissa\'s courier', ['1', 'G', 'G']).
card_cmc('glissa\'s courier', 3).
card_layout('glissa\'s courier', 'normal').
card_power('glissa\'s courier', 2).
card_toughness('glissa\'s courier', 3).

% Found in: NPH
card_name('glissa\'s scorn', 'Glissa\'s Scorn').
card_type('glissa\'s scorn', 'Instant').
card_types('glissa\'s scorn', ['Instant']).
card_subtypes('glissa\'s scorn', []).
card_colors('glissa\'s scorn', ['G']).
card_text('glissa\'s scorn', 'Destroy target artifact. Its controller loses 1 life.').
card_mana_cost('glissa\'s scorn', ['1', 'G']).
card_cmc('glissa\'s scorn', 2).
card_layout('glissa\'s scorn', 'normal').

% Found in: MBS, pPRE
card_name('glissa, the traitor', 'Glissa, the Traitor').
card_type('glissa, the traitor', 'Legendary Creature — Zombie Elf').
card_types('glissa, the traitor', ['Creature']).
card_subtypes('glissa, the traitor', ['Zombie', 'Elf']).
card_supertypes('glissa, the traitor', ['Legendary']).
card_colors('glissa, the traitor', ['B', 'G']).
card_text('glissa, the traitor', 'First strike, deathtouch\nWhenever a creature an opponent controls dies, you may return target artifact card from your graveyard to your hand.').
card_mana_cost('glissa, the traitor', ['B', 'G', 'G']).
card_cmc('glissa, the traitor', 3).
card_layout('glissa, the traitor', 'normal').
card_power('glissa, the traitor', 3).
card_toughness('glissa, the traitor', 3).

% Found in: NPH, pFNM
card_name('glistener elf', 'Glistener Elf').
card_type('glistener elf', 'Creature — Elf Warrior').
card_types('glistener elf', ['Creature']).
card_subtypes('glistener elf', ['Elf', 'Warrior']).
card_colors('glistener elf', ['G']).
card_text('glistener elf', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('glistener elf', ['G']).
card_cmc('glistener elf', 1).
card_layout('glistener elf', 'normal').
card_power('glistener elf', 1).
card_toughness('glistener elf', 1).

% Found in: NPH
card_name('glistening oil', 'Glistening Oil').
card_type('glistening oil', 'Enchantment — Aura').
card_types('glistening oil', ['Enchantment']).
card_subtypes('glistening oil', ['Aura']).
card_colors('glistening oil', ['B']).
card_text('glistening oil', 'Enchant creature\nEnchanted creature has infect.\nAt the beginning of your upkeep, put a -1/-1 counter on enchanted creature.\nWhen Glistening Oil is put into a graveyard from the battlefield, return Glistening Oil to its owner\'s hand.').
card_mana_cost('glistening oil', ['B', 'B']).
card_cmc('glistening oil', 2).
card_layout('glistening oil', 'normal').

% Found in: SOK
card_name('glitterfang', 'Glitterfang').
card_type('glitterfang', 'Creature — Spirit').
card_types('glitterfang', ['Creature']).
card_subtypes('glitterfang', ['Spirit']).
card_colors('glitterfang', ['R']).
card_text('glitterfang', 'Haste\nAt the beginning of the end step, return Glitterfang to its owner\'s hand.').
card_mana_cost('glitterfang', ['R']).
card_cmc('glitterfang', 1).
card_layout('glitterfang', 'normal').
card_power('glitterfang', 1).
card_toughness('glitterfang', 1).

% Found in: PCY
card_name('glittering lion', 'Glittering Lion').
card_type('glittering lion', 'Creature — Cat').
card_types('glittering lion', ['Creature']).
card_subtypes('glittering lion', ['Cat']).
card_colors('glittering lion', ['W']).
card_text('glittering lion', 'Prevent all damage that would be dealt to Glittering Lion.\n{3}: Until end of turn, Glittering Lion loses \"Prevent all damage that would be dealt to Glittering Lion.\" Any player may activate this ability.').
card_mana_cost('glittering lion', ['2', 'W']).
card_cmc('glittering lion', 3).
card_layout('glittering lion', 'normal').
card_power('glittering lion', 2).
card_toughness('glittering lion', 2).

% Found in: PCY
card_name('glittering lynx', 'Glittering Lynx').
card_type('glittering lynx', 'Creature — Cat').
card_types('glittering lynx', ['Creature']).
card_subtypes('glittering lynx', ['Cat']).
card_colors('glittering lynx', ['W']).
card_text('glittering lynx', 'Prevent all damage that would be dealt to Glittering Lynx.\n{2}: Until end of turn, Glittering Lynx loses \"Prevent all damage that would be dealt to Glittering Lynx.\" Any player may activate this ability.').
card_mana_cost('glittering lynx', ['W']).
card_cmc('glittering lynx', 1).
card_layout('glittering lynx', 'normal').
card_power('glittering lynx', 1).
card_toughness('glittering lynx', 1).

% Found in: FUT
card_name('glittering wish', 'Glittering Wish').
card_type('glittering wish', 'Sorcery').
card_types('glittering wish', ['Sorcery']).
card_subtypes('glittering wish', []).
card_colors('glittering wish', ['W', 'G']).
card_text('glittering wish', 'You may choose a multicolored card you own from outside the game, reveal that card, and put it into your hand. Exile Glittering Wish.').
card_mana_cost('glittering wish', ['G', 'W']).
card_cmc('glittering wish', 2).
card_layout('glittering wish', 'normal').

% Found in: INV
card_name('global ruin', 'Global Ruin').
card_type('global ruin', 'Sorcery').
card_types('global ruin', ['Sorcery']).
card_subtypes('global ruin', []).
card_colors('global ruin', ['W']).
card_text('global ruin', 'Each player chooses from the lands he or she controls a land of each basic land type, then sacrifices the rest.').
card_mana_cost('global ruin', ['4', 'W']).
card_cmc('global ruin', 5).
card_layout('global ruin', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('gloom', 'Gloom').
card_type('gloom', 'Enchantment').
card_types('gloom', ['Enchantment']).
card_subtypes('gloom', []).
card_colors('gloom', ['B']).
card_text('gloom', 'White spells cost {3} more to cast.\nActivated abilities of white enchantments cost {3} more to activate.').
card_mana_cost('gloom', ['2', 'B']).
card_cmc('gloom', 3).
card_layout('gloom', 'normal').

% Found in: AVR
card_name('gloom surgeon', 'Gloom Surgeon').
card_type('gloom surgeon', 'Creature — Spirit').
card_types('gloom surgeon', ['Creature']).
card_subtypes('gloom surgeon', ['Spirit']).
card_colors('gloom surgeon', ['B']).
card_text('gloom surgeon', 'If combat damage would be dealt to Gloom Surgeon, prevent that damage and exile that many cards from the top of your library.').
card_mana_cost('gloom surgeon', ['1', 'B']).
card_cmc('gloom surgeon', 2).
card_layout('gloom surgeon', 'normal').
card_power('gloom surgeon', 2).
card_toughness('gloom surgeon', 1).

% Found in: TOR
card_name('gloomdrifter', 'Gloomdrifter').
card_type('gloomdrifter', 'Creature — Zombie Minion').
card_types('gloomdrifter', ['Creature']).
card_subtypes('gloomdrifter', ['Zombie', 'Minion']).
card_colors('gloomdrifter', ['B']).
card_text('gloomdrifter', 'Flying\nThreshold — As long as seven or more cards are in your graveyard, Gloomdrifter has \"When Gloomdrifter enters the battlefield, nonblack creatures get -2/-2 until end of turn.\"').
card_mana_cost('gloomdrifter', ['3', 'B']).
card_cmc('gloomdrifter', 4).
card_layout('gloomdrifter', 'normal').
card_power('gloomdrifter', 2).
card_toughness('gloomdrifter', 2).

% Found in: ROE
card_name('gloomhunter', 'Gloomhunter').
card_type('gloomhunter', 'Creature — Bat').
card_types('gloomhunter', ['Creature']).
card_subtypes('gloomhunter', ['Bat']).
card_colors('gloomhunter', ['B']).
card_text('gloomhunter', 'Flying').
card_mana_cost('gloomhunter', ['2', 'B']).
card_cmc('gloomhunter', 3).
card_layout('gloomhunter', 'normal').
card_power('gloomhunter', 2).
card_toughness('gloomhunter', 1).

% Found in: SHM
card_name('gloomlance', 'Gloomlance').
card_type('gloomlance', 'Sorcery').
card_types('gloomlance', ['Sorcery']).
card_subtypes('gloomlance', []).
card_colors('gloomlance', ['B']).
card_text('gloomlance', 'Destroy target creature. If that creature was green or white, its controller discards a card.').
card_mana_cost('gloomlance', ['3', 'B', 'B']).
card_cmc('gloomlance', 5).
card_layout('gloomlance', 'normal').

% Found in: AVR, SHM
card_name('gloomwidow', 'Gloomwidow').
card_type('gloomwidow', 'Creature — Spider').
card_types('gloomwidow', ['Creature']).
card_subtypes('gloomwidow', ['Spider']).
card_colors('gloomwidow', ['G']).
card_text('gloomwidow', 'Reach\nGloomwidow can block only creatures with flying.').
card_mana_cost('gloomwidow', ['2', 'G']).
card_cmc('gloomwidow', 3).
card_layout('gloomwidow', 'normal').
card_power('gloomwidow', 3).
card_toughness('gloomwidow', 3).

% Found in: SHM
card_name('gloomwidow\'s feast', 'Gloomwidow\'s Feast').
card_type('gloomwidow\'s feast', 'Instant').
card_types('gloomwidow\'s feast', ['Instant']).
card_subtypes('gloomwidow\'s feast', []).
card_colors('gloomwidow\'s feast', ['G']).
card_text('gloomwidow\'s feast', 'Destroy target creature with flying. If that creature was blue or black, put a 1/2 green Spider creature token with reach onto the battlefield. (It can block creatures with flying.)').
card_mana_cost('gloomwidow\'s feast', ['3', 'G']).
card_cmc('gloomwidow\'s feast', 4).
card_layout('gloomwidow\'s feast', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, USG, pSUS
card_name('glorious anthem', 'Glorious Anthem').
card_type('glorious anthem', 'Enchantment').
card_types('glorious anthem', ['Enchantment']).
card_subtypes('glorious anthem', []).
card_colors('glorious anthem', ['W']).
card_text('glorious anthem', 'Creatures you control get +1/+1.').
card_mana_cost('glorious anthem', ['1', 'W', 'W']).
card_cmc('glorious anthem', 3).
card_layout('glorious anthem', 'normal').

% Found in: M10, M13
card_name('glorious charge', 'Glorious Charge').
card_type('glorious charge', 'Instant').
card_types('glorious charge', ['Instant']).
card_subtypes('glorious charge', []).
card_colors('glorious charge', ['W']).
card_text('glorious charge', 'Creatures you control get +1/+1 until end of turn.').
card_mana_cost('glorious charge', ['1', 'W']).
card_cmc('glorious charge', 2).
card_layout('glorious charge', 'normal').

% Found in: JUD, pPRE
card_name('glory', 'Glory').
card_type('glory', 'Creature — Incarnation').
card_types('glory', ['Creature']).
card_subtypes('glory', ['Incarnation']).
card_colors('glory', ['W']).
card_text('glory', 'Flying\n{2}{W}: Choose a color. Creatures you control gain protection from the chosen color until end of turn. Activate this ability only if Glory is in your graveyard.').
card_mana_cost('glory', ['3', 'W', 'W']).
card_cmc('glory', 5).
card_layout('glory', 'normal').
card_power('glory', 3).
card_toughness('glory', 3).

% Found in: ARB, HOP
card_name('glory of warfare', 'Glory of Warfare').
card_type('glory of warfare', 'Enchantment').
card_types('glory of warfare', ['Enchantment']).
card_subtypes('glory of warfare', []).
card_colors('glory of warfare', ['W', 'R']).
card_text('glory of warfare', 'As long as it\'s your turn, creatures you control get +2/+0.\nAs long as it\'s not your turn, creatures you control get +0/+2.').
card_mana_cost('glory of warfare', ['2', 'R', 'W']).
card_cmc('glory of warfare', 4).
card_layout('glory of warfare', 'normal').

% Found in: 8ED, 9ED, DDF, ONS, ROE
card_name('glory seeker', 'Glory Seeker').
card_type('glory seeker', 'Creature — Human Soldier').
card_types('glory seeker', ['Creature']).
card_subtypes('glory seeker', ['Human', 'Soldier']).
card_colors('glory seeker', ['W']).
card_text('glory seeker', '').
card_mana_cost('glory seeker', ['1', 'W']).
card_cmc('glory seeker', 2).
card_layout('glory seeker', 'normal').
card_power('glory seeker', 2).
card_toughness('glory seeker', 2).

% Found in: ARB
card_name('gloryscale viashino', 'Gloryscale Viashino').
card_type('gloryscale viashino', 'Creature — Viashino Soldier').
card_types('gloryscale viashino', ['Creature']).
card_subtypes('gloryscale viashino', ['Viashino', 'Soldier']).
card_colors('gloryscale viashino', ['W', 'R', 'G']).
card_text('gloryscale viashino', 'Whenever you cast a multicolored spell, Gloryscale Viashino gets +3/+3 until end of turn.').
card_mana_cost('gloryscale viashino', ['1', 'R', 'G', 'W']).
card_cmc('gloryscale viashino', 4).
card_layout('gloryscale viashino', 'normal').
card_power('gloryscale viashino', 3).
card_toughness('gloryscale viashino', 3).

% Found in: LGN
card_name('glowering rogon', 'Glowering Rogon').
card_type('glowering rogon', 'Creature — Beast').
card_types('glowering rogon', ['Creature']).
card_subtypes('glowering rogon', ['Beast']).
card_colors('glowering rogon', ['G']).
card_text('glowering rogon', 'Amplify 1 (As this creature enters the battlefield, put a +1/+1 counter on it for each Beast card you reveal in your hand.)').
card_mana_cost('glowering rogon', ['5', 'G']).
card_cmc('glowering rogon', 6).
card_layout('glowering rogon', 'normal').
card_power('glowering rogon', 4).
card_toughness('glowering rogon', 4).

% Found in: MMQ
card_name('glowing anemone', 'Glowing Anemone').
card_type('glowing anemone', 'Creature — Jellyfish Beast').
card_types('glowing anemone', ['Creature']).
card_subtypes('glowing anemone', ['Jellyfish', 'Beast']).
card_colors('glowing anemone', ['U']).
card_text('glowing anemone', 'When Glowing Anemone enters the battlefield, you may return target land to its owner\'s hand.').
card_mana_cost('glowing anemone', ['3', 'U']).
card_cmc('glowing anemone', 4).
card_layout('glowing anemone', 'normal').
card_power('glowing anemone', 1).
card_toughness('glowing anemone', 3).

% Found in: LGN
card_name('glowrider', 'Glowrider').
card_type('glowrider', 'Creature — Human Cleric').
card_types('glowrider', ['Creature']).
card_subtypes('glowrider', ['Human', 'Cleric']).
card_colors('glowrider', ['W']).
card_text('glowrider', 'Noncreature spells cost {1} more to cast.').
card_mana_cost('glowrider', ['2', 'W']).
card_cmc('glowrider', 3).
card_layout('glowrider', 'normal').
card_power('glowrider', 2).
card_toughness('glowrider', 1).

% Found in: UNH
card_name('gluetius maximus', 'Gluetius Maximus').
card_type('gluetius maximus', 'Creature — Beast').
card_types('gluetius maximus', ['Creature']).
card_subtypes('gluetius maximus', ['Beast']).
card_colors('gluetius maximus', ['G']).
card_text('gluetius maximus', 'As Gluetius Maximus comes into play, an opponent chooses one of your fingers. (Thumbs are fingers, too.)\nWhen the chosen finger isn\'t touching Gluetius Maximus, sacrifice Gluetius Maximus.').
card_mana_cost('gluetius maximus', ['3', 'G', 'G']).
card_cmc('gluetius maximus', 5).
card_layout('gluetius maximus', 'normal').
card_power('gluetius maximus', 5).
card_toughness('gluetius maximus', 5).

% Found in: JOU
card_name('gluttonous cyclops', 'Gluttonous Cyclops').
card_type('gluttonous cyclops', 'Creature — Cyclops').
card_types('gluttonous cyclops', ['Creature']).
card_subtypes('gluttonous cyclops', ['Cyclops']).
card_colors('gluttonous cyclops', ['R']).
card_text('gluttonous cyclops', '{5}{R}{R}: Monstrosity 3. (If this creature isn\'t monstrous, put three +1/+1 counters on it and it becomes monstrous.)').
card_mana_cost('gluttonous cyclops', ['5', 'R']).
card_cmc('gluttonous cyclops', 6).
card_layout('gluttonous cyclops', 'normal').
card_power('gluttonous cyclops', 5).
card_toughness('gluttonous cyclops', 4).

% Found in: CON, PC2
card_name('gluttonous slime', 'Gluttonous Slime').
card_type('gluttonous slime', 'Creature — Ooze').
card_types('gluttonous slime', ['Creature']).
card_subtypes('gluttonous slime', ['Ooze']).
card_colors('gluttonous slime', ['G']).
card_text('gluttonous slime', 'Flash\nDevour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)').
card_mana_cost('gluttonous slime', ['2', 'G']).
card_cmc('gluttonous slime', 3).
card_layout('gluttonous slime', 'normal').
card_power('gluttonous slime', 2).
card_toughness('gluttonous slime', 2).

% Found in: 8ED, 9ED, ONS
card_name('gluttonous zombie', 'Gluttonous Zombie').
card_type('gluttonous zombie', 'Creature — Zombie').
card_types('gluttonous zombie', ['Creature']).
card_subtypes('gluttonous zombie', ['Zombie']).
card_colors('gluttonous zombie', ['B']).
card_text('gluttonous zombie', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('gluttonous zombie', ['4', 'B']).
card_cmc('gluttonous zombie', 5).
card_layout('gluttonous zombie', 'normal').
card_power('gluttonous zombie', 3).
card_toughness('gluttonous zombie', 3).

% Found in: LEG
card_name('glyph of delusion', 'Glyph of Delusion').
card_type('glyph of delusion', 'Instant').
card_types('glyph of delusion', ['Instant']).
card_subtypes('glyph of delusion', []).
card_colors('glyph of delusion', ['U']).
card_text('glyph of delusion', 'Put X glyph counters on target creature that target Wall blocked this turn, where X is the power of that blocked creature. The creature gains \"This creature doesn\'t untap during your untap step if it has a glyph counter on it\" and \"At the beginning of your upkeep, remove a glyph counter from this creature.\"').
card_mana_cost('glyph of delusion', ['U']).
card_cmc('glyph of delusion', 1).
card_layout('glyph of delusion', 'normal').

% Found in: LEG
card_name('glyph of destruction', 'Glyph of Destruction').
card_type('glyph of destruction', 'Instant').
card_types('glyph of destruction', ['Instant']).
card_subtypes('glyph of destruction', []).
card_colors('glyph of destruction', ['R']).
card_text('glyph of destruction', 'Target blocking Wall you control gets +10/+0 until end of combat. Prevent all damage that would be dealt to it this turn. Destroy it at the beginning of the next end step.').
card_mana_cost('glyph of destruction', ['R']).
card_cmc('glyph of destruction', 1).
card_layout('glyph of destruction', 'normal').

% Found in: LEG
card_name('glyph of doom', 'Glyph of Doom').
card_type('glyph of doom', 'Instant').
card_types('glyph of doom', ['Instant']).
card_subtypes('glyph of doom', []).
card_colors('glyph of doom', ['B']).
card_text('glyph of doom', 'Choose target Wall creature. At this turn\'s next end of combat, destroy all creatures that were blocked by that creature this turn.').
card_mana_cost('glyph of doom', ['B']).
card_cmc('glyph of doom', 1).
card_layout('glyph of doom', 'normal').

% Found in: LEG
card_name('glyph of life', 'Glyph of Life').
card_type('glyph of life', 'Instant').
card_types('glyph of life', ['Instant']).
card_subtypes('glyph of life', []).
card_colors('glyph of life', ['W']).
card_text('glyph of life', 'Choose target Wall creature. Whenever that creature is dealt damage by an attacking creature this turn, you gain that much life.').
card_mana_cost('glyph of life', ['W']).
card_cmc('glyph of life', 1).
card_layout('glyph of life', 'normal').

% Found in: LEG
card_name('glyph of reincarnation', 'Glyph of Reincarnation').
card_type('glyph of reincarnation', 'Instant').
card_types('glyph of reincarnation', ['Instant']).
card_subtypes('glyph of reincarnation', []).
card_colors('glyph of reincarnation', ['G']).
card_text('glyph of reincarnation', 'Cast Glyph of Reincarnation only after combat.\nDestroy all creatures that were blocked by target Wall this turn. They can\'t be regenerated. For each creature that died this way, put a creature card from the graveyard of the player who controlled that creature the last time it became blocked by that Wall onto the battlefield under its owner\'s control.').
card_mana_cost('glyph of reincarnation', ['G']).
card_cmc('glyph of reincarnation', 1).
card_layout('glyph of reincarnation', 'normal').

% Found in: SHM
card_name('gnarled effigy', 'Gnarled Effigy').
card_type('gnarled effigy', 'Artifact').
card_types('gnarled effigy', ['Artifact']).
card_subtypes('gnarled effigy', []).
card_colors('gnarled effigy', []).
card_text('gnarled effigy', '{4}, {T}: Put a -1/-1 counter on target creature.').
card_mana_cost('gnarled effigy', ['4']).
card_cmc('gnarled effigy', 4).
card_layout('gnarled effigy', 'normal').

% Found in: BOK
card_name('gnarled mass', 'Gnarled Mass').
card_type('gnarled mass', 'Creature — Spirit').
card_types('gnarled mass', ['Creature']).
card_subtypes('gnarled mass', ['Spirit']).
card_colors('gnarled mass', ['G']).
card_text('gnarled mass', '').
card_mana_cost('gnarled mass', ['1', 'G', 'G']).
card_cmc('gnarled mass', 3).
card_layout('gnarled mass', 'normal').
card_power('gnarled mass', 3).
card_toughness('gnarled mass', 3).

% Found in: JOU
card_name('gnarled scarhide', 'Gnarled Scarhide').
card_type('gnarled scarhide', 'Enchantment Creature — Minotaur').
card_types('gnarled scarhide', ['Enchantment', 'Creature']).
card_subtypes('gnarled scarhide', ['Minotaur']).
card_colors('gnarled scarhide', ['B']).
card_text('gnarled scarhide', 'Bestow {3}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nGnarled Scarhide can\'t block.\nEnchanted creature gets +2/+1 and can\'t block.').
card_mana_cost('gnarled scarhide', ['B']).
card_cmc('gnarled scarhide', 1).
card_layout('gnarled scarhide', 'normal').
card_power('gnarled scarhide', 2).
card_toughness('gnarled scarhide', 1).

% Found in: CNS, MM2, WWK
card_name('gnarlid pack', 'Gnarlid Pack').
card_type('gnarlid pack', 'Creature — Beast').
card_types('gnarlid pack', ['Creature']).
card_subtypes('gnarlid pack', ['Beast']).
card_colors('gnarlid pack', ['G']).
card_text('gnarlid pack', 'Multikicker {1}{G} (You may pay an additional {1}{G} any number of times as you cast this spell.)\nGnarlid Pack enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_mana_cost('gnarlid pack', ['1', 'G']).
card_cmc('gnarlid pack', 2).
card_layout('gnarlid pack', 'normal').
card_power('gnarlid pack', 2).
card_toughness('gnarlid pack', 2).

% Found in: ORI
card_name('gnarlroot trapper', 'Gnarlroot Trapper').
card_type('gnarlroot trapper', 'Creature — Elf Druid').
card_types('gnarlroot trapper', ['Creature']).
card_subtypes('gnarlroot trapper', ['Elf', 'Druid']).
card_colors('gnarlroot trapper', ['B']).
card_text('gnarlroot trapper', '{T}, Pay 1 life: Add {G} to your mana pool. Spend this mana only to cast an Elf creature spell.\n{T}: Target attacking Elf you control gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('gnarlroot trapper', ['B']).
card_cmc('gnarlroot trapper', 1).
card_layout('gnarlroot trapper', 'normal').
card_power('gnarlroot trapper', 1).
card_toughness('gnarlroot trapper', 1).

% Found in: DIS
card_name('gnat alley creeper', 'Gnat Alley Creeper').
card_type('gnat alley creeper', 'Creature — Human Rogue').
card_types('gnat alley creeper', ['Creature']).
card_subtypes('gnat alley creeper', ['Human', 'Rogue']).
card_colors('gnat alley creeper', ['R']).
card_text('gnat alley creeper', 'Gnat Alley Creeper can\'t be blocked by creatures with flying.').
card_mana_cost('gnat alley creeper', ['2', 'R']).
card_cmc('gnat alley creeper', 3).
card_layout('gnat alley creeper', 'normal').
card_power('gnat alley creeper', 3).
card_toughness('gnat alley creeper', 1).

% Found in: SOK
card_name('gnat miser', 'Gnat Miser').
card_type('gnat miser', 'Creature — Rat Shaman').
card_types('gnat miser', ['Creature']).
card_subtypes('gnat miser', ['Rat', 'Shaman']).
card_colors('gnat miser', ['B']).
card_text('gnat miser', 'Each opponent\'s maximum hand size is reduced by one.').
card_mana_cost('gnat miser', ['B']).
card_cmc('gnat miser', 1).
card_layout('gnat miser', 'normal').
card_power('gnat miser', 1).
card_toughness('gnat miser', 1).

% Found in: MBS
card_name('gnathosaur', 'Gnathosaur').
card_type('gnathosaur', 'Creature — Lizard').
card_types('gnathosaur', ['Creature']).
card_subtypes('gnathosaur', ['Lizard']).
card_colors('gnathosaur', ['R']).
card_text('gnathosaur', 'Sacrifice an artifact: Gnathosaur gains trample until end of turn.').
card_mana_cost('gnathosaur', ['4', 'R', 'R']).
card_cmc('gnathosaur', 6).
card_layout('gnathosaur', 'normal').
card_power('gnathosaur', 5).
card_toughness('gnathosaur', 4).

% Found in: ISD
card_name('gnaw to the bone', 'Gnaw to the Bone').
card_type('gnaw to the bone', 'Instant').
card_types('gnaw to the bone', ['Instant']).
card_subtypes('gnaw to the bone', []).
card_colors('gnaw to the bone', ['G']).
card_text('gnaw to the bone', 'You gain 2 life for each creature card in your graveyard.\nFlashback {2}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('gnaw to the bone', ['2', 'G']).
card_cmc('gnaw to the bone', 3).
card_layout('gnaw to the bone', 'normal').

% Found in: M14
card_name('gnawing zombie', 'Gnawing Zombie').
card_type('gnawing zombie', 'Creature — Zombie').
card_types('gnawing zombie', ['Creature']).
card_subtypes('gnawing zombie', ['Zombie']).
card_colors('gnawing zombie', ['B']).
card_text('gnawing zombie', '{1}{B}, Sacrifice a creature: Target player loses 1 life and you gain 1 life.').
card_mana_cost('gnawing zombie', ['1', 'B']).
card_cmc('gnawing zombie', 2).
card_layout('gnawing zombie', 'normal').
card_power('gnawing zombie', 1).
card_toughness('gnawing zombie', 3).

% Found in: MBS, pFNM
card_name('go for the throat', 'Go for the Throat').
card_type('go for the throat', 'Instant').
card_types('go for the throat', ['Instant']).
card_subtypes('go for the throat', []).
card_colors('go for the throat', ['B']).
card_text('go for the throat', 'Destroy target nonartifact creature.').
card_mana_cost('go for the throat', ['1', 'B']).
card_cmc('go for the throat', 2).
card_layout('go for the throat', 'normal').

% Found in: LRW
card_name('goatnapper', 'Goatnapper').
card_type('goatnapper', 'Creature — Goblin Rogue').
card_types('goatnapper', ['Creature']).
card_subtypes('goatnapper', ['Goblin', 'Rogue']).
card_colors('goatnapper', ['R']).
card_text('goatnapper', 'When Goatnapper enters the battlefield, untap target Goat and gain control of it until end of turn. It gains haste until end of turn.').
card_mana_cost('goatnapper', ['2', 'R']).
card_cmc('goatnapper', 3).
card_layout('goatnapper', 'normal').
card_power('goatnapper', 2).
card_toughness('goatnapper', 2).

% Found in: RTR
card_name('gobbling ooze', 'Gobbling Ooze').
card_type('gobbling ooze', 'Creature — Ooze').
card_types('gobbling ooze', ['Creature']).
card_subtypes('gobbling ooze', ['Ooze']).
card_colors('gobbling ooze', ['G']).
card_text('gobbling ooze', '{G}, Sacrifice another creature: Put a +1/+1 counter on Gobbling Ooze.').
card_mana_cost('gobbling ooze', ['4', 'G']).
card_cmc('gobbling ooze', 5).
card_layout('gobbling ooze', 'normal').
card_power('gobbling ooze', 3).
card_toughness('gobbling ooze', 3).

% Found in: DIS
card_name('gobhobbler rats', 'Gobhobbler Rats').
card_type('gobhobbler rats', 'Creature — Rat').
card_types('gobhobbler rats', ['Creature']).
card_subtypes('gobhobbler rats', ['Rat']).
card_colors('gobhobbler rats', ['B', 'R']).
card_text('gobhobbler rats', 'Hellbent — As long as you have no cards in hand, Gobhobbler Rats gets +1/+0 and has \"{B}: Regenerate Gobhobbler Rats.\"').
card_mana_cost('gobhobbler rats', ['B', 'R']).
card_cmc('gobhobbler rats', 2).
card_layout('gobhobbler rats', 'normal').
card_power('gobhobbler rats', 2).
card_toughness('gobhobbler rats', 2).

% Found in: DDN, EVG
card_name('goblin', 'Goblin').
card_type('goblin', 'Creature — Goblin').
card_types('goblin', ['Creature']).
card_subtypes('goblin', ['Goblin']).
card_colors('goblin', []).
card_text('goblin', '').
card_layout('goblin', 'token').
card_power('goblin', 1).
card_toughness('goblin', 1).

% Found in: DST
card_name('goblin archaeologist', 'Goblin Archaeologist').
card_type('goblin archaeologist', 'Creature — Goblin Artificer').
card_types('goblin archaeologist', ['Creature']).
card_subtypes('goblin archaeologist', ['Goblin', 'Artificer']).
card_colors('goblin archaeologist', ['R']).
card_text('goblin archaeologist', '{R}, {T}: Flip a coin. If you win the flip, destroy target artifact and untap Goblin Archaeologist. If you lose the flip, sacrifice Goblin Archaeologist.').
card_mana_cost('goblin archaeologist', ['1', 'R']).
card_cmc('goblin archaeologist', 2).
card_layout('goblin archaeologist', 'normal').
card_power('goblin archaeologist', 1).
card_toughness('goblin archaeologist', 2).

% Found in: DDK, M12, M13, ROE
card_name('goblin arsonist', 'Goblin Arsonist').
card_type('goblin arsonist', 'Creature — Goblin Shaman').
card_types('goblin arsonist', ['Creature']).
card_subtypes('goblin arsonist', ['Goblin', 'Shaman']).
card_colors('goblin arsonist', ['R']).
card_text('goblin arsonist', 'When Goblin Arsonist dies, you may have it deal 1 damage to target creature or player.').
card_mana_cost('goblin arsonist', ['R']).
card_cmc('goblin arsonist', 1).
card_layout('goblin arsonist', 'normal').
card_power('goblin arsonist', 1).
card_toughness('goblin arsonist', 1).

% Found in: M10
card_name('goblin artillery', 'Goblin Artillery').
card_type('goblin artillery', 'Creature — Goblin Warrior').
card_types('goblin artillery', ['Creature']).
card_subtypes('goblin artillery', ['Goblin', 'Warrior']).
card_colors('goblin artillery', ['R']).
card_text('goblin artillery', '{T}: Goblin Artillery deals 2 damage to target creature or player and 3 damage to you.').
card_mana_cost('goblin artillery', ['1', 'R', 'R']).
card_cmc('goblin artillery', 3).
card_layout('goblin artillery', 'normal').
card_power('goblin artillery', 1).
card_toughness('goblin artillery', 3).

% Found in: ATQ, CHR
card_name('goblin artisans', 'Goblin Artisans').
card_type('goblin artisans', 'Creature — Goblin Artificer').
card_types('goblin artisans', ['Creature']).
card_subtypes('goblin artisans', ['Goblin', 'Artificer']).
card_colors('goblin artisans', ['R']).
card_text('goblin artisans', '{T}: Flip a coin. If you win the flip, draw a card. If you lose the flip, counter target artifact spell you control that isn\'t the target of an ability from another creature named Goblin Artisans.').
card_mana_cost('goblin artisans', ['R']).
card_cmc('goblin artisans', 1).
card_layout('goblin artisans', 'normal').
card_power('goblin artisans', 1).
card_toughness('goblin artisans', 1).

% Found in: LGN
card_name('goblin assassin', 'Goblin Assassin').
card_type('goblin assassin', 'Creature — Goblin Assassin').
card_types('goblin assassin', ['Creature']).
card_subtypes('goblin assassin', ['Goblin', 'Assassin']).
card_colors('goblin assassin', ['R']).
card_text('goblin assassin', 'Whenever Goblin Assassin or another Goblin enters the battlefield, each player flips a coin. Each player whose coin comes up tails sacrifices a creature.').
card_mana_cost('goblin assassin', ['3', 'R', 'R']).
card_cmc('goblin assassin', 5).
card_layout('goblin assassin', 'normal').
card_power('goblin assassin', 2).
card_toughness('goblin assassin', 2).

% Found in: ALA
card_name('goblin assault', 'Goblin Assault').
card_type('goblin assault', 'Enchantment').
card_types('goblin assault', ['Enchantment']).
card_subtypes('goblin assault', []).
card_colors('goblin assault', ['R']).
card_text('goblin assault', 'At the beginning of your upkeep, put a 1/1 red Goblin creature token with haste onto the battlefield.\nGoblin creatures attack each turn if able.').
card_mana_cost('goblin assault', ['2', 'R']).
card_cmc('goblin assault', 3).
card_layout('goblin assault', 'normal').

% Found in: 2ED, 3ED, 4ED, 9ED, ATH, CED, CEI, LEA, LEB, M11
card_name('goblin balloon brigade', 'Goblin Balloon Brigade').
card_type('goblin balloon brigade', 'Creature — Goblin Warrior').
card_types('goblin balloon brigade', ['Creature']).
card_subtypes('goblin balloon brigade', ['Goblin', 'Warrior']).
card_colors('goblin balloon brigade', ['R']).
card_text('goblin balloon brigade', '{R}: Goblin Balloon Brigade gains flying until end of turn.').
card_mana_cost('goblin balloon brigade', ['R']).
card_cmc('goblin balloon brigade', 1).
card_layout('goblin balloon brigade', 'normal').
card_power('goblin balloon brigade', 1).
card_toughness('goblin balloon brigade', 1).

% Found in: M12
card_name('goblin bangchuckers', 'Goblin Bangchuckers').
card_type('goblin bangchuckers', 'Creature — Goblin Warrior').
card_types('goblin bangchuckers', ['Creature']).
card_subtypes('goblin bangchuckers', ['Goblin', 'Warrior']).
card_colors('goblin bangchuckers', ['R']).
card_text('goblin bangchuckers', '{T}: Flip a coin. If you win the flip, Goblin Bangchuckers deals 2 damage to target creature or player. If you lose the flip, Goblin Bangchuckers deals 2 damage to itself.').
card_mana_cost('goblin bangchuckers', ['2', 'R', 'R']).
card_cmc('goblin bangchuckers', 4).
card_layout('goblin bangchuckers', 'normal').
card_power('goblin bangchuckers', 2).
card_toughness('goblin bangchuckers', 2).

% Found in: M13
card_name('goblin battle jester', 'Goblin Battle Jester').
card_type('goblin battle jester', 'Creature — Goblin').
card_types('goblin battle jester', ['Creature']).
card_subtypes('goblin battle jester', ['Goblin']).
card_colors('goblin battle jester', ['R']).
card_text('goblin battle jester', 'Whenever you cast a red spell, target creature can\'t block this turn.').
card_mana_cost('goblin battle jester', ['3', 'R']).
card_cmc('goblin battle jester', 4).
card_layout('goblin battle jester', 'normal').
card_power('goblin battle jester', 2).
card_toughness('goblin battle jester', 2).

% Found in: UDS
card_name('goblin berserker', 'Goblin Berserker').
card_type('goblin berserker', 'Creature — Goblin Berserker').
card_types('goblin berserker', ['Creature']).
card_subtypes('goblin berserker', ['Goblin', 'Berserker']).
card_colors('goblin berserker', ['R']).
card_text('goblin berserker', 'First strike, haste').
card_mana_cost('goblin berserker', ['3', 'R']).
card_cmc('goblin berserker', 4).
card_layout('goblin berserker', 'normal').
card_power('goblin berserker', 2).
card_toughness('goblin berserker', 2).

% Found in: WTH
card_name('goblin bomb', 'Goblin Bomb').
card_type('goblin bomb', 'Enchantment').
card_types('goblin bomb', ['Enchantment']).
card_subtypes('goblin bomb', []).
card_colors('goblin bomb', ['R']).
card_text('goblin bomb', 'At the beginning of your upkeep, you may flip a coin. If you win the flip, put a fuse counter on Goblin Bomb. If you lose the flip, remove a fuse counter from Goblin Bomb.\nRemove five fuse counters from Goblin Bomb, Sacrifice Goblin Bomb: Goblin Bomb deals 20 damage to target player.').
card_mana_cost('goblin bomb', ['1', 'R']).
card_cmc('goblin bomb', 2).
card_layout('goblin bomb', 'normal').
card_reserved('goblin bomb').

% Found in: C13, DDN, TMP, TPR, pFNM
card_name('goblin bombardment', 'Goblin Bombardment').
card_type('goblin bombardment', 'Enchantment').
card_types('goblin bombardment', ['Enchantment']).
card_subtypes('goblin bombardment', []).
card_colors('goblin bombardment', ['R']).
card_text('goblin bombardment', 'Sacrifice a creature: Goblin Bombardment deals 1 damage to target creature or player.').
card_mana_cost('goblin bombardment', ['1', 'R']).
card_cmc('goblin bombardment', 2).
card_layout('goblin bombardment', 'normal').

% Found in: UGL
card_name('goblin bookie', 'Goblin Bookie').
card_type('goblin bookie', 'Creature — Goblin').
card_types('goblin bookie', ['Creature']).
card_subtypes('goblin bookie', ['Goblin']).
card_colors('goblin bookie', ['R']).
card_text('goblin bookie', '{R}, {T}: Reflip any coin or reroll any die.').
card_mana_cost('goblin bookie', ['R']).
card_cmc('goblin bookie', 1).
card_layout('goblin bookie', 'normal').
card_power('goblin bookie', 1).
card_toughness('goblin bookie', 1).

% Found in: FRF
card_name('goblin boom keg', 'Goblin Boom Keg').
card_type('goblin boom keg', 'Artifact').
card_types('goblin boom keg', ['Artifact']).
card_subtypes('goblin boom keg', []).
card_colors('goblin boom keg', []).
card_text('goblin boom keg', 'At the beginning of your upkeep, sacrifice Goblin Boom Keg.\nWhen Goblin Boom Keg is put into a graveyard from the battlefield, it deals 3 damage to target creature or player.').
card_mana_cost('goblin boom keg', ['4']).
card_cmc('goblin boom keg', 4).
card_layout('goblin boom keg', 'normal').

% Found in: UGL
card_name('goblin bowling team', 'Goblin Bowling Team').
card_type('goblin bowling team', 'Creature — Goblins').
card_types('goblin bowling team', ['Creature']).
card_subtypes('goblin bowling team', ['Goblins']).
card_colors('goblin bowling team', ['R']).
card_text('goblin bowling team', 'Whenever Goblin Bowling Team damages any creature or player, roll a six-sided die. Goblin Bowling Team deals to that creature or player additional damage equal to the die roll.').
card_mana_cost('goblin bowling team', ['3', 'R']).
card_cmc('goblin bowling team', 4).
card_layout('goblin bowling team', 'normal').
card_power('goblin bowling team', 1).
card_toughness('goblin bowling team', 1).

% Found in: 5DN
card_name('goblin brawler', 'Goblin Brawler').
card_type('goblin brawler', 'Creature — Goblin Warrior').
card_types('goblin brawler', ['Creature']).
card_subtypes('goblin brawler', ['Goblin', 'Warrior']).
card_colors('goblin brawler', ['R']).
card_text('goblin brawler', 'First strike\nGoblin Brawler can\'t be equipped.').
card_mana_cost('goblin brawler', ['2', 'R']).
card_cmc('goblin brawler', 3).
card_layout('goblin brawler', 'normal').
card_power('goblin brawler', 2).
card_toughness('goblin brawler', 2).

% Found in: 9ED, SCG
card_name('goblin brigand', 'Goblin Brigand').
card_type('goblin brigand', 'Creature — Goblin Warrior').
card_types('goblin brigand', ['Creature']).
card_subtypes('goblin brigand', ['Goblin', 'Warrior']).
card_colors('goblin brigand', ['R']).
card_text('goblin brigand', 'Goblin Brigand attacks each turn if able.').
card_mana_cost('goblin brigand', ['1', 'R']).
card_cmc('goblin brigand', 2).
card_layout('goblin brigand', 'normal').
card_power('goblin brigand', 2).
card_toughness('goblin brigand', 2).

% Found in: ME4, POR
card_name('goblin bully', 'Goblin Bully').
card_type('goblin bully', 'Creature — Goblin').
card_types('goblin bully', ['Creature']).
card_subtypes('goblin bully', ['Goblin']).
card_colors('goblin bully', ['R']).
card_text('goblin bully', '').
card_mana_cost('goblin bully', ['1', 'R']).
card_cmc('goblin bully', 2).
card_layout('goblin bully', 'normal').
card_power('goblin bully', 2).
card_toughness('goblin bully', 1).

% Found in: DD3_EVG, EVG, ONS
card_name('goblin burrows', 'Goblin Burrows').
card_type('goblin burrows', 'Land').
card_types('goblin burrows', ['Land']).
card_subtypes('goblin burrows', []).
card_colors('goblin burrows', []).
card_text('goblin burrows', '{T}: Add {1} to your mana pool.\n{1}{R}, {T}: Target Goblin creature gets +2/+0 until end of turn.').
card_layout('goblin burrows', 'normal').

% Found in: ZEN
card_name('goblin bushwhacker', 'Goblin Bushwhacker').
card_type('goblin bushwhacker', 'Creature — Goblin Warrior').
card_types('goblin bushwhacker', ['Creature']).
card_subtypes('goblin bushwhacker', ['Goblin', 'Warrior']).
card_colors('goblin bushwhacker', ['R']).
card_text('goblin bushwhacker', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nWhen Goblin Bushwhacker enters the battlefield, if it was kicked, creatures you control get +1/+0 and gain haste until end of turn.').
card_mana_cost('goblin bushwhacker', ['R']).
card_cmc('goblin bushwhacker', 1).
card_layout('goblin bushwhacker', 'normal').
card_power('goblin bushwhacker', 1).
card_toughness('goblin bushwhacker', 1).

% Found in: CMD, USG
card_name('goblin cadets', 'Goblin Cadets').
card_type('goblin cadets', 'Creature — Goblin').
card_types('goblin cadets', ['Creature']).
card_subtypes('goblin cadets', ['Goblin']).
card_colors('goblin cadets', ['R']).
card_text('goblin cadets', 'Whenever Goblin Cadets blocks or becomes blocked, target opponent gains control of it. (This removes Goblin Cadets from combat.)').
card_mana_cost('goblin cadets', ['R']).
card_cmc('goblin cadets', 1).
card_layout('goblin cadets', 'normal').
card_power('goblin cadets', 2).
card_toughness('goblin cadets', 1).

% Found in: 5DN
card_name('goblin cannon', 'Goblin Cannon').
card_type('goblin cannon', 'Artifact').
card_types('goblin cannon', ['Artifact']).
card_subtypes('goblin cannon', []).
card_colors('goblin cannon', []).
card_text('goblin cannon', '{2}: Goblin Cannon deals 1 damage to target creature or player. Sacrifice Goblin Cannon.').
card_mana_cost('goblin cannon', ['4']).
card_cmc('goblin cannon', 4).
card_layout('goblin cannon', 'normal').

% Found in: ME4, PO2, S99
card_name('goblin cavaliers', 'Goblin Cavaliers').
card_type('goblin cavaliers', 'Creature — Goblin').
card_types('goblin cavaliers', ['Creature']).
card_subtypes('goblin cavaliers', ['Goblin']).
card_colors('goblin cavaliers', ['R']).
card_text('goblin cavaliers', '').
card_mana_cost('goblin cavaliers', ['2', 'R']).
card_cmc('goblin cavaliers', 3).
card_layout('goblin cavaliers', 'normal').
card_power('goblin cavaliers', 3).
card_toughness('goblin cavaliers', 2).

% Found in: DRK, ME4
card_name('goblin caves', 'Goblin Caves').
card_type('goblin caves', 'Enchantment — Aura').
card_types('goblin caves', ['Enchantment']).
card_subtypes('goblin caves', ['Aura']).
card_colors('goblin caves', ['R']).
card_text('goblin caves', 'Enchant land\nAs long as enchanted land is a basic Mountain, Goblin creatures get +0/+2.').
card_mana_cost('goblin caves', ['1', 'R', 'R']).
card_cmc('goblin caves', 3).
card_layout('goblin caves', 'normal').

% Found in: MRD
card_name('goblin charbelcher', 'Goblin Charbelcher').
card_type('goblin charbelcher', 'Artifact').
card_types('goblin charbelcher', ['Artifact']).
card_subtypes('goblin charbelcher', []).
card_colors('goblin charbelcher', []).
card_text('goblin charbelcher', '{3}, {T}: Reveal cards from the top of your library until you reveal a land card. Goblin Charbelcher deals damage equal to the number of nonland cards revealed this way to target creature or player. If the revealed land card was a Mountain, Goblin Charbelcher deals double that damage instead. Put the revealed cards on the bottom of your library in any order.').
card_mana_cost('goblin charbelcher', ['4']).
card_cmc('goblin charbelcher', 4).
card_layout('goblin charbelcher', 'normal').

% Found in: 7ED, 8ED, 9ED, S99
card_name('goblin chariot', 'Goblin Chariot').
card_type('goblin chariot', 'Creature — Goblin Warrior').
card_types('goblin chariot', ['Creature']).
card_subtypes('goblin chariot', ['Goblin', 'Warrior']).
card_colors('goblin chariot', ['R']).
card_text('goblin chariot', 'Haste (This creature can attack the turn it comes under your control.)').
card_mana_cost('goblin chariot', ['2', 'R']).
card_cmc('goblin chariot', 3).
card_layout('goblin chariot', 'normal').
card_power('goblin chariot', 2).
card_toughness('goblin chariot', 2).

% Found in: M10, M11, M12
card_name('goblin chieftain', 'Goblin Chieftain').
card_type('goblin chieftain', 'Creature — Goblin').
card_types('goblin chieftain', ['Creature']).
card_subtypes('goblin chieftain', ['Goblin']).
card_colors('goblin chieftain', ['R']).
card_text('goblin chieftain', 'Haste (This creature can attack and {T} as soon as it comes under your control.)\nOther Goblin creatures you control get +1/+1 and have haste.').
card_mana_cost('goblin chieftain', ['1', 'R', 'R']).
card_cmc('goblin chieftain', 3).
card_layout('goblin chieftain', 'normal').
card_power('goblin chieftain', 2).
card_toughness('goblin chieftain', 2).

% Found in: FEM, MED
card_name('goblin chirurgeon', 'Goblin Chirurgeon').
card_type('goblin chirurgeon', 'Creature — Goblin Shaman').
card_types('goblin chirurgeon', ['Creature']).
card_subtypes('goblin chirurgeon', ['Goblin', 'Shaman']).
card_colors('goblin chirurgeon', ['R']).
card_text('goblin chirurgeon', 'Sacrifice a Goblin: Regenerate target creature.').
card_mana_cost('goblin chirurgeon', ['R']).
card_cmc('goblin chirurgeon', 1).
card_layout('goblin chirurgeon', 'normal').
card_power('goblin chirurgeon', 0).
card_toughness('goblin chirurgeon', 2).

% Found in: LGN
card_name('goblin clearcutter', 'Goblin Clearcutter').
card_type('goblin clearcutter', 'Creature — Goblin').
card_types('goblin clearcutter', ['Creature']).
card_subtypes('goblin clearcutter', ['Goblin']).
card_colors('goblin clearcutter', ['R']).
card_text('goblin clearcutter', '{T}, Sacrifice a Forest: Add three mana in any combination of {R} and/or {G} to your mana pool.').
card_mana_cost('goblin clearcutter', ['3', 'R']).
card_cmc('goblin clearcutter', 4).
card_layout('goblin clearcutter', 'normal').
card_power('goblin clearcutter', 3).
card_toughness('goblin clearcutter', 3).

% Found in: BOK, DD3_EVG, EVG
card_name('goblin cohort', 'Goblin Cohort').
card_type('goblin cohort', 'Creature — Goblin Warrior').
card_types('goblin cohort', ['Creature']).
card_subtypes('goblin cohort', ['Goblin', 'Warrior']).
card_colors('goblin cohort', ['R']).
card_text('goblin cohort', 'Goblin Cohort can\'t attack unless you\'ve cast a creature spell this turn.').
card_mana_cost('goblin cohort', ['R']).
card_cmc('goblin cohort', 1).
card_layout('goblin cohort', 'normal').
card_power('goblin cohort', 2).
card_toughness('goblin cohort', 2).

% Found in: S99, VMA
card_name('goblin commando', 'Goblin Commando').
card_type('goblin commando', 'Creature — Goblin').
card_types('goblin commando', ['Creature']).
card_subtypes('goblin commando', ['Goblin']).
card_colors('goblin commando', ['R']).
card_text('goblin commando', 'When Goblin Commando enters the battlefield, it deals 2 damage to target creature.').
card_mana_cost('goblin commando', ['4', 'R']).
card_cmc('goblin commando', 5).
card_layout('goblin commando', 'normal').
card_power('goblin commando', 2).
card_toughness('goblin commando', 2).

% Found in: ALA, DDN
card_name('goblin deathraiders', 'Goblin Deathraiders').
card_type('goblin deathraiders', 'Creature — Goblin Warrior').
card_types('goblin deathraiders', ['Creature']).
card_subtypes('goblin deathraiders', ['Goblin', 'Warrior']).
card_colors('goblin deathraiders', ['B', 'R']).
card_text('goblin deathraiders', 'Trample').
card_mana_cost('goblin deathraiders', ['B', 'R']).
card_cmc('goblin deathraiders', 2).
card_layout('goblin deathraiders', 'normal').
card_power('goblin deathraiders', 3).
card_toughness('goblin deathraiders', 1).

% Found in: 5ED, 6ED, 7ED, ATH, CHR, DRK
card_name('goblin digging team', 'Goblin Digging Team').
card_type('goblin digging team', 'Creature — Goblin').
card_types('goblin digging team', ['Creature']).
card_subtypes('goblin digging team', ['Goblin']).
card_colors('goblin digging team', ['R']).
card_text('goblin digging team', '{T}, Sacrifice Goblin Digging Team: Destroy target Wall.').
card_mana_cost('goblin digging team', ['R']).
card_cmc('goblin digging team', 1).
card_layout('goblin digging team', 'normal').
card_power('goblin digging team', 1).
card_toughness('goblin digging team', 1).

% Found in: M14, pMGD
card_name('goblin diplomats', 'Goblin Diplomats').
card_type('goblin diplomats', 'Creature — Goblin').
card_types('goblin diplomats', ['Creature']).
card_subtypes('goblin diplomats', ['Goblin']).
card_colors('goblin diplomats', ['R']).
card_text('goblin diplomats', '{T}: Each creature attacks this turn if able.').
card_mana_cost('goblin diplomats', ['1', 'R']).
card_cmc('goblin diplomats', 2).
card_layout('goblin diplomats', 'normal').
card_power('goblin diplomats', 2).
card_toughness('goblin diplomats', 1).

% Found in: MRD
card_name('goblin dirigible', 'Goblin Dirigible').
card_type('goblin dirigible', 'Artifact Creature — Construct').
card_types('goblin dirigible', ['Artifact', 'Creature']).
card_subtypes('goblin dirigible', ['Construct']).
card_colors('goblin dirigible', []).
card_text('goblin dirigible', 'Flying\nGoblin Dirigible doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {4}. If you do, untap Goblin Dirigible.').
card_mana_cost('goblin dirigible', ['6']).
card_cmc('goblin dirigible', 6).
card_layout('goblin dirigible', 'normal').
card_power('goblin dirigible', 4).
card_toughness('goblin dirigible', 4).

% Found in: LGN
card_name('goblin dynamo', 'Goblin Dynamo').
card_type('goblin dynamo', 'Creature — Goblin Mutant').
card_types('goblin dynamo', ['Creature']).
card_subtypes('goblin dynamo', ['Goblin', 'Mutant']).
card_colors('goblin dynamo', ['R']).
card_text('goblin dynamo', '{T}: Goblin Dynamo deals 1 damage to target creature or player.\n{X}{R}, {T}, Sacrifice Goblin Dynamo: Goblin Dynamo deals X damage to target creature or player.').
card_mana_cost('goblin dynamo', ['5', 'R', 'R']).
card_cmc('goblin dynamo', 7).
card_layout('goblin dynamo', 'normal').
card_power('goblin dynamo', 4).
card_toughness('goblin dynamo', 4).

% Found in: DDJ, RTR
card_name('goblin electromancer', 'Goblin Electromancer').
card_type('goblin electromancer', 'Creature — Goblin Wizard').
card_types('goblin electromancer', ['Creature']).
card_subtypes('goblin electromancer', ['Goblin', 'Wizard']).
card_colors('goblin electromancer', ['U', 'R']).
card_text('goblin electromancer', 'Instant and sorcery spells you cast cost {1} less to cast.').
card_mana_cost('goblin electromancer', ['U', 'R']).
card_cmc('goblin electromancer', 2).
card_layout('goblin electromancer', 'normal').
card_power('goblin electromancer', 2).
card_toughness('goblin electromancer', 2).

% Found in: 10E, 6ED, 7ED, MIR
card_name('goblin elite infantry', 'Goblin Elite Infantry').
card_type('goblin elite infantry', 'Creature — Goblin Warrior').
card_types('goblin elite infantry', ['Creature']).
card_subtypes('goblin elite infantry', ['Goblin', 'Warrior']).
card_colors('goblin elite infantry', ['R']).
card_text('goblin elite infantry', 'Whenever Goblin Elite Infantry blocks or becomes blocked, it gets -1/-1 until end of turn.').
card_mana_cost('goblin elite infantry', ['1', 'R']).
card_cmc('goblin elite infantry', 2).
card_layout('goblin elite infantry', 'normal').
card_power('goblin elite infantry', 2).
card_toughness('goblin elite infantry', 2).

% Found in: UDS
card_name('goblin festival', 'Goblin Festival').
card_type('goblin festival', 'Enchantment').
card_types('goblin festival', ['Enchantment']).
card_subtypes('goblin festival', []).
card_colors('goblin festival', ['R']).
card_text('goblin festival', '{2}: Goblin Festival deals 1 damage to target creature or player. Flip a coin. If you lose the flip, choose one of your opponents. That player gains control of Goblin Festival.').
card_mana_cost('goblin festival', ['1', 'R']).
card_cmc('goblin festival', 2).
card_layout('goblin festival', 'normal').

% Found in: RAV
card_name('goblin fire fiend', 'Goblin Fire Fiend').
card_type('goblin fire fiend', 'Creature — Goblin Berserker').
card_types('goblin fire fiend', ['Creature']).
card_subtypes('goblin fire fiend', ['Goblin', 'Berserker']).
card_colors('goblin fire fiend', ['R']).
card_text('goblin fire fiend', 'Haste\nGoblin Fire Fiend must be blocked if able.\n{R}: Goblin Fire Fiend gets +1/+0 until end of turn.').
card_mana_cost('goblin fire fiend', ['3', 'R']).
card_cmc('goblin fire fiend', 4).
card_layout('goblin fire fiend', 'normal').
card_power('goblin fire fiend', 1).
card_toughness('goblin fire fiend', 1).

% Found in: LGN
card_name('goblin firebug', 'Goblin Firebug').
card_type('goblin firebug', 'Creature — Goblin').
card_types('goblin firebug', ['Creature']).
card_subtypes('goblin firebug', ['Goblin']).
card_colors('goblin firebug', ['R']).
card_text('goblin firebug', 'When Goblin Firebug leaves the battlefield, sacrifice a land.').
card_mana_cost('goblin firebug', ['1', 'R']).
card_cmc('goblin firebug', 2).
card_layout('goblin firebug', 'normal').
card_power('goblin firebug', 2).
card_toughness('goblin firebug', 2).

% Found in: M12, MM2
card_name('goblin fireslinger', 'Goblin Fireslinger').
card_type('goblin fireslinger', 'Creature — Goblin Warrior').
card_types('goblin fireslinger', ['Creature']).
card_subtypes('goblin fireslinger', ['Goblin', 'Warrior']).
card_colors('goblin fireslinger', ['R']).
card_text('goblin fireslinger', '{T}: Goblin Fireslinger deals 1 damage to target player.').
card_mana_cost('goblin fireslinger', ['R']).
card_cmc('goblin fireslinger', 1).
card_layout('goblin fireslinger', 'normal').
card_power('goblin fireslinger', 1).
card_toughness('goblin fireslinger', 1).

% Found in: ME4, PO2
card_name('goblin firestarter', 'Goblin Firestarter').
card_type('goblin firestarter', 'Creature — Goblin').
card_types('goblin firestarter', ['Creature']).
card_subtypes('goblin firestarter', ['Goblin']).
card_colors('goblin firestarter', ['R']).
card_text('goblin firestarter', 'Sacrifice Goblin Firestarter: Goblin Firestarter deals 1 damage to target creature or player. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('goblin firestarter', ['R']).
card_cmc('goblin firestarter', 1).
card_layout('goblin firestarter', 'normal').
card_power('goblin firestarter', 1).
card_toughness('goblin firestarter', 1).

% Found in: GPT
card_name('goblin flectomancer', 'Goblin Flectomancer').
card_type('goblin flectomancer', 'Creature — Goblin Wizard').
card_types('goblin flectomancer', ['Creature']).
card_subtypes('goblin flectomancer', ['Goblin', 'Wizard']).
card_colors('goblin flectomancer', ['U', 'R']).
card_text('goblin flectomancer', 'Sacrifice Goblin Flectomancer: You may change the targets of target instant or sorcery spell.').
card_mana_cost('goblin flectomancer', ['U', 'R', 'R']).
card_cmc('goblin flectomancer', 3).
card_layout('goblin flectomancer', 'normal').
card_power('goblin flectomancer', 2).
card_toughness('goblin flectomancer', 2).

% Found in: FEM
card_name('goblin flotilla', 'Goblin Flotilla').
card_type('goblin flotilla', 'Creature — Goblin').
card_types('goblin flotilla', ['Creature']).
card_subtypes('goblin flotilla', ['Goblin']).
card_colors('goblin flotilla', ['R']).
card_text('goblin flotilla', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nAt the beginning of each combat, unless you pay {R}, whenever Goblin Flotilla blocks or becomes blocked by a creature this combat, that creature gains first strike until end of turn.').
card_mana_cost('goblin flotilla', ['2', 'R']).
card_cmc('goblin flotilla', 3).
card_layout('goblin flotilla', 'normal').
card_power('goblin flotilla', 2).
card_toughness('goblin flotilla', 2).
card_reserved('goblin flotilla').

% Found in: CSP
card_name('goblin furrier', 'Goblin Furrier').
card_type('goblin furrier', 'Creature — Goblin Warrior').
card_types('goblin furrier', ['Creature']).
card_subtypes('goblin furrier', ['Goblin', 'Warrior']).
card_colors('goblin furrier', ['R']).
card_text('goblin furrier', 'Prevent all damage that Goblin Furrier would deal to snow creatures.').
card_mana_cost('goblin furrier', ['1', 'R']).
card_cmc('goblin furrier', 2).
card_layout('goblin furrier', 'normal').
card_power('goblin furrier', 2).
card_toughness('goblin furrier', 2).

% Found in: PLS
card_name('goblin game', 'Goblin Game').
card_type('goblin game', 'Sorcery').
card_types('goblin game', ['Sorcery']).
card_subtypes('goblin game', []).
card_colors('goblin game', ['R']).
card_text('goblin game', 'Each player hides at least one item, then all players reveal them simultaneously. Each player loses life equal to the number of items he or she revealed. The player who revealed the fewest items then loses half his or her life, rounded up. If two or more players are tied for fewest, each loses half his or her life, rounded up.').
card_mana_cost('goblin game', ['5', 'R', 'R']).
card_cmc('goblin game', 7).
card_layout('goblin game', 'normal').

% Found in: 7ED, UDS
card_name('goblin gardener', 'Goblin Gardener').
card_type('goblin gardener', 'Creature — Goblin').
card_types('goblin gardener', ['Creature']).
card_subtypes('goblin gardener', ['Goblin']).
card_colors('goblin gardener', ['R']).
card_text('goblin gardener', 'When Goblin Gardener dies, destroy target land.').
card_mana_cost('goblin gardener', ['3', 'R']).
card_cmc('goblin gardener', 4).
card_layout('goblin gardener', 'normal').
card_power('goblin gardener', 2).
card_toughness('goblin gardener', 1).

% Found in: SOM
card_name('goblin gaveleer', 'Goblin Gaveleer').
card_type('goblin gaveleer', 'Creature — Goblin Warrior').
card_types('goblin gaveleer', ['Creature']).
card_subtypes('goblin gaveleer', ['Goblin', 'Warrior']).
card_colors('goblin gaveleer', ['R']).
card_text('goblin gaveleer', 'Trample\nGoblin Gaveleer gets +2/+0 for each Equipment attached to it.').
card_mana_cost('goblin gaveleer', ['R']).
card_cmc('goblin gaveleer', 1).
card_layout('goblin gaveleer', 'normal').
card_power('goblin gaveleer', 1).
card_toughness('goblin gaveleer', 1).

% Found in: ME4, PO2, S99, VMA
card_name('goblin general', 'Goblin General').
card_type('goblin general', 'Creature — Goblin Warrior').
card_types('goblin general', ['Creature']).
card_subtypes('goblin general', ['Goblin', 'Warrior']).
card_colors('goblin general', ['R']).
card_text('goblin general', 'Whenever Goblin General attacks, Goblin creatures you control get +1/+1 until end of turn.').
card_mana_cost('goblin general', ['1', 'R', 'R']).
card_cmc('goblin general', 3).
card_layout('goblin general', 'normal').
card_power('goblin general', 1).
card_toughness('goblin general', 1).

% Found in: 7ED, 8ED, PO2, S99
card_name('goblin glider', 'Goblin Glider').
card_type('goblin glider', 'Creature — Goblin').
card_types('goblin glider', ['Creature']).
card_subtypes('goblin glider', ['Goblin']).
card_colors('goblin glider', ['R']).
card_text('goblin glider', 'Flying\nGoblin Glider can\'t block.').
card_mana_cost('goblin glider', ['1', 'R']).
card_cmc('goblin glider', 2).
card_layout('goblin glider', 'normal').
card_power('goblin glider', 1).
card_toughness('goblin glider', 1).

% Found in: ORI
card_name('goblin glory chaser', 'Goblin Glory Chaser').
card_type('goblin glory chaser', 'Creature — Goblin Warrior').
card_types('goblin glory chaser', ['Creature']).
card_subtypes('goblin glory chaser', ['Goblin', 'Warrior']).
card_colors('goblin glory chaser', ['R']).
card_text('goblin glory chaser', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)\nAs long as Goblin Glory Chaser is renowned, it has menace. (It can\'t be blocked except by two or more creatures.)').
card_mana_cost('goblin glory chaser', ['R']).
card_cmc('goblin glory chaser', 1).
card_layout('goblin glory chaser', 'normal').
card_power('goblin glory chaser', 1).
card_toughness('goblin glory chaser', 1).

% Found in: LGN, VMA
card_name('goblin goon', 'Goblin Goon').
card_type('goblin goon', 'Creature — Goblin Mutant').
card_types('goblin goon', ['Creature']).
card_subtypes('goblin goon', ['Goblin', 'Mutant']).
card_colors('goblin goon', ['R']).
card_text('goblin goon', 'Goblin Goon can\'t attack unless you control more creatures than defending player.\nGoblin Goon can\'t block unless you control more creatures than attacking player.').
card_mana_cost('goblin goon', ['3', 'R']).
card_cmc('goblin goon', 4).
card_layout('goblin goon', 'normal').
card_power('goblin goon', 6).
card_toughness('goblin goon', 6).

% Found in: LGN
card_name('goblin grappler', 'Goblin Grappler').
card_type('goblin grappler', 'Creature — Goblin').
card_types('goblin grappler', ['Creature']).
card_subtypes('goblin grappler', ['Goblin']).
card_colors('goblin grappler', ['R']).
card_text('goblin grappler', 'Provoke (Whenever this creature attacks, you may have target creature defending player controls untap and block it if able.)').
card_mana_cost('goblin grappler', ['R']).
card_cmc('goblin grappler', 1).
card_layout('goblin grappler', 'normal').
card_power('goblin grappler', 1).
card_toughness('goblin grappler', 1).

% Found in: ATH, FEM, M12, MED
card_name('goblin grenade', 'Goblin Grenade').
card_type('goblin grenade', 'Sorcery').
card_types('goblin grenade', ['Sorcery']).
card_subtypes('goblin grenade', []).
card_colors('goblin grenade', ['R']).
card_text('goblin grenade', 'As an additional cost to cast Goblin Grenade, sacrifice a Goblin.\nGoblin Grenade deals 5 damage to target creature or player.').
card_mana_cost('goblin grenade', ['R']).
card_cmc('goblin grenade', 1).
card_layout('goblin grenade', 'normal').

% Found in: WTH
card_name('goblin grenadiers', 'Goblin Grenadiers').
card_type('goblin grenadiers', 'Creature — Goblin').
card_types('goblin grenadiers', ['Creature']).
card_subtypes('goblin grenadiers', ['Goblin']).
card_colors('goblin grenadiers', ['R']).
card_text('goblin grenadiers', 'Whenever Goblin Grenadiers attacks and isn\'t blocked, you may sacrifice it. If you do, destroy target creature and target land.').
card_mana_cost('goblin grenadiers', ['3', 'R']).
card_cmc('goblin grenadiers', 4).
card_layout('goblin grenadiers', 'normal').
card_power('goblin grenadiers', 2).
card_toughness('goblin grenadiers', 2).

% Found in: ZEN, pGPX
card_name('goblin guide', 'Goblin Guide').
card_type('goblin guide', 'Creature — Goblin Scout').
card_types('goblin guide', ['Creature']).
card_subtypes('goblin guide', ['Goblin', 'Scout']).
card_colors('goblin guide', ['R']).
card_text('goblin guide', 'Haste\nWhenever Goblin Guide attacks, defending player reveals the top card of his or her library. If it\'s a land card, that player puts it into his or her hand.').
card_mana_cost('goblin guide', ['R']).
card_cmc('goblin guide', 1).
card_layout('goblin guide', 'normal').
card_power('goblin guide', 2).
card_toughness('goblin guide', 2).

% Found in: FRF
card_name('goblin heelcutter', 'Goblin Heelcutter').
card_type('goblin heelcutter', 'Creature — Goblin Berserker').
card_types('goblin heelcutter', ['Creature']).
card_subtypes('goblin heelcutter', ['Goblin', 'Berserker']).
card_colors('goblin heelcutter', ['R']).
card_text('goblin heelcutter', 'Whenever Goblin Heelcutter attacks, target creature can\'t block this turn.\nDash {2}{R} (You may cast this spell for its dash cost. If you do, it gains haste, and it\'s returned from the battlefield to its owner\'s hand at the beginning of the next end step.)').
card_mana_cost('goblin heelcutter', ['3', 'R']).
card_cmc('goblin heelcutter', 4).
card_layout('goblin heelcutter', 'normal').
card_power('goblin heelcutter', 3).
card_toughness('goblin heelcutter', 2).

% Found in: 5ED, 6ED, ATH, DRK, S00, S99
card_name('goblin hero', 'Goblin Hero').
card_type('goblin hero', 'Creature — Goblin').
card_types('goblin hero', ['Creature']).
card_subtypes('goblin hero', ['Goblin']).
card_colors('goblin hero', ['R']).
card_text('goblin hero', '').
card_mana_cost('goblin hero', ['2', 'R']).
card_cmc('goblin hero', 3).
card_layout('goblin hero', 'normal').
card_power('goblin hero', 2).
card_toughness('goblin hero', 2).

% Found in: M15
card_name('goblin kaboomist', 'Goblin Kaboomist').
card_type('goblin kaboomist', 'Creature — Goblin Warrior').
card_types('goblin kaboomist', ['Creature']).
card_subtypes('goblin kaboomist', ['Goblin', 'Warrior']).
card_colors('goblin kaboomist', ['R']).
card_text('goblin kaboomist', 'At the beginning of your upkeep, put a colorless artifact token named Land Mine onto the battlefield with \"{R}, Sacrifice this artifact: This artifact deals 2 damage to target attacking creature without flying.\" Then flip a coin. If you lose the flip, Goblin Kaboomist deals 2 damage to itself.').
card_mana_cost('goblin kaboomist', ['1', 'R']).
card_cmc('goblin kaboomist', 2).
card_layout('goblin kaboomist', 'normal').
card_power('goblin kaboomist', 1).
card_toughness('goblin kaboomist', 2).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, ATH, CED, CEI, LEA, LEB
card_name('goblin king', 'Goblin King').
card_type('goblin king', 'Creature — Goblin').
card_types('goblin king', ['Creature']).
card_subtypes('goblin king', ['Goblin']).
card_colors('goblin king', ['R']).
card_text('goblin king', 'Other Goblin creatures get +1/+1 and have mountainwalk. (They can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('goblin king', ['1', 'R', 'R']).
card_cmc('goblin king', 3).
card_layout('goblin king', 'normal').
card_power('goblin king', 2).
card_toughness('goblin king', 2).

% Found in: FEM
card_name('goblin kites', 'Goblin Kites').
card_type('goblin kites', 'Enchantment').
card_types('goblin kites', ['Enchantment']).
card_subtypes('goblin kites', []).
card_colors('goblin kites', ['R']).
card_text('goblin kites', '{R}: Target creature you control with toughness 2 or less gains flying until end of turn. Flip a coin at the beginning of the next end step. If you lose the flip, sacrifice that creature.').
card_mana_cost('goblin kites', ['1', 'R']).
card_cmc('goblin kites', 2).
card_layout('goblin kites', 'normal').

% Found in: USG, V09, VMA
card_name('goblin lackey', 'Goblin Lackey').
card_type('goblin lackey', 'Creature — Goblin').
card_types('goblin lackey', ['Creature']).
card_subtypes('goblin lackey', ['Goblin']).
card_colors('goblin lackey', ['R']).
card_text('goblin lackey', 'Whenever Goblin Lackey deals damage to a player, you may put a Goblin permanent card from your hand onto the battlefield.').
card_mana_cost('goblin lackey', ['R']).
card_cmc('goblin lackey', 1).
card_layout('goblin lackey', 'normal').
card_power('goblin lackey', 1).
card_toughness('goblin lackey', 1).

% Found in: APC, pFNM
card_name('goblin legionnaire', 'Goblin Legionnaire').
card_type('goblin legionnaire', 'Creature — Goblin Soldier').
card_types('goblin legionnaire', ['Creature']).
card_subtypes('goblin legionnaire', ['Goblin', 'Soldier']).
card_colors('goblin legionnaire', ['W', 'R']).
card_text('goblin legionnaire', '{R}, Sacrifice Goblin Legionnaire: Goblin Legionnaire deals 2 damage to target creature or player.\n{W}, Sacrifice Goblin Legionnaire: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('goblin legionnaire', ['R', 'W']).
card_cmc('goblin legionnaire', 2).
card_layout('goblin legionnaire', 'normal').
card_power('goblin legionnaire', 2).
card_toughness('goblin legionnaire', 2).

% Found in: LGN
card_name('goblin lookout', 'Goblin Lookout').
card_type('goblin lookout', 'Creature — Goblin').
card_types('goblin lookout', ['Creature']).
card_subtypes('goblin lookout', ['Goblin']).
card_colors('goblin lookout', ['R']).
card_text('goblin lookout', '{T}, Sacrifice a Goblin: Goblin creatures get +2/+0 until end of turn.').
card_mana_cost('goblin lookout', ['1', 'R']).
card_cmc('goblin lookout', 2).
card_layout('goblin lookout', 'normal').
card_power('goblin lookout', 1).
card_toughness('goblin lookout', 2).

% Found in: 10E, PO2, S99
card_name('goblin lore', 'Goblin Lore').
card_type('goblin lore', 'Sorcery').
card_types('goblin lore', ['Sorcery']).
card_subtypes('goblin lore', []).
card_colors('goblin lore', ['R']).
card_text('goblin lore', 'Draw four cards, then discard three cards at random.').
card_mana_cost('goblin lore', ['1', 'R']).
card_cmc('goblin lore', 2).
card_layout('goblin lore', 'normal').

% Found in: ICE
card_name('goblin lyre', 'Goblin Lyre').
card_type('goblin lyre', 'Artifact').
card_types('goblin lyre', ['Artifact']).
card_subtypes('goblin lyre', []).
card_colors('goblin lyre', []).
card_text('goblin lyre', 'Sacrifice Goblin Lyre: Flip a coin. If you win the flip, Goblin Lyre deals damage to target opponent equal to the number of creatures you control. If you lose the flip, Goblin Lyre deals damage to you equal to the number of creatures that opponent controls.').
card_mana_cost('goblin lyre', ['3']).
card_cmc('goblin lyre', 3).
card_layout('goblin lyre', 'normal').

% Found in: ONS
card_name('goblin machinist', 'Goblin Machinist').
card_type('goblin machinist', 'Creature — Goblin').
card_types('goblin machinist', ['Creature']).
card_subtypes('goblin machinist', ['Goblin']).
card_colors('goblin machinist', ['R']).
card_text('goblin machinist', '{2}{R}: Reveal cards from the top of your library until you reveal a nonland card. Goblin Machinist gets +X/+0 until end of turn, where X is that card\'s converted mana cost. Put the revealed cards on the bottom of your library in any order.').
card_mana_cost('goblin machinist', ['4', 'R']).
card_cmc('goblin machinist', 5).
card_layout('goblin machinist', 'normal').
card_power('goblin machinist', 0).
card_toughness('goblin machinist', 5).

% Found in: UDS
card_name('goblin marshal', 'Goblin Marshal').
card_type('goblin marshal', 'Creature — Goblin Warrior').
card_types('goblin marshal', ['Creature']).
card_subtypes('goblin marshal', ['Goblin', 'Warrior']).
card_colors('goblin marshal', ['R']).
card_text('goblin marshal', 'Echo {4}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Goblin Marshal enters the battlefield or dies, put two 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('goblin marshal', ['4', 'R', 'R']).
card_cmc('goblin marshal', 6).
card_layout('goblin marshal', 'normal').
card_power('goblin marshal', 3).
card_toughness('goblin marshal', 3).

% Found in: UDS
card_name('goblin masons', 'Goblin Masons').
card_type('goblin masons', 'Creature — Goblin').
card_types('goblin masons', ['Creature']).
card_subtypes('goblin masons', ['Goblin']).
card_colors('goblin masons', ['R']).
card_text('goblin masons', 'When Goblin Masons dies, destroy target Wall.').
card_mana_cost('goblin masons', ['1', 'R']).
card_cmc('goblin masons', 2).
card_layout('goblin masons', 'normal').
card_power('goblin masons', 2).
card_toughness('goblin masons', 1).

% Found in: 7ED, ATH, DD3_EVG, EVG, PO2, USG, VMA
card_name('goblin matron', 'Goblin Matron').
card_type('goblin matron', 'Creature — Goblin').
card_types('goblin matron', ['Creature']).
card_subtypes('goblin matron', ['Goblin']).
card_colors('goblin matron', ['R']).
card_text('goblin matron', 'When Goblin Matron enters the battlefield, you may search your library for a Goblin card, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('goblin matron', ['2', 'R']).
card_cmc('goblin matron', 3).
card_layout('goblin matron', 'normal').
card_power('goblin matron', 1).
card_toughness('goblin matron', 1).

% Found in: ULG
card_name('goblin medics', 'Goblin Medics').
card_type('goblin medics', 'Creature — Goblin Shaman').
card_types('goblin medics', ['Creature']).
card_subtypes('goblin medics', ['Goblin', 'Shaman']).
card_colors('goblin medics', ['R']).
card_text('goblin medics', 'Whenever Goblin Medics becomes tapped, it deals 1 damage to target creature or player.').
card_mana_cost('goblin medics', ['2', 'R']).
card_cmc('goblin medics', 3).
card_layout('goblin medics', 'normal').
card_power('goblin medics', 1).
card_toughness('goblin medics', 1).

% Found in: UNH, pARL
card_name('goblin mime', 'Goblin Mime').
card_type('goblin mime', 'Creature — Goblin Mime').
card_types('goblin mime', ['Creature']).
card_subtypes('goblin mime', ['Goblin', 'Mime']).
card_colors('goblin mime', ['R']).
card_text('goblin mime', 'When you speak, sacrifice Goblin Mime.').
card_mana_cost('goblin mime', ['1', 'R']).
card_cmc('goblin mime', 2).
card_layout('goblin mime', 'normal').
card_power('goblin mime', 2).
card_toughness('goblin mime', 2).

% Found in: 9ED, ALA, PO2, S99
card_name('goblin mountaineer', 'Goblin Mountaineer').
card_type('goblin mountaineer', 'Creature — Goblin Scout').
card_types('goblin mountaineer', ['Creature']).
card_subtypes('goblin mountaineer', ['Goblin', 'Scout']).
card_colors('goblin mountaineer', ['R']).
card_text('goblin mountaineer', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('goblin mountaineer', ['R']).
card_cmc('goblin mountaineer', 1).
card_layout('goblin mountaineer', 'normal').
card_power('goblin mountaineer', 1).
card_toughness('goblin mountaineer', 1).

% Found in: ATH, DKM, ICE, MED
card_name('goblin mutant', 'Goblin Mutant').
card_type('goblin mutant', 'Creature — Goblin Mutant').
card_types('goblin mutant', ['Creature']).
card_subtypes('goblin mutant', ['Goblin', 'Mutant']).
card_colors('goblin mutant', ['R']).
card_text('goblin mutant', 'Trample\nGoblin Mutant can\'t attack if defending player controls an untapped creature with power 3 or greater.\nGoblin Mutant can\'t block creatures with power 3 or greater.').
card_mana_cost('goblin mutant', ['2', 'R', 'R']).
card_cmc('goblin mutant', 4).
card_layout('goblin mutant', 'normal').
card_power('goblin mutant', 5).
card_toughness('goblin mutant', 3).

% Found in: ATH, HOP, USG
card_name('goblin offensive', 'Goblin Offensive').
card_type('goblin offensive', 'Sorcery').
card_types('goblin offensive', ['Sorcery']).
card_subtypes('goblin offensive', []).
card_colors('goblin offensive', ['R']).
card_text('goblin offensive', 'Put X 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('goblin offensive', ['X', '1', 'R', 'R']).
card_cmc('goblin offensive', 3).
card_layout('goblin offensive', 'normal').

% Found in: CON
card_name('goblin outlander', 'Goblin Outlander').
card_type('goblin outlander', 'Creature — Goblin Scout').
card_types('goblin outlander', ['Creature']).
card_subtypes('goblin outlander', ['Goblin', 'Scout']).
card_colors('goblin outlander', ['B', 'R']).
card_text('goblin outlander', 'Protection from white').
card_mana_cost('goblin outlander', ['B', 'R']).
card_cmc('goblin outlander', 2).
card_layout('goblin outlander', 'normal').
card_power('goblin outlander', 2).
card_toughness('goblin outlander', 2).

% Found in: USG, VMA
card_name('goblin patrol', 'Goblin Patrol').
card_type('goblin patrol', 'Creature — Goblin').
card_types('goblin patrol', ['Creature']).
card_subtypes('goblin patrol', ['Goblin']).
card_colors('goblin patrol', ['R']).
card_text('goblin patrol', 'Echo {R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('goblin patrol', ['R']).
card_cmc('goblin patrol', 1).
card_layout('goblin patrol', 'normal').
card_power('goblin patrol', 2).
card_toughness('goblin patrol', 1).

% Found in: 10E, 9ED, DPA, M10, M11, M12, PO2
card_name('goblin piker', 'Goblin Piker').
card_type('goblin piker', 'Creature — Goblin Warrior').
card_types('goblin piker', ['Creature']).
card_subtypes('goblin piker', ['Goblin', 'Warrior']).
card_colors('goblin piker', ['R']).
card_text('goblin piker', '').
card_mana_cost('goblin piker', ['1', 'R']).
card_cmc('goblin piker', 2).
card_layout('goblin piker', 'normal').
card_power('goblin piker', 2).
card_toughness('goblin piker', 1).

% Found in: ONS, ORI, VMA, pJGP
card_name('goblin piledriver', 'Goblin Piledriver').
card_type('goblin piledriver', 'Creature — Goblin Warrior').
card_types('goblin piledriver', ['Creature']).
card_subtypes('goblin piledriver', ['Goblin', 'Warrior']).
card_colors('goblin piledriver', ['R']).
card_text('goblin piledriver', 'Protection from blue (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything blue.)\nWhenever Goblin Piledriver attacks, it gets +2/+0 until end of turn for each other attacking Goblin.').
card_mana_cost('goblin piledriver', ['1', 'R']).
card_cmc('goblin piledriver', 2).
card_layout('goblin piledriver', 'normal').
card_power('goblin piledriver', 1).
card_toughness('goblin piledriver', 2).

% Found in: SCG
card_name('goblin psychopath', 'Goblin Psychopath').
card_type('goblin psychopath', 'Creature — Goblin Mutant').
card_types('goblin psychopath', ['Creature']).
card_subtypes('goblin psychopath', ['Goblin', 'Mutant']).
card_colors('goblin psychopath', ['R']).
card_text('goblin psychopath', 'Whenever Goblin Psychopath attacks or blocks, flip a coin. If you lose the flip, the next time it would deal combat damage this turn, it deals that damage to you instead.').
card_mana_cost('goblin psychopath', ['3', 'R']).
card_cmc('goblin psychopath', 4).
card_layout('goblin psychopath', 'normal').
card_power('goblin psychopath', 5).
card_toughness('goblin psychopath', 5).

% Found in: ONS
card_name('goblin pyromancer', 'Goblin Pyromancer').
card_type('goblin pyromancer', 'Creature — Goblin Wizard').
card_types('goblin pyromancer', ['Creature']).
card_subtypes('goblin pyromancer', ['Goblin', 'Wizard']).
card_colors('goblin pyromancer', ['R']).
card_text('goblin pyromancer', 'When Goblin Pyromancer enters the battlefield, Goblin creatures get +3/+0 until end of turn.\nAt the beginning of the end step, destroy all Goblins.').
card_mana_cost('goblin pyromancer', ['3', 'R']).
card_cmc('goblin pyromancer', 4).
card_layout('goblin pyromancer', 'normal').
card_power('goblin pyromancer', 2).
card_toughness('goblin pyromancer', 2).

% Found in: M15, pMEI
card_name('goblin rabblemaster', 'Goblin Rabblemaster').
card_type('goblin rabblemaster', 'Creature — Goblin Warrior').
card_types('goblin rabblemaster', ['Creature']).
card_subtypes('goblin rabblemaster', ['Goblin', 'Warrior']).
card_colors('goblin rabblemaster', ['R']).
card_text('goblin rabblemaster', 'Other Goblin creatures you control attack each turn if able.\nAt the beginning of combat on your turn, put a 1/1 red Goblin creature token with haste onto the battlefield.\nWhenever Goblin Rabblemaster attacks, it gets +1/+0 until end of turn for each other attacking Goblin.').
card_mana_cost('goblin rabblemaster', ['2', 'R']).
card_cmc('goblin rabblemaster', 3).
card_layout('goblin rabblemaster', 'normal').
card_power('goblin rabblemaster', 2).
card_toughness('goblin rabblemaster', 2).

% Found in: 7ED, 8ED, 9ED, PO2, USG
card_name('goblin raider', 'Goblin Raider').
card_type('goblin raider', 'Creature — Goblin Warrior').
card_types('goblin raider', ['Creature']).
card_subtypes('goblin raider', ['Goblin', 'Warrior']).
card_colors('goblin raider', ['R']).
card_text('goblin raider', 'Goblin Raider can\'t block.').
card_mana_cost('goblin raider', ['1', 'R']).
card_cmc('goblin raider', 2).
card_layout('goblin raider', 'normal').
card_power('goblin raider', 2).
card_toughness('goblin raider', 2).

% Found in: RTR
card_name('goblin rally', 'Goblin Rally').
card_type('goblin rally', 'Sorcery').
card_types('goblin rally', ['Sorcery']).
card_subtypes('goblin rally', []).
card_colors('goblin rally', ['R']).
card_text('goblin rally', 'Put four 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('goblin rally', ['3', 'R', 'R']).
card_cmc('goblin rally', 5).
card_layout('goblin rally', 'normal').

% Found in: CON
card_name('goblin razerunners', 'Goblin Razerunners').
card_type('goblin razerunners', 'Creature — Goblin Warrior').
card_types('goblin razerunners', ['Creature']).
card_subtypes('goblin razerunners', ['Goblin', 'Warrior']).
card_colors('goblin razerunners', ['R']).
card_text('goblin razerunners', '{1}{R}, Sacrifice a land: Put a +1/+1 counter on Goblin Razerunners.\nAt the beginning of your end step, you may have Goblin Razerunners deal damage equal to the number of +1/+1 counters on it to target player.').
card_mana_cost('goblin razerunners', ['2', 'R', 'R']).
card_cmc('goblin razerunners', 4).
card_layout('goblin razerunners', 'normal').
card_power('goblin razerunners', 3).
card_toughness('goblin razerunners', 4).

% Found in: 6ED, ATH, VIS
card_name('goblin recruiter', 'Goblin Recruiter').
card_type('goblin recruiter', 'Creature — Goblin').
card_types('goblin recruiter', ['Creature']).
card_subtypes('goblin recruiter', ['Goblin']).
card_colors('goblin recruiter', ['R']).
card_text('goblin recruiter', 'When Goblin Recruiter enters the battlefield, search your library for any number of Goblin cards and reveal those cards. Shuffle your library, then put them on top of it in any order.').
card_mana_cost('goblin recruiter', ['1', 'R']).
card_cmc('goblin recruiter', 2).
card_layout('goblin recruiter', 'normal').
card_power('goblin recruiter', 1).
card_toughness('goblin recruiter', 1).

% Found in: MRD
card_name('goblin replica', 'Goblin Replica').
card_type('goblin replica', 'Artifact Creature — Goblin').
card_types('goblin replica', ['Artifact', 'Creature']).
card_subtypes('goblin replica', ['Goblin']).
card_colors('goblin replica', []).
card_text('goblin replica', '{3}{R}, Sacrifice Goblin Replica: Destroy target artifact.').
card_mana_cost('goblin replica', ['3']).
card_cmc('goblin replica', 3).
card_layout('goblin replica', 'normal').
card_power('goblin replica', 2).
card_toughness('goblin replica', 2).

% Found in: CSP
card_name('goblin rimerunner', 'Goblin Rimerunner').
card_type('goblin rimerunner', 'Snow Creature — Goblin Warrior').
card_types('goblin rimerunner', ['Creature']).
card_subtypes('goblin rimerunner', ['Goblin', 'Warrior']).
card_supertypes('goblin rimerunner', ['Snow']).
card_colors('goblin rimerunner', ['R']).
card_text('goblin rimerunner', '{T}: Target creature can\'t block this turn.\n{S}: Goblin Rimerunner gains haste until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('goblin rimerunner', ['2', 'R']).
card_cmc('goblin rimerunner', 3).
card_layout('goblin rimerunner', 'normal').
card_power('goblin rimerunner', 2).
card_toughness('goblin rimerunner', 2).

% Found in: APC, DD3_EVG, EVG, VMA, pFNM
card_name('goblin ringleader', 'Goblin Ringleader').
card_type('goblin ringleader', 'Creature — Goblin').
card_types('goblin ringleader', ['Creature']).
card_subtypes('goblin ringleader', ['Goblin']).
card_colors('goblin ringleader', ['R']).
card_text('goblin ringleader', 'Haste\nWhen Goblin Ringleader enters the battlefield, reveal the top four cards of your library. Put all Goblin cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('goblin ringleader', ['3', 'R']).
card_cmc('goblin ringleader', 4).
card_layout('goblin ringleader', 'normal').
card_power('goblin ringleader', 2).
card_toughness('goblin ringleader', 2).

% Found in: 4ED, DRK
card_name('goblin rock sled', 'Goblin Rock Sled').
card_type('goblin rock sled', 'Creature — Goblin').
card_types('goblin rock sled', ['Creature']).
card_subtypes('goblin rock sled', ['Goblin']).
card_colors('goblin rock sled', ['R']).
card_text('goblin rock sled', 'Trample\nGoblin Rock Sled doesn\'t untap during your untap step if it attacked during your last turn.\nGoblin Rock Sled can\'t attack unless defending player controls a Mountain.').
card_mana_cost('goblin rock sled', ['1', 'R']).
card_cmc('goblin rock sled', 2).
card_layout('goblin rock sled', 'normal').
card_power('goblin rock sled', 3).
card_toughness('goblin rock sled', 1).

% Found in: M15, WWK
card_name('goblin roughrider', 'Goblin Roughrider').
card_type('goblin roughrider', 'Creature — Goblin Knight').
card_types('goblin roughrider', ['Creature']).
card_subtypes('goblin roughrider', ['Goblin', 'Knight']).
card_colors('goblin roughrider', ['R']).
card_text('goblin roughrider', '').
card_mana_cost('goblin roughrider', ['2', 'R']).
card_cmc('goblin roughrider', 3).
card_layout('goblin roughrider', 'normal').
card_power('goblin roughrider', 3).
card_toughness('goblin roughrider', 2).

% Found in: ZEN
card_name('goblin ruinblaster', 'Goblin Ruinblaster').
card_type('goblin ruinblaster', 'Creature — Goblin Shaman').
card_types('goblin ruinblaster', ['Creature']).
card_subtypes('goblin ruinblaster', ['Goblin', 'Shaman']).
card_colors('goblin ruinblaster', ['R']).
card_text('goblin ruinblaster', 'Kicker {R} (You may pay an additional {R} as you cast this spell.)\nHaste\nWhen Goblin Ruinblaster enters the battlefield, if it was kicked, destroy target nonbasic land.').
card_mana_cost('goblin ruinblaster', ['2', 'R']).
card_cmc('goblin ruinblaster', 3).
card_layout('goblin ruinblaster', 'normal').
card_power('goblin ruinblaster', 2).
card_toughness('goblin ruinblaster', 1).

% Found in: UNH
card_name('goblin s.w.a.t. team', 'Goblin S.W.A.T. Team').
card_type('goblin s.w.a.t. team', 'Creature — Goblin Warrior').
card_types('goblin s.w.a.t. team', ['Creature']).
card_subtypes('goblin s.w.a.t. team', ['Goblin', 'Warrior']).
card_colors('goblin s.w.a.t. team', ['R']).
card_text('goblin s.w.a.t. team', 'Say \"Goblin S.W.A.T. Team\": Put a +1/+1 counter on Goblin S.W.A.T. Team unless an opponent swats the table within five seconds. Play this ability only once each turn.').
card_mana_cost('goblin s.w.a.t. team', ['3', 'R']).
card_cmc('goblin s.w.a.t. team', 4).
card_layout('goblin s.w.a.t. team', 'normal').
card_power('goblin s.w.a.t. team', 2).
card_toughness('goblin s.w.a.t. team', 2).

% Found in: ICE
card_name('goblin sappers', 'Goblin Sappers').
card_type('goblin sappers', 'Creature — Goblin').
card_types('goblin sappers', ['Creature']).
card_subtypes('goblin sappers', ['Goblin']).
card_colors('goblin sappers', ['R']).
card_text('goblin sappers', '{R}{R}, {T}: Target creature you control can\'t be blocked this turn. Destroy it and Goblin Sappers at end of combat.\n{R}{R}{R}{R}, {T}: Target creature you control can\'t be blocked this turn. Destroy it at end of combat.').
card_mana_cost('goblin sappers', ['1', 'R']).
card_cmc('goblin sappers', 2).
card_layout('goblin sappers', 'normal').
card_power('goblin sappers', 1).
card_toughness('goblin sappers', 1).

% Found in: MIR
card_name('goblin scouts', 'Goblin Scouts').
card_type('goblin scouts', 'Sorcery').
card_types('goblin scouts', ['Sorcery']).
card_subtypes('goblin scouts', []).
card_colors('goblin scouts', ['R']).
card_text('goblin scouts', 'Put three 1/1 red Goblin Scout creature tokens with mountainwalk onto the battlefield. (They can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('goblin scouts', ['3', 'R', 'R']).
card_cmc('goblin scouts', 5).
card_layout('goblin scouts', 'normal').

% Found in: UNH
card_name('goblin secret agent', 'Goblin Secret Agent').
card_type('goblin secret agent', 'Creature — Goblin Rogue').
card_types('goblin secret agent', ['Creature']).
card_subtypes('goblin secret agent', ['Goblin', 'Rogue']).
card_colors('goblin secret agent', ['R']).
card_text('goblin secret agent', 'First strike\nAt the beginning of your upkeep, reveal a card from your hand at random.').
card_mana_cost('goblin secret agent', ['2', 'R']).
card_cmc('goblin secret agent', 3).
card_layout('goblin secret agent', 'normal').
card_power('goblin secret agent', 2).
card_toughness('goblin secret agent', 2).

% Found in: S99, VMA
card_name('goblin settler', 'Goblin Settler').
card_type('goblin settler', 'Creature — Goblin').
card_types('goblin settler', ['Creature']).
card_subtypes('goblin settler', ['Goblin']).
card_colors('goblin settler', ['R']).
card_text('goblin settler', 'When Goblin Settler enters the battlefield, destroy target land.').
card_mana_cost('goblin settler', ['3', 'R']).
card_cmc('goblin settler', 4).
card_layout('goblin settler', 'normal').
card_power('goblin settler', 1).
card_toughness('goblin settler', 1).

% Found in: C13, ONS
card_name('goblin sharpshooter', 'Goblin Sharpshooter').
card_type('goblin sharpshooter', 'Creature — Goblin').
card_types('goblin sharpshooter', ['Creature']).
card_subtypes('goblin sharpshooter', ['Goblin']).
card_colors('goblin sharpshooter', ['R']).
card_text('goblin sharpshooter', 'Goblin Sharpshooter doesn\'t untap during your untap step.\nWhenever a creature dies, untap Goblin Sharpshooter.\n{T}: Goblin Sharpshooter deals 1 damage to target creature or player.').
card_mana_cost('goblin sharpshooter', ['2', 'R']).
card_cmc('goblin sharpshooter', 3).
card_layout('goblin sharpshooter', 'normal').
card_power('goblin sharpshooter', 1).
card_toughness('goblin sharpshooter', 1).

% Found in: M14, ZEN
card_name('goblin shortcutter', 'Goblin Shortcutter').
card_type('goblin shortcutter', 'Creature — Goblin Scout').
card_types('goblin shortcutter', ['Creature']).
card_subtypes('goblin shortcutter', ['Goblin', 'Scout']).
card_colors('goblin shortcutter', ['R']).
card_text('goblin shortcutter', 'When Goblin Shortcutter enters the battlefield, target creature can\'t block this turn.').
card_mana_cost('goblin shortcutter', ['1', 'R']).
card_cmc('goblin shortcutter', 2).
card_layout('goblin shortcutter', 'normal').
card_power('goblin shortcutter', 2).
card_toughness('goblin shortcutter', 1).

% Found in: CHR, DRK, ME4
card_name('goblin shrine', 'Goblin Shrine').
card_type('goblin shrine', 'Enchantment — Aura').
card_types('goblin shrine', ['Enchantment']).
card_subtypes('goblin shrine', ['Aura']).
card_colors('goblin shrine', ['R']).
card_text('goblin shrine', 'Enchant land\nAs long as enchanted land is a basic Mountain, Goblin creatures get +1/+0.\nWhen Goblin Shrine leaves the battlefield, it deals 1 damage to each Goblin creature.').
card_mana_cost('goblin shrine', ['1', 'R', 'R']).
card_cmc('goblin shrine', 3).
card_layout('goblin shrine', 'normal').

% Found in: ICE, ME2
card_name('goblin ski patrol', 'Goblin Ski Patrol').
card_type('goblin ski patrol', 'Creature — Goblin').
card_types('goblin ski patrol', ['Creature']).
card_subtypes('goblin ski patrol', ['Goblin']).
card_colors('goblin ski patrol', ['R']).
card_text('goblin ski patrol', '{1}{R}: Goblin Ski Patrol gets +2/+0 and gains flying. Its controller sacrifices it at the beginning of the next end step. Activate this ability only once and only if you control a snow Mountain.').
card_mana_cost('goblin ski patrol', ['1', 'R']).
card_cmc('goblin ski patrol', 2).
card_layout('goblin ski patrol', 'normal').
card_power('goblin ski patrol', 1).
card_toughness('goblin ski patrol', 1).

% Found in: 10E, 9ED, DPA, ONS
card_name('goblin sky raider', 'Goblin Sky Raider').
card_type('goblin sky raider', 'Creature — Goblin Warrior').
card_types('goblin sky raider', ['Creature']).
card_subtypes('goblin sky raider', ['Goblin', 'Warrior']).
card_colors('goblin sky raider', ['R']).
card_text('goblin sky raider', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('goblin sky raider', ['2', 'R']).
card_cmc('goblin sky raider', 3).
card_layout('goblin sky raider', 'normal').
card_power('goblin sky raider', 1).
card_toughness('goblin sky raider', 2).

% Found in: TSP
card_name('goblin skycutter', 'Goblin Skycutter').
card_type('goblin skycutter', 'Creature — Goblin Warrior').
card_types('goblin skycutter', ['Creature']).
card_subtypes('goblin skycutter', ['Goblin', 'Warrior']).
card_colors('goblin skycutter', ['R']).
card_text('goblin skycutter', 'Sacrifice Goblin Skycutter: Goblin Skycutter deals 2 damage to target creature with flying. That creature loses flying until end of turn.').
card_mana_cost('goblin skycutter', ['1', 'R']).
card_cmc('goblin skycutter', 2).
card_layout('goblin skycutter', 'normal').
card_power('goblin skycutter', 2).
card_toughness('goblin skycutter', 1).

% Found in: DD3_EVG, EVG, ONS
card_name('goblin sledder', 'Goblin Sledder').
card_type('goblin sledder', 'Creature — Goblin').
card_types('goblin sledder', ['Creature']).
card_subtypes('goblin sledder', ['Goblin']).
card_colors('goblin sledder', ['R']).
card_text('goblin sledder', 'Sacrifice a Goblin: Target creature gets +1/+1 until end of turn.').
card_mana_cost('goblin sledder', ['R']).
card_cmc('goblin sledder', 1).
card_layout('goblin sledder', 'normal').
card_power('goblin sledder', 1).
card_toughness('goblin sledder', 1).

% Found in: ATH, ICE, TSB
card_name('goblin snowman', 'Goblin Snowman').
card_type('goblin snowman', 'Creature — Goblin').
card_types('goblin snowman', ['Creature']).
card_subtypes('goblin snowman', ['Goblin']).
card_colors('goblin snowman', ['R']).
card_text('goblin snowman', 'Whenever Goblin Snowman blocks, prevent all combat damage that would be dealt to and dealt by it this turn.\n{T}: Goblin Snowman deals 1 damage to target creature it\'s blocking.').
card_mana_cost('goblin snowman', ['3', 'R']).
card_cmc('goblin snowman', 4).
card_layout('goblin snowman', 'normal').
card_power('goblin snowman', 1).
card_toughness('goblin snowman', 1).

% Found in: MIR
card_name('goblin soothsayer', 'Goblin Soothsayer').
card_type('goblin soothsayer', 'Creature — Goblin Shaman').
card_types('goblin soothsayer', ['Creature']).
card_subtypes('goblin soothsayer', ['Goblin', 'Shaman']).
card_colors('goblin soothsayer', ['R']).
card_text('goblin soothsayer', '{R}, {T}, Sacrifice a Goblin: Red creatures get +1/+1 until end of turn.').
card_mana_cost('goblin soothsayer', ['R']).
card_cmc('goblin soothsayer', 1).
card_layout('goblin soothsayer', 'normal').
card_power('goblin soothsayer', 1).
card_toughness('goblin soothsayer', 1).

% Found in: 7ED, RAV, USG
card_name('goblin spelunkers', 'Goblin Spelunkers').
card_type('goblin spelunkers', 'Creature — Goblin Warrior').
card_types('goblin spelunkers', ['Creature']).
card_subtypes('goblin spelunkers', ['Goblin', 'Warrior']).
card_colors('goblin spelunkers', ['R']).
card_text('goblin spelunkers', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('goblin spelunkers', ['2', 'R']).
card_cmc('goblin spelunkers', 3).
card_layout('goblin spelunkers', 'normal').
card_power('goblin spelunkers', 2).
card_toughness('goblin spelunkers', 2).

% Found in: INV
card_name('goblin spy', 'Goblin Spy').
card_type('goblin spy', 'Creature — Goblin Rogue').
card_types('goblin spy', ['Creature']).
card_subtypes('goblin spy', ['Goblin', 'Rogue']).
card_colors('goblin spy', ['R']).
card_text('goblin spy', 'Play with the top card of your library revealed.').
card_mana_cost('goblin spy', ['R']).
card_cmc('goblin spy', 1).
card_layout('goblin spy', 'normal').
card_power('goblin spy', 1).
card_toughness('goblin spy', 1).

% Found in: MRD
card_name('goblin striker', 'Goblin Striker').
card_type('goblin striker', 'Creature — Goblin Berserker').
card_types('goblin striker', ['Creature']).
card_subtypes('goblin striker', ['Goblin', 'Berserker']).
card_colors('goblin striker', ['R']).
card_text('goblin striker', 'First strike, haste').
card_mana_cost('goblin striker', ['1', 'R']).
card_cmc('goblin striker', 2).
card_layout('goblin striker', 'normal').
card_power('goblin striker', 1).
card_toughness('goblin striker', 1).

% Found in: VIS
card_name('goblin swine-rider', 'Goblin Swine-Rider').
card_type('goblin swine-rider', 'Creature — Goblin').
card_types('goblin swine-rider', ['Creature']).
card_subtypes('goblin swine-rider', ['Goblin']).
card_colors('goblin swine-rider', ['R']).
card_text('goblin swine-rider', 'Whenever Goblin Swine-Rider becomes blocked, it deals 2 damage to each attacking creature and each blocking creature.').
card_mana_cost('goblin swine-rider', ['R']).
card_cmc('goblin swine-rider', 1).
card_layout('goblin swine-rider', 'normal').
card_power('goblin swine-rider', 1).
card_toughness('goblin swine-rider', 1).

% Found in: ONS
card_name('goblin taskmaster', 'Goblin Taskmaster').
card_type('goblin taskmaster', 'Creature — Goblin').
card_types('goblin taskmaster', ['Creature']).
card_subtypes('goblin taskmaster', ['Goblin']).
card_colors('goblin taskmaster', ['R']).
card_text('goblin taskmaster', '{1}{R}: Target Goblin creature gets +1/+0 until end of turn.\nMorph {R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('goblin taskmaster', ['R']).
card_cmc('goblin taskmaster', 1).
card_layout('goblin taskmaster', 'normal').
card_power('goblin taskmaster', 1).
card_toughness('goblin taskmaster', 1).

% Found in: DGM
card_name('goblin test pilot', 'Goblin Test Pilot').
card_type('goblin test pilot', 'Creature — Goblin Wizard').
card_types('goblin test pilot', ['Creature']).
card_subtypes('goblin test pilot', ['Goblin', 'Wizard']).
card_colors('goblin test pilot', ['U', 'R']).
card_text('goblin test pilot', 'Flying\n{T}: Goblin Test Pilot deals 2 damage to target creature or player chosen at random.').
card_mana_cost('goblin test pilot', ['1', 'U', 'R']).
card_cmc('goblin test pilot', 3).
card_layout('goblin test pilot', 'normal').
card_power('goblin test pilot', 0).
card_toughness('goblin test pilot', 2).

% Found in: ATH, MIR
card_name('goblin tinkerer', 'Goblin Tinkerer').
card_type('goblin tinkerer', 'Creature — Goblin Artificer').
card_types('goblin tinkerer', ['Creature']).
card_subtypes('goblin tinkerer', ['Goblin', 'Artificer']).
card_colors('goblin tinkerer', ['R']).
card_text('goblin tinkerer', '{R}, {T}: Destroy target artifact. That artifact deals damage equal to its converted mana cost to Goblin Tinkerer.').
card_mana_cost('goblin tinkerer', ['1', 'R']).
card_cmc('goblin tinkerer', 2).
card_layout('goblin tinkerer', 'normal').
card_power('goblin tinkerer', 1).
card_toughness('goblin tinkerer', 2).

% Found in: UGL
card_name('goblin token card', 'Goblin token card').
card_type('goblin token card', '').
card_types('goblin token card', []).
card_subtypes('goblin token card', []).
card_colors('goblin token card', []).
card_text('goblin token card', '').
card_layout('goblin token card', 'token').

% Found in: APC, VMA
card_name('goblin trenches', 'Goblin Trenches').
card_type('goblin trenches', 'Enchantment').
card_types('goblin trenches', ['Enchantment']).
card_subtypes('goblin trenches', []).
card_colors('goblin trenches', ['W', 'R']).
card_text('goblin trenches', '{2}, Sacrifice a land: Put two 1/1 red and white Goblin Soldier creature tokens onto the battlefield.').
card_mana_cost('goblin trenches', ['1', 'R', 'W']).
card_cmc('goblin trenches', 3).
card_layout('goblin trenches', 'normal').

% Found in: M11, M12, ROE
card_name('goblin tunneler', 'Goblin Tunneler').
card_type('goblin tunneler', 'Creature — Goblin Rogue').
card_types('goblin tunneler', ['Creature']).
card_subtypes('goblin tunneler', ['Goblin', 'Rogue']).
card_colors('goblin tunneler', ['R']).
card_text('goblin tunneler', '{T}: Target creature with power 2 or less can\'t be blocked this turn.').
card_mana_cost('goblin tunneler', ['1', 'R']).
card_cmc('goblin tunneler', 2).
card_layout('goblin tunneler', 'normal').
card_power('goblin tunneler', 1).
card_toughness('goblin tunneler', 1).

% Found in: LGN
card_name('goblin turncoat', 'Goblin Turncoat').
card_type('goblin turncoat', 'Creature — Goblin Mercenary').
card_types('goblin turncoat', ['Creature']).
card_subtypes('goblin turncoat', ['Goblin', 'Mercenary']).
card_colors('goblin turncoat', ['B']).
card_text('goblin turncoat', 'Sacrifice a Goblin: Regenerate Goblin Turncoat.').
card_mana_cost('goblin turncoat', ['1', 'B']).
card_cmc('goblin turncoat', 2).
card_layout('goblin turncoat', 'normal').
card_power('goblin turncoat', 2).
card_toughness('goblin turncoat', 1).

% Found in: UGL
card_name('goblin tutor', 'Goblin Tutor').
card_type('goblin tutor', 'Instant').
card_types('goblin tutor', ['Instant']).
card_subtypes('goblin tutor', []).
card_colors('goblin tutor', ['R']).
card_text('goblin tutor', 'Roll a six-sided die for Goblin Tutor. On a 1, Goblin Tutor has no effect. Otherwise, search your library for the indicated card, reveal that card to all players, and put it into your hand. Shuffle your library afterwards.\n2 Any Goblin Tutor\n3 Any enchantment\n4 Any artifact\n5 Any creature\n6 Any sorcery, instant, or interrupt').
card_mana_cost('goblin tutor', ['R']).
card_cmc('goblin tutor', 1).
card_layout('goblin tutor', 'normal').

% Found in: ATH, WTH
card_name('goblin vandal', 'Goblin Vandal').
card_type('goblin vandal', 'Creature — Goblin Rogue').
card_types('goblin vandal', ['Creature']).
card_subtypes('goblin vandal', ['Goblin', 'Rogue']).
card_colors('goblin vandal', ['R']).
card_text('goblin vandal', 'Whenever Goblin Vandal attacks and isn\'t blocked, you may pay {R}. If you do, destroy target artifact defending player controls and Goblin Vandal assigns no combat damage this turn.').
card_mana_cost('goblin vandal', ['R']).
card_cmc('goblin vandal', 1).
card_layout('goblin vandal', 'normal').
card_power('goblin vandal', 1).
card_toughness('goblin vandal', 1).

% Found in: USG
card_name('goblin war buggy', 'Goblin War Buggy').
card_type('goblin war buggy', 'Creature — Goblin').
card_types('goblin war buggy', ['Creature']).
card_subtypes('goblin war buggy', ['Goblin']).
card_colors('goblin war buggy', ['R']).
card_text('goblin war buggy', 'Haste\nEcho {1}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('goblin war buggy', ['1', 'R']).
card_cmc('goblin war buggy', 2).
card_layout('goblin war buggy', 'normal').
card_power('goblin war buggy', 2).
card_toughness('goblin war buggy', 2).

% Found in: PO2
card_name('goblin war cry', 'Goblin War Cry').
card_type('goblin war cry', 'Sorcery').
card_types('goblin war cry', ['Sorcery']).
card_subtypes('goblin war cry', []).
card_colors('goblin war cry', ['R']).
card_text('goblin war cry', 'Target opponent chooses a creature he or she controls. Other creatures he or she controls can\'t block this turn.').
card_mana_cost('goblin war cry', ['2', 'R']).
card_cmc('goblin war cry', 3).
card_layout('goblin war cry', 'normal').

% Found in: 5ED, 7ED, FEM
card_name('goblin war drums', 'Goblin War Drums').
card_type('goblin war drums', 'Enchantment').
card_types('goblin war drums', ['Enchantment']).
card_subtypes('goblin war drums', []).
card_colors('goblin war drums', ['R']).
card_text('goblin war drums', 'Creatures you control have menace. (They can\'t be blocked except by two or more creatures.)').
card_mana_cost('goblin war drums', ['2', 'R']).
card_cmc('goblin war drums', 3).
card_layout('goblin war drums', 'normal').

% Found in: BFZ, M12, MM2, ZEN
card_name('goblin war paint', 'Goblin War Paint').
card_type('goblin war paint', 'Enchantment — Aura').
card_types('goblin war paint', ['Enchantment']).
card_subtypes('goblin war paint', ['Aura']).
card_colors('goblin war paint', ['R']).
card_text('goblin war paint', 'Enchant creature\nEnchanted creature gets +2/+2 and has haste.').
card_mana_cost('goblin war paint', ['1', 'R']).
card_cmc('goblin war paint', 2).
card_layout('goblin war paint', 'normal').

% Found in: PO2, SCG
card_name('goblin war strike', 'Goblin War Strike').
card_type('goblin war strike', 'Sorcery').
card_types('goblin war strike', ['Sorcery']).
card_subtypes('goblin war strike', []).
card_colors('goblin war strike', ['R']).
card_text('goblin war strike', 'Goblin War Strike deals damage equal to the number of Goblins you control to target player.').
card_mana_cost('goblin war strike', ['R']).
card_cmc('goblin war strike', 1).
card_layout('goblin war strike', 'normal').

% Found in: MRD
card_name('goblin war wagon', 'Goblin War Wagon').
card_type('goblin war wagon', 'Artifact Creature — Juggernaut').
card_types('goblin war wagon', ['Artifact', 'Creature']).
card_subtypes('goblin war wagon', ['Juggernaut']).
card_colors('goblin war wagon', []).
card_text('goblin war wagon', 'Goblin War Wagon doesn\'t untap during your untap step.\nAt the beginning of your upkeep, you may pay {2}. If you do, untap Goblin War Wagon.').
card_mana_cost('goblin war wagon', ['4']).
card_cmc('goblin war wagon', 4).
card_layout('goblin war wagon', 'normal').
card_power('goblin war wagon', 3).
card_toughness('goblin war wagon', 3).

% Found in: DD3_EVG, DDN, EVG, SCG, VMA, pFNM
card_name('goblin warchief', 'Goblin Warchief').
card_type('goblin warchief', 'Creature — Goblin Warrior').
card_types('goblin warchief', ['Creature']).
card_subtypes('goblin warchief', ['Goblin', 'Warrior']).
card_colors('goblin warchief', ['R']).
card_text('goblin warchief', 'Goblin spells you cast cost {1} less to cast.\nGoblin creatures you control have haste.').
card_mana_cost('goblin warchief', ['1', 'R', 'R']).
card_cmc('goblin warchief', 3).
card_layout('goblin warchief', 'normal').
card_power('goblin warchief', 2).
card_toughness('goblin warchief', 2).

% Found in: VAN
card_name('goblin warchief avatar', 'Goblin Warchief Avatar').
card_type('goblin warchief avatar', 'Vanguard').
card_types('goblin warchief avatar', ['Vanguard']).
card_subtypes('goblin warchief avatar', []).
card_colors('goblin warchief avatar', []).
card_text('goblin warchief avatar', 'Attacking creatures you control get +1/+0.').
card_layout('goblin warchief avatar', 'vanguard').

% Found in: MBS
card_name('goblin wardriver', 'Goblin Wardriver').
card_type('goblin wardriver', 'Creature — Goblin Warrior').
card_types('goblin wardriver', ['Creature']).
card_subtypes('goblin wardriver', ['Goblin', 'Warrior']).
card_colors('goblin wardriver', ['R']).
card_text('goblin wardriver', 'Battle cry (Whenever this creature attacks, each other attacking creature gets +1/+0 until end of turn.)').
card_mana_cost('goblin wardriver', ['R', 'R']).
card_cmc('goblin wardriver', 2).
card_layout('goblin wardriver', 'normal').
card_power('goblin wardriver', 2).
card_toughness('goblin wardriver', 2).

% Found in: 5ED, 6ED, ATH, FEM, ME4
card_name('goblin warrens', 'Goblin Warrens').
card_type('goblin warrens', 'Enchantment').
card_types('goblin warrens', ['Enchantment']).
card_subtypes('goblin warrens', []).
card_colors('goblin warrens', ['R']).
card_text('goblin warrens', '{2}{R}, Sacrifice two Goblins: Put three 1/1 red Goblin creature tokens onto the battlefield.').
card_mana_cost('goblin warrens', ['2', 'R']).
card_cmc('goblin warrens', 3).
card_layout('goblin warrens', 'normal').

% Found in: C14, ULG, pJGP
card_name('goblin welder', 'Goblin Welder').
card_type('goblin welder', 'Creature — Goblin Artificer').
card_types('goblin welder', ['Creature']).
card_subtypes('goblin welder', ['Goblin', 'Artificer']).
card_colors('goblin welder', ['R']).
card_text('goblin welder', '{T}: Choose target artifact a player controls and target artifact card in that player\'s graveyard. If both targets are still legal as this ability resolves, that player simultaneously sacrifices the artifact and returns the artifact card to the battlefield.').
card_mana_cost('goblin welder', ['R']).
card_cmc('goblin welder', 1).
card_layout('goblin welder', 'normal').
card_power('goblin welder', 1).
card_toughness('goblin welder', 1).

% Found in: DRK, MED
card_name('goblin wizard', 'Goblin Wizard').
card_type('goblin wizard', 'Creature — Goblin Wizard').
card_types('goblin wizard', ['Creature']).
card_subtypes('goblin wizard', ['Goblin', 'Wizard']).
card_colors('goblin wizard', ['R']).
card_text('goblin wizard', '{T}: You may put a Goblin permanent card from your hand onto the battlefield.\n{R}: Target Goblin gains protection from white until end of turn.').
card_mana_cost('goblin wizard', ['2', 'R', 'R']).
card_cmc('goblin wizard', 4).
card_layout('goblin wizard', 'normal').
card_power('goblin wizard', 1).
card_toughness('goblin wizard', 1).
card_reserved('goblin wizard').

% Found in: CHR, DRK, MED
card_name('goblins of the flarg', 'Goblins of the Flarg').
card_type('goblins of the flarg', 'Creature — Goblin Warrior').
card_types('goblins of the flarg', ['Creature']).
card_subtypes('goblins of the flarg', ['Goblin', 'Warrior']).
card_colors('goblins of the flarg', ['R']).
card_text('goblins of the flarg', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)\nWhen you control a Dwarf, sacrifice Goblins of the Flarg.').
card_mana_cost('goblins of the flarg', ['R']).
card_cmc('goblins of the flarg', 1).
card_layout('goblins of the flarg', 'normal').
card_power('goblins of the flarg', 1).
card_toughness('goblins of the flarg', 1).

% Found in: KTK
card_name('goblinslide', 'Goblinslide').
card_type('goblinslide', 'Enchantment').
card_types('goblinslide', ['Enchantment']).
card_subtypes('goblinslide', []).
card_colors('goblinslide', ['R']).
card_text('goblinslide', 'Whenever you cast a noncreature spell, you may pay {1}. If you do, put a 1/1 red Goblin creature token with haste onto the battlefield.').
card_mana_cost('goblinslide', ['2', 'R']).
card_cmc('goblinslide', 3).
card_layout('goblinslide', 'normal').

% Found in: BNG
card_name('god-favored general', 'God-Favored General').
card_type('god-favored general', 'Creature — Human Soldier').
card_types('god-favored general', ['Creature']).
card_subtypes('god-favored general', ['Human', 'Soldier']).
card_colors('god-favored general', ['W']).
card_text('god-favored general', 'Inspired — Whenever God-Favored General becomes untapped, you may pay {2}{W}. If you do, put two 1/1 white Soldier enchantment creature tokens onto the battlefield.').
card_mana_cost('god-favored general', ['1', 'W']).
card_cmc('god-favored general', 2).
card_layout('god-favored general', 'normal').
card_power('god-favored general', 1).
card_toughness('god-favored general', 1).

% Found in: SHM
card_name('godhead of awe', 'Godhead of Awe').
card_type('godhead of awe', 'Creature — Spirit Avatar').
card_types('godhead of awe', ['Creature']).
card_subtypes('godhead of awe', ['Spirit', 'Avatar']).
card_colors('godhead of awe', ['W', 'U']).
card_text('godhead of awe', 'Flying\nOther creatures have base power and toughness 1/1.').
card_mana_cost('godhead of awe', ['W/U', 'W/U', 'W/U', 'W/U', 'W/U']).
card_cmc('godhead of awe', 5).
card_layout('godhead of awe', 'normal').
card_power('godhead of awe', 4).
card_toughness('godhead of awe', 4).

% Found in: JOU
card_name('godhunter octopus', 'Godhunter Octopus').
card_type('godhunter octopus', 'Creature — Octopus').
card_types('godhunter octopus', ['Creature']).
card_subtypes('godhunter octopus', ['Octopus']).
card_colors('godhunter octopus', ['U']).
card_text('godhunter octopus', 'Godhunter Octopus can\'t attack unless defending player controls an enchantment or an enchanted permanent.').
card_mana_cost('godhunter octopus', ['5', 'U']).
card_cmc('godhunter octopus', 6).
card_layout('godhunter octopus', 'normal').
card_power('godhunter octopus', 5).
card_toughness('godhunter octopus', 5).

% Found in: EXP, GPT, GTC
card_name('godless shrine', 'Godless Shrine').
card_type('godless shrine', 'Land — Plains Swamp').
card_types('godless shrine', ['Land']).
card_subtypes('godless shrine', ['Plains', 'Swamp']).
card_colors('godless shrine', []).
card_text('godless shrine', '({T}: Add {W} or {B} to your mana pool.)\nAs Godless Shrine enters the battlefield, you may pay 2 life. If you don\'t, Godless Shrine enters the battlefield tapped.').
card_layout('godless shrine', 'normal').

% Found in: SOK
card_name('godo\'s irregulars', 'Godo\'s Irregulars').
card_type('godo\'s irregulars', 'Creature — Human Warrior').
card_types('godo\'s irregulars', ['Creature']).
card_subtypes('godo\'s irregulars', ['Human', 'Warrior']).
card_colors('godo\'s irregulars', ['R']).
card_text('godo\'s irregulars', '{R}: Godo\'s Irregulars deals 1 damage to target creature blocking it.').
card_mana_cost('godo\'s irregulars', ['R']).
card_cmc('godo\'s irregulars', 1).
card_layout('godo\'s irregulars', 'normal').
card_power('godo\'s irregulars', 1).
card_toughness('godo\'s irregulars', 1).

% Found in: CHK
card_name('godo, bandit warlord', 'Godo, Bandit Warlord').
card_type('godo, bandit warlord', 'Legendary Creature — Human Barbarian').
card_types('godo, bandit warlord', ['Creature']).
card_subtypes('godo, bandit warlord', ['Human', 'Barbarian']).
card_supertypes('godo, bandit warlord', ['Legendary']).
card_colors('godo, bandit warlord', ['R']).
card_text('godo, bandit warlord', 'When Godo, Bandit Warlord enters the battlefield, you may search your library for an Equipment card and put it onto the battlefield. If you do, shuffle your library.\nWhenever Godo attacks for the first time each turn, untap it and all Samurai you control. After this phase, there is an additional combat phase.').
card_mana_cost('godo, bandit warlord', ['5', 'R']).
card_cmc('godo, bandit warlord', 6).
card_layout('godo, bandit warlord', 'normal').
card_power('godo, bandit warlord', 3).
card_toughness('godo, bandit warlord', 3).

% Found in: THS
card_name('gods willing', 'Gods Willing').
card_type('gods willing', 'Instant').
card_types('gods willing', ['Instant']).
card_subtypes('gods willing', []).
card_colors('gods willing', ['W']).
card_text('gods willing', 'Target creature you control gains protection from the color of your choice until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('gods willing', ['W']).
card_cmc('gods willing', 1).
card_layout('gods willing', 'normal').

% Found in: BOK
card_name('gods\' eye, gate to the reikai', 'Gods\' Eye, Gate to the Reikai').
card_type('gods\' eye, gate to the reikai', 'Legendary Land').
card_types('gods\' eye, gate to the reikai', ['Land']).
card_subtypes('gods\' eye, gate to the reikai', []).
card_supertypes('gods\' eye, gate to the reikai', ['Legendary']).
card_colors('gods\' eye, gate to the reikai', []).
card_text('gods\' eye, gate to the reikai', '{T}: Add {1} to your mana pool.\nWhen Gods\' Eye, Gate to the Reikai is put into a graveyard from the battlefield, put a 1/1 colorless Spirit creature token onto the battlefield.').
card_layout('gods\' eye, gate to the reikai', 'normal').

% Found in: JOU
card_name('godsend', 'Godsend').
card_type('godsend', 'Legendary Artifact — Equipment').
card_types('godsend', ['Artifact']).
card_subtypes('godsend', ['Equipment']).
card_supertypes('godsend', ['Legendary']).
card_colors('godsend', ['W']).
card_text('godsend', 'Equipped creature gets +3/+3.\nWhenever equipped creature blocks or becomes blocked by one or more creatures, you may exile one of those creatures.\nOpponents can\'t cast cards with the same name as cards exiled with Godsend.\nEquip {3}').
card_mana_cost('godsend', ['1', 'W', 'W']).
card_cmc('godsend', 3).
card_layout('godsend', 'normal').

% Found in: ALA
card_name('godsire', 'Godsire').
card_type('godsire', 'Creature — Beast').
card_types('godsire', ['Creature']).
card_subtypes('godsire', ['Beast']).
card_colors('godsire', ['W', 'R', 'G']).
card_text('godsire', 'Vigilance\n{T}: Put an 8/8 Beast creature token that\'s red, green, and white onto the battlefield.').
card_mana_cost('godsire', ['4', 'R', 'G', 'G', 'W']).
card_cmc('godsire', 8).
card_layout('godsire', 'normal').
card_power('godsire', 8).
card_toughness('godsire', 8).

% Found in: ALA
card_name('godtoucher', 'Godtoucher').
card_type('godtoucher', 'Creature — Elf Cleric').
card_types('godtoucher', ['Creature']).
card_subtypes('godtoucher', ['Elf', 'Cleric']).
card_colors('godtoucher', ['G']).
card_text('godtoucher', '{1}{W}, {T}: Prevent all damage that would be dealt to target creature with power 5 or greater this turn.').
card_mana_cost('godtoucher', ['3', 'G']).
card_cmc('godtoucher', 4).
card_layout('godtoucher', 'normal').
card_power('godtoucher', 2).
card_toughness('godtoucher', 2).

% Found in: ARB
card_name('godtracker of jund', 'Godtracker of Jund').
card_type('godtracker of jund', 'Creature — Elf Shaman').
card_types('godtracker of jund', ['Creature']).
card_subtypes('godtracker of jund', ['Elf', 'Shaman']).
card_colors('godtracker of jund', ['R', 'G']).
card_text('godtracker of jund', 'Whenever a creature with power 5 or greater enters the battlefield under your control, you may put a +1/+1 counter on Godtracker of Jund.').
card_mana_cost('godtracker of jund', ['1', 'R', 'G']).
card_cmc('godtracker of jund', 3).
card_layout('godtracker of jund', 'normal').
card_power('godtracker of jund', 2).
card_toughness('godtracker of jund', 2).

% Found in: INV
card_name('goham djinn', 'Goham Djinn').
card_type('goham djinn', 'Creature — Djinn').
card_types('goham djinn', ['Creature']).
card_subtypes('goham djinn', ['Djinn']).
card_colors('goham djinn', ['B']).
card_text('goham djinn', '{1}{B}: Regenerate Goham Djinn.\nGoham Djinn gets -2/-2 as long as black is the most common color among all permanents or is tied for most common.').
card_mana_cost('goham djinn', ['5', 'B']).
card_cmc('goham djinn', 6).
card_layout('goham djinn', 'normal').
card_power('goham djinn', 5).
card_toughness('goham djinn', 5).

% Found in: CHK
card_name('goka the unjust', 'Goka the Unjust').
card_type('goka the unjust', 'Legendary Creature — Ogre Shaman').
card_types('goka the unjust', ['Creature']).
card_subtypes('goka the unjust', ['Ogre', 'Shaman']).
card_supertypes('goka the unjust', ['Legendary']).
card_colors('goka the unjust', ['R']).
card_text('goka the unjust', '{T}: Goka the Unjust deals 4 damage to target creature that was dealt damage this turn.').
card_mana_cost('goka the unjust', ['3', 'R']).
card_cmc('goka the unjust', 4).
card_layout('goka the unjust', 'flip').
card_power('goka the unjust', 4).
card_toughness('goka the unjust', 4).

% Found in: HOP, MRD, SOM
card_name('gold myr', 'Gold Myr').
card_type('gold myr', 'Artifact Creature — Myr').
card_types('gold myr', ['Artifact', 'Creature']).
card_subtypes('gold myr', ['Myr']).
card_colors('gold myr', []).
card_text('gold myr', '{T}: Add {W} to your mana pool.').
card_mana_cost('gold myr', ['2']).
card_cmc('gold myr', 2).
card_layout('gold myr', 'normal').
card_power('gold myr', 1).
card_toughness('gold myr', 1).

% Found in: JOU, ORI
card_name('gold-forged sentinel', 'Gold-Forged Sentinel').
card_type('gold-forged sentinel', 'Artifact Creature — Chimera').
card_types('gold-forged sentinel', ['Artifact', 'Creature']).
card_subtypes('gold-forged sentinel', ['Chimera']).
card_colors('gold-forged sentinel', []).
card_text('gold-forged sentinel', 'Flying').
card_mana_cost('gold-forged sentinel', ['6']).
card_cmc('gold-forged sentinel', 6).
card_layout('gold-forged sentinel', 'normal').
card_power('gold-forged sentinel', 4).
card_toughness('gold-forged sentinel', 4).

% Found in: PO2
card_name('golden bear', 'Golden Bear').
card_type('golden bear', 'Creature — Bear').
card_types('golden bear', ['Creature']).
card_subtypes('golden bear', ['Bear']).
card_colors('golden bear', ['G']).
card_text('golden bear', '').
card_mana_cost('golden bear', ['3', 'G']).
card_cmc('golden bear', 4).
card_layout('golden bear', 'normal').
card_power('golden bear', 4).
card_toughness('golden bear', 3).

% Found in: JOU
card_name('golden hind', 'Golden Hind').
card_type('golden hind', 'Creature — Elk').
card_types('golden hind', ['Creature']).
card_subtypes('golden hind', ['Elk']).
card_colors('golden hind', ['G']).
card_text('golden hind', '{T}: Add {G} to your mana pool.').
card_mana_cost('golden hind', ['1', 'G']).
card_cmc('golden hind', 2).
card_layout('golden hind', 'normal').
card_power('golden hind', 2).
card_toughness('golden hind', 1).

% Found in: SOM
card_name('golden urn', 'Golden Urn').
card_type('golden urn', 'Artifact').
card_types('golden urn', ['Artifact']).
card_subtypes('golden urn', []).
card_colors('golden urn', []).
card_text('golden urn', 'At the beginning of your upkeep, you may put a charge counter on Golden Urn.\n{T}, Sacrifice Golden Urn: You gain life equal to the number of charge counters on Golden Urn.').
card_mana_cost('golden urn', ['1']).
card_cmc('golden urn', 1).
card_layout('golden urn', 'normal').

% Found in: JUD
card_name('golden wish', 'Golden Wish').
card_type('golden wish', 'Sorcery').
card_types('golden wish', ['Sorcery']).
card_subtypes('golden wish', []).
card_colors('golden wish', ['W']).
card_text('golden wish', 'You may choose an artifact or enchantment card you own from outside the game, reveal that card, and put it into your hand. Exile Golden Wish.').
card_mana_cost('golden wish', ['3', 'W', 'W']).
card_cmc('golden wish', 5).
card_layout('golden wish', 'normal').

% Found in: M11, SHM
card_name('goldenglow moth', 'Goldenglow Moth').
card_type('goldenglow moth', 'Creature — Insect').
card_types('goldenglow moth', ['Creature']).
card_subtypes('goldenglow moth', ['Insect']).
card_colors('goldenglow moth', ['W']).
card_text('goldenglow moth', 'Flying\nWhenever Goldenglow Moth blocks, you may gain 4 life.').
card_mana_cost('goldenglow moth', ['W']).
card_cmc('goldenglow moth', 1).
card_layout('goldenglow moth', 'normal').
card_power('goldenglow moth', 0).
card_toughness('goldenglow moth', 1).

% Found in: JOU
card_name('goldenhide ox', 'Goldenhide Ox').
card_type('goldenhide ox', 'Enchantment Creature — Ox').
card_types('goldenhide ox', ['Enchantment', 'Creature']).
card_subtypes('goldenhide ox', ['Ox']).
card_colors('goldenhide ox', ['G']).
card_text('goldenhide ox', 'Constellation — Whenever Goldenhide Ox or another enchantment enters the battlefield under your control, target creature must be blocked this turn if able.').
card_mana_cost('goldenhide ox', ['5', 'G']).
card_cmc('goldenhide ox', 6).
card_layout('goldenhide ox', 'normal').
card_power('goldenhide ox', 5).
card_toughness('goldenhide ox', 4).

% Found in: HOP
card_name('goldmeadow', 'Goldmeadow').
card_type('goldmeadow', 'Plane — Lorwyn').
card_types('goldmeadow', ['Plane']).
card_subtypes('goldmeadow', ['Lorwyn']).
card_colors('goldmeadow', []).
card_text('goldmeadow', 'Whenever a land enters the battlefield, that land\'s controller puts three 0/1 white Goat creature tokens onto the battlefield.\nWhenever you roll {C}, put a 0/1 white Goat creature token onto the battlefield.').
card_layout('goldmeadow', 'plane').

% Found in: LRW
card_name('goldmeadow dodger', 'Goldmeadow Dodger').
card_type('goldmeadow dodger', 'Creature — Kithkin Rogue').
card_types('goldmeadow dodger', ['Creature']).
card_subtypes('goldmeadow dodger', ['Kithkin', 'Rogue']).
card_colors('goldmeadow dodger', ['W']).
card_text('goldmeadow dodger', 'Goldmeadow Dodger can\'t be blocked by creatures with power 4 or greater.').
card_mana_cost('goldmeadow dodger', ['W']).
card_cmc('goldmeadow dodger', 1).
card_layout('goldmeadow dodger', 'normal').
card_power('goldmeadow dodger', 1).
card_toughness('goldmeadow dodger', 1).

% Found in: DDF, LRW
card_name('goldmeadow harrier', 'Goldmeadow Harrier').
card_type('goldmeadow harrier', 'Creature — Kithkin Soldier').
card_types('goldmeadow harrier', ['Creature']).
card_subtypes('goldmeadow harrier', ['Kithkin', 'Soldier']).
card_colors('goldmeadow harrier', ['W']).
card_text('goldmeadow harrier', '{W}, {T}: Tap target creature.').
card_mana_cost('goldmeadow harrier', ['W']).
card_cmc('goldmeadow harrier', 1).
card_layout('goldmeadow harrier', 'normal').
card_power('goldmeadow harrier', 1).
card_toughness('goldmeadow harrier', 1).

% Found in: FUT
card_name('goldmeadow lookout', 'Goldmeadow Lookout').
card_type('goldmeadow lookout', 'Creature — Kithkin Spellshaper').
card_types('goldmeadow lookout', ['Creature']).
card_subtypes('goldmeadow lookout', ['Kithkin', 'Spellshaper']).
card_colors('goldmeadow lookout', ['W']).
card_text('goldmeadow lookout', '{W}, {T}, Discard a card: Put a 1/1 white Kithkin Soldier creature token named Goldmeadow Harrier onto the battlefield. It has \"{W}, {T}: Tap target creature.\"').
card_mana_cost('goldmeadow lookout', ['3', 'W']).
card_cmc('goldmeadow lookout', 4).
card_layout('goldmeadow lookout', 'normal').
card_power('goldmeadow lookout', 2).
card_toughness('goldmeadow lookout', 2).

% Found in: LRW
card_name('goldmeadow stalwart', 'Goldmeadow Stalwart').
card_type('goldmeadow stalwart', 'Creature — Kithkin Soldier').
card_types('goldmeadow stalwart', ['Creature']).
card_subtypes('goldmeadow stalwart', ['Kithkin', 'Soldier']).
card_colors('goldmeadow stalwart', ['W']).
card_text('goldmeadow stalwart', 'As an additional cost to cast Goldmeadow Stalwart, reveal a Kithkin card from your hand or pay {3}.').
card_mana_cost('goldmeadow stalwart', ['W']).
card_cmc('goldmeadow stalwart', 1).
card_layout('goldmeadow stalwart', 'normal').
card_power('goldmeadow stalwart', 2).
card_toughness('goldmeadow stalwart', 2).

% Found in: AVR
card_name('goldnight commander', 'Goldnight Commander').
card_type('goldnight commander', 'Creature — Human Cleric Soldier').
card_types('goldnight commander', ['Creature']).
card_subtypes('goldnight commander', ['Human', 'Cleric', 'Soldier']).
card_colors('goldnight commander', ['W']).
card_text('goldnight commander', 'Whenever another creature enters the battlefield under your control, creatures you control get +1/+1 until end of turn.').
card_mana_cost('goldnight commander', ['3', 'W']).
card_cmc('goldnight commander', 4).
card_layout('goldnight commander', 'normal').
card_power('goldnight commander', 2).
card_toughness('goldnight commander', 2).

% Found in: AVR
card_name('goldnight redeemer', 'Goldnight Redeemer').
card_type('goldnight redeemer', 'Creature — Angel').
card_types('goldnight redeemer', ['Creature']).
card_subtypes('goldnight redeemer', ['Angel']).
card_colors('goldnight redeemer', ['W']).
card_text('goldnight redeemer', 'Flying\nWhen Goldnight Redeemer enters the battlefield, you gain 2 life for each other creature you control.').
card_mana_cost('goldnight redeemer', ['4', 'W', 'W']).
card_cmc('goldnight redeemer', 6).
card_layout('goldnight redeemer', 'normal').
card_power('goldnight redeemer', 4).
card_toughness('goldnight redeemer', 4).

% Found in: SOM
card_name('golem artisan', 'Golem Artisan').
card_type('golem artisan', 'Artifact Creature — Golem').
card_types('golem artisan', ['Artifact', 'Creature']).
card_subtypes('golem artisan', ['Golem']).
card_colors('golem artisan', []).
card_text('golem artisan', '{2}: Target artifact creature gets +1/+1 until end of turn.\n{2}: Target artifact creature gains your choice of flying, trample, or haste until end of turn.').
card_mana_cost('golem artisan', ['5']).
card_cmc('golem artisan', 5).
card_layout('golem artisan', 'normal').
card_power('golem artisan', 3).
card_toughness('golem artisan', 3).

% Found in: SOM
card_name('golem foundry', 'Golem Foundry').
card_type('golem foundry', 'Artifact').
card_types('golem foundry', ['Artifact']).
card_subtypes('golem foundry', []).
card_colors('golem foundry', []).
card_text('golem foundry', 'Whenever you cast an artifact spell, you may put a charge counter on Golem Foundry.\nRemove three charge counters from Golem Foundry: Put a 3/3 colorless Golem artifact creature token onto the battlefield.').
card_mana_cost('golem foundry', ['3']).
card_cmc('golem foundry', 3).
card_layout('golem foundry', 'normal').

% Found in: SOM, pWPN
card_name('golem\'s heart', 'Golem\'s Heart').
card_type('golem\'s heart', 'Artifact').
card_types('golem\'s heart', ['Artifact']).
card_subtypes('golem\'s heart', []).
card_colors('golem\'s heart', []).
card_text('golem\'s heart', 'Whenever a player casts an artifact spell, you may gain 1 life.').
card_mana_cost('golem\'s heart', ['2']).
card_cmc('golem\'s heart', 2).
card_layout('golem\'s heart', 'normal').

% Found in: MRD
card_name('golem-skin gauntlets', 'Golem-Skin Gauntlets').
card_type('golem-skin gauntlets', 'Artifact — Equipment').
card_types('golem-skin gauntlets', ['Artifact']).
card_subtypes('golem-skin gauntlets', ['Equipment']).
card_colors('golem-skin gauntlets', []).
card_text('golem-skin gauntlets', 'Equipped creature gets +1/+0 for each Equipment attached to it.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery. This card enters the battlefield unattached and stays on the battlefield if the creature leaves.)').
card_mana_cost('golem-skin gauntlets', ['1']).
card_cmc('golem-skin gauntlets', 1).
card_layout('golem-skin gauntlets', 'normal').

% Found in: RAV
card_name('golgari brownscale', 'Golgari Brownscale').
card_type('golgari brownscale', 'Creature — Lizard').
card_types('golgari brownscale', ['Creature']).
card_subtypes('golgari brownscale', ['Lizard']).
card_colors('golgari brownscale', ['G']).
card_text('golgari brownscale', 'When Golgari Brownscale is put into your hand from your graveyard, you gain 2 life.\nDredge 2 (If you would draw a card, instead you may put exactly two cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('golgari brownscale', ['1', 'G', 'G']).
card_cmc('golgari brownscale', 3).
card_layout('golgari brownscale', 'normal').
card_power('golgari brownscale', 2).
card_toughness('golgari brownscale', 3).

% Found in: RTR
card_name('golgari charm', 'Golgari Charm').
card_type('golgari charm', 'Instant').
card_types('golgari charm', ['Instant']).
card_subtypes('golgari charm', []).
card_colors('golgari charm', ['B', 'G']).
card_text('golgari charm', 'Choose one —\n• All creatures get -1/-1 until end of turn.\n• Destroy target enchantment.\n• Regenerate each creature you control.').
card_mana_cost('golgari charm', ['B', 'G']).
card_cmc('golgari charm', 2).
card_layout('golgari charm', 'normal').

% Found in: DGM
card_name('golgari cluestone', 'Golgari Cluestone').
card_type('golgari cluestone', 'Artifact').
card_types('golgari cluestone', ['Artifact']).
card_subtypes('golgari cluestone', []).
card_colors('golgari cluestone', []).
card_text('golgari cluestone', '{T}: Add {B} or {G} to your mana pool.\n{B}{G}, {T}, Sacrifice Golgari Cluestone: Draw a card.').
card_mana_cost('golgari cluestone', ['3']).
card_cmc('golgari cluestone', 3).
card_layout('golgari cluestone', 'normal').

% Found in: RTR
card_name('golgari decoy', 'Golgari Decoy').
card_type('golgari decoy', 'Creature — Elf Rogue').
card_types('golgari decoy', ['Creature']).
card_subtypes('golgari decoy', ['Elf', 'Rogue']).
card_colors('golgari decoy', ['G']).
card_text('golgari decoy', 'All creatures able to block Golgari Decoy do so.\nScavenge {3}{G}{G} ({3}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('golgari decoy', ['3', 'G']).
card_cmc('golgari decoy', 4).
card_layout('golgari decoy', 'normal').
card_power('golgari decoy', 2).
card_toughness('golgari decoy', 2).

% Found in: DDJ, RAV
card_name('golgari germination', 'Golgari Germination').
card_type('golgari germination', 'Enchantment').
card_types('golgari germination', ['Enchantment']).
card_subtypes('golgari germination', []).
card_colors('golgari germination', ['B', 'G']).
card_text('golgari germination', 'Whenever a nontoken creature you control dies, put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('golgari germination', ['1', 'B', 'G']).
card_cmc('golgari germination', 3).
card_layout('golgari germination', 'normal').

% Found in: DDJ, RAV
card_name('golgari grave-troll', 'Golgari Grave-Troll').
card_type('golgari grave-troll', 'Creature — Skeleton Troll').
card_types('golgari grave-troll', ['Creature']).
card_subtypes('golgari grave-troll', ['Skeleton', 'Troll']).
card_colors('golgari grave-troll', ['G']).
card_text('golgari grave-troll', 'Golgari Grave-Troll enters the battlefield with a +1/+1 counter on it for each creature card in your graveyard.\n{1}, Remove a +1/+1 counter from Golgari Grave-Troll: Regenerate Golgari Grave-Troll.\nDredge 6 (If you would draw a card, instead you may put exactly six cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('golgari grave-troll', ['4', 'G']).
card_cmc('golgari grave-troll', 5).
card_layout('golgari grave-troll', 'normal').
card_power('golgari grave-troll', 0).
card_toughness('golgari grave-troll', 0).

% Found in: C13, DDM, DGM, RTR
card_name('golgari guildgate', 'Golgari Guildgate').
card_type('golgari guildgate', 'Land — Gate').
card_types('golgari guildgate', ['Land']).
card_subtypes('golgari guildgate', ['Gate']).
card_colors('golgari guildgate', []).
card_text('golgari guildgate', 'Golgari Guildgate enters the battlefield tapped.\n{T}: Add {B} or {G} to your mana pool.').
card_layout('golgari guildgate', 'normal').

% Found in: C13, CMD, RAV
card_name('golgari guildmage', 'Golgari Guildmage').
card_type('golgari guildmage', 'Creature — Elf Shaman').
card_types('golgari guildmage', ['Creature']).
card_subtypes('golgari guildmage', ['Elf', 'Shaman']).
card_colors('golgari guildmage', ['B', 'G']).
card_text('golgari guildmage', '{4}{B}, Sacrifice a creature: Return target creature card from your graveyard to your hand.\n{4}{G}: Put a +1/+1 counter on target creature.').
card_mana_cost('golgari guildmage', ['B/G', 'B/G']).
card_cmc('golgari guildmage', 2).
card_layout('golgari guildmage', 'normal').
card_power('golgari guildmage', 2).
card_toughness('golgari guildmage', 2).

% Found in: RTR
card_name('golgari keyrune', 'Golgari Keyrune').
card_type('golgari keyrune', 'Artifact').
card_types('golgari keyrune', ['Artifact']).
card_subtypes('golgari keyrune', []).
card_colors('golgari keyrune', []).
card_text('golgari keyrune', '{T}: Add {B} or {G} to your mana pool.\n{B}{G}: Golgari Keyrune becomes a 2/2 black and green Insect artifact creature with deathtouch until end of turn.').
card_mana_cost('golgari keyrune', ['3']).
card_cmc('golgari keyrune', 3).
card_layout('golgari keyrune', 'normal').

% Found in: RTR
card_name('golgari longlegs', 'Golgari Longlegs').
card_type('golgari longlegs', 'Creature — Insect').
card_types('golgari longlegs', ['Creature']).
card_subtypes('golgari longlegs', ['Insect']).
card_colors('golgari longlegs', ['B', 'G']).
card_text('golgari longlegs', '').
card_mana_cost('golgari longlegs', ['3', 'B/G', 'B/G']).
card_cmc('golgari longlegs', 5).
card_layout('golgari longlegs', 'normal').
card_power('golgari longlegs', 5).
card_toughness('golgari longlegs', 4).

% Found in: C13, CMD, DDJ, MM2, RAV
card_name('golgari rot farm', 'Golgari Rot Farm').
card_type('golgari rot farm', 'Land').
card_types('golgari rot farm', ['Land']).
card_subtypes('golgari rot farm', []).
card_colors('golgari rot farm', []).
card_text('golgari rot farm', 'Golgari Rot Farm enters the battlefield tapped.\nWhen Golgari Rot Farm enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {B}{G} to your mana pool.').
card_layout('golgari rot farm', 'normal').

% Found in: DDJ, RAV
card_name('golgari rotwurm', 'Golgari Rotwurm').
card_type('golgari rotwurm', 'Creature — Zombie Wurm').
card_types('golgari rotwurm', ['Creature']).
card_subtypes('golgari rotwurm', ['Zombie', 'Wurm']).
card_colors('golgari rotwurm', ['B', 'G']).
card_text('golgari rotwurm', '{B}, Sacrifice a creature: Target player loses 1 life.').
card_mana_cost('golgari rotwurm', ['3', 'B', 'G']).
card_cmc('golgari rotwurm', 5).
card_layout('golgari rotwurm', 'normal').
card_power('golgari rotwurm', 5).
card_toughness('golgari rotwurm', 4).

% Found in: CMD, DDJ, RAV
card_name('golgari signet', 'Golgari Signet').
card_type('golgari signet', 'Artifact').
card_types('golgari signet', ['Artifact']).
card_subtypes('golgari signet', []).
card_colors('golgari signet', []).
card_text('golgari signet', '{1}, {T}: Add {B}{G} to your mana pool.').
card_mana_cost('golgari signet', ['2']).
card_cmc('golgari signet', 2).
card_layout('golgari signet', 'normal').

% Found in: DDJ, RAV
card_name('golgari thug', 'Golgari Thug').
card_type('golgari thug', 'Creature — Human Warrior').
card_types('golgari thug', ['Creature']).
card_subtypes('golgari thug', ['Human', 'Warrior']).
card_colors('golgari thug', ['B']).
card_text('golgari thug', 'When Golgari Thug dies, put target creature card from your graveyard on top of your library.\nDredge 4 (If you would draw a card, instead you may put exactly four cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('golgari thug', ['1', 'B']).
card_cmc('golgari thug', 2).
card_layout('golgari thug', 'normal').
card_power('golgari thug', 1).
card_toughness('golgari thug', 1).

% Found in: ATQ
card_name('golgothian sylex', 'Golgothian Sylex').
card_type('golgothian sylex', 'Artifact').
card_types('golgothian sylex', ['Artifact']).
card_subtypes('golgothian sylex', []).
card_colors('golgothian sylex', []).
card_text('golgothian sylex', '{1}, {T}: Each nontoken permanent originally printed in the Antiquities expansion is sacrificed by its controller.').
card_mana_cost('golgothian sylex', ['4']).
card_cmc('golgothian sylex', 4).
card_layout('golgothian sylex', 'normal').
card_reserved('golgothian sylex').

% Found in: UDS
card_name('goliath beetle', 'Goliath Beetle').
card_type('goliath beetle', 'Creature — Insect').
card_types('goliath beetle', ['Creature']).
card_subtypes('goliath beetle', ['Insect']).
card_colors('goliath beetle', ['G']).
card_text('goliath beetle', 'Trample').
card_mana_cost('goliath beetle', ['2', 'G']).
card_cmc('goliath beetle', 3).
card_layout('goliath beetle', 'normal').
card_power('goliath beetle', 3).
card_toughness('goliath beetle', 1).

% Found in: WWK
card_name('goliath sphinx', 'Goliath Sphinx').
card_type('goliath sphinx', 'Creature — Sphinx').
card_types('goliath sphinx', ['Creature']).
card_subtypes('goliath sphinx', ['Sphinx']).
card_colors('goliath sphinx', ['U']).
card_text('goliath sphinx', 'Flying').
card_mana_cost('goliath sphinx', ['5', 'U', 'U']).
card_cmc('goliath sphinx', 7).
card_layout('goliath sphinx', 'normal').
card_power('goliath sphinx', 8).
card_toughness('goliath sphinx', 7).

% Found in: RAV
card_name('goliath spider', 'Goliath Spider').
card_type('goliath spider', 'Creature — Spider').
card_types('goliath spider', ['Creature']).
card_subtypes('goliath spider', ['Spider']).
card_colors('goliath spider', ['G']).
card_text('goliath spider', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('goliath spider', ['6', 'G', 'G']).
card_cmc('goliath spider', 8).
card_layout('goliath spider', 'normal').
card_power('goliath spider', 7).
card_toughness('goliath spider', 6).

% Found in: CMD, ZEN
card_name('gomazoa', 'Gomazoa').
card_type('gomazoa', 'Creature — Jellyfish').
card_types('gomazoa', ['Creature']).
card_subtypes('gomazoa', ['Jellyfish']).
card_colors('gomazoa', ['U']).
card_text('gomazoa', 'Defender, flying\n{T}: Put Gomazoa and each creature it\'s blocking on top of their owners\' libraries, then those players shuffle their libraries.').
card_mana_cost('gomazoa', ['2', 'U']).
card_cmc('gomazoa', 3).
card_layout('gomazoa', 'normal').
card_power('gomazoa', 0).
card_toughness('gomazoa', 3).

% Found in: PLC
card_name('gone', 'Gone').
card_type('gone', 'Instant').
card_types('gone', ['Instant']).
card_subtypes('gone', []).
card_colors('gone', ['R']).
card_text('gone', 'Return target creature you don\'t control to its owner\'s hand.').
card_mana_cost('gone', ['2', 'R']).
card_cmc('gone', 3).
card_layout('gone', 'split').

% Found in: FRF
card_name('gore swine', 'Gore Swine').
card_type('gore swine', 'Creature — Boar').
card_types('gore swine', ['Creature']).
card_subtypes('gore swine', ['Boar']).
card_colors('gore swine', ['R']).
card_text('gore swine', '').
card_mana_cost('gore swine', ['2', 'R']).
card_cmc('gore swine', 3).
card_layout('gore swine', 'normal').
card_power('gore swine', 4).
card_toughness('gore swine', 1).

% Found in: MBS
card_name('gore vassal', 'Gore Vassal').
card_type('gore vassal', 'Creature — Hound').
card_types('gore vassal', ['Creature']).
card_subtypes('gore vassal', ['Hound']).
card_colors('gore vassal', ['W']).
card_text('gore vassal', 'Sacrifice Gore Vassal: Put a -1/-1 counter on target creature. Then if that creature\'s toughness is 1 or greater, regenerate it.').
card_mana_cost('gore vassal', ['2', 'W']).
card_cmc('gore vassal', 3).
card_layout('gore vassal', 'normal').
card_power('gore vassal', 2).
card_toughness('gore vassal', 1).

% Found in: RTR
card_name('gore-house chainwalker', 'Gore-House Chainwalker').
card_type('gore-house chainwalker', 'Creature — Human Warrior').
card_types('gore-house chainwalker', ['Creature']).
card_subtypes('gore-house chainwalker', ['Human', 'Warrior']).
card_colors('gore-house chainwalker', ['R']).
card_text('gore-house chainwalker', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_mana_cost('gore-house chainwalker', ['1', 'R']).
card_cmc('gore-house chainwalker', 2).
card_layout('gore-house chainwalker', 'normal').
card_power('gore-house chainwalker', 2).
card_toughness('gore-house chainwalker', 1).

% Found in: DDL, M12, MM2
card_name('gorehorn minotaurs', 'Gorehorn Minotaurs').
card_type('gorehorn minotaurs', 'Creature — Minotaur Warrior').
card_types('gorehorn minotaurs', ['Creature']).
card_subtypes('gorehorn minotaurs', ['Minotaur', 'Warrior']).
card_colors('gorehorn minotaurs', ['R']).
card_text('gorehorn minotaurs', 'Bloodthirst 2 (If an opponent was dealt damage this turn, this creature enters the battlefield with two +1/+1 counters on it.)').
card_mana_cost('gorehorn minotaurs', ['2', 'R', 'R']).
card_cmc('gorehorn minotaurs', 4).
card_layout('gorehorn minotaurs', 'normal').
card_power('gorehorn minotaurs', 3).
card_toughness('gorehorn minotaurs', 3).

% Found in: JUD
card_name('goretusk firebeast', 'Goretusk Firebeast').
card_type('goretusk firebeast', 'Creature — Elemental Boar Beast').
card_types('goretusk firebeast', ['Creature']).
card_subtypes('goretusk firebeast', ['Elemental', 'Boar', 'Beast']).
card_colors('goretusk firebeast', ['R']).
card_text('goretusk firebeast', 'When Goretusk Firebeast enters the battlefield, it deals 4 damage to target player.').
card_mana_cost('goretusk firebeast', ['5', 'R']).
card_cmc('goretusk firebeast', 6).
card_layout('goretusk firebeast', 'normal').
card_power('goretusk firebeast', 2).
card_toughness('goretusk firebeast', 2).

% Found in: ARB
card_name('gorger wurm', 'Gorger Wurm').
card_type('gorger wurm', 'Creature — Wurm').
card_types('gorger wurm', ['Creature']).
card_subtypes('gorger wurm', ['Wurm']).
card_colors('gorger wurm', ['R', 'G']).
card_text('gorger wurm', 'Devour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)').
card_mana_cost('gorger wurm', ['3', 'R', 'G']).
card_cmc('gorger wurm', 5).
card_layout('gorger wurm', 'normal').
card_power('gorger wurm', 5).
card_toughness('gorger wurm', 5).

% Found in: M10
card_name('gorgon flail', 'Gorgon Flail').
card_type('gorgon flail', 'Artifact — Equipment').
card_types('gorgon flail', ['Artifact']).
card_subtypes('gorgon flail', ['Equipment']).
card_colors('gorgon flail', []).
card_text('gorgon flail', 'Equipped creature gets +1/+1 and has deathtouch. (Any amount of damage it deals to a creature is enough to destroy that creature.)\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('gorgon flail', ['2']).
card_cmc('gorgon flail', 2).
card_layout('gorgon flail', 'normal').

% Found in: TSP
card_name('gorgon recluse', 'Gorgon Recluse').
card_type('gorgon recluse', 'Creature — Gorgon').
card_types('gorgon recluse', ['Creature']).
card_subtypes('gorgon recluse', ['Gorgon']).
card_colors('gorgon recluse', ['B']).
card_text('gorgon recluse', 'Whenever Gorgon Recluse blocks or becomes blocked by a nonblack creature, destroy that creature at end of combat.\nMadness {B}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('gorgon recluse', ['3', 'B', 'B']).
card_cmc('gorgon recluse', 5).
card_layout('gorgon recluse', 'normal').
card_power('gorgon recluse', 2).
card_toughness('gorgon recluse', 4).

% Found in: BNG
card_name('gorgon\'s head', 'Gorgon\'s Head').
card_type('gorgon\'s head', 'Artifact — Equipment').
card_types('gorgon\'s head', ['Artifact']).
card_subtypes('gorgon\'s head', ['Equipment']).
card_colors('gorgon\'s head', []).
card_text('gorgon\'s head', 'Equipped creature has deathtouch.\nEquip {2}').
card_mana_cost('gorgon\'s head', ['1']).
card_cmc('gorgon\'s head', 1).
card_layout('gorgon\'s head', 'normal').

% Found in: ALL
card_name('gorilla berserkers', 'Gorilla Berserkers').
card_type('gorilla berserkers', 'Creature — Ape Berserker').
card_types('gorilla berserkers', ['Creature']).
card_subtypes('gorilla berserkers', ['Ape', 'Berserker']).
card_colors('gorilla berserkers', ['G']).
card_text('gorilla berserkers', 'Trample; rampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)\nGorilla Berserkers can\'t be blocked except by three or more creatures.').
card_mana_cost('gorilla berserkers', ['3', 'G', 'G']).
card_cmc('gorilla berserkers', 5).
card_layout('gorilla berserkers', 'normal').
card_power('gorilla berserkers', 2).
card_toughness('gorilla berserkers', 3).

% Found in: 6ED, 7ED, ALL, ATH
card_name('gorilla chieftain', 'Gorilla Chieftain').
card_type('gorilla chieftain', 'Creature — Ape').
card_types('gorilla chieftain', ['Creature']).
card_subtypes('gorilla chieftain', ['Ape']).
card_colors('gorilla chieftain', ['G']).
card_text('gorilla chieftain', '{1}{G}: Regenerate Gorilla Chieftain.').
card_mana_cost('gorilla chieftain', ['2', 'G', 'G']).
card_cmc('gorilla chieftain', 4).
card_layout('gorilla chieftain', 'normal').
card_power('gorilla chieftain', 3).
card_toughness('gorilla chieftain', 3).

% Found in: ICE
card_name('gorilla pack', 'Gorilla Pack').
card_type('gorilla pack', 'Creature — Ape').
card_types('gorilla pack', ['Creature']).
card_subtypes('gorilla pack', ['Ape']).
card_colors('gorilla pack', ['G']).
card_text('gorilla pack', 'Gorilla Pack can\'t attack unless defending player controls a Forest.\nWhen you control no Forests, sacrifice Gorilla Pack.').
card_mana_cost('gorilla pack', ['2', 'G']).
card_cmc('gorilla pack', 3).
card_layout('gorilla pack', 'normal').
card_power('gorilla pack', 3).
card_toughness('gorilla pack', 3).

% Found in: ALL, CST, ME2
card_name('gorilla shaman', 'Gorilla Shaman').
card_type('gorilla shaman', 'Creature — Ape Shaman').
card_types('gorilla shaman', ['Creature']).
card_subtypes('gorilla shaman', ['Ape', 'Shaman']).
card_colors('gorilla shaman', ['R']).
card_text('gorilla shaman', '{X}{X}{1}: Destroy target noncreature artifact with converted mana cost X.').
card_mana_cost('gorilla shaman', ['R']).
card_cmc('gorilla shaman', 1).
card_layout('gorilla shaman', 'normal').
card_power('gorilla shaman', 1).
card_toughness('gorilla shaman', 1).

% Found in: ODY
card_name('gorilla titan', 'Gorilla Titan').
card_type('gorilla titan', 'Creature — Ape').
card_types('gorilla titan', ['Creature']).
card_subtypes('gorilla titan', ['Ape']).
card_colors('gorilla titan', ['G']).
card_text('gorilla titan', 'Trample\nGorilla Titan gets +4/+4 as long as there are no cards in your graveyard.').
card_mana_cost('gorilla titan', ['3', 'G', 'G']).
card_cmc('gorilla titan', 5).
card_layout('gorilla titan', 'normal').
card_power('gorilla titan', 4).
card_toughness('gorilla titan', 4).

% Found in: ALL, ME4
card_name('gorilla war cry', 'Gorilla War Cry').
card_type('gorilla war cry', 'Instant').
card_types('gorilla war cry', ['Instant']).
card_subtypes('gorilla war cry', []).
card_colors('gorilla war cry', ['R']).
card_text('gorilla war cry', 'Cast Gorilla War Cry only during combat before blockers are declared.\nAll creatures gain menace until end of turn. (They can\'t be blocked except by two or more creatures.)\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('gorilla war cry', ['1', 'R']).
card_cmc('gorilla war cry', 2).
card_layout('gorilla war cry', 'normal').

% Found in: BRB, POR, S99, USG
card_name('gorilla warrior', 'Gorilla Warrior').
card_type('gorilla warrior', 'Creature — Ape Warrior').
card_types('gorilla warrior', ['Creature']).
card_subtypes('gorilla warrior', ['Ape', 'Warrior']).
card_colors('gorilla warrior', ['G']).
card_text('gorilla warrior', '').
card_mana_cost('gorilla warrior', ['2', 'G']).
card_cmc('gorilla warrior', 3).
card_layout('gorilla warrior', 'normal').
card_power('gorilla warrior', 3).
card_toughness('gorilla warrior', 2).

% Found in: BOK
card_name('goryo\'s vengeance', 'Goryo\'s Vengeance').
card_type('goryo\'s vengeance', 'Instant — Arcane').
card_types('goryo\'s vengeance', ['Instant']).
card_subtypes('goryo\'s vengeance', ['Arcane']).
card_colors('goryo\'s vengeance', ['B']).
card_text('goryo\'s vengeance', 'Return target legendary creature card from your graveyard to the battlefield. That creature gains haste. Exile it at the beginning of the next end step.\nSplice onto Arcane {2}{B} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('goryo\'s vengeance', ['1', 'B']).
card_cmc('goryo\'s vengeance', 2).
card_layout('goryo\'s vengeance', 'normal').

% Found in: VIS
card_name('gossamer chains', 'Gossamer Chains').
card_type('gossamer chains', 'Enchantment').
card_types('gossamer chains', ['Enchantment']).
card_subtypes('gossamer chains', []).
card_colors('gossamer chains', ['W']).
card_text('gossamer chains', 'Return Gossamer Chains to its owner\'s hand: Prevent all combat damage that would be dealt by target unblocked creature this turn.').
card_mana_cost('gossamer chains', ['W', 'W']).
card_cmc('gossamer chains', 2).
card_layout('gossamer chains', 'normal').

% Found in: PLC
card_name('gossamer phantasm', 'Gossamer Phantasm').
card_type('gossamer phantasm', 'Creature — Illusion').
card_types('gossamer phantasm', ['Creature']).
card_subtypes('gossamer phantasm', ['Illusion']).
card_colors('gossamer phantasm', ['U']).
card_text('gossamer phantasm', 'Flying\nWhen Gossamer Phantasm becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('gossamer phantasm', ['1', 'U']).
card_cmc('gossamer phantasm', 2).
card_layout('gossamer phantasm', 'normal').
card_power('gossamer phantasm', 2).
card_toughness('gossamer phantasm', 1).

% Found in: LEG
card_name('gosta dirk', 'Gosta Dirk').
card_type('gosta dirk', 'Legendary Creature — Human Warrior').
card_types('gosta dirk', ['Creature']).
card_subtypes('gosta dirk', ['Human', 'Warrior']).
card_supertypes('gosta dirk', ['Legendary']).
card_colors('gosta dirk', ['W', 'U']).
card_text('gosta dirk', 'First strike\nCreatures with islandwalk can be blocked as though they didn\'t have islandwalk.').
card_mana_cost('gosta dirk', ['3', 'W', 'W', 'U', 'U']).
card_cmc('gosta dirk', 7).
card_layout('gosta dirk', 'normal').
card_power('gosta dirk', 4).
card_toughness('gosta dirk', 4).
card_reserved('gosta dirk').

% Found in: DIS
card_name('govern the guildless', 'Govern the Guildless').
card_type('govern the guildless', 'Sorcery').
card_types('govern the guildless', ['Sorcery']).
card_subtypes('govern the guildless', []).
card_colors('govern the guildless', ['U']).
card_text('govern the guildless', 'Gain control of target monocolored creature.\nForecast — {1}{U}, Reveal Govern the Guildless from your hand: Target creature becomes the color or colors of your choice until end of turn. (Activate this ability only during your upkeep and only once each turn.)').
card_mana_cost('govern the guildless', ['5', 'U']).
card_cmc('govern the guildless', 6).
card_layout('govern the guildless', 'normal').

% Found in: MRD
card_name('grab the reins', 'Grab the Reins').
card_type('grab the reins', 'Instant').
card_types('grab the reins', ['Instant']).
card_subtypes('grab the reins', []).
card_colors('grab the reins', ['R']).
card_text('grab the reins', 'Choose one —\n• Until end of turn, you gain control of target creature and it gains haste.\n• Sacrifice a creature. Grab the Reins deals damage equal to that creature\'s power to target creature or player.\nEntwine {2}{R} (Choose both if you pay the entwine cost.)').
card_mana_cost('grab the reins', ['3', 'R']).
card_cmc('grab the reins', 4).
card_layout('grab the reins', 'normal').

% Found in: DTK
card_name('graceblade artisan', 'Graceblade Artisan').
card_type('graceblade artisan', 'Creature — Human Monk').
card_types('graceblade artisan', ['Creature']).
card_subtypes('graceblade artisan', ['Human', 'Monk']).
card_colors('graceblade artisan', ['W']).
card_text('graceblade artisan', 'Graceblade Artisan gets +2/+2 for each Aura attached to it.').
card_mana_cost('graceblade artisan', ['2', 'W']).
card_cmc('graceblade artisan', 3).
card_layout('graceblade artisan', 'normal').
card_power('graceblade artisan', 2).
card_toughness('graceblade artisan', 3).

% Found in: CHK
card_name('graceful adept', 'Graceful Adept').
card_type('graceful adept', 'Creature — Human Wizard').
card_types('graceful adept', ['Creature']).
card_subtypes('graceful adept', ['Human', 'Wizard']).
card_colors('graceful adept', ['U']).
card_text('graceful adept', 'You have no maximum hand size.').
card_mana_cost('graceful adept', ['2', 'U']).
card_cmc('graceful adept', 3).
card_layout('graceful adept', 'normal').
card_power('graceful adept', 1).
card_toughness('graceful adept', 3).

% Found in: ODY
card_name('graceful antelope', 'Graceful Antelope').
card_type('graceful antelope', 'Creature — Antelope').
card_types('graceful antelope', ['Creature']).
card_subtypes('graceful antelope', ['Antelope']).
card_colors('graceful antelope', ['W']).
card_text('graceful antelope', 'Plainswalk (This creature can\'t be blocked as long as defending player controls a Plains.)\nWhenever Graceful Antelope deals combat damage to a player, you may have target land become a Plains until Graceful Antelope leaves the battlefield.').
card_mana_cost('graceful antelope', ['2', 'W', 'W']).
card_cmc('graceful antelope', 4).
card_layout('graceful antelope', 'normal').
card_power('graceful antelope', 1).
card_toughness('graceful antelope', 4).

% Found in: MOR
card_name('graceful reprieve', 'Graceful Reprieve').
card_type('graceful reprieve', 'Instant').
card_types('graceful reprieve', ['Instant']).
card_subtypes('graceful reprieve', []).
card_colors('graceful reprieve', ['W']).
card_text('graceful reprieve', 'When target creature dies this turn, return that card to the battlefield under its owner\'s control.').
card_mana_cost('graceful reprieve', ['1', 'W']).
card_cmc('graceful reprieve', 2).
card_layout('graceful reprieve', 'normal').

% Found in: DKA
card_name('grafdigger\'s cage', 'Grafdigger\'s Cage').
card_type('grafdigger\'s cage', 'Artifact').
card_types('grafdigger\'s cage', ['Artifact']).
card_subtypes('grafdigger\'s cage', []).
card_colors('grafdigger\'s cage', []).
card_text('grafdigger\'s cage', 'Creature cards can\'t enter the battlefield from graveyards or libraries.\nPlayers can\'t cast cards in graveyards or libraries.').
card_mana_cost('grafdigger\'s cage', ['1']).
card_cmc('grafdigger\'s cage', 1).
card_layout('grafdigger\'s cage', 'normal').

% Found in: SOM
card_name('grafted exoskeleton', 'Grafted Exoskeleton').
card_type('grafted exoskeleton', 'Artifact — Equipment').
card_types('grafted exoskeleton', ['Artifact']).
card_subtypes('grafted exoskeleton', ['Equipment']).
card_colors('grafted exoskeleton', []).
card_text('grafted exoskeleton', 'Equipped creature gets +2/+2 and has infect. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhenever Grafted Exoskeleton becomes unattached from a permanent, sacrifice that permanent.\nEquip {2}').
card_mana_cost('grafted exoskeleton', ['4']).
card_cmc('grafted exoskeleton', 4).
card_layout('grafted exoskeleton', 'normal').

% Found in: 7ED, USG
card_name('grafted skullcap', 'Grafted Skullcap').
card_type('grafted skullcap', 'Artifact').
card_types('grafted skullcap', ['Artifact']).
card_subtypes('grafted skullcap', []).
card_colors('grafted skullcap', []).
card_text('grafted skullcap', 'At the beginning of your draw step, draw an additional card.\nAt the beginning of your end step, discard your hand.').
card_mana_cost('grafted skullcap', ['4']).
card_cmc('grafted skullcap', 4).
card_layout('grafted skullcap', 'normal').

% Found in: 5DN
card_name('grafted wargear', 'Grafted Wargear').
card_type('grafted wargear', 'Artifact — Equipment').
card_types('grafted wargear', ['Artifact']).
card_subtypes('grafted wargear', ['Equipment']).
card_colors('grafted wargear', []).
card_text('grafted wargear', 'Equipped creature gets +3/+2.\nWhenever Grafted Wargear becomes unattached from a permanent, sacrifice that permanent.\nEquip {0} ({0}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('grafted wargear', ['3']).
card_cmc('grafted wargear', 3).
card_layout('grafted wargear', 'normal').

% Found in: C14, M12
card_name('grand abolisher', 'Grand Abolisher').
card_type('grand abolisher', 'Creature — Human Cleric').
card_types('grand abolisher', ['Creature']).
card_subtypes('grand abolisher', ['Human', 'Cleric']).
card_colors('grand abolisher', ['W']).
card_text('grand abolisher', 'During your turn, your opponents can\'t cast spells or activate abilities of artifacts, creatures, or enchantments.').
card_mana_cost('grand abolisher', ['W', 'W']).
card_cmc('grand abolisher', 2).
card_layout('grand abolisher', 'normal').
card_power('grand abolisher', 2).
card_toughness('grand abolisher', 2).

% Found in: DIS, MMA
card_name('grand arbiter augustin iv', 'Grand Arbiter Augustin IV').
card_type('grand arbiter augustin iv', 'Legendary Creature — Human Advisor').
card_types('grand arbiter augustin iv', ['Creature']).
card_subtypes('grand arbiter augustin iv', ['Human', 'Advisor']).
card_supertypes('grand arbiter augustin iv', ['Legendary']).
card_colors('grand arbiter augustin iv', ['W', 'U']).
card_text('grand arbiter augustin iv', 'White spells you cast cost {1} less to cast.\nBlue spells you cast cost {1} less to cast.\nSpells your opponents cast cost {1} more to cast.').
card_mana_cost('grand arbiter augustin iv', ['2', 'W', 'U']).
card_cmc('grand arbiter augustin iv', 4).
card_layout('grand arbiter augustin iv', 'normal').
card_power('grand arbiter augustin iv', 2).
card_toughness('grand arbiter augustin iv', 3).

% Found in: SOM
card_name('grand architect', 'Grand Architect').
card_type('grand architect', 'Creature — Vedalken Artificer').
card_types('grand architect', ['Creature']).
card_subtypes('grand architect', ['Vedalken', 'Artificer']).
card_colors('grand architect', ['U']).
card_text('grand architect', 'Other blue creatures you control get +1/+1.\n{U}: Target artifact creature becomes blue until end of turn.\nTap an untapped blue creature you control: Add {2} to your mana pool. Spend this mana only to cast artifact spells or activate abilities of artifacts.').
card_mana_cost('grand architect', ['1', 'U', 'U']).
card_cmc('grand architect', 3).
card_layout('grand architect', 'normal').
card_power('grand architect', 1).
card_toughness('grand architect', 3).

% Found in: ONS, VMA
card_name('grand coliseum', 'Grand Coliseum').
card_type('grand coliseum', 'Land').
card_types('grand coliseum', ['Land']).
card_subtypes('grand coliseum', []).
card_colors('grand coliseum', []).
card_text('grand coliseum', 'Grand Coliseum enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Grand Coliseum deals 1 damage to you.').
card_layout('grand coliseum', 'normal').

% Found in: ONS
card_name('grand melee', 'Grand Melee').
card_type('grand melee', 'Enchantment').
card_types('grand melee', ['Enchantment']).
card_subtypes('grand melee', []).
card_colors('grand melee', ['R']).
card_text('grand melee', 'All creatures attack each turn if able.\nAll creatures block each turn if able.').
card_mana_cost('grand melee', ['3', 'R']).
card_cmc('grand melee', 4).
card_layout('grand melee', 'normal').

% Found in: PC2
card_name('grand ossuary', 'Grand Ossuary').
card_type('grand ossuary', 'Plane — Ravnica').
card_types('grand ossuary', ['Plane']).
card_subtypes('grand ossuary', ['Ravnica']).
card_colors('grand ossuary', []).
card_text('grand ossuary', 'Whenever a creature dies, its controller distributes a number of +1/+1 counters equal to its power among any number of target creatures he or she controls.\nWhenever you roll {C}, each player exiles all creatures he or she controls and puts X 1/1 green Saproling creature tokens onto the battlefield, where X is the total power of the creatures he or she exiled this way. Then planeswalk.').
card_layout('grand ossuary', 'plane').

% Found in: HML, ME2
card_name('grandmother sengir', 'Grandmother Sengir').
card_type('grandmother sengir', 'Legendary Creature — Human Wizard').
card_types('grandmother sengir', ['Creature']).
card_subtypes('grandmother sengir', ['Human', 'Wizard']).
card_supertypes('grandmother sengir', ['Legendary']).
card_colors('grandmother sengir', ['B']).
card_text('grandmother sengir', '{1}{B}, {T}: Target creature gets -1/-1 until end of turn.').
card_mana_cost('grandmother sengir', ['4', 'B']).
card_cmc('grandmother sengir', 5).
card_layout('grandmother sengir', 'normal').
card_power('grandmother sengir', 3).
card_toughness('grandmother sengir', 3).
card_reserved('grandmother sengir').

% Found in: MIR
card_name('granger guildmage', 'Granger Guildmage').
card_type('granger guildmage', 'Creature — Human Wizard').
card_types('granger guildmage', ['Creature']).
card_subtypes('granger guildmage', ['Human', 'Wizard']).
card_colors('granger guildmage', ['G']).
card_text('granger guildmage', '{R}, {T}: Granger Guildmage deals 1 damage to target creature or player and 1 damage to you.\n{W}, {T}: Target creature gains first strike until end of turn.').
card_mana_cost('granger guildmage', ['G']).
card_cmc('granger guildmage', 1).
card_layout('granger guildmage', 'normal').
card_power('granger guildmage', 1).
card_toughness('granger guildmage', 1).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, MED
card_name('granite gargoyle', 'Granite Gargoyle').
card_type('granite gargoyle', 'Creature — Gargoyle').
card_types('granite gargoyle', ['Creature']).
card_subtypes('granite gargoyle', ['Gargoyle']).
card_colors('granite gargoyle', ['R']).
card_text('granite gargoyle', 'Flying\n{R}: Granite Gargoyle gets +0/+1 until end of turn.').
card_mana_cost('granite gargoyle', ['2', 'R']).
card_cmc('granite gargoyle', 3).
card_layout('granite gargoyle', 'normal').
card_power('granite gargoyle', 2).
card_toughness('granite gargoyle', 2).
card_reserved('granite gargoyle').

% Found in: 7ED, ULG
card_name('granite grip', 'Granite Grip').
card_type('granite grip', 'Enchantment — Aura').
card_types('granite grip', ['Enchantment']).
card_subtypes('granite grip', ['Aura']).
card_colors('granite grip', ['R']).
card_text('granite grip', 'Enchant creature\nEnchanted creature gets +1/+0 for each Mountain you control.').
card_mana_cost('granite grip', ['2', 'R']).
card_cmc('granite grip', 3).
card_layout('granite grip', 'normal').

% Found in: MRD
card_name('granite shard', 'Granite Shard').
card_type('granite shard', 'Artifact').
card_types('granite shard', ['Artifact']).
card_subtypes('granite shard', []).
card_colors('granite shard', []).
card_text('granite shard', '{3}, {T} or {R}, {T}: Granite Shard deals 1 damage to target creature or player.').
card_mana_cost('granite shard', ['3']).
card_cmc('granite shard', 3).
card_layout('granite shard', 'normal').

% Found in: UNH, pARL
card_name('granny\'s payback', 'Granny\'s Payback').
card_type('granny\'s payback', 'Sorcery').
card_types('granny\'s payback', ['Sorcery']).
card_subtypes('granny\'s payback', []).
card_colors('granny\'s payback', ['G']).
card_text('granny\'s payback', 'You gain life equal to your age.').
card_mana_cost('granny\'s payback', ['7', 'G']).
card_cmc('granny\'s payback', 8).
card_layout('granny\'s payback', 'normal').

% Found in: 5DN
card_name('granulate', 'Granulate').
card_type('granulate', 'Sorcery').
card_types('granulate', ['Sorcery']).
card_subtypes('granulate', []).
card_colors('granulate', ['R']).
card_text('granulate', 'Destroy each nonland artifact with converted mana cost 4 or less.').
card_mana_cost('granulate', ['2', 'R', 'R']).
card_cmc('granulate', 4).
card_layout('granulate', 'normal').

% Found in: MMA, TSP
card_name('grapeshot', 'Grapeshot').
card_type('grapeshot', 'Sorcery').
card_types('grapeshot', ['Sorcery']).
card_subtypes('grapeshot', []).
card_colors('grapeshot', ['R']).
card_text('grapeshot', 'Grapeshot deals 1 damage to target creature or player.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('grapeshot', ['1', 'R']).
card_cmc('grapeshot', 2).
card_layout('grapeshot', 'normal').

% Found in: 4ED, 5ED, 7ED, ATQ, ME4
card_name('grapeshot catapult', 'Grapeshot Catapult').
card_type('grapeshot catapult', 'Artifact Creature — Construct').
card_types('grapeshot catapult', ['Artifact', 'Creature']).
card_subtypes('grapeshot catapult', ['Construct']).
card_colors('grapeshot catapult', []).
card_text('grapeshot catapult', '{T}: Grapeshot Catapult deals 1 damage to target creature with flying.').
card_mana_cost('grapeshot catapult', ['4']).
card_cmc('grapeshot catapult', 4).
card_layout('grapeshot catapult', 'normal').
card_power('grapeshot catapult', 2).
card_toughness('grapeshot catapult', 3).

% Found in: UNH
card_name('graphic violence', 'Graphic Violence').
card_type('graphic violence', 'Instant').
card_types('graphic violence', ['Instant']).
card_subtypes('graphic violence', []).
card_colors('graphic violence', ['G']).
card_text('graphic violence', 'All creatures by the artist of your choice get +2/+2 and gain trample until end of turn.').
card_mana_cost('graphic violence', ['2', 'G']).
card_cmc('graphic violence', 3).
card_layout('graphic violence', 'normal').

% Found in: WWK
card_name('grappler spider', 'Grappler Spider').
card_type('grappler spider', 'Creature — Spider').
card_types('grappler spider', ['Creature']).
card_subtypes('grappler spider', ['Spider']).
card_colors('grappler spider', ['G']).
card_text('grappler spider', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('grappler spider', ['1', 'G']).
card_cmc('grappler spider', 2).
card_layout('grappler spider', 'normal').
card_power('grappler spider', 2).
card_toughness('grappler spider', 1).

% Found in: ZEN
card_name('grappling hook', 'Grappling Hook').
card_type('grappling hook', 'Artifact — Equipment').
card_types('grappling hook', ['Artifact']).
card_subtypes('grappling hook', ['Equipment']).
card_colors('grappling hook', []).
card_text('grappling hook', 'Equipped creature has double strike.\nWhenever equipped creature attacks, you may have target creature block it this turn if able.\nEquip {4}').
card_mana_cost('grappling hook', ['4']).
card_cmc('grappling hook', 4).
card_layout('grappling hook', 'normal').

% Found in: SOM
card_name('grasp of darkness', 'Grasp of Darkness').
card_type('grasp of darkness', 'Instant').
card_types('grasp of darkness', ['Instant']).
card_subtypes('grasp of darkness', []).
card_colors('grasp of darkness', ['B']).
card_text('grasp of darkness', 'Target creature gets -4/-4 until end of turn.').
card_mana_cost('grasp of darkness', ['B', 'B']).
card_cmc('grasp of darkness', 2).
card_layout('grasp of darkness', 'normal').

% Found in: ISD
card_name('grasp of phantoms', 'Grasp of Phantoms').
card_type('grasp of phantoms', 'Sorcery').
card_types('grasp of phantoms', ['Sorcery']).
card_subtypes('grasp of phantoms', []).
card_colors('grasp of phantoms', ['U']).
card_text('grasp of phantoms', 'Put target creature on top of its owner\'s library.\nFlashback {7}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('grasp of phantoms', ['3', 'U']).
card_cmc('grasp of phantoms', 4).
card_layout('grasp of phantoms', 'normal').

% Found in: ORI
card_name('grasp of the hieromancer', 'Grasp of the Hieromancer').
card_type('grasp of the hieromancer', 'Enchantment — Aura').
card_types('grasp of the hieromancer', ['Enchantment']).
card_subtypes('grasp of the hieromancer', ['Aura']).
card_colors('grasp of the hieromancer', ['W']).
card_text('grasp of the hieromancer', 'Enchant creature\nEnchanted creature gets +1/+1 and has \"Whenever this creature attacks, tap target creature defending player controls.\"').
card_mana_cost('grasp of the hieromancer', ['1', 'W']).
card_cmc('grasp of the hieromancer', 2).
card_layout('grasp of the hieromancer', 'normal').

% Found in: ONS
card_name('grassland crusader', 'Grassland Crusader').
card_type('grassland crusader', 'Creature — Human Cleric Soldier').
card_types('grassland crusader', ['Creature']).
card_subtypes('grassland crusader', ['Human', 'Cleric', 'Soldier']).
card_colors('grassland crusader', ['W']).
card_text('grassland crusader', '{T}: Target Elf or Soldier creature gets +2/+2 until end of turn.').
card_mana_cost('grassland crusader', ['5', 'W']).
card_cmc('grassland crusader', 6).
card_layout('grassland crusader', 'normal').
card_power('grassland crusader', 2).
card_toughness('grassland crusader', 4).

% Found in: DDG, MIR, VMA
card_name('grasslands', 'Grasslands').
card_type('grasslands', 'Land').
card_types('grasslands', ['Land']).
card_subtypes('grasslands', []).
card_colors('grasslands', []).
card_text('grasslands', 'Grasslands enters the battlefield tapped.\n{T}, Sacrifice Grasslands: Search your library for a Forest or Plains card and put it onto the battlefield. Then shuffle your library.').
card_layout('grasslands', 'normal').

% Found in: ONS
card_name('gratuitous violence', 'Gratuitous Violence').
card_type('gratuitous violence', 'Enchantment').
card_types('gratuitous violence', ['Enchantment']).
card_subtypes('gratuitous violence', []).
card_colors('gratuitous violence', ['R']).
card_text('gratuitous violence', 'If a creature you control would deal damage to a creature or player, it deals double that damage to that creature or player instead.').
card_mana_cost('gratuitous violence', ['2', 'R', 'R', 'R']).
card_cmc('gratuitous violence', 5).
card_layout('gratuitous violence', 'normal').

% Found in: RTR
card_name('grave betrayal', 'Grave Betrayal').
card_type('grave betrayal', 'Enchantment').
card_types('grave betrayal', ['Enchantment']).
card_subtypes('grave betrayal', []).
card_colors('grave betrayal', ['B']).
card_text('grave betrayal', 'Whenever a creature you don\'t control dies, return it to the battlefield under your control with an additional +1/+1 counter on it at the beginning of the next end step. That creature is a black Zombie in addition to its other colors and types.').
card_mana_cost('grave betrayal', ['5', 'B', 'B']).
card_cmc('grave betrayal', 7).
card_layout('grave betrayal', 'normal').

% Found in: BFZ
card_name('grave birthing', 'Grave Birthing').
card_type('grave birthing', 'Instant').
card_types('grave birthing', ['Instant']).
card_subtypes('grave birthing', []).
card_colors('grave birthing', []).
card_text('grave birthing', 'Devoid (This card has no color.)\nTarget opponent exiles a card from his or her graveyard. You put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"\nDraw a card.').
card_mana_cost('grave birthing', ['2', 'B']).
card_cmc('grave birthing', 3).
card_layout('grave birthing', 'normal').

% Found in: ISD
card_name('grave bramble', 'Grave Bramble').
card_type('grave bramble', 'Creature — Plant').
card_types('grave bramble', ['Creature']).
card_subtypes('grave bramble', ['Plant']).
card_colors('grave bramble', ['G']).
card_text('grave bramble', 'Defender, protection from Zombies').
card_mana_cost('grave bramble', ['1', 'G', 'G']).
card_cmc('grave bramble', 3).
card_layout('grave bramble', 'normal').
card_power('grave bramble', 3).
card_toughness('grave bramble', 4).

% Found in: JUD
card_name('grave consequences', 'Grave Consequences').
card_type('grave consequences', 'Instant').
card_types('grave consequences', ['Instant']).
card_subtypes('grave consequences', []).
card_colors('grave consequences', ['B']).
card_text('grave consequences', 'Each player may exile any number of cards from his or her graveyard. Then each player loses 1 life for each card in his or her graveyard.\nDraw a card.').
card_mana_cost('grave consequences', ['1', 'B']).
card_cmc('grave consequences', 2).
card_layout('grave consequences', 'normal').

% Found in: APC
card_name('grave defiler', 'Grave Defiler').
card_type('grave defiler', 'Creature — Zombie').
card_types('grave defiler', ['Creature']).
card_subtypes('grave defiler', ['Zombie']).
card_colors('grave defiler', ['B']).
card_text('grave defiler', 'When Grave Defiler enters the battlefield, reveal the top four cards of your library. Put all Zombie cards revealed this way into your hand and the rest on the bottom of your library in any order.\n{1}{B}: Regenerate Grave Defiler.').
card_mana_cost('grave defiler', ['3', 'B']).
card_cmc('grave defiler', 4).
card_layout('grave defiler', 'normal').
card_power('grave defiler', 2).
card_toughness('grave defiler', 1).

% Found in: AVR
card_name('grave exchange', 'Grave Exchange').
card_type('grave exchange', 'Sorcery').
card_types('grave exchange', ['Sorcery']).
card_subtypes('grave exchange', []).
card_colors('grave exchange', ['B']).
card_text('grave exchange', 'Return target creature card from your graveyard to your hand. Target player sacrifices a creature.').
card_mana_cost('grave exchange', ['4', 'B', 'B']).
card_cmc('grave exchange', 6).
card_layout('grave exchange', 'normal').

% Found in: 10E, 8ED, 9ED, CMD, HOP, STH
card_name('grave pact', 'Grave Pact').
card_type('grave pact', 'Enchantment').
card_types('grave pact', ['Enchantment']).
card_subtypes('grave pact', []).
card_colors('grave pact', ['B']).
card_text('grave pact', 'Whenever a creature you control dies, each other player sacrifices a creature.').
card_mana_cost('grave pact', ['1', 'B', 'B', 'B']).
card_cmc('grave pact', 4).
card_layout('grave pact', 'normal').

% Found in: FUT
card_name('grave peril', 'Grave Peril').
card_type('grave peril', 'Enchantment').
card_types('grave peril', ['Enchantment']).
card_subtypes('grave peril', []).
card_colors('grave peril', ['B']).
card_text('grave peril', 'When a nonblack creature enters the battlefield, sacrifice Grave Peril. If you do, destroy that creature.').
card_mana_cost('grave peril', ['1', 'B']).
card_cmc('grave peril', 2).
card_layout('grave peril', 'normal').

% Found in: DRK
card_name('grave robbers', 'Grave Robbers').
card_type('grave robbers', 'Creature — Human Rogue').
card_types('grave robbers', ['Creature']).
card_subtypes('grave robbers', ['Human', 'Rogue']).
card_colors('grave robbers', ['B']).
card_text('grave robbers', '{B}, {T}: Exile target artifact card from a graveyard. You gain 2 life.').
card_mana_cost('grave robbers', ['1', 'B', 'B']).
card_cmc('grave robbers', 3).
card_layout('grave robbers', 'normal').
card_power('grave robbers', 1).
card_toughness('grave robbers', 1).
card_reserved('grave robbers').

% Found in: FUT
card_name('grave scrabbler', 'Grave Scrabbler').
card_type('grave scrabbler', 'Creature — Zombie').
card_types('grave scrabbler', ['Creature']).
card_subtypes('grave scrabbler', ['Zombie']).
card_colors('grave scrabbler', ['B']).
card_text('grave scrabbler', 'Madness {1}{B} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)\nWhen Grave Scrabbler enters the battlefield, if its madness cost was paid, you may return target creature card from a graveyard to its owner\'s hand.').
card_mana_cost('grave scrabbler', ['3', 'B']).
card_cmc('grave scrabbler', 4).
card_layout('grave scrabbler', 'normal').
card_power('grave scrabbler', 2).
card_toughness('grave scrabbler', 2).

% Found in: MIR
card_name('grave servitude', 'Grave Servitude').
card_type('grave servitude', 'Enchantment — Aura').
card_types('grave servitude', ['Enchantment']).
card_subtypes('grave servitude', ['Aura']).
card_colors('grave servitude', ['B']).
card_text('grave servitude', 'You may cast Grave Servitude as though it had flash. If you cast it any time a sorcery couldn\'t have been cast, the controller of the permanent it becomes sacrifices it at the beginning of the next cleanup step.\nEnchant creature\nEnchanted creature gets +3/-1 and is black.').
card_mana_cost('grave servitude', ['1', 'B']).
card_cmc('grave servitude', 2).
card_layout('grave servitude', 'normal').

% Found in: C14
card_name('grave sifter', 'Grave Sifter').
card_type('grave sifter', 'Creature — Elemental Beast').
card_types('grave sifter', ['Creature']).
card_subtypes('grave sifter', ['Elemental', 'Beast']).
card_colors('grave sifter', ['G']).
card_text('grave sifter', 'When Grave Sifter enters the battlefield, each player chooses a creature type and returns any number of cards of that type from his or her graveyard to his or her hand.').
card_mana_cost('grave sifter', ['5', 'G']).
card_cmc('grave sifter', 6).
card_layout('grave sifter', 'normal').
card_power('grave sifter', 5).
card_toughness('grave sifter', 7).

% Found in: FRF
card_name('grave strength', 'Grave Strength').
card_type('grave strength', 'Sorcery').
card_types('grave strength', ['Sorcery']).
card_subtypes('grave strength', []).
card_colors('grave strength', ['B']).
card_text('grave strength', 'Choose target creature. Put the top three cards of your library into your graveyard, then put a +1/+1 counter on that creature for each creature card in your graveyard.').
card_mana_cost('grave strength', ['1', 'B']).
card_cmc('grave strength', 2).
card_layout('grave strength', 'normal').

% Found in: C14, M11, M12, pMEI
card_name('grave titan', 'Grave Titan').
card_type('grave titan', 'Creature — Giant').
card_types('grave titan', ['Creature']).
card_subtypes('grave titan', ['Giant']).
card_colors('grave titan', ['B']).
card_text('grave titan', 'Deathtouch\nWhenever Grave Titan enters the battlefield or attacks, put two 2/2 black Zombie creature tokens onto the battlefield.').
card_mana_cost('grave titan', ['4', 'B', 'B']).
card_cmc('grave titan', 6).
card_layout('grave titan', 'normal').
card_power('grave titan', 6).
card_toughness('grave titan', 6).

% Found in: RAV
card_name('grave-shell scarab', 'Grave-Shell Scarab').
card_type('grave-shell scarab', 'Creature — Insect').
card_types('grave-shell scarab', ['Creature']).
card_subtypes('grave-shell scarab', ['Insect']).
card_colors('grave-shell scarab', ['B', 'G']).
card_text('grave-shell scarab', '{1}, Sacrifice Grave-Shell Scarab: Draw a card.\nDredge 1 (If you would draw a card, instead you may put exactly one card from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('grave-shell scarab', ['2', 'B', 'G', 'G']).
card_cmc('grave-shell scarab', 5).
card_layout('grave-shell scarab', 'normal').
card_power('grave-shell scarab', 4).
card_toughness('grave-shell scarab', 4).

% Found in: 6ED, MIR
card_name('gravebane zombie', 'Gravebane Zombie').
card_type('gravebane zombie', 'Creature — Zombie').
card_types('gravebane zombie', ['Creature']).
card_subtypes('gravebane zombie', ['Zombie']).
card_colors('gravebane zombie', ['B']).
card_text('gravebane zombie', 'If Gravebane Zombie would die, put Gravebane Zombie on top of its owner\'s library instead.').
card_mana_cost('gravebane zombie', ['3', 'B']).
card_cmc('gravebane zombie', 4).
card_layout('gravebane zombie', 'normal').
card_power('gravebane zombie', 3).
card_toughness('gravebane zombie', 2).

% Found in: ICE, ME4
card_name('gravebind', 'Gravebind').
card_type('gravebind', 'Instant').
card_types('gravebind', ['Instant']).
card_subtypes('gravebind', []).
card_colors('gravebind', ['B']).
card_text('gravebind', 'Target creature can\'t be regenerated this turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('gravebind', ['B']).
card_cmc('gravebind', 1).
card_layout('gravebind', 'normal').
card_reserved('gravebind').

% Found in: ORI
card_name('graveblade marauder', 'Graveblade Marauder').
card_type('graveblade marauder', 'Creature — Human Warrior').
card_types('graveblade marauder', ['Creature']).
card_subtypes('graveblade marauder', ['Human', 'Warrior']).
card_colors('graveblade marauder', ['B']).
card_text('graveblade marauder', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nWhenever Graveblade Marauder deals combat damage to a player, that player loses life equal to the number of creature cards in your graveyard.').
card_mana_cost('graveblade marauder', ['2', 'B']).
card_cmc('graveblade marauder', 3).
card_layout('graveblade marauder', 'normal').
card_power('graveblade marauder', 1).
card_toughness('graveblade marauder', 4).

% Found in: 10E, LGN
card_name('graveborn muse', 'Graveborn Muse').
card_type('graveborn muse', 'Creature — Zombie Spirit').
card_types('graveborn muse', ['Creature']).
card_subtypes('graveborn muse', ['Zombie', 'Spirit']).
card_colors('graveborn muse', ['B']).
card_text('graveborn muse', 'At the beginning of your upkeep, you draw X cards and you lose X life, where X is the number of Zombies you control.').
card_mana_cost('graveborn muse', ['2', 'B', 'B']).
card_cmc('graveborn muse', 4).
card_layout('graveborn muse', 'normal').
card_power('graveborn muse', 3).
card_toughness('graveborn muse', 3).

% Found in: DKA, pMEI
card_name('gravecrawler', 'Gravecrawler').
card_type('gravecrawler', 'Creature — Zombie').
card_types('gravecrawler', ['Creature']).
card_subtypes('gravecrawler', ['Zombie']).
card_colors('gravecrawler', ['B']).
card_text('gravecrawler', 'Gravecrawler can\'t block.\nYou may cast Gravecrawler from your graveyard as long as you control a Zombie.').
card_mana_cost('gravecrawler', ['B']).
card_cmc('gravecrawler', 1).
card_layout('gravecrawler', 'normal').
card_power('gravecrawler', 2).
card_toughness('gravecrawler', 1).

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, BTD, CMD, HOP, M10, M11, M12, M15, ODY, POR, S99, TMP, TPR, pGTW
card_name('gravedigger', 'Gravedigger').
card_type('gravedigger', 'Creature — Zombie').
card_types('gravedigger', ['Creature']).
card_subtypes('gravedigger', ['Zombie']).
card_colors('gravedigger', ['B']).
card_text('gravedigger', 'When Gravedigger enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_mana_cost('gravedigger', ['3', 'B']).
card_cmc('gravedigger', 4).
card_layout('gravedigger', 'normal').
card_power('gravedigger', 2).
card_toughness('gravedigger', 2).

% Found in: TOR
card_name('gravegouger', 'Gravegouger').
card_type('gravegouger', 'Creature — Nightmare Horror').
card_types('gravegouger', ['Creature']).
card_subtypes('gravegouger', ['Nightmare', 'Horror']).
card_colors('gravegouger', ['B']).
card_text('gravegouger', 'When Gravegouger enters the battlefield, exile up to two target cards from a single graveyard.\nWhen Gravegouger leaves the battlefield, return the exiled cards to their owner\'s graveyard.').
card_mana_cost('gravegouger', ['2', 'B']).
card_cmc('gravegouger', 3).
card_layout('gravegouger', 'normal').
card_power('gravegouger', 2).
card_toughness('gravegouger', 2).

% Found in: ONS
card_name('gravel slinger', 'Gravel Slinger').
card_type('gravel slinger', 'Creature — Human Soldier').
card_types('gravel slinger', ['Creature']).
card_subtypes('gravel slinger', ['Human', 'Soldier']).
card_colors('gravel slinger', ['W']).
card_text('gravel slinger', '{T}: Gravel Slinger deals 1 damage to target attacking or blocking creature.\nMorph {1}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('gravel slinger', ['3', 'W']).
card_cmc('gravel slinger', 4).
card_layout('gravel slinger', 'normal').
card_power('gravel slinger', 1).
card_toughness('gravel slinger', 3).

% Found in: SHM
card_name('gravelgill axeshark', 'Gravelgill Axeshark').
card_type('gravelgill axeshark', 'Creature — Merfolk Soldier').
card_types('gravelgill axeshark', ['Creature']).
card_subtypes('gravelgill axeshark', ['Merfolk', 'Soldier']).
card_colors('gravelgill axeshark', ['U', 'B']).
card_text('gravelgill axeshark', 'Persist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('gravelgill axeshark', ['4', 'U/B']).
card_cmc('gravelgill axeshark', 5).
card_layout('gravelgill axeshark', 'normal').
card_power('gravelgill axeshark', 3).
card_toughness('gravelgill axeshark', 3).

% Found in: SHM
card_name('gravelgill duo', 'Gravelgill Duo').
card_type('gravelgill duo', 'Creature — Merfolk Rogue Warrior').
card_types('gravelgill duo', ['Creature']).
card_subtypes('gravelgill duo', ['Merfolk', 'Rogue', 'Warrior']).
card_colors('gravelgill duo', ['U', 'B']).
card_text('gravelgill duo', 'Whenever you cast a blue spell, Gravelgill Duo gets +1/+1 until end of turn.\nWhenever you cast a black spell, Gravelgill Duo gains fear until end of turn. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('gravelgill duo', ['2', 'U/B']).
card_cmc('gravelgill duo', 3).
card_layout('gravelgill duo', 'normal').
card_power('gravelgill duo', 2).
card_toughness('gravelgill duo', 1).

% Found in: FUT, SHM
card_name('graven cairns', 'Graven Cairns').
card_type('graven cairns', 'Land').
card_types('graven cairns', ['Land']).
card_subtypes('graven cairns', []).
card_colors('graven cairns', []).
card_text('graven cairns', '{T}: Add {1} to your mana pool.\n{B/R}, {T}: Add {B}{B}, {B}{R}, or {R}{R} to your mana pool.').
card_layout('graven cairns', 'normal').

% Found in: GPT
card_name('graven dominator', 'Graven Dominator').
card_type('graven dominator', 'Creature — Gargoyle').
card_types('graven dominator', ['Creature']).
card_subtypes('graven dominator', ['Gargoyle']).
card_colors('graven dominator', ['W']).
card_text('graven dominator', 'Flying\nHaunt (When this creature dies, exile it haunting target creature.)\nWhen Graven Dominator enters the battlefield or the creature it haunts dies, each other creature has base power and toughness 1/1 until end of turn.').
card_mana_cost('graven dominator', ['4', 'W', 'W']).
card_cmc('graven dominator', 6).
card_layout('graven dominator', 'normal').
card_power('graven dominator', 4).
card_toughness('graven dominator', 4).

% Found in: DKA, DTK
card_name('gravepurge', 'Gravepurge').
card_type('gravepurge', 'Instant').
card_types('gravepurge', ['Instant']).
card_subtypes('gravepurge', []).
card_colors('gravepurge', ['B']).
card_text('gravepurge', 'Put any number of target creature cards from your graveyard on top of your library.\nDraw a card.').
card_mana_cost('gravepurge', ['2', 'B']).
card_cmc('gravepurge', 3).
card_layout('gravepurge', 'normal').

% Found in: BNG
card_name('graverobber spider', 'Graverobber Spider').
card_type('graverobber spider', 'Creature — Spider').
card_types('graverobber spider', ['Creature']).
card_subtypes('graverobber spider', ['Spider']).
card_colors('graverobber spider', ['G']).
card_text('graverobber spider', 'Reach\n{3}{B}: Graverobber Spider gets +X/+X until end of turn, where X is the number of creature cards in your graveyard. Activate this ability only once each turn.').
card_mana_cost('graverobber spider', ['3', 'G']).
card_cmc('graverobber spider', 4).
card_layout('graverobber spider', 'normal').
card_power('graverobber spider', 2).
card_toughness('graverobber spider', 4).

% Found in: ONS
card_name('gravespawn sovereign', 'Gravespawn Sovereign').
card_type('gravespawn sovereign', 'Creature — Zombie').
card_types('gravespawn sovereign', ['Creature']).
card_subtypes('gravespawn sovereign', ['Zombie']).
card_colors('gravespawn sovereign', ['B']).
card_text('gravespawn sovereign', 'Tap five untapped Zombies you control: Put target creature card from a graveyard onto the battlefield under your control.').
card_mana_cost('gravespawn sovereign', ['4', 'B', 'B']).
card_cmc('gravespawn sovereign', 6).
card_layout('gravespawn sovereign', 'normal').
card_power('gravespawn sovereign', 3).
card_toughness('gravespawn sovereign', 3).

% Found in: ODY
card_name('gravestorm', 'Gravestorm').
card_type('gravestorm', 'Enchantment').
card_types('gravestorm', ['Enchantment']).
card_subtypes('gravestorm', []).
card_colors('gravestorm', ['B']).
card_text('gravestorm', 'At the beginning of your upkeep, target opponent may exile a card from his or her graveyard. If that player doesn\'t, you may draw a card.').
card_mana_cost('gravestorm', ['B', 'B', 'B']).
card_cmc('gravestorm', 3).
card_layout('gravestorm', 'normal').

% Found in: DKA
card_name('gravetiller wurm', 'Gravetiller Wurm').
card_type('gravetiller wurm', 'Creature — Wurm').
card_types('gravetiller wurm', ['Creature']).
card_subtypes('gravetiller wurm', ['Wurm']).
card_colors('gravetiller wurm', ['G']).
card_text('gravetiller wurm', 'Trample\nMorbid — Gravetiller Wurm enters the battlefield with four +1/+1 counters on it if a creature died this turn.').
card_mana_cost('gravetiller wurm', ['5', 'G']).
card_cmc('gravetiller wurm', 6).
card_layout('gravetiller wurm', 'normal').
card_power('gravetiller wurm', 4).
card_toughness('gravetiller wurm', 4).

% Found in: ISD
card_name('graveyard shovel', 'Graveyard Shovel').
card_type('graveyard shovel', 'Artifact').
card_types('graveyard shovel', ['Artifact']).
card_subtypes('graveyard shovel', []).
card_colors('graveyard shovel', []).
card_text('graveyard shovel', '{2}, {T}: Target player exiles a card from his or her graveyard. If it\'s a creature card, you gain 2 life.').
card_mana_cost('graveyard shovel', ['2']).
card_cmc('graveyard shovel', 2).
card_layout('graveyard shovel', 'normal').

% Found in: ROE
card_name('gravitational shift', 'Gravitational Shift').
card_type('gravitational shift', 'Enchantment').
card_types('gravitational shift', ['Enchantment']).
card_subtypes('gravitational shift', []).
card_colors('gravitational shift', ['U']).
card_text('gravitational shift', 'Creatures with flying get +2/+0.\nCreatures without flying get -2/-0.').
card_mana_cost('gravitational shift', ['3', 'U', 'U']).
card_cmc('gravitational shift', 5).
card_layout('gravitational shift', 'normal').

% Found in: LEG
card_name('gravity sphere', 'Gravity Sphere').
card_type('gravity sphere', 'World Enchantment').
card_types('gravity sphere', ['Enchantment']).
card_subtypes('gravity sphere', []).
card_supertypes('gravity sphere', ['World']).
card_colors('gravity sphere', ['R']).
card_text('gravity sphere', 'All creatures lose flying.').
card_mana_cost('gravity sphere', ['2', 'R']).
card_cmc('gravity sphere', 3).
card_layout('gravity sphere', 'normal').
card_reserved('gravity sphere').

% Found in: ROE
card_name('gravity well', 'Gravity Well').
card_type('gravity well', 'Enchantment').
card_types('gravity well', ['Enchantment']).
card_subtypes('gravity well', []).
card_colors('gravity well', ['G']).
card_text('gravity well', 'Whenever a creature with flying attacks, it loses flying until end of turn.').
card_mana_cost('gravity well', ['1', 'G', 'G']).
card_cmc('gravity well', 3).
card_layout('gravity well', 'normal').

% Found in: ONS
card_name('graxiplon', 'Graxiplon').
card_type('graxiplon', 'Creature — Beast').
card_types('graxiplon', ['Creature']).
card_subtypes('graxiplon', ['Beast']).
card_colors('graxiplon', ['U']).
card_text('graxiplon', 'Graxiplon can\'t be blocked unless defending player controls three or more creatures that share a creature type.').
card_mana_cost('graxiplon', ['5', 'U']).
card_cmc('graxiplon', 6).
card_layout('graxiplon', 'normal').
card_power('graxiplon', 3).
card_toughness('graxiplon', 4).

% Found in: C14, THS
card_name('gray merchant of asphodel', 'Gray Merchant of Asphodel').
card_type('gray merchant of asphodel', 'Creature — Zombie').
card_types('gray merchant of asphodel', ['Creature']).
card_subtypes('gray merchant of asphodel', ['Zombie']).
card_colors('gray merchant of asphodel', ['B']).
card_text('gray merchant of asphodel', 'When Gray Merchant of Asphodel enters the battlefield, each opponent loses X life, where X is your devotion to black. You gain life equal to the life lost this way. (Each {B} in the mana costs of permanents you control counts toward your devotion to black.)').
card_mana_cost('gray merchant of asphodel', ['3', 'B', 'B']).
card_cmc('gray merchant of asphodel', 5).
card_layout('gray merchant of asphodel', 'normal').
card_power('gray merchant of asphodel', 2).
card_toughness('gray merchant of asphodel', 4).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('gray ogre', 'Gray Ogre').
card_type('gray ogre', 'Creature — Ogre').
card_types('gray ogre', ['Creature']).
card_subtypes('gray ogre', ['Ogre']).
card_colors('gray ogre', ['R']).
card_text('gray ogre', '').
card_mana_cost('gray ogre', ['2', 'R']).
card_cmc('gray ogre', 3).
card_layout('gray ogre', 'normal').
card_power('gray ogre', 2).
card_toughness('gray ogre', 2).

% Found in: DDP, WWK
card_name('graypelt hunter', 'Graypelt Hunter').
card_type('graypelt hunter', 'Creature — Human Warrior Ally').
card_types('graypelt hunter', ['Creature']).
card_subtypes('graypelt hunter', ['Human', 'Warrior', 'Ally']).
card_colors('graypelt hunter', ['G']).
card_text('graypelt hunter', 'Trample\nWhenever Graypelt Hunter or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Graypelt Hunter.').
card_mana_cost('graypelt hunter', ['3', 'G']).
card_cmc('graypelt hunter', 4).
card_layout('graypelt hunter', 'normal').
card_power('graypelt hunter', 2).
card_toughness('graypelt hunter', 2).

% Found in: ARC, DDH, DDP, PC2, ZEN
card_name('graypelt refuge', 'Graypelt Refuge').
card_type('graypelt refuge', 'Land').
card_types('graypelt refuge', ['Land']).
card_subtypes('graypelt refuge', []).
card_colors('graypelt refuge', []).
card_text('graypelt refuge', 'Graypelt Refuge enters the battlefield tapped.\nWhen Graypelt Refuge enters the battlefield, you gain 1 life.\n{T}: Add {G} or {W} to your mana pool.').
card_layout('graypelt refuge', 'normal').

% Found in: RAV
card_name('grayscaled gharial', 'Grayscaled Gharial').
card_type('grayscaled gharial', 'Creature — Crocodile').
card_types('grayscaled gharial', ['Creature']).
card_subtypes('grayscaled gharial', ['Crocodile']).
card_colors('grayscaled gharial', ['U']).
card_text('grayscaled gharial', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('grayscaled gharial', ['U']).
card_cmc('grayscaled gharial', 1).
card_layout('grayscaled gharial', 'normal').
card_power('grayscaled gharial', 1).
card_toughness('grayscaled gharial', 1).

% Found in: C13, DDH, DDO, DDP, ZEN
card_name('grazing gladehart', 'Grazing Gladehart').
card_type('grazing gladehart', 'Creature — Antelope').
card_types('grazing gladehart', ['Creature']).
card_subtypes('grazing gladehart', ['Antelope']).
card_colors('grazing gladehart', ['G']).
card_text('grazing gladehart', 'Landfall — Whenever a land enters the battlefield under your control, you may gain 2 life.').
card_mana_cost('grazing gladehart', ['2', 'G']).
card_cmc('grazing gladehart', 3).
card_layout('grazing gladehart', 'normal').
card_power('grazing gladehart', 2).
card_toughness('grazing gladehart', 2).

% Found in: EVE
card_name('grazing kelpie', 'Grazing Kelpie').
card_type('grazing kelpie', 'Creature — Beast').
card_types('grazing kelpie', ['Creature']).
card_subtypes('grazing kelpie', ['Beast']).
card_colors('grazing kelpie', ['U', 'G']).
card_text('grazing kelpie', '{G/U}, Sacrifice Grazing Kelpie: Put target card from a graveyard on the bottom of its owner\'s library.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('grazing kelpie', ['3', 'G/U']).
card_cmc('grazing kelpie', 4).
card_layout('grazing kelpie', 'normal').
card_power('grazing kelpie', 2).
card_toughness('grazing kelpie', 3).

% Found in: LEG
card_name('great defender', 'Great Defender').
card_type('great defender', 'Instant').
card_types('great defender', ['Instant']).
card_subtypes('great defender', []).
card_colors('great defender', ['W']).
card_text('great defender', 'Target creature gets +0/+X until end of turn, where X is its converted mana cost.').
card_mana_cost('great defender', ['W']).
card_cmc('great defender', 1).
card_layout('great defender', 'normal').

% Found in: C14, HOP, MRD
card_name('great furnace', 'Great Furnace').
card_type('great furnace', 'Artifact Land').
card_types('great furnace', ['Artifact', 'Land']).
card_subtypes('great furnace', []).
card_colors('great furnace', []).
card_text('great furnace', '{T}: Add {R} to your mana pool.').
card_layout('great furnace', 'normal').

% Found in: BNG
card_name('great hart', 'Great Hart').
card_type('great hart', 'Creature — Elk').
card_types('great hart', ['Creature']).
card_subtypes('great hart', ['Elk']).
card_colors('great hart', ['W']).
card_text('great hart', '').
card_mana_cost('great hart', ['3', 'W']).
card_cmc('great hart', 4).
card_layout('great hart', 'normal').
card_power('great hart', 2).
card_toughness('great hart', 4).

% Found in: M10
card_name('great sable stag', 'Great Sable Stag').
card_type('great sable stag', 'Creature — Elk').
card_types('great sable stag', ['Creature']).
card_subtypes('great sable stag', ['Elk']).
card_colors('great sable stag', ['G']).
card_text('great sable stag', 'Great Sable Stag can\'t be countered.\nProtection from blue and from black (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything blue or black.)').
card_mana_cost('great sable stag', ['1', 'G', 'G']).
card_cmc('great sable stag', 3).
card_layout('great sable stag', 'normal').
card_power('great sable stag', 3).
card_toughness('great sable stag', 3).

% Found in: DTK
card_name('great teacher\'s decree', 'Great Teacher\'s Decree').
card_type('great teacher\'s decree', 'Sorcery').
card_types('great teacher\'s decree', ['Sorcery']).
card_subtypes('great teacher\'s decree', []).
card_colors('great teacher\'s decree', ['W']).
card_text('great teacher\'s decree', 'Creatures you control get +2/+1 until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('great teacher\'s decree', ['3', 'W']).
card_cmc('great teacher\'s decree', 4).
card_layout('great teacher\'s decree', 'normal').

% Found in: LEG
card_name('great wall', 'Great Wall').
card_type('great wall', 'Enchantment').
card_types('great wall', ['Enchantment']).
card_subtypes('great wall', []).
card_colors('great wall', ['W']).
card_text('great wall', 'Creatures with plainswalk can be blocked as though they didn\'t have plainswalk.').
card_mana_cost('great wall', ['2', 'W']).
card_cmc('great wall', 3).
card_layout('great wall', 'normal').

% Found in: USG
card_name('great whale', 'Great Whale').
card_type('great whale', 'Creature — Whale').
card_types('great whale', ['Creature']).
card_subtypes('great whale', ['Whale']).
card_colors('great whale', ['U']).
card_text('great whale', 'When Great Whale enters the battlefield, untap up to seven lands.').
card_mana_cost('great whale', ['5', 'U', 'U']).
card_cmc('great whale', 7).
card_layout('great whale', 'normal').
card_power('great whale', 5).
card_toughness('great whale', 5).
card_reserved('great whale').

% Found in: FRF
card_name('great-horn krushok', 'Great-Horn Krushok').
card_type('great-horn krushok', 'Creature — Beast').
card_types('great-horn krushok', ['Creature']).
card_subtypes('great-horn krushok', ['Beast']).
card_colors('great-horn krushok', ['W']).
card_text('great-horn krushok', '').
card_mana_cost('great-horn krushok', ['4', 'W']).
card_cmc('great-horn krushok', 5).
card_layout('great-horn krushok', 'normal').
card_power('great-horn krushok', 3).
card_toughness('great-horn krushok', 5).

% Found in: MOR
card_name('greatbow doyen', 'Greatbow Doyen').
card_type('greatbow doyen', 'Creature — Elf Archer').
card_types('greatbow doyen', ['Creature']).
card_subtypes('greatbow doyen', ['Elf', 'Archer']).
card_colors('greatbow doyen', ['G']).
card_text('greatbow doyen', 'Other Archer creatures you control get +1/+1.\nWhenever an Archer you control deals damage to a creature, that Archer deals that much damage to that creature\'s controller.').
card_mana_cost('greatbow doyen', ['4', 'G']).
card_cmc('greatbow doyen', 5).
card_layout('greatbow doyen', 'normal').
card_power('greatbow doyen', 2).
card_toughness('greatbow doyen', 4).

% Found in: SHM
card_name('greater auramancy', 'Greater Auramancy').
card_type('greater auramancy', 'Enchantment').
card_types('greater auramancy', ['Enchantment']).
card_subtypes('greater auramancy', []).
card_colors('greater auramancy', ['W']).
card_text('greater auramancy', 'Other enchantments you control have shroud. (A permanent with shroud can\'t be the target of spells or abilities.)\nEnchanted creatures you control have shroud.').
card_mana_cost('greater auramancy', ['1', 'W']).
card_cmc('greater auramancy', 2).
card_layout('greater auramancy', 'normal').

% Found in: M11, M12
card_name('greater basilisk', 'Greater Basilisk').
card_type('greater basilisk', 'Creature — Basilisk').
card_types('greater basilisk', ['Creature']).
card_subtypes('greater basilisk', ['Basilisk']).
card_colors('greater basilisk', ['G']).
card_text('greater basilisk', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('greater basilisk', ['3', 'G', 'G']).
card_cmc('greater basilisk', 5).
card_layout('greater basilisk', 'normal').
card_power('greater basilisk', 3).
card_toughness('greater basilisk', 5).

% Found in: RAV
card_name('greater forgeling', 'Greater Forgeling').
card_type('greater forgeling', 'Creature — Elemental').
card_types('greater forgeling', ['Creature']).
card_subtypes('greater forgeling', ['Elemental']).
card_colors('greater forgeling', ['R']).
card_text('greater forgeling', '{1}{R}: Greater Forgeling gets +3/-3 until end of turn.').
card_mana_cost('greater forgeling', ['3', 'R', 'R']).
card_cmc('greater forgeling', 5).
card_layout('greater forgeling', 'normal').
card_power('greater forgeling', 3).
card_toughness('greater forgeling', 4).

% Found in: MMA, TSP
card_name('greater gargadon', 'Greater Gargadon').
card_type('greater gargadon', 'Creature — Beast').
card_types('greater gargadon', ['Creature']).
card_subtypes('greater gargadon', ['Beast']).
card_colors('greater gargadon', ['R']).
card_text('greater gargadon', 'Suspend 10—{R}\nSacrifice an artifact, creature, or land: Remove a time counter from Greater Gargadon. Activate this ability only if Greater Gargadon is suspended.').
card_mana_cost('greater gargadon', ['9', 'R']).
card_cmc('greater gargadon', 10).
card_layout('greater gargadon', 'normal').
card_power('greater gargadon', 9).
card_toughness('greater gargadon', 7).

% Found in: 9ED, USG, pJGP
card_name('greater good', 'Greater Good').
card_type('greater good', 'Enchantment').
card_types('greater good', ['Enchantment']).
card_subtypes('greater good', []).
card_colors('greater good', ['G']).
card_text('greater good', 'Sacrifice a creature: Draw cards equal to the sacrificed creature\'s power, then discard three cards.').
card_mana_cost('greater good', ['2', 'G', 'G']).
card_cmc('greater good', 4).
card_layout('greater good', 'normal').

% Found in: DST
card_name('greater harvester', 'Greater Harvester').
card_type('greater harvester', 'Creature — Horror').
card_types('greater harvester', ['Creature']).
card_subtypes('greater harvester', ['Horror']).
card_colors('greater harvester', ['B']).
card_text('greater harvester', 'At the beginning of your upkeep, sacrifice a permanent.\nWhenever Greater Harvester deals combat damage to a player, that player sacrifices two permanents.').
card_mana_cost('greater harvester', ['2', 'B', 'B', 'B']).
card_cmc('greater harvester', 5).
card_layout('greater harvester', 'normal').
card_power('greater harvester', 5).
card_toughness('greater harvester', 6).

% Found in: UNH
card_name('greater morphling', 'Greater Morphling').
card_type('greater morphling', 'Creature — Shapeshifter').
card_types('greater morphling', ['Creature']).
card_subtypes('greater morphling', ['Shapeshifter']).
card_colors('greater morphling', ['U']).
card_text('greater morphling', '{2}: Greater Morphling gains your choice of banding, bushido 1, double strike, fear, flying, first strike, haste, landwalk of your choice, protection from a color of your choice, provoke, rampage 1, shadow, or trample until end of turn.\n{2}: Greater Morphling becomes the colors of your choice until end of turn.\n{2}: Greater Morphling\'s type becomes the creature type of your choice until end of turn.\n{2}: Greater Morphling\'s expansion symbol becomes the symbol of your choice until end of turn.\n{2}: Greater Morphling\'s artist becomes the artist of your choice until end of turn.\n{2}: Greater Morphling gets +2/-2 or -2/+2 until end of turn.\n{2}: Untap Greater Morphling.').
card_mana_cost('greater morphling', ['6', 'U', 'U']).
card_cmc('greater morphling', 8).
card_layout('greater morphling', 'normal').
card_power('greater morphling', 5).
card_toughness('greater morphling', 5).

% Found in: DDJ, MMA, RAV
card_name('greater mossdog', 'Greater Mossdog').
card_type('greater mossdog', 'Creature — Plant Hound').
card_types('greater mossdog', ['Creature']).
card_subtypes('greater mossdog', ['Plant', 'Hound']).
card_colors('greater mossdog', ['G']).
card_text('greater mossdog', 'Dredge 3 (If you would draw a card, instead you may put exactly three cards from the top of your library into your graveyard. If you do, return this card from your graveyard to your hand. Otherwise, draw a card.)').
card_mana_cost('greater mossdog', ['3', 'G']).
card_cmc('greater mossdog', 4).
card_layout('greater mossdog', 'normal').
card_power('greater mossdog', 3).
card_toughness('greater mossdog', 3).

% Found in: 5ED, LEG, MED
card_name('greater realm of preservation', 'Greater Realm of Preservation').
card_type('greater realm of preservation', 'Enchantment').
card_types('greater realm of preservation', ['Enchantment']).
card_subtypes('greater realm of preservation', []).
card_colors('greater realm of preservation', ['W']).
card_text('greater realm of preservation', '{1}{W}: The next time a black or red source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('greater realm of preservation', ['1', 'W']).
card_cmc('greater realm of preservation', 2).
card_layout('greater realm of preservation', 'normal').

% Found in: CSP, DDI
card_name('greater stone spirit', 'Greater Stone Spirit').
card_type('greater stone spirit', 'Creature — Elemental Spirit').
card_types('greater stone spirit', ['Creature']).
card_subtypes('greater stone spirit', ['Elemental', 'Spirit']).
card_colors('greater stone spirit', ['R']).
card_text('greater stone spirit', 'Greater Stone Spirit can\'t be blocked by creatures with flying.\n{2}{R}: Until end of turn, target creature gets +0/+2 and gains \"{R}: This creature gets +1/+0 until end of turn.\"').
card_mana_cost('greater stone spirit', ['4', 'R', 'R']).
card_cmc('greater stone spirit', 6).
card_layout('greater stone spirit', 'normal').
card_power('greater stone spirit', 4).
card_toughness('greater stone spirit', 4).

% Found in: 5ED, HML
card_name('greater werewolf', 'Greater Werewolf').
card_type('greater werewolf', 'Creature — Werewolf').
card_types('greater werewolf', ['Creature']).
card_subtypes('greater werewolf', ['Werewolf']).
card_colors('greater werewolf', ['B']).
card_text('greater werewolf', 'At end of combat, put a -0/-2 counter on each creature blocking or blocked by Greater Werewolf.').
card_mana_cost('greater werewolf', ['4', 'B']).
card_cmc('greater werewolf', 5).
card_layout('greater werewolf', 'normal').
card_power('greater werewolf', 2).
card_toughness('greater werewolf', 4).

% Found in: M12
card_name('greatsword', 'Greatsword').
card_type('greatsword', 'Artifact — Equipment').
card_types('greatsword', ['Artifact']).
card_subtypes('greatsword', ['Equipment']).
card_colors('greatsword', []).
card_text('greatsword', 'Equipped creature gets +3/+0.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('greatsword', ['3']).
card_cmc('greatsword', 3).
card_layout('greatsword', 'normal').

% Found in: 4ED, 6ED, 7ED, C13, LEG
card_name('greed', 'Greed').
card_type('greed', 'Enchantment').
card_types('greed', ['Enchantment']).
card_subtypes('greed', []).
card_colors('greed', ['B']).
card_text('greed', '{B}, Pay 2 life: Draw a card.').
card_mana_cost('greed', ['3', 'B']).
card_cmc('greed', 4).
card_layout('greed', 'normal').

% Found in: PCY
card_name('greel\'s caress', 'Greel\'s Caress').
card_type('greel\'s caress', 'Enchantment — Aura').
card_types('greel\'s caress', ['Enchantment']).
card_subtypes('greel\'s caress', ['Aura']).
card_colors('greel\'s caress', ['B']).
card_text('greel\'s caress', 'Flash\nEnchant creature\nEnchanted creature gets -3/-0.').
card_mana_cost('greel\'s caress', ['1', 'B']).
card_cmc('greel\'s caress', 2).
card_layout('greel\'s caress', 'normal').

% Found in: PCY
card_name('greel, mind raker', 'Greel, Mind Raker').
card_type('greel, mind raker', 'Legendary Creature — Horror Spellshaper').
card_types('greel, mind raker', ['Creature']).
card_subtypes('greel, mind raker', ['Horror', 'Spellshaper']).
card_supertypes('greel, mind raker', ['Legendary']).
card_colors('greel, mind raker', ['B']).
card_text('greel, mind raker', '{X}{B}, {T}, Discard two cards: Target player discards X cards at random.').
card_mana_cost('greel, mind raker', ['3', 'B', 'B']).
card_cmc('greel, mind raker', 5).
card_layout('greel, mind raker', 'normal').
card_power('greel, mind raker', 3).
card_toughness('greel, mind raker', 3).

% Found in: 4ED, LEG
card_name('green mana battery', 'Green Mana Battery').
card_type('green mana battery', 'Artifact').
card_types('green mana battery', ['Artifact']).
card_subtypes('green mana battery', []).
card_colors('green mana battery', []).
card_text('green mana battery', '{2}, {T}: Put a charge counter on Green Mana Battery.\n{T}, Remove any number of charge counters from Green Mana Battery: Add {G} to your mana pool, then add an additional {G} to your mana pool for each charge counter removed this way.').
card_mana_cost('green mana battery', ['4']).
card_cmc('green mana battery', 4).
card_layout('green mana battery', 'normal').

% Found in: ICE
card_name('green scarab', 'Green Scarab').
card_type('green scarab', 'Enchantment — Aura').
card_types('green scarab', ['Enchantment']).
card_subtypes('green scarab', ['Aura']).
card_colors('green scarab', ['W']).
card_text('green scarab', 'Enchant creature\nEnchanted creature can\'t be blocked by green creatures.\nEnchanted creature gets +2/+2 as long as an opponent controls a green permanent.').
card_mana_cost('green scarab', ['W']).
card_cmc('green scarab', 1).
card_layout('green scarab', 'normal').

% Found in: MBS, V13
card_name('green sun\'s zenith', 'Green Sun\'s Zenith').
card_type('green sun\'s zenith', 'Sorcery').
card_types('green sun\'s zenith', ['Sorcery']).
card_subtypes('green sun\'s zenith', []).
card_colors('green sun\'s zenith', ['G']).
card_text('green sun\'s zenith', 'Search your library for a green creature card with converted mana cost X or less, put it onto the battlefield, then shuffle your library. Shuffle Green Sun\'s Zenith into its owner\'s library.').
card_mana_cost('green sun\'s zenith', ['X', 'G']).
card_cmc('green sun\'s zenith', 1).
card_layout('green sun\'s zenith', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('green ward', 'Green Ward').
card_type('green ward', 'Enchantment — Aura').
card_types('green ward', ['Enchantment']).
card_subtypes('green ward', ['Aura']).
card_colors('green ward', ['W']).
card_text('green ward', 'Enchant creature\nEnchanted creature has protection from green. This effect doesn\'t remove Green Ward.').
card_mana_cost('green ward', ['W']).
card_cmc('green ward', 1).
card_layout('green ward', 'normal').

% Found in: USG
card_name('greener pastures', 'Greener Pastures').
card_type('greener pastures', 'Enchantment').
card_types('greener pastures', ['Enchantment']).
card_subtypes('greener pastures', []).
card_colors('greener pastures', ['G']).
card_text('greener pastures', 'At the beginning of each player\'s upkeep, if that player controls more lands than each other player, the player puts a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('greener pastures', ['2', 'G']).
card_cmc('greener pastures', 3).
card_layout('greener pastures', 'normal').

% Found in: NPH
card_name('greenhilt trainee', 'Greenhilt Trainee').
card_type('greenhilt trainee', 'Creature — Elf Warrior').
card_types('greenhilt trainee', ['Creature']).
card_subtypes('greenhilt trainee', ['Elf', 'Warrior']).
card_colors('greenhilt trainee', ['G']).
card_text('greenhilt trainee', '{T}: Target creature gets +4/+4 until end of turn. Activate this ability only if Greenhilt Trainee\'s power is 4 or greater.').
card_mana_cost('greenhilt trainee', ['3', 'G']).
card_cmc('greenhilt trainee', 4).
card_layout('greenhilt trainee', 'normal').
card_power('greenhilt trainee', 2).
card_toughness('greenhilt trainee', 3).

% Found in: TSP
card_name('greenseeker', 'Greenseeker').
card_type('greenseeker', 'Creature — Elf Spellshaper').
card_types('greenseeker', ['Creature']).
card_subtypes('greenseeker', ['Elf', 'Spellshaper']).
card_colors('greenseeker', ['G']).
card_text('greenseeker', '{G}, {T}, Discard a card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('greenseeker', ['G']).
card_cmc('greenseeker', 1).
card_layout('greenseeker', 'normal').
card_power('greenseeker', 1).
card_toughness('greenseeker', 1).

% Found in: GTC
card_name('greenside watcher', 'Greenside Watcher').
card_type('greenside watcher', 'Creature — Elf Druid').
card_types('greenside watcher', ['Creature']).
card_subtypes('greenside watcher', ['Elf', 'Druid']).
card_colors('greenside watcher', ['G']).
card_text('greenside watcher', '{T}: Untap target Gate.').
card_mana_cost('greenside watcher', ['1', 'G']).
card_cmc('greenside watcher', 2).
card_layout('greenside watcher', 'normal').
card_power('greenside watcher', 2).
card_toughness('greenside watcher', 1).

% Found in: BFZ
card_name('greenwarden of murasa', 'Greenwarden of Murasa').
card_type('greenwarden of murasa', 'Creature — Elemental').
card_types('greenwarden of murasa', ['Creature']).
card_subtypes('greenwarden of murasa', ['Elemental']).
card_colors('greenwarden of murasa', ['G']).
card_text('greenwarden of murasa', 'When Greenwarden of Murasa enters the battlefield, you may return target card from your graveyard to your hand.\nWhen Greenwarden of Murasa dies, you may exile it. If you do, return target card from your graveyard to your hand.').
card_mana_cost('greenwarden of murasa', ['4', 'G', 'G']).
card_cmc('greenwarden of murasa', 6).
card_layout('greenwarden of murasa', 'normal').
card_power('greenwarden of murasa', 5).
card_toughness('greenwarden of murasa', 4).

% Found in: DPA, ZEN
card_name('greenweaver druid', 'Greenweaver Druid').
card_type('greenweaver druid', 'Creature — Elf Druid').
card_types('greenweaver druid', ['Creature']).
card_subtypes('greenweaver druid', ['Elf', 'Druid']).
card_colors('greenweaver druid', ['G']).
card_text('greenweaver druid', '{T}: Add {G}{G} to your mana pool.').
card_mana_cost('greenweaver druid', ['2', 'G']).
card_cmc('greenweaver druid', 3).
card_layout('greenweaver druid', 'normal').
card_power('greenweaver druid', 1).
card_toughness('greenweaver druid', 1).

% Found in: NPH
card_name('gremlin mine', 'Gremlin Mine').
card_type('gremlin mine', 'Artifact').
card_types('gremlin mine', ['Artifact']).
card_subtypes('gremlin mine', []).
card_colors('gremlin mine', []).
card_text('gremlin mine', '{1}, {T}, Sacrifice Gremlin Mine: Gremlin Mine deals 4 damage to target artifact creature.\n{1}, {T}, Sacrifice Gremlin Mine: Remove up to four charge counters from target noncreature artifact.').
card_mana_cost('gremlin mine', ['1']).
card_cmc('gremlin mine', 1).
card_layout('gremlin mine', 'normal').

% Found in: CNS
card_name('grenzo\'s cutthroat', 'Grenzo\'s Cutthroat').
card_type('grenzo\'s cutthroat', 'Creature — Goblin Rogue').
card_types('grenzo\'s cutthroat', ['Creature']).
card_subtypes('grenzo\'s cutthroat', ['Goblin', 'Rogue']).
card_colors('grenzo\'s cutthroat', ['R']).
card_text('grenzo\'s cutthroat', 'First strike\nDethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)').
card_mana_cost('grenzo\'s cutthroat', ['1', 'R']).
card_cmc('grenzo\'s cutthroat', 2).
card_layout('grenzo\'s cutthroat', 'normal').
card_power('grenzo\'s cutthroat', 1).
card_toughness('grenzo\'s cutthroat', 1).

% Found in: CNS
card_name('grenzo\'s rebuttal', 'Grenzo\'s Rebuttal').
card_type('grenzo\'s rebuttal', 'Sorcery').
card_types('grenzo\'s rebuttal', ['Sorcery']).
card_subtypes('grenzo\'s rebuttal', []).
card_colors('grenzo\'s rebuttal', ['R']).
card_text('grenzo\'s rebuttal', 'Put a 4/4 red Ogre creature token onto the battlefield. Starting with you, each player chooses an artifact, a creature, and a land from among the permanents controlled by the player to his or her left. Destroy each permanent chosen this way.').
card_mana_cost('grenzo\'s rebuttal', ['4', 'R', 'R']).
card_cmc('grenzo\'s rebuttal', 6).
card_layout('grenzo\'s rebuttal', 'normal').

% Found in: CNS, VMA
card_name('grenzo, dungeon warden', 'Grenzo, Dungeon Warden').
card_type('grenzo, dungeon warden', 'Legendary Creature — Goblin Rogue').
card_types('grenzo, dungeon warden', ['Creature']).
card_subtypes('grenzo, dungeon warden', ['Goblin', 'Rogue']).
card_supertypes('grenzo, dungeon warden', ['Legendary']).
card_colors('grenzo, dungeon warden', ['B', 'R']).
card_text('grenzo, dungeon warden', 'Grenzo, Dungeon Warden enters the battlefield with X +1/+1 counters on it.\n{2}: Put the bottom card of your library into your graveyard. If it\'s a creature card with power less than or equal to Grenzo\'s power, put it onto the battlefield.').
card_mana_cost('grenzo, dungeon warden', ['X', 'B', 'R']).
card_cmc('grenzo, dungeon warden', 2).
card_layout('grenzo, dungeon warden', 'normal').
card_power('grenzo, dungeon warden', 2).
card_toughness('grenzo, dungeon warden', 2).

% Found in: VAN
card_name('greven il-vec', 'Greven il-Vec').
card_type('greven il-vec', 'Vanguard').
card_types('greven il-vec', ['Vanguard']).
card_subtypes('greven il-vec', []).
card_colors('greven il-vec', []).
card_text('greven il-vec', 'Whenever a creature you control deals damage to a creature, destroy the other creature. It can\'t be regenerated.').
card_layout('greven il-vec', 'vanguard').

% Found in: MRD
card_name('grid monitor', 'Grid Monitor').
card_type('grid monitor', 'Artifact Creature — Construct').
card_types('grid monitor', ['Artifact', 'Creature']).
card_subtypes('grid monitor', ['Construct']).
card_colors('grid monitor', []).
card_text('grid monitor', 'You can\'t cast creature spells.').
card_mana_cost('grid monitor', ['4']).
card_cmc('grid monitor', 4).
card_layout('grid monitor', 'normal').
card_power('grid monitor', 4).
card_toughness('grid monitor', 6).

% Found in: GTC
card_name('gridlock', 'Gridlock').
card_type('gridlock', 'Instant').
card_types('gridlock', ['Instant']).
card_subtypes('gridlock', []).
card_colors('gridlock', ['U']).
card_text('gridlock', 'Tap X target nonland permanents.').
card_mana_cost('gridlock', ['X', 'U']).
card_cmc('gridlock', 1).
card_layout('gridlock', 'normal').

% Found in: SHM
card_name('grief tyrant', 'Grief Tyrant').
card_type('grief tyrant', 'Creature — Horror').
card_types('grief tyrant', ['Creature']).
card_subtypes('grief tyrant', ['Horror']).
card_colors('grief tyrant', ['B', 'R']).
card_text('grief tyrant', 'Grief Tyrant enters the battlefield with four -1/-1 counters on it.\nWhen Grief Tyrant dies, put a -1/-1 counter on target creature for each -1/-1 counter on Grief Tyrant.').
card_mana_cost('grief tyrant', ['5', 'B/R']).
card_cmc('grief tyrant', 6).
card_layout('grief tyrant', 'normal').
card_power('grief tyrant', 8).
card_toughness('grief tyrant', 8).

% Found in: VIS
card_name('griffin canyon', 'Griffin Canyon').
card_type('griffin canyon', 'Land').
card_types('griffin canyon', ['Land']).
card_subtypes('griffin canyon', []).
card_colors('griffin canyon', []).
card_text('griffin canyon', '{T}: Add {1} to your mana pool.\n{T}: Untap target Griffin. If it\'s a creature, it gets +1/+1 until end of turn.').
card_layout('griffin canyon', 'normal').
card_reserved('griffin canyon').

% Found in: BNG
card_name('griffin dreamfinder', 'Griffin Dreamfinder').
card_type('griffin dreamfinder', 'Creature — Griffin').
card_types('griffin dreamfinder', ['Creature']).
card_subtypes('griffin dreamfinder', ['Griffin']).
card_colors('griffin dreamfinder', ['W']).
card_text('griffin dreamfinder', 'Flying\nWhen Griffin Dreamfinder enters the battlefield, return target enchantment card from your graveyard to your hand.').
card_mana_cost('griffin dreamfinder', ['3', 'W', 'W']).
card_cmc('griffin dreamfinder', 5).
card_layout('griffin dreamfinder', 'normal').
card_power('griffin dreamfinder', 1).
card_toughness('griffin dreamfinder', 4).

% Found in: DDG, DDH, DDL, TSP
card_name('griffin guide', 'Griffin Guide').
card_type('griffin guide', 'Enchantment — Aura').
card_types('griffin guide', ['Enchantment']).
card_subtypes('griffin guide', ['Aura']).
card_colors('griffin guide', ['W']).
card_text('griffin guide', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying.\nWhen enchanted creature dies, put a 2/2 white Griffin creature token with flying onto the battlefield.').
card_mana_cost('griffin guide', ['2', 'W']).
card_cmc('griffin guide', 3).
card_layout('griffin guide', 'normal').

% Found in: M13
card_name('griffin protector', 'Griffin Protector').
card_type('griffin protector', 'Creature — Griffin').
card_types('griffin protector', ['Creature']).
card_subtypes('griffin protector', ['Griffin']).
card_colors('griffin protector', ['W']).
card_text('griffin protector', 'Flying\nWhenever another creature enters the battlefield under your control, Griffin Protector gets +1/+1 until end of turn.').
card_mana_cost('griffin protector', ['3', 'W']).
card_cmc('griffin protector', 4).
card_layout('griffin protector', 'normal').
card_power('griffin protector', 2).
card_toughness('griffin protector', 3).

% Found in: M12
card_name('griffin rider', 'Griffin Rider').
card_type('griffin rider', 'Creature — Human Knight').
card_types('griffin rider', ['Creature']).
card_subtypes('griffin rider', ['Human', 'Knight']).
card_colors('griffin rider', ['W']).
card_text('griffin rider', 'As long as you control a Griffin creature, Griffin Rider gets +3/+3 and has flying.').
card_mana_cost('griffin rider', ['1', 'W']).
card_cmc('griffin rider', 2).
card_layout('griffin rider', 'normal').
card_power('griffin rider', 1).
card_toughness('griffin rider', 1).

% Found in: M10, M12, M14
card_name('griffin sentinel', 'Griffin Sentinel').
card_type('griffin sentinel', 'Creature — Griffin').
card_types('griffin sentinel', ['Creature']).
card_subtypes('griffin sentinel', ['Griffin']).
card_colors('griffin sentinel', ['W']).
card_text('griffin sentinel', 'Flying\nVigilance (Attacking doesn\'t cause this creature to tap.)').
card_mana_cost('griffin sentinel', ['2', 'W']).
card_cmc('griffin sentinel', 3).
card_layout('griffin sentinel', 'normal').
card_power('griffin sentinel', 1).
card_toughness('griffin sentinel', 3).

% Found in: RAV
card_name('grifter\'s blade', 'Grifter\'s Blade').
card_type('grifter\'s blade', 'Artifact — Equipment').
card_types('grifter\'s blade', ['Artifact']).
card_subtypes('grifter\'s blade', ['Equipment']).
card_colors('grifter\'s blade', []).
card_text('grifter\'s blade', 'Flash\nAs Grifter\'s Blade enters the battlefield, choose a creature you control it could be attached to. If you do, it enters the battlefield attached to that creature.\nEquipped creature gets +1/+1.\nEquip {1}').
card_mana_cost('grifter\'s blade', ['3']).
card_cmc('grifter\'s blade', 3).
card_layout('grifter\'s blade', 'normal').

% Found in: MM2, NPH
card_name('grim affliction', 'Grim Affliction').
card_type('grim affliction', 'Instant').
card_types('grim affliction', ['Instant']).
card_subtypes('grim affliction', []).
card_colors('grim affliction', ['B']).
card_text('grim affliction', 'Put a -1/-1 counter on target creature, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('grim affliction', ['2', 'B']).
card_cmc('grim affliction', 3).
card_layout('grim affliction', 'normal').

% Found in: C13, DKA
card_name('grim backwoods', 'Grim Backwoods').
card_type('grim backwoods', 'Land').
card_types('grim backwoods', ['Land']).
card_subtypes('grim backwoods', []).
card_colors('grim backwoods', []).
card_text('grim backwoods', '{T}: Add {1} to your mana pool.\n{2}{B}{G}, {T}, Sacrifice a creature: Draw a card.').
card_layout('grim backwoods', 'normal').

% Found in: FRF
card_name('grim contest', 'Grim Contest').
card_type('grim contest', 'Instant').
card_types('grim contest', ['Instant']).
card_subtypes('grim contest', []).
card_colors('grim contest', ['B', 'G']).
card_text('grim contest', 'Choose target creature you control and target creature an opponent controls. Each of those creatures deals damage equal to its toughness to the other.').
card_mana_cost('grim contest', ['1', 'B', 'G']).
card_cmc('grim contest', 3).
card_layout('grim contest', 'normal').

% Found in: ZEN
card_name('grim discovery', 'Grim Discovery').
card_type('grim discovery', 'Sorcery').
card_types('grim discovery', ['Sorcery']).
card_subtypes('grim discovery', []).
card_colors('grim discovery', ['B']).
card_text('grim discovery', 'Choose one or both —\n• Return target creature card from your graveyard to your hand.\n• Return target land card from your graveyard to your hand.').
card_mana_cost('grim discovery', ['1', 'B']).
card_cmc('grim discovery', 2).
card_layout('grim discovery', 'normal').

% Found in: MIR
card_name('grim feast', 'Grim Feast').
card_type('grim feast', 'Enchantment').
card_types('grim feast', ['Enchantment']).
card_subtypes('grim feast', []).
card_colors('grim feast', ['B', 'G']).
card_text('grim feast', 'At the beginning of your upkeep, Grim Feast deals 1 damage to you.\nWhenever a creature is put into an opponent\'s graveyard from the battlefield, you gain life equal to its toughness.').
card_mana_cost('grim feast', ['1', 'B', 'G']).
card_cmc('grim feast', 3).
card_layout('grim feast', 'normal').
card_reserved('grim feast').

% Found in: C14, DDJ, DKA
card_name('grim flowering', 'Grim Flowering').
card_type('grim flowering', 'Sorcery').
card_types('grim flowering', ['Sorcery']).
card_subtypes('grim flowering', []).
card_colors('grim flowering', ['G']).
card_text('grim flowering', 'Draw a card for each creature card in your graveyard.').
card_mana_cost('grim flowering', ['5', 'G']).
card_cmc('grim flowering', 6).
card_layout('grim flowering', 'normal').

% Found in: JOU
card_name('grim guardian', 'Grim Guardian').
card_type('grim guardian', 'Enchantment Creature — Zombie').
card_types('grim guardian', ['Enchantment', 'Creature']).
card_subtypes('grim guardian', ['Zombie']).
card_colors('grim guardian', ['B']).
card_text('grim guardian', 'Constellation — Whenever Grim Guardian or another enchantment enters the battlefield under your control, each opponent loses 1 life.').
card_mana_cost('grim guardian', ['2', 'B']).
card_cmc('grim guardian', 3).
card_layout('grim guardian', 'normal').
card_power('grim guardian', 1).
card_toughness('grim guardian', 4).

% Found in: FRF_UGIN, KTK, pPRE
card_name('grim haruspex', 'Grim Haruspex').
card_type('grim haruspex', 'Creature — Human Wizard').
card_types('grim haruspex', ['Creature']).
card_subtypes('grim haruspex', ['Human', 'Wizard']).
card_colors('grim haruspex', ['B']).
card_text('grim haruspex', 'Morph {B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhenever another nontoken creature you control dies, draw a card.').
card_mana_cost('grim haruspex', ['2', 'B']).
card_cmc('grim haruspex', 3).
card_layout('grim haruspex', 'normal').
card_power('grim haruspex', 3).
card_toughness('grim haruspex', 2).

% Found in: CSP
card_name('grim harvest', 'Grim Harvest').
card_type('grim harvest', 'Instant').
card_types('grim harvest', ['Instant']).
card_subtypes('grim harvest', []).
card_colors('grim harvest', ['B']).
card_text('grim harvest', 'Return target creature card from your graveyard to your hand.\nRecover {2}{B} (When a creature is put into your graveyard from the battlefield, you may pay {2}{B}. If you do, return this card from your graveyard to your hand. Otherwise, exile this card.)').
card_mana_cost('grim harvest', ['1', 'B']).
card_cmc('grim harvest', 2).
card_layout('grim harvest', 'normal').

% Found in: M12, PD2, TOR, pJGP
card_name('grim lavamancer', 'Grim Lavamancer').
card_type('grim lavamancer', 'Creature — Human Wizard').
card_types('grim lavamancer', ['Creature']).
card_subtypes('grim lavamancer', ['Human', 'Wizard']).
card_colors('grim lavamancer', ['R']).
card_text('grim lavamancer', '{R}, {T}, Exile two cards from your graveyard: Grim Lavamancer deals 2 damage to target creature or player.').
card_mana_cost('grim lavamancer', ['R']).
card_cmc('grim lavamancer', 1).
card_layout('grim lavamancer', 'normal').
card_power('grim lavamancer', 1).
card_toughness('grim lavamancer', 1).

% Found in: ULG
card_name('grim monolith', 'Grim Monolith').
card_type('grim monolith', 'Artifact').
card_types('grim monolith', ['Artifact']).
card_subtypes('grim monolith', []).
card_colors('grim monolith', []).
card_text('grim monolith', 'Grim Monolith doesn\'t untap during your untap step.\n{T}: Add {3} to your mana pool.\n{4}: Untap Grim Monolith.').
card_mana_cost('grim monolith', ['2']).
card_cmc('grim monolith', 2).
card_layout('grim monolith', 'normal').
card_reserved('grim monolith').

% Found in: SHM
card_name('grim poppet', 'Grim Poppet').
card_type('grim poppet', 'Artifact Creature — Scarecrow').
card_types('grim poppet', ['Artifact', 'Creature']).
card_subtypes('grim poppet', ['Scarecrow']).
card_colors('grim poppet', []).
card_text('grim poppet', 'Grim Poppet enters the battlefield with three -1/-1 counters on it.\nRemove a -1/-1 counter from Grim Poppet: Put a -1/-1 counter on another target creature.').
card_mana_cost('grim poppet', ['7']).
card_cmc('grim poppet', 7).
card_layout('grim poppet', 'normal').
card_power('grim poppet', 4).
card_toughness('grim poppet', 4).

% Found in: MRD
card_name('grim reminder', 'Grim Reminder').
card_type('grim reminder', 'Instant').
card_types('grim reminder', ['Instant']).
card_subtypes('grim reminder', []).
card_colors('grim reminder', ['B']).
card_text('grim reminder', 'Search your library for a nonland card and reveal it. Each opponent who cast a card this turn with the same name as that card loses 6 life. Then shuffle your library.\n{B}{B}: Return Grim Reminder from your graveyard to your hand. Activate this ability only during your upkeep.').
card_mana_cost('grim reminder', ['2', 'B']).
card_cmc('grim reminder', 3).
card_layout('grim reminder', 'normal').

% Found in: M14
card_name('grim return', 'Grim Return').
card_type('grim return', 'Instant').
card_types('grim return', ['Instant']).
card_subtypes('grim return', []).
card_colors('grim return', ['B']).
card_text('grim return', 'Choose target creature card in a graveyard that was put there from the battlefield this turn. Put that card onto the battlefield under your control.').
card_mana_cost('grim return', ['2', 'B']).
card_cmc('grim return', 3).
card_layout('grim return', 'normal').

% Found in: RTR
card_name('grim roustabout', 'Grim Roustabout').
card_type('grim roustabout', 'Creature — Skeleton Warrior').
card_types('grim roustabout', ['Creature']).
card_subtypes('grim roustabout', ['Skeleton', 'Warrior']).
card_colors('grim roustabout', ['B']).
card_text('grim roustabout', 'Unleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\n{1}{B}: Regenerate Grim Roustabout.').
card_mana_cost('grim roustabout', ['1', 'B']).
card_cmc('grim roustabout', 2).
card_layout('grim roustabout', 'normal').
card_power('grim roustabout', 1).
card_toughness('grim roustabout', 1).

% Found in: ME3, S99
card_name('grim tutor', 'Grim Tutor').
card_type('grim tutor', 'Sorcery').
card_types('grim tutor', ['Sorcery']).
card_subtypes('grim tutor', []).
card_colors('grim tutor', ['B']).
card_text('grim tutor', 'Search your library for a card and put that card into your hand, then shuffle your library. You lose 3 life.').
card_mana_cost('grim tutor', ['1', 'B', 'B']).
card_cmc('grim tutor', 3).
card_layout('grim tutor', 'normal').

% Found in: DST
card_name('grimclaw bats', 'Grimclaw Bats').
card_type('grimclaw bats', 'Creature — Bat').
card_types('grimclaw bats', ['Creature']).
card_subtypes('grimclaw bats', ['Bat']).
card_colors('grimclaw bats', ['B']).
card_text('grimclaw bats', 'Flying\n{B}, Pay 1 life: Grimclaw Bats gets +1/+1 until end of turn.').
card_mana_cost('grimclaw bats', ['1', 'B']).
card_cmc('grimclaw bats', 2).
card_layout('grimclaw bats', 'normal').
card_power('grimclaw bats', 1).
card_toughness('grimclaw bats', 1).

% Found in: ISD
card_name('grimgrin, corpse-born', 'Grimgrin, Corpse-Born').
card_type('grimgrin, corpse-born', 'Legendary Creature — Zombie Warrior').
card_types('grimgrin, corpse-born', ['Creature']).
card_subtypes('grimgrin, corpse-born', ['Zombie', 'Warrior']).
card_supertypes('grimgrin, corpse-born', ['Legendary']).
card_colors('grimgrin, corpse-born', ['U', 'B']).
card_text('grimgrin, corpse-born', 'Grimgrin, Corpse-Born enters the battlefield tapped and doesn\'t untap during your untap step.\nSacrifice another creature: Untap Grimgrin and put a +1/+1 counter on it.\nWhenever Grimgrin attacks, destroy target creature defending player controls, then put a +1/+1 counter on Grimgrin.').
card_mana_cost('grimgrin, corpse-born', ['3', 'U', 'B']).
card_cmc('grimgrin, corpse-born', 5).
card_layout('grimgrin, corpse-born', 'normal').
card_power('grimgrin, corpse-born', 5).
card_toughness('grimgrin, corpse-born', 5).

% Found in: ISD
card_name('grimoire of the dead', 'Grimoire of the Dead').
card_type('grimoire of the dead', 'Legendary Artifact').
card_types('grimoire of the dead', ['Artifact']).
card_subtypes('grimoire of the dead', []).
card_supertypes('grimoire of the dead', ['Legendary']).
card_colors('grimoire of the dead', []).
card_text('grimoire of the dead', '{1}, {T}, Discard a card: Put a study counter on Grimoire of the Dead.\n{T}, Remove three study counters from Grimoire of the Dead and sacrifice it: Put all creature cards from all graveyards onto the battlefield under your control. They\'re black Zombies in addition to their other colors and types.').
card_mana_cost('grimoire of the dead', ['4']).
card_cmc('grimoire of the dead', 4).
card_layout('grimoire of the dead', 'normal').

% Found in: MOR
card_name('grimoire thief', 'Grimoire Thief').
card_type('grimoire thief', 'Creature — Merfolk Rogue').
card_types('grimoire thief', ['Creature']).
card_subtypes('grimoire thief', ['Merfolk', 'Rogue']).
card_colors('grimoire thief', ['U']).
card_text('grimoire thief', 'Whenever Grimoire Thief becomes tapped, exile the top three cards of target opponent\'s library face down.\nYou may look at cards exiled with Grimoire Thief.\n{U}, Sacrifice Grimoire Thief: Turn all cards exiled with Grimoire Thief face up. Counter all spells with those names.').
card_mana_cost('grimoire thief', ['U', 'U']).
card_cmc('grimoire thief', 2).
card_layout('grimoire thief', 'normal').
card_power('grimoire thief', 2).
card_toughness('grimoire thief', 2).

% Found in: M15, SOM
card_name('grindclock', 'Grindclock').
card_type('grindclock', 'Artifact').
card_types('grindclock', ['Artifact']).
card_subtypes('grindclock', []).
card_colors('grindclock', []).
card_text('grindclock', '{T}: Put a charge counter on Grindclock.\n{T}: Target player puts the top X cards of his or her library into his or her graveyard, where X is the number of charge counters on Grindclock.').
card_mana_cost('grindclock', ['2']).
card_cmc('grindclock', 2).
card_layout('grindclock', 'normal').

% Found in: 5DN
card_name('grinding station', 'Grinding Station').
card_type('grinding station', 'Artifact').
card_types('grinding station', ['Artifact']).
card_subtypes('grinding station', []).
card_colors('grinding station', []).
card_text('grinding station', '{T}, Sacrifice an artifact: Target player puts the top three cards of his or her library into his or her graveyard.\nWhenever an artifact enters the battlefield, you may untap Grinding Station.').
card_mana_cost('grinding station', ['2']).
card_cmc('grinding station', 2).
card_layout('grinding station', 'normal').

% Found in: TMP, TPR
card_name('grindstone', 'Grindstone').
card_type('grindstone', 'Artifact').
card_types('grindstone', ['Artifact']).
card_subtypes('grindstone', []).
card_colors('grindstone', []).
card_text('grindstone', '{3}, {T}: Target player puts the top two cards of his or her library into his or her graveyard. If both cards share a color, repeat this process.').
card_mana_cost('grindstone', ['1']).
card_cmc('grindstone', 1).
card_layout('grindstone', 'normal').

% Found in: ONS
card_name('grinning demon', 'Grinning Demon').
card_type('grinning demon', 'Creature — Demon').
card_types('grinning demon', ['Creature']).
card_subtypes('grinning demon', ['Demon']).
card_colors('grinning demon', ['B']).
card_text('grinning demon', 'At the beginning of your upkeep, you lose 2 life.\nMorph {2}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('grinning demon', ['2', 'B', 'B']).
card_cmc('grinning demon', 4).
card_layout('grinning demon', 'normal').
card_power('grinning demon', 6).
card_toughness('grinning demon', 6).

% Found in: VAN
card_name('grinning demon avatar', 'Grinning Demon Avatar').
card_type('grinning demon avatar', 'Vanguard').
card_types('grinning demon avatar', ['Vanguard']).
card_subtypes('grinning demon avatar', []).
card_colors('grinning demon avatar', []).
card_text('grinning demon avatar', 'Whenever a nontoken creature you control dies, target opponent discards a card.').
card_layout('grinning demon avatar', 'vanguard').

% Found in: FUT, MMA
card_name('grinning ignus', 'Grinning Ignus').
card_type('grinning ignus', 'Creature — Elemental').
card_types('grinning ignus', ['Creature']).
card_subtypes('grinning ignus', ['Elemental']).
card_colors('grinning ignus', ['R']).
card_text('grinning ignus', '{R}, Return Grinning Ignus to its owner\'s hand: Add {2}{R} to your mana pool. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('grinning ignus', ['2', 'R']).
card_cmc('grinning ignus', 3).
card_layout('grinning ignus', 'normal').
card_power('grinning ignus', 2).
card_toughness('grinning ignus', 2).

% Found in: 6ED, MIR, TSB
card_name('grinning totem', 'Grinning Totem').
card_type('grinning totem', 'Artifact').
card_types('grinning totem', ['Artifact']).
card_subtypes('grinning totem', []).
card_colors('grinning totem', []).
card_text('grinning totem', '{2}, {T}, Sacrifice Grinning Totem: Search target opponent\'s library for a card and exile it. Then that player shuffles his or her library. Until the beginning of your next upkeep, you may play that card. At the beginning of your next upkeep, if you haven\'t played it, put it into its owner\'s graveyard.').
card_mana_cost('grinning totem', ['4']).
card_cmc('grinning totem', 4).
card_layout('grinning totem', 'normal').

% Found in: JUD
card_name('grip of amnesia', 'Grip of Amnesia').
card_type('grip of amnesia', 'Instant').
card_types('grip of amnesia', ['Instant']).
card_subtypes('grip of amnesia', []).
card_colors('grip of amnesia', ['U']).
card_text('grip of amnesia', 'Counter target spell unless its controller exiles all cards from his or her graveyard.\nDraw a card.').
card_mana_cost('grip of amnesia', ['1', 'U']).
card_cmc('grip of amnesia', 2).
card_layout('grip of amnesia', 'normal').

% Found in: SCG
card_name('grip of chaos', 'Grip of Chaos').
card_type('grip of chaos', 'Enchantment').
card_types('grip of chaos', ['Enchantment']).
card_subtypes('grip of chaos', []).
card_colors('grip of chaos', ['R']).
card_text('grip of chaos', 'Whenever a spell or ability is put onto the stack, if it has a single target, reselect its target at random. (Select from among all legal targets.)').
card_mana_cost('grip of chaos', ['4', 'R', 'R']).
card_cmc('grip of chaos', 6).
card_layout('grip of chaos', 'normal').

% Found in: BFZ
card_name('grip of desolation', 'Grip of Desolation').
card_type('grip of desolation', 'Instant').
card_types('grip of desolation', ['Instant']).
card_subtypes('grip of desolation', []).
card_colors('grip of desolation', []).
card_text('grip of desolation', 'Devoid (This card has no color.)\nExile target creature and target land.').
card_mana_cost('grip of desolation', ['4', 'B', 'B']).
card_cmc('grip of desolation', 6).
card_layout('grip of desolation', 'normal').

% Found in: DDM, DKA, THS
card_name('griptide', 'Griptide').
card_type('griptide', 'Instant').
card_types('griptide', ['Instant']).
card_subtypes('griptide', []).
card_colors('griptide', ['U']).
card_text('griptide', 'Put target creature on top of its owner\'s library.').
card_mana_cost('griptide', ['3', 'U']).
card_cmc('griptide', 4).
card_layout('griptide', 'normal').

% Found in: AVR, pGPX
card_name('griselbrand', 'Griselbrand').
card_type('griselbrand', 'Legendary Creature — Demon').
card_types('griselbrand', ['Creature']).
card_subtypes('griselbrand', ['Demon']).
card_supertypes('griselbrand', ['Legendary']).
card_colors('griselbrand', ['B']).
card_text('griselbrand', 'Flying, lifelink\nPay 7 life: Draw seven cards.').
card_mana_cost('griselbrand', ['4', 'B', 'B', 'B', 'B']).
card_cmc('griselbrand', 8).
card_layout('griselbrand', 'normal').
card_power('griselbrand', 7).
card_toughness('griselbrand', 7).

% Found in: RTR, pFNM
card_name('grisly salvage', 'Grisly Salvage').
card_type('grisly salvage', 'Instant').
card_types('grisly salvage', ['Instant']).
card_subtypes('grisly salvage', []).
card_colors('grisly salvage', ['B', 'G']).
card_text('grisly salvage', 'Reveal the top five cards of your library. You may put a creature or land card from among them into your hand. Put the rest into your graveyard.').
card_mana_cost('grisly salvage', ['B', 'G']).
card_cmc('grisly salvage', 2).
card_layout('grisly salvage', 'normal').

% Found in: DDM, GTC
card_name('grisly spectacle', 'Grisly Spectacle').
card_type('grisly spectacle', 'Instant').
card_types('grisly spectacle', ['Instant']).
card_subtypes('grisly spectacle', []).
card_colors('grisly spectacle', ['B']).
card_text('grisly spectacle', 'Destroy target nonartifact creature. Its controller puts a number of cards equal to that creature\'s power from the top of his or her library into his or her graveyard.').
card_mana_cost('grisly spectacle', ['2', 'B', 'B']).
card_cmc('grisly spectacle', 4).
card_layout('grisly spectacle', 'normal').

% Found in: BNG
card_name('grisly transformation', 'Grisly Transformation').
card_type('grisly transformation', 'Enchantment — Aura').
card_types('grisly transformation', ['Enchantment']).
card_subtypes('grisly transformation', ['Aura']).
card_colors('grisly transformation', ['B']).
card_text('grisly transformation', 'Enchant creature\nWhen Grisly Transformation enters the battlefield, draw a card.\nEnchanted creature has intimidate. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('grisly transformation', ['2', 'B']).
card_cmc('grisly transformation', 3).
card_layout('grisly transformation', 'normal').

% Found in: CSP
card_name('gristle grinner', 'Gristle Grinner').
card_type('gristle grinner', 'Creature — Zombie').
card_types('gristle grinner', ['Creature']).
card_subtypes('gristle grinner', ['Zombie']).
card_colors('gristle grinner', ['B']).
card_text('gristle grinner', 'Whenever a creature dies, Gristle Grinner gets +2/+2 until end of turn.').
card_mana_cost('gristle grinner', ['4', 'B']).
card_cmc('gristle grinner', 5).
card_layout('gristle grinner', 'normal').
card_power('gristle grinner', 3).
card_toughness('gristle grinner', 3).

% Found in: GPT
card_name('gristleback', 'Gristleback').
card_type('gristleback', 'Creature — Boar Beast').
card_types('gristleback', ['Creature']).
card_subtypes('gristleback', ['Boar', 'Beast']).
card_colors('gristleback', ['G']).
card_text('gristleback', 'Bloodthirst 1 (If an opponent was dealt damage this turn, this creature enters the battlefield with a +1/+1 counter on it.)\nSacrifice Gristleback: You gain life equal to Gristleback\'s power.').
card_mana_cost('gristleback', ['2', 'G']).
card_cmc('gristleback', 3).
card_layout('gristleback', 'normal').
card_power('gristleback', 2).
card_toughness('gristleback', 2).

% Found in: HOP
card_name('grixis', 'Grixis').
card_type('grixis', 'Plane — Alara').
card_types('grixis', ['Plane']).
card_subtypes('grixis', ['Alara']).
card_colors('grixis', []).
card_text('grixis', 'Blue, black, and/or red creature cards in your graveyard have unearth. The unearth cost is equal to the card\'s mana cost. (Pay the card\'s mana cost: Return it to the battlefield. The creature gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)\nWhenever you roll {C}, put target creature card from a graveyard onto the battlefield under your control.').
card_layout('grixis', 'plane').

% Found in: ALA
card_name('grixis battlemage', 'Grixis Battlemage').
card_type('grixis battlemage', 'Creature — Human Wizard').
card_types('grixis battlemage', ['Creature']).
card_subtypes('grixis battlemage', ['Human', 'Wizard']).
card_colors('grixis battlemage', ['B']).
card_text('grixis battlemage', '{U}, {T}: Draw a card, then discard a card.\n{R}, {T}: Target creature can\'t block this turn.').
card_mana_cost('grixis battlemage', ['2', 'B']).
card_cmc('grixis battlemage', 3).
card_layout('grixis battlemage', 'normal').
card_power('grixis battlemage', 2).
card_toughness('grixis battlemage', 2).

% Found in: ALA, C13, DDH
card_name('grixis charm', 'Grixis Charm').
card_type('grixis charm', 'Instant').
card_types('grixis charm', ['Instant']).
card_subtypes('grixis charm', []).
card_colors('grixis charm', ['U', 'B', 'R']).
card_text('grixis charm', 'Choose one —\n• Return target permanent to its owner\'s hand.\n• Target creature gets -4/-4 until end of turn.\n• Creatures you control get +2/+0 until end of turn.').
card_mana_cost('grixis charm', ['U', 'B', 'R']).
card_cmc('grixis charm', 3).
card_layout('grixis charm', 'normal').

% Found in: ARB
card_name('grixis grimblade', 'Grixis Grimblade').
card_type('grixis grimblade', 'Creature — Zombie Warrior').
card_types('grixis grimblade', ['Creature']).
card_subtypes('grixis grimblade', ['Zombie', 'Warrior']).
card_colors('grixis grimblade', ['U', 'B', 'R']).
card_text('grixis grimblade', 'As long as you control another multicolored permanent, Grixis Grimblade gets +1/+1 and has deathtouch. (Any amount of damage it deals to a creature is enough to destroy that creature.)').
card_mana_cost('grixis grimblade', ['U/R', 'B']).
card_cmc('grixis grimblade', 2).
card_layout('grixis grimblade', 'normal').
card_power('grixis grimblade', 2).
card_toughness('grixis grimblade', 1).

% Found in: CNS, CON
card_name('grixis illusionist', 'Grixis Illusionist').
card_type('grixis illusionist', 'Creature — Human Wizard').
card_types('grixis illusionist', ['Creature']).
card_subtypes('grixis illusionist', ['Human', 'Wizard']).
card_colors('grixis illusionist', ['U']).
card_text('grixis illusionist', '{T}: Target land you control becomes the basic land type of your choice until end of turn.').
card_mana_cost('grixis illusionist', ['U']).
card_cmc('grixis illusionist', 1).
card_layout('grixis illusionist', 'normal').
card_power('grixis illusionist', 1).
card_toughness('grixis illusionist', 1).

% Found in: ALA, C13
card_name('grixis panorama', 'Grixis Panorama').
card_type('grixis panorama', 'Land').
card_types('grixis panorama', ['Land']).
card_subtypes('grixis panorama', []).
card_colors('grixis panorama', []).
card_text('grixis panorama', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Grixis Panorama: Search your library for a basic Island, Swamp, or Mountain card and put it onto the battlefield tapped. Then shuffle your library.').
card_layout('grixis panorama', 'normal').

% Found in: CON
card_name('grixis slavedriver', 'Grixis Slavedriver').
card_type('grixis slavedriver', 'Creature — Zombie Giant').
card_types('grixis slavedriver', ['Creature']).
card_subtypes('grixis slavedriver', ['Zombie', 'Giant']).
card_colors('grixis slavedriver', ['B']).
card_text('grixis slavedriver', 'When Grixis Slavedriver leaves the battlefield, put a 2/2 black Zombie creature token onto the battlefield.\nUnearth {3}{B} ({3}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('grixis slavedriver', ['5', 'B']).
card_cmc('grixis slavedriver', 6).
card_layout('grixis slavedriver', 'normal').
card_power('grixis slavedriver', 4).
card_toughness('grixis slavedriver', 4).

% Found in: ARB
card_name('grixis sojourners', 'Grixis Sojourners').
card_type('grixis sojourners', 'Creature — Zombie Ogre').
card_types('grixis sojourners', ['Creature']).
card_subtypes('grixis sojourners', ['Zombie', 'Ogre']).
card_colors('grixis sojourners', ['U', 'B', 'R']).
card_text('grixis sojourners', 'When you cycle Grixis Sojourners or it dies, you may exile target card from a graveyard.\nCycling {2}{B} ({2}{B}, Discard this card: Draw a card.)').
card_mana_cost('grixis sojourners', ['1', 'U', 'B', 'R']).
card_cmc('grixis sojourners', 4).
card_layout('grixis sojourners', 'normal').
card_power('grixis sojourners', 4).
card_toughness('grixis sojourners', 3).

% Found in: ARB
card_name('grizzled leotau', 'Grizzled Leotau').
card_type('grizzled leotau', 'Creature — Cat').
card_types('grizzled leotau', ['Creature']).
card_subtypes('grizzled leotau', ['Cat']).
card_colors('grizzled leotau', ['W', 'G']).
card_text('grizzled leotau', '').
card_mana_cost('grizzled leotau', ['G', 'W']).
card_cmc('grizzled leotau', 2).
card_layout('grizzled leotau', 'normal').
card_power('grizzled leotau', 1).
card_toughness('grizzled leotau', 5).

% Found in: ISD
card_name('grizzled outcasts', 'Grizzled Outcasts').
card_type('grizzled outcasts', 'Creature — Human Werewolf').
card_types('grizzled outcasts', ['Creature']).
card_subtypes('grizzled outcasts', ['Human', 'Werewolf']).
card_colors('grizzled outcasts', ['G']).
card_text('grizzled outcasts', 'At the beginning of each upkeep, if no spells were cast last turn, transform Grizzled Outcasts.').
card_mana_cost('grizzled outcasts', ['4', 'G']).
card_cmc('grizzled outcasts', 5).
card_layout('grizzled outcasts', 'double-faced').
card_power('grizzled outcasts', 4).
card_toughness('grizzled outcasts', 4).
card_sides('grizzled outcasts', 'krallenhorde wantons').

% Found in: ICE
card_name('grizzled wolverine', 'Grizzled Wolverine').
card_type('grizzled wolverine', 'Creature — Wolverine').
card_types('grizzled wolverine', ['Creature']).
card_subtypes('grizzled wolverine', ['Wolverine']).
card_colors('grizzled wolverine', ['R']).
card_text('grizzled wolverine', '{R}: Grizzled Wolverine gets +2/+0 until end of turn. Activate this ability only during the declare blockers step, only if at least one creature is blocking Grizzled Wolverine, and only once each turn.').
card_mana_cost('grizzled wolverine', ['1', 'R', 'R']).
card_cmc('grizzled wolverine', 3).
card_layout('grizzled wolverine', 'normal').
card_power('grizzled wolverine', 2).
card_toughness('grizzled wolverine', 2).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ITP, LEA, LEB, POR, RQS, S99
card_name('grizzly bears', 'Grizzly Bears').
card_type('grizzly bears', 'Creature — Bear').
card_types('grizzly bears', ['Creature']).
card_subtypes('grizzly bears', ['Bear']).
card_colors('grizzly bears', ['G']).
card_text('grizzly bears', '').
card_mana_cost('grizzly bears', ['1', 'G']).
card_cmc('grizzly bears', 2).
card_layout('grizzly bears', 'normal').
card_power('grizzly bears', 2).
card_toughness('grizzly bears', 2).

% Found in: JUD, VMA
card_name('grizzly fate', 'Grizzly Fate').
card_type('grizzly fate', 'Sorcery').
card_types('grizzly fate', ['Sorcery']).
card_subtypes('grizzly fate', []).
card_colors('grizzly fate', ['G']).
card_text('grizzly fate', 'Put two 2/2 green Bear creature tokens onto the battlefield.\nThreshold — Put four 2/2 green Bear creature tokens onto the battlefield instead if seven or more cards are in your graveyard.\nFlashback {5}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('grizzly fate', ['3', 'G', 'G']).
card_cmc('grizzly fate', 5).
card_layout('grizzly fate', 'normal').

% Found in: MRD
card_name('groffskithur', 'Groffskithur').
card_type('groffskithur', 'Creature — Beast').
card_types('groffskithur', ['Creature']).
card_subtypes('groffskithur', ['Beast']).
card_colors('groffskithur', ['G']).
card_text('groffskithur', 'Whenever Groffskithur becomes blocked, you may return target card named Groffskithur from your graveyard to your hand.').
card_mana_cost('groffskithur', ['5', 'G']).
card_cmc('groffskithur', 6).
card_layout('groffskithur', 'normal').
card_power('groffskithur', 3).
card_toughness('groffskithur', 3).

% Found in: EXO
card_name('grollub', 'Grollub').
card_type('grollub', 'Creature — Beast').
card_types('grollub', ['Creature']).
card_subtypes('grollub', ['Beast']).
card_colors('grollub', ['B']).
card_text('grollub', 'Whenever Grollub is dealt damage, each opponent gains that much life.').
card_mana_cost('grollub', ['2', 'B']).
card_cmc('grollub', 3).
card_layout('grollub', 'normal').
card_power('grollub', 3).
card_toughness('grollub', 3).

% Found in: ROE
card_name('grotag siege-runner', 'Grotag Siege-Runner').
card_type('grotag siege-runner', 'Creature — Goblin Rogue').
card_types('grotag siege-runner', ['Creature']).
card_subtypes('grotag siege-runner', ['Goblin', 'Rogue']).
card_colors('grotag siege-runner', ['R']).
card_text('grotag siege-runner', '{R}, Sacrifice Grotag Siege-Runner: Destroy target creature with defender. Grotag Siege-Runner deals 2 damage to that creature\'s controller.').
card_mana_cost('grotag siege-runner', ['1', 'R']).
card_cmc('grotag siege-runner', 2).
card_layout('grotag siege-runner', 'normal').
card_power('grotag siege-runner', 2).
card_toughness('grotag siege-runner', 1).

% Found in: WWK
card_name('grotag thrasher', 'Grotag Thrasher').
card_type('grotag thrasher', 'Creature — Lizard').
card_types('grotag thrasher', ['Creature']).
card_subtypes('grotag thrasher', ['Lizard']).
card_colors('grotag thrasher', ['R']).
card_text('grotag thrasher', 'Whenever Grotag Thrasher attacks, target creature can\'t block this turn.').
card_mana_cost('grotag thrasher', ['4', 'R']).
card_cmc('grotag thrasher', 5).
card_layout('grotag thrasher', 'normal').
card_power('grotag thrasher', 3).
card_toughness('grotag thrasher', 3).

% Found in: TOR
card_name('grotesque hybrid', 'Grotesque Hybrid').
card_type('grotesque hybrid', 'Creature — Zombie').
card_types('grotesque hybrid', ['Creature']).
card_subtypes('grotesque hybrid', ['Zombie']).
card_colors('grotesque hybrid', ['B']).
card_text('grotesque hybrid', 'Whenever Grotesque Hybrid deals combat damage to a creature, destroy that creature. It can\'t be regenerated.\nDiscard a card: Grotesque Hybrid gains flying and protection from green and from white until end of turn.').
card_mana_cost('grotesque hybrid', ['4', 'B']).
card_cmc('grotesque hybrid', 5).
card_layout('grotesque hybrid', 'normal').
card_power('grotesque hybrid', 3).
card_toughness('grotesque hybrid', 3).

% Found in: GTC
card_name('ground assault', 'Ground Assault').
card_type('ground assault', 'Sorcery').
card_types('ground assault', ['Sorcery']).
card_subtypes('ground assault', []).
card_colors('ground assault', ['R', 'G']).
card_text('ground assault', 'Ground Assault deals damage to target creature equal to the number of lands you control.').
card_mana_cost('ground assault', ['R', 'G']).
card_cmc('ground assault', 2).
card_layout('ground assault', 'normal').

% Found in: TSP
card_name('ground rift', 'Ground Rift').
card_type('ground rift', 'Sorcery').
card_types('ground rift', ['Sorcery']).
card_subtypes('ground rift', []).
card_colors('ground rift', ['R']).
card_text('ground rift', 'Target creature without flying can\'t block this turn.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('ground rift', ['R']).
card_cmc('ground rift', 1).
card_layout('ground rift', 'normal').

% Found in: M13, ODY
card_name('ground seal', 'Ground Seal').
card_type('ground seal', 'Enchantment').
card_types('ground seal', ['Enchantment']).
card_subtypes('ground seal', []).
card_colors('ground seal', ['G']).
card_text('ground seal', 'When Ground Seal enters the battlefield, draw a card.\nCards in graveyards can\'t be the targets of spells or abilities.').
card_mana_cost('ground seal', ['1', 'G']).
card_cmc('ground seal', 2).
card_layout('ground seal', 'normal').

% Found in: PLC, pCMP
card_name('groundbreaker', 'Groundbreaker').
card_type('groundbreaker', 'Creature — Elemental').
card_types('groundbreaker', ['Creature']).
card_subtypes('groundbreaker', ['Elemental']).
card_colors('groundbreaker', ['G']).
card_text('groundbreaker', 'Trample, haste\nAt the beginning of the end step, sacrifice Groundbreaker.').
card_mana_cost('groundbreaker', ['G', 'G', 'G']).
card_cmc('groundbreaker', 3).
card_layout('groundbreaker', 'normal').
card_power('groundbreaker', 6).
card_toughness('groundbreaker', 1).

% Found in: AVR
card_name('grounded', 'Grounded').
card_type('grounded', 'Enchantment — Aura').
card_types('grounded', ['Enchantment']).
card_subtypes('grounded', ['Aura']).
card_colors('grounded', ['G']).
card_text('grounded', 'Enchant creature\nEnchanted creature loses flying.').
card_mana_cost('grounded', ['1', 'G']).
card_cmc('grounded', 2).
card_layout('grounded', 'normal').

% Found in: EVE
card_name('groundling pouncer', 'Groundling Pouncer').
card_type('groundling pouncer', 'Creature — Faerie').
card_types('groundling pouncer', ['Creature']).
card_subtypes('groundling pouncer', ['Faerie']).
card_colors('groundling pouncer', ['U', 'G']).
card_text('groundling pouncer', '{G/U}: Groundling Pouncer gets +1/+3 and gains flying until end of turn. Activate this ability only once each turn and only if an opponent controls a creature with flying.').
card_mana_cost('groundling pouncer', ['1', 'G/U']).
card_cmc('groundling pouncer', 2).
card_layout('groundling pouncer', 'normal').
card_power('groundling pouncer', 2).
card_toughness('groundling pouncer', 1).

% Found in: M14
card_name('groundshaker sliver', 'Groundshaker Sliver').
card_type('groundshaker sliver', 'Creature — Sliver').
card_types('groundshaker sliver', ['Creature']).
card_subtypes('groundshaker sliver', ['Sliver']).
card_colors('groundshaker sliver', ['G']).
card_text('groundshaker sliver', 'Sliver creatures you control have trample. (If a Sliver you control would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('groundshaker sliver', ['6', 'G']).
card_cmc('groundshaker sliver', 7).
card_layout('groundshaker sliver', 'normal').
card_power('groundshaker sliver', 5).
card_toughness('groundshaker sliver', 5).

% Found in: 9ED, MMQ
card_name('groundskeeper', 'Groundskeeper').
card_type('groundskeeper', 'Creature — Human Druid').
card_types('groundskeeper', ['Creature']).
card_subtypes('groundskeeper', ['Human', 'Druid']).
card_colors('groundskeeper', ['G']).
card_text('groundskeeper', '{1}{G}: Return target basic land card from your graveyard to your hand.').
card_mana_cost('groundskeeper', ['G']).
card_cmc('groundskeeper', 1).
card_layout('groundskeeper', 'normal').
card_power('groundskeeper', 1).
card_toughness('groundskeeper', 1).

% Found in: DDP, WWK
card_name('groundswell', 'Groundswell').
card_type('groundswell', 'Instant').
card_types('groundswell', ['Instant']).
card_subtypes('groundswell', []).
card_colors('groundswell', ['G']).
card_text('groundswell', 'Target creature gets +2/+2 until end of turn.\nLandfall — If you had a land enter the battlefield under your control this turn, that creature gets +4/+4 until end of turn instead.').
card_mana_cost('groundswell', ['G']).
card_cmc('groundswell', 1).
card_layout('groundswell', 'normal').

% Found in: FUT, V12
card_name('grove of the burnwillows', 'Grove of the Burnwillows').
card_type('grove of the burnwillows', 'Land').
card_types('grove of the burnwillows', ['Land']).
card_subtypes('grove of the burnwillows', []).
card_colors('grove of the burnwillows', []).
card_text('grove of the burnwillows', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Each opponent gains 1 life.').
card_layout('grove of the burnwillows', 'normal').

% Found in: PC2
card_name('grove of the dreampods', 'Grove of the Dreampods').
card_type('grove of the dreampods', 'Plane — Fabacin').
card_types('grove of the dreampods', ['Plane']).
card_subtypes('grove of the dreampods', ['Fabacin']).
card_colors('grove of the dreampods', []).
card_text('grove of the dreampods', 'When you planeswalk to Grove of the Dreampods or at the beginning of your upkeep, reveal cards from the top of your library until you reveal a creature card. Put that card onto the battlefield and the rest on the bottom of your library in a random order.\nWhenever you roll {C}, return target creature card from your graveyard to the battlefield.').
card_layout('grove of the dreampods', 'plane').

% Found in: RTR, pPRE
card_name('grove of the guardian', 'Grove of the Guardian').
card_type('grove of the guardian', 'Land').
card_types('grove of the guardian', ['Land']).
card_subtypes('grove of the guardian', []).
card_colors('grove of the guardian', []).
card_text('grove of the guardian', '{T}: Add {1} to your mana pool.\n{3}{G}{W}, {T}, Tap two untapped creatures you control, Sacrifice Grove of the Guardian: Put an 8/8 green and white Elemental creature token with vigilance onto the battlefield.').
card_layout('grove of the guardian', 'normal').

% Found in: BFZ
card_name('grove rumbler', 'Grove Rumbler').
card_type('grove rumbler', 'Creature — Elemental').
card_types('grove rumbler', ['Creature']).
card_subtypes('grove rumbler', ['Elemental']).
card_colors('grove rumbler', ['R', 'G']).
card_text('grove rumbler', 'Trample\nLandfall — Whenever a land enters the battlefield under your control, Grove Rumbler gets +2/+2 until end of turn.').
card_mana_cost('grove rumbler', ['2', 'R', 'G']).
card_cmc('grove rumbler', 4).
card_layout('grove rumbler', 'normal').
card_power('grove rumbler', 3).
card_toughness('grove rumbler', 3).

% Found in: BFZ
card_name('grovetender druids', 'Grovetender Druids').
card_type('grovetender druids', 'Creature — Elf Druid Ally').
card_types('grovetender druids', ['Creature']).
card_subtypes('grovetender druids', ['Elf', 'Druid', 'Ally']).
card_colors('grovetender druids', ['W', 'G']).
card_text('grovetender druids', 'Rally — Whenever Grovetender Druids or another Ally enters the battlefield under your control, you may pay {1}. If you do, put a 1/1 green Plant creature token onto the battlefield.').
card_mana_cost('grovetender druids', ['2', 'G', 'W']).
card_cmc('grovetender druids', 4).
card_layout('grovetender druids', 'normal').
card_power('grovetender druids', 3).
card_toughness('grovetender druids', 3).

% Found in: RTR
card_name('growing ranks', 'Growing Ranks').
card_type('growing ranks', 'Enchantment').
card_types('growing ranks', ['Enchantment']).
card_subtypes('growing ranks', []).
card_colors('growing ranks', ['W', 'G']).
card_text('growing ranks', 'At the beginning of your upkeep, populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('growing ranks', ['2', 'G/W', 'G/W']).
card_cmc('growing ranks', 4).
card_layout('growing ranks', 'normal').

% Found in: ROE
card_name('growth spasm', 'Growth Spasm').
card_type('growth spasm', 'Sorcery').
card_types('growth spasm', ['Sorcery']).
card_subtypes('growth spasm', []).
card_colors('growth spasm', ['G']).
card_text('growth spasm', 'Search your library for a basic land card, put it onto the battlefield tapped, then shuffle your library. Put a 0/1 colorless Eldrazi Spawn creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('growth spasm', ['2', 'G']).
card_cmc('growth spasm', 3).
card_layout('growth spasm', 'normal').

% Found in: UGL
card_name('growth spurt', 'Growth Spurt').
card_type('growth spurt', 'Instant').
card_types('growth spurt', ['Instant']).
card_subtypes('growth spurt', []).
card_colors('growth spurt', ['G']).
card_text('growth spurt', 'Roll a six-sided die. Target creature gets +X/+X until end of turn, where X is equal to the die roll.').
card_mana_cost('growth spurt', ['1', 'G']).
card_cmc('growth spurt', 2).
card_layout('growth spurt', 'normal').

% Found in: RAV
card_name('grozoth', 'Grozoth').
card_type('grozoth', 'Creature — Leviathan').
card_types('grozoth', ['Creature']).
card_subtypes('grozoth', ['Leviathan']).
card_colors('grozoth', ['U']).
card_text('grozoth', 'Defender (This creature can\'t attack.)\nWhen Grozoth enters the battlefield, you may search your library for any number of cards that have converted mana cost 9, reveal them, and put them into your hand. If you do, shuffle your library.\n{4}: Grozoth loses defender until end of turn.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('grozoth', ['6', 'U', 'U', 'U']).
card_cmc('grozoth', 9).
card_layout('grozoth', 'normal').
card_power('grozoth', 9).
card_toughness('grozoth', 9).

% Found in: CNS
card_name('grudge keeper', 'Grudge Keeper').
card_type('grudge keeper', 'Creature — Zombie Wizard').
card_types('grudge keeper', ['Creature']).
card_subtypes('grudge keeper', ['Zombie', 'Wizard']).
card_colors('grudge keeper', ['B']).
card_text('grudge keeper', 'Whenever players finish voting, each opponent who voted for a choice you didn\'t vote for loses 2 life.').
card_mana_cost('grudge keeper', ['1', 'B']).
card_cmc('grudge keeper', 2).
card_layout('grudge keeper', 'normal').
card_power('grudge keeper', 2).
card_toughness('grudge keeper', 1).

% Found in: ISD
card_name('gruesome deformity', 'Gruesome Deformity').
card_type('gruesome deformity', 'Enchantment — Aura').
card_types('gruesome deformity', ['Enchantment']).
card_subtypes('gruesome deformity', ['Aura']).
card_colors('gruesome deformity', ['B']).
card_text('gruesome deformity', 'Enchant creature\nEnchanted creature has intimidate. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('gruesome deformity', ['B']).
card_cmc('gruesome deformity', 1).
card_layout('gruesome deformity', 'normal').

% Found in: DKA
card_name('gruesome discovery', 'Gruesome Discovery').
card_type('gruesome discovery', 'Sorcery').
card_types('gruesome discovery', ['Sorcery']).
card_subtypes('gruesome discovery', []).
card_colors('gruesome discovery', ['B']).
card_text('gruesome discovery', 'Target player discards two cards.\nMorbid — If a creature died this turn, instead that player reveals his or her hand, you choose two cards from it, then that player discards those cards.').
card_mana_cost('gruesome discovery', ['2', 'B', 'B']).
card_cmc('gruesome discovery', 4).
card_layout('gruesome discovery', 'normal').

% Found in: MBS
card_name('gruesome encore', 'Gruesome Encore').
card_type('gruesome encore', 'Sorcery').
card_types('gruesome encore', ['Sorcery']).
card_subtypes('gruesome encore', []).
card_colors('gruesome encore', ['B']).
card_text('gruesome encore', 'Put target creature card from an opponent\'s graveyard onto the battlefield under your control. It gains haste. Exile it at the beginning of the next end step. If that creature would leave the battlefield, exile it instead of putting it anywhere else.').
card_mana_cost('gruesome encore', ['2', 'B']).
card_cmc('gruesome encore', 3).
card_layout('gruesome encore', 'normal').

% Found in: BFZ
card_name('gruesome slaughter', 'Gruesome Slaughter').
card_type('gruesome slaughter', 'Sorcery').
card_types('gruesome slaughter', ['Sorcery']).
card_subtypes('gruesome slaughter', []).
card_colors('gruesome slaughter', []).
card_text('gruesome slaughter', 'Until end of turn, colorless creatures you control gain \"{T}: This creature deals damage equal to its power to target creature.\"').
card_mana_cost('gruesome slaughter', ['6']).
card_cmc('gruesome slaughter', 6).
card_layout('gruesome slaughter', 'normal').

% Found in: GTC
card_name('gruul charm', 'Gruul Charm').
card_type('gruul charm', 'Instant').
card_types('gruul charm', ['Instant']).
card_subtypes('gruul charm', []).
card_colors('gruul charm', ['R', 'G']).
card_text('gruul charm', 'Choose one —\n• Creatures without flying can\'t block this turn.\n• Gain control of all permanents you own.\n• Gruul Charm deals 3 damage to each creature with flying.').
card_mana_cost('gruul charm', ['R', 'G']).
card_cmc('gruul charm', 2).
card_layout('gruul charm', 'normal').

% Found in: DGM
card_name('gruul cluestone', 'Gruul Cluestone').
card_type('gruul cluestone', 'Artifact').
card_types('gruul cluestone', ['Artifact']).
card_subtypes('gruul cluestone', []).
card_colors('gruul cluestone', []).
card_text('gruul cluestone', '{T}: Add {R} or {G} to your mana pool.\n{R}{G}, {T}, Sacrifice Gruul Cluestone: Draw a card.').
card_mana_cost('gruul cluestone', ['3']).
card_cmc('gruul cluestone', 3).
card_layout('gruul cluestone', 'normal').

% Found in: C13, DGM, GTC
card_name('gruul guildgate', 'Gruul Guildgate').
card_type('gruul guildgate', 'Land — Gate').
card_types('gruul guildgate', ['Land']).
card_subtypes('gruul guildgate', ['Gate']).
card_colors('gruul guildgate', []).
card_text('gruul guildgate', 'Gruul Guildgate enters the battlefield tapped.\n{T}: Add {R} or {G} to your mana pool.').
card_layout('gruul guildgate', 'normal').

% Found in: GPT, pREL
card_name('gruul guildmage', 'Gruul Guildmage').
card_type('gruul guildmage', 'Creature — Human Shaman').
card_types('gruul guildmage', ['Creature']).
card_subtypes('gruul guildmage', ['Human', 'Shaman']).
card_colors('gruul guildmage', ['R', 'G']).
card_text('gruul guildmage', '({R/G} can be paid with either {R} or {G}.)\n{3}{R}, Sacrifice a land: Gruul Guildmage deals 2 damage to target player.\n{3}{G}: Target creature gets +2/+2 until end of turn.').
card_mana_cost('gruul guildmage', ['R/G', 'R/G']).
card_cmc('gruul guildmage', 2).
card_layout('gruul guildmage', 'normal').
card_power('gruul guildmage', 2).
card_toughness('gruul guildmage', 2).

% Found in: GTC
card_name('gruul keyrune', 'Gruul Keyrune').
card_type('gruul keyrune', 'Artifact').
card_types('gruul keyrune', ['Artifact']).
card_subtypes('gruul keyrune', []).
card_colors('gruul keyrune', []).
card_text('gruul keyrune', '{T}: Add {R} or {G} to your mana pool.\n{R}{G}: Gruul Keyrune becomes a 3/2 red and green Beast artifact creature with trample until end of turn.').
card_mana_cost('gruul keyrune', ['3']).
card_cmc('gruul keyrune', 3).
card_layout('gruul keyrune', 'normal').

% Found in: GPT
card_name('gruul nodorog', 'Gruul Nodorog').
card_type('gruul nodorog', 'Creature — Beast').
card_types('gruul nodorog', ['Creature']).
card_subtypes('gruul nodorog', ['Beast']).
card_colors('gruul nodorog', ['G']).
card_text('gruul nodorog', '{R}: Gruul Nodorog gains menace until end of turn. (It can\'t be blocked except by two or more creatures.)').
card_mana_cost('gruul nodorog', ['4', 'G', 'G']).
card_cmc('gruul nodorog', 6).
card_layout('gruul nodorog', 'normal').
card_power('gruul nodorog', 4).
card_toughness('gruul nodorog', 4).

% Found in: GTC
card_name('gruul ragebeast', 'Gruul Ragebeast').
card_type('gruul ragebeast', 'Creature — Beast').
card_types('gruul ragebeast', ['Creature']).
card_subtypes('gruul ragebeast', ['Beast']).
card_colors('gruul ragebeast', ['R', 'G']).
card_text('gruul ragebeast', 'Whenever Gruul Ragebeast or another creature enters the battlefield under your control, that creature fights target creature an opponent controls.').
card_mana_cost('gruul ragebeast', ['5', 'R', 'G']).
card_cmc('gruul ragebeast', 7).
card_layout('gruul ragebeast', 'normal').
card_power('gruul ragebeast', 6).
card_toughness('gruul ragebeast', 6).

% Found in: GPT
card_name('gruul scrapper', 'Gruul Scrapper').
card_type('gruul scrapper', 'Creature — Human Berserker').
card_types('gruul scrapper', ['Creature']).
card_subtypes('gruul scrapper', ['Human', 'Berserker']).
card_colors('gruul scrapper', ['G']).
card_text('gruul scrapper', 'When Gruul Scrapper enters the battlefield, if {R} was spent to cast Gruul Scrapper, it gains haste until end of turn.').
card_mana_cost('gruul scrapper', ['3', 'G']).
card_cmc('gruul scrapper', 4).
card_layout('gruul scrapper', 'normal').
card_power('gruul scrapper', 3).
card_toughness('gruul scrapper', 2).

% Found in: ARC, CMD, GPT
card_name('gruul signet', 'Gruul Signet').
card_type('gruul signet', 'Artifact').
card_types('gruul signet', ['Artifact']).
card_subtypes('gruul signet', []).
card_colors('gruul signet', []).
card_text('gruul signet', '{1}, {T}: Add {R}{G} to your mana pool.').
card_mana_cost('gruul signet', ['2']).
card_cmc('gruul signet', 2).
card_layout('gruul signet', 'normal').

% Found in: CMD, GPT, HOP, MM2, PC2
card_name('gruul turf', 'Gruul Turf').
card_type('gruul turf', 'Land').
card_types('gruul turf', ['Land']).
card_subtypes('gruul turf', []).
card_colors('gruul turf', []).
card_text('gruul turf', 'Gruul Turf enters the battlefield tapped.\nWhen Gruul Turf enters the battlefield, return a land you control to its owner\'s hand.\n{T}: Add {R}{G} to your mana pool.').
card_layout('gruul turf', 'normal').

% Found in: DGM
card_name('gruul war chant', 'Gruul War Chant').
card_type('gruul war chant', 'Enchantment').
card_types('gruul war chant', ['Enchantment']).
card_subtypes('gruul war chant', []).
card_colors('gruul war chant', ['R', 'G']).
card_text('gruul war chant', 'Attacking creatures you control get +1/+0 and have menace. (They can\'t be blocked except by two or more creatures.)').
card_mana_cost('gruul war chant', ['2', 'R', 'G']).
card_cmc('gruul war chant', 4).
card_layout('gruul war chant', 'normal').

% Found in: GPT
card_name('gruul war plow', 'Gruul War Plow').
card_type('gruul war plow', 'Artifact').
card_types('gruul war plow', ['Artifact']).
card_subtypes('gruul war plow', []).
card_colors('gruul war plow', []).
card_text('gruul war plow', 'Creatures you control have trample.\n{1}{R}{G}: Gruul War Plow becomes a 4/4 Juggernaut artifact creature until end of turn.').
card_mana_cost('gruul war plow', ['4']).
card_cmc('gruul war plow', 4).
card_layout('gruul war plow', 'normal').

% Found in: AVR
card_name('gryff vanguard', 'Gryff Vanguard').
card_type('gryff vanguard', 'Creature — Human Knight').
card_types('gryff vanguard', ['Creature']).
card_subtypes('gryff vanguard', ['Human', 'Knight']).
card_colors('gryff vanguard', ['U']).
card_text('gryff vanguard', 'Flying\nWhen Gryff Vanguard enters the battlefield, draw a card.').
card_mana_cost('gryff vanguard', ['4', 'U']).
card_cmc('gryff vanguard', 5).
card_layout('gryff vanguard', 'normal').
card_power('gryff vanguard', 3).
card_toughness('gryff vanguard', 2).

% Found in: ME3, PTK
card_name('guan yu\'s 1,000-li march', 'Guan Yu\'s 1,000-Li March').
card_type('guan yu\'s 1,000-li march', 'Sorcery').
card_types('guan yu\'s 1,000-li march', ['Sorcery']).
card_subtypes('guan yu\'s 1,000-li march', []).
card_colors('guan yu\'s 1,000-li march', ['W']).
card_text('guan yu\'s 1,000-li march', 'Destroy all tapped creatures.').
card_mana_cost('guan yu\'s 1,000-li march', ['4', 'W', 'W']).
card_cmc('guan yu\'s 1,000-li march', 6).
card_layout('guan yu\'s 1,000-li march', 'normal').

% Found in: ME3, PTK
card_name('guan yu, sainted warrior', 'Guan Yu, Sainted Warrior').
card_type('guan yu, sainted warrior', 'Legendary Creature — Human Soldier Warrior').
card_types('guan yu, sainted warrior', ['Creature']).
card_subtypes('guan yu, sainted warrior', ['Human', 'Soldier', 'Warrior']).
card_supertypes('guan yu, sainted warrior', ['Legendary']).
card_colors('guan yu, sainted warrior', ['W']).
card_text('guan yu, sainted warrior', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhen Guan Yu, Sainted Warrior is put into your graveyard from the battlefield, you may shuffle Guan Yu into your library.').
card_mana_cost('guan yu, sainted warrior', ['3', 'W', 'W']).
card_cmc('guan yu, sainted warrior', 5).
card_layout('guan yu, sainted warrior', 'normal').
card_power('guan yu, sainted warrior', 3).
card_toughness('guan yu, sainted warrior', 5).

% Found in: PLS
card_name('guard dogs', 'Guard Dogs').
card_type('guard dogs', 'Creature — Hound').
card_types('guard dogs', ['Creature']).
card_subtypes('guard dogs', ['Hound']).
card_colors('guard dogs', ['W']).
card_text('guard dogs', '{2}{W}, {T}: Choose a permanent you control. Prevent all combat damage target creature would deal this turn if it shares a color with that permanent.').
card_mana_cost('guard dogs', ['3', 'W']).
card_cmc('guard dogs', 4).
card_layout('guard dogs', 'normal').
card_power('guard dogs', 2).
card_toughness('guard dogs', 2).

% Found in: ROE
card_name('guard duty', 'Guard Duty').
card_type('guard duty', 'Enchantment — Aura').
card_types('guard duty', ['Enchantment']).
card_subtypes('guard duty', ['Aura']).
card_colors('guard duty', ['W']).
card_text('guard duty', 'Enchant creature\nEnchanted creature has defender.').
card_mana_cost('guard duty', ['W']).
card_cmc('guard duty', 1).
card_layout('guard duty', 'normal').

% Found in: C13, CMD, PC2, ROE
card_name('guard gomazoa', 'Guard Gomazoa').
card_type('guard gomazoa', 'Creature — Jellyfish').
card_types('guard gomazoa', ['Creature']).
card_subtypes('guard gomazoa', ['Jellyfish']).
card_colors('guard gomazoa', ['U']).
card_text('guard gomazoa', 'Defender, flying\nPrevent all combat damage that would be dealt to Guard Gomazoa.').
card_mana_cost('guard gomazoa', ['2', 'U']).
card_cmc('guard gomazoa', 3).
card_layout('guard gomazoa', 'normal').
card_power('guard gomazoa', 1).
card_toughness('guard gomazoa', 3).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('guardian angel', 'Guardian Angel').
card_type('guardian angel', 'Instant').
card_types('guardian angel', ['Instant']).
card_subtypes('guardian angel', []).
card_colors('guardian angel', ['W']).
card_text('guardian angel', 'Prevent the next X damage that would be dealt to target creature or player this turn. Until end of turn, you may pay {1} any time you could cast an instant. If you do, prevent the next 1 damage that would be dealt to that creature or player this turn.').
card_mana_cost('guardian angel', ['X', 'W']).
card_cmc('guardian angel', 1).
card_layout('guardian angel', 'normal').

% Found in: ORI
card_name('guardian automaton', 'Guardian Automaton').
card_type('guardian automaton', 'Artifact Creature — Construct').
card_types('guardian automaton', ['Artifact', 'Creature']).
card_subtypes('guardian automaton', ['Construct']).
card_colors('guardian automaton', []).
card_text('guardian automaton', 'When Guardian Automaton dies, you gain 3 life.').
card_mana_cost('guardian automaton', ['4']).
card_cmc('guardian automaton', 4).
card_layout('guardian automaton', 'normal').
card_power('guardian automaton', 3).
card_toughness('guardian automaton', 3).

% Found in: ARN, ME4
card_name('guardian beast', 'Guardian Beast').
card_type('guardian beast', 'Creature — Beast').
card_types('guardian beast', ['Creature']).
card_subtypes('guardian beast', ['Beast']).
card_colors('guardian beast', ['B']).
card_text('guardian beast', 'As long as Guardian Beast is untapped, noncreature artifacts you control can\'t be enchanted, they have indestructible, and other players can\'t gain control of them. This effect doesn\'t remove Auras already attached to those artifacts.').
card_mana_cost('guardian beast', ['3', 'B']).
card_cmc('guardian beast', 4).
card_layout('guardian beast', 'normal').
card_power('guardian beast', 2).
card_toughness('guardian beast', 4).
card_reserved('guardian beast').

% Found in: 5DN
card_name('guardian idol', 'Guardian Idol').
card_type('guardian idol', 'Artifact').
card_types('guardian idol', ['Artifact']).
card_subtypes('guardian idol', []).
card_colors('guardian idol', []).
card_text('guardian idol', 'Guardian Idol enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{2}: Guardian Idol becomes a 2/2 Golem artifact creature until end of turn.').
card_mana_cost('guardian idol', ['2']).
card_cmc('guardian idol', 2).
card_layout('guardian idol', 'normal').

% Found in: M13
card_name('guardian lions', 'Guardian Lions').
card_type('guardian lions', 'Creature — Cat').
card_types('guardian lions', ['Creature']).
card_subtypes('guardian lions', ['Cat']).
card_colors('guardian lions', ['W']).
card_text('guardian lions', 'Vigilance (Attacking doesn\'t cause this creature to tap.)').
card_mana_cost('guardian lions', ['4', 'W']).
card_cmc('guardian lions', 5).
card_layout('guardian lions', 'normal').
card_power('guardian lions', 1).
card_toughness('guardian lions', 6).

% Found in: LRW
card_name('guardian of cloverdell', 'Guardian of Cloverdell').
card_type('guardian of cloverdell', 'Creature — Treefolk Shaman').
card_types('guardian of cloverdell', ['Creature']).
card_subtypes('guardian of cloverdell', ['Treefolk', 'Shaman']).
card_colors('guardian of cloverdell', ['G']).
card_text('guardian of cloverdell', 'When Guardian of Cloverdell enters the battlefield, put three 1/1 white Kithkin Soldier creature tokens onto the battlefield.\n{G}, Sacrifice a Kithkin: You gain 1 life.').
card_mana_cost('guardian of cloverdell', ['5', 'G', 'G']).
card_cmc('guardian of cloverdell', 7).
card_layout('guardian of cloverdell', 'normal').
card_power('guardian of cloverdell', 4).
card_toughness('guardian of cloverdell', 5).

% Found in: CHK
card_name('guardian of solitude', 'Guardian of Solitude').
card_type('guardian of solitude', 'Creature — Spirit').
card_types('guardian of solitude', ['Creature']).
card_subtypes('guardian of solitude', ['Spirit']).
card_colors('guardian of solitude', ['U']).
card_text('guardian of solitude', 'Whenever you cast a Spirit or Arcane spell, target creature gains flying until end of turn.').
card_mana_cost('guardian of solitude', ['1', 'U']).
card_cmc('guardian of solitude', 2).
card_layout('guardian of solitude', 'normal').
card_power('guardian of solitude', 1).
card_toughness('guardian of solitude', 2).

% Found in: BFZ
card_name('guardian of tazeem', 'Guardian of Tazeem').
card_type('guardian of tazeem', 'Creature — Sphinx').
card_types('guardian of tazeem', ['Creature']).
card_subtypes('guardian of tazeem', ['Sphinx']).
card_colors('guardian of tazeem', ['U']).
card_text('guardian of tazeem', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, tap target creature an opponent controls. If that land is an Island, that creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('guardian of tazeem', ['3', 'U', 'U']).
card_cmc('guardian of tazeem', 5).
card_layout('guardian of tazeem', 'normal').
card_power('guardian of tazeem', 4).
card_toughness('guardian of tazeem', 5).

% Found in: M14
card_name('guardian of the ages', 'Guardian of the Ages').
card_type('guardian of the ages', 'Artifact Creature — Golem').
card_types('guardian of the ages', ['Artifact', 'Creature']).
card_subtypes('guardian of the ages', ['Golem']).
card_colors('guardian of the ages', []).
card_text('guardian of the ages', 'Defender (This creature can\'t attack.)\nWhen a creature attacks you or a planeswalker you control, if Guardian of the Ages has defender, it loses defender and gains trample.').
card_mana_cost('guardian of the ages', ['7']).
card_cmc('guardian of the ages', 7).
card_layout('guardian of the ages', 'normal').
card_power('guardian of the ages', 7).
card_toughness('guardian of the ages', 7).

% Found in: GTC
card_name('guardian of the gateless', 'Guardian of the Gateless').
card_type('guardian of the gateless', 'Creature — Angel').
card_types('guardian of the gateless', ['Creature']).
card_subtypes('guardian of the gateless', ['Angel']).
card_colors('guardian of the gateless', ['W']).
card_text('guardian of the gateless', 'Flying\nGuardian of the Gateless can block any number of creatures.\nWhenever Guardian of the Gateless blocks, it gets +1/+1 until end of turn for each creature it\'s blocking.').
card_mana_cost('guardian of the gateless', ['4', 'W']).
card_cmc('guardian of the gateless', 5).
card_layout('guardian of the gateless', 'normal').
card_power('guardian of the gateless', 3).
card_toughness('guardian of the gateless', 3).

% Found in: DIS
card_name('guardian of the guildpact', 'Guardian of the Guildpact').
card_type('guardian of the guildpact', 'Creature — Spirit').
card_types('guardian of the guildpact', ['Creature']).
card_subtypes('guardian of the guildpact', ['Spirit']).
card_colors('guardian of the guildpact', ['W']).
card_text('guardian of the guildpact', 'Protection from monocolored').
card_mana_cost('guardian of the guildpact', ['3', 'W']).
card_cmc('guardian of the guildpact', 4).
card_layout('guardian of the guildpact', 'normal').
card_power('guardian of the guildpact', 2).
card_toughness('guardian of the guildpact', 3).

% Found in: RAV
card_name('guardian of vitu-ghazi', 'Guardian of Vitu-Ghazi').
card_type('guardian of vitu-ghazi', 'Creature — Elemental').
card_types('guardian of vitu-ghazi', ['Creature']).
card_subtypes('guardian of vitu-ghazi', ['Elemental']).
card_colors('guardian of vitu-ghazi', ['W', 'G']).
card_text('guardian of vitu-ghazi', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nVigilance').
card_mana_cost('guardian of vitu-ghazi', ['6', 'G', 'W']).
card_cmc('guardian of vitu-ghazi', 8).
card_layout('guardian of vitu-ghazi', 'normal').
card_power('guardian of vitu-ghazi', 4).
card_toughness('guardian of vitu-ghazi', 7).

% Found in: M10
card_name('guardian seraph', 'Guardian Seraph').
card_type('guardian seraph', 'Creature — Angel').
card_types('guardian seraph', ['Creature']).
card_subtypes('guardian seraph', ['Angel']).
card_colors('guardian seraph', ['W']).
card_text('guardian seraph', 'Flying\nIf a source an opponent controls would deal damage to you, prevent 1 of that damage.').
card_mana_cost('guardian seraph', ['2', 'W', 'W']).
card_cmc('guardian seraph', 4).
card_layout('guardian seraph', 'normal').
card_power('guardian seraph', 3).
card_toughness('guardian seraph', 4).

% Found in: DTK
card_name('guardian shield-bearer', 'Guardian Shield-Bearer').
card_type('guardian shield-bearer', 'Creature — Human Soldier').
card_types('guardian shield-bearer', ['Creature']).
card_subtypes('guardian shield-bearer', ['Human', 'Soldier']).
card_colors('guardian shield-bearer', ['G']).
card_text('guardian shield-bearer', 'Megamorph {3}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)\nWhen Guardian Shield-Bearer is turned face up, put a +1/+1 counter on another target creature you control.').
card_mana_cost('guardian shield-bearer', ['1', 'G']).
card_cmc('guardian shield-bearer', 2).
card_layout('guardian shield-bearer', 'normal').
card_power('guardian shield-bearer', 2).
card_toughness('guardian shield-bearer', 1).

% Found in: CNS, WWK
card_name('guardian zendikon', 'Guardian Zendikon').
card_type('guardian zendikon', 'Enchantment — Aura').
card_types('guardian zendikon', ['Enchantment']).
card_subtypes('guardian zendikon', ['Aura']).
card_colors('guardian zendikon', ['W']).
card_text('guardian zendikon', 'Enchant land\nEnchanted land is a 2/6 white Wall creature with defender. It\'s still a land.\nWhen enchanted land dies, return that card to its owner\'s hand.').
card_mana_cost('guardian zendikon', ['2', 'W']).
card_cmc('guardian zendikon', 3).
card_layout('guardian zendikon', 'normal').

% Found in: GPT
card_name('guardian\'s magemark', 'Guardian\'s Magemark').
card_type('guardian\'s magemark', 'Enchantment — Aura').
card_types('guardian\'s magemark', ['Enchantment']).
card_subtypes('guardian\'s magemark', ['Aura']).
card_colors('guardian\'s magemark', ['W']).
card_text('guardian\'s magemark', 'Flash\nEnchant creature\nCreatures you control that are enchanted get +1/+1.').
card_mana_cost('guardian\'s magemark', ['2', 'W']).
card_cmc('guardian\'s magemark', 3).
card_layout('guardian\'s magemark', 'normal').

% Found in: ALA, M13
card_name('guardians of akrasa', 'Guardians of Akrasa').
card_type('guardians of akrasa', 'Creature — Human Soldier').
card_types('guardians of akrasa', ['Creature']).
card_subtypes('guardians of akrasa', ['Human', 'Soldier']).
card_colors('guardians of akrasa', ['W']).
card_text('guardians of akrasa', 'Defender (This creature can\'t attack.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('guardians of akrasa', ['2', 'W']).
card_cmc('guardians of akrasa', 3).
card_layout('guardians of akrasa', 'normal').
card_power('guardians of akrasa', 0).
card_toughness('guardians of akrasa', 4).

% Found in: ORI, THS
card_name('guardians of meletis', 'Guardians of Meletis').
card_type('guardians of meletis', 'Artifact Creature — Golem').
card_types('guardians of meletis', ['Artifact', 'Creature']).
card_subtypes('guardians of meletis', ['Golem']).
card_colors('guardians of meletis', []).
card_text('guardians of meletis', 'Defender (This creature can\'t attack.)').
card_mana_cost('guardians of meletis', ['3']).
card_cmc('guardians of meletis', 3).
card_layout('guardians of meletis', 'normal').
card_power('guardians of meletis', 0).
card_toughness('guardians of meletis', 6).

% Found in: M12
card_name('guardians\' pledge', 'Guardians\' Pledge').
card_type('guardians\' pledge', 'Instant').
card_types('guardians\' pledge', ['Instant']).
card_subtypes('guardians\' pledge', []).
card_colors('guardians\' pledge', ['W']).
card_text('guardians\' pledge', 'White creatures you control get +2/+2 until end of turn.').
card_mana_cost('guardians\' pledge', ['1', 'W', 'W']).
card_cmc('guardians\' pledge', 3).
card_layout('guardians\' pledge', 'normal').

% Found in: DTK
card_name('gudul lurker', 'Gudul Lurker').
card_type('gudul lurker', 'Creature — Salamander').
card_types('gudul lurker', ['Creature']).
card_subtypes('gudul lurker', ['Salamander']).
card_colors('gudul lurker', ['U']).
card_text('gudul lurker', 'Gudul Lurker can\'t be blocked.\nMegamorph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its megamorph cost and put a +1/+1 counter on it.)').
card_mana_cost('gudul lurker', ['U']).
card_cmc('gudul lurker', 1).
card_layout('gudul lurker', 'normal').
card_power('gudul lurker', 1).
card_toughness('gudul lurker', 1).

% Found in: 10E, 8ED, 9ED, ALL, DKM
card_name('guerrilla tactics', 'Guerrilla Tactics').
card_type('guerrilla tactics', 'Instant').
card_types('guerrilla tactics', ['Instant']).
card_subtypes('guerrilla tactics', []).
card_colors('guerrilla tactics', ['R']).
card_text('guerrilla tactics', 'Guerrilla Tactics deals 2 damage to target creature or player.\nWhen a spell or ability an opponent controls causes you to discard Guerrilla Tactics, Guerrilla Tactics deals 4 damage to target creature or player.').
card_mana_cost('guerrilla tactics', ['1', 'R']).
card_cmc('guerrilla tactics', 2).
card_layout('guerrilla tactics', 'normal').

% Found in: APC
card_name('guided passage', 'Guided Passage').
card_type('guided passage', 'Sorcery').
card_types('guided passage', ['Sorcery']).
card_subtypes('guided passage', []).
card_colors('guided passage', ['U', 'R', 'G']).
card_text('guided passage', 'Reveal the cards in your library. An opponent chooses from among them a creature card, a land card, and a noncreature, nonland card. You put the chosen cards into your hand. Then shuffle your library.').
card_mana_cost('guided passage', ['U', 'R', 'G']).
card_cmc('guided passage', 3).
card_layout('guided passage', 'normal').

% Found in: JUD, WTH
card_name('guided strike', 'Guided Strike').
card_type('guided strike', 'Instant').
card_types('guided strike', ['Instant']).
card_subtypes('guided strike', []).
card_colors('guided strike', ['W']).
card_text('guided strike', 'Target creature gets +1/+0 and gains first strike until end of turn.\nDraw a card.').
card_mana_cost('guided strike', ['1', 'W']).
card_cmc('guided strike', 2).
card_layout('guided strike', 'normal').

% Found in: VIS
card_name('guiding spirit', 'Guiding Spirit').
card_type('guiding spirit', 'Creature — Angel Spirit').
card_types('guiding spirit', ['Creature']).
card_subtypes('guiding spirit', ['Angel', 'Spirit']).
card_colors('guiding spirit', ['W', 'U']).
card_text('guiding spirit', 'Flying\n{T}: If the top card of target player\'s graveyard is a creature card, put that card on top of that player\'s library.').
card_mana_cost('guiding spirit', ['1', 'W', 'U']).
card_cmc('guiding spirit', 3).
card_layout('guiding spirit', 'normal').
card_power('guiding spirit', 1).
card_toughness('guiding spirit', 2).
card_reserved('guiding spirit').

% Found in: RTR
card_name('guild feud', 'Guild Feud').
card_type('guild feud', 'Enchantment').
card_types('guild feud', ['Enchantment']).
card_subtypes('guild feud', []).
card_colors('guild feud', ['R']).
card_text('guild feud', 'At the beginning of your upkeep, target opponent reveals the top three cards of his or her library, may put a creature card from among them onto the battlefield, then puts the rest into his or her graveyard. You do the same with the top three cards of your library. If two creatures are put onto the battlefield this way, those creatures fight each other.').
card_mana_cost('guild feud', ['5', 'R']).
card_cmc('guild feud', 6).
card_layout('guild feud', 'normal').

% Found in: GTC
card_name('guildscorn ward', 'Guildscorn Ward').
card_type('guildscorn ward', 'Enchantment — Aura').
card_types('guildscorn ward', ['Enchantment']).
card_subtypes('guildscorn ward', ['Aura']).
card_colors('guildscorn ward', ['W']).
card_text('guildscorn ward', 'Enchant creature\nEnchanted creature has protection from multicolored.').
card_mana_cost('guildscorn ward', ['W']).
card_cmc('guildscorn ward', 1).
card_layout('guildscorn ward', 'normal').

% Found in: DD2, DD3_JVC, LRW, MM2
card_name('guile', 'Guile').
card_type('guile', 'Creature — Elemental Incarnation').
card_types('guile', ['Creature']).
card_subtypes('guile', ['Elemental', 'Incarnation']).
card_colors('guile', ['U']).
card_text('guile', 'Guile can\'t be blocked except by three or more creatures.\nIf a spell or ability you control would counter a spell, instead exile that spell and you may play that card without paying its mana cost.\nWhen Guile is put into a graveyard from anywhere, shuffle it into its owner\'s library.').
card_mana_cost('guile', ['3', 'U', 'U', 'U']).
card_cmc('guile', 6).
card_layout('guile', 'normal').
card_power('guile', 6).
card_toughness('guile', 6).

% Found in: JUD
card_name('guiltfeeder', 'Guiltfeeder').
card_type('guiltfeeder', 'Creature — Horror').
card_types('guiltfeeder', ['Creature']).
card_subtypes('guiltfeeder', ['Horror']).
card_colors('guiltfeeder', ['B']).
card_text('guiltfeeder', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhenever Guiltfeeder attacks and isn\'t blocked, defending player loses 1 life for each card in his or her graveyard.').
card_mana_cost('guiltfeeder', ['3', 'B', 'B']).
card_cmc('guiltfeeder', 5).
card_layout('guiltfeeder', 'normal').
card_power('guiltfeeder', 0).
card_toughness('guiltfeeder', 4).

% Found in: SCG
card_name('guilty conscience', 'Guilty Conscience').
card_type('guilty conscience', 'Enchantment — Aura').
card_types('guilty conscience', ['Enchantment']).
card_subtypes('guilty conscience', ['Aura']).
card_colors('guilty conscience', ['W']).
card_text('guilty conscience', 'Enchant creature\nWhenever enchanted creature deals damage, Guilty Conscience deals that much damage to that creature.').
card_mana_cost('guilty conscience', ['W']).
card_cmc('guilty conscience', 1).
card_layout('guilty conscience', 'normal').

% Found in: AVR
card_name('guise of fire', 'Guise of Fire').
card_type('guise of fire', 'Enchantment — Aura').
card_types('guise of fire', ['Enchantment']).
card_subtypes('guise of fire', ['Aura']).
card_colors('guise of fire', ['R']).
card_text('guise of fire', 'Enchant creature\nEnchanted creature gets +1/-1 and attacks each turn if able.').
card_mana_cost('guise of fire', ['R']).
card_cmc('guise of fire', 1).
card_layout('guise of fire', 'normal').

% Found in: PCY
card_name('gulf squid', 'Gulf Squid').
card_type('gulf squid', 'Creature — Squid Beast').
card_types('gulf squid', ['Creature']).
card_subtypes('gulf squid', ['Squid', 'Beast']).
card_colors('gulf squid', ['U']).
card_text('gulf squid', 'When Gulf Squid enters the battlefield, tap all lands target player controls.').
card_mana_cost('gulf squid', ['3', 'U']).
card_cmc('gulf squid', 4).
card_layout('gulf squid', 'normal').
card_power('gulf squid', 2).
card_toughness('gulf squid', 2).

% Found in: USG
card_name('guma', 'Guma').
card_type('guma', 'Creature — Cat').
card_types('guma', ['Creature']).
card_subtypes('guma', ['Cat']).
card_colors('guma', ['R']).
card_text('guma', 'Protection from blue').
card_mana_cost('guma', ['2', 'R']).
card_cmc('guma', 3).
card_layout('guma', 'normal').
card_power('guma', 2).
card_toughness('guma', 2).

% Found in: FRF
card_name('gurmag angler', 'Gurmag Angler').
card_type('gurmag angler', 'Creature — Zombie Fish').
card_types('gurmag angler', ['Creature']).
card_subtypes('gurmag angler', ['Zombie', 'Fish']).
card_colors('gurmag angler', ['B']).
card_text('gurmag angler', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)').
card_mana_cost('gurmag angler', ['6', 'B']).
card_cmc('gurmag angler', 7).
card_layout('gurmag angler', 'normal').
card_power('gurmag angler', 5).
card_toughness('gurmag angler', 5).

% Found in: DTK
card_name('gurmag drowner', 'Gurmag Drowner').
card_type('gurmag drowner', 'Creature — Naga Wizard').
card_types('gurmag drowner', ['Creature']).
card_subtypes('gurmag drowner', ['Naga', 'Wizard']).
card_colors('gurmag drowner', ['U']).
card_text('gurmag drowner', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Gurmag Drowner exploits a creature, look at the top four cards of your library. Put one of them into your hand and the rest into your graveyard.').
card_mana_cost('gurmag drowner', ['3', 'U']).
card_cmc('gurmag drowner', 4).
card_layout('gurmag drowner', 'normal').
card_power('gurmag drowner', 2).
card_toughness('gurmag drowner', 4).

% Found in: KTK
card_name('gurmag swiftwing', 'Gurmag Swiftwing').
card_type('gurmag swiftwing', 'Creature — Bat').
card_types('gurmag swiftwing', ['Creature']).
card_subtypes('gurmag swiftwing', ['Bat']).
card_colors('gurmag swiftwing', ['B']).
card_text('gurmag swiftwing', 'Flying, first strike, haste').
card_mana_cost('gurmag swiftwing', ['1', 'B']).
card_cmc('gurmag swiftwing', 2).
card_layout('gurmag swiftwing', 'normal').
card_power('gurmag swiftwing', 1).
card_toughness('gurmag swiftwing', 2).

% Found in: TOR
card_name('gurzigost', 'Gurzigost').
card_type('gurzigost', 'Creature — Beast').
card_types('gurzigost', ['Creature']).
card_subtypes('gurzigost', ['Beast']).
card_colors('gurzigost', ['G']).
card_text('gurzigost', 'At the beginning of your upkeep, sacrifice Gurzigost unless you put two cards from your graveyard on the bottom of your library.\n{G}{G}, Discard a card: You may have Gurzigost assign its combat damage this turn as though it weren\'t blocked.').
card_mana_cost('gurzigost', ['3', 'G', 'G']).
card_cmc('gurzigost', 5).
card_layout('gurzigost', 'normal').
card_power('gurzigost', 6).
card_toughness('gurzigost', 8).

% Found in: UGL
card_name('gus', 'Gus').
card_type('gus', 'Creature — Gus').
card_types('gus', ['Creature']).
card_subtypes('gus', ['Gus']).
card_colors('gus', ['G']).
card_text('gus', 'Gus comes into play with one +1/+1 counter on it for each game you have lost to your opponent since you last won a Magic game against him or her.').
card_mana_cost('gus', ['2', 'G']).
card_cmc('gus', 3).
card_layout('gus', 'normal').
card_power('gus', 2).
card_toughness('gus', 2).

% Found in: DD2, DD3_JVC, MMQ, VMA
card_name('gush', 'Gush').
card_type('gush', 'Instant').
card_types('gush', ['Instant']).
card_subtypes('gush', []).
card_colors('gush', ['U']).
card_text('gush', 'You may return two Islands you control to their owner\'s hand rather than pay Gush\'s mana cost.\nDraw two cards.').
card_mana_cost('gush', ['4', 'U']).
card_cmc('gush', 5).
card_layout('gush', 'normal').

% Found in: MBS, MM2
card_name('gust-skimmer', 'Gust-Skimmer').
card_type('gust-skimmer', 'Artifact Creature — Insect').
card_types('gust-skimmer', ['Artifact', 'Creature']).
card_subtypes('gust-skimmer', ['Insect']).
card_colors('gust-skimmer', []).
card_text('gust-skimmer', '{U}: Gust-Skimmer gains flying until end of turn.').
card_mana_cost('gust-skimmer', ['2']).
card_cmc('gust-skimmer', 2).
card_layout('gust-skimmer', 'normal').
card_power('gust-skimmer', 2).
card_toughness('gust-skimmer', 1).

% Found in: TSP
card_name('gustcloak cavalier', 'Gustcloak Cavalier').
card_type('gustcloak cavalier', 'Creature — Human Knight').
card_types('gustcloak cavalier', ['Creature']).
card_subtypes('gustcloak cavalier', ['Human', 'Knight']).
card_colors('gustcloak cavalier', ['W']).
card_text('gustcloak cavalier', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nWhenever Gustcloak Cavalier attacks, you may tap target creature.\nWhenever Gustcloak Cavalier becomes blocked, you may untap Gustcloak Cavalier and remove it from combat.').
card_mana_cost('gustcloak cavalier', ['3', 'W', 'W']).
card_cmc('gustcloak cavalier', 5).
card_layout('gustcloak cavalier', 'normal').
card_power('gustcloak cavalier', 2).
card_toughness('gustcloak cavalier', 2).

% Found in: DDO, ONS, VMA
card_name('gustcloak harrier', 'Gustcloak Harrier').
card_type('gustcloak harrier', 'Creature — Bird Soldier').
card_types('gustcloak harrier', ['Creature']).
card_subtypes('gustcloak harrier', ['Bird', 'Soldier']).
card_colors('gustcloak harrier', ['W']).
card_text('gustcloak harrier', 'Flying\nWhenever Gustcloak Harrier becomes blocked, you may untap it and remove it from combat.').
card_mana_cost('gustcloak harrier', ['1', 'W', 'W']).
card_cmc('gustcloak harrier', 3).
card_layout('gustcloak harrier', 'normal').
card_power('gustcloak harrier', 2).
card_toughness('gustcloak harrier', 2).

% Found in: ONS
card_name('gustcloak runner', 'Gustcloak Runner').
card_type('gustcloak runner', 'Creature — Human Soldier').
card_types('gustcloak runner', ['Creature']).
card_subtypes('gustcloak runner', ['Human', 'Soldier']).
card_colors('gustcloak runner', ['W']).
card_text('gustcloak runner', 'Whenever Gustcloak Runner becomes blocked, you may untap it and remove it from combat.').
card_mana_cost('gustcloak runner', ['W']).
card_cmc('gustcloak runner', 1).
card_layout('gustcloak runner', 'normal').
card_power('gustcloak runner', 1).
card_toughness('gustcloak runner', 1).

% Found in: DDO, ONS
card_name('gustcloak savior', 'Gustcloak Savior').
card_type('gustcloak savior', 'Creature — Bird Soldier').
card_types('gustcloak savior', ['Creature']).
card_subtypes('gustcloak savior', ['Bird', 'Soldier']).
card_colors('gustcloak savior', ['W']).
card_text('gustcloak savior', 'Flying\nWhenever a creature you control becomes blocked, you may untap that creature and remove it from combat.').
card_mana_cost('gustcloak savior', ['4', 'W']).
card_cmc('gustcloak savior', 5).
card_layout('gustcloak savior', 'normal').
card_power('gustcloak savior', 3).
card_toughness('gustcloak savior', 4).

% Found in: DDL, DDO, ONS
card_name('gustcloak sentinel', 'Gustcloak Sentinel').
card_type('gustcloak sentinel', 'Creature — Human Soldier').
card_types('gustcloak sentinel', ['Creature']).
card_subtypes('gustcloak sentinel', ['Human', 'Soldier']).
card_colors('gustcloak sentinel', ['W']).
card_text('gustcloak sentinel', 'Whenever Gustcloak Sentinel becomes blocked, you may untap it and remove it from combat.').
card_mana_cost('gustcloak sentinel', ['2', 'W', 'W']).
card_cmc('gustcloak sentinel', 4).
card_layout('gustcloak sentinel', 'normal').
card_power('gustcloak sentinel', 3).
card_toughness('gustcloak sentinel', 3).

% Found in: DDO, ONS
card_name('gustcloak skirmisher', 'Gustcloak Skirmisher').
card_type('gustcloak skirmisher', 'Creature — Bird Soldier').
card_types('gustcloak skirmisher', ['Creature']).
card_subtypes('gustcloak skirmisher', ['Bird', 'Soldier']).
card_colors('gustcloak skirmisher', ['W']).
card_text('gustcloak skirmisher', 'Flying\nWhenever Gustcloak Skirmisher becomes blocked, you may untap it and remove it from combat.').
card_mana_cost('gustcloak skirmisher', ['3', 'W']).
card_cmc('gustcloak skirmisher', 4).
card_layout('gustcloak skirmisher', 'normal').
card_power('gustcloak skirmisher', 2).
card_toughness('gustcloak skirmisher', 3).

% Found in: ALL, ME2
card_name('gustha\'s scepter', 'Gustha\'s Scepter').
card_type('gustha\'s scepter', 'Artifact').
card_types('gustha\'s scepter', ['Artifact']).
card_subtypes('gustha\'s scepter', []).
card_colors('gustha\'s scepter', []).
card_text('gustha\'s scepter', '{T}: Exile a card from your hand face down. You may look at it for as long as it remains exiled.\n{T}: Return a card you own exiled with Gustha\'s Scepter to your hand.\nWhen you lose control of Gustha\'s Scepter, put all cards exiled with Gustha\'s Scepter into their owner\'s graveyard.').
card_mana_cost('gustha\'s scepter', ['0']).
card_cmc('gustha\'s scepter', 0).
card_layout('gustha\'s scepter', 'normal').
card_reserved('gustha\'s scepter').

% Found in: ALA
card_name('gustrider exuberant', 'Gustrider Exuberant').
card_type('gustrider exuberant', 'Creature — Human Wizard').
card_types('gustrider exuberant', ['Creature']).
card_subtypes('gustrider exuberant', ['Human', 'Wizard']).
card_colors('gustrider exuberant', ['W']).
card_text('gustrider exuberant', 'Flying\nSacrifice Gustrider Exuberant: Creatures you control with power 5 or greater gain flying until end of turn.').
card_mana_cost('gustrider exuberant', ['2', 'W']).
card_cmc('gustrider exuberant', 3).
card_layout('gustrider exuberant', 'normal').
card_power('gustrider exuberant', 1).
card_toughness('gustrider exuberant', 2).

% Found in: MM2, NPH
card_name('gut shot', 'Gut Shot').
card_type('gut shot', 'Instant').
card_types('gut shot', ['Instant']).
card_subtypes('gut shot', []).
card_colors('gut shot', ['R']).
card_text('gut shot', '({R/P} can be paid with either {R} or 2 life.)\nGut Shot deals 1 damage to target creature or player.').
card_mana_cost('gut shot', ['R/P']).
card_cmc('gut shot', 1).
card_layout('gut shot', 'normal').

% Found in: CSP
card_name('gutless ghoul', 'Gutless Ghoul').
card_type('gutless ghoul', 'Snow Creature — Zombie').
card_types('gutless ghoul', ['Creature']).
card_subtypes('gutless ghoul', ['Zombie']).
card_supertypes('gutless ghoul', ['Snow']).
card_colors('gutless ghoul', ['B']).
card_text('gutless ghoul', '{1}, Sacrifice a creature: You gain 2 life.').
card_mana_cost('gutless ghoul', ['2', 'B']).
card_cmc('gutless ghoul', 3).
card_layout('gutless ghoul', 'normal').
card_power('gutless ghoul', 2).
card_toughness('gutless ghoul', 2).

% Found in: ISD
card_name('gutter grime', 'Gutter Grime').
card_type('gutter grime', 'Enchantment').
card_types('gutter grime', ['Enchantment']).
card_subtypes('gutter grime', []).
card_colors('gutter grime', ['G']).
card_text('gutter grime', 'Whenever a nontoken creature you control dies, put a slime counter on Gutter Grime, then put a green Ooze creature token onto the battlefield with \"This creature\'s power and toughness are each equal to the number of slime counters on Gutter Grime.\"').
card_mana_cost('gutter grime', ['4', 'G']).
card_cmc('gutter grime', 5).
card_layout('gutter grime', 'normal').

% Found in: GTC
card_name('gutter skulk', 'Gutter Skulk').
card_type('gutter skulk', 'Creature — Zombie Rat').
card_types('gutter skulk', ['Creature']).
card_subtypes('gutter skulk', ['Zombie', 'Rat']).
card_colors('gutter skulk', ['B']).
card_text('gutter skulk', '').
card_mana_cost('gutter skulk', ['1', 'B']).
card_cmc('gutter skulk', 2).
card_layout('gutter skulk', 'normal').
card_power('gutter skulk', 2).
card_toughness('gutter skulk', 2).

% Found in: C13, RTR
card_name('guttersnipe', 'Guttersnipe').
card_type('guttersnipe', 'Creature — Goblin Shaman').
card_types('guttersnipe', ['Creature']).
card_subtypes('guttersnipe', ['Goblin', 'Shaman']).
card_colors('guttersnipe', ['R']).
card_text('guttersnipe', 'Whenever you cast an instant or sorcery spell, Guttersnipe deals 2 damage to each opponent.').
card_mana_cost('guttersnipe', ['2', 'R']).
card_cmc('guttersnipe', 3).
card_layout('guttersnipe', 'normal').
card_power('guttersnipe', 2).
card_toughness('guttersnipe', 2).

% Found in: SHM
card_name('guttural response', 'Guttural Response').
card_type('guttural response', 'Instant').
card_types('guttural response', ['Instant']).
card_subtypes('guttural response', []).
card_colors('guttural response', ['R', 'G']).
card_text('guttural response', 'Counter target blue instant spell.').
card_mana_cost('guttural response', ['R/G']).
card_cmc('guttural response', 1).
card_layout('guttural response', 'normal').

% Found in: CHK
card_name('gutwrencher oni', 'Gutwrencher Oni').
card_type('gutwrencher oni', 'Creature — Demon Spirit').
card_types('gutwrencher oni', ['Creature']).
card_subtypes('gutwrencher oni', ['Demon', 'Spirit']).
card_colors('gutwrencher oni', ['B']).
card_text('gutwrencher oni', 'Trample\nAt the beginning of your upkeep, discard a card if you don\'t control an Ogre.').
card_mana_cost('gutwrencher oni', ['3', 'B', 'B']).
card_cmc('gutwrencher oni', 5).
card_layout('gutwrencher oni', 'normal').
card_power('gutwrencher oni', 5).
card_toughness('gutwrencher oni', 4).

% Found in: ROE, pMEI
card_name('guul draz assassin', 'Guul Draz Assassin').
card_type('guul draz assassin', 'Creature — Vampire Assassin').
card_types('guul draz assassin', ['Creature']).
card_subtypes('guul draz assassin', ['Vampire', 'Assassin']).
card_colors('guul draz assassin', ['B']).
card_text('guul draz assassin', 'Level up {1}{B} ({1}{B}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-3\n2/2\n{B}, {T}: Target creature gets -2/-2 until end of turn.\nLEVEL 4+\n4/4\n{B}, {T}: Target creature gets -4/-4 until end of turn.').
card_mana_cost('guul draz assassin', ['B']).
card_cmc('guul draz assassin', 1).
card_layout('guul draz assassin', 'leveler').
card_power('guul draz assassin', 1).
card_toughness('guul draz assassin', 1).

% Found in: BFZ
card_name('guul draz overseer', 'Guul Draz Overseer').
card_type('guul draz overseer', 'Creature — Vampire').
card_types('guul draz overseer', ['Creature']).
card_subtypes('guul draz overseer', ['Vampire']).
card_colors('guul draz overseer', ['B']).
card_text('guul draz overseer', 'Flying\nLandfall — Whenever a land enters the battlefield under your control, other creatures you control get +1/+0 until end of turn. If that land is a Swamp, those creatures get +2/+0 until end of turn instead.').
card_mana_cost('guul draz overseer', ['4', 'B', 'B']).
card_cmc('guul draz overseer', 6).
card_layout('guul draz overseer', 'normal').
card_power('guul draz overseer', 3).
card_toughness('guul draz overseer', 4).

% Found in: ZEN
card_name('guul draz specter', 'Guul Draz Specter').
card_type('guul draz specter', 'Creature — Specter').
card_types('guul draz specter', ['Creature']).
card_subtypes('guul draz specter', ['Specter']).
card_colors('guul draz specter', ['B']).
card_text('guul draz specter', 'Flying\nGuul Draz Specter gets +3/+3 as long as an opponent has no cards in hand.\nWhenever Guul Draz Specter deals combat damage to a player, that player discards a card.').
card_mana_cost('guul draz specter', ['2', 'B', 'B']).
card_cmc('guul draz specter', 4).
card_layout('guul draz specter', 'normal').
card_power('guul draz specter', 2).
card_toughness('guul draz specter', 2).

% Found in: ZEN
card_name('guul draz vampire', 'Guul Draz Vampire').
card_type('guul draz vampire', 'Creature — Vampire Rogue').
card_types('guul draz vampire', ['Creature']).
card_subtypes('guul draz vampire', ['Vampire', 'Rogue']).
card_colors('guul draz vampire', ['B']).
card_text('guul draz vampire', 'As long as an opponent has 10 or less life, Guul Draz Vampire gets +2/+1 and has intimidate. (It can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)').
card_mana_cost('guul draz vampire', ['B']).
card_cmc('guul draz vampire', 1).
card_layout('guul draz vampire', 'normal').
card_power('guul draz vampire', 1).
card_toughness('guul draz vampire', 1).

% Found in: CON
card_name('gwafa hazid, profiteer', 'Gwafa Hazid, Profiteer').
card_type('gwafa hazid, profiteer', 'Legendary Creature — Human Rogue').
card_types('gwafa hazid, profiteer', ['Creature']).
card_subtypes('gwafa hazid, profiteer', ['Human', 'Rogue']).
card_supertypes('gwafa hazid, profiteer', ['Legendary']).
card_colors('gwafa hazid, profiteer', ['W', 'U']).
card_text('gwafa hazid, profiteer', '{W}{U}, {T}: Put a bribery counter on target creature you don\'t control. Its controller draws a card.\nCreatures with bribery counters on them can\'t attack or block.').
card_mana_cost('gwafa hazid, profiteer', ['1', 'W', 'U']).
card_cmc('gwafa hazid, profiteer', 3).
card_layout('gwafa hazid, profiteer', 'normal').
card_power('gwafa hazid, profiteer', 2).
card_toughness('gwafa hazid, profiteer', 2).

% Found in: LEG, ME3
card_name('gwendlyn di corci', 'Gwendlyn Di Corci').
card_type('gwendlyn di corci', 'Legendary Creature — Human Rogue').
card_types('gwendlyn di corci', ['Creature']).
card_subtypes('gwendlyn di corci', ['Human', 'Rogue']).
card_supertypes('gwendlyn di corci', ['Legendary']).
card_colors('gwendlyn di corci', ['U', 'B', 'R']).
card_text('gwendlyn di corci', '{T}: Target player discards a card at random. Activate this ability only during your turn.').
card_mana_cost('gwendlyn di corci', ['U', 'B', 'B', 'R']).
card_cmc('gwendlyn di corci', 4).
card_layout('gwendlyn di corci', 'normal').
card_power('gwendlyn di corci', 3).
card_toughness('gwendlyn di corci', 5).
card_reserved('gwendlyn di corci').

% Found in: CMD, EVE
card_name('gwyllion hedge-mage', 'Gwyllion Hedge-Mage').
card_type('gwyllion hedge-mage', 'Creature — Hag Wizard').
card_types('gwyllion hedge-mage', ['Creature']).
card_subtypes('gwyllion hedge-mage', ['Hag', 'Wizard']).
card_colors('gwyllion hedge-mage', ['W', 'B']).
card_text('gwyllion hedge-mage', 'When Gwyllion Hedge-Mage enters the battlefield, if you control two or more Plains, you may put a 1/1 white Kithkin Soldier creature token onto the battlefield.\nWhen Gwyllion Hedge-Mage enters the battlefield, if you control two or more Swamps, you may put a -1/-1 counter on target creature.').
card_mana_cost('gwyllion hedge-mage', ['2', 'W/B']).
card_cmc('gwyllion hedge-mage', 3).
card_layout('gwyllion hedge-mage', 'normal').
card_power('gwyllion hedge-mage', 2).
card_toughness('gwyllion hedge-mage', 2).

% Found in: GTC
card_name('gyre sage', 'Gyre Sage').
card_type('gyre sage', 'Creature — Elf Druid').
card_types('gyre sage', ['Creature']).
card_subtypes('gyre sage', ['Elf', 'Druid']).
card_colors('gyre sage', ['G']).
card_text('gyre sage', 'Evolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)\n{T}: Add {G} to your mana pool for each +1/+1 counter on Gyre Sage.').
card_mana_cost('gyre sage', ['1', 'G']).
card_cmc('gyre sage', 2).
card_layout('gyre sage', 'normal').
card_power('gyre sage', 1).
card_toughness('gyre sage', 2).

