% Card-specific data

% Found in: ATQ, ME4
card_name('tablet of epityr', 'Tablet of Epityr').
card_type('tablet of epityr', 'Artifact').
card_types('tablet of epityr', ['Artifact']).
card_subtypes('tablet of epityr', []).
card_colors('tablet of epityr', []).
card_text('tablet of epityr', 'Whenever an artifact you control is put into a graveyard from the battlefield, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('tablet of epityr', ['1']).
card_cmc('tablet of epityr', 1).
card_layout('tablet of epityr', 'normal').

% Found in: RTR
card_name('tablet of the guilds', 'Tablet of the Guilds').
card_type('tablet of the guilds', 'Artifact').
card_types('tablet of the guilds', ['Artifact']).
card_subtypes('tablet of the guilds', []).
card_colors('tablet of the guilds', []).
card_text('tablet of the guilds', 'As Tablet of the Guilds enters the battlefield, choose two colors.\nWhenever you cast a spell, if it\'s at least one of the chosen colors, you gain 1 life for each of the chosen colors it is.').
card_mana_cost('tablet of the guilds', ['2']).
card_cmc('tablet of the guilds', 2).
card_layout('tablet of the guilds', 'normal').

% Found in: VAN
card_name('tahngarth', 'Tahngarth').
card_type('tahngarth', 'Vanguard').
card_types('tahngarth', ['Vanguard']).
card_subtypes('tahngarth', []).
card_colors('tahngarth', []).
card_text('tahngarth', 'Creatures you control have haste.').
card_layout('tahngarth', 'vanguard').

% Found in: APC
card_name('tahngarth\'s glare', 'Tahngarth\'s Glare').
card_type('tahngarth\'s glare', 'Sorcery').
card_types('tahngarth\'s glare', ['Sorcery']).
card_subtypes('tahngarth\'s glare', []).
card_colors('tahngarth\'s glare', ['R']).
card_text('tahngarth\'s glare', 'Look at the top three cards of target opponent\'s library, then put them back in any order. That player looks at the top three cards of your library, then puts them back in any order.').
card_mana_cost('tahngarth\'s glare', ['R']).
card_cmc('tahngarth\'s glare', 1).
card_layout('tahngarth\'s glare', 'normal').

% Found in: TMP
card_name('tahngarth\'s rage', 'Tahngarth\'s Rage').
card_type('tahngarth\'s rage', 'Enchantment — Aura').
card_types('tahngarth\'s rage', ['Enchantment']).
card_subtypes('tahngarth\'s rage', ['Aura']).
card_colors('tahngarth\'s rage', ['R']).
card_text('tahngarth\'s rage', 'Enchant creature\nEnchanted creature gets +3/+0 as long as it\'s attacking. Otherwise, it gets -2/-1.').
card_mana_cost('tahngarth\'s rage', ['R']).
card_cmc('tahngarth\'s rage', 1).
card_layout('tahngarth\'s rage', 'normal').

% Found in: PLS
card_name('tahngarth, talruum hero', 'Tahngarth, Talruum Hero').
card_type('tahngarth, talruum hero', 'Legendary Creature — Minotaur Warrior').
card_types('tahngarth, talruum hero', ['Creature']).
card_subtypes('tahngarth, talruum hero', ['Minotaur', 'Warrior']).
card_supertypes('tahngarth, talruum hero', ['Legendary']).
card_colors('tahngarth, talruum hero', ['R']).
card_text('tahngarth, talruum hero', 'Vigilance\n{1}{R}, {T}: Tahngarth, Talruum Hero deals damage equal to its power to target creature. That creature deals damage equal to its power to Tahngarth.').
card_mana_cost('tahngarth, talruum hero', ['3', 'R', 'R']).
card_cmc('tahngarth, talruum hero', 5).
card_layout('tahngarth, talruum hero', 'normal').
card_power('tahngarth, talruum hero', 4).
card_toughness('tahngarth, talruum hero', 4).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME2, ME4, VMA
card_name('taiga', 'Taiga').
card_type('taiga', 'Land — Mountain Forest').
card_types('taiga', ['Land']).
card_subtypes('taiga', ['Mountain', 'Forest']).
card_colors('taiga', []).
card_text('taiga', '({T}: Add {R} or {G} to your mana pool.)').
card_layout('taiga', 'normal').
card_reserved('taiga').

% Found in: KTK
card_name('taigam\'s scheming', 'Taigam\'s Scheming').
card_type('taigam\'s scheming', 'Sorcery').
card_types('taigam\'s scheming', ['Sorcery']).
card_subtypes('taigam\'s scheming', []).
card_colors('taigam\'s scheming', ['U']).
card_text('taigam\'s scheming', 'Look at the top five cards of your library. Put any number of them into your graveyard and the rest back on top of your library in any order.').
card_mana_cost('taigam\'s scheming', ['1', 'U']).
card_cmc('taigam\'s scheming', 2).
card_layout('taigam\'s scheming', 'normal').

% Found in: DTK
card_name('taigam\'s strike', 'Taigam\'s Strike').
card_type('taigam\'s strike', 'Sorcery').
card_types('taigam\'s strike', ['Sorcery']).
card_subtypes('taigam\'s strike', []).
card_colors('taigam\'s strike', ['U']).
card_text('taigam\'s strike', 'Target creature gets +2/+0 until end of turn and can\'t be blocked this turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('taigam\'s strike', ['3', 'U']).
card_cmc('taigam\'s strike', 4).
card_layout('taigam\'s strike', 'normal').

% Found in: DTK
card_name('tail slash', 'Tail Slash').
card_type('tail slash', 'Instant').
card_types('tail slash', ['Instant']).
card_subtypes('tail slash', []).
card_colors('tail slash', ['R']).
card_text('tail slash', 'Target creature you control deals damage equal to its power to target creature you don\'t control.').
card_mana_cost('tail slash', ['2', 'R']).
card_cmc('tail slash', 3).
card_layout('tail slash', 'normal').

% Found in: DDK, TOR
card_name('tainted field', 'Tainted Field').
card_type('tainted field', 'Land').
card_types('tainted field', ['Land']).
card_subtypes('tainted field', []).
card_colors('tainted field', []).
card_text('tainted field', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Activate this ability only if you control a Swamp.').
card_layout('tainted field', 'normal').

% Found in: PC2, TOR
card_name('tainted isle', 'Tainted Isle').
card_type('tainted isle', 'Land').
card_types('tainted isle', ['Land']).
card_subtypes('tainted isle', []).
card_colors('tainted isle', []).
card_text('tainted isle', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Activate this ability only if you control a Swamp.').
card_layout('tainted isle', 'normal').

% Found in: UNH
card_name('tainted monkey', 'Tainted Monkey').
card_type('tainted monkey', 'Creature — Ape').
card_types('tainted monkey', ['Creature']).
card_subtypes('tainted monkey', ['Ape']).
card_colors('tainted monkey', ['B']).
card_text('tainted monkey', '{T}: Choose a word. Target player puts the top card of his or her library into his or her graveyard. If that card has the chosen word in its text box, that player loses 3 life.').
card_mana_cost('tainted monkey', ['1', 'B']).
card_cmc('tainted monkey', 2).
card_layout('tainted monkey', 'normal').
card_power('tainted monkey', 1).
card_toughness('tainted monkey', 1).

% Found in: ODY
card_name('tainted pact', 'Tainted Pact').
card_type('tainted pact', 'Instant').
card_types('tainted pact', ['Instant']).
card_subtypes('tainted pact', []).
card_colors('tainted pact', ['B']).
card_text('tainted pact', 'Exile the top card of your library. You may put that card into your hand unless it has the same name as another card exiled this way. Repeat this process until you put a card into your hand or you exile two cards with the same name, whichever comes first.').
card_mana_cost('tainted pact', ['1', 'B']).
card_cmc('tainted pact', 2).
card_layout('tainted pact', 'normal').

% Found in: TOR
card_name('tainted peak', 'Tainted Peak').
card_type('tainted peak', 'Land').
card_types('tainted peak', ['Land']).
card_subtypes('tainted peak', []).
card_colors('tainted peak', []).
card_text('tainted peak', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Activate this ability only if you control a Swamp.').
card_layout('tainted peak', 'normal').

% Found in: ORI
card_name('tainted remedy', 'Tainted Remedy').
card_type('tainted remedy', 'Enchantment').
card_types('tainted remedy', ['Enchantment']).
card_subtypes('tainted remedy', []).
card_colors('tainted remedy', ['B']).
card_text('tainted remedy', 'If an opponent would gain life, that player loses that much life instead.').
card_mana_cost('tainted remedy', ['2', 'B']).
card_cmc('tainted remedy', 3).
card_layout('tainted remedy', 'normal').

% Found in: ARB
card_name('tainted sigil', 'Tainted Sigil').
card_type('tainted sigil', 'Artifact').
card_types('tainted sigil', ['Artifact']).
card_subtypes('tainted sigil', []).
card_colors('tainted sigil', ['W', 'B']).
card_text('tainted sigil', '{T}, Sacrifice Tainted Sigil: You gain life equal to the total life lost by all players this turn. (Damage causes loss of life.)').
card_mana_cost('tainted sigil', ['1', 'W', 'B']).
card_cmc('tainted sigil', 3).
card_layout('tainted sigil', 'normal').

% Found in: MIR
card_name('tainted specter', 'Tainted Specter').
card_type('tainted specter', 'Creature — Specter').
card_types('tainted specter', ['Creature']).
card_subtypes('tainted specter', ['Specter']).
card_colors('tainted specter', ['B']).
card_text('tainted specter', 'Flying\n{1}{B}{B}, {T}: Target player discards a card unless he or she puts a card from his or her hand on top of his or her library. If that player discards a card this way, Tainted Specter deals 1 damage to each creature and each player. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('tainted specter', ['3', 'B']).
card_cmc('tainted specter', 4).
card_layout('tainted specter', 'normal').
card_power('tainted specter', 2).
card_toughness('tainted specter', 2).
card_reserved('tainted specter').

% Found in: SOM
card_name('tainted strike', 'Tainted Strike').
card_type('tainted strike', 'Instant').
card_types('tainted strike', ['Instant']).
card_subtypes('tainted strike', []).
card_colors('tainted strike', ['B']).
card_text('tainted strike', 'Target creature gets +1/+0 and gains infect until end of turn. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('tainted strike', ['B']).
card_cmc('tainted strike', 1).
card_layout('tainted strike', 'normal').

% Found in: INV
card_name('tainted well', 'Tainted Well').
card_type('tainted well', 'Enchantment — Aura').
card_types('tainted well', ['Enchantment']).
card_subtypes('tainted well', ['Aura']).
card_colors('tainted well', ['B']).
card_text('tainted well', 'Enchant land\nWhen Tainted Well enters the battlefield, draw a card.\nEnchanted land is a Swamp.').
card_mana_cost('tainted well', ['2', 'B']).
card_cmc('tainted well', 3).
card_layout('tainted well', 'normal').

% Found in: DDM, TOR
card_name('tainted wood', 'Tainted Wood').
card_type('tainted wood', 'Land').
card_types('tainted wood', ['Land']).
card_subtypes('tainted wood', []).
card_colors('tainted wood', []).
card_text('tainted wood', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {G} to your mana pool. Activate this ability only if you control a Swamp.').
card_layout('tainted wood', 'normal').

% Found in: 7ED, USG
card_name('tainted æther', 'Tainted Æther').
card_type('tainted æther', 'Enchantment').
card_types('tainted æther', ['Enchantment']).
card_subtypes('tainted æther', []).
card_colors('tainted æther', ['B']).
card_text('tainted æther', 'Whenever a creature enters the battlefield, its controller sacrifices a creature or land.').
card_mana_cost('tainted æther', ['2', 'B', 'B']).
card_cmc('tainted æther', 4).
card_layout('tainted æther', 'normal').

% Found in: MM2, MRD
card_name('taj-nar swordsmith', 'Taj-Nar Swordsmith').
card_type('taj-nar swordsmith', 'Creature — Cat Soldier').
card_types('taj-nar swordsmith', ['Creature']).
card_subtypes('taj-nar swordsmith', ['Cat', 'Soldier']).
card_colors('taj-nar swordsmith', ['W']).
card_text('taj-nar swordsmith', 'When Taj-Nar Swordsmith enters the battlefield, you may pay {X}. If you do, search your library for an Equipment card with converted mana cost X or less and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('taj-nar swordsmith', ['3', 'W']).
card_cmc('taj-nar swordsmith', 4).
card_layout('taj-nar swordsmith', 'normal').
card_power('taj-nar swordsmith', 2).
card_toughness('taj-nar swordsmith', 3).

% Found in: DGM
card_name('tajic, blade of the legion', 'Tajic, Blade of the Legion').
card_type('tajic, blade of the legion', 'Legendary Creature — Human Soldier').
card_types('tajic, blade of the legion', ['Creature']).
card_subtypes('tajic, blade of the legion', ['Human', 'Soldier']).
card_supertypes('tajic, blade of the legion', ['Legendary']).
card_colors('tajic, blade of the legion', ['W', 'R']).
card_text('tajic, blade of the legion', 'Indestructible\nBattalion — Whenever Tajic, Blade of the Legion and at least two other creatures attack, Tajic gets +5/+5 until end of turn.').
card_mana_cost('tajic, blade of the legion', ['2', 'R', 'W']).
card_cmc('tajic, blade of the legion', 4).
card_layout('tajic, blade of the legion', 'normal').
card_power('tajic, blade of the legion', 2).
card_toughness('tajic, blade of the legion', 2).

% Found in: DDP, ZEN
card_name('tajuru archer', 'Tajuru Archer').
card_type('tajuru archer', 'Creature — Elf Archer Ally').
card_types('tajuru archer', ['Creature']).
card_subtypes('tajuru archer', ['Elf', 'Archer', 'Ally']).
card_colors('tajuru archer', ['G']).
card_text('tajuru archer', 'Whenever Tajuru Archer or another Ally enters the battlefield under your control, you may have Tajuru Archer deal damage to target creature with flying equal to the number of Allies you control.').
card_mana_cost('tajuru archer', ['2', 'G']).
card_cmc('tajuru archer', 3).
card_layout('tajuru archer', 'normal').
card_power('tajuru archer', 1).
card_toughness('tajuru archer', 2).

% Found in: BFZ
card_name('tajuru beastmaster', 'Tajuru Beastmaster').
card_type('tajuru beastmaster', 'Creature — Elf Warrior Ally').
card_types('tajuru beastmaster', ['Creature']).
card_subtypes('tajuru beastmaster', ['Elf', 'Warrior', 'Ally']).
card_colors('tajuru beastmaster', ['G']).
card_text('tajuru beastmaster', 'Rally — Whenever Tajuru Beastmaster or another Ally enters the battlefield under your control, creatures you control get +1/+1 until end of turn.').
card_mana_cost('tajuru beastmaster', ['5', 'G']).
card_cmc('tajuru beastmaster', 6).
card_layout('tajuru beastmaster', 'normal').
card_power('tajuru beastmaster', 5).
card_toughness('tajuru beastmaster', 5).

% Found in: ROE
card_name('tajuru preserver', 'Tajuru Preserver').
card_type('tajuru preserver', 'Creature — Elf Shaman').
card_types('tajuru preserver', ['Creature']).
card_subtypes('tajuru preserver', ['Elf', 'Shaman']).
card_colors('tajuru preserver', ['G']).
card_text('tajuru preserver', 'Spells and abilities your opponents control can\'t cause you to sacrifice permanents.').
card_mana_cost('tajuru preserver', ['1', 'G']).
card_cmc('tajuru preserver', 2).
card_layout('tajuru preserver', 'normal').
card_power('tajuru preserver', 2).
card_toughness('tajuru preserver', 1).

% Found in: BFZ
card_name('tajuru stalwart', 'Tajuru Stalwart').
card_type('tajuru stalwart', 'Creature — Elf Scout Ally').
card_types('tajuru stalwart', ['Creature']).
card_subtypes('tajuru stalwart', ['Elf', 'Scout', 'Ally']).
card_colors('tajuru stalwart', ['G']).
card_text('tajuru stalwart', 'Converge — Tajuru Stalwart enters the battlefield with a +1/+1 counter on it for each color of mana spent to cast it.').
card_mana_cost('tajuru stalwart', ['2', 'G']).
card_cmc('tajuru stalwart', 3).
card_layout('tajuru stalwart', 'normal').
card_power('tajuru stalwart', 0).
card_toughness('tajuru stalwart', 1).

% Found in: BFZ
card_name('tajuru warcaller', 'Tajuru Warcaller').
card_type('tajuru warcaller', 'Creature — Elf Warrior Ally').
card_types('tajuru warcaller', ['Creature']).
card_subtypes('tajuru warcaller', ['Elf', 'Warrior', 'Ally']).
card_colors('tajuru warcaller', ['G']).
card_text('tajuru warcaller', 'Rally — Whenever Tajuru Warcaller or another Ally enters the battlefield under your control, creatures you control get +2/+2 until end of turn.').
card_mana_cost('tajuru warcaller', ['3', 'G', 'G']).
card_cmc('tajuru warcaller', 5).
card_layout('tajuru warcaller', 'normal').
card_power('tajuru warcaller', 2).
card_toughness('tajuru warcaller', 1).

% Found in: VAN
card_name('takara', 'Takara').
card_type('takara', 'Vanguard').
card_types('takara', ['Vanguard']).
card_subtypes('takara', []).
card_colors('takara', []).
card_text('takara', 'Sacrifice a creature: Takara deals 1 damage to target creature or player.').
card_layout('takara', 'vanguard').

% Found in: DGM
card_name('take', 'Take').
card_type('take', 'Sorcery').
card_types('take', ['Sorcery']).
card_subtypes('take', []).
card_colors('take', ['U']).
card_text('take', 'Remove all +1/+1 counters from target creature you control. Draw that many cards.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('take', ['2', 'U']).
card_cmc('take', 3).
card_layout('take', 'split').

% Found in: FUT, MMA
card_name('take possession', 'Take Possession').
card_type('take possession', 'Enchantment — Aura').
card_types('take possession', ['Enchantment']).
card_subtypes('take possession', ['Aura']).
card_colors('take possession', ['U']).
card_text('take possession', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nEnchant permanent\nYou control enchanted permanent.').
card_mana_cost('take possession', ['5', 'U', 'U']).
card_cmc('take possession', 7).
card_layout('take possession', 'normal').

% Found in: KTK
card_name('take up arms', 'Take Up Arms').
card_type('take up arms', 'Instant').
card_types('take up arms', ['Instant']).
card_subtypes('take up arms', []).
card_colors('take up arms', ['W']).
card_text('take up arms', 'Put three 1/1 white Warrior creature tokens onto the battlefield.').
card_mana_cost('take up arms', ['4', 'W']).
card_cmc('take up arms', 5).
card_layout('take up arms', 'normal').

% Found in: BOK
card_name('takeno\'s cavalry', 'Takeno\'s Cavalry').
card_type('takeno\'s cavalry', 'Creature — Human Samurai Archer').
card_types('takeno\'s cavalry', ['Creature']).
card_subtypes('takeno\'s cavalry', ['Human', 'Samurai', 'Archer']).
card_colors('takeno\'s cavalry', ['W']).
card_text('takeno\'s cavalry', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{T}: Takeno\'s Cavalry deals 1 damage to target attacking or blocking Spirit.').
card_mana_cost('takeno\'s cavalry', ['3', 'W']).
card_cmc('takeno\'s cavalry', 4).
card_layout('takeno\'s cavalry', 'normal').
card_power('takeno\'s cavalry', 1).
card_toughness('takeno\'s cavalry', 1).

% Found in: CHK
card_name('takeno, samurai general', 'Takeno, Samurai General').
card_type('takeno, samurai general', 'Legendary Creature — Human Samurai').
card_types('takeno, samurai general', ['Creature']).
card_subtypes('takeno, samurai general', ['Human', 'Samurai']).
card_supertypes('takeno, samurai general', ['Legendary']).
card_colors('takeno, samurai general', ['W']).
card_text('takeno, samurai general', 'Bushido 2 (Whenever this creature blocks or becomes blocked, it gets +2/+2 until end of turn.)\nEach other Samurai creature you control gets +1/+1 for each point of bushido it has.').
card_mana_cost('takeno, samurai general', ['5', 'W']).
card_cmc('takeno, samurai general', 6).
card_layout('takeno, samurai general', 'normal').
card_power('takeno, samurai general', 3).
card_toughness('takeno, samurai general', 3).

% Found in: PC2
card_name('takenuma', 'Takenuma').
card_type('takenuma', 'Plane — Kamigawa').
card_types('takenuma', ['Plane']).
card_subtypes('takenuma', ['Kamigawa']).
card_colors('takenuma', []).
card_text('takenuma', 'Whenever a creature leaves the battlefield, its controller draws a card.\nWhenever you roll {C}, return target creature you control to its owner\'s hand.').
card_layout('takenuma', 'plane').

% Found in: BOK
card_name('takenuma bleeder', 'Takenuma Bleeder').
card_type('takenuma bleeder', 'Creature — Ogre Shaman').
card_types('takenuma bleeder', ['Creature']).
card_subtypes('takenuma bleeder', ['Ogre', 'Shaman']).
card_colors('takenuma bleeder', ['B']).
card_text('takenuma bleeder', 'Whenever Takenuma Bleeder attacks or blocks, you lose 1 life if you don\'t control a Demon.').
card_mana_cost('takenuma bleeder', ['2', 'B']).
card_cmc('takenuma bleeder', 3).
card_layout('takenuma bleeder', 'normal').
card_power('takenuma bleeder', 3).
card_toughness('takenuma bleeder', 3).

% Found in: CHR, LEG, ME3
card_name('takklemaggot', 'Takklemaggot').
card_type('takklemaggot', 'Enchantment — Aura').
card_types('takklemaggot', ['Enchantment']).
card_subtypes('takklemaggot', ['Aura']).
card_colors('takklemaggot', ['B']).
card_text('takklemaggot', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, put a -0/-1 counter on that creature.\nWhen enchanted creature dies, that creature\'s controller chooses a creature that Takklemaggot could enchant. If he or she does, return Takklemaggot to the battlefield under your control attached to that creature. If he or she doesn\'t, return Takklemaggot to the battlefield under your control as a non-Aura enchantment. It loses \"enchant creature\" and gains \"At the beginning of that player\'s upkeep, Takklemaggot deals 1 damage to him or her.\"').
card_mana_cost('takklemaggot', ['2', 'B', 'B']).
card_cmc('takklemaggot', 4).
card_layout('takklemaggot', 'normal').

% Found in: EVE
card_name('talara\'s bane', 'Talara\'s Bane').
card_type('talara\'s bane', 'Sorcery').
card_types('talara\'s bane', ['Sorcery']).
card_subtypes('talara\'s bane', []).
card_colors('talara\'s bane', ['B']).
card_text('talara\'s bane', 'Target opponent reveals his or her hand. You choose a green or white creature card from it. You gain life equal to that creature card\'s toughness, then that player discards that card.').
card_mana_cost('talara\'s bane', ['1', 'B']).
card_cmc('talara\'s bane', 2).
card_layout('talara\'s bane', 'normal').

% Found in: DPA, EVE
card_name('talara\'s battalion', 'Talara\'s Battalion').
card_type('talara\'s battalion', 'Creature — Elf Warrior').
card_types('talara\'s battalion', ['Creature']).
card_subtypes('talara\'s battalion', ['Elf', 'Warrior']).
card_colors('talara\'s battalion', ['G']).
card_text('talara\'s battalion', 'Trample\nCast Talara\'s Battalion only if you\'ve cast another green spell this turn.').
card_mana_cost('talara\'s battalion', ['1', 'G']).
card_cmc('talara\'s battalion', 2).
card_layout('talara\'s battalion', 'normal').
card_power('talara\'s battalion', 4).
card_toughness('talara\'s battalion', 3).

% Found in: PO2
card_name('talas air ship', 'Talas Air Ship').
card_type('talas air ship', 'Creature — Human Pirate').
card_types('talas air ship', ['Creature']).
card_subtypes('talas air ship', ['Human', 'Pirate']).
card_colors('talas air ship', ['U']).
card_text('talas air ship', 'Flying').
card_mana_cost('talas air ship', ['3', 'U']).
card_cmc('talas air ship', 4).
card_layout('talas air ship', 'normal').
card_power('talas air ship', 3).
card_toughness('talas air ship', 2).

% Found in: PO2
card_name('talas explorer', 'Talas Explorer').
card_type('talas explorer', 'Creature — Human Pirate Scout').
card_types('talas explorer', ['Creature']).
card_subtypes('talas explorer', ['Human', 'Pirate', 'Scout']).
card_colors('talas explorer', ['U']).
card_text('talas explorer', 'Flying\nWhen Talas Explorer enters the battlefield, look at target opponent\'s hand.').
card_mana_cost('talas explorer', ['1', 'U']).
card_cmc('talas explorer', 2).
card_layout('talas explorer', 'normal').
card_power('talas explorer', 1).
card_toughness('talas explorer', 1).

% Found in: PO2
card_name('talas merchant', 'Talas Merchant').
card_type('talas merchant', 'Creature — Human Pirate').
card_types('talas merchant', ['Creature']).
card_subtypes('talas merchant', ['Human', 'Pirate']).
card_colors('talas merchant', ['U']).
card_text('talas merchant', '').
card_mana_cost('talas merchant', ['1', 'U']).
card_cmc('talas merchant', 2).
card_layout('talas merchant', 'normal').
card_power('talas merchant', 1).
card_toughness('talas merchant', 3).

% Found in: ME4, PO2
card_name('talas researcher', 'Talas Researcher').
card_type('talas researcher', 'Creature — Human Pirate Wizard').
card_types('talas researcher', ['Creature']).
card_subtypes('talas researcher', ['Human', 'Pirate', 'Wizard']).
card_colors('talas researcher', ['U']).
card_text('talas researcher', '{T}: Draw a card. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('talas researcher', ['4', 'U']).
card_cmc('talas researcher', 5).
card_layout('talas researcher', 'normal').
card_power('talas researcher', 1).
card_toughness('talas researcher', 1).

% Found in: PO2
card_name('talas scout', 'Talas Scout').
card_type('talas scout', 'Creature — Human Pirate Scout').
card_types('talas scout', ['Creature']).
card_subtypes('talas scout', ['Human', 'Pirate', 'Scout']).
card_colors('talas scout', ['U']).
card_text('talas scout', 'Flying').
card_mana_cost('talas scout', ['1', 'U']).
card_cmc('talas scout', 2).
card_layout('talas scout', 'normal').
card_power('talas scout', 1).
card_toughness('talas scout', 2).

% Found in: PO2
card_name('talas warrior', 'Talas Warrior').
card_type('talas warrior', 'Creature — Human Pirate Warrior').
card_types('talas warrior', ['Creature']).
card_subtypes('talas warrior', ['Human', 'Pirate', 'Warrior']).
card_colors('talas warrior', ['U']).
card_text('talas warrior', 'Talas Warrior can\'t be blocked.').
card_mana_cost('talas warrior', ['1', 'U', 'U']).
card_cmc('talas warrior', 3).
card_layout('talas warrior', 'normal').
card_power('talas warrior', 2).
card_toughness('talas warrior', 2).

% Found in: ORI
card_name('talent of the telepath', 'Talent of the Telepath').
card_type('talent of the telepath', 'Sorcery').
card_types('talent of the telepath', ['Sorcery']).
card_subtypes('talent of the telepath', []).
card_colors('talent of the telepath', ['U']).
card_text('talent of the telepath', 'Target opponent reveals the top seven cards of his or her library. You may cast an instant or sorcery card from among them without paying its mana cost. Then that player puts the rest into his or her graveyard.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, you may cast up to two revealed instant and/or sorcery cards instead of one.').
card_mana_cost('talent of the telepath', ['2', 'U', 'U']).
card_cmc('talent of the telepath', 4).
card_layout('talent of the telepath', 'normal').

% Found in: MRD
card_name('talisman of dominance', 'Talisman of Dominance').
card_type('talisman of dominance', 'Artifact').
card_types('talisman of dominance', ['Artifact']).
card_subtypes('talisman of dominance', []).
card_colors('talisman of dominance', []).
card_text('talisman of dominance', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Talisman of Dominance deals 1 damage to you.').
card_mana_cost('talisman of dominance', ['2']).
card_cmc('talisman of dominance', 2).
card_layout('talisman of dominance', 'normal').

% Found in: MRD
card_name('talisman of impulse', 'Talisman of Impulse').
card_type('talisman of impulse', 'Artifact').
card_types('talisman of impulse', ['Artifact']).
card_subtypes('talisman of impulse', []).
card_colors('talisman of impulse', []).
card_text('talisman of impulse', '{T}: Add {1} to your mana pool.\n{T}: Add {R} or {G} to your mana pool. Talisman of Impulse deals 1 damage to you.').
card_mana_cost('talisman of impulse', ['2']).
card_cmc('talisman of impulse', 2).
card_layout('talisman of impulse', 'normal').

% Found in: MRD
card_name('talisman of indulgence', 'Talisman of Indulgence').
card_type('talisman of indulgence', 'Artifact').
card_types('talisman of indulgence', ['Artifact']).
card_subtypes('talisman of indulgence', []).
card_colors('talisman of indulgence', []).
card_text('talisman of indulgence', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Talisman of Indulgence deals 1 damage to you.').
card_mana_cost('talisman of indulgence', ['2']).
card_cmc('talisman of indulgence', 2).
card_layout('talisman of indulgence', 'normal').

% Found in: MRD
card_name('talisman of progress', 'Talisman of Progress').
card_type('talisman of progress', 'Artifact').
card_types('talisman of progress', ['Artifact']).
card_subtypes('talisman of progress', []).
card_colors('talisman of progress', []).
card_text('talisman of progress', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Talisman of Progress deals 1 damage to you.').
card_mana_cost('talisman of progress', ['2']).
card_cmc('talisman of progress', 2).
card_layout('talisman of progress', 'normal').

% Found in: MRD
card_name('talisman of unity', 'Talisman of Unity').
card_type('talisman of unity', 'Artifact').
card_types('talisman of unity', ['Artifact']).
card_subtypes('talisman of unity', []).
card_colors('talisman of unity', []).
card_text('talisman of unity', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Talisman of Unity deals 1 damage to you.').
card_mana_cost('talisman of unity', ['2']).
card_cmc('talisman of unity', 2).
card_layout('talisman of unity', 'normal').

% Found in: BOK
card_name('tallowisp', 'Tallowisp').
card_type('tallowisp', 'Creature — Spirit').
card_types('tallowisp', ['Creature']).
card_subtypes('tallowisp', ['Spirit']).
card_colors('tallowisp', ['W']).
card_text('tallowisp', 'Whenever you cast a Spirit or Arcane spell, you may search your library for an Aura card with enchant creature, reveal it, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('tallowisp', ['1', 'W']).
card_cmc('tallowisp', 2).
card_layout('tallowisp', 'normal').
card_power('tallowisp', 1).
card_toughness('tallowisp', 3).

% Found in: PC2
card_name('talon gates', 'Talon Gates').
card_type('talon gates', 'Plane — Dominaria').
card_types('talon gates', ['Plane']).
card_subtypes('talon gates', ['Dominaria']).
card_colors('talon gates', []).
card_text('talon gates', 'Any time you could cast a sorcery, you may exile a nonland card from your hand with X time counters on it, where X is its converted mana cost. If the exiled card doesn\'t have suspend, it gains suspend. (At the beginning of its owner\'s upkeep, he or she removes a time counter. When the last is removed, the player casts it without paying its mana cost. If it\'s a creature, it has haste.)\nWhenever you roll {C}, remove two time counters from each suspended card you own.').
card_layout('talon gates', 'plane').

% Found in: DST
card_name('talon of pain', 'Talon of Pain').
card_type('talon of pain', 'Artifact').
card_types('talon of pain', ['Artifact']).
card_subtypes('talon of pain', []).
card_colors('talon of pain', []).
card_text('talon of pain', 'Whenever a source you control other than Talon of Pain deals damage to an opponent, put a charge counter on Talon of Pain.\n{X}, {T}, Remove X charge counters from Talon of Pain: Talon of Pain deals X damage to target creature or player.').
card_mana_cost('talon of pain', ['4']).
card_cmc('talon of pain', 4).
card_layout('talon of pain', 'normal').

% Found in: TMP
card_name('talon sliver', 'Talon Sliver').
card_type('talon sliver', 'Creature — Sliver').
card_types('talon sliver', ['Creature']).
card_subtypes('talon sliver', ['Sliver']).
card_colors('talon sliver', ['W']).
card_text('talon sliver', 'All Sliver creatures have first strike.').
card_mana_cost('talon sliver', ['1', 'W']).
card_cmc('talon sliver', 2).
card_layout('talon sliver', 'normal').
card_power('talon sliver', 1).
card_toughness('talon sliver', 1).

% Found in: ARB
card_name('talon trooper', 'Talon Trooper').
card_type('talon trooper', 'Creature — Bird Scout').
card_types('talon trooper', ['Creature']).
card_subtypes('talon trooper', ['Bird', 'Scout']).
card_colors('talon trooper', ['W', 'U']).
card_text('talon trooper', 'Flying').
card_mana_cost('talon trooper', ['1', 'W', 'U']).
card_cmc('talon trooper', 3).
card_layout('talon trooper', 'normal').
card_power('talon trooper', 2).
card_toughness('talon trooper', 3).

% Found in: EVE
card_name('talonrend', 'Talonrend').
card_type('talonrend', 'Creature — Elemental').
card_types('talonrend', ['Creature']).
card_subtypes('talonrend', ['Elemental']).
card_colors('talonrend', ['U']).
card_text('talonrend', 'Flying\n{U/R}: Talonrend gets +1/-1 until end of turn.').
card_mana_cost('talonrend', ['4', 'U']).
card_cmc('talonrend', 5).
card_layout('talonrend', 'normal').
card_power('talonrend', 0).
card_toughness('talonrend', 5).

% Found in: DKA
card_name('talons of falkenrath', 'Talons of Falkenrath').
card_type('talons of falkenrath', 'Enchantment — Aura').
card_types('talons of falkenrath', ['Enchantment']).
card_subtypes('talons of falkenrath', ['Aura']).
card_colors('talons of falkenrath', ['R']).
card_text('talons of falkenrath', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature has \"{1}{R}: This creature gets +2/+0 until end of turn.\"').
card_mana_cost('talons of falkenrath', ['1', 'R']).
card_cmc('talons of falkenrath', 2).
card_layout('talons of falkenrath', 'normal').

% Found in: M13
card_name('talrand\'s invocation', 'Talrand\'s Invocation').
card_type('talrand\'s invocation', 'Sorcery').
card_types('talrand\'s invocation', ['Sorcery']).
card_subtypes('talrand\'s invocation', []).
card_colors('talrand\'s invocation', ['U']).
card_text('talrand\'s invocation', 'Put two 2/2 blue Drake creature tokens with flying onto the battlefield.').
card_mana_cost('talrand\'s invocation', ['2', 'U', 'U']).
card_cmc('talrand\'s invocation', 4).
card_layout('talrand\'s invocation', 'normal').

% Found in: M13
card_name('talrand, sky summoner', 'Talrand, Sky Summoner').
card_type('talrand, sky summoner', 'Legendary Creature — Merfolk Wizard').
card_types('talrand, sky summoner', ['Creature']).
card_subtypes('talrand, sky summoner', ['Merfolk', 'Wizard']).
card_supertypes('talrand, sky summoner', ['Legendary']).
card_colors('talrand, sky summoner', ['U']).
card_text('talrand, sky summoner', 'Whenever you cast an instant or sorcery spell, put a 2/2 blue Drake creature token with flying onto the battlefield.').
card_mana_cost('talrand, sky summoner', ['2', 'U', 'U']).
card_cmc('talrand, sky summoner', 4).
card_layout('talrand, sky summoner', 'normal').
card_power('talrand, sky summoner', 2).
card_toughness('talrand, sky summoner', 2).

% Found in: VIS
card_name('talruum champion', 'Talruum Champion').
card_type('talruum champion', 'Creature — Minotaur').
card_types('talruum champion', ['Creature']).
card_subtypes('talruum champion', ['Minotaur']).
card_colors('talruum champion', ['R']).
card_text('talruum champion', 'First strike\nWhenever Talruum Champion blocks or becomes blocked by a creature, that creature loses first strike until end of turn.').
card_mana_cost('talruum champion', ['4', 'R']).
card_cmc('talruum champion', 5).
card_layout('talruum champion', 'normal').
card_power('talruum champion', 3).
card_toughness('talruum champion', 3).

% Found in: 6ED, BTD, MIR
card_name('talruum minotaur', 'Talruum Minotaur').
card_type('talruum minotaur', 'Creature — Minotaur Berserker').
card_types('talruum minotaur', ['Creature']).
card_subtypes('talruum minotaur', ['Minotaur', 'Berserker']).
card_colors('talruum minotaur', ['R']).
card_text('talruum minotaur', 'Haste').
card_mana_cost('talruum minotaur', ['2', 'R', 'R']).
card_cmc('talruum minotaur', 4).
card_layout('talruum minotaur', 'normal').
card_power('talruum minotaur', 3).
card_toughness('talruum minotaur', 3).

% Found in: VIS
card_name('talruum piper', 'Talruum Piper').
card_type('talruum piper', 'Creature — Minotaur').
card_types('talruum piper', ['Creature']).
card_subtypes('talruum piper', ['Minotaur']).
card_colors('talruum piper', ['R']).
card_text('talruum piper', 'All creatures with flying able to block Talruum Piper do so.').
card_mana_cost('talruum piper', ['4', 'R']).
card_cmc('talruum piper', 5).
card_layout('talruum piper', 'normal').
card_power('talruum piper', 3).
card_toughness('talruum piper', 3).

% Found in: WWK
card_name('talus paladin', 'Talus Paladin').
card_type('talus paladin', 'Creature — Human Knight Ally').
card_types('talus paladin', ['Creature']).
card_subtypes('talus paladin', ['Human', 'Knight', 'Ally']).
card_colors('talus paladin', ['W']).
card_text('talus paladin', 'Whenever Talus Paladin or another Ally enters the battlefield under your control, you may have Allies you control gain lifelink until end of turn, and you may put a +1/+1 counter on Talus Paladin.').
card_mana_cost('talus paladin', ['3', 'W']).
card_cmc('talus paladin', 4).
card_layout('talus paladin', 'normal').
card_power('talus paladin', 2).
card_toughness('talus paladin', 3).

% Found in: CSP
card_name('tamanoa', 'Tamanoa').
card_type('tamanoa', 'Creature — Spirit').
card_types('tamanoa', ['Creature']).
card_subtypes('tamanoa', ['Spirit']).
card_colors('tamanoa', ['W', 'R', 'G']).
card_text('tamanoa', 'Whenever a noncreature source you control deals damage, you gain that much life.').
card_mana_cost('tamanoa', ['R', 'G', 'W']).
card_cmc('tamanoa', 3).
card_layout('tamanoa', 'normal').
card_power('tamanoa', 2).
card_toughness('tamanoa', 4).

% Found in: AVR
card_name('tamiyo, the moon sage', 'Tamiyo, the Moon Sage').
card_type('tamiyo, the moon sage', 'Planeswalker — Tamiyo').
card_types('tamiyo, the moon sage', ['Planeswalker']).
card_subtypes('tamiyo, the moon sage', ['Tamiyo']).
card_colors('tamiyo, the moon sage', ['U']).
card_text('tamiyo, the moon sage', '+1: Tap target permanent. It doesn\'t untap during its controller\'s next untap step.\n−2: Draw a card for each tapped creature target player controls.\n−8: You get an emblem with \"You have no maximum hand size\" and \"Whenever a card is put into your graveyard from anywhere, you may return it to your hand.\"').
card_mana_cost('tamiyo, the moon sage', ['3', 'U', 'U']).
card_cmc('tamiyo, the moon sage', 5).
card_layout('tamiyo, the moon sage', 'normal').
card_loyalty('tamiyo, the moon sage', 4).

% Found in: AVR
card_name('tandem lookout', 'Tandem Lookout').
card_type('tandem lookout', 'Creature — Human Scout').
card_types('tandem lookout', ['Creature']).
card_subtypes('tandem lookout', ['Human', 'Scout']).
card_colors('tandem lookout', ['U']).
card_text('tandem lookout', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Tandem Lookout is paired with another creature, each of those creatures has \"Whenever this creature deals damage to an opponent, draw a card.\"').
card_mana_cost('tandem lookout', ['2', 'U']).
card_cmc('tandem lookout', 3).
card_layout('tandem lookout', 'normal').
card_power('tandem lookout', 2).
card_toughness('tandem lookout', 1).

% Found in: BFZ
card_name('tandem tactics', 'Tandem Tactics').
card_type('tandem tactics', 'Instant').
card_types('tandem tactics', ['Instant']).
card_subtypes('tandem tactics', []).
card_colors('tandem tactics', ['W']).
card_text('tandem tactics', 'Up to two target creatures each get +1/+2 until end of turn. You gain 2 life.').
card_mana_cost('tandem tactics', ['1', 'W']).
card_cmc('tandem tactics', 2).
card_layout('tandem tactics', 'normal').

% Found in: INV, VMA
card_name('tangle', 'Tangle').
card_type('tangle', 'Instant').
card_types('tangle', ['Instant']).
card_subtypes('tangle', []).
card_colors('tangle', ['G']).
card_text('tangle', 'Prevent all combat damage that would be dealt this turn.\nEach attacking creature doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('tangle', ['1', 'G']).
card_cmc('tangle', 2).
card_layout('tangle', 'normal').

% Found in: SOM
card_name('tangle angler', 'Tangle Angler').
card_type('tangle angler', 'Creature — Horror').
card_types('tangle angler', ['Creature']).
card_subtypes('tangle angler', ['Horror']).
card_colors('tangle angler', ['G']).
card_text('tangle angler', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{G}: Target creature blocks Tangle Angler this turn if able.').
card_mana_cost('tangle angler', ['3', 'G']).
card_cmc('tangle angler', 4).
card_layout('tangle angler', 'normal').
card_power('tangle angler', 1).
card_toughness('tangle angler', 5).

% Found in: 5DN
card_name('tangle asp', 'Tangle Asp').
card_type('tangle asp', 'Creature — Snake').
card_types('tangle asp', ['Creature']).
card_subtypes('tangle asp', ['Snake']).
card_colors('tangle asp', ['G']).
card_text('tangle asp', 'Whenever Tangle Asp blocks or becomes blocked by a creature, destroy that creature at end of combat.').
card_mana_cost('tangle asp', ['1', 'G']).
card_cmc('tangle asp', 2).
card_layout('tangle asp', 'normal').
card_power('tangle asp', 1).
card_toughness('tangle asp', 2).

% Found in: DST
card_name('tangle golem', 'Tangle Golem').
card_type('tangle golem', 'Artifact Creature — Golem').
card_types('tangle golem', ['Artifact', 'Creature']).
card_subtypes('tangle golem', ['Golem']).
card_colors('tangle golem', []).
card_text('tangle golem', 'Affinity for Forests (This spell costs {1} less to cast for each Forest you control.)').
card_mana_cost('tangle golem', ['7']).
card_cmc('tangle golem', 7).
card_layout('tangle golem', 'normal').
card_power('tangle golem', 5).
card_toughness('tangle golem', 4).

% Found in: MBS
card_name('tangle hulk', 'Tangle Hulk').
card_type('tangle hulk', 'Artifact Creature — Beast').
card_types('tangle hulk', ['Artifact', 'Creature']).
card_subtypes('tangle hulk', ['Beast']).
card_colors('tangle hulk', []).
card_text('tangle hulk', '{2}{G}: Regenerate Tangle Hulk.').
card_mana_cost('tangle hulk', ['5']).
card_cmc('tangle hulk', 5).
card_layout('tangle hulk', 'normal').
card_power('tangle hulk', 5).
card_toughness('tangle hulk', 3).

% Found in: DRK
card_name('tangle kelp', 'Tangle Kelp').
card_type('tangle kelp', 'Enchantment — Aura').
card_types('tangle kelp', ['Enchantment']).
card_subtypes('tangle kelp', ['Aura']).
card_colors('tangle kelp', ['U']).
card_text('tangle kelp', 'Enchant creature\nWhen Tangle Kelp enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step if it attacked during its controller\'s last turn.').
card_mana_cost('tangle kelp', ['U']).
card_cmc('tangle kelp', 1).
card_layout('tangle kelp', 'normal').

% Found in: MBS
card_name('tangle mantis', 'Tangle Mantis').
card_type('tangle mantis', 'Creature — Insect').
card_types('tangle mantis', ['Creature']).
card_subtypes('tangle mantis', ['Insect']).
card_colors('tangle mantis', ['G']).
card_text('tangle mantis', 'Trample').
card_mana_cost('tangle mantis', ['2', 'G', 'G']).
card_cmc('tangle mantis', 4).
card_layout('tangle mantis', 'normal').
card_power('tangle mantis', 3).
card_toughness('tangle mantis', 4).

% Found in: 10E, DST
card_name('tangle spider', 'Tangle Spider').
card_type('tangle spider', 'Creature — Spider').
card_types('tangle spider', ['Creature']).
card_subtypes('tangle spider', ['Spider']).
card_colors('tangle spider', ['G']).
card_text('tangle spider', 'Flash (You may cast this spell any time you could cast an instant.)\nReach (This creature can block creatures with flying.)').
card_mana_cost('tangle spider', ['4', 'G', 'G']).
card_cmc('tangle spider', 6).
card_layout('tangle spider', 'normal').
card_power('tangle spider', 3).
card_toughness('tangle spider', 4).

% Found in: NMS, V13
card_name('tangle wire', 'Tangle Wire').
card_type('tangle wire', 'Artifact').
card_types('tangle wire', ['Artifact']).
card_subtypes('tangle wire', []).
card_colors('tangle wire', []).
card_text('tangle wire', 'Fading 4 (This artifact enters the battlefield with four fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)\nAt the beginning of each player\'s upkeep, that player taps an untapped artifact, creature, or land he or she controls for each fade counter on Tangle Wire.').
card_mana_cost('tangle wire', ['3']).
card_cmc('tangle wire', 3).
card_layout('tangle wire', 'normal').

% Found in: 9ED, MRD
card_name('tanglebloom', 'Tanglebloom').
card_type('tanglebloom', 'Artifact').
card_types('tanglebloom', ['Artifact']).
card_subtypes('tanglebloom', []).
card_colors('tanglebloom', []).
card_text('tanglebloom', '{1}, {T}: You gain 1 life.').
card_mana_cost('tanglebloom', ['1']).
card_cmc('tanglebloom', 1).
card_layout('tanglebloom', 'normal').

% Found in: MRD
card_name('tangleroot', 'Tangleroot').
card_type('tangleroot', 'Artifact').
card_types('tangleroot', ['Artifact']).
card_subtypes('tangleroot', []).
card_colors('tangleroot', []).
card_text('tangleroot', 'Whenever a player casts a creature spell, that player adds {G} to his or her mana pool.').
card_mana_cost('tangleroot', ['3']).
card_cmc('tangleroot', 3).
card_layout('tangleroot', 'normal').

% Found in: ZEN
card_name('tanglesap', 'Tanglesap').
card_type('tanglesap', 'Instant').
card_types('tanglesap', ['Instant']).
card_subtypes('tanglesap', []).
card_colors('tanglesap', ['G']).
card_text('tanglesap', 'Prevent all combat damage that would be dealt this turn by creatures without trample.').
card_mana_cost('tanglesap', ['1', 'G']).
card_cmc('tanglesap', 2).
card_layout('tanglesap', 'normal').

% Found in: DST
card_name('tanglewalker', 'Tanglewalker').
card_type('tanglewalker', 'Creature — Dryad').
card_types('tanglewalker', ['Creature']).
card_subtypes('tanglewalker', ['Dryad']).
card_colors('tanglewalker', ['G']).
card_text('tanglewalker', 'Each creature you control can\'t be blocked as long as defending player controls an artifact land.').
card_mana_cost('tanglewalker', ['2', 'G']).
card_cmc('tanglewalker', 3).
card_layout('tanglewalker', 'normal').
card_power('tanglewalker', 2).
card_toughness('tanglewalker', 2).

% Found in: MIR
card_name('taniwha', 'Taniwha').
card_type('taniwha', 'Legendary Creature — Serpent').
card_types('taniwha', ['Creature']).
card_subtypes('taniwha', ['Serpent']).
card_supertypes('taniwha', ['Legendary']).
card_colors('taniwha', ['U']).
card_text('taniwha', 'Trample\nPhasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)\nAt the beginning of your upkeep, all lands you control phase out. (They phase in before you untap during your next untap step.)').
card_mana_cost('taniwha', ['3', 'U', 'U']).
card_cmc('taniwha', 5).
card_layout('taniwha', 'normal').
card_power('taniwha', 7).
card_toughness('taniwha', 7).
card_reserved('taniwha').

% Found in: PTK
card_name('taoist hermit', 'Taoist Hermit').
card_type('taoist hermit', 'Creature — Human Mystic').
card_types('taoist hermit', ['Creature']).
card_subtypes('taoist hermit', ['Human', 'Mystic']).
card_colors('taoist hermit', ['G']).
card_text('taoist hermit', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('taoist hermit', ['2', 'G']).
card_cmc('taoist hermit', 3).
card_layout('taoist hermit', 'normal').
card_power('taoist hermit', 2).
card_toughness('taoist hermit', 2).

% Found in: PTK
card_name('taoist mystic', 'Taoist Mystic').
card_type('taoist mystic', 'Creature — Human Mystic').
card_types('taoist mystic', ['Creature']).
card_subtypes('taoist mystic', ['Human', 'Mystic']).
card_colors('taoist mystic', ['G']).
card_text('taoist mystic', 'Taoist Mystic can\'t be blocked by creatures with horsemanship.').
card_mana_cost('taoist mystic', ['2', 'G']).
card_cmc('taoist mystic', 3).
card_layout('taoist mystic', 'normal').
card_power('taoist mystic', 2).
card_toughness('taoist mystic', 2).

% Found in: DTK
card_name('tapestry of the ages', 'Tapestry of the Ages').
card_type('tapestry of the ages', 'Artifact').
card_types('tapestry of the ages', ['Artifact']).
card_subtypes('tapestry of the ages', []).
card_colors('tapestry of the ages', []).
card_text('tapestry of the ages', '{2}, {T}: Draw a card. Activate this ability only if you\'ve cast a noncreature spell this turn.').
card_mana_cost('tapestry of the ages', ['4']).
card_cmc('tapestry of the ages', 4).
card_layout('tapestry of the ages', 'normal').

% Found in: ALA
card_name('tar fiend', 'Tar Fiend').
card_type('tar fiend', 'Creature — Elemental').
card_types('tar fiend', ['Creature']).
card_subtypes('tar fiend', ['Elemental']).
card_colors('tar fiend', ['B']).
card_text('tar fiend', 'Devour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\nWhen Tar Fiend enters the battlefield, target player discards a card for each creature it devoured.').
card_mana_cost('tar fiend', ['5', 'B']).
card_cmc('tar fiend', 6).
card_layout('tar fiend', 'normal').
card_power('tar fiend', 4).
card_toughness('tar fiend', 4).

% Found in: BTD, VIS
card_name('tar pit warrior', 'Tar Pit Warrior').
card_type('tar pit warrior', 'Creature — Cyclops Warrior').
card_types('tar pit warrior', ['Creature']).
card_subtypes('tar pit warrior', ['Cyclops', 'Warrior']).
card_colors('tar pit warrior', ['B']).
card_text('tar pit warrior', 'When Tar Pit Warrior becomes the target of a spell or ability, sacrifice it.').
card_mana_cost('tar pit warrior', ['2', 'B']).
card_cmc('tar pit warrior', 3).
card_layout('tar pit warrior', 'normal').
card_power('tar pit warrior', 3).
card_toughness('tar pit warrior', 4).

% Found in: DD3_EVG, EVG, LRW, MMA
card_name('tar pitcher', 'Tar Pitcher').
card_type('tar pitcher', 'Creature — Goblin Shaman').
card_types('tar pitcher', ['Creature']).
card_subtypes('tar pitcher', ['Goblin', 'Shaman']).
card_colors('tar pitcher', ['R']).
card_text('tar pitcher', '{T}, Sacrifice a Goblin: Tar Pitcher deals 2 damage to target creature or player.').
card_mana_cost('tar pitcher', ['3', 'R']).
card_cmc('tar pitcher', 4).
card_layout('tar pitcher', 'normal').
card_power('tar pitcher', 2).
card_toughness('tar pitcher', 2).

% Found in: DD3_EVG, EVG, LRW
card_name('tarfire', 'Tarfire').
card_type('tarfire', 'Tribal Instant — Goblin').
card_types('tarfire', ['Tribal', 'Instant']).
card_subtypes('tarfire', ['Goblin']).
card_colors('tarfire', ['R']).
card_text('tarfire', 'Tarfire deals 2 damage to target creature or player.').
card_mana_cost('tarfire', ['R']).
card_cmc('tarfire', 1).
card_layout('tarfire', 'normal').

% Found in: CMD, V15
card_name('tariel, reckoner of souls', 'Tariel, Reckoner of Souls').
card_type('tariel, reckoner of souls', 'Legendary Creature — Angel').
card_types('tariel, reckoner of souls', ['Creature']).
card_subtypes('tariel, reckoner of souls', ['Angel']).
card_supertypes('tariel, reckoner of souls', ['Legendary']).
card_colors('tariel, reckoner of souls', ['W', 'B', 'R']).
card_text('tariel, reckoner of souls', 'Flying, vigilance\n{T}: Choose a creature card at random from target opponent\'s graveyard. Put that card onto the battlefield under your control.').
card_mana_cost('tariel, reckoner of souls', ['4', 'W', 'B', 'R']).
card_cmc('tariel, reckoner of souls', 7).
card_layout('tariel, reckoner of souls', 'normal').
card_power('tariel, reckoner of souls', 4).
card_toughness('tariel, reckoner of souls', 7).

% Found in: 6ED, WTH
card_name('tariff', 'Tariff').
card_type('tariff', 'Sorcery').
card_types('tariff', ['Sorcery']).
card_subtypes('tariff', []).
card_colors('tariff', ['W']).
card_text('tariff', 'Each player sacrifices the creature he or she controls with the highest converted mana cost unless he or she pays that creature\'s mana cost. If two or more creatures a player controls are tied for highest cost, that player chooses one.').
card_mana_cost('tariff', ['1', 'W']).
card_cmc('tariff', 2).
card_layout('tariff', 'normal').

% Found in: FUT, MM2, MMA
card_name('tarmogoyf', 'Tarmogoyf').
card_type('tarmogoyf', 'Creature — Lhurgoyf').
card_types('tarmogoyf', ['Creature']).
card_subtypes('tarmogoyf', ['Lhurgoyf']).
card_colors('tarmogoyf', ['G']).
card_text('tarmogoyf', 'Tarmogoyf\'s power is equal to the number of card types among cards in all graveyards and its toughness is equal to that number plus 1.').
card_mana_cost('tarmogoyf', ['1', 'G']).
card_cmc('tarmogoyf', 2).
card_layout('tarmogoyf', 'normal').
card_power('tarmogoyf', '*').
card_toughness('tarmogoyf', '1+*').

% Found in: ODY
card_name('tarnished citadel', 'Tarnished Citadel').
card_type('tarnished citadel', 'Land').
card_types('tarnished citadel', ['Land']).
card_subtypes('tarnished citadel', []).
card_colors('tarnished citadel', []).
card_text('tarnished citadel', '{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Tarnished Citadel deals 3 damage to you.').
card_layout('tarnished citadel', 'normal').

% Found in: FUT
card_name('tarox bladewing', 'Tarox Bladewing').
card_type('tarox bladewing', 'Legendary Creature — Dragon').
card_types('tarox bladewing', ['Creature']).
card_subtypes('tarox bladewing', ['Dragon']).
card_supertypes('tarox bladewing', ['Legendary']).
card_colors('tarox bladewing', ['R']).
card_text('tarox bladewing', 'Flying, haste\nGrandeur — Discard another card named Tarox Bladewing: Tarox Bladewing gets +X/+X until end of turn, where X is its power.').
card_mana_cost('tarox bladewing', ['2', 'R', 'R', 'R']).
card_cmc('tarox bladewing', 5).
card_layout('tarox bladewing', 'normal').
card_power('tarox bladewing', 4).
card_toughness('tarox bladewing', 3).

% Found in: 5ED, ICE
card_name('tarpan', 'Tarpan').
card_type('tarpan', 'Creature — Horse').
card_types('tarpan', ['Creature']).
card_subtypes('tarpan', ['Horse']).
card_colors('tarpan', ['G']).
card_text('tarpan', 'When Tarpan dies, you gain 1 life.').
card_mana_cost('tarpan', ['G']).
card_cmc('tarpan', 1).
card_layout('tarpan', 'normal').
card_power('tarpan', 1).
card_toughness('tarpan', 1).

% Found in: FRF
card_name('tasigur\'s cruelty', 'Tasigur\'s Cruelty').
card_type('tasigur\'s cruelty', 'Sorcery').
card_types('tasigur\'s cruelty', ['Sorcery']).
card_subtypes('tasigur\'s cruelty', []).
card_colors('tasigur\'s cruelty', ['B']).
card_text('tasigur\'s cruelty', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nEach opponent discards two cards.').
card_mana_cost('tasigur\'s cruelty', ['5', 'B']).
card_cmc('tasigur\'s cruelty', 6).
card_layout('tasigur\'s cruelty', 'normal').

% Found in: FRF
card_name('tasigur, the golden fang', 'Tasigur, the Golden Fang').
card_type('tasigur, the golden fang', 'Legendary Creature — Human Shaman').
card_types('tasigur, the golden fang', ['Creature']).
card_subtypes('tasigur, the golden fang', ['Human', 'Shaman']).
card_supertypes('tasigur, the golden fang', ['Legendary']).
card_colors('tasigur, the golden fang', ['B']).
card_text('tasigur, the golden fang', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\n{2}{G/U}{G/U}: Put the top two cards of your library into your graveyard, then return a nonland card of an opponent\'s choice from your graveyard to your hand.').
card_mana_cost('tasigur, the golden fang', ['5', 'B']).
card_cmc('tasigur, the golden fang', 6).
card_layout('tasigur, the golden fang', 'normal').
card_power('tasigur, the golden fang', 4).
card_toughness('tasigur, the golden fang', 5).

% Found in: MMQ
card_name('task force', 'Task Force').
card_type('task force', 'Creature — Human Rebel').
card_types('task force', ['Creature']).
card_subtypes('task force', ['Human', 'Rebel']).
card_colors('task force', ['W']).
card_text('task force', 'Whenever Task Force becomes the target of a spell or ability, it gets +0/+3 until end of turn.').
card_mana_cost('task force', ['2', 'W']).
card_cmc('task force', 3).
card_layout('task force', 'normal').
card_power('task force', 1).
card_toughness('task force', 3).

% Found in: PCY
card_name('task mage assembly', 'Task Mage Assembly').
card_type('task mage assembly', 'Enchantment').
card_types('task mage assembly', ['Enchantment']).
card_subtypes('task mage assembly', []).
card_colors('task mage assembly', ['R']).
card_text('task mage assembly', 'When there are no creatures on the battlefield, sacrifice Task Mage Assembly.\n{2}: Task Mage Assembly deals 1 damage to target creature. Any player may activate this ability but only any time he or she could cast a sorcery.').
card_mana_cost('task mage assembly', ['2', 'R']).
card_cmc('task mage assembly', 3).
card_layout('task mage assembly', 'normal').

% Found in: DIS
card_name('taste for mayhem', 'Taste for Mayhem').
card_type('taste for mayhem', 'Enchantment — Aura').
card_types('taste for mayhem', ['Enchantment']).
card_subtypes('taste for mayhem', ['Aura']).
card_colors('taste for mayhem', ['R']).
card_text('taste for mayhem', 'Enchant creature\nEnchanted creature gets +2/+0.\nHellbent — Enchanted creature gets an additional +2/+0 as long as you have no cards in hand.').
card_mana_cost('taste for mayhem', ['R']).
card_cmc('taste for mayhem', 1).
card_layout('taste for mayhem', 'normal').

% Found in: M12
card_name('taste of blood', 'Taste of Blood').
card_type('taste of blood', 'Sorcery').
card_types('taste of blood', ['Sorcery']).
card_subtypes('taste of blood', []).
card_colors('taste of blood', ['B']).
card_text('taste of blood', 'Taste of Blood deals 1 damage to target player and you gain 1 life.').
card_mana_cost('taste of blood', ['B']).
card_cmc('taste of blood', 1).
card_layout('taste of blood', 'normal').

% Found in: ALL
card_name('taste of paradise', 'Taste of Paradise').
card_type('taste of paradise', 'Sorcery').
card_types('taste of paradise', ['Sorcery']).
card_subtypes('taste of paradise', []).
card_colors('taste of paradise', ['G']).
card_text('taste of paradise', 'As an additional cost to cast Taste of Paradise, you may pay {1}{G} any number of times.\nYou gain 3 life plus an additional 3 life for each additional {1}{G} you paid.').
card_mana_cost('taste of paradise', ['3', 'G']).
card_cmc('taste of paradise', 4).
card_layout('taste of paradise', 'normal').

% Found in: CHK
card_name('tatsumasa, the dragon\'s fang', 'Tatsumasa, the Dragon\'s Fang').
card_type('tatsumasa, the dragon\'s fang', 'Legendary Artifact — Equipment').
card_types('tatsumasa, the dragon\'s fang', ['Artifact']).
card_subtypes('tatsumasa, the dragon\'s fang', ['Equipment']).
card_supertypes('tatsumasa, the dragon\'s fang', ['Legendary']).
card_colors('tatsumasa, the dragon\'s fang', []).
card_text('tatsumasa, the dragon\'s fang', 'Equipped creature gets +5/+5.\n{6}, Exile Tatsumasa, the Dragon\'s Fang: Put a 5/5 blue Dragon Spirit creature token with flying onto the battlefield. Return Tatsumasa to the battlefield under its owner\'s control when that token dies.\nEquip {3}').
card_mana_cost('tatsumasa, the dragon\'s fang', ['6']).
card_cmc('tatsumasa, the dragon\'s fang', 6).
card_layout('tatsumasa, the dragon\'s fang', 'normal').

% Found in: RAV
card_name('tattered drake', 'Tattered Drake').
card_type('tattered drake', 'Creature — Zombie Drake').
card_types('tattered drake', ['Creature']).
card_subtypes('tattered drake', ['Zombie', 'Drake']).
card_colors('tattered drake', ['U']).
card_text('tattered drake', 'Flying\n{B}: Regenerate Tattered Drake.').
card_mana_cost('tattered drake', ['4', 'U']).
card_cmc('tattered drake', 5).
card_layout('tattered drake', 'normal').
card_power('tattered drake', 2).
card_toughness('tattered drake', 2).

% Found in: SHM
card_name('tatterkite', 'Tatterkite').
card_type('tatterkite', 'Artifact Creature — Scarecrow').
card_types('tatterkite', ['Artifact', 'Creature']).
card_subtypes('tatterkite', ['Scarecrow']).
card_colors('tatterkite', []).
card_text('tatterkite', 'Flying\nTatterkite can\'t have counters placed on it.').
card_mana_cost('tatterkite', ['3']).
card_cmc('tatterkite', 3).
card_layout('tatterkite', 'normal').
card_power('tatterkite', 2).
card_toughness('tatterkite', 1).

% Found in: SHM
card_name('tattermunge duo', 'Tattermunge Duo').
card_type('tattermunge duo', 'Creature — Goblin Warrior Shaman').
card_types('tattermunge duo', ['Creature']).
card_subtypes('tattermunge duo', ['Goblin', 'Warrior', 'Shaman']).
card_colors('tattermunge duo', ['R', 'G']).
card_text('tattermunge duo', 'Whenever you cast a red spell, Tattermunge Duo gets +1/+1 until end of turn.\nWhenever you cast a green spell, Tattermunge Duo gains forestwalk until end of turn. (It can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('tattermunge duo', ['2', 'R/G']).
card_cmc('tattermunge duo', 3).
card_layout('tattermunge duo', 'normal').
card_power('tattermunge duo', 2).
card_toughness('tattermunge duo', 3).

% Found in: SHM
card_name('tattermunge maniac', 'Tattermunge Maniac').
card_type('tattermunge maniac', 'Creature — Goblin Warrior').
card_types('tattermunge maniac', ['Creature']).
card_subtypes('tattermunge maniac', ['Goblin', 'Warrior']).
card_colors('tattermunge maniac', ['R', 'G']).
card_text('tattermunge maniac', 'Tattermunge Maniac attacks each turn if able.').
card_mana_cost('tattermunge maniac', ['R/G']).
card_cmc('tattermunge maniac', 1).
card_layout('tattermunge maniac', 'normal').
card_power('tattermunge maniac', 2).
card_toughness('tattermunge maniac', 1).

% Found in: SHM
card_name('tattermunge witch', 'Tattermunge Witch').
card_type('tattermunge witch', 'Creature — Goblin Shaman').
card_types('tattermunge witch', ['Creature']).
card_subtypes('tattermunge witch', ['Goblin', 'Shaman']).
card_colors('tattermunge witch', ['R', 'G']).
card_text('tattermunge witch', '{R}{G}: Each blocked creature gets +1/+0 and gains trample until end of turn.').
card_mana_cost('tattermunge witch', ['1', 'R/G']).
card_cmc('tattermunge witch', 2).
card_layout('tattermunge witch', 'normal').
card_power('tattermunge witch', 2).
card_toughness('tattermunge witch', 1).

% Found in: ODY
card_name('tattoo ward', 'Tattoo Ward').
card_type('tattoo ward', 'Enchantment — Aura').
card_types('tattoo ward', ['Enchantment']).
card_subtypes('tattoo ward', ['Aura']).
card_colors('tattoo ward', ['W']).
card_text('tattoo ward', 'Enchant creature\nEnchanted creature gets +1/+1 and has protection from enchantments. This effect doesn\'t remove Tattoo Ward.\nSacrifice Tattoo Ward: Destroy target enchantment.').
card_mana_cost('tattoo ward', ['2', 'W']).
card_cmc('tattoo ward', 3).
card_layout('tattoo ward', 'normal').

% Found in: POR
card_name('taunt', 'Taunt').
card_type('taunt', 'Sorcery').
card_types('taunt', ['Sorcery']).
card_subtypes('taunt', []).
card_colors('taunt', ['U']).
card_text('taunt', 'During target player\'s next turn, creatures that player controls attack you if able.').
card_mana_cost('taunt', ['U']).
card_cmc('taunt', 1).
card_layout('taunt', 'normal').

% Found in: PTK
card_name('taunting challenge', 'Taunting Challenge').
card_type('taunting challenge', 'Sorcery').
card_types('taunting challenge', ['Sorcery']).
card_subtypes('taunting challenge', []).
card_colors('taunting challenge', ['G']).
card_text('taunting challenge', 'All creatures able to block target creature this turn do so.').
card_mana_cost('taunting challenge', ['1', 'G', 'G']).
card_cmc('taunting challenge', 3).
card_layout('taunting challenge', 'normal').

% Found in: ONS, UDS
card_name('taunting elf', 'Taunting Elf').
card_type('taunting elf', 'Creature — Elf').
card_types('taunting elf', ['Creature']).
card_subtypes('taunting elf', ['Elf']).
card_colors('taunting elf', ['G']).
card_text('taunting elf', 'All creatures able to block Taunting Elf do so.').
card_mana_cost('taunting elf', ['G']).
card_cmc('taunting elf', 1).
card_layout('taunting elf', 'normal').
card_power('taunting elf', 0).
card_toughness('taunting elf', 1).

% Found in: ARC, HOP, MOR
card_name('taurean mauler', 'Taurean Mauler').
card_type('taurean mauler', 'Creature — Shapeshifter').
card_types('taurean mauler', ['Creature']).
card_subtypes('taurean mauler', ['Shapeshifter']).
card_colors('taurean mauler', ['R']).
card_text('taurean mauler', 'Changeling (This card is every creature type.)\nWhenever an opponent casts a spell, you may put a +1/+1 counter on Taurean Mauler.').
card_mana_cost('taurean mauler', ['2', 'R']).
card_cmc('taurean mauler', 3).
card_layout('taurean mauler', 'normal').
card_power('taurean mauler', 2).
card_toughness('taurean mauler', 2).

% Found in: DDM, RTR
card_name('tavern swindler', 'Tavern Swindler').
card_type('tavern swindler', 'Creature — Human Rogue').
card_types('tavern swindler', ['Creature']).
card_subtypes('tavern swindler', ['Human', 'Rogue']).
card_colors('tavern swindler', ['B']).
card_text('tavern swindler', '{T}, Pay 3 life: Flip a coin. If you win the flip, you gain 6 life.').
card_mana_cost('tavern swindler', ['1', 'B']).
card_cmc('tavern swindler', 2).
card_layout('tavern swindler', 'normal').
card_power('tavern swindler', 2).
card_toughness('tavern swindler', 2).

% Found in: VAN
card_name('tawnos', 'Tawnos').
card_type('tawnos', 'Vanguard').
card_types('tawnos', ['Vanguard']).
card_subtypes('tawnos', []).
card_colors('tawnos', []).
card_text('tawnos', 'You may cast artifact, creature, and enchantment spells as though they had flash.').
card_layout('tawnos', 'vanguard').

% Found in: ATQ, MED
card_name('tawnos\'s coffin', 'Tawnos\'s Coffin').
card_type('tawnos\'s coffin', 'Artifact').
card_types('tawnos\'s coffin', ['Artifact']).
card_subtypes('tawnos\'s coffin', []).
card_colors('tawnos\'s coffin', []).
card_text('tawnos\'s coffin', 'You may choose not to untap Tawnos\'s Coffin during your untap step.\n{3}, {T}: Exile target creature and all Auras attached to it. Note the number and kind of counters that were on that creature. When Tawnos\'s Coffin leaves the battlefield or becomes untapped, return that exiled card to the battlefield under its owner\'s control tapped with the noted number and kind of counters on it. If you do, return the other exiled cards to the battlefield under their owner\'s control attached to that permanent.').
card_mana_cost('tawnos\'s coffin', ['4']).
card_cmc('tawnos\'s coffin', 4).
card_layout('tawnos\'s coffin', 'normal').
card_reserved('tawnos\'s coffin').

% Found in: 4ED, ATQ, ME4
card_name('tawnos\'s wand', 'Tawnos\'s Wand').
card_type('tawnos\'s wand', 'Artifact').
card_types('tawnos\'s wand', ['Artifact']).
card_subtypes('tawnos\'s wand', []).
card_colors('tawnos\'s wand', []).
card_text('tawnos\'s wand', '{2}, {T}: Target creature with power 2 or less can\'t be blocked this turn.').
card_mana_cost('tawnos\'s wand', ['4']).
card_cmc('tawnos\'s wand', 4).
card_layout('tawnos\'s wand', 'normal').

% Found in: 4ED, 5ED, ATQ, ME4
card_name('tawnos\'s weaponry', 'Tawnos\'s Weaponry').
card_type('tawnos\'s weaponry', 'Artifact').
card_types('tawnos\'s weaponry', ['Artifact']).
card_subtypes('tawnos\'s weaponry', []).
card_colors('tawnos\'s weaponry', []).
card_text('tawnos\'s weaponry', 'You may choose not to untap Tawnos\'s Weaponry during your untap step.\n{2}, {T}: Target creature gets +1/+1 for as long as Tawnos\'s Weaponry remains tapped.').
card_mana_cost('tawnos\'s weaponry', ['2']).
card_cmc('tawnos\'s weaponry', 2).
card_layout('tawnos\'s weaponry', 'normal').

% Found in: HOP
card_name('tazeem', 'Tazeem').
card_type('tazeem', 'Plane — Zendikar').
card_types('tazeem', ['Plane']).
card_subtypes('tazeem', ['Zendikar']).
card_colors('tazeem', []).
card_text('tazeem', 'Creatures can\'t block.\nWhenever you roll {C}, draw a card for each land you control.').
card_layout('tazeem', 'plane').

% Found in: UGL
card_name('team spirit', 'Team Spirit').
card_type('team spirit', 'Instant').
card_types('team spirit', ['Instant']).
card_subtypes('team spirit', []).
card_colors('team spirit', ['G']).
card_text('team spirit', 'All creatures controlled by target player and his or her teammates get +1/+1 until end of turn.').
card_mana_cost('team spirit', ['2', 'G']).
card_cmc('team spirit', 3).
card_layout('team spirit', 'normal').

% Found in: DGM
card_name('tear', 'Tear').
card_type('tear', 'Instant').
card_types('tear', ['Instant']).
card_subtypes('tear', []).
card_colors('tear', ['W']).
card_text('tear', 'Destroy target enchantment.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('tear', ['W']).
card_cmc('tear', 1).
card_layout('tear', 'split').

% Found in: BOK
card_name('teardrop kami', 'Teardrop Kami').
card_type('teardrop kami', 'Creature — Spirit').
card_types('teardrop kami', ['Creature']).
card_subtypes('teardrop kami', ['Spirit']).
card_colors('teardrop kami', ['U']).
card_text('teardrop kami', 'Sacrifice Teardrop Kami: You may tap or untap target creature.').
card_mana_cost('teardrop kami', ['U']).
card_cmc('teardrop kami', 1).
card_layout('teardrop kami', 'normal').
card_power('teardrop kami', 1).
card_toughness('teardrop kami', 1).

% Found in: DST
card_name('tears of rage', 'Tears of Rage').
card_type('tears of rage', 'Instant').
card_types('tears of rage', ['Instant']).
card_subtypes('tears of rage', []).
card_colors('tears of rage', ['R']).
card_text('tears of rage', 'Cast Tears of Rage only during the declare attackers step.\nAttacking creatures you control get +X/+0 until end of turn, where X is the number of attacking creatures. Sacrifice those creatures at the beginning of the next end step.').
card_mana_cost('tears of rage', ['2', 'R', 'R']).
card_cmc('tears of rage', 4).
card_layout('tears of rage', 'normal').

% Found in: MMQ
card_name('tectonic break', 'Tectonic Break').
card_type('tectonic break', 'Sorcery').
card_types('tectonic break', ['Sorcery']).
card_subtypes('tectonic break', []).
card_colors('tectonic break', ['R']).
card_text('tectonic break', 'Each player sacrifices X lands.').
card_mana_cost('tectonic break', ['X', 'R', 'R']).
card_cmc('tectonic break', 2).
card_layout('tectonic break', 'normal').

% Found in: C14, WWK, pFNM
card_name('tectonic edge', 'Tectonic Edge').
card_type('tectonic edge', 'Land').
card_types('tectonic edge', ['Land']).
card_subtypes('tectonic edge', []).
card_colors('tectonic edge', []).
card_text('tectonic edge', '{T}: Add {1} to your mana pool.\n{1}, {T}, Sacrifice Tectonic Edge: Destroy target nonbasic land. Activate this ability only if an opponent controls four or more lands.').
card_layout('tectonic edge', 'normal').

% Found in: TSP
card_name('tectonic fiend', 'Tectonic Fiend').
card_type('tectonic fiend', 'Creature — Elemental').
card_types('tectonic fiend', ['Creature']).
card_subtypes('tectonic fiend', ['Elemental']).
card_colors('tectonic fiend', ['R']).
card_text('tectonic fiend', 'Echo {4}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nTectonic Fiend attacks each turn if able.').
card_mana_cost('tectonic fiend', ['4', 'R', 'R']).
card_cmc('tectonic fiend', 6).
card_layout('tectonic fiend', 'normal').
card_power('tectonic fiend', 7).
card_toughness('tectonic fiend', 7).

% Found in: INV
card_name('tectonic instability', 'Tectonic Instability').
card_type('tectonic instability', 'Enchantment').
card_types('tectonic instability', ['Enchantment']).
card_subtypes('tectonic instability', []).
card_colors('tectonic instability', ['R']).
card_text('tectonic instability', 'Whenever a land enters the battlefield, tap all lands its controller controls.').
card_mana_cost('tectonic instability', ['2', 'R']).
card_cmc('tectonic instability', 3).
card_layout('tectonic instability', 'normal').

% Found in: M12
card_name('tectonic rift', 'Tectonic Rift').
card_type('tectonic rift', 'Sorcery').
card_types('tectonic rift', ['Sorcery']).
card_subtypes('tectonic rift', []).
card_colors('tectonic rift', ['R']).
card_text('tectonic rift', 'Destroy target land. Creatures without flying can\'t block this turn.').
card_mana_cost('tectonic rift', ['3', 'R']).
card_cmc('tectonic rift', 4).
card_layout('tectonic rift', 'normal').

% Found in: MIR
card_name('teeka\'s dragon', 'Teeka\'s Dragon').
card_type('teeka\'s dragon', 'Artifact Creature — Dragon').
card_types('teeka\'s dragon', ['Artifact', 'Creature']).
card_subtypes('teeka\'s dragon', ['Dragon']).
card_colors('teeka\'s dragon', []).
card_text('teeka\'s dragon', 'Flying; trample; rampage 4 (Whenever this creature becomes blocked, it gets +4/+4 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('teeka\'s dragon', ['9']).
card_cmc('teeka\'s dragon', 9).
card_layout('teeka\'s dragon', 'normal').
card_power('teeka\'s dragon', 5).
card_toughness('teeka\'s dragon', 5).
card_reserved('teeka\'s dragon').

% Found in: PD2, ZEN, pFNM
card_name('teetering peaks', 'Teetering Peaks').
card_type('teetering peaks', 'Land').
card_types('teetering peaks', ['Land']).
card_subtypes('teetering peaks', []).
card_colors('teetering peaks', []).
card_text('teetering peaks', 'Teetering Peaks enters the battlefield tapped.\nWhen Teetering Peaks enters the battlefield, target creature gets +2/+0 until end of turn.\n{T}: Add {R} to your mana pool.').
card_layout('teetering peaks', 'normal').

% Found in: INV
card_name('teferi\'s care', 'Teferi\'s Care').
card_type('teferi\'s care', 'Enchantment').
card_types('teferi\'s care', ['Enchantment']).
card_subtypes('teferi\'s care', []).
card_colors('teferi\'s care', ['W']).
card_text('teferi\'s care', '{W}, Sacrifice an enchantment: Destroy target enchantment.\n{3}{U}{U}: Counter target enchantment spell.').
card_mana_cost('teferi\'s care', ['2', 'W']).
card_cmc('teferi\'s care', 3).
card_layout('teferi\'s care', 'normal').

% Found in: MIR
card_name('teferi\'s curse', 'Teferi\'s Curse').
card_type('teferi\'s curse', 'Enchantment — Aura').
card_types('teferi\'s curse', ['Enchantment']).
card_subtypes('teferi\'s curse', ['Aura']).
card_colors('teferi\'s curse', ['U']).
card_text('teferi\'s curse', 'Enchant artifact or creature\nEnchanted permanent has phasing. (It phases in or out before its controller untaps during each of his or her untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)').
card_mana_cost('teferi\'s curse', ['1', 'U']).
card_cmc('teferi\'s curse', 2).
card_layout('teferi\'s curse', 'normal').

% Found in: MIR
card_name('teferi\'s drake', 'Teferi\'s Drake').
card_type('teferi\'s drake', 'Creature — Drake').
card_types('teferi\'s drake', ['Creature']).
card_subtypes('teferi\'s drake', ['Drake']).
card_colors('teferi\'s drake', ['U']).
card_text('teferi\'s drake', 'Flying\nPhasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)').
card_mana_cost('teferi\'s drake', ['2', 'U']).
card_cmc('teferi\'s drake', 3).
card_layout('teferi\'s drake', 'normal').
card_power('teferi\'s drake', 3).
card_toughness('teferi\'s drake', 2).

% Found in: VIS
card_name('teferi\'s honor guard', 'Teferi\'s Honor Guard').
card_type('teferi\'s honor guard', 'Creature — Human Knight').
card_types('teferi\'s honor guard', ['Creature']).
card_subtypes('teferi\'s honor guard', ['Human', 'Knight']).
card_colors('teferi\'s honor guard', ['W']).
card_text('teferi\'s honor guard', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{U}{U}: Teferi\'s Honor Guard phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before you untap during your next untap step.)').
card_mana_cost('teferi\'s honor guard', ['2', 'W']).
card_cmc('teferi\'s honor guard', 3).
card_layout('teferi\'s honor guard', 'normal').
card_power('teferi\'s honor guard', 2).
card_toughness('teferi\'s honor guard', 2).

% Found in: MIR
card_name('teferi\'s imp', 'Teferi\'s Imp').
card_type('teferi\'s imp', 'Creature — Imp').
card_types('teferi\'s imp', ['Creature']).
card_subtypes('teferi\'s imp', ['Imp']).
card_colors('teferi\'s imp', ['U']).
card_text('teferi\'s imp', 'Flying\nPhasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)\nWhenever Teferi\'s Imp phases out, discard a card.\nWhenever Teferi\'s Imp phases in, draw a card.').
card_mana_cost('teferi\'s imp', ['2', 'U']).
card_cmc('teferi\'s imp', 3).
card_layout('teferi\'s imp', 'normal').
card_power('teferi\'s imp', 1).
card_toughness('teferi\'s imp', 1).
card_reserved('teferi\'s imp').

% Found in: MIR
card_name('teferi\'s isle', 'Teferi\'s Isle').
card_type('teferi\'s isle', 'Legendary Land').
card_types('teferi\'s isle', ['Land']).
card_subtypes('teferi\'s isle', []).
card_supertypes('teferi\'s isle', ['Legendary']).
card_colors('teferi\'s isle', []).
card_text('teferi\'s isle', 'Phasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)\nTeferi\'s Isle enters the battlefield tapped.\n{T}: Add {U}{U} to your mana pool.').
card_layout('teferi\'s isle', 'normal').
card_reserved('teferi\'s isle').

% Found in: INV, TSB
card_name('teferi\'s moat', 'Teferi\'s Moat').
card_type('teferi\'s moat', 'Enchantment').
card_types('teferi\'s moat', ['Enchantment']).
card_subtypes('teferi\'s moat', []).
card_colors('teferi\'s moat', ['W', 'U']).
card_text('teferi\'s moat', 'As Teferi\'s Moat enters the battlefield, choose a color.\nCreatures of the chosen color without flying can\'t attack you.').
card_mana_cost('teferi\'s moat', ['3', 'W', 'U']).
card_cmc('teferi\'s moat', 5).
card_layout('teferi\'s moat', 'normal').

% Found in: 6ED, 7ED, 8ED, 9ED, VIS
card_name('teferi\'s puzzle box', 'Teferi\'s Puzzle Box').
card_type('teferi\'s puzzle box', 'Artifact').
card_types('teferi\'s puzzle box', ['Artifact']).
card_subtypes('teferi\'s puzzle box', []).
card_colors('teferi\'s puzzle box', []).
card_text('teferi\'s puzzle box', 'At the beginning of each player\'s draw step, that player puts the cards in his or her hand on the bottom of his or her library in any order, then draws that many cards.').
card_mana_cost('teferi\'s puzzle box', ['4']).
card_cmc('teferi\'s puzzle box', 4).
card_layout('teferi\'s puzzle box', 'normal').

% Found in: VIS
card_name('teferi\'s realm', 'Teferi\'s Realm').
card_type('teferi\'s realm', 'World Enchantment').
card_types('teferi\'s realm', ['Enchantment']).
card_subtypes('teferi\'s realm', []).
card_supertypes('teferi\'s realm', ['World']).
card_colors('teferi\'s realm', ['U']).
card_text('teferi\'s realm', 'At the beginning of each player\'s upkeep, that player chooses artifact, creature, land, or non-Aura enchantment. All nontoken permanents of that type phase out. (While they\'re phased out, they\'re treated as though they don\'t exist. Each one phases in before its controller untaps during his or her next untap step.)').
card_mana_cost('teferi\'s realm', ['1', 'U', 'U']).
card_cmc('teferi\'s realm', 3).
card_layout('teferi\'s realm', 'normal').
card_reserved('teferi\'s realm').

% Found in: INV
card_name('teferi\'s response', 'Teferi\'s Response').
card_type('teferi\'s response', 'Instant').
card_types('teferi\'s response', ['Instant']).
card_subtypes('teferi\'s response', []).
card_colors('teferi\'s response', ['U']).
card_text('teferi\'s response', 'Counter target spell or ability an opponent controls that targets a land you control. If a permanent\'s ability is countered this way, destroy that permanent.\nDraw two cards.').
card_mana_cost('teferi\'s response', ['1', 'U']).
card_cmc('teferi\'s response', 2).
card_layout('teferi\'s response', 'normal').

% Found in: WTH
card_name('teferi\'s veil', 'Teferi\'s Veil').
card_type('teferi\'s veil', 'Enchantment').
card_types('teferi\'s veil', ['Enchantment']).
card_subtypes('teferi\'s veil', []).
card_colors('teferi\'s veil', ['U']).
card_text('teferi\'s veil', 'Whenever a creature you control attacks, it phases out at end of combat. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before you untap during your next untap step.)').
card_mana_cost('teferi\'s veil', ['1', 'U']).
card_cmc('teferi\'s veil', 2).
card_layout('teferi\'s veil', 'normal').

% Found in: TSP, V11
card_name('teferi, mage of zhalfir', 'Teferi, Mage of Zhalfir').
card_type('teferi, mage of zhalfir', 'Legendary Creature — Human Wizard').
card_types('teferi, mage of zhalfir', ['Creature']).
card_subtypes('teferi, mage of zhalfir', ['Human', 'Wizard']).
card_supertypes('teferi, mage of zhalfir', ['Legendary']).
card_colors('teferi, mage of zhalfir', ['U']).
card_text('teferi, mage of zhalfir', 'Flash (You may cast this spell any time you could cast an instant.)\nCreature cards you own that aren\'t on the battlefield have flash.\nEach opponent can cast spells only any time he or she could cast a sorcery.').
card_mana_cost('teferi, mage of zhalfir', ['2', 'U', 'U', 'U']).
card_cmc('teferi, mage of zhalfir', 5).
card_layout('teferi, mage of zhalfir', 'normal').
card_power('teferi, mage of zhalfir', 3).
card_toughness('teferi, mage of zhalfir', 4).

% Found in: C14
card_name('teferi, temporal archmage', 'Teferi, Temporal Archmage').
card_type('teferi, temporal archmage', 'Planeswalker — Teferi').
card_types('teferi, temporal archmage', ['Planeswalker']).
card_subtypes('teferi, temporal archmage', ['Teferi']).
card_colors('teferi, temporal archmage', ['U']).
card_text('teferi, temporal archmage', '+1: Look at the top two cards of your library. Put one of them into your hand and the other on the bottom of your library.\n−1: Untap up to four target permanents.\n−10: You get an emblem with \"You may activate loyalty abilities of planeswalkers you control on any player\'s turn any time you could cast an instant.\"\nTeferi, Temporal Archmage can be your commander.').
card_mana_cost('teferi, temporal archmage', ['4', 'U', 'U']).
card_cmc('teferi, temporal archmage', 6).
card_layout('teferi, temporal archmage', 'normal').
card_loyalty('teferi, temporal archmage', 5).

% Found in: INV
card_name('tek', 'Tek').
card_type('tek', 'Artifact Creature — Dragon').
card_types('tek', ['Artifact', 'Creature']).
card_subtypes('tek', ['Dragon']).
card_colors('tek', []).
card_text('tek', 'Tek gets +0/+2 as long as you control a Plains, has flying as long as you control an Island, gets +2/+0 as long as you control a Swamp, has first strike as long as you control a Mountain, and has trample as long as you control a Forest.').
card_mana_cost('tek', ['5']).
card_cmc('tek', 5).
card_layout('tek', 'normal').
card_power('tek', 2).
card_toughness('tek', 2).

% Found in: MRD
card_name('tel-jilad archers', 'Tel-Jilad Archers').
card_type('tel-jilad archers', 'Creature — Elf Archer').
card_types('tel-jilad archers', ['Creature']).
card_subtypes('tel-jilad archers', ['Elf', 'Archer']).
card_colors('tel-jilad archers', ['G']).
card_text('tel-jilad archers', 'Protection from artifacts; reach (This creature can block creatures with flying.)').
card_mana_cost('tel-jilad archers', ['4', 'G']).
card_cmc('tel-jilad archers', 5).
card_layout('tel-jilad archers', 'normal').
card_power('tel-jilad archers', 2).
card_toughness('tel-jilad archers', 4).

% Found in: MRD
card_name('tel-jilad chosen', 'Tel-Jilad Chosen').
card_type('tel-jilad chosen', 'Creature — Elf Warrior').
card_types('tel-jilad chosen', ['Creature']).
card_subtypes('tel-jilad chosen', ['Elf', 'Warrior']).
card_colors('tel-jilad chosen', ['G']).
card_text('tel-jilad chosen', 'Protection from artifacts').
card_mana_cost('tel-jilad chosen', ['1', 'G']).
card_cmc('tel-jilad chosen', 2).
card_layout('tel-jilad chosen', 'normal').
card_power('tel-jilad chosen', 2).
card_toughness('tel-jilad chosen', 1).

% Found in: SOM
card_name('tel-jilad defiance', 'Tel-Jilad Defiance').
card_type('tel-jilad defiance', 'Instant').
card_types('tel-jilad defiance', ['Instant']).
card_subtypes('tel-jilad defiance', []).
card_colors('tel-jilad defiance', ['G']).
card_text('tel-jilad defiance', 'Target creature gains protection from artifacts until end of turn.\nDraw a card.').
card_mana_cost('tel-jilad defiance', ['1', 'G']).
card_cmc('tel-jilad defiance', 2).
card_layout('tel-jilad defiance', 'normal').

% Found in: MRD
card_name('tel-jilad exile', 'Tel-Jilad Exile').
card_type('tel-jilad exile', 'Creature — Troll Warrior').
card_types('tel-jilad exile', ['Creature']).
card_subtypes('tel-jilad exile', ['Troll', 'Warrior']).
card_colors('tel-jilad exile', ['G']).
card_text('tel-jilad exile', '{1}{G}: Regenerate Tel-Jilad Exile.').
card_mana_cost('tel-jilad exile', ['3', 'G']).
card_cmc('tel-jilad exile', 4).
card_layout('tel-jilad exile', 'normal').
card_power('tel-jilad exile', 2).
card_toughness('tel-jilad exile', 3).

% Found in: SOM
card_name('tel-jilad fallen', 'Tel-Jilad Fallen').
card_type('tel-jilad fallen', 'Creature — Elf Warrior').
card_types('tel-jilad fallen', ['Creature']).
card_subtypes('tel-jilad fallen', ['Elf', 'Warrior']).
card_colors('tel-jilad fallen', ['G']).
card_text('tel-jilad fallen', 'Protection from artifacts\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('tel-jilad fallen', ['2', 'G', 'G']).
card_cmc('tel-jilad fallen', 4).
card_layout('tel-jilad fallen', 'normal').
card_power('tel-jilad fallen', 3).
card_toughness('tel-jilad fallen', 1).

% Found in: 5DN
card_name('tel-jilad justice', 'Tel-Jilad Justice').
card_type('tel-jilad justice', 'Instant').
card_types('tel-jilad justice', ['Instant']).
card_subtypes('tel-jilad justice', []).
card_colors('tel-jilad justice', ['G']).
card_text('tel-jilad justice', 'Destroy target artifact. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('tel-jilad justice', ['1', 'G']).
card_cmc('tel-jilad justice', 2).
card_layout('tel-jilad justice', 'normal').

% Found in: 5DN
card_name('tel-jilad lifebreather', 'Tel-Jilad Lifebreather').
card_type('tel-jilad lifebreather', 'Creature — Troll Shaman').
card_types('tel-jilad lifebreather', ['Creature']).
card_subtypes('tel-jilad lifebreather', ['Troll', 'Shaman']).
card_colors('tel-jilad lifebreather', ['G']).
card_text('tel-jilad lifebreather', '{G}, {T}, Sacrifice a Forest: Regenerate target creature.').
card_mana_cost('tel-jilad lifebreather', ['4', 'G']).
card_cmc('tel-jilad lifebreather', 5).
card_layout('tel-jilad lifebreather', 'normal').
card_power('tel-jilad lifebreather', 3).
card_toughness('tel-jilad lifebreather', 2).

% Found in: DST
card_name('tel-jilad outrider', 'Tel-Jilad Outrider').
card_type('tel-jilad outrider', 'Creature — Elf Warrior').
card_types('tel-jilad outrider', ['Creature']).
card_subtypes('tel-jilad outrider', ['Elf', 'Warrior']).
card_colors('tel-jilad outrider', ['G']).
card_text('tel-jilad outrider', 'Protection from artifacts').
card_mana_cost('tel-jilad outrider', ['3', 'G']).
card_cmc('tel-jilad outrider', 4).
card_layout('tel-jilad outrider', 'normal').
card_power('tel-jilad outrider', 3).
card_toughness('tel-jilad outrider', 1).

% Found in: MRD
card_name('tel-jilad stylus', 'Tel-Jilad Stylus').
card_type('tel-jilad stylus', 'Artifact').
card_types('tel-jilad stylus', ['Artifact']).
card_subtypes('tel-jilad stylus', []).
card_colors('tel-jilad stylus', []).
card_text('tel-jilad stylus', '{T}: Put target permanent you own on the bottom of your library.').
card_mana_cost('tel-jilad stylus', ['1']).
card_cmc('tel-jilad stylus', 1).
card_layout('tel-jilad stylus', 'normal').

% Found in: DST
card_name('tel-jilad wolf', 'Tel-Jilad Wolf').
card_type('tel-jilad wolf', 'Creature — Wolf').
card_types('tel-jilad wolf', ['Creature']).
card_subtypes('tel-jilad wolf', ['Wolf']).
card_colors('tel-jilad wolf', ['G']).
card_text('tel-jilad wolf', 'Whenever Tel-Jilad Wolf becomes blocked by an artifact creature, Tel-Jilad Wolf gets +3/+3 until end of turn.').
card_mana_cost('tel-jilad wolf', ['2', 'G']).
card_cmc('tel-jilad wolf', 3).
card_layout('tel-jilad wolf', 'normal').
card_power('tel-jilad wolf', 2).
card_toughness('tel-jilad wolf', 2).

% Found in: LEG, MED
card_name('telekinesis', 'Telekinesis').
card_type('telekinesis', 'Instant').
card_types('telekinesis', ['Instant']).
card_subtypes('telekinesis', []).
card_colors('telekinesis', ['U']).
card_text('telekinesis', 'Tap target creature. Prevent all combat damage that would be dealt by that creature this turn. It doesn\'t untap during its controller\'s next two untap steps.').
card_mana_cost('telekinesis', ['U', 'U']).
card_cmc('telekinesis', 2).
card_layout('telekinesis', 'normal').
card_reserved('telekinesis').

% Found in: JUD
card_name('telekinetic bonds', 'Telekinetic Bonds').
card_type('telekinetic bonds', 'Enchantment').
card_types('telekinetic bonds', ['Enchantment']).
card_subtypes('telekinetic bonds', []).
card_colors('telekinetic bonds', ['U']).
card_text('telekinetic bonds', 'Whenever a player discards a card, you may pay {1}{U}. If you do, you may tap or untap target permanent.').
card_mana_cost('telekinetic bonds', ['2', 'U', 'U', 'U']).
card_cmc('telekinetic bonds', 5).
card_layout('telekinetic bonds', 'normal').

% Found in: TSP
card_name('telekinetic sliver', 'Telekinetic Sliver').
card_type('telekinetic sliver', 'Creature — Sliver').
card_types('telekinetic sliver', ['Creature']).
card_subtypes('telekinetic sliver', ['Sliver']).
card_colors('telekinetic sliver', ['U']).
card_text('telekinetic sliver', 'All Slivers have \"{T}: Tap target permanent.\"').
card_mana_cost('telekinetic sliver', ['2', 'U', 'U']).
card_cmc('telekinetic sliver', 4).
card_layout('telekinetic sliver', 'normal').
card_power('telekinetic sliver', 2).
card_toughness('telekinetic sliver', 2).

% Found in: CON
card_name('telemin performance', 'Telemin Performance').
card_type('telemin performance', 'Sorcery').
card_types('telemin performance', ['Sorcery']).
card_subtypes('telemin performance', []).
card_colors('telemin performance', ['U']).
card_text('telemin performance', 'Target opponent reveals cards from the top of his or her library until he or she reveals a creature card. That player puts all noncreature cards revealed this way into his or her graveyard, then you put the creature card onto the battlefield under your control.').
card_mana_cost('telemin performance', ['3', 'U', 'U']).
card_cmc('telemin performance', 5).
card_layout('telemin performance', 'normal').

% Found in: 7ED, UDS
card_name('telepathic spies', 'Telepathic Spies').
card_type('telepathic spies', 'Creature — Human Wizard').
card_types('telepathic spies', ['Creature']).
card_subtypes('telepathic spies', ['Human', 'Wizard']).
card_colors('telepathic spies', ['U']).
card_text('telepathic spies', 'When Telepathic Spies enters the battlefield, look at target opponent\'s hand.').
card_mana_cost('telepathic spies', ['2', 'U']).
card_cmc('telepathic spies', 3).
card_layout('telepathic spies', 'normal').
card_power('telepathic spies', 2).
card_toughness('telepathic spies', 2).

% Found in: 10E, 7ED, 8ED, 9ED, M10, USG
card_name('telepathy', 'Telepathy').
card_type('telepathy', 'Enchantment').
card_types('telepathy', ['Enchantment']).
card_subtypes('telepathy', []).
card_colors('telepathy', ['U']).
card_text('telepathy', 'Your opponents play with their hands revealed.').
card_mana_cost('telepathy', ['U']).
card_cmc('telepathy', 1).
card_layout('telepathy', 'normal').

% Found in: CHR, LEG
card_name('teleport', 'Teleport').
card_type('teleport', 'Instant').
card_types('teleport', ['Instant']).
card_subtypes('teleport', []).
card_colors('teleport', ['U']).
card_text('teleport', 'Cast Teleport only during the declare attackers step.\nTarget creature can\'t be blocked this turn.').
card_mana_cost('teleport', ['U', 'U', 'U']).
card_cmc('teleport', 3).
card_layout('teleport', 'normal').

% Found in: RTR
card_name('teleportal', 'Teleportal').
card_type('teleportal', 'Sorcery').
card_types('teleportal', ['Sorcery']).
card_subtypes('teleportal', []).
card_colors('teleportal', ['U', 'R']).
card_text('teleportal', 'Target creature you control gets +1/+0 until end of turn and can\'t be blocked this turn.\nOverload {3}{U}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('teleportal', ['U', 'R']).
card_cmc('teleportal', 2).
card_layout('teleportal', 'normal').

% Found in: TMP, TPR
card_name('telethopter', 'Telethopter').
card_type('telethopter', 'Artifact Creature — Thopter').
card_types('telethopter', ['Artifact', 'Creature']).
card_subtypes('telethopter', ['Thopter']).
card_colors('telethopter', []).
card_text('telethopter', 'Tap an untapped creature you control: Telethopter gains flying until end of turn.').
card_mana_cost('telethopter', ['4']).
card_cmc('telethopter', 4).
card_layout('telethopter', 'normal').
card_power('telethopter', 3).
card_toughness('telethopter', 1).

% Found in: MIR
card_name('telim\'tor', 'Telim\'Tor').
card_type('telim\'tor', 'Legendary Creature — Human Knight').
card_types('telim\'tor', ['Creature']).
card_subtypes('telim\'tor', ['Human', 'Knight']).
card_supertypes('telim\'tor', ['Legendary']).
card_colors('telim\'tor', ['R']).
card_text('telim\'tor', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nWhenever Telim\'Tor attacks, all attacking creatures with flanking get +1/+1 until end of turn.').
card_mana_cost('telim\'tor', ['4', 'R']).
card_cmc('telim\'tor', 5).
card_layout('telim\'tor', 'normal').
card_power('telim\'tor', 2).
card_toughness('telim\'tor', 2).
card_reserved('telim\'tor').

% Found in: MIR
card_name('telim\'tor\'s darts', 'Telim\'Tor\'s Darts').
card_type('telim\'tor\'s darts', 'Artifact').
card_types('telim\'tor\'s darts', ['Artifact']).
card_subtypes('telim\'tor\'s darts', []).
card_colors('telim\'tor\'s darts', []).
card_text('telim\'tor\'s darts', '{2}, {T}: Telim\'Tor\'s Darts deals 1 damage to target player.').
card_mana_cost('telim\'tor\'s darts', ['2']).
card_cmc('telim\'tor\'s darts', 2).
card_layout('telim\'tor\'s darts', 'normal').

% Found in: MIR
card_name('telim\'tor\'s edict', 'Telim\'Tor\'s Edict').
card_type('telim\'tor\'s edict', 'Instant').
card_types('telim\'tor\'s edict', ['Instant']).
card_subtypes('telim\'tor\'s edict', []).
card_colors('telim\'tor\'s edict', ['R']).
card_text('telim\'tor\'s edict', 'Exile target permanent you own or control.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('telim\'tor\'s edict', ['R']).
card_cmc('telim\'tor\'s edict', 1).
card_layout('telim\'tor\'s edict', 'normal').
card_reserved('telim\'tor\'s edict').

% Found in: CHK
card_name('teller of tales', 'Teller of Tales').
card_type('teller of tales', 'Creature — Spirit').
card_types('teller of tales', ['Creature']).
card_subtypes('teller of tales', ['Spirit']).
card_colors('teller of tales', ['U']).
card_text('teller of tales', 'Flying\nWhenever you cast a Spirit or Arcane spell, you may tap or untap target creature.').
card_mana_cost('teller of tales', ['3', 'U', 'U']).
card_cmc('teller of tales', 5).
card_layout('teller of tales', 'normal').
card_power('teller of tales', 3).
card_toughness('teller of tales', 3).

% Found in: 10E, MM2, RAV
card_name('telling time', 'Telling Time').
card_type('telling time', 'Instant').
card_types('telling time', ['Instant']).
card_subtypes('telling time', []).
card_colors('telling time', ['U']).
card_text('telling time', 'Look at the top three cards of your library. Put one of those cards into your hand, one on top of your library, and one on the bottom of your library.').
card_mana_cost('telling time', ['1', 'U']).
card_cmc('telling time', 2).
card_layout('telling time', 'normal').

% Found in: UGL
card_name('temp of the damned', 'Temp of the Damned').
card_type('temp of the damned', 'Creature — Zombie').
card_types('temp of the damned', ['Creature']).
card_subtypes('temp of the damned', ['Zombie']).
card_colors('temp of the damned', ['B']).
card_text('temp of the damned', 'When you play Temp of the Damned, roll a six-sided die. Temp of the Damned comes into play with a number of funk counters on it equal to the die roll.\nDuring your upkeep, remove a funk counter from Temp of the Damned or sacrifice Temp of the Damned.').
card_mana_cost('temp of the damned', ['2', 'B']).
card_cmc('temp of the damned', 3).
card_layout('temp of the damned', 'normal').
card_power('temp of the damned', 3).
card_toughness('temp of the damned', 3).

% Found in: STH
card_name('temper', 'Temper').
card_type('temper', 'Instant').
card_types('temper', ['Instant']).
card_subtypes('temper', []).
card_colors('temper', ['W']).
card_text('temper', 'Prevent the next X damage that would be dealt to target creature this turn. For each 1 damage prevented this way, put a +1/+1 counter on that creature.').
card_mana_cost('temper', ['X', '1', 'W']).
card_cmc('temper', 2).
card_layout('temper', 'normal').

% Found in: SOM, pMGD
card_name('tempered steel', 'Tempered Steel').
card_type('tempered steel', 'Enchantment').
card_types('tempered steel', ['Enchantment']).
card_subtypes('tempered steel', []).
card_colors('tempered steel', ['W']).
card_text('tempered steel', 'Artifact creatures you control get +2/+2.').
card_mana_cost('tempered steel', ['1', 'W', 'W']).
card_cmc('tempered steel', 3).
card_layout('tempered steel', 'normal').

% Found in: VIS
card_name('tempest drake', 'Tempest Drake').
card_type('tempest drake', 'Creature — Drake').
card_types('tempest drake', ['Creature']).
card_subtypes('tempest drake', ['Drake']).
card_colors('tempest drake', ['W', 'U']).
card_text('tempest drake', 'Flying, vigilance').
card_mana_cost('tempest drake', ['1', 'W', 'U']).
card_cmc('tempest drake', 3).
card_layout('tempest drake', 'normal').
card_power('tempest drake', 2).
card_toughness('tempest drake', 2).

% Found in: 4ED, LEG
card_name('tempest efreet', 'Tempest Efreet').
card_type('tempest efreet', 'Creature — Efreet').
card_types('tempest efreet', ['Creature']).
card_subtypes('tempest efreet', ['Efreet']).
card_colors('tempest efreet', ['R']).
card_text('tempest efreet', 'Remove Tempest Efreet from your deck before playing if you\'re not playing for ante.\n{T}, Sacrifice Tempest Efreet: Target opponent may pay 10 life. If that player doesn\'t, he or she reveals a card at random from his or her hand. Exchange ownership of the revealed card and Tempest Efreet. Put the revealed card into your hand and Tempest Efreet from anywhere into that player\'s graveyard. This change in ownership is permanent.').
card_mana_cost('tempest efreet', ['1', 'R', 'R', 'R']).
card_cmc('tempest efreet', 4).
card_layout('tempest efreet', 'normal').
card_power('tempest efreet', 3).
card_toughness('tempest efreet', 3).

% Found in: 10E, 9ED, M10, MRD
card_name('tempest of light', 'Tempest of Light').
card_type('tempest of light', 'Instant').
card_types('tempest of light', ['Instant']).
card_subtypes('tempest of light', []).
card_colors('tempest of light', ['W']).
card_text('tempest of light', 'Destroy all enchantments.').
card_mana_cost('tempest of light', ['2', 'W']).
card_cmc('tempest of light', 3).
card_layout('tempest of light', 'normal').

% Found in: ZEN
card_name('tempest owl', 'Tempest Owl').
card_type('tempest owl', 'Creature — Bird').
card_types('tempest owl', ['Creature']).
card_subtypes('tempest owl', ['Bird']).
card_colors('tempest owl', ['U']).
card_text('tempest owl', 'Kicker {4}{U} (You may pay an additional {4}{U} as you cast this spell.)\nFlying\nWhen Tempest Owl enters the battlefield, if it was kicked, tap up to three target permanents.').
card_mana_cost('tempest owl', ['1', 'U']).
card_cmc('tempest owl', 2).
card_layout('tempest owl', 'normal').
card_power('tempest owl', 1).
card_toughness('tempest owl', 2).

% Found in: DDF, ME4, PO2
card_name('temple acolyte', 'Temple Acolyte').
card_type('temple acolyte', 'Creature — Human Cleric').
card_types('temple acolyte', ['Creature']).
card_subtypes('temple acolyte', ['Human', 'Cleric']).
card_colors('temple acolyte', ['W']).
card_text('temple acolyte', 'When Temple Acolyte enters the battlefield, you gain 3 life.').
card_mana_cost('temple acolyte', ['1', 'W']).
card_cmc('temple acolyte', 2).
card_layout('temple acolyte', 'normal').
card_power('temple acolyte', 1).
card_toughness('temple acolyte', 3).

% Found in: C13, M11
card_name('temple bell', 'Temple Bell').
card_type('temple bell', 'Artifact').
card_types('temple bell', ['Artifact']).
card_subtypes('temple bell', []).
card_colors('temple bell', []).
card_text('temple bell', '{T}: Each player draws a card.').
card_mana_cost('temple bell', ['3']).
card_cmc('temple bell', 3).
card_layout('temple bell', 'normal').

% Found in: PO2
card_name('temple elder', 'Temple Elder').
card_type('temple elder', 'Creature — Human Cleric').
card_types('temple elder', ['Creature']).
card_subtypes('temple elder', ['Human', 'Cleric']).
card_colors('temple elder', ['W']).
card_text('temple elder', '{T}: You gain 1 life. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('temple elder', ['2', 'W']).
card_cmc('temple elder', 3).
card_layout('temple elder', 'normal').
card_power('temple elder', 1).
card_toughness('temple elder', 2).

% Found in: EXP, RAV, RTR
card_name('temple garden', 'Temple Garden').
card_type('temple garden', 'Land — Forest Plains').
card_types('temple garden', ['Land']).
card_subtypes('temple garden', ['Forest', 'Plains']).
card_colors('temple garden', []).
card_text('temple garden', '({T}: Add {G} or {W} to your mana pool.)\nAs Temple Garden enters the battlefield, you may pay 2 life. If you don\'t, Temple Garden enters the battlefield tapped.').
card_layout('temple garden', 'normal').

% Found in: THS
card_name('temple of abandon', 'Temple of Abandon').
card_type('temple of abandon', 'Land').
card_types('temple of abandon', ['Land']).
card_subtypes('temple of abandon', []).
card_colors('temple of abandon', []).
card_text('temple of abandon', 'Temple of Abandon enters the battlefield tapped.\nWhen Temple of Abandon enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {R} or {G} to your mana pool.').
card_layout('temple of abandon', 'normal').

% Found in: THS
card_name('temple of deceit', 'Temple of Deceit').
card_type('temple of deceit', 'Land').
card_types('temple of deceit', ['Land']).
card_subtypes('temple of deceit', []).
card_colors('temple of deceit', []).
card_text('temple of deceit', 'Temple of Deceit enters the battlefield tapped.\nWhen Temple of Deceit enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {U} or {B} to your mana pool.').
card_layout('temple of deceit', 'normal').

% Found in: BNG
card_name('temple of enlightenment', 'Temple of Enlightenment').
card_type('temple of enlightenment', 'Land').
card_types('temple of enlightenment', ['Land']).
card_subtypes('temple of enlightenment', []).
card_colors('temple of enlightenment', []).
card_text('temple of enlightenment', 'Temple of Enlightenment enters the battlefield tapped.\nWhen Temple of Enlightenment enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {W} or {U} to your mana pool.').
card_layout('temple of enlightenment', 'normal').

% Found in: JOU
card_name('temple of epiphany', 'Temple of Epiphany').
card_type('temple of epiphany', 'Land').
card_types('temple of epiphany', ['Land']).
card_subtypes('temple of epiphany', []).
card_colors('temple of epiphany', []).
card_text('temple of epiphany', 'Temple of Epiphany enters the battlefield tapped.\nWhen Temple of Epiphany enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {U} or {R} to your mana pool.').
card_layout('temple of epiphany', 'normal').

% Found in: JOU
card_name('temple of malady', 'Temple of Malady').
card_type('temple of malady', 'Land').
card_types('temple of malady', ['Land']).
card_subtypes('temple of malady', []).
card_colors('temple of malady', []).
card_text('temple of malady', 'Temple of Malady enters the battlefield tapped.\nWhen Temple of Malady enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {B} or {G} to your mana pool.').
card_layout('temple of malady', 'normal').

% Found in: BNG
card_name('temple of malice', 'Temple of Malice').
card_type('temple of malice', 'Land').
card_types('temple of malice', ['Land']).
card_subtypes('temple of malice', []).
card_colors('temple of malice', []).
card_text('temple of malice', 'Temple of Malice enters the battlefield tapped.\nWhen Temple of Malice enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {B} or {R} to your mana pool.').
card_layout('temple of malice', 'normal').

% Found in: CPK, THS
card_name('temple of mystery', 'Temple of Mystery').
card_type('temple of mystery', 'Land').
card_types('temple of mystery', ['Land']).
card_subtypes('temple of mystery', []).
card_colors('temple of mystery', []).
card_text('temple of mystery', 'Temple of Mystery enters the battlefield tapped.\nWhen Temple of Mystery enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {G} or {U} to your mana pool.').
card_layout('temple of mystery', 'normal').

% Found in: BNG
card_name('temple of plenty', 'Temple of Plenty').
card_type('temple of plenty', 'Land').
card_types('temple of plenty', ['Land']).
card_subtypes('temple of plenty', []).
card_colors('temple of plenty', []).
card_text('temple of plenty', 'Temple of Plenty enters the battlefield tapped.\nWhen Temple of Plenty enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {G} or {W} to your mana pool.').
card_layout('temple of plenty', 'normal').

% Found in: THS
card_name('temple of silence', 'Temple of Silence').
card_type('temple of silence', 'Land').
card_types('temple of silence', ['Land']).
card_subtypes('temple of silence', []).
card_colors('temple of silence', []).
card_text('temple of silence', 'Temple of Silence enters the battlefield tapped.\nWhen Temple of Silence enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {W} or {B} to your mana pool.').
card_layout('temple of silence', 'normal').

% Found in: C13, C14, CMD, DDO, SCG
card_name('temple of the false god', 'Temple of the False God').
card_type('temple of the false god', 'Land').
card_types('temple of the false god', ['Land']).
card_subtypes('temple of the false god', []).
card_colors('temple of the false god', []).
card_text('temple of the false god', '{T}: Add {2} to your mana pool. Activate this ability only if you control five or more lands.').
card_layout('temple of the false god', 'normal').

% Found in: THS
card_name('temple of triumph', 'Temple of Triumph').
card_type('temple of triumph', 'Land').
card_types('temple of triumph', ['Land']).
card_subtypes('temple of triumph', []).
card_colors('temple of triumph', []).
card_text('temple of triumph', 'Temple of Triumph enters the battlefield tapped.\nWhen Temple of Triumph enters the battlefield, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)\n{T}: Add {R} or {W} to your mana pool.').
card_layout('temple of triumph', 'normal').

% Found in: 7ED, 8ED, 9ED, UDS
card_name('temporal adept', 'Temporal Adept').
card_type('temporal adept', 'Creature — Human Wizard').
card_types('temporal adept', ['Creature']).
card_subtypes('temporal adept', ['Human', 'Wizard']).
card_colors('temporal adept', ['U']).
card_text('temporal adept', '{U}{U}{U}, {T}: Return target permanent to its owner\'s hand.').
card_mana_cost('temporal adept', ['1', 'U', 'U']).
card_cmc('temporal adept', 3).
card_layout('temporal adept', 'normal').
card_power('temporal adept', 1).
card_toughness('temporal adept', 1).

% Found in: USG
card_name('temporal aperture', 'Temporal Aperture').
card_type('temporal aperture', 'Artifact').
card_types('temporal aperture', ['Artifact']).
card_subtypes('temporal aperture', []).
card_colors('temporal aperture', []).
card_text('temporal aperture', '{5}, {T}: Shuffle your library, then reveal the top card. Until end of turn, for as long as that card remains on top of your library, play with the top card of your library revealed and you may play that card without paying its mana cost. (If it has X in its mana cost, X is 0.)').
card_mana_cost('temporal aperture', ['2']).
card_cmc('temporal aperture', 2).
card_layout('temporal aperture', 'normal').
card_reserved('temporal aperture').

% Found in: MRD
card_name('temporal cascade', 'Temporal Cascade').
card_type('temporal cascade', 'Sorcery').
card_types('temporal cascade', ['Sorcery']).
card_subtypes('temporal cascade', []).
card_colors('temporal cascade', ['U']).
card_text('temporal cascade', 'Choose one —\n• Each player shuffles his or her hand and graveyard into his or her library.\n• Each player draws seven cards.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_mana_cost('temporal cascade', ['5', 'U', 'U']).
card_cmc('temporal cascade', 7).
card_layout('temporal cascade', 'normal').

% Found in: INV
card_name('temporal distortion', 'Temporal Distortion').
card_type('temporal distortion', 'Enchantment').
card_types('temporal distortion', ['Enchantment']).
card_subtypes('temporal distortion', []).
card_colors('temporal distortion', ['U']).
card_text('temporal distortion', 'Whenever a creature or land becomes tapped, put an hourglass counter on it.\nEach permanent with an hourglass counter on it doesn\'t untap during its controller\'s untap step.\nAt the beginning of each player\'s upkeep, remove all hourglass counters from permanents that player controls.').
card_mana_cost('temporal distortion', ['3', 'U', 'U']).
card_cmc('temporal distortion', 5).
card_layout('temporal distortion', 'normal').

% Found in: TSP
card_name('temporal eddy', 'Temporal Eddy').
card_type('temporal eddy', 'Sorcery').
card_types('temporal eddy', ['Sorcery']).
card_subtypes('temporal eddy', []).
card_colors('temporal eddy', ['U']).
card_text('temporal eddy', 'Put target creature or land on top of its owner\'s library.').
card_mana_cost('temporal eddy', ['2', 'U', 'U']).
card_cmc('temporal eddy', 4).
card_layout('temporal eddy', 'normal').

% Found in: PLC
card_name('temporal extortion', 'Temporal Extortion').
card_type('temporal extortion', 'Sorcery').
card_types('temporal extortion', ['Sorcery']).
card_subtypes('temporal extortion', []).
card_colors('temporal extortion', ['B']).
card_text('temporal extortion', 'When you cast Temporal Extortion, any player may pay half his or her life, rounded up. If a player does, counter Temporal Extortion.\nTake an extra turn after this one.').
card_mana_cost('temporal extortion', ['B', 'B', 'B', 'B']).
card_cmc('temporal extortion', 4).
card_layout('temporal extortion', 'normal').

% Found in: SCG, VMA
card_name('temporal fissure', 'Temporal Fissure').
card_type('temporal fissure', 'Sorcery').
card_types('temporal fissure', ['Sorcery']).
card_subtypes('temporal fissure', []).
card_colors('temporal fissure', ['U']).
card_text('temporal fissure', 'Return target permanent to its owner\'s hand.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('temporal fissure', ['4', 'U']).
card_cmc('temporal fissure', 5).
card_layout('temporal fissure', 'normal').

% Found in: TSP
card_name('temporal isolation', 'Temporal Isolation').
card_type('temporal isolation', 'Enchantment — Aura').
card_types('temporal isolation', ['Enchantment']).
card_subtypes('temporal isolation', ['Aura']).
card_colors('temporal isolation', ['W']).
card_text('temporal isolation', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature has shadow. (It can block or be blocked by only creatures with shadow.)\nPrevent all damage that would be dealt by enchanted creature.').
card_mana_cost('temporal isolation', ['1', 'W']).
card_cmc('temporal isolation', 2).
card_layout('temporal isolation', 'normal').

% Found in: ME2, PO2
card_name('temporal manipulation', 'Temporal Manipulation').
card_type('temporal manipulation', 'Sorcery').
card_types('temporal manipulation', ['Sorcery']).
card_subtypes('temporal manipulation', []).
card_colors('temporal manipulation', ['U']).
card_text('temporal manipulation', 'Take an extra turn after this one.').
card_mana_cost('temporal manipulation', ['3', 'U', 'U']).
card_cmc('temporal manipulation', 5).
card_layout('temporal manipulation', 'normal').

% Found in: AVR
card_name('temporal mastery', 'Temporal Mastery').
card_type('temporal mastery', 'Sorcery').
card_types('temporal mastery', ['Sorcery']).
card_subtypes('temporal mastery', []).
card_colors('temporal mastery', ['U']).
card_text('temporal mastery', 'Take an extra turn after this one. Exile Temporal Mastery.\nMiracle {1}{U} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('temporal mastery', ['5', 'U', 'U']).
card_cmc('temporal mastery', 7).
card_layout('temporal mastery', 'normal').

% Found in: APC
card_name('temporal spring', 'Temporal Spring').
card_type('temporal spring', 'Sorcery').
card_types('temporal spring', ['Sorcery']).
card_subtypes('temporal spring', []).
card_colors('temporal spring', ['U', 'G']).
card_text('temporal spring', 'Put target permanent on top of its owner\'s library.').
card_mana_cost('temporal spring', ['1', 'G', 'U']).
card_cmc('temporal spring', 3).
card_layout('temporal spring', 'normal').

% Found in: FRF
card_name('temporal trespass', 'Temporal Trespass').
card_type('temporal trespass', 'Sorcery').
card_types('temporal trespass', ['Sorcery']).
card_subtypes('temporal trespass', []).
card_colors('temporal trespass', ['U']).
card_text('temporal trespass', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nTake an extra turn after this one. Exile Temporal Trespass.').
card_mana_cost('temporal trespass', ['8', 'U', 'U', 'U']).
card_cmc('temporal trespass', 11).
card_layout('temporal trespass', 'normal').

% Found in: DDG, TOR
card_name('temporary insanity', 'Temporary Insanity').
card_type('temporary insanity', 'Instant').
card_types('temporary insanity', ['Instant']).
card_subtypes('temporary insanity', []).
card_colors('temporary insanity', ['R']).
card_text('temporary insanity', 'Untap target creature with power less than the number of cards in your graveyard and gain control of it until end of turn. That creature gains haste until end of turn.').
card_mana_cost('temporary insanity', ['3', 'R']).
card_cmc('temporary insanity', 4).
card_layout('temporary insanity', 'normal').

% Found in: POR
card_name('temporary truce', 'Temporary Truce').
card_type('temporary truce', 'Sorcery').
card_types('temporary truce', ['Sorcery']).
card_subtypes('temporary truce', []).
card_colors('temporary truce', ['W']).
card_text('temporary truce', 'Each player may draw up to two cards. For each card less than two a player draws this way, that player gains 2 life.').
card_mana_cost('temporary truce', ['1', 'W']).
card_cmc('temporary truce', 2).
card_layout('temporary truce', 'normal').

% Found in: C13
card_name('tempt with discovery', 'Tempt with Discovery').
card_type('tempt with discovery', 'Sorcery').
card_types('tempt with discovery', ['Sorcery']).
card_subtypes('tempt with discovery', []).
card_colors('tempt with discovery', ['G']).
card_text('tempt with discovery', 'Tempting offer — Search your library for a land card and put it onto the battlefield. Each opponent may search his or her library for a land card and put it onto the battlefield. For each opponent who searches a library this way, search your library for a land card and put it onto the battlefield. Then each player who searched a library this way shuffles it.').
card_mana_cost('tempt with discovery', ['3', 'G']).
card_cmc('tempt with discovery', 4).
card_layout('tempt with discovery', 'normal').

% Found in: C13
card_name('tempt with glory', 'Tempt with Glory').
card_type('tempt with glory', 'Sorcery').
card_types('tempt with glory', ['Sorcery']).
card_subtypes('tempt with glory', []).
card_colors('tempt with glory', ['W']).
card_text('tempt with glory', 'Tempting offer — Put a +1/+1 counter on each creature you control. Each opponent may put a +1/+1 counter on each creature he or she controls. For each opponent who does, put a +1/+1 counter on each creature you control.').
card_mana_cost('tempt with glory', ['5', 'W']).
card_cmc('tempt with glory', 6).
card_layout('tempt with glory', 'normal').

% Found in: C13
card_name('tempt with immortality', 'Tempt with Immortality').
card_type('tempt with immortality', 'Sorcery').
card_types('tempt with immortality', ['Sorcery']).
card_subtypes('tempt with immortality', []).
card_colors('tempt with immortality', ['B']).
card_text('tempt with immortality', 'Tempting offer — Return a creature card from your graveyard to the battlefield. Each opponent may return a creature card from his or her graveyard to the battlefield. For each player who does, return a creature card from your graveyard to the battlefield.').
card_mana_cost('tempt with immortality', ['4', 'B']).
card_cmc('tempt with immortality', 5).
card_layout('tempt with immortality', 'normal').

% Found in: C13
card_name('tempt with reflections', 'Tempt with Reflections').
card_type('tempt with reflections', 'Sorcery').
card_types('tempt with reflections', ['Sorcery']).
card_subtypes('tempt with reflections', []).
card_colors('tempt with reflections', ['U']).
card_text('tempt with reflections', 'Tempting offer — Choose target creature you control. Put a token onto the battlefield that\'s a copy of that creature. Each opponent may put a token onto the battlefield that\'s a copy of that creature. For each opponent who does, put a token onto the battlefield that\'s a copy of that creature.').
card_mana_cost('tempt with reflections', ['3', 'U']).
card_cmc('tempt with reflections', 4).
card_layout('tempt with reflections', 'normal').

% Found in: C13
card_name('tempt with vengeance', 'Tempt with Vengeance').
card_type('tempt with vengeance', 'Sorcery').
card_types('tempt with vengeance', ['Sorcery']).
card_subtypes('tempt with vengeance', []).
card_colors('tempt with vengeance', ['R']).
card_text('tempt with vengeance', 'Tempting offer — Put X 1/1 red Elemental creature tokens with haste onto the battlefield. Each opponent may put X 1/1 red Elemental creature tokens with haste onto the battlefield. For each player who does, put X 1/1 red Elemental creature tokens with haste onto the battlefield.').
card_mana_cost('tempt with vengeance', ['X', 'R']).
card_cmc('tempt with vengeance', 1).
card_layout('tempt with vengeance', 'normal').

% Found in: STH
card_name('tempting licid', 'Tempting Licid').
card_type('tempting licid', 'Creature — Licid').
card_types('tempting licid', ['Creature']).
card_subtypes('tempting licid', ['Licid']).
card_colors('tempting licid', ['G']).
card_text('tempting licid', '{G}, {T}: Tempting Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {G} to end this effect.\nAll creatures able to block enchanted creature do so.').
card_mana_cost('tempting licid', ['2', 'G']).
card_cmc('tempting licid', 3).
card_layout('tempting licid', 'normal').
card_power('tempting licid', 2).
card_toughness('tempting licid', 2).

% Found in: ONS
card_name('tempting wurm', 'Tempting Wurm').
card_type('tempting wurm', 'Creature — Wurm').
card_types('tempting wurm', ['Creature']).
card_subtypes('tempting wurm', ['Wurm']).
card_colors('tempting wurm', ['G']).
card_text('tempting wurm', 'When Tempting Wurm enters the battlefield, each opponent may put any number of artifact, creature, enchantment, and/or land cards from his or her hand onto the battlefield.').
card_mana_cost('tempting wurm', ['1', 'G']).
card_cmc('tempting wurm', 2).
card_layout('tempting wurm', 'normal').
card_power('tempting wurm', 5).
card_toughness('tempting wurm', 5).

% Found in: KTK, pPRE
card_name('temur ascendancy', 'Temur Ascendancy').
card_type('temur ascendancy', 'Enchantment').
card_types('temur ascendancy', ['Enchantment']).
card_subtypes('temur ascendancy', []).
card_colors('temur ascendancy', ['U', 'R', 'G']).
card_text('temur ascendancy', 'Creatures you control have haste.\nWhenever a creature with power 4 or greater enters the battlefield under your control, you may draw a card.').
card_mana_cost('temur ascendancy', ['G', 'U', 'R']).
card_cmc('temur ascendancy', 3).
card_layout('temur ascendancy', 'normal').

% Found in: KTK
card_name('temur banner', 'Temur Banner').
card_type('temur banner', 'Artifact').
card_types('temur banner', ['Artifact']).
card_subtypes('temur banner', []).
card_colors('temur banner', []).
card_text('temur banner', '{T}: Add {G}, {U}, or {R} to your mana pool.\n{G}{U}{R}, {T}, Sacrifice Temur Banner: Draw a card.').
card_mana_cost('temur banner', ['3']).
card_cmc('temur banner', 3).
card_layout('temur banner', 'normal').

% Found in: FRF
card_name('temur battle rage', 'Temur Battle Rage').
card_type('temur battle rage', 'Instant').
card_types('temur battle rage', ['Instant']).
card_subtypes('temur battle rage', []).
card_colors('temur battle rage', ['R']).
card_text('temur battle rage', 'Target creature gains double strike until end of turn. (It deals both first-strike and regular combat damage.)\nFerocious — That creature also gains trample until end of turn if you control a creature with power 4 or greater.').
card_mana_cost('temur battle rage', ['1', 'R']).
card_cmc('temur battle rage', 2).
card_layout('temur battle rage', 'normal').

% Found in: KTK
card_name('temur charger', 'Temur Charger').
card_type('temur charger', 'Creature — Horse').
card_types('temur charger', ['Creature']).
card_subtypes('temur charger', ['Horse']).
card_colors('temur charger', ['G']).
card_text('temur charger', 'Morph—Reveal a green card in your hand. (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Temur Charger is turned face up, target creature gains trample until end of turn.').
card_mana_cost('temur charger', ['1', 'G']).
card_cmc('temur charger', 2).
card_layout('temur charger', 'normal').
card_power('temur charger', 3).
card_toughness('temur charger', 1).

% Found in: KTK
card_name('temur charm', 'Temur Charm').
card_type('temur charm', 'Instant').
card_types('temur charm', ['Instant']).
card_subtypes('temur charm', []).
card_colors('temur charm', ['U', 'R', 'G']).
card_text('temur charm', 'Choose one —\n• Target creature you control gets +1/+1 until end of turn. It fights target creature you don\'t control.\n• Counter target spell unless its controller pays {3}.\n• Creatures with power 3 or less can\'t block this turn.').
card_mana_cost('temur charm', ['G', 'U', 'R']).
card_cmc('temur charm', 3).
card_layout('temur charm', 'normal').

% Found in: FRF
card_name('temur runemark', 'Temur Runemark').
card_type('temur runemark', 'Enchantment — Aura').
card_types('temur runemark', ['Enchantment']).
card_subtypes('temur runemark', ['Aura']).
card_colors('temur runemark', ['G']).
card_text('temur runemark', 'Enchant creature\nEnchanted creature gets +2/+2.\nEnchanted creature has trample as long as you control a blue or red permanent.').
card_mana_cost('temur runemark', ['2', 'G']).
card_cmc('temur runemark', 3).
card_layout('temur runemark', 'normal').

% Found in: FRF
card_name('temur sabertooth', 'Temur Sabertooth').
card_type('temur sabertooth', 'Creature — Cat').
card_types('temur sabertooth', ['Creature']).
card_subtypes('temur sabertooth', ['Cat']).
card_colors('temur sabertooth', ['G']).
card_text('temur sabertooth', '{1}{G}: You may return another creature you control to its owner\'s hand. If you do, Temur Sabertooth gains indestructible until end of turn.').
card_mana_cost('temur sabertooth', ['2', 'G', 'G']).
card_cmc('temur sabertooth', 4).
card_layout('temur sabertooth', 'normal').
card_power('temur sabertooth', 4).
card_toughness('temur sabertooth', 3).

% Found in: FRF, pMEI
card_name('temur war shaman', 'Temur War Shaman').
card_type('temur war shaman', 'Creature — Human Shaman').
card_types('temur war shaman', ['Creature']).
card_subtypes('temur war shaman', ['Human', 'Shaman']).
card_colors('temur war shaman', ['G']).
card_text('temur war shaman', 'When Temur War Shaman enters the battlefield, manifest the top card of your library. (Put that card onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nWhenever a permanent you control is turned face up, if it\'s a creature, you may have it fight target creature you don\'t control.').
card_mana_cost('temur war shaman', ['4', 'G', 'G']).
card_cmc('temur war shaman', 6).
card_layout('temur war shaman', 'normal').
card_power('temur war shaman', 4).
card_toughness('temur war shaman', 5).

% Found in: M14
card_name('tenacious dead', 'Tenacious Dead').
card_type('tenacious dead', 'Creature — Skeleton Warrior').
card_types('tenacious dead', ['Creature']).
card_subtypes('tenacious dead', ['Skeleton', 'Warrior']).
card_colors('tenacious dead', ['B']).
card_text('tenacious dead', 'When Tenacious Dead dies, you may pay {1}{B}. If you do, return it to the battlefield tapped under its owner\'s control.').
card_mana_cost('tenacious dead', ['B']).
card_cmc('tenacious dead', 1).
card_layout('tenacious dead', 'normal').
card_power('tenacious dead', 1).
card_toughness('tenacious dead', 1).

% Found in: BOK
card_name('tendo ice bridge', 'Tendo Ice Bridge').
card_type('tendo ice bridge', 'Land').
card_types('tendo ice bridge', ['Land']).
card_subtypes('tendo ice bridge', []).
card_colors('tendo ice bridge', []).
card_text('tendo ice bridge', 'Tendo Ice Bridge enters the battlefield with a charge counter on it.\n{T}: Add {1} to your mana pool.\n{T}, Remove a charge counter from Tendo Ice Bridge: Add one mana of any color to your mana pool.').
card_layout('tendo ice bridge', 'normal').

% Found in: SCG, VMA, pFNM
card_name('tendrils of agony', 'Tendrils of Agony').
card_type('tendrils of agony', 'Sorcery').
card_types('tendrils of agony', ['Sorcery']).
card_subtypes('tendrils of agony', []).
card_colors('tendrils of agony', ['B']).
card_text('tendrils of agony', 'Target player loses 2 life and you gain 2 life.\nStorm (When you cast this spell, copy it for each spell cast before it this turn. You may choose new targets for the copies.)').
card_mana_cost('tendrils of agony', ['2', 'B', 'B']).
card_cmc('tendrils of agony', 4).
card_layout('tendrils of agony', 'normal').

% Found in: C14, DD3_GVL, DDD, DDE, M10, TSP
card_name('tendrils of corruption', 'Tendrils of Corruption').
card_type('tendrils of corruption', 'Instant').
card_types('tendrils of corruption', ['Instant']).
card_subtypes('tendrils of corruption', []).
card_colors('tendrils of corruption', ['B']).
card_text('tendrils of corruption', 'Tendrils of Corruption deals X damage to target creature and you gain X life, where X is the number of Swamps you control.').
card_mana_cost('tendrils of corruption', ['3', 'B']).
card_cmc('tendrils of corruption', 4).
card_layout('tendrils of corruption', 'normal').

% Found in: WTH
card_name('tendrils of despair', 'Tendrils of Despair').
card_type('tendrils of despair', 'Sorcery').
card_types('tendrils of despair', ['Sorcery']).
card_subtypes('tendrils of despair', []).
card_colors('tendrils of despair', ['B']).
card_text('tendrils of despair', 'As an additional cost to cast Tendrils of Despair, sacrifice a creature.\nTarget opponent discards two cards.').
card_mana_cost('tendrils of despair', ['B']).
card_cmc('tendrils of despair', 1).
card_layout('tendrils of despair', 'normal').

% Found in: CMD, PLC
card_name('teneb, the harvester', 'Teneb, the Harvester').
card_type('teneb, the harvester', 'Legendary Creature — Dragon').
card_types('teneb, the harvester', ['Creature']).
card_subtypes('teneb, the harvester', ['Dragon']).
card_supertypes('teneb, the harvester', ['Legendary']).
card_colors('teneb, the harvester', ['W', 'B', 'G']).
card_text('teneb, the harvester', 'Flying\nWhenever Teneb, the Harvester deals combat damage to a player, you may pay {2}{B}. If you do, put target creature card from a graveyard onto the battlefield under your control.').
card_mana_cost('teneb, the harvester', ['3', 'B', 'G', 'W']).
card_cmc('teneb, the harvester', 6).
card_layout('teneb, the harvester', 'normal').
card_power('teneb, the harvester', 6).
card_toughness('teneb, the harvester', 6).

% Found in: RTR
card_name('tenement crasher', 'Tenement Crasher').
card_type('tenement crasher', 'Creature — Beast').
card_types('tenement crasher', ['Creature']).
card_subtypes('tenement crasher', ['Beast']).
card_colors('tenement crasher', ['R']).
card_text('tenement crasher', 'Haste').
card_mana_cost('tenement crasher', ['5', 'R']).
card_cmc('tenement crasher', 6).
card_layout('tenement crasher', 'normal').
card_power('tenement crasher', 5).
card_toughness('tenement crasher', 4).

% Found in: CHK
card_name('tenza, godo\'s maul', 'Tenza, Godo\'s Maul').
card_type('tenza, godo\'s maul', 'Legendary Artifact — Equipment').
card_types('tenza, godo\'s maul', ['Artifact']).
card_subtypes('tenza, godo\'s maul', ['Equipment']).
card_supertypes('tenza, godo\'s maul', ['Legendary']).
card_colors('tenza, godo\'s maul', []).
card_text('tenza, godo\'s maul', 'Equipped creature gets +1/+1. As long as it\'s legendary, it gets an additional +2/+2. As long as it\'s red, it has trample.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('tenza, godo\'s maul', ['3']).
card_cmc('tenza, godo\'s maul', 3).
card_layout('tenza, godo\'s maul', 'normal').

% Found in: ONS
card_name('tephraderm', 'Tephraderm').
card_type('tephraderm', 'Creature — Beast').
card_types('tephraderm', ['Creature']).
card_subtypes('tephraderm', ['Beast']).
card_colors('tephraderm', ['R']).
card_text('tephraderm', 'Whenever a creature deals damage to Tephraderm, Tephraderm deals that much damage to that creature.\nWhenever a spell deals damage to Tephraderm, Tephraderm deals that much damage to that spell\'s controller.').
card_mana_cost('tephraderm', ['4', 'R']).
card_cmc('tephraderm', 5).
card_layout('tephraderm', 'normal').
card_power('tephraderm', 4).
card_toughness('tephraderm', 5).

% Found in: CHK
card_name('terashi\'s cry', 'Terashi\'s Cry').
card_type('terashi\'s cry', 'Sorcery — Arcane').
card_types('terashi\'s cry', ['Sorcery']).
card_subtypes('terashi\'s cry', ['Arcane']).
card_colors('terashi\'s cry', ['W']).
card_text('terashi\'s cry', 'Tap up to three target creatures.').
card_mana_cost('terashi\'s cry', ['3', 'W']).
card_cmc('terashi\'s cry', 4).
card_layout('terashi\'s cry', 'normal').

% Found in: BOK, MM2, MMA
card_name('terashi\'s grasp', 'Terashi\'s Grasp').
card_type('terashi\'s grasp', 'Sorcery — Arcane').
card_types('terashi\'s grasp', ['Sorcery']).
card_subtypes('terashi\'s grasp', ['Arcane']).
card_colors('terashi\'s grasp', ['W']).
card_text('terashi\'s grasp', 'Destroy target artifact or enchantment. You gain life equal to its converted mana cost.').
card_mana_cost('terashi\'s grasp', ['2', 'W']).
card_cmc('terashi\'s grasp', 3).
card_layout('terashi\'s grasp', 'normal').

% Found in: BOK
card_name('terashi\'s verdict', 'Terashi\'s Verdict').
card_type('terashi\'s verdict', 'Instant — Arcane').
card_types('terashi\'s verdict', ['Instant']).
card_subtypes('terashi\'s verdict', ['Arcane']).
card_colors('terashi\'s verdict', ['W']).
card_text('terashi\'s verdict', 'Destroy target attacking creature with power 3 or less.').
card_mana_cost('terashi\'s verdict', ['1', 'W']).
card_cmc('terashi\'s verdict', 2).
card_layout('terashi\'s verdict', 'normal').

% Found in: C14, CNS, PD3, WWK, pMEI
card_name('terastodon', 'Terastodon').
card_type('terastodon', 'Creature — Elephant').
card_types('terastodon', ['Creature']).
card_subtypes('terastodon', ['Elephant']).
card_colors('terastodon', ['G']).
card_text('terastodon', 'When Terastodon enters the battlefield, you may destroy up to three target noncreature permanents. For each permanent put into a graveyard this way, its controller puts a 3/3 green Elephant creature token onto the battlefield.').
card_mana_cost('terastodon', ['6', 'G', 'G']).
card_cmc('terastodon', 8).
card_layout('terastodon', 'normal').
card_power('terastodon', 9).
card_toughness('terastodon', 9).

% Found in: MIR
card_name('teremko griffin', 'Teremko Griffin').
card_type('teremko griffin', 'Creature — Griffin').
card_types('teremko griffin', ['Creature']).
card_subtypes('teremko griffin', ['Griffin']).
card_colors('teremko griffin', ['W']).
card_text('teremko griffin', 'Flying; banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('teremko griffin', ['3', 'W']).
card_cmc('teremko griffin', 4).
card_layout('teremko griffin', 'normal').
card_power('teremko griffin', 2).
card_toughness('teremko griffin', 2).

% Found in: PLS
card_name('terminal moraine', 'Terminal Moraine').
card_type('terminal moraine', 'Land').
card_types('terminal moraine', ['Land']).
card_subtypes('terminal moraine', []).
card_colors('terminal moraine', []).
card_text('terminal moraine', '{T}: Add {1} to your mana pool.\n{2}, {T}, Sacrifice Terminal Moraine: Search your library for a basic land card and put that card onto the battlefield tapped. Then shuffle your library.').
card_layout('terminal moraine', 'normal').

% Found in: ARB, ARC, CMD, DDK, PLS, pFNM, pMPR
card_name('terminate', 'Terminate').
card_type('terminate', 'Instant').
card_types('terminate', ['Instant']).
card_subtypes('terminate', []).
card_colors('terminate', ['B', 'R']).
card_text('terminate', 'Destroy target creature. It can\'t be regenerated.').
card_mana_cost('terminate', ['B', 'R']).
card_cmc('terminate', 2).
card_layout('terminate', 'normal').

% Found in: AVR, V14
card_name('terminus', 'Terminus').
card_type('terminus', 'Sorcery').
card_types('terminus', ['Sorcery']).
card_subtypes('terminus', []).
card_colors('terminus', ['W']).
card_text('terminus', 'Put all creatures on the bottom of their owners\' libraries.\nMiracle {W} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('terminus', ['4', 'W', 'W']).
card_cmc('terminus', 6).
card_layout('terminus', 'normal').

% Found in: TOR, VMA
card_name('teroh\'s faithful', 'Teroh\'s Faithful').
card_type('teroh\'s faithful', 'Creature — Human Cleric').
card_types('teroh\'s faithful', ['Creature']).
card_subtypes('teroh\'s faithful', ['Human', 'Cleric']).
card_colors('teroh\'s faithful', ['W']).
card_text('teroh\'s faithful', 'When Teroh\'s Faithful enters the battlefield, you gain 4 life.').
card_mana_cost('teroh\'s faithful', ['3', 'W']).
card_cmc('teroh\'s faithful', 4).
card_layout('teroh\'s faithful', 'normal').
card_power('teroh\'s faithful', 1).
card_toughness('teroh\'s faithful', 4).

% Found in: TOR
card_name('teroh\'s vanguard', 'Teroh\'s Vanguard').
card_type('teroh\'s vanguard', 'Creature — Human Nomad').
card_types('teroh\'s vanguard', ['Creature']).
card_subtypes('teroh\'s vanguard', ['Human', 'Nomad']).
card_colors('teroh\'s vanguard', ['W']).
card_text('teroh\'s vanguard', 'Flash\nThreshold — As long as seven or more cards are in your graveyard, Teroh\'s Vanguard has \"When Teroh\'s Vanguard enters the battlefield, creatures you control gain protection from black until end of turn.\"').
card_mana_cost('teroh\'s vanguard', ['3', 'W']).
card_cmc('teroh\'s vanguard', 4).
card_layout('teroh\'s vanguard', 'normal').
card_power('teroh\'s vanguard', 2).
card_toughness('teroh\'s vanguard', 3).

% Found in: WWK
card_name('terra eternal', 'Terra Eternal').
card_type('terra eternal', 'Enchantment').
card_types('terra eternal', ['Enchantment']).
card_subtypes('terra eternal', []).
card_colors('terra eternal', ['W']).
card_text('terra eternal', 'All lands have indestructible.').
card_mana_cost('terra eternal', ['2', 'W']).
card_cmc('terra eternal', 3).
card_layout('terra eternal', 'normal').

% Found in: C13
card_name('terra ravager', 'Terra Ravager').
card_type('terra ravager', 'Creature — Elemental Beast').
card_types('terra ravager', ['Creature']).
card_subtypes('terra ravager', ['Elemental', 'Beast']).
card_colors('terra ravager', ['R']).
card_text('terra ravager', 'Whenever Terra Ravager attacks, it gets +X/+0 until end of turn, where X is the number of lands defending player controls.').
card_mana_cost('terra ravager', ['2', 'R', 'R']).
card_cmc('terra ravager', 4).
card_layout('terra ravager', 'normal').
card_power('terra ravager', 0).
card_toughness('terra ravager', 4).

% Found in: M15, ORI, ZEN
card_name('terra stomper', 'Terra Stomper').
card_type('terra stomper', 'Creature — Beast').
card_types('terra stomper', ['Creature']).
card_subtypes('terra stomper', ['Beast']).
card_colors('terra stomper', ['G']).
card_text('terra stomper', 'Terra Stomper can\'t be countered.\nTrample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('terra stomper', ['3', 'G', 'G', 'G']).
card_cmc('terra stomper', 6).
card_layout('terra stomper', 'normal').
card_power('terra stomper', 8).
card_toughness('terra stomper', 8).

% Found in: RAV
card_name('terraformer', 'Terraformer').
card_type('terraformer', 'Creature — Human Wizard').
card_types('terraformer', ['Creature']).
card_subtypes('terraformer', ['Human', 'Wizard']).
card_colors('terraformer', ['U']).
card_text('terraformer', '{1}: Choose a basic land type. Each land you control becomes that type until end of turn.').
card_mana_cost('terraformer', ['2', 'U']).
card_cmc('terraformer', 3).
card_layout('terraformer', 'normal').
card_power('terraformer', 2).
card_toughness('terraformer', 2).

% Found in: DD2, DD3_JVC, NMS
card_name('terrain generator', 'Terrain Generator').
card_type('terrain generator', 'Land').
card_types('terrain generator', ['Land']).
card_subtypes('terrain generator', []).
card_colors('terrain generator', []).
card_text('terrain generator', '{T}: Add {1} to your mana pool.\n{2}, {T}: You may put a basic land card from your hand onto the battlefield tapped.').
card_layout('terrain generator', 'normal').

% Found in: 10E, ARC, C13, C14, CMD, DDE, DDH, DDN, H09, HOP, M10, M11, MMA, PC2, TSP
card_name('terramorphic expanse', 'Terramorphic Expanse').
card_type('terramorphic expanse', 'Land').
card_types('terramorphic expanse', ['Land']).
card_subtypes('terramorphic expanse', []).
card_colors('terramorphic expanse', []).
card_text('terramorphic expanse', '{T}, Sacrifice Terramorphic Expanse: Search your library for a basic land card and put it onto the battlefield tapped. Then shuffle your library.').
card_layout('terramorphic expanse', 'normal').

% Found in: RAV
card_name('terrarion', 'Terrarion').
card_type('terrarion', 'Artifact').
card_types('terrarion', ['Artifact']).
card_subtypes('terrarion', []).
card_colors('terrarion', []).
card_text('terrarion', 'Terrarion enters the battlefield tapped.\n{2}, {T}, Sacrifice Terrarion: Add two mana in any combination of colors to your mana pool.\nWhen Terrarion is put into a graveyard from the battlefield, draw a card.').
card_mana_cost('terrarion', ['1']).
card_cmc('terrarion', 1).
card_layout('terrarion', 'normal').

% Found in: ODY
card_name('terravore', 'Terravore').
card_type('terravore', 'Creature — Lhurgoyf').
card_types('terravore', ['Creature']).
card_subtypes('terravore', ['Lhurgoyf']).
card_colors('terravore', ['G']).
card_text('terravore', 'Trample\nTerravore\'s power and toughness are each equal to the number of land cards in all graveyards.').
card_mana_cost('terravore', ['1', 'G', 'G']).
card_cmc('terravore', 3).
card_layout('terravore', 'normal').
card_power('terravore', '*').
card_toughness('terravore', '*').

% Found in: AVR, DDL
card_name('terrifying presence', 'Terrifying Presence').
card_type('terrifying presence', 'Instant').
card_types('terrifying presence', ['Instant']).
card_subtypes('terrifying presence', []).
card_colors('terrifying presence', ['G']).
card_text('terrifying presence', 'Prevent all combat damage that would be dealt by creatures other than target creature this turn.').
card_mana_cost('terrifying presence', ['1', 'G']).
card_cmc('terrifying presence', 2).
card_layout('terrifying presence', 'normal').

% Found in: BFZ, DDP, ZEN
card_name('territorial baloth', 'Territorial Baloth').
card_type('territorial baloth', 'Creature — Beast').
card_types('territorial baloth', ['Creature']).
card_subtypes('territorial baloth', ['Beast']).
card_colors('territorial baloth', ['G']).
card_text('territorial baloth', 'Landfall — Whenever a land enters the battlefield under your control, Territorial Baloth gets +2/+2 until end of turn.').
card_mana_cost('territorial baloth', ['4', 'G']).
card_cmc('territorial baloth', 5).
card_layout('territorial baloth', 'normal').
card_power('territorial baloth', 4).
card_toughness('territorial baloth', 4).

% Found in: MMQ
card_name('territorial dispute', 'Territorial Dispute').
card_type('territorial dispute', 'Enchantment').
card_types('territorial dispute', ['Enchantment']).
card_subtypes('territorial dispute', []).
card_colors('territorial dispute', ['R']).
card_text('territorial dispute', 'At the beginning of your upkeep, sacrifice Territorial Dispute unless you sacrifice a land.\nPlayers can\'t play lands.').
card_mana_cost('territorial dispute', ['4', 'R', 'R']).
card_cmc('territorial dispute', 6).
card_layout('territorial dispute', 'normal').

% Found in: DTK
card_name('territorial roc', 'Territorial Roc').
card_type('territorial roc', 'Creature — Bird').
card_types('territorial roc', ['Creature']).
card_subtypes('territorial roc', ['Bird']).
card_colors('territorial roc', ['W']).
card_text('territorial roc', 'Flying').
card_mana_cost('territorial roc', ['1', 'W']).
card_cmc('territorial roc', 2).
card_layout('territorial roc', 'normal').
card_power('territorial roc', 1).
card_toughness('territorial roc', 3).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, ATH, BRB, BTD, CED, CEI, DPA, ITP, LEA, LEB, ME4, MRD, RQS, S00, pFNM, pMPR
card_name('terror', 'Terror').
card_type('terror', 'Instant').
card_types('terror', ['Instant']).
card_subtypes('terror', []).
card_colors('terror', ['B']).
card_text('terror', 'Destroy target nonartifact, nonblack creature. It can\'t be regenerated.').
card_mana_cost('terror', ['1', 'B']).
card_cmc('terror', 2).
card_layout('terror', 'normal').

% Found in: ISD
card_name('terror of kruin pass', 'Terror of Kruin Pass').
card_type('terror of kruin pass', 'Creature — Werewolf').
card_types('terror of kruin pass', ['Creature']).
card_subtypes('terror of kruin pass', ['Werewolf']).
card_colors('terror of kruin pass', ['R']).
card_text('terror of kruin pass', 'Double strike\nWerewolves you control have menace. (They can\'t be blocked except by two or more creatures.)\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Terror of Kruin Pass.').
card_layout('terror of kruin pass', 'double-faced').
card_power('terror of kruin pass', 3).
card_toughness('terror of kruin pass', 3).

% Found in: RTR
card_name('terrus wurm', 'Terrus Wurm').
card_type('terrus wurm', 'Creature — Zombie Wurm').
card_types('terrus wurm', ['Creature']).
card_subtypes('terrus wurm', ['Zombie', 'Wurm']).
card_colors('terrus wurm', ['B']).
card_text('terrus wurm', 'Scavenge {6}{B} ({6}{B}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('terrus wurm', ['6', 'B']).
card_cmc('terrus wurm', 7).
card_layout('terrus wurm', 'normal').
card_power('terrus wurm', 5).
card_toughness('terrus wurm', 5).

% Found in: JUD
card_name('test of endurance', 'Test of Endurance').
card_type('test of endurance', 'Enchantment').
card_types('test of endurance', ['Enchantment']).
card_subtypes('test of endurance', []).
card_colors('test of endurance', ['W']).
card_text('test of endurance', 'At the beginning of your upkeep, if you have 50 or more life, you win the game.').
card_mana_cost('test of endurance', ['2', 'W', 'W']).
card_cmc('test of endurance', 4).
card_layout('test of endurance', 'normal').

% Found in: DDG, DST, MMA
card_name('test of faith', 'Test of Faith').
card_type('test of faith', 'Instant').
card_types('test of faith', ['Instant']).
card_subtypes('test of faith', []).
card_colors('test of faith', ['W']).
card_text('test of faith', 'Prevent the next 3 damage that would be dealt to target creature this turn. For each 1 damage prevented this way, put a +1/+1 counter on that creature.').
card_mana_cost('test of faith', ['1', 'W']).
card_cmc('test of faith', 2).
card_layout('test of faith', 'normal').

% Found in: ODY
card_name('testament of faith', 'Testament of Faith').
card_type('testament of faith', 'Enchantment').
card_types('testament of faith', ['Enchantment']).
card_subtypes('testament of faith', []).
card_colors('testament of faith', ['W']).
card_text('testament of faith', '{X}: Testament of Faith becomes an X/X Wall creature with defender in addition to its other types until end of turn.').
card_mana_cost('testament of faith', ['W']).
card_cmc('testament of faith', 1).
card_layout('testament of faith', 'normal').

% Found in: UDS
card_name('tethered griffin', 'Tethered Griffin').
card_type('tethered griffin', 'Creature — Griffin').
card_types('tethered griffin', ['Creature']).
card_subtypes('tethered griffin', ['Griffin']).
card_colors('tethered griffin', ['W']).
card_text('tethered griffin', 'Flying\nWhen you control no enchantments, sacrifice Tethered Griffin.').
card_mana_cost('tethered griffin', ['W']).
card_cmc('tethered griffin', 1).
card_layout('tethered griffin', 'normal').
card_power('tethered griffin', 2).
card_toughness('tethered griffin', 3).

% Found in: ULG
card_name('tethered skirge', 'Tethered Skirge').
card_type('tethered skirge', 'Creature — Imp').
card_types('tethered skirge', ['Creature']).
card_subtypes('tethered skirge', ['Imp']).
card_colors('tethered skirge', ['B']).
card_text('tethered skirge', 'Flying\nWhenever Tethered Skirge becomes the target of a spell or ability, you lose 1 life.').
card_mana_cost('tethered skirge', ['2', 'B']).
card_cmc('tethered skirge', 3).
card_layout('tethered skirge', 'normal').
card_power('tethered skirge', 2).
card_toughness('tethered skirge', 2).

% Found in: JOU
card_name('tethmos high priest', 'Tethmos High Priest').
card_type('tethmos high priest', 'Creature — Cat Cleric').
card_types('tethmos high priest', ['Creature']).
card_subtypes('tethmos high priest', ['Cat', 'Cleric']).
card_colors('tethmos high priest', ['W']).
card_text('tethmos high priest', 'Heroic — Whenever you cast a spell that targets Tethmos High Priest, return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.').
card_mana_cost('tethmos high priest', ['2', 'W']).
card_cmc('tethmos high priest', 3).
card_layout('tethmos high priest', 'normal').
card_power('tethmos high priest', 2).
card_toughness('tethmos high priest', 3).

% Found in: 4ED, ATQ, ME4
card_name('tetravus', 'Tetravus').
card_type('tetravus', 'Artifact Creature — Construct').
card_types('tetravus', ['Artifact', 'Creature']).
card_subtypes('tetravus', ['Construct']).
card_colors('tetravus', []).
card_text('tetravus', 'Flying\nTetravus enters the battlefield with three +1/+1 counters on it.\nAt the beginning of your upkeep, you may remove any number of +1/+1 counters from Tetravus. If you do, put that many 1/1 colorless Tetravite artifact creature tokens onto the battlefield. They each have flying and \"This creature can\'t be enchanted.\"\nAt the beginning of your upkeep, you may exile any number of tokens put onto the battlefield with Tetravus. If you do, put that many +1/+1 counters on Tetravus.').
card_mana_cost('tetravus', ['6']).
card_cmc('tetravus', 6).
card_layout('tetravus', 'normal').
card_power('tetravus', 1).
card_toughness('tetravus', 1).

% Found in: LEG, ME3
card_name('tetsuo umezawa', 'Tetsuo Umezawa').
card_type('tetsuo umezawa', 'Legendary Creature — Human Archer').
card_types('tetsuo umezawa', ['Creature']).
card_subtypes('tetsuo umezawa', ['Human', 'Archer']).
card_supertypes('tetsuo umezawa', ['Legendary']).
card_colors('tetsuo umezawa', ['U', 'B', 'R']).
card_text('tetsuo umezawa', 'Tetsuo Umezawa can\'t be the target of Aura spells.\n{U}{B}{B}{R}, {T}: Destroy target tapped or blocking creature.').
card_mana_cost('tetsuo umezawa', ['U', 'B', 'R']).
card_cmc('tetsuo umezawa', 3).
card_layout('tetsuo umezawa', 'normal').
card_power('tetsuo umezawa', 3).
card_toughness('tetsuo umezawa', 3).
card_reserved('tetsuo umezawa').

% Found in: DGM
card_name('teysa, envoy of ghosts', 'Teysa, Envoy of Ghosts').
card_type('teysa, envoy of ghosts', 'Legendary Creature — Human Advisor').
card_types('teysa, envoy of ghosts', ['Creature']).
card_subtypes('teysa, envoy of ghosts', ['Human', 'Advisor']).
card_supertypes('teysa, envoy of ghosts', ['Legendary']).
card_colors('teysa, envoy of ghosts', ['W', 'B']).
card_text('teysa, envoy of ghosts', 'Vigilance, protection from creatures\nWhenever a creature deals combat damage to you, destroy that creature. Put a 1/1 white and black Spirit creature token with flying onto the battlefield.').
card_mana_cost('teysa, envoy of ghosts', ['5', 'W', 'B']).
card_cmc('teysa, envoy of ghosts', 7).
card_layout('teysa, envoy of ghosts', 'normal').
card_power('teysa, envoy of ghosts', 4).
card_toughness('teysa, envoy of ghosts', 4).

% Found in: GPT
card_name('teysa, orzhov scion', 'Teysa, Orzhov Scion').
card_type('teysa, orzhov scion', 'Legendary Creature — Human Advisor').
card_types('teysa, orzhov scion', ['Creature']).
card_subtypes('teysa, orzhov scion', ['Human', 'Advisor']).
card_supertypes('teysa, orzhov scion', ['Legendary']).
card_colors('teysa, orzhov scion', ['W', 'B']).
card_text('teysa, orzhov scion', 'Sacrifice three white creatures: Exile target creature.\nWhenever another black creature you control dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('teysa, orzhov scion', ['1', 'W', 'B']).
card_cmc('teysa, orzhov scion', 3).
card_layout('teysa, orzhov scion', 'normal').
card_power('teysa, orzhov scion', 2).
card_toughness('teysa, orzhov scion', 3).

% Found in: VAN
card_name('teysa, orzhov scion avatar', 'Teysa, Orzhov Scion Avatar').
card_type('teysa, orzhov scion avatar', 'Vanguard').
card_types('teysa, orzhov scion avatar', ['Vanguard']).
card_subtypes('teysa, orzhov scion avatar', []).
card_colors('teysa, orzhov scion avatar', []).
card_text('teysa, orzhov scion avatar', 'Whenever a nontoken creature dies, put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_layout('teysa, orzhov scion avatar', 'vanguard').

% Found in: ALA, DDF, MM2
card_name('tezzeret the seeker', 'Tezzeret the Seeker').
card_type('tezzeret the seeker', 'Planeswalker — Tezzeret').
card_types('tezzeret the seeker', ['Planeswalker']).
card_subtypes('tezzeret the seeker', ['Tezzeret']).
card_colors('tezzeret the seeker', ['U']).
card_text('tezzeret the seeker', '+1: Untap up to two target artifacts.\n−X: Search your library for an artifact card with converted mana cost X or less and put it onto the battlefield. Then shuffle your library.\n−5: Artifacts you control become artifact creatures with base power and toughness 5/5 until end of turn.').
card_mana_cost('tezzeret the seeker', ['3', 'U', 'U']).
card_cmc('tezzeret the seeker', 5).
card_layout('tezzeret the seeker', 'normal').
card_loyalty('tezzeret the seeker', 4).

% Found in: MM2, NPH
card_name('tezzeret\'s gambit', 'Tezzeret\'s Gambit').
card_type('tezzeret\'s gambit', 'Sorcery').
card_types('tezzeret\'s gambit', ['Sorcery']).
card_subtypes('tezzeret\'s gambit', []).
card_colors('tezzeret\'s gambit', ['U']).
card_text('tezzeret\'s gambit', '({U/P} can be paid with either {U} or 2 life.)\nDraw two cards, then proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('tezzeret\'s gambit', ['3', 'U/P']).
card_cmc('tezzeret\'s gambit', 4).
card_layout('tezzeret\'s gambit', 'normal').

% Found in: MBS
card_name('tezzeret, agent of bolas', 'Tezzeret, Agent of Bolas').
card_type('tezzeret, agent of bolas', 'Planeswalker — Tezzeret').
card_types('tezzeret, agent of bolas', ['Planeswalker']).
card_subtypes('tezzeret, agent of bolas', ['Tezzeret']).
card_colors('tezzeret, agent of bolas', ['U', 'B']).
card_text('tezzeret, agent of bolas', '+1: Look at the top five cards of your library. You may reveal an artifact card from among them and put it into your hand. Put the rest on the bottom of your library in any order.\n−1: Target artifact becomes an artifact creature with base power and toughness 5/5.\n−4: Target player loses X life and you gain X life, where X is twice the number of artifacts you control.').
card_mana_cost('tezzeret, agent of bolas', ['2', 'U', 'B']).
card_cmc('tezzeret, agent of bolas', 4).
card_layout('tezzeret, agent of bolas', 'normal').
card_loyalty('tezzeret, agent of bolas', 3).

% Found in: WWK
card_name('thada adel, acquisitor', 'Thada Adel, Acquisitor').
card_type('thada adel, acquisitor', 'Legendary Creature — Merfolk Rogue').
card_types('thada adel, acquisitor', ['Creature']).
card_subtypes('thada adel, acquisitor', ['Merfolk', 'Rogue']).
card_supertypes('thada adel, acquisitor', ['Legendary']).
card_colors('thada adel, acquisitor', ['U']).
card_text('thada adel, acquisitor', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhenever Thada Adel, Acquisitor deals combat damage to a player, search that player\'s library for an artifact card and exile it. Then that player shuffles his or her library. Until end of turn, you may play that card.').
card_mana_cost('thada adel, acquisitor', ['1', 'U', 'U']).
card_cmc('thada adel, acquisitor', 3).
card_layout('thada adel, acquisitor', 'normal').
card_power('thada adel, acquisitor', 2).
card_toughness('thada adel, acquisitor', 2).

% Found in: STH
card_name('thalakos deceiver', 'Thalakos Deceiver').
card_type('thalakos deceiver', 'Creature — Thalakos Wizard').
card_types('thalakos deceiver', ['Creature']).
card_subtypes('thalakos deceiver', ['Thalakos', 'Wizard']).
card_colors('thalakos deceiver', ['U']).
card_text('thalakos deceiver', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhenever Thalakos Deceiver attacks and isn\'t blocked, you may sacrifice it. If you do, gain control of target creature. (This effect lasts indefinitely.)').
card_mana_cost('thalakos deceiver', ['3', 'U']).
card_cmc('thalakos deceiver', 4).
card_layout('thalakos deceiver', 'normal').
card_power('thalakos deceiver', 1).
card_toughness('thalakos deceiver', 1).

% Found in: TMP
card_name('thalakos dreamsower', 'Thalakos Dreamsower').
card_type('thalakos dreamsower', 'Creature — Thalakos Wizard').
card_types('thalakos dreamsower', ['Creature']).
card_subtypes('thalakos dreamsower', ['Thalakos', 'Wizard']).
card_colors('thalakos dreamsower', ['U']).
card_text('thalakos dreamsower', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nYou may choose not to untap Thalakos Dreamsower during your untap step.\nWhenever Thalakos Dreamsower deals damage to an opponent, tap target creature. That creature doesn\'t untap during its controller\'s untap step for as long as Thalakos Dreamsower remains tapped.').
card_mana_cost('thalakos dreamsower', ['2', 'U']).
card_cmc('thalakos dreamsower', 3).
card_layout('thalakos dreamsower', 'normal').
card_power('thalakos dreamsower', 1).
card_toughness('thalakos dreamsower', 1).

% Found in: EXO, TPR, VMA
card_name('thalakos drifters', 'Thalakos Drifters').
card_type('thalakos drifters', 'Creature — Thalakos').
card_types('thalakos drifters', ['Creature']).
card_subtypes('thalakos drifters', ['Thalakos']).
card_colors('thalakos drifters', ['U']).
card_text('thalakos drifters', 'Discard a card: Thalakos Drifters gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_mana_cost('thalakos drifters', ['2', 'U', 'U']).
card_cmc('thalakos drifters', 4).
card_layout('thalakos drifters', 'normal').
card_power('thalakos drifters', 3).
card_toughness('thalakos drifters', 3).

% Found in: BRB, TMP, TPR
card_name('thalakos lowlands', 'Thalakos Lowlands').
card_type('thalakos lowlands', 'Land').
card_types('thalakos lowlands', ['Land']).
card_subtypes('thalakos lowlands', []).
card_colors('thalakos lowlands', []).
card_text('thalakos lowlands', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Thalakos Lowlands doesn\'t untap during your next untap step.').
card_layout('thalakos lowlands', 'normal').

% Found in: TMP
card_name('thalakos mistfolk', 'Thalakos Mistfolk').
card_type('thalakos mistfolk', 'Creature — Thalakos Illusion').
card_types('thalakos mistfolk', ['Creature']).
card_subtypes('thalakos mistfolk', ['Thalakos', 'Illusion']).
card_colors('thalakos mistfolk', ['U']).
card_text('thalakos mistfolk', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\n{U}: Put Thalakos Mistfolk on top of its owner\'s library.').
card_mana_cost('thalakos mistfolk', ['2', 'U']).
card_cmc('thalakos mistfolk', 3).
card_layout('thalakos mistfolk', 'normal').
card_power('thalakos mistfolk', 2).
card_toughness('thalakos mistfolk', 1).

% Found in: EXO, TPR
card_name('thalakos scout', 'Thalakos Scout').
card_type('thalakos scout', 'Creature — Thalakos Soldier Scout').
card_types('thalakos scout', ['Creature']).
card_subtypes('thalakos scout', ['Thalakos', 'Soldier', 'Scout']).
card_colors('thalakos scout', ['U']).
card_text('thalakos scout', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nDiscard a card: Return Thalakos Scout to its owner\'s hand.').
card_mana_cost('thalakos scout', ['2', 'U']).
card_cmc('thalakos scout', 3).
card_layout('thalakos scout', 'normal').
card_power('thalakos scout', 2).
card_toughness('thalakos scout', 1).

% Found in: TMP, TPR
card_name('thalakos seer', 'Thalakos Seer').
card_type('thalakos seer', 'Creature — Thalakos Wizard').
card_types('thalakos seer', ['Creature']).
card_subtypes('thalakos seer', ['Thalakos', 'Wizard']).
card_colors('thalakos seer', ['U']).
card_text('thalakos seer', 'Shadow (This creature can block or be blocked by only creatures with shadow.)\nWhen Thalakos Seer leaves the battlefield, draw a card.').
card_mana_cost('thalakos seer', ['U', 'U']).
card_cmc('thalakos seer', 2).
card_layout('thalakos seer', 'normal').
card_power('thalakos seer', 1).
card_toughness('thalakos seer', 1).

% Found in: TMP
card_name('thalakos sentry', 'Thalakos Sentry').
card_type('thalakos sentry', 'Creature — Thalakos Soldier').
card_types('thalakos sentry', ['Creature']).
card_subtypes('thalakos sentry', ['Thalakos', 'Soldier']).
card_colors('thalakos sentry', ['U']).
card_text('thalakos sentry', 'Shadow (This creature can block or be blocked by only creatures with shadow.)').
card_mana_cost('thalakos sentry', ['1', 'U']).
card_cmc('thalakos sentry', 2).
card_layout('thalakos sentry', 'normal').
card_power('thalakos sentry', 1).
card_toughness('thalakos sentry', 2).

% Found in: DKA
card_name('thalia, guardian of thraben', 'Thalia, Guardian of Thraben').
card_type('thalia, guardian of thraben', 'Legendary Creature — Human Soldier').
card_types('thalia, guardian of thraben', ['Creature']).
card_subtypes('thalia, guardian of thraben', ['Human', 'Soldier']).
card_supertypes('thalia, guardian of thraben', ['Legendary']).
card_colors('thalia, guardian of thraben', ['W']).
card_text('thalia, guardian of thraben', 'First strike\nNoncreature spells cost {1} more to cast.').
card_mana_cost('thalia, guardian of thraben', ['1', 'W']).
card_cmc('thalia, guardian of thraben', 2).
card_layout('thalia, guardian of thraben', 'normal').
card_power('thalia, guardian of thraben', 2).
card_toughness('thalia, guardian of thraben', 1).

% Found in: FEM, ME2, MMA, TSB
card_name('thallid', 'Thallid').
card_type('thallid', 'Creature — Fungus').
card_types('thallid', ['Creature']).
card_subtypes('thallid', ['Fungus']).
card_colors('thallid', ['G']).
card_text('thallid', 'At the beginning of your upkeep, put a spore counter on Thallid.\nRemove three spore counters from Thallid: Put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('thallid', ['G']).
card_cmc('thallid', 1).
card_layout('thallid', 'normal').
card_power('thallid', 1).
card_toughness('thallid', 1).

% Found in: FEM, ME2
card_name('thallid devourer', 'Thallid Devourer').
card_type('thallid devourer', 'Creature — Fungus').
card_types('thallid devourer', ['Creature']).
card_subtypes('thallid devourer', ['Fungus']).
card_colors('thallid devourer', ['G']).
card_text('thallid devourer', 'At the beginning of your upkeep, put a spore counter on Thallid Devourer.\nRemove three spore counters from Thallid Devourer: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Thallid Devourer gets +1/+2 until end of turn.').
card_mana_cost('thallid devourer', ['1', 'G', 'G']).
card_cmc('thallid devourer', 3).
card_layout('thallid devourer', 'normal').
card_power('thallid devourer', 2).
card_toughness('thallid devourer', 2).

% Found in: MMA, TSP
card_name('thallid germinator', 'Thallid Germinator').
card_type('thallid germinator', 'Creature — Fungus').
card_types('thallid germinator', ['Creature']).
card_subtypes('thallid germinator', ['Fungus']).
card_colors('thallid germinator', ['G']).
card_text('thallid germinator', 'At the beginning of your upkeep, put a spore counter on Thallid Germinator.\nRemove three spore counters from Thallid Germinator: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Target creature gets +1/+1 until end of turn.').
card_mana_cost('thallid germinator', ['2', 'G']).
card_cmc('thallid germinator', 3).
card_layout('thallid germinator', 'normal').
card_power('thallid germinator', 2).
card_toughness('thallid germinator', 2).

% Found in: MMA, TSP
card_name('thallid shell-dweller', 'Thallid Shell-Dweller').
card_type('thallid shell-dweller', 'Creature — Fungus').
card_types('thallid shell-dweller', ['Creature']).
card_subtypes('thallid shell-dweller', ['Fungus']).
card_colors('thallid shell-dweller', ['G']).
card_text('thallid shell-dweller', 'Defender\nAt the beginning of your upkeep, put a spore counter on Thallid Shell-Dweller.\nRemove three spore counters from Thallid Shell-Dweller: Put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('thallid shell-dweller', ['1', 'G']).
card_cmc('thallid shell-dweller', 2).
card_layout('thallid shell-dweller', 'normal').
card_power('thallid shell-dweller', 0).
card_toughness('thallid shell-dweller', 5).

% Found in: THS
card_name('thassa\'s bounty', 'Thassa\'s Bounty').
card_type('thassa\'s bounty', 'Sorcery').
card_types('thassa\'s bounty', ['Sorcery']).
card_subtypes('thassa\'s bounty', []).
card_colors('thassa\'s bounty', ['U']).
card_text('thassa\'s bounty', 'Draw three cards. Target player puts the top three cards of his or her library into his or her graveyard.').
card_mana_cost('thassa\'s bounty', ['5', 'U']).
card_cmc('thassa\'s bounty', 6).
card_layout('thassa\'s bounty', 'normal').

% Found in: JOU
card_name('thassa\'s devourer', 'Thassa\'s Devourer').
card_type('thassa\'s devourer', 'Enchantment Creature — Elemental').
card_types('thassa\'s devourer', ['Enchantment', 'Creature']).
card_subtypes('thassa\'s devourer', ['Elemental']).
card_colors('thassa\'s devourer', ['U']).
card_text('thassa\'s devourer', 'Constellation — Whenever Thassa\'s Devourer or another enchantment enters the battlefield under your control, target player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('thassa\'s devourer', ['4', 'U']).
card_cmc('thassa\'s devourer', 5).
card_layout('thassa\'s devourer', 'normal').
card_power('thassa\'s devourer', 2).
card_toughness('thassa\'s devourer', 6).

% Found in: THS
card_name('thassa\'s emissary', 'Thassa\'s Emissary').
card_type('thassa\'s emissary', 'Enchantment Creature — Crab').
card_types('thassa\'s emissary', ['Enchantment', 'Creature']).
card_subtypes('thassa\'s emissary', ['Crab']).
card_colors('thassa\'s emissary', ['U']).
card_text('thassa\'s emissary', 'Bestow {5}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nWhenever Thassa\'s Emissary or enchanted creature deals combat damage to a player, draw a card.\nEnchanted creature gets +3/+3.').
card_mana_cost('thassa\'s emissary', ['3', 'U']).
card_cmc('thassa\'s emissary', 4).
card_layout('thassa\'s emissary', 'normal').
card_power('thassa\'s emissary', 3).
card_toughness('thassa\'s emissary', 3).

% Found in: JOU
card_name('thassa\'s ire', 'Thassa\'s Ire').
card_type('thassa\'s ire', 'Enchantment').
card_types('thassa\'s ire', ['Enchantment']).
card_subtypes('thassa\'s ire', []).
card_colors('thassa\'s ire', ['U']).
card_text('thassa\'s ire', '{3}{U}: You may tap or untap target creature.').
card_mana_cost('thassa\'s ire', ['U']).
card_cmc('thassa\'s ire', 1).
card_layout('thassa\'s ire', 'normal').

% Found in: BNG
card_name('thassa\'s rebuff', 'Thassa\'s Rebuff').
card_type('thassa\'s rebuff', 'Instant').
card_types('thassa\'s rebuff', ['Instant']).
card_subtypes('thassa\'s rebuff', []).
card_colors('thassa\'s rebuff', ['U']).
card_text('thassa\'s rebuff', 'Counter target spell unless its controller pays {X}, where X is your devotion to blue. (Each {U} in the mana costs of permanents you control counts toward your devotion to blue.)').
card_mana_cost('thassa\'s rebuff', ['1', 'U']).
card_cmc('thassa\'s rebuff', 2).
card_layout('thassa\'s rebuff', 'normal').

% Found in: THS
card_name('thassa, god of the sea', 'Thassa, God of the Sea').
card_type('thassa, god of the sea', 'Legendary Enchantment Creature — God').
card_types('thassa, god of the sea', ['Enchantment', 'Creature']).
card_subtypes('thassa, god of the sea', ['God']).
card_supertypes('thassa, god of the sea', ['Legendary']).
card_colors('thassa, god of the sea', ['U']).
card_text('thassa, god of the sea', 'Indestructible\nAs long as your devotion to blue is less than five, Thassa isn\'t a creature. (Each {U} in the mana costs of permanents you control counts toward your devotion to blue.)\nAt the beginning of your upkeep, scry 1.\n{1}{U}: Target creature you control can\'t be blocked this turn.').
card_mana_cost('thassa, god of the sea', ['2', 'U']).
card_cmc('thassa, god of the sea', 3).
card_layout('thassa, god of the sea', 'normal').
card_power('thassa, god of the sea', 5).
card_toughness('thassa, god of the sea', 5).

% Found in: BOK
card_name('that which was taken', 'That Which Was Taken').
card_type('that which was taken', 'Legendary Artifact').
card_types('that which was taken', ['Artifact']).
card_subtypes('that which was taken', []).
card_supertypes('that which was taken', ['Legendary']).
card_colors('that which was taken', []).
card_text('that which was taken', '{4}, {T}: Put a divinity counter on target permanent other than That Which Was Taken.\nEach permanent with a divinity counter on it has indestructible.').
card_mana_cost('that which was taken', ['5']).
card_cmc('that which was taken', 5).
card_layout('that which was taken', 'normal').

% Found in: AVR
card_name('thatcher revolt', 'Thatcher Revolt').
card_type('thatcher revolt', 'Sorcery').
card_types('thatcher revolt', ['Sorcery']).
card_subtypes('thatcher revolt', []).
card_colors('thatcher revolt', ['R']).
card_text('thatcher revolt', 'Put three 1/1 red Human creature tokens with haste onto the battlefield. Sacrifice those tokens at the beginning of the next end step.').
card_mana_cost('thatcher revolt', ['2', 'R']).
card_cmc('thatcher revolt', 3).
card_layout('thatcher revolt', 'normal').

% Found in: ODY
card_name('thaumatog', 'Thaumatog').
card_type('thaumatog', 'Creature — Atog').
card_types('thaumatog', ['Creature']).
card_subtypes('thaumatog', ['Atog']).
card_colors('thaumatog', ['W', 'G']).
card_text('thaumatog', 'Sacrifice a land: Thaumatog gets +1/+1 until end of turn.\nSacrifice an enchantment: Thaumatog gets +1/+1 until end of turn.').
card_mana_cost('thaumatog', ['1', 'G', 'W']).
card_cmc('thaumatog', 3).
card_layout('thaumatog', 'normal').
card_power('thaumatog', 1).
card_toughness('thaumatog', 2).

% Found in: ALL, MED, VMA, pJGP
card_name('thawing glaciers', 'Thawing Glaciers').
card_type('thawing glaciers', 'Land').
card_types('thawing glaciers', ['Land']).
card_subtypes('thawing glaciers', []).
card_colors('thawing glaciers', []).
card_text('thawing glaciers', 'Thawing Glaciers enters the battlefield tapped.\n{1}, {T}: Search your library for a basic land card, put that card onto the battlefield tapped, then shuffle your library. Return Thawing Glaciers to its owner\'s hand at the beginning of the next cleanup step.').
card_layout('thawing glaciers', 'normal').
card_reserved('thawing glaciers').

% Found in: LEG, ME3
card_name('the abyss', 'The Abyss').
card_type('the abyss', 'World Enchantment').
card_types('the abyss', ['Enchantment']).
card_subtypes('the abyss', []).
card_supertypes('the abyss', ['World']).
card_colors('the abyss', ['B']).
card_text('the abyss', 'At the beginning of each player\'s upkeep, destroy target nonartifact creature that player controls of his or her choice. It can\'t be regenerated.').
card_mana_cost('the abyss', ['3', 'B']).
card_cmc('the abyss', 4).
card_layout('the abyss', 'normal').
card_reserved('the abyss').

% Found in: 4ED, 5ED, LEG
card_name('the brute', 'The Brute').
card_type('the brute', 'Enchantment — Aura').
card_types('the brute', ['Enchantment']).
card_subtypes('the brute', ['Aura']).
card_colors('the brute', ['R']).
card_text('the brute', 'Enchant creature\nEnchanted creature gets +1/+0.\n{R}{R}{R}: Regenerate enchanted creature.').
card_mana_cost('the brute', ['1', 'R']).
card_cmc('the brute', 2).
card_layout('the brute', 'normal').

% Found in: M15
card_name('the chain veil', 'The Chain Veil').
card_type('the chain veil', 'Legendary Artifact').
card_types('the chain veil', ['Artifact']).
card_subtypes('the chain veil', []).
card_supertypes('the chain veil', ['Legendary']).
card_colors('the chain veil', []).
card_text('the chain veil', 'At the beginning of your end step, if you didn\'t activate a loyalty ability of a planeswalker this turn, you lose 2 life.\n{4}, {T}: For each planeswalker you control, you may activate one of its loyalty abilities once this turn as though none of its loyalty abilities have been activated this turn.').
card_mana_cost('the chain veil', ['4']).
card_cmc('the chain veil', 4).
card_layout('the chain veil', 'normal').

% Found in: UGL
card_name('the cheese stands alone', 'The Cheese Stands Alone').
card_type('the cheese stands alone', 'Enchantment').
card_types('the cheese stands alone', ['Enchantment']).
card_subtypes('the cheese stands alone', []).
card_colors('the cheese stands alone', ['W']).
card_text('the cheese stands alone', 'If you control no cards in play other than The Cheese Stands Alone and have no cards in your hand, you win the game.').
card_mana_cost('the cheese stands alone', ['4', 'W', 'W']).
card_cmc('the cheese stands alone', 6).
card_layout('the cheese stands alone', 'normal').

% Found in: HOP
card_name('the dark barony', 'The Dark Barony').
card_type('the dark barony', 'Plane — Ulgrotha').
card_types('the dark barony', ['Plane']).
card_subtypes('the dark barony', ['Ulgrotha']).
card_colors('the dark barony', []).
card_text('the dark barony', 'Whenever a nonblack card is put into a player\'s graveyard from anywhere, that player loses 1 life.\nWhenever you roll {C}, each opponent discards a card.').
card_layout('the dark barony', 'plane').

% Found in: ARC
card_name('the dead shall serve', 'The Dead Shall Serve').
card_type('the dead shall serve', 'Scheme').
card_types('the dead shall serve', ['Scheme']).
card_subtypes('the dead shall serve', []).
card_colors('the dead shall serve', []).
card_text('the dead shall serve', 'When you set this scheme in motion, for each opponent, put up to one target creature card from that player\'s graveyard onto the battlefield under your control. Each of those creatures attacks its owner each combat if able.').
card_layout('the dead shall serve', 'scheme').

% Found in: HOP
card_name('the eon fog', 'The Eon Fog').
card_type('the eon fog', 'Plane — Equilor').
card_types('the eon fog', ['Plane']).
card_subtypes('the eon fog', ['Equilor']).
card_colors('the eon fog', []).
card_text('the eon fog', 'Players skip their untap steps.\nWhenever you roll {C}, untap all permanents you control.').
card_layout('the eon fog', 'plane').

% Found in: CHR, DRK, MED
card_name('the fallen', 'The Fallen').
card_type('the fallen', 'Creature — Zombie').
card_types('the fallen', ['Creature']).
card_subtypes('the fallen', ['Zombie']).
card_colors('the fallen', ['B']).
card_text('the fallen', 'At the beginning of your upkeep, The Fallen deals 1 damage to each opponent it has dealt damage to this game.').
card_mana_cost('the fallen', ['1', 'B', 'B', 'B']).
card_cmc('the fallen', 4).
card_layout('the fallen', 'normal').
card_power('the fallen', 2).
card_toughness('the fallen', 3).

% Found in: UNH
card_name('the fallen apart', 'The Fallen Apart').
card_type('the fallen apart', 'Creature — Zombie').
card_types('the fallen apart', ['Creature']).
card_subtypes('the fallen apart', ['Zombie']).
card_colors('the fallen apart', ['B']).
card_text('the fallen apart', 'The Fallen Apart comes into play with two arms and two legs.\nWhenever damage is dealt to The Fallen Apart, remove an arm or a leg from it.\nThe Fallen Apart can\'t attack if it has no legs and can\'t block if it has no arms.').
card_mana_cost('the fallen apart', ['2', 'B', 'B']).
card_cmc('the fallen apart', 4).
card_layout('the fallen apart', 'normal').
card_power('the fallen apart', 4).
card_toughness('the fallen apart', 4).

% Found in: ARC
card_name('the fate of the flammable', 'The Fate of the Flammable').
card_type('the fate of the flammable', 'Scheme').
card_types('the fate of the flammable', ['Scheme']).
card_subtypes('the fate of the flammable', []).
card_colors('the fate of the flammable', []).
card_text('the fate of the flammable', 'When you set this scheme in motion, target opponent chooses self or others. If that player chooses self, this scheme deals 6 damage to him or her. If the player chooses others, this scheme deals 3 damage to each of your other opponents.').
card_layout('the fate of the flammable', 'scheme').

% Found in: HOP
card_name('the fourth sphere', 'The Fourth Sphere').
card_type('the fourth sphere', 'Plane — Phyrexia').
card_types('the fourth sphere', ['Plane']).
card_subtypes('the fourth sphere', ['Phyrexia']).
card_colors('the fourth sphere', []).
card_text('the fourth sphere', 'At the beginning of your upkeep, sacrifice a nonblack creature.\nWhenever you roll {C}, put a 2/2 black Zombie creature token onto the battlefield.').
card_layout('the fourth sphere', 'plane').

% Found in: ORI
card_name('the great aurora', 'The Great Aurora').
card_type('the great aurora', 'Sorcery').
card_types('the great aurora', ['Sorcery']).
card_subtypes('the great aurora', []).
card_colors('the great aurora', ['G']).
card_text('the great aurora', 'Each player shuffles all cards from his or her hand and all permanents he or she owns into his or her library, then draws that many cards. Each player may put any number of land cards from his or her hand onto the battlefield. Exile The Great Aurora.').
card_mana_cost('the great aurora', ['6', 'G', 'G', 'G']).
card_cmc('the great aurora', 9).
card_layout('the great aurora', 'normal').

% Found in: HOP
card_name('the great forest', 'The Great Forest').
card_type('the great forest', 'Plane — Lorwyn').
card_types('the great forest', ['Plane']).
card_subtypes('the great forest', ['Lorwyn']).
card_colors('the great forest', []).
card_text('the great forest', 'Each creature assigns combat damage equal to its toughness rather than its power.\nWhenever you roll {C}, creatures you control get +0/+2 and gain trample until end of turn.').
card_layout('the great forest', 'plane').

% Found in: HOP
card_name('the hippodrome', 'The Hippodrome').
card_type('the hippodrome', 'Plane — Segovia').
card_types('the hippodrome', ['Plane']).
card_subtypes('the hippodrome', ['Segovia']).
card_colors('the hippodrome', []).
card_text('the hippodrome', 'All creatures get -5/-0.\nWhenever you roll {C}, you may destroy target creature if its power is 0 or less.').
card_layout('the hippodrome', 'plane').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB
card_name('the hive', 'The Hive').
card_type('the hive', 'Artifact').
card_types('the hive', ['Artifact']).
card_subtypes('the hive', []).
card_colors('the hive', []).
card_text('the hive', '{5}, {T}: Put a 1/1 colorless Insect artifact creature token with flying named Wasp onto the battlefield. (It can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('the hive', ['5']).
card_cmc('the hive', 5).
card_layout('the hive', 'normal').

% Found in: ARC
card_name('the iron guardian stirs', 'The Iron Guardian Stirs').
card_type('the iron guardian stirs', 'Scheme').
card_types('the iron guardian stirs', ['Scheme']).
card_subtypes('the iron guardian stirs', []).
card_colors('the iron guardian stirs', []).
card_text('the iron guardian stirs', 'When you set this scheme in motion, put a 4/6 colorless Golem artifact creature token onto the battlefield.').
card_layout('the iron guardian stirs', 'scheme').

% Found in: LEG, ME3
card_name('the lady of the mountain', 'The Lady of the Mountain').
card_type('the lady of the mountain', 'Legendary Creature — Giant').
card_types('the lady of the mountain', ['Creature']).
card_subtypes('the lady of the mountain', ['Giant']).
card_supertypes('the lady of the mountain', ['Legendary']).
card_colors('the lady of the mountain', ['R', 'G']).
card_text('the lady of the mountain', '').
card_mana_cost('the lady of the mountain', ['4', 'R', 'G']).
card_cmc('the lady of the mountain', 6).
card_layout('the lady of the mountain', 'normal').
card_power('the lady of the mountain', 5).
card_toughness('the lady of the mountain', 5).

% Found in: HOP
card_name('the maelstrom', 'The Maelstrom').
card_type('the maelstrom', 'Plane — Alara').
card_types('the maelstrom', ['Plane']).
card_subtypes('the maelstrom', ['Alara']).
card_colors('the maelstrom', []).
card_text('the maelstrom', 'When you planeswalk to The Maelstrom or at the beginning of your upkeep, you may reveal the top card of your library. If it\'s a permanent card, you may put it onto the battlefield. If you revealed a card but didn\'t put it onto the battlefield, put it on the bottom of your library.\nWhenever you roll {C}, return target permanent card from your graveyard to the battlefield.').
card_layout('the maelstrom', 'plane').

% Found in: CM1, CMD
card_name('the mimeoplasm', 'The Mimeoplasm').
card_type('the mimeoplasm', 'Legendary Creature — Ooze').
card_types('the mimeoplasm', ['Creature']).
card_subtypes('the mimeoplasm', ['Ooze']).
card_supertypes('the mimeoplasm', ['Legendary']).
card_colors('the mimeoplasm', ['U', 'B', 'G']).
card_text('the mimeoplasm', 'As The Mimeoplasm enters the battlefield, you may exile two creature cards from graveyards. If you do, it enters the battlefield as a copy of one of those cards with a number of additional +1/+1 counters on it equal to the power of the other card.').
card_mana_cost('the mimeoplasm', ['2', 'G', 'U', 'B']).
card_cmc('the mimeoplasm', 5).
card_layout('the mimeoplasm', 'normal').
card_power('the mimeoplasm', 0).
card_toughness('the mimeoplasm', 0).

% Found in: ARC
card_name('the pieces are coming together', 'The Pieces Are Coming Together').
card_type('the pieces are coming together', 'Scheme').
card_types('the pieces are coming together', ['Scheme']).
card_subtypes('the pieces are coming together', []).
card_colors('the pieces are coming together', []).
card_text('the pieces are coming together', 'When you set this scheme in motion, draw two cards. Artifact spells you cast this turn cost {2} less to cast.').
card_layout('the pieces are coming together', 'scheme').

% Found in: 3ED, 4ED, ATQ, DPA, TSB
card_name('the rack', 'The Rack').
card_type('the rack', 'Artifact').
card_types('the rack', ['Artifact']).
card_subtypes('the rack', []).
card_colors('the rack', []).
card_text('the rack', 'As The Rack enters the battlefield, choose an opponent.\nAt the beginning of the chosen player\'s upkeep, The Rack deals X damage to that player, where X is 3 minus the number of cards in his or her hand.').
card_mana_cost('the rack', ['1']).
card_cmc('the rack', 1).
card_layout('the rack', 'normal').

% Found in: LEG, ME3
card_name('the tabernacle at pendrell vale', 'The Tabernacle at Pendrell Vale').
card_type('the tabernacle at pendrell vale', 'Legendary Land').
card_types('the tabernacle at pendrell vale', ['Land']).
card_subtypes('the tabernacle at pendrell vale', []).
card_supertypes('the tabernacle at pendrell vale', ['Legendary']).
card_colors('the tabernacle at pendrell vale', []).
card_text('the tabernacle at pendrell vale', 'All creatures have \"At the beginning of your upkeep, destroy this creature unless you pay {1}.\"').
card_layout('the tabernacle at pendrell vale', 'normal').
card_reserved('the tabernacle at pendrell vale').

% Found in: UGL
card_name('the ultimate nightmare of wizards of the coast® customer service', 'The Ultimate Nightmare of Wizards of the Coast® Customer Service').
card_type('the ultimate nightmare of wizards of the coast® customer service', 'Sorcery').
card_types('the ultimate nightmare of wizards of the coast® customer service', ['Sorcery']).
card_subtypes('the ultimate nightmare of wizards of the coast® customer service', []).
card_colors('the ultimate nightmare of wizards of the coast® customer service', ['R']).
card_text('the ultimate nightmare of wizards of the coast® customer service', 'The Ultimate Nightmare of Wizards of the Coast® Customer Service deals X damage to each of Y target creatures and Z target players.').
card_mana_cost('the ultimate nightmare of wizards of the coast® customer service', ['X', 'Y', 'Z', 'R', 'R']).
card_cmc('the ultimate nightmare of wizards of the coast® customer service', 2).
card_layout('the ultimate nightmare of wizards of the coast® customer service', 'normal').

% Found in: CHK
card_name('the unspeakable', 'The Unspeakable').
card_type('the unspeakable', 'Legendary Creature — Spirit').
card_types('the unspeakable', ['Creature']).
card_subtypes('the unspeakable', ['Spirit']).
card_supertypes('the unspeakable', ['Legendary']).
card_colors('the unspeakable', ['U']).
card_text('the unspeakable', 'Flying, trample\nWhenever The Unspeakable deals combat damage to a player, you may return target Arcane card from your graveyard to your hand.').
card_mana_cost('the unspeakable', ['6', 'U', 'U', 'U']).
card_cmc('the unspeakable', 9).
card_layout('the unspeakable', 'normal').
card_power('the unspeakable', 6).
card_toughness('the unspeakable', 7).

% Found in: ARC
card_name('the very soil shall shake', 'The Very Soil Shall Shake').
card_type('the very soil shall shake', 'Ongoing Scheme').
card_types('the very soil shall shake', ['Scheme']).
card_subtypes('the very soil shall shake', []).
card_supertypes('the very soil shall shake', ['Ongoing']).
card_colors('the very soil shall shake', []).
card_text('the very soil shall shake', '(An ongoing scheme remains face up until it\'s abandoned.)\nCreatures you control get +2/+2 and have trample.\nWhen a creature you control dies, abandon this scheme.').
card_layout('the very soil shall shake', 'scheme').

% Found in: 5ED, CHR, LEG, ME3
card_name('the wretched', 'The Wretched').
card_type('the wretched', 'Creature — Demon').
card_types('the wretched', ['Creature']).
card_subtypes('the wretched', ['Demon']).
card_colors('the wretched', ['B']).
card_text('the wretched', 'At end of combat, gain control of all creatures blocking The Wretched for as long as you control The Wretched.').
card_mana_cost('the wretched', ['3', 'B', 'B']).
card_cmc('the wretched', 5).
card_layout('the wretched', 'normal').
card_power('the wretched', 2).
card_toughness('the wretched', 5).

% Found in: PC2
card_name('the zephyr maze', 'The Zephyr Maze').
card_type('the zephyr maze', 'Plane — Kyneth').
card_types('the zephyr maze', ['Plane']).
card_subtypes('the zephyr maze', ['Kyneth']).
card_colors('the zephyr maze', []).
card_text('the zephyr maze', 'Creatures with flying get +2/+0.\nCreatures without flying get -2/-0.\nWhenever you roll {C}, target creature gains flying until end of turn.').
card_layout('the zephyr maze', 'plane').

% Found in: HOP
card_name('the æther flues', 'The Æther Flues').
card_type('the æther flues', 'Plane — Iquatana').
card_types('the æther flues', ['Plane']).
card_subtypes('the æther flues', ['Iquatana']).
card_colors('the æther flues', []).
card_text('the æther flues', 'When you planeswalk to The Æther Flues or at the beginning of your upkeep, you may sacrifice a creature. If you do, reveal cards from the top of your library until you reveal a creature card, put that card onto the battlefield, then shuffle all other cards revealed this way into your library.\nWhenever you roll {C}, you may put a creature card from your hand onto the battlefield.').
card_layout('the æther flues', 'plane').

% Found in: EXO, ME4, PO2, POR
card_name('theft of dreams', 'Theft of Dreams').
card_type('theft of dreams', 'Sorcery').
card_types('theft of dreams', ['Sorcery']).
card_subtypes('theft of dreams', []).
card_colors('theft of dreams', ['U']).
card_text('theft of dreams', 'Draw a card for each tapped creature target opponent controls.').
card_mana_cost('theft of dreams', ['2', 'U']).
card_cmc('theft of dreams', 3).
card_layout('theft of dreams', 'normal').

% Found in: TSP
card_name('thelon of havenwood', 'Thelon of Havenwood').
card_type('thelon of havenwood', 'Legendary Creature — Elf Druid').
card_types('thelon of havenwood', ['Creature']).
card_subtypes('thelon of havenwood', ['Elf', 'Druid']).
card_supertypes('thelon of havenwood', ['Legendary']).
card_colors('thelon of havenwood', ['G']).
card_text('thelon of havenwood', 'Each Fungus creature gets +1/+1 for each spore counter on it.\n{B}{G}, Exile a Fungus card from a graveyard: Put a spore counter on each Fungus on the battlefield.').
card_mana_cost('thelon of havenwood', ['G', 'G']).
card_cmc('thelon of havenwood', 2).
card_layout('thelon of havenwood', 'normal').
card_power('thelon of havenwood', 2).
card_toughness('thelon of havenwood', 2).

% Found in: FEM
card_name('thelon\'s chant', 'Thelon\'s Chant').
card_type('thelon\'s chant', 'Enchantment').
card_types('thelon\'s chant', ['Enchantment']).
card_subtypes('thelon\'s chant', []).
card_colors('thelon\'s chant', ['G']).
card_text('thelon\'s chant', 'At the beginning of your upkeep, sacrifice Thelon\'s Chant unless you pay {G}.\nWhenever a player puts a Swamp onto the battlefield, Thelon\'s Chant deals 3 damage to that player unless he or she puts a -1/-1 counter on a creature he or she controls.').
card_mana_cost('thelon\'s chant', ['1', 'G', 'G']).
card_cmc('thelon\'s chant', 3).
card_layout('thelon\'s chant', 'normal').

% Found in: FEM
card_name('thelon\'s curse', 'Thelon\'s Curse').
card_type('thelon\'s curse', 'Enchantment').
card_types('thelon\'s curse', ['Enchantment']).
card_subtypes('thelon\'s curse', []).
card_colors('thelon\'s curse', ['G']).
card_text('thelon\'s curse', 'Blue creatures don\'t untap during their controllers\' untap steps.\nAt the beginning of each player\'s upkeep, that player may choose any number of tapped blue creatures he or she controls and pay {U} for each creature chosen this way. If the player does, untap those creatures.').
card_mana_cost('thelon\'s curse', ['G', 'G']).
card_cmc('thelon\'s curse', 2).
card_layout('thelon\'s curse', 'normal').
card_reserved('thelon\'s curse').

% Found in: FEM, ME2
card_name('thelonite druid', 'Thelonite Druid').
card_type('thelonite druid', 'Creature — Human Cleric Druid').
card_types('thelonite druid', ['Creature']).
card_subtypes('thelonite druid', ['Human', 'Cleric', 'Druid']).
card_colors('thelonite druid', ['G']).
card_text('thelonite druid', '{1}{G}, {T}, Sacrifice a creature: Forests you control become 2/3 creatures until end of turn. They\'re still lands.').
card_mana_cost('thelonite druid', ['2', 'G']).
card_cmc('thelonite druid', 3).
card_layout('thelonite druid', 'normal').
card_power('thelonite druid', 1).
card_toughness('thelonite druid', 1).

% Found in: ARC, TSP
card_name('thelonite hermit', 'Thelonite Hermit').
card_type('thelonite hermit', 'Creature — Elf Shaman').
card_types('thelonite hermit', ['Creature']).
card_subtypes('thelonite hermit', ['Elf', 'Shaman']).
card_colors('thelonite hermit', ['G']).
card_text('thelonite hermit', 'Saproling creatures get +1/+1.\nMorph {3}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Thelonite Hermit is turned face up, put four 1/1 green Saproling creature tokens onto the battlefield.').
card_mana_cost('thelonite hermit', ['3', 'G']).
card_cmc('thelonite hermit', 4).
card_layout('thelonite hermit', 'normal').
card_power('thelonite hermit', 1).
card_toughness('thelonite hermit', 1).

% Found in: FEM
card_name('thelonite monk', 'Thelonite Monk').
card_type('thelonite monk', 'Creature — Insect Monk Cleric').
card_types('thelonite monk', ['Creature']).
card_subtypes('thelonite monk', ['Insect', 'Monk', 'Cleric']).
card_colors('thelonite monk', ['G']).
card_text('thelonite monk', '{T}, Sacrifice a green creature: Target land becomes a Forest. (This effect lasts indefinitely.)').
card_mana_cost('thelonite monk', ['2', 'G', 'G']).
card_cmc('thelonite monk', 4).
card_layout('thelonite monk', 'normal').
card_power('thelonite monk', 1).
card_toughness('thelonite monk', 2).
card_reserved('thelonite monk').

% Found in: ODY
card_name('thermal blast', 'Thermal Blast').
card_type('thermal blast', 'Instant').
card_types('thermal blast', ['Instant']).
card_subtypes('thermal blast', []).
card_colors('thermal blast', ['R']).
card_text('thermal blast', 'Thermal Blast deals 3 damage to target creature.\nThreshold — Thermal Blast deals 5 damage to that creature instead if seven or more cards are in your graveyard.').
card_mana_cost('thermal blast', ['4', 'R']).
card_cmc('thermal blast', 5).
card_layout('thermal blast', 'normal').

% Found in: CSP
card_name('thermal flux', 'Thermal Flux').
card_type('thermal flux', 'Instant').
card_types('thermal flux', ['Instant']).
card_subtypes('thermal flux', []).
card_colors('thermal flux', ['U']).
card_text('thermal flux', 'Choose one —\n• Target nonsnow permanent becomes snow until end of turn.\n• Target snow permanent isn\'t snow until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('thermal flux', ['U']).
card_cmc('thermal flux', 1).
card_layout('thermal flux', 'normal').

% Found in: MMQ
card_name('thermal glider', 'Thermal Glider').
card_type('thermal glider', 'Creature — Human Rebel').
card_types('thermal glider', ['Creature']).
card_subtypes('thermal glider', ['Human', 'Rebel']).
card_colors('thermal glider', ['W']).
card_text('thermal glider', 'Flying, protection from red').
card_mana_cost('thermal glider', ['2', 'W']).
card_cmc('thermal glider', 3).
card_layout('thermal glider', 'normal').
card_power('thermal glider', 2).
card_toughness('thermal glider', 1).

% Found in: 5DN
card_name('thermal navigator', 'Thermal Navigator').
card_type('thermal navigator', 'Artifact Creature — Construct').
card_types('thermal navigator', ['Artifact', 'Creature']).
card_subtypes('thermal navigator', ['Construct']).
card_colors('thermal navigator', []).
card_text('thermal navigator', 'Sacrifice an artifact: Thermal Navigator gains flying until end of turn.').
card_mana_cost('thermal navigator', ['3']).
card_cmc('thermal navigator', 3).
card_layout('thermal navigator', 'normal').
card_power('thermal navigator', 2).
card_toughness('thermal navigator', 2).

% Found in: ICE, ME2
card_name('thermokarst', 'Thermokarst').
card_type('thermokarst', 'Sorcery').
card_types('thermokarst', ['Sorcery']).
card_subtypes('thermokarst', []).
card_colors('thermokarst', ['G']).
card_text('thermokarst', 'Destroy target land. If that land was a snow land, you gain 1 life.').
card_mana_cost('thermokarst', ['1', 'G', 'G']).
card_cmc('thermokarst', 3).
card_layout('thermokarst', 'normal').

% Found in: CSP
card_name('thermopod', 'Thermopod').
card_type('thermopod', 'Snow Creature — Slug').
card_types('thermopod', ['Creature']).
card_subtypes('thermopod', ['Slug']).
card_supertypes('thermopod', ['Snow']).
card_colors('thermopod', ['R']).
card_text('thermopod', '{S}: Thermopod gains haste until end of turn. ({S} can be paid with one mana from a snow permanent.)\nSacrifice a creature: Add {R} to your mana pool.').
card_mana_cost('thermopod', ['4', 'R']).
card_cmc('thermopod', 5).
card_layout('thermopod', 'normal').
card_power('thermopod', 4).
card_toughness('thermopod', 3).

% Found in: GTC
card_name('thespian\'s stage', 'Thespian\'s Stage').
card_type('thespian\'s stage', 'Land').
card_types('thespian\'s stage', ['Land']).
card_subtypes('thespian\'s stage', []).
card_colors('thespian\'s stage', []).
card_text('thespian\'s stage', '{T}: Add {1} to your mana pool.\n{2}, {T}: Thespian\'s Stage becomes a copy of target land and gains this ability.').
card_layout('thespian\'s stage', 'normal').

% Found in: TSP
card_name('thick-skinned goblin', 'Thick-Skinned Goblin').
card_type('thick-skinned goblin', 'Creature — Goblin Shaman').
card_types('thick-skinned goblin', ['Creature']).
card_subtypes('thick-skinned goblin', ['Goblin', 'Shaman']).
card_colors('thick-skinned goblin', ['R']).
card_text('thick-skinned goblin', 'You may pay {0} rather than pay the echo cost for permanents you control.\n{R}: Thick-Skinned Goblin gains protection from red until end of turn.').
card_mana_cost('thick-skinned goblin', ['1', 'R']).
card_cmc('thick-skinned goblin', 2).
card_layout('thick-skinned goblin', 'normal').
card_power('thick-skinned goblin', 2).
card_toughness('thick-skinned goblin', 1).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, LEA, LEB, MED
card_name('thicket basilisk', 'Thicket Basilisk').
card_type('thicket basilisk', 'Creature — Basilisk').
card_types('thicket basilisk', ['Creature']).
card_subtypes('thicket basilisk', ['Basilisk']).
card_colors('thicket basilisk', ['G']).
card_text('thicket basilisk', 'Whenever Thicket Basilisk blocks or becomes blocked by a non-Wall creature, destroy that creature at end of combat.').
card_mana_cost('thicket basilisk', ['3', 'G', 'G']).
card_cmc('thicket basilisk', 5).
card_layout('thicket basilisk', 'normal').
card_power('thicket basilisk', 2).
card_toughness('thicket basilisk', 4).

% Found in: INV
card_name('thicket elemental', 'Thicket Elemental').
card_type('thicket elemental', 'Creature — Elemental').
card_types('thicket elemental', ['Creature']).
card_subtypes('thicket elemental', ['Elemental']).
card_colors('thicket elemental', ['G']).
card_text('thicket elemental', 'Kicker {1}{G} (You may pay an additional {1}{G} as you cast this spell.)\nWhen Thicket Elemental enters the battlefield, if it was kicked, you may reveal cards from the top of your library until you reveal a creature card. If you do, put that card onto the battlefield and shuffle all other cards revealed this way into your library.').
card_mana_cost('thicket elemental', ['3', 'G', 'G']).
card_cmc('thicket elemental', 5).
card_layout('thicket elemental', 'normal').
card_power('thicket elemental', 4).
card_toughness('thicket elemental', 4).

% Found in: CHK, MM2
card_name('thief of hope', 'Thief of Hope').
card_type('thief of hope', 'Creature — Spirit').
card_types('thief of hope', ['Creature']).
card_subtypes('thief of hope', ['Spirit']).
card_colors('thief of hope', ['B']).
card_text('thief of hope', 'Whenever you cast a Spirit or Arcane spell, target opponent loses 1 life and you gain 1 life.\nSoulshift 2 (When this creature dies, you may return target Spirit card with converted mana cost 2 or less from your graveyard to your hand.)').
card_mana_cost('thief of hope', ['2', 'B']).
card_cmc('thief of hope', 3).
card_layout('thief of hope', 'normal').
card_power('thief of hope', 2).
card_toughness('thief of hope', 2).

% Found in: 8ED, MMQ
card_name('thieves\' auction', 'Thieves\' Auction').
card_type('thieves\' auction', 'Sorcery').
card_types('thieves\' auction', ['Sorcery']).
card_subtypes('thieves\' auction', []).
card_colors('thieves\' auction', ['R']).
card_text('thieves\' auction', 'Exile all nontoken permanents. Starting with you, each player chooses one of the exiled cards and puts it onto the battlefield tapped under his or her control. Repeat this process until all cards exiled this way have been chosen.').
card_mana_cost('thieves\' auction', ['4', 'R', 'R', 'R']).
card_cmc('thieves\' auction', 7).
card_layout('thieves\' auction', 'normal').

% Found in: MOR
card_name('thieves\' fortune', 'Thieves\' Fortune').
card_type('thieves\' fortune', 'Tribal Instant — Rogue').
card_types('thieves\' fortune', ['Tribal', 'Instant']).
card_subtypes('thieves\' fortune', ['Rogue']).
card_colors('thieves\' fortune', ['U']).
card_text('thieves\' fortune', 'Prowl {U} (You may cast this for its prowl cost if you dealt combat damage to a player this turn with a Rogue.)\nLook at the top four cards of your library. Put one of them into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('thieves\' fortune', ['2', 'U']).
card_cmc('thieves\' fortune', 3).
card_layout('thieves\' fortune', 'normal').

% Found in: 10E, 7ED, 8ED, 9ED, DPA, UDS
card_name('thieving magpie', 'Thieving Magpie').
card_type('thieving magpie', 'Creature — Bird').
card_types('thieving magpie', ['Creature']).
card_subtypes('thieving magpie', ['Bird']).
card_colors('thieving magpie', ['U']).
card_text('thieving magpie', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever Thieving Magpie deals damage to an opponent, draw a card.').
card_mana_cost('thieving magpie', ['2', 'U', 'U']).
card_cmc('thieving magpie', 4).
card_layout('thieving magpie', 'normal').
card_power('thieving magpie', 1).
card_toughness('thieving magpie', 3).

% Found in: LRW, MMA
card_name('thieving sprite', 'Thieving Sprite').
card_type('thieving sprite', 'Creature — Faerie Rogue').
card_types('thieving sprite', ['Creature']).
card_subtypes('thieving sprite', ['Faerie', 'Rogue']).
card_colors('thieving sprite', ['B']).
card_text('thieving sprite', 'Flying\nWhen Thieving Sprite enters the battlefield, target player reveals X cards from his or her hand, where X is the number of Faeries you control. You choose one of those cards. That player discards that card.').
card_mana_cost('thieving sprite', ['2', 'B']).
card_cmc('thieving sprite', 3).
card_layout('thieving sprite', 'normal').
card_power('thieving sprite', 1).
card_toughness('thieving sprite', 1).

% Found in: ME4, POR
card_name('thing from the deep', 'Thing from the Deep').
card_type('thing from the deep', 'Creature — Leviathan').
card_types('thing from the deep', ['Creature']).
card_subtypes('thing from the deep', ['Leviathan']).
card_colors('thing from the deep', ['U']).
card_text('thing from the deep', 'Whenever Thing from the Deep attacks, sacrifice it unless you sacrifice an Island.').
card_mana_cost('thing from the deep', ['6', 'U', 'U', 'U']).
card_cmc('thing from the deep', 9).
card_layout('thing from the deep', 'normal').
card_power('thing from the deep', 9).
card_toughness('thing from the deep', 9).

% Found in: ODY
card_name('think tank', 'Think Tank').
card_type('think tank', 'Enchantment').
card_types('think tank', ['Enchantment']).
card_subtypes('think tank', []).
card_colors('think tank', ['U']).
card_text('think tank', 'At the beginning of your upkeep, look at the top card of your library. You may put that card into your graveyard.').
card_mana_cost('think tank', ['2', 'U']).
card_cmc('think tank', 3).
card_layout('think tank', 'normal').

% Found in: ISD, TSP
card_name('think twice', 'Think Twice').
card_type('think twice', 'Instant').
card_types('think twice', ['Instant']).
card_subtypes('think twice', []).
card_colors('think twice', ['U']).
card_text('think twice', 'Draw a card.\nFlashback {2}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('think twice', ['1', 'U']).
card_cmc('think twice', 2).
card_layout('think twice', 'normal').

% Found in: MIR
card_name('thirst', 'Thirst').
card_type('thirst', 'Enchantment — Aura').
card_types('thirst', ['Enchantment']).
card_subtypes('thirst', ['Aura']).
card_colors('thirst', ['U']).
card_text('thirst', 'Enchant creature\nWhen Thirst enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.\nAt the beginning of your upkeep, sacrifice Thirst unless you pay {U}.').
card_mana_cost('thirst', ['2', 'U']).
card_cmc('thirst', 3).
card_layout('thirst', 'normal').

% Found in: DDF, HOP, MMA, MRD, pFNM
card_name('thirst for knowledge', 'Thirst for Knowledge').
card_type('thirst for knowledge', 'Instant').
card_types('thirst for knowledge', ['Instant']).
card_subtypes('thirst for knowledge', []).
card_colors('thirst for knowledge', ['U']).
card_text('thirst for knowledge', 'Draw three cards. Then discard two cards unless you discard an artifact card.').
card_mana_cost('thirst for knowledge', ['2', 'U']).
card_cmc('thirst for knowledge', 3).
card_layout('thirst for knowledge', 'normal').

% Found in: SHM
card_name('thistledown duo', 'Thistledown Duo').
card_type('thistledown duo', 'Creature — Kithkin Soldier Wizard').
card_types('thistledown duo', ['Creature']).
card_subtypes('thistledown duo', ['Kithkin', 'Soldier', 'Wizard']).
card_colors('thistledown duo', ['W', 'U']).
card_text('thistledown duo', 'Whenever you cast a white spell, Thistledown Duo gets +1/+1 until end of turn.\nWhenever you cast a blue spell, Thistledown Duo gains flying until end of turn.').
card_mana_cost('thistledown duo', ['2', 'W/U']).
card_cmc('thistledown duo', 3).
card_layout('thistledown duo', 'normal').
card_power('thistledown duo', 2).
card_toughness('thistledown duo', 2).

% Found in: SHM
card_name('thistledown liege', 'Thistledown Liege').
card_type('thistledown liege', 'Creature — Kithkin Knight').
card_types('thistledown liege', ['Creature']).
card_subtypes('thistledown liege', ['Kithkin', 'Knight']).
card_colors('thistledown liege', ['W', 'U']).
card_text('thistledown liege', 'Flash\nOther white creatures you control get +1/+1.\nOther blue creatures you control get +1/+1.').
card_mana_cost('thistledown liege', ['1', 'W/U', 'W/U', 'W/U']).
card_cmc('thistledown liege', 4).
card_layout('thistledown liege', 'normal').
card_power('thistledown liege', 1).
card_toughness('thistledown liege', 3).

% Found in: MBS, pLPA
card_name('thopter assembly', 'Thopter Assembly').
card_type('thopter assembly', 'Artifact Creature — Thopter').
card_types('thopter assembly', ['Artifact', 'Creature']).
card_subtypes('thopter assembly', ['Thopter']).
card_colors('thopter assembly', []).
card_text('thopter assembly', 'Flying\nAt the beginning of your upkeep, if you control no Thopters other than Thopter Assembly, return Thopter Assembly to its owner\'s hand and put five 1/1 colorless Thopter artifact creature tokens with flying onto the battlefield.').
card_mana_cost('thopter assembly', ['6']).
card_cmc('thopter assembly', 6).
card_layout('thopter assembly', 'normal').
card_power('thopter assembly', 5).
card_toughness('thopter assembly', 5).

% Found in: ORI
card_name('thopter engineer', 'Thopter Engineer').
card_type('thopter engineer', 'Creature — Human Artificer').
card_types('thopter engineer', ['Creature']).
card_subtypes('thopter engineer', ['Human', 'Artificer']).
card_colors('thopter engineer', ['R']).
card_text('thopter engineer', 'When Thopter Engineer enters the battlefield, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield.\nArtifact creatures you control have haste. (They can attack and {T} as soon as they come under your control.)').
card_mana_cost('thopter engineer', ['2', 'R']).
card_cmc('thopter engineer', 3).
card_layout('thopter engineer', 'normal').
card_power('thopter engineer', 1).
card_toughness('thopter engineer', 3).

% Found in: ARB, C13
card_name('thopter foundry', 'Thopter Foundry').
card_type('thopter foundry', 'Artifact').
card_types('thopter foundry', ['Artifact']).
card_subtypes('thopter foundry', []).
card_colors('thopter foundry', ['W', 'U', 'B']).
card_text('thopter foundry', '{1}, Sacrifice a nontoken artifact: Put a 1/1 blue Thopter artifact creature token with flying onto the battlefield. You gain 1 life.').
card_mana_cost('thopter foundry', ['W/B', 'U']).
card_cmc('thopter foundry', 2).
card_layout('thopter foundry', 'normal').

% Found in: ORI
card_name('thopter spy network', 'Thopter Spy Network').
card_type('thopter spy network', 'Enchantment').
card_types('thopter spy network', ['Enchantment']).
card_subtypes('thopter spy network', []).
card_colors('thopter spy network', ['U']).
card_text('thopter spy network', 'At the beginning of your upkeep, if you control an artifact, put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield.\nWhenever one or more artifact creatures you control deal combat damage to a player, draw a card.').
card_mana_cost('thopter spy network', ['2', 'U', 'U']).
card_cmc('thopter spy network', 4).
card_layout('thopter spy network', 'normal').

% Found in: EXO, TPR, VMA
card_name('thopter squadron', 'Thopter Squadron').
card_type('thopter squadron', 'Artifact Creature — Thopter').
card_types('thopter squadron', ['Artifact', 'Creature']).
card_subtypes('thopter squadron', ['Thopter']).
card_colors('thopter squadron', []).
card_text('thopter squadron', 'Flying\nThopter Squadron enters the battlefield with three +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Thopter Squadron: Put a 1/1 colorless Thopter artifact creature token with flying onto the battlefield. Activate this ability only any time you could cast a sorcery.\n{1}, Sacrifice another Thopter: Put a +1/+1 counter on Thopter Squadron. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('thopter squadron', ['5']).
card_cmc('thopter squadron', 5).
card_layout('thopter squadron', 'normal').
card_power('thopter squadron', 0).
card_toughness('thopter squadron', 0).

% Found in: 7ED, 8ED, S99, UDS
card_name('thorn elemental', 'Thorn Elemental').
card_type('thorn elemental', 'Creature — Elemental').
card_types('thorn elemental', ['Creature']).
card_subtypes('thorn elemental', ['Elemental']).
card_colors('thorn elemental', ['G']).
card_text('thorn elemental', 'You may have Thorn Elemental assign its combat damage as though it weren\'t blocked.').
card_mana_cost('thorn elemental', ['5', 'G', 'G']).
card_cmc('thorn elemental', 7).
card_layout('thorn elemental', 'normal').
card_power('thorn elemental', 7).
card_toughness('thorn elemental', 7).

% Found in: LRW
card_name('thorn of amethyst', 'Thorn of Amethyst').
card_type('thorn of amethyst', 'Artifact').
card_types('thorn of amethyst', ['Artifact']).
card_subtypes('thorn of amethyst', []).
card_colors('thorn of amethyst', []).
card_text('thorn of amethyst', 'Noncreature spells cost {1} more to cast.').
card_mana_cost('thorn of amethyst', ['2']).
card_cmc('thorn of amethyst', 2).
card_layout('thorn of amethyst', 'normal').

% Found in: FEM, MED
card_name('thorn thallid', 'Thorn Thallid').
card_type('thorn thallid', 'Creature — Fungus').
card_types('thorn thallid', ['Creature']).
card_subtypes('thorn thallid', ['Fungus']).
card_colors('thorn thallid', ['G']).
card_text('thorn thallid', 'At the beginning of your upkeep, put a spore counter on Thorn Thallid.\nRemove three spore counters from Thorn Thallid: Thorn Thallid deals 1 damage to target creature or player.').
card_mana_cost('thorn thallid', ['1', 'G', 'G']).
card_cmc('thorn thallid', 3).
card_layout('thorn thallid', 'normal').
card_power('thorn thallid', 2).
card_toughness('thorn thallid', 2).

% Found in: ALA, PC2
card_name('thorn-thrash viashino', 'Thorn-Thrash Viashino').
card_type('thorn-thrash viashino', 'Creature — Viashino Warrior').
card_types('thorn-thrash viashino', ['Creature']).
card_subtypes('thorn-thrash viashino', ['Viashino', 'Warrior']).
card_colors('thorn-thrash viashino', ['R']).
card_text('thorn-thrash viashino', 'Devour 2 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with twice that many +1/+1 counters on it.)\n{G}: Thorn-Thrash Viashino gains trample until end of turn.').
card_mana_cost('thorn-thrash viashino', ['3', 'R']).
card_cmc('thorn-thrash viashino', 4).
card_layout('thorn-thrash viashino', 'normal').
card_power('thorn-thrash viashino', 2).
card_toughness('thorn-thrash viashino', 2).

% Found in: MOR
card_name('thornbite staff', 'Thornbite Staff').
card_type('thornbite staff', 'Tribal Artifact — Shaman Equipment').
card_types('thornbite staff', ['Tribal', 'Artifact']).
card_subtypes('thornbite staff', ['Shaman', 'Equipment']).
card_colors('thornbite staff', []).
card_text('thornbite staff', 'Equipped creature has \"{2}, {T}: This creature deals 1 damage to target creature or player\" and \"Whenever a creature dies, untap this creature.\"\nWhenever a Shaman creature enters the battlefield, you may attach Thornbite Staff to it.\nEquip {4}').
card_mana_cost('thornbite staff', ['2']).
card_cmc('thornbite staff', 2).
card_layout('thornbite staff', 'normal').

% Found in: ORI
card_name('thornbow archer', 'Thornbow Archer').
card_type('thornbow archer', 'Creature — Elf Archer').
card_types('thornbow archer', ['Creature']).
card_subtypes('thornbow archer', ['Elf', 'Archer']).
card_colors('thornbow archer', ['B']).
card_text('thornbow archer', 'Whenever Thornbow Archer attacks, each opponent who doesn\'t control an Elf loses 1 life.').
card_mana_cost('thornbow archer', ['B']).
card_cmc('thornbow archer', 1).
card_layout('thornbow archer', 'normal').
card_power('thornbow archer', 1).
card_toughness('thornbow archer', 2).

% Found in: M14
card_name('thorncaster sliver', 'Thorncaster Sliver').
card_type('thorncaster sliver', 'Creature — Sliver').
card_types('thorncaster sliver', ['Creature']).
card_subtypes('thorncaster sliver', ['Sliver']).
card_colors('thorncaster sliver', ['R']).
card_text('thorncaster sliver', 'Sliver creatures you control have \"Whenever this creature attacks, it deals 1 damage to target creature or player.\"').
card_mana_cost('thorncaster sliver', ['4', 'R']).
card_cmc('thorncaster sliver', 5).
card_layout('thorncaster sliver', 'normal').
card_power('thorncaster sliver', 2).
card_toughness('thorncaster sliver', 2).

% Found in: CON
card_name('thornling', 'Thornling').
card_type('thornling', 'Creature — Elemental Shapeshifter').
card_types('thornling', ['Creature']).
card_subtypes('thornling', ['Elemental', 'Shapeshifter']).
card_colors('thornling', ['G']).
card_text('thornling', '{G}: Thornling gains haste until end of turn.\n{G}: Thornling gains trample until end of turn.\n{G}: Thornling gains indestructible until end of turn.\n{1}: Thornling gets +1/-1 until end of turn.\n{1}: Thornling gets -1/+1 until end of turn.').
card_mana_cost('thornling', ['3', 'G', 'G']).
card_cmc('thornling', 5).
card_layout('thornling', 'normal').
card_power('thornling', 4).
card_toughness('thornling', 4).

% Found in: DDE, INV
card_name('thornscape apprentice', 'Thornscape Apprentice').
card_type('thornscape apprentice', 'Creature — Human Wizard').
card_types('thornscape apprentice', ['Creature']).
card_subtypes('thornscape apprentice', ['Human', 'Wizard']).
card_colors('thornscape apprentice', ['G']).
card_text('thornscape apprentice', '{R}, {T}: Target creature gains first strike until end of turn.\n{W}, {T}: Tap target creature.').
card_mana_cost('thornscape apprentice', ['G']).
card_cmc('thornscape apprentice', 1).
card_layout('thornscape apprentice', 'normal').
card_power('thornscape apprentice', 1).
card_toughness('thornscape apprentice', 1).

% Found in: DDE, PLS, TSB
card_name('thornscape battlemage', 'Thornscape Battlemage').
card_type('thornscape battlemage', 'Creature — Elf Wizard').
card_types('thornscape battlemage', ['Creature']).
card_subtypes('thornscape battlemage', ['Elf', 'Wizard']).
card_colors('thornscape battlemage', ['G']).
card_text('thornscape battlemage', 'Kicker {R} and/or {W} (You may pay an additional {R} and/or {W} as you cast this spell.)\nWhen Thornscape Battlemage enters the battlefield, if it was kicked with its {R} kicker, it deals 2 damage to target creature or player.\nWhen Thornscape Battlemage enters the battlefield, if it was kicked with its {W} kicker, destroy target artifact.').
card_mana_cost('thornscape battlemage', ['2', 'G']).
card_cmc('thornscape battlemage', 3).
card_layout('thornscape battlemage', 'normal').
card_power('thornscape battlemage', 2).
card_toughness('thornscape battlemage', 2).

% Found in: PLS
card_name('thornscape familiar', 'Thornscape Familiar').
card_type('thornscape familiar', 'Creature — Insect').
card_types('thornscape familiar', ['Creature']).
card_subtypes('thornscape familiar', ['Insect']).
card_colors('thornscape familiar', ['G']).
card_text('thornscape familiar', 'Red spells and white spells you cast cost {1} less to cast.').
card_mana_cost('thornscape familiar', ['1', 'G']).
card_cmc('thornscape familiar', 2).
card_layout('thornscape familiar', 'normal').
card_power('thornscape familiar', 2).
card_toughness('thornscape familiar', 1).

% Found in: INV
card_name('thornscape master', 'Thornscape Master').
card_type('thornscape master', 'Creature — Human Wizard').
card_types('thornscape master', ['Creature']).
card_subtypes('thornscape master', ['Human', 'Wizard']).
card_colors('thornscape master', ['G']).
card_text('thornscape master', '{R}{R}, {T}: Thornscape Master deals 2 damage to target creature.\n{W}{W}, {T}: Target creature gains protection from the color of your choice until end of turn.').
card_mana_cost('thornscape master', ['2', 'G', 'G']).
card_cmc('thornscape master', 4).
card_layout('thornscape master', 'normal').
card_power('thornscape master', 2).
card_toughness('thornscape master', 2).

% Found in: LRW
card_name('thorntooth witch', 'Thorntooth Witch').
card_type('thorntooth witch', 'Creature — Treefolk Shaman').
card_types('thorntooth witch', ['Creature']).
card_subtypes('thorntooth witch', ['Treefolk', 'Shaman']).
card_colors('thorntooth witch', ['B']).
card_text('thorntooth witch', 'Whenever you cast a Treefolk spell, you may have target creature get +3/-3 until end of turn.').
card_mana_cost('thorntooth witch', ['5', 'B']).
card_cmc('thorntooth witch', 6).
card_layout('thorntooth witch', 'normal').
card_power('thorntooth witch', 3).
card_toughness('thorntooth witch', 4).

% Found in: SHM
card_name('thornwatch scarecrow', 'Thornwatch Scarecrow').
card_type('thornwatch scarecrow', 'Artifact Creature — Scarecrow').
card_types('thornwatch scarecrow', ['Artifact', 'Creature']).
card_subtypes('thornwatch scarecrow', ['Scarecrow']).
card_colors('thornwatch scarecrow', []).
card_text('thornwatch scarecrow', 'Thornwatch Scarecrow has wither as long as you control a green creature. (It deals damage to creatures in the form of -1/-1 counters.)\nThornwatch Scarecrow has vigilance as long as you control a white creature.').
card_mana_cost('thornwatch scarecrow', ['6']).
card_cmc('thornwatch scarecrow', 6).
card_layout('thornwatch scarecrow', 'normal').
card_power('thornwatch scarecrow', 4).
card_toughness('thornwatch scarecrow', 4).

% Found in: C14, FUT
card_name('thornweald archer', 'Thornweald Archer').
card_type('thornweald archer', 'Creature — Elf Archer').
card_types('thornweald archer', ['Creature']).
card_subtypes('thornweald archer', ['Elf', 'Archer']).
card_colors('thornweald archer', ['G']).
card_text('thornweald archer', 'Reach (This creature can block creatures with flying.)\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('thornweald archer', ['1', 'G']).
card_cmc('thornweald archer', 2).
card_layout('thornweald archer', 'normal').
card_power('thornweald archer', 2).
card_toughness('thornweald archer', 1).

% Found in: C13, ULG
card_name('thornwind faeries', 'Thornwind Faeries').
card_type('thornwind faeries', 'Creature — Faerie').
card_types('thornwind faeries', ['Creature']).
card_subtypes('thornwind faeries', ['Faerie']).
card_colors('thornwind faeries', ['U']).
card_text('thornwind faeries', 'Flying\n{T}: Thornwind Faeries deals 1 damage to target creature or player.').
card_mana_cost('thornwind faeries', ['1', 'U', 'U']).
card_cmc('thornwind faeries', 3).
card_layout('thornwind faeries', 'normal').
card_power('thornwind faeries', 1).
card_toughness('thornwind faeries', 1).

% Found in: FRF, KTK
card_name('thornwood falls', 'Thornwood Falls').
card_type('thornwood falls', 'Land').
card_types('thornwood falls', ['Land']).
card_subtypes('thornwood falls', []).
card_colors('thornwood falls', []).
card_text('thornwood falls', 'Thornwood Falls enters the battlefield tapped.\nWhen Thornwood Falls enters the battlefield, you gain 1 life.\n{T}: Add {G} or {U} to your mana pool.').
card_layout('thornwood falls', 'normal').

% Found in: 5DN, 9ED
card_name('thought courier', 'Thought Courier').
card_type('thought courier', 'Creature — Human Wizard').
card_types('thought courier', ['Creature']).
card_subtypes('thought courier', ['Human', 'Wizard']).
card_colors('thought courier', ['U']).
card_text('thought courier', '{T}: Draw a card, then discard a card.').
card_mana_cost('thought courier', ['1', 'U']).
card_cmc('thought courier', 2).
card_layout('thought courier', 'normal').
card_power('thought courier', 1).
card_toughness('thought courier', 1).

% Found in: ODY
card_name('thought devourer', 'Thought Devourer').
card_type('thought devourer', 'Creature — Beast').
card_types('thought devourer', ['Creature']).
card_subtypes('thought devourer', ['Beast']).
card_colors('thought devourer', ['U']).
card_text('thought devourer', 'Flying\nYour maximum hand size is reduced by four.').
card_mana_cost('thought devourer', ['2', 'U', 'U']).
card_cmc('thought devourer', 4).
card_layout('thought devourer', 'normal').
card_power('thought devourer', 4).
card_toughness('thought devourer', 4).

% Found in: DST
card_name('thought dissector', 'Thought Dissector').
card_type('thought dissector', 'Artifact').
card_types('thought dissector', ['Artifact']).
card_subtypes('thought dissector', []).
card_colors('thought dissector', []).
card_text('thought dissector', '{X}, {T}: Target opponent reveals cards from the top of his or her library until an artifact card or X cards are revealed, whichever comes first. If an artifact card is revealed this way, put it onto the battlefield under your control and sacrifice Thought Dissector. Put the rest of the revealed cards into that player\'s graveyard.').
card_mana_cost('thought dissector', ['4']).
card_cmc('thought dissector', 4).
card_layout('thought dissector', 'normal').

% Found in: ODY
card_name('thought eater', 'Thought Eater').
card_type('thought eater', 'Creature — Beast').
card_types('thought eater', ['Creature']).
card_subtypes('thought eater', ['Beast']).
card_colors('thought eater', ['U']).
card_text('thought eater', 'Flying\nYour maximum hand size is reduced by three.').
card_mana_cost('thought eater', ['1', 'U']).
card_cmc('thought eater', 2).
card_layout('thought eater', 'normal').
card_power('thought eater', 2).
card_toughness('thought eater', 2).

% Found in: ROE
card_name('thought gorger', 'Thought Gorger').
card_type('thought gorger', 'Creature — Horror').
card_types('thought gorger', ['Creature']).
card_subtypes('thought gorger', ['Horror']).
card_colors('thought gorger', ['B']).
card_text('thought gorger', 'Trample\nWhen Thought Gorger enters the battlefield, put a +1/+1 counter on it for each card in your hand. If you do, discard your hand.\nWhen Thought Gorger leaves the battlefield, draw a card for each +1/+1 counter on it.').
card_mana_cost('thought gorger', ['2', 'B', 'B']).
card_cmc('thought gorger', 4).
card_layout('thought gorger', 'normal').
card_power('thought gorger', 2).
card_toughness('thought gorger', 2).

% Found in: ARB
card_name('thought hemorrhage', 'Thought Hemorrhage').
card_type('thought hemorrhage', 'Sorcery').
card_types('thought hemorrhage', ['Sorcery']).
card_subtypes('thought hemorrhage', []).
card_colors('thought hemorrhage', ['B', 'R']).
card_text('thought hemorrhage', 'Name a nonland card. Target player reveals his or her hand. Thought Hemorrhage deals 3 damage to that player for each card with that name revealed this way. Search that player\'s graveyard, hand, and library for all cards with that name and exile them. Then that player shuffles his or her library.').
card_mana_cost('thought hemorrhage', ['2', 'B', 'R']).
card_cmc('thought hemorrhage', 4).
card_layout('thought hemorrhage', 'normal').

% Found in: ALL, ME2
card_name('thought lash', 'Thought Lash').
card_type('thought lash', 'Enchantment').
card_types('thought lash', ['Enchantment']).
card_subtypes('thought lash', []).
card_colors('thought lash', ['U']).
card_text('thought lash', 'Cumulative upkeep—Exile the top card of your library. (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nWhen a player doesn\'t pay Thought Lash\'s cumulative upkeep, that player exiles all cards from his or her library.\nExile the top card of your library: Prevent the next 1 damage that would be dealt to you this turn.').
card_mana_cost('thought lash', ['2', 'U', 'U']).
card_cmc('thought lash', 4).
card_layout('thought lash', 'normal').
card_reserved('thought lash').

% Found in: ODY
card_name('thought nibbler', 'Thought Nibbler').
card_type('thought nibbler', 'Creature — Beast').
card_types('thought nibbler', ['Creature']).
card_subtypes('thought nibbler', ['Beast']).
card_colors('thought nibbler', ['U']).
card_text('thought nibbler', 'Flying\nYour maximum hand size is reduced by two.').
card_mana_cost('thought nibbler', ['U']).
card_cmc('thought nibbler', 1).
card_layout('thought nibbler', 'normal').
card_power('thought nibbler', 1).
card_toughness('thought nibbler', 1).

% Found in: MRD
card_name('thought prison', 'Thought Prison').
card_type('thought prison', 'Artifact').
card_types('thought prison', ['Artifact']).
card_subtypes('thought prison', []).
card_colors('thought prison', []).
card_text('thought prison', 'Imprint — When Thought Prison enters the battlefield, you may have target player reveal his or her hand. If you do, choose a nonland card from it and exile that card.\nWhenever a player casts a spell that shares a color or converted mana cost with the exiled card, Thought Prison deals 2 damage to that player.').
card_mana_cost('thought prison', ['5']).
card_cmc('thought prison', 5).
card_layout('thought prison', 'normal').

% Found in: SHM
card_name('thought reflection', 'Thought Reflection').
card_type('thought reflection', 'Enchantment').
card_types('thought reflection', ['Enchantment']).
card_subtypes('thought reflection', []).
card_colors('thought reflection', ['U']).
card_text('thought reflection', 'If you would draw a card, draw two cards instead.').
card_mana_cost('thought reflection', ['4', 'U', 'U', 'U']).
card_cmc('thought reflection', 7).
card_layout('thought reflection', 'normal').

% Found in: DDM, DKA
card_name('thought scour', 'Thought Scour').
card_type('thought scour', 'Instant').
card_types('thought scour', ['Instant']).
card_subtypes('thought scour', []).
card_colors('thought scour', ['U']).
card_text('thought scour', 'Target player puts the top two cards of his or her library into his or her graveyard.\nDraw a card.').
card_mana_cost('thought scour', ['U']).
card_cmc('thought scour', 1).
card_layout('thought scour', 'normal').

% Found in: CHK
card_name('thoughtbind', 'Thoughtbind').
card_type('thoughtbind', 'Instant').
card_types('thoughtbind', ['Instant']).
card_subtypes('thoughtbind', []).
card_colors('thoughtbind', ['U']).
card_text('thoughtbind', 'Counter target spell with converted mana cost 4 or less.').
card_mana_cost('thoughtbind', ['2', 'U']).
card_cmc('thoughtbind', 3).
card_layout('thoughtbind', 'normal').

% Found in: ONS
card_name('thoughtbound primoc', 'Thoughtbound Primoc').
card_type('thoughtbound primoc', 'Creature — Bird Beast').
card_types('thoughtbound primoc', ['Creature']).
card_subtypes('thoughtbound primoc', ['Bird', 'Beast']).
card_colors('thoughtbound primoc', ['R']).
card_text('thoughtbound primoc', 'Flying\nAt the beginning of your upkeep, if a player controls more Wizards than each other player, the player who controls the most Wizards gains control of Thoughtbound Primoc.').
card_mana_cost('thoughtbound primoc', ['2', 'R']).
card_cmc('thoughtbound primoc', 3).
card_layout('thoughtbound primoc', 'normal').
card_power('thoughtbound primoc', 2).
card_toughness('thoughtbound primoc', 3).

% Found in: DDF, MM2, MRD
card_name('thoughtcast', 'Thoughtcast').
card_type('thoughtcast', 'Sorcery').
card_types('thoughtcast', ['Sorcery']).
card_subtypes('thoughtcast', []).
card_colors('thoughtcast', ['U']).
card_text('thoughtcast', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nDraw two cards.').
card_mana_cost('thoughtcast', ['4', 'U']).
card_cmc('thoughtcast', 5).
card_layout('thoughtcast', 'normal').

% Found in: ALA
card_name('thoughtcutter agent', 'Thoughtcutter Agent').
card_type('thoughtcutter agent', 'Artifact Creature — Human Rogue').
card_types('thoughtcutter agent', ['Artifact', 'Creature']).
card_subtypes('thoughtcutter agent', ['Human', 'Rogue']).
card_colors('thoughtcutter agent', ['U', 'B']).
card_text('thoughtcutter agent', '{U}{B}, {T}: Target player loses 1 life and reveals his or her hand.').
card_mana_cost('thoughtcutter agent', ['U', 'B']).
card_cmc('thoughtcutter agent', 2).
card_layout('thoughtcutter agent', 'normal').
card_power('thoughtcutter agent', 1).
card_toughness('thoughtcutter agent', 1).

% Found in: RTR
card_name('thoughtflare', 'Thoughtflare').
card_type('thoughtflare', 'Instant').
card_types('thoughtflare', ['Instant']).
card_subtypes('thoughtflare', []).
card_colors('thoughtflare', ['U', 'R']).
card_text('thoughtflare', 'Draw four cards, then discard two cards.').
card_mana_cost('thoughtflare', ['3', 'U', 'R']).
card_cmc('thoughtflare', 5).
card_layout('thoughtflare', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('thoughtlace', 'Thoughtlace').
card_type('thoughtlace', 'Instant').
card_types('thoughtlace', ['Instant']).
card_subtypes('thoughtlace', []).
card_colors('thoughtlace', ['U']).
card_text('thoughtlace', 'Target spell or permanent becomes blue. (Mana symbols on that permanent remain unchanged.)').
card_mana_cost('thoughtlace', ['U']).
card_cmc('thoughtlace', 1).
card_layout('thoughtlace', 'normal').

% Found in: 7ED, ICE
card_name('thoughtleech', 'Thoughtleech').
card_type('thoughtleech', 'Enchantment').
card_types('thoughtleech', ['Enchantment']).
card_subtypes('thoughtleech', []).
card_colors('thoughtleech', ['G']).
card_text('thoughtleech', 'Whenever an Island an opponent controls becomes tapped, you may gain 1 life.').
card_mana_cost('thoughtleech', ['G', 'G']).
card_cmc('thoughtleech', 2).
card_layout('thoughtleech', 'normal').

% Found in: RAV
card_name('thoughtpicker witch', 'Thoughtpicker Witch').
card_type('thoughtpicker witch', 'Creature — Human Wizard').
card_types('thoughtpicker witch', ['Creature']).
card_subtypes('thoughtpicker witch', ['Human', 'Wizard']).
card_colors('thoughtpicker witch', ['B']).
card_text('thoughtpicker witch', '{1}, Sacrifice a creature: Look at the top two cards of target opponent\'s library, then exile one of them.').
card_mana_cost('thoughtpicker witch', ['B']).
card_cmc('thoughtpicker witch', 1).
card_layout('thoughtpicker witch', 'normal').
card_power('thoughtpicker witch', 1).
card_toughness('thoughtpicker witch', 1).

% Found in: JOU
card_name('thoughtrender lamia', 'Thoughtrender Lamia').
card_type('thoughtrender lamia', 'Enchantment Creature — Lamia').
card_types('thoughtrender lamia', ['Enchantment', 'Creature']).
card_subtypes('thoughtrender lamia', ['Lamia']).
card_colors('thoughtrender lamia', ['B']).
card_text('thoughtrender lamia', 'Constellation — Whenever Thoughtrender Lamia or another enchantment enters the battlefield under your control, each opponent discards a card.').
card_mana_cost('thoughtrender lamia', ['4', 'B', 'B']).
card_cmc('thoughtrender lamia', 6).
card_layout('thoughtrender lamia', 'normal').
card_power('thoughtrender lamia', 5).
card_toughness('thoughtrender lamia', 3).

% Found in: SOK
card_name('thoughts of ruin', 'Thoughts of Ruin').
card_type('thoughts of ruin', 'Sorcery').
card_types('thoughts of ruin', ['Sorcery']).
card_subtypes('thoughts of ruin', []).
card_colors('thoughts of ruin', ['R']).
card_text('thoughts of ruin', 'Each player sacrifices a land for each card in your hand.').
card_mana_cost('thoughts of ruin', ['2', 'R', 'R']).
card_cmc('thoughts of ruin', 4).
card_layout('thoughts of ruin', 'normal').

% Found in: LRW, THS
card_name('thoughtseize', 'Thoughtseize').
card_type('thoughtseize', 'Sorcery').
card_types('thoughtseize', ['Sorcery']).
card_subtypes('thoughtseize', []).
card_colors('thoughtseize', ['B']).
card_text('thoughtseize', 'Target player reveals his or her hand. You choose a nonland card from it. That player discards that card. You lose 2 life.').
card_mana_cost('thoughtseize', ['B']).
card_cmc('thoughtseize', 1).
card_layout('thoughtseize', 'normal').

% Found in: SHM
card_name('thoughtweft gambit', 'Thoughtweft Gambit').
card_type('thoughtweft gambit', 'Instant').
card_types('thoughtweft gambit', ['Instant']).
card_subtypes('thoughtweft gambit', []).
card_colors('thoughtweft gambit', ['W', 'U']).
card_text('thoughtweft gambit', 'Tap all creatures your opponents control and untap all creatures you control.').
card_mana_cost('thoughtweft gambit', ['4', 'W/U', 'W/U']).
card_cmc('thoughtweft gambit', 6).
card_layout('thoughtweft gambit', 'normal').

% Found in: LRW
card_name('thoughtweft trio', 'Thoughtweft Trio').
card_type('thoughtweft trio', 'Creature — Kithkin Soldier').
card_types('thoughtweft trio', ['Creature']).
card_subtypes('thoughtweft trio', ['Kithkin', 'Soldier']).
card_colors('thoughtweft trio', ['W']).
card_text('thoughtweft trio', 'First strike, vigilance\nChampion a Kithkin (When this enters the battlefield, sacrifice it unless you exile another Kithkin you control. When this leaves the battlefield, that card returns to the battlefield.)\nThoughtweft Trio can block any number of creatures.').
card_mana_cost('thoughtweft trio', ['2', 'W', 'W']).
card_cmc('thoughtweft trio', 4).
card_layout('thoughtweft trio', 'normal').
card_power('thoughtweft trio', 5).
card_toughness('thoughtweft trio', 5).

% Found in: DDN, KTK, pPRE
card_name('thousand winds', 'Thousand Winds').
card_type('thousand winds', 'Creature — Elemental').
card_types('thousand winds', ['Creature']).
card_subtypes('thousand winds', ['Elemental']).
card_colors('thousand winds', ['U']).
card_text('thousand winds', 'Flying\nMorph {5}{U}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Thousand Winds is turned face up, return all other tapped creatures to their owners\' hands.').
card_mana_cost('thousand winds', ['4', 'U', 'U']).
card_cmc('thousand winds', 6).
card_layout('thousand winds', 'normal').
card_power('thousand winds', 5).
card_toughness('thousand winds', 6).

% Found in: CHK
card_name('thousand-legged kami', 'Thousand-legged Kami').
card_type('thousand-legged kami', 'Creature — Spirit').
card_types('thousand-legged kami', ['Creature']).
card_subtypes('thousand-legged kami', ['Spirit']).
card_colors('thousand-legged kami', ['G']).
card_text('thousand-legged kami', 'Soulshift 7 (When this creature dies, you may return target Spirit card with converted mana cost 7 or less from your graveyard to your hand.)').
card_mana_cost('thousand-legged kami', ['6', 'G', 'G']).
card_cmc('thousand-legged kami', 8).
card_layout('thousand-legged kami', 'normal').
card_power('thousand-legged kami', 6).
card_toughness('thousand-legged kami', 6).

% Found in: C13, LRW
card_name('thousand-year elixir', 'Thousand-Year Elixir').
card_type('thousand-year elixir', 'Artifact').
card_types('thousand-year elixir', ['Artifact']).
card_subtypes('thousand-year elixir', []).
card_colors('thousand-year elixir', []).
card_text('thousand-year elixir', 'You may activate abilities of creatures you control as though those creatures had haste.\n{1}, {T}: Untap target creature.').
card_mana_cost('thousand-year elixir', ['3']).
card_cmc('thousand-year elixir', 3).
card_layout('thousand-year elixir', 'normal').

% Found in: DKA
card_name('thraben doomsayer', 'Thraben Doomsayer').
card_type('thraben doomsayer', 'Creature — Human Cleric').
card_types('thraben doomsayer', ['Creature']).
card_subtypes('thraben doomsayer', ['Human', 'Cleric']).
card_colors('thraben doomsayer', ['W']).
card_text('thraben doomsayer', '{T}: Put a 1/1 white Human creature token onto the battlefield.\nFateful hour — As long as you have 5 or less life, other creatures you control get +2/+2.').
card_mana_cost('thraben doomsayer', ['1', 'W', 'W']).
card_cmc('thraben doomsayer', 3).
card_layout('thraben doomsayer', 'normal').
card_power('thraben doomsayer', 2).
card_toughness('thraben doomsayer', 2).

% Found in: DKA
card_name('thraben heretic', 'Thraben Heretic').
card_type('thraben heretic', 'Creature — Human Wizard').
card_types('thraben heretic', ['Creature']).
card_subtypes('thraben heretic', ['Human', 'Wizard']).
card_colors('thraben heretic', ['W']).
card_text('thraben heretic', '{T}: Exile target creature card from a graveyard.').
card_mana_cost('thraben heretic', ['1', 'W']).
card_cmc('thraben heretic', 2).
card_layout('thraben heretic', 'normal').
card_power('thraben heretic', 2).
card_toughness('thraben heretic', 2).

% Found in: ISD
card_name('thraben militia', 'Thraben Militia').
card_type('thraben militia', 'Creature — Human Soldier').
card_types('thraben militia', ['Creature']).
card_subtypes('thraben militia', ['Human', 'Soldier']).
card_colors('thraben militia', ['W']).
card_text('thraben militia', 'Trample').
card_layout('thraben militia', 'double-faced').
card_power('thraben militia', 5).
card_toughness('thraben militia', 4).

% Found in: ISD
card_name('thraben purebloods', 'Thraben Purebloods').
card_type('thraben purebloods', 'Creature — Hound').
card_types('thraben purebloods', ['Creature']).
card_subtypes('thraben purebloods', ['Hound']).
card_colors('thraben purebloods', ['W']).
card_text('thraben purebloods', '').
card_mana_cost('thraben purebloods', ['4', 'W']).
card_cmc('thraben purebloods', 5).
card_layout('thraben purebloods', 'normal').
card_power('thraben purebloods', 3).
card_toughness('thraben purebloods', 5).

% Found in: ISD
card_name('thraben sentry', 'Thraben Sentry').
card_type('thraben sentry', 'Creature — Human Soldier').
card_types('thraben sentry', ['Creature']).
card_subtypes('thraben sentry', ['Human', 'Soldier']).
card_colors('thraben sentry', ['W']).
card_text('thraben sentry', 'Vigilance\nWhenever another creature you control dies, you may transform Thraben Sentry.').
card_mana_cost('thraben sentry', ['3', 'W']).
card_cmc('thraben sentry', 4).
card_layout('thraben sentry', 'double-faced').
card_power('thraben sentry', 2).
card_toughness('thraben sentry', 2).
card_sides('thraben sentry', 'thraben militia').

% Found in: AVR, DDL
card_name('thraben valiant', 'Thraben Valiant').
card_type('thraben valiant', 'Creature — Human Soldier').
card_types('thraben valiant', ['Creature']).
card_subtypes('thraben valiant', ['Human', 'Soldier']).
card_colors('thraben valiant', ['W']).
card_text('thraben valiant', 'Vigilance').
card_mana_cost('thraben valiant', ['1', 'W']).
card_cmc('thraben valiant', 2).
card_layout('thraben valiant', 'normal').
card_power('thraben valiant', 2).
card_toughness('thraben valiant', 1).

% Found in: M13
card_name('thragtusk', 'Thragtusk').
card_type('thragtusk', 'Creature — Beast').
card_types('thragtusk', ['Creature']).
card_subtypes('thragtusk', ['Beast']).
card_colors('thragtusk', ['G']).
card_text('thragtusk', 'When Thragtusk enters the battlefield, you gain 5 life.\nWhen Thragtusk leaves the battlefield, put a 3/3 green Beast creature token onto the battlefield.').
card_mana_cost('thragtusk', ['4', 'G']).
card_cmc('thragtusk', 5).
card_layout('thragtusk', 'normal').
card_power('thragtusk', 5).
card_toughness('thragtusk', 3).

% Found in: ARC, C14, UDS, V13
card_name('thran dynamo', 'Thran Dynamo').
card_type('thran dynamo', 'Artifact').
card_types('thran dynamo', ['Artifact']).
card_subtypes('thran dynamo', []).
card_colors('thran dynamo', []).
card_text('thran dynamo', '{T}: Add {3} to your mana pool.').
card_mana_cost('thran dynamo', ['4']).
card_cmc('thran dynamo', 4).
card_layout('thran dynamo', 'normal').

% Found in: WTH
card_name('thran forge', 'Thran Forge').
card_type('thran forge', 'Artifact').
card_types('thran forge', ['Artifact']).
card_subtypes('thran forge', []).
card_colors('thran forge', []).
card_text('thran forge', '{2}: Until end of turn, target nonartifact creature gets +1/+0 and becomes an artifact in addition to its other types.').
card_mana_cost('thran forge', ['3']).
card_cmc('thran forge', 3).
card_layout('thran forge', 'normal').

% Found in: UDS
card_name('thran foundry', 'Thran Foundry').
card_type('thran foundry', 'Artifact').
card_types('thran foundry', ['Artifact']).
card_subtypes('thran foundry', []).
card_colors('thran foundry', []).
card_text('thran foundry', '{1}, {T}, Exile Thran Foundry: Target player shuffles his or her graveyard into his or her library.').
card_mana_cost('thran foundry', ['1']).
card_cmc('thran foundry', 1).
card_layout('thran foundry', 'normal').

% Found in: 9ED, M12, PC2, UDS
card_name('thran golem', 'Thran Golem').
card_type('thran golem', 'Artifact Creature — Golem').
card_types('thran golem', ['Artifact', 'Creature']).
card_subtypes('thran golem', ['Golem']).
card_colors('thran golem', []).
card_text('thran golem', 'As long as Thran Golem is enchanted, it gets +2/+2 and has flying, first strike, and trample.').
card_mana_cost('thran golem', ['5']).
card_cmc('thran golem', 5).
card_layout('thran golem', 'normal').
card_power('thran golem', 3).
card_toughness('thran golem', 3).

% Found in: ULG
card_name('thran lens', 'Thran Lens').
card_type('thran lens', 'Artifact').
card_types('thran lens', ['Artifact']).
card_subtypes('thran lens', []).
card_colors('thran lens', []).
card_text('thran lens', 'All permanents are colorless.').
card_mana_cost('thran lens', ['2']).
card_cmc('thran lens', 2).
card_layout('thran lens', 'normal').

% Found in: USG, pSUS
card_name('thran quarry', 'Thran Quarry').
card_type('thran quarry', 'Land').
card_types('thran quarry', ['Land']).
card_subtypes('thran quarry', []).
card_colors('thran quarry', []).
card_text('thran quarry', 'At the beginning of the end step, if you control no creatures, sacrifice Thran Quarry.\n{T}: Add one mana of any color to your mana pool.').
card_layout('thran quarry', 'normal').

% Found in: WTH
card_name('thran tome', 'Thran Tome').
card_type('thran tome', 'Artifact').
card_types('thran tome', ['Artifact']).
card_subtypes('thran tome', []).
card_colors('thran tome', []).
card_text('thran tome', '{5}, {T}: Reveal the top three cards of your library. Target opponent chooses one of those cards. Put that card into your graveyard, then draw two cards.').
card_mana_cost('thran tome', ['4']).
card_cmc('thran tome', 4).
card_layout('thran tome', 'normal').
card_reserved('thran tome').

% Found in: USG
card_name('thran turbine', 'Thran Turbine').
card_type('thran turbine', 'Artifact').
card_types('thran turbine', ['Artifact']).
card_subtypes('thran turbine', []).
card_colors('thran turbine', []).
card_text('thran turbine', 'At the beginning of your upkeep, you may add {1} or {2} to your mana pool. You can\'t spend this mana to cast spells.').
card_mana_cost('thran turbine', ['1']).
card_cmc('thran turbine', 1).
card_layout('thran turbine', 'normal').

% Found in: ULG
card_name('thran war machine', 'Thran War Machine').
card_type('thran war machine', 'Artifact Creature — Construct').
card_types('thran war machine', ['Artifact', 'Creature']).
card_subtypes('thran war machine', ['Construct']).
card_colors('thran war machine', []).
card_text('thran war machine', 'Echo {4} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nThran War Machine attacks each turn if able.').
card_mana_cost('thran war machine', ['4']).
card_cmc('thran war machine', 4).
card_layout('thran war machine', 'normal').
card_power('thran war machine', 4).
card_toughness('thran war machine', 5).

% Found in: ULG
card_name('thran weaponry', 'Thran Weaponry').
card_type('thran weaponry', 'Artifact').
card_types('thran weaponry', ['Artifact']).
card_subtypes('thran weaponry', []).
card_colors('thran weaponry', []).
card_text('thran weaponry', 'Echo {4} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nYou may choose not to untap Thran Weaponry during your untap step.\n{2}, {T}: All creatures get +2/+2 for as long as Thran Weaponry remains tapped.').
card_mana_cost('thran weaponry', ['4']).
card_cmc('thran weaponry', 4).
card_layout('thran weaponry', 'normal').

% Found in: DGM
card_name('thrashing mossdog', 'Thrashing Mossdog').
card_type('thrashing mossdog', 'Creature — Plant Hound').
card_types('thrashing mossdog', ['Creature']).
card_subtypes('thrashing mossdog', ['Plant', 'Hound']).
card_colors('thrashing mossdog', ['G']).
card_text('thrashing mossdog', 'Reach (This creature can block creatures with flying.)\nScavenge {4}{G}{G} ({4}{G}{G}, Exile this card from your graveyard: Put a number of +1/+1 counters equal to this card\'s power on target creature. Scavenge only as a sorcery.)').
card_mana_cost('thrashing mossdog', ['3', 'G']).
card_cmc('thrashing mossdog', 4).
card_layout('thrashing mossdog', 'normal').
card_power('thrashing mossdog', 3).
card_toughness('thrashing mossdog', 3).

% Found in: ONS
card_name('thrashing mudspawn', 'Thrashing Mudspawn').
card_type('thrashing mudspawn', 'Creature — Beast').
card_types('thrashing mudspawn', ['Creature']).
card_subtypes('thrashing mudspawn', ['Beast']).
card_colors('thrashing mudspawn', ['B']).
card_text('thrashing mudspawn', 'Whenever Thrashing Mudspawn is dealt damage, you lose that much life.\nMorph {1}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('thrashing mudspawn', ['3', 'B', 'B']).
card_cmc('thrashing mudspawn', 5).
card_layout('thrashing mudspawn', 'normal').
card_power('thrashing mudspawn', 4).
card_toughness('thrashing mudspawn', 4).

% Found in: MMQ
card_name('thrashing wumpus', 'Thrashing Wumpus').
card_type('thrashing wumpus', 'Creature — Beast').
card_types('thrashing wumpus', ['Creature']).
card_subtypes('thrashing wumpus', ['Beast']).
card_colors('thrashing wumpus', ['B']).
card_text('thrashing wumpus', '{B}: Thrashing Wumpus deals 1 damage to each creature and each player.').
card_mana_cost('thrashing wumpus', ['3', 'B', 'B']).
card_cmc('thrashing wumpus', 5).
card_layout('thrashing wumpus', 'normal').
card_power('thrashing wumpus', 3).
card_toughness('thrashing wumpus', 3).

% Found in: ARB, C13
card_name('thraximundar', 'Thraximundar').
card_type('thraximundar', 'Legendary Creature — Zombie Assassin').
card_types('thraximundar', ['Creature']).
card_subtypes('thraximundar', ['Zombie', 'Assassin']).
card_supertypes('thraximundar', ['Legendary']).
card_colors('thraximundar', ['U', 'B', 'R']).
card_text('thraximundar', 'Haste\nWhenever Thraximundar attacks, defending player sacrifices a creature.\nWhenever a player sacrifices a creature, you may put a +1/+1 counter on Thraximundar.').
card_mana_cost('thraximundar', ['4', 'U', 'B', 'R']).
card_cmc('thraximundar', 7).
card_layout('thraximundar', 'normal').
card_power('thraximundar', 6).
card_toughness('thraximundar', 6).

% Found in: BOK
card_name('threads of disloyalty', 'Threads of Disloyalty').
card_type('threads of disloyalty', 'Enchantment — Aura').
card_types('threads of disloyalty', ['Enchantment']).
card_subtypes('threads of disloyalty', ['Aura']).
card_colors('threads of disloyalty', ['U']).
card_text('threads of disloyalty', 'Enchant creature with converted mana cost 2 or less\nYou control enchanted creature.').
card_mana_cost('threads of disloyalty', ['1', 'U', 'U']).
card_cmc('threads of disloyalty', 3).
card_layout('threads of disloyalty', 'normal').

% Found in: 10E, 9ED, ONS
card_name('threaten', 'Threaten').
card_type('threaten', 'Sorcery').
card_types('threaten', ['Sorcery']).
card_subtypes('threaten', []).
card_colors('threaten', ['R']).
card_text('threaten', 'Untap target creature and gain control of it until end of turn. That creature gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('threaten', ['2', 'R']).
card_cmc('threaten', 3).
card_layout('threaten', 'normal').

% Found in: PC2, RAV
card_name('three dreams', 'Three Dreams').
card_type('three dreams', 'Sorcery').
card_types('three dreams', ['Sorcery']).
card_subtypes('three dreams', []).
card_colors('three dreams', ['W']).
card_text('three dreams', 'Search your library for up to three Aura cards with different names, reveal them, and put them into your hand. Then shuffle your library.').
card_mana_cost('three dreams', ['4', 'W']).
card_cmc('three dreams', 5).
card_layout('three dreams', 'normal').

% Found in: BOK
card_name('three tragedies', 'Three Tragedies').
card_type('three tragedies', 'Sorcery — Arcane').
card_types('three tragedies', ['Sorcery']).
card_subtypes('three tragedies', ['Arcane']).
card_colors('three tragedies', ['B']).
card_text('three tragedies', 'Target player discards three cards.').
card_mana_cost('three tragedies', ['3', 'B', 'B']).
card_cmc('three tragedies', 5).
card_layout('three tragedies', 'normal').

% Found in: ME3, PTK
card_name('three visits', 'Three Visits').
card_type('three visits', 'Sorcery').
card_types('three visits', ['Sorcery']).
card_subtypes('three visits', []).
card_colors('three visits', ['G']).
card_text('three visits', 'Search your library for a Forest card and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('three visits', ['1', 'G']).
card_cmc('three visits', 2).
card_layout('three visits', 'normal').

% Found in: VIS
card_name('three wishes', 'Three Wishes').
card_type('three wishes', 'Instant').
card_types('three wishes', ['Instant']).
card_subtypes('three wishes', []).
card_colors('three wishes', ['U']).
card_text('three wishes', 'Exile the top three cards of your library face down. You may look at those cards for as long as they remain exiled. Until your next turn, you may play those cards. At the beginning of your next upkeep, put any of those cards you didn\'t play into your graveyard.').
card_mana_cost('three wishes', ['1', 'U', 'U']).
card_cmc('three wishes', 3).
card_layout('three wishes', 'normal').
card_reserved('three wishes').

% Found in: PCY
card_name('thresher beast', 'Thresher Beast').
card_type('thresher beast', 'Creature — Beast').
card_types('thresher beast', ['Creature']).
card_subtypes('thresher beast', ['Beast']).
card_colors('thresher beast', ['G']).
card_text('thresher beast', 'Whenever Thresher Beast becomes blocked, defending player sacrifices a land.').
card_mana_cost('thresher beast', ['3', 'G', 'G']).
card_cmc('thresher beast', 5).
card_layout('thresher beast', 'normal').
card_power('thresher beast', 4).
card_toughness('thresher beast', 4).

% Found in: TSP
card_name('thrill of the hunt', 'Thrill of the Hunt').
card_type('thrill of the hunt', 'Instant').
card_types('thrill of the hunt', ['Instant']).
card_subtypes('thrill of the hunt', []).
card_colors('thrill of the hunt', ['G']).
card_text('thrill of the hunt', 'Target creature gets +1/+2 until end of turn.\nFlashback {W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('thrill of the hunt', ['G']).
card_cmc('thrill of the hunt', 1).
card_layout('thrill of the hunt', 'normal').

% Found in: RTR
card_name('thrill-kill assassin', 'Thrill-Kill Assassin').
card_type('thrill-kill assassin', 'Creature — Human Assassin').
card_types('thrill-kill assassin', ['Creature']).
card_subtypes('thrill-kill assassin', ['Human', 'Assassin']).
card_colors('thrill-kill assassin', ['B']).
card_text('thrill-kill assassin', 'Deathtouch\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_mana_cost('thrill-kill assassin', ['1', 'B']).
card_cmc('thrill-kill assassin', 2).
card_layout('thrill-kill assassin', 'normal').
card_power('thrill-kill assassin', 1).
card_toughness('thrill-kill assassin', 2).

% Found in: JUD
card_name('thriss, nantuko primus', 'Thriss, Nantuko Primus').
card_type('thriss, nantuko primus', 'Legendary Creature — Insect Druid').
card_types('thriss, nantuko primus', ['Creature']).
card_subtypes('thriss, nantuko primus', ['Insect', 'Druid']).
card_supertypes('thriss, nantuko primus', ['Legendary']).
card_colors('thriss, nantuko primus', ['G']).
card_text('thriss, nantuko primus', '{G}, {T}: Target creature gets +5/+5 until end of turn.').
card_mana_cost('thriss, nantuko primus', ['5', 'G', 'G']).
card_cmc('thriss, nantuko primus', 7).
card_layout('thriss, nantuko primus', 'normal').
card_power('thriss, nantuko primus', 5).
card_toughness('thriss, nantuko primus', 5).

% Found in: DIS, MM2, PCY
card_name('thrive', 'Thrive').
card_type('thrive', 'Sorcery').
card_types('thrive', ['Sorcery']).
card_subtypes('thrive', []).
card_colors('thrive', ['G']).
card_text('thrive', 'Put a +1/+1 counter on each of X target creatures.').
card_mana_cost('thrive', ['X', 'G']).
card_cmc('thrive', 1).
card_layout('thrive', 'normal').

% Found in: BOK, PC2
card_name('throat slitter', 'Throat Slitter').
card_type('throat slitter', 'Creature — Rat Ninja').
card_types('throat slitter', ['Creature']).
card_subtypes('throat slitter', ['Rat', 'Ninja']).
card_colors('throat slitter', ['B']).
card_text('throat slitter', 'Ninjutsu {2}{B} ({2}{B}, Return an unblocked attacker you control to hand: Put this card onto the battlefield from your hand tapped and attacking.)\nWhenever Throat Slitter deals combat damage to a player, destroy target nonblack creature that player controls.').
card_mana_cost('throat slitter', ['4', 'B']).
card_cmc('throat slitter', 5).
card_layout('throat slitter', 'normal').
card_power('throat slitter', 2).
card_toughness('throat slitter', 2).

% Found in: PC2
card_name('thromok the insatiable', 'Thromok the Insatiable').
card_type('thromok the insatiable', 'Legendary Creature — Hellion').
card_types('thromok the insatiable', ['Creature']).
card_subtypes('thromok the insatiable', ['Hellion']).
card_supertypes('thromok the insatiable', ['Legendary']).
card_colors('thromok the insatiable', ['R', 'G']).
card_text('thromok the insatiable', 'Devour X, where X is the number of creatures devoured this way (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with X +1/+1 counters on it for each of those creatures.)').
card_mana_cost('thromok the insatiable', ['3', 'R', 'G']).
card_cmc('thromok the insatiable', 5).
card_layout('thromok the insatiable', 'normal').
card_power('thromok the insatiable', 0).
card_toughness('thromok the insatiable', 0).

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB
card_name('throne of bone', 'Throne of Bone').
card_type('throne of bone', 'Artifact').
card_types('throne of bone', ['Artifact']).
card_subtypes('throne of bone', []).
card_colors('throne of bone', []).
card_text('throne of bone', 'Whenever a player casts a black spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('throne of bone', ['1']).
card_cmc('throne of bone', 1).
card_layout('throne of bone', 'normal').

% Found in: M12
card_name('throne of empires', 'Throne of Empires').
card_type('throne of empires', 'Artifact').
card_types('throne of empires', ['Artifact']).
card_subtypes('throne of empires', []).
card_colors('throne of empires', []).
card_text('throne of empires', '{1}, {T}: Put a 1/1 white Soldier creature token onto the battlefield. Put five of those tokens onto the battlefield instead if you control artifacts named Crown of Empires and Scepter of Empires.').
card_mana_cost('throne of empires', ['4']).
card_cmc('throne of empires', 4).
card_layout('throne of empires', 'normal').

% Found in: SOM
card_name('throne of geth', 'Throne of Geth').
card_type('throne of geth', 'Artifact').
card_types('throne of geth', ['Artifact']).
card_subtypes('throne of geth', []).
card_colors('throne of geth', []).
card_text('throne of geth', '{T}, Sacrifice an artifact: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('throne of geth', ['2']).
card_cmc('throne of geth', 2).
card_layout('throne of geth', 'normal').

% Found in: KTK
card_name('throttle', 'Throttle').
card_type('throttle', 'Instant').
card_types('throttle', ['Instant']).
card_subtypes('throttle', []).
card_colors('throttle', ['B']).
card_text('throttle', 'Target creature gets -4/-4 until end of turn.').
card_mana_cost('throttle', ['4', 'B']).
card_cmc('throttle', 5).
card_layout('throttle', 'normal').

% Found in: CHK
card_name('through the breach', 'Through the Breach').
card_type('through the breach', 'Instant — Arcane').
card_types('through the breach', ['Instant']).
card_subtypes('through the breach', ['Arcane']).
card_colors('through the breach', ['R']).
card_text('through the breach', 'You may put a creature card from your hand onto the battlefield. That creature gains haste. Sacrifice that creature at the beginning of the next end step.\nSplice onto Arcane {2}{R}{R} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('through the breach', ['4', 'R']).
card_cmc('through the breach', 5).
card_layout('through the breach', 'normal').

% Found in: ORI
card_name('throwing knife', 'Throwing Knife').
card_type('throwing knife', 'Artifact — Equipment').
card_types('throwing knife', ['Artifact']).
card_subtypes('throwing knife', ['Equipment']).
card_colors('throwing knife', []).
card_text('throwing knife', 'Equipped creature gets +2/+0.\nWhenever equipped creature attacks, you may sacrifice Throwing Knife. If you do, Throwing Knife deals 2 damage to target creature or player.\nEquip {2} ({2}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('throwing knife', ['2']).
card_cmc('throwing knife', 2).
card_layout('throwing knife', 'normal').

% Found in: DDC
card_name('thrull', 'Thrull').
card_type('thrull', 'Creature — Thrull').
card_types('thrull', ['Creature']).
card_subtypes('thrull', ['Thrull']).
card_colors('thrull', []).
card_text('thrull', '').
card_layout('thrull', 'token').
card_power('thrull', 0).
card_toughness('thrull', 1).

% Found in: FEM, MED
card_name('thrull champion', 'Thrull Champion').
card_type('thrull champion', 'Creature — Thrull').
card_types('thrull champion', ['Creature']).
card_subtypes('thrull champion', ['Thrull']).
card_colors('thrull champion', ['B']).
card_text('thrull champion', 'Thrull creatures get +1/+1.\n{T}: Gain control of target Thrull for as long as you control Thrull Champion.').
card_mana_cost('thrull champion', ['4', 'B']).
card_cmc('thrull champion', 5).
card_layout('thrull champion', 'normal').
card_power('thrull champion', 2).
card_toughness('thrull champion', 2).
card_reserved('thrull champion').

% Found in: GTC
card_name('thrull parasite', 'Thrull Parasite').
card_type('thrull parasite', 'Creature — Thrull').
card_types('thrull parasite', ['Creature']).
card_subtypes('thrull parasite', ['Thrull']).
card_colors('thrull parasite', ['B']).
card_text('thrull parasite', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\n{T}, Pay 2 life: Remove a counter from target nonland permanent.').
card_mana_cost('thrull parasite', ['B']).
card_cmc('thrull parasite', 1).
card_layout('thrull parasite', 'normal').
card_power('thrull parasite', 1).
card_toughness('thrull parasite', 1).

% Found in: 5ED, FEM, MED
card_name('thrull retainer', 'Thrull Retainer').
card_type('thrull retainer', 'Enchantment — Aura').
card_types('thrull retainer', ['Enchantment']).
card_subtypes('thrull retainer', ['Aura']).
card_colors('thrull retainer', ['B']).
card_text('thrull retainer', 'Enchant creature\nEnchanted creature gets +1/+1.\nSacrifice Thrull Retainer: Regenerate enchanted creature.').
card_mana_cost('thrull retainer', ['B']).
card_cmc('thrull retainer', 1).
card_layout('thrull retainer', 'normal').

% Found in: 10E, EXO, TPR
card_name('thrull surgeon', 'Thrull Surgeon').
card_type('thrull surgeon', 'Creature — Thrull').
card_types('thrull surgeon', ['Creature']).
card_subtypes('thrull surgeon', ['Thrull']).
card_colors('thrull surgeon', ['B']).
card_text('thrull surgeon', '{1}{B}, Sacrifice Thrull Surgeon: Look at target player\'s hand and choose a card from it. That player discards that card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('thrull surgeon', ['1', 'B']).
card_cmc('thrull surgeon', 2).
card_layout('thrull surgeon', 'normal').
card_power('thrull surgeon', 1).
card_toughness('thrull surgeon', 1).

% Found in: FEM
card_name('thrull wizard', 'Thrull Wizard').
card_type('thrull wizard', 'Creature — Thrull Wizard').
card_types('thrull wizard', ['Creature']).
card_subtypes('thrull wizard', ['Thrull', 'Wizard']).
card_colors('thrull wizard', ['B']).
card_text('thrull wizard', '{1}{B}: Counter target black spell unless that spell\'s controller pays {B} or {3}.').
card_mana_cost('thrull wizard', ['2', 'B']).
card_cmc('thrull wizard', 3).
card_layout('thrull wizard', 'normal').
card_power('thrull wizard', 1).
card_toughness('thrull wizard', 1).

% Found in: CSP
card_name('thrumming stone', 'Thrumming Stone').
card_type('thrumming stone', 'Legendary Artifact').
card_types('thrumming stone', ['Artifact']).
card_subtypes('thrumming stone', []).
card_supertypes('thrumming stone', ['Legendary']).
card_colors('thrumming stone', []).
card_text('thrumming stone', 'Spells you cast have ripple 4. (Whenever you cast a spell, you may reveal the top four cards of your library. You may cast any revealed cards with the same name as the spell without paying their mana costs. Put the rest on the bottom of your library.)').
card_mana_cost('thrumming stone', ['5']).
card_cmc('thrumming stone', 5).
card_layout('thrumming stone', 'normal').

% Found in: MM2, SOM
card_name('thrummingbird', 'Thrummingbird').
card_type('thrummingbird', 'Creature — Bird Horror').
card_types('thrummingbird', ['Creature']).
card_subtypes('thrummingbird', ['Bird', 'Horror']).
card_colors('thrummingbird', ['U']).
card_text('thrummingbird', 'Flying\nWhenever Thrummingbird deals combat damage to a player, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('thrummingbird', ['1', 'U']).
card_cmc('thrummingbird', 2).
card_layout('thrummingbird', 'normal').
card_power('thrummingbird', 1).
card_toughness('thrummingbird', 1).

% Found in: MBS
card_name('thrun, the last troll', 'Thrun, the Last Troll').
card_type('thrun, the last troll', 'Legendary Creature — Troll Shaman').
card_types('thrun, the last troll', ['Creature']).
card_subtypes('thrun, the last troll', ['Troll', 'Shaman']).
card_supertypes('thrun, the last troll', ['Legendary']).
card_colors('thrun, the last troll', ['G']).
card_text('thrun, the last troll', 'Thrun, the Last Troll can\'t be countered.\nHexproof (This creature can\'t be the target of spells or abilities your opponents control.)\n{1}{G}: Regenerate Thrun.').
card_mana_cost('thrun, the last troll', ['2', 'G', 'G']).
card_cmc('thrun, the last troll', 4).
card_layout('thrun, the last troll', 'normal').
card_power('thrun, the last troll', 4).
card_toughness('thrun, the last troll', 4).

% Found in: TMP
card_name('thumbscrews', 'Thumbscrews').
card_type('thumbscrews', 'Artifact').
card_types('thumbscrews', ['Artifact']).
card_subtypes('thumbscrews', []).
card_colors('thumbscrews', []).
card_text('thumbscrews', 'At the beginning of your upkeep, if you have five or more cards in hand, Thumbscrews deals 1 damage to target opponent.').
card_mana_cost('thumbscrews', ['2']).
card_cmc('thumbscrews', 2).
card_layout('thumbscrews', 'normal').

% Found in: BNG
card_name('thunder brute', 'Thunder Brute').
card_type('thunder brute', 'Creature — Cyclops').
card_types('thunder brute', ['Creature']).
card_subtypes('thunder brute', ['Cyclops']).
card_colors('thunder brute', ['R']).
card_text('thunder brute', 'Trample\nTribute 3 (As this creature enters the battlefield, an opponent of your choice may place three +1/+1 counters on it.)\nWhen Thunder Brute enters the battlefield, if tribute wasn\'t paid, it gains haste until end of turn.').
card_mana_cost('thunder brute', ['4', 'R', 'R']).
card_cmc('thunder brute', 6).
card_layout('thunder brute', 'normal').
card_power('thunder brute', 5).
card_toughness('thunder brute', 5).

% Found in: DDG, DRB, ME4, S99
card_name('thunder dragon', 'Thunder Dragon').
card_type('thunder dragon', 'Creature — Dragon').
card_types('thunder dragon', ['Creature']).
card_subtypes('thunder dragon', ['Dragon']).
card_colors('thunder dragon', ['R']).
card_text('thunder dragon', 'Flying\nWhen Thunder Dragon enters the battlefield, it deals 3 damage to each creature without flying.').
card_mana_cost('thunder dragon', ['5', 'R', 'R']).
card_cmc('thunder dragon', 7).
card_layout('thunder dragon', 'normal').
card_power('thunder dragon', 5).
card_toughness('thunder dragon', 5).

% Found in: ONS
card_name('thunder of hooves', 'Thunder of Hooves').
card_type('thunder of hooves', 'Sorcery').
card_types('thunder of hooves', ['Sorcery']).
card_subtypes('thunder of hooves', []).
card_colors('thunder of hooves', ['R']).
card_text('thunder of hooves', 'Thunder of Hooves deals X damage to each creature without flying and each player, where X is the number of Beasts on the battlefield.').
card_mana_cost('thunder of hooves', ['3', 'R']).
card_cmc('thunder of hooves', 4).
card_layout('thunder of hooves', 'normal').

% Found in: LEG, MED
card_name('thunder spirit', 'Thunder Spirit').
card_type('thunder spirit', 'Creature — Elemental Spirit').
card_types('thunder spirit', ['Creature']).
card_subtypes('thunder spirit', ['Elemental', 'Spirit']).
card_colors('thunder spirit', ['W']).
card_text('thunder spirit', 'Flying, first strike').
card_mana_cost('thunder spirit', ['1', 'W', 'W']).
card_cmc('thunder spirit', 3).
card_layout('thunder spirit', 'normal').
card_power('thunder spirit', 2).
card_toughness('thunder spirit', 2).
card_reserved('thunder spirit').

% Found in: M11, M14
card_name('thunder strike', 'Thunder Strike').
card_type('thunder strike', 'Instant').
card_types('thunder strike', ['Instant']).
card_subtypes('thunder strike', []).
card_colors('thunder strike', ['R']).
card_text('thunder strike', 'Target creature gets +2/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_mana_cost('thunder strike', ['1', 'R']).
card_cmc('thunder strike', 2).
card_layout('thunder strike', 'normal').

% Found in: TSP
card_name('thunder totem', 'Thunder Totem').
card_type('thunder totem', 'Artifact').
card_types('thunder totem', ['Artifact']).
card_subtypes('thunder totem', []).
card_colors('thunder totem', []).
card_text('thunder totem', '{T}: Add {W} to your mana pool.\n{1}{W}{W}: Thunder Totem becomes a 2/2 white Spirit artifact creature with flying and first strike until end of turn.').
card_mana_cost('thunder totem', ['3']).
card_cmc('thunder totem', 3).
card_layout('thunder totem', 'normal').

% Found in: ICE, ME2
card_name('thunder wall', 'Thunder Wall').
card_type('thunder wall', 'Creature — Wall').
card_types('thunder wall', ['Creature']).
card_subtypes('thunder wall', ['Wall']).
card_colors('thunder wall', ['U']).
card_text('thunder wall', 'Defender (This creature can\'t attack.)\nFlying\n{U}: Thunder Wall gets +1/+1 until end of turn.').
card_mana_cost('thunder wall', ['1', 'U', 'U']).
card_cmc('thunder wall', 3).
card_layout('thunder wall', 'normal').
card_power('thunder wall', 0).
card_toughness('thunder wall', 2).

% Found in: ALA, PC2
card_name('thunder-thrash elder', 'Thunder-Thrash Elder').
card_type('thunder-thrash elder', 'Creature — Viashino Warrior').
card_types('thunder-thrash elder', ['Creature']).
card_subtypes('thunder-thrash elder', ['Viashino', 'Warrior']).
card_colors('thunder-thrash elder', ['R']).
card_text('thunder-thrash elder', 'Devour 3 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with three times that many +1/+1 counters on it.)').
card_mana_cost('thunder-thrash elder', ['2', 'R']).
card_cmc('thunder-thrash elder', 3).
card_layout('thunder-thrash elder', 'normal').
card_power('thunder-thrash elder', 1).
card_toughness('thunder-thrash elder', 1).

% Found in: FUT
card_name('thunderblade charge', 'Thunderblade Charge').
card_type('thunderblade charge', 'Sorcery').
card_types('thunderblade charge', ['Sorcery']).
card_subtypes('thunderblade charge', []).
card_colors('thunderblade charge', ['R']).
card_text('thunderblade charge', 'Thunderblade Charge deals 3 damage to target creature or player.\nWhenever one or more creatures you control deal combat damage to a player, if Thunderblade Charge is in your graveyard, you may pay {2}{R}{R}{R}. If you do, you may cast it without paying its mana cost.').
card_mana_cost('thunderblade charge', ['1', 'R', 'R']).
card_cmc('thunderblade charge', 3).
card_layout('thunderblade charge', 'normal').

% Found in: EVE, MM2
card_name('thunderblust', 'Thunderblust').
card_type('thunderblust', 'Creature — Elemental').
card_types('thunderblust', ['Creature']).
card_subtypes('thunderblust', ['Elemental']).
card_colors('thunderblust', ['R']).
card_text('thunderblust', 'Haste\nThunderblust has trample as long as it has a -1/-1 counter on it.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('thunderblust', ['2', 'R', 'R', 'R']).
card_cmc('thunderblust', 5).
card_layout('thunderblust', 'normal').
card_power('thunderblust', 7).
card_toughness('thunderblust', 2).

% Found in: AVR, BTD, PD2, WTH
card_name('thunderbolt', 'Thunderbolt').
card_type('thunderbolt', 'Instant').
card_types('thunderbolt', ['Instant']).
card_subtypes('thunderbolt', []).
card_colors('thunderbolt', ['R']).
card_text('thunderbolt', 'Choose one —\n• Thunderbolt deals 3 damage to target player.\n• Thunderbolt deals 4 damage to target creature with flying.').
card_mana_cost('thunderbolt', ['1', 'R']).
card_cmc('thunderbolt', 2).
card_layout('thunderbolt', 'normal').

% Found in: DTK, pMGD
card_name('thunderbreak regent', 'Thunderbreak Regent').
card_type('thunderbreak regent', 'Creature — Dragon').
card_types('thunderbreak regent', ['Creature']).
card_subtypes('thunderbreak regent', ['Dragon']).
card_colors('thunderbreak regent', ['R']).
card_text('thunderbreak regent', 'Flying\nWhenever a Dragon you control becomes the target of a spell or ability an opponent controls, Thunderbreak Regent deals 3 damage to that player.').
card_mana_cost('thunderbreak regent', ['2', 'R', 'R']).
card_cmc('thunderbreak regent', 4).
card_layout('thunderbreak regent', 'normal').
card_power('thunderbreak regent', 4).
card_toughness('thunderbreak regent', 4).

% Found in: MMQ
card_name('thunderclap', 'Thunderclap').
card_type('thunderclap', 'Instant').
card_types('thunderclap', ['Instant']).
card_subtypes('thunderclap', []).
card_colors('thunderclap', ['R']).
card_text('thunderclap', 'You may sacrifice a Mountain rather than pay Thunderclap\'s mana cost.\nThunderclap deals 3 damage to target creature.').
card_mana_cost('thunderclap', ['2', 'R']).
card_cmc('thunderclap', 3).
card_layout('thunderclap', 'normal').

% Found in: ORI
card_name('thunderclap wyvern', 'Thunderclap Wyvern').
card_type('thunderclap wyvern', 'Creature — Drake').
card_types('thunderclap wyvern', ['Creature']).
card_subtypes('thunderclap wyvern', ['Drake']).
card_colors('thunderclap wyvern', ['W', 'U']).
card_text('thunderclap wyvern', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nOther creatures you control with flying get +1/+1.').
card_mana_cost('thunderclap wyvern', ['2', 'W', 'U']).
card_cmc('thunderclap wyvern', 4).
card_layout('thunderclap wyvern', 'normal').
card_power('thunderclap wyvern', 2).
card_toughness('thunderclap wyvern', 3).

% Found in: SCG
card_name('thundercloud elemental', 'Thundercloud Elemental').
card_type('thundercloud elemental', 'Creature — Elemental').
card_types('thundercloud elemental', ['Creature']).
card_subtypes('thundercloud elemental', ['Elemental']).
card_colors('thundercloud elemental', ['U']).
card_text('thundercloud elemental', 'Flying\n{3}{U}: Tap all creatures with toughness 2 or less.\n{3}{U}: All other creatures lose flying until end of turn.').
card_mana_cost('thundercloud elemental', ['5', 'U', 'U']).
card_cmc('thundercloud elemental', 7).
card_layout('thundercloud elemental', 'normal').
card_power('thundercloud elemental', 3).
card_toughness('thundercloud elemental', 4).

% Found in: LRW, MMA
card_name('thundercloud shaman', 'Thundercloud Shaman').
card_type('thundercloud shaman', 'Creature — Giant Shaman').
card_types('thundercloud shaman', ['Creature']).
card_subtypes('thundercloud shaman', ['Giant', 'Shaman']).
card_colors('thundercloud shaman', ['R']).
card_text('thundercloud shaman', 'When Thundercloud Shaman enters the battlefield, it deals damage equal to the number of Giants you control to each non-Giant creature.').
card_mana_cost('thundercloud shaman', ['3', 'R', 'R']).
card_cmc('thundercloud shaman', 5).
card_layout('thundercloud shaman', 'normal').
card_power('thundercloud shaman', 4).
card_toughness('thundercloud shaman', 4).

% Found in: C14
card_name('thunderfoot baloth', 'Thunderfoot Baloth').
card_type('thunderfoot baloth', 'Creature — Beast').
card_types('thunderfoot baloth', ['Creature']).
card_subtypes('thunderfoot baloth', ['Beast']).
card_colors('thunderfoot baloth', ['G']).
card_text('thunderfoot baloth', 'Trample\nLieutenant — As long as you control your commander, Thunderfoot Baloth gets +2/+2 and other creatures you control get +2/+2 and have trample.').
card_mana_cost('thunderfoot baloth', ['4', 'G', 'G']).
card_cmc('thunderfoot baloth', 6).
card_layout('thunderfoot baloth', 'normal').
card_power('thunderfoot baloth', 5).
card_toughness('thunderfoot baloth', 5).

% Found in: DDJ, GPT
card_name('thunderheads', 'Thunderheads').
card_type('thunderheads', 'Instant').
card_types('thunderheads', ['Instant']).
card_subtypes('thunderheads', []).
card_colors('thunderheads', ['U']).
card_text('thunderheads', 'Replicate {2}{U} (When you cast this spell, copy it for each time you paid its replicate cost.)\nPut a 3/3 blue Weird creature token with defender and flying onto the battlefield. Exile it at the beginning of the next end step.').
card_mana_cost('thunderheads', ['2', 'U']).
card_cmc('thunderheads', 3).
card_layout('thunderheads', 'normal').

% Found in: 10E, BTD, M15, MMA, USG
card_name('thundering giant', 'Thundering Giant').
card_type('thundering giant', 'Creature — Giant').
card_types('thundering giant', ['Creature']).
card_subtypes('thundering giant', ['Giant']).
card_colors('thundering giant', ['R']).
card_text('thundering giant', 'Haste (This creature can attack and {T} as soon as it comes under your control.)').
card_mana_cost('thundering giant', ['3', 'R', 'R']).
card_cmc('thundering giant', 5).
card_layout('thundering giant', 'normal').
card_power('thundering giant', 4).
card_toughness('thundering giant', 3).

% Found in: NPH
card_name('thundering tanadon', 'Thundering Tanadon').
card_type('thundering tanadon', 'Artifact Creature — Beast').
card_types('thundering tanadon', ['Artifact', 'Creature']).
card_subtypes('thundering tanadon', ['Beast']).
card_colors('thundering tanadon', ['G']).
card_text('thundering tanadon', '({G/P} can be paid with either {G} or 2 life.)\nTrample').
card_mana_cost('thundering tanadon', ['4', 'G/P', 'G/P']).
card_cmc('thundering tanadon', 6).
card_layout('thundering tanadon', 'normal').
card_power('thundering tanadon', 5).
card_toughness('thundering tanadon', 4).

% Found in: POR
card_name('thundering wurm', 'Thundering Wurm').
card_type('thundering wurm', 'Creature — Wurm').
card_types('thundering wurm', ['Creature']).
card_subtypes('thundering wurm', ['Wurm']).
card_colors('thundering wurm', ['G']).
card_text('thundering wurm', 'When Thundering Wurm enters the battlefield, sacrifice it unless you discard a land card.').
card_mana_cost('thundering wurm', ['2', 'G']).
card_cmc('thundering wurm', 3).
card_layout('thundering wurm', 'normal').
card_power('thundering wurm', 4).
card_toughness('thundering wurm', 4).

% Found in: 9ED, POR, WTH
card_name('thundermare', 'Thundermare').
card_type('thundermare', 'Creature — Elemental Horse').
card_types('thundermare', ['Creature']).
card_subtypes('thundermare', ['Elemental', 'Horse']).
card_colors('thundermare', ['R']).
card_text('thundermare', 'Haste (This creature can attack the turn it comes under your control.)\nWhen Thundermare enters the battlefield, tap all other creatures.').
card_mana_cost('thundermare', ['5', 'R']).
card_cmc('thundermare', 6).
card_layout('thundermare', 'normal').
card_power('thundermare', 5).
card_toughness('thundermare', 5).

% Found in: M13
card_name('thundermaw hellkite', 'Thundermaw Hellkite').
card_type('thundermaw hellkite', 'Creature — Dragon').
card_types('thundermaw hellkite', ['Creature']).
card_subtypes('thundermaw hellkite', ['Dragon']).
card_colors('thundermaw hellkite', ['R']).
card_text('thundermaw hellkite', 'Flying\nHaste (This creature can attack and {T} as soon as it comes under your control.)\nWhen Thundermaw Hellkite enters the battlefield, it deals 1 damage to each creature with flying your opponents control. Tap those creatures.').
card_mana_cost('thundermaw hellkite', ['3', 'R', 'R']).
card_cmc('thundermaw hellkite', 5).
card_layout('thundermaw hellkite', 'normal').
card_power('thundermaw hellkite', 5).
card_toughness('thundermaw hellkite', 5).

% Found in: BNG
card_name('thunderous might', 'Thunderous Might').
card_type('thunderous might', 'Enchantment — Aura').
card_types('thunderous might', ['Enchantment']).
card_subtypes('thunderous might', ['Aura']).
card_colors('thunderous might', ['R']).
card_text('thunderous might', 'Enchant creature\nWhenever enchanted creature attacks, it gets +X/+0 until end of turn, where X is your devotion to red. (Each {R} in the mana costs of permanents you control counts toward your devotion to red.)').
card_mana_cost('thunderous might', ['1', 'R']).
card_cmc('thunderous might', 2).
card_layout('thunderous might', 'normal').

% Found in: AVR
card_name('thunderous wrath', 'Thunderous Wrath').
card_type('thunderous wrath', 'Instant').
card_types('thunderous wrath', ['Instant']).
card_subtypes('thunderous wrath', []).
card_colors('thunderous wrath', ['R']).
card_text('thunderous wrath', 'Thunderous Wrath deals 5 damage to target creature or player.\nMiracle {R} (You may cast this card for its miracle cost when you draw it if it\'s the first card you drew this turn.)').
card_mana_cost('thunderous wrath', ['4', 'R', 'R']).
card_cmc('thunderous wrath', 6).
card_layout('thunderous wrath', 'normal').

% Found in: INV
card_name('thunderscape apprentice', 'Thunderscape Apprentice').
card_type('thunderscape apprentice', 'Creature — Human Wizard').
card_types('thunderscape apprentice', ['Creature']).
card_subtypes('thunderscape apprentice', ['Human', 'Wizard']).
card_colors('thunderscape apprentice', ['R']).
card_text('thunderscape apprentice', '{B}, {T}: Target player loses 1 life.\n{G}, {T}: Target creature gets +1/+1 until end of turn.').
card_mana_cost('thunderscape apprentice', ['R']).
card_cmc('thunderscape apprentice', 1).
card_layout('thunderscape apprentice', 'normal').
card_power('thunderscape apprentice', 1).
card_toughness('thunderscape apprentice', 1).

% Found in: DDE, PLS
card_name('thunderscape battlemage', 'Thunderscape Battlemage').
card_type('thunderscape battlemage', 'Creature — Human Wizard').
card_types('thunderscape battlemage', ['Creature']).
card_subtypes('thunderscape battlemage', ['Human', 'Wizard']).
card_colors('thunderscape battlemage', ['R']).
card_text('thunderscape battlemage', 'Kicker {1}{B} and/or {G} (You may pay an additional {1}{B} and/or {G} as you cast this spell.)\nWhen Thunderscape Battlemage enters the battlefield, if it was kicked with its {1}{B} kicker, target player discards two cards.\nWhen Thunderscape Battlemage enters the battlefield, if it was kicked with its {G} kicker, destroy target enchantment.').
card_mana_cost('thunderscape battlemage', ['2', 'R']).
card_cmc('thunderscape battlemage', 3).
card_layout('thunderscape battlemage', 'normal').
card_power('thunderscape battlemage', 2).
card_toughness('thunderscape battlemage', 2).

% Found in: PLS
card_name('thunderscape familiar', 'Thunderscape Familiar').
card_type('thunderscape familiar', 'Creature — Kavu').
card_types('thunderscape familiar', ['Creature']).
card_subtypes('thunderscape familiar', ['Kavu']).
card_colors('thunderscape familiar', ['R']).
card_text('thunderscape familiar', 'First strike\nBlack spells and green spells you cast cost {1} less to cast.').
card_mana_cost('thunderscape familiar', ['1', 'R']).
card_cmc('thunderscape familiar', 2).
card_layout('thunderscape familiar', 'normal').
card_power('thunderscape familiar', 1).
card_toughness('thunderscape familiar', 1).

% Found in: INV
card_name('thunderscape master', 'Thunderscape Master').
card_type('thunderscape master', 'Creature — Human Wizard').
card_types('thunderscape master', ['Creature']).
card_subtypes('thunderscape master', ['Human', 'Wizard']).
card_colors('thunderscape master', ['R']).
card_text('thunderscape master', '{B}{B}, {T}: Target player loses 2 life and you gain 2 life.\n{G}{G}, {T}: Creatures you control get +2/+2 until end of turn.').
card_mana_cost('thunderscape master', ['2', 'R', 'R']).
card_cmc('thunderscape master', 4).
card_layout('thunderscape master', 'normal').
card_power('thunderscape master', 2).
card_toughness('thunderscape master', 2).

% Found in: RAV
card_name('thundersong trumpeter', 'Thundersong Trumpeter').
card_type('thundersong trumpeter', 'Creature — Human Soldier').
card_types('thundersong trumpeter', ['Creature']).
card_subtypes('thundersong trumpeter', ['Human', 'Soldier']).
card_colors('thundersong trumpeter', ['W', 'R']).
card_text('thundersong trumpeter', '{T}: Target creature can\'t attack or block this turn.').
card_mana_cost('thundersong trumpeter', ['R', 'W']).
card_cmc('thundersong trumpeter', 2).
card_layout('thundersong trumpeter', 'normal').
card_power('thundersong trumpeter', 2).
card_toughness('thundersong trumpeter', 1).

% Found in: ARC, C13, DST
card_name('thunderstaff', 'Thunderstaff').
card_type('thunderstaff', 'Artifact').
card_types('thunderstaff', ['Artifact']).
card_subtypes('thunderstaff', []).
card_colors('thunderstaff', []).
card_text('thunderstaff', 'As long as Thunderstaff is untapped, if a creature would deal combat damage to you, prevent 1 of that damage.\n{2}, {T}: Attacking creatures get +1/+0 until end of turn.').
card_mana_cost('thunderstaff', ['3']).
card_cmc('thunderstaff', 3).
card_layout('thunderstaff', 'normal').

% Found in: MMQ
card_name('thwart', 'Thwart').
card_type('thwart', 'Instant').
card_types('thwart', ['Instant']).
card_subtypes('thwart', []).
card_colors('thwart', ['U']).
card_text('thwart', 'You may return three Islands you control to their owner\'s hand rather than pay Thwart\'s mana cost.\nCounter target spell.').
card_mana_cost('thwart', ['2', 'U', 'U']).
card_cmc('thwart', 4).
card_layout('thwart', 'normal').

% Found in: AVR, DDK
card_name('tibalt, the fiend-blooded', 'Tibalt, the Fiend-Blooded').
card_type('tibalt, the fiend-blooded', 'Planeswalker — Tibalt').
card_types('tibalt, the fiend-blooded', ['Planeswalker']).
card_subtypes('tibalt, the fiend-blooded', ['Tibalt']).
card_colors('tibalt, the fiend-blooded', ['R']).
card_text('tibalt, the fiend-blooded', '+1: Draw a card, then discard a card at random.\n−4: Tibalt, the Fiend-Blooded deals damage equal to the number of cards in target player\'s hand to that player.\n−6: Gain control of all creatures until end of turn. Untap them. They gain haste until end of turn.').
card_mana_cost('tibalt, the fiend-blooded', ['R', 'R']).
card_cmc('tibalt, the fiend-blooded', 2).
card_layout('tibalt, the fiend-blooded', 'normal').
card_loyalty('tibalt, the fiend-blooded', 2).

% Found in: GPT
card_name('tibor and lumia', 'Tibor and Lumia').
card_type('tibor and lumia', 'Legendary Creature — Human Wizard').
card_types('tibor and lumia', ['Creature']).
card_subtypes('tibor and lumia', ['Human', 'Wizard']).
card_supertypes('tibor and lumia', ['Legendary']).
card_colors('tibor and lumia', ['U', 'R']).
card_text('tibor and lumia', 'Whenever you cast a blue spell, target creature gains flying until end of turn.\nWhenever you cast a red spell, Tibor and Lumia deals 1 damage to each creature without flying.').
card_mana_cost('tibor and lumia', ['2', 'U', 'R']).
card_cmc('tibor and lumia', 4).
card_layout('tibor and lumia', 'normal').
card_power('tibor and lumia', 3).
card_toughness('tibor and lumia', 3).

% Found in: ULG
card_name('ticking gnomes', 'Ticking Gnomes').
card_type('ticking gnomes', 'Artifact Creature — Gnome').
card_types('ticking gnomes', ['Artifact', 'Creature']).
card_subtypes('ticking gnomes', ['Gnome']).
card_colors('ticking gnomes', []).
card_text('ticking gnomes', 'Echo {3} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nSacrifice Ticking Gnomes: Ticking Gnomes deals 1 damage to target creature or player.').
card_mana_cost('ticking gnomes', ['3']).
card_cmc('ticking gnomes', 3).
card_layout('ticking gnomes', 'normal').
card_power('ticking gnomes', 3).
card_toughness('ticking gnomes', 3).

% Found in: MMQ
card_name('tidal bore', 'Tidal Bore').
card_type('tidal bore', 'Instant').
card_types('tidal bore', ['Instant']).
card_subtypes('tidal bore', []).
card_colors('tidal bore', ['U']).
card_text('tidal bore', 'You may return an Island you control to its owner\'s hand rather than pay Tidal Bore\'s mana cost.\nYou may tap or untap target creature.').
card_mana_cost('tidal bore', ['1', 'U']).
card_cmc('tidal bore', 2).
card_layout('tidal bore', 'normal').

% Found in: ALL
card_name('tidal control', 'Tidal Control').
card_type('tidal control', 'Enchantment').
card_types('tidal control', ['Enchantment']).
card_subtypes('tidal control', []).
card_colors('tidal control', ['U']).
card_text('tidal control', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nPay 2 life or {2}: Counter target red or green spell. Any player may activate this ability.').
card_mana_cost('tidal control', ['1', 'U', 'U']).
card_cmc('tidal control', 3).
card_layout('tidal control', 'normal').
card_reserved('tidal control').

% Found in: APC
card_name('tidal courier', 'Tidal Courier').
card_type('tidal courier', 'Creature — Merfolk').
card_types('tidal courier', ['Creature']).
card_subtypes('tidal courier', ['Merfolk']).
card_colors('tidal courier', ['U']).
card_text('tidal courier', 'When Tidal Courier enters the battlefield, reveal the top four cards of your library. Put all Merfolk cards revealed this way into your hand and the rest on the bottom of your library in any order.\n{3}{U}: Tidal Courier gains flying until end of turn.').
card_mana_cost('tidal courier', ['3', 'U']).
card_cmc('tidal courier', 4).
card_layout('tidal courier', 'normal').
card_power('tidal courier', 1).
card_toughness('tidal courier', 2).

% Found in: FEM
card_name('tidal flats', 'Tidal Flats').
card_type('tidal flats', 'Enchantment').
card_types('tidal flats', ['Enchantment']).
card_subtypes('tidal flats', []).
card_colors('tidal flats', ['U']).
card_text('tidal flats', '{U}{U}: For each attacking creature without flying, its controller may pay {1}. If he or she doesn\'t, creatures you control blocking that creature gain first strike until end of turn.').
card_mana_cost('tidal flats', ['U']).
card_cmc('tidal flats', 1).
card_layout('tidal flats', 'normal').

% Found in: C13
card_name('tidal force', 'Tidal Force').
card_type('tidal force', 'Creature — Elemental').
card_types('tidal force', ['Creature']).
card_subtypes('tidal force', ['Elemental']).
card_colors('tidal force', ['U']).
card_text('tidal force', 'At the beginning of each upkeep, you may tap or untap target permanent.').
card_mana_cost('tidal force', ['5', 'U', 'U', 'U']).
card_cmc('tidal force', 8).
card_layout('tidal force', 'normal').
card_power('tidal force', 7).
card_toughness('tidal force', 7).

% Found in: FEM
card_name('tidal influence', 'Tidal Influence').
card_type('tidal influence', 'Enchantment').
card_types('tidal influence', ['Enchantment']).
card_subtypes('tidal influence', []).
card_colors('tidal influence', ['U']).
card_text('tidal influence', 'Cast Tidal Influence only if no permanents named Tidal Influence are on the battlefield.\nTidal Influence enters the battlefield with a tide counter on it.\nAt the beginning of your upkeep, put a tide counter on Tidal Influence.\nAs long as there is exactly one tide counter on Tidal Influence, all blue creatures get -2/-0.\nAs long as there are exactly three tide counters on Tidal Influence, all blue creatures get +2/+0.\nWhenever there are four tide counters on Tidal Influence, remove all tide counters from it.').
card_mana_cost('tidal influence', ['2', 'U']).
card_cmc('tidal influence', 3).
card_layout('tidal influence', 'normal').

% Found in: 8ED, 9ED, MMQ
card_name('tidal kraken', 'Tidal Kraken').
card_type('tidal kraken', 'Creature — Kraken').
card_types('tidal kraken', ['Creature']).
card_subtypes('tidal kraken', ['Kraken']).
card_colors('tidal kraken', ['U']).
card_text('tidal kraken', 'Tidal Kraken can\'t be blocked.').
card_mana_cost('tidal kraken', ['5', 'U', 'U', 'U']).
card_cmc('tidal kraken', 8).
card_layout('tidal kraken', 'normal').
card_power('tidal kraken', 6).
card_toughness('tidal kraken', 6).

% Found in: 6ED, PO2, POR, STH
card_name('tidal surge', 'Tidal Surge').
card_type('tidal surge', 'Sorcery').
card_types('tidal surge', ['Sorcery']).
card_subtypes('tidal surge', []).
card_colors('tidal surge', ['U']).
card_text('tidal surge', 'Tap up to three target creatures without flying.').
card_mana_cost('tidal surge', ['1', 'U']).
card_cmc('tidal surge', 2).
card_layout('tidal surge', 'normal').

% Found in: INV
card_name('tidal visionary', 'Tidal Visionary').
card_type('tidal visionary', 'Creature — Merfolk Wizard').
card_types('tidal visionary', ['Creature']).
card_subtypes('tidal visionary', ['Merfolk', 'Wizard']).
card_colors('tidal visionary', ['U']).
card_text('tidal visionary', '{T}: Target creature becomes the color of your choice until end of turn.').
card_mana_cost('tidal visionary', ['U']).
card_cmc('tidal visionary', 1).
card_layout('tidal visionary', 'normal').
card_power('tidal visionary', 1).
card_toughness('tidal visionary', 1).

% Found in: STH
card_name('tidal warrior', 'Tidal Warrior').
card_type('tidal warrior', 'Creature — Merfolk Warrior').
card_types('tidal warrior', ['Creature']).
card_subtypes('tidal warrior', ['Merfolk', 'Warrior']).
card_colors('tidal warrior', ['U']).
card_text('tidal warrior', '{T}: Target land becomes an Island until end of turn.').
card_mana_cost('tidal warrior', ['U']).
card_cmc('tidal warrior', 1).
card_layout('tidal warrior', 'normal').
card_power('tidal warrior', 1).
card_toughness('tidal warrior', 1).

% Found in: MIR
card_name('tidal wave', 'Tidal Wave').
card_type('tidal wave', 'Instant').
card_types('tidal wave', ['Instant']).
card_subtypes('tidal wave', []).
card_colors('tidal wave', ['U']).
card_text('tidal wave', 'Put a 5/5 blue Wall creature token with defender onto the battlefield. Sacrifice it at the beginning of the next end step.').
card_mana_cost('tidal wave', ['2', 'U']).
card_cmc('tidal wave', 3).
card_layout('tidal wave', 'normal').

% Found in: BFZ
card_name('tide drifter', 'Tide Drifter').
card_type('tide drifter', 'Creature — Eldrazi Drone').
card_types('tide drifter', ['Creature']).
card_subtypes('tide drifter', ['Eldrazi', 'Drone']).
card_colors('tide drifter', []).
card_text('tide drifter', 'Devoid (This card has no color.)\nOther colorless creatures you control get +0/+1.').
card_mana_cost('tide drifter', ['1', 'U']).
card_cmc('tide drifter', 2).
card_layout('tide drifter', 'normal').
card_power('tide drifter', 0).
card_toughness('tide drifter', 5).

% Found in: CHK
card_name('tide of war', 'Tide of War').
card_type('tide of war', 'Enchantment').
card_types('tide of war', ['Enchantment']).
card_subtypes('tide of war', []).
card_colors('tide of war', ['R']).
card_text('tide of war', 'Whenever one or more creatures block, flip a coin. If you win the flip, each blocking creature is sacrificed by its controller. If you lose the flip, each blocked creature is sacrificed by its controller.').
card_mana_cost('tide of war', ['4', 'R', 'R']).
card_cmc('tide of war', 6).
card_layout('tide of war', 'normal').

% Found in: M14
card_name('tidebinder mage', 'Tidebinder Mage').
card_type('tidebinder mage', 'Creature — Merfolk Wizard').
card_types('tidebinder mage', ['Creature']).
card_subtypes('tidebinder mage', ['Merfolk', 'Wizard']).
card_colors('tidebinder mage', ['U']).
card_text('tidebinder mage', 'When Tidebinder Mage enters the battlefield, tap target red or green creature an opponent controls. That creature doesn\'t untap during its controller\'s untap step for as long as you control Tidebinder Mage.').
card_mana_cost('tidebinder mage', ['U', 'U']).
card_cmc('tidebinder mage', 2).
card_layout('tidebinder mage', 'normal').
card_power('tidebinder mage', 2).
card_toughness('tidebinder mage', 2).

% Found in: WWK
card_name('tideforce elemental', 'Tideforce Elemental').
card_type('tideforce elemental', 'Creature — Elemental').
card_types('tideforce elemental', ['Creature']).
card_subtypes('tideforce elemental', ['Elemental']).
card_colors('tideforce elemental', ['U']).
card_text('tideforce elemental', '{U}, {T}: You may tap or untap another target creature.\nLandfall — Whenever a land enters the battlefield under your control, you may untap Tideforce Elemental.').
card_mana_cost('tideforce elemental', ['2', 'U']).
card_cmc('tideforce elemental', 3).
card_layout('tideforce elemental', 'normal').
card_power('tideforce elemental', 2).
card_toughness('tideforce elemental', 1).

% Found in: ALA, MD1, MMA, pFNM
card_name('tidehollow sculler', 'Tidehollow Sculler').
card_type('tidehollow sculler', 'Artifact Creature — Zombie').
card_types('tidehollow sculler', ['Artifact', 'Creature']).
card_subtypes('tidehollow sculler', ['Zombie']).
card_colors('tidehollow sculler', ['W', 'B']).
card_text('tidehollow sculler', 'When Tidehollow Sculler enters the battlefield, target opponent reveals his or her hand and you choose a nonland card from it. Exile that card.\nWhen Tidehollow Sculler leaves the battlefield, return the exiled card to its owner\'s hand.').
card_mana_cost('tidehollow sculler', ['W', 'B']).
card_cmc('tidehollow sculler', 2).
card_layout('tidehollow sculler', 'normal').
card_power('tidehollow sculler', 2).
card_toughness('tidehollow sculler', 2).

% Found in: ALA, C13
card_name('tidehollow strix', 'Tidehollow Strix').
card_type('tidehollow strix', 'Artifact Creature — Bird').
card_types('tidehollow strix', ['Artifact', 'Creature']).
card_subtypes('tidehollow strix', ['Bird']).
card_colors('tidehollow strix', ['U', 'B']).
card_text('tidehollow strix', 'Flying\nDeathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('tidehollow strix', ['U', 'B']).
card_cmc('tidehollow strix', 2).
card_layout('tidehollow strix', 'normal').
card_power('tidehollow strix', 2).
card_toughness('tidehollow strix', 1).

% Found in: LRW
card_name('tideshaper mystic', 'Tideshaper Mystic').
card_type('tideshaper mystic', 'Creature — Merfolk Wizard').
card_types('tideshaper mystic', ['Creature']).
card_subtypes('tideshaper mystic', ['Merfolk', 'Wizard']).
card_colors('tideshaper mystic', ['U']).
card_text('tideshaper mystic', '{T}: Target land becomes the basic land type of your choice until end of turn. Activate this ability only during your turn.').
card_mana_cost('tideshaper mystic', ['U']).
card_cmc('tideshaper mystic', 1).
card_layout('tideshaper mystic', 'normal').
card_power('tideshaper mystic', 1).
card_toughness('tideshaper mystic', 1).

% Found in: DIS
card_name('tidespout tyrant', 'Tidespout Tyrant').
card_type('tidespout tyrant', 'Creature — Djinn').
card_types('tidespout tyrant', ['Creature']).
card_subtypes('tidespout tyrant', ['Djinn']).
card_colors('tidespout tyrant', ['U']).
card_text('tidespout tyrant', 'Flying\nWhenever you cast a spell, return target permanent to its owner\'s hand.').
card_mana_cost('tidespout tyrant', ['5', 'U', 'U', 'U']).
card_cmc('tidespout tyrant', 8).
card_layout('tidespout tyrant', 'normal').
card_power('tidespout tyrant', 5).
card_toughness('tidespout tyrant', 5).

% Found in: PLC
card_name('tidewalker', 'Tidewalker').
card_type('tidewalker', 'Creature — Elemental').
card_types('tidewalker', ['Creature']).
card_subtypes('tidewalker', ['Elemental']).
card_colors('tidewalker', ['U']).
card_text('tidewalker', 'Tidewalker enters the battlefield with a time counter on it for each Island you control.\nVanishing (At the beginning of your upkeep, remove a time counter from this permanent. When the last is removed, sacrifice it.)\nTidewalker\'s power and toughness are each equal to the number of time counters on it.').
card_mana_cost('tidewalker', ['2', 'U']).
card_cmc('tidewalker', 3).
card_layout('tidewalker', 'normal').
card_power('tidewalker', '*').
card_toughness('tidewalker', '*').

% Found in: RAV
card_name('tidewater minion', 'Tidewater Minion').
card_type('tidewater minion', 'Creature — Elemental Minion').
card_types('tidewater minion', ['Creature']).
card_subtypes('tidewater minion', ['Elemental', 'Minion']).
card_colors('tidewater minion', ['U']).
card_text('tidewater minion', 'Defender (This creature can\'t attack.)\n{4}: Tidewater Minion loses defender until end of turn.\n{T}: Untap target permanent.').
card_mana_cost('tidewater minion', ['3', 'U', 'U']).
card_cmc('tidewater minion', 5).
card_layout('tidewater minion', 'normal').
card_power('tidewater minion', 4).
card_toughness('tidewater minion', 4).

% Found in: 10E, 9ED, S99, pMPR
card_name('tidings', 'Tidings').
card_type('tidings', 'Sorcery').
card_types('tidings', ['Sorcery']).
card_subtypes('tidings', []).
card_colors('tidings', ['U']).
card_text('tidings', 'Draw four cards.').
card_mana_cost('tidings', ['3', 'U', 'U']).
card_cmc('tidings', 5).
card_layout('tidings', 'normal').

% Found in: MMQ
card_name('tiger claws', 'Tiger Claws').
card_type('tiger claws', 'Enchantment — Aura').
card_types('tiger claws', ['Enchantment']).
card_subtypes('tiger claws', ['Aura']).
card_colors('tiger claws', ['G']).
card_text('tiger claws', 'Flash\nEnchant creature\nEnchanted creature gets +1/+1 and has trample.').
card_mana_cost('tiger claws', ['2', 'G']).
card_cmc('tiger claws', 3).
card_layout('tiger claws', 'normal').

% Found in: INV
card_name('tigereye cameo', 'Tigereye Cameo').
card_type('tigereye cameo', 'Artifact').
card_types('tigereye cameo', ['Artifact']).
card_subtypes('tigereye cameo', []).
card_colors('tigereye cameo', []).
card_text('tigereye cameo', '{T}: Add {G} or {W} to your mana pool.').
card_mana_cost('tigereye cameo', ['3']).
card_cmc('tigereye cameo', 3).
card_layout('tigereye cameo', 'normal').

% Found in: BFZ
card_name('tightening coils', 'Tightening Coils').
card_type('tightening coils', 'Enchantment — Aura').
card_types('tightening coils', ['Enchantment']).
card_subtypes('tightening coils', ['Aura']).
card_colors('tightening coils', ['U']).
card_text('tightening coils', 'Enchant creature\nEnchanted creature gets -6/-0 and loses flying.').
card_mana_cost('tightening coils', ['1', 'U']).
card_cmc('tightening coils', 2).
card_layout('tightening coils', 'normal').

% Found in: EVE
card_name('tilling treefolk', 'Tilling Treefolk').
card_type('tilling treefolk', 'Creature — Treefolk Druid').
card_types('tilling treefolk', ['Creature']).
card_subtypes('tilling treefolk', ['Treefolk', 'Druid']).
card_colors('tilling treefolk', ['G']).
card_text('tilling treefolk', 'When Tilling Treefolk enters the battlefield, you may return up to two target land cards from your graveyard to your hand.').
card_mana_cost('tilling treefolk', ['2', 'G']).
card_cmc('tilling treefolk', 3).
card_layout('tilling treefolk', 'normal').
card_power('tilling treefolk', 1).
card_toughness('tilling treefolk', 3).

% Found in: LRW
card_name('timber protector', 'Timber Protector').
card_type('timber protector', 'Creature — Treefolk Warrior').
card_types('timber protector', ['Creature']).
card_subtypes('timber protector', ['Treefolk', 'Warrior']).
card_colors('timber protector', ['G']).
card_text('timber protector', 'Other Treefolk creatures you control get +1/+1.\nOther Treefolk and Forests you control have indestructible.').
card_mana_cost('timber protector', ['4', 'G']).
card_cmc('timber protector', 5).
card_layout('timber protector', 'normal').
card_power('timber protector', 4).
card_toughness('timber protector', 6).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('timber wolves', 'Timber Wolves').
card_type('timber wolves', 'Creature — Wolf').
card_types('timber wolves', ['Creature']).
card_subtypes('timber wolves', ['Wolf']).
card_colors('timber wolves', ['G']).
card_text('timber wolves', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('timber wolves', ['G']).
card_cmc('timber wolves', 1).
card_layout('timber wolves', 'normal').
card_power('timber wolves', 1).
card_toughness('timber wolves', 1).

% Found in: AVR
card_name('timberland guide', 'Timberland Guide').
card_type('timberland guide', 'Creature — Human Scout').
card_types('timberland guide', ['Creature']).
card_subtypes('timberland guide', ['Human', 'Scout']).
card_colors('timberland guide', ['G']).
card_text('timberland guide', 'When Timberland Guide enters the battlefield, put a +1/+1 counter on target creature.').
card_mana_cost('timberland guide', ['1', 'G']).
card_cmc('timberland guide', 2).
card_layout('timberland guide', 'normal').
card_power('timberland guide', 1).
card_toughness('timberland guide', 1).

% Found in: ODY
card_name('timberland ruins', 'Timberland Ruins').
card_type('timberland ruins', 'Land').
card_types('timberland ruins', ['Land']).
card_subtypes('timberland ruins', []).
card_colors('timberland ruins', []).
card_text('timberland ruins', 'Timberland Ruins enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Timberland Ruins: Add one mana of any color to your mana pool.').
card_layout('timberland ruins', 'normal').

% Found in: ICE
card_name('timberline ridge', 'Timberline Ridge').
card_type('timberline ridge', 'Land').
card_types('timberline ridge', ['Land']).
card_subtypes('timberline ridge', []).
card_colors('timberline ridge', []).
card_text('timberline ridge', 'Timberline Ridge doesn\'t untap during your untap step if it has a depletion counter on it.\nAt the beginning of your upkeep, remove a depletion counter from Timberline Ridge.\n{T}: Add {R} or {G} to your mana pool. Put a depletion counter on Timberline Ridge.').
card_layout('timberline ridge', 'normal').
card_reserved('timberline ridge').

% Found in: PLC
card_name('timbermare', 'Timbermare').
card_type('timbermare', 'Creature — Elemental Horse').
card_types('timbermare', ['Creature']).
card_subtypes('timbermare', ['Elemental', 'Horse']).
card_colors('timbermare', ['G']).
card_text('timbermare', 'Haste\nEcho {5}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Timbermare enters the battlefield, tap all other creatures.').
card_mana_cost('timbermare', ['3', 'G']).
card_cmc('timbermare', 4).
card_layout('timbermare', 'normal').
card_power('timbermare', 5).
card_toughness('timbermare', 5).

% Found in: ZEN
card_name('timbermaw larva', 'Timbermaw Larva').
card_type('timbermaw larva', 'Creature — Beast').
card_types('timbermaw larva', ['Creature']).
card_subtypes('timbermaw larva', ['Beast']).
card_colors('timbermaw larva', ['G']).
card_text('timbermaw larva', 'Whenever Timbermaw Larva attacks, it gets +1/+1 until end of turn for each Forest you control.').
card_mana_cost('timbermaw larva', ['3', 'G']).
card_cmc('timbermaw larva', 4).
card_layout('timbermaw larva', 'normal').
card_power('timbermaw larva', 2).
card_toughness('timbermaw larva', 2).

% Found in: M13, ORI
card_name('timberpack wolf', 'Timberpack Wolf').
card_type('timberpack wolf', 'Creature — Wolf').
card_types('timberpack wolf', ['Creature']).
card_subtypes('timberpack wolf', ['Wolf']).
card_colors('timberpack wolf', ['G']).
card_text('timberpack wolf', 'Timberpack Wolf gets +1/+1 for each other creature you control named Timberpack Wolf.').
card_mana_cost('timberpack wolf', ['1', 'G']).
card_cmc('timberpack wolf', 2).
card_layout('timberpack wolf', 'normal').
card_power('timberpack wolf', 2).
card_toughness('timberpack wolf', 2).

% Found in: C14, DD3_EVG, EVG, LGN
card_name('timberwatch elf', 'Timberwatch Elf').
card_type('timberwatch elf', 'Creature — Elf').
card_types('timberwatch elf', ['Creature']).
card_subtypes('timberwatch elf', ['Elf']).
card_colors('timberwatch elf', ['G']).
card_text('timberwatch elf', '{T}: Target creature gets +X/+X until end of turn, where X is the number of Elves on the battlefield.').
card_mana_cost('timberwatch elf', ['2', 'G']).
card_cmc('timberwatch elf', 3).
card_layout('timberwatch elf', 'normal').
card_power('timberwatch elf', 1).
card_toughness('timberwatch elf', 2).

% Found in: VIS
card_name('time and tide', 'Time and Tide').
card_type('time and tide', 'Instant').
card_types('time and tide', ['Instant']).
card_subtypes('time and tide', []).
card_colors('time and tide', ['U']).
card_text('time and tide', 'Simultaneously, all phased-out creatures phase in and all creatures with phasing phase out.').
card_mana_cost('time and tide', ['U', 'U']).
card_cmc('time and tide', 2).
card_layout('time and tide', 'normal').

% Found in: 5ED, ICE, ME2
card_name('time bomb', 'Time Bomb').
card_type('time bomb', 'Artifact').
card_types('time bomb', ['Artifact']).
card_subtypes('time bomb', []).
card_colors('time bomb', []).
card_text('time bomb', 'At the beginning of your upkeep, put a time counter on Time Bomb.\n{1}, {T}, Sacrifice Time Bomb: Time Bomb deals damage equal to the number of time counters on it to each creature and each player.').
card_mana_cost('time bomb', ['4']).
card_cmc('time bomb', 4).
card_layout('time bomb', 'normal').

% Found in: PC2
card_name('time distortion', 'Time Distortion').
card_type('time distortion', 'Phenomenon').
card_types('time distortion', ['Phenomenon']).
card_subtypes('time distortion', []).
card_colors('time distortion', []).
card_text('time distortion', 'When you encounter Time Distortion, reverse the game\'s turn order. (For example, if play had proceeded clockwise around the table, it now goes counterclockwise. Then planeswalk away from this phenomenon.)').
card_layout('time distortion', 'phenomenon').

% Found in: 9ED, M14, PO2, POR, S00, S99, TMP, TPR
card_name('time ebb', 'Time Ebb').
card_type('time ebb', 'Sorcery').
card_types('time ebb', ['Sorcery']).
card_subtypes('time ebb', []).
card_colors('time ebb', ['U']).
card_text('time ebb', 'Put target creature on top of its owner\'s library.').
card_mana_cost('time ebb', ['2', 'U']).
card_cmc('time ebb', 3).
card_layout('time ebb', 'normal').

% Found in: 4ED, 5ED, LEG, MED
card_name('time elemental', 'Time Elemental').
card_type('time elemental', 'Creature — Elemental').
card_types('time elemental', ['Creature']).
card_subtypes('time elemental', ['Elemental']).
card_colors('time elemental', ['U']).
card_text('time elemental', 'When Time Elemental attacks or blocks, at end of combat, sacrifice it and it deals 5 damage to you.\n{2}{U}{U}, {T}: Return target permanent that isn\'t enchanted to its owner\'s hand.').
card_mana_cost('time elemental', ['2', 'U']).
card_cmc('time elemental', 3).
card_layout('time elemental', 'normal').
card_power('time elemental', 0).
card_toughness('time elemental', 2).

% Found in: UNH
card_name('time machine', 'Time Machine').
card_type('time machine', 'Artifact').
card_types('time machine', ['Artifact']).
card_subtypes('time machine', []).
card_colors('time machine', []).
card_text('time machine', '{T}: Remove Time Machine and target nontoken creature you own from the game. Return both cards to play at the beginning of your upkeep on your turn X of the next game you play with the same opponent, where X is the removed creature\'s converted mana cost.').
card_mana_cost('time machine', ['5']).
card_cmc('time machine', 5).
card_layout('time machine', 'normal').

% Found in: ROE
card_name('time of heroes', 'Time of Heroes').
card_type('time of heroes', 'Enchantment').
card_types('time of heroes', ['Enchantment']).
card_subtypes('time of heroes', []).
card_colors('time of heroes', ['W']).
card_text('time of heroes', 'Each creature you control with a level counter on it gets +2/+2.').
card_mana_cost('time of heroes', ['1', 'W']).
card_cmc('time of heroes', 2).
card_layout('time of heroes', 'normal').

% Found in: CHK
card_name('time of need', 'Time of Need').
card_type('time of need', 'Sorcery').
card_types('time of need', ['Sorcery']).
card_subtypes('time of need', []).
card_colors('time of need', ['G']).
card_text('time of need', 'Search your library for a legendary creature card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('time of need', ['1', 'G']).
card_cmc('time of need', 2).
card_layout('time of need', 'normal').

% Found in: M11, M12
card_name('time reversal', 'Time Reversal').
card_type('time reversal', 'Sorcery').
card_types('time reversal', ['Sorcery']).
card_subtypes('time reversal', []).
card_colors('time reversal', ['U']).
card_text('time reversal', 'Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. Exile Time Reversal.').
card_mana_cost('time reversal', ['3', 'U', 'U']).
card_cmc('time reversal', 5).
card_layout('time reversal', 'normal').

% Found in: ARB
card_name('time sieve', 'Time Sieve').
card_type('time sieve', 'Artifact').
card_types('time sieve', ['Artifact']).
card_subtypes('time sieve', []).
card_colors('time sieve', ['U', 'B']).
card_text('time sieve', '{T}, Sacrifice five artifacts: Take an extra turn after this one.').
card_mana_cost('time sieve', ['U', 'B']).
card_cmc('time sieve', 2).
card_layout('time sieve', 'normal').

% Found in: USG
card_name('time spiral', 'Time Spiral').
card_type('time spiral', 'Sorcery').
card_types('time spiral', ['Sorcery']).
card_subtypes('time spiral', []).
card_colors('time spiral', ['U']).
card_text('time spiral', 'Exile Time Spiral. Each player shuffles his or her graveyard and hand into his or her library, then draws seven cards. You untap up to six lands.').
card_mana_cost('time spiral', ['4', 'U', 'U']).
card_cmc('time spiral', 6).
card_layout('time spiral', 'normal').
card_reserved('time spiral').

% Found in: 10E, CHK
card_name('time stop', 'Time Stop').
card_type('time stop', 'Instant').
card_types('time stop', ['Instant']).
card_subtypes('time stop', []).
card_colors('time stop', ['U']).
card_text('time stop', 'End the turn. (Exile all spells and abilities on the stack, including this card. The player whose turn it is discards down to his or her maximum hand size. Damage wears off, and \"this turn\" and \"until end of turn\" effects end.)').
card_mana_cost('time stop', ['4', 'U', 'U']).
card_cmc('time stop', 6).
card_layout('time stop', 'normal').

% Found in: 10E, ODY
card_name('time stretch', 'Time Stretch').
card_type('time stretch', 'Sorcery').
card_types('time stretch', ['Sorcery']).
card_subtypes('time stretch', []).
card_colors('time stretch', ['U']).
card_text('time stretch', 'Target player takes two extra turns after this one.').
card_mana_cost('time stretch', ['8', 'U', 'U']).
card_cmc('time stretch', 10).
card_layout('time stretch', 'normal').

% Found in: DDO, THS
card_name('time to feed', 'Time to Feed').
card_type('time to feed', 'Sorcery').
card_types('time to feed', ['Sorcery']).
card_subtypes('time to feed', []).
card_colors('time to feed', ['G']).
card_text('time to feed', 'Choose target creature an opponent controls. When that creature dies this turn, you gain 3 life. Target creature you control fights that creature. (Each deals damage equal to its power to the other.)').
card_mana_cost('time to feed', ['2', 'G']).
card_cmc('time to feed', 3).
card_layout('time to feed', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, ME4, VMA
card_name('time vault', 'Time Vault').
card_type('time vault', 'Artifact').
card_types('time vault', ['Artifact']).
card_subtypes('time vault', []).
card_colors('time vault', []).
card_text('time vault', 'Time Vault enters the battlefield tapped.\nTime Vault doesn\'t untap during your untap step.\nIf you would begin your turn while Time Vault is tapped, you may skip that turn instead. If you do, untap Time Vault.\n{T}: Take an extra turn after this one.').
card_mana_cost('time vault', ['2']).
card_cmc('time vault', 2).
card_layout('time vault', 'normal').
card_reserved('time vault').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('time walk', 'Time Walk').
card_type('time walk', 'Sorcery').
card_types('time walk', ['Sorcery']).
card_subtypes('time walk', []).
card_colors('time walk', ['U']).
card_text('time walk', 'Take an extra turn after this one.').
card_mana_cost('time walk', ['1', 'U']).
card_cmc('time walk', 2).
card_layout('time walk', 'normal').
card_reserved('time walk').

% Found in: M10, S99, TMP, TPR, pJGP
card_name('time warp', 'Time Warp').
card_type('time warp', 'Sorcery').
card_types('time warp', ['Sorcery']).
card_subtypes('time warp', []).
card_colors('time warp', ['U']).
card_text('time warp', 'Target player takes an extra turn after this one.').
card_mana_cost('time warp', ['3', 'U', 'U']).
card_cmc('time warp', 5).
card_layout('time warp', 'normal').

% Found in: PLC
card_name('timebender', 'Timebender').
card_type('timebender', 'Creature — Human Wizard').
card_types('timebender', ['Creature']).
card_subtypes('timebender', ['Human', 'Wizard']).
card_colors('timebender', ['U']).
card_text('timebender', 'Morph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Timebender is turned face up, choose one —\n• Remove two time counters from target permanent or suspended card.\n• Put two time counters on target permanent with a time counter on it or suspended card.').
card_mana_cost('timebender', ['U']).
card_cmc('timebender', 1).
card_layout('timebender', 'normal').
card_power('timebender', 1).
card_toughness('timebender', 1).

% Found in: PLC
card_name('timecrafting', 'Timecrafting').
card_type('timecrafting', 'Instant').
card_types('timecrafting', ['Instant']).
card_subtypes('timecrafting', []).
card_colors('timecrafting', ['R']).
card_text('timecrafting', 'Choose one —\n• Remove X time counters from target permanent or suspended card.\n• Put X time counters on target permanent with a time counter on it or suspended card.').
card_mana_cost('timecrafting', ['X', 'R']).
card_cmc('timecrafting', 1).
card_layout('timecrafting', 'normal').

% Found in: KTK
card_name('timely hordemate', 'Timely Hordemate').
card_type('timely hordemate', 'Creature — Human Warrior').
card_types('timely hordemate', ['Creature']).
card_subtypes('timely hordemate', ['Human', 'Warrior']).
card_colors('timely hordemate', ['W']).
card_text('timely hordemate', 'Raid — When Timely Hordemate enters the battlefield, if you attacked with a creature this turn, return target creature card with converted mana cost 2 or less from your graveyard to the battlefield.').
card_mana_cost('timely hordemate', ['3', 'W']).
card_cmc('timely hordemate', 4).
card_layout('timely hordemate', 'normal').
card_power('timely hordemate', 3).
card_toughness('timely hordemate', 2).

% Found in: M12
card_name('timely reinforcements', 'Timely Reinforcements').
card_type('timely reinforcements', 'Sorcery').
card_types('timely reinforcements', ['Sorcery']).
card_subtypes('timely reinforcements', []).
card_colors('timely reinforcements', ['W']).
card_text('timely reinforcements', 'If you have less life than an opponent, you gain 6 life. If you control fewer creatures than an opponent, put three 1/1 white Soldier creature tokens onto the battlefield.').
card_mana_cost('timely reinforcements', ['2', 'W']).
card_cmc('timely reinforcements', 3).
card_layout('timely reinforcements', 'normal').

% Found in: MRD
card_name('timesifter', 'Timesifter').
card_type('timesifter', 'Artifact').
card_types('timesifter', ['Artifact']).
card_subtypes('timesifter', []).
card_colors('timesifter', []).
card_text('timesifter', 'At the beginning of each upkeep, each player exiles the top card of his or her library. The player who exiled the card with the highest converted mana cost takes an extra turn after this one. If two or more players\' cards are tied for highest cost, the tied players repeat this process until the tie is broken.').
card_mana_cost('timesifter', ['5']).
card_cmc('timesifter', 5).
card_layout('timesifter', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, VMA
card_name('timetwister', 'Timetwister').
card_type('timetwister', 'Sorcery').
card_types('timetwister', ['Sorcery']).
card_subtypes('timetwister', []).
card_colors('timetwister', ['U']).
card_text('timetwister', 'Each player shuffles his or her hand and graveyard into his or her library, then draws seven cards. (Then put Timetwister into its owner\'s graveyard.)').
card_mana_cost('timetwister', ['2', 'U']).
card_cmc('timetwister', 3).
card_layout('timetwister', 'normal').
card_reserved('timetwister').

% Found in: MMQ, WTH
card_name('timid drake', 'Timid Drake').
card_type('timid drake', 'Creature — Drake').
card_types('timid drake', ['Creature']).
card_subtypes('timid drake', ['Drake']).
card_colors('timid drake', ['U']).
card_text('timid drake', 'Flying\nWhen another creature enters the battlefield, return Timid Drake to its owner\'s hand.').
card_mana_cost('timid drake', ['2', 'U']).
card_cmc('timid drake', 3).
card_layout('timid drake', 'normal').
card_power('timid drake', 3).
card_toughness('timid drake', 3).

% Found in: HML
card_name('timmerian fiends', 'Timmerian Fiends').
card_type('timmerian fiends', 'Creature — Horror').
card_types('timmerian fiends', ['Creature']).
card_subtypes('timmerian fiends', ['Horror']).
card_colors('timmerian fiends', ['B']).
card_text('timmerian fiends', 'Remove Timmerian Fiends from your deck before playing if you\'re not playing for ante.\n{B}{B}{B}, Sacrifice Timmerian Fiends: The owner of target artifact may ante the top card of his or her library. If that player doesn\'t, exchange ownership of that artifact and Timmerian Fiends. Put the artifact card into your graveyard and Timmerian Fiends from anywhere into that player\'s graveyard. This change in ownership is permanent.').
card_mana_cost('timmerian fiends', ['1', 'B', 'B']).
card_cmc('timmerian fiends', 3).
card_layout('timmerian fiends', 'normal').
card_power('timmerian fiends', 1).
card_toughness('timmerian fiends', 1).
card_reserved('timmerian fiends').

% Found in: UGL
card_name('timmy, power gamer', 'Timmy, Power Gamer').
card_type('timmy, power gamer', 'Creature — Legend').
card_types('timmy, power gamer', ['Creature']).
card_subtypes('timmy, power gamer', ['Legend']).
card_colors('timmy, power gamer', ['G']).
card_text('timmy, power gamer', '{4}: Put a creature into play from your hand.').
card_mana_cost('timmy, power gamer', ['2', 'G', 'G']).
card_cmc('timmy, power gamer', 4).
card_layout('timmy, power gamer', 'normal').
card_power('timmy, power gamer', 1).
card_toughness('timmy, power gamer', 1).

% Found in: GPT
card_name('tin street hooligan', 'Tin Street Hooligan').
card_type('tin street hooligan', 'Creature — Goblin Rogue').
card_types('tin street hooligan', ['Creature']).
card_subtypes('tin street hooligan', ['Goblin', 'Rogue']).
card_colors('tin street hooligan', ['R']).
card_text('tin street hooligan', 'When Tin Street Hooligan enters the battlefield, if {G} was spent to cast Tin Street Hooligan, destroy target artifact.').
card_mana_cost('tin street hooligan', ['1', 'R']).
card_cmc('tin street hooligan', 2).
card_layout('tin street hooligan', 'normal').
card_power('tin street hooligan', 2).
card_toughness('tin street hooligan', 1).

% Found in: GTC
card_name('tin street market', 'Tin Street Market').
card_type('tin street market', 'Enchantment — Aura').
card_types('tin street market', ['Enchantment']).
card_subtypes('tin street market', ['Aura']).
card_colors('tin street market', ['R']).
card_text('tin street market', 'Enchant land\nEnchanted land has \"{T}, Discard a card: Draw a card.\"').
card_mana_cost('tin street market', ['4', 'R']).
card_cmc('tin street market', 5).
card_layout('tin street market', 'normal').

% Found in: VIS
card_name('tin-wing chimera', 'Tin-Wing Chimera').
card_type('tin-wing chimera', 'Artifact Creature — Chimera').
card_types('tin-wing chimera', ['Artifact', 'Creature']).
card_subtypes('tin-wing chimera', ['Chimera']).
card_colors('tin-wing chimera', []).
card_text('tin-wing chimera', 'Flying\nSacrifice Tin-Wing Chimera: Put a +2/+2 counter on target Chimera creature. It gains flying. (This effect lasts indefinitely.)').
card_mana_cost('tin-wing chimera', ['4']).
card_cmc('tin-wing chimera', 4).
card_layout('tin-wing chimera', 'normal').
card_power('tin-wing chimera', 2).
card_toughness('tin-wing chimera', 2).

% Found in: INV
card_name('tinder farm', 'Tinder Farm').
card_type('tinder farm', 'Land').
card_types('tinder farm', ['Land']).
card_subtypes('tinder farm', []).
card_colors('tinder farm', []).
card_text('tinder farm', 'Tinder Farm enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{T}, Sacrifice Tinder Farm: Add {R}{W} to your mana pool.').
card_layout('tinder farm', 'normal').

% Found in: CST, ICE, ME2
card_name('tinder wall', 'Tinder Wall').
card_type('tinder wall', 'Creature — Plant Wall').
card_types('tinder wall', ['Creature']).
card_subtypes('tinder wall', ['Plant', 'Wall']).
card_colors('tinder wall', ['G']).
card_text('tinder wall', 'Defender (This creature can\'t attack.)\nSacrifice Tinder Wall: Add {R}{R} to your mana pool.\n{R}, Sacrifice Tinder Wall: Tinder Wall deals 2 damage to target creature it\'s blocking.').
card_mana_cost('tinder wall', ['G']).
card_cmc('tinder wall', 1).
card_layout('tinder wall', 'normal').
card_power('tinder wall', 0).
card_toughness('tinder wall', 3).

% Found in: MBS
card_name('tine shrike', 'Tine Shrike').
card_type('tine shrike', 'Creature — Bird').
card_types('tine shrike', ['Creature']).
card_subtypes('tine shrike', ['Bird']).
card_colors('tine shrike', ['W']).
card_text('tine shrike', 'Flying\nInfect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('tine shrike', ['3', 'W']).
card_cmc('tine shrike', 4).
card_layout('tine shrike', 'normal').
card_power('tine shrike', 2).
card_toughness('tine shrike', 1).

% Found in: ULG, V09
card_name('tinker', 'Tinker').
card_type('tinker', 'Sorcery').
card_types('tinker', ['Sorcery']).
card_subtypes('tinker', []).
card_colors('tinker', ['U']).
card_text('tinker', 'As an additional cost to cast Tinker, sacrifice an artifact.\nSearch your library for an artifact card and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('tinker', ['2', 'U']).
card_cmc('tinker', 3).
card_layout('tinker', 'normal').

% Found in: M11, M15
card_name('tireless missionaries', 'Tireless Missionaries').
card_type('tireless missionaries', 'Creature — Human Cleric').
card_types('tireless missionaries', ['Creature']).
card_subtypes('tireless missionaries', ['Human', 'Cleric']).
card_colors('tireless missionaries', ['W']).
card_text('tireless missionaries', 'When Tireless Missionaries enters the battlefield, you gain 3 life.').
card_mana_cost('tireless missionaries', ['4', 'W']).
card_cmc('tireless missionaries', 5).
card_layout('tireless missionaries', 'normal').
card_power('tireless missionaries', 2).
card_toughness('tireless missionaries', 3).

% Found in: ODY
card_name('tireless tribe', 'Tireless Tribe').
card_type('tireless tribe', 'Creature — Human Nomad').
card_types('tireless tribe', ['Creature']).
card_subtypes('tireless tribe', ['Human', 'Nomad']).
card_colors('tireless tribe', ['W']).
card_text('tireless tribe', 'Discard a card: Tireless Tribe gets +0/+4 until end of turn.').
card_mana_cost('tireless tribe', ['W']).
card_cmc('tireless tribe', 1).
card_layout('tireless tribe', 'normal').
card_power('tireless tribe', 1).
card_toughness('tireless tribe', 1).

% Found in: MBS
card_name('titan forge', 'Titan Forge').
card_type('titan forge', 'Artifact').
card_types('titan forge', ['Artifact']).
card_subtypes('titan forge', []).
card_colors('titan forge', []).
card_text('titan forge', '{3}, {T}: Put a charge counter on Titan Forge.\n{T}, Remove three charge counters from Titan Forge: Put a 9/9 colorless Golem artifact creature token onto the battlefield.').
card_mana_cost('titan forge', ['3']).
card_cmc('titan forge', 3).
card_layout('titan forge', 'normal').

% Found in: THS
card_name('titan of eternal fire', 'Titan of Eternal Fire').
card_type('titan of eternal fire', 'Creature — Giant').
card_types('titan of eternal fire', ['Creature']).
card_subtypes('titan of eternal fire', ['Giant']).
card_colors('titan of eternal fire', ['R']).
card_text('titan of eternal fire', 'Each Human creature you control has \"{R}, {T}: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('titan of eternal fire', ['5', 'R']).
card_cmc('titan of eternal fire', 6).
card_layout('titan of eternal fire', 'normal').
card_power('titan of eternal fire', 5).
card_toughness('titan of eternal fire', 6).

% Found in: BFZ
card_name('titan\'s presence', 'Titan\'s Presence').
card_type('titan\'s presence', 'Instant').
card_types('titan\'s presence', ['Instant']).
card_subtypes('titan\'s presence', []).
card_colors('titan\'s presence', []).
card_text('titan\'s presence', 'As an additional cost to cast Titan\'s Presence, reveal a colorless creature card from your hand.\nExile target creature if its power is less than or equal to the revealed card\'s power.').
card_mana_cost('titan\'s presence', ['3']).
card_cmc('titan\'s presence', 3).
card_layout('titan\'s presence', 'normal').

% Found in: MOR
card_name('titan\'s revenge', 'Titan\'s Revenge').
card_type('titan\'s revenge', 'Sorcery').
card_types('titan\'s revenge', ['Sorcery']).
card_subtypes('titan\'s revenge', []).
card_colors('titan\'s revenge', ['R']).
card_text('titan\'s revenge', 'Titan\'s Revenge deals X damage to target creature or player. Clash with an opponent. If you win, return Titan\'s Revenge to its owner\'s hand. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('titan\'s revenge', ['X', 'R', 'R']).
card_cmc('titan\'s revenge', 2).
card_layout('titan\'s revenge', 'normal').

% Found in: ORI, THS
card_name('titan\'s strength', 'Titan\'s Strength').
card_type('titan\'s strength', 'Instant').
card_types('titan\'s strength', ['Instant']).
card_subtypes('titan\'s strength', []).
card_colors('titan\'s strength', ['R']).
card_text('titan\'s strength', 'Target creature gets +3/+1 until end of turn. Scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('titan\'s strength', ['R']).
card_cmc('titan\'s strength', 1).
card_layout('titan\'s strength', 'normal').

% Found in: VAN
card_name('titania', 'Titania').
card_type('titania', 'Vanguard').
card_types('titania', ['Vanguard']).
card_subtypes('titania', []).
card_colors('titania', []).
card_text('titania', 'You may play an additional land on each of your turns.').
card_layout('titania', 'vanguard').

% Found in: USG
card_name('titania\'s boon', 'Titania\'s Boon').
card_type('titania\'s boon', 'Sorcery').
card_types('titania\'s boon', ['Sorcery']).
card_subtypes('titania\'s boon', []).
card_colors('titania\'s boon', ['G']).
card_text('titania\'s boon', 'Put a +1/+1 counter on each creature you control.').
card_mana_cost('titania\'s boon', ['3', 'G']).
card_cmc('titania\'s boon', 4).
card_layout('titania\'s boon', 'normal').

% Found in: C14, USG
card_name('titania\'s chosen', 'Titania\'s Chosen').
card_type('titania\'s chosen', 'Creature — Elf Archer').
card_types('titania\'s chosen', ['Creature']).
card_subtypes('titania\'s chosen', ['Elf', 'Archer']).
card_colors('titania\'s chosen', ['G']).
card_text('titania\'s chosen', 'Whenever a player casts a green spell, put a +1/+1 counter on Titania\'s Chosen.').
card_mana_cost('titania\'s chosen', ['2', 'G']).
card_cmc('titania\'s chosen', 3).
card_layout('titania\'s chosen', 'normal').
card_power('titania\'s chosen', 1).
card_toughness('titania\'s chosen', 1).

% Found in: 3ED, 4ED, 5ED, ATQ, ME4
card_name('titania\'s song', 'Titania\'s Song').
card_type('titania\'s song', 'Enchantment').
card_types('titania\'s song', ['Enchantment']).
card_subtypes('titania\'s song', []).
card_colors('titania\'s song', ['G']).
card_text('titania\'s song', 'Each noncreature artifact loses all abilities and becomes an artifact creature with power and toughness each equal to its converted mana cost. If Titania\'s Song leaves the battlefield, this effect continues until end of turn.').
card_mana_cost('titania\'s song', ['3', 'G']).
card_cmc('titania\'s song', 4).
card_layout('titania\'s song', 'normal').

% Found in: C14
card_name('titania, protector of argoth', 'Titania, Protector of Argoth').
card_type('titania, protector of argoth', 'Legendary Creature — Elemental').
card_types('titania, protector of argoth', ['Creature']).
card_subtypes('titania, protector of argoth', ['Elemental']).
card_supertypes('titania, protector of argoth', ['Legendary']).
card_colors('titania, protector of argoth', ['G']).
card_text('titania, protector of argoth', 'When Titania, Protector of Argoth enters the battlefield, return target land card from your graveyard to the battlefield.\nWhenever a land you control is put into a graveyard from the battlefield, put a 5/3 green Elemental creature token onto the battlefield.').
card_mana_cost('titania, protector of argoth', ['3', 'G', 'G']).
card_cmc('titania, protector of argoth', 5).
card_layout('titania, protector of argoth', 'normal').
card_power('titania, protector of argoth', 5).
card_toughness('titania, protector of argoth', 3).

% Found in: SCG
card_name('titanic bulvox', 'Titanic Bulvox').
card_type('titanic bulvox', 'Creature — Beast').
card_types('titanic bulvox', ['Creature']).
card_subtypes('titanic bulvox', ['Beast']).
card_colors('titanic bulvox', ['G']).
card_text('titanic bulvox', 'Trample\nMorph {4}{G}{G}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('titanic bulvox', ['6', 'G', 'G']).
card_cmc('titanic bulvox', 8).
card_layout('titanic bulvox', 'normal').
card_power('titanic bulvox', 7).
card_toughness('titanic bulvox', 4).

% Found in: M12, M13, M15, ORI
card_name('titanic growth', 'Titanic Growth').
card_type('titanic growth', 'Instant').
card_types('titanic growth', ['Instant']).
card_subtypes('titanic growth', []).
card_colors('titanic growth', ['G']).
card_text('titanic growth', 'Target creature gets +4/+4 until end of turn.').
card_mana_cost('titanic growth', ['1', 'G']).
card_cmc('titanic growth', 2).
card_layout('titanic growth', 'normal').

% Found in: ALA, DDH
card_name('titanic ultimatum', 'Titanic Ultimatum').
card_type('titanic ultimatum', 'Sorcery').
card_types('titanic ultimatum', ['Sorcery']).
card_subtypes('titanic ultimatum', []).
card_colors('titanic ultimatum', ['W', 'R', 'G']).
card_text('titanic ultimatum', 'Until end of turn, creatures you control get +5/+5 and gain first strike, lifelink, and trample.').
card_mana_cost('titanic ultimatum', ['R', 'R', 'G', 'G', 'G', 'W', 'W']).
card_cmc('titanic ultimatum', 7).
card_layout('titanic ultimatum', 'normal').

% Found in: MRD
card_name('titanium golem', 'Titanium Golem').
card_type('titanium golem', 'Artifact Creature — Golem').
card_types('titanium golem', ['Artifact', 'Creature']).
card_subtypes('titanium golem', ['Golem']).
card_colors('titanium golem', []).
card_text('titanium golem', '{1}{W}: Titanium Golem gains first strike until end of turn.').
card_mana_cost('titanium golem', ['5']).
card_cmc('titanium golem', 5).
card_layout('titanium golem', 'normal').
card_power('titanium golem', 3).
card_toughness('titanium golem', 3).

% Found in: VIS
card_name('tithe', 'Tithe').
card_type('tithe', 'Instant').
card_types('tithe', ['Instant']).
card_subtypes('tithe', []).
card_colors('tithe', ['W']).
card_text('tithe', 'Search your library for a Plains card. If target opponent controls more lands than you, you may search your library for an additional Plains card. Reveal those cards and put them into your hand. Then shuffle your library.').
card_mana_cost('tithe', ['W']).
card_cmc('tithe', 1).
card_layout('tithe', 'normal').
card_reserved('tithe').

% Found in: DGM
card_name('tithe drinker', 'Tithe Drinker').
card_type('tithe drinker', 'Creature — Vampire').
card_types('tithe drinker', ['Creature']).
card_subtypes('tithe drinker', ['Vampire']).
card_colors('tithe drinker', ['W', 'B']).
card_text('tithe drinker', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)\nExtort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)').
card_mana_cost('tithe drinker', ['W', 'B']).
card_cmc('tithe drinker', 2).
card_layout('tithe drinker', 'normal').
card_power('tithe drinker', 2).
card_toughness('tithe drinker', 1).

% Found in: TSP
card_name('tivadar of thorn', 'Tivadar of Thorn').
card_type('tivadar of thorn', 'Legendary Creature — Human Knight').
card_types('tivadar of thorn', ['Creature']).
card_subtypes('tivadar of thorn', ['Human', 'Knight']).
card_supertypes('tivadar of thorn', ['Legendary']).
card_colors('tivadar of thorn', ['W']).
card_text('tivadar of thorn', 'First strike, protection from red\nWhen Tivadar of Thorn enters the battlefield, destroy target Goblin.').
card_mana_cost('tivadar of thorn', ['1', 'W', 'W']).
card_cmc('tivadar of thorn', 3).
card_layout('tivadar of thorn', 'normal').
card_power('tivadar of thorn', 2).
card_toughness('tivadar of thorn', 2).

% Found in: DRK, MED
card_name('tivadar\'s crusade', 'Tivadar\'s Crusade').
card_type('tivadar\'s crusade', 'Sorcery').
card_types('tivadar\'s crusade', ['Sorcery']).
card_subtypes('tivadar\'s crusade', []).
card_colors('tivadar\'s crusade', ['W']).
card_text('tivadar\'s crusade', 'Destroy all Goblins.').
card_mana_cost('tivadar\'s crusade', ['1', 'W', 'W']).
card_cmc('tivadar\'s crusade', 3).
card_layout('tivadar\'s crusade', 'normal').

% Found in: GPT
card_name('to arms!', 'To Arms!').
card_type('to arms!', 'Instant').
card_types('to arms!', ['Instant']).
card_subtypes('to arms!', []).
card_colors('to arms!', ['W']).
card_text('to arms!', 'Untap all creatures you control.\nDraw a card.').
card_mana_cost('to arms!', ['1', 'W']).
card_cmc('to arms!', 2).
card_layout('to arms!', 'normal').

% Found in: CHR, LEG, ME3
card_name('tobias andrion', 'Tobias Andrion').
card_type('tobias andrion', 'Legendary Creature — Human Advisor').
card_types('tobias andrion', ['Creature']).
card_subtypes('tobias andrion', ['Human', 'Advisor']).
card_supertypes('tobias andrion', ['Legendary']).
card_colors('tobias andrion', ['W', 'U']).
card_text('tobias andrion', '').
card_mana_cost('tobias andrion', ['3', 'W', 'U']).
card_cmc('tobias andrion', 5).
card_layout('tobias andrion', 'normal').
card_power('tobias andrion', 4).
card_toughness('tobias andrion', 4).

% Found in: CHK
card_name('tobita, master of winds', 'Tobita, Master of Winds').
card_type('tobita, master of winds', 'Legendary Creature — Human Wizard').
card_types('tobita, master of winds', ['Creature']).
card_subtypes('tobita, master of winds', ['Human', 'Wizard']).
card_supertypes('tobita, master of winds', ['Legendary']).
card_colors('tobita, master of winds', ['U']).
card_text('tobita, master of winds', 'Creatures you control have flying.').
card_mana_cost('tobita, master of winds', ['1', 'U']).
card_cmc('tobita, master of winds', 2).
card_layout('tobita, master of winds', 'flip').
card_power('tobita, master of winds', 3).
card_toughness('tobita, master of winds', 3).

% Found in: UNH
card_name('togglodyte', 'Togglodyte').
card_type('togglodyte', 'Artifact Creature — Golem').
card_types('togglodyte', ['Artifact', 'Creature']).
card_subtypes('togglodyte', ['Golem']).
card_colors('togglodyte', []).
card_text('togglodyte', 'Togglodyte comes into play turned on.\nWhenever a player plays a spell, toggle Togglodyte\'s ON/OFF switch.\nAs long as Togglodyte is turned off, it can\'t attack or block, and all damage it would deal is prevented.').
card_mana_cost('togglodyte', ['3']).
card_cmc('togglodyte', 3).
card_layout('togglodyte', 'normal').
card_power('togglodyte', 4).
card_toughness('togglodyte', 4).

% Found in: DGM
card_name('toil', 'Toil').
card_type('toil', 'Sorcery').
card_types('toil', ['Sorcery']).
card_subtypes('toil', []).
card_colors('toil', ['B']).
card_text('toil', 'Target player draws two cards and loses 2 life.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('toil', ['2', 'B']).
card_cmc('toil', 3).
card_layout('toil', 'split').
card_sides('toil', 'trouble').

% Found in: SHM
card_name('toil to renown', 'Toil to Renown').
card_type('toil to renown', 'Sorcery').
card_types('toil to renown', ['Sorcery']).
card_subtypes('toil to renown', []).
card_colors('toil to renown', ['G']).
card_text('toil to renown', 'You gain 1 life for each tapped artifact, creature, and land you control.').
card_mana_cost('toil to renown', ['1', 'G']).
card_cmc('toil to renown', 2).
card_layout('toil to renown', 'normal').

% Found in: BOK
card_name('toils of night and day', 'Toils of Night and Day').
card_type('toils of night and day', 'Instant — Arcane').
card_types('toils of night and day', ['Instant']).
card_subtypes('toils of night and day', ['Arcane']).
card_colors('toils of night and day', ['U']).
card_text('toils of night and day', 'You may tap or untap target permanent, then you may tap or untap another target permanent.').
card_mana_cost('toils of night and day', ['2', 'U']).
card_cmc('toils of night and day', 3).
card_layout('toils of night and day', 'normal').

% Found in: CHK
card_name('tok-tok, volcano born', 'Tok-Tok, Volcano Born').
card_type('tok-tok, volcano born', 'Legendary Creature — Goblin Shaman').
card_types('tok-tok, volcano born', ['Creature']).
card_subtypes('tok-tok, volcano born', ['Goblin', 'Shaman']).
card_supertypes('tok-tok, volcano born', ['Legendary']).
card_colors('tok-tok, volcano born', ['R']).
card_text('tok-tok, volcano born', 'Protection from red\nIf a red source would deal damage to a player, it deals that much damage plus 1 to that player instead.').
card_mana_cost('tok-tok, volcano born', ['3', 'R']).
card_cmc('tok-tok, volcano born', 4).
card_layout('tok-tok, volcano born', 'flip').
card_power('tok-tok, volcano born', 2).
card_toughness('tok-tok, volcano born', 2).

% Found in: LEG
card_name('tolaria', 'Tolaria').
card_type('tolaria', 'Legendary Land').
card_types('tolaria', ['Land']).
card_subtypes('tolaria', []).
card_supertypes('tolaria', ['Legendary']).
card_colors('tolaria', []).
card_text('tolaria', '{T}: Add {U} to your mana pool.\n{T}: Target creature loses banding and all \"bands with other\" abilities until end of turn. Activate this ability only during any upkeep step.').
card_layout('tolaria', 'normal').

% Found in: FUT
card_name('tolaria west', 'Tolaria West').
card_type('tolaria west', 'Land').
card_types('tolaria west', ['Land']).
card_subtypes('tolaria west', []).
card_colors('tolaria west', []).
card_text('tolaria west', 'Tolaria West enters the battlefield tapped.\n{T}: Add {U} to your mana pool.\nTransmute {1}{U}{U} ({1}{U}{U}, Discard this card: Search your library for a card with converted mana cost 0, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_layout('tolaria west', 'normal').

% Found in: USG, VMA
card_name('tolarian academy', 'Tolarian Academy').
card_type('tolarian academy', 'Legendary Land').
card_types('tolarian academy', ['Land']).
card_subtypes('tolarian academy', []).
card_supertypes('tolarian academy', ['Legendary']).
card_colors('tolarian academy', []).
card_text('tolarian academy', '{T}: Add {U} to your mana pool for each artifact you control.').
card_layout('tolarian academy', 'normal').
card_reserved('tolarian academy').

% Found in: WTH
card_name('tolarian drake', 'Tolarian Drake').
card_type('tolarian drake', 'Creature — Drake').
card_types('tolarian drake', ['Creature']).
card_subtypes('tolarian drake', ['Drake']).
card_colors('tolarian drake', ['U']).
card_text('tolarian drake', 'Flying\nPhasing (This phases in or out before you untap during each of your untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)').
card_mana_cost('tolarian drake', ['2', 'U']).
card_cmc('tolarian drake', 3).
card_layout('tolarian drake', 'normal').
card_power('tolarian drake', 2).
card_toughness('tolarian drake', 4).

% Found in: INV
card_name('tolarian emissary', 'Tolarian Emissary').
card_type('tolarian emissary', 'Creature — Human Wizard').
card_types('tolarian emissary', ['Creature']).
card_subtypes('tolarian emissary', ['Human', 'Wizard']).
card_colors('tolarian emissary', ['U']).
card_text('tolarian emissary', 'Kicker {1}{W} (You may pay an additional {1}{W} as you cast this spell.)\nFlying\nWhen Tolarian Emissary enters the battlefield, if it was kicked, destroy target enchantment.').
card_mana_cost('tolarian emissary', ['2', 'U']).
card_cmc('tolarian emissary', 3).
card_layout('tolarian emissary', 'normal').
card_power('tolarian emissary', 1).
card_toughness('tolarian emissary', 2).

% Found in: WTH
card_name('tolarian entrancer', 'Tolarian Entrancer').
card_type('tolarian entrancer', 'Creature — Human Wizard').
card_types('tolarian entrancer', ['Creature']).
card_subtypes('tolarian entrancer', ['Human', 'Wizard']).
card_colors('tolarian entrancer', ['U']).
card_text('tolarian entrancer', 'Whenever Tolarian Entrancer becomes blocked by a creature, gain control of that creature at end of combat.').
card_mana_cost('tolarian entrancer', ['1', 'U']).
card_cmc('tolarian entrancer', 2).
card_layout('tolarian entrancer', 'normal').
card_power('tolarian entrancer', 1).
card_toughness('tolarian entrancer', 1).
card_reserved('tolarian entrancer').

% Found in: TSP
card_name('tolarian sentinel', 'Tolarian Sentinel').
card_type('tolarian sentinel', 'Creature — Human Spellshaper').
card_types('tolarian sentinel', ['Creature']).
card_subtypes('tolarian sentinel', ['Human', 'Spellshaper']).
card_colors('tolarian sentinel', ['U']).
card_text('tolarian sentinel', 'Flying\n{U}, {T}, Discard a card: Return target permanent you control to its owner\'s hand.').
card_mana_cost('tolarian sentinel', ['3', 'U']).
card_cmc('tolarian sentinel', 4).
card_layout('tolarian sentinel', 'normal').
card_power('tolarian sentinel', 1).
card_toughness('tolarian sentinel', 3).

% Found in: WTH
card_name('tolarian serpent', 'Tolarian Serpent').
card_type('tolarian serpent', 'Creature — Serpent').
card_types('tolarian serpent', ['Creature']).
card_subtypes('tolarian serpent', ['Serpent']).
card_colors('tolarian serpent', ['U']).
card_text('tolarian serpent', 'At the beginning of your upkeep, put the top seven cards of your library into your graveyard.').
card_mana_cost('tolarian serpent', ['5', 'U', 'U']).
card_cmc('tolarian serpent', 7).
card_layout('tolarian serpent', 'normal').
card_power('tolarian serpent', 7).
card_toughness('tolarian serpent', 7).
card_reserved('tolarian serpent').

% Found in: 7ED, BTD, USG
card_name('tolarian winds', 'Tolarian Winds').
card_type('tolarian winds', 'Instant').
card_types('tolarian winds', ['Instant']).
card_subtypes('tolarian winds', []).
card_colors('tolarian winds', ['U']).
card_text('tolarian winds', 'Discard all the cards in your hand, then draw that many cards.').
card_mana_cost('tolarian winds', ['1', 'U']).
card_cmc('tolarian winds', 2).
card_layout('tolarian winds', 'normal').

% Found in: RAV
card_name('tolsimir wolfblood', 'Tolsimir Wolfblood').
card_type('tolsimir wolfblood', 'Legendary Creature — Elf Warrior').
card_types('tolsimir wolfblood', ['Creature']).
card_subtypes('tolsimir wolfblood', ['Elf', 'Warrior']).
card_supertypes('tolsimir wolfblood', ['Legendary']).
card_colors('tolsimir wolfblood', ['W', 'G']).
card_text('tolsimir wolfblood', 'Other green creatures you control get +1/+1.\nOther white creatures you control get +1/+1.\n{T}: Put a legendary 2/2 green and white Wolf creature token named Voja onto the battlefield.').
card_mana_cost('tolsimir wolfblood', ['4', 'G', 'W']).
card_cmc('tolsimir wolfblood', 6).
card_layout('tolsimir wolfblood', 'normal').
card_power('tolsimir wolfblood', 3).
card_toughness('tolsimir wolfblood', 4).

% Found in: WWK
card_name('tomb hex', 'Tomb Hex').
card_type('tomb hex', 'Instant').
card_types('tomb hex', ['Instant']).
card_subtypes('tomb hex', []).
card_colors('tomb hex', ['B']).
card_text('tomb hex', 'Target creature gets -2/-2 until end of turn.\nLandfall — If you had a land enter the battlefield under your control this turn, that creature gets -4/-4 until end of turn instead.').
card_mana_cost('tomb hex', ['2', 'B']).
card_cmc('tomb hex', 3).
card_layout('tomb hex', 'normal').

% Found in: KTK
card_name('tomb of the spirit dragon', 'Tomb of the Spirit Dragon').
card_type('tomb of the spirit dragon', 'Land').
card_types('tomb of the spirit dragon', ['Land']).
card_subtypes('tomb of the spirit dragon', []).
card_colors('tomb of the spirit dragon', []).
card_text('tomb of the spirit dragon', '{T}: Add {1} to your mana pool.\n{2}, {T}: You gain 1 life for each colorless creature you control.').
card_layout('tomb of the spirit dragon', 'normal').

% Found in: SOK
card_name('tomb of urami', 'Tomb of Urami').
card_type('tomb of urami', 'Legendary Land').
card_types('tomb of urami', ['Land']).
card_subtypes('tomb of urami', []).
card_supertypes('tomb of urami', ['Legendary']).
card_colors('tomb of urami', []).
card_text('tomb of urami', '{T}: Add {B} to your mana pool. Tomb of Urami deals 1 damage to you if you don\'t control an Ogre.\n{2}{B}{B}, {T}, Sacrifice all lands you control: Put a legendary 5/5 black Demon Spirit creature token with flying named Urami onto the battlefield.').
card_layout('tomb of urami', 'normal').

% Found in: ODY
card_name('tombfire', 'Tombfire').
card_type('tombfire', 'Sorcery').
card_types('tombfire', ['Sorcery']).
card_subtypes('tombfire', []).
card_colors('tombfire', ['B']).
card_text('tombfire', 'Target player exiles all cards with flashback from his or her graveyard.').
card_mana_cost('tombfire', ['B']).
card_cmc('tombfire', 1).
card_layout('tombfire', 'normal').

% Found in: FUT, MMA
card_name('tombstalker', 'Tombstalker').
card_type('tombstalker', 'Creature — Demon').
card_types('tombstalker', ['Creature']).
card_subtypes('tombstalker', ['Demon']).
card_colors('tombstalker', ['B']).
card_text('tombstalker', 'Flying\nDelve (Each card you exile from your graveyard while casting this spell pays for {1}.)').
card_mana_cost('tombstalker', ['6', 'B', 'B']).
card_cmc('tombstalker', 8).
card_layout('tombstalker', 'normal').
card_power('tombstalker', 5).
card_toughness('tombstalker', 5).

% Found in: MIR
card_name('tombstone stairwell', 'Tombstone Stairwell').
card_type('tombstone stairwell', 'World Enchantment').
card_types('tombstone stairwell', ['Enchantment']).
card_subtypes('tombstone stairwell', []).
card_supertypes('tombstone stairwell', ['World']).
card_colors('tombstone stairwell', ['B']).
card_text('tombstone stairwell', 'Cumulative upkeep {1}{B} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of each upkeep, if Tombstone Stairwell is on the battlefield, each player puts a 2/2 black Zombie creature token with haste named Tombspawn onto the battlefield for each creature card in his or her graveyard.\nAt the beginning of each end step or when Tombstone Stairwell leaves the battlefield, destroy all tokens put onto the battlefield with Tombstone Stairwell. They can\'t be regenerated.').
card_mana_cost('tombstone stairwell', ['2', 'B', 'B']).
card_cmc('tombstone stairwell', 4).
card_layout('tombstone stairwell', 'normal').
card_reserved('tombstone stairwell').

% Found in: M10, M11, M14
card_name('tome scour', 'Tome Scour').
card_type('tome scour', 'Sorcery').
card_types('tome scour', ['Sorcery']).
card_subtypes('tome scour', []).
card_colors('tome scour', ['U']).
card_text('tome scour', 'Target player puts the top five cards of his or her library into his or her graveyard.').
card_mana_cost('tome scour', ['U']).
card_cmc('tome scour', 1).
card_layout('tome scour', 'normal').

% Found in: BOK
card_name('tomorrow, azami\'s familiar', 'Tomorrow, Azami\'s Familiar').
card_type('tomorrow, azami\'s familiar', 'Legendary Creature — Spirit').
card_types('tomorrow, azami\'s familiar', ['Creature']).
card_subtypes('tomorrow, azami\'s familiar', ['Spirit']).
card_supertypes('tomorrow, azami\'s familiar', ['Legendary']).
card_colors('tomorrow, azami\'s familiar', ['U']).
card_text('tomorrow, azami\'s familiar', 'If you would draw a card, look at the top three cards of your library instead. Put one of those cards into your hand and the rest on the bottom of your library in any order.').
card_mana_cost('tomorrow, azami\'s familiar', ['5', 'U']).
card_cmc('tomorrow, azami\'s familiar', 6).
card_layout('tomorrow, azami\'s familiar', 'normal').
card_power('tomorrow, azami\'s familiar', 1).
card_toughness('tomorrow, azami\'s familiar', 5).

% Found in: CHK
card_name('tomoya the revealer', 'Tomoya the Revealer').
card_type('tomoya the revealer', 'Legendary Creature — Human Wizard').
card_types('tomoya the revealer', ['Creature']).
card_subtypes('tomoya the revealer', ['Human', 'Wizard']).
card_supertypes('tomoya the revealer', ['Legendary']).
card_colors('tomoya the revealer', ['U']).
card_text('tomoya the revealer', '{3}{U}{U}, {T}: Target player draws X cards, where X is the number of cards in your hand.').
card_mana_cost('tomoya the revealer', ['1', 'U']).
card_cmc('tomoya the revealer', 2).
card_layout('tomoya the revealer', 'flip').
card_power('tomoya the revealer', 2).
card_toughness('tomoya the revealer', 3).

% Found in: MMQ
card_name('tonic peddler', 'Tonic Peddler').
card_type('tonic peddler', 'Creature — Human Spellshaper').
card_types('tonic peddler', ['Creature']).
card_subtypes('tonic peddler', ['Human', 'Spellshaper']).
card_colors('tonic peddler', ['W']).
card_text('tonic peddler', '{W}, {T}, Discard a card: Target player gains 3 life.').
card_mana_cost('tonic peddler', ['1', 'W']).
card_cmc('tonic peddler', 2).
card_layout('tonic peddler', 'normal').
card_power('tonic peddler', 1).
card_toughness('tonic peddler', 1).

% Found in: C13, TMP
card_name('tooth and claw', 'Tooth and Claw').
card_type('tooth and claw', 'Enchantment').
card_types('tooth and claw', ['Enchantment']).
card_subtypes('tooth and claw', []).
card_colors('tooth and claw', ['R']).
card_text('tooth and claw', 'Sacrifice two creatures: Put a 3/1 red Beast creature token named Carnivore onto the battlefield.').
card_mana_cost('tooth and claw', ['3', 'R']).
card_cmc('tooth and claw', 4).
card_layout('tooth and claw', 'normal').

% Found in: MMA, MRD
card_name('tooth and nail', 'Tooth and Nail').
card_type('tooth and nail', 'Sorcery').
card_types('tooth and nail', ['Sorcery']).
card_subtypes('tooth and nail', []).
card_colors('tooth and nail', ['G']).
card_text('tooth and nail', 'Choose one —\n• Search your library for up to two creature cards, reveal them, put them into your hand, then shuffle your library.\n• Put up to two creature cards from your hand onto the battlefield.\nEntwine {2} (Choose both if you pay the entwine cost.)').
card_mana_cost('tooth and nail', ['5', 'G', 'G']).
card_cmc('tooth and nail', 7).
card_layout('tooth and nail', 'normal').

% Found in: MRD
card_name('tooth of chiss-goria', 'Tooth of Chiss-Goria').
card_type('tooth of chiss-goria', 'Artifact').
card_types('tooth of chiss-goria', ['Artifact']).
card_subtypes('tooth of chiss-goria', []).
card_colors('tooth of chiss-goria', []).
card_text('tooth of chiss-goria', 'Flash\nAffinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\n{T}: Target creature gets +1/+0 until end of turn.').
card_mana_cost('tooth of chiss-goria', ['3']).
card_cmc('tooth of chiss-goria', 3).
card_layout('tooth of chiss-goria', 'normal').

% Found in: MMQ
card_name('tooth of ramos', 'Tooth of Ramos').
card_type('tooth of ramos', 'Artifact').
card_types('tooth of ramos', ['Artifact']).
card_subtypes('tooth of ramos', []).
card_colors('tooth of ramos', []).
card_text('tooth of ramos', '{T}: Add {W} to your mana pool.\nSacrifice Tooth of Ramos: Add {W} to your mana pool.').
card_mana_cost('tooth of ramos', ['3']).
card_cmc('tooth of ramos', 3).
card_layout('tooth of ramos', 'normal').

% Found in: ARC
card_name('tooth, claw, and tail', 'Tooth, Claw, and Tail').
card_type('tooth, claw, and tail', 'Scheme').
card_types('tooth, claw, and tail', ['Scheme']).
card_subtypes('tooth, claw, and tail', []).
card_colors('tooth, claw, and tail', []).
card_text('tooth, claw, and tail', 'When you set this scheme in motion, destroy up to three target nonland permanents.').
card_layout('tooth, claw, and tail', 'scheme').

% Found in: ALA
card_name('topan ascetic', 'Topan Ascetic').
card_type('topan ascetic', 'Creature — Human Monk').
card_types('topan ascetic', ['Creature']).
card_subtypes('topan ascetic', ['Human', 'Monk']).
card_colors('topan ascetic', ['G']).
card_text('topan ascetic', 'Tap an untapped creature you control: Topan Ascetic gets +1/+1 until end of turn.').
card_mana_cost('topan ascetic', ['2', 'G']).
card_cmc('topan ascetic', 3).
card_layout('topan ascetic', 'normal').
card_power('topan ascetic', 2).
card_toughness('topan ascetic', 2).

% Found in: ORI
card_name('topan freeblade', 'Topan Freeblade').
card_type('topan freeblade', 'Creature — Human Soldier').
card_types('topan freeblade', ['Creature']).
card_subtypes('topan freeblade', ['Human', 'Soldier']).
card_colors('topan freeblade', ['W']).
card_text('topan freeblade', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)').
card_mana_cost('topan freeblade', ['1', 'W']).
card_cmc('topan freeblade', 2).
card_layout('topan freeblade', 'normal').
card_power('topan freeblade', 2).
card_toughness('topan freeblade', 2).

% Found in: NMS
card_name('topple', 'Topple').
card_type('topple', 'Sorcery').
card_types('topple', ['Sorcery']).
card_subtypes('topple', []).
card_colors('topple', ['W']).
card_text('topple', 'Exile target creature with the greatest power among creatures on the battlefield. (If two or more creatures are tied for greatest power, target any one of them.)').
card_mana_cost('topple', ['2', 'W']).
card_cmc('topple', 3).
card_layout('topple', 'normal').

% Found in: UNH
card_name('topsy turvy', 'Topsy Turvy').
card_type('topsy turvy', 'Enchantment').
card_types('topsy turvy', ['Enchantment']).
card_subtypes('topsy turvy', []).
card_colors('topsy turvy', ['U']).
card_text('topsy turvy', 'The phases of each player\'s turn are reversed. (The phases are, in reverse order, end, postcombat main, combat, precombat main, and beginning.)\nIf there are more than two players in the game, the turn order is reversed.').
card_mana_cost('topsy turvy', ['2', 'U']).
card_cmc('topsy turvy', 3).
card_layout('topsy turvy', 'normal').

% Found in: ICE
card_name('tor giant', 'Tor Giant').
card_type('tor giant', 'Creature — Giant').
card_types('tor giant', ['Creature']).
card_subtypes('tor giant', ['Giant']).
card_colors('tor giant', ['R']).
card_text('tor giant', '').
card_mana_cost('tor giant', ['3', 'R']).
card_cmc('tor giant', 4).
card_layout('tor giant', 'normal').
card_power('tor giant', 3).
card_toughness('tor giant', 3).

% Found in: CHR, LEG, ME3
card_name('tor wauki', 'Tor Wauki').
card_type('tor wauki', 'Legendary Creature — Human Archer').
card_types('tor wauki', ['Creature']).
card_subtypes('tor wauki', ['Human', 'Archer']).
card_supertypes('tor wauki', ['Legendary']).
card_colors('tor wauki', ['B', 'R']).
card_text('tor wauki', '{T}: Tor Wauki deals 2 damage to target attacking or blocking creature.').
card_mana_cost('tor wauki', ['2', 'B', 'B', 'R']).
card_cmc('tor wauki', 5).
card_layout('tor wauki', 'normal').
card_power('tor wauki', 3).
card_toughness('tor wauki', 3).

% Found in: GPT
card_name('torch drake', 'Torch Drake').
card_type('torch drake', 'Creature — Drake').
card_types('torch drake', ['Creature']).
card_subtypes('torch drake', ['Drake']).
card_colors('torch drake', ['U']).
card_text('torch drake', 'Flying\n{1}{R}: Torch Drake gets +1/+0 until end of turn.').
card_mana_cost('torch drake', ['3', 'U']).
card_cmc('torch drake', 4).
card_layout('torch drake', 'normal').
card_power('torch drake', 2).
card_toughness('torch drake', 2).

% Found in: CNS, DKA, M13, M15
card_name('torch fiend', 'Torch Fiend').
card_type('torch fiend', 'Creature — Devil').
card_types('torch fiend', ['Creature']).
card_subtypes('torch fiend', ['Devil']).
card_colors('torch fiend', ['R']).
card_text('torch fiend', '{R}, Sacrifice Torch Fiend: Destroy target artifact.').
card_mana_cost('torch fiend', ['1', 'R']).
card_cmc('torch fiend', 2).
card_layout('torch fiend', 'normal').
card_power('torch fiend', 2).
card_toughness('torch fiend', 1).

% Found in: DDP, ZEN
card_name('torch slinger', 'Torch Slinger').
card_type('torch slinger', 'Creature — Goblin Shaman').
card_types('torch slinger', ['Creature']).
card_subtypes('torch slinger', ['Goblin', 'Shaman']).
card_colors('torch slinger', ['R']).
card_text('torch slinger', 'Kicker {1}{R} (You may pay an additional {1}{R} as you cast this spell.)\nWhen Torch Slinger enters the battlefield, if it was kicked, it deals 2 damage to target creature.').
card_mana_cost('torch slinger', ['2', 'R']).
card_cmc('torch slinger', 3).
card_layout('torch slinger', 'normal').
card_power('torch slinger', 2).
card_toughness('torch slinger', 2).

% Found in: USG
card_name('torch song', 'Torch Song').
card_type('torch song', 'Enchantment').
card_types('torch song', ['Enchantment']).
card_subtypes('torch song', []).
card_colors('torch song', ['R']).
card_text('torch song', 'At the beginning of your upkeep, you may put a verse counter on Torch Song.\n{2}{R}, Sacrifice Torch Song: Torch Song deals X damage to target creature or player, where X is the number of verse counters on Torch Song.').
card_mana_cost('torch song', ['2', 'R']).
card_cmc('torch song', 3).
card_layout('torch song', 'normal').

% Found in: DDI, PLC
card_name('torchling', 'Torchling').
card_type('torchling', 'Creature — Shapeshifter').
card_types('torchling', ['Creature']).
card_subtypes('torchling', ['Shapeshifter']).
card_colors('torchling', ['R']).
card_text('torchling', '{R}: Untap Torchling.\n{R}: Target creature blocks Torchling this turn if able.\n{R}: Change the target of target spell that targets only Torchling.\n{1}: Torchling gets +1/-1 until end of turn.\n{1}: Torchling gets -1/+1 until end of turn.').
card_mana_cost('torchling', ['3', 'R', 'R']).
card_cmc('torchling', 5).
card_layout('torchling', 'normal').
card_power('torchling', 3).
card_toughness('torchling', 3).

% Found in: SOK
card_name('torii watchward', 'Torii Watchward').
card_type('torii watchward', 'Creature — Spirit').
card_types('torii watchward', ['Creature']).
card_subtypes('torii watchward', ['Spirit']).
card_colors('torii watchward', ['W']).
card_text('torii watchward', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nSoulshift 4 (When this creature dies, you may return target Spirit card with converted mana cost 4 or less from your graveyard to your hand.)').
card_mana_cost('torii watchward', ['4', 'W']).
card_cmc('torii watchward', 5).
card_layout('torii watchward', 'normal').
card_power('torii watchward', 3).
card_toughness('torii watchward', 3).

% Found in: STH
card_name('torment', 'Torment').
card_type('torment', 'Enchantment — Aura').
card_types('torment', ['Enchantment']).
card_subtypes('torment', ['Aura']).
card_colors('torment', ['B']).
card_text('torment', 'Enchant creature\nEnchanted creature gets -3/-0.').
card_mana_cost('torment', ['1', 'B']).
card_cmc('torment', 2).
card_layout('torment', 'normal').

% Found in: UDS
card_name('tormented angel', 'Tormented Angel').
card_type('tormented angel', 'Creature — Angel').
card_types('tormented angel', ['Creature']).
card_subtypes('tormented angel', ['Angel']).
card_colors('tormented angel', ['W']).
card_text('tormented angel', 'Flying').
card_mana_cost('tormented angel', ['3', 'W']).
card_cmc('tormented angel', 4).
card_layout('tormented angel', 'normal').
card_power('tormented angel', 1).
card_toughness('tormented angel', 5).

% Found in: THS, pFNM
card_name('tormented hero', 'Tormented Hero').
card_type('tormented hero', 'Creature — Human Warrior').
card_types('tormented hero', ['Creature']).
card_subtypes('tormented hero', ['Human', 'Warrior']).
card_colors('tormented hero', ['B']).
card_text('tormented hero', 'Tormented Hero enters the battlefield tapped.\nHeroic — Whenever you cast a spell that targets Tormented Hero, each opponent loses 1 life. You gain life equal to the life lost this way.').
card_mana_cost('tormented hero', ['B']).
card_cmc('tormented hero', 1).
card_layout('tormented hero', 'normal').
card_power('tormented hero', 2).
card_toughness('tormented hero', 1).

% Found in: ISD
card_name('tormented pariah', 'Tormented Pariah').
card_type('tormented pariah', 'Creature — Human Warrior Werewolf').
card_types('tormented pariah', ['Creature']).
card_subtypes('tormented pariah', ['Human', 'Warrior', 'Werewolf']).
card_colors('tormented pariah', ['R']).
card_text('tormented pariah', 'At the beginning of each upkeep, if no spells were cast last turn, transform Tormented Pariah.').
card_mana_cost('tormented pariah', ['3', 'R']).
card_cmc('tormented pariah', 4).
card_layout('tormented pariah', 'double-faced').
card_power('tormented pariah', 3).
card_toughness('tormented pariah', 2).
card_sides('tormented pariah', 'rampaging werewolf').

% Found in: M12, M13, PC2, pWPN
card_name('tormented soul', 'Tormented Soul').
card_type('tormented soul', 'Creature — Spirit').
card_types('tormented soul', ['Creature']).
card_subtypes('tormented soul', ['Spirit']).
card_colors('tormented soul', ['B']).
card_text('tormented soul', 'Tormented Soul can\'t block and can\'t be blocked.').
card_mana_cost('tormented soul', ['B']).
card_cmc('tormented soul', 1).
card_layout('tormented soul', 'normal').
card_power('tormented soul', 1).
card_toughness('tormented soul', 1).

% Found in: JOU, ORI
card_name('tormented thoughts', 'Tormented Thoughts').
card_type('tormented thoughts', 'Sorcery').
card_types('tormented thoughts', ['Sorcery']).
card_subtypes('tormented thoughts', []).
card_colors('tormented thoughts', ['B']).
card_text('tormented thoughts', 'As an additional cost to cast Tormented Thoughts, sacrifice a creature.\nTarget player discards a number of cards equal to the sacrificed creature\'s power.').
card_mana_cost('tormented thoughts', ['2', 'B']).
card_cmc('tormented thoughts', 3).
card_layout('tormented thoughts', 'normal').

% Found in: DTK, KTK
card_name('tormenting voice', 'Tormenting Voice').
card_type('tormenting voice', 'Sorcery').
card_types('tormenting voice', ['Sorcery']).
card_subtypes('tormenting voice', []).
card_colors('tormenting voice', ['R']).
card_text('tormenting voice', 'As an additional cost to cast Tormenting Voice, discard a card.\nDraw two cards.').
card_mana_cost('tormenting voice', ['1', 'R']).
card_cmc('tormenting voice', 2).
card_layout('tormenting voice', 'normal').

% Found in: NPH
card_name('tormentor exarch', 'Tormentor Exarch').
card_type('tormentor exarch', 'Creature — Cleric').
card_types('tormentor exarch', ['Creature']).
card_subtypes('tormentor exarch', ['Cleric']).
card_colors('tormentor exarch', ['R']).
card_text('tormentor exarch', 'When Tormentor Exarch enters the battlefield, choose one —\n• Target creature gets +2/+0 until end of turn.\n• Target creature gets -0/-2 until end of turn.').
card_mana_cost('tormentor exarch', ['3', 'R']).
card_cmc('tormentor exarch', 4).
card_layout('tormentor exarch', 'normal').
card_power('tormentor exarch', 2).
card_toughness('tormentor exarch', 2).

% Found in: AVR
card_name('tormentor\'s trident', 'Tormentor\'s Trident').
card_type('tormentor\'s trident', 'Artifact — Equipment').
card_types('tormentor\'s trident', ['Artifact']).
card_subtypes('tormentor\'s trident', ['Equipment']).
card_colors('tormentor\'s trident', []).
card_text('tormentor\'s trident', 'Equipped creature gets +3/+0 and attacks each turn if able.\nEquip {3}').
card_mana_cost('tormentor\'s trident', ['2']).
card_cmc('tormentor\'s trident', 2).
card_layout('tormentor\'s trident', 'normal').

% Found in: C14, CHR, DRK, M13, M15, TSB, pFNM
card_name('tormod\'s crypt', 'Tormod\'s Crypt').
card_type('tormod\'s crypt', 'Artifact').
card_types('tormod\'s crypt', ['Artifact']).
card_subtypes('tormod\'s crypt', []).
card_colors('tormod\'s crypt', []).
card_text('tormod\'s crypt', '{T}, Sacrifice Tormod\'s Crypt: Exile all cards from target player\'s graveyard.').
card_mana_cost('tormod\'s crypt', ['0']).
card_cmc('tormod\'s crypt', 0).
card_layout('tormod\'s crypt', 'normal').

% Found in: ALL, MED
card_name('tornado', 'Tornado').
card_type('tornado', 'Enchantment').
card_types('tornado', ['Enchantment']).
card_subtypes('tornado', []).
card_colors('tornado', ['G']).
card_text('tornado', 'Cumulative upkeep {G} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\n{2}{G}, Pay 3 life for each velocity counter on Tornado: Destroy target permanent and put a velocity counter on Tornado. Activate this ability only once each turn.').
card_mana_cost('tornado', ['4', 'G']).
card_cmc('tornado', 5).
card_layout('tornado', 'normal').
card_reserved('tornado').

% Found in: 5DN, C14, HOP
card_name('tornado elemental', 'Tornado Elemental').
card_type('tornado elemental', 'Creature — Elemental').
card_types('tornado elemental', ['Creature']).
card_subtypes('tornado elemental', ['Elemental']).
card_colors('tornado elemental', ['G']).
card_text('tornado elemental', 'When Tornado Elemental enters the battlefield, it deals 6 damage to each creature with flying.\nYou may have Tornado Elemental assign its combat damage as though it weren\'t blocked.').
card_mana_cost('tornado elemental', ['5', 'G', 'G']).
card_cmc('tornado elemental', 7).
card_layout('tornado elemental', 'normal').
card_power('tornado elemental', 6).
card_toughness('tornado elemental', 6).

% Found in: RAV
card_name('torpid moloch', 'Torpid Moloch').
card_type('torpid moloch', 'Creature — Lizard').
card_types('torpid moloch', ['Creature']).
card_subtypes('torpid moloch', ['Lizard']).
card_colors('torpid moloch', ['R']).
card_text('torpid moloch', 'Defender (This creature can\'t attack.)\nSacrifice three lands: Torpid Moloch loses defender until end of turn.').
card_mana_cost('torpid moloch', ['R']).
card_cmc('torpid moloch', 1).
card_layout('torpid moloch', 'normal').
card_power('torpid moloch', 3).
card_toughness('torpid moloch', 2).

% Found in: SHM
card_name('torpor dust', 'Torpor Dust').
card_type('torpor dust', 'Enchantment — Aura').
card_types('torpor dust', ['Enchantment']).
card_subtypes('torpor dust', ['Aura']).
card_colors('torpor dust', ['U', 'B']).
card_text('torpor dust', 'Flash\nEnchant creature\nEnchanted creature gets -3/-0.').
card_mana_cost('torpor dust', ['2', 'U/B']).
card_cmc('torpor dust', 3).
card_layout('torpor dust', 'normal').

% Found in: NPH
card_name('torpor orb', 'Torpor Orb').
card_type('torpor orb', 'Artifact').
card_types('torpor orb', ['Artifact']).
card_subtypes('torpor orb', []).
card_colors('torpor orb', []).
card_text('torpor orb', 'Creatures entering the battlefield don\'t cause abilities to trigger.').
card_mana_cost('torpor orb', ['2']).
card_cmc('torpor orb', 2).
card_layout('torpor orb', 'normal').

% Found in: FRF
card_name('torrent elemental', 'Torrent Elemental').
card_type('torrent elemental', 'Creature — Elemental').
card_types('torrent elemental', ['Creature']).
card_subtypes('torrent elemental', ['Elemental']).
card_colors('torrent elemental', ['U']).
card_text('torrent elemental', 'Flying\nWhenever Torrent Elemental attacks, tap all creatures defending player controls.\n{3}{B/G}{B/G}: Put Torrent Elemental from exile onto the battlefield tapped. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('torrent elemental', ['4', 'U']).
card_cmc('torrent elemental', 5).
card_layout('torrent elemental', 'normal').
card_power('torrent elemental', 3).
card_toughness('torrent elemental', 5).

% Found in: SCG
card_name('torrent of fire', 'Torrent of Fire').
card_type('torrent of fire', 'Sorcery').
card_types('torrent of fire', ['Sorcery']).
card_subtypes('torrent of fire', []).
card_colors('torrent of fire', ['R']).
card_text('torrent of fire', 'Torrent of Fire deals damage equal to the highest converted mana cost among permanents you control to target creature or player.').
card_mana_cost('torrent of fire', ['3', 'R', 'R']).
card_cmc('torrent of fire', 5).
card_layout('torrent of fire', 'normal').

% Found in: MIR
card_name('torrent of lava', 'Torrent of Lava').
card_type('torrent of lava', 'Sorcery').
card_types('torrent of lava', ['Sorcery']).
card_subtypes('torrent of lava', []).
card_colors('torrent of lava', ['R']).
card_text('torrent of lava', 'Torrent of Lava deals X damage to each creature without flying.\nAs long as Torrent of Lava is on the stack, each creature has \"{T}: Prevent the next 1 damage that would be dealt to this creature by Torrent of Lava this turn.\"').
card_mana_cost('torrent of lava', ['X', 'R', 'R']).
card_cmc('torrent of lava', 2).
card_layout('torrent of lava', 'normal').
card_reserved('torrent of lava').

% Found in: ARC, DDK, SHM
card_name('torrent of souls', 'Torrent of Souls').
card_type('torrent of souls', 'Sorcery').
card_types('torrent of souls', ['Sorcery']).
card_subtypes('torrent of souls', []).
card_colors('torrent of souls', ['B', 'R']).
card_text('torrent of souls', 'Return up to one target creature card from your graveyard to the battlefield if {B} was spent to cast Torrent of Souls. Creatures target player controls get +2/+0 and gain haste until end of turn if {R} was spent to cast Torrent of Souls. (Do both if {B}{R} was spent.)').
card_mana_cost('torrent of souls', ['4', 'B/R']).
card_cmc('torrent of souls', 5).
card_layout('torrent of souls', 'normal').

% Found in: BOK, MMA
card_name('torrent of stone', 'Torrent of Stone').
card_type('torrent of stone', 'Instant — Arcane').
card_types('torrent of stone', ['Instant']).
card_subtypes('torrent of stone', ['Arcane']).
card_colors('torrent of stone', ['R']).
card_text('torrent of stone', 'Torrent of Stone deals 4 damage to target creature.\nSplice onto Arcane—Sacrifice two Mountains. (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('torrent of stone', ['3', 'R']).
card_cmc('torrent of stone', 4).
card_layout('torrent of stone', 'normal').

% Found in: LEG, ME3
card_name('torsten von ursus', 'Torsten Von Ursus').
card_type('torsten von ursus', 'Legendary Creature — Human Soldier').
card_types('torsten von ursus', ['Creature']).
card_subtypes('torsten von ursus', ['Human', 'Soldier']).
card_supertypes('torsten von ursus', ['Legendary']).
card_colors('torsten von ursus', ['W', 'G']).
card_text('torsten von ursus', '').
card_mana_cost('torsten von ursus', ['3', 'G', 'G', 'W']).
card_cmc('torsten von ursus', 6).
card_layout('torsten von ursus', 'normal').
card_power('torsten von ursus', 5).
card_toughness('torsten von ursus', 5).

% Found in: ALA
card_name('tortoise formation', 'Tortoise Formation').
card_type('tortoise formation', 'Instant').
card_types('tortoise formation', ['Instant']).
card_subtypes('tortoise formation', []).
card_colors('tortoise formation', ['U']).
card_text('tortoise formation', 'Creatures you control gain shroud until end of turn. (They can\'t be the targets of spells or abilities.)').
card_mana_cost('tortoise formation', ['3', 'U']).
card_cmc('tortoise formation', 4).
card_layout('tortoise formation', 'normal').

% Found in: 5ED, HML, SHM
card_name('torture', 'Torture').
card_type('torture', 'Enchantment — Aura').
card_types('torture', ['Enchantment']).
card_subtypes('torture', ['Aura']).
card_colors('torture', ['B']).
card_text('torture', 'Enchant creature\n{1}{B}: Put a -1/-1 counter on enchanted creature.').
card_mana_cost('torture', ['B']).
card_cmc('torture', 1).
card_layout('torture', 'normal').

% Found in: TMP
card_name('torture chamber', 'Torture Chamber').
card_type('torture chamber', 'Artifact').
card_types('torture chamber', ['Artifact']).
card_subtypes('torture chamber', []).
card_colors('torture chamber', []).
card_text('torture chamber', 'At the beginning of your upkeep, put a pain counter on Torture Chamber.\nAt the beginning of your end step, Torture Chamber deals damage to you equal to the number of pain counters on it.\n{1}, {T}, Remove all pain counters from Torture Chamber: Torture Chamber deals damage to target creature equal to the number of pain counters removed this way.').
card_mana_cost('torture chamber', ['3']).
card_cmc('torture chamber', 3).
card_layout('torture chamber', 'normal').

% Found in: STH
card_name('tortured existence', 'Tortured Existence').
card_type('tortured existence', 'Enchantment').
card_types('tortured existence', ['Enchantment']).
card_subtypes('tortured existence', []).
card_colors('tortured existence', ['B']).
card_text('tortured existence', '{B}, Discard a creature card: Return target creature card from your graveyard to your hand.').
card_mana_cost('tortured existence', ['B']).
card_cmc('tortured existence', 1).
card_layout('tortured existence', 'normal').

% Found in: BOK
card_name('toshiro umezawa', 'Toshiro Umezawa').
card_type('toshiro umezawa', 'Legendary Creature — Human Samurai').
card_types('toshiro umezawa', ['Creature']).
card_subtypes('toshiro umezawa', ['Human', 'Samurai']).
card_supertypes('toshiro umezawa', ['Legendary']).
card_colors('toshiro umezawa', ['B']).
card_text('toshiro umezawa', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\nWhenever a creature an opponent controls dies, you may cast target instant card from your graveyard. If that card would be put into a graveyard this turn, exile it instead.').
card_mana_cost('toshiro umezawa', ['1', 'B', 'B']).
card_cmc('toshiro umezawa', 3).
card_layout('toshiro umezawa', 'normal').
card_power('toshiro umezawa', 2).
card_toughness('toshiro umezawa', 2).

% Found in: ICE
card_name('total war', 'Total War').
card_type('total war', 'Enchantment').
card_types('total war', ['Enchantment']).
card_subtypes('total war', []).
card_colors('total war', ['R']).
card_text('total war', 'Whenever a player attacks with one or more creatures, destroy all untapped non-Wall creatures that player controls that didn\'t attack, except for creatures the player hasn\'t controlled continuously since the beginning of the turn.').
card_mana_cost('total war', ['3', 'R']).
card_cmc('total war', 4).
card_layout('total war', 'normal').

% Found in: GTC
card_name('totally lost', 'Totally Lost').
card_type('totally lost', 'Instant').
card_types('totally lost', ['Instant']).
card_subtypes('totally lost', []).
card_colors('totally lost', ['U']).
card_text('totally lost', 'Put target nonland permanent on top of its owner\'s library.').
card_mana_cost('totally lost', ['4', 'U']).
card_cmc('totally lost', 5).
card_layout('totally lost', 'normal').

% Found in: LGN
card_name('totem speaker', 'Totem Speaker').
card_type('totem speaker', 'Creature — Elf Druid').
card_types('totem speaker', ['Creature']).
card_subtypes('totem speaker', ['Elf', 'Druid']).
card_colors('totem speaker', ['G']).
card_text('totem speaker', 'Whenever a Beast enters the battlefield, you may gain 3 life.').
card_mana_cost('totem speaker', ['4', 'G']).
card_cmc('totem speaker', 5).
card_layout('totem speaker', 'normal').
card_power('totem speaker', 3).
card_toughness('totem speaker', 3).

% Found in: ORI, ROE
card_name('totem-guide hartebeest', 'Totem-Guide Hartebeest').
card_type('totem-guide hartebeest', 'Creature — Antelope').
card_types('totem-guide hartebeest', ['Creature']).
card_subtypes('totem-guide hartebeest', ['Antelope']).
card_colors('totem-guide hartebeest', ['W']).
card_text('totem-guide hartebeest', 'When Totem-Guide Hartebeest enters the battlefield, you may search your library for an Aura card, reveal it, put it into your hand, then shuffle your library.').
card_mana_cost('totem-guide hartebeest', ['4', 'W']).
card_cmc('totem-guide hartebeest', 5).
card_layout('totem-guide hartebeest', 'normal').
card_power('totem-guide hartebeest', 2).
card_toughness('totem-guide hartebeest', 5).

% Found in: UNH
card_name('touch and go', 'Touch and Go').
card_type('touch and go', 'Sorcery').
card_types('touch and go', ['Sorcery']).
card_subtypes('touch and go', []).
card_colors('touch and go', ['R']).
card_text('touch and go', 'Destroy target land.\nGotcha Whenever an opponent touches his or her face, you may say \"Gotcha\" If you do, return Touch and Go from your graveyard to your hand.').
card_mana_cost('touch and go', ['3', 'R']).
card_cmc('touch and go', 4).
card_layout('touch and go', 'normal').

% Found in: PO2, POR, S99
card_name('touch of brilliance', 'Touch of Brilliance').
card_type('touch of brilliance', 'Sorcery').
card_types('touch of brilliance', ['Sorcery']).
card_subtypes('touch of brilliance', []).
card_colors('touch of brilliance', ['U']).
card_text('touch of brilliance', 'Draw two cards.').
card_mana_cost('touch of brilliance', ['3', 'U']).
card_cmc('touch of brilliance', 4).
card_layout('touch of brilliance', 'normal').

% Found in: LEG
card_name('touch of darkness', 'Touch of Darkness').
card_type('touch of darkness', 'Instant').
card_types('touch of darkness', ['Instant']).
card_subtypes('touch of darkness', []).
card_colors('touch of darkness', ['B']).
card_text('touch of darkness', 'Any number of target creatures become black until end of turn.').
card_mana_cost('touch of darkness', ['B']).
card_cmc('touch of darkness', 1).
card_layout('touch of darkness', 'normal').

% Found in: 5ED, ICE
card_name('touch of death', 'Touch of Death').
card_type('touch of death', 'Sorcery').
card_types('touch of death', ['Sorcery']).
card_subtypes('touch of death', []).
card_colors('touch of death', ['B']).
card_text('touch of death', 'Touch of Death deals 1 damage to target player. You gain 1 life.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('touch of death', ['2', 'B']).
card_cmc('touch of death', 3).
card_layout('touch of death', 'normal').

% Found in: ODY
card_name('touch of invisibility', 'Touch of Invisibility').
card_type('touch of invisibility', 'Sorcery').
card_types('touch of invisibility', ['Sorcery']).
card_subtypes('touch of invisibility', []).
card_colors('touch of invisibility', ['U']).
card_text('touch of invisibility', 'Target creature can\'t be blocked this turn.\nDraw a card.').
card_mana_cost('touch of invisibility', ['3', 'U']).
card_cmc('touch of invisibility', 4).
card_layout('touch of invisibility', 'normal').

% Found in: ORI
card_name('touch of moonglove', 'Touch of Moonglove').
card_type('touch of moonglove', 'Instant').
card_types('touch of moonglove', ['Instant']).
card_subtypes('touch of moonglove', []).
card_colors('touch of moonglove', ['B']).
card_text('touch of moonglove', 'Target creature you control gets +1/+0 and gains deathtouch until end of turn. Whenever a creature dealt damage by that creature dies this turn, its controller loses 2 life. (Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_mana_cost('touch of moonglove', ['B']).
card_cmc('touch of moonglove', 1).
card_layout('touch of moonglove', 'normal').

% Found in: M13
card_name('touch of the eternal', 'Touch of the Eternal').
card_type('touch of the eternal', 'Enchantment').
card_types('touch of the eternal', ['Enchantment']).
card_subtypes('touch of the eternal', []).
card_colors('touch of the eternal', ['W']).
card_text('touch of the eternal', 'At the beginning of your upkeep, count the number of permanents you control. Your life total becomes that number.').
card_mana_cost('touch of the eternal', ['5', 'W', 'W']).
card_cmc('touch of the eternal', 7).
card_layout('touch of the eternal', 'normal').

% Found in: BFZ
card_name('touch of the void', 'Touch of the Void').
card_type('touch of the void', 'Sorcery').
card_types('touch of the void', ['Sorcery']).
card_subtypes('touch of the void', []).
card_colors('touch of the void', []).
card_text('touch of the void', 'Devoid (This card has no color.)\nTouch of the Void deals 3 damage to target creature or player. If a creature dealt damage this way would die this turn, exile it instead.').
card_mana_cost('touch of the void', ['2', 'R']).
card_cmc('touch of the void', 3).
card_layout('touch of the void', 'normal').

% Found in: ICE
card_name('touch of vitae', 'Touch of Vitae').
card_type('touch of vitae', 'Instant').
card_types('touch of vitae', ['Instant']).
card_subtypes('touch of vitae', []).
card_colors('touch of vitae', ['G']).
card_text('touch of vitae', 'Until end of turn, target creature gains haste and \"{0}: Untap this creature. Activate this ability only once.\"\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('touch of vitae', ['2', 'G']).
card_cmc('touch of vitae', 3).
card_layout('touch of vitae', 'normal').

% Found in: WTH
card_name('touchstone', 'Touchstone').
card_type('touchstone', 'Artifact').
card_types('touchstone', ['Artifact']).
card_subtypes('touchstone', []).
card_colors('touchstone', []).
card_text('touchstone', '{T}: Tap target artifact you don\'t control.').
card_mana_cost('touchstone', ['2']).
card_cmc('touchstone', 2).
card_layout('touchstone', 'normal').

% Found in: FEM
card_name('tourach\'s chant', 'Tourach\'s Chant').
card_type('tourach\'s chant', 'Enchantment').
card_types('tourach\'s chant', ['Enchantment']).
card_subtypes('tourach\'s chant', []).
card_colors('tourach\'s chant', ['B']).
card_text('tourach\'s chant', 'At the beginning of your upkeep, sacrifice Tourach\'s Chant unless you pay {B}.\nWhenever a player puts a Forest onto the battlefield, Tourach\'s Chant deals 3 damage to that player unless he or she puts a -1/-1 counter on a creature he or she controls.').
card_mana_cost('tourach\'s chant', ['1', 'B', 'B']).
card_cmc('tourach\'s chant', 3).
card_layout('tourach\'s chant', 'normal').

% Found in: FEM
card_name('tourach\'s gate', 'Tourach\'s Gate').
card_type('tourach\'s gate', 'Enchantment — Aura').
card_types('tourach\'s gate', ['Enchantment']).
card_subtypes('tourach\'s gate', ['Aura']).
card_colors('tourach\'s gate', ['B']).
card_text('tourach\'s gate', 'Enchant land you control\nSacrifice a Thrull: Put three time counters on Tourach\'s Gate.\nAt the beginning of your upkeep, remove a time counter from Tourach\'s Gate. If there are no time counters on Tourach\'s Gate, sacrifice it.\nTap enchanted land: Attacking creatures you control get +2/-1 until end of turn. Activate this ability only if enchanted land is untapped.').
card_mana_cost('tourach\'s gate', ['1', 'B', 'B']).
card_cmc('tourach\'s gate', 3).
card_layout('tourach\'s gate', 'normal').
card_reserved('tourach\'s gate').

% Found in: DKA, pLPA
card_name('tovolar\'s magehunter', 'Tovolar\'s Magehunter').
card_type('tovolar\'s magehunter', 'Creature — Werewolf').
card_types('tovolar\'s magehunter', ['Creature']).
card_subtypes('tovolar\'s magehunter', ['Werewolf']).
card_colors('tovolar\'s magehunter', ['R']).
card_text('tovolar\'s magehunter', 'Whenever an opponent casts a spell, Tovolar\'s Magehunter deals 2 damage to that player.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Tovolar\'s Magehunter.').
card_layout('tovolar\'s magehunter', 'double-faced').
card_power('tovolar\'s magehunter', 5).
card_toughness('tovolar\'s magehunter', 5).

% Found in: SHM
card_name('tower above', 'Tower Above').
card_type('tower above', 'Sorcery').
card_types('tower above', ['Sorcery']).
card_subtypes('tower above', []).
card_colors('tower above', ['G']).
card_text('tower above', '({2/G} can be paid with any two mana or with {G}. This card\'s converted mana cost is 6.)\nUntil end of turn, target creature gets +4/+4 and gains trample, wither, and \"When this creature attacks, target creature blocks it this turn if able.\" (It deals damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('tower above', ['2/G', '2/G', '2/G']).
card_cmc('tower above', 6).
card_layout('tower above', 'normal').

% Found in: GTC
card_name('tower defense', 'Tower Defense').
card_type('tower defense', 'Instant').
card_types('tower defense', ['Instant']).
card_subtypes('tower defense', []).
card_colors('tower defense', ['G']).
card_text('tower defense', 'Creatures you control get +0/+5 and gain reach until end of turn.').
card_mana_cost('tower defense', ['1', 'G']).
card_cmc('tower defense', 2).
card_layout('tower defense', 'normal').

% Found in: INV, RTR
card_name('tower drake', 'Tower Drake').
card_type('tower drake', 'Creature — Drake').
card_types('tower drake', ['Creature']).
card_subtypes('tower drake', ['Drake']).
card_colors('tower drake', ['U']).
card_text('tower drake', 'Flying\n{W}: Tower Drake gets +0/+1 until end of turn.').
card_mana_cost('tower drake', ['2', 'U']).
card_cmc('tower drake', 3).
card_layout('tower drake', 'normal').
card_power('tower drake', 2).
card_toughness('tower drake', 1).

% Found in: ALA, C13
card_name('tower gargoyle', 'Tower Gargoyle').
card_type('tower gargoyle', 'Artifact Creature — Gargoyle').
card_types('tower gargoyle', ['Artifact', 'Creature']).
card_subtypes('tower gargoyle', ['Gargoyle']).
card_colors('tower gargoyle', ['W', 'U', 'B']).
card_text('tower gargoyle', 'Flying').
card_mana_cost('tower gargoyle', ['1', 'W', 'U', 'B']).
card_cmc('tower gargoyle', 4).
card_layout('tower gargoyle', 'normal').
card_power('tower gargoyle', 4).
card_toughness('tower gargoyle', 4).

% Found in: DKA, ORI
card_name('tower geist', 'Tower Geist').
card_type('tower geist', 'Creature — Spirit').
card_types('tower geist', ['Creature']).
card_subtypes('tower geist', ['Spirit']).
card_colors('tower geist', ['U']).
card_text('tower geist', 'Flying\nWhen Tower Geist enters the battlefield, look at the top two cards of your library. Put one of them into your hand and the other into your graveyard.').
card_mana_cost('tower geist', ['3', 'U']).
card_cmc('tower geist', 4).
card_layout('tower geist', 'normal').
card_power('tower geist', 2).
card_toughness('tower geist', 2).

% Found in: SOM
card_name('tower of calamities', 'Tower of Calamities').
card_type('tower of calamities', 'Artifact').
card_types('tower of calamities', ['Artifact']).
card_subtypes('tower of calamities', []).
card_colors('tower of calamities', []).
card_text('tower of calamities', '{8}, {T}: Tower of Calamities deals 12 damage to target creature.').
card_mana_cost('tower of calamities', ['4']).
card_cmc('tower of calamities', 4).
card_layout('tower of calamities', 'normal').

% Found in: MRD
card_name('tower of champions', 'Tower of Champions').
card_type('tower of champions', 'Artifact').
card_types('tower of champions', ['Artifact']).
card_subtypes('tower of champions', []).
card_colors('tower of champions', []).
card_text('tower of champions', '{8}, {T}: Target creature gets +6/+6 until end of turn.').
card_mana_cost('tower of champions', ['4']).
card_cmc('tower of champions', 4).
card_layout('tower of champions', 'normal').

% Found in: DRK
card_name('tower of coireall', 'Tower of Coireall').
card_type('tower of coireall', 'Artifact').
card_types('tower of coireall', ['Artifact']).
card_subtypes('tower of coireall', []).
card_colors('tower of coireall', []).
card_text('tower of coireall', '{T}: Target creature can\'t be blocked by Walls this turn.').
card_mana_cost('tower of coireall', ['2']).
card_cmc('tower of coireall', 2).
card_layout('tower of coireall', 'normal').

% Found in: MRD
card_name('tower of eons', 'Tower of Eons').
card_type('tower of eons', 'Artifact').
card_types('tower of eons', ['Artifact']).
card_subtypes('tower of eons', []).
card_colors('tower of eons', []).
card_text('tower of eons', '{8}, {T}: You gain 10 life.').
card_mana_cost('tower of eons', ['4']).
card_cmc('tower of eons', 4).
card_layout('tower of eons', 'normal').

% Found in: C13, MRD
card_name('tower of fortunes', 'Tower of Fortunes').
card_type('tower of fortunes', 'Artifact').
card_types('tower of fortunes', ['Artifact']).
card_subtypes('tower of fortunes', []).
card_colors('tower of fortunes', []).
card_text('tower of fortunes', '{8}, {T}: Draw four cards.').
card_mana_cost('tower of fortunes', ['4']).
card_cmc('tower of fortunes', 4).
card_layout('tower of fortunes', 'normal').

% Found in: MRD
card_name('tower of murmurs', 'Tower of Murmurs').
card_type('tower of murmurs', 'Artifact').
card_types('tower of murmurs', ['Artifact']).
card_subtypes('tower of murmurs', []).
card_colors('tower of murmurs', []).
card_text('tower of murmurs', '{8}, {T}: Target player puts the top eight cards of his or her library into his or her graveyard.').
card_mana_cost('tower of murmurs', ['4']).
card_cmc('tower of murmurs', 4).
card_layout('tower of murmurs', 'normal').

% Found in: MMQ
card_name('tower of the magistrate', 'Tower of the Magistrate').
card_type('tower of the magistrate', 'Land').
card_types('tower of the magistrate', ['Land']).
card_subtypes('tower of the magistrate', []).
card_colors('tower of the magistrate', []).
card_text('tower of the magistrate', '{T}: Add {1} to your mana pool.\n{1}, {T}: Target creature gains protection from artifacts until end of turn.').
card_layout('tower of the magistrate', 'normal').

% Found in: ONS
card_name('towering baloth', 'Towering Baloth').
card_type('towering baloth', 'Creature — Beast').
card_types('towering baloth', ['Creature']).
card_subtypes('towering baloth', ['Beast']).
card_colors('towering baloth', ['G']).
card_text('towering baloth', 'Morph {6}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('towering baloth', ['6', 'G', 'G']).
card_cmc('towering baloth', 8).
card_layout('towering baloth', 'normal').
card_power('towering baloth', 7).
card_toughness('towering baloth', 6).

% Found in: RTR
card_name('towering indrik', 'Towering Indrik').
card_type('towering indrik', 'Creature — Beast').
card_types('towering indrik', ['Creature']).
card_subtypes('towering indrik', ['Beast']).
card_colors('towering indrik', ['G']).
card_text('towering indrik', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('towering indrik', ['3', 'G']).
card_cmc('towering indrik', 4).
card_layout('towering indrik', 'normal').
card_power('towering indrik', 2).
card_toughness('towering indrik', 4).

% Found in: GTC
card_name('towering thunderfist', 'Towering Thunderfist').
card_type('towering thunderfist', 'Creature — Giant Soldier').
card_types('towering thunderfist', ['Creature']).
card_subtypes('towering thunderfist', ['Giant', 'Soldier']).
card_colors('towering thunderfist', ['R']).
card_text('towering thunderfist', '{W}: Towering Thunderfist gains vigilance until end of turn.').
card_mana_cost('towering thunderfist', ['4', 'R']).
card_cmc('towering thunderfist', 5).
card_layout('towering thunderfist', 'normal').
card_power('towering thunderfist', 4).
card_toughness('towering thunderfist', 4).

% Found in: PO2
card_name('town sentry', 'Town Sentry').
card_type('town sentry', 'Creature — Human Soldier').
card_types('town sentry', ['Creature']).
card_subtypes('town sentry', ['Human', 'Soldier']).
card_colors('town sentry', ['W']).
card_text('town sentry', 'Whenever Town Sentry blocks, it gets +0/+2 until end of turn.').
card_mana_cost('town sentry', ['2', 'W']).
card_cmc('town sentry', 3).
card_layout('town sentry', 'normal').
card_power('town sentry', 2).
card_toughness('town sentry', 2).

% Found in: C13
card_name('toxic deluge', 'Toxic Deluge').
card_type('toxic deluge', 'Sorcery').
card_types('toxic deluge', ['Sorcery']).
card_subtypes('toxic deluge', []).
card_colors('toxic deluge', ['B']).
card_text('toxic deluge', 'As an additional cost to cast Toxic Deluge, pay X life.\nAll creatures get -X/-X until end of turn.').
card_mana_cost('toxic deluge', ['2', 'B']).
card_cmc('toxic deluge', 3).
card_layout('toxic deluge', 'normal').

% Found in: CON
card_name('toxic iguanar', 'Toxic Iguanar').
card_type('toxic iguanar', 'Creature — Lizard').
card_types('toxic iguanar', ['Creature']).
card_subtypes('toxic iguanar', ['Lizard']).
card_colors('toxic iguanar', ['R']).
card_text('toxic iguanar', 'Toxic Iguanar has deathtouch as long as you control a green permanent. (Any amount of damage a creature with deathtouch deals to a creature is enough to destroy it.)').
card_mana_cost('toxic iguanar', ['R']).
card_cmc('toxic iguanar', 1).
card_layout('toxic iguanar', 'normal').
card_power('toxic iguanar', 1).
card_toughness('toxic iguanar', 1).

% Found in: NPH
card_name('toxic nim', 'Toxic Nim').
card_type('toxic nim', 'Creature — Zombie').
card_types('toxic nim', ['Creature']).
card_subtypes('toxic nim', ['Zombie']).
card_colors('toxic nim', ['B']).
card_text('toxic nim', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\n{B}: Regenerate Toxic Nim.').
card_mana_cost('toxic nim', ['4', 'B', 'B']).
card_cmc('toxic nim', 6).
card_layout('toxic nim', 'normal').
card_power('toxic nim', 4).
card_toughness('toxic nim', 1).

% Found in: JUD
card_name('toxic stench', 'Toxic Stench').
card_type('toxic stench', 'Instant').
card_types('toxic stench', ['Instant']).
card_subtypes('toxic stench', []).
card_colors('toxic stench', ['B']).
card_text('toxic stench', 'Target nonblack creature gets -1/-1 until end of turn.\nThreshold — If seven or more cards are in your graveyard, instead destroy that creature. It can\'t be regenerated.').
card_mana_cost('toxic stench', ['1', 'B']).
card_cmc('toxic stench', 2).
card_layout('toxic stench', 'normal').

% Found in: LGN
card_name('toxin sliver', 'Toxin Sliver').
card_type('toxin sliver', 'Creature — Sliver').
card_types('toxin sliver', ['Creature']).
card_subtypes('toxin sliver', ['Sliver']).
card_colors('toxin sliver', ['B']).
card_text('toxin sliver', 'Whenever a Sliver deals combat damage to a creature, destroy that creature. It can\'t be regenerated.').
card_mana_cost('toxin sliver', ['3', 'B']).
card_cmc('toxin sliver', 4).
card_layout('toxin sliver', 'normal').
card_power('toxin sliver', 3).
card_toughness('toxin sliver', 3).

% Found in: UNH
card_name('toy boat', 'Toy Boat').
card_type('toy boat', 'Artifact Creature — Ship').
card_types('toy boat', ['Artifact', 'Creature']).
card_subtypes('toy boat', ['Ship']).
card_colors('toy boat', []).
card_text('toy boat', 'Cumulative upkeep—Say \"Toy Boat\" quickly. (At the beginning of your upkeep, put an age counter on Toy Boat, then sacrifice it unless you say \"Toy Boat\" once for each age counter on it—without pausing between or fumbling it.)').
card_mana_cost('toy boat', ['3']).
card_cmc('toy boat', 3).
card_layout('toy boat', 'normal').
card_power('toy boat', 3).
card_toughness('toy boat', 3).

% Found in: MMQ
card_name('toymaker', 'Toymaker').
card_type('toymaker', 'Artifact Creature — Spellshaper').
card_types('toymaker', ['Artifact', 'Creature']).
card_subtypes('toymaker', ['Spellshaper']).
card_colors('toymaker', []).
card_text('toymaker', '{1}, {T}, Discard a card: Target noncreature artifact becomes an artifact creature with power and toughness each equal to its converted mana cost until end of turn. (It retains its abilities.)').
card_mana_cost('toymaker', ['2']).
card_cmc('toymaker', 2).
card_layout('toymaker', 'normal').
card_power('toymaker', 1).
card_toughness('toymaker', 1).

% Found in: ARB
card_name('trace of abundance', 'Trace of Abundance').
card_type('trace of abundance', 'Enchantment — Aura').
card_types('trace of abundance', ['Enchantment']).
card_subtypes('trace of abundance', ['Aura']).
card_colors('trace of abundance', ['W', 'R', 'G']).
card_text('trace of abundance', 'Enchant land\nEnchanted land has shroud. (It can\'t be the target of spells or abilities.)\nWhenever enchanted land is tapped for mana, its controller adds one mana of any color to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('trace of abundance', ['R/W', 'G']).
card_cmc('trace of abundance', 2).
card_layout('trace of abundance', 'normal').

% Found in: DRK, ME3
card_name('tracker', 'Tracker').
card_type('tracker', 'Creature — Human').
card_types('tracker', ['Creature']).
card_subtypes('tracker', ['Human']).
card_colors('tracker', ['G']).
card_text('tracker', '{G}{G}, {T}: Tracker deals damage equal to its power to target creature. That creature deals damage equal to its power to Tracker.').
card_mana_cost('tracker', ['2', 'G']).
card_cmc('tracker', 3).
card_layout('tracker', 'normal').
card_power('tracker', 2).
card_toughness('tracker', 2).
card_reserved('tracker').

% Found in: DKA
card_name('tracker\'s instincts', 'Tracker\'s Instincts').
card_type('tracker\'s instincts', 'Sorcery').
card_types('tracker\'s instincts', ['Sorcery']).
card_subtypes('tracker\'s instincts', []).
card_colors('tracker\'s instincts', ['G']).
card_text('tracker\'s instincts', 'Reveal the top four cards of your library. Put a creature card from among them into your hand and the rest into your graveyard.\nFlashback {2}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('tracker\'s instincts', ['1', 'G']).
card_cmc('tracker\'s instincts', 2).
card_layout('tracker\'s instincts', 'normal').

% Found in: HML
card_name('trade caravan', 'Trade Caravan').
card_type('trade caravan', 'Creature — Human Nomad').
card_types('trade caravan', ['Creature']).
card_subtypes('trade caravan', ['Human', 'Nomad']).
card_colors('trade caravan', ['W']).
card_text('trade caravan', 'At the beginning of your upkeep, put a currency counter on Trade Caravan.\nRemove two currency counters from Trade Caravan: Untap target basic land. Activate this ability only during an opponent\'s upkeep.').
card_mana_cost('trade caravan', ['W']).
card_cmc('trade caravan', 1).
card_layout('trade caravan', 'normal').
card_power('trade caravan', 1).
card_toughness('trade caravan', 1).

% Found in: 8ED, 9ED, MMQ
card_name('trade routes', 'Trade Routes').
card_type('trade routes', 'Enchantment').
card_types('trade routes', ['Enchantment']).
card_subtypes('trade routes', []).
card_colors('trade routes', ['U']).
card_text('trade routes', '{1}: Return target land you control to its owner\'s hand.\n{1}, Discard a land card: Draw a card.').
card_mana_cost('trade routes', ['1', 'U']).
card_cmc('trade routes', 2).
card_layout('trade routes', 'normal').

% Found in: CMD, ONS
card_name('trade secrets', 'Trade Secrets').
card_type('trade secrets', 'Sorcery').
card_types('trade secrets', ['Sorcery']).
card_subtypes('trade secrets', []).
card_colors('trade secrets', ['U']).
card_text('trade secrets', 'Target opponent draws two cards, then you draw up to four cards. That opponent may repeat this process as many times as he or she chooses.').
card_mana_cost('trade secrets', ['1', 'U', 'U']).
card_cmc('trade secrets', 3).
card_layout('trade secrets', 'normal').

% Found in: TMP, TPR, VMA, pJGP
card_name('tradewind rider', 'Tradewind Rider').
card_type('tradewind rider', 'Creature — Spirit').
card_types('tradewind rider', ['Creature']).
card_subtypes('tradewind rider', ['Spirit']).
card_colors('tradewind rider', ['U']).
card_text('tradewind rider', 'Flying\n{T}, Tap two untapped creatures you control: Return target permanent to its owner\'s hand.').
card_mana_cost('tradewind rider', ['3', 'U']).
card_cmc('tradewind rider', 4).
card_layout('tradewind rider', 'normal').
card_power('tradewind rider', 1).
card_toughness('tradewind rider', 4).

% Found in: VAN
card_name('tradewind rider avatar', 'Tradewind Rider Avatar').
card_type('tradewind rider avatar', 'Vanguard').
card_types('tradewind rider avatar', ['Vanguard']).
card_subtypes('tradewind rider avatar', []).
card_colors('tradewind rider avatar', []).
card_text('tradewind rider avatar', '{3}: Each player returns a permanent he or she controls to its owner\'s hand unless he or she pays 2 life.').
card_layout('tradewind rider avatar', 'vanguard').

% Found in: C14, M13, M14
card_name('trading post', 'Trading Post').
card_type('trading post', 'Artifact').
card_types('trading post', ['Artifact']).
card_subtypes('trading post', []).
card_colors('trading post', []).
card_text('trading post', '{1}, {T}, Discard a card: You gain 4 life.\n{1}, {T}, Pay 1 life: Put a 0/1 white Goat creature token onto the battlefield.\n{1}, {T}, Sacrifice a creature: Return target artifact card from your graveyard to your hand.\n{1}, {T}, Sacrifice an artifact: Draw a card.').
card_mana_cost('trading post', ['4']).
card_cmc('trading post', 4).
card_layout('trading post', 'normal').

% Found in: ORI
card_name('tragic arrogance', 'Tragic Arrogance').
card_type('tragic arrogance', 'Sorcery').
card_types('tragic arrogance', ['Sorcery']).
card_subtypes('tragic arrogance', []).
card_colors('tragic arrogance', ['W']).
card_text('tragic arrogance', 'For each player, you choose from among the permanents that player controls an artifact, a creature, an enchantment, and a planeswalker. Then each player sacrifices all other nonland permanents he or she controls.').
card_mana_cost('tragic arrogance', ['3', 'W', 'W']).
card_cmc('tragic arrogance', 5).
card_layout('tragic arrogance', 'normal').

% Found in: ULG
card_name('tragic poet', 'Tragic Poet').
card_type('tragic poet', 'Creature — Human').
card_types('tragic poet', ['Creature']).
card_subtypes('tragic poet', ['Human']).
card_colors('tragic poet', ['W']).
card_text('tragic poet', '{T}, Sacrifice Tragic Poet: Return target enchantment card from your graveyard to your hand.').
card_mana_cost('tragic poet', ['W']).
card_cmc('tragic poet', 1).
card_layout('tragic poet', 'normal').
card_power('tragic poet', 1).
card_toughness('tragic poet', 1).

% Found in: C14, CNS, DDM, DKA
card_name('tragic slip', 'Tragic Slip').
card_type('tragic slip', 'Instant').
card_types('tragic slip', ['Instant']).
card_subtypes('tragic slip', []).
card_colors('tragic slip', ['B']).
card_text('tragic slip', 'Target creature gets -1/-1 until end of turn.\nMorbid — That creature gets -13/-13 until end of turn instead if a creature died this turn.').
card_mana_cost('tragic slip', ['B']).
card_cmc('tragic slip', 1).
card_layout('tragic slip', 'normal').

% Found in: KTK, pPRE
card_name('trail of mystery', 'Trail of Mystery').
card_type('trail of mystery', 'Enchantment').
card_types('trail of mystery', ['Enchantment']).
card_subtypes('trail of mystery', []).
card_colors('trail of mystery', ['G']).
card_text('trail of mystery', 'Whenever a face-down creature enters the battlefield under your control, you may search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.\nWhenever a permanent you control is turned face up, if it\'s a creature, it gets +2/+2 until end of turn.').
card_mana_cost('trail of mystery', ['1', 'G']).
card_cmc('trail of mystery', 2).
card_layout('trail of mystery', 'normal').

% Found in: PC2
card_name('trail of the mage-rings', 'Trail of the Mage-Rings').
card_type('trail of the mage-rings', 'Plane — Vryn').
card_types('trail of the mage-rings', ['Plane']).
card_subtypes('trail of the mage-rings', ['Vryn']).
card_colors('trail of the mage-rings', []).
card_text('trail of the mage-rings', 'Instant and sorcery spells have rebound. (The spell\'s controller exiles the spell as it resolves if he or she cast it from his or her hand. At the beginning of that player\'s next upkeep, he or she may cast that card from exile without paying its mana cost.)\nWhenever you roll {C}, you may search your library for an instant or sorcery card, reveal it, put it into your hand, then shuffle your library.').
card_layout('trail of the mage-rings', 'plane').

% Found in: ICE
card_name('trailblazer', 'Trailblazer').
card_type('trailblazer', 'Instant').
card_types('trailblazer', ['Instant']).
card_subtypes('trailblazer', []).
card_colors('trailblazer', ['G']).
card_text('trailblazer', 'Target creature can\'t be blocked this turn.').
card_mana_cost('trailblazer', ['2', 'G', 'G']).
card_cmc('trailblazer', 4).
card_layout('trailblazer', 'normal').
card_reserved('trailblazer').

% Found in: ZEN
card_name('trailblazer\'s boots', 'Trailblazer\'s Boots').
card_type('trailblazer\'s boots', 'Artifact — Equipment').
card_types('trailblazer\'s boots', ['Artifact']).
card_subtypes('trailblazer\'s boots', ['Equipment']).
card_colors('trailblazer\'s boots', []).
card_text('trailblazer\'s boots', 'Equipped creature has nonbasic landwalk. (It can\'t be blocked as long as defending player controls a nonbasic land.)\nEquip {2}').
card_mana_cost('trailblazer\'s boots', ['2']).
card_cmc('trailblazer\'s boots', 2).
card_layout('trailblazer\'s boots', 'normal').

% Found in: DDJ, GPT
card_name('train of thought', 'Train of Thought').
card_type('train of thought', 'Sorcery').
card_types('train of thought', ['Sorcery']).
card_subtypes('train of thought', []).
card_colors('train of thought', ['U']).
card_text('train of thought', 'Replicate {1}{U} (When you cast this spell, copy it for each time you paid its replicate cost.)\nDraw a card.').
card_mana_cost('train of thought', ['1', 'U']).
card_cmc('train of thought', 2).
card_layout('train of thought', 'normal').

% Found in: 6ED, 7ED, 8ED, 9ED, DPA, TMP, TPR
card_name('trained armodon', 'Trained Armodon').
card_type('trained armodon', 'Creature — Elephant').
card_types('trained armodon', ['Creature']).
card_subtypes('trained armodon', ['Elephant']).
card_colors('trained armodon', ['G']).
card_text('trained armodon', '').
card_mana_cost('trained armodon', ['1', 'G', 'G']).
card_cmc('trained armodon', 3).
card_layout('trained armodon', 'normal').
card_power('trained armodon', 3).
card_toughness('trained armodon', 3).

% Found in: RTR
card_name('trained caracal', 'Trained Caracal').
card_type('trained caracal', 'Creature — Cat').
card_types('trained caracal', ['Creature']).
card_subtypes('trained caracal', ['Cat']).
card_colors('trained caracal', ['W']).
card_text('trained caracal', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('trained caracal', ['W']).
card_cmc('trained caracal', 1).
card_layout('trained caracal', 'normal').
card_power('trained caracal', 1).
card_toughness('trained caracal', 1).

% Found in: PTK
card_name('trained cheetah', 'Trained Cheetah').
card_type('trained cheetah', 'Creature — Cat').
card_types('trained cheetah', ['Creature']).
card_subtypes('trained cheetah', ['Cat']).
card_colors('trained cheetah', ['G']).
card_text('trained cheetah', 'Whenever Trained Cheetah becomes blocked, it gets +1/+1 until end of turn.').
card_mana_cost('trained cheetah', ['2', 'G']).
card_cmc('trained cheetah', 3).
card_layout('trained cheetah', 'normal').
card_power('trained cheetah', 2).
card_toughness('trained cheetah', 2).

% Found in: M14
card_name('trained condor', 'Trained Condor').
card_type('trained condor', 'Creature — Bird').
card_types('trained condor', ['Creature']).
card_subtypes('trained condor', ['Bird']).
card_colors('trained condor', ['U']).
card_text('trained condor', 'Flying\nWhenever Trained Condor attacks, another target creature you control gains flying until end of turn.').
card_mana_cost('trained condor', ['2', 'U']).
card_cmc('trained condor', 3).
card_layout('trained condor', 'normal').
card_power('trained condor', 2).
card_toughness('trained condor', 1).

% Found in: PTK
card_name('trained jackal', 'Trained Jackal').
card_type('trained jackal', 'Creature — Hound').
card_types('trained jackal', ['Creature']).
card_subtypes('trained jackal', ['Hound']).
card_colors('trained jackal', ['G']).
card_text('trained jackal', '').
card_mana_cost('trained jackal', ['G']).
card_cmc('trained jackal', 1).
card_layout('trained jackal', 'normal').
card_power('trained jackal', 1).
card_toughness('trained jackal', 2).

% Found in: 7ED, S00, S99
card_name('trained orgg', 'Trained Orgg').
card_type('trained orgg', 'Creature — Orgg').
card_types('trained orgg', ['Creature']).
card_subtypes('trained orgg', ['Orgg']).
card_colors('trained orgg', ['R']).
card_text('trained orgg', '').
card_mana_cost('trained orgg', ['6', 'R']).
card_cmc('trained orgg', 7).
card_layout('trained orgg', 'normal').
card_power('trained orgg', 6).
card_toughness('trained orgg', 6).

% Found in: JUD
card_name('trained pronghorn', 'Trained Pronghorn').
card_type('trained pronghorn', 'Creature — Antelope').
card_types('trained pronghorn', ['Creature']).
card_subtypes('trained pronghorn', ['Antelope']).
card_colors('trained pronghorn', ['W']).
card_text('trained pronghorn', 'Discard a card: Prevent all damage that would be dealt to Trained Pronghorn this turn.').
card_mana_cost('trained pronghorn', ['1', 'W']).
card_cmc('trained pronghorn', 2).
card_layout('trained pronghorn', 'normal').
card_power('trained pronghorn', 1).
card_toughness('trained pronghorn', 1).

% Found in: MBS
card_name('training drone', 'Training Drone').
card_type('training drone', 'Artifact Creature — Drone').
card_types('training drone', ['Artifact', 'Creature']).
card_subtypes('training drone', ['Drone']).
card_colors('training drone', []).
card_text('training drone', 'Training Drone can\'t attack or block unless it\'s equipped.').
card_mana_cost('training drone', ['3']).
card_cmc('training drone', 3).
card_layout('training drone', 'normal').
card_power('training drone', 4).
card_toughness('training drone', 4).

% Found in: ROE
card_name('training grounds', 'Training Grounds').
card_type('training grounds', 'Enchantment').
card_types('training grounds', ['Enchantment']).
card_subtypes('training grounds', []).
card_colors('training grounds', ['U']).
card_text('training grounds', 'Activated abilities of creatures you control cost up to {2} less to activate. This effect can\'t reduce the amount of mana an ability costs to activate to less than one mana.').
card_mana_cost('training grounds', ['U']).
card_cmc('training grounds', 1).
card_layout('training grounds', 'normal').

% Found in: DGM
card_name('trait doctoring', 'Trait Doctoring').
card_type('trait doctoring', 'Sorcery').
card_types('trait doctoring', ['Sorcery']).
card_subtypes('trait doctoring', []).
card_colors('trait doctoring', ['U']).
card_text('trait doctoring', 'Change the text of target permanent by replacing all instances of one color word with another or one basic land type with another until end of turn.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('trait doctoring', ['U']).
card_cmc('trait doctoring', 1).
card_layout('trait doctoring', 'normal').

% Found in: TSP
card_name('traitor\'s clutch', 'Traitor\'s Clutch').
card_type('traitor\'s clutch', 'Instant').
card_types('traitor\'s clutch', ['Instant']).
card_subtypes('traitor\'s clutch', []).
card_colors('traitor\'s clutch', ['B']).
card_text('traitor\'s clutch', 'Target creature gets +1/+0, becomes black, and gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)\nFlashback {1}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('traitor\'s clutch', ['4', 'B']).
card_cmc('traitor\'s clutch', 5).
card_layout('traitor\'s clutch', 'normal').

% Found in: SHM
card_name('traitor\'s roar', 'Traitor\'s Roar').
card_type('traitor\'s roar', 'Sorcery').
card_types('traitor\'s roar', ['Sorcery']).
card_subtypes('traitor\'s roar', []).
card_colors('traitor\'s roar', ['B', 'R']).
card_text('traitor\'s roar', 'Tap target untapped creature. It deals damage equal to its power to its controller.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('traitor\'s roar', ['4', 'B/R']).
card_cmc('traitor\'s roar', 5).
card_layout('traitor\'s roar', 'normal').

% Found in: ISD
card_name('traitorous blood', 'Traitorous Blood').
card_type('traitorous blood', 'Sorcery').
card_types('traitorous blood', ['Sorcery']).
card_subtypes('traitorous blood', []).
card_colors('traitorous blood', ['R']).
card_text('traitorous blood', 'Gain control of target creature until end of turn. Untap it. It gains trample and haste until end of turn.').
card_mana_cost('traitorous blood', ['1', 'R', 'R']).
card_cmc('traitorous blood', 3).
card_layout('traitorous blood', 'normal').

% Found in: ROE, RTR
card_name('traitorous instinct', 'Traitorous Instinct').
card_type('traitorous instinct', 'Sorcery').
card_types('traitorous instinct', ['Sorcery']).
card_subtypes('traitorous instinct', []).
card_colors('traitorous instinct', ['R']).
card_text('traitorous instinct', 'Gain control of target creature until end of turn. Untap that creature. Until end of turn, it gets +2/+0 and gains haste.').
card_mana_cost('traitorous instinct', ['3', 'R']).
card_cmc('traitorous instinct', 4).
card_layout('traitorous instinct', 'normal').

% Found in: FRF, KTK
card_name('tranquil cove', 'Tranquil Cove').
card_type('tranquil cove', 'Land').
card_types('tranquil cove', ['Land']).
card_subtypes('tranquil cove', []).
card_colors('tranquil cove', []).
card_text('tranquil cove', 'Tranquil Cove enters the battlefield tapped.\nWhen Tranquil Cove enters the battlefield, you gain 1 life.\n{T}: Add {W} or {U} to your mana pool.').
card_layout('tranquil cove', 'normal').

% Found in: MIR
card_name('tranquil domain', 'Tranquil Domain').
card_type('tranquil domain', 'Instant').
card_types('tranquil domain', ['Instant']).
card_subtypes('tranquil domain', []).
card_colors('tranquil domain', ['G']).
card_text('tranquil domain', 'Destroy all non-Aura enchantments.').
card_mana_cost('tranquil domain', ['1', 'G']).
card_cmc('tranquil domain', 2).
card_layout('tranquil domain', 'normal').

% Found in: CHK
card_name('tranquil garden', 'Tranquil Garden').
card_type('tranquil garden', 'Land').
card_types('tranquil garden', ['Land']).
card_subtypes('tranquil garden', []).
card_colors('tranquil garden', []).
card_text('tranquil garden', '{T}: Add {1} to your mana pool.\n{T}: Add {G} or {W} to your mana pool. Tranquil Garden doesn\'t untap during your next untap step.').
card_layout('tranquil garden', 'normal').

% Found in: 6ED, WTH
card_name('tranquil grove', 'Tranquil Grove').
card_type('tranquil grove', 'Enchantment').
card_types('tranquil grove', ['Enchantment']).
card_subtypes('tranquil grove', []).
card_colors('tranquil grove', ['G']).
card_text('tranquil grove', '{1}{G}{G}: Destroy all other enchantments.').
card_mana_cost('tranquil grove', ['1', 'G']).
card_cmc('tranquil grove', 2).
card_layout('tranquil grove', 'normal').

% Found in: APC
card_name('tranquil path', 'Tranquil Path').
card_type('tranquil path', 'Sorcery').
card_types('tranquil path', ['Sorcery']).
card_subtypes('tranquil path', []).
card_colors('tranquil path', ['G']).
card_text('tranquil path', 'Destroy all enchantments.\nDraw a card.').
card_mana_cost('tranquil path', ['4', 'G']).
card_cmc('tranquil path', 5).
card_layout('tranquil path', 'normal').

% Found in: ARC, C13, C14, CMD, DD3_EVG, DDJ, EVG, ONS, VMA
card_name('tranquil thicket', 'Tranquil Thicket').
card_type('tranquil thicket', 'Land').
card_types('tranquil thicket', ['Land']).
card_subtypes('tranquil thicket', []).
card_colors('tranquil thicket', []).
card_text('tranquil thicket', 'Tranquil Thicket enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\nCycling {G} ({G}, Discard this card: Draw a card.)').
card_layout('tranquil thicket', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, BRB, CED, CEI, INV, LEA, LEB, MMQ, TMP, TPR
card_name('tranquility', 'Tranquility').
card_type('tranquility', 'Sorcery').
card_types('tranquility', ['Sorcery']).
card_subtypes('tranquility', []).
card_colors('tranquility', ['G']).
card_text('tranquility', 'Destroy all enchantments.').
card_mana_cost('tranquility', ['2', 'G']).
card_cmc('tranquility', 3).
card_layout('tranquility', 'normal').

% Found in: TOR
card_name('transcendence', 'Transcendence').
card_type('transcendence', 'Enchantment').
card_types('transcendence', ['Enchantment']).
card_subtypes('transcendence', []).
card_colors('transcendence', ['W']).
card_text('transcendence', 'You don\'t lose the game for having 0 or less life.\nWhen you have 20 or more life, you lose the game.\nWhenever you lose life, you gain 2 life for each 1 life you lost. (Damage dealt to you causes you to lose life.)').
card_mana_cost('transcendence', ['3', 'W', 'W', 'W']).
card_cmc('transcendence', 6).
card_layout('transcendence', 'normal').

% Found in: ROE
card_name('transcendent master', 'Transcendent Master').
card_type('transcendent master', 'Creature — Human Cleric Avatar').
card_types('transcendent master', ['Creature']).
card_subtypes('transcendent master', ['Human', 'Cleric', 'Avatar']).
card_colors('transcendent master', ['W']).
card_text('transcendent master', 'Level up {1} ({1}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 6-11\n6/6\nLifelink\nLEVEL 12+\n9/9\nLifelink, indestructible').
card_mana_cost('transcendent master', ['1', 'W', 'W']).
card_cmc('transcendent master', 3).
card_layout('transcendent master', 'leveler').
card_power('transcendent master', 3).
card_toughness('transcendent master', 3).

% Found in: BFZ
card_name('transgress the mind', 'Transgress the Mind').
card_type('transgress the mind', 'Sorcery').
card_types('transgress the mind', ['Sorcery']).
card_subtypes('transgress the mind', []).
card_colors('transgress the mind', []).
card_text('transgress the mind', 'Devoid (This card has no color.)\nTarget player reveals his or her hand. You choose a card from it with converted mana cost 3 or greater and exile that card.').
card_mana_cost('transgress the mind', ['1', 'B']).
card_cmc('transgress the mind', 2).
card_layout('transgress the mind', 'normal').

% Found in: DIS
card_name('transguild courier', 'Transguild Courier').
card_type('transguild courier', 'Artifact Creature — Golem').
card_types('transguild courier', ['Artifact', 'Creature']).
card_subtypes('transguild courier', ['Golem']).
card_colors('transguild courier', ['W', 'U', 'B', 'R', 'G']).
card_text('transguild courier', '').
card_mana_cost('transguild courier', ['4']).
card_cmc('transguild courier', 4).
card_layout('transguild courier', 'normal').
card_power('transguild courier', 3).
card_toughness('transguild courier', 3).

% Found in: C13, RTR
card_name('transguild promenade', 'Transguild Promenade').
card_type('transguild promenade', 'Land').
card_types('transguild promenade', ['Land']).
card_subtypes('transguild promenade', []).
card_colors('transguild promenade', []).
card_text('transguild promenade', 'Transguild Promenade enters the battlefield tapped.\nWhen Transguild Promenade enters the battlefield, sacrifice it unless you pay {1}.\n{T}: Add one mana of any color to your mana pool.').
card_layout('transguild promenade', 'normal').

% Found in: RAV
card_name('transluminant', 'Transluminant').
card_type('transluminant', 'Creature — Dryad Shaman').
card_types('transluminant', ['Creature']).
card_subtypes('transluminant', ['Dryad', 'Shaman']).
card_colors('transluminant', ['G']).
card_text('transluminant', '{W}, Sacrifice Transluminant: Put a 1/1 white Spirit creature token with flying onto the battlefield at the beginning of the next end step.').
card_mana_cost('transluminant', ['1', 'G']).
card_cmc('transluminant', 2).
card_layout('transluminant', 'normal').
card_power('transluminant', 2).
card_toughness('transluminant', 2).

% Found in: EXO
card_name('transmogrifying licid', 'Transmogrifying Licid').
card_type('transmogrifying licid', 'Artifact Creature — Licid').
card_types('transmogrifying licid', ['Artifact', 'Creature']).
card_subtypes('transmogrifying licid', ['Licid']).
card_colors('transmogrifying licid', []).
card_text('transmogrifying licid', '{1}, {T}: Transmogrifying Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {1} to end this effect.\nEnchanted creature gets +1/+1 and is an artifact in addition to its other types.').
card_mana_cost('transmogrifying licid', ['3']).
card_cmc('transmogrifying licid', 3).
card_layout('transmogrifying licid', 'normal').
card_power('transmogrifying licid', 2).
card_toughness('transmogrifying licid', 2).

% Found in: CHR, LEG
card_name('transmutation', 'Transmutation').
card_type('transmutation', 'Instant').
card_types('transmutation', ['Instant']).
card_subtypes('transmutation', []).
card_colors('transmutation', ['B']).
card_text('transmutation', 'Switch target creature\'s power and toughness until end of turn.').
card_mana_cost('transmutation', ['1', 'B']).
card_cmc('transmutation', 2).
card_layout('transmutation', 'normal').

% Found in: ATQ, ME4
card_name('transmute artifact', 'Transmute Artifact').
card_type('transmute artifact', 'Sorcery').
card_types('transmute artifact', ['Sorcery']).
card_subtypes('transmute artifact', []).
card_colors('transmute artifact', ['U']).
card_text('transmute artifact', 'Sacrifice an artifact. If you do, search your library for an artifact card. If that card\'s converted mana cost is less than or equal to the sacrificed artifact\'s converted mana cost, put it onto the battlefield. If it\'s greater, you may pay {X}, where X is the difference. If you do, put it onto the battlefield. If you don\'t, put it into its owner\'s graveyard. Then shuffle your library.').
card_mana_cost('transmute artifact', ['U', 'U']).
card_cmc('transmute artifact', 2).
card_layout('transmute artifact', 'normal').
card_reserved('transmute artifact').

% Found in: SCG
card_name('trap digger', 'Trap Digger').
card_type('trap digger', 'Creature — Human Soldier').
card_types('trap digger', ['Creature']).
card_subtypes('trap digger', ['Human', 'Soldier']).
card_colors('trap digger', ['W']).
card_text('trap digger', '{2}{W}, {T}: Put a trap counter on target land you control.\nSacrifice a land with a trap counter on it: Trap Digger deals 3 damage to target attacking creature without flying.').
card_mana_cost('trap digger', ['3', 'W']).
card_cmc('trap digger', 4).
card_layout('trap digger', 'normal').
card_power('trap digger', 1).
card_toughness('trap digger', 3).

% Found in: KTK, pPRE
card_name('trap essence', 'Trap Essence').
card_type('trap essence', 'Instant').
card_types('trap essence', ['Instant']).
card_subtypes('trap essence', []).
card_colors('trap essence', ['U', 'R', 'G']).
card_text('trap essence', 'Counter target creature spell. Put two +1/+1 counters on up to one target creature.').
card_mana_cost('trap essence', ['G', 'U', 'R']).
card_cmc('trap essence', 3).
card_layout('trap essence', 'normal').

% Found in: MMQ
card_name('trap runner', 'Trap Runner').
card_type('trap runner', 'Creature — Human Soldier').
card_types('trap runner', ['Creature']).
card_subtypes('trap runner', ['Human', 'Soldier']).
card_colors('trap runner', ['W']).
card_text('trap runner', '{T}: Target unblocked attacking creature becomes blocked. Activate this ability only during combat after blockers are declared. (This ability works on creatures that can\'t be blocked.)').
card_mana_cost('trap runner', ['2', 'W', 'W']).
card_cmc('trap runner', 4).
card_layout('trap runner', 'normal').
card_power('trap runner', 2).
card_toughness('trap runner', 3).

% Found in: ZEN
card_name('trapfinder\'s trick', 'Trapfinder\'s Trick').
card_type('trapfinder\'s trick', 'Sorcery').
card_types('trapfinder\'s trick', ['Sorcery']).
card_subtypes('trapfinder\'s trick', []).
card_colors('trapfinder\'s trick', ['U']).
card_text('trapfinder\'s trick', 'Target player reveals his or her hand and discards all Trap cards.').
card_mana_cost('trapfinder\'s trick', ['1', 'U']).
card_cmc('trapfinder\'s trick', 2).
card_layout('trapfinder\'s trick', 'normal').

% Found in: EVE
card_name('trapjaw kelpie', 'Trapjaw Kelpie').
card_type('trapjaw kelpie', 'Creature — Beast').
card_types('trapjaw kelpie', ['Creature']).
card_subtypes('trapjaw kelpie', ['Beast']).
card_colors('trapjaw kelpie', ['U', 'G']).
card_text('trapjaw kelpie', 'Flash\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('trapjaw kelpie', ['4', 'G/U', 'G/U']).
card_cmc('trapjaw kelpie', 6).
card_layout('trapjaw kelpie', 'normal').
card_power('trapjaw kelpie', 3).
card_toughness('trapjaw kelpie', 3).

% Found in: ZEN
card_name('trapmaker\'s snare', 'Trapmaker\'s Snare').
card_type('trapmaker\'s snare', 'Instant').
card_types('trapmaker\'s snare', ['Instant']).
card_subtypes('trapmaker\'s snare', []).
card_colors('trapmaker\'s snare', ['U']).
card_text('trapmaker\'s snare', 'Search your library for a Trap card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('trapmaker\'s snare', ['1', 'U']).
card_cmc('trapmaker\'s snare', 2).
card_layout('trapmaker\'s snare', 'normal').

% Found in: BOK
card_name('traproot kami', 'Traproot Kami').
card_type('traproot kami', 'Creature — Spirit').
card_types('traproot kami', ['Creature']).
card_subtypes('traproot kami', ['Spirit']).
card_colors('traproot kami', ['G']).
card_text('traproot kami', 'Defender; reach (This creature can block creatures with flying.)\nTraproot Kami\'s toughness is equal to the number of Forests on the battlefield.').
card_mana_cost('traproot kami', ['G']).
card_cmc('traproot kami', 1).
card_layout('traproot kami', 'normal').
card_power('traproot kami', 0).
card_toughness('traproot kami', '*').

% Found in: MRD
card_name('trash for treasure', 'Trash for Treasure').
card_type('trash for treasure', 'Sorcery').
card_types('trash for treasure', ['Sorcery']).
card_subtypes('trash for treasure', []).
card_colors('trash for treasure', ['R']).
card_text('trash for treasure', 'As an additional cost to cast Trash for Treasure, sacrifice an artifact.\nReturn target artifact card from your graveyard to the battlefield.').
card_mana_cost('trash for treasure', ['2', 'R']).
card_cmc('trash for treasure', 3).
card_layout('trash for treasure', 'normal').

% Found in: CON, DDN, MMA
card_name('traumatic visions', 'Traumatic Visions').
card_type('traumatic visions', 'Instant').
card_types('traumatic visions', ['Instant']).
card_subtypes('traumatic visions', []).
card_colors('traumatic visions', ['U']).
card_text('traumatic visions', 'Counter target spell.\nBasic landcycling {1}{U} ({1}{U}, Discard this card: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('traumatic visions', ['3', 'U', 'U']).
card_cmc('traumatic visions', 5).
card_layout('traumatic visions', 'normal').

% Found in: 10E, 9ED, M10, M11, M14, ODY
card_name('traumatize', 'Traumatize').
card_type('traumatize', 'Sorcery').
card_types('traumatize', ['Sorcery']).
card_subtypes('traumatize', []).
card_colors('traumatize', ['U']).
card_text('traumatize', 'Target player puts the top half of his or her library, rounded down, into his or her graveyard.').
card_mana_cost('traumatize', ['3', 'U', 'U']).
card_cmc('traumatize', 5).
card_layout('traumatize', 'normal').

% Found in: ISD
card_name('travel preparations', 'Travel Preparations').
card_type('travel preparations', 'Sorcery').
card_types('travel preparations', ['Sorcery']).
card_subtypes('travel preparations', []).
card_colors('travel preparations', ['G']).
card_text('travel preparations', 'Put a +1/+1 counter on each of up to two target creatures.\nFlashback {1}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('travel preparations', ['1', 'G']).
card_cmc('travel preparations', 2).
card_layout('travel preparations', 'normal').

% Found in: ISD, THS
card_name('traveler\'s amulet', 'Traveler\'s Amulet').
card_type('traveler\'s amulet', 'Artifact').
card_types('traveler\'s amulet', ['Artifact']).
card_subtypes('traveler\'s amulet', []).
card_colors('traveler\'s amulet', []).
card_text('traveler\'s amulet', '{1}, Sacrifice Traveler\'s Amulet: Search your library for a basic land card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('traveler\'s amulet', ['1']).
card_cmc('traveler\'s amulet', 1).
card_layout('traveler\'s amulet', 'normal').

% Found in: CNS, INV
card_name('traveler\'s cloak', 'Traveler\'s Cloak').
card_type('traveler\'s cloak', 'Enchantment — Aura').
card_types('traveler\'s cloak', ['Enchantment']).
card_subtypes('traveler\'s cloak', ['Aura']).
card_colors('traveler\'s cloak', ['U']).
card_text('traveler\'s cloak', 'Enchant creature\nAs Traveler\'s Cloak enters the battlefield, choose a land type.\nWhen Traveler\'s Cloak enters the battlefield, draw a card.\nEnchanted creature has landwalk of the chosen type. (It can\'t be blocked as long as defending player controls a land of that type.)').
card_mana_cost('traveler\'s cloak', ['2', 'U']).
card_cmc('traveler\'s cloak', 3).
card_layout('traveler\'s cloak', 'normal').

% Found in: THS
card_name('traveling philosopher', 'Traveling Philosopher').
card_type('traveling philosopher', 'Creature — Human Advisor').
card_types('traveling philosopher', ['Creature']).
card_subtypes('traveling philosopher', ['Human', 'Advisor']).
card_colors('traveling philosopher', ['W']).
card_text('traveling philosopher', '').
card_mana_cost('traveling philosopher', ['1', 'W']).
card_cmc('traveling philosopher', 2).
card_layout('traveling philosopher', 'normal').
card_power('traveling philosopher', 2).
card_toughness('traveling philosopher', 2).

% Found in: ODY
card_name('traveling plague', 'Traveling Plague').
card_type('traveling plague', 'Enchantment — Aura').
card_types('traveling plague', ['Enchantment']).
card_subtypes('traveling plague', ['Aura']).
card_colors('traveling plague', ['B']).
card_text('traveling plague', 'Enchant creature\nAt the beginning of each upkeep, put a plague counter on Traveling Plague.\nEnchanted creature gets -1/-1 for each plague counter on Traveling Plague.\nWhen enchanted creature leaves the battlefield, that creature\'s controller returns Traveling Plague from its owner\'s graveyard to the battlefield.').
card_mana_cost('traveling plague', ['3', 'B', 'B']).
card_cmc('traveling plague', 5).
card_layout('traveling plague', 'normal').

% Found in: ULG
card_name('treacherous link', 'Treacherous Link').
card_type('treacherous link', 'Enchantment — Aura').
card_types('treacherous link', ['Enchantment']).
card_subtypes('treacherous link', ['Aura']).
card_colors('treacherous link', ['B']).
card_text('treacherous link', 'Enchant creature\nAll damage that would be dealt to enchanted creature is dealt to its controller instead.').
card_mana_cost('treacherous link', ['1', 'B']).
card_cmc('treacherous link', 2).
card_layout('treacherous link', 'normal').

% Found in: AVR
card_name('treacherous pit-dweller', 'Treacherous Pit-Dweller').
card_type('treacherous pit-dweller', 'Creature — Demon').
card_types('treacherous pit-dweller', ['Creature']).
card_subtypes('treacherous pit-dweller', ['Demon']).
card_colors('treacherous pit-dweller', ['B']).
card_text('treacherous pit-dweller', 'When Treacherous Pit-Dweller enters the battlefield from a graveyard, target opponent gains control of it.\nUndying (When this creature dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('treacherous pit-dweller', ['B', 'B']).
card_cmc('treacherous pit-dweller', 2).
card_layout('treacherous pit-dweller', 'normal').
card_power('treacherous pit-dweller', 4).
card_toughness('treacherous pit-dweller', 3).

% Found in: PLC
card_name('treacherous urge', 'Treacherous Urge').
card_type('treacherous urge', 'Instant').
card_types('treacherous urge', ['Instant']).
card_subtypes('treacherous urge', []).
card_colors('treacherous urge', ['B']).
card_text('treacherous urge', 'Target opponent reveals his or her hand. You may put a creature card from it onto the battlefield under your control. That creature gains haste. Sacrifice it at the beginning of the next end step.').
card_mana_cost('treacherous urge', ['4', 'B']).
card_cmc('treacherous urge', 5).
card_layout('treacherous urge', 'normal').

% Found in: JUD
card_name('treacherous vampire', 'Treacherous Vampire').
card_type('treacherous vampire', 'Creature — Vampire').
card_types('treacherous vampire', ['Creature']).
card_subtypes('treacherous vampire', ['Vampire']).
card_colors('treacherous vampire', ['B']).
card_text('treacherous vampire', 'Flying\nWhenever Treacherous Vampire attacks or blocks, sacrifice it unless you exile a card from your graveyard.\nThreshold — As long as seven or more cards are in your graveyard, Treacherous Vampire gets +2/+2 and has \"When Treacherous Vampire dies, you lose 6 life.\"').
card_mana_cost('treacherous vampire', ['4', 'B']).
card_cmc('treacherous vampire', 5).
card_layout('treacherous vampire', 'normal').
card_power('treacherous vampire', 4).
card_toughness('treacherous vampire', 4).

% Found in: JUD
card_name('treacherous werewolf', 'Treacherous Werewolf').
card_type('treacherous werewolf', 'Creature — Werewolf Minion').
card_types('treacherous werewolf', ['Creature']).
card_subtypes('treacherous werewolf', ['Werewolf', 'Minion']).
card_colors('treacherous werewolf', ['B']).
card_text('treacherous werewolf', 'Threshold — As long as seven or more cards are in your graveyard, Treacherous Werewolf gets +2/+2 and has \"When Treacherous Werewolf dies, you lose 4 life.\"').
card_mana_cost('treacherous werewolf', ['2', 'B']).
card_cmc('treacherous werewolf', 3).
card_layout('treacherous werewolf', 'normal').
card_power('treacherous werewolf', 2).
card_toughness('treacherous werewolf', 2).

% Found in: UDS
card_name('treachery', 'Treachery').
card_type('treachery', 'Enchantment — Aura').
card_types('treachery', ['Enchantment']).
card_subtypes('treachery', ['Aura']).
card_colors('treachery', ['U']).
card_text('treachery', 'Enchant creature\nWhen Treachery enters the battlefield, untap up to five lands.\nYou control enchanted creature.').
card_mana_cost('treachery', ['3', 'U', 'U']).
card_cmc('treachery', 5).
card_layout('treachery', 'normal').
card_reserved('treachery').

% Found in: DTK
card_name('tread upon', 'Tread Upon').
card_type('tread upon', 'Instant').
card_types('tread upon', ['Instant']).
card_subtypes('tread upon', []).
card_colors('tread upon', ['G']).
card_text('tread upon', 'Target creature gets +2/+2 and gains trample until end of turn.').
card_mana_cost('tread upon', ['1', 'G']).
card_cmc('tread upon', 2).
card_layout('tread upon', 'normal').

% Found in: CNS
card_name('treasonous ogre', 'Treasonous Ogre').
card_type('treasonous ogre', 'Creature — Ogre Shaman').
card_types('treasonous ogre', ['Creature']).
card_subtypes('treasonous ogre', ['Ogre', 'Shaman']).
card_colors('treasonous ogre', ['R']).
card_text('treasonous ogre', 'Dethrone (Whenever this creature attacks the player with the most life or tied for most life, put a +1/+1 counter on it.)\nPay 3 life: Add {R} to your mana pool.').
card_mana_cost('treasonous ogre', ['3', 'R']).
card_cmc('treasonous ogre', 4).
card_layout('treasonous ogre', 'normal').
card_power('treasonous ogre', 2).
card_toughness('treasonous ogre', 3).

% Found in: KTK
card_name('treasure cruise', 'Treasure Cruise').
card_type('treasure cruise', 'Sorcery').
card_types('treasure cruise', ['Sorcery']).
card_subtypes('treasure cruise', []).
card_colors('treasure cruise', ['U']).
card_text('treasure cruise', 'Delve (Each card you exile from your graveyard while casting this spell pays for {1}.)\nDraw three cards.').
card_mana_cost('treasure cruise', ['7', 'U']).
card_cmc('treasure cruise', 8).
card_layout('treasure cruise', 'normal').

% Found in: WWK, pMEI, pMPR
card_name('treasure hunt', 'Treasure Hunt').
card_type('treasure hunt', 'Sorcery').
card_types('treasure hunt', ['Sorcery']).
card_subtypes('treasure hunt', []).
card_colors('treasure hunt', ['U']).
card_text('treasure hunt', 'Reveal cards from the top of your library until you reveal a nonland card, then put all cards revealed this way into your hand.').
card_mana_cost('treasure hunt', ['1', 'U']).
card_cmc('treasure hunt', 2).
card_layout('treasure hunt', 'normal').

% Found in: 10E, EXO
card_name('treasure hunter', 'Treasure Hunter').
card_type('treasure hunter', 'Creature — Human').
card_types('treasure hunter', ['Creature']).
card_subtypes('treasure hunter', ['Human']).
card_colors('treasure hunter', ['W']).
card_text('treasure hunter', 'When Treasure Hunter enters the battlefield, you may return target artifact card from your graveyard to your hand.').
card_mana_cost('treasure hunter', ['2', 'W']).
card_cmc('treasure hunter', 3).
card_layout('treasure hunter', 'normal').
card_power('treasure hunter', 2).
card_toughness('treasure hunter', 2).

% Found in: MBS, pMGD
card_name('treasure mage', 'Treasure Mage').
card_type('treasure mage', 'Creature — Human Wizard').
card_types('treasure mage', ['Creature']).
card_subtypes('treasure mage', ['Human', 'Wizard']).
card_colors('treasure mage', ['U']).
card_text('treasure mage', 'When Treasure Mage enters the battlefield, you may search your library for an artifact card with converted mana cost 6 or greater, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('treasure mage', ['2', 'U']).
card_cmc('treasure mage', 3).
card_layout('treasure mage', 'normal').
card_power('treasure mage', 2).
card_toughness('treasure mage', 2).

% Found in: 7ED, 8ED, 9ED, EXO
card_name('treasure trove', 'Treasure Trove').
card_type('treasure trove', 'Enchantment').
card_types('treasure trove', ['Enchantment']).
card_subtypes('treasure trove', []).
card_colors('treasure trove', ['U']).
card_text('treasure trove', '{2}{U}{U}: Draw a card.').
card_mana_cost('treasure trove', ['2', 'U', 'U']).
card_cmc('treasure trove', 4).
card_layout('treasure trove', 'normal').

% Found in: DDM, RTR
card_name('treasured find', 'Treasured Find').
card_type('treasured find', 'Sorcery').
card_types('treasured find', ['Sorcery']).
card_subtypes('treasured find', []).
card_colors('treasured find', ['B', 'G']).
card_text('treasured find', 'Return target card from your graveyard to your hand. Exile Treasured Find.').
card_mana_cost('treasured find', ['B', 'G']).
card_cmc('treasured find', 2).
card_layout('treasured find', 'normal').

% Found in: GTC, pPRE
card_name('treasury thrull', 'Treasury Thrull').
card_type('treasury thrull', 'Creature — Thrull').
card_types('treasury thrull', ['Creature']).
card_subtypes('treasury thrull', ['Thrull']).
card_colors('treasury thrull', ['W', 'B']).
card_text('treasury thrull', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nWhenever Treasury Thrull attacks, you may return target artifact, creature, or enchantment card from your graveyard to your hand.').
card_mana_cost('treasury thrull', ['4', 'W', 'B']).
card_cmc('treasury thrull', 6).
card_layout('treasury thrull', 'normal').
card_power('treasury thrull', 4).
card_toughness('treasury thrull', 4).

% Found in: 9ED, PO2
card_name('tree monkey', 'Tree Monkey').
card_type('tree monkey', 'Creature — Ape').
card_types('tree monkey', ['Creature']).
card_subtypes('tree monkey', ['Ape']).
card_colors('tree monkey', ['G']).
card_text('tree monkey', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('tree monkey', ['G']).
card_cmc('tree monkey', 1).
card_layout('tree monkey', 'normal').
card_power('tree monkey', 1).
card_toughness('tree monkey', 1).

% Found in: ISD
card_name('tree of redemption', 'Tree of Redemption').
card_type('tree of redemption', 'Creature — Plant').
card_types('tree of redemption', ['Creature']).
card_subtypes('tree of redemption', ['Plant']).
card_colors('tree of redemption', ['G']).
card_text('tree of redemption', 'Defender\n{T}: Exchange your life total with Tree of Redemption\'s toughness.').
card_mana_cost('tree of redemption', ['3', 'G']).
card_cmc('tree of redemption', 4).
card_layout('tree of redemption', 'normal').
card_power('tree of redemption', 0).
card_toughness('tree of redemption', 13).

% Found in: HOP, MRD
card_name('tree of tales', 'Tree of Tales').
card_type('tree of tales', 'Artifact Land').
card_types('tree of tales', ['Artifact', 'Land']).
card_subtypes('tree of tales', []).
card_colors('tree of tales', []).
card_text('tree of tales', '(Tree of Tales isn\'t a spell.)\n{T}: Add {G} to your mana pool.').
card_layout('tree of tales', 'normal').

% Found in: LRW
card_name('treefolk harbinger', 'Treefolk Harbinger').
card_type('treefolk harbinger', 'Creature — Treefolk Druid').
card_types('treefolk harbinger', ['Creature']).
card_subtypes('treefolk harbinger', ['Treefolk', 'Druid']).
card_colors('treefolk harbinger', ['G']).
card_text('treefolk harbinger', 'When Treefolk Harbinger enters the battlefield, you may search your library for a Treefolk or Forest card, reveal it, then shuffle your library and put that card on top of it.').
card_mana_cost('treefolk harbinger', ['G']).
card_cmc('treefolk harbinger', 1).
card_layout('treefolk harbinger', 'normal').
card_power('treefolk harbinger', 0).
card_toughness('treefolk harbinger', 3).

% Found in: INV
card_name('treefolk healer', 'Treefolk Healer').
card_type('treefolk healer', 'Creature — Treefolk Cleric').
card_types('treefolk healer', ['Creature']).
card_subtypes('treefolk healer', ['Treefolk', 'Cleric']).
card_colors('treefolk healer', ['G']).
card_text('treefolk healer', '{2}{W}, {T}: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('treefolk healer', ['4', 'G']).
card_cmc('treefolk healer', 5).
card_layout('treefolk healer', 'normal').
card_power('treefolk healer', 2).
card_toughness('treefolk healer', 3).

% Found in: ULG
card_name('treefolk mystic', 'Treefolk Mystic').
card_type('treefolk mystic', 'Creature — Treefolk').
card_types('treefolk mystic', ['Creature']).
card_subtypes('treefolk mystic', ['Treefolk']).
card_colors('treefolk mystic', ['G']).
card_text('treefolk mystic', 'Whenever Treefolk Mystic blocks or becomes blocked by a creature, destroy all Auras attached to that creature.').
card_mana_cost('treefolk mystic', ['3', 'G']).
card_cmc('treefolk mystic', 4).
card_layout('treefolk mystic', 'normal').
card_power('treefolk mystic', 2).
card_toughness('treefolk mystic', 4).

% Found in: 7ED, USG
card_name('treefolk seedlings', 'Treefolk Seedlings').
card_type('treefolk seedlings', 'Creature — Treefolk').
card_types('treefolk seedlings', ['Creature']).
card_subtypes('treefolk seedlings', ['Treefolk']).
card_colors('treefolk seedlings', ['G']).
card_text('treefolk seedlings', 'Treefolk Seedlings\'s toughness is equal to the number of Forests you control.').
card_mana_cost('treefolk seedlings', ['2', 'G']).
card_cmc('treefolk seedlings', 3).
card_layout('treefolk seedlings', 'normal').
card_power('treefolk seedlings', 2).
card_toughness('treefolk seedlings', '*').

% Found in: ONS
card_name('treespring lorian', 'Treespring Lorian').
card_type('treespring lorian', 'Creature — Beast').
card_types('treespring lorian', ['Creature']).
card_subtypes('treespring lorian', ['Beast']).
card_colors('treespring lorian', ['G']).
card_text('treespring lorian', 'Morph {5}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('treespring lorian', ['5', 'G']).
card_cmc('treespring lorian', 6).
card_layout('treespring lorian', 'normal').
card_power('treespring lorian', 5).
card_toughness('treespring lorian', 4).

% Found in: 10E, 9ED, NMS
card_name('treetop bracers', 'Treetop Bracers').
card_type('treetop bracers', 'Enchantment — Aura').
card_types('treetop bracers', ['Enchantment']).
card_subtypes('treetop bracers', ['Aura']).
card_colors('treetop bracers', ['G']).
card_text('treetop bracers', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nEnchanted creature gets +1/+1 and can\'t be blocked except by creatures with flying.').
card_mana_cost('treetop bracers', ['1', 'G']).
card_cmc('treetop bracers', 2).
card_layout('treetop bracers', 'normal').

% Found in: POR
card_name('treetop defense', 'Treetop Defense').
card_type('treetop defense', 'Instant').
card_types('treetop defense', ['Instant']).
card_subtypes('treetop defense', []).
card_colors('treetop defense', ['G']).
card_text('treetop defense', 'Cast Treetop Defense only during the declare attackers step and only if you\'ve been attacked this step.\nCreatures you control gain reach until end of turn. (They can block creatures with flying.)').
card_mana_cost('treetop defense', ['1', 'G']).
card_cmc('treetop defense', 2).
card_layout('treetop defense', 'normal').

% Found in: USG
card_name('treetop rangers', 'Treetop Rangers').
card_type('treetop rangers', 'Creature — Elf').
card_types('treetop rangers', ['Creature']).
card_subtypes('treetop rangers', ['Elf']).
card_colors('treetop rangers', ['G']).
card_text('treetop rangers', 'Treetop Rangers can\'t be blocked except by creatures with flying.').
card_mana_cost('treetop rangers', ['2', 'G']).
card_cmc('treetop rangers', 3).
card_layout('treetop rangers', 'normal').
card_power('treetop rangers', 2).
card_toughness('treetop rangers', 2).

% Found in: SCG
card_name('treetop scout', 'Treetop Scout').
card_type('treetop scout', 'Creature — Elf Scout').
card_types('treetop scout', ['Creature']).
card_subtypes('treetop scout', ['Elf', 'Scout']).
card_colors('treetop scout', ['G']).
card_text('treetop scout', 'Treetop Scout can\'t be blocked except by creatures with flying.').
card_mana_cost('treetop scout', ['G']).
card_cmc('treetop scout', 1).
card_layout('treetop scout', 'normal').
card_power('treetop scout', 1).
card_toughness('treetop scout', 1).

% Found in: ODY
card_name('treetop sentinel', 'Treetop Sentinel').
card_type('treetop sentinel', 'Creature — Bird Soldier').
card_types('treetop sentinel', ['Creature']).
card_subtypes('treetop sentinel', ['Bird', 'Soldier']).
card_colors('treetop sentinel', ['U']).
card_text('treetop sentinel', 'Flying, protection from green').
card_mana_cost('treetop sentinel', ['2', 'U', 'U']).
card_cmc('treetop sentinel', 4).
card_layout('treetop sentinel', 'normal').
card_power('treetop sentinel', 2).
card_toughness('treetop sentinel', 3).

% Found in: 10E, DD3_GVL, DDD, DDG, ULG, pFNM, pSUM
card_name('treetop village', 'Treetop Village').
card_type('treetop village', 'Land').
card_types('treetop village', ['Land']).
card_subtypes('treetop village', []).
card_colors('treetop village', []).
card_text('treetop village', 'Treetop Village enters the battlefield tapped.\n{T}: Add {G} to your mana pool.\n{1}{G}: Treetop Village becomes a 3/3 green Ape creature with trample until end of turn. It\'s still a land. (If it would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_layout('treetop village', 'normal').

% Found in: ODY
card_name('tremble', 'Tremble').
card_type('tremble', 'Sorcery').
card_types('tremble', ['Sorcery']).
card_subtypes('tremble', []).
card_colors('tremble', ['R']).
card_text('tremble', 'Each player sacrifices a land.').
card_mana_cost('tremble', ['1', 'R']).
card_cmc('tremble', 2).
card_layout('tremble', 'normal').

% Found in: 6ED, 7ED, 8ED, MMQ, PO2, S99, VIS
card_name('tremor', 'Tremor').
card_type('tremor', 'Sorcery').
card_types('tremor', ['Sorcery']).
card_subtypes('tremor', []).
card_colors('tremor', ['R']).
card_text('tremor', 'Tremor deals 1 damage to each creature without flying.').
card_mana_cost('tremor', ['R']).
card_cmc('tremor', 1).
card_layout('tremor', 'normal').

% Found in: CMD
card_name('trench gorger', 'Trench Gorger').
card_type('trench gorger', 'Creature — Leviathan').
card_types('trench gorger', ['Creature']).
card_subtypes('trench gorger', ['Leviathan']).
card_colors('trench gorger', ['U']).
card_text('trench gorger', 'Trample\nWhen Trench Gorger enters the battlefield, you may search your library for any number of land cards, exile them, then shuffle your library. If you do, Trench Gorger has base power and base toughness each equal to the number of cards exiled this way.').
card_mana_cost('trench gorger', ['6', 'U', 'U']).
card_cmc('trench gorger', 8).
card_layout('trench gorger', 'normal').
card_power('trench gorger', 6).
card_toughness('trench gorger', 6).

% Found in: INV
card_name('trench wurm', 'Trench Wurm').
card_type('trench wurm', 'Creature — Wurm').
card_types('trench wurm', ['Creature']).
card_subtypes('trench wurm', ['Wurm']).
card_colors('trench wurm', ['B']).
card_text('trench wurm', '{2}{R}, {T}: Destroy target nonbasic land.').
card_mana_cost('trench wurm', ['3', 'B']).
card_cmc('trench wurm', 4).
card_layout('trench wurm', 'normal').
card_power('trench wurm', 3).
card_toughness('trench wurm', 3).

% Found in: PCY
card_name('trenching steed', 'Trenching Steed').
card_type('trenching steed', 'Creature — Horse Rebel').
card_types('trenching steed', ['Creature']).
card_subtypes('trenching steed', ['Horse', 'Rebel']).
card_colors('trenching steed', ['W']).
card_text('trenching steed', 'Sacrifice a land: Trenching Steed gets +0/+3 until end of turn.').
card_mana_cost('trenching steed', ['3', 'W']).
card_cmc('trenching steed', 4).
card_layout('trenching steed', 'normal').
card_power('trenching steed', 2).
card_toughness('trenching steed', 3).

% Found in: ISD
card_name('trepanation blade', 'Trepanation Blade').
card_type('trepanation blade', 'Artifact — Equipment').
card_types('trepanation blade', ['Artifact']).
card_subtypes('trepanation blade', ['Equipment']).
card_colors('trepanation blade', []).
card_text('trepanation blade', 'Whenever equipped creature attacks, defending player reveals cards from the top of his or her library until he or she reveals a land card. The creature gets +1/+0 until end of turn for each card revealed this way. That player puts the revealed cards into his or her graveyard.\nEquip {2}').
card_mana_cost('trepanation blade', ['3']).
card_cmc('trepanation blade', 3).
card_layout('trepanation blade', 'normal').

% Found in: TSP
card_name('trespasser il-vec', 'Trespasser il-Vec').
card_type('trespasser il-vec', 'Creature — Human Rogue').
card_types('trespasser il-vec', ['Creature']).
card_subtypes('trespasser il-vec', ['Human', 'Rogue']).
card_colors('trespasser il-vec', ['B']).
card_text('trespasser il-vec', 'Discard a card: Trespasser il-Vec gains shadow until end of turn. (It can block or be blocked by only creatures with shadow.)').
card_mana_cost('trespasser il-vec', ['2', 'B']).
card_cmc('trespasser il-vec', 3).
card_layout('trespasser il-vec', 'normal').
card_power('trespasser il-vec', 3).
card_toughness('trespasser il-vec', 1).

% Found in: NPH
card_name('trespassing souleater', 'Trespassing Souleater').
card_type('trespassing souleater', 'Artifact Creature — Construct').
card_types('trespassing souleater', ['Artifact', 'Creature']).
card_subtypes('trespassing souleater', ['Construct']).
card_colors('trespassing souleater', []).
card_text('trespassing souleater', '{U/P}: Trespassing Souleater can\'t be blocked this turn. ({U/P} can be paid with either {U} or 2 life.)').
card_mana_cost('trespassing souleater', ['3']).
card_cmc('trespassing souleater', 3).
card_layout('trespassing souleater', 'normal').
card_power('trespassing souleater', 2).
card_toughness('trespassing souleater', 2).

% Found in: CSP
card_name('tresserhorn sinks', 'Tresserhorn Sinks').
card_type('tresserhorn sinks', 'Snow Land').
card_types('tresserhorn sinks', ['Land']).
card_subtypes('tresserhorn sinks', []).
card_supertypes('tresserhorn sinks', ['Snow']).
card_colors('tresserhorn sinks', []).
card_text('tresserhorn sinks', 'Tresserhorn Sinks enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('tresserhorn sinks', 'normal').

% Found in: CSP
card_name('tresserhorn skyknight', 'Tresserhorn Skyknight').
card_type('tresserhorn skyknight', 'Creature — Zombie Knight').
card_types('tresserhorn skyknight', ['Creature']).
card_subtypes('tresserhorn skyknight', ['Zombie', 'Knight']).
card_colors('tresserhorn skyknight', ['B']).
card_text('tresserhorn skyknight', 'Flying\nPrevent all damage that would be dealt to Tresserhorn Skyknight by creatures with first strike.').
card_mana_cost('tresserhorn skyknight', ['5', 'B', 'B']).
card_cmc('tresserhorn skyknight', 7).
card_layout('tresserhorn skyknight', 'normal').
card_power('tresserhorn skyknight', 5).
card_toughness('tresserhorn skyknight', 3).

% Found in: RTR
card_name('trestle troll', 'Trestle Troll').
card_type('trestle troll', 'Creature — Troll').
card_types('trestle troll', ['Creature']).
card_subtypes('trestle troll', ['Troll']).
card_colors('trestle troll', ['B', 'G']).
card_text('trestle troll', 'Defender\nReach (This creature can block creatures with flying.)\n{1}{B}{G}: Regenerate Trestle Troll.').
card_mana_cost('trestle troll', ['1', 'B', 'G']).
card_cmc('trestle troll', 3).
card_layout('trestle troll', 'normal').
card_power('trestle troll', 1).
card_toughness('trestle troll', 4).

% Found in: INV
card_name('treva\'s attendant', 'Treva\'s Attendant').
card_type('treva\'s attendant', 'Artifact Creature — Golem').
card_types('treva\'s attendant', ['Artifact', 'Creature']).
card_subtypes('treva\'s attendant', ['Golem']).
card_colors('treva\'s attendant', []).
card_text('treva\'s attendant', '{1}, Sacrifice Treva\'s Attendant: Add {G}{W}{U} to your mana pool.').
card_mana_cost('treva\'s attendant', ['5']).
card_cmc('treva\'s attendant', 5).
card_layout('treva\'s attendant', 'normal').
card_power('treva\'s attendant', 3).
card_toughness('treva\'s attendant', 3).

% Found in: DDE, PLS
card_name('treva\'s charm', 'Treva\'s Charm').
card_type('treva\'s charm', 'Instant').
card_types('treva\'s charm', ['Instant']).
card_subtypes('treva\'s charm', []).
card_colors('treva\'s charm', ['W', 'U', 'G']).
card_text('treva\'s charm', 'Choose one —\n• Destroy target enchantment.\n• Exile target attacking creature.\n• Draw a card, then discard a card.').
card_mana_cost('treva\'s charm', ['G', 'W', 'U']).
card_cmc('treva\'s charm', 3).
card_layout('treva\'s charm', 'normal').

% Found in: PLS
card_name('treva\'s ruins', 'Treva\'s Ruins').
card_type('treva\'s ruins', 'Land — Lair').
card_types('treva\'s ruins', ['Land']).
card_subtypes('treva\'s ruins', ['Lair']).
card_colors('treva\'s ruins', []).
card_text('treva\'s ruins', 'When Treva\'s Ruins enters the battlefield, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {G}, {W}, or {U} to your mana pool.').
card_layout('treva\'s ruins', 'normal').

% Found in: DDE, INV, pPRO
card_name('treva, the renewer', 'Treva, the Renewer').
card_type('treva, the renewer', 'Legendary Creature — Dragon').
card_types('treva, the renewer', ['Creature']).
card_subtypes('treva, the renewer', ['Dragon']).
card_supertypes('treva, the renewer', ['Legendary']).
card_colors('treva, the renewer', ['W', 'U', 'G']).
card_text('treva, the renewer', 'Flying\nWhenever Treva, the Renewer deals combat damage to a player, you may pay {2}{W}. If you do, choose a color, then you gain 1 life for each permanent of that color.').
card_mana_cost('treva, the renewer', ['3', 'G', 'W', 'U']).
card_cmc('treva, the renewer', 6).
card_layout('treva, the renewer', 'normal').
card_power('treva, the renewer', 6).
card_toughness('treva, the renewer', 6).

% Found in: THS
card_name('triad of fates', 'Triad of Fates').
card_type('triad of fates', 'Legendary Creature — Human Wizard').
card_types('triad of fates', ['Creature']).
card_subtypes('triad of fates', ['Human', 'Wizard']).
card_supertypes('triad of fates', ['Legendary']).
card_colors('triad of fates', ['W', 'B']).
card_text('triad of fates', '{1}, {T}: Put a fate counter on another target creature.\n{W}, {T}: Exile target creature that has a fate counter on it, then return it to the battlefield under its owner\'s control.\n{B}, {T}: Exile target creature that has a fate counter on it. Its controller draws two cards.').
card_mana_cost('triad of fates', ['2', 'W', 'B']).
card_cmc('triad of fates', 4).
card_layout('triad of fates', 'normal').
card_power('triad of fates', 3).
card_toughness('triad of fates', 3).

% Found in: DIS
card_name('trial', 'Trial').
card_type('trial', 'Instant').
card_types('trial', ['Instant']).
card_subtypes('trial', []).
card_colors('trial', ['W', 'U']).
card_text('trial', 'Return all creatures blocking or blocked by target creature to their owner\'s hand.').
card_mana_cost('trial', ['W', 'U']).
card_cmc('trial', 2).
card_layout('trial', 'split').
card_sides('trial', 'error').

% Found in: VIS, VMA
card_name('triangle of war', 'Triangle of War').
card_type('triangle of war', 'Artifact').
card_types('triangle of war', ['Artifact']).
card_subtypes('triangle of war', []).
card_colors('triangle of war', []).
card_text('triangle of war', '{2}, Sacrifice Triangle of War: Target creature you control fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_mana_cost('triangle of war', ['1']).
card_cmc('triangle of war', 1).
card_layout('triangle of war', 'normal').
card_reserved('triangle of war').

% Found in: CHR, LEG, ME4
card_name('triassic egg', 'Triassic Egg').
card_type('triassic egg', 'Artifact').
card_types('triassic egg', ['Artifact']).
card_subtypes('triassic egg', []).
card_colors('triassic egg', []).
card_text('triassic egg', '{3}, {T}: Put a hatchling counter on Triassic Egg.\nSacrifice Triassic Egg: Choose one —\n• You may put a creature card from your hand onto the battlefield. Activate this ability only if two or more hatchling counters are on Triassic Egg.\n• Return target creature card from your graveyard to the battlefield. Activate this ability only if two or more hatchling counters are on Triassic Egg.').
card_mana_cost('triassic egg', ['4']).
card_cmc('triassic egg', 4).
card_layout('triassic egg', 'normal').

% Found in: DDE, INV, MM2, MMA, TSB
card_name('tribal flames', 'Tribal Flames').
card_type('tribal flames', 'Sorcery').
card_types('tribal flames', ['Sorcery']).
card_subtypes('tribal flames', []).
card_colors('tribal flames', ['R']).
card_text('tribal flames', 'Domain — Tribal Flames deals X damage to target creature or player, where X is the number of basic land types among lands you control.').
card_mana_cost('tribal flames', ['1', 'R']).
card_cmc('tribal flames', 2).
card_layout('tribal flames', 'normal').

% Found in: LGN
card_name('tribal forcemage', 'Tribal Forcemage').
card_type('tribal forcemage', 'Creature — Elf Wizard').
card_types('tribal forcemage', ['Creature']).
card_subtypes('tribal forcemage', ['Elf', 'Wizard']).
card_colors('tribal forcemage', ['G']).
card_text('tribal forcemage', 'Morph {1}{G} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Tribal Forcemage is turned face up, creatures of the creature type of your choice get +2/+2 and gain trample until end of turn.').
card_mana_cost('tribal forcemage', ['1', 'G']).
card_cmc('tribal forcemage', 2).
card_layout('tribal forcemage', 'normal').
card_power('tribal forcemage', 1).
card_toughness('tribal forcemage', 1).

% Found in: ONS
card_name('tribal golem', 'Tribal Golem').
card_type('tribal golem', 'Artifact Creature — Golem').
card_types('tribal golem', ['Artifact', 'Creature']).
card_subtypes('tribal golem', ['Golem']).
card_colors('tribal golem', []).
card_text('tribal golem', 'Tribal Golem has trample as long as you control a Beast, haste as long as you control a Goblin, first strike as long as you control a Soldier, flying as long as you control a Wizard, and \"{B}: Regenerate Tribal Golem\" as long as you control a Zombie.').
card_mana_cost('tribal golem', ['6']).
card_cmc('tribal golem', 6).
card_layout('tribal golem', 'normal').
card_power('tribal golem', 4).
card_toughness('tribal golem', 4).

% Found in: HOP, ONS
card_name('tribal unity', 'Tribal Unity').
card_type('tribal unity', 'Instant').
card_types('tribal unity', ['Instant']).
card_subtypes('tribal unity', []).
card_colors('tribal unity', ['G']).
card_text('tribal unity', 'Creatures of the creature type of your choice get +X/+X until end of turn.').
card_mana_cost('tribal unity', ['X', '2', 'G']).
card_cmc('tribal unity', 3).
card_layout('tribal unity', 'normal').

% Found in: ISD
card_name('tribute to hunger', 'Tribute to Hunger').
card_type('tribute to hunger', 'Instant').
card_types('tribute to hunger', ['Instant']).
card_subtypes('tribute to hunger', []).
card_colors('tribute to hunger', ['B']).
card_text('tribute to hunger', 'Target opponent sacrifices a creature. You gain life equal to that creature\'s toughness.').
card_mana_cost('tribute to hunger', ['2', 'B']).
card_cmc('tribute to hunger', 3).
card_layout('tribute to hunger', 'normal').

% Found in: CMD, VMA
card_name('tribute to the wild', 'Tribute to the Wild').
card_type('tribute to the wild', 'Instant').
card_types('tribute to the wild', ['Instant']).
card_subtypes('tribute to the wild', []).
card_colors('tribute to the wild', ['G']).
card_text('tribute to the wild', 'Each opponent sacrifices an artifact or enchantment.').
card_mana_cost('tribute to the wild', ['1', 'G']).
card_cmc('tribute to the wild', 2).
card_layout('tribute to the wild', 'normal').

% Found in: TSP
card_name('trickbind', 'Trickbind').
card_type('trickbind', 'Instant').
card_types('trickbind', ['Instant']).
card_subtypes('trickbind', []).
card_colors('trickbind', ['U']).
card_text('trickbind', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nCounter target activated or triggered ability. If a permanent\'s ability is countered this way, activated abilities of that permanent can\'t be activated this turn. (Mana abilities can\'t be targeted.)').
card_mana_cost('trickbind', ['1', 'U']).
card_cmc('trickbind', 2).
card_layout('trickbind', 'normal').

% Found in: ONS
card_name('trickery charm', 'Trickery Charm').
card_type('trickery charm', 'Instant').
card_types('trickery charm', ['Instant']).
card_subtypes('trickery charm', []).
card_colors('trickery charm', ['U']).
card_text('trickery charm', 'Choose one —\n• Target creature gains flying until end of turn.\n• Target creature becomes the creature type of your choice until end of turn.\n• Look at the top four cards of your library, then put them back in any order.').
card_mana_cost('trickery charm', ['U']).
card_cmc('trickery charm', 1).
card_layout('trickery charm', 'normal').

% Found in: M13
card_name('tricks of the trade', 'Tricks of the Trade').
card_type('tricks of the trade', 'Enchantment — Aura').
card_types('tricks of the trade', ['Enchantment']).
card_subtypes('tricks of the trade', ['Aura']).
card_colors('tricks of the trade', ['U']).
card_text('tricks of the trade', 'Enchant creature\nEnchanted creature gets +2/+0 and can\'t be blocked.').
card_mana_cost('tricks of the trade', ['3', 'U']).
card_cmc('tricks of the trade', 4).
card_layout('tricks of the trade', 'normal').

% Found in: NMS
card_name('trickster mage', 'Trickster Mage').
card_type('trickster mage', 'Creature — Human Spellshaper').
card_types('trickster mage', ['Creature']).
card_subtypes('trickster mage', ['Human', 'Spellshaper']).
card_colors('trickster mage', ['U']).
card_text('trickster mage', '{U}, {T}, Discard a card: You may tap or untap target artifact, creature, or land.').
card_mana_cost('trickster mage', ['U']).
card_cmc('trickster mage', 1).
card_layout('trickster mage', 'normal').
card_power('trickster mage', 1).
card_toughness('trickster mage', 1).

% Found in: LRW
card_name('triclopean sight', 'Triclopean Sight').
card_type('triclopean sight', 'Enchantment — Aura').
card_types('triclopean sight', ['Enchantment']).
card_subtypes('triclopean sight', ['Aura']).
card_colors('triclopean sight', ['W']).
card_text('triclopean sight', 'Flash\nEnchant creature\nWhen Triclopean Sight enters the battlefield, untap enchanted creature.\nEnchanted creature gets +1/+1 and has vigilance.').
card_mana_cost('triclopean sight', ['1', 'W']).
card_cmc('triclopean sight', 2).
card_layout('triclopean sight', 'normal').

% Found in: SOM
card_name('trigon of corruption', 'Trigon of Corruption').
card_type('trigon of corruption', 'Artifact').
card_types('trigon of corruption', ['Artifact']).
card_subtypes('trigon of corruption', []).
card_colors('trigon of corruption', []).
card_text('trigon of corruption', 'Trigon of Corruption enters the battlefield with three charge counters on it.\n{B}{B}, {T}: Put a charge counter on Trigon of Corruption.\n{2}, {T}, Remove a charge counter from Trigon of Corruption: Put a -1/-1 counter on target creature.').
card_mana_cost('trigon of corruption', ['4']).
card_cmc('trigon of corruption', 4).
card_layout('trigon of corruption', 'normal').

% Found in: SOM
card_name('trigon of infestation', 'Trigon of Infestation').
card_type('trigon of infestation', 'Artifact').
card_types('trigon of infestation', ['Artifact']).
card_subtypes('trigon of infestation', []).
card_colors('trigon of infestation', []).
card_text('trigon of infestation', 'Trigon of Infestation enters the battlefield with three charge counters on it.\n{G}{G}, {T}: Put a charge counter on Trigon of Infestation.\n{2}, {T}, Remove a charge counter from Trigon of Infestation: Put a 1/1 green Insect creature token with infect onto the battlefield.').
card_mana_cost('trigon of infestation', ['4']).
card_cmc('trigon of infestation', 4).
card_layout('trigon of infestation', 'normal').

% Found in: SOM
card_name('trigon of mending', 'Trigon of Mending').
card_type('trigon of mending', 'Artifact').
card_types('trigon of mending', ['Artifact']).
card_subtypes('trigon of mending', []).
card_colors('trigon of mending', []).
card_text('trigon of mending', 'Trigon of Mending enters the battlefield with three charge counters on it.\n{W}{W}, {T}: Put a charge counter on Trigon of Mending.\n{2}, {T}, Remove a charge counter from Trigon of Mending: Target player gains 3 life.').
card_mana_cost('trigon of mending', ['2']).
card_cmc('trigon of mending', 2).
card_layout('trigon of mending', 'normal').

% Found in: SOM
card_name('trigon of rage', 'Trigon of Rage').
card_type('trigon of rage', 'Artifact').
card_types('trigon of rage', ['Artifact']).
card_subtypes('trigon of rage', []).
card_colors('trigon of rage', []).
card_text('trigon of rage', 'Trigon of Rage enters the battlefield with three charge counters on it.\n{R}{R}, {T}: Put a charge counter on Trigon of Rage.\n{2}, {T}, Remove a charge counter from Trigon of Rage: Target creature gets +3/+0 until end of turn.').
card_mana_cost('trigon of rage', ['2']).
card_cmc('trigon of rage', 2).
card_layout('trigon of rage', 'normal').

% Found in: SOM
card_name('trigon of thought', 'Trigon of Thought').
card_type('trigon of thought', 'Artifact').
card_types('trigon of thought', ['Artifact']).
card_subtypes('trigon of thought', []).
card_colors('trigon of thought', []).
card_text('trigon of thought', 'Trigon of Thought enters the battlefield with three charge counters on it.\n{U}{U}, {T}: Put a charge counter on Trigon of Thought.\n{2}, {T}, Remove a charge counter from Trigon of Thought: Draw a card.').
card_mana_cost('trigon of thought', ['5']).
card_cmc('trigon of thought', 5).
card_layout('trigon of thought', 'normal').

% Found in: DST, V09
card_name('trinisphere', 'Trinisphere').
card_type('trinisphere', 'Artifact').
card_types('trinisphere', ['Artifact']).
card_subtypes('trinisphere', []).
card_colors('trinisphere', []).
card_text('trinisphere', 'As long as Trinisphere is untapped, each spell that would cost less than three mana to cast costs three mana to cast. (Additional mana in the cost may be paid with any color of mana or colorless mana. For example, a spell that would cost {1}{B} to cast costs {2}{B} to cast instead.)').
card_mana_cost('trinisphere', ['3']).
card_cmc('trinisphere', 3).
card_layout('trinisphere', 'normal').

% Found in: 5DN, DDF, SOM
card_name('trinket mage', 'Trinket Mage').
card_type('trinket mage', 'Creature — Human Wizard').
card_types('trinket mage', ['Creature']).
card_subtypes('trinket mage', ['Human', 'Wizard']).
card_colors('trinket mage', ['U']).
card_text('trinket mage', 'When Trinket Mage enters the battlefield, you may search your library for an artifact card with converted mana cost 1 or less, reveal that card, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('trinket mage', ['2', 'U']).
card_cmc('trinket mage', 3).
card_layout('trinket mage', 'normal').
card_power('trinket mage', 2).
card_toughness('trinket mage', 2).

% Found in: DDF, SHM
card_name('trip noose', 'Trip Noose').
card_type('trip noose', 'Artifact').
card_types('trip noose', ['Artifact']).
card_subtypes('trip noose', []).
card_colors('trip noose', []).
card_text('trip noose', '{2}, {T}: Tap target creature.').
card_mana_cost('trip noose', ['2']).
card_cmc('trip noose', 2).
card_layout('trip noose', 'normal').

% Found in: ME3, PTK
card_name('trip wire', 'Trip Wire').
card_type('trip wire', 'Sorcery').
card_types('trip wire', ['Sorcery']).
card_subtypes('trip wire', []).
card_colors('trip wire', ['G']).
card_text('trip wire', 'Destroy target creature with horsemanship.').
card_mana_cost('trip wire', ['2', 'G']).
card_cmc('trip wire', 3).
card_layout('trip wire', 'normal').

% Found in: M15
card_name('triplicate spirits', 'Triplicate Spirits').
card_type('triplicate spirits', 'Sorcery').
card_types('triplicate spirits', ['Sorcery']).
card_subtypes('triplicate spirits', []).
card_colors('triplicate spirits', ['W']).
card_text('triplicate spirits', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPut three 1/1 white Spirit creature tokens with flying onto the battlefield. (They can\'t be blocked except by creatures with flying or reach.)').
card_mana_cost('triplicate spirits', ['4', 'W', 'W']).
card_cmc('triplicate spirits', 6).
card_layout('triplicate spirits', 'normal').

% Found in: CMD, TSP
card_name('triskelavus', 'Triskelavus').
card_type('triskelavus', 'Artifact Creature — Construct').
card_types('triskelavus', ['Artifact', 'Creature']).
card_subtypes('triskelavus', ['Construct']).
card_colors('triskelavus', []).
card_text('triskelavus', 'Flying\nTriskelavus enters the battlefield with three +1/+1 counters on it.\n{1}, Remove a +1/+1 counter from Triskelavus: Put a 1/1 colorless Triskelavite artifact creature token with flying onto the battlefield. It has \"Sacrifice this creature: This creature deals 1 damage to target creature or player.\"').
card_mana_cost('triskelavus', ['7']).
card_cmc('triskelavus', 7).
card_layout('triskelavus', 'normal').
card_power('triskelavus', 1).
card_toughness('triskelavus', 1).

% Found in: 4ED, ATQ, DDF, M11, MRD
card_name('triskelion', 'Triskelion').
card_type('triskelion', 'Artifact Creature — Construct').
card_types('triskelion', ['Artifact', 'Creature']).
card_subtypes('triskelion', ['Construct']).
card_colors('triskelion', []).
card_text('triskelion', 'Triskelion enters the battlefield with three +1/+1 counters on it.\nRemove a +1/+1 counter from Triskelion: Triskelion deals 1 damage to target creature or player.').
card_mana_cost('triskelion', ['6']).
card_cmc('triskelion', 6).
card_layout('triskelion', 'normal').
card_power('triskelion', 1).
card_toughness('triskelion', 1).

% Found in: JOU
card_name('triton cavalry', 'Triton Cavalry').
card_type('triton cavalry', 'Creature — Merfolk Soldier').
card_types('triton cavalry', ['Creature']).
card_subtypes('triton cavalry', ['Merfolk', 'Soldier']).
card_colors('triton cavalry', ['U']).
card_text('triton cavalry', 'Heroic — Whenever you cast a spell that targets Triton Cavalry, you may return target enchantment to its owner\'s hand.').
card_mana_cost('triton cavalry', ['3', 'U']).
card_cmc('triton cavalry', 4).
card_layout('triton cavalry', 'normal').
card_power('triton cavalry', 2).
card_toughness('triton cavalry', 4).

% Found in: THS
card_name('triton fortune hunter', 'Triton Fortune Hunter').
card_type('triton fortune hunter', 'Creature — Merfolk Soldier').
card_types('triton fortune hunter', ['Creature']).
card_subtypes('triton fortune hunter', ['Merfolk', 'Soldier']).
card_colors('triton fortune hunter', ['U']).
card_text('triton fortune hunter', 'Heroic — Whenever you cast a spell that targets Triton Fortune Hunter, draw a card.').
card_mana_cost('triton fortune hunter', ['2', 'U']).
card_cmc('triton fortune hunter', 3).
card_layout('triton fortune hunter', 'normal').
card_power('triton fortune hunter', 2).
card_toughness('triton fortune hunter', 2).

% Found in: JOU
card_name('triton shorestalker', 'Triton Shorestalker').
card_type('triton shorestalker', 'Creature — Merfolk Rogue').
card_types('triton shorestalker', ['Creature']).
card_subtypes('triton shorestalker', ['Merfolk', 'Rogue']).
card_colors('triton shorestalker', ['U']).
card_text('triton shorestalker', 'Triton Shorestalker can\'t be blocked.').
card_mana_cost('triton shorestalker', ['U']).
card_cmc('triton shorestalker', 1).
card_layout('triton shorestalker', 'normal').
card_power('triton shorestalker', 1).
card_toughness('triton shorestalker', 1).

% Found in: THS
card_name('triton shorethief', 'Triton Shorethief').
card_type('triton shorethief', 'Creature — Merfolk Rogue').
card_types('triton shorethief', ['Creature']).
card_subtypes('triton shorethief', ['Merfolk', 'Rogue']).
card_colors('triton shorethief', ['U']).
card_text('triton shorethief', '').
card_mana_cost('triton shorethief', ['U']).
card_cmc('triton shorethief', 1).
card_layout('triton shorethief', 'normal').
card_power('triton shorethief', 1).
card_toughness('triton shorethief', 2).

% Found in: THS
card_name('triton tactics', 'Triton Tactics').
card_type('triton tactics', 'Instant').
card_types('triton tactics', ['Instant']).
card_subtypes('triton tactics', []).
card_colors('triton tactics', ['U']).
card_text('triton tactics', 'Up to two target creatures each get +0/+3 until end of turn. Untap those creatures. At this turn\'s next end of combat, tap each creature that was blocked by one of those creatures this turn and it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('triton tactics', ['U']).
card_cmc('triton tactics', 1).
card_layout('triton tactics', 'normal').

% Found in: AVR
card_name('triumph of cruelty', 'Triumph of Cruelty').
card_type('triumph of cruelty', 'Enchantment').
card_types('triumph of cruelty', ['Enchantment']).
card_subtypes('triumph of cruelty', []).
card_colors('triumph of cruelty', ['B']).
card_text('triumph of cruelty', 'At the beginning of your upkeep, target opponent discards a card if you control the creature with the greatest power or tied for the greatest power.').
card_mana_cost('triumph of cruelty', ['2', 'B']).
card_cmc('triumph of cruelty', 3).
card_layout('triumph of cruelty', 'normal').

% Found in: AVR
card_name('triumph of ferocity', 'Triumph of Ferocity').
card_type('triumph of ferocity', 'Enchantment').
card_types('triumph of ferocity', ['Enchantment']).
card_subtypes('triumph of ferocity', []).
card_colors('triumph of ferocity', ['G']).
card_text('triumph of ferocity', 'At the beginning of your upkeep, draw a card if you control the creature with the greatest power or tied for the greatest power.').
card_mana_cost('triumph of ferocity', ['2', 'G']).
card_cmc('triumph of ferocity', 3).
card_layout('triumph of ferocity', 'normal').

% Found in: NPH
card_name('triumph of the hordes', 'Triumph of the Hordes').
card_type('triumph of the hordes', 'Sorcery').
card_types('triumph of the hordes', ['Sorcery']).
card_subtypes('triumph of the hordes', []).
card_colors('triumph of the hordes', ['G']).
card_text('triumph of the hordes', 'Until end of turn, creatures you control get +1/+1 and gain trample and infect. (Creatures with infect deal damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('triumph of the hordes', ['2', 'G', 'G']).
card_cmc('triumph of the hordes', 4).
card_layout('triumph of the hordes', 'normal').

% Found in: PO2
card_name('trokin high guard', 'Trokin High Guard').
card_type('trokin high guard', 'Creature — Human Knight').
card_types('trokin high guard', ['Creature']).
card_subtypes('trokin high guard', ['Human', 'Knight']).
card_colors('trokin high guard', ['W']).
card_text('trokin high guard', '').
card_mana_cost('trokin high guard', ['3', 'W']).
card_cmc('trokin high guard', 4).
card_layout('trokin high guard', 'normal').
card_power('trokin high guard', 3).
card_toughness('trokin high guard', 3).

% Found in: 10E, CMD, DDL, DPA, MRD
card_name('troll ascetic', 'Troll Ascetic').
card_type('troll ascetic', 'Creature — Troll Shaman').
card_types('troll ascetic', ['Creature']).
card_subtypes('troll ascetic', ['Troll', 'Shaman']).
card_colors('troll ascetic', ['G']).
card_text('troll ascetic', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\n{1}{G}: Regenerate Troll Ascetic.').
card_mana_cost('troll ascetic', ['1', 'G', 'G']).
card_cmc('troll ascetic', 3).
card_layout('troll ascetic', 'normal').
card_power('troll ascetic', 3).
card_toughness('troll ascetic', 2).

% Found in: INV
card_name('troll-horn cameo', 'Troll-Horn Cameo').
card_type('troll-horn cameo', 'Artifact').
card_types('troll-horn cameo', ['Artifact']).
card_subtypes('troll-horn cameo', []).
card_colors('troll-horn cameo', []).
card_text('troll-horn cameo', '{T}: Add {R} or {G} to your mana pool.').
card_mana_cost('troll-horn cameo', ['3']).
card_cmc('troll-horn cameo', 3).
card_layout('troll-horn cameo', 'normal').

% Found in: M12, M14
card_name('trollhide', 'Trollhide').
card_type('trollhide', 'Enchantment — Aura').
card_types('trollhide', ['Enchantment']).
card_subtypes('trollhide', ['Aura']).
card_colors('trollhide', ['G']).
card_text('trollhide', 'Enchant creature\nEnchanted creature gets +2/+2 and has \"{1}{G}: Regenerate this creature.\" (The next time the creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('trollhide', ['2', 'G']).
card_cmc('trollhide', 3).
card_layout('trollhide', 'normal').

% Found in: MRD
card_name('trolls of tel-jilad', 'Trolls of Tel-Jilad').
card_type('trolls of tel-jilad', 'Creature — Troll Shaman').
card_types('trolls of tel-jilad', ['Creature']).
card_subtypes('trolls of tel-jilad', ['Troll', 'Shaman']).
card_colors('trolls of tel-jilad', ['G']).
card_text('trolls of tel-jilad', '{1}{G}: Regenerate target green creature.').
card_mana_cost('trolls of tel-jilad', ['5', 'G', 'G']).
card_cmc('trolls of tel-jilad', 7).
card_layout('trolls of tel-jilad', 'normal').
card_power('trolls of tel-jilad', 5).
card_toughness('trolls of tel-jilad', 6).

% Found in: BNG, pLPA
card_name('tromokratis', 'Tromokratis').
card_type('tromokratis', 'Legendary Creature — Kraken').
card_types('tromokratis', ['Creature']).
card_subtypes('tromokratis', ['Kraken']).
card_supertypes('tromokratis', ['Legendary']).
card_colors('tromokratis', ['U']).
card_text('tromokratis', 'Tromokratis has hexproof unless it\'s attacking or blocking.\nTromokratis can\'t be blocked unless all creatures defending player controls block it. (If any creature that player controls doesn\'t block this creature, it can\'t be blocked.)').
card_mana_cost('tromokratis', ['5', 'U', 'U']).
card_cmc('tromokratis', 7).
card_layout('tromokratis', 'normal').
card_power('tromokratis', 8).
card_toughness('tromokratis', 8).

% Found in: MMA, TSP
card_name('tromp the domains', 'Tromp the Domains').
card_type('tromp the domains', 'Sorcery').
card_types('tromp the domains', ['Sorcery']).
card_subtypes('tromp the domains', []).
card_colors('tromp the domains', ['G']).
card_text('tromp the domains', 'Domain — Until end of turn, creatures you control gain trample and get +1/+1 for each basic land type among lands you control.').
card_mana_cost('tromp the domains', ['5', 'G']).
card_cmc('tromp the domains', 6).
card_layout('tromp the domains', 'normal').

% Found in: RAV
card_name('trophy hunter', 'Trophy Hunter').
card_type('trophy hunter', 'Creature — Human Archer').
card_types('trophy hunter', ['Creature']).
card_subtypes('trophy hunter', ['Human', 'Archer']).
card_colors('trophy hunter', ['G']).
card_text('trophy hunter', '{1}{G}: Trophy Hunter deals 1 damage to target creature with flying.\nWhenever a creature with flying dealt damage by Trophy Hunter this turn dies, put a +1/+1 counter on Trophy Hunter.').
card_mana_cost('trophy hunter', ['2', 'G']).
card_cmc('trophy hunter', 3).
card_layout('trophy hunter', 'normal').
card_power('trophy hunter', 2).
card_toughness('trophy hunter', 3).

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME3, ME4, VMA
card_name('tropical island', 'Tropical Island').
card_type('tropical island', 'Land — Forest Island').
card_types('tropical island', ['Land']).
card_subtypes('tropical island', ['Forest', 'Island']).
card_colors('tropical island', []).
card_text('tropical island', '({T}: Add {G} or {U} to your mana pool.)').
card_layout('tropical island', 'normal').
card_reserved('tropical island').

% Found in: MIR
card_name('tropical storm', 'Tropical Storm').
card_type('tropical storm', 'Sorcery').
card_types('tropical storm', ['Sorcery']).
card_subtypes('tropical storm', []).
card_colors('tropical storm', ['G']).
card_text('tropical storm', 'Tropical Storm deals X damage to each creature with flying and 1 additional damage to each blue creature.').
card_mana_cost('tropical storm', ['X', 'G']).
card_cmc('tropical storm', 1).
card_layout('tropical storm', 'normal').

% Found in: RTR
card_name('trostani\'s judgment', 'Trostani\'s Judgment').
card_type('trostani\'s judgment', 'Instant').
card_types('trostani\'s judgment', ['Instant']).
card_subtypes('trostani\'s judgment', []).
card_colors('trostani\'s judgment', ['W']).
card_text('trostani\'s judgment', 'Exile target creature, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('trostani\'s judgment', ['5', 'W']).
card_cmc('trostani\'s judgment', 6).
card_layout('trostani\'s judgment', 'normal').

% Found in: DGM, pMGD
card_name('trostani\'s summoner', 'Trostani\'s Summoner').
card_type('trostani\'s summoner', 'Creature — Elf Shaman').
card_types('trostani\'s summoner', ['Creature']).
card_subtypes('trostani\'s summoner', ['Elf', 'Shaman']).
card_colors('trostani\'s summoner', ['W', 'G']).
card_text('trostani\'s summoner', 'When Trostani\'s Summoner enters the battlefield, put a 2/2 white Knight creature token with vigilance, a 3/3 green Centaur creature token, and a 4/4 green Rhino creature token with trample onto the battlefield.').
card_mana_cost('trostani\'s summoner', ['5', 'G', 'W']).
card_cmc('trostani\'s summoner', 7).
card_layout('trostani\'s summoner', 'normal').
card_power('trostani\'s summoner', 1).
card_toughness('trostani\'s summoner', 1).

% Found in: RTR
card_name('trostani, selesnya\'s voice', 'Trostani, Selesnya\'s Voice').
card_type('trostani, selesnya\'s voice', 'Legendary Creature — Dryad').
card_types('trostani, selesnya\'s voice', ['Creature']).
card_subtypes('trostani, selesnya\'s voice', ['Dryad']).
card_supertypes('trostani, selesnya\'s voice', ['Legendary']).
card_colors('trostani, selesnya\'s voice', ['W', 'G']).
card_text('trostani, selesnya\'s voice', 'Whenever another creature enters the battlefield under your control, you gain life equal to that creature\'s toughness.\n{1}{G}{W}, {T}: Populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('trostani, selesnya\'s voice', ['G', 'G', 'W', 'W']).
card_cmc('trostani, selesnya\'s voice', 4).
card_layout('trostani, selesnya\'s voice', 'normal').
card_power('trostani, selesnya\'s voice', 2).
card_toughness('trostani, selesnya\'s voice', 5).

% Found in: DGM
card_name('trouble', 'Trouble').
card_type('trouble', 'Sorcery').
card_types('trouble', ['Sorcery']).
card_subtypes('trouble', []).
card_colors('trouble', ['R']).
card_text('trouble', 'Trouble deals damage to target player equal to the number of cards in that player\'s hand.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('trouble', ['2', 'R']).
card_cmc('trouble', 3).
card_layout('trouble', 'split').

% Found in: PCY
card_name('troubled healer', 'Troubled Healer').
card_type('troubled healer', 'Creature — Human Cleric').
card_types('troubled healer', ['Creature']).
card_subtypes('troubled healer', ['Human', 'Cleric']).
card_colors('troubled healer', ['W']).
card_text('troubled healer', 'Sacrifice a land: Prevent the next 2 damage that would be dealt to target creature or player this turn.').
card_mana_cost('troubled healer', ['2', 'W']).
card_cmc('troubled healer', 3).
card_layout('troubled healer', 'normal').
card_power('troubled healer', 1).
card_toughness('troubled healer', 2).

% Found in: PCY
card_name('troublesome spirit', 'Troublesome Spirit').
card_type('troublesome spirit', 'Creature — Spirit').
card_types('troublesome spirit', ['Creature']).
card_subtypes('troublesome spirit', ['Spirit']).
card_colors('troublesome spirit', ['U']).
card_text('troublesome spirit', 'Flying\nAt the beginning of your end step, tap all lands you control.').
card_mana_cost('troublesome spirit', ['2', 'U', 'U']).
card_cmc('troublesome spirit', 4).
card_layout('troublesome spirit', 'normal').
card_power('troublesome spirit', 3).
card_toughness('troublesome spirit', 4).

% Found in: 5ED, HML
card_name('truce', 'Truce').
card_type('truce', 'Instant').
card_types('truce', ['Instant']).
card_subtypes('truce', []).
card_colors('truce', ['W']).
card_text('truce', 'Each player may draw up to two cards. For each card less than two a player draws this way, that player gains 2 life.').
card_mana_cost('truce', ['2', 'W']).
card_cmc('truce', 3).
card_layout('truce', 'normal').

% Found in: 10E, ONS
card_name('true believer', 'True Believer').
card_type('true believer', 'Creature — Human Cleric').
card_types('true believer', ['Creature']).
card_subtypes('true believer', ['Human', 'Cleric']).
card_colors('true believer', ['W']).
card_text('true believer', 'You have shroud. (You can\'t be the target of spells or abilities.)').
card_mana_cost('true believer', ['W', 'W']).
card_cmc('true believer', 2).
card_layout('true believer', 'normal').
card_power('true believer', 2).
card_toughness('true believer', 2).

% Found in: C14, SOM
card_name('true conviction', 'True Conviction').
card_type('true conviction', 'Enchantment').
card_types('true conviction', ['Enchantment']).
card_subtypes('true conviction', []).
card_colors('true conviction', ['W']).
card_text('true conviction', 'Creatures you control have double strike and lifelink.').
card_mana_cost('true conviction', ['3', 'W', 'W', 'W']).
card_cmc('true conviction', 6).
card_layout('true conviction', 'normal').

% Found in: C13
card_name('true-name nemesis', 'True-Name Nemesis').
card_type('true-name nemesis', 'Creature — Merfolk Rogue').
card_types('true-name nemesis', ['Creature']).
card_subtypes('true-name nemesis', ['Merfolk', 'Rogue']).
card_colors('true-name nemesis', ['U']).
card_text('true-name nemesis', 'As True-Name Nemesis enters the battlefield, choose a player.\nTrue-Name Nemesis has protection from the chosen player. (This creature can\'t be blocked, targeted, dealt damage, or enchanted by anything controlled by that player.)').
card_mana_cost('true-name nemesis', ['1', 'U', 'U']).
card_cmc('true-name nemesis', 3).
card_layout('true-name nemesis', 'normal').
card_power('true-name nemesis', 3).
card_toughness('true-name nemesis', 1).

% Found in: DDL, GTC
card_name('truefire paladin', 'Truefire Paladin').
card_type('truefire paladin', 'Creature — Human Knight').
card_types('truefire paladin', ['Creature']).
card_subtypes('truefire paladin', ['Human', 'Knight']).
card_colors('truefire paladin', ['W', 'R']).
card_text('truefire paladin', 'Vigilance\n{R}{W}: Truefire Paladin gets +2/+0 until end of turn.\n{R}{W}: Truefire Paladin gains first strike until end of turn.').
card_mana_cost('truefire paladin', ['R', 'W']).
card_cmc('truefire paladin', 2).
card_layout('truefire paladin', 'normal').
card_power('truefire paladin', 2).
card_toughness('truefire paladin', 2).

% Found in: PC2
card_name('truga jungle', 'Truga Jungle').
card_type('truga jungle', 'Plane — Ergamon').
card_types('truga jungle', ['Plane']).
card_subtypes('truga jungle', ['Ergamon']).
card_colors('truga jungle', []).
card_text('truga jungle', 'All lands have \"{T}: Add one mana of any color to your mana pool.\"\nWhenever you roll {C}, reveal the top three cards of your library. Put all land cards revealed this way into your hand and the rest on the bottom of your library in any order.').
card_layout('truga jungle', 'plane').

% Found in: CNS, KTK, M10, M13, UDS
card_name('trumpet blast', 'Trumpet Blast').
card_type('trumpet blast', 'Instant').
card_types('trumpet blast', ['Instant']).
card_subtypes('trumpet blast', []).
card_colors('trumpet blast', ['R']).
card_text('trumpet blast', 'Attacking creatures get +2/+0 until end of turn.').
card_mana_cost('trumpet blast', ['2', 'R']).
card_cmc('trumpet blast', 3).
card_layout('trumpet blast', 'normal').

% Found in: BRB, TMP
card_name('trumpeting armodon', 'Trumpeting Armodon').
card_type('trumpeting armodon', 'Creature — Elephant').
card_types('trumpeting armodon', ['Creature']).
card_subtypes('trumpeting armodon', ['Elephant']).
card_colors('trumpeting armodon', ['G']).
card_text('trumpeting armodon', '{1}{G}: Target creature blocks Trumpeting Armodon this turn if able.').
card_mana_cost('trumpeting armodon', ['3', 'G']).
card_cmc('trumpeting armodon', 4).
card_layout('trumpeting armodon', 'normal').
card_power('trumpeting armodon', 3).
card_toughness('trumpeting armodon', 3).

% Found in: SOK
card_name('trusted advisor', 'Trusted Advisor').
card_type('trusted advisor', 'Creature — Human Advisor').
card_types('trusted advisor', ['Creature']).
card_subtypes('trusted advisor', ['Human', 'Advisor']).
card_colors('trusted advisor', ['U']).
card_text('trusted advisor', 'Your maximum hand size is increased by two.\nAt the beginning of your upkeep, return a blue creature you control to its owner\'s hand.').
card_mana_cost('trusted advisor', ['U']).
card_cmc('trusted advisor', 1).
card_layout('trusted advisor', 'normal').
card_power('trusted advisor', 1).
card_toughness('trusted advisor', 2).

% Found in: AVR
card_name('trusted forcemage', 'Trusted Forcemage').
card_type('trusted forcemage', 'Creature — Human Shaman').
card_types('trusted forcemage', ['Creature']).
card_subtypes('trusted forcemage', ['Human', 'Shaman']).
card_colors('trusted forcemage', ['G']).
card_text('trusted forcemage', 'Soulbond (You may pair this creature with another unpaired creature when either enters the battlefield. They remain paired for as long as you control both of them.)\nAs long as Trusted Forcemage is paired with another creature, each of those creatures gets +1/+1.').
card_mana_cost('trusted forcemage', ['2', 'G']).
card_cmc('trusted forcemage', 3).
card_layout('trusted forcemage', 'normal').
card_power('trusted forcemage', 2).
card_toughness('trusted forcemage', 2).

% Found in: ZEN
card_name('trusty machete', 'Trusty Machete').
card_type('trusty machete', 'Artifact — Equipment').
card_types('trusty machete', ['Artifact']).
card_subtypes('trusty machete', ['Equipment']).
card_colors('trusty machete', []).
card_text('trusty machete', 'Equipped creature gets +2/+1.\nEquip {2}').
card_mana_cost('trusty machete', ['1']).
card_cmc('trusty machete', 1).
card_layout('trusty machete', 'normal').

% Found in: TSP
card_name('truth or tale', 'Truth or Tale').
card_type('truth or tale', 'Instant').
card_types('truth or tale', ['Instant']).
card_subtypes('truth or tale', []).
card_colors('truth or tale', ['U']).
card_text('truth or tale', 'Reveal the top five cards of your library and separate them into two piles. An opponent chooses one of those piles. Put a card from the chosen pile into your hand, then put all other cards revealed this way on the bottom of your library in any order.').
card_mana_cost('truth or tale', ['1', 'U']).
card_cmc('truth or tale', 2).
card_layout('truth or tale', 'normal').

% Found in: DIS, MMA
card_name('trygon predator', 'Trygon Predator').
card_type('trygon predator', 'Creature — Beast').
card_types('trygon predator', ['Creature']).
card_subtypes('trygon predator', ['Beast']).
card_colors('trygon predator', ['U', 'G']).
card_text('trygon predator', 'Flying\nWhenever Trygon Predator deals combat damage to a player, you may destroy target artifact or enchantment that player controls.').
card_mana_cost('trygon predator', ['1', 'G', 'U']).
card_cmc('trygon predator', 3).
card_layout('trygon predator', 'normal').
card_power('trygon predator', 2).
card_toughness('trygon predator', 3).

% Found in: INV
card_name('tsabo tavoc', 'Tsabo Tavoc').
card_type('tsabo tavoc', 'Legendary Creature — Horror').
card_types('tsabo tavoc', ['Creature']).
card_subtypes('tsabo tavoc', ['Horror']).
card_supertypes('tsabo tavoc', ['Legendary']).
card_colors('tsabo tavoc', ['B', 'R']).
card_text('tsabo tavoc', 'First strike, protection from legendary creatures\n{B}{B}, {T}: Destroy target legendary creature. It can\'t be regenerated.').
card_mana_cost('tsabo tavoc', ['5', 'B', 'R']).
card_cmc('tsabo tavoc', 7).
card_layout('tsabo tavoc', 'normal').
card_power('tsabo tavoc', 7).
card_toughness('tsabo tavoc', 4).

% Found in: INV
card_name('tsabo\'s assassin', 'Tsabo\'s Assassin').
card_type('tsabo\'s assassin', 'Creature — Zombie Assassin').
card_types('tsabo\'s assassin', ['Creature']).
card_subtypes('tsabo\'s assassin', ['Zombie', 'Assassin']).
card_colors('tsabo\'s assassin', ['B']).
card_text('tsabo\'s assassin', '{T}: Destroy target creature if it shares a color with the most common color among all permanents or a color tied for most common. A creature destroyed this way can\'t be regenerated.').
card_mana_cost('tsabo\'s assassin', ['2', 'B', 'B']).
card_cmc('tsabo\'s assassin', 4).
card_layout('tsabo\'s assassin', 'normal').
card_power('tsabo\'s assassin', 1).
card_toughness('tsabo\'s assassin', 1).

% Found in: INV
card_name('tsabo\'s decree', 'Tsabo\'s Decree').
card_type('tsabo\'s decree', 'Instant').
card_types('tsabo\'s decree', ['Instant']).
card_subtypes('tsabo\'s decree', []).
card_colors('tsabo\'s decree', ['B']).
card_text('tsabo\'s decree', 'Choose a creature type. Target player reveals his or her hand and discards all creature cards of that type. Then destroy all creatures of that type that player controls. They can\'t be regenerated.').
card_mana_cost('tsabo\'s decree', ['5', 'B']).
card_cmc('tsabo\'s decree', 6).
card_layout('tsabo\'s decree', 'normal').

% Found in: INV
card_name('tsabo\'s web', 'Tsabo\'s Web').
card_type('tsabo\'s web', 'Artifact').
card_types('tsabo\'s web', ['Artifact']).
card_subtypes('tsabo\'s web', []).
card_colors('tsabo\'s web', []).
card_text('tsabo\'s web', 'When Tsabo\'s Web enters the battlefield, draw a card.\nEach land with an activated ability that isn\'t a mana ability doesn\'t untap during its controller\'s untap step.').
card_mana_cost('tsabo\'s web', ['2']).
card_cmc('tsabo\'s web', 2).
card_layout('tsabo\'s web', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, ME4
card_name('tsunami', 'Tsunami').
card_type('tsunami', 'Sorcery').
card_types('tsunami', ['Sorcery']).
card_subtypes('tsunami', []).
card_colors('tsunami', ['G']).
card_text('tsunami', 'Destroy all Islands.').
card_mana_cost('tsunami', ['3', 'G']).
card_cmc('tsunami', 4).
card_layout('tsunami', 'normal').

% Found in: CON, MM2, PC2
card_name('tukatongue thallid', 'Tukatongue Thallid').
card_type('tukatongue thallid', 'Creature — Fungus').
card_types('tukatongue thallid', ['Creature']).
card_subtypes('tukatongue thallid', ['Fungus']).
card_colors('tukatongue thallid', ['G']).
card_text('tukatongue thallid', 'When Tukatongue Thallid dies, put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('tukatongue thallid', ['G']).
card_cmc('tukatongue thallid', 1).
card_layout('tukatongue thallid', 'normal').
card_power('tukatongue thallid', 1).
card_toughness('tukatongue thallid', 1).

% Found in: LEG, ME3
card_name('tuknir deathlock', 'Tuknir Deathlock').
card_type('tuknir deathlock', 'Legendary Creature — Human Wizard').
card_types('tuknir deathlock', ['Creature']).
card_subtypes('tuknir deathlock', ['Human', 'Wizard']).
card_supertypes('tuknir deathlock', ['Legendary']).
card_colors('tuknir deathlock', ['R', 'G']).
card_text('tuknir deathlock', 'Flying\n{R}{G}, {T}: Target creature gets +2/+2 until end of turn.').
card_mana_cost('tuknir deathlock', ['R', 'R', 'G', 'G']).
card_cmc('tuknir deathlock', 4).
card_layout('tuknir deathlock', 'normal').
card_power('tuknir deathlock', 2).
card_toughness('tuknir deathlock', 2).
card_reserved('tuknir deathlock').

% Found in: ZEN
card_name('tuktuk grunts', 'Tuktuk Grunts').
card_type('tuktuk grunts', 'Creature — Goblin Warrior Ally').
card_types('tuktuk grunts', ['Creature']).
card_subtypes('tuktuk grunts', ['Goblin', 'Warrior', 'Ally']).
card_colors('tuktuk grunts', ['R']).
card_text('tuktuk grunts', 'Haste\nWhenever Tuktuk Grunts or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Tuktuk Grunts.').
card_mana_cost('tuktuk grunts', ['4', 'R']).
card_cmc('tuktuk grunts', 5).
card_layout('tuktuk grunts', 'normal').
card_power('tuktuk grunts', 2).
card_toughness('tuktuk grunts', 2).

% Found in: WWK
card_name('tuktuk scrapper', 'Tuktuk Scrapper').
card_type('tuktuk scrapper', 'Creature — Goblin Artificer Ally').
card_types('tuktuk scrapper', ['Creature']).
card_subtypes('tuktuk scrapper', ['Goblin', 'Artificer', 'Ally']).
card_colors('tuktuk scrapper', ['R']).
card_text('tuktuk scrapper', 'Whenever Tuktuk Scrapper or another Ally enters the battlefield under your control, you may destroy target artifact. If that artifact is put into a graveyard this way, Tuktuk Scrapper deals damage to that artifact\'s controller equal to the number of Allies you control.').
card_mana_cost('tuktuk scrapper', ['3', 'R']).
card_cmc('tuktuk scrapper', 4).
card_layout('tuktuk scrapper', 'normal').
card_power('tuktuk scrapper', 2).
card_toughness('tuktuk scrapper', 2).

% Found in: C14, ROE
card_name('tuktuk the explorer', 'Tuktuk the Explorer').
card_type('tuktuk the explorer', 'Legendary Creature — Goblin').
card_types('tuktuk the explorer', ['Creature']).
card_subtypes('tuktuk the explorer', ['Goblin']).
card_supertypes('tuktuk the explorer', ['Legendary']).
card_colors('tuktuk the explorer', ['R']).
card_text('tuktuk the explorer', 'Haste\nWhen Tuktuk the Explorer dies, put a legendary 5/5 colorless Goblin Golem artifact creature token named Tuktuk the Returned onto the battlefield.').
card_mana_cost('tuktuk the explorer', ['2', 'R']).
card_cmc('tuktuk the explorer', 3).
card_layout('tuktuk the explorer', 'normal').
card_power('tuktuk the explorer', 1).
card_toughness('tuktuk the explorer', 1).

% Found in: C13, PLC
card_name('tumble', 'Tumble').
card_type('tumble', 'Sorcery').
card_types('tumble', ['Sorcery']).
card_subtypes('tumble', []).
card_colors('tumble', ['R']).
card_text('tumble', 'Tumble deals 6 damage to each creature with flying.').
card_mana_cost('tumble', ['5', 'R']).
card_cmc('tumble', 6).
card_layout('tumble', 'split').

% Found in: MM2, SOM
card_name('tumble magnet', 'Tumble Magnet').
card_type('tumble magnet', 'Artifact').
card_types('tumble magnet', ['Artifact']).
card_subtypes('tumble magnet', []).
card_colors('tumble magnet', []).
card_text('tumble magnet', 'Tumble Magnet enters the battlefield with three charge counters on it.\n{T}, Remove a charge counter from Tumble Magnet: Tap target artifact or creature.').
card_mana_cost('tumble magnet', ['3']).
card_cmc('tumble magnet', 3).
card_layout('tumble magnet', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME2, ME4, VMA
card_name('tundra', 'Tundra').
card_type('tundra', 'Land — Plains Island').
card_types('tundra', ['Land']).
card_subtypes('tundra', ['Plains', 'Island']).
card_colors('tundra', []).
card_text('tundra', '({T}: Add {W} or {U} to your mana pool.)').
card_layout('tundra', 'normal').
card_reserved('tundra').

% Found in: APC
card_name('tundra kavu', 'Tundra Kavu').
card_type('tundra kavu', 'Creature — Kavu').
card_types('tundra kavu', ['Creature']).
card_subtypes('tundra kavu', ['Kavu']).
card_colors('tundra kavu', ['R']).
card_text('tundra kavu', '{T}: Target land becomes a Plains or an Island until end of turn.').
card_mana_cost('tundra kavu', ['2', 'R']).
card_cmc('tundra kavu', 3).
card_layout('tundra kavu', 'normal').
card_power('tundra kavu', 2).
card_toughness('tundra kavu', 2).

% Found in: 10E, 4ED, 5ED, 6ED, 8ED, LEG
card_name('tundra wolves', 'Tundra Wolves').
card_type('tundra wolves', 'Creature — Wolf').
card_types('tundra wolves', ['Creature']).
card_subtypes('tundra wolves', ['Wolf']).
card_colors('tundra wolves', ['W']).
card_text('tundra wolves', 'First strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('tundra wolves', ['W']).
card_cmc('tundra wolves', 1).
card_layout('tundra wolves', 'normal').
card_power('tundra wolves', 1).
card_toughness('tundra wolves', 1).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('tunnel', 'Tunnel').
card_type('tunnel', 'Instant').
card_types('tunnel', ['Instant']).
card_subtypes('tunnel', []).
card_colors('tunnel', ['R']).
card_text('tunnel', 'Destroy target Wall. It can\'t be regenerated.').
card_mana_cost('tunnel', ['R']).
card_cmc('tunnel', 1).
card_layout('tunnel', 'normal').

% Found in: SOM
card_name('tunnel ignus', 'Tunnel Ignus').
card_type('tunnel ignus', 'Creature — Elemental').
card_types('tunnel ignus', ['Creature']).
card_subtypes('tunnel ignus', ['Elemental']).
card_colors('tunnel ignus', ['R']).
card_text('tunnel ignus', 'Whenever a land enters the battlefield under an opponent\'s control, if that player had another land enter the battlefield under his or her control this turn, Tunnel Ignus deals 3 damage to that player.').
card_mana_cost('tunnel ignus', ['1', 'R']).
card_cmc('tunnel ignus', 2).
card_layout('tunnel ignus', 'normal').
card_power('tunnel ignus', 2).
card_toughness('tunnel ignus', 1).

% Found in: RAV
card_name('tunnel vision', 'Tunnel Vision').
card_type('tunnel vision', 'Sorcery').
card_types('tunnel vision', ['Sorcery']).
card_subtypes('tunnel vision', []).
card_colors('tunnel vision', ['U']).
card_text('tunnel vision', 'Name a card. Target player reveals cards from the top of his or her library until the named card is revealed. If it is, that player puts the rest of the revealed cards into his or her graveyard and puts the named card on top of his or her library. Otherwise, the player shuffles his or her library.').
card_mana_cost('tunnel vision', ['5', 'U']).
card_cmc('tunnel vision', 6).
card_layout('tunnel vision', 'normal').

% Found in: JUD
card_name('tunneler wurm', 'Tunneler Wurm').
card_type('tunneler wurm', 'Creature — Wurm').
card_types('tunneler wurm', ['Creature']).
card_subtypes('tunneler wurm', ['Wurm']).
card_colors('tunneler wurm', ['G']).
card_text('tunneler wurm', 'Discard a card: Regenerate Tunneler Wurm.').
card_mana_cost('tunneler wurm', ['6', 'G', 'G']).
card_cmc('tunneler wurm', 8).
card_layout('tunneler wurm', 'normal').
card_power('tunneler wurm', 6).
card_toughness('tunneler wurm', 6).

% Found in: BFZ
card_name('tunneling geopede', 'Tunneling Geopede').
card_type('tunneling geopede', 'Creature — Insect').
card_types('tunneling geopede', ['Creature']).
card_subtypes('tunneling geopede', ['Insect']).
card_colors('tunneling geopede', ['R']).
card_text('tunneling geopede', 'Landfall — Whenever a land enters the battlefield under your control, Tunneling Geopede deals 1 damage to each opponent.').
card_mana_cost('tunneling geopede', ['2', 'R']).
card_cmc('tunneling geopede', 3).
card_layout('tunneling geopede', 'normal').
card_power('tunneling geopede', 3).
card_toughness('tunneling geopede', 2).

% Found in: TOR
card_name('turbulent dreams', 'Turbulent Dreams').
card_type('turbulent dreams', 'Sorcery').
card_types('turbulent dreams', ['Sorcery']).
card_subtypes('turbulent dreams', []).
card_colors('turbulent dreams', ['U']).
card_text('turbulent dreams', 'As an additional cost to cast Turbulent Dreams, discard X cards.\nReturn X target nonland permanents to their owners\' hands.').
card_mana_cost('turbulent dreams', ['U', 'U']).
card_cmc('turbulent dreams', 2).
card_layout('turbulent dreams', 'normal').

% Found in: INV
card_name('turf wound', 'Turf Wound').
card_type('turf wound', 'Instant').
card_types('turf wound', ['Instant']).
card_subtypes('turf wound', []).
card_colors('turf wound', ['R']).
card_text('turf wound', 'Target player can\'t play land cards this turn.\nDraw a card.').
card_mana_cost('turf wound', ['2', 'R']).
card_cmc('turf wound', 3).
card_layout('turf wound', 'normal').

% Found in: DGM
card_name('turn', 'Turn').
card_type('turn', 'Instant').
card_types('turn', ['Instant']).
card_subtypes('turn', []).
card_colors('turn', ['U']).
card_text('turn', 'Until end of turn, target creature loses all abilities and becomes a red Weird with base power and toughness 0/1.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('turn', ['2', 'U']).
card_cmc('turn', 3).
card_layout('turn', 'split').
card_sides('turn', 'burn').

% Found in: BFZ
card_name('turn against', 'Turn Against').
card_type('turn against', 'Instant').
card_types('turn against', ['Instant']).
card_subtypes('turn against', []).
card_colors('turn against', []).
card_text('turn against', 'Devoid (This card has no color.)\nGain control of target creature until end of turn. Untap that creature. It gains haste until end of turn.').
card_mana_cost('turn against', ['4', 'R']).
card_cmc('turn against', 5).
card_layout('turn against', 'normal').

% Found in: SOM
card_name('turn aside', 'Turn Aside').
card_type('turn aside', 'Instant').
card_types('turn aside', ['Instant']).
card_subtypes('turn aside', []).
card_colors('turn aside', ['U']).
card_text('turn aside', 'Counter target spell that targets a permanent you control.').
card_mana_cost('turn aside', ['U']).
card_cmc('turn aside', 1).
card_layout('turn aside', 'normal').

% Found in: DST
card_name('turn the tables', 'Turn the Tables').
card_type('turn the tables', 'Instant').
card_types('turn the tables', ['Instant']).
card_subtypes('turn the tables', []).
card_colors('turn the tables', ['W']).
card_text('turn the tables', 'All combat damage that would be dealt to you this turn is dealt to target attacking creature instead.').
card_mana_cost('turn the tables', ['3', 'W', 'W']).
card_cmc('turn the tables', 5).
card_layout('turn the tables', 'normal').

% Found in: CNS, MBS
card_name('turn the tide', 'Turn the Tide').
card_type('turn the tide', 'Instant').
card_types('turn the tide', ['Instant']).
card_subtypes('turn the tide', []).
card_colors('turn the tide', ['U']).
card_text('turn the tide', 'Creatures your opponents control get -2/-0 until end of turn.').
card_mana_cost('turn the tide', ['1', 'U']).
card_cmc('turn the tide', 2).
card_layout('turn the tide', 'normal').

% Found in: MRD
card_name('turn to dust', 'Turn to Dust').
card_type('turn to dust', 'Instant').
card_types('turn to dust', ['Instant']).
card_subtypes('turn to dust', []).
card_colors('turn to dust', ['G']).
card_text('turn to dust', 'Destroy target Equipment. Add {G} to your mana pool.').
card_mana_cost('turn to dust', ['G']).
card_cmc('turn to dust', 1).
card_layout('turn to dust', 'normal').

% Found in: C14, M12, M15, ORI
card_name('turn to frog', 'Turn to Frog').
card_type('turn to frog', 'Instant').
card_types('turn to frog', ['Instant']).
card_subtypes('turn to frog', []).
card_colors('turn to frog', ['U']).
card_text('turn to frog', 'Until end of turn, target creature loses all abilities and becomes a blue Frog with base power and toughness 1/1.').
card_mana_cost('turn to frog', ['1', 'U']).
card_cmc('turn to frog', 2).
card_layout('turn to frog', 'normal').

% Found in: SHM
card_name('turn to mist', 'Turn to Mist').
card_type('turn to mist', 'Instant').
card_types('turn to mist', ['Instant']).
card_subtypes('turn to mist', []).
card_colors('turn to mist', ['W', 'U']).
card_text('turn to mist', 'Exile target creature. Return that card to the battlefield under its owner\'s control at the beginning of the next end step.').
card_mana_cost('turn to mist', ['1', 'W/U']).
card_cmc('turn to mist', 2).
card_layout('turn to mist', 'normal').

% Found in: M13, SOM
card_name('turn to slag', 'Turn to Slag').
card_type('turn to slag', 'Sorcery').
card_types('turn to slag', ['Sorcery']).
card_subtypes('turn to slag', []).
card_colors('turn to slag', ['R']).
card_text('turn to slag', 'Turn to Slag deals 5 damage to target creature. Destroy all Equipment attached to that creature.').
card_mana_cost('turn to slag', ['3', 'R', 'R']).
card_cmc('turn to slag', 5).
card_layout('turn to slag', 'normal').

% Found in: USG, VMA, pMEI
card_name('turnabout', 'Turnabout').
card_type('turnabout', 'Instant').
card_types('turnabout', ['Instant']).
card_subtypes('turnabout', []).
card_colors('turnabout', ['U']).
card_text('turnabout', 'Choose artifact, creature, or land. Tap all untapped permanents of the chosen type target player controls, or untap all tapped permanents of that type that player controls.').
card_mana_cost('turnabout', ['2', 'U', 'U']).
card_cmc('turnabout', 4).
card_layout('turnabout', 'normal').

% Found in: DDP, ZEN
card_name('turntimber basilisk', 'Turntimber Basilisk').
card_type('turntimber basilisk', 'Creature — Basilisk').
card_types('turntimber basilisk', ['Creature']).
card_subtypes('turntimber basilisk', ['Basilisk']).
card_colors('turntimber basilisk', ['G']).
card_text('turntimber basilisk', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)\nLandfall — Whenever a land enters the battlefield under your control, you may have target creature block Turntimber Basilisk this turn if able.').
card_mana_cost('turntimber basilisk', ['1', 'G', 'G']).
card_cmc('turntimber basilisk', 3).
card_layout('turntimber basilisk', 'normal').
card_power('turntimber basilisk', 2).
card_toughness('turntimber basilisk', 1).

% Found in: DDP, ZEN
card_name('turntimber grove', 'Turntimber Grove').
card_type('turntimber grove', 'Land').
card_types('turntimber grove', ['Land']).
card_subtypes('turntimber grove', []).
card_colors('turntimber grove', []).
card_text('turntimber grove', 'Turntimber Grove enters the battlefield tapped.\nWhen Turntimber Grove enters the battlefield, target creature gets +1/+1 until end of turn.\n{T}: Add {G} to your mana pool.').
card_layout('turntimber grove', 'normal').

% Found in: ZEN
card_name('turntimber ranger', 'Turntimber Ranger').
card_type('turntimber ranger', 'Creature — Elf Scout Ally').
card_types('turntimber ranger', ['Creature']).
card_subtypes('turntimber ranger', ['Elf', 'Scout', 'Ally']).
card_colors('turntimber ranger', ['G']).
card_text('turntimber ranger', 'Whenever Turntimber Ranger or another Ally enters the battlefield under your control, you may put a 2/2 green Wolf creature token onto the battlefield. If you do, put a +1/+1 counter on Turntimber Ranger.').
card_mana_cost('turntimber ranger', ['3', 'G', 'G']).
card_cmc('turntimber ranger', 5).
card_layout('turntimber ranger', 'normal').
card_power('turntimber ranger', 2).
card_toughness('turntimber ranger', 2).

% Found in: HOP
card_name('turri island', 'Turri Island').
card_type('turri island', 'Plane — Ir').
card_types('turri island', ['Plane']).
card_subtypes('turri island', ['Ir']).
card_colors('turri island', []).
card_text('turri island', 'Creature spells cost {2} less to cast.\nWhenever you roll {C}, reveal the top three cards of your library. Put all creature cards revealed this way into your hand and the rest into your graveyard.').
card_layout('turri island', 'plane').

% Found in: LRW
card_name('turtleshell changeling', 'Turtleshell Changeling').
card_type('turtleshell changeling', 'Creature — Shapeshifter').
card_types('turtleshell changeling', ['Creature']).
card_subtypes('turtleshell changeling', ['Shapeshifter']).
card_colors('turtleshell changeling', ['U']).
card_text('turtleshell changeling', 'Changeling (This card is every creature type.)\n{1}{U}: Switch Turtleshell Changeling\'s power and toughness until end of turn.').
card_mana_cost('turtleshell changeling', ['3', 'U']).
card_cmc('turtleshell changeling', 4).
card_layout('turtleshell changeling', 'normal').
card_power('turtleshell changeling', 1).
card_toughness('turtleshell changeling', 4).

% Found in: KTK
card_name('tusked colossodon', 'Tusked Colossodon').
card_type('tusked colossodon', 'Creature — Beast').
card_types('tusked colossodon', ['Creature']).
card_subtypes('tusked colossodon', ['Beast']).
card_colors('tusked colossodon', ['G']).
card_text('tusked colossodon', '').
card_mana_cost('tusked colossodon', ['4', 'G', 'G']).
card_cmc('tusked colossodon', 6).
card_layout('tusked colossodon', 'normal').
card_power('tusked colossodon', 6).
card_toughness('tusked colossodon', 5).

% Found in: KTK
card_name('tuskguard captain', 'Tuskguard Captain').
card_type('tuskguard captain', 'Creature — Human Warrior').
card_types('tuskguard captain', ['Creature']).
card_subtypes('tuskguard captain', ['Human', 'Warrior']).
card_colors('tuskguard captain', ['G']).
card_text('tuskguard captain', 'Outlast {G} ({G}, {T}: Put a +1/+1 counter on this creature. Outlast only as a sorcery.)\nEach creature you control with a +1/+1 counter on it has trample.').
card_mana_cost('tuskguard captain', ['2', 'G']).
card_cmc('tuskguard captain', 3).
card_layout('tuskguard captain', 'normal').
card_power('tuskguard captain', 2).
card_toughness('tuskguard captain', 3).

% Found in: 2ED, 4ED, 5ED, 7ED, 8ED, CED, CEI, ITP, LEA, LEB, RQS
card_name('twiddle', 'Twiddle').
card_type('twiddle', 'Instant').
card_types('twiddle', ['Instant']).
card_subtypes('twiddle', []).
card_colors('twiddle', ['U']).
card_text('twiddle', 'You may tap or untap target artifact, creature, or land.').
card_mana_cost('twiddle', ['U']).
card_cmc('twiddle', 1).
card_layout('twiddle', 'normal').

% Found in: ODY
card_name('twigwalker', 'Twigwalker').
card_type('twigwalker', 'Creature — Insect').
card_types('twigwalker', ['Creature']).
card_subtypes('twigwalker', ['Insect']).
card_colors('twigwalker', ['G']).
card_text('twigwalker', '{1}{G}, Sacrifice Twigwalker: Two target creatures each get +2/+2 until end of turn.').
card_mana_cost('twigwalker', ['2', 'G']).
card_cmc('twigwalker', 3).
card_layout('twigwalker', 'normal').
card_power('twigwalker', 2).
card_toughness('twigwalker', 2).

% Found in: DDK, RAV
card_name('twilight drover', 'Twilight Drover').
card_type('twilight drover', 'Creature — Spirit').
card_types('twilight drover', ['Creature']).
card_subtypes('twilight drover', ['Spirit']).
card_colors('twilight drover', ['W']).
card_text('twilight drover', 'Whenever a creature token leaves the battlefield, put a +1/+1 counter on Twilight Drover.\n{2}{W}, Remove a +1/+1 counter from Twilight Drover: Put two 1/1 white Spirit creature tokens with flying onto the battlefield.').
card_mana_cost('twilight drover', ['2', 'W']).
card_cmc('twilight drover', 3).
card_layout('twilight drover', 'normal').
card_power('twilight drover', 1).
card_toughness('twilight drover', 1).

% Found in: EVE
card_name('twilight mire', 'Twilight Mire').
card_type('twilight mire', 'Land').
card_types('twilight mire', ['Land']).
card_subtypes('twilight mire', []).
card_colors('twilight mire', []).
card_text('twilight mire', '{T}: Add {1} to your mana pool.\n{B/G}, {T}: Add {B}{B}, {B}{G}, or {G}{G} to your mana pool.').
card_layout('twilight mire', 'normal').

% Found in: C14, DD3_DVD, DDC, SHM
card_name('twilight shepherd', 'Twilight Shepherd').
card_type('twilight shepherd', 'Creature — Angel').
card_types('twilight shepherd', ['Creature']).
card_subtypes('twilight shepherd', ['Angel']).
card_colors('twilight shepherd', ['W']).
card_text('twilight shepherd', 'Flying, vigilance\nWhen Twilight Shepherd enters the battlefield, return to your hand all cards in your graveyard that were put there from the battlefield this turn.\nPersist (When this creature dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('twilight shepherd', ['3', 'W', 'W', 'W']).
card_cmc('twilight shepherd', 6).
card_layout('twilight shepherd', 'normal').
card_power('twilight shepherd', 5).
card_toughness('twilight shepherd', 5).

% Found in: DDJ, INV
card_name('twilight\'s call', 'Twilight\'s Call').
card_type('twilight\'s call', 'Sorcery').
card_types('twilight\'s call', ['Sorcery']).
card_subtypes('twilight\'s call', []).
card_colors('twilight\'s call', ['B']).
card_text('twilight\'s call', 'You may cast Twilight\'s Call as though it had flash if you pay {2} more to cast it. (You may cast it any time you could cast an instant.)\nEach player returns all creature cards from his or her graveyard to the battlefield.').
card_mana_cost('twilight\'s call', ['4', 'B', 'B']).
card_cmc('twilight\'s call', 6).
card_layout('twilight\'s call', 'normal').

% Found in: DTK
card_name('twin bolt', 'Twin Bolt').
card_type('twin bolt', 'Instant').
card_types('twin bolt', ['Instant']).
card_subtypes('twin bolt', []).
card_colors('twin bolt', ['R']).
card_text('twin bolt', 'Twin Bolt deals 2 damage divided as you choose among one or two target creatures and/or players.').
card_mana_cost('twin bolt', ['1', 'R']).
card_cmc('twin bolt', 2).
card_layout('twin bolt', 'normal').

% Found in: EVE
card_name('twinblade slasher', 'Twinblade Slasher').
card_type('twinblade slasher', 'Creature — Elf Warrior').
card_types('twinblade slasher', ['Creature']).
card_subtypes('twinblade slasher', ['Elf', 'Warrior']).
card_colors('twinblade slasher', ['G']).
card_text('twinblade slasher', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{1}{G}: Twinblade Slasher gets +2/+2 until end of turn. Activate this ability only once each turn.').
card_mana_cost('twinblade slasher', ['G']).
card_cmc('twinblade slasher', 1).
card_layout('twinblade slasher', 'normal').
card_power('twinblade slasher', 1).
card_toughness('twinblade slasher', 1).

% Found in: 10E, M10, SOK
card_name('twincast', 'Twincast').
card_type('twincast', 'Instant').
card_types('twincast', ['Instant']).
card_subtypes('twincast', []).
card_colors('twincast', ['U']).
card_text('twincast', 'Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_mana_cost('twincast', ['U', 'U']).
card_cmc('twincast', 2).
card_layout('twincast', 'normal').

% Found in: JOU
card_name('twinflame', 'Twinflame').
card_type('twinflame', 'Sorcery').
card_types('twinflame', ['Sorcery']).
card_subtypes('twinflame', []).
card_colors('twinflame', ['R']).
card_text('twinflame', 'Strive — Twinflame costs {2}{R} more to cast for each target beyond the first.\nChoose any number of target creatures you control. For each of them, put a token that\'s a copy of that creature onto the battlefield. Those tokens have haste. Exile them at the beginning of the next end step.').
card_mana_cost('twinflame', ['1', 'R']).
card_cmc('twinflame', 2).
card_layout('twinflame', 'normal').

% Found in: LRW
card_name('twinning glass', 'Twinning Glass').
card_type('twinning glass', 'Artifact').
card_types('twinning glass', ['Artifact']).
card_subtypes('twinning glass', []).
card_colors('twinning glass', []).
card_text('twinning glass', '{1}, {T}: You may cast a nonland card from your hand without paying its mana cost if it has the same name as a spell that was cast this turn.').
card_mana_cost('twinning glass', ['4']).
card_cmc('twinning glass', 4).
card_layout('twinning glass', 'normal').

% Found in: DIS
card_name('twinstrike', 'Twinstrike').
card_type('twinstrike', 'Instant').
card_types('twinstrike', ['Instant']).
card_subtypes('twinstrike', []).
card_colors('twinstrike', ['B', 'R']).
card_text('twinstrike', 'Twinstrike deals 2 damage to each of two target creatures.\nHellbent — Destroy those creatures instead if you have no cards in hand.').
card_mana_cost('twinstrike', ['3', 'B', 'R']).
card_cmc('twinstrike', 5).
card_layout('twinstrike', 'normal').

% Found in: BOK
card_name('twist allegiance', 'Twist Allegiance').
card_type('twist allegiance', 'Sorcery').
card_types('twist allegiance', ['Sorcery']).
card_subtypes('twist allegiance', []).
card_colors('twist allegiance', ['R']).
card_text('twist allegiance', 'You and target opponent each gain control of all creatures the other controls until end of turn. Untap those creatures. Those creatures gain haste until end of turn.').
card_mana_cost('twist allegiance', ['6', 'R']).
card_cmc('twist allegiance', 7).
card_layout('twist allegiance', 'normal').

% Found in: ARC, CNS, DD3_GVL, DDD, PD3, SCG, TSB
card_name('twisted abomination', 'Twisted Abomination').
card_type('twisted abomination', 'Creature — Zombie Mutant').
card_types('twisted abomination', ['Creature']).
card_subtypes('twisted abomination', ['Zombie', 'Mutant']).
card_colors('twisted abomination', ['B']).
card_text('twisted abomination', '{B}: Regenerate Twisted Abomination.\nSwampcycling {2} ({2}, Discard this card: Search your library for a Swamp card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('twisted abomination', ['5', 'B']).
card_cmc('twisted abomination', 6).
card_layout('twisted abomination', 'normal').
card_power('twisted abomination', 5).
card_toughness('twisted abomination', 3).

% Found in: UDS
card_name('twisted experiment', 'Twisted Experiment').
card_type('twisted experiment', 'Enchantment — Aura').
card_types('twisted experiment', ['Enchantment']).
card_subtypes('twisted experiment', ['Aura']).
card_colors('twisted experiment', ['B']).
card_text('twisted experiment', 'Enchant creature\nEnchanted creature gets +3/-1.').
card_mana_cost('twisted experiment', ['1', 'B']).
card_cmc('twisted experiment', 2).
card_layout('twisted experiment', 'normal').

% Found in: SOM
card_name('twisted image', 'Twisted Image').
card_type('twisted image', 'Instant').
card_types('twisted image', ['Instant']).
card_subtypes('twisted image', []).
card_colors('twisted image', ['U']).
card_text('twisted image', 'Switch target creature\'s power and toughness until end of turn.\nDraw a card.').
card_mana_cost('twisted image', ['U']).
card_cmc('twisted image', 1).
card_layout('twisted image', 'normal').

% Found in: RAV
card_name('twisted justice', 'Twisted Justice').
card_type('twisted justice', 'Sorcery').
card_types('twisted justice', ['Sorcery']).
card_subtypes('twisted justice', []).
card_colors('twisted justice', ['U', 'B']).
card_text('twisted justice', 'Target player sacrifices a creature. You draw cards equal to that creature\'s power.').
card_mana_cost('twisted justice', ['4', 'U', 'B']).
card_cmc('twisted justice', 6).
card_layout('twisted justice', 'normal').

% Found in: 10E, TMP, TPR, WWK
card_name('twitch', 'Twitch').
card_type('twitch', 'Instant').
card_types('twitch', ['Instant']).
card_subtypes('twitch', []).
card_colors('twitch', ['U']).
card_text('twitch', 'You may tap or untap target artifact, creature, or land.\nDraw a card.').
card_mana_cost('twitch', ['2', 'U']).
card_cmc('twitch', 3).
card_layout('twitch', 'normal').

% Found in: THS
card_name('two-headed cerberus', 'Two-Headed Cerberus').
card_type('two-headed cerberus', 'Creature — Hound').
card_types('two-headed cerberus', ['Creature']).
card_subtypes('two-headed cerberus', ['Hound']).
card_colors('two-headed cerberus', ['R']).
card_text('two-headed cerberus', 'Double strike (This creature deals both first-strike and regular combat damage.)').
card_mana_cost('two-headed cerberus', ['1', 'R', 'R']).
card_cmc('two-headed cerberus', 3).
card_layout('two-headed cerberus', 'normal').
card_power('two-headed cerberus', 1).
card_toughness('two-headed cerberus', 2).

% Found in: 8ED, ARC, DRB, MMQ, pSUS
card_name('two-headed dragon', 'Two-Headed Dragon').
card_type('two-headed dragon', 'Creature — Dragon').
card_types('two-headed dragon', ['Creature']).
card_subtypes('two-headed dragon', ['Dragon']).
card_colors('two-headed dragon', ['R']).
card_text('two-headed dragon', 'Flying\nMenace (This creature can\'t be blocked except by two or more creatures.)\nTwo-Headed Dragon can block an additional creature.\n{1}{R}: Two-Headed Dragon gets +2/+0 until end of turn.').
card_mana_cost('two-headed dragon', ['4', 'R', 'R']).
card_cmc('two-headed dragon', 6).
card_layout('two-headed dragon', 'normal').
card_power('two-headed dragon', 4).
card_toughness('two-headed dragon', 4).

% Found in: 2ED, CED, CEI, LEA, LEB, ME4
card_name('two-headed giant of foriys', 'Two-Headed Giant of Foriys').
card_type('two-headed giant of foriys', 'Creature — Giant').
card_types('two-headed giant of foriys', ['Creature']).
card_subtypes('two-headed giant of foriys', ['Giant']).
card_colors('two-headed giant of foriys', ['R']).
card_text('two-headed giant of foriys', 'Trample\nTwo-Headed Giant of Foriys can block an additional creature.').
card_mana_cost('two-headed giant of foriys', ['4', 'R']).
card_cmc('two-headed giant of foriys', 5).
card_layout('two-headed giant of foriys', 'normal').
card_power('two-headed giant of foriys', 4).
card_toughness('two-headed giant of foriys', 4).
card_reserved('two-headed giant of foriys').

% Found in: VAN
card_name('two-headed giant of foriys avatar', 'Two-Headed Giant of Foriys Avatar').
card_type('two-headed giant of foriys avatar', 'Vanguard').
card_types('two-headed giant of foriys avatar', ['Vanguard']).
card_subtypes('two-headed giant of foriys avatar', []).
card_colors('two-headed giant of foriys avatar', []).
card_text('two-headed giant of foriys avatar', 'Creatures you control have menace. (They can\'t be blocked except by two or more creatures.)\nEach creature you control can block an additional creature each combat.').
card_layout('two-headed giant of foriys avatar', 'vanguard').

% Found in: TSP
card_name('two-headed sliver', 'Two-Headed Sliver').
card_type('two-headed sliver', 'Creature — Sliver').
card_types('two-headed sliver', ['Creature']).
card_subtypes('two-headed sliver', ['Sliver']).
card_colors('two-headed sliver', ['R']).
card_text('two-headed sliver', 'All Sliver creatures have menace. (They can\'t be blocked except by two or more creatures.)').
card_mana_cost('two-headed sliver', ['1', 'R']).
card_cmc('two-headed sliver', 2).
card_layout('two-headed sliver', 'normal').
card_power('two-headed sliver', 1).
card_toughness('two-headed sliver', 1).

% Found in: THS
card_name('tymaret, the murder king', 'Tymaret, the Murder King').
card_type('tymaret, the murder king', 'Legendary Creature — Zombie Warrior').
card_types('tymaret, the murder king', ['Creature']).
card_subtypes('tymaret, the murder king', ['Zombie', 'Warrior']).
card_supertypes('tymaret, the murder king', ['Legendary']).
card_colors('tymaret, the murder king', ['B', 'R']).
card_text('tymaret, the murder king', '{1}{R}, Sacrifice another creature: Tymaret, the Murder King deals 2 damage to target player.\n{1}{B}, Sacrifice a creature: Return Tymaret from your graveyard to your hand.').
card_mana_cost('tymaret, the murder king', ['B', 'R']).
card_cmc('tymaret, the murder king', 2).
card_layout('tymaret, the murder king', 'normal').
card_power('tymaret, the murder king', 2).
card_toughness('tymaret, the murder king', 2).

% Found in: CNS, FRF, ISD, M15
card_name('typhoid rats', 'Typhoid Rats').
card_type('typhoid rats', 'Creature — Rat').
card_types('typhoid rats', ['Creature']).
card_subtypes('typhoid rats', ['Rat']).
card_colors('typhoid rats', ['B']).
card_text('typhoid rats', 'Deathtouch (Any amount of damage this deals to a creature is enough to destroy it.)').
card_mana_cost('typhoid rats', ['B']).
card_cmc('typhoid rats', 1).
card_layout('typhoid rats', 'normal').
card_power('typhoid rats', 1).
card_toughness('typhoid rats', 1).

% Found in: LEG
card_name('typhoon', 'Typhoon').
card_type('typhoon', 'Sorcery').
card_types('typhoon', ['Sorcery']).
card_subtypes('typhoon', []).
card_colors('typhoon', ['G']).
card_text('typhoon', 'Typhoon deals damage to each opponent equal to the number of Islands that player controls.').
card_mana_cost('typhoon', ['2', 'G']).
card_cmc('typhoon', 3).
card_layout('typhoon', 'normal').
card_reserved('typhoon').

% Found in: SHM
card_name('tyrannize', 'Tyrannize').
card_type('tyrannize', 'Sorcery').
card_types('tyrannize', ['Sorcery']).
card_subtypes('tyrannize', []).
card_colors('tyrannize', ['B', 'R']).
card_text('tyrannize', 'Target player discards his or her hand unless he or she pays 7 life.').
card_mana_cost('tyrannize', ['3', 'B/R', 'B/R']).
card_cmc('tyrannize', 5).
card_layout('tyrannize', 'normal').

% Found in: AVR
card_name('tyrant of discord', 'Tyrant of Discord').
card_type('tyrant of discord', 'Creature — Elemental').
card_types('tyrant of discord', ['Creature']).
card_subtypes('tyrant of discord', ['Elemental']).
card_colors('tyrant of discord', ['R']).
card_text('tyrant of discord', 'When Tyrant of Discord enters the battlefield, target opponent chooses a permanent he or she controls at random and sacrifices it. If a nonland permanent is sacrificed this way, repeat this process.').
card_mana_cost('tyrant of discord', ['4', 'R', 'R', 'R']).
card_cmc('tyrant of discord', 7).
card_layout('tyrant of discord', 'normal').
card_power('tyrant of discord', 7).
card_toughness('tyrant of discord', 7).

% Found in: CNS, VMA
card_name('tyrant\'s choice', 'Tyrant\'s Choice').
card_type('tyrant\'s choice', 'Sorcery').
card_types('tyrant\'s choice', ['Sorcery']).
card_subtypes('tyrant\'s choice', []).
card_colors('tyrant\'s choice', ['B']).
card_text('tyrant\'s choice', 'Will of the council — Starting with you, each player votes for death or torture. If death gets more votes, each opponent sacrifices a creature. If torture gets more votes or the vote is tied, each opponent loses 4 life.').
card_mana_cost('tyrant\'s choice', ['1', 'B']).
card_cmc('tyrant\'s choice', 2).
card_layout('tyrant\'s choice', 'normal').

% Found in: C14
card_name('tyrant\'s familiar', 'Tyrant\'s Familiar').
card_type('tyrant\'s familiar', 'Creature — Dragon').
card_types('tyrant\'s familiar', ['Creature']).
card_subtypes('tyrant\'s familiar', ['Dragon']).
card_colors('tyrant\'s familiar', ['R']).
card_text('tyrant\'s familiar', 'Flying, haste\nLieutenant — As long as you control your commander, Tyrant\'s Familiar gets +2/+2 and has \"Whenever Tyrant\'s Familiar attacks, it deals 7 damage to target creature defending player controls.\"').
card_mana_cost('tyrant\'s familiar', ['5', 'R', 'R']).
card_cmc('tyrant\'s familiar', 7).
card_layout('tyrant\'s familiar', 'normal').
card_power('tyrant\'s familiar', 5).
card_toughness('tyrant\'s familiar', 5).

% Found in: M15
card_name('tyrant\'s machine', 'Tyrant\'s Machine').
card_type('tyrant\'s machine', 'Artifact').
card_types('tyrant\'s machine', ['Artifact']).
card_subtypes('tyrant\'s machine', []).
card_colors('tyrant\'s machine', []).
card_text('tyrant\'s machine', '{4}, {T}: Tap target creature.').
card_mana_cost('tyrant\'s machine', ['2']).
card_cmc('tyrant\'s machine', 2).
card_layout('tyrant\'s machine', 'normal').

% Found in: 5DN
card_name('tyrranax', 'Tyrranax').
card_type('tyrranax', 'Creature — Beast').
card_types('tyrranax', ['Creature']).
card_subtypes('tyrranax', ['Beast']).
card_colors('tyrranax', ['G']).
card_text('tyrranax', '{1}{G}: Tyrranax gets -1/+1 until end of turn.').
card_mana_cost('tyrranax', ['4', 'G', 'G']).
card_cmc('tyrranax', 6).
card_layout('tyrranax', 'normal').
card_power('tyrranax', 5).
card_toughness('tyrranax', 4).

