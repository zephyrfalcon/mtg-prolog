% Card-specific data

% Found in: ONS
card_name('cabal archon', 'Cabal Archon').
card_type('cabal archon', 'Creature — Human Cleric').
card_types('cabal archon', ['Creature']).
card_subtypes('cabal archon', ['Human', 'Cleric']).
card_colors('cabal archon', ['B']).
card_text('cabal archon', '{B}, Sacrifice a Cleric: Target player loses 2 life and you gain 2 life.').
card_mana_cost('cabal archon', ['2', 'B']).
card_cmc('cabal archon', 3).
card_layout('cabal archon', 'normal').
card_power('cabal archon', 2).
card_toughness('cabal archon', 2).

% Found in: HOP, TOR, pFNM
card_name('cabal coffers', 'Cabal Coffers').
card_type('cabal coffers', 'Land').
card_types('cabal coffers', ['Land']).
card_subtypes('cabal coffers', []).
card_colors('cabal coffers', []).
card_text('cabal coffers', '{2}, {T}: Add {B} to your mana pool for each Swamp you control.').
card_layout('cabal coffers', 'normal').

% Found in: SCG
card_name('cabal conditioning', 'Cabal Conditioning').
card_type('cabal conditioning', 'Sorcery').
card_types('cabal conditioning', ['Sorcery']).
card_subtypes('cabal conditioning', []).
card_colors('cabal conditioning', ['B']).
card_text('cabal conditioning', 'Any number of target players each discard a number of cards equal to the highest converted mana cost among permanents you control.').
card_mana_cost('cabal conditioning', ['6', 'B']).
card_cmc('cabal conditioning', 7).
card_layout('cabal conditioning', 'normal').

% Found in: ONS
card_name('cabal executioner', 'Cabal Executioner').
card_type('cabal executioner', 'Creature — Human Cleric').
card_types('cabal executioner', ['Creature']).
card_subtypes('cabal executioner', ['Human', 'Cleric']).
card_colors('cabal executioner', ['B']).
card_text('cabal executioner', 'Whenever Cabal Executioner deals combat damage to a player, that player sacrifices a creature.\nMorph {3}{B}{B} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('cabal executioner', ['2', 'B', 'B']).
card_cmc('cabal executioner', 4).
card_layout('cabal executioner', 'normal').
card_power('cabal executioner', 2).
card_toughness('cabal executioner', 2).

% Found in: ODY
card_name('cabal inquisitor', 'Cabal Inquisitor').
card_type('cabal inquisitor', 'Creature — Human Minion').
card_types('cabal inquisitor', ['Creature']).
card_subtypes('cabal inquisitor', ['Human', 'Minion']).
card_colors('cabal inquisitor', ['B']).
card_text('cabal inquisitor', 'Threshold — {1}{B}, {T}, Exile two cards from your graveyard: Target player discards a card. Activate this ability only any time you could cast a sorcery, and only if seven or more cards are in your graveyard.').
card_mana_cost('cabal inquisitor', ['1', 'B']).
card_cmc('cabal inquisitor', 2).
card_layout('cabal inquisitor', 'normal').
card_power('cabal inquisitor', 1).
card_toughness('cabal inquisitor', 1).

% Found in: SCG
card_name('cabal interrogator', 'Cabal Interrogator').
card_type('cabal interrogator', 'Creature — Zombie Wizard').
card_types('cabal interrogator', ['Creature']).
card_subtypes('cabal interrogator', ['Zombie', 'Wizard']).
card_colors('cabal interrogator', ['B']).
card_text('cabal interrogator', '{X}{B}, {T}: Target player reveals X cards from his or her hand and you choose one of them. That player discards that card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('cabal interrogator', ['1', 'B']).
card_cmc('cabal interrogator', 2).
card_layout('cabal interrogator', 'normal').
card_power('cabal interrogator', 1).
card_toughness('cabal interrogator', 1).

% Found in: ODY
card_name('cabal patriarch', 'Cabal Patriarch').
card_type('cabal patriarch', 'Legendary Creature — Human Wizard').
card_types('cabal patriarch', ['Creature']).
card_subtypes('cabal patriarch', ['Human', 'Wizard']).
card_supertypes('cabal patriarch', ['Legendary']).
card_colors('cabal patriarch', ['B']).
card_text('cabal patriarch', '{2}{B}, Sacrifice a creature: Target creature gets -2/-2 until end of turn.\n{2}{B}, Exile a creature card from your graveyard: Target creature gets -2/-2 until end of turn.').
card_mana_cost('cabal patriarch', ['3', 'B', 'B', 'B']).
card_cmc('cabal patriarch', 6).
card_layout('cabal patriarch', 'normal').
card_power('cabal patriarch', 5).
card_toughness('cabal patriarch', 5).

% Found in: ODY
card_name('cabal pit', 'Cabal Pit').
card_type('cabal pit', 'Land').
card_types('cabal pit', ['Land']).
card_subtypes('cabal pit', []).
card_colors('cabal pit', []).
card_text('cabal pit', '{T}: Add {B} to your mana pool. Cabal Pit deals 1 damage to you.\nThreshold — {B}, {T}, Sacrifice Cabal Pit: Target creature gets -2/-2 until end of turn. Activate this ability only if seven or more cards are in your graveyard.').
card_layout('cabal pit', 'normal').

% Found in: TOR, VMA
card_name('cabal ritual', 'Cabal Ritual').
card_type('cabal ritual', 'Instant').
card_types('cabal ritual', ['Instant']).
card_subtypes('cabal ritual', []).
card_colors('cabal ritual', ['B']).
card_text('cabal ritual', 'Add {B}{B}{B} to your mana pool.\nThreshold — Add {B}{B}{B}{B}{B} to your mana pool instead if seven or more cards are in your graveyard.').
card_mana_cost('cabal ritual', ['1', 'B']).
card_cmc('cabal ritual', 2).
card_layout('cabal ritual', 'normal').

% Found in: ODY
card_name('cabal shrine', 'Cabal Shrine').
card_type('cabal shrine', 'Enchantment').
card_types('cabal shrine', ['Enchantment']).
card_subtypes('cabal shrine', []).
card_colors('cabal shrine', ['B']).
card_text('cabal shrine', 'Whenever a player casts a spell, that player discards X cards, where X is the number of cards in all graveyards with the same name as that spell.').
card_mana_cost('cabal shrine', ['1', 'B', 'B']).
card_cmc('cabal shrine', 3).
card_layout('cabal shrine', 'normal').

% Found in: ONS
card_name('cabal slaver', 'Cabal Slaver').
card_type('cabal slaver', 'Creature — Human Cleric').
card_types('cabal slaver', ['Creature']).
card_subtypes('cabal slaver', ['Human', 'Cleric']).
card_colors('cabal slaver', ['B']).
card_text('cabal slaver', 'Whenever a Goblin deals combat damage to a player, that player discards a card.').
card_mana_cost('cabal slaver', ['2', 'B']).
card_cmc('cabal slaver', 3).
card_layout('cabal slaver', 'normal').
card_power('cabal slaver', 2).
card_toughness('cabal slaver', 1).

% Found in: TOR
card_name('cabal surgeon', 'Cabal Surgeon').
card_type('cabal surgeon', 'Creature — Human Minion').
card_types('cabal surgeon', ['Creature']).
card_subtypes('cabal surgeon', ['Human', 'Minion']).
card_colors('cabal surgeon', ['B']).
card_text('cabal surgeon', '{2}{B}{B}, {T}, Exile two cards from your graveyard: Return target creature card from your graveyard to your hand.').
card_mana_cost('cabal surgeon', ['2', 'B', 'B']).
card_cmc('cabal surgeon', 4).
card_layout('cabal surgeon', 'normal').
card_power('cabal surgeon', 2).
card_toughness('cabal surgeon', 1).

% Found in: JUD, PD3, pFNM
card_name('cabal therapy', 'Cabal Therapy').
card_type('cabal therapy', 'Sorcery').
card_types('cabal therapy', ['Sorcery']).
card_subtypes('cabal therapy', []).
card_colors('cabal therapy', ['B']).
card_text('cabal therapy', 'Name a nonland card. Target player reveals his or her hand and discards all cards with that name.\nFlashback—Sacrifice a creature. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('cabal therapy', ['B']).
card_cmc('cabal therapy', 1).
card_layout('cabal therapy', 'normal').

% Found in: TOR
card_name('cabal torturer', 'Cabal Torturer').
card_type('cabal torturer', 'Creature — Human Minion').
card_types('cabal torturer', ['Creature']).
card_subtypes('cabal torturer', ['Human', 'Minion']).
card_colors('cabal torturer', ['B']).
card_text('cabal torturer', '{B}, {T}: Target creature gets -1/-1 until end of turn.\nThreshold — {3}{B}{B}, {T}: Target creature gets -2/-2 until end of turn. Activate this ability only if seven or more cards are in your graveyard.').
card_mana_cost('cabal torturer', ['1', 'B', 'B']).
card_cmc('cabal torturer', 3).
card_layout('cabal torturer', 'normal').
card_power('cabal torturer', 1).
card_toughness('cabal torturer', 1).

% Found in: JUD
card_name('cabal trainee', 'Cabal Trainee').
card_type('cabal trainee', 'Creature — Human Minion').
card_types('cabal trainee', ['Creature']).
card_subtypes('cabal trainee', ['Human', 'Minion']).
card_colors('cabal trainee', ['B']).
card_text('cabal trainee', 'Sacrifice Cabal Trainee: Target creature gets -2/-0 until end of turn.').
card_mana_cost('cabal trainee', ['B']).
card_cmc('cabal trainee', 1).
card_layout('cabal trainee', 'normal').
card_power('cabal trainee', 1).
card_toughness('cabal trainee', 1).

% Found in: DDI, EVE
card_name('cache raiders', 'Cache Raiders').
card_type('cache raiders', 'Creature — Merfolk Rogue').
card_types('cache raiders', ['Creature']).
card_subtypes('cache raiders', ['Merfolk', 'Rogue']).
card_colors('cache raiders', ['U']).
card_text('cache raiders', 'At the beginning of your upkeep, return a permanent you control to its owner\'s hand.').
card_mana_cost('cache raiders', ['3', 'U', 'U']).
card_cmc('cache raiders', 5).
card_layout('cache raiders', 'normal').
card_power('cache raiders', 4).
card_toughness('cache raiders', 4).

% Found in: FRF
card_name('cached defenses', 'Cached Defenses').
card_type('cached defenses', 'Sorcery').
card_types('cached defenses', ['Sorcery']).
card_subtypes('cached defenses', []).
card_colors('cached defenses', ['G']).
card_text('cached defenses', 'Bolster 3. (Choose a creature with the least toughness among creatures you control and put three +1/+1 counters on it.)').
card_mana_cost('cached defenses', ['2', 'G']).
card_cmc('cached defenses', 3).
card_layout('cached defenses', 'normal').

% Found in: C14, ISD
card_name('cackling counterpart', 'Cackling Counterpart').
card_type('cackling counterpart', 'Instant').
card_types('cackling counterpart', ['Instant']).
card_subtypes('cackling counterpart', []).
card_colors('cackling counterpart', ['U']).
card_text('cackling counterpart', 'Put a token onto the battlefield that\'s a copy of target creature you control.\nFlashback {5}{U}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('cackling counterpart', ['1', 'U', 'U']).
card_cmc('cackling counterpart', 3).
card_layout('cackling counterpart', 'normal').

% Found in: BRB, USG
card_name('cackling fiend', 'Cackling Fiend').
card_type('cackling fiend', 'Creature — Zombie').
card_types('cackling fiend', ['Creature']).
card_subtypes('cackling fiend', ['Zombie']).
card_colors('cackling fiend', ['B']).
card_text('cackling fiend', 'When Cackling Fiend enters the battlefield, each opponent discards a card.').
card_mana_cost('cackling fiend', ['2', 'B', 'B']).
card_cmc('cackling fiend', 4).
card_layout('cackling fiend', 'normal').
card_power('cackling fiend', 2).
card_toughness('cackling fiend', 1).

% Found in: DIS
card_name('cackling flames', 'Cackling Flames').
card_type('cackling flames', 'Instant').
card_types('cackling flames', ['Instant']).
card_subtypes('cackling flames', []).
card_colors('cackling flames', ['R']).
card_text('cackling flames', 'Cackling Flames deals 3 damage to target creature or player.\nHellbent — Cackling Flames deals 5 damage to that creature or player instead if you have no cards in hand.').
card_mana_cost('cackling flames', ['3', 'R']).
card_cmc('cackling flames', 4).
card_layout('cackling flames', 'normal').

% Found in: 5DN, DD3_DVD, DDC
card_name('cackling imp', 'Cackling Imp').
card_type('cackling imp', 'Creature — Imp').
card_types('cackling imp', ['Creature']).
card_subtypes('cackling imp', ['Imp']).
card_colors('cackling imp', ['B']).
card_text('cackling imp', 'Flying\n{T}: Target player loses 1 life.').
card_mana_cost('cackling imp', ['2', 'B', 'B']).
card_cmc('cackling imp', 4).
card_layout('cackling imp', 'normal').
card_power('cackling imp', 2).
card_toughness('cackling imp', 2).

% Found in: MMQ
card_name('cackling witch', 'Cackling Witch').
card_type('cackling witch', 'Creature — Human Spellshaper').
card_types('cackling witch', ['Creature']).
card_subtypes('cackling witch', ['Human', 'Spellshaper']).
card_colors('cackling witch', ['B']).
card_text('cackling witch', '{X}{B}, {T}, Discard a card: Target creature gets +X/+0 until end of turn.').
card_mana_cost('cackling witch', ['1', 'B']).
card_cmc('cackling witch', 2).
card_layout('cackling witch', 'normal').
card_power('cackling witch', 1).
card_toughness('cackling witch', 1).

% Found in: DDP, PC2, ROE
card_name('cadaver imp', 'Cadaver Imp').
card_type('cadaver imp', 'Creature — Imp').
card_types('cadaver imp', ['Creature']).
card_subtypes('cadaver imp', ['Imp']).
card_colors('cadaver imp', ['B']).
card_text('cadaver imp', 'Flying\nWhen Cadaver Imp enters the battlefield, you may return target creature card from your graveyard to your hand.').
card_mana_cost('cadaver imp', ['1', 'B', 'B']).
card_cmc('cadaver imp', 3).
card_layout('cadaver imp', 'normal').
card_power('cadaver imp', 1).
card_toughness('cadaver imp', 1).

% Found in: MIR
card_name('cadaverous bloom', 'Cadaverous Bloom').
card_type('cadaverous bloom', 'Enchantment').
card_types('cadaverous bloom', ['Enchantment']).
card_subtypes('cadaverous bloom', []).
card_colors('cadaverous bloom', ['B', 'G']).
card_text('cadaverous bloom', 'Exile a card from your hand: Add {B}{B} or {G}{G} to your mana pool.').
card_mana_cost('cadaverous bloom', ['3', 'B', 'G']).
card_cmc('cadaverous bloom', 5).
card_layout('cadaverous bloom', 'normal').
card_reserved('cadaverous bloom').

% Found in: HOP, MIR
card_name('cadaverous knight', 'Cadaverous Knight').
card_type('cadaverous knight', 'Creature — Zombie Knight').
card_types('cadaverous knight', ['Creature']).
card_subtypes('cadaverous knight', ['Zombie', 'Knight']).
card_colors('cadaverous knight', ['B']).
card_text('cadaverous knight', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\n{1}{B}{B}: Regenerate Cadaverous Knight.').
card_mana_cost('cadaverous knight', ['2', 'B']).
card_cmc('cadaverous knight', 3).
card_layout('cadaverous knight', 'normal').
card_power('cadaverous knight', 2).
card_toughness('cadaverous knight', 2).

% Found in: CHK, PC2
card_name('cage of hands', 'Cage of Hands').
card_type('cage of hands', 'Enchantment — Aura').
card_types('cage of hands', ['Enchantment']).
card_subtypes('cage of hands', ['Aura']).
card_colors('cage of hands', ['W']).
card_text('cage of hands', 'Enchant creature\nEnchanted creature can\'t attack or block.\n{1}{W}: Return Cage of Hands to its owner\'s hand.').
card_mana_cost('cage of hands', ['2', 'W']).
card_cmc('cage of hands', 3).
card_layout('cage of hands', 'normal').

% Found in: C14, NPH
card_name('caged sun', 'Caged Sun').
card_type('caged sun', 'Artifact').
card_types('caged sun', ['Artifact']).
card_subtypes('caged sun', []).
card_colors('caged sun', []).
card_text('caged sun', 'As Caged Sun enters the battlefield, choose a color.\nCreatures you control of the chosen color get +1/+1.\nWhenever a land\'s ability adds one or more mana of the chosen color to your mana pool, add one additional mana of that color to your mana pool.').
card_mana_cost('caged sun', ['6']).
card_cmc('caged sun', 6).
card_layout('caged sun', 'normal').

% Found in: JUD
card_name('cagemail', 'Cagemail').
card_type('cagemail', 'Enchantment — Aura').
card_types('cagemail', ['Enchantment']).
card_subtypes('cagemail', ['Aura']).
card_colors('cagemail', ['W']).
card_text('cagemail', 'Enchant creature\nEnchanted creature gets +2/+2 and can\'t attack.').
card_mana_cost('cagemail', ['1', 'W']).
card_cmc('cagemail', 2).
card_layout('cagemail', 'normal').

% Found in: LRW
card_name('cairn wanderer', 'Cairn Wanderer').
card_type('cairn wanderer', 'Creature — Shapeshifter').
card_types('cairn wanderer', ['Creature']).
card_subtypes('cairn wanderer', ['Shapeshifter']).
card_colors('cairn wanderer', ['B']).
card_text('cairn wanderer', 'Changeling (This card is every creature type.)\nAs long as a creature card with flying is in a graveyard, Cairn Wanderer has flying. The same is true for fear, first strike, double strike, deathtouch, haste, landwalk, lifelink, protection, reach, trample, shroud, and vigilance.').
card_mana_cost('cairn wanderer', ['4', 'B']).
card_cmc('cairn wanderer', 5).
card_layout('cairn wanderer', 'normal').
card_power('cairn wanderer', 4).
card_toughness('cairn wanderer', 4).

% Found in: PLC, pGTW
card_name('calciderm', 'Calciderm').
card_type('calciderm', 'Creature — Beast').
card_types('calciderm', ['Creature']).
card_subtypes('calciderm', ['Beast']).
card_colors('calciderm', ['W']).
card_text('calciderm', 'Shroud (This creature can\'t be the target of spells or abilities.)\nVanishing 4 (This permanent enters the battlefield with four time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)').
card_mana_cost('calciderm', ['2', 'W', 'W']).
card_cmc('calciderm', 4).
card_layout('calciderm', 'normal').
card_power('calciderm', 5).
card_toughness('calciderm', 5).

% Found in: TSP
card_name('calciform pools', 'Calciform Pools').
card_type('calciform pools', 'Land').
card_types('calciform pools', ['Land']).
card_subtypes('calciform pools', []).
card_colors('calciform pools', []).
card_text('calciform pools', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Calciform Pools.\n{1}, Remove X storage counters from Calciform Pools: Add X mana in any combination of {W} and/or {U} to your mana pool.').
card_layout('calciform pools', 'normal').

% Found in: WWK
card_name('calcite snapper', 'Calcite Snapper').
card_type('calcite snapper', 'Creature — Turtle').
card_types('calcite snapper', ['Creature']).
card_subtypes('calcite snapper', ['Turtle']).
card_colors('calcite snapper', ['U']).
card_text('calcite snapper', 'Shroud (This creature can\'t be the target of spells or abilities.)\nLandfall — Whenever a land enters the battlefield under your control, you may switch Calcite Snapper\'s power and toughness until end of turn.').
card_mana_cost('calcite snapper', ['1', 'U', 'U']).
card_cmc('calcite snapper', 3).
card_layout('calcite snapper', 'normal').
card_power('calcite snapper', 1).
card_toughness('calcite snapper', 4).

% Found in: ORI
card_name('calculated dismissal', 'Calculated Dismissal').
card_type('calculated dismissal', 'Instant').
card_types('calculated dismissal', ['Instant']).
card_subtypes('calculated dismissal', []).
card_colors('calculated dismissal', ['U']).
card_text('calculated dismissal', 'Counter target spell unless its controller pays {3}.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, scry 2. (To scry 2, look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('calculated dismissal', ['2', 'U']).
card_cmc('calculated dismissal', 3).
card_layout('calculated dismissal', 'normal').

% Found in: ALA
card_name('caldera hellion', 'Caldera Hellion').
card_type('caldera hellion', 'Creature — Hellion').
card_types('caldera hellion', ['Creature']).
card_subtypes('caldera hellion', ['Hellion']).
card_colors('caldera hellion', ['R']).
card_text('caldera hellion', 'Devour 1 (As this enters the battlefield, you may sacrifice any number of creatures. This creature enters the battlefield with that many +1/+1 counters on it.)\nWhen Caldera Hellion enters the battlefield, it deals 3 damage to each creature.').
card_mana_cost('caldera hellion', ['3', 'R', 'R']).
card_cmc('caldera hellion', 5).
card_layout('caldera hellion', 'normal').
card_power('caldera hellion', 3).
card_toughness('caldera hellion', 3).

% Found in: PLS
card_name('caldera kavu', 'Caldera Kavu').
card_type('caldera kavu', 'Creature — Kavu').
card_types('caldera kavu', ['Creature']).
card_subtypes('caldera kavu', ['Kavu']).
card_colors('caldera kavu', ['R']).
card_text('caldera kavu', '{1}{B}: Caldera Kavu gets +1/+1 until end of turn.\n{G}: Caldera Kavu becomes the color of your choice until end of turn.').
card_mana_cost('caldera kavu', ['2', 'R']).
card_cmc('caldera kavu', 3).
card_layout('caldera kavu', 'normal').
card_power('caldera kavu', 2).
card_toughness('caldera kavu', 2).

% Found in: TMP, TPR, VMA
card_name('caldera lake', 'Caldera Lake').
card_type('caldera lake', 'Land').
card_types('caldera lake', ['Land']).
card_subtypes('caldera lake', []).
card_colors('caldera lake', []).
card_text('caldera lake', 'Caldera Lake enters the battlefield tapped.\n{T}: Add {1} to your mana pool.\n{T}: Add {U} or {R} to your mana pool. Caldera Lake deals 1 damage to you.').
card_layout('caldera lake', 'normal').

% Found in: DGM
card_name('call', 'Call').
card_type('call', 'Sorcery').
card_types('call', ['Sorcery']).
card_subtypes('call', []).
card_colors('call', ['W', 'U']).
card_text('call', 'Put four 1/1 white Bird creature tokens with flying onto the battlefield.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('call', ['4', 'W', 'U']).
card_cmc('call', 6).
card_layout('call', 'split').

% Found in: BOK
card_name('call for blood', 'Call for Blood').
card_type('call for blood', 'Instant — Arcane').
card_types('call for blood', ['Instant']).
card_subtypes('call for blood', ['Arcane']).
card_colors('call for blood', ['B']).
card_text('call for blood', 'As an additional cost to cast Call for Blood, sacrifice a creature.\nTarget creature gets -X/-X until end of turn, where X is the sacrificed creature\'s power.').
card_mana_cost('call for blood', ['4', 'B']).
card_cmc('call for blood', 5).
card_layout('call for blood', 'normal').

% Found in: RTR, pFNM
card_name('call of the conclave', 'Call of the Conclave').
card_type('call of the conclave', 'Sorcery').
card_types('call of the conclave', ['Sorcery']).
card_subtypes('call of the conclave', []).
card_colors('call of the conclave', ['W', 'G']).
card_text('call of the conclave', 'Put a 3/3 green Centaur creature token onto the battlefield.').
card_mana_cost('call of the conclave', ['G', 'W']).
card_cmc('call of the conclave', 2).
card_layout('call of the conclave', 'normal').

% Found in: ORI
card_name('call of the full moon', 'Call of the Full Moon').
card_type('call of the full moon', 'Enchantment — Aura').
card_types('call of the full moon', ['Enchantment']).
card_subtypes('call of the full moon', ['Aura']).
card_colors('call of the full moon', ['R']).
card_text('call of the full moon', 'Enchant creature\nEnchanted creature gets +3/+2 and has trample. (It can deal excess combat damage to defending player or planeswalker while attacking.)\nAt the beginning of each upkeep, if a player cast two or more spells last turn, sacrifice Call of the Full Moon.').
card_mana_cost('call of the full moon', ['1', 'R']).
card_cmc('call of the full moon', 2).
card_layout('call of the full moon', 'normal').

% Found in: ODY, TSB, pGPX
card_name('call of the herd', 'Call of the Herd').
card_type('call of the herd', 'Sorcery').
card_types('call of the herd', ['Sorcery']).
card_subtypes('call of the herd', []).
card_colors('call of the herd', ['G']).
card_text('call of the herd', 'Put a 3/3 green Elephant creature token onto the battlefield.\nFlashback {3}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('call of the herd', ['2', 'G']).
card_cmc('call of the herd', 3).
card_layout('call of the herd', 'normal').

% Found in: GTC
card_name('call of the nightwing', 'Call of the Nightwing').
card_type('call of the nightwing', 'Sorcery').
card_types('call of the nightwing', ['Sorcery']).
card_subtypes('call of the nightwing', []).
card_colors('call of the nightwing', ['U', 'B']).
card_text('call of the nightwing', 'Put a 1/1 blue and black Horror creature token with flying onto the battlefield.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('call of the nightwing', ['2', 'U', 'B']).
card_cmc('call of the nightwing', 4).
card_layout('call of the nightwing', 'normal').

% Found in: 6ED, 8ED, WTH
card_name('call of the wild', 'Call of the Wild').
card_type('call of the wild', 'Enchantment').
card_types('call of the wild', ['Enchantment']).
card_subtypes('call of the wild', []).
card_colors('call of the wild', ['G']).
card_text('call of the wild', '{2}{G}{G}: Reveal the top card of your library. If it\'s a creature card, put it onto the battlefield. Otherwise, put it into your graveyard.').
card_mana_cost('call of the wild', ['2', 'G', 'G']).
card_cmc('call of the wild', 4).
card_layout('call of the wild', 'normal').

% Found in: BFZ
card_name('call the scions', 'Call the Scions').
card_type('call the scions', 'Sorcery').
card_types('call the scions', ['Sorcery']).
card_subtypes('call the scions', []).
card_colors('call the scions', []).
card_text('call the scions', 'Devoid (This card has no color.)\nPut two 1/1 colorless Eldrazi Scion creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('call the scions', ['2', 'G']).
card_cmc('call the scions', 3).
card_layout('call the scions', 'normal').

% Found in: CMD, EVE
card_name('call the skybreaker', 'Call the Skybreaker').
card_type('call the skybreaker', 'Sorcery').
card_types('call the skybreaker', ['Sorcery']).
card_subtypes('call the skybreaker', []).
card_colors('call the skybreaker', ['U', 'R']).
card_text('call the skybreaker', 'Put a 5/5 blue and red Elemental creature token with flying onto the battlefield.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('call the skybreaker', ['5', 'U/R', 'U/R']).
card_cmc('call the skybreaker', 7).
card_layout('call the skybreaker', 'normal').

% Found in: ICE, ME3
card_name('call to arms', 'Call to Arms').
card_type('call to arms', 'Enchantment').
card_types('call to arms', ['Enchantment']).
card_subtypes('call to arms', []).
card_colors('call to arms', ['W']).
card_text('call to arms', 'As Call to Arms enters the battlefield, choose a color and an opponent.\nWhite creatures get +1/+1 as long as the chosen color is the most common color among nontoken permanents the chosen player controls but isn\'t tied for most common.\nWhen the chosen color isn\'t the most common color among nontoken permanents the chosen player controls or is tied for most common, sacrifice Call to Arms.').
card_mana_cost('call to arms', ['1', 'W']).
card_cmc('call to arms', 2).
card_layout('call to arms', 'normal').
card_reserved('call to arms').

% Found in: CHK
card_name('call to glory', 'Call to Glory').
card_type('call to glory', 'Instant').
card_types('call to glory', ['Instant']).
card_subtypes('call to glory', []).
card_colors('call to glory', ['W']).
card_text('call to glory', 'Untap all creatures you control. Samurai creatures you control get +1/+1 until end of turn.').
card_mana_cost('call to glory', ['1', 'W']).
card_cmc('call to glory', 2).
card_layout('call to glory', 'normal').

% Found in: ALA, DDJ
card_name('call to heel', 'Call to Heel').
card_type('call to heel', 'Instant').
card_types('call to heel', ['Instant']).
card_subtypes('call to heel', []).
card_colors('call to heel', ['U']).
card_text('call to heel', 'Return target creature to its owner\'s hand. Its controller draws a card.').
card_mana_cost('call to heel', ['1', 'U']).
card_cmc('call to heel', 2).
card_layout('call to heel', 'normal').

% Found in: C14, M11
card_name('call to mind', 'Call to Mind').
card_type('call to mind', 'Sorcery').
card_types('call to mind', ['Sorcery']).
card_subtypes('call to mind', []).
card_colors('call to mind', ['U']).
card_text('call to mind', 'Return target instant or sorcery card from your graveyard to your hand.').
card_mana_cost('call to mind', ['2', 'U']).
card_cmc('call to mind', 3).
card_layout('call to mind', 'normal').

% Found in: AVR
card_name('call to serve', 'Call to Serve').
card_type('call to serve', 'Enchantment — Aura').
card_types('call to serve', ['Enchantment']).
card_subtypes('call to serve', ['Aura']).
card_colors('call to serve', ['W']).
card_text('call to serve', 'Enchant nonblack creature\nEnchanted creature gets +1/+2, has flying, and is an Angel in addition to its other types.').
card_mana_cost('call to serve', ['1', 'W']).
card_cmc('call to serve', 2).
card_layout('call to serve', 'normal').

% Found in: M12, SCG
card_name('call to the grave', 'Call to the Grave').
card_type('call to the grave', 'Enchantment').
card_types('call to the grave', ['Enchantment']).
card_subtypes('call to the grave', []).
card_colors('call to the grave', ['B']).
card_text('call to the grave', 'At the beginning of each player\'s upkeep, that player sacrifices a non-Zombie creature.\nAt the beginning of the end step, if no creatures are on the battlefield, sacrifice Call to the Grave.').
card_mana_cost('call to the grave', ['4', 'B']).
card_cmc('call to the grave', 5).
card_layout('call to the grave', 'normal').

% Found in: DKA
card_name('call to the kindred', 'Call to the Kindred').
card_type('call to the kindred', 'Enchantment — Aura').
card_types('call to the kindred', ['Enchantment']).
card_subtypes('call to the kindred', ['Aura']).
card_colors('call to the kindred', ['U']).
card_text('call to the kindred', 'Enchant creature\nAt the beginning of your upkeep, you may look at the top five cards of your library. If you do, you may put a creature card that shares a creature type with enchanted creature from among them onto the battlefield, then you put the rest of those cards on the bottom of your library in any order.').
card_mana_cost('call to the kindred', ['3', 'U']).
card_cmc('call to the kindred', 4).
card_layout('call to the kindred', 'normal').

% Found in: TSP
card_name('call to the netherworld', 'Call to the Netherworld').
card_type('call to the netherworld', 'Sorcery').
card_types('call to the netherworld', ['Sorcery']).
card_subtypes('call to the netherworld', []).
card_colors('call to the netherworld', ['B']).
card_text('call to the netherworld', 'Return target black creature card from your graveyard to your hand.\nMadness {0} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('call to the netherworld', ['B']).
card_cmc('call to the netherworld', 1).
card_layout('call to the netherworld', 'normal').

% Found in: ZEN
card_name('caller of gales', 'Caller of Gales').
card_type('caller of gales', 'Creature — Merfolk Wizard').
card_types('caller of gales', ['Creature']).
card_subtypes('caller of gales', ['Merfolk', 'Wizard']).
card_colors('caller of gales', ['U']).
card_text('caller of gales', '{1}{U}, {T}: Target creature gains flying until end of turn.').
card_mana_cost('caller of gales', ['U']).
card_cmc('caller of gales', 1).
card_layout('caller of gales', 'normal').
card_power('caller of gales', 1).
card_toughness('caller of gales', 1).

% Found in: LGN
card_name('caller of the claw', 'Caller of the Claw').
card_type('caller of the claw', 'Creature — Elf').
card_types('caller of the claw', ['Creature']).
card_subtypes('caller of the claw', ['Elf']).
card_colors('caller of the claw', ['G']).
card_text('caller of the claw', 'Flash\nWhen Caller of the Claw enters the battlefield, put a 2/2 green Bear creature token onto the battlefield for each nontoken creature put into your graveyard from the battlefield this turn.').
card_mana_cost('caller of the claw', ['2', 'G']).
card_cmc('caller of the claw', 3).
card_layout('caller of the claw', 'normal').
card_power('caller of the claw', 2).
card_toughness('caller of the claw', 2).

% Found in: MMQ
card_name('caller of the hunt', 'Caller of the Hunt').
card_type('caller of the hunt', 'Creature — Human').
card_types('caller of the hunt', ['Creature']).
card_subtypes('caller of the hunt', ['Human']).
card_colors('caller of the hunt', ['G']).
card_text('caller of the hunt', 'As an additional cost to cast Caller of the Hunt, choose a creature type.\nCaller of the Hunt\'s power and toughness are each equal to the number of creatures of the chosen type on the battlefield.').
card_mana_cost('caller of the hunt', ['2', 'G']).
card_cmc('caller of the hunt', 3).
card_layout('caller of the hunt', 'normal').
card_power('caller of the hunt', '*').
card_toughness('caller of the hunt', '*').

% Found in: CHK
card_name('callous deceiver', 'Callous Deceiver').
card_type('callous deceiver', 'Creature — Spirit').
card_types('callous deceiver', ['Creature']).
card_subtypes('callous deceiver', ['Spirit']).
card_colors('callous deceiver', ['U']).
card_text('callous deceiver', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land card, Callous Deceiver gets +1/+0 and gains flying until end of turn. Activate this ability only once each turn.').
card_mana_cost('callous deceiver', ['2', 'U']).
card_cmc('callous deceiver', 3).
card_layout('callous deceiver', 'normal').
card_power('callous deceiver', 1).
card_toughness('callous deceiver', 3).

% Found in: INV
card_name('callous giant', 'Callous Giant').
card_type('callous giant', 'Creature — Giant').
card_types('callous giant', ['Creature']).
card_subtypes('callous giant', ['Giant']).
card_colors('callous giant', ['R']).
card_text('callous giant', 'If a source would deal 3 or less damage to Callous Giant, prevent that damage.').
card_mana_cost('callous giant', ['4', 'R', 'R']).
card_cmc('callous giant', 6).
card_layout('callous giant', 'normal').
card_power('callous giant', 4).
card_toughness('callous giant', 4).

% Found in: ONS
card_name('callous oppressor', 'Callous Oppressor').
card_type('callous oppressor', 'Creature — Cephalid').
card_types('callous oppressor', ['Creature']).
card_subtypes('callous oppressor', ['Cephalid']).
card_colors('callous oppressor', ['U']).
card_text('callous oppressor', 'You may choose not to untap Callous Oppressor during your untap step.\nAs Callous Oppressor enters the battlefield, an opponent chooses a creature type.\n{T}: Gain control of target creature that isn\'t of the chosen type for as long as Callous Oppressor remains tapped.').
card_mana_cost('callous oppressor', ['1', 'U', 'U']).
card_cmc('callous oppressor', 3).
card_layout('callous oppressor', 'normal').
card_power('callous oppressor', 1).
card_toughness('callous oppressor', 2).

% Found in: BOK
card_name('callow jushi', 'Callow Jushi').
card_type('callow jushi', 'Creature — Human Wizard').
card_types('callow jushi', ['Creature']).
card_subtypes('callow jushi', ['Human', 'Wizard']).
card_colors('callow jushi', ['U']).
card_text('callow jushi', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Callow Jushi.\nAt the beginning of the end step, if there are two or more ki counters on Callow Jushi, you may flip it.').
card_mana_cost('callow jushi', ['1', 'U', 'U']).
card_cmc('callow jushi', 3).
card_layout('callow jushi', 'flip').
card_power('callow jushi', 2).
card_toughness('callow jushi', 2).
card_sides('callow jushi', 'jaraku the interloper').

% Found in: STH
card_name('calming licid', 'Calming Licid').
card_type('calming licid', 'Creature — Licid').
card_types('calming licid', ['Creature']).
card_subtypes('calming licid', ['Licid']).
card_colors('calming licid', ['W']).
card_text('calming licid', '{W}, {T}: Calming Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {W} to end this effect.\nEnchanted creature can\'t attack.').
card_mana_cost('calming licid', ['2', 'W']).
card_cmc('calming licid', 3).
card_layout('calming licid', 'normal').
card_power('calming licid', 2).
card_toughness('calming licid', 2).

% Found in: PCY
card_name('calming verse', 'Calming Verse').
card_type('calming verse', 'Sorcery').
card_types('calming verse', ['Sorcery']).
card_subtypes('calming verse', []).
card_colors('calming verse', ['G']).
card_text('calming verse', 'Destroy all enchantments you don\'t control. Then, if you control an untapped land, destroy all enchantments you control.').
card_mana_cost('calming verse', ['3', 'G']).
card_cmc('calming verse', 4).
card_layout('calming verse', 'normal').

% Found in: 7ED, UDS
card_name('caltrops', 'Caltrops').
card_type('caltrops', 'Artifact').
card_types('caltrops', ['Artifact']).
card_subtypes('caltrops', []).
card_colors('caltrops', []).
card_text('caltrops', 'Whenever a creature attacks, Caltrops deals 1 damage to it.').
card_mana_cost('caltrops', ['3']).
card_cmc('caltrops', 3).
card_layout('caltrops', 'normal').

% Found in: ARN
card_name('camel', 'Camel').
card_type('camel', 'Creature — Camel').
card_types('camel', ['Creature']).
card_subtypes('camel', ['Camel']).
card_colors('camel', ['W']).
card_text('camel', 'Banding (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\nAs long as Camel is attacking, prevent all damage Deserts would deal to Camel and to creatures banded with Camel.').
card_mana_cost('camel', ['W']).
card_cmc('camel', 1).
card_layout('camel', 'normal').
card_power('camel', 0).
card_toughness('camel', 1).

% Found in: 2ED, CED, CEI, LEA, LEB
card_name('camouflage', 'Camouflage').
card_type('camouflage', 'Instant').
card_types('camouflage', ['Instant']).
card_subtypes('camouflage', []).
card_colors('camouflage', ['G']).
card_text('camouflage', 'Cast Camouflage only during your declare attackers step.\nThis turn, instead of declaring blockers, each defending player chooses any number of creatures he or she controls and divides them into a number of piles equal to the number of attacking creatures for whom that player is the defending player. Creatures he or she controls that can block additional creatures may likewise be put into additional piles. Assign each pile to a different one of those attacking creatures at random. Each creature in a pile that can block the creature that pile is assigned to does so. (Piles can be empty.)').
card_mana_cost('camouflage', ['G']).
card_cmc('camouflage', 1).
card_layout('camouflage', 'normal').

% Found in: CNS
card_name('canal dredger', 'Canal Dredger').
card_type('canal dredger', 'Artifact Creature — Construct').
card_types('canal dredger', ['Artifact', 'Creature']).
card_subtypes('canal dredger', ['Construct']).
card_colors('canal dredger', []).
card_text('canal dredger', 'Draft Canal Dredger face up.\nEach player passes the last card from each booster pack to a player who drafted a card named Canal Dredger.\n{T}: Put target card from your graveyard on the bottom of your library.').
card_mana_cost('canal dredger', ['4']).
card_cmc('canal dredger', 4).
card_layout('canal dredger', 'normal').
card_power('canal dredger', 1).
card_toughness('canal dredger', 5).

% Found in: 10E, ALA, DPA, KTK, M10, M11, M12, M14, M15, PC2, RTR, TSP, ZEN, pMPR
card_name('cancel', 'Cancel').
card_type('cancel', 'Instant').
card_types('cancel', ['Instant']).
card_subtypes('cancel', []).
card_colors('cancel', ['U']).
card_text('cancel', 'Counter target spell.').
card_mana_cost('cancel', ['1', 'U', 'U']).
card_cmc('cancel', 3).
card_layout('cancel', 'normal').

% Found in: ATQ, ME4
card_name('candelabra of tawnos', 'Candelabra of Tawnos').
card_type('candelabra of tawnos', 'Artifact').
card_types('candelabra of tawnos', ['Artifact']).
card_subtypes('candelabra of tawnos', []).
card_colors('candelabra of tawnos', []).
card_text('candelabra of tawnos', '{X}, {T}: Untap X target lands.').
card_mana_cost('candelabra of tawnos', ['1']).
card_cmc('candelabra of tawnos', 1).
card_layout('candelabra of tawnos', 'normal').
card_reserved('candelabra of tawnos').

% Found in: TSP
card_name('candles of leng', 'Candles of Leng').
card_type('candles of leng', 'Artifact').
card_types('candles of leng', ['Artifact']).
card_subtypes('candles of leng', []).
card_colors('candles of leng', []).
card_text('candles of leng', '{4}, {T}: Reveal the top card of your library. If it has the same name as a card in your graveyard, put it into your graveyard. Otherwise, draw a card.').
card_mana_cost('candles of leng', ['2']).
card_cmc('candles of leng', 2).
card_layout('candles of leng', 'normal').

% Found in: CHK
card_name('candles\' glow', 'Candles\' Glow').
card_type('candles\' glow', 'Instant — Arcane').
card_types('candles\' glow', ['Instant']).
card_subtypes('candles\' glow', ['Arcane']).
card_colors('candles\' glow', ['W']).
card_text('candles\' glow', 'Prevent the next 3 damage that would be dealt to target creature or player this turn. You gain life equal to the damage prevented this way.\nSplice onto Arcane {1}{W} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('candles\' glow', ['1', 'W']).
card_cmc('candles\' glow', 2).
card_layout('candles\' glow', 'normal').

% Found in: EVE
card_name('canker abomination', 'Canker Abomination').
card_type('canker abomination', 'Creature — Treefolk Horror').
card_types('canker abomination', ['Creature']).
card_subtypes('canker abomination', ['Treefolk', 'Horror']).
card_colors('canker abomination', ['B', 'G']).
card_text('canker abomination', 'As Canker Abomination enters the battlefield, choose an opponent. Canker Abomination enters the battlefield with a -1/-1 counter on it for each creature that player controls.').
card_mana_cost('canker abomination', ['2', 'B/G', 'B/G']).
card_cmc('canker abomination', 4).
card_layout('canker abomination', 'normal').
card_power('canker abomination', 6).
card_toughness('canker abomination', 6).

% Found in: EVE
card_name('cankerous thirst', 'Cankerous Thirst').
card_type('cankerous thirst', 'Instant').
card_types('cankerous thirst', ['Instant']).
card_subtypes('cankerous thirst', []).
card_colors('cankerous thirst', ['B', 'G']).
card_text('cankerous thirst', 'If {B} was spent to cast Cankerous Thirst, you may have target creature get -3/-3 until end of turn. If {G} was spent to cast Cankerous Thirst, you may have target creature get +3/+3 until end of turn. (Do both if {B}{G} was spent.)').
card_mana_cost('cankerous thirst', ['3', 'B/G']).
card_cmc('cankerous thirst', 4).
card_layout('cankerous thirst', 'normal').

% Found in: STH, TPR
card_name('cannibalize', 'Cannibalize').
card_type('cannibalize', 'Sorcery').
card_types('cannibalize', ['Sorcery']).
card_subtypes('cannibalize', []).
card_colors('cannibalize', ['B']).
card_text('cannibalize', 'Choose two target creatures controlled by the same player. Exile one of those creatures and put two +1/+1 counters on the other.').
card_mana_cost('cannibalize', ['1', 'B']).
card_cmc('cannibalize', 2).
card_layout('cannibalize', 'normal').

% Found in: JUD
card_name('canopy claws', 'Canopy Claws').
card_type('canopy claws', 'Instant').
card_types('canopy claws', ['Instant']).
card_subtypes('canopy claws', []).
card_colors('canopy claws', ['G']).
card_text('canopy claws', 'Target creature loses flying until end of turn.\nFlashback {G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('canopy claws', ['G']).
card_cmc('canopy claws', 1).
card_layout('canopy claws', 'normal').

% Found in: WWK
card_name('canopy cover', 'Canopy Cover').
card_type('canopy cover', 'Enchantment — Aura').
card_types('canopy cover', ['Enchantment']).
card_subtypes('canopy cover', ['Aura']).
card_colors('canopy cover', ['G']).
card_text('canopy cover', 'Enchant creature\nEnchanted creature can\'t be blocked except by creatures with flying or reach.\nEnchanted creature can\'t be the target of spells or abilities your opponents control.').
card_mana_cost('canopy cover', ['1', 'G']).
card_cmc('canopy cover', 2).
card_layout('canopy cover', 'normal').

% Found in: LGN
card_name('canopy crawler', 'Canopy Crawler').
card_type('canopy crawler', 'Creature — Beast').
card_types('canopy crawler', ['Creature']).
card_subtypes('canopy crawler', ['Beast']).
card_colors('canopy crawler', ['G']).
card_text('canopy crawler', 'Amplify 1 (As this creature enters the battlefield, put a +1/+1 counter on it for each Beast card you reveal in your hand.)\n{T}: Target creature gets +1/+1 until end of turn for each +1/+1 counter on Canopy Crawler.').
card_mana_cost('canopy crawler', ['3', 'G']).
card_cmc('canopy crawler', 4).
card_layout('canopy crawler', 'normal').
card_power('canopy crawler', 2).
card_toughness('canopy crawler', 2).

% Found in: MIR
card_name('canopy dragon', 'Canopy Dragon').
card_type('canopy dragon', 'Creature — Dragon').
card_types('canopy dragon', ['Creature']).
card_subtypes('canopy dragon', ['Dragon']).
card_colors('canopy dragon', ['G']).
card_text('canopy dragon', 'Trample\n{1}{G}: Canopy Dragon gains flying and loses trample until end of turn.').
card_mana_cost('canopy dragon', ['4', 'G', 'G']).
card_cmc('canopy dragon', 6).
card_layout('canopy dragon', 'normal').
card_power('canopy dragon', 4).
card_toughness('canopy dragon', 4).
card_reserved('canopy dragon').

% Found in: 10E, 7ED, 8ED, ATH, TMP, TPR
card_name('canopy spider', 'Canopy Spider').
card_type('canopy spider', 'Creature — Spider').
card_types('canopy spider', ['Creature']).
card_subtypes('canopy spider', ['Spider']).
card_colors('canopy spider', ['G']).
card_text('canopy spider', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('canopy spider', ['1', 'G']).
card_cmc('canopy spider', 2).
card_layout('canopy spider', 'normal').
card_power('canopy spider', 1).
card_toughness('canopy spider', 3).

% Found in: INV
card_name('canopy surge', 'Canopy Surge').
card_type('canopy surge', 'Sorcery').
card_types('canopy surge', ['Sorcery']).
card_subtypes('canopy surge', []).
card_colors('canopy surge', ['G']).
card_text('canopy surge', 'Kicker {2} (You may pay an additional {2} as you cast this spell.)\nCanopy Surge deals 1 damage to each creature with flying and each player. If Canopy Surge was kicked, it deals 4 damage to each creature with flying and each player instead.').
card_mana_cost('canopy surge', ['1', 'G']).
card_cmc('canopy surge', 2).
card_layout('canopy surge', 'normal').

% Found in: BFZ, EXP
card_name('canopy vista', 'Canopy Vista').
card_type('canopy vista', 'Land — Forest Plains').
card_types('canopy vista', ['Land']).
card_subtypes('canopy vista', ['Forest', 'Plains']).
card_colors('canopy vista', []).
card_text('canopy vista', '({T}: Add {G} or {W} to your mana pool.)\nCanopy Vista enters the battlefield tapped unless you control two or more basic lands.').
card_layout('canopy vista', 'normal').

% Found in: ODY
card_name('cantivore', 'Cantivore').
card_type('cantivore', 'Creature — Lhurgoyf').
card_types('cantivore', ['Creature']).
card_subtypes('cantivore', ['Lhurgoyf']).
card_colors('cantivore', ['W']).
card_text('cantivore', 'Vigilance\nCantivore\'s power and toughness are each equal to the number of enchantment cards in all graveyards.').
card_mana_cost('cantivore', ['1', 'W', 'W']).
card_cmc('cantivore', 3).
card_layout('cantivore', 'normal').
card_power('cantivore', '*').
card_toughness('cantivore', '*').

% Found in: TMP
card_name('canyon drake', 'Canyon Drake').
card_type('canyon drake', 'Creature — Drake').
card_types('canyon drake', ['Creature']).
card_subtypes('canyon drake', ['Drake']).
card_colors('canyon drake', ['R']).
card_text('canyon drake', 'Flying\n{1}, Discard a card at random: Canyon Drake gets +2/+0 until end of turn.').
card_mana_cost('canyon drake', ['2', 'R', 'R']).
card_cmc('canyon drake', 4).
card_layout('canyon drake', 'normal').
card_power('canyon drake', 1).
card_toughness('canyon drake', 2).

% Found in: KTK
card_name('canyon lurkers', 'Canyon Lurkers').
card_type('canyon lurkers', 'Creature — Human Rogue').
card_types('canyon lurkers', ['Creature']).
card_subtypes('canyon lurkers', ['Human', 'Rogue']).
card_colors('canyon lurkers', ['R']).
card_text('canyon lurkers', 'Morph {3}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('canyon lurkers', ['4', 'R']).
card_cmc('canyon lurkers', 5).
card_layout('canyon lurkers', 'normal').
card_power('canyon lurkers', 5).
card_toughness('canyon lurkers', 2).

% Found in: CON, M10, M11, M13, M14
card_name('canyon minotaur', 'Canyon Minotaur').
card_type('canyon minotaur', 'Creature — Minotaur Warrior').
card_types('canyon minotaur', ['Creature']).
card_subtypes('canyon minotaur', ['Minotaur', 'Warrior']).
card_colors('canyon minotaur', ['R']).
card_text('canyon minotaur', '').
card_mana_cost('canyon minotaur', ['3', 'R']).
card_cmc('canyon minotaur', 4).
card_layout('canyon minotaur', 'normal').
card_power('canyon minotaur', 3).
card_toughness('canyon minotaur', 3).

% Found in: 8ED, DDH, TMP, TPR
card_name('canyon wildcat', 'Canyon Wildcat').
card_type('canyon wildcat', 'Creature — Cat').
card_types('canyon wildcat', ['Creature']).
card_subtypes('canyon wildcat', ['Cat']).
card_colors('canyon wildcat', ['R']).
card_text('canyon wildcat', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('canyon wildcat', ['1', 'R']).
card_cmc('canyon wildcat', 2).
card_layout('canyon wildcat', 'normal').
card_power('canyon wildcat', 2).
card_toughness('canyon wildcat', 1).

% Found in: PTK, V11
card_name('cao cao, lord of wei', 'Cao Cao, Lord of Wei').
card_type('cao cao, lord of wei', 'Legendary Creature — Human Soldier').
card_types('cao cao, lord of wei', ['Creature']).
card_subtypes('cao cao, lord of wei', ['Human', 'Soldier']).
card_supertypes('cao cao, lord of wei', ['Legendary']).
card_colors('cao cao, lord of wei', ['B']).
card_text('cao cao, lord of wei', '{T}: Target opponent discards two cards. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('cao cao, lord of wei', ['3', 'B', 'B']).
card_cmc('cao cao, lord of wei', 5).
card_layout('cao cao, lord of wei', 'normal').
card_power('cao cao, lord of wei', 3).
card_toughness('cao cao, lord of wei', 3).

% Found in: PTK
card_name('cao ren, wei commander', 'Cao Ren, Wei Commander').
card_type('cao ren, wei commander', 'Legendary Creature — Human Soldier Warrior').
card_types('cao ren, wei commander', ['Creature']).
card_subtypes('cao ren, wei commander', ['Human', 'Soldier', 'Warrior']).
card_supertypes('cao ren, wei commander', ['Legendary']).
card_colors('cao ren, wei commander', ['B']).
card_text('cao ren, wei commander', 'Horsemanship (This creature can\'t be blocked except by creatures with horsemanship.)\nWhen Cao Ren, Wei Commander enters the battlefield, you lose 3 life.').
card_mana_cost('cao ren, wei commander', ['2', 'B', 'B']).
card_cmc('cao ren, wei commander', 4).
card_layout('cao ren, wei commander', 'normal').
card_power('cao ren, wei commander', 3).
card_toughness('cao ren, wei commander', 3).

% Found in: M14, UDS
card_name('capashen knight', 'Capashen Knight').
card_type('capashen knight', 'Creature — Human Knight').
card_types('capashen knight', ['Creature']).
card_subtypes('capashen knight', ['Human', 'Knight']).
card_colors('capashen knight', ['W']).
card_text('capashen knight', 'First strike (This creature deals combat damage before creatures without first strike.)\n{1}{W}: Capashen Knight gets +1/+0 until end of turn.').
card_mana_cost('capashen knight', ['1', 'W']).
card_cmc('capashen knight', 2).
card_layout('capashen knight', 'normal').
card_power('capashen knight', 1).
card_toughness('capashen knight', 1).

% Found in: UDS
card_name('capashen standard', 'Capashen Standard').
card_type('capashen standard', 'Enchantment — Aura').
card_types('capashen standard', ['Enchantment']).
card_subtypes('capashen standard', ['Aura']).
card_colors('capashen standard', ['W']).
card_text('capashen standard', 'Enchant creature\nEnchanted creature gets +1/+1.\n{2}, Sacrifice Capashen Standard: Draw a card.').
card_mana_cost('capashen standard', ['W']).
card_cmc('capashen standard', 1).
card_layout('capashen standard', 'normal').

% Found in: UDS
card_name('capashen templar', 'Capashen Templar').
card_type('capashen templar', 'Creature — Human Knight').
card_types('capashen templar', ['Creature']).
card_subtypes('capashen templar', ['Human', 'Knight']).
card_colors('capashen templar', ['W']).
card_text('capashen templar', '{W}: Capashen Templar gets +0/+1 until end of turn.').
card_mana_cost('capashen templar', ['2', 'W']).
card_cmc('capashen templar', 3).
card_layout('capashen templar', 'normal').
card_power('capashen templar', 2).
card_toughness('capashen templar', 2).

% Found in: INV
card_name('capashen unicorn', 'Capashen Unicorn').
card_type('capashen unicorn', 'Creature — Unicorn').
card_types('capashen unicorn', ['Creature']).
card_subtypes('capashen unicorn', ['Unicorn']).
card_colors('capashen unicorn', ['W']).
card_text('capashen unicorn', '{1}{W}, {T}, Sacrifice Capashen Unicorn: Destroy target artifact or enchantment.').
card_mana_cost('capashen unicorn', ['1', 'W']).
card_cmc('capashen unicorn', 2).
card_layout('capashen unicorn', 'normal').
card_power('capashen unicorn', 1).
card_toughness('capashen unicorn', 2).

% Found in: C13, M10
card_name('capricious efreet', 'Capricious Efreet').
card_type('capricious efreet', 'Creature — Efreet').
card_types('capricious efreet', ['Creature']).
card_subtypes('capricious efreet', ['Efreet']).
card_colors('capricious efreet', ['R']).
card_text('capricious efreet', 'At the beginning of your upkeep, choose target nonland permanent you control and up to two target nonland permanents you don\'t control. Destroy one of them at random.').
card_mana_cost('capricious efreet', ['4', 'R', 'R']).
card_cmc('capricious efreet', 6).
card_layout('capricious efreet', 'normal').
card_power('capricious efreet', 6).
card_toughness('capricious efreet', 4).

% Found in: POR
card_name('capricious sorcerer', 'Capricious Sorcerer').
card_type('capricious sorcerer', 'Creature — Human Wizard').
card_types('capricious sorcerer', ['Creature']).
card_subtypes('capricious sorcerer', ['Human', 'Wizard']).
card_colors('capricious sorcerer', ['U']).
card_text('capricious sorcerer', '{T}: Capricious Sorcerer deals 1 damage to target creature or player. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('capricious sorcerer', ['2', 'U']).
card_cmc('capricious sorcerer', 3).
card_layout('capricious sorcerer', 'normal').
card_power('capricious sorcerer', 1).
card_toughness('capricious sorcerer', 1).

% Found in: TMP, TPR, pFNM
card_name('capsize', 'Capsize').
card_type('capsize', 'Instant').
card_types('capsize', ['Instant']).
card_subtypes('capsize', []).
card_colors('capsize', ['U']).
card_text('capsize', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nReturn target permanent to its owner\'s hand.').
card_mana_cost('capsize', ['1', 'U', 'U']).
card_cmc('capsize', 3).
card_layout('capsize', 'normal').

% Found in: AVR
card_name('captain of the mists', 'Captain of the Mists').
card_type('captain of the mists', 'Creature — Human Wizard').
card_types('captain of the mists', ['Creature']).
card_subtypes('captain of the mists', ['Human', 'Wizard']).
card_colors('captain of the mists', ['U']).
card_text('captain of the mists', 'Whenever another Human enters the battlefield under your control, untap Captain of the Mists.\n{1}{U}, {T}: You may tap or untap target permanent.').
card_mana_cost('captain of the mists', ['2', 'U']).
card_cmc('captain of the mists', 3).
card_layout('captain of the mists', 'normal').
card_power('captain of the mists', 2).
card_toughness('captain of the mists', 3).

% Found in: DDO, M10, M13
card_name('captain of the watch', 'Captain of the Watch').
card_type('captain of the watch', 'Creature — Human Soldier').
card_types('captain of the watch', ['Creature']).
card_subtypes('captain of the watch', ['Human', 'Soldier']).
card_colors('captain of the watch', ['W']).
card_text('captain of the watch', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nOther Soldier creatures you control get +1/+1 and have vigilance.\nWhen Captain of the Watch enters the battlefield, put three 1/1 white Soldier creature tokens onto the battlefield.').
card_mana_cost('captain of the watch', ['4', 'W', 'W']).
card_cmc('captain of the watch', 6).
card_layout('captain of the watch', 'normal').
card_power('captain of the watch', 3).
card_toughness('captain of the watch', 3).

% Found in: INV, V11
card_name('captain sisay', 'Captain Sisay').
card_type('captain sisay', 'Legendary Creature — Human Soldier').
card_types('captain sisay', ['Creature']).
card_subtypes('captain sisay', ['Human', 'Soldier']).
card_supertypes('captain sisay', ['Legendary']).
card_colors('captain sisay', ['W', 'G']).
card_text('captain sisay', '{T}: Search your library for a legendary card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('captain sisay', ['2', 'G', 'W']).
card_cmc('captain sisay', 4).
card_layout('captain sisay', 'normal').
card_power('captain sisay', 2).
card_toughness('captain sisay', 2).

% Found in: M13
card_name('captain\'s call', 'Captain\'s Call').
card_type('captain\'s call', 'Sorcery').
card_types('captain\'s call', ['Sorcery']).
card_subtypes('captain\'s call', []).
card_colors('captain\'s call', ['W']).
card_text('captain\'s call', 'Put three 1/1 white Soldier creature tokens onto the battlefield.').
card_mana_cost('captain\'s call', ['3', 'W']).
card_cmc('captain\'s call', 4).
card_layout('captain\'s call', 'normal').

% Found in: APC, HOP
card_name('captain\'s maneuver', 'Captain\'s Maneuver').
card_type('captain\'s maneuver', 'Instant').
card_types('captain\'s maneuver', ['Instant']).
card_subtypes('captain\'s maneuver', []).
card_colors('captain\'s maneuver', ['W', 'R']).
card_text('captain\'s maneuver', 'The next X damage that would be dealt to target creature or player this turn is dealt to another target creature or player instead.').
card_mana_cost('captain\'s maneuver', ['X', 'R', 'W']).
card_cmc('captain\'s maneuver', 2).
card_layout('captain\'s maneuver', 'normal').

% Found in: LRW
card_name('captivating glance', 'Captivating Glance').
card_type('captivating glance', 'Enchantment — Aura').
card_types('captivating glance', ['Enchantment']).
card_subtypes('captivating glance', ['Aura']).
card_colors('captivating glance', ['U']).
card_text('captivating glance', 'Enchant creature\nAt the beginning of your end step, clash with an opponent. If you win, gain control of enchanted creature. Otherwise, that player gains control of enchanted creature. (Each clashing player reveals the top card of his or her library, then puts that card on the top or bottom. A player wins if his or her card had a higher converted mana cost.)').
card_mana_cost('captivating glance', ['2', 'U']).
card_cmc('captivating glance', 3).
card_layout('captivating glance', 'normal').

% Found in: M11
card_name('captivating vampire', 'Captivating Vampire').
card_type('captivating vampire', 'Creature — Vampire').
card_types('captivating vampire', ['Creature']).
card_subtypes('captivating vampire', ['Vampire']).
card_colors('captivating vampire', ['B']).
card_text('captivating vampire', 'Other Vampire creatures you control get +1/+1.\nTap five untapped Vampires you control: Gain control of target creature. It becomes a Vampire in addition to its other types.').
card_mana_cost('captivating vampire', ['1', 'B', 'B']).
card_cmc('captivating vampire', 3).
card_layout('captivating vampire', 'normal').
card_power('captivating vampire', 2).
card_toughness('captivating vampire', 2).

% Found in: DDG, SOK
card_name('captive flame', 'Captive Flame').
card_type('captive flame', 'Enchantment').
card_types('captive flame', ['Enchantment']).
card_subtypes('captive flame', []).
card_colors('captive flame', ['R']).
card_text('captive flame', '{R}: Target creature gets +1/+0 until end of turn.').
card_mana_cost('captive flame', ['2', 'R']).
card_cmc('captive flame', 3).
card_layout('captive flame', 'normal').

% Found in: ME3, PTK
card_name('capture of jingzhou', 'Capture of Jingzhou').
card_type('capture of jingzhou', 'Sorcery').
card_types('capture of jingzhou', ['Sorcery']).
card_subtypes('capture of jingzhou', []).
card_colors('capture of jingzhou', ['U']).
card_text('capture of jingzhou', 'Take an extra turn after this one.').
card_mana_cost('capture of jingzhou', ['3', 'U', 'U']).
card_cmc('capture of jingzhou', 5).
card_layout('capture of jingzhou', 'normal').

% Found in: ARB
card_name('captured sunlight', 'Captured Sunlight').
card_type('captured sunlight', 'Sorcery').
card_types('captured sunlight', ['Sorcery']).
card_subtypes('captured sunlight', []).
card_colors('captured sunlight', ['W', 'G']).
card_text('captured sunlight', 'Cascade (When you cast this spell, exile cards from the top of your library until you exile a nonland card that costs less. You may cast it without paying its mana cost. Put the exiled cards on the bottom in a random order.)\nYou gain 4 life.').
card_mana_cost('captured sunlight', ['2', 'G', 'W']).
card_cmc('captured sunlight', 4).
card_layout('captured sunlight', 'normal').

% Found in: 5ED, HML, ME2
card_name('carapace', 'Carapace').
card_type('carapace', 'Enchantment — Aura').
card_types('carapace', ['Enchantment']).
card_subtypes('carapace', ['Aura']).
card_colors('carapace', ['G']).
card_text('carapace', 'Enchant creature\nEnchanted creature gets +0/+2.\nSacrifice Carapace: Regenerate enchanted creature.').
card_mana_cost('carapace', ['G']).
card_cmc('carapace', 1).
card_layout('carapace', 'normal').

% Found in: SOM
card_name('carapace forger', 'Carapace Forger').
card_type('carapace forger', 'Creature — Elf Artificer').
card_types('carapace forger', ['Creature']).
card_subtypes('carapace forger', ['Elf', 'Artificer']).
card_colors('carapace forger', ['G']).
card_text('carapace forger', 'Metalcraft — Carapace Forger gets +2/+2 as long as you control three or more artifacts.').
card_mana_cost('carapace forger', ['1', 'G']).
card_cmc('carapace forger', 2).
card_layout('carapace forger', 'normal').
card_power('carapace forger', 2).
card_toughness('carapace forger', 2).

% Found in: DDG, DDP, ROE
card_name('caravan escort', 'Caravan Escort').
card_type('caravan escort', 'Creature — Human Knight').
card_types('caravan escort', ['Creature']).
card_subtypes('caravan escort', ['Human', 'Knight']).
card_colors('caravan escort', ['W']).
card_text('caravan escort', 'Level up {2} ({2}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 1-4\n2/2\nLEVEL 5+\n5/5\nFirst strike').
card_mana_cost('caravan escort', ['W']).
card_cmc('caravan escort', 1).
card_layout('caravan escort', 'leveler').
card_power('caravan escort', 1).
card_toughness('caravan escort', 1).

% Found in: ZEN
card_name('caravan hurda', 'Caravan Hurda').
card_type('caravan hurda', 'Creature — Giant').
card_types('caravan hurda', ['Creature']).
card_subtypes('caravan hurda', ['Giant']).
card_colors('caravan hurda', ['W']).
card_text('caravan hurda', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('caravan hurda', ['4', 'W']).
card_cmc('caravan hurda', 5).
card_layout('caravan hurda', 'normal').
card_power('caravan hurda', 1).
card_toughness('caravan hurda', 5).

% Found in: ISD
card_name('caravan vigil', 'Caravan Vigil').
card_type('caravan vigil', 'Sorcery').
card_types('caravan vigil', ['Sorcery']).
card_subtypes('caravan vigil', []).
card_colors('caravan vigil', ['G']).
card_text('caravan vigil', 'Search your library for a basic land card, reveal it, put it into your hand, then shuffle your library.\nMorbid — You may put that card onto the battlefield instead of putting it into your hand if a creature died this turn.').
card_mana_cost('caravan vigil', ['G']).
card_cmc('caravan vigil', 1).
card_layout('caravan vigil', 'normal').

% Found in: SCG
card_name('carbonize', 'Carbonize').
card_type('carbonize', 'Instant').
card_types('carbonize', ['Instant']).
card_subtypes('carbonize', []).
card_colors('carbonize', ['R']).
card_text('carbonize', 'Carbonize deals 3 damage to target creature or player. That creature can\'t be regenerated this turn. If the creature would die this turn, exile it instead.').
card_mana_cost('carbonize', ['2', 'R']).
card_cmc('carbonize', 3).
card_layout('carbonize', 'normal').

% Found in: UGL
card_name('cardboard carapace', 'Cardboard Carapace').
card_type('cardboard carapace', 'Enchant Creature').
card_types('cardboard carapace', ['Enchant', 'Creature']).
card_subtypes('cardboard carapace', []).
card_colors('cardboard carapace', ['G']).
card_text('cardboard carapace', 'For each other Cardboard Carapace card you have with you, enchanted creature gets +1/+1.\nErrata: This does not count any Cardboard Carapace cards in play that you control or in your graveyard, hand, or library.').
card_mana_cost('cardboard carapace', ['5', 'G']).
card_cmc('cardboard carapace', 6).
card_layout('cardboard carapace', 'normal').

% Found in: UNH
card_name('cardpecker', 'Cardpecker').
card_type('cardpecker', 'Creature — Bird').
card_types('cardpecker', ['Creature']).
card_subtypes('cardpecker', ['Bird']).
card_colors('cardpecker', ['W']).
card_text('cardpecker', 'Flying\nGotcha Whenever an opponent touches the table with his or her hand, you may say \"Gotcha\" If you do, return Cardpecker from your graveyard to your hand.').
card_mana_cost('cardpecker', ['1', 'W']).
card_cmc('cardpecker', 2).
card_layout('cardpecker', 'normal').
card_power('cardpecker', 1.5).
card_toughness('cardpecker', 1).

% Found in: MMA, TSP
card_name('careful consideration', 'Careful Consideration').
card_type('careful consideration', 'Instant').
card_types('careful consideration', ['Instant']).
card_subtypes('careful consideration', []).
card_colors('careful consideration', ['U']).
card_text('careful consideration', 'Target player draws four cards, then discards three cards. If you cast this spell during your main phase, instead that player draws four cards, then discards two cards.').
card_mana_cost('careful consideration', ['2', 'U', 'U']).
card_cmc('careful consideration', 4).
card_layout('careful consideration', 'normal').

% Found in: ODY
card_name('careful study', 'Careful Study').
card_type('careful study', 'Sorcery').
card_types('careful study', ['Sorcery']).
card_subtypes('careful study', []).
card_colors('careful study', ['U']).
card_text('careful study', 'Draw two cards, then discard two cards.').
card_mana_cost('careful study', ['U']).
card_cmc('careful study', 1).
card_layout('careful study', 'normal').

% Found in: RAV
card_name('caregiver', 'Caregiver').
card_type('caregiver', 'Creature — Human Cleric').
card_types('caregiver', ['Creature']).
card_subtypes('caregiver', ['Human', 'Cleric']).
card_colors('caregiver', ['W']).
card_text('caregiver', '{W}, Sacrifice a creature: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('caregiver', ['W']).
card_cmc('caregiver', 1).
card_layout('caregiver', 'normal').
card_power('caregiver', 1).
card_toughness('caregiver', 1).

% Found in: NPH
card_name('caress of phyrexia', 'Caress of Phyrexia').
card_type('caress of phyrexia', 'Sorcery').
card_types('caress of phyrexia', ['Sorcery']).
card_subtypes('caress of phyrexia', []).
card_colors('caress of phyrexia', ['B']).
card_text('caress of phyrexia', 'Target player draws three cards, loses 3 life, and gets three poison counters.').
card_mana_cost('caress of phyrexia', ['3', 'B', 'B']).
card_cmc('caress of phyrexia', 5).
card_layout('caress of phyrexia', 'normal').

% Found in: 5ED, ICE, ME2
card_name('caribou range', 'Caribou Range').
card_type('caribou range', 'Enchantment — Aura').
card_types('caribou range', ['Enchantment']).
card_subtypes('caribou range', ['Aura']).
card_colors('caribou range', ['W']).
card_text('caribou range', 'Enchant land you control\nEnchanted land has \"{W}{W}, {T}: Put a 0/1 white Caribou creature token onto the battlefield.\"\nSacrifice a Caribou token: You gain 1 life.').
card_mana_cost('caribou range', ['2', 'W', 'W']).
card_cmc('caribou range', 4).
card_layout('caribou range', 'normal').

% Found in: C13, ZEN
card_name('carnage altar', 'Carnage Altar').
card_type('carnage altar', 'Artifact').
card_types('carnage altar', ['Artifact']).
card_subtypes('carnage altar', []).
card_colors('carnage altar', []).
card_text('carnage altar', '{3}, Sacrifice a creature: Draw a card.').
card_mana_cost('carnage altar', ['2']).
card_cmc('carnage altar', 2).
card_layout('carnage altar', 'normal').

% Found in: DGM
card_name('carnage gladiator', 'Carnage Gladiator').
card_type('carnage gladiator', 'Creature — Skeleton Warrior').
card_types('carnage gladiator', ['Creature']).
card_subtypes('carnage gladiator', ['Skeleton', 'Warrior']).
card_colors('carnage gladiator', ['B', 'R']).
card_text('carnage gladiator', 'Whenever a creature blocks, that creature\'s controller loses 1 life.\n{1}{B}{R}: Regenerate Carnage Gladiator.').
card_mana_cost('carnage gladiator', ['2', 'B', 'R']).
card_cmc('carnage gladiator', 4).
card_layout('carnage gladiator', 'normal').
card_power('carnage gladiator', 4).
card_toughness('carnage gladiator', 2).

% Found in: M12
card_name('carnage wurm', 'Carnage Wurm').
card_type('carnage wurm', 'Creature — Wurm').
card_types('carnage wurm', ['Creature']).
card_subtypes('carnage wurm', ['Wurm']).
card_colors('carnage wurm', ['G']).
card_text('carnage wurm', 'Bloodthirst 3 (If an opponent was dealt damage this turn, this creature enters the battlefield with three +1/+1 counters on it.)\nTrample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)').
card_mana_cost('carnage wurm', ['6', 'G']).
card_cmc('carnage wurm', 7).
card_layout('carnage wurm', 'normal').
card_power('carnage wurm', 6).
card_toughness('carnage wurm', 6).

% Found in: STH, TPR
card_name('carnassid', 'Carnassid').
card_type('carnassid', 'Creature — Beast').
card_types('carnassid', ['Creature']).
card_subtypes('carnassid', ['Beast']).
card_colors('carnassid', ['G']).
card_text('carnassid', 'Trample\n{1}{G}: Regenerate Carnassid.').
card_mana_cost('carnassid', ['4', 'G', 'G']).
card_cmc('carnassid', 6).
card_layout('carnassid', 'normal').
card_power('carnassid', 5).
card_toughness('carnassid', 4).

% Found in: SOM
card_name('carnifex demon', 'Carnifex Demon').
card_type('carnifex demon', 'Creature — Demon').
card_types('carnifex demon', ['Creature']).
card_subtypes('carnifex demon', ['Demon']).
card_colors('carnifex demon', ['B']).
card_text('carnifex demon', 'Flying\nCarnifex Demon enters the battlefield with two -1/-1 counters on it.\n{B}, Remove a -1/-1 counter from Carnifex Demon: Put a -1/-1 counter on each other creature.').
card_mana_cost('carnifex demon', ['4', 'B', 'B']).
card_cmc('carnifex demon', 6).
card_layout('carnifex demon', 'normal').
card_power('carnifex demon', 6).
card_toughness('carnifex demon', 6).

% Found in: RTR, pPRE
card_name('carnival hellsteed', 'Carnival Hellsteed').
card_type('carnival hellsteed', 'Creature — Nightmare Horse').
card_types('carnival hellsteed', ['Creature']).
card_subtypes('carnival hellsteed', ['Nightmare', 'Horse']).
card_colors('carnival hellsteed', ['B', 'R']).
card_text('carnival hellsteed', 'First strike, haste\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)').
card_mana_cost('carnival hellsteed', ['4', 'B', 'R']).
card_cmc('carnival hellsteed', 6).
card_layout('carnival hellsteed', 'normal').
card_power('carnival hellsteed', 5).
card_toughness('carnival hellsteed', 4).

% Found in: UDS
card_name('carnival of souls', 'Carnival of Souls').
card_type('carnival of souls', 'Enchantment').
card_types('carnival of souls', ['Enchantment']).
card_subtypes('carnival of souls', []).
card_colors('carnival of souls', ['B']).
card_text('carnival of souls', 'Whenever a creature enters the battlefield, you lose 1 life and add {B} to your mana pool.').
card_mana_cost('carnival of souls', ['1', 'B']).
card_cmc('carnival of souls', 2).
card_layout('carnival of souls', 'normal').
card_reserved('carnival of souls').

% Found in: UNH
card_name('carnivorous death-parrot', 'Carnivorous Death-Parrot').
card_type('carnivorous death-parrot', 'Creature — Bird').
card_types('carnivorous death-parrot', ['Creature']).
card_subtypes('carnivorous death-parrot', ['Bird']).
card_colors('carnivorous death-parrot', ['U']).
card_text('carnivorous death-parrot', 'Flying\nAt the beginning of your upkeep, sacrifice Carnivorous Death-Parrot unless you say its flavor text.').
card_mana_cost('carnivorous death-parrot', ['1', 'U']).
card_cmc('carnivorous death-parrot', 2).
card_layout('carnivorous death-parrot', 'normal').
card_power('carnivorous death-parrot', 2).
card_toughness('carnivorous death-parrot', 2).

% Found in: M15
card_name('carnivorous moss-beast', 'Carnivorous Moss-Beast').
card_type('carnivorous moss-beast', 'Creature — Plant Elemental Beast').
card_types('carnivorous moss-beast', ['Creature']).
card_subtypes('carnivorous moss-beast', ['Plant', 'Elemental', 'Beast']).
card_colors('carnivorous moss-beast', ['G']).
card_text('carnivorous moss-beast', '{5}{G}{G}: Put a +1/+1 counter on Carnivorous Moss-Beast.').
card_mana_cost('carnivorous moss-beast', ['4', 'G', 'G']).
card_cmc('carnivorous moss-beast', 6).
card_layout('carnivorous moss-beast', 'normal').
card_power('carnivorous moss-beast', 4).
card_toughness('carnivorous moss-beast', 5).

% Found in: 4ED, ATH, DRK, MED
card_name('carnivorous plant', 'Carnivorous Plant').
card_type('carnivorous plant', 'Creature — Plant Wall').
card_types('carnivorous plant', ['Creature']).
card_subtypes('carnivorous plant', ['Plant', 'Wall']).
card_colors('carnivorous plant', ['G']).
card_text('carnivorous plant', 'Defender').
card_mana_cost('carnivorous plant', ['3', 'G']).
card_cmc('carnivorous plant', 4).
card_layout('carnivorous plant', 'normal').
card_power('carnivorous plant', 4).
card_toughness('carnivorous plant', 5).

% Found in: EXO, TPR, VMA, pFNM
card_name('carnophage', 'Carnophage').
card_type('carnophage', 'Creature — Zombie').
card_types('carnophage', ['Creature']).
card_subtypes('carnophage', ['Zombie']).
card_colors('carnophage', ['B']).
card_text('carnophage', 'At the beginning of your upkeep, tap Carnophage unless you pay 1 life.').
card_mana_cost('carnophage', ['B']).
card_cmc('carnophage', 1).
card_layout('carnophage', 'normal').
card_power('carnophage', 2).
card_toughness('carnophage', 2).

% Found in: DIS
card_name('carom', 'Carom').
card_type('carom', 'Instant').
card_types('carom', ['Instant']).
card_subtypes('carom', []).
card_colors('carom', ['W']).
card_text('carom', 'The next 1 damage that would be dealt to target creature this turn is dealt to another target creature instead.\nDraw a card.').
card_mana_cost('carom', ['1', 'W']).
card_cmc('carom', 2).
card_layout('carom', 'normal').

% Found in: USG
card_name('carpet of flowers', 'Carpet of Flowers').
card_type('carpet of flowers', 'Enchantment').
card_types('carpet of flowers', ['Enchantment']).
card_subtypes('carpet of flowers', []).
card_colors('carpet of flowers', ['G']).
card_text('carpet of flowers', 'At the beginning of each of your main phases, if you haven\'t added mana to your mana pool with this ability this turn, you may add up to X mana of any one color to your mana pool, where X is the number of Islands target opponent controls.').
card_mana_cost('carpet of flowers', ['G']).
card_cmc('carpet of flowers', 1).
card_layout('carpet of flowers', 'normal').

% Found in: ALL
card_name('carrier pigeons', 'Carrier Pigeons').
card_type('carrier pigeons', 'Creature — Bird').
card_types('carrier pigeons', ['Creature']).
card_subtypes('carrier pigeons', ['Bird']).
card_colors('carrier pigeons', ['W']).
card_text('carrier pigeons', 'Flying\nWhen Carrier Pigeons enters the battlefield, draw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('carrier pigeons', ['3', 'W']).
card_cmc('carrier pigeons', 4).
card_layout('carrier pigeons', 'normal').
card_power('carrier pigeons', 1).
card_toughness('carrier pigeons', 1).

% Found in: BFZ
card_name('carrier thrall', 'Carrier Thrall').
card_type('carrier thrall', 'Creature — Vampire').
card_types('carrier thrall', ['Creature']).
card_subtypes('carrier thrall', ['Vampire']).
card_colors('carrier thrall', ['B']).
card_text('carrier thrall', 'When Carrier Thrall dies, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('carrier thrall', ['1', 'B']).
card_cmc('carrier thrall', 2).
card_layout('carrier thrall', 'normal').
card_power('carrier thrall', 2).
card_toughness('carrier thrall', 1).

% Found in: MIR
card_name('carrion', 'Carrion').
card_type('carrion', 'Instant').
card_types('carrion', ['Instant']).
card_subtypes('carrion', []).
card_colors('carrion', ['B']).
card_text('carrion', 'As an additional cost to cast Carrion, sacrifice a creature.\nPut X 0/1 black Insect creature tokens onto the battlefield, where X is the sacrificed creature\'s power.').
card_mana_cost('carrion', ['1', 'B', 'B']).
card_cmc('carrion', 3).
card_layout('carrion', 'normal').
card_reserved('carrion').

% Found in: 4ED, 5ED, LEG, ME3
card_name('carrion ants', 'Carrion Ants').
card_type('carrion ants', 'Creature — Insect').
card_types('carrion ants', ['Creature']).
card_subtypes('carrion ants', ['Insect']).
card_colors('carrion ants', ['B']).
card_text('carrion ants', '{1}: Carrion Ants gets +1/+1 until end of turn.').
card_mana_cost('carrion ants', ['2', 'B', 'B']).
card_cmc('carrion ants', 4).
card_layout('carrion ants', 'normal').
card_power('carrion ants', 0).
card_toughness('carrion ants', 1).

% Found in: USG
card_name('carrion beetles', 'Carrion Beetles').
card_type('carrion beetles', 'Creature — Insect').
card_types('carrion beetles', ['Creature']).
card_subtypes('carrion beetles', ['Insect']).
card_colors('carrion beetles', ['B']).
card_text('carrion beetles', '{2}{B}, {T}: Exile up to three target cards from a single graveyard.').
card_mana_cost('carrion beetles', ['B']).
card_cmc('carrion beetles', 1).
card_layout('carrion beetles', 'normal').
card_power('carrion beetles', 1).
card_toughness('carrion beetles', 1).

% Found in: SOM
card_name('carrion call', 'Carrion Call').
card_type('carrion call', 'Instant').
card_types('carrion call', ['Instant']).
card_subtypes('carrion call', []).
card_colors('carrion call', ['G']).
card_text('carrion call', 'Put two 1/1 green Insect creature tokens with infect onto the battlefield. (They deal damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('carrion call', ['3', 'G']).
card_cmc('carrion call', 4).
card_layout('carrion call', 'normal').

% Found in: M15
card_name('carrion crow', 'Carrion Crow').
card_type('carrion crow', 'Creature — Zombie Bird').
card_types('carrion crow', ['Creature']).
card_subtypes('carrion crow', ['Zombie', 'Bird']).
card_colors('carrion crow', ['B']).
card_text('carrion crow', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nCarrion Crow enters the battlefield tapped.').
card_mana_cost('carrion crow', ['2', 'B']).
card_cmc('carrion crow', 3).
card_layout('carrion crow', 'normal').
card_power('carrion crow', 2).
card_toughness('carrion crow', 2).

% Found in: DDE, SCG, pFNM
card_name('carrion feeder', 'Carrion Feeder').
card_type('carrion feeder', 'Creature — Zombie').
card_types('carrion feeder', ['Creature']).
card_subtypes('carrion feeder', ['Zombie']).
card_colors('carrion feeder', ['B']).
card_text('carrion feeder', 'Carrion Feeder can\'t block.\nSacrifice a creature: Put a +1/+1 counter on Carrion Feeder.').
card_mana_cost('carrion feeder', ['B']).
card_cmc('carrion feeder', 1).
card_layout('carrion feeder', 'normal').
card_power('carrion feeder', 1).
card_toughness('carrion feeder', 1).

% Found in: RAV
card_name('carrion howler', 'Carrion Howler').
card_type('carrion howler', 'Creature — Zombie Wolf').
card_types('carrion howler', ['Creature']).
card_subtypes('carrion howler', ['Zombie', 'Wolf']).
card_colors('carrion howler', ['B']).
card_text('carrion howler', 'Pay 1 life: Carrion Howler gets +2/-1 until end of turn.').
card_mana_cost('carrion howler', ['3', 'B']).
card_cmc('carrion howler', 4).
card_layout('carrion howler', 'normal').
card_power('carrion howler', 2).
card_toughness('carrion howler', 2).

% Found in: TOR
card_name('carrion rats', 'Carrion Rats').
card_type('carrion rats', 'Creature — Rat').
card_types('carrion rats', ['Creature']).
card_subtypes('carrion rats', ['Rat']).
card_colors('carrion rats', ['B']).
card_text('carrion rats', 'Whenever Carrion Rats attacks or blocks, any player may exile a card from his or her graveyard. If a player does, Carrion Rats assigns no combat damage this turn.').
card_mana_cost('carrion rats', ['B']).
card_cmc('carrion rats', 1).
card_layout('carrion rats', 'normal').
card_power('carrion rats', 2).
card_toughness('carrion rats', 1).

% Found in: ALA
card_name('carrion thrash', 'Carrion Thrash').
card_type('carrion thrash', 'Creature — Viashino Warrior').
card_types('carrion thrash', ['Creature']).
card_subtypes('carrion thrash', ['Viashino', 'Warrior']).
card_colors('carrion thrash', ['B', 'R', 'G']).
card_text('carrion thrash', 'When Carrion Thrash dies, you may pay {2}. If you do, return another target creature card from your graveyard to your hand.').
card_mana_cost('carrion thrash', ['2', 'B', 'R', 'G']).
card_cmc('carrion thrash', 5).
card_layout('carrion thrash', 'normal').
card_power('carrion thrash', 4).
card_toughness('carrion thrash', 4).

% Found in: 8ED, NMS
card_name('carrion wall', 'Carrion Wall').
card_type('carrion wall', 'Creature — Wall').
card_types('carrion wall', ['Creature']).
card_subtypes('carrion wall', ['Wall']).
card_colors('carrion wall', ['B']).
card_text('carrion wall', 'Defender (This creature can\'t attack.)\n{1}{B}: Regenerate Carrion Wall.').
card_mana_cost('carrion wall', ['1', 'B', 'B']).
card_cmc('carrion wall', 3).
card_layout('carrion wall', 'normal').
card_power('carrion wall', 3).
card_toughness('carrion wall', 2).

% Found in: TOR
card_name('carrion wurm', 'Carrion Wurm').
card_type('carrion wurm', 'Creature — Zombie Wurm').
card_types('carrion wurm', ['Creature']).
card_subtypes('carrion wurm', ['Zombie', 'Wurm']).
card_colors('carrion wurm', ['B']).
card_text('carrion wurm', 'Whenever Carrion Wurm attacks or blocks, any player may exile three cards from his or her graveyard. If a player does, Carrion Wurm assigns no combat damage this turn.').
card_mana_cost('carrion wurm', ['3', 'B', 'B']).
card_cmc('carrion wurm', 5).
card_layout('carrion wurm', 'normal').
card_power('carrion wurm', 6).
card_toughness('carrion wurm', 5).

% Found in: TMP
card_name('carrionette', 'Carrionette').
card_type('carrionette', 'Creature — Skeleton').
card_types('carrionette', ['Creature']).
card_subtypes('carrionette', ['Skeleton']).
card_colors('carrionette', ['B']).
card_text('carrionette', '{2}{B}{B}: Exile Carrionette and target creature unless that creature\'s controller pays {2}. Activate this ability only if Carrionette is in your graveyard.').
card_mana_cost('carrionette', ['1', 'B']).
card_cmc('carrionette', 2).
card_layout('carrionette', 'normal').
card_power('carrionette', 1).
card_toughness('carrionette', 1).

% Found in: DST
card_name('carry away', 'Carry Away').
card_type('carry away', 'Enchantment — Aura').
card_types('carry away', ['Enchantment']).
card_subtypes('carry away', ['Aura']).
card_colors('carry away', ['U']).
card_text('carry away', 'Enchant Equipment\nWhen Carry Away enters the battlefield, unattach enchanted Equipment.\nYou control enchanted Equipment.').
card_mana_cost('carry away', ['U', 'U']).
card_cmc('carry away', 2).
card_layout('carry away', 'normal').

% Found in: GTC
card_name('cartel aristocrat', 'Cartel Aristocrat').
card_type('cartel aristocrat', 'Creature — Human Advisor').
card_types('cartel aristocrat', ['Creature']).
card_subtypes('cartel aristocrat', ['Human', 'Advisor']).
card_colors('cartel aristocrat', ['W', 'B']).
card_text('cartel aristocrat', 'Sacrifice another creature: Cartel Aristocrat gains protection from the color of your choice until end of turn.').
card_mana_cost('cartel aristocrat', ['W', 'B']).
card_cmc('cartel aristocrat', 2).
card_layout('cartel aristocrat', 'normal').
card_power('cartel aristocrat', 2).
card_toughness('cartel aristocrat', 2).

% Found in: EXO, ODY
card_name('cartographer', 'Cartographer').
card_type('cartographer', 'Creature — Human').
card_types('cartographer', ['Creature']).
card_subtypes('cartographer', ['Human']).
card_colors('cartographer', ['G']).
card_text('cartographer', 'When Cartographer enters the battlefield, you may return target land card from your graveyard to your hand.').
card_mana_cost('cartographer', ['2', 'G']).
card_cmc('cartographer', 3).
card_layout('cartographer', 'normal').
card_power('cartographer', 2).
card_toughness('cartographer', 2).

% Found in: RAV
card_name('carven caryatid', 'Carven Caryatid').
card_type('carven caryatid', 'Creature — Spirit').
card_types('carven caryatid', ['Creature']).
card_subtypes('carven caryatid', ['Spirit']).
card_colors('carven caryatid', ['G']).
card_text('carven caryatid', 'Defender (This creature can\'t attack.)\nWhen Carven Caryatid enters the battlefield, draw a card.').
card_mana_cost('carven caryatid', ['1', 'G', 'G']).
card_cmc('carven caryatid', 3).
card_layout('carven caryatid', 'normal').
card_power('carven caryatid', 2).
card_toughness('carven caryatid', 5).

% Found in: EVE
card_name('cascade bluffs', 'Cascade Bluffs').
card_type('cascade bluffs', 'Land').
card_types('cascade bluffs', ['Land']).
card_subtypes('cascade bluffs', []).
card_colors('cascade bluffs', []).
card_text('cascade bluffs', '{T}: Add {1} to your mana pool.\n{U/R}, {T}: Add {U}{U}, {U}{R}, or {R}{R} to your mana pool.').
card_layout('cascade bluffs', 'normal').

% Found in: JOU
card_name('cast into darkness', 'Cast into Darkness').
card_type('cast into darkness', 'Enchantment — Aura').
card_types('cast into darkness', ['Enchantment']).
card_subtypes('cast into darkness', ['Aura']).
card_colors('cast into darkness', ['B']).
card_text('cast into darkness', 'Enchant creature\nEnchanted creature gets -2/-0 and can\'t block.').
card_mana_cost('cast into darkness', ['1', 'B']).
card_cmc('cast into darkness', 2).
card_layout('cast into darkness', 'normal').

% Found in: ROE
card_name('cast through time', 'Cast Through Time').
card_type('cast through time', 'Enchantment').
card_types('cast through time', ['Enchantment']).
card_subtypes('cast through time', []).
card_colors('cast through time', ['U']).
card_text('cast through time', 'Instant and sorcery spells you control have rebound. (Exile the spell as it resolves if you cast it from your hand. At the beginning of your next upkeep, you may cast that card from exile without paying its mana cost.)').
card_mana_cost('cast through time', ['4', 'U', 'U', 'U']).
card_cmc('cast through time', 7).
card_layout('cast through time', 'normal').

% Found in: GPT, pARL
card_name('castigate', 'Castigate').
card_type('castigate', 'Sorcery').
card_types('castigate', ['Sorcery']).
card_subtypes('castigate', []).
card_colors('castigate', ['W', 'B']).
card_text('castigate', 'Target opponent reveals his or her hand. You choose a nonland card from it and exile that card.').
card_mana_cost('castigate', ['W', 'B']).
card_cmc('castigate', 2).
card_layout('castigate', 'normal').

% Found in: ALL, CST
card_name('casting of bones', 'Casting of Bones').
card_type('casting of bones', 'Enchantment — Aura').
card_types('casting of bones', ['Enchantment']).
card_subtypes('casting of bones', ['Aura']).
card_colors('casting of bones', ['B']).
card_text('casting of bones', 'Enchant creature\nWhen enchanted creature dies, draw three cards, then discard one of them.').
card_mana_cost('casting of bones', ['2', 'B']).
card_cmc('casting of bones', 3).
card_layout('casting of bones', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, CED, CEI, LEA, LEB
card_name('castle', 'Castle').
card_type('castle', 'Enchantment').
card_types('castle', ['Enchantment']).
card_subtypes('castle', []).
card_colors('castle', ['W']).
card_text('castle', 'Untapped creatures you control get +0/+2.').
card_mana_cost('castle', ['3', 'W']).
card_cmc('castle', 4).
card_layout('castle', 'normal').

% Found in: TSP
card_name('castle raptors', 'Castle Raptors').
card_type('castle raptors', 'Creature — Bird Soldier').
card_types('castle raptors', ['Creature']).
card_subtypes('castle raptors', ['Bird', 'Soldier']).
card_colors('castle raptors', ['W']).
card_text('castle raptors', 'Flying\nAs long as Castle Raptors is untapped, it gets +0/+2.').
card_mana_cost('castle raptors', ['4', 'W']).
card_cmc('castle raptors', 5).
card_layout('castle raptors', 'normal').
card_power('castle raptors', 3).
card_toughness('castle raptors', 3).

% Found in: HML
card_name('castle sengir', 'Castle Sengir').
card_type('castle sengir', 'Land').
card_types('castle sengir', ['Land']).
card_subtypes('castle sengir', []).
card_colors('castle sengir', []).
card_text('castle sengir', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add {B} to your mana pool.\n{2}, {T}: Add {U} or {R} to your mana pool.').
card_layout('castle sengir', 'normal').

% Found in: EXO
card_name('cat burglar', 'Cat Burglar').
card_type('cat burglar', 'Creature — Kor Rogue Minion').
card_types('cat burglar', ['Creature']).
card_subtypes('cat burglar', ['Kor', 'Rogue', 'Minion']).
card_colors('cat burglar', ['B']).
card_text('cat burglar', '{2}{B}, {T}: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('cat burglar', ['3', 'B']).
card_cmc('cat burglar', 4).
card_layout('cat burglar', 'normal').
card_power('cat burglar', 2).
card_toughness('cat burglar', 2).

% Found in: 5ED, 6ED, CHR, LEG
card_name('cat warriors', 'Cat Warriors').
card_type('cat warriors', 'Creature — Cat Warrior').
card_types('cat warriors', ['Creature']).
card_subtypes('cat warriors', ['Cat', 'Warrior']).
card_colors('cat warriors', ['G']).
card_text('cat warriors', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)').
card_mana_cost('cat warriors', ['1', 'G', 'G']).
card_cmc('cat warriors', 3).
card_layout('cat warriors', 'normal').
card_power('cat warriors', 2).
card_toughness('cat warriors', 2).

% Found in: EXO, TPR, V14
card_name('cataclysm', 'Cataclysm').
card_type('cataclysm', 'Sorcery').
card_types('cataclysm', ['Sorcery']).
card_subtypes('cataclysm', []).
card_colors('cataclysm', ['W']).
card_text('cataclysm', 'Each player chooses from among the permanents he or she controls an artifact, a creature, an enchantment, and a land, then sacrifices the rest.').
card_mana_cost('cataclysm', ['2', 'W', 'W']).
card_cmc('cataclysm', 4).
card_layout('cataclysm', 'normal').

% Found in: MIR
card_name('catacomb dragon', 'Catacomb Dragon').
card_type('catacomb dragon', 'Creature — Dragon').
card_types('catacomb dragon', ['Creature']).
card_subtypes('catacomb dragon', ['Dragon']).
card_colors('catacomb dragon', ['B']).
card_text('catacomb dragon', 'Flying\nWhenever Catacomb Dragon becomes blocked by a nonartifact, non-Dragon creature, that creature gets -X/-0 until end of turn, where X is half the creature\'s power, rounded down.').
card_mana_cost('catacomb dragon', ['4', 'B', 'B']).
card_cmc('catacomb dragon', 6).
card_layout('catacomb dragon', 'normal').
card_power('catacomb dragon', 4).
card_toughness('catacomb dragon', 4).
card_reserved('catacomb dragon').

% Found in: BFZ
card_name('catacomb sifter', 'Catacomb Sifter').
card_type('catacomb sifter', 'Creature — Eldrazi Drone').
card_types('catacomb sifter', ['Creature']).
card_subtypes('catacomb sifter', ['Eldrazi', 'Drone']).
card_colors('catacomb sifter', []).
card_text('catacomb sifter', 'Devoid (This card has no color.)\nWhen Catacomb Sifter enters the battlefield, put a 1/1 colorless Eldrazi Scion creature token onto the battlefield. It has \"Sacrifice this creature: Add {1} to your mana pool.\"\nWhenever another creature you control dies, scry 1. (Look at the top card of your library. You may put that card on the bottom of your library.)').
card_mana_cost('catacomb sifter', ['1', 'B', 'G']).
card_cmc('catacomb sifter', 3).
card_layout('catacomb sifter', 'normal').
card_power('catacomb sifter', 2).
card_toughness('catacomb sifter', 3).

% Found in: ORI, RTR
card_name('catacomb slug', 'Catacomb Slug').
card_type('catacomb slug', 'Creature — Slug').
card_types('catacomb slug', ['Creature']).
card_subtypes('catacomb slug', ['Slug']).
card_colors('catacomb slug', ['B']).
card_text('catacomb slug', '').
card_mana_cost('catacomb slug', ['4', 'B']).
card_cmc('catacomb slug', 5).
card_layout('catacomb slug', 'normal').
card_power('catacomb slug', 2).
card_toughness('catacomb slug', 6).

% Found in: 8ED, USG
card_name('catalog', 'Catalog').
card_type('catalog', 'Instant').
card_types('catalog', ['Instant']).
card_subtypes('catalog', []).
card_colors('catalog', ['U']).
card_text('catalog', 'Draw two cards, then discard a card.').
card_mana_cost('catalog', ['2', 'U']).
card_cmc('catalog', 3).
card_layout('catalog', 'normal').

% Found in: ODY
card_name('catalyst stone', 'Catalyst Stone').
card_type('catalyst stone', 'Artifact').
card_types('catalyst stone', ['Artifact']).
card_subtypes('catalyst stone', []).
card_colors('catalyst stone', []).
card_text('catalyst stone', 'Flashback costs you pay cost up to {2} less.\nFlashback costs your opponents pay cost {2} more.').
card_mana_cost('catalyst stone', ['2']).
card_cmc('catalyst stone', 2).
card_layout('catalyst stone', 'normal').

% Found in: DDF, ONS
card_name('catapult master', 'Catapult Master').
card_type('catapult master', 'Creature — Human Soldier').
card_types('catapult master', ['Creature']).
card_subtypes('catapult master', ['Human', 'Soldier']).
card_colors('catapult master', ['W']).
card_text('catapult master', 'Tap five untapped Soldiers you control: Exile target creature.').
card_mana_cost('catapult master', ['3', 'W', 'W']).
card_cmc('catapult master', 5).
card_layout('catapult master', 'normal').
card_power('catapult master', 3).
card_toughness('catapult master', 3).

% Found in: ONS
card_name('catapult squad', 'Catapult Squad').
card_type('catapult squad', 'Creature — Human Soldier').
card_types('catapult squad', ['Creature']).
card_subtypes('catapult squad', ['Human', 'Soldier']).
card_colors('catapult squad', ['W']).
card_text('catapult squad', 'Tap two untapped Soldiers you control: Catapult Squad deals 2 damage to target attacking or blocking creature.').
card_mana_cost('catapult squad', ['1', 'W']).
card_cmc('catapult squad', 2).
card_layout('catapult squad', 'normal').
card_power('catapult squad', 2).
card_toughness('catapult squad', 1).

% Found in: BRB, USG
card_name('catastrophe', 'Catastrophe').
card_type('catastrophe', 'Sorcery').
card_types('catastrophe', ['Sorcery']).
card_subtypes('catastrophe', []).
card_colors('catastrophe', ['W']).
card_text('catastrophe', 'Destroy all lands or all creatures. Creatures destroyed this way can\'t be regenerated.').
card_mana_cost('catastrophe', ['4', 'W', 'W']).
card_cmc('catastrophe', 6).
card_layout('catastrophe', 'normal').

% Found in: DGM
card_name('catch', 'Catch').
card_type('catch', 'Sorcery').
card_types('catch', ['Sorcery']).
card_subtypes('catch', []).
card_colors('catch', ['U', 'R']).
card_text('catch', 'Gain control of target permanent until end of turn. Untap it. It gains haste until end of turn.\nFuse (You may cast one or both halves of this card from your hand.)').
card_mana_cost('catch', ['1', 'U', 'R']).
card_cmc('catch', 3).
card_layout('catch', 'split').
card_sides('catch', 'release').

% Found in: MMQ
card_name('cateran brute', 'Cateran Brute').
card_type('cateran brute', 'Creature — Horror Mercenary').
card_types('cateran brute', ['Creature']).
card_subtypes('cateran brute', ['Horror', 'Mercenary']).
card_colors('cateran brute', ['B']).
card_text('cateran brute', '{2}, {T}: Search your library for a Mercenary permanent card with converted mana cost 2 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('cateran brute', ['2', 'B']).
card_cmc('cateran brute', 3).
card_layout('cateran brute', 'normal').
card_power('cateran brute', 2).
card_toughness('cateran brute', 2).

% Found in: MMQ
card_name('cateran enforcer', 'Cateran Enforcer').
card_type('cateran enforcer', 'Creature — Horror Mercenary').
card_types('cateran enforcer', ['Creature']).
card_subtypes('cateran enforcer', ['Horror', 'Mercenary']).
card_colors('cateran enforcer', ['B']).
card_text('cateran enforcer', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{4}, {T}: Search your library for a Mercenary permanent card with converted mana cost 4 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('cateran enforcer', ['3', 'B', 'B']).
card_cmc('cateran enforcer', 5).
card_layout('cateran enforcer', 'normal').
card_power('cateran enforcer', 4).
card_toughness('cateran enforcer', 3).

% Found in: MMQ
card_name('cateran kidnappers', 'Cateran Kidnappers').
card_type('cateran kidnappers', 'Creature — Human Mercenary').
card_types('cateran kidnappers', ['Creature']).
card_subtypes('cateran kidnappers', ['Human', 'Mercenary']).
card_colors('cateran kidnappers', ['B']).
card_text('cateran kidnappers', '{3}, {T}: Search your library for a Mercenary permanent card with converted mana cost 3 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('cateran kidnappers', ['2', 'B', 'B']).
card_cmc('cateran kidnappers', 4).
card_layout('cateran kidnappers', 'normal').
card_power('cateran kidnappers', 4).
card_toughness('cateran kidnappers', 2).

% Found in: MMQ
card_name('cateran overlord', 'Cateran Overlord').
card_type('cateran overlord', 'Creature — Horror Mercenary').
card_types('cateran overlord', ['Creature']).
card_subtypes('cateran overlord', ['Horror', 'Mercenary']).
card_colors('cateran overlord', ['B']).
card_text('cateran overlord', 'Sacrifice a creature: Regenerate Cateran Overlord.\n{6}, {T}: Search your library for a Mercenary permanent card with converted mana cost 6 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('cateran overlord', ['4', 'B', 'B', 'B']).
card_cmc('cateran overlord', 7).
card_layout('cateran overlord', 'normal').
card_power('cateran overlord', 7).
card_toughness('cateran overlord', 5).

% Found in: MMQ
card_name('cateran persuader', 'Cateran Persuader').
card_type('cateran persuader', 'Creature — Human Mercenary').
card_types('cateran persuader', ['Creature']).
card_subtypes('cateran persuader', ['Human', 'Mercenary']).
card_colors('cateran persuader', ['B']).
card_text('cateran persuader', '{1}, {T}: Search your library for a Mercenary permanent card with converted mana cost 1 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('cateran persuader', ['B', 'B']).
card_cmc('cateran persuader', 2).
card_layout('cateran persuader', 'normal').
card_power('cateran persuader', 2).
card_toughness('cateran persuader', 1).

% Found in: MMQ
card_name('cateran slaver', 'Cateran Slaver').
card_type('cateran slaver', 'Creature — Horror Mercenary').
card_types('cateran slaver', ['Creature']).
card_subtypes('cateran slaver', ['Horror', 'Mercenary']).
card_colors('cateran slaver', ['B']).
card_text('cateran slaver', 'Swampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\n{5}, {T}: Search your library for a Mercenary permanent card with converted mana cost 5 or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('cateran slaver', ['4', 'B', 'B']).
card_cmc('cateran slaver', 6).
card_layout('cateran slaver', 'normal').
card_power('cateran slaver', 5).
card_toughness('cateran slaver', 5).

% Found in: MMQ
card_name('cateran summons', 'Cateran Summons').
card_type('cateran summons', 'Sorcery').
card_types('cateran summons', ['Sorcery']).
card_subtypes('cateran summons', []).
card_colors('cateran summons', ['B']).
card_text('cateran summons', 'Search your library for a Mercenary card, reveal that card, and put it into your hand. Then shuffle your library.').
card_mana_cost('cateran summons', ['B']).
card_cmc('cateran summons', 1).
card_layout('cateran summons', 'normal').

% Found in: LRW
card_name('caterwauling boggart', 'Caterwauling Boggart').
card_type('caterwauling boggart', 'Creature — Goblin Shaman').
card_types('caterwauling boggart', ['Creature']).
card_subtypes('caterwauling boggart', ['Goblin', 'Shaman']).
card_colors('caterwauling boggart', ['R']).
card_text('caterwauling boggart', 'Goblins you control and Elementals you control have menace. (They can\'t be blocked except by two or more creatures.)').
card_mana_cost('caterwauling boggart', ['3', 'R']).
card_cmc('caterwauling boggart', 4).
card_layout('caterwauling boggart', 'normal').
card_power('caterwauling boggart', 2).
card_toughness('caterwauling boggart', 2).

% Found in: AVR, C14
card_name('cathars\' crusade', 'Cathars\' Crusade').
card_type('cathars\' crusade', 'Enchantment').
card_types('cathars\' crusade', ['Enchantment']).
card_subtypes('cathars\' crusade', []).
card_colors('cathars\' crusade', ['W']).
card_text('cathars\' crusade', 'Whenever a creature enters the battlefield under your control, put a +1/+1 counter on each creature you control.').
card_mana_cost('cathars\' crusade', ['3', 'W', 'W']).
card_cmc('cathars\' crusade', 5).
card_layout('cathars\' crusade', 'normal').

% Found in: ALA
card_name('cathartic adept', 'Cathartic Adept').
card_type('cathartic adept', 'Creature — Human Wizard').
card_types('cathartic adept', ['Creature']).
card_subtypes('cathartic adept', ['Human', 'Wizard']).
card_colors('cathartic adept', ['U']).
card_text('cathartic adept', '{T}: Target player puts the top card of his or her library into his or her graveyard.').
card_mana_cost('cathartic adept', ['U']).
card_cmc('cathartic adept', 1).
card_layout('cathartic adept', 'normal').
card_power('cathartic adept', 1).
card_toughness('cathartic adept', 1).

% Found in: NPH
card_name('cathedral membrane', 'Cathedral Membrane').
card_type('cathedral membrane', 'Artifact Creature — Wall').
card_types('cathedral membrane', ['Artifact', 'Creature']).
card_subtypes('cathedral membrane', ['Wall']).
card_colors('cathedral membrane', ['W']).
card_text('cathedral membrane', '({W/P} can be paid with either {W} or 2 life.)\nDefender\nWhen Cathedral Membrane dies during combat, it deals 6 damage to each creature it blocked this combat.').
card_mana_cost('cathedral membrane', ['1', 'W/P']).
card_cmc('cathedral membrane', 2).
card_layout('cathedral membrane', 'normal').
card_power('cathedral membrane', 0).
card_toughness('cathedral membrane', 3).

% Found in: LEG
card_name('cathedral of serra', 'Cathedral of Serra').
card_type('cathedral of serra', 'Land').
card_types('cathedral of serra', ['Land']).
card_subtypes('cathedral of serra', []).
card_colors('cathedral of serra', []).
card_text('cathedral of serra', 'White legendary creatures you control have \"bands with other legendary creatures.\" (Any legendary creatures can attack in a band as long as at least one has \"bands with other legendary creatures.\" Bands are blocked as a group. If at least two legendary creatures you control, one of which has \"bands with other legendary creatures,\" are blocking or being blocked by the same creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_layout('cathedral of serra', 'normal').

% Found in: M13, pMEI
card_name('cathedral of war', 'Cathedral of War').
card_type('cathedral of war', 'Land').
card_types('cathedral of war', ['Land']).
card_subtypes('cathedral of war', []).
card_colors('cathedral of war', []).
card_text('cathedral of war', 'Cathedral of War enters the battlefield tapped.\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{T}: Add {1} to your mana pool.').
card_layout('cathedral of war', 'normal').

% Found in: AVR
card_name('cathedral sanctifier', 'Cathedral Sanctifier').
card_type('cathedral sanctifier', 'Creature — Human Cleric').
card_types('cathedral sanctifier', ['Creature']).
card_subtypes('cathedral sanctifier', ['Human', 'Cleric']).
card_colors('cathedral sanctifier', ['W']).
card_text('cathedral sanctifier', 'When Cathedral Sanctifier enters the battlefield, you gain 3 life.').
card_mana_cost('cathedral sanctifier', ['W']).
card_cmc('cathedral sanctifier', 1).
card_layout('cathedral sanctifier', 'normal').
card_power('cathedral sanctifier', 1).
card_toughness('cathedral sanctifier', 1).

% Found in: C14, MM2, MRD, USG
card_name('cathodion', 'Cathodion').
card_type('cathodion', 'Artifact Creature — Construct').
card_types('cathodion', ['Artifact', 'Creature']).
card_subtypes('cathodion', ['Construct']).
card_colors('cathodion', []).
card_text('cathodion', 'When Cathodion dies, add {3} to your mana pool.').
card_mana_cost('cathodion', ['3']).
card_cmc('cathodion', 3).
card_layout('cathodion', 'normal').
card_power('cathodion', 3).
card_toughness('cathodion', 3).

% Found in: INV
card_name('cauldron dance', 'Cauldron Dance').
card_type('cauldron dance', 'Instant').
card_types('cauldron dance', ['Instant']).
card_subtypes('cauldron dance', []).
card_colors('cauldron dance', ['B', 'R']).
card_text('cauldron dance', 'Cast Cauldron Dance only during combat.\nReturn target creature card from your graveyard to the battlefield. That creature gains haste. Return it to your hand at the beginning of the next end step.\nYou may put a creature card from your hand onto the battlefield. That creature gains haste. Its controller sacrifices it at the beginning of the next end step.').
card_mana_cost('cauldron dance', ['4', 'B', 'R']).
card_cmc('cauldron dance', 6).
card_layout('cauldron dance', 'normal').

% Found in: EVE
card_name('cauldron haze', 'Cauldron Haze').
card_type('cauldron haze', 'Instant').
card_types('cauldron haze', ['Instant']).
card_subtypes('cauldron haze', []).
card_colors('cauldron haze', ['W', 'B']).
card_text('cauldron haze', 'Choose any number of target creatures. Each of those creatures gains persist until end of turn. (When it dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('cauldron haze', ['1', 'W/B']).
card_cmc('cauldron haze', 2).
card_layout('cauldron haze', 'normal').

% Found in: SHM
card_name('cauldron of souls', 'Cauldron of Souls').
card_type('cauldron of souls', 'Artifact').
card_types('cauldron of souls', ['Artifact']).
card_subtypes('cauldron of souls', []).
card_colors('cauldron of souls', []).
card_text('cauldron of souls', '{T}: Choose any number of target creatures. Each of those creatures gains persist until end of turn. (When it dies, if it had no -1/-1 counters on it, return it to the battlefield under its owner\'s control with a -1/-1 counter on it.)').
card_mana_cost('cauldron of souls', ['5']).
card_cmc('cauldron of souls', 5).
card_layout('cauldron of souls', 'normal').

% Found in: ORI
card_name('caustic caterpillar', 'Caustic Caterpillar').
card_type('caustic caterpillar', 'Creature — Insect').
card_types('caustic caterpillar', ['Creature']).
card_subtypes('caustic caterpillar', ['Insect']).
card_colors('caustic caterpillar', ['G']).
card_text('caustic caterpillar', '{1}{G}, Sacrifice Caustic Caterpillar: Destroy target artifact or enchantment.').
card_mana_cost('caustic caterpillar', ['G']).
card_cmc('caustic caterpillar', 1).
card_layout('caustic caterpillar', 'normal').
card_power('caustic caterpillar', 1).
card_toughness('caustic caterpillar', 1).

% Found in: WWK
card_name('caustic crawler', 'Caustic Crawler').
card_type('caustic crawler', 'Creature — Insect').
card_types('caustic crawler', ['Creature']).
card_subtypes('caustic crawler', ['Insect']).
card_colors('caustic crawler', ['B']).
card_text('caustic crawler', 'Landfall — Whenever a land enters the battlefield under your control, you may have target creature get -1/-1 until end of turn.').
card_mana_cost('caustic crawler', ['3', 'B', 'B']).
card_cmc('caustic crawler', 5).
card_layout('caustic crawler', 'normal').
card_power('caustic crawler', 4).
card_toughness('caustic crawler', 3).

% Found in: MBS
card_name('caustic hound', 'Caustic Hound').
card_type('caustic hound', 'Creature — Hound').
card_types('caustic hound', ['Creature']).
card_subtypes('caustic hound', ['Hound']).
card_colors('caustic hound', ['B']).
card_text('caustic hound', 'When Caustic Hound dies, each player loses 4 life.').
card_mana_cost('caustic hound', ['5', 'B']).
card_cmc('caustic hound', 6).
card_layout('caustic hound', 'normal').
card_power('caustic hound', 4).
card_toughness('caustic hound', 4).

% Found in: GPT
card_name('caustic rain', 'Caustic Rain').
card_type('caustic rain', 'Sorcery').
card_types('caustic rain', ['Sorcery']).
card_subtypes('caustic rain', []).
card_colors('caustic rain', ['B']).
card_text('caustic rain', 'Exile target land.').
card_mana_cost('caustic rain', ['2', 'B', 'B']).
card_cmc('caustic rain', 4).
card_layout('caustic rain', 'normal').

% Found in: M15, ODY
card_name('caustic tar', 'Caustic Tar').
card_type('caustic tar', 'Enchantment — Aura').
card_types('caustic tar', ['Enchantment']).
card_subtypes('caustic tar', ['Aura']).
card_colors('caustic tar', ['B']).
card_text('caustic tar', 'Enchant land\nEnchanted land has \"{T}: Target player loses 3 life.\"').
card_mana_cost('caustic tar', ['4', 'B', 'B']).
card_cmc('caustic tar', 6).
card_layout('caustic tar', 'normal').

% Found in: MMQ
card_name('caustic wasps', 'Caustic Wasps').
card_type('caustic wasps', 'Creature — Insect').
card_types('caustic wasps', ['Creature']).
card_subtypes('caustic wasps', ['Insect']).
card_colors('caustic wasps', ['G']).
card_text('caustic wasps', 'Flying\nWhenever Caustic Wasps deals combat damage to a player, you may destroy target artifact that player controls.').
card_mana_cost('caustic wasps', ['2', 'G']).
card_cmc('caustic wasps', 3).
card_layout('caustic wasps', 'normal').
card_power('caustic wasps', 1).
card_toughness('caustic wasps', 1).

% Found in: PLC
card_name('cautery sliver', 'Cautery Sliver').
card_type('cautery sliver', 'Creature — Sliver').
card_types('cautery sliver', ['Creature']).
card_subtypes('cautery sliver', ['Sliver']).
card_colors('cautery sliver', ['W', 'R']).
card_text('cautery sliver', 'All Slivers have \"{1}, Sacrifice this permanent: This permanent deals 1 damage to target creature or player.\"\nAll Slivers have \"{1}, Sacrifice this permanent: Prevent the next 1 damage that would be dealt to target Sliver creature or player this turn.\"').
card_mana_cost('cautery sliver', ['R', 'W']).
card_cmc('cautery sliver', 2).
card_layout('cautery sliver', 'normal').
card_power('cautery sliver', 2).
card_toughness('cautery sliver', 2).

% Found in: TSP
card_name('cavalry master', 'Cavalry Master').
card_type('cavalry master', 'Creature — Human Knight').
card_types('cavalry master', ['Creature']).
card_subtypes('cavalry master', ['Human', 'Knight']).
card_colors('cavalry master', ['W']).
card_text('cavalry master', 'Flanking (Whenever a creature without flanking blocks this creature, the blocking creature gets -1/-1 until end of turn.)\nOther creatures you control with flanking have flanking. (Each instance of flanking triggers separately.)').
card_mana_cost('cavalry master', ['2', 'W', 'W']).
card_cmc('cavalry master', 4).
card_layout('cavalry master', 'normal').
card_power('cavalry master', 3).
card_toughness('cavalry master', 3).

% Found in: DDL, THS
card_name('cavalry pegasus', 'Cavalry Pegasus').
card_type('cavalry pegasus', 'Creature — Pegasus').
card_types('cavalry pegasus', ['Creature']).
card_subtypes('cavalry pegasus', ['Pegasus']).
card_colors('cavalry pegasus', ['W']).
card_text('cavalry pegasus', 'Flying\nWhenever Cavalry Pegasus attacks, each attacking Human gains flying until end of turn.').
card_mana_cost('cavalry pegasus', ['1', 'W']).
card_cmc('cavalry pegasus', 2).
card_layout('cavalry pegasus', 'normal').
card_power('cavalry pegasus', 1).
card_toughness('cavalry pegasus', 1).

% Found in: 4ED, 5ED, DRK
card_name('cave people', 'Cave People').
card_type('cave people', 'Creature — Human').
card_types('cave people', ['Creature']).
card_subtypes('cave people', ['Human']).
card_colors('cave people', ['R']).
card_text('cave people', 'Whenever Cave People attacks, it gets +1/-2 until end of turn.\n{1}{R}{R}, {T}: Target creature gains mountainwalk until end of turn. (It can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('cave people', ['1', 'R', 'R']).
card_cmc('cave people', 3).
card_layout('cave people', 'normal').
card_power('cave people', 1).
card_toughness('cave people', 4).

% Found in: MMQ
card_name('cave sense', 'Cave Sense').
card_type('cave sense', 'Enchantment — Aura').
card_types('cave sense', ['Enchantment']).
card_subtypes('cave sense', ['Aura']).
card_colors('cave sense', ['R']).
card_text('cave sense', 'Enchant creature\nEnchanted creature gets +1/+1 and has mountainwalk. (It can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('cave sense', ['1', 'R']).
card_cmc('cave sense', 2).
card_layout('cave sense', 'normal').

% Found in: USG
card_name('cave tiger', 'Cave Tiger').
card_type('cave tiger', 'Creature — Cat').
card_types('cave tiger', ['Creature']).
card_subtypes('cave tiger', ['Cat']).
card_colors('cave tiger', ['G']).
card_text('cave tiger', 'Whenever Cave Tiger becomes blocked by a creature, Cave Tiger gets +1/+1 until end of turn.').
card_mana_cost('cave tiger', ['2', 'G']).
card_cmc('cave tiger', 3).
card_layout('cave tiger', 'normal').
card_power('cave tiger', 2).
card_toughness('cave tiger', 2).

% Found in: MMQ
card_name('cave-in', 'Cave-In').
card_type('cave-in', 'Sorcery').
card_types('cave-in', ['Sorcery']).
card_subtypes('cave-in', []).
card_colors('cave-in', ['R']).
card_text('cave-in', 'You may exile a red card from your hand rather than pay Cave-In\'s mana cost.\nCave-In deals 2 damage to each creature and each player.').
card_mana_cost('cave-in', ['3', 'R', 'R']).
card_cmc('cave-in', 5).
card_layout('cave-in', 'normal').

% Found in: MMQ
card_name('cavern crawler', 'Cavern Crawler').
card_type('cavern crawler', 'Creature — Insect').
card_types('cavern crawler', ['Creature']).
card_subtypes('cavern crawler', ['Insect']).
card_colors('cavern crawler', ['R']).
card_text('cavern crawler', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)\n{R}: Cavern Crawler gets +1/-1 until end of turn.').
card_mana_cost('cavern crawler', ['2', 'R']).
card_cmc('cavern crawler', 3).
card_layout('cavern crawler', 'normal').
card_power('cavern crawler', 0).
card_toughness('cavern crawler', 3).

% Found in: PLS
card_name('cavern harpy', 'Cavern Harpy').
card_type('cavern harpy', 'Creature — Harpy Beast').
card_types('cavern harpy', ['Creature']).
card_subtypes('cavern harpy', ['Harpy', 'Beast']).
card_colors('cavern harpy', ['U', 'B']).
card_text('cavern harpy', 'Flying\nWhen Cavern Harpy enters the battlefield, return a blue or black creature you control to its owner\'s hand.\nPay 1 life: Return Cavern Harpy to its owner\'s hand.').
card_mana_cost('cavern harpy', ['U', 'B']).
card_cmc('cavern harpy', 2).
card_layout('cavern harpy', 'normal').
card_power('cavern harpy', 2).
card_toughness('cavern harpy', 1).

% Found in: THS
card_name('cavern lampad', 'Cavern Lampad').
card_type('cavern lampad', 'Enchantment Creature — Nymph').
card_types('cavern lampad', ['Enchantment', 'Creature']).
card_subtypes('cavern lampad', ['Nymph']).
card_colors('cavern lampad', ['B']).
card_text('cavern lampad', 'Bestow {5}{B} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nIntimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nEnchanted creature gets +2/+2 and has intimidate.').
card_mana_cost('cavern lampad', ['3', 'B']).
card_cmc('cavern lampad', 4).
card_layout('cavern lampad', 'normal').
card_power('cavern lampad', 2).
card_toughness('cavern lampad', 2).

% Found in: AVR
card_name('cavern of souls', 'Cavern of Souls').
card_type('cavern of souls', 'Land').
card_types('cavern of souls', ['Land']).
card_subtypes('cavern of souls', []).
card_colors('cavern of souls', []).
card_text('cavern of souls', 'As Cavern of Souls enters the battlefield, choose a creature type.\n{T}: Add {1} to your mana pool.\n{T}: Add one mana of any color to your mana pool. Spend this mana only to cast a creature spell of the chosen type, and that spell can\'t be countered.').
card_layout('cavern of souls', 'normal').

% Found in: ALA
card_name('cavern thoctar', 'Cavern Thoctar').
card_type('cavern thoctar', 'Creature — Beast').
card_types('cavern thoctar', ['Creature']).
card_subtypes('cavern thoctar', ['Beast']).
card_colors('cavern thoctar', ['G']).
card_text('cavern thoctar', '{1}{R}: Cavern Thoctar gets +1/+0 until end of turn.').
card_mana_cost('cavern thoctar', ['5', 'G']).
card_cmc('cavern thoctar', 6).
card_layout('cavern thoctar', 'normal').
card_power('cavern thoctar', 5).
card_toughness('cavern thoctar', 5).

% Found in: LEG
card_name('caverns of despair', 'Caverns of Despair').
card_type('caverns of despair', 'World Enchantment').
card_types('caverns of despair', ['Enchantment']).
card_subtypes('caverns of despair', []).
card_supertypes('caverns of despair', ['World']).
card_colors('caverns of despair', ['R']).
card_text('caverns of despair', 'No more than two creatures can attack each combat.\nNo more than two creatures can block each combat.').
card_mana_cost('caverns of despair', ['2', 'R', 'R']).
card_cmc('caverns of despair', 4).
card_layout('caverns of despair', 'normal').
card_reserved('caverns of despair').

% Found in: 10E, 9ED, APC, M15, MD1, ORI
card_name('caves of koilos', 'Caves of Koilos').
card_type('caves of koilos', 'Land').
card_types('caves of koilos', ['Land']).
card_subtypes('caves of koilos', []).
card_colors('caves of koilos', []).
card_text('caves of koilos', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {B} to your mana pool. Caves of Koilos deals 1 damage to you.').
card_layout('caves of koilos', 'normal').

% Found in: ODY
card_name('cease-fire', 'Cease-Fire').
card_type('cease-fire', 'Instant').
card_types('cease-fire', ['Instant']).
card_subtypes('cease-fire', []).
card_colors('cease-fire', ['W']).
card_text('cease-fire', 'Target player can\'t cast creature spells this turn.\nDraw a card.').
card_mana_cost('cease-fire', ['2', 'W']).
card_cmc('cease-fire', 3).
card_layout('cease-fire', 'normal').

% Found in: LRW
card_name('ceaseless searblades', 'Ceaseless Searblades').
card_type('ceaseless searblades', 'Creature — Elemental Warrior').
card_types('ceaseless searblades', ['Creature']).
card_subtypes('ceaseless searblades', ['Elemental', 'Warrior']).
card_colors('ceaseless searblades', ['R']).
card_text('ceaseless searblades', 'Whenever you activate an ability of an Elemental, Ceaseless Searblades gets +1/+0 until end of turn.').
card_mana_cost('ceaseless searblades', ['3', 'R']).
card_cmc('ceaseless searblades', 4).
card_layout('ceaseless searblades', 'normal').
card_power('ceaseless searblades', 2).
card_toughness('ceaseless searblades', 4).

% Found in: DIS, PC2
card_name('celestial ancient', 'Celestial Ancient').
card_type('celestial ancient', 'Creature — Elemental').
card_types('celestial ancient', ['Creature']).
card_subtypes('celestial ancient', ['Elemental']).
card_colors('celestial ancient', ['W']).
card_text('celestial ancient', 'Flying\nWhenever you cast an enchantment spell, put a +1/+1 counter on each creature you control.').
card_mana_cost('celestial ancient', ['3', 'W', 'W']).
card_cmc('celestial ancient', 5).
card_layout('celestial ancient', 'normal').
card_power('celestial ancient', 3).
card_toughness('celestial ancient', 3).

% Found in: THS, pPRE
card_name('celestial archon', 'Celestial Archon').
card_type('celestial archon', 'Enchantment Creature — Archon').
card_types('celestial archon', ['Enchantment', 'Creature']).
card_subtypes('celestial archon', ['Archon']).
card_colors('celestial archon', ['W']).
card_text('celestial archon', 'Bestow {5}{W}{W} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying, first strike\nEnchanted creature gets +4/+4 and has flying and first strike.').
card_mana_cost('celestial archon', ['3', 'W', 'W']).
card_cmc('celestial archon', 5).
card_layout('celestial archon', 'normal').
card_power('celestial archon', 4).
card_toughness('celestial archon', 4).

% Found in: WWK, pMEI
card_name('celestial colonnade', 'Celestial Colonnade').
card_type('celestial colonnade', 'Land').
card_types('celestial colonnade', ['Land']).
card_subtypes('celestial colonnade', []).
card_colors('celestial colonnade', []).
card_text('celestial colonnade', 'Celestial Colonnade enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.\n{3}{W}{U}: Until end of turn, Celestial Colonnade becomes a 4/4 white and blue Elemental creature with flying and vigilance. It\'s still a land.').
card_layout('celestial colonnade', 'normal').

% Found in: PCY
card_name('celestial convergence', 'Celestial Convergence').
card_type('celestial convergence', 'Enchantment').
card_types('celestial convergence', ['Enchantment']).
card_subtypes('celestial convergence', []).
card_colors('celestial convergence', ['W']).
card_text('celestial convergence', 'Celestial Convergence enters the battlefield with seven omen counters on it.\nAt the beginning of your upkeep, remove an omen counter from Celestial Convergence. If there are no omen counters on Celestial Convergence, the player with the highest life total wins the game. If two or more players are tied for highest life total, the game is a draw.').
card_mana_cost('celestial convergence', ['2', 'W', 'W']).
card_cmc('celestial convergence', 4).
card_layout('celestial convergence', 'normal').

% Found in: C14, DDF, TSP
card_name('celestial crusader', 'Celestial Crusader').
card_type('celestial crusader', 'Creature — Spirit').
card_types('celestial crusader', ['Creature']).
card_subtypes('celestial crusader', ['Spirit']).
card_colors('celestial crusader', ['W']).
card_text('celestial crusader', 'Flash (You may cast this spell any time you could cast an instant.)\nSplit second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nFlying\nOther white creatures get +1/+1.').
card_mana_cost('celestial crusader', ['2', 'W', 'W']).
card_cmc('celestial crusader', 4).
card_layout('celestial crusader', 'normal').
card_power('celestial crusader', 2).
card_toughness('celestial crusader', 2).

% Found in: 6ED, MIR, TSB
card_name('celestial dawn', 'Celestial Dawn').
card_type('celestial dawn', 'Enchantment').
card_types('celestial dawn', ['Enchantment']).
card_subtypes('celestial dawn', []).
card_colors('celestial dawn', ['W']).
card_text('celestial dawn', 'Lands you control are Plains.\nNonland cards you own that aren\'t on the battlefield, spells you control, and nonland permanents you control are white.\nYou may spend white mana as though it were mana of any color. You may spend other mana only as though it were colorless mana.').
card_mana_cost('celestial dawn', ['1', 'W', 'W']).
card_cmc('celestial dawn', 3).
card_layout('celestial dawn', 'normal').

% Found in: DDO, M14, ORI
card_name('celestial flare', 'Celestial Flare').
card_type('celestial flare', 'Instant').
card_types('celestial flare', ['Instant']).
card_subtypes('celestial flare', []).
card_colors('celestial flare', ['W']).
card_text('celestial flare', 'Target player sacrifices an attacking or blocking creature.').
card_mana_cost('celestial flare', ['W', 'W']).
card_cmc('celestial flare', 2).
card_layout('celestial flare', 'normal').

% Found in: CMD
card_name('celestial force', 'Celestial Force').
card_type('celestial force', 'Creature — Elemental').
card_types('celestial force', ['Creature']).
card_subtypes('celestial force', ['Elemental']).
card_colors('celestial force', ['W']).
card_text('celestial force', 'At the beginning of each upkeep, you gain 3 life.').
card_mana_cost('celestial force', ['5', 'W', 'W', 'W']).
card_cmc('celestial force', 8).
card_layout('celestial force', 'normal').
card_power('celestial force', 7).
card_toughness('celestial force', 7).

% Found in: LGN
card_name('celestial gatekeeper', 'Celestial Gatekeeper').
card_type('celestial gatekeeper', 'Creature — Bird Cleric').
card_types('celestial gatekeeper', ['Creature']).
card_subtypes('celestial gatekeeper', ['Bird', 'Cleric']).
card_colors('celestial gatekeeper', ['W']).
card_text('celestial gatekeeper', 'Flying\nWhen Celestial Gatekeeper dies, exile it, then return up to two target Bird and/or Cleric permanent cards from your graveyard to the battlefield.').
card_mana_cost('celestial gatekeeper', ['3', 'W', 'W']).
card_cmc('celestial gatekeeper', 5).
card_layout('celestial gatekeeper', 'normal').
card_power('celestial gatekeeper', 2).
card_toughness('celestial gatekeeper', 2).

% Found in: SOK
card_name('celestial kirin', 'Celestial Kirin').
card_type('celestial kirin', 'Legendary Creature — Kirin Spirit').
card_types('celestial kirin', ['Creature']).
card_subtypes('celestial kirin', ['Kirin', 'Spirit']).
card_supertypes('celestial kirin', ['Legendary']).
card_colors('celestial kirin', ['W']).
card_text('celestial kirin', 'Flying\nWhenever you cast a Spirit or Arcane spell, destroy all permanents with that spell\'s converted mana cost.').
card_mana_cost('celestial kirin', ['2', 'W', 'W']).
card_cmc('celestial kirin', 4).
card_layout('celestial kirin', 'normal').
card_power('celestial kirin', 3).
card_toughness('celestial kirin', 3).

% Found in: ZEN
card_name('celestial mantle', 'Celestial Mantle').
card_type('celestial mantle', 'Enchantment — Aura').
card_types('celestial mantle', ['Enchantment']).
card_subtypes('celestial mantle', ['Aura']).
card_colors('celestial mantle', ['W']).
card_text('celestial mantle', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhenever enchanted creature deals combat damage to a player, double its controller\'s life total.').
card_mana_cost('celestial mantle', ['3', 'W', 'W', 'W']).
card_cmc('celestial mantle', 6).
card_layout('celestial mantle', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('celestial prism', 'Celestial Prism').
card_type('celestial prism', 'Artifact').
card_types('celestial prism', ['Artifact']).
card_subtypes('celestial prism', []).
card_colors('celestial prism', []).
card_text('celestial prism', '{2}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('celestial prism', ['3']).
card_cmc('celestial prism', 3).
card_layout('celestial prism', 'normal').

% Found in: CON, M10, M11, M12, MM2, pMPR
card_name('celestial purge', 'Celestial Purge').
card_type('celestial purge', 'Instant').
card_types('celestial purge', ['Instant']).
card_subtypes('celestial purge', []).
card_colors('celestial purge', ['W']).
card_text('celestial purge', 'Exile target black or red permanent.').
card_mana_cost('celestial purge', ['1', 'W']).
card_cmc('celestial purge', 2).
card_layout('celestial purge', 'normal').

% Found in: ICE, ME4
card_name('celestial sword', 'Celestial Sword').
card_type('celestial sword', 'Artifact').
card_types('celestial sword', ['Artifact']).
card_subtypes('celestial sword', []).
card_colors('celestial sword', []).
card_text('celestial sword', '{3}, {T}: Target creature you control gets +3/+3 until end of turn. Its controller sacrifices it at the beginning of the next end step.').
card_mana_cost('celestial sword', ['6']).
card_cmc('celestial sword', 6).
card_layout('celestial sword', 'normal').

% Found in: ISD
card_name('cellar door', 'Cellar Door').
card_type('cellar door', 'Artifact').
card_types('cellar door', ['Artifact']).
card_subtypes('cellar door', []).
card_colors('cellar door', []).
card_text('cellar door', '{3}, {T}: Target player puts the bottom card of his or her library into his or her graveyard. If it\'s a creature card, you put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('cellar door', ['2']).
card_cmc('cellar door', 2).
card_layout('cellar door', 'normal').

% Found in: HML
card_name('cemetery gate', 'Cemetery Gate').
card_type('cemetery gate', 'Creature — Wall').
card_types('cemetery gate', ['Creature']).
card_subtypes('cemetery gate', ['Wall']).
card_colors('cemetery gate', ['B']).
card_text('cemetery gate', 'Defender (This creature can\'t attack.)\nProtection from black').
card_mana_cost('cemetery gate', ['2', 'B']).
card_cmc('cemetery gate', 3).
card_layout('cemetery gate', 'normal').
card_power('cemetery gate', 0).
card_toughness('cemetery gate', 5).

% Found in: SHM
card_name('cemetery puca', 'Cemetery Puca').
card_type('cemetery puca', 'Creature — Shapeshifter').
card_types('cemetery puca', ['Creature']).
card_subtypes('cemetery puca', ['Shapeshifter']).
card_colors('cemetery puca', ['U', 'B']).
card_text('cemetery puca', 'Whenever a creature dies, you may pay {1}. If you do, Cemetery Puca becomes a copy of that creature and gains this ability.').
card_mana_cost('cemetery puca', ['1', 'U/B', 'U/B']).
card_cmc('cemetery puca', 3).
card_layout('cemetery puca', 'normal').
card_power('cemetery puca', 1).
card_toughness('cemetery puca', 2).

% Found in: ARC, M10, M12
card_name('cemetery reaper', 'Cemetery Reaper').
card_type('cemetery reaper', 'Creature — Zombie').
card_types('cemetery reaper', ['Creature']).
card_subtypes('cemetery reaper', ['Zombie']).
card_colors('cemetery reaper', ['B']).
card_text('cemetery reaper', 'Other Zombie creatures you control get +1/+1.\n{2}{B}, {T}: Exile target creature card from a graveyard. Put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('cemetery reaper', ['1', 'B', 'B']).
card_cmc('cemetery reaper', 3).
card_layout('cemetery reaper', 'normal').
card_power('cemetery reaper', 2).
card_toughness('cemetery reaper', 2).

% Found in: EVE, MMA
card_name('cenn\'s enlistment', 'Cenn\'s Enlistment').
card_type('cenn\'s enlistment', 'Sorcery').
card_types('cenn\'s enlistment', ['Sorcery']).
card_subtypes('cenn\'s enlistment', []).
card_colors('cenn\'s enlistment', ['W']).
card_text('cenn\'s enlistment', 'Put two 1/1 white Kithkin Soldier creature tokens onto the battlefield.\nRetrace (You may cast this card from your graveyard by discarding a land card in addition to paying its other costs.)').
card_mana_cost('cenn\'s enlistment', ['3', 'W']).
card_cmc('cenn\'s enlistment', 4).
card_layout('cenn\'s enlistment', 'normal').

% Found in: LRW
card_name('cenn\'s heir', 'Cenn\'s Heir').
card_type('cenn\'s heir', 'Creature — Kithkin Soldier').
card_types('cenn\'s heir', ['Creature']).
card_subtypes('cenn\'s heir', ['Kithkin', 'Soldier']).
card_colors('cenn\'s heir', ['W']).
card_text('cenn\'s heir', 'Whenever Cenn\'s Heir attacks, it gets +1/+1 until end of turn for each other attacking Kithkin.').
card_mana_cost('cenn\'s heir', ['1', 'W']).
card_cmc('cenn\'s heir', 2).
card_layout('cenn\'s heir', 'normal').
card_power('cenn\'s heir', 1).
card_toughness('cenn\'s heir', 1).

% Found in: MOR, pGTW
card_name('cenn\'s tactician', 'Cenn\'s Tactician').
card_type('cenn\'s tactician', 'Creature — Kithkin Soldier').
card_types('cenn\'s tactician', ['Creature']).
card_subtypes('cenn\'s tactician', ['Kithkin', 'Soldier']).
card_colors('cenn\'s tactician', ['W']).
card_text('cenn\'s tactician', '{W}, {T}: Put a +1/+1 counter on target Soldier creature.\nEach creature you control with a +1/+1 counter on it can block an additional creature.').
card_mana_cost('cenn\'s tactician', ['W']).
card_cmc('cenn\'s tactician', 1).
card_layout('cenn\'s tactician', 'normal').
card_power('cenn\'s tactician', 1).
card_toughness('cenn\'s tactician', 1).

% Found in: UGL
card_name('censorship', 'Censorship').
card_type('censorship', 'Enchantment').
card_types('censorship', ['Enchantment']).
card_subtypes('censorship', []).
card_colors('censorship', ['U']).
card_text('censorship', 'When Censorship comes into play, choose a CENSORED word.\nWhenever any CENSORED player says the chosen CENSORED word, Censorship deals 2 CENSORED damage to him or her.').
card_mana_cost('censorship', ['U']).
card_cmc('censorship', 1).
card_layout('censorship', 'normal').

% Found in: ICE, MED
card_name('centaur archer', 'Centaur Archer').
card_type('centaur archer', 'Creature — Centaur Archer').
card_types('centaur archer', ['Creature']).
card_subtypes('centaur archer', ['Centaur', 'Archer']).
card_colors('centaur archer', ['R', 'G']).
card_text('centaur archer', '{T}: Centaur Archer deals 1 damage to target creature with flying.').
card_mana_cost('centaur archer', ['1', 'R', 'G']).
card_cmc('centaur archer', 3).
card_layout('centaur archer', 'normal').
card_power('centaur archer', 3).
card_toughness('centaur archer', 2).

% Found in: THS
card_name('centaur battlemaster', 'Centaur Battlemaster').
card_type('centaur battlemaster', 'Creature — Centaur Warrior').
card_types('centaur battlemaster', ['Creature']).
card_subtypes('centaur battlemaster', ['Centaur', 'Warrior']).
card_colors('centaur battlemaster', ['G']).
card_text('centaur battlemaster', 'Heroic — Whenever you cast a spell that targets Centaur Battlemaster, put three +1/+1 counters on Centaur Battlemaster.').
card_mana_cost('centaur battlemaster', ['3', 'G', 'G']).
card_cmc('centaur battlemaster', 5).
card_layout('centaur battlemaster', 'normal').
card_power('centaur battlemaster', 3).
card_toughness('centaur battlemaster', 3).

% Found in: TOR
card_name('centaur chieftain', 'Centaur Chieftain').
card_type('centaur chieftain', 'Creature — Centaur').
card_types('centaur chieftain', ['Creature']).
card_subtypes('centaur chieftain', ['Centaur']).
card_colors('centaur chieftain', ['G']).
card_text('centaur chieftain', 'Haste\nThreshold — As long as seven or more cards are in your graveyard, Centaur Chieftain has \"When Centaur Chieftain enters the battlefield, creatures you control get +1/+1 and gain trample until end of turn.\"').
card_mana_cost('centaur chieftain', ['3', 'G']).
card_cmc('centaur chieftain', 4).
card_layout('centaur chieftain', 'normal').
card_power('centaur chieftain', 3).
card_toughness('centaur chieftain', 3).

% Found in: M10, M13, M15
card_name('centaur courser', 'Centaur Courser').
card_type('centaur courser', 'Creature — Centaur Warrior').
card_types('centaur courser', ['Creature']).
card_subtypes('centaur courser', ['Centaur', 'Warrior']).
card_colors('centaur courser', ['G']).
card_text('centaur courser', '').
card_mana_cost('centaur courser', ['2', 'G']).
card_cmc('centaur courser', 3).
card_layout('centaur courser', 'normal').
card_power('centaur courser', 3).
card_toughness('centaur courser', 3).

% Found in: ODY
card_name('centaur garden', 'Centaur Garden').
card_type('centaur garden', 'Land').
card_types('centaur garden', ['Land']).
card_subtypes('centaur garden', []).
card_colors('centaur garden', []).
card_text('centaur garden', '{T}: Add {G} to your mana pool. Centaur Garden deals 1 damage to you.\nThreshold — {G}, {T}, Sacrifice Centaur Garden: Target creature gets +3/+3 until end of turn. Activate this ability only if seven or more cards are in your graveyard.').
card_layout('centaur garden', 'normal').

% Found in: ONS
card_name('centaur glade', 'Centaur Glade').
card_type('centaur glade', 'Enchantment').
card_types('centaur glade', ['Enchantment']).
card_subtypes('centaur glade', []).
card_colors('centaur glade', ['G']).
card_text('centaur glade', '{2}{G}{G}: Put a 3/3 green Centaur creature token onto the battlefield.').
card_mana_cost('centaur glade', ['3', 'G', 'G']).
card_cmc('centaur glade', 5).
card_layout('centaur glade', 'normal').

% Found in: RTR
card_name('centaur healer', 'Centaur Healer').
card_type('centaur healer', 'Creature — Centaur Cleric').
card_types('centaur healer', ['Creature']).
card_subtypes('centaur healer', ['Centaur', 'Cleric']).
card_colors('centaur healer', ['W', 'G']).
card_text('centaur healer', 'When Centaur Healer enters the battlefield, you gain 3 life.').
card_mana_cost('centaur healer', ['1', 'G', 'W']).
card_cmc('centaur healer', 3).
card_layout('centaur healer', 'normal').
card_power('centaur healer', 3).
card_toughness('centaur healer', 3).

% Found in: FUT
card_name('centaur omenreader', 'Centaur Omenreader').
card_type('centaur omenreader', 'Snow Creature — Centaur Shaman').
card_types('centaur omenreader', ['Creature']).
card_subtypes('centaur omenreader', ['Centaur', 'Shaman']).
card_supertypes('centaur omenreader', ['Snow']).
card_colors('centaur omenreader', ['G']).
card_text('centaur omenreader', 'As long as Centaur Omenreader is tapped, creature spells you cast cost {2} less to cast.').
card_mana_cost('centaur omenreader', ['3', 'G']).
card_cmc('centaur omenreader', 4).
card_layout('centaur omenreader', 'normal').
card_power('centaur omenreader', 3).
card_toughness('centaur omenreader', 3).

% Found in: JUD
card_name('centaur rootcaster', 'Centaur Rootcaster').
card_type('centaur rootcaster', 'Creature — Centaur Druid').
card_types('centaur rootcaster', ['Creature']).
card_subtypes('centaur rootcaster', ['Centaur', 'Druid']).
card_colors('centaur rootcaster', ['G']).
card_text('centaur rootcaster', 'Whenever Centaur Rootcaster deals combat damage to a player, you may search your library for a basic land card and put that card onto the battlefield tapped. If you do, shuffle your library.').
card_mana_cost('centaur rootcaster', ['3', 'G']).
card_cmc('centaur rootcaster', 4).
card_layout('centaur rootcaster', 'normal').
card_power('centaur rootcaster', 2).
card_toughness('centaur rootcaster', 2).

% Found in: RAV
card_name('centaur safeguard', 'Centaur Safeguard').
card_type('centaur safeguard', 'Creature — Centaur Warrior').
card_types('centaur safeguard', ['Creature']).
card_subtypes('centaur safeguard', ['Centaur', 'Warrior']).
card_colors('centaur safeguard', ['W', 'G']).
card_text('centaur safeguard', '({G/W} can be paid with either {G} or {W}.)\nWhen Centaur Safeguard dies, you may gain 3 life.').
card_mana_cost('centaur safeguard', ['2', 'G/W']).
card_cmc('centaur safeguard', 3).
card_layout('centaur safeguard', 'normal').
card_power('centaur safeguard', 3).
card_toughness('centaur safeguard', 1).

% Found in: TOR
card_name('centaur veteran', 'Centaur Veteran').
card_type('centaur veteran', 'Creature — Centaur').
card_types('centaur veteran', ['Creature']).
card_subtypes('centaur veteran', ['Centaur']).
card_colors('centaur veteran', ['G']).
card_text('centaur veteran', 'Trample\n{G}, Discard a card: Regenerate Centaur Veteran.').
card_mana_cost('centaur veteran', ['5', 'G']).
card_cmc('centaur veteran', 6).
card_layout('centaur veteran', 'normal').
card_power('centaur veteran', 3).
card_toughness('centaur veteran', 3).

% Found in: RTR
card_name('centaur\'s herald', 'Centaur\'s Herald').
card_type('centaur\'s herald', 'Creature — Elf Scout').
card_types('centaur\'s herald', ['Creature']).
card_subtypes('centaur\'s herald', ['Elf', 'Scout']).
card_colors('centaur\'s herald', ['G']).
card_text('centaur\'s herald', '{2}{G}, Sacrifice Centaur\'s Herald: Put a 3/3 green Centaur creature token onto the battlefield.').
card_mana_cost('centaur\'s herald', ['G']).
card_cmc('centaur\'s herald', 1).
card_layout('centaur\'s herald', 'normal').
card_power('centaur\'s herald', 0).
card_toughness('centaur\'s herald', 1).

% Found in: DTK
card_name('center soul', 'Center Soul').
card_type('center soul', 'Instant').
card_types('center soul', ['Instant']).
card_subtypes('center soul', []).
card_colors('center soul', ['W']).
card_text('center soul', 'Target creature you control gains protection from the color of your choice until end of turn.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('center soul', ['1', 'W']).
card_cmc('center soul', 2).
card_layout('center soul', 'normal').

% Found in: TOR
card_name('cephalid aristocrat', 'Cephalid Aristocrat').
card_type('cephalid aristocrat', 'Creature — Cephalid').
card_types('cephalid aristocrat', ['Creature']).
card_subtypes('cephalid aristocrat', ['Cephalid']).
card_colors('cephalid aristocrat', ['U']).
card_text('cephalid aristocrat', 'Whenever Cephalid Aristocrat becomes the target of a spell or ability, put the top two cards of your library into your graveyard.').
card_mana_cost('cephalid aristocrat', ['4', 'U']).
card_cmc('cephalid aristocrat', 5).
card_layout('cephalid aristocrat', 'normal').
card_power('cephalid aristocrat', 3).
card_toughness('cephalid aristocrat', 3).

% Found in: ODY
card_name('cephalid broker', 'Cephalid Broker').
card_type('cephalid broker', 'Creature — Cephalid').
card_types('cephalid broker', ['Creature']).
card_subtypes('cephalid broker', ['Cephalid']).
card_colors('cephalid broker', ['U']).
card_text('cephalid broker', '{T}: Target player draws two cards, then discards two cards.').
card_mana_cost('cephalid broker', ['3', 'U']).
card_cmc('cephalid broker', 4).
card_layout('cephalid broker', 'normal').
card_power('cephalid broker', 2).
card_toughness('cephalid broker', 2).

% Found in: ODY, V12
card_name('cephalid coliseum', 'Cephalid Coliseum').
card_type('cephalid coliseum', 'Land').
card_types('cephalid coliseum', ['Land']).
card_subtypes('cephalid coliseum', []).
card_colors('cephalid coliseum', []).
card_text('cephalid coliseum', '{T}: Add {U} to your mana pool. Cephalid Coliseum deals 1 damage to you.\nThreshold — {U}, {T}, Sacrifice Cephalid Coliseum: Target player draws three cards, then discards three cards. Activate this ability only if seven or more cards are in your graveyard.').
card_layout('cephalid coliseum', 'normal').

% Found in: 10E, JUD
card_name('cephalid constable', 'Cephalid Constable').
card_type('cephalid constable', 'Creature — Cephalid Wizard').
card_types('cephalid constable', ['Creature']).
card_subtypes('cephalid constable', ['Cephalid', 'Wizard']).
card_colors('cephalid constable', ['U']).
card_text('cephalid constable', 'Whenever Cephalid Constable deals combat damage to a player, return up to that many target permanents that player controls to their owners\' hands.').
card_mana_cost('cephalid constable', ['1', 'U', 'U']).
card_cmc('cephalid constable', 3).
card_layout('cephalid constable', 'normal').
card_power('cephalid constable', 1).
card_toughness('cephalid constable', 1).

% Found in: TOR
card_name('cephalid illusionist', 'Cephalid Illusionist').
card_type('cephalid illusionist', 'Creature — Cephalid Wizard').
card_types('cephalid illusionist', ['Creature']).
card_subtypes('cephalid illusionist', ['Cephalid', 'Wizard']).
card_colors('cephalid illusionist', ['U']).
card_text('cephalid illusionist', 'Whenever Cephalid Illusionist becomes the target of a spell or ability, put the top three cards of your library into your graveyard.\n{2}{U}, {T}: Prevent all combat damage that would be dealt to and dealt by target creature you control this turn.').
card_mana_cost('cephalid illusionist', ['1', 'U']).
card_cmc('cephalid illusionist', 2).
card_layout('cephalid illusionist', 'normal').
card_power('cephalid illusionist', 1).
card_toughness('cephalid illusionist', 1).

% Found in: JUD
card_name('cephalid inkshrouder', 'Cephalid Inkshrouder').
card_type('cephalid inkshrouder', 'Creature — Cephalid').
card_types('cephalid inkshrouder', ['Creature']).
card_subtypes('cephalid inkshrouder', ['Cephalid']).
card_colors('cephalid inkshrouder', ['U']).
card_text('cephalid inkshrouder', 'Discard a card: Cephalid Inkshrouder gains shroud until end of turn and can\'t be blocked this turn. (A creature with shroud can\'t be the target of spells or abilities.)').
card_mana_cost('cephalid inkshrouder', ['2', 'U']).
card_cmc('cephalid inkshrouder', 3).
card_layout('cephalid inkshrouder', 'normal').
card_power('cephalid inkshrouder', 2).
card_toughness('cephalid inkshrouder', 1).

% Found in: ODY
card_name('cephalid looter', 'Cephalid Looter').
card_type('cephalid looter', 'Creature — Cephalid Rogue').
card_types('cephalid looter', ['Creature']).
card_subtypes('cephalid looter', ['Cephalid', 'Rogue']).
card_colors('cephalid looter', ['U']).
card_text('cephalid looter', '{T}: Target player draws a card, then discards a card.').
card_mana_cost('cephalid looter', ['2', 'U']).
card_cmc('cephalid looter', 3).
card_layout('cephalid looter', 'normal').
card_power('cephalid looter', 2).
card_toughness('cephalid looter', 1).

% Found in: LGN
card_name('cephalid pathmage', 'Cephalid Pathmage').
card_type('cephalid pathmage', 'Creature — Cephalid Wizard').
card_types('cephalid pathmage', ['Creature']).
card_subtypes('cephalid pathmage', ['Cephalid', 'Wizard']).
card_colors('cephalid pathmage', ['U']).
card_text('cephalid pathmage', 'Cephalid Pathmage can\'t be blocked.\n{T}, Sacrifice Cephalid Pathmage: Target creature can\'t be blocked this turn.').
card_mana_cost('cephalid pathmage', ['2', 'U']).
card_cmc('cephalid pathmage', 3).
card_layout('cephalid pathmage', 'normal').
card_power('cephalid pathmage', 1).
card_toughness('cephalid pathmage', 2).

% Found in: ODY
card_name('cephalid retainer', 'Cephalid Retainer').
card_type('cephalid retainer', 'Creature — Cephalid').
card_types('cephalid retainer', ['Creature']).
card_subtypes('cephalid retainer', ['Cephalid']).
card_colors('cephalid retainer', ['U']).
card_text('cephalid retainer', '{U}{U}: Tap target creature without flying.').
card_mana_cost('cephalid retainer', ['2', 'U', 'U']).
card_cmc('cephalid retainer', 4).
card_layout('cephalid retainer', 'normal').
card_power('cephalid retainer', 2).
card_toughness('cephalid retainer', 3).

% Found in: TOR
card_name('cephalid sage', 'Cephalid Sage').
card_type('cephalid sage', 'Creature — Cephalid').
card_types('cephalid sage', ['Creature']).
card_subtypes('cephalid sage', ['Cephalid']).
card_colors('cephalid sage', ['U']).
card_text('cephalid sage', 'Threshold — As long as seven or more cards are in your graveyard, Cephalid Sage has \"When Cephalid Sage enters the battlefield, draw three cards, then discard two cards.\"').
card_mana_cost('cephalid sage', ['3', 'U']).
card_cmc('cephalid sage', 4).
card_layout('cephalid sage', 'normal').
card_power('cephalid sage', 2).
card_toughness('cephalid sage', 3).

% Found in: ODY
card_name('cephalid scout', 'Cephalid Scout').
card_type('cephalid scout', 'Creature — Cephalid Wizard Scout').
card_types('cephalid scout', ['Creature']).
card_subtypes('cephalid scout', ['Cephalid', 'Wizard', 'Scout']).
card_colors('cephalid scout', ['U']).
card_text('cephalid scout', 'Flying\n{2}{U}, Sacrifice a land: Draw a card.').
card_mana_cost('cephalid scout', ['1', 'U']).
card_cmc('cephalid scout', 2).
card_layout('cephalid scout', 'normal').
card_power('cephalid scout', 1).
card_toughness('cephalid scout', 1).

% Found in: ODY
card_name('cephalid shrine', 'Cephalid Shrine').
card_type('cephalid shrine', 'Enchantment').
card_types('cephalid shrine', ['Enchantment']).
card_subtypes('cephalid shrine', []).
card_colors('cephalid shrine', ['U']).
card_text('cephalid shrine', 'Whenever a player casts a spell, counter that spell unless that player pays {X}, where X is the number of cards in all graveyards with the same name as the spell.').
card_mana_cost('cephalid shrine', ['1', 'U', 'U']).
card_cmc('cephalid shrine', 3).
card_layout('cephalid shrine', 'normal').

% Found in: TOR
card_name('cephalid snitch', 'Cephalid Snitch').
card_type('cephalid snitch', 'Creature — Cephalid Wizard').
card_types('cephalid snitch', ['Creature']).
card_subtypes('cephalid snitch', ['Cephalid', 'Wizard']).
card_colors('cephalid snitch', ['U']).
card_text('cephalid snitch', 'Sacrifice Cephalid Snitch: Target creature loses protection from black until end of turn.').
card_mana_cost('cephalid snitch', ['1', 'U']).
card_cmc('cephalid snitch', 2).
card_layout('cephalid snitch', 'normal').
card_power('cephalid snitch', 1).
card_toughness('cephalid snitch', 1).

% Found in: TOR
card_name('cephalid vandal', 'Cephalid Vandal').
card_type('cephalid vandal', 'Creature — Cephalid Rogue').
card_types('cephalid vandal', ['Creature']).
card_subtypes('cephalid vandal', ['Cephalid', 'Rogue']).
card_colors('cephalid vandal', ['U']).
card_text('cephalid vandal', 'At the beginning of your upkeep, put a shred counter on Cephalid Vandal. Then put the top card of your library into your graveyard for each shred counter on Cephalid Vandal.').
card_mana_cost('cephalid vandal', ['1', 'U']).
card_cmc('cephalid vandal', 2).
card_layout('cephalid vandal', 'normal').
card_power('cephalid vandal', 1).
card_toughness('cephalid vandal', 1).

% Found in: SOM
card_name('cerebral eruption', 'Cerebral Eruption').
card_type('cerebral eruption', 'Sorcery').
card_types('cerebral eruption', ['Sorcery']).
card_subtypes('cerebral eruption', []).
card_colors('cerebral eruption', ['R']).
card_text('cerebral eruption', 'Target opponent reveals the top card of his or her library. Cerebral Eruption deals damage equal to the revealed card\'s converted mana cost to that player and each creature he or she controls. If a land card is revealed this way, return Cerebral Eruption to its owner\'s hand.').
card_mana_cost('cerebral eruption', ['2', 'R', 'R']).
card_cmc('cerebral eruption', 4).
card_layout('cerebral eruption', 'normal').

% Found in: GPT
card_name('cerebral vortex', 'Cerebral Vortex').
card_type('cerebral vortex', 'Instant').
card_types('cerebral vortex', ['Instant']).
card_subtypes('cerebral vortex', []).
card_colors('cerebral vortex', ['U', 'R']).
card_text('cerebral vortex', 'Target player draws two cards, then Cerebral Vortex deals damage to that player equal to the number of cards he or she has drawn this turn.').
card_mana_cost('cerebral vortex', ['1', 'U', 'R']).
card_cmc('cerebral vortex', 3).
card_layout('cerebral vortex', 'normal').

% Found in: MMQ
card_name('ceremonial guard', 'Ceremonial Guard').
card_type('ceremonial guard', 'Creature — Human Soldier').
card_types('ceremonial guard', ['Creature']).
card_subtypes('ceremonial guard', ['Human', 'Soldier']).
card_colors('ceremonial guard', ['R']).
card_text('ceremonial guard', 'When Ceremonial Guard attacks or blocks, destroy it at end of combat.').
card_mana_cost('ceremonial guard', ['2', 'R']).
card_cmc('ceremonial guard', 3).
card_layout('ceremonial guard', 'normal').
card_power('ceremonial guard', 3).
card_toughness('ceremonial guard', 4).

% Found in: ARB, HOP
card_name('cerodon yearling', 'Cerodon Yearling').
card_type('cerodon yearling', 'Creature — Beast').
card_types('cerodon yearling', ['Creature']).
card_subtypes('cerodon yearling', ['Beast']).
card_colors('cerodon yearling', ['W', 'R']).
card_text('cerodon yearling', 'Vigilance, haste').
card_mana_cost('cerodon yearling', ['R', 'W']).
card_cmc('cerodon yearling', 2).
card_layout('cerodon yearling', 'normal').
card_power('cerodon yearling', 2).
card_toughness('cerodon yearling', 2).

% Found in: RAV
card_name('cerulean sphinx', 'Cerulean Sphinx').
card_type('cerulean sphinx', 'Creature — Sphinx').
card_types('cerulean sphinx', ['Creature']).
card_subtypes('cerulean sphinx', ['Sphinx']).
card_colors('cerulean sphinx', ['U']).
card_text('cerulean sphinx', 'Flying\n{U}: Cerulean Sphinx\'s owner shuffles it into his or her library.').
card_mana_cost('cerulean sphinx', ['4', 'U', 'U']).
card_cmc('cerulean sphinx', 6).
card_layout('cerulean sphinx', 'normal').
card_power('cerulean sphinx', 5).
card_toughness('cerulean sphinx', 5).

% Found in: SHM
card_name('cerulean wisps', 'Cerulean Wisps').
card_type('cerulean wisps', 'Instant').
card_types('cerulean wisps', ['Instant']).
card_subtypes('cerulean wisps', []).
card_colors('cerulean wisps', ['U']).
card_text('cerulean wisps', 'Target creature becomes blue until end of turn. Untap that creature.\nDraw a card.').
card_mana_cost('cerulean wisps', ['U']).
card_cmc('cerulean wisps', 1).
card_layout('cerulean wisps', 'normal').

% Found in: MIR
card_name('cerulean wyvern', 'Cerulean Wyvern').
card_type('cerulean wyvern', 'Creature — Drake').
card_types('cerulean wyvern', ['Creature']).
card_subtypes('cerulean wyvern', ['Drake']).
card_colors('cerulean wyvern', ['U']).
card_text('cerulean wyvern', 'Flying, protection from green').
card_mana_cost('cerulean wyvern', ['4', 'U']).
card_cmc('cerulean wyvern', 5).
card_layout('cerulean wyvern', 'normal').
card_power('cerulean wyvern', 3).
card_toughness('cerulean wyvern', 3).

% Found in: ULG
card_name('cessation', 'Cessation').
card_type('cessation', 'Enchantment — Aura').
card_types('cessation', ['Enchantment']).
card_subtypes('cessation', ['Aura']).
card_colors('cessation', ['W']).
card_text('cessation', 'Enchant creature\nEnchanted creature can\'t attack.\nWhen Cessation is put into a graveyard from the battlefield, return Cessation to its owner\'s hand.').
card_mana_cost('cessation', ['2', 'W']).
card_cmc('cessation', 3).
card_layout('cessation', 'normal').

% Found in: APC
card_name('ceta disciple', 'Ceta Disciple').
card_type('ceta disciple', 'Creature — Merfolk Wizard').
card_types('ceta disciple', ['Creature']).
card_subtypes('ceta disciple', ['Merfolk', 'Wizard']).
card_colors('ceta disciple', ['U']).
card_text('ceta disciple', '{R}, {T}: Target creature gets +2/+0 until end of turn.\n{G}, {T}: Add one mana of any color to your mana pool.').
card_mana_cost('ceta disciple', ['U']).
card_cmc('ceta disciple', 1).
card_layout('ceta disciple', 'normal').
card_power('ceta disciple', 1).
card_toughness('ceta disciple', 1).

% Found in: APC
card_name('ceta sanctuary', 'Ceta Sanctuary').
card_type('ceta sanctuary', 'Enchantment').
card_types('ceta sanctuary', ['Enchantment']).
card_subtypes('ceta sanctuary', []).
card_colors('ceta sanctuary', ['U']).
card_text('ceta sanctuary', 'At the beginning of your upkeep, if you control a red or green permanent, draw a card, then discard a card. If you control a red permanent and a green permanent, instead draw two cards, then discard a card.').
card_mana_cost('ceta sanctuary', ['2', 'U']).
card_cmc('ceta sanctuary', 3).
card_layout('ceta sanctuary', 'normal').

% Found in: APC
card_name('cetavolver', 'Cetavolver').
card_type('cetavolver', 'Creature — Volver').
card_types('cetavolver', ['Creature']).
card_subtypes('cetavolver', ['Volver']).
card_colors('cetavolver', ['U']).
card_text('cetavolver', 'Kicker {1}{R} and/or {G} (You may pay an additional {1}{R} and/or {G} as you cast this spell.)\nIf Cetavolver was kicked with its {1}{R} kicker, it enters the battlefield with two +1/+1 counters on it and with first strike.\nIf Cetavolver was kicked with its {G} kicker, it enters the battlefield with a +1/+1 counter on it and with trample.').
card_mana_cost('cetavolver', ['1', 'U']).
card_cmc('cetavolver', 2).
card_layout('cetavolver', 'normal').
card_power('cetavolver', 1).
card_toughness('cetavolver', 1).

% Found in: LEG, ME3, PD2, VMA
card_name('chain lightning', 'Chain Lightning').
card_type('chain lightning', 'Sorcery').
card_types('chain lightning', ['Sorcery']).
card_subtypes('chain lightning', []).
card_colors('chain lightning', ['R']).
card_text('chain lightning', 'Chain Lightning deals 3 damage to target creature or player. Then that player or that creature\'s controller may pay {R}{R}. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_mana_cost('chain lightning', ['R']).
card_cmc('chain lightning', 1).
card_layout('chain lightning', 'normal').

% Found in: ONS
card_name('chain of acid', 'Chain of Acid').
card_type('chain of acid', 'Sorcery').
card_types('chain of acid', ['Sorcery']).
card_subtypes('chain of acid', []).
card_colors('chain of acid', ['G']).
card_text('chain of acid', 'Destroy target noncreature permanent. Then that permanent\'s controller may copy this spell and may choose a new target for that copy.').
card_mana_cost('chain of acid', ['3', 'G']).
card_cmc('chain of acid', 4).
card_layout('chain of acid', 'normal').

% Found in: ONS
card_name('chain of plasma', 'Chain of Plasma').
card_type('chain of plasma', 'Instant').
card_types('chain of plasma', ['Instant']).
card_subtypes('chain of plasma', []).
card_colors('chain of plasma', ['R']).
card_text('chain of plasma', 'Chain of Plasma deals 3 damage to target creature or player. Then that player or that creature\'s controller may discard a card. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_mana_cost('chain of plasma', ['1', 'R']).
card_cmc('chain of plasma', 2).
card_layout('chain of plasma', 'normal').

% Found in: ONS
card_name('chain of silence', 'Chain of Silence').
card_type('chain of silence', 'Instant').
card_types('chain of silence', ['Instant']).
card_subtypes('chain of silence', []).
card_colors('chain of silence', ['W']).
card_text('chain of silence', 'Prevent all damage target creature would deal this turn. That creature\'s controller may sacrifice a land. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_mana_cost('chain of silence', ['1', 'W']).
card_cmc('chain of silence', 2).
card_layout('chain of silence', 'normal').

% Found in: ONS
card_name('chain of smog', 'Chain of Smog').
card_type('chain of smog', 'Sorcery').
card_types('chain of smog', ['Sorcery']).
card_subtypes('chain of smog', []).
card_colors('chain of smog', ['B']).
card_text('chain of smog', 'Target player discards two cards. That player may copy this spell and may choose a new target for that copy.').
card_mana_cost('chain of smog', ['1', 'B']).
card_cmc('chain of smog', 2).
card_layout('chain of smog', 'normal').

% Found in: ONS
card_name('chain of vapor', 'Chain of Vapor').
card_type('chain of vapor', 'Instant').
card_types('chain of vapor', ['Instant']).
card_subtypes('chain of vapor', []).
card_colors('chain of vapor', ['U']).
card_text('chain of vapor', 'Return target nonland permanent to its owner\'s hand. Then that permanent\'s controller may sacrifice a land. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_mana_cost('chain of vapor', ['U']).
card_cmc('chain of vapor', 1).
card_layout('chain of vapor', 'normal').

% Found in: CMD, WWK
card_name('chain reaction', 'Chain Reaction').
card_type('chain reaction', 'Sorcery').
card_types('chain reaction', ['Sorcery']).
card_subtypes('chain reaction', []).
card_colors('chain reaction', ['R']).
card_text('chain reaction', 'Chain Reaction deals X damage to each creature, where X is the number of creatures on the battlefield.').
card_mana_cost('chain reaction', ['2', 'R', 'R']).
card_cmc('chain reaction', 4).
card_layout('chain reaction', 'normal').

% Found in: HML
card_name('chain stasis', 'Chain Stasis').
card_type('chain stasis', 'Instant').
card_types('chain stasis', ['Instant']).
card_subtypes('chain stasis', []).
card_colors('chain stasis', ['U']).
card_text('chain stasis', 'You may tap or untap target creature. Then that creature\'s controller may pay {2}{U}. If the player does, he or she may copy this spell and may choose a new target for that copy.').
card_mana_cost('chain stasis', ['U']).
card_cmc('chain stasis', 1).
card_layout('chain stasis', 'normal').
card_reserved('chain stasis').

% Found in: SHM
card_name('chainbreaker', 'Chainbreaker').
card_type('chainbreaker', 'Artifact Creature — Scarecrow').
card_types('chainbreaker', ['Artifact', 'Creature']).
card_subtypes('chainbreaker', ['Scarecrow']).
card_colors('chainbreaker', []).
card_text('chainbreaker', 'Chainbreaker enters the battlefield with two -1/-1 counters on it.\n{3}, {T}: Remove a -1/-1 counter from target creature.').
card_mana_cost('chainbreaker', ['2']).
card_cmc('chainbreaker', 2).
card_layout('chainbreaker', 'normal').
card_power('chainbreaker', 3).
card_toughness('chainbreaker', 3).

% Found in: NPH
card_name('chained throatseeker', 'Chained Throatseeker').
card_type('chained throatseeker', 'Creature — Horror').
card_types('chained throatseeker', ['Creature']).
card_subtypes('chained throatseeker', ['Horror']).
card_colors('chained throatseeker', ['U']).
card_text('chained throatseeker', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nChained Throatseeker can\'t attack unless defending player is poisoned.').
card_mana_cost('chained throatseeker', ['5', 'U']).
card_cmc('chained throatseeker', 6).
card_layout('chained throatseeker', 'normal').
card_power('chained throatseeker', 5).
card_toughness('chained throatseeker', 5).

% Found in: THS
card_name('chained to the rocks', 'Chained to the Rocks').
card_type('chained to the rocks', 'Enchantment — Aura').
card_types('chained to the rocks', ['Enchantment']).
card_subtypes('chained to the rocks', ['Aura']).
card_colors('chained to the rocks', ['W']).
card_text('chained to the rocks', 'Enchant Mountain you control\nWhen Chained to the Rocks enters the battlefield, exile target creature an opponent controls until Chained to the Rocks leaves the battlefield. (That creature returns under its owner\'s control.)').
card_mana_cost('chained to the rocks', ['W']).
card_cmc('chained to the rocks', 1).
card_layout('chained to the rocks', 'normal').

% Found in: TOR, V13, VMA, pFNM
card_name('chainer\'s edict', 'Chainer\'s Edict').
card_type('chainer\'s edict', 'Sorcery').
card_types('chainer\'s edict', ['Sorcery']).
card_subtypes('chainer\'s edict', []).
card_colors('chainer\'s edict', ['B']).
card_text('chainer\'s edict', 'Target player sacrifices a creature.\nFlashback {5}{B}{B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('chainer\'s edict', ['1', 'B']).
card_cmc('chainer\'s edict', 2).
card_layout('chainer\'s edict', 'normal').

% Found in: TOR
card_name('chainer, dementia master', 'Chainer, Dementia Master').
card_type('chainer, dementia master', 'Legendary Creature — Human Minion').
card_types('chainer, dementia master', ['Creature']).
card_subtypes('chainer, dementia master', ['Human', 'Minion']).
card_supertypes('chainer, dementia master', ['Legendary']).
card_colors('chainer, dementia master', ['B']).
card_text('chainer, dementia master', 'Nightmare creatures get +1/+1.\n{B}{B}{B}, Pay 3 life: Put target creature card from a graveyard onto the battlefield under your control. That creature is black and is a Nightmare in addition to its other creature types.\nWhen Chainer, Dementia Master leaves the battlefield, exile all Nightmares.').
card_mana_cost('chainer, dementia master', ['3', 'B', 'B']).
card_cmc('chainer, dementia master', 5).
card_layout('chainer, dementia master', 'normal').
card_power('chainer, dementia master', 3).
card_toughness('chainer, dementia master', 3).

% Found in: ODY
card_name('chainflinger', 'Chainflinger').
card_type('chainflinger', 'Creature — Beast').
card_types('chainflinger', ['Creature']).
card_subtypes('chainflinger', ['Beast']).
card_colors('chainflinger', ['R']).
card_text('chainflinger', '{1}{R}, {T}: Chainflinger deals 1 damage to target creature or player.\nThreshold — {2}{R}, {T}: Chainflinger deals 2 damage to target creature or player. Activate this ability only if seven or more cards are in your graveyard.').
card_mana_cost('chainflinger', ['3', 'R']).
card_cmc('chainflinger', 4).
card_layout('chainflinger', 'normal').
card_power('chainflinger', 2).
card_toughness('chainflinger', 2).

% Found in: LEG, MED
card_name('chains of mephistopheles', 'Chains of Mephistopheles').
card_type('chains of mephistopheles', 'Enchantment').
card_types('chains of mephistopheles', ['Enchantment']).
card_subtypes('chains of mephistopheles', []).
card_colors('chains of mephistopheles', ['B']).
card_text('chains of mephistopheles', 'If a player would draw a card except the first one he or she draws in his or her draw step each turn, that player discards a card instead. If the player discards a card this way, he or she draws a card. If the player doesn\'t discard a card this way, he or she puts the top card of his or her library into his or her graveyard.').
card_mana_cost('chains of mephistopheles', ['1', 'B']).
card_cmc('chains of mephistopheles', 2).
card_layout('chains of mephistopheles', 'normal').
card_reserved('chains of mephistopheles').

% Found in: DKA
card_name('chalice of death', 'Chalice of Death').
card_type('chalice of death', 'Artifact').
card_types('chalice of death', ['Artifact']).
card_subtypes('chalice of death', []).
card_colors('chalice of death', []).
card_text('chalice of death', '{T}: Target player loses 5 life.').
card_layout('chalice of death', 'double-faced').

% Found in: DKA
card_name('chalice of life', 'Chalice of Life').
card_type('chalice of life', 'Artifact').
card_types('chalice of life', ['Artifact']).
card_subtypes('chalice of life', []).
card_colors('chalice of life', []).
card_text('chalice of life', '{T}: You gain 1 life. Then if you have at least 10 life more than your starting life total, transform Chalice of Life.').
card_mana_cost('chalice of life', ['3']).
card_cmc('chalice of life', 3).
card_layout('chalice of life', 'double-faced').
card_sides('chalice of life', 'chalice of death').

% Found in: MMA, MRD
card_name('chalice of the void', 'Chalice of the Void').
card_type('chalice of the void', 'Artifact').
card_types('chalice of the void', ['Artifact']).
card_subtypes('chalice of the void', []).
card_colors('chalice of the void', []).
card_text('chalice of the void', 'Chalice of the Void enters the battlefield with X charge counters on it.\nWhenever a player casts a spell with converted mana cost equal to the number of charge counters on Chalice of the Void, counter that spell.').
card_mana_cost('chalice of the void', ['X', 'X']).
card_cmc('chalice of the void', 0).
card_layout('chalice of the void', 'normal').

% Found in: ODY
card_name('chamber of manipulation', 'Chamber of Manipulation').
card_type('chamber of manipulation', 'Enchantment — Aura').
card_types('chamber of manipulation', ['Enchantment']).
card_subtypes('chamber of manipulation', ['Aura']).
card_colors('chamber of manipulation', ['U']).
card_text('chamber of manipulation', 'Enchant land\nEnchanted land has \"{T}, Discard a card: Gain control of target creature until end of turn.\"').
card_mana_cost('chamber of manipulation', ['2', 'U', 'U']).
card_cmc('chamber of manipulation', 4).
card_layout('chamber of manipulation', 'normal').

% Found in: MMQ
card_name('chambered nautilus', 'Chambered Nautilus').
card_type('chambered nautilus', 'Creature — Nautilus Beast').
card_types('chambered nautilus', ['Creature']).
card_subtypes('chambered nautilus', ['Nautilus', 'Beast']).
card_colors('chambered nautilus', ['U']).
card_text('chambered nautilus', 'Whenever Chambered Nautilus becomes blocked, you may draw a card.').
card_mana_cost('chambered nautilus', ['2', 'U']).
card_cmc('chambered nautilus', 3).
card_layout('chambered nautilus', 'normal').
card_power('chambered nautilus', 2).
card_toughness('chambered nautilus', 2).

% Found in: TSP
card_name('chameleon blur', 'Chameleon Blur').
card_type('chameleon blur', 'Instant').
card_types('chameleon blur', ['Instant']).
card_subtypes('chameleon blur', []).
card_colors('chameleon blur', ['G']).
card_text('chameleon blur', 'Prevent all damage that creatures would deal to players this turn.').
card_mana_cost('chameleon blur', ['3', 'G']).
card_cmc('chameleon blur', 4).
card_layout('chameleon blur', 'normal').

% Found in: ARC, MOR, V13
card_name('chameleon colossus', 'Chameleon Colossus').
card_type('chameleon colossus', 'Creature — Shapeshifter').
card_types('chameleon colossus', ['Creature']).
card_subtypes('chameleon colossus', ['Shapeshifter']).
card_colors('chameleon colossus', ['G']).
card_text('chameleon colossus', 'Changeling (This card is every creature type.)\nProtection from black\n{2}{G}{G}: Chameleon Colossus gets +X/+X until end of turn, where X is its power.').
card_mana_cost('chameleon colossus', ['2', 'G', 'G']).
card_cmc('chameleon colossus', 4).
card_layout('chameleon colossus', 'normal').
card_power('chameleon colossus', 4).
card_toughness('chameleon colossus', 4).

% Found in: MMQ
card_name('chameleon spirit', 'Chameleon Spirit').
card_type('chameleon spirit', 'Creature — Illusion Spirit').
card_types('chameleon spirit', ['Creature']).
card_subtypes('chameleon spirit', ['Illusion', 'Spirit']).
card_colors('chameleon spirit', ['U']).
card_text('chameleon spirit', 'As Chameleon Spirit enters the battlefield, choose a color.\nChameleon Spirit\'s power and toughness are each equal to the number of permanents of the chosen color your opponents control.').
card_mana_cost('chameleon spirit', ['3', 'U']).
card_cmc('chameleon spirit', 4).
card_layout('chameleon spirit', 'normal').
card_power('chameleon spirit', '*').
card_toughness('chameleon spirit', '*').

% Found in: ME4, S99
card_name('champion lancer', 'Champion Lancer').
card_type('champion lancer', 'Creature — Human Knight').
card_types('champion lancer', ['Creature']).
card_subtypes('champion lancer', ['Human', 'Knight']).
card_colors('champion lancer', ['W']).
card_text('champion lancer', 'Prevent all damage that would be dealt to Champion Lancer by creatures.').
card_mana_cost('champion lancer', ['4', 'W', 'W']).
card_cmc('champion lancer', 6).
card_layout('champion lancer', 'normal').
card_power('champion lancer', 3).
card_toughness('champion lancer', 3).

% Found in: DTK
card_name('champion of arashin', 'Champion of Arashin').
card_type('champion of arashin', 'Creature — Hound Warrior').
card_types('champion of arashin', ['Creature']).
card_subtypes('champion of arashin', ['Hound', 'Warrior']).
card_colors('champion of arashin', ['W']).
card_text('champion of arashin', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('champion of arashin', ['3', 'W']).
card_cmc('champion of arashin', 4).
card_layout('champion of arashin', 'normal').
card_power('champion of arashin', 3).
card_toughness('champion of arashin', 2).

% Found in: AVR
card_name('champion of lambholt', 'Champion of Lambholt').
card_type('champion of lambholt', 'Creature — Human Warrior').
card_types('champion of lambholt', ['Creature']).
card_subtypes('champion of lambholt', ['Human', 'Warrior']).
card_colors('champion of lambholt', ['G']).
card_text('champion of lambholt', 'Creatures with power less than Champion of Lambholt\'s power can\'t block creatures you control.\nWhenever another creature enters the battlefield under your control, put a +1/+1 counter on Champion of Lambholt.').
card_mana_cost('champion of lambholt', ['1', 'G', 'G']).
card_cmc('champion of lambholt', 3).
card_layout('champion of lambholt', 'normal').
card_power('champion of lambholt', 1).
card_toughness('champion of lambholt', 1).

% Found in: BNG
card_name('champion of stray souls', 'Champion of Stray Souls').
card_type('champion of stray souls', 'Creature — Skeleton Warrior').
card_types('champion of stray souls', ['Creature']).
card_subtypes('champion of stray souls', ['Skeleton', 'Warrior']).
card_colors('champion of stray souls', ['B']).
card_text('champion of stray souls', '{3}{B}{B}, {T}, Sacrifice X other creatures: Return X target creature cards from your graveyard to the battlefield.\n{5}{B}{B}: Put Champion of Stray Souls on top of your library from your graveyard.').
card_mana_cost('champion of stray souls', ['4', 'B', 'B']).
card_cmc('champion of stray souls', 6).
card_layout('champion of stray souls', 'normal').
card_power('champion of stray souls', 4).
card_toughness('champion of stray souls', 4).

% Found in: ISD
card_name('champion of the parish', 'Champion of the Parish').
card_type('champion of the parish', 'Creature — Human Soldier').
card_types('champion of the parish', ['Creature']).
card_subtypes('champion of the parish', ['Human', 'Soldier']).
card_colors('champion of the parish', ['W']).
card_text('champion of the parish', 'Whenever another Human enters the battlefield under your control, put a +1/+1 counter on Champion of the Parish.').
card_mana_cost('champion of the parish', ['W']).
card_cmc('champion of the parish', 1).
card_layout('champion of the parish', 'normal').
card_power('champion of the parish', 1).
card_toughness('champion of the parish', 1).

% Found in: ROE
card_name('champion\'s drake', 'Champion\'s Drake').
card_type('champion\'s drake', 'Creature — Drake').
card_types('champion\'s drake', ['Creature']).
card_subtypes('champion\'s drake', ['Drake']).
card_colors('champion\'s drake', ['U']).
card_text('champion\'s drake', 'Flying\nChampion\'s Drake gets +3/+3 as long as you control a creature with three or more level counters on it.').
card_mana_cost('champion\'s drake', ['1', 'U']).
card_cmc('champion\'s drake', 2).
card_layout('champion\'s drake', 'normal').
card_power('champion\'s drake', 1).
card_toughness('champion\'s drake', 1).

% Found in: CMD
card_name('champion\'s helm', 'Champion\'s Helm').
card_type('champion\'s helm', 'Artifact — Equipment').
card_types('champion\'s helm', ['Artifact']).
card_subtypes('champion\'s helm', ['Equipment']).
card_colors('champion\'s helm', []).
card_text('champion\'s helm', 'Equipped creature gets +2/+2.\nAs long as equipped creature is legendary, it has hexproof. (It can\'t be the target of spells or abilities your opponents control.)\nEquip {1}').
card_mana_cost('champion\'s helm', ['3']).
card_cmc('champion\'s helm', 3).
card_layout('champion\'s helm', 'normal').

% Found in: PTK
card_name('champion\'s victory', 'Champion\'s Victory').
card_type('champion\'s victory', 'Instant').
card_types('champion\'s victory', ['Instant']).
card_subtypes('champion\'s victory', []).
card_colors('champion\'s victory', ['U']).
card_text('champion\'s victory', 'Cast Champion\'s Victory only during the declare attackers step and only if you\'ve been attacked this step.\nReturn target attacking creature to its owner\'s hand.').
card_mana_cost('champion\'s victory', ['U']).
card_cmc('champion\'s victory', 1).
card_layout('champion\'s victory', 'normal').

% Found in: ODY
card_name('chance encounter', 'Chance Encounter').
card_type('chance encounter', 'Enchantment').
card_types('chance encounter', ['Enchantment']).
card_subtypes('chance encounter', []).
card_colors('chance encounter', ['R']).
card_text('chance encounter', 'Whenever you win a coin flip, put a luck counter on Chance Encounter.\nAt the beginning of your upkeep, if Chance Encounter has ten or more luck counters on it, you win the game.').
card_mana_cost('chance encounter', ['2', 'R', 'R']).
card_cmc('chance encounter', 4).
card_layout('chance encounter', 'normal').

% Found in: NPH
card_name('chancellor of the annex', 'Chancellor of the Annex').
card_type('chancellor of the annex', 'Creature — Angel').
card_types('chancellor of the annex', ['Creature']).
card_subtypes('chancellor of the annex', ['Angel']).
card_colors('chancellor of the annex', ['W']).
card_text('chancellor of the annex', 'You may reveal this card from your opening hand. If you do, when each opponent casts his or her first spell of the game, counter that spell unless that player pays {1}.\nFlying\nWhenever an opponent casts a spell, counter it unless that player pays {1}.').
card_mana_cost('chancellor of the annex', ['4', 'W', 'W', 'W']).
card_cmc('chancellor of the annex', 7).
card_layout('chancellor of the annex', 'normal').
card_power('chancellor of the annex', 5).
card_toughness('chancellor of the annex', 6).

% Found in: NPH
card_name('chancellor of the dross', 'Chancellor of the Dross').
card_type('chancellor of the dross', 'Creature — Vampire').
card_types('chancellor of the dross', ['Creature']).
card_subtypes('chancellor of the dross', ['Vampire']).
card_colors('chancellor of the dross', ['B']).
card_text('chancellor of the dross', 'You may reveal this card from your opening hand. If you do, at the beginning of the first upkeep, each opponent loses 3 life, then you gain life equal to the life lost this way.\nFlying, lifelink').
card_mana_cost('chancellor of the dross', ['4', 'B', 'B', 'B']).
card_cmc('chancellor of the dross', 7).
card_layout('chancellor of the dross', 'normal').
card_power('chancellor of the dross', 6).
card_toughness('chancellor of the dross', 6).

% Found in: NPH
card_name('chancellor of the forge', 'Chancellor of the Forge').
card_type('chancellor of the forge', 'Creature — Giant').
card_types('chancellor of the forge', ['Creature']).
card_subtypes('chancellor of the forge', ['Giant']).
card_colors('chancellor of the forge', ['R']).
card_text('chancellor of the forge', 'You may reveal this card from your opening hand. If you do, at the beginning of the first upkeep, put a 1/1 red Goblin creature token with haste onto the battlefield.\nWhen Chancellor of the Forge enters the battlefield, put X 1/1 red Goblin creature tokens with haste onto the battlefield, where X is the number of creatures you control.').
card_mana_cost('chancellor of the forge', ['4', 'R', 'R', 'R']).
card_cmc('chancellor of the forge', 7).
card_layout('chancellor of the forge', 'normal').
card_power('chancellor of the forge', 5).
card_toughness('chancellor of the forge', 5).

% Found in: NPH
card_name('chancellor of the spires', 'Chancellor of the Spires').
card_type('chancellor of the spires', 'Creature — Sphinx').
card_types('chancellor of the spires', ['Creature']).
card_subtypes('chancellor of the spires', ['Sphinx']).
card_colors('chancellor of the spires', ['U']).
card_text('chancellor of the spires', 'You may reveal this card from your opening hand. If you do, at the beginning of the first upkeep, each opponent puts the top seven cards of his or her library into his or her graveyard.\nFlying\nWhen Chancellor of the Spires enters the battlefield, you may cast target instant or sorcery card from an opponent\'s graveyard without paying its mana cost.').
card_mana_cost('chancellor of the spires', ['4', 'U', 'U', 'U']).
card_cmc('chancellor of the spires', 7).
card_layout('chancellor of the spires', 'normal').
card_power('chancellor of the spires', 5).
card_toughness('chancellor of the spires', 7).

% Found in: NPH
card_name('chancellor of the tangle', 'Chancellor of the Tangle').
card_type('chancellor of the tangle', 'Creature — Beast').
card_types('chancellor of the tangle', ['Creature']).
card_subtypes('chancellor of the tangle', ['Beast']).
card_colors('chancellor of the tangle', ['G']).
card_text('chancellor of the tangle', 'You may reveal this card from your opening hand. If you do, at the beginning of your first main phase, add {G} to your mana pool.\nVigilance, reach').
card_mana_cost('chancellor of the tangle', ['4', 'G', 'G', 'G']).
card_cmc('chancellor of the tangle', 7).
card_layout('chancellor of the tangle', 'normal').
card_power('chancellor of the tangle', 6).
card_toughness('chancellor of the tangle', 7).

% Found in: HML
card_name('chandler', 'Chandler').
card_type('chandler', 'Legendary Creature — Human Rogue').
card_types('chandler', ['Creature']).
card_subtypes('chandler', ['Human', 'Rogue']).
card_supertypes('chandler', ['Legendary']).
card_colors('chandler', ['R']).
card_text('chandler', '{R}{R}{R}, {T}: Destroy target artifact creature.').
card_mana_cost('chandler', ['4', 'R']).
card_cmc('chandler', 5).
card_layout('chandler', 'normal').
card_power('chandler', 3).
card_toughness('chandler', 3).

% Found in: ZEN
card_name('chandra ablaze', 'Chandra Ablaze').
card_type('chandra ablaze', 'Planeswalker — Chandra').
card_types('chandra ablaze', ['Planeswalker']).
card_subtypes('chandra ablaze', ['Chandra']).
card_colors('chandra ablaze', ['R']).
card_text('chandra ablaze', '+1: Discard a card. If a red card is discarded this way, Chandra Ablaze deals 4 damage to target creature or player.\n−2: Each player discards his or her hand, then draws three cards.\n−7: Cast any number of red instant and/or sorcery cards from your graveyard without paying their mana costs.').
card_mana_cost('chandra ablaze', ['4', 'R', 'R']).
card_cmc('chandra ablaze', 6).
card_layout('chandra ablaze', 'normal').
card_loyalty('chandra ablaze', 5).

% Found in: DD2, DD3_JVC, LRW, M10, M11
card_name('chandra nalaar', 'Chandra Nalaar').
card_type('chandra nalaar', 'Planeswalker — Chandra').
card_types('chandra nalaar', ['Planeswalker']).
card_subtypes('chandra nalaar', ['Chandra']).
card_colors('chandra nalaar', ['R']).
card_text('chandra nalaar', '+1: Chandra Nalaar deals 1 damage to target player.\n−X: Chandra Nalaar deals X damage to target creature.\n−8: Chandra Nalaar deals 10 damage to target player and each creature he or she controls.').
card_mana_cost('chandra nalaar', ['3', 'R', 'R']).
card_cmc('chandra nalaar', 5).
card_layout('chandra nalaar', 'normal').
card_loyalty('chandra nalaar', 6).

% Found in: M13, ORI, pMEI
card_name('chandra\'s fury', 'Chandra\'s Fury').
card_type('chandra\'s fury', 'Instant').
card_types('chandra\'s fury', ['Instant']).
card_subtypes('chandra\'s fury', []).
card_colors('chandra\'s fury', ['R']).
card_text('chandra\'s fury', 'Chandra\'s Fury deals 4 damage to target player and 1 damage to each creature that player controls.').
card_mana_cost('chandra\'s fury', ['4', 'R']).
card_cmc('chandra\'s fury', 5).
card_layout('chandra\'s fury', 'normal').

% Found in: ORI
card_name('chandra\'s ignition', 'Chandra\'s Ignition').
card_type('chandra\'s ignition', 'Sorcery').
card_types('chandra\'s ignition', ['Sorcery']).
card_subtypes('chandra\'s ignition', []).
card_colors('chandra\'s ignition', ['R']).
card_text('chandra\'s ignition', 'Target creature you control deals damage equal to its power to each other creature and each opponent.').
card_mana_cost('chandra\'s ignition', ['3', 'R', 'R']).
card_cmc('chandra\'s ignition', 5).
card_layout('chandra\'s ignition', 'normal').

% Found in: ARC, M11, M12, M14
card_name('chandra\'s outrage', 'Chandra\'s Outrage').
card_type('chandra\'s outrage', 'Instant').
card_types('chandra\'s outrage', ['Instant']).
card_subtypes('chandra\'s outrage', []).
card_colors('chandra\'s outrage', ['R']).
card_text('chandra\'s outrage', 'Chandra\'s Outrage deals 4 damage to target creature and 2 damage to that creature\'s controller.').
card_mana_cost('chandra\'s outrage', ['2', 'R', 'R']).
card_cmc('chandra\'s outrage', 4).
card_layout('chandra\'s outrage', 'normal').

% Found in: M12, M14, pMEI
card_name('chandra\'s phoenix', 'Chandra\'s Phoenix').
card_type('chandra\'s phoenix', 'Creature — Phoenix').
card_types('chandra\'s phoenix', ['Creature']).
card_subtypes('chandra\'s phoenix', ['Phoenix']).
card_colors('chandra\'s phoenix', ['R']).
card_text('chandra\'s phoenix', 'Flying\nHaste (This creature can attack and {T} as soon as it comes under your control.)\nWhenever an opponent is dealt damage by a red instant or sorcery spell you control or by a red planeswalker you control, return Chandra\'s Phoenix from your graveyard to your hand.').
card_mana_cost('chandra\'s phoenix', ['1', 'R', 'R']).
card_cmc('chandra\'s phoenix', 3).
card_layout('chandra\'s phoenix', 'normal').
card_power('chandra\'s phoenix', 2).
card_toughness('chandra\'s phoenix', 2).

% Found in: M11
card_name('chandra\'s spitfire', 'Chandra\'s Spitfire').
card_type('chandra\'s spitfire', 'Creature — Elemental').
card_types('chandra\'s spitfire', ['Creature']).
card_subtypes('chandra\'s spitfire', ['Elemental']).
card_colors('chandra\'s spitfire', ['R']).
card_text('chandra\'s spitfire', 'Flying\nWhenever an opponent is dealt noncombat damage, Chandra\'s Spitfire gets +3/+0 until end of turn.').
card_mana_cost('chandra\'s spitfire', ['2', 'R']).
card_cmc('chandra\'s spitfire', 3).
card_layout('chandra\'s spitfire', 'normal').
card_power('chandra\'s spitfire', 1).
card_toughness('chandra\'s spitfire', 3).

% Found in: ORI
card_name('chandra, fire of kaladesh', 'Chandra, Fire of Kaladesh').
card_type('chandra, fire of kaladesh', 'Legendary Creature — Human Shaman').
card_types('chandra, fire of kaladesh', ['Creature']).
card_subtypes('chandra, fire of kaladesh', ['Human', 'Shaman']).
card_supertypes('chandra, fire of kaladesh', ['Legendary']).
card_colors('chandra, fire of kaladesh', ['R']).
card_text('chandra, fire of kaladesh', 'Whenever you cast a red spell, untap Chandra, Fire of Kaladesh.\n{T}: Chandra, Fire of Kaladesh deals 1 damage to target player. If Chandra has dealt 3 or more damage this turn, exile her, then return her to the battlefield transformed under her owner\'s control.').
card_mana_cost('chandra, fire of kaladesh', ['1', 'R', 'R']).
card_cmc('chandra, fire of kaladesh', 3).
card_layout('chandra, fire of kaladesh', 'double-faced').
card_power('chandra, fire of kaladesh', 2).
card_toughness('chandra, fire of kaladesh', 2).
card_sides('chandra, fire of kaladesh', 'chandra, roaring flame').

% Found in: M14, M15, pMEI
card_name('chandra, pyromaster', 'Chandra, Pyromaster').
card_type('chandra, pyromaster', 'Planeswalker — Chandra').
card_types('chandra, pyromaster', ['Planeswalker']).
card_subtypes('chandra, pyromaster', ['Chandra']).
card_colors('chandra, pyromaster', ['R']).
card_text('chandra, pyromaster', '+1: Chandra, Pyromaster deals 1 damage to target player and 1 damage to up to one target creature that player controls. That creature can\'t block this turn.\n0: Exile the top card of your library. You may play it this turn.\n−7: Exile the top ten cards of your library. Choose an instant or sorcery card exiled this way and copy it three times. You may cast the copies without paying their mana costs.').
card_mana_cost('chandra, pyromaster', ['2', 'R', 'R']).
card_cmc('chandra, pyromaster', 4).
card_layout('chandra, pyromaster', 'normal').
card_loyalty('chandra, pyromaster', 4).

% Found in: ORI
card_name('chandra, roaring flame', 'Chandra, Roaring Flame').
card_type('chandra, roaring flame', 'Planeswalker — Chandra').
card_types('chandra, roaring flame', ['Planeswalker']).
card_subtypes('chandra, roaring flame', ['Chandra']).
card_colors('chandra, roaring flame', ['R']).
card_text('chandra, roaring flame', '+1: Chandra, Roaring Flame deals 2 damage to target player.\n−2: Chandra, Roaring Flame deals 2 damage to target creature.\n−7: Chandra, Roaring Flame deals 6 damage to each opponent. Each player dealt damage this way gets an emblem with \"At the beginning of your upkeep, this emblem deals 3 damage to you.\"').
card_layout('chandra, roaring flame', 'double-faced').
card_loyalty('chandra, roaring flame', 4).

% Found in: M12, M13
card_name('chandra, the firebrand', 'Chandra, the Firebrand').
card_type('chandra, the firebrand', 'Planeswalker — Chandra').
card_types('chandra, the firebrand', ['Planeswalker']).
card_subtypes('chandra, the firebrand', ['Chandra']).
card_colors('chandra, the firebrand', ['R']).
card_text('chandra, the firebrand', '+1: Chandra, the Firebrand deals 1 damage to target creature or player.\n−2: When you cast your next instant or sorcery spell this turn, copy that spell. You may choose new targets for the copy.\n−6: Chandra, the Firebrand deals 6 damage to each of up to six target creatures and/or players.').
card_mana_cost('chandra, the firebrand', ['3', 'R']).
card_cmc('chandra, the firebrand', 4).
card_layout('chandra, the firebrand', 'normal').
card_loyalty('chandra, the firebrand', 3).

% Found in: STH
card_name('change of heart', 'Change of Heart').
card_type('change of heart', 'Instant').
card_types('change of heart', ['Instant']).
card_subtypes('change of heart', []).
card_colors('change of heart', ['W']).
card_text('change of heart', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nTarget creature can\'t attack this turn.').
card_mana_cost('change of heart', ['W']).
card_cmc('change of heart', 1).
card_layout('change of heart', 'normal').

% Found in: LRW
card_name('changeling berserker', 'Changeling Berserker').
card_type('changeling berserker', 'Creature — Shapeshifter').
card_types('changeling berserker', ['Creature']).
card_subtypes('changeling berserker', ['Shapeshifter']).
card_colors('changeling berserker', ['R']).
card_text('changeling berserker', 'Changeling (This card is every creature type.)\nHaste\nChampion a creature (When this enters the battlefield, sacrifice it unless you exile another creature you control. When this leaves the battlefield, that card returns to the battlefield.)').
card_mana_cost('changeling berserker', ['3', 'R']).
card_cmc('changeling berserker', 4).
card_layout('changeling berserker', 'normal').
card_power('changeling berserker', 5).
card_toughness('changeling berserker', 3).

% Found in: LRW
card_name('changeling hero', 'Changeling Hero').
card_type('changeling hero', 'Creature — Shapeshifter').
card_types('changeling hero', ['Creature']).
card_subtypes('changeling hero', ['Shapeshifter']).
card_colors('changeling hero', ['W']).
card_text('changeling hero', 'Changeling (This card is every creature type.)\nChampion a creature (When this enters the battlefield, sacrifice it unless you exile another creature you control. When this leaves the battlefield, that card returns to the battlefield.)\nLifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('changeling hero', ['4', 'W']).
card_cmc('changeling hero', 5).
card_layout('changeling hero', 'normal').
card_power('changeling hero', 4).
card_toughness('changeling hero', 4).

% Found in: MOR
card_name('changeling sentinel', 'Changeling Sentinel').
card_type('changeling sentinel', 'Creature — Shapeshifter').
card_types('changeling sentinel', ['Creature']).
card_subtypes('changeling sentinel', ['Shapeshifter']).
card_colors('changeling sentinel', ['W']).
card_text('changeling sentinel', 'Changeling (This card is every creature type.)\nVigilance').
card_mana_cost('changeling sentinel', ['3', 'W']).
card_cmc('changeling sentinel', 4).
card_layout('changeling sentinel', 'normal').
card_power('changeling sentinel', 3).
card_toughness('changeling sentinel', 2).

% Found in: LRW
card_name('changeling titan', 'Changeling Titan').
card_type('changeling titan', 'Creature — Shapeshifter').
card_types('changeling titan', ['Creature']).
card_subtypes('changeling titan', ['Shapeshifter']).
card_colors('changeling titan', ['G']).
card_text('changeling titan', 'Changeling (This card is every creature type.)\nChampion a creature (When this enters the battlefield, sacrifice it unless you exile another creature you control. When this leaves the battlefield, that card returns to the battlefield.)').
card_mana_cost('changeling titan', ['4', 'G']).
card_cmc('changeling titan', 5).
card_layout('changeling titan', 'normal').
card_power('changeling titan', 7).
card_toughness('changeling titan', 7).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4, V09, VMA
card_name('channel', 'Channel').
card_type('channel', 'Sorcery').
card_types('channel', ['Sorcery']).
card_subtypes('channel', []).
card_colors('channel', ['G']).
card_text('channel', 'Until end of turn, any time you could activate a mana ability, you may pay 1 life. If you do, add {1} to your mana pool.').
card_mana_cost('channel', ['G', 'G']).
card_cmc('channel', 2).
card_layout('channel', 'normal').

% Found in: FRF
card_name('channel harm', 'Channel Harm').
card_type('channel harm', 'Instant').
card_types('channel harm', ['Instant']).
card_subtypes('channel harm', []).
card_colors('channel harm', ['W']).
card_text('channel harm', 'Prevent all damage that would be dealt to you and permanents you control this turn by sources you don\'t control. If damage is prevented this way, you may have Channel Harm deal that much damage to target creature.').
card_mana_cost('channel harm', ['5', 'W']).
card_cmc('channel harm', 6).
card_layout('channel harm', 'normal').

% Found in: 5DN
card_name('channel the suns', 'Channel the Suns').
card_type('channel the suns', 'Sorcery').
card_types('channel the suns', ['Sorcery']).
card_subtypes('channel the suns', []).
card_colors('channel the suns', ['G']).
card_text('channel the suns', 'Add {W}{U}{B}{R}{G} to your mana pool.').
card_mana_cost('channel the suns', ['3', 'G']).
card_cmc('channel the suns', 4).
card_layout('channel the suns', 'normal').

% Found in: DKA
card_name('chant of the skifsang', 'Chant of the Skifsang').
card_type('chant of the skifsang', 'Enchantment — Aura').
card_types('chant of the skifsang', ['Enchantment']).
card_subtypes('chant of the skifsang', ['Aura']).
card_colors('chant of the skifsang', ['U']).
card_text('chant of the skifsang', 'Enchant creature\nEnchanted creature gets -13/-0.').
card_mana_cost('chant of the skifsang', ['2', 'U']).
card_cmc('chant of the skifsang', 3).
card_layout('chant of the skifsang', 'normal').

% Found in: RAV
card_name('chant of vitu-ghazi', 'Chant of Vitu-Ghazi').
card_type('chant of vitu-ghazi', 'Instant').
card_types('chant of vitu-ghazi', ['Instant']).
card_subtypes('chant of vitu-ghazi', []).
card_colors('chant of vitu-ghazi', ['W']).
card_text('chant of vitu-ghazi', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nPrevent all damage that would be dealt by creatures this turn. You gain life equal to the damage prevented this way.').
card_mana_cost('chant of vitu-ghazi', ['6', 'W', 'W']).
card_cmc('chant of vitu-ghazi', 8).
card_layout('chant of vitu-ghazi', 'normal').

% Found in: APC, HOP
card_name('chaos', 'Chaos').
card_type('chaos', 'Instant').
card_types('chaos', ['Instant']).
card_subtypes('chaos', []).
card_colors('chaos', ['R']).
card_text('chaos', 'Creatures can\'t block this turn.').
card_mana_cost('chaos', ['2', 'R']).
card_cmc('chaos', 3).
card_layout('chaos', 'split').

% Found in: MIR
card_name('chaos charm', 'Chaos Charm').
card_type('chaos charm', 'Instant').
card_types('chaos charm', ['Instant']).
card_subtypes('chaos charm', []).
card_colors('chaos charm', ['R']).
card_text('chaos charm', 'Choose one —\n• Destroy target Wall.\n• Chaos Charm deals 1 damage to target creature.\n• Target creature gains haste until end of turn.').
card_mana_cost('chaos charm', ['R']).
card_cmc('chaos charm', 1).
card_layout('chaos charm', 'normal').

% Found in: UGL
card_name('chaos confetti', 'Chaos Confetti').
card_type('chaos confetti', 'Artifact').
card_types('chaos confetti', ['Artifact']).
card_subtypes('chaos confetti', []).
card_colors('chaos confetti', []).
card_text('chaos confetti', '{4}, {T}: Tear Chaos Confetti into pieces. Throw the pieces onto the playing area from a distance of at least five feet. Destroy each card in play that a piece touches. Remove the pieces from the game afterwards.').
card_mana_cost('chaos confetti', ['4']).
card_cmc('chaos confetti', 4).
card_layout('chaos confetti', 'normal').

% Found in: ALL
card_name('chaos harlequin', 'Chaos Harlequin').
card_type('chaos harlequin', 'Creature — Human').
card_types('chaos harlequin', ['Creature']).
card_subtypes('chaos harlequin', ['Human']).
card_colors('chaos harlequin', ['R']).
card_text('chaos harlequin', '{R}: Exile the top card of your library. If that card is a land card, Chaos Harlequin gets -4/-0 until end of turn. Otherwise, Chaos Harlequin gets +2/+0 until end of turn.').
card_mana_cost('chaos harlequin', ['2', 'R', 'R']).
card_cmc('chaos harlequin', 4).
card_layout('chaos harlequin', 'normal').
card_power('chaos harlequin', 2).
card_toughness('chaos harlequin', 4).
card_reserved('chaos harlequin').

% Found in: RTR
card_name('chaos imps', 'Chaos Imps').
card_type('chaos imps', 'Creature — Imp').
card_types('chaos imps', ['Creature']).
card_subtypes('chaos imps', ['Imp']).
card_colors('chaos imps', ['R']).
card_text('chaos imps', 'Flying\nUnleash (You may have this creature enter the battlefield with a +1/+1 counter on it. It can\'t block as long as it has a +1/+1 counter on it.)\nChaos Imps has trample as long as it has a +1/+1 counter on it.').
card_mana_cost('chaos imps', ['4', 'R', 'R']).
card_cmc('chaos imps', 6).
card_layout('chaos imps', 'normal').
card_power('chaos imps', 6).
card_toughness('chaos imps', 5).

% Found in: ICE
card_name('chaos lord', 'Chaos Lord').
card_type('chaos lord', 'Creature — Human').
card_types('chaos lord', ['Creature']).
card_subtypes('chaos lord', ['Human']).
card_colors('chaos lord', ['R']).
card_text('chaos lord', 'First strike\nAt the beginning of your upkeep, target opponent gains control of Chaos Lord if the number of permanents is even.\nChaos Lord can attack as though it had haste unless it entered the battlefield this turn.').
card_mana_cost('chaos lord', ['4', 'R', 'R', 'R']).
card_cmc('chaos lord', 7).
card_layout('chaos lord', 'normal').
card_power('chaos lord', 7).
card_toughness('chaos lord', 7).

% Found in: ICE
card_name('chaos moon', 'Chaos Moon').
card_type('chaos moon', 'Enchantment').
card_types('chaos moon', ['Enchantment']).
card_subtypes('chaos moon', []).
card_colors('chaos moon', ['R']).
card_text('chaos moon', 'At the beginning of each upkeep, count the number of permanents. If the number is odd, until end of turn, red creatures get +1/+1 and whenever a player taps a Mountain for mana, that player adds {R} to his or her mana pool (in addition to the mana the land produces). If the number is even, until end of turn, red creatures get -1/-1 and if a player taps a Mountain for mana, that Mountain produces colorless mana instead of any other type.').
card_mana_cost('chaos moon', ['3', 'R']).
card_cmc('chaos moon', 4).
card_layout('chaos moon', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB
card_name('chaos orb', 'Chaos Orb').
card_type('chaos orb', 'Artifact').
card_types('chaos orb', ['Artifact']).
card_subtypes('chaos orb', []).
card_colors('chaos orb', []).
card_text('chaos orb', '{1}, {T}: If Chaos Orb is on the battlefield, flip Chaos Orb onto the battlefield from a height of at least one foot. If Chaos Orb turns over completely at least once during the flip, destroy all nontoken permanents it touches. Then destroy Chaos Orb.').
card_mana_cost('chaos orb', ['2']).
card_cmc('chaos orb', 2).
card_layout('chaos orb', 'normal').
card_reserved('chaos orb').

% Found in: C14, CM1, CMD, VMA
card_name('chaos warp', 'Chaos Warp').
card_type('chaos warp', 'Instant').
card_types('chaos warp', ['Instant']).
card_subtypes('chaos warp', []).
card_colors('chaos warp', ['R']).
card_text('chaos warp', 'The owner of target permanent shuffles it into his or her library, then reveals the top card of his or her library. If it\'s a permanent card, he or she puts it onto the battlefield.').
card_mana_cost('chaos warp', ['2', 'R']).
card_cmc('chaos warp', 3).
card_layout('chaos warp', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('chaoslace', 'Chaoslace').
card_type('chaoslace', 'Instant').
card_types('chaoslace', ['Instant']).
card_subtypes('chaoslace', []).
card_colors('chaoslace', ['R']).
card_text('chaoslace', 'Target spell or permanent becomes red. (Its mana symbols remain unchanged.)').
card_mana_cost('chaoslace', ['R']).
card_cmc('chaoslace', 1).
card_layout('chaoslace', 'normal').

% Found in: MIR
card_name('chaosphere', 'Chaosphere').
card_type('chaosphere', 'World Enchantment').
card_types('chaosphere', ['Enchantment']).
card_subtypes('chaosphere', []).
card_supertypes('chaosphere', ['World']).
card_colors('chaosphere', ['R']).
card_text('chaosphere', 'Creatures with flying can block only creatures with flying.\nCreatures without flying have reach. (They can block creatures with flying.)').
card_mana_cost('chaosphere', ['2', 'R']).
card_cmc('chaosphere', 3).
card_layout('chaosphere', 'normal').
card_reserved('chaosphere').

% Found in: EVE
card_name('chaotic backlash', 'Chaotic Backlash').
card_type('chaotic backlash', 'Instant').
card_types('chaotic backlash', ['Instant']).
card_subtypes('chaotic backlash', []).
card_colors('chaotic backlash', ['R']).
card_text('chaotic backlash', 'Chaotic Backlash deals damage to target player equal to twice the number of white and/or blue permanents he or she controls.').
card_mana_cost('chaotic backlash', ['4', 'R']).
card_cmc('chaotic backlash', 5).
card_layout('chaotic backlash', 'normal').

% Found in: TMP
card_name('chaotic goo', 'Chaotic Goo').
card_type('chaotic goo', 'Creature — Ooze').
card_types('chaotic goo', ['Creature']).
card_subtypes('chaotic goo', ['Ooze']).
card_colors('chaotic goo', ['R']).
card_text('chaotic goo', 'Chaotic Goo enters the battlefield with three +1/+1 counters on it.\nAt the beginning of your upkeep, you may flip a coin. If you win the flip, put a +1/+1 counter on Chaotic Goo. If you lose the flip, remove a +1/+1 counter from Chaotic Goo.').
card_mana_cost('chaotic goo', ['2', 'R', 'R']).
card_cmc('chaotic goo', 4).
card_layout('chaotic goo', 'normal').
card_power('chaotic goo', 0).
card_toughness('chaotic goo', 0).

% Found in: INV
card_name('chaotic strike', 'Chaotic Strike').
card_type('chaotic strike', 'Instant').
card_types('chaotic strike', ['Instant']).
card_subtypes('chaotic strike', []).
card_colors('chaotic strike', ['R']).
card_text('chaotic strike', 'Cast Chaotic Strike only during combat after blockers are declared.\nFlip a coin. If you win the flip, target creature gets +1/+1 until end of turn.\nDraw a card.').
card_mana_cost('chaotic strike', ['1', 'R']).
card_cmc('chaotic strike', 2).
card_layout('chaotic strike', 'normal').

% Found in: PC2
card_name('chaotic æther', 'Chaotic Æther').
card_type('chaotic æther', 'Phenomenon').
card_types('chaotic æther', ['Phenomenon']).
card_subtypes('chaotic æther', []).
card_colors('chaotic æther', []).
card_text('chaotic æther', 'When you encounter Chaotic Æther, each blank roll of the planar die is a {C} roll until a player planeswalks away from a plane. (Then planeswalk away from this phenomenon.)').
card_layout('chaotic æther', 'phenomenon').

% Found in: ISD
card_name('chapel geist', 'Chapel Geist').
card_type('chapel geist', 'Creature — Spirit').
card_types('chapel geist', ['Creature']).
card_subtypes('chapel geist', ['Spirit']).
card_colors('chapel geist', ['W']).
card_text('chapel geist', 'Flying').
card_mana_cost('chapel geist', ['1', 'W', 'W']).
card_cmc('chapel geist', 3).
card_layout('chapel geist', 'normal').
card_power('chapel geist', 2).
card_toughness('chapel geist', 3).

% Found in: RAV, V13, p15A
card_name('char', 'Char').
card_type('char', 'Instant').
card_types('char', ['Instant']).
card_subtypes('char', []).
card_colors('char', ['R']).
card_text('char', 'Char deals 4 damage to target creature or player and 2 damage to you.').
card_mana_cost('char', ['2', 'R']).
card_cmc('char', 3).
card_layout('char', 'normal').

% Found in: FUT
card_name('char-rumbler', 'Char-Rumbler').
card_type('char-rumbler', 'Creature — Elemental').
card_types('char-rumbler', ['Creature']).
card_subtypes('char-rumbler', ['Elemental']).
card_colors('char-rumbler', ['R']).
card_text('char-rumbler', 'Double strike\n{R}: Char-Rumbler gets +1/+0 until end of turn.').
card_mana_cost('char-rumbler', ['2', 'R', 'R']).
card_cmc('char-rumbler', 4).
card_layout('char-rumbler', 'normal').
card_power('char-rumbler', '-1').
card_toughness('char-rumbler', 3).

% Found in: 6ED, 7ED, C14, MIR
card_name('charcoal diamond', 'Charcoal Diamond').
card_type('charcoal diamond', 'Artifact').
card_types('charcoal diamond', ['Artifact']).
card_subtypes('charcoal diamond', []).
card_colors('charcoal diamond', []).
card_text('charcoal diamond', 'Charcoal Diamond enters the battlefield tapped.\n{T}: Add {B} to your mana pool.').
card_mana_cost('charcoal diamond', ['2']).
card_cmc('charcoal diamond', 2).
card_layout('charcoal diamond', 'normal').

% Found in: SOK
card_name('charge across the araba', 'Charge Across the Araba').
card_type('charge across the araba', 'Instant — Arcane').
card_types('charge across the araba', ['Instant']).
card_subtypes('charge across the araba', ['Arcane']).
card_colors('charge across the araba', ['W']).
card_text('charge across the araba', 'Sweep — Return any number of Plains you control to their owner\'s hand. Creatures you control get +1/+1 until end of turn for each Plains returned this way.').
card_mana_cost('charge across the araba', ['4', 'W']).
card_cmc('charge across the araba', 5).
card_layout('charge across the araba', 'normal').

% Found in: BNG
card_name('charging badger', 'Charging Badger').
card_type('charging badger', 'Creature — Badger').
card_types('charging badger', ['Creature']).
card_subtypes('charging badger', ['Badger']).
card_colors('charging badger', ['G']).
card_text('charging badger', 'Trample').
card_mana_cost('charging badger', ['G']).
card_cmc('charging badger', 1).
card_layout('charging badger', 'normal').
card_power('charging badger', 1).
card_toughness('charging badger', 1).

% Found in: POR
card_name('charging bandits', 'Charging Bandits').
card_type('charging bandits', 'Creature — Human Rogue').
card_types('charging bandits', ['Creature']).
card_subtypes('charging bandits', ['Human', 'Rogue']).
card_colors('charging bandits', ['B']).
card_text('charging bandits', 'Whenever Charging Bandits attacks, it gets +2/+0 until end of turn.').
card_mana_cost('charging bandits', ['4', 'B']).
card_cmc('charging bandits', 5).
card_layout('charging bandits', 'normal').
card_power('charging bandits', 3).
card_toughness('charging bandits', 3).

% Found in: M14, ORI
card_name('charging griffin', 'Charging Griffin').
card_type('charging griffin', 'Creature — Griffin').
card_types('charging griffin', ['Creature']).
card_subtypes('charging griffin', ['Griffin']).
card_colors('charging griffin', ['W']).
card_text('charging griffin', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nWhenever Charging Griffin attacks, it gets +1/+1 until end of turn.').
card_mana_cost('charging griffin', ['3', 'W']).
card_cmc('charging griffin', 4).
card_layout('charging griffin', 'normal').
card_power('charging griffin', 2).
card_toughness('charging griffin', 2).

% Found in: DD3_DVD, DDC, EXO, POR, S99, TPR
card_name('charging paladin', 'Charging Paladin').
card_type('charging paladin', 'Creature — Human Knight').
card_types('charging paladin', ['Creature']).
card_subtypes('charging paladin', ['Human', 'Knight']).
card_colors('charging paladin', ['W']).
card_text('charging paladin', 'Whenever Charging Paladin attacks, it gets +0/+3 until end of turn.').
card_mana_cost('charging paladin', ['2', 'W']).
card_cmc('charging paladin', 3).
card_layout('charging paladin', 'normal').
card_power('charging paladin', 2).
card_toughness('charging paladin', 2).

% Found in: CNS, M15, POR, TMP
card_name('charging rhino', 'Charging Rhino').
card_type('charging rhino', 'Creature — Rhino').
card_types('charging rhino', ['Creature']).
card_subtypes('charging rhino', ['Rhino']).
card_colors('charging rhino', ['G']).
card_text('charging rhino', 'Charging Rhino can\'t be blocked by more than one creature.').
card_mana_cost('charging rhino', ['3', 'G', 'G']).
card_cmc('charging rhino', 5).
card_layout('charging rhino', 'normal').
card_power('charging rhino', 4).
card_toughness('charging rhino', 4).

% Found in: ONS
card_name('charging slateback', 'Charging Slateback').
card_type('charging slateback', 'Creature — Beast').
card_types('charging slateback', ['Creature']).
card_subtypes('charging slateback', ['Beast']).
card_colors('charging slateback', ['R']).
card_text('charging slateback', 'Charging Slateback can\'t block.\nMorph {4}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('charging slateback', ['4', 'R']).
card_cmc('charging slateback', 5).
card_layout('charging slateback', 'normal').
card_power('charging slateback', 4).
card_toughness('charging slateback', 3).

% Found in: DDE, INV
card_name('charging troll', 'Charging Troll').
card_type('charging troll', 'Creature — Troll').
card_types('charging troll', ['Creature']).
card_subtypes('charging troll', ['Troll']).
card_colors('charging troll', ['W', 'G']).
card_text('charging troll', 'Vigilance\n{G}: Regenerate Charging Troll.').
card_mana_cost('charging troll', ['2', 'G', 'W']).
card_cmc('charging troll', 4).
card_layout('charging troll', 'normal').
card_power('charging troll', 3).
card_toughness('charging troll', 3).

% Found in: MIR
card_name('chariot of the sun', 'Chariot of the Sun').
card_type('chariot of the sun', 'Artifact').
card_types('chariot of the sun', ['Artifact']).
card_subtypes('chariot of the sun', []).
card_colors('chariot of the sun', []).
card_text('chariot of the sun', '{2}, {T}: Until end of turn, target creature you control gains flying and has base toughness 1.').
card_mana_cost('chariot of the sun', ['3']).
card_cmc('chariot of the sun', 3).
card_layout('chariot of the sun', 'normal').

% Found in: JOU
card_name('chariot of victory', 'Chariot of Victory').
card_type('chariot of victory', 'Artifact — Equipment').
card_types('chariot of victory', ['Artifact']).
card_subtypes('chariot of victory', ['Equipment']).
card_colors('chariot of victory', []).
card_text('chariot of victory', 'Equipped creature has first strike, trample, and haste.\nEquip {1}').
card_mana_cost('chariot of victory', ['3']).
card_cmc('chariot of victory', 3).
card_layout('chariot of victory', 'normal').

% Found in: MMQ
card_name('charisma', 'Charisma').
card_type('charisma', 'Enchantment — Aura').
card_types('charisma', ['Enchantment']).
card_subtypes('charisma', ['Aura']).
card_colors('charisma', ['U']).
card_text('charisma', 'Enchant creature\nWhenever enchanted creature deals damage to a creature, gain control of the other creature for as long as Charisma remains on the battlefield.').
card_mana_cost('charisma', ['U', 'U', 'U']).
card_cmc('charisma', 3).
card_layout('charisma', 'normal').

% Found in: MMQ
card_name('charm peddler', 'Charm Peddler').
card_type('charm peddler', 'Creature — Human Spellshaper').
card_types('charm peddler', ['Creature']).
card_subtypes('charm peddler', ['Human', 'Spellshaper']).
card_colors('charm peddler', ['W']).
card_text('charm peddler', '{W}, {T}, Discard a card: The next time a source of your choice would deal damage to target creature this turn, prevent that damage.').
card_mana_cost('charm peddler', ['W']).
card_cmc('charm peddler', 1).
card_layout('charm peddler', 'normal').
card_power('charm peddler', 1).
card_toughness('charm peddler', 1).

% Found in: UGL
card_name('charm school', 'Charm School').
card_type('charm school', 'Enchant Player').
card_types('charm school', ['Enchant', 'Player']).
card_subtypes('charm school', []).
card_colors('charm school', ['W']).
card_text('charm school', 'When Charm School comes into play, choose a color and balance Charm School on your head.\nPrevent all damage to you of the chosen color.\nIf Charm School falls off your head, sacrifice Charm School.').
card_mana_cost('charm school', ['2', 'W']).
card_cmc('charm school', 3).
card_layout('charm school', 'normal').

% Found in: C13, ISD
card_name('charmbreaker devils', 'Charmbreaker Devils').
card_type('charmbreaker devils', 'Creature — Devil').
card_types('charmbreaker devils', ['Creature']).
card_subtypes('charmbreaker devils', ['Devil']).
card_colors('charmbreaker devils', ['R']).
card_text('charmbreaker devils', 'At the beginning of your upkeep, return an instant or sorcery card at random from your graveyard to your hand.\nWhenever you cast an instant or sorcery spell, Charmbreaker Devils gets +4/+0 until end of turn.').
card_mana_cost('charmbreaker devils', ['5', 'R']).
card_cmc('charmbreaker devils', 6).
card_layout('charmbreaker devils', 'normal').
card_power('charmbreaker devils', 4).
card_toughness('charmbreaker devils', 4).

% Found in: MMQ
card_name('charmed griffin', 'Charmed Griffin').
card_type('charmed griffin', 'Creature — Griffin').
card_types('charmed griffin', ['Creature']).
card_subtypes('charmed griffin', ['Griffin']).
card_colors('charmed griffin', ['W']).
card_text('charmed griffin', 'Flying\nWhen Charmed Griffin enters the battlefield, each other player may put an artifact or enchantment card onto the battlefield from his or her hand.').
card_mana_cost('charmed griffin', ['3', 'W']).
card_cmc('charmed griffin', 4).
card_layout('charmed griffin', 'normal').
card_power('charmed griffin', 3).
card_toughness('charmed griffin', 3).

% Found in: ODY
card_name('charmed pendant', 'Charmed Pendant').
card_type('charmed pendant', 'Artifact').
card_types('charmed pendant', ['Artifact']).
card_subtypes('charmed pendant', []).
card_colors('charmed pendant', []).
card_text('charmed pendant', '{T}, Put the top card of your library into your graveyard: For each colored mana symbol in that card\'s mana cost, add one mana of that color to your mana pool. Activate this ability only any time you could cast an instant. (For example, if the card\'s mana cost is {3}{U}{U}{B}, you add {U}{U}{B} to your mana pool.)').
card_mana_cost('charmed pendant', ['4']).
card_cmc('charmed pendant', 4).
card_layout('charmed pendant', 'normal').

% Found in: C13, CON
card_name('charnelhoard wurm', 'Charnelhoard Wurm').
card_type('charnelhoard wurm', 'Creature — Wurm').
card_types('charnelhoard wurm', ['Creature']).
card_subtypes('charnelhoard wurm', ['Wurm']).
card_colors('charnelhoard wurm', ['B', 'R', 'G']).
card_text('charnelhoard wurm', 'Trample\nWhenever Charnelhoard Wurm deals damage to an opponent, you may return target card from your graveyard to your hand.').
card_mana_cost('charnelhoard wurm', ['4', 'B', 'R', 'G']).
card_cmc('charnelhoard wurm', 7).
card_layout('charnelhoard wurm', 'normal').
card_power('charnelhoard wurm', 6).
card_toughness('charnelhoard wurm', 6).

% Found in: CMD, CNS, DD2, DD3_JVC, DDI, SCG, VMA
card_name('chartooth cougar', 'Chartooth Cougar').
card_type('chartooth cougar', 'Creature — Cat Beast').
card_types('chartooth cougar', ['Creature']).
card_subtypes('chartooth cougar', ['Cat', 'Beast']).
card_colors('chartooth cougar', ['R']).
card_text('chartooth cougar', '{R}: Chartooth Cougar gets +1/+0 until end of turn.\nMountaincycling {2} ({2}, Discard this card: Search your library for a Mountain card, reveal it, and put it into your hand. Then shuffle your library.)').
card_mana_cost('chartooth cougar', ['5', 'R']).
card_cmc('chartooth cougar', 6).
card_layout('chartooth cougar', 'normal').
card_power('chartooth cougar', 4).
card_toughness('chartooth cougar', 4).

% Found in: M12
card_name('chasm drake', 'Chasm Drake').
card_type('chasm drake', 'Creature — Drake').
card_types('chasm drake', ['Creature']).
card_subtypes('chasm drake', ['Drake']).
card_colors('chasm drake', ['U']).
card_text('chasm drake', 'Flying\nWhenever Chasm Drake attacks, target creature you control gains flying until end of turn.').
card_mana_cost('chasm drake', ['4', 'U']).
card_cmc('chasm drake', 5).
card_layout('chasm drake', 'normal').
card_power('chasm drake', 3).
card_toughness('chasm drake', 3).

% Found in: BFZ
card_name('chasm guide', 'Chasm Guide').
card_type('chasm guide', 'Creature — Goblin Scout Ally').
card_types('chasm guide', ['Creature']).
card_subtypes('chasm guide', ['Goblin', 'Scout', 'Ally']).
card_colors('chasm guide', ['R']).
card_text('chasm guide', 'Rally — Whenever Chasm Guide or another Ally enters the battlefield under your control, creatures you control gain haste until end of turn.').
card_mana_cost('chasm guide', ['3', 'R']).
card_cmc('chasm guide', 4).
card_layout('chasm guide', 'normal').
card_power('chasm guide', 3).
card_toughness('chasm guide', 2).

% Found in: M15
card_name('chasm skulker', 'Chasm Skulker').
card_type('chasm skulker', 'Creature — Squid Horror').
card_types('chasm skulker', ['Creature']).
card_subtypes('chasm skulker', ['Squid', 'Horror']).
card_colors('chasm skulker', ['U']).
card_text('chasm skulker', 'Whenever you draw a card, put a +1/+1 counter on Chasm Skulker.\nWhen Chasm Skulker dies, put X 1/1 blue Squid creature tokens with islandwalk onto the battlefield, where X is the number of +1/+1 counters on Chasm Skulker. (They can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('chasm skulker', ['2', 'U']).
card_cmc('chasm skulker', 3).
card_layout('chasm skulker', 'normal').
card_power('chasm skulker', 1).
card_toughness('chasm skulker', 1).

% Found in: 8ED, 9ED, JUD
card_name('chastise', 'Chastise').
card_type('chastise', 'Instant').
card_types('chastise', ['Instant']).
card_subtypes('chastise', []).
card_colors('chastise', ['W']).
card_text('chastise', 'Destroy target attacking creature. You gain life equal to its power.').
card_mana_cost('chastise', ['3', 'W']).
card_cmc('chastise', 4).
card_layout('chastise', 'normal').

% Found in: ODY
card_name('chatter of the squirrel', 'Chatter of the Squirrel').
card_type('chatter of the squirrel', 'Sorcery').
card_types('chatter of the squirrel', ['Sorcery']).
card_subtypes('chatter of the squirrel', []).
card_colors('chatter of the squirrel', ['G']).
card_text('chatter of the squirrel', 'Put a 1/1 green Squirrel creature token onto the battlefield.\nFlashback {1}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('chatter of the squirrel', ['G']).
card_cmc('chatter of the squirrel', 1).
card_layout('chatter of the squirrel', 'normal').

% Found in: UNH
card_name('cheap ass', 'Cheap Ass').
card_type('cheap ass', 'Creature — Donkey Townsfolk').
card_types('cheap ass', ['Creature']).
card_subtypes('cheap ass', ['Donkey', 'Townsfolk']).
card_colors('cheap ass', ['W']).
card_text('cheap ass', 'Spells you play cost {½} less to play.').
card_mana_cost('cheap ass', ['1', 'W']).
card_cmc('cheap ass', 2).
card_layout('cheap ass', 'normal').
card_power('cheap ass', 1).
card_toughness('cheap ass', 3.5).

% Found in: UNH
card_name('cheatyface', 'Cheatyface').
card_type('cheatyface', 'Creature — Efreet').
card_types('cheatyface', ['Creature']).
card_subtypes('cheatyface', ['Efreet']).
card_colors('cheatyface', ['U']).
card_text('cheatyface', 'You may sneak Cheatyface into play at any time without paying for it, but if an opponent catches you right away, that player may remove Cheatyface from the game.\nFlying').
card_mana_cost('cheatyface', ['U', 'U', 'U']).
card_cmc('cheatyface', 3).
card_layout('cheatyface', 'normal').
card_power('cheatyface', 2).
card_toughness('cheatyface', 2).

% Found in: UGL
card_name('checks and balances', 'Checks and Balances').
card_type('checks and balances', 'Enchantment').
card_types('checks and balances', ['Enchantment']).
card_subtypes('checks and balances', []).
card_colors('checks and balances', ['U']).
card_text('checks and balances', 'Whenever any spell is played, counter that spell if each player, other than the caster and his or her teammates, agrees to choose and discard a card. Those players must discard those cards after agreeing.\nChecks and Balances may be played only in a game with three or more players.').
card_mana_cost('checks and balances', ['2', 'U']).
card_cmc('checks and balances', 3).
card_layout('checks and balances', 'normal').

% Found in: RTR
card_name('chemister\'s trick', 'Chemister\'s Trick').
card_type('chemister\'s trick', 'Instant').
card_types('chemister\'s trick', ['Instant']).
card_subtypes('chemister\'s trick', []).
card_colors('chemister\'s trick', ['U', 'R']).
card_text('chemister\'s trick', 'Target creature you don\'t control gets -2/-0 until end of turn and attacks this turn if able.\nOverload {3}{U}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('chemister\'s trick', ['U', 'R']).
card_cmc('chemister\'s trick', 2).
card_layout('chemister\'s trick', 'normal').

% Found in: UGL
card_name('chicken egg', 'Chicken Egg').
card_type('chicken egg', 'Creature — Egg').
card_types('chicken egg', ['Creature']).
card_subtypes('chicken egg', ['Egg']).
card_colors('chicken egg', ['R']).
card_text('chicken egg', 'During your upkeep, roll a six-sided die. On a 6, sacrifice Chicken Egg and put a Giant Chicken token into play. Treat this token as a 4/4 red creature that counts as a Chicken.').
card_mana_cost('chicken egg', ['1', 'R']).
card_cmc('chicken egg', 2).
card_layout('chicken egg', 'normal').
card_power('chicken egg', 0).
card_toughness('chicken egg', 1).

% Found in: UGL
card_name('chicken à la king', 'Chicken à la King').
card_type('chicken à la king', 'Creature — Chicken').
card_types('chicken à la king', ['Creature']).
card_subtypes('chicken à la king', ['Chicken']).
card_colors('chicken à la king', ['U']).
card_text('chicken à la king', 'Whenever a 6 is rolled on a six-sided die, put a +1/+1 counter on each Chicken in play. (You may roll dice only when a card instructs you to.)\nTap a Chicken you control: Roll a six-sided die.').
card_mana_cost('chicken à la king', ['1', 'U', 'U']).
card_cmc('chicken à la king', 3).
card_layout('chicken à la king', 'normal').
card_power('chicken à la king', 2).
card_toughness('chicken à la king', 2).

% Found in: M15, pMGD
card_name('chief engineer', 'Chief Engineer').
card_type('chief engineer', 'Creature — Vedalken Artificer').
card_types('chief engineer', ['Creature']).
card_subtypes('chief engineer', ['Vedalken', 'Artificer']).
card_colors('chief engineer', ['U']).
card_text('chief engineer', 'Artifact spells you cast have convoke. (Your creatures can help cast those spells. Each creature you tap while casting an artifact spell pays for {1} or one mana of that creature\'s color.)').
card_mana_cost('chief engineer', ['1', 'U']).
card_cmc('chief engineer', 2).
card_layout('chief engineer', 'normal').
card_power('chief engineer', 1).
card_toughness('chief engineer', 3).

% Found in: KTK
card_name('chief of the edge', 'Chief of the Edge').
card_type('chief of the edge', 'Creature — Human Warrior').
card_types('chief of the edge', ['Creature']).
card_subtypes('chief of the edge', ['Human', 'Warrior']).
card_colors('chief of the edge', ['W', 'B']).
card_text('chief of the edge', 'Other Warrior creatures you control get +1/+0.').
card_mana_cost('chief of the edge', ['W', 'B']).
card_cmc('chief of the edge', 2).
card_layout('chief of the edge', 'normal').
card_power('chief of the edge', 3).
card_toughness('chief of the edge', 2).

% Found in: ORI
card_name('chief of the foundry', 'Chief of the Foundry').
card_type('chief of the foundry', 'Artifact Creature — Construct').
card_types('chief of the foundry', ['Artifact', 'Creature']).
card_subtypes('chief of the foundry', ['Construct']).
card_colors('chief of the foundry', []).
card_text('chief of the foundry', 'Other artifact creatures you control get +1/+1.').
card_mana_cost('chief of the foundry', ['3']).
card_cmc('chief of the foundry', 3).
card_layout('chief of the foundry', 'normal').
card_power('chief of the foundry', 2).
card_toughness('chief of the foundry', 3).

% Found in: KTK
card_name('chief of the scale', 'Chief of the Scale').
card_type('chief of the scale', 'Creature — Human Warrior').
card_types('chief of the scale', ['Creature']).
card_subtypes('chief of the scale', ['Human', 'Warrior']).
card_colors('chief of the scale', ['W', 'B']).
card_text('chief of the scale', 'Other Warrior creatures you control get +0/+1.').
card_mana_cost('chief of the scale', ['W', 'B']).
card_cmc('chief of the scale', 2).
card_layout('chief of the scale', 'normal').
card_power('chief of the scale', 2).
card_toughness('chief of the scale', 3).

% Found in: NMS
card_name('chieftain en-dal', 'Chieftain en-Dal').
card_type('chieftain en-dal', 'Creature — Human Knight').
card_types('chieftain en-dal', ['Creature']).
card_subtypes('chieftain en-dal', ['Human', 'Knight']).
card_colors('chieftain en-dal', ['W']).
card_text('chieftain en-dal', 'Whenever Chieftain en-Dal attacks, attacking creatures gain first strike until end of turn.').
card_mana_cost('chieftain en-dal', ['1', 'W', 'W']).
card_cmc('chieftain en-dal', 3).
card_layout('chieftain en-dal', 'normal').
card_power('chieftain en-dal', 2).
card_toughness('chieftain en-dal', 2).

% Found in: CON, V14
card_name('child of alara', 'Child of Alara').
card_type('child of alara', 'Legendary Creature — Avatar').
card_types('child of alara', ['Creature']).
card_subtypes('child of alara', ['Avatar']).
card_supertypes('child of alara', ['Legendary']).
card_colors('child of alara', ['W', 'U', 'B', 'R', 'G']).
card_text('child of alara', 'Trample\nWhen Child of Alara dies, destroy all nonland permanents. They can\'t be regenerated.').
card_mana_cost('child of alara', ['W', 'U', 'B', 'R', 'G']).
card_cmc('child of alara', 5).
card_layout('child of alara', 'normal').
card_power('child of alara', 6).
card_toughness('child of alara', 6).

% Found in: USG
card_name('child of gaea', 'Child of Gaea').
card_type('child of gaea', 'Creature — Elemental').
card_types('child of gaea', ['Creature']).
card_subtypes('child of gaea', ['Elemental']).
card_colors('child of gaea', ['G']).
card_text('child of gaea', 'Trample\nAt the beginning of your upkeep, sacrifice Child of Gaea unless you pay {G}{G}.\n{1}{G}: Regenerate Child of Gaea.').
card_mana_cost('child of gaea', ['3', 'G', 'G', 'G']).
card_cmc('child of gaea', 6).
card_layout('child of gaea', 'normal').
card_power('child of gaea', 7).
card_toughness('child of gaea', 7).

% Found in: DDK, M10, M11, M12, M14, M15
card_name('child of night', 'Child of Night').
card_type('child of night', 'Creature — Vampire').
card_types('child of night', ['Creature']).
card_subtypes('child of night', ['Vampire']).
card_colors('child of night', ['B']).
card_text('child of night', 'Lifelink (Damage dealt by this creature also causes you to gain that much life.)').
card_mana_cost('child of night', ['1', 'B']).
card_cmc('child of night', 2).
card_layout('child of night', 'normal').
card_power('child of night', 2).
card_toughness('child of night', 1).

% Found in: BOK
card_name('child of thorns', 'Child of Thorns').
card_type('child of thorns', 'Creature — Spirit').
card_types('child of thorns', ['Creature']).
card_subtypes('child of thorns', ['Spirit']).
card_colors('child of thorns', ['G']).
card_text('child of thorns', 'Sacrifice Child of Thorns: Target creature gets +1/+1 until end of turn.').
card_mana_cost('child of thorns', ['G']).
card_cmc('child of thorns', 1).
card_layout('child of thorns', 'normal').
card_power('child of thorns', 1).
card_toughness('child of thorns', 1).

% Found in: ODY
card_name('childhood horror', 'Childhood Horror').
card_type('childhood horror', 'Creature — Horror').
card_types('childhood horror', ['Creature']).
card_subtypes('childhood horror', ['Horror']).
card_colors('childhood horror', ['B']).
card_text('childhood horror', 'Flying\nThreshold — As long as seven or more cards are in your graveyard, Childhood Horror gets +2/+2 and can\'t block.').
card_mana_cost('childhood horror', ['3', 'B']).
card_cmc('childhood horror', 4).
card_layout('childhood horror', 'normal').
card_power('childhood horror', 2).
card_toughness('childhood horror', 2).

% Found in: TSP
card_name('children of korlis', 'Children of Korlis').
card_type('children of korlis', 'Creature — Human Rebel Cleric').
card_types('children of korlis', ['Creature']).
card_subtypes('children of korlis', ['Human', 'Rebel', 'Cleric']).
card_colors('children of korlis', ['W']).
card_text('children of korlis', 'Sacrifice Children of Korlis: You gain life equal to the life you\'ve lost this turn. (Damage causes loss of life.)').
card_mana_cost('children of korlis', ['W']).
card_cmc('children of korlis', 1).
card_layout('children of korlis', 'normal').
card_power('children of korlis', 1).
card_toughness('children of korlis', 1).

% Found in: 6ED, TMP, pARL
card_name('chill', 'Chill').
card_type('chill', 'Enchantment').
card_types('chill', ['Enchantment']).
card_subtypes('chill', []).
card_colors('chill', ['U']).
card_text('chill', 'Red spells cost {2} more to cast.').
card_mana_cost('chill', ['1', 'U']).
card_cmc('chill', 2).
card_layout('chill', 'normal').

% Found in: SCG
card_name('chill haunting', 'Chill Haunting').
card_type('chill haunting', 'Instant').
card_types('chill haunting', ['Instant']).
card_subtypes('chill haunting', []).
card_colors('chill haunting', ['B']).
card_text('chill haunting', 'As an additional cost to cast Chill Haunting, exile X creature cards from your graveyard.\nTarget creature gets -X/-X until end of turn.').
card_mana_cost('chill haunting', ['1', 'B']).
card_cmc('chill haunting', 2).
card_layout('chill haunting', 'normal').

% Found in: DKA
card_name('chill of foreboding', 'Chill of Foreboding').
card_type('chill of foreboding', 'Sorcery').
card_types('chill of foreboding', ['Sorcery']).
card_subtypes('chill of foreboding', []).
card_colors('chill of foreboding', ['U']).
card_text('chill of foreboding', 'Each player puts the top five cards of his or her library into his or her graveyard.\nFlashback {7}{U} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('chill of foreboding', ['2', 'U']).
card_cmc('chill of foreboding', 3).
card_layout('chill of foreboding', 'normal').

% Found in: CSP
card_name('chill to the bone', 'Chill to the Bone').
card_type('chill to the bone', 'Instant').
card_types('chill to the bone', ['Instant']).
card_subtypes('chill to the bone', []).
card_colors('chill to the bone', ['B']).
card_text('chill to the bone', 'Destroy target nonsnow creature.').
card_mana_cost('chill to the bone', ['3', 'B']).
card_cmc('chill to the bone', 4).
card_layout('chill to the bone', 'normal').

% Found in: PCY
card_name('chilling apparition', 'Chilling Apparition').
card_type('chilling apparition', 'Creature — Spirit').
card_types('chilling apparition', ['Creature']).
card_subtypes('chilling apparition', ['Spirit']).
card_colors('chilling apparition', ['B']).
card_text('chilling apparition', '{B}: Regenerate Chilling Apparition.\nWhenever Chilling Apparition deals combat damage to a player, that player discards a card.').
card_mana_cost('chilling apparition', ['2', 'B']).
card_cmc('chilling apparition', 3).
card_layout('chilling apparition', 'normal').
card_power('chilling apparition', 1).
card_toughness('chilling apparition', 1).

% Found in: CSP
card_name('chilling shade', 'Chilling Shade').
card_type('chilling shade', 'Snow Creature — Shade').
card_types('chilling shade', ['Creature']).
card_subtypes('chilling shade', ['Shade']).
card_supertypes('chilling shade', ['Snow']).
card_colors('chilling shade', ['B']).
card_text('chilling shade', 'Flying\n{S}: Chilling Shade gets +1/+1 until end of turn. ({S} can be paid with one mana from a snow permanent.)').
card_mana_cost('chilling shade', ['2', 'B']).
card_cmc('chilling shade', 3).
card_layout('chilling shade', 'normal').
card_power('chilling shade', 1).
card_toughness('chilling shade', 1).

% Found in: UDS
card_name('chime of night', 'Chime of Night').
card_type('chime of night', 'Enchantment — Aura').
card_types('chime of night', ['Enchantment']).
card_subtypes('chime of night', ['Aura']).
card_colors('chime of night', ['B']).
card_text('chime of night', 'Enchant creature\nWhen Chime of Night is put into a graveyard from the battlefield, destroy target nonblack creature.').
card_mana_cost('chime of night', ['1', 'B']).
card_cmc('chime of night', 2).
card_layout('chime of night', 'normal').

% Found in: 5DN
card_name('chimeric coils', 'Chimeric Coils').
card_type('chimeric coils', 'Artifact').
card_types('chimeric coils', ['Artifact']).
card_subtypes('chimeric coils', []).
card_colors('chimeric coils', []).
card_text('chimeric coils', '{X}{1}: Chimeric Coils becomes an X/X Construct artifact creature. Sacrifice it at the beginning of the next end step.').
card_mana_cost('chimeric coils', ['1']).
card_cmc('chimeric coils', 1).
card_layout('chimeric coils', 'normal').

% Found in: DST
card_name('chimeric egg', 'Chimeric Egg').
card_type('chimeric egg', 'Artifact').
card_types('chimeric egg', ['Artifact']).
card_subtypes('chimeric egg', []).
card_colors('chimeric egg', []).
card_text('chimeric egg', 'Whenever an opponent casts a nonartifact spell, put a charge counter on Chimeric Egg.\nRemove three charge counters from Chimeric Egg: Chimeric Egg becomes a 6/6 Construct artifact creature with trample until end of turn.').
card_mana_cost('chimeric egg', ['3']).
card_cmc('chimeric egg', 3).
card_layout('chimeric egg', 'normal').

% Found in: PCY, VMA
card_name('chimeric idol', 'Chimeric Idol').
card_type('chimeric idol', 'Artifact').
card_types('chimeric idol', ['Artifact']).
card_subtypes('chimeric idol', []).
card_colors('chimeric idol', []).
card_text('chimeric idol', '{0}: Tap all lands you control. Chimeric Idol becomes a 3/3 Turtle artifact creature until end of turn.').
card_mana_cost('chimeric idol', ['3']).
card_cmc('chimeric idol', 3).
card_layout('chimeric idol', 'normal').

% Found in: MM2, SOM
card_name('chimeric mass', 'Chimeric Mass').
card_type('chimeric mass', 'Artifact').
card_types('chimeric mass', ['Artifact']).
card_subtypes('chimeric mass', []).
card_colors('chimeric mass', []).
card_text('chimeric mass', 'Chimeric Mass enters the battlefield with X charge counters on it.\n{1}: Until end of turn, Chimeric Mass becomes a Construct artifact creature with \"This creature\'s power and toughness are each equal to the number of charge counters on it.\"').
card_mana_cost('chimeric mass', ['X']).
card_cmc('chimeric mass', 0).
card_layout('chimeric mass', 'normal').

% Found in: WTH
card_name('chimeric sphere', 'Chimeric Sphere').
card_type('chimeric sphere', 'Artifact').
card_types('chimeric sphere', ['Artifact']).
card_subtypes('chimeric sphere', []).
card_colors('chimeric sphere', []).
card_text('chimeric sphere', '{2}: Until end of turn, Chimeric Sphere becomes a 2/1 Construct artifact creature with flying.\n{2}: Until end of turn, Chimeric Sphere becomes a 3/2 Construct artifact creature and loses flying.').
card_mana_cost('chimeric sphere', ['3']).
card_cmc('chimeric sphere', 3).
card_layout('chimeric sphere', 'normal').

% Found in: 10E, USG
card_name('chimeric staff', 'Chimeric Staff').
card_type('chimeric staff', 'Artifact').
card_types('chimeric staff', ['Artifact']).
card_subtypes('chimeric staff', []).
card_colors('chimeric staff', []).
card_text('chimeric staff', '{X}: Chimeric Staff becomes an X/X Construct artifact creature until end of turn.').
card_mana_cost('chimeric staff', ['4']).
card_cmc('chimeric staff', 4).
card_layout('chimeric staff', 'normal').

% Found in: MRD
card_name('chimney imp', 'Chimney Imp').
card_type('chimney imp', 'Creature — Imp').
card_types('chimney imp', ['Creature']).
card_subtypes('chimney imp', ['Imp']).
card_colors('chimney imp', ['B']).
card_text('chimney imp', 'Flying\nWhen Chimney Imp dies, target opponent puts a card from his or her hand on top of his or her library.').
card_mana_cost('chimney imp', ['4', 'B']).
card_cmc('chimney imp', 5).
card_layout('chimney imp', 'normal').
card_power('chimney imp', 1).
card_toughness('chimney imp', 2).

% Found in: BOK
card_name('chisei, heart of oceans', 'Chisei, Heart of Oceans').
card_type('chisei, heart of oceans', 'Legendary Creature — Spirit').
card_types('chisei, heart of oceans', ['Creature']).
card_subtypes('chisei, heart of oceans', ['Spirit']).
card_supertypes('chisei, heart of oceans', ['Legendary']).
card_colors('chisei, heart of oceans', ['U']).
card_text('chisei, heart of oceans', 'Flying\nAt the beginning of your upkeep, sacrifice Chisei, Heart of Oceans unless you remove a counter from a permanent you control.').
card_mana_cost('chisei, heart of oceans', ['2', 'U', 'U']).
card_cmc('chisei, heart of oceans', 4).
card_layout('chisei, heart of oceans', 'normal').
card_power('chisei, heart of oceans', 4).
card_toughness('chisei, heart of oceans', 4).

% Found in: DST
card_name('chittering rats', 'Chittering Rats').
card_type('chittering rats', 'Creature — Rat').
card_types('chittering rats', ['Creature']).
card_subtypes('chittering rats', ['Rat']).
card_colors('chittering rats', ['B']).
card_text('chittering rats', 'When Chittering Rats enters the battlefield, target opponent puts a card from his or her hand on top of his or her library.').
card_mana_cost('chittering rats', ['1', 'B', 'B']).
card_cmc('chittering rats', 3).
card_layout('chittering rats', 'normal').
card_power('chittering rats', 2).
card_toughness('chittering rats', 2).

% Found in: ODY
card_name('chlorophant', 'Chlorophant').
card_type('chlorophant', 'Creature — Elemental').
card_types('chlorophant', ['Creature']).
card_subtypes('chlorophant', ['Elemental']).
card_colors('chlorophant', ['G']).
card_text('chlorophant', 'At the beginning of your upkeep, you may put a +1/+1 counter on Chlorophant.\nThreshold — As long as seven or more cards are in your graveyard, Chlorophant has \"At the beginning of your upkeep, you may put another +1/+1 counter on Chlorophant.\"').
card_mana_cost('chlorophant', ['G', 'G', 'G']).
card_cmc('chlorophant', 3).
card_layout('chlorophant', 'normal').
card_power('chlorophant', 1).
card_toughness('chlorophant', 1).

% Found in: MMQ
card_name('cho-arrim alchemist', 'Cho-Arrim Alchemist').
card_type('cho-arrim alchemist', 'Creature — Human Spellshaper').
card_types('cho-arrim alchemist', ['Creature']).
card_subtypes('cho-arrim alchemist', ['Human', 'Spellshaper']).
card_colors('cho-arrim alchemist', ['W']).
card_text('cho-arrim alchemist', '{1}{W}{W}, {T}, Discard a card: The next time a source of your choice would deal damage to you this turn, prevent that damage. You gain life equal to the damage prevented this way.').
card_mana_cost('cho-arrim alchemist', ['W']).
card_cmc('cho-arrim alchemist', 1).
card_layout('cho-arrim alchemist', 'normal').
card_power('cho-arrim alchemist', 1).
card_toughness('cho-arrim alchemist', 1).

% Found in: MMQ
card_name('cho-arrim bruiser', 'Cho-Arrim Bruiser').
card_type('cho-arrim bruiser', 'Creature — Ogre Rebel').
card_types('cho-arrim bruiser', ['Creature']).
card_subtypes('cho-arrim bruiser', ['Ogre', 'Rebel']).
card_colors('cho-arrim bruiser', ['W']).
card_text('cho-arrim bruiser', 'Whenever Cho-Arrim Bruiser attacks, you may tap up to two target creatures.').
card_mana_cost('cho-arrim bruiser', ['5', 'W']).
card_cmc('cho-arrim bruiser', 6).
card_layout('cho-arrim bruiser', 'normal').
card_power('cho-arrim bruiser', 3).
card_toughness('cho-arrim bruiser', 4).

% Found in: MMQ
card_name('cho-arrim legate', 'Cho-Arrim Legate').
card_type('cho-arrim legate', 'Creature — Human Soldier').
card_types('cho-arrim legate', ['Creature']).
card_subtypes('cho-arrim legate', ['Human', 'Soldier']).
card_colors('cho-arrim legate', ['W']).
card_text('cho-arrim legate', 'Protection from black\nIf an opponent controls a Swamp and you control a Plains, you may cast Cho-Arrim Legate without paying its mana cost.').
card_mana_cost('cho-arrim legate', ['2', 'W']).
card_cmc('cho-arrim legate', 3).
card_layout('cho-arrim legate', 'normal').
card_power('cho-arrim legate', 1).
card_toughness('cho-arrim legate', 2).

% Found in: MMQ
card_name('cho-manno\'s blessing', 'Cho-Manno\'s Blessing').
card_type('cho-manno\'s blessing', 'Enchantment — Aura').
card_types('cho-manno\'s blessing', ['Enchantment']).
card_subtypes('cho-manno\'s blessing', ['Aura']).
card_colors('cho-manno\'s blessing', ['W']).
card_text('cho-manno\'s blessing', 'Flash\nEnchant creature\nAs Cho-Manno\'s Blessing enters the battlefield, choose a color.\nEnchanted creature has protection from the chosen color. This effect doesn\'t remove Cho-Manno\'s Blessing.').
card_mana_cost('cho-manno\'s blessing', ['W', 'W']).
card_cmc('cho-manno\'s blessing', 2).
card_layout('cho-manno\'s blessing', 'normal').

% Found in: 10E, MMQ
card_name('cho-manno, revolutionary', 'Cho-Manno, Revolutionary').
card_type('cho-manno, revolutionary', 'Legendary Creature — Human Rebel').
card_types('cho-manno, revolutionary', ['Creature']).
card_subtypes('cho-manno, revolutionary', ['Human', 'Rebel']).
card_supertypes('cho-manno, revolutionary', ['Legendary']).
card_colors('cho-manno, revolutionary', ['W']).
card_text('cho-manno, revolutionary', 'Prevent all damage that would be dealt to Cho-Manno, Revolutionary.').
card_mana_cost('cho-manno, revolutionary', ['2', 'W', 'W']).
card_cmc('cho-manno, revolutionary', 4).
card_layout('cho-manno, revolutionary', 'normal').
card_power('cho-manno, revolutionary', 2).
card_toughness('cho-manno, revolutionary', 2).

% Found in: SOK
card_name('choice of damnations', 'Choice of Damnations').
card_type('choice of damnations', 'Sorcery — Arcane').
card_types('choice of damnations', ['Sorcery']).
card_subtypes('choice of damnations', ['Arcane']).
card_colors('choice of damnations', ['B']).
card_text('choice of damnations', 'Target opponent chooses a number. You may have that player lose that much life. If you don\'t, that player sacrifices all but that many permanents.').
card_mana_cost('choice of damnations', ['5', 'B']).
card_cmc('choice of damnations', 6).
card_layout('choice of damnations', 'normal').

% Found in: 8ED, TMP
card_name('choke', 'Choke').
card_type('choke', 'Enchantment').
card_types('choke', ['Enchantment']).
card_subtypes('choke', []).
card_colors('choke', ['G']).
card_text('choke', 'Islands don\'t untap during their controllers\' untap steps.').
card_mana_cost('choke', ['2', 'G']).
card_cmc('choke', 3).
card_layout('choke', 'normal').

% Found in: MBS
card_name('choking fumes', 'Choking Fumes').
card_type('choking fumes', 'Instant').
card_types('choking fumes', ['Instant']).
card_subtypes('choking fumes', []).
card_colors('choking fumes', ['W']).
card_text('choking fumes', 'Put a -1/-1 counter on each attacking creature.').
card_mana_cost('choking fumes', ['2', 'W']).
card_cmc('choking fumes', 3).
card_layout('choking fumes', 'normal').

% Found in: MIR, VMA
card_name('choking sands', 'Choking Sands').
card_type('choking sands', 'Sorcery').
card_types('choking sands', ['Sorcery']).
card_subtypes('choking sands', []).
card_colors('choking sands', ['B']).
card_text('choking sands', 'Destroy target non-Swamp land. If that land was nonbasic, Choking Sands deals 2 damage to the land\'s controller.').
card_mana_cost('choking sands', ['1', 'B', 'B']).
card_cmc('choking sands', 3).
card_layout('choking sands', 'normal').

% Found in: ONS, VMA
card_name('choking tethers', 'Choking Tethers').
card_type('choking tethers', 'Instant').
card_types('choking tethers', ['Instant']).
card_subtypes('choking tethers', []).
card_colors('choking tethers', ['U']).
card_text('choking tethers', 'Tap up to four target creatures.\nCycling {1}{U} ({1}{U}, Discard this card: Draw a card.)\nWhen you cycle Choking Tethers, you may tap target creature.').
card_mana_cost('choking tethers', ['3', 'U']).
card_cmc('choking tethers', 4).
card_layout('choking tethers', 'normal').

% Found in: WTH
card_name('choking vines', 'Choking Vines').
card_type('choking vines', 'Instant').
card_types('choking vines', ['Instant']).
card_subtypes('choking vines', []).
card_colors('choking vines', ['G']).
card_text('choking vines', 'Cast Choking Vines only during the declare blockers step.\nX target attacking creatures become blocked. Choking Vines deals 1 damage to each of those creatures. (This spell works on creatures that can\'t be blocked.)').
card_mana_cost('choking vines', ['X', 'G']).
card_cmc('choking vines', 1).
card_layout('choking vines', 'normal').

% Found in: ARC
card_name('choose your champion', 'Choose Your Champion').
card_type('choose your champion', 'Scheme').
card_types('choose your champion', ['Scheme']).
card_subtypes('choose your champion', []).
card_colors('choose your champion', []).
card_text('choose your champion', 'When you set this scheme in motion, target opponent chooses a player. Until your next turn, only you and the chosen player can cast spells and attack with creatures.').
card_layout('choose your champion', 'scheme').

% Found in: M15, RAV
card_name('chord of calling', 'Chord of Calling').
card_type('chord of calling', 'Instant').
card_types('chord of calling', ['Instant']).
card_subtypes('chord of calling', []).
card_colors('chord of calling', ['G']).
card_text('chord of calling', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nSearch your library for a creature card with converted mana cost X or less and put it onto the battlefield. Then shuffle your library.').
card_mana_cost('chord of calling', ['X', 'G', 'G', 'G']).
card_cmc('chord of calling', 3).
card_layout('chord of calling', 'normal').

% Found in: RTR
card_name('chorus of might', 'Chorus of Might').
card_type('chorus of might', 'Instant').
card_types('chorus of might', ['Instant']).
card_subtypes('chorus of might', []).
card_colors('chorus of might', ['G']).
card_text('chorus of might', 'Until end of turn, target creature gets +1/+1 for each creature you control and gains trample.').
card_mana_cost('chorus of might', ['3', 'G']).
card_cmc('chorus of might', 4).
card_layout('chorus of might', 'normal').

% Found in: CMD, RAV
card_name('chorus of the conclave', 'Chorus of the Conclave').
card_type('chorus of the conclave', 'Legendary Creature — Dryad').
card_types('chorus of the conclave', ['Creature']).
card_subtypes('chorus of the conclave', ['Dryad']).
card_supertypes('chorus of the conclave', ['Legendary']).
card_colors('chorus of the conclave', ['W', 'G']).
card_text('chorus of the conclave', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)\nAs an additional cost to cast creature spells, you may pay any amount of mana. If you do, that creature enters the battlefield with that many additional +1/+1 counters on it.').
card_mana_cost('chorus of the conclave', ['4', 'G', 'G', 'W', 'W']).
card_cmc('chorus of the conclave', 8).
card_layout('chorus of the conclave', 'normal').
card_power('chorus of the conclave', 3).
card_toughness('chorus of the conclave', 8).

% Found in: BNG
card_name('chorus of the tides', 'Chorus of the Tides').
card_type('chorus of the tides', 'Creature — Siren').
card_types('chorus of the tides', ['Creature']).
card_subtypes('chorus of the tides', ['Siren']).
card_colors('chorus of the tides', ['U']).
card_text('chorus of the tides', 'Flying\nHeroic — Whenever you cast a spell that targets Chorus of the Tides, scry 1. (To scry 1, look at the top card of your library, then you may put that card on the bottom of your library.)').
card_mana_cost('chorus of the tides', ['3', 'U']).
card_cmc('chorus of the tides', 4).
card_layout('chorus of the tides', 'normal').
card_power('chorus of the tides', 3).
card_toughness('chorus of the tides', 2).

% Found in: PO2, S99
card_name('chorus of woe', 'Chorus of Woe').
card_type('chorus of woe', 'Sorcery').
card_types('chorus of woe', ['Sorcery']).
card_subtypes('chorus of woe', []).
card_colors('chorus of woe', ['B']).
card_text('chorus of woe', 'Creatures you control get +1/+0 until end of turn.').
card_mana_cost('chorus of woe', ['B']).
card_cmc('chorus of woe', 1).
card_layout('chorus of woe', 'normal').

% Found in: THS
card_name('chosen by heliod', 'Chosen by Heliod').
card_type('chosen by heliod', 'Enchantment — Aura').
card_types('chosen by heliod', ['Enchantment']).
card_subtypes('chosen by heliod', ['Aura']).
card_colors('chosen by heliod', ['W']).
card_text('chosen by heliod', 'Enchant creature\nWhen Chosen by Heliod enters the battlefield, draw a card.\nEnchanted creature gets +0/+2.').
card_mana_cost('chosen by heliod', ['1', 'W']).
card_cmc('chosen by heliod', 2).
card_layout('chosen by heliod', 'normal').

% Found in: DKA
card_name('chosen of markov', 'Chosen of Markov').
card_type('chosen of markov', 'Creature — Human').
card_types('chosen of markov', ['Creature']).
card_subtypes('chosen of markov', ['Human']).
card_colors('chosen of markov', ['B']).
card_text('chosen of markov', '{T}, Tap an untapped Vampire you control: Transform Chosen of Markov.').
card_mana_cost('chosen of markov', ['2', 'B']).
card_cmc('chosen of markov', 3).
card_layout('chosen of markov', 'double-faced').
card_power('chosen of markov', 2).
card_toughness('chosen of markov', 2).
card_sides('chosen of markov', 'markov\'s servant').

% Found in: BNG
card_name('chromanticore', 'Chromanticore').
card_type('chromanticore', 'Enchantment Creature — Manticore').
card_types('chromanticore', ['Enchantment', 'Creature']).
card_subtypes('chromanticore', ['Manticore']).
card_colors('chromanticore', ['W', 'U', 'B', 'R', 'G']).
card_text('chromanticore', 'Bestow {2}{W}{U}{B}{R}{G} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nFlying, first strike, vigilance, trample, lifelink\nEnchanted creature gets +4/+4 and has flying, first strike, vigilance, trample, and lifelink.').
card_mana_cost('chromanticore', ['W', 'U', 'B', 'R', 'G']).
card_cmc('chromanticore', 5).
card_layout('chromanticore', 'normal').
card_power('chromanticore', 4).
card_toughness('chromanticore', 4).

% Found in: ICE
card_name('chromatic armor', 'Chromatic Armor').
card_type('chromatic armor', 'Enchantment — Aura').
card_types('chromatic armor', ['Enchantment']).
card_subtypes('chromatic armor', ['Aura']).
card_colors('chromatic armor', ['W', 'U']).
card_text('chromatic armor', 'Enchant creature\nAs Chromatic Armor enters the battlefield, choose a color.\nChromatic Armor enters the battlefield with a sleight counter on it.\nPrevent all damage that would be dealt to enchanted creature by sources of the last chosen color.\n{X}: Put a sleight counter on Chromatic Armor and choose a color. X is the number of sleight counters on Chromatic Armor.').
card_mana_cost('chromatic armor', ['1', 'W', 'U']).
card_cmc('chromatic armor', 3).
card_layout('chromatic armor', 'normal').
card_reserved('chromatic armor').

% Found in: RTR
card_name('chromatic lantern', 'Chromatic Lantern').
card_type('chromatic lantern', 'Artifact').
card_types('chromatic lantern', ['Artifact']).
card_subtypes('chromatic lantern', []).
card_colors('chromatic lantern', []).
card_text('chromatic lantern', 'Lands you control have \"{T}: Add one mana of any color to your mana pool.\"\n{T}: Add one mana of any color to your mana pool.').
card_mana_cost('chromatic lantern', ['3']).
card_cmc('chromatic lantern', 3).
card_layout('chromatic lantern', 'normal').

% Found in: INV, MRD
card_name('chromatic sphere', 'Chromatic Sphere').
card_type('chromatic sphere', 'Artifact').
card_types('chromatic sphere', ['Artifact']).
card_subtypes('chromatic sphere', []).
card_colors('chromatic sphere', []).
card_text('chromatic sphere', '{1}, {T}, Sacrifice Chromatic Sphere: Add one mana of any color to your mana pool. Draw a card.').
card_mana_cost('chromatic sphere', ['1']).
card_cmc('chromatic sphere', 1).
card_layout('chromatic sphere', 'normal').

% Found in: 10E, TSP
card_name('chromatic star', 'Chromatic Star').
card_type('chromatic star', 'Artifact').
card_types('chromatic star', ['Artifact']).
card_subtypes('chromatic star', []).
card_colors('chromatic star', []).
card_text('chromatic star', '{1}, {T}, Sacrifice Chromatic Star: Add one mana of any color to your mana pool.\nWhen Chromatic Star is put into a graveyard from the battlefield, draw a card.').
card_mana_cost('chromatic star', ['1']).
card_cmc('chromatic star', 1).
card_layout('chromatic star', 'normal').

% Found in: MRD, pGPX
card_name('chrome mox', 'Chrome Mox').
card_type('chrome mox', 'Artifact').
card_types('chrome mox', ['Artifact']).
card_subtypes('chrome mox', []).
card_colors('chrome mox', []).
card_text('chrome mox', 'Imprint — When Chrome Mox enters the battlefield, you may exile a nonartifact, nonland card from your hand.\n{T}: Add one mana of any of the exiled card\'s colors to your mana pool.').
card_mana_cost('chrome mox', ['0']).
card_cmc('chrome mox', 0).
card_layout('chrome mox', 'normal').

% Found in: SOM
card_name('chrome steed', 'Chrome Steed').
card_type('chrome steed', 'Artifact Creature — Horse').
card_types('chrome steed', ['Artifact', 'Creature']).
card_subtypes('chrome steed', ['Horse']).
card_colors('chrome steed', []).
card_text('chrome steed', 'Metalcraft — Chrome Steed gets +2/+2 as long as you control three or more artifacts.').
card_mana_cost('chrome steed', ['4']).
card_cmc('chrome steed', 4).
card_layout('chrome steed', 'normal').
card_power('chrome steed', 2).
card_toughness('chrome steed', 2).

% Found in: DST
card_name('chromescale drake', 'Chromescale Drake').
card_type('chromescale drake', 'Creature — Drake').
card_types('chromescale drake', ['Creature']).
card_subtypes('chromescale drake', ['Drake']).
card_colors('chromescale drake', ['U']).
card_text('chromescale drake', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying\nWhen Chromescale Drake enters the battlefield, reveal the top three cards of your library. Put all artifact cards revealed this way into your hand and the rest into your graveyard.').
card_mana_cost('chromescale drake', ['6', 'U', 'U', 'U']).
card_cmc('chromescale drake', 9).
card_layout('chromescale drake', 'normal').
card_power('chromescale drake', 3).
card_toughness('chromescale drake', 4).

% Found in: CMD, LGN
card_name('chromeshell crab', 'Chromeshell Crab').
card_type('chromeshell crab', 'Creature — Crab Beast').
card_types('chromeshell crab', ['Creature']).
card_subtypes('chromeshell crab', ['Crab', 'Beast']).
card_colors('chromeshell crab', ['U']).
card_text('chromeshell crab', 'Morph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Chromeshell Crab is turned face up, you may exchange control of target creature you control and target creature an opponent controls.').
card_mana_cost('chromeshell crab', ['4', 'U']).
card_cmc('chromeshell crab', 5).
card_layout('chromeshell crab', 'normal').
card_power('chromeshell crab', 3).
card_toughness('chromeshell crab', 3).

% Found in: CHR, LEG, ME3
card_name('chromium', 'Chromium').
card_type('chromium', 'Legendary Creature — Elder Dragon').
card_types('chromium', ['Creature']).
card_subtypes('chromium', ['Elder', 'Dragon']).
card_supertypes('chromium', ['Legendary']).
card_colors('chromium', ['W', 'U', 'B']).
card_text('chromium', 'Flying\nRampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)\nAt the beginning of your upkeep, sacrifice Chromium unless you pay {W}{U}{B}.').
card_mana_cost('chromium', ['2', 'W', 'W', 'U', 'U', 'B', 'B']).
card_cmc('chromium', 8).
card_layout('chromium', 'normal').
card_power('chromium', 7).
card_toughness('chromium', 7).

% Found in: VIS
card_name('chronatog', 'Chronatog').
card_type('chronatog', 'Creature — Atog').
card_types('chronatog', ['Creature']).
card_subtypes('chronatog', ['Atog']).
card_colors('chronatog', ['U']).
card_text('chronatog', '{0}: Chronatog gets +3/+3 until end of turn. You skip your next turn. Activate this ability only once each turn.').
card_mana_cost('chronatog', ['1', 'U']).
card_cmc('chronatog', 2).
card_layout('chronatog', 'normal').
card_power('chronatog', 1).
card_toughness('chronatog', 2).
card_reserved('chronatog').

% Found in: VAN
card_name('chronatog avatar', 'Chronatog Avatar').
card_type('chronatog avatar', 'Vanguard').
card_types('chronatog avatar', ['Vanguard']).
card_subtypes('chronatog avatar', []).
card_colors('chronatog avatar', []).
card_text('chronatog avatar', 'You have no maximum hand size.\n{0}: Draw three cards. You skip your next turn. Activate this ability only once each turn.').
card_layout('chronatog avatar', 'vanguard').

% Found in: TSP
card_name('chronatog totem', 'Chronatog Totem').
card_type('chronatog totem', 'Artifact').
card_types('chronatog totem', ['Artifact']).
card_subtypes('chronatog totem', []).
card_colors('chronatog totem', []).
card_text('chronatog totem', '{T}: Add {U} to your mana pool.\n{1}{U}: Chronatog Totem becomes a 1/2 blue Atog artifact creature until end of turn.\n{0}: Chronatog Totem gets +3/+3 until end of turn. You skip your next turn. Activate this ability only once each turn and only if Chronatog Totem is a creature.').
card_mana_cost('chronatog totem', ['3']).
card_cmc('chronatog totem', 3).
card_layout('chronatog totem', 'normal').

% Found in: RTR
card_name('chronic flooding', 'Chronic Flooding').
card_type('chronic flooding', 'Enchantment — Aura').
card_types('chronic flooding', ['Enchantment']).
card_subtypes('chronic flooding', ['Aura']).
card_colors('chronic flooding', ['U']).
card_text('chronic flooding', 'Enchant land\nWhenever enchanted land becomes tapped, its controller puts the top three cards of his or her library into his or her graveyard.').
card_mana_cost('chronic flooding', ['1', 'U']).
card_cmc('chronic flooding', 2).
card_layout('chronic flooding', 'normal').

% Found in: THS
card_name('chronicler of heroes', 'Chronicler of Heroes').
card_type('chronicler of heroes', 'Creature — Centaur Wizard').
card_types('chronicler of heroes', ['Creature']).
card_subtypes('chronicler of heroes', ['Centaur', 'Wizard']).
card_colors('chronicler of heroes', ['W', 'G']).
card_text('chronicler of heroes', 'When Chronicler of Heroes enters the battlefield, draw a card if you control a creature with a +1/+1 counter on it.').
card_mana_cost('chronicler of heroes', ['1', 'G', 'W']).
card_cmc('chronicler of heroes', 3).
card_layout('chronicler of heroes', 'normal').
card_power('chronicler of heroes', 3).
card_toughness('chronicler of heroes', 3).

% Found in: FUT
card_name('chronomantic escape', 'Chronomantic Escape').
card_type('chronomantic escape', 'Sorcery').
card_types('chronomantic escape', ['Sorcery']).
card_subtypes('chronomantic escape', []).
card_colors('chronomantic escape', ['W']).
card_text('chronomantic escape', 'Until your next turn, creatures can\'t attack you. Exile Chronomantic Escape with three time counters on it.\nSuspend 3—{2}{W} (Rather than cast this card from your hand, you may pay {2}{W} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('chronomantic escape', ['4', 'W', 'W']).
card_cmc('chronomantic escape', 6).
card_layout('chronomantic escape', 'normal').

% Found in: DDM, M13
card_name('chronomaton', 'Chronomaton').
card_type('chronomaton', 'Artifact Creature — Golem').
card_types('chronomaton', ['Artifact', 'Creature']).
card_subtypes('chronomaton', ['Golem']).
card_colors('chronomaton', []).
card_text('chronomaton', '{1}, {T}: Put a +1/+1 counter on Chronomaton.').
card_mana_cost('chronomaton', ['1']).
card_cmc('chronomaton', 1).
card_layout('chronomaton', 'normal').
card_power('chronomaton', 1).
card_toughness('chronomaton', 1).

% Found in: TSP
card_name('chronosavant', 'Chronosavant').
card_type('chronosavant', 'Creature — Giant').
card_types('chronosavant', ['Creature']).
card_subtypes('chronosavant', ['Giant']).
card_colors('chronosavant', ['W']).
card_text('chronosavant', '{1}{W}: Return Chronosavant from your graveyard to the battlefield tapped. You skip your next turn.').
card_mana_cost('chronosavant', ['5', 'W']).
card_cmc('chronosavant', 6).
card_layout('chronosavant', 'normal').
card_power('chronosavant', 5).
card_toughness('chronosavant', 5).

% Found in: M15
card_name('chronostutter', 'Chronostutter').
card_type('chronostutter', 'Instant').
card_types('chronostutter', ['Instant']).
card_subtypes('chronostutter', []).
card_colors('chronostutter', ['U']).
card_text('chronostutter', 'Put target creature into its owner\'s library second from the top.').
card_mana_cost('chronostutter', ['5', 'U']).
card_cmc('chronostutter', 6).
card_layout('chronostutter', 'normal').

% Found in: PLC
card_name('chronozoa', 'Chronozoa').
card_type('chronozoa', 'Creature — Illusion').
card_types('chronozoa', ['Creature']).
card_subtypes('chronozoa', ['Illusion']).
card_colors('chronozoa', ['U']).
card_text('chronozoa', 'Flying\nVanishing 3 (This permanent enters the battlefield with three time counters on it. At the beginning of your upkeep, remove a time counter from it. When the last is removed, sacrifice it.)\nWhen Chronozoa dies, if it had no time counters on it, put two tokens that are copies of it onto the battlefield.').
card_mana_cost('chronozoa', ['3', 'U']).
card_cmc('chronozoa', 4).
card_layout('chronozoa', 'normal').
card_power('chronozoa', 3).
card_toughness('chronozoa', 3).

% Found in: 5ED, ICE, MED
card_name('chub toad', 'Chub Toad').
card_type('chub toad', 'Creature — Frog').
card_types('chub toad', ['Creature']).
card_subtypes('chub toad', ['Frog']).
card_colors('chub toad', ['G']).
card_text('chub toad', 'Whenever Chub Toad blocks or becomes blocked, it gets +2/+2 until end of turn.').
card_mana_cost('chub toad', ['2', 'G']).
card_cmc('chub toad', 3).
card_layout('chub toad', 'normal').
card_power('chub toad', 1).
card_toughness('chub toad', 1).

% Found in: TOR
card_name('churning eddy', 'Churning Eddy').
card_type('churning eddy', 'Sorcery').
card_types('churning eddy', ['Sorcery']).
card_subtypes('churning eddy', []).
card_colors('churning eddy', ['U']).
card_text('churning eddy', 'Return target creature and target land to their owners\' hands.').
card_mana_cost('churning eddy', ['3', 'U']).
card_cmc('churning eddy', 4).
card_layout('churning eddy', 'normal').

% Found in: MIR
card_name('cinder cloud', 'Cinder Cloud').
card_type('cinder cloud', 'Instant').
card_types('cinder cloud', ['Instant']).
card_subtypes('cinder cloud', []).
card_colors('cinder cloud', ['R']).
card_text('cinder cloud', 'Destroy target creature. If a white creature dies this way, Cinder Cloud deals damage to that creature\'s controller equal to the creature\'s power.').
card_mana_cost('cinder cloud', ['3', 'R', 'R']).
card_cmc('cinder cloud', 5).
card_layout('cinder cloud', 'normal').

% Found in: EXO
card_name('cinder crawler', 'Cinder Crawler').
card_type('cinder crawler', 'Creature — Salamander').
card_types('cinder crawler', ['Creature']).
card_subtypes('cinder crawler', ['Salamander']).
card_colors('cinder crawler', ['R']).
card_text('cinder crawler', '{R}: Cinder Crawler gets +1/+0 until end of turn. Activate this ability only if Cinder Crawler is blocked.').
card_mana_cost('cinder crawler', ['1', 'R']).
card_cmc('cinder crawler', 2).
card_layout('cinder crawler', 'normal').
card_power('cinder crawler', 1).
card_toughness('cinder crawler', 2).

% Found in: GTC, HOP, MMQ
card_name('cinder elemental', 'Cinder Elemental').
card_type('cinder elemental', 'Creature — Elemental').
card_types('cinder elemental', ['Creature']).
card_subtypes('cinder elemental', ['Elemental']).
card_colors('cinder elemental', ['R']).
card_text('cinder elemental', '{X}{R}, {T}, Sacrifice Cinder Elemental: Cinder Elemental deals X damage to target creature or player.').
card_mana_cost('cinder elemental', ['3', 'R']).
card_cmc('cinder elemental', 4).
card_layout('cinder elemental', 'normal').
card_power('cinder elemental', 2).
card_toughness('cinder elemental', 2).

% Found in: WTH
card_name('cinder giant', 'Cinder Giant').
card_type('cinder giant', 'Creature — Giant').
card_types('cinder giant', ['Creature']).
card_subtypes('cinder giant', ['Giant']).
card_colors('cinder giant', ['R']).
card_text('cinder giant', 'At the beginning of your upkeep, Cinder Giant deals 2 damage to each other creature you control.').
card_mana_cost('cinder giant', ['3', 'R']).
card_cmc('cinder giant', 4).
card_layout('cinder giant', 'normal').
card_power('cinder giant', 5).
card_toughness('cinder giant', 3).

% Found in: BFZ, EXP
card_name('cinder glade', 'Cinder Glade').
card_type('cinder glade', 'Land — Mountain Forest').
card_types('cinder glade', ['Land']).
card_subtypes('cinder glade', ['Mountain', 'Forest']).
card_colors('cinder glade', []).
card_text('cinder glade', '({T}: Add {R} or {G} to your mana pool.)\nCinder Glade enters the battlefield tapped unless you control two or more basic lands.').
card_layout('cinder glade', 'normal').

% Found in: BRB, TMP, TPR
card_name('cinder marsh', 'Cinder Marsh').
card_type('cinder marsh', 'Land').
card_types('cinder marsh', ['Land']).
card_subtypes('cinder marsh', []).
card_colors('cinder marsh', []).
card_text('cinder marsh', '{T}: Add {1} to your mana pool.\n{T}: Add {B} or {R} to your mana pool. Cinder Marsh doesn\'t untap during your next untap step.').
card_layout('cinder marsh', 'normal').

% Found in: DPA, EVE, PD2
card_name('cinder pyromancer', 'Cinder Pyromancer').
card_type('cinder pyromancer', 'Creature — Elemental Shaman').
card_types('cinder pyromancer', ['Creature']).
card_subtypes('cinder pyromancer', ['Elemental', 'Shaman']).
card_colors('cinder pyromancer', ['R']).
card_text('cinder pyromancer', '{T}: Cinder Pyromancer deals 1 damage to target player.\nWhenever you cast a red spell, you may untap Cinder Pyromancer.').
card_mana_cost('cinder pyromancer', ['2', 'R']).
card_cmc('cinder pyromancer', 3).
card_layout('cinder pyromancer', 'normal').
card_power('cinder pyromancer', 0).
card_toughness('cinder pyromancer', 1).

% Found in: UDS
card_name('cinder seer', 'Cinder Seer').
card_type('cinder seer', 'Creature — Human Wizard').
card_types('cinder seer', ['Creature']).
card_subtypes('cinder seer', ['Human', 'Wizard']).
card_colors('cinder seer', ['R']).
card_text('cinder seer', '{2}{R}, {T}: Reveal any number of red cards in your hand. Cinder Seer deals X damage to target creature or player, where X is the number of cards revealed this way.').
card_mana_cost('cinder seer', ['3', 'R']).
card_cmc('cinder seer', 4).
card_layout('cinder seer', 'normal').
card_power('cinder seer', 1).
card_toughness('cinder seer', 1).

% Found in: INV
card_name('cinder shade', 'Cinder Shade').
card_type('cinder shade', 'Creature — Shade').
card_types('cinder shade', ['Creature']).
card_subtypes('cinder shade', ['Shade']).
card_colors('cinder shade', ['B', 'R']).
card_text('cinder shade', '{B}: Cinder Shade gets +1/+1 until end of turn.\n{R}, Sacrifice Cinder Shade: Cinder Shade deals damage equal to its power to target creature.').
card_mana_cost('cinder shade', ['1', 'B', 'R']).
card_cmc('cinder shade', 3).
card_layout('cinder shade', 'normal').
card_power('cinder shade', 1).
card_toughness('cinder shade', 1).

% Found in: ME3, S99
card_name('cinder storm', 'Cinder Storm').
card_type('cinder storm', 'Sorcery').
card_types('cinder storm', ['Sorcery']).
card_subtypes('cinder storm', []).
card_colors('cinder storm', ['R']).
card_text('cinder storm', 'Cinder Storm deals 7 damage to target creature or player.').
card_mana_cost('cinder storm', ['6', 'R']).
card_cmc('cinder storm', 7).
card_layout('cinder storm', 'normal').

% Found in: 8ED, CNS, DDG, WTH
card_name('cinder wall', 'Cinder Wall').
card_type('cinder wall', 'Creature — Wall').
card_types('cinder wall', ['Creature']).
card_subtypes('cinder wall', ['Wall']).
card_colors('cinder wall', ['R']).
card_text('cinder wall', 'Defender\nWhen Cinder Wall blocks, destroy it at end of combat.').
card_mana_cost('cinder wall', ['R']).
card_cmc('cinder wall', 1).
card_layout('cinder wall', 'normal').
card_power('cinder wall', 3).
card_toughness('cinder wall', 3).

% Found in: SHM
card_name('cinderbones', 'Cinderbones').
card_type('cinderbones', 'Creature — Elemental Skeleton').
card_types('cinderbones', ['Creature']).
card_subtypes('cinderbones', ['Elemental', 'Skeleton']).
card_colors('cinderbones', ['B']).
card_text('cinderbones', 'Wither (This deals damage to creatures in the form of -1/-1 counters.)\n{1}{B}: Regenerate Cinderbones.').
card_mana_cost('cinderbones', ['2', 'B']).
card_cmc('cinderbones', 3).
card_layout('cinderbones', 'normal').
card_power('cinderbones', 1).
card_toughness('cinderbones', 1).

% Found in: SHM
card_name('cinderhaze wretch', 'Cinderhaze Wretch').
card_type('cinderhaze wretch', 'Creature — Elemental Shaman').
card_types('cinderhaze wretch', ['Creature']).
card_subtypes('cinderhaze wretch', ['Elemental', 'Shaman']).
card_colors('cinderhaze wretch', ['B']).
card_text('cinderhaze wretch', '{T}: Target player discards a card. Activate this ability only during your turn.\nPut a -1/-1 counter on Cinderhaze Wretch: Untap Cinderhaze Wretch.').
card_mana_cost('cinderhaze wretch', ['4', 'B']).
card_cmc('cinderhaze wretch', 5).
card_layout('cinderhaze wretch', 'normal').
card_power('cinderhaze wretch', 3).
card_toughness('cinderhaze wretch', 2).

% Found in: PLC
card_name('circle of affliction', 'Circle of Affliction').
card_type('circle of affliction', 'Enchantment').
card_types('circle of affliction', ['Enchantment']).
card_subtypes('circle of affliction', []).
card_colors('circle of affliction', ['B']).
card_text('circle of affliction', 'As Circle of Affliction enters the battlefield, choose a color.\nWhenever a source of the chosen color deals damage to you, you may pay {1}. If you do, target player loses 1 life and you gain 1 life.').
card_mana_cost('circle of affliction', ['1', 'B']).
card_cmc('circle of affliction', 2).
card_layout('circle of affliction', 'normal').

% Found in: MIR
card_name('circle of despair', 'Circle of Despair').
card_type('circle of despair', 'Enchantment').
card_types('circle of despair', ['Enchantment']).
card_subtypes('circle of despair', []).
card_colors('circle of despair', ['W', 'B']).
card_text('circle of despair', '{1}, Sacrifice a creature: The next time a source of your choice would deal damage to target creature or player this turn, prevent that damage.').
card_mana_cost('circle of despair', ['1', 'W', 'B']).
card_cmc('circle of despair', 3).
card_layout('circle of despair', 'normal').
card_reserved('circle of despair').

% Found in: DTK
card_name('circle of elders', 'Circle of Elders').
card_type('circle of elders', 'Creature — Human Shaman').
card_types('circle of elders', ['Creature']).
card_subtypes('circle of elders', ['Human', 'Shaman']).
card_colors('circle of elders', ['G']).
card_text('circle of elders', 'Vigilance\nFormidable — {T}: Add {3} to your mana pool. Activate this ability only if creatures you control have total power 8 or greater.').
card_mana_cost('circle of elders', ['2', 'G', 'G']).
card_cmc('circle of elders', 4).
card_layout('circle of elders', 'normal').
card_power('circle of elders', 2).
card_toughness('circle of elders', 4).

% Found in: M12, M15, pWPN
card_name('circle of flame', 'Circle of Flame').
card_type('circle of flame', 'Enchantment').
card_types('circle of flame', ['Enchantment']).
card_subtypes('circle of flame', []).
card_colors('circle of flame', ['R']).
card_text('circle of flame', 'Whenever a creature without flying attacks you or a planeswalker you control, Circle of Flame deals 1 damage to that creature.').
card_mana_cost('circle of flame', ['1', 'R']).
card_cmc('circle of flame', 2).
card_layout('circle of flame', 'normal').

% Found in: UNH, pARL
card_name('circle of protection: art', 'Circle of Protection: Art').
card_type('circle of protection: art', 'Enchantment').
card_types('circle of protection: art', ['Enchantment']).
card_subtypes('circle of protection: art', []).
card_colors('circle of protection: art', ['W']).
card_text('circle of protection: art', 'As Circle of Protection: Art comes into play, choose an artist.\n{1}{W}: The next time a source of your choice by the chosen artist would deal damage to you this turn, prevent that damage.\n{1}{W}: Return Circle of Protection: Art to its owner\'s hand.').
card_mana_cost('circle of protection: art', ['1', 'W']).
card_cmc('circle of protection: art', 2).
card_layout('circle of protection: art', 'normal').

% Found in: 4ED, 5DN, 5ED, ATQ
card_name('circle of protection: artifacts', 'Circle of Protection: Artifacts').
card_type('circle of protection: artifacts', 'Enchantment').
card_types('circle of protection: artifacts', ['Enchantment']).
card_subtypes('circle of protection: artifacts', []).
card_colors('circle of protection: artifacts', ['W']).
card_text('circle of protection: artifacts', '{2}: The next time an artifact source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of protection: artifacts', ['1', 'W']).
card_cmc('circle of protection: artifacts', 2).
card_layout('circle of protection: artifacts', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ICE, ITP, LEB, RQS, TMP
card_name('circle of protection: black', 'Circle of Protection: Black').
card_type('circle of protection: black', 'Enchantment').
card_types('circle of protection: black', ['Enchantment']).
card_subtypes('circle of protection: black', []).
card_colors('circle of protection: black', ['W']).
card_text('circle of protection: black', '{1}: The next time a black source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of protection: black', ['1', 'W']).
card_cmc('circle of protection: black', 2).
card_layout('circle of protection: black', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, ICE, LEA, LEB, TMP
card_name('circle of protection: blue', 'Circle of Protection: Blue').
card_type('circle of protection: blue', 'Enchantment').
card_types('circle of protection: blue', ['Enchantment']).
card_subtypes('circle of protection: blue', []).
card_colors('circle of protection: blue', ['W']).
card_text('circle of protection: blue', '{1}: The next time a blue source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of protection: blue', ['1', 'W']).
card_cmc('circle of protection: blue', 2).
card_layout('circle of protection: blue', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, ICE, LEA, LEB, TMP
card_name('circle of protection: green', 'Circle of Protection: Green').
card_type('circle of protection: green', 'Enchantment').
card_types('circle of protection: green', ['Enchantment']).
card_subtypes('circle of protection: green', []).
card_colors('circle of protection: green', ['W']).
card_text('circle of protection: green', '{1}: The next time a green source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of protection: green', ['1', 'W']).
card_cmc('circle of protection: green', 2).
card_layout('circle of protection: green', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, 9ED, CED, CEI, ICE, ITP, LEA, LEB, RQS, TMP, pFNM
card_name('circle of protection: red', 'Circle of Protection: Red').
card_type('circle of protection: red', 'Enchantment').
card_types('circle of protection: red', ['Enchantment']).
card_subtypes('circle of protection: red', []).
card_colors('circle of protection: red', ['W']).
card_text('circle of protection: red', '{1}: The next time a red source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of protection: red', ['1', 'W']).
card_cmc('circle of protection: red', 2).
card_layout('circle of protection: red', 'normal').

% Found in: TMP
card_name('circle of protection: shadow', 'Circle of Protection: Shadow').
card_type('circle of protection: shadow', 'Enchantment').
card_types('circle of protection: shadow', ['Enchantment']).
card_subtypes('circle of protection: shadow', []).
card_colors('circle of protection: shadow', ['W']).
card_text('circle of protection: shadow', '{1}: The next time a creature of your choice with shadow would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of protection: shadow', ['1', 'W']).
card_cmc('circle of protection: shadow', 2).
card_layout('circle of protection: shadow', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, ICE, LEA, LEB, TMP
card_name('circle of protection: white', 'Circle of Protection: White').
card_type('circle of protection: white', 'Enchantment').
card_types('circle of protection: white', ['Enchantment']).
card_subtypes('circle of protection: white', []).
card_colors('circle of protection: white', ['W']).
card_text('circle of protection: white', '{1}: The next time a white source of your choice would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of protection: white', ['1', 'W']).
card_cmc('circle of protection: white', 2).
card_layout('circle of protection: white', 'normal').

% Found in: ONS
card_name('circle of solace', 'Circle of Solace').
card_type('circle of solace', 'Enchantment').
card_types('circle of solace', ['Enchantment']).
card_subtypes('circle of solace', []).
card_colors('circle of solace', ['W']).
card_text('circle of solace', 'As Circle of Solace enters the battlefield, choose a creature type.\n{1}{W}: The next time a creature of the chosen type would deal damage to you this turn, prevent that damage.').
card_mana_cost('circle of solace', ['3', 'W']).
card_cmc('circle of solace', 4).
card_layout('circle of solace', 'normal').

% Found in: WTH
card_name('circling vultures', 'Circling Vultures').
card_type('circling vultures', 'Creature — Bird').
card_types('circling vultures', ['Creature']).
card_subtypes('circling vultures', ['Bird']).
card_colors('circling vultures', ['B']).
card_text('circling vultures', 'Flying\nYou may discard Circling Vultures any time you could cast an instant.\nAt the beginning of your upkeep, sacrifice Circling Vultures unless you exile the top creature card of your graveyard.').
card_mana_cost('circling vultures', ['B']).
card_cmc('circling vultures', 1).
card_layout('circling vultures', 'normal').
card_power('circling vultures', 3).
card_toughness('circling vultures', 2).

% Found in: RAV
card_name('circu, dimir lobotomist', 'Circu, Dimir Lobotomist').
card_type('circu, dimir lobotomist', 'Legendary Creature — Human Wizard').
card_types('circu, dimir lobotomist', ['Creature']).
card_subtypes('circu, dimir lobotomist', ['Human', 'Wizard']).
card_supertypes('circu, dimir lobotomist', ['Legendary']).
card_colors('circu, dimir lobotomist', ['U', 'B']).
card_text('circu, dimir lobotomist', 'Whenever you cast a blue spell, exile the top card of target library.\nWhenever you cast a black spell, exile the top card of target library.\nYour opponents can\'t cast nonland cards with the same name as a card exiled with Circu, Dimir Lobotomist.').
card_mana_cost('circu, dimir lobotomist', ['2', 'U', 'B']).
card_cmc('circu, dimir lobotomist', 4).
card_layout('circu, dimir lobotomist', 'normal').
card_power('circu, dimir lobotomist', 2).
card_toughness('circu, dimir lobotomist', 3).

% Found in: TOR, VMA, pFNM
card_name('circular logic', 'Circular Logic').
card_type('circular logic', 'Instant').
card_types('circular logic', ['Instant']).
card_subtypes('circular logic', []).
card_colors('circular logic', ['U']).
card_text('circular logic', 'Counter target spell unless its controller pays {1} for each card in your graveyard.\nMadness {U} (If you discard this card, you may cast it for its madness cost instead of putting it into your graveyard.)').
card_mana_cost('circular logic', ['2', 'U']).
card_cmc('circular logic', 3).
card_layout('circular logic', 'normal').

% Found in: ORI
card_name('citadel castellan', 'Citadel Castellan').
card_type('citadel castellan', 'Creature — Human Knight').
card_types('citadel castellan', ['Creature']).
card_subtypes('citadel castellan', ['Human', 'Knight']).
card_colors('citadel castellan', ['W', 'G']).
card_text('citadel castellan', 'Vigilance (Attacking doesn\'t cause this creature to tap.)\nRenown 2 (When this creature deals combat damage to a player, if it isn\'t renowned, put two +1/+1 counters on it and it becomes renowned.)').
card_mana_cost('citadel castellan', ['1', 'G', 'W']).
card_cmc('citadel castellan', 3).
card_layout('citadel castellan', 'normal').
card_power('citadel castellan', 2).
card_toughness('citadel castellan', 3).

% Found in: PCY
card_name('citadel of pain', 'Citadel of Pain').
card_type('citadel of pain', 'Enchantment').
card_types('citadel of pain', ['Enchantment']).
card_subtypes('citadel of pain', []).
card_colors('citadel of pain', ['R']).
card_text('citadel of pain', 'At the beginning of each player\'s end step, Citadel of Pain deals X damage to that player, where X is the number of untapped lands he or she controls.').
card_mana_cost('citadel of pain', ['2', 'R']).
card_cmc('citadel of pain', 3).
card_layout('citadel of pain', 'normal').

% Found in: FRF
card_name('citadel siege', 'Citadel Siege').
card_type('citadel siege', 'Enchantment').
card_types('citadel siege', ['Enchantment']).
card_subtypes('citadel siege', []).
card_colors('citadel siege', ['W']).
card_text('citadel siege', 'As Citadel Siege enters the battlefield, choose Khans or Dragons.\n• Khans — At the beginning of combat on your turn, put two +1/+1 counters on target creature you control.\n• Dragons — At the beginning of combat on each opponent\'s turn, tap target creature that player controls.').
card_mana_cost('citadel siege', ['2', 'W', 'W']).
card_cmc('citadel siege', 4).
card_layout('citadel siege', 'normal').

% Found in: USG
card_name('citanul centaurs', 'Citanul Centaurs').
card_type('citanul centaurs', 'Creature — Centaur').
card_types('citanul centaurs', ['Creature']).
card_subtypes('citanul centaurs', ['Centaur']).
card_colors('citanul centaurs', ['G']).
card_text('citanul centaurs', 'Shroud (This creature can\'t be the target of spells or abilities.)\nEcho {3}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('citanul centaurs', ['3', 'G']).
card_cmc('citanul centaurs', 4).
card_layout('citanul centaurs', 'normal').
card_power('citanul centaurs', 6).
card_toughness('citanul centaurs', 3).
card_reserved('citanul centaurs').

% Found in: ATQ, ME4
card_name('citanul druid', 'Citanul Druid').
card_type('citanul druid', 'Creature — Human Druid').
card_types('citanul druid', ['Creature']).
card_subtypes('citanul druid', ['Human', 'Druid']).
card_colors('citanul druid', ['G']).
card_text('citanul druid', 'Whenever an opponent casts an artifact spell, put a +1/+1 counter on Citanul Druid.').
card_mana_cost('citanul druid', ['1', 'G']).
card_cmc('citanul druid', 2).
card_layout('citanul druid', 'normal').
card_power('citanul druid', 1).
card_toughness('citanul druid', 1).
card_reserved('citanul druid').

% Found in: 10E, USG
card_name('citanul flute', 'Citanul Flute').
card_type('citanul flute', 'Artifact').
card_types('citanul flute', ['Artifact']).
card_subtypes('citanul flute', []).
card_colors('citanul flute', []).
card_text('citanul flute', '{X}, {T}: Search your library for a creature card with converted mana cost X or less, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('citanul flute', ['5']).
card_cmc('citanul flute', 5).
card_layout('citanul flute', 'normal').

% Found in: USG
card_name('citanul hierophants', 'Citanul Hierophants').
card_type('citanul hierophants', 'Creature — Human Druid').
card_types('citanul hierophants', ['Creature']).
card_subtypes('citanul hierophants', ['Human', 'Druid']).
card_colors('citanul hierophants', ['G']).
card_text('citanul hierophants', 'Creatures you control have \"{T}: Add {G} to your mana pool.\"').
card_mana_cost('citanul hierophants', ['3', 'G']).
card_cmc('citanul hierophants', 4).
card_layout('citanul hierophants', 'normal').
card_power('citanul hierophants', 3).
card_toughness('citanul hierophants', 2).

% Found in: MMA, PLC
card_name('citanul woodreaders', 'Citanul Woodreaders').
card_type('citanul woodreaders', 'Creature — Human Druid').
card_types('citanul woodreaders', ['Creature']).
card_subtypes('citanul woodreaders', ['Human', 'Druid']).
card_colors('citanul woodreaders', ['G']).
card_text('citanul woodreaders', 'Kicker {2}{G} (You may pay an additional {2}{G} as you cast this spell.)\nWhen Citanul Woodreaders enters the battlefield, if it was kicked, draw two cards.').
card_mana_cost('citanul woodreaders', ['2', 'G']).
card_cmc('citanul woodreaders', 3).
card_layout('citanul woodreaders', 'normal').
card_power('citanul woodreaders', 1).
card_toughness('citanul woodreaders', 4).

% Found in: ARN, VMA
card_name('city in a bottle', 'City in a Bottle').
card_type('city in a bottle', 'Artifact').
card_types('city in a bottle', ['Artifact']).
card_subtypes('city in a bottle', []).
card_colors('city in a bottle', []).
card_text('city in a bottle', 'Whenever a nontoken permanent originally printed in the Arabian Nights expansion other than City in a Bottle is on the battlefield, its controller sacrifices it.\nPlayers can\'t play cards originally printed in the Arabian Nights expansion.').
card_mana_cost('city in a bottle', ['2']).
card_cmc('city in a bottle', 2).
card_layout('city in a bottle', 'normal').
card_reserved('city in a bottle').

% Found in: UNH
card_name('city of ass', 'City of Ass').
card_type('city of ass', 'Land').
card_types('city of ass', ['Land']).
card_subtypes('city of ass', []).
card_colors('city of ass', []).
card_text('city of ass', 'City of Ass comes into play tapped.\n{T}: Add one and one-half mana of any one color to your mana pool.').
card_layout('city of ass', 'normal').

% Found in: 5ED, 6ED, 7ED, 8ED, ARN, CHR, MD1, ME4, MMA, pSUS
card_name('city of brass', 'City of Brass').
card_type('city of brass', 'Land').
card_types('city of brass', ['Land']).
card_subtypes('city of brass', []).
card_colors('city of brass', []).
card_text('city of brass', 'Whenever City of Brass becomes tapped, it deals 1 damage to you.\n{T}: Add one mana of any color to your mana pool.').
card_layout('city of brass', 'normal').

% Found in: DRK, ME3
card_name('city of shadows', 'City of Shadows').
card_type('city of shadows', 'Land').
card_types('city of shadows', ['Land']).
card_subtypes('city of shadows', []).
card_colors('city of shadows', []).
card_text('city of shadows', '{T}, Exile a creature you control: Put a storage counter on City of Shadows.\n{T}: Add {X} to your mana pool, where X is the number of storage counters on City of Shadows.').
card_layout('city of shadows', 'normal').
card_reserved('city of shadows').

% Found in: VIS
card_name('city of solitude', 'City of Solitude').
card_type('city of solitude', 'Enchantment').
card_types('city of solitude', ['Enchantment']).
card_subtypes('city of solitude', []).
card_colors('city of solitude', ['G']).
card_text('city of solitude', 'Players can cast spells and activate abilities only during their own turns.').
card_mana_cost('city of solitude', ['2', 'G']).
card_cmc('city of solitude', 3).
card_layout('city of solitude', 'normal').
card_reserved('city of solitude').

% Found in: EXO, TPR
card_name('city of traitors', 'City of Traitors').
card_type('city of traitors', 'Land').
card_types('city of traitors', ['Land']).
card_subtypes('city of traitors', []).
card_colors('city of traitors', []).
card_text('city of traitors', 'When you play another land, sacrifice City of Traitors.\n{T}: Add {2} to your mana pool.').
card_layout('city of traitors', 'normal').
card_reserved('city of traitors').

% Found in: MIR
card_name('civic guildmage', 'Civic Guildmage').
card_type('civic guildmage', 'Creature — Human Wizard').
card_types('civic guildmage', ['Creature']).
card_subtypes('civic guildmage', ['Human', 'Wizard']).
card_colors('civic guildmage', ['W']).
card_text('civic guildmage', '{G}, {T}: Target creature gets +0/+1 until end of turn.\n{U}, {T}: Put target creature you control on top of its owner\'s library.').
card_mana_cost('civic guildmage', ['W']).
card_cmc('civic guildmage', 1).
card_layout('civic guildmage', 'normal').
card_power('civic guildmage', 1).
card_toughness('civic guildmage', 1).

% Found in: RTR
card_name('civic saber', 'Civic Saber').
card_type('civic saber', 'Artifact — Equipment').
card_types('civic saber', ['Artifact']).
card_subtypes('civic saber', ['Equipment']).
card_colors('civic saber', []).
card_text('civic saber', 'Equipped creature gets +1/+0 for each of its colors.\nEquip {1}').
card_mana_cost('civic saber', ['1']).
card_cmc('civic saber', 1).
card_layout('civic saber', 'normal').

% Found in: 10E, DPA, RAV
card_name('civic wayfinder', 'Civic Wayfinder').
card_type('civic wayfinder', 'Creature — Elf Warrior Druid').
card_types('civic wayfinder', ['Creature']).
card_subtypes('civic wayfinder', ['Elf', 'Warrior', 'Druid']).
card_colors('civic wayfinder', ['G']).
card_text('civic wayfinder', 'When Civic Wayfinder enters the battlefield, you may search your library for a basic land card, reveal it, and put it into your hand. If you do, shuffle your library.').
card_mana_cost('civic wayfinder', ['2', 'G']).
card_cmc('civic wayfinder', 3).
card_layout('civic wayfinder', 'normal').
card_power('civic wayfinder', 2).
card_toughness('civic wayfinder', 2).

% Found in: ISD
card_name('civilized scholar', 'Civilized Scholar').
card_type('civilized scholar', 'Creature — Human Advisor').
card_types('civilized scholar', ['Creature']).
card_subtypes('civilized scholar', ['Human', 'Advisor']).
card_colors('civilized scholar', ['U']).
card_text('civilized scholar', '{T}: Draw a card, then discard a card. If a creature card is discarded this way, untap Civilized Scholar, then transform it.').
card_mana_cost('civilized scholar', ['2', 'U']).
card_cmc('civilized scholar', 3).
card_layout('civilized scholar', 'double-faced').
card_power('civilized scholar', 0).
card_toughness('civilized scholar', 1).
card_sides('civilized scholar', 'homicidal brute').

% Found in: BNG
card_name('claim of erebos', 'Claim of Erebos').
card_type('claim of erebos', 'Enchantment — Aura').
card_types('claim of erebos', ['Enchantment']).
card_subtypes('claim of erebos', ['Aura']).
card_colors('claim of erebos', ['B']).
card_text('claim of erebos', 'Enchant creature\nEnchanted creature has \"{1}{B}, {T}: Target player loses 2 life.\"').
card_mana_cost('claim of erebos', ['1', 'B']).
card_cmc('claim of erebos', 2).
card_layout('claim of erebos', 'normal').

% Found in: ICE
card_name('clairvoyance', 'Clairvoyance').
card_type('clairvoyance', 'Instant').
card_types('clairvoyance', ['Instant']).
card_subtypes('clairvoyance', []).
card_colors('clairvoyance', ['U']).
card_text('clairvoyance', 'Look at target player\'s hand.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('clairvoyance', ['U']).
card_cmc('clairvoyance', 1).
card_layout('clairvoyance', 'normal').

% Found in: UGL
card_name('clam session', 'Clam Session').
card_type('clam session', 'Creature — Clamfolk').
card_types('clam session', ['Creature']).
card_subtypes('clam session', ['Clamfolk']).
card_colors('clam session', ['U']).
card_text('clam session', 'When Clam Session comes into play, choose a word. \nDuring your upkeep, sing at least six words of a song, one of which must be the chosen word, or sacrifice Clam Session. You cannot repeat a song.').
card_mana_cost('clam session', ['1', 'U', 'U']).
card_cmc('clam session', 3).
card_layout('clam session', 'normal').
card_power('clam session', 2).
card_toughness('clam session', 5).

% Found in: UGL
card_name('clam-i-am', 'Clam-I-Am').
card_type('clam-i-am', 'Creature — Clamfolk').
card_types('clam-i-am', ['Creature']).
card_subtypes('clam-i-am', ['Clamfolk']).
card_colors('clam-i-am', ['U']).
card_text('clam-i-am', 'Whenever you roll a 3 on a six-sided die, you may reroll that die.').
card_mana_cost('clam-i-am', ['2', 'U']).
card_cmc('clam-i-am', 3).
card_layout('clam-i-am', 'normal').
card_power('clam-i-am', 2).
card_toughness('clam-i-am', 2).

% Found in: UGL
card_name('clambassadors', 'Clambassadors').
card_type('clambassadors', 'Creature — Clamfolk').
card_types('clambassadors', ['Creature']).
card_subtypes('clambassadors', ['Clamfolk']).
card_colors('clambassadors', ['U']).
card_text('clambassadors', 'If Clambassadors damages any player, choose an artifact, creature, or land you control. That player gains control of that artifact, creature, or land.').
card_mana_cost('clambassadors', ['3', 'U']).
card_cmc('clambassadors', 4).
card_layout('clambassadors', 'normal').
card_power('clambassadors', 4).
card_toughness('clambassadors', 4).

% Found in: GTC
card_name('clan defiance', 'Clan Defiance').
card_type('clan defiance', 'Sorcery').
card_types('clan defiance', ['Sorcery']).
card_subtypes('clan defiance', []).
card_colors('clan defiance', ['R', 'G']).
card_text('clan defiance', 'Choose one or more —\n• Clan Defiance deals X damage to target creature with flying.\n• Clan Defiance deals X damage to target creature without flying.\n• Clan Defiance deals X damage to target player.').
card_mana_cost('clan defiance', ['X', 'R', 'G']).
card_cmc('clan defiance', 2).
card_layout('clan defiance', 'normal').

% Found in: ALA
card_name('clarion ultimatum', 'Clarion Ultimatum').
card_type('clarion ultimatum', 'Sorcery').
card_types('clarion ultimatum', ['Sorcery']).
card_subtypes('clarion ultimatum', []).
card_colors('clarion ultimatum', ['W', 'U', 'G']).
card_text('clarion ultimatum', 'Choose five permanents you control. For each of those permanents, you may search your library for a card with the same name as that permanent. Put those cards onto the battlefield tapped, then shuffle your library.').
card_mana_cost('clarion ultimatum', ['G', 'G', 'W', 'W', 'W', 'U', 'U']).
card_cmc('clarion ultimatum', 7).
card_layout('clarion ultimatum', 'normal').

% Found in: BOK
card_name('clash of realities', 'Clash of Realities').
card_type('clash of realities', 'Enchantment').
card_types('clash of realities', ['Enchantment']).
card_subtypes('clash of realities', []).
card_colors('clash of realities', ['R']).
card_text('clash of realities', 'All Spirits have \"When this permanent enters the battlefield, you may have it deal 3 damage to target non-Spirit creature.\"\nNon-Spirit creatures have \"When this creature enters the battlefield, you may have it deal 3 damage to target Spirit creature.\"').
card_mana_cost('clash of realities', ['3', 'R']).
card_cmc('clash of realities', 4).
card_layout('clash of realities', 'normal').

% Found in: ORI
card_name('clash of wills', 'Clash of Wills').
card_type('clash of wills', 'Instant').
card_types('clash of wills', ['Instant']).
card_subtypes('clash of wills', []).
card_colors('clash of wills', ['U']).
card_text('clash of wills', 'Counter target spell unless its controller pays {X}.').
card_mana_cost('clash of wills', ['X', 'U']).
card_cmc('clash of wills', 1).
card_layout('clash of wills', 'normal').

% Found in: DDM, ISD, M14, ORI
card_name('claustrophobia', 'Claustrophobia').
card_type('claustrophobia', 'Enchantment — Aura').
card_types('claustrophobia', ['Enchantment']).
card_subtypes('claustrophobia', ['Aura']).
card_colors('claustrophobia', ['U']).
card_text('claustrophobia', 'Enchant creature\nWhen Claustrophobia enters the battlefield, tap enchanted creature.\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('claustrophobia', ['1', 'U', 'U']).
card_cmc('claustrophobia', 3).
card_layout('claustrophobia', 'normal').

% Found in: TSB, USG
card_name('claws of gix', 'Claws of Gix').
card_type('claws of gix', 'Artifact').
card_types('claws of gix', ['Artifact']).
card_subtypes('claws of gix', []).
card_colors('claws of gix', []).
card_text('claws of gix', '{1}, Sacrifice a permanent: You gain 1 life.').
card_mana_cost('claws of gix', ['0']).
card_cmc('claws of gix', 0).
card_layout('claws of gix', 'normal').

% Found in: DDG, WWK
card_name('claws of valakut', 'Claws of Valakut').
card_type('claws of valakut', 'Enchantment — Aura').
card_types('claws of valakut', ['Enchantment']).
card_subtypes('claws of valakut', ['Aura']).
card_colors('claws of valakut', ['R']).
card_text('claws of valakut', 'Enchant creature\nEnchanted creature gets +1/+0 for each Mountain you control and has first strike.').
card_mana_cost('claws of valakut', ['1', 'R', 'R']).
card_cmc('claws of valakut', 3).
card_layout('claws of valakut', 'normal').

% Found in: SCG, VMA
card_name('claws of wirewood', 'Claws of Wirewood').
card_type('claws of wirewood', 'Sorcery').
card_types('claws of wirewood', ['Sorcery']).
card_subtypes('claws of wirewood', []).
card_colors('claws of wirewood', ['G']).
card_text('claws of wirewood', 'Claws of Wirewood deals 3 damage to each creature with flying and each player.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('claws of wirewood', ['3', 'G']).
card_cmc('claws of wirewood', 4).
card_layout('claws of wirewood', 'normal').

% Found in: UGL
card_name('clay pigeon', 'Clay Pigeon').
card_type('clay pigeon', 'Artifact Creature').
card_types('clay pigeon', ['Artifact', 'Creature']).
card_subtypes('clay pigeon', []).
card_colors('clay pigeon', []).
card_text('clay pigeon', 'Flying\n{1}, Throw Clay Pigeon into the air at least two feet above your head while seated, Attempt to catch it with one hand: If you catch Clay Pigeon, prevent all damage to you from any one source and return Clay Pigeon to play, tapped. Otherwise, sacrifice it.').
card_mana_cost('clay pigeon', ['3']).
card_cmc('clay pigeon', 3).
card_layout('clay pigeon', 'normal').
card_power('clay pigeon', 1).
card_toughness('clay pigeon', 1).

% Found in: 4ED, 5ED, ATQ, ME4
card_name('clay statue', 'Clay Statue').
card_type('clay statue', 'Artifact Creature — Golem').
card_types('clay statue', ['Artifact', 'Creature']).
card_subtypes('clay statue', ['Golem']).
card_colors('clay statue', []).
card_text('clay statue', '{2}: Regenerate Clay Statue.').
card_mana_cost('clay statue', ['4']).
card_cmc('clay statue', 4).
card_layout('clay statue', 'normal').
card_power('clay statue', 3).
card_toughness('clay statue', 1).

% Found in: CHK
card_name('cleanfall', 'Cleanfall').
card_type('cleanfall', 'Sorcery — Arcane').
card_types('cleanfall', ['Sorcery']).
card_subtypes('cleanfall', ['Arcane']).
card_colors('cleanfall', ['W']).
card_text('cleanfall', 'Destroy all enchantments.').
card_mana_cost('cleanfall', ['2', 'W']).
card_cmc('cleanfall', 3).
card_layout('cleanfall', 'normal').

% Found in: LEG, ME3
card_name('cleanse', 'Cleanse').
card_type('cleanse', 'Sorcery').
card_types('cleanse', ['Sorcery']).
card_subtypes('cleanse', []).
card_colors('cleanse', ['W']).
card_text('cleanse', 'Destroy all black creatures.').
card_mana_cost('cleanse', ['2', 'W', 'W']).
card_cmc('cleanse', 4).
card_layout('cleanse', 'normal').
card_reserved('cleanse').

% Found in: DRK
card_name('cleansing', 'Cleansing').
card_type('cleansing', 'Sorcery').
card_types('cleansing', ['Sorcery']).
card_subtypes('cleansing', []).
card_colors('cleansing', ['W']).
card_text('cleansing', 'For each land, destroy that land unless any player pays 1 life.').
card_mana_cost('cleansing', ['W', 'W', 'W']).
card_cmc('cleansing', 3).
card_layout('cleansing', 'normal').
card_reserved('cleansing').

% Found in: CMD, RAV
card_name('cleansing beam', 'Cleansing Beam').
card_type('cleansing beam', 'Instant').
card_types('cleansing beam', ['Instant']).
card_subtypes('cleansing beam', []).
card_colors('cleansing beam', ['R']).
card_text('cleansing beam', 'Radiance — Cleansing Beam deals 2 damage to target creature and each other creature that shares a color with it.').
card_mana_cost('cleansing beam', ['4', 'R']).
card_cmc('cleansing beam', 5).
card_layout('cleansing beam', 'normal').

% Found in: TOR
card_name('cleansing meditation', 'Cleansing Meditation').
card_type('cleansing meditation', 'Sorcery').
card_types('cleansing meditation', ['Sorcery']).
card_subtypes('cleansing meditation', []).
card_colors('cleansing meditation', ['W']).
card_text('cleansing meditation', 'Destroy all enchantments.\nThreshold — If seven or more cards are in your graveyard, instead destroy all enchantments, then return all cards in your graveyard destroyed this way to the battlefield.').
card_mana_cost('cleansing meditation', ['1', 'W', 'W']).
card_cmc('cleansing meditation', 3).
card_layout('cleansing meditation', 'normal').

% Found in: USG
card_name('clear', 'Clear').
card_type('clear', 'Instant').
card_types('clear', ['Instant']).
card_subtypes('clear', []).
card_colors('clear', ['W']).
card_text('clear', 'Destroy target enchantment.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('clear', ['1', 'W']).
card_cmc('clear', 2).
card_layout('clear', 'normal').

% Found in: DGM, M15
card_name('clear a path', 'Clear a Path').
card_type('clear a path', 'Sorcery').
card_types('clear a path', ['Sorcery']).
card_subtypes('clear a path', []).
card_colors('clear a path', ['R']).
card_text('clear a path', 'Destroy target creature with defender.').
card_mana_cost('clear a path', ['R']).
card_cmc('clear a path', 1).
card_layout('clear a path', 'normal').

% Found in: MMQ
card_name('clear the land', 'Clear the Land').
card_type('clear the land', 'Sorcery').
card_types('clear the land', ['Sorcery']).
card_subtypes('clear the land', []).
card_colors('clear the land', ['G']).
card_text('clear the land', 'Each player reveals the top five cards of his or her library, puts all land cards revealed this way onto the battlefield tapped, and exiles the rest.').
card_mana_cost('clear the land', ['2', 'G']).
card_cmc('clear the land', 3).
card_layout('clear the land', 'normal').

% Found in: 5DN
card_name('clearwater goblet', 'Clearwater Goblet').
card_type('clearwater goblet', 'Artifact').
card_types('clearwater goblet', ['Artifact']).
card_subtypes('clearwater goblet', []).
card_colors('clearwater goblet', []).
card_text('clearwater goblet', 'Sunburst (This enters the battlefield with a charge counter on it for each color of mana spent to cast it.)\nAt the beginning of your upkeep, you may gain life equal to the number of charge counters on Clearwater Goblet.').
card_mana_cost('clearwater goblet', ['5']).
card_cmc('clearwater goblet', 5).
card_layout('clearwater goblet', 'normal').

% Found in: M13
card_name('cleaver riot', 'Cleaver Riot').
card_type('cleaver riot', 'Sorcery').
card_types('cleaver riot', ['Sorcery']).
card_subtypes('cleaver riot', []).
card_colors('cleaver riot', ['R']).
card_text('cleaver riot', 'Creatures you control gain double strike until end of turn. (They deal both first-strike and regular combat damage.)').
card_mana_cost('cleaver riot', ['4', 'R']).
card_cmc('cleaver riot', 5).
card_layout('cleaver riot', 'normal').

% Found in: TMP
card_name('clergy en-vec', 'Clergy en-Vec').
card_type('clergy en-vec', 'Creature — Human Cleric').
card_types('clergy en-vec', ['Creature']).
card_subtypes('clergy en-vec', ['Human', 'Cleric']).
card_colors('clergy en-vec', ['W']).
card_text('clergy en-vec', '{T}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('clergy en-vec', ['1', 'W']).
card_cmc('clergy en-vec', 2).
card_layout('clergy en-vec', 'normal').
card_power('clergy en-vec', 1).
card_toughness('clergy en-vec', 1).

% Found in: LEG
card_name('clergy of the holy nimbus', 'Clergy of the Holy Nimbus').
card_type('clergy of the holy nimbus', 'Creature — Human Cleric').
card_types('clergy of the holy nimbus', ['Creature']).
card_subtypes('clergy of the holy nimbus', ['Human', 'Cleric']).
card_colors('clergy of the holy nimbus', ['W']).
card_text('clergy of the holy nimbus', 'If Clergy of the Holy Nimbus would be destroyed, regenerate it.\n{1}: Clergy of the Holy Nimbus can\'t be regenerated this turn. Only any opponent may activate this ability.').
card_mana_cost('clergy of the holy nimbus', ['W']).
card_cmc('clergy of the holy nimbus', 1).
card_layout('clergy of the holy nimbus', 'normal').
card_power('clergy of the holy nimbus', 1).
card_toughness('clergy of the holy nimbus', 1).

% Found in: ORI
card_name('cleric of the forward order', 'Cleric of the Forward Order').
card_type('cleric of the forward order', 'Creature — Human Cleric').
card_types('cleric of the forward order', ['Creature']).
card_subtypes('cleric of the forward order', ['Human', 'Cleric']).
card_colors('cleric of the forward order', ['W']).
card_text('cleric of the forward order', 'When Cleric of the Forward Order enters the battlefield, you gain 2 life for each creature you control named Cleric of the Forward Order.').
card_mana_cost('cleric of the forward order', ['1', 'W']).
card_cmc('cleric of the forward order', 2).
card_layout('cleric of the forward order', 'normal').
card_power('cleric of the forward order', 2).
card_toughness('cleric of the forward order', 2).

% Found in: KTK
card_name('clever impersonator', 'Clever Impersonator').
card_type('clever impersonator', 'Creature — Shapeshifter').
card_types('clever impersonator', ['Creature']).
card_subtypes('clever impersonator', ['Shapeshifter']).
card_colors('clever impersonator', ['U']).
card_text('clever impersonator', 'You may have Clever Impersonator enter the battlefield as a copy of any nonland permanent on the battlefield.').
card_mana_cost('clever impersonator', ['2', 'U', 'U']).
card_cmc('clever impersonator', 4).
card_layout('clever impersonator', 'normal').
card_power('clever impersonator', 0).
card_toughness('clever impersonator', 0).

% Found in: DD3_EVG, EVG, LGN, VMA
card_name('clickslither', 'Clickslither').
card_type('clickslither', 'Creature — Insect').
card_types('clickslither', ['Creature']).
card_subtypes('clickslither', ['Insect']).
card_colors('clickslither', ['R']).
card_text('clickslither', 'Haste\nSacrifice a Goblin: Clickslither gets +2/+2 and gains trample until end of turn.').
card_mana_cost('clickslither', ['1', 'R', 'R', 'R']).
card_cmc('clickslither', 4).
card_layout('clickslither', 'normal').
card_power('clickslither', 3).
card_toughness('clickslither', 3).

% Found in: ZEN
card_name('cliff threader', 'Cliff Threader').
card_type('cliff threader', 'Creature — Kor Scout').
card_types('cliff threader', ['Creature']).
card_subtypes('cliff threader', ['Kor', 'Scout']).
card_colors('cliff threader', ['W']).
card_text('cliff threader', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)').
card_mana_cost('cliff threader', ['1', 'W']).
card_cmc('cliff threader', 2).
card_layout('cliff threader', 'normal').
card_power('cliff threader', 2).
card_toughness('cliff threader', 1).

% Found in: CON
card_name('cliffrunner behemoth', 'Cliffrunner Behemoth').
card_type('cliffrunner behemoth', 'Creature — Rhino Beast').
card_types('cliffrunner behemoth', ['Creature']).
card_subtypes('cliffrunner behemoth', ['Rhino', 'Beast']).
card_colors('cliffrunner behemoth', ['G']).
card_text('cliffrunner behemoth', 'Cliffrunner Behemoth has haste as long as you control a red permanent.\nCliffrunner Behemoth has lifelink as long as you control a white permanent.').
card_mana_cost('cliffrunner behemoth', ['3', 'G']).
card_cmc('cliffrunner behemoth', 4).
card_layout('cliffrunner behemoth', 'normal').
card_power('cliffrunner behemoth', 5).
card_toughness('cliffrunner behemoth', 3).

% Found in: BFZ
card_name('cliffside lookout', 'Cliffside Lookout').
card_type('cliffside lookout', 'Creature — Kor Scout Ally').
card_types('cliffside lookout', ['Creature']).
card_subtypes('cliffside lookout', ['Kor', 'Scout', 'Ally']).
card_colors('cliffside lookout', ['W']).
card_text('cliffside lookout', '{4}{W}: Creatures you control get +1/+1 until end of turn.').
card_mana_cost('cliffside lookout', ['W']).
card_cmc('cliffside lookout', 1).
card_layout('cliffside lookout', 'normal').
card_power('cliffside lookout', 1).
card_toughness('cliffside lookout', 1).

% Found in: HOP
card_name('cliffside market', 'Cliffside Market').
card_type('cliffside market', 'Plane — Mercadia').
card_types('cliffside market', ['Plane']).
card_subtypes('cliffside market', ['Mercadia']).
card_colors('cliffside market', []).
card_text('cliffside market', 'When you planeswalk to Cliffside Market or at the beginning of your upkeep, you may exchange life totals with target player.\nWhenever you roll {C}, exchange control of two target permanents that share a card type.').
card_layout('cliffside market', 'plane').

% Found in: ISD
card_name('clifftop retreat', 'Clifftop Retreat').
card_type('clifftop retreat', 'Land').
card_types('clifftop retreat', ['Land']).
card_subtypes('clifftop retreat', []).
card_colors('clifftop retreat', []).
card_text('clifftop retreat', 'Clifftop Retreat enters the battlefield tapped unless you control a Mountain or a Plains.\n{T}: Add {R} or {W} to your mana pool.').
card_layout('clifftop retreat', 'normal').

% Found in: GTC
card_name('clinging anemones', 'Clinging Anemones').
card_type('clinging anemones', 'Creature — Jellyfish').
card_types('clinging anemones', ['Creature']).
card_subtypes('clinging anemones', ['Jellyfish']).
card_colors('clinging anemones', ['U']).
card_text('clinging anemones', 'Defender\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_mana_cost('clinging anemones', ['3', 'U']).
card_cmc('clinging anemones', 4).
card_layout('clinging anemones', 'normal').
card_power('clinging anemones', 1).
card_toughness('clinging anemones', 4).

% Found in: RAV
card_name('clinging darkness', 'Clinging Darkness').
card_type('clinging darkness', 'Enchantment — Aura').
card_types('clinging darkness', ['Enchantment']).
card_subtypes('clinging darkness', ['Aura']).
card_colors('clinging darkness', ['B']).
card_text('clinging darkness', 'Enchant creature\nEnchanted creature gets -4/-1.').
card_mana_cost('clinging darkness', ['1', 'B']).
card_cmc('clinging darkness', 2).
card_layout('clinging darkness', 'normal').

% Found in: DKA
card_name('clinging mists', 'Clinging Mists').
card_type('clinging mists', 'Instant').
card_types('clinging mists', ['Instant']).
card_subtypes('clinging mists', []).
card_colors('clinging mists', ['G']).
card_text('clinging mists', 'Prevent all combat damage that would be dealt this turn.\nFateful hour — If you have 5 or less life, tap all attacking creatures. Those creatures don\'t untap during their controller\'s next untap step.').
card_mana_cost('clinging mists', ['2', 'G']).
card_cmc('clinging mists', 3).
card_layout('clinging mists', 'normal').

% Found in: MOR
card_name('cloak and dagger', 'Cloak and Dagger').
card_type('cloak and dagger', 'Tribal Artifact — Rogue Equipment').
card_types('cloak and dagger', ['Tribal', 'Artifact']).
card_subtypes('cloak and dagger', ['Rogue', 'Equipment']).
card_colors('cloak and dagger', []).
card_text('cloak and dagger', 'Equipped creature gets +2/+0 and has shroud. (It can\'t be the target of spells or abilities.)\nWhenever a Rogue creature enters the battlefield, you may attach Cloak and Dagger to it.\nEquip {3}').
card_mana_cost('cloak and dagger', ['2']).
card_cmc('cloak and dagger', 2).
card_layout('cloak and dagger', 'normal').

% Found in: 5ED, ICE, ME2
card_name('cloak of confusion', 'Cloak of Confusion').
card_type('cloak of confusion', 'Enchantment — Aura').
card_types('cloak of confusion', ['Enchantment']).
card_subtypes('cloak of confusion', ['Aura']).
card_colors('cloak of confusion', ['B']).
card_text('cloak of confusion', 'Enchant creature you control\nWhenever enchanted creature attacks and isn\'t blocked, you may have it assign no combat damage this turn. If you do, defending player discards a card at random.').
card_mana_cost('cloak of confusion', ['1', 'B']).
card_cmc('cloak of confusion', 2).
card_layout('cloak of confusion', 'normal').

% Found in: POR
card_name('cloak of feathers', 'Cloak of Feathers').
card_type('cloak of feathers', 'Sorcery').
card_types('cloak of feathers', ['Sorcery']).
card_subtypes('cloak of feathers', []).
card_colors('cloak of feathers', ['U']).
card_text('cloak of feathers', 'Target creature gains flying until end of turn.\nDraw a card.').
card_mana_cost('cloak of feathers', ['U']).
card_cmc('cloak of feathers', 1).
card_layout('cloak of feathers', 'normal').

% Found in: MIR
card_name('cloak of invisibility', 'Cloak of Invisibility').
card_type('cloak of invisibility', 'Enchantment — Aura').
card_types('cloak of invisibility', ['Enchantment']).
card_subtypes('cloak of invisibility', ['Aura']).
card_colors('cloak of invisibility', ['U']).
card_text('cloak of invisibility', 'Enchant creature\nEnchanted creature has phasing and can\'t be blocked except by Walls. (It phases in or out before its controller untaps during each of his or her untap steps. While it\'s phased out, it\'s treated as though it doesn\'t exist.)').
card_mana_cost('cloak of invisibility', ['U']).
card_cmc('cloak of invisibility', 1).
card_layout('cloak of invisibility', 'normal').

% Found in: USG
card_name('cloak of mists', 'Cloak of Mists').
card_type('cloak of mists', 'Enchantment — Aura').
card_types('cloak of mists', ['Enchantment']).
card_subtypes('cloak of mists', ['Aura']).
card_colors('cloak of mists', ['U']).
card_text('cloak of mists', 'Enchant creature\nEnchanted creature can\'t be blocked.').
card_mana_cost('cloak of mists', ['1', 'U']).
card_cmc('cloak of mists', 2).
card_layout('cloak of mists', 'normal').

% Found in: JOU
card_name('cloaked siren', 'Cloaked Siren').
card_type('cloaked siren', 'Creature — Siren').
card_types('cloaked siren', ['Creature']).
card_subtypes('cloaked siren', ['Siren']).
card_colors('cloaked siren', ['U']).
card_text('cloaked siren', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying').
card_mana_cost('cloaked siren', ['3', 'U']).
card_cmc('cloaked siren', 4).
card_layout('cloaked siren', 'normal').
card_power('cloaked siren', 3).
card_toughness('cloaked siren', 2).

% Found in: 5DN, M13
card_name('clock of omens', 'Clock of Omens').
card_type('clock of omens', 'Artifact').
card_types('clock of omens', ['Artifact']).
card_subtypes('clock of omens', []).
card_colors('clock of omens', []).
card_text('clock of omens', 'Tap two untapped artifacts you control: Untap target artifact.').
card_mana_cost('clock of omens', ['4']).
card_cmc('clock of omens', 4).
card_layout('clock of omens', 'normal').

% Found in: TSP
card_name('clockspinning', 'Clockspinning').
card_type('clockspinning', 'Instant').
card_types('clockspinning', ['Instant']).
card_subtypes('clockspinning', []).
card_colors('clockspinning', ['U']).
card_text('clockspinning', 'Buyback {3} (You may pay an additional {3} as you cast this spell. If you do, put this card into your hand as it resolves.)\nChoose a counter on target permanent or suspended card. Remove that counter from that permanent or card or put another of those counters on it.').
card_mana_cost('clockspinning', ['U']).
card_cmc('clockspinning', 1).
card_layout('clockspinning', 'normal').

% Found in: 4ED, ATQ, BTD, ME4
card_name('clockwork avian', 'Clockwork Avian').
card_type('clockwork avian', 'Artifact Creature — Bird').
card_types('clockwork avian', ['Artifact', 'Creature']).
card_subtypes('clockwork avian', ['Bird']).
card_colors('clockwork avian', []).
card_text('clockwork avian', 'Flying\nClockwork Avian enters the battlefield with four +1/+0 counters on it.\nAt end of combat, if Clockwork Avian attacked or blocked this combat, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Avian. This ability can\'t cause the total number of +1/+0 counters on Clockwork Avian to be greater than four. Activate this ability only during your upkeep.').
card_mana_cost('clockwork avian', ['5']).
card_cmc('clockwork avian', 5).
card_layout('clockwork avian', 'normal').
card_power('clockwork avian', 0).
card_toughness('clockwork avian', 4).

% Found in: 2ED, 3ED, 4ED, 5ED, BTD, CED, CEI, ITP, LEA, LEB, MED, RQS
card_name('clockwork beast', 'Clockwork Beast').
card_type('clockwork beast', 'Artifact Creature — Beast').
card_types('clockwork beast', ['Artifact', 'Creature']).
card_subtypes('clockwork beast', ['Beast']).
card_colors('clockwork beast', []).
card_text('clockwork beast', 'Clockwork Beast enters the battlefield with seven +1/+0 counters on it.\nAt end of combat, if Clockwork Beast attacked or blocked this combat, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Beast. This ability can\'t cause the total number of +1/+0 counters on Clockwork Beast to be greater than seven. Activate this ability only during your upkeep.').
card_mana_cost('clockwork beast', ['6']).
card_cmc('clockwork beast', 6).
card_layout('clockwork beast', 'normal').
card_power('clockwork beast', 0).
card_toughness('clockwork beast', 4).

% Found in: MRD
card_name('clockwork beetle', 'Clockwork Beetle').
card_type('clockwork beetle', 'Artifact Creature — Insect').
card_types('clockwork beetle', ['Artifact', 'Creature']).
card_subtypes('clockwork beetle', ['Insect']).
card_colors('clockwork beetle', []).
card_text('clockwork beetle', 'Clockwork Beetle enters the battlefield with two +1/+1 counters on it.\nWhenever Clockwork Beetle attacks or blocks, remove a +1/+1 counter from it at end of combat.').
card_mana_cost('clockwork beetle', ['1']).
card_cmc('clockwork beetle', 1).
card_layout('clockwork beetle', 'normal').
card_power('clockwork beetle', 0).
card_toughness('clockwork beetle', 0).

% Found in: DDF, MRD
card_name('clockwork condor', 'Clockwork Condor').
card_type('clockwork condor', 'Artifact Creature — Bird').
card_types('clockwork condor', ['Artifact', 'Creature']).
card_subtypes('clockwork condor', ['Bird']).
card_colors('clockwork condor', []).
card_text('clockwork condor', 'Flying\nClockwork Condor enters the battlefield with three +1/+1 counters on it.\nWhenever Clockwork Condor attacks or blocks, remove a +1/+1 counter from it at end of combat.').
card_mana_cost('clockwork condor', ['4']).
card_cmc('clockwork condor', 4).
card_layout('clockwork condor', 'normal').
card_power('clockwork condor', 0).
card_toughness('clockwork condor', 0).

% Found in: MRD
card_name('clockwork dragon', 'Clockwork Dragon').
card_type('clockwork dragon', 'Artifact Creature — Dragon').
card_types('clockwork dragon', ['Artifact', 'Creature']).
card_subtypes('clockwork dragon', ['Dragon']).
card_colors('clockwork dragon', []).
card_text('clockwork dragon', 'Flying\nClockwork Dragon enters the battlefield with six +1/+1 counters on it.\nWhenever Clockwork Dragon attacks or blocks, remove a +1/+1 counter from it at end of combat.\n{3}: Put a +1/+1 counter on Clockwork Dragon.').
card_mana_cost('clockwork dragon', ['7']).
card_cmc('clockwork dragon', 7).
card_layout('clockwork dragon', 'normal').
card_power('clockwork dragon', 0).
card_toughness('clockwork dragon', 0).

% Found in: HML, ME4
card_name('clockwork gnomes', 'Clockwork Gnomes').
card_type('clockwork gnomes', 'Artifact Creature — Gnome').
card_types('clockwork gnomes', ['Artifact', 'Creature']).
card_subtypes('clockwork gnomes', ['Gnome']).
card_colors('clockwork gnomes', []).
card_text('clockwork gnomes', '{3}, {T}: Regenerate target artifact creature.').
card_mana_cost('clockwork gnomes', ['4']).
card_cmc('clockwork gnomes', 4).
card_layout('clockwork gnomes', 'normal').
card_power('clockwork gnomes', 2).
card_toughness('clockwork gnomes', 2).

% Found in: DDF, TSP
card_name('clockwork hydra', 'Clockwork Hydra').
card_type('clockwork hydra', 'Artifact Creature — Hydra').
card_types('clockwork hydra', ['Artifact', 'Creature']).
card_subtypes('clockwork hydra', ['Hydra']).
card_colors('clockwork hydra', []).
card_text('clockwork hydra', 'Clockwork Hydra enters the battlefield with four +1/+1 counters on it.\nWhenever Clockwork Hydra attacks or blocks, remove a +1/+1 counter from it. If you do, Clockwork Hydra deals 1 damage to target creature or player.\n{T}: Put a +1/+1 counter on Clockwork Hydra.').
card_mana_cost('clockwork hydra', ['5']).
card_cmc('clockwork hydra', 5).
card_layout('clockwork hydra', 'normal').
card_power('clockwork hydra', 0).
card_toughness('clockwork hydra', 0).

% Found in: 5ED, HML, ME2
card_name('clockwork steed', 'Clockwork Steed').
card_type('clockwork steed', 'Artifact Creature — Horse').
card_types('clockwork steed', ['Artifact', 'Creature']).
card_subtypes('clockwork steed', ['Horse']).
card_colors('clockwork steed', []).
card_text('clockwork steed', 'Clockwork Steed enters the battlefield with four +1/+0 counters on it.\nClockwork Steed can\'t be blocked by artifact creatures.\nAt end of combat, if Clockwork Steed attacked or blocked this combat, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Steed. This ability can\'t cause the total number of +1/+0 counters on Clockwork Steed to be greater than four. Activate this ability only during your upkeep.').
card_mana_cost('clockwork steed', ['4']).
card_cmc('clockwork steed', 4).
card_layout('clockwork steed', 'normal').
card_power('clockwork steed', 0).
card_toughness('clockwork steed', 3).

% Found in: HML, ME4
card_name('clockwork swarm', 'Clockwork Swarm').
card_type('clockwork swarm', 'Artifact Creature — Insect').
card_types('clockwork swarm', ['Artifact', 'Creature']).
card_subtypes('clockwork swarm', ['Insect']).
card_colors('clockwork swarm', []).
card_text('clockwork swarm', 'Clockwork Swarm enters the battlefield with four +1/+0 counters on it.\nClockwork Swarm can\'t be blocked by Walls.\nAt end of combat, if Clockwork Swarm attacked or blocked this combat, remove a +1/+0 counter from it.\n{X}, {T}: Put up to X +1/+0 counters on Clockwork Swarm. This ability can\'t cause the total number of +1/+0 counters on Clockwork Swarm to be greater than four. Activate this ability only during your upkeep.').
card_mana_cost('clockwork swarm', ['4']).
card_cmc('clockwork swarm', 4).
card_layout('clockwork swarm', 'normal').
card_power('clockwork swarm', 0).
card_toughness('clockwork swarm', 3).

% Found in: MRD
card_name('clockwork vorrac', 'Clockwork Vorrac').
card_type('clockwork vorrac', 'Artifact Creature — Boar Beast').
card_types('clockwork vorrac', ['Artifact', 'Creature']).
card_subtypes('clockwork vorrac', ['Boar', 'Beast']).
card_colors('clockwork vorrac', []).
card_text('clockwork vorrac', 'Trample\nClockwork Vorrac enters the battlefield with four +1/+1 counters on it.\nWhenever Clockwork Vorrac attacks or blocks, remove a +1/+1 counter from it at end of combat.\n{T}: Put a +1/+1 counter on Clockwork Vorrac.').
card_mana_cost('clockwork vorrac', ['5']).
card_cmc('clockwork vorrac', 5).
card_layout('clockwork vorrac', 'normal').
card_power('clockwork vorrac', 0).
card_toughness('clockwork vorrac', 0).

% Found in: ISD
card_name('cloistered youth', 'Cloistered Youth').
card_type('cloistered youth', 'Creature — Human').
card_types('cloistered youth', ['Creature']).
card_subtypes('cloistered youth', ['Human']).
card_colors('cloistered youth', ['W']).
card_text('cloistered youth', 'At the beginning of your upkeep, you may transform Cloistered Youth.').
card_mana_cost('cloistered youth', ['1', 'W']).
card_cmc('cloistered youth', 2).
card_layout('cloistered youth', 'double-faced').
card_power('cloistered youth', 1).
card_toughness('cloistered youth', 1).
card_sides('cloistered youth', 'unholy fiend').

% Found in: 10E, 2ED, 3ED, 9ED, CED, CEI, DDI, LEA, LEB, M10, M11, M13, M14, ONS
card_name('clone', 'Clone').
card_type('clone', 'Creature — Shapeshifter').
card_types('clone', ['Creature']).
card_subtypes('clone', ['Shapeshifter']).
card_colors('clone', ['U']).
card_text('clone', 'You may have Clone enter the battlefield as a copy of any creature on the battlefield.').
card_mana_cost('clone', ['3', 'U']).
card_cmc('clone', 4).
card_layout('clone', 'normal').
card_power('clone', 0).
card_toughness('clone', 0).

% Found in: DTK
card_name('clone legion', 'Clone Legion').
card_type('clone legion', 'Sorcery').
card_types('clone legion', ['Sorcery']).
card_subtypes('clone legion', []).
card_colors('clone legion', ['U']).
card_text('clone legion', 'For each creature target player controls, put a token onto the battlefield that\'s a copy of that creature.').
card_mana_cost('clone legion', ['7', 'U', 'U']).
card_cmc('clone legion', 9).
card_layout('clone legion', 'normal').

% Found in: SOM
card_name('clone shell', 'Clone Shell').
card_type('clone shell', 'Artifact Creature — Shapeshifter').
card_types('clone shell', ['Artifact', 'Creature']).
card_subtypes('clone shell', ['Shapeshifter']).
card_colors('clone shell', []).
card_text('clone shell', 'Imprint — When Clone Shell enters the battlefield, look at the top four cards of your library, exile one face down, then put the rest on the bottom of your library in any order.\nWhen Clone Shell dies, turn the exiled card face up. If it\'s a creature card, put it onto the battlefield under your control.').
card_mana_cost('clone shell', ['5']).
card_cmc('clone shell', 5).
card_layout('clone shell', 'normal').
card_power('clone shell', 2).
card_toughness('clone shell', 2).

% Found in: MMQ
card_name('close quarters', 'Close Quarters').
card_type('close quarters', 'Enchantment').
card_types('close quarters', ['Enchantment']).
card_subtypes('close quarters', []).
card_colors('close quarters', ['R']).
card_text('close quarters', 'Whenever a creature you control becomes blocked, Close Quarters deals 1 damage to target creature or player.').
card_mana_cost('close quarters', ['2', 'R', 'R']).
card_cmc('close quarters', 4).
card_layout('close quarters', 'normal').

% Found in: H09, TMP, TPR
card_name('clot sliver', 'Clot Sliver').
card_type('clot sliver', 'Creature — Sliver').
card_types('clot sliver', ['Creature']).
card_subtypes('clot sliver', ['Sliver']).
card_colors('clot sliver', ['B']).
card_text('clot sliver', 'All Slivers have \"{2}: Regenerate this permanent.\"').
card_mana_cost('clot sliver', ['1', 'B']).
card_cmc('clot sliver', 2).
card_layout('clot sliver', 'normal').
card_power('clot sliver', 1).
card_toughness('clot sliver', 1).

% Found in: PLS
card_name('cloud cover', 'Cloud Cover').
card_type('cloud cover', 'Enchantment').
card_types('cloud cover', ['Enchantment']).
card_subtypes('cloud cover', []).
card_colors('cloud cover', ['W', 'U']).
card_text('cloud cover', 'Whenever another permanent you control becomes the target of a spell or ability an opponent controls, you may return that permanent to its owner\'s hand.').
card_mana_cost('cloud cover', ['2', 'W', 'U']).
card_cmc('cloud cover', 4).
card_layout('cloud cover', 'normal').

% Found in: M11
card_name('cloud crusader', 'Cloud Crusader').
card_type('cloud crusader', 'Creature — Human Knight').
card_types('cloud crusader', ['Creature']).
card_subtypes('cloud crusader', ['Human', 'Knight']).
card_colors('cloud crusader', ['W']).
card_text('cloud crusader', 'Flying\nFirst strike (This creature deals combat damage before creatures without first strike.)').
card_mana_cost('cloud crusader', ['2', 'W', 'W']).
card_cmc('cloud crusader', 4).
card_layout('cloud crusader', 'normal').
card_power('cloud crusader', 2).
card_toughness('cloud crusader', 3).

% Found in: BTD, VMA, WTH
card_name('cloud djinn', 'Cloud Djinn').
card_type('cloud djinn', 'Creature — Djinn').
card_types('cloud djinn', ['Creature']).
card_subtypes('cloud djinn', ['Djinn']).
card_colors('cloud djinn', ['U']).
card_text('cloud djinn', 'Flying\nCloud Djinn can block only creatures with flying.').
card_mana_cost('cloud djinn', ['5', 'U']).
card_cmc('cloud djinn', 6).
card_layout('cloud djinn', 'normal').
card_power('cloud djinn', 5).
card_toughness('cloud djinn', 4).

% Found in: ME4, POR
card_name('cloud dragon', 'Cloud Dragon').
card_type('cloud dragon', 'Creature — Illusion Dragon').
card_types('cloud dragon', ['Creature']).
card_subtypes('cloud dragon', ['Illusion', 'Dragon']).
card_colors('cloud dragon', ['U']).
card_text('cloud dragon', 'Flying\nCloud Dragon can block only creatures with flying.').
card_mana_cost('cloud dragon', ['5', 'U']).
card_cmc('cloud dragon', 6).
card_layout('cloud dragon', 'normal').
card_power('cloud dragon', 5).
card_toughness('cloud dragon', 4).

% Found in: 10E, BTD, M11, MM2, VIS
card_name('cloud elemental', 'Cloud Elemental').
card_type('cloud elemental', 'Creature — Elemental').
card_types('cloud elemental', ['Creature']).
card_subtypes('cloud elemental', ['Elemental']).
card_colors('cloud elemental', ['U']).
card_text('cloud elemental', 'Flying\nCloud Elemental can block only creatures with flying.').
card_mana_cost('cloud elemental', ['2', 'U']).
card_cmc('cloud elemental', 3).
card_layout('cloud elemental', 'normal').
card_power('cloud elemental', 2).
card_toughness('cloud elemental', 3).

% Found in: FUT
card_name('cloud key', 'Cloud Key').
card_type('cloud key', 'Artifact').
card_types('cloud key', ['Artifact']).
card_subtypes('cloud key', []).
card_colors('cloud key', []).
card_text('cloud key', 'As Cloud Key enters the battlefield, choose artifact, creature, enchantment, instant, or sorcery.\nSpells you cast of the chosen type cost {1} less to cast.').
card_mana_cost('cloud key', ['3']).
card_cmc('cloud key', 3).
card_layout('cloud key', 'normal').

% Found in: BFZ
card_name('cloud manta', 'Cloud Manta').
card_type('cloud manta', 'Creature — Fish').
card_types('cloud manta', ['Creature']).
card_subtypes('cloud manta', ['Fish']).
card_colors('cloud manta', ['U']).
card_text('cloud manta', 'Flying').
card_mana_cost('cloud manta', ['3', 'U']).
card_cmc('cloud manta', 4).
card_layout('cloud manta', 'normal').
card_power('cloud manta', 3).
card_toughness('cloud manta', 2).

% Found in: ULG, VMA
card_name('cloud of faeries', 'Cloud of Faeries').
card_type('cloud of faeries', 'Creature — Faerie').
card_types('cloud of faeries', ['Creature']).
card_subtypes('cloud of faeries', ['Faerie']).
card_colors('cloud of faeries', ['U']).
card_text('cloud of faeries', 'Flying\nWhen Cloud of Faeries enters the battlefield, untap up to two lands.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('cloud of faeries', ['1', 'U']).
card_cmc('cloud of faeries', 2).
card_layout('cloud of faeries', 'normal').
card_power('cloud of faeries', 1).
card_toughness('cloud of faeries', 1).

% Found in: POR, pPOD
card_name('cloud pirates', 'Cloud Pirates').
card_type('cloud pirates', 'Creature — Human Pirate').
card_types('cloud pirates', ['Creature']).
card_subtypes('cloud pirates', ['Human', 'Pirate']).
card_colors('cloud pirates', ['U']).
card_text('cloud pirates', 'Flying\nCloud Pirates can block only creatures with flying.').
card_mana_cost('cloud pirates', ['U']).
card_cmc('cloud pirates', 1).
card_layout('cloud pirates', 'normal').
card_power('cloud pirates', 1).
card_toughness('cloud pirates', 1).

% Found in: ME4, POR, STH
card_name('cloud spirit', 'Cloud Spirit').
card_type('cloud spirit', 'Creature — Spirit').
card_types('cloud spirit', ['Creature']).
card_subtypes('cloud spirit', ['Spirit']).
card_colors('cloud spirit', ['U']).
card_text('cloud spirit', 'Flying\nCloud Spirit can block only creatures with flying.').
card_mana_cost('cloud spirit', ['2', 'U']).
card_cmc('cloud spirit', 3).
card_layout('cloud spirit', 'normal').
card_power('cloud spirit', 3).
card_toughness('cloud spirit', 1).

% Found in: 10E, DPA, MMQ
card_name('cloud sprite', 'Cloud Sprite').
card_type('cloud sprite', 'Creature — Faerie').
card_types('cloud sprite', ['Creature']).
card_subtypes('cloud sprite', ['Faerie']).
card_colors('cloud sprite', ['U']).
card_text('cloud sprite', 'Flying (This creature can\'t be blocked except by creatures with flying or reach.)\nCloud Sprite can block only creatures with flying.').
card_mana_cost('cloud sprite', ['U']).
card_cmc('cloud sprite', 1).
card_layout('cloud sprite', 'normal').
card_power('cloud sprite', 1).
card_toughness('cloud sprite', 1).

% Found in: 7ED, TMP
card_name('cloudchaser eagle', 'Cloudchaser Eagle').
card_type('cloudchaser eagle', 'Creature — Bird').
card_types('cloudchaser eagle', ['Creature']).
card_subtypes('cloudchaser eagle', ['Bird']).
card_colors('cloudchaser eagle', ['W']).
card_text('cloudchaser eagle', 'Flying\nWhen Cloudchaser Eagle enters the battlefield, destroy target enchantment.').
card_mana_cost('cloudchaser eagle', ['3', 'W']).
card_cmc('cloudchaser eagle', 4).
card_layout('cloudchaser eagle', 'normal').
card_power('cloudchaser eagle', 2).
card_toughness('cloudchaser eagle', 2).

% Found in: TSP
card_name('cloudchaser kestrel', 'Cloudchaser Kestrel').
card_type('cloudchaser kestrel', 'Creature — Bird').
card_types('cloudchaser kestrel', ['Creature']).
card_subtypes('cloudchaser kestrel', ['Bird']).
card_colors('cloudchaser kestrel', ['W']).
card_text('cloudchaser kestrel', 'Flying\nWhen Cloudchaser Kestrel enters the battlefield, destroy target enchantment.\n{W}: Target permanent becomes white until end of turn.').
card_mana_cost('cloudchaser kestrel', ['1', 'W', 'W']).
card_cmc('cloudchaser kestrel', 3).
card_layout('cloudchaser kestrel', 'normal').
card_power('cloudchaser kestrel', 2).
card_toughness('cloudchaser kestrel', 2).

% Found in: CHK
card_name('cloudcrest lake', 'Cloudcrest Lake').
card_type('cloudcrest lake', 'Land').
card_types('cloudcrest lake', ['Land']).
card_subtypes('cloudcrest lake', []).
card_colors('cloudcrest lake', []).
card_text('cloudcrest lake', '{T}: Add {1} to your mana pool.\n{T}: Add {W} or {U} to your mana pool. Cloudcrest Lake doesn\'t untap during your next untap step.').
card_layout('cloudcrest lake', 'normal').

% Found in: LRW
card_name('cloudcrown oak', 'Cloudcrown Oak').
card_type('cloudcrown oak', 'Creature — Treefolk Warrior').
card_types('cloudcrown oak', ['Creature']).
card_subtypes('cloudcrown oak', ['Treefolk', 'Warrior']).
card_colors('cloudcrown oak', ['G']).
card_text('cloudcrown oak', 'Reach (This creature can block creatures with flying.)').
card_mana_cost('cloudcrown oak', ['2', 'G', 'G']).
card_cmc('cloudcrown oak', 4).
card_layout('cloudcrown oak', 'normal').
card_power('cloudcrown oak', 3).
card_toughness('cloudcrown oak', 4).

% Found in: GTC
card_name('cloudfin raptor', 'Cloudfin Raptor').
card_type('cloudfin raptor', 'Creature — Bird Mutant').
card_types('cloudfin raptor', ['Creature']).
card_subtypes('cloudfin raptor', ['Bird', 'Mutant']).
card_colors('cloudfin raptor', ['U']).
card_text('cloudfin raptor', 'Flying\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_mana_cost('cloudfin raptor', ['U']).
card_cmc('cloudfin raptor', 1).
card_layout('cloudfin raptor', 'normal').
card_power('cloudfin raptor', 0).
card_toughness('cloudfin raptor', 1).

% Found in: FRF
card_name('cloudform', 'Cloudform').
card_type('cloudform', 'Enchantment').
card_types('cloudform', ['Enchantment']).
card_subtypes('cloudform', []).
card_colors('cloudform', ['U']).
card_text('cloudform', 'When Cloudform enters the battlefield, it becomes an Aura with enchant creature. Manifest the top card of your library and attach Cloudform to it. (To manifest a card, put it onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)\nEnchanted creature has flying and hexproof.').
card_mana_cost('cloudform', ['1', 'U', 'U']).
card_cmc('cloudform', 3).
card_layout('cloudform', 'normal').

% Found in: LRW, MMA
card_name('cloudgoat ranger', 'Cloudgoat Ranger').
card_type('cloudgoat ranger', 'Creature — Giant Warrior').
card_types('cloudgoat ranger', ['Creature']).
card_subtypes('cloudgoat ranger', ['Giant', 'Warrior']).
card_colors('cloudgoat ranger', ['W']).
card_text('cloudgoat ranger', 'When Cloudgoat Ranger enters the battlefield, put three 1/1 white Kithkin Soldier creature tokens onto the battlefield.\nTap three untapped Kithkin you control: Cloudgoat Ranger gets +2/+0 and gains flying until end of turn.').
card_mana_cost('cloudgoat ranger', ['3', 'W', 'W']).
card_cmc('cloudgoat ranger', 5).
card_layout('cloudgoat ranger', 'normal').
card_power('cloudgoat ranger', 3).
card_toughness('cloudgoat ranger', 3).

% Found in: ALA
card_name('cloudheath drake', 'Cloudheath Drake').
card_type('cloudheath drake', 'Artifact Creature — Drake').
card_types('cloudheath drake', ['Artifact', 'Creature']).
card_subtypes('cloudheath drake', ['Drake']).
card_colors('cloudheath drake', ['U']).
card_text('cloudheath drake', 'Flying\n{1}{W}: Cloudheath Drake gains vigilance until end of turn.').
card_mana_cost('cloudheath drake', ['4', 'U']).
card_cmc('cloudheath drake', 5).
card_layout('cloudheath drake', 'normal').
card_power('cloudheath drake', 3).
card_toughness('cloudheath drake', 3).

% Found in: SOK
card_name('cloudhoof kirin', 'Cloudhoof Kirin').
card_type('cloudhoof kirin', 'Legendary Creature — Kirin Spirit').
card_types('cloudhoof kirin', ['Creature']).
card_subtypes('cloudhoof kirin', ['Kirin', 'Spirit']).
card_supertypes('cloudhoof kirin', ['Legendary']).
card_colors('cloudhoof kirin', ['U']).
card_text('cloudhoof kirin', 'Flying\nWhenever you cast a Spirit or Arcane spell, you may have target player put the top X cards of his or her library into his or her graveyard, where X is that spell\'s converted mana cost.').
card_mana_cost('cloudhoof kirin', ['3', 'U', 'U']).
card_cmc('cloudhoof kirin', 5).
card_layout('cloudhoof kirin', 'normal').
card_power('cloudhoof kirin', 4).
card_toughness('cloudhoof kirin', 4).

% Found in: MRD, pFNM
card_name('cloudpost', 'Cloudpost').
card_type('cloudpost', 'Land — Locus').
card_types('cloudpost', ['Land']).
card_subtypes('cloudpost', ['Locus']).
card_colors('cloudpost', []).
card_text('cloudpost', 'Cloudpost enters the battlefield tapped.\n{T}: Add {1} to your mana pool for each Locus on the battlefield.').
card_layout('cloudpost', 'normal').

% Found in: LGN
card_name('cloudreach cavalry', 'Cloudreach Cavalry').
card_type('cloudreach cavalry', 'Creature — Human Soldier').
card_types('cloudreach cavalry', ['Creature']).
card_subtypes('cloudreach cavalry', ['Human', 'Soldier']).
card_colors('cloudreach cavalry', ['W']).
card_text('cloudreach cavalry', 'As long as you control a Bird, Cloudreach Cavalry gets +2/+2 and has flying.').
card_mana_cost('cloudreach cavalry', ['1', 'W']).
card_cmc('cloudreach cavalry', 2).
card_layout('cloudreach cavalry', 'normal').
card_power('cloudreach cavalry', 1).
card_toughness('cloudreach cavalry', 1).

% Found in: FUT
card_name('cloudseeder', 'Cloudseeder').
card_type('cloudseeder', 'Creature — Faerie Spellshaper').
card_types('cloudseeder', ['Creature']).
card_subtypes('cloudseeder', ['Faerie', 'Spellshaper']).
card_colors('cloudseeder', ['U']).
card_text('cloudseeder', 'Flying\n{U}, {T}, Discard a card: Put a 1/1 blue Faerie creature token named Cloud Sprite onto the battlefield. It has flying and \"Cloud Sprite can block only creatures with flying.\"').
card_mana_cost('cloudseeder', ['1', 'U']).
card_cmc('cloudseeder', 2).
card_layout('cloudseeder', 'normal').
card_power('cloudseeder', 1).
card_toughness('cloudseeder', 1).

% Found in: AVR
card_name('cloudshift', 'Cloudshift').
card_type('cloudshift', 'Instant').
card_types('cloudshift', ['Instant']).
card_subtypes('cloudshift', []).
card_colors('cloudshift', ['W']).
card_text('cloudshift', 'Exile target creature you control, then return that card to the battlefield under your control.').
card_mana_cost('cloudshift', ['W']).
card_cmc('cloudshift', 1).
card_layout('cloudshift', 'normal').

% Found in: NMS
card_name('cloudskate', 'Cloudskate').
card_type('cloudskate', 'Creature — Illusion').
card_types('cloudskate', ['Creature']).
card_subtypes('cloudskate', ['Illusion']).
card_colors('cloudskate', ['U']).
card_text('cloudskate', 'Flying\nFading 3 (This creature enters the battlefield with three fade counters on it. At the beginning of your upkeep, remove a fade counter from it. If you can\'t, sacrifice it.)').
card_mana_cost('cloudskate', ['1', 'U']).
card_cmc('cloudskate', 2).
card_layout('cloudskate', 'normal').
card_power('cloudskate', 2).
card_toughness('cloudskate', 2).

% Found in: RAV
card_name('cloudstone curio', 'Cloudstone Curio').
card_type('cloudstone curio', 'Artifact').
card_types('cloudstone curio', ['Artifact']).
card_subtypes('cloudstone curio', []).
card_colors('cloudstone curio', []).
card_text('cloudstone curio', 'Whenever a nonartifact permanent enters the battlefield under your control, you may return another permanent you control that shares a permanent type with it to its owner\'s hand.').
card_mana_cost('cloudstone curio', ['3']).
card_cmc('cloudstone curio', 3).
card_layout('cloudstone curio', 'normal').

% Found in: LRW
card_name('cloudthresher', 'Cloudthresher').
card_type('cloudthresher', 'Creature — Elemental').
card_types('cloudthresher', ['Creature']).
card_subtypes('cloudthresher', ['Elemental']).
card_colors('cloudthresher', ['G']).
card_text('cloudthresher', 'Flash\nReach (This creature can block creatures with flying.)\nWhen Cloudthresher enters the battlefield, it deals 2 damage to each creature with flying and each player.\nEvoke {2}{G}{G} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('cloudthresher', ['2', 'G', 'G', 'G', 'G']).
card_cmc('cloudthresher', 6).
card_layout('cloudthresher', 'normal').
card_power('cloudthresher', 7).
card_toughness('cloudthresher', 7).

% Found in: EVE
card_name('clout of the dominus', 'Clout of the Dominus').
card_type('clout of the dominus', 'Enchantment — Aura').
card_types('clout of the dominus', ['Enchantment']).
card_subtypes('clout of the dominus', ['Aura']).
card_colors('clout of the dominus', ['U', 'R']).
card_text('clout of the dominus', 'Enchant creature\nAs long as enchanted creature is blue, it gets +1/+1 and has shroud. (It can\'t be the target of spells or abilities.)\nAs long as enchanted creature is red, it gets +1/+1 and has haste.').
card_mana_cost('clout of the dominus', ['U/R']).
card_cmc('clout of the dominus', 1).
card_layout('clout of the dominus', 'normal').

% Found in: ARB
card_name('cloven casting', 'Cloven Casting').
card_type('cloven casting', 'Enchantment').
card_types('cloven casting', ['Enchantment']).
card_subtypes('cloven casting', []).
card_colors('cloven casting', ['U', 'R']).
card_text('cloven casting', 'Whenever you cast a multicolored instant or sorcery spell, you may pay {1}. If you do, copy that spell. You may choose new targets for the copy.').
card_mana_cost('cloven casting', ['5', 'U', 'R']).
card_cmc('cloven casting', 7).
card_layout('cloven casting', 'normal').

% Found in: BFZ
card_name('clutch of currents', 'Clutch of Currents').
card_type('clutch of currents', 'Sorcery').
card_types('clutch of currents', ['Sorcery']).
card_subtypes('clutch of currents', []).
card_colors('clutch of currents', ['U']).
card_text('clutch of currents', 'Return target creature to its owner\'s hand.\nAwaken 3—{4}{U} (If you cast this spell for {4}{U}, also put three +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('clutch of currents', ['U']).
card_cmc('clutch of currents', 1).
card_layout('clutch of currents', 'normal').

% Found in: RAV
card_name('clutch of the undercity', 'Clutch of the Undercity').
card_type('clutch of the undercity', 'Instant').
card_types('clutch of the undercity', ['Instant']).
card_subtypes('clutch of the undercity', []).
card_colors('clutch of the undercity', ['U', 'B']).
card_text('clutch of the undercity', 'Return target permanent to its owner\'s hand. Its controller loses 3 life.\nTransmute {1}{U}{B} ({1}{U}{B}, Discard this card: Search your library for a card with the same converted mana cost as this card, reveal it, and put it into your hand. Then shuffle your library. Transmute only as a sorcery.)').
card_mana_cost('clutch of the undercity', ['1', 'U', 'U', 'B']).
card_cmc('clutch of the undercity', 4).
card_layout('clutch of the undercity', 'normal').

% Found in: SCG
card_name('clutch of undeath', 'Clutch of Undeath').
card_type('clutch of undeath', 'Enchantment — Aura').
card_types('clutch of undeath', ['Enchantment']).
card_subtypes('clutch of undeath', ['Aura']).
card_colors('clutch of undeath', ['B']).
card_text('clutch of undeath', 'Enchant creature\nEnchanted creature gets +3/+3 as long as it\'s a Zombie. Otherwise, it gets -3/-3.').
card_mana_cost('clutch of undeath', ['3', 'B', 'B']).
card_cmc('clutch of undeath', 5).
card_layout('clutch of undeath', 'normal').

% Found in: DRK, ME3
card_name('coal golem', 'Coal Golem').
card_type('coal golem', 'Artifact Creature — Golem').
card_types('coal golem', ['Artifact', 'Creature']).
card_subtypes('coal golem', ['Golem']).
card_colors('coal golem', []).
card_text('coal golem', '{3}, Sacrifice Coal Golem: Add {R}{R}{R} to your mana pool.').
card_mana_cost('coal golem', ['5']).
card_cmc('coal golem', 5).
card_layout('coal golem', 'normal').
card_power('coal golem', 3).
card_toughness('coal golem', 3).

% Found in: DDK, TSP
card_name('coal stoker', 'Coal Stoker').
card_type('coal stoker', 'Creature — Elemental').
card_types('coal stoker', ['Creature']).
card_subtypes('coal stoker', ['Elemental']).
card_colors('coal stoker', ['R']).
card_text('coal stoker', 'When Coal Stoker enters the battlefield, if you cast it from your hand, add {R}{R}{R} to your mana pool.').
card_mana_cost('coal stoker', ['3', 'R']).
card_cmc('coal stoker', 4).
card_layout('coal stoker', 'normal').
card_power('coal stoker', 3).
card_toughness('coal stoker', 3).

% Found in: RAV
card_name('coalhauler swine', 'Coalhauler Swine').
card_type('coalhauler swine', 'Creature — Boar Beast').
card_types('coalhauler swine', ['Creature']).
card_subtypes('coalhauler swine', ['Boar', 'Beast']).
card_colors('coalhauler swine', ['R']).
card_text('coalhauler swine', 'Whenever Coalhauler Swine is dealt damage, it deals that much damage to each player.').
card_mana_cost('coalhauler swine', ['4', 'R', 'R']).
card_cmc('coalhauler swine', 6).
card_layout('coalhauler swine', 'normal').
card_power('coalhauler swine', 4).
card_toughness('coalhauler swine', 4).

% Found in: APC
card_name('coalition flag', 'Coalition Flag').
card_type('coalition flag', 'Enchantment — Aura').
card_types('coalition flag', ['Enchantment']).
card_subtypes('coalition flag', ['Aura']).
card_colors('coalition flag', ['W']).
card_text('coalition flag', 'Enchant creature you control\nEnchanted creature is a Flagbearer.\nWhile choosing targets as part of casting a spell or activating an ability, your opponents must choose at least one Flagbearer on the battlefield if able.').
card_mana_cost('coalition flag', ['W']).
card_cmc('coalition flag', 1).
card_layout('coalition flag', 'normal').

% Found in: APC
card_name('coalition honor guard', 'Coalition Honor Guard').
card_type('coalition honor guard', 'Creature — Human Flagbearer').
card_types('coalition honor guard', ['Creature']).
card_subtypes('coalition honor guard', ['Human', 'Flagbearer']).
card_colors('coalition honor guard', ['W']).
card_text('coalition honor guard', 'While choosing targets as part of casting a spell or activating an ability, your opponents must choose at least one Flagbearer on the battlefield if able.').
card_mana_cost('coalition honor guard', ['3', 'W']).
card_cmc('coalition honor guard', 4).
card_layout('coalition honor guard', 'normal').
card_power('coalition honor guard', 2).
card_toughness('coalition honor guard', 4).

% Found in: DDE, FUT
card_name('coalition relic', 'Coalition Relic').
card_type('coalition relic', 'Artifact').
card_types('coalition relic', ['Artifact']).
card_subtypes('coalition relic', []).
card_colors('coalition relic', []).
card_text('coalition relic', '{T}: Add one mana of any color to your mana pool.\n{T}: Put a charge counter on Coalition Relic.\nAt the beginning of your precombat main phase, remove all charge counters from Coalition Relic. Add one mana of any color to your mana pool for each charge counter removed this way.').
card_mana_cost('coalition relic', ['3']).
card_cmc('coalition relic', 3).
card_layout('coalition relic', 'normal').

% Found in: INV, TSB
card_name('coalition victory', 'Coalition Victory').
card_type('coalition victory', 'Sorcery').
card_types('coalition victory', ['Sorcery']).
card_subtypes('coalition victory', []).
card_colors('coalition victory', ['W', 'U', 'B', 'R', 'G']).
card_text('coalition victory', 'You win the game if you control a land of each basic land type and a creature of each color.').
card_mana_cost('coalition victory', ['3', 'W', 'U', 'B', 'R', 'G']).
card_cmc('coalition victory', 8).
card_layout('coalition victory', 'normal').

% Found in: SCG
card_name('coast watcher', 'Coast Watcher').
card_type('coast watcher', 'Creature — Bird Soldier').
card_types('coast watcher', ['Creature']).
card_subtypes('coast watcher', ['Bird', 'Soldier']).
card_colors('coast watcher', ['U']).
card_text('coast watcher', 'Flying, protection from green').
card_mana_cost('coast watcher', ['1', 'U']).
card_cmc('coast watcher', 2).
card_layout('coast watcher', 'normal').
card_power('coast watcher', 1).
card_toughness('coast watcher', 1).

% Found in: BFZ
card_name('coastal discovery', 'Coastal Discovery').
card_type('coastal discovery', 'Sorcery').
card_types('coastal discovery', ['Sorcery']).
card_subtypes('coastal discovery', []).
card_colors('coastal discovery', ['U']).
card_text('coastal discovery', 'Draw two cards.\nAwaken 4—{5}{U} (If you cast this spell for {5}{U}, also put four +1/+1 counters on target land you control and it becomes a 0/0 Elemental creature with haste. It\'s still a land.)').
card_mana_cost('coastal discovery', ['3', 'U']).
card_cmc('coastal discovery', 4).
card_layout('coastal discovery', 'normal').

% Found in: APC
card_name('coastal drake', 'Coastal Drake').
card_type('coastal drake', 'Creature — Drake').
card_types('coastal drake', ['Creature']).
card_subtypes('coastal drake', ['Drake']).
card_colors('coastal drake', ['U']).
card_text('coastal drake', 'Flying\n{1}{U}, {T}: Return target Kavu to its owner\'s hand.').
card_mana_cost('coastal drake', ['2', 'U']).
card_cmc('coastal drake', 3).
card_layout('coastal drake', 'normal').
card_power('coastal drake', 2).
card_toughness('coastal drake', 1).

% Found in: 8ED, PCY
card_name('coastal hornclaw', 'Coastal Hornclaw').
card_type('coastal hornclaw', 'Creature — Bird').
card_types('coastal hornclaw', ['Creature']).
card_subtypes('coastal hornclaw', ['Bird']).
card_colors('coastal hornclaw', ['U']).
card_text('coastal hornclaw', 'Sacrifice a land: Coastal Hornclaw gains flying until end of turn.').
card_mana_cost('coastal hornclaw', ['4', 'U']).
card_cmc('coastal hornclaw', 5).
card_layout('coastal hornclaw', 'normal').
card_power('coastal hornclaw', 3).
card_toughness('coastal hornclaw', 3).

% Found in: 8ED, MMQ
card_name('coastal piracy', 'Coastal Piracy').
card_type('coastal piracy', 'Enchantment').
card_types('coastal piracy', ['Enchantment']).
card_subtypes('coastal piracy', []).
card_colors('coastal piracy', ['U']).
card_text('coastal piracy', 'Whenever a creature you control deals combat damage to an opponent, you may draw a card.').
card_mana_cost('coastal piracy', ['2', 'U', 'U']).
card_cmc('coastal piracy', 4).
card_layout('coastal piracy', 'normal').

% Found in: 8ED, INV
card_name('coastal tower', 'Coastal Tower').
card_type('coastal tower', 'Land').
card_types('coastal tower', ['Land']).
card_subtypes('coastal tower', []).
card_colors('coastal tower', []).
card_text('coastal tower', 'Coastal Tower enters the battlefield tapped.\n{T}: Add {W} or {U} to your mana pool.').
card_layout('coastal tower', 'normal').

% Found in: PO2
card_name('coastal wizard', 'Coastal Wizard').
card_type('coastal wizard', 'Creature — Human Wizard').
card_types('coastal wizard', ['Creature']).
card_subtypes('coastal wizard', ['Human', 'Wizard']).
card_colors('coastal wizard', ['U']).
card_text('coastal wizard', '{T}: Return Coastal Wizard and another target creature to their owners\' hands. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('coastal wizard', ['2', 'U', 'U']).
card_cmc('coastal wizard', 4).
card_layout('coastal wizard', 'normal').
card_power('coastal wizard', 1).
card_toughness('coastal wizard', 1).

% Found in: THS
card_name('coastline chimera', 'Coastline Chimera').
card_type('coastline chimera', 'Creature — Chimera').
card_types('coastline chimera', ['Creature']).
card_subtypes('coastline chimera', ['Chimera']).
card_colors('coastline chimera', ['U']).
card_text('coastline chimera', 'Flying\n{1}{W}: Coastline Chimera can block an additional creature this turn.').
card_mana_cost('coastline chimera', ['3', 'U']).
card_cmc('coastline chimera', 4).
card_layout('coastline chimera', 'normal').
card_power('coastline chimera', 1).
card_toughness('coastline chimera', 5).

% Found in: 10E, 7ED, 8ED, 9ED, DPA, EXO, H09, M10, TPR
card_name('coat of arms', 'Coat of Arms').
card_type('coat of arms', 'Artifact').
card_types('coat of arms', ['Artifact']).
card_subtypes('coat of arms', []).
card_colors('coat of arms', []).
card_text('coat of arms', 'Each creature gets +1/+1 for each other creature on the battlefield that shares at least one creature type with it. (For example, if two Goblin Warriors and a Goblin Shaman are on the battlefield, each gets +2/+2.)').
card_mana_cost('coat of arms', ['5']).
card_cmc('coat of arms', 5).
card_layout('coat of arms', 'normal').

% Found in: DTK
card_name('coat with venom', 'Coat with Venom').
card_type('coat with venom', 'Instant').
card_types('coat with venom', ['Instant']).
card_subtypes('coat with venom', []).
card_colors('coat with venom', ['B']).
card_text('coat with venom', 'Target creature gets +1/+2 and gains deathtouch until end of turn. (Any amount of damage it deals to a creature is enough to destroy it.)').
card_mana_cost('coat with venom', ['B']).
card_cmc('coat with venom', 1).
card_layout('coat with venom', 'normal').

% Found in: MRD
card_name('cobalt golem', 'Cobalt Golem').
card_type('cobalt golem', 'Artifact Creature — Golem').
card_types('cobalt golem', ['Artifact', 'Creature']).
card_subtypes('cobalt golem', ['Golem']).
card_colors('cobalt golem', []).
card_text('cobalt golem', '{1}{U}: Cobalt Golem gains flying until end of turn.').
card_mana_cost('cobalt golem', ['4']).
card_cmc('cobalt golem', 4).
card_layout('cobalt golem', 'normal').
card_power('cobalt golem', 2).
card_toughness('cobalt golem', 3).

% Found in: ORI, RTR
card_name('cobblebrute', 'Cobblebrute').
card_type('cobblebrute', 'Creature — Elemental').
card_types('cobblebrute', ['Creature']).
card_subtypes('cobblebrute', ['Elemental']).
card_colors('cobblebrute', ['R']).
card_text('cobblebrute', '').
card_mana_cost('cobblebrute', ['3', 'R']).
card_cmc('cobblebrute', 4).
card_layout('cobblebrute', 'normal').
card_power('cobblebrute', 5).
card_toughness('cobblebrute', 2).

% Found in: ISD
card_name('cobbled wings', 'Cobbled Wings').
card_type('cobbled wings', 'Artifact — Equipment').
card_types('cobbled wings', ['Artifact']).
card_subtypes('cobbled wings', ['Equipment']).
card_colors('cobbled wings', []).
card_text('cobbled wings', 'Equipped creature has flying.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('cobbled wings', ['2']).
card_cmc('cobbled wings', 2).
card_layout('cobbled wings', 'normal').

% Found in: CMD, ZEN
card_name('cobra trap', 'Cobra Trap').
card_type('cobra trap', 'Instant — Trap').
card_types('cobra trap', ['Instant']).
card_subtypes('cobra trap', ['Trap']).
card_colors('cobra trap', ['G']).
card_text('cobra trap', 'If a noncreature permanent under your control was destroyed this turn by a spell or ability an opponent controlled, you may pay {G} rather than pay Cobra Trap\'s mana cost.\nPut four 1/1 green Snake creature tokens onto the battlefield.').
card_mana_cost('cobra trap', ['4', 'G', 'G']).
card_cmc('cobra trap', 6).
card_layout('cobra trap', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, LEA, LEB, TSB
card_name('cockatrice', 'Cockatrice').
card_type('cockatrice', 'Creature — Cockatrice').
card_types('cockatrice', ['Creature']).
card_subtypes('cockatrice', ['Cockatrice']).
card_colors('cockatrice', ['G']).
card_text('cockatrice', 'Flying\nWhenever Cockatrice blocks or becomes blocked by a non-Wall creature, destroy that creature at end of combat.').
card_mana_cost('cockatrice', ['3', 'G', 'G']).
card_cmc('cockatrice', 5).
card_layout('cockatrice', 'normal').
card_power('cockatrice', 2).
card_toughness('cockatrice', 4).

% Found in: CHR, LEG
card_name('cocoon', 'Cocoon').
card_type('cocoon', 'Enchantment — Aura').
card_types('cocoon', ['Enchantment']).
card_subtypes('cocoon', ['Aura']).
card_colors('cocoon', ['G']).
card_text('cocoon', 'Enchant creature you control\nEnchanted creature doesn\'t untap during your untap step if Cocoon has a pupa counter on it.\nWhen Cocoon enters the battlefield, tap enchanted creature and put three pupa counters on Cocoon.\nAt the beginning of your upkeep, remove a pupa counter from Cocoon. If you can\'t, sacrifice it, put a +1/+1 counter on enchanted creature, and that creature gains flying. (This effect lasts indefinitely.)').
card_mana_cost('cocoon', ['G']).
card_cmc('cocoon', 1).
card_layout('cocoon', 'normal').

% Found in: RTR
card_name('codex shredder', 'Codex Shredder').
card_type('codex shredder', 'Artifact').
card_types('codex shredder', ['Artifact']).
card_subtypes('codex shredder', []).
card_colors('codex shredder', []).
card_text('codex shredder', '{T}: Target player puts the top card of his or her library into his or her graveyard.\n{5}, {T}, Sacrifice Codex Shredder: Return target card from your graveyard to your hand.').
card_mana_cost('codex shredder', ['1']).
card_cmc('codex shredder', 1).
card_layout('codex shredder', 'normal').

% Found in: GTC
card_name('coerced confession', 'Coerced Confession').
card_type('coerced confession', 'Sorcery').
card_types('coerced confession', ['Sorcery']).
card_subtypes('coerced confession', []).
card_colors('coerced confession', ['U', 'B']).
card_text('coerced confession', 'Target player puts the top four cards of his or her library into his or her graveyard. You draw a card for each creature card put into that graveyard this way.').
card_mana_cost('coerced confession', ['4', 'U/B']).
card_cmc('coerced confession', 5).
card_layout('coerced confession', 'normal').

% Found in: 6ED, 8ED, 9ED, BTD, PO2, PTK, S00, S99, TMP, TPR, VIS
card_name('coercion', 'Coercion').
card_type('coercion', 'Sorcery').
card_types('coercion', ['Sorcery']).
card_subtypes('coercion', []).
card_colors('coercion', ['B']).
card_text('coercion', 'Target opponent reveals his or her hand. You choose a card from it. That player discards that card.').
card_mana_cost('coercion', ['2', 'B']).
card_cmc('coercion', 3).
card_layout('coercion', 'normal').

% Found in: CNS, VMA
card_name('coercive portal', 'Coercive Portal').
card_type('coercive portal', 'Artifact').
card_types('coercive portal', ['Artifact']).
card_subtypes('coercive portal', []).
card_colors('coercive portal', []).
card_text('coercive portal', 'Will of the council — At the beginning of your upkeep, starting with you, each player votes for carnage or homage. If carnage gets more votes, sacrifice Coercive Portal and destroy all nonland permanents. If homage gets more votes or the vote is tied, draw a card.').
card_mana_cost('coercive portal', ['4']).
card_cmc('coercive portal', 4).
card_layout('coercive portal', 'normal').

% Found in: PCY
card_name('coffin puppets', 'Coffin Puppets').
card_type('coffin puppets', 'Creature — Zombie').
card_types('coffin puppets', ['Creature']).
card_subtypes('coffin puppets', ['Zombie']).
card_colors('coffin puppets', ['B']).
card_text('coffin puppets', 'Sacrifice two lands: Return Coffin Puppets from your graveyard to the battlefield. Activate this ability only during your upkeep and only if you control a Swamp.').
card_mana_cost('coffin puppets', ['3', 'B', 'B']).
card_cmc('coffin puppets', 5).
card_layout('coffin puppets', 'normal').
card_power('coffin puppets', 3).
card_toughness('coffin puppets', 3).

% Found in: ODY
card_name('coffin purge', 'Coffin Purge').
card_type('coffin purge', 'Instant').
card_types('coffin purge', ['Instant']).
card_subtypes('coffin purge', []).
card_colors('coffin purge', ['B']).
card_text('coffin purge', 'Exile target card from a graveyard.\nFlashback {B} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('coffin purge', ['B']).
card_cmc('coffin purge', 1).
card_layout('coffin purge', 'normal').

% Found in: TMP, TPR
card_name('coffin queen', 'Coffin Queen').
card_type('coffin queen', 'Creature — Zombie Wizard').
card_types('coffin queen', ['Creature']).
card_subtypes('coffin queen', ['Zombie', 'Wizard']).
card_colors('coffin queen', ['B']).
card_text('coffin queen', 'You may choose not to untap Coffin Queen during your untap step.\n{2}{B}, {T}: Put target creature card from a graveyard onto the battlefield under your control. When Coffin Queen becomes untapped or you lose control of Coffin Queen, exile that creature.').
card_mana_cost('coffin queen', ['2', 'B']).
card_cmc('coffin queen', 3).
card_layout('coffin queen', 'normal').
card_power('coffin queen', 1).
card_toughness('coffin queen', 1).

% Found in: ODY
card_name('cognivore', 'Cognivore').
card_type('cognivore', 'Creature — Lhurgoyf').
card_types('cognivore', ['Creature']).
card_subtypes('cognivore', ['Lhurgoyf']).
card_colors('cognivore', ['U']).
card_text('cognivore', 'Flying\nCognivore\'s power and toughness are each equal to the number of instant cards in all graveyards.').
card_mana_cost('cognivore', ['6', 'U', 'U']).
card_cmc('cognivore', 8).
card_layout('cognivore', 'normal').
card_power('cognivore', '*').
card_toughness('cognivore', '*').

% Found in: CNS
card_name('cogwork grinder', 'Cogwork Grinder').
card_type('cogwork grinder', 'Artifact Creature — Construct').
card_types('cogwork grinder', ['Artifact', 'Creature']).
card_subtypes('cogwork grinder', ['Construct']).
card_colors('cogwork grinder', []).
card_text('cogwork grinder', 'Draft Cogwork Grinder face up.\nAs you draft a card, you may remove it from the draft face down. (Those cards aren\'t in your card pool.)\nCogwork Grinder enters the battlefield with X +1/+1 counters on it, where X is the number of cards you removed from the draft with cards named Cogwork Grinder.').
card_mana_cost('cogwork grinder', ['6']).
card_cmc('cogwork grinder', 6).
card_layout('cogwork grinder', 'normal').
card_power('cogwork grinder', 0).
card_toughness('cogwork grinder', 0).

% Found in: CNS
card_name('cogwork librarian', 'Cogwork Librarian').
card_type('cogwork librarian', 'Artifact Creature — Construct').
card_types('cogwork librarian', ['Artifact', 'Creature']).
card_subtypes('cogwork librarian', ['Construct']).
card_colors('cogwork librarian', []).
card_text('cogwork librarian', 'Draft Cogwork Librarian face up.\nAs you draft a card, you may draft an additional card from that booster pack. If you do, put Cogwork Librarian into that booster pack.').
card_mana_cost('cogwork librarian', ['4']).
card_cmc('cogwork librarian', 4).
card_layout('cogwork librarian', 'normal').
card_power('cogwork librarian', 3).
card_toughness('cogwork librarian', 3).

% Found in: CNS
card_name('cogwork spy', 'Cogwork Spy').
card_type('cogwork spy', 'Artifact Creature — Bird Construct').
card_types('cogwork spy', ['Artifact', 'Creature']).
card_subtypes('cogwork spy', ['Bird', 'Construct']).
card_colors('cogwork spy', []).
card_text('cogwork spy', 'Reveal Cogwork Spy as you draft it. You may look at the next card drafted from this booster pack.\nFlying').
card_mana_cost('cogwork spy', ['3']).
card_cmc('cogwork spy', 3).
card_layout('cogwork spy', 'normal').
card_power('cogwork spy', 2).
card_toughness('cogwork spy', 1).

% Found in: CNS
card_name('cogwork tracker', 'Cogwork Tracker').
card_type('cogwork tracker', 'Artifact Creature — Hound Construct').
card_types('cogwork tracker', ['Artifact', 'Creature']).
card_subtypes('cogwork tracker', ['Hound', 'Construct']).
card_colors('cogwork tracker', []).
card_text('cogwork tracker', 'Reveal Cogwork Tracker as you draft it and note the player who passed it to you.\nCogwork Tracker attacks each turn if able.\nCogwork Tracker attacks a player you noted for cards named Cogwork Tracker each turn if able.').
card_mana_cost('cogwork tracker', ['4']).
card_cmc('cogwork tracker', 4).
card_layout('cogwork tracker', 'normal').
card_power('cogwork tracker', 4).
card_toughness('cogwork tracker', 4).

% Found in: TMP, TPR
card_name('coiled tinviper', 'Coiled Tinviper').
card_type('coiled tinviper', 'Artifact Creature — Snake').
card_types('coiled tinviper', ['Artifact', 'Creature']).
card_subtypes('coiled tinviper', ['Snake']).
card_colors('coiled tinviper', []).
card_text('coiled tinviper', 'First strike').
card_mana_cost('coiled tinviper', ['3']).
card_cmc('coiled tinviper', 3).
card_layout('coiled tinviper', 'normal').
card_power('coiled tinviper', 2).
card_toughness('coiled tinviper', 1).

% Found in: DDO, DIS, pARL
card_name('coiling oracle', 'Coiling Oracle').
card_type('coiling oracle', 'Creature — Snake Elf Druid').
card_types('coiling oracle', ['Creature']).
card_subtypes('coiling oracle', ['Snake', 'Elf', 'Druid']).
card_colors('coiling oracle', ['U', 'G']).
card_text('coiling oracle', 'When Coiling Oracle enters the battlefield, reveal the top card of your library. If it\'s a land card, put it onto the battlefield. Otherwise, put that card into your hand.').
card_mana_cost('coiling oracle', ['G', 'U']).
card_cmc('coiling oracle', 2).
card_layout('coiling oracle', 'normal').
card_power('coiling oracle', 1).
card_toughness('coiling oracle', 1).

% Found in: NMS
card_name('coiling woodworm', 'Coiling Woodworm').
card_type('coiling woodworm', 'Creature — Insect Worm').
card_types('coiling woodworm', ['Creature']).
card_subtypes('coiling woodworm', ['Insect', 'Worm']).
card_colors('coiling woodworm', ['G']).
card_text('coiling woodworm', 'Coiling Woodworm\'s power is equal to the number of Forests on the battlefield.').
card_mana_cost('coiling woodworm', ['2', 'G']).
card_cmc('coiling woodworm', 3).
card_layout('coiling woodworm', 'normal').
card_power('coiling woodworm', '*').
card_toughness('coiling woodworm', 1).

% Found in: WTH
card_name('coils of the medusa', 'Coils of the Medusa').
card_type('coils of the medusa', 'Enchantment — Aura').
card_types('coils of the medusa', ['Enchantment']).
card_subtypes('coils of the medusa', ['Aura']).
card_colors('coils of the medusa', ['B']).
card_text('coils of the medusa', 'Enchant creature\nEnchanted creature gets +1/-1.\nSacrifice Coils of the Medusa: Destroy all non-Wall creatures blocking enchanted creature.').
card_mana_cost('coils of the medusa', ['1', 'B']).
card_cmc('coils of the medusa', 2).
card_layout('coils of the medusa', 'normal').

% Found in: ICE
card_name('cold snap', 'Cold Snap').
card_type('cold snap', 'Enchantment').
card_types('cold snap', ['Enchantment']).
card_subtypes('cold snap', []).
card_colors('cold snap', ['W']).
card_text('cold snap', 'Cumulative upkeep {2} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of each player\'s upkeep, Cold Snap deals damage to that player equal to the number of snow lands he or she controls.').
card_mana_cost('cold snap', ['2', 'W']).
card_cmc('cold snap', 3).
card_layout('cold snap', 'normal').

% Found in: TMP
card_name('cold storage', 'Cold Storage').
card_type('cold storage', 'Artifact').
card_types('cold storage', ['Artifact']).
card_subtypes('cold storage', []).
card_colors('cold storage', []).
card_text('cold storage', '{3}: Exile target creature you control.\nSacrifice Cold Storage: Return each creature card exiled with Cold Storage to the battlefield under your control.').
card_mana_cost('cold storage', ['4']).
card_cmc('cold storage', 4).
card_layout('cold storage', 'normal').

% Found in: EVE, MMA
card_name('cold-eyed selkie', 'Cold-Eyed Selkie').
card_type('cold-eyed selkie', 'Creature — Merfolk Rogue').
card_types('cold-eyed selkie', ['Creature']).
card_subtypes('cold-eyed selkie', ['Merfolk', 'Rogue']).
card_colors('cold-eyed selkie', ['U', 'G']).
card_text('cold-eyed selkie', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhenever Cold-Eyed Selkie deals combat damage to a player, you may draw that many cards.').
card_mana_cost('cold-eyed selkie', ['1', 'G/U', 'G/U']).
card_cmc('cold-eyed selkie', 3).
card_layout('cold-eyed selkie', 'normal').
card_power('cold-eyed selkie', 1).
card_toughness('cold-eyed selkie', 1).

% Found in: CSP
card_name('coldsteel heart', 'Coldsteel Heart').
card_type('coldsteel heart', 'Snow Artifact').
card_types('coldsteel heart', ['Artifact']).
card_subtypes('coldsteel heart', []).
card_supertypes('coldsteel heart', ['Snow']).
card_colors('coldsteel heart', []).
card_text('coldsteel heart', 'Coldsteel Heart enters the battlefield tapped.\nAs Coldsteel Heart enters the battlefield, choose a color.\n{T}: Add one mana of the chosen color to your mana pool.').
card_mana_cost('coldsteel heart', ['2']).
card_cmc('coldsteel heart', 2).
card_layout('coldsteel heart', 'normal').

% Found in: LRW
card_name('colfenor\'s plans', 'Colfenor\'s Plans').
card_type('colfenor\'s plans', 'Enchantment').
card_types('colfenor\'s plans', ['Enchantment']).
card_subtypes('colfenor\'s plans', []).
card_colors('colfenor\'s plans', ['B']).
card_text('colfenor\'s plans', 'When Colfenor\'s Plans enters the battlefield, exile the top seven cards of your library face down.\nYou may look at and play cards exiled with Colfenor\'s Plans.\nSkip your draw step.\nYou can\'t cast more than one spell each turn.').
card_mana_cost('colfenor\'s plans', ['2', 'B', 'B']).
card_cmc('colfenor\'s plans', 4).
card_layout('colfenor\'s plans', 'normal').

% Found in: LRW
card_name('colfenor\'s urn', 'Colfenor\'s Urn').
card_type('colfenor\'s urn', 'Artifact').
card_types('colfenor\'s urn', ['Artifact']).
card_subtypes('colfenor\'s urn', []).
card_colors('colfenor\'s urn', []).
card_text('colfenor\'s urn', 'Whenever a creature with toughness 4 or greater is put into your graveyard from the battlefield, you may exile it.\nAt the beginning of the end step, if three or more cards have been exiled with Colfenor\'s Urn, sacrifice it. If you do, return those cards to the battlefield under their owner\'s control.').
card_mana_cost('colfenor\'s urn', ['3']).
card_cmc('colfenor\'s urn', 3).
card_layout('colfenor\'s urn', 'normal').

% Found in: INV
card_name('collapsing borders', 'Collapsing Borders').
card_type('collapsing borders', 'Enchantment').
card_types('collapsing borders', ['Enchantment']).
card_subtypes('collapsing borders', []).
card_colors('collapsing borders', ['R']).
card_text('collapsing borders', 'Domain — At the beginning of each player\'s upkeep, that player gains 1 life for each basic land type among lands he or she controls. Then Collapsing Borders deals 3 damage to him or her.').
card_mana_cost('collapsing borders', ['3', 'R']).
card_cmc('collapsing borders', 4).
card_layout('collapsing borders', 'normal').

% Found in: FRF
card_name('collateral damage', 'Collateral Damage').
card_type('collateral damage', 'Instant').
card_types('collateral damage', ['Instant']).
card_subtypes('collateral damage', []).
card_colors('collateral damage', ['R']).
card_text('collateral damage', 'As an additional cost to cast Collateral Damage, sacrifice a creature.\nCollateral Damage deals 3 damage to target creature or player.').
card_mana_cost('collateral damage', ['R']).
card_cmc('collateral damage', 1).
card_layout('collateral damage', 'normal').

% Found in: DTK
card_name('collected company', 'Collected Company').
card_type('collected company', 'Instant').
card_types('collected company', ['Instant']).
card_subtypes('collected company', []).
card_colors('collected company', ['G']).
card_text('collected company', 'Look at the top six cards of your library. Put up to two creature cards with converted mana cost 3 or less from among them onto the battlefield. Put the rest on the bottom of your library in any order.').
card_mana_cost('collected company', ['3', 'G']).
card_cmc('collected company', 4).
card_layout('collected company', 'normal').

% Found in: RTR
card_name('collective blessing', 'Collective Blessing').
card_type('collective blessing', 'Enchantment').
card_types('collective blessing', ['Enchantment']).
card_subtypes('collective blessing', []).
card_colors('collective blessing', ['W', 'G']).
card_text('collective blessing', 'Creatures you control get +3/+3.').
card_mana_cost('collective blessing', ['3', 'G', 'G', 'W']).
card_cmc('collective blessing', 6).
card_layout('collective blessing', 'normal').

% Found in: INV
card_name('collective restraint', 'Collective Restraint').
card_type('collective restraint', 'Enchantment').
card_types('collective restraint', ['Enchantment']).
card_subtypes('collective restraint', []).
card_colors('collective restraint', ['U']).
card_text('collective restraint', 'Domain — Creatures can\'t attack you unless their controller pays {X} for each creature he or she controls that\'s attacking you, where X is the number of basic land types among lands you control.').
card_mana_cost('collective restraint', ['3', 'U']).
card_cmc('collective restraint', 4).
card_layout('collective restraint', 'normal').

% Found in: 8ED, C14, MMQ
card_name('collective unconscious', 'Collective Unconscious').
card_type('collective unconscious', 'Sorcery').
card_types('collective unconscious', ['Sorcery']).
card_subtypes('collective unconscious', []).
card_colors('collective unconscious', ['G']).
card_text('collective unconscious', 'Draw a card for each creature you control.').
card_mana_cost('collective unconscious', ['4', 'G', 'G']).
card_cmc('collective unconscious', 6).
card_layout('collective unconscious', 'normal').

% Found in: CMD
card_name('collective voyage', 'Collective Voyage').
card_type('collective voyage', 'Sorcery').
card_types('collective voyage', ['Sorcery']).
card_subtypes('collective voyage', []).
card_colors('collective voyage', ['G']).
card_text('collective voyage', 'Join forces — Starting with you, each player may pay any amount of mana. Each player searches his or her library for up to X basic land cards, where X is the total amount of mana paid this way, puts them onto the battlefield tapped, then shuffles his or her library.').
card_mana_cost('collective voyage', ['G']).
card_cmc('collective voyage', 1).
card_layout('collective voyage', 'normal').

% Found in: UNH
card_name('collector protector', 'Collector Protector').
card_type('collector protector', 'Creature — Human Gamer').
card_types('collector protector', ['Creature']).
card_subtypes('collector protector', ['Human', 'Gamer']).
card_colors('collector protector', ['W']).
card_text('collector protector', '{W}, Give an opponent a nonland card you own from outside the game: Prevent the next 1 damage that would be dealt to you or Collector Protector this turn.').
card_mana_cost('collector protector', ['3', 'W', 'W']).
card_cmc('collector protector', 5).
card_layout('collector protector', 'normal').
card_power('collector protector', 2).
card_toughness('collector protector', 5).

% Found in: UDS
card_name('colos yearling', 'Colos Yearling').
card_type('colos yearling', 'Creature — Goat Beast').
card_types('colos yearling', ['Creature']).
card_subtypes('colos yearling', ['Goat', 'Beast']).
card_colors('colos yearling', ['R']).
card_text('colos yearling', 'Mountainwalk (This creature can\'t be blocked as long as defending player controls a Mountain.)\n{R}: Colos Yearling gets +1/+0 until end of turn.').
card_mana_cost('colos yearling', ['2', 'R']).
card_cmc('colos yearling', 3).
card_layout('colos yearling', 'normal').
card_power('colos yearling', 1).
card_toughness('colos yearling', 1).

% Found in: JOU
card_name('colossal heroics', 'Colossal Heroics').
card_type('colossal heroics', 'Instant').
card_types('colossal heroics', ['Instant']).
card_subtypes('colossal heroics', []).
card_colors('colossal heroics', ['G']).
card_text('colossal heroics', 'Strive — Colossal Heroics costs {1}{G} more to cast for each target beyond the first.\nAny number of target creatures each get +2/+2 until end of turn. Untap those creatures.').
card_mana_cost('colossal heroics', ['2', 'G']).
card_cmc('colossal heroics', 3).
card_layout('colossal heroics', 'normal').

% Found in: ARB, ARC, CMD
card_name('colossal might', 'Colossal Might').
card_type('colossal might', 'Instant').
card_types('colossal might', ['Instant']).
card_subtypes('colossal might', []).
card_colors('colossal might', ['R', 'G']).
card_text('colossal might', 'Target creature gets +4/+2 and gains trample until end of turn.').
card_mana_cost('colossal might', ['R', 'G']).
card_cmc('colossal might', 2).
card_layout('colossal might', 'normal').

% Found in: M14, pLPA
card_name('colossal whale', 'Colossal Whale').
card_type('colossal whale', 'Creature — Whale').
card_types('colossal whale', ['Creature']).
card_subtypes('colossal whale', ['Whale']).
card_colors('colossal whale', ['U']).
card_text('colossal whale', 'Islandwalk (This creature can\'t be blocked as long as defending player controls an Island.)\nWhenever Colossal Whale attacks, you may exile target creature defending player controls until Colossal Whale leaves the battlefield. (That creature returns under its owner\'s control.)').
card_mana_cost('colossal whale', ['5', 'U', 'U']).
card_cmc('colossal whale', 7).
card_layout('colossal whale', 'normal').
card_power('colossal whale', 5).
card_toughness('colossal whale', 5).

% Found in: DTK
card_name('colossodon yearling', 'Colossodon Yearling').
card_type('colossodon yearling', 'Creature — Beast').
card_types('colossodon yearling', ['Creature']).
card_subtypes('colossodon yearling', ['Beast']).
card_colors('colossodon yearling', ['G']).
card_text('colossodon yearling', '').
card_mana_cost('colossodon yearling', ['2', 'G']).
card_cmc('colossodon yearling', 3).
card_layout('colossodon yearling', 'normal').
card_power('colossodon yearling', 2).
card_toughness('colossodon yearling', 4).

% Found in: THS
card_name('colossus of akros', 'Colossus of Akros').
card_type('colossus of akros', 'Artifact Creature — Golem').
card_types('colossus of akros', ['Artifact', 'Creature']).
card_subtypes('colossus of akros', ['Golem']).
card_colors('colossus of akros', []).
card_text('colossus of akros', 'Defender, indestructible\n{10}: Monstrosity 10. (If this creature isn\'t monstrous, put ten +1/+1 counters on it and it becomes monstrous.)\nAs long as Colossus of Akros is monstrous, it has trample and can attack as though it didn\'t have defender.').
card_mana_cost('colossus of akros', ['8']).
card_cmc('colossus of akros', 8).
card_layout('colossus of akros', 'normal').
card_power('colossus of akros', 10).
card_toughness('colossus of akros', 10).

% Found in: 10E, 4ED, 5ED, ATQ, ME4
card_name('colossus of sardia', 'Colossus of Sardia').
card_type('colossus of sardia', 'Artifact Creature — Golem').
card_types('colossus of sardia', ['Artifact', 'Creature']).
card_subtypes('colossus of sardia', ['Golem']).
card_colors('colossus of sardia', []).
card_text('colossus of sardia', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nColossus of Sardia doesn\'t untap during your untap step.\n{9}: Untap Colossus of Sardia. Activate this ability only during your upkeep.').
card_mana_cost('colossus of sardia', ['9']).
card_cmc('colossus of sardia', 9).
card_layout('colossus of sardia', 'normal').
card_power('colossus of sardia', 9).
card_toughness('colossus of sardia', 9).

% Found in: ALA
card_name('coma veil', 'Coma Veil').
card_type('coma veil', 'Enchantment — Aura').
card_types('coma veil', ['Enchantment']).
card_subtypes('coma veil', ['Aura']).
card_colors('coma veil', ['U']).
card_text('coma veil', 'Enchant artifact or creature\nEnchanted permanent doesn\'t untap during its controller\'s untap step.').
card_mana_cost('coma veil', ['4', 'U']).
card_cmc('coma veil', 5).
card_layout('coma veil', 'normal').

% Found in: ATH, FEM, ME2
card_name('combat medic', 'Combat Medic').
card_type('combat medic', 'Creature — Human Cleric Soldier').
card_types('combat medic', ['Creature']).
card_subtypes('combat medic', ['Human', 'Cleric', 'Soldier']).
card_colors('combat medic', ['W']).
card_text('combat medic', '{1}{W}: Prevent the next 1 damage that would be dealt to target creature or player this turn.').
card_mana_cost('combat medic', ['2', 'W']).
card_cmc('combat medic', 3).
card_layout('combat medic', 'normal').
card_power('combat medic', 0).
card_toughness('combat medic', 2).

% Found in: M11, M12, MM2
card_name('combust', 'Combust').
card_type('combust', 'Instant').
card_types('combust', ['Instant']).
card_subtypes('combust', []).
card_colors('combust', ['R']).
card_text('combust', 'Combust can\'t be countered by spells or abilities.\nCombust deals 5 damage to target white or blue creature. The damage can\'t be prevented.').
card_mana_cost('combust', ['1', 'R']).
card_cmc('combust', 2).
card_layout('combust', 'normal').

% Found in: CMD, MM2, WWK, pPRE
card_name('comet storm', 'Comet Storm').
card_type('comet storm', 'Instant').
card_types('comet storm', ['Instant']).
card_subtypes('comet storm', []).
card_colors('comet storm', ['R']).
card_text('comet storm', 'Multikicker {1} (You may pay an additional {1} any number of times as you cast this spell.)\nChoose target creature or player, then choose another target creature or player for each time Comet Storm was kicked. Comet Storm deals X damage to each of them.').
card_mana_cost('comet storm', ['X', 'R', 'R']).
card_cmc('comet storm', 2).
card_layout('comet storm', 'normal').

% Found in: C14
card_name('comeuppance', 'Comeuppance').
card_type('comeuppance', 'Instant').
card_types('comeuppance', ['Instant']).
card_subtypes('comeuppance', []).
card_colors('comeuppance', ['W']).
card_text('comeuppance', 'Prevent all damage that would be dealt to you and planeswalkers you control this turn by sources you don\'t control. If damage from a creature source is prevented this way, Comeuppance deals that much damage to that creature. If damage from a noncreature source is prevented this way, Comeuppance deals that much damage to the source\'s controller.').
card_mana_cost('comeuppance', ['3', 'W']).
card_cmc('comeuppance', 4).
card_layout('comeuppance', 'normal').

% Found in: POR
card_name('command of unsummoning', 'Command of Unsummoning').
card_type('command of unsummoning', 'Instant').
card_types('command of unsummoning', ['Instant']).
card_subtypes('command of unsummoning', []).
card_colors('command of unsummoning', ['U']).
card_text('command of unsummoning', 'Cast Command of Unsummoning only during the declare attackers step and only if you\'ve been attacked this step.\nReturn one or two target attacking creatures to their owner\'s hand.').
card_mana_cost('command of unsummoning', ['2', 'U']).
card_cmc('command of unsummoning', 3).
card_layout('command of unsummoning', 'normal').

% Found in: C13, CM1, CMD, pJGP
card_name('command tower', 'Command Tower').
card_type('command tower', 'Land').
card_types('command tower', ['Land']).
card_subtypes('command tower', []).
card_colors('command tower', []).
card_text('command tower', '{T}: Add to your mana pool one mana of any color in your commander\'s color identity.').
card_layout('command tower', 'normal').

% Found in: CSP
card_name('commandeer', 'Commandeer').
card_type('commandeer', 'Instant').
card_types('commandeer', ['Instant']).
card_subtypes('commandeer', []).
card_colors('commandeer', ['U']).
card_text('commandeer', 'You may exile two blue cards from your hand rather than pay Commandeer\'s mana cost.\nGain control of target noncreature spell. You may choose new targets for it. (If that spell is an artifact, enchantment, or planeswalker, the permanent enters the battlefield under your control.)').
card_mana_cost('commandeer', ['5', 'U', 'U']).
card_cmc('commandeer', 7).
card_layout('commandeer', 'normal').

% Found in: JUD
card_name('commander eesha', 'Commander Eesha').
card_type('commander eesha', 'Legendary Creature — Bird Soldier').
card_types('commander eesha', ['Creature']).
card_subtypes('commander eesha', ['Bird', 'Soldier']).
card_supertypes('commander eesha', ['Legendary']).
card_colors('commander eesha', ['W']).
card_text('commander eesha', 'Flying, protection from creatures').
card_mana_cost('commander eesha', ['2', 'W', 'W']).
card_cmc('commander eesha', 4).
card_layout('commander eesha', 'normal').
card_power('commander eesha', 2).
card_toughness('commander eesha', 4).

% Found in: TMP, TPR
card_name('commander greven il-vec', 'Commander Greven il-Vec').
card_type('commander greven il-vec', 'Legendary Creature — Human Warrior').
card_types('commander greven il-vec', ['Creature']).
card_subtypes('commander greven il-vec', ['Human', 'Warrior']).
card_supertypes('commander greven il-vec', ['Legendary']).
card_colors('commander greven il-vec', ['B']).
card_text('commander greven il-vec', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nWhen Commander Greven il-Vec enters the battlefield, sacrifice a creature.').
card_mana_cost('commander greven il-vec', ['3', 'B', 'B', 'B']).
card_cmc('commander greven il-vec', 6).
card_layout('commander greven il-vec', 'normal').
card_power('commander greven il-vec', 7).
card_toughness('commander greven il-vec', 5).
card_reserved('commander greven il-vec').

% Found in: AVR
card_name('commander\'s authority', 'Commander\'s Authority').
card_type('commander\'s authority', 'Enchantment — Aura').
card_types('commander\'s authority', ['Enchantment']).
card_subtypes('commander\'s authority', ['Aura']).
card_colors('commander\'s authority', ['W']).
card_text('commander\'s authority', 'Enchant creature\nEnchanted creature has \"At the beginning of your upkeep, put a 1/1 white Human creature token onto the battlefield.\"').
card_mana_cost('commander\'s authority', ['4', 'W']).
card_cmc('commander\'s authority', 5).
card_layout('commander\'s authority', 'normal').

% Found in: C14
card_name('commander\'s sphere', 'Commander\'s Sphere').
card_type('commander\'s sphere', 'Artifact').
card_types('commander\'s sphere', ['Artifact']).
card_subtypes('commander\'s sphere', []).
card_colors('commander\'s sphere', []).
card_text('commander\'s sphere', '{T}: Add to your mana pool one mana of any color in your commander\'s color identity.\nSacrifice Commander\'s Sphere: Draw a card.').
card_mana_cost('commander\'s sphere', ['3']).
card_cmc('commander\'s sphere', 3).
card_layout('commander\'s sphere', 'normal').

% Found in: ONS
card_name('commando raid', 'Commando Raid').
card_type('commando raid', 'Instant').
card_types('commando raid', ['Instant']).
card_subtypes('commando raid', []).
card_colors('commando raid', ['R']).
card_text('commando raid', 'Until end of turn, target creature you control gains \"When this creature deals combat damage to a player, you may have it deal damage equal to its power to target creature that player controls.\"').
card_mana_cost('commando raid', ['2', 'R']).
card_cmc('commando raid', 3).
card_layout('commando raid', 'normal').

% Found in: RTR
card_name('common bond', 'Common Bond').
card_type('common bond', 'Instant').
card_types('common bond', ['Instant']).
card_subtypes('common bond', []).
card_colors('common bond', ['W', 'G']).
card_text('common bond', 'Put a +1/+1 counter on target creature.\nPut a +1/+1 counter on target creature.').
card_mana_cost('common bond', ['1', 'G', 'W']).
card_cmc('common bond', 3).
card_layout('common bond', 'normal').

% Found in: MMQ
card_name('common cause', 'Common Cause').
card_type('common cause', 'Enchantment').
card_types('common cause', ['Enchantment']).
card_subtypes('common cause', []).
card_colors('common cause', ['W']).
card_text('common cause', 'Nonartifact creatures get +2/+2 as long as they all share a color.').
card_mana_cost('common cause', ['2', 'W']).
card_cmc('common cause', 3).
card_layout('common cause', 'normal').

% Found in: UGL
card_name('common courtesy', 'Common Courtesy').
card_type('common courtesy', 'Enchantment').
card_types('common courtesy', ['Enchantment']).
card_subtypes('common courtesy', []).
card_colors('common courtesy', ['U']).
card_text('common courtesy', 'Counter any spell unless its caster asks your permission to play that spell. If you refuse permission, Sacrifice Common Courtesy and counter the spell.').
card_mana_cost('common courtesy', ['2', 'U', 'U']).
card_cmc('common courtesy', 4).
card_layout('common courtesy', 'normal').

% Found in: DTK
card_name('commune with lava', 'Commune with Lava').
card_type('commune with lava', 'Instant').
card_types('commune with lava', ['Instant']).
card_subtypes('commune with lava', []).
card_colors('commune with lava', ['R']).
card_text('commune with lava', 'Exile the top X cards of your library. Until the end of your next turn, you may play those cards.').
card_mana_cost('commune with lava', ['X', 'R', 'R']).
card_cmc('commune with lava', 2).
card_layout('commune with lava', 'normal').

% Found in: 10E, CHK, MM2
card_name('commune with nature', 'Commune with Nature').
card_type('commune with nature', 'Sorcery').
card_types('commune with nature', ['Sorcery']).
card_subtypes('commune with nature', []).
card_colors('commune with nature', ['G']).
card_text('commune with nature', 'Look at the top five cards of your library. You may reveal a creature card from among them and put it into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('commune with nature', ['G']).
card_cmc('commune with nature', 1).
card_layout('commune with nature', 'normal').

% Found in: THS
card_name('commune with the gods', 'Commune with the Gods').
card_type('commune with the gods', 'Sorcery').
card_types('commune with the gods', ['Sorcery']).
card_subtypes('commune with the gods', []).
card_colors('commune with the gods', ['G']).
card_text('commune with the gods', 'Reveal the top five cards of your library. You may put a creature or enchantment card from among them into your hand. Put the rest into your graveyard.').
card_mana_cost('commune with the gods', ['1', 'G']).
card_cmc('commune with the gods', 2).
card_layout('commune with the gods', 'normal').

% Found in: BFZ
card_name('complete disregard', 'Complete Disregard').
card_type('complete disregard', 'Instant').
card_types('complete disregard', ['Instant']).
card_subtypes('complete disregard', []).
card_colors('complete disregard', []).
card_text('complete disregard', 'Devoid (This card has no color.)\nExile target creature with power 3 or less.').
card_mana_cost('complete disregard', ['2', 'B']).
card_cmc('complete disregard', 3).
card_layout('complete disregard', 'normal').

% Found in: NMS
card_name('complex automaton', 'Complex Automaton').
card_type('complex automaton', 'Artifact Creature — Golem').
card_types('complex automaton', ['Artifact', 'Creature']).
card_subtypes('complex automaton', ['Golem']).
card_colors('complex automaton', []).
card_text('complex automaton', 'At the beginning of your upkeep, if you control seven or more permanents, return Complex Automaton to its owner\'s hand.').
card_mana_cost('complex automaton', ['4']).
card_cmc('complex automaton', 4).
card_layout('complex automaton', 'normal').
card_power('complex automaton', 4).
card_toughness('complex automaton', 4).

% Found in: ONS
card_name('complicate', 'Complicate').
card_type('complicate', 'Instant').
card_types('complicate', ['Instant']).
card_subtypes('complicate', []).
card_colors('complicate', ['U']).
card_text('complicate', 'Counter target spell unless its controller pays {3}.\nCycling {2}{U} ({2}{U}, Discard this card: Draw a card.)\nWhen you cycle Complicate, you may counter target spell unless its controller pays {1}.').
card_mana_cost('complicate', ['2', 'U']).
card_cmc('complicate', 3).
card_layout('complicate', 'normal').

% Found in: 10E, 5DN
card_name('composite golem', 'Composite Golem').
card_type('composite golem', 'Artifact Creature — Golem').
card_types('composite golem', ['Artifact', 'Creature']).
card_subtypes('composite golem', ['Golem']).
card_colors('composite golem', []).
card_text('composite golem', 'Sacrifice Composite Golem: Add {W}{U}{B}{R}{G} to your mana pool.').
card_mana_cost('composite golem', ['6']).
card_cmc('composite golem', 6).
card_layout('composite golem', 'normal').
card_power('composite golem', 4).
card_toughness('composite golem', 4).

% Found in: 7ED, UDS
card_name('compost', 'Compost').
card_type('compost', 'Enchantment').
card_types('compost', ['Enchantment']).
card_subtypes('compost', []).
card_colors('compost', ['G']).
card_text('compost', 'Whenever a black card is put into an opponent\'s graveyard from anywhere, you may draw a card.').
card_mana_cost('compost', ['1', 'G']).
card_cmc('compost', 2).
card_layout('compost', 'normal').

% Found in: TOR
card_name('compulsion', 'Compulsion').
card_type('compulsion', 'Enchantment').
card_types('compulsion', ['Enchantment']).
card_subtypes('compulsion', []).
card_colors('compulsion', ['U']).
card_text('compulsion', '{1}{U}, Discard a card: Draw a card.\n{1}{U}, Sacrifice Compulsion: Draw a card.').
card_mana_cost('compulsion', ['1', 'U']).
card_cmc('compulsion', 2).
card_layout('compulsion', 'normal').

% Found in: C14, CNS, RAV
card_name('compulsive research', 'Compulsive Research').
card_type('compulsive research', 'Sorcery').
card_types('compulsive research', ['Sorcery']).
card_subtypes('compulsive research', []).
card_colors('compulsive research', ['U']).
card_text('compulsive research', 'Target player draws three cards. Then that player discards two cards unless he or she discards a land card.').
card_mana_cost('compulsive research', ['2', 'U']).
card_cmc('compulsive research', 3).
card_layout('compulsive research', 'normal').

% Found in: 8ED, C14, ODY, PC2
card_name('concentrate', 'Concentrate').
card_type('concentrate', 'Sorcery').
card_types('concentrate', ['Sorcery']).
card_subtypes('concentrate', []).
card_colors('concentrate', ['U']).
card_text('concentrate', 'Draw three cards.').
card_mana_cost('concentrate', ['2', 'U', 'U']).
card_cmc('concentrate', 4).
card_layout('concentrate', 'normal').

% Found in: RAV
card_name('concerted effort', 'Concerted Effort').
card_type('concerted effort', 'Enchantment').
card_types('concerted effort', ['Enchantment']).
card_subtypes('concerted effort', []).
card_colors('concerted effort', ['W']).
card_text('concerted effort', 'At the beginning of each upkeep, creatures you control gain flying until end of turn if a creature you control has flying. The same is true for fear, first strike, double strike, landwalk, protection, trample, and vigilance.').
card_mana_cost('concerted effort', ['2', 'W', 'W']).
card_cmc('concerted effort', 4).
card_layout('concerted effort', 'normal').

% Found in: FEM
card_name('conch horn', 'Conch Horn').
card_type('conch horn', 'Artifact').
card_types('conch horn', ['Artifact']).
card_subtypes('conch horn', []).
card_colors('conch horn', []).
card_text('conch horn', '{1}, {T}, Sacrifice Conch Horn: Draw two cards, then put a card from your hand on top of your library.').
card_mana_cost('conch horn', ['2']).
card_cmc('conch horn', 2).
card_layout('conch horn', 'normal').
card_reserved('conch horn').

% Found in: DDF, RAV
card_name('conclave equenaut', 'Conclave Equenaut').
card_type('conclave equenaut', 'Creature — Human Soldier').
card_types('conclave equenaut', ['Creature']).
card_subtypes('conclave equenaut', ['Human', 'Soldier']).
card_colors('conclave equenaut', ['W']).
card_text('conclave equenaut', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nFlying').
card_mana_cost('conclave equenaut', ['4', 'W', 'W']).
card_cmc('conclave equenaut', 6).
card_layout('conclave equenaut', 'normal').
card_power('conclave equenaut', 3).
card_toughness('conclave equenaut', 3).

% Found in: ORI
card_name('conclave naturalists', 'Conclave Naturalists').
card_type('conclave naturalists', 'Creature — Dryad').
card_types('conclave naturalists', ['Creature']).
card_subtypes('conclave naturalists', ['Dryad']).
card_colors('conclave naturalists', ['G']).
card_text('conclave naturalists', 'When Conclave Naturalists enters the battlefield, you may destroy target artifact or enchantment.').
card_mana_cost('conclave naturalists', ['4', 'G']).
card_cmc('conclave naturalists', 5).
card_layout('conclave naturalists', 'normal').
card_power('conclave naturalists', 4).
card_toughness('conclave naturalists', 4).

% Found in: DDF, MM2, RAV
card_name('conclave phalanx', 'Conclave Phalanx').
card_type('conclave phalanx', 'Creature — Human Soldier').
card_types('conclave phalanx', ['Creature']).
card_subtypes('conclave phalanx', ['Human', 'Soldier']).
card_colors('conclave phalanx', ['W']).
card_text('conclave phalanx', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nWhen Conclave Phalanx enters the battlefield, you gain 1 life for each creature you control.').
card_mana_cost('conclave phalanx', ['4', 'W']).
card_cmc('conclave phalanx', 5).
card_layout('conclave phalanx', 'normal').
card_power('conclave phalanx', 2).
card_toughness('conclave phalanx', 4).

% Found in: RAV
card_name('conclave\'s blessing', 'Conclave\'s Blessing').
card_type('conclave\'s blessing', 'Enchantment — Aura').
card_types('conclave\'s blessing', ['Enchantment']).
card_subtypes('conclave\'s blessing', ['Aura']).
card_colors('conclave\'s blessing', ['W']).
card_text('conclave\'s blessing', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nEnchant creature\nEnchanted creature gets +0/+2 for each other creature you control.').
card_mana_cost('conclave\'s blessing', ['3', 'W']).
card_cmc('conclave\'s blessing', 4).
card_layout('conclave\'s blessing', 'normal').

% Found in: CHR, LEG, ME3
card_name('concordant crossroads', 'Concordant Crossroads').
card_type('concordant crossroads', 'World Enchantment').
card_types('concordant crossroads', ['Enchantment']).
card_subtypes('concordant crossroads', []).
card_supertypes('concordant crossroads', ['World']).
card_colors('concordant crossroads', ['G']).
card_text('concordant crossroads', 'All creatures have haste.').
card_mana_cost('concordant crossroads', ['G']).
card_cmc('concordant crossroads', 1).
card_layout('concordant crossroads', 'normal').

% Found in: RTR
card_name('concordia pegasus', 'Concordia Pegasus').
card_type('concordia pegasus', 'Creature — Pegasus').
card_types('concordia pegasus', ['Creature']).
card_subtypes('concordia pegasus', ['Pegasus']).
card_colors('concordia pegasus', ['W']).
card_text('concordia pegasus', 'Flying').
card_mana_cost('concordia pegasus', ['1', 'W']).
card_cmc('concordia pegasus', 2).
card_layout('concordia pegasus', 'normal').
card_power('concordia pegasus', 1).
card_toughness('concordia pegasus', 3).

% Found in: MBS
card_name('concussive bolt', 'Concussive Bolt').
card_type('concussive bolt', 'Sorcery').
card_types('concussive bolt', ['Sorcery']).
card_subtypes('concussive bolt', []).
card_colors('concussive bolt', ['R']).
card_text('concussive bolt', 'Concussive Bolt deals 4 damage to target player.\nMetalcraft — If you control three or more artifacts, creatures that player controls can\'t block this turn.').
card_mana_cost('concussive bolt', ['3', 'R', 'R']).
card_cmc('concussive bolt', 5).
card_layout('concussive bolt', 'normal').

% Found in: 10E, C14, DDL, DIS, M11, pMPR
card_name('condemn', 'Condemn').
card_type('condemn', 'Instant').
card_types('condemn', ['Instant']).
card_subtypes('condemn', []).
card_colors('condemn', ['W']).
card_text('condemn', 'Put target attacking creature on the bottom of its owner\'s library. Its controller gains life equal to its toughness.').
card_mana_cost('condemn', ['W']).
card_cmc('condemn', 1).
card_layout('condemn', 'normal').

% Found in: 5DN, DD2, DD3_JVC
card_name('condescend', 'Condescend').
card_type('condescend', 'Instant').
card_types('condescend', ['Instant']).
card_subtypes('condescend', []).
card_colors('condescend', ['U']).
card_text('condescend', 'Counter target spell unless its controller pays {X}. Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('condescend', ['X', 'U']).
card_cmc('condescend', 1).
card_layout('condescend', 'normal').

% Found in: BFZ
card_name('conduit of ruin', 'Conduit of Ruin').
card_type('conduit of ruin', 'Creature — Eldrazi').
card_types('conduit of ruin', ['Creature']).
card_subtypes('conduit of ruin', ['Eldrazi']).
card_colors('conduit of ruin', []).
card_text('conduit of ruin', 'When you cast Conduit of Ruin, you may search your library for a colorless creature card with converted mana cost 7 or greater, reveal it, then shuffle your library and put that card on top of it.\nThe first creature spell you cast each turn costs {2} less to cast.').
card_mana_cost('conduit of ruin', ['6']).
card_cmc('conduit of ruin', 6).
card_layout('conduit of ruin', 'normal').
card_power('conduit of ruin', 5).
card_toughness('conduit of ruin', 5).

% Found in: 10E, DD2, DD3_JVC, DDG, HOP, M15, WTH
card_name('cone of flame', 'Cone of Flame').
card_type('cone of flame', 'Sorcery').
card_types('cone of flame', ['Sorcery']).
card_subtypes('cone of flame', []).
card_colors('cone of flame', ['R']).
card_text('cone of flame', 'Cone of Flame deals 1 damage to target creature or player, 2 damage to another target creature or player, and 3 damage to a third target creature or player.').
card_mana_cost('cone of flame', ['3', 'R', 'R']).
card_cmc('cone of flame', 5).
card_layout('cone of flame', 'normal').

% Found in: ODY
card_name('confessor', 'Confessor').
card_type('confessor', 'Creature — Human Cleric').
card_types('confessor', ['Creature']).
card_subtypes('confessor', ['Human', 'Cleric']).
card_colors('confessor', ['W']).
card_text('confessor', 'Whenever a player discards a card, you may gain 1 life.').
card_mana_cost('confessor', ['W']).
card_cmc('confessor', 1).
card_layout('confessor', 'normal').
card_power('confessor', 1).
card_toughness('confessor', 1).

% Found in: 7ED, 8ED, 9ED, USG
card_name('confiscate', 'Confiscate').
card_type('confiscate', 'Enchantment — Aura').
card_types('confiscate', ['Enchantment']).
card_subtypes('confiscate', ['Aura']).
card_colors('confiscate', ['U']).
card_text('confiscate', 'Enchant permanent (Target a permanent as you cast this. This card enters the battlefield attached to that permanent.)\nYou control enchanted permanent.').
card_mana_cost('confiscate', ['4', 'U', 'U']).
card_cmc('confiscate', 6).
card_layout('confiscate', 'normal').

% Found in: TSP
card_name('conflagrate', 'Conflagrate').
card_type('conflagrate', 'Sorcery').
card_types('conflagrate', ['Sorcery']).
card_subtypes('conflagrate', []).
card_colors('conflagrate', ['R']).
card_text('conflagrate', 'Conflagrate deals X damage divided as you choose among any number of target creatures and/or players.\nFlashback—{R}{R}, Discard X cards. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('conflagrate', ['X', 'X', 'R']).
card_cmc('conflagrate', 1).
card_layout('conflagrate', 'normal').

% Found in: CON
card_name('conflux', 'Conflux').
card_type('conflux', 'Sorcery').
card_types('conflux', ['Sorcery']).
card_subtypes('conflux', []).
card_colors('conflux', ['W', 'U', 'B', 'R', 'G']).
card_text('conflux', 'Search your library for a white card, a blue card, a black card, a red card, and a green card. Reveal those cards and put them into your hand. Then shuffle your library.').
card_mana_cost('conflux', ['3', 'W', 'U', 'B', 'R', 'G']).
card_cmc('conflux', 8).
card_layout('conflux', 'normal').

% Found in: PLS
card_name('confound', 'Confound').
card_type('confound', 'Instant').
card_types('confound', ['Instant']).
card_subtypes('confound', []).
card_colors('confound', ['U']).
card_text('confound', 'Counter target spell that targets one or more creatures.\nDraw a card.').
card_mana_cost('confound', ['1', 'U']).
card_cmc('confound', 2).
card_layout('confound', 'normal').

% Found in: MRD
card_name('confusion in the ranks', 'Confusion in the Ranks').
card_type('confusion in the ranks', 'Enchantment').
card_types('confusion in the ranks', ['Enchantment']).
card_subtypes('confusion in the ranks', []).
card_colors('confusion in the ranks', ['R']).
card_text('confusion in the ranks', 'Whenever an artifact, creature, or enchantment enters the battlefield, its controller chooses target permanent another player controls that shares a card type with it. Exchange control of those permanents.').
card_mana_cost('confusion in the ranks', ['3', 'R', 'R']).
card_cmc('confusion in the ranks', 5).
card_layout('confusion in the ranks', 'normal').

% Found in: CMD, HOP, M14, M15, USG
card_name('congregate', 'Congregate').
card_type('congregate', 'Instant').
card_types('congregate', ['Instant']).
card_subtypes('congregate', []).
card_colors('congregate', ['W']).
card_text('congregate', 'Target player gains 2 life for each creature on the battlefield.').
card_mana_cost('congregate', ['3', 'W']).
card_cmc('congregate', 4).
card_layout('congregate', 'normal').

% Found in: RAV
card_name('congregation at dawn', 'Congregation at Dawn').
card_type('congregation at dawn', 'Instant').
card_types('congregation at dawn', ['Instant']).
card_subtypes('congregation at dawn', []).
card_colors('congregation at dawn', ['W', 'G']).
card_text('congregation at dawn', 'Search your library for up to three creature cards and reveal them. Shuffle your library, then put those cards on top of it in any order.').
card_mana_cost('congregation at dawn', ['G', 'G', 'W']).
card_cmc('congregation at dawn', 3).
card_layout('congregation at dawn', 'normal').

% Found in: DTK
card_name('conifer strider', 'Conifer Strider').
card_type('conifer strider', 'Creature — Elemental').
card_types('conifer strider', ['Creature']).
card_subtypes('conifer strider', ['Elemental']).
card_colors('conifer strider', ['G']).
card_text('conifer strider', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)').
card_mana_cost('conifer strider', ['3', 'G']).
card_cmc('conifer strider', 4).
card_layout('conifer strider', 'normal').
card_power('conifer strider', 5).
card_toughness('conifer strider', 1).

% Found in: RTR
card_name('conjured currency', 'Conjured Currency').
card_type('conjured currency', 'Enchantment').
card_types('conjured currency', ['Enchantment']).
card_subtypes('conjured currency', []).
card_colors('conjured currency', ['U']).
card_text('conjured currency', 'At the beginning of your upkeep, you may exchange control of Conjured Currency and target permanent you neither own nor control.').
card_mana_cost('conjured currency', ['5', 'U']).
card_cmc('conjured currency', 6).
card_layout('conjured currency', 'normal').

% Found in: GPT
card_name('conjurer\'s ban', 'Conjurer\'s Ban').
card_type('conjurer\'s ban', 'Sorcery').
card_types('conjurer\'s ban', ['Sorcery']).
card_subtypes('conjurer\'s ban', []).
card_colors('conjurer\'s ban', ['W', 'B']).
card_text('conjurer\'s ban', 'Name a card. Until your next turn, the named card can\'t be played.\nDraw a card.').
card_mana_cost('conjurer\'s ban', ['W', 'B']).
card_cmc('conjurer\'s ban', 2).
card_layout('conjurer\'s ban', 'normal').

% Found in: 5DN
card_name('conjurer\'s bauble', 'Conjurer\'s Bauble').
card_type('conjurer\'s bauble', 'Artifact').
card_types('conjurer\'s bauble', ['Artifact']).
card_subtypes('conjurer\'s bauble', []).
card_colors('conjurer\'s bauble', []).
card_text('conjurer\'s bauble', '{T}, Sacrifice Conjurer\'s Bauble: Put up to one target card from your graveyard on the bottom of your library. Draw a card.').
card_mana_cost('conjurer\'s bauble', ['1']).
card_cmc('conjurer\'s bauble', 1).
card_layout('conjurer\'s bauble', 'normal').

% Found in: AVR, C13
card_name('conjurer\'s closet', 'Conjurer\'s Closet').
card_type('conjurer\'s closet', 'Artifact').
card_types('conjurer\'s closet', ['Artifact']).
card_subtypes('conjurer\'s closet', []).
card_colors('conjurer\'s closet', []).
card_text('conjurer\'s closet', 'At the beginning of your end step, you may exile target creature you control, then return that card to the battlefield under your control.').
card_mana_cost('conjurer\'s closet', ['5']).
card_cmc('conjurer\'s closet', 5).
card_layout('conjurer\'s closet', 'normal').

% Found in: 5ED, 6ED, ICE, ME2
card_name('conquer', 'Conquer').
card_type('conquer', 'Enchantment — Aura').
card_types('conquer', ['Enchantment']).
card_subtypes('conquer', ['Aura']).
card_colors('conquer', ['R']).
card_text('conquer', 'Enchant land\nYou control enchanted land.').
card_mana_cost('conquer', ['3', 'R', 'R']).
card_cmc('conquer', 5).
card_layout('conquer', 'normal').

% Found in: DDL, ROE
card_name('conquering manticore', 'Conquering Manticore').
card_type('conquering manticore', 'Creature — Manticore').
card_types('conquering manticore', ['Creature']).
card_subtypes('conquering manticore', ['Manticore']).
card_colors('conquering manticore', ['R']).
card_text('conquering manticore', 'Flying\nWhen Conquering Manticore enters the battlefield, gain control of target creature an opponent controls until end of turn. Untap that creature. It gains haste until end of turn.').
card_mana_cost('conquering manticore', ['4', 'R', 'R']).
card_cmc('conquering manticore', 6).
card_layout('conquering manticore', 'normal').
card_power('conquering manticore', 5).
card_toughness('conquering manticore', 5).

% Found in: ZEN
card_name('conqueror\'s pledge', 'Conqueror\'s Pledge').
card_type('conqueror\'s pledge', 'Sorcery').
card_types('conqueror\'s pledge', ['Sorcery']).
card_subtypes('conqueror\'s pledge', []).
card_colors('conqueror\'s pledge', ['W']).
card_text('conqueror\'s pledge', 'Kicker {6} (You may pay an additional {6} as you cast this spell.)\nPut six 1/1 white Kor Soldier creature tokens onto the battlefield. If Conqueror\'s Pledge was kicked, put twelve of those tokens onto the battlefield instead.').
card_mana_cost('conqueror\'s pledge', ['2', 'W', 'W', 'W']).
card_cmc('conqueror\'s pledge', 5).
card_layout('conqueror\'s pledge', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, TSB
card_name('consecrate land', 'Consecrate Land').
card_type('consecrate land', 'Enchantment — Aura').
card_types('consecrate land', ['Enchantment']).
card_subtypes('consecrate land', ['Aura']).
card_colors('consecrate land', ['W']).
card_text('consecrate land', 'Enchant land\nEnchanted land has indestructible and can\'t be enchanted by other Auras.').
card_mana_cost('consecrate land', ['W']).
card_cmc('consecrate land', 1).
card_layout('consecrate land', 'normal').

% Found in: ORI
card_name('consecrated by blood', 'Consecrated by Blood').
card_type('consecrated by blood', 'Enchantment — Aura').
card_types('consecrated by blood', ['Enchantment']).
card_subtypes('consecrated by blood', ['Aura']).
card_colors('consecrated by blood', ['B']).
card_text('consecrated by blood', 'Enchant creature\nEnchanted creature gets +2/+2 and has flying and \"Sacrifice two other creatures: Regenerate this creature.\" (The next time the creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('consecrated by blood', ['2', 'B', 'B']).
card_cmc('consecrated by blood', 4).
card_layout('consecrated by blood', 'normal').

% Found in: MBS
card_name('consecrated sphinx', 'Consecrated Sphinx').
card_type('consecrated sphinx', 'Creature — Sphinx').
card_types('consecrated sphinx', ['Creature']).
card_subtypes('consecrated sphinx', ['Sphinx']).
card_colors('consecrated sphinx', ['U']).
card_text('consecrated sphinx', 'Flying\nWhenever an opponent draws a card, you may draw two cards.').
card_mana_cost('consecrated sphinx', ['4', 'U', 'U']).
card_cmc('consecrated sphinx', 6).
card_layout('consecrated sphinx', 'normal').
card_power('consecrated sphinx', 4).
card_toughness('consecrated sphinx', 6).

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('conservator', 'Conservator').
card_type('conservator', 'Artifact').
card_types('conservator', ['Artifact']).
card_subtypes('conservator', []).
card_colors('conservator', []).
card_text('conservator', '{3}, {T}: Prevent the next 2 damage that would be dealt to you this turn.').
card_mana_cost('conservator', ['4']).
card_cmc('conservator', 4).
card_layout('conservator', 'normal').

% Found in: SHM
card_name('consign to dream', 'Consign to Dream').
card_type('consign to dream', 'Instant').
card_types('consign to dream', ['Instant']).
card_subtypes('consign to dream', []).
card_colors('consign to dream', ['U']).
card_text('consign to dream', 'Return target permanent to its owner\'s hand. If that permanent is red or green, put it on top of its owner\'s library instead.').
card_mana_cost('consign to dream', ['2', 'U']).
card_cmc('consign to dream', 3).
card_layout('consign to dream', 'normal').

% Found in: JOU
card_name('consign to dust', 'Consign to Dust').
card_type('consign to dust', 'Instant').
card_types('consign to dust', ['Instant']).
card_subtypes('consign to dust', []).
card_colors('consign to dust', ['G']).
card_text('consign to dust', 'Strive — Consign to Dust costs {2}{G} more to cast for each target beyond the first.\nDestroy any number of target artifacts and/or enchantments.').
card_mana_cost('consign to dust', ['2', 'G']).
card_cmc('consign to dust', 3).
card_layout('consign to dust', 'normal').

% Found in: MMQ, TSB
card_name('conspiracy', 'Conspiracy').
card_type('conspiracy', 'Enchantment').
card_types('conspiracy', ['Enchantment']).
card_subtypes('conspiracy', []).
card_colors('conspiracy', ['B']).
card_text('conspiracy', 'As Conspiracy enters the battlefield, choose a creature type.\nCreature cards you own that aren\'t on the battlefield, creature spells you control, and creatures you control are the chosen type.').
card_mana_cost('conspiracy', ['3', 'B', 'B']).
card_cmc('conspiracy', 5).
card_layout('conspiracy', 'normal').

% Found in: STH
card_name('constant mists', 'Constant Mists').
card_type('constant mists', 'Instant').
card_types('constant mists', ['Instant']).
card_subtypes('constant mists', []).
card_colors('constant mists', ['G']).
card_text('constant mists', 'Buyback—Sacrifice a land. (You may sacrifice a land in addition to any other costs as you cast this spell. If you do, put this card into your hand as it resolves.)\nPrevent all combat damage that would be dealt this turn.').
card_mana_cost('constant mists', ['1', 'G']).
card_cmc('constant mists', 2).
card_layout('constant mists', 'normal').

% Found in: M15
card_name('constricting sliver', 'Constricting Sliver').
card_type('constricting sliver', 'Creature — Sliver').
card_types('constricting sliver', ['Creature']).
card_subtypes('constricting sliver', ['Sliver']).
card_colors('constricting sliver', ['W']).
card_text('constricting sliver', 'Sliver creatures you control have \"When this creature enters the battlefield, you may exile target creature an opponent controls until this creature leaves the battlefield.\"').
card_mana_cost('constricting sliver', ['5', 'W']).
card_cmc('constricting sliver', 6).
card_layout('constricting sliver', 'normal').
card_power('constricting sliver', 3).
card_toughness('constricting sliver', 3).

% Found in: CON
card_name('constricting tendrils', 'Constricting Tendrils').
card_type('constricting tendrils', 'Instant').
card_types('constricting tendrils', ['Instant']).
card_subtypes('constricting tendrils', []).
card_colors('constricting tendrils', ['U']).
card_text('constricting tendrils', 'Target creature gets -3/-0 until end of turn.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('constricting tendrils', ['U']).
card_cmc('constricting tendrils', 1).
card_layout('constricting tendrils', 'normal').

% Found in: ORI
card_name('consul\'s lieutenant', 'Consul\'s Lieutenant').
card_type('consul\'s lieutenant', 'Creature — Human Soldier').
card_types('consul\'s lieutenant', ['Creature']).
card_subtypes('consul\'s lieutenant', ['Human', 'Soldier']).
card_colors('consul\'s lieutenant', ['W']).
card_text('consul\'s lieutenant', 'First strike\nRenown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)\nWhenever Consul\'s Lieutenant attacks, if it\'s renowned, other attacking creatures you control get +1/+1 until end of turn.').
card_mana_cost('consul\'s lieutenant', ['W', 'W']).
card_cmc('consul\'s lieutenant', 2).
card_layout('consul\'s lieutenant', 'normal').
card_power('consul\'s lieutenant', 2).
card_toughness('consul\'s lieutenant', 1).

% Found in: RAV
card_name('consult the necrosages', 'Consult the Necrosages').
card_type('consult the necrosages', 'Sorcery').
card_types('consult the necrosages', ['Sorcery']).
card_subtypes('consult the necrosages', []).
card_colors('consult the necrosages', ['U', 'B']).
card_text('consult the necrosages', 'Choose one —\n• Target player draws two cards.\n• Target player discards two cards.').
card_mana_cost('consult the necrosages', ['1', 'U', 'B']).
card_cmc('consult the necrosages', 3).
card_layout('consult the necrosages', 'normal').

% Found in: 10E, 9ED, DD3_DVD, DDC, DPA, HOP, M10, M12, MRD, pMEI
card_name('consume spirit', 'Consume Spirit').
card_type('consume spirit', 'Sorcery').
card_types('consume spirit', ['Sorcery']).
card_subtypes('consume spirit', []).
card_colors('consume spirit', ['B']).
card_text('consume spirit', 'Spend only black mana on X.\nConsume Spirit deals X damage to target creature or player and you gain X life.').
card_mana_cost('consume spirit', ['X', '1', 'B']).
card_cmc('consume spirit', 2).
card_layout('consume spirit', 'normal').

% Found in: APC, DDM
card_name('consume strength', 'Consume Strength').
card_type('consume strength', 'Instant').
card_types('consume strength', ['Instant']).
card_subtypes('consume strength', []).
card_colors('consume strength', ['B', 'G']).
card_text('consume strength', 'Target creature gets +2/+2 until end of turn. Another target creature gets -2/-2 until end of turn.').
card_mana_cost('consume strength', ['1', 'B', 'G']).
card_cmc('consume strength', 3).
card_layout('consume strength', 'normal').

% Found in: DDP, ROE
card_name('consume the meek', 'Consume the Meek').
card_type('consume the meek', 'Instant').
card_types('consume the meek', ['Instant']).
card_subtypes('consume the meek', []).
card_colors('consume the meek', ['B']).
card_text('consume the meek', 'Destroy each creature with converted mana cost 3 or less. They can\'t be regenerated.').
card_mana_cost('consume the meek', ['3', 'B', 'B']).
card_cmc('consume the meek', 5).
card_layout('consume the meek', 'normal').

% Found in: GTC, pPRE
card_name('consuming aberration', 'Consuming Aberration').
card_type('consuming aberration', 'Creature — Horror').
card_types('consuming aberration', ['Creature']).
card_subtypes('consuming aberration', ['Horror']).
card_colors('consuming aberration', ['U', 'B']).
card_text('consuming aberration', 'Consuming Aberration\'s power and toughness are each equal to the number of cards in your opponents\' graveyards.\nWhenever you cast a spell, each opponent reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_mana_cost('consuming aberration', ['3', 'U', 'B']).
card_cmc('consuming aberration', 5).
card_layout('consuming aberration', 'normal').
card_power('consuming aberration', '*').
card_toughness('consuming aberration', '*').

% Found in: LRW
card_name('consuming bonfire', 'Consuming Bonfire').
card_type('consuming bonfire', 'Tribal Sorcery — Elemental').
card_types('consuming bonfire', ['Tribal', 'Sorcery']).
card_subtypes('consuming bonfire', ['Elemental']).
card_colors('consuming bonfire', ['R']).
card_text('consuming bonfire', 'Choose one —\n• Consuming Bonfire deals 4 damage to target non-Elemental creature.\n• Consuming Bonfire deals 7 damage to target Treefolk creature.').
card_mana_cost('consuming bonfire', ['3', 'R', 'R']).
card_cmc('consuming bonfire', 5).
card_layout('consuming bonfire', 'normal').

% Found in: MIR
card_name('consuming ferocity', 'Consuming Ferocity').
card_type('consuming ferocity', 'Enchantment — Aura').
card_types('consuming ferocity', ['Enchantment']).
card_subtypes('consuming ferocity', ['Aura']).
card_colors('consuming ferocity', ['R']).
card_text('consuming ferocity', 'Enchant non-Wall creature\nEnchanted creature gets +1/+0.\nAt the beginning of your upkeep, put a +1/+0 counter on enchanted creature. If that creature has three or more +1/+0 counters on it, it deals damage equal to its power to its controller, then destroy that creature and it can\'t be regenerated.').
card_mana_cost('consuming ferocity', ['1', 'R']).
card_cmc('consuming ferocity', 2).
card_layout('consuming ferocity', 'normal').

% Found in: ROE
card_name('consuming vapors', 'Consuming Vapors').
card_type('consuming vapors', 'Sorcery').
card_types('consuming vapors', ['Sorcery']).
card_subtypes('consuming vapors', []).
card_colors('consuming vapors', ['B']).
card_text('consuming vapors', 'Target player sacrifices a creature. You gain life equal to that creature\'s toughness.\nRebound (If you cast this spell from your hand, exile it as it resolves. At the beginning of your next upkeep, you may cast this card from exile without paying its mana cost.)').
card_mana_cost('consuming vapors', ['3', 'B']).
card_cmc('consuming vapors', 4).
card_layout('consuming vapors', 'normal').

% Found in: CHK
card_name('consuming vortex', 'Consuming Vortex').
card_type('consuming vortex', 'Instant — Arcane').
card_types('consuming vortex', ['Instant']).
card_subtypes('consuming vortex', ['Arcane']).
card_colors('consuming vortex', ['U']).
card_text('consuming vortex', 'Return target creature to its owner\'s hand.\nSplice onto Arcane {3}{U} (As you cast an Arcane spell, you may reveal this card from your hand and pay its splice cost. If you do, add this card\'s effects to that spell.)').
card_mana_cost('consuming vortex', ['1', 'U']).
card_cmc('consuming vortex', 2).
card_layout('consuming vortex', 'normal').

% Found in: SCG
card_name('consumptive goo', 'Consumptive Goo').
card_type('consumptive goo', 'Creature — Ooze').
card_types('consumptive goo', ['Creature']).
card_subtypes('consumptive goo', ['Ooze']).
card_colors('consumptive goo', ['B']).
card_text('consumptive goo', '{2}{B}{B}: Target creature gets -1/-1 until end of turn. Put a +1/+1 counter on Consumptive Goo.').
card_mana_cost('consumptive goo', ['B', 'B']).
card_cmc('consumptive goo', 2).
card_layout('consumptive goo', 'normal').
card_power('consumptive goo', 1).
card_toughness('consumptive goo', 1).

% Found in: ALL, DKM, MED
card_name('contagion', 'Contagion').
card_type('contagion', 'Instant').
card_types('contagion', ['Instant']).
card_subtypes('contagion', []).
card_colors('contagion', ['B']).
card_text('contagion', 'You may pay 1 life and exile a black card from your hand rather than pay Contagion\'s mana cost.\nDistribute two -2/-1 counters among one or two target creatures.').
card_mana_cost('contagion', ['3', 'B', 'B']).
card_cmc('contagion', 5).
card_layout('contagion', 'normal').

% Found in: DDF, SOM, pFNM
card_name('contagion clasp', 'Contagion Clasp').
card_type('contagion clasp', 'Artifact').
card_types('contagion clasp', ['Artifact']).
card_subtypes('contagion clasp', []).
card_colors('contagion clasp', []).
card_text('contagion clasp', 'When Contagion Clasp enters the battlefield, put a -1/-1 counter on target creature.\n{4}, {T}: Proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('contagion clasp', ['2']).
card_cmc('contagion clasp', 2).
card_layout('contagion clasp', 'normal').

% Found in: SOM
card_name('contagion engine', 'Contagion Engine').
card_type('contagion engine', 'Artifact').
card_types('contagion engine', ['Artifact']).
card_subtypes('contagion engine', []).
card_colors('contagion engine', []).
card_text('contagion engine', 'When Contagion Engine enters the battlefield, put a -1/-1 counter on each creature target player controls.\n{4}, {T}: Proliferate, then proliferate again. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there. Then do it again.)').
card_mana_cost('contagion engine', ['6']).
card_cmc('contagion engine', 6).
card_layout('contagion engine', 'normal').

% Found in: SOM
card_name('contagious nim', 'Contagious Nim').
card_type('contagious nim', 'Creature — Zombie').
card_types('contagious nim', ['Creature']).
card_subtypes('contagious nim', ['Zombie']).
card_colors('contagious nim', ['B']).
card_text('contagious nim', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('contagious nim', ['2', 'B']).
card_cmc('contagious nim', 3).
card_layout('contagious nim', 'normal').
card_power('contagious nim', 2).
card_toughness('contagious nim', 2).

% Found in: C14
card_name('containment priest', 'Containment Priest').
card_type('containment priest', 'Creature — Human Cleric').
card_types('containment priest', ['Creature']).
card_subtypes('containment priest', ['Human', 'Cleric']).
card_colors('containment priest', ['W']).
card_text('containment priest', 'Flash\nIf a nontoken creature would enter the battlefield and it wasn\'t cast, exile it instead.').
card_mana_cost('containment priest', ['1', 'W']).
card_cmc('containment priest', 2).
card_layout('containment priest', 'normal').
card_power('containment priest', 2).
card_toughness('containment priest', 2).

% Found in: 10E, 9ED, MRD
card_name('contaminated bond', 'Contaminated Bond').
card_type('contaminated bond', 'Enchantment — Aura').
card_types('contaminated bond', ['Enchantment']).
card_subtypes('contaminated bond', ['Aura']).
card_colors('contaminated bond', ['B']).
card_text('contaminated bond', 'Enchant creature (Target a creature as you cast this. This card enters the battlefield attached to that creature.)\nWhenever enchanted creature attacks or blocks, its controller loses 3 life.').
card_mana_cost('contaminated bond', ['1', 'B']).
card_cmc('contaminated bond', 2).
card_layout('contaminated bond', 'normal').

% Found in: GTC, ROE
card_name('contaminated ground', 'Contaminated Ground').
card_type('contaminated ground', 'Enchantment — Aura').
card_types('contaminated ground', ['Enchantment']).
card_subtypes('contaminated ground', ['Aura']).
card_colors('contaminated ground', ['B']).
card_text('contaminated ground', 'Enchant land\nEnchanted land is a Swamp.\nWhenever enchanted land becomes tapped, its controller loses 2 life.').
card_mana_cost('contaminated ground', ['1', 'B']).
card_cmc('contaminated ground', 2).
card_layout('contaminated ground', 'normal').

% Found in: USG
card_name('contamination', 'Contamination').
card_type('contamination', 'Enchantment').
card_types('contamination', ['Enchantment']).
card_subtypes('contamination', []).
card_colors('contamination', ['B']).
card_text('contamination', 'At the beginning of your upkeep, sacrifice Contamination unless you sacrifice a creature.\nIf a land is tapped for mana, it produces {B} instead of any other type and amount.').
card_mana_cost('contamination', ['2', 'B']).
card_cmc('contamination', 3).
card_layout('contamination', 'normal').

% Found in: STH
card_name('contemplation', 'Contemplation').
card_type('contemplation', 'Enchantment').
card_types('contemplation', ['Enchantment']).
card_subtypes('contemplation', []).
card_colors('contemplation', ['W']).
card_text('contemplation', 'Whenever you cast a spell, you gain 1 life.').
card_mana_cost('contemplation', ['1', 'W', 'W']).
card_cmc('contemplation', 3).
card_layout('contemplation', 'normal').

% Found in: STH
card_name('contempt', 'Contempt').
card_type('contempt', 'Enchantment — Aura').
card_types('contempt', ['Enchantment']).
card_subtypes('contempt', ['Aura']).
card_colors('contempt', ['U']).
card_text('contempt', 'Enchant creature\nWhen enchanted creature attacks, return it and Contempt to their owners\' hands at end of combat.').
card_mana_cost('contempt', ['1', 'U']).
card_cmc('contempt', 2).
card_layout('contempt', 'normal').

% Found in: C13, ONS
card_name('contested cliffs', 'Contested Cliffs').
card_type('contested cliffs', 'Land').
card_types('contested cliffs', ['Land']).
card_subtypes('contested cliffs', []).
card_colors('contested cliffs', []).
card_text('contested cliffs', '{T}: Add {1} to your mana pool.\n{R}{G}, {T}: Target Beast creature you control fights target creature an opponent controls. (Each deals damage equal to its power to the other.)').
card_layout('contested cliffs', 'normal').

% Found in: MBS
card_name('contested war zone', 'Contested War Zone').
card_type('contested war zone', 'Land').
card_types('contested war zone', ['Land']).
card_subtypes('contested war zone', []).
card_colors('contested war zone', []).
card_text('contested war zone', 'Whenever a creature deals combat damage to you, that creature\'s controller gains control of Contested War Zone.\n{T}: Add {1} to your mana pool.\n{1}, {T}: Attacking creatures get +1/+0 until end of turn.').
card_layout('contested war zone', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB
card_name('contract from below', 'Contract from Below').
card_type('contract from below', 'Sorcery').
card_types('contract from below', ['Sorcery']).
card_subtypes('contract from below', []).
card_colors('contract from below', ['B']).
card_text('contract from below', 'Remove Contract from Below from your deck before playing if you\'re not playing for ante.\nDiscard your hand, ante the top card of your library, then draw seven cards.').
card_mana_cost('contract from below', ['B']).
card_cmc('contract from below', 1).
card_layout('contract from below', 'normal').
card_reserved('contract from below').

% Found in: DTK
card_name('contradict', 'Contradict').
card_type('contradict', 'Instant').
card_types('contradict', ['Instant']).
card_subtypes('contradict', []).
card_colors('contradict', ['U']).
card_text('contradict', 'Counter target spell.\nDraw a card.').
card_mana_cost('contradict', ['3', 'U', 'U']).
card_cmc('contradict', 5).
card_layout('contradict', 'normal').

% Found in: 2ED, 3ED, 4ED, BRB, C13, CED, CEI, DDM, LEA, LEB, ME4, VMA
card_name('control magic', 'Control Magic').
card_type('control magic', 'Enchantment — Aura').
card_types('control magic', ['Enchantment']).
card_subtypes('control magic', ['Aura']).
card_colors('control magic', ['U']).
card_text('control magic', 'Enchant creature\nYou control enchanted creature.').
card_mana_cost('control magic', ['2', 'U', 'U']).
card_cmc('control magic', 4).
card_layout('control magic', 'normal').

% Found in: PTK
card_name('control of the court', 'Control of the Court').
card_type('control of the court', 'Sorcery').
card_types('control of the court', ['Sorcery']).
card_subtypes('control of the court', []).
card_colors('control of the court', ['R']).
card_text('control of the court', 'Draw four cards, then discard three cards at random.').
card_mana_cost('control of the court', ['1', 'R']).
card_cmc('control of the court', 2).
card_layout('control of the court', 'normal').

% Found in: CON
card_name('controlled instincts', 'Controlled Instincts').
card_type('controlled instincts', 'Enchantment — Aura').
card_types('controlled instincts', ['Enchantment']).
card_subtypes('controlled instincts', ['Aura']).
card_colors('controlled instincts', ['U']).
card_text('controlled instincts', 'Enchant red or green creature\nEnchanted creature doesn\'t untap during its controller\'s untap step.').
card_mana_cost('controlled instincts', ['U']).
card_cmc('controlled instincts', 1).
card_layout('controlled instincts', 'normal').

% Found in: CSP
card_name('controvert', 'Controvert').
card_type('controvert', 'Instant').
card_types('controvert', ['Instant']).
card_subtypes('controvert', []).
card_colors('controvert', ['U']).
card_text('controvert', 'Counter target spell.\nRecover {2}{U}{U} (When a creature is put into your graveyard from the battlefield, you may pay {2}{U}{U}. If you do, return this card from your graveyard to your hand. Otherwise, exile this card.)').
card_mana_cost('controvert', ['2', 'U', 'U']).
card_cmc('controvert', 4).
card_layout('controvert', 'normal').

% Found in: CMD, M11
card_name('conundrum sphinx', 'Conundrum Sphinx').
card_type('conundrum sphinx', 'Creature — Sphinx').
card_types('conundrum sphinx', ['Creature']).
card_subtypes('conundrum sphinx', ['Sphinx']).
card_colors('conundrum sphinx', ['U']).
card_text('conundrum sphinx', 'Flying\nWhenever Conundrum Sphinx attacks, each player names a card. Then each player reveals the top card of his or her library. If the card a player revealed is the card he or she named, that player puts it into his or her hand. If it\'s not, that player puts it on the bottom of his or her library.').
card_mana_cost('conundrum sphinx', ['2', 'U', 'U']).
card_cmc('conundrum sphinx', 4).
card_layout('conundrum sphinx', 'normal').
card_power('conundrum sphinx', 4).
card_toughness('conundrum sphinx', 4).

% Found in: EXO
card_name('convalescence', 'Convalescence').
card_type('convalescence', 'Enchantment').
card_types('convalescence', ['Enchantment']).
card_subtypes('convalescence', []).
card_colors('convalescence', ['W']).
card_text('convalescence', 'At the beginning of your upkeep, if you have 10 or less life, you gain 1 life.').
card_mana_cost('convalescence', ['1', 'W']).
card_cmc('convalescence', 2).
card_layout('convalescence', 'normal').

% Found in: ONS
card_name('convalescent care', 'Convalescent Care').
card_type('convalescent care', 'Enchantment').
card_types('convalescent care', ['Enchantment']).
card_subtypes('convalescent care', []).
card_colors('convalescent care', ['W']).
card_text('convalescent care', 'At the beginning of your upkeep, if you have 5 or less life, you gain 3 life and draw a card.').
card_mana_cost('convalescent care', ['1', 'W', 'W']).
card_cmc('convalescent care', 3).
card_layout('convalescent care', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB, ME4
card_name('conversion', 'Conversion').
card_type('conversion', 'Enchantment').
card_types('conversion', ['Enchantment']).
card_subtypes('conversion', []).
card_colors('conversion', ['W']).
card_text('conversion', 'At the beginning of your upkeep, sacrifice Conversion unless you pay {W}{W}.\nAll Mountains are Plains.').
card_mana_cost('conversion', ['2', 'W', 'W']).
card_cmc('conversion', 4).
card_layout('conversion', 'normal').

% Found in: NPH
card_name('conversion chamber', 'Conversion Chamber').
card_type('conversion chamber', 'Artifact').
card_types('conversion chamber', ['Artifact']).
card_subtypes('conversion chamber', []).
card_colors('conversion chamber', []).
card_text('conversion chamber', '{2}, {T}: Exile target artifact card from a graveyard. Put a charge counter on Conversion Chamber.\n{2}, {T}, Remove a charge counter from Conversion Chamber: Put a 3/3 colorless Golem artifact creature token onto the battlefield.').
card_mana_cost('conversion chamber', ['3']).
card_cmc('conversion chamber', 3).
card_layout('conversion chamber', 'normal').

% Found in: STH, TPR
card_name('conviction', 'Conviction').
card_type('conviction', 'Enchantment — Aura').
card_types('conviction', ['Enchantment']).
card_subtypes('conviction', ['Aura']).
card_colors('conviction', ['W']).
card_text('conviction', 'Enchant creature\nEnchanted creature gets +1/+3.\n{W}: Return Conviction to its owner\'s hand.').
card_mana_cost('conviction', ['1', 'W']).
card_cmc('conviction', 2).
card_layout('conviction', 'normal').

% Found in: M10
card_name('convincing mirage', 'Convincing Mirage').
card_type('convincing mirage', 'Enchantment — Aura').
card_types('convincing mirage', ['Enchantment']).
card_subtypes('convincing mirage', ['Aura']).
card_colors('convincing mirage', ['U']).
card_text('convincing mirage', 'Enchant land\nAs Convincing Mirage enters the battlefield, choose a basic land type.\nEnchanted land is the chosen type.').
card_mana_cost('convincing mirage', ['1', 'U']).
card_cmc('convincing mirage', 2).
card_layout('convincing mirage', 'normal').

% Found in: RAV
card_name('convolute', 'Convolute').
card_type('convolute', 'Instant').
card_types('convolute', ['Instant']).
card_subtypes('convolute', []).
card_colors('convolute', ['U']).
card_text('convolute', 'Counter target spell unless its controller pays {4}.').
card_mana_cost('convolute', ['2', 'U']).
card_cmc('convolute', 3).
card_layout('convolute', 'normal').

% Found in: STH
card_name('convulsing licid', 'Convulsing Licid').
card_type('convulsing licid', 'Creature — Licid').
card_types('convulsing licid', ['Creature']).
card_subtypes('convulsing licid', ['Licid']).
card_colors('convulsing licid', ['R']).
card_text('convulsing licid', '{R}, {T}: Convulsing Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {R} to end this effect.\nEnchanted creature can\'t block.').
card_mana_cost('convulsing licid', ['2', 'R']).
card_cmc('convulsing licid', 3).
card_layout('convulsing licid', 'normal').
card_power('convulsing licid', 2).
card_toughness('convulsing licid', 2).

% Found in: ICE
card_name('cooperation', 'Cooperation').
card_type('cooperation', 'Enchantment — Aura').
card_types('cooperation', ['Enchantment']).
card_subtypes('cooperation', ['Aura']).
card_colors('cooperation', ['W']).
card_text('cooperation', 'Enchant creature\nEnchanted creature has banding. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding a player controls are blocking or being blocked by a creature, that player divides that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('cooperation', ['2', 'W']).
card_cmc('cooperation', 3).
card_layout('cooperation', 'normal').

% Found in: THS
card_name('coordinated assault', 'Coordinated Assault').
card_type('coordinated assault', 'Instant').
card_types('coordinated assault', ['Instant']).
card_subtypes('coordinated assault', []).
card_colors('coordinated assault', ['R']).
card_text('coordinated assault', 'Up to two target creatures each get +1/+0 and gain first strike until end of turn.').
card_mana_cost('coordinated assault', ['R']).
card_cmc('coordinated assault', 1).
card_layout('coordinated assault', 'normal').

% Found in: MOR
card_name('coordinated barrage', 'Coordinated Barrage').
card_type('coordinated barrage', 'Instant').
card_types('coordinated barrage', ['Instant']).
card_subtypes('coordinated barrage', []).
card_colors('coordinated barrage', ['W']).
card_text('coordinated barrage', 'Choose a creature type. Coordinated Barrage deals damage to target attacking or blocking creature equal to the number of permanents you control of the chosen type.').
card_mana_cost('coordinated barrage', ['W']).
card_cmc('coordinated barrage', 1).
card_layout('coordinated barrage', 'normal').

% Found in: MBS, MM2
card_name('copper carapace', 'Copper Carapace').
card_type('copper carapace', 'Artifact — Equipment').
card_types('copper carapace', ['Artifact']).
card_subtypes('copper carapace', ['Equipment']).
card_colors('copper carapace', []).
card_text('copper carapace', 'Equipped creature gets +2/+2 and can\'t block.\nEquip {3} ({3}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('copper carapace', ['1']).
card_cmc('copper carapace', 1).
card_layout('copper carapace', 'normal').

% Found in: USG
card_name('copper gnomes', 'Copper Gnomes').
card_type('copper gnomes', 'Artifact Creature — Gnome').
card_types('copper gnomes', ['Artifact', 'Creature']).
card_subtypes('copper gnomes', ['Gnome']).
card_colors('copper gnomes', []).
card_text('copper gnomes', '{4}, Sacrifice Copper Gnomes: You may put an artifact card from your hand onto the battlefield.').
card_mana_cost('copper gnomes', ['2']).
card_cmc('copper gnomes', 2).
card_layout('copper gnomes', 'normal').
card_power('copper gnomes', 1).
card_toughness('copper gnomes', 1).

% Found in: HOP, MRD, SOM
card_name('copper myr', 'Copper Myr').
card_type('copper myr', 'Artifact Creature — Myr').
card_types('copper myr', ['Artifact', 'Creature']).
card_subtypes('copper myr', ['Myr']).
card_colors('copper myr', []).
card_text('copper myr', '{T}: Add {G} to your mana pool.').
card_mana_cost('copper myr', ['2']).
card_cmc('copper myr', 2).
card_layout('copper myr', 'normal').
card_power('copper myr', 1).
card_toughness('copper myr', 1).

% Found in: 2ED, CED, CEI, LEA, LEB, MED
card_name('copper tablet', 'Copper Tablet').
card_type('copper tablet', 'Artifact').
card_types('copper tablet', ['Artifact']).
card_subtypes('copper tablet', []).
card_colors('copper tablet', []).
card_text('copper tablet', 'At the beginning of each player\'s upkeep, Copper Tablet deals 1 damage to that player.').
card_mana_cost('copper tablet', ['2']).
card_cmc('copper tablet', 2).
card_layout('copper tablet', 'normal').

% Found in: PCY
card_name('copper-leaf angel', 'Copper-Leaf Angel').
card_type('copper-leaf angel', 'Artifact Creature — Angel').
card_types('copper-leaf angel', ['Artifact', 'Creature']).
card_subtypes('copper-leaf angel', ['Angel']).
card_colors('copper-leaf angel', []).
card_text('copper-leaf angel', 'Flying\n{T}, Sacrifice X lands: Put X +1/+1 counters on Copper-Leaf Angel.').
card_mana_cost('copper-leaf angel', ['5']).
card_cmc('copper-leaf angel', 5).
card_layout('copper-leaf angel', 'normal').
card_power('copper-leaf angel', 2).
card_toughness('copper-leaf angel', 2).

% Found in: MRD
card_name('copperhoof vorrac', 'Copperhoof Vorrac').
card_type('copperhoof vorrac', 'Creature — Boar Beast').
card_types('copperhoof vorrac', ['Creature']).
card_subtypes('copperhoof vorrac', ['Boar', 'Beast']).
card_colors('copperhoof vorrac', ['G']).
card_text('copperhoof vorrac', 'Copperhoof Vorrac gets +1/+1 for each untapped permanent your opponents control.').
card_mana_cost('copperhoof vorrac', ['3', 'G', 'G']).
card_cmc('copperhoof vorrac', 5).
card_layout('copperhoof vorrac', 'normal').
card_power('copperhoof vorrac', 2).
card_toughness('copperhoof vorrac', 2).

% Found in: CNS, SOM
card_name('copperhorn scout', 'Copperhorn Scout').
card_type('copperhorn scout', 'Creature — Elf Scout').
card_types('copperhorn scout', ['Creature']).
card_subtypes('copperhorn scout', ['Elf', 'Scout']).
card_colors('copperhorn scout', ['G']).
card_text('copperhorn scout', 'Whenever Copperhorn Scout attacks, untap each other creature you control.').
card_mana_cost('copperhorn scout', ['G']).
card_cmc('copperhorn scout', 1).
card_layout('copperhorn scout', 'normal').
card_power('copperhorn scout', 1).
card_toughness('copperhorn scout', 1).

% Found in: SOM
card_name('copperline gorge', 'Copperline Gorge').
card_type('copperline gorge', 'Land').
card_types('copperline gorge', ['Land']).
card_subtypes('copperline gorge', []).
card_colors('copperline gorge', []).
card_text('copperline gorge', 'Copperline Gorge enters the battlefield tapped unless you control two or fewer other lands.\n{T}: Add {R} or {G} to your mana pool.').
card_layout('copperline gorge', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME4
card_name('copy artifact', 'Copy Artifact').
card_type('copy artifact', 'Enchantment').
card_types('copy artifact', ['Enchantment']).
card_subtypes('copy artifact', []).
card_colors('copy artifact', ['U']).
card_text('copy artifact', 'You may have Copy Artifact enter the battlefield as a copy of any artifact on the battlefield, except it\'s an enchantment in addition to its other types.').
card_mana_cost('copy artifact', ['1', 'U']).
card_cmc('copy artifact', 2).
card_layout('copy artifact', 'normal').
card_reserved('copy artifact').

% Found in: RAV
card_name('copy enchantment', 'Copy Enchantment').
card_type('copy enchantment', 'Enchantment').
card_types('copy enchantment', ['Enchantment']).
card_subtypes('copy enchantment', []).
card_colors('copy enchantment', ['U']).
card_text('copy enchantment', 'You may have Copy Enchantment enter the battlefield as a copy of any enchantment on the battlefield.').
card_mana_cost('copy enchantment', ['2', 'U']).
card_cmc('copy enchantment', 3).
card_layout('copy enchantment', 'normal').

% Found in: C14, VIS
card_name('coral atoll', 'Coral Atoll').
card_type('coral atoll', 'Land').
card_types('coral atoll', ['Land']).
card_subtypes('coral atoll', []).
card_colors('coral atoll', []).
card_text('coral atoll', 'Coral Atoll enters the battlefield tapped.\nWhen Coral Atoll enters the battlefield, sacrifice it unless you return an untapped Island you control to its owner\'s hand.\n{T}: Add {1}{U} to your mana pool.').
card_layout('coral atoll', 'normal').

% Found in: M15
card_name('coral barrier', 'Coral Barrier').
card_type('coral barrier', 'Creature — Wall').
card_types('coral barrier', ['Creature']).
card_subtypes('coral barrier', ['Wall']).
card_colors('coral barrier', ['U']).
card_text('coral barrier', 'Defender (This creature can\'t attack.)\nWhen Coral Barrier enters the battlefield, put a 1/1 blue Squid creature token with islandwalk onto the battlefield. (It can\'t be blocked as long as defending player controls an Island.)').
card_mana_cost('coral barrier', ['2', 'U']).
card_cmc('coral barrier', 3).
card_layout('coral barrier', 'normal').
card_power('coral barrier', 1).
card_toughness('coral barrier', 3).

% Found in: 8ED, 9ED, POR, S99
card_name('coral eel', 'Coral Eel').
card_type('coral eel', 'Creature — Fish').
card_types('coral eel', ['Creature']).
card_subtypes('coral eel', ['Fish']).
card_colors('coral eel', ['U']).
card_text('coral eel', '').
card_mana_cost('coral eel', ['1', 'U']).
card_cmc('coral eel', 2).
card_layout('coral eel', 'normal').
card_power('coral eel', 2).
card_toughness('coral eel', 1).

% Found in: DDI, MIR
card_name('coral fighters', 'Coral Fighters').
card_type('coral fighters', 'Creature — Merfolk Soldier').
card_types('coral fighters', ['Creature']).
card_subtypes('coral fighters', ['Merfolk', 'Soldier']).
card_colors('coral fighters', ['U']).
card_text('coral fighters', 'Whenever Coral Fighters attacks and isn\'t blocked, look at the top card of defending player\'s library. You may put that card on the bottom of that player\'s library.').
card_mana_cost('coral fighters', ['1', 'U']).
card_cmc('coral fighters', 2).
card_layout('coral fighters', 'normal').
card_power('coral fighters', 1).
card_toughness('coral fighters', 1).

% Found in: 4ED, 5ED, ATQ, ME4
card_name('coral helm', 'Coral Helm').
card_type('coral helm', 'Artifact').
card_types('coral helm', ['Artifact']).
card_subtypes('coral helm', []).
card_colors('coral helm', []).
card_text('coral helm', '{3}, Discard a card at random: Target creature gets +2/+2 until end of turn.').
card_mana_cost('coral helm', ['3']).
card_cmc('coral helm', 3).
card_layout('coral helm', 'normal').

% Found in: 7ED, M10, M12, M14, USG
card_name('coral merfolk', 'Coral Merfolk').
card_type('coral merfolk', 'Creature — Merfolk').
card_types('coral merfolk', ['Creature']).
card_subtypes('coral merfolk', ['Merfolk']).
card_colors('coral merfolk', ['U']).
card_text('coral merfolk', '').
card_mana_cost('coral merfolk', ['1', 'U']).
card_cmc('coral merfolk', 2).
card_layout('coral merfolk', 'normal').
card_power('coral merfolk', 2).
card_toughness('coral merfolk', 1).

% Found in: TOR
card_name('coral net', 'Coral Net').
card_type('coral net', 'Enchantment — Aura').
card_types('coral net', ['Enchantment']).
card_subtypes('coral net', ['Aura']).
card_colors('coral net', ['U']).
card_text('coral net', 'Enchant green or white creature\nEnchanted creature has \"At the beginning of your upkeep, sacrifice this creature unless you discard a card.\"').
card_mana_cost('coral net', ['U']).
card_cmc('coral net', 1).
card_layout('coral net', 'normal').

% Found in: HML
card_name('coral reef', 'Coral Reef').
card_type('coral reef', 'Enchantment').
card_types('coral reef', ['Enchantment']).
card_subtypes('coral reef', []).
card_colors('coral reef', ['U']).
card_text('coral reef', 'Coral Reef enters the battlefield with four polyp counters on it.\nSacrifice an Island: Put two polyp counters on Coral Reef.\n{U}, Tap an untapped blue creature you control, Remove a polyp counter from Coral Reef: Put a +0/+1 counter on target creature.').
card_mana_cost('coral reef', ['U', 'U']).
card_cmc('coral reef', 2).
card_layout('coral reef', 'normal').

% Found in: DDN, TSP
card_name('coral trickster', 'Coral Trickster').
card_type('coral trickster', 'Creature — Merfolk Rogue').
card_types('coral trickster', ['Creature']).
card_subtypes('coral trickster', ['Merfolk', 'Rogue']).
card_colors('coral trickster', ['U']).
card_text('coral trickster', 'Morph {U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Coral Trickster is turned face up, you may tap or untap target permanent.').
card_mana_cost('coral trickster', ['1', 'U']).
card_cmc('coral trickster', 2).
card_layout('coral trickster', 'normal').
card_power('coral trickster', 2).
card_toughness('coral trickster', 1).

% Found in: ROE
card_name('coralhelm commander', 'Coralhelm Commander').
card_type('coralhelm commander', 'Creature — Merfolk Soldier').
card_types('coralhelm commander', ['Creature']).
card_subtypes('coralhelm commander', ['Merfolk', 'Soldier']).
card_colors('coralhelm commander', ['U']).
card_text('coralhelm commander', 'Level up {1} ({1}: Put a level counter on this. Level up only as a sorcery.)\nLEVEL 2-3\n3/3\nFlying\nLEVEL 4+\n4/4\nFlying\nOther Merfolk creatures you control get +1/+1.').
card_mana_cost('coralhelm commander', ['U', 'U']).
card_cmc('coralhelm commander', 2).
card_layout('coralhelm commander', 'leveler').
card_power('coralhelm commander', 2).
card_toughness('coralhelm commander', 2).

% Found in: BFZ
card_name('coralhelm guide', 'Coralhelm Guide').
card_type('coralhelm guide', 'Creature — Merfolk Scout Ally').
card_types('coralhelm guide', ['Creature']).
card_subtypes('coralhelm guide', ['Merfolk', 'Scout', 'Ally']).
card_colors('coralhelm guide', ['U']).
card_text('coralhelm guide', '{4}{U}: Target creature can\'t be blocked this turn.').
card_mana_cost('coralhelm guide', ['1', 'U']).
card_cmc('coralhelm guide', 2).
card_layout('coralhelm guide', 'normal').
card_power('coralhelm guide', 2).
card_toughness('coralhelm guide', 1).

% Found in: MBS
card_name('core prowler', 'Core Prowler').
card_type('core prowler', 'Artifact Creature — Horror').
card_types('core prowler', ['Artifact', 'Creature']).
card_subtypes('core prowler', ['Horror']).
card_colors('core prowler', []).
card_text('core prowler', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Core Prowler dies, proliferate. (You choose any number of permanents and/or players with counters on them, then give each another counter of a kind already there.)').
card_mana_cost('core prowler', ['4']).
card_cmc('core prowler', 4).
card_layout('core prowler', 'normal').
card_power('core prowler', 2).
card_toughness('core prowler', 2).

% Found in: DST
card_name('coretapper', 'Coretapper').
card_type('coretapper', 'Artifact Creature — Myr').
card_types('coretapper', ['Artifact', 'Creature']).
card_subtypes('coretapper', ['Myr']).
card_colors('coretapper', []).
card_text('coretapper', '{T}: Put a charge counter on target artifact.\nSacrifice Coretapper: Put two charge counters on target artifact.').
card_mana_cost('coretapper', ['2']).
card_cmc('coretapper', 2).
card_layout('coretapper', 'normal').
card_power('coretapper', 1).
card_toughness('coretapper', 1).

% Found in: MMQ
card_name('cornered market', 'Cornered Market').
card_type('cornered market', 'Enchantment').
card_types('cornered market', ['Enchantment']).
card_subtypes('cornered market', []).
card_colors('cornered market', ['W']).
card_text('cornered market', 'Players can\'t cast spells with the same name as a nontoken permanent.\nPlayers can\'t play nonbasic lands with the same name as a nontoken permanent.').
card_mana_cost('cornered market', ['2', 'W']).
card_cmc('cornered market', 3).
card_layout('cornered market', 'normal').

% Found in: GTC
card_name('corpse blockade', 'Corpse Blockade').
card_type('corpse blockade', 'Creature — Zombie').
card_types('corpse blockade', ['Creature']).
card_subtypes('corpse blockade', ['Zombie']).
card_colors('corpse blockade', ['B']).
card_text('corpse blockade', 'Defender\nSacrifice another creature: Corpse Blockade gains deathtouch until end of turn.').
card_mana_cost('corpse blockade', ['2', 'B']).
card_cmc('corpse blockade', 3).
card_layout('corpse blockade', 'normal').
card_power('corpse blockade', 1).
card_toughness('corpse blockade', 4).

% Found in: ALA, ARC, DDK
card_name('corpse connoisseur', 'Corpse Connoisseur').
card_type('corpse connoisseur', 'Creature — Zombie Wizard').
card_types('corpse connoisseur', ['Creature']).
card_subtypes('corpse connoisseur', ['Zombie', 'Wizard']).
card_colors('corpse connoisseur', ['B']).
card_text('corpse connoisseur', 'When Corpse Connoisseur enters the battlefield, you may search your library for a creature card and put that card into your graveyard. If you do, shuffle your library.\nUnearth {3}{B} ({3}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('corpse connoisseur', ['4', 'B']).
card_cmc('corpse connoisseur', 5).
card_layout('corpse connoisseur', 'normal').
card_power('corpse connoisseur', 3).
card_toughness('corpse connoisseur', 3).

% Found in: SOM
card_name('corpse cur', 'Corpse Cur').
card_type('corpse cur', 'Artifact Creature — Hound').
card_types('corpse cur', ['Artifact', 'Creature']).
card_subtypes('corpse cur', ['Hound']).
card_colors('corpse cur', []).
card_text('corpse cur', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)\nWhen Corpse Cur enters the battlefield, you may return target creature card with infect from your graveyard to your hand.').
card_mana_cost('corpse cur', ['4']).
card_cmc('corpse cur', 4).
card_layout('corpse cur', 'normal').
card_power('corpse cur', 2).
card_toughness('corpse cur', 2).

% Found in: TMP, TPR
card_name('corpse dance', 'Corpse Dance').
card_type('corpse dance', 'Instant').
card_types('corpse dance', ['Instant']).
card_subtypes('corpse dance', []).
card_colors('corpse dance', ['B']).
card_text('corpse dance', 'Buyback {2} (You may pay an additional {2} as you cast this spell. If you do, put this card into your hand as it resolves.)\nReturn the top creature card of your graveyard to the battlefield. That creature gains haste until end of turn. Exile it at the beginning of the next end step.').
card_mana_cost('corpse dance', ['2', 'B']).
card_cmc('corpse dance', 3).
card_layout('corpse dance', 'normal').
card_reserved('corpse dance').

% Found in: HOP, LGN
card_name('corpse harvester', 'Corpse Harvester').
card_type('corpse harvester', 'Creature — Zombie Wizard').
card_types('corpse harvester', ['Creature']).
card_subtypes('corpse harvester', ['Zombie', 'Wizard']).
card_colors('corpse harvester', ['B']).
card_text('corpse harvester', '{1}{B}, {T}, Sacrifice a creature: Search your library for a Zombie card and a Swamp card, reveal them, and put them into your hand. Then shuffle your library.').
card_mana_cost('corpse harvester', ['3', 'B', 'B']).
card_cmc('corpse harvester', 5).
card_layout('corpse harvester', 'normal').
card_power('corpse harvester', 3).
card_toughness('corpse harvester', 3).

% Found in: M14
card_name('corpse hauler', 'Corpse Hauler').
card_type('corpse hauler', 'Creature — Human Rogue').
card_types('corpse hauler', ['Creature']).
card_subtypes('corpse hauler', ['Human', 'Rogue']).
card_colors('corpse hauler', ['B']).
card_text('corpse hauler', '{2}{B}, Sacrifice Corpse Hauler: Return another target creature card from your graveyard to your hand.').
card_mana_cost('corpse hauler', ['1', 'B']).
card_cmc('corpse hauler', 2).
card_layout('corpse hauler', 'normal').
card_power('corpse hauler', 2).
card_toughness('corpse hauler', 1).

% Found in: ISD
card_name('corpse lunge', 'Corpse Lunge').
card_type('corpse lunge', 'Instant').
card_types('corpse lunge', ['Instant']).
card_subtypes('corpse lunge', []).
card_colors('corpse lunge', ['B']).
card_text('corpse lunge', 'As an additional cost to cast Corpse Lunge, exile a creature card from your graveyard.\nCorpse Lunge deals damage equal to the exiled card\'s power to target creature.').
card_mana_cost('corpse lunge', ['2', 'B']).
card_cmc('corpse lunge', 3).
card_layout('corpse lunge', 'normal').

% Found in: AVR, DDM
card_name('corpse traders', 'Corpse Traders').
card_type('corpse traders', 'Creature — Human Rogue').
card_types('corpse traders', ['Creature']).
card_subtypes('corpse traders', ['Human', 'Rogue']).
card_colors('corpse traders', ['B']).
card_text('corpse traders', '{2}{B}, Sacrifice a creature: Target opponent reveals his or her hand. You choose a card from it. That player discards that card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('corpse traders', ['3', 'B']).
card_cmc('corpse traders', 4).
card_layout('corpse traders', 'normal').
card_power('corpse traders', 3).
card_toughness('corpse traders', 3).

% Found in: DDP, ROE
card_name('corpsehatch', 'Corpsehatch').
card_type('corpsehatch', 'Sorcery').
card_types('corpsehatch', ['Sorcery']).
card_subtypes('corpsehatch', []).
card_colors('corpsehatch', ['B']).
card_text('corpsehatch', 'Destroy target nonblack creature. Put two 0/1 colorless Eldrazi Spawn creature tokens onto the battlefield. They have \"Sacrifice this creature: Add {1} to your mana pool.\"').
card_mana_cost('corpsehatch', ['3', 'B', 'B']).
card_cmc('corpsehatch', 5).
card_layout('corpsehatch', 'normal').

% Found in: RTR, pPRE
card_name('corpsejack menace', 'Corpsejack Menace').
card_type('corpsejack menace', 'Creature — Fungus').
card_types('corpsejack menace', ['Creature']).
card_subtypes('corpsejack menace', ['Fungus']).
card_colors('corpsejack menace', ['B', 'G']).
card_text('corpsejack menace', 'If one or more +1/+1 counters would be placed on a creature you control, twice that many +1/+1 counters are placed on it instead.').
card_mana_cost('corpsejack menace', ['2', 'B', 'G']).
card_cmc('corpsejack menace', 4).
card_layout('corpsejack menace', 'normal').
card_power('corpsejack menace', 4).
card_toughness('corpsejack menace', 4).

% Found in: DTK
card_name('corpseweft', 'Corpseweft').
card_type('corpseweft', 'Enchantment').
card_types('corpseweft', ['Enchantment']).
card_subtypes('corpseweft', []).
card_colors('corpseweft', ['B']).
card_text('corpseweft', '{1}{B}, Exile one or more creature cards from your graveyard: Put an X/X black Zombie Horror creature token onto the battlefield tapped, where X is twice the number of cards exiled this way.').
card_mana_cost('corpseweft', ['2', 'B']).
card_cmc('corpseweft', 3).
card_layout('corpseweft', 'normal').

% Found in: TSP
card_name('corpulent corpse', 'Corpulent Corpse').
card_type('corpulent corpse', 'Creature — Zombie').
card_types('corpulent corpse', ['Creature']).
card_subtypes('corpulent corpse', ['Zombie']).
card_colors('corpulent corpse', ['B']).
card_text('corpulent corpse', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nSuspend 5—{B} (Rather than cast this card from your hand, you may pay {B} and exile it with five time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost. It has haste.)').
card_mana_cost('corpulent corpse', ['5', 'B']).
card_cmc('corpulent corpse', 6).
card_layout('corpulent corpse', 'normal').
card_power('corpulent corpse', 3).
card_toughness('corpulent corpse', 3).

% Found in: VIS
card_name('corrosion', 'Corrosion').
card_type('corrosion', 'Enchantment').
card_types('corrosion', ['Enchantment']).
card_subtypes('corrosion', []).
card_colors('corrosion', ['B', 'R']).
card_text('corrosion', 'Cumulative upkeep {1} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)\nAt the beginning of your upkeep, put a rust counter on each artifact target opponent controls. Then destroy each artifact with converted mana cost less than or equal to the number of rust counters on it. Artifacts destroyed this way can\'t be regenerated.\nWhen Corrosion leaves the battlefield, remove all rust counters from all permanents.').
card_mana_cost('corrosion', ['1', 'B', 'R']).
card_cmc('corrosion', 3).
card_layout('corrosion', 'normal').
card_reserved('corrosion').

% Found in: NPH
card_name('corrosive gale', 'Corrosive Gale').
card_type('corrosive gale', 'Sorcery').
card_types('corrosive gale', ['Sorcery']).
card_subtypes('corrosive gale', []).
card_colors('corrosive gale', ['G']).
card_text('corrosive gale', '({G/P} can be paid with either {G} or 2 life.)\nCorrosive Gale deals X damage to each creature with flying.').
card_mana_cost('corrosive gale', ['X', 'G/P']).
card_cmc('corrosive gale', 1).
card_layout('corrosive gale', 'normal').

% Found in: SHM
card_name('corrosive mentor', 'Corrosive Mentor').
card_type('corrosive mentor', 'Creature — Elemental Rogue').
card_types('corrosive mentor', ['Creature']).
card_subtypes('corrosive mentor', ['Elemental', 'Rogue']).
card_colors('corrosive mentor', ['B']).
card_text('corrosive mentor', 'Black creatures you control have wither. (They deal damage to creatures in the form of -1/-1 counters.)').
card_mana_cost('corrosive mentor', ['2', 'B']).
card_cmc('corrosive mentor', 3).
card_layout('corrosive mentor', 'normal').
card_power('corrosive mentor', 1).
card_toughness('corrosive mentor', 3).

% Found in: 7ED, DD3_DVD, DD3_GVL, DDC, DDD, M11, M14, SHM, USG, pMEI, pMPR
card_name('corrupt', 'Corrupt').
card_type('corrupt', 'Sorcery').
card_types('corrupt', ['Sorcery']).
card_subtypes('corrupt', []).
card_colors('corrupt', ['B']).
card_text('corrupt', 'Corrupt deals damage equal to the number of Swamps you control to target creature or player. You gain life equal to the damage dealt this way.').
card_mana_cost('corrupt', ['5', 'B']).
card_cmc('corrupt', 6).
card_layout('corrupt', 'normal').

% Found in: PTK
card_name('corrupt court official', 'Corrupt Court Official').
card_type('corrupt court official', 'Creature — Human Advisor').
card_types('corrupt court official', ['Creature']).
card_subtypes('corrupt court official', ['Human', 'Advisor']).
card_colors('corrupt court official', ['B']).
card_text('corrupt court official', 'When Corrupt Court Official enters the battlefield, target opponent discards a card.').
card_mana_cost('corrupt court official', ['1', 'B']).
card_cmc('corrupt court official', 2).
card_layout('corrupt court official', 'normal').
card_power('corrupt court official', 1).
card_toughness('corrupt court official', 1).

% Found in: ME3, PTK
card_name('corrupt eunuchs', 'Corrupt Eunuchs').
card_type('corrupt eunuchs', 'Creature — Human Advisor').
card_types('corrupt eunuchs', ['Creature']).
card_subtypes('corrupt eunuchs', ['Human', 'Advisor']).
card_colors('corrupt eunuchs', ['R']).
card_text('corrupt eunuchs', 'When Corrupt Eunuchs enters the battlefield, it deals 2 damage to target creature.').
card_mana_cost('corrupt eunuchs', ['3', 'R']).
card_cmc('corrupt eunuchs', 4).
card_layout('corrupt eunuchs', 'normal').
card_power('corrupt eunuchs', 2).
card_toughness('corrupt eunuchs', 2).

% Found in: MMQ
card_name('corrupt official', 'Corrupt Official').
card_type('corrupt official', 'Creature — Human Minion').
card_types('corrupt official', ['Creature']).
card_subtypes('corrupt official', ['Human', 'Minion']).
card_colors('corrupt official', ['B']).
card_text('corrupt official', '{2}{B}: Regenerate Corrupt Official.\nWhenever Corrupt Official becomes blocked, defending player discards a card at random.').
card_mana_cost('corrupt official', ['4', 'B']).
card_cmc('corrupt official', 5).
card_layout('corrupt official', 'normal').
card_power('corrupt official', 3).
card_toughness('corrupt official', 1).

% Found in: MBS
card_name('corrupted conscience', 'Corrupted Conscience').
card_type('corrupted conscience', 'Enchantment — Aura').
card_types('corrupted conscience', ['Enchantment']).
card_subtypes('corrupted conscience', ['Aura']).
card_colors('corrupted conscience', ['U']).
card_text('corrupted conscience', 'Enchant creature\nYou control enchanted creature.\nEnchanted creature has infect. (It deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('corrupted conscience', ['3', 'U', 'U']).
card_cmc('corrupted conscience', 5).
card_layout('corrupted conscience', 'normal').

% Found in: SOM
card_name('corrupted harvester', 'Corrupted Harvester').
card_type('corrupted harvester', 'Creature — Horror').
card_types('corrupted harvester', ['Creature']).
card_subtypes('corrupted harvester', ['Horror']).
card_colors('corrupted harvester', ['B']).
card_text('corrupted harvester', '{B}, Sacrifice a creature: Regenerate Corrupted Harvester.').
card_mana_cost('corrupted harvester', ['4', 'B', 'B']).
card_cmc('corrupted harvester', 6).
card_layout('corrupted harvester', 'normal').
card_power('corrupted harvester', 6).
card_toughness('corrupted harvester', 3).

% Found in: NPH
card_name('corrupted resolve', 'Corrupted Resolve').
card_type('corrupted resolve', 'Instant').
card_types('corrupted resolve', ['Instant']).
card_subtypes('corrupted resolve', []).
card_colors('corrupted resolve', ['U']).
card_text('corrupted resolve', 'Counter target spell if its controller is poisoned.').
card_mana_cost('corrupted resolve', ['1', 'U']).
card_cmc('corrupted resolve', 2).
card_layout('corrupted resolve', 'normal').

% Found in: CON
card_name('corrupted roots', 'Corrupted Roots').
card_type('corrupted roots', 'Enchantment — Aura').
card_types('corrupted roots', ['Enchantment']).
card_subtypes('corrupted roots', ['Aura']).
card_colors('corrupted roots', ['B']).
card_text('corrupted roots', 'Enchant Forest or Plains\nWhenever enchanted land becomes tapped, its controller loses 2 life.').
card_mana_cost('corrupted roots', ['B']).
card_cmc('corrupted roots', 1).
card_layout('corrupted roots', 'normal').

% Found in: WWK
card_name('corrupted zendikon', 'Corrupted Zendikon').
card_type('corrupted zendikon', 'Enchantment — Aura').
card_types('corrupted zendikon', ['Enchantment']).
card_subtypes('corrupted zendikon', ['Aura']).
card_colors('corrupted zendikon', ['B']).
card_text('corrupted zendikon', 'Enchant land\nEnchanted land is a 3/3 black Ooze creature. It\'s still a land.\nWhen enchanted land dies, return that card to its owner\'s hand.').
card_mana_cost('corrupted zendikon', ['1', 'B']).
card_cmc('corrupted zendikon', 2).
card_layout('corrupted zendikon', 'normal').

% Found in: STH
card_name('corrupting licid', 'Corrupting Licid').
card_type('corrupting licid', 'Creature — Licid').
card_types('corrupting licid', ['Creature']).
card_subtypes('corrupting licid', ['Licid']).
card_colors('corrupting licid', ['B']).
card_text('corrupting licid', '{B}, {T}: Corrupting Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {B} to end this effect.\nEnchanted creature has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('corrupting licid', ['2', 'B']).
card_cmc('corrupting licid', 3).
card_layout('corrupting licid', 'normal').
card_power('corrupting licid', 2).
card_toughness('corrupting licid', 2).

% Found in: DDI, WWK
card_name('cosi\'s ravager', 'Cosi\'s Ravager').
card_type('cosi\'s ravager', 'Creature — Elemental').
card_types('cosi\'s ravager', ['Creature']).
card_subtypes('cosi\'s ravager', ['Elemental']).
card_colors('cosi\'s ravager', ['R']).
card_text('cosi\'s ravager', 'Landfall — Whenever a land enters the battlefield under your control, you may have Cosi\'s Ravager deal 1 damage to target player.').
card_mana_cost('cosi\'s ravager', ['3', 'R']).
card_cmc('cosi\'s ravager', 4).
card_layout('cosi\'s ravager', 'normal').
card_power('cosi\'s ravager', 2).
card_toughness('cosi\'s ravager', 2).

% Found in: ZEN
card_name('cosi\'s trickster', 'Cosi\'s Trickster').
card_type('cosi\'s trickster', 'Creature — Merfolk Wizard').
card_types('cosi\'s trickster', ['Creature']).
card_subtypes('cosi\'s trickster', ['Merfolk', 'Wizard']).
card_colors('cosi\'s trickster', ['U']).
card_text('cosi\'s trickster', 'Whenever an opponent shuffles his or her library, you may put a +1/+1 counter on Cosi\'s Trickster.').
card_mana_cost('cosi\'s trickster', ['U']).
card_cmc('cosi\'s trickster', 1).
card_layout('cosi\'s trickster', 'normal').
card_power('cosi\'s trickster', 1).
card_toughness('cosi\'s trickster', 1).

% Found in: 4ED, LEG, ME3
card_name('cosmic horror', 'Cosmic Horror').
card_type('cosmic horror', 'Creature — Horror').
card_types('cosmic horror', ['Creature']).
card_subtypes('cosmic horror', ['Horror']).
card_colors('cosmic horror', ['B']).
card_text('cosmic horror', 'First strike\nAt the beginning of your upkeep, destroy Cosmic Horror unless you pay {3}{B}{B}{B}. If Cosmic Horror is destroyed this way, it deals 7 damage to you.').
card_mana_cost('cosmic horror', ['3', 'B', 'B', 'B']).
card_cmc('cosmic horror', 6).
card_layout('cosmic horror', 'normal').
card_power('cosmic horror', 7).
card_toughness('cosmic horror', 7).

% Found in: 5DN
card_name('cosmic larva', 'Cosmic Larva').
card_type('cosmic larva', 'Creature — Beast').
card_types('cosmic larva', ['Creature']).
card_subtypes('cosmic larva', ['Beast']).
card_colors('cosmic larva', ['R']).
card_text('cosmic larva', 'Trample\nAt the beginning of your upkeep, sacrifice Cosmic Larva unless you sacrifice two lands.').
card_mana_cost('cosmic larva', ['1', 'R', 'R']).
card_cmc('cosmic larva', 3).
card_layout('cosmic larva', 'normal').
card_power('cosmic larva', 7).
card_toughness('cosmic larva', 6).

% Found in: CNS
card_name('council guardian', 'Council Guardian').
card_type('council guardian', 'Creature — Giant Soldier').
card_types('council guardian', ['Creature']).
card_subtypes('council guardian', ['Giant', 'Soldier']).
card_colors('council guardian', ['W']).
card_text('council guardian', 'Will of the council — When Council Guardian enters the battlefield, starting with you, each player votes for blue, black, red, or green. Council Guardian gains protection from each color with the most votes or tied for most votes.').
card_mana_cost('council guardian', ['5', 'W']).
card_cmc('council guardian', 6).
card_layout('council guardian', 'normal').
card_power('council guardian', 5).
card_toughness('council guardian', 5).

% Found in: PTK
card_name('council of advisors', 'Council of Advisors').
card_type('council of advisors', 'Creature — Human Advisor').
card_types('council of advisors', ['Creature']).
card_subtypes('council of advisors', ['Human', 'Advisor']).
card_colors('council of advisors', ['U']).
card_text('council of advisors', 'When Council of Advisors enters the battlefield, draw a card.').
card_mana_cost('council of advisors', ['2', 'U']).
card_cmc('council of advisors', 3).
card_layout('council of advisors', 'normal').
card_power('council of advisors', 1).
card_toughness('council of advisors', 1).

% Found in: DGM
card_name('council of the absolute', 'Council of the Absolute').
card_type('council of the absolute', 'Creature — Human Advisor').
card_types('council of the absolute', ['Creature']).
card_subtypes('council of the absolute', ['Human', 'Advisor']).
card_colors('council of the absolute', ['W', 'U']).
card_text('council of the absolute', 'As Council of the Absolute enters the battlefield, name a card other than a creature or land card.\nYour opponents can\'t cast cards with the chosen name.\nSpells with the chosen name you cast cost {2} less to cast.').
card_mana_cost('council of the absolute', ['2', 'W', 'U']).
card_cmc('council of the absolute', 4).
card_layout('council of the absolute', 'normal').
card_power('council of the absolute', 2).
card_toughness('council of the absolute', 4).

% Found in: CNS, VMA
card_name('council\'s judgment', 'Council\'s Judgment').
card_type('council\'s judgment', 'Sorcery').
card_types('council\'s judgment', ['Sorcery']).
card_subtypes('council\'s judgment', []).
card_colors('council\'s judgment', ['W']).
card_text('council\'s judgment', 'Will of the council — Starting with you, each player votes for a nonland permanent you don\'t control. Exile each permanent with the most votes or tied for most votes.').
card_mana_cost('council\'s judgment', ['1', 'W', 'W']).
card_cmc('council\'s judgment', 3).
card_layout('council\'s judgment', 'normal').

% Found in: 10E, 9ED, CHK
card_name('counsel of the soratami', 'Counsel of the Soratami').
card_type('counsel of the soratami', 'Sorcery').
card_types('counsel of the soratami', ['Sorcery']).
card_subtypes('counsel of the soratami', []).
card_colors('counsel of the soratami', ['U']).
card_text('counsel of the soratami', 'Draw two cards.').
card_mana_cost('counsel of the soratami', ['2', 'U']).
card_cmc('counsel of the soratami', 3).
card_layout('counsel of the soratami', 'normal').

% Found in: CSP
card_name('counterbalance', 'Counterbalance').
card_type('counterbalance', 'Enchantment').
card_types('counterbalance', ['Enchantment']).
card_subtypes('counterbalance', []).
card_colors('counterbalance', ['U']).
card_text('counterbalance', 'Whenever an opponent casts a spell, you may reveal the top card of your library. If you do, counter that spell if it has the same converted mana cost as the revealed card.').
card_mana_cost('counterbalance', ['U', 'U']).
card_cmc('counterbalance', 2).
card_layout('counterbalance', 'normal').

% Found in: DPA, SHM
card_name('counterbore', 'Counterbore').
card_type('counterbore', 'Instant').
card_types('counterbore', ['Instant']).
card_subtypes('counterbore', []).
card_colors('counterbore', ['U']).
card_text('counterbore', 'Counter target spell. Search its controller\'s graveyard, hand, and library for all cards with the same name as that spell and exile them. Then that player shuffles his or her library.').
card_mana_cost('counterbore', ['3', 'U', 'U']).
card_cmc('counterbore', 5).
card_layout('counterbore', 'normal').

% Found in: RTR
card_name('counterflux', 'Counterflux').
card_type('counterflux', 'Instant').
card_types('counterflux', ['Instant']).
card_subtypes('counterflux', []).
card_colors('counterflux', ['U', 'R']).
card_text('counterflux', 'Counterflux can\'t be countered by spells or abilities.\nCounter target spell you don\'t control.\nOverload {1}{U}{U}{R} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('counterflux', ['U', 'U', 'R']).
card_cmc('counterflux', 3).
card_layout('counterflux', 'normal').

% Found in: PTK
card_name('counterintelligence', 'Counterintelligence').
card_type('counterintelligence', 'Sorcery').
card_types('counterintelligence', ['Sorcery']).
card_subtypes('counterintelligence', []).
card_colors('counterintelligence', ['U']).
card_text('counterintelligence', 'Return one or two target creatures to their owners\' hands.').
card_mana_cost('counterintelligence', ['2', 'U', 'U']).
card_cmc('counterintelligence', 4).
card_layout('counterintelligence', 'normal').

% Found in: DKA
card_name('counterlash', 'Counterlash').
card_type('counterlash', 'Instant').
card_types('counterlash', ['Instant']).
card_subtypes('counterlash', []).
card_colors('counterlash', ['U']).
card_text('counterlash', 'Counter target spell. You may cast a nonland card in your hand that shares a card type with that spell without paying its mana cost.').
card_mana_cost('counterlash', ['4', 'U', 'U']).
card_cmc('counterlash', 6).
card_layout('counterlash', 'normal').

% Found in: JOU
card_name('countermand', 'Countermand').
card_type('countermand', 'Instant').
card_types('countermand', ['Instant']).
card_subtypes('countermand', []).
card_colors('countermand', ['U']).
card_text('countermand', 'Counter target spell. Its controller puts the top four cards of his or her library into his or her graveyard.').
card_mana_cost('countermand', ['2', 'U', 'U']).
card_cmc('countermand', 4).
card_layout('countermand', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, BRB, BTD, CED, CEI, DD2, DD3_JVC, ICE, LEA, LEB, ME2, ME4, MMQ, S00, S99, TMP, TPR, VMA, pFNM, pJGP, pLGM
card_name('counterspell', 'Counterspell').
card_type('counterspell', 'Instant').
card_types('counterspell', ['Instant']).
card_subtypes('counterspell', []).
card_colors('counterspell', ['U']).
card_text('counterspell', 'Counter target spell.').
card_mana_cost('counterspell', ['U', 'U']).
card_cmc('counterspell', 2).
card_layout('counterspell', 'normal').

% Found in: CON, DDH
card_name('countersquall', 'Countersquall').
card_type('countersquall', 'Instant').
card_types('countersquall', ['Instant']).
card_subtypes('countersquall', []).
card_colors('countersquall', ['U', 'B']).
card_text('countersquall', 'Counter target noncreature spell. Its controller loses 2 life.').
card_mana_cost('countersquall', ['U', 'B']).
card_cmc('countersquall', 2).
card_layout('countersquall', 'normal').

% Found in: MMA, MOR
card_name('countryside crusher', 'Countryside Crusher').
card_type('countryside crusher', 'Creature — Giant Warrior').
card_types('countryside crusher', ['Creature']).
card_subtypes('countryside crusher', ['Giant', 'Warrior']).
card_colors('countryside crusher', ['R']).
card_text('countryside crusher', 'At the beginning of your upkeep, reveal the top card of your library. If it\'s a land card, put it into your graveyard and repeat this process.\nWhenever a land card is put into your graveyard from anywhere, put a +1/+1 counter on Countryside Crusher.').
card_mana_cost('countryside crusher', ['1', 'R', 'R']).
card_cmc('countryside crusher', 3).
card_layout('countryside crusher', 'normal').
card_power('countryside crusher', 3).
card_toughness('countryside crusher', 3).

% Found in: BFZ
card_name('courier griffin', 'Courier Griffin').
card_type('courier griffin', 'Creature — Griffin').
card_types('courier griffin', ['Creature']).
card_subtypes('courier griffin', ['Griffin']).
card_colors('courier griffin', ['W']).
card_text('courier griffin', 'Flying\nWhen Courier Griffin enters the battlefield, you gain 2 life.').
card_mana_cost('courier griffin', ['3', 'W']).
card_cmc('courier griffin', 4).
card_layout('courier griffin', 'normal').
card_power('courier griffin', 2).
card_toughness('courier griffin', 3).

% Found in: CNS, RAV
card_name('courier hawk', 'Courier Hawk').
card_type('courier hawk', 'Creature — Bird').
card_types('courier hawk', ['Creature']).
card_subtypes('courier hawk', ['Bird']).
card_colors('courier hawk', ['W']).
card_text('courier hawk', 'Flying, vigilance').
card_mana_cost('courier hawk', ['1', 'W']).
card_cmc('courier hawk', 2).
card_layout('courier hawk', 'normal').
card_power('courier hawk', 1).
card_toughness('courier hawk', 2).

% Found in: ALA
card_name('courier\'s capsule', 'Courier\'s Capsule').
card_type('courier\'s capsule', 'Artifact').
card_types('courier\'s capsule', ['Artifact']).
card_subtypes('courier\'s capsule', []).
card_colors('courier\'s capsule', ['U']).
card_text('courier\'s capsule', '{1}{U}, {T}, Sacrifice Courier\'s Capsule: Draw two cards.').
card_mana_cost('courier\'s capsule', ['1', 'U']).
card_cmc('courier\'s capsule', 2).
card_layout('courier\'s capsule', 'normal').

% Found in: BNG, CPK
card_name('courser of kruphix', 'Courser of Kruphix').
card_type('courser of kruphix', 'Enchantment Creature — Centaur').
card_types('courser of kruphix', ['Enchantment', 'Creature']).
card_subtypes('courser of kruphix', ['Centaur']).
card_colors('courser of kruphix', ['G']).
card_text('courser of kruphix', 'Play with the top card of your library revealed.\nYou may play the top card of your library if it\'s a land card.\nWhenever a land enters the battlefield under your control, you gain 1 life.').
card_mana_cost('courser of kruphix', ['1', 'G', 'G']).
card_cmc('courser of kruphix', 3).
card_layout('courser of kruphix', 'normal').
card_power('courser of kruphix', 2).
card_toughness('courser of kruphix', 4).

% Found in: RTR
card_name('coursers\' accord', 'Coursers\' Accord').
card_type('coursers\' accord', 'Sorcery').
card_types('coursers\' accord', ['Sorcery']).
card_subtypes('coursers\' accord', []).
card_colors('coursers\' accord', ['W', 'G']).
card_text('coursers\' accord', 'Put a 3/3 green Centaur creature token onto the battlefield, then populate. (Put a token onto the battlefield that\'s a copy of a creature token you control.)').
card_mana_cost('coursers\' accord', ['4', 'G', 'W']).
card_cmc('coursers\' accord', 6).
card_layout('coursers\' accord', 'normal').

% Found in: ALA
card_name('court archers', 'Court Archers').
card_type('court archers', 'Creature — Human Archer').
card_types('court archers', ['Creature']).
card_subtypes('court archers', ['Human', 'Archer']).
card_colors('court archers', ['G']).
card_text('court archers', 'Reach (This creature can block creatures with flying.)\nExalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)').
card_mana_cost('court archers', ['2', 'G']).
card_cmc('court archers', 3).
card_layout('court archers', 'normal').
card_power('court archers', 1).
card_toughness('court archers', 3).

% Found in: CON, MM2, MMA
card_name('court homunculus', 'Court Homunculus').
card_type('court homunculus', 'Artifact Creature — Homunculus').
card_types('court homunculus', ['Artifact', 'Creature']).
card_subtypes('court homunculus', ['Homunculus']).
card_colors('court homunculus', ['W']).
card_text('court homunculus', 'Court Homunculus gets +1/+1 as long as you control another artifact.').
card_mana_cost('court homunculus', ['W']).
card_cmc('court homunculus', 1).
card_layout('court homunculus', 'normal').
card_power('court homunculus', 1).
card_toughness('court homunculus', 1).

% Found in: CMD, DIS
card_name('court hussar', 'Court Hussar').
card_type('court hussar', 'Creature — Vedalken Knight').
card_types('court hussar', ['Creature']).
card_subtypes('court hussar', ['Vedalken', 'Knight']).
card_colors('court hussar', ['U']).
card_text('court hussar', 'Vigilance\nWhen Court Hussar enters the battlefield, look at the top three cards of your library, then put one of them into your hand and the rest on the bottom of your library in any order.\nWhen Court Hussar enters the battlefield, sacrifice it unless {W} was spent to cast it.').
card_mana_cost('court hussar', ['2', 'U']).
card_cmc('court hussar', 3).
card_layout('court hussar', 'normal').
card_power('court hussar', 1).
card_toughness('court hussar', 3).

% Found in: DDO, GTC
card_name('court street denizen', 'Court Street Denizen').
card_type('court street denizen', 'Creature — Human Soldier').
card_types('court street denizen', ['Creature']).
card_subtypes('court street denizen', ['Human', 'Soldier']).
card_colors('court street denizen', ['W']).
card_text('court street denizen', 'Whenever another white creature enters the battlefield under your control, tap target creature an opponent controls.').
card_mana_cost('court street denizen', ['2', 'W']).
card_cmc('court street denizen', 3).
card_layout('court street denizen', 'normal').
card_power('court street denizen', 2).
card_toughness('court street denizen', 2).

% Found in: M13
card_name('courtly provocateur', 'Courtly Provocateur').
card_type('courtly provocateur', 'Creature — Human Wizard').
card_types('courtly provocateur', ['Creature']).
card_subtypes('courtly provocateur', ['Human', 'Wizard']).
card_colors('courtly provocateur', ['U']).
card_text('courtly provocateur', '{T}: Target creature attacks this turn if able.\n{T}: Target creature blocks this turn if able.').
card_mana_cost('courtly provocateur', ['2', 'U']).
card_cmc('courtly provocateur', 3).
card_layout('courtly provocateur', 'normal').
card_power('courtly provocateur', 1).
card_toughness('courtly provocateur', 1).

% Found in: M15
card_name('covenant of blood', 'Covenant of Blood').
card_type('covenant of blood', 'Sorcery').
card_types('covenant of blood', ['Sorcery']).
card_subtypes('covenant of blood', []).
card_colors('covenant of blood', ['B']).
card_text('covenant of blood', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nCovenant of Blood deals 4 damage to target creature or player and you gain 4 life.').
card_mana_cost('covenant of blood', ['6', 'B']).
card_cmc('covenant of blood', 7).
card_layout('covenant of blood', 'normal').

% Found in: ALA
card_name('covenant of minds', 'Covenant of Minds').
card_type('covenant of minds', 'Sorcery').
card_types('covenant of minds', ['Sorcery']).
card_subtypes('covenant of minds', []).
card_colors('covenant of minds', ['U']).
card_text('covenant of minds', 'Reveal the top three cards of your library. Target opponent may choose to put those cards into your hand. If he or she doesn\'t, put those cards into your graveyard and draw five cards.').
card_mana_cost('covenant of minds', ['4', 'U']).
card_cmc('covenant of minds', 5).
card_layout('covenant of minds', 'normal').

% Found in: ONS
card_name('cover of darkness', 'Cover of Darkness').
card_type('cover of darkness', 'Enchantment').
card_types('cover of darkness', ['Enchantment']).
card_subtypes('cover of darkness', []).
card_colors('cover of darkness', ['B']).
card_text('cover of darkness', 'As Cover of Darkness enters the battlefield, choose a creature type.\nCreatures of the chosen type have fear. (They can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('cover of darkness', ['1', 'B']).
card_cmc('cover of darkness', 2).
card_layout('cover of darkness', 'normal').

% Found in: CSP
card_name('cover of winter', 'Cover of Winter').
card_type('cover of winter', 'Snow Enchantment').
card_types('cover of winter', ['Enchantment']).
card_subtypes('cover of winter', []).
card_supertypes('cover of winter', ['Snow']).
card_colors('cover of winter', ['W']).
card_text('cover of winter', 'Cumulative upkeep {S} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it. {S} can be paid with one mana from a snow permanent.)\nIf a creature would deal combat damage to you and/or one or more creatures you control, prevent X of that damage, where X is the number of age counters on Cover of Winter.\n{S}: Put an age counter on Cover of Winter.').
card_mana_cost('cover of winter', ['2', 'W']).
card_cmc('cover of winter', 3).
card_layout('cover of winter', 'normal').

% Found in: LGN
card_name('covert operative', 'Covert Operative').
card_type('covert operative', 'Creature — Human Wizard').
card_types('covert operative', ['Creature']).
card_subtypes('covert operative', ['Human', 'Wizard']).
card_colors('covert operative', ['U']).
card_text('covert operative', 'Covert Operative can\'t be blocked.').
card_mana_cost('covert operative', ['4', 'U']).
card_cmc('covert operative', 5).
card_layout('covert operative', 'normal').
card_power('covert operative', 3).
card_toughness('covert operative', 2).

% Found in: UDS
card_name('covetous dragon', 'Covetous Dragon').
card_type('covetous dragon', 'Creature — Dragon').
card_types('covetous dragon', ['Creature']).
card_subtypes('covetous dragon', ['Dragon']).
card_colors('covetous dragon', ['R']).
card_text('covetous dragon', 'Flying\nWhen you control no artifacts, sacrifice Covetous Dragon.').
card_mana_cost('covetous dragon', ['4', 'R']).
card_cmc('covetous dragon', 5).
card_layout('covetous dragon', 'normal').
card_power('covetous dragon', 6).
card_toughness('covetous dragon', 5).
card_reserved('covetous dragon').

% Found in: 8ED, 9ED, MMQ
card_name('cowardice', 'Cowardice').
card_type('cowardice', 'Enchantment').
card_types('cowardice', ['Enchantment']).
card_subtypes('cowardice', []).
card_colors('cowardice', ['U']).
card_text('cowardice', 'Whenever a creature becomes the target of a spell or ability, return that creature to its owner\'s hand. (It won\'t be affected by the spell or ability.)').
card_mana_cost('cowardice', ['3', 'U', 'U']).
card_cmc('cowardice', 5).
card_layout('cowardice', 'normal').

% Found in: SOK
card_name('cowed by wisdom', 'Cowed by Wisdom').
card_type('cowed by wisdom', 'Enchantment — Aura').
card_types('cowed by wisdom', ['Enchantment']).
card_subtypes('cowed by wisdom', ['Aura']).
card_colors('cowed by wisdom', ['W']).
card_text('cowed by wisdom', 'Enchant creature\nEnchanted creature can\'t attack or block unless its controller pays {1} for each card in your hand.').
card_mana_cost('cowed by wisdom', ['W']).
card_cmc('cowed by wisdom', 1).
card_layout('cowed by wisdom', 'normal').

% Found in: M13
card_name('cower in fear', 'Cower in Fear').
card_type('cower in fear', 'Instant').
card_types('cower in fear', ['Instant']).
card_subtypes('cower in fear', []).
card_colors('cower in fear', ['B']).
card_text('cower in fear', 'Creatures your opponents control get -1/-1 until end of turn.').
card_mana_cost('cower in fear', ['1', 'B', 'B']).
card_cmc('cower in fear', 3).
card_layout('cower in fear', 'normal').

% Found in: ROE
card_name('crab umbra', 'Crab Umbra').
card_type('crab umbra', 'Enchantment — Aura').
card_types('crab umbra', ['Enchantment']).
card_subtypes('crab umbra', ['Aura']).
card_colors('crab umbra', ['U']).
card_text('crab umbra', 'Enchant creature\n{2}{U}: Untap enchanted creature.\nTotem armor (If enchanted creature would be destroyed, instead remove all damage from it and destroy this Aura.)').
card_mana_cost('crab umbra', ['U']).
card_cmc('crab umbra', 1).
card_layout('crab umbra', 'normal').

% Found in: SHM
card_name('crabapple cohort', 'Crabapple Cohort').
card_type('crabapple cohort', 'Creature — Treefolk Warrior').
card_types('crabapple cohort', ['Creature']).
card_subtypes('crabapple cohort', ['Treefolk', 'Warrior']).
card_colors('crabapple cohort', ['G']).
card_text('crabapple cohort', 'Crabapple Cohort gets +1/+1 as long as you control another green creature.').
card_mana_cost('crabapple cohort', ['4', 'G']).
card_cmc('crabapple cohort', 5).
card_layout('crabapple cohort', 'normal').
card_power('crabapple cohort', 4).
card_toughness('crabapple cohort', 4).

% Found in: BOK
card_name('crack the earth', 'Crack the Earth').
card_type('crack the earth', 'Sorcery — Arcane').
card_types('crack the earth', ['Sorcery']).
card_subtypes('crack the earth', ['Arcane']).
card_colors('crack the earth', ['R']).
card_text('crack the earth', 'Each player sacrifices a permanent.').
card_mana_cost('crack the earth', ['R']).
card_cmc('crack the earth', 1).
card_layout('crack the earth', 'normal').

% Found in: MMQ
card_name('crackdown', 'Crackdown').
card_type('crackdown', 'Enchantment').
card_types('crackdown', ['Enchantment']).
card_subtypes('crackdown', []).
card_colors('crackdown', ['W']).
card_text('crackdown', 'Nonwhite creatures with power 3 or greater don\'t untap during their controllers\' untap steps.').
card_mana_cost('crackdown', ['2', 'W']).
card_cmc('crackdown', 3).
card_layout('crackdown', 'normal').

% Found in: EVE
card_name('crackleburr', 'Crackleburr').
card_type('crackleburr', 'Creature — Elemental').
card_types('crackleburr', ['Creature']).
card_subtypes('crackleburr', ['Elemental']).
card_colors('crackleburr', ['U', 'R']).
card_text('crackleburr', '{U/R}{U/R}, {T}, Tap two untapped red creatures you control: Crackleburr deals 3 damage to target creature or player.\n{U/R}{U/R}, {Q}, Untap two tapped blue creatures you control: Return target creature to its owner\'s hand. ({Q} is the untap symbol.)').
card_mana_cost('crackleburr', ['1', 'U/R', 'U/R']).
card_cmc('crackleburr', 3).
card_layout('crackleburr', 'normal').
card_power('crackleburr', 2).
card_toughness('crackleburr', 2).

% Found in: TOR
card_name('crackling club', 'Crackling Club').
card_type('crackling club', 'Enchantment — Aura').
card_types('crackling club', ['Enchantment']).
card_subtypes('crackling club', ['Aura']).
card_colors('crackling club', ['R']).
card_text('crackling club', 'Enchant creature\nEnchanted creature gets +1/+0.\nSacrifice Crackling Club: Crackling Club deals 1 damage to target creature.').
card_mana_cost('crackling club', ['R']).
card_cmc('crackling club', 1).
card_layout('crackling club', 'normal').

% Found in: KTK, pPRE
card_name('crackling doom', 'Crackling Doom').
card_type('crackling doom', 'Instant').
card_types('crackling doom', ['Instant']).
card_subtypes('crackling doom', []).
card_colors('crackling doom', ['W', 'B', 'R']).
card_text('crackling doom', 'Crackling Doom deals 2 damage to each opponent. Each opponent sacrifices a creature with the greatest power among creatures he or she controls.').
card_mana_cost('crackling doom', ['R', 'W', 'B']).
card_cmc('crackling doom', 3).
card_layout('crackling doom', 'normal').

% Found in: GTC
card_name('crackling perimeter', 'Crackling Perimeter').
card_type('crackling perimeter', 'Enchantment').
card_types('crackling perimeter', ['Enchantment']).
card_subtypes('crackling perimeter', []).
card_colors('crackling perimeter', ['R']).
card_text('crackling perimeter', 'Tap an untapped Gate you control: Crackling Perimeter deals 1 damage to each opponent.').
card_mana_cost('crackling perimeter', ['1', 'R']).
card_cmc('crackling perimeter', 2).
card_layout('crackling perimeter', 'normal').

% Found in: THS
card_name('crackling triton', 'Crackling Triton').
card_type('crackling triton', 'Creature — Merfolk Wizard').
card_types('crackling triton', ['Creature']).
card_subtypes('crackling triton', ['Merfolk', 'Wizard']).
card_colors('crackling triton', ['U']).
card_text('crackling triton', '{2}{R}, Sacrifice Crackling Triton: Crackling Triton deals 2 damage to target creature or player.').
card_mana_cost('crackling triton', ['2', 'U']).
card_cmc('crackling triton', 3).
card_layout('crackling triton', 'normal').
card_power('crackling triton', 2).
card_toughness('crackling triton', 3).

% Found in: USG
card_name('cradle guard', 'Cradle Guard').
card_type('cradle guard', 'Creature — Treefolk').
card_types('cradle guard', ['Creature']).
card_subtypes('cradle guard', ['Treefolk']).
card_colors('cradle guard', ['G']).
card_text('cradle guard', 'Trample\nEcho {1}{G}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('cradle guard', ['1', 'G', 'G']).
card_cmc('cradle guard', 3).
card_layout('cradle guard', 'normal').
card_power('cradle guard', 4).
card_toughness('cradle guard', 4).

% Found in: ALA, C13
card_name('cradle of vitality', 'Cradle of Vitality').
card_type('cradle of vitality', 'Enchantment').
card_types('cradle of vitality', ['Enchantment']).
card_subtypes('cradle of vitality', []).
card_colors('cradle of vitality', ['W']).
card_text('cradle of vitality', 'Whenever you gain life, you may pay {1}{W}. If you do, put a +1/+1 counter on target creature for each 1 life you gained.').
card_mana_cost('cradle of vitality', ['3', 'W']).
card_cmc('cradle of vitality', 4).
card_layout('cradle of vitality', 'normal').

% Found in: PLC
card_name('cradle to grave', 'Cradle to Grave').
card_type('cradle to grave', 'Instant').
card_types('cradle to grave', ['Instant']).
card_subtypes('cradle to grave', []).
card_colors('cradle to grave', ['B']).
card_text('cradle to grave', 'Destroy target nonblack creature that entered the battlefield this turn.').
card_mana_cost('cradle to grave', ['1', 'B']).
card_cmc('cradle to grave', 2).
card_layout('cradle to grave', 'normal').

% Found in: 10E, 9ED, ONS
card_name('crafty pathmage', 'Crafty Pathmage').
card_type('crafty pathmage', 'Creature — Human Wizard').
card_types('crafty pathmage', ['Creature']).
card_subtypes('crafty pathmage', ['Human', 'Wizard']).
card_colors('crafty pathmage', ['U']).
card_text('crafty pathmage', '{T}: Target creature with power 2 or less can\'t be blocked this turn.').
card_mana_cost('crafty pathmage', ['2', 'U']).
card_cmc('crafty pathmage', 3).
card_layout('crafty pathmage', 'normal').
card_power('crafty pathmage', 1).
card_toughness('crafty pathmage', 1).

% Found in: EVE
card_name('crag puca', 'Crag Puca').
card_type('crag puca', 'Creature — Shapeshifter').
card_types('crag puca', ['Creature']).
card_subtypes('crag puca', ['Shapeshifter']).
card_colors('crag puca', ['U', 'R']).
card_text('crag puca', '{U/R}: Switch Crag Puca\'s power and toughness until end of turn.').
card_mana_cost('crag puca', ['U/R', 'U/R', 'U/R']).
card_cmc('crag puca', 3).
card_layout('crag puca', 'normal').
card_power('crag puca', 2).
card_toughness('crag puca', 4).

% Found in: MMQ
card_name('crag saurian', 'Crag Saurian').
card_type('crag saurian', 'Creature — Lizard').
card_types('crag saurian', ['Creature']).
card_subtypes('crag saurian', ['Lizard']).
card_colors('crag saurian', ['R']).
card_text('crag saurian', 'Whenever a source deals damage to Crag Saurian, that source\'s controller gains control of Crag Saurian.').
card_mana_cost('crag saurian', ['R', 'R', 'R']).
card_cmc('crag saurian', 3).
card_layout('crag saurian', 'normal').
card_power('crag saurian', 4).
card_toughness('crag saurian', 4).

% Found in: SHM
card_name('cragganwick cremator', 'Cragganwick Cremator').
card_type('cragganwick cremator', 'Creature — Giant Shaman').
card_types('cragganwick cremator', ['Creature']).
card_subtypes('cragganwick cremator', ['Giant', 'Shaman']).
card_colors('cragganwick cremator', ['R']).
card_text('cragganwick cremator', 'When Cragganwick Cremator enters the battlefield, discard a card at random. If you discard a creature card this way, Cragganwick Cremator deals damage equal to that card\'s power to target player.').
card_mana_cost('cragganwick cremator', ['2', 'R', 'R']).
card_cmc('cragganwick cremator', 4).
card_layout('cragganwick cremator', 'normal').
card_power('cragganwick cremator', 5).
card_toughness('cragganwick cremator', 4).

% Found in: KTK
card_name('cranial archive', 'Cranial Archive').
card_type('cranial archive', 'Artifact').
card_types('cranial archive', ['Artifact']).
card_subtypes('cranial archive', []).
card_colors('cranial archive', []).
card_text('cranial archive', '{2}, Exile Cranial Archive: Target player shuffles his or her graveyard into his or her library. Draw a card.').
card_mana_cost('cranial archive', ['2']).
card_cmc('cranial archive', 2).
card_layout('cranial archive', 'normal').

% Found in: CHK
card_name('cranial extraction', 'Cranial Extraction').
card_type('cranial extraction', 'Sorcery — Arcane').
card_types('cranial extraction', ['Sorcery']).
card_subtypes('cranial extraction', ['Arcane']).
card_colors('cranial extraction', ['B']).
card_text('cranial extraction', 'Name a nonland card. Search target player\'s graveyard, hand, and library for all cards with that name and exile them. Then that player shuffles his or her library.').
card_mana_cost('cranial extraction', ['3', 'B']).
card_cmc('cranial extraction', 4).
card_layout('cranial extraction', 'normal').

% Found in: 5DN, HOP, MM2
card_name('cranial plating', 'Cranial Plating').
card_type('cranial plating', 'Artifact — Equipment').
card_types('cranial plating', ['Artifact']).
card_subtypes('cranial plating', ['Equipment']).
card_colors('cranial plating', []).
card_text('cranial plating', 'Equipped creature gets +1/+0 for each artifact you control.\n{B}{B}: Attach Cranial Plating to target creature you control.\nEquip {1} ({1}: Attach to target creature you control. Equip only as a sorcery.)').
card_mana_cost('cranial plating', ['2']).
card_cmc('cranial plating', 2).
card_layout('cranial plating', 'normal').

% Found in: MMQ
card_name('crash', 'Crash').
card_type('crash', 'Instant').
card_types('crash', ['Instant']).
card_subtypes('crash', []).
card_colors('crash', ['R']).
card_text('crash', 'You may sacrifice a Mountain rather than pay Crash\'s mana cost.\nDestroy target artifact.').
card_mana_cost('crash', ['2', 'R']).
card_cmc('crash', 3).
card_layout('crash', 'normal').

% Found in: GPT
card_name('crash landing', 'Crash Landing').
card_type('crash landing', 'Instant').
card_types('crash landing', ['Instant']).
card_subtypes('crash landing', []).
card_colors('crash landing', ['G']).
card_text('crash landing', 'Target creature with flying loses flying until end of turn. Crash Landing deals damage to that creature equal to the number of Forests you control.').
card_mana_cost('crash landing', ['2', 'G']).
card_cmc('crash landing', 3).
card_layout('crash landing', 'normal').

% Found in: BTD, MIR
card_name('crash of rhinos', 'Crash of Rhinos').
card_type('crash of rhinos', 'Creature — Rhino').
card_types('crash of rhinos', ['Creature']).
card_subtypes('crash of rhinos', ['Rhino']).
card_colors('crash of rhinos', ['G']).
card_text('crash of rhinos', 'Trample').
card_mana_cost('crash of rhinos', ['6', 'G', 'G']).
card_cmc('crash of rhinos', 8).
card_layout('crash of rhinos', 'normal').
card_power('crash of rhinos', 8).
card_toughness('crash of rhinos', 4).

% Found in: BTD, EXO, TPR
card_name('crashing boars', 'Crashing Boars').
card_type('crashing boars', 'Creature — Boar').
card_types('crashing boars', ['Creature']).
card_subtypes('crashing boars', ['Boar']).
card_colors('crashing boars', ['G']).
card_text('crashing boars', 'Whenever Crashing Boars attacks, defending player chooses an untapped creature he or she controls. That creature blocks Crashing Boars this turn if able.').
card_mana_cost('crashing boars', ['3', 'G', 'G']).
card_cmc('crashing boars', 5).
card_layout('crashing boars', 'normal').
card_power('crashing boars', 4).
card_toughness('crashing boars', 4).

% Found in: ODY
card_name('crashing centaur', 'Crashing Centaur').
card_type('crashing centaur', 'Creature — Centaur').
card_types('crashing centaur', ['Creature']).
card_subtypes('crashing centaur', ['Centaur']).
card_colors('crashing centaur', ['G']).
card_text('crashing centaur', '{G}, Discard a card: Crashing Centaur gains trample until end of turn.\nThreshold — As long as seven or more cards are in your graveyard, Crashing Centaur gets +2/+2 and has shroud. (It can\'t be the target of spells or abilities.)').
card_mana_cost('crashing centaur', ['4', 'G', 'G']).
card_cmc('crashing centaur', 6).
card_layout('crashing centaur', 'normal').
card_power('crashing centaur', 3).
card_toughness('crashing centaur', 4).

% Found in: DTK
card_name('crater elemental', 'Crater Elemental').
card_type('crater elemental', 'Creature — Elemental').
card_types('crater elemental', ['Creature']).
card_subtypes('crater elemental', ['Elemental']).
card_colors('crater elemental', ['R']).
card_text('crater elemental', '{R}, {T}, Sacrifice Crater Elemental: Crater Elemental deals 4 damage to target creature.\nFormidable — {2}{R}: Crater Elemental has base power 8 until end of turn. Activate this ability only if creatures you control have total power 8 or greater.').
card_mana_cost('crater elemental', ['2', 'R']).
card_cmc('crater elemental', 3).
card_layout('crater elemental', 'normal').
card_power('crater elemental', 0).
card_toughness('crater elemental', 6).

% Found in: C13, DDL, USG, VMA
card_name('crater hellion', 'Crater Hellion').
card_type('crater hellion', 'Creature — Hellion Beast').
card_types('crater hellion', ['Creature']).
card_subtypes('crater hellion', ['Hellion', 'Beast']).
card_colors('crater hellion', ['R']).
card_text('crater hellion', 'Echo {4}{R}{R} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\nWhen Crater Hellion enters the battlefield, it deals 4 damage to each other creature.').
card_mana_cost('crater hellion', ['4', 'R', 'R']).
card_cmc('crater hellion', 6).
card_layout('crater hellion', 'normal').
card_power('crater hellion', 6).
card_toughness('crater hellion', 6).

% Found in: KTK, pPRE
card_name('crater\'s claws', 'Crater\'s Claws').
card_type('crater\'s claws', 'Sorcery').
card_types('crater\'s claws', ['Sorcery']).
card_subtypes('crater\'s claws', []).
card_colors('crater\'s claws', ['R']).
card_text('crater\'s claws', 'Crater\'s Claws deals X damage to target creature or player.\nFerocious — Crater\'s Claws deals X plus 2 damage to that creature or player instead if you control a creature with power 4 or greater.').
card_mana_cost('crater\'s claws', ['X', 'R']).
card_cmc('crater\'s claws', 1).
card_layout('crater\'s claws', 'normal').

% Found in: AVR
card_name('craterhoof behemoth', 'Craterhoof Behemoth').
card_type('craterhoof behemoth', 'Creature — Beast').
card_types('craterhoof behemoth', ['Creature']).
card_subtypes('craterhoof behemoth', ['Beast']).
card_colors('craterhoof behemoth', ['G']).
card_text('craterhoof behemoth', 'Haste\nWhen Craterhoof Behemoth enters the battlefield, creatures you control gain trample and get +X/+X until end of turn, where X is the number of creatures you control.').
card_mana_cost('craterhoof behemoth', ['5', 'G', 'G', 'G']).
card_cmc('craterhoof behemoth', 8).
card_layout('craterhoof behemoth', 'normal').
card_power('craterhoof behemoth', 5).
card_toughness('craterhoof behemoth', 5).

% Found in: M13
card_name('craterize', 'Craterize').
card_type('craterize', 'Sorcery').
card_types('craterize', ['Sorcery']).
card_subtypes('craterize', []).
card_colors('craterize', ['R']).
card_text('craterize', 'Destroy target land.').
card_mana_cost('craterize', ['3', 'R']).
card_cmc('craterize', 4).
card_layout('craterize', 'normal').

% Found in: POR, STH, TPR
card_name('craven giant', 'Craven Giant').
card_type('craven giant', 'Creature — Giant').
card_types('craven giant', ['Creature']).
card_subtypes('craven giant', ['Giant']).
card_colors('craven giant', ['R']).
card_text('craven giant', 'Craven Giant can\'t block.').
card_mana_cost('craven giant', ['2', 'R']).
card_cmc('craven giant', 3).
card_layout('craven giant', 'normal').
card_power('craven giant', 4).
card_toughness('craven giant', 1).

% Found in: POR
card_name('craven knight', 'Craven Knight').
card_type('craven knight', 'Creature — Human Knight').
card_types('craven knight', ['Creature']).
card_subtypes('craven knight', ['Human', 'Knight']).
card_colors('craven knight', ['B']).
card_text('craven knight', 'Craven Knight can\'t block.').
card_mana_cost('craven knight', ['1', 'B']).
card_cmc('craven knight', 2).
card_layout('craven knight', 'normal').
card_power('craven knight', 2).
card_toughness('craven knight', 2).

% Found in: 5ED, CHR, LEG, TSB
card_name('craw giant', 'Craw Giant').
card_type('craw giant', 'Creature — Giant').
card_types('craw giant', ['Creature']).
card_subtypes('craw giant', ['Giant']).
card_colors('craw giant', ['G']).
card_text('craw giant', 'Trample\nRampage 2 (Whenever this creature becomes blocked, it gets +2/+2 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('craw giant', ['3', 'G', 'G', 'G', 'G']).
card_cmc('craw giant', 7).
card_layout('craw giant', 'normal').
card_power('craw giant', 6).
card_toughness('craw giant', 4).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 8ED, 9ED, CED, CEI, LEA, LEB, M10
card_name('craw wurm', 'Craw Wurm').
card_type('craw wurm', 'Creature — Wurm').
card_types('craw wurm', ['Creature']).
card_subtypes('craw wurm', ['Wurm']).
card_colors('craw wurm', ['G']).
card_text('craw wurm', '').
card_mana_cost('craw wurm', ['4', 'G', 'G']).
card_cmc('craw wurm', 6).
card_layout('craw wurm', 'normal').
card_power('craw wurm', 6).
card_toughness('craw wurm', 4).

% Found in: BOK
card_name('crawling filth', 'Crawling Filth').
card_type('crawling filth', 'Creature — Spirit').
card_types('crawling filth', ['Creature']).
card_subtypes('crawling filth', ['Spirit']).
card_colors('crawling filth', ['B']).
card_text('crawling filth', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nSoulshift 5 (When this creature dies, you may return target Spirit card with converted mana cost 5 or less from your graveyard to your hand.)').
card_mana_cost('crawling filth', ['5', 'B']).
card_cmc('crawling filth', 6).
card_layout('crawling filth', 'normal').
card_power('crawling filth', 2).
card_toughness('crawling filth', 2).

% Found in: C13, ULG
card_name('crawlspace', 'Crawlspace').
card_type('crawlspace', 'Artifact').
card_types('crawlspace', ['Artifact']).
card_subtypes('crawlspace', []).
card_colors('crawlspace', []).
card_text('crawlspace', 'No more than two creatures can attack you each combat.').
card_mana_cost('crawlspace', ['3']).
card_cmc('crawlspace', 3).
card_layout('crawlspace', 'normal').

% Found in: TMP
card_name('crazed armodon', 'Crazed Armodon').
card_type('crazed armodon', 'Creature — Elephant').
card_types('crazed armodon', ['Creature']).
card_subtypes('crazed armodon', ['Elephant']).
card_colors('crazed armodon', ['G']).
card_text('crazed armodon', '{G}: Crazed Armodon gets +3/+0 and gains trample until end of turn. Destroy Crazed Armodon at the beginning of the next end step. Activate this ability only once each turn.').
card_mana_cost('crazed armodon', ['2', 'G', 'G']).
card_cmc('crazed armodon', 4).
card_layout('crazed armodon', 'normal').
card_power('crazed armodon', 3).
card_toughness('crazed armodon', 3).

% Found in: TOR
card_name('crazed firecat', 'Crazed Firecat').
card_type('crazed firecat', 'Creature — Elemental Cat').
card_types('crazed firecat', ['Creature']).
card_subtypes('crazed firecat', ['Elemental', 'Cat']).
card_colors('crazed firecat', ['R']).
card_text('crazed firecat', 'When Crazed Firecat enters the battlefield, flip a coin until you lose a flip. Put a +1/+1 counter on Crazed Firecat for each flip you win.').
card_mana_cost('crazed firecat', ['5', 'R', 'R']).
card_cmc('crazed firecat', 7).
card_layout('crazed firecat', 'normal').
card_power('crazed firecat', 4).
card_toughness('crazed firecat', 4).

% Found in: DST
card_name('crazed goblin', 'Crazed Goblin').
card_type('crazed goblin', 'Creature — Goblin Warrior').
card_types('crazed goblin', ['Creature']).
card_subtypes('crazed goblin', ['Goblin', 'Warrior']).
card_colors('crazed goblin', ['R']).
card_text('crazed goblin', 'Crazed Goblin attacks each turn if able.').
card_mana_cost('crazed goblin', ['R']).
card_cmc('crazed goblin', 1).
card_layout('crazed goblin', 'normal').
card_power('crazed goblin', 1).
card_toughness('crazed goblin', 1).

% Found in: BRB, USG
card_name('crazed skirge', 'Crazed Skirge').
card_type('crazed skirge', 'Creature — Imp').
card_types('crazed skirge', ['Creature']).
card_subtypes('crazed skirge', ['Imp']).
card_colors('crazed skirge', ['B']).
card_text('crazed skirge', 'Flying, haste').
card_mana_cost('crazed skirge', ['3', 'B']).
card_cmc('crazed skirge', 4).
card_layout('crazed skirge', 'normal').
card_power('crazed skirge', 2).
card_toughness('crazed skirge', 2).

% Found in: EVE
card_name('creakwood ghoul', 'Creakwood Ghoul').
card_type('creakwood ghoul', 'Creature — Plant Zombie').
card_types('creakwood ghoul', ['Creature']).
card_subtypes('creakwood ghoul', ['Plant', 'Zombie']).
card_colors('creakwood ghoul', ['B']).
card_text('creakwood ghoul', '{B/G}{B/G}: Exile target card from a graveyard. You gain 1 life.').
card_mana_cost('creakwood ghoul', ['4', 'B']).
card_cmc('creakwood ghoul', 5).
card_layout('creakwood ghoul', 'normal').
card_power('creakwood ghoul', 3).
card_toughness('creakwood ghoul', 3).

% Found in: EVE, MM2
card_name('creakwood liege', 'Creakwood Liege').
card_type('creakwood liege', 'Creature — Horror').
card_types('creakwood liege', ['Creature']).
card_subtypes('creakwood liege', ['Horror']).
card_colors('creakwood liege', ['B', 'G']).
card_text('creakwood liege', 'Other black creatures you control get +1/+1.\nOther green creatures you control get +1/+1.\nAt the beginning of your upkeep, you may put a 1/1 black and green Worm creature token onto the battlefield.').
card_mana_cost('creakwood liege', ['1', 'B/G', 'B/G', 'B/G']).
card_cmc('creakwood liege', 4).
card_layout('creakwood liege', 'normal').
card_power('creakwood liege', 2).
card_toughness('creakwood liege', 2).

% Found in: MOR
card_name('cream of the crop', 'Cream of the Crop').
card_type('cream of the crop', 'Enchantment').
card_types('cream of the crop', ['Enchantment']).
card_subtypes('cream of the crop', []).
card_colors('cream of the crop', ['G']).
card_text('cream of the crop', 'Whenever a creature enters the battlefield under your control, you may look at the top X cards of your library, where X is that creature\'s power. If you do, put one of those cards on top of your library and the rest on the bottom of your library in any order.').
card_mana_cost('cream of the crop', ['1', 'G']).
card_cmc('cream of the crop', 2).
card_layout('cream of the crop', 'normal').

% Found in: 2ED, 3ED, 4ED, CED, CEI, LEA, LEB
card_name('creature bond', 'Creature Bond').
card_type('creature bond', 'Enchantment — Aura').
card_types('creature bond', ['Enchantment']).
card_subtypes('creature bond', ['Aura']).
card_colors('creature bond', ['U']).
card_text('creature bond', 'Enchant creature\nWhen enchanted creature dies, Creature Bond deals damage equal to that creature\'s toughness to the creature\'s controller.').
card_mana_cost('creature bond', ['1', 'U']).
card_cmc('creature bond', 2).
card_layout('creature bond', 'normal').

% Found in: UNH
card_name('creature guy', 'Creature Guy').
card_type('creature guy', 'Creature — Beast').
card_types('creature guy', ['Creature']).
card_subtypes('creature guy', ['Beast']).
card_colors('creature guy', ['G']).
card_text('creature guy', 'Gotcha Whenever an opponent says \"Creature\" or \"Guy,\" you may say \"Gotcha\" If you do, return Creature Guy from your graveyard to your hand.').
card_mana_cost('creature guy', ['3', 'G']).
card_cmc('creature guy', 4).
card_layout('creature guy', 'normal').
card_power('creature guy', 3).
card_toughness('creature guy', 3).

% Found in: MMQ
card_name('credit voucher', 'Credit Voucher').
card_type('credit voucher', 'Artifact').
card_types('credit voucher', ['Artifact']).
card_subtypes('credit voucher', []).
card_colors('credit voucher', []).
card_text('credit voucher', '{2}, {T}, Sacrifice Credit Voucher: Shuffle any number of cards from your hand into your library, then draw that many cards.').
card_mana_cost('credit voucher', ['2']).
card_cmc('credit voucher', 2).
card_layout('credit voucher', 'normal').

% Found in: C14
card_name('creeperhulk', 'Creeperhulk').
card_type('creeperhulk', 'Creature — Plant Elemental').
card_types('creeperhulk', ['Creature']).
card_subtypes('creeperhulk', ['Plant', 'Elemental']).
card_colors('creeperhulk', ['G']).
card_text('creeperhulk', 'Trample\n{1}{G}: Until end of turn, target creature you control has base power and toughness 5/5 and gains trample.').
card_mana_cost('creeperhulk', ['3', 'G', 'G']).
card_cmc('creeperhulk', 5).
card_layout('creeperhulk', 'normal').
card_power('creeperhulk', 5).
card_toughness('creeperhulk', 5).

% Found in: MBS
card_name('creeping corrosion', 'Creeping Corrosion').
card_type('creeping corrosion', 'Sorcery').
card_types('creeping corrosion', ['Sorcery']).
card_subtypes('creeping corrosion', []).
card_colors('creeping corrosion', ['G']).
card_text('creeping corrosion', 'Destroy all artifacts.').
card_mana_cost('creeping corrosion', ['2', 'G', 'G']).
card_cmc('creeping corrosion', 4).
card_layout('creeping corrosion', 'normal').

% Found in: 10E, 6ED, 7ED, 8ED, 9ED, MRD, VIS, pARL
card_name('creeping mold', 'Creeping Mold').
card_type('creeping mold', 'Sorcery').
card_types('creeping mold', ['Sorcery']).
card_subtypes('creeping mold', []).
card_colors('creeping mold', ['G']).
card_text('creeping mold', 'Destroy target artifact, enchantment, or land.').
card_mana_cost('creeping mold', ['2', 'G', 'G']).
card_cmc('creeping mold', 4).
card_layout('creeping mold', 'normal').

% Found in: ISD
card_name('creeping renaissance', 'Creeping Renaissance').
card_type('creeping renaissance', 'Sorcery').
card_types('creeping renaissance', ['Sorcery']).
card_subtypes('creeping renaissance', []).
card_colors('creeping renaissance', ['G']).
card_text('creeping renaissance', 'Choose a permanent type. Return all cards of the chosen type from your graveyard to your hand.\nFlashback {5}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('creeping renaissance', ['3', 'G', 'G']).
card_cmc('creeping renaissance', 5).
card_layout('creeping renaissance', 'normal').

% Found in: WWK
card_name('creeping tar pit', 'Creeping Tar Pit').
card_type('creeping tar pit', 'Land').
card_types('creeping tar pit', ['Land']).
card_subtypes('creeping tar pit', []).
card_colors('creeping tar pit', []).
card_text('creeping tar pit', 'Creeping Tar Pit enters the battlefield tapped.\n{T}: Add {U} or {B} to your mana pool.\n{1}{U}{B}: Creeping Tar Pit becomes a 3/2 blue and black Elemental creature until end of turn and can\'t be blocked this turn. It\'s still a land.').
card_layout('creeping tar pit', 'normal').

% Found in: ISD
card_name('creepy doll', 'Creepy Doll').
card_type('creepy doll', 'Artifact Creature — Construct').
card_types('creepy doll', ['Artifact', 'Creature']).
card_subtypes('creepy doll', ['Construct']).
card_colors('creepy doll', []).
card_text('creepy doll', 'Indestructible\nWhenever Creepy Doll deals combat damage to a creature, flip a coin. If you win the flip, destroy that creature.').
card_mana_cost('creepy doll', ['5']).
card_cmc('creepy doll', 5).
card_layout('creepy doll', 'normal').
card_power('creepy doll', 1).
card_toughness('creepy doll', 1).

% Found in: GPT, INV, RTR
card_name('cremate', 'Cremate').
card_type('cremate', 'Instant').
card_types('cremate', ['Instant']).
card_subtypes('cremate', []).
card_colors('cremate', ['B']).
card_text('cremate', 'Exile target card from a graveyard.\nDraw a card.').
card_mana_cost('cremate', ['B']).
card_cmc('cremate', 1).
card_layout('cremate', 'normal').

% Found in: MMQ
card_name('crenellated wall', 'Crenellated Wall').
card_type('crenellated wall', 'Artifact Creature — Wall').
card_types('crenellated wall', ['Artifact', 'Creature']).
card_subtypes('crenellated wall', ['Wall']).
card_colors('crenellated wall', []).
card_text('crenellated wall', 'Defender (This creature can\'t attack.)\n{T}: Target creature gets +0/+4 until end of turn.').
card_mana_cost('crenellated wall', ['4']).
card_cmc('crenellated wall', 4).
card_layout('crenellated wall', 'normal').
card_power('crenellated wall', 0).
card_toughness('crenellated wall', 4).

% Found in: CMD, VMA
card_name('crescendo of war', 'Crescendo of War').
card_type('crescendo of war', 'Enchantment').
card_types('crescendo of war', ['Enchantment']).
card_subtypes('crescendo of war', []).
card_colors('crescendo of war', ['W']).
card_text('crescendo of war', 'At the beginning of each upkeep, put a strife counter on Crescendo of War.\nAttacking creatures get +1/+0 for each strife counter on Crescendo of War.\nBlocking creatures you control get +1/+0 for each strife counter on Crescendo of War.').
card_mana_cost('crescendo of war', ['3', 'W']).
card_cmc('crescendo of war', 4).
card_layout('crescendo of war', 'normal').

% Found in: LGN
card_name('crested craghorn', 'Crested Craghorn').
card_type('crested craghorn', 'Creature — Goat Beast').
card_types('crested craghorn', ['Creature']).
card_subtypes('crested craghorn', ['Goat', 'Beast']).
card_colors('crested craghorn', ['R']).
card_text('crested craghorn', 'Haste\nProvoke (Whenever this creature attacks, you may have target creature defending player controls untap and block it if able.)').
card_mana_cost('crested craghorn', ['4', 'R']).
card_cmc('crested craghorn', 5).
card_layout('crested craghorn', 'normal').
card_power('crested craghorn', 4).
card_toughness('crested craghorn', 1).

% Found in: LEG
card_name('crevasse', 'Crevasse').
card_type('crevasse', 'Enchantment').
card_types('crevasse', ['Enchantment']).
card_subtypes('crevasse', []).
card_colors('crevasse', ['R']).
card_text('crevasse', 'Creatures with mountainwalk can be blocked as though they didn\'t have mountainwalk.').
card_mana_cost('crevasse', ['2', 'R']).
card_cmc('crevasse', 3).
card_layout('crevasse', 'normal').

% Found in: LRW
card_name('crib swap', 'Crib Swap').
card_type('crib swap', 'Tribal Instant — Shapeshifter').
card_types('crib swap', ['Tribal', 'Instant']).
card_subtypes('crib swap', ['Shapeshifter']).
card_colors('crib swap', ['W']).
card_text('crib swap', 'Changeling (This card is every creature type.)\nExile target creature. Its controller puts a 1/1 colorless Shapeshifter creature token with changeling onto the battlefield.').
card_mana_cost('crib swap', ['2', 'W']).
card_cmc('crib swap', 3).
card_layout('crib swap', 'normal').

% Found in: DIS
card_name('crime', 'Crime').
card_type('crime', 'Sorcery').
card_types('crime', ['Sorcery']).
card_subtypes('crime', []).
card_colors('crime', ['W', 'B']).
card_text('crime', 'Put target creature or enchantment card from an opponent\'s graveyard onto the battlefield under your control.').
card_mana_cost('crime', ['3', 'W', 'B']).
card_cmc('crime', 5).
card_layout('crime', 'split').
card_sides('crime', 'punishment').

% Found in: INV
card_name('crimson acolyte', 'Crimson Acolyte').
card_type('crimson acolyte', 'Creature — Human Cleric').
card_types('crimson acolyte', ['Creature']).
card_subtypes('crimson acolyte', ['Human', 'Cleric']).
card_colors('crimson acolyte', ['W']).
card_text('crimson acolyte', 'Protection from red\n{W}: Target creature gains protection from red until end of turn.').
card_mana_cost('crimson acolyte', ['1', 'W']).
card_cmc('crimson acolyte', 2).
card_layout('crimson acolyte', 'normal').
card_power('crimson acolyte', 1).
card_toughness('crimson acolyte', 1).

% Found in: 6ED, 7ED, MIR
card_name('crimson hellkite', 'Crimson Hellkite').
card_type('crimson hellkite', 'Creature — Dragon').
card_types('crimson hellkite', ['Creature']).
card_subtypes('crimson hellkite', ['Dragon']).
card_colors('crimson hellkite', ['R']).
card_text('crimson hellkite', 'Flying\n{X}, {T}: Crimson Hellkite deals X damage to target creature. Spend only red mana on X.').
card_mana_cost('crimson hellkite', ['6', 'R', 'R', 'R']).
card_cmc('crimson hellkite', 9).
card_layout('crimson hellkite', 'normal').
card_power('crimson hellkite', 6).
card_toughness('crimson hellkite', 6).

% Found in: LEG, ME3
card_name('crimson kobolds', 'Crimson Kobolds').
card_type('crimson kobolds', 'Creature — Kobold').
card_types('crimson kobolds', ['Creature']).
card_subtypes('crimson kobolds', ['Kobold']).
card_colors('crimson kobolds', ['R']).
card_text('crimson kobolds', '').
card_mana_cost('crimson kobolds', ['0']).
card_cmc('crimson kobolds', 0).
card_layout('crimson kobolds', 'normal').
card_power('crimson kobolds', 0).
card_toughness('crimson kobolds', 1).

% Found in: M12
card_name('crimson mage', 'Crimson Mage').
card_type('crimson mage', 'Creature — Human Shaman').
card_types('crimson mage', ['Creature']).
card_subtypes('crimson mage', ['Human', 'Shaman']).
card_colors('crimson mage', ['R']).
card_text('crimson mage', '{R}: Target creature you control gains haste until end of turn. (It can attack and {T} this turn.)').
card_mana_cost('crimson mage', ['1', 'R']).
card_cmc('crimson mage', 2).
card_layout('crimson mage', 'normal').
card_power('crimson mage', 2).
card_toughness('crimson mage', 1).

% Found in: 4ED, 5ED, LEG, ME3
card_name('crimson manticore', 'Crimson Manticore').
card_type('crimson manticore', 'Creature — Manticore').
card_types('crimson manticore', ['Creature']).
card_subtypes('crimson manticore', ['Manticore']).
card_colors('crimson manticore', ['R']).
card_text('crimson manticore', 'Flying\n{R}, {T}: Crimson Manticore deals 1 damage to target attacking or blocking creature.').
card_mana_cost('crimson manticore', ['2', 'R', 'R']).
card_cmc('crimson manticore', 4).
card_layout('crimson manticore', 'normal').
card_power('crimson manticore', 2).
card_toughness('crimson manticore', 2).

% Found in: M13
card_name('crimson muckwader', 'Crimson Muckwader').
card_type('crimson muckwader', 'Creature — Lizard').
card_types('crimson muckwader', ['Creature']).
card_subtypes('crimson muckwader', ['Lizard']).
card_colors('crimson muckwader', ['R']).
card_text('crimson muckwader', 'Crimson Muckwader gets +1/+1 as long as you control a Swamp.\n{2}{B}: Regenerate Crimson Muckwader. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('crimson muckwader', ['1', 'R']).
card_cmc('crimson muckwader', 2).
card_layout('crimson muckwader', 'normal').
card_power('crimson muckwader', 2).
card_toughness('crimson muckwader', 1).

% Found in: MIR
card_name('crimson roc', 'Crimson Roc').
card_type('crimson roc', 'Creature — Bird').
card_types('crimson roc', ['Creature']).
card_subtypes('crimson roc', ['Bird']).
card_colors('crimson roc', ['R']).
card_text('crimson roc', 'Flying\nWhenever Crimson Roc blocks a creature without flying, Crimson Roc gets +1/+0 and gains first strike until end of turn.').
card_mana_cost('crimson roc', ['4', 'R']).
card_cmc('crimson roc', 5).
card_layout('crimson roc', 'normal').
card_power('crimson roc', 2).
card_toughness('crimson roc', 2).

% Found in: SHM
card_name('crimson wisps', 'Crimson Wisps').
card_type('crimson wisps', 'Instant').
card_types('crimson wisps', ['Instant']).
card_subtypes('crimson wisps', []).
card_colors('crimson wisps', ['R']).
card_text('crimson wisps', 'Target creature becomes red and gains haste until end of turn.\nDraw a card.').
card_mana_cost('crimson wisps', ['R']).
card_cmc('crimson wisps', 1).
card_layout('crimson wisps', 'normal').

% Found in: M13, M15
card_name('crippling blight', 'Crippling Blight').
card_type('crippling blight', 'Enchantment — Aura').
card_types('crippling blight', ['Enchantment']).
card_subtypes('crippling blight', ['Aura']).
card_colors('crippling blight', ['B']).
card_text('crippling blight', 'Enchant creature\nEnchanted creature gets -1/-1 and can\'t block.').
card_mana_cost('crippling blight', ['B']).
card_cmc('crippling blight', 1).
card_layout('crippling blight', 'normal').

% Found in: AVR, KTK
card_name('crippling chill', 'Crippling Chill').
card_type('crippling chill', 'Instant').
card_types('crippling chill', ['Instant']).
card_subtypes('crippling chill', []).
card_colors('crippling chill', ['U']).
card_text('crippling chill', 'Tap target creature. It doesn\'t untap during its controller\'s next untap step.\nDraw a card.').
card_mana_cost('crippling chill', ['2', 'U']).
card_cmc('crippling chill', 3).
card_layout('crippling chill', 'normal').

% Found in: TOR
card_name('crippling fatigue', 'Crippling Fatigue').
card_type('crippling fatigue', 'Sorcery').
card_types('crippling fatigue', ['Sorcery']).
card_subtypes('crippling fatigue', []).
card_colors('crippling fatigue', ['B']).
card_text('crippling fatigue', 'Target creature gets -2/-2 until end of turn.\nFlashback—{1}{B}, Pay 3 life. (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('crippling fatigue', ['1', 'B', 'B']).
card_cmc('crippling fatigue', 3).
card_layout('crippling fatigue', 'normal').

% Found in: GTC
card_name('crocanura', 'Crocanura').
card_type('crocanura', 'Creature — Crocodile Frog').
card_types('crocanura', ['Creature']).
card_subtypes('crocanura', ['Crocodile', 'Frog']).
card_colors('crocanura', ['G']).
card_text('crocanura', 'Reach (This creature can block creatures with flying.)\nEvolve (Whenever a creature enters the battlefield under your control, if that creature has greater power or toughness than this creature, put a +1/+1 counter on this creature.)').
card_mana_cost('crocanura', ['2', 'G']).
card_cmc('crocanura', 3).
card_layout('crocanura', 'normal').
card_power('crocanura', 1).
card_toughness('crocanura', 3).

% Found in: APC
card_name('cromat', 'Cromat').
card_type('cromat', 'Legendary Creature — Illusion').
card_types('cromat', ['Creature']).
card_subtypes('cromat', ['Illusion']).
card_supertypes('cromat', ['Legendary']).
card_colors('cromat', ['W', 'U', 'B', 'R', 'G']).
card_text('cromat', '{W}{B}: Destroy target creature blocking or blocked by Cromat.\n{U}{R}: Cromat gains flying until end of turn.\n{B}{G}: Regenerate Cromat.\n{R}{W}: Cromat gets +1/+1 until end of turn.\n{G}{U}: Put Cromat on top of its owner\'s library.').
card_mana_cost('cromat', ['W', 'U', 'B', 'R', 'G']).
card_cmc('cromat', 5).
card_layout('cromat', 'normal').
card_power('cromat', 5).
card_toughness('cromat', 5).

% Found in: LGN
card_name('crookclaw elder', 'Crookclaw Elder').
card_type('crookclaw elder', 'Creature — Bird Wizard').
card_types('crookclaw elder', ['Creature']).
card_subtypes('crookclaw elder', ['Bird', 'Wizard']).
card_colors('crookclaw elder', ['U']).
card_text('crookclaw elder', 'Flying\nTap two untapped Birds you control: Draw a card.\nTap two untapped Wizards you control: Target creature gains flying until end of turn.').
card_mana_cost('crookclaw elder', ['5', 'U']).
card_cmc('crookclaw elder', 6).
card_layout('crookclaw elder', 'normal').
card_power('crookclaw elder', 3).
card_toughness('crookclaw elder', 2).

% Found in: CNS, TSP
card_name('crookclaw transmuter', 'Crookclaw Transmuter').
card_type('crookclaw transmuter', 'Creature — Bird Wizard').
card_types('crookclaw transmuter', ['Creature']).
card_subtypes('crookclaw transmuter', ['Bird', 'Wizard']).
card_colors('crookclaw transmuter', ['U']).
card_text('crookclaw transmuter', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Crookclaw Transmuter enters the battlefield, switch target creature\'s power and toughness until end of turn.').
card_mana_cost('crookclaw transmuter', ['3', 'U']).
card_cmc('crookclaw transmuter', 4).
card_layout('crookclaw transmuter', 'normal').
card_power('crookclaw transmuter', 3).
card_toughness('crookclaw transmuter', 1).

% Found in: MMQ
card_name('crooked scales', 'Crooked Scales').
card_type('crooked scales', 'Artifact').
card_types('crooked scales', ['Artifact']).
card_subtypes('crooked scales', []).
card_colors('crooked scales', []).
card_text('crooked scales', '{4}, {T}: Flip a coin. If you win the flip, destroy target creature an opponent controls. If you lose the flip, destroy target creature you control unless you pay {3} and repeat this process.').
card_mana_cost('crooked scales', ['4']).
card_cmc('crooked scales', 4).
card_layout('crooked scales', 'normal').

% Found in: LEG, MED
card_name('crookshank kobolds', 'Crookshank Kobolds').
card_type('crookshank kobolds', 'Creature — Kobold').
card_types('crookshank kobolds', ['Creature']).
card_subtypes('crookshank kobolds', ['Kobold']).
card_colors('crookshank kobolds', ['R']).
card_text('crookshank kobolds', '').
card_mana_cost('crookshank kobolds', ['0']).
card_cmc('crookshank kobolds', 0).
card_layout('crookshank kobolds', 'normal').
card_power('crookshank kobolds', 0).
card_toughness('crookshank kobolds', 1).

% Found in: ULG
card_name('crop rotation', 'Crop Rotation').
card_type('crop rotation', 'Instant').
card_types('crop rotation', ['Instant']).
card_subtypes('crop rotation', []).
card_colors('crop rotation', ['G']).
card_text('crop rotation', 'As an additional cost to cast Crop Rotation, sacrifice a land.\nSearch your library for a land card and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('crop rotation', ['G']).
card_cmc('crop rotation', 1).
card_layout('crop rotation', 'normal').

% Found in: INV
card_name('crosis\'s attendant', 'Crosis\'s Attendant').
card_type('crosis\'s attendant', 'Artifact Creature — Golem').
card_types('crosis\'s attendant', ['Artifact', 'Creature']).
card_subtypes('crosis\'s attendant', ['Golem']).
card_colors('crosis\'s attendant', []).
card_text('crosis\'s attendant', '{1}, Sacrifice Crosis\'s Attendant: Add {U}{B}{R} to your mana pool.').
card_mana_cost('crosis\'s attendant', ['5']).
card_cmc('crosis\'s attendant', 5).
card_layout('crosis\'s attendant', 'normal').
card_power('crosis\'s attendant', 3).
card_toughness('crosis\'s attendant', 3).

% Found in: PLS
card_name('crosis\'s catacombs', 'Crosis\'s Catacombs').
card_type('crosis\'s catacombs', 'Land — Lair').
card_types('crosis\'s catacombs', ['Land']).
card_subtypes('crosis\'s catacombs', ['Lair']).
card_colors('crosis\'s catacombs', []).
card_text('crosis\'s catacombs', 'When Crosis\'s Catacombs enters the battlefield, sacrifice it unless you return a non-Lair land you control to its owner\'s hand.\n{T}: Add {U}, {B}, or {R} to your mana pool.').
card_layout('crosis\'s catacombs', 'normal').

% Found in: C13, PLS
card_name('crosis\'s charm', 'Crosis\'s Charm').
card_type('crosis\'s charm', 'Instant').
card_types('crosis\'s charm', ['Instant']).
card_subtypes('crosis\'s charm', []).
card_colors('crosis\'s charm', ['U', 'B', 'R']).
card_text('crosis\'s charm', 'Choose one —\n• Return target permanent to its owner\'s hand.\n• Destroy target nonblack creature. It can\'t be regenerated.\n• Destroy target artifact.').
card_mana_cost('crosis\'s charm', ['U', 'B', 'R']).
card_cmc('crosis\'s charm', 3).
card_layout('crosis\'s charm', 'normal').

% Found in: INV, PD3
card_name('crosis, the purger', 'Crosis, the Purger').
card_type('crosis, the purger', 'Legendary Creature — Dragon').
card_types('crosis, the purger', ['Creature']).
card_subtypes('crosis, the purger', ['Dragon']).
card_supertypes('crosis, the purger', ['Legendary']).
card_colors('crosis, the purger', ['U', 'B', 'R']).
card_text('crosis, the purger', 'Flying\nWhenever Crosis, the Purger deals combat damage to a player, you may pay {2}{B}. If you do, choose a color, then that player reveals his or her hand and discards all cards of that color.').
card_mana_cost('crosis, the purger', ['3', 'U', 'B', 'R']).
card_cmc('crosis, the purger', 6).
card_layout('crosis, the purger', 'normal').
card_power('crosis, the purger', 6).
card_toughness('crosis, the purger', 6).

% Found in: STH
card_name('crossbow ambush', 'Crossbow Ambush').
card_type('crossbow ambush', 'Instant').
card_types('crossbow ambush', ['Instant']).
card_subtypes('crossbow ambush', []).
card_colors('crossbow ambush', ['G']).
card_text('crossbow ambush', 'Creatures you control gain reach until end of turn. (They can block creatures with flying.)').
card_mana_cost('crossbow ambush', ['G']).
card_cmc('crossbow ambush', 1).
card_layout('crossbow ambush', 'normal').

% Found in: 7ED, 8ED, 9ED, MMQ
card_name('crossbow infantry', 'Crossbow Infantry').
card_type('crossbow infantry', 'Creature — Human Soldier Archer').
card_types('crossbow infantry', ['Creature']).
card_subtypes('crossbow infantry', ['Human', 'Soldier', 'Archer']).
card_colors('crossbow infantry', ['W']).
card_text('crossbow infantry', '{T}: Crossbow Infantry deals 1 damage to target attacking or blocking creature.').
card_mana_cost('crossbow infantry', ['1', 'W']).
card_cmc('crossbow infantry', 2).
card_layout('crossbow infantry', 'normal').
card_power('crossbow infantry', 1).
card_toughness('crossbow infantry', 1).

% Found in: DDM, RTR
card_name('crosstown courier', 'Crosstown Courier').
card_type('crosstown courier', 'Creature — Vedalken').
card_types('crosstown courier', ['Creature']).
card_subtypes('crosstown courier', ['Vedalken']).
card_colors('crosstown courier', ['U']).
card_text('crosstown courier', 'Whenever Crosstown Courier deals combat damage to a player, that player puts that many cards from the top of his or her library into his or her graveyard.').
card_mana_cost('crosstown courier', ['1', 'U']).
card_cmc('crosstown courier', 2).
card_layout('crosstown courier', 'normal').
card_power('crosstown courier', 2).
card_toughness('crosstown courier', 1).

% Found in: ISD
card_name('crossway vampire', 'Crossway Vampire').
card_type('crossway vampire', 'Creature — Vampire').
card_types('crossway vampire', ['Creature']).
card_subtypes('crossway vampire', ['Vampire']).
card_colors('crossway vampire', ['R']).
card_text('crossway vampire', 'When Crossway Vampire enters the battlefield, target creature can\'t block this turn.').
card_mana_cost('crossway vampire', ['1', 'R', 'R']).
card_cmc('crossway vampire', 3).
card_layout('crossway vampire', 'normal').
card_power('crossway vampire', 3).
card_toughness('crossway vampire', 2).

% Found in: USG
card_name('crosswinds', 'Crosswinds').
card_type('crosswinds', 'Enchantment').
card_types('crosswinds', ['Enchantment']).
card_subtypes('crosswinds', []).
card_colors('crosswinds', ['G']).
card_text('crosswinds', 'Creatures with flying get -2/-0.').
card_mana_cost('crosswinds', ['1', 'G']).
card_cmc('crosswinds', 2).
card_layout('crosswinds', 'normal').

% Found in: VAN
card_name('crovax', 'Crovax').
card_type('crovax', 'Vanguard').
card_types('crovax', ['Vanguard']).
card_subtypes('crovax', []).
card_colors('crovax', []).
card_text('crovax', 'Whenever a creature you control deals damage to a creature or player, you gain 1 life.').
card_layout('crovax', 'vanguard').

% Found in: STH, TPR, VMA
card_name('crovax the cursed', 'Crovax the Cursed').
card_type('crovax the cursed', 'Legendary Creature — Vampire').
card_types('crovax the cursed', ['Creature']).
card_subtypes('crovax the cursed', ['Vampire']).
card_supertypes('crovax the cursed', ['Legendary']).
card_colors('crovax the cursed', ['B']).
card_text('crovax the cursed', 'Crovax the Cursed enters the battlefield with four +1/+1 counters on it.\nAt the beginning of your upkeep, you may sacrifice a creature. If you do, put a +1/+1 counter on Crovax. If you don\'t, remove a +1/+1 counter from Crovax.\n{B}: Crovax gains flying until end of turn.').
card_mana_cost('crovax the cursed', ['2', 'B', 'B']).
card_cmc('crovax the cursed', 4).
card_layout('crovax the cursed', 'normal').
card_power('crovax the cursed', 0).
card_toughness('crovax the cursed', 0).
card_reserved('crovax the cursed').

% Found in: PLC
card_name('crovax, ascendant hero', 'Crovax, Ascendant Hero').
card_type('crovax, ascendant hero', 'Legendary Creature — Human').
card_types('crovax, ascendant hero', ['Creature']).
card_subtypes('crovax, ascendant hero', ['Human']).
card_supertypes('crovax, ascendant hero', ['Legendary']).
card_colors('crovax, ascendant hero', ['W']).
card_text('crovax, ascendant hero', 'Other white creatures get +1/+1.\nNonwhite creatures get -1/-1.\nPay 2 life: Return Crovax, Ascendant Hero to its owner\'s hand.').
card_mana_cost('crovax, ascendant hero', ['4', 'W', 'W']).
card_cmc('crovax, ascendant hero', 6).
card_layout('crovax, ascendant hero', 'normal').
card_power('crovax, ascendant hero', 4).
card_toughness('crovax, ascendant hero', 4).

% Found in: ONS
card_name('crowd favorites', 'Crowd Favorites').
card_type('crowd favorites', 'Creature — Human Soldier').
card_types('crowd favorites', ['Creature']).
card_subtypes('crowd favorites', ['Human', 'Soldier']).
card_colors('crowd favorites', ['W']).
card_text('crowd favorites', '{3}{W}: Tap target creature.\n{3}{W}: Crowd Favorites gets +0/+5 until end of turn.').
card_mana_cost('crowd favorites', ['6', 'W']).
card_cmc('crowd favorites', 7).
card_layout('crowd favorites', 'normal').
card_power('crowd favorites', 4).
card_toughness('crowd favorites', 4).

% Found in: DPA, SHM
card_name('crowd of cinders', 'Crowd of Cinders').
card_type('crowd of cinders', 'Creature — Elemental').
card_types('crowd of cinders', ['Creature']).
card_subtypes('crowd of cinders', ['Elemental']).
card_colors('crowd of cinders', ['B']).
card_text('crowd of cinders', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\nCrowd of Cinders\'s power and toughness are each equal to the number of black permanents you control.').
card_mana_cost('crowd of cinders', ['3', 'B']).
card_cmc('crowd of cinders', 4).
card_layout('crowd of cinders', 'normal').
card_power('crowd of cinders', '*').
card_toughness('crowd of cinders', '*').

% Found in: M15
card_name('crowd\'s favor', 'Crowd\'s Favor').
card_type('crowd\'s favor', 'Instant').
card_types('crowd\'s favor', ['Instant']).
card_subtypes('crowd\'s favor', []).
card_colors('crowd\'s favor', ['R']).
card_text('crowd\'s favor', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nTarget creature gets +1/+0 and gains first strike until end of turn. (It deals combat damage before creatures without first strike.)').
card_mana_cost('crowd\'s favor', ['R']).
card_cmc('crowd\'s favor', 1).
card_layout('crowd\'s favor', 'normal').

% Found in: ONS
card_name('crown of ascension', 'Crown of Ascension').
card_type('crown of ascension', 'Enchantment — Aura').
card_types('crown of ascension', ['Enchantment']).
card_subtypes('crown of ascension', ['Aura']).
card_colors('crown of ascension', ['U']).
card_text('crown of ascension', 'Enchant creature\nEnchanted creature has flying.\nSacrifice Crown of Ascension: Enchanted creature and other creatures that share a creature type with it gain flying until end of turn.').
card_mana_cost('crown of ascension', ['1', 'U']).
card_cmc('crown of ascension', 2).
card_layout('crown of ascension', 'normal').

% Found in: ONS
card_name('crown of awe', 'Crown of Awe').
card_type('crown of awe', 'Enchantment — Aura').
card_types('crown of awe', ['Enchantment']).
card_subtypes('crown of awe', ['Aura']).
card_colors('crown of awe', ['W']).
card_text('crown of awe', 'Enchant creature\nEnchanted creature has protection from black and from red.\nSacrifice Crown of Awe: Enchanted creature and other creatures that share a creature type with it gain protection from black and from red until end of turn.').
card_mana_cost('crown of awe', ['1', 'W']).
card_cmc('crown of awe', 2).
card_layout('crown of awe', 'normal').

% Found in: RAV
card_name('crown of convergence', 'Crown of Convergence').
card_type('crown of convergence', 'Artifact').
card_types('crown of convergence', ['Artifact']).
card_subtypes('crown of convergence', []).
card_colors('crown of convergence', []).
card_text('crown of convergence', 'Play with the top card of your library revealed.\nAs long as the top card of your library is a creature card, creatures you control that share a color with that card get +1/+1.\n{G}{W}: Put the top card of your library on the bottom of your library.').
card_mana_cost('crown of convergence', ['2']).
card_cmc('crown of convergence', 2).
card_layout('crown of convergence', 'normal').

% Found in: C14
card_name('crown of doom', 'Crown of Doom').
card_type('crown of doom', 'Artifact').
card_types('crown of doom', ['Artifact']).
card_subtypes('crown of doom', []).
card_colors('crown of doom', []).
card_text('crown of doom', 'Whenever a creature attacks you or a planeswalker you control, it gets +2/+0 until end of turn.\n{2}: Target player other than Crown of Doom\'s owner gains control of it. Activate this ability only during your turn.').
card_mana_cost('crown of doom', ['3']).
card_cmc('crown of doom', 3).
card_layout('crown of doom', 'normal').

% Found in: M12
card_name('crown of empires', 'Crown of Empires').
card_type('crown of empires', 'Artifact').
card_types('crown of empires', ['Artifact']).
card_subtypes('crown of empires', []).
card_colors('crown of empires', []).
card_text('crown of empires', '{3}, {T}: Tap target creature. Gain control of that creature instead if you control artifacts named Scepter of Empires and Throne of Empires.').
card_mana_cost('crown of empires', ['2']).
card_cmc('crown of empires', 2).
card_layout('crown of empires', 'normal').

% Found in: INV, TMP
card_name('crown of flames', 'Crown of Flames').
card_type('crown of flames', 'Enchantment — Aura').
card_types('crown of flames', ['Enchantment']).
card_subtypes('crown of flames', ['Aura']).
card_colors('crown of flames', ['R']).
card_text('crown of flames', 'Enchant creature\n{R}: Enchanted creature gets +1/+0 until end of turn.\n{R}: Return Crown of Flames to its owner\'s hand.').
card_mana_cost('crown of flames', ['R']).
card_cmc('crown of flames', 1).
card_layout('crown of flames', 'normal').

% Found in: ONS
card_name('crown of fury', 'Crown of Fury').
card_type('crown of fury', 'Enchantment — Aura').
card_types('crown of fury', ['Enchantment']).
card_subtypes('crown of fury', ['Aura']).
card_colors('crown of fury', ['R']).
card_text('crown of fury', 'Enchant creature\nEnchanted creature gets +1/+0 and has first strike.\nSacrifice Crown of Fury: Enchanted creature and other creatures that share a creature type with it get +1/+0 and gain first strike until end of turn.').
card_mana_cost('crown of fury', ['1', 'R']).
card_cmc('crown of fury', 2).
card_layout('crown of fury', 'normal').

% Found in: ONS
card_name('crown of suspicion', 'Crown of Suspicion').
card_type('crown of suspicion', 'Enchantment — Aura').
card_types('crown of suspicion', ['Enchantment']).
card_subtypes('crown of suspicion', ['Aura']).
card_colors('crown of suspicion', ['B']).
card_text('crown of suspicion', 'Enchant creature\nEnchanted creature gets +2/-1.\nSacrifice Crown of Suspicion: Enchanted creature and other creatures that share a creature type with it get +2/-1 until end of turn.').
card_mana_cost('crown of suspicion', ['1', 'B']).
card_cmc('crown of suspicion', 2).
card_layout('crown of suspicion', 'normal').

% Found in: 5ED, ICE
card_name('crown of the ages', 'Crown of the Ages').
card_type('crown of the ages', 'Artifact').
card_types('crown of the ages', ['Artifact']).
card_subtypes('crown of the ages', []).
card_colors('crown of the ages', []).
card_text('crown of the ages', '{4}, {T}: Attach target Aura attached to a creature to another creature.').
card_mana_cost('crown of the ages', ['2']).
card_cmc('crown of the ages', 2).
card_layout('crown of the ages', 'normal').

% Found in: ONS
card_name('crown of vigor', 'Crown of Vigor').
card_type('crown of vigor', 'Enchantment — Aura').
card_types('crown of vigor', ['Enchantment']).
card_subtypes('crown of vigor', ['Aura']).
card_colors('crown of vigor', ['G']).
card_text('crown of vigor', 'Enchant creature\nEnchanted creature gets +1/+1.\nSacrifice Crown of Vigor: Enchanted creature and other creatures that share a creature type with it get +1/+1 until end of turn.').
card_mana_cost('crown of vigor', ['1', 'G']).
card_cmc('crown of vigor', 2).
card_layout('crown of vigor', 'normal').

% Found in: DDL, GTC
card_name('crowned ceratok', 'Crowned Ceratok').
card_type('crowned ceratok', 'Creature — Rhino').
card_types('crowned ceratok', ['Creature']).
card_subtypes('crowned ceratok', ['Rhino']).
card_colors('crowned ceratok', ['G']).
card_text('crowned ceratok', 'Trample\nEach creature you control with a +1/+1 counter on it has trample.').
card_mana_cost('crowned ceratok', ['3', 'G']).
card_cmc('crowned ceratok', 4).
card_layout('crowned ceratok', 'normal').
card_power('crowned ceratok', 4).
card_toughness('crowned ceratok', 3).

% Found in: ALA, M15
card_name('crucible of fire', 'Crucible of Fire').
card_type('crucible of fire', 'Enchantment').
card_types('crucible of fire', ['Enchantment']).
card_subtypes('crucible of fire', []).
card_colors('crucible of fire', ['R']).
card_text('crucible of fire', 'Dragon creatures you control get +3/+3.').
card_mana_cost('crucible of fire', ['3', 'R']).
card_cmc('crucible of fire', 4).
card_layout('crucible of fire', 'normal').

% Found in: FRF
card_name('crucible of the spirit dragon', 'Crucible of the Spirit Dragon').
card_type('crucible of the spirit dragon', 'Land').
card_types('crucible of the spirit dragon', ['Land']).
card_subtypes('crucible of the spirit dragon', []).
card_colors('crucible of the spirit dragon', []).
card_text('crucible of the spirit dragon', '{T}: Add {1} to your mana pool.\n{1}, {T}: Put a storage counter on Crucible of the Spirit Dragon.\n{T}, Remove X storage counters from Crucible of the Spirit Dragon: Add X mana in any combination of colors to your mana pool. Spend this mana only to cast Dragon spells or activate abilities of Dragons.').
card_layout('crucible of the spirit dragon', 'normal').

% Found in: 10E, 5DN, pJGP
card_name('crucible of worlds', 'Crucible of Worlds').
card_type('crucible of worlds', 'Artifact').
card_types('crucible of worlds', ['Artifact']).
card_subtypes('crucible of worlds', []).
card_colors('crucible of worlds', []).
card_text('crucible of worlds', 'You may play land cards from your graveyard.').
card_mana_cost('crucible of worlds', ['3']).
card_cmc('crucible of worlds', 3).
card_layout('crucible of worlds', 'normal').

% Found in: ONS
card_name('crude rampart', 'Crude Rampart').
card_type('crude rampart', 'Creature — Wall').
card_types('crude rampart', ['Creature']).
card_subtypes('crude rampart', ['Wall']).
card_colors('crude rampart', ['W']).
card_text('crude rampart', 'Defender (This creature can\'t attack.)\nMorph {4}{W} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('crude rampart', ['3', 'W']).
card_cmc('crude rampart', 4).
card_layout('crude rampart', 'normal').
card_power('crude rampart', 4).
card_toughness('crude rampart', 5).

% Found in: POR, VMA
card_name('cruel bargain', 'Cruel Bargain').
card_type('cruel bargain', 'Sorcery').
card_types('cruel bargain', ['Sorcery']).
card_subtypes('cruel bargain', []).
card_colors('cruel bargain', ['B']).
card_text('cruel bargain', 'Draw four cards. You lose half your life, rounded up.').
card_mana_cost('cruel bargain', ['B', 'B', 'B']).
card_cmc('cruel bargain', 3).
card_layout('cruel bargain', 'normal').

% Found in: CHK
card_name('cruel deceiver', 'Cruel Deceiver').
card_type('cruel deceiver', 'Creature — Spirit').
card_types('cruel deceiver', ['Creature']).
card_subtypes('cruel deceiver', ['Spirit']).
card_colors('cruel deceiver', ['B']).
card_text('cruel deceiver', '{1}: Look at the top card of your library.\n{2}: Reveal the top card of your library. If it\'s a land card, Cruel Deceiver gains \"Whenever Cruel Deceiver deals damage to a creature, destroy that creature\" until end of turn. Activate this ability only once each turn.').
card_mana_cost('cruel deceiver', ['1', 'B']).
card_cmc('cruel deceiver', 2).
card_layout('cruel deceiver', 'normal').
card_power('cruel deceiver', 2).
card_toughness('cruel deceiver', 1).

% Found in: 10E, 9ED, DD3_DVD, DDC, PO2, pMPR
card_name('cruel edict', 'Cruel Edict').
card_type('cruel edict', 'Sorcery').
card_types('cruel edict', ['Sorcery']).
card_subtypes('cruel edict', []).
card_colors('cruel edict', ['B']).
card_text('cruel edict', 'Target opponent sacrifices a creature.').
card_mana_cost('cruel edict', ['1', 'B']).
card_cmc('cruel edict', 2).
card_layout('cruel edict', 'normal').

% Found in: POR
card_name('cruel fate', 'Cruel Fate').
card_type('cruel fate', 'Sorcery').
card_types('cruel fate', ['Sorcery']).
card_subtypes('cruel fate', []).
card_colors('cruel fate', ['U']).
card_text('cruel fate', 'Look at the top five cards of target opponent\'s library. Put one of those cards into that player\'s graveyard and the rest on top of his or her library in any order.').
card_mana_cost('cruel fate', ['4', 'U']).
card_cmc('cruel fate', 5).
card_layout('cruel fate', 'normal').

% Found in: JOU
card_name('cruel feeding', 'Cruel Feeding').
card_type('cruel feeding', 'Instant').
card_types('cruel feeding', ['Instant']).
card_subtypes('cruel feeding', []).
card_colors('cruel feeding', ['B']).
card_text('cruel feeding', 'Strive — Cruel Feeding costs {2}{B} more to cast for each target beyond the first.\nAny number of target creatures each get +1/+0 and gain lifelink until end of turn. (Damage dealt by a creature with lifelink also causes its controller to gain that much life.)').
card_mana_cost('cruel feeding', ['B']).
card_cmc('cruel feeding', 1).
card_layout('cruel feeding', 'normal').

% Found in: HOP, ONS, ORI
card_name('cruel revival', 'Cruel Revival').
card_type('cruel revival', 'Instant').
card_types('cruel revival', ['Instant']).
card_subtypes('cruel revival', []).
card_colors('cruel revival', ['B']).
card_text('cruel revival', 'Destroy target non-Zombie creature. It can\'t be regenerated. Return up to one target Zombie card from your graveyard to your hand.').
card_mana_cost('cruel revival', ['4', 'B']).
card_cmc('cruel revival', 5).
card_layout('cruel revival', 'normal').

% Found in: M15
card_name('cruel sadist', 'Cruel Sadist').
card_type('cruel sadist', 'Creature — Human Assassin').
card_types('cruel sadist', ['Creature']).
card_subtypes('cruel sadist', ['Human', 'Assassin']).
card_colors('cruel sadist', ['B']).
card_text('cruel sadist', '{B}, {T}, Pay 1 life: Put a +1/+1 counter on Cruel Sadist.\n{2}{B}, {T}, Remove X +1/+1 counters from Cruel Sadist: Cruel Sadist deals X damage to target creature.').
card_mana_cost('cruel sadist', ['B']).
card_cmc('cruel sadist', 1).
card_layout('cruel sadist', 'normal').
card_power('cruel sadist', 1).
card_toughness('cruel sadist', 1).

% Found in: POR
card_name('cruel tutor', 'Cruel Tutor').
card_type('cruel tutor', 'Sorcery').
card_types('cruel tutor', ['Sorcery']).
card_subtypes('cruel tutor', []).
card_colors('cruel tutor', ['B']).
card_text('cruel tutor', 'Search your library for a card, then shuffle your library and put that card on top of it. You lose 2 life.').
card_mana_cost('cruel tutor', ['2', 'B']).
card_cmc('cruel tutor', 3).
card_layout('cruel tutor', 'normal').

% Found in: ALA, C13, DDH, V13
card_name('cruel ultimatum', 'Cruel Ultimatum').
card_type('cruel ultimatum', 'Sorcery').
card_types('cruel ultimatum', ['Sorcery']).
card_subtypes('cruel ultimatum', []).
card_colors('cruel ultimatum', ['U', 'B', 'R']).
card_text('cruel ultimatum', 'Target opponent sacrifices a creature, discards three cards, then loses 5 life. You return a creature card from your graveyard to your hand, draw three cards, then gain 5 life.').
card_mana_cost('cruel ultimatum', ['U', 'U', 'B', 'B', 'B', 'R', 'R']).
card_cmc('cruel ultimatum', 7).
card_layout('cruel ultimatum', 'normal').

% Found in: 3ED, 4ED, 5ED, ATQ, ME4
card_name('crumble', 'Crumble').
card_type('crumble', 'Instant').
card_types('crumble', ['Instant']).
card_subtypes('crumble', []).
card_colors('crumble', ['G']).
card_text('crumble', 'Destroy target artifact. It can\'t be regenerated. That artifact\'s controller gains life equal to its converted mana cost.').
card_mana_cost('crumble', ['G']).
card_cmc('crumble', 1).
card_layout('crumble', 'normal').

% Found in: BFZ
card_name('crumble to dust', 'Crumble to Dust').
card_type('crumble to dust', 'Sorcery').
card_types('crumble to dust', ['Sorcery']).
card_subtypes('crumble to dust', []).
card_colors('crumble to dust', []).
card_text('crumble to dust', 'Devoid (This card has no color.)\nExile target nonbasic land. Search its controller\'s graveyard, hand, and library for any number of cards with the same name as that land and exile them. Then that player shuffles his or her library.').
card_mana_cost('crumble to dust', ['3', 'R']).
card_cmc('crumble to dust', 4).
card_layout('crumble to dust', 'normal').

% Found in: EVE
card_name('crumbling ashes', 'Crumbling Ashes').
card_type('crumbling ashes', 'Enchantment').
card_types('crumbling ashes', ['Enchantment']).
card_subtypes('crumbling ashes', []).
card_colors('crumbling ashes', ['B']).
card_text('crumbling ashes', 'At the beginning of your upkeep, destroy target creature with a -1/-1 counter on it.').
card_mana_cost('crumbling ashes', ['1', 'B']).
card_cmc('crumbling ashes', 2).
card_layout('crumbling ashes', 'normal').

% Found in: M12
card_name('crumbling colossus', 'Crumbling Colossus').
card_type('crumbling colossus', 'Artifact Creature — Golem').
card_types('crumbling colossus', ['Artifact', 'Creature']).
card_subtypes('crumbling colossus', ['Golem']).
card_colors('crumbling colossus', []).
card_text('crumbling colossus', 'Trample (If this creature would assign enough damage to its blockers to destroy them, you may have it assign the rest of its damage to defending player or planeswalker.)\nWhen Crumbling Colossus attacks, sacrifice it at end of combat.').
card_mana_cost('crumbling colossus', ['5']).
card_cmc('crumbling colossus', 5).
card_layout('crumbling colossus', 'normal').
card_power('crumbling colossus', 7).
card_toughness('crumbling colossus', 4).

% Found in: ALA, C13, DDH
card_name('crumbling necropolis', 'Crumbling Necropolis').
card_type('crumbling necropolis', 'Land').
card_types('crumbling necropolis', ['Land']).
card_subtypes('crumbling necropolis', []).
card_colors('crumbling necropolis', []).
card_text('crumbling necropolis', 'Crumbling Necropolis enters the battlefield tapped.\n{T}: Add {U}, {B}, or {R} to your mana pool.').
card_layout('crumbling necropolis', 'normal').

% Found in: MMQ
card_name('crumbling sanctuary', 'Crumbling Sanctuary').
card_type('crumbling sanctuary', 'Artifact').
card_types('crumbling sanctuary', ['Artifact']).
card_subtypes('crumbling sanctuary', []).
card_colors('crumbling sanctuary', []).
card_text('crumbling sanctuary', 'If damage would be dealt to a player, that player exiles that many cards from the top of his or her library instead.').
card_mana_cost('crumbling sanctuary', ['5']).
card_cmc('crumbling sanctuary', 5).
card_layout('crumbling sanctuary', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, CED, CEI, DDF, LEA, LEB, MED, pSUS
card_name('crusade', 'Crusade').
card_type('crusade', 'Enchantment').
card_types('crusade', ['Enchantment']).
card_subtypes('crusade', []).
card_colors('crusade', ['W']).
card_text('crusade', 'White creatures get +1/+1.').
card_mana_cost('crusade', ['W', 'W']).
card_cmc('crusade', 2).
card_layout('crusade', 'normal').

% Found in: M13
card_name('crusader of odric', 'Crusader of Odric').
card_type('crusader of odric', 'Creature — Human Soldier').
card_types('crusader of odric', ['Creature']).
card_subtypes('crusader of odric', ['Human', 'Soldier']).
card_colors('crusader of odric', ['W']).
card_text('crusader of odric', 'Crusader of Odric\'s power and toughness are each equal to the number of creatures you control.').
card_mana_cost('crusader of odric', ['2', 'W']).
card_cmc('crusader of odric', 3).
card_layout('crusader of odric', 'normal').
card_power('crusader of odric', '*').
card_toughness('crusader of odric', '*').

% Found in: INV
card_name('crusading knight', 'Crusading Knight').
card_type('crusading knight', 'Creature — Human Knight').
card_types('crusading knight', ['Creature']).
card_subtypes('crusading knight', ['Human', 'Knight']).
card_colors('crusading knight', ['W']).
card_text('crusading knight', 'Protection from black\nCrusading Knight gets +1/+1 for each Swamp your opponents control.').
card_mana_cost('crusading knight', ['2', 'W', 'W']).
card_cmc('crusading knight', 4).
card_layout('crusading knight', 'normal').
card_power('crusading knight', 2).
card_toughness('crusading knight', 2).

% Found in: MBS
card_name('crush', 'Crush').
card_type('crush', 'Instant').
card_types('crush', ['Instant']).
card_subtypes('crush', []).
card_colors('crush', ['R']).
card_text('crush', 'Destroy target noncreature artifact.').
card_mana_cost('crush', ['R']).
card_cmc('crush', 1).
card_layout('crush', 'normal').

% Found in: JUD
card_name('crush of wurms', 'Crush of Wurms').
card_type('crush of wurms', 'Sorcery').
card_types('crush of wurms', ['Sorcery']).
card_subtypes('crush of wurms', []).
card_colors('crush of wurms', ['G']).
card_text('crush of wurms', 'Put three 6/6 green Wurm creature tokens onto the battlefield.\nFlashback {9}{G}{G}{G} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('crush of wurms', ['6', 'G', 'G', 'G']).
card_cmc('crush of wurms', 9).
card_layout('crush of wurms', 'normal').

% Found in: LRW, MMA
card_name('crush underfoot', 'Crush Underfoot').
card_type('crush underfoot', 'Tribal Instant — Giant').
card_types('crush underfoot', ['Tribal', 'Instant']).
card_subtypes('crush underfoot', ['Giant']).
card_colors('crush underfoot', ['R']).
card_text('crush underfoot', 'Choose a Giant creature you control. It deals damage equal to its power to target creature.').
card_mana_cost('crush underfoot', ['1', 'R']).
card_cmc('crush underfoot', 2).
card_layout('crush underfoot', 'normal').

% Found in: WWK
card_name('crusher zendikon', 'Crusher Zendikon').
card_type('crusher zendikon', 'Enchantment — Aura').
card_types('crusher zendikon', ['Enchantment']).
card_subtypes('crusher zendikon', ['Aura']).
card_colors('crusher zendikon', ['R']).
card_text('crusher zendikon', 'Enchant land\nEnchanted land is a 4/2 red Beast creature with trample. It\'s still a land.\nWhen enchanted land dies, return that card to its owner\'s hand.').
card_mana_cost('crusher zendikon', ['2', 'R']).
card_cmc('crusher zendikon', 3).
card_layout('crusher zendikon', 'normal').

% Found in: CHK
card_name('crushing pain', 'Crushing Pain').
card_type('crushing pain', 'Instant — Arcane').
card_types('crushing pain', ['Instant']).
card_subtypes('crushing pain', ['Arcane']).
card_colors('crushing pain', ['R']).
card_text('crushing pain', 'Crushing Pain deals 6 damage to target creature that was dealt damage this turn.').
card_mana_cost('crushing pain', ['1', 'R']).
card_cmc('crushing pain', 2).
card_layout('crushing pain', 'normal').

% Found in: DKA
card_name('crushing vines', 'Crushing Vines').
card_type('crushing vines', 'Instant').
card_types('crushing vines', ['Instant']).
card_subtypes('crushing vines', []).
card_colors('crushing vines', ['G']).
card_text('crushing vines', 'Choose one —\n• Destroy target creature with flying.\n• Destroy target artifact.').
card_mana_cost('crushing vines', ['2', 'G']).
card_cmc('crushing vines', 3).
card_layout('crushing vines', 'normal').

% Found in: FRF
card_name('crux of fate', 'Crux of Fate').
card_type('crux of fate', 'Sorcery').
card_types('crux of fate', ['Sorcery']).
card_subtypes('crux of fate', []).
card_colors('crux of fate', ['B']).
card_text('crux of fate', 'Choose one —\n• Destroy all Dragon creatures.\n• Destroy all non-Dragon creatures.').
card_mana_cost('crux of fate', ['3', 'B', 'B']).
card_cmc('crux of fate', 5).
card_layout('crux of fate', 'normal').

% Found in: GPT
card_name('cry of contrition', 'Cry of Contrition').
card_type('cry of contrition', 'Sorcery').
card_types('cry of contrition', ['Sorcery']).
card_subtypes('cry of contrition', []).
card_colors('cry of contrition', ['B']).
card_text('cry of contrition', 'Target player discards a card.\nHaunt (When this spell card is put into a graveyard after resolving, exile it haunting target creature.)\nWhen the creature Cry of Contrition haunts dies, target player discards a card.').
card_mana_cost('cry of contrition', ['B']).
card_cmc('cry of contrition', 1).
card_layout('cry of contrition', 'normal').

% Found in: 10E, CSP
card_name('cryoclasm', 'Cryoclasm').
card_type('cryoclasm', 'Sorcery').
card_types('cryoclasm', ['Sorcery']).
card_subtypes('cryoclasm', []).
card_colors('cryoclasm', ['R']).
card_text('cryoclasm', 'Destroy target Plains or Island. Cryoclasm deals 3 damage to that land\'s controller.').
card_mana_cost('cryoclasm', ['2', 'R']).
card_cmc('cryoclasm', 3).
card_layout('cryoclasm', 'normal').

% Found in: BNG
card_name('crypsis', 'Crypsis').
card_type('crypsis', 'Instant').
card_types('crypsis', ['Instant']).
card_subtypes('crypsis', []).
card_colors('crypsis', ['U']).
card_text('crypsis', 'Target creature you control gains protection from creatures your opponents control until end of turn. Untap it.').
card_mana_cost('crypsis', ['1', 'U']).
card_cmc('crypsis', 2).
card_layout('crypsis', 'normal').

% Found in: INV
card_name('crypt angel', 'Crypt Angel').
card_type('crypt angel', 'Creature — Angel').
card_types('crypt angel', ['Creature']).
card_subtypes('crypt angel', ['Angel']).
card_colors('crypt angel', ['B']).
card_text('crypt angel', 'Flying, protection from white\nWhen Crypt Angel enters the battlefield, return target blue or red creature card from your graveyard to your hand.').
card_mana_cost('crypt angel', ['4', 'B']).
card_cmc('crypt angel', 5).
card_layout('crypt angel', 'normal').
card_power('crypt angel', 3).
card_toughness('crypt angel', 3).

% Found in: DIS
card_name('crypt champion', 'Crypt Champion').
card_type('crypt champion', 'Creature — Zombie').
card_types('crypt champion', ['Creature']).
card_subtypes('crypt champion', ['Zombie']).
card_colors('crypt champion', ['B']).
card_text('crypt champion', 'Double strike\nWhen Crypt Champion enters the battlefield, each player puts a creature card with converted mana cost 3 or less from his or her graveyard onto the battlefield.\nWhen Crypt Champion enters the battlefield, sacrifice it unless {R} was spent to cast it.').
card_mana_cost('crypt champion', ['3', 'B']).
card_cmc('crypt champion', 4).
card_layout('crypt champion', 'normal').
card_power('crypt champion', 2).
card_toughness('crypt champion', 2).

% Found in: MIR
card_name('crypt cobra', 'Crypt Cobra').
card_type('crypt cobra', 'Creature — Snake').
card_types('crypt cobra', ['Creature']).
card_subtypes('crypt cobra', ['Snake']).
card_colors('crypt cobra', ['B']).
card_text('crypt cobra', 'Whenever Crypt Cobra attacks and isn\'t blocked, defending player gets a poison counter. (A player with ten or more poison counters loses the game.)').
card_mana_cost('crypt cobra', ['3', 'B']).
card_cmc('crypt cobra', 4).
card_layout('crypt cobra', 'normal').
card_power('crypt cobra', 3).
card_toughness('crypt cobra', 3).

% Found in: AVR, ODY
card_name('crypt creeper', 'Crypt Creeper').
card_type('crypt creeper', 'Creature — Zombie').
card_types('crypt creeper', ['Creature']).
card_subtypes('crypt creeper', ['Zombie']).
card_colors('crypt creeper', ['B']).
card_text('crypt creeper', 'Sacrifice Crypt Creeper: Exile target card from a graveyard.').
card_mana_cost('crypt creeper', ['1', 'B']).
card_cmc('crypt creeper', 2).
card_layout('crypt creeper', 'normal').
card_power('crypt creeper', 2).
card_toughness('crypt creeper', 1).

% Found in: C14, GTC
card_name('crypt ghast', 'Crypt Ghast').
card_type('crypt ghast', 'Creature — Spirit').
card_types('crypt ghast', ['Creature']).
card_subtypes('crypt ghast', ['Spirit']).
card_colors('crypt ghast', ['B']).
card_text('crypt ghast', 'Extort (Whenever you cast a spell, you may pay {W/B}. If you do, each opponent loses 1 life and you gain that much life.)\nWhenever you tap a Swamp for mana, add {B} to your mana pool (in addition to the mana the land produces).').
card_mana_cost('crypt ghast', ['3', 'B']).
card_cmc('crypt ghast', 4).
card_layout('crypt ghast', 'normal').
card_power('crypt ghast', 2).
card_toughness('crypt ghast', 2).

% Found in: DGM
card_name('crypt incursion', 'Crypt Incursion').
card_type('crypt incursion', 'Instant').
card_types('crypt incursion', ['Instant']).
card_subtypes('crypt incursion', []).
card_colors('crypt incursion', ['B']).
card_text('crypt incursion', 'Exile all creature cards from target player\'s graveyard. You gain 3 life for each card exiled this way.').
card_mana_cost('crypt incursion', ['2', 'B']).
card_cmc('crypt incursion', 3).
card_layout('crypt incursion', 'normal').

% Found in: C14, ZEN
card_name('crypt of agadeem', 'Crypt of Agadeem').
card_type('crypt of agadeem', 'Land').
card_types('crypt of agadeem', ['Land']).
card_subtypes('crypt of agadeem', []).
card_colors('crypt of agadeem', []).
card_text('crypt of agadeem', 'Crypt of Agadeem enters the battlefield tapped.\n{T}: Add {B} to your mana pool.\n{2}, {T}: Add {B} to your mana pool for each black creature card in your graveyard.').
card_layout('crypt of agadeem', 'normal').

% Found in: 7ED, VIS
card_name('crypt rats', 'Crypt Rats').
card_type('crypt rats', 'Creature — Rat').
card_types('crypt rats', ['Creature']).
card_subtypes('crypt rats', ['Rat']).
card_colors('crypt rats', ['B']).
card_text('crypt rats', '{X}: Crypt Rats deals X damage to each creature and each player. Spend only black mana on X.').
card_mana_cost('crypt rats', ['2', 'B']).
card_cmc('crypt rats', 3).
card_layout('crypt rats', 'normal').
card_power('crypt rats', 1).
card_toughness('crypt rats', 1).

% Found in: ZEN
card_name('crypt ripper', 'Crypt Ripper').
card_type('crypt ripper', 'Creature — Shade').
card_types('crypt ripper', ['Creature']).
card_subtypes('crypt ripper', ['Shade']).
card_colors('crypt ripper', ['B']).
card_text('crypt ripper', 'Haste\n{B}: Crypt Ripper gets +1/+1 until end of turn.').
card_mana_cost('crypt ripper', ['2', 'B', 'B']).
card_cmc('crypt ripper', 4).
card_layout('crypt ripper', 'normal').
card_power('crypt ripper', 2).
card_toughness('crypt ripper', 2).

% Found in: LGN
card_name('crypt sliver', 'Crypt Sliver').
card_type('crypt sliver', 'Creature — Sliver').
card_types('crypt sliver', ['Creature']).
card_subtypes('crypt sliver', ['Sliver']).
card_colors('crypt sliver', ['B']).
card_text('crypt sliver', 'All Slivers have \"{T}: Regenerate target Sliver.\"').
card_mana_cost('crypt sliver', ['1', 'B']).
card_cmc('crypt sliver', 2).
card_layout('crypt sliver', 'normal').
card_power('crypt sliver', 1).
card_toughness('crypt sliver', 1).

% Found in: RTR, pMGD
card_name('cryptborn horror', 'Cryptborn Horror').
card_type('cryptborn horror', 'Creature — Horror').
card_types('cryptborn horror', ['Creature']).
card_subtypes('cryptborn horror', ['Horror']).
card_colors('cryptborn horror', ['B', 'R']).
card_text('cryptborn horror', 'Trample\nCryptborn Horror enters the battlefield with X +1/+1 counters on it, where X is the total life lost by your opponents this turn.').
card_mana_cost('cryptborn horror', ['1', 'B/R', 'B/R']).
card_cmc('cryptborn horror', 3).
card_layout('cryptborn horror', 'normal').
card_power('cryptborn horror', 0).
card_toughness('cryptborn horror', 0).

% Found in: DDI, FUT
card_name('cryptic annelid', 'Cryptic Annelid').
card_type('cryptic annelid', 'Creature — Worm Beast').
card_types('cryptic annelid', ['Creature']).
card_subtypes('cryptic annelid', ['Worm', 'Beast']).
card_colors('cryptic annelid', ['U']).
card_text('cryptic annelid', 'When Cryptic Annelid enters the battlefield, scry 1, then scry 2, then scry 3. (To scry X, look at the top X cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('cryptic annelid', ['3', 'U']).
card_cmc('cryptic annelid', 4).
card_layout('cryptic annelid', 'normal').
card_power('cryptic annelid', 1).
card_toughness('cryptic annelid', 4).

% Found in: LRW, MM2, MMA, pMPR
card_name('cryptic command', 'Cryptic Command').
card_type('cryptic command', 'Instant').
card_types('cryptic command', ['Instant']).
card_subtypes('cryptic command', []).
card_colors('cryptic command', ['U']).
card_text('cryptic command', 'Choose two —\n• Counter target spell.\n• Return target permanent to its owner\'s hand.\n• Tap all creatures your opponents control.\n• Draw a card.').
card_mana_cost('cryptic command', ['1', 'U', 'U', 'U']).
card_cmc('cryptic command', 4).
card_layout('cryptic command', 'normal').

% Found in: BFZ
card_name('cryptic cruiser', 'Cryptic Cruiser').
card_type('cryptic cruiser', 'Creature — Eldrazi Processor').
card_types('cryptic cruiser', ['Creature']).
card_subtypes('cryptic cruiser', ['Eldrazi', 'Processor']).
card_colors('cryptic cruiser', []).
card_text('cryptic cruiser', 'Devoid (This card has no color.)\n{2}{U}, Put a card an opponent owns from exile into that player\'s graveyard: Tap target creature.').
card_mana_cost('cryptic cruiser', ['3', 'U']).
card_cmc('cryptic cruiser', 4).
card_layout('cryptic cruiser', 'normal').
card_power('cryptic cruiser', 3).
card_toughness('cryptic cruiser', 3).

% Found in: ONS
card_name('cryptic gateway', 'Cryptic Gateway').
card_type('cryptic gateway', 'Artifact').
card_types('cryptic gateway', ['Artifact']).
card_subtypes('cryptic gateway', []).
card_colors('cryptic gateway', []).
card_text('cryptic gateway', 'Tap two untapped creatures you control: You may put a creature card from your hand that shares a creature type with each creature tapped this way onto the battlefield.').
card_mana_cost('cryptic gateway', ['5']).
card_cmc('cryptic gateway', 5).
card_layout('cryptic gateway', 'normal').

% Found in: MBS
card_name('cryptoplasm', 'Cryptoplasm').
card_type('cryptoplasm', 'Creature — Shapeshifter').
card_types('cryptoplasm', ['Creature']).
card_subtypes('cryptoplasm', ['Shapeshifter']).
card_colors('cryptoplasm', ['U']).
card_text('cryptoplasm', 'At the beginning of your upkeep, you may have Cryptoplasm become a copy of another target creature. If you do, Cryptoplasm gains this ability.').
card_mana_cost('cryptoplasm', ['1', 'U', 'U']).
card_cmc('cryptoplasm', 3).
card_layout('cryptoplasm', 'normal').
card_power('cryptoplasm', 2).
card_toughness('cryptoplasm', 2).

% Found in: GPT
card_name('cryptwailing', 'Cryptwailing').
card_type('cryptwailing', 'Enchantment').
card_types('cryptwailing', ['Enchantment']).
card_subtypes('cryptwailing', []).
card_colors('cryptwailing', ['B']).
card_text('cryptwailing', '{1}, Exile two creature cards from your graveyard: Target player discards a card. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('cryptwailing', ['3', 'B']).
card_cmc('cryptwailing', 4).
card_layout('cryptwailing', 'normal').

% Found in: M11
card_name('crystal ball', 'Crystal Ball').
card_type('crystal ball', 'Artifact').
card_types('crystal ball', ['Artifact']).
card_subtypes('crystal ball', []).
card_colors('crystal ball', []).
card_text('crystal ball', '{1}, {T}: Scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('crystal ball', ['3']).
card_cmc('crystal ball', 3).
card_layout('crystal ball', 'normal').

% Found in: USG
card_name('crystal chimes', 'Crystal Chimes').
card_type('crystal chimes', 'Artifact').
card_types('crystal chimes', ['Artifact']).
card_subtypes('crystal chimes', []).
card_colors('crystal chimes', []).
card_text('crystal chimes', '{3}, {T}, Sacrifice Crystal Chimes: Return all enchantment cards from your graveyard to your hand.').
card_mana_cost('crystal chimes', ['3']).
card_cmc('crystal chimes', 3).
card_layout('crystal chimes', 'normal').

% Found in: MIR
card_name('crystal golem', 'Crystal Golem').
card_type('crystal golem', 'Artifact Creature — Golem').
card_types('crystal golem', ['Artifact', 'Creature']).
card_subtypes('crystal golem', ['Golem']).
card_colors('crystal golem', []).
card_text('crystal golem', 'At the beginning of your end step, Crystal Golem phases out. (While it\'s phased out, it\'s treated as though it doesn\'t exist. It phases in before you untap during your next untap step.)').
card_mana_cost('crystal golem', ['4']).
card_cmc('crystal golem', 4).
card_layout('crystal golem', 'normal').
card_power('crystal golem', 3).
card_toughness('crystal golem', 3).

% Found in: ODY
card_name('crystal quarry', 'Crystal Quarry').
card_type('crystal quarry', 'Land').
card_types('crystal quarry', ['Land']).
card_subtypes('crystal quarry', []).
card_colors('crystal quarry', []).
card_text('crystal quarry', '{T}: Add {1} to your mana pool.\n{5}, {T}: Add {W}{U}{B}{R}{G} to your mana pool.').
card_layout('crystal quarry', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, LEA, LEB
card_name('crystal rod', 'Crystal Rod').
card_type('crystal rod', 'Artifact').
card_types('crystal rod', ['Artifact']).
card_subtypes('crystal rod', []).
card_colors('crystal rod', []).
card_text('crystal rod', 'Whenever a player casts a blue spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('crystal rod', ['1']).
card_cmc('crystal rod', 1).
card_layout('crystal rod', 'normal').

% Found in: GPT
card_name('crystal seer', 'Crystal Seer').
card_type('crystal seer', 'Creature — Vedalken Wizard').
card_types('crystal seer', ['Creature']).
card_subtypes('crystal seer', ['Vedalken', 'Wizard']).
card_colors('crystal seer', ['U']).
card_text('crystal seer', 'When Crystal Seer enters the battlefield, look at the top four cards of your library, then put them back in any order.\n{4}{U}: Return Crystal Seer to its owner\'s hand.').
card_mana_cost('crystal seer', ['4', 'U']).
card_cmc('crystal seer', 5).
card_layout('crystal seer', 'normal').
card_power('crystal seer', 2).
card_toughness('crystal seer', 2).

% Found in: MRD
card_name('crystal shard', 'Crystal Shard').
card_type('crystal shard', 'Artifact').
card_types('crystal shard', ['Artifact']).
card_subtypes('crystal shard', []).
card_colors('crystal shard', []).
card_text('crystal shard', '{3}, {T} or {U}, {T}: Return target creature to its owner\'s hand unless its controller pays {1}.').
card_mana_cost('crystal shard', ['3']).
card_cmc('crystal shard', 3).
card_layout('crystal shard', 'normal').

% Found in: INV
card_name('crystal spray', 'Crystal Spray').
card_type('crystal spray', 'Instant').
card_types('crystal spray', ['Instant']).
card_subtypes('crystal spray', []).
card_colors('crystal spray', ['U']).
card_text('crystal spray', 'Change the text of target spell or permanent by replacing all instances of one color word with another or one basic land type with another until end of turn.\nDraw a card.').
card_mana_cost('crystal spray', ['2', 'U']).
card_cmc('crystal spray', 3).
card_layout('crystal spray', 'normal').

% Found in: 6ED, C14, MIR, PD3
card_name('crystal vein', 'Crystal Vein').
card_type('crystal vein', 'Land').
card_types('crystal vein', ['Land']).
card_subtypes('crystal vein', []).
card_colors('crystal vein', []).
card_text('crystal vein', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Crystal Vein: Add {2} to your mana pool.').
card_layout('crystal vein', 'normal').

% Found in: JOU
card_name('crystalline nautilus', 'Crystalline Nautilus').
card_type('crystalline nautilus', 'Enchantment Creature — Nautilus').
card_types('crystalline nautilus', ['Enchantment', 'Creature']).
card_subtypes('crystalline nautilus', ['Nautilus']).
card_colors('crystalline nautilus', ['U']).
card_text('crystalline nautilus', 'Bestow {3}{U}{U} (If you cast this card for its bestow cost, it\'s an Aura spell with enchant creature. It becomes a creature again if it\'s not attached to a creature.)\nWhen Crystalline Nautilus becomes the target of a spell or ability, sacrifice it.\nEnchanted creature gets +4/+4 and has \"When this creature becomes the target of a spell or ability, sacrifice it.\"').
card_mana_cost('crystalline nautilus', ['2', 'U']).
card_cmc('crystalline nautilus', 3).
card_layout('crystalline nautilus', 'normal').
card_power('crystalline nautilus', 4).
card_toughness('crystalline nautilus', 4).

% Found in: H09, STH, TPR, pFNM
card_name('crystalline sliver', 'Crystalline Sliver').
card_type('crystalline sliver', 'Creature — Sliver').
card_types('crystalline sliver', ['Creature']).
card_subtypes('crystalline sliver', ['Sliver']).
card_colors('crystalline sliver', ['W', 'U']).
card_text('crystalline sliver', 'All Slivers have shroud. (They can\'t be the targets of spells or abilities.)').
card_mana_cost('crystalline sliver', ['W', 'U']).
card_cmc('crystalline sliver', 2).
card_layout('crystalline sliver', 'normal').
card_power('crystalline sliver', 2).
card_toughness('crystalline sliver', 2).

% Found in: ARB
card_name('crystallization', 'Crystallization').
card_type('crystallization', 'Enchantment — Aura').
card_types('crystallization', ['Enchantment']).
card_subtypes('crystallization', ['Aura']).
card_colors('crystallization', ['W', 'U', 'G']).
card_text('crystallization', 'Enchant creature\nEnchanted creature can\'t attack or block.\nWhen enchanted creature becomes the target of a spell or ability, exile that creature.').
card_mana_cost('crystallization', ['G/U', 'W']).
card_cmc('crystallization', 2).
card_layout('crystallization', 'normal').

% Found in: M10, M11, M12
card_name('cudgel troll', 'Cudgel Troll').
card_type('cudgel troll', 'Creature — Troll').
card_types('cudgel troll', ['Creature']).
card_subtypes('cudgel troll', ['Troll']).
card_colors('cudgel troll', ['G']).
card_text('cudgel troll', '{G}: Regenerate Cudgel Troll. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('cudgel troll', ['2', 'G', 'G']).
card_cmc('cudgel troll', 4).
card_layout('cudgel troll', 'normal').
card_power('cudgel troll', 4).
card_toughness('cudgel troll', 3).

% Found in: MM2, SOM
card_name('culling dais', 'Culling Dais').
card_type('culling dais', 'Artifact').
card_types('culling dais', ['Artifact']).
card_subtypes('culling dais', []).
card_colors('culling dais', []).
card_text('culling dais', '{T}, Sacrifice a creature: Put a charge counter on Culling Dais.\n{1}, Sacrifice Culling Dais: Draw a card for each charge counter on Culling Dais.').
card_mana_cost('culling dais', ['2']).
card_cmc('culling dais', 2).
card_layout('culling dais', 'normal').

% Found in: BFZ
card_name('culling drone', 'Culling Drone').
card_type('culling drone', 'Creature — Eldrazi Drone').
card_types('culling drone', ['Creature']).
card_subtypes('culling drone', ['Eldrazi', 'Drone']).
card_colors('culling drone', []).
card_text('culling drone', 'Devoid (This card has no color.)\nIngest (Whenever this creature deals combat damage to a player, that player exiles the top card of his or her library.)').
card_mana_cost('culling drone', ['1', 'B']).
card_cmc('culling drone', 2).
card_layout('culling drone', 'normal').
card_power('culling drone', 2).
card_toughness('culling drone', 2).

% Found in: BNG
card_name('culling mark', 'Culling Mark').
card_type('culling mark', 'Sorcery').
card_types('culling mark', ['Sorcery']).
card_subtypes('culling mark', []).
card_colors('culling mark', ['G']).
card_text('culling mark', 'Target creature blocks this turn if able.').
card_mana_cost('culling mark', ['2', 'G']).
card_cmc('culling mark', 3).
card_layout('culling mark', 'normal').

% Found in: MRD
card_name('culling scales', 'Culling Scales').
card_type('culling scales', 'Artifact').
card_types('culling scales', ['Artifact']).
card_subtypes('culling scales', []).
card_colors('culling scales', []).
card_text('culling scales', 'At the beginning of your upkeep, destroy target nonland permanent with the lowest converted mana cost. (If two or more permanents are tied for lowest cost, target any one of them.)').
card_mana_cost('culling scales', ['3']).
card_cmc('culling scales', 3).
card_layout('culling scales', 'normal').

% Found in: GPT
card_name('culling sun', 'Culling Sun').
card_type('culling sun', 'Sorcery').
card_types('culling sun', ['Sorcery']).
card_subtypes('culling sun', []).
card_colors('culling sun', ['W', 'B']).
card_text('culling sun', 'Destroy each creature with converted mana cost 3 or less.').
card_mana_cost('culling sun', ['2', 'W', 'W', 'B']).
card_cmc('culling sun', 5).
card_layout('culling sun', 'normal').

% Found in: EXO
card_name('culling the weak', 'Culling the Weak').
card_type('culling the weak', 'Instant').
card_types('culling the weak', ['Instant']).
card_subtypes('culling the weak', []).
card_colors('culling the weak', ['B']).
card_text('culling the weak', 'As an additional cost to cast Culling the Weak, sacrifice a creature.\nAdd {B}{B}{B}{B} to your mana pool.').
card_mana_cost('culling the weak', ['B']).
card_cmc('culling the weak', 1).
card_layout('culling the weak', 'normal').

% Found in: SHM
card_name('cultbrand cinder', 'Cultbrand Cinder').
card_type('cultbrand cinder', 'Creature — Elemental Shaman').
card_types('cultbrand cinder', ['Creature']).
card_subtypes('cultbrand cinder', ['Elemental', 'Shaman']).
card_colors('cultbrand cinder', ['B', 'R']).
card_text('cultbrand cinder', 'When Cultbrand Cinder enters the battlefield, put a -1/-1 counter on target creature.').
card_mana_cost('cultbrand cinder', ['4', 'B/R']).
card_cmc('cultbrand cinder', 5).
card_layout('cultbrand cinder', 'normal').
card_power('cultbrand cinder', 3).
card_toughness('cultbrand cinder', 3).

% Found in: C13, CMD, M11, PC2, pFNM
card_name('cultivate', 'Cultivate').
card_type('cultivate', 'Sorcery').
card_types('cultivate', ['Sorcery']).
card_subtypes('cultivate', []).
card_colors('cultivate', ['G']).
card_text('cultivate', 'Search your library for up to two basic land cards, reveal those cards, and put one onto the battlefield tapped and the other into your hand. Then shuffle your library.').
card_mana_cost('cultivate', ['2', 'G']).
card_cmc('cultivate', 3).
card_layout('cultivate', 'normal').

% Found in: ODY
card_name('cultural exchange', 'Cultural Exchange').
card_type('cultural exchange', 'Sorcery').
card_types('cultural exchange', ['Sorcery']).
card_subtypes('cultural exchange', []).
card_colors('cultural exchange', ['U']).
card_text('cultural exchange', 'Choose any number of creatures target player controls. Choose the same number of creatures another target player controls. Those players exchange control of those creatures. (This effect lasts indefinitely.)').
card_mana_cost('cultural exchange', ['4', 'U', 'U']).
card_cmc('cultural exchange', 6).
card_layout('cultural exchange', 'normal').

% Found in: CON
card_name('cumber stone', 'Cumber Stone').
card_type('cumber stone', 'Artifact').
card_types('cumber stone', ['Artifact']).
card_subtypes('cumber stone', []).
card_colors('cumber stone', ['U']).
card_text('cumber stone', 'Creatures your opponents control get -1/-0.').
card_mana_cost('cumber stone', ['3', 'U']).
card_cmc('cumber stone', 4).
card_layout('cumber stone', 'normal').

% Found in: EXO
card_name('cunning', 'Cunning').
card_type('cunning', 'Enchantment — Aura').
card_types('cunning', ['Enchantment']).
card_subtypes('cunning', ['Aura']).
card_colors('cunning', ['U']).
card_text('cunning', 'Enchant creature\nEnchanted creature gets +3/+3.\nWhen enchanted creature attacks or blocks, sacrifice Cunning at the beginning of the next cleanup step.').
card_mana_cost('cunning', ['1', 'U']).
card_cmc('cunning', 2).
card_layout('cunning', 'normal').

% Found in: PTK
card_name('cunning advisor', 'Cunning Advisor').
card_type('cunning advisor', 'Creature — Human Advisor').
card_types('cunning advisor', ['Creature']).
card_subtypes('cunning advisor', ['Human', 'Advisor']).
card_colors('cunning advisor', ['B']).
card_text('cunning advisor', '{T}: Target opponent discards a card. Activate this ability only during your turn, before attackers are declared.').
card_mana_cost('cunning advisor', ['3', 'B']).
card_cmc('cunning advisor', 4).
card_layout('cunning advisor', 'normal').
card_power('cunning advisor', 1).
card_toughness('cunning advisor', 1).

% Found in: BOK
card_name('cunning bandit', 'Cunning Bandit').
card_type('cunning bandit', 'Creature — Human Warrior').
card_types('cunning bandit', ['Creature']).
card_subtypes('cunning bandit', ['Human', 'Warrior']).
card_colors('cunning bandit', ['R']).
card_text('cunning bandit', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Cunning Bandit.\nAt the beginning of the end step, if there are two or more ki counters on Cunning Bandit, you may flip it.').
card_mana_cost('cunning bandit', ['1', 'R', 'R']).
card_cmc('cunning bandit', 3).
card_layout('cunning bandit', 'flip').
card_power('cunning bandit', 2).
card_toughness('cunning bandit', 2).
card_sides('cunning bandit', 'azamuki, treachery incarnate').

% Found in: DTK
card_name('cunning breezedancer', 'Cunning Breezedancer').
card_type('cunning breezedancer', 'Creature — Dragon').
card_types('cunning breezedancer', ['Creature']).
card_subtypes('cunning breezedancer', ['Dragon']).
card_colors('cunning breezedancer', ['W', 'U']).
card_text('cunning breezedancer', 'Flying\nWhenever you cast a noncreature spell, Cunning Breezedancer gets +2/+2 until end of turn.').
card_mana_cost('cunning breezedancer', ['4', 'W', 'U']).
card_cmc('cunning breezedancer', 6).
card_layout('cunning breezedancer', 'normal').
card_power('cunning breezedancer', 4).
card_toughness('cunning breezedancer', 4).

% Found in: PO2
card_name('cunning giant', 'Cunning Giant').
card_type('cunning giant', 'Creature — Giant').
card_types('cunning giant', ['Creature']).
card_subtypes('cunning giant', ['Giant']).
card_colors('cunning giant', ['R']).
card_text('cunning giant', 'If Cunning Giant is unblocked, you may have it assign its combat damage to a creature defending player controls.').
card_mana_cost('cunning giant', ['5', 'R']).
card_cmc('cunning giant', 6).
card_layout('cunning giant', 'normal').
card_power('cunning giant', 4).
card_toughness('cunning giant', 4).

% Found in: ALA
card_name('cunning lethemancer', 'Cunning Lethemancer').
card_type('cunning lethemancer', 'Creature — Human Wizard').
card_types('cunning lethemancer', ['Creature']).
card_subtypes('cunning lethemancer', ['Human', 'Wizard']).
card_colors('cunning lethemancer', ['B']).
card_text('cunning lethemancer', 'At the beginning of your upkeep, each player discards a card.').
card_mana_cost('cunning lethemancer', ['2', 'B']).
card_cmc('cunning lethemancer', 3).
card_layout('cunning lethemancer', 'normal').
card_power('cunning lethemancer', 2).
card_toughness('cunning lethemancer', 2).

% Found in: WWK
card_name('cunning sparkmage', 'Cunning Sparkmage').
card_type('cunning sparkmage', 'Creature — Human Shaman').
card_types('cunning sparkmage', ['Creature']).
card_subtypes('cunning sparkmage', ['Human', 'Shaman']).
card_colors('cunning sparkmage', ['R']).
card_text('cunning sparkmage', 'Haste\n{T}: Cunning Sparkmage deals 1 damage to target creature or player.').
card_mana_cost('cunning sparkmage', ['2', 'R']).
card_cmc('cunning sparkmage', 3).
card_layout('cunning sparkmage', 'normal').
card_power('cunning sparkmage', 0).
card_toughness('cunning sparkmage', 1).

% Found in: FRF
card_name('cunning strike', 'Cunning Strike').
card_type('cunning strike', 'Instant').
card_types('cunning strike', ['Instant']).
card_subtypes('cunning strike', []).
card_colors('cunning strike', ['U', 'R']).
card_text('cunning strike', 'Cunning Strike deals 2 damage to target creature and 2 damage to target player.\nDraw a card.').
card_mana_cost('cunning strike', ['3', 'U', 'R']).
card_cmc('cunning strike', 5).
card_layout('cunning strike', 'normal').

% Found in: JUD, pJGP
card_name('cunning wish', 'Cunning Wish').
card_type('cunning wish', 'Instant').
card_types('cunning wish', ['Instant']).
card_subtypes('cunning wish', []).
card_colors('cunning wish', ['U']).
card_text('cunning wish', 'You may choose an instant card you own from outside the game, reveal that card, and put it into your hand. Exile Cunning Wish.').
card_mana_cost('cunning wish', ['2', 'U']).
card_cmc('cunning wish', 3).
card_layout('cunning wish', 'normal').

% Found in: ARN, ATH, CHR, MED
card_name('cuombajj witches', 'Cuombajj Witches').
card_type('cuombajj witches', 'Creature — Human Wizard').
card_types('cuombajj witches', ['Creature']).
card_subtypes('cuombajj witches', ['Human', 'Wizard']).
card_colors('cuombajj witches', ['B']).
card_text('cuombajj witches', '{T}: Cuombajj Witches deals 1 damage to target creature or player and 1 damage to target creature or player of an opponent\'s choice.').
card_mana_cost('cuombajj witches', ['B', 'B']).
card_cmc('cuombajj witches', 2).
card_layout('cuombajj witches', 'normal').
card_power('cuombajj witches', 1).
card_toughness('cuombajj witches', 3).

% Found in: BRB, USG
card_name('curfew', 'Curfew').
card_type('curfew', 'Instant').
card_types('curfew', ['Instant']).
card_subtypes('curfew', []).
card_colors('curfew', ['U']).
card_text('curfew', 'Each player returns a creature he or she controls to its owner\'s hand.').
card_mana_cost('curfew', ['U']).
card_cmc('curfew', 1).
card_layout('curfew', 'normal').

% Found in: 8ED, EXO, ISD, TPR
card_name('curiosity', 'Curiosity').
card_type('curiosity', 'Enchantment — Aura').
card_types('curiosity', ['Enchantment']).
card_subtypes('curiosity', ['Aura']).
card_colors('curiosity', ['U']).
card_text('curiosity', 'Enchant creature\nWhenever enchanted creature deals damage to an opponent, you may draw a card.').
card_mana_cost('curiosity', ['U']).
card_cmc('curiosity', 1).
card_layout('curiosity', 'normal').

% Found in: DRK
card_name('curse artifact', 'Curse Artifact').
card_type('curse artifact', 'Enchantment — Aura').
card_types('curse artifact', ['Enchantment']).
card_subtypes('curse artifact', ['Aura']).
card_colors('curse artifact', ['B']).
card_text('curse artifact', 'Enchant artifact\nAt the beginning of the upkeep of enchanted artifact\'s controller, Curse Artifact deals 2 damage to that player unless he or she sacrifices that artifact.').
card_mana_cost('curse artifact', ['2', 'B', 'B']).
card_cmc('curse artifact', 4).
card_layout('curse artifact', 'normal').

% Found in: DKA
card_name('curse of bloodletting', 'Curse of Bloodletting').
card_type('curse of bloodletting', 'Enchantment — Aura Curse').
card_types('curse of bloodletting', ['Enchantment']).
card_subtypes('curse of bloodletting', ['Aura', 'Curse']).
card_colors('curse of bloodletting', ['R']).
card_text('curse of bloodletting', 'Enchant player\nIf a source would deal damage to enchanted player, it deals double that damage to that player instead.').
card_mana_cost('curse of bloodletting', ['3', 'R', 'R']).
card_cmc('curse of bloodletting', 5).
card_layout('curse of bloodletting', 'normal').

% Found in: SHM
card_name('curse of chains', 'Curse of Chains').
card_type('curse of chains', 'Enchantment — Aura').
card_types('curse of chains', ['Enchantment']).
card_subtypes('curse of chains', ['Aura']).
card_colors('curse of chains', ['W', 'U']).
card_text('curse of chains', 'Enchant creature\nAt the beginning of each upkeep, tap enchanted creature.').
card_mana_cost('curse of chains', ['1', 'W/U']).
card_cmc('curse of chains', 2).
card_layout('curse of chains', 'normal').

% Found in: C13
card_name('curse of chaos', 'Curse of Chaos').
card_type('curse of chaos', 'Enchantment — Aura Curse').
card_types('curse of chaos', ['Enchantment']).
card_subtypes('curse of chaos', ['Aura', 'Curse']).
card_colors('curse of chaos', ['R']).
card_text('curse of chaos', 'Enchant player\nWhenever a player attacks enchanted player with one or more creatures, that attacking player may discard a card. If the player does, he or she draws a card.').
card_mana_cost('curse of chaos', ['2', 'R']).
card_cmc('curse of chaos', 3).
card_layout('curse of chaos', 'normal').

% Found in: ISD
card_name('curse of death\'s hold', 'Curse of Death\'s Hold').
card_type('curse of death\'s hold', 'Enchantment — Aura Curse').
card_types('curse of death\'s hold', ['Enchantment']).
card_subtypes('curse of death\'s hold', ['Aura', 'Curse']).
card_colors('curse of death\'s hold', ['B']).
card_text('curse of death\'s hold', 'Enchant player\nCreatures enchanted player controls get -1/-1.').
card_mana_cost('curse of death\'s hold', ['3', 'B', 'B']).
card_cmc('curse of death\'s hold', 5).
card_layout('curse of death\'s hold', 'normal').

% Found in: DKA
card_name('curse of echoes', 'Curse of Echoes').
card_type('curse of echoes', 'Enchantment — Aura Curse').
card_types('curse of echoes', ['Enchantment']).
card_subtypes('curse of echoes', ['Aura', 'Curse']).
card_colors('curse of echoes', ['U']).
card_text('curse of echoes', 'Enchant player\nWhenever enchanted player casts an instant or sorcery spell, each other player may copy that spell and may choose new targets for the copy he or she controls.').
card_mana_cost('curse of echoes', ['4', 'U']).
card_cmc('curse of echoes', 5).
card_layout('curse of echoes', 'normal').

% Found in: DKA
card_name('curse of exhaustion', 'Curse of Exhaustion').
card_type('curse of exhaustion', 'Enchantment — Aura Curse').
card_types('curse of exhaustion', ['Enchantment']).
card_subtypes('curse of exhaustion', ['Aura', 'Curse']).
card_colors('curse of exhaustion', ['W']).
card_text('curse of exhaustion', 'Enchant player\nEnchanted player can\'t cast more than one spell each turn.').
card_mana_cost('curse of exhaustion', ['2', 'W', 'W']).
card_cmc('curse of exhaustion', 4).
card_layout('curse of exhaustion', 'normal').

% Found in: C13
card_name('curse of inertia', 'Curse of Inertia').
card_type('curse of inertia', 'Enchantment — Aura Curse').
card_types('curse of inertia', ['Enchantment']).
card_subtypes('curse of inertia', ['Aura', 'Curse']).
card_colors('curse of inertia', ['U']).
card_text('curse of inertia', 'Enchant player\nWhenever a player attacks enchanted player with one or more creatures, that attacking player may tap or untap target permanent of his or her choice.').
card_mana_cost('curse of inertia', ['2', 'U']).
card_cmc('curse of inertia', 3).
card_layout('curse of inertia', 'normal').

% Found in: ICE
card_name('curse of marit lage', 'Curse of Marit Lage').
card_type('curse of marit lage', 'Enchantment').
card_types('curse of marit lage', ['Enchantment']).
card_subtypes('curse of marit lage', []).
card_colors('curse of marit lage', ['R']).
card_text('curse of marit lage', 'When Curse of Marit Lage enters the battlefield, tap all Islands.\nIslands don\'t untap during their controllers\' untap steps.').
card_mana_cost('curse of marit lage', ['3', 'R', 'R']).
card_cmc('curse of marit lage', 5).
card_layout('curse of marit lage', 'normal').

% Found in: DKA
card_name('curse of misfortunes', 'Curse of Misfortunes').
card_type('curse of misfortunes', 'Enchantment — Aura Curse').
card_types('curse of misfortunes', ['Enchantment']).
card_subtypes('curse of misfortunes', ['Aura', 'Curse']).
card_colors('curse of misfortunes', ['B']).
card_text('curse of misfortunes', 'Enchant player\nAt the beginning of your upkeep, you may search your library for a Curse card that doesn\'t have the same name as a Curse attached to enchanted player, put it onto the battlefield attached to that player, then shuffle your library.').
card_mana_cost('curse of misfortunes', ['4', 'B']).
card_cmc('curse of misfortunes', 5).
card_layout('curse of misfortunes', 'normal').

% Found in: ISD
card_name('curse of oblivion', 'Curse of Oblivion').
card_type('curse of oblivion', 'Enchantment — Aura Curse').
card_types('curse of oblivion', ['Enchantment']).
card_subtypes('curse of oblivion', ['Aura', 'Curse']).
card_colors('curse of oblivion', ['B']).
card_text('curse of oblivion', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, that player exiles two cards from his or her graveyard.').
card_mana_cost('curse of oblivion', ['3', 'B']).
card_cmc('curse of oblivion', 4).
card_layout('curse of oblivion', 'normal').

% Found in: C13
card_name('curse of predation', 'Curse of Predation').
card_type('curse of predation', 'Enchantment — Aura Curse').
card_types('curse of predation', ['Enchantment']).
card_subtypes('curse of predation', ['Aura', 'Curse']).
card_colors('curse of predation', ['G']).
card_text('curse of predation', 'Enchant player\nWhenever a creature attacks enchanted player, put a +1/+1 counter on it.').
card_mana_cost('curse of predation', ['2', 'G']).
card_cmc('curse of predation', 3).
card_layout('curse of predation', 'normal').

% Found in: C13
card_name('curse of shallow graves', 'Curse of Shallow Graves').
card_type('curse of shallow graves', 'Enchantment — Aura Curse').
card_types('curse of shallow graves', ['Enchantment']).
card_subtypes('curse of shallow graves', ['Aura', 'Curse']).
card_colors('curse of shallow graves', ['B']).
card_text('curse of shallow graves', 'Enchant player\nWhenever a player attacks enchanted player with one or more creatures, that attacking player may put a 2/2 black Zombie creature token onto the battlefield tapped.').
card_mana_cost('curse of shallow graves', ['2', 'B']).
card_cmc('curse of shallow graves', 3).
card_layout('curse of shallow graves', 'normal').

% Found in: ISD
card_name('curse of stalked prey', 'Curse of Stalked Prey').
card_type('curse of stalked prey', 'Enchantment — Aura Curse').
card_types('curse of stalked prey', ['Enchantment']).
card_subtypes('curse of stalked prey', ['Aura', 'Curse']).
card_colors('curse of stalked prey', ['R']).
card_text('curse of stalked prey', 'Enchant player\nWhenever a creature deals combat damage to enchanted player, put a +1/+1 counter on that creature.').
card_mana_cost('curse of stalked prey', ['1', 'R']).
card_cmc('curse of stalked prey', 2).
card_layout('curse of stalked prey', 'normal').

% Found in: ISD, pWPN
card_name('curse of the bloody tome', 'Curse of the Bloody Tome').
card_type('curse of the bloody tome', 'Enchantment — Aura Curse').
card_types('curse of the bloody tome', ['Enchantment']).
card_subtypes('curse of the bloody tome', ['Aura', 'Curse']).
card_colors('curse of the bloody tome', ['U']).
card_text('curse of the bloody tome', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, that player puts the top two cards of his or her library into his or her graveyard.').
card_mana_cost('curse of the bloody tome', ['2', 'U']).
card_cmc('curse of the bloody tome', 3).
card_layout('curse of the bloody tome', 'normal').

% Found in: TSP
card_name('curse of the cabal', 'Curse of the Cabal').
card_type('curse of the cabal', 'Sorcery').
card_types('curse of the cabal', ['Sorcery']).
card_subtypes('curse of the cabal', []).
card_colors('curse of the cabal', ['B']).
card_text('curse of the cabal', 'Target player sacrifices half the permanents he or she controls, rounded down.\nSuspend 2—{2}{B}{B} (Rather than cast this card from your hand, you may pay {2}{B}{B} and exile it with two time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)\nAt the beginning of each player\'s upkeep, if Curse of the Cabal is suspended, that player may sacrifice a permanent. If he or she does, put two time counters on Curse of the Cabal.').
card_mana_cost('curse of the cabal', ['9', 'B']).
card_cmc('curse of the cabal', 10).
card_layout('curse of the cabal', 'normal').

% Found in: UNH
card_name('curse of the fire penguin', 'Curse of the Fire Penguin').
card_type('curse of the fire penguin', 'Enchant Creature').
card_types('curse of the fire penguin', ['Enchant', 'Creature']).
card_subtypes('curse of the fire penguin', []).
card_colors('curse of the fire penguin', ['R']).
card_text('curse of the fire penguin', 'Curse of the Fire Penguin consumes and confuses enchanted creature.\n-----\nCreature Penguin\nTrample\nWhen this creature is put into a graveyard from play, return Curse of the Fire Penguin from your graveyard to play.\n6/5').
card_mana_cost('curse of the fire penguin', ['4', 'R', 'R']).
card_cmc('curse of the fire penguin', 6).
card_layout('curse of the fire penguin', 'normal').

% Found in: C13
card_name('curse of the forsaken', 'Curse of the Forsaken').
card_type('curse of the forsaken', 'Enchantment — Aura Curse').
card_types('curse of the forsaken', ['Enchantment']).
card_subtypes('curse of the forsaken', ['Aura', 'Curse']).
card_colors('curse of the forsaken', ['W']).
card_text('curse of the forsaken', 'Enchant player\nWhenever a creature attacks enchanted player, its controller gains 1 life.').
card_mana_cost('curse of the forsaken', ['2', 'W']).
card_cmc('curse of the forsaken', 3).
card_layout('curse of the forsaken', 'normal').

% Found in: ISD
card_name('curse of the nightly hunt', 'Curse of the Nightly Hunt').
card_type('curse of the nightly hunt', 'Enchantment — Aura Curse').
card_types('curse of the nightly hunt', ['Enchantment']).
card_subtypes('curse of the nightly hunt', ['Aura', 'Curse']).
card_colors('curse of the nightly hunt', ['R']).
card_text('curse of the nightly hunt', 'Enchant player\nCreatures enchanted player controls attack each turn if able.').
card_mana_cost('curse of the nightly hunt', ['2', 'R']).
card_cmc('curse of the nightly hunt', 3).
card_layout('curse of the nightly hunt', 'normal').

% Found in: ISD
card_name('curse of the pierced heart', 'Curse of the Pierced Heart').
card_type('curse of the pierced heart', 'Enchantment — Aura Curse').
card_types('curse of the pierced heart', ['Enchantment']).
card_subtypes('curse of the pierced heart', ['Aura', 'Curse']).
card_colors('curse of the pierced heart', ['R']).
card_text('curse of the pierced heart', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, Curse of the Pierced Heart deals 1 damage to that player.').
card_mana_cost('curse of the pierced heart', ['1', 'R']).
card_cmc('curse of the pierced heart', 2).
card_layout('curse of the pierced heart', 'normal').

% Found in: THS
card_name('curse of the swine', 'Curse of the Swine').
card_type('curse of the swine', 'Sorcery').
card_types('curse of the swine', ['Sorcery']).
card_subtypes('curse of the swine', []).
card_colors('curse of the swine', ['U']).
card_text('curse of the swine', 'Exile X target creatures. For each creature exiled this way, its controller puts a 2/2 green Boar creature token onto the battlefield.').
card_mana_cost('curse of the swine', ['X', 'U', 'U']).
card_cmc('curse of the swine', 2).
card_layout('curse of the swine', 'normal').

% Found in: DKA, pWPN
card_name('curse of thirst', 'Curse of Thirst').
card_type('curse of thirst', 'Enchantment — Aura Curse').
card_types('curse of thirst', ['Enchantment']).
card_subtypes('curse of thirst', ['Aura', 'Curse']).
card_colors('curse of thirst', ['B']).
card_text('curse of thirst', 'Enchant player\nAt the beginning of enchanted player\'s upkeep, Curse of Thirst deals damage to that player equal to the number of Curses attached to him or her.').
card_mana_cost('curse of thirst', ['4', 'B']).
card_cmc('curse of thirst', 5).
card_layout('curse of thirst', 'normal').

% Found in: ROE, pWPN
card_name('curse of wizardry', 'Curse of Wizardry').
card_type('curse of wizardry', 'Enchantment').
card_types('curse of wizardry', ['Enchantment']).
card_subtypes('curse of wizardry', []).
card_colors('curse of wizardry', ['B']).
card_text('curse of wizardry', 'As Curse of Wizardry enters the battlefield, choose a color.\nWhenever a player casts a spell of the chosen color, that player loses 1 life.').
card_mana_cost('curse of wizardry', ['2', 'B', 'B']).
card_cmc('curse of wizardry', 4).
card_layout('curse of wizardry', 'normal').

% Found in: AVR
card_name('cursebreak', 'Cursebreak').
card_type('cursebreak', 'Instant').
card_types('cursebreak', ['Instant']).
card_subtypes('cursebreak', []).
card_colors('cursebreak', ['W']).
card_text('cursebreak', 'Destroy target enchantment. You gain 2 life.').
card_mana_cost('cursebreak', ['1', 'W']).
card_cmc('cursebreak', 2).
card_layout('cursebreak', 'normal').

% Found in: SHM
card_name('cursecatcher', 'Cursecatcher').
card_type('cursecatcher', 'Creature — Merfolk Wizard').
card_types('cursecatcher', ['Creature']).
card_subtypes('cursecatcher', ['Merfolk', 'Wizard']).
card_colors('cursecatcher', ['U']).
card_text('cursecatcher', 'Sacrifice Cursecatcher: Counter target instant or sorcery spell unless its controller pays {1}.').
card_mana_cost('cursecatcher', ['U']).
card_cmc('cursecatcher', 1).
card_layout('cursecatcher', 'normal').
card_power('cursecatcher', 1).
card_toughness('cursecatcher', 1).

% Found in: EXO, INV, TPR
card_name('cursed flesh', 'Cursed Flesh').
card_type('cursed flesh', 'Enchantment — Aura').
card_types('cursed flesh', ['Enchantment']).
card_subtypes('cursed flesh', ['Aura']).
card_colors('cursed flesh', ['B']).
card_text('cursed flesh', 'Enchant creature\nEnchanted creature gets -1/-1 and has fear. (It can\'t be blocked except by artifact creatures and/or black creatures.)').
card_mana_cost('cursed flesh', ['B']).
card_cmc('cursed flesh', 1).
card_layout('cursed flesh', 'normal').

% Found in: 2ED, 3ED, 4ED, 5ED, CED, CEI, ITP, LEA, LEB, RQS
card_name('cursed land', 'Cursed Land').
card_type('cursed land', 'Enchantment — Aura').
card_types('cursed land', ['Enchantment']).
card_subtypes('cursed land', ['Aura']).
card_colors('cursed land', ['B']).
card_text('cursed land', 'Enchant land\nAt the beginning of the upkeep of enchanted land\'s controller, Cursed Land deals 1 damage to that player.').
card_mana_cost('cursed land', ['2', 'B', 'B']).
card_cmc('cursed land', 4).
card_layout('cursed land', 'normal').

% Found in: ODY
card_name('cursed monstrosity', 'Cursed Monstrosity').
card_type('cursed monstrosity', 'Creature — Horror').
card_types('cursed monstrosity', ['Creature']).
card_subtypes('cursed monstrosity', ['Horror']).
card_colors('cursed monstrosity', ['B']).
card_text('cursed monstrosity', 'Flying\nWhenever Cursed Monstrosity becomes the target of a spell or ability, sacrifice it unless you discard a land card.').
card_mana_cost('cursed monstrosity', ['4', 'B']).
card_cmc('cursed monstrosity', 5).
card_layout('cursed monstrosity', 'normal').
card_power('cursed monstrosity', 4).
card_toughness('cursed monstrosity', 3).

% Found in: 4ED, ATQ, MED
card_name('cursed rack', 'Cursed Rack').
card_type('cursed rack', 'Artifact').
card_types('cursed rack', ['Artifact']).
card_subtypes('cursed rack', []).
card_colors('cursed rack', []).
card_text('cursed rack', 'As Cursed Rack enters the battlefield, choose an opponent.\nThe chosen player\'s maximum hand size is four.').
card_mana_cost('cursed rack', ['4']).
card_cmc('cursed rack', 4).
card_layout('cursed rack', 'normal').

% Found in: CHK
card_name('cursed ronin', 'Cursed Ronin').
card_type('cursed ronin', 'Creature — Human Samurai').
card_types('cursed ronin', ['Creature']).
card_subtypes('cursed ronin', ['Human', 'Samurai']).
card_colors('cursed ronin', ['B']).
card_text('cursed ronin', 'Bushido 1 (Whenever this creature blocks or becomes blocked, it gets +1/+1 until end of turn.)\n{B}: Cursed Ronin gets +1/+1 until end of turn.').
card_mana_cost('cursed ronin', ['3', 'B']).
card_cmc('cursed ronin', 4).
card_layout('cursed ronin', 'normal').
card_power('cursed ronin', 1).
card_toughness('cursed ronin', 1).

% Found in: TMP, TPR, VMA
card_name('cursed scroll', 'Cursed Scroll').
card_type('cursed scroll', 'Artifact').
card_types('cursed scroll', ['Artifact']).
card_subtypes('cursed scroll', []).
card_colors('cursed scroll', []).
card_text('cursed scroll', '{3}, {T}: Name a card. Reveal a card at random from your hand. If it\'s the named card, Cursed Scroll deals 2 damage to target creature or player.').
card_mana_cost('cursed scroll', ['1']).
card_cmc('cursed scroll', 1).
card_layout('cursed scroll', 'normal').
card_reserved('cursed scroll').

% Found in: 6ED, MIR
card_name('cursed totem', 'Cursed Totem').
card_type('cursed totem', 'Artifact').
card_types('cursed totem', ['Artifact']).
card_subtypes('cursed totem', []).
card_colors('cursed totem', []).
card_text('cursed totem', 'Activated abilities of creatures can\'t be activated.').
card_mana_cost('cursed totem', ['2']).
card_cmc('cursed totem', 2).
card_layout('cursed totem', 'normal').

% Found in: SOK
card_name('curtain of light', 'Curtain of Light').
card_type('curtain of light', 'Instant').
card_types('curtain of light', ['Instant']).
card_subtypes('curtain of light', []).
card_colors('curtain of light', ['W']).
card_text('curtain of light', 'Cast Curtain of Light only during combat after blockers are declared.\nTarget unblocked attacking creature becomes blocked. (This spell works on creatures that can\'t be blocked.)\nDraw a card.').
card_mana_cost('curtain of light', ['1', 'W']).
card_cmc('curtain of light', 2).
card_layout('curtain of light', 'normal').

% Found in: CNS
card_name('custodi soulbinders', 'Custodi Soulbinders').
card_type('custodi soulbinders', 'Creature — Human Cleric').
card_types('custodi soulbinders', ['Creature']).
card_subtypes('custodi soulbinders', ['Human', 'Cleric']).
card_colors('custodi soulbinders', ['W']).
card_text('custodi soulbinders', 'Custodi Soulbinders enters the battlefield with X +1/+1 counters on it, where X is the number of other creatures on the battlefield.\n{2}{W}, Remove a +1/+1 counter from Custodi Soulbinders: Put a 1/1 white Spirit creature token with flying onto the battlefield.').
card_mana_cost('custodi soulbinders', ['3', 'W']).
card_cmc('custodi soulbinders', 4).
card_layout('custodi soulbinders', 'normal').
card_power('custodi soulbinders', 0).
card_toughness('custodi soulbinders', 0).

% Found in: CNS
card_name('custodi squire', 'Custodi Squire').
card_type('custodi squire', 'Creature — Spirit Cleric').
card_types('custodi squire', ['Creature']).
card_subtypes('custodi squire', ['Spirit', 'Cleric']).
card_colors('custodi squire', ['W']).
card_text('custodi squire', 'Flying\nWill of the council — When Custodi Squire enters the battlefield, starting with you, each player votes for an artifact, creature, or enchantment card in your graveyard. Return each card with the most votes or tied for most votes to your hand.').
card_mana_cost('custodi squire', ['4', 'W']).
card_cmc('custodi squire', 5).
card_layout('custodi squire', 'normal').
card_power('custodi squire', 3).
card_toughness('custodi squire', 3).

% Found in: DTK
card_name('custodian of the trove', 'Custodian of the Trove').
card_type('custodian of the trove', 'Artifact Creature — Golem').
card_types('custodian of the trove', ['Artifact', 'Creature']).
card_subtypes('custodian of the trove', ['Golem']).
card_colors('custodian of the trove', []).
card_text('custodian of the trove', 'Defender\nCustodian of the Trove enters the battlefield tapped.').
card_mana_cost('custodian of the trove', ['3']).
card_cmc('custodian of the trove', 3).
card_layout('custodian of the trove', 'normal').
card_power('custodian of the trove', 2).
card_toughness('custodian of the trove', 5).

% Found in: ONS
card_name('custody battle', 'Custody Battle').
card_type('custody battle', 'Enchantment — Aura').
card_types('custody battle', ['Enchantment']).
card_subtypes('custody battle', ['Aura']).
card_colors('custody battle', ['R']).
card_text('custody battle', 'Enchant creature\nEnchanted creature has \"At the beginning of your upkeep, target opponent gains control of this creature unless you sacrifice a land.\"').
card_mana_cost('custody battle', ['1', 'R']).
card_cmc('custody battle', 2).
card_layout('custody battle', 'normal').

% Found in: MMQ
card_name('customs depot', 'Customs Depot').
card_type('customs depot', 'Enchantment').
card_types('customs depot', ['Enchantment']).
card_subtypes('customs depot', []).
card_colors('customs depot', ['U']).
card_text('customs depot', 'Whenever you cast a creature spell, you may pay {1}. If you do, draw a card, then discard a card.').
card_mana_cost('customs depot', ['1', 'U']).
card_cmc('customs depot', 2).
card_layout('customs depot', 'normal').

% Found in: SOK
card_name('cut the earthly bond', 'Cut the Earthly Bond').
card_type('cut the earthly bond', 'Instant — Arcane').
card_types('cut the earthly bond', ['Instant']).
card_subtypes('cut the earthly bond', ['Arcane']).
card_colors('cut the earthly bond', ['U']).
card_text('cut the earthly bond', 'Return target enchanted permanent to its owner\'s hand.').
card_mana_cost('cut the earthly bond', ['U']).
card_cmc('cut the earthly bond', 1).
card_layout('cut the earthly bond', 'normal').

% Found in: CHK
card_name('cut the tethers', 'Cut the Tethers').
card_type('cut the tethers', 'Sorcery').
card_types('cut the tethers', ['Sorcery']).
card_subtypes('cut the tethers', []).
card_colors('cut the tethers', ['U']).
card_text('cut the tethers', 'For each Spirit, return it to its owner\'s hand unless that player pays {3}.').
card_mana_cost('cut the tethers', ['2', 'U', 'U']).
card_cmc('cut the tethers', 4).
card_layout('cut the tethers', 'normal').

% Found in: FUT
card_name('cutthroat il-dal', 'Cutthroat il-Dal').
card_type('cutthroat il-dal', 'Creature — Human Rogue').
card_types('cutthroat il-dal', ['Creature']).
card_subtypes('cutthroat il-dal', ['Human', 'Rogue']).
card_colors('cutthroat il-dal', ['B']).
card_text('cutthroat il-dal', 'Hellbent — Cutthroat il-Dal has shadow as long as you have no cards in hand. (It can block or be blocked by only creatures with shadow.)').
card_mana_cost('cutthroat il-dal', ['3', 'B']).
card_cmc('cutthroat il-dal', 4).
card_layout('cutthroat il-dal', 'normal').
card_power('cutthroat il-dal', 4).
card_toughness('cutthroat il-dal', 1).

% Found in: THS
card_name('cutthroat maneuver', 'Cutthroat Maneuver').
card_type('cutthroat maneuver', 'Instant').
card_types('cutthroat maneuver', ['Instant']).
card_subtypes('cutthroat maneuver', []).
card_colors('cutthroat maneuver', ['B']).
card_text('cutthroat maneuver', 'Up to two target creatures each get +1/+1 and gain lifelink until end of turn.').
card_mana_cost('cutthroat maneuver', ['3', 'B']).
card_cmc('cutthroat maneuver', 4).
card_layout('cutthroat maneuver', 'normal').

% Found in: MIR
card_name('cycle of life', 'Cycle of Life').
card_type('cycle of life', 'Enchantment').
card_types('cycle of life', ['Enchantment']).
card_subtypes('cycle of life', []).
card_colors('cycle of life', ['G']).
card_text('cycle of life', 'Return Cycle of Life to its owner\'s hand: Target creature you cast this turn has base power and toughness 0/1 until your next upkeep. At the beginning of your next upkeep, put a +1/+1 counter on that creature.').
card_mana_cost('cycle of life', ['1', 'G', 'G']).
card_cmc('cycle of life', 3).
card_layout('cycle of life', 'normal').
card_reserved('cycle of life').

% Found in: FUT
card_name('cyclical evolution', 'Cyclical Evolution').
card_type('cyclical evolution', 'Sorcery').
card_types('cyclical evolution', ['Sorcery']).
card_subtypes('cyclical evolution', []).
card_colors('cyclical evolution', ['G']).
card_text('cyclical evolution', 'Target creature gets +3/+3 until end of turn. Exile Cyclical Evolution with three time counters on it.\nSuspend 3—{2}{G} (Rather than cast this card from your hand, you may pay {2}{G} and exile it with three time counters on it. At the beginning of your upkeep, remove a time counter. When the last is removed, cast it without paying its mana cost.)').
card_mana_cost('cyclical evolution', ['3', 'G', 'G']).
card_cmc('cyclical evolution', 5).
card_layout('cyclical evolution', 'normal').

% Found in: ARN, CHR, ME4
card_name('cyclone', 'Cyclone').
card_type('cyclone', 'Enchantment').
card_types('cyclone', ['Enchantment']).
card_subtypes('cyclone', []).
card_colors('cyclone', ['G']).
card_text('cyclone', 'At the beginning of your upkeep, put a wind counter on Cyclone, then sacrifice Cyclone unless you pay {G} for each wind counter on it. If you pay, Cyclone deals damage equal to the number of wind counters on it to each creature and each player.').
card_mana_cost('cyclone', ['2', 'G', 'G']).
card_cmc('cyclone', 4).
card_layout('cyclone', 'normal').

% Found in: C14, RTR
card_name('cyclonic rift', 'Cyclonic Rift').
card_type('cyclonic rift', 'Instant').
card_types('cyclonic rift', ['Instant']).
card_subtypes('cyclonic rift', []).
card_colors('cyclonic rift', ['U']).
card_text('cyclonic rift', 'Return target nonland permanent you don\'t control to its owner\'s hand.\nOverload {6}{U} (You may cast this spell for its overload cost. If you do, change its text by replacing all instances of \"target\" with \"each.\")').
card_mana_cost('cyclonic rift', ['1', 'U']).
card_cmc('cyclonic rift', 2).
card_layout('cyclonic rift', 'normal').

% Found in: TSP
card_name('cyclopean giant', 'Cyclopean Giant').
card_type('cyclopean giant', 'Creature — Zombie Giant').
card_types('cyclopean giant', ['Creature']).
card_subtypes('cyclopean giant', ['Zombie', 'Giant']).
card_colors('cyclopean giant', ['B']).
card_text('cyclopean giant', 'When Cyclopean Giant dies, target land becomes a Swamp. Exile Cyclopean Giant.').
card_mana_cost('cyclopean giant', ['2', 'B', 'B']).
card_cmc('cyclopean giant', 4).
card_layout('cyclopean giant', 'normal').
card_power('cyclopean giant', 4).
card_toughness('cyclopean giant', 2).

% Found in: 4ED, LEG, ME4
card_name('cyclopean mummy', 'Cyclopean Mummy').
card_type('cyclopean mummy', 'Creature — Zombie').
card_types('cyclopean mummy', ['Creature']).
card_subtypes('cyclopean mummy', ['Zombie']).
card_colors('cyclopean mummy', ['B']).
card_text('cyclopean mummy', 'When Cyclopean Mummy dies, exile it.').
card_mana_cost('cyclopean mummy', ['1', 'B']).
card_cmc('cyclopean mummy', 2).
card_layout('cyclopean mummy', 'normal').
card_power('cyclopean mummy', 2).
card_toughness('cyclopean mummy', 1).

% Found in: RAV
card_name('cyclopean snare', 'Cyclopean Snare').
card_type('cyclopean snare', 'Artifact').
card_types('cyclopean snare', ['Artifact']).
card_subtypes('cyclopean snare', []).
card_colors('cyclopean snare', []).
card_text('cyclopean snare', '{3}, {T}: Tap target creature, then return Cyclopean Snare to its owner\'s hand.').
card_mana_cost('cyclopean snare', ['2']).
card_cmc('cyclopean snare', 2).
card_layout('cyclopean snare', 'normal').

% Found in: 2ED, CED, CEI, LEA, LEB, ME4
card_name('cyclopean tomb', 'Cyclopean Tomb').
card_type('cyclopean tomb', 'Artifact').
card_types('cyclopean tomb', ['Artifact']).
card_subtypes('cyclopean tomb', []).
card_colors('cyclopean tomb', []).
card_text('cyclopean tomb', '{2}, {T}: Put a mire counter on target non-Swamp land. That land is a Swamp for as long as it has a mire counter on it. Activate this ability only during your upkeep.\nWhen Cyclopean Tomb is put into a graveyard from the battlefield, at the beginning of each of your upkeeps for the rest of the game, remove all mire counters from a land that a mire counter was put onto with Cyclopean Tomb but that a mire counter has not been removed from with Cyclopean Tomb.').
card_mana_cost('cyclopean tomb', ['4']).
card_cmc('cyclopean tomb', 4).
card_layout('cyclopean tomb', 'normal').
card_reserved('cyclopean tomb').

% Found in: M11
card_name('cyclops gladiator', 'Cyclops Gladiator').
card_type('cyclops gladiator', 'Creature — Cyclops Warrior').
card_types('cyclops gladiator', ['Creature']).
card_subtypes('cyclops gladiator', ['Cyclops', 'Warrior']).
card_colors('cyclops gladiator', ['R']).
card_text('cyclops gladiator', 'Whenever Cyclops Gladiator attacks, you may have it deal damage equal to its power to target creature defending player controls. If you do, that creature deals damage equal to its power to Cyclops Gladiator.').
card_mana_cost('cyclops gladiator', ['1', 'R', 'R', 'R']).
card_cmc('cyclops gladiator', 4).
card_layout('cyclops gladiator', 'normal').
card_power('cyclops gladiator', 4).
card_toughness('cyclops gladiator', 4).

% Found in: JOU
card_name('cyclops of eternal fury', 'Cyclops of Eternal Fury').
card_type('cyclops of eternal fury', 'Enchantment Creature — Cyclops').
card_types('cyclops of eternal fury', ['Enchantment', 'Creature']).
card_subtypes('cyclops of eternal fury', ['Cyclops']).
card_colors('cyclops of eternal fury', ['R']).
card_text('cyclops of eternal fury', 'Creatures you control have haste.').
card_mana_cost('cyclops of eternal fury', ['4', 'R', 'R']).
card_cmc('cyclops of eternal fury', 6).
card_layout('cyclops of eternal fury', 'normal').
card_power('cyclops of eternal fury', 5).
card_toughness('cyclops of eternal fury', 3).

% Found in: BNG
card_name('cyclops of one-eyed pass', 'Cyclops of One-Eyed Pass').
card_type('cyclops of one-eyed pass', 'Creature — Cyclops').
card_types('cyclops of one-eyed pass', ['Creature']).
card_subtypes('cyclops of one-eyed pass', ['Cyclops']).
card_colors('cyclops of one-eyed pass', ['R']).
card_text('cyclops of one-eyed pass', '').
card_mana_cost('cyclops of one-eyed pass', ['2', 'R', 'R']).
card_cmc('cyclops of one-eyed pass', 4).
card_layout('cyclops of one-eyed pass', 'normal').
card_power('cyclops of one-eyed pass', 5).
card_toughness('cyclops of one-eyed pass', 2).

% Found in: M14
card_name('cyclops tyrant', 'Cyclops Tyrant').
card_type('cyclops tyrant', 'Creature — Cyclops').
card_types('cyclops tyrant', ['Creature']).
card_subtypes('cyclops tyrant', ['Cyclops']).
card_colors('cyclops tyrant', ['R']).
card_text('cyclops tyrant', 'Intimidate (This creature can\'t be blocked except by artifact creatures and/or creatures that share a color with it.)\nCyclops Tyrant can\'t block creatures with power 2 or less.').
card_mana_cost('cyclops tyrant', ['5', 'R']).
card_cmc('cyclops tyrant', 6).
card_layout('cyclops tyrant', 'normal').
card_power('cyclops tyrant', 3).
card_toughness('cyclops tyrant', 4).

% Found in: ALA
card_name('cylian elf', 'Cylian Elf').
card_type('cylian elf', 'Creature — Elf Scout').
card_types('cylian elf', ['Creature']).
card_subtypes('cylian elf', ['Elf', 'Scout']).
card_colors('cylian elf', ['G']).
card_text('cylian elf', '').
card_mana_cost('cylian elf', ['1', 'G']).
card_cmc('cylian elf', 2).
card_layout('cylian elf', 'normal').
card_power('cylian elf', 2).
card_toughness('cylian elf', 2).

% Found in: CON
card_name('cylian sunsinger', 'Cylian Sunsinger').
card_type('cylian sunsinger', 'Creature — Elf Shaman').
card_types('cylian sunsinger', ['Creature']).
card_subtypes('cylian sunsinger', ['Elf', 'Shaman']).
card_colors('cylian sunsinger', ['G']).
card_text('cylian sunsinger', '{R}{G}{W}: Cylian Sunsinger and each other creature with the same name as it get +3/+3 until end of turn.').
card_mana_cost('cylian sunsinger', ['1', 'G']).
card_cmc('cylian sunsinger', 2).
card_layout('cylian sunsinger', 'normal').
card_power('cylian sunsinger', 2).
card_toughness('cylian sunsinger', 2).

% Found in: SOM
card_name('cystbearer', 'Cystbearer').
card_type('cystbearer', 'Creature — Beast').
card_types('cystbearer', ['Creature']).
card_subtypes('cystbearer', ['Beast']).
card_colors('cystbearer', ['G']).
card_text('cystbearer', 'Infect (This creature deals damage to creatures in the form of -1/-1 counters and to players in the form of poison counters.)').
card_mana_cost('cystbearer', ['2', 'G']).
card_cmc('cystbearer', 3).
card_layout('cystbearer', 'normal').
card_power('cystbearer', 2).
card_toughness('cystbearer', 3).

% Found in: DIS
card_name('cytoplast manipulator', 'Cytoplast Manipulator').
card_type('cytoplast manipulator', 'Creature — Human Wizard Mutant').
card_types('cytoplast manipulator', ['Creature']).
card_subtypes('cytoplast manipulator', ['Human', 'Wizard', 'Mutant']).
card_colors('cytoplast manipulator', ['U']).
card_text('cytoplast manipulator', 'Graft 2 (This creature enters the battlefield with two +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{U}, {T}: Gain control of target creature with a +1/+1 counter on it for as long as Cytoplast Manipulator remains on the battlefield.').
card_mana_cost('cytoplast manipulator', ['2', 'U', 'U']).
card_cmc('cytoplast manipulator', 4).
card_layout('cytoplast manipulator', 'normal').
card_power('cytoplast manipulator', 0).
card_toughness('cytoplast manipulator', 0).

% Found in: DIS, MM2
card_name('cytoplast root-kin', 'Cytoplast Root-Kin').
card_type('cytoplast root-kin', 'Creature — Elemental Mutant').
card_types('cytoplast root-kin', ['Creature']).
card_subtypes('cytoplast root-kin', ['Elemental', 'Mutant']).
card_colors('cytoplast root-kin', ['G']).
card_text('cytoplast root-kin', 'Graft 4 (This creature enters the battlefield with four +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\nWhen Cytoplast Root-Kin enters the battlefield, put a +1/+1 counter on each other creature you control with a +1/+1 counter on it.\n{2}: Move a +1/+1 counter from target creature you control onto Cytoplast Root-Kin.').
card_mana_cost('cytoplast root-kin', ['2', 'G', 'G']).
card_cmc('cytoplast root-kin', 4).
card_layout('cytoplast root-kin', 'normal').
card_power('cytoplast root-kin', 0).
card_toughness('cytoplast root-kin', 0).

% Found in: DIS
card_name('cytoshape', 'Cytoshape').
card_type('cytoshape', 'Instant').
card_types('cytoshape', ['Instant']).
card_subtypes('cytoshape', []).
card_colors('cytoshape', ['U', 'G']).
card_text('cytoshape', 'Choose a nonlegendary creature on the battlefield. Target creature becomes a copy of that creature until end of turn.').
card_mana_cost('cytoshape', ['1', 'G', 'U']).
card_cmc('cytoshape', 3).
card_layout('cytoshape', 'normal').

% Found in: DIS
card_name('cytospawn shambler', 'Cytospawn Shambler').
card_type('cytospawn shambler', 'Creature — Elemental Mutant').
card_types('cytospawn shambler', ['Creature']).
card_subtypes('cytospawn shambler', ['Elemental', 'Mutant']).
card_colors('cytospawn shambler', ['G']).
card_text('cytospawn shambler', 'Graft 6 (This creature enters the battlefield with six +1/+1 counters on it. Whenever another creature enters the battlefield, you may move a +1/+1 counter from this creature onto it.)\n{G}: Target creature with a +1/+1 counter on it gains trample until end of turn.').
card_mana_cost('cytospawn shambler', ['6', 'G']).
card_cmc('cytospawn shambler', 7).
card_layout('cytospawn shambler', 'normal').
card_power('cytospawn shambler', 0).
card_toughness('cytospawn shambler', 0).

