% Card-specific data

% Found in: UNH
card_name('\"ach! hans, run!\"', '\"Ach! Hans, Run!\"').
card_type('\"ach! hans, run!\"', 'Enchantment').
card_types('\"ach! hans, run!\"', ['Enchantment']).
card_subtypes('\"ach! hans, run!\"', []).
card_colors('\"ach! hans, run!\"', ['R', 'G']).
card_text('\"ach! hans, run!\"', 'At the beginning of your upkeep, you may say \"Ach! Hans, run! It\'s the . . .\" and name a creature card. If you do, search your library for the named card, put it into play, then shuffle your library. That creature has haste. Remove it from the game at end of turn.').
card_mana_cost('\"ach! hans, run!\"', ['2', 'R', 'R', 'G', 'G']).
card_cmc('\"ach! hans, run!\"', 6).
card_layout('\"ach! hans, run!\"', 'normal').

% Found in: pCEL
card_name('1996 world champion', '1996 World Champion').
card_type('1996 world champion', 'Creature — Legend').
card_types('1996 world champion', ['Creature']).
card_subtypes('1996 world champion', ['Legend']).
card_colors('1996 world champion', ['W', 'U', 'B', 'R', 'G']).
card_text('1996 world champion', 'Cannot be the targets of spells or effects. World Champion has power and toughness equal to the life totals of target opponent. {0}: Discard your hand to search your library for 1996 World Champion and reveal it to all players. Shuffle your library and put 1996 World Champion on top of it. Use this ability only at the beginning of your upkeep, and only if 1996 World Champion is in your library.').
card_mana_cost('1996 world champion', ['W', 'U', 'B', 'R', 'G']).
card_cmc('1996 world champion', 5).
card_layout('1996 world champion', 'normal').
card_power('1996 world champion', '*').
card_toughness('1996 world champion', '*').

% Found in: UNH
card_name('_____', '_____').
card_type('_____', 'Creature — Shapeshifter').
card_types('_____', ['Creature']).
card_subtypes('_____', ['Shapeshifter']).
card_colors('_____', ['U']).
card_text('_____', '{1}: This card\'s name becomes the name of your choice. Play this ability anywhere, anytime.').
card_mana_cost('_____', ['1', 'U']).
card_cmc('_____', 2).
card_layout('_____', 'normal').
card_power('_____', 1).
card_toughness('_____', 1).

% Found in: LEG
card_name('ærathi berserker', 'Ærathi Berserker').
card_type('ærathi berserker', 'Creature — Human Berserker').
card_types('ærathi berserker', ['Creature']).
card_subtypes('ærathi berserker', ['Human', 'Berserker']).
card_colors('ærathi berserker', ['R']).
card_text('ærathi berserker', 'Rampage 3 (Whenever this creature becomes blocked, it gets +3/+3 until end of turn for each creature blocking it beyond the first.)').
card_mana_cost('ærathi berserker', ['2', 'R', 'R', 'R']).
card_cmc('ærathi berserker', 5).
card_layout('ærathi berserker', 'normal').
card_power('ærathi berserker', 2).
card_toughness('ærathi berserker', 4).

% Found in: DDM, M11, M12
card_name('æther adept', 'Æther Adept').
card_type('æther adept', 'Creature — Human Wizard').
card_types('æther adept', ['Creature']).
card_subtypes('æther adept', ['Human', 'Wizard']).
card_colors('æther adept', ['U']).
card_text('æther adept', 'When Æther Adept enters the battlefield, return target creature to its owner\'s hand.').
card_mana_cost('æther adept', ['1', 'U', 'U']).
card_cmc('æther adept', 3).
card_layout('æther adept', 'normal').
card_power('æther adept', 2).
card_toughness('æther adept', 2).

% Found in: NMS
card_name('æther barrier', 'Æther Barrier').
card_type('æther barrier', 'Enchantment').
card_types('æther barrier', ['Enchantment']).
card_subtypes('æther barrier', []).
card_colors('æther barrier', ['U']).
card_text('æther barrier', 'Whenever a player casts a creature spell, that player sacrifices a permanent unless he or she pays {1}.').
card_mana_cost('æther barrier', ['2', 'U']).
card_cmc('æther barrier', 3).
card_layout('æther barrier', 'normal').

% Found in: ODY
card_name('æther burst', 'Æther Burst').
card_type('æther burst', 'Instant').
card_types('æther burst', ['Instant']).
card_subtypes('æther burst', []).
card_colors('æther burst', ['U']).
card_text('æther burst', 'Return up to X target creatures to their owners\' hands, where X is one plus the number of cards named Æther Burst in all graveyards as you cast Æther Burst.').
card_mana_cost('æther burst', ['1', 'U']).
card_cmc('æther burst', 2).
card_layout('æther burst', 'normal').

% Found in: ONS
card_name('æther charge', 'Æther Charge').
card_type('æther charge', 'Enchantment').
card_types('æther charge', ['Enchantment']).
card_subtypes('æther charge', []).
card_colors('æther charge', ['R']).
card_text('æther charge', 'Whenever a Beast enters the battlefield under your control, you may have it deal 4 damage to target opponent.').
card_mana_cost('æther charge', ['4', 'R']).
card_cmc('æther charge', 5).
card_layout('æther charge', 'normal').

% Found in: DDM, ZEN
card_name('æther figment', 'Æther Figment').
card_type('æther figment', 'Creature — Illusion').
card_types('æther figment', ['Creature']).
card_subtypes('æther figment', ['Illusion']).
card_colors('æther figment', ['U']).
card_text('æther figment', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\nÆther Figment can\'t be blocked.\nIf Æther Figment was kicked, it enters the battlefield with two +1/+1 counters on it.').
card_mana_cost('æther figment', ['1', 'U']).
card_cmc('æther figment', 2).
card_layout('æther figment', 'normal').
card_power('æther figment', 1).
card_toughness('æther figment', 1).

% Found in: 6ED, 7ED, WTH
card_name('æther flash', 'Æther Flash').
card_type('æther flash', 'Enchantment').
card_types('æther flash', ['Enchantment']).
card_subtypes('æther flash', []).
card_colors('æther flash', ['R']).
card_text('æther flash', 'Whenever a creature enters the battlefield, Æther Flash deals 2 damage to it.').
card_mana_cost('æther flash', ['2', 'R', 'R']).
card_cmc('æther flash', 4).
card_layout('æther flash', 'normal').

% Found in: C14
card_name('æther gale', 'Æther Gale').
card_type('æther gale', 'Sorcery').
card_types('æther gale', ['Sorcery']).
card_subtypes('æther gale', []).
card_colors('æther gale', ['U']).
card_text('æther gale', 'Return six target nonland permanents to their owners\' hands.').
card_mana_cost('æther gale', ['3', 'U', 'U']).
card_cmc('æther gale', 5).
card_layout('æther gale', 'normal').

% Found in: DDI, PLC
card_name('æther membrane', 'Æther Membrane').
card_type('æther membrane', 'Creature — Wall').
card_types('æther membrane', ['Creature']).
card_subtypes('æther membrane', ['Wall']).
card_colors('æther membrane', ['R']).
card_text('æther membrane', 'Defender; reach (This creature can block creatures with flying.)\nWhenever Æther Membrane blocks a creature, return that creature to its owner\'s hand at end of combat.').
card_mana_cost('æther membrane', ['1', 'R', 'R']).
card_cmc('æther membrane', 3).
card_layout('æther membrane', 'normal').
card_power('æther membrane', 0).
card_toughness('æther membrane', 5).

% Found in: APC, VMA
card_name('æther mutation', 'Æther Mutation').
card_type('æther mutation', 'Sorcery').
card_types('æther mutation', ['Sorcery']).
card_subtypes('æther mutation', []).
card_colors('æther mutation', ['U', 'G']).
card_text('æther mutation', 'Return target creature to its owner\'s hand. Put X 1/1 green Saproling creature tokens onto the battlefield, where X is that creature\'s converted mana cost.').
card_mana_cost('æther mutation', ['3', 'G', 'U']).
card_cmc('æther mutation', 5).
card_layout('æther mutation', 'normal').

% Found in: INV
card_name('æther rift', 'Æther Rift').
card_type('æther rift', 'Enchantment').
card_types('æther rift', ['Enchantment']).
card_subtypes('æther rift', []).
card_colors('æther rift', ['R', 'G']).
card_text('æther rift', 'At the beginning of your upkeep, discard a card at random. If you discard a creature card this way, return it from your graveyard to the battlefield unless any player pays 5 life.').
card_mana_cost('æther rift', ['1', 'R', 'G']).
card_cmc('æther rift', 3).
card_layout('æther rift', 'normal').

% Found in: CNS
card_name('æther searcher', 'Æther Searcher').
card_type('æther searcher', 'Artifact Creature — Construct').
card_types('æther searcher', ['Artifact', 'Creature']).
card_subtypes('æther searcher', ['Construct']).
card_colors('æther searcher', []).
card_text('æther searcher', 'Reveal Æther Searcher as you draft it. Reveal the next card you draft and note its name. \nWhen Æther Searcher enters the battlefield, you may search your hand and/or library for a card with a name noted as you drafted cards named Æther Searcher. You may cast it without paying its mana cost. If you searched your library this way, shuffle it.').
card_mana_cost('æther searcher', ['7']).
card_cmc('æther searcher', 7).
card_layout('æther searcher', 'normal').
card_power('æther searcher', 6).
card_toughness('æther searcher', 4).

% Found in: SOK
card_name('æther shockwave', 'Æther Shockwave').
card_type('æther shockwave', 'Instant').
card_types('æther shockwave', ['Instant']).
card_subtypes('æther shockwave', []).
card_colors('æther shockwave', ['W']).
card_text('æther shockwave', 'Choose one —\n• Tap all Spirits.\n• Tap all non-Spirit creatures.').
card_mana_cost('æther shockwave', ['3', 'W']).
card_cmc('æther shockwave', 4).
card_layout('æther shockwave', 'normal').

% Found in: C14, DST
card_name('æther snap', 'Æther Snap').
card_type('æther snap', 'Sorcery').
card_types('æther snap', ['Sorcery']).
card_subtypes('æther snap', []).
card_colors('æther snap', ['B']).
card_text('æther snap', 'Remove all counters from all permanents and exile all tokens.').
card_mana_cost('æther snap', ['3', 'B', 'B']).
card_cmc('æther snap', 5).
card_layout('æther snap', 'normal').

% Found in: ARC, DDF, MMA, MRD
card_name('æther spellbomb', 'Æther Spellbomb').
card_type('æther spellbomb', 'Artifact').
card_types('æther spellbomb', ['Artifact']).
card_subtypes('æther spellbomb', []).
card_colors('æther spellbomb', []).
card_text('æther spellbomb', '{U}, Sacrifice Æther Spellbomb: Return target creature to its owner\'s hand.\n{1}, Sacrifice Æther Spellbomb: Draw a card.').
card_mana_cost('æther spellbomb', ['1']).
card_cmc('æther spellbomb', 1).
card_layout('æther spellbomb', 'normal').

% Found in: UDS
card_name('æther sting', 'Æther Sting').
card_type('æther sting', 'Enchantment').
card_types('æther sting', ['Enchantment']).
card_subtypes('æther sting', []).
card_colors('æther sting', ['R']).
card_text('æther sting', 'Whenever an opponent casts a creature spell, Æther Sting deals 1 damage to that player.').
card_mana_cost('æther sting', ['3', 'R']).
card_cmc('æther sting', 4).
card_layout('æther sting', 'normal').

% Found in: 5ED, HML, ME2
card_name('æther storm', 'Æther Storm').
card_type('æther storm', 'Enchantment').
card_types('æther storm', ['Enchantment']).
card_subtypes('æther storm', []).
card_colors('æther storm', ['U']).
card_text('æther storm', 'Creature spells can\'t be cast.\nPay 4 life: Destroy Æther Storm. It can\'t be regenerated. Any player may activate this ability.').
card_mana_cost('æther storm', ['3', 'U']).
card_cmc('æther storm', 4).
card_layout('æther storm', 'normal').

% Found in: EXO
card_name('æther tide', 'Æther Tide').
card_type('æther tide', 'Sorcery').
card_types('æther tide', ['Sorcery']).
card_subtypes('æther tide', []).
card_colors('æther tide', ['U']).
card_text('æther tide', 'As an additional cost to cast Æther Tide, discard X creature cards.\nReturn X target creatures to their owners\' hands.').
card_mana_cost('æther tide', ['X', 'U']).
card_cmc('æther tide', 1).
card_layout('æther tide', 'normal').

% Found in: CNS, WWK
card_name('æther tradewinds', 'Æther Tradewinds').
card_type('æther tradewinds', 'Instant').
card_types('æther tradewinds', ['Instant']).
card_subtypes('æther tradewinds', []).
card_colors('æther tradewinds', ['U']).
card_text('æther tradewinds', 'Return target permanent you control and target permanent you don\'t control to their owners\' hands.').
card_mana_cost('æther tradewinds', ['2', 'U']).
card_cmc('æther tradewinds', 3).
card_layout('æther tradewinds', 'normal').

% Found in: DST, MMA, V10
card_name('æther vial', 'Æther Vial').
card_type('æther vial', 'Artifact').
card_types('æther vial', ['Artifact']).
card_subtypes('æther vial', []).
card_colors('æther vial', []).
card_text('æther vial', 'At the beginning of your upkeep, you may put a charge counter on Æther Vial.\n{T}: You may put a creature card with converted mana cost equal to the number of charge counters on Æther Vial from your hand onto the battlefield.').
card_mana_cost('æther vial', ['1']).
card_cmc('æther vial', 1).
card_layout('æther vial', 'normal').

% Found in: TSP
card_name('æther web', 'Æther Web').
card_type('æther web', 'Enchantment — Aura').
card_types('æther web', ['Enchantment']).
card_subtypes('æther web', ['Aura']).
card_colors('æther web', ['G']).
card_text('æther web', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +1/+1, has reach, and can block creatures with shadow as though they didn\'t have shadow. (Creatures with reach can block creatures with flying.)').
card_mana_cost('æther web', ['1', 'G']).
card_cmc('æther web', 2).
card_layout('æther web', 'normal').

% Found in: TSP
card_name('ætherflame wall', 'Ætherflame Wall').
card_type('ætherflame wall', 'Creature — Wall').
card_types('ætherflame wall', ['Creature']).
card_subtypes('ætherflame wall', ['Wall']).
card_colors('ætherflame wall', ['R']).
card_text('ætherflame wall', 'Defender\nÆtherflame Wall can block creatures with shadow as though they didn\'t have shadow.\n{R}: Ætherflame Wall gets +1/+0 until end of turn.').
card_mana_cost('ætherflame wall', ['1', 'R']).
card_cmc('ætherflame wall', 2).
card_layout('ætherflame wall', 'normal').
card_power('ætherflame wall', 0).
card_toughness('ætherflame wall', 4).

% Found in: DDO, GTC
card_name('ætherize', 'Ætherize').
card_type('ætherize', 'Instant').
card_types('ætherize', ['Instant']).
card_subtypes('ætherize', []).
card_colors('ætherize', ['U']).
card_text('ætherize', 'Return all attacking creatures to their owner\'s hand.').
card_mana_cost('ætherize', ['3', 'U']).
card_cmc('ætherize', 4).
card_layout('ætherize', 'normal').

% Found in: DGM
card_name('ætherling', 'Ætherling').
card_type('ætherling', 'Creature — Shapeshifter').
card_types('ætherling', ['Creature']).
card_subtypes('ætherling', ['Shapeshifter']).
card_colors('ætherling', ['U']).
card_text('ætherling', '{U}: Exile Ætherling. Return it to the battlefield under its owner\'s control at the beginning of the next end step.\n{U}: Ætherling can\'t be blocked this turn.\n{1}: Ætherling gets +1/-1 until end of turn.\n{1}: Ætherling gets -1/+1 until end of turn.').
card_mana_cost('ætherling', ['4', 'U', 'U']).
card_cmc('ætherling', 6).
card_layout('ætherling', 'normal').
card_power('ætherling', 4).
card_toughness('ætherling', 5).

% Found in: C13, DIS
card_name('æthermage\'s touch', 'Æthermage\'s Touch').
card_type('æthermage\'s touch', 'Instant').
card_types('æthermage\'s touch', ['Instant']).
card_subtypes('æthermage\'s touch', []).
card_colors('æthermage\'s touch', ['W', 'U']).
card_text('æthermage\'s touch', 'Reveal the top four cards of your library. You may put a creature card from among them onto the battlefield. It gains \"At the beginning of your end step, return this creature to its owner\'s hand.\" Then put the rest of the cards revealed this way on the bottom of your library in any order.').
card_mana_cost('æthermage\'s touch', ['2', 'W', 'U']).
card_cmc('æthermage\'s touch', 4).
card_layout('æthermage\'s touch', 'normal').

% Found in: GPT
card_name('ætherplasm', 'Ætherplasm').
card_type('ætherplasm', 'Creature — Illusion').
card_types('ætherplasm', ['Creature']).
card_subtypes('ætherplasm', ['Illusion']).
card_colors('ætherplasm', ['U']).
card_text('ætherplasm', 'Whenever Ætherplasm blocks a creature, you may return Ætherplasm to its owner\'s hand. If you do, you may put a creature card from your hand onto the battlefield blocking that creature.').
card_mana_cost('ætherplasm', ['2', 'U', 'U']).
card_cmc('ætherplasm', 4).
card_layout('ætherplasm', 'normal').
card_power('ætherplasm', 1).
card_toughness('ætherplasm', 1).

% Found in: CMD, DD2, DD3_JVC, LRW, MM2, MMA
card_name('æthersnipe', 'Æthersnipe').
card_type('æthersnipe', 'Creature — Elemental').
card_types('æthersnipe', ['Creature']).
card_subtypes('æthersnipe', ['Elemental']).
card_colors('æthersnipe', ['U']).
card_text('æthersnipe', 'When Æthersnipe enters the battlefield, return target nonland permanent to its owner\'s hand.\nEvoke {1}{U}{U} (You may cast this spell for its evoke cost. If you do, it\'s sacrificed when it enters the battlefield.)').
card_mana_cost('æthersnipe', ['5', 'U']).
card_cmc('æthersnipe', 6).
card_layout('æthersnipe', 'normal').
card_power('æthersnipe', 4).
card_toughness('æthersnipe', 4).

% Found in: M15
card_name('ætherspouts', 'Ætherspouts').
card_type('ætherspouts', 'Instant').
card_types('ætherspouts', ['Instant']).
card_subtypes('ætherspouts', []).
card_colors('ætherspouts', ['U']).
card_text('ætherspouts', 'For each attacking creature, its owner puts it on the top or bottom of his or her library.').
card_mana_cost('ætherspouts', ['3', 'U', 'U']).
card_cmc('ætherspouts', 5).
card_layout('ætherspouts', 'normal').

% Found in: SHM
card_name('æthertow', 'Æthertow').
card_type('æthertow', 'Instant').
card_types('æthertow', ['Instant']).
card_subtypes('æthertow', []).
card_colors('æthertow', ['W', 'U']).
card_text('æthertow', 'Put target attacking or blocking creature on top of its owner\'s library.\nConspire (As you cast this spell, you may tap two untapped creatures you control that share a color with it. When you do, copy it and you may choose a new target for the copy.)').
card_mana_cost('æthertow', ['3', 'W/U']).
card_cmc('æthertow', 4).
card_layout('æthertow', 'normal').

