% Card-specific data

% Found in: CHK
card_name('uba mask', 'Uba Mask').
card_type('uba mask', 'Artifact').
card_types('uba mask', ['Artifact']).
card_subtypes('uba mask', []).
card_colors('uba mask', []).
card_text('uba mask', 'If a player would draw a card, that player exiles that card face up instead.\nEach player may play cards he or she exiled with Uba Mask this turn.').
card_mana_cost('uba mask', ['4']).
card_cmc('uba mask', 4).
card_layout('uba mask', 'normal').

% Found in: DGM
card_name('ubul sar gatekeepers', 'Ubul Sar Gatekeepers').
card_type('ubul sar gatekeepers', 'Creature — Zombie Soldier').
card_types('ubul sar gatekeepers', ['Creature']).
card_subtypes('ubul sar gatekeepers', ['Zombie', 'Soldier']).
card_colors('ubul sar gatekeepers', ['B']).
card_text('ubul sar gatekeepers', 'When Ubul Sar Gatekeepers enters the battlefield, if you control two or more Gates, target creature an opponent controls gets -2/-2 until end of turn.').
card_mana_cost('ubul sar gatekeepers', ['3', 'B']).
card_cmc('ubul sar gatekeepers', 4).
card_layout('ubul sar gatekeepers', 'normal').
card_power('ubul sar gatekeepers', 2).
card_toughness('ubul sar gatekeepers', 4).

% Found in: FRF, FRF_UGIN
card_name('ugin\'s construct', 'Ugin\'s Construct').
card_type('ugin\'s construct', 'Artifact Creature — Construct').
card_types('ugin\'s construct', ['Artifact', 'Creature']).
card_subtypes('ugin\'s construct', ['Construct']).
card_colors('ugin\'s construct', []).
card_text('ugin\'s construct', 'When Ugin\'s Construct enters the battlefield, sacrifice a permanent that\'s one or more colors.').
card_mana_cost('ugin\'s construct', ['4']).
card_cmc('ugin\'s construct', 4).
card_layout('ugin\'s construct', 'normal').
card_power('ugin\'s construct', 4).
card_toughness('ugin\'s construct', 5).

% Found in: BFZ
card_name('ugin\'s insight', 'Ugin\'s Insight').
card_type('ugin\'s insight', 'Sorcery').
card_types('ugin\'s insight', ['Sorcery']).
card_subtypes('ugin\'s insight', []).
card_colors('ugin\'s insight', ['U']).
card_text('ugin\'s insight', 'Scry X, where X is the highest converted mana cost among permanents you control, then draw three cards.').
card_mana_cost('ugin\'s insight', ['3', 'U', 'U']).
card_cmc('ugin\'s insight', 5).
card_layout('ugin\'s insight', 'normal').

% Found in: KTK
card_name('ugin\'s nexus', 'Ugin\'s Nexus').
card_type('ugin\'s nexus', 'Legendary Artifact').
card_types('ugin\'s nexus', ['Artifact']).
card_subtypes('ugin\'s nexus', []).
card_supertypes('ugin\'s nexus', ['Legendary']).
card_colors('ugin\'s nexus', []).
card_text('ugin\'s nexus', 'If a player would begin an extra turn, that player skips that turn instead.\nIf Ugin\'s Nexus would be put into a graveyard from the battlefield, instead exile it and take an extra turn after this one.').
card_mana_cost('ugin\'s nexus', ['5']).
card_cmc('ugin\'s nexus', 5).
card_layout('ugin\'s nexus', 'normal').

% Found in: FRF, FRF_UGIN
card_name('ugin, the spirit dragon', 'Ugin, the Spirit Dragon').
card_type('ugin, the spirit dragon', 'Planeswalker — Ugin').
card_types('ugin, the spirit dragon', ['Planeswalker']).
card_subtypes('ugin, the spirit dragon', ['Ugin']).
card_colors('ugin, the spirit dragon', []).
card_text('ugin, the spirit dragon', '+2: Ugin, the Spirit Dragon deals 3 damage to target creature or player.\n−X: Exile each permanent with converted mana cost X or less that\'s one or more colors.\n−10: You gain 7 life, draw seven cards, then put up to seven permanent cards from your hand onto the battlefield.').
card_mana_cost('ugin, the spirit dragon', ['8']).
card_cmc('ugin, the spirit dragon', 8).
card_layout('ugin, the spirit dragon', 'normal').
card_loyalty('ugin, the spirit dragon', 7).

% Found in: PLC
card_name('uktabi drake', 'Uktabi Drake').
card_type('uktabi drake', 'Creature — Drake').
card_types('uktabi drake', ['Creature']).
card_subtypes('uktabi drake', ['Drake']).
card_colors('uktabi drake', ['G']).
card_text('uktabi drake', 'Flying, haste\nEcho {1}{G}{G} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)').
card_mana_cost('uktabi drake', ['G']).
card_cmc('uktabi drake', 1).
card_layout('uktabi drake', 'normal').
card_power('uktabi drake', 2).
card_toughness('uktabi drake', 1).

% Found in: WTH
card_name('uktabi efreet', 'Uktabi Efreet').
card_type('uktabi efreet', 'Creature — Efreet').
card_types('uktabi efreet', ['Creature']).
card_subtypes('uktabi efreet', ['Efreet']).
card_colors('uktabi efreet', ['G']).
card_text('uktabi efreet', 'Cumulative upkeep {G} (At the beginning of your upkeep, put an age counter on this permanent, then sacrifice it unless you pay its upkeep cost for each age counter on it.)').
card_mana_cost('uktabi efreet', ['2', 'G', 'G']).
card_cmc('uktabi efreet', 4).
card_layout('uktabi efreet', 'normal').
card_power('uktabi efreet', 5).
card_toughness('uktabi efreet', 4).

% Found in: MIR
card_name('uktabi faerie', 'Uktabi Faerie').
card_type('uktabi faerie', 'Creature — Faerie').
card_types('uktabi faerie', ['Creature']).
card_subtypes('uktabi faerie', ['Faerie']).
card_colors('uktabi faerie', ['G']).
card_text('uktabi faerie', 'Flying\n{3}{G}, Sacrifice Uktabi Faerie: Destroy target artifact.').
card_mana_cost('uktabi faerie', ['1', 'G']).
card_cmc('uktabi faerie', 2).
card_layout('uktabi faerie', 'normal').
card_power('uktabi faerie', 1).
card_toughness('uktabi faerie', 1).

% Found in: UNH
card_name('uktabi kong', 'Uktabi Kong').
card_type('uktabi kong', 'Creature — Ape').
card_types('uktabi kong', ['Creature']).
card_subtypes('uktabi kong', ['Ape']).
card_colors('uktabi kong', ['G']).
card_text('uktabi kong', 'Trample\nWhen Uktabi Kong comes into play, destroy all artifacts.\nTap two untapped Apes you control: Put a 1/1 green Ape creature token into play.').
card_mana_cost('uktabi kong', ['5', 'G', 'G', 'G']).
card_cmc('uktabi kong', 8).
card_layout('uktabi kong', 'normal').
card_power('uktabi kong', 8).
card_toughness('uktabi kong', 8).

% Found in: 6ED, VIS, pARL
card_name('uktabi orangutan', 'Uktabi Orangutan').
card_type('uktabi orangutan', 'Creature — Ape').
card_types('uktabi orangutan', ['Creature']).
card_subtypes('uktabi orangutan', ['Ape']).
card_colors('uktabi orangutan', ['G']).
card_text('uktabi orangutan', 'When Uktabi Orangutan enters the battlefield, destroy target artifact.').
card_mana_cost('uktabi orangutan', ['2', 'G']).
card_cmc('uktabi orangutan', 3).
card_layout('uktabi orangutan', 'normal').
card_power('uktabi orangutan', 2).
card_toughness('uktabi orangutan', 2).

% Found in: 6ED, 7ED, MIR
card_name('uktabi wildcats', 'Uktabi Wildcats').
card_type('uktabi wildcats', 'Creature — Cat').
card_types('uktabi wildcats', ['Creature']).
card_subtypes('uktabi wildcats', ['Cat']).
card_colors('uktabi wildcats', ['G']).
card_text('uktabi wildcats', 'Uktabi Wildcats\'s power and toughness are each equal to the number of Forests you control.\n{G}, Sacrifice a Forest: Regenerate Uktabi Wildcats.').
card_mana_cost('uktabi wildcats', ['4', 'G']).
card_cmc('uktabi wildcats', 5).
card_layout('uktabi wildcats', 'normal').
card_power('uktabi wildcats', '*').
card_toughness('uktabi wildcats', '*').

% Found in: DTK
card_name('ukud cobra', 'Ukud Cobra').
card_type('ukud cobra', 'Creature — Snake').
card_types('ukud cobra', ['Creature']).
card_subtypes('ukud cobra', ['Snake']).
card_colors('ukud cobra', ['B']).
card_text('ukud cobra', 'Deathtouch').
card_mana_cost('ukud cobra', ['3', 'B']).
card_cmc('ukud cobra', 4).
card_layout('ukud cobra', 'normal').
card_power('ukud cobra', 2).
card_toughness('ukud cobra', 5).

% Found in: DDP, MM2, ROE
card_name('ulamog\'s crusher', 'Ulamog\'s Crusher').
card_type('ulamog\'s crusher', 'Creature — Eldrazi').
card_types('ulamog\'s crusher', ['Creature']).
card_subtypes('ulamog\'s crusher', ['Eldrazi']).
card_colors('ulamog\'s crusher', []).
card_text('ulamog\'s crusher', 'Annihilator 2 (Whenever this creature attacks, defending player sacrifices two permanents.)\nUlamog\'s Crusher attacks each turn if able.').
card_mana_cost('ulamog\'s crusher', ['8']).
card_cmc('ulamog\'s crusher', 8).
card_layout('ulamog\'s crusher', 'normal').
card_power('ulamog\'s crusher', 8).
card_toughness('ulamog\'s crusher', 8).

% Found in: BFZ
card_name('ulamog\'s despoiler', 'Ulamog\'s Despoiler').
card_type('ulamog\'s despoiler', 'Creature — Eldrazi Processor').
card_types('ulamog\'s despoiler', ['Creature']).
card_subtypes('ulamog\'s despoiler', ['Eldrazi', 'Processor']).
card_colors('ulamog\'s despoiler', []).
card_text('ulamog\'s despoiler', 'As Ulamog\'s Despoiler enters the battlefield, you may put two cards your opponents own from exile into their owners\' graveyards. If you do, Ulamog\'s Despoiler enters the battlefield with four +1/+1 counters on it.').
card_mana_cost('ulamog\'s despoiler', ['6']).
card_cmc('ulamog\'s despoiler', 6).
card_layout('ulamog\'s despoiler', 'normal').
card_power('ulamog\'s despoiler', 5).
card_toughness('ulamog\'s despoiler', 5).

% Found in: BFZ
card_name('ulamog\'s nullifier', 'Ulamog\'s Nullifier').
card_type('ulamog\'s nullifier', 'Creature — Eldrazi Processor').
card_types('ulamog\'s nullifier', ['Creature']).
card_subtypes('ulamog\'s nullifier', ['Eldrazi', 'Processor']).
card_colors('ulamog\'s nullifier', []).
card_text('ulamog\'s nullifier', 'Devoid (This card has no color.)\nFlash\nFlying\nWhen Ulamog\'s Nullifier enters the battlefield, you may put two cards your opponents own from exile into their owners\' graveyards. If you do, counter target spell.').
card_mana_cost('ulamog\'s nullifier', ['2', 'U', 'B']).
card_cmc('ulamog\'s nullifier', 4).
card_layout('ulamog\'s nullifier', 'normal').
card_power('ulamog\'s nullifier', 2).
card_toughness('ulamog\'s nullifier', 3).

% Found in: BFZ
card_name('ulamog\'s reclaimer', 'Ulamog\'s Reclaimer').
card_type('ulamog\'s reclaimer', 'Creature — Eldrazi Processor').
card_types('ulamog\'s reclaimer', ['Creature']).
card_subtypes('ulamog\'s reclaimer', ['Eldrazi', 'Processor']).
card_colors('ulamog\'s reclaimer', []).
card_text('ulamog\'s reclaimer', 'Devoid (This card has no color.)\nWhen Ulamog\'s Reclaimer enters the battlefield, you may put a card an opponent owns from exile into that player\'s graveyard. If you do, return target instant or sorcery card from your graveyard to your hand.').
card_mana_cost('ulamog\'s reclaimer', ['4', 'U']).
card_cmc('ulamog\'s reclaimer', 5).
card_layout('ulamog\'s reclaimer', 'normal').
card_power('ulamog\'s reclaimer', 2).
card_toughness('ulamog\'s reclaimer', 5).

% Found in: BFZ
card_name('ulamog, the ceaseless hunger', 'Ulamog, the Ceaseless Hunger').
card_type('ulamog, the ceaseless hunger', 'Legendary Creature — Eldrazi').
card_types('ulamog, the ceaseless hunger', ['Creature']).
card_subtypes('ulamog, the ceaseless hunger', ['Eldrazi']).
card_supertypes('ulamog, the ceaseless hunger', ['Legendary']).
card_colors('ulamog, the ceaseless hunger', []).
card_text('ulamog, the ceaseless hunger', 'When you cast Ulamog, the Ceaseless Hunger, exile two target permanents.\nIndestructible\nWhenever Ulamog attacks, defending player exiles the top twenty cards of his or her library.').
card_mana_cost('ulamog, the ceaseless hunger', ['10']).
card_cmc('ulamog, the ceaseless hunger', 10).
card_layout('ulamog, the ceaseless hunger', 'normal').
card_power('ulamog, the ceaseless hunger', 10).
card_toughness('ulamog, the ceaseless hunger', 10).

% Found in: MM2, ROE, V11
card_name('ulamog, the infinite gyre', 'Ulamog, the Infinite Gyre').
card_type('ulamog, the infinite gyre', 'Legendary Creature — Eldrazi').
card_types('ulamog, the infinite gyre', ['Creature']).
card_subtypes('ulamog, the infinite gyre', ['Eldrazi']).
card_supertypes('ulamog, the infinite gyre', ['Legendary']).
card_colors('ulamog, the infinite gyre', []).
card_text('ulamog, the infinite gyre', 'When you cast Ulamog, the Infinite Gyre, destroy target permanent.\nIndestructible\nAnnihilator 4 (Whenever this creature attacks, defending player sacrifices four permanents.)\nWhen Ulamog is put into a graveyard from anywhere, its owner shuffles his or her graveyard into his or her library.').
card_mana_cost('ulamog, the infinite gyre', ['11']).
card_cmc('ulamog, the infinite gyre', 11).
card_layout('ulamog, the infinite gyre', 'normal').
card_power('ulamog, the infinite gyre', 10).
card_toughness('ulamog, the infinite gyre', 10).

% Found in: GPT
card_name('ulasht, the hate seed', 'Ulasht, the Hate Seed').
card_type('ulasht, the hate seed', 'Legendary Creature — Hellion Hydra').
card_types('ulasht, the hate seed', ['Creature']).
card_subtypes('ulasht, the hate seed', ['Hellion', 'Hydra']).
card_supertypes('ulasht, the hate seed', ['Legendary']).
card_colors('ulasht, the hate seed', ['R', 'G']).
card_text('ulasht, the hate seed', 'Ulasht, the Hate Seed enters the battlefield with a +1/+1 counter on it for each other red creature you control and a +1/+1 counter on it for each other green creature you control.\n{1}, Remove a +1/+1 counter from Ulasht: Choose one —\n• Ulasht deals 1 damage to target creature.\n• Put a 1/1 green Saproling creature token onto the battlefield.').
card_mana_cost('ulasht, the hate seed', ['2', 'R', 'G']).
card_cmc('ulasht, the hate seed', 4).
card_layout('ulasht, the hate seed', 'normal').
card_power('ulasht, the hate seed', 0).
card_toughness('ulasht, the hate seed', 0).

% Found in: M15
card_name('ulcerate', 'Ulcerate').
card_type('ulcerate', 'Instant').
card_types('ulcerate', ['Instant']).
card_subtypes('ulcerate', []).
card_colors('ulcerate', ['B']).
card_text('ulcerate', 'Target creature gets -3/-3 until end of turn. You lose 3 life.').
card_mana_cost('ulcerate', ['B']).
card_cmc('ulcerate', 1).
card_layout('ulcerate', 'normal').

% Found in: DTK, RTR
card_name('ultimate price', 'Ultimate Price').
card_type('ultimate price', 'Instant').
card_types('ultimate price', ['Instant']).
card_subtypes('ultimate price', []).
card_colors('ultimate price', ['B']).
card_text('ultimate price', 'Destroy target monocolored creature.').
card_mana_cost('ultimate price', ['1', 'B']).
card_cmc('ultimate price', 2).
card_layout('ultimate price', 'normal').

% Found in: DKA
card_name('ulvenwald bear', 'Ulvenwald Bear').
card_type('ulvenwald bear', 'Creature — Bear').
card_types('ulvenwald bear', ['Creature']).
card_subtypes('ulvenwald bear', ['Bear']).
card_colors('ulvenwald bear', ['G']).
card_text('ulvenwald bear', 'Morbid — When Ulvenwald Bear enters the battlefield, if a creature died this turn, put two +1/+1 counters on target creature.').
card_mana_cost('ulvenwald bear', ['2', 'G']).
card_cmc('ulvenwald bear', 3).
card_layout('ulvenwald bear', 'normal').
card_power('ulvenwald bear', 2).
card_toughness('ulvenwald bear', 2).

% Found in: ISD
card_name('ulvenwald mystics', 'Ulvenwald Mystics').
card_type('ulvenwald mystics', 'Creature — Human Shaman Werewolf').
card_types('ulvenwald mystics', ['Creature']).
card_subtypes('ulvenwald mystics', ['Human', 'Shaman', 'Werewolf']).
card_colors('ulvenwald mystics', ['G']).
card_text('ulvenwald mystics', 'At the beginning of each upkeep, if no spells were cast last turn, transform Ulvenwald Mystics.').
card_mana_cost('ulvenwald mystics', ['2', 'G', 'G']).
card_cmc('ulvenwald mystics', 4).
card_layout('ulvenwald mystics', 'double-faced').
card_power('ulvenwald mystics', 3).
card_toughness('ulvenwald mystics', 3).
card_sides('ulvenwald mystics', 'ulvenwald primordials').

% Found in: ISD
card_name('ulvenwald primordials', 'Ulvenwald Primordials').
card_type('ulvenwald primordials', 'Creature — Werewolf').
card_types('ulvenwald primordials', ['Creature']).
card_subtypes('ulvenwald primordials', ['Werewolf']).
card_colors('ulvenwald primordials', ['G']).
card_text('ulvenwald primordials', '{G}: Regenerate Ulvenwald Primordials.\nAt the beginning of each upkeep, if a player cast two or more spells last turn, transform Ulvenwald Primordials.').
card_layout('ulvenwald primordials', 'double-faced').
card_power('ulvenwald primordials', 5).
card_toughness('ulvenwald primordials', 5).

% Found in: AVR
card_name('ulvenwald tracker', 'Ulvenwald Tracker').
card_type('ulvenwald tracker', 'Creature — Human Shaman').
card_types('ulvenwald tracker', ['Creature']).
card_subtypes('ulvenwald tracker', ['Human', 'Shaman']).
card_colors('ulvenwald tracker', ['G']).
card_text('ulvenwald tracker', '{1}{G}, {T}: Target creature you control fights another target creature. (Each deals damage equal to its power to the other.)').
card_mana_cost('ulvenwald tracker', ['G']).
card_cmc('ulvenwald tracker', 1).
card_layout('ulvenwald tracker', 'normal').
card_power('ulvenwald tracker', 1).
card_toughness('ulvenwald tracker', 1).

% Found in: ZEN
card_name('umara raptor', 'Umara Raptor').
card_type('umara raptor', 'Creature — Bird Ally').
card_types('umara raptor', ['Creature']).
card_subtypes('umara raptor', ['Bird', 'Ally']).
card_colors('umara raptor', ['U']).
card_text('umara raptor', 'Flying\nWhenever Umara Raptor or another Ally enters the battlefield under your control, you may put a +1/+1 counter on Umara Raptor.').
card_mana_cost('umara raptor', ['2', 'U']).
card_cmc('umara raptor', 3).
card_layout('umara raptor', 'normal').
card_power('umara raptor', 1).
card_toughness('umara raptor', 1).

% Found in: USG
card_name('umbilicus', 'Umbilicus').
card_type('umbilicus', 'Artifact').
card_types('umbilicus', ['Artifact']).
card_subtypes('umbilicus', []).
card_colors('umbilicus', []).
card_text('umbilicus', 'At the beginning of each player\'s upkeep, that player returns a permanent he or she controls to its owner\'s hand unless he or she pays 2 life.').
card_mana_cost('umbilicus', ['4']).
card_cmc('umbilicus', 4).
card_layout('umbilicus', 'normal').

% Found in: ROE
card_name('umbra mystic', 'Umbra Mystic').
card_type('umbra mystic', 'Creature — Human Wizard').
card_types('umbra mystic', ['Creature']).
card_subtypes('umbra mystic', ['Human', 'Wizard']).
card_colors('umbra mystic', ['W']).
card_text('umbra mystic', 'Auras attached to permanents you control have totem armor. (If an enchanted permanent you control would be destroyed, instead remove all damage from it and destroy an Aura attached to it.)').
card_mana_cost('umbra mystic', ['2', 'W']).
card_cmc('umbra mystic', 3).
card_layout('umbra mystic', 'normal').
card_power('umbra mystic', 2).
card_toughness('umbra mystic', 2).

% Found in: EVE
card_name('umbra stalker', 'Umbra Stalker').
card_type('umbra stalker', 'Creature — Elemental').
card_types('umbra stalker', ['Creature']).
card_subtypes('umbra stalker', ['Elemental']).
card_colors('umbra stalker', ['B']).
card_text('umbra stalker', 'Chroma — Umbra Stalker\'s power and toughness are each equal to the number of black mana symbols in the mana costs of cards in your graveyard.').
card_mana_cost('umbra stalker', ['4', 'B', 'B', 'B']).
card_cmc('umbra stalker', 7).
card_layout('umbra stalker', 'normal').
card_power('umbra stalker', '*').
card_toughness('umbra stalker', '*').

% Found in: SHM
card_name('umbral mantle', 'Umbral Mantle').
card_type('umbral mantle', 'Artifact — Equipment').
card_types('umbral mantle', ['Artifact']).
card_subtypes('umbral mantle', ['Equipment']).
card_colors('umbral mantle', []).
card_text('umbral mantle', 'Equipped creature has \"{3}, {Q}: This creature gets +2/+2 until end of turn.\" ({Q} is the untap symbol.)\nEquip {0}').
card_mana_cost('umbral mantle', ['3']).
card_cmc('umbral mantle', 3).
card_layout('umbral mantle', 'normal').

% Found in: BOK, pGPX
card_name('umezawa\'s jitte', 'Umezawa\'s Jitte').
card_type('umezawa\'s jitte', 'Legendary Artifact — Equipment').
card_types('umezawa\'s jitte', ['Artifact']).
card_subtypes('umezawa\'s jitte', ['Equipment']).
card_supertypes('umezawa\'s jitte', ['Legendary']).
card_colors('umezawa\'s jitte', []).
card_text('umezawa\'s jitte', 'Whenever equipped creature deals combat damage, put two charge counters on Umezawa\'s Jitte.\nRemove a charge counter from Umezawa\'s Jitte: Choose one —\n• Equipped creature gets +2/+2 until end of turn.\n• Target creature gets -1/-1 until end of turn.\n• You gain 2 life.\nEquip {2}').
card_mana_cost('umezawa\'s jitte', ['2']).
card_cmc('umezawa\'s jitte', 2).
card_layout('umezawa\'s jitte', 'normal').

% Found in: ARB, ARC
card_name('unbender tine', 'Unbender Tine').
card_type('unbender tine', 'Artifact').
card_types('unbender tine', ['Artifact']).
card_subtypes('unbender tine', []).
card_colors('unbender tine', ['W', 'U']).
card_text('unbender tine', '{T}: Untap another target permanent.').
card_mana_cost('unbender tine', ['2', 'W', 'U']).
card_cmc('unbender tine', 4).
card_layout('unbender tine', 'normal').

% Found in: FUT
card_name('unblinking bleb', 'Unblinking Bleb').
card_type('unblinking bleb', 'Creature — Illusion').
card_types('unblinking bleb', ['Creature']).
card_subtypes('unblinking bleb', ['Illusion']).
card_colors('unblinking bleb', ['U']).
card_text('unblinking bleb', 'Morph {2}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhenever Unblinking Bleb or another permanent is turned face up, you may scry 2. (Look at the top two cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)').
card_mana_cost('unblinking bleb', ['3', 'U']).
card_cmc('unblinking bleb', 4).
card_layout('unblinking bleb', 'normal').
card_power('unblinking bleb', 1).
card_toughness('unblinking bleb', 3).

% Found in: ISD
card_name('unbreathing horde', 'Unbreathing Horde').
card_type('unbreathing horde', 'Creature — Zombie').
card_types('unbreathing horde', ['Creature']).
card_subtypes('unbreathing horde', ['Zombie']).
card_colors('unbreathing horde', ['B']).
card_text('unbreathing horde', 'Unbreathing Horde enters the battlefield with a +1/+1 counter on it for each other Zombie you control and each Zombie card in your graveyard.\nIf Unbreathing Horde would be dealt damage, prevent that damage and remove a +1/+1 counter from it.').
card_mana_cost('unbreathing horde', ['2', 'B']).
card_cmc('unbreathing horde', 3).
card_layout('unbreathing horde', 'normal').
card_power('unbreathing horde', 0).
card_toughness('unbreathing horde', 0).

% Found in: SCG
card_name('unburden', 'Unburden').
card_type('unburden', 'Sorcery').
card_types('unburden', ['Sorcery']).
card_subtypes('unburden', []).
card_colors('unburden', ['B']).
card_text('unburden', 'Target player discards two cards.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('unburden', ['1', 'B', 'B']).
card_cmc('unburden', 3).
card_layout('unburden', 'normal').

% Found in: ISD
card_name('unburial rites', 'Unburial Rites').
card_type('unburial rites', 'Sorcery').
card_types('unburial rites', ['Sorcery']).
card_subtypes('unburial rites', []).
card_colors('unburial rites', ['B']).
card_text('unburial rites', 'Return target creature card from your graveyard to the battlefield.\nFlashback {3}{W} (You may cast this card from your graveyard for its flashback cost. Then exile it.)').
card_mana_cost('unburial rites', ['4', 'B']).
card_cmc('unburial rites', 5).
card_layout('unburial rites', 'normal').

% Found in: AVR
card_name('uncanny speed', 'Uncanny Speed').
card_type('uncanny speed', 'Instant').
card_types('uncanny speed', ['Instant']).
card_subtypes('uncanny speed', []).
card_colors('uncanny speed', ['R']).
card_text('uncanny speed', 'Target creature gets +3/+0 and gains haste until end of turn.').
card_mana_cost('uncanny speed', ['1', 'R']).
card_cmc('uncanny speed', 2).
card_layout('uncanny speed', 'normal').

% Found in: BOK
card_name('unchecked growth', 'Unchecked Growth').
card_type('unchecked growth', 'Instant — Arcane').
card_types('unchecked growth', ['Instant']).
card_subtypes('unchecked growth', ['Arcane']).
card_colors('unchecked growth', ['G']).
card_text('unchecked growth', 'Target creature gets +4/+4 until end of turn. If it\'s a Spirit, it gains trample until end of turn.').
card_mana_cost('unchecked growth', ['2', 'G']).
card_cmc('unchecked growth', 3).
card_layout('unchecked growth', 'normal').

% Found in: 4ED, DRK, TSB
card_name('uncle istvan', 'Uncle Istvan').
card_type('uncle istvan', 'Creature — Human').
card_types('uncle istvan', ['Creature']).
card_subtypes('uncle istvan', ['Human']).
card_colors('uncle istvan', ['B']).
card_text('uncle istvan', 'Prevent all damage that would be dealt to Uncle Istvan by creatures.').
card_mana_cost('uncle istvan', ['1', 'B', 'B', 'B']).
card_cmc('uncle istvan', 4).
card_layout('uncle istvan', 'normal').
card_power('uncle istvan', 1).
card_toughness('uncle istvan', 3).

% Found in: 10E, CHK, CNS
card_name('uncontrollable anger', 'Uncontrollable Anger').
card_type('uncontrollable anger', 'Enchantment — Aura').
card_types('uncontrollable anger', ['Enchantment']).
card_subtypes('uncontrollable anger', ['Aura']).
card_colors('uncontrollable anger', ['R']).
card_text('uncontrollable anger', 'Flash (You may cast this spell any time you could cast an instant.)\nEnchant creature\nEnchanted creature gets +2/+2 and attacks each turn if able.').
card_mana_cost('uncontrollable anger', ['2', 'R', 'R']).
card_cmc('uncontrollable anger', 4).
card_layout('uncontrollable anger', 'normal').

% Found in: SCG
card_name('uncontrolled infestation', 'Uncontrolled Infestation').
card_type('uncontrolled infestation', 'Enchantment — Aura').
card_types('uncontrolled infestation', ['Enchantment']).
card_subtypes('uncontrolled infestation', ['Aura']).
card_colors('uncontrolled infestation', ['R']).
card_text('uncontrolled infestation', 'Enchant nonbasic land\nWhen enchanted land becomes tapped, destroy it.').
card_mana_cost('uncontrolled infestation', ['1', 'R']).
card_cmc('uncontrolled infestation', 2).
card_layout('uncontrolled infestation', 'normal').

% Found in: DGM
card_name('uncovered clues', 'Uncovered Clues').
card_type('uncovered clues', 'Sorcery').
card_types('uncovered clues', ['Sorcery']).
card_subtypes('uncovered clues', []).
card_colors('uncovered clues', ['U']).
card_text('uncovered clues', 'Look at the top four cards of your library. You may reveal up to two instant and/or sorcery cards from among them and put the revealed cards into your hand. Put the rest on the bottom of your library in any order.').
card_mana_cost('uncovered clues', ['2', 'U']).
card_cmc('uncovered clues', 3).
card_layout('uncovered clues', 'normal').

% Found in: ISD
card_name('undead alchemist', 'Undead Alchemist').
card_type('undead alchemist', 'Creature — Zombie').
card_types('undead alchemist', ['Creature']).
card_subtypes('undead alchemist', ['Zombie']).
card_colors('undead alchemist', ['U']).
card_text('undead alchemist', 'If a Zombie you control would deal combat damage to a player, instead that player puts that many cards from the top of his or her library into his or her graveyard.\nWhenever a creature card is put into an opponent\'s graveyard from his or her library, exile that card and put a 2/2 black Zombie creature token onto the battlefield.').
card_mana_cost('undead alchemist', ['3', 'U']).
card_cmc('undead alchemist', 4).
card_layout('undead alchemist', 'normal').
card_power('undead alchemist', 4).
card_toughness('undead alchemist', 2).

% Found in: AVR
card_name('undead executioner', 'Undead Executioner').
card_type('undead executioner', 'Creature — Zombie').
card_types('undead executioner', ['Creature']).
card_subtypes('undead executioner', ['Zombie']).
card_colors('undead executioner', ['B']).
card_text('undead executioner', 'When Undead Executioner dies, you may have target creature get -2/-2 until end of turn.').
card_mana_cost('undead executioner', ['3', 'B']).
card_cmc('undead executioner', 4).
card_layout('undead executioner', 'normal').
card_power('undead executioner', 2).
card_toughness('undead executioner', 2).

% Found in: ONS
card_name('undead gladiator', 'Undead Gladiator').
card_type('undead gladiator', 'Creature — Zombie Barbarian').
card_types('undead gladiator', ['Creature']).
card_subtypes('undead gladiator', ['Zombie', 'Barbarian']).
card_colors('undead gladiator', ['B']).
card_text('undead gladiator', '{1}{B}, Discard a card: Return Undead Gladiator from your graveyard to your hand. Activate this ability only during your upkeep.\nCycling {1}{B} ({1}{B}, Discard this card: Draw a card.)').
card_mana_cost('undead gladiator', ['1', 'B', 'B']).
card_cmc('undead gladiator', 3).
card_layout('undead gladiator', 'normal').
card_power('undead gladiator', 3).
card_toughness('undead gladiator', 1).

% Found in: ALA
card_name('undead leotau', 'Undead Leotau').
card_type('undead leotau', 'Creature — Zombie Cat').
card_types('undead leotau', ['Creature']).
card_subtypes('undead leotau', ['Zombie', 'Cat']).
card_colors('undead leotau', ['B']).
card_text('undead leotau', '{R}: Undead Leotau gets +1/-1 until end of turn.\nUnearth {2}{B} ({2}{B}: Return this card from your graveyard to the battlefield. It gains haste. Exile it at the beginning of the next end step or if it would leave the battlefield. Unearth only as a sorcery.)').
card_mana_cost('undead leotau', ['5', 'B']).
card_cmc('undead leotau', 6).
card_layout('undead leotau', 'normal').
card_power('undead leotau', 3).
card_toughness('undead leotau', 4).

% Found in: M14
card_name('undead minotaur', 'Undead Minotaur').
card_type('undead minotaur', 'Creature — Zombie Minotaur').
card_types('undead minotaur', ['Creature']).
card_subtypes('undead minotaur', ['Zombie', 'Minotaur']).
card_colors('undead minotaur', ['B']).
card_text('undead minotaur', '').
card_mana_cost('undead minotaur', ['2', 'B']).
card_cmc('undead minotaur', 3).
card_layout('undead minotaur', 'normal').
card_power('undead minotaur', 2).
card_toughness('undead minotaur', 3).

% Found in: ORI
card_name('undead servant', 'Undead Servant').
card_type('undead servant', 'Creature — Zombie').
card_types('undead servant', ['Creature']).
card_subtypes('undead servant', ['Zombie']).
card_colors('undead servant', ['B']).
card_text('undead servant', 'When Undead Servant enters the battlefield, put a 2/2 black Zombie creature token onto the battlefield for each card named Undead Servant in your graveyard.').
card_mana_cost('undead servant', ['3', 'B']).
card_cmc('undead servant', 4).
card_layout('undead servant', 'normal').
card_power('undead servant', 3).
card_toughness('undead servant', 2).

% Found in: M10
card_name('undead slayer', 'Undead Slayer').
card_type('undead slayer', 'Creature — Human Cleric').
card_types('undead slayer', ['Creature']).
card_subtypes('undead slayer', ['Human', 'Cleric']).
card_colors('undead slayer', ['W']).
card_text('undead slayer', '{W}, {T}: Exile target Skeleton, Vampire, or Zombie.').
card_mana_cost('undead slayer', ['2', 'W']).
card_cmc('undead slayer', 3).
card_layout('undead slayer', 'normal').
card_power('undead slayer', 2).
card_toughness('undead slayer', 2).

% Found in: HOP, SCG, TSB
card_name('undead warchief', 'Undead Warchief').
card_type('undead warchief', 'Creature — Zombie').
card_types('undead warchief', ['Creature']).
card_subtypes('undead warchief', ['Zombie']).
card_colors('undead warchief', ['B']).
card_text('undead warchief', 'Zombie spells you cast cost {1} less to cast.\nZombie creatures you control get +2/+1.').
card_mana_cost('undead warchief', ['2', 'B', 'B']).
card_cmc('undead warchief', 4).
card_layout('undead warchief', 'normal').
card_power('undead warchief', 1).
card_toughness('undead warchief', 1).

% Found in: GTC
card_name('undercity informer', 'Undercity Informer').
card_type('undercity informer', 'Creature — Human Rogue').
card_types('undercity informer', ['Creature']).
card_subtypes('undercity informer', ['Human', 'Rogue']).
card_colors('undercity informer', ['B']).
card_text('undercity informer', '{1}, Sacrifice a creature: Target player reveals cards from the top of his or her library until he or she reveals a land card, then puts those cards into his or her graveyard.').
card_mana_cost('undercity informer', ['2', 'B']).
card_cmc('undercity informer', 3).
card_layout('undercity informer', 'normal').
card_power('undercity informer', 2).
card_toughness('undercity informer', 3).

% Found in: GTC
card_name('undercity plague', 'Undercity Plague').
card_type('undercity plague', 'Sorcery').
card_types('undercity plague', ['Sorcery']).
card_subtypes('undercity plague', []).
card_colors('undercity plague', ['B']).
card_text('undercity plague', 'Target player loses 1 life, discards a card, then sacrifices a permanent.\nCipher (Then you may exile this spell card encoded on a creature you control. Whenever that creature deals combat damage to a player, its controller may cast a copy of the encoded card without paying its mana cost.)').
card_mana_cost('undercity plague', ['4', 'B', 'B']).
card_cmc('undercity plague', 6).
card_layout('undercity plague', 'normal').

% Found in: HOP
card_name('undercity reaches', 'Undercity Reaches').
card_type('undercity reaches', 'Plane — Ravnica').
card_types('undercity reaches', ['Plane']).
card_subtypes('undercity reaches', ['Ravnica']).
card_colors('undercity reaches', []).
card_text('undercity reaches', 'Whenever a creature deals combat damage to a player, its controller may draw a card.\nWhenever you roll {C}, you have no maximum hand size for the rest of the game.').
card_layout('undercity reaches', 'plane').

% Found in: RAV
card_name('undercity shade', 'Undercity Shade').
card_type('undercity shade', 'Creature — Shade').
card_types('undercity shade', ['Creature']).
card_subtypes('undercity shade', ['Shade']).
card_colors('undercity shade', ['B']).
card_text('undercity shade', 'Fear (This creature can\'t be blocked except by artifact creatures and/or black creatures.)\n{B}: Undercity Shade gets +1/+1 until end of turn.').
card_mana_cost('undercity shade', ['4', 'B']).
card_cmc('undercity shade', 5).
card_layout('undercity shade', 'normal').
card_power('undercity shade', 1).
card_toughness('undercity shade', 1).

% Found in: ORI
card_name('undercity troll', 'Undercity Troll').
card_type('undercity troll', 'Creature — Troll').
card_types('undercity troll', ['Creature']).
card_subtypes('undercity troll', ['Troll']).
card_colors('undercity troll', ['G']).
card_text('undercity troll', 'Renown 1 (When this creature deals combat damage to a player, if it isn\'t renowned, put a +1/+1 counter on it and it becomes renowned.)\n{2}{G}: Regenerate Undercity Troll. (The next time this creature would be destroyed this turn, it isn\'t. Instead tap it, remove all damage from it, and remove it from combat.)').
card_mana_cost('undercity troll', ['1', 'G']).
card_cmc('undercity troll', 2).
card_layout('undercity troll', 'normal').
card_power('undercity troll', 2).
card_toughness('undercity troll', 2).

% Found in: 10E, 5ED, 6ED, 7ED, 9ED, DKM, ICE
card_name('underground river', 'Underground River').
card_type('underground river', 'Land').
card_types('underground river', ['Land']).
card_subtypes('underground river', []).
card_colors('underground river', []).
card_text('underground river', '{T}: Add {1} to your mana pool.\n{T}: Add {U} or {B} to your mana pool. Underground River deals 1 damage to you.').
card_layout('underground river', 'normal').

% Found in: 2ED, 3ED, CED, CEI, LEA, LEB, ME2, ME4, VMA
card_name('underground sea', 'Underground Sea').
card_type('underground sea', 'Land — Island Swamp').
card_types('underground sea', ['Land']).
card_subtypes('underground sea', ['Island', 'Swamp']).
card_colors('underground sea', []).
card_text('underground sea', '({T}: Add {U} or {B} to your mana pool.)').
card_layout('underground sea', 'normal').
card_reserved('underground sea').

% Found in: ALL
card_name('undergrowth', 'Undergrowth').
card_type('undergrowth', 'Instant').
card_types('undergrowth', ['Instant']).
card_subtypes('undergrowth', []).
card_colors('undergrowth', ['G']).
card_text('undergrowth', 'As an additional cost to cast Undergrowth, you may pay {2}{R}.\nPrevent all combat damage that would be dealt this turn. If its additional cost was paid, Undergrowth doesn\'t affect combat damage that would be dealt by red creatures.').
card_mana_cost('undergrowth', ['G']).
card_cmc('undergrowth', 1).
card_layout('undergrowth', 'normal').

% Found in: BFZ
card_name('undergrowth champion', 'Undergrowth Champion').
card_type('undergrowth champion', 'Creature — Elemental').
card_types('undergrowth champion', ['Creature']).
card_subtypes('undergrowth champion', ['Elemental']).
card_colors('undergrowth champion', ['G']).
card_text('undergrowth champion', 'If damage would be dealt to Undergrowth Champion while it has a +1/+1 counter on it, prevent that damage and remove a +1/+1 counter from Undergrowth Champion.\nLandfall — Whenever a land enters the battlefield under your control, put a +1/+1 counter on Undergrowth Champion.').
card_mana_cost('undergrowth champion', ['1', 'G', 'G']).
card_cmc('undergrowth champion', 3).
card_layout('undergrowth champion', 'normal').
card_power('undergrowth champion', 2).
card_toughness('undergrowth champion', 2).

% Found in: M15
card_name('undergrowth scavenger', 'Undergrowth Scavenger').
card_type('undergrowth scavenger', 'Creature — Fungus Horror').
card_types('undergrowth scavenger', ['Creature']).
card_subtypes('undergrowth scavenger', ['Fungus', 'Horror']).
card_colors('undergrowth scavenger', ['G']).
card_text('undergrowth scavenger', 'Undergrowth Scavenger enters the battlefield with a number of +1/+1 counters on it equal to the number of creature cards in all graveyards.').
card_mana_cost('undergrowth scavenger', ['3', 'G']).
card_cmc('undergrowth scavenger', 4).
card_layout('undergrowth scavenger', 'normal').
card_power('undergrowth scavenger', 0).
card_toughness('undergrowth scavenger', 0).

% Found in: DDH, INV
card_name('undermine', 'Undermine').
card_type('undermine', 'Instant').
card_types('undermine', ['Instant']).
card_subtypes('undermine', []).
card_colors('undermine', ['U', 'B']).
card_text('undermine', 'Counter target spell. Its controller loses 3 life.').
card_mana_cost('undermine', ['U', 'U', 'B']).
card_cmc('undermine', 3).
card_layout('undermine', 'normal').

% Found in: MMQ, TSB
card_name('undertaker', 'Undertaker').
card_type('undertaker', 'Creature — Human Spellshaper').
card_types('undertaker', ['Creature']).
card_subtypes('undertaker', ['Human', 'Spellshaper']).
card_colors('undertaker', ['B']).
card_text('undertaker', '{B}, {T}, Discard a card: Return target creature card from your graveyard to your hand.').
card_mana_cost('undertaker', ['1', 'B']).
card_cmc('undertaker', 2).
card_layout('undertaker', 'normal').
card_power('undertaker', 1).
card_toughness('undertaker', 1).

% Found in: LEG
card_name('undertow', 'Undertow').
card_type('undertow', 'Enchantment').
card_types('undertow', ['Enchantment']).
card_subtypes('undertow', []).
card_colors('undertow', ['U']).
card_text('undertow', 'Creatures with islandwalk can be blocked as though they didn\'t have islandwalk.').
card_mana_cost('undertow', ['2', 'U']).
card_cmc('undertow', 3).
card_layout('undertow', 'normal').

% Found in: THS
card_name('underworld cerberus', 'Underworld Cerberus').
card_type('underworld cerberus', 'Creature — Hound').
card_types('underworld cerberus', ['Creature']).
card_subtypes('underworld cerberus', ['Hound']).
card_colors('underworld cerberus', ['B', 'R']).
card_text('underworld cerberus', 'Underworld Cerberus can\'t be blocked except by three or more creatures.\nCards in graveyards can\'t be the targets of spells or abilities.\nWhen Underworld Cerberus dies, exile it and each player returns all creature cards from his or her graveyard to his or her hand.').
card_mana_cost('underworld cerberus', ['3', 'B', 'R']).
card_cmc('underworld cerberus', 5).
card_layout('underworld cerberus', 'normal').
card_power('underworld cerberus', 6).
card_toughness('underworld cerberus', 6).

% Found in: JOU
card_name('underworld coinsmith', 'Underworld Coinsmith').
card_type('underworld coinsmith', 'Enchantment Creature — Human Cleric').
card_types('underworld coinsmith', ['Enchantment', 'Creature']).
card_subtypes('underworld coinsmith', ['Human', 'Cleric']).
card_colors('underworld coinsmith', ['W', 'B']).
card_text('underworld coinsmith', 'Constellation — Whenever Underworld Coinsmith or another enchantment enters the battlefield under your control, you gain 1 life.\n{W}{B}, Pay 1 life: Each opponent loses 1 life.').
card_mana_cost('underworld coinsmith', ['W', 'B']).
card_cmc('underworld coinsmith', 2).
card_layout('underworld coinsmith', 'normal').
card_power('underworld coinsmith', 2).
card_toughness('underworld coinsmith', 2).

% Found in: DDM, RTR
card_name('underworld connections', 'Underworld Connections').
card_type('underworld connections', 'Enchantment — Aura').
card_types('underworld connections', ['Enchantment']).
card_subtypes('underworld connections', ['Aura']).
card_colors('underworld connections', ['B']).
card_text('underworld connections', 'Enchant land\nEnchanted land has \"{T}, Pay 1 life: Draw a card.\"').
card_mana_cost('underworld connections', ['1', 'B', 'B']).
card_cmc('underworld connections', 3).
card_layout('underworld connections', 'normal').

% Found in: 10E, 8ED, 9ED, DPA, LEG, M10, p2HG
card_name('underworld dreams', 'Underworld Dreams').
card_type('underworld dreams', 'Enchantment').
card_types('underworld dreams', ['Enchantment']).
card_subtypes('underworld dreams', []).
card_colors('underworld dreams', ['B']).
card_text('underworld dreams', 'Whenever an opponent draws a card, Underworld Dreams deals 1 damage to him or her.').
card_mana_cost('underworld dreams', ['B', 'B', 'B']).
card_cmc('underworld dreams', 3).
card_layout('underworld dreams', 'normal').

% Found in: VIS
card_name('undiscovered paradise', 'Undiscovered Paradise').
card_type('undiscovered paradise', 'Land').
card_types('undiscovered paradise', ['Land']).
card_subtypes('undiscovered paradise', []).
card_colors('undiscovered paradise', []).
card_text('undiscovered paradise', '{T}: Add one mana of any color to your mana pool. During your next untap step, as you untap your permanents, return Undiscovered Paradise to its owner\'s hand.').
card_layout('undiscovered paradise', 'normal').
card_reserved('undiscovered paradise').

% Found in: PO2, S99, VIS
card_name('undo', 'Undo').
card_type('undo', 'Sorcery').
card_types('undo', ['Sorcery']).
card_subtypes('undo', []).
card_colors('undo', ['U']).
card_text('undo', 'Return two target creatures to their owners\' hands.').
card_mana_cost('undo', ['1', 'U', 'U']).
card_cmc('undo', 3).
card_layout('undo', 'normal').

% Found in: POR
card_name('undying beast', 'Undying Beast').
card_type('undying beast', 'Creature — Beast').
card_types('undying beast', ['Creature']).
card_subtypes('undying beast', ['Beast']).
card_colors('undying beast', ['B']).
card_text('undying beast', 'When Undying Beast dies, put it on top of its owner\'s library.').
card_mana_cost('undying beast', ['3', 'B']).
card_cmc('undying beast', 4).
card_layout('undying beast', 'normal').
card_power('undying beast', 3).
card_toughness('undying beast', 2).

% Found in: DKA
card_name('undying evil', 'Undying Evil').
card_type('undying evil', 'Instant').
card_types('undying evil', ['Instant']).
card_subtypes('undying evil', []).
card_colors('undying evil', ['B']).
card_text('undying evil', 'Target creature gains undying until end of turn. (When it dies, if it had no +1/+1 counters on it, return it to the battlefield under its owner\'s control with a +1/+1 counter on it.)').
card_mana_cost('undying evil', ['B']).
card_cmc('undying evil', 1).
card_layout('undying evil', 'normal').

% Found in: SOK
card_name('undying flames', 'Undying Flames').
card_type('undying flames', 'Sorcery').
card_types('undying flames', ['Sorcery']).
card_subtypes('undying flames', []).
card_colors('undying flames', ['R']).
card_text('undying flames', 'Exile cards from the top of your library until you exile a nonland card. Undying Flames deals damage to target creature or player equal to that card\'s converted mana cost.\nEpic (For the rest of the game, you can\'t cast spells. At the beginning of each of your upkeeps, copy this spell except for its epic ability. You may choose a new target for the copy.)').
card_mana_cost('undying flames', ['4', 'R', 'R']).
card_cmc('undying flames', 6).
card_layout('undying flames', 'normal').

% Found in: DDL, TSP
card_name('undying rage', 'Undying Rage').
card_type('undying rage', 'Enchantment — Aura').
card_types('undying rage', ['Enchantment']).
card_subtypes('undying rage', ['Aura']).
card_colors('undying rage', ['R']).
card_text('undying rage', 'Enchant creature\nEnchanted creature gets +2/+2 and can\'t block.\nWhen Undying Rage is put into a graveyard from the battlefield, return Undying Rage to its owner\'s hand.').
card_mana_cost('undying rage', ['2', 'R']).
card_cmc('undying rage', 3).
card_layout('undying rage', 'normal').

% Found in: ULG
card_name('unearth', 'Unearth').
card_type('unearth', 'Sorcery').
card_types('unearth', ['Sorcery']).
card_subtypes('unearth', []).
card_colors('unearth', ['B']).
card_text('unearth', 'Return target creature card with converted mana cost 3 or less from your graveyard to the battlefield.\nCycling {2} ({2}, Discard this card: Draw a card.)').
card_mana_cost('unearth', ['B']).
card_cmc('unearth', 1).
card_layout('unearth', 'normal').

% Found in: CHK
card_name('unearthly blizzard', 'Unearthly Blizzard').
card_type('unearthly blizzard', 'Sorcery — Arcane').
card_types('unearthly blizzard', ['Sorcery']).
card_subtypes('unearthly blizzard', ['Arcane']).
card_colors('unearthly blizzard', ['R']).
card_text('unearthly blizzard', 'Up to three target creatures can\'t block this turn.').
card_mana_cost('unearthly blizzard', ['2', 'R']).
card_cmc('unearthly blizzard', 3).
card_layout('unearthly blizzard', 'normal').

% Found in: MIR
card_name('unerring sling', 'Unerring Sling').
card_type('unerring sling', 'Artifact').
card_types('unerring sling', ['Artifact']).
card_subtypes('unerring sling', []).
card_colors('unerring sling', []).
card_text('unerring sling', '{3}, {T}, Tap an untapped creature you control: Unerring Sling deals damage equal to the tapped creature\'s power to target attacking or blocking creature with flying.').
card_mana_cost('unerring sling', ['3']).
card_cmc('unerring sling', 3).
card_layout('unerring sling', 'normal').

% Found in: CNS
card_name('unexpected potential', 'Unexpected Potential').
card_type('unexpected potential', 'Conspiracy').
card_types('unexpected potential', ['Conspiracy']).
card_subtypes('unexpected potential', []).
card_colors('unexpected potential', []).
card_text('unexpected potential', 'Hidden agenda (Start the game with this conspiracy face down in the command zone and secretly name a card. You may turn this conspiracy face up any time and reveal the chosen name.)\nYou may spend mana as though it were mana of any color to cast spells with the chosen name.').
card_layout('unexpected potential', 'normal').

% Found in: GTC
card_name('unexpected results', 'Unexpected Results').
card_type('unexpected results', 'Sorcery').
card_types('unexpected results', ['Sorcery']).
card_subtypes('unexpected results', []).
card_colors('unexpected results', ['U', 'G']).
card_text('unexpected results', 'Shuffle your library, then reveal the top card. If it\'s a nonland card, you may cast it without paying its mana cost. If it\'s a land card, you may put it onto the battlefield and return Unexpected Results to its owner\'s hand.').
card_mana_cost('unexpected results', ['2', 'G', 'U']).
card_cmc('unexpected results', 4).
card_layout('unexpected results', 'normal').

% Found in: C13
card_name('unexpectedly absent', 'Unexpectedly Absent').
card_type('unexpectedly absent', 'Instant').
card_types('unexpectedly absent', ['Instant']).
card_subtypes('unexpectedly absent', []).
card_colors('unexpectedly absent', ['W']).
card_text('unexpectedly absent', 'Put target nonland permanent into its owner\'s library just beneath the top X cards of that library.').
card_mana_cost('unexpectedly absent', ['X', 'W', 'W']).
card_cmc('unexpectedly absent', 2).
card_layout('unexpectedly absent', 'normal').

% Found in: DGM
card_name('unflinching courage', 'Unflinching Courage').
card_type('unflinching courage', 'Enchantment — Aura').
card_types('unflinching courage', ['Enchantment']).
card_subtypes('unflinching courage', ['Aura']).
card_colors('unflinching courage', ['W', 'G']).
card_text('unflinching courage', 'Enchant creature\nEnchanted creature gets +2/+2 and has trample and lifelink.').
card_mana_cost('unflinching courage', ['1', 'G', 'W']).
card_cmc('unflinching courage', 3).
card_layout('unflinching courage', 'normal').

% Found in: DST
card_name('unforge', 'Unforge').
card_type('unforge', 'Instant').
card_types('unforge', ['Instant']).
card_subtypes('unforge', []).
card_colors('unforge', ['R']).
card_text('unforge', 'Destroy target Equipment. If that Equipment was attached to a creature, Unforge deals 2 damage to that creature.').
card_mana_cost('unforge', ['2', 'R']).
card_cmc('unforge', 3).
card_layout('unforge', 'normal').

% Found in: MIR
card_name('unfulfilled desires', 'Unfulfilled Desires').
card_type('unfulfilled desires', 'Enchantment').
card_types('unfulfilled desires', ['Enchantment']).
card_subtypes('unfulfilled desires', []).
card_colors('unfulfilled desires', ['U', 'B']).
card_text('unfulfilled desires', '{1}, Pay 1 life: Draw a card, then discard a card.').
card_mana_cost('unfulfilled desires', ['1', 'U', 'B']).
card_cmc('unfulfilled desires', 3).
card_layout('unfulfilled desires', 'normal').
card_reserved('unfulfilled desires').

% Found in: DKA
card_name('unhallowed cathar', 'Unhallowed Cathar').
card_type('unhallowed cathar', 'Creature — Zombie Soldier').
card_types('unhallowed cathar', ['Creature']).
card_subtypes('unhallowed cathar', ['Zombie', 'Soldier']).
card_colors('unhallowed cathar', ['B']).
card_text('unhallowed cathar', 'Unhallowed Cathar can\'t block.').
card_layout('unhallowed cathar', 'double-faced').
card_power('unhallowed cathar', 2).
card_toughness('unhallowed cathar', 1).

% Found in: AVR, CNS
card_name('unhallowed pact', 'Unhallowed Pact').
card_type('unhallowed pact', 'Enchantment — Aura').
card_types('unhallowed pact', ['Enchantment']).
card_subtypes('unhallowed pact', ['Aura']).
card_colors('unhallowed pact', ['B']).
card_text('unhallowed pact', 'Enchant creature\nWhen enchanted creature dies, return that card to the battlefield under your control.').
card_mana_cost('unhallowed pact', ['2', 'B']).
card_cmc('unhallowed pact', 3).
card_layout('unhallowed pact', 'normal').

% Found in: TOR
card_name('unhinge', 'Unhinge').
card_type('unhinge', 'Sorcery').
card_types('unhinge', ['Sorcery']).
card_subtypes('unhinge', []).
card_colors('unhinge', ['B']).
card_text('unhinge', 'Target player discards a card.\nDraw a card.').
card_mana_cost('unhinge', ['2', 'B']).
card_cmc('unhinge', 3).
card_layout('unhinge', 'normal').

% Found in: LEG
card_name('unholy citadel', 'Unholy Citadel').
card_type('unholy citadel', 'Land').
card_types('unholy citadel', ['Land']).
card_subtypes('unholy citadel', []).
card_colors('unholy citadel', []).
card_text('unholy citadel', 'Black legendary creatures you control have \"bands with other legendary creatures.\" (Any legendary creatures can attack in a band as long as at least one has \"bands with other legendary creatures.\" Bands are blocked as a group. If at least two legendary creatures you control, one of which has \"bands with other legendary creatures,\" are blocking or being blocked by the same creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_layout('unholy citadel', 'normal').

% Found in: ISD
card_name('unholy fiend', 'Unholy Fiend').
card_type('unholy fiend', 'Creature — Horror').
card_types('unholy fiend', ['Creature']).
card_subtypes('unholy fiend', ['Horror']).
card_colors('unholy fiend', ['B']).
card_text('unholy fiend', 'At the beginning of your end step, you lose 1 life.').
card_layout('unholy fiend', 'double-faced').
card_power('unholy fiend', 3).
card_toughness('unholy fiend', 3).

% Found in: ONS
card_name('unholy grotto', 'Unholy Grotto').
card_type('unholy grotto', 'Land').
card_types('unholy grotto', ['Land']).
card_subtypes('unholy grotto', []).
card_colors('unholy grotto', []).
card_text('unholy grotto', '{T}: Add {1} to your mana pool.\n{B}, {T}: Put target Zombie card from your graveyard on top of your library.').
card_layout('unholy grotto', 'normal').

% Found in: ORI
card_name('unholy hunger', 'Unholy Hunger').
card_type('unholy hunger', 'Instant').
card_types('unholy hunger', ['Instant']).
card_subtypes('unholy hunger', []).
card_colors('unholy hunger', ['B']).
card_text('unholy hunger', 'Destroy target creature.\nSpell mastery — If there are two or more instant and/or sorcery cards in your graveyard, you gain 2 life.').
card_mana_cost('unholy hunger', ['3', 'B', 'B']).
card_cmc('unholy hunger', 5).
card_layout('unholy hunger', 'normal').

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 7ED, 8ED, 9ED, ATH, CED, CEI, DD3_DVD, DDC, DPA, LEA, LEB, M10, M11
card_name('unholy strength', 'Unholy Strength').
card_type('unholy strength', 'Enchantment — Aura').
card_types('unholy strength', ['Enchantment']).
card_subtypes('unholy strength', ['Aura']).
card_colors('unholy strength', ['B']).
card_text('unholy strength', 'Enchant creature\nEnchanted creature gets +2/+1.').
card_mana_cost('unholy strength', ['B']).
card_cmc('unholy strength', 1).
card_layout('unholy strength', 'normal').

% Found in: BFZ
card_name('unified front', 'Unified Front').
card_type('unified front', 'Sorcery').
card_types('unified front', ['Sorcery']).
card_subtypes('unified front', []).
card_colors('unified front', ['W']).
card_text('unified front', 'Converge — Put a 1/1 white Kor Ally creature token onto the battlefield for each color of mana spent to cast Unified Front.').
card_mana_cost('unified front', ['3', 'W']).
card_cmc('unified front', 4).
card_layout('unified front', 'normal').

% Found in: ONS
card_name('unified strike', 'Unified Strike').
card_type('unified strike', 'Instant').
card_types('unified strike', ['Instant']).
card_subtypes('unified strike', []).
card_colors('unified strike', ['W']).
card_text('unified strike', 'Exile target attacking creature if its power is less than or equal to the number of Soldiers on the battlefield.').
card_mana_cost('unified strike', ['W']).
card_cmc('unified strike', 1).
card_layout('unified strike', 'normal').

% Found in: ROE
card_name('unified will', 'Unified Will').
card_type('unified will', 'Instant').
card_types('unified will', ['Instant']).
card_subtypes('unified will', []).
card_colors('unified will', ['U']).
card_text('unified will', 'Counter target spell if you control more creatures than that spell\'s controller.').
card_mana_cost('unified will', ['1', 'U']).
card_cmc('unified will', 2).
card_layout('unified will', 'normal').

% Found in: ODY
card_name('unifying theory', 'Unifying Theory').
card_type('unifying theory', 'Enchantment').
card_types('unifying theory', ['Enchantment']).
card_subtypes('unifying theory', []).
card_colors('unifying theory', ['U']).
card_text('unifying theory', 'Whenever a player casts a spell, that player may pay {2}. If the player does, he or she draws a card.').
card_mana_cost('unifying theory', ['1', 'U']).
card_cmc('unifying theory', 2).
card_layout('unifying theory', 'normal').

% Found in: THS
card_name('unknown shores', 'Unknown Shores').
card_type('unknown shores', 'Land').
card_types('unknown shores', ['Land']).
card_subtypes('unknown shores', []).
card_colors('unknown shores', []).
card_text('unknown shores', '{T}: Add {1} to your mana pool.\n{1}, {T}: Add one mana of any color to your mana pool.').
card_layout('unknown shores', 'normal').

% Found in: ALL
card_name('unlikely alliance', 'Unlikely Alliance').
card_type('unlikely alliance', 'Enchantment').
card_types('unlikely alliance', ['Enchantment']).
card_subtypes('unlikely alliance', []).
card_colors('unlikely alliance', ['W']).
card_text('unlikely alliance', '{1}{W}: Target nonattacking, nonblocking creature gets +0/+2 until end of turn.').
card_mana_cost('unlikely alliance', ['1', 'W']).
card_cmc('unlikely alliance', 2).
card_layout('unlikely alliance', 'normal').

% Found in: DIS
card_name('unliving psychopath', 'Unliving Psychopath').
card_type('unliving psychopath', 'Creature — Zombie Assassin').
card_types('unliving psychopath', ['Creature']).
card_subtypes('unliving psychopath', ['Zombie', 'Assassin']).
card_colors('unliving psychopath', ['B']).
card_text('unliving psychopath', '{B}: Unliving Psychopath gets +1/-1 until end of turn.\n{B}, {T}: Destroy target creature with power less than Unliving Psychopath\'s power.').
card_mana_cost('unliving psychopath', ['2', 'B', 'B']).
card_cmc('unliving psychopath', 4).
card_layout('unliving psychopath', 'normal').
card_power('unliving psychopath', 0).
card_toughness('unliving psychopath', 4).

% Found in: ARC, DDK, EVE, pMPR
card_name('unmake', 'Unmake').
card_type('unmake', 'Instant').
card_types('unmake', ['Instant']).
card_subtypes('unmake', []).
card_colors('unmake', ['W', 'B']).
card_text('unmake', 'Exile target creature.').
card_mana_cost('unmake', ['W/B', 'W/B', 'W/B']).
card_cmc('unmake', 3).
card_layout('unmake', 'normal').

% Found in: M15
card_name('unmake the graves', 'Unmake the Graves').
card_type('unmake the graves', 'Instant').
card_types('unmake the graves', ['Instant']).
card_subtypes('unmake the graves', []).
card_colors('unmake the graves', ['B']).
card_text('unmake the graves', 'Convoke (Your creatures can help cast this spell. Each creature you tap while casting this spell pays for {1} or one mana of that creature\'s color.)\nReturn up to two target creature cards from your graveyard to your hand.').
card_mana_cost('unmake the graves', ['4', 'B']).
card_cmc('unmake the graves', 5).
card_layout('unmake the graves', 'normal').

% Found in: MMQ
card_name('unmask', 'Unmask').
card_type('unmask', 'Sorcery').
card_types('unmask', ['Sorcery']).
card_subtypes('unmask', []).
card_colors('unmask', ['B']).
card_text('unmask', 'You may exile a black card from your hand rather than pay Unmask\'s mana cost.\nTarget player reveals his or her hand. You choose a nonland card from it. That player discards that card.').
card_mana_cost('unmask', ['3', 'B']).
card_cmc('unmask', 4).
card_layout('unmask', 'normal').

% Found in: BFZ
card_name('unnatural aggression', 'Unnatural Aggression').
card_type('unnatural aggression', 'Instant').
card_types('unnatural aggression', ['Instant']).
card_subtypes('unnatural aggression', []).
card_colors('unnatural aggression', []).
card_text('unnatural aggression', 'Devoid (This card has no color.)\nTarget creature you control fights target creature an opponent controls. If the creature an opponent controls would die this turn, exile it instead.').
card_mana_cost('unnatural aggression', ['2', 'G']).
card_cmc('unnatural aggression', 3).
card_layout('unnatural aggression', 'normal').

% Found in: MMQ
card_name('unnatural hunger', 'Unnatural Hunger').
card_type('unnatural hunger', 'Enchantment — Aura').
card_types('unnatural hunger', ['Enchantment']).
card_subtypes('unnatural hunger', ['Aura']).
card_colors('unnatural hunger', ['B']).
card_text('unnatural hunger', 'Enchant creature\nAt the beginning of the upkeep of enchanted creature\'s controller, Unnatural Hunger deals damage to that player equal to that creature\'s power unless he or she sacrifices another creature.').
card_mana_cost('unnatural hunger', ['3', 'B', 'B']).
card_cmc('unnatural hunger', 5).
card_layout('unnatural hunger', 'normal').

% Found in: MBS
card_name('unnatural predation', 'Unnatural Predation').
card_type('unnatural predation', 'Instant').
card_types('unnatural predation', ['Instant']).
card_subtypes('unnatural predation', []).
card_colors('unnatural predation', ['G']).
card_text('unnatural predation', 'Target creature gets +1/+1 and gains trample until end of turn.').
card_mana_cost('unnatural predation', ['G']).
card_cmc('unnatural predation', 1).
card_layout('unnatural predation', 'normal').

% Found in: APC
card_name('unnatural selection', 'Unnatural Selection').
card_type('unnatural selection', 'Enchantment').
card_types('unnatural selection', ['Enchantment']).
card_subtypes('unnatural selection', []).
card_colors('unnatural selection', ['U']).
card_text('unnatural selection', '{1}: Choose a creature type other than Wall. Target creature becomes that type until end of turn.').
card_mana_cost('unnatural selection', ['1', 'U']).
card_cmc('unnatural selection', 2).
card_layout('unnatural selection', 'normal').

% Found in: CHK
card_name('unnatural speed', 'Unnatural Speed').
card_type('unnatural speed', 'Instant — Arcane').
card_types('unnatural speed', ['Instant']).
card_subtypes('unnatural speed', ['Arcane']).
card_colors('unnatural speed', ['R']).
card_text('unnatural speed', 'Target creature gains haste until end of turn.').
card_mana_cost('unnatural speed', ['R']).
card_cmc('unnatural speed', 1).
card_layout('unnatural speed', 'normal').

% Found in: BRB, CMD, USG
card_name('unnerve', 'Unnerve').
card_type('unnerve', 'Sorcery').
card_types('unnerve', ['Sorcery']).
card_subtypes('unnerve', []).
card_colors('unnerve', ['B']).
card_text('unnerve', 'Each opponent discards two cards.').
card_mana_cost('unnerve', ['3', 'B']).
card_cmc('unnerve', 4).
card_layout('unnerve', 'normal').

% Found in: EVE
card_name('unnerving assault', 'Unnerving Assault').
card_type('unnerving assault', 'Instant').
card_types('unnerving assault', ['Instant']).
card_subtypes('unnerving assault', []).
card_colors('unnerving assault', ['U', 'R']).
card_text('unnerving assault', 'Creatures your opponents control get -1/-0 until end of turn if {U} was spent to cast Unnerving Assault, and creatures you control get +1/+0 until end of turn if {R} was spent to cast it. (Do both if {U}{R} was spent.)').
card_mana_cost('unnerving assault', ['2', 'U/R']).
card_cmc('unnerving assault', 3).
card_layout('unnerving assault', 'normal').

% Found in: CNS, JUD
card_name('unquestioned authority', 'Unquestioned Authority').
card_type('unquestioned authority', 'Enchantment — Aura').
card_types('unquestioned authority', ['Enchantment']).
card_subtypes('unquestioned authority', ['Aura']).
card_colors('unquestioned authority', ['W']).
card_text('unquestioned authority', 'Enchant creature\nWhen Unquestioned Authority enters the battlefield, draw a card.\nEnchanted creature has protection from creatures.').
card_mana_cost('unquestioned authority', ['2', 'W']).
card_cmc('unquestioned authority', 3).
card_layout('unquestioned authority', 'normal').

% Found in: BNG
card_name('unravel the æther', 'Unravel the Æther').
card_type('unravel the æther', 'Instant').
card_types('unravel the æther', ['Instant']).
card_subtypes('unravel the æther', []).
card_colors('unravel the æther', ['G']).
card_text('unravel the æther', 'Choose target artifact or enchantment. Its owner shuffles it into his or her library.').
card_mana_cost('unravel the æther', ['1', 'G']).
card_cmc('unravel the æther', 2).
card_layout('unravel the æther', 'normal').

% Found in: ISD
card_name('unruly mob', 'Unruly Mob').
card_type('unruly mob', 'Creature — Human').
card_types('unruly mob', ['Creature']).
card_subtypes('unruly mob', ['Human']).
card_colors('unruly mob', ['W']).
card_text('unruly mob', 'Whenever another creature you control dies, put a +1/+1 counter on Unruly Mob.').
card_mana_cost('unruly mob', ['1', 'W']).
card_cmc('unruly mob', 2).
card_layout('unruly mob', 'normal').
card_power('unruly mob', 1).
card_toughness('unruly mob', 1).

% Found in: ARB
card_name('unscythe, killer of kings', 'Unscythe, Killer of Kings').
card_type('unscythe, killer of kings', 'Legendary Artifact — Equipment').
card_types('unscythe, killer of kings', ['Artifact']).
card_subtypes('unscythe, killer of kings', ['Equipment']).
card_supertypes('unscythe, killer of kings', ['Legendary']).
card_colors('unscythe, killer of kings', ['U', 'B', 'R']).
card_text('unscythe, killer of kings', 'Equipped creature gets +3/+3 and has first strike.\nWhenever a creature dealt damage by equipped creature this turn dies, you may exile that card. If you do, put a 2/2 black Zombie creature token onto the battlefield.\nEquip {2}').
card_mana_cost('unscythe, killer of kings', ['U', 'B', 'B', 'R']).
card_cmc('unscythe, killer of kings', 4).
card_layout('unscythe, killer of kings', 'normal').

% Found in: 6ED, MIR
card_name('unseen walker', 'Unseen Walker').
card_type('unseen walker', 'Creature — Dryad').
card_types('unseen walker', ['Creature']).
card_subtypes('unseen walker', ['Dryad']).
card_colors('unseen walker', ['G']).
card_text('unseen walker', 'Forestwalk (This creature can\'t be blocked as long as defending player controls a Forest.)\n{1}{G}{G}: Target creature gains forestwalk until end of turn.').
card_mana_cost('unseen walker', ['1', 'G']).
card_cmc('unseen walker', 2).
card_layout('unseen walker', 'normal').
card_power('unseen walker', 1).
card_toughness('unseen walker', 1).

% Found in: SCG
card_name('unspeakable symbol', 'Unspeakable Symbol').
card_type('unspeakable symbol', 'Enchantment').
card_types('unspeakable symbol', ['Enchantment']).
card_subtypes('unspeakable symbol', []).
card_colors('unspeakable symbol', ['B']).
card_text('unspeakable symbol', 'Pay 3 life: Put a +1/+1 counter on target creature.').
card_mana_cost('unspeakable symbol', ['1', 'B', 'B']).
card_cmc('unspeakable symbol', 3).
card_layout('unspeakable symbol', 'normal').

% Found in: ZEN
card_name('unstable footing', 'Unstable Footing').
card_type('unstable footing', 'Instant').
card_types('unstable footing', ['Instant']).
card_subtypes('unstable footing', []).
card_colors('unstable footing', ['R']).
card_text('unstable footing', 'Kicker {3}{R} (You may pay an additional {3}{R} as you cast this spell.)\nDamage can\'t be prevented this turn. If Unstable Footing was kicked, it deals 5 damage to target player.').
card_mana_cost('unstable footing', ['R']).
card_cmc('unstable footing', 1).
card_layout('unstable footing', 'normal').

% Found in: CON
card_name('unstable frontier', 'Unstable Frontier').
card_type('unstable frontier', 'Land').
card_types('unstable frontier', ['Land']).
card_subtypes('unstable frontier', []).
card_colors('unstable frontier', []).
card_text('unstable frontier', '{T}: Add {1} to your mana pool.\n{T}: Target land you control becomes the basic land type of your choice until end of turn.').
card_layout('unstable frontier', 'normal').

% Found in: LGN
card_name('unstable hulk', 'Unstable Hulk').
card_type('unstable hulk', 'Creature — Goblin Mutant').
card_types('unstable hulk', ['Creature']).
card_subtypes('unstable hulk', ['Goblin', 'Mutant']).
card_colors('unstable hulk', ['R']).
card_text('unstable hulk', 'Morph {3}{R}{R} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)\nWhen Unstable Hulk is turned face up, it gets +6/+6 and gains trample until end of turn. You skip your next turn.').
card_mana_cost('unstable hulk', ['1', 'R', 'R']).
card_cmc('unstable hulk', 3).
card_layout('unstable hulk', 'normal').
card_power('unstable hulk', 2).
card_toughness('unstable hulk', 2).

% Found in: 3ED, 4ED, 5ED, ARN, TSB
card_name('unstable mutation', 'Unstable Mutation').
card_type('unstable mutation', 'Enchantment — Aura').
card_types('unstable mutation', ['Enchantment']).
card_subtypes('unstable mutation', ['Aura']).
card_colors('unstable mutation', ['U']).
card_text('unstable mutation', 'Enchant creature\nEnchanted creature gets +3/+3.\nAt the beginning of the upkeep of enchanted creature\'s controller, put a -1/-1 counter on that creature.').
card_mana_cost('unstable mutation', ['U']).
card_cmc('unstable mutation', 1).
card_layout('unstable mutation', 'normal').

% Found in: C14
card_name('unstable obelisk', 'Unstable Obelisk').
card_type('unstable obelisk', 'Artifact').
card_types('unstable obelisk', ['Artifact']).
card_subtypes('unstable obelisk', []).
card_colors('unstable obelisk', []).
card_text('unstable obelisk', '{T}: Add {1} to your mana pool.\n{7}, {T}, Sacrifice Unstable Obelisk: Destroy target permanent.').
card_mana_cost('unstable obelisk', ['3']).
card_cmc('unstable obelisk', 3).
card_layout('unstable obelisk', 'normal').

% Found in: TMP
card_name('unstable shapeshifter', 'Unstable Shapeshifter').
card_type('unstable shapeshifter', 'Creature — Shapeshifter').
card_types('unstable shapeshifter', ['Creature']).
card_subtypes('unstable shapeshifter', ['Shapeshifter']).
card_colors('unstable shapeshifter', ['U']).
card_text('unstable shapeshifter', 'Whenever another creature enters the battlefield, Unstable Shapeshifter becomes a copy of that creature and gains this ability.').
card_mana_cost('unstable shapeshifter', ['3', 'U']).
card_cmc('unstable shapeshifter', 4).
card_layout('unstable shapeshifter', 'normal').
card_power('unstable shapeshifter', 0).
card_toughness('unstable shapeshifter', 1).

% Found in: MOR
card_name('unstoppable ash', 'Unstoppable Ash').
card_type('unstoppable ash', 'Creature — Treefolk Warrior').
card_types('unstoppable ash', ['Creature']).
card_subtypes('unstoppable ash', ['Treefolk', 'Warrior']).
card_colors('unstoppable ash', ['G']).
card_text('unstoppable ash', 'Trample\nChampion a Treefolk or Warrior (When this enters the battlefield, sacrifice it unless you exile another Treefolk or Warrior you control. When this leaves the battlefield, that card returns to the battlefield.)\nWhenever a creature you control becomes blocked, it gets +0/+5 until end of turn.').
card_mana_cost('unstoppable ash', ['3', 'G']).
card_cmc('unstoppable ash', 4).
card_layout('unstoppable ash', 'normal').
card_power('unstoppable ash', 5).
card_toughness('unstoppable ash', 5).

% Found in: 10E, 2ED, 3ED, 4ED, 5ED, 6ED, 7ED, 8ED, CED, CEI, CON, DPA, ITP, LEA, LEB, M10, M11, M12, M13, RQS
card_name('unsummon', 'Unsummon').
card_type('unsummon', 'Instant').
card_types('unsummon', ['Instant']).
card_subtypes('unsummon', []).
card_colors('unsummon', ['U']).
card_text('unsummon', 'Return target creature to its owner\'s hand.').
card_mana_cost('unsummon', ['U']).
card_cmc('unsummon', 1).
card_layout('unsummon', 'normal').

% Found in: CHK
card_name('untaidake, the cloud keeper', 'Untaidake, the Cloud Keeper').
card_type('untaidake, the cloud keeper', 'Legendary Land').
card_types('untaidake, the cloud keeper', ['Land']).
card_subtypes('untaidake, the cloud keeper', []).
card_supertypes('untaidake, the cloud keeper', ['Legendary']).
card_colors('untaidake, the cloud keeper', []).
card_text('untaidake, the cloud keeper', 'Untaidake, the Cloud Keeper enters the battlefield tapped.\n{T}, Pay 2 life: Add {2} to your mana pool. Spend this mana only to cast legendary spells.').
card_layout('untaidake, the cloud keeper', 'normal').

% Found in: SOM
card_name('untamed might', 'Untamed Might').
card_type('untamed might', 'Instant').
card_types('untamed might', ['Instant']).
card_subtypes('untamed might', []).
card_colors('untamed might', ['G']).
card_text('untamed might', 'Target creature gets +X/+X until end of turn.').
card_mana_cost('untamed might', ['X', 'G']).
card_cmc('untamed might', 1).
card_layout('untamed might', 'normal').

% Found in: 4ED, 5ED, 6ED, 7ED, ITP, LEG, PO2, POR, RQS, S99
card_name('untamed wilds', 'Untamed Wilds').
card_type('untamed wilds', 'Sorcery').
card_types('untamed wilds', ['Sorcery']).
card_subtypes('untamed wilds', []).
card_colors('untamed wilds', ['G']).
card_text('untamed wilds', 'Search your library for a basic land card and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('untamed wilds', ['2', 'G']).
card_cmc('untamed wilds', 3).
card_layout('untamed wilds', 'normal').

% Found in: EVE
card_name('unwilling recruit', 'Unwilling Recruit').
card_type('unwilling recruit', 'Sorcery').
card_types('unwilling recruit', ['Sorcery']).
card_subtypes('unwilling recruit', []).
card_colors('unwilling recruit', ['R']).
card_text('unwilling recruit', 'Gain control of target creature until end of turn. Untap that creature. It gets +X/+0 and gains haste until end of turn.').
card_mana_cost('unwilling recruit', ['X', 'R', 'R', 'R']).
card_cmc('unwilling recruit', 3).
card_layout('unwilling recruit', 'normal').

% Found in: NPH
card_name('unwinding clock', 'Unwinding Clock').
card_type('unwinding clock', 'Artifact').
card_types('unwinding clock', ['Artifact']).
card_subtypes('unwinding clock', []).
card_colors('unwinding clock', []).
card_text('unwinding clock', 'Untap all artifacts you control during each other player\'s untap step.').
card_mana_cost('unwinding clock', ['4']).
card_cmc('unwinding clock', 4).
card_layout('unwinding clock', 'normal').

% Found in: USG
card_name('unworthy dead', 'Unworthy Dead').
card_type('unworthy dead', 'Creature — Skeleton').
card_types('unworthy dead', ['Creature']).
card_subtypes('unworthy dead', ['Skeleton']).
card_colors('unworthy dead', ['B']).
card_text('unworthy dead', '{B}: Regenerate Unworthy Dead.').
card_mana_cost('unworthy dead', ['1', 'B']).
card_cmc('unworthy dead', 2).
card_layout('unworthy dead', 'normal').
card_power('unworthy dead', 1).
card_toughness('unworthy dead', 1).

% Found in: MIR
card_name('unyaro bee sting', 'Unyaro Bee Sting').
card_type('unyaro bee sting', 'Sorcery').
card_types('unyaro bee sting', ['Sorcery']).
card_subtypes('unyaro bee sting', []).
card_colors('unyaro bee sting', ['G']).
card_text('unyaro bee sting', 'Unyaro Bee Sting deals 2 damage to target creature or player.').
card_mana_cost('unyaro bee sting', ['3', 'G']).
card_cmc('unyaro bee sting', 4).
card_layout('unyaro bee sting', 'normal').

% Found in: TSP
card_name('unyaro bees', 'Unyaro Bees').
card_type('unyaro bees', 'Creature — Insect').
card_types('unyaro bees', ['Creature']).
card_subtypes('unyaro bees', ['Insect']).
card_colors('unyaro bees', ['G']).
card_text('unyaro bees', 'Flying\n{G}: Unyaro Bees gets +1/+1 until end of turn.\n{3}{G}, Sacrifice Unyaro Bees: Unyaro Bees deals 2 damage to target creature or player.').
card_mana_cost('unyaro bees', ['G', 'G', 'G']).
card_cmc('unyaro bees', 3).
card_layout('unyaro bees', 'normal').
card_power('unyaro bees', 0).
card_toughness('unyaro bees', 1).

% Found in: 6ED, MIR
card_name('unyaro griffin', 'Unyaro Griffin').
card_type('unyaro griffin', 'Creature — Griffin').
card_types('unyaro griffin', ['Creature']).
card_subtypes('unyaro griffin', ['Griffin']).
card_colors('unyaro griffin', ['W']).
card_text('unyaro griffin', 'Flying\nSacrifice Unyaro Griffin: Counter target red instant or sorcery spell.').
card_mana_cost('unyaro griffin', ['3', 'W']).
card_cmc('unyaro griffin', 4).
card_layout('unyaro griffin', 'normal').
card_power('unyaro griffin', 2).
card_toughness('unyaro griffin', 2).

% Found in: KTK
card_name('unyielding krumar', 'Unyielding Krumar').
card_type('unyielding krumar', 'Creature — Orc Warrior').
card_types('unyielding krumar', ['Creature']).
card_subtypes('unyielding krumar', ['Orc', 'Warrior']).
card_colors('unyielding krumar', ['B']).
card_text('unyielding krumar', '{1}{W}: Unyielding Krumar gains first strike until end of turn.').
card_mana_cost('unyielding krumar', ['3', 'B']).
card_cmc('unyielding krumar', 4).
card_layout('unyielding krumar', 'normal').
card_power('unyielding krumar', 3).
card_toughness('unyielding krumar', 3).

% Found in: 5ED, ICE
card_name('updraft', 'Updraft').
card_type('updraft', 'Instant').
card_types('updraft', ['Instant']).
card_subtypes('updraft', []).
card_colors('updraft', ['U']).
card_text('updraft', 'Target creature gains flying until end of turn.\nDraw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('updraft', ['1', 'U']).
card_cmc('updraft', 2).
card_layout('updraft', 'normal').

% Found in: DTK
card_name('updraft elemental', 'Updraft Elemental').
card_type('updraft elemental', 'Creature — Elemental').
card_types('updraft elemental', ['Creature']).
card_subtypes('updraft elemental', ['Elemental']).
card_colors('updraft elemental', ['U']).
card_text('updraft elemental', 'Flying').
card_mana_cost('updraft elemental', ['2', 'U']).
card_cmc('updraft elemental', 3).
card_layout('updraft elemental', 'normal').
card_power('updraft elemental', 1).
card_toughness('updraft elemental', 4).

% Found in: ODY, V14, VMA
card_name('upheaval', 'Upheaval').
card_type('upheaval', 'Sorcery').
card_types('upheaval', ['Sorcery']).
card_subtypes('upheaval', []).
card_colors('upheaval', ['U']).
card_text('upheaval', 'Return all permanents to their owners\' hands.').
card_mana_cost('upheaval', ['4', 'U', 'U']).
card_cmc('upheaval', 6).
card_layout('upheaval', 'normal').

% Found in: MMQ
card_name('uphill battle', 'Uphill Battle').
card_type('uphill battle', 'Enchantment').
card_types('uphill battle', ['Enchantment']).
card_subtypes('uphill battle', []).
card_colors('uphill battle', ['R']).
card_text('uphill battle', 'Creatures played by your opponents enter the battlefield tapped.').
card_mana_cost('uphill battle', ['2', 'R']).
card_cmc('uphill battle', 3).
card_layout('uphill battle', 'normal').

% Found in: BOK
card_name('uproot', 'Uproot').
card_type('uproot', 'Sorcery — Arcane').
card_types('uproot', ['Sorcery']).
card_subtypes('uproot', ['Arcane']).
card_colors('uproot', ['G']).
card_text('uproot', 'Put target land on top of its owner\'s library.').
card_mana_cost('uproot', ['3', 'G']).
card_cmc('uproot', 4).
card_layout('uproot', 'normal').

% Found in: 10E, SCG
card_name('upwelling', 'Upwelling').
card_type('upwelling', 'Enchantment').
card_types('upwelling', ['Enchantment']).
card_subtypes('upwelling', []).
card_colors('upwelling', ['G']).
card_text('upwelling', 'Mana pools don\'t empty as steps and phases end.').
card_mana_cost('upwelling', ['3', 'G']).
card_cmc('upwelling', 4).
card_layout('upwelling', 'normal').

% Found in: LEG
card_name('ur-drago', 'Ur-Drago').
card_type('ur-drago', 'Legendary Creature — Elemental').
card_types('ur-drago', ['Creature']).
card_subtypes('ur-drago', ['Elemental']).
card_supertypes('ur-drago', ['Legendary']).
card_colors('ur-drago', ['U', 'B']).
card_text('ur-drago', 'First strike\nCreatures with swampwalk can be blocked as though they didn\'t have swampwalk.').
card_mana_cost('ur-drago', ['3', 'U', 'U', 'B', 'B']).
card_cmc('ur-drago', 7).
card_layout('ur-drago', 'normal').
card_power('ur-drago', 4).
card_toughness('ur-drago', 4).
card_reserved('ur-drago').

% Found in: 9ED, C14, DST
card_name('ur-golem\'s eye', 'Ur-Golem\'s Eye').
card_type('ur-golem\'s eye', 'Artifact').
card_types('ur-golem\'s eye', ['Artifact']).
card_subtypes('ur-golem\'s eye', []).
card_colors('ur-golem\'s eye', []).
card_text('ur-golem\'s eye', '{T}: Add {2} to your mana pool.').
card_mana_cost('ur-golem\'s eye', ['4']).
card_cmc('ur-golem\'s eye', 4).
card_layout('ur-golem\'s eye', 'normal').

% Found in: NPH
card_name('urabrask the hidden', 'Urabrask the Hidden').
card_type('urabrask the hidden', 'Legendary Creature — Praetor').
card_types('urabrask the hidden', ['Creature']).
card_subtypes('urabrask the hidden', ['Praetor']).
card_supertypes('urabrask the hidden', ['Legendary']).
card_colors('urabrask the hidden', ['R']).
card_text('urabrask the hidden', 'Creatures you control have haste.\nCreatures your opponents control enter the battlefield tapped.').
card_mana_cost('urabrask the hidden', ['3', 'R', 'R']).
card_cmc('urabrask the hidden', 5).
card_layout('urabrask the hidden', 'normal').
card_power('urabrask the hidden', 4).
card_toughness('urabrask the hidden', 4).

% Found in: RTR
card_name('urban burgeoning', 'Urban Burgeoning').
card_type('urban burgeoning', 'Enchantment — Aura').
card_types('urban burgeoning', ['Enchantment']).
card_subtypes('urban burgeoning', ['Aura']).
card_colors('urban burgeoning', ['G']).
card_text('urban burgeoning', 'Enchant land\nEnchanted land has \"Untap this land during each other player\'s untap step.\"').
card_mana_cost('urban burgeoning', ['G']).
card_cmc('urban burgeoning', 1).
card_layout('urban burgeoning', 'normal').

% Found in: DDO, GTC
card_name('urban evolution', 'Urban Evolution').
card_type('urban evolution', 'Sorcery').
card_types('urban evolution', ['Sorcery']).
card_subtypes('urban evolution', []).
card_colors('urban evolution', ['U', 'G']).
card_text('urban evolution', 'Draw three cards. You may play an additional land this turn.').
card_mana_cost('urban evolution', ['3', 'G', 'U']).
card_cmc('urban evolution', 5).
card_layout('urban evolution', 'normal').

% Found in: GTC
card_name('urbis protector', 'Urbis Protector').
card_type('urbis protector', 'Creature — Human Cleric').
card_types('urbis protector', ['Creature']).
card_subtypes('urbis protector', ['Human', 'Cleric']).
card_colors('urbis protector', ['W']).
card_text('urbis protector', 'When Urbis Protector enters the battlefield, put a 4/4 white Angel creature token with flying onto the battlefield.').
card_mana_cost('urbis protector', ['4', 'W', 'W']).
card_cmc('urbis protector', 6).
card_layout('urbis protector', 'normal').
card_power('urbis protector', 1).
card_toughness('urbis protector', 1).

% Found in: LEG, ME3
card_name('urborg', 'Urborg').
card_type('urborg', 'Legendary Land').
card_types('urborg', ['Land']).
card_subtypes('urborg', []).
card_supertypes('urborg', ['Legendary']).
card_colors('urborg', []).
card_text('urborg', '{T}: Add {B} to your mana pool.\n{T}: Target creature loses first strike or swampwalk until end of turn.').
card_layout('urborg', 'normal').

% Found in: INV
card_name('urborg drake', 'Urborg Drake').
card_type('urborg drake', 'Creature — Drake').
card_types('urborg drake', ['Creature']).
card_subtypes('urborg drake', ['Drake']).
card_colors('urborg drake', ['U', 'B']).
card_text('urborg drake', 'Flying\nUrborg Drake attacks each turn if able.').
card_mana_cost('urborg drake', ['1', 'U', 'B']).
card_cmc('urborg drake', 3).
card_layout('urborg drake', 'normal').
card_power('urborg drake', 2).
card_toughness('urborg drake', 3).

% Found in: APC
card_name('urborg elf', 'Urborg Elf').
card_type('urborg elf', 'Creature — Elf Druid').
card_types('urborg elf', ['Creature']).
card_subtypes('urborg elf', ['Elf', 'Druid']).
card_colors('urborg elf', ['G']).
card_text('urborg elf', '{T}: Add {G}, {U}, or {B} to your mana pool.').
card_mana_cost('urborg elf', ['1', 'G']).
card_cmc('urborg elf', 2).
card_layout('urborg elf', 'normal').
card_power('urborg elf', 1).
card_toughness('urborg elf', 1).

% Found in: INV
card_name('urborg emissary', 'Urborg Emissary').
card_type('urborg emissary', 'Creature — Human Wizard').
card_types('urborg emissary', ['Creature']).
card_subtypes('urborg emissary', ['Human', 'Wizard']).
card_colors('urborg emissary', ['B']).
card_text('urborg emissary', 'Kicker {1}{U} (You may pay an additional {1}{U} as you cast this spell.)\nWhen Urborg Emissary enters the battlefield, if it was kicked, return target permanent to its owner\'s hand.').
card_mana_cost('urborg emissary', ['2', 'B']).
card_cmc('urborg emissary', 3).
card_layout('urborg emissary', 'normal').
card_power('urborg emissary', 3).
card_toughness('urborg emissary', 1).

% Found in: WTH
card_name('urborg justice', 'Urborg Justice').
card_type('urborg justice', 'Instant').
card_types('urborg justice', ['Instant']).
card_subtypes('urborg justice', []).
card_colors('urborg justice', ['B']).
card_text('urborg justice', 'Target opponent sacrifices a creature for each creature put into your graveyard from the battlefield this turn.').
card_mana_cost('urborg justice', ['B', 'B']).
card_cmc('urborg justice', 2).
card_layout('urborg justice', 'normal').
card_reserved('urborg justice').

% Found in: MGB, VIS
card_name('urborg mindsucker', 'Urborg Mindsucker').
card_type('urborg mindsucker', 'Creature — Horror').
card_types('urborg mindsucker', ['Creature']).
card_subtypes('urborg mindsucker', ['Horror']).
card_colors('urborg mindsucker', ['B']).
card_text('urborg mindsucker', '{B}, Sacrifice Urborg Mindsucker: Target opponent discards a card at random. Activate this ability only any time you could cast a sorcery.').
card_mana_cost('urborg mindsucker', ['2', 'B']).
card_cmc('urborg mindsucker', 3).
card_layout('urborg mindsucker', 'normal').
card_power('urborg mindsucker', 2).
card_toughness('urborg mindsucker', 2).

% Found in: MIR
card_name('urborg panther', 'Urborg Panther').
card_type('urborg panther', 'Creature — Nightstalker Cat').
card_types('urborg panther', ['Creature']).
card_subtypes('urborg panther', ['Nightstalker', 'Cat']).
card_colors('urborg panther', ['B']).
card_text('urborg panther', '{B}, Sacrifice Urborg Panther: Destroy target creature blocking Urborg Panther.\nSacrifice a creature named Feral Shadow, a creature named Breathstealer, and Urborg Panther: Search your library for a card named Spirit of the Night and put that card onto the battlefield. Then shuffle your library.').
card_mana_cost('urborg panther', ['2', 'B']).
card_cmc('urborg panther', 3).
card_layout('urborg panther', 'normal').
card_power('urborg panther', 2).
card_toughness('urborg panther', 2).

% Found in: INV
card_name('urborg phantom', 'Urborg Phantom').
card_type('urborg phantom', 'Creature — Spirit Minion').
card_types('urborg phantom', ['Creature']).
card_subtypes('urborg phantom', ['Spirit', 'Minion']).
card_colors('urborg phantom', ['B']).
card_text('urborg phantom', 'Urborg Phantom can\'t block.\n{U}: Prevent all combat damage that would be dealt to and dealt by Urborg Phantom this turn.').
card_mana_cost('urborg phantom', ['2', 'B']).
card_cmc('urborg phantom', 3).
card_layout('urborg phantom', 'normal').
card_power('urborg phantom', 3).
card_toughness('urborg phantom', 1).

% Found in: INV
card_name('urborg shambler', 'Urborg Shambler').
card_type('urborg shambler', 'Creature — Horror').
card_types('urborg shambler', ['Creature']).
card_subtypes('urborg shambler', ['Horror']).
card_colors('urborg shambler', ['B']).
card_text('urborg shambler', 'Other black creatures get -1/-1.').
card_mana_cost('urborg shambler', ['2', 'B', 'B']).
card_cmc('urborg shambler', 4).
card_layout('urborg shambler', 'normal').
card_power('urborg shambler', 4).
card_toughness('urborg shambler', 3).

% Found in: INV
card_name('urborg skeleton', 'Urborg Skeleton').
card_type('urborg skeleton', 'Creature — Skeleton').
card_types('urborg skeleton', ['Creature']).
card_subtypes('urborg skeleton', ['Skeleton']).
card_colors('urborg skeleton', ['B']).
card_text('urborg skeleton', 'Kicker {3} (You may pay an additional {3} as you cast this spell.)\n{B}: Regenerate Urborg Skeleton.\nIf Urborg Skeleton was kicked, it enters the battlefield with a +1/+1 counter on it.').
card_mana_cost('urborg skeleton', ['B']).
card_cmc('urborg skeleton', 1).
card_layout('urborg skeleton', 'normal').
card_power('urborg skeleton', 0).
card_toughness('urborg skeleton', 1).

% Found in: WTH
card_name('urborg stalker', 'Urborg Stalker').
card_type('urborg stalker', 'Creature — Horror').
card_types('urborg stalker', ['Creature']).
card_subtypes('urborg stalker', ['Horror']).
card_colors('urborg stalker', ['B']).
card_text('urborg stalker', 'At the beginning of each player\'s upkeep, if that player controls a nonblack, nonland permanent, Urborg Stalker deals 1 damage to that player.').
card_mana_cost('urborg stalker', ['3', 'B']).
card_cmc('urborg stalker', 4).
card_layout('urborg stalker', 'normal').
card_power('urborg stalker', 2).
card_toughness('urborg stalker', 4).
card_reserved('urborg stalker').

% Found in: ARC, DD3_GVL, DDD, TSP
card_name('urborg syphon-mage', 'Urborg Syphon-Mage').
card_type('urborg syphon-mage', 'Creature — Human Spellshaper').
card_types('urborg syphon-mage', ['Creature']).
card_subtypes('urborg syphon-mage', ['Human', 'Spellshaper']).
card_colors('urborg syphon-mage', ['B']).
card_text('urborg syphon-mage', '{2}{B}, {T}, Discard a card: Each other player loses 2 life. You gain life equal to the life lost this way.').
card_mana_cost('urborg syphon-mage', ['2', 'B']).
card_cmc('urborg syphon-mage', 3).
card_layout('urborg syphon-mage', 'normal').
card_power('urborg syphon-mage', 2).
card_toughness('urborg syphon-mage', 2).

% Found in: APC, VMA
card_name('urborg uprising', 'Urborg Uprising').
card_type('urborg uprising', 'Sorcery').
card_types('urborg uprising', ['Sorcery']).
card_subtypes('urborg uprising', []).
card_colors('urborg uprising', ['B']).
card_text('urborg uprising', 'Return up to two target creature cards from your graveyard to your hand.\nDraw a card.').
card_mana_cost('urborg uprising', ['4', 'B']).
card_cmc('urborg uprising', 5).
card_layout('urborg uprising', 'normal').

% Found in: 8ED, INV
card_name('urborg volcano', 'Urborg Volcano').
card_type('urborg volcano', 'Land').
card_types('urborg volcano', ['Land']).
card_subtypes('urborg volcano', []).
card_colors('urborg volcano', []).
card_text('urborg volcano', 'Urborg Volcano enters the battlefield tapped.\n{T}: Add {B} or {R} to your mana pool.').
card_layout('urborg volcano', 'normal').

% Found in: M15, PLC, V12
card_name('urborg, tomb of yawgmoth', 'Urborg, Tomb of Yawgmoth').
card_type('urborg, tomb of yawgmoth', 'Legendary Land').
card_types('urborg, tomb of yawgmoth', ['Land']).
card_subtypes('urborg, tomb of yawgmoth', []).
card_supertypes('urborg, tomb of yawgmoth', ['Legendary']).
card_colors('urborg, tomb of yawgmoth', []).
card_text('urborg, tomb of yawgmoth', 'Each land is a Swamp in addition to its other land types.').
card_layout('urborg, tomb of yawgmoth', 'normal').

% Found in: DDK, WWK
card_name('urge to feed', 'Urge to Feed').
card_type('urge to feed', 'Instant').
card_types('urge to feed', ['Instant']).
card_subtypes('urge to feed', []).
card_colors('urge to feed', ['B']).
card_text('urge to feed', 'Target creature gets -3/-3 until end of turn. You may tap any number of untapped Vampire creatures you control. If you do, put a +1/+1 counter on each of those Vampires.').
card_mana_cost('urge to feed', ['B', 'B']).
card_cmc('urge to feed', 2).
card_layout('urge to feed', 'normal').

% Found in: ISD
card_name('urgent exorcism', 'Urgent Exorcism').
card_type('urgent exorcism', 'Instant').
card_types('urgent exorcism', ['Instant']).
card_subtypes('urgent exorcism', []).
card_colors('urgent exorcism', ['W']).
card_text('urgent exorcism', 'Destroy target Spirit or enchantment.').
card_mana_cost('urgent exorcism', ['1', 'W']).
card_cmc('urgent exorcism', 2).
card_layout('urgent exorcism', 'normal').

% Found in: ARB
card_name('uril, the miststalker', 'Uril, the Miststalker').
card_type('uril, the miststalker', 'Legendary Creature — Beast').
card_types('uril, the miststalker', ['Creature']).
card_subtypes('uril, the miststalker', ['Beast']).
card_supertypes('uril, the miststalker', ['Legendary']).
card_colors('uril, the miststalker', ['W', 'R', 'G']).
card_text('uril, the miststalker', 'Hexproof (This creature can\'t be the target of spells or abilities your opponents control.)\nUril, the Miststalker gets +2/+2 for each Aura attached to it.').
card_mana_cost('uril, the miststalker', ['2', 'R', 'G', 'W']).
card_cmc('uril, the miststalker', 5).
card_layout('uril, the miststalker', 'normal').
card_power('uril, the miststalker', 5).
card_toughness('uril, the miststalker', 5).

% Found in: RAV
card_name('ursapine', 'Ursapine').
card_type('ursapine', 'Creature — Beast').
card_types('ursapine', ['Creature']).
card_subtypes('ursapine', ['Beast']).
card_colors('ursapine', ['G']).
card_text('ursapine', '{G}: Target creature gets +1/+1 until end of turn.').
card_mana_cost('ursapine', ['3', 'G', 'G']).
card_cmc('ursapine', 5).
card_layout('ursapine', 'normal').
card_power('ursapine', 3).
card_toughness('ursapine', 3).

% Found in: CSP
card_name('ursine fylgja', 'Ursine Fylgja').
card_type('ursine fylgja', 'Creature — Spirit Bear').
card_types('ursine fylgja', ['Creature']).
card_subtypes('ursine fylgja', ['Spirit', 'Bear']).
card_colors('ursine fylgja', ['W']).
card_text('ursine fylgja', 'Ursine Fylgja enters the battlefield with four healing counters on it.\nRemove a healing counter from Ursine Fylgja: Prevent the next 1 damage that would be dealt to Ursine Fylgja this turn.\n{2}{W}: Put a healing counter on Ursine Fylgja.').
card_mana_cost('ursine fylgja', ['4', 'W']).
card_cmc('ursine fylgja', 5).
card_layout('ursine fylgja', 'normal').
card_power('ursine fylgja', 3).
card_toughness('ursine fylgja', 3).

% Found in: VAN
card_name('urza', 'Urza').
card_type('urza', 'Vanguard').
card_types('urza', ['Vanguard']).
card_subtypes('urza', []).
card_colors('urza', []).
card_text('urza', '{3}: Urza deals 1 damage to target creature or player.').
card_layout('urza', 'vanguard').

% Found in: 8ED, USG
card_name('urza\'s armor', 'Urza\'s Armor').
card_type('urza\'s armor', 'Artifact').
card_types('urza\'s armor', ['Artifact']).
card_subtypes('urza\'s armor', []).
card_colors('urza\'s armor', []).
card_text('urza\'s armor', 'If a source would deal damage to you, prevent 1 of that damage.').
card_mana_cost('urza\'s armor', ['6']).
card_cmc('urza\'s armor', 6).
card_layout('urza\'s armor', 'normal').

% Found in: 4ED, 5ED, ATQ
card_name('urza\'s avenger', 'Urza\'s Avenger').
card_type('urza\'s avenger', 'Artifact Creature — Shapeshifter').
card_types('urza\'s avenger', ['Artifact', 'Creature']).
card_subtypes('urza\'s avenger', ['Shapeshifter']).
card_colors('urza\'s avenger', []).
card_text('urza\'s avenger', '{0}: Urza\'s Avenger gets -1/-1 and gains your choice of banding, flying, first strike, or trample until end of turn. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)').
card_mana_cost('urza\'s avenger', ['6']).
card_cmc('urza\'s avenger', 6).
card_layout('urza\'s avenger', 'normal').
card_power('urza\'s avenger', 4).
card_toughness('urza\'s avenger', 4).

% Found in: 5ED, ICE, MED
card_name('urza\'s bauble', 'Urza\'s Bauble').
card_type('urza\'s bauble', 'Artifact').
card_types('urza\'s bauble', ['Artifact']).
card_subtypes('urza\'s bauble', []).
card_colors('urza\'s bauble', []).
card_text('urza\'s bauble', '{T}, Sacrifice Urza\'s Bauble: Look at a card at random in target player\'s hand. You draw a card at the beginning of the next turn\'s upkeep.').
card_mana_cost('urza\'s bauble', ['0']).
card_cmc('urza\'s bauble', 0).
card_layout('urza\'s bauble', 'normal').

% Found in: ULG
card_name('urza\'s blueprints', 'Urza\'s Blueprints').
card_type('urza\'s blueprints', 'Artifact').
card_types('urza\'s blueprints', ['Artifact']).
card_subtypes('urza\'s blueprints', []).
card_colors('urza\'s blueprints', []).
card_text('urza\'s blueprints', 'Echo {6} (At the beginning of your upkeep, if this came under your control since the beginning of your last upkeep, sacrifice it unless you pay its echo cost.)\n{T}: Draw a card.').
card_mana_cost('urza\'s blueprints', ['6']).
card_cmc('urza\'s blueprints', 6).
card_layout('urza\'s blueprints', 'normal').

% Found in: ATQ, ME4, MED
card_name('urza\'s chalice', 'Urza\'s Chalice').
card_type('urza\'s chalice', 'Artifact').
card_types('urza\'s chalice', ['Artifact']).
card_subtypes('urza\'s chalice', []).
card_colors('urza\'s chalice', []).
card_text('urza\'s chalice', 'Whenever a player casts an artifact spell, you may pay {1}. If you do, you gain 1 life.').
card_mana_cost('urza\'s chalice', ['1']).
card_cmc('urza\'s chalice', 1).
card_layout('urza\'s chalice', 'normal').

% Found in: UGL
card_name('urza\'s contact lenses', 'Urza\'s Contact Lenses').
card_type('urza\'s contact lenses', 'Artifact').
card_types('urza\'s contact lenses', ['Artifact']).
card_subtypes('urza\'s contact lenses', []).
card_colors('urza\'s contact lenses', []).
card_text('urza\'s contact lenses', 'Urza\'s Contact Lenses comes into play tapped and does not untap during its controller\'s untap phase.\nAll players play with their hands face up.\nClap your hands twice: Tap or untap Urza\'s Contact Lenses.').
card_mana_cost('urza\'s contact lenses', ['0']).
card_cmc('urza\'s contact lenses', 0).
card_layout('urza\'s contact lenses', 'normal').

% Found in: ALL
card_name('urza\'s engine', 'Urza\'s Engine').
card_type('urza\'s engine', 'Artifact Creature — Juggernaut').
card_types('urza\'s engine', ['Artifact', 'Creature']).
card_subtypes('urza\'s engine', ['Juggernaut']).
card_colors('urza\'s engine', []).
card_text('urza\'s engine', 'Trample\n{3}: Urza\'s Engine gains banding until end of turn. (Any creatures with banding, and up to one without, can attack in a band. Bands are blocked as a group. If any creatures with banding you control are blocking or being blocked by a creature, you divide that creature\'s combat damage, not its controller, among any of the creatures it\'s being blocked by or is blocking.)\n{3}: Attacking creatures banded with Urza\'s Engine gain trample until end of turn.').
card_mana_cost('urza\'s engine', ['5']).
card_cmc('urza\'s engine', 5).
card_layout('urza\'s engine', 'normal').
card_power('urza\'s engine', 1).
card_toughness('urza\'s engine', 5).

% Found in: C13, TSP, pCMP
card_name('urza\'s factory', 'Urza\'s Factory').
card_type('urza\'s factory', 'Land — Urza’s').
card_types('urza\'s factory', ['Land']).
card_subtypes('urza\'s factory', ['Urza’s']).
card_colors('urza\'s factory', []).
card_text('urza\'s factory', '{T}: Add {1} to your mana pool.\n{7}, {T}: Put a 2/2 colorless Assembly-Worker artifact creature token onto the battlefield.').
card_layout('urza\'s factory', 'normal').

% Found in: INV
card_name('urza\'s filter', 'Urza\'s Filter').
card_type('urza\'s filter', 'Artifact').
card_types('urza\'s filter', ['Artifact']).
card_subtypes('urza\'s filter', []).
card_colors('urza\'s filter', []).
card_text('urza\'s filter', 'Multicolored spells cost up to {2} less to cast.').
card_mana_cost('urza\'s filter', ['4']).
card_cmc('urza\'s filter', 4).
card_layout('urza\'s filter', 'normal').

% Found in: PLS
card_name('urza\'s guilt', 'Urza\'s Guilt').
card_type('urza\'s guilt', 'Sorcery').
card_types('urza\'s guilt', ['Sorcery']).
card_subtypes('urza\'s guilt', []).
card_colors('urza\'s guilt', ['U', 'B']).
card_text('urza\'s guilt', 'Each player draws two cards, then discards three cards, then loses 4 life.').
card_mana_cost('urza\'s guilt', ['2', 'U', 'B']).
card_cmc('urza\'s guilt', 4).
card_layout('urza\'s guilt', 'normal').

% Found in: UNH
card_name('urza\'s hot tub', 'Urza\'s Hot Tub').
card_type('urza\'s hot tub', 'Artifact').
card_types('urza\'s hot tub', ['Artifact']).
card_subtypes('urza\'s hot tub', []).
card_colors('urza\'s hot tub', []).
card_text('urza\'s hot tub', '{2}, Discard a card: Search your library for a card that shares a complete word in its name with the discarded card, reveal it, and put it into your hand. Then shuffle your library.').
card_mana_cost('urza\'s hot tub', ['2']).
card_cmc('urza\'s hot tub', 2).
card_layout('urza\'s hot tub', 'normal').

% Found in: UDS
card_name('urza\'s incubator', 'Urza\'s Incubator').
card_type('urza\'s incubator', 'Artifact').
card_types('urza\'s incubator', ['Artifact']).
card_subtypes('urza\'s incubator', []).
card_colors('urza\'s incubator', []).
card_text('urza\'s incubator', 'As Urza\'s Incubator enters the battlefield, choose a creature type.\nCreature spells of the chosen type cost {2} less to cast.').
card_mana_cost('urza\'s incubator', ['3']).
card_cmc('urza\'s incubator', 3).
card_layout('urza\'s incubator', 'normal').

% Found in: 5ED, 8ED, 9ED, ATQ, CHR, ME4
card_name('urza\'s mine', 'Urza\'s Mine').
card_type('urza\'s mine', 'Land — Urza’s Mine').
card_types('urza\'s mine', ['Land']).
card_subtypes('urza\'s mine', ['Urza’s', 'Mine']).
card_colors('urza\'s mine', []).
card_text('urza\'s mine', '{T}: Add {1} to your mana pool. If you control an Urza\'s Power-Plant and an Urza\'s Tower, add {2} to your mana pool instead.').
card_layout('urza\'s mine', 'normal').

% Found in: ATQ, ME4
card_name('urza\'s miter', 'Urza\'s Miter').
card_type('urza\'s miter', 'Artifact').
card_types('urza\'s miter', ['Artifact']).
card_subtypes('urza\'s miter', []).
card_colors('urza\'s miter', []).
card_text('urza\'s miter', 'Whenever an artifact you control is put into a graveyard from the battlefield, if it wasn\'t sacrificed, you may pay {3}. If you do, draw a card.').
card_mana_cost('urza\'s miter', ['3']).
card_cmc('urza\'s miter', 3).
card_layout('urza\'s miter', 'normal').
card_reserved('urza\'s miter').

% Found in: 5ED, 8ED, 9ED, ATQ, CHR, ME4
card_name('urza\'s power plant', 'Urza\'s Power Plant').
card_type('urza\'s power plant', 'Land — Urza’s Power-Plant').
card_types('urza\'s power plant', ['Land']).
card_subtypes('urza\'s power plant', ['Urza’s', 'Power-Plant']).
card_colors('urza\'s power plant', []).
card_text('urza\'s power plant', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Tower, add {2} to your mana pool instead.').
card_layout('urza\'s power plant', 'normal').

% Found in: DDE, INV
card_name('urza\'s rage', 'Urza\'s Rage').
card_type('urza\'s rage', 'Instant').
card_types('urza\'s rage', ['Instant']).
card_subtypes('urza\'s rage', []).
card_colors('urza\'s rage', ['R']).
card_text('urza\'s rage', 'Kicker {8}{R} (You may pay an additional {8}{R} as you cast this spell.)\nUrza\'s Rage can\'t be countered by spells or abilities.\nUrza\'s Rage deals 3 damage to target creature or player. If Urza\'s Rage was kicked, instead it deals 10 damage to that creature or player and the damage can\'t be prevented.').
card_mana_cost('urza\'s rage', ['2', 'R']).
card_cmc('urza\'s rage', 3).
card_layout('urza\'s rage', 'normal').

% Found in: UGL
card_name('urza\'s science fair project', 'Urza\'s Science Fair Project').
card_type('urza\'s science fair project', 'Artifact Creature').
card_types('urza\'s science fair project', ['Artifact', 'Creature']).
card_subtypes('urza\'s science fair project', []).
card_colors('urza\'s science fair project', []).
card_text('urza\'s science fair project', '{2}: Roll a six-sided die for Urza\'s Science Fair Project.\n1 It gets -2/-2 until end of turn.\n2 It deals no combat damage this turn.\n3 Attacking does not cause it to tap this turn.\n4 It gains first strike until end of turn.\n5 It gains flying until end of turn.\n6 It gets +2/+2 until end of turn.').
card_mana_cost('urza\'s science fair project', ['6']).
card_cmc('urza\'s science fair project', 6).
card_layout('urza\'s science fair project', 'normal').
card_power('urza\'s science fair project', 4).
card_toughness('urza\'s science fair project', 4).

% Found in: 5ED, 8ED, 9ED, ATQ, CHR, ME4
card_name('urza\'s tower', 'Urza\'s Tower').
card_type('urza\'s tower', 'Land — Urza’s Tower').
card_types('urza\'s tower', ['Land']).
card_subtypes('urza\'s tower', ['Urza’s', 'Tower']).
card_colors('urza\'s tower', []).
card_text('urza\'s tower', '{T}: Add {1} to your mana pool. If you control an Urza\'s Mine and an Urza\'s Power-Plant, add {3} to your mana pool instead.').
card_layout('urza\'s tower', 'normal').

% Found in: 2ED, 3ED, 4ED, ATH, BRB, CED, CEI, LEA, LEB, TSB
card_name('uthden troll', 'Uthden Troll').
card_type('uthden troll', 'Creature — Troll').
card_types('uthden troll', ['Creature']).
card_subtypes('uthden troll', ['Troll']).
card_colors('uthden troll', ['R']).
card_text('uthden troll', '{R}: Regenerate Uthden Troll.').
card_mana_cost('uthden troll', ['2', 'R']).
card_cmc('uthden troll', 3).
card_layout('uthden troll', 'normal').
card_power('uthden troll', 2).
card_toughness('uthden troll', 2).

% Found in: FUT
card_name('utopia mycon', 'Utopia Mycon').
card_type('utopia mycon', 'Creature — Fungus').
card_types('utopia mycon', ['Creature']).
card_subtypes('utopia mycon', ['Fungus']).
card_colors('utopia mycon', ['G']).
card_text('utopia mycon', 'At the beginning of your upkeep, put a spore counter on Utopia Mycon.\nRemove three spore counters from Utopia Mycon: Put a 1/1 green Saproling creature token onto the battlefield.\nSacrifice a Saproling: Add one mana of any color to your mana pool.').
card_mana_cost('utopia mycon', ['G']).
card_cmc('utopia mycon', 1).
card_layout('utopia mycon', 'normal').
card_power('utopia mycon', 0).
card_toughness('utopia mycon', 2).

% Found in: DIS
card_name('utopia sprawl', 'Utopia Sprawl').
card_type('utopia sprawl', 'Enchantment — Aura').
card_types('utopia sprawl', ['Enchantment']).
card_subtypes('utopia sprawl', ['Aura']).
card_colors('utopia sprawl', ['G']).
card_text('utopia sprawl', 'Enchant Forest\nAs Utopia Sprawl enters the battlefield, choose a color.\nWhenever enchanted Forest is tapped for mana, its controller adds one mana of the chosen color to his or her mana pool (in addition to the mana the land produces).').
card_mana_cost('utopia sprawl', ['G']).
card_cmc('utopia sprawl', 1).
card_layout('utopia sprawl', 'normal').

% Found in: 9ED, INV
card_name('utopia tree', 'Utopia Tree').
card_type('utopia tree', 'Creature — Plant').
card_types('utopia tree', ['Creature']).
card_subtypes('utopia tree', ['Plant']).
card_colors('utopia tree', ['G']).
card_text('utopia tree', '{T}: Add one mana of any color to your mana pool.').
card_mana_cost('utopia tree', ['1', 'G']).
card_cmc('utopia tree', 2).
card_layout('utopia tree', 'normal').
card_power('utopia tree', 0).
card_toughness('utopia tree', 2).

% Found in: PLC
card_name('utopia vow', 'Utopia Vow').
card_type('utopia vow', 'Enchantment — Aura').
card_types('utopia vow', ['Enchantment']).
card_subtypes('utopia vow', ['Aura']).
card_colors('utopia vow', ['G']).
card_text('utopia vow', 'Enchant creature\nEnchanted creature can\'t attack or block.\nEnchanted creature has \"{T}: Add one mana of any color to your mana pool.\"').
card_mana_cost('utopia vow', ['1', 'G']).
card_cmc('utopia vow', 2).
card_layout('utopia vow', 'normal').

% Found in: KTK, pMGD, pPRE
card_name('utter end', 'Utter End').
card_type('utter end', 'Instant').
card_types('utter end', ['Instant']).
card_subtypes('utter end', []).
card_colors('utter end', ['W', 'B']).
card_text('utter end', 'Exile target nonland permanent.').
card_mana_cost('utter end', ['2', 'W', 'B']).
card_cmc('utter end', 4).
card_layout('utter end', 'normal').

% Found in: RTR
card_name('utvara hellkite', 'Utvara Hellkite').
card_type('utvara hellkite', 'Creature — Dragon').
card_types('utvara hellkite', ['Creature']).
card_subtypes('utvara hellkite', ['Dragon']).
card_colors('utvara hellkite', ['R']).
card_text('utvara hellkite', 'Flying\nWhenever a Dragon you control attacks, put a 6/6 red Dragon creature token with flying onto the battlefield.').
card_mana_cost('utvara hellkite', ['6', 'R', 'R']).
card_cmc('utvara hellkite', 8).
card_layout('utvara hellkite', 'normal').
card_power('utvara hellkite', 6).
card_toughness('utvara hellkite', 6).

% Found in: DIS
card_name('utvara scalper', 'Utvara Scalper').
card_type('utvara scalper', 'Creature — Goblin Scout').
card_types('utvara scalper', ['Creature']).
card_subtypes('utvara scalper', ['Goblin', 'Scout']).
card_colors('utvara scalper', ['R']).
card_text('utvara scalper', 'Flying\nUtvara Scalper attacks each turn if able.').
card_mana_cost('utvara scalper', ['1', 'R']).
card_cmc('utvara scalper', 2).
card_layout('utvara scalper', 'normal').
card_power('utvara scalper', 1).
card_toughness('utvara scalper', 2).

% Found in: C13, CHK
card_name('uyo, silent prophet', 'Uyo, Silent Prophet').
card_type('uyo, silent prophet', 'Legendary Creature — Moonfolk Wizard').
card_types('uyo, silent prophet', ['Creature']).
card_subtypes('uyo, silent prophet', ['Moonfolk', 'Wizard']).
card_supertypes('uyo, silent prophet', ['Legendary']).
card_colors('uyo, silent prophet', ['U']).
card_text('uyo, silent prophet', 'Flying\n{2}, Return two lands you control to their owner\'s hand: Copy target instant or sorcery spell. You may choose new targets for the copy.').
card_mana_cost('uyo, silent prophet', ['4', 'U', 'U']).
card_cmc('uyo, silent prophet', 6).
card_layout('uyo, silent prophet', 'normal').
card_power('uyo, silent prophet', 4).
card_toughness('uyo, silent prophet', 4).

