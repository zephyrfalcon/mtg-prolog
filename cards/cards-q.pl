% Card-specific data

% Found in: DTK
card_name('qal sisma behemoth', 'Qal Sisma Behemoth').
card_type('qal sisma behemoth', 'Creature — Ogre Warrior').
card_types('qal sisma behemoth', ['Creature']).
card_subtypes('qal sisma behemoth', ['Ogre', 'Warrior']).
card_colors('qal sisma behemoth', ['R']).
card_text('qal sisma behemoth', 'Qal Sisma Behemoth can\'t attack or block unless you pay {2}.').
card_mana_cost('qal sisma behemoth', ['2', 'R']).
card_cmc('qal sisma behemoth', 3).
card_layout('qal sisma behemoth', 'normal').
card_power('qal sisma behemoth', 5).
card_toughness('qal sisma behemoth', 5).

% Found in: DTK
card_name('qarsi deceiver', 'Qarsi Deceiver').
card_type('qarsi deceiver', 'Creature — Naga Wizard').
card_types('qarsi deceiver', ['Creature']).
card_subtypes('qarsi deceiver', ['Naga', 'Wizard']).
card_colors('qarsi deceiver', ['U']).
card_text('qarsi deceiver', '{T}: Add {1} to your mana pool. Spend this mana only to cast a face-down creature spell, pay a mana cost to turn a manifested creature face up, or pay a morph cost. (A megamorph cost is a morph cost.)').
card_mana_cost('qarsi deceiver', ['1', 'U']).
card_cmc('qarsi deceiver', 2).
card_layout('qarsi deceiver', 'normal').
card_power('qarsi deceiver', 0).
card_toughness('qarsi deceiver', 4).

% Found in: FRF
card_name('qarsi high priest', 'Qarsi High Priest').
card_type('qarsi high priest', 'Creature — Human Cleric').
card_types('qarsi high priest', ['Creature']).
card_subtypes('qarsi high priest', ['Human', 'Cleric']).
card_colors('qarsi high priest', ['B']).
card_text('qarsi high priest', '{1}{B}, {T}, Sacrifice another creature: Manifest the top card of your library. (Put that card onto the battlefield face down as a 2/2 creature. Turn it face up any time for its mana cost if it\'s a creature card.)').
card_mana_cost('qarsi high priest', ['B']).
card_cmc('qarsi high priest', 1).
card_layout('qarsi high priest', 'normal').
card_power('qarsi high priest', 0).
card_toughness('qarsi high priest', 2).

% Found in: DTK
card_name('qarsi sadist', 'Qarsi Sadist').
card_type('qarsi sadist', 'Creature — Human Cleric').
card_types('qarsi sadist', ['Creature']).
card_subtypes('qarsi sadist', ['Human', 'Cleric']).
card_colors('qarsi sadist', ['B']).
card_text('qarsi sadist', 'Exploit (When this creature enters the battlefield, you may sacrifice a creature.)\nWhen Qarsi Sadist exploits a creature, target opponent loses 2 life and you gain 2 life.').
card_mana_cost('qarsi sadist', ['1', 'B']).
card_cmc('qarsi sadist', 2).
card_layout('qarsi sadist', 'normal').
card_power('qarsi sadist', 1).
card_toughness('qarsi sadist', 3).

% Found in: ALA
card_name('qasali ambusher', 'Qasali Ambusher').
card_type('qasali ambusher', 'Creature — Cat Warrior').
card_types('qasali ambusher', ['Creature']).
card_subtypes('qasali ambusher', ['Cat', 'Warrior']).
card_colors('qasali ambusher', ['W', 'G']).
card_text('qasali ambusher', 'Reach\nIf a creature is attacking you and you control a Forest and a Plains, you may cast Qasali Ambusher without paying its mana cost and as though it had flash.').
card_mana_cost('qasali ambusher', ['1', 'G', 'W']).
card_cmc('qasali ambusher', 3).
card_layout('qasali ambusher', 'normal').
card_power('qasali ambusher', 2).
card_toughness('qasali ambusher', 3).

% Found in: ARB, DDH, pFNM
card_name('qasali pridemage', 'Qasali Pridemage').
card_type('qasali pridemage', 'Creature — Cat Wizard').
card_types('qasali pridemage', ['Creature']).
card_subtypes('qasali pridemage', ['Cat', 'Wizard']).
card_colors('qasali pridemage', ['W', 'G']).
card_text('qasali pridemage', 'Exalted (Whenever a creature you control attacks alone, that creature gets +1/+1 until end of turn.)\n{1}, Sacrifice Qasali Pridemage: Destroy target artifact or enchantment.').
card_mana_cost('qasali pridemage', ['G', 'W']).
card_cmc('qasali pridemage', 2).
card_layout('qasali pridemage', 'normal').
card_power('qasali pridemage', 2).
card_toughness('qasali pridemage', 2).

% Found in: M11, M14
card_name('quag sickness', 'Quag Sickness').
card_type('quag sickness', 'Enchantment — Aura').
card_types('quag sickness', ['Enchantment']).
card_subtypes('quag sickness', ['Aura']).
card_colors('quag sickness', ['B']).
card_text('quag sickness', 'Enchant creature\nEnchanted creature gets -1/-1 for each Swamp you control.').
card_mana_cost('quag sickness', ['2', 'B']).
card_cmc('quag sickness', 3).
card_layout('quag sickness', 'normal').

% Found in: CNS, WWK
card_name('quag vampires', 'Quag Vampires').
card_type('quag vampires', 'Creature — Vampire Rogue').
card_types('quag vampires', ['Creature']).
card_subtypes('quag vampires', ['Vampire', 'Rogue']).
card_colors('quag vampires', ['B']).
card_text('quag vampires', 'Multikicker {1}{B} (You may pay an additional {1}{B} any number of times as you cast this spell.)\nSwampwalk (This creature can\'t be blocked as long as defending player controls a Swamp.)\nQuag Vampires enters the battlefield with a +1/+1 counter on it for each time it was kicked.').
card_mana_cost('quag vampires', ['B']).
card_cmc('quag vampires', 1).
card_layout('quag vampires', 'normal').
card_power('quag vampires', 1).
card_toughness('quag vampires', 1).

% Found in: LEG
card_name('quagmire', 'Quagmire').
card_type('quagmire', 'Enchantment').
card_types('quagmire', ['Enchantment']).
card_subtypes('quagmire', []).
card_colors('quagmire', ['B']).
card_text('quagmire', 'Creatures with swampwalk can be blocked as though they didn\'t have swampwalk.').
card_mana_cost('quagmire', ['2', 'B']).
card_cmc('quagmire', 3).
card_layout('quagmire', 'normal').

% Found in: APC, C13
card_name('quagmire druid', 'Quagmire Druid').
card_type('quagmire druid', 'Creature — Zombie Druid').
card_types('quagmire druid', ['Creature']).
card_subtypes('quagmire druid', ['Zombie', 'Druid']).
card_colors('quagmire druid', ['B']).
card_text('quagmire druid', '{G}, {T}, Sacrifice a creature: Destroy target enchantment.').
card_mana_cost('quagmire druid', ['2', 'B']).
card_cmc('quagmire druid', 3).
card_layout('quagmire druid', 'normal').
card_power('quagmire druid', 2).
card_toughness('quagmire druid', 2).

% Found in: MMQ
card_name('quagmire lamprey', 'Quagmire Lamprey').
card_type('quagmire lamprey', 'Creature — Fish').
card_types('quagmire lamprey', ['Creature']).
card_subtypes('quagmire lamprey', ['Fish']).
card_colors('quagmire lamprey', ['B']).
card_text('quagmire lamprey', 'Whenever Quagmire Lamprey becomes blocked by a creature, put a -1/-1 counter on that creature.').
card_mana_cost('quagmire lamprey', ['2', 'B']).
card_cmc('quagmire lamprey', 3).
card_layout('quagmire lamprey', 'normal').
card_power('quagmire lamprey', 1).
card_toughness('quagmire lamprey', 1).

% Found in: FUT
card_name('quagnoth', 'Quagnoth').
card_type('quagnoth', 'Creature — Beast').
card_types('quagnoth', ['Creature']).
card_subtypes('quagnoth', ['Beast']).
card_colors('quagnoth', ['G']).
card_text('quagnoth', 'Split second (As long as this spell is on the stack, players can\'t cast spells or activate abilities that aren\'t mana abilities.)\nShroud (This creature can\'t be the target of spells or abilities.)\nWhen a spell or ability an opponent controls causes you to discard Quagnoth, return it to your hand.').
card_mana_cost('quagnoth', ['5', 'G']).
card_cmc('quagnoth', 6).
card_layout('quagnoth', 'normal').
card_power('quagnoth', 4).
card_toughness('quagnoth', 5).

% Found in: BFZ
card_name('quarantine field', 'Quarantine Field').
card_type('quarantine field', 'Enchantment').
card_types('quarantine field', ['Enchantment']).
card_subtypes('quarantine field', []).
card_colors('quarantine field', ['W']).
card_text('quarantine field', 'Quarantine Field enters the battlefield with X isolation counters on it.\nWhen Quarantine Field enters the battlefield, for each isolation counter on it, exile up to one target nonland permanent an opponent controls until Quarantine Field leaves the battlefield.').
card_mana_cost('quarantine field', ['X', 'X', 'W', 'W']).
card_cmc('quarantine field', 2).
card_layout('quarantine field', 'normal').

% Found in: JOU
card_name('quarry colossus', 'Quarry Colossus').
card_type('quarry colossus', 'Creature — Giant').
card_types('quarry colossus', ['Creature']).
card_subtypes('quarry colossus', ['Giant']).
card_colors('quarry colossus', ['W']).
card_text('quarry colossus', 'When Quarry Colossus enters the battlefield, put target creature into its owner\'s library just beneath the top X cards of that library, where X is the number of Plains you control.').
card_mana_cost('quarry colossus', ['5', 'W', 'W']).
card_cmc('quarry colossus', 7).
card_layout('quarry colossus', 'normal').
card_power('quarry colossus', 5).
card_toughness('quarry colossus', 6).

% Found in: LEG
card_name('quarum trench gnomes', 'Quarum Trench Gnomes').
card_type('quarum trench gnomes', 'Creature — Gnome').
card_types('quarum trench gnomes', ['Creature']).
card_subtypes('quarum trench gnomes', ['Gnome']).
card_colors('quarum trench gnomes', ['R']).
card_text('quarum trench gnomes', '{T}: If target Plains is tapped for mana, it produces colorless mana instead of white mana. (This effect lasts indefinitely.)').
card_mana_cost('quarum trench gnomes', ['3', 'R']).
card_cmc('quarum trench gnomes', 4).
card_layout('quarum trench gnomes', 'normal').
card_power('quarum trench gnomes', 1).
card_toughness('quarum trench gnomes', 1).
card_reserved('quarum trench gnomes').

% Found in: BOK, UDS
card_name('quash', 'Quash').
card_type('quash', 'Instant').
card_types('quash', ['Instant']).
card_subtypes('quash', []).
card_colors('quash', ['U']).
card_text('quash', 'Counter target instant or sorcery spell. Search its controller\'s graveyard, hand, and library for all cards with the same name as that spell and exile them. Then that player shuffles his or her library.').
card_mana_cost('quash', ['2', 'U', 'U']).
card_cmc('quash', 4).
card_layout('quash', 'normal').

% Found in: CON
card_name('quenchable fire', 'Quenchable Fire').
card_type('quenchable fire', 'Sorcery').
card_types('quenchable fire', ['Sorcery']).
card_subtypes('quenchable fire', []).
card_colors('quenchable fire', ['R']).
card_text('quenchable fire', 'Quenchable Fire deals 3 damage to target player. It deals an additional 3 damage to that player at the beginning of your next upkeep step unless he or she pays {U} before that step.').
card_mana_cost('quenchable fire', ['3', 'R']).
card_cmc('quenchable fire', 4).
card_layout('quenchable fire', 'normal').

% Found in: ZEN
card_name('quest for ancient secrets', 'Quest for Ancient Secrets').
card_type('quest for ancient secrets', 'Enchantment').
card_types('quest for ancient secrets', ['Enchantment']).
card_subtypes('quest for ancient secrets', []).
card_colors('quest for ancient secrets', ['U']).
card_text('quest for ancient secrets', 'Whenever a card is put into your graveyard from anywhere, you may put a quest counter on Quest for Ancient Secrets.\nRemove five quest counters from Quest for Ancient Secrets and sacrifice it: Target player shuffles his or her graveyard into his or her library.').
card_mana_cost('quest for ancient secrets', ['U']).
card_cmc('quest for ancient secrets', 1).
card_layout('quest for ancient secrets', 'normal').

% Found in: ZEN
card_name('quest for pure flame', 'Quest for Pure Flame').
card_type('quest for pure flame', 'Enchantment').
card_types('quest for pure flame', ['Enchantment']).
card_subtypes('quest for pure flame', []).
card_colors('quest for pure flame', ['R']).
card_text('quest for pure flame', 'Whenever a source you control deals damage to an opponent, you may put a quest counter on Quest for Pure Flame.\nRemove four quest counters from Quest for Pure Flame and sacrifice it: If any source you control would deal damage to a creature or player this turn, it deals double that damage to that creature or player instead.').
card_mana_cost('quest for pure flame', ['R']).
card_cmc('quest for pure flame', 1).
card_layout('quest for pure flame', 'normal').

% Found in: WWK
card_name('quest for renewal', 'Quest for Renewal').
card_type('quest for renewal', 'Enchantment').
card_types('quest for renewal', ['Enchantment']).
card_subtypes('quest for renewal', []).
card_colors('quest for renewal', ['G']).
card_text('quest for renewal', 'Whenever a creature you control becomes tapped, you may put a quest counter on Quest for Renewal.\nAs long as there are four or more quest counters on Quest for Renewal, untap all creatures you control during each other player\'s untap step.').
card_mana_cost('quest for renewal', ['1', 'G']).
card_cmc('quest for renewal', 2).
card_layout('quest for renewal', 'normal').

% Found in: ZEN
card_name('quest for the gemblades', 'Quest for the Gemblades').
card_type('quest for the gemblades', 'Enchantment').
card_types('quest for the gemblades', ['Enchantment']).
card_subtypes('quest for the gemblades', []).
card_colors('quest for the gemblades', ['G']).
card_text('quest for the gemblades', 'Whenever a creature you control deals combat damage to a creature, you may put a quest counter on Quest for the Gemblades.\nRemove a quest counter from Quest for the Gemblades and sacrifice it: Put four +1/+1 counters on target creature.').
card_mana_cost('quest for the gemblades', ['1', 'G']).
card_cmc('quest for the gemblades', 2).
card_layout('quest for the gemblades', 'normal').

% Found in: WWK
card_name('quest for the goblin lord', 'Quest for the Goblin Lord').
card_type('quest for the goblin lord', 'Enchantment').
card_types('quest for the goblin lord', ['Enchantment']).
card_subtypes('quest for the goblin lord', []).
card_colors('quest for the goblin lord', ['R']).
card_text('quest for the goblin lord', 'Whenever a Goblin enters the battlefield under your control, you may put a quest counter on Quest for the Goblin Lord.\nAs long as Quest for the Goblin Lord has five or more quest counters on it, creatures you control get +2/+0.').
card_mana_cost('quest for the goblin lord', ['R']).
card_cmc('quest for the goblin lord', 1).
card_layout('quest for the goblin lord', 'normal').

% Found in: ZEN
card_name('quest for the gravelord', 'Quest for the Gravelord').
card_type('quest for the gravelord', 'Enchantment').
card_types('quest for the gravelord', ['Enchantment']).
card_subtypes('quest for the gravelord', []).
card_colors('quest for the gravelord', ['B']).
card_text('quest for the gravelord', 'Whenever a creature dies, you may put a quest counter on Quest for the Gravelord.\nRemove three quest counters from Quest for the Gravelord and sacrifice it: Put a 5/5 black Zombie Giant creature token onto the battlefield.').
card_mana_cost('quest for the gravelord', ['B']).
card_cmc('quest for the gravelord', 1).
card_layout('quest for the gravelord', 'normal').

% Found in: ZEN
card_name('quest for the holy relic', 'Quest for the Holy Relic').
card_type('quest for the holy relic', 'Enchantment').
card_types('quest for the holy relic', ['Enchantment']).
card_subtypes('quest for the holy relic', []).
card_colors('quest for the holy relic', ['W']).
card_text('quest for the holy relic', 'Whenever you cast a creature spell, you may put a quest counter on Quest for the Holy Relic.\nRemove five quest counters from Quest for the Holy Relic and sacrifice it: Search your library for an Equipment card, put it onto the battlefield, and attach it to a creature you control. Then shuffle your library.').
card_mana_cost('quest for the holy relic', ['W']).
card_cmc('quest for the holy relic', 1).
card_layout('quest for the holy relic', 'normal').

% Found in: WWK
card_name('quest for the nihil stone', 'Quest for the Nihil Stone').
card_type('quest for the nihil stone', 'Enchantment').
card_types('quest for the nihil stone', ['Enchantment']).
card_subtypes('quest for the nihil stone', []).
card_colors('quest for the nihil stone', ['B']).
card_text('quest for the nihil stone', 'Whenever an opponent discards a card, you may put a quest counter on Quest for the Nihil Stone.\nAt the beginning of each opponent\'s upkeep, if that player has no cards in hand and Quest for the Nihil Stone has two or more quest counters on it, you may have that player lose 5 life.').
card_mana_cost('quest for the nihil stone', ['B']).
card_cmc('quest for the nihil stone', 1).
card_layout('quest for the nihil stone', 'normal').

% Found in: WWK
card_name('quest for ula\'s temple', 'Quest for Ula\'s Temple').
card_type('quest for ula\'s temple', 'Enchantment').
card_types('quest for ula\'s temple', ['Enchantment']).
card_subtypes('quest for ula\'s temple', []).
card_colors('quest for ula\'s temple', ['U']).
card_text('quest for ula\'s temple', 'At the beginning of your upkeep, you may look at the top card of your library. If it\'s a creature card, you may reveal it and put a quest counter on Quest for Ula\'s Temple.\nAt the beginning of each end step, if there are three or more quest counters on Quest for Ula\'s Temple, you may put a Kraken, Leviathan, Octopus, or Serpent creature card from your hand onto the battlefield.').
card_mana_cost('quest for ula\'s temple', ['U']).
card_cmc('quest for ula\'s temple', 1).
card_layout('quest for ula\'s temple', 'normal').

% Found in: PLS, pPRE
card_name('questing phelddagrif', 'Questing Phelddagrif').
card_type('questing phelddagrif', 'Creature — Phelddagrif').
card_types('questing phelddagrif', ['Creature']).
card_subtypes('questing phelddagrif', ['Phelddagrif']).
card_colors('questing phelddagrif', ['W', 'U', 'G']).
card_text('questing phelddagrif', '{G}: Questing Phelddagrif gets +1/+1 until end of turn. Target opponent puts a 1/1 green Hippo creature token onto the battlefield.\n{W}: Questing Phelddagrif gains protection from black and from red until end of turn. Target opponent gains 2 life.\n{U}: Questing Phelddagrif gains flying until end of turn. Target opponent may draw a card.').
card_mana_cost('questing phelddagrif', ['1', 'G', 'W', 'U']).
card_cmc('questing phelddagrif', 4).
card_layout('questing phelddagrif', 'normal').
card_power('questing phelddagrif', 4).
card_toughness('questing phelddagrif', 4).

% Found in: UNH
card_name('question elemental?', 'Question Elemental?').
card_type('question elemental?', 'Creature — Elemental').
card_types('question elemental?', ['Creature']).
card_subtypes('question elemental?', ['Elemental']).
card_colors('question elemental?', ['U']).
card_text('question elemental?', 'Flying\nAre you aware that when you say something that isn\'t a question, the player who first points out this fact gains control of Question Elemental?').
card_mana_cost('question elemental?', ['2', 'U', 'U']).
card_cmc('question elemental?', 4).
card_layout('question elemental?', 'normal').
card_power('question elemental?', 3).
card_toughness('question elemental?', 4).

% Found in: H09, LGN
card_name('quick sliver', 'Quick Sliver').
card_type('quick sliver', 'Creature — Sliver').
card_types('quick sliver', ['Creature']).
card_subtypes('quick sliver', ['Sliver']).
card_colors('quick sliver', ['G']).
card_text('quick sliver', 'Flash\nAny player may play Sliver cards as though they had flash.').
card_mana_cost('quick sliver', ['1', 'G']).
card_cmc('quick sliver', 2).
card_layout('quick sliver', 'normal').
card_power('quick sliver', 1).
card_toughness('quick sliver', 1).

% Found in: RAV
card_name('quickchange', 'Quickchange').
card_type('quickchange', 'Instant').
card_types('quickchange', ['Instant']).
card_subtypes('quickchange', []).
card_colors('quickchange', ['U']).
card_text('quickchange', 'Target creature becomes the color or colors of your choice until end of turn.\nDraw a card.').
card_mana_cost('quickchange', ['1', 'U']).
card_cmc('quickchange', 2).
card_layout('quickchange', 'normal').

% Found in: GPT, M14
card_name('quicken', 'Quicken').
card_type('quicken', 'Instant').
card_types('quicken', ['Instant']).
card_subtypes('quicken', []).
card_colors('quicken', ['U']).
card_text('quicken', 'The next sorcery card you cast this turn can be cast as though it had flash. (It can be cast any time you could cast an instant.)\nDraw a card.').
card_mana_cost('quicken', ['U']).
card_cmc('quicken', 1).
card_layout('quicken', 'normal').

% Found in: TMP
card_name('quickening licid', 'Quickening Licid').
card_type('quickening licid', 'Creature — Licid').
card_types('quickening licid', ['Creature']).
card_subtypes('quickening licid', ['Licid']).
card_colors('quickening licid', ['W']).
card_text('quickening licid', '{1}{W}, {T}: Quickening Licid loses this ability and becomes an Aura enchantment with enchant creature. Attach it to target creature. You may pay {W} to end this effect.\nEnchanted creature has first strike.').
card_mana_cost('quickening licid', ['1', 'W']).
card_cmc('quickening licid', 2).
card_layout('quickening licid', 'normal').
card_power('quickening licid', 1).
card_toughness('quickening licid', 1).

% Found in: M15
card_name('quickling', 'Quickling').
card_type('quickling', 'Creature — Faerie Rogue').
card_types('quickling', ['Creature']).
card_subtypes('quickling', ['Faerie', 'Rogue']).
card_colors('quickling', ['U']).
card_text('quickling', 'Flash (You may cast this spell any time you could cast an instant.)\nFlying\nWhen Quickling enters the battlefield, sacrifice it unless you return another creature you control to its owner\'s hand.').
card_mana_cost('quickling', ['1', 'U']).
card_cmc('quickling', 2).
card_layout('quickling', 'normal').
card_power('quickling', 2).
card_toughness('quickling', 2).

% Found in: 10E, 9ED, CNS, VIS, WWK
card_name('quicksand', 'Quicksand').
card_type('quicksand', 'Land').
card_types('quicksand', ['Land']).
card_subtypes('quicksand', []).
card_colors('quicksand', []).
card_text('quicksand', '{T}: Add {1} to your mana pool.\n{T}, Sacrifice Quicksand: Target attacking creature without flying gets -1/-2 until end of turn.').
card_layout('quicksand', 'normal').

% Found in: M12, ULG
card_name('quicksilver amulet', 'Quicksilver Amulet').
card_type('quicksilver amulet', 'Artifact').
card_types('quicksilver amulet', ['Artifact']).
card_subtypes('quicksilver amulet', []).
card_colors('quicksilver amulet', []).
card_text('quicksilver amulet', '{4}, {T}: You may put a creature card from your hand onto the battlefield.').
card_mana_cost('quicksilver amulet', ['4']).
card_cmc('quicksilver amulet', 4).
card_layout('quicksilver amulet', 'normal').

% Found in: DST
card_name('quicksilver behemoth', 'Quicksilver Behemoth').
card_type('quicksilver behemoth', 'Creature — Beast').
card_types('quicksilver behemoth', ['Creature']).
card_subtypes('quicksilver behemoth', ['Beast']).
card_colors('quicksilver behemoth', ['U']).
card_text('quicksilver behemoth', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nWhen Quicksilver Behemoth attacks or blocks, return it to its owner\'s hand at end of combat. (Return it only if it\'s on the battlefield.)').
card_mana_cost('quicksilver behemoth', ['6', 'U']).
card_cmc('quicksilver behemoth', 7).
card_layout('quicksilver behemoth', 'normal').
card_power('quicksilver behemoth', 4).
card_toughness('quicksilver behemoth', 5).

% Found in: APC, DDJ
card_name('quicksilver dagger', 'Quicksilver Dagger').
card_type('quicksilver dagger', 'Enchantment — Aura').
card_types('quicksilver dagger', ['Enchantment']).
card_subtypes('quicksilver dagger', ['Aura']).
card_colors('quicksilver dagger', ['U', 'R']).
card_text('quicksilver dagger', 'Enchant creature\nEnchanted creature has \"{T}: This creature deals 1 damage to target player. You draw a card.\"').
card_mana_cost('quicksilver dagger', ['1', 'U', 'R']).
card_cmc('quicksilver dagger', 3).
card_layout('quicksilver dagger', 'normal').

% Found in: DD2, DD3_JVC, ONS
card_name('quicksilver dragon', 'Quicksilver Dragon').
card_type('quicksilver dragon', 'Creature — Dragon').
card_types('quicksilver dragon', ['Creature']).
card_subtypes('quicksilver dragon', ['Dragon']).
card_colors('quicksilver dragon', ['U']).
card_text('quicksilver dragon', 'Flying\n{U}: If target spell has only one target and that target is Quicksilver Dragon, change that spell\'s target to another creature.\nMorph {4}{U} (You may cast this card face down as a 2/2 creature for {3}. Turn it face up any time for its morph cost.)').
card_mana_cost('quicksilver dragon', ['4', 'U', 'U']).
card_cmc('quicksilver dragon', 6).
card_layout('quicksilver dragon', 'normal').
card_power('quicksilver dragon', 5).
card_toughness('quicksilver dragon', 5).

% Found in: MRD
card_name('quicksilver elemental', 'Quicksilver Elemental').
card_type('quicksilver elemental', 'Creature — Elemental').
card_types('quicksilver elemental', ['Creature']).
card_subtypes('quicksilver elemental', ['Elemental']).
card_colors('quicksilver elemental', ['U']).
card_text('quicksilver elemental', '{U}: Quicksilver Elemental gains all activated abilities of target creature until end of turn. (If any of the abilities use that creature\'s name, use this creature\'s name instead.)\nYou may spend blue mana as though it were mana of any color to pay the activation costs of Quicksilver Elemental\'s abilities.').
card_mana_cost('quicksilver elemental', ['3', 'U', 'U']).
card_cmc('quicksilver elemental', 5).
card_layout('quicksilver elemental', 'normal').
card_power('quicksilver elemental', 3).
card_toughness('quicksilver elemental', 4).

% Found in: MRD
card_name('quicksilver fountain', 'Quicksilver Fountain').
card_type('quicksilver fountain', 'Artifact').
card_types('quicksilver fountain', ['Artifact']).
card_subtypes('quicksilver fountain', []).
card_colors('quicksilver fountain', []).
card_text('quicksilver fountain', 'At the beginning of each player\'s upkeep, that player puts a flood counter on target non-Island land he or she controls of his or her choice. That land is an Island for as long as it has a flood counter on it.\nAt the beginning of each end step, if all lands on the battlefield are Islands, remove all flood counters from them.').
card_mana_cost('quicksilver fountain', ['3']).
card_cmc('quicksilver fountain', 3).
card_layout('quicksilver fountain', 'normal').

% Found in: SOM
card_name('quicksilver gargantuan', 'Quicksilver Gargantuan').
card_type('quicksilver gargantuan', 'Creature — Shapeshifter').
card_types('quicksilver gargantuan', ['Creature']).
card_subtypes('quicksilver gargantuan', ['Shapeshifter']).
card_colors('quicksilver gargantuan', ['U']).
card_text('quicksilver gargantuan', 'You may have Quicksilver Gargantuan enter the battlefield as a copy of any creature on the battlefield, except it\'s still 7/7.').
card_mana_cost('quicksilver gargantuan', ['5', 'U', 'U']).
card_cmc('quicksilver gargantuan', 7).
card_layout('quicksilver gargantuan', 'normal').
card_power('quicksilver gargantuan', 7).
card_toughness('quicksilver gargantuan', 7).

% Found in: MBS
card_name('quicksilver geyser', 'Quicksilver Geyser').
card_type('quicksilver geyser', 'Instant').
card_types('quicksilver geyser', ['Instant']).
card_subtypes('quicksilver geyser', []).
card_colors('quicksilver geyser', ['U']).
card_text('quicksilver geyser', 'Return up to two target nonland permanents to their owners\' hands.').
card_mana_cost('quicksilver geyser', ['4', 'U']).
card_cmc('quicksilver geyser', 5).
card_layout('quicksilver geyser', 'normal').

% Found in: PC2
card_name('quicksilver sea', 'Quicksilver Sea').
card_type('quicksilver sea', 'Plane — Mirrodin').
card_types('quicksilver sea', ['Plane']).
card_subtypes('quicksilver sea', ['Mirrodin']).
card_colors('quicksilver sea', []).
card_text('quicksilver sea', 'When you planeswalk to Quicksilver Sea or at the beginning of your upkeep, scry 4. (Look at the top four cards of your library, then put any number of them on the bottom of your library and the rest on top in any order.)\nWhenever you roll {C}, reveal the top card of your library. You may play it without paying its mana cost.').
card_layout('quicksilver sea', 'plane').

% Found in: PCY
card_name('quicksilver wall', 'Quicksilver Wall').
card_type('quicksilver wall', 'Creature — Wall').
card_types('quicksilver wall', ['Creature']).
card_subtypes('quicksilver wall', ['Wall']).
card_colors('quicksilver wall', ['U']).
card_text('quicksilver wall', 'Defender (This creature can\'t attack.)\n{4}: Return Quicksilver Wall to its owner\'s hand. Any player may activate this ability.').
card_mana_cost('quicksilver wall', ['2', 'U']).
card_cmc('quicksilver wall', 3).
card_layout('quicksilver wall', 'normal').
card_power('quicksilver wall', 1).
card_toughness('quicksilver wall', 6).

% Found in: KTK
card_name('quiet contemplation', 'Quiet Contemplation').
card_type('quiet contemplation', 'Enchantment').
card_types('quiet contemplation', ['Enchantment']).
card_subtypes('quiet contemplation', []).
card_colors('quiet contemplation', ['U']).
card_text('quiet contemplation', 'Whenever you cast a noncreature spell, you may pay {1}. If you do, tap target creature an opponent controls and it doesn\'t untap during its controller\'s next untap step.').
card_mana_cost('quiet contemplation', ['2', 'U']).
card_cmc('quiet contemplation', 3).
card_layout('quiet contemplation', 'normal').

% Found in: FUT, PC2
card_name('quiet disrepair', 'Quiet Disrepair').
card_type('quiet disrepair', 'Enchantment — Aura').
card_types('quiet disrepair', ['Enchantment']).
card_subtypes('quiet disrepair', ['Aura']).
card_colors('quiet disrepair', ['G']).
card_text('quiet disrepair', 'Enchant artifact or enchantment\nAt the beginning of your upkeep, choose one —\n• Destroy enchanted permanent.\n• You gain 2 life.').
card_mana_cost('quiet disrepair', ['1', 'G']).
card_cmc('quiet disrepair', 2).
card_layout('quiet disrepair', 'normal').

% Found in: CHK
card_name('quiet purity', 'Quiet Purity').
card_type('quiet purity', 'Instant — Arcane').
card_types('quiet purity', ['Instant']).
card_subtypes('quiet purity', ['Arcane']).
card_colors('quiet purity', ['W']).
card_text('quiet purity', 'Destroy target enchantment.').
card_mana_cost('quiet purity', ['W']).
card_cmc('quiet purity', 1).
card_layout('quiet purity', 'normal').

% Found in: JUD
card_name('quiet speculation', 'Quiet Speculation').
card_type('quiet speculation', 'Sorcery').
card_types('quiet speculation', ['Sorcery']).
card_subtypes('quiet speculation', []).
card_colors('quiet speculation', ['U']).
card_text('quiet speculation', 'Search target player\'s library for up to three cards with flashback and put them into that player\'s graveyard. Then the player shuffles his or her library.').
card_mana_cost('quiet speculation', ['1', 'U']).
card_cmc('quiet speculation', 2).
card_layout('quiet speculation', 'normal').

% Found in: ALA, PC2
card_name('quietus spike', 'Quietus Spike').
card_type('quietus spike', 'Artifact — Equipment').
card_types('quietus spike', ['Artifact']).
card_subtypes('quietus spike', ['Equipment']).
card_colors('quietus spike', []).
card_text('quietus spike', 'Equipped creature has deathtouch.\nWhenever equipped creature deals combat damage to a player, that player loses half his or her life, rounded up.\nEquip {3}').
card_mana_cost('quietus spike', ['3']).
card_cmc('quietus spike', 3).
card_layout('quietus spike', 'normal').

% Found in: LRW
card_name('quill-slinger boggart', 'Quill-Slinger Boggart').
card_type('quill-slinger boggart', 'Creature — Goblin Warrior').
card_types('quill-slinger boggart', ['Creature']).
card_subtypes('quill-slinger boggart', ['Goblin', 'Warrior']).
card_colors('quill-slinger boggart', ['B']).
card_text('quill-slinger boggart', 'Whenever a player casts a Kithkin spell, you may have target player lose 1 life.').
card_mana_cost('quill-slinger boggart', ['3', 'B']).
card_cmc('quill-slinger boggart', 4).
card_layout('quill-slinger boggart', 'normal').
card_power('quill-slinger boggart', 3).
card_toughness('quill-slinger boggart', 2).

% Found in: MBS
card_name('quilled slagwurm', 'Quilled Slagwurm').
card_type('quilled slagwurm', 'Creature — Wurm').
card_types('quilled slagwurm', ['Creature']).
card_subtypes('quilled slagwurm', ['Wurm']).
card_colors('quilled slagwurm', ['G']).
card_text('quilled slagwurm', '').
card_mana_cost('quilled slagwurm', ['4', 'G', 'G', 'G']).
card_cmc('quilled slagwurm', 7).
card_layout('quilled slagwurm', 'normal').
card_power('quilled slagwurm', 8).
card_toughness('quilled slagwurm', 8).

% Found in: TSP
card_name('quilled sliver', 'Quilled Sliver').
card_type('quilled sliver', 'Creature — Sliver').
card_types('quilled sliver', ['Creature']).
card_subtypes('quilled sliver', ['Sliver']).
card_colors('quilled sliver', ['W']).
card_text('quilled sliver', 'All Slivers have \"{T}: This permanent deals 1 damage to target attacking or blocking creature.\"').
card_mana_cost('quilled sliver', ['1', 'W']).
card_cmc('quilled sliver', 2).
card_layout('quilled sliver', 'normal').
card_power('quilled sliver', 1).
card_toughness('quilled sliver', 1).

% Found in: BOK
card_name('quillmane baku', 'Quillmane Baku').
card_type('quillmane baku', 'Creature — Spirit').
card_types('quillmane baku', ['Creature']).
card_subtypes('quillmane baku', ['Spirit']).
card_colors('quillmane baku', ['U']).
card_text('quillmane baku', 'Whenever you cast a Spirit or Arcane spell, you may put a ki counter on Quillmane Baku.\n{1}, {T}, Remove X ki counters from Quillmane Baku: Return target creature with converted mana cost X or less to its owner\'s hand.').
card_mana_cost('quillmane baku', ['4', 'U']).
card_cmc('quillmane baku', 5).
card_layout('quillmane baku', 'normal').
card_power('quillmane baku', 3).
card_toughness('quillmane baku', 3).

% Found in: EVE
card_name('quillspike', 'Quillspike').
card_type('quillspike', 'Creature — Beast').
card_types('quillspike', ['Creature']).
card_subtypes('quillspike', ['Beast']).
card_colors('quillspike', ['B', 'G']).
card_text('quillspike', '{B/G}, Remove a -1/-1 counter from a creature you control: Quillspike gets +3/+3 until end of turn.').
card_mana_cost('quillspike', ['2', 'B/G']).
card_cmc('quillspike', 3).
card_layout('quillspike', 'normal').
card_power('quillspike', 1).
card_toughness('quillspike', 1).

% Found in: VIS
card_name('quirion druid', 'Quirion Druid').
card_type('quirion druid', 'Creature — Elf Druid').
card_types('quirion druid', ['Creature']).
card_subtypes('quirion druid', ['Elf', 'Druid']).
card_colors('quirion druid', ['G']).
card_text('quirion druid', '{G}, {T}: Target land becomes a 2/2 green creature that\'s still a land. (This effect lasts indefinitely.)').
card_mana_cost('quirion druid', ['2', 'G']).
card_cmc('quirion druid', 3).
card_layout('quirion druid', 'normal').
card_power('quirion druid', 1).
card_toughness('quirion druid', 2).
card_reserved('quirion druid').

% Found in: 10E, M13, PLS
card_name('quirion dryad', 'Quirion Dryad').
card_type('quirion dryad', 'Creature — Dryad').
card_types('quirion dryad', ['Creature']).
card_subtypes('quirion dryad', ['Dryad']).
card_colors('quirion dryad', ['G']).
card_text('quirion dryad', 'Whenever you cast a white, blue, black, or red spell, put a +1/+1 counter on Quirion Dryad.').
card_mana_cost('quirion dryad', ['1', 'G']).
card_cmc('quirion dryad', 2).
card_layout('quirion dryad', 'normal').
card_power('quirion dryad', 1).
card_toughness('quirion dryad', 1).

% Found in: BTD, DDE, INV, MIR
card_name('quirion elves', 'Quirion Elves').
card_type('quirion elves', 'Creature — Elf Druid').
card_types('quirion elves', ['Creature']).
card_subtypes('quirion elves', ['Elf', 'Druid']).
card_colors('quirion elves', ['G']).
card_text('quirion elves', 'As Quirion Elves enters the battlefield, choose a color.\n{T}: Add {G} to your mana pool.\n{T}: Add one mana of the chosen color to your mana pool.').
card_mana_cost('quirion elves', ['1', 'G']).
card_cmc('quirion elves', 2).
card_layout('quirion elves', 'normal').
card_power('quirion elves', 1).
card_toughness('quirion elves', 1).

% Found in: PLS
card_name('quirion explorer', 'Quirion Explorer').
card_type('quirion explorer', 'Creature — Elf Druid Scout').
card_types('quirion explorer', ['Creature']).
card_subtypes('quirion explorer', ['Elf', 'Druid', 'Scout']).
card_colors('quirion explorer', ['G']).
card_text('quirion explorer', '{T}: Add to your mana pool one mana of any color that a land an opponent controls could produce.').
card_mana_cost('quirion explorer', ['1', 'G']).
card_cmc('quirion explorer', 2).
card_layout('quirion explorer', 'normal').
card_power('quirion explorer', 1).
card_toughness('quirion explorer', 1).

% Found in: VIS, pFNM
card_name('quirion ranger', 'Quirion Ranger').
card_type('quirion ranger', 'Creature — Elf').
card_types('quirion ranger', ['Creature']).
card_subtypes('quirion ranger', ['Elf']).
card_colors('quirion ranger', ['G']).
card_text('quirion ranger', 'Return a Forest you control to its owner\'s hand: Untap target creature. Activate this ability only once each turn.').
card_mana_cost('quirion ranger', ['G']).
card_cmc('quirion ranger', 1).
card_layout('quirion ranger', 'normal').
card_power('quirion ranger', 1).
card_toughness('quirion ranger', 1).

% Found in: INV
card_name('quirion sentinel', 'Quirion Sentinel').
card_type('quirion sentinel', 'Creature — Elf Druid').
card_types('quirion sentinel', ['Creature']).
card_subtypes('quirion sentinel', ['Elf', 'Druid']).
card_colors('quirion sentinel', ['G']).
card_text('quirion sentinel', 'When Quirion Sentinel enters the battlefield, add one mana of any color to your mana pool.').
card_mana_cost('quirion sentinel', ['1', 'G']).
card_cmc('quirion sentinel', 2).
card_layout('quirion sentinel', 'normal').
card_power('quirion sentinel', 2).
card_toughness('quirion sentinel', 1).

% Found in: INV
card_name('quirion trailblazer', 'Quirion Trailblazer').
card_type('quirion trailblazer', 'Creature — Elf Scout').
card_types('quirion trailblazer', ['Creature']).
card_subtypes('quirion trailblazer', ['Elf', 'Scout']).
card_colors('quirion trailblazer', ['G']).
card_text('quirion trailblazer', 'When Quirion Trailblazer enters the battlefield, you may search your library for a basic land card and put that card onto the battlefield tapped. If you do, shuffle your library.').
card_mana_cost('quirion trailblazer', ['3', 'G']).
card_cmc('quirion trailblazer', 4).
card_layout('quirion trailblazer', 'normal').
card_power('quirion trailblazer', 1).
card_toughness('quirion trailblazer', 2).

% Found in: 5DN, DDF, HOP, MM2
card_name('qumulox', 'Qumulox').
card_type('qumulox', 'Creature — Beast').
card_types('qumulox', ['Creature']).
card_subtypes('qumulox', ['Beast']).
card_colors('qumulox', ['U']).
card_text('qumulox', 'Affinity for artifacts (This spell costs {1} less to cast for each artifact you control.)\nFlying').
card_mana_cost('qumulox', ['6', 'U', 'U']).
card_cmc('qumulox', 8).
card_layout('qumulox', 'normal').
card_power('qumulox', 5).
card_toughness('qumulox', 4).

